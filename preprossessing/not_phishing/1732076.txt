<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<!--[if IE]><script src="../../../html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script src="/assets/eae64ddd/jquery.min.js" type="text/javascript"></script>
<script src="/assets/eae64ddd/jquery.yiiactiveform.js" type="text/javascript"></script>
<title>Юридический форум России</title>
<meta content="призывник, помощь призывнику, отсрочка от армии, адвокат, юридический форум, юридические консультации, юридические услуги, федеральные законы, кодексы РФ, юристы, адвокаты, бланки договоров" name="keywords"/>
<meta content="Юридический форум Зона Закона! На сайте вы получите бесплатные юридические консультации по любым вопросам, в том числе разъяснения по федеральным законам, кодексам РФ. Профессиональные юристы и адвокаты окажут вам юридические услуги. Также вы можете найти на форуме большое количество бланков договоров." name="description"/>
<meta content="width=device-width" name="viewport"/>
<meta content="#000" name="msapplication-TileColor"/>
<meta content="icon.png" name="msapplication-TileImage"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/touch-icon-iphone.png" rel="apple-touch-icon"/>
<link href="/touch-icon-ipad.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/touch-icon-iphone-retina.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/touch-icon-ipad-retina.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/themes/zonazakona/web/css/bootstrap.css" media="screen, projection" rel="stylesheet" type="text/css"/>
<link href="/themes/zonazakona/web/css/jquery.jscrollpane.css" media="screen, projection" rel="stylesheet" type="text/css"/>
<link href="/themes/zonazakona/web/css/jquery.formstyler.css" media="screen, projection" rel="stylesheet" type="text/css"/>
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
<link href="/themes/zonazakona/web/css/pager.css" media="screen, projection" rel="stylesheet" type="text/css"/>
<link href="/themes/zonazakona/web/css/style.css" media="screen, projection" rel="stylesheet" type="text/css"/>
<link href="/themes/zonazakona/web/css/style.alerts.css" media="screen, projection" rel="stylesheet" type="text/css"/>
<script src="/themes/zonazakona/web/js/jquery.fileupload.js" type="text/javascript"></script>
<script src="/themes/zonazakona/web/js/jquery.maskedinput.js" type="text/javascript"></script>
<script src="/themes/zonazakona/web/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/themes/zonazakona/web/js/jquery.formstyler.min.js" type="text/javascript"></script>
<script src="/themes/zonazakona/web/js/jquery.jscrollpane.min.js" type="text/javascript"></script>
<script src="/themes/zonazakona/web/js/jalerts/jquery.alerts.js" type="text/javascript"></script>
<script src="/themes/zonazakona/web/js/jquery.loading.js" type="text/javascript"></script>
<script src="/themes/zonazakona/web/js/scripts.js" type="text/javascript"></script>
<script async="" data-ad-client="ca-pub-3342635859225483" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<body>
<div class="page index">
<!-- Header -->
<header itemscope="" itemtype="http://schema.org/WPHeader">
<div class="top-menu wrapper">
<ul class="centered">
<!--<li><a href="" class="iconed icon-googleplus"></i>google+</a></li>
            <li><a href="" class="iconed icon-twitter"></i>twitter</a></li>
            <li><a href="" class="iconed icon-facebook">facebook</a></li>-->
</ul>
<ul class="right pull-right">
<li><a href="/registration/">Регистрация</a></li>
<li>и</li>
<li><a data-toggle="modal" href="#login-modal">личный кабинет</a></li>
</ul>
</div>
<section class="b-image" style="color: white;">
</section>
<section class="title wrapper">
<div class="logo">
<span itemprop="headline">Зона закона</span>
<meta content="Юридический интернет портал" itemprop="description"/>
<meta content="https://www.zonazakona.ru/themes/zonazakona/web/images/logo.png" itemprop="image"/>
</div>
<div class="b-image-hright">
</div>
</section>
<section class="main-menu">
<div class="wrapper">
<ul> <li><a href="/">Главная</a> <span></span>
</li>
<li><a href="https://www.zonazakona.ru/forum">Форум</a> <span></span>
</li>
<li><a href="/documents/">Документы</a> <span></span>
</li>
<li><a href="/vsezakony/">Законодательство</a> <span></span>
</li>
</ul>
</div>
</section>
</header><!-- \Header -->
<div class="content">
<div class="wrapper shadowed-large">
<!-- Search FORM -->
<!-- \Search FORM -->
<section class="info-about femida-bg" style="background-color: white; border-bottom: solid 1px #ccc;">
<div class="info">
<h1>Зона Закона, для чего этот сайт</h1>
<p>
	     Адвокатские консультации на форуме адвокатов Зона закона.РУ Адвокат ответит на вопросы по ДТП, ГИБДД и возмещению ущерба.
</p>
<p>
	     Основные разделы сайта - помощь призывникам в отсрочке от армии (откос от армии), раздел имущества, уголовное право, защита потребителей. Узнайте от адвоката как получить наследство и многое другое на страницах Зоны закона.
</p>
<p>
	     Юристы форума Зона закона ответят на любые вопросы по темам: трудовое право, финансовое и налоговое право. Бесплатные юридические консультации. Федеральный закон (бланки договоров)
</p> </div>
</section>
<div style="clear: both;"></div>
<section class="old-documents grey-theme">
<div style="text-align: center;"><h1>Законодательство</h1></div>
<ul class="list-links">
<li class="col-3">
<ul>
<li><a href="/law/docs/">Типовые бланки, договоры</a></li>
<li><a href="/law/zakon_mo/">Законодательство Московской области</a></li>
<li><a href="/law/zakon_med/">Медицинское законодательство</a></li>
<li><a href="/law/abro/">Международное законодательство</a></li>
<li><a href="/law/jude_moskow/">Судебная практика: Москва и Московская область</a></li>
<li><a href="/law/jude_szo/">Судебная практика: Северо-Запад</a></li>
<li><a href="/law/jude_vostsib/">Судебная практика: Восточная Сибирь</a></li>
<li><a href="/law/consult_buh/">Бухгалтерские консультации</a></li>
</ul>
</li>
<li class="col-3">
<ul>
<li><a href="/law/zakon_rf/">Законодательство РФ</a></li>
<li><a href="/law/zakon_spb/">Законодательство Санкт-Петербурга и Ленинградской области</a></li>
<li><a href="/law/projects/">Законопроекты</a></li>
<li><a href="/law/comments/">Комментарии к законам</a></li>
<li><a href="/law/jude_povol/">Судебная практика: Поволжье</a></li>
<li><a href="/law/jude_ural/">Судебная практика: Урал</a></li>
<li><a href="/law/jude_zapsib/">Судебная практика: Западная Сибирь</a></li>
<li><a href="/law/consult_fin/">Финансовые консультации</a></li>
</ul>
</li>
<li class="col-3">
<ul>
<li><a href="/law/zakon_msk/">Законодательство Москвы</a></li>
<li><a href="/law/ukaz/">Постановления и Указы</a></li>
<li><a href="/law/docs_ussr/">Документы СССР</a></li>
<li><a href="/law/jude_practice/">Общая судебная практика</a></li>
<li><a href="/law/jude_kavkaz/">Судебная практика: Северо-Кавказский регион</a></li>
<li><a href="/law/jude_volga/">Судебная практика: Волговятский регион</a></li>
<li><a href="/law/docs_ur/">Юридические статьи</a></li>
<li><a href="/law/docs_buh/">Статьи бухгалтеру</a></li>
</ul>
</li>
</ul>
</section>
<!-- Lawyer Rating -->
<!-- \Lawyer Rating -->
<!-- Lawyer Rating -->
<div class="forum-info light-theme" id="forum-info">
<div class="messages">
<div style="text-align: center"><h1>Форум</h1></div>
<ul class="forum-topics_new">
<li class="iconed-man">
<h2><a href="https://www.zonazakona.ru/forum/forum/11-forum-dlya-prizyivnikov-i-voennosluzhashhih/">Форум<br/>для призывников<br/> и военнослужащих​</a></h2>
<ul>
<li>
<div class="right-info">
<h4><a href="https://www.zonazakona.ru/forum/forum/33-pomoshh-prizyivnikam-i-otsrochka-ot-armii/">Помощь призывникам и отсрочка от армии</a></h4>
<div class="annotation">Консультации для призывников, военный билет, военно-врачебная экспертиза</div>
</div>
</li>
<li>
<div class="right-info">
<h4><a href="https://www.zonazakona.ru/forum/forum/65-forum-po-voenno-vrachebnoy-ekspertize-dlya-prizyivnikov/">Форум по военно-врачебной экспертизе для призывников</a></h4>
<div class="annotation">Консультации по медицинским вопросам для призывников</div>
</div>
</li>
<li>
<div class="right-info">
<h4><a href="https://www.zonazakona.ru/forum/forum/55-yuridicheskaya-pomoshh-voennosluzhashhim/">Юридическая помощь военнослужащим</a></h4>
<div class="annotation">Увольнение из армии, получение жилья и денег с министерства обороны, боевые деньги, социальные вопросы</div>
</div>
</li>
<li>
<div class="right-info">
<h4><a href="https://www.zonazakona.ru/forum/forum/64-stati/">Статьи</a></h4>
<div class="annotation">Статьи о призыве</div>
</div>
</li>
</ul>
</li>
<li class="iconed-house">
<h2><a href="https://www.zonazakona.ru/forum/forum/8-zhilishhnoe-pravo-yuridicheskaya-konsultatsiya/">Жилищное право<br/> юридическая<br/> консультация</a></h2>
<ul>
<li>
<div class="right-info">
<h4><a href="https://www.zonazakona.ru/forum/forum/28-obshhie-voprosyi-po-zhilishhnomu-pravu/">Общие вопросы по жилищному праву</a></h4>
<div class="annotation">Регистрация права собственности, приватизация, бытовые вопросы</div>
</div>
</li>
<li>
<div class="right-info">
<h4><a href="https://www.zonazakona.ru/forum/forum/42-plata-za-zhile-upravlenie-zhilishhnyim-fondom/">Плата за жилье, управление жилищным фондом</a></h4>
<div class="annotation">ТСЖ и управляющие компании. Права и обязанности собственников жилья</div>
</div>
</li>
<li>
<div class="right-info">
<h4><a href="https://www.zonazakona.ru/forum/forum/44-predostavlenie-zhilyih-pomeshheniy/">Предоставление жилых помещений</a></h4>
<div class="annotation">Аварийное жилье и учет нуждающихся, очередь на жилье</div>
</div>
</li>
<li>
<div class="right-info">
<h4><a href="https://www.zonazakona.ru/forum/forum/45-registratsiya-propiska/">Регистрация (прописка)</a></h4>
<div class="annotation">Снятие с регистрационного учета, перерегистрация, выписка членов семьи</div>
</div>
</li>
<li>
<div class="right-info">
<h4><a href="https://www.zonazakona.ru/forum/forum/46-sdelki-s-zhilyimi-pomeshheniyami/">Сделки с жилыми помещениями</a></h4>
<div class="annotation">Купля, продажа, приватизация, аренда жилых помещений</div>
</div>
</li>
<li>
<div class="right-info">
<h4><a href="https://www.zonazakona.ru/forum/forum/68-obmanutyie-dolshhiki/">Обманутые дольщики</a></h4>
<div class="annotation">Взыскание неустойки по ФЗ №214-ФЗ</div>
</div>
</li>
<li style="padding-bottom: 0px;text-align: center;"><a class="btn" href="https://www.zonazakona.ru/forum/" style="    margin-top: 20px;">Перейти ко все разделам форума</a></li>
</ul>
</li>
<li class="iconed-car">
<h2><a href="https://www.zonazakona.ru/forum/forum/10-yuridicheskaya-pomoshh-avtovladeltsam/">Юридическая<br/> помощь<br/> автовладельцам</a></h2>
<ul>
<li>
<div class="right-info">
<h4><a href="https://www.zonazakona.ru/forum/forum/40-vozmeshhenie-ushherba-avtovladeltsam/">Возмещение ущерба автовладельцам</a></h4>
<div class="annotation">Автострахование, споры со страховыми компаниями, взыскание денег</div>
</div>
</li>
<li>
<div class="right-info">
<h4><a href="https://www.zonazakona.ru/forum/forum/47-gibdd/">ГИБДД</a></h4>
<div class="annotation">Споры с ГИБДД, лишение прав, оставление места ДТП, провокация взятки</div>
</div>
</li>
<li>
<div class="right-info">
<h4><a href="https://www.zonazakona.ru/forum/forum/24-obshhiy-forum-dlya-avtovladeltsev/">Общий форум для автовладельцев</a></h4>
<div class="annotation">Нарушение ПДД, лишение прав, проблемы с ГАИ, экспертиза</div>
</div>
</li>
</ul>
</li>
</ul>
<!--<div style="text-align: center"></div>-->
</div>
</div>
<div style="clear: both;"></div> <!-- \Lawyer Rating -->
<div style="clear: both;"></div>
</div>
</div>
<div id="guarantor"></div>
</div>
<!-- Footer -->
<footer>
<div class="wrapper">
<div class="copyright">
            &amp;copy 2007 - 2021 - «зонаЗакона.ру». <span>Все права защищены.</span>
<div class="units2" style=""><a href="/pages/polzovatelskoe-soglashenie/" style="color: #888;
padding-right: 10px;
text-decoration: underline;
font-size: 10px;
border: none;
text-transform: none;">Пользовательское соглашение</a></div>
</div>
<a href="http://www.tigla.ru" style="color: #333333;"><div class="made-in icon-tigla">Сделано в</div></a>
<div id="footer-counter">
<noindex>
<!--LiveInternet counter--><script type="text/javascript"><!--
			document.write("<a href='//www.liveinternet.ru/click' "+
			"target=_blank><img src='//counter.yadro.ru/hit?t18.5;r"+
			escape(document.referrer)+((typeof(screen)=="undefined")?"":
			";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
			screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
			";"+Math.random()+
			"' alt='' title='LiveInternet: показано число просмотров за 24"+
			" часа, посетителей за 24 часа и за сегодня' "+
			"border='0' width='88' height='31'><\/a>")
			//--></script><!--/LiveInternet-->
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
			var _tmr = _tmr || [];
			_tmr.push({id: "943027", type: "pageView", start: (new Date()).getTime()});
			(function (d, w) {
			   var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true;
			   ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
			   var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
			   if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
			})(document, window);
			</script><noscript><div style="position:absolute;left:-10000px;">
<img alt="Рейтинг@Mail.ru" height="1" src="//top-fwz1.mail.ru/counter?id=943027;js=na" style="border:0;" width="1"/>
</div></noscript>
<!-- //Rating@Mail.ru counter -->
<!-- Rating@Mail.ru logo -->
<a href="http://top.mail.ru/jump?from=943027">
<img alt="Рейтинг@Mail.ru" height="31" src="//top-fwz1.mail.ru/counter?id=943027;t=487;l=1" style="border:0;" width="88"/></a>
<!-- //Rating@Mail.ru logo -->
<a href="http://www.yandex.ru/cy?base=0&amp;host=www.zonazakona.ru">
<img alt="Яндекс цитирования" border="0" height="31" src="//www.yandex.ru/cycounter?www.zonazakona.ru" width="88"/></a>
</noindex>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
                (function (d, w, c) {
                    (w[c] = w[c] || []).push(function() {
                        try {
                            w.yaCounter26287896 = new Ya.Metrika({
                                id:26287896,
                                clickmap:true,
                                trackLinks:true,
                                accurateTrackBounce:true,
                                webvisor:true,
                                trackHash:true
                            });
                        } catch(e) { }
                    });

                    var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () { n.parentNode.insertBefore(s, n); };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = "https://mc.yandex.ru/metrika/watch.js";

                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else { f(); }
                })(document, window, "yandex_metrika_callbacks");
            </script>
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/26287896" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=26287896&amp;from=informer" rel="nofollow" target="_blank"><img alt="Яндекс.Метрика" src="https://informer.yandex.ru/informer/26287896/3_0_FFFFFFFF_FFFFFFFF_0_pageviews" style="width:88px; height:31px; border:0;" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"/></a>
<!-- /Yandex.Metrika informer -->
</div>
<div class="main-menu">
<ul> <li><a href="/">Главная</a></li>
<li class="list-devider"></li>
<li><a href="https://www.zonazakona.ru/forum">Форум</a></li>
<li class="list-devider"></li>
<li><a href="/documents/">Документы</a></li>
<li class="list-devider"></li>
<li><a href="/vsezakony/">Законодательство</a></li>
<li class="list-devider"></li>
</ul>
</div>
<div class="units">
<a href="/subscribe/">Рассылки</a>
<a href="/dogovora/index.php">Бланки договоров</a>
<a href="/zakon/index.php">Законы и кодексы РФ</a>
<a href="/articles/index.php">Статьи</a>
</div>
<div class="units">
<a href="http://www.gvka.ru">Помощь призывникам</a>
</div>
<script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-30550477-7', 'auto');
            ga('send', 'pageview');

        </script>
</div>
</footer><!-- \Footer -->
<div aria-hidden="true" class="modal hide fade" id="login-modal">
<div class="modal-header">
<a aria-hidden="true" class="close icon-close" data-dismiss="modal"> </a>
<h4>Авторизация в каталоге</h4>
</div>
<div class="modal-body">
<div class="form form-pad" style="text-align: center;">
<form action="/login" id="login-form-modal" method="post">
<input name="ZONA_TOKEN" type="hidden" value="NzVfMjlvYU15U24zYzFpSUpwaVFIeWl0TWNxdFBHR2KrwEveuxHPfpRNh2n3I1VIRYmx8X63VcvegpDOOmlUpQ=="/> <div class="row-fluid span7 ">
<label for="LoginForm_email">Электронная почта/логин</label> <input class="span7" id="LoginForm_email" name="LoginForm[email]" type="text"/> <div class="errorMessageWrapper" id="">Ошибка заполнения<br/><span class="black"><div class="errorMessage" id="LoginForm_email_em_" style="display:none"></div></span></div>
</div>
<div class="row-fluid span7 ">
<label for="LoginForm_password">Пароль</label> <input class="span7" id="LoginForm_password" name="LoginForm[password]" type="password"/> <div class="errorMessageWrapper" id="">Ошибка заполнения<br/><span class="black"><div class="errorMessage" id="LoginForm_password_em_" style="display:none"></div></span></div>
</div>
<div style="clear: both;"></div>
<div class="row-fluid">
<p class="hint">
<a href="/registration/">Регистрация</a>                                                                        | <a href="/recovery/">Восстановление пароля</a> </p>
</div>
<div class="row-fluid">
<label> </label>
<input name="yt0" style="opacity:0; position:absolute; " type="submit" value="Восстановить пароль"/> <a class="btn" id="submit-login-modal-btn"><span class="icon-btn-auth"><i></i></span>Войти</a> </div>
</form> <div style="clear: both;"></div>
</div>
</div>
</div>
<script type="text/javascript">
/*<![CDATA[*/
jQuery(function($) {

    reloadStyler();
    $('.dropdown-toggle').dropdown();
    $('#notice-tab').tab('show');
    
jQuery('#login-form-modal').yiiactiveform({'validateOnChange':true,'validateOnType':true,'attributes':[{'id':'LoginForm_email','inputID':'LoginForm_email','errorID':'LoginForm_email_em_','model':'LoginForm','name':'email','enableAjaxValidation':false,'clientValidation':function(value, messages, attribute) {

if(jQuery.trim(value)=='') {
	messages.push("\u041d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e \u0437\u0430\u043f\u043e\u043b\u043d\u0438\u0442\u044c \u043f\u043e\u043b\u0435 \u00ab\u042d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u0430\u044f \u043f\u043e\u0447\u0442\u0430\/\u043b\u043e\u0433\u0438\u043d\u00bb.");
}

}},{'id':'LoginForm_password','inputID':'LoginForm_password','errorID':'LoginForm_password_em_','model':'LoginForm','name':'password','enableAjaxValidation':false,'clientValidation':function(value, messages, attribute) {

if(jQuery.trim(value)=='') {
	messages.push("\u041d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e \u0437\u0430\u043f\u043e\u043b\u043d\u0438\u0442\u044c \u043f\u043e\u043b\u0435 \u00ab\u041f\u0430\u0440\u043e\u043b\u044c\u00bb.");
}

}}],'errorCss':'error'});
        $('#submit-login-modal-btn').on('click', function(){
            if ($('#login-form-modal .error').length == 0 && $('#login-form-modal #LoginForm_email').val().length > 0  && $('#login-form-modal #LoginForm_password').val().length > 0)
            {
                jLoadingShow();
                $('#login-form-modal').submit();
            }
        });
});
jQuery(window).on('load',function() {
    $.ajax({
        type : "POST",
        url : "/banner/banner/getbanner",
        data : {'place' : 'header'},
        dataType : "json"
    }).done(function(req){
        if (req.result)
        {
            $(req.selector).html(req.text).slideDown(600);
        }
    });
    $.ajax({
        type : "POST",
        url : "/banner/banner/getbanner",
        data : {'place' : 'header-right'},
        dataType : "json"
    }).done(function(req){
        if (req.result)
        {
            $(req.selector).html(req.text).slideDown(600);
        }
    });
});
/*]]>*/
</script>
</body>
</html>