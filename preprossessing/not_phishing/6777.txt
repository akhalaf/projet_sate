<!DOCTYPE HTML>
<html lang="zh-CN">
<head>
<meta charset="utf-8"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.123admin.com/xmlrpc.php" rel="pingback"/>
<title>123admin – 系统管理员日志</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.123admin.com/feed/" rel="alternate" title="123admin » Feed" type="application/rss+xml"/>
<link href="https://www.123admin.com/comments/feed/" rel="alternate" title="123admin » 评论Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.123admin.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.123admin.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.123admin.com/wp-content/themes/everbox/style.css?ver=5.6" id="everbox-style-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://www.123admin.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://www.123admin.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="velocity-js-js" src="https://www.123admin.com/wp-content/themes/everbox/js/velocity.js" type="text/javascript"></script>
<script id="velocity-ui-js-js" src="https://www.123admin.com/wp-content/themes/everbox/js/velocity.ui.js" type="text/javascript"></script>
<script id="fastclick-js-js" src="https://www.123admin.com/wp-content/themes/everbox/js/fastclick.js" type="text/javascript"></script>
<link href="https://www.123admin.com/wp-json/" rel="https://api.w.org/"/><link href="https://www.123admin.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.123admin.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<!--[if lt IE 9]>
    <script src="https://www.123admin.com/wp-content/themes/everbox/js/html5shiv-printshiv.js"></script>
  	<![endif]-->
<!-- Set the viewport. -->
<meta content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/>
<!-- Allow web app to be run in full-screen mode. -->
<meta content="yes" name="apple-mobile-web-app-capable"/>
<!-- Make the app title different than the page title. -->
<meta content="123admin" name="apple-mobile-web-app-title"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<!-- Configure the status bar. -->
<meta content="black-translucent" name="apple-mobile-web-app-status-bar-style"/>
<!-- Disable automatic phone number detection. -->
<meta content="telephone=no" name="format-detection"/>
<!-- ICONS -->
<script>var _hmt = _hmt || [];(function() {var hm = document.createElement("script");hm.src = "https://hm.baidu.com/hm.js?399b806341d76713dc94d59b77008777";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(hm, s);})();</script>
</head>
<body class="home blog">
<div class="hfeed site" id="page">
<header class="site-header" id="masthead" role="banner">
<div class="container-fluid">
<div class="group">
<div class="left-column">
<div class="site-heading">
<h1 class="site-title">
<a href="https://www.123admin.com/" rel="home">123admin</a>
</h1>
</div>
<!-- END .site-heading -->
<nav class="main-navigation" id="site-navigation" role="navigation">
<div class="nav-toggle"><i class="icon-bars"></i></div>
<div class="main-menu-container">
<ul class="main-nav" id="menu-%e8%8f%9c%e5%8d%95"><li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-7" id="menu-item-7"><a href="https://www.123admin.com/category/general/">系统管理</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-4" id="menu-item-4"><a href="https://www.123admin.com/category/web-server/">应用服务</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5" id="menu-item-5"><a href="https://www.123admin.com/category/languages/">开发语言</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6" id="menu-item-6"><a href="https://www.123admin.com/category/database/">数据库</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8" id="menu-item-8"><a href="https://www.123admin.com/category/networking/">网络</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10" id="menu-item-10"><a href="https://www.123admin.com/about/">关于</a></li>
</ul> </div>
</nav>
<!-- END .main-nagivation -->
</div>
<!-- END .left-column -->
<div class="right-column">
<div class="mobile-search-toggle"><i class="icon-search"></i></div>
<div class="site-search-area">
<form action="https://www.123admin.com/" class="searchform" id="searchform" method="get" role="search">
<input id="s" name="s" type="text" value=""/>
<button id="searchsubmit" type="submit"><i class="icon-search"></i></button>
<button id="search-toggle" type="button"></button>
</form>
</div>
<!-- END .site-search-area -->
</div>
<!-- END .right-column -->
</div>
<!-- END .group -->
</div>
<!-- END .container-fluid -->
<div class="mobile-menu-container">
<ul class="mobile-menu" id="menu-%e8%8f%9c%e5%8d%95-1"><li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-7"><a href="https://www.123admin.com/category/general/">系统管理</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-4"><a href="https://www.123admin.com/category/web-server/">应用服务</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5"><a href="https://www.123admin.com/category/languages/">开发语言</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6"><a href="https://www.123admin.com/category/database/">数据库</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8"><a href="https://www.123admin.com/category/networking/">网络</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10"><a href="https://www.123admin.com/about/">关于</a></li>
</ul> </div>
<!-- END .mobile-menu-container -->
<div class="mobile-search-container">
<form action="https://www.123admin.com/" class="searchform" id="searchform" method="get" role="search">
<input id="s" name="s" placeholder="搜索..." type="search" value=""/>
</form>
</div>
<!-- END .mobile-search-container -->
</header>
<!-- END .site-header -->
<div class="container-fluid">
<div class="site-content group" id="content">
<div class="left-column" id="primary">
<main class="site-main" id="main" role="main">
<article class="post-item post-1095 post type-post status-publish format-standard hentry category-general tag-firewalld tag-ipset" id="post-1095">
<div class="left">
<div class="post-meta category-links">
<a href="https://www.123admin.com/category/general/" title="查看 系统管理 的所有文章">系统管理</a> </div>
<!-- END .post-meta -->
<h2 class="post-title">
<a href="https://www.123admin.com/how-to-configure-firewalld-on-centos8/" rel="bookmark" title="CentOS8 firewalld基本配置方法，集合和ipset配置">CentOS8 firewalld基本配置方法，集合和ipset配置</a> </h2>
<div class="post-meta">
<span class="vcard author">
<a class="vcard author" href="https://www.123admin.com/author/admin/">admin</a></span>
<span class="divider">•</span>
<time class="entry-date published" datetime="2020-12-25T11:21:51+08:00">12/25/2020</time>
</div>
<!-- END .post-meta -->
<div class="post-excerpt">
<p>说明：此规范分为两部分：一是，常规配置方法；二是集合配置方法 常规的配置方法是指对于端口，服务及源地址的限制方法 集合配置方法是针对于源地址和端口组合限制，但源地址是一个IP地址集合，此集合中可以任意添加IP地址及网段 一、常规配置方法： 1、端口限制 1）放开UDP 161/162端口 firewall-cmd –permanent –zone=public –add-port=161/udp firewall-cmd –permanent &amp;#…</p>
<a class="read-more" href="https://www.123admin.com/how-to-configure-firewalld-on-centos8/">阅读全文 »</a> </div>
<!-- END .post-excerpt -->
</div>
<!-- END .left -->
</article>
<!-- END .post-item -->
<article class="post-item post-1090 post type-post status-publish format-standard hentry category-general tag-gitlab" id="post-1090">
<div class="left">
<div class="post-meta category-links">
<a href="https://www.123admin.com/category/general/" title="查看 系统管理 的所有文章">系统管理</a> </div>
<!-- END .post-meta -->
<h2 class="post-title">
<a href="https://www.123admin.com/how-to-use-ssh-clone-on-gitlab/" rel="bookmark" title="GitLab 开启 SSH 方式克隆">GitLab 开启 SSH 方式克隆</a> </h2>
<div class="post-meta">
<span class="vcard author">
<a class="vcard author" href="https://www.123admin.com/author/admin/">admin</a></span>
<span class="divider">•</span>
<time class="entry-date published" datetime="2020-12-12T16:31:10+08:00">12/12/2020</time>
</div>
<!-- END .post-meta -->
<div class="post-excerpt">
<p>假设SSH端口号将22改为2222 1 生成私钥 # ssh-keygen -t rsa -C “<a class="__cf_email__" data-cfemail="f2959b86b2969d9f939b9cdc919d9f" href="/cdn-cgi/l/email-protection">[email protected]</a>” 密钥默认生成在/root/.ssh目录下 # cd /root/.ssh # ls id_rsa id_rsa.pub 2 配置gitlab 用户-&gt;设置-&gt;SSH密钥，复制id_rsa.pub里面内容到密钥中 3.客户端ssh克隆 # git clone ssh://<a class="__cf_email__" data-cfemail="c5a2acb185a1aaa8a4acabeba6aaa8" href="/cdn-cgi/l/email-protection">[email protected]</a>:2222/root/test.gi…</p>
<a class="read-more" href="https://www.123admin.com/how-to-use-ssh-clone-on-gitlab/">阅读全文 »</a> </div>
<!-- END .post-excerpt -->
</div>
<!-- END .left -->
</article>
<!-- END .post-item -->
<article class="post-item post-1086 post type-post status-publish format-standard hentry category-general tag-gitlab" id="post-1086">
<div class="left">
<div class="post-meta category-links">
<a href="https://www.123admin.com/category/general/" title="查看 系统管理 的所有文章">系统管理</a> </div>
<!-- END .post-meta -->
<h2 class="post-title">
<a href="https://www.123admin.com/how-to-enable-https-on-gitlab/" rel="bookmark" title="GitLab 启用 https">GitLab 启用 https</a> </h2>
<div class="post-meta">
<span class="vcard author">
<a class="vcard author" href="https://www.123admin.com/author/admin/">admin</a></span>
<span class="divider">•</span>
<time class="entry-date published" datetime="2020-12-12T14:22:59+08:00">12/12/2020</time>
</div>
<!-- END .post-meta -->
<div class="post-excerpt">
<p>启用HTTPS 警告 Nginx配置会告诉浏览器和客户端，只需在未来24个月通过安全连接与您的GitLab实例进行通信。通过启用HTTPS，您需要至少在24个月内为您的实例提供安全连接。 默认情况下，omnibus-gitlab不使用HTTPS。如果要为gitlab.example.com启用HTTPS，请将以下语句添加到/etc/gitlab/gitlab.rb： # note the ‘https’ below external_url “https://…</p>
<a class="read-more" href="https://www.123admin.com/how-to-enable-https-on-gitlab/">阅读全文 »</a> </div>
<!-- END .post-excerpt -->
</div>
<!-- END .left -->
</article>
<!-- END .post-item -->
<article class="post-item post-1068 post type-post status-publish format-standard hentry category-general tag-zabbix" id="post-1068">
<div class="left">
<div class="post-meta category-links">
<a href="https://www.123admin.com/category/general/" title="查看 系统管理 的所有文章">系统管理</a> </div>
<!-- END .post-meta -->
<h2 class="post-title">
<a href="https://www.123admin.com/how-to-install-zabbix-5-on-centos/" rel="bookmark" title="CentOS 7 安装 Zabbix 5.0">CentOS 7 安装 Zabbix 5.0</a> </h2>
<div class="post-meta">
<span class="vcard author">
<a class="vcard author" href="https://www.123admin.com/author/admin/">admin</a></span>
<span class="divider">•</span>
<time class="entry-date published" datetime="2020-11-26T16:14:37+08:00">11/26/2020</time>
</div>
<!-- END .post-meta -->
<div class="post-excerpt">
<p>a. 安装Zabbix源 # rpm -Uvh https://repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/zabbix-release-5.0-1.el7.noarch.rpm # yum clean all 修改zabbix官方源，加快下载 打开/etc/yum.repos.d/zabbix.repo，把所有的“https://repo.zabbix.com” 改成 “https://mirrors.aliyun.com/zabbix/” （除了zabb…</p>
<a class="read-more" href="https://www.123admin.com/how-to-install-zabbix-5-on-centos/">阅读全文 »</a> </div>
<!-- END .post-excerpt -->
</div>
<!-- END .left -->
</article>
<!-- END .post-item -->
<article class="post-item post-1051 post type-post status-publish format-standard hentry category-general tag-docker" id="post-1051">
<div class="left">
<div class="post-meta category-links">
<a href="https://www.123admin.com/category/general/" title="查看 系统管理 的所有文章">系统管理</a> </div>
<!-- END .post-meta -->
<h2 class="post-title">
<a href="https://www.123admin.com/how-to-install-docker-on-centos/" rel="bookmark" title="CentOS 7 安装Docker">CentOS 7 安装Docker</a> </h2>
<div class="post-meta">
<span class="vcard author">
<a class="vcard author" href="https://www.123admin.com/author/admin/">admin</a></span>
<span class="divider">•</span>
<time class="entry-date published" datetime="2020-09-28T10:28:24+08:00">09/28/2020</time>
</div>
<!-- END .post-meta -->
<div class="post-excerpt">
<p>系统要求 Docker CE 支持 64 位版本 CentOS 7，并且要求内核版本不低于 3.10。 CentOS 7 满足最低内核的要求，但由于内核版本比较低，部分功能（如 overlay2 存储层驱动）无法使用，并且部分功能可能不太稳定。 卸载旧版本 旧版本的 Docker 称为 docker 或者 docker-engine，使用以下命令卸载旧版本： # yum remove docker \ docker-client \ docker-client-latest \ docker-c…</p>
<a class="read-more" href="https://www.123admin.com/how-to-install-docker-on-centos/">阅读全文 »</a> </div>
<!-- END .post-excerpt -->
</div>
<!-- END .left -->
</article>
<!-- END .post-item -->
<article class="post-item post-1049 post type-post status-publish format-standard hentry category-web-server tag-git tag-gitlab" id="post-1049">
<div class="left">
<div class="post-meta category-links">
<a href="https://www.123admin.com/category/web-server/" title="查看 应用服务 的所有文章">应用服务</a> </div>
<!-- END .post-meta -->
<h2 class="post-title">
<a href="https://www.123admin.com/gitlab-configure/" rel="bookmark" title="GitLab 常用配置">GitLab 常用配置</a> </h2>
<div class="post-meta">
<span class="vcard author">
<a class="vcard author" href="https://www.123admin.com/author/admin/">admin</a></span>
<span class="divider">•</span>
<time class="entry-date published" datetime="2020-09-21T20:37:11+08:00">09/21/2020</time>
</div>
<!-- END .post-meta -->
<div class="post-excerpt">
<p>1、邮件配置 配置邮箱可以让GitLab在发生相应事件的时候进行邮件通知比如：找回密码、添加邮箱等 2、禁用创建组权限 GitLab默认所有的注册用户都可以创建组。但对于团队来说，通常只会给Leader相关权限。虽然可以在用户管理界面取消权限，但毕竟不方便。我们可以通过配置GitLab默认禁用创建组权限。 3、gitlab-ctl常用命令介绍 命令 说明 check-config 检查在gitlab中是否有任何配置。在指定版本中删除的rb deploy-page 安装部署页面 diff-conf…</p>
<a class="read-more" href="https://www.123admin.com/gitlab-configure/">阅读全文 »</a> </div>
<!-- END .post-excerpt -->
</div>
<!-- END .left -->
</article>
<!-- END .post-item -->
<article class="post-item post-1046 post type-post status-publish format-standard hentry category-web-server tag-git tag-gitlab" id="post-1046">
<div class="left">
<div class="post-meta category-links">
<a href="https://www.123admin.com/category/web-server/" title="查看 应用服务 的所有文章">应用服务</a> </div>
<!-- END .post-meta -->
<h2 class="post-title">
<a href="https://www.123admin.com/how-to-install-gitlab-on-centos-7/" rel="bookmark" title="CentOS 7 安装 GitLab">CentOS 7 安装 GitLab</a> </h2>
<div class="post-meta">
<span class="vcard author">
<a class="vcard author" href="https://www.123admin.com/author/admin/">admin</a></span>
<span class="divider">•</span>
<time class="entry-date published" datetime="2020-09-21T20:30:26+08:00">09/21/2020</time>
</div>
<!-- END .post-meta -->
<div class="post-excerpt">
<p>GitLab介绍 GitLab一个开源的git仓库管理平台，方便团队协作开发、管理。在GitLab上可以实现完整的CI（持续集成）、CD（持续发布）流程。而且还提供了免费使用的Plan，以及免费的可以独立部署的社区版本(https://gitlab.com/gitlab-org/gitlab-ce )。 官网：https://about.gitlab.com/ 1、安准基础依赖 2、安装Postfix Postfix是一个邮件服务器，GitLab发送邮件需要用到 3、开放ssh以及ht…</p>
<a class="read-more" href="https://www.123admin.com/how-to-install-gitlab-on-centos-7/">阅读全文 »</a> </div>
<!-- END .post-excerpt -->
</div>
<!-- END .left -->
</article>
<!-- END .post-item -->
<article class="post-item post-1040 post type-post status-publish format-standard hentry category-web-server tag-zabbix" id="post-1040">
<div class="left">
<div class="post-meta category-links">
<a href="https://www.123admin.com/category/web-server/" title="查看 应用服务 的所有文章">应用服务</a> </div>
<!-- END .post-meta -->
<h2 class="post-title">
<a href="https://www.123admin.com/zabbix-5-0-%e4%b8%ad%e6%96%87%e4%b9%b1%e7%a0%81%e8%a7%a3%e5%86%b3/" rel="bookmark" title="Zabbix 5.0 中文乱码解决">Zabbix 5.0 中文乱码解决</a> </h2>
<div class="post-meta">
<span class="vcard author">
<a class="vcard author" href="https://www.123admin.com/author/admin/">admin</a></span>
<span class="divider">•</span>
<time class="entry-date published" datetime="2020-08-05T11:30:27+08:00">08/05/2020</time>
</div>
<!-- END .post-meta -->
<div class="post-excerpt">
<p>查看字体文件夹 # vim /usr/share/zabbix/include/defines.inc.php define(‘ZBX_FONTPATH’,                          realpath(…</p>
<a class="read-more" href="https://www.123admin.com/zabbix-5-0-%e4%b8%ad%e6%96%87%e4%b9%b1%e7%a0%81%e8%a7%a3%e5%86%b3/">阅读全文 »</a> </div>
<!-- END .post-excerpt -->
</div>
<!-- END .left -->
</article>
<!-- END .post-item -->
<article class="post-item post-1034 post type-post status-publish format-standard hentry category-general tag-filebeat tag-logstash" id="post-1034">
<div class="left">
<div class="post-meta category-links">
<a href="https://www.123admin.com/category/general/" title="查看 系统管理 的所有文章">系统管理</a> </div>
<!-- END .post-meta -->
<h2 class="post-title">
<a href="https://www.123admin.com/filebeat-ssl-logstash/" rel="bookmark" title="filebeat ssl 加密传输日志到 logstash 配置">filebeat ssl 加密传输日志到 logstash 配置</a> </h2>
<div class="post-meta">
<span class="vcard author">
<a class="vcard author" href="https://www.123admin.com/author/admin/">admin</a></span>
<span class="divider">•</span>
<time class="entry-date published" datetime="2020-07-23T16:29:12+08:00">07/23/2020</time>
</div>
<!-- END .post-meta -->
<div class="post-excerpt">
<p>背景 如果服务器位于世界各地，但又需要通过filebeat传输日志到logstash分析，这时候为了保证数据的安全，在传输的时候使用加密是基本的要求。 一、创建自签名证书 生成ca密钥# openssl genrsa 2048 &gt; ca.key使用ca私钥建立ca证书# openssl req -new -x509 -nodes -days 3650 -key ca.key -out ca.crt生成服务器csr证书请求文件# openssl req -newkey rsa:2048 -d…</p>
<a class="read-more" href="https://www.123admin.com/filebeat-ssl-logstash/">阅读全文 »</a> </div>
<!-- END .post-excerpt -->
</div>
<!-- END .left -->
</article>
<!-- END .post-item -->
<article class="post-item post-1024 post type-post status-publish format-standard hentry category-web-server" id="post-1024">
<div class="left">
<div class="post-meta category-links">
<a href="https://www.123admin.com/category/web-server/" title="查看 应用服务 的所有文章">应用服务</a> </div>
<!-- END .post-meta -->
<h2 class="post-title">
<a href="https://www.123admin.com/cacti-%e6%8a%a5%e9%94%99-csrf-timeout-occurred-due-to-inactivity-page-refreshed/" rel="bookmark" title="Cacti 报错 CSRF Timeout occurred due to inactivity, page refreshed.">Cacti 报错 CSRF Timeout occurred due to inactivity, page refreshed.</a> </h2>
<div class="post-meta">
<span class="vcard author">
<a class="vcard author" href="https://www.123admin.com/author/admin/">admin</a></span>
<span class="divider">•</span>
<time class="entry-date published" datetime="2020-06-16T20:51:26+08:00">06/16/2020</time>
</div>
<!-- END .post-meta -->
<div class="post-excerpt">
<p>编辑文件csrf-magic.php # vim /usr/local/cacti/include/vendor/csrf/csrf-magic.php 添加如下： $GLOBALS[‘csrf’][‘defer’] = false;$GLOBALS[‘csrf’][‘defer’] = true;</p>
<a class="read-more" href="https://www.123admin.com/cacti-%e6%8a%a5%e9%94%99-csrf-timeout-occurred-due-to-inactivity-page-refreshed/">阅读全文 »</a> </div>
<!-- END .post-excerpt -->
</div>
<!-- END .left -->
</article>
<!-- END .post-item -->
<div class="pagination-wrap">
<nav aria-label="文章" class="navigation pagination" role="navigation">
<h2 class="screen-reader-text">文章导航</h2>
<div class="nav-links"><span aria-current="page" class="page-numbers current">1</span>
<a class="page-numbers" href="https://www.123admin.com/page/2/">2</a>
<span class="page-numbers dots">…</span>
<a class="page-numbers" href="https://www.123admin.com/page/32/">32</a>
<a class="next page-numbers" href="https://www.123admin.com/page/2/">下一个</a></div>
</nav></div> </main>
<!-- END .site-main -->
</div>
<!-- END #primary -->
<div class="right-column" id="secondary">
<div class="secondary-toggle"><i class="icon-angle-left"></i></div>
<div class="sidebar" role="complementary">
<aside class="widget widget_stream-list" id="everbox_popular-2"> <div class="widget-content">
<ul id="filter-toggle-buttons">
<li class="filter-button"><a href="#popular_posts-2">热门文章</a></li>
<li class="filter-button"><a href="#latest_posts-2">最新更新</a></li>
</ul>
<!-- END #filter-toggle-buttons -->
<div class="posts-list tab_content" id="popular_posts-2">
<ul><li><div class="right-body"><a class="title" href="https://www.123admin.com/how-to-configure-firewalld-on-centos8/" role="bookmark" title="CentOS8 firewalld基本配置方法，集合和ipset配置">CentOS8 firewalld基本配置方法，集合和ipset配置</a><time class="entry-date published" datetime="2020-12-25T11:21:51+08:00">3周前</time><span class="divider">•</span><span class="comments-count">0 条评论<span></span></span></div></li></ul> </div>
<!-- END #popular_posts -->
<div class="posts-list tab_content" id="latest_posts-2">
<ul><li><div class="right-body"><a class="title" href="https://www.123admin.com/how-to-configure-firewalld-on-centos8/" role="bookmark" title="CentOS8 firewalld基本配置方法，集合和ipset配置">CentOS8 firewalld基本配置方法，集合和ipset配置</a><time class="entry-date published" datetime="2020-12-25T11:21:51+08:00">3周前</time></div></li><li><div class="right-body"><a class="title" href="https://www.123admin.com/how-to-use-ssh-clone-on-gitlab/" role="bookmark" title="GitLab 开启 SSH 方式克隆">GitLab 开启 SSH 方式克隆</a><time class="entry-date published" datetime="2020-12-12T16:31:10+08:00">1月前</time></div></li><li><div class="right-body"><a class="title" href="https://www.123admin.com/how-to-enable-https-on-gitlab/" role="bookmark" title="GitLab 启用 https">GitLab 启用 https</a><time class="entry-date published" datetime="2020-12-12T14:22:59+08:00">1月前</time></div></li><li><div class="right-body"><a class="title" href="https://www.123admin.com/how-to-install-zabbix-5-on-centos/" role="bookmark" title="CentOS 7 安装 Zabbix 5.0">CentOS 7 安装 Zabbix 5.0</a><time class="entry-date published" datetime="2020-11-26T16:14:37+08:00">2月前</time></div></li><li><div class="right-body"><a class="title" href="https://www.123admin.com/how-to-install-docker-on-centos/" role="bookmark" title="CentOS 7 安装Docker">CentOS 7 安装Docker</a><time class="entry-date published" datetime="2020-09-28T10:28:24+08:00">4月前</time></div></li><li><div class="right-body"><a class="title" href="https://www.123admin.com/gitlab-configure/" role="bookmark" title="GitLab 常用配置">GitLab 常用配置</a><time class="entry-date published" datetime="2020-09-21T20:37:11+08:00">4月前</time></div></li><li><div class="right-body"><a class="title" href="https://www.123admin.com/how-to-install-gitlab-on-centos-7/" role="bookmark" title="CentOS 7 安装 GitLab">CentOS 7 安装 GitLab</a><time class="entry-date published" datetime="2020-09-21T20:30:26+08:00">4月前</time></div></li><li><div class="right-body"><a class="title" href="https://www.123admin.com/zabbix-5-0-%e4%b8%ad%e6%96%87%e4%b9%b1%e7%a0%81%e8%a7%a3%e5%86%b3/" role="bookmark" title="Zabbix 5.0 中文乱码解决">Zabbix 5.0 中文乱码解决</a><time class="entry-date published" datetime="2020-08-05T11:30:27+08:00">5月前</time></div></li><li><div class="right-body"><a class="title" href="https://www.123admin.com/filebeat-ssl-logstash/" role="bookmark" title="filebeat ssl 加密传输日志到 logstash 配置">filebeat ssl 加密传输日志到 logstash 配置</a><time class="entry-date published" datetime="2020-07-23T16:29:12+08:00">6月前</time></div></li><li><div class="right-body"><a class="title" href="https://www.123admin.com/cacti-%e6%8a%a5%e9%94%99-csrf-timeout-occurred-due-to-inactivity-page-refreshed/" role="bookmark" title="Cacti 报错 CSRF Timeout occurred due to inactivity, page refreshed.">Cacti 报错 CSRF Timeout occurred due to inactivity, page refreshed.</a><time class="entry-date published" datetime="2020-06-16T20:51:26+08:00">7月前</time></div></li></ul> </div>
<!-- END #latest_posts -->
</div>
<!-- END .widget-content -->
</aside><aside class="widget widget_tag_cloud" id="tag_cloud-2"><div class="widget-head"><h3 class="widget-title">标签</h3></div><div class="tagcloud"><a aria-label="Apache (27个项目)" class="tag-cloud-link tag-link-8 tag-link-position-1" href="https://www.123admin.com/tag/apache/" style="font-size: 16.095238095238pt;">Apache</a>
<a aria-label="AWStats (5个项目)" class="tag-cloud-link tag-link-60 tag-link-position-2" href="https://www.123admin.com/tag/awstats/" style="font-size: 9.7142857142857pt;">AWStats</a>
<a aria-label="Cacti (3个项目)" class="tag-cloud-link tag-link-130 tag-link-position-3" href="https://www.123admin.com/tag/cacti/" style="font-size: 8pt;">Cacti</a>
<a aria-label="CentOS (117个项目)" class="tag-cloud-link tag-link-9 tag-link-position-4" href="https://www.123admin.com/tag/centos/" style="font-size: 22pt;">CentOS</a>
<a aria-label="Debian (3个项目)" class="tag-cloud-link tag-link-52 tag-link-position-5" href="https://www.123admin.com/tag/debian/" style="font-size: 8pt;">Debian</a>
<a aria-label="ELK (3个项目)" class="tag-cloud-link tag-link-134 tag-link-position-6" href="https://www.123admin.com/tag/elk/" style="font-size: 8pt;">ELK</a>
<a aria-label="FastDFS (4个项目)" class="tag-cloud-link tag-link-14 tag-link-position-7" href="https://www.123admin.com/tag/fastdfs/" style="font-size: 8.952380952381pt;">FastDFS</a>
<a aria-label="GitLab (4个项目)" class="tag-cloud-link tag-link-147 tag-link-position-8" href="https://www.123admin.com/tag/gitlab/" style="font-size: 8.952380952381pt;">GitLab</a>
<a aria-label="gzip (3个项目)" class="tag-cloud-link tag-link-74 tag-link-position-9" href="https://www.123admin.com/tag/gzip/" style="font-size: 8pt;">gzip</a>
<a aria-label="HTTPS (8个项目)" class="tag-cloud-link tag-link-56 tag-link-position-10" href="https://www.123admin.com/tag/https/" style="font-size: 11.333333333333pt;">HTTPS</a>
<a aria-label="IIS (8个项目)" class="tag-cloud-link tag-link-38 tag-link-position-11" href="https://www.123admin.com/tag/iis/" style="font-size: 11.333333333333pt;">IIS</a>
<a aria-label="iptables (4个项目)" class="tag-cloud-link tag-link-34 tag-link-position-12" href="https://www.123admin.com/tag/iptables/" style="font-size: 8.952380952381pt;">iptables</a>
<a aria-label="Keepalived (5个项目)" class="tag-cloud-link tag-link-22 tag-link-position-13" href="https://www.123admin.com/tag/keepalived/" style="font-size: 9.7142857142857pt;">Keepalived</a>
<a aria-label="Linux (12个项目)" class="tag-cloud-link tag-link-54 tag-link-position-14" href="https://www.123admin.com/tag/linux/" style="font-size: 12.857142857143pt;">Linux</a>
<a aria-label="Linux Basics (19个项目)" class="tag-cloud-link tag-link-28 tag-link-position-15" href="https://www.123admin.com/tag/linux-basics/" style="font-size: 14.666666666667pt;">Linux Basics</a>
<a aria-label="Logstash (5个项目)" class="tag-cloud-link tag-link-135 tag-link-position-16" href="https://www.123admin.com/tag/logstash/" style="font-size: 9.7142857142857pt;">Logstash</a>
<a aria-label="LVS (5个项目)" class="tag-cloud-link tag-link-35 tag-link-position-17" href="https://www.123admin.com/tag/lvs/" style="font-size: 9.7142857142857pt;">LVS</a>
<a aria-label="Memcache (3个项目)" class="tag-cloud-link tag-link-26 tag-link-position-18" href="https://www.123admin.com/tag/memcache/" style="font-size: 8pt;">Memcache</a>
<a aria-label="Memcached (3个项目)" class="tag-cloud-link tag-link-27 tag-link-position-19" href="https://www.123admin.com/tag/memcached/" style="font-size: 8pt;">Memcached</a>
<a aria-label="MySQL (57个项目)" class="tag-cloud-link tag-link-12 tag-link-position-20" href="https://www.123admin.com/tag/mysql/" style="font-size: 19.047619047619pt;">MySQL</a>
<a aria-label="mysqldump (4个项目)" class="tag-cloud-link tag-link-75 tag-link-position-21" href="https://www.123admin.com/tag/mysqldump/" style="font-size: 8.952380952381pt;">mysqldump</a>
<a aria-label="Nagios (7个项目)" class="tag-cloud-link tag-link-18 tag-link-position-22" href="https://www.123admin.com/tag/nagios/" style="font-size: 10.857142857143pt;">Nagios</a>
<a aria-label="Naxsi (4个项目)" class="tag-cloud-link tag-link-41 tag-link-position-23" href="https://www.123admin.com/tag/naxsi/" style="font-size: 8.952380952381pt;">Naxsi</a>
<a aria-label="Nginx (34个项目)" class="tag-cloud-link tag-link-10 tag-link-position-24" href="https://www.123admin.com/tag/nginx/" style="font-size: 16.952380952381pt;">Nginx</a>
<a aria-label="Oracle (5个项目)" class="tag-cloud-link tag-link-25 tag-link-position-25" href="https://www.123admin.com/tag/oracle/" style="font-size: 9.7142857142857pt;">Oracle</a>
<a aria-label="PHP (38个项目)" class="tag-cloud-link tag-link-11 tag-link-position-26" href="https://www.123admin.com/tag/php/" style="font-size: 17.428571428571pt;">PHP</a>
<a aria-label="PHP-FPM (12个项目)" class="tag-cloud-link tag-link-47 tag-link-position-27" href="https://www.123admin.com/tag/php-fpm/" style="font-size: 12.857142857143pt;">PHP-FPM</a>
<a aria-label="phpMyAdmin (5个项目)" class="tag-cloud-link tag-link-70 tag-link-position-28" href="https://www.123admin.com/tag/phpmyadmin/" style="font-size: 9.7142857142857pt;">phpMyAdmin</a>
<a aria-label="prefork (3个项目)" class="tag-cloud-link tag-link-89 tag-link-position-29" href="https://www.123admin.com/tag/prefork/" style="font-size: 8pt;">prefork</a>
<a aria-label="Pure-FTPd (4个项目)" class="tag-cloud-link tag-link-72 tag-link-position-30" href="https://www.123admin.com/tag/pure-ftpd/" style="font-size: 8.952380952381pt;">Pure-FTPd</a>
<a aria-label="Python (3个项目)" class="tag-cloud-link tag-link-20 tag-link-position-31" href="https://www.123admin.com/tag/python/" style="font-size: 8pt;">Python</a>
<a aria-label="Redis (3个项目)" class="tag-cloud-link tag-link-95 tag-link-position-32" href="https://www.123admin.com/tag/redis/" style="font-size: 8pt;">Redis</a>
<a aria-label="Redmine (3个项目)" class="tag-cloud-link tag-link-67 tag-link-position-33" href="https://www.123admin.com/tag/redmine/" style="font-size: 8pt;">Redmine</a>
<a aria-label="SQL Server (7个项目)" class="tag-cloud-link tag-link-73 tag-link-position-34" href="https://www.123admin.com/tag/sql-server/" style="font-size: 10.857142857143pt;">SQL Server</a>
<a aria-label="Squid (8个项目)" class="tag-cloud-link tag-link-19 tag-link-position-35" href="https://www.123admin.com/tag/squid/" style="font-size: 11.333333333333pt;">Squid</a>
<a aria-label="SSL (8个项目)" class="tag-cloud-link tag-link-57 tag-link-position-36" href="https://www.123admin.com/tag/ssl/" style="font-size: 11.333333333333pt;">SSL</a>
<a aria-label="Subversion (4个项目)" class="tag-cloud-link tag-link-29 tag-link-position-37" href="https://www.123admin.com/tag/subversion/" style="font-size: 8.952380952381pt;">Subversion</a>
<a aria-label="SVN (3个项目)" class="tag-cloud-link tag-link-66 tag-link-position-38" href="https://www.123admin.com/tag/svn/" style="font-size: 8pt;">SVN</a>
<a aria-label="Tomcat (10个项目)" class="tag-cloud-link tag-link-63 tag-link-position-39" href="https://www.123admin.com/tag/tomcat/" style="font-size: 12.190476190476pt;">Tomcat</a>
<a aria-label="Windows (16个项目)" class="tag-cloud-link tag-link-32 tag-link-position-40" href="https://www.123admin.com/tag/windows/" style="font-size: 14pt;">Windows</a>
<a aria-label="Windows Server (9个项目)" class="tag-cloud-link tag-link-99 tag-link-position-41" href="https://www.123admin.com/tag/windows-server/" style="font-size: 11.809523809524pt;">Windows Server</a>
<a aria-label="yum (6个项目)" class="tag-cloud-link tag-link-77 tag-link-position-42" href="https://www.123admin.com/tag/yum/" style="font-size: 10.380952380952pt;">yum</a>
<a aria-label="Zabbix (3个项目)" class="tag-cloud-link tag-link-45 tag-link-position-43" href="https://www.123admin.com/tag/zabbix/" style="font-size: 8pt;">Zabbix</a>
<a aria-label="Zend (5个项目)" class="tag-cloud-link tag-link-39 tag-link-position-44" href="https://www.123admin.com/tag/zend/" style="font-size: 9.7142857142857pt;">Zend</a>
<a aria-label="ZendOptimizer (3个项目)" class="tag-cloud-link tag-link-42 tag-link-position-45" href="https://www.123admin.com/tag/zendoptimizer/" style="font-size: 8pt;">ZendOptimizer</a></div>
</aside> <div class="site-footer">
<div class="footer-links"><ul class="menu" id="menu-%e9%a1%b5%e8%84%9a%e8%8f%9c%e5%8d%95"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1063" id="menu-item-1063"><a href="https://www.xiaxinyun.com">阿里云优惠</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1064" id="menu-item-1064"><a href="https://www.xiaxin.net">腾讯云优惠</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1065" id="menu-item-1065"><a href="https://www.xiaxincloud.com">华为云优惠</a></li>
</ul></div>
<div class="site-info">
			&amp;copy <a href="//www.123admin.com" target="_blank">123admin</a> Powered by WordPress.			</div>
<!-- END .site-info -->
</div>
<!-- END .site-footer -->
</div>
<!-- END .sidebar -->
</div>
<!-- END #secondary -->
</div>
<!-- END .site-content -->
</div>
<!-- END .container-fluid -->
</div>
<!-- END #page -->
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script id="site-js-js" src="https://www.123admin.com/wp-content/themes/everbox/js/site.js" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.123admin.com/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
</body>
</html>
<!-- Dynamic page generated in 0.086 seconds. -->
