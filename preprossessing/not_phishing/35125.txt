<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,IE=11,IE=10,IE=9,IE=8" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" name="viewport"/>
<title>未找到页面-楚盟网</title>
<script>
window._deel = {
    name: '楚盟网',
    url: 'https://www.5yun.org/wp-content/themes/d8',
    rss: '',
    ajaxpager: '',
    commenton: 0,
    roll: [0,0],
    tougaoContentmin: 200,
    tougaoContentmax: 5000}
</script>
<link href="https://www.5yun.org/wp-content/themes/d8/style.css?ver=5.3" id="style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.5yun.org/wp-includes/css/dist/block-library/style.min.css?ver=5.5.1" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.5yun.org/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.2.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.5yun.org/wp-content/plugins/easy-video-player/lib/plyr.css?ver=5.5.1" id="plyr-css-css" media="all" rel="stylesheet" type="text/css"/>
<style id="md-style-inline-css" type="text/css">
 code.kb-btn { display: inline-block; color: #666; font: bold 9pt arial; text-decoration: none; text-align: center; padding: 2px 5px; margin: 0 5px; background: #eff0f2; -moz-border-radius: 4px; border-radius: 4px; border-top: 1px solid #f5f5f5; -webkit-box-shadow: inset 0 0 20px #e8e8e8, 0 1px 0 #c3c3c3, 0 1px 0 #c9c9c9, 0 1px 2px #333; -moz-box-shadow: inset 0 0 20px #e8e8e8, 0 1px 0 #c3c3c3, 0 1px 0 #c9c9c9, 0 1px 2px #333; box-shadow: inset 0 0 20px #e8e8e8, 0 1px 0 #c3c3c3, 0 1px 0 #c9c9c9, 0 1px 2px #333; text-shadow: 0px 1px 0px #f5f5f5; } .copy-button { cursor: pointer; border: 0; font-size: 12px; text-transform: uppercase; font-weight: 500; padding: 3px 6px 3px 6px; background-color: rgba(255, 255, 255, 0.6); position: absolute; overflow: hidden; top: 5px; right: 5px; border-radius: 3px; } .copy-button:before { content: ""; display: inline-block; width: 16px; height: 16px; margin-right: 3px; background-size: contain; background-image: url("data:image/svg+xml,%3Csvg version=\'1.1\' xmlns=\'http://www.w3.org/2000/svg\' xmlns:xlink=\'http://www.w3.org/1999/xlink\' x=\'0px\' y=\'0px\' width=\'16px\' height=\'16px\' viewBox=\'888 888 16 16\' enable-background=\'new 888 888 16 16\' xml:space=\'preserve\'%3E %3Cpath fill=\'%23333333\' d=\'M903.143,891.429c0.238,0,0.44,0.083,0.607,0.25c0.167,0.167,0.25,0.369,0.25,0.607v10.857 c0,0.238-0.083,0.44-0.25,0.607s-0.369,0.25-0.607,0.25h-8.571c-0.238,0-0.44-0.083-0.607-0.25s-0.25-0.369-0.25-0.607v-2.571 h-4.857c-0.238,0-0.44-0.083-0.607-0.25s-0.25-0.369-0.25-0.607v-6c0-0.238,0.06-0.5,0.179-0.786s0.262-0.512,0.428-0.679 l3.643-3.643c0.167-0.167,0.393-0.309,0.679-0.428s0.547-0.179,0.786-0.179h3.714c0.238,0,0.44,0.083,0.607,0.25 c0.166,0.167,0.25,0.369,0.25,0.607v2.929c0.404-0.238,0.785-0.357,1.143-0.357H903.143z M898.286,893.331l-2.67,2.669h2.67V893.331 z M892.571,889.902l-2.669,2.669h2.669V889.902z M894.321,895.679l2.821-2.822v-3.714h-3.428v3.714c0,0.238-0.083,0.441-0.25,0.607 s-0.369,0.25-0.607,0.25h-3.714v5.714h4.571v-2.286c0-0.238,0.06-0.5,0.179-0.786C894.012,896.071,894.155,895.845,894.321,895.679z M902.857,902.857v-10.286h-3.429v3.714c0,0.238-0.083,0.441-0.25,0.607c-0.167,0.167-0.369,0.25-0.607,0.25h-3.714v5.715H902.857z\' /%3E %3C/svg%3E"); background-repeat: no-repeat; position: relative; top: 3px; } pre { position: relative; } pre:hover .copy-button { background-color: rgba(255, 255, 255, 0.9); } 
</style>
<link href="https://www.5yun.org/wp-content/plugins/wavesurfer-wp/css/wavesurfer-wp_font.css?ver=5.5.1" id="wavesurfer_font-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.5yun.org/wp-content/plugins/wavesurfer-wp/css/wavesurfer-wp_default.css?ver=5.5.1" id="wavesurfer_default-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.5yun.org/wp-content/plugins/wp-githuber-md/assets/vendor/emojify/css/emojify.min.css?ver=1.1.0" id="emojify-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-js" src="https://www.5yun.org/wp-content/themes/d8/js/jquery.js?ver=5.3" type="text/javascript"></script>
<script id="plyr-js-js" src="https://www.5yun.org/wp-content/plugins/easy-video-player/lib/plyr.min.js?ver=5.5.1" type="text/javascript"></script>
<link href="https://www.5yun.org/wp-json/" rel="https://api.w.org/"/><meta content="未找到页面" name="keywords"/>
<meta content="楚盟网'未找到页面'" name="description"/>
<style></style><!--[if lt IE 9]><script src="https://www.5yun.org/wp-content/themes/d8/js/html5.js"></script><![endif]-->
</head>
<body class="error404">
<div class="navbar-wrap">
<div class="navbar">
<div class="logo"><a href="https://www.5yun.org" title="楚盟网-记录学习分享的个人网站">楚盟网</a></div>
<ul class="nav">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-13942" id="menu-item-13942"><a href="https://www.5yun.org/">首页</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-11524" id="menu-item-11524"><a href="https://www.5yun.org/category/software">软件·服务器</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-20602" id="menu-item-20602"><a href="https://www.5yun.org/category/electrical-engineer">电子电气</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-20665" id="menu-item-20665"><a href="https://www.5yun.org/category/science-and-nature">科学与自然</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-20481" id="menu-item-20481"><a href="https://www.5yun.org/category/ae">AE</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-20966" id="menu-item-20966"><a href="https://www.5yun.org/category/ps">Photoshop</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-21160" id="menu-item-21160"><a href="https://www.5yun.org/category/wenzhai">文摘语录</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-21397" id="menu-item-21397"><a href="https://www.5yun.org/category/zuji">足迹</a></li>
</ul>
<div class="menu pull-right">
<form action="https://www.5yun.org/" class="dropdown search-form" method="get">
<input class="search-input" name="s" placeholder="输入关键字搜索" type="text" x-webkit-speech=""/><input class="btn btn-success search-submit" type="submit" value="搜索"/>
<ul class="dropdown-menu search-suggest"></ul>
</form>
<div class="btn-group pull-right">
</div>
</div>
</div>
</div>
<header class="header">
<div class="speedbar">
<div class="toptip"><strong class="text-success">最新消息：</strong>不要跟我说什么底层原理、框架内核！老夫敲代码就是一把梭！ 复制，粘贴，拿起键盘就是干！！</div>
</div>
</header>
<section class="container">
<div class="content-wrap">
<div style="text-align:center;padding:60px 0;font-size:16px;">
<h2 style="font-size:60px;margin-bottom:32px;">404 . Not Just An Error</h2>
		抱歉，沒有找到您需要的内容！！
	</div>
<h1 class="page-title">给您推荐以下内容：</h1>
<article class="excerpt excerpt-nothumbnail">
<header>
<a class="label label-important" href="https://www.5yun.org/category/software">软件·服务器<i class="label-arrow"></i></a> <h2><a href="https://www.5yun.org/21552.html" title="youtube-dl-server通过网页下载视频到服务器 - 楚盟网">youtube-dl-server通过网页下载视频到服务器</a></h2>
</header>
<p>
<span class="muted"><i class="icon-time icon12"></i> 1个月前 (11-30)</span> <span class="muted"><i class="icon-eye-open icon12"></i> 1072浏览</span> </p><p class="note">
		安装依赖环境（我用的python3） pip3 install bottle pip3 install --upgrade youtube-dl pip3 install --upgrade ffmpeg 启动命令： YDL_SERVER_PORT=...	</p>
</article>
<article class="excerpt excerpt-nothumbnail">
<header>
<a class="label label-important" href="https://www.5yun.org/category/electrical-engineer">电子电气<i class="label-arrow"></i></a> <h2><a href="https://www.5yun.org/21395.html" title="2.5平电缆700米长的电压降 - 楚盟网">2.5平电缆700米长的电压降</a></h2>
</header>
<p>
<span class="muted"><i class="icon-time icon12"></i> 7个月前 (06-19)</span> <span class="muted"><i class="icon-eye-open icon12"></i> 2984浏览</span> </p><p class="note">
		有人曾经买了700米2.5平从山脚拉到了山上用,最后发现只能勉强带个灯泡而且亮度也不够,由此可见电压降很大输出电流很小; 这个问题是电损造成的,并非电缆质量问题造成,所谓电压降是指电流在导体中流动的过程能量消耗损失引起电压降低电流变小; 有团队曾经做...	</p>
</article>
<article class="excerpt excerpt-nothumbnail">
<header>
<a class="label label-important" href="https://www.5yun.org/category/software">软件·服务器<i class="label-arrow"></i></a> <h2><a href="https://www.5yun.org/21384.html" title="使用Visual Studio Code正则提取magnet磁力链接 - 楚盟网">使用Visual Studio Code正则提取magnet磁力链接</a></h2>
</header>
<p>
<span class="muted"><i class="icon-time icon12"></i> 7个月前 (06-18)</span> <span class="muted"><i class="icon-eye-open icon12"></i> 2328浏览</span> </p><p class="note">
		在一段网页html代码中,包含了magnet磁力链接地址,在VScode中使用正则将磁力链接提取出来,正则如下: magnet.*?" 提取从magnet内容开头到"结尾的内容,.*?非贪婪匹配 问题:这个正则把"也包含...	</p>
</article>
<article class="excerpt excerpt-nothumbnail">
<header>
<a class="label label-important" href="https://www.5yun.org/category/electrical-engineer">电子电气<i class="label-arrow"></i></a> <h2><a href="https://www.5yun.org/21377.html" title="Panasonic FP-XO L60MR通讯线连接方法 - 楚盟网">Panasonic FP-XO L60MR通讯线连接方法</a></h2>
</header>
<p>
<span class="muted"><i class="icon-time icon12"></i> 7个月前 (06-17)</span> <span class="muted"><i class="icon-eye-open icon12"></i> 1937浏览</span> </p><p class="note">
		某设备中的PLC需要连接触摸控制屏,这里记录一下485信号线的连接位置,24v供电从专门的24v开关电源其中引用(图3); 485的信号线不能接反,区分正负极 转载请注明：楚盟网 » Panasonic FP-XO L60MR通讯线连接方...	</p>
</article>
<div class="pagination"><ul><li class="prev-page"></li><li class="next-page"></li></ul></div></div>
</section>
<!-- 百度统计开始 -->
<script>
    var _hmt = _hmt || [];
    (function() {
    var hm = document.createElement("script");
    hm.src = "https://hm.baidu.com/hm.js?bc04224ba3dc54ea282f6132790b9e3e";
    var s = document.getElementsByTagName("script")[0]; 
    s.parentNode.insertBefore(hm, s);
    })();
    </script>
<!-- 百度统计结束  -->
<footer class="footer">
<div class="footer-inner">
<div class="copyright pull-left">
                友情链接:
                <a href="http://codefun007.xyz" target="_blank">代码和段子手</a>
<a href="https://www.sxsay.com" target="_blank">静思雨夜</a>
<a href="https://www.wulintang.cn" target="_blank">伍林堂工作室</a>
<a href="https://www.dasu.net.cn" target="_blank">大苏网</a>
<a href="https://www.biaoza.club" target="_blank">小表砸源码</a>
<a href="http://likedge.top" target="_blank">极度空间</a>
<a href="https://www.ningcaichen.top" target="_blank">宁采晨's Blog</a>
<a href="http://www.beian.miit.gov.cn/">备案号:湘ICP备14003571号-1</a> 版权所有，保留一切权利！ © 2021 
        </div>
<div class="trackcode pull-right">
</div>
</div>
</footer>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.5yun.org\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://www.5yun.org/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.2.2" type="text/javascript"></script>
<script id="clipboard-js" src="https://www.5yun.org/wp-includes/js/clipboard.min.js?ver=5.5.1" type="text/javascript"></script>
<script id="emojify-js" src="https://www.5yun.org/wp-content/plugins/wp-githuber-md/assets/vendor/emojify/js/emojify.min.js?ver=1.1.0" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.5yun.org/wp-includes/js/wp-embed.min.js?ver=5.5.1" type="text/javascript"></script>
<script id="preference-link-target"> (function($) { $(function() { $(".post").find("a").each(function() { var link_href = $(this).attr("href"); if (link_href.indexOf("#") == -1) { $(this).attr("target", "_blank"); } }); }); })(jQuery); </script> <script id="module-highlight-js"> (function($) { $(function() { $("pre code").each(function(i, e) { var thisclass = $(this).attr("class"); if (typeof thisclass !== "undefined") { if ( thisclass.indexOf("katex") === -1 && thisclass.indexOf("mermaid") === -1 && thisclass.indexOf("seq") === -1 && thisclass.indexOf("flow") === -1 ) { if (typeof hljs !== "undefined") { $(this).closest("pre").addClass("hljs"); hljs.highlightBlock(e); } else { console.log("%c WP Githuber MD %c You have enabled highlight.js modules already, but you have to update this post to take effect, identifying which file should be loaded.\nGithuber MD does not load a whole-fat-packed file for every post.", "background: #222; color: #bada55", "color: #637338"); } } } }); }); })(jQuery); </script> <script id="module-clipboard"> (function($) { $(function() { var pre = document.getElementsByTagName("pre"); var pasteContent = document.getElementById("paste-content"); var hasLanguage = false; for (var i = 0; i < pre.length; i++) { var codeClass = pre[i].children[0].className; var isLanguage = codeClass.indexOf("language-"); var excludedCodeClassNames = [ "language-katex", "language-seq", "language-sequence", "language-flow", "language-flowchart", "language-mermaid", ]; var isExcluded = excludedCodeClassNames.indexOf(codeClass); if (isExcluded !== -1) { isLanguage = -1; } if (isLanguage !== -1) { var button = document.createElement("button"); button.className = "copy-button"; button.textContent = "Copy"; pre[i].appendChild(button); hasLanguage = true; } }; if (hasLanguage) { var copyCode = new ClipboardJS(".copy-button", { target: function(trigger) { return trigger.previousElementSibling; } }); copyCode.on("success", function(event) { event.clearSelection(); event.trigger.textContent = "Copied"; window.setTimeout(function() { event.trigger.textContent = "Copy"; }, 2000); }); } }); })(jQuery); </script>
<script id="module-emojify">
				(function($) {
					$(function() {
						if (typeof emojify !== "undefined") {
							emojify.setConfig({
								img_dir: "https://www.5yun.org/wp-content/plugins/wp-githuber-md/assets/vendor/emojify/images",
								blacklist: {
									"classes": ["no-emojify"],
									"elements": ["script", "textarea", "pre", "code"]
								}
							});
							emojify.run();
						} else {
							console.log("[wp-githuber-md] emogify is undefined.");
						}
					});
				})(jQuery);
			</script>
</body>
</html>