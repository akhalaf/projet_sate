<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>走啦网 - 最大的查询工具网站</title>
<meta content="走啦网提供IP地址、手机号码归属地、邮编区号、周公解梦大全、天气预报、列车时刻表、万年历、在线翻译、在线代理、最新汇率、新华字典、成语词典等查询工具。" name="description"/>
<!-- Bootstrap -->
<link href="//cdn.jsdelivr.net/npm/bootstrap@3.3.4/dist/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Custom styles for this template -->
<link href="/Public/css/style.css" rel="stylesheet"/>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="//cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
<![endif]-->
<style>
.panel-body {padding: 10px;}
</style>
</head>
<body>
<nav class="navbar navbar-fixed-top navbar-inverse">
<div class="container">
<div class="navbar-header">
<button aria-controls="navbar" aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="#">走啦网</a>
</div>
<div class="collapse navbar-collapse" id="navbar">
<ul class="nav navbar-nav">
<li class="active"><a href="#">Home</a></li>
<li><a href="//baby.zou.la/" target="_blank">未来宝宝照片合成</a></li>
<li><a href="//fanyi.zou.la/" target="_blank">在线翻译工具</a></li>
<li><a href="//iq.zou.la/" target="_blank">智商测试</a></li>
</ul>
</div>
</div>
</nav>
<div class="container">
<div class="row">
<div class="col-md-9">
<div class="jumbotron">
<h2>欢迎来到走啦网</h2>
<p>我们的域名是：<span style="color: #ff0000;">zou.la</span>，是不是很好记，本站是以提供查询工具为主的站点，我们为大家搜罗了各种各样的在线小工具，生活、工作、学习都用得到，按下 <span style="color: #ff0000;">CTRL + D</span> 收藏走啦网，随时随地访问，手机也可以上走啦。</p>
<p>下面为大家列出来的是热门的查询工具，网友使用的次数比较多，在右边的列表可以查看到各种类型的工具大全。</p>
</div>
<div class="row">
<div class="col-xs-6 col-md-3">
<center><a href="//baby.zou.la/" target="_blank"><img alt="合成宝宝照片" class="img-circle" height="80" src="//www.zou.la/Public/images/baby.jpg" width="80"/></a>
<h4><a href="//baby.zou.la/" target="_blank"><strong>合成宝宝照片</strong></a></h4>
<p>想知道你和TA未来宝宝的模样？赶快来预测宝宝长相试试吧。</p>
</center></div>
<div class="col-xs-6 col-md-3">
<center><a href="//fanyi.zou.la/" target="_blank"><img alt="免费在线翻译" class="img-circle" height="80" src="//www.zou.la/Public/images/fanyi.jpg" width="80"/></a>
<h4><a href="//fanyi.zou.la/" target="_blank">在线翻译</a></h4>
<p>完全免费的在线翻译工具，支持主流外语和小语种的在线翻译。</p>
</center></div>
<div class="col-xs-6 col-md-3">
<center><a href="//iq.zou.la/" target="_blank"><img alt="智商测试" class="img-circle" height="80" src="//www.zou.la/Public/images/iq.png" width="80"/></a>
<h4><a href="//iq.zou.la/" target="_blank"><strong>智商测试</strong></a></h4>
<p>最权威、最科学的IQ测试题，通过测试来检测您的（IQ）智商水平。</p>
</center></div>
<div class="col-xs-6 col-md-3">
<center><a href="//zhangxiang.zou.la/" target="_blank"><img alt="长相测试打分" class="img-circle" height="80" src="//www.zou.la/Public/images/zhangxiang.jpg" width="80"/></a>
<h4><a href="//zhangxiang.zou.la/" target="_blank"><strong>长相测试打分</strong></a></h4>
<p>免费的长相测试软件，上传脸部照片自动为您的长相打分。</p>
</center></div>
</div>
</div>
<div class="col-md-3">
<div aria-multiselectable="true" class="panel-group" id="accordion" role="tablist">
<div class="panel panel-success">
<div class="panel-heading" id="headingOne" role="tab">
<h4 class="panel-title">
<a aria-controls="collapseOne" aria-expanded="true" data-parent="#accordion" data-toggle="collapse" href="#collapseOne">趣味工具大全</a>
</h4>
</div>
<div aria-labelledby="headingOne" class="panel-collapse collapse in" id="collapseOne" role="tabpanel">
<div class="panel-body"><a href="//nicheng.zou.la/" target="_blank">另一半的称呼</a></div>
<div class="panel-body"><a href="//lybmz.zou.la/" target="_blank">预测另一半的名字</a></div>
<div class="panel-body"><a href="//lybzx.zou.la/" target="_blank">预测另一半的长相</a></div>
</div>
</div>
<div class="panel panel-success">
<div class="panel-heading" id="headingTwo" role="tab">
<h4 class="panel-title">
<a aria-controls="collapseTwo" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseTwo">实用工具大全</a>
</h4>
</div>
<div aria-labelledby="headingTwo" class="panel-collapse collapse" id="collapseTwo" role="tabpanel">
<div class="panel-body"><a href="/worldtime.html" target="_blank">世界各地时差查询</a></div>
<div class="panel-body"><a href="//chengyuzaoju.zou.la/" target="_blank">成语造句大全</a></div>
<div class="panel-body"><a href="/fuhao/" target="_blank">QQ特殊符号大全</a></div>
</div>
</div>
<div class="panel panel-success">
<div class="panel-heading" id="headingThree" role="tab">
<h4 class="panel-title">
<a aria-controls="collapseThree" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseThree">免费在线翻译工具</a>
</h4>
</div>
<div aria-labelledby="headingThree" class="panel-collapse collapse" id="collapseThree" role="tabpanel">
<div class="panel-body"><a href="//fanyi.zou.la/en/" target="_blank">英语在线翻译</a></div>
<div class="panel-body"><a href="//fanyi.zou.la/ko/" target="_blank">韩语在线翻译</a></div>
<div class="panel-body"><a href="//fanyi.zou.la/ru/" target="_blank">俄语在线翻译</a></div>
<div class="panel-body"><a href="//fanyi.zou.la/es/" target="_blank">西班牙语在线翻译</a></div>
<div class="panel-body"><a href="//fanyi.zou.la/ja/" target="_blank">日语在线翻译</a></div>
<div class="panel-body"><a href="//fanyi.zou.la/fr/" target="_blank">法语在线翻译</a></div>
<div class="panel-body"><a href="//fanyi.zou.la/de/" target="_blank">德语在线翻译</a></div>
<div class="panel-body"><a href="//fanyi.zou.la/it/" target="_blank">意大利语在线翻译</a></div>
</div>
</div>
<div class="panel panel-success">
<div class="panel-heading" id="headingFour" role="tab">
<h4 class="panel-title">
<a aria-controls="collapseFour" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseFour">热门网络小说合集</a>
</h4>
</div>
<div aria-labelledby="headingFour" class="panel-collapse collapse" id="collapseFour" role="tabpanel">
<div class="panel-body"><a href="//zanghaihua.zou.la/" target="_blank">藏海花在线阅读</a></div>
<div class="panel-body"><a href="//shahai.zou.la/" target="_blank">盗墓笔记少年篇·沙海在线阅读</a></div>
<div class="panel-body"><a href="//damocanglang.zou.la/" target="_blank">大漠苍狼在线阅读</a></div>
<div class="panel-body"><a href="//nujiangzhizhan.zou.la/" target="_blank">怒江之战在线阅读</a></div>
</div>
</div>
<div class="panel panel-success">
<div class="panel-heading" id="headingFive" role="tab">
<h4 class="panel-title">
<a aria-controls="collapseFive" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseFive">flash娱乐小应用</a>
</h4>
</div>
<div aria-labelledby="headingFive" class="panel-collapse collapse" id="collapseFive" role="tabpanel">
<div class="panel-body"><a href="/piano.html" target="_blank">用键盘弹钢琴</a></div>
<div class="panel-body"><a href="/game/" target="_blank">精选flash小游戏</a></div>
<div class="panel-body"><a href="//iq.zou.la" target="_blank">IQ测试题</a></div>
</div>
</div>
</div>
<div class="panel panel-warning">
<div class="panel-heading">友情链接</div>
<div class="panel-body"><a href="//www.free9.cn/" target="_blank">免费在线翻译</a></div>
<div class="panel-body"><a href="//www.900cha.com/" target="_blank">900查询网</a></div>
<div class="panel-body"><a href="//www.buyaocha.com/" target="_blank">在线工具大全</a></div>
<div class="panel-body"><a href="//www.chafayin.com/" target="_blank">查单词发音</a></div>
<div class="panel-body"><a href="//fanyi.zou.la/" target="_blank">走啦网翻译</a></div>
</div>
</div>
</div>
<hr/>
<center><span class="help-block">2008-2015 走啦网 版权所有 <a href="http://www.beian.miit.gov.cn/" rel="nofollow" target="_blank">豫ICP备14003601号</a></span></center>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="//cdn.jsdelivr.net/npm/jquery@2.1.1/dist/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="//cdn.jsdelivr.net/npm/bootstrap@3.3.4/dist/js/bootstrap.min.js"></script>
<div class="hidden">
<script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1496945'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "v1.cnzz.com/stat.php%3Fid%3D1496945' type='text/javascript'%3E%3C/script%3E"));</script>
</div>
</body>
</html>