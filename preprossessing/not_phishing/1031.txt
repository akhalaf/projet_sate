<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-type"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="https://m.05wang.com/" media="handheld" rel="alternate"/>
<title>零五网 教案 教学设计 试卷练习 教学视频 教学反思 说课稿 课件 作文 课文录音 -  05网 零5网 0五网 新知语文网</title>
<meta content="教案，教学设计，试卷练习，教学视频，教学反思，说课稿，课件，作文，课文录音" name="keywords"/>
<meta content=" 教学资源一网打尽 ,零五网" name="description"/>
<script>(function(){if(/(iphone|ipad|android|mobile|symbian)/i.test(navigator.userAgent)){var page_url=window.location.href;if(page_url.indexOf('://www')>-1) window.location.href=page_url.replace(/:\/\/www./,'://m.');else if(page_url.indexOf('://05wang')>-1) window.location.href=page_url.replace(/:\/\/05wang/,'://m.05wang');}})()</script>
<base href="https://www.05wang.com/"/>
<script>var M_SITE = "https://www.05wang.com/",M_SITE_API = "https://handler.05wang.com/",M_PIC = "https://thumb.1010pic.com/";</script>
<link href="https://www.05wang.com/static/css/bootstrap.min.css?v=6" rel="stylesheet" type="text/css"/>
<link href="https://www.05wang.com/static/css/style.css?v=6" rel="stylesheet" type="text/css"/>
<link href="https://www.05wang.com/static/css/non-responsive.css?v=6" rel="stylesheet" type="text/css"/>
<script src="https://www.05wang.com/static/js/jquery.min.js?v=6"></script>
<script src="https://www.05wang.com/static/js/navway.js?v=6"></script>
<script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "//hm.baidu.com/hm.js?93bcca87e7e6c90674ad20428855d3ba";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
        (window._ssp_global = window._ssp_global || {}).userConfig = {
            domain: "myc.1010jiajiao.com"
        }

    </script>
</head>
<body>
<!--顶部Start-->
<style>
    .login_box .headimg_box{
        width:50px;
        height: 50px;
        border: 1px solid #ccc;
    }
    .login_box .headimg_box img{
        max-width:45px;
        max-height: 45px;
        margin: 2px;
    }
    .user-set-menu {
        padding-top: 6px;
        top: 70px;
        position: absolute;
        float: right;
        width: 80px;
        z-index: 999;
        text-align: left;
        display: block;
        background-color: #fff;
    }
    .user-set-menu div {
        position: relative;
        border: 1px solid #E3E3E3;
        box-shadow: 1px 1px 5px #d1d1d1;
        -moz-box-shadow: 1px 1px 5px #d1d1d1;
        -webkit-box-shadow: 1px 1px 5px #d1d1d1;
    }
    .user-set-menu a {
        text-align: center;
        display: block;
        width: 80px;
        text-shadow: none;
        color: #000;
    }
    .user-set-menu .menu-arrow {
        float: right;
        right: 32px;
    }
    .user-set-menu .menu-arrow {
        position: absolute;
        z-index: 2;
        top: -4px;
        left: 28px;
        display: inline-block;
        width: 0;
        height: 0;
        line-height: 0;
        border: 5px dashed transparent;
        border-bottom: 5px solid #e3e3e3;
        font-size: 0;
    }
</style>
<div id="top">
<div class="container">
<div class="header_top">
<div class="row">
<div class="col-xs-3" id="logo">
<a href="https://www.05wang.com/"><img alt="零五网" src="https://www.05wang.com/static/image/common/logo.png"/></a>
</div>
<div class="col-xs-5" style="margin-top:20px;">
<form action="https://www.05wang.com/search" role="form">
<div class="input-group">
<input class="form-control" name="word" placeholder="输入关键词..." type="text" value=""/>
<span class="input-group-btn">
<button class="btn btn-info" type="submit"> 搜  索 </button>
</span> </div>
</form>
</div>
<!--<div class="col-xs-2">
							<p>快捷登录方式<br />
                            <span id="qqLoginBtn"></span>
                            </a></p>
						</div>-->
<div class="col-xs-4 login_box">
</div>
</div>
</div>
</div>
<!--顶部End-->
<!--导航菜单Start-->
<nav class="navbar" style="border-width: 1px 0;border-radius: 0;background-color: #0ca4e3;border-color: #eaf6fc;">
<div class="container">
<div class="container-fluid">
<div class="navbar-collapse collapse" id="navbar">
<span class="daohang">
<ul class="nav navbar-nav">
<li class="active"><a href="https://www.05wang.com/">首  页</a></li>
<li><a href="https://www.05wang.com/thread-10104-1-1.html">练习与测试答案</a></li>
<li><a href="https://www.05wang.com/forum-128-1.html">补充习题答案</a></li>
<li><a href="https://www.05wang.com/thread-14162-1-1.html">课课练答案</a></li>
<li><a href="https://www.05wang.com/thread-14613-1-1.html">同步练习答案</a></li>
<li><a href="https://www.05wang.com/forum-103-1.html">阅读答案</a></li>
<li><a href="https://www.05wang.com/dianzi/">电子课本</a></li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">更多 <span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="https://www.05wang.com/forum-138-1.html">其他答案</a></li>
<li><a href="https://www.05wang.com/forum-130-1.html">课件下载</a></li>
<li><a href="https://www.05wang.com/thread-4408-1-1.html">试卷练习</a></li>
<li><a href="https://www.05wang.com/forum-91-1.html">教学反思</a></li>
<li><a href="https://www.05wang.com/forum-92-1.html">说课稿</a></li>
<li><a href="https://www.05wang.com/thread-4411-1-1.html">录音下载</a></li>
<li><a href="https://www.05wang.com/thread-4410-1-1.html">教案下载</a></li>
<li><a href="https://www.05wang.com/forum-101-1.html">作文范例</a></li>
<li><a href="https://www.05wang.com/forum-107-1.html">简笔画下载</a></li>
<li><a href="https://www.05wang.com/forum-2-1.html">教学视频</a></li>
<li><a href="https://www.05wang.com/forum-132-1.html">语文知识</a></li>
<li><a href="https://www.05wang.com/forum-82-1.html">班主任资料</a></li>
<li><a href="https://www.05wang.com/forum-86-1.html">教材</a></li>
<li><a href="https://www.05wang.com/forum-87-1.html">其他资料</a></li>
<li><a href="/xuexiao/" target="_blank">找学校</a></li>
</ul>
</li>
</ul>
</span>
<ul class="nav navbar-nav navbar-right">
</ul>
</div>
</div>
<!-- 导航菜单End-->
</div>
<div style="text-align: center;margin: -18px auto 0;width: 1000px;"><script src="//fanyi.fanyi100.com.cn/idmzzeacvgkcef.js" type="text/javascript"></script></div>
<script>
    $(function(){
        /*api.post('login/login_info',function(s){
            var str='';
            if(s.code==1){
                str='<a href="'+U("login")+'">登录</a> | <a href="'+U("register")+'">注册</a>';
            }else{
                //console.log(s);
                str='<div  class="big_box" style="float:right"><div class="headimg_box"><img src="'+ s.result.headimg+'"></div><p>你好，'+s.result.username+'</p><div  class="user-set-menu hide"> <div> <a href="'+U('individual')+'" target="_blank"> 个人中心 </a> <a href="javascript:web.loginOut()">退出登录</a> </div> <span class="menu-arrow"><em></em></span></div></div>';
            }
            $('.login_box').html(str);
        })

       $(document).on('mouseover','.big_box',function(){
            $('.user-set-menu').removeClass('hide');
        });

        $(document).on('mouseout','.big_box',function(){
            $('.user-set-menu').addClass('hide');
        });*/
    })
</script><style type="text/css">

.main {
    width:100%;
    margin: 0 auto;
    zoom: 1;
}

* {
    margin: 0;
    padding: 0;
}

div {
    display: block;
}
a  {
    text-decoration:none;
    color: #333;
}

a:hover{
    text-decoration:none;
}
ul, ol, li {
    list-style: none;
}
li {
    display: list-item;
    text-align: -webkit-match-parent;
}
.index_h1 {
    height: 40px;
    border-bottom: 1px solid #ddd;
}
.index_h1 .tt {
    display: block;
    color: #333;
    font: 400 20px/40px Arial,"微软雅黑";
    float: left;
    width: 100px;
    border-bottom: 1px solid #4593fd;
}
.h8 {
    clear: both;
    height: 8px;
}

.h20 {
    height: 20px;
    clear: both;
}

.index_h1 .more {
    float: right;
    border: 1px solid #e0e0e0;
    padding: 2px 8px;
    border-radius: 10px;
    margin-top: 13px;
    cursor: pointer;
}
.ebook_list {
    border: 1px solid #e9e9e9;
    border-top: none;
    padding: 8px 0px 8px 8px;
    overflow: hidden;
}
.ebook_list li {
    width: 150px;
    height: 220px;
    float: left;
    margin-left: 30px;
    margin-bottom: 10px;
    border: 1px solid #e0e0e0;
}
.ebook_list li .tt {
    display: block;
    overflow: hidden;
    padding: 0px 5px;
    text-align: center;
}
.ebook_list li img {
    margin: 5px auto ;
    width: 127px;
    height: 175px;
    display: block;
}
.pub{
    text-align: center;
    display: block;
    width: 150px;
    height: 25px;
    overflow: hidden;
    font-size:15px;
    padding: 0 5px;
}
ul, menu, dir {
    display: block;
    -webkit-margin-after: 1em;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    -webkit-padding-start: 30px;
}
.shop_list {
    border: 1px solid #e9e9e9;
    border-top: none;
    padding: 8px 0px 8px 8px;
    overflow: hidden;
}
.shop_list li {
    float: left;
    margin: 0px 8px 15px 8px;
    line-height: 24px;
}
.shop_list li span {
    display: block;
    border-radius: 10px;
    box-shadow: 4px 4px 2px #ccc;
    font-size: 20px;
    margin-top: 8px;
    border: 1px solid #ddd;
    padding: 8px 5px;
    background: #f0f0f0;
    color: #0068b7;
    text-align: center;
}
.flink {
    line-height: 28px;
}
.flink .tt {
    font-size: 15px;
    margin-bottom: 5px;
    border-bottom: 2px solid #4593fd;
}
.flink .list {
    margin-bottom: 20px;
}
.flink .list li {
    float: left;
    width: auto;
    overflow: hidden;
    height: 28px;
    margin-left:20px;
}
.flink .list a {
    color: #666;
}


/*书单 */
#booklist{
    padding: 8px 0px 8px 8px;
    background-color: #fff;
    border: 1px solid #e9e9e9;
    border-top: none;
    overflow: hidden;
}
#booklist ul{
    list-style: none;
    display: block;
}
#booklist .book_box {
    width: 200px;
    height: 250px;
    padding: 5px 8px;
    border: 1px #ccc solid;
    float:left;
    margin: 10px;
    text-align: center;
}
#booklist .book_box  .img {
    position: relative;
    height: 180px;
}
#booklist .book_box  .img img {
    display: block;
    max-width: 150px;
    max-height: 150px;
}
#booklist .book_box  .img .cover{
    position:absolute;
    top:20px;
    left:20px;
}

#booklist .book_box .bottom .bookname {
    margin-top: 5px;
    height: 50px;
    line-height: 20px;
    display: block;
}
</style>
<div class="container">
<div id="booxs_box">
</div>
<div class="main">
<div class="index_h1">
<span class="tt">最新资源</span>
<div class="more">
<a href="https://www.05wang.com/xilie/">更多</a>
</div>
</div>
<div class="h8"></div>
<div class="shop_list"><p style="text-align: center;font-size: 16px;color: red;">零五网2021版最新答案正在努力更新中... 请大家持续关注哦！</p>
<ul>
<li><a href="https://www.05wang.com/thread-10104-1-1.html"><span>练习与测试答案</span></a></li>
<li><a href="https://www.05wang.com/thread-123542-1-1.html"><span>补充习题答案</span></a></li>
<li><a href="https://www.05wang.com/thread-14162-1-1.html"><span>课课练答案</span></a></li>
<li><a href="https://www.05wang.com/thread-14613-1-1.html"><span>同步练习答案</span></a></li>
<li><a href="https://www.05wang.com/thread-102572-1-1.html"><span>通城学典课时作业本答案</span></a></li>
<li><a href="https://www.05wang.com/thread-62716-1-1.html"><span>学习与评价答案</span></a></li>
<li><a href="https://www.05wang.com/thread-14185-1-1.html"><span>伴你学答案</span></a></li>
<li><a href="https://www.05wang.com/thread-957449-1-1.html"><span>讲练超链接</span></a></li>
<li><a href="https://www.05wang.com/thread-957419-1-1.html"><span>随堂反馈</span></a></li>
<li><a href="https://www.05wang.com/thread-115347-1-1.html"><span>亮点给力提优课时作业本答案</span></a></li>
<li><a href="https://www.05wang.com/thread-957418-1-1.html"><span>创新优化学案</span></a></li>
<li><a href="https://www.05wang.com/thread-98569-1-1.html"><span>新课程自主学习与测评答案</span></a></li>
<li><a href="https://www.05wang.com/thread-11043-1-1.html"><span>数学英语课本答案</span></a></li>
<li><a href="https://www.05wang.com/thread-115067-1-1.html"><span>金钥匙课时学案作业本答案</span></a></li>
<li><a href="https://www.05wang.com/thread-116485-1-1.html"><span>听读空间答案</span></a></li>
</ul>
</div>
</div>
<div class="h20"></div>
<div class="main">
<div class="index_h1">
<span class="tt">电子课本</span>
<div class="more">
<a href="https://www.05wang.com/dianzi/">更多</a>
</div>
</div>
<div class="h8"></div>
<div class="ebook_list">
<ul>
<li>
<a href="https://www.05wang.com/diandu/book/89.html">
<img src="https://thumb.1010pic.com/dmt/diandu/89/cover.jpg"/>
<span class="pub">四年级语文（上）</span>
</a>
</li>
<li>
<a href="https://www.05wang.com/diandu/book/92.html">
<img src="https://thumb.1010pic.com/dmt/diandu/92/cover.jpg"/>
<span class="pub">三年级语文（上）</span>
</a>
</li>
<li>
<a href="https://www.05wang.com/diandu/book/95.html">
<img src="https://thumb.1010pic.com/dmt/diandu/95/cover.jpg"/>
<span class="pub">一年级语文（上）</span>
</a>
</li>
<li>
<a href="https://www.05wang.com/diandu/book/97.html">
<img src="https://thumb.1010pic.com/dmt/diandu/97/cover.jpg"/>
<span class="pub">二年级语文（上）</span>
</a>
</li>
<li>
<a href="https://www.05wang.com/diandu/book/99.html">
<img src="https://thumb.1010pic.com/dmt/diandu/99/cover.jpg"/>
<span class="pub">五年级语文（上）</span>
</a>
</li>
<li>
<a href="https://www.05wang.com/diandu/book/101.html">
<img src="https://thumb.1010pic.com/dmt/diandu/101/cover.jpg"/>
<span class="pub">六年级语文（上）</span>
</a>
</li>
<li>
<a href="https://www.05wang.com/diandu/book/1647.html">
<img src="https://thumb.1010pic.com/dmt/diandu3/cover/149.jpg"/>
<span class="pub">苏教版七年级语文上册</span>
</a>
</li>
<li>
<a href="https://www.05wang.com/diandu/book/1650.html">
<img src="https://thumb.1010pic.com/dmt/diandu3/cover/152.jpeg"/>
<span class="pub">苏教版七年级上册生物</span>
</a>
</li>
<li>
<a href="https://www.05wang.com/diandu/book/1652.html">
<img src="https://thumb.1010pic.com/dmt/diandu3/cover/429.jpg"/>
<span class="pub">苏教版一年级数学上册</span>
</a>
</li>
<li>
<a href="https://www.05wang.com/diandu/book/1654.html">
<img src="https://thumb.1010pic.com/dmt/diandu3/cover/431.jpg"/>
<span class="pub">苏教版小学一年级语文上册</span>
</a>
</li>
<li>
<a href="https://www.05wang.com/diandu/book/1656.html">
<img src="https://thumb.1010pic.com/dmt/diandu3/cover/433.jpg"/>
<span class="pub">苏教版小学一年级英语上册</span>
</a>
</li>
<li>
<a href="https://www.05wang.com/diandu/book/1658.html">
<img src="https://thumb.1010pic.com/dmt/diandu3/cover/435.jpg"/>
<span class="pub">苏教版二年级数学上册</span>
</a>
</li>
</ul>
</div>
</div>
<div class="h20"></div>
<div class="main">
<div class="index_h1">
<span class="tt">学习资料</span>
</div>
<div class="h8"></div>
<div class="shop_list">
<ul>
<li><a href="https://www.05wang.com/forum-133-1.html"><span>语文试卷练习</span></a></li>
<li><a href="https://www.05wang.com/forum-134-1.html"><span>数学试卷练习</span></a></li>
<li><a href="https://www.05wang.com/forum-82-1.html"><span>班主任资料</span></a></li>
<li><a href="https://www.05wang.com/forum-138-1.html"><span>其他答案</span></a></li>
<li><a href="https://www.05wang.com/forum-101-1.html"><span>作文范例</span></a></li>
<li><a href="https://www.05wang.com/forum-91-1.html"><span>语文教学反思</span></a></li>
<li><a href="https://www.05wang.com/forum-92-1.html"><span>语文说课稿</span></a></li>
<li><a href="https://www.05wang.com/forum-132-1.html"><span>语文知识集锦</span></a></li>
<li><a href="https://www.05wang.com/forum-107-1.html"><span>简笔画下载</span></a></li>
<li><a href="https://www.05wang.com/forum-97-1.html"><span>语文课件</span></a></li>
<li><a href="https://www.05wang.com/forum-99-1.html"><span>英语课件</span></a></li>
<li><a href="https://www.05wang.com/forum-110-1.html"><span>语文课文录音下载</span></a></li>
<li><a href="https://www.05wang.com/forum-111-1.html"><span>英语课文录音下载</span></a></li>
<li><a href="https://www.05wang.com/forum-98-1.html"><span>数学课件</span></a></li>
<li><a href="https://www.05wang.com/forum-2-1.html"><span>教学视频</span></a></li>
<li><a href="https://www.05wang.com/forum-103-1.html"><span>阅读题答案</span></a></li>
<li><a href="https://www.05wang.com/thread-4410-1-1.html"><span>教案下载</span></a></li>
</ul>
</div>
</div>
<div class="h20"></div>
<div class="main">
<div class="index_h1">
<span class="tt">找学校</span>
</div>
<div class="h8"></div>
<div class="shop_list">
<ul>
<li><a href="/xuexiao/" target="_blank"><span>大学排名</span></a></li>
<li><a href="/xuexiao/daxue-liebiao-0-20-20-20-6-1/" target="_blank"><span>211工程院校</span></a></li>
<li><a href="/xuexiao/daxue-liebiao-0-20-20-20-5-1/" target="_blank"><span>985工程院校</span></a></li>
<li><a href="/xuexiao/daxue-liebiao-0-20-20-0-0-1/" target="_blank"><span>本科院校</span></a></li>
<li><a href="/xuexiao/daxue-liebiao-0-20-20-1-0-1/" target="_blank"><span>专科学校</span></a></li>
<li><a href="/xuexiao/daxue-1/" target="_blank"><span>北京大学</span></a></li>
<li><a href="/xuexiao/daxue-2/" target="_blank"><span>清华大学</span></a></li>
<li><a href="/xuexiao/daxue-3/" target="_blank"><span>上海交大</span></a></li>
<li><a href="/xuexiao/daxue-4/" target="_blank"><span>复旦大学</span></a></li>
<li><a href="/xuexiao/daxue-6/" target="_blank"><span>浙江大学</span></a></li>
<li><a href="/xuexiao/daxue-5/" target="_blank"><span>武汉大学</span></a></li>
<li><a href="/xuexiao/daxue-7/" target="_blank"><span>中国人民大学</span></a></li>
<li><a href="/xuexiao/daxue-8/" target="_blank"><span>南京大学</span></a></li>
<li><a href="/xuexiao/daxue-22/" target="_blank"><span>北京理工大学</span></a></li>
<li><a href="/xuexiao/daxue-81/" target="_blank"><span>北京师范大学</span></a></li>
<li><a href="/xuexiao/daxue-2086/" target="_blank"><span>中国农业大学</span></a></li>
<li><a href="/xuexiao/daxue-2098/" target="_blank"><span>中央民族大学</span></a></li>
<li><a href="/xuexiao/daxue-29/" target="_blank"><span>北京科技大学</span></a></li>
<li><a href="/xuexiao/daxue-125/" target="_blank"><span>北京交通大学</span></a></li>
<li><a href="/xuexiao/daxue-145/" target="_blank"><span>中国政法大学</span></a></li>
<li><a href="/xuexiao/daxue-155/" target="_blank"><span>中国传媒大学</span></a></li>
<li><a href="/xuexiao/daxue-liebiao-0-20-20-20-0-1/" target="_blank"><span>选大学</span></a></li>
<li><a href="/xuexiao/zhuanye/" target="_blank"><span>选专业</span></a></li>
<li><a href="/xuexiao/fenshuxian-diqu-20-0-20-0-1/" target="_blank"><span>分数线</span></a></li>
<li><a href="/xuexiao/gaozhong-liebiao-0-0-0-1/" target="_blank"><span>找高中</span></a></li>
<li><a href="/xuexiao/chuzhong-liebiao-0-0-0-1/" target="_blank"><span>初中学校</span></a></li>
<li><a href="/xuexiao/sitemap.xml" target="_blank"><span>站内导航</span></a></li>
<li><a href="/xuexiao/gaozhong-liebiao-34-0-0-1/" target="_blank"><span>北京高中</span></a></li>
<li><a href="/xuexiao/gaozhong-liebiao-33-0-0-1/" target="_blank"><span>上海高中</span></a></li>
<li><a href="/xuexiao/gaozhong-liebiao-16-0-0-1/" target="_blank"><span>广东高中</span></a></li>
<li><a href="/xuexiao/gaozhong-liebiao-19-0-0-1/" target="_blank"><span>河南高中</span></a></li>
<li><a href="/xuexiao/gaozhong-liebiao-32-0-0-1/" target="_blank"><span>天津高中</span></a></li>
<li><a href="/xuexiao/gaozhong-liebiao-13-0-0-1/" target="_blank"><span>重庆高中</span></a></li>
<li><a href="/xuexiao/gaozhong-liebiao-31-0-0-1/" target="_blank"><span>河北高中</span></a></li>
<li><a href="/xuexiao/" target="_blank"><span>更多学校查询</span></a></li>
</ul>
</div>
</div>
<div class="h20"></div>
<div class="main">
<div class="index_h1">
<span class="tt">热门IT培训</span>
</div>
<div class="h8"></div>
<div class="shop_list">
<ul>
<li><a href="https://www.05wang.com/it/ls/" target="_blank"><span>IT培训名师</span></a></li>
<li><a href="https://www.05wang.com/it/xy/" target="_blank"><span>IT行业就业怎么样</span></a></li>
<li><a href="https://www.05wang.com/it/xq/" target="_blank"><span>IT培训机构查询</span></a></li>
<li><a href="https://www.05wang.com/it/jigou/" target="_blank"><span>IT培训机构</span></a></li>
<li><a href="https://www.05wang.com/it/javacs/" target="_blank"><span>java培训机构</span></a></li>
<li><a href="https://www.05wang.com/it/uids/" target="_blank"><span>UI培训班</span></a></li>
<li><a href="https://www.05wang.com/it/beidaqingniao/" target="_blank"><span>北大青鸟学校</span></a></li>
<li><a href="https://www.05wang.com/it/frontweb/" target="_blank"><span>web前段培训</span></a></li>
<li><a href="https://www.05wang.com/it/course/" target="_blank"><span>IT培训课程</span></a></li>
<li><a href="https://www.05wang.com/it/hotcourse/" target="_blank"><span>IT培训学校</span></a></li>
<li><a href="https://www.05wang.com/it/ruanjian/" target="_blank"><span>软件开发培训</span></a></li>
<li><a href="https://www.05wang.com/it/itzhiye/" target="_blank"><span>学什么技术好</span></a></li>
<li><a href="https://www.05wang.com/it/itpeixun/" target="_blank"><span>电脑培训学校</span></a></li>
<li><a href="https://www.05wang.com/it/newnet/" target="_blank"><span>网络营销培训</span></a></li>
<li><a href="https://www.05wang.com/it/news/" target="_blank"><span>IT培训哪家好</span></a></li>
<li><a href="https://www.05wang.com/it/ls/" target="_blank"><span>IT培训名师</span></a></li>
<li><a href="https://www.05wang.com/it/" target="_blank"><span>北京java培训</span></a></li>
<li><a href="https://www.05wang.com/it/" target="_blank"><span>大数据培训</span></a></li>
<li><a href="https://www.05wang.com/it/" target="_blank"><span>人工智能培训</span></a></li>
<li><a href="https://www.05wang.com/it/" target="_blank"><span>Python培训</span></a></li>
<li><a href="https://www.05wang.com/it/sitemap.xml" target="_blank"><span>站内索引导航</span></a></li>
</ul>
</div>
</div>
<div class="h20"></div>
<div class="flink">
<div class="tt main">
<ul id="flink">
<li>友情链接</li>
</ul>
<div class="h0"></div>
</div>
<div class="main">
<div class="list">
<ul>
<li><a href="http://www.1010jiajiao.com/" target="_blank">精英家教网</a></li>
<li><a href="http://www.xj5u.com/" target="_blank">小学资源网</a></li>
<li><a href="http://www.jisiedu.com/" target="_blank">月亮岛教育网</a></li>
<li><a href="http://edu.yjbys.com/" target="_blank">应届毕业生培训网</a></li>
<li><a href="http://www.edudo.com/" target="_blank">语文网</a></li>
<li><a href="http://www.jiangshi.org/" target="_blank">培训讲师</a></li>
<li><a href="http://zuowen.mofangge.com/" target="_blank">作文网</a></li>
<li><a href="http://www.9xwang.com/" target="_blank">范文网</a></li>
<li><a href="http://www.ayyzz.cn/" target="_blank">作文大全</a></li>
<li><a href="http://gan.0s.net.cn/" target="_blank">读后感网</a></li>
<li><a href="http://www.xiaobao8.com/ " target="_blank">小报吧</a></li>
<li><a href="http://www.zuowen.org.cn/ " target="_blank">作文素材</a></li>
<li><a href="http://www.zuowen8.com/" target="_blank">作文吧</a></li>
<li><a href="http://www.slkj.org/" target="_blank">古诗</a></li>
<li><a href="http://kaoshi.yjbys.com/" target="_blank">考试网</a></li>
<li><a href="http://wangming.oicq88.com/" target="_blank">QQ网名</a></li>
<li><a href="http://www.yuwenmi.com/" target="_blank">语文网</a></li>
<li><a href="http://www.pincai.com/" target="_blank">聘才网</a></li>
<li><a href="http://www.cnfla.com/" target="_blank">儿童学习网</a></li>
<li><a href="http://www.2018.cn/" target="_blank">招生</a></li>
<li><a href="http://www.ninpang.com" target="_blank">您旁网</a></li>
</ul>
</div>
</div>
</div>
<div class="h20"></div>
</div>
<script>
    $(function(){
        api.post('user/individual/get_profile',function(s){
            if(s.code===1){
                //$('#booklist').html('<div style="text-align: center;font-size: 18px;">请先<a href="'+U('login')+'" style="color:blue;">登录</a>，再查看书单</div>');
            }else{
                $('.headimg_box .box_top').html('<img class="headimg" src="'+ s.result.headimg+'" />');
                $('.username').html(s.result.username);
                    var str='<div class="main"><div class="index_h1"><span class="tt">我的书单</span></div><div class="h8"></div> <div  id="booklist"><ul>';
                    if(s.result.collect_list.length>0){
                        $.each(s.result.collect_list,function(i,e){
                            str+='<li class="book_box"><a target="_blank" href="'+U('thread-'+ e.thread_id+'-1-1.html')+'"><div class="img"><img  class="cover" src="'+ e.cover+'">';
                            str+='</div><div class="bottom"><span class="bookname">'+ e.bookname+'</span></div></a></li>';
                        });
                    }else{
                        str+='<div style="text-align: center;font-size: 18px;">您还没有任何收藏</div>';
                    }

                    str+='</ul></div></div><div class="h20"></div>';
                    $('#booxs_box').html(str);

            }

        });
    })
</script><div class="footer">
<div class="container">
<ul class="footer-nav">
<li><a href="https://www.05wang.com/thread-1615-1-1.html" target="_blank">关于零五网</a></li>
<li><a href="https://www.05wang.com/thread-817-1-1.html" target="_blank">联系站长</a></li>
<li><a href="https://www.05wang.com/thread-1614-1-1.html" target="_blank">免责申明</a></li>
<li><a href="https://m.05wang.com/">零五网手机版</a></li>
</ul>
<p class="copyright">© 2012-2018 www.05wang.com - <a href="https://beian.miit.gov.cn" target="_blank">( 沪ICP备07509807号-14 ) </a> <a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=42018502000814" target="_blank">鄂公网安备42018502000814号</a></p>
</div>
</div>
<script src="https://www.05wang.com/static/js/bootstrap.min.js?v=6"></script>
<script src="https://www.05wang.com/static/js/jquery.cookie.js?v=6"></script>
<script src="https://www.05wang.com/static/js/global.js?v=6"></script>
<script>
    $(function () {
        $("#center").css({"min-height":(document.body.clientHeight-$("#top").height()-$(".footer").height()-78)+"px"});
    });
</script>
</nav></div></body>
</html>