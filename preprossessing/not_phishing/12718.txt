<!--- marxchshow_zhaobiao---><!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>郑州市二七区疾病预防控制中心关于医用药品冷藏车购置项目竞争性谈判公告（三次）_专用汽车招标公告_专汽网</title>
<meta content="医用药品 冷藏车 " name="keywords"/>
<meta content="招标编号：二七政采谈判-2015-92河南腾飞工程造价咨询有限公司受郑州市二七区疾病预防控制中..." name="description"/>
<meta content="pc,mobile" name="applicable-device"/>
<meta content="no-siteapp" http-equiv="Cache-Control"/>
<meta content="no-transform" http-equiv="Cache-Control"/>
<link href="//www.17350.com/statics/css/zqw/public.css" rel="stylesheet" type="text/css"/>
<script src="//www.17350.com/statics/js/zqw/jquery.min.js" type="text/javascript"></script>
<script src="//www.17350.com/statics/js/zqw/common.js" type="text/javascript"></script>
<script>
(function(){
    var bp = document.createElement('script');
    var curProtocol = window.location.protocol.split(':')[0];
    if (curProtocol === 'https'){
   bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
  }
  else{
  bp.src = 'http://push.zhanzhang.baidu.com/push.js';
  }
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(bp, s);
})();
</script><link href="//www.17350.com/statics/css/zqw/zhaobiao.css" rel="stylesheet"/>
</head>
<body>
<!--head部分!-->
<div class="container container_top cf"> <div class="container_1200 m_center header cf">
<div class="zqw-logo">
<a href="/"><span></span></a>
<span>上专汽网<br/>买放心车</span>
</div>
<div class="car_searchbar">
<!-- <script type="text/javascript">
(function(){
	document.write(unescape('%3Cdiv id="bdcs"%3E%3C/div%3E'));
	var bdcs = document.createElement('script');
	var href = "http://www.17350.com/";//window.location.href;
	bdcs.type = 'text/javascript';
	bdcs.async = true;
	bdcs.src = 'http://znsv.baidu.com/customer_search/api/js?sid=3789804597456952333' + '&plate_url=' + encodeURIComponent(href) + '&t=' + Math.ceil(new Date()/3600000);
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(bdcs, s);})();
</script> -->
<!--<div id="bdcs">
			<div class="bdcs-container">
				<meta http-equiv="x-ua-compatible" content="IE=9">  
					<div class="bdcs-main bdcs-clearfix" id="default-searchbox">      
						<div class="bdcs-search bdcs-clearfix" id="bdcs-search-inline">          
							<form action="http://so.17350.com/cse/search" method="get" target="_blank" class="bdcs-search-form" autocomplete="off" id="bdcs-search-form">              
								<input type="hidden" name="s" value="3789804597456952333">              
								<input type="hidden" name="entry" value="1">                                                                        
								<input type="text" name="q" class="bdcs-search-form-input" id="bdcs-search-form-input" placeholder="请输入关键词" style="height: 30px; line-height: 30px;">              
								<input type="submit" class="bdcs-search-form-submit " id="bdcs-search-form-submit" value="搜索">                      
							</form>      
						</div>                     
					<div class="bdcs-hot" id="bdcs-hot">               
					</div>       
				</div>                           
			</div>
		</div>-->
</div>
<div class="head_r f_right">
<span class="tel">客服热线 <span class="phone">
	  
	   			18008665020
		</span></span>
</div>
</div> </div>
<div class="m_b10" id="menu">
<ul id="nav">
<li class="mainlevel" id="mainlevel_01"><a href="/">首页</a> </li>
<li class="mainlevel" id="mainlevel_01"><a href="/zhaobiao/">招标中心</a> </li>
<li class="mainlevel" id="mainlevel_0{$n+1}"><a href="https://www.17350.com/zhaobiao/yugao/">招标预告</a></li>
<li class="mainlevel" id="mainlevel_0{$n+1}"><a href="https://www.17350.com/zhaobiao/gonggao/">招标公告</a></li>
<li class="mainlevel" id="mainlevel_0{$n+1}"><a href="https://www.17350.com/zhaobiao/biangeng/">招标变更</a></li>
<li class="mainlevel" id="mainlevel_0{$n+1}"><a href="https://www.17350.com/zhaobiao/zhongbiao/">中标结果</a></li>
<div class="clear"></div>
</ul>
</div><div class="container_1200 m_center cf m_t20">
<!--面包屑导航!-->
<div class="mianbaoxie_text">当前位置：<a href="http://www.17350.com">主页</a> &gt; <a href="http://www.17350.com/zhaobiao/">招标中心</a> &gt; <a href="https://www.17350.com/zhaobiao/gonggao/"> 招标公告</a> &gt; 郑州市二七区疾病预防控制中心关于医用药品冷藏车购置项目竞争性谈判公告（三次）</div>
<div class="wenshang">
<h1>郑州市二七区疾病预防控制中心关于医用药品冷藏车购置项目竞争性谈判公告（三次）</h1>
<div class="daoxiao"> <span class="site">地区：
            河南-郑州</span> <span class="zhuangtai">状态：
      招标公告 </span>
<span class="zhuangtai">
			
			 			 
			  								 招标车型：冷藏车 
				 			  			 
		  </span>
</div>
<div class="wenzhong">
<div class="biaoge">
<table cellpadding="0" cellspacing="0" class="table_content" style="margin-bottom: 10px;width:800px;">
<tr>
<td align="center" class="bg" width="89"><span class="b2">更新时间:</span></td>
<td width="195"><span class="d2">2016年03月07日</span></td>
<td align="center" class="bg" width="89"><span class="b2">招标城市:</span></td>
<td width="187"><span class="d2">郑州</span></td>
</tr>
<tr>
<td align="center" class="bg" width="89"><span class="b2">截止时间:</span></td>
<td width="187"><span class="d2">2016年03月11日</span></td>
<td align="center" class="bg" width="89"><span class="b2">项目名称:</span></td>
<td width="187"><span class="d2">郑州市二七区疾病预防控制中心关于医用药品冷藏车购置项目竞争性谈判公告（三次）</span></td>
</tr>
<tr>
<td align="center" class="bg" width="89"><span class="b2">代理机构:</span></td>
<td width="187"><span class="d2"></span></td>
<td align="center" class="bg" width="89"><span class="b2">招标单位:</span></td>
<td width="187"><span class="d2">受郑州市二七区疾病预防控制中心</span></td>
</tr>
<tr>
<td align="center" class="bg" width="89"><span class="b2">招标编号:</span></td>
<td width="187"><span class="d2">二七政采谈判-2015-92</span></td>
<td align="center" class="bg" width="89"><span class="b2">招标类别:</span></td>
<td width="187"><span class="d2">公告</span></td>
</tr>
</table>
</div>
<div id="wen">
<div> <table border="0" cellpadding="0" cellspacing="1" style="background: #ccc;"><p>招标编号：二七政采谈判-2015-92</p><p>河南腾飞工程造价咨询有限公司受郑州市二七区疾病预防控制中心委托，就郑州市二七区疾病预防控制中心关于医用药品冷藏车购置项目进行三次竞争性谈判采购。欢迎符合相关条件的供应商申请参加谈判，现将谈判事宜公告如下：</p><p>一、项目名称及编号：</p><p>项目名称：郑州市二七区疾病预防控制中心关于医用药品冷藏车购置项目（三次）</p><p>招标编号：二七政采谈判-2015-92</p><p>二、项目概况：</p><p>购置医用药品冷藏车一辆，预算金额15万元 (具体参数详见谈判文件)</p><p>三、供应商资质要求： </p><p>3.1供应商应符合《中华人民共和国政府采购法》第22条规定；</p><p>3.2供应商须具有独立法人资格，且年检合格；</p><p>3.3供应商须具有财务状况报告，依法缴纳税收和社会保障资金的材料；</p><p>3.4参加政府采购活动前3年内在经营活动中没有重大违法记录的书面声明；</p><p>3.4本项目不接受联合体投标； </p><p>3.5供应商须提供企业注册所在地或项目所在地检察院职务犯罪预防局行贿犯罪档案查询函，时间需在有效期内。 </p><p>四、谈判文件获取信息： </p><p>1、获取谈判文件时间： 2016年3月7日— 2016年3月9日上午8：30分—12:00时，下午 14:30分—17:30分(北京时间，下同；法定节假日除外)；</p><p>2、获取文件地点：郑州市郑汴路玉凤路交叉口南500米升龙环球大厦C座10层1005室；</p><p>3、文件售价300元/套，售后不退。</p><p>注意事项：供应商购买谈判文件时须提供法人授权委托书原件、法人身份证复印件、授权委托人身份证、企业营业执照、税务登记证、组织机构代码证、检察机关无行贿犯罪档案查询，以上证件审验原件留存复印件（加盖公章）壹套。</p><p>备注：供应商应提供所有盖章复印件必须是清晰、完整的，供应商应将相关证件的变更、延期等材料一并复印盖章，资料不完整不清晰的报名申请，工作人员有权不予接纳，由此造成的后果由申请人自行承担。 </p><p>五、响应文件递交信息：</p><p>1、响应文件接收截止时间：2016年3月11日上午9：30分（北京时间）</p><p>2、地点：郑州市郑汴路玉凤路交叉口南500米升龙环球大厦C座10层河南腾飞工程造价咨询有限公司会议室</p><p>3、逾期送达或者未送达指定地点的响应文件，招标人不予受理。</p><p>六、谈判有关信息：</p><p>谈判时间：2016年3月11日上午9：30分</p><p>谈判地点：郑州市郑汴路玉凤路交叉口南500米升龙环球大厦C座10层河南腾飞工程造价咨询有限公司会议室</p><p>其他有关事项：各参与本次招标采购的投标人需派代表参加谈判会议。</p><p>七、联系方式：</p><p>招 标 人：郑州市二七区疾病预防控制中心</p><p>联 系 人：吕老师</p><p>联系电话：0371-55630059</p><p>代理机构：河南腾飞工程造价咨询有限公司</p><p>联 系 人：刘先生</p><p>联系电话：0371-55037391   55037390 </p><p>八、公告发布媒介 ：</p><p>本次谈判公告同时在《____》、《河南省政府采购网》、《河南招标采购综合网》《二七区政府采购网》等相关媒体网站同时发布</p><p></p><p></p><p>2016年3月7日</p><div></div></table> </div> </div>
</div>
<!--相关招标信息!-->
<div class="title-box">
<span>相关招标信息</span>
</div>
<div class="related_news">
<div class="left">
<div class="tab_title"><em></em><a href="/zhaobiao/yugao/" target="_blank">招标预告</a><em></em></div>
<ul>
<li><a href="http://www.17350.com/zhaobiao/yugao/43931.html" target="_blank">关于拟采用单一来源采购方式采购疫苗冷藏车项目的征求意见公示</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/yugao/42841.html" target="_blank">泰山景区疾控中心医药冷藏车采购项目政府采购需求公示</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/yugao/40794.html" target="_blank">关于货车型疫苗冷藏车单一来源采购专家论证意见</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/yugao/38126.html" target="_blank">信宜市农业局购置疫苗专用冷藏车采购计划</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/yugao/36730.html" target="_blank">惠州市疾病预防控制中心惠州市疾病预防控制中心疫苗冷藏车采购采购计划</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/yugao/36360.html" target="_blank">夹江县疾病预防控制中心关于拟采用单一来源采购方式采购疫苗冷藏车的征求意见公示</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/yugao/29647.html" target="_blank">惠州市疾病预防控制中心惠州市疾病预防控制中心疫苗冷藏车采购采购计划</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/yugao/28922.html" target="_blank">潮州市潮安区疾病预防控制中心潮州市潮安区疾病预防控制中心免疫规划冷藏车采购项目采购计划</a><span class="f_right">11-30</span></li>
</ul>
</div>
<div class="right">
<div class="tab_title"><em></em><a href="/zhaobiao/gonggao/" target="_blank">招标公告</a><em></em></div>
<ul>
<li><a href="http://www.17350.com/zhaobiao/gonggao/48036.html" target="_blank">财购网竞[PNCGZX2017-(C)002]冷链运输车网上竞价公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/gonggao/48035.html" target="_blank">福建省金福集团公司福建省公安厅“警官之家”大宗物资采购项目竞争性谈判公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/gonggao/48019.html" target="_blank">2017年塔里木项目部七辆生产值班车及一辆冷藏车运输服务招标公告（二次）</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/gonggao/47979.html" target="_blank">四川省泸州市江阳区疾控中心疫苗冷藏车单一来源采购公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/gonggao/47926.html" target="_blank">先声麦得津药品冷藏车项目招标公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/gonggao/47924.html" target="_blank">四川省德阳市旌阳区疾病预防控制中心疫苗冷藏车采购询价采购公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/gonggao/47884.html" target="_blank">江阳区疾控中心—疫苗冷藏车</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/gonggao/47857.html" target="_blank">武警8661部队副食品招标公告</a><span class="f_right">11-30</span></li>
</ul>
</div>
<div class="left">
<div class="tab_title"><em></em><a href="/zhaobiao/biangeng/" target="_blank">招标变更</a><em></em></div>
<ul>
<li><a href="http://www.17350.com/zhaobiao/biangeng/43849.html" target="_blank">寿阳县畜牧兽医局购疫苗冷藏车变更公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/biangeng/43709.html" target="_blank">江苏建协工程咨询有限公司关于2017年食品安全抽检项目的更正公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/biangeng/39314.html" target="_blank">安阳市殷都区疾病预防控制中心所需疫苗冷藏车项目暂停公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/biangeng/38603.html" target="_blank">前郭县疾病预防控制中心购置疫苗冷藏车项目（二次）采购任务取消公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/biangeng/38498.html" target="_blank">前郭县疾病预防控制中心购置疫苗冷藏车项目（二次）采购任务取消公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/biangeng/38225.html" target="_blank">前郭县疾病预防控制中心购置疫苗冷藏车项目（二次）变更公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/biangeng/38189.html" target="_blank">省:吉林:前郭县疾病预防控制中心购置疫苗冷藏车项目（二次）变更公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/biangeng/37924.html" target="_blank">蕲春县疾病预防控制中心疫苗冷藏车采购项目补充说明</a><span class="f_right">11-30</span></li>
</ul>
</div>
<div class="right">
<div class="tab_title"><em></em><a href="/zhaobiao/zhongbiao/" target="_blank"> 中标结果</a><em></em></div>
<ul>
<li><a href="http://www.17350.com/zhaobiao/zhongbiao/48037.html" target="_blank">吉林市船营区疾病预防控制中心疫苗冷藏车采购项目中标公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/zhongbiao/47925.html" target="_blank">四川省德阳市旌阳区疾病预防控制中心疫苗冷藏车采购询价结果公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/zhongbiao/47642.html" target="_blank">泰山景区疾控中心医药冷藏车采购项目政府采购合同公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/zhongbiao/47623.html" target="_blank">东营区疾病预防控制中心疫苗运输用面包式冷藏车政府采购项目废标公告（第二次）</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/zhongbiao/47593.html" target="_blank">泰山景区疾控中心医药冷藏车采购项目合同公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/zhongbiao/47563.html" target="_blank">珠海市疾病预防控制中心冷藏车采购项目废标公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/zhongbiao/47534.html" target="_blank">珠海市疾病预防控制中心冷藏车采购项目招标失败公告</a><span class="f_right">11-30</span></li>
<li><a href="http://www.17350.com/zhaobiao/zhongbiao/47276.html" target="_blank">宜昌市猇亭区农林水局病死畜禽无害化处理收集中心工程(冷藏车采购)项目</a><span class="f_right">11-30</span></li>
</ul>
</div>
</div>
</div>
<div class="content">
<!--相关报价!-->
<div class="zb_title">
<a href="/product/lcc/" target="_blank">冷藏车报价</a>
</div>
<ul class="right_list">
<li>
<a href="https://www.17350.com/lengcangche/4757.html" target="_blank" title="重汽汕德卡C5H国五7米冷藏车">
<img alt="重汽汕德卡C5H国五7米冷藏车" class="f_left" src="http://upload.17350.com/2020/1221/20201221032753369.jpg" title="重汽汕德卡C5H国五7米冷藏车"/>
</a>
<a href="https://www.17350.com/lengcangche/4757.html" target="_blank" title="重汽汕德卡C5H国五7米冷藏车">重汽汕德卡C5H国五7米冷藏车</a>
<a class="zixun_button" href="http://p.qiao.baidu.com//im/index?siteid=7266564&amp;ucid=9843870" rel="nofollow" target="_blank">询问底价</a> </li>
<li>
<a href="https://www.17350.com/lengcangche/4673.html" target="_blank" title="现代泓图300国五4.08米冷藏车">
<img alt="现代泓图300国五4.08米冷藏车" class="f_left" src="http://upload.17350.com/2020/1205/20201205085615423.jpg" title="现代泓图300国五4.08米冷藏车"/>
</a>
<a href="https://www.17350.com/lengcangche/4673.html" target="_blank" title="现代泓图300国五4.08米冷藏车">现代泓图300国五4.08米冷藏车</a>
<span class="f_red">优惠价:11.8~13.8万元</span> </li>
<li>
<a href="https://www.17350.com/lengcangche/4650.html" target="_blank" title="大运祥龙国五4.05米冷藏车(白色)">
<img alt="大运祥龙国五4.05米冷藏车(白色)" class="f_left" src="http://upload.17350.com/2020/1123/20201123114825863.jpg" title="大运祥龙国五4.05米冷藏车(白色)"/>
</a>
<a href="https://www.17350.com/lengcangche/4650.html" target="_blank" title="大运祥龙国五4.05米冷藏车(白色)">大运祥龙国五4.05米冷藏车(白色)</a>
<span class="f_red">优惠价:13.2~15.2万元</span> </li>
<li>
<a href="https://www.17350.com/lengcangche/4649.html" target="_blank" title="解放JH6前四后八国五9.4米冷藏车">
<img alt="解放JH6前四后八国五9.4米冷藏车" class="f_left" src="http://upload.17350.com/2020/1123/20201123115115226.jpg" title="解放JH6前四后八国五9.4米冷藏车"/>
</a>
<a href="https://www.17350.com/lengcangche/4649.html" target="_blank" title="解放JH6前四后八国五9.4米冷藏车">解放JH6前四后八国五9.4米冷藏车</a>
<a class="zixun_button" href="http://p.qiao.baidu.com//im/index?siteid=7266564&amp;ucid=9843870" rel="nofollow" target="_blank">询问底价</a> </li>
<li>
<a href="https://www.17350.com/lengcangche/4648.html" target="_blank" title="庆铃五十铃ELF国六4.1米冷藏车">
<img alt="庆铃五十铃ELF国六4.1米冷藏车" class="f_left" src="http://upload.17350.com/2020/1123/20201123115314375.jpg" title="庆铃五十铃ELF国六4.1米冷藏车"/>
</a>
<a href="https://www.17350.com/lengcangche/4648.html" target="_blank" title="庆铃五十铃ELF国六4.1米冷藏车">庆铃五十铃ELF国六4.1米冷藏车</a>
<span class="f_red">优惠价:14.2~16.2万元</span> </li>
<li>
<a href="https://www.17350.com/lengcangche/4647.html" target="_blank" title="跃进小福星排半国六3.55米冷藏车">
<img alt="跃进小福星排半国六3.55米冷藏车" class="f_left" src="http://upload.17350.com/2020/1123/20201123115539598.jpg" title="跃进小福星排半国六3.55米冷藏车"/>
</a>
<a href="https://www.17350.com/lengcangche/4647.html" target="_blank" title="跃进小福星排半国六3.55米冷藏车">跃进小福星排半国六3.55米冷藏车</a>
<span class="f_red">优惠价:6.7~8.7万元</span> </li>
</ul>
<!--车型图库!-->
<div class="zb_title">
<a href="/product/lcc/" target="_blank">冷藏车图片</a>
</div>
<ul class="right_list">
<li>
<a href="https://www.17350.com/photo/lengcangche/3781.html" target="_blank" title="福田欧航R国五6.6米冷藏车图片">
<img alt="福田欧航R国五6.6米冷藏车图片" class="f_left" src="http://upload.17350.com/2021/0103/20210103012426700.jpg" title="福田欧航R国五6.6米冷藏车图片"/>
</a>
<a href="https://www.17350.com/photo/lengcangche/3781.html" target="_blank" title="福田欧航R国五6.6米冷藏车图片">福田欧航R国五6.6米冷藏车</a>
</li>
<li>
<a href="https://www.17350.com/photo/lengcangche/3780.html" target="_blank" title="飞碟缔途DX国六柴油蓝牌3.8米冷藏车图片">
<img alt="飞碟缔途DX国六柴油蓝牌3.8米冷藏车图片" class="f_left" src="http://upload.17350.com/2021/0103/20210103114153199.jpg" title="飞碟缔途DX国六柴油蓝牌3.8米冷藏车图片"/>
</a>
<a href="https://www.17350.com/photo/lengcangche/3780.html" target="_blank" title="飞碟缔途DX国六柴油蓝牌3.8米冷藏车图片">飞碟缔途DX国六柴油蓝牌3.8米冷藏车</a>
</li>
<li>
<a href="https://www.17350.com/photo/lengcangche/3762.html" target="_blank" title="江淮骏铃V6国六蓝牌冷藏车图片">
<img alt="江淮骏铃V6国六蓝牌冷藏车图片" class="f_left" src="http://upload.17350.com/2020/1228/20201228095221568.jpg" title="江淮骏铃V6国六蓝牌冷藏车图片"/>
</a>
<a href="https://www.17350.com/photo/lengcangche/3762.html" target="_blank" title="江淮骏铃V6国六蓝牌冷藏车图片">江淮骏铃V6国六蓝牌冷藏车</a>
</li>
<li>
<a href="https://www.17350.com/photo/lengcangche/3759.html" target="_blank" title="重汽豪瀚N5G国五6.2米冷藏车图片">
<img alt="重汽豪瀚N5G国五6.2米冷藏车图片" class="f_left" src="http://upload.17350.com/2020/1226/20201226100742475.jpg" title="重汽豪瀚N5G国五6.2米冷藏车图片"/>
</a>
<a href="https://www.17350.com/photo/lengcangche/3759.html" target="_blank" title="重汽豪瀚N5G国五6.2米冷藏车图片">重汽豪瀚N5G国五6.2米冷藏车</a>
</li>
<li>
<a href="https://www.17350.com/photo/lengcangche/3743.html" target="_blank" title="庆铃五十铃KV100国六4.075米冷藏车图片">
<img alt="庆铃五十铃KV100国六4.075米冷藏车图片" class="f_left" src="http://upload.17350.com/2020/1218/20201218111619240.jpg" title="庆铃五十铃KV100国六4.075米冷藏车图片"/>
</a>
<a href="https://www.17350.com/photo/lengcangche/3743.html" target="_blank" title="庆铃五十铃KV100国六4.075米冷藏车图片">庆铃五十铃KV100国六4.075米冷藏车</a>
</li>
<li>
<a href="https://www.17350.com/photo/lengcangche/3742.html" target="_blank" title="跃进福运S80国六3.26米冷藏车图片">
<img alt="跃进福运S80国六3.26米冷藏车图片" class="f_left" src="http://upload.17350.com/2020/1218/20201218110043754.jpg" title="跃进福运S80国六3.26米冷藏车图片"/>
</a>
<a href="https://www.17350.com/photo/lengcangche/3742.html" target="_blank" title="跃进福运S80国六3.26米冷藏车图片">跃进福运S80国六3.26米冷藏车</a>
</li>
</ul>
<div class="news_list cf">
<div class="zb_title"><a href="/news/lcchy_1.html" target="_blank">冷藏车动态</a></div>
<ul>
<!--行业动态-->
<li>
<a class="f_red" href="/news/lcchy_1.html" target="_blank" title="冷藏车动态"> 【行业动态】</a>
<a href="https://www.17350.com/news/65512.html" target="_blank" title="第337批次公告之冷藏车统计分析">第337批次公告之冷藏车统计分析</a>
</li>
<li>
<a class="f_red" href="/news/lcchy_1.html" target="_blank" title="冷藏车动态"> 【行业动态】</a>
<a href="https://www.17350.com/news/65384.html" target="_blank" title="2021亚太生鲜配送及冷链技术设备展览会">2021亚太生鲜配送及冷链技术设备展览会</a>
</li>
<!--专汽评测-->
<li>
<a class="f_red" href="/news/lccpc_1.html" target="_blank" title="冷藏车评测"> 【专汽评测】</a>
<a href="https://www.17350.com/news/65707.html" target="_blank" title="宽敞舒适 四川现代盛图单排3.3米轴距底盘评测篇">宽敞舒适 四川现代盛图单排3.3米轴距底盘评测篇</a>
</li>
<li>
<a class="f_red" href="/news/lccpc_1.html" target="_blank" title="冷藏车评测"> 【专汽评测】</a>
<a href="https://www.17350.com/news/65700.html" target="_blank" title="高端神秘 陕汽轩德翼9国五4.2米冷藏车评测之上装篇">高端神秘 陕汽轩德翼9国五4.2米冷藏车评测之上装篇</a>
</li>
<!--促销导购-->
<li>
<a class="f_red" href="/news/lcccx_1.html" target="_blank" title="冷藏车促销"> 【促销导购】</a>
<a href="https://www.17350.com/news/65749.html" target="_blank" title="​高端大气的东风天龙KL前四后八国六9.5米冷藏车能入你的眼吗？">​高端大气的东风天龙KL前四后八国六9.5米冷藏车能入你的眼吗？</a>
</li>
<li>
<a class="f_red" href="/news/lcccx_1.html" target="_blank" title="冷藏车促销"> 【促销导购】</a>
<a href="https://www.17350.com/news/65679.html" target="_blank" title="江淮帅铃Q7排半国六5.2米冷藏车多少钱一辆">江淮帅铃Q7排半国六5.2米冷藏车多少钱一辆</a>
</li>
<!--专汽话题-->
<li>
<a class="f_red" href="/news/llys_1.html" target="_blank" title="冷链运输"> 【专汽话题】</a>
<a href="https://www.17350.com/news/65656.html" target="_blank" title="在未来的专用车中，冷藏车的销量将进入稳步增长阶段">在未来的专用车中，冷藏车的销量将进入稳步增长阶段</a>
</li>
<li>
<a class="f_red" href="/news/llys_1.html" target="_blank" title="冷链运输"> 【专汽话题】</a>
<a href="https://www.17350.com/news/65620.html" target="_blank" title="冷藏车最新的安全技术要求已下达">冷藏车最新的安全技术要求已下达</a>
</li>
<!--用车养车-->
<li>
<a class="f_red" href="/news/lccyh_1.html" target="_blank" title="冷藏车养护"> 【用车养车】</a>
<a href="https://www.17350.com/news/65752.html" target="_blank" title="轮胎上面的小铁块你知道是什么吗？">轮胎上面的小铁块你知道是什么吗？</a>
</li>
<li>
<a class="f_red" href="/news/lccyh_1.html" target="_blank" title="冷藏车养护"> 【用车养车】</a>
<a href="https://www.17350.com/news/65724.html" target="_blank" title="什么样的车叫非法改装车？">什么样的车叫非法改装车？</a>
</li>
<!--政策法规-->
<li>
<a class="f_red" href="/news/lcczc_1.html" target="_blank" title="冷藏车政策法规"> 【政策法规】</a>
<a href="https://www.17350.com/news/65623.html" target="_blank" title="征求对强制性国家标准《道路运输 易腐食品与生物制品 冷藏车安全要求">征求对强制性国家标准《道路运输 易腐食品与生物制品 冷藏车安全要求</a>
</li>
<li>
<a class="f_red" href="/news/lcczc_1.html" target="_blank" title="冷藏车政策法规"> 【政策法规】</a>
<a href="https://www.17350.com/news/65597.html" target="_blank" title="国六车（机）型环保信息公开汇总 （2020年11月28日-12月04日）">国六车（机）型环保信息公开汇总 （2020年11月28日-12月04日）</a>
</li>
</ul>
<div class="page_more"><a href="/lengcangche/" target="_blank">更多冷藏车相关内容&gt;&gt;</a></div>
</div>
</div>
</div>
<div class="title_coloumn"></div>
﻿<!--底部文字链接!-->
<div class="container filed cf ">
<div class="container_1200 m_center cf m_t20">
<div class="block tlink">
<ul>
<p>关于我们</p>
<li>
<a href="https://www.17350.com/about/about.html" rel="nofollow" target="_blank">专汽网简介</a>
<a href="https://www.17350.com/about/advantage.html" rel="nofollow" target="_blank">专汽网优势</a>
<a href="https://www.17350.com/about/environment.html" rel="nofollow" target="_blank">专汽网环境</a>
<a href="https://www.17350.com/about/contact.html" rel="nofollow" target="_blank">联系我们</a>
<a href="https://www.17350.com/about/yszc.html" rel="nofollow" target="_blank">隐私政策</a>
<a href="https://www.17350.com/about/yhxy.html" rel="nofollow" target="_blank">用户协议</a>
</li>
</ul>
</div>
<!--
      <div class="block tlink">
        <ul>
		   <p>优惠活动</p>
           <li><a href="https://www.17350.com/shouhuochetuan.html" target="_blank" rel="nofollow">售货车团购</a></li>
           <li><a href="https://www.17350.com/qingzhangche.html" target="_blank" rel="nofollow">清障车团购</a></li>
           <li><a href="https://www.17350.com/fangche.html" target="_blank" rel="nofollow">房车团购</a></li>
		   <li><a href="https://www.17350.com/dingzhi.html" target="_blank" rel="nofollow">售货车改装</a></li>
        </ul>
      </div>
	  !-->
<div class="block tlink">
<ul>
<p>支付方式</p>
<li>
<a href="https://www.17350.com/about/fenqifukuan.html" rel="nofollow" target="_blank">分期付款</a>
<a href="https://www.17350.com/about/huodaofukuan.html" rel="nofollow" target="_blank">货到付款</a>
<a href="https://www.17350.com/about/zhuanzhang.html" rel="nofollow" target="_blank">公司转帐</a>
<a href="https://www.17350.com/about/process.html" rel="nofollow" target="_blank">购车流程</a>
</li>
</ul>
</div>
<div class="block tlink">
<ul>
<p>配送方式</p>
<li>
<a href="https://www.17350.com/about/yonghuziti.html" rel="nofollow" target="_blank">用户自提</a>
<a href="https://www.17350.com/about/songhuoshangmen.html" rel="nofollow" target="_blank">送货上门</a>
<a href="https://www.17350.com/about/haiwaipeisong.html" rel="nofollow" target="_blank">海外配送</a>
</li>
</ul>
</div>
<div class="block tlink">
<ul>
<p>新闻资讯</p>
<li>
<a href="https://www.17350.com/news/gongsi_1.html" target="_blank">公司新闻</a>
<a href="https://www.17350.com/news/hangye_1.html" target="_blank">行业动态</a>
<a href="https://www.17350.com/news/pingce_1.html" target="_blank">专汽评测</a>
<a href="https://www.17350.com/news/cuxiao_1.html" target="_blank">促销导购</a>
<a href="https://www.17350.com/news/huati_1.html" target="_blank">专汽话题</a>
<a href="https://www.17350.com/news/yangche_1.html" target="_blank">用车养车</a>
<a href="https://www.17350.com/jiedu/" target="_blank">汽车公告</a>
<a href="https://www.17350.com/ranyou/" target="_blank">燃油公告</a>
<a href="https://www.17350.com/newGgPublicity/" target="_blank">新公告公示</a>
<a href="https://www.17350.com/ggChanges/" target="_blank">公告变更、扩展及勘误</a>
<a href="https://www.17350.com/yingyun/" target="_blank">道路运输车辆营运达标公告</a>
<!--<a href="http://www.17350.com/ranyou/" target="_blank">燃油公告</a>-->
</li>
</ul>
</div>
<div class="block tlink">
<ul>
<p>关注 专汽网 微信</p>
<img alt="专汽网微信" src="http://images.17350.com/images/code1_img.jpg" width="80%"/>
</ul>
</div>
<div class="block tlink">
<ul>
<p>下载 专汽宝 </p>
<img alt="下载 专汽宝" src="http://images.17350.com/images/code_img.jpg" width="80%"/>
</ul>
</div>
</div>
</div><!--版权!-->
<div class="footer">
<p>Copyright © 2014-2021 All Rights Reserved. 湖北专汽网科技有限公司 版权所有</p>
<p>地址：湖北省随州市曾都区交通大道国际汽配城一幢 客服热线：180-0866-5020 </p><a href="http://221.232.141.246/iciaicweb/dzbscheck.do?method=change&amp;id=E2016112900105488" rel="nofollow"><img alt="网络经济主体信息" border="0" dragover="true" src="http://images.17350.com/images/gongshang.png"/></a>
<div style="display:none;">
<script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1255183328'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s95.cnzz.com/z_stat.php%3Fid%3D1255183328' type='text/javascript'%3E%3C/script%3E"));</script>
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?53275f6adb13c71afaf07269f0cadc55";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script></div>
</div>
</body>
</html>
