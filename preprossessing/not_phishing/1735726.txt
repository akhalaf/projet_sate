<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<title>ZZZS - Zavod za zdravstveno zavarovanje Slovenije</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="/webdsg.nsf/favicon.ico" rel="icon" type="image/x-icon"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="ZZZS" name="description"/>
<meta content="ZZZS" name="keywords"/>
<meta content="no-cache" http-equiv="pragma"/>
<meta content="no-cache" http-equiv="Cache-control"/>
<script charset="utf-8" src="/webdsg.nsf/anketa.js" type="text/javascript"></script>
<link charset="utf-8" href="/webdsg.nsf/zzzsM.css" media="all" rel="stylesheet" title="ZZZS normal" type="text/css"/>
<link charset="utf-8" href="/zzzs/internet/zzzs.nsf/vstopgumbi.css" media="all" rel="stylesheet" title="ZZZS normal" type="text/css"/>
<link charset="utf-8" href="/zzzs/internet/zzzs.nsf/vstopgumbiyellow.css" media="all" rel="stylesheet" title="ZZZS normal" type="text/css"/>
<link charset="utf-8" href="/zzzs/internet/zzzs.nsf/vstopgumbired.css" media="all" rel="stylesheet" title="ZZZS normal" type="text/css"/>
<link href="/webdsg.nsf/zzzsX.css" media="all" rel="alternate stylesheet" title="ZZZS big" type="text/css"/>
<!--[if lte IE 6]><link rel='stylesheet' href='/webdsg.nsf/ie6.css' type='text/css' title='ZZZS normal' charset='utf-8' /><![endif]-->
<script charset="windows-1250" src="/zzzs/internet/zzzs.nsf/ver1.2.data.js" type="text/javascript"></script>
<script charset="utf-8" src="/webdsg.nsf/jquery-1.2.6.min.portal.js" type="text/javascript"></script>
<script charset="utf-8" src="/webdsg.nsf/jquery.bgiframe.min.portal.js" type="text/javascript"></script>
<script charset="utf-8" src="/webdsg.nsf/hoverIntent.portal.js" type="text/javascript"></script>
<script charset="utf-8" src="/webdsg.nsf/styleswitch.portal.js" type="text/javascript"></script>
<script charset="utf-8" src="/webdsg.nsf/superfish.portal.js" type="text/javascript"></script>
<script charset="utf-8" src="/webdsg.nsf/navigacija.portal.js" type="text/javascript"></script>
<style media="screen" type="text/css">		.novosti-seznam {			height: 288px;		}	</style>
<script language="JavaScript" type="text/javascript">
<!-- 
document._domino_target = "_self";
function _doClick(v, o, t, h) {
  var form = document._Portal;
  if (form.onsubmit) {
     var retVal = form.onsubmit();
     if (typeof retVal == "boolean" && retVal == false)
       return false;
  }
  var target = document._domino_target;
  if (o.href != null) {
    if (o.target != null)
       target = o.target;
  } else {
    if (t != null)
      target = t;
  }
  form.target = target;
  form.__Click.value = v;
  if (h != null)
    form.action += h;
  form.submit();
  return false;
}
// -->
</script>
</head>
<body bgcolor="#FFFFFF" class="home-page" onload="DrawLevel(0)" text="#000000">
<form action="/zzzs/internet/zzzs.nsf/portal?OpenForm&amp;Seq=1" method="post" name="_Portal">
<input name="__Click" type="hidden" value="0"/></form><div id="body">
<div id="header">
<h1 style="display: block;"><a href="/index.html" title="ZZZS — Zavod za zdravstveno zavarovanje Slovenije">ZZZS — Zavod za zdravstveno zavarovanje Slovenije</a></h1>
<div id="home"><a href="/index.html">prva stran</a></div>
<ul id="font-size">
<li id="big"><a class="styleswitch" href="#" rel="ZZZS big">A</a></li>
<li id="normal"><a class="styleswitch" href="#" rel="ZZZS normal">a</a></li>
</ul>
<ul id="lang">
<li id="sl"><a href="/index.html"><span>slovenski</span></a></li>
<li id="en"><a href="/indexeng.html"><span>english</span></a></li>
</ul>
<form accept-charset="utf-8" action="/zzzs/internet/zzzs.nsf/iskanje?CreateDocument" id="searchForm" method="post">
<fieldset><legend>iskalnik</legend>
<input id="q" name="Query" size="60" type="text" value=""/>
<p><input type="submit" value="Continue →"/></p>
</fieldset>
</form>
<p id="hImg"><img alt="Solidarnost" height="94" src="/webdsg.nsf/solidarnost.jpg" width="156"/></p>
</div>
<div id="navigation">
<div id="navH"><img alt="" height="32" src="/webdsg.nsf/ajax-loader.gif" width="32"/></div>
<ul id="kontakt-nav">
<li id="enote"><a href="/imenik">Kontakt z ZZZS </a>    </li>
<li id="eposta"><a href="http://www.zzzs.si/zzzs/internet/zzzs.nsf/o/DE0192AD84AC1751C12572C2003A4C71">Elektronska pošta ZZZS</a></li>
<!--			<li id="lokacije"><a href="http://www.zzzs.si/zzzs/internet/zzzs.nsf/o/833174C0ADBE815DC125778500299EC7">Lokacije samopostrežnih terminalov</a></li>  -->
<li id="izvajalci"><a href="http://www.zzzs.si/Izvajalci">Izvajalci zdravstvenih storitev</a></li>
<!--			<li id="sluzba"><a href="/">Služba za poslovanje s karticami<br>(01) 30-77-466</a></li> -->
<li id="avtomat"><a href="/#">Avtomatski telefonski odzivnik<br/>(01) 30-77-300</a></li>
<li id="border-b"> </li>
</ul>
<br class="clear"/>
<div class="clearfix" id="info-uradne-ure">
<div class="info-green-box">
<div class="ure">
<p><strong>Uradne ure ZZZS:</strong></p>
<p><strong> pon:</strong> 8h – 12h<br/>in 13h – 15h</p>
<p><strong> tor:</strong> 8h – 12h<br/>in 13h – 15h</p>
<p><strong> sre:</strong> 8h – 12h<br/>in 13h – 17h</p>
<p><strong> pet:</strong> 8h – 13h</p>
</div>
<p>Ob četrtkih ni uradnih ur. </p>
<p class="info-green-text"><a href="https://www.zzzs.si/zzzs/info/index.nsf">Količnik valorizacije osnove za bolniško</a></p>
</div>
<div class="info-green-box130">
<div class="poveza130">
<!--                                                <a href="/ZZZS/info/gradiva.nsf/o/D656EBBA0D34D7ABC1258449001F6BA1?OpenDocument"><img src="/webdsg.nsf/130_let_ZZ.jpg"  alt="EUKZZ"></a> --!>
                                               </div>
	 	</div>
	 </div> 

	 </div> <!-- #navigation --><br/>
<style type="text/css">
.content-row9 {
    position: relative;
    width: 100%;
    margin: 0 0 10px 0;
}
.banner9 {
    margin: 0 0 1px 0;
}
.poveza{
		 		 padding: 1px 2px 8px 9px;
}
.info-green-box130 {
    background: #FFFFFF;
    width: 110px;
    padding: 10px;
    margin: 0 5px;
}
.poveza130{
		 		 padding: 1px 2px 8px 0px;
}
.povezaPK{
		 		padding: 12px 2px 8px 9px;
}
.button {

		 		 width: 210px;  
		 		 display: inline-block;
		 		 line-height: 1;
		 		 height: 45px;
		 		 padding: 1px 2px 2px 2px;
		 		  text-decoration: none;
				  font: bold 14px Arial, Verdana, sans-serif;
		 		  color: #fff;
/*		 		  background-color: #AB0000;*/
		 		  background-color: #C33;
		 		  -moz-border-radius: 5px;
		 		  -webkit-border-radius: 5px;
		 		  -khtml-border-radius: 5px;
		 		  border-radius: 5px;
				  position:absolute;
				  top: 140px;
				  left: 15px;
}
.button:hover { 
background: #FF0000;
/*background: #EAEAEA;*/
}
input.button, button.button {
		 		  border: 0px none;
}
.buttonizv {

		 		 width: 210px;  
		 		 display: inline-block;
		 		 line-height: 1;
		 		 height: 60px;
		 		 padding: 1px 2px 2px 2px;
		 		  text-decoration: none;
				  font: bold 14px Arial, Verdana, sans-serif;
		 		  color: #fff;
/*		 		  background-color: #AB0000;*/
		 		  background-color: #C33;
		 		  -moz-border-radius: 5px;
		 		  -webkit-border-radius: 5px;
		 		  -khtml-border-radius: 5px;
		 		  border-radius: 5px;
				  position:absolute;
				  top: 140px;
				  left: 15px;
}
.buttonizv:hover { 
background: #FF0000
/*background: #EAEAEA;*/
}
input.buttonizv, buttonizv.buttonizv {
		 		  border: 0px none;
}
</style>
<div id="main-content" style="display: block">
<div class="content-row9 clearfix" id="box3">
<div class="column red box">
<h2><span>Storitve za zavarovane osebe</span></h2>
<br/>
<p>
<a class="multi-line-button-red" href="https://zavarovanec.zzzs.si " style="width:210px">
<span class="title">Vstop v spletne strani</span>
<span class="subtitle">za zavarovane osebe</span>
</a>
</p>
<br/>
<div class="poveza"><a href="https://www.zzzs.si/tujina"><img alt="EUKZZ" src="/webdsg.nsf/eukzz9.gif"/></a></div>
<div class="poveza"><a href="https://zavarovanec.zzzs.si/wps/portal/portali/azos/e_storitve_zzzs/narocanje_kzz"><img alt="Ste izgubili kartico zdravstvenega zavarovanja?" src="/webdsg.nsf/narockzz9.gif"/></a></div>
<div class="poveza"><a href="/COVID-19 "><img alt="Koronavirus (COVID-19)" height="97" src="/webdsg.nsf/koronavirus.png" width="222"/></a></div>
</div>
<div class="column orange box">
<h2><span>Storitve za izvajalce zdravstvenih storitev</span></h2>
<br/>
<p>
<a class="multi-line-button" href="https://partner.zzzs.si" style="width:210px">
<span class="title">Vstop v spletne strani za </span>
<span class="title">izvajalce zdravstvenih storitev in </span>
<span class="title">dobavitelje medicinskih pripomočkov</span>
</a>
</p>
<div class="povezaPK"><a href="/ZZZS/info/egradiva.nsf/o/79042846984E856AC12574DE00397849"><img alt="Navodilo o profesionalni kartici in pooblastilih" src="/webdsg.nsf/narocanjePK.gif"/></a></div>
</div>
<div class="column green box">
<h2><span>Storitve za zavezance za prijavo in za prispevek</span></h2>
<br/>
<p>
<a class="multi-line-button-yellow" href="https://zavezanec.zzzs.si" style="width:210px">
<span class="title barvazav">Vstop v spletne strani</span>
<span class="title barvazav">za zavezance za prijavo</span>
<span class="title">in za prispevek</span>
</a>
</p>
<br/>
<div class="poveza"><a href="https://zavezanec.zzzs.si/wps/portal/portali/azap/e-poslovanje/prijava-oseb-v-obvezna-socialna-zavarovanja"><img alt="Ste izgubili kartico zdravstvenega zavarovanja?" height="97" src="/webdsg.nsf/urejzavposlsub2.png" width="222"/></a></div>
<div class="poveza"><a href="https://zavezanec.zzzs.si/wps/portal/portali/azap/e-poslovanje/vlaganje-refund-zahtevkov-za-nadomestila-plac"><img alt="Imam urejeno zavarovanje?" height="97" src="/webdsg.nsf/elevlagref2.png" width="222"/></a></div>
<div class="poveza"><a href="https://zavezanec.zzzs.si/wps/portal/portali/azap/e-poslovanje/pridobitev-elektronskih-potrdil-o-upraviceni-zadrz"><img alt="Pridobitev elektronskih potrdil o upravičeni zadržanosti od dela - eBOL" height="97" src="/webdsg.nsf/eBOL.png" width="222"/></a></div>
</div>
</div> <!-- #box3 -->
<br class="clear"/>
<style>
#naslovZZZScorona { 
height: 100px;
width: 750px;
background-image: url("/webdsg.nsf/footer.jpg");
background-repeat: no-repeat;
clear: both;
margin: 0 0 20px 30px;
border-bottom: 1px solid #ccc;
text-align: center;
}
#footer-nav { 
    padding: 2px 0;
    line-height: 14px;
}
a.obv:link {color: #ff8080;
font-weight: bold;
text-decoration: none;}
a.obv:visited {color:#ff8080;}
a.obv:hover {color:#ff0000;}
</style>
<!-- Dodano 20110405 -->
<div id="naslovZZZScorona">
<div id="footer-nav">
<p>
<b>Kontakt: Zavod za zdravstveno zavarovanje Slovenije</b>
</p>
<p><a href="/imenik">Telefoni, faksi, e-pošte.</a>
</p>
<p><a href="/zzzs/internet/zzzs.nsf/o/A95C4D2D26F6062BC1256E8D002B5FB3">Katalog informacij javnega značaja</a>
</p>
<p></p><div class="coro"><a class="obv" href="/zzzs/internet/zzzs.nsf/o/2086720C7AADDEFBC125852D002F1987">Zaradi koronavirusa (COVID-19) stranke naprošamo, da omejijo svoje obiske in urejajo zadeve z ZZZS zlasti preko spletne strani, e-pošte, navadne pošte ali po telefonu.<img src="/webdsg.nsf/footer-nav-bullet.gif"/></a></div>
</div>
</div>
<br class="clear"/>
<!-- Dodano 20110405 -->
<div class="content-row9 clearfix">
<div class="column"><script language="JavaScript">
function pozenijoze(){
   document.anketa.submit();
   setCookie("anketa20151019",20151019,356);
   showResultsR(20151019);
   return true;
}
</script><div class="anketa">
<h2>Anketa</h2>
<p>Ali vaš delodajalec izvaja aktivnosti promocije zdravja na delovnem mestu (ZVZD-1) ?</p><form accept-charset="utf-8" action="/zzzs/info/Anketa.nsf/anketawr?CreateDocument" id="form-anketa" method="post" name="anketa"><div><label><input checked="checked" name="Query" type="radio" value="1"/> Da, sistematično.</label></div>
<div><label><input name="Query" type="radio" value="2"/> Delno.</label></div>
<div><label><input name="Query" type="radio" value="3"/> Ne.</label></div>
<p class="func-links"><a href="javascript:pozenijoze();"><strong>pošlji</strong></a> | <a href="javascript:showResultsR(20151019);">rezultati</a></p>
</form>
</div>
<div class="banner9"><a href="https://zavarovanec.zzzs.si/wps/portal/portali/azos/vkljucitev_ozz_kzz/kzz"><img alt="Zamenjava KZZ 2" height="102" src="/webdsg.nsf/kzz2gen.png" width="241"/></a></div>
<div class="banner9"><a href="https://zavarovanec.zzzs.si/wps/portal/portali/azos/e_storitve_zzzs/status_zavarovanja"><img alt="Imam urejeno zavarovanje?" height="102" src="/webdsg.nsf/urejzav.jpg" width="241"/></a></div>
<div class="banner9"><a href="https://zavarovanec.zzzs.si/wps/portal/portali/azos/e_storitve_zzzs"><img alt="Vpogled v moje podatke" height="102" src="/webdsg.nsf/vpogled3.png" width="241"/></a></div>
<div class="banner9"><a href="http://www.nkt-z.si/wps/portal/nktz/home/abroad/planned/"><img alt="EUKZZ" height="101" src="/webdsg.nsf/nkt.jpg" width="240"/></a></div>
<div class="banner9">
<a href="https://zavarovanec.zzzs.si/wps/portal/portali/azos/pravice_zdravstvenih_storitev/cakalne_dobe"><img alt="Čakalne dobe in ordinacijski časi " height="101" src="/webdsg.nsf/banner3.jpg" width="240"/></a></div>
</div>
<div class="column">
<div class="novosti">
<h2>novosti</h2>
<div class="novosti-seznam">
<p><a href="/ZZZS/internet/zzzs.nsf/webnovosti/6CBB65860BC6495BC125852200390CD3?OpenDocument"><strong>11. do 12. marec 2021</strong><br/>6. Mednarodni kongres medicinskih izvedencev Slovenije</a></p> <p><a href="/ZZZS/internet/zzzs.nsf/webnovosti/A929629D02AC048BC125861C00484F49?OpenDocument"><strong>29. januar 2021</strong><br/>Uveljavitev novih najvišjih priznanih vrednosti za skupine medsebojno zamenljivih zdravil in za terapevtske skupine zdravil</a></p> <p><a href="/ZZZS/internet/zzzs.nsf/webnovosti/0F6C92F6F9A562DFC1258656002FB123?OpenDocument"><strong>27. januar 2021</strong><br/>Rok za oddajo izjav o interesu za najem dveh pisarn na Vrhniki</a></p> <p><a href="/ZZZS/internet/zzzs.nsf/webnovosti/E8BA1EDF367CCF11C12586390033A88C?OpenDocument"><strong>15. januar 2021</strong><br/>Rok za oddajo ponudbe na javnem razpisu za program izvajanja izdaje in izposoje medicinskih pripomočkov zavarovanim osebam</a></p> <p><a href="/ZZZS/internet/zzzs.nsf/webnovosti/EA42C680030532E4C1258646002CF128?OpenDocument"><strong>1. januar 2021</strong><br/>Izstop Združenega kraljestva Velike Britanije in Severne Irske iz Evropske unije in Evropske skupnosti za atomsko energijo - brexit</a></p> <p><a href="/ZZZS/internet/zzzs.nsf/webnovosti/7F3DFEA6BC36D28EC1258641003ADFED?OpenDocument"><strong>11. december 2020</strong><br/>Spremenjeno poslovanje ZZZS v upravnih zadevah</a></p> <p><a href="/ZZZS/internet/zzzs.nsf/webnovosti/69DBEE442483FF84C12586370039C78B?OpenDocument"><strong>10. december 2020</strong><br/>Javna razprava o predlogu Sprememb in dopolnitev Pravil obveznega zdravstvenega zavarovanja</a></p> <p><a href="/ZZZS/internet/zzzs.nsf/webnovosti/CF50C5F10905C9D0C125863A0032C111?OpenDocument"><strong>10. december 2020</strong><br/>Javna razprava o predlogu Sklepa o zdravstvenih stanjih in drugih pogojih za upravičenost do medicinskih pripomočkov iz obveznega zdravstvenega zavarovanja</a></p> <p><a href="/ZZZS/internet/zzzs.nsf/webnovosti/392D3498541C2EC1C125863800350AEB?OpenDocument"><strong>28. november 2020</strong><br/>Pravica do sobivanja ob hospitaliziranem otroku (velja od 28.11.2020 do 31.12.2021)</a></p> <p><a href="/ZZZS/internet/zzzs.nsf/webnovosti/E62703667F31A842C1258626002D485A?OpenDocument"><strong>20 november 2020</strong><br/>Spremembe pri izdaji medicinskih pripomočkov in delu imenovanih zdravnikov in zdravstvene komisije ZZZS</a></p>
</div>
<a class="arhiv" href="/zzzs/internet/zzzs.nsf/webnovosti">Arhiv</a>
</div>
<br/><br/><br/>
<div class="banner9"><a href="https://www.zzzs.si/zdravje/index.html"><img alt="Zdravje" src="/webdsg.nsf/banner1.jpg"/></a></div>
<div class="banner9"><a href="https://www.zzzs.si/moji_zobje/index.html"><img alt="Moji zobje" src="/moji_zobje/images/banner-moji-zobje.jpg"/></a></div>
</div>
<div class="column">
<div class="novinarsko-sredisce">
<h2>Novinarsko središče</h2>
<div class="novosti-seznam">
<p><a href="/ZZZS/info/gradiva.nsf/o/B4ACBAC63A8FB651C125863F004C3CF2?OpenDocument"><strong>Sporočilo za javnost</strong><br/>15.12.2020 - 10. redna seja Skupščine ZZZS (sprejet Rebalans finančnega načrta za leto 2020 in Finančni načrt za leto 2021)</a></p> <p><a href="/ZZZS/info/gradiva.nsf/o/D32A2398D56BF9BBC125860E0036B0CE?OpenDocument"><strong>Sporočilo za javnost</strong><br/>27.10.2020 - Priznanje eNagrada 2020 za najboljšo elektronsko storitev podeljena ZZZS za nacionalno uvedbo elektronskega bolniškega lista</a></p> <p><a href="/ZZZS/info/gradiva.nsf/o/0585C3EABDED890AC1258603002E51DE?OpenDocument"><strong>Sporočilo za javnost</strong><br/>16.10.2020 - Spremenjeno poslovanje ZZZS s strankami zaradi nalezljive bolezni SARS-CoV-2 (COVID-19)</a></p> <p><a href="/ZZZS/info/gradiva.nsf/o/3496A0F1BC11DB70C12585F90021B813?OpenDocument"><strong>Sporočilo za javnost</strong><br/>06.10.2020 - Uspešna nacionalna uvedba elektronskega bolniškega lista</a></p> <p><a href="/ZZZS/info/gradiva.nsf/o/64907F00B1E087D3C12585F4004032F4?OpenDocument"><strong>Tiskovna konferenca</strong><br/>01.10.2020 - Stališče ZZZS k Predlogu zakona o dolgotrajni oskrbi</a></p> <p><a href="/ZZZS/info/gradiva.nsf/o/F8A868F3919400C0C12585EB00324B9D?OpenDocument"><strong>Sporočilo za javnost</strong><br/>22.09.2020 - Finančno poslovanje ZZZS v letu 2020 in dostopnost do zdravstvenih storitev</a></p> <p><a href="/ZZZS/info/gradiva.nsf/o/7F973792F5467C9CC12585C80035DFC1?OpenDocument"><strong>Sporočilo za javnost</strong><br/>18.08.2020 - 28. redna seja Upravnega odbora ZZZS (dostopnost do zdravstvenih storitev, finančno poslovanje)</a></p> <p><a href="/ZZZS/info/gradiva.nsf/o/97900DEA3A87072AC12585A60031E800?OpenDocument"><strong>Sporočilo za javnost</strong><br/>15.07.2020 - Po prvih 6 mesecih ZZZS izkazal primanjkljaj prihodkov nad odhodki v višini 107,7 milijona evrov </a></p> <p><a href="/ZZZS/info/gradiva.nsf/o/BE8318132EBC8B1FC12585A5002DF3C1?OpenDocument"><strong>Tiskovna konferenca</strong><br/>14.07.2020 - Z uvedbo novega sodobnega modela plačevanja molekularne genetske diagnostike v onkologiji s 1. julijem 2020 tudi uvedba financiranja genetskih testov za določitev zdravljenja pri zgodnjem raku dojke</a></p> <p><a href="/ZZZS/info/gradiva.nsf/o/CD500125EBB47B7EC1258598003F2EA6?OpenDocument"><strong>Sporočilo za javnost</strong><br/>01.07.2020 - Po prvih 5 mesecih ZZZS zadolžen in brez razpoložljivih lastnih sredstev</a></p>
</div>
<a class="arhiv" href=" http://www.zzzs.si/ZZZS/info/gradiva.nsf/sporocila?OpenView&amp;RestrictToCategory=2021">Arhiv</a>
</div>
<br/><br/><br/>
<div class="banner9"><a href="/zaposlitve"><img alt="Zaposlitve " src="/webdsg.nsf/zap6.jpg"/></a></div>
<div class="banner9"><a href="https://www.zzzs.si/zzzs/info/zadovoljstvo.nsf/Dokument?OpenForm"><img alt="Anketa o zadovoljstvu strank ZZZS " src="/webdsg.nsf/zadovoljstvo.jpg"/></a></div>
</div>
<br class="clear"/>
</div>
</div> <!-- main content -->
<div id="footer">
<div id="footer-nav">
<p>
<a href="http://www.zzzs.si/">Domov</a>
<a href="http://www.cbz.si/cbz/bazazdr2.nsf/Search/$searchForm?SearchView">Centralna baza zdravil</a>
<a href="https://zavarovanec.zzzs.si/wps/portal/portali/azos/mtp/mp_seznam_dobaviteljev/">Dobavitelji medicinskih pripomočkov</a>
<a href="https://zavarovanec.zzzs.si/wps/portal/portali/azos/pravice_zdravstvenih_storitev/cakalne_dobe">Čakalne dobe in ordinacijski časi</a>
</p>
<p>
<a href="http://www.zzzs.si/tujina">Mednarodno zdravstveno zavarovanje in naročanje evropske kartice zdravstvenega zavarovanja</a>
<a href="http://www.zzzs.si/egradiva">Elektronska gradiva ZZZS</a>
</p>
<p>
<a href="http://www.zzzs.si/zzzs/internet/zzzsjn.nsf">Javna naročila ZZZS</a>
<a href="/zaposlitve">Zaposlitve</a>
<a href="http://www.zzzs.si/zzzs/internet/zzzs.nsf/o/C39D17FB75BC0140C1256D41002D375C">Povezave na sorodne strani</a>
<!--		<a href="http://www.zzzs.si/zzzs/internet/zzzs.nsf/o/117B56F75EDF9BDFC12574E200287B3A">Informacije o delovanju ON-LINE sistema</a> -->
<a href="https://partner.zzzs.si/wps/portal/portali/aizv/e-poslovanje/sistem_on_line_resitve_za_preverjanje_urejenosti_z/sistem_on_line/">Informacije o delovanju ON-LINE sistema</a>
</p>
<p>
<a href="http://www.zzzs.si/zzzs/internet/zzzs.nsf/o/9FA597ABDE867677C1257F46003F37F2">Prijava suma goljufij</a>
<a href="http://www.zzzs.si/zdravje/index.html">Za svoje zdravje</a>
</p>
</div>
<style>
.politika:link, .politika:visited, .politika:hover, .politika:active   {
 color: black;
 text-decoration: none;
}
</style>
<p><br/><br/><br/>© 2009 ZZZS Vse pravice pridržane | <a class="politika" href="/zzzs/internet/zzzs.nsf/o/974BD4C43C7F8D1DC12585D6003B903A">Izjava o dostopnosti | </a><a class="politika" href="https://www.zzzs.si/ZZZS/info/egradiva.nsf/o/0490F087A2213065C125851C0046BF5E?OpenDocument">Pogoji uporabe spletne strani</a></p>
</div>
</div>
</div></div></div></div></body>
</html>
