<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="initial-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<meta content="noindex,nofollow" name="robots"/>
<link href="/images/favicon/default/fav-144.ico" rel="icon" sizes="144x144" type="image/x-icon"/>
<link href="/images/favicon/default/fav-48.ico" rel="icon" sizes="48x48" type="image/x-icon"/>
<link href="/images/favicon/default/fav-32.ico" rel="icon" sizes="32x32" type="image/x-icon"/>
<link href="/images/touch-icons/default/57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="/images/touch-icons/default/76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/images/touch-icons/default/120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/images/touch-icons/default/152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/images/touch-icons/default/167x167.png" rel="apple-touch-icon" sizes="167x167"/>
<link href="/images/touch-icons/default/180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<title>Вход в систему</title>
<meta content="" name="keywords"/>
<meta content="Вход в систему" name="description"/>
<link href="/css/default.47d0cca0647c.css" rel="stylesheet" type="text/css"/>
<link href="/css/styles.89cc7e5ab5b5.css" rel="stylesheet" type="text/css"/>
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/css/smoothness/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" type="text/css"/>
<link href="/css/jquery.btnloader.css" rel="stylesheet" type="text/css"/>
<script src="/js/jquery-1.6.2.min.js" type="text/javascript"></script>
<script language="javascript" src="/js/bootstrap.js"></script>
<script src="/js/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
<script src="/js/jquery.cookie.js" type="text/javascript"></script>
<script src="/js/vendor/history.min.js" type="text/javascript"></script>
<script src="/js/jquery.btnloader.js" type="text/javascript"></script>
<script language="javascript" src="/js/public/user/action.0bff042ce289.js"></script>
<script type="text/javascript">
			$.ajaxSetup({
				data: {
					"csrf_test_name": $.cookie('csrf_cookie_name')
				}
			});
			$(document).ready(function(){
				if (!jQuery.support.ajax || !jQuery.support.boxModel || !jQuery.support.leadingWhitespace)
				{
					$('#content').prepend('<div class="global_alert">ВНИМАНИЕ!<br/>Вы используете устаревшую версию браузера!<br/>Для корректной и быстрой работы системы рекомендуем<br/><a target="_blank" href="http://soft.yandex.ru">скачать</a> и установить свежую версию!</div>');
					if($("#menu .in_header").length>0) $("#menu .in_header").prependTo("#registration");
				}
			});
		</script>
<script type="text/javascript">
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-6723474-6']);
                _gaq.push(['_trackPageview']);

                (function() {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            </script>
</head>
<body>
<div class="public_outer" id="outer">
<div id="public_header">
<a class="logo" href="/main"><img src="/images/extranet_logo_red.png"/></a>
</div>
<div id="content">
<div class="public-primary-content" id="primaryContent" style="margin: 0;">
<div id="infoMessage"></div>
<div class="mainInfo">
<div class="flippedWrapper">
<div id="login">
<div class="inner">
<h1>
</h1>
<div class="formWrapper">
<form accept-charset="utf-8" action="https://101hotels.info/auth/login" method="post"> <input name="uI9q4TFX" type="hidden" value="tWt6pgsdWtFlTKtMkeEf"/>
<div class="tip">
<label for="identity">Логин:</label>
<input id="identity" name="identity" type="text" value=""/> </div>
<div class="tip" style="margin-bottom: 15px;">
<label class="mb-10" for="password">Пароль:<a class="flip" href="#">Забыли свой пароль?</a></label>
<input id="password" name="password" type="password" value=""/> </div>
<div class="panelButton" style="float: left">
<div class="btn-group">
<button class="btn" name="submit" type="submit">Войти</button> </div>
</div>
</form> </div>
</div>
<div class="shadow"></div>
</div>
<div id="recover">
<div class="inner">
<h2>Восстановление пароля</h2>
<div class="formWrapper">
<form accept-charset="utf-8" action="https://101hotels.info/public/user/forgot_password" id="fRecover" method="post"> <input name="uI9q4TFX" type="hidden" value="tWt6pgsdWtFlTKtMkeEf"/>
<div class="tip" style="margin-bottom: 15px;">
<label for="login">Укажите Ваш логин:</label>
<input id="username" name="username" type="text" value=""/> </div>
<div class="panelButton">
<div class="btn-group" style="float: left">
<button class="btn flip" name="back" type="button"> « Назад </button>
</div>
<div class="btn-group" style="float: right">
<button class="btn" name="recover" type="submit">Восстановить</button> <button class="btn" name="no_remember" type="button"> Я не помню свой логин </button>
</div>
<div class="clearfix"></div>
</div>
<div id="recover_error">Мы не можем сменить Ваш пароль таким образом. Пожалуйста, свяжитесь со службой технической поддержки.</div>
</form> </div>
</div>
<div class="shadow"></div>
</div>
</div>
</div> </div>
<div class="clear"></div>
</div>
</div>
</body>
<!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter31644108 = new Ya.Metrika({ id:31644108, clickmap:true, trackLinks:true, accurateTrackBounce:true, trackHash:true, ut:"noindex" }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img alt="" src="https://mc.yandex.ru/watch/31644108?ut=noindex" style="position:absolute; left:-9999px;"/></div></noscript><!-- /Yandex.Metrika counter -->
</html>