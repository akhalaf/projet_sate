<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="/js/m.js" type="text/javascript"></script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>E小说_书友最值得收藏!</title>
<meta content="E小说,小说阅读网,zwda.com" name="keywords"/>
<meta content="E小说是广大书友最值得收藏的网络小说阅读网，网站收录了当前最火热的网络小说，免费提供高质量的小说最新章节，是广大网络小说爱好者必备的小说阅读网。" name="description"/>
<meta content="no-siteapp" http-equiv="Cache-Control"/>
<meta content="no-transform" http-equiv="Cache-Control"/>
<meta content="format=html5; url=https://m.zwda.com" http-equiv="mobile-agent"/>
<meta content="format=xhtml; url=https://m.zwda.com" http-equiv="mobile-agent"/>
<link href="/css/style.css" rel="stylesheet" type="text/css"/>
<script src="/js/zepto.min.js" type="text/javascript"></script>
<script src="/js/common.js?v1" type="text/javascript"></script>
<link href="/favicon.ico" rel="shortcut icon"/>
</head>
<body>
<div id="wrapper">
<script>login();</script>
<div class="header">
<div class="header_logo">
<a href="/">E小说</a>
</div>
<script>panel();</script>
</div>
<div class="nav">
<ul>
<li><a href="/">首页</a></li>
<li><a href="/xuanhuan/">玄幻魔法</a></li>
<li><a href="/xiuzhen/">武侠仙侠</a></li>
<li><a href="/dushi/">都市小说</a></li>
<li><a href="/lishi/">历史军事</a></li>
<li><a href="/wangyou/">网游小说</a></li>
<li><a href="/kehuan/">科幻小说</a></li>
<li><a href="/yanqing/">言情小说</a></li>
<li><a href="/qita/">其他小说</a></li>
<li><a href="/quanben/">全本小说</a></li>
<li><a href="/bookcase.html">临时书架</a></li>
</ul>
</div>
<div id="main">
<div id="hotcontent">
<div class="l">
<div class="item">
<div class="image"><a href="/fanrenxiuxianzhixianjiepian/"><img alt="凡人修仙之仙界篇" height="150" src="https://www.zwda.com/files/article/image/1/1690/1690s.jpg" width="120"/></a></div>
<dl>
<dt><span>忘语</span><a href="/fanrenxiuxianzhixianjiepian/">凡人修仙之仙界篇</a></dt>
<dd>凡人修仙，风云再起时空穿梭，轮回逆转金仙太乙，大罗道祖三千大道，法则至尊《凡人修仙传》仙界篇，一个韩立叱咤仙界的故事，一个凡人小子修仙的不灭传说。特说明下，没有看过前传的书友，并不影响本书的阅读体验，但感兴趣的书友，也可以先去看看《凡人修仙传》，再来看本书哦。</dd>
</dl>
<div class="clear"></div>
</div>
<div class="item">
<div class="image"><a href="/bumielongdi/"><img alt="不灭龙帝" height="150" src="https://www.zwda.com/files/article/image/12/12839/12839s.jpg" width="120"/></a></div>
<dl>
<dt><span>妖夜</span><a href="/bumielongdi/">不灭龙帝</a></dt>
<dd>神州大地，万族争雄，血脉战士横行，强者之战，天穹碎，星辰陨！少年自北漠拉棺而来，世人方知何为最强血脉！撒弥天大谎，背一世骂名，他万里独行，伴魔乱舞，只为男儿一诺。跨千山万水，闯九天十地，斩尽天下英豪，他誓要归来，只因她仍守着孤城。</dd>
</dl>
<div class="clear"></div>
</div>
<div class="item">
<div class="image"><a href="/wuliandianfeng/"><img alt="武炼巅峰" height="150" src="https://www.zwda.com/files/article/image/0/413/413s.jpg" width="120"/></a></div>
<dl>
<dt><span>莫默</span><a href="/wuliandianfeng/">武炼巅峰</a></dt>
<dd>武之巅峰，是孤独，是寂寞，是漫漫求索，是高处不胜寒逆境中成长，绝地里求生，不屈不饶，才能堪破武之极道。凌霄阁试炼弟子兼扫地小厮杨开偶获一本无字黑书，从此踏上漫漫武道。******************友情提示：这是一个持久的男人的故事，这也是一个淫者见淫，智者见智的故事。做男人，当持久！*****************</dd>
</dl>
<div class="clear"></div>
</div>
<div class="item">
<div class="image"><a href="/cangyuantu/"><img alt="沧元图" height="150" src="https://www.zwda.com/files/article/image/31/31416/31416s.jpg" width="120"/></a></div>
<dl>
<dt><span>我吃西红柿</span><a href="/cangyuantu/">沧元图</a></dt>
<dd>我叫孟川，今年十五岁，是东宁府“镜湖道院”的当代大师兄。</dd>
</dl>
<div class="clear"></div>
</div>
</div>
<div class="r">
<h2>强势推荐</h2>
<ul>
<li><span class="s1">[玄幻]</span><span class="s2"><a href="/dadaochaotian/">大道朝天</a></span><span class="s5">猫腻</span></li>
<li><span class="s1">[玄幻]</span><span class="s2"><a href="/yuanzun/">元尊</a></span><span class="s5">天蚕土豆</span></li>
<li><span class="s1">[修真]</span><span class="s2"><a href="/sancunrenjian/">三寸人间</a></span><span class="s5">耳根</span></li>
<li><span class="s1">[修真]</span><span class="s2"><a href="/dafuzhuanshi/">大符篆师</a></span><span class="s5">小刀锋利</span></li>
<li><span class="s1">[玄幻]</span><span class="s2"><a href="/wodexuejiehuimofa/">我的学姐会魔法</a></span><span class="s5">荣小荣</span></li>
<li><span class="s1">[玄幻]</span><span class="s2"><a href="/chaoshenzhikashi/">超神制卡师</a></span><span class="s5">零下九十度</span></li>
<li><span class="s1">[都市]</span><span class="s2"><a href="/zhongshengzhizuihaoshidai/">重生之最好时代</a></span><span class="s5">九灯和善</span></li>
<li><span class="s1">[玄幻]</span><span class="s2"><a href="/futianshi/">伏天氏</a></span><span class="s5">净无痕</span></li>
<li><span class="s1">[都市]</span><span class="s2"><a href="/wodejuesezongcaiweihunqi/">我的绝色总裁未婚妻</a></span><span class="s5">花幽山月</span></li>
</ul>
</div>
<div class="clear"></div>
</div>
<div class="novelslist">
<div class="content">
<h2>玄幻魔法</h2>
<div class="top">
<div class="image"><a href="/shengxu/">
<img alt="圣墟" height="82" src="https://www.zwda.com/files/article/image/13/13975/13975s.jpg" width="67"/></a></div>
<dl>
<dt><a href="/shengxu/">圣墟</a></dt>
<dd>在破败中崛起，在寂灭中复苏。沧海成尘，雷电枯竭，那一缕幽雾又一次临近大地，世间的枷锁被打开了，一个全新的世界就此揭开神秘的一角……</dd>
</dl>
<div class="clear"></div>
</div>
<ul>
<li><a href="/shenyunxianwang/">天命主宰</a>/开荒</li>
<li><a href="/guimizhizhu/">诡秘之主</a>/爱潜水的乌贼</li>
<li><a href="/xiuluowushen/">修罗武神</a>/善良的蜜蜂</li>
<li><a href="/nijiankuangshen/">逆剑狂神</a>/一剑清新</li>
<li><a href="/wangushendi/">万古神帝</a>/飞天鱼</li>
<li><a href="/wandaojianzun/">万道剑尊</a>/打死都要钱</li>
<li><a href="/zhongshengshangzhouwang/">重生商纣王</a>/星辰雨</li>
<li><a href="/wozhenshizuzhang/">万古最强部落</a>/山人有妙计</li>
<li><a href="/longshenzhizun/">龙神至尊</a>/七栾</li>
<li><a href="/chuidiaozhishen/">垂钓之神</a>/会狼叫的猪</li>
<li><a href="/xiuluodanshen/">修罗丹神</a>/空神</li>
<li><a href="/busiwuhuang1/">不死武皇</a>/xiao少爷</li>
</ul>
</div>
<div class="content">
<h2>修真小说</h2>
<div class="top">
<div class="image"><a href="/tianxiadijiu/">
<img alt="天下第九" height="82" src="https://www.zwda.com/files/article/image/4/4722/4722s.jpg" width="67"/></a></div>
<dl>
<dt><a href="/tianxiadijiu/">天下第九</a></dt>
<dd>　　无尽宇宙之中有八道鸿蒙道则，这八道道则每一道都被一个无上强者融合。没有人知道，宇宙之中还有第九道道则，这一道道则破开鸿蒙，无人可触。</dd>
</dl>
<div class="clear"></div>
</div>
<ul>
<li><a href="/laozixiuxianhuilaile/">我是仙凡</a>/百里玺</li>
<li><a href="/dashujuxiuxian/">大数据修仙</a>/陈风笑</li>
<li><a href="/yonghengguodu/">永恒国度</a>/孤独漂流</li>
<li><a href="/pingtiance/">平天策</a>/无罪</li>
<li><a href="/momenbailei/">魔门败类</a>/惊涛骇浪</li>
<li><a href="/lingyuanjie/">灵缘界</a>/谢十叁</li>
<li><a href="/woshibanyao/">我是半妖</a>/北燎</li>
<li><a href="/zhongshengxiyouzhitianpengyaozun/">重生西游之天篷妖尊</a>/拼搏的射手</li>
<li><a href="/zhongshengzhidoushixianzun/">重生之都市仙尊</a>/周炎植</li>
<li><a href="/chaonaotaijian/">超脑太监</a>/萧舒</li>
<li><a href="/kuangshenxingtian/">狂神刑天</a>/妖的天空</li>
<li><a href="/dafuzhuanshi/">大符篆师</a>/小刀锋利</li>
</ul>
</div>
<div class="content border">
<h2>都市小说</h2>
<div class="top">
<div class="image"><a href="/wodejuesezongcailaopo1/">
<img alt="我的绝色总裁老婆" height="82" src="https://www.zwda.com/files/article/image/21/21531/21531s.jpg" width="67"/></a></div>
<dl>
<dt><a href="/wodejuesezongcailaopo1/">我的绝色总裁老婆</a></dt>
<dd>硬汉高手回归都市，一路张狂帅气，锋芒毕露，冷艳绝色总裁看之不起反被征服！友情提示：纯洁男女勿入！</dd>
</dl>
<div class="clear"></div>
</div>
<ul>
<li><a href="/doushichaojiyisheng1/">都市超级医圣</a>/断桥残雪</li>
<li><a href="/sanjiehongbaoqun/">三界红包群</a>/小教主</li>
<li><a href="/zuiqianghaoxu/">最强豪婿</a>/狸力</li>
<li><a href="/nuzongcaideshangmennuxu/">女总裁的上门女婿</a>/一起成功</li>
<li><a href="/shuangshichongfeiwureyaoniexiewang/">双世宠妃，误惹妖孽邪王</a>/葱不吃糖</li>
<li><a href="/doushijuewuyishen/">都市绝武医神</a>/小萌靓</li>
<li><a href="/zuiqiangnixi/">最强逆袭</a>/关中老人</li>
<li><a href="/kuangtan/">狂探</a>/旷海忘湖</li>
<li><a href="/jiaohuadetieshengaoshou/">校花的贴身高手</a>/鱼人二代</li>
<li><a href="/nuzongcaidexiaoyaobingwang/">女总裁的逍遥兵王</a>/君已老</li>
<li><a href="/zuiqiangshenyihundoushi/">最强神医混都市</a>/九歌</li>
<li><a href="/nvzongcaidequannengbingwang/">女总裁的全能兵王</a>/寂寞的舞者</li>
</ul>
</div>
<div class="clear"></div>
</div>
<div class="novelslist">
<div class="content">
<h2>历史小说</h2>
<div class="top">
<div class="image"><a href="/minguodieying/">
<img alt="民国谍影" height="82" src="https://www.zwda.com/files/article/image/24/24468/24468s.jpg" width="67"/></a></div>
<dl>
<dt><a href="/minguodieying/">民国谍影</a></dt>
<dd>一个普通平凡的公务员，被一棵菩提树带回到了民国25年，被迫加入军统，凭借着前世的信息和菩提树的神奇力量，寻找地下组织，追查日本间谍，在波澜壮阔的大时代中为祖国，为民族的解放与复兴贡献着自己的一份力量，开始了他传奇的谍海生涯。</dd>
</dl>
<div class="clear"></div>
</div>
<ul>
<li><a href="/mingtianxia/">明天下</a>/孑与2</li>
<li><a href="/hanmenjueqi/">寒门崛起</a>/朱郎才尽</li>
<li><a href="/tiantangjinxiu/">天唐锦绣</a>/公子許</li>
<li><a href="/hanmenzhuangyuan/">寒门状元</a>/天子</li>
<li><a href="/jiuguo/">九国</a>/我糕呢</li>
<li><a href="/zhongshengzhilaozishihuangdi/">重生之老子是皇帝</a>/步梵</li>
<li><a href="/zhanchanghetonggong/">战场合同工</a>/勿亦行</li>
<li><a href="/zhuixu/">赘婿</a>/愤怒的香蕉</li>
<li><a href="/guisanguo/">诡三国</a>/马月猴年</li>
<li><a href="/mingdiguodejueqi/">明帝国的崛起</a>/九悟</li>
<li><a href="/lietianzhengfeng/">猎天争锋</a>/睡秋</li>
<li><a href="/shenjiwangzheshengjixitong/">神级王者升级系统</a>/心中伤梦中泪</li>
</ul>
</div>
<div class="content">
<h2>网游小说</h2>
<div class="top">
<div class="image"><a href="/doupocangqiongzhimoshangzhijing/">
<img alt="斗破苍穹之无上之境" height="82" src="https://www.zwda.com/files/article/image/1/1775/1775s.jpg" width="67"/></a></div>
<dl>
<dt><a href="/doupocangqiongzhimoshangzhijing/">斗破苍穹之无上之境</a></dt>
<dd>你常说，复仇才是我的使命，但你却不知道，守护你，才是我一生的宿命等级制度：斗帝、斗仙、斗神、帝之不朽</dd>
</dl>
<div class="clear"></div>
</div>
<ul>
<li><a href="/zhanyue/">斩月</a>/失落叶</li>
<li><a href="/wangzheshike/">王者时刻</a>/蝴蝶蓝</li>
<li><a href="/ciyuanfadian/">次元法典</a>/西贝猫</li>
<li><a href="/huoyingzhiqianyechuanshuo/">火影之千叶传说</a>/零始</li>
<li><a href="/douluozhixiantianershiji/">斗罗之先天二十级</a>/超粉</li>
<li><a href="/aizelasiyueyezhiying/">艾泽拉斯月夜之影</a>/咸鱼不惧突刺</li>
<li><a href="/woyouyizuomoricheng/">我有一座末日城</a>/头发掉了</li>
<li><a href="/zhongshengzhikuangbaohuofa/">重生之狂暴火法</a>/燃烧的地狱咆哮</li>
<li><a href="/wanjiezhizuiqiangnaiba/">万界之最强奶爸</a>/淡抹总相宜</li>
<li><a href="/lianmengzhiyongbingxitong/">联盟之佣兵系统</a>/初四兮</li>
<li><a href="/wozhendekongzhibuzhuziji/">我真的控制不住自己</a>/张扬的五月</li>
<li><a href="/wangyouzhishunfafashi/">网游之瞬发法师</a>/祎辉</li>
</ul>
</div>
<div class="content border">
<h2>科幻小说</h2>
<div class="top">
<div class="image"><a href="/weilaitianwang/">
<img alt="未来天王" height="82" src="https://www.zwda.com/files/article/image/15/15094/15094s.jpg" width="67"/></a></div>
<dl>
<dt><a href="/weilaitianwang/">未来天王</a></dt>
<dd>知名作曲家方召重生在五百年后的未来新世界霸（zhuang）气（bi）外（zuo）露（si）的故事。“每当我脑子里响起BGM的时候，就觉得自己无所畏惧。”——方召。————————————————已创作三本小说，《星级猎人》、《回到过去变成猫》、《原始战记》。</dd>
</dl>
<div class="clear"></div>
</div>
<ul>
<li><a href="/woyouyizuokongbuwu/">我有一座恐怖屋</a>/我会修空调</li>
<li><a href="/jiuxingdunai/">九星毒奶</a>/育</li>
<li><a href="/limingzhijian/">黎明之剑</a>/远瞳</li>
<li><a href="/zhutianwanjiejutouqun/">诸天万界剧透群</a>/摘星上人</li>
<li><a href="/zhutianjinshouzhi/">诸天金手指</a>/横空日月</li>
<li><a href="/morinushenyangchenggonglue/">末日女神养成攻略</a>/刺嫩芽</li>
<li><a href="/shenkongqiuzhang/">深空球长</a>/最终永恒</li>
<li><a href="/yiciyuanhongjingshijie/">异次元红警世界</a>/三千狼</li>
<li><a href="/fulanguoduzhihuoxiaqu/">腐烂国度之活下去</a>/奇异果008</li>
<li><a href="/wodexibaojianyu/">我的细胞监狱</a>/穿黄衣的阿肥</li>
<li><a href="/zhibokuaichuanzhidalianchengshen/">直播快穿之打脸成神</a>/醉饮桂花酒</li>
<li><a href="/kejibaquan/">科技霸权</a>/秒速九光年</li>
</ul>
</div>
<div class="clear"></div>
</div>
<div id="newscontent">
<div class="l">
<h2>最近更新小说列表</h2>
<ul>
<li><span class="s1">[言情小说]</span>
<span class="s2"><a href="/cangzhu1/" target="_blank">藏珠</a></span>
<span class="s3"><a href="/cangzhu1/730028/" target="_blank">第166章 善后</a></span>
<span class="s4">云芨</span><span class="s5">01-14</span></li>
<li><span class="s1">[言情小说]</span>
<span class="s2"><a href="/book/2136/" target="_blank">主角徐吟</a></span>
<span class="s3"><a href="/book/2136/730028.html" target="_blank">第166章 善后</a></span>
<span class="s4">云芨</span><span class="s5">01-14</span></li>
<li><span class="s1">[玄幻小说]</span>
<span class="s2"><a href="/kaijuqiandaohuanggushengti/" target="_blank">开局签到荒古圣体</a></span>
<span class="s3"><a href="/kaijuqiandaohuanggushengti/730027/" target="_blank">第726章 只愿为君，长袖善舞，我带你走</a></span>
<span class="s4">J神</span><span class="s5">01-14</span></li>
<li><span class="s1">[玄幻小说]</span>
<span class="s2"><a href="/book/2431/" target="_blank">君逍遥拜玉儿</a></span>
<span class="s3"><a href="/book/2431/730027.html" target="_blank">第726章 只愿为君，长袖善舞，我带你走</a></span>
<span class="s4">J神</span><span class="s5">01-14</span></li>
<li><span class="s1">[玄幻小说]</span>
<span class="s2"><a href="/book/2432/" target="_blank">开局签到荒古圣体君逍遥拜玉儿</a></span>
<span class="s3"><a href="/book/2432/730027.html" target="_blank">第726章 只愿为君，长袖善舞，我带你走</a></span>
<span class="s4">J神</span><span class="s5">01-14</span></li>
<li><span class="s1">[玄幻小说]</span>
<span class="s2"><a href="/beichaoqiushengshilu/" target="_blank">北朝求生实录</a></span>
<span class="s3"><a href="/beichaoqiushengshilu/404510/" target="_blank">第1110章 惊蛰（下）</a></span>
<span class="s4">携剑远行</span><span class="s5">01-14</span></li>
<li><span class="s1">[玄幻小说]</span>
<span class="s2"><a href="/book/2924/" target="_blank">北朝求生实录高伯逸</a></span>
<span class="s3"><a href="/book/2924/404510.html" target="_blank">第1110章 惊蛰（下）</a></span>
<span class="s4">携剑远行</span><span class="s5">01-14</span></li>
<li><span class="s1">[修真小说]</span>
<span class="s2"><a href="/jiuzhoufengyunlu/" target="_blank">九州风云录</a></span>
<span class="s3"><a href="/jiuzhoufengyunlu/730026/" target="_blank">第一百九十章 先生姓焦</a></span>
<span class="s4">炭雪小蛟龙</span><span class="s5">01-14</span></li>
<li><span class="s1">[都市小说]</span>
<span class="s2"><a href="/jipinxiaocunyi/" target="_blank">极品小村医</a></span>
<span class="s3"><a href="/jipinxiaocunyi/20302223/" target="_blank">第七百五十一章 违心道歉</a></span>
<span class="s4">山海路人</span><span class="s5">01-14</span></li>
<li><span class="s1">[玄幻小说]</span>
<span class="s2"><a href="/wudizhizuiqiangshenjixuanzexitong/" target="_blank">无敌之最强神级选择系统</a></span>
<span class="s3"><a href="/wudizhizuiqiangshenjixuanzexitong/20686236/" target="_blank">第1870章 渊的表态【四更】</a></span>
<span class="s4">跳舞的傻猫</span><span class="s5">01-14</span></li>
<li><span class="s1">[历史小说]</span>
<span class="s2"><a href="/wanmingzhiworuoweihuang/" target="_blank">晚明之我若为皇</a></span>
<span class="s3"><a href="/wanmingzhiworuoweihuang/404508/" target="_blank">第20章 锦衣卫酷刑之弹琵琶</a></span>
<span class="s4">行者寒寒</span><span class="s5">01-14</span></li>
<li><span class="s1">[历史小说]</span>
<span class="s2"><a href="/book/2991/" target="_blank">晚明之我若为皇李杰</a></span>
<span class="s3"><a href="/book/2991/404508.html" target="_blank">第20章 锦衣卫酷刑之弹琵琶</a></span>
<span class="s4">行者寒寒</span><span class="s5">01-14</span></li>
<li><span class="s1">[都市小说]</span>
<span class="s2"><a href="/yishendianlinshigong/" target="_blank">医神殿临时工</a></span>
<span class="s3"><a href="/yishendianlinshigong/20686235/" target="_blank">第六六九章 束手就擒？</a></span>
<span class="s4">疯二神</span><span class="s5">01-14</span></li>
<li><span class="s1">[都市小说]</span>
<span class="s2"><a href="/tianxiadiyi1/" target="_blank">天下第一</a></span>
<span class="s3"><a href="/tianxiadiyi1/20686128/" target="_blank">1013 二皇子，跪下了</a></span>
<span class="s4">抚琴的人</span><span class="s5">01-14</span></li>
<li><span class="s1">[玄幻小说]</span>
<span class="s2"><a href="/chaojifanpaixitong/" target="_blank">超级反派系统</a></span>
<span class="s3"><a href="/chaojifanpaixitong/404507/" target="_blank">第300章 大家超崇拜上宗仙师</a></span>
<span class="s4">暖风轻阳</span><span class="s5">01-14</span></li>
<li><span class="s1">[玄幻小说]</span>
<span class="s2"><a href="/book/2954/" target="_blank">超级反派系统秦宇</a></span>
<span class="s3"><a href="/book/2954/404507.html" target="_blank">第300章 大家超崇拜上宗仙师</a></span>
<span class="s4">暖风轻阳</span><span class="s5">01-14</span></li>
<li><span class="s1">[科幻小说]</span>
<span class="s2"><a href="/wocongmoshikaishiwudi/" target="_blank">我从末世开始无敌</a></span>
<span class="s3"><a href="/wocongmoshikaishiwudi/20686234/" target="_blank">第416章 继续谈生意（求订阅）</a></span>
<span class="s4">水晶脑袋</span><span class="s5">01-14</span></li>
<li><span class="s1">[历史小说]</span>
<span class="s2"><a href="/jipinningchen/" target="_blank">极品佞臣</a></span>
<span class="s3"><a href="/jipinningchen/730025/" target="_blank">第二百四十三章 杀心</a></span>
<span class="s4">日日生</span><span class="s5">01-14</span></li>
<li><span class="s1">[历史小说]</span>
<span class="s2"><a href="/book/2267/" target="_blank">我真不是狗官</a></span>
<span class="s3"><a href="/book/2267/730025.html" target="_blank">第二百四十三章 杀心</a></span>
<span class="s4">日日生</span><span class="s5">01-14</span></li>
<li><span class="s1">[玄幻小说]</span>
<span class="s2"><a href="/lenggongkaijuqiandaokuihuabaodian/" target="_blank">冷宫开局签到葵花宝典</a></span>
<span class="s3"><a href="/lenggongkaijuqiandaokuihuabaodian/404506/" target="_blank">第21章 皇帝的决断</a></span>
<span class="s4">六年磨一剑</span><span class="s5">01-14</span></li>
<li><span class="s1">[玄幻小说]</span>
<span class="s2"><a href="/book/3146/" target="_blank">冷宫开局签到葵花宝典林平</a></span>
<span class="s3"><a href="/book/3146/404506.html" target="_blank">第21章 皇帝的决断</a></span>
<span class="s4">六年磨一剑</span><span class="s5">01-14</span></li>
<li><span class="s1">[玄幻小说]</span>
<span class="s2"><a href="/woyougongfatiquqi/" target="_blank">我有功法提取器</a></span>
<span class="s3"><a href="/woyougongfatiquqi/404505/" target="_blank">第33章 ?再临回元堂</a></span>
<span class="s4">轻舟煮酒</span><span class="s5">01-14</span></li>
<li><span class="s1">[玄幻小说]</span>
<span class="s2"><a href="/book/2636/" target="_blank">我有功法提取器陈苍</a></span>
<span class="s3"><a href="/book/2636/404505.html" target="_blank">第33章 ?再临回元堂</a></span>
<span class="s4">轻舟煮酒</span><span class="s5">01-14</span></li>
<li><span class="s1">[玄幻小说]</span>
<span class="s2"><a href="/chushengjiubeibaoyangdelong/" target="_blank">出生就被包养的龙</a></span>
<span class="s3"><a href="/chushengjiubeibaoyangdelong/667633/" target="_blank">第一百一十八章 真龙</a></span>
<span class="s4">瑞血丰年</span><span class="s5">01-14</span></li>
<li><span class="s1">[都市小说]</span>
<span class="s2"><a href="/quannengqiandao/" target="_blank">全能签到</a></span>
<span class="s3"><a href="/quannengqiandao/730024/" target="_blank">第三百四十九章 向钱看，向厚赚</a></span>
<span class="s4">当心枪走火</span><span class="s5">01-14</span></li>
<li><span class="s1">[玄幻小说]</span>
<span class="s2"><a href="/shishangzuiqiangkuangdi/" target="_blank">史上最强狂帝</a></span>
<span class="s3"><a href="/shishangzuiqiangkuangdi/20213092/" target="_blank">第2077章 秦长空的玉佩</a></span>
<span class="s4">天下1</span><span class="s5">01-14</span></li>
<li><span class="s1">[修真小说]</span>
<span class="s2"><a href="/quedaoxunchang/" target="_blank">却道寻常</a></span>
<span class="s3"><a href="/quedaoxunchang/667632/" target="_blank">第二十九章 形神俱灭</a></span>
<span class="s4">三两才气</span><span class="s5">01-14</span></li>
<li><span class="s1">[都市小说]</span>
<span class="s2"><a href="/zhuixudangdao/" target="_blank">赘婿当道</a></span>
<span class="s3"><a href="/zhuixudangdao/20479170/" target="_blank">第一千七百零二章  喜出望外</a></span>
<span class="s4">吻天的狼</span><span class="s5">01-14</span></li>
<li><span class="s1">[都市小说]</span>
<span class="s2"><a href="/book/279/" target="_blank">岳风柳萱免费阅读</a></span>
<span class="s3"><a href="/book/279/20479170.html" target="_blank">第一千七百零二章  喜出望外</a></span>
<span class="s4">吻天的狼</span><span class="s5">01-14</span></li>
<li><span class="s1">[都市小说]</span>
<span class="s2"><a href="/book/280/" target="_blank">赘婿当道全文免费 阅读小说最新</a></span>
<span class="s3"><a href="/book/280/20479170.html" target="_blank">第一千七百零二章  喜出望外</a></span>
<span class="s4">吻天的狼</span><span class="s5">01-14</span></li>
</ul>
</div>
<div class="r">
<h2>最新入库小说</h2>
<ul>
<li><span class="s1">[科幻]</span>
<span class="s2"><a href="/zhushenguale/">主神挂了</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/meizizi/">美滋滋</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/kuaichuanzhidalaotazongzaiweizhuang/">快穿之大佬她总在伪装</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/xijingchuanjinkuqingju/">戏精穿进苦情剧</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/tadeshantadehai/">她的山，她的海</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[修真]</span>
<span class="s2"><a href="/wozhixianganwenqiudao/">我只想安稳求道</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/niyinqilewodezhuyi/">你引起了我的注意</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/weikexuefendou/">为科学奋斗</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/xianyuchuanchengniandaixiaofubao/">咸鱼穿成年代小福宝</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/huoguoshiyan/">祸国·式燕</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[都市]</span>
<span class="s2"><a href="/nihaokeluosinongchang/">你好，克洛斯农场</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[玄幻]</span>
<span class="s2"><a href="/congwanglingxiaokuloudaozhongjida/">从亡灵小骷髅到终极大BOSS</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/huoguoguicheng/">祸国·归程</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[都市]</span>
<span class="s2"><a href="/qiandaobihuo/">签到必火</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/yueliangyouniyibanyuan/">月亮有你一半圆</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[玄幻]</span>
<span class="s2"><a href="/meiyueyibachaoshenqi/">每月一把超神器</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/quannichenzaoxihuanwo/">劝你趁早喜欢我</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/mingqiangyiduoanliannanfangxiaoduibanfanwai/">明枪易躲，暗恋难防(校对版+番外)</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/dalaomajiawubuzhule/">大佬马甲捂不住了</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/tanametianwanjie/">她那么甜(完结)</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/jintiannvzhutaxuefeilema/">今天女主她学废了吗</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[玄幻]</span>
<span class="s2"><a href="/quanzhibazhe/">拳之霸者</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[历史]</span>
<span class="s2"><a href="/bixiaqiuwozuotaizi/">陛下求我做太子</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/wodeyinchaojinvyou/">我的印钞机女友</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[修真]</span>
<span class="s2"><a href="/honghuangzhicongbuzhoushankaishiqiandao/">洪荒之从不周山开始签到</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/xinyoumenghuxiuqiangwei/">心有猛虎嗅蔷薇</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/nishengrenjian/">你胜人间</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/bixiaheihuahouchaonanhong/">陛下黑化后超难哄</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[言情]</span>
<span class="s2"><a href="/xueshentazhenxiangle/">Mr学神他真香了</a></span>
<span class="s5">01-14</span></li>
<li><span class="s1">[科幻]</span>
<span class="s2"><a href="/congshediaokaishichuanyuezhutian/">从射雕开始穿越诸天</a></span>
<span class="s5">01-14</span></li>
</ul>
</div>
<div class="clear"></div>
</div>
</div>
</div>
<div id="firendlink">
友情连接：
<a href="https://m.zwda.com/" target="_blank">E小说</a>
<a href="https://www.zwda.com/mulongshi/" target="_blank">牧龙师</a>
<a href="https://www.zwda.com/yuanzun/" target="_blank">元尊</a>
<a href="http://www.biqubu.com/" target="_blank">笔趣阁</a>
<a href="https://www.zwda.com/wanzuzhijie/" target="_blank">万族之劫</a>
<a href="https://www.siluke.tv/" target="_blank">思路客</a>
<a href="https://www.zwda.com/futianshi/" target="_blank">伏天氏</a>
<a href="http://www.quduwu.com/" target="_blank">趣读屋</a>
<a href="https://www.zwda.com/shengxu/" target="_blank">圣墟</a>
<a href="https://www.longtanshuwang.com/" target="_blank">龙坛书网</a>
<a href="https://www.ltsw888.com/" target="_blank">龙坛书网</a>
<a href="https://www.zwda.com/chaopinmingshi/" target="_blank">超品命师</a>
<a href="https://www.zwda.com/yujiutian/" target="_blank">御九天</a>
<a href="http://www.book520.com/" target="_blank">小说520</a>
<a href="https://www.zwda.com/dadaochaotian/" target="_blank">大道朝天</a>
<a href="http://www.s81zw.com/" target="_blank">八一中文网</a>
<a href="http://www.shushuw.com/" target="_blank">书书网</a>
<a href="http://www.mibaoge.com/" target="_blank">笔趣阁</a>
<a href="https://www.xs222.tw/" target="_blank">顶点小说</a>
<a href="https://www.xsbiquge.com/" target="_blank">新笔趣阁</a>
<a href="http://www.jid5.com/" target="_blank">六迹之大荒祭</a>
<a href="http://www.gugu123.com/" target="_blank">点道为止</a>
<a href="http://www.abctang.com/" target="_blank">剑来</a>
<a href="https://www.tmwx.net/" target="_blank">大宋明月</a>
<a href="http://www.mangg.com/" target="_blank">追书网</a>
<a href="https://www.zwwx.com/" target="_blank">中网文学</a>
<a href="https://www.biqubao.com/" target="_blank">笔趣阁</a>
<a href="http://www.8du8.com/" target="_blank">八度吧小说网</a>
<a href="https://www.booktxt.com/" target="_blank">顶点小说</a>
<a href="https://www.sbiquge.com/" target="_blank">笔趣阁</a>
<a href="https://www.wenxuemi6.com/" target="_blank">文学迷</a>
<a href="https://www.liewen.la/" target="_blank">猎文网</a>
<a href="https://www.23txt.com/" target="_blank">天籁小说网</a>
<a href="https://www.biduo.cc/" target="_blank">笔趣阁</a>
<a href="https://www.biyuwu.cc/" target="_blank">官术网</a>
<a href="http://www.biqugexs.com/" target="_blank">笔趣阁</a>
<a href="https://www.xbiqugew.net/" target="_blank">新笔趣阁</a>
<a href="http://www.biquge.com.cn/" target="_blank">笔趣阁</a>
<a href="http://www.bqg.tw/" target="_blank">笔趣阁</a>
<a href="http://www.biquge.lu/" target="_blank">笔趣阁</a>
<a href="http://www.biqugex.com/" target="_blank">笔趣阁</a>
<a href="http://www.shengyan.org/" target="_blank">笔趣阁</a>
<a href="http://www.wjduo.com/" target="_blank">我是至尊</a>
<a href="https://www.biqupa.com/" target="_blank">替天行盗</a>
<a href="http://www.qingkanshu.cc/" target="_blank">请看书</a>
<a href="http://www.8jzw.com/" target="_blank">八戒中文网</a>
<a href="https://www.yb3.cc/" target="_blank">久看中文网</a>
<a href="https://www.kanshula.com/" target="_blank">看书啦</a>
<a href="https://www.ddbiquge.com/" target="_blank">顶点笔趣阁</a>
<a href="http://www.qb5200.tw/" target="_blank">全本小说网</a>
<a href="http://www.xwxguan.com/" target="_blank">文学馆</a>
<a href="http://www.doulaidu.cc/" target="_blank">都来读</a>
<a href="https://www.qixinge.com/" target="_blank">沧元图</a>
<a href="https://www.wansong.net/" target="_blank">废土指挥官</a>
<a href="https://www.81zw.org/" target="_blank">一剑斩破九重天</a>
<a href="https://www.dashen88.com/" target="_blank">猛卒</a>
</div>
<div class="footer">
<div class="footer_cont">
<p>
本站所有小说为转载作品，所有章节均由网友上传，转载至本站只是为了宣传本书让更多读者欣赏。</p>
<p>
Copyright ? 2016 E小说 All Rights Reserved.</p>
<script>dl();</script>
<script>footer();</script>
</div>
</div>
<script charset="utf-8" src="https://www.baidu.com/js/opensug.js" type="text/javascript"></script>
</body>
</html>
