<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" version="XHTML+RDFa 1.0" xml:lang="sl" xmlns="http://www.w3.org/1999/xhtml" xmlns:article="http://ogp.me/ns/article#" xmlns:book="http://ogp.me/ns/book#" xmlns:fb="http://ogp.me/ns/fb#" xmlns:og="http://ogp.me/ns#" xmlns:product="http://ogp.me/ns/product#" xmlns:profile="http://ogp.me/ns/profile#" xmlns:video="http://ogp.me/ns/video#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="https://www.zrc-sazu.si/misc/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<meta content="Drupal 7 (https://www.drupal.org)" name="generator"/>
<link href="https://www.zrc-sazu.si/sl" rel="canonical"/>
<link href="https://www.zrc-sazu.si/sl" rel="shortlink"/>
<meta content="ZRC SAZU" property="og:site_name"/>
<meta content="website" property="og:type"/>
<meta content="https://www.zrc-sazu.si/sl" property="og:url"/>
<meta content="ZRC SAZU" property="og:title"/>
<meta content="https://www.zrc-sazu.si/sites/all/themes/zrc/img/zrc-logo-sl.png" property="og:image"/>
<meta content='"image/png"' property="og:image:type"/>
<title>Stran ni bila najdena | ZRC SAZU</title>
<link charset="utf-8" href="/sites/all/themes/zrc/css/reset.css" media="all" rel="stylesheet" type="text/css"/>
<link charset="utf-8" href="/sites/all/themes/zrc/css/colorbox.css" media="all" rel="stylesheet" type="text/css"/>
<link charset="utf-8" href="/sites/all/themes/zrc/css/style.css?v=1" media="all" rel="stylesheet" type="text/css"/>
<link charset="utf-8" href="/sites/all/themes/zrc/css/webform.css?v=3" media="all" rel="stylesheet" type="text/css"/>
<link charset="utf-8" href="/sites/all/themes/zrc/css/cookiecuttr.css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 7]><link rel="stylesheet" href="/sites/all/themes/zrc/css/ie.css" type="text/css" media="all" charset="utf-8" /></script><![endif]-->
<!--[if lt IE 7]><script type="text/javascript" src="/sites/all/themes/zrc/js/lib/IE7.js"></script><![endif]-->
<script src="https://www.zrc-sazu.si/sites/default/files/js/js__uAm0E5qXnef6RJ2a6GEeM0bbgBwk3ndUHEl1lKvQ4M.js" type="text/javascript"></script>
<script src="https://www.zrc-sazu.si/sites/default/files/js/js_-EecdSamaAp7kP45wiN0uqxJ7XkVjx72VslM7pM58kY.js" type="text/javascript"></script>
<script src="https://www.zrc-sazu.si/sites/default/files/js/js_gIjrFOUQIZJpQG-TOPjGGO_gUz44T9YKcOsdyrEVENg.js" type="text/javascript"></script>
<script src="https://www.zrc-sazu.si/sites/default/files/js/js_h8h_4TUs6xgoKKN1OT7l0la0Zg80LzvxNK5ac1vC3z0.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"sl\/","ajaxPageState":{"theme":"zrcfront","theme_token":"3Xt0mS2s8eYPjgYYmKeuLnQO0JcmA8e24tTJlVBK8zU","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.5\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"misc\/ui\/jquery.ui.core.min.js":1,"misc\/ui\/jquery.ui.widget.min.js":1,"misc\/ui\/jquery.ui.position.min.js":1,"misc\/ui\/jquery.ui.autocomplete.min.js":1,"misc\/form.js":1,"sites\/all\/modules\/google_cse\/google_cse.js":1,"sites\/all\/modules\/gss\/scripts\/autocomplete.js":1,"public:\/\/languages\/sl_L1LtjVCBZpL-CWdpBF3nI1IOlfDpZ76K_59S7_Hqe-4.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/custom_search\/js\/custom_search.js":1,"misc\/collapse.js":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Zapri","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"googleCSE":{"cx":"002737855993378622774:jf6wjbhyyks","language":"","resultsWidth":600,"domain":"www.google.com","showWaterMark":1},"gss":{"key":"008262997110643672012:h7phomevllq"},"custom_search":{"form_target":"_self","solr":0},"urlIsAjaxTrusted":{"\/izrk\/carsologica\/Acta282\/zagros.htm":true}});
//--><!]]>
</script>
<script src="/sites/all/themes/zrc/js/lib/cufon/cufon-yui-1.09i.js" type="text/javascript"></script>
<script src="/sites/all/themes/zrc/js/lib/cufon/helvetica_neue_lt_pro_750.cufon.font.js" type="text/javascript"></script>
<script src="/sites/all/themes/zrc/js/lib/jquery.tmpl.min.js" type="text/javascript"></script>
<script src="/sites/all/themes/zrc/js/lib/jquery.selectBox.js" type="text/javascript"></script>
<script src="/sites/all/themes/zrc/js/lib/jquery.mcdropdown.js" type="text/javascript"></script>
<script src="/sites/all/themes/zrc/js/lib/jquery.bgiframe.js" type="text/javascript"></script>
<script src="/sites/all/themes/zrc/js/lib/jwplayer.js" type="text/javascript"></script>
<script src="/sites/all/themes/zrc/js/lib/jquery.media.js" type="text/javascript"></script>
<script src="/sites/all/themes/zrc/js/lib/colorbox.js" type="text/javascript"></script>
<script src="/sites/all/themes/zrc/js/lib/jquery.cookie.js" type="text/javascript"></script>
<script src="/sites/all/themes/zrc/js/lib/jquery.cookiecuttr.js" type="text/javascript"></script>
<script src="/sites/all/themes/zrc/js/lib/autocomplete-main-min.js" type="text/javascript"></script>
<script src="/sites/all/themes/zrc/js/common.js" type="text/javascript"></script>
<script type="text/javascript">
            if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept") {
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                ga('create', 'UA-28370047-1', 'auto');
                ga('require', 'displayfeatures');
                ga('require', 'linkid', 'linkid.js');
                ga('send', 'pageview');

                (function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id))
                        return;
                    js = d.createElement(s);
                    js.id = id;
                        js.src = "//connect.facebook.net/sl_SI/sdk.js#xfbml=1&version=v2.0";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
            }
        </script>
</head>
<body class="html not-front not-logged-in no-sidebars page-izrk page-izrk-carsologica page-izrk-carsologica-acta282 page-izrk-carsologica-acta282-zagroshtm domain-www-zrc-sazu-si i18n-sl">
<div id="fb-root"></div>
<script></script>
<div id="wrapper">
<div id="container">
<div class="layout" id="top">
<img alt="" height="28" src="/sites/all/themes/zrc/img/headers/header-small.jpg" width="960"/>
<div class="language menu">
<div class="languages">
<ul>
<li class="first active">
<a class="active" href="/sl/izrk/carsologica/Acta282/zagros.htm">SLOVENŠČINA</a> </li>
<li class="last ">
<a href="/en/izrk/carsologica/Acta282/zagros.htm">ENGLISH</a> </li>
</ul>
</div>
</div>
<div class="title">
<a href="https://www.zrc-sazu.si/sl"><img alt="" height="59" src="/sites/all/themes/zrc/img/zrc-logo-sl.png" width="719"/></a>
<a href="https://www.zrc-sazu.si/sl/cart">
<div class="kosarica">
                Košarica (0)            </div>
</a>
<div class="dropdown"> <!-- class="kosarica" -->
<div class="dropbtn"><a href="/sl/user">Prijava</a></div>
</div>
</div>
<div class="menu navigation">
<div id="search-small">
<form accept-charset="UTF-8" action="/izrk/carsologica/Acta282/zagros.htm" class="search-form" id="search-block-form" method="post" role="search"><div><div class="form-item form-type-textfield form-item-search-block-form">
<label class="element-invisible" for="edit-search-block-form--2">Search this site </label>
<input class="custom-search-box form-text" id="edit-search-block-form--2" maxlength="128" name="search_block_form" placeholder="" size="15" title="Vpišite izraze, ki jih želite poiskati." type="text" value=""/>
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input class="form-submit" id="edit-submit" name="op" type="submit" value="Išči"/></div><input name="form_build_id" type="hidden" value="form-2834kC-OqjbTE0JPAeDRCHLrFH3xS_ypz5dM10VnDXI"/>
<input name="form_id" type="hidden" value="search_block_form"/>
</div></form> </div>
</div>
</div>
<div class="layout" id="header">
<a id="v"></a>
<h2 class="title" id="page-title">Stran ni bila najdena</h2>
<div class="header-spacer"> </div>
<div class="clear"></div>
</div>
</div>
<div class="layout" id="main">
<div class="page"> <div class="region region-content">
<div class="block block-system" id="block-system-main">
<div class="content">
    The requested page "/izrk/carsologica/Acta282/zagros.htm" could not be found.  </div>
</div>
</div>
</div>
<div class="clear"></div>
</div>
<div id="push"></div>
</div>
<div id="footer">
<div class="layout" id="upper">
<div class="navigation links">
</div>
<div class="about">
<p class="strong black">Znanstvenoraziskovalni center</p>
<p class="strong black">Slovenske akademije znanosti in umetnosti</p>
<p>Novi trg 2, 1000 Ljubljana</p>
<br/>
<p><img src="https://www.zrc-sazu.si/sites/all/themes/zrc/img/email.png"/><a href="mailto:zrc@zrc-sazu.si">zrc@zrc-sazu.si</a></p>
<p><img src="https://www.zrc-sazu.si/sites/all/themes/zrc/img/phone.png"/>+386 1 470 61 00</p>
<p><img src="https://www.zrc-sazu.si/sites/all/themes/zrc/img/fax.png"/>+386 1 425 52 53</p>
<br/>
<p class="strong black">Dodatne informacije:</p>
<a href="https://uprava.zrc-sazu.si/">Uprava ZRC SAZU</a>
</div>
<div id="map-canvas">
<h1>Zemljevid</h1>
<div id="map"></div>
</div>
</div>
<div class="layout" id="bottom">
    © 2004-2021    ZRC SAZU • <a href="https://www.facebook.com/ZRC-SAZU-187909304594445"><img src="https://www.zrc-sazu.si/sites/all/themes/zrc/img/fb.png"/></a> • <a href="https://twitter.com/ZrcSazu"><img src="https://www.zrc-sazu.si/sites/all/themes/zrc/img/twitter.png"/></a> • <a href="https://www.zrc-sazu.si/sl/strani/piskotki">Piškotki</a> • Oblikovanje strani: <a href="http://www.innovatif.com">Innovatif</a>
    • Izdelava strani: <a href="http://www.xlab.si">XLAB d.o.o.</a>
</div>
<style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
<script>
 if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept") {           
var map;
function initMap() {
   var LatLng = {lat: 46.047552, lng: 14.504731};
  map = new google.maps.Map(document.getElementById('map'), {
    center: LatLng,
    zoom: 15
  });
  var marker = new google.maps.Marker({
    position: LatLng,
    map: map,
    icon: "/misc/favicon.png"
  });
  map.setOptions({minZoom: 6, mapTypeControl:false, streetViewControl:false});
}
 }
</script>
<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3d6rH9CiLI23IivOlLy5ZRUwO-kvkKX8&amp;callback=initMap&amp;language=sl"></script>
</div>
</body>
</html>
