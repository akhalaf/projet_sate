<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Network Software for Windows: Monitoring, Inventory, Mapper, Scanner, NMS</title>
<meta content="10-strike, software, inventory, network, windows, mapping, monitoring, search, file, bandwidth, administration, management, diagramming" name="Keywords"/>
<meta content="Network Software for Windows: Administration, Management, Inventory, Monitoring, Mapping, Diagramming, File Search" name="Description"/>
<link href="https://www.10-strike.com" rel="canonical"/>
<link href="https://www.10-strike.com/" hreflang="en-us" rel="alternate"/>
<link href="https://www.10-strike.ru/" hreflang="ru" rel="alternate"/>
<meta content="67c7c002dfea62ae" name="yandex-verification"/>
<style type="text/css">
ol,p,ul ul ul{list-style-type:none}h1,h2{margin:10px 0 7px;padding:0 0 4px}h1,h2,h3{color:#556296;font-weight:700}#page,#pager{width:900px;margin:0 auto}.im-item,.langs a,.nav a{display:inline-block}.feature ul,li,ul{list-style-position:inside}ul{list-style-type:circle}ul ul{list-style-type:disc}ol,p{margin:0;padding:0}.feature ul,.feature ul li{list-style-type:disc;padding:0}body{margin:0;padding:0;background:url(/img/bg.jpg) top repeat-x;font-family:Calibri,"Lucida Grande","Arial rounded",Tahoma,Helvetica,sans-serif,Arial;font-size:15px}img{border:none}a,a:link,a:visited{color:#336296;text-decoration:none}a:hover{text-decoration:underline}.bcrumps a,.im-item p a,.nav a{text-decoration:none}h1{font-size:22px;border-bottom:1px solid #c2c2c2}h2{font-size:20px}h3{padding:3px 0 1px;text-align:left;font-size:19px}.im-item,.im-nav,.langs,.nav{text-align:center}#page{background:url(/img/pagebg.jpg) top left no-repeat}#pager{background:url(/img/pagebgrus.jpg) top left no-repeat}#header{position:relative;height:95px}.langs,.logo,.tel{position:absolute}.logo{top:22px;left:5px;width:345px;height:48px}.tel{top:70px;left:130px;font-size:16px}.nav,.nav a{color:#326296;font-size:14px}.langs{background:url(/img/langbg.png) top no-repeat;top:0;right:0;padding:7px 12px 6px}.langs a{margin:0 4px}.nav{background:url(/img/navbg.jpg)}.nav a{padding:7px 12px;margin:0 6px}.nav a:hover{color:#fff;background:url(/img/navhover.jpg)}.im-nav{background:url(/img/im-blockbg.png) top repeat-x}.im-left{background:url(/img/imleft.png) top left no-repeat}.im-right{background:url(/img/imright.png) top right no-repeat}.im-item{background:url(/img/imi.png);width:115px;height:115px;margin:12px 3px 13px;position:relative;padding:20px 0 0}.im-item p{margin:10px 0 0;padding:0;font-family:Arial;font-size:11px;font-weight:400;border-bottom:none}.im-item p a{color:#fff}.im-item img{width:65px;height:60px}#left{float:left;width:650px}.bcrumps{font-size:14px;color:#999;margin:5px 0 10px}.bcrumps a{color:#337310}.bcrumps a:hover{text-decoration:underline}.content{font-size:15px;text-align:justify}.content h1{font-size:22px;color:#556296;margin:10px 0 7px;padding:0 0 4px;border-bottom:1px solid #c2c2c2;font-weight:700}.content h2{font-size:20px;color:#556296;border:none;padding:0}.abtn,.dbtn{padding:23px 0 20px 90px}.content p{margin:9px 0}.abtn,.bbtn,.dbtn{cursor:pointer;display:block;margin:5px 0;line-height:100%;color:#fff!important;font-size:23px}.dbtn{background:url(/img/dbtn.png) no-repeat;width:306px}.dbtn:hover{color:#fff!important;background:url(/img/dbtn.png) 0 -66px no-repeat;width:396px;font-size:23px}.abtn{background:url(/img/abtn.png) no-repeat;width:306px}.abtn:hover{color:#fff!important;background:url(/img/abtn.png) 0 -66px no-repeat;width:396px;font-size:23px}.bbtn{background:url(/img/bbtn.png) no-repeat;width:306px;padding:27px 0 22px 90px}.bbtn:hover{color:#fff!important;background:url(/img/bbtn.png) 0 -74px no-repeat;width:396px;font-size:23px}.sbtn,.vbtn{color:#fff!important;width:116px;font-size:19px;cursor:pointer;margin:5px 0;display:inline-block;line-height:100%}.sbtn{background:url(/img/sbtn.png) no-repeat;padding:9px 0 12px 63px}.sbtn:hover{background:url(/img/sbtn.png) 0 -39px no-repeat}.vbtn{background:url(/img/vbtn.png) no-repeat;padding:11px 0 12px 63px}.vbtn:hover{background:url(/img/vbtn.png) 0 -41px no-repeat}.feature{padding:0 0 0 10px}.feature table{font-size:100%}.feature h1{font-size:22px;color:#556296;margin:10px 0 7px;padding:0 0 4px;border-bottom:1px solid #c2c2c2;font-weight:700}.feature h2,.feature h3{padding:3px 0 1px;font-weight:700;color:#556296}.feature h2{text-align:center;font-size:20px}.feature h3{text-align:left;font-size:19px}.feature img{float:left;padding:10px 10px 0 0;border:0}.feature ul{margin:0}.feature p{margin:0 0 5px;padding:0}.feature ul li{margin:0}.ft{color:#753;font-size:18px}.story{padding:0 0 0 10px}.story p{padding:0 0 5px;margin:0 0 5px}.story img{border:0;margin:5px}.prd-head,.relatedLinks h3,.story h1{border-bottom:1px solid #c2c2c2}.story h1{color:#556296;font-size:22px;margin:10px 0 7px;padding:0 0 4px;font-weight:700}.story h2,.story h3{color:#556296;padding:3px 0 1px;font-weight:700}.story h2{text-align:center;font-size:20px}.story h3{text-align:left;font-size:19px}#footer,.pict,.soft-item .left{text-align:center}.overview{margin:7px 0 7px 7px}.overview img{width:35px;height:35px;float:left;margin:0 3px 0 0}.buybtn,.shotbtn{height:16px text-decoration: none}.overview li{margin:7px 0}.overview li strong{color:#235b0d}.over-text{margin:0;padding:0 0 4px}.pict{margin:15px 0}ul.list1{margin:4px 0 4px 10px;line-height:150%}ul.list1 li{list-style-type:square!important;margin:0 0 0 20px}.clear{clear:both}.gray{color:#313131}ul.list2{margin:10px 0!important}ul.list2 li{list-style-image:url(/img/list2.gif);margin:4px 0 4px 35px}.soft-item{width:305px;float:left;margin:5px 20px 15px 0;background:url(/img/soft-line.gif) bottom repeat-x;padding:0 0 7px}.soft-item .title{display:block;font-size:15px;margin:0 0 5px}.soft-item .left{float:left;width:115px}.soft-item .right{margin:0 0 0 116px;font-size:13px}.details{margin:5px 0 0;background:url(/img/detarrow.gif) left bottom no-repeat;padding:0 0 0 15px}.buybtn,.downbtn,.shotbtn{cursor:pointer;display:inline-block;padding:0 0 4px;width:84px;margin:0 0 3px}.details a{color:#3c9d07;font-size:14px;text-decoration:none}.details a:hover{text-decoration:underline}.downbtn,.subscribe{color:#fff!important;text-decoration:none}.buybtn{color:#fff!important;background:url(/img/buybtn.png) no-repeat}.buybtn:hover{background:url(/img/buybtn.png) 0 -21px no-repeat}.shotbtn{color:#fff!important;background:url(/img/shotbtn.png) no-repeat}.shotbtn:hover{background:url(/img/shotbtn.png) 0 -21px no-repeat}.downbtn{background:url(/img/downbtn.png) no-repeat;height:16px}.downbtn:hover{background:url(/img/downbtn.png) 0 -21px no-repeat}#right{width:232px;float:right}.subscribe{display:block;width:232px;font-size:22px;background:url(/img/subscr.jpg) no-repeat;padding:15px 0 10px 30px}.subscribe:hover{text-decoration:underline}.relatedLinks{margin:7px 0 0;padding:5px}.relatedLinks h3{font-size:17px;padding:2px 0 4px 1px;font-weight:400;margin:0}.relatedLinks ul li{margin:3px 0 3px 7px;padding:0;list-style-type:none}.relatedLinks ul{margin:0;padding:0;list-style-type:none}.prod-list{background:#f3f3f3;margin:7px 0;padding:5px}.prd-head{font-size:16px;padding:2px 0 4px 1px}.prod-item{font-size:12px;padding:1px 0 4px;border-bottom:1px dotted #3c9fec;margin:3px 0}.prod-item img{width:40px;height:40px;float:left}.prod-text{margin:0 0 0 45px}.prod-text a{font-size:14px;color:#337310;text-decoration:none}.prod-text a:hover{text-decoration:underline}.prod-promo{width:97%;margin:2px;padding:2px;background-color:red}.prod-promo a{text-decoration:none;padding:5px 0;display:block;font-size:110%;font-weight:700}.prod-promo a:hover{background-color:#ff3030}#footer{background:url(/img/nav.png) top no-repeat #e8e8e8;font-size:12px;padding:10px 0;margin:10px 0 0}.footer_nav{margin:0 0 5px}.footer_nav a{color:#000;display:inline-block;margin:0 10px;text-decoration:none}.footer_nav a:hover{text-decoration:underline}.copy{color:#515151}@media print{#footer,#header,#right,.im-nav,.nav{display:none}body{font:14pt georgia,serif}h1{font-size:22pt}h2{font-size:20pt}h3{font-size:18pt}}@media screen and (max-width:50em){#left,#page,#pager{width:100%}#right,.im-nav{display:none}#left{float:left}.nav a{font-size:15px}.bcrumps{font-size:16px}}
</style>
<!--[if lte IE 7]>
	<link rel="stylesheet" href="/ie6-7.css" type="text/css">
<![endif]-->
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="en-us" http-equiv="Content-Language"/>
<meta content="global" name="distribution"/>
<meta content="2018 (c) 10-Strike Software, All Rights Reserved" name="copyright"/>
<link href="/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="/touch-icon.png" rel="apple-touch-icon"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-12014432-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-12014432-1');
</script>
</head>
<body>
<div id="page">
<div id="header">
<a class="logo" href="/" title="Home"></a>
<div class="langs">
<a href="#"><img alt="English version" height="19" src="/img/en.png" title="This is English site" width="24"/></a>
<a href="/de/"><img alt="switch to German" height="19" src="/img/de.png" title="Go to German site" width="24"/></a>
<a href="/rus/"><img alt="switch to Russian" height="19" src="/img/ru.png" title="Switch to Russian language" width="24"/></a>
</div>
</div>
<div class="nav">
<a href="/news.shtml">News</a> |
		<a href="/products.shtml">Products</a> | <a href="/download.shtml">Download</a> |
		<a href="/order.shtml">Order</a> | <a href="/partners.shtml">Partners</a> |
		<a href="/contacts.shtml">Contacts</a> | <a href="/community/">Forum</a> |
		<a href="/blog/">Blog</a>
</div>
<div class="im-nav">
<div class="im-left">
<div class="im-right">
<div class="im-item">
<a class="im-i" href="/networkinventoryexplorer/"><img alt="Computer Inventory Software" height="60" src="/img/im-inv.png" width="65"/></a>
<p><a href="/networkinventoryexplorer/">Network Inventory Software</a></p>
</div>
<div class="im-item">
<a class="im-i" href="/network-monitor/"><img alt="LAN Monitoring Software" height="60" src="/img/im-mon.png" width="65"/></a>
<p><a href="/network-monitor/">Network Monitoring Software</a></p>
</div>
<div class="im-item">
<a class="im-i" href="/network-diagram/"><img alt="LAN Diagram Software" height="60" src="/img/im-map.png" width="65"/></a>
<p><a href="/network-diagram/">Network Diagram Software</a></p>
</div>
<div class="im-item">
<a class="im-i" href="/lanstate/"><img alt="Network Mapping and Visual Management" height="60" src="/img/im-serch.png" width="65"/></a>
<p><a href="/lanstate/">Network Mapping and Management</a></p>
</div>
<div class="im-item">
<a class="im-i" href="/networkinventoryexplorer/software-inventory.shtml"><img alt="Software and License Auditing" height="60" src="/img/im-lis.png" width="65"/></a>
<p><a href="/networkinventoryexplorer/software-inventory.shtml">Installed Software &amp; License Auditing</a></p>
</div>
<div class="im-item">
<a class="im-i" href="/networkinventoryexplorer/"><img alt="Hardware Tracking Software" height="60" src="/img/im-hard.png" width="65"/></a>
<p><a href="/networkinventoryexplorer/">Hardware Tracking Software</a></p>
</div>
<div class="im-item">
<a class="im-i" href="/bandwidth-monitor/"><img alt="Bandwidth Monitoring Software" height="60" src="/img/im-traf.png" width="65"/></a>
<p><a href="/bandwidth-monitor/">Bandwidth Monitoring Software</a></p>
</div>
</div>
</div>
</div>
<div id="left">
<div class="bcrumps">
  You are here: Home</div>
<h1 align="center" id="pageName">Network Software for Windows: Monitoring, Administration, Management, Inventory, Mapping, Diagramming</h1>
<div class="feature">
<p><strong><img align="left" alt="10-Strike Software logo" height="59" src="/img/10strike-logo60.jpg" title="10-Strike Software logo" width="63"/>10-Strike Software</strong> is a network software developing company.  The product line includes <a href="/network-monitor/">network monitoring software</a>, <a href="/networkinventoryexplorer/"> computer inventory software</a>, <a href="/lanstate/">network mapping and management software</a>, <a href="/network-diagram/">LAN topology discovery and diagramming program</a>, <a href="/network-file-search/">file searching program</a>, <a href="/bandwidth-monitor/">bandwidth monitoring and Internet traffic usage tool</a>, and <a href="/connectionmonitor/">share remote user access auditing program</a>.</p>
<p>We sell our products world-wide since 1999. In 2003, the company concentrated on creating and marketing administrative and networking applications (primarily for the Windows platform).</p>
<p> </p>
<h2>Our Networking Products</h2>
<h3>Computer Inventory Software</h3>
<p><img alt="Network Computer Inventory" height="32" src="networkinventoryexplorer/network-inventory-icon.png" title="network inventory software" width="32"/>
"10-Strike Network Inventory Explorer" is a powerful <a href="/networkinventoryexplorer/">computer inventory software</a>  which allows you to maintain  the network  computer inventory database. View hardware and software  on computers remotely, track hardware and software changes, plan upgrades, generate reports on installed applications and hardware, mine data and export it to your database. The Pro version has got advanced SAM functions.</p>
<br/>
<h3>Network Monitoring Software</h3>
<p><img alt="Network Monitor" height="32" src="network-monitor/network-monitor-icon.png" title="network monitoring software" width="32"/>
"10-Strike Network Monitor" is a <a href="/network-monitor/"> monitoring program</a> which allows you to obtain online data on the time a device becomes available and the time it goes out, and thus ensure a prompt response to various events. See response times graphs in real-time, configure alerts, and be notified when your services work slower or go down.</p>
<br/>
<p><img alt="Network Monitoring software - LANState" height="32" src="lanstate/lanstate-icon.png" title="visual network monitoring software with graphical map" width="32"/>
"10-Strike LANState" is a simple NMS. This <a href="/lanstate/">visual network mapper</a> and monitor allows you to see the state of your network
at any time on a graphical map. Monitor your servers, switches, drives, services, and databases in real time. Configure alerts and be notified on failures or any other specific events. Manage your  hosts, workstations, and servers using the program's visual LAN map.</p>
<br/>
<p><img alt="Bandwidth Monitor" height="32" src="bandwidth-monitor/bandwidth-monitor-icon.png" title="network bandwidth monitoring software" width="32"/>
"10-Strike Bandwidth Monitor" is a <a href="/bandwidth-monitor/">traffic and bandwidth monitoring program</a> which allows you to obtain online data on the traffic volume utilized by  hosts on your LAN (switches, computers, routers, etc.), calculates and displays the current bandwidth. See the bandwidth graphs in real-time, configure alerts and be notified when hosts download more traffic than the configured limit.</p>
<br/>
<h3>Network Diagram and Mapping Software</h3>
<p><img alt="network diagram" height="32" src="network-diagram/network-diagram-icon.png" title="network diagrammer software" width="32"/>
"10-Strike Network Diagram" <a href="/network-diagram/">builds diagrams of local networks and draws network topology</a>
by scanning switches using the SNMP protocol. The program contains a feature-rich diagram editor and an object library with
a wide range of  device icons. You can save diagrams in popular graphical formats, copy and paste it to a document,
or export it to Microsoft Visio for further processing. Or edit network diagrams just in our program because it is much faster than Visio!</p>
<br/>
<p><img alt="Network Map and Monitor - LANState" height="32" src="lanstate/lanstate-icon.png" width="32"/>
"10-Strike LANState" does the network diagramming as well (using the same scanning engine). But in addition to the <a href="/lanstate/">network mapping</a> functions, LANState monitors the mapped hosts in the real time displaying state of your hosts as icons on a graphical map. You can always see active and inactive hosts. This basic NMS allows you to discover new hosts, monitor switch ports and other parameters via SNMP, and manage Windows computers.</p>
<br/>
<p><img alt="network IP LAN scanner" height="32" src="network-scanner/network-scanner-icon32.png" width="32"/>
"10-Strike Network Scanner" is a <a href="/network-scanner/">free lightweight LAN and TCP port scanner</a> and network address detecting software. Scan your LAN, find all connected devices, and detect their IP addresses in a few seconds. The program is absolutely free!</p>
<br/>
<h3>Network File Searching Software</h3>
<p><img alt="Search files in local network" height="32" src="network-file-search/network-file-search_icon.png" title="network file searching software" width="32"/>
The "10-Strike Network File Search" program can <a href="/network-file-search/">search files on your LAN shares and FTP servers</a>.
Specify a search string, quickly find the necessary files, and save handy reports. The program  uses fast multithreading searching engine and works very fast. It allows you to search content in documents as well.</p>
<br/>
<h3> Share and Folder Access Monitoring Software</h3>
<p><img alt="Connection Monitor" height="32" src="connectionmonitor/connection-monitor-icon.png" title="network share remote user access monitoring program" width="32"/>
"10-Strike Connection Monitor" <a href="/connectionmonitor/">monitors remote users' connections and their access to shares, shared folders, and files</a>, records all 
sessions to a log file. The program generates screen, sound, and e-mail notifications on connections. Monitor file and folder access, audit file deletion and creation. Learn who deleted files in a share. The log file stores the
exhaustive data on connected users, folders and files accessed.</p>
<br/>
<h3>Networking Software Bundles</h3>
<p><img alt="Networking Software" height="52" src="img/boxes120.jpg" title="networking Sowftare" width="120"/>
We offer all our networking products bundled together with a 40% discount. Buy one of our bundles and save money! We offer <b>"10-Strike Full Network Admin Bundle</b>" including all our 7 networking products listed above offering a comprehensive set of functions which can be useful for any system administrator, IT professional, or LAN engineer.
<a href="/bundles.shtml">Learn more about bundles...</a></p>
<p> </p>
</div>
<div class="story">
<h3>Resellers and Customers</h3>
<p><i>10-Strike Software</i> has <a href="partners.shtml">resellers</a> worldwide. Primary resellers are located in the USA, Germany, Russian Federation, Japan, Switzerland, and Turkey.</p>
<p>Our networking products are installed and used in many thousands of organizations all over the world.</p>
<div align="center">
<img alt="our clients" class="responsive-img" height="270" src="/img/logos/logos-en.jpg" width="640"/>
</div>
<p>Free products have hundreds of thousands installations. The company takes part in various IT events like expos and conferences all over the world.</p>
<p align="center"><img align="absmiddle" alt="10-Strike's team at expo" height="170" src="/img/expo2-250.jpg" width="250"/> <img align="absmiddle" height="166" src="/img/expo1.jpg" width="250"/></p>
<h2 align="center">Find a Solution for You!</h2>
<p><a href="/network-monitor/monitor-network-devices.shtml">Network Device Monitoring</a></p>
<p><a href="/network-monitor/pro/switch-monitoring.shtml">Monitor Managed Switches (Cisco, D-Link, HP, Huawei, etc.)</a></p>
<p><a href="/network-monitor/server-monitoring.shtml">Server Monitoring</a></p>
<p><a href="/lanstate/network-mapping.shtml">Network Topology Mapping</a></p>
<p><a href="/lanstate/">Managing Network Using Graphic Map</a></p>
<p><a href="hardware-inventory.shtml">Hardware Inventory</a></p>
<p><a href="software-inventory.shtml">Software Inventory</a></p>
<p><a href="network-inventory.shtml">Network Inventory</a></p>
<p><a href="asset-tracking.shtml">Track Computer and Hardware Assets</a></p>
<p><a href="/press/network-computer-inventory-tool.shtml">Network Computer Inventory</a></p>
<p><a href="asset-management.shtml">Network Asset Management</a></p>
<p><a href="/networkinventoryexplorer/track-hardware.shtml">Track Hardware Changes on Remote Computers</a></p>
<p><a href="/networkinventoryexplorer/track-software.shtml">Track Software Changes on Remote Computers</a></p>
<p><a href="/lanstate/network-diagram.shtml">Network Topology Scanner and Diagram Builder</a></p>
<p><a href="/networkinventoryexplorer/plan-upgrade.shtml">Plan and Prepare PC Hardware Upgrades</a></p>
<p><a href="/network-monitor/pro/cpu-temperature-monitoring.shtml">Monitor CPU Temperature, Fan Speed, and Voltage</a></p>
<p><a href="/network-monitor/pro/ip-camera-monitoring.shtml">Monitor Surveillance CCTV Cameras, IP Cameras, and DVR</a></p>
<p><a href="/network-monitor/pro/hdd-smart-monitoring.shtml">Monitor Hard Drive's SMART Parameters</a></p>
<p><a href="/network-scanner/">Scan Network and Search for IP Addresses, Computers, Tablets, etc.</a> (FREE program!)</p>
<p> </p>
</div>
</div>
<!-- Content and Left part END here; Right part goes here -->
<div id="right">
<a class="subscribe" href="/subscribe.php" title="Subscribe to our newsletter!">Subscribe</a>
<h3>Our News</h3>
<p><b>December 23, 2020</b><br/>
Updated <a href="/lanstate/">LANState</a> to the version 9.6.
<a href="/lanstate/history.shtml">What's new...</a></p>
<div style="clear:both;"> </div>
<p><b>December 4, 2020</b><br/>
Updated 10-Strike <a href="/bandwidth-monitor/">Bandwidth Monitor</a> to the v4.0. Added the Agent data saving. Learn more: <a href="/bandwidth-monitor/history.shtml">What's new...</a></p>
<div style="clear:both;"> </div>
<p><b>November 25, 2020</b><br/>
Updated 10-Strike <a href="/network-monitor/">Network Monitor</a> (Pro) to the v6.6. Added new functions including the <a href="/network-monitor/help/monitoring/rtsp-videostream-monitoring.shtml">RSTP monitoring</a> (for <a href="/network-monitor/pro/ip-camera-monitoring.shtml">CCTV systems</a>) and <a href="/network-monitor/help/monitoring/opc-server-monitoring.shtml">OPC monitoring</a> for CNC machines. Learn more in our blog <a href="/blog/new-network-monitor-6-6-controls-cctv-video-camera-bitrate-and-cnc-machines/">What's new...</a></p>
<div style="clear:both;"> </div>
<p><b>August 14, 2020</b><br/>
Added a new article on the <a href="/network-monitor/ups-monitoring.shtml">UPS monitoring using SNMP over network</a> to our site.</p>
<div style="clear:both;"> </div>
<p><b>August 3, 2020</b><br/>
Updated our huge <a href="/network-monitor/pro/ip-camera-monitoring.shtml">CCTV system monitoring</a> article and described 10 methods for doing this.</p>
<div style="clear:both;"> </div>
<p><b>July 10, 2020</b><br/>
Updated our <a href="/network-scanner/">free network scanner</a> to the v4.0.</p>
<div style="clear:both;"> </div>
<p><b>June 18, 2020</b><br/>
Updated our <a href="/networkinventoryexplorer/">network inventory program</a> to the v9.05.
Improved the SNMP device detection. <a href="/networkinventoryexplorer/history.shtml">What's new...</a></p>
<div style="clear:both;"> </div>
<!-- Related here -->
</div>
<!-- RIGHT END -->
<div class="clear"></div>
<p> </p>
<div id="footer">
<div class="footer_nav">
<a href="/privacy.shtml">Privacy Policy</a>    |    <a href="/links.shtml">Links</a>
</div>
<div class="copy">
				Copyright © 1998-2020, 10-Strike Software
			</div>
</div>
</div> <!--end page-->
</body>
</html>
