<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="https://www.w3.org/1999/xhtml">
<head>
<!-- <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true"> -->
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="lt" name="language"/>
<link href="/css/style.css?v=1" rel="stylesheet" type="text/css"/>
<meta content="Sveiki atvykę į internetinį tarptautinių žodžių žodyną! Šiame puslapyje galite nesudėtingai ir greitai ieškoti tarp daugiau nei 20 000 tarptautinių žodžių." name="description"/>
<link href="/assets/9cc90252/jui/css/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="/assets/9cc90252/jquery.min.js" type="text/javascript"></script>
<title>Tarptautinių žodžių žodynas - Tarptautiniai žodžiai - Zodziai.lt</title>
<meta content="1NknGP_fi5yav9EV3e_N7yToL-F5dssV_LbwuHeb0o0" name="google-site-verification"/>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-2067074081486301",
          enable_page_level_ads: true
     });
</script>
<!-- setupad -->
<script src="//lv.adocean.pl/files/js/aomini.js" type="text/javascript"></script>
<!--setupad GAM-->
<script async="" src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<script>
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() {



            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>
<!--setupad GAM-->
</head>
<body>
<!-- setupad GAM header -->
<div align="center" class="banner-header" style="padding:10px 0 10px 0">
<!-- /1007073/zodziai.lt_970x250_top -->
<div id="div-gpt-ad-1592814273242-0">
<script>
                if($(window).width() > 979) {
                    googletag.cmd.push(function () {
                        googletag.defineSlot('/1007073/zodziai.lt_970x250_top', [[970, 90], [970, 250], [728, 90]], 'div-gpt-ad-1592814273242-0').addService(googletag.pubads());
                        googletag.display('div-gpt-ad-1592814273242-0');
                    });
                }
            </script>
</div>
</div>
<div id="fb-root"></div>
<!--      Setup.zodziai.lt.zodziai.lt_300x250_mobile_top-->
<div class="mobileHeader">
<script src="//lv.adocean.pl/files/js/aomini.js" type="text/javascript"></script>
<div data-ao-pid="_zLwP6ct12MZhtI8ynNJ7HGsnOGHyDADvJjsRd5___j.E7" style="display: none;"></div>
<script type="text/javascript">
        /* (c)AdOcean 2003-2020 */
        /* PLACEMENT: Setup.zodziai.lt.zodziai.lt_300x250_mobile_top */
        if(800 > $(window).width()) {
            (window['aomini'] || (window['aomini'] = [])).push({
                cmd: 'display',
                server: 'lv.adocean.pl',
                id: '_zLwP6ct12MZhtI8ynNJ7HGsnOGHyDADvJjsRd5___j.E7',
                consent: null
            });
        }
    </script>
</div>
<div class="container">
<div class="header">
<a class="logo" href="/" title="Zodziai.lt"></a>
<div class="count"><span>20976</span> <br/>terminų</div>
<div class="search">
<form action="/paieska" id="search" method="get">
<input id="word" name="word" type="text"/> <button id="trans"></button>
</form>
<div class="clear"></div>
</div>
<div class="index">
<ul>
<li><a href="/raide?letter=a">A</a></li>
<li><a href="/raide?letter=b">B</a></li>
<li><a href="/raide?letter=c">C</a></li>
<li><a href="/raide?letter=%C4%8D">Č</a></li>
<li><a href="/raide?letter=d">D</a></li>
<li><a href="/raide?letter=e">E</a></li>
<li><a href="/raide?letter=f">F</a></li>
<li><a href="/raide?letter=g">G</a></li>
<li><a href="/raide?letter=h">H</a></li>
<li><a href="/raide?letter=i">I</a></li>
<li><a href="/raide?letter=j">J</a></li>
<li><a href="/raide?letter=k">K</a></li>
<li><a href="/raide?letter=l">L</a></li>
<li><a href="/raide?letter=m">M</a></li>
<li><a href="/raide?letter=n">N</a></li>
<li><a href="/raide?letter=o">O</a></li>
<li><a href="/raide?letter=p">P</a></li>
<li><a href="/raide?letter=q">Q</a></li>
<li><a href="/raide?letter=r">R</a></li>
<li><a href="/raide?letter=s">S</a></li>
<li><a href="/raide?letter=%C5%A1">Š</a></li>
<li><a href="/raide?letter=t">T</a></li>
<li><a href="/raide?letter=u">U</a></li>
<li><a href="/raide?letter=v">V</a></li>
<li><a href="/raide?letter=z">Z</a></li>
<li><a href="/raide?letter=%C5%BE">Ž</a></li>
</ul>
</div>
</div>
<div class="content">
<div class="text">
<h1>Sveiki atvykę į internetinį tarptautinių žodžių žodyną!</h1>
<p>
        Zodziai.lt - tai tarptautinių žodžių žodynas lietuvių kalba. Jame rasite daugiau nei 20000 tarptautinių žodžių. Šiuo žodynu labai paprasta naudotis - paprasčiausiai įveskite jus dominantį žodį į paieškos laukelį ir paspauskite „Enter“ klavišą. 
Žodis „tarptautinis“ - tai skolinys, kuris pasikartoja keliose kalbose ir turi tokią pat arba panašią reikšmę bei etimologiją. Europietiški tarptautiniai žodžiai daugiausiai kilę iš lotynų ir graikų kalbų.
Tarptautiniai žodžiai dažniausiai atsiranda kartu su naujomis technologijomis. Pavyzdžiui, kompiuterijos žodyne galima rasti daugybę iš anglų kalbos kilusių žodžių, tokių kaip kompiuteris, diskas ar monitorius (computer, disc, monitor). Nauji išradimai, politinės institucijos, maisto produktai, laisvalaikio veikla, mokslas, technologijų pažanga - visose naujose srityse yra kuriama naujos leksemos, tokios kaip, bionika, kibernetika, genai, kava, šokoladas ir t.t. Kai kurie tarptautiniai žodžiai atsiranda dėl gyventojų, kalbančių viena kalba ir gyvenančių teritorijoje, kurioje kalbama kita kalba. 
</p><p>Plintant tarptautiniams žodžiams, atsirado nauja sąvoka - žodžiai hibridai. Hibridai- tai tokie žodžiai, kurių viena dalis yra paimta iš vienos kalbos, o kita - iš kitos kalbos. Patys populiariausi hibridai yra susiformavę iš lotyniškų ir graikiškų žodžių junginių. Pavyzdžiui žodis "automobilis" yra hibridas, susidedantis iš graikiško žodžio autos (pats) ir lotyniško žodžio mobilis (judamas).
        </p>
</div>
<div class="right-side">
<div class="clear"></div>
<script type="text/javascript">
          window.___gcfg = {lang: 'lt'};        
          (function() {
            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
          })();
        </script>
<div class="clear"></div>
<div class="banner-side">
<!-- /1007073/zodziai.lt_300x600_sidebar_1 -->
<div class="sidebar" id="div-gpt-ad-1592815681930-0">
<script>
                    if($(window).width() >= 979) {
                        googletag.cmd.push(function () {
                            googletag.defineSlot('/1007073/zodziai.lt_300x600_sidebar_1', [[300, 600], [300, 250]], 'div-gpt-ad-1592815681930-0').addService(googletag.pubads());
                            googletag.display('div-gpt-ad-1592815681930-0');
                        });
                    }
                </script>
</div>
</div>
<div class="friends">
<h2>Draugai</h2>
<a href="http://lietuviuangluzodynas.lt/" target="_blank" title="lietuviuangluzodynas.lt">Lietuvių anglų žodynas</a>
<a href="http://anglulietuviuzodynas.lt/" target="_blank" title="anglulietuviuzodynas.lt">Anglų lietuvių žodynas</a>
<a href="http://www.receptuknyga.lt/" target="_blank" title="receptai">Receptai</a>
</div>
<div class="friends">
<div align="left">
<a href="https://www.hey.lt/details.php?id=tamoshiunas" rel="nofollow" target="_blank"><img alt="Hey.lt - Interneto reitingai, lankomumo statistika, lankytoju; skaitliukai" border="0" height="31" src="https://www.hey.lt/count.php?id=tamoshiunas&amp;width=1680&amp;height=1050&amp;color=32&amp;referer=" width="88"/></a>
</div>
</div>
</div>
<div class="clear"></div> </div>
<div align="center" class="header-banner" style="margin-bottom:10px">
<!-- /1007073/zodziai.lt_970x90_bottom -->
<div id="div-gpt-ad-1592817128428-0">
<script>
                  if($(window).width() > 979) {
                      googletag.cmd.push(function () {
                          googletag.defineSlot('/1007073/zodziai.lt_970x90_bottom', [[970, 90], [728, 90]], 'div-gpt-ad-1592817128428-0').addService(googletag.pubads());
                          googletag.display('div-gpt-ad-1592817128428-0');
                      });
                  }
              </script>-
          </div>
</div>
<div class="footer">
      &amp;copy 2009-2021  Zodziai.lt - tarptautinių žodžių žodynas. 
    </div>
</div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-37887390-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-37887390-3');
</script>
<div id="ZO00_Pixel"></div> <!-- Adnet pixel kodas -->
<script src="/assets/9cc90252/jui/js/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript">
/*<![CDATA[*/
jQuery(function($) {

    $("#search").submit(function(){
      var word = $("#word").val();
      word = word.trim();      
      if(word.length == 0)
      {
        return false;
      } 
    })
    
    $("#trans").click(function(){
      var word = $("#word").val();
      word = word.trim();
      if(word.length > 1)
      {
        $("#search").submit();  
      }
    })    
  
jQuery('#word').autocomplete({'minLength':'2','select':function(event, ui){$("#word").val(ui.item.label);$("#search").submit();},'source':'/site/suggest'});

          $('#word').data('autocomplete')._renderItem = function( ul, item ) {
            var re = new RegExp( '(' + $.ui.autocomplete.escapeRegex(this.term) + ')', 'gi' );
            var highlightedResult = item.label.replace( re, '<span>$1</span>' );
            return $( '<li></li>' )
              .data( 'item.autocomplete', item )
              .append( '<a>' + highlightedResult + '</a>' )
              .appendTo( ul );
          };
        
});
/*]]>*/
</script>
</body>
</html>
