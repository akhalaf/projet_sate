<!DOCTYPE HTML>
<html>
<head>
<title>James Bond 007 - The Site's Bond, James Bond</title>
<meta charset="utf-8"/>
<meta content="Learn about the James Bond lifestyle and explore the rich history of 007: the exotic Bond girls, the devious villains, the fast cars and the 24 films from the 50 year old franchise." name="description"/>
<link href="https://media.007james.com/static/main.8.css" rel="stylesheet"/>
<link href="https://media.007james.com/static/responsive.4.css" rel="stylesheet"/>
<meta content="width=device-width,initial-scale=1,minimum-scale=1" name="viewport"/>
</head>
<body>
<div class="main">
<div class="header">
<div class="search">
<form action="/s/search.php" method="get">
        Search Site:<br/><input id="q" name="q" type="text"/><br/>
<input type="submit" value="Search"/>
</form>
</div>
<div class="space">
<a href="https://www.twitter.com/007JamesNews"><span class="sp-twitter"></span></a> <a href="https://www.twitter.com/007JamesNews">Follow us</a> on Twitter
      <br/><a href="https://www.facebook.com/007Jamescom"><span class="sp-facebook"></span></a> <a href="https://www.facebook.com/007Jamescom">Like us</a> on Facebook
    </div>
<a href="/"><img alt="007James - The Site's Bond, James Bond." class="logo" src="https://media.007james.com/static/logo.jpg"/></a>
<div class="menu">
<ul class="nav">
<li><a href="/lists/">Lists</a></li>
<li><a href="/top10/">Top 10s</a></li>
<li><a href="/characters/">Characters</a></li>
<li><a href="/actors/">Cast</a><ul>
<li><a href="/actors/">Actors</a></li>
<li><a href="/actresses/">Actresses</a></li>
</ul>
</li>
<li><a href="/gadgets/">Gadgets</a></li>
<li><a href="/movies/">Movies</a></li>
<li><a href="/quotes/">Quotes</a></li>
<li><a href="/site/map.php">More</a><ul>
<li><a href="/articles/">Articles</a></li>
<li><a href="/crew/">Crew</a></li>
<li><a href="/locations/">Locations</a></li>
</ul>
</li>
</ul>
</div>
</div>
<div class="content">
<div style="text-align:center">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- FIREDT -->
<ins class="adsbygoogle" data-ad-client="ca-pub-2551115369970040" data-ad-format="auto" data-ad-slot="4566616660" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
<div>
<style scoped="">
h3 {
 font-size: 1.35em;
 font-family: "News Cycle", Arial, Helvetica, sans-serif;
}
div.essentials {
   padding:  4px;
   font-size: 0.9em;
}
@media only screen and ( max-width: 675px ) {
   div.essentialsright {
      float: none !important;
   }
}
</style>
<h1>James Bond 007</h1>
<p>Learn about the James Bond lifestyle and explore the rich history of 007: the exotic Bond girls, the devious villains, the fast cars and the 24 films that make up the 50 year old franchise.</p>
<h2>Explore the Essentials</h2>
<div class="essentials">
<div class="essentialsright" style="float: right; width:350px;">
<h3><a href="/articles/list_of_james_bond_cars.php">James Bond Cars</a></h3>
<a href="/articles/list_of_james_bond_cars.php"><img alt="List of All James Bond Cars" class="sthumb" src="/i/search/thumbs/0/134.jpg"/></a>
<p>The complete list of all James Bond cars from the humble Sunbeam Alpine in Dr. No through to the beautiful Aston Martin DB5 in Skyfall, learn about all the iconic Bond cars in one place.</p>
<h3><a href="/articles/list_of_james_bond_girls.php">Bond Girls</a></h3>
<a href="/articles/list_of_james_bond_girls.php"><img alt="List of All James Bond Girls" class="sthumb" src="/i/search/thumbs/0/132.jpg"/></a>
<p>The complete list of all James Bond girls from Honey Ryder in Dr. No through to Sévérine in Skyfall, learn about all the iconic Bond girls in one place.</p>
<h3><a href="/articles/top_10_james_bond_villains.php">Bond Villains</a></h3>
<a href="/articles/top_10_james_bond_villains.php"><img alt="Top 10 James Bond Villains" class="sthumb" src="/i/search/thumbs/0/43.jpg"/></a>
<p>Our list of the Top 10 Bond villains and henchmen from the iconic Oddjob to the larger than life Jaws, to the menacing Le Chiffre. Sorry, no Dr. Evil.</p>
</div>
<div style="width:350px">
<h3><a href="/articles/list_of_james_bond_movies.php">James Bond Movies</a></h3>
<a href="/articles/list_of_james_bond_movies.php"><img alt="List of All James Bond Movies" class="sthumb" src="/i/search/thumbs/0/101.jpg"/></a>
<p>The complete list of official James Bond films, Beginning with Sean Connery, and going through George Lazenby, Roger Moore, Timothy Dalton, Pierce Brosnan and Daniel Craig.</p>
<h3><a href="/articles/who_played_james_bond.php">James Bond Actors</a></h3>
<a href="/articles/who_played_james_bond.php"><img alt="Who Played James Bond: A Complete History" class="sthumb" src="/i/search/thumbs/0/25.jpg"/></a>
<p>A complete history of all the actors who have played James Bond in film, radio and TV. From Barry Nelson through Sean Connery and all the way to Daniel Craig!</p>
<h3><a href="/articles/james_bonds_top_10_close_shaves.php">Close Shaves</a></h3>
<a href="/articles/james_bonds_top_10_close_shaves.php"><img alt="James Bond's Top 10 Closest Shaves" class="sthumb" src="/i/search/thumbs/0/126.jpg"/></a>
<p>Our list of Bond's Top 10 most memorable and tense close shaves, where he narrowly escaped near certain death against explosions, fire, crocodiles and lasers.</p>
</div>
</div><br/>
<h2>James Bond News</h2>
<div class="serp">
<h3><a href="/news/the-drinks-vodka-belvedere-vodka/">The Drink's Vodka, Belvedere Vodka</a></h3>
<div class="iright"><a href="/news/the-drinks-vodka-belvedere-vodka/"><img alt="Belvedere Vodka" src="/i/thumbs/news/111/belvedere.jpg" style="width:200px; height:212px;"/></a></div>
<p>Belvedere Vodka, the premium vodka for those who know the difference, is now the official vodka brand of James Bond and the upcoming film <em>Spectre</em>. In an exclusive gentleman's club in Knightsbridge, London, President of Belvedere Vodka, Charles Gibb, and Head of Mixology, Claire Smith, discussed the partnership and talked about the evolution of the martini and its relationship with James Bond.</p>
<p>Before James Bond gave the martini its glamorous reputation, most were made with gin and stirred. Bond's influence on popular culture created a shift toward the vodka martini, shaken not stirred. Belvedere President, Charles Gibb, said "James Bond is recognized as the most admired and influential tastemaker in the world. We're delighted that Belvedere will be partnering with SPECTRE, our largest global partnership to date."</p><p><a href="/news/the-drinks-vodka-belvedere-vodka/">Continue reading this news item»</a></p>
<h3><a href="/news/interview-with-skyfall-comic-artist-josh-edelglass/">Interview with Skyfall Comic Artist Josh Edelglass</a></h3>
<div class="iright"><a href="/news/interview-with-skyfall-comic-artist-josh-edelglass/"><img alt="Skyfall Comic" src="/i/thumbs/news/110/comic.jpg" style="width:200px; height:232px;"/></a></div>
<p>Today we have an interview with comic writer and artist Josh Edelglass, who
recently published a hilarious series of comics parodying Skyfall and James
Bond. The series was part of his project <a href="http://www.motionpicturescomics.com/">Motion Pictures Comics</a>, which
Josh aptly describes as "A boy and his robot mysteriously gain the power to
jump in and out of movies.  Hilarity ensues."</p>
<p>Q: Tell us a bit more about Motion Picture Comics. How did you come up with the idea of having a man and his robot morph in and out of various films, making fun of them as they go?</p>
<p>Motion Pictures has had a lot of permutations.  Back when I was in college, I drew a daily political cartoon for the school paper.  After I graduated, I started contacting syndicates.  I didn't have much luck with the political cartoon, but I got a few nice responses from people who said if I ever came up with any new ideas, I should try them again.  OK, great, all I needed was a new idea!</p><p><a href="/news/interview-with-skyfall-comic-artist-josh-edelglass/">Continue reading this news item»</a></p>
<h3><a href="/news/book-review-james-bond-50-years-of-movie-posters/">Book Review: James Bond 50 Years of Movie Posters</a></h3>
<div class="iright"><a href="/news/book-review-james-bond-50-years-of-movie-posters/"><img alt="James Bond 50 Years of Movie Posters" src="/i/thumbs/news/109/bond-movies-posters-cover.jpg" style="width:200px; height:276px;"/></a></div>
<p>The 50th anniversary of James Bond has brought forward a wealth of new
publications. While some focus exclusively on <em>Skyfall</em>, many have taken the
opportunity to give a deep retrospective on the entire series. One such
book, <em>James Bond: 50 Years of Movie Posters</em>, was published on
September 3rd by DK, who were kind enough to send a copy for review.</p>
<p><em>50 Years</em> is ordered chronologically, with a chapter covering each
film from <em><a href="/movies/dr_no.php">Dr. No</a></em> through to <em><a href="/movies/skyfall.php">Skyfall</a></em>. It is also very text light, with
90% of the space being pure posters. Each film gets an introduction and each
poster has a short comment or piece of trivia. Other than that, it is posters
galore.</p>
<p>A lot of time and effort has been spent on the layout, which minimizes
wasted space. High resolution portrait posters typically get a full page to
themselves. Landscape posters on the other hand are either two-page spreads,
two to a page, or mixed together with smaller lobby cards and odd shaped
foreign posters...</p>
<p><a href="/news/book-review-james-bond-50-years-of-movie-posters/">Continue reading this news item»</a></p>
<h3><a href="/news/skyfall-full-theatrical-trailer/">Skyfall Full Theatrical Trailer</a></h3>
<div class="iright"><a href="/news/skyfall-full-theatrical-trailer/"><img alt="Skyfall Poster" src="/i/thumbs/news/108/skyfall_poster.jpg" style="width:200px; height:243px;"/></a></div>
<p>A full 2 minute trailer has been released for <em><a href="/movies/skyfall.php">Skyfall</a></em>, the final version before the film's opening day on October 26th. The trailer contains segments from earlier teaser trailers, but also includes some new, previously unseen footage.</p>
<p><em>Skyfall</em>, directed by Bond newcomer <a href="/crew/sam_mendes.php">Sam Mendes</a>, will feature Spanish actor Javier Bardem as villain Raoul Silva, an ex-MI6 agent who is seeking revenge against the service. The star-studded cast also includes Ralph Fiennes as Mallory, Naomie Harris as Eve, Albert Finney as Kincade, and Bérénice Marlohe as Sévérine.</p>
<p>With early critical praise, it has been speculated that <em>Skyfall</em> may
be the first 007 film to bring in over $1 billion at the box office...</p>
<p><a href="/news/skyfall-full-theatrical-trailer/">Continue reading this news item»</a></p>
<h3><a href="/news/press-reviews-for-previews-of-skyfall/">Press Reviews for Previews of Skyfall</a></h3>
<div class="iright"><a href="/news/press-reviews-for-previews-of-skyfall/"><img alt="Bond at the National Gallery in Skyfall" src="/i/thumbs/news/107/bond-national-galary.jpg" style="width:200px; height:133px;"/></a></div>
<p>The press reviews for <em><a href="/movies/skyfall.php">Skyfall</a></em> are
in, and the results are glowing. The exclusive media preview was held
yesterday, Friday 12th October, by EON Productions, and garnered over 1600
reporters.</p>
<p>The Independent says that director <a href="/crew/sam_mendes.php">Sam
Mendes</a> has "...gone back to basics: chases, stunts, fights. At the
same time, he has subtly re-invented the franchise, throwing in far greater
depth of characterization than we're accustomed". After mentioning the
disappointment of <em>Quantum of Solace</em>, they go on to say that
<em>Skyfall</em> is "...certainly one of the best Bonds in recent memory", and
end with "At the age of 50, there is no sign at all that Bond is finished yet."</p><p><a href="/news/press-reviews-for-previews-of-skyfall/">Continue reading this news item»</a></p>
</div>
<div style="clear:right;">
<p style="font-size:28px">
<span style="float:right"><a href="/news/2/">Older News</a> »</span></p>
</div>
</div>
<div style="clear:both;"></div>
</div>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- FIREDB -->
<ins class="adsbygoogle" data-ad-client="ca-pub-2551115369970040" data-ad-format="auto" data-ad-slot="7520083066" style="display:block"></ins>
<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
<div class="related" style="text-align:center">
<h3 style="font-size:1.1em">Popular Articles</h3>
<table class="rtable">
<tr>
<td><a href="/articles/james_bonds_top_10_close_shaves.php"><span style="background-position: 0 0"></span></a></td>
<td><a href="/articles/top_10_james_bond_theme_songs.php"><span style="background-position: -150px 0"></span></a></td>
<td><a href="/articles/top_10_james_bond_villains.php"><span style="background-position: -300px 0"></span></a></td>
<td><a href="/articles/list_of_james_bond_cars.php"><span style="background-position: -600px 0"></span></a></td>
</tr>
<tr>
<td><a href="/articles/james_bonds_top_10_close_shaves.php">Bond's Best Near Death Experiences</a></td>
<td><a href="/articles/top_10_james_bond_theme_songs.php">Top 10 James Bond Theme Songs</a></td>
<td><a href="/articles/top_10_james_bond_villains.php">Top 10 James Bond Villains</a></td>
<td><a href="/articles/list_of_james_bond_cars.php">List of All James Bond Cars</a></td>
</tr>
</table>
</div>
<div class="foot">
<p><span>Copyright © 007james.com 2004-2020</span> <span class="nomobile">|</span>
<a href="/site/terms.php">Terms of Use</a> | <a href="/site/privacy.php">Privacy Policy</a> | <a href="/site/contact/">Contact</a></p>
</div>
</div>
<script>
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

     ga('create', 'UA-958753-1', 'auto');
     ga('require', 'displayfeatures');
     ga('send', 'pageview');
   </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://media.007james.com/static/fimg.js"></script>
<script>
   $(document).ready(function() {
      $('a[href^="/i/"]').attr("rel", "gallery");
      $('a[href^="/i/"]').attr("class", "fimg");
      $('.fimg').fimg();
   });
   </script>
</body>
</html>
