<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Optimiser les textes de votre site Internet avec 1.fr</title>
<meta content="Outil d'optimisation de vos textes, pour de meilleurs classements. Adaptez vos contenus aux attentes des moteurs de recherche." name="description"/>
<meta content="noarchive" name="robots"/>
<meta content="max-age=3600,public" http-equiv="Cache-control"/>
<link href="/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/css/animate.css" rel="stylesheet"/>
<link href="/css/textoptimizer.css" rel="stylesheet"/>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"/>
<link crossorigin="anonymous" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" rel="stylesheet"/>
<link href="/css/aos.css" rel="stylesheet"/>
<link href="/css/please-wait.css" rel="stylesheet" type="text/css"/>
<meta content="authenticity_token" name="csrf-param"/>
<meta content="+rmAXT6ZM6twaOFGA5hRvQTSzFEoJN0jzT5O0oRtTgAiuo2LMGOWi9BTq4jLF34XdVeR/jceiO5nOYPrZgECSQ==" name="csrf-token"/>
<script src="/js/fastclick.js"></script>
<script src="/js/jquery.js"></script>
</head>
<body class="home scrollTop">
<div class="header header1 header_medium">
<div class="joined_us bg_007aab p40 pt20 pb20 font14 color_fff">
<table>
<tr>
<td class="pr20">
<img alt="" src="/i/users_icon.png"/>
</td>
<td>
<p class="font600 lh22 m0">
            104 rédacteur(ice)s<br/>
            48 spécialistes en SEO<br/>
            22 stratégistes de contenu<br/>
            45 professionnel(le)s du marketing<br/>
            106 propriétaires de site web<br/>
</p>
</td>
</tr>
</table>
<div class="spacer h10"></div>
<strong class="font20 color_18caf4 block">
      Inscrit(e)s ces 7 derniers jours.
    </strong>
</div>
<div class="header_top" id="header_top">
<div class="container">
<div class="row">
<div class="col-md-2 col-sm-2 col-xs-2 relative">
<a class="logo logo4" href="/"></a> </div>
<div class="col-md-10 col-sm-10 col-xs-10">
<div class="r">
<ul class="menu">
<li><a href="/o">Optimiser votre texte</a></li>
<li><a href="/q">Idées de contenu</a></li>
<li><a href="/offers">Tarifs</a></li>
</ul>
<a class="btn" href="/sign"><span>Connexion</span></a>
<select class="nice-select wide" onchange="location = this.value;">
<option selected="" value="https://1.fr">FR</option>
<option value="https://textoptimizer.com/update_lang?lang=ar">AR</option>
<option value="https://textoptimizer.com/update_lang?lang=da">DA</option>
<option value="https://textoptimizer.com/update_lang?lang=de">DE</option>
<option value="https://textoptimizer.com/update_lang?lang=en">EN</option>
<option value="https://textoptimizer.com/update_lang?lang=es">ES</option>
<option value="https://textoptimizer.com/update_lang?lang=it">IT</option>
<option value="https://textoptimizer.com/update_lang?lang=nl">NL</option>
<option value="https://textoptimizer.com/update_lang?lang=no">NO</option>
<option value="https://textoptimizer.com/update_lang?lang=pt">PT</option>
<option value="https://textoptimizer.com/update_lang?lang=fi">FI</option>
<option value="https://textoptimizer.com/update_lang?lang=sv">SV</option>
<option value="https://textoptimizer.com/update_lang?lang=hi">हिन्दी</option>
<option value="https://textoptimizer.com/update_lang?lang=ja">日本語</option>
</select>
</div>
</div>
</div>
</div>
<div id="mobile_menu_overlay" style="display:none">
<ul>
<li><a href="/sign">Connexion</a></li>
<li><a href="/o">Optimiser</a></li>
<li><a href="/q">Trouver des idées</a></li>
<li><a href="/offers">Tarifs</a></li>
</ul>
</div>
</div>
<div class="header_btm">
<div class="container relative">
<div class="w100pc vertical_middle">
<div class="row">
<div class="col-lg-7 col-md-6 col-xs-12 relative">
<div class="vertical_middle">
<div class="block">
<h1 class="inline_block" style="font-size:42px">Outil d'optimisation de vos textes,</h1>
<h3>pour de meilleurs classements dans les moteurs de recherche.</h3>
<div class="spacer"></div>
</div>
</div>
</div>
<div class="col-lg-5 col-md-6 col-xs-12">
<div class="video_holder">
<video autoplay="" controls="true" height="auto" loop="" muted="" playsinline="" width="100%">
<source src="/anim/fr3_900x557.mp4" type="video/mp4"/>
<source src="/anim/fr3_900x557.webm" type="video/webm"/>
<source src="/anim/fr3_900x557.ogv" type="video/ogg"/>
</video>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!--.header-->
<div class="container over_header">
<div class="row">
<div class="col-md-1">
</div>
<div class="col-md-10">
<div class="white_box rounded bg_rocket">
<div class="bg_gear p50 relative">
<div class="row">
<div class="col-md-9">
<form accept-charset="UTF-8" action="/d?method=get" class="position-form" method="post" onsubmit="JavaScript:loadscreen()"><input name="utf8" type="hidden" value="✓"/><input name="authenticity_token" type="hidden" value="Nn1EE9UWcNqfI++OQL1CkqaEdgWUIWlR4/i1y7q+NVTZ3P4K+DfEvzu8brN0BCiJirLjDOz2mS38S/mr+GuxXA=="/>
<script src="/js/fp.js"></script><input id="fp" name="fp" type="hidden"/> <script>var fp2 = new Fingerprint2(); fp2.get(function(result) { console.log(result); document.getElementById("fp").value = result; }); </script>
<input id="query" name="query" type="hidden" value=""/>
<input id="engine" name="engine" type="hidden" value="google"/>
<strong class="font30 font800 color_00afd8 valign_middle">Faire le test</strong>
<br/><span class="valign_middle">Entrez l'<strong>adresse</strong> de votre page web :</span>
<div class="spacer h10"></div>
<table class="input_group">
<tr>
<td>
<input class="input_blue" id="url" name="url" onfocus="{ this.placeholder = ''; }" placeholder="site.fr/page-a-analyser" style="width:100%" type="text" value=""/> </td>
<td>
<input onclick="JavaScript:loadscreen()" type="submit" value="Go"/>
</td>
</tr>
</table>
<div class="spacer h10"></div>
</form> </div>
<div class="col-md-3">
</div>
</div>
</div>
</div>
</div>
<div class="col-md-1">
</div>
</div>
<div class="spacer h90"></div>
<h2 class="text-center font600">Pourquoi 1.fr ?</h2>
<div class="spacer h10"></div>
<div class="row row-flex">
<div class="col-md-4 col-sm-12 col-xs-12 pb40 text-center">
<div class="h100pc p50 border rounded_sm lh30">
<div class="relative" style="min-height:99px">
<img alt="" class="middle" src="/i/icon-anim_01.gif"/>
</div>
<div class="h40"></div>
          Environ <b>70%</b> des pages optimisées obtiennent de meilleurs classements sous 5 semaines.
      </div>
</div>
<div class="col-md-4 col-sm-12 col-xs-12 pb40 text-center">
<div class="h100pc p50 border rounded_sm lh30">
<img alt="" src="/i/icon-anim_02.gif"/>
<div class="h40"></div>
          Utilisé par plus de <b>30 000</b> rédacteurs, professionnels du marketing et du référencement.
      </div>
</div>
<div class="col-md-4 col-sm-12 col-xs-12 pb40 text-center">
<div class="h100pc p50 border rounded_sm lh30">
<img alt="" src="/i/icon-anim_03.gif"/>
<div class="h40"></div>
<b>0</b> connaissance technique requise.
      </div>
</div>
</div>
<div class="spacer h60 hidden-sm"></div>
</div><!-- .over_header -->
<div class="bg_f5f7f7">
<div style="max-width:1920px;margin:0 auto">
<div class="spacer h140"></div>
<div class="relative">
<div class="container">
<div class="row">
<div class="col-md-6 pt20 pb20 pr60">
<h3 class="font600">Adaptez vos contenus<br/> aux attentes des moteurs de recherche</h3>
<div class="spacer h30"></div>
<p>
                Créez des contenus conçus pour de meilleurs classements dans les moteurs de recherche.
                L'optimisation de texte vise à ajouter ou supprimer des mots dans votre texte, afin de mieux correspondre aux attentes des moteurs de recherche.
              </p>
<div class="spacer h10"></div>
<a class="btn btn_lg" href="/o"><span>Optimisez mon site</span></a> <div class="spacer h100"></div>
</div>
</div>
</div>
<div class="half_side half_side_r">
<div class="w100pc overflow_hidden">
<div data-aos="fade-left" data-aos-easing="ease-in-sine" data-aos-offset="300">
<img alt="" class="fr rounded_l shadow_dwn" src="/i/external/img1_fr.jpg"/>
</div>
</div>
</div>
</div>
<div class="spacer h100"></div>
<div class="relative">
<div class="container">
<div class="row">
<div class="col-md-6"></div>
<div class="col-md-6 pt30 pb60 pl60">
<h3 class="font600">Trouvez le sujet de votre prochain article</h3>
<div class="spacer h30"></div>
<p>Explorez les questions que se posent les internautes et produisez du contenu optimisé.</p>
<div class="spacer h10"></div>
<a class="btn btn_lg" href="/q"><span>Recherchez des idées</span></a> <div class="spacer h10"></div>
</div>
</div>
</div>
<div class="half_side half_side_l">
<div class="w100pc overflow_hidden">
<div data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-offset="300">
<img alt="" class="fr rounded_l shadow_dwn" src="/i/external/img4.jpg"/>
</div>
</div>
</div>
</div>
<div class="spacer h100"></div>
<div class="relative">
<div class="container">
<div class="row">
<div class="col-md-6 pt20 pb20 pr60">
<h3 class="font600">Rédigez optimisé</h3>
<div class="spacer h30"></div>
<p>Produisez des contenus optimisés sur le fond et sur la forme grâce au meilleur outil d'assistance éditoriale.
                Contrôlez et améliorez vos productions de contenus, établissez de meilleures stratégies éditoriales.</p>
<div class="spacer h10"></div>
<a class="btn btn_lg" href="/h"><span>Comment ça marche ?</span></a> <div class="spacer h100"></div>
</div>
</div>
</div>
<div class="half_side half_side_r">
<div class="w100pc overflow_hidden">
<div data-aos="fade-left" data-aos-easing="ease-in-sine" data-aos-offset="300">
<img alt="" class="fr rounded_l shadow_dwn" src="/i/external/img2_fr.jpg"/>
</div>
</div>
</div>
</div>
<div class="spacer h100"></div>
<div class="relative">
<div class="container">
<div class="row">
<div class="col-md-6"></div>
<div class="col-md-6 pt30 pb60 pl60">
<h3 class="font600">Pour plus de trafic</h3>
<div class="spacer h30"></div>
<p>Plus de <b>50 000 rédacteurs, référenceurs et professionnels du marketing</b> utilisent notre technologie exclusive afin d'obtenir de meilleures positions dans les classements des moteurs de recherche.
              <a class="btn btn_lg" href="/h3"><span>Exemple d'optimisation</span></a> </p></div>
</div>
</div>
<div class="half_side half_side_l">
<div class="w100pc overflow_hidden">
<div data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-offset="300">
<img alt="" class="fl rounded_r shadow_dwn" src="/i/external/img5.jpg"/>
</div>
</div>
</div>
</div>
<div class="spacer h60"></div>
</div>
</div>
<div class="spacer h100"></div>
<div class="bg_chart" style="background-size:120%">
<div class="container text-center">
<div class="spacer h140"></div>
<h2>Libérez le potentiel de votre site</h2>
<div class="spacer h60"></div>
</div>
<div class="container">
<div class="row">
<div class="col-md-1">
</div>
<div class="col-md-10">
<div class="white_box rounded bg_rocket">
<div class="bg_gear p50 relative">
<div class="row">
<div class="col-md-9">
<form accept-charset="UTF-8" action="/d?method=get" class="position-form" method="post" onsubmit="JavaScript:loadscreen()"><input name="utf8" type="hidden" value="✓"/><input name="authenticity_token" type="hidden" value="jimZL7Z/6TuEwBMFzYIzH/tM799ydJPY2wmmyXFCDthhiCM2m15dXiBfkjj5O1kE13p61gqjY6TEuuqpM5eK0A=="/>
<script src="/js/fp.js"></script><input id="fp" name="fp" type="hidden"/> <script>var fp2 = new Fingerprint2(); fp2.get(function(result) { console.log(result); document.getElementById("fp").value = result; }); </script>
<input id="query" name="query" type="hidden" value=""/>
<input id="engine" name="engine" type="hidden" value="google"/>
<strong class="font30 font800 color_00afd8 valign_middle">Faire le test</strong>
<br/><span class="valign_middle">Entrez l'<strong>adresse</strong> de votre page web :</span>
<div class="spacer h10"></div>
<table class="input_group">
<tr>
<td>
<input class="input_blue" id="url" name="url" onfocus="{ this.placeholder = ''; }" placeholder="site.fr/page-a-analyser" style="width:100%" type="text" value=""/> </td>
<td>
<input onclick="JavaScript:loadscreen()" type="submit" value="Go"/>
</td>
</tr>
</table>
<div class="spacer h10"></div>
</form> </div>
<div class="col-md-3">
</div>
</div>
</div>
</div>
</div>
<div class="col-md-1">
</div>
</div>
</div>
</div>
<div class="footer">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-6 pb20">
<a class="footer_logo " href="/"></a>
<div class="spacer h20"></div>
          En utilisant ce site, vous acceptez les <a href="/terms">conditions d'utilisation</a> et la <a href="/privacy">politique de confidentialité</a>.<br/>
          © Copyright 2021, 1.fr<br/>All Rights Reserved.
      </div>
<div class="col-md-2 col-sm-6 pb20">
<h6 class="color_00aed7 uppercase">Entreprise</h6>
<ul>
<li><a href="/about">A propos</a></li>
<li><a href="/privacy">Pol. de confidentialité</a></li>
<li><a href="/cookies">Pol. de Cookies</a></li>
<li><a href="/terms">Mentions légales</a></li>
</ul>
</div>
<div class="col-md-2 col-sm-6 pb20">
<h6 class="color_00aed7 uppercase">Services</h6>
<ul>
<li><a href="/signup">Inscription</a></li>
<li><a href="/o">Optimiser un texte</a></li>
<li><a href="/h">Fonctionnement</a></li>
<li><a href="/api">API</a></li>
</ul>
</div>
<div class="col-md-2 col-sm-6 pb20">
<h6 class="color_00aed7 uppercase">Contact</h6>
<ul>
<ul><li><a href="mailto:contact@textoptimizer.com?subject=1.fr">Contactez-nous</a></li></ul>
<li><a href="https://twitter.com/1fr_broadcast" target="_new"></a>Twitter</li>
</ul>
</div>
<div class="col-md-3 col-sm-6 pb20">
<h6 class="color_00aed7 uppercase nowrap">Les Avis de nos Utilisateurs</h6>
<img alt="" class="fl float_none-sm mr5" src="/i/star_orange.png" style="width:20px"/>
<img alt="" class="fl float_none-sm mr5" src="/i/star_orange.png" style="width:20px"/>
<img alt="" class="fl float_none-sm mr5" src="/i/star_orange.png" style="width:20px"/>
<img alt="" class="fl float_none-sm mr5" src="/i/star_orange.png" style="width:20px"/>
<img alt="" class="fl float_none-sm mr5" src="/i/star_orange2_white.png" style="width:20px"/>
<span class="color_fff font600 pl10"><a href="https://fr.trustpilot.com/review/1.fr" target="_new">Trustpilot</a></span>
</div>
</div>
</div>
<script type="text/javascript">
    var cnt=0, texts=[];

    // save the texts in an array for re-use
    $(".textContent").each(function() {
        texts[cnt++]=$(this).text();
    });
    function slide() {
        if (cnt>=texts.length) cnt=0;
        $('#textMessage').html(texts[cnt++]);
        $('#textMessage')
            .fadeIn('slow').animate({opacity: 1.0}, 1000).fadeOut('slow',
            function() {
                return slide()
            }
        );
    }
    slide()
</script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.nice-select.min.js"></script>
<script src="/js/aos.js"></script>
<script>
    AOS.init({
        easing: 'ease-in-out-back'
    });
</script>
<script src="/js/jquery_ujs.js"></script>
<script src="/js/clipboard.min.js"></script>
<script src="/js/add-to-cart-fr.js"></script>
<script src="/js/please-wait.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.checkmark').on('click', function(){
            if ($('.field_with_errors #user_terms').length) {
                $('.field_with_errors #user_terms').unwrap();
            }
        })
        $('.btn_signup_by_email_popup').on('click', function(){
            $('.signup_by_email_popup').show();
        })
        $('.btn_explore_popup').on('click', function(){
            $('#explore_popup_content').html('<div class="text-center pt60 pb60"><img src="/i/loading.gif"></div>');
            $('.explore_popup').show();
        })

        $('.btn_common_sentences_popup').on('click', function(){
            $('.common_sentences_popup').show();
        })
        $('.btn_clipboard_list').on('click', function(){
            $('.clipboard_list').show();
        })
        $('.textoptimizer_popup').on('click', function(e){
            if ($(e.target).hasClass('textoptimizer_popup')) {
                $(this).hide();
            }
        })
        $('.close_popup_btn').on('click', function(){
            $('.signup_by_email_popup').hide();
            $('.explore_popup').hide();
            $('.common_sentences_popup').hide();
            $('.textoptimizer_popup').hide();
            $('.clipboard_list').hide();
            $('.kwrd_pop').hide();
        })
    })
</script>
<script>
    var acc = document.getElementsByClassName("accordion_top");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
</script>
<script>
    function myFunction(x) {
        x.classList.toggle("close");
    }
</script>
<script>
    $(document).ready(function() {
        $('.nice-select').niceSelect();
        FastClick.attach(document.body);
    });
</script>
<script type="text/javascript">
    function hideshow(which){
        if (      !document.getElementById)
            return;
        if (which.style.display=="block")
            which.style.display="none";
        else
            which.style.display="block";
    }
</script>
<script type="text/javascript">
    function loadscreen_download(){
        window.loading_screen = window.pleaseWait({
            logo: "/i/logo_2x.png",
            backgroundColor: '#fff',
            loadingHtml: "<p class='loading-message'>Votre téléchargement va démarrer automatiquement.</p>"
                + "<a href='' onclick='dummy(0);return false;'><span>Cliquez-ici une fois votre téléchargement terminé.</span></a>"
        });
    }
</script>
<script type="text/javascript">
    function loadscreen(){
        window.loading_screen = window.pleaseWait({
            logo: "/i/logo_2x.png",
            backgroundColor: '#fff',
            loadingHtml: "<p class='loading-message'><img src='/i/loading.gif'></p><div class='sk-spinner sk-spinner-wave'><div class='sk-rect1'></div><div class='sk-rect2'></div><div class='sk-rect3'></div><div class='sk-rect4'></div><div class='sk-rect5'></div></div>"
        });
    }
</script>
<script type="text/javascript">
    function loadscreen_progressbar(){
        window.loading_screen = window.pleaseWait({
            logo: "",
            backgroundColor: '#fff',
            loadingHtml: "<div class='loading_screen'> <div class='loading_screen_cnt'> <table class='mauto'> <tr> <td class='pr50 block-sm center-sm'> <img src='/i/logo.png' alt='' style='max-width:147px' /> </td> <td class='block-sm center-sm'> Analyse des  résultats du moteur de recherche <div class='relative'> <div class='info_popup sm' style='top:-36px;right:-25px;z-index:2'> <div class='question_icon'></div> <div class='popup'> <div class='pop_arw'></div> <div class='pop_contain white_box rounded border p10'> <img src='/anim/anim_explain_optimize.gif' alt='' /> </div> </div> </div> </div> <img src='/anim/progress_bar.gif' /> </td> </tr> </table> </div> </div>"
        });
    }
</script>
</div></body>
</html>
