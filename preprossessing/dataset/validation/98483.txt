<!DOCTYPE HTML>
<html xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
<head><meta content="text/html; charset=utf-8" http-equiv="content-type"/><title>Using Fasting for Natural Healing | AllAboutFasting</title><meta content="fasting" name="keywords"/><meta content="Fasting information and guidelines that show you how to use this natural therapy to heal and enhance your life." name="description"/><meta content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1" id="viewport" name="viewport"/>
<link href="/sd/support-files/A.style.css.pagespeed.cf.W9ASiphyjS.css" rel="stylesheet" type="text/css"/>
<!-- start: tool_blocks.sbi_html_head -->
<link href="https://www.allaboutfasting.com/" rel="canonical"/>
<link href="https://www.allaboutfasting.com/fasting.xml" rel="alternate" title="RSS" type="application/rss+xml"/>
<meta content="Using Fasting for Natural Healing | AllAboutFasting" property="og:title"/>
<meta content="Fasting information and guidelines that show you how to use this natural therapy to heal and enhance your life." property="og:description"/>
<meta content="website" property="og:type"/>
<meta content="https://www.allaboutfasting.com/" property="og:url"/>
<script language="JavaScript" type="text/javascript">var https_page=0</script>
<script async="" defer="" src="https://www.allaboutfasting.com/sd/support-files/gdprcookie.js.pagespeed.jm.yFshyEJ2qE.js" type="text/javascript"></script><!-- end: tool_blocks.sbi_html_head -->
<!-- start: shared_blocks.118597860#end-of-head -->
<!-- end: shared_blocks.118597860#end-of-head -->
<meta content="008FD7B1902FD43F1DA2EFE08B4EEAB5" name="msvalidate.01"/>
<script type="text/javascript">var FIX=FIX||{};</script>
</head>
<body class="responsive">
<div class="modern" id="PageWrapper">
<div id="HeaderWrapper">
<div id="Header">
<div class="Liner">
<div class="WebsiteName">
<a href="/"> </a>
</div><div class="Tagline"> </div>
<!-- start: shared_blocks.118597856#top-of-header -->
<!-- end: shared_blocks.118597856#top-of-header -->
<!-- start: shared_blocks.118597847#bottom-of-header -->
<div class="desktopOnly"><div class="ResponsiveNavWrapper">
<div class="ResponsiveNavButton"><span>Menu</span></div><div class="HorizontalNavBarLeft HorizontalNavBar HorizontalNavBarCSS ResponsiveNav">
<ul class="root"><li class="li1"><a href="/">Home</a></li><li class="li1"><a href="/about.html">About</a></li><li class="li1"><a href="/fasting-blog.html">What's New</a></li>
</ul>
</div></div>
</div>
<!-- end: shared_blocks.118597847#bottom-of-header -->
</div><!-- end Liner -->
</div><!-- end Header -->
</div><!-- end HeaderWrapper -->
<div id="ColumnsWrapper">
<div id="ContentWrapper">
<div id="ContentColumn">
<div class="Liner">
<!-- start: shared_blocks.118597844#above-h1 -->
<div class="mobileOnly">
<div class="mobileOnly"><!-- start: tool_blocks.navbar.horizontal.left --><div class="ResponsiveNavWrapper">
<div class="ResponsiveNavButton"><span>Menu</span></div><div class="HorizontalNavBarLeft HorizontalNavBar HorizontalNavBarCSS ResponsiveNav">
<ul class="root"><li class="li1"><a href="/">Home</a></li><li class="li1">
<span class="navheader">About Fasting</span></li><li class="li1"><a href="/benefits-of-fasting.html">Benefits of Fasting</a></li><li class="li1"><a href="/types-of-fasting.html">Types of Fasting</a></li><li class="li1"><a href="/fasting-to-lose-weight.html">Fasting to Lose Weight</a></li><li class="li1"><a href="/who-can-fast.html">Who Can Fast?</a></li><li class="li1"><a href="/what-is-fasting.html">What is Fasting?</a></li><li class="li1"><a href="/fasting-experiences.html">Fasting Experiences</a></li><li class="li1"><a href="/fasting-resources.html">Fasting Resources</a></li><li class="li1">
<span class="navheader">Popular Fasts</span></li><li class="li1"><a href="/fruit-fasting.html">Fruit Fasting</a></li><li class="li1"><a href="/water-fasting.html">Water Fasting</a></li><li class="li1"><a href="/intermittent-fasting.html">Intermittent Fasting</a></li><li class="li1"><a href="/rice-fasting.html">Rice Fasting</a></li><li class="li1"><a href="/juice-fasting.html">Juice Fasting</a></li><li class="li1"><a href="/master-cleanse.html">Master Cleanse</a></li><li class="li1"><a href="/cleansing-diet.html">Cleansing Diets</a></li><li class="li1">
<span class="navheader">After the Fast</span></li><li class="li1"><a href="/healthy-food-choices.html">New Food Choices</a></li><li class="li1"><a href="/easy-healthy-recipes.html">Easy Healthy Recipes</a></li></ul>
</div></div><!-- end: tool_blocks.navbar.horizontal.left -->
</div>
</div>
<!-- end: shared_blocks.118597844#above-h1 -->
<h1 id="top">Fasting for Health, Balance, and Vitality<br/></h1>
<!-- start: shared_blocks.118597864#below-h1 -->
<!-- end: shared_blocks.118597864#below-h1 -->
<p>Fasting is a completely natural healing therapy that has been used for 
thousands of years to aid, treat, and sometimes even cure many common maladies. It
 creates the same healing effects today.     
</p>
<!-- start: shared_blocks.118597859#below-paragraph-1 -->
<!-- end: shared_blocks.118597859#below-paragraph-1 -->
<p>
And while modern medicine so often attempts to alleviate outer symptoms 
of a health condition, fasting affects healing from the inside out, 
getting to the actual source of the condition, helping to burn inferior 
cells and build new healthier cells and tissues.
</p><p>
Our bodies are capable of instigating their own perfect healing if we 
allow them the opportunity. Fasting is such an opportunity.
</p>
<h2 style="text-align: left"><span style="font-weight: normal;">Potential benefits of fasting:</span><br/></h2>
<ul><li>heal a host of minor (and sometimes major) health disorders<br/><br/></li><li>help you lose weight and keep it off<br/><br/></li><li>cleanse your body of metabolic wastes and toxins<br/><br/></li><li>improve your skin tone and health, making you look younger<br/><br/></li><li>stimulate new cell growth, making you feel younger<br/><br/></li><li>strengthen your immune system and natural defenses<br/><br/></li><li>improve glandular health and hormonal balance<br/><br/></li><li>increase mental clarity<br/><br/></li><li>enhance your moods, enjoy a more positive outlook<br/><br/></li><li>give you more energy and enthusiasm<br/><br/></li><li>enhance your spiritual connection</li></ul>
<h2 style="text-align: center">Information you can use to begin fasting.</h2>
<p>At AllAboutFasting, you will learn about the different methods of 
fasting along with each one's advantages and disadvantages, and the 
particulars on how to do them.
</p><p>Here, you can discover the numerous benefits fasting creates in our lives, affecting us on all levels of our being, enhancing our lives in so many ways.
</p><p> 
You'll find tips on how to make a fasting experience more pleasant, how 
to get the greatest benefit from a fast, techniques to aid in detoxing 
the wastes from your body, and how to take the greatest 
advantage of the opportunity fasting creates to clear out emotional 
baggage.
</p><p>
Never fasted before? There is information on <a href="https://www.allaboutfasting.com/fruit-fast.html" onclick="return FIX.track(this);">easy one-day fasting</a> that will "get your feet wet", so you can begin to experience the healing effects of this natural therapy.
</p><p>
Not ready for an all-out fast? Or you don't meet the <a href="https://www.allaboutfasting.com/who-can-fast.html">criteria for fasting</a>? Discover how cleansing diets can be used to gain the same benefits as fasting.
</p>
<div class="CalloutBox" style="box-sizing: border-box"><p style="text-align: center;"><span style="font-weight: normal;font-size: 23px;"><i>Guidance for improving your<br/> health and vitality naturally.</i></span></p>
</div>
<h2 style="text-align: center">Begin Healing . . . Add Balance to Your Life.</h2>
<p>Introducing fasting to your life is easier than you think. Even just occasional short fasts will reward you with many insights
and increased awareness. But no matter the length, <span style="font-weight: normal;font-style: normal;">fasting will take you on a journey full of life-changing experiences.</span>
</p>
<div class="" style="margin-left: auto; margin-right: auto; float: none; width: 90%; box-sizing: border-box"><p><a href="https://www.allaboutfasting.com/types-of-fasting.html" onclick="return FIX.track(this);">Types of Fasting</a> Offers an overview of the basic methods of fasting,
 their main differences, and help in choosing which one is right for you.<br/>
<br/>
<a href="https://www.allaboutfasting.com/fruit-fasting.html" onclick="return FIX.track(this);">Fruit Fasting</a> gives the details on how to cleanse and revitalize using
  fresh whole fruits. Many find this method of fasting easier to accomplish.<br/>
<br/>
<a href="https://www.allaboutfasting.com/cleansing-diets.html" onclick="return FIX.track(this);">Cleansing Diets</a> Guidance for using a cleansing diet, a form of partial
   fasting, as an alternative to more intense fasting methods, or as preparation for them.<br/>
<br/>
<a href="https://www.allaboutfasting.com/how-long-should-you-fast.html" onclick="return FIX.track(this);">How Long Should You Fast?</a> Discover
  what length of time is best for you and how much preparation is required.<br/>
<br/>
<a href="https://www.allaboutfasting.com/benefits-of-fasting.html" onclick="return FIX.track(this);">Benefits of Fasting</a> Learn how broad the benefits are, affecting us
   not just physically, but emotionally and spiritually as well.<br/>
<br/>
<a href="https://www.allaboutfasting.com/fasting-for-weight-loss.html" onclick="return FIX.track(this);">Fasting for Weight Loss</a> shows you the <b><i>real</i></b>
 benefit to using
   fasting to lose weight; the insights into, and positive changes in, 
the unhealthy patterns that have led to your current weight.<br/>
<br/>
<a href="https://www.allaboutfasting.com/colon-cleansing.html" onclick="return FIX.track(this);">Colon Cleansing</a> details several options for use both during a fast
   and at other times, including a psyllium-based <a href="https://www.allaboutfasting.com/colon-cleanse-recipe.html" onclick="return FIX.track(this);">colon cleanse recipe</a>
    that is all natural and gentle on the system.<br/></p>
</div>
<div class="CalloutBox" style="margin-left: auto; margin-right: auto; float: none; box-sizing: border-box"><p style="text-align: center;"><span style="font-weight: normal;font-size: 23px;"><i>Fasting will take you on a journey<br/> of life-changing experiences.</i></span></p>
</div>
<!-- start: shared_blocks.118597858#above-socialize-it -->
<!-- end: shared_blocks.118597858#above-socialize-it -->
<!-- start: shared_blocks.118597846#socialize-it -->
<!-- start: tool_blocks.socializeit -->
<div class="js-socializeit" id="socializeit_215163300"></div>
<script type="text/javascript">var https_page;var socializeit_options=socializeit_options||[];socializeit_options.push({el_id:"socializeit_215163300",pack:1,domain:"allaboutfasting.com",https_page:https_page,share_horizontal_label:"Share this page:",share_sticky_label:"Share",payItText:"",payItExpanded:"",szColor:"#FFFFFF",whatIsThisLabel:"What&rsquo;s this?",whatIsThisUrl:"",background_color:"",version:1,display_variant:""});</script><!-- end: tool_blocks.socializeit -->
<!-- end: shared_blocks.118597846#socialize-it -->
<!-- start: shared_blocks.118597855#below-socialize-it -->
<!-- end: shared_blocks.118597855#below-socialize-it -->
<p><a href="https://www.allaboutfasting.com/index.html#top" onclick="return FIX.track(this);">Return to top </a></p>
</div><!-- end Liner -->
</div><!-- end ContentColumn -->
</div><!-- end ContentWrapper -->
<div id="NavWrapper">
<div id="NavColumn">
<div class="Liner">
<!-- start: shared_blocks.118597865#top-of-nav-column -->
<div class="desktopOnly"><!-- start: tool_blocks.navbar --><div class="Navigation"><ul><li><a href="/">Home</a></li></ul><h3>About Fasting</h3><ul><li><a href="/benefits-of-fasting.html">Benefits of Fasting</a></li><li><a href="/types-of-fasting.html">Types of Fasting</a></li><li><a href="/fasting-to-lose-weight.html">Fasting to Lose Weight</a></li><li><a href="/who-can-fast.html">Who Can Fast?</a></li><li><a href="/what-is-fasting.html">What is Fasting?</a></li><li><a href="/fasting-experiences.html">Fasting Experiences</a></li><li><a href="/fasting-resources.html">Fasting Resources</a></li></ul><h3>Popular Fasts</h3><ul><li><a href="/fruit-fasting.html">Fruit Fasting</a></li><li><a href="/water-fasting.html">Water Fasting</a></li><li><a href="/intermittent-fasting.html">Intermittent Fasting</a></li><li><a href="/rice-fasting.html">Rice Fasting</a></li><li><a href="/juice-fasting.html">Juice Fasting</a></li><li><a href="/master-cleanse.html">Master Cleanse</a></li><li><a href="/cleansing-diet.html">Cleansing Diets</a></li></ul><h3>After the Fast</h3><ul><li><a href="/healthy-food-choices.html">New Food Choices</a></li><li><a href="/easy-healthy-recipes.html">Easy Healthy Recipes</a></li></ul></div><!-- end: tool_blocks.navbar -->
</div>
<!-- end: shared_blocks.118597865#top-of-nav-column -->
<!-- start: shared_blocks.118597861#navigation -->
<!-- end: shared_blocks.118597861#navigation -->
<div class="ImageBlock ImageBlockCenter"><a href="https://www.allaboutfasting.com/fasting-quotes.html" title="Go to Fasting Quotes - historical and modern day | AllAboutFasting"><img alt="Alan Cott quote on fasting" data-pin-media="https://www.allaboutfasting.com/images/AlanCott3.png" src="https://www.allaboutfasting.com/images/xAlanCott3.png.pagespeed.ic.T5FGxDebKB.png" title="Alan Cott quote on fasting" width="220"/></a></div>
<!-- start: shared_blocks.118597866#bottom-of-nav-column -->
<div class="desktopOnly"><div align="center">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 160x600, created 3/27/10 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-5870823571824131" data-ad-slot="7360772148" style="display:inline-block;width:160px;height:600px"></ins>
<script>(adsbygoogle=window.adsbygoogle||[]).push({});</script>
</div>
</div>
<div class="mobileOnly"><script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Mobile in lieu of 160x600 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-5870823571824131" data-ad-format="auto" data-ad-slot="7436838136" data-full-width-responsive="true" style="display:block"></ins>
<script>(adsbygoogle=window.adsbygoogle||[]).push({});</script>
</div>
<!-- end: shared_blocks.118597866#bottom-of-nav-column -->
</div><!-- end Liner -->
</div><!-- end NavColumn -->
</div><!-- end NavWrapper -->
<div id="ExtraWrapper">
<div id="ExtraColumn">
<div class="Liner">
<!-- start: shared_blocks.118597849#top-extra-default -->
<!-- DDG search widget,@author Juri Wornowitski,@version 2.0,@link https://www.plainlight.com/ddg --><div id="widdget" style="width:100%;margin:10px 0;"><style type="text/css">#widdget input{display:block;height:30px;padding:4px;outline:none;border:1px solid #8d8d8d;border-right:0;box-sizing:border-box;border-radius:3px 0 0 3px;width:calc(100% - 41px);font-size:15px}#widdget button{float:left;cursor:pointer;width:43px;height:30px;color:#fff;background-image:linear-gradient(#cacaca,#929292);border:0;border-radius:0 3px 3px 0}#widdget button:active{background-image:linear-gradient(#909090,#585858)}#widdget button:focus{outline:none}</style><form onsubmit="this.elements[0].click();return false;" style="position:relative"><div style="float:right;position:absolute;top:0;right:-2px;z-index:3"><button onclick="var v=this.parentElement.parentElement.elements[1].value;window.open('https://duckduckgo.com?q=%22'+encodeURIComponent(v)+'%22+site%3Awww.allaboutfasting.com&amp;kx=%236666ff&amp;k9=%23437240&amp;k8=%23333333');" type="button">🔍</button></div><input name="que" placeholder="Search" type="text"/></form></div>
<!-- end: shared_blocks.118597849#top-extra-default -->
<div class="" style="border-color: #cccccc; border-width: 1px 1px 1px 1px; border-style: solid; margin-top: 25px; margin-bottom: 20px; padding-left: 8px; padding-right: 8px; padding-top: 8px; padding-bottom: 8px; box-sizing: border-box"><h4>Further info<br/></h4>
<p><a href="https://www.allaboutfasting.com/what-is-fasting.html">What is Fasting? </a>Dispels any doubts about fasting being akin to starvation. </p><p><a href="https://www.allaboutfasting.com/who-can-fast.html">Who Can Fast?</a> Not everyone is a good candidate for fasting. Certain conditions prohibit fasting.</p><p>Be aware of the <a href="https://www.allaboutfasting.com/healthy-fasting.html">Precautions to Fasting </a>before planning a fast.</p><p><a href="https://www.allaboutfasting.com/benefit-of-fasting.html">Health Conditions Improved by Fasting</a>  See the compiled list of ailments that are known to respond favorably to fasting.       <br/></p><p><a href="https://www.allaboutfasting.com/benefit-of-fasting-degenerative-diseases.html">Chronic Degenerative Diseases</a> respond better to holistic therapies such as fasting than to traditional medical treatments.<br/></p>
</div>
<!-- start: shared_blocks.118597863#extra-default-nav -->
<center>
<!-- AddThis Button BEGIN -->
<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4ba783032f1ec70a"><img alt="Bookmark and Share" height="24" src="https://s7.addthis.com/static/btn/v2/lg-share-en.gif" style="border:0" vspace="20px" width="175"/></a><script src="https://s7.addthis.com/js/250/addthis_widget.js#username=xa-4ba783032f1ec70a" type="text/javascript"></script>
<!-- AddThis Button END -->
</center>
<!-- end: shared_blocks.118597863#extra-default-nav -->
<!-- start: shared_blocks.118597852#bottom-extra-default -->
<div class="desktopOnly"><!-- start: tool_blocks.whatsnew.eyJ3aWR0aCI6IjEwMCUiLCJibG9nbGV0X2RhdGUiOiIwIiwiYmxvZ2xldF9jb3VudCI6IjMiLCJibG9nbGV0X2Rlc2NyaXB0aW9uIjoiMSIsImhlYWRsaW5lX2xpbmsiOiIxIiwiYmxvZ2xldF9tb3JlIjoiMSIsImhlYWRsaW5lX3RleHQiOiJSZWNlbnQgUG9zdHMiLCJibG9nbGV0X21vcmVfdGV4dCI6IlJlYWQgTW9yZSIsInJzc19pY29uIjoiMSJ9 -->
<div class="WhatsNew" style="width: 100%">
<h2><a href="https://www.allaboutfasting.com/fasting-blog.html">Recent Posts</a></h2><div class="WhatsNew-subscribe">
<img alt="RSS" class="WhatsNew-icon" height="16" src="https://www.allaboutfasting.com/objects/xrss.png.pagespeed.ic.nVmUyyfqP7.png" width="16"/>
<ul>
<li>
<a href="https://www.allaboutfasting.com/fasting.xml">
<img alt="XML RSS" height="17" src="https://www.allaboutfasting.com/objects/xrss.jpg.pagespeed.ic.tg98uIqgYi.jpg" width="91"/>
</a>
</li> <li>
<a href="https://feedly.com/#subscription%2Ffeed%2Fhttps://www.allaboutfasting.com/fasting.xml" rel="nofollow" target="new">
<img alt="follow us in feedly" height="17" src="https://www.allaboutfasting.com/objects/xfeedly.gif.pagespeed.ic.t3Vu8JhVKA.png" width="91"/>
</a>
</li> <li>
<a href="http://add.my.yahoo.com/rss?url=https://www.allaboutfasting.com/fasting.xml" rel="nofollow" target="new">
<img alt="Add to My Yahoo!" height="17" src="https://www.allaboutfasting.com/objects/xaddtomyyahoo4.gif.pagespeed.ic.PgZzd5uL-1.png" width="91"/>
</a>
</li> </ul>
</div>
<ol> <li>
<h3><a href="https://www.allaboutfasting.com/fasting-blog.html" target="_new">Valter Longo and the Longevity Diet</a></h3><p class="WhatsNew-content">Valter Longo's Longevity Diet, which is a fasting mimicking diet, is showing great promise as a prescribed therapy. If you haven't heard of Valter Longo, check out his newly launched website to follow…</p> <p class="WhatsNew-more"><a href="/fasting-blog.html#Valter-Longo-and-the-Longevity-Diet" target="_new">Read More</a></p> </li> <li>
<h3><a href="https://www.allaboutfasting.com/information-on-fasting.html">Fasting Overview for Beginners</a></h3><p class="WhatsNew-content">Information on fasting especially geared toward the beginner. Important guidelines on fasting including the contraindications and how to do a simple one-day fast.</p> <p class="WhatsNew-more"><a href="https://www.allaboutfasting.com/information-on-fasting.html">Read More</a></p> </li> <li>
<h3><a href="https://www.allaboutfasting.com/meat-vs-vegan.html">Meat vs. Vegan | AllAboutFasting</a></h3><p class="WhatsNew-content">Confusion seems to arise as to whether this site promotes meat-eating or veganism. Let's set the record straight and talk about ideal diets. Ideal for whom? Each of us individually.</p> <p class="WhatsNew-more"><a href="https://www.allaboutfasting.com/meat-vs-vegan.html">Read More</a></p> </li></ol>
</div><!-- end: tool_blocks.whatsnew.eyJ3aWR0aCI6IjEwMCUiLCJibG9nbGV0X2RhdGUiOiIwIiwiYmxvZ2xldF9jb3VudCI6IjMiLCJibG9nbGV0X2Rlc2NyaXB0aW9uIjoiMSIsImhlYWRsaW5lX2xpbmsiOiIxIiwiYmxvZ2xldF9tb3JlIjoiMSIsImhlYWRsaW5lX3RleHQiOiJSZWNlbnQgUG9zdHMiLCJibG9nbGV0X21vcmVfdGV4dCI6IlJlYWQgTW9yZSIsInJzc19pY29uIjoiMSJ9 -->
</div>
<!-- end: shared_blocks.118597852#bottom-extra-default -->
</div><!-- end Liner -->
</div><!-- end NavColumn -->
</div><!-- end NavWrapper -->
</div><!-- end ColumnsWrapper -->
<div id="FooterWrapper">
<div id="Footer">
<div class="Liner">
<!-- start: shared_blocks.118597848#above-bottom-nav -->
<!-- end: shared_blocks.118597848#above-bottom-nav -->
<!-- start: shared_blocks.118597851#bottom-navigation -->
<!-- end: shared_blocks.118597851#bottom-navigation -->
<!-- start: shared_blocks.118597845#below-bottom-nav -->
<!-- end: shared_blocks.118597845#below-bottom-nav -->
<!-- start: shared_blocks.118597850#footer -->
<div class="" style="padding-top: 5px; padding-bottom: 5px; background-color: #d1e3d0; box-sizing: border-box"><p style="text-align: center;"><span style="font-size: 13px;"><span style="font-size: 16px;font-style: normal;"><a href="https://www.allaboutfasting.com/">Home</a>  |  <a href="https://www.allaboutfasting.com/contact.html">Contact</a>  |  <a href="https://www.allaboutfasting.com/about.html">About</a>  |  <a href="https://www.allaboutfasting.com/fasting-blog.html">What's New</a>  |  <a href="https://www.allaboutfasting.com/privacy-policy.html">Privacy</a>  |  <a href="https://www.allaboutfasting.com/disclosure.html">Disclosure</a></span></span></p><p style="text-align: center;"><span style="font-size: 13px;"><em>The information on this site is not intended to replace the advice of a health professional </em></span><span style="font-size: 13px;"><em>who is familiar with your individual health-related issues.<br/></em></span><span style="font-size: 13px;"><em>CopyrightÂ© 2008-2019 AllAboutFasting.com. All rights reserved.</em></span></p>
</div>
<!-- end: shared_blocks.118597850#footer -->
</div><!-- end Liner -->
</div><!-- end Footer -->
</div><!-- end FooterWrapper -->
</div><!-- end PageWrapper -->
<script src="/sd/support-files/fix.js.pagespeed.jm.3phKUrh9Pj.js" type="text/javascript"></script>
<script type="text/javascript">FIX.doEndOfBody();</script>
<script src="/sd/support-files/design.js.pagespeed.jm.wq3hSiafo4.js" type="text/javascript"></script>
<!-- start: tool_blocks.sbi_html_body_end -->
<script>var SS_PARAMS={pinterest_enabled:false,googleplus1_on_page:false,socializeit_onpage:true};</script><style>.g-recaptcha{display:inline-block}.recaptcha_wrapper{text-align:center}</style>
<script>if(typeof recaptcha_callbackings!=="undefined"){SS_PARAMS.recaptcha_callbackings=recaptcha_callbackings||[]};</script>
<script>(function(d,id){if(d.getElementById(id)){return;}var s=d.createElement('script');s.async=true;s.defer=true;s.src="/ssjs/ldr.js";s.id=id;d.getElementsByTagName('head')[0].appendChild(s);})(document,'_ss_ldr_script');</script>
<!-- end: tool_blocks.sbi_html_body_end -->
<!-- Generated at 00:55:34 16-Apr-2019 -->
</body>
</html>
