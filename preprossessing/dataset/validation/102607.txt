<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html dir="ltr" lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="/sites/all/themes/atltheme/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<meta content="Tea Party lyrics: 'Relearn Love [7aurelius, Scott Stapp]', 'Chimera', 'Temptation' etc" name="description"/>
<meta content="283249275118803" property="fb:app_id"/>
<title>Tea Party lyrics: 84 song lyrics</title>
<meta content="width=device-width" name="viewport"/>
<link href="http://www.allthelyrics.com/lyrics/tea_party" hreflang="en" rel="alternate"/>
<link href="http://www.allthelyrics.com/fr/lyrics/tea_party" hreflang="fr" rel="alternate"/>
<link href="http://www.allthelyrics.com/de/lyrics/tea_party" hreflang="de" rel="alternate"/>
<link href="http://www.allthelyrics.com/it/lyrics/tea_party" hreflang="it" rel="alternate"/>
<link href="http://www.allthelyrics.com/es/lyrics/tea_party" hreflang="es" rel="alternate"/>
<link href="/sites/default/files/css/css_7958ffeb95bb688d2456f734a58bba69.css" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/default/files/css/css_70145eb6949315cdd0246d4fc26c2a3d.css" media="print" rel="stylesheet" type="text/css"/>
<style media="all" type="text/css"></style>
<!--[if lte IE 8]>
  <link type="text/css" rel="stylesheet" href="/sites/all/themes/atltheme/styles/ie8.css" media="all" />
  <![endif]-->
<!--[if lte IE 7]>
  <link type="text/css" rel="stylesheet" href="/sites/all/themes/atltheme/styles/ie7.css" media="all" />
  <![endif]-->
<script src="/sites/default/files/js/js_573f0c79eb488d569ead5ae1164286a7.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, { "basePath": "/", "my_module": { "atl_artistname": [ "Tea Party", "Tea Party" ] } });
//--><!]]>
</script>
<link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700&amp;light&amp;v1&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css"/>
<!--BEGIN FIRSTIMPRESSION TAG -->
<script data-cfasync="false" type="text/javascript">
	window.apd_options = { 'websiteId': 6189, 'runFromFrame': false };
	(function() {
		var w = window.apd_options.runFromFrame ? window.top : window;
		if(window.apd_options.runFromFrame && w!=window.parent) w=window.parent;
		if (w.location.hash.indexOf('apdAdmin') != -1){if(typeof(Storage) !== 'undefined') {w.localStorage.apdAdmin = 1;}}
		var adminMode = ((typeof(Storage) == 'undefined') || (w.localStorage.apdAdmin == 1));
		w.apd_options=window.apd_options;
		var apd = w.document.createElement('script'); apd.type = 'text/javascript'; apd.async = true;
		apd.src = '//' + (adminMode ? 'cdn' : 'ecdn') + '.firstimpression.io/' + (adminMode ? 'fi.js?id=' + window.apd_options.websiteId : 'fi_client.js') ;
		var s = w.document.getElementsByTagName('head')[0]; s.appendChild(apd);
	})();
</script>
<!-- END FIRSTIMPRESSION TAG -->
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-9394998891234553",
          enable_page_level_ads: true
     });
</script>
</head>
<body class="not-front not-logged-in page-node node-type-contentartist i18n-en one-sidebar">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        
  ga('create', 'UA-57480429-1', 'auto');
  ga('send', 'pageview');
</script>
<div id="header-wrapper">
<!-- header-wrapper-top -->
<!-- end header-wrapper-top -->
<!-- header -->
<div class="clear-block header-pos" id="header">
<div class="header-pos" id="headerleft"></div>
<div id="header-line">
<div id="header-logo"><a href="/" rel="home" title="Home"><img alt="Home" src="/sites/all/themes/atltheme/images/logo.png"/></a></div>
<div id="header-search">
<div class="searchformhead searchformhead-active" id="searchform1">
<div class="searchformhead-text"> </div>
<script>
  (function() {
    var cx = 'partner-pub-9394998891234553:4879285622';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:searchbox-only queryparametername="query" resultsurl="https://www.allthelyrics.com/google-search"></gcse:searchbox-only>
</div>
</div>
</div>
</div>
</div>
<div id="main-columns-wrapper">
<div id="main-menu">
<div id="buttonjoinus"><a href="/forum/register.php" title="Join us">Join us</a></div>
<div id="buttonsubmitlyrics"><a href="/atl/add/lyrics/1138004" title="Submit Lyrics">Add new song</a></div> <ul id="mainmenuul">
<li class="mainmenuli mainmenuli-open mainmenuli-active" id="mainmenuli"><a href="/">Lyrics</a> <ul class="mainmenuulul">
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/">Forum</a></li>
<li class="mainmenulili nomobile"><a href="https://www.allthelyrics.com/forum/activity.php">What's New?</a></li>
<li class="mainmenulili"><a href="/index">A-Z Artists</a></li>
</ul>
</li>
<li class="mainmenuli nomobile" id="mainmenuli"><a href="/">Identify it</a> <ul class="mainmenuulul">
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=11">All</a></li>
<li class="mainmenulili nomobile"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=124">Non-English Songs</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=137">Music from TV</a></li>
<li class="mainmenulili nomobile"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=53">Store Musics Identify</a></li>
<li class="mainmenulili nomobile"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=136">Scat Lyrics Identify</a></li>
</ul>
</li>
<li class="mainmenuli nomobile" id="mainmenuli"><a href="/">Lyrics translations</a> <ul class="mainmenuulul">
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=46">All</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=44">Arabic</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=48">French</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=50">German</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=32">Greek</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=113">Hebrew</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=47">Italian</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=65">Persian</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=33">Spanish</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=129">Turkish</a></li>
</ul>
</li>
<li class="mainmenuli nomobile" id="mainmenuli"><a href="/">Lyrics request</a> <ul class="mainmenuulul">
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=10">Lyrics request</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/showthread.php?t=13574">Please read before posting</a></li>
</ul>
</li>
<li class="mainmenuli nomobile" id="mainmenuli"><a href="/">More</a> <ul class="mainmenuulul">
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=13">Lyrics Review</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=67">Poetry</a></li>
<li class="mainmenulili nomobile"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=31">Tabs</a></li>
<li class="mainmenulili nomobile"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=30">Songs for Special Moments</a></li>
<li class="mainmenulili "><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=15">Music Events</a></li>
<li class="mainmenulili nomobile"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=17">Music instruments</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=20">Look for</a></li>
</ul>
</li>
</ul>
</div>
<div class="clear"></div>
<div id="header-banner">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
(adsbygoogle = window.adsbygoogle || []).push({
google_ad_client: "ca-pub-9394998891234553",
enable_page_level_ads: true
});
</script>
<ins class="adsbygoogle ads-center-top" data-ad-client="ca-pub-9394998891234553" data-ad-slot="7664738822" style="display:inline-block;"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
<div class="clear"></div>
<div id="main-columns">
<div id="breadcrumb"><div class="breadcrumb"><a href="/">Home</a> » <a href="/index">Artists</a> » <a href="http://www.allthelyrics.com/lyrics/tea_party">Tea Party</a></div></div>
<div id="main-columns-inner">
<div class="clear-block " id="sidebar-first">
<div class="region region-sidebar-first sticky" id="sticky-sidebar" style="top: 10px;">
<div class="block block-atl" id="block-atl-atl-banner-left">
<div class="inner">
<div class="content">
<style type="text/css">
@media screen and (min-width: 701px) { .ads-left-top { width:300px; height:250px; } }
@media screen and (max-width: 700px) { .ads-left-top { width:120px; height:600px; } }
</style>
<div class="node-inside-banner">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle ads-left-top" data-ad-client="ca-pub-9394998891234553" data-ad-slot="6638249221" style="display:inline-block;"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div> </div>
</div>
</div>
<div class="block block-atl" id="block-atl-atl-banner-left-bottom">
<div class="inner">
<div class="content">
<div class="node-inside-banner">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle ads-left-bottom" data-ad-client="ca-pub-9394998891234553" data-ad-slot="4711272423" style="display:inline-block;"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div> </div>
</div>
</div>
<img class="pixel" src="/sites/all/themes/atltheme/images/pixel.png"/>
</div>
</div> <!-- /#sidebar-first -->
<div id="main-wrapper">
<div id="main">
<div class="clear-block" id="page">
<h1 class="page-title">Tea Party lyrics</h1>
<div class="region region-content">
<div class="node nodetype-artist " id="node-1138004">
<script>
/* BIT - Allthelyics.com - Flex */
cf_page_artist = "Tea Party";
cf_page_song = "";
cf_adunit_id = "39381075";
cf_flex = true;
</script>
<script src="//srv.clickfuse.com/showads/showad.js"></script>
<div class="node-center">
<div class="content">
<div class="nodetype-artist-content-top">
<div class="nodetype-artist-content-top">
<div class="content-top-lyricslist">Tea Party lyrics: 'Relearn Love [7aurelius, Scott Stapp]', 'Chimera', 'Temptation', 'Save Me', 'Gyroscope' </div>
<div class="content-top-lyricspopular"><div class="artist-lyrics-list artist-lyrics-list-popular"><h2 class="artist-lyrics-list-h2">Top 5 songs</h2><ol><li class="lyrics-list-item lyrics-list-item-1589029"><a href="/lyrics/tea_party/relearn_love_7aurelius_scott_stapp-lyrics-1122189.html">Relearn Love [7aurelius, Scott Stapp]</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181559"><a href="/lyrics/tea_party/chimera-lyrics-18954.html">Chimera</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181538"><a href="/lyrics/tea_party/temptation-lyrics-18933.html">Temptation</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181512"><a href="/lyrics/tea_party/save_me-lyrics-18903.html">Save Me</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181541"><a href="/lyrics/tea_party/gyroscope-lyrics-18936.html">Gyroscope</a> <span></span></li></ol></div></div>
</div>
</div>
<div class="clear"></div>
<div class="nodetype-artist-content-lyrics-all"><div class="artist-lyrics-list artist-lyrics-list-all"><h2 class="artist-lyrics-list-h2">Tea Party song lyrics</h2><div class="lyricslist-output" id="lyricslist-left"><div class="artist-lyrics-list-letter">A</div><ul><li class="lyrics-list-item lyrics-list-item-1181522"><a href="/lyrics/tea_party/a_certain_slant_of_light-lyrics-18914.html">A Certain Slant Of Light</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181556"><a href="/lyrics/tea_party/a_slight_attack-lyrics-18951.html">A Slight Attack</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181548"><a href="/lyrics/tea_party/aftermath-lyrics-18943.html">Aftermath</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181542"><a href="/lyrics/tea_party/alarum-lyrics-18937.html">Alarum</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181518"><a href="/lyrics/tea_party/all_my_charms-lyrics-18909.html">All My Charms</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1437087"><a href="/lyrics/tea_party/angels-lyrics-521247.html">Angels</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1437085"><a href="/lyrics/tea_party/apathy-lyrics-521245.html">Apathy</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181539"><a href="/lyrics/tea_party/army_ants-lyrics-18934.html">Army Ants</a> <span></span></li></ul><ul><li class="lyrics-list-item lyrics-list-item-1181519"><a href="/lyrics/tea_party/baby_what_you_trying_to_do-lyrics-18910.html">Baby What You Trying To Do</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181545"><a href="/lyrics/tea_party/babylon-lyrics-18940.html">Babylon</a> <span></span></li></ul><div class="artist-lyrics-list-letter">C</div><ul><li class="lyrics-list-item lyrics-list-item-1181564"><a href="/lyrics/tea_party/can_you_see_my_tears-lyrics-18959.html">Can You See My Tears</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1437081"><a href="/lyrics/tea_party/cathartik-lyrics-521240.html">Cathartik</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181559"><a href="/lyrics/tea_party/chimera-lyrics-18954.html">Chimera</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1420310"><a href="/lyrics/tea_party/coming_back_again-lyrics-486105.html">Coming Back Again</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181535"><a href="/lyrics/tea_party/coming_home-lyrics-18930.html">Coming Home</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181528"><a href="/lyrics/tea_party/correspondences-lyrics-18923.html">Correspondences</a> <span></span></li></ul><div class="artist-lyrics-list-letter">D</div><ul><li class="lyrics-list-item lyrics-list-item-1181533"><a href="/lyrics/tea_party/drawing_down_the_moon-lyrics-18928.html">Drawing Down The Moon</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181516"><a href="/lyrics/tea_party/dreams_of_reason-lyrics-18907.html">Dreams Of Reason</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1437080"><a href="/lyrics/tea_party/dust_to_gold-lyrics-521239.html">Dust To Gold</a> <span></span></li></ul><ul><li class="lyrics-list-item lyrics-list-item-1181547"><a href="/lyrics/tea_party/emerald-lyrics-18942.html">Emerald</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1420309"><a href="/lyrics/tea_party/empty_glass-lyrics-486104.html">Empty Glass</a> <span></span></li></ul><ul><li class="lyrics-list-item lyrics-list-item-1181515"><a href="/lyrics/tea_party/fallen_angel-lyrics-18906.html">Fallen Angel</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181526"><a href="/lyrics/tea_party/fire_in_the_head-lyrics-18921.html">Fire In The Head</a> <span></span></li></ul><div class="artist-lyrics-list-letter">G</div><ul><li class="lyrics-list-item lyrics-list-item-1181560"><a href="/lyrics/tea_party/gone-lyrics-18955.html">Gone</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181517"><a href="/lyrics/tea_party/goodman_rag-lyrics-18908.html">Goodman Rag</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181551"><a href="/lyrics/tea_party/great_big_lie-lyrics-18946.html">Great Big Lie</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181541"><a href="/lyrics/tea_party/gyroscope-lyrics-18936.html">Gyroscope</a> <span></span></li></ul><ul><li class="lyrics-list-item lyrics-list-item-1181552"><a href="/lyrics/tea_party/heaven_coming_down-lyrics-18947.html">Heaven Coming Down</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181562"><a href="/lyrics/tea_party/hurt-lyrics-18957.html">Hurt</a> <span></span></li></ul><div class="artist-lyrics-list-letter">I</div><ul><li class="lyrics-list-item lyrics-list-item-1181523"><a href="/lyrics/tea_party/in_this_time-lyrics-18917.html">In This Time</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181534"><a href="/lyrics/tea_party/inanna-lyrics-18929.html">Inanna</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1437088"><a href="/lyrics/tea_party/interzone-lyrics-521248.html">Interzone</a> <span></span></li></ul><div class="artist-lyrics-list-letter">L</div><ul><li class="lyrics-list-item lyrics-list-item-1181509"><a href="/lyrics/tea_party/let_me_show_you_the_door-lyrics-18900.html">Let Me Show You The Door</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1506828"><a href="/lyrics/tea_party/life_line-lyrics-752060.html">Life Line</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181511"><a href="/lyrics/tea_party/little_miss_heaven-lyrics-18902.html">Little Miss Heaven</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181561"><a href="/lyrics/tea_party/lullaby-lyrics-18956.html">Lullaby</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1420312"><a href="/lyrics/tea_party/luxuria-lyrics-486107.html">Luxuria</a> <span></span></li></ul><div class="artist-lyrics-list-letter">M</div><ul><li class="lyrics-list-item lyrics-list-item-1437078"><a href="/lyrics/tea_party/mantra-lyrics-521237.html">Mantra</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181510"><a href="/lyrics/tea_party/midsummer_day-lyrics-18901.html">Midsummer Day</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1437083"><a href="/lyrics/tea_party/must_must-lyrics-521242.html">Must Must</a> <span></span></li></ul><div class="artist-lyrics-list-letter">O</div><ul><li class="lyrics-list-item lyrics-list-item-1370402"><a href="/lyrics/tea_party/oceans-lyrics-318859.html">Oceans</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1420313"><a href="/lyrics/tea_party/one_step_closer_away-lyrics-486108.html">One Step Closer Away</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1420311"><a href="/lyrics/tea_party/overload-lyrics-486106.html">Overload</a> <span></span></li></ul></div><div id="lyricslist-right"><div class="artist-lyrics-list-letter">P</div><ul><li class="lyrics-list-item lyrics-list-item-1506829"><a href="/lyrics/tea_party/paint_it_black-lyrics-752061.html">Paint It Black</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181520"><a href="/lyrics/tea_party/poem_on_the_inside_cover-lyrics-18911.html">Poem On The Inside Cover</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181540"><a href="/lyrics/tea_party/psychopomp-lyrics-18935.html">Psychopomp</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181546"><a href="/lyrics/tea_party/pulse-lyrics-18941.html">Pulse</a> <span></span></li></ul><div class="artist-lyrics-list-letter">R</div><ul><li class="lyrics-list-item lyrics-list-item-1181524"><a href="/lyrics/tea_party/raven_skies-lyrics-18919.html">Raven Skies</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1589029"><a href="/lyrics/tea_party/relearn_love_7aurelius_scott_stapp-lyrics-1122189.html">Relearn Love [7aurelius, Scott Stapp]</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181543"><a href="/lyrics/tea_party/release-lyrics-18938.html">Release</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1437079"><a href="/lyrics/tea_party/requiem-lyrics-521238.html">Requiem</a> <span></span></li></ul><div class="artist-lyrics-list-letter">S</div><ul><li class="lyrics-list-item lyrics-list-item-1181555"><a href="/lyrics/tea_party/samsara-lyrics-18950.html">Samsara</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181512"><a href="/lyrics/tea_party/save_me-lyrics-18903.html">Save Me</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1420307"><a href="/lyrics/tea_party/seven_circles-lyrics-486102.html">Seven Circles</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181532"><a href="/lyrics/tea_party/shadows_on_the_mountainside-lyrics-18927.html">Shadows On The Mountainside</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181529"><a href="/lyrics/tea_party/silence-lyrics-18924.html">Silence</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181530"><a href="/lyrics/tea_party/sister_awake-lyrics-18925.html">Sister Awake</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1437084"><a href="/lyrics/tea_party/soulbreaking-lyrics-521244.html">Soulbreaking</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181537"><a href="/lyrics/tea_party/spoken_word-lyrics-18932.html">Spoken Word</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1370404"><a href="/lyrics/tea_party/stargazer-lyrics-318861.html">Stargazer</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181513"><a href="/lyrics/tea_party/sun_going_down-lyrics-18904.html">Sun Going Down</a> <span></span></li></ul><div class="artist-lyrics-list-letter">T</div><ul><li class="lyrics-list-item lyrics-list-item-1181557"><a href="/lyrics/tea_party/taking_me_away-lyrics-18952.html">Taking Me Away</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181538"><a href="/lyrics/tea_party/temptation-lyrics-18933.html">Temptation</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181527"><a href="/lyrics/tea_party/the_bazaar-lyrics-18922.html">The Bazaar</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181553"><a href="/lyrics/tea_party/the_halcyon_days-lyrics-18948.html">The Halcyon Days</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181525"><a href="/lyrics/tea_party/the_majestic_song-lyrics-18920.html">The Majestic Song</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1437086"><a href="/lyrics/tea_party/the_master_and_margarita-lyrics-521246.html">The Master And Margarita</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181554"><a href="/lyrics/tea_party/the_messenger-lyrics-18949.html">The Messenger</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181521"><a href="/lyrics/tea_party/the_river-lyrics-18912.html">The River</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181563"><a href="/lyrics/tea_party/the_ubiquitous_mr_lovegrove-lyrics-18958.html">The Ubiquitous Mr. Lovegrove</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1370403"><a href="/lyrics/tea_party/the_watcher-lyrics-318860.html">The Watcher</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181558"><a href="/lyrics/tea_party/these_living_arms-lyrics-18953.html">These Living Arms</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1506830"><a href="/lyrics/tea_party/time-lyrics-752062.html">Time</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181549"><a href="/lyrics/tea_party/touch-lyrics-18944.html">Touch</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181544"><a href="/lyrics/tea_party/transmission-lyrics-18939.html">Transmission</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181531"><a href="/lyrics/tea_party/turn_the_lamp_down_low-lyrics-18926.html">Turn The Lamp Down Low</a> <span></span></li></ul><ul><li class="lyrics-list-item lyrics-list-item-1181550"><a href="/lyrics/tea_party/underground-lyrics-18945.html">Underground</a> <span></span></li></ul><div class="artist-lyrics-list-letter">W</div><ul><li class="lyrics-list-item lyrics-list-item-1181536"><a href="/lyrics/tea_party/walk_with_me-lyrics-18931.html">Walk With Me</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1392986"><a href="/lyrics/tea_party/walking_wounded-lyrics-424938.html">Walking Wounded</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1181514"><a href="/lyrics/tea_party/watching_what_the_rain_blows_in-lyrics-18905.html">Watching What The Rain Blows In</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1437082"><a href="/lyrics/tea_party/white_water_siren-lyrics-521241.html">White Water Siren</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1420308"><a href="/lyrics/tea_party/wishing_you_would_stay-lyrics-486103.html">Wishing You Would Stay</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1420314"><a href="/lyrics/tea_party/writings_on_the_wall-lyrics-486109.html">Writing's On The Wall</a> <span></span></li></ul><ul><li class="lyrics-list-item lyrics-list-item-1712216"><a href="/lyrics/tea_party/your_baby_is_flatter_than_a_short_stack_at_ihop-lyrics-1267190.html">Your Baby Is Flatter Than A Short Stack At Ihop</a> <span></span></li></ul></div></div></div> <div class="clear"></div>
<div class="nodetype-artist-content-lyrics-submit"><a class="atladdlyrics" href="/atl/add/lyrics/1138004">Submit new song</a></div>
<div class="clear"></div>
</div>
</div>
</div>
</div>
</div> <!-- /#page -->
</div> <!-- /#main -->
</div> <!-- /#main-wrapper -->
</div>
</div> <!-- /#main-columns -->
<div id="footer">
<div id="footer-menu">
<p>
<span class="footer-logo-text"><a href="/">AllTheLyrics.com</a></span>
<a href="/index">A-Z Artists</a>   |  
        <a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=46">Lyrics translations</a>   |  
        <a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=11">Identify</a>   |  
        <a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=10">Lyrics request</a>   
        </p>
</div>
<div id="footer-languages">
<div class="footer-languages">
<p>Translate interface: English
 | <a href="http://www.allthelyrics.com/fr/lyrics/tea_party">Français</a>
 | <a href="http://www.allthelyrics.com/de/lyrics/tea_party">Deutsch</a>
 | <a href="http://www.allthelyrics.com/it/lyrics/tea_party">Italiano</a>
 | <a href="http://www.allthelyrics.com/es/lyrics/tea_party">Español</a>
</p>
<p><a href="/privacy.htm">Privacy Policy</a>  |  <a href="/dmca-policy.htm">DMCA Policy</a>  |  <a href="/contact">Contact us</a></p>
<p>All lyrics are property and copyright of their owners.<br/>All lyrics provided for educational purposes only.</p>
</div>
</div>
<div class="clear"></div>
</div>
</div> <!-- /#main-columns-wrapper -->
</body>
</html>
