<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Catamaran Charter Grenada | First Impressions Starwind</title>
<meta content="Sharing our beautiful island and marine activities with guests from around the world. Being Grenada's largest tour operator with over 21 tours, we guarantee that we have just the tour for you." name="description"/>
<meta content="http://catamaranchartering.com/index.html" property="og:url"/>
<meta content="website" property="og:type"/>
<meta content="Catamaran Charter Grenada | First Impressions Starwind" property="og:title"/>
<meta content="Sharing our beautiful island and marine activities with guests from around the world. Being Grenada's largest tour operator with over 21 tours, we guarantee that we have just the tour for you." property="og:description"/>
<meta content="graphics/banner.jpg" property="og:image"/>
<link href="https://fonts.googleapis.com/css?family=Domine" rel="stylesheet"/>
<!-- Bootstrap -->
<link href="css/bootstrap-3.4.0.css" rel="stylesheet" type="text/css"/>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<link href="Slideshow/css/simple-slideshow-styles.css" rel="stylesheet"/>
<link href="Slideshow/css/style.css" rel="stylesheet"/>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default">
<div class="container-fluid">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<button aria-expanded="false" class="navbar-toggle collapsed" data-target="#myNavbar" data-toggle="collapse" type="button">
<span class="sr-only">Toggle</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span></button>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="myNavbar">
<ul class="nav navbar-nav">
<li class="active"><a href="index.html">Home<span class="sr-only">(current)</span></a></li>
<li><a href="whale-watching.html">Whale &amp; Dolphin Watching</a></li>
<li class="dropdown"><a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">Sailing Tours<span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="snorkeling-tour-east.html">East Coast Tour</a></li>
<li><a href="snorkeling-tour-west.html">West Coast Tour</a></li>
</ul>
</li>
<li class="dropdown"><a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">Sunset Cruises<span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="sunset-cruise.html">Sunset Cruise</a></li>
<li><a href="wedding-cruise.html">Wedding Sunset Cruise</a></li>
</ul>
</li>
<li class="dropdown"><a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">Fishing Tours<span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="fishing-charter.html">Drift Party Fishing</a></li>
<li><a href="deep-sea-fishing.html">Coastal Deep Sea Fishing</a></li>
</ul>
</li>
<li class="dropdown"><a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">Excursions<span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="island-tour.html">Sandy Island Tour</a></li>
<li><a href="sailing-tour.html">Sea Safari Tour</a></li>
</ul>
</li>
</ul>
<ul class="nav navbar-nav navbar-right">
<li><a href="photo-gallery.html">Photos</a></li>
<li><a href="boat-sale.html">Boats For Sale</a></li>
<li><button class="btn btn-md navbar-btn btn-primary"><a href="book.html">Book Now</a></button></li>
</ul>
</div>
<!-- /.navbar-collapse -->
</div>
<!-- /.container-fluid -->
</nav>
<header>
<img alt="Catamaran Chartering Grenada First Impressions" class="img-responsive center-block banner" src="graphics/banner.jpg"/>
</header>
<div class="container-fluid" id="textgallery">
<br/>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<br/>
<div class="image-general" id="slideshow">
<div autofocus="autofocus" class="bss-slides num1" tabindex="1">
<figure>
<img alt="Catamaran Cruise Grenada" src="images/index1.jpg" width="100%"/>
<figcaption>Fun in the Paradise</figcaption>
</figure>
<figure>
<img alt="Starwind Catamaran Cruises" src="images/index2.jpg" width="100%"/>
<figcaption>Starwind Catamaran Cruises</figcaption>
</figure>
<figure>
<img alt="Whale Watching in Grenada" src="images/index3.jpg" width="100%"/>
<figcaption>Whale Watching in Grenada</figcaption>
</figure>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<h1>Welcome</h1>
<br/>
<p>For the past 35 years, we've been sharing our beautiful island and marine activities with guests from around the world. Being Grenada's largest tour operator with over 21 tours, we guarantee that we have just the tour for you.</p>
<p>Whether it's a <a href="whale-watching.html">whale watching</a> tour, a full or half day <a href="sailing-tour.html">sailing excursion</a>, a thrilling <a href="fishing-charter.html">fishing charter</a>, or a <a href="sunset-cruise.html">sunset cruise</a> aboard our catamaran, we know you take your recreation, and your fun, seriously. We won't let you down.</p>
<br/>
<h2>Our Crew</h2>
<br/>
<p>Our management and staff are 100% Grenadian!</p>
<p>That means that when you book any of our tours, you can be sure that you're getting the real deal.</p>
<h6>Browse through our site to learn more about our exciting tours and how we can make your vacation an unforgettable experience.</h6>
</div>
</div>
</div>
<br/>
<div class="container-fluid" id="address">
<div class="row">
<address class="text-center">
<strong><abbr title="Phone"><span aria-hidden="true" class="glyphicon glyphicon-phone-alt"></span></abbr> Phone: +1 (473) 440 3678 <br/><abbr title="Phone"><span aria-hidden="true" class="glyphicon glyphicon-earphone"></span></abbr> Cell: +1 (473) 407 1147 or +1 (473) 406 6993 <br/> <abbr title="Phone"><span aria-hidden="true" class="glyphicon glyphicon-user"></span></abbr> WhatsApp: +1 (473) 406 6993 <br/><abbr title="Email Address"><span aria-hidden="true" class="glyphicon glyphicon-send"></span></abbr> Email: starwindsailing@spiceisle.com</strong>
</address>
</div>
</div>
<footer class="text-center">
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">
<p style="display:inline-block">Copyright Â© 2019 <a href="index.html">First Impressions Ltd.</a> All Rights Reserved. Web Design by <a href="http://www.grenadaexplorer.com">Grenada Explorer</a>.</p>
</div>
</div>
</div>
</footer>
<a class="back-to-top" href="#">Back to Top</a>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.12.4.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap-3.4.0.js"></script>
<script src="Slideshow/js/hammer.min.js"></script><!-- for swipe support on touch interfaces -->
<script src="Slideshow/js/better-simple-slideshow.min.js"></script>
<script>
var opts = {
    auto : {
        speed : 5000, 
        pauseOnHover : true
    },
    fullScreen : false, 
    swipe : true
};
makeBSS('.num1', opts);

var opts2 = {
    auto : false,
    fullScreen : true,
    swipe : true
};
makeBSS('.num2', opts2);
</script>
<script>var amountScrolled = 500;
$(window).scroll(function() {
	if ( $(window).scrollTop() > amountScrolled ) {
		$('a.back-to-top').fadeIn('slow');
	} else {
		$('a.back-to-top').fadeOut('slow');
	}
});</script>
<script>$('a.back-to-top').click(function() {
	$('html, body').animate({
		scrollTop: 0
	}, 700);
	return false;
});</script>
</body>
</html>