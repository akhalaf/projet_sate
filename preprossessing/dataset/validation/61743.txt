<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>ABTorrents :: Login</title>
<link href="templates/2/css/tooltipster.bundle.min.css?1217" rel="stylesheet"/>
<link href="templates/2/css/tooltipster-sideTip-borderless.min.css?1217" rel="stylesheet"/>
<link href="templates/2/default.css?1217" rel="stylesheet"/>
<link href="templates/2/css/classcolors.css?1217" rel="stylesheet"/>
<link href="templates/2/css/icons.css?1217" rel="stylesheet"/>
<link href="templates/2/css/navbar.css?1217" rel="stylesheet"/>
<link crossorigin="anonymous" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" rel="stylesheet"/>
<link href="favicon.ico" rel="shortcut icon"/>
<script>(function(w,d,u){w.readyQ=[];w.bindReadyQ=[];function p(x,y){if(x==='ready'){w.bindReadyQ.push(y);}else{w.readyQ.push(x);}};let a={ready:p,bind:p};w.$=w.jQuery=function(f){if(f===d||f===u){return a}else{p(f)}}})(window,document)</script>
<script>
        function resizeIframe(obj) {
            obj.style.height = '0px';
            obj.style.height = obj.contentWindow.document.body.scrollHeight + 25 + 'px';
        }
    </script>
</head>
<body>
<div id="base_around">
</div>
<table align="center" border="0" cellpadding="10" cellspacing="0" class="mainouter">
<tr>
<td align="center" class="outer" id="control" style="padding-bottom: 10px"><h4>Note: You need cookies enabled to sign up or log in.</h4>
<h4>Note: if you're experiencing login issues delete your old cookies.</h4>
<h4>
<b>[5]</b> failed logins in a row will result in banning your ip<br/>You have <b> <span style="color:rgb(0,128,0)">5</span> </b> login attempt remaining.</h4>
<div class="login-container center-block">
<form action="takelogin.php" class="well form-inline" method="post">
<table class="table table-bordered center-block">
<tr>
<td>Username:</td><td align="left"><input name="username" size="40" type="text"/></td>
</tr>
<tr>
<td>Password:</td>
<td align="left">
<input name="password" size="40" type="password"/>
</td>
</tr>
<tr>
<td class="rowhead" colspan="2">
<div class="level-center-center">
<input checked="" class="right5" id="remember" name="remember" type="checkbox" value="1"/>
<label class="level-item tooltipper" for="remember" title="Keep me logged in.">Remember Me?</label>
</div>
</td>
</tr>
<tr>
<td colspan="2"><em class="center-block">Now click the button marked <strong>X</strong></em></td>
</tr>
<tr>
<td colspan="2">
<div class="level-center">
<span><input class="btn btn-small btn-primary" name="submitme" type="submit" value="..."/></span>
<span><input class="btn btn-small btn-primary" name="submitme" type="submit" value="..."/></span>
<span><input class="btn btn-small btn-primary" name="submitme" type="submit" value="X"/></span>
<span><input class="btn btn-small btn-primary" name="submitme" type="submit" value="..."/></span>
<span><input class="btn btn-small btn-primary" name="submitme" type="submit" value="..."/></span>
<span><input class="btn btn-small btn-primary" name="submitme" type="submit" value="..."/></span>
</div><input name="returnto" type="hidden" value="/"/>
</td>
</tr>
<tr>
<td class="center-block" colspan="2">
<div class="level-center">
<em class="btn btn-mini"><strong><a href="signup.php">Join us</a>!</strong></em>    
                            <em class="btn btn-mini"><strong><a href="resetpw.php">Forgot Password</a>!</strong></em>    
                            <em class="btn btn-mini"><strong><a href="recover.php">Mail recover?</a></strong></em>
</div>
</td>
</tr>
</table>
</form>
</div></td></tr></table><br/>
<!-- Ends Footer -->
<a class="back-to-top" href="#">Back to Top</a>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</body>
</html>