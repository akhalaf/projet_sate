<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Rankings for Artists, Tracks and Labels based off the Beatport Top-100 charts" name="DESCRIPTION"/>
<meta content="beatport, charts, top, 100, trance, electro-house, electro-tech, rankings, labels, artists" name="KEYWORDS"/>
<link href="/_includes/bootstrap.css" rel="stylesheet"/>
<link href="/_includes/style_f.css" rel="stylesheet" type="text/css"/>
<script src="/_includes/jquery-1.11.3.min.js"></script>
<script src="/_includes/bootstrap.js"></script>
<script language="javascript">
            function audioplayerget(str){
                if (str==""){
                  document.getElementById("footer").innerHTML="";
                  return;
                  }
                xmlhttp=new XMLHttpRequest();
                xmlhttp.onreadystatechange=function(){
                    if (xmlhttp.readyState==4 && xmlhttp.status==200){
                    document.getElementById("footer").innerHTML=xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET","https://www.beatstats.com/_includes/audioplayer.php?BPID="+str,true);
                xmlhttp.send();
            }
        </script>
<script language="javascript">
            function audiolike(str){
                if (str==""){
                  document.getElementById("footer-audioplayer-get").innerHTML="";
                  return;
                  }
                xmlhttp=new XMLHttpRequest();
                xmlhttp.onreadystatechange=function(){
                    if (xmlhttp.readyState==4 && xmlhttp.status==200){
                    document.getElementById("footer-audioplayer-get").innerHTML=xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET","https://www.beatstats.com/_includes/playerget.php?BPID="+str,true);
                xmlhttp.send();
            }
        </script>
<script language="javascript">
            function audiobuy(str){
                if (str==""){
                  document.getElementById("footer-audioplayer-get").innerHTML="";
                  return;
                  }
                xmlhttp=new XMLHttpRequest();
                xmlhttp.onreadystatechange=function(){
                    if (xmlhttp.readyState==4 && xmlhttp.status==200){
                    document.getElementById("footer-audioplayer-get").innerHTML=xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET","https://www.beatstats.com/_includes/playerbuy.php?BPID="+str,true);
                xmlhttp.send();
            }
        </script>
<title>BeatStats - Rankings for Artists, Tracks and Labels based off the Beatport Top-100 charts</title>
</head>
<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-71570264-1', 'auto');
  ga('send', 'pageview');

</script>
<div id="container">
<div id="header">
<div id="header-logo">
<a href="https://www.beatstats.com"><img alt="" height="66" src="https://www.beatstats.com/_images/interface/logo_heading.png" width="944"/> </a></div>
<div id="header-searchbox">
<form action="https://www.beatstats.com/search/search/index" class="form-wrapper cf" method="post">
<input name="searchresult" pattern="[a-zA-Z0-9-.\s]+" placeholder="" required="" type="text"/>
<button type="submit"><strong>Search</strong></button>
</form>
</div>
<div id="header-top10">
<a href="https://www.beatstats.com/top10/list/home-12months-genre0"><img alt="" height="25" src="https://www.beatstats.com/_images/interface/topbar_top10_b.png" width="59"/></a>
</div>
<div id="header-artists">
<a href="https://www.beatstats.com/artists/list/artists-12months-genre0"><img alt="" height="25" src="https://www.beatstats.com/_images/interface/topbar_artists_a.png" width="66"/></a>
</div>
<div id="header-tracks">
<a href="https://www.beatstats.com/tracks/list/tracks-12months-genre0"><img alt="" height="25" src="https://www.beatstats.com/_images/interface/topbar_tracks_a.png" width="66"/></a>
</div>
<div id="header-labels">
<a href="https://www.beatstats.com/labels/list/labels-12months-genre0"><img alt="" height="25" src="https://www.beatstats.com/_images/interface/topbar_labels_a.png" width="66"/></a>
</div>
<div id="header-timeselect3">
<a href="https://www.beatstats.com/top10/home/list-3months-genre0"><img alt="" height="21" src="https://www.beatstats.com/_images/interface/time_selector_3_a.png" width="66"/></a>
</div>
<div id="header-timeselect12">
<a href="https://www.beatstats.com/top10/home/list-12months-genre0"><img alt="" height="21" src="https://www.beatstats.com/_images/interface/time_selector_12_b.png" width="66"/></a>
</div>
<div id="header-timeselectalltime">
<a href="https://www.beatstats.com/top10/home/list-alltime-genre0"><img alt="" height="21" src="https://www.beatstats.com/_images/interface/time_selector_alltime_a.png" width="55"/></a>
</div>       
        
        
       	  list<div id="header-genreselect">
<div class="btn-group">
<button aria-expanded="false" aria-haspopup="true" class="btn btn-secondary btn-default dropdown-toggle" data-toggle="dropdown" type="button">
<span class="genreheader"> TOP 100 (ALL GENRES)</span> <span class="caret"></span>
</button>
<div class="dropdown-menu">
<li><a href="https://www.beatstats.com/top10/home/list-12months-genre0">TOP 100 (all genres)</a></li>
<li class="divider" role="separator"></li>
<li><a href="https://www.beatstats.com/top10/home/list-12months-genre79"> <span class="genre1"> BIG ROOM</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre12"> <span class="genre1"> DEEP HOUSE</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre18"> <span class="genre1"> DUBSTEP</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre94"> <span class="genre1"> ELECTRO (CLASSIC / DETROIT / MODERN)</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre17"> <span class="genre1"> ELECTRO HOUSE</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre3"> <span class="genre1"> ELECTRONICA</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre65"> <span class="genre1"> FUTURE HOUSE</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre2"> <span class="genre1"> HARD TECHNO</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre5"> <span class="genre1"> HOUSE</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre90"> <span class="genre1"> MELODIC HOUSE &amp; TECHNO</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre14"> <span class="genre1"> MINIMAL / DEEP TECH</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre93"> <span class="genre1"> ORGANIC HOUSE / DOWNTEMPO</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre15"> <span class="genre1"> PROGRESSIVE HOUSE</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre11"> <span class="genre1"> TECH HOUSE</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre6"> <span class="genre1"> TECHNO (PEAK TIME / DRIVING / HARD)</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre92"> <span class="genre1"> TECHNO (RAW / DEEP / HYPNOTIC)</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre7"> <span class="genre1"> TRANCE</span></a></li><li class="divider" role="separator"></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre89"> <span class="genre2"> AFRO HOUSE</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre91"> <span class="genre2"> BASS HOUSE</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre9"> <span class="genre2"> BREAKS</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre39"> <span class="genre2"> DANCE / ELECTRO POP</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre16"> <span class="genre2"> DJ TOOLS</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre1"> <span class="genre2"> DRUM &amp; BASS</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre81"> <span class="genre2"> FUNKY / GROOVE / JACKIN HOUSE</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre86"> <span class="genre2"> GARAGE / BASSLINE / GRIME</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre8"> <span class="genre2"> HARD DANCE / HARDCORE</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre37"> <span class="genre2"> INDIE DANCE</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre85"> <span class="genre2"> LEFTFIELD BASS</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre80"> <span class="genre2"> LEFTFIELD HOUSE AND TECHNO</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre50"> <span class="genre2"> NU DISCO / DISCO </span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre13"> <span class="genre2"> PSY TRANCE</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre41"> <span class="genre2"> REGGAE / DANCEHALL / DUB</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre87"> <span class="genre2"> TRAP / FUTURE BASS</span></a></li><li><a href="https://www.beatstats.com/top10/home/list-12months-genre38"> <span class="genre2"> TRAP / HIP-HOP / R&amp;B</span></a></li>
</div>
</div>
</div>
<div id="header-rule">
</div>
</div><!-- #header -->
<div id="content">
<div id="content-top10artists">
<div id="top10artistchart-header"><span class="charttextname-a">TOP <strong>ARTISTS</strong></span> <span class="charttextname-b"></span></div>
<a href="https://www.beatstats.com/artist/artbat/499932-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">1</div>
<div id="top10artistchart-image">
<img height="50" src="https://www.beatstats.com/images/artist/small/499932.jpg"/></div>
<div id="top10artistchart-name"><span class="charttextname">ARTBAT</span>
<br/>
<span class="charttextpoints">45182 POINTS </span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/artist/earth-n-days/610928-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">2</div>
<div id="top10artistchart-image">
<img height="50" src="https://www.beatstats.com/images/artist/small/610928.jpg"/></div>
<div id="top10artistchart-name"><span class="charttextname">EARTH N DAYS</span>
<br/>
<span class="charttextpoints">40078 POINTS </span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/artist/space-92/769906-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">3<div id="top10artistchart-arrow"><img height="16" src="https://www.beatstats.com/_images/interface/uparrow_green.png" title="UP 4 in the last month" width="16"/></div><div id="top10artistchart-arrowtext">4</div></div>
<div id="top10artistchart-image">
<img height="50" src="https://www.beatstats.com/images/artist/small/769906.jpg"/></div>
<div id="top10artistchart-name"><span class="charttextname">SPACE 92</span>
<br/>
<span class="charttextpoints">37577 POINTS </span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/artist/john-summit/610028-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">4<div id="top10artistchart-arrow"><img height="16" src="https://www.beatstats.com/_images/interface/uparrow_green.png" title="UP 7 in the last month" width="16"/></div><div id="top10artistchart-arrowtext">7</div></div>
<div id="top10artistchart-image">
<img height="50" src="https://www.beatstats.com/images/artist/small/610028.jpg"/></div>
<div id="top10artistchart-name"><span class="charttextname">JOHN SUMMIT</span>
<br/>
<span class="charttextpoints">34922 POINTS </span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/artist/camelphat/150756-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">5</div>
<div id="top10artistchart-image">
<img height="50" src="https://www.beatstats.com/images/artist/small/150756.jpg"/></div>
<div id="top10artistchart-name"><span class="charttextname">CAMELPHAT</span>
<br/>
<span class="charttextpoints">32447 POINTS </span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/artist/block-and-crown/170336-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">6</div>
<div id="top10artistchart-image">
<img height="50" src="https://www.beatstats.com/images/artist/small/170336.jpg"/></div>
<div id="top10artistchart-name"><span class="charttextname">BLOCK &amp; CROWN</span>
<br/>
<span class="charttextpoints">29897 POINTS </span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/artist/moreno-pezzolato/73031-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">7<div id="top10artistchart-arrow"><img height="16" src="https://www.beatstats.com/_images/interface/uparrow_green.png" title="UP 2 in the last month" width="16"/></div><div id="top10artistchart-arrowtext">2</div></div>
<div id="top10artistchart-image">
<img height="50" src="https://www.beatstats.com/images/artist/small/73031.jpg"/></div>
<div id="top10artistchart-name"><span class="charttextname">MORENO PEZZOLATO</span>
<br/>
<span class="charttextpoints">29653 POINTS </span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/artist/umek/4177-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">8</div>
<div id="top10artistchart-image">
<img height="50" src="https://www.beatstats.com/images/artist/small/4177.jpg"/></div>
<div id="top10artistchart-name"><span class="charttextname">UMEK</span>
<br/>
<span class="charttextpoints">28437 POINTS </span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/artist/martin-ikin/153990-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">9</div>
<div id="top10artistchart-image">
<img height="50" src="https://www.beatstats.com/images/artist/small/153990.jpg"/></div>
<div id="top10artistchart-name"><span class="charttextname">MARTIN IKIN</span>
<br/>
<span class="charttextpoints">28094 POINTS </span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/artist/purple-disco-machine/130259-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">10</div>
<div id="top10artistchart-image">
<img height="50" src="https://www.beatstats.com/images/artist/small/130259.jpg"/></div>
<div id="top10artistchart-name"><span class="charttextname">PURPLE DISCO MACHINE</span>
<br/>
<span class="charttextpoints">24175 POINTS </span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a> <a href="https://www.beatstats.com/artists/list/list-YEAR-genre0">
<div id="top10artistchart-footer"><span class="charttextname-c">SHOW FULL CHART</span> </div> </a>
</div>
<div id="content-top10tracks">
<div id="top10artistchart-header"><span class="charttextname-a">TOP <strong>TRACKS</strong></span> </div>
<a href="https://www.beatstats.com/track/just-be-good-to-me-original-mix/13098550">
<div id="top10artistchart">
<div id="top10artistchart-number">1</div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/track/small/13098550.jpg"/></div><div id="top10trackchart-text">
<div id="top10trackchart-artistname">EARTH N DAYS</div><div id="top10trackchart-title"><strong>Just Be Good To Me</strong></div><div id="top10trackchart-points"><strong>17534</strong> POINTS /<strong> 265</strong> DAYS</div> <a href="https://pro.beatport.com/track/just-be-good-to-me-original-mix/13098550" target="_blank"><div id="shopping-icon"><img height="20" src="https://www.beatstats.com/_images/interface/shopping_icon_alpha.png" width="20"/></div></a>
<a href="https://pro.beatport.com/track/just-be-good-to-me-original-mix/13098550" target="_blank"><div id="play-icon"><img height="22" src="https://www.beatstats.com/_images/interface/play_icon_alpha.png" width="22"/></div></a><a> </a></div>
</div></a>
<a href="https://www.beatstats.com/track/deep-end-extended-mix/13775146">
<div id="top10artistchart">
<div id="top10artistchart-number">2<div id="top10artistchart-arrow"><img height="16" src="https://www.beatstats.com/_images/interface/uparrow_green.png" title="UP 3 in the last month" width="16"/></div><div id="top10artistchart-arrowtext">3</div></div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/track/small/13775146.jpg"/></div><div id="top10trackchart-text">
<div id="top10trackchart-artistname">JOHN SUMMIT</div><div id="top10trackchart-title"><strong>Deep End</strong></div><div id="top10trackchart-points"><strong>15235</strong> POINTS /<strong> 201</strong> DAYS</div> <a href="https://pro.beatport.com/track/deep-end-extended-mix/13775146" target="_blank"><div id="shopping-icon"><img height="20" src="https://www.beatstats.com/_images/interface/shopping_icon_alpha.png" width="20"/></div></a>
<a href="https://pro.beatport.com/track/deep-end-extended-mix/13775146" target="_blank"><div id="play-icon"><img height="22" src="https://www.beatstats.com/_images/interface/play_icon_alpha.png" width="22"/></div></a><a> </a></div>
</div></a>
<a href="https://www.beatstats.com/track/pump-up-the-jam-nightfunk-remix/13602183">
<div id="top10artistchart">
<div id="top10artistchart-number">3<div id="top10artistchart-arrow"><img height="16" src="https://www.beatstats.com/_images/interface/uparrow_green.png" title="UP 3 in the last month" width="16"/></div><div id="top10artistchart-arrowtext">3</div></div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/track/small/13602183.jpg"/></div><div id="top10trackchart-text">
<div id="top10trackchart-artistname">TECHNOTRONIC</div><div id="top10trackchart-title"><strong>Pump up the Jam</strong> ( Nightfunk Remix )</div><div id="top10trackchart-points"><strong>14414</strong> POINTS /<strong> 213</strong> DAYS</div> <a href="https://pro.beatport.com/track/pump-up-the-jam-nightfunk-remix/13602183" target="_blank"><div id="shopping-icon"><img height="20" src="https://www.beatstats.com/_images/interface/shopping_icon_alpha.png" width="20"/></div></a>
<a href="https://pro.beatport.com/track/pump-up-the-jam-nightfunk-remix/13602183" target="_blank"><div id="play-icon"><img height="22" src="https://www.beatstats.com/_images/interface/play_icon_alpha.png" width="22"/></div></a><a> </a></div>
</div></a>
<a href="https://www.beatstats.com/track/dustpig-original-mix/13798519">
<div id="top10artistchart">
<div id="top10artistchart-number">4<div id="top10artistchart-arrow"><img height="16" src="https://www.beatstats.com/_images/interface/uparrow_green.png" title="UP 6 in the last month" width="16"/></div><div id="top10artistchart-arrowtext">6</div></div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/track/small/13798519.jpg"/></div><div id="top10trackchart-text">
<div id="top10trackchart-artistname">FABRICATION</div><div id="top10trackchart-title"><strong>Dustpig</strong></div><div id="top10trackchart-points"><strong>14309</strong> POINTS /<strong> 171</strong> DAYS</div> <a href="https://pro.beatport.com/track/dustpig-original-mix/13798519" target="_blank"><div id="shopping-icon"><img height="20" src="https://www.beatstats.com/_images/interface/shopping_icon_alpha.png" width="20"/></div></a>
<a href="https://pro.beatport.com/track/dustpig-original-mix/13798519" target="_blank"><div id="play-icon"><img height="22" src="https://www.beatstats.com/_images/interface/play_icon_alpha.png" width="22"/></div></a><a> </a></div>
</div></a>
<a href="https://www.beatstats.com/track/planet-x-original-mix/13922322">
<div id="top10artistchart">
<div id="top10artistchart-number">5<div id="top10artistchart-arrow"><img height="16" src="https://www.beatstats.com/_images/interface/uparrow_green.png" title="UP 7 in the last month" width="16"/></div><div id="top10artistchart-arrowtext">7</div></div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/track/small/13922322.jpg"/></div><div id="top10trackchart-text">
<div id="top10trackchart-artistname">THE YELLOWHEADS, SPACE 92</div><div id="top10trackchart-title"><strong>Planet X</strong></div><div id="top10trackchart-points"><strong>12628</strong> POINTS /<strong> 159</strong> DAYS</div> <a href="https://pro.beatport.com/track/planet-x-original-mix/13922322" target="_blank"><div id="shopping-icon"><img height="20" src="https://www.beatstats.com/_images/interface/shopping_icon_alpha.png" width="20"/></div></a>
<a href="https://pro.beatport.com/track/planet-x-original-mix/13922322" target="_blank"><div id="play-icon"><img height="22" src="https://www.beatstats.com/_images/interface/play_icon_alpha.png" width="22"/></div></a><a> </a></div>
</div></a>
<a href="https://www.beatstats.com/track/keep-control-artbat-remix/12792193">
<div id="top10artistchart">
<div id="top10artistchart-number">6<div id="top10artistchart-arrow"><img height="16" src="https://www.beatstats.com/_images/interface/uparrow_green.png" title="UP 1 in the last month" width="16"/></div><div id="top10artistchart-arrowtext">1</div></div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/track/small/12792193.jpg"/></div><div id="top10trackchart-text">
<div id="top10trackchart-artistname">SONO</div><div id="top10trackchart-title"><strong>Keep Control</strong> ( ARTBAT Remix )</div><div id="top10trackchart-points"><strong>12432</strong> POINTS /<strong> 161</strong> DAYS</div> <a href="https://pro.beatport.com/track/keep-control-artbat-remix/12792193" target="_blank"><div id="shopping-icon"><img height="20" src="https://www.beatstats.com/_images/interface/shopping_icon_alpha.png" width="20"/></div></a>
<a href="https://pro.beatport.com/track/keep-control-artbat-remix/12792193" target="_blank"><div id="play-icon"><img height="22" src="https://www.beatstats.com/_images/interface/play_icon_alpha.png" width="22"/></div></a><a> </a></div>
</div></a>
<a href="https://www.beatstats.com/track/midnight-original-mix/12806184">
<div id="top10artistchart">
<div id="top10artistchart-number">7</div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/track/small/12806184.jpg"/></div><div id="top10trackchart-text">
<div id="top10trackchart-artistname">HOSH, 1979, JALJA</div><div id="top10trackchart-title"><strong>Midnight</strong></div><div id="top10trackchart-points"><strong>12278</strong> POINTS /<strong> 151</strong> DAYS</div> <a href="https://pro.beatport.com/track/midnight-original-mix/12806184" target="_blank"><div id="shopping-icon"><img height="20" src="https://www.beatstats.com/_images/interface/shopping_icon_alpha.png" width="20"/></div></a>
<a href="https://pro.beatport.com/track/midnight-original-mix/12806184" target="_blank"><div id="play-icon"><img height="22" src="https://www.beatstats.com/_images/interface/play_icon_alpha.png" width="22"/></div></a><a> </a></div>
</div></a>
<a href="https://www.beatstats.com/track/slow-down-feat-jorja-smith-vintage-culture-and-slow-motion-extended-remix/13270039">
<div id="top10artistchart">
<div id="top10artistchart-number">8</div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/track/small/13270039.jpg"/></div><div id="top10trackchart-text">
<div id="top10trackchart-artistname">MAVERICK SABRE, JORJA SMITH</div><div id="top10trackchart-title"><strong>Slow Down (feat. Jorja Smith)</strong> ( Vintage Culture &amp; Slow Mo.. )</div><div id="top10trackchart-points"><strong>12259</strong> POINTS /<strong> 205</strong> DAYS</div> <a href="https://pro.beatport.com/track/slow-down-feat-jorja-smith-vintage-culture-and-slow-motion-extended-remix/13270039" target="_blank"><div id="shopping-icon"><img height="20" src="https://www.beatstats.com/_images/interface/shopping_icon_alpha.png" width="20"/></div></a>
<a href="https://pro.beatport.com/track/slow-down-feat-jorja-smith-vintage-culture-and-slow-motion-extended-remix/13270039" target="_blank"><div id="play-icon"><img height="22" src="https://www.beatstats.com/_images/interface/play_icon_alpha.png" width="22"/></div></a><a> </a></div>
</div></a>
<a href="https://www.beatstats.com/track/drop-the-pressure-extended-version/13224643">
<div id="top10artistchart">
<div id="top10artistchart-number">9</div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/track/small/13224643.jpg"/></div><div id="top10trackchart-text">
<div id="top10trackchart-artistname">MYLO, CLAPTONE</div><div id="top10trackchart-title"><strong>Drop The Pressure</strong></div><div id="top10trackchart-points"><strong>11326</strong> POINTS /<strong> 145</strong> DAYS</div> <a href="https://pro.beatport.com/track/drop-the-pressure-extended-version/13224643" target="_blank"><div id="shopping-icon"><img height="20" src="https://www.beatstats.com/_images/interface/shopping_icon_alpha.png" width="20"/></div></a>
<a href="https://pro.beatport.com/track/drop-the-pressure-extended-version/13224643" target="_blank"><div id="play-icon"><img height="22" src="https://www.beatstats.com/_images/interface/play_icon_alpha.png" width="22"/></div></a><a> </a></div>
</div></a>
<a href="https://www.beatstats.com/track/more-life-extended-mix/12747484">
<div id="top10artistchart">
<div id="top10artistchart-number">10</div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/track/small/12747484.jpg"/></div><div id="top10trackchart-text">
<div id="top10trackchart-artistname">TORREN FOOT</div><div id="top10trackchart-title"><strong>More Life</strong></div><div id="top10trackchart-points"><strong>10919</strong> POINTS /<strong> 143</strong> DAYS</div> <a href="https://pro.beatport.com/track/more-life-extended-mix/12747484" target="_blank"><div id="shopping-icon"><img height="20" src="https://www.beatstats.com/_images/interface/shopping_icon_alpha.png" width="20"/></div></a>
<a href="https://pro.beatport.com/track/more-life-extended-mix/12747484" target="_blank"><div id="play-icon"><img height="22" src="https://www.beatstats.com/_images/interface/play_icon_alpha.png" width="22"/></div></a><a> </a></div>
</div></a>
<a href="https://www.beatstats.com/tracks/list/list-YEAR-genre0">
<div id="top10artistchart-footer"><span class="charttextname-c">SHOW FULL CHART</span> </div></a>
</div>
<div id="content-top10labels">
<div id="top10artistchart-header"><span class="charttextname-a">TOP <strong>LABELS</strong></span> </div>
<a href="https://www.beatstats.com/label/defected/1354-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">1</div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/label/small/1354.jpg"/></div>
<div id="top10labelchart-name"><span class="labelcharttextname">DEFECTED</span>
<br/>
<span class="charttextpoints">98066 POINTS</span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/label/toolroom/495-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">2</div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/label/small/495.jpg"/></div>
<div id="top10labelchart-name"><span class="labelcharttextname">TOOLROOM RECORDS</span>
<br/>
<span class="charttextpoints">59127 POINTS</span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/label/drumcode/2027-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">3</div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/label/small/2027.jpg"/></div>
<div id="top10labelchart-name"><span class="labelcharttextname">DRUMCODE</span>
<br/>
<span class="charttextpoints">53193 POINTS</span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/label/glasgow-underground/14542-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">4<div id="top10artistchart-arrow"><img height="16" src="https://www.beatstats.com/_images/interface/uparrow_green.png" title="UP 1 in the last month" width="16"/></div><div id="top10artistchart-arrowtext">1</div></div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/label/small/14542.jpg"/></div>
<div id="top10labelchart-name"><span class="labelcharttextname">GLASGOW UNDERGROUND</span>
<br/>
<span class="charttextpoints">40792 POINTS</span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/label/rca-records-label/34322-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">5</div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/label/small/34322.jpg"/></div>
<div id="top10labelchart-name"><span class="labelcharttextname">RCA RECORDS LABEL</span>
<br/>
<span class="charttextpoints">37792 POINTS</span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/label/1605/10775-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">6<div id="top10artistchart-arrow"><img height="16" src="https://www.beatstats.com/_images/interface/uparrow_green.png" title="UP 1 in the last month" width="16"/></div><div id="top10artistchart-arrowtext">1</div></div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/label/small/10775.jpg"/></div>
<div id="top10labelchart-name"><span class="labelcharttextname">1605</span>
<br/>
<span class="charttextpoints">32394 POINTS</span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/label/anjunadeep/1390-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">7<div id="top10artistchart-arrow"><img height="16" src="https://www.beatstats.com/_images/interface/uparrow_green.png" title="UP 2 in the last month" width="16"/></div><div id="top10artistchart-arrowtext">2</div></div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/label/small/1390.jpg"/></div>
<div id="top10labelchart-name"><span class="labelcharttextname">ANJUNADEEP</span>
<br/>
<span class="charttextpoints">30936 POINTS</span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/label/black-book-records/60197-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">8<div id="top10artistchart-arrow"><img height="16" src="https://www.beatstats.com/_images/interface/uparrow_green.png" title="UP 2 in the last month" width="16"/></div><div id="top10artistchart-arrowtext">2</div></div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/label/small/60197.jpg"/></div>
<div id="top10labelchart-name"><span class="labelcharttextname">BLACK BOOK RECORDS</span>
<br/>
<span class="charttextpoints">30519 POINTS</span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/label/club-sweat/30552-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">9</div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/label/small/30552.jpg"/></div>
<div id="top10labelchart-name"><span class="labelcharttextname">CLUB SWEAT</span>
<br/>
<span class="charttextpoints">29242 POINTS</span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a><a href="https://www.beatstats.com/label/sola/63883-genre0"><div id="top10artistchart-med">
<div id="top10artistchart-number">10<div id="top10artistchart-arrow"><img height="16" src="https://www.beatstats.com/_images/interface/uparrow_green.png" title="UP 4 in the last month" width="16"/></div><div id="top10artistchart-arrowtext">4</div></div>
<div id="top10artistchart-image"><img height="50" src="https://www.beatstats.com/images/label/small/63883.jpg"/></div>
<div id="top10labelchart-name"><span class="labelcharttextname">SOLA</span>
<br/>
<span class="charttextpoints">25869 POINTS</span>
<br/>
<span class="chartinfotext"></span>
</div>
</div></a> <a href="https://www.beatstats.com/labels/list/list-YEAR-genre0">
<div id="top10artistchart-footer"><span class="charttextname-c">SHOW FULL CHART</span> </div></a>
</div>
</div><!-- #content -->
</div><!-- #container -->
<div id="footer">
<div id="headercontainer">
<a href="https://www.beatstats.com/info/info/list">
<div id="footer-points"><strong>POINTS</strong> are calculated based on a Song's daily position in the <strong>BEATPORT Top 100 Charts</strong>. If a track is at position <strong>1</strong>, it gets <strong>100 points</strong>, if it is in position <strong>100</strong>, it gets <strong>1 point</strong>. Charts are now updated daily! <strong>This site is not affiliated with BEATPORT</strong> </div></a>
<div id="footer-disclaimer"><a href="https://twitter.com/beatstats"><strong>TWITTER</strong></a><br/><a href="https://www.facebook.com/beatstats/"><strong>FACEBOOK</strong></a></div>
<div id="footer-disclaimer2"><strong><a href="https://www.beatstats.com/blog/blog/list">UPDATES</a></strong><br/><strong><a href="https://www.beatstats.com/info/info/list">ABOUT / FAQ</a></strong><br/><strong><a href="https://www.beatstats.com/contact/contact/list">CONTACT US</a></strong><br/></div>
</div>
</div><!-- #footer -->
</body>
</html>
