<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "48962",
      cRay: "610a992d1f7e1a80",
      cHash: "432162e1918859f",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuZGVzdGlub3JzLmNvbS5ici9pbWcvaW1hZ2VzL3N0YXJ0OC5waHAlMDklMEE=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "b2i/2qheyZN0qd2CZMkI+RjVVynMHcqsWaoH7EQJ09mtlD6dOl+aQIrb3VjGNs8096KLk6hOaqvtnwqe/bpbLi0K6p68ZJ/enCUCgxdEh+BiOTY+FNua1QZUQuHfMea/jF9iVO6IxeqbYcwEx9aYdcBPuU8geKrsgEq7g1yjtxI3DXDa+FmM6MNmsQp7yalY1oaMGpNlD5per37KtFk8PKs7fhl15IkVT+a3lSUhIPdh+Nxx7KPPzjmdmHuT8cBedsCskIYLx+gCDpvkCq7FWLsA/ob47Oya0ChaRtdlVePO0L5jgC7OboumSrHD02R0YV8vMyQM+xm1qwfgWLdCvqX5uuPiK98DWVpTyQOUqp5fA0pZAL0HyC0Cqw/CKFW9Md+Hx6zuK1lhB23FeLJqJ6WWUErw+KRvEHCVd1RtYhGAeiR9Ig1SwL1zTHenlK9kCd86pvndYN0RWd9ZStBV1zQCj+puDZhB4ums4qvKTHLEyX/N/fWf6ZvfEC/bXiPCbS0jShAjtqPUI4+6dPpnwlZugRJis1X5Mk93JrSRaq3qp/b07K8K9yRWteBYkD6bafpH1L3qwiWXNCG26WSqWSf2wLlE3i0g5W4EwRr5krjFnNDmku9D1K6MdJNpaEtyMekhNvsVYc/H+1FqKtfgU18yz8MkDaiU0aMu+E9PMCNjVKxJGwBYFnwRiIX2vWEPFyVsQ43VpBP2fSc233CVR5m8V9HIOvprtNTi4fFwU8L5PgHHl+T2zwN8p2RsroXZ",
        t: "MTYxMDQ5MzUyNC4wMTgwMDA=",
        m: "7zYKVI45PUXaj3lb6gEw26BdnIwb4gE9KL7zrVpMjrU=",
        i1: "wbmJ//VIyzvlXCgUQoV1rA==",
        i2: "GssuQGliUAZc2dpVyfazlw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "XSfVqyLeGsVRua9dqidyTogKaGA3n66ouckApQNsyrg=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.destinors.com.br</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/img/images/start8.php%09%0A?__cf_chl_captcha_tk__=4f032431a698fce7c878666d0e4b20754adeeda0-1610493524-0-AR3w5uuID6_wGPf7lz7ueyyk-QM2tytiHIccEhntO5GrzuRXSdL5rqJg0fnz98_bSRenGA4LgxwUU-5ckNI--VXwznP0Ee-IeHuZLA_MGrvn9y5Wke1XM-MarBoNM3S_sEL1eIcr7PzfaevXJeMVtQR4NyUWcF2kjVwgIDwkkmCZKv5WSzObgObarF4hiqx48mg2TkB0ebic3hcMG-DLlsKd_vOcJl3OwSDSUsUaaAOaak3pSidCIKC3BB2H1sN-ER8rpOnNZ7zbH6-Jg_Bmct2CRDZf2GyKvbOhutSrkmCzawc5jJlKQyNfVVC_JrqMuQqtHg3DeKMZ8VWlnZKgPCdLr8_jbpVstNaIRt6ATCMW4dDa6IS-xkyWdEPS5-vLNAZKCCG4CbUV7lL6Ps9As3bJJqCy7MjKTp_WExAgCzvTL2WTF5ejmtw8Mw4BnPDZXCUWXAUE5-OtEiN43zqnnYeByPLm5eicZ3KURWAa9CixUx4c72yAHpbTHFkpSuj3WbNnBDw5XpbQ_QyOaS4dPW5yBla5p3ZNrtV5lIt6Or8JAQ11JjD4S_3x2YySiQ25MA" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="b92b21977423a075c5c4a19ab0eb5bee73b5f961-1610493524-0-AfV2cIlw/5ImPZN9oMeiKGCFKvk5f9Ae4cv5ZPdWRgHuaONS1MlJdsBMz5xt/58DZWXt/79z8+m2B6+tua+VhwzNf66FEm4A54IFUXsqeXGQfmhvPsG0yE5OyDVLsodWOHgM96OAX+b8MYQE4YBmUZ0xnCVMNkVb7ShPPrKf98zvABnuYaAbVv5QeYM/9MWocWRzBxszYMhPI6q5He6cLdzH/Av8XzsLbN+qRVODUKLDbJL4vY2yk80YkzTsfq+hakrGezjestt8fgXqxjB/CnzHEssRY1UGuPjo6gqDtZoAqU0skGXZXUEmYG0xWFBPLYXPXzDD0CZzDm6N4SE8KrnLaa9m7HRw8YfLnpE5l5koR02gDa7p+6e8/BUCIHPLadFGj7kAlx63QJRlaUUhHgcdVKLLfJK4wPnaA48M7tMGqzY4uCK+7IeKaNNsGRwvlYlX8tyuUskE543iKNdgBvfM1uyS0P/UMCYF99h6x7YqsYFJ/6bqlD4EF3ySvKjMKZaaXNcHvEyLd6kgbTlgLv0A6WbucfjTF14VtDtTO6kP4g9nLXN8qBnDXpS6neXFuOCi0jEht1ipZsvxaf1x+J1N8g1QbDHixNgL7Ntdf/vpnnxhSPOh1XHFpPjzmdLQnalHV+DoqsSDLOSrAOnxuNkszka8+a5E1GGhnqZo8ZG9KKxrd19735rMBZFAxsIiNKeKxwF7mA3TVFVCs5zDM0Os9NbTCDEaXs+kupAe5NV8ipCbUf+aRebcE8sUit9/pe5RlddeoEtjCbAkwEQm+CthWj4Lt1doHlVUzaYW7A/ywdS+3TdlCtygIrFd6lWQ6zKLhH1d7XjACJq5I1SvXT3/stofP2J3FT9d7poH6OQFuqKlaHXhZziq+HFdEMKszogJANXrUGWMNMHW9PrIyQ7T/lc/1zU8UROVh10d0ayd8GRPH/izgDrUXhS7LS0Qt2gXthqe/7kjlvCMGLZDU/sL2sA9jDFuXnP9LoO1KXqyRkcg/5IolXPs37lxfgfIImm5RiV456uFohEuOpS+MZzgFZMInt5gaqgefEa4e1rhlAjM+2K6Ijdm5VRoHrtZV8cUSaRAG812rDvJHWL6oFbsuf5j1mnAQPJUPCpK3X7DVPmk/h4y3B+pL/yjrLXYs7/I14h++bFNw4QDuqP8rXVoJzOQux4N/lna/9O3JxGvFXfj31Tv34+OzQRjhOLMrrNbILAeJofU8YnHyZ4x0c3ytl9Uiq0XJvk3vw7ALgKcfMnFLjujtmhaSAZH2xOqr3oW40iZOhq7V2Jzrn1E9cPyLPsAIUo2jyAiWaqqGRGmnJTXq6dh3rBmtSPMls++gw=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="d82f5af53aac696fdfbdc9e29ac3a8cd"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610a992d1f7e1a80')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<a href="https://thing-dot.com/bearded.php?cfm=8694"></a>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610a992d1f7e1a80</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
