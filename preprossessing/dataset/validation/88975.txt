<!DOCTYPE html>
<html>
<head>
<style>
		@font-face {
		  font-family: 'FontAwesome';
		  src: url('/bitrix/templates/AIR20/fonts/fontawesome-webfont.eot?v=4.7.0');
		  src: url('/bitrix/templates/AIR20/fonts/fontawesome-webfont.eot?#iefix&v=4.7.0') format('embedded-opentype'), url('/bitrix/templates/AIR20/fonts/fontawesome-webfont.woff2?v=4.7.0') format('woff2'), url('/bitrix/templates/AIR20/fonts/fontawesome-webfont.woff?v=4.7.0') format('woff'), url('/bitrix/templates/AIR20/fonts/fontawesome-webfont.ttf?v=4.7.0') format('truetype'), url('/bitrix/templates/AIR20/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular') format('svg');
		  font-weight: normal;
		  font-style: normal;		  
		  font-display: swap;
		}
	</style>
<link as="font" crossorigin="" href="/bitrix/templates/AIR20/font/fontawesome-webfont.woff" rel="preload" type="font/woff2"/>
<link href="https://www.airlines-inform.ru/bitrix/templates/AIR20/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/world_airlines/lan_airlines.html" rel="canonical"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>LATAM Airlines. Airline code, web site, phone, reviews and opinions.</title>
<meta content="LATAM Airlines - Largest Chilean airline, extended domestic and international services in South America, formerly known as LAN Airlines. Airline code, web site, phone, reviews and opinions." name="description"/>
<meta content="LATAM Airlines airline code LATAM Airlines web site phone LATAM Airlines reviews opinions LATAM Airlines Chile Largest Chilean airline, extended domestic and international services in South America, formerly known as LAN Airlines" name="keywords"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="ALL" name="ROBOTS"/>
<script>
var resolut = Math.round(window.outerWidth*0.78);
document.cookie='resolution='+resolut+'; path=/';
</script>
<link href="/bitrix/js/main/core/css/core.css?1308106319" rel="stylesheet" type="text/css"/>
<link href="/bitrix/js/socialservices/css/ss.css?1308106329" rel="stylesheet" type="text/css"/>
<link href="/bitrix/js/main/core/css/core_window.css?1308106320" rel="stylesheet" type="text/css"/>
<link href="/bitrix/templates/AIR20/styles.css?1609477891" rel="stylesheet" type="text/css"/>
<link as="style" href="https://www.airlines-inform.ru/bitrix/templates/AIR20/js/fancybox2/jquery.fancybox-1.3.4.css?01" onload="this.rel='stylesheet'" rel="preload" type="text/css"/>
<script src="/bitrix/templates/AIR20/js/jquery.min.js" type="text/javascript"></script>
<script src="/bitrix/js/main/core/core.js?1308106324" type="text/javascript"></script>
<script type="text/javascript">BX.message({'LANGUAGE_ID':'en','FORMAT_DATE':'DD.MM.YYYY','FORMAT_DATETIME':'DD.MM.YYYY HH:MI:SS','COOKIE_PREFIX':'BITRIX_SM','bitrix_sessid':'8a1927b7197712881c5f0d0ac42bf129','JS_CORE_LOADING':'Loading...','JS_CORE_WINDOW_CLOSE':'Close','JS_CORE_WINDOW_EXPAND':'Expand','JS_CORE_WINDOW_NARROW':'Restore','JS_CORE_WINDOW_SAVE':'Save','JS_CORE_WINDOW_CANCEL':'Cancel','JS_CORE_H':'h','JS_CORE_M':'m','JS_CORE_S':'s'})</script>
<script async="" defer="" src="/bitrix/js/main/core/core_ajax.js?1308106320" type="text/javascript"></script>
<script src="/bitrix/js/main/session.js?1308106306" type="text/javascript"></script>
<script type="text/javascript">
bxSession.Expand(1440, '8a1927b7197712881c5f0d0ac42bf129', false, '1a78fd629ff2580e03df101e54d99ebc');
</script>
<script src="/bitrix/js/main/core/core_window.js?1308106320" type="text/javascript"></script>
<script async="" defer="" src="/bitrix/js/socialservices/ss.js?1308106327" type="text/javascript"></script>
<script src="/bitrix/js/main/core/core_window.js" type="text/javascript"></script>
<script async="" defer="" src="/bitrix/js/main/core/core_ajax.js" type="text/javascript"></script>
<script src="https://www.airlines-inform.ru/bitrix/templates/AIR20/js/autoresize.js?2" type="text/javascript"></script>
<script src="/bitrix/templates/AIR20/js/script.js?57" type="text/javascript"></script>
<script src="https://www.airlines-inform.ru/bitrix/templates/AIR20/js/preLoad.ai.js?15910" type="text/javascript"></script>
<script src="https://www.airlines-inform.ru/bitrix/templates/AIR20/modules/photos/scripts.js?007" type="text/javascript"></script>
<script src="https://www.airlines-inform.ru/bitrix/templates/AIR20/js/fancybox2/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script>
<script type="text/javascript">

	function showhidemenu(){
		if($(".mobile-left").length>0){
			$(".mobile-left").removeClass("mobile-left");	
			$("#overmenu").remove();
			$("#close-menu").hide();		
		}
		else{	
			
			$("#center, #center1, #center2").prepend("<div id='overmenu'></div>");
			$("#overmenu").height($("body").height());
			$("#overmenu").show();
			$("#overmenu").unbind("click");
			$("#overmenu").bind("click", function(){showhidemenu();});
			$("#left").addClass("mobile-left");
			$("#close-menu").show();			
		}
	}
	

	$(window).scroll(function(){
		if($(window).scrollTop() >= 3000){
			$('.roll-button').fadeIn(1000);
			//$('#ajx-pagination').fadeIn(1000);
			
		}
		else{
			$('.roll-button').fadeOut(1000);
			//$('#ajx-pagination').fadeOut(1000);
		}		
		
		
		var rightHeight = 0;
		rightHeight = $("#right").height();
		if(!rightHeight)
			rightHeight = $("#right3").height();
		//console.log(rightHeight);
		
			
		if($(window).width() > "1279"){
	    	
	    	if($(window).scrollTop() > rightHeight+150)
	    		$('.adv2').addClass('adv2fixed');
	    	else
	    		$('.adv2').removeClass('adv2fixed');
	    		

	    		    			    		if($(window).scrollTop() > 1600 && $(window).scrollTop() < 2000)
	    			    		$('.adv1').addClass('adv2fixed1');
	    	else{
	    		$('.adv1').removeClass('adv2fixed1');	    			    		
	    	}
	    		    }
	    
	    if($(window).width() >= "940" && $(window).width() <= "1279"){	    	
	    	if($(window).scrollTop() > $("#left-left").height()+150 && $(window).scrollTop() < 2000)
	    		$('.adv2').addClass('adv2fixed');
	    	else
	    		$('.adv2').removeClass('adv2fixed');	    		
	    	
	    }
	    
	    if($(window).width() <= 940){	    	
	    	
	    		$('.hide940').hide();	    		
	    	
	    }
	    
	    if($(window).scrollTop() >= 2000){
	    		$('.social-share').addClass('adv2fixed');
	    		$('.social-share').css('width', ($('#menuleft').width())+'px');
	    		$('#opros').addClass('adv2fixed');
	    		$('#opros').css('width', ($('#menuleft').width())+'px');
	    		$('#opros').css('top', '130px');
	    	}
	    	else{
	    		$('.social-share').removeClass('adv2fixed');
	    		$('.social-share').css('width', '100%');
	    		$('#opros').removeClass('adv2fixed');
	    		$('#opros').css('width', '100%');
	    	}
		
		
	});

  $(function(){
  	

  	
  	$(".message-photo-admin>a, .message-photo>a, a.fancybox").fancybox({titleShow: 'false', width: 'auto'});

  	
  	  	$('.my-profile').hover(function(){
  		$('.my-profile-menu').show();
  	});
  	$('.my-profile').click(function(){
  		$('.my-profile-menu').show();
  	});
  	
  	
  	$('#auth-profile').hover(function(){
  		$('.auth-menu-box').show();
  	});
  	$('#auth-profile').click(function(){
  		$('.auth-menu-box').show();
  	});
  	
  	
  	$('#center1, #center, #left-left, #greytop, #top').hover(function(){
  		$('.my-profile-menu').hide();
  		$('.auth-menu-box').hide();
  	});
  	
  	if($("#right").length)
  		$('.adv1').hide();
  	
  	$('.roll-button').click(function(){$('.roll-button').fadeOut(300);});
  	
  	/*var resolut = Math.round($(window).width()*0.78);  	
    document.cookie='resolution='+resolut+'; path=/';*/
    
    $('#over').css('height', $(document).height());
   	$('#over').css('width', '100%');
   	$(document).keydown(
		function(event){
	  		if(event.keyCode == '27')
	  			{$('#over').css('display', 'none'); $('.hidew').css('display', 'none');}  
		}	       
    );
    $("#over").click(function(){
    	$('#over').css('display', 'none'); $('.hidew').css('display', 'none');
    });
    
    
    
    if($('body').height() < 1800){
    	$(".adv1").css('display', 'none');
    }
    
     if($(window).width() < 1279){
    	$("#right3").css('display', 'none');
    }
    
	$(".auth_forgot").click(function(){
		$('.hidew').css('display', 'none');
		$(".forgot_window").load("/tm-scripts/forgot.php?ver=2");
		$(".forgot_window").show();		
		
	});
	
	$(".auth_register").click(function(){
		$('.hidew').css('display', 'none');
		$(".register_window").load("/tm-scripts/register.php?ver=2&success=/world_airlines/lan_airlines.html?AIR_CODE=lan_airlines&ver=2");
		$(".register_window").show();		
		
	});
	
   
    
  });
  

</script>
<!--[if lt IE 9]>
        <script src="/bitrix/templates/AIR20/js/html5shiv.js"></script>
        <script src="/bitrix/templates/AIR20/js/respond.min.js"></script>
<![endif]-->
</head>
<body>
<a name="up"></a>
<div id="page">
<div id="over"></div>
<div class="auth_window hidew">
<div class="notauthred" id="noticeauth"></div>
<form action="/bitrix/urlrewrite.php?SEF_APPLICATION_CUR_PAGE_URL=%2Fworld_airlines%2Flan_airlines.html" method="post" target="_top">
<input name="backurl" type="hidden" value="/world_airlines/lan_airlines.html?AIR_CODE=lan_airlines&amp;ver=2"/>
<input name="AUTH_FORM" type="hidden" value="Y"/>
<input name="TYPE" type="hidden" value="AUTH"/>
<input class="auth_input auth_l" maxlength="50" name="USER_LOGIN" placeholder="Login" type="text" value=""/>
<input class="auth_input auth_p" maxlength="50" name="USER_PASSWORD" placeholder="Password" type="password"/>
<div class="chkme">
<input checked="checked" id="USER_REMEMBER_frm" name="USER_REMEMBER" type="checkbox" value="Y"/>
<label for="USER_REMEMBER_frm" title="Remember me">Remember me</label>
</div>
<input class="buy_button_auth auth_but" name="Login" type="submit" value="Login"/>
<noindex><a class="reg_but auth_register" href="javascript:void(0);" rel="nofollow">Registration</a></noindex>
<noindex><a class="auth_forgot" href="javascript:void(0);" rel="nofollow">Forgot your password?</a></noindex>
<div class="auth_ico">
<div class="bx-auth-serv-icons">
<a href="javascript:void(0)" onclick="BX.util.popup('https://www.facebook.com/dialog/oauth?client_id=1468799666696457&amp;redirect_uri=https%3A%2F%2Fwww.airlines-inform.com%2Findex.php%3Fauth_service_id%3DFacebook&amp;scope=email&amp;display=popup', 580, 400)" title="Facebook"><i class="bx-ss-icon facebook"></i></a>
<a href="javascript:void(0)" onclick="BxShowAuthFloat('Blogger', 'form')" title="Blogger"><i class="bx-ss-icon blogger"></i></a>
<a href="javascript:void(0)" onclick="BxShowAuthFloat('Livejournal', 'form')" title="Livejournal"><i class="bx-ss-icon livejournal"></i></a>
</div>
</div>
</form>
<div style="display:none">
<div class="bx-auth-float" id="bx_auth_float">
<div class="bx-auth">
<form action="/bitrix/urlrewrite.php?SEF_APPLICATION_CUR_PAGE_URL=%2Fworld_airlines%2Flan_airlines.html" method="post" name="bx_auth_servicesform" target="_top">
<div class="bx-auth-title">Login As</div>
<div class="bx-auth-note">You can log in if you are registered at one of these services:</div>
<div class="bx-auth-services">
<div><a href="javascript:void(0)" id="bx_auth_href_formFacebook" onclick="BxShowAuthService('Facebook', 'form')"><i class="bx-ss-icon facebook"></i><b>Facebook</b></a></div>
<div><a href="javascript:void(0)" id="bx_auth_href_formBlogger" onclick="BxShowAuthService('Blogger', 'form')"><i class="bx-ss-icon blogger"></i><b>Blogger</b></a></div>
<div><a href="javascript:void(0)" id="bx_auth_href_formLivejournal" onclick="BxShowAuthService('Livejournal', 'form')"><i class="bx-ss-icon livejournal"></i><b>Livejournal</b></a></div>
</div>
<div class="bx-auth-line"></div>
<div class="bx-auth-service-form" id="bx_auth_servform" style="display:none">
<div id="bx_auth_serv_formFacebook" style="display:none"><a class="bx-ss-button facebook-button andnone" href="javascript:void(0)" onclick="BX.util.popup('https://www.facebook.com/dialog/oauth?client_id=1468799666696457&amp;redirect_uri=http%3A%2F%2Fwww.airlines-inform.com%2Fworld_airlines%2Flan_airlines.html%3Fauth_service_id%3DFacebook%26AIR_CODE%3Dlan_airlines%26ver%3D2&amp;scope=email&amp;display=popup', 580, 400)"></a><span class="bx-spacer"></span><span>Use your Facebook.com profile to log in.</span></div>
<div id="bx_auth_serv_formBlogger" style="display:none">
<span class="bx-ss-icon openid"></span>
<input name="OPENID_IDENTITY_BLOGGER" size="20" type="text" value=""/>
<span>.blogspot.com</span>
<input class="button" name="" type="submit" value="Log In"/>
</div>
<div id="bx_auth_serv_formLivejournal" style="display:none">
<span class="bx-ss-icon openid"></span>
<input name="OPENID_IDENTITY_LIVEJOURNAL" size="20" type="text" value=""/>
<span>.livejournal.com</span>
<input class="button" name="" type="submit" value="Log In"/>
</div>
</div>
<input name="auth_service_id" type="hidden" value=""/>
</form>
</div>
</div>
</div>
</div>
<div class="forgot_window hidew">
</div>
<div class="register_window hidew">
</div>
<div id="ajax-loader"><img src="https://www.airlines-inform.ru/bitrix/templates/AIR/images/ajax-loader.gif"/></div>
<header id="top">
<a class="logo" href="https://www.airlines-inform.com/"><font class="airlines">Airlines</font> <font class="inform">Inform</font></a>
<div id="slogan">
	 			your guide to airlines all over the world	 		</div>
<div id="menubutton">
<a class="filter_set" href="javascript:void(0);" onclick="showhidemenu();">Menu<i id="close-menu">×</i></a>
</div>
</header>
<div id="poisktop">
<form action="/search/">
<a href="javascript:void(0);" onclick="$('.auth_window').css('display', 'block'); $('#over').css('display', 'block');">Login</a>
<input class="sitepoisk" name="q" placeholder="site search..." type="text" x-webkit-speech="x-webkit-speech"/>
<input class="spbutton" type="submit" value="Search"/>
</form>
</div>
<div id="greytop">
<div id="kroshki">
<div class="bread-itemscope" itemscope="" itemtype="http://schema.org/BreadcrumbList"><span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a href="https://www.airlines-inform.com" itemprop="item" title="HOME"><span itemprop="name">HOME</span></a><meta content="1" itemprop="position"/></span> ⇒ <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a href="/world_airlines/" itemprop="item" title="World airlines"><span itemprop="name">World airlines</span></a><meta content="2" itemprop="position"/></span> ⇒ <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a href="/Chile/" itemprop="item" title="Chile"><span itemprop="name">Chile</span></a><meta content="3" itemprop="position"/></span> ⇒ <span class="bc">LATAM Airlines</span></div> </div>
</div>
<div id="left-left">
<div id="left">
<div class="roll-button"><a href="#up">Top ▲</a></div>
<div id="ajx-pagination"></div>
<nav id="menuleft">
<div class="menu1">
<a class="lastlink" href="https://www.airlines-inform.com/" title="HOME">HOME</a>
</div>
<div class="menu2">
<a href="https://www.airlines-inform.com/world_airlines/" title="Airlines of the World">Airlines of the World</a>
<a href="https://airport.airlines-inform.com/" title="Airports of the World">Airports of the World</a>
<a href="https://www.airlines-inform.com/low_cost_airlines/" title="Low-Cost Airline Guide">Low-Cost Airline Guide</a>
<a href="https://www.airlines-inform.com/commercial-aircraft/" title="Commercial Aircraft Guide">Commercial Aircraft Guide</a>
<a class="lastlink" href="https://www.airlines-inform.com/rankings/" title="Airline &amp; Airport Rankings">Airline &amp; Airport Rankings</a>
</div>
<div class="menu3">
<a href="https://www.airlines-inform.com/airline_reviews/" title="Airline Reviews">Airline Reviews</a>
<a href="https://airport.airlines-inform.com/airport-reviews/" title="Airport Reviews">Airport Reviews</a>
<a href="https://www.airlines-inform.com/aircraft-reviews/" title="Airplane Reviews">Airplane Reviews</a>
<a class="lastlink" href="https://www.airlines-inform.com/flight-reports/" title="Flight Reports">Flight Reports</a>
</div>
<div class="menu4">
<a class="lastlink" href="https://photo.airlines-inform.com/" title="Airline Photos">Airline Photos</a>
</div>
<div class="menu5">
<a class="lastlink" href="https://forum.airlines-inform.com/" title="Forum">Forum</a>
</div>
</nav>
<div id="shareblock">
<script>
 $(function(){
 	//console.log($("title").html());
 	euri = $("title").html();
 	$(".twi").attr("href", encodeURI("https://twitter.com/share?url=https://www.airlines-inform.com/world_airlines/lan_airlines.html?AIR_CODE=lan_airlines&ver=2&text="+euri));
 	$(".vki").attr("href", encodeURI("http://vk.com/share.php?url=https://www.airlines-inform.com/world_airlines/lan_airlines.html?AIR_CODE=lan_airlines&ver=2&title="+euri+"&noparse=true"));
 });
</script>
<aside class="social-share">
<div class="rovno-center1"><div class="rovno-center2">
<p class="title">Share:</p>
<a class="share-icon fbi" href="https://www.facebook.com/sharer/sharer.php?u=https://www.airlines-inform.com/world_airlines/lan_airlines.html?AIR_CODE=lan_airlines&amp;ver=2" rel="nofollow" target="_blank"></a>
<a class="share-icon twi" href="https://twitter.com/share?url=https://www.airlines-inform.com/world_airlines/lan_airlines.html?AIR_CODE=lan_airlines&amp;ver=2&amp;text=Airlines Inform. World airlines directory." rel="nofollow" target="_blank"></a>
<a class="share-icon vki" href="http://vk.com/share.php?url=https://www.airlines-inform.com/world_airlines/lan_airlines.html?AIR_CODE=lan_airlines&amp;ver=2&amp;title=Airlines Inform. World airlines directory.&amp;description=WORLD AIRLINES DIRECTORY. Largest airlines of the world. Global airline alliances. World airlines news and contacts. Airline tickets. Airlines of the USA, Canada, America, Asia, Europe, Russia and CIS, Africa, Australia and Oceania.&amp;noparse=true" rel="nofollow" target="_blank"></a>
<div class="clear"></div>
</div> </div>
</aside>
</div>
<div align="center" class="hide940"><!-- Google AdSense Start-->
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Crazy Planes - Adaptive -->
<ins class="adsbygoogle" data-ad-client="ca-pub-9358098653861583" data-ad-format="auto" data-ad-slot="3867471477" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<!-- Google AdSense Finish --> </div>
<br/>
<div align="center" class="hide940"> </div>
<br/>
<div class="adv1">
</div>
</div>
</div>
<link href="/bitrix/templates/AIR20/fa/css/font-awesome.css" rel="stylesheet"/>
<div id="center1" itemscope="" itemtype="http://schema.org/Organization">
<h1 id="h1ad" itemprop="name">LATAM Airlines</h1>
<p class="text anons-ad" id="forum-boxforma">   
       	Country: <strong>Chile</strong><br/>      
       	Year established: 1929            
        </p>
<div class="hideApp" id="resButton">
<a class="submit buttonyel rb32" onclick="setStatusPassenger(14834, 0); return false;"><i class="icon-flag"></i> I flew this airline</a>
</div>
<section class="fotoblok fotoblockmenu hideApp">
<a class="nabor4 blok-central" href="/flight-reports/lan_airlines/">Reports (0)</a>
<a class="nabor4 blok-central" href="/airline_reviews/lan_airlines/">Reviews (11)</a>
<a class="nabor4 blok-central" href="https://photo.airlines-inform.com/lan_airlines.html">Photos (0) </a>
<a class="nabor4 blok-central" href="https://forum.airlines-inform.com/lan_airlines/">Forum (22) </a>
</section>
<section class="ad-infoblok">
<script>
        function setMyWidth(from, to){
            $(function(){
                var padding = $(to).innerHeight() - $(to).height();         
                if($(document).width() <= 1359 && $(document).width() >= 729){
                    
                    if($(from).height() >= $(to).innerHeight())
                        $(to).height($(from).height() - padding); 
                    else  
                        $(to).height('auto');       
                }
                else  
                    $(to).height('auto');               
            });     
        }
        
        $(function(){
            $("#resButton").hover(
                function(){if($(".popup-status").length > 0) $(".popup-status").show();},
                function(){$(".popup-status").hide();}
            );
        });
        
    </script>
<article class="aird-left">
<img alt="LATAM Airlines airline" class="setWOfMe" itemprop="image" src="https://cdn.airlines-inform.ru/upload/iblock/eac/LATAM.jpg"/>
<div class="aird-left1 text blok-central white posrel">
<h3>General information about LATAM Airlines</h3>
<p><span itemprop="description">Largest Chilean airline, extended domestic and international services in South America, formerly known as LAN Airlines</span></p>
<p><strong>IATA code:</strong> LA</p> <p><strong>ICAO code:</strong> LAN</p>
<p><strong>Membership in the alliance:</strong>
     <a href="/airline_alliances/oneworld.html" title="Oneworld">Oneworld</a>
</p>
<p>
<strong>Web-site:</strong> <a href="http://www.latam.com" itemprop="url" rel="external noopener noreferrer" target="_blank">www.latam.com</a></p>
<p><strong>Social media profiles:</strong> <a href="https://www.facebook.com/LATAMAirlines/?brand_redir=159639460764792" rel="external noopener noreferrer" target="_blank">Facebook</a>, <a href="https://twitter.com/LATAM_CHI" rel="external noopener noreferrer" target="_blank">Twitter</a>, <a href="https://www.instagram.com/latamairlines/" rel="external noopener noreferrer" target="_blank">Instagram</a>, <a href="https://www.youtube.com/channel/UCU1eJBwR1RLg50fB-lEt0AA" rel="external noopener noreferrer" target="_blank">YouTube</a></p>
<p><strong>Head office:</strong> <span itemprop="address">Americo Vespucio 1501, Comuna de Renca, 8660360 Santiago, Chile</span></p> <p><strong>Phone number:</strong> <span itemprop="telephone">+562 2 579 8990</span></p> <p><strong>E-mail:</strong> comunicaciones.externas.eu@lan.com</p>
<p><strong>Main bases and hubs:</strong>
   <a href="https://airport.airlines-inform.com/Santiago-Arturo-Merino-Benitez.html">Santiago Arturo Merino Benitez</a> </p>
<p><strong>Fleet:</strong>
   <a href="https://www.airlines-inform.com/commercial-aircraft/Airbus-A319.html" title="Airbus A319">Airbus A319</a>, <a href="https://www.airlines-inform.com/commercial-aircraft/Airbus-A320.html" title="Airbus A320">Airbus A320</a>, <a href="https://www.airlines-inform.com/commercial-aircraft/Airbus-A321.html" title="Airbus A321">Airbus A321</a>, <a href="https://www.airlines-inform.com/commercial-aircraft/Boeing-767-300.html" title="Boeing 767-300">Boeing 767-300</a>, <a href="https://www.airlines-inform.com/commercial-aircraft/Boeing-777-200.html" title="Boeing 777-200">Boeing 777-200</a>, <a href="https://www.airlines-inform.com/commercial-aircraft/Boeing-787.html" title="Boeing 787-8">Boeing 787-8</a>, <a href="https://www.airlines-inform.com/commercial-aircraft/Boeing-787-9.html" title="Boeing 787-9">Boeing 787-9</a> </p>
<p class="greycom">Last update date: 16 October 2019</p>
</div>
</article>
<div class="air-links setWMe">
<h2 class="air-links-under-img">LATAM Airlines</h2>
<p>Rating: 
<img height="22" src="https://cdn.airlines-inform.ru/images/star.gif" width="22"/><img height="22" src="https://cdn.airlines-inform.ru/images/star.gif" width="22"/><img height="22" src="https://cdn.airlines-inform.ru/images/star.gif" width="22"/><img height="22" src="https://cdn.airlines-inform.ru/images/star.gif" width="22"/><img height="22" src="https://cdn.airlines-inform.ru/images/star_gray.gif" width="22"/>
</p>
<table>
<tbody><tr>
<td class="positiv" width="54%">54%</td>
<td class="neutral" width="42%">42%</td>
<td class="negativ" width="4%">4%</td>
</tr>
</tbody>
</table>
<ul>
<li class="positiv-text">positive</li>
<li class="neutral-text">neutral</li>
<li class="negativ-text">negative</li>
</ul>
<div class="small-text-grey invis1" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
    Votes: <span itemprop="ratingCount">56</span>. 
    Reviews: <span itemprop="reviewCount">11</span>. 
    Rating <span itemprop="ratingValue">3.73</span>
<span class="maxrate" itemprop="bestRating">5</span>
</div>
</div>
<aside class="aird-right1 yellowfon hideApp" id="hideMenuAir">
<div class="my-links-ad revlink">
<a href="/airline_reviews/lan_airlines/"><i class="fa fa-pencil-square-o"></i>Leave review</a>
<a href="https://photo.airlines-inform.com/lan_airlines.html?add=Y"><i class="fa fa-picture-o"></i>Add photo</a>
</div>
</aside>
<aside class="aird-right2 summh hideApp" id="hideButAir1">
<input class="submit subscr rb100" name="submit" onclick="$('#hideFormAir').addClass('centerAdvAir'); $('#over').show();" type="submit" value="Find Cheap Airline Ticket"/>
</aside>
<aside class="aird-right2 summh hideAppAdv" id="hideAdvAir1">
<!-- Google AdSense Start-->
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Crazy Planes - Adaptive -->
<ins class="adsbygoogle" data-ad-client="ca-pub-9358098653861583" data-ad-format="auto" data-ad-slot="3867471477" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<!-- Google AdSense Finish -->
</aside>
<aside class="aird-right2 hideApp" id="hideFormAir" style="display: none;">
<i class="fa fa-close" onclick="$('#over').hide(); $('#hideFormAir').removeClass('centerAdvAir');"></i>
<section class="right">
<div class="search_airline bluefon buytiket-visible textcenter">
<h3>Search for Cheap Flights</h3>
<div data-affiliate="network:TDBL;publisher:2242805;market:RU" data-locale="en-US" data-skyscanner-widget="SearchWidget"></div>
<!--<script src="https://widgets.skyscanner.net/widget-server/js/loader.js" async></script>-->
<script async="" src="https://www.skyscanner.net/g/widget-server/js/loader.js"></script>
</div>
</section>
</aside>
</section>
<section class="fotoblok text margintop0 hideApp">
<a class="foto4 fotolist" href="https://photo.airlines-inform.com/lan_airlines.html?add=Y" rel="ai[photo]" title="">
<img alt="no photo" onload="cropImgHeight(this);" src="https://www.airlines-inform.ru/upload/no_foto.png"/>
<div class="btn text-block-image">
<span class="addlinkphoto">Add photos</span>
</div>
</a>
<a class="foto4 fotolist" href="https://photo.airlines-inform.com/lan_airlines.html?add=Y" rel="ai[photo]" title="">
<img alt="no photo" onload="cropImgHeight(this);" src="https://www.airlines-inform.ru/upload/no_foto.png"/>
<div class="btn text-block-image">
<span class="addlinkphoto">Add photos</span>
</div>
</a>
<a class="foto4 fotolist" href="https://photo.airlines-inform.com/lan_airlines.html?add=Y" rel="ai[photo]" title="">
<img alt="no photo" onload="cropImgHeight(this);" src="https://www.airlines-inform.ru/upload/no_foto.png"/>
<div class="btn text-block-image">
<span class="addlinkphoto">Add photos</span>
</div>
</a>
<a class="foto4 fotolist" href="https://photo.airlines-inform.com/lan_airlines.html?add=Y" rel="ai[photo]" title="">
<img alt="no photo" onload="cropImgHeight(this);" src="https://www.airlines-inform.ru/upload/no_foto.png"/>
<div class="btn text-block-image">
<span class="addlinkphoto">Add photos</span>
</div>
</a>
<script src="/bitrix/templates/AIR20/js/jquery.touchSwipe.min.js?88192" type="text/javascript"></script>
</section>
<section class="fotoblok">
<div class="foto2 whiteblock">
<h4>Reviews about the Airline</h4>
<article class="height12 hqual">
<div class="forum_data neutral">
<span class="message_date">
                        
                        24.07.2019                        
                    </span>
<span class="message_author">Guest</span>
</div>
<a class="message-a" href="/airline_reviews/lan_airlines/#message580973">
<p>
                        They do not have a “customer service” policy. So if something goes wrong good luck because it will not be their fault at all and they won’t be able to help you....                </p>
</a>
</article>
<a class="link-allpages" href="/airline_reviews/lan_airlines/">More Reviews</a>
</div>
<div class="foto2 foto2-last whiteblock">
<h4>Discussion of the Airline</h4>
<article class="height12 hqual">
<div class="forum_data airlinecolor">
<span class="message_date">
                        01.10.2019                    </span>
<span class="message_author">Katherine Batac</span>
</div>
<a class="message-a" href="https://forum.airlines-inform.com/lan_airlines/#message591036">
<p>
                        I inadvertently left on my seat 14J, my iPhone and prescription eyeglasses on Latam flight LA 2063 Lima to Cusco on September 28, 2019. Someone named Lopez Garcia called my friend  Judy Morris of Atlanta that she found my phone and for me to contact LATAM to claim my phone. Gate 1 tour guide Ronald Collada called Latam and he was advised the i...  
                    </p>
</a>
</article>
<a class="link-allpages" href="https://forum.airlines-inform.com/lan_airlines/">All Forum Posts</a>
</div>
</section>
<section class="fotoblok2 text hideApp">
<h2>LATAM Airlines Flight Reports</h2>
<p>There are no flight report about this airline. Be the first! <a href="https://www.airlines-inform.com/flight-reports/">Add Flight Report</a></p>
</section>
<div class="fotoblok hideApp">
<!-- Google AdSense Start-->
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Crazy Planes - Adaptive -->
<ins class="adsbygoogle" data-ad-client="ca-pub-9358098653861583" data-ad-format="auto" data-ad-slot="3867471477" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<!-- Google AdSense Finish --></div>
<section class="ad-infoblok">
<div class="foto3 foto3-last aset3">
</div>
</section>
<section class="mistakeinfo text greycom hideApp">
      Found a mistake on the page? <a class="punktir" href="#" onclick="$('#send-error').show();$('#over').show();return false;">Report us</a>
</section>
</div>
<div class="hidew hideApp" id="send-error">
<div id="errormess"></div>
<input id="errorurl" name="errorurl" type="hidden" value="/world_airlines/lan_airlines.html?AIR_CODE=lan_airlines&amp;ver=2"/>
<textarea id="errortext" name="errortext" placeholder="Please describe your problem" rows="7"></textarea>
<input class="submit subscr" name="submit" onclick="sendError(); return false;" type="submit" value="Send"/>
<p class="errorcancel">Esc - cancel</p>
</div>
<a name="bottom"></a>
<footer id="bottom">
<a href="https://www.airlines-inform.com/advertising/">Advertising</a>
<span> | </span>
<a href="https://www.airlines-inform.com/contact_us/">Contact Us</a>
<span> | </span>
<a href="https://www.airlines-inform.com/site_map/">Site Map</a>
<span> | </span>
<a href="https://www.airlines-inform.ru">Russian Version</a>
</footer>
<div id="mainsite">
<div class="scores">
<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t23.1;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число посетителей за"+
" сегодня' "+
"border='0' width='88' height='15'><\/a>")
//--></script><!--/LiveInternet-->
<!-- START AVIATION TOP 100 CODE: DO NOT CHANGE WITHOUT PERMISSION -->
</div>
<div class="bottext small-text">
				Airlines Inform - your guide to airlines all over the world.			</div>
<div class="bottext2 small-text">
							Copyright © 2008-2021 www.airlines-inform.com. All rights reserved.							</div>
</div>
</div> <!-- id=page end -->
<span id="cururi">/world_airlines/lan_airlines.html</span>
<span id="pagen1">1</span>
<span id="pagen2">1</span>
<span id="pagen3">1</span>
</body>
</html>
