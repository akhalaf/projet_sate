<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-GB">
<![endif]--><!--[if IE 8]>
<html class="ie ie8" lang="en-GB">
<![endif]--><!--[if !(IE 7) | !(IE 8)  ]><!--><html lang="en-GB">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<title>Page not found | ExhibitionTrailer.co.uk</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.exhibitiontrailer.co.uk/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
	<script src="https://www.exhibitiontrailer.co.uk/wp-content/themes/exhibitiontrailer/js/html5.js"></script>
	<![endif]-->
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.exhibitiontrailer.co.uk\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.exhibitiontrailer.co.uk/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.exhibitiontrailer.co.uk/wp-content/plugins/simple-sitemap/lib/assets/css/simple-sitemap.css?ver=5.4.4" id="simple-sitemap-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.exhibitiontrailer.co.uk/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.9" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.exhibitiontrailer.co.uk/wp-content/themes/exhibitiontrailer/css/style.css?ver=17-04-2019" id="exhibitiontrailer-style-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='exhibitiontrailer-ie-css'  href='https://www.exhibitiontrailer.co.uk/wp-content/themes/exhibitiontrailer/css/ie.css?ver=2016-01-07' type='text/css' media='all' />
<![endif]-->
<script src="https://www.exhibitiontrailer.co.uk/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.exhibitiontrailer.co.uk/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.exhibitiontrailer.co.uk/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.exhibitiontrailer.co.uk/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.exhibitiontrailer.co.uk/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.4" name="generator"/>
<!-- All in one Favicon 4.7 --> <link href="https://www.exhibitiontrailer.co.uk/wp-content/themes/exhibitiontrailer/slick/slick.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="outer-header">
<div class="header">
<div class="header-top">
<div class="logo"><a href="https://www.exhibitiontrailer.co.uk"><img src="https://www.exhibitiontrailer.co.uk/wp-content/themes/exhibitiontrailer/images/logo.png"/></a></div>
<div class="contactdetails">
<div><strong>t: </strong>01780 720435<strong>  |</strong>
<strong>e: </strong>info@exhibitiontrailer.co.uk</div>
</div>
</div>
<div id="navigation">
<div class="menu-toggle">≡ Menu</div>
<div class="menu-main-menu-latest-container"><ul class="nav-menu" id="menu-main-menu-latest"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-383" id="menu-item-383"><a href="https://www.exhibitiontrailer.co.uk/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-384" id="menu-item-384"><a href="https://www.exhibitiontrailer.co.uk/trailers/">Trailers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-386" id="menu-item-386"><a href="https://www.exhibitiontrailer.co.uk/van-conversions/">Van Conversions</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-400" id="menu-item-400"><a href="https://www.exhibitiontrailer.co.uk/veterinary/">Veterinary</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-387" id="menu-item-387"><a href="https://www.exhibitiontrailer.co.uk/services/">Services</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-396" id="menu-item-396"><a href="https://www.exhibitiontrailer.co.uk/services/refurbishment/">Refurbishment</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-397" id="menu-item-397"><a href="https://www.exhibitiontrailer.co.uk/services/for-hire/">For Hire</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-398" id="menu-item-398"><a href="https://www.exhibitiontrailer.co.uk/services/finance-solutions/">Finance Solutions</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-392" id="menu-item-392"><a href="https://www.exhibitiontrailer.co.uk/for-sale/">For Sale</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-393" id="menu-item-393"><a href="https://www.exhibitiontrailer.co.uk/about-us/">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-394" id="menu-item-394"><a href="https://www.exhibitiontrailer.co.uk/contact/">Contact Us</a></li>
</ul></div>
</div><!-- navigation END -->
</div><!-- header END -->
</div><!-- outer-header END -->
<div class="slider">
</div>
    
    
    


		
		
	

			404.php

		



		<!-- #content -->
<!-- #wrapper -->
<footer class="page-footer">
<div class="footer">
<div class="copyright">
</div> <div class="copyright">Versatile Copyright 2019 © Exhibition Trailers. All Rights Reserved. 			<div class="footernav">
</div>
</div>
<!-- end article footer -->
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.exhibitiontrailer.co.uk\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://www.exhibitiontrailer.co.uk/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.9" type="text/javascript"></script>
<script src="https://www.exhibitiontrailer.co.uk/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
<script src="//code.jquery.com/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="https://www.exhibitiontrailer.co.uk/wp-content/themes/exhibitiontrailer/slick/slick.min.js" type="text/javascript"></script>
<script>
	jQuery(document).ready(function ($) {
    width = $(window).width();
		$(document).ready(function(){

			$(".menu-toggle").click(function() {
				$(".nav-menu").slideToggle();
			});

			// handle the mutiple hover styles in the menu
			$(".nav-sub-menu li").mouseover(function() {
				$(this).parentsUntil(".nav-menu").next("li").prev("li").css("background-color", "#b2bb1e");
				var $nav = $(this).parentsUntil(".nav-menu").next("li").prev("li");
				$("a.active", $nav).css("color", "#FFFFFF");
				$(this).css("background-color", "#b2bb1e");
			});

			$(".nav-sub-menu li").mouseout(function() {
				$(this).parentsUntil(".nav-menu").next("li").prev("li").css("background-color", "#002e61");
				$(this).css("background-color", "#ffffff");
				var $nav = $(this).parentsUntil(".nav-menu").next("li").prev("li");
				$("a.active", $nav).css("color", "#b2bb1e");
			});

			$(".nav-menu li a.active").mouseover(function() {
				$(this).css("color", "#FFFFFF");
			});

			$(".nav-menu li a.active").mouseout(function() {
				$(this).css("color", "#b2bb1e");
			});

			$('.acf-map').each(function(){
				render_map( $(this) );
			});

			$('.hero-slider').slick({
				autoplay: true,
				infinite: true,
				speed: 500,
				fade: true,
				cssEase: 'linear',
				prevArrow: '<button type="button" class="slick-prev">&lsaquo;</button>',
				nextArrow: '<button type="button" class="slick-next">&rsaquo;</button>',
			});

			$('.logo-slider').slick({
				slidesToShow: 4,
				slidesToScroll: 1,
				autoplay: true,
				infinite: true,
				speed: 500,
				responsive: [
						{
							breakpoint: 1000,
							settings: {
								slidesToShow: 4,
							}
						},
						{
							breakpoint: 700,
							settings: {
								slidesToShow: 3,
							}
						},
						{
							breakpoint: 480,
							settings: {
								slidesToShow: 2,
							}
						}
					],
				prevArrow: '<button type="button" class="slick-prev">&lsaquo;</button>',
				nextArrow: '<button type="button" class="slick-next">&rsaquo;</button>',
			});

	});

});

</script>
</div></footer></body>
</html>
