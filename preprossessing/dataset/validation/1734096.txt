<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<style type="text/css">
			body{
				background-color:#ffffff;
				font:normal 11px Verdana, Sans-Serif;
				color:#000000;
				
				margin:0px;
			}
			
			img {
				border-width:0px;
			}
			
			.PHeader{
				background-color:#C8CFD8;
				color:#000000;
				padding:3px;
			}
			.PSubHeader{
				background-color:#A4A9B0;
				color:#000000;
				padding:3px;
				font:bold 11px Verdana, Sans-Serif;
				text-align:center;
			}
			
			a:link, a:active, a:visited{
				background-color:transparent;
				text-decoration:underline;
				color:#203040;
			}
			a:hover{
				background-color:#d0d0d0;
				text-decoration:underline;
				color:#506070;
			}
			
			.MMenu{
				width:200px;
				padding:10px;
				padding-top:0px;
				padding-right:0px;
				vertical-align:top;
			}
			.MBody{
				padding:10px;
				padding-top:0px;
				vertical-align:top;
			}
			
			.TBox{
				background-color:#C8CFD8;
				border:solid 1px #404040;
				color:#000000;
			}
			.TBoxHead{
				background-color:#A4A9B0;
				color:#000000;
				font:bold 14px Arial, Sans-Serif;
				padding:0px;
				padding-left:4px;
				background-image: url( './images/corner.gif' );
				background-position:top right;
				background-repeat: no-repeat;
				height:25px;
			}
			.TBoxSubHead{
				background-color:#A4A9B0;
				color:#000000;
				font:bold 14px Arial, Sans-Serif;
			}
			.TBoxText{
				background-color:#C8CFD8;
				color:#000000;
				font:normal 13px Verdana, Sans-Serif;
				padding:5px;
			}
			.TBoxFoot{
				background-color:#A4A9B0;
				color:#000000;
				font:bold 12px Verdana, Sans-Serif;
				padding:4px;
			}
			
			.FListing{
				background-color:#000000;
				color:#ffffff;
			}
			.FListing td{
				background-color:#e0e0e0;
				color:#000000;
				width: 30%;
				text-align:center;
			}
			.FListing td.FLabel{
				background-color:#000000;
				color:#ffffff;
				font:bold 11px Verdana, Sans-Serif;
				width: 10%;
				text-align:left;
			}
			.FListing th{
				background-color:#405060;
				color:#ffffff;
				text-align:center;
				
			}
			
			.small{
				font-size:11px;
			}
			.big{
				font-size:16px;
			}
			
			p{
				text-indent: 12px;
			}
			
			input, textarea, select {
				background-color:#ffffff;
				color:#000000;
				border:solid 1px #000000;
				font:normal 12px Verdana, Sans-Serif;
				padding:3px;
			}
			
			
		</style>
<title>ZSNES Home Page - About ZSNES</title>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td class="PHeader">
<script async="" data-ad-client="ca-pub-7645045873107134" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</td>
<td class="PHeader">
<img alt="ZSNES" src="./images/logo.gif"/>
<br/> SNES games emulator 
				</td>
<td class="PHeader" style="text-align:right;padding:14px">
</td>
</tr>
<tr>
<td class="PSubHeader" colspan="3">
					[<a href="./index.php?page=about">About</a>]
					[<a href="./index.php?page=news">News</a>]
					[<a href="./index.php?page=files">Files</a>]
					[<a href="./index.php?page=features">Features</a>]
					[<a href="./index.php?page=discussion">Forums</a>]
					[<a href="./index.php?page=rominfo">Compatibility</a>]
					[<a href="./index.php?page=links">Links</a>]
					[<a href="./index.php?page=thankyou">Thanks</a>]
					[<a href="./index.php?page=donate">Donate</a>]
					[<a href="http://sourceforge.net">SourceForge</a>]
					
					
					
</td>
</tr>
<tr>
<td class="PSubHeader" colspan="3">
<script async="" data-ad-client="ca-pub-7645045873107134" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<br/>
</td>
</tr>
</table>
<p></p>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td class="MMenu">
<table border="0" cellpadding="2" cellspacing="1" class="TBox" width="100%">
<tr>
<td class="TBoxHead">
								ZSNES Blip
							</td>
</tr>
<tr>
<td class="TBoxText">
<b>Latest Version:</b> 1.51 - LATEST VERSION
								<br/>
<a href="http://www.fosshub.com/ZSNES.html">Win</a> |
								<a href="http://www.fosshub.com/ZSNES.html">DOS</a> |
								<a href="http://www.fosshub.com/ZSNES.html">Linux</a>
<br/>
<b>Last News Update:</b> <a href="./index.php?page=news">January 25, 2007</a>
<p class="small">
									The feed URL may be copied from 
									<a href="./feed.php">this link</a>. For information
									about parsing the feed data go <a href="./index.php?page=feed">here</a>.
								</p>
</td>
</tr>
</table>
<br/>
<table border="0" cellpadding="2" cellspacing="1" class="TBox" width="100%">
<tr>
<td class="TBoxHead">
								People
							</td>
</tr>
<tr>
<td class="TBoxText">
<b> Main Coders</b>
<ul>
<li>zsKnight</li>
<li>_Demo_</li>
<li>pagefault</li>
<li>Nach</li>
<li>grinvader</li>
</ul>
<b> Contributors </b>
<ul>
<li>Deathlike2</li>
<li>Jonas Quinn</li>
<li>blargg</li>
<li>Pharos</li>
<li>teuf</li>
<li>relnev</li>
<li>prometheus</li>
<li>theoddone33</li>
<li>EvilTypeGuy</li>
<li>stainless</li>
<li>aaronl</li>
<li>MKendora</li>
<li>kode54</li>
<li>zinx</li>
<li>amit</li>
<li>Khan Artist</li>
<li>hpsolo</li>
<li>Kreed</li>
<li>Neviksti</li>
</ul>
</td>
</tr>
</table>
<br/>
<table border="0" cellpadding="2" cellspacing="1" class="TBox" width="100%">
<tr>
<td class="TBoxHead">
								Useless Information
							</td>
</tr>
<tr>
<td class="TBoxText"><span class="small">
<br/><br/>
									 Site scripting and design (2003-Present) by <a href="http://www.xehas.org">Radio</a>
</span>
</td>
</tr>
</table>
</td>
<td class="MBody">
<table border="0" cellpadding="2" cellspacing="1" class="TBox" width="100%">
<tr>
<td class="TBoxHead">
								Welcome the ZSNES Home Page
							</td>
</tr>
<tr>
<td class="TBoxText">
<p>
									ZSNES is a Super Nintendo emulator programmed by zsKnight and _Demo_. 
									On April 2, 2001 the ZSNES project was GPL'ed and its source released 
									to the public. It currently runs on Windows, Linux, FreeBSD, and DOS.
									Remember that this is a public beta so don't expect this to run on your
									machine.
								</p>
<div style="text-align:center">
<table border="0" cellpadding="2" cellspacing="1" width="100%">
<tr>
<td>
<img alt="" src="./images/screenshot1.png"/> <br/>Earthbound<br/>
</td>
<td>
<img alt="" src="./images/screenshot2.png"/> <br/> Megaman X3<br/>
</td>
<td>
<img alt="" src="./images/screenshot3.png"/> <br/> Final Fantasy 4 <br/>
</td>
<td>
<img alt="" src="./images/screenshot4.png"/> <br/> Super Mario Kart<br/>
</td>
</tr>
</table>
<table border="0" cellpadding="2" cellspacing="1" width="100%">
<tr>
<td>
<img alt="" src="./images/screenshot5.png"/> <br/> Castlevania <br/>
<img alt="" src="./images/screenshot6.png"/> <br/> Star Ocean <br/>
</td>
<td>
<img alt="" src="./images/screenshot7.png"/> <br/>  Street Fighter 2 <br/>
<img alt="" src="./images/screenshot8.png"/><br/> Tales of Phantasia <br/>
</td>
</tr>
</table>
</div>
</td>
</tr>
</table>
<br/>
<table border="0" cellpadding="2" cellspacing="1" class="TBox" width="100%">
<tr>
<td class="TBoxHead">
								Legal
							</td>
</tr>
<tr>
<td class="TBoxText">
<p>
									This program is distributed in the hope that it will useful, but WITHOUT
									ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
									FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
									more details.
								</p>
<p>
									The ZSNES Team is not connected or affiliated with any mentioned company
									in any way. Companies and all products pertaining to that company are 
									trademarks of that company. Please contact that company for trademark and
									copyright information.
								</p>
</td>
</tr>
</table>
<br/>
<div style="text-align:center">
<script type="text/javascript">
							if (typeof topbar_banner_0_ad == "function") { topbar_banner_0_ad(); }
						</script>
</div>
</td>
</tr>
</table>
</body>
</html>
