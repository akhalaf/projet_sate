<!DOCTYPE html>
<html dir="ltr" lang="es-es">
<head>
<base href="https://www.electronicacardenas.cl/imagenes/filewords/"/> <link href="https://www.electronicacardenas.cl/templates/ferrecar/images/designer/4380512a16c7f29d761bd74a9f0fc904_favicon.ico" rel="icon" type="image/x-icon"/>
<script>
    var themeHasJQuery = !!window.jQuery;
</script>
<script src="/templates/ferrecar/jquery.js?version=1.0.481"></script>
<script>
    window._$ = jQuery.noConflict(themeHasJQuery);
</script>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<script src="/templates/ferrecar/bootstrap.min.js?version=1.0.481"></script>
<!--[if lte IE 9]>
<script src="/templates/ferrecar/layout.ie.js?version=1.0.481"></script>
<link rel="stylesheet" href="/templates/ferrecar/layout.ie.css?version=1.0.481" media="screen"/>
<![endif]-->
<link class="" href="//fonts.googleapis.com/css?family=Quicksand:300,regular,700|Raleway:100,200,300,regular,500,600,700,800,900&amp;subset=latin" rel="stylesheet" type="text/css"/>
<script src="/templates/ferrecar/layout.core.js?version=1.0.481"></script>
<script src="/templates/ferrecar/CloudZoom.js?version=1.0.481" type="text/javascript"></script>
<link href="/templates/ferrecar/css/bootstrap.min.css?version=1.0.481" media="screen" rel="stylesheet"/>
<link href="/templates/ferrecar/css/template.min.css?version=1.0.481" media="screen" rel="stylesheet"/>
<script src="/templates/ferrecar/script.js?version=1.0.481"></script>
</head>
<body class=" bootstrap bd-body-9 bd-pagebackground bd-margins">
<div class=" bd-stretchtobottom-5 bd-stretch-to-bottom" data-control-selector=".bd-sheet-9"><div class="bd-sheet-9 bd-sheetstyles-8 ">
<div class="bd-container-inner">
<div class=" bd-flexalign-2 bd-no-margins bd-flexalign">
<div class=" bd-text404-41 bd-tagstyles shape-only">
404 - Categoría no encontrada</div></div>
</div>
</div></div>
</body>
</html>