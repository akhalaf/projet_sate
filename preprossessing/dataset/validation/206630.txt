<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta content="Mike Campbell" name="author"/>
<meta content="The meaning, origin and history of the given name Nils" name="description"/>
<meta content="meaning, baby names, list, dictionary, encyclopedia, etymology, name, etymologies, origin, meanings, origins, onomastics, boy names, girl names, reference" name="keywords"/>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width" id="viewport" name="viewport"/>
<title>Meaning, origin and history of the name Nils - Behind the Name</title>
<link href="/style.php?v=302" rel="stylesheet" type="text/css"/>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="/favicon.ico" rel="icon" type="image/vnd.microsoft.icon"/>
<link href="/images/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/images/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/images/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://www.behindthename.com/name/nils" rel="canonical"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/btn.js?version=63" type="text/javascript"></script>
<meta content="Meaning, origin and history of the name Nils" property="og:title"/>
<meta content="name" property="og:type"/>
<meta content="https://www.behindthename.com/images/btn_sm.png" property="og:image"/>
<meta content="Behind the Name" property="og:site_name"/>
<meta content="171755776192813" property="fb:admins"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-6043105-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-6043105-1');
</script>
<script src="https://tagan.adlightning.com/math-aids/op.js"></script>
<script src="https://qd.admetricspro.com/js/behindthename/layout-728.js"></script>
<script src="//www.googletagservices.com/tag/js/gpt.js"></script>
<script src="https://qd.admetricspro.com/js/behindthename/cmp.js"></script>
<script src="//cdn.districtm.ca/merge/merge.169485.js"></script>
<script src="https://qd.admetricspro.com/js/behindthename/prebid.js"></script>
<script src="https://qd.admetricspro.com/js/behindthename/engine.js"></script>
</head>
<body>
<nav>
<div class="menubar-wrapper">
<div class="menubar">
<table class="menubar-table"><tr>
<td id="menucell1">
<a href="/"><img alt="Behind the Name - the etymology and history of first names" id="nav_logo" src="/images/btn_header.png"/></a>
<a href="/"><img alt="Behind the Name - the etymology and history of first names" id="nav_logo_small" src="/images/btn_header_small.png"/></a>
</td>
<td id="menucell2">
<form action="/names/search.php" method="get" style="padding:0px;">
<div class="menusearch">
<table cellpadding="0" cellspacing="0" style="margin:0px;"><tr>
<td><input autocapitalize="off" autocomplete="off" autocorrect="off" id="nav_search" name="terms" spellcheck="false" type="text"/></td>
<td><button id="nav_search_button" type="submit"><img alt="Search" src="/images/search_header.png"/></button></td>
</tr></table>
</div>
</form>
<script type="text/javascript">
$(function() {
	set_search_autocomplete("#nav_search");
});
</script>
</td>
<td id="menucell3">
<ul class="btnmenu">
<li><a href="" onclick="return false;">Names</a>
<ul class="btnsubmenu">
<li><a href="/info/names">Introduction</a></li>
<li><a href="/names/list">Browse Names</a></li>
<li><a href="/names/search">Advanced Search</a></li>
<li><a href="/top/">Popularity</a></li> <li><a href="/namesakes/">Namesakes</a></li>
<li><a href="/namedays/">Name Days</a></li> <li><a href="/submit/">Submitted Names</a></li>
</ul>
</li>
<li><a href="" onclick="return false;">Interact</a>
<ul class="btnsubmenu">
<li><a href="/bb/">Message Boards</a></li>
<li><a href="/polls/">Polls</a></li> <li><a href="/derby/">Predict Rankings</a></li> <li><a href="/submit/submit">Submit a Name</a></li>
</ul>
</li>
<li><a href="" onclick="return false;">Tools</a>
<ul class="btnsubmenu">
<li><a href="/random/">Random Renamer</a></li> <li><a href="/names/translate">Name Translator</a></li> <li><a href="/names/themes">Name Themes</a></li> <li><a href="/anagram/">Anagrams</a></li> <li><a href="/guide/">Baby Name Expert</a></li> <li><a href="https://surnames.behindthename.com">Surname Site</a></li> <li><a href="https://places.behindthename.com">Place Name Site</a></li> </ul>
</li>
</ul>
</td>
<td id="menucell0">
<div id="mobileopts">
<div id="mobileoptsheading">
<img alt="Menu" src="/images/menu3.png" style="height:18px"/>
<span style="display:block;font-size:12px;line-height:12px;margin-top:6px;">Menu</span>
</div>
<div id="mobilemenu">
<ul id="mobilemenu-user">
<li><a href="https://www.behindthename.com/members/login">Sign In</a></li>
<li><a href="https://www.behindthename.com/members/signup">Register</a></li>
<li><hr/></li>
<li><a href="https://surnames.behindthename.com">Surname Site</a></li> <li><a href="https://places.behindthename.com">Place Name Site</a></li></ul>
<ul id="mobilemenu-main">
<li><a href="/info/names">Introduction</a></li>
<li><a href="/names/list">Browse Names</a></li>
<li><a href="/names/search">Advanced Search</a></li>
<li><a href="/top/">Popularity</a></li> <li><a href="/namesakes/">Namesakes</a></li>
<li><a href="/namedays/">Name Days</a></li> <li><a href="/submit/">Submitted Names</a></li>
<li><a href="/bb/">Message Boards</a></li>
<li><a href="/polls/">Polls</a></li> <li><a href="/derby/">Predict Rankings</a></li> <li><a href="/submit/submit">Submit a Name</a></li>
<li><a href="/random/">Random Renamer</a></li> <li><a href="/names/translate">Name Translator</a></li> <li><a href="/names/themes">Name Themes</a></li> <li><a href="/anagram/">Anagrams</a></li> <li><a href="/guide/">Baby Name Expert</a></li></ul>
</div>
</div>
</td>
<td id="menucell4">
<div style="padding:0px 10px">
<a href="https://www.behindthename.com/members/login">Sign In</a>  
<a href="https://www.behindthename.com/members/signup">Register</a>
</div>
</td>
</tr></table>
</div>
</div>
</nav>
<div style="text-align:center">
<div class="bannerad">
<!-- /192633929/behindthename-728x90-ATF -->
<div id="div-gpt-ad-1581477966538-0">
<script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1581477966538-0'); });
  </script>
</div>
</div>
</div>
<div class="body-wrapper">
<div class="body">
<div class="body-inner">
<div class="namebanner">
<div style="height:70px;background:#F83A45;background:-moz-linear-gradient(-45deg, #F83A45 24%, #451BBD 81.6%);
background:-webkit-gradient(linear, left top, right bottom, color-stop(24%,#F83A45), color-stop(81.6%,#451BBD));
background:-webkit-linear-gradient(-45deg, #F83A45 24%, #451BBD 81.6%);
background:-o-linear-gradient(-45deg, #F83A45 24%, #451BBD 81.6%);
background:-ms-linear-gradient(-45deg, #F83A45 24%, #451BBD 81.6%);
background:linear-gradient(135deg, #F83A45 24%, #451BBD 81.6%);
">
<div style="height:60px;padding-top:10px;background:#F83A45;background:-moz-linear-gradient(top, rgba(3,3,3,0) 10%, rgba(3,3,3,0.4) 100%);
background:-webkit-gradient(linear, left top, left bottom, color-stop(10%,rgba(3,3,3,0)), color-stop(100%,rgba(3,3,3,0.4)));
background:-webkit-linear-gradient(top, rgba(3,3,3,0) 10%, rgba(3,3,3,0.4) 100%);
background:-o-linear-gradient(top, rgba(3,3,3,0) 10%, rgba(3,3,3,0.4) 100%);
background:-ms-linear-gradient(top, rgba(3,3,3,0) 10%, rgba(3,3,3,0.4) 100%);
background:linear-gradient(to bottom, rgba(3,3,3,0) 10%, rgba(3,3,3,0.4) 100%);
">
<h1 class="namebanner-title" style="font-size:36px;text-shadow: 4px 4px 8px #451BBD, -4px 4px 8px #451BBD, 4px -4px 8px #451BBD, -4px -4px 8px #451BBD;">Nils</h1>
</div>
</div>
<nav class="name-tabbar"><a class="nametab nametab_active" href="/name/nils" style="max-width:150px;min-width:72px;width:14.3%;">Name</a><a class="nametab nametab_inactive" href="/name/nils/top" style="max-width:150px;min-width:72px;width:14.3%;">Popularity</a><a class="nametab nametab_inactive nametab_long" href="/name/nils/related" style="max-width:150px;min-width:72px;width:14.3%;">Related Names</a><a class="nametab nametab_inactive nametab_short" href="/name/nils/related" style="max-width:150px;min-width:72px;width:14.3%;">Related</a><a class="nametab nametab_inactive" href="/name/nils/rating" style="max-width:150px;min-width:72px;width:14.3%;">Ratings</a><a class="nametab nametab_inactive" href="/name/nils/comments" style="max-width:150px;min-width:72px;width:14.3%;">Comments</a><a class="nametab nametab_inactive" href="/name/nils/namesakes" style="max-width:150px;min-width:72px;width:14.3%;">Namesakes</a><a class="nametab nametab_inactive" href="/name/nils/namedays" style="max-width:150px;min-width:72px;width:14.3%;">Name Days</a><div class="nametab nametab_inactive" style="width:100%"></div></nav>
</div>
<div class="info_message"></div>
<div>
<article>
<div class="nametools"><div class="ratebutton" id="ratebutton_nils"><div class="ratenumber" onclick="window.location.href='/name/nils/rating'">77%<span>Rating</span></div></div><div class="pnlbutton" id="pnlbutton_nils"><div class="pnladd" onclick="return add2pnl('nils', false, 0, 'sytd9ha2sc');"><img alt="add to your list" src="/images/name/star-black.png"/><span>Save</span></div></div></div>
<div class="infogroup" style="padding-top:10px">
<div class="infoname"><span class="infoname-title">Gender </span><span class="infoname-info"><span class="masc">Masculine</span></span></div>
<div class="infoname"><span class="infoname-title" title="Usage (where this name is used)">Usage </span><span class="infoname-info"><a class="usg" href="/names/usage/swedish">Swedish</a>, <a class="usg" href="/names/usage/norwegian">Norwegian</a>, <a class="usg" href="/names/usage/danish">Danish</a></span></div>
<div class="infoname"><span class="infoname-title infoname-title-full">Pronounced </span><span class="infoname-title infoname-title-short" title="Pronunciation">Pron. </span><span class="infoname-info" id="infoname-info-pron"><span class="infoname-unit"><a class="prn" href="#" onclick="return show_prondetail('NILS', 1, false, false);">NILS</a><div class="pron-detail" id="pron-detail-1"></div></span>  [<a class="text-function" href="/info/pronunciation">key</a><span class="muted-sep muted-sep-mini">·</span><a class="text-function" href="#" onclick="return toggle_pron('nils', true, 'infoname-info-pron');">IPA</a>]</span></div>
</div>
<section>
<div class="namemain"><nav class="headingright"><div><a class="text-function" href="#" onclick="return expand_name_links('nils', 'expanded_links');">Expand Links</a></div></nav><h2>Meaning &amp; History</h2></div>
<div>
Scandinavian form of <a class="nl" href="/name/nicholas">NICHOLAS</a>.<div id="expanded_links" style="display:none;margin-top:8px;"></div>
</div>
</section>
<section>
<div class="namemain"><nav class="headingright"><div><a class="text-function" href="/name/nils/tree">Family Tree</a><span class="muted-sep">·</span><a class="text-function" href="/name/nils/related">Details</a></div></nav><h2>Related Names</h2></div>
<div class="infogroup">
<div class="inforel"><span class="inforel-title">Variants</span><span class="inforel-info"><a class="ngl" href="/name/claes">Claes</a>, <a class="ngl" href="/name/klas">Klas</a><span class="infoname-usage">(<span>Swedish</span>)</span> <a class="ngl" href="/name/nels">Nels</a><span class="infoname-usage">(<span>Danish</span>)</span> </span></div><div class="inforel"><span class="inforel-title">Other Languages &amp; Cultures</span><span class="inforel-info"><a class="ngl" href="/name/nikolle12">Nikollë</a><span class="infoname-usage">(<span>Albanian</span>)</span> <a class="ngl" href="/name/nicolaus">Nicolaus</a>, <a class="ngl" href="/name/nikolaos">Nikolaos</a><span class="infoname-usage">(<span>Ancient Greek</span>)</span> <a class="ngl" href="/name/nikola-1">Nikola</a><span class="infoname-usage">(<span>Basque</span>)</span> <a class="ngl" href="/name/mikalai">Mikalai</a>, <a class="ngl" href="/name/mikalay">Mikalay</a><span class="infoname-usage">(<span>Belarusian</span>)</span> <a class="ngl" href="/name/nikola-1">Nikola</a>, <a class="ngl" href="/name/nikolai">Nikolai</a>, <a class="ngl" href="/name/nikolay">Nikolay</a><span class="infoname-usage">(<span>Bulgarian</span>)</span> <a class="ngl" href="/name/nicolau">Nicolau</a><span class="infoname-usage">(<span>Catalan</span>)</span> <a class="ngl" href="/name/nikola-1">Nikola</a>, <a class="ngl" href="/name/nikica">Nikica</a>, <a class="ngl" href="/name/niko">Niko</a>, <a class="ngl" href="/name/niks18a">Nikša</a><span class="infoname-usage">(<span>Croatian</span>)</span> <a class="ngl" href="/name/mikula10s18">Mikuláš</a>, <a class="ngl" href="/name/mikola10s18">Mikoláš</a>, <a class="ngl" href="/name/mikula">Mikula</a>, <a class="ngl" href="/name/nikola-1">Nikola</a><span class="infoname-usage">(<span>Czech</span>)</span> <a class="ngl" href="/name/nicolaas">Nicolaas</a>, <a class="ngl" href="/name/nikolaas">Nikolaas</a>, <a class="ngl" href="/name/kai-1">Kai</a>, <a class="ngl" href="/name/klaas">Klaas</a>, <a class="ngl" href="/name/nick">Nick</a>, <a class="ngl" href="/name/nico">Nico</a>, <a class="ngl" href="/name/niek">Niek</a><span class="infoname-usage">(<span>Dutch</span>)</span> <a class="ngl" href="/name/nicholas">Nicholas</a>, <a class="ngl" href="/name/colin-2">Colin</a>, <a class="ngl" href="/name/collin">Collin</a>, <a class="ngl" href="/name/collyn">Collyn</a>, <a class="ngl" href="/name/nic">Nic</a>, <a class="ngl" href="/name/nick">Nick</a>, <a class="ngl" href="/name/nickolas">Nickolas</a>, <a class="ngl" href="/name/nicky">Nicky</a>, <a class="ngl" href="/name/nik">Nik</a>, <a class="ngl" href="/name/nikolas">Nikolas</a><span class="infoname-usage">(<span>English</span>)</span> <a class="ngl" href="/name/nikolao">Nikolao</a>, <a class="ngl" href="/name/nic13jo">Niĉjo</a><span class="infoname-usage">(<span>Esperanto</span>)</span> <a class="ngl" href="/name/nigul">Nigul</a><span class="infoname-usage">(<span>Estonian</span>)</span> <a class="ngl" href="/name/niklas">Niklas</a>, <a class="ngl" href="/name/kai-1">Kai</a>, <a class="ngl" href="/name/klaus">Klaus</a>, <a class="ngl" href="/name/launo">Launo</a>, <a class="ngl" href="/name/niclas">Niclas</a>, <a class="ngl" href="/name/niilo">Niilo</a>, <a class="ngl" href="/name/niko">Niko</a><span class="infoname-usage">(<span>Finnish</span>)</span> <a class="ngl" href="/name/nicolas">Nicolas</a><span class="infoname-usage">(<span>French</span>)</span> <a class="ngl" href="/name/kai-1">Kai</a>, <a class="ngl" href="/name/kay-3">Kay</a>, <a class="ngl" href="/name/klaes">Klaes</a><span class="infoname-usage">(<span>Frisian</span>)</span> <a class="ngl" href="/name/nicolau">Nicolau</a><span class="infoname-usage">(<span>Galician</span>)</span> <a class="ngl" href="/name/nikoloz">Nikoloz</a>, <a class="ngl" href="/name/nika-3">Nika</a>, <a class="ngl" href="/name/niko">Niko</a>, <a class="ngl" href="/name/nikusha">Nikusha</a><span class="infoname-usage">(<span>Georgian</span>)</span> <a class="ngl" href="/name/niklas">Niklas</a>, <a class="ngl" href="/name/nikolaus">Nikolaus</a>, <a class="ngl" href="/name/claus">Claus</a>, <a class="ngl" href="/name/kai-1">Kai</a>, <a class="ngl" href="/name/klaus">Klaus</a>, <a class="ngl" href="/name/nickolaus">Nickolaus</a>, <a class="ngl" href="/name/nico">Nico</a>, <a class="ngl" href="/name/nicolaus">Nicolaus</a>, <a class="ngl" href="/name/niko">Niko</a><span class="infoname-usage">(<span>German</span>)</span> <a class="ngl" href="/name/niklaus">Niklaus</a><span class="infoname-usage">(<span>German (Swiss)</span>)</span> <a class="ngl" href="/name/nicolaos">Nicolaos</a>, <a class="ngl" href="/name/nikolaos">Nikolaos</a>, <a class="ngl" href="/name/nik">Nik</a>, <a class="ngl" href="/name/nikolas">Nikolas</a>, <a class="ngl" href="/name/nikos">Nikos</a><span class="infoname-usage">(<span>Greek</span>)</span> <a class="ngl" href="/name/miklo10s">Miklós</a>, <a class="ngl" href="/name/nikolasz">Nikolasz</a>, <a class="ngl" href="/name/kolos">Kolos</a>, <a class="ngl" href="/name/miksa">Miksa</a><span class="infoname-usage">(<span>Hungarian</span>)</span> <a class="ngl" href="/name/niocla10s">Nioclás</a><span class="infoname-usage">(<span>Irish</span>)</span> <a class="ngl" href="/name/niccolo11">Niccolò</a>, <a class="ngl" href="/name/nicola-1">Nicola</a>, <a class="ngl" href="/name/nico">Nico</a>, <a class="ngl" href="/name/nicolao">Nicolao</a>, <a class="ngl" href="/name/nicolo11">Nicolò</a><span class="infoname-usage">(<span>Italian</span>)</span> <a class="ngl" href="/name/nikolajs">Nikolajs</a>, <a class="ngl" href="/name/kla23vs">Klāvs</a>, <a class="ngl" href="/name/nikla23vs">Niklāvs</a><span class="infoname-usage">(<span>Latvian</span>)</span> <a class="ngl" href="/name/klaos">Klaos</a><span class="infoname-usage">(<span>Limburgish</span>)</span> <a class="ngl" href="/name/klaas">Klaas</a><span class="infoname-usage">(<span>Low German</span>)</span> <a class="ngl" href="/name/nikola-1">Nikola</a>, <a class="ngl" href="/name/nikolc18e">Nikolče</a>, <a class="ngl" href="/name/nikolche">Nikolche</a><span class="infoname-usage">(<span>Macedonian</span>)</span> <a class="ngl" href="/name/nikora">Nikora</a><span class="infoname-usage">(<span>Maori</span>)</span> <a class="ngl" href="/name/nichol">Nichol</a>, <a class="ngl" href="/name/nicol-1">Nicol</a>, <a class="ngl" href="/name/col">Col</a><span class="infoname-usage">(<span>Medieval English</span>)</span> <a class="ngl" href="/name/mikol16aj">Mikołaj</a><span class="infoname-usage">(<span>Polish</span>)</span> <a class="ngl" href="/name/nicolau">Nicolau</a>, <a class="ngl" href="/name/nico">Nico</a><span class="infoname-usage">(<span>Portuguese</span>)</span> <a class="ngl" href="/name/nicolae">Nicolae</a>, <a class="ngl" href="/name/neculai">Neculai</a>, <a class="ngl" href="/name/nicu">Nicu</a>, <a class="ngl" href="/name/nicus25or">Nicușor</a><span class="infoname-usage">(<span>Romanian</span>)</span> <a class="ngl" href="/name/nikolai">Nikolai</a>, <a class="ngl" href="/name/nikolay">Nikolay</a>, <a class="ngl" href="/name/kolya">Kolya</a><span class="infoname-usage">(<span>Russian</span>)</span> <a class="ngl" href="/name/niillas">Niillas</a>, <a class="ngl" href="/name/nilas">Nilas</a><span class="infoname-usage">(<span>Sami</span>)</span> <a class="ngl" href="/name/neacel">Neacel</a>, <a class="ngl" href="/name/nichol">Nichol</a>, <a class="ngl" href="/name/nicol-1">Nicol</a><span class="infoname-usage">(<span>Scottish</span>)</span> <a class="ngl" href="/name/nikola-1">Nikola</a>, <a class="ngl" href="/name/nikica">Nikica</a><span class="infoname-usage">(<span>Serbian</span>)</span> <a class="ngl" href="/name/mikula10s18">Mikuláš</a><span class="infoname-usage">(<span>Slovak</span>)</span> <a class="ngl" href="/name/miklavz18">Miklavž</a>, <a class="ngl" href="/name/nikola-1">Nikola</a>, <a class="ngl" href="/name/nikolaj">Nikolaj</a>, <a class="ngl" href="/name/nik">Nik</a>, <a class="ngl" href="/name/niko">Niko</a><span class="infoname-usage">(<span>Slovene</span>)</span> <a class="ngl" href="/name/nicola10s">Nicolás</a>, <a class="ngl" href="/name/nico">Nico</a><span class="infoname-usage">(<span>Spanish</span>)</span> <a class="ngl" href="/name/mykola">Mykola</a><span class="infoname-usage">(<span>Ukrainian</span>)</span> </span></div><div class="inforel"><span class="inforel-title">Surname Descendants</span><span class="inforel-info"><a class="ngl" href="//surnames.behindthename.com/name/nilsen">Nilsen</a><span class="infoname-usage">(<span>Norwegian</span>)</span> <a class="ngl" href="//surnames.behindthename.com/name/nilsson">Nilsson</a><span class="infoname-usage">(<span>Swedish</span>)</span> </span></div><div class="inforel"><span class="inforel-title">User Submission</span><span class="inforel-info"><a class="ngl" href="/name/ni10ls/submitted">Níls</a></span></div></div>
</section>
<section>
<div class="namemain"><nav class="headingright"><div><a class="text-function" href="/name/nils/top">Details</a></div></nav><h2>Popularity</h2></div>
<div>
<div class="regionlist">
<div class="regionlink" id="regionlink_fr">
<a class="rl" href="/name/nils/top/france"><svg class="svggraph" id="svgthumb_fr" preserveaspectratio="none" style="overflow:visible;width:155px;height:58px;" title="Chart for nils" viewbox="0 0 155 58" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<title>Last ranked
#296 in 2019</title>
<image height="11" preserveaspectratio="xMinYMax meet" width="16" x="3" xlink:href="/images/topicons/fr.png" y="44"></image>
<text class="svgtitle" style="fill:#000000;font-size:13px;" text-anchor="end" x="152" y="55">France</text>
<line class="svggridline" style="stroke:rgb(200,200,200);stroke-width:1.3" x1="0" x2="155" y1="41" y2="41"></line>
<g id="svgthumb_fr-namepaths">
<g class="svgcurveline" id="svgthumb_fr-nils-m" style="fill:none;stroke:#0000ff;stroke-width:1.3;">
<path d=" M98.3403 39.4 L98.9916 39.4 L99.6429 39.4 M108.7605 38.8 L109.4118 38.6 L110.7143 38 L112.0168 37.2 L113.3193 36.8 L114.6218 37.2 L115.9244 36 L117.2269 36.2 L118.5294 36.6 L119.8319 36 L121.1345 35.6 L122.437 35 L123.7395 35.2 L125.042 34.6 L126.3445 35.2 L127.6471 32.2 L128.9496 34.6 L130.2521 32.6 L131.5546 32.2 L132.8571 30.6 L134.1597 31.6 L135.4622 29 L136.7647 25.6 L138.0672 25.4 L139.3697 25.8 L140.6723 27.4 L141.9748 28 L143.2773 24.8 L144.5798 29.8 L145.8824 26.8 L147.1849 28.4 L148.4874 29 L149.7899 31.2 L151.0924 30.2 L152.395 31 L153.6975 31.2 L155 30"></path>
</g>
</g>
</svg>
</a>
</div>
<div class="regionlink" id="regionlink_nl">
<a class="rl" href="/name/nils/top/netherlands"><svg class="svggraph" id="svgthumb_nl" preserveaspectratio="none" style="overflow:visible;width:155px;height:58px;" title="Chart for nils" viewbox="0 0 155 58" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<title>Last ranked
#445 in 2020</title>
<image height="11" preserveaspectratio="xMinYMax meet" width="16" x="3" xlink:href="/images/topicons/nl.png" y="44"></image>
<text class="svgtitle" style="fill:#000000;font-size:13px;" text-anchor="end" x="152" y="55">Netherlands</text>
<line class="svggridline" style="stroke:rgb(200,200,200);stroke-width:1.3" x1="0" x2="155" y1="41" y2="41"></line>
<g id="svgthumb_nl-namepaths">
<g class="svgcurveline" id="svgthumb_nl-nils-m" style="fill:none;stroke:#0000ff;stroke-width:1.3;">
<path d="M0 26.2 L12.9167 24.6 L25.8333 28 L38.75 29.8 L51.6667 29.2 L64.5833 29.8 L77.5 29.2 L90.4167 31.2 L103.3333 30.6 L116.25 31.4 L129.1667 31 L142.0833 33.8 L155 32.8"></path>
</g>
</g>
</svg>
</a>
</div>
<div class="regionlink" id="regionlink_no">
<a class="rl" href="/name/nils/top/norway"><svg class="svggraph" id="svgthumb_no" preserveaspectratio="none" style="overflow:visible;width:155px;height:58px;" title="Chart for nils" viewbox="0 0 155 58" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<title>Last ranked
#76 in 2019</title>
<image height="11" preserveaspectratio="xMinYMax meet" width="16" x="3" xlink:href="/images/topicons/no.png" y="44"></image>
<text class="svgtitle" style="fill:#000000;font-size:13px;" text-anchor="end" x="152" y="55">Norway</text>
<line class="svggridline" style="stroke:rgb(200,200,200);stroke-width:1.3" x1="0" x2="155" y1="41" y2="41"></line>
<g id="svgthumb_no-namepaths">
<g class="svgcurveline" id="svgthumb_no-nils-m" style="fill:none;stroke:#0000ff;stroke-width:1.3;">
<path d="M0 13.3333 L2.0946 12.75 L4.1892 7.6667 L6.2838 10.1667 L8.3784 13.8333 L10.473 11.8333 L12.5676 14.6667 L14.6622 13.1667 L16.7568 14.25 L18.8514 13.75 L20.9459 18.6667 L23.0405 17 L25.1351 17.1667 L27.2297 15.1667 L29.3243 16.3333 L31.4189 17.25 L33.5135 18.3333 L35.6081 14.6667 L37.7027 15.25 L39.7973 18.25 L41.8919 18.8333 L43.9865 17.25 L46.0811 18.1667 L48.1757 17.1667 L50.2703 19.9167 L52.3649 20 L54.4595 23.1667 L56.5541 21.3333 L58.6486 26.25 L60.7432 25.5 L62.8378 26.5833 L64.9324 25.0833 L67.027 28.5833 L69.1216 27.0833 L71.2162 26.3333 L73.3108 28.3333 L75.4054 29.3333 L77.5 27.8333 L79.5946 28.25 L81.6892 30.25 L83.7838 30 L85.8784 32.3333 L87.973 30.6667 L90.0676 30.8333 L92.1622 30.5833 L94.2568 31.5833 L96.3514 32.0833 L98.4459 33.9167 L100.5405 34.9167 L102.6351 33.9167 L103.6824 35.1667 M149.7635 34.75 L150.8108 34.1667 L151.8581 35 M153.9527 35 L155 33.25"></path>
</g>
</g>
</svg>
</a>
</div>
<div class="regionlink" id="regionlink_sw">
<a class="rl" href="/name/nils/top/sweden"><svg class="svggraph" id="svgthumb_sw" preserveaspectratio="none" style="overflow:visible;width:155px;height:58px;" title="Chart for nils" viewbox="0 0 155 58" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<title>Last ranked
#9 in 2019</title>
<image height="11" preserveaspectratio="xMinYMax meet" width="16" x="3" xlink:href="/images/topicons/sw.png" y="44"></image>
<text class="svgtitle" style="fill:#000000;font-size:13px;" text-anchor="end" x="152" y="55">Sweden</text>
<line class="svggridline" style="stroke:rgb(200,200,200);stroke-width:1.3" x1="0" x2="155" y1="41" y2="41"></line>
<g id="svgthumb_sw-namepaths">
<g class="svgcurveline" id="svgthumb_sw-nils-m" style="fill:none;stroke:#0000ff;stroke-width:1.3;">
<path d="M0 28.56 L7.381 25.72 L14.7619 24.08 L22.1429 23 L29.5238 21.72 L36.9048 20.4 L44.2857 21.36 L51.6667 18.88 L59.0476 18.4 L66.4286 15.96 L73.8095 15.32 L81.1905 13.84 L88.5714 12.24 L95.9524 7.44 L103.3333 8.88 L110.7143 7.6 L118.0952 6.2 L125.4762 5.36 L132.8571 4.64 L140.2381 8.32 L147.619 7.28 L155 4.8"></path>
</g>
</g>
</svg>
</a>
</div>
<div class="regionlink" id="regionlink_sz">
<a class="rl" href="/name/nils/top/switzerland"><svg class="svggraph" id="svgthumb_sz" preserveaspectratio="none" style="overflow:visible;width:155px;height:58px;" title="Chart for nils" viewbox="0 0 155 58" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<title>Last ranked
#67 in 2019</title>
<image height="11" preserveaspectratio="xMinYMax meet" width="16" x="3" xlink:href="/images/topicons/ch.png" y="44"></image>
<text class="svgtitle" style="fill:#000000;font-size:13px;" text-anchor="end" x="152" y="55">Switzerland</text>
<line class="svggridline" style="stroke:rgb(200,200,200);stroke-width:1.3" x1="0" x2="155" y1="41" y2="41"></line>
<g id="svgthumb_sz-namepaths">
<g class="svgcurveline" id="svgthumb_sz-nils-m" style="fill:none;stroke:#0000ff;stroke-width:1.3;">
<path d="M0 25.48 L7.381 21.64 L14.7619 21.96 L22.1429 15.4 L29.5238 18.44 L36.9048 15.24 L44.2857 12.84 L51.6667 9.32 L59.0476 5.8 L66.4286 8.68 L73.8095 11.24 L81.1905 13 L88.5714 11.72 L95.9524 10.44 L103.3333 13 L110.7143 19.4 L118.0952 18.12 L125.4762 19.4 L132.8571 17.64 L140.2381 20.84 L147.619 20.36 L155 23.4"></path>
</g>
</g>
</svg>
</a>
</div>
<div class="regionlink" id="regionlink_us">
<a class="rl" href="/name/nils/top/united-states"><svg class="svggraph" id="svgthumb_us" preserveaspectratio="none" style="overflow:visible;width:155px;height:58px;" title="Chart for nils" viewbox="0 0 155 58" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<title>Last ranked
#815 in 1888</title>
<image height="11" preserveaspectratio="xMinYMax meet" width="16" x="3" xlink:href="/images/topicons/us.png" y="44"></image>
<text class="svgtitle" style="fill:#000000;font-size:13px;" text-anchor="end" x="152" y="55">United States</text>
<line class="svggridline" style="stroke:rgb(200,200,200);stroke-width:1.3" x1="0" x2="155" y1="41" y2="41"></line>
<g id="svgthumb_us-namepaths">
<g class="svgcurveline" id="svgthumb_us-nils-m" style="fill:none;stroke:#0000ff;stroke-width:1.3;">
<path d=" M8.3633 31 L8.9209 31 L9.4784 33"></path>
</g>
</g>
</svg>
</a>
</div>
</div>
<div id="topblurb_graph" style="margin:10px 0px;"></div></div>
</section>
<section>
<div class="namemain"><nav class="headingright"><div><a class="text-function" href="/rating/rate.php?name=nils">Rate</a><span class="muted-sep">·</span><a class="text-function" href="/name/nils/rating">Details</a></div></nav><h2>People think this name is</h2></div>
<div style="text-align:justify">
<span style="font-size:65%;line-height:100%;">youthful</span>   <span style="font-size:100%;line-height:100%;">wholesome</span>   <span style="font-size:80%;line-height:100%;">strong</span>   <span style="font-size:100%;line-height:100%;">strange</span>   <span style="font-size:100%;line-height:100%;">simple</span>   <span style="font-size:100%;line-height:100%;">nerdy</span>  </div>
</section>
<section>
<div class="namemain"><nav class="headingright"><div><a class="text-function" href="/namedays/">Name Days?</a></div></nav><h2>Name Days</h2></div>
<div>
<div class="ndlist"><div class="ndinfo"><a class="ngl" href="/namedays/country/estonia">Estonia</a>: December 6</div><div class="ndinfo"><a class="ngl" href="/namedays/country/latvia">Latvia</a>: June 19</div><div class="ndinfo"><a class="ngl" href="/namedays/country/sweden">Sweden</a>: October 8</div></div></div>
</section>
<section>
<div class="namemain"><h2>Categories</h2></div>
<div>
<a class="ngl" href="/names/tag/athletes">athletes</a>, <a class="ngl" href="/names/tag/elder_scrolls_characters">Elder Scrolls characters</a>, <a class="ngl" href="/names/tag/fire_emblem_characters">Fire Emblem characters</a>, <a class="ngl" href="/names/tag/high_representatives_for_bosnia_and_herzegovina">High Representatives for Bosnia and Herzegovina</a>, <a class="ngl" href="/names/tag/the_sopranos_characters">The Sopranos characters</a>, <a class="ngl" href="/names/tag/top_10_in_sweden">top 10 in Sweden</a>, <a class="ngl" href="/names/tag/world_leaders">world leaders</a>, <a class="ngl" href="/names/tag/xeno_characters">Xeno characters</a></div>
</section>
<div style="padding-top:45px;text-align:right;"><span style="vertical-align:middle">Entry updated <a href="/names/history.php?name=nils"><time>January 22, 2019</time></a><time datetime="2007-02-12" pubdate=""></time> · </span><div class="social_block">
<a class="socic socic-facebook" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.behindthename.com%2Fname%2Fnils&amp;picture=https%3A%2F%2Fwww.behindthename.com%2Fimages%2Fbtn_sm.png&amp;title=Behind%20the%20Name%3A%20Meaning%2C+origin+and+history+of+the+name+Nils" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;"><img alt="Share on Facebook" src="/images/social/facebook.svg" style="width:24px;height:24px;"/></a><a class="socic socic-twitter" href="https://twitter.com/share?text=Behind%20the%20Name%3A%20Meaning%2C+origin+and+history+of+the+name+Nils&amp;url=https%3A%2F%2Fwww.behindthename.com%2Fname%2Fnils&amp;via=onomast" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;"><img alt="Share on Twitter" src="/images/social/twitter.svg" style="width:24px;height:24px;"/></a><a class="socic socic-pinterest" href="https://www.pinterest.com/pin/create/button/?url=https%3A%2F%2Fwww.behindthename.com%2Fname%2Fnils&amp;media=https%3A%2F%2Fwww.behindthename.com%2Fimages%2Fbtn_sm.png&amp;description=Behind%20the%20Name%3A%20Meaning%2C+origin+and+history+of+the+name+Nils" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;"><img src="/images/social/pinterest.svg" style="width:24px;height:24px;"/></a>
</div>
</div>
</article>
<div style="clear:both;height:0px;"></div>
</div>
</div>
<nav class="name-navbar name-navbar-many"><a class="nameicon nameicon-active" href="/name/nils"><img src="/images/tabs/name-white.png"/><span>Name</span></a><a class="nameicon" href="/name/nils/top"><img src="/images/tabs/top-white.png"/><span>Popularity</span></a><a class="nameicon" href="/name/nils/related"><img src="/images/tabs/related-white.png"/><span>Related</span></a><a class="nameicon" href="/name/nils/rating"><img src="/images/tabs/rating-white.png"/><span>Ratings</span></a><a class="nameicon" href="/name/nils/comments"><img src="/images/tabs/comments-white.png"/><span>Comments</span></a><a class="nameicon" href="/name/nils/namesakes"><img src="/images/tabs/namesakes-white.png"/><span>Namesakes</span></a><a class="nameicon" href="/name/nils/namedays"><img src="/images/tabs/namedays-white.png"/><span>Name Days</span></a></nav></div>
</div>
<footer>
<div class="footer">
<table style="width:100%">
<tr><td style="padding:3px" valign="top">
<nav>
<span class="tiny"><a href="/">Home</a>
» <a href="/names/">Names</a>
</span>
</nav>
</td><td align="right" style="padding:3px">
<nav>
<span class="tiny">
<a href="/info">About</a> ·
<a href="/info/help">Help</a> ·
<a href="/info/copyright">Copyright</a> ·
<a href="/info/terms">Terms</a> ·
<a href="/info/privacy">Privacy</a> ·
<a href="/info/contact">Contact</a>
</span>
</nav>
</td></tr>
</table>
</div>
</footer>
</body>
</html>
