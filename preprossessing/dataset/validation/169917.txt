<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta content="ru" http-equiv="Content-Language"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="no-cache" http-equiv="Pragma"/>
<meta content="0" http-equiv="Expires"/>
<meta content="no-cache" http-equiv="Cache-Control"/>
<script src="/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="/js/main.js" type="text/javascript"></script>
<meta content="width=device-width, initial-scale=1.0, minimum-scale=0.8, maximum-scale=1.0, user-scalable=yes" name="viewport"/>
<meta content="True" name="HandheldFriendly"/>
<title>AudioTracker.org</title>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/style.css?1" rel="stylesheet" type="text/css"/>
</head>
<body>
<center>
<div class="main">
<div class="logo">
<div style="overflow: hidden;"><div class="login"><a class="off" href="/login.php" rel="nofollow">Войти</a></div><div class="logotip"><a href="/"><img class="logo" src="/img/logo.png"/></a></div>
</div><div class="menu">
<a href="/">Новое</a>
<a href="/authors/">Авторы книг</a>
<a href="/category/">Категории</a>
<a href="/help/" style="color: #666; margin-left: 5px; font: 13px Tahoma;">Как тут качать?</a>
<div style="float: right;"><img class="ico16" src="/img/find.gif"/><a href="javascript:HideShowDiv('search-box','')">Найти</a>
</div></div>
</div>
<div class="fly-menu" id="fly-menu" style="display: none;"><div><a href="/">Новинки</a></div><div><a href="/authors/">Авторы</a></div><div style="border-bottom-width: 0;"><a href="/category/">Категории</a></div></div><div class="main-body">
<div class="box-admin" id="search-box" style="display: none;">
<form action="/search.php" method="get">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td style="padding: 0 5px 0 0;"><nobr><b>Поиск:</b></nobr></td><td width="100%"><input id="search-box-text" name="words" style="width: 99%;" type="text" value=""/></td><td style="padding: 0 0 0 5px;"><input type="submit" value="Искать"/></td></tr></table></form>
</div>
<h1>Новые аудиозаписи</h1><div class="video-list">
<div class="foto"><a href="/audio/5549/"><img src="//www.koob.ru/foto/author/9366.jpg"/></a></div>
<div class="text">
<h2><a href="/audio/5549/">Избранная проза</a></h2>
<p class="avtor"><a href="/lomonosov/">Ломоносов М.В.</a></p>
<p class="avtor">Категория: <a href="/category/classic/">Русская классика</a></p>
<p class="text">Михаил Васильевич Ломоносов "Избранная проза". Высокое искусство слова великого поэта нашло отражение в прозаических произведениях, среди которых и научные труды ("Слова"), и письма, и служебные документы (рапорты, доношения, представления).</p><p>
</p></div>
</div>
<div class="video-list">
<div class="foto"><a href="/audio/5548/"><img src="//www.koob.ru/foto/author/9366.jpg"/></a></div>
<div class="text">
<h2><a href="/audio/5548/">М. В. Ломоносов. Стихи</a></h2>
<p class="avtor"><a href="/lomonosov/">Ломоносов М.В.</a></p>
<p class="avtor">Категория: <a href="/category/classic/">Русская классика</a></p>
<p class="text">Михаил Васильевич Ломоносов - мирового значения, энциклопедист, химик и физик. Стихи.</p><p>
</p></div>
</div>
<div class="video-list">
<div class="foto"><a href="/audio/5547/"><img src="//www.koob.ru/foto/author/9791.jpg"/></a></div>
<div class="text">
<h2><a href="/audio/5547/">Жестокая лирика (Глеб Успенский) - 14 альбомов</a></h2>
<p class="avtor"><a href="/uspensky/">Успенский Г.И.</a></p>
<p class="avtor">Категория: <a href="/category/classic/">Русская классика</a></p>
<p class="text">Глеб Успенский (литературный псевдоним Голя Монголин). В сборнике представлены 14 альбомов.</p><p>
</p></div>
</div>
<div class="video-list">
<div class="foto"><a href="/audio/5546/"><img src="//www.koob.ru/foto/author/9791.jpg"/></a></div>
<div class="text">
<h2><a href="/audio/5546/">Парамон юродивый</a></h2>
<p class="avtor"><a href="/uspensky/">Успенский Г.И.</a></p>
<p class="avtor">Категория: <a href="/category/classic/">Русская классика</a></p>
<p class="text">Глеб Иванович Успенский "Парамон юродивый". "Он так глубоко верил в будущее блаженство, так глубоко был проникнут сознанием того, что выше этой «вечной славы» ничего нет ни в жизни человека, ни на земле, ни под землей...".</p><p>
</p></div>
</div>
<div class="video-list">
<div class="foto"><a href="/audio/5545/"><img src="//www.koob.ru/foto/author/9791.jpg"/></a></div>
<div class="text">
<h2><a href="/audio/5545/">Нравы Растеряевой улицы. Очерки</a></h2>
<p class="avtor"><a href="/uspensky/">Успенский Г.И.</a></p>
<p class="avtor">Категория: <a href="/category/classic/">Русская классика</a></p>
<p class="text">Формирование книги «Нравы Растеряевой улицы» было закончено Г.И. Успенским в 1883 году. Писатель объединил в одно целое очерки трёх циклов, печатавшиеся в 1866 году в различных журналах.</p><p>
</p></div>
</div>
<div class="video-list">
<div class="foto"><a href="/audio/5544/"><img src="//www.koob.ru/foto/author/9791.jpg"/></a></div>
<div class="text">
<h2><a href="/audio/5544/">Земной рай. Сборник рассказов</a></h2>
<p class="avtor"><a href="/uspensky/">Успенский Г.И.</a></p>
<p class="avtor">Категория: <a href="/category/classic/">Русская классика</a></p>
<p class="text">В сборнике представлены рассказы русского писателя Глеба Ивановича Успенского.</p><p>
</p></div>
</div>
<div class="video-list">
<div class="foto"><a href="/audio/5543/"><img src="//www.koob.ru/foto/author/9788.jpg"/></a></div>
<div class="text">
<h2><a href="/audio/5543/">"Поездка в Полесье"</a></h2>
<p class="avtor"><a href="/turgenev/">Тургенев И.С.</a></p>
<p class="avtor">Категория: <a href="/category/classic/">Русская классика</a></p>
<p class="text">Иван Сергеевич Тургенев "Поездка в Полесье", где воплотились многолетние размышления Тургенева над Человеком и Природой.</p><p>
</p></div>
</div>
<div class="video-list">
<div class="foto"><a href="/audio/5542/"><img src="//www.koob.ru/foto/author/9788.jpg"/></a></div>
<div class="text">
<h2><a href="/audio/5542/">"Пунин и Бабурин". "Конец Чертопханова"</a></h2>
<p class="avtor"><a href="/turgenev/">Тургенев И.С.</a></p>
<p class="avtor">Категория: <a href="/category/classic/">Русская классика</a></p>
<p class="text">Иван Сергеевич Тургенев "Пунин и Бабурин" - читают Дарья Белоусова, Олег Зима, Алексей Мясников, Андрей Сипин. "Конец Чертопханова" - читает Олег Зима.</p><p>
</p></div>
</div>
<div class="video-list">
<div class="foto"><a href="/audio/5541/"><img src="//www.koob.ru/foto/author/9788.jpg"/></a></div>
<div class="text">
<h2><a href="/audio/5541/">"Затишье"</a></h2>
<p class="avtor"><a href="/turgenev/">Тургенев И.С.</a></p>
<p class="avtor">Категория: <a href="/category/classic/">Русская классика</a></p>
<p class="text">Иван Сергеевич Тургенев "Затишье". Тургенев - имя уникальное даже в золотой плеяде классиков русской прозы XIX века.</p><p>
</p></div>
</div>
<div class="video-list">
<div class="foto"><a href="/audio/5540/"><img src="//www.koob.ru/foto/author/9788.jpg"/></a></div>
<div class="text">
<h2><a href="/audio/5540/">Тургенев в записях современников</a></h2>
<p class="avtor"><a href="/turgenev/">Тургенев И.С.</a></p>
<p class="avtor">Категория: <a href="/category/other/">Разное</a></p>
<p class="text">Тургенев в записях современников. Воспоминания, письма, дневники - Островский Арсений. Со страниц книги встает живой, полнокровный образ замечательного русского писателя в его творческой и человеческой ипостасях.</p><p>
</p></div>
</div>
<div class="video-list">
<div class="foto"><a href="/audio/5539/"><img src="//www.koob.ru/foto/author/9788.jpg"/></a></div>
<div class="text">
<h2><a href="/audio/5539/">"Живые мощи"</a></h2>
<p class="avtor"><a href="/turgenev/">Тургенев И.С.</a></p>
<p class="avtor">Категория: <a href="/category/classic/">Русская классика</a></p>
<p class="text">Иван Сергеевич Тургенев "Живые мощи". В спектакле принимали участие артисты Марья Афанасьева и Илья Ильин.</p><p>
</p></div>
</div>
<div class="video-list">
<div class="foto"><a href="/audio/5538/"><img src="//www.koob.ru/foto/author/9788.jpg"/></a></div>
<div class="text">
<h2><a href="/audio/5538/">Иван Тургенев</a></h2>
<p class="avtor"><a href="/turgenev/">Тургенев И.С.</a></p>
<p class="avtor">Категория: <a href="/category/other/">Разное</a></p>
<p class="text">Труайя Анри "Иван Тургенев". Славянский колосс с резкими чертами лица и мягким характером, увлекающийся и способный на глубокие чувства и абсолютно чуждый политическим страстям, великодушный человек, преданный родине и открытый всем соблазнам Европы, - таким предстает перед нами загадочный Иван Тургенев.</p><p>
</p></div>
</div>
<div class="video-list">
<div class="foto"><a href="/audio/5537/"><img src="//www.koob.ru/foto/author/9788.jpg"/></a></div>
<div class="text">
<h2><a href="/audio/5537/">"Дым"</a></h2>
<p class="avtor"><a href="/turgenev/">Тургенев И.С.</a></p>
<p class="avtor">Категория: <a href="/category/classic/">Русская классика</a></p>
<p class="text">Иван Сергеевич Тургенев "Дым". Безупречное литературное мастерство И.С. Тургенева соотносится со столь же безупречным знанием человеческой души.</p><p>
</p></div>
</div>
<div class="video-list">
<div class="foto"><a href="/audio/5536/"><img src="//www.koob.ru/foto/author/9788.jpg"/></a></div>
<div class="text">
<h2><a href="/audio/5536/">"Тургенев"</a></h2>
<p class="avtor"><a href="/turgenev/">Тургенев И.С.</a></p>
<p class="avtor">Категория: <a href="/category/other/">Разное</a></p>
<p class="text">Книга Андре Моруа "Тургенев" посвященная жизни и творчеству нашего классика, - это, прежде всего, взгляд на русских и русскую культуру XIX века, взгляд со стороны из Европы на малопонятную, загадочную Россию как на некое таинственное явление природы.</p><p>
</p></div>
</div>
<div class="video-list">
<div class="foto"><a href="/audio/5535/"><img src="//www.koob.ru/foto/author/9788.jpg"/></a></div>
<div class="text">
<h2><a href="/audio/5535/">"Певцы"</a></h2>
<p class="avtor"><a href="/turgenev/">Тургенев И.С.</a></p>
<p class="avtor">Категория: <a href="/category/classic/">Русская классика</a></p>
<p class="text">Рассказ Ивана Сергеевича Тургенева из трех частей "Певцы" в исполнении И. Ледогорова.</p><p>
</p></div>
</div>
<div class="page-list" style="font-size: 16px; margin: 20px 0;"><b>Страница:</b> <span><a class="on">1</a></span><span><a href="/?page=2">2</a></span><span><a href="/?page=3">3</a></span><span><a href="/?page=4">4</a></span><span><a href="/?page=5">5</a></span> ... <span><a href="/?page=366">366</a></span></div></div>
<div class="main-bottom" style="margin-top: 20px; padding: 10px; background-color: #f3f3f3; overflow: hidden; position: relative;">
<div style="position: absolute; height: 1px; width: 1px; overflow: hidden;">
<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter23618818 = new Ya.Metrika({id:23618818, clickmap:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img alt="" src="//mc.yandex.ru/watch/23618818" style="position:absolute; left:-9999px;"/></div></noscript><!-- /Yandex.Metrika counter -->
</div>
<noindex><div class="nikonov">Автор сайта: <a href="http://vk.com/nikonov_vladimir" rel="nofollow" target="_blank">Никонов Владимир</a></div></noindex>
</div>
</div>
</center>
</body>
</html>
