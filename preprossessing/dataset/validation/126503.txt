<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Este é o site da Associação Nacional dos Centros de Pós-graduação em Economia (Brasil)." name="description"/>
<meta content="economia, mestrado em economia, doutorado em economia, pós-graduação em economia, doutorado, mestrado, economics, phd, graduate programs, anpec" name="keywords"/>
<title>ANPEC - Associação Nacional dos Centros de Pós-graduação em Economia</title>
<base href="http://www.anpec.org.br/novosite/"/>
<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="styles.css" rel="stylesheet" type="text/css"/>
<link href="js/nivo-slider.css" rel="stylesheet" type="text/css"/>
<link href="js/themes/default/default.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="lz-admin/js/jquery-1.9.1.js"></script>
<script src="js/jquery.nivo.slider.js"></script>
<script src="js/mask.js"></script>
<script src="scripts.js"></script>
<link href="lz-admin/js/shadowbox/shadowbox.css" rel="stylesheet" type="text/css"/>
<script src="lz-admin/js/shadowbox/shadowbox.js" type="text/javascript"></script>
<script type="text/javascript">
	Shadowbox.init();
</script>
</head>
<body>
<div class="container_12">
<div id="idioma">
<div class="idiomatxt">Idioma</div>
<span>
<a href="http://www.anpec.org.br/novosite/br"><img class="ativo" src="images/flag_br.jpg"/></a>
<a href="http://www.anpec.org.br/novosite/us"><img src="images/flag_us.jpg"/></a>
<a href="http://www.anpec.org.br/novosite/es"><img src="images/flag_es.jpg"/></a>
</span>
</div>
</div>
<div id="wrapper">
<div id="topo">
<div class="container_12">
<div class="grid_3"><a href="http://www.anpec.org.br/novosite/br"><img id="logo" src="images/anpec.png"/></a></div>
<div class="grid_9"><h1>Associação Nacional dos Centros de Pós-Graduação em Economia</h1></div>
</div>
</div>
<div id="menu">
<div class="container_12">
<div class="grid_8">
<ul>
<li>
<a href="br/secretaria">Secretaria</a>
</li><li>
<a href="br/centros-associados">Centros</a>
</li><li>
<a href="br/exame">Exame</a>
</li><li>
<a href="br/encontros">Encontros</a>
</li><li>
<a href="br/revista">Revista</a>
</li><li>
<a href="br/premio">Prêmio</a>
</li><li>
<a href="br/fale-conosco">Contato</a>
</li>
</ul>
</div>
<div class="grid_4">
<form action="br/busca" method="GET">
<a href="https://www.facebook.com/pages/Anpec/172576972775219" target="_blank"><img src="images/ico_facebook.png"/></a>
<a href="https://twitter.com/anpec_br" target="_blank"><img src="images/ico_twitter.png"/></a>
<div class="campo_busca"><input class="campo_buscar" name="q" placeholder="Faça uma busca" type="text" value=""/><input class="lupa" type="submit" value=""/></div>
</form>
</div>
</div>
</div>
<div id="conteudo">
<div class="container_12">
<div class="grid_4">
<div class="grid_4 alpha omega box_lateral">
<div class="texto">
<div class="txt1">
<h2>A ANPEC</h2>
<p>Fundada em 1973, a Associação Nacional dos Centros de Pós-Graduação em Economia congrega as instituições brasileiras que desenvolvem atividades de pesquisa e formação em nível de pós-graduação na área de Economia.</p> </div>
</div>
<div class="bt_leiamais"><a href="br/a-anpec"><img src="images/bt_leiamais.jpg"/></a></div>
</div>
<div class="grid_4 alpha omega box_lateral">
<div class="texto">
<div class="txt2">
<h2>Centros Associados</h2>
<p>A ANPEC reúne atualmente 28 centros de excelência acadêmica de diversos Estados do Brasil.</p> </div>
</div>
<div class="bt_leiamais"><a href="br/centros-associados"><img src="images/bt_leiamais.jpg"/></a></div>
</div>
</div>
<div class="grid_8">
<div class="slider-wrapper theme-default">
<div class="nivoSlider" id="slider">
<a href="https://en.anpec.org.br" target="_self"> <img height="330" src="arquivos/banner-48_encontro-5.png" width="620"/>
</a> <a href="http://www.anpec.org.br/novosite/br/exame" target="_self"> <img height="330" src="arquivos/banner_exame-optimized.png" width="620"/>
</a> </div>
</div>
</div>
<div class="grid_6">
<div class="box_inferior ico_estrela">
<div class="texto">
<h2>Destaques</h2>
<ul>
<li>
<a href="https://economia.ufes.br/pt-br/selecao-2021-pos-doutorado" target="_blank">
<div class="thumb" style="background-image: url('arquivos/workshop_41.jpg')"></div> <div class="txt">
<h4>PPGEco/UFES - Processo seletivo para estágio de pós-doutorado 2021</h4>
Confira as informações </div>
<div class="clearfix"></div>
</a>
</li>
<li>
<a href="https://www.kas.de/pt/web/brasilien/veranstaltungen/detail/-/content/i-seminario-sobre-politicas-publicas-baseadas-em-evidencias-no-sistema-de-justica-criminal-brasileir" target="_blank">
<div class="thumb" style="background-image: url('arquivos/workshop_38.jpg')"></div> <div class="txt">
<h4>CAEN/UFC - I Seminário sobre Políticas Públicas baseadas em Evidências no Sistema de Justiça Criminal Brasileiro: Construindo Pontes entre Universidades, Polícias, Tribunais e Gestões Públicas</h4>
Confira as informações </div>
<div class="clearfix"></div>
</a>
</li>
<li>
<a href="uploads/FEA_RP-USP-Edital_PPGE 01_2020_ Mestrado_ingressantes-2021.pdf" target="_blank">
<div class="thumb" style="background-image: url('arquivos/workshop_35.jpg')"></div> <div class="txt">
<h4>FEA-RP/USP - Processo seletivo para o curso de Mestrado - Área Economia Aplicada (PPGE) para ingressantes em 2021.</h4>
Confira o edital </div>
<div class="clearfix"></div>
</a>
</li>
</ul>
</div>
</div>
<div class="bt_mais"><a href="br/destaques">Mais Destaques</a></div>
</div>
<div class="grid_6">
<div class="box_inferior ico_planeta">
<div class="texto">
<h2>Notícias</h2>
<ul>
<li>
<a href="http://www.anpec.org.br/novosite/br/premio">
<div class="thumb" style="background-image: url('arquivos/anpec_4.jpg')"></div> <div class="txt">
<h4>Prêmio Haralambos Simeonidis 2020</h4>
Confira os ganhadores </div>
<div class="clearfix"></div>
</a>
</li>
<li>
<a href="https://en.anpec.org.br/docs/48_encontro_nacional_de_economia-Aula_Magna-v20121216.pdf" target="_blank">
<div class="thumb" style="background-image: url('arquivos/logo_anpec_150x50.png')"></div> <div class="txt">
<h4>48º Encontro Nacional de Economia - Aula Magna</h4>
Acesse o paper </div>
<div class="clearfix"></div>
</a>
</li>
<li>
<a href="https://en.anpec.org.br/index.php#programming" target="_blank">
<div class="thumb" style="background-image: url('arquivos/anpec_10.jpg')"></div> <div class="txt">
<h4>48º Encontro Nacional de Economia - Programação</h4>
Confira a programação preliminar v.20201126 </div>
<div class="clearfix"></div>
</a>
</li>
</ul>
</div>
</div>
<div class="bt_mais"><a href="br/noticias">Mais Notícias</a></div>
</div>
</div>
</div>
</div>
<div class="container_12">
<div class="grid_12">
<div id="copyright">
©2004-2016 Anpec | UFF/Faculdade de Economia - Rua Prof Marcos Valdemar de Freitas Reis s/n - Bloco F - Sala 508 - Niterói, RJ - 24210-201 <div id="lazuli"><a href="http://lazulistudio.com.br/" target="_blank"><img src="images/lazuli.png"/></a>
</div>
</div>
</div></div></body>
</html>
