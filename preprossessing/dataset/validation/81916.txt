<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]--><!--[if IE 7 ]>    <html class="ie7"> <![endif]--><!--[if IE 8 ]>    <html class="ie8"> <![endif]--><!--[if IE 9 ]>    <html class="ie9"> <![endif]--><!--[if (gt IE 9)|!(IE)]><!--><html>
<!--<![endif]-->
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!-- icons & favicons -->
<link href="/TMS/content/images/favicon.ico" rel="shortcut icon"/>
<link href="/TMS/content/images/apple-touch-icon.png" rel="apple-touch-icon"/>
<!-- default stylesheet -->
<link href="/TMS/content/default.css" rel="stylesheet" type="text/css"/>
<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if necessary -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">        window.jQuery || document.write(unescape('%3Cscript src="/TMS/scripts/jquery-1.5.1.min.js"%3E%3C/script%3E'))</script>
<!-- CSS Modernizer Script -->
<script src="/TMS/scripts/modernizr-1.7.min.js" type="text/javascript"></script>
<!-- Afford Script -->
<script src="/TMS/scripts/afford.js" type="text/javascript"></script>
<!-- Link Script -->
<!-- Link Script -->
<script src="/TMS/scripts/config.js" type="text/javascript"></script>
<script src="/TMS/scripts/linkBuilder.js" type="text/javascript"></script>
<script type="text/javascript">
        document.domain = "afford.com";
    </script>
<!-- Afford stylesheet -->
<link href="/TMS/content/afford.css" rel="stylesheet" type="text/css"/>
<title>Afford.com - Home | Nelnet Campus Commerce</title>
</head>
<body>
<div id="top-bar">
<div id="blue-bar">
<div class="wrap-align">
<div id="admin-login-button">
<a class="fill affSchlLink" href="/SchoolLogin.aspx">Administrator Login</a></div>
</div>
</div>
<header class="wrap-align" role="banner">
<div id="logo-top">
<a href="/">
<img alt="TMS Logo" height="57" src="/TMS/content/images/tms-logo.png" width="247"/>
</a>
</div>
</header>
<nav role="navigation">
<div class="wrap-align">
<div class="menu">
<ul>
<li class="first"><a class="main" href="/TMS/afford-education">Explore Your Options</a>
<ul class="sub-menu sub1">
<li><a href="/TMS/afford-education/payment-plan.htm">Payment Plan</a></li>
<li><a href="/TMS/afford-education/faq.htm">FAQ</a></li>
</ul>
</li>
<li class="second"><a class="main" href="/TMS/payment-plans">Payment Plans</a>
<ul class="sub-menu sub2">
<li><a class="sfsLink" href="/SchoolSearch/">Enroll</a></li>
<li><a class="sfsLink" href="/SchoolSearch/">Reactivate</a></li>
<li><a class="sfsLink" href="/SchoolSearch/">Login to My Account</a></li>
<li><a class="sfsLink" href="/SchoolSearch/">Register for Online Access</a></li>
<li><a href="/TMS/payment-plans/faq.htm">FAQ</a></li>
</ul>
</li>
<li class="third"><a class="main" href="/TMS/make-payment">Make A Payment</a>
<ul class="sub-menu sub3">
<li><a class="sfsLink" href="/SchoolSearch/">Payment Plan</a></li>
<li><a class="sfsLink" href="/SchoolSearch/">Pay in Full</a></li>
<li><a href="/TMS/make-payment/faq.htm">FAQ</a></li>
</ul>
</li>
<li class="fourth"><a class="main" href="/TMS/school-refunds">School Refunds</a>
<ul class="sub-menu sub4">
<li><a href="/TMS/school-refunds">All Refunds</a></li>
<li><a href="/TMS/school-refunds/ach.htm">ACH</a></li>
<li><a href="/TMS/school-refunds/refund-checks.htm">Paper Check</a></li>
</ul>
</li>
</ul>
</div>
<div class="secondary-menu">
<ul>
<li class="about"><a href="/TMS/about-us">About Us</a></li>
<li class="contact"><a href="/TMS/contact">Contact</a></li>
<li class="student-family"><a class="sfsLink" href="/SchoolSearch/">Student/Family
                    Login</a></li>
</ul>
</div>
</div>
</nav>
</div>
<div id="content">
<div id="homepage">
<div id="home-upper">
<div class="wrap-align">
<img alt="Mother and Daughter" height="383" id="home-main-image" src="TMS/content/images/home-upper-image.png" width="620"/>
<div class="right">
<h1>
                    Partnering with your school to make education more affordable.</h1>
<p>
                    Tuition Management Systems is now Nelnet Campus Commerce. Nelnet Campus Commerce offers payment plans 
					that allow you to pay tuition and fees over time. Learn how payment plans can help make college more affordable.</p>
<div class="action">
<div id="get-started">
<a class="fill sfsLink" href="/SchoolSearch/"></a> </div>
<span>or</span> <a class="see-how" href="TMS/payment-plans">See how it works</a> </div>
</div>
</div>
</div>
<div id="home-lower">
<div class="wrap-align">
<div class="cell one">
<h2>
                    Why Choose Us?</h2>
<p>
                    Relax. The challenges of financing an education can appear overwhelming. <strong>That's
                        where we come in.</strong></p>
</div>
<div class="cell two">
<h3>
                    An Easier Way To Pay Your Bill</h3>
<p>
                    Pay your expenses over smaller monthly payments instead of one large lump sum payment.
                    No approval is necessary.</p>
</div>
<div class="cell three">
<h3>
                    Make A Payment</h3>
<p>
                    Why wait in line? Here's a convenient way to make online payments to your school
                    using a checking account, statement savings account, or credit card.</p>
</div>
</div>
</div>
</div>
</div><!--content -->
<footer role="contentinfo">
<div class="wrap-align">
<div class="left">
<div class="cell phone">
<!-- <span class="phonenumber">Call 800-722-4867</span> --> <!-- comment out MKapadia-->
<p>
                    Call 800-722-4867<br/>
                    Need Help? Call Us:<br/>
                    7 AM - 9 PM M-F (CT)<br/>
</p>
</div>
<div class="cell">
<!-- <span class="phonenumber">Call 800-722-4867</span> --> <!-- comment out MKapadia-->
<p>
                    Outside U.S. and Canada<br/>
                    Call 401-921-3999<br/>
</p></div>
<div class="cell">
<h4>
<a href="http://www.tuitionmanagementsystems.com">Administrators</a></h4>
<p>College, University and K12</p>
</div>
<div class="cell no-border">
<h4>
                    All Rights Reserved
                    
                    2021
                </h4>
<ul class="bullets">
<li><a href="https://www.campuscommerce.com/privacy">Privacy Notice</a></li>
</ul>
</div>
</div>
</div>
</footer>
<!--[if lt IE 9]>
			<script type="text/javascript" src="/TMS/scripts/ie/selectivizr.js"></script>
		<![endif]-->
<!--[if lt IE 7 ]>
    		<script type="text/javascript" src="/TMS/scripts/ie/dd_belatedpng.js"></script>
    		<script type="text/javascript">DD_belatedPNG.fix("img, .png_bg");</script>
  		<![endif]-->
<!-- Google Analytics -->
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-3271532-3']);
    _gaq.push(['_trackPageview']);

    (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
</body> </html>
