<!DOCTYPE html>
<html class="avada-html-layout-wide" lang="en-US" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Page not found - Curtain Suite Singapore | Curtain, Blinds, Shades</title>
<!-- This site is optimized with the Yoast SEO plugin v11.7 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex,follow" name="robots"/>
<meta content="en_US" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Page not found - Curtain Suite Singapore | Curtain, Blinds, Shades" property="og:title"/>
<meta content="Curtain Suite Singapore | Curtain, Blinds, Shades" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Page not found - Curtain Suite Singapore | Curtain, Blinds, Shades" name="twitter:title"/>
<script class="yoast-schema-graph yoast-schema-graph--main" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://curtainsuite.com/#website","url":"https://curtainsuite.com/","name":"Curtain Suite Singapore | Curtain, Blinds, Shades","potentialAction":{"@type":"SearchAction","target":"https://curtainsuite.com/?s={search_term_string}","query-input":"required name=search_term_string"}}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//js.hs-scripts.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://curtainsuite.com/feed/" rel="alternate" title="Curtain Suite Singapore | Curtain, Blinds, Shades » Feed" type="application/rss+xml"/>
<link href="https://curtainsuite.com/comments/feed/" rel="alternate" title="Curtain Suite Singapore | Curtain, Blinds, Shades » Comments Feed" type="application/rss+xml"/>
<link href="https://curtainsuite.com/wp-content/uploads/2019/07/CSFavicon.png" rel="shortcut icon" type="image/x-icon"/>
<!-- For iPhone -->
<link href="https://curtainsuite.com/wp-content/uploads/2019/07/PrestigeFaviconFB1.png" rel="apple-touch-icon"/>
<!-- For iPhone Retina display -->
<link href="https://curtainsuite.com/wp-content/uploads/2019/07/CSFaviconIpad1.png" rel="apple-touch-icon" sizes="114x114"/>
<!-- For iPad -->
<link href="https://curtainsuite.com/wp-content/uploads/2019/07/CSFaviconIpad1.png" rel="apple-touch-icon" sizes="72x72"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/curtainsuite.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://curtainsuite.com/wp-content/themes/Avada/assets/css/style.min.css?ver=5.9.1" id="avada-stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if IE]>
<link rel='stylesheet' id='avada-IE-css'  href='https://curtainsuite.com/wp-content/themes/Avada/assets/css/ie.min.css?ver=5.9.1' type='text/css' media='all' />
<style id='avada-IE-inline-css' type='text/css'>
.avada-select-parent .select-arrow{background-color:#ffffff}
.select-arrow{background-color:#ffffff}
</style>
<![endif]-->
<link href="https://curtainsuite.com/wp-content/uploads/fusion-styles/d64624e4df4edb26e341478457909124.min.css?ver=5.2.9" id="fusion-dynamic-css-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://curtainsuite.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://curtainsuite.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://curtainsuite.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://curtainsuite.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://curtainsuite.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.2.9" name="generator"/>
<!-- DO NOT COPY THIS SNIPPET! Start of Page Analytics Tracking for HubSpot WordPress plugin v7.6.0-->
<script type="text/javascript">
				var _hsq = _hsq || [];
				_hsq.push(["setContentType", "standard-page"]);
			</script>
<!-- DO NOT COPY THIS SNIPPET! End of Page Analytics Tracking for HubSpot WordPress plugin -->
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<script type="text/javascript">
		var doc = document.documentElement;
		doc.setAttribute('data-useragent', navigator.userAgent);
	</script>
</head>
<body class="error404 fusion-image-hovers fusion-body ltr fusion-sticky-header no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-mobile-totop fusion-disable-outline fusion-sub-menu-fade mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v6 avada-responsive avada-footer-fx-parallax-effect fusion-search-form-classic fusion-avatar-square">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<div class="" id="wrapper">
<div id="home" style="position:relative;top:-1px;"></div>
<header class="fusion-header-wrapper">
<div class="fusion-header-v6 fusion-logo-left fusion-sticky-menu- fusion-sticky-logo-1 fusion-mobile-logo-1 fusion-header-has-flyout-menu">
<div class="fusion-header-sticky-height"></div>
<div class="fusion-header">
<div class="fusion-row">
<div class="fusion-header-v6-content fusion-header-has-flyout-menu-content">
<div class="fusion-logo" data-margin-bottom="31px" data-margin-left="0px" data-margin-right="0px" data-margin-top="31px">
<a class="fusion-logo-link" href="https://curtainsuite.com/">
<!-- standard logo -->
<img alt="Curtain Suite Singapore | Curtain, Blinds, Shades Logo" class="fusion-standard-logo" data-retina_logo_url="https://curtainsuite.com/wp-content/uploads/2019/07/CSPrestigeDefaultLogo2.png" height="46" src="https://curtainsuite.com/wp-content/uploads/2019/07/CSPrestigeDefaulttLogo.png" srcset="https://curtainsuite.com/wp-content/uploads/2019/07/CSPrestigeDefaulttLogo.png 1x, https://curtainsuite.com/wp-content/uploads/2019/07/CSPrestigeDefaultLogo2.png 2x" style="max-height:46px;height:auto;" width="200"/>
<!-- mobile logo -->
<img alt="Curtain Suite Singapore | Curtain, Blinds, Shades Logo" class="fusion-mobile-logo" data-retina_logo_url="" height="46" src="https://curtainsuite.com/wp-content/uploads/2019/07/CSPrestigeDefaulttLogo.png" srcset="https://curtainsuite.com/wp-content/uploads/2019/07/CSPrestigeDefaulttLogo.png 1x" width="200"/>
<!-- sticky header logo -->
<img alt="Curtain Suite Singapore | Curtain, Blinds, Shades Logo" class="fusion-sticky-logo" data-retina_logo_url="https://curtainsuite.com/wp-content/uploads/2019/07/CSPrestigeDefaultLogo2.png" height="46" src="https://curtainsuite.com/wp-content/uploads/2019/07/CSPrestigeDefaulttLogo.png" srcset="https://curtainsuite.com/wp-content/uploads/2019/07/CSPrestigeDefaulttLogo.png 1x, https://curtainsuite.com/wp-content/uploads/2019/07/CSPrestigeDefaultLogo2.png 2x" style="max-height:46px;height:auto;" width="200"/>
</a>
</div>
<div class="fusion-flyout-menu-icons">
<div class="fusion-flyout-sliding-bar-toggle">
<a aria-label="Toggle Sliding Bar" class="fusion-toggle-icon fusion-icon fusion-icon-sliding-bar" href="#"></a>
</div>
<div class="fusion-flyout-search-toggle">
<div class="fusion-toggle-icon">
<div class="fusion-toggle-icon-line"></div>
<div class="fusion-toggle-icon-line"></div>
<div class="fusion-toggle-icon-line"></div>
</div>
<a aria-hidden="true" aria-label="Toggle Search" class="fusion-icon fusion-icon-search" href="#"></a>
</div>
<a aria-hidden="true" aria-label="Toggle Menu" class="fusion-flyout-menu-toggle" href="#">
<div class="fusion-toggle-icon-line"></div>
<div class="fusion-toggle-icon-line"></div>
<div class="fusion-toggle-icon-line"></div>
</a>
</div>
</div>
<div aria-label="Main Menu" class="fusion-main-menu fusion-flyout-menu" role="navigation">
<ul class="fusion-menu" id="menu-freelancer-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-70" data-item-id="70" id="menu-item-70"><a class="fusion-bar-highlight" href="https://curtainsuite.com/"><span class="menu-text">Home</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67" data-item-id="67" id="menu-item-67"><a class="fusion-bar-highlight" href="https://curtainsuite.com/recent-work/"><span class="menu-text">Work</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69" data-item-id="69" id="menu-item-69"><a class="fusion-bar-highlight" href="https://curtainsuite.com/article/"><span class="menu-text">Articles</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-790 fusion-flyout-menu-item-last" data-item-id="790" id="menu-item-790"><a class="fusion-bar-highlight" href="https://curtainsuite.com/lets-talk/"><span class="menu-text">Let’s Talk</span></a></li></ul> </div>
<div class="fusion-flyout-search">
<form action="https://curtainsuite.com/" class="searchform fusion-search-form fusion-live-search" method="get" role="search">
<div class="fusion-search-form-content">
<div class="fusion-search-field search-field">
<label><span class="screen-reader-text">Search for:</span>
<input aria-label="Search ..." aria-required="true" class="s" name="s" placeholder="Search ..." required="" type="text" value=""/>
</label>
</div>
<div class="fusion-search-button search-button">
<input class="fusion-search-submit searchsubmit" type="submit" value=""/>
</div>
</div>
</form>
</div>
<div class="fusion-flyout-menu-bg"></div>
</div>
</div>
</div>
<div class="fusion-clearfix"></div>
</header>
<div id="sliders-container">
</div>
<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-center">
<div class="fusion-page-title-row">
<div class="fusion-page-title-wrapper">
<div class="fusion-page-title-captions">
<h1 class="entry-title">Error 404 Page</h1>
</div>
</div>
</div>
</div>
<main class="clearfix " id="main" style="">
<div class="fusion-row" style="">
<section class="full-width" id="content">
<div id="post-404page">
<div class="post-content">
<div class="fusion-title fusion-title-size-two sep-" style="margin-top:0px;margin-bottom:0px;">
<h2 class="title-heading-left" style="margin:0;">
						Oops, This Page Could Not Be Found!					</h2>
<div class="title-sep-container">
<div class="title-sep sep-"></div>
</div>
</div>
<div class="fusion-clearfix"></div>
<div class="error-page">
<div class="fusion-columns fusion-columns-3">
<div class="fusion-column col-lg-4 col-md-4 col-sm-4 fusion-error-page-404">
<div class="error-message">404</div>
</div>
<div class="fusion-column col-lg-4 col-md-4 col-sm-4 useful-links fusion-error-page-useful-links">
<h3>Helpful Links</h3>
<ul class="fusion-checklist fusion-404-checklist error-menu" id="fusion-checklist-1" style="font-size:14px;line-height:23.8px;"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-70"><span class="icon-wrapper circle-yes" style="background-color:#f9225b;font-size:14px;height:23.8px;width:23.8px;margin-right:9.8px;"><i class="fusion-li-icon fa fa-angle-right" style="color:#ffffff;"></i></span><div class="fusion-li-item-content" style="margin-left:33.6px;"><a href="https://curtainsuite.com/">Home</a></div></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67"><span class="icon-wrapper circle-yes" style="background-color:#f9225b;font-size:14px;height:23.8px;width:23.8px;margin-right:9.8px;"><i class="fusion-li-icon fa fa-angle-right" style="color:#ffffff;"></i></span><div class="fusion-li-item-content" style="margin-left:33.6px;"><a href="https://curtainsuite.com/recent-work/">Work</a></div></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69"><span class="icon-wrapper circle-yes" style="background-color:#f9225b;font-size:14px;height:23.8px;width:23.8px;margin-right:9.8px;"><i class="fusion-li-icon fa fa-angle-right" style="color:#ffffff;"></i></span><div class="fusion-li-item-content" style="margin-left:33.6px;"><a href="https://curtainsuite.com/article/">Articles</a></div></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-790"><span class="icon-wrapper circle-yes" style="background-color:#f9225b;font-size:14px;height:23.8px;width:23.8px;margin-right:9.8px;"><i class="fusion-li-icon fa fa-angle-right" style="color:#ffffff;"></i></span><div class="fusion-li-item-content" style="margin-left:33.6px;"><a href="https://curtainsuite.com/lets-talk/">Let’s Talk</a></div></li></ul> </div>
<div class="fusion-column col-lg-4 col-md-4 col-sm-4 fusion-error-page-search">
<h3>Search Our Website</h3>
<p>Can't find what you need? Take a moment and do a search below!</p>
<div class="search-page-search-form">
<form action="https://curtainsuite.com/" class="searchform fusion-search-form fusion-live-search" method="get" role="search">
<div class="fusion-search-form-content">
<div class="fusion-search-field search-field">
<label><span class="screen-reader-text">Search for:</span>
<input aria-label="Search ..." aria-required="true" class="s" name="s" placeholder="Search ..." required="" type="text" value=""/>
</label>
</div>
<div class="fusion-search-button search-button">
<input class="fusion-search-submit searchsubmit" type="submit" value=""/>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div> <!-- fusion-row -->
</main> <!-- #main -->
<div class="fusion-footer fusion-footer-parallax">
<footer class="fusion-footer-widget-area fusion-widget-area fusion-footer-widget-area-center">
<div class="fusion-row">
<div class="fusion-columns fusion-columns-1 fusion-widget-area">
<div class="fusion-column fusion-column-last col-lg-12 col-md-12 col-sm-12">
<section class="fusion-footer-widget-column widget widget_media_image" id="media_image-2"><img alt="" class="image wp-image-1156 attachment-full size-full" height="72" sizes="(max-width: 72px) 100vw, 72px" src="https://curtainsuite.com/wp-content/uploads/2019/07/CSFaviconIpad1.png" srcset="https://curtainsuite.com/wp-content/uploads/2019/07/CSFaviconIpad1-66x66.png 66w, https://curtainsuite.com/wp-content/uploads/2019/07/CSFaviconIpad1.png 72w" style="max-width: 100%; height: auto;" width="72"/><div style="clear:both;"></div></section> </div>
<div class="fusion-clearfix"></div>
</div> <!-- fusion-columns -->
</div> <!-- fusion-row -->
</footer> <!-- fusion-footer-widget-area -->
<footer class="fusion-footer-copyright-area fusion-footer-copyright-center" id="footer">
<div class="fusion-row">
<div class="fusion-copyright-content">
<div class="fusion-copyright-notice">
<div>
		© Copyright 2014 - <script>document.write(new Date().getFullYear());</script>   |   Developed by <a href="http://curtainsuite.com" target="_blank">CS Suite</a>   |   All Rights Reserved 	</div>
</div>
<div class="fusion-social-links-footer">
<div class="fusion-social-networks boxed-icons"><div class="fusion-social-networks-wrapper"><a class="fusion-social-network-icon fusion-tooltip fusion-facebook fusion-icon-facebook" data-placement="top" data-title="Facebook" data-toggle="tooltip" href="https://www.facebook.com/curtainsuite" rel="noopener noreferrer" style="color:#1d2028;background-color:#ffffff;border-color:#ffffff;border-radius:0px;" target="_blank" title="Facebook"><span class="screen-reader-text">Facebook</span></a><a class="fusion-social-network-icon fusion-tooltip fusion-instagram fusion-icon-instagram" data-placement="top" data-title="Instagram" data-toggle="tooltip" href="https://www.instagram.com/curtainsuite" rel="noopener noreferrer" style="color:#1d2028;background-color:#ffffff;border-color:#ffffff;border-radius:0px;" target="_blank" title="Instagram"><span class="screen-reader-text">Instagram</span></a><a class="fusion-social-network-icon fusion-tooltip fusion-youtube fusion-icon-youtube" data-placement="top" data-title="YouTube" data-toggle="tooltip" href="https://www.youtube.com/watch?v=pGYBX85-7-c" rel="noopener noreferrer" style="color:#1d2028;background-color:#ffffff;border-color:#ffffff;border-radius:0px;" target="_blank" title="YouTube"><span class="screen-reader-text">YouTube</span></a><a class="fusion-social-network-icon fusion-tooltip fusion-mail fusion-icon-mail" data-placement="top" data-title="Email" data-toggle="tooltip" href="https://curtainsuite.com/lets-talk/" rel="noopener noreferrer" style="color:#1d2028;background-color:#ffffff;border-color:#ffffff;border-radius:0px;" target="_self" title="Email"><span class="screen-reader-text">Email</span></a></div></div></div>
</div> <!-- fusion-fusion-copyright-content -->
</div> <!-- fusion-row -->
</footer> <!-- #footer -->
</div> <!-- fusion-footer -->
<div class="slidingbar-area fusion-sliding-bar-area fusion-widget-area fusion-sliding-bar-position-left fusion-sliding-bar-text-align-left fusion-sliding-bar-toggle-menu fusion-sliding-bar-sticky fusion-sliding-bar-columns-stacked" data-breakpoint="800" data-toggle="menu" id="slidingbar-area">
<div class="fusion-sliding-bar" id="slidingbar">
<div class="fusion-sliding-bar-content-wrapper">
<div class="fusion-sb-toggle-wrapper">
<a class="fusion-sb-close" href="#"><span class="screen-reader-text">Close Sliding Bar Area</span></a>
</div>
<div class="fusion-sliding-bar-content">
<div class="fusion-column">
<section class="fusion-slidingbar-widget-column widget widget_text" id="text-2"><h4 class="widget-title">About Our Works</h4> <div class="textwidget"><p>We are committed to giving the best we can. We constantly upgrade and update ourselves working with leading international partners to bring you the latest and most advance technology and systems to you.</p>
</div>
<div style="clear:both;"></div></section><section class="fusion-slidingbar-widget-column widget recent_works" id="recent_works-widget-4"><h4 class="widget-title">Recent Works</h4>
<div class="recent-works-items clearfix">
<a href="https://curtainsuite.com/portfolio-items/drapery/" rel="" target="_self" title="DRAPERY">
<img alt="" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" height="66" sizes="(max-width: 66px) 100vw, 66px" src="https://curtainsuite.com/wp-content/uploads/2016/01/LucindaCurtainPorfolio-66x66.png" srcset="https://curtainsuite.com/wp-content/uploads/2016/01/LucindaCurtainPorfolio-66x66.png 66w, https://curtainsuite.com/wp-content/uploads/2016/01/LucindaCurtainPorfolio-150x150.png 150w" width="66"/> </a>
<a href="https://curtainsuite.com/portfolio-items/rollerblinds/" rel="" target="_self" title="ROLLER BLINDS">
<img alt="" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" height="66" sizes="(max-width: 66px) 100vw, 66px" src="https://curtainsuite.com/wp-content/uploads/2016/01/RollerblindsPortfolio1-66x66.png" srcset="https://curtainsuite.com/wp-content/uploads/2016/01/RollerblindsPortfolio1-66x66.png 66w, https://curtainsuite.com/wp-content/uploads/2016/01/RollerblindsPortfolio1-150x150.png 150w" width="66"/> </a>
<a href="https://curtainsuite.com/portfolio-items/romanblinds/" rel="" target="_self" title="ROMAN BLINDS">
<img alt="" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" height="66" sizes="(max-width: 66px) 100vw, 66px" src="https://curtainsuite.com/wp-content/uploads/2016/01/RomanBlindsPortfolio-66x66.png" srcset="https://curtainsuite.com/wp-content/uploads/2016/01/RomanBlindsPortfolio-66x66.png 66w, https://curtainsuite.com/wp-content/uploads/2016/01/RomanBlindsPortfolio-150x150.png 150w" width="66"/> </a>
<a href="https://curtainsuite.com/portfolio-items/flexiduoshades/" rel="" target="_self" title="FLEXI DUO SHADES">
<img alt="" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" height="66" sizes="(max-width: 66px) 100vw, 66px" src="https://curtainsuite.com/wp-content/uploads/2016/01/RippleCurtainPortfolio-66x66.png" srcset="https://curtainsuite.com/wp-content/uploads/2016/01/RippleCurtainPortfolio-66x66.png 66w, https://curtainsuite.com/wp-content/uploads/2016/01/RippleCurtainPortfolio-150x150.png 150w" width="66"/> </a>
<a href="https://curtainsuite.com/portfolio-items/venetianblinds/" rel="" target="_self" title="VENETIAN BLINDS">
<img alt="" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" height="66" sizes="(max-width: 66px) 100vw, 66px" src="https://curtainsuite.com/wp-content/uploads/2016/01/VenetianBlinds-66x66.png" srcset="https://curtainsuite.com/wp-content/uploads/2016/01/VenetianBlinds-66x66.png 66w, https://curtainsuite.com/wp-content/uploads/2016/01/VenetianBlinds-150x150.png 150w" width="66"/> </a>
<a href="https://curtainsuite.com/portfolio-items/zipscreenblinds/" rel="" target="_self" title="ZIP SCREEN">
<img alt="" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" height="66" sizes="(max-width: 66px) 100vw, 66px" src="https://curtainsuite.com/wp-content/uploads/2016/01/ZipScreenPortfolio-66x66.png" srcset="https://curtainsuite.com/wp-content/uploads/2016/01/ZipScreenPortfolio-66x66.png 66w, https://curtainsuite.com/wp-content/uploads/2016/01/ZipScreenPortfolio-150x150.png 150w" width="66"/> </a>
</div>
<div style="clear:both;"></div></section> <section class="fusion-slidingbar-widget-column widget widget_recent_entries" id="recent-posts-5"> <h4 class="widget-title">Recent Posts</h4> <ul>
<li>
<a href="https://curtainsuite.com/2016/01/27/live-the-design-process/">Design Process</a>
</li>
<li>
<a href="https://curtainsuite.com/2016/01/27/rollerblindsvscurtains/">The rise of Roller Blinds vs Curtains</a>
</li>
<li>
<a href="https://curtainsuite.com/2016/01/27/installation-services-n-guidelines/">Installation Services &amp; Guidelines</a>
</li>
</ul>
<div style="clear:both;"></div></section><section class="fusion-slidingbar-widget-column widget social_links" id="social_links-widget-3"><h4 class="widget-title">Let’s Talk</h4>
<div class="fusion-social-networks">
<div class="fusion-social-networks-wrapper">
<a aria-label="Facebook" class="fusion-social-network-icon fusion-tooltip fusion-facebook fusion-icon-facebook" data-original-title="" data-placement="top" data-title="Facebook" data-toggle="tooltip" href="https://www.facebook.com/curtainsuite" rel="noopener noreferrer" style="font-size:16px;color:#bebdbd;" target="_self" title="Facebook"></a>
<a aria-label="Instagram" class="fusion-social-network-icon fusion-tooltip fusion-instagram fusion-icon-instagram" data-original-title="" data-placement="top" data-title="Instagram" data-toggle="tooltip" href="https://www.instagram.com/curtainsuite" rel="noopener noreferrer" style="font-size:16px;color:#bebdbd;" target="_self" title="Instagram"></a>
<a aria-label="Mail" class="fusion-social-network-icon fusion-tooltip fusion-mail fusion-icon-mail" data-original-title="" data-placement="top" data-title="Mail" data-toggle="tooltip" href="mailto:https://www.curtainsuite.com/lets-talk" rel="noopener noreferrer" style="font-size:16px;color:#bebdbd;" target="_self" title="Mail"></a>
<a aria-label="Whatsapp" class="fusion-social-network-icon fusion-tooltip fusion-whatsapp fusion-icon-whatsapp" data-original-title="" data-placement="top" data-title="Whatsapp" data-toggle="tooltip" href="https://wa.me/6597812985?text=I'm%20interested%20to%20know%20more%20about%20window%20furnishing" rel="noopener noreferrer" style="font-size:16px;color:#bebdbd;" target="_self" title="Whatsapp"></a>
</div>
</div>
<div style="clear:both;"></div></section> </div>
<div class="fusion-clearfix"></div>
</div>
</div>
</div>
</div>
</div> <!-- wrapper -->
<a class="fusion-one-page-text-link fusion-page-load-link"></a>
<script type="text/javascript">
				jQuery( document ).ready( function() {
					var ajaxurl = 'https://curtainsuite.com/wp-admin/admin-ajax.php';
					if ( 0 < jQuery( '.fusion-login-nonce' ).length ) {
						jQuery.get( ajaxurl, { 'action': 'fusion_login_nonce' }, function( response ) {
							jQuery( '.fusion-login-nonce' ).html( response );
						});
					}
				});
				</script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/curtainsuite.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://curtainsuite.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.3" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var leadin_wordpress = {"userRole":"visitor","pageType":"other","leadinPluginVersion":"7.6.0"};
/* ]]> */
</script>
<script async="" defer="" src="//js.hs-scripts.com/6190779.js?integration=WordPress&amp;ver=7.6.0" type="text/javascript"></script>
<script src="https://curtainsuite.com/wp-content/uploads/fusion-scripts/68089a9fd1d6ab55e86ee95b52360b55.min.js" type="text/javascript"></script>
<script src="https://curtainsuite.com/wp-includes/js/wp-embed.min.js?ver=5.2.9" type="text/javascript"></script>
</body>
</html>
