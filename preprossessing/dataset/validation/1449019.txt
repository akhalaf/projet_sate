<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<title>Thymio - Il robot educativo per tutti</title>
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<!-- Custom CSS -->
<link href="css/one-page-wonder.css" rel="stylesheet"/>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
<!-- Add jQuery library -->
<script src="lib/jquery-1.10.1.min.js" type="text/javascript"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script src="lib/jquery.mousewheel-3.0.6.pack.js" type="text/javascript"></script>
<!-- Add fancyBox main JS and CSS files -->
<script src="source/jquery.fancybox.js?v=2.1.5" type="text/javascript"></script>
<link href="source/jquery.fancybox.css?v=2.1.5" media="screen" rel="stylesheet" type="text/css"/>
<!-- Add Button helper (this is optional) -->
<link href="source/helpers/jquery.fancybox-buttons.css?v=1.0.5" rel="stylesheet" type="text/css"/>
<script src="source/helpers/jquery.fancybox-buttons.js?v=1.0.5" type="text/javascript"></script>
<!-- Add Thumbnail helper (this is optional) -->
<link href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" rel="stylesheet" type="text/css"/>
<script src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.7" type="text/javascript"></script>
<!-- Add Media helper (this is optional) -->
<script src="source/helpers/jquery.fancybox-media.js?v=1.0.6" type="text/javascript"></script>
<script src="js/gif.js" type="text/javascript"></script>
<script src="js/video.js" type="text/javascript"></script>
<style>.fancyboxVid .videoTag {
		display: none;
	}
	video {
		background: #000;
		display: block;
		max-width: 100%;
		max-height: 620px;
	}
	.container-fixed {
		margin: 0 auto;
		max-width: 960px;
		background-color: white;
	}
	.row {
		margin-left:0 !important;
		margin-right: 0!important;
	}
	.content {
		height: 274px;
		overflow: hidden;
		position: relative;
		width: 468px;
	}
	.content img {
		left: 0;
		position: absolute;
		top: 0;
	}
	</style>
</head>
<body>
<div class="container-fixed">
<header class="header-image">
<a class="video" href="https://www.youtube.com/watch?v=wdCFg52i2Lc?fs=1&amp;autoplay=1" title="">
<img class="img-responsive" src="img/thymio-head.jpg"/>
</a>
</header>
<div class="featurette" id="about">
<h2 class="featurette-heading">Con Thymio Ã¨ piÃ¹ facile!</h2>
<p class="lead" style="text-align:center;">
				Thymio Ã¨ un piccolo robot progettato per lâuso didattico. <br/>
				Ã per tutte le fasce dâetÃ . Dallâinfanzia allâuniversitÃ .
			</p>
</div>
<div class="row" style="background:#e5e5e5; padding-top:30px; padding-bottom:30px;">
<div class="col-md-2 col-md-offset-1 col-sm-6 col-sm-offset-1 text-center">
<img alt="" class="img-circle img-responsive img-center center-block" src="img/svizzero.png"/>
<h3>Ã¨ svizzero</h3>
<p>Ã il frutto della migliore ricerca universitaria elvetica in robotica educativa. </p>
</div>
<div class="col-md-2 col-sm-6 text-center">
<img alt="" class="img-circle img-responsive img-center center-block" src="img/economico.png"/>
<h3>Ã¨ economico </h3>
<p>...ed ha un grandissimo numero di sensori e attuatori.</p>
</div>
<div class="col-md-2 col-sm-6 text-center">
<img alt="" class="img-circle img-responsive img-center center-block" src="img/interattivo.png"/>
<h3>Ã¨ interattivo</h3>
<p>Per incrementare la comprensione delle sue funzionalitÃ  e per facilitare lâapprendimento di molte materie nellâarco dei cicli scolastici.</p>
</div>
<div class="col-md-2 col-sm-6 text-center">
<img alt="" class="img-circle img-responsive img-center center-block" src="img/opensource.png"/>
<h3>Ã¨ open source</h3>
<p>Offre un ambiente di programmazione efficiente e intuitivo sia visuale che testuale appositamente progettato per la didattica.</p>
</div>
<div class="col-md-2 col-sm-6 text-center">
<img alt="" class="img-circle img-responsive img-center center-block" src="img/duttile.png"/>
<h3>Ã¨ duttile</h3>
<p>
					Permette di fare molte attivitÃ , documentate e raccolte secondo obiettivi didattici.
					Ha una comunitÃ  di docenti europei che cresce ogni giorno.
				</p>
</div>
</div>
<img class="img-responsive" src="img/thymio-kids.jpg"/>
<header class="image-bg">
<div class="headline">
<h2 style="padding-top: 20px;">
					 Con i robot Ã¨ piÃ¹ facile accendere <br/>
					la passione per le materie scientifiche <br/>
					e sviluppare le attivitÃ  creative
				</h2>
</div>
</header>
<div class="featurette" id="about">
<h2 class="featurette-heading">Per i piÃ¹ piccoli, un robot pronto allâuso</h2>
<div class="col-lg-6 col-sm-6">
<img border="0" class="featurette-image img-circle img-responsive " src="img/4-6-anni.png" style="margin: 0 auto;"/>
</div>
<div class="col-lg-6 col-sm-6">
<p class="lead">
					Si puÃ² iniziare la scoperta di Thymio senza perdere un attimo, appena estratto dalla scatola.
					Ha giÃ  6 comportamenti pre-programmati.
					Ogni comportamento Ã¨ associato ad un colore.
				</p>
<p class="lead2">
<strong>â¢ VERDE AMICHEVOLE</strong>. Segue un oggetto  a una certa distanza.<br/>
<strong>â¢ GIALLO ESPLORATORE</strong>. Esplora evitando  gli ostacoli.<br/>
<strong>â¢ ROSSO TIMOROSO</strong>. Scappa se qualcosa si avvicina troppo. <br/>
<strong>â¢ VIOLA OBBEDIENTE</strong>. Obbedisce  ai tasti capacitivi o al telecomando.<br/>
<strong>â¢ AZZURRO INVESTIGATORE</strong>. Segue una linea a terra.. <br/>
<strong>â¢ BLU ATTENTO</strong>. Reagisce ai suoni. Si puÃ² comandare battendo le mani.
				</p>
</div>
</div>
<div class="col-lg-6 col-sm-6" style="padding:0;">
<div class="content" id="gif">
<img src="img/Thymio_gif_01.jpg"/>
<img src="img/Thymio_gif_02.jpg"/>
<img src="img/Thymio_gif_03.jpg"/>
<img src="img/Thymio_gif_04.jpg"/>
<img src="img/Thymio_gif_05.jpg"/>
<img src="img/Thymio_gif_06.jpg"/>
</div>
</div>
<div class="col-lg-6 col-sm-6">
<p class="lead3">
				Scoprire Thymio attraverso i suoi comportamenti permette
				giÃ  molte attivitÃ  educative.
				Poi trovarne alcune nel volume âAttivitÃ  per lâinfanziaâ
				e molte altre nella comunitÃ  di docenti europei che utilizzano
				giÃ  Thymio in classe.
			</p>
</div>
<img class="img-responsive" src="img/thymio-classroom.jpg"/>
<div class="col-lg-12" style="background:#e5e5e5; padding:40px;">
<h2 class="featurette-heading">
				Alle primarie, Thymio<br/>
				si puÃ² programmare con le immagini!
			</h2>
</div>
<div class="row" style="background:#e5e5e5; padding-bottom:40px;">
<div class="col-md-6" style="padding-left: 0;">
<img class="img-responsive" src="img/thymio-imac.jpg"/>
</div>
<div class="col-md-3">
<p class="lead4">
<strong>
						Iniziare a comprendere
						la programmazione
						Ã¨ piÃ¹ facile utilizzando
						le immagini.
					</strong>
<br/>
<br/>
					Ã il metodo VPL - Programmazione Visuale per THYMIO con il software Aseba Studio, scaricabile gratutitamente e installabile
					sia su PC che Mac.
					Il programma Ã¨ facile e intuitivo,
					Ã¨ sufficiente accoppiare sullo schermo gli eventi che si vuole accadano e cliccare PLAY e osservare Thymio allâopera!
				</p>
</div>
<div class="col-md-3">
<img class="img-responsive img-left" src="img/6-12-anni.png"/>
</div>
</div>
<img class="img-responsive" src="img/thymio-infographic.jpg"/>
<div class="blockly-scratch">
<div class="col-md-6">
<h2 class="featurette-heading" style="text-align:right; padding-top:20px;">...e poi<br/> con Blockly<br/> e Scratch</h2>
</div>
<div class="col-md-6"></div>
</div>
<header class="image-bg2">
<div class="headline">
<h2 style="padding-top: 35px;">
					Con i robot Ã¨ piÃ¹ facile creare<br/>
					un gruppo sinergico e collaborativo
				</h2>
</div>
</header>
<img class="img-responsive" src="img/thymio-floor.jpg"/>
<div class="col-lg-12" style="background:#1a2930; padding-top:40px; padding-bottom:20px;color:white;">
<h2 class="featurette-heading" style="padding-bottom:0px!important;">
				E per i grandi, programmare <br/>
				con le parole Ã¨ una sfida
			</h2>
</div>
<div class="row" style="background:#1a2930;">
<div class="col-md-4" style="padding-left:40px; margin-top: -30px;">
<img class="img-responsive" src="img/14-18-anni.png"/>
</div>
<div class="col-md-4">
<img class="img-responsive img-center" src="img/aseba.png"/>
</div>
<div class="col-md-4"></div>
</div>
<div class="col-md-6">
<p class="lead6" style="color:white; padding-top:30px;padding-left:40px;">
<strong style="font-size:18px;">
					Thymio ha anche un ambiente di sviluppo integrato, Aseba, open source, progettato per apprendere
					la robotica e condurre progetti di ricerca. .
				</strong>
<br/>
<br/>
				Ã una architettura basata sugli eventi, per il controllo distribuito, in tempo reale. Aseba Ã¨ integrabile
				anche con ROS (Robot Operating System) ed utilizzato
				in numerose UniversitÃ  internazionali.
			</p>
</div>
<header class="image-bg3">
<div class="col-md-6" style="padding-left:40px;"></div>
<div class="col-md-6"></div>
</header>
<img class="img-responsive" src="img/thymio-classroom2.jpg"/>
<div class="col-lg-12" style="background:#cccccc; padding-top:40px;">
<h2 class="featurette-heading" style=" padding-bottom:0">
				Con i robot<br/>
				Ã¨ piÃ¹ facile
				insegnare
			</h2>
</div>
<img class="img-responsive" src="img/thymio-adv.jpg"/>
<div class="col-md-12" style="padding-left:80px;padding-right:80px;">
<p class="lead4" style="text-align:center">
				Thymio puÃ² essere utilizzato in ambito educativo scientifico, tecnico ed artistico. Molte attivitÃ  sono giÃ  classificate secondo obiettivi scolastici e etÃ  consigliata. Molte altre si aggiungono continuamente grazie ad unâattiva comunitÃ  di docenti europei che condivide le attivitÃ  realizzate in classe.
				<br/>
<br/>
<strong>
					PRENOTA IL LIBRO CON  LE ATTIVITÃ TRADOTTE IN ITALIANO
				</strong>
</p></div>
<img class="img-responsive" src="img/thymio-book.jpg"/>
<header class="image-bg2">
<div class="headline">
<h2 style=" padding-top: 35px;">
						Con Thymio Ã¨ piÃ¹ facile<br/>
						sentirsi parte della ricerca europea
					</h2>
</div>
</header>
<div class="col-lg-12" style="background:#e5e5e5; padding-top:40px; padding-left:80px; padding-right:80px;">
<h4>
					Thymio nasce dalla migliore ricerca svizzera sullâimpiego<br/>
					della robotica per lâeducazione.
				</h4>
</div>
<div class="row" style="background:#e5e5e5; padding-bottom:50px;">
<div class="col-lg-6" style="background:#e5e5e5;padding-top:40px; padding-left:80px; ">
<p class="lead4">
						Thymio Ã¨ stato sviluppato al Politecnico Federale di Losanna (EPFL) e Zurigo (ETH) dove si occupano della progettazione hardware del robot, completamente open source, e degli ambienti di sviluppo visuali per ragazzi (VPL, Blockly) e testuali Aseba cosi come delle applicazioni di realtÃ  aumentata.
						Il  Dipartimento di Scienze della formazione del Weizmann Institute of Science in Israele sviluppa materiali educativi per Thymio e svolge attivitÃ  di ricerca in campo educativo. LâECAL - Scuola cantonale dâarte di Losanna si Ã¨ occupato alla progettazione estetica del robot. Mente lâ INRIA in Francia sviluppa materiali educativi per gli insegnanti e lâinterfaccia Thymio per Scratch del MIT. NCRR Robotics ha contribuito a finanziare lo sviluppo del robot Thymio e la sua validazione scientifica insieme a numerosi altri partner e ad una comunitaâ di sviluppatori, docenti ed educatori.
					</p>
</div>
<div class="col-lg-6" style="background:#e5e5e5;padding-top:40px;padding-right:80px;">
<p class="lead4">
						Lâassociazione Mobsya, senza scopo di lucro, presiede la produzione e commercializzazione del robot educativo e delle attivitaâ ad esso connesso per favorire lâapprendimento delle materie scientifiche e tecniche, dellâarte e delle scienze.

						In Italia si sta diffondendo come  robot educativo versatile e utilizzabile da una fascia dâetÃ  ampia che va dai 4 ai 18 anni. Qui in Italia ci occupiamo non solo della distribuzione ma anche dellâorganizzazione delle informazioni, della comunicazione e della didattica.
					</p>
</div>
</div>
<div class="col-lg-12" style="background:#fff; padding-top:40px;">
<h2 class="featurette-heading" style=" padding-bottom:0">ComâÃ¨ fatto Thymio?</h2>
</div>
<img class="img-responsive" src="img/thymio-components.jpg"/>
<header class="image-bg">
<div class="headline">
<h2 style="padding-top: 50px;"> Listino </h2>
</div>
</header>
<div class="col-lg-12" style="background:#e5e5e5; padding-top:40px; padding-left:80px; padding-bottom:40px; padding-right:80px; text-align:center;">
<h3 style="font-size:24px;">PRODOTTI</h3>
<br/>
<p class="lead5">
<strong>
						Il robot educativo Thymio Ã¨ un piccolo strumento di formazione interattivo ad un prezzo conveniente.
						Totalmente open-source a livello software e hardware.
					</strong>
<br/>
<br/>
					Di dimensioni molto contenute (11 x 11 x 5 cm), molto robusto, con numerosissimi LED, tasti capacitivi, accelerometro, sensore della temperatura, due motori indipendenti, microfono e altoparlante e molto altro.
					Adatto dalla scuola dellâinfanzia allâuniversitÃ . Scelto dalle scuole svizzere e francesi come miglior robot didattico per le sue caratteristiche tecniche e per le attivitÃ  pedagogiche a corredo.
				</p>
</div>
<div class="row" style="border-bottom:1px solid #cccccc; ">
<div class="col-lg-6" style="padding-top:30px; padding-left:150px; padding-bottom:30px;">
<img class="img-responsive" src="img/thymio-1.jpg"/>
</div>
<div class="col-lg-6" style="padding-top:30px;padding-bottom:30px;">
<h1>
						THYMIO BASE<br/>
<span></span>
</h1>
<p class="lead4">
						Programmabile collegando un cavetto USB <br/>
						a un PC con qualsiasi sistema operativo.
					</p>
</div>
</div>
<div class="row" style="border-bottom:1px solid #cccccc; ">
<div class="col-lg-6" style="padding-top:30px;padding-bottom:30px;padding-left:150px">
<img class="img-responsive" src="img/thymio-wireless.jpg"/>
</div>
<div class="col-lg-6" style="padding-top:30px;padding-bottom:30px;">
<h1>
						THYMIO WIRELESS<br/>
<span></span>
</h1>
<p class="lead4">
						Programmabile collegando via wireless<br/>
						a un PC con qualsiasi sistema operativo.
					</p>
</div>
</div>
<div class="col-lg-6" style="padding-left:0;">
<img class="img-responsive" src="img/thymio-kit.jpg"/>
</div>
<div class="col-lg-6" style="padding-top:30px;">
<h1>
					KIT ATELIER ROBOTICA<br/>
					EDUCATIONAL PACK 6 THYMIO <br/>
<span></span>
</h1>
<p class="lead4">
					SET DI 6 ROBOT EDUCATIVI THYMIO BASE PROGRAMMABILI VIA USB, <br/>
					CON CARICATORE USB E TELECOMANDO A INFRAROSSI <br/>
					per allestire un atelier di una scuola o un comprensorio scolastico.
					Rende semplice e comodo lâimpiego in classe, facilita il trasporto e la ricarica dei Thymio nelle diverse attivitÃ  scolastiche con i robot.
				</p>
</div>
<div class="col-lg-12" style="background:#e5e5e5; padding-top:40px; padding-bottom:40px; padding-left:80px; padding-right:80px; text-align:center;">
<h3 style="font-size:24px;">ATTIVITÃ DIDATTICHE E CORSI</h3>
<br/>
<p class="lead5">
					Le attivitÃ  possono essere organizzate presso la scuola da un formatore THYMIO, oppure acquistate e utilizzate direttamente dai docenti. Chi fosse interessato puÃ² richiedere maggiori informazioni.
				</p>
</div>
<div class="row" style="border-bottom:1px solid #cccccc;">
<div class="col-lg-6" style="padding-left:0;">
<img class="img-responsive" src="img/robokkio.jpg"/>
</div>
<div class="col-lg-6" style="padding-top:20px;">
<h1>
						ROBOKKIO<br/>
</h1><p>Attivitaâ di robotica educativa affrontando i temi dei rischi e delle opportunitaâ delle nuove tecnologie e del web.</p>
<p class="lead4">
						AttivitÃ  di robotica educativa che affronta i temi dei rischi e delle opportunitÃ  delle nuove tecnologie e del web.
						Ã rivolta agli alunni dellâultimo anno delle scuole primarie e delle scuole secondarie di primo grado.
						Dopo una breve introduzione alla robotica, si affrontano i temi: i <span>comportamenti pericolosi sul web, la gestione dei dati personali, il cyberbullismo e il conformismo on-line, raccontati e fatti vivere ai ragazzi grazie ai robot Thymio con giochi, quiz e rappresentazioni sceniche</span>.
						Ã previsto anche un il âdietro le quinteâ degli aspetti tecnici che consentono la realizzazione delle attivitÃ  didattiche in cui si cimentano gli studenti per stimolarne la curiositÃ  e lâinteresse verso le materie scientifiche e la tecnologia.
						Numero di Thymio necessari: 4
					</p>
</div>
</div>
<div class="row" style="border-bottom:1px solid #cccccc; ">
<div class="col-lg-6" style="padding-top:70px;">
<img class="img-responsive center-block" src="img/formazione-icon.jpg"/>
</div>
<div class="col-lg-6" style="padding-top:30px;padding-bottom:30px;">
<h1>
						FORMAZIONE DOCENTI<br/>
</h1><p>Base</p>
<p class="lead4">
						Giornata di formazione rivolta ai docenti delle scuole primarie e secondarie di primo grado.
						Obiettivi didattici: scoprire lâutilizzo del robot Thymio nelle attivitÃ  didattiche in classe, conoscere il suo funzionamento e imparare alcune attivitÃ  didattiche che prevedono lâimpiego del robot Thymio nei suoi comportamenti di base e nella modalitÃ  di programmazione.
					</p>
</div>
</div>
<div class="row" style="border-bottom:1px solid #cccccc;">
<div class="col-lg-6" style="padding-top:70px;">
<img class="img-responsive center-block" src="img/formazione-thymio.jpg"/>
</div>
<div class="col-lg-6" style="padding-top:30px;padding-bottom:30px;">
<h1>
						Formazione docenti + Thymio<br/>
</h1><p>(corso + Robot Thymio BASE)</p>
<p class="lead4">
						Giornata di formazione rivolta ai docenti delle scuole primarie e secondarie di primo grado con 1 Thymio BASE incluso nel costo iscrizione.
						Obiettivi del corso: scoprire lâutilizzo del robot Thymio nelle attivitÃ  didattiche in classe, conoscere il suo funzionamento e imparare alcune attivitÃ  didattiche che prevedono lâimpiego del robot Thymio nei suoi comportamenti di base e nella modalitÃ  di programmazione.
					</p>
</div>
</div>
<div class="featurette" style="padding-bottom:0px; padding-top:20px;">
<h2 class="featurette-heading" style="font-size:40px">Per maggiori informazioni</h2>
<div class="col-lg-4 linea">
<p class="lead7">Sulle modalitÃ <br/>di acquisto dei robot </p>
<br/>
<a href="https://www.borgione.it/catalogsearch/result/?q=thymio" target="_blank"><img src="img/borgione.gif"/></a>
<br/>
<br/>
<br/>
<p class="link">www.borgione.it<br/>011 4551555</p>
<br/>
<br/>
</div>
<div class="col-lg-8">
<p class="lead7">sulle attivitÃ  didattiche e corsi</p>
<br/>
<div class="col-lg-6 linea">
<img src="img/brt.gif"/>
<br/>
<br/>
<br/>
<p class="link">Paolo Rossetti<br/>335 6968980</p>
<br/>
<br/>
</div>
<br/>
<div class="col-lg-6">
<a href="https://digituslab.it/" target="_blank"><img src="img/stripes.gif"/></a>
<br/>
<br/>
<br/>
<p class="link">digituslab.it<br/>345 0104806</p>
<br/>
<br/>
</div>
</div>
</div>
<div class="col-lg-12" style="background-color:#1a2930; height:100px;"></div>
</div>
<!-- jQuery -->
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-4531050-2', 'auto');
	ga('send', 'pageview');
	</script>
</body>
</html>
