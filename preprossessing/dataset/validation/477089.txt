<!DOCTYPE html>
<html lang="en"><head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="Empy İnşaat.Güven İnşa Ediyoruz." name="description"/>
<meta content="Empy,empy,İnşaat,inşaat,Empy İnşaat,empy inşaat,inşaat sektörü,bina,yapı,konut,ev,proje" name="keywords"/>
<meta content="İnşaat Sektörü" name="classification"/>
<meta content="tr" name="content-language"/>
<meta content="Hakan Yücel" name="author"/>
<link href="http://getbootstrap.com/assets/ico/favicon.ico" rel="shortcut icon"/>
<title>Empy İnşaat </title>
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet"/>
<!-- Custom styles for this template -->
<link href="css/justified-nav.css" rel="stylesheet"/>
<link href="css/bootstrap-image-gallery.min.css" rel="stylesheet"/>
<link href="css/font-awesome.min.css" rel="stylesheet"/>
<link href="css/bootstrap-social.css" rel="stylesheet"/>
<link href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css" rel="stylesheet"/>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>	  
    <![endif]-->
</head>
<body>
<div class="container">
<div class="row hidden-xs" style="margin-top:20px;">
<a href="index.php"><img alt="Empy İnşaat" class="col-md-12 img-responsive" src="logo.jpg"/></a>
</div>
<div class="row">
<div class="masthead col-md-12 hidden-xs">
<ul class="nav nav-justified">
<li><a href="index.php">Ana Sayfa</a></li>
<!--!-->
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Kurumsal</a>
<ul class="dropdown-menu">
<li><a href="hakkimizda.php">Hakkımızda</a></li>
<li><a href="misyonumuz.php">Misyonumuz</a></li>
<li><a href="vizyonumuz.php">Vizyonumuz</a></li>
</ul>
<!--!--></li>
<!--!-->
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Projelerimiz</a>
<ul class="dropdown-menu">
<li><a href="projeler.php?tamam=0">Devam Eden Projeler</a></li>
<li><a href="projeler.php?tamam=1">Tamamlanan Projeler</a></li>
</ul>
<!--!--></li>
<li><a href="haberler.php">Haberler</a></li>
<li><a href="galeri.php">Galeri</a></li>
<li><a href="iletisim.php">İletişim</a></li>
</ul>
</div>
<nav class="visible-xs navbar navbar-default navbar-static-top" role="navigation">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="#">Empy İnşaat</a>
</div>
<div class="navbar-collapse collapse">
<ul class="nav navbar-nav">
<li class="active"><a href="index.php">Ana Sayfa</a></li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Kurumsal <b class="caret"></b></a>
<ul class="dropdown-menu">
<li><a href="hakkimizda.php">Hakkımızda</a></li>
<li><a href="misyonumuz.php">Misyonumuz</a></li>
<li><a href="vizyonumuz.php">Vizyonumuz</a></li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Projelerimiz <b class="caret"></b></a>
<ul class="dropdown-menu">
<li><a href="projeler.php?tamam=1">Tamamlanan Projeler</a></li>
<li><a href="projeler.php?tamam=0">Devam Eden Projeler</a></li>
</ul>
</li>
<li><a href="haberler.php">Haber</a></li>
<li><a href="galeri.php">Galeri</a></li>
<li><a href="iletisim.php">İletişim</a></li>
</ul>
</div>
</div>
</nav>
</div>
<center> <div class="row visible-xs" style="margin-top:20px;">
<a href="index.php"><img alt="Empy İnşaat" class="col-md-12 img-responsive" src="img/xslogo.jpg"/></a>
</div></center> <div class="row">
<!--<img src="banner.png"   class="col-md-12 img-responsive" />!-->
<div class="carousel slide hidden-xs " data-ride="carousel" id="myCarousel">
<!-- Indicators -->
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#myCarousel"></li>
<li data-slide-to="1" data-target="#myCarousel"></li>
<li data-slide-to="2" data-target="#myCarousel"></li>
</ol>
<div class="carousel-inner " style="margin:0px">
<div class="item active">
<img alt="First slide" class="img-responsive" src="banner.jpg"/>
</div>
<div class="item">
<img alt="First slide" class="img-responsive" src="banner2.jpg"/>
</div>
<div class="item">
<img alt="First slide" class="img-responsive" src="banner3.jpg"/>
</div>
</div>
<a class="left carousel-control" data-slide="prev" href="#myCarousel">
<span class="glyphicon glyphicon-chevron-left"></span>
</a>
<a class="right carousel-control" data-slide="next" href="#myCarousel">
<span class="glyphicon glyphicon-chevron-right"></span>
</a>
</div><!-- /.carousel -->
</div>
<br/>
<div class="row">
<div class="col-xs-12">
<div class="well">
<h1 class="text-center">Güven İnşa Ediyoruz</h1>
<p class="text-center">
Empy İnşaat teknoloji ve tasarımın uyum içinde olduğu,doğaya saygılı,modern projeler üretmeye; milli inşaat sektörüne büyük katkılar sunmaya devam edecektir
</p>
</div>
</div>
</div>
<div class="row">
<div class="col-xs-12">
<div class="alert alert-info well-sm">
<h3>
<marquee behavior="scroll" direction="left" onmouseout="this.start();" onmouseover="this.stop();" scrollamount="7">  2B arsalarınız değerlendirilir.
Arsalarınız değerinde alınır.
Arsalarınız kat karşılığında değerlendirilir.
2+1,3+1 dublex dairelerimiz satışa sunulmuştur.</marquee>
</h3>
</div> </div>
</div>
<!-- Example row of columns -->
<div class="row">
<div class="col-sm-6 col-md-4">
<div class="thumbnail well">
<img alt="..." class="img-responsive img-rounded" src="misyonumuz.jpg"/>
<div class="caption">
<h3>Misyonumuz</h3>
<p>Kuruluşundan bugüne sürekli olarak büyüyen Empy İnşaat,üstlendiği projeleri zamanında ve sorunsuz olarak sonuçlandırmıştır.
Şirketimizin gerçekleştirdiği başarılı çalışmaları(...)</p>
<p><a class="btn btn-primary" href="misyonumuz.php" role="button">Devamı..</a> </p>
</div>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="thumbnail well">
<img alt="..." class="img-responsive img-rounded" src="vizyonumuz.jpg"/>
<div class="caption">
<h3>Vizyonumuz</h3>
<p>Kişi ve Kurumların İnşaat Sektöründe ihtiyaç duyduğu tüm çözümleri sunarak sektörde hızla gelişen bir inşaat firması  olmak.
Köklü ve güçlü yapımızla (...)</p>
<p><a class="btn btn-primary" href="vizyonumuz.php" role="button">Devamı..</a> </p>
</div>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="thumbnail well">
<img alt="..." class="img-responsive img-rounded" height="192" src="http://empy.com.tr/haber/HaberImg_8.jpg" width="256"/>
<div class="caption">
<h3>Bizden Haberler</h3>
<p>Empy İnşaat, İnkılap Mahallesi (Latife Sokak,Aydoğan Sokak), Fevzipaşa Mahallesi ve Ümraniye-Elmalıkentte 
çalışmalarına baş(...)</p>
<p><a class="btn btn-primary" href="haberler.php" role="button">Devamı..</a> </p>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-xs-12">
<div class="panel panel-default">
<div class="panel-heading"><strong>Galerimiz</strong></div>
<div class="panel-body">
<div id="links">
<div class="col-lg-3 col-md-4 col-xs-6 ">
<a class="thumbnail" data-gallery="" href="http://empy.com.tr/gallery/galleryimg_13.jpg" title="Foto 1">
<img class="img-responsive" src="http://empy.com.tr/gallery/galleryimg_13.jpg"/>
</a>
</div>
</div>
</div>
</div>
</div>
</div>
<hr/>
<div class="row hidden-xs">
<div class="col-md-offset-1 col-md-5">
<ul class="list-unstyled">
<li><a href="projeler.php">Çiçek Apartmanı Projesi</a></li><li><a href="projeler.php">Ertuğrul Apartmanı Projemiz</a></li><li><a href="projeler.php">İnkılap Mahallesi Latife Sokak Projesi</a></li><li><a href="projeler.php">İnkılap Mahallesi Yıldırım Caddesi Projesi</a></li><li><a href="projeler.php">İnkılap Mahallesi Fevzipaşa Caddesi Projesi</a></li><li><a href="projeler.php">Ümraniye Elmalıkent Mahallesi Projesi</a></li>
</ul>
</div>
<div class="col-md-6">
<ul class="list-unstyled">
<li><a href="haberler.php">Güçlü ve Başarılı ekibiyle Empy İnşaat </a></li> </ul>
</div></div>
<!-- Site footer -->
<div class="footer">
<div class="row">
<ul class="list-inline col-md-8">
<li><a href="index.php">Anasayfa</a></li>
<li><a href="hakkimizda.php">Hakkımızda</a></li>
<li><a href="misyonumuz.php">Misyonumuz</a></li>
<li><a href="vizyonumuz.php">Vizyonumuz</a></li>
<li><a href="projeler.php?tamam=0">Projeler</a></li>
<li><a href="haberler.php">Haberler</a></li>
<li><a href="galeri.php">Galeri</a></li>
<li><a href="iletisim.php">İletişim</a></li>
</ul>
<ul class="list-inline col-md-4 text-right">
<li>
<a class="btn btn-social-icon btn-twitter" href="https://twitter.com/empyinsaat" title="Twitter">
<i class="fa fa-twitter"></i>
</a>
</li>
<li>
<a class="btn btn-social-icon btn-google-plus" href="https://plus.google.com/+EmpyinsaatTr/about" title="Google+">
<i class="fa fa-google-plus"></i>
</a>
</li>
<li>
<a class="btn btn-social-icon btn-facebook" href="https://www.facebook.com/Empyinsaat" title="Facebook">
<i class="fa fa-facebook"></i>
</a>
</li>
</ul>
</div>
<p>©IntercomPC 2014</p>
</div> <!-- Site footer -->
</div> <!-- /container -->
<!-- Gallery Widget !-->
<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div class="blueimp-gallery" id="blueimp-gallery">
<!-- The container for the modal slides -->
<div class="slides"></div>
<!-- Controls for the borderless lightbox -->
<h3 class="title"></h3>
<a class="prev">‹</a>
<a class="next">›</a>
<a class="close">×</a>
<a class="play-pause"></a>
<ol class="indicator"></ol>
<!-- The modal dialog, which will be used to wrap the lightbox content -->
<div class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button aria-hidden="true" class="close" type="button">×</button>
<h4 class="modal-title"></h4>
</div>
<div class="modal-body next"></div>
<div class="modal-footer">
<button class="btn btn-default pull-left prev" type="button">
<i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
<button class="btn btn-primary next" type="button">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
</button>
</div>
</div>
</div>
</div>
</div>
<!-- Gallery Widget !-->
<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<script src="js/docs.min"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.marquee.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".dropdown-toggle").dropdown();
	   $("#marquee1").marquee();
});  

</script>
</body></html>