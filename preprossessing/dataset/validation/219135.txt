<!DOCTYPE html>
<html class="cspio">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<title>Biip.no nettsamfunn</title>
<meta content="seedprod.com 5.0.0" name="generator"/>
<meta content="Biip.no er et norsk nettsamfunn med over 500.000 registrerte medlemmer." name="description"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!-- Open Graph -->
<meta content="https://www.biip.no/" property="og:url"/>
<meta content="website" property="og:type"/>
<meta content="Biip.no nettsamfunn" property="og:title"/>
<meta content="Biip.no er et norsk nettsamfunn med over 500.000 registrerte medlemmer." property="og:description"/>
<!-- Twitter Card -->
<meta content="summary" name="twitter:card"/>
<meta content="Biip.no nettsamfunn" name="twitter:title"/>
<meta content="Biip.no er et norsk nettsamfunn med over 500.000 registrerte medlemmer." name="twitter:description"/>
<!--[if lte IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
<!-- Font Awesome -->
<script defer="" src="https://www.biip.no/wp-content/plugins/seedprod-coming-soon-pro-5/template/js/fontawesome-all.min.js"></script>
<script defer="" src="https://www.biip.no/wp-content/plugins/seedprod-coming-soon-pro-5/template/js/fa-v4-shims.min.js"></script>
<!-- Bootstrap and default Style -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://www.biip.no/wp-content/plugins/seedprod-coming-soon-pro-5/template/style.css" rel="stylesheet"/>
<!-- Google Fonts -->
<link class="gf-headline" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:700&amp;subset=" rel="stylesheet" type="text/css"/>
<link class="gf-text" href="https://fonts.googleapis.com/css?family=Roboto:400&amp;subset=" rel="stylesheet" type="text/css"/>
<link class="gf-button" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400&amp;subset=" rel="stylesheet" type="text/css"/>
<!-- RTL -->
<!-- Animate CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet"/>
<style type="text/css">
		.cspio .progress-bar {
  background-color: #812d89;
  background-image: -moz-linear-gradient(top,#812d89,#4b1a4f);
  background-image: -ms-linear-gradient(top,#812d89,#4b1a4f);
  background-image: -webkit-gradient(linear,0 0,0 100%,from(#812d89),to(#4b1a4f));
  background-image: -webkit-linear-gradient(top,#812d89,#4b1a4f);
  background-image: -o-linear-gradient(top,#812d89,#4b1a4f);
  background-image: linear-gradient(top,#812d89,#4b1a4f);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#812d89', endColorstr='#4b1a4f', GradientType=0);
}
	.progress-striped .progress-bar, .progress.active .progress-bar{
		background-color:#812d89	}
	
		.countdown_section {
  background-color: #812d89;
  background-image: -moz-linear-gradient(top,#812d89,#4b1a4f);
  background-image: -ms-linear-gradient(top,#812d89,#4b1a4f);
  background-image: -webkit-gradient(linear,0 0,0 100%,from(#812d89),to(#4b1a4f));
  background-image: -webkit-linear-gradient(top,#812d89,#4b1a4f);
  background-image: -o-linear-gradient(top,#812d89,#4b1a4f);
  background-image: linear-gradient(top,#812d89,#4b1a4f);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#812d89', endColorstr='#4b1a4f', GradientType=0);
}
	

	/* Background Style */
	html{
		height:100%;
						background: #812d89 url(http://s3.amazonaws.com/static.seedprod.com/public-backgrounds/seedprod-pattern-1-compressor.png) no-repeat center center fixed;
									-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					background-size: cover;
					    	}

	
	.flexbox #cspio-page{
			-webkit-align-items: center;
		-webkit-justify-content: center;
		align-items: center;
		justify-content: center;
										}

	/* IE 11 Flexbox hack */
	@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
		.flexbox #cspio-page{
			flex-shrink: 0;
			overflow:visible;
		}
	}

	.cspio body{
		background: transparent;
	}

    /* Text Styles */
    	    .cspio body, .cspio body p{
	        font-family: 'Roboto';
			font-weight: 400;
			font-style: ;
	        font-size: 16px;
	        line-height: 1.50em;
	        	        color:#424242;
	        	 	}

		::-webkit-input-placeholder {
			font-family:'Roboto';
			font-weight: 400;
			font-style: ;
		}
		::-moz-placeholder {
			font-family:'Roboto';
			font-weight: 400;
			font-style: ;
		} /* firefox 19+ */
		:-ms-input-placeholder {
			font-family:'Roboto';
			font-weight: 400;
			font-style: ;
		} /* ie */
		:-moz-placeholder {
			font-family:'Roboto';
			font-weight: 400;
			font-style: ;
		}

    

    .cspio h1, .cspio h2, .cspio h3, .cspio h4, .cspio h5, .cspio h6{
            font-family: 'Roboto Condensed';
                    color:#424242;
            }
	#cspio-headline{
			font-family: 'Roboto Condensed';
		font-weight: 700;
		font-style: ;
			font-size: 42px;
		color:#424242;
		line-height: 1.00em;
	}

    	    .cspio button{
	        font-family: 'Roboto Condensed';
			font-weight: 400;
			font-style: ;
	    }
	
    /* Link Styles */
    		.cspio a, .cspio a:visited, .cspio a:hover, .cspio a:active{
			color: #812d89;
		}

		.cspio-subscriber-count,.cspio-reveal{
			color: #812d89;
		}

		#cspio-socialprofiles a {
  color: #424242;
}
.cspio a.btn-primary,
.cspio .btn-primary,
.cspio .btn-primary:focus,
.gform_button,
#mc-embedded-subscribe,
.mymail-wrapper .submit-button,
.mailster-wrapper .submit-button,
input[type='button'].ninja-forms-field,
.wpcf7-submit,
.frm_button_submit {
  color: white;
  text-shadow: 0 -1px 0 rgba(0,0,0,0.3);
  background-color: #812d89;
  background-image: -moz-linear-gradient(top,#812d89,#4b1a4f);
  background-image: -ms-linear-gradient(top,#812d89,#4b1a4f);
  background-image: -webkit-gradient(linear,0 0,0 100%,from(#812d89),to(#4b1a4f));
  background-image: -webkit-linear-gradient(top,#812d89,#4b1a4f);
  background-image: -o-linear-gradient(top,#812d89,#4b1a4f);
  background-image: linear-gradient(top,#812d89,#4b1a4f);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#812d89', endColorstr='#4b1a4f', GradientType=0);
  border-color: #4b1a4f #4b1a4f #150716;
  border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);
  *background-color: #4b1a4f;
  filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
}
.cspio a.btn-primary:hover,
.cspio a.btn-primary:active,
.cspio a.btn-primary.active,
.cspio a.btn-primary.disabled,
.cspio a.btn-primary[disabled],
.cspio .btn-primary:hover,
.cspio .btn-primary:active,
.cspio .btn-primary.active,
.cspio .btn-primary.disabled,
.cspio .btn-primary[disabled],
.cspio .btn-primary:focus:hover,
.cspio .btn-primary:focus:active,
.cspio .btn-primary:focus.active,
.cspio .btn-primary:focus.disabled,
.cspio .btn-primary:focus[disabled],
.gform_button:hover,
.gform_button:active,
.gform_button.active,
.gform_button.disabled,
.gform_button[disabled],
#mc-embedded-subscribe:hover,
#mc-embedded-subscribe:active,
#mc-embedded-subscribe.active,
#mc-embedded-subscribe.disabled,
#mc-embedded-subscribe[disabled],
.mymail-wrapper .submit-button:hover,
.mymail-wrapper .submit-button:active,
.mymail-wrapper .submit-button.active,
.mymail-wrapper .submit-button.disabled,
.mymail-wrapper .submit-button[disabled],
.mailster-wrapper .submit-button:hover,
.mailster-wrapper .submit-button:active,
.mailster-wrapper .submit-button.active,
.mailster-wrapper .submit-button.disabled,
.mailster-wrapper .submit-button[disabled],
input[type='button'].ninja-forms-field:hover,
input[type='button'].ninja-forms-field:active,
input[type='button'].ninja-forms-field.active,
input[type='button'].ninja-forms-field.disabled,
input[type='button'].ninja-forms-field[disabled],
.wpcf7-submit:hover,
.wpcf7-submit:active,
.wpcf7-submit.active,
.wpcf7-submit.disabled,
.wpcf7-submit[disabled],
.frm_button_submit:hover,
.frm_button_submit:active,
.frm_button_submit.active,
.frm_button_submit.disabled,
.frm_button_submit[disabled] {
  background-color: #4b1a4f;
  *background-color: #39143c;
}
.cspio a.btn-primary:active,
.cspio a.btn-primary.active,
.cspio .btn-primary:active,
.cspio .btn-primary.active,
.cspio .btn-primary:focus:active,
.cspio .btn-primary:focus.active,
.gform_button:active,
.gform_button.active,
#mc-embedded-subscribe:active,
#mc-embedded-subscribe.active,
.mymail-wrapper .submit-button:active,
.mymail-wrapper .submit-button.active,
.mailster-wrapper .submit-button:active,
.mailster-wrapper .submit-button.active,
input[type='button'].ninja-forms-field:active,
input[type='button'].ninja-forms-field.active,
.wpcf7-submit:active,
.wpcf7-submit.active,
.frm_button_submit:active,
.frm_button_submit.active {
  background-color: #270d29 \9;
}
.form-control,
.progress {
  background-color: #ffffff;
}
.form-control {
  color: black;
  text-shadow: 0 -1px 0 rgba(255,255,255,0.3);
}
#cspio-progressbar span,
.countdown_section {
  color: white;
  text-shadow: 0 -1px 0 rgba(0,0,0,0.3);
}
.cspio .btn-primary:hover,
.cspio .btn-primary:active {
  color: white;
  text-shadow: 0 -1px 0 rgba(0,0,0,0.3);
  border-color: #5d2063;
}
.cspio input[type='text']:focus {
  webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075), 0 0 8px rgba(76,26,81,0.6);
  -moz-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075), 0 0 8px rgba(76,26,81,0.6);
  box-shadow: inset 0 1px 1px rgba(0,0,0,0.075), 0 0 8px rgba(76,26,81,0.6);
}
    
    
    

        #cspio-credit,#cspio-credit a{
    	color: #ffffff    }
    
        	    	    


    #cspio-content {
  display: none;
  max-width: 600px;
  background-color: #ffffff;
  -webkit-border-radius: 2px;
  border-radius: 2px;
  -moz-background-clip: padding;
  -webkit-background-clip: padding-box;
  background-clip: padding-box;
}

    
        #cspio-content{
    	 /* display:none; */
    }
    

		.cspio a.btn-primary,
.cspio .progress-bar,
.countdown_section,
.cspio .btn-primary,
.cspio .btn-primary:focus,
.gform_button,
.mymail-wrapper .submit-button,
.mailster-wrapper .submit-button,
input[type='button'].ninja-forms-field,
.wpcf7-submit,
.frm_button_submit {
  background-image: none;
  text-shadow: none;
}
.countdown_section,
.cspio .progress-bar {
  -webkit-box-shadow: none;
  box-shadow: none;
}
.cspio input,
.cspio input:focus {
  -webkit-box-shadow: none !important;
  box-shadow: none !important;
}
	
		
						

				html {
				height: 100%;
				overflow: hidden;
				}
				body
				{
				height:100%;
				overflow: auto;
				-webkit-overflow-scrolling: touch;
				}

					
		#cspio-content,.btn-primary,.countdown_section,.progress {
    box-shadow: 0 4px 5px 0 rgba(0,0,0,.14),0 1px 10px 0 rgba(0,0,0,.12),0 2px 4px -1px rgba(0,0,0,.2);
}

.btn-primary,.countdown_section,.progress {
border-radius:2px;
}

.form-control{
   border-bottom: 1px solid #424242 !important;
    border-radius: 0 !important;
}		
	

	


	</style>
<!-- JS -->
<script src="https://www.biip.no/wp-includes/js/jquery/jquery.js"></script>
<!-- Modernizr -->
<script src="https://www.biip.no/wp-content/plugins/seedprod-coming-soon-pro-5/template/js/modernizr-custom.js"></script>
<!-- Retina JS -->
<!-- Header Scripts -->
<!-- Google Analytics -->
<script src="https://cdn.jsdelivr.net/jquery.url.parser/2.3.1/purl.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.cookie/1.4.1/jquery.cookie.min.js"></script>
<script>
	var email_cookie = jQuery.cookie('email');
	var return_user = false;
		</script>
</head>
<body>
<div id="cspio-page">
<div id="cspio-content">
<h1 id="cspio-headline">Biip.no blir ny!</h1><div id="cspio-description"><p>Siden oppstarten i 2005 har Biip.no samlet over 500.000 medlemmer.</p>
<p>Nå blir Biip.no ny. Snart kan du logge inn og fortsette praten med nye og gamle venner. Hold deg oppdatert ved å melde deg på vårt nyhetsbrev nedenfor, så blir du førstemann til å bli varslet når relanseringen skjer.</p>
<p>Biip.no er startet av <a href="https://www.omegamedia.no" target="_blank">Omega Media</a>, som også står bak <a href="https://www.spill.no" target="_blank">Spill.no</a> og advokattjenesten <a href="https://www.advokatguiden.no" target="_blank">Advokatguiden.no</a>.</p>
<div id="advertisement"></div>
</div><form action="test" id="cspio-form" method="post">
<input id="cspio-ref" name="ref" type="hidden" value=""/>
<input id="cspio-href" name="href" type="hidden" value=""/>
<input id="cspio-lang" name="lang" type="hidden" value=""/>
<input id="cspio-page_id" name="page_id" type="hidden" value="1"/>
<input id="cspio-comment" name="comment" type="hidden" value=""/>
<div id="cspio-field-wrapper">
<div class="row">
<div class="col-md-12 seperate"><div class="input-group"><input class="form-control input-lg form-el" id="cspio-email" name="email" placeholder="Email" required="" type="email"/>
<span class="input-group-btn"><button class="btn btn-lg btn-primary form-el noglow" id="cspio-subscribe-btn" type="submit">Varsle meg</button></span></div></div>
</div>
</div>
</form>
<span id="cspio-privacy-policy-txt">We promise to never spam you.</span>
<script>
function send_request(){
		jQuery.ajax({
	    url: "https://www.biip.no/wp-admin/admin-ajax.php?action=seed_cspv5_subscribe_callback&_wpnonce=632eb3e37b",
	    dataType: "jsonp",
	    timeout: 30000,
	    data: jQuery("#cspio-form").serialize(),
	 
	    // Work with the response
	    success: function( response ) {
	        //console.log( response); // server response
	        //console.log( response.status);
	        // Sucess or Already Subscribed
	        if(response.status == '200' || response.status == '409'){
		        // Replace Content
		        		        jQuery("#cspio-sharebuttons").fadeOut(200);
		        
		        jQuery("#cspio-privacy-policy-txt,#cspio-description,#cspio-headline").fadeOut(200);
		        jQuery( "#cspio-form,#cspio-countdown,#cspio-progressbar" ).fadeOut(200).promise().done(function() {
		        	if(return_user){
		        		jQuery('#cspio-content').show();
		        	}
		        	jQuery( "#cspio-form" ).replaceWith( response.html ).hide().fadeIn();
		        });

		        // Set cookie if new user and user comes back
		        jQuery.cookie('email', jQuery("#cspio-email").val(), { expires: 365 } );
	    	}

	    	// Validation Error
	    	if(response.status == '400'){
	    		jQuery('#cspio-alert').remove();
	    		jQuery(response.html).hide().appendTo("#cspio-field-wrapper").fadeIn();
	    						//$('#cspio-field-wrapper').before(response.html).done().find('#cspio-alert').hide().fadeIn();
	    	}
	    	
	    	// Other error display html
	        if(response.status == '500'){
				alert(response.html);
	    	}

	    },
	    error: function(jqXHR, textStatus, errorThrown) {
        	alert(textStatus);
    	}
	});
}



jQuery( "#cspio-subscribe-btn" ).click(function( event ) {
	//if($("form")[0].checkValidity()){
	event.preventDefault();
	send_request()
	//}
});

// Read ref param if present
var ref = jQuery.url().param('ref');
jQuery("#cspio-ref").val(ref);
jQuery("#cspio-href").val(location.href);


if(return_user){
	jQuery('#cspio-email').val(email_cookie);
	jQuery('#cspio-name').val(email_cookie);
	jQuery('#cspio-comment').val('1');
	send_request();
}



// Disbale Button on Submit
jQuery(document)
.ajaxStart(function () {
    jQuery("#cspio-subscribe-btn").attr('disabled','disabled');
    jQuery("#cspio-email").addClass('cspio-loading');
})
.ajaxStop(function () {
    jQuery("#cspio-subscribe-btn").removeAttr('disabled');
    jQuery("#cspio-email").removeClass('cspio-loading');
});

</script>
<div id="cspio-progressbar">
<div class="progress ">
<div class="progress-bar" style="width: 100%;"><span>100%</span></div>
</div>
</div>
<script>
				jQuery(document).ready(function($){
					var endDate = new Date();
					endDate= new Date('2020', '11', '14', '21', '55', '00');
					//console.log(endDate);
					$('#cspio-countdown').countdown({
						labels: ['Years', 'Months', 'Weeks', 'Days', 'Hours', 'Minutes', 'Seconds'],
						labels1: ['Years', 'Months', 'Weeks', 'Day', 'Hour', 'Minute', 'Second'],
						until: endDate,
						timezone:1,
																		format: 'dHMS'
					});

					// $('#cspio-countdown').countdown('2015/11/18').on('update.countdown', function(event) {
					//    var $this = $(this).html(event.strftime('%S %!S:sekunde,sekunden;'
					//      + '<span class="countdown_section"><span class="countdown-amount">%-D</span> day%!d:singular,plural</span>'
					//      + '<span class="countdown_section"><span class="countdown-amount">%H</span> hr</span>'
					//      + '<span class="countdown_section"><span class="countdown-amount">%M</span> min</span>'
					//      + '<span class="countdown_section"><span class="countdown-amount">%S</span> </span> %S %!S:sekunde,sekunden;'));
					//  });
				});
				</script>
<div id="cspio-countdown"></div>
</div><!-- end of #seed-cspio-content -->
<div id="cspio-credit">
<span><a href="http://www.omegamedia.no" target="_blank">Omega Media AS</a></span>
</div>
</div>
<!-- Contact Form -->
<!-- FitVid -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fitvids/1.1.0/jquery.fitvids.min.js"></script>
<script>jQuery(document).ready(function($){$("#cspio-description,#cspio-thankyoumsg").fitVids();});</script>
<!-- Scripts -->
<script src="https://www.biip.no/wp-content/plugins/seedprod-coming-soon-pro-5/template/js/scripts.js"></script>
<!-- Animate -->
<script>
		if(return_user == false){
			jQuery('<img/>').attr('src', 'http://s3.amazonaws.com/static.seedprod.com/public-backgrounds/seedprod-pattern-1-compressor.png').load(function() {
			   jQuery(this).remove(); // prevent memory leaks as @benweet suggested
			   jQuery("#cspio-content").show().addClass('animated fadeIn');
			}).error(function() {
			   jQuery(this).remove(); // prevent memory leaks as @benweet suggested
			   jQuery("#cspio-content").show().addClass('animated fadeIn');	
			});
		}
		</script>
<script>
	function resize(){
			jQuery('head').append("<style id='form-style' type='text/css'></style>");
			jQuery('#form-style').html('.cspio .input-group-btn, .cspio .input-group{display:block;width:100%;}.cspio #cspio-subscribe-btn{margin-left:0;width:100%;display:block;}.cspio .input-group .form-control:first-child, .cspio .input-group-addon:first-child, .cspio .input-group-btn:first-child>.btn, .cspio .input-group-btn:first-child>.dropdown-toggle, .cspio .input-group-btn:last-child>.btn:not(:last-child):not(.dropdown-toggle) {border-bottom-right-radius: 4px;border-top-right-radius: 4px;}.cspio .input-group .form-control:last-child, .cspio .input-group-addon:last-child, .cspio .input-group-btn:last-child>.btn, .cspio .input-group-btn:last-child>.dropdown-toggle, .cspio .input-group-btn:first-child>.btn:not(:first-child) {border-bottom-left-radius: 4px;border-top-left-radius: 4px;}');
	}
	
	jQuery('#cspio-content').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
		var width = jQuery('#cspio-field-wrapper').width();
		//console.log(width);
		if(width < 350 && width != 0){
			resize();
		}
	}
	);

		</script>
<!-- Footer Scripts -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-3071052-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-3071052-1');
</script>
<script type="text/javascript">

window.addEventListener("load", function() { var a = document.getElementById("advertisement"); 
  if(location.pathname == "/") {
    a.innerHTML = '<p>Annonser:</p><div><a href="https://www.dagbladet.no/refinansiering"><img class="size-full wp-image-5" src="https://www.biip.no/wp-content/uploads/2020/03/crewmedia_320x250.jpg" alt="" width="300" height="250"></a></div><a href="https://www.klikk.no/refinansiering">Refinansiering</a><br /><a href="https://www.dagbladet.no/tjeneste/forbrukslan/">Forbrukslån</a><br><a href="https://www.abcnyheter.no/forbruksl%C3%A5n">Lånehjelp</a></br>';
  }
})

</script>
</body>
</html>
<!-- This page was generated by SeedProd.com | Learn more: http://www.seedprod.com -->
