<!DOCTYPE html>
<html amp="" lang="fr">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width,minimum-scale=1,initial-scale=1" name="viewport"/>
<title>احوال  الطقس الجزائر  Météo Algérie</title>
<link href="https://algerie-meteo.com/" rel="canonical"/>
<meta content="Nabil" name="Author"/>
<meta content="Météo, , Algérie, weather, plages, BMS, canicule, vent, temperature, Prévisions, Pays" name="keywords"/>
<meta content="Le site Algérie Météo احوال  الطقس الجزائر publie les prévisons avec bulletins métérologiques speciaux (BMS), des informations, des températures jours et nuits, de la météo des plages pour les villes cotieres de l'Algerie ainsi que les horaires Imsak et Iftar pour le jeune musulman." name="description"/>
<link href="images/favicon.jpg" rel="icon" type="image/png"/>
<script async="" custom-element="amp-ad" src="https://cdn.ampproject.org/v0/amp-ad-0.1.js"></script>
<script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "Article",
"description": "Le site Alg&eacute;rie M&eacute;t&eacute;o &#1575;&#1581;&#1608;&#1575;&#1604;  &#1575;&#1604;&#1591;&#1602;&#1587; &#1575;&#1604;&#1580;&#1586;&#1575;&#1574;&#1585; publie les pr&eacute;visons avec bulletins m&eacute;t&eacute;rologiques speciaux (BMS), des informations, des temp&eacute;ratures jours et nuits, de la m&eacute;t&eacute;o des plages pour les villes cotieres de l'Algerie ainsi que les horaires Imsak et Iftar pour le jeune musulman.",
        "headline": "&#1575;&#1581;&#1608;&#1575;&#1604;  &#1575;&#1604;&#1591;&#1602;&#1587; &#1575;&#1604;&#1580;&#1586;&#1575;&#1574;&#1585;  M&eacute;t&eacute;o Alg&eacute;rie",
"mainEntityOfPage": "https://algerie-meteo.com/",
       "author": {
      "@type": "Person",
      "name": "Nabil"
	  }, 
         "publisher": {
      "@type": "Organization",
      "name": "Algerie Meteo",
      "logo": {
        "@type": "ImageObject",
        "url": "https://algerie-meteo.com/images/logo.png",
        "width": 294,
        "height": 158
      }
    },
	"datePublished": "2010-06-28T22:16:51Z",
    "dateModified": "2021-01-14T01:02:41Z",
"image": {
     	    "@type": "ImageObject",
        	"url": "https://algerie-meteo.com/images/logo.jpg",
            "width": 640,
            "height": 360
    }
        
      }
    </script>
<style amp-boilerplate="">body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate="">body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<style amp-custom="">
html, body {
width:1000px;
margin:auto;
background-color:#FFFFFF;
font-family:Georgia, "Times New Roman", Times, serif, Georgia;
font-size: 14px;
text-align:center;
}
a
{
text-decoration: none;
color:#000000;
font-weight:bold;
}
p
{
text-align: center;
}
.top
{
background-image:url(images/top.jpg);
text-align:right;
width:1000px;
height:100px;
}

.main2 {width:660px; float:left; overflow:hidden; padding:0px; }
.pub {width:660px; overflow:hidden; padding-bottom:10px; }
.main {width:650px; float:left; color:#000000; background-color:#FFFFFF; padding:10px; }
.page {max-width:1000px; overflow:hidden; float:left; color:#000000; background-color:#FFFFFF; padding:10px; }
.menu {max-width:600px; overflow:hidden; float:left; color:#000000; background-color:#FFFFFF; padding:10px}
.menu2 {width:300px; overflow:hidden; color:#000000; background-color:#FFFFFF; padding:10px}
.aside {width:300px; overflow:hidden; float:right; color:#000000; background-color:#FFFFFF; padding:10px}



.text 
	{
	text-align: left;
	text-decoration: none;
	color: #000000;
	line-height:20px;
	}
	
.temp 
	{font-weight:bold; color:#FF0000;
	}
.temp2 
	{font-weight:bold; color: #0000FF;
	}
.temp22 
	{
	font-weight:bold;
	color: #FFFFFF;
	font-size:x-large;
	background-color:#CC0000;
	padding:5px;
	}
.liste 
	{ 
-moz-column-count: 2;
-webkit-column-count: 2;
-o-column-count: 2;
column-count: 2;
-moz-column-gap: 10px;
-webkit-column-gap: 10px;
-o-column-gap: 10px;
column-gap: 10px;
	text-align:left;
	padding:10px;
	}
.liste p { text-align:left}	

.text2 
	{
	font-weight:bold;
	}
.small
	{
	font-style: italic;
	}
.title 
	{
	font-face: Verdana, Arabic Transparent;
	text-align: center;
	text-decoration: none;
	color: #CC0000;
	font-weight:bold;
	padding:2px;
	
	}
h1
	{
	text-align: center;
	text-decoration: none;
	color: #006600;
	font-weight:bold;
	width:100%;
	padding:5px;
	margin:0;
	background-color:#F4F4F4;
	}
h2
	{
	text-align: center;
	text-decoration: none;
	color: #006600;
	font-weight:bold;
	width:100%;
	padding:5px;
	border-bottom: #006600 solid 1px;
	}
.title3
	{font-face: Verdana, Arabic Transparent;
	font-size: 18px;
	text-align: left;
	text-decoration: none;
	background-color:#009900;
	color:#FFFFFF;
	padding:5px;
	margin:0;
	font-weight:bold;
	float:left;
	width:100%;
	
	}
.title4
	{
	font-weight:bold;
	font-size: 16px;
	width:99%;
	padding:5px;
	margin:0;
	float:left;
	color:#006600;
	
	
	}
.description
	{
	font-weight:bold;
	font-size: 16px;
	padding:5px;
	margin:0;
	color:#006600;
	
	
	}
.bas {color:#000000; width:1000px; float:left; padding:5px; text-align:center; margin:0; font-size:14px; margin:0px; line-height:20px }
.actu {
margin:5px;
text-align:center;
float:left;
width:310px
}

.rond {margin:0; padding:0; border-radius:50%; object-fit:cover; width:150px; height:150px; margin-left: auto; margin-right: auto; background-image:url(images/day.jpg); 
}
.rond2 {margin:0; padding:0; border-radius:50%; object-fit:cover; width:150px; height:150px; margin-left: auto; margin-right: auto; background-image:url(images/night.jpg); 
}
.plage {margin:0; padding:0; border-radius:50%; object-fit:cover; width:150px; height:150px; margin-left: auto; margin-right: auto; background-image:url(images/plage.jpg); 
}
.detail1 {
margin-top: 20px;
border-top: black solid 1px;
display:table;
width: 320px;
background: #FCFCFC;
}
.detail2 {
width: 100%;
display: table-row;
}

.detail3 {
padding:10px;
margin-left:15px;
margin-right:15px;
display:table-cell;
width: 50%;
text-align:justify;
border-bottom: black solid 1px;


}

.daily {
text-align:center;
width:100%;
padding:0;
margin:0;
margin-bottom:5px;
border: 0; 
background-color: #FCFCFC;
display:table;

}
.daily2
{
display:table-cell;
text-align:center;
vertical-align:middle;
width:25%;
}

.scroll {
display:table;
-webkit-overflow-scrolling: touch;
-webkit-overflow-scrolling: auto;
background-color: #FCFCFC;
margin:0;
padding:0;
}
.hourly {
width:85px;
height:130px;
margin:0;
padding:0;
display:table-cell;
}

.imsak1 {
width:100%;
margin:0;
padding:0;
display:table; }
.imsak2 {
width:50%;
margin:0;
padding:0;
display:table-cell;
}
.table-plages {
width:314px;
height:154px;
text-align:center;
background: url(images/plages.jpg);
margin-left:auto; 
margin-right:auto;
}

.table-une {
width:400px;
height:500px;
text-align:center;
background: url(images/carte.jpg);
margin-left:auto; 
margin-right:auto;
}
nav {

	padding: 2px;
	width:100%;
	


}

#menu-icon {

	display: hidden;
	width: 40px;
	height: 40px;
	background: #CC0000 url(images/menuicon.png) center;

}

nav ul {

	list-style: none;
	float:right;
	margin:0;
	padding:0;

}

nav li {

	display: inline-block;
	padding: 10px;
	margin:0;
	background-color: #006600;
	margin:2px;
	font-weight:bold;

} 

nav li a {color:#FFFFFF;}
	
@media (max-width: 750px) {

html, body {
width:100%;
margin:0;
padding:0;
text-align:center;
font-family:Georgia, "Times New Roman", Times, serif;
	}
h2 {
font-size:18px;
}	
.top
{
width:100%;
overflow:hidden;
margin:0;
}

.text 
	{
	text-align: left;
	text-decoration: none;
	color: #000000;
	line-height:20px;
	}

.menu2
{
display:none;
}
.aside
{
display:none;
}

p amp-img {max-width:320px; max-height:300px;}

.actu {width:100%; overflow:hidden;}
.detail1 {width:100%; overflow:hidden; margin:0;}
.main {width:100%; float:left; color:#000000; background-color:#FFFFFF; overflow:hidden; margin:0;}
.main2 {width:100%; float:left; color:#000000; background-color:#FFFFFF; overflow:hidden; padding:0; margin:0;}
.pub {width:100%;}
.pub-top {width:100%;}
.bas {width:100%; background-color:#CCCCCC; color:#FFFFFF; overflow:hidden; padding:0; margin:0}
 #menu-icon {

		display:inline-block;

	}
	
	nav
	{
	float:right
	}

	nav ul, nav:active ul { 

		display: none;
		position: absolute;
		padding: 1px;
		background: #FFF;
		border: 1px solid #009900;
		right: 20px;
		top: 20px;
		width: 172px;
		z-index:10;

	}

	nav li {

		text-align: left;
		width: 160px;
		padding: 5px;
		margin: 1px;
		font-size:16px;

	}
	li a {

		color:#FFFFFF;

	}

	nav:hover ul {

		display: block;

	}
	
	}
amp-user-notification > div {
    padding: 1rem;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: #CCCCCC;
	
  }</style>
<script async="" src="https://cdn.ampproject.org/v0.js"></script>
<script async="" custom-element="amp-user-notification" src="https://cdn.ampproject.org/v0/amp-user-notification-0.1.js"></script>
<script async="" custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
</head>
<body>
<amp-analytics data-credentials="include" type="gtag">
<script type="application/json">
{
  "vars" : {
    "gtag_id": "UA-54559424-3",
    "config" : {
      "UA-54559424-3": { "groups": "default" }
    }
  }
}
</script>
</amp-analytics>
<amp-user-notification id="my-notification" layout="nodisplay">
<div>En poursuivant votre navigation, vous acceptez l'utilisation de Cookies pour personnaliser les annonces selon vos centres d'intéret. 
<a href="mentions_legales.html"> En savoir plus </a>
<button class="ampstart-btn caps ml1" on="tap:my-notification.dismiss"> Ok </button>
</div>
</amp-user-notification>
<div class="top">
<nav>
<a href="#" id="menu-icon"></a>
<ul><li><a href="villes.html" title="meteo par ville">Météo des villes </a> </li>
<li> <a href="plages.html" title="meteo plages">Météo des plages</a> </li>
<li> <a href="imsakiftar.html" title="imsak iftar">Horaires Imsak &amp; Iftar</a> </li>
<li> <a href="Satellite.html" title="images de Satellite">Satellite</a> </li>
</ul>
</nav>
</div>
<div class="main2">
<h3 align="center">توقعات حالة الطقس بالجزائر 
      <br/>
   Prévisions météo en Algérie</h3>
<h3 align="center"><span class="title">الخميس                        14/01/2021</span>
</h3>
<table align="center" border="0" cellpadding="0" cellspacing="2" class="table-une">
<tr>
<td></td>
<td></td>
<td></td>
<td rowspan="3"><a href="Oran-fr"><span class="temp">13</span>°<amp-img height="40" src="https://openweathermap.org/img/w/02n.png" width="40"></amp-img><br/><amp-img height="23" src="images/vert.png" width="15"></amp-img></a></td>
<td></td>
<td><a href="Alger-fr"><span class="temp">12</span>°<amp-img height="40" src="https://openweathermap.org/img/w/02n.png" width="40"></amp-img><br/><amp-img height="23" src="images/vert.png" width="15"></amp-img></a></td>
<td></td>
<td></td>
<td></td>
<td><a href="Annaba-fr"><span class="temp">13</span>°<amp-img height="40" src="https://openweathermap.org/img/w/04n.png" width="40"></amp-img><br/>
<amp-img height="23" src="images/vert.png" width="15"></amp-img></a></td>
<td></td>
<td></td>
</tr>
<tr>
<td></td>
<td></td>
<td></td>
<td rowspan="2"><a href="Tlemcen-fr"><span class="temp">13</span>°<amp-img height="40" src="https://openweathermap.org/img/w/01n.png" width="40"></amp-img> </a></td>
<td></td>
<td></td>
<td></td>
<td rowspan="2"><a href="Setif-fr"><span class="temp">6</span>°<amp-img height="40" src="https://openweathermap.org/img/w/02n.png" width="40"></amp-img> </a></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td></td>
<td></td>
<td><a href="Bechar-fr"><span class="temp">14</span>°<amp-img height="40" src="https://openweathermap.org/img/w/01n.png" width="40"></amp-img></a></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><a href="Ouargla-fr"><span class="temp">15</span>°<amp-img height="40" src="https://openweathermap.org/img/w/01n.png" width="40"></amp-img></a></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr><td></td>
<td><a href="Tindouf-fr"><span class="temp">17</span>°<amp-img height="40" src="https://openweathermap.org/img/w/01d.png" width="40"></amp-img></a></td>
<td> </td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td> </td>
<td><a href="Illizi-fr"><span class="temp">20</span>°<amp-img height="40" src="https://openweathermap.org/img/w/01n.png" width="40"></amp-img></a></td>
</tr>
<tr>
<td></td>
<td> </td>
<td></td>
<td></td>
<td></td>
<td><a href="Tamanrasset-fr"><span class="temp">21</span>°<amp-img height="40" src="https://openweathermap.org/img/w/01n.png" width="40"></amp-img></a></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td></td>
<td> </td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td></td>
<td> </td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td></td>
<td> </td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table>
<amp-ad data-ad-client="ca-pub-6929348806136288" data-ad-slot="8443027881" data-auto-format="rspv" data-full-width="" height="320" type="adsense" width="100vw">
<div overflow=""></div>
</amp-ad>
</div>
<div class="menu2">
<amp-ad data-ad-client="ca-pub-6929348806136288" data-ad-slot="3699651875" data-auto-format="rspv" data-full-width="" height="320" type="adsense" width="100vw">
<div overflow=""></div>
</amp-ad>
</div>
<div class="bas">
<hr/>
<span class="text"><a href="https://algerie-meteo.com">Météo Algérie</a>, © 2021 tous droits réservés | 
<a href="Contact.html">Contact</a> </span>
</div>
</body></html>