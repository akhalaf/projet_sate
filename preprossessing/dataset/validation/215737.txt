<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>BVM School</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="032C4626DDD67DD0E0C2A46F934FC06C" name="msvalidate.01"/>
<link href="images/logo/logo.png" rel="icon"/>
<meta content="BVM School" property="og:title"/>
<meta content="BVM School" property="og:url"/>
<meta content="Educationsite" property="og:type"/>
<meta content="images/logo/logo.png" property="og:image"/>
<meta content="#8373ce" name="theme-color"/>
<meta content="#8373ce" name="apple-mobile-web-app-status-bar-style"/>
<meta content="#8373ce" name="msapplication-navbutton-color"/>
<!-- mobile responsive meta -->
<!-- mobile responsive meta -->
<link href="css/common-style.css" rel="stylesheet"/>
<link href="css/responsive.css" rel="stylesheet"/>
<script src="https://use.fontawesome.com/23cb2284b9.js"></script>
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<meta content="TWT" name="author"/>
<meta content="Bhararth Vidhya Matriculation School, Arakkonam" name="description"/>
<meta content="BVM school, BVM School Arakkonam, Bhararth Vidhya Matriculation School, BVM AKM, BVM AJJ, BVM school akm" name="keyword"/>
</head>
<body class="home-one">
<div class="preloader"></div>
<header class="header clearfix">
<div class="main-header stricky bubble">
<div class="container">
<div class="logo pull-left">
<a href="index.php">
<img alt="BVM logo" src="images/logo/logo.png" style="width: 100px; height: 100px"/>BVM School
                </a>
</div>
<div class="nav-outer">
<div class="header-top">
<!-- <div class="contact">
                        <span class="icon fa fa-phone"></span>Call :  +(62) 123 456 7890
                    </div> -->
</div>
<nav class="mainmenu-area">
<div class="navbar" role="navigation">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="navbar-collapse collapse text-center">
<ul>
<li><a href="index.php">Home</a></li>
<li class="dropdown"><a href="about.php">About</a>
<ul class="submenu">
<li><a href="about.php#mo">Moto</a></li>
<li><a href="about.php#co">Corespondent</a></li>
</ul>
</li>
<li><a href="circular.php">Circular</a></li>
<li><a href="facilities.php">Facilities</a></li>
<li class="dropdown">
<a href="rules.php">Rules</a>
<ul class="submenu">
<li><a href="rules.php#pg">For Parents / Guardians</a></li>
<li><a href="rules.php#rr">For Students</a></li>
<li><a href="rules.php#ex">Exercise</a></li>
</ul>
</li>
<li class="dropdown">
<a href="#">Gallery</a>
<ul class="submenu">
<li><a href="gallery.php">Image Gallery</a></li>
<li><a href="vgallery.php">Video Gallery</a></li>
</ul>
</li>
<li><a href="application.php">Application</a></li>
<li><a href="contact.php">Contact</a></li>
</ul>
</div>
</div>
</nav>
</div>
</div>
</div>
</header>
<section class="rev_slider_wrapper">
<div class="carousel slide " data-ride="carousel" id="myCarousel">
<!-- Indicators -->
<div class="container">
<!-- Wrapper for slides -->
<div class="carousel-inner">
<div class="item active">
<div class="item active">
<img alt="Los Angeles" src="admin/images/583342SCHOOL PHOTO NEW - Copy-min.jpg" style="width:100%;"/>
<div class="carousel-caption">
<h3 style="color: white">Welcome to Bharath Vidhya Scoo</h3>
<p></p><p style="color: white"></p>
</div>
</div>
</div>
<div class="item">
<img alt="Los Angeles" src="admin/images/350231SCHOOL PHOTO NEW BACK-min.jpg" style="width:100%;"/>
<div class="carousel-caption">
<h3></h3><p style="color: white">.
          </p><p></p><p style="color: white"></p>
</div>
</div>
<div class="item">
<img alt="Los Angeles" src="admin/images/131018SCHOOL PHOTO NEW-min.jpg" style="width:100%;"/>
<div class="carousel-caption">
<h3></h3><p style="color: white">.
          </p><p></p><p style="color: white"></p>
</div>
</div>
<div class="item">
<img alt="Los Angeles" src="admin/images/405386IMG_20190815_092017 (2).jpg" style="width:100%;"/>
<div class="carousel-caption">
<h3></h3><p style="color: white">73 INDEPENDENCE DAY CELEBRATIO
          </p><p></p><p style="color: white">FLAG HOISTING IN CHIEF GUST</p>
</div>
</div>
<!-- Left and right controls -->
<a class="left carousel-control" data-slide="prev" href="#myCarousel">
<span class="glyphicon glyphicon-chevron-left"></span>
<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" data-slide="next" href="#myCarousel">
<span class="glyphicon glyphicon-chevron-right"></span>
<span class="sr-only">Next</span>
</a>
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#myCarousel"></li>
<li data-slide-to="1" data-target="#myCarousel"></li>
<li data-slide-to="2" data-target="#myCarousel"></li>
<li data-slide-to="3" data-target="#myCarousel"></li>
</ol>
</div>
</div>
<section class="about">
<div class="container">
<div class="row">
<div class="single-column col-md-6 col-sm-12">
<div class="wow fadeIn" data-wow-delay="0.5s" data-wow-duration="2s" data-wow-offset="0" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeIn;">
<div class="post-content">
<div class="section-title center">
<h2>Welcome TO <span>BVM</span> </h2>
</div>
<div class="text">
<p>We're a childcare centre with an engaging curriculum backed by qualified, experienced and passionate teachers! By learning about those of differing social, cultural and perspectives, young people </p>
</div>
<ul class="list">
<li>Educational field trips and school presentations</li>
<li>Comprehensive reporting on individual achievement</li>
<li>Individual attention in a small-class setting</li>
</ul>
<div class="link">
<a class="read-more" href="#">Get Involved</a>
<a class="read-more" href="#">Join Now</a>
</div>
</div>
</div>
</div>
<div class="single-column col-md-6 col-sm-12">
<div class="wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="2s" data-wow-offset="0" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeIn;">
<figure class="img-box">
<a href="#"><img alt="" src="images/resource/1.png"/></a>
</figure>
</div>
</div>
</div>
</div>
</section>
<section class="call-out">
<div class="container">
<div class="row">
<div class="column col-md-9 col-sm-12 col-xs-12">
<div class="text-left clearfix">
<h2>Join Now!</h2>
<p>Nothing is more important than your child’s well-being. Join our seminars <br/>and training and learn how to keep it.</p>
</div>
</div>
<div class="column col-md-3 col-sm-12 col-xs-12">
<div class="text-right padd-top-20">
<a class="theme-btn btn-style-one" href="application.php">Get Involved</a>
</div>
</div>
</div>
</div>
</section>
<section class="feature">
<div class="container">
<div class="section-title center pb-60">
<h2>Why I am a <span>successful student</span> ! </h2>
</div>
<div class="row">
<div class="column col-md-4 col-sm-12">
<div class="item right">
<div class="icon"><span class="fa fa-child"></span></div>
<h4><a href="#">I am a good student and study well in my class</a></h4>
<!-- <p>Lorem ipsum dolor sit amet cotetur adipisicing elit mod tempor incididuntut labore</p> -->
</div>
<div class="item right">
<div class="icon"><span class="fa fa-child"></span></div>
<h4><a href="#">My power of concentration also its increasing, My memory- power is good and I am able to recall with confidence in myself in all my exam</a></h4>
</div>
<div class="item right">
<div class="icon"><span class="fa fa-child"></span></div>
<h4><a href="#">I can recall well at exam</a></h4>
</div>
</div>
<div class="column col-md-4 col-sm-12">
<div class="wow fadeIn" data-wow-delay="0.5s" data-wow-duration="2s" data-wow-offset="0" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeIn;">
<figure class="img-box ">
<a href="#"><img alt="" src="images/resource/2.png"/></a>
</figure>
</div>
</div>
<div class="column col-md-4 col-sm-12">
<div class="item left">
<div class="icon"><span class="fa fa-child"></span></div>
<h4><a href="#">I understand &amp; remember well</a></h4>
</div>
<div class="item left">
<div class="icon"><span class="fa fa-child"></span></div>
<h4><a href="#">Day by day in every way. I am becoming better and better, My capacity to understand is excellent</a></h4>
</div>
<div class="item left">
<div class="icon"><span class="fa fa-child"></span></div>
<h4><a href="#">So I am a successful student</a></h4>
</div>
</div>
</div>
</div>
<div class="see-more">
<div class="container">
<div class="text-center">
<h2>See Our school Special Features!</h2>
<div class="link">
<a class="theme-btn btn-style-one" href="facilities.php">Get Involved</a>
</div>
</div>
</div>
</div>
</section>
<section class="cource">
<div class="container">
<div class="row">
<div align="center" class="container">
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="section-title center pb-60">
<h2><span style="color:white">Circular</span> </h2>
</div>
<br/>
<marquee align="center" behavior="scroll" direction="up" height="200" id="test3" onmouseout="document.all.test3.start()" onmouseover="document.all.test3.stop()" scrollamount="2" scrolldelay="100" style="width: 50%;text-align: center">
</marquee>
<br/>
</div>
</div>
<br/>
<br/>
<br/>
<br/>
<br/>
<a class="btn-style-one center" href="circular.php" id="btn_submit" name="submit" type="submit">View all</a>
</div>
</div>
</div>
</section>
<section class="team">
<div class="container">
<div class="section-title center pb-60">
<h2>our team <span>member</span></h2>
</div>
<div class="team-list">
<div class="row">
<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.5s" data-wow-duration="2s" data-wow-offset="0" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeIn;">
<div class="item">
<div class="img-holder">
<figure><a href="#"><img alt="Awesome Image" src="images/team/1.jpg"/></a></figure>
<div class="content">
<h2><a href="#">S. Nagarajan</a></h2>
<p>Corespondent</p>
</div>
</div>
<div class="overlay">
<div class="inner">
<div class="content">
<h2><a href="#">S. Nagarajan</a></h2>
<p>Corespondent</p>
</div>
<ul class="social">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-instagram"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.6s" data-wow-duration="2s" data-wow-offset="0" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeIn;">
<div class="item">
<div class="img-holder">
<figure><a href="#"><img alt="Awesome Image" src="images/team/2.jpg"/></a></figure>
<div class="content">
<h2><a href="#">N.Tamilselvi </a></h2>
<p>Assistant Corepondent</p>
</div>
</div>
<div class="overlay">
<div class="inner">
<div class="content">
<h2><a href="#">N.Tamilselvi </a></h2>
<p>Assistant Corepondent</p>
</div>
<ul class="social">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-instagram"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.8s" data-wow-duration="2s" data-wow-offset="0" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeIn;">
<div class="item">
<div class="img-holder">
<figure><a href="#"><img alt="Awesome Image" src="images/team/4.jpg"/></a></figure>
<div class="content">
<h2><a href="#">R.Sethuraj </a></h2>
<p>Principal</p>
</div>
</div>
<div class="overlay">
<div class="inner">
<div class="content">
<h2><a href="#">R.Sethuraj </a></h2>
<p>Principal</p>
</div>
<ul class="social">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-instagram"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.7s" data-wow-duration="2s" data-wow-offset="0" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeIn;">
<div class="item">
<div class="img-holder">
<figure><a href="#"><img alt="Awesome Image" src="images/team/3.jpg"/></a></figure>
<div class="content">
<h2><a href="#">R.Vijayalakshmi</a></h2>
<p>Head mistress</p>
</div>
</div>
<div class="overlay">
<div class="inner">
<div class="content">
<h2><a href="#">R.Vijayalakshmi</a></h2>
<p>Head mistress</p>
</div>
<ul class="social">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-instagram"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="two-column">
<div class="container">
<div class="row">
<div class="col-md-8 col-sm-12 tab-column">
<div class="section-title center">
<h2>Our<span> Activities</span></h2>
</div>
<div class="outer-box">
<ul class="tab-list" role="tablist">
<li class="item active hvr-bubble-bottom" data-tab-name="cur">
<a aria-controls="cur" class="clearfix" data-toggle="tab" href="#cur" role="tab" style="text-decoration: none">
                                Curriculum
                            </a>
</li>
<li class="item hvr-bubble-bottom" data-tab-name="sub">
<a aria-controls="sub" class="clearfix" data-toggle="tab" href="#sub" role="tab" style="text-decoration: none">
                                Subjects 
                            </a>
</li>
<li class="item hvr-bubble-bottom" data-tab-name="cc">
<a aria-controls="cc" class="clearfix" data-toggle="tab" href="#cc" role="tab" style="text-decoration: none">
                                Co-curricular 
                            </a>
</li>
<li class="item hvr-bubble-bottom" data-tab-name="ec">
<a aria-controls="ec" class="clearfix" data-toggle="tab" href="#ec" role="tab" style="text-decoration: none">
                                Extra-curricular 
                            </a>
</li></ul>
</div>
<div class="content-column tab-content">
<div class="inner-box tab-pane fade in active " id="cur">
<div class="content">
<h2>Let the Learning Begin </h2>
<p># Our school’s medium of instruction is English. Every school must be satisfying the needs of the students. According to that we provide opportunity for the development of all round personality of children, variety of subjects like computer science, general knowledge, sports and games are learning right from the 1st standard <br/> It will be operate to all the areas around the school especially remote villages </p>
</div>
</div>
<div class="inner-box tab-pane fade " id="sub">
<div class="content">
<h2>Let the Learning Begin </h2>
<p># English<br/># Tamil  <br/># Mathematics <br/># Social <br/># Science </p>
</div>
</div>
<div class="inner-box tab-pane fade " id="cc">
<div class="content">
<h2>Let the Learning Begin </h2>
<p># Abacus<br/># Spoken English  <br/># Mathematics <br/># Hindi (special subject)</p>
</div>
</div>
<div class="inner-box tab-pane fade " id="ec">
<div class="content">
<h2>Let the Learning Begin </h2>
<p>#Karate<br/># Music  <br/># Yoga</p>
</div>
</div>
</div>
</div>
<div class="col-md-4 col-sm-12 testimonial">
<div class="section-title center">
<h2>What The <span>Parents Say</span></h2>
</div>
<div class="testimonial-slide">
<div class="item center">
<img alt="" src="images/team/t1.jpg"/>
<div class="img-ifo">
<div class="text">
<p>Our teachers graduated from differnt Universities with as different subjects but its all included in Education M</p>
</div>
<div class="rating">
<span class="star fa fa-star"></span>
<span class="star fa fa-star"></span>
<span class="star fa fa-star"></span>
<span class="star fa fa-star"></span>
<span class="star fa fa-star-o"></span>
</div>
<div class="link">
<a class="btn-style-one" href="#">Ellina Gilbert</a>
</div>
</div>
</div>
<div class="item center">
<img alt="" src="images/team/t1.jpg"/>
<div class="img-ifo">
<div class="text">
<p>Our teachers graduated from differnt Universities with as different subjects but its all included in Education M</p>
</div>
<div class="rating">
<span class="star fa fa-star"></span>
<span class="star fa fa-star"></span>
<span class="star fa fa-star"></span>
<span class="star fa fa-star"></span>
<span class="star fa fa-star"></span>
</div>
<div class="link">
<a class="btn-style-one" href="#">Ellina Gilbert</a>
</div>
</div>
</div>
<div class="item center">
<img alt="" src="images/team/t1.jpg"/>
<div class="img-ifo">
<div class="text">
<p>Our teachers graduated from differnt Universities with as different subjects but its all included in Education M</p>
</div>
<div class="rating">
<span class="star fa fa-star"></span>
<span class="star fa fa-star"></span>
<span class="star fa fa-star"></span>
<span class="star fa fa-star"></span>
<span class="star fa fa-star"></span>
</div>
<div class="link">
<a class="btn-style-one" href="#">Ellina Gilbert</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="footer">
<div class="footer-upper">
<div class="container">
<div class="row">
<div class="col-md-4 col-sm-6 col-xs-12 column">
<div class="item center">
<div class="icon">
<span class="fa fa-phone "></span>
</div>
<div class="content">
<h4><a href="#">Phone number</a></h4>
<p>+(91) 812 444 2118</p>
<p>+(91) 978 789 6427</p>
<p>+(91) 950 078 9148</p>
</div>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 column">
<div class="item center">
<div class="icon">
<span class="fa fa-bandcamp fa-spin "></span>
</div>
<div class="content">
<h4><a href="#">Address</a></h4>
<p>Nemili road, Senthamangalam<br/>Arakkonam Taulk, Vellore - 631 051</p>
</div>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 column">
<div class="item center">
<div class="icon">
<span class="fa fa-envelope"></span>
</div>
<div class="content">
<h4><a href="#">Email id</a></h4>
<p><a class="__cf_email__" data-cfemail="e0828881928194889689848899818d93a0878d81898cce838f8d" href="/cdn-cgi/l/email-protection">[email protected]</a></p>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="footer-bottom">
<div class="footer-bottom-bg">
<div class="container">
<div class="pull-left">
<figure><a href="#"><img alt="" src="images/logo/logo.png" style="width: 100px; height: 100px"/></a></figure>
</div>
<div class="pull-right">
<div class="menu">
<ul class="clearfix">
<li><a href="index.php">Home</a></li>
<li><a href="about.php">About</a></li>
<li><a href="circular.php">Circular</a></li>
<li><a href="facilities.php">Facilities</a></li>
<li><a href="rules.php">Rules</a></li>
<li><a href="gallery.php">Image Gallery</a></li>
<li><a href="vgallery.php">Video Gallery</a></li>
<li><a href="application.php">Application</a></li>
<li><a href="contact.php">Contact</a></li>
</ul>
</div>
<div class="copy-right">
                        Copyright © <a href="#"><span>BVM</span></a> 2016
                    </div>
</div>
</div>
</div>
</div>
</section>
<!--Scroll to top-->
<div class="scroll-to-top"><i class="fa fa-long-arrow-up"></i></div>
<!-- Gallery Box -->
<!-- jQuery js -->
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/jquery/jquery-1.12.3.min.js"></script>
<!-- bootstrap js -->
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<!-- jQuery ui js -->
<script src="assets/jquery-ui-1.11.4/jquery-ui.js"></script>
<!-- wow js -->
<script src="assets/wow.js"></script>
<!-- owl carousel js -->
<script src="assets/owl.carousel-2/owl.carousel.min.js"></script> <!-- jquery.bxslider js -->
<script src="assets/jquery.bxslider/jquery.bxslider.min.js"></script>
<!-- jQuery validation -->
<script src="assets/jquery-validation/dist/jquery.validate.min.js"></script>
<!-- gmap.js helper -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRvBPo3-t31YFk588DpMYS6EqKf-oGBSI"></script>
<!-- gmap.js -->
<script src="assets/gmaps.js"></script>
<!-- mixit u -->
<script src="assets/jquery.mixitup.min.js"></script>
<script src="assets/isotope.pkgd.min.js"></script>
<script src="assets/jquery.countdown.min.js"></script>
<script src="assets/masterslider/masterslider.js"></script>
<script src="assets/SmoothScroll.js"></script>
<!-- revolution slider js -->
<script src="assets/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="assets/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="assets/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="assets/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="assets/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="assets/Polyglot-Language-Switcher-master/js/jquery.polyglot.language.switcher.js"></script>
<script src="assets/fancyapps-fancyBox/source/jquery.fancybox.pack.js"></script>
<script src="assets/scrollbar.js"></script>
<script src="js/script.js"></script>
<script type="text/javascript">$('#ban').modal('show')</script>
</div></section></body>
</html>
