<!DOCTYPE html>
<!--[if (lt IE 9) ]> <html class="ie"> <![endif]--><!--[if (gt IE 9)|!(IE)]><!--><html class="" lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="astro chart, free astro chart, astro birth chart, astrology chart, natal charts, my horoscope, composite, synastry, celebrity astrology, horoscopes, chart generator, free astrology software, create birthchart, create astrology chart, my horoscope, astro horoscope free " name="keywords"/>
<meta content="a717c29f665e87e5e215d3babc53acec" name="p:domain_verify"/>
<meta content="Astro-Charts is the home of beautiful, free astrology charts. Create your free birth, synastry, composite, transits, celebrity charts. Using our tools you can hide/show planets and asteroids, choose a house system, customize orbs, show declinations, sidereal charts and more..." name="description"/>
<meta content="True" name="HandheldFriendly"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="all" name="robots"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>Beautiful Free Astrology Charts | Astro-Charts </title>
<link href="/client/common/css/base.min.css?v=6.58" rel="stylesheet" type="text/css"/>
<link href="/client/homepage/css/homepage.css?v=6.58" rel="stylesheet" type="text/css"/>
<link href="/client/common/images/site/apple-touch-icon-precomposed.png" rel="apple-touch-icon-precomposed"/>
<link href="/client/common/images/site/apple-touch-icon-precomposed.png" rel="apple-touch-icon-120x120-precomposed"/>
<link href="/client/common/images/site/apple-touch-icon.png" rel="apple-touch-icon"/>
<link href="/client/common/images/site/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/client/common/images/site/favicon.ico" rel="icon" type="image/x-icon"/>
<meta content="1574765942741085" property="fb:app_id"/>
<meta content="website" property="og:type"/>
<meta content="Astro Charts" property="og:title"/>
<meta content="Create Beautiful, Free Astrology Charts: Birth, Composite, Synastry, Transits, Celebrity Charts and more" property="og:description"/>
<meta content="http://astro-charts.com/client/common/images/site/astro-charts-img.png" property="og:image"/>
</head>
<body class="home page page-template-default right-sidebar">
<div class="hfeed site wrap" id="page">
<header class="noprint site-header default-header" id="masthead" itemscope="itemscope" itemtype="http://schema.org/WPHeader" role="banner">
<!-- logo and desc -->
<div class="site-branding">
<a class="astro-cell-sheet site-logo" href="/" rel="home">
</a>
<p class="site-description" itemprop="description"></p>
</div>
<div class="secondary-navigation">
<span class="user_block sign-up-btn">
<a href="/user/signup/">Sign up</a>
</span>
<span class="user_block log-in-btn">
<a href="/user/login/">Log in</a>
</span>
<span class="nav-divider"></span>
<span class="user_block cart-img">
<a href="/shop/cart/">
<span aria-label="your cart" class="astro-cell-sheet cart-icon" id="cartarea" role="img"></span>
<div class="num">0</div>
</a>
</span>
</div>
<!-- .secondary-navigation -->
<a data-close-text="Close" data-open-text="Menu&lt;i class='fa fa-reorder'&gt;" href="" id="site-mobile-navigation-toggle">Menu<i class="fa fa-reorder"></i></a>
<nav class="main-navigation" id="site-navigation" itemscope="itemscope" role="navigation">
<h3 class="semantic">Site Menu</h3>
<div class="header-navigation">
<ul class="sf-menu" id="menu-navigation" style="touch-action: pan-y;">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home current-menu-item"><a href="/">Home</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category megamenu-parent ">
<a href="/tools/new/birthchart">Create Chart</a>
<ul class="sub-menu" id="submenu1">
<li class="menu-item menu-item-type-post_type menu-item-object-post"><a href="/tools/new/birthchart">Birth Chart</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post"><a href="/tools/new/synastry">Synastry Chart</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post"><a href="/tools/new/composite">Composite Chart</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post"><a href="/tools/new/transits">Personal Transits</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post"><a href="/tools/widget">Free Astrology Widgets</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category "><a href="/shop/">Shop</a>
</li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category megamenu-parent "><a href="/persons">Famous People</a>
<ul class="sub-menu" id="submenu2">
<li class="menu-item menu-item-type-post_type menu-item-object-post"><a href="/persons">By Sun Sign</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post"><a href="/persons/aspect">By Aspect</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post"><a href="/persons/combos/sunmoon">By Sun 
                      &amp; Moon Combination</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post"><a href="/persons/planet">By Planetary Placement</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post"><a href="/persons/pattern">By Chart Patterns</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post"><a href="/persons/search">Advanced Search</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category "><a href="/planetary_transits">Planetary Transits</a>
<ul class="sub-menu" id="submenu5">
<li class="menu-item menu-item-type-post_type menu-item-object-post"><a href="/planetary_transits">Planetary Transits</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post"><a href="/chart-of-moment">Chart of the Moment</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category ">
<a href="/blog">Blog</a>
<div class="megamenu no-mobile-megamenu" id="submenu3">
<ul class="subnav-posts">
<li>
<a class="menu-item-with-img" href="/blog/2018/sunchiron-aspects-synastry/"><img alt="Sun / Chiron Aspects in Synastry" class="lazyload" data-height="158" data-src="https://astro-charts.com/client/attachments/2018/sunchiron-aspects-synastry/fogfire248x158.jpg" data-width="248" height="158" width="248"/></a>
<a href="/blog/2018/sunchiron-aspects-synastry/">Sun / Chiron Aspects in Synastry</a>
</li>
<li>
<a class="menu-item-with-img" href="/blog/2018/how-customize-orbs-your-charts/"><img alt="How to customize orbs in Astro-Charts" class="lazyload" data-height="158" data-src="https://astro-charts.com/client/attachments/2018/how-customize-orbs-your-charts/orb_248x158.jpg" data-width="248" height="158" width="248"/></a>
<a href="/blog/2018/how-customize-orbs-your-charts/">How to customize orbs in Astro-Charts</a>
</li>
<li>
<a class="menu-item-with-img" href="/blog/2018/meaning-mc-libra/"><img alt="The Meaning of MC in Libra" class="lazyload" data-height="158" data-src="https://astro-charts.com/client/attachments/2018/meaning-mc-libra/libra248x158.jpg" data-width="248" height="158" width="248"/></a>
<a href="/blog/2018/meaning-mc-libra/">The Meaning of MC in Libra</a>
</li>
<li>
<a class="menu-item-with-img" href="/blog/2018/interpretations-chart-shapes-natal-chart/"><img alt="Interpretations for Chart Shapes in the Natal Chart" class="lazyload" data-height="158" data-src="https://astro-charts.com/client/attachments/2018/interpretations-chart-shapes-natal-chart/shape248x158.jpg" data-width="248" height="158" width="248"/></a>
<a href="/blog/2018/interpretations-chart-shapes-natal-chart/">Interpretations for Chart Shapes in the Natal Chart</a>
</li>
<li>
<a class="menu-item-with-img" href="/blog/2018/meaning-mutual-reception-and-sole-dispositors-nata/"><img alt="The Meaning of Mutual Reception and Sole Dispositors in a Natal Birth Chart" class="lazyload" data-height="158" data-src="https://astro-charts.com/client/attachments/2018/meaning-mutual-reception-and-sole-dispositors-nata/ch248x158.jpg" data-width="248" height="158" width="248"/></a>
<a href="/blog/2018/meaning-mutual-reception-and-sole-dispositors-nata/">The Meaning of Mutual Reception and Sole Dispositors in a Natal Birth Chart</a>
</li>
</ul>
</div>
</li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category "><a href="/feedback">Feedback</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category mobile-menu-item"><a href="/shop/cart/">Cart</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category mobile-menu-item"><a href="/user/login/">Log in</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category mobile-menu-item"><a href="/user/signup/">Sign up</a></li>
</ul>
</div>
</nav>
<!-- #site-navigation -->
</header>
<div class="site-content" id="content">
<div class="content-area" id="primary">
<!--<div>
      <img src="/client/common/images/site/print.png" />
    </div>-->
<div class="hp_birthchart">
<div class="hp_banner">
<img src="/client/common/images/site/hp-stamp.svg"/>
<div><h1>Welcome to the Home of Beautiful Astrology Charts</h1>
<p>Create your free, personalized, and highly customizable birth chart (natal chart) by filling in the form below. Using our tools you can hide/show planets and asteroids, choose a house system, customize orbs, show declinations, sidereal charts and more...</p></div>
</div>
<div class="hp_form new-form">
<form action="/tools/birth_chart/" id="add_chart_form" method="get">
<div class="error"></div>
<div class="form-group">
<label for="name">My name is:</label>
<input autocomplete="new-password" class="form-control input name" name="name" placeholder=""/>
</div>
<div class="form-group">
<label for="geolocation">I was born in:</label>
<input autocomplete="off" class="form-control input geolocation" placeholder="City, state, country"/>
</div>
<input class="hidden_input pk" name="pk" type="hidden"/>
<input class="hidden_input lat" name="lat" type="hidden"/>
<input class="hidden_input lng" name="lng" type="hidden"/>
<input class="hidden_input place" name="place" type="hidden"/>
<input class="hidden_input date" name="date" type="hidden"/>
<div class="dt-group form-group">
<label for="date of birth">My date of birth is:</label>
<div class="date_time_group">
<div class="date-group">
<div class="form_set">
<input autocomplete="new-password" class="form-control input month center-align" maxlength="2" pattern="\d*" placeholder="MM"/>
</div><span class="slash">/</span>
<div class="form_set">
<input autocomplete="new-password" class="form-control input day center-align" maxlength="2" pattern="\d*" placeholder="DD"/>
</div><span class="slash">/</span>
<div class="form_set">
<input autocomplete="new-password" class="form-control input year center-align" maxlength="4" pattern="\d*" placeholder="YYYY"/>
</div>
</div>
<div class="time-group">
<div class="form_set">
<input autocomplete="new-password" class="form-control input hour center-align" maxlength="2" pattern="\d*" placeholder="hh"/>
</div><span class="slash">:</span>
<div class="form_set form_set_min">
<input autocomplete="new-password" class="form-control input min center-align" maxlength="2" pattern="\d*" placeholder="mm"/>
</div>
<div class="form_set ampm_form_set">
<label class="radio form-label">
<input class="timeRadio1 custom-radio" data-radiocheck-toggle="radio" data-toggle="radio" name="timeRadio" type="radio" value="am"/>
             AM
           </label>
<label class="radio form-label">
<input checked="" class="timeRadio2 custom-radio" data-radiocheck-toggle="radio" data-toggle="radio" name="timeRadio" type="radio" value="pm"/>
             PM
           </label>
</div>
</div>
</div>
</div>
<div class="form-group">
<button class="btn btn-med" id="submit_chart">Create Chart</button>
</div>
</form>
</div>
<img class="hp_icon" src="/client/common/images/site/hp-abstract.svg"/>
</div>
<div class="section-title"><h4>Create your Free Chart</h4></div>
<div id="tools-section-4col">
<div class="tool-hp">
<a href="/tools/new/birthchart/">
<span aria-label="birth chart" class="tools-icon icon-smaller" role="img">
<img src="/client/common/images/site/birth-icon.svg"/>
</span>
<h3>Birth Chart</h3>
<p>Discover your planetary positions, aspects, aspect patterns, chart shapes, houses, and more.</p>
</a>
<a class="more-link" href="/tools/new/birthchart/">Create chart</a>
<span class="icon fa fa-angle-right"></span>
</div>
<div class="tool-hp">
<a href="/tools/new/synastry/">
<span aria-label="synastry chart" class="tools-icon" role="img">
<img src="/client/common/images/site/syn-icon.svg"/>
</span>
<h3>Synastry</h3>
<p>Understand the astrological compatibilty between two people by overlaying their birth charts.</p>
</a>
<a class="more-link" href="/tools/new/synastry/">Create chart</a>
<span class="icon fa fa-angle-right"></span>
</div>
<div class="tool-hp">
<a href="/tools/new/composite/">
<span aria-label="composite chart" class="tools-icon" role="img">
<img class="tool-hp-comp" src="/client/common/images/site/comp-icon.svg"/>
</span>
<h3>Composite</h3>
<p>Get insights into the dynamics of a relationship between two people by calculating the midpoints between their birth charts.</p>
</a>
<a class="more-link" href="/tools/new/composite/">Create chart</a>
<span class="icon fa fa-angle-right"></span>
</div>
<div class="tool-hp">
<a href="/tools/new/transits/">
<span aria-label="transit chart" class="tools-icon icon-smaller" role="img">
<img src="/client/common/images/site/trans-icon.svg"/>
</span>
<h3>Transits</h3>
<p>See what energy patterns are unfolding for you, due to the movements of the stars, for the upcoming month.</p>
</a>
<a class="more-link" href="/tools/new/transits/">Create chart</a>
<span class="icon fa fa-angle-right"></span>
</div>
</div>
<div class="post-modules wdgt vertical highlight">
<div class="post-modules-highlight">
<article class="post-module post-module-2cols" itemscope="itemscope" itemtype="http://schema.org/Article">
<a class="post-thumbnail" href="/blog/2018/meaning-elements-within-birth-chart-air-earth-fire/">
<img alt="thumbnail" class="wp-post-image lazyload" data-src="https://astro-charts.com/client/attachments/2018/meaning-elements-within-birth-chart-air-earth-fire/elements650x433.jpg" itemprop="image"/>
</a>
<div class="post-module-content">
<header class="entry-header">
<div class="entry-cats">
<a class="milkit_cat" href="/blog/category/charts" rel="category tag">Charts</a>
</div>
<!-- .entry-meta -->
<h2 class="entry-title" itemprop="name"><a href="/blog/2018/meaning-elements-within-birth-chart-air-earth-fire/" itemprop="url" rel="bookmark">The Meaning of the Elements within a Birth Chart: Air, Earth, Fire, Water</a></h2>
</header>
<!-- .entry-header -->
<div class="entry-summary" itemprop="text">
<p>The meaning of elements (fire, air, earth, and water) within your birth chart, including excessive and deficient elements within the natal chart.</p>
</div>
<!-- .entry-summary -->
<footer class="entry-footer">
<div class="author-date">
<span class="posted-on ">Published <a href="" rel="bookmark"><time class="entry-date published" itemprop="datePublished">Aug. 23, 2018</time></a></span>
<span class="byline ">By <span class="author vcard" itemprop="author" itemscope="itemscope">
<a>Taya</a>
</span></span>
</div>
<!-- .author-date -->
</footer>
<!-- .entry-footer -->
</div>
<!-- .post-module-content -->
</article>
<article class="post-module post-module-2cols" itemscope="itemscope" itemtype="http://schema.org/Article">
<a class="post-thumbnail" href="/blog/2018/navigating-saturn-return/">
<img alt="thumbnail" class="wp-post-image lazyload" data-src="https://astro-charts.com/client/attachments/2020/navigating-saturn-return/column_650x433.jpg" itemprop="image"/>
</a>
<div class="post-module-content">
<header class="entry-header">
<div class="entry-cats">
<a class="milkit_cat" href="/blog/category/transits" rel="category tag">transits</a>
</div>
<!-- .entry-meta -->
<h2 class="entry-title" itemprop="name"><a href="/blog/2018/navigating-saturn-return/" itemprop="url" rel="bookmark">Navigating the Saturn Return</a></h2>
</header>
<!-- .entry-header -->
<div class="entry-summary" itemprop="text">
<p>The Saturn Return lights up a path for the individual to find the keys to their own personal empire: in their own ...</p>
</div>
<!-- .entry-summary -->
<footer class="entry-footer">
<div class="author-date">
<span class="posted-on ">Published <a href="" rel="bookmark"><time class="entry-date published" itemprop="datePublished">Aug. 18, 2018</time></a></span>
<span class="byline ">By <span class="author vcard" itemprop="author" itemscope="itemscope">
<a>Taya</a>
</span></span>
</div>
<!-- .author-date -->
</footer>
<!-- .entry-footer -->
</div>
<!-- .post-module-content -->
</article>
</div><!-- .post-modules-highlight -->
</div>
<div class="section-title"><h4>Blog</h4></div>
<div class="post-modules wdgt vertical">
<article class="post-module post-module-4cols vertical" itemscope="itemscope" itemtype="http://schema.org/Article">
<a class="post-thumbnail" href="/blog/2018/sunchiron-aspects-synastry/">
<img alt="thumbnail" class="wp-post-image lazyload" data-src="https://astro-charts.com/client/attachments/2018/sunchiron-aspects-synastry/fogfire440x293.jpg" itemprop="image"/>
</a>
<div class="post-module-content">
<header class="entry-header">
<div class="entry-cats">
<a class="milkit_cat" href="/blog/category/synastry" rel="category tag">Synastry</a>
</div>
<!-- .entry-meta -->
<h2 class="entry-title" itemprop="name"><a href="/blog/2018/sunchiron-aspects-synastry/" itemprop="url" rel="bookmark">Sun / Chiron Aspects in Synastry</a></h2>
</header>
<!-- .entry-header -->
<div class="entry-summary" itemprop="text">
<p>The interpretation for Sun/Chiron Aspects within a Synastry relationship chart</p>
</div>
<!-- .entry-summary -->
<footer class="entry-footer">
<div class="author-date">
<span class="posted-on ">Published <a href="" rel="bookmark"><time class="entry-date published" itemprop="datePublished">Dec. 31, 2018</time></a></span>
<span class="byline ">By <span class="author vcard" itemprop="author" itemscope="itemscope">
<a>Taya</a>
</span></span>
</div>
<!-- .author-date -->
</footer>
<!-- .entry-footer -->
</div>
<!-- .post-module-content -->
</article>
<article class="post-module post-module-4cols vertical" itemscope="itemscope" itemtype="http://schema.org/Article">
<a class="post-thumbnail" href="/blog/2018/how-customize-orbs-your-charts/">
<img alt="thumbnail" class="wp-post-image lazyload" data-src="https://astro-charts.com/client/attachments/2018/how-customize-orbs-your-charts/orb_440x293.jpg" itemprop="image"/>
</a>
<div class="post-module-content">
<header class="entry-header">
<div class="entry-cats">
<a class="milkit_cat" href="/blog/category/charts" rel="category tag">Charts</a>
</div>
<!-- .entry-meta -->
<h2 class="entry-title" itemprop="name"><a href="/blog/2018/how-customize-orbs-your-charts/" itemprop="url" rel="bookmark">How to customize orbs in Astro-Charts</a></h2>
</header>
<!-- .entry-header -->
<div class="entry-summary" itemprop="text">
<p>Learn how to customize orb degrees for aspects (ie conjunction, sextile, square...) on your charts in Astro-Charts</p>
</div>
<!-- .entry-summary -->
<footer class="entry-footer">
<div class="author-date">
<span class="posted-on ">Published <a href="" rel="bookmark"><time class="entry-date published" itemprop="datePublished">Dec. 31, 2018</time></a></span>
<span class="byline ">By <span class="author vcard" itemprop="author" itemscope="itemscope">
<a>Taya</a>
</span></span>
</div>
<!-- .author-date -->
</footer>
<!-- .entry-footer -->
</div>
<!-- .post-module-content -->
</article>
<article class="post-module post-module-4cols vertical" itemscope="itemscope" itemtype="http://schema.org/Article">
<a class="post-thumbnail" href="/blog/2018/meaning-mc-libra/">
<img alt="thumbnail" class="wp-post-image lazyload" data-src="https://astro-charts.com/client/attachments/2018/meaning-mc-libra/libra440x293.jpg" itemprop="image"/>
</a>
<div class="post-module-content">
<header class="entry-header">
<div class="entry-cats">
<a class="milkit_cat" href="/blog/category/planets" rel="category tag">Planets</a>
</div>
<!-- .entry-meta -->
<h2 class="entry-title" itemprop="name"><a href="/blog/2018/meaning-mc-libra/" itemprop="url" rel="bookmark">The Meaning of MC in Libra</a></h2>
</header>
<!-- .entry-header -->
<div class="entry-summary" itemprop="text">
<p>Exploring the symbolism of MC (Midheaven) in Libra, the Scales...</p>
</div>
<!-- .entry-summary -->
<footer class="entry-footer">
<div class="author-date">
<span class="posted-on ">Published <a href="" rel="bookmark"><time class="entry-date published" itemprop="datePublished">Dec. 31, 2018</time></a></span>
<span class="byline ">By <span class="author vcard" itemprop="author" itemscope="itemscope">
<a>Taya</a>
</span></span>
</div>
<!-- .author-date -->
</footer>
<!-- .entry-footer -->
</div>
<!-- .post-module-content -->
</article>
<article class="post-module post-module-4cols vertical" itemscope="itemscope" itemtype="http://schema.org/Article">
<a class="post-thumbnail" href="/blog/2018/interpretations-chart-shapes-natal-chart/">
<img alt="thumbnail" class="wp-post-image lazyload" data-src="https://astro-charts.com/client/attachments/2018/interpretations-chart-shapes-natal-chart/shape440x293.jpg" itemprop="image"/>
</a>
<div class="post-module-content">
<header class="entry-header">
<div class="entry-cats">
<a class="milkit_cat" href="/blog/category/charts" rel="category tag">Charts</a>
</div>
<!-- .entry-meta -->
<h2 class="entry-title" itemprop="name"><a href="/blog/2018/interpretations-chart-shapes-natal-chart/" itemprop="url" rel="bookmark">Interpretations for Chart Shapes in the Natal Chart</a></h2>
</header>
<!-- .entry-header -->
<div class="entry-summary" itemprop="text">
<p>Interpretations for the chart shapes within the birth chart: Splash pattern, Bowl pattern, Bucket pattern, Locomotive pattern, Seesaw pattern, Bundle pattern, Fan ...</p>
</div>
<!-- .entry-summary -->
<footer class="entry-footer">
<div class="author-date">
<span class="posted-on ">Published <a href="" rel="bookmark"><time class="entry-date published" itemprop="datePublished">Nov. 3, 2018</time></a></span>
<span class="byline ">By <span class="author vcard" itemprop="author" itemscope="itemscope">
<a>Taya</a>
</span></span>
</div>
<!-- .author-date -->
</footer>
<!-- .entry-footer -->
</div>
<!-- .post-module-content -->
</article>
</div>
<!-- .post-modules -->
<a href="/blog/page/1">
<div class="view-more-btn">
<p>View more</p>
</div>
</a>
<!-- #main -->
</div>
<!-- #primary -->
<div class="clear"></div>
<div class="pop_persons">
<div class="section-title"><h4>Popular Celebrity Charts</h4></div>
<div class="inner_ibox">
<div class="psidebar-item">
<a href="/persons/chart/cardi-b/" onclick="ga('send', 'event', 'Famous Astrology Charts', 'click', 'sidebar' );">
<span class="small-person-icon">
<img class="lazyload" data-src="/client/common/images/persons/cardi_60x60.jpg"/>
</span>
<div class="small-person-info">
<span>Libra</span>
<b>Cardi B</b>
            October 11, 1992
          </div>
</a>
</div>
<div class="psidebar-item">
<a href="/persons/chart/meghan-markle/" onclick="ga('send', 'event', 'Famous Astrology Charts', 'click', 'sidebar' );">
<span class="small-person-icon">
<img class="lazyload" data-src="/client/common/images/persons/markle_60x60.jpg"/>
</span>
<div class="small-person-info">
<span>Leo</span>
<b>Meghan Markle</b>
            August 4, 1981
          </div>
</a>
</div>
<div class="psidebar-item">
<a href="/persons/chart/kanye-west/" onclick="ga('send', 'event', 'Famous Astrology Charts', 'click', 'sidebar' );">
<span class="small-person-icon">
<img class="lazyload" data-src="/client/common/images/persons/kanye_60x60.jpg"/>
</span>
<div class="small-person-info">
<span>Gemini</span>
<b>Kanye West</b>
            June 8, 1977
          </div>
</a>
</div>
<div class="psidebar-item">
<a href="/persons/chart/beyonce/" onclick="ga('send', 'event', 'Famous Astrology Charts', 'click', 'sidebar' );">
<span class="small-person-icon">
<img class="lazyload" data-src="/client/common/images/persons/bey_60x60.jpg"/>
</span>
<div class="small-person-info">
<span>Virgo</span>
<b>Beyoncé</b>
            September 4, 1981
          </div>
</a>
</div>
<div class="psidebar-item">
<a href="/persons/chart/taylor-swift/" onclick="ga('send', 'event', 'Famous Astrology Charts', 'click', 'sidebar' );">
<span class="small-person-icon">
<img class="lazyload" data-src="/client/common/images/persons/tay-swift_60x60.jpg"/>
</span>
<div class="small-person-info">
<span>Sagittarius</span>
<b>Taylor Swift</b>
            December 13, 1989
          </div>
</a>
</div>
<div class="psidebar-item">
<a href="/persons/chart/dua-lipa/" onclick="ga('send', 'event', 'Famous Astrology Charts', 'click', 'sidebar' );">
<span class="small-person-icon">
<img class="lazyload" data-src="/client/common/images/persons/dua-lipa_60x60.jpg"/>
</span>
<div class="small-person-info">
<span>Leo</span>
<b>Dua Lipa</b>
            August 22, 1995
          </div>
</a>
</div>
<div class="psidebar-item">
<a href="/persons/chart/tristan-thompson/" onclick="ga('send', 'event', 'Famous Astrology Charts', 'click', 'sidebar' );">
<span class="small-person-icon">
<img class="lazyload" data-src="/client/common/images/persons/tristan_60x60.jpg"/>
</span>
<div class="small-person-info">
<span>Pisces</span>
<b>Tristan Thompson</b>
            March 13, 1991
          </div>
</a>
</div>
<div class="psidebar-item">
<a href="/persons/chart/tupac-shakur/" onclick="ga('send', 'event', 'Famous Astrology Charts', 'click', 'sidebar' );">
<span class="small-person-icon">
<img class="lazyload" data-src="/client/common/images/persons/tupac_60x60.jpg"/>
</span>
<div class="small-person-info">
<span>Gemini</span>
<b>Tupac Shakur</b>
            June 16, 1971
          </div>
</a>
</div>
<div class="psidebar-item">
<a href="/persons/chart/rihanna/" onclick="ga('send', 'event', 'Famous Astrology Charts', 'click', 'sidebar' );">
<span class="small-person-icon">
<img class="lazyload" data-src="/client/common/images/persons/rihanna_60x60.jpg"/>
</span>
<div class="small-person-info">
<span>Pisces</span>
<b>Rihanna</b>
            February 20, 1988
          </div>
</a>
</div>
<div class="psidebar-item">
<a href="/persons/chart/lana-del-rey/" onclick="ga('send', 'event', 'Famous Astrology Charts', 'click', 'sidebar' );">
<span class="small-person-icon">
<img class="lazyload" data-src="/client/common/images/persons/lana-del-ray_60x60.jpg"/>
</span>
<div class="small-person-info">
<span>Gemini</span>
<b>Lana Del Rey</b>
            June 21, 1985
          </div>
</a>
</div>
<div class="psidebar-item">
<a href="/persons/chart/jay-z/" onclick="ga('send', 'event', 'Famous Astrology Charts', 'click', 'sidebar' );">
<span class="small-person-icon">
<img class="lazyload" data-src="/client/common/images/persons/jayz_60x60.jpg"/>
</span>
<div class="small-person-info">
<span>Sagittarius</span>
<b>Jay-Z</b>
            December 4, 1970
          </div>
</a>
</div>
<div class="psidebar-item">
<a href="/persons/chart/the-weeknd/" onclick="ga('send', 'event', 'Famous Astrology Charts', 'click', 'sidebar' );">
<span class="small-person-icon">
<img class="lazyload" data-src="/client/common/images/persons/weeknd_60x60.jpg"/>
</span>
<div class="small-person-info">
<span>Aquarius</span>
<b>The Weeknd</b>
            February 16, 1990
          </div>
</a>
</div>
</div><!--end inner_ibox-->
</div>
<br/>
<div class="section-title"><h4>Personalized Astrology Reports</h4></div>
<div id="report-section">
<div class="report-hp">
<a href="/shop/your-astrology-birth-chart/?r=h">
<span aria-label="Birth Chart Report" class="report-icon" role="img">
<img src="/client/common/images/site/report-icon.svg"/>
</span>
<h3>Guide To Your Birth Chart</h3>
<p>Your birth chart is the blueprint of your soul. In this report we explore the layout of your birth chart and provide you unique insights into the strengths and challenges of your chart.</p>
</a>
<a class="more-link" href="/shop/your-astrology-birth-chart/?r=h">Get my report</a>
<span class="icon fa fa-angle-right"></span>
</div>
<div class="report-hp">
<a href="/shop/your-synastry-chart/?r=h">
<span aria-label="Synastry Report" class="report-icon" role="img">
<img src="/client/common/images/site/report-icon.svg"/>
</span>
<h3>Guide To Your Synastry Chart</h3>
<p>Discover your relationship’s unique voice. In this guide we analyze and interpret your relationship’s shared planetary aspects and uncover its strengths and growth areas.</p>
</a>
<a class="more-link" href="/shop/your-synastry-chart/?r=h">Get my report</a>
<span class="icon fa fa-angle-right"></span>
</div>
<div class="report-hp">
<a href="/shop/your-transits-report/?r=h">
<span aria-label="Transit Report" class="report-icon" role="img">
<img src="/client/common/images/site/report-icon.svg"/>
</span>
<h3>Guide To Your Yearly Transits</h3>
<p>In this report we analyze and interpret your upcoming transit’s for the year, providing you with insightful hints and tips to help you best navigate the windfalls and challenges on your path up ahead.</p>
</a>
<a class="more-link" href="/shop/your-transits-report/?r=h">Get my report</a>
<span class="icon fa fa-angle-right"></span>
</div>
</div>
</div>
<footer class="site-footer noprint" id="colophon" itemscope="itemscope" itemtype="http://schema.org/WPFooter" role="contentinfo">
<a class="astro-cell-sheet footer-logo" href="/" rel="home"></a>
<ul id="footer-links">
<li><a href="/about">About</a></li>
<li><a href="/terms_of_use">Terms of use</a></li>
<li><a href="/feedback">Contact us</a></li>
</ul>
<p class="site-description" itemprop="description">
	  © 2020 Astro-Charts		
	</p>
</footer>
</div><!--page-->
</body>
<script src="/client/common/js/base.min.js?v=6.58" type="text/javascript"></script>
<script type="text/javascript">
    window.anyTime = true; // birthtime not needed
  </script>
<script src="/client/forms/js/birthchart.js?v=6.58" type="text/javascript"></script>
<script>
      // Detecting IE
      (function ($) {
        "use strict";
        if ($('html').is('.ie')) {
            alert('Our site is not compatible with this version of IE. Please upgrade to IE 9 or greater and come back!');
        }
      }(jQuery));

      // Analytics
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-61395619-1', 'auto');
      ga('send', 'pageview');
    </script>
<script type="application/ld+json">
  // site name displayed in search
  {  "@context" : "http://schema.org",
     "@type" : "WebSite",
     "name" : "Astro-Charts",
     "alternateName" : "Astro Charts",
     "url" : "http://astro-charts.com"
  }
  </script>
</html>