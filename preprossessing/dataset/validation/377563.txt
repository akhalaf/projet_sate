<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<title>Página no encontrada – CTA</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://cta.com.py/feed/" rel="alternate" title="CTA » Feed" type="application/rss+xml"/>
<link href="https://cta.com.py/comments/feed/" rel="alternate" title="CTA » Feed de los comentarios" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/cta.com.py\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://cta.com.py/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cta.com.py/wp-includes/css/dist/block-library/theme.min.css?ver=5.5.3" id="wp-block-library-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cta.com.py/wp-content/themes/twentynineteen/style.css?ver=1.4" id="twentynineteen-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cta.com.py/wp-content/themes/twentynineteen/print.css?ver=1.4" id="twentynineteen-print-style-css" media="print" rel="stylesheet" type="text/css"/>
<link href="https://cta.com.py/wp-json/" rel="https://api.w.org/"/><link href="https://cta.com.py/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://cta.com.py/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><link href="https://cta.com.py/wp-content/uploads/2020/03/cropped-LOGO150px-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://cta.com.py/wp-content/uploads/2020/03/cropped-LOGO150px-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://cta.com.py/wp-content/uploads/2020/03/cropped-LOGO150px-180x180.png" rel="apple-touch-icon"/>
<meta content="https://cta.com.py/wp-content/uploads/2020/03/cropped-LOGO150px-270x270.png" name="msapplication-TileImage"/>
<style id="wp-custom-css" type="text/css">
			

/** Start Envato Elements CSS: Sustainable Housing (132-3-52529c35d23bc8ae13d3f5731ba934da) **/

.envato-kit-131-project-item .envato-kit-131-project-item-title{
	position:absolute;
	bottom:20px;
	left:-30px;
	width:1px;
	height:1px;
	z-index:1;
	-webkit-transform:rotate(-90deg);
	-moz-transform:rotate(-90deg);
	transform:rotate(-90deg);
}

.envato-kit-131-project-item .envato-kit-131-project-item-title .elementor-heading-title{
	white-space:nowrap;
}

.envato-kit-131-project-item-flip .elementor-flip-box__front{
	top:0px;
	left:0px;
}

.envato-kit-131-project-item-flip:hover .elementor-flip-box__front{
	top:-10px;
	left:-10px;
}

.envato-kit-131-project-item-flip .elementor-flip-box__back{
	bottom:0px;
	right:0px;
}

.envato-kit-131-project-item-flip:hover .elementor-flip-box__back{
	bottom:-10px;
	right:-10px;
}

/** End Envato Elements CSS: Sustainable Housing (132-3-52529c35d23bc8ae13d3f5731ba934da) **/

		</style>
</head>
<body class="error404 wp-custom-logo wp-embed-responsive hfeed image-filters-enabled elementor-default elementor-kit-10" data-rsssl="1">
<div class="site" id="page">
<a class="skip-link screen-reader-text" href="#content">Saltar al contenido</a>
<header class="site-header" id="masthead">
<div class="site-branding-container">
<div class="site-branding">
<div class="site-logo"><a class="custom-logo-link" href="https://cta.com.py/" rel="home"><img alt="CTA" class="custom-logo" height="190" sizes="(max-width: 34.9rem) calc(100vw - 2rem), (max-width: 53rem) calc(8 * (100vw / 12)), (min-width: 53rem) calc(6 * (100vw / 12)), 100vw" src="https://cta.com.py/wp-content/uploads/2020/03/cropped-imagotipo-gris-y-rojo.png" srcset="https://cta.com.py/wp-content/uploads/2020/03/cropped-imagotipo-gris-y-rojo.png 190w, https://cta.com.py/wp-content/uploads/2020/03/cropped-imagotipo-gris-y-rojo-150x150.png 150w" width="190"/></a></div>
<p class="site-title"><a href="https://cta.com.py/" rel="home">CTA</a></p>
<p class="site-description">
				Constructora			</p>
</div><!-- .site-branding -->
</div><!-- .site-branding-container -->
</header><!-- #masthead -->
<div class="site-content" id="content">
<section class="content-area" id="primary">
<main class="site-main" id="main">
<div class="error-404 not-found">
<header class="page-header">
<h1 class="page-title">¡Vaya! No se ha podido encontrar esa página.</h1>
</header><!-- .page-header -->
<div class="page-content">
<p>Parece que no se ha encontrado nada en esta ubicación. ¿Quieres probar una búsqueda?</p>
<form action="https://cta.com.py/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Buscar:</span>
<input class="search-field" name="s" placeholder="Buscar …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Buscar"/>
</form> </div><!-- .page-content -->
</div><!-- .error-404 -->
</main><!-- #main -->
</section><!-- #primary -->
</div><!-- #content -->
<footer class="site-footer" id="colophon">
<aside aria-label="Pie de página" class="widget-area" role="complementary">
<div class="widget-column footer-widget-1">
<section class="widget widget_search" id="search-2"><form action="https://cta.com.py/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Buscar:</span>
<input class="search-field" name="s" placeholder="Buscar …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Buscar"/>
</form></section>
<section class="widget widget_recent_entries" id="recent-posts-2">
<h2 class="widget-title">Entradas recientes</h2>
<ul>
<li>
<a href="https://cta.com.py/hello-world/">Hello world!</a>
</li>
</ul>
</section><section class="widget widget_recent_comments" id="recent-comments-2"><h2 class="widget-title">Comentarios recientes</h2><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link"><a class="url" href="https://wordpress.org/" rel="external nofollow ugc">A WordPress Commenter</a></span> en <a href="https://cta.com.py/hello-world/#comment-1">Hello world!</a></li></ul></section><section class="widget widget_archive" id="archives-2"><h2 class="widget-title">Archivos</h2>
<ul>
<li><a href="https://cta.com.py/2020/03/">marzo 2020</a></li>
</ul>
</section><section class="widget widget_categories" id="categories-2"><h2 class="widget-title">Categorías</h2>
<ul>
<li class="cat-item cat-item-1"><a href="https://cta.com.py/category/uncategorized/">Uncategorized</a>
</li>
</ul>
</section><section class="widget widget_meta" id="meta-2"><h2 class="widget-title">Meta</h2>
<ul>
<li><a href="https://cta.com.py/wp-login.php">Acceder</a></li>
<li><a href="https://cta.com.py/feed/">Feed de entradas</a></li>
<li><a href="https://cta.com.py/comments/feed/">Feed de comentarios</a></li>
<li><a href="https://es.wordpress.org/">WordPress.org</a></li>
</ul>
</section> </div>
</aside><!-- .widget-area -->
<div class="site-info">
<a class="site-name" href="https://cta.com.py/" rel="home">CTA</a>,
						<a class="imprint" href="https://es.wordpress.org/">
				Funciona gracias a WordPress.			</a>
</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->
<script id="wp-embed-js" src="https://cta.com.py/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
</body>
</html>
