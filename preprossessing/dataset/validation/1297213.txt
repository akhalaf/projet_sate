<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Serenity Hotel</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet"/>
<!--link rel="stylesheet/less" href="https://serenitykathu.com/wp-content/themes/serenityhotelphuket/less/bootstrap.less" type="text/css" /-->
<!--link rel="stylesheet/less" href="https://serenitykathu.com/wp-content/themes/serenityhotelphuket/less/responsive.less" type="text/css" /-->
<!--script src="https://serenitykathu.com/wp-content/themes/serenityhotelphuket/js/less-1.3.3.min.js"></script-->
<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
<link href="https://serenitykathu.com/wp-content/themes/serenityhotelphuket/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://serenitykathu.com/wp-content/themes/serenityhotelphuket/style.css" rel="stylesheet"/>
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
          <script src="https://serenitykathu.com/wp-content/themes/serenityhotelphuket/js/html5shiv.js"></script>
        <![endif]-->
<!-- Fav and touch icons -->
<link href="https://serenitykathu.com/wp-content/themes/serenityhotelphuket/img/favicon.ico" rel="apple-touch-icon-precomposed"/>
<link href="https://serenitykathu.com/wp-content/themes/serenityhotelphuket/img/favicon.ico" rel="shortcut icon"/>
<script src="https://serenitykathu.com/wp-content/themes/serenityhotelphuket/js/jquery.min.js" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-content/themes/serenityhotelphuket/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-content/themes/serenityhotelphuket/js/scripts.js" type="text/javascript"></script>
<script type="text/javascript">
            jQuery(document).ready(function() {
                $('.dropdown').hover(
                        function() {
                            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
                        },
                        function() {
                            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
                        }
                );
            });
        </script>
<!-- This site is optimized with the Yoast SEO plugin v13.1 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex,follow" name="robots"/>
<meta content="en_US" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Page not found - Serenity Hotel Phuket" property="og:title"/>
<meta content="Serenity Hotel Phuket" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Page not found - Serenity Hotel Phuket" name="twitter:title"/>
<script class="yoast-schema-graph yoast-schema-graph--main" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://serenitykathu.com/#website","url":"https://serenitykathu.com/","name":"Serenity Hotel Phuket","inLanguage":"en-US","description":"Serenity Hotel Phuket","potentialAction":{"@type":"SearchAction","target":"https://serenitykathu.com/?s={search_term_string}","query-input":"required name=search_term_string"}}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//ajax.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://serenitykathu.com/feed" rel="alternate" title="Serenity Hotel Phuket » Feed" type="application/rss+xml"/>
<link href="https://serenitykathu.com/comments/feed" rel="alternate" title="Serenity Hotel Phuket » Comments Feed" type="application/rss+xml"/>
<link href="https://serenitykathu.com/wp-includes/css/dist/block-library/style.min.css?ver=5.3.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://serenitykathu.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.6" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css?ver=1.11.4" id="jquery-ui-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://serenitykathu.com/wp-content/plugins/contact-form-7-datepicker/js/jquery-ui-timepicker/jquery-ui-timepicker-addon.min.css?ver=5.3.6" id="jquery-ui-timepicker-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://serenitykathu.com/wp-content/plugins/simple-lightbox/client/css/app.css?ver=2.8.0" id="slb_core-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://serenitykathu.com/wp-content/plugins/booking/assets/libs/bootstrap/css/bootstrap.css?ver=3.3.5.1" id="wpdevelop-bts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://serenitykathu.com/wp-content/plugins/booking/assets/libs/bootstrap/css/bootstrap-theme.css?ver=3.3.5.1" id="wpdevelop-bts-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://serenitykathu.com/wp-content/plugins/booking/css/client.css?ver=8.7.5" id="wpbc-client-pages-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://serenitykathu.com/wp-content/plugins/booking/css/calendar.css?ver=8.7.5" id="wpbc-calendar-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://serenitykathu.com/wp-content/plugins/booking/css/skins/traditional.css?ver=8.7.5" id="wpbc-calendar-skin-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://serenitykathu.com/wp-content/plugins/booking/core/timeline/v2/css/timeline_v2.css?ver=8.7.5" id="wpbc-flex-timeline-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://serenitykathu.com/wp-content/plugins/booking/core/timeline/v2/css/timeline_skin_v2.css?ver=8.7.5" id="wpbc-flex-timeline-skin-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://serenitykathu.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpbc_global1 = {"wpbc_ajaxurl":"https:\/\/serenitykathu.com\/wp-admin\/admin-ajax.php","wpdev_bk_plugin_url":"https:\/\/serenitykathu.com\/wp-content\/plugins\/booking","wpdev_bk_today":"[2021,1,14,14,46]","visible_booking_id_on_page":"[]","booking_max_monthes_in_calendar":"1y","user_unavilable_days":"[999]","wpdev_bk_edit_id_hash":"","wpdev_bk_plugin_filename":"wpdev-booking.php","bk_days_selection_mode":"multiple","wpdev_bk_personal":"0","block_some_dates_from_today":"0","message_verif_requred":"This field is required","message_verif_requred_for_check_box":"This checkbox must be checked","message_verif_requred_for_radio_box":"At least one option must be selected","message_verif_emeil":"Incorrect email field","message_verif_same_emeil":"Your emails do not match","message_verif_selectdts":"Please, select booking date(s) at Calendar.","parent_booking_resources":"[]","new_booking_title":"Thank you for your online booking.  We will send confirmation of your booking as soon as possible.","new_booking_title_time":"7000","type_of_thank_you_message":"message","thank_you_page_URL":"https:\/\/serenitykathu.com\/thank-you","is_am_pm_inside_time":"true","is_booking_used_check_in_out_time":"false","wpbc_active_locale":"en_US","wpbc_message_processing":"Processing","wpbc_message_deleting":"Deleting","wpbc_message_updating":"Updating","wpbc_message_saving":"Saving","message_checkinouttime_error":"Error! Please reset your check-in\/check-out dates above.","message_starttime_error":"Start Time is invalid. The date or time may be booked, or already in the past! Please choose another date or time.","message_endtime_error":"End Time is invalid. The date or time may be booked, or already in the past. The End Time may also be earlier that the start time, if only 1 day was selected! Please choose another date or time.","message_rangetime_error":"The time(s) may be booked, or already in the past!","message_durationtime_error":"The time(s) may be booked, or already in the past!","bk_highlight_timeslot_word":"Times:"};
/* ]]> */
</script>
<script src="https://serenitykathu.com/wp-content/plugins/booking/js/wpbc_vars.js?ver=8.7.5" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-content/plugins/booking/js/wpbc-migrate.js?ver=1.1" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-content/plugins/booking/js/datepick/jquery.datepick.js?ver=1.1" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-content/plugins/booking/js/client.js?ver=8.7.5" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-content/plugins/booking/js/wpbc_times.js?ver=8.7.5" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-content/plugins/booking/core/timeline/v2/_out/timeline_v2.js?ver=8.7.5" type="text/javascript"></script>
<link href="https://serenitykathu.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://serenitykathu.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://serenitykathu.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3.6" name="generator"/>
<script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		if (window.wfLogHumanRan) { return; }
		window.wfLogHumanRan = true;
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//serenitykathu.com/?wordfence_lh=1&hid=CADAE508BE94FC246E3B157456F6F4E9');
</script><style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style> </head>
<body>
<!-- ======================================= content ======================================= -->
<div class=" fill">
<div class="carousel slide carousel-fade" id="myCarousel">
<div class="carousel-inner">
<div class="active item">
<div class="fill" style="background-image:url('https://serenitykathu.com/wp-content/themes/serenityhotelphuket/images/content/2/Facilities10.jpg');">
<div class="container">
</div>
</div>
</div>
<div class="item">
<div class="fill" style="background-image:url('https://serenitykathu.com/wp-content/themes/serenityhotelphuket/images/content/2/Facilities14.jpg');">
<div class="container">
</div>
</div>
</div>
<div class="item">
<div class="fill" style="background-image:url('https://serenitykathu.com/wp-content/themes/serenityhotelphuket/images/content/2/Facilities13.jpg');">
<div class="container">
</div>
</div>
</div>
</div>
<div class="pull-center">
<a class="carousel-control left" data-slide="prev" href="#myCarousel"></a>
<a class="carousel-control right" data-slide="next" href="#myCarousel"></a>
</div>
</div>
</div>
<div class="container-fluid">
<div class="row clearfix">
<div class="col-md-12 column web-contents">
<div class="clearfix">
<div class="col-md-3 column left-col">
<div class="row content">
<img alt="logo" class="img-responsive" src="https://serenitykathu.com/wp-content/themes/serenityhotelphuket/images/logo.png"/>
<p>Sorry, no posts matched your criteria.</p>
</div>
<div class="col-md-9 column right">
<nav class="navbar navbar-default" role="navigation">
<div class="navbar-header">
<button class="navbar-toggle" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> <a class="navbar-brand" href="#">Navigation</a>
</div>
<!--div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">	
                                <li>
                                    <a href="#">Home</a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">rooms & rates<strong class="caret"></strong></a>
                                    <ul class="dropdown-menu">

                                        <li>
                                            <a href="#">Action</a>
                                        </li>
                                        <li>
                                            <a href="#">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#">Something else here</a>
                                        </li>
                                        <li class="divider">
                                        </li>
                                        <li>
                                            <a href="#">Separated link</a>
                                        </li>
                                        <li class="divider">
                                        </li>
                                        <li>
                                            <a href="#">One more separated link</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">facilities</a>
                                </li>
                                <li>
                                    <a href="#">book online</a>
                                </li>
                                <li>
                                    <a href="#">location</a>
                                </li>
                                <li class="active">
                                    <a href="#">contact us</a>
                                </li>
                            </ul>

                        </div-->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"><ul class="nav navbar-nav" id="menu-main"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-24" id="menu-item-24"><a href="https://serenitykathu.com/" title="Home">Home</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-34 dropdown" id="menu-item-34"><a aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" title="Rooms &amp; Rates">Rooms &amp; Rates <span class="caret"></span></a>
<ul class=" dropdown-menu" role="menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30" id="menu-item-30"><a href="https://serenitykathu.com/standard" title="Standard Room (2)">Standard Room (2)</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-152" id="menu-item-152"><a href="https://serenitykathu.com/standard-luxe" title="Deluxe Room with Lake View (6)">Deluxe Room with Lake View (6)</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25" id="menu-item-25"><a href="https://serenitykathu.com/bungalow" title="Bungalow with terrace by the Lake (4)">Bungalow with terrace by the Lake (4)</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-727" id="menu-item-727"><a href="https://serenitykathu.com/suite-with-pool-access-1" title="Pool Suite  (1)">Pool Suite  (1)</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32" id="menu-item-32"><a href="https://serenitykathu.com/family-room" title="Family Room (1)">Family Room (1)</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-94" id="menu-item-94"><a href="https://serenitykathu.com/condominium-2" title="Apartments">Apartments</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26" id="menu-item-26"><a href="https://serenitykathu.com/facilities" title="Facilities">Facilities</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28" id="menu-item-28"><a href="https://serenitykathu.com/location" title="Location">Location</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-759" id="menu-item-759"><a href="https://serenitykathu.com/send-us-your-booking-request" title="Booking online">Booking online</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27" id="menu-item-27"><a href="https://serenitykathu.com/general-inquiries" title="Contact Us">Contact Us</a></li>
</ul></div> </nav>
</div>
</div>
</div>
</div>
</div>
<!--WPFC_FOOTER_START-->
<!-- footer -->
<script type="text/javascript">
                                $('.carousel').carousel({
                                    interval: 20000
                                });
                            </script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/serenitykathu.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://serenitykathu.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.6" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.11.4" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Close","currentText":"Today","monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Previous","dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"dateFormat":"MM d, yy","firstDay":1,"isRTL":false});});
</script>
<script src="https://serenitykathu.com/wp-content/plugins/contact-form-7-datepicker/js/jquery-ui-timepicker/jquery-ui-timepicker-addon.min.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-includes/js/jquery/ui/slider.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-includes/js/jquery/ui/button.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-content/plugins/contact-form-7-datepicker/js/jquery-ui-sliderAccess.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://serenitykathu.com/wp-includes/js/wp-embed.min.js?ver=5.3.6" type="text/javascript"></script>
<script id="slb_context" type="text/javascript">/* <![CDATA[ */if ( !!window.jQuery ) {(function($){$(document).ready(function(){if ( !!window.SLB ) { {$.extend(SLB, {"context":["public","user_guest"]});} }})})(jQuery);}/* ]]> */</script>
</div></body>
</html>
