<!DOCTYPE html>
<html class="no-js" lang="nl">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
<title>Pagina niet gevonden – Dutch Jobs in Barcelona</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link crossorigin="" href="https://fonts.gstatic.com" rel="preconnect"/>
<link href="https://dutchjobsinbarcelona.com/feed/" rel="alternate" title="Dutch Jobs in Barcelona » Feed" type="application/rss+xml"/>
<link href="https://dutchjobsinbarcelona.com/comments/feed/" rel="alternate" title="Dutch Jobs in Barcelona » Reactiesfeed" type="application/rss+xml"/>
<script>
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/dutchjobsinbarcelona.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style>
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://dutchjobsinbarcelona.com/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet"/>
<link href="https://dutchjobsinbarcelona.com/wp-includes/css/dist/block-library/theme.min.css?ver=5.5.3" id="wp-block-library-theme-css" media="all" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Merriweather%3A400%2C700%2C900%2C400italic%2C700italic%2C900italic%7CMontserrat%3A400%2C700%7CInconsolata%3A400&amp;subset=latin%2Clatin-ext&amp;display=fallback" id="twentysixteen-fonts-css" media="all" rel="stylesheet"/>
<link href="https://dutchjobsinbarcelona.com/wp-content/themes/twentysixteen/genericons/genericons.css?ver=3.4.1" id="genericons-css" media="all" rel="stylesheet"/>
<link href="https://dutchjobsinbarcelona.com/wp-content/themes/twentysixteen/style.css?ver=20190507" id="twentysixteen-style-css" media="all" rel="stylesheet"/>
<link href="https://dutchjobsinbarcelona.com/wp-content/themes/twentysixteen/css/blocks.css?ver=20190102" id="twentysixteen-block-style-css" media="all" rel="stylesheet"/>
<!--[if lt IE 10]>
<link rel='stylesheet' id='twentysixteen-ie-css'  href='https://dutchjobsinbarcelona.com/wp-content/themes/twentysixteen/css/ie.css?ver=20170530' media='all' />
<![endif]-->
<!--[if lt IE 9]>
<link rel='stylesheet' id='twentysixteen-ie8-css'  href='https://dutchjobsinbarcelona.com/wp-content/themes/twentysixteen/css/ie8.css?ver=20170530' media='all' />
<![endif]-->
<!--[if lt IE 8]>
<link rel='stylesheet' id='twentysixteen-ie7-css'  href='https://dutchjobsinbarcelona.com/wp-content/themes/twentysixteen/css/ie7.css?ver=20170530' media='all' />
<![endif]-->
<!--[if lt IE 9]>
<script src='https://dutchjobsinbarcelona.com/wp-content/themes/twentysixteen/js/html5.js?ver=3.7.3' id='twentysixteen-html5-js'></script>
<![endif]-->
<script id="jquery-core-js" src="https://dutchjobsinbarcelona.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"></script>
<link href="https://dutchjobsinbarcelona.com/wp-json/" rel="https://api.w.org/"/><link href="https://dutchjobsinbarcelona.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://dutchjobsinbarcelona.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
</head>
<body class="error404 wp-embed-responsive hfeed">
<div class="site" id="page">
<div class="site-inner">
<a class="skip-link screen-reader-text" href="#content">Naar de inhoud springen</a>
<header class="site-header" id="masthead" role="banner">
<div class="site-header-main">
<div class="site-branding">
<p class="site-title"><a href="https://dutchjobsinbarcelona.com/" rel="home">Dutch Jobs in Barcelona</a></p>
<p class="site-description">Nederlandse vacatures in Barcelona</p>
</div><!-- .site-branding -->
<button class="menu-toggle" id="menu-toggle">Menu</button>
<div class="site-header-menu" id="site-header-menu">
<nav aria-label="Hoofdmenu" class="main-navigation" id="site-navigation" role="navigation">
<div class="menu-top-container"><ul class="primary-menu" id="menu-top"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2664" id="menu-item-2664"><a href="https://alljobs.recruit4.work/index.php/page/applicants/bb/1/command/startsignup/cms_categorie/2253">REGISTREREN</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2665" id="menu-item-2665"><a href="https://alljobs.recruit4.work/index.php/page/applicants/bb/1/command/startlogin">LOGIN</a></li>
</ul></div> </nav><!-- .main-navigation -->
</div><!-- .site-header-menu -->
</div><!-- .site-header-main -->
</header><!-- .site-header -->
<div class="site-content" id="content">
<div class="content-area" id="primary">
<main class="site-main" id="main" role="main">
<section class="error-404 not-found">
<header class="page-header">
<h1 class="page-title">Oeps! Deze pagina kon niet worden gevonden.</h1>
</header><!-- .page-header -->
<div class="page-content">
<p>Het lijkt erop dat er niets gevonden is op deze locatie. Probeer het eens met een zoekopdracht?</p>
<form action="https://dutchjobsinbarcelona.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Zoeken naar:</span>
<input class="search-field" name="s" placeholder="Zoeken …" type="search" value=""/>
</label>
<button class="search-submit" type="submit"><span class="screen-reader-text">Zoeken</span></button>
</form>
</div><!-- .page-content -->
</section><!-- .error-404 -->
</main><!-- .site-main -->
</div><!-- .content-area -->
<aside class="sidebar widget-area" id="secondary" role="complementary">
<section class="widget widget_search" id="search-2">
<form action="https://dutchjobsinbarcelona.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Zoeken naar:</span>
<input class="search-field" name="s" placeholder="Zoeken …" type="search" value=""/>
</label>
<button class="search-submit" type="submit"><span class="screen-reader-text">Zoeken</span></button>
</form>
</section> </aside><!-- .sidebar .widget-area -->
</div><!-- .site-content -->
<footer class="site-footer" id="colophon" role="contentinfo">
<nav aria-label="Footer primaire menu" class="main-navigation" role="navigation">
<div class="menu-top-container"><ul class="primary-menu" id="menu-top-1"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2664"><a href="https://alljobs.recruit4.work/index.php/page/applicants/bb/1/command/startsignup/cms_categorie/2253">REGISTREREN</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2665"><a href="https://alljobs.recruit4.work/index.php/page/applicants/bb/1/command/startlogin">LOGIN</a></li>
</ul></div> </nav><!-- .main-navigation -->
<div class="site-info">
<span class="site-title"><a href="https://dutchjobsinbarcelona.com/" rel="home">Dutch Jobs in Barcelona</a></span>
<a class="imprint" href="https://nl.wordpress.org/">
					Ondersteund door WordPress				</a>
</div><!-- .site-info -->
</footer><!-- .site-footer -->
</div><!-- .site-inner -->
</div><!-- .site -->
<script id="twentysixteen-skip-link-focus-fix-js" src="https://dutchjobsinbarcelona.com/wp-content/themes/twentysixteen/js/skip-link-focus-fix.js?ver=20170530"></script>
<script id="twentysixteen-script-js-extra">
var screenReaderText = {"expand":"Alles uitklappen","collapse":"Alles inklappen"};
</script>
<script id="twentysixteen-script-js" src="https://dutchjobsinbarcelona.com/wp-content/themes/twentysixteen/js/functions.js?ver=20181217"></script>
<script id="page-links-to-js" src="https://dutchjobsinbarcelona.com/wp-content/plugins/page-links-to/dist/new-tab.js?ver=3.3.4"></script>
<script id="wp-embed-js" src="https://dutchjobsinbarcelona.com/wp-includes/js/wp-embed.min.js?ver=5.5.3"></script>
</body>
</html>
