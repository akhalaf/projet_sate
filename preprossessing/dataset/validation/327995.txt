<!DOCTYPE html>
<!--[if IE 8 ]><html lang="en-US" class="ie ie8"><![endif]--><!--[if IE 9 ]><html lang="en-US" class="ie ie9"><![endif]--><!--[if IE 10 ]><html lang="en-US" class="ie ie10"><![endif]--><!--[if (gt IE 10)|!(IE)]><!--><html lang="en-US"><!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Page not found | Clariter</title>
<link href="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/themes/clariter/assets/img/icon.png" rel="shortcut icon" type="image/png"/>
<link href="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/themes/clariter/assets/img/icon.png" rel="apple-touch-icon"/>
<!-- Bootstrap core CSS -->
<link href="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/themes/clariter/assets/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/themes/clariter/assets/js/html5shiv.min.js"></script>
      <script src="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/themes/clariter/assets/js/respond.min.js"></script>
    <![endif]-->
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&amp;display=swap" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;600&amp;display=swap" rel="stylesheet"/>
<link href="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/themes/clariter/style.css?v41" rel="stylesheet"/>
<meta content="We end the life of plastic" name="description"/><meta content="The influence of plastic - Clariter" property="og:title"/><meta content="We end the life of plastic" property="og:description"/><meta content="article" property="og:type"/><meta content="https://clariter.com/the-influence-of-plastic/" property="og:url"/><meta content="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/uploads/2020/06/single-use-plastic-bottles-cups-forks-spoons-conce-KE6CMAS-300x200.jpg" property="og:image"/><link href="//s.w.org" rel="dns-prefetch"/>
<link crossorigin="" href="https://secureservercdn.net" rel="preconnect"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/secureservercdn.net\/160.153.137.163\/ugb.68a.myftpupload.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6&time=1610384825"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6&amp;time=1610384825" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2&amp;time=1610384825" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1&amp;time=1610384825" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2&amp;time=1610384825" type="text/javascript"></script>
<script id="gainwp-tracking-analytics-events-js-extra" type="text/javascript">
/* <![CDATA[ */
var gainwpUAEventsData = {"options":{"event_tracking":"1","event_downloads":"zip|mp3*|mpe*g|pdf|docx*|pptx*|xlsx*|rar*","event_bouncerate":0,"aff_tracking":0,"event_affiliates":"\/out\/","hash_tracking":0,"root_domain":"clariter.com","event_timeout":100,"event_precision":0,"event_formsubmit":1,"ga_pagescrolldepth_tracking":0,"ga_with_gtag":0}};
/* ]]> */
</script>
<script id="gainwp-tracking-analytics-events-js" src="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/plugins/ga-in/front/js/tracking-analytics-events.min.js?ver=5.4.6&amp;time=1610384825" type="text/javascript"></script>
<script id="plugins-js" src="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/themes/clariter/assets/js/plugins.js?ver=v9&amp;time=1610384825" type="text/javascript"></script>
<link href="https://clariter.com/wp-json/" rel="https://api.w.org/"/><link href="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/uploads/2020/06/cropped-icon-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/uploads/2020/06/cropped-icon-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/uploads/2020/06/cropped-icon-180x180.png" rel="apple-touch-icon"/>
<meta content="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/uploads/2020/06/cropped-icon-270x270.png" name="msapplication-TileImage"/>
<!-- BEGIN GAINWP v5.4.6 Universal Analytics - https://intelligencewp.com/google-analytics-in-wordpress/ -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-180085936-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- END GAINWP Universal Analytics -->
<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/01c8497c62db378cc9b12288c/8240edb1ec79fd3135c5316b6.js");</script>
</head>
<body class="error404">
<nav class="navbar navbar-fixed-top hidden-print navbar--scroll" id="navbar-spy">
<div class="container container-lg-fluid container--navbar">
<div class="navbar-header">
<button class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button">
<span class="nav-text hidden">Menu</span>
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar top-bar"></span>
<span class="icon-bar middle-bar"></span>
<span class="icon-bar bottom-bar"></span>
</button>
<a class="navbar-brand cssa" href="https://clariter.com/">
<img alt="We end the life of plastic" class="img-responsive" src="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/themes/clariter/assets/img/clariter.svg" title=""/>
</a>
</div>
<div class="collapse navbar-collapse" id="navbar">
<ul class="nav navbar-nav navbar-right nav--top" id="menu-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-8" id="menu-item-8"><a href="https://clariter.com/"><span class="menu-nav-hover">Home</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14" id="menu-item-14"><a href="https://clariter.com/solution/"><span class="menu-nav-hover">Solution</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13" id="menu-item-13"><a href="https://clariter.com/products/"><span class="menu-nav-hover">Products</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-882" id="menu-item-882"><a href="https://clariter.com/people/"><span class="menu-nav-hover">People</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-883" id="menu-item-883"><a href="https://clariter.com/news/"><span class="menu-nav-hover">News</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-729" id="menu-item-729"><a href="https://clariter.com/insights/"><span class="menu-nav-hover">Insights</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17" id="menu-item-17"><a href="https://clariter.com/contact/"><span class="menu-nav-hover">Contact</span></a></li>
</ul> </div><!-- /.navbar-collapse -->
</div><!-- /.container-->
</nav>
<div class="wrapper" id="wrap-theme">
<div class="bg-fluid">
<article>
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-12">
<br/>
<div class="text-black"><h1><strong>404 - Oops! We can’t find that page.</strong></h1></div>
<div class="content-body text-black">
<p><a class="no-underline" href="https://clariter.com/">&lt;&lt; Go to Homepage</a></p>
</div>
<br/>
<br/>
<br/>
</div>
</div>
</div>
</article>
</div>
<div class="footer-fix"><!-- footer --></div>
</div><!-- /.wrapper -->
<footer class="footer hidden-print">
<div class="container container-lg-fluid footer-holder">
<div class="row">
<div class="col-sm-8">
<div class="row">
<div class="widget_text widget-container col-md-12 widget_custom_html" id="custom_html-2"><span class="widget-header hidden">Footer</span><div class="textwidget custom-html-widget"><h2 class="h3 d-inline-block va-middle my-0 ">
	Follow the revolution!
</h2>
<ul class="list-social">
<li class="pl-xs-0"><a href="https://www.facebook.com/clariter.cleanslate/" rel="noopener" target="_blank"><div class="icon-wrapper"><svg class="icon icon-regular icon-social icon-white icon-anim-top"><use xlink:href="/wp-content/themes/clariter/assets/img/symbol-defs.svg#icon-facebook_sm"></use></svg></div></a></li>
<li><a href="https://www.instagram.com/clariter.cleanslate/" rel="noopener" target="_blank"><div class="icon-wrapper"><svg class="icon icon-regular icon-social icon-white icon-anim-top"><use xlink:href="/wp-content/themes/clariter/assets/img/symbol-defs.svg#icon-instagram_sm"></use></svg></div></a></li>
<li><a href="https://twitter.com/claritergroup" rel="noopener" target="_blank"><div class="icon-wrapper"><svg class="icon icon-regular icon-social icon-white icon-anim-top"><use xlink:href="/wp-content/themes/clariter/assets/img/symbol-defs.svg#icon-twitter_sm"></use></svg></div></a></li>
<li><a href="https://www.youtube.com/channel/UCm5Bn_0zE6SoH10efwJHjCQ" rel="noopener" target="_blank"><div class="icon-wrapper"><svg class="icon icon-regular icon-social icon-white icon-anim-top"><use xlink:href="/wp-content/themes/clariter/assets/img/symbol-defs.svg#icon-youtube_sm"></use></svg></div></a></li>
<li><a href="https://www.linkedin.com/company/clariter-group-bv/?originalSubdomain=il" rel="noopener" target="_blank"><div class="icon-wrapper"><svg class="icon icon-regular icon-social icon-white icon-anim-top"><use xlink:href="/wp-content/themes/clariter/assets/img/symbol-defs.svg#icon-linkedin_sm"></use></svg></div></a></li>
</ul></div></div> </div>
</div>
<div class="col-sm-4 text-md-right">
<ul class="nav d-inline-block nav--footer clearfix" id="menu-footer"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-privacy-policy menu-item-20" id="menu-item-20"><a href="https://clariter.com/privacy-policy/"><span class="menu-nav-hover">Privacy Policy</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-638" id="menu-item-638"><a href="https://clariter.com/term-of-use/"><span class="menu-nav-hover">Terms of Use</span></a></li>
</ul> </div>
</div>
</div>
</footer>
<div class="cookies-bar hidden-print">
<div class="container cookies-txt">
<div class="row">
<div class="col-md-9 text-md-left mb-3 mb-md-0"><span>This site uses cookies to provide you with a better browsing experience. By browsing this website, you agree with using cookies. Find out more on how we use cookies <a href="/privacy-policy">here</a>.</span></div>
<div class="col-md-3 text-md-center"><button class="btn btn-primary close-cb" type="button">Accept</button></div>
</div>
</div>
</div>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/clariter.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2&amp;time=1610384825" type="text/javascript"></script>
<script id="bootstrap-script-js" src="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/themes/clariter/assets/js/bootstrap.min.js?ver=v9&amp;time=1610384825" type="text/javascript"></script>
<script id="script-js" src="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/themes/clariter/assets/js/script.js?ver=v9&amp;time=1610384825" type="text/javascript"></script>
<script id="workaround-script-js" src="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-content/themes/clariter/assets/js/ie10-viewport-bug-workaround.js?ver=5.6&amp;time=1610384825" type="text/javascript"></script>
<script id="wp-embed-js" src="https://secureservercdn.net/160.153.137.163/ugb.68a.myftpupload.com/wp-includes/js/wp-embed.min.js?ver=5.6&amp;time=1610384825" type="text/javascript"></script>
</body>
</html>
