<!DOCTYPE html>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- Adobe Launch Script-->
<script async="" src="//assets.adobedtm.com/launch-EN8381bd59bc3540a894c1c8a875373f6a-development.min.js"></script>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>Apartments For Rent in Maine | ApartmentSearch.com</title>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://fonts.googleapis.com/css2?family=Montserrat&amp;family=Raleway&amp;display=swap" lazyload="" rel="stylesheet"/>
<style type="text/css">
        .nav-collapse.collapse {
            height: auto !important;
            overflow: visible !important;
        }
    .navbar-brand {
        background-image: url(/assets/images/logo-white.png);
height: 39px;width:156px;background-size: cover;background-position: center bottom;background-position-x: 0;background-position-y: 0;    }

.navbar-left { padding-top: 10px; }.navbar-header { padding-top: 10px; padding-left:20px; }

	</style>
<script>
        var baseURL = 'https://www.apartmentsearch.com';
	</script>
<script async="" defer="" src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&amp;render=explicit"></script>
</head>
<body>
<div id="wrapper" style="position:relative;min-height:100%;">
<asp:updatepanel id="upnlBounds" rendermode="Inline" runat="server">
<contenttemplate>
<asp:hiddenfield clientidmode="Static" id="NELAT" runat="server" value="0"></asp:hiddenfield>
<asp:hiddenfield clientidmode="Static" id="NELNG" runat="server" value="0"></asp:hiddenfield>
<asp:hiddenfield clientidmode="Static" id="SWLAT" runat="server" value="0"></asp:hiddenfield>
<asp:hiddenfield clientidmode="Static" id="SWLNG" runat="server" value="0"></asp:hiddenfield>
</contenttemplate>
</asp:updatepanel>
<div class="navbar navbar-default navbar-fixed-top">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
                            MENU <i aria-hidden="true" class="fa fa-bars"></i>
</button>
<a class="navbar-brand" href="/"> </a>
</div>
<div class="collapse navbar-collapse navbar-right">
<div class="row">
<div class="col-md-12">
<ul class="nav navbar-nav navbar-right">
<li class="round-nav-button"><a href="#" onclick="ShowLogin();">Sign In</a></li>
<li class="round-nav-button red"><a href="#" onclick="ShowRegister();">Register</a></li>
</ul>
</div>
</div>
</div>
<div class="navbar-collapse collapse navbar-right">
<ul class="nav navbar-nav">
<li class="active"><a href="/apartments">Search</a></li>
<li class=""><a href="/reward/claim" onclick="GoogleTrackEvent('Get Rewarded');">$200 Reward</a></li>
<li class=""><a href="/mytools/favorites">Favorites</a></li>
<li class=""><a href="/furnishit">Furnish It!</a></li>
<li class=""><a href="/helpmemove">Moving</a></li>
<li class=""><a href="/resources">Resources</a></li>
</ul>
</div>
</div>
</div>
<div class="container-full body-content" style="padding: 0 15px 0 15px;">
<div class="container">
<div class="row">
<div class="col-md-12 whiteBox" style="padding:10px;">
<h2 class="header">Apartments For Rent in Maine</h2>
<div class="row">
<div class="col-md-12">
<div class="col-md-3">
<a href="/Apartments/Maine/Auburn">Auburn (1)</a>
</div>
<div class="col-md-3">
<a href="/Apartments/Maine/Augusta">Augusta (1)</a>
</div>
<div class="col-md-3">
<a href="/Apartments/Maine/Bangor">Bangor (1)</a>
</div>
<div class="col-md-3">
<a href="/Apartments/Maine/Falmouth">Falmouth (1)</a>
</div>
<div class="col-md-3">
<a href="/Apartments/Maine/Kittery">Kittery (2)</a>
</div>
<div class="col-md-3">
<a href="/Apartments/Maine/North Reading">North Reading (1)</a>
</div>
<div class="col-md-3">
<a href="/Apartments/Maine/Norwood">Norwood (1)</a>
</div>
<div class="col-md-3">
<a href="/Apartments/Maine/Pittsfield">Pittsfield (1)</a>
</div>
<div class="col-md-3">
<a href="/Apartments/Maine/PORTLAND">PORTLAND (8)</a>
</div>
<div class="col-md-3">
<a href="/Apartments/Maine/South Portland">South Portland (5)</a>
</div>
<div class="col-md-3">
<a href="/Apartments/Maine/Waterville">Waterville (1)</a>
</div>
<div class="col-md-3">
<a href="/Apartments/Maine/Westbrook">Westbrook (1)</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="container-full transparent footer-content" style="padding: 0 15px 0 15px;">
<div class="row">
<div class="col-md-12 redBar">
<div class="container">
<div class="row">
<div class="col-md-3">
<a href="/contactus">Contact Us</a>
</div>
<div class="col-md-3">
<a href="/brokerlicenseinformation">Broker License Information</a>
</div>
<div class="col-md-3">
<a href="https://www.cort.com/privacy-legal" target="_blank">Legal and Privacy</a>
</div>
<div class="col-md-3">
<a href="https://manager.apartmentsearch.com" target="_blank">Property Managers</a>
</div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12 bottomBar">
<div class="container">
<div class="row">
<div class="col-md-6">
                        © 2021 CORT Business Services, Inc. All rights reserved.  Equal Housing Opportunity
                    </div>
<div class="col-md-6 social">
<div class="icon social gle"><a href="https://plus.google.com/+ApartmentsearchbyCORT" style="margin-left:10px;" target="_blank" title="google+"><i class="fa fa-google-plus"></i></a></div>
<div class="icon social tw"><a href="https://twitter.com/apartmentsearch" style="margin-left:10px;" target="_blank" title="twitter"><i class="fa fa-twitter"></i></a></div>
<div class="icon social fb"><a href="https://www.facebook.com/AptSearch" style="margin-left:10px;" target="_blank" title="facebook"><i class="fa fa-facebook"></i></a></div>
<div class="icon social fb"><a href="https://blog.apartmentsearch.com" style="margin-left:10px;" target="_blank" title="blog"><i class="fa fa-rss"></i></a></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="modLogIn" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<a class="close" href="javascript:CloseLogin();">×</a>
<h4 class="modal-title">Sign In</h4>
</div>
<div class="modal-body">
<style>
    .whiteBox {
        background-color: #fff;
        padding: 20px;
    }

    label {
        font-family: 'Montserrat', sans-serif;
        font-weight: bold;
        color: #333;
        text-transform: uppercase;
    }
</style>
<div class="container">
<div class="form-group">
<div class="row">
<div class="col-md-12">
<div class="col-md-12 " style="padding:10px;margin-top:20px;">
<div id="dLoginActive" style="display:none;position: absolute; top: 0px; left: 0px; height: 100%; width: 100%; text-align: center; z-index: 99; background-color: rgba(0, 0, 0, 0.6);">
<div style="margin:175px auto 0 auto;color:#fff;font-weight:bold;">ONE MOMENT PLEASE<br/>ATTEMPTING TO SIGN IN</div>
</div>
<div class="row">
<div class="col-md-12" style="margin-bottom:10px;">
<label>Email Address</label>
<input class="form-control" id="tUserName" placeholder="" tabindex="1" type="text"/>
</div>
</div>
<div class="row">
<div class="col-md-12" id="dLoginError">
</div>
</div>
<div class="row">
<div class="col-md-12" style="margin-top:10px;">
<label>Password</label>
<a href="javascript:ForgotPassword();" style="float:right;" tabindex="4">Forgot Password?</a>
<input class="form-control showpassword" id="tPassword" placeholder="" tabindex="2" type="password"/>
<a class="showPasswordButton" href="javascript:void(0);" style="float:right;" tabindex="5">Show Password</a>
</div>
</div>
<div class="row">
<div class="col-md-12" style="padding-top:10px;">
<a class="round-button red button-block" href="javascript:LogIn();" tabindex="3">Sign In</a>
<a class="round-button grey button-block" href="javascript:SwitchToRegister();" style="margin-top:15px;" tabindex="5">Switch to Register</a>
</div>
</div>
<div class="row">
<div class="col-md-12" style="padding:10px;border-top: solid 1px #ccc;margin-top: 30px;">
<h4>Having Trouble?</h4>
<p>
                                Need additional help or have questions, don't hesitate to contact us!  You can reach us at 1-800-APARTMENT or
                                simply send an email to <a href="mailto:info@apartmentsearch.com">info@apartmentsearch.com</a> and we will be glad to help.
                            </p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="modRegister" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button class="close" data-dismiss="modal" type="button">×</button>
<h4 class="modal-title">Register</h4>
</div>
<div class="modal-body">
<style>
    .registerBox {
        border: solid 1px #cdcdcd;
        border-radius: 5px;
        padding: 10px;
    }
</style>
<div class="container">
<div class="form-group">
<div class="row">
<div class="col-md-12">
<div class="col-md-12" style="padding:10px;margin:0;">
<div id="dRegisterActive" style="display:none;position: absolute; top: 0px; left: 0px; height: 100%; width: 100%; text-align: center; z-index: 99; background-color: rgba(0, 0, 0, 0.6);">
<div style="margin:175px auto 0 auto;color:#fff;font-weight:bold;">ONE MOMENT PLEASE<br/>ATTEMPTING TO CREATE ACCOUNT</div>
</div>
<div class="row">
<div class="col-md-12 registerBox shadow" style="margin-bottom:30px;">
<h4 style="margin-top:0;">Registration is <strong>FREE</strong> and. . .</h4>
<ul>
<li>Registered Users Enjoy Many More Features</li>
<li>Eligible For $200 in Rewards</li>
</ul>
</div>
</div>
<div class="row">
<div class="col-md-6" style="margin-bottom:5px;">
<label>First Name</label>
<input class="form-control" id="tRegisterFirstName" placeholder="" tabindex="1" type="text"/>
</div>
<div class="col-md-6" style="margin-bottom:5px;">
<label>Last Name</label>
<input class="form-control" id="tRegisterLastName" placeholder="" tabindex="2" type="text"/>
</div>
</div>
<div class="row">
<div class="col-md-12" style="margin-bottom:5px;">
<label>Email Address</label>
<input class="form-control" id="tRegisterUserName" onchange="CheckEmailAddress(); CheckRegistrationEmailAddress(this);" placeholder="" tabindex="3" type="text"/>
</div>
</div>
<div class="row">
<div class="col-md-12" id="dRegisterError">
</div>
</div>
<div class="row">
<div class="col-md-6" style="margin-top:5px;">
<label>Password</label>
<input class="form-control" id="tPassword1" placeholder="" tabindex="4" type="password"/>
</div>
<div class="col-md-6" style="margin-top:5px;">
<label>Confirm Password</label>
<input class="form-control" id="tPassword2" placeholder="" tabindex="5" type="password"/>
</div>
</div>
<div class="row" style="margin:10px 0 0 0;text-align:center;">
<div class="col-md-12 text-xs-center">
<div class="g-recaptcha-register" data-sitekey="Y6LecQooUAAAAAOZF-OMx3VNtYohKZeOvzTapltBy" id="RecaptchaField1"></div>
<input class="hiddenRecaptcha" id="rt_hiddenRecaptcha" name="rt_hiddenRecaptcha" type="hidden"/>
</div>
</div>
<div class="row">
<div class="col-md-12" style="padding-top:10px;">
<a class="round-button red button-block" href="javascript:Register();" tabindex="6">Register</a>
<a class="round-button grey button-block" href="javascript:SwitchToLogin();" style="margin-top:15px;" tabindex="7">Switch to Login</a>
</div>
</div>
<div class="row">
<div class="col-md-12" style="padding:10px;font-size:smaller;">
<p> We'll occasionally email you properties matching your search criteria and will never share your email without your consent.<br/> <a href="https://www.cort.com/privacy-legal" style="pull-right" target="_blank">legal and privacy link</a></p>
</div>
</div>
<div class="row">
<div class="col-md-12" style="padding:10px;font-size:x-small;text-align:center;">
<p> © 2021 CORT Business Services, Inc. All rights reserved. Equal Housing Opportunity</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="modSearching" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content" style="border:none;">
<div class="modal-header" style="background-color: #404040;border-top-left-radius: 6px; border-top-right-radius: 6px;">
<img src="/assets/images/logo-white.png" style="max-height: 39px;"/>
</div>
<div class="modal-body" style="font-size:18pt; font-weight:bold; padding:50px;">
<div style="text-align:center;">
<img alt="searching" src="/assets/images/ajax-loader.gif"/>
</div>
<div id="dSearchingMessage" style="text-align:center; margin-top:10px;">
						Searching for your next appartment....
					</div>
<div id="dSearchingMessage2" style="text-align:center; margin-top:10px; font-size: 10pt; font-style:italic; font-weight: normal;">
<strong>Psst!</strong>  Don't forget to tell them <strong>ApartmentSearch.com</strong> sent you so that you!<br/>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="modTimeout" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content" style="border:none;">
<div class="modal-header" style="background-color: #404040;border-top-left-radius: 6px; border-top-right-radius: 6px;">
<img src="/assets/images/logo-white.png" style="max-height: 39px;"/>
</div>
<div class="modal-body" style="font-size:18pt; font-weight:bold; padding:50px;text-align:center;">
<div id="dSearchingMessage" style="text-align:center; margin:10px 0 10px 0;">
						Your session has timed out, you are being signed out.
					</div>
<a class="btn btn-danger btn-ok" href="/">OK</a>
</div>
</div>
</div>
</div>
<div class="modal fade" id="modTimeoutWarning" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content" style="border:none;">
<div class="modal-header" style="background-color: #404040;border-top-left-radius: 6px; border-top-right-radius: 6px;">
<img src="/assets/images/logo-white.png" style="max-height: 39px;"/>
</div>
<div class="modal-body" style="font-size:18pt; font-weight:bold; padding:50px;text-align:center;">
<div id="dWarningMessage" style="text-align:center; margin:10px 0 10px 0;">
						Your Session Will Time Out In 60 Seconds.
					</div>
<a class="btn btn-success btn-ok" href="javascript:location.reload();" style="margin-right:30px;">Extend</a>
<a class="btn btn-danger btn-ok" href="javascript:LogOff();">Log Off</a>
</div>
</div>
</div>
</div>
<div style="display:none;">
<input id="txtPlaceID" type="text"/>
</div>
<div id="dToolMenu">
</div>
<script src="/scripts/top?v=l_2JQLDpEf4NHz4ob7KHLjmeRFnOogKHIQKkMQQ4Chs1"></script>
<script src="/bundles/bootstrap?v=MpIrNdNWHGwtdUsDo6CO0gGo-NXtj7E_txXB9DLKeNY1"></script>
<script src="/bundles/modernizr?v=wBEWDufH_8Md-Pbioxomt90vm6tJN2Pyy9u9zHtWsPo1"></script>
<script src="/bundles/jquery?v=8hA5hlufggv8KsNt4gQjRUV9yz60nO5jRe-k-3LATUM1"></script>
<script src="//maps-api-ssl.google.com/maps/api/js?key=AIzaSyDMmeDt_NTPjtbKZq-l00lz3n05JrxJ2O8&amp;libraries=places" type="text/javascript"></script>
<script>
        var CustomerID = '';
        var ContactID = '';
        var UserID = '';
        var AffiliateID = '';
        var SiteName = 'ApartmentSearch.com';
        var SecuredSite = false;
        var SalesForceURL = 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8';
        var EmptyGuid = '00000000-0000-0000-0000-000000000000';
        var GoogleAnalyticsAccountID = 'UA-38841601-1';

var console = {};console.log = function(){};window.console = console;	</script>
<link href="/Content/css?v=Y9KPfiRUuOOZ7Ew2CFfr59qZh5nWjDPOKBkG253mFEc1" rel="stylesheet"/>
<script>

			var CaptchaCallback = function () {
				var widgetId1;
				var widgetId2;
				var widgetId3;
				var widgetId4;
				var widgetId5;

				// Register Form
				if ($('#rt_hiddenRecaptcha').length) {
					widgetId1 = grecaptcha.render('RecaptchaField1', { 'sitekey': '6LecQooUAAAAAOZF-OMx3VNtYohKZeOvzTapltBy', 'callback': correctCaptcha_register });
				}

				//Furniture Calculator
				if ($('#ft_hiddenRecaptcha').length) {
					widgetId2 = grecaptcha.render('RecaptchaField2', { 'sitekey': '6LecQooUAAAAAOZF-OMx3VNtYohKZeOvzTapltBy', 'callback': correctCaptcha_furniture });
				}

				//Contact Property
				if ($('#ct_hiddenRecaptcha').length) {
					widgetId3 = grecaptcha.render('RecaptchaField3', { 'sitekey': '6LecQooUAAAAAOZF-OMx3VNtYohKZeOvzTapltBy', 'callback': correctCaptcha_contact });
				}

				//Contact Property - On Property
				if ($('#cpt_hiddenRecaptcha').length) {
					widgetId4 = grecaptcha.render('RecaptchaField4', { 'sitekey': '6LecQooUAAAAAOZF-OMx3VNtYohKZeOvzTapltBy', 'callback': correctCaptcha_contact_property });
				}

				//Claim Reward
				if ($('#crt_hiddenRecaptcha').length) {
					widgetId5 = grecaptcha.render('RecaptchaField5', { 'sitekey': '6LecQooUAAAAAOZF-OMx3VNtYohKZeOvzTapltBy', 'callback': correctCaptcha_claim });
				}
			};
		</script>
<script>

		var correctCaptcha_register = function (response) {
            $("#rt_hiddenRecaptcha").val(response);
		};
		var correctCaptcha_furniture = function(response) {
            $("#ft_hiddenRecaptcha").val(response);
		};
		var correctCaptcha_contact = function(response) {
            $("#ct_hiddenRecaptcha").val(response);
		};
		var correctCaptcha_contact_property = function(response) {
            $("#cpt_hiddenRecaptcha").val(response);
		};
		var correctCaptcha_claim = function(response) {
            $("#crt_hiddenRecaptcha").val(response);
		};


		var checkTimeout;
		var checkWarning;
		var warningTime = 60;
		var ShowReward = true

        $(document).ready(function () {

            $('#tUserName').keydown(function (e) {
				var keyCode = e.keyCode || e.which;
				if (keyCode == 13) {
					LogIn();
					return false;
				}
            });

            $('#tPassword').keydown(function (e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode == 13) {
                    LogIn();
                    return false;
                }
            });


        });

        function CloseLogin() {
            $('#tUserName').val('');
            $('#dLoginActive').hide();
            $('#dLoginError').html('');
            $('#modLogIn').modal('hide');
        }

        function checkWarningTime() {
            if (($("#modTimeout").data('bs.modal') || {}).isShown) {
                $('#modTimeoutWarning').modal("hide");
                return;
            }
            warningTime = warningTime - 1;

            if (warningTime < 0) {
                LogOff();
            }

            $('#modTimeoutWarning').modal("show").find('#dWarningMessage').html('Your Session Will Time Out In ' + warningTime + ' Seconds');
            checkWarning = setTimeout(checkWarningTime, 1000);
        }


        function checkSession() {
            $.ajax({
                url: "/Customer/CheckIfSessionValid",
                type: "POST",
                success: function (result) {
                    if (result.msg == "false" || result.msg == false) {
                        // alert('You have been logged out due to inactivity.');
                        $('#modTimeoutWarning').modal("hide");
                        clearTimeout(checkWarningTime);
                        clearTimeout(checkTimeout);
                        //window.location = "/";
                        $('#modTimeout').modal('show');
                    } else {
                        setupSessionTimeoutCheck();
                    }
                },
                complete: function () {


                }
            });
        }

        function setupSessionTimeoutCheck() {
            clearTimeout(checkTimeout);
            checkTimeout = setTimeout(checkSession, 900000);
        }

	</script>
</body>
</html>
