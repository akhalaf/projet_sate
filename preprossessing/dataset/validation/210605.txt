<!DOCTYPE html>
<html lang="en">
<head><meta content="text/html, charset=UTF-8" http-equiv="Content-Type"/><meta charset="utf-8"/><meta content="IE=edge" http-equiv="X-UA-Compatible"/><meta content="width=device-width, initial-scale=1.0" name="viewport"/><title>
	Bernstein Research
</title><meta content="At AB, we strive to keep clients ahead with actionable ideas and investor insight. We offer a broad range of investment services and resources for individual investors in the US." name="Description"/><meta name="keywords"/><meta property="og:title"/><meta content="https://www.abglobal.com/resources/images/logo.jpg" property="og:image"/><meta content="At AB, we strive to keep clients ahead with actionable ideas and investor insight. We offer a broad range of investment services and resources for individual investors in the US." property="og:description"/><meta name="twitter:title"/><meta name="twitter:description"/>
<!--Google +-->
<link href="https://plus.google.com/+alliancebernstein/posts" rel="author"/><link href="https://plus.google.com/+alliancebernstein/about" rel="publisher"/>
<!--Twitter-->
<meta content="summary" name="twitter:card"/><meta content="@ab_insights" name="twitter:site"/><meta content="@ab_insights" name="twitter:creator"/>
<!--OpenGraph-->
<meta content="article" property="og:type"/><meta content="https://abglobal.com" property="og:site"/><meta content="AB" property="og:site_name"/><meta content="https://abglobal.com" property="og:url"/>
<script src="//assets.adobedtm.com/247cc4026177366ac991447f33e83b0c14ba1521/satelliteLib-473429afd1ffa3b869b5e6af359e2a635dbedf21.js" type="text/javascript"></script>
<script language="javascript" src="../Scripts/Utilities2.js"></script>
<script language="javascript" src="/CmsObjectBR/resources/js/ab-util.js"></script>
<link href="/CmsObjectBR/resources/images/favicon.ico" rel="icon"/>
<link href="/CmsObjectBR/resources/css/ab-local.css?v=20200318" rel="stylesheet" type="text/css"/>
</head>
<!-- DWS39PSC  -->
<body>
<!-- brch-template Start -->
<form action="./Login.aspx?ReturnUrl=%2fbrweb%2fHome.aspx" id="aspnetForm" method="post">
<input id="__VIEWSTATE" name="__VIEWSTATE" type="hidden" value="Sz8mo2jh/aqCCArv39nKqFeDhqC2ym2zmV77CEFJgxgwUXuFjcPfvRJLdaWWTQvPQpsG/C3IEqCIHYRpSBDUI5X4fe2TjjiyVgsUw11Uf2xLJ0ID6aDdM91W6x4IyKkDp0eQaPcSu4X2pqkAcDIoMVzq8kki+9OewJPfImUjFycEIeK08JYFRr7CsG6mQgm0884X62vq9am8tCGfrLSsafUNUbWyLqKDllK/C/RXhzFlJzfQB5uSaHJYcQhnS5kJvqyTBF2NBdE7x+r7LL3NSNFAobYD+/Emy/OsVz+SB88JxgMqrvW6sNhK94bNqjXnG7T+6AT13r70KgOJvJLPPhM0G4k="/>
<script src="/brweb/ScriptResource.axd?d=Sfktcq_awEbhXTk2mnx2JdXQO9UGsxU3gCEVIErOPy9bwDUdLwj2qmSD7b8_Qht2EFA4F-6aJjE-dJumh9B7jI8nbPZgXKcE6qmD8BXYnPHOUJYd0&amp;t=f2cd5c5" type="text/javascript"></script>
<input id="__VIEWSTATEGENERATOR" name="__VIEWSTATEGENERATOR" type="hidden" value="324FF8C9"/>
<input id="__VIEWSTATEENCRYPTED" name="__VIEWSTATEENCRYPTED" type="hidden" value=""/>
<input id="__EVENTVALIDATION" name="__EVENTVALIDATION" type="hidden" value="/ojoqrXvL7PAOanwiHTDvqd0spDa0c/SqMQ8owR88VLlWTrmfn9krBkC42na2AL8OxgzNEGfrWlLZyM1ijafNB4KO5NYMYdICe6HNWhNgzRXilA6Usnm7iUflFtdXEvrn658zEE+WuShzlxiBL3DleTGRGI8okBL8wD+nXEOoj+ObyG4QWKL5UjxLD8Q8t2NAqfEenh2JFnki6pX1t1fxO+am+TX6ng3JALzfg=="/>
<div class="ab-container-full brch-template brch-capabilities">
<!-- gdpr login -->
<script>
</script>
<!-- Local Navigation Start -->
<div class="ab-container-full ab-local-desktop ab-local-nav brch-nav-bar">
<div class="outer-wrapper">
<div class="ab-container">
<div class="ab-row">
<button class="navbar-toggle" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
<div class="ab-col-3 ab-submenu abpl-grid-col-block"> <a href="/brweb/Home.aspx"><img alt="AB Bernstein" class="brch-branding" src="/CmsObjectBR/resources/images/ab-logo.png" title="Bernstein Research"/></a> </div>
<nav class="ab-retail-nav abpl-grid-col-block ab-col-9 section-to-collapse">
<ul>
<li><a href="/brweb/Home.aspx">Home</a> </li>
<li><a href="/BernsteinTrading/index.html">Trading</a></li>
<li><a href="javascript:OpenCmaContentPopup('/brweb', '9912', '82937');">Contact Us</a></li>
<li>
<a href="https://www.autonomous.com" id="atnm" target="_blank"><img alt="Autonomous" id="atnm-logo" src="/CMSObjectBR/Resources/images/Autonomous-logo.png" title="Autonomous"/></a>
</li>
<li><a class="brch-login-trgr" href="javascript:;">Login</a></li>
</ul>
</nav>
</div>
</div>
</div>
<div class="overlay-background" id="soBackground"></div>
<div class="clear-fix"></div>
</div>
<!-- Local Navigation End -->
<!-- MainContent <BEGIN> -->
<!-- brch-promo-bar Start -->
<div class="ab-container-full brch-background-dark">
<div class="outer-wrapper">
<div class="brch-hero-capabilities-wrapper">
<div class="brch-hero-capabilities">
<!--************************************ start HERO IMG*************************************************************************** -->
<!--**************************************************************************************************************** -->
<img alt="Independent Clarity. Trusted Research." src="/CmsObjectBR/resources/images/brch-hero-capabilities.jpg" title="Independent Clarity. Trusted Research."/>
<!--********************************************end HERO IMG******************************************************************** -->
<!--**************************************************************************************************************** -->
</div>
<div class="ab-container">
<div class="ab-row">
<div class="brch-tagline ab-container">
<h1><span class="ab-font-light">KEEPING OUR CLIENTS</span> AHEAD OF TOMORROW®</h1>
<h2>Research Has Always Been Bernstein's Calling Card</h2>
<p>Widely recognized as Wall Street's premier sell-side research and brokerage firm, we deliver trusted research and execution that help our clients stay ahead of what's next.</p>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="ab-container-full brch-background-dark">
<div class="outer-wrapper">
<div class="ab-container">
<div class="ab-row abpl-main-cnt">
<div class="brch-hero-teaser">
<div class="ab-col-8 abpl-grid-col-block">
<!--************************************ start HERO*************************************************************************** -->
<!--**************************************************************************************************************** -->
<h2><span>Independent Clarity.</span> <span>Trusted Research.</span></h2>
<p>The Bernstein Blackbook embodies our ability to cut through complexity with independent clarity, generating exclusive insights that reframe perspectives and strategies across markets and sectors.</p>
<br/><br/>
<p style="font-size:18px">Please note that Bernstein Research is intended for and may be distributed to eligible institutional clients only.</p>
<!--********************************************end HERO******************************************************************** -->
<!--**************************************************************************************************************** -->
</div>
<div class="ab-col-4 abpl-grid-col-block abcom-pull-text-right"><img alt="Independent Clarity. Trusted Research." src="/CmsObjectBR/resources/images/icn-globe.png" title="Independent Clarity. Trusted Research."/></div>
</div>
</div>
<!--	//ab-row ab-row brch-hero-teaser-->
</div>
</div>
</div>
<!--**************************************************************************************************************** -->
<!--**************************************************************************************************************** -->
<div class="ab-container-full">
<div class="outer-wrapper">
<div class="ab-container">
<div class="ab-row abpl-main-cnt">
<div class="ab-col-12 abpl-grid-col-block">
<h2>A Reputation for Excellence</h2>
<article class="brch-cards brch-cards-dark-theme">
<section class="ab-col-4 abpl-grid-col-block">
<div><span>  </span>
<aside>#1 Ranked for Quality in the US for 17 Straight Years</aside>
</div>
</section>
<section class="ab-col-4 abpl-grid-col-block">
<div><span> </span>
<aside>#1 Ranked for Written Research in the US </aside>
</div>
</section>
<section class="ab-col-4 abpl-grid-col-block">
<div><span> </span>
<aside>#1 Ranked for Best Knowledge of US Companies and Industries for 16 Straight Years </aside>
</div>
</section>
<section class="ab-col-4 abpl-grid-col-block">
<div><span> </span>
<aside>#1 Ranked for Best Knowledge of European Companies and Industries for 10 Straight Years</aside>
</div>
</section>
<section class="ab-col-4 abpl-grid-col-block">
<div><span> </span>
<aside>#1 Ranked US Sales Team Overall Capability</aside>
</div>
</section>
<section class="ab-col-4 abpl-grid-col-block">
<div><span> </span>
<aside>#1 Ranked US Sales Team Intensity of Coverage</aside>
</div>
</section>
<section class="ab-col-4 abpl-grid-col-block">
<div><span> </span>
<aside>Top-3 Ranked for Most Important Relationship for Large-Cap Corporate Access </aside>
</div>
</section>
<section class="ab-col-4 abpl-grid-col-block">
<div><span> </span>
<aside>Eighteen Top-Ranked Analysts in Institutional Investor's 2019 All-America Research Team</aside>
</div>
</section>
<section class="ab-col-4 abpl-grid-col-block">
<div><span> </span>
<aside>Seven #1 and 13 Top-Ranked Analysts in Institutional Investor's 2019 All-Europe Research Team </aside>
</div>
</section>
</article>
<!--**************************************************************************************************************** -->
<!--**************************************************************************************************************** -->
<!--**************************************************************************************************************** -->
<!--**************************************************************************************************************** -->
</div>
</div>
</div>
</div>
</div>
<!--**************************************************************************************************************** -->
<!--**************************************************************************************************************** -->
<div class="ab-container-full ab-background-lowlight brch-our-people">
<div class="outer-wrapper">
<div class="ab-container">
<div class="ab-row abpl-main-cnt">
<div class="ab-col-12 abpl-grid-col-block">
<h2 class="ab-mc-header">Our People</h2>
<!--React Our People-->
<div id="brch_our_people"> </div>
<!--**************************************************************************************************************** -->
<!--**************************************************************************************************************** -->
<!--**************************************************************************************************************** -->
<!--**************************************************************************************************************** -->
</div>
</div>
</div>
</div>
</div>
<!--		insights Featuted -->
<div class="ab-container-full ab-background-lowlight">
<div class="outer-wrapper abtl-bg-featured abtl-featured">
<img alt="Is the Price Right? A New Toolkit for European Investors" src="https://www.alliancebernstein.com/sites/library/uploads/b-Hero-IsthePriceRightANewToolkitforEuropeanInvestors.w.jpg" title="Bernstein Trading"/>
<div class="tl-hero-overlay"></div>
<aside>
<div class="ab-container">
<header>Bernstein Trading</header>
<h2>We are a market leader in providing best-in-class fundamental research and full service execution services to institutional clients globally.</h2>
<p></p>
<a href="https://www.bernsteinresearch.com/CmsObjectBR/bt/index.htm" target="_blank"><div class="abtl-button">Learn More</div></a>
</div>
</aside>
</div>
</div>
<!--		insights Featuted -->
<!--**************************************************************************************************************** -->
<!--**************************************************************************************************************** -->
<div class="ab-container-full">
<div class="outer-wrapper">
<div class="ab-container">
<div class="ab-row abpl-main-cnt">
<div class="ab-col-12 abpl-grid-col-block">
<article class="brch-cards brch-cards-extended-footer">
<section class="ab-col-6 abpl-grid-col-block">
<aside><h2>How Can We Help?</h2><p>If you'd like to learn more about Bernstein's insights and execution or how they can help advance your business, please contact us.</p><a class="abtl-button" href="mailto:prospects@bernstein.com?">Contact Us</a></aside>
</section>
<section class="ab-col-6 abpl-grid-col-block">
<aside>
<!--<header>CAREER OPPORTUNITIES</header>-->
<h2>Commission Management Solutions</h2><p>Sanford C. Bernstein's Commission Management Solutions team offers institutional clients global capabilities and custom solutions combining best trade execution and seamless administration of research payments to providers around the world.</p><a class="abtl-button" href="https://www.bernsteinresearch.com/BRWEB/Public/CmsContent.aspx?cid=76935&amp;nid=9912">Apply Today</a></aside>
</section>
</article>
<!--**************************************************************************************************************** -->
<!--**************************************************************************************************************** -->
<!--**************************************************************************************************************** -->
<!--**************************************************************************************************************** -->
</div>
</div>
</div>
</div>
</div>
<!--		Start Login			Dialog     -->
<div class="brch-login-dialog" id="ctl00_BRContentPlaceHolder_brchloginDialog" title="account access">
<div id="ctl00_BRContentPlaceHolder_upLoginPanel">
<div class="brch-acc-access">
                                Please note that Bernstein Research is intended for and may be distributed to eligible institutional clients only.<input id="ctl00_BRContentPlaceHolder_txtCsrfToken" name="ctl00$BRContentPlaceHolder$txtCsrfToken" type="hidden" value="BR48AZ0G134DM7XN63M6"/>
<input aria-label="Enter Email" class="brch-text-field user-name-text" id="ctl00_BRContentPlaceHolder_txtUserName" name="ctl00$BRContentPlaceHolder$txtUserName" placeholder="Email" required="" type="text"/>
<input aria-label="Enter Password" autocomplete="off" class="brch-text-field user-pswd-text" id="ctl00_BRContentPlaceHolder_txtPassword" name="ctl00$BRContentPlaceHolder$txtPassword" placeholder="Password" required="" type="password"/>
<div class="brch-acc-details">
<input aria-label="Remember me checkbox" checked="checked" id="ctl00_BRContentPlaceHolder_cbRememberUser" name="ctl00$BRContentPlaceHolder$cbRememberUser" type="checkbox"/>Keep me signed in
                                    <a class="brch-forgot-pass" href="#" id="brch-forgot-pass" onclick="ShowForgotPwdWindow()">Forgot Password?</a>
</div>
<div id="error_message">
<div class="error_title"></div>
<div class="error_content"></div>
<!--error_content-->
</div>
<div class="brch-login-error"></div>
<div>
<input class="brch-btn-submit" id="ctl00_BRContentPlaceHolder_btnLogin" name="ctl00$BRContentPlaceHolder$btnLogin" type="submit" value="Login"/>
</div>
</div>
<input class="hdnLoginError" id="ctl00_BRContentPlaceHolder_hdnLoginError" name="ctl00$BRContentPlaceHolder$hdnLoginError" type="hidden" value="false"/>
</div>
</div>
<!--			END	Login		Dialog     -->
<!-- brch-template End -->
<script src="/CmsObjectBR/resources/js/lib/jquery-latest.min.js?v=20190109"></script>
<script src="/CmsObjectBR/resources/js/ab-local.js?v=202002261222"></script>
<script src="/CmsObjectBR/resources/js/brweb-ourpeople-bundle.js?v=20200226"></script>
<script src="/CmsObjectBR/Resources/js/Components/Login.js?v=1343B85" type="text/javascript"></script>
<!-- MainContent <END> -->
<link href="/CmsObjectBR/Styles/Footer.css?v=20200317" rel="stylesheet" type="text/css"/>
<div class="ab-container-full">
<div id="footer_br">
<!-- footer_links -->
<br/>
<a href="javascript:NewWindow('https://www.bernsteinresearch.com/CMSObjectBR/disclosures.pdf', '900', '700', 'yes', 'yes', 'Disclosures')">Bernstein Research Disclosures</a> | 
<a href="javascript:NewWindow('https://www.autonomous.com/legal.html', '900', '700', 'yes', 'yes', 'Disclosures')">Autonomous Research Disclosures</a> | 
<a href="javascript:OpenCmsContentPopup('https://www.bernsteinresearch.com/brweb', '9920', '50889');">Terms of Use</a> |
<a href="javascript:OpenCmsContentPopup('https://www.bernsteinresearch.com/brweb', '9920', '50887');">Privacy Policy</a> | 
<a href="javascript:OpenCmsContentPopup('https://www.bernsteinresearch.com/brweb', '9920', '50884');">Business Continuity Statement</a> | 

<a href="javascript:OpenCmsContentPopup('https://www.bernsteinresearch.com/brweb', '9920', '63443');">Reports pursuant to SEC Rules 605 and 606</a> |

<a href="javascript:NewWindow('https://www.bernsteinresearch.com/CMSObjectBR/BA_LLP_Complaints_Policy.pdf', '900', '700');">BA LLP Complaints Policy</a> | 

<a href="javascript:NewWindow('https://www.bernsteinresearch.com/CMSObjectBR/BA_LLP_Conflicts_of_Interest_Policy.pdf', '900', '700');">BA LLP Conflicts of Interest Policy</a> | 

<a href="javascript:OpenCmsContentPopup('https://www.bernsteinresearch.com/brweb', '9920', '50888');">Program Trading Risk Disclosures</a> | 
<a href="javascript:NewWindow('https://www.bernsteinresearch.com/CMSObjectBR/SCBL_Pillar_3_Disclosures.pdf', '900', '700', 'yes', 'yes', 'SCBL Pillar 3 Disclosures')">SCBL Pillar 3 Disclosures</a> |

<a href="javascript:NewWindow('https://www.bernsteinresearch.com/CMSObjectBR/BA_LLP_Order_Execution_Policy.pdf', '900', '700', 'yes', 'yes', 'Order Execution Policy')">BA LLP Order Execution Policy</a> | 

<a href="javascript:NewWindow('https://www.bernsteinresearch.com/brweb/viewresearch.aspx?cid=PWePlT531926L770g4zvaeempb6i2twa4q5iqpbztj7bp3fy6thyxrufkzbbbptl2rvleditp2rham3xr5xcqretzfudq1', '900', '700', 'yes', 'yes', 'MAR Recommendations')">Bernstein Research MAR Recommendations</a> |

<a href="javascript:NewWindow('https://www.bernsteinresearch.com/brweb/viewresearch.aspx?cid=PWePlT531927L770u6fq25g3rxbzurpjjovpd2iqy454pga7tfxmsvwd5kycsxatseauiz6klli7qlnxd75qdyrnovb6u1', '900', '700', 'yes', 'yes', 'MAR Recommendations')">Bernstein Sales and Trading MAR Recommendations</a> |

<a href="javascript:NewWindow('https://www.bernsteinresearch.com/CMSObjectBR/Bernstein_Pairs_Terms_of_Use.pdf', '900', '700', 'yes', 'yes', 'SCBL Pillar 3 Disclosures')">Bernstein Pairs Terms of Use</a> |

<a href="javascript:NewWindow('https://www.bernsteinresearch.com/CMSObjectBR/Disclosures _SCB_LLC.pdf', '900', '700', 'yes', 'yes', 'After Hours Trading Disclosure')">SCB LLC Disclosures</a> | 

<a href="javascript:NewWindow('https://www.bernsteinresearch.com/CMSObjectBR/Swiss_Bankers_Disclosure.pdf', '900', '700', 'yes', 'yes', 'Swiss Bankers Association Risk Disclosure Statement')">Swiss Bankers Association Risk Disclosure Statement</a> | 


<a href="javascript:NewWindow('https://www.bernsteinresearch.com/CMSObjectBR/ALP_Guidelines.pdf', '900', '700', 'yes', 'yes', 'SCBL Best Execution Policy')">SCB HK Disclosures</a> | 

<a href="javascript:NewWindow('https://www.bernsteinresearch.com/CMSObjectBR/SCBL_Modern_Slavery_Act_Statement.pdf', '900', '700', 'yes', 'yes', 'SCBL Modern Slavery Act Statement')">SCBL Modern Slavery Act Statement</a> | 

<a href="javascript:NewWindow('https://www.bernsteinresearch.com/CMSObjectBR/SanctionList.pdf', '900', '700', 'yes', 'yes', 'EO 13959 – Restricted Securities')">EO 13959 – Restricted Securities</a> | 


<a href="mailto:support@bernstein.com">Support</a> | 

<a href="https://www.bernsteinresearch.com/BRWEB/Public/CmsContent.aspx?nid=9912&amp;cid=82937">Contact Us</a>
<!-- footer_text -->
<p>
    On and as of April 1, 2019, AllianceBernstein L.P. acquired Autonomous Research. As a result of the acquisition, the research activities formerly conducted by Autonomous Research US LP were assumed by Sanford C. Bernstein &amp; Co., LLC, which continues to publish research under the Autonomous Research US brand and the research activities formerly conducted by Autonomous Research Asia Limited were assumed by Sanford C. Bernstein (Hong Kong) Limited 盛博香港有限公司, which continues to publish research under the Autonomous Research Asia brand.<br/>
<br/>On and after close of business on December 31, 2020, as part of an internal reorganisation of the corporate group, Sanford C. Bernstein Limited transferred its business to its affiliate Autonomous Research LLP. Subsequent to this transfer, Autonomous Research LLP changed its name to Bernstein Autonomous LLP. As a result of the reorganisation, the research activities formerly conducted by Sanford C. Bernstein Limited were assumed by Bernstein Autonomous LLP, which is authorised and regulated by the Financial Conduct Authority (FRN 500498) and now publishes research under the Bernstein Research brand. For more information about the Autonomous brand, please go to <a href="https://www.autonomous.com/">https://www.autonomous.com/</a>.<br/>
<br/>Bernstein Autonomous LLP is authorised and regulated by the Financial Conduct Authority in the United Kingdom (FRN: 500498) and registered in England and Wales No. OC343985. The company has its registered office at 50 Berkeley Street, London W1J 8SB. <br/>
<br/>Sanford C. Bernstein &amp; Co., LLC, is a broker-dealer registered with the United States Securities and Exchange Commission, with its offices at 1345 Avenue of the Americas, New York, NY 10105 ~ 212-756-4400. Member, New York Stock Exchange, <a href="https://www.finra.org" target="_blank">FINRA</a>, SIPC. <br/>
<br/>Sanford C. Bernstein (Hong Kong) Limited 盛博香港有限公司 is licensed and regulated in Hong Kong by the Securities and Futures Commission (Central Entity No. AXC846).  It is incorporated in Hong Kong with limited liability, and has its registered office at 39th Floor, One Island East, Taikoo Place, 18 Westlands Road, Quarry Bay, Hong Kong.<br/>
<br/>Sanford C. Bernstein in Singapore operates as a unit of AllianceBernstein (Singapore) Ltd. which is a licensed entity under the Securities and Futures Act and registered with Company Registration No. 199703364C. AllianceBernstein (Singapore) Ltd. is regulated by the Monetary Authority of Singapore, and has its office at One Raffles Quay, #27-11 South Tower, Singapore 048583. The business name “Bernstein” is registered under business registration number 53193989L.<br/>
<br/>Sanford C. Bernstein (India) Private Limited is a private limited company incorporated under the Companies Act, 2013, on April 12, 2017 bearing corporate identification number U65999MH2017FTC293762 and is licensed and regulated by Securities and Exchange Board of India (SEBI) as Research Analyst entity with registration no. INH000006378 and as Stock Broker with registration no: INZ000213537. It has its registered office at Level 6, 4 North Avenue, Maker Maxity, Bandra Kurla Complex, Bandra (East), Mumbai 400051, Maharashtra, India (Phone No: +91-22-68421401).<br/>
<br/>Sanford C. Bernstein Ireland Limited is regulated by the Central Bank of Ireland. The registered office is located at 4 Earlsfort Terrace, D02 E024, Dublin, Ireland. The company registered number is 627809.<br/>
<br/>Copyright
    <script>document.write(Math.max(2014, new Date().getFullYear()));</script>, Sanford C. Bernstein &amp; Co., LLC, Bernstein Autonomous LLP, Sanford C. Bernstein (Hong Kong) Limited 盛博香港有限公司, Sanford C. Bernstein (India) Private Limited, and AllianceBernstein (Singapore) Ltd., subsidiaries of <a href="https://www.alliancebernstein.com">AllianceBernstein L.P.</a> All rights reserved.
</p>
</div>
</div>
<!-- footer_global -->
<div id="footer_global">
<ul id="footer_data">
<li>
<a href="https://www.abglobal.com/#/">&amp;commat;AllianceBernstein L.P.</a>
</li>
<li>
<a href="javascript:OpenCmaContentPopup('/brweb', '9920', '50889');">Terms of Use</a>
</li>
<li>
<a href="javascript:OpenCmaContentPopup('/brweb', '9920', '50887');">Privacy Policy</a>
</li>
<li>
<a href="javascript:OpenCmaContentPopup('/brweb', '9920', '50884');">Business Continuity Plan</a>
</li>
<li>
<a href="javascript:OpenCmaContentPopup('/brweb', '9912', '82937');">Contact Us</a>
</li>
</ul>
</div>
<script type="text/javascript">
    var ABSource = 'BernsteinResearch.com'; var InternalExternal = 'External'; var ServerRegion = 'PSC'; var ServerNumber = 'S39';        
</script>
<script type="text/javascript">
    try {
        _satellite.pageBottom();
    }
    catch (err) { }
</script>
<!-- brch-template End -->
</div>
</form>
</body>
</html>
