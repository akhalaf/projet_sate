<html>
<head>
<title>- MailCheck -</title>
<style type="text/css">


body {
margin: 0;
padding: 0;
color: #999999;
background: #ffffff;
font-family: times;
font-size: 1.5em;
text-align: center;
}


#main {
width: 210px;
position: absolute;
top: 50px;
left: 50%;
margin-left: -105px;
text-align: center;
}

img {
	border: 0;
}

</style>
</head>
<body><div id="main">
<b><i>Click to read WebMail</i></b>
<br/>
<br/>
<a href="https://webmail.edilsimsrl.net">
<img alt="WebMail" height="210" src="img/ReadMail.jpg" width="210"/>
</a></div>
<!-- Codice per accettazione cookie - Inizio -->
<script type="text/javascript">
//<![CDATA[
(function(window) {
  if (!!window.cookieChoices) {
    return window.cookieChoices;
  }
  var document = window.document;  
  var supportsTextContent = 'textContent' in document.body;
  var cookieChoices = (function() {
    var cookieName = 'displayCookieConsent';
    var cookieConsentId = 'cookieChoiceInfo';
    var dismissLinkId = 'cookieChoiceDismiss';
    function _createHeaderElement(cookieText, dismissText, linkText, linkHref) {
      var butterBarStyles = 'position:fixed;width:100%;background-color:#eee;' +
          'margin:0; left:0; top:0;padding:4px;z-index:1000;text-align:center;';
      var cookieConsentElement = document.createElement('div');
      cookieConsentElement.id = cookieConsentId;
      cookieConsentElement.style.cssText = butterBarStyles;
      cookieConsentElement.appendChild(_createConsentText(cookieText));
      if (!!linkText && !!linkHref) {
        cookieConsentElement.appendChild(_createInformationLink(linkText, linkHref));
      }
      cookieConsentElement.appendChild(_createDismissLink(dismissText));
      return cookieConsentElement;
    }
    function _createDialogElement(cookieText, dismissText, linkText, linkHref) {
      var glassStyle = 'position:fixed;width:100%;height:100%;z-index:999;' +
          'top:0;left:0;opacity:0.5;filter:alpha(opacity=50);' +
          'background-color:#ccc;';
      var dialogStyle = 'z-index:1000;position:fixed;left:50%;top:50%';
      var contentStyle = 'position:relative;left:-50%;margin-top:-25%;' +
          'background-color:#fff;padding:20px;box-shadow:4px 4px 25px #888;';
      var cookieConsentElement = document.createElement('div');
      cookieConsentElement.id = cookieConsentId;
      var glassPanel = document.createElement('div');
      glassPanel.style.cssText = glassStyle;
      var content = document.createElement('div');
      content.style.cssText = contentStyle;
      var dialog = document.createElement('div');
      dialog.style.cssText = dialogStyle;
      var dismissLink = _createDismissLink(dismissText);
      dismissLink.style.display = 'block';
      dismissLink.style.textAlign = 'right';
      dismissLink.style.marginTop = '8px';
      content.appendChild(_createConsentText(cookieText));
      if (!!linkText && !!linkHref) {
        content.appendChild(_createInformationLink(linkText, linkHref));
      }
      content.appendChild(dismissLink);
      dialog.appendChild(content);
      cookieConsentElement.appendChild(glassPanel);
      cookieConsentElement.appendChild(dialog);
      return cookieConsentElement;
    }
    function _setElementText(element, text) {
      if (supportsTextContent) {
        element.textContent = text;
      } else {
        element.innerText = text;
      }
    }
    function _createConsentText(cookieText) {
      var consentText = document.createElement('span');
      _setElementText(consentText, cookieText);
      return consentText;
    }
    function _createDismissLink(dismissText) {
      var dismissLink = document.createElement('a');
      _setElementText(dismissLink, dismissText);
      dismissLink.id = dismissLinkId;
      dismissLink.href = '#';
      dismissLink.style.marginLeft = '24px';
      return dismissLink;
    }
    function _createInformationLink(linkText, linkHref) {
      var infoLink = document.createElement('a');
      _setElementText(infoLink, linkText);
      infoLink.href = linkHref;
      infoLink.target = '_blank';
      infoLink.style.marginLeft = '8px';
      return infoLink;
    }
    function _dismissLinkClick() {
      _saveUserPreference();
      _removeCookieConsent();
      return false;
    }
    function _showCookieConsent(cookieText, dismissText, linkText, linkHref, isDialog) {
      if (_shouldDisplayConsent()) {
        _removeCookieConsent();
        var consentElement = (isDialog) ?
            _createDialogElement(cookieText, dismissText, linkText, linkHref) :
            _createHeaderElement(cookieText, dismissText, linkText, linkHref);
        var fragment = document.createDocumentFragment();
        fragment.appendChild(consentElement);
        document.body.appendChild(fragment.cloneNode(true));
        document.getElementById(dismissLinkId).onclick = _dismissLinkClick;
      }
    }
    function showCookieConsentBar(cookieText, dismissText, linkText, linkHref) {
      _showCookieConsent(cookieText, dismissText, linkText, linkHref, false);
    }
    function showCookieConsentDialog(cookieText, dismissText, linkText, linkHref) {
      _showCookieConsent(cookieText, dismissText, linkText, linkHref, true);
    }
    function _removeCookieConsent() {
      var cookieChoiceElement = document.getElementById(cookieConsentId);
      if (cookieChoiceElement != null) {
        cookieChoiceElement.parentNode.removeChild(cookieChoiceElement);
      }
    }
    function _saveUserPreference() {
      // Durata del cookie di un anno
      var expiryDate = new Date();
      expiryDate.setFullYear(expiryDate.getFullYear() + 1);
      document.cookie = cookieName + '=y; expires=' + expiryDate.toGMTString();
    }
    function _shouldDisplayConsent() {
      // Per mostrare il banner solo in mancanza del cookie
      return !document.cookie.match(new RegExp(cookieName + '=([^;]+)'));
    }
    var exports = {};
    exports.showCookieConsentBar = showCookieConsentBar;
    exports.showCookieConsentDialog = showCookieConsentDialog;
    return exports;
  })();
  window.cookieChoices = cookieChoices;
  return cookieChoices;
})(this);
document.addEventListener('DOMContentLoaded', function(event) {
    cookieChoices.showCookieConsentBar('Questo sito utilizza i cookie per migliorare servizi ed esperienza dei lettori. Se decidi di continuare la navigazione consideriamo che accetti il loro uso.',
        'OK', 'Informazioni', 'http://www.ideepercomputeredinternet.com/p/politica-dei-cookie.html');
  });
//]]> 
</script>
<!-- Codice per accettazione cookie - Fine -->
</body></html>
