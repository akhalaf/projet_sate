<!DOCTYPE html>
<html lang="en-US">
<head>
<title> Fa Consulting</title>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"https:\/\/faconsulting-ks.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.3.25"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://faconsulting-ks.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.3" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://faconsulting-ks.com/wp-content/plugins/mailchimp-for-wp/assets/css/checkbox.min.css?ver=2.3.17" id="mailchimp-for-wp-checkbox-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://faconsulting-ks.com/wp-content/themes/faconsulting/css/jquery.bxslider.css?ver=4.3.25" id="BxSlider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://faconsulting-ks.com/wp-content/themes/faconsulting/css/bootstrap.min.css?ver=4.3.25" id="Bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://faconsulting-ks.com/wp-content/themes/faconsulting/css/font-awesome.min.css?ver=4.3.25" id="Font-Awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://faconsulting-ks.com/wp-content/themes/faconsulting/css/normalize.css?ver=4.3.25" id="Normalize-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://faconsulting-ks.com/wp-content/themes/faconsulting/style.css?ver=4.3.25" id="Style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://faconsulting-ks.com/wp-content/themes/faconsulting/css/main.css?ver=4.3.25" id="Main-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://faconsulting-ks.com/wp-content/themes/faconsulting/css/responsive.css?ver=4.3.25" id="Responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://faconsulting-ks.com/wp-content/plugins/mailchimp-for-wp/assets/css/form.min.css?ver=2.3.17" id="mailchimp-for-wp-form-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://faconsulting-ks.com/wp-includes/js/jquery/jquery.js?ver=1.11.3" type="text/javascript"></script>
<script src="https://faconsulting-ks.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1" type="text/javascript"></script>
<link href="https://faconsulting-ks.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://faconsulting-ks.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.3.25" name="generator"/>
<link href="https://faconsulting-ks.com/" rel="canonical"/>
<link href="https://faconsulting-ks.com/" rel="shortlink"/>
</head>
<body class="home page page-id-5 page-template-default">
<!-- Header -->
<header id="header">
<div class="subheader-one">
<div class="container">
<div class="row">
<div class="left-content">
<p>Ne jemi FA Consulting, na lejoni t’ju zvoglojmë problemet</p>
</div>
<div class="right-content">
<span><i class="fa fa-phone"></i> +377 44 261 147</span>
<span><i class="fa fa-home"></i> Prishtina, Ks</span>
</div>
</div>
</div>
</div>
<div class="subheader-two">
<div class="strips"></div>
<nav class="navbar navbar-default">
<div class="container">
<div class="container-fluid">
<div class="navbar-header">
<button aria-expanded="false" class="navbar-toggle collapsed" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="https://faconsulting-ks.com"><img alt="Fa Consulting" src="http://faconsulting-ks.com/wp-content/uploads/2015/12/logo.png"/></a>
</div>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<div class="menu-header-menu-container"><ul class="nav navbar-nav navbar-right" id="menu-header-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-5 current_page_item menu-item-10" id="menu-item-10"><a href="https://faconsulting-ks.com/">Ballina</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18" id="menu-item-18"><a href="https://faconsulting-ks.com/per-ne/">Për Ne</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21" id="menu-item-21"><a href="https://faconsulting-ks.com/sherbimet/">Shërbimet</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9" id="menu-item-9"><a href="https://faconsulting-ks.com/kontakt/">Kontakt</a></li>
</ul></div>
</div>
</div>
</div>
</nav>
</div>
</header>
<section id="home-bg" style="background-image: url('https://faconsulting-ks.com/wp-content/uploads/2015/10/home-faconsulting.jpg')">
<div class="home-inner">
<div class="home-content">
<h1> Biznesi juaj me ne është <span>në duar të sigurta</span> </h1>
<p>Me shërbimet tona profesionale ju përfitoni zgjidhje në tatime për individ, biznese apo OJQ, këshilla në hapje të biznesit, konsulta në tatime, deklarime, etj. Gjithashtu ne ofrojmë dhe kontabilitet financiar, mbajtje të librave dhe regjistrimeve konform Legjislacionit Tatimor, Analiza financiare, ndihmë rreth rrjedhjes së keshit dhe analiza, përgatitje të pasqyrave financiare dhe raportimi periodik, Kontabilitet menaxherial, etj.</p>
<a class="btn-left" href="/sherbimet" target="_blank">Lexo më shumë</a>
<a class="btn-right" href="/kontakt" target="_blank">Na Kontaktoni</a>
</div>
</div>
</section>
<section id="icons">
<div class="container">
<div class="row">
<div class="col-md-3">
<a href="/sherbimet">
<div class="thumbnail">
<div class="icons">
<i class="fa fa-file-text-o"></i>
</div>
<div class="caption">
<h4> Konsultime rreth tatimeve </h4>
<p>Veproni shpejtë dhe në kohë me pagesa të tatimeve dhe deklarimet përkatëse. </p>
</div>
</div>
</a>
</div>
<div class="col-md-3">
<a href="/sherbimet">
<div class="thumbnail">
<div class="icons">
<i class="fa fa-line-chart"></i>
</div>
<div class="caption">
<h4>Kontabilitet Financiar</h4>
<p>Përmbledhje, analiza dhe raportime të transaksioneve financiare të një biznesi.</p>
</div>
</div>
</a>
</div>
<div class="col-md-3">
<a href="/sherbimet">
<div class="thumbnail">
<div class="icons">
<i class="fa fa-bar-chart"></i>
</div>
<div class="caption">
<h4>Kontabilitet Menaxherial</h4>
<p>Kuptoni çfarë ka rëndësi para se të vendosni për funksionet kontrolluese në performancë.</p>
</div>
</div>
</a>
</div>
<div class="col-md-3">
<a href="/sherbimet">
<div class="thumbnail">
<div class="icons">
<i class="fa fa-file-text"></i>
</div>
<div class="caption">
<h4>Menaxhimi Financiar</h4>
<p>Menaxhimi efektiv dhe efikas i fondeve për arritjen e caqeve të kompanisë.</p>
</div>
</div>
</a></div>
</div>
</div>
</section>
<br/>
<b>Warning</b>:  Use of undefined constant ASC - assumed 'ASC' (this will throw an Error in a future version of PHP) in <b>/home/faconsulting/public_html/wp-content/themes/faconsulting/front-page.php</b> on line <b>75</b><br/>
<section id="testimonials">
<div class="container">
<div class="row">
<div class="testimonials">
<ul class="testimonials-js">
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2015/12/zombiesoup.png"/>
<h3>“Jo vetëm që përkujdeset për financat e kompanisë sonë, por sigurohet që ne të kemi  projektime reale për një të ardhme stabile.”</h3>
<!-- <p>  </p> -->
<span> Agon Avdimetaj, Appsix LLC </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2016/12/ags.png"/>
<h3>“Këshillat dhe shërbimet e tyre na ndihmojnë shumë në menaxhimin e financave në përgjithësi dhe kontabilitetit financiar në veçanti”</h3>
<!-- <p>  </p> -->
<span> Krenare Shala, Allmakes Global Services NPSH </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2016/12/kosovaprojekt.png"/>
<h3>“Shërbime të shpejta, profesionale dhe korrekte. Ndihma juaj në projeksionet financiare ka qenë e jashtëzakonshme”</h3>
<!-- <p>  </p> -->
<span> Arif Rukiqi, Kosova Projekt </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2016/12/projoni.png"/>
<h3>“Profesional, shërbime sipas kodit të etikës profesionale”</h3>
<!-- <p>  </p> -->
<span> Fatijon Ibrahimi, Projoni </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2016/12/lighthouse.png"/>
<h3>“Shërbime të sakta, korrekte dhe shumë të shpejta që garantojnë suksesin e biznesit tuaj ”</h3>
<!-- <p>  </p> -->
<span> Arben Surdulli, Lighthouse SH.P.K. </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2016/12/pro4.png"/>
<h3>“Me kompaninë Fa Consulting kemi bashkëpunim të gjatë. Do të thoja se janë shumë korrekt, të saktë dhe profesional në punën që bëjnë”</h3>
<!-- <p>  </p> -->
<span> Florat Krasniqi-Labinot Merkoja, Pro 4 SH.P.K. </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2016/12/li-ori.png"/>
<h3>“Nëse na pyetni për kompaninë “FA Consulting” me besim të plotë mund të themi se kjo kompani është meritore për suksesin e klinikës sonë. Me përkushtimin e tyre, punën e pa lodhshme profesionale dhe me korrektësi të plotë ata bëjnë punën e tyre ashtu që ne profesionistëve mjekësorë na mbetet që të merremi vetëm me punën tonë profesionale”</h3>
<!-- <p>  </p> -->
<span> Afrim Kotori, Li-Ori </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2016/12/catering.png"/>
<h3>“Shërbimet, korrektësia, shpjegimi i gjërave dhe vizitat pa përtesë (në çdo kohë) bërë lokalit tonë, sa herë që kemi nevojë, janë vetëm disa nga arsyet që firma NTP Catering vazhdon t’i besojë FA Consulting-ut dhe të refuzojë ofertat tjera joshëse”</h3>
<!-- <p>  </p> -->
<span> Kushtrim Krasniqi, NTP Catering </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2016/12/tanore.png"/>
<h3>“Të shpejtë, të saktë dhe shumë profesional”</h3>
<!-- <p>  </p> -->
<span> Kushtrim Krasniqi, NTP Catering </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2016/12/nikapharm.png"/>
<h3>“Ofrojnë këshilla dhe sherbime në çdo kohë, janë profesional  dhe shumë korrekt”</h3>
<!-- <p>  </p> -->
<span> Ridvan Beqiri, Nika Pharm SH.P.K. </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2016/12/vera-remskar.png"/>
<h3>“Korrekt, profesional dhe mirëkuptues”</h3>
<!-- <p>  </p> -->
<span> Vera Remskar, Fondacioni Together Kosova </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2016/12/smart-pharm-kosova.png"/>
<h3>“Ajo çka nuk matet (llogaritet) saktë, nuk mund të taksohet saktë. Taksimi i pasaktë është më shumë se një problem administrativë. Prandaj ne këtë ia kemi besuar Fa Consulting”</h3>
<!-- <p>  </p> -->
<span> Blerim Veseli, Smart Pharm Kosova SH.P.K. </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2016/12/bohemian.png"/>
<h3>“Bashkepunimi i jonë me FA Consulting është i shkëlqyeshëm. Ne i konsiderojmë si pjesë përbërëse të ekipit tonë. Gatishmëria për të reaguar shpejtë dhe saktë i dallon nga ofruesit e tjerë të shërbimeve financiare”</h3>
<!-- <p>  </p> -->
<span> Arjan Shabani, Bohemian SH.P.K.  </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2016/12/k-aks.png"/>
<h3>“Këshilltari më i mirë i ecurisë së biznesit tonë”</h3>
<!-- <p>  </p> -->
<span> Jona Mino, K-AKS SH.P.K. </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2016/12/en-provence.png"/>
<h3>“FA Consulting është një kompani që bën shërbime financiare me saktësi dhe korrektësi dhe profesionalizëm. Kompania jonë ka më shumë se një vit e gjysëm që merr shërbime financiare dhe jemi shumë të kënaqur, shërbim perfekt dhe saktë gjatë gjithë kohës”</h3>
<!-- <p>  </p> -->
<span> Besnik Hoxhaj, En Provence </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2016/12/guliver.png"/>
<h3>“Kontributi i Fa Consulting ka qenë i jashtëzakonshëm në suksesin e kompanisë tonë. Fa Consulting kanë mbushur boshllëkun në kompaninë tonë dhe tani janë të pazëvendësueshëm.”</h3>
<!-- <p>  </p> -->
<span> Gazmend Bajrami, Guliver </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2017/02/prolab.png"/>
<h3>“Këshillues financiar shumë të mirë të biznesit tonë”</h3>
<!-- <p>  </p> -->
<span> Shkëlqim Kabashi, Prolab SH.P.K. </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2017/02/agsrent.png"/>
<h3>“Me Fa Consulting kemi bashkëpunim të shkëlqyer në fushën e financave. Janë korrekt e tepër profesional.”</h3>
<!-- <p>  </p> -->
<span> Albion Shala, AGS SH.P.K. </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2017/02/cargomaskosova.png"/>
<h3>“Bashkëpunimi me Fa Consulting ka qenë tejet i frytshëm. Na ofrojnë këshilla pa përtesë rreth financave e raportimit financiar.”</h3>
<!-- <p>  </p> -->
<span> Latif Gashi, Kargomax SH.P.K. </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2017/02/hejtaxi.png"/>
<h3>“Zanatit t’huj veç thuj puna e mbarë. Çka janë PROFI janë!”</h3>
<!-- <p>  </p> -->
<span> Fahrudin Deshaj, Hej Taxi SH.P.K. </span>
</li>
<li>
<img class="logo-kompani" src="https://faconsulting-ks.com/wp-content/uploads/2017/02/asha.png"/>
<h3>“Fillimi i bashkëpunimit me FA Consulting është i kënaqshëm, shpresojmë që edhe vazhdimi do të jetë i tillë.”</h3>
<!-- <p>  </p> -->
<span> Arbër Zogjani, ASHA SH.P.K.  </span>
</li>
<li>
<h3>“Kompania jonë sapo ka filluar bashkëpunimin me FA Consulting. Deri tani kemi hasur në shërbim korrekt.”</h3>
<!-- <p>  </p> -->
<span> Shpend Zogjani, Zog &amp; Zog SH.P.K. </span>
</li>
</ul>
</div>
</div>
</div>
</section>
<section id="about">
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="about-inner">
<h1>Për Ne</h1>
<p>FA Consulting është kompani e specializuar në ofrimin e shërbimeve për tatime, kontabilitet dhe konsultime financiare si dhe për konsultime tjera biznesore. FA Consulting ofron shërbime në <br/>
shumë fusha rreth financave me një eksperiencë shumë vjeçare, me klientë të shtrirë në territorin e Kosovës, dhe të specializuar në fusha të ndryshme vendore e ndërkombëtare. <br/>
<br/>
Fokusimi i kompanisë është dhe mbetet kuptimi i nevojave të klientëve duke analizuar detajet përkatëse, dhe paraqitja e klientit në mënyrën më profesionale të mundshme para autoriteteve të <br/>
taksimit dhe para klientëve vendor e të huaj.</p>
</div>
</div>
<div class="col-md-6">
<div class="about-img">
<img src="https://faconsulting-ks.com/wp-content/uploads/2015/10/about-images.jpg"/>
</div>
</div>
</div>
<section id="scroll-top">
<a class="cd-top" href="#0"><i class="fa fa-chevron-up"></i></a>
</section>
<br/>
<b>Warning</b>:  Use of undefined constant ASC - assumed 'ASC' (this will throw an Error in a future version of PHP) in <b>/home/faconsulting/public_html/wp-content/themes/faconsulting/footer.php</b> on line <b>8</b><br/>
<footer id="footer">
<div class="footer-one">
<h3>Na kontaktoni për t’u informuar në lidhje me ofertat tona</h3>
</div>
<div class="footer-two">
<div class="container">
<div class="row">
<div class="col-md-4">
<h4>Prishtinë, KS</h4>
<address>
							Rr.Muharrem Fejza, Kompleksi Royal<br/>
<abbr title="Phone">Mobile:</abbr> 044 261 147 <br/>
<span>E-mail: </span><a href="mailto:#">faconsulting10@gmail.com</a>
</address>
</div>
<div class="col-md-4">
<h4>Çfarë thonë klientët</h4>
<ul class="testimonials-js">
<li>
<p> Jo vetëm që përkujdeset për financat e kompanisë sonë, por sigurohet që ne të kemi projektime reale për një të ardhme stabile </p>
<span> Agon Avdimetaj, Zombie Soup LLC  </span>
</li>
<li>
<p> Shërbime të shpejta, profesionale dhe korrekte. Ndihma juaj në projeksionet financiare ka qenë e jashtëzakonshme </p>
<span> Arif Rukiqi, Kosova Projekt </span>
</li>
<li>
<p> Këshillues financiar shumë të mirë të biznesit tonë </p>
<span> Shkëlqim Kabashi, Prolab SH.P.K. </span>
</li>
<li>
<p> Profesional, shërbime sipas kodit të etikës profesionale </p>
<span> Fatijon Ibrahimi, Projoni </span>
</li>
<li>
<p> Këshillat dhe shërbimet e tyre na ndihmojnë shumë në menaxhimin e financave në përgjithësi dhe kontabilitetit financiar në veçanti </p>
<span> Krenare Shala, Allmakes Global Services NPSH </span>
</li>
<li>
<p> Shërbime të sakta, korrekte dhe shumë të shpejta që garantojnë suksesin e biznesit tuaj  </p>
<span> Arben Surdulli, Lighthouse SH.P.K. </span>
</li>
<li>
<p> Me kompaninë Fa Consulting kemi bashkëpunim të gjatë. Do të thoja se janë shumë korrekt, të saktë dhe profesional në punën që bëjnë </p>
<span> Florat Krasniqi-Labinot Merkoja, Pro 4 SH.P.K. </span>
</li>
<li>
<p> Nëse na pyetni për kompaninë “FA Consulting” me besim të plotë mund të themi se kjo kompani është meritore për suksesin e klinikës sonë.<br/>
Me përkushtimin e tyre, punën e pa lodhshme profesionale dhe me korrektësi të plotë ata bëjnë punën e tyre ashtu që ne profesionistëve mjekësorë na mbetet që të merremi vetëm me punën tonë profesionale<br/>
</p>
<span> Afrim Kotori, Li-Ori </span>
</li>
<li>
<p> Shërbimet, korrektësia, shpjegimi i gjërave dhe vizitat pa përtesë (në çdo kohë) bërë lokalit tonë, sa herë që kemi nevojë, janë vetëm disa nga arsyet që firma NTP Catering vazhdon t’i besojë FA Consulting-ut dhe të refuzojë ofertat tjera joshëse </p>
<span> Kushtrim Krasniqi, NTP Catering </span>
</li>
<li>
<p> Të shpejtë, të saktë dhe shumë profesional </p>
<span> Agim Morina, Tanore </span>
</li>
<li>
<p> Ofrojnë këshilla dhe sherbime në çdo kohë, janë profesional  dhe shumë korrekt </p>
<span> Ridvan Beqiri, Nika Pharm SH.P.K. </span>
</li>
<li>
<p> Vera Remskar, Fondacioni Together Kosova </p>
<span> Korrekt, profesional dhe mirëkuptues </span>
</li>
<li>
<p> Ajo çka nuk matet (llogaritet) saktë, nuk mund të taksohet saktë. Taksimi i pasaktë është më shumë se një problem administrativë. Prandaj ne këtë ia kemi besuar Fa Consulting </p>
<span> Blerim Veseli, Smart Pharm Kosova SH.P.K. </span>
</li>
<li>
<p> Bashkepunimi i jonë me FA Consulting është i shkëlqyeshëm. Ne i konsiderojmë si pjesë përbërëse të ekipit tonë. Gatishmëria për të reaguar shpejtë dhe saktë i dallon nga ofruesit e tjerë të shërbimeve financiare </p>
<span> Arjan Shabani, Bohemian SH.P.K.  </span>
</li>
<li>
<p> Këshilltari më i mirë i ecurisë së biznesit tonë </p>
<span> Jona Mino, K-AKS SH.P.K. </span>
</li>
<li>
<p> FA Consulting është një kompani që bën shërbime financiare me saktësi dhe korrektësi dhe profesionalizëm. Kompania jonë ka më shumë se një vit e gjysëm që merr shërbime financiare dhe jemi shumë të kënaqur, shërbim perfekt dhe saktë gjatë gjithë kohës </p>
<span> Besnik Hoxhaj, En Provence </span>
</li>
<li>
<p> Kontributi i Fa Consulting ka qenë i jashtëzakonshëm në suksesin e kompanisë tonë. Fa Consulting kanë mbushur boshllëkun në kompaninë tonë dhe tani janë të pazëvendësueshëm. </p>
<span> Gazmend Bajrami, Guliver </span>
</li>
<li>
<p> Këshillues financiar shumë të mirë të biznesit tonë </p>
<span> Shkëlqim Kabashi, Prolab SH.P.K. </span>
</li>
<li>
<p> Me Fa Consulting kemi bashkëpunim të shkëlqyer në fushën e financave. Janë korrekt e tepër profesional. </p>
<span> Albion Shala, AGS SH.P.K. </span>
</li>
<li>
<p> Bashkëpunimi me Fa Consulting ka qenë tejet i frytshëm. Na ofrojnë këshilla pa përtesë rreth financave e raportimit financiar. </p>
<span> Latif Gashi, Kargomax SH.P.K. </span>
</li>
<li>
<p> Zanatit t’huj veç thuj puna e mbarë. Çka janë PROFI janë!<br/>
</p>
<span> Fahrudin Deshaj, Hej Taxi SH.P.K. </span>
</li>
<li>
<p> Fillimi i bashkëpunimit me FA Consulting është i kënaqshëm, shpresojmë që edhe vazhdimi do të jetë i tillë. </p>
<span> Arbër Zogjani, ASHA SH.P.K.  </span>
</li>
<li>
<p> Kompania jonë sapo ka filluar bashkëpunimin me FA Consulting. Deri tani kemi hasur në shërbim korrekt. </p>
<span> Shpend Zogjani, Zog &amp; Zog SH.P.K. </span>
</li>
</ul>
</div>
<div class="col-md-4">
<h4>Newsletter</h4>
<p>Regjistrohu për tu informuar cdo javë me ofertat tona dhe të rejat nga kompania.</p>
<div class="form-group">
<div class="wpcf7" dir="ltr" id="wpcf7-f15-o1" lang="sq" role="form">
<div class="screen-reader-response"></div>
<form action="/#wpcf7-f15-o1" class="wpcf7-form" method="post" novalidate="novalidate">
<div style="display: none;">
<input name="_wpcf7" type="hidden" value="15"/>
<input name="_wpcf7_version" type="hidden" value="4.3"/>
<input name="_wpcf7_locale" type="hidden" value="sq"/>
<input name="_wpcf7_unit_tag" type="hidden" value="wpcf7-f15-o1"/>
<input name="_wpnonce" type="hidden" value="79eb83f62a"/>
</div>
<p><span class="wpcf7-form-control-wrap your-email"><input aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" name="your-email" placeholder="Email" size="40" type="email" value=""/></span> <input class="wpcf7-form-control wpcf7-submit btn btn-default" type="submit" value="Dergo"/></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div> </div>
</div>
</div>
</div>
</div>
<div class="footer-three">
<div class="container">
<div class="row">
<div class="copyright">
<p>© 2015 FA Consulting. All Rights Reserved.</p>
</div>
<div class="social">
<ul>
<li><a href="https://www.facebook.com/Faconsulting10" target="_blank"><i class="fa fa-facebook"></i></a></li>
</ul>
</div>
</div>
</div>
</div>
</footer>
<script src="https://faconsulting-ks.com/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"https:\/\/faconsulting-ks.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ..."};
/* ]]> */
</script>
<script src="https://faconsulting-ks.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.3" type="text/javascript"></script>
<script src="https://faconsulting-ks.com/wp-content/themes/faconsulting/js/jquery.js?ver=4.3.25" type="text/javascript"></script>
<script src="https://faconsulting-ks.com/wp-content/themes/faconsulting/js/smoothscroll.js?ver=4.3.25" type="text/javascript"></script>
<script src="https://faconsulting-ks.com/wp-content/themes/faconsulting/js/jquery.bxslider.min.js?ver=4.3.25" type="text/javascript"></script>
<script src="https://faconsulting-ks.com/wp-content/themes/faconsulting/js/bootstrap.min.js?ver=4.3.25" type="text/javascript"></script>
<script src="https://faconsulting-ks.com/wp-content/themes/faconsulting/js/main.js?ver=4.3.25" type="text/javascript"></script>
</div></section></body>
</html>