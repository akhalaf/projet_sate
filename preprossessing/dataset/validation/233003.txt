<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<meta content="" name="description"/>
<meta content="Bitxoxo" name="author"/>
<link href="images/favicon.ico" rel="icon"/>
<title>Bitxoxo - India's Leading Bitcoin OTC Exchange since 2016</title>
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<link href="css/style.css" rel="stylesheet"/>
<link href="css/bx-user.css" rel="stylesheet"/>
<script>
    setInterval(function() {
      $.get("https://api.bitxoxo.com/api/bitcoins/rates", function(data) {

        $("#buy-rate").html(data.buy);
        $("#sell-rate").html(data.sell);
      });
    }, 3000);
  </script>
<style>
    .error {
      color: #fff !important;
    }

    .c2a {
      font-family: 'Ubuntu', sans-serif;
      font-size: 2.5rem;
      color: #fff;
      line-height: 1.2;
    }
  </style>
</head>
<body>
<link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css" rel="stylesheet"/>
<section class="top-nav">
<div class="container">
<div class="row">
<div class="col-md-12">
<nav class="navbar navbar-expand-lg navbar-dark nav-front">
<a class="navbar-brand" href="https://www.bitxoxo.com">
<div class="logo-bg"></div>
</a>
<button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right" data-target="#navbarSupportedContent" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav ml-auto ">
<li class="nav-item">
<a class="nav-link" href="https://www.bitxoxo.com/index.php">Home <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://www.bitxoxo.com/about.php">About Us</a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://www.bitxoxo.com/partner.php">Partner</a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://www.bitxoxo.com/learn.php">Learn</a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://www.bitxoxo.com/investor.php">Investor</a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://client.bitxoxo.com/login">Login</a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://client.bitxoxo.com/signup">Sign Up</a>
</li>
</ul>
</div>
</nav>
</div>
</div>
</div>
</section>
<section class="bg-container">
<div class="container">
<div class="row">
<div class="col-6">
<div class="heading-8 text-white text-center">BUY : <span id="buy-rate"></span></div>
</div>
<div class="col-6">
<div class="heading-8 text-white text-center">SELL :<span id="sell-rate"></span></div>
</div>
</div>
<div class="row pt-3">
<div class="col-md-1"></div>
<div class="col-md-10">
<div class="c2a-holder ">
<div class="c2a animated zoomInUp text-center">INDIA'S LARGEST BITCOIN OTC EXCHANGE</div>
<hr style="background-color:#fff;"/>
<div class="row">
<div class="col-md-6 text-left border-r-white ">
<p class="text-white heading7 f-bold"><i class="fa fa-check-circle"></i> Minimum Deposit - ₹10 Lacs</p>
<p class="text-white heading7 f-bold"><i class="fa fa-check-circle"></i> No Maximum Limit</p>
<p class="text-white heading7 f-bold"><i class="fa fa-check-circle"></i> Competitive Rates</p>
<p class="text-white heading7 f-bold"><i class="fa fa-check-circle"></i> Fastest Account Activation</p>
<p class="text-white heading7 f-bold"><i class="fa fa-check-circle"></i> Custom Rates for 10₿+ Orders</p>
<p class="text-white heading7 f-bold"><i class="fa fa-check-circle"></i> Fastest Settlements (NEFT/RTGS)</p>
<p class="text-white heading7 f-bold"><i class="fa fa-check-circle"></i> Operating since 2016</p>
<p class="text-white heading7 f-bold"><i class="fa fa-check-circle"></i> 24/7 Support</p>
</div>
<div class="col-md-6 pl-5 pr-5" id="front-signup">
<form class="custom-form front-form" id="front-reg" name="front-reg">
<div class="form-group row">
<div class="col pt-3 pb-3">
<input class="form-control" id="username" name="username" placeholder="Mobile" type="text"/>
</div>
</div>
<div class="form-group row">
<div class="col pt-3 pb-3">
<input class="form-control" id="email" name="email" placeholder="Email" type="text"/>
</div>
</div>
<div class="form-group row ">
<div class="col pt-3 pb-3">
<input class="form-control" id="password" name="password" placeholder="Password" type="password"/>
</div>
<div class="col pt-3 pb-3">
<input class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Confirm Password" type="password"/>
</div>
</div>
<div class="form-group row ">
<div class="col">
<input class="form-control" id="referedId" name="referedId" placeholder="Referral Code" type="text"/>
</div>
</div>
<div class="form-group row ">
<div class="col">
<span class="error-message text-white"></span>
<span class="success-message text-success"></span>
</div>
</div>
<div class="form-group row ">
<div class="col">
<button class="btn btn-outline-w btn-lg btn-block" id="submitBtn" type="submit">Signup</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="header-main pb-2 pt-2">
<div class="container">
<br/><br/>
<div class="row">
<div class="col-md-2"></div>
<div class="col-md-8 border-b-black">
<div class="heading1 text-center">Download Apps</div>
</div>
</div>
<br/><br/>
<div class="row text-center ">
<div class="col-md-6">
<a href="#">
<img class="img-fluid" src="images/playstore.png"/>
</a>
</div>
<div class="col-md-6">
<a href="#">
<img class="img-fluid" src="images/apple-store.png"/>
</a>
</div>
</div>
<br/><br/>
</div>
</section>
<section class="step-featured">
<div class="container">
<div class="row">
<div class="col-md-2"></div>
<div class="col-md-8 border-b-white">
<div class="heading1 text-center">Rate Calculator</div>
</div>
</div>
<br/><br/>
<div class="row">
<div class="col-md-6">
<div class="fs-18p fw-bbld text-white text-center"><u>Buy Rate</u></div>
<form class="custom-form user-form">
<div class="row">
<div class="col-md-12 ">
<label class="fs-14p text-white">BTC</label>
<input class="form-control text-white" id="btcBuy" name="btcBuy" type="text"/>
</div>
</div>
<div class="row pt-3">
<div class="col-md-12 ">
<label class="fs-14p text-white">INR</label>
<input class="form-control text-white" id="inrBuy" name="inrBuy" type="text"/>
</div>
</div>
</form>
</div>
<div class="col-md-6">
<div class="fs-18p fw-bbld text-white text-center"><u>Sell Rate</u></div>
<form class="custom-form user-form">
<div class="row">
<div class="col-md-12">
<label class="fs-14p text-white">BTC</label>
<input class="form-control text-white" id="btcSell" name="btcSell" onchange="onChangeBtcSell()" type="text"/>
</div>
</div>
<div class="row pt-3 form-group">
<div class="col-md-12 ">
<label class="fs-14p text-white">INR</label>
<input class="form-control text-white" id="inrSell" name="inrSell" onchange="onChangeInrSell()" type="text"/>
</div>
</div>
</form>
</div>
</div>
</div>
</section>
<section class="header-main pb-2 pt-2">
<div class="container">
<div class="row text-center ">
<div class="col-6 col-md-6 border-r-light border-b-light p-3 ">
<h2><i class="fa fa-exchange text-base-pink"></i> </h2>
<div class="heading4 ">Best Rates</div>
<p>Buy, Sell and Gift bitcoins at best available rates in market.</p>
</div>
<div class="col-6 col-md-6 border-b-light p-3 ">
<h2><i class="fa fa-rocket text-base-pink"></i> </h2>
<div class="heading4 ">Fast Activation</div>
<p>Get your account verified with our smooth verification process.</p>
</div>
<div class="col-6 col-md-6 border-r-light p-3 ">
<h2><i class="fa fa-line-chart text-base-pink"></i> </h2>
<div class="heading4 ">Realtime Trade</div>
<p>All our Buying/Selling trades are processed instantly.</p>
</div>
<div class="col-6 col-md-6 p-3 ">
<h2><i class="fa fa-money text-base-pink"></i> </h2>
<div class="heading4 ">0 % Fees</div>
<p>Yes, you read it right. We don't charge any Fees for any of our services.</p>
</div>
</div>
</div>
</section>
<section class="step-featured">
<div class="container">
<div class="row">
<div class="col-md-2"></div>
<div class="col-md-8 border-b-white">
<div class="heading1 text-center">You are 3 Steps away from Bitcoin World</div>
</div>
</div>
<div class="row text-center p-2">
<div class="col-md-4 p-3">
<div class="heading4"><i class="fa fa-user-plus"></i>
<br/> Register </div>
<div class="step-featued-text p-2">Fill in some of your basic details in registration form, and you are ready to go.</div>
</div>
<div class="col-md-4 p-3">
<div class="heading4"><i class="fa fa-check-circle"></i>
<br/>Verify</div>
<div class="step-featued-text p-2">Complete your profile by going to "My Profile" section and upload the required documents. Your profile will be verified within few minutes.</div>
</div>
<div class="col-md-4 p-3">
<div class="heading4"><i class="fa fa-repeat"></i>
<br/>Start Trading</div>
<div class="step-featued-text p-2">Now you can Buy/Sell bitcoin using our super easy platform.</div>
</div>
</div>
</div>
</section>
<section class="app-bitxoxo">
<div class="container">
<div class="row pt-4">
<div class="col-md-6 text-center">
<img alt="Bitxoxo App" class="img-fluid" src="images/app_mockup.png"/>
</div>
<div class="col-md-6 text-justify pt-4">
<div class="heading4">Bitxoxo App</div>
<p class="text-grey l-height-30">Now buy and sell bitcoin become more easy and convenient with Bitxoxo Mobile App. Bitxoxo launched Android and iOS mobile app for its users to make your experience memorable with Bitxoxo.</p>
<p>Its Smart, Its Fast , Its Bitxoxo</p>
<div class="pt-5">
<a href="#">
<img class="img-fluid" src="images/playstore.png"/>
</a>
<a href="#">
<img class="img-fluid" src="images/apple-store.png"/>
</a>
</div>
</div>
</div>
</div>
</section>
<section class="pos-section">
<div class="container">
<div class="row">
<div class="col-md-4 text-center pt-6">
<div class="heading1 text-white"> Shop with Freedom </div>
<div class="heading3-3 text-white"> Payment made easy and safe with bitcoins. Now consumers can make payment in few clicks. In POS service, the consumers can send the amount just by scanning the QR code from its smartphone, whether it is android or IOS its works on both. </div>
</div>
<div class="col-md-4 text-center pt-6">
<img class="img-fluid" src="images/pos_app_phone.png"/>
</div>
<div class="col-md-4 text-center pb-4 pt-6">
<div class="heading1 text-white"> Merchants </div>
<div class="heading3-3 text-white"> Accepting Bitcoin payments is one touch away. Any merchant who accepts payment in bitcoin will be having the QR code. Scanning the same QR code consumers send the bitcoin to merchant address on QR. </div>
</div>
</div>
</div>
</section>
<section class="adding-soon-currency pt-5 pb-5">
<div class="container">
<div class="row">
<div class="col-md-12 ">
<div class="heading5 text-center">Adding Soon</div>
<div class="row text-center pt-4">
<div class="col-md-4"> <img alt="Bitcoin Cash" class="img-fluid text-center" src="images/btc-cash.png"/></div>
<div class="col-md-4"><img alt="Ethereum" class="img-fluid text-center" src="images/eth.png"/></div>
<div class="col-md-4"><img alt="Ripple" class="img-fluid text-center" src="images/ripple.png"/></div>
</div>
</div>
</div>
</div>
</section>
<section class="bitcoin-info">
<div class="container">
<div class="row">
<div class="col-md-4 border-r ">
<div class="heading1-base text-white">What is Bitcoin ?</div>
<p class="featured-text-del">Bitcoin (also written as BTC) is the world’s first decentralized digital currency and payment network, which offers quick, reasonably-priced and incredibly non-public bills for each person.This digital internet currency helps us to connect financially just like the internet has helped us attach socially. </p>
<a class="btn btn-sm btn-white-outline" href="what-is-bitcoin.php">Read More</a>
</div>
<div class="col-md-4 border-r">
<div class="heading1-base text-white">Is that Legal ?</div>
<p class="featured-text-del">The legal status of bitcoin varies extensively from country to country and continues to be undefined or converting in lots of them. While some countries have explicitly allowed its use and exchange, others have banned or restricted it. </p>
<a class="btn btn-sm btn-white-outline" href="what-is-bitcoin.php">Read More</a>
</div>
<div class="col-md-4">
<div class="heading1-base text-white">Why use bitcoins ?</div>
<p class="featured-text-del">Bitcoins are superior payment method for the virtual age. Firstly, bitcoin transactions cost nearly is free which makes it a miles less expensive alternative compared to debit or credit cards for instance. </p>
<a class="btn btn-sm btn-white-outline " href="what-is-bitcoin.php">Read More</a>
</div>
</div>
</div>
</section>
<section class="media-featured">
<div class="heading5 text-center">Mentioned On</div>
<div class="container">
<div class="row text-center pt-4">
<div class="col-md-2">
<a href="http://timesofindia.indiatimes.com/trend-tracking/startups-banking-on-bitcoins/articleshow/59083430.cms"><img alt="Times of India" class="img-fluid text-center" src="images/time-of-india.png"/></a>
</div>
<div class="col-md-2">
<a href="http://www.dqindia.com/bitxoxo-launches-pre-paid-bitcoin-gift-cards/"><img alt="Dataquest" class="img-fluid text-center" src="images/dq-logo.png"/></a>
</div>
<div class="col-md-2">
<a href="http://www.deccanchronicle.com/technology/in-other-news/090517/why-bitcoin-platforms-are-investors-delight-now.html"><img alt="Deccan Chronicle" class="img-fluid text-center" src="images/deccan.png"/></a>
</div>
<div class="col-md-2">
<a href="http://www.thehindubusinessline.com/money-and-banking/to-prevent-fraud-cyber-experts-want-bitcoins-to-be-regulated/article9711777.ece"><img alt="The Hindu Business Line" class="img-fluid text-center" src="images/hindu.png"/></a>
</div>
<div class="col-md-2">
<a href="https://www.forbes.com/sites/sindhujabalaji/2017/06/21/bitcoin-india-regulation/#25f8afe67e4a"><img alt="Forbes" class="img-fluid text-center" src="images/forbes.png"/></a>
</div>
<div class="col-md-2">
<a href="http://www.livemint.com/Money/KGbmio83nicnIPuyRhgvnJ/Bitcoin-cash-challenger-child-or-just-chatter.html"><img alt="Hindustan Times" class="img-fluid text-center" src="images/ht.png"/></a>
</div>
</div>
<div class="row text-center pt-4">
<div class="col-md-2">
<a href="https://profiles.yourstory.com/organization/bitxoxo.com/news"><img alt="Yourstory" class="img-fluid text-center" src="images/yourstory-logo.png"/></a>
</div>
<div class="col-md-2">
<a href="http://www.indiainfoline.com/article/news-business-wire-advertising/gifting-goes-hi-tech-with-bitxoxo-s-bitcoin-pre-paid-gift-card-117040600608_1.html"><img alt="India infoline" class="img-fluid text-center" src="images/IIFL-Logo.png"/></a>
</div>
<div class="col-md-2">
<a href="https://cointelegraph.com/news/who-should-decide-for-bitcoin-true-decentralization-need-not-be-absolute"><img alt="Coin Telegraph" class="img-fluid text-center" src="images/cointelegraph-logo.png"/></a>
</div>
<div class="col-md-2">
<a href="http://tech.economictimes.indiatimes.com/news/internet/pros-of-using-bitcoins/58895628"><img alt="Economic Times" class="img-fluid text-center" src="images/et.png"/></a>
</div>
<div class="col-md-2">
<a href="https://www.siliconindiamagazine.com/viewpoint/ceo-insights/why-is-cryptocurrency-not-a-threat-nwid-8740.html"><img alt="Silicon India" class="img-fluid text-center" src="images/si.png"/></a>
</div>
<div class="col-md-2">
<a href="http://www.moneycontrol.com/news/business/history-of-bitcoin-across-the-globe-and-how-have-countries-accepted-it-2271757.html"><img alt="Money Control" class="img-fluid text-center" src="images/moneycontrol.png"/></a>
</div>
</div>
<div class="row text-center pt-4">
<div class="col-md-2">
<a href="https://profiles.yourstory.com/organization/bitxoxo.com/news"><img alt="Vc Circle" class="img-fluid text-center" src="images/vc.png"/></a>
</div>
<div class="col-md-2">
<a href="http://www.livemint.com/Money/KGbmio83nicnIPuyRhgvnJ/Bitcoin-cash-challenger-child-or-just-chatter.html"><img alt="Live Mint" class="img-fluid text-center" src="images/lm.png"/></a>
</div>
<div class="col-md-2">
<a href="http://www.businesstoday.in/magazine/money-today/cover-story/investing-in-digital-currency-or-bitcoins-is-full-of-pitfalls-be-careful-while-playing-the-game/story/257828.html"><img alt="Business Today" class="img-fluid text-center" src="images/bt.png"/></a>
</div>
<div class="col-md-2">
<a href="http://m.dailyhunt.in/news/india/english/deccan+chronicle-epaper-deccanch/why+bitcoin+platforms+are+investor+s+delight+now-newsid-67431400"><img alt="Daily Hunt" class="img-fluid text-center" src="images/dh.png"/></a>
</div>
<div class="col-md-2">
<a href="http://www.pcquest.com/digital-mints-know-factory/"><img alt="PC Quest" class="img-fluid text-center" src="images/pc.png"/></a>
</div>
<div class="col-md-2">
<a href="https://inc42.com/buzz/bitcoin-cash-hard-fork-bitcoin-civil-war/"><img alt="INC 42" class="img-fluid text-center" src="images/inc.png"/></a>
</div>
</div>
</div>
</section>
<section class="banking-partner pt-5 pb-5">
<div class="container">
<div class="row">
<div class="col-md-12 ">
<div class="heading5 text-center">Our Banking Partners</div>
<div class="row text-center pt-4">
<div class="col-md-2"> <img alt="Bandhan Bank partner" class="img-fluid text-center" src="images/bandhan.png"/></div>
<div class="col-md-2"><img alt="Icici Bank partner" class="img-fluid text-center" src="images/icici.png"/></div>
</div>
</div>
</div>
</div>
</section>
<section class="footer">
<div class="container">
<div class="row text-left">
<div class="col-md-3 col-6 ">
<div class="heading3 text-base-pink fs-16p"><span class="border-b"> Overview </span></div>
<ul class="footer-menu">
<li class="footer-link"> <a href="https://www.bitxoxo.com/about.php">About Us</a></li>
<li class="footer-link"> <a href="https://www.bitxoxo.com/partner.php">Partner</a></li>
<li class="footer-link"> <a href="https://www.bitxoxo.com/learn.php">Learn</a></li>
</ul>
</div>
<div class="col-md-3 col-6">
<div class="heading3 text-base-pink fs-16p"><span class="border-b"> Policies </span></div>
<ul class="footer-menu">
<li class="footer-link"> <a href="https://www.bitxoxo.com/terms-of-use.php">Term of Use</a></li>
<li class="footer-link"> <a href="https://www.bitxoxo.com/aml-policy.php">AML Policy</a></li>
<li class="footer-link"> <a href="https://www.bitxoxo.com/kyc-policy.php">KYC Policy</a></li>
<li class="footer-link"> <a href="https://www.bitxoxo.com/privacy-policy.php">Privacy Policy</a></li>
<li class="footer-link"> <a href="https://www.bitxoxo.com/anti-spam-policy.php">Anti-Spam Policy</a></li>
</ul>
</div>
<div class="col-md-3 col-6">
<div class="heading3 text-base-pink fs-16p"><span class="border-b"> We are Social </span></div>
<ul class="footer-menu">
<li class="footer-link"> <a href="#" style="color: #fff; text-decoration: none;" target="_blank"> <img height="16px" src="https://www.bitxoxo.com/images/social/facebook.png" width="16px"/> Facebook </a></li>
<li class="footer-link"> <a href="#" style="color: #fff; text-decoration: none;" target="_blank"><img height="16px" src="https://www.bitxoxo.com/images/social/twitter.png" width="16px"/> Twitter </a></li>
<li class="footer-link"> <a href="#" style="color: #fff; text-decoration: none;" target="_blank"><img height="16px" src="https://www.bitxoxo.com/images/social/linkedin.png" width="16px"/> Linkedin </a></li>
<li class="footer-link"> <a href="#" style="color: #fff; text-decoration: none;"><img height="16px" src="https://www.bitxoxo.com/images/social/youtube.png" width="16px"/> Youtube </a></li>
<li class="footer-link"> <a href="#" style="color: #fff; text-decoration: none;" target="_blank"><img height="16px" src="https://www.bitxoxo.com/images/social/google.png" width="16px"/> Google+ </a></li>
</ul>
</div>
<div class="col-md-3 col-6">
<div class="heading3 text-base-pink fs-16p "><span class="border-b"> Address </span></div>
<div class="fs-14p fw-bold text-white pt-2">Bitxoxo Bitcoins Online Pvt. Ltd.</div>
<div class="text-white fs-12p">City Plaza, NGO's Colony Road,<br/>
Hanamkonda, Warangal<br/>
Telangana <br/><br/>
<img alt="Bitxoxo Logo" class="img-fluid" height="50%" src="images/logo_bw.png" width="50%"/>
</div>
</div>
</div>
</div>
</section>
<section class="copyright text-center">
<div class="container">
<div class="row">
<div class="col-md-3">
<div class="footer-heading">Made with <span class="fa fa-heart"></span> in India</div>
</div>
<div class="col-md-6">
<div class="footer-heading">Copyright 2016-20 Bitxoxo Bitcoins Online Pvt. Ltd. All Rights Reserved.</div>
</div>
<div class="col-md-3">
</div>
</div>
</div>
</section>
<div aria-hidden="true" aria-labelledby="exampleModalCenterTitle" class="modal fade bd-example-modal-sm" id="myModal" role="dialog" tabindex="-1">
<div class="modal-dialog modal-dialog-centered" role="document">
<div class="modal-content">
<div class="modal-header ">
<h5 class="modal-title heading10 col-12 text-center " id="exampleModalCenterTitle">Support</h5>
<button aria-label="Close" class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">×</span>
</button>
</div>
<div class="modal-body text-center">
<img alt="Support" class="img-fluid" src="images/support.jpg"/>
</div>
</div>
</div>
</div>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script src="js/front.js"></script>
<script src="js/form-partner.js"></script>
<script src="js/form-investor.js"></script>
<script src="js/form-learn.js"></script>
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5a82aea1d7591465c7079ec2/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1198410340181378');
fbq('track', "PageView");</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=1198410340181378&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-77494708-1');
      ga('send', 'pageview');
        </script>
<script type="text/javascript">
  /* <![CDATA[ */
  goog_snippet_vars = function() {
    var w = window;
    w.google_conversion_id = 879507452;
    w.google_conversion_label = "nhmzCOTgv3kQ_O-wowM";
    w.google_remarketing_only = false;
  }
  // DO NOT CHANGE THE CODE BELOW.
  goog_report_conversion = function(url) {
    goog_snippet_vars();
    window.google_conversion_format = "3";
    var opt = new Object();
    opt.onload_callback = function() {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  }
  var conv_handler = window['google_trackConversion'];
  if (typeof(conv_handler) == 'function') {
    conv_handler(opt);
  }
}
/* ]]> */
</script>
<script src="//www.googleadservices.com/pagead/conversion_async.js" type="text/javascript">
</script>
</body>
</html>