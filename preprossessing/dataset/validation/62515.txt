<html><body><p>ï»¿<!DOCTYPE html>

<!-- saved from url=(0054)http://rematricula.devrybrasil.com.br/rematricula.html -->
</p>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="no-cache" http-equiv="Cache-control"/>
<meta content="-1" http-equiv="Expires"/>
<title>RematrÃ­cula On-Line</title>
<script type="text/javascript">
        (function (i, s, o, g, r, a, m) { i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () { (i[r].q = i[r].q || []).push(arguments) }, i[r].l = 1 * new Date(); a = s.createElement(o), m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m) })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-44251655-2', 'devrybrasil.com.br');
        ga('send', 'pageview');
    </script>
<style type="text/css">
        * {
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            /* font-size: 12px; */
        }

        html,
        body {
            /* width: 100%;
            height: 100%;
            position: relative; */
            background-color: #EEEEEE;
        }

        .texto {
            font-weight: bold;
            color: #FFF;
            font-size: 1.4em;
            text-align: center;
            top: 0%;
            padding: 20px;
            margin-bottom: 10px;
            text-transform: uppercase;
        }

        .header {
            background-color: #005492;
            padding: 10px;
            margin-bottom: 20px;
            box-shadow: 2px 2px 2px #C0C0C0;
        }

            .header .header-logo {
                display: inline-block;
                margin-left: 20px;
                margin-top: -7px
            }

                .header .header-logo * {
                    font-family: "Bookman Old Style";
                    font-size: 16px;
                    color: #fff;
                    display: block;
                    letter-spacing: 4px;
                }

                .header .header-logo .re {
                    font-variant: small-caps;
                }

                .header .header-logo .letivo {
                    font-size: 12px;
                    text-align: end;
                    letter-spacing: 2px;
                }

                .header .header-logo span {
                    color: #faa400;
                }

        .panel {
            width: 100%;
            height: 100%;
            /* position: absolute; */
            /* top: 60px;
            bottom: 60px; */
            margin-left: auto;
            margin-right: auto;
            margin-bottom: 60px;
            display: block;
        }

        .panel-devry {
            border: 4px solid #005492;
            background: #005492;
            border-radius: 50px;
        }

        .panel .panel-heading {
            /* background: #005492;
            border-top-left-radius: 43px;
            border-top-right-radius: 43px; */
            max-height: 60px;
            width: 100%;
            height: 100%;
            /* float: left; */
        }

        .panel .panel-body {
            background: #EEEEEE;
            width: 100%;
            height: 100%;
            border-radius: 50px;
            display: flex;
            /* position: absolute;
            bottom: 0px;
            top: 60px; */
        }


        .container {
            width: 100%;
            height: 100%;
            margin-left: auto;
            margin-right: auto;
            margin-top: -15px;
            /* padding: 5px; */
            padding-bottom: 10px;
        }

        @media (max-width: 484px) {
            .panel {
                left: calc(50% - (280px / 2));
                max-width: 280px;
            }

            .container {
                max-width: 280px;
            }

            .panel .panel-heading {
                max-height: 120px;
            }
        }

        @media (min-width: 485px) and (max-width: 684px) {
            .panel {
                left: calc(50% - (485px / 2));
                max-width: 485px;
            }

            .container {
                max-width: 485px;
            }

            .panel .panel-heading {
                max-height: 80px;
            }
        }

        @media (min-width: 685px) and (max-width: 884px) {
            .panel {
                left: calc(50% - (685px / 2));
                max-width: 685px;
            }

            .container {
                max-width: 685px;
            }
        }

        @media (min-width: 885px) and (max-width: 1084px) {
            .panel {
                left: calc(50% - (885px / 2));
                max-width: 885px;
            }

            .container {
                max-width: 885px;
            }
        }

        @media (min-width: 1085px) and (max-width: 1284px) {
            .panel {
                left: calc(50% - (1085px / 2));
                max-width: 1085px;
            }

            .container {
                max-width: 1085px;
            }
        }

        @media (min-width: 1285px) {
            .panel {
                left: calc(50% - (1285px / 2));
                max-width: 1285px;
            }

            .container {
                max-width: 1285px;
            }
        }



        .lista-faculdade {
            list-style: none;
            padding: 0;
            margin: 30px;
            /* margin-top: 60px; */
        }

            .lista-faculdade .item {
                float: left;
                width: 200px;
                padding: 0;
                text-align: center !important;
                /* position: relative; */
                font-size: 15px !important;
                /* margin: 10px; */
                margin-bottom: 25px;
                height: 165px;
            }

                .lista-faculdade .item a {
                    color: #005492;
                    text-decoration: none;
                    /* background: #fff;
            border-radius: 50px; */
                }

            .lista-faculdade .icone-item {
                display: inline-block;
                box-shadow: 0 2px 4px rgba(0, 0, 0, 0.15);
                border-radius: 25%;
                margin-bottom: 10px;
                width: 134px;
                height: 134px;
                padding: 14px;
                background: #fff;
            }

        .img-responsive {
            display: block;
            width: 100%;
            margin: auto;
            position: relative;
            left: 50%;
            top: 50%;
            /* margin-right: -50%; */
            transform: translate(-50%, -50%);
            max-width: 110px;
        }

        breadcrumb {
            background-color: transparent;
            margin-bottom: 20px;
            float: left;
            list-style: none;
            width: 100%;
            padding: 0 10px;
            border-radius: 4px;
        }

        .breadcrumb li.active {
            font-weight: bold;
            color: #005492;
            background-color: #e7e7e7;
            font-size: 14px;
            padding-top: 4px;
        }

        .breadcrumb li:first-child {
            padding-left: 15px;
        }

        .breadcrumb > .active {
            color: #777;
        }

        .breadcrumb li {
            background-color: #dbdbdb;
            font-size: 12px;
            float: left;
            position: relative;
            padding: 5px 10px 4px 25px;
            border: 1px solid #fff;
            height: 28px;
        }

        .breadcrumb > li {
            display: inline-block;
        }

        .breadcrumb li:before {
            content: "" !important;
            display: block;
            width: 0;
            height: 0;
            border-top: 13px solid transparent;
            border-bottom: 13px solid transparent;
            border-left: 13px solid #fff;
            position: absolute;
            top: 50%;
            margin-top: -13px;
            margin-left: 1px;
            left: 100%;
            z-index: 1;
        }

        .breadcrumb li.active {
            font-weight: bold;
            color: #005492;
            background-color: #e7e7e7;
            font-size: 14px;
            padding-top: 4px;
        }

        .breadcrumb li:first-child {
            padding-left: 15px;
        }

        .breadcrumb > .active {
            color: #777;
        }

        .breadcrumb li {
            background-color: #dbdbdb;
            font-size: 12px;
            float: left;
            position: relative;
            padding: 5px 10px 4px 25px;
            border: 1px solid #fff;
            height: 28px;
        }

        .breadcrumb > li {
            display: inline-block;
        }

        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        user agent stylesheet li {
            display: list-item;
            text-align: -webkit-match-parent;
        }

        .breadcrumb {
            padding: 8px 15px;
            margin-bottom: 20px;
            list-style: none;
            /* background-color: #f5f5f5; */
            border-radius: 4px;
        }

        user agent stylesheet ol {
            display: block;
            list-style-type: decimal;
            -webkit-margin-before: 1em;
            -webkit-margin-after: 1em;
            -webkit-margin-start: 0px;
            -webkit-margin-end: 0px;
            -webkit-padding-start: 40px;
        }

        .breadcrumb li:before {
            content: "" !important;
            display: block;
            width: 0;
            height: 0;
            border-top: 13px solid transparent;
            border-bottom: 13px solid transparent;
            border-left: 13px solid #fff;
            position: absolute;
            top: 50%;
            margin-top: -13px;
            margin-left: 1px;
            left: 100%;
            z-index: 1;
        }

        *:before,
        *:after {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .breadcrumb li.active:after {
            border-left-color: #e7e7e7;
        }

        .breadcrumb li:after {
            content: "";
            display: block;
            width: 0;
            height: 0;
            border-top: 13px solid transparent;
            border-bottom: 13px solid transparent;
            border-left: 13px solid #dbdbdb;
            position: absolute;
            top: 50%;
            margin-top: -13px;
            left: 100%;
            z-index: 2;
        }

        #display-faculdade {
            width: 100%;
            height: 100%;
            margin-top: 30px;
            margin-left: auto;
            margin-right: auto;
            margin-bottom: 60px;
            padding: 10px;
            max-width: 590px;
        }

        .links-portais {
            display: inline-flex;
        }

            .links-portais img {
                width: 100%;
                max-width: 295px;
            }

        .link-academus {
            text-align: center;
            margin: 10px;
            font-size: 12px;
        }

        .link-devryone {
            text-align: center
        }

        #topo_rematricula {
            width: 100%;
        }

        .btn {
            text-decoration: none;
            display: inline-block;
            margin-bottom: 0;
            margin-top: 10px;
            font-weight: normal;
            text-align: center;
            vertical-align: middle;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            white-space: nowrap;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.428571429;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .btn-devryone {
            border: none;
            border-radius: 20px;
            height: 30px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.15);
            outline: none;
            background-color: #D2A61D;
            color: #fff;
        }

        .header-right {
            float: right;
            /* border: 1px solid #000; */
            /* margin: -10px;
            padding: 10px; */
        }

            .header-right a {
                text-decoration: none;
                color: #fff;
                padding: 10px;
                line-height: 2.8;
            }

            .header-right ul {
                list-style: none;
            }

        .header-logo h1 {
            float: left;
        }

            .header-logo h1.sign-icon {
                font-size: 35px;
                margin-top: -5px;
            }
    </style>
<div class="header">
<div class="header-logo">
<h1 class="sign-icon">⛊</h1>
<h1><span class="re">[RE]</span>MATRÃCULA <span class="letivo">2020.1</span></h1>
</div>
<!-- <div class="header-right"><ul class="header-menu">
            <li><a href="https://www.academusportal.com.br/Login.aspx" class="funcionario">Ir Para Academus</a></li>
        </ul> -->
</div>
<div class="container">
<ol class="breadcrumb">
<li class="active">
<a href="#" id="btn-inicio">InÃ­cio</a>
</li>
</ol>
</div>
<div class="body-contaner">
<div class="panel panel-devry">
<div class="panel-heading">
<div class="texto" id="label-title">Selecione a sua instituiÃ§Ã£o de ensino</div>
</div>
<div class="panel-body">
<div id="display-faculdade" style="display:none;">
<div class="img-banner">
<a id="link_video" target="_blank">
<img alt="RematrÃ­cula On-Line" id="topo_rematricula" src="./rematricula/img/topo-rematricula.jpg"/>
</a>
</div>
<div class="links-portais">
<div class="left">
<a href="https://rematricula.adtalembrasil.com.br/LoginRematricula.aspx" id="btn1">
<img alt="RematrÃ­cula" src="rematricula/img/bt-1.png"/>
</a>
</div>
<div class="right">
<a href="https://financeiro.adtalembrasil.com.br/Views/Logon/Login.aspx?ReturnUrl=%2f" id="btn2">
<img alt="Portal Financeiro" src="rematricula/img/bt-2.png"/>
</a>
</div>
</div>
<div class="link-academus">
<i>
                            *AtenÃ§Ã£o, seu bloqueador de Pop-ups precisa estar desabilitado
                            <br/>
                            para as funcionalidades do portal se comportarem adequadamente.
                        </i>
</div>
</div>
<ul class="lista-faculdade" id="list-sede"></ul>
</div>
<div class="link-academus">
<i style="color: #fff;">
                    **Se nÃ£o encontrou sua instituiÃ§Ã£o acesse direto os links abaixo.
                </i>
</div>
<div class="link-devryone" id="link-devryone">
<a class="btn btn-devryone" href="https://web.integrees.net">Ir para o Integrees</a>
<a class="btn btn-devryone" href="https://www.academusportal.com.br/Login.aspx">Ir para o Academus</a>
</div>
</div>
</div>
<script type="text/javascript">

        var LinkItem  ='<a href="#/{{name}}" class="sede-adtalem" data-sede="{{name}}" data-name="{{label}}" data-dtini="{{DtIni}}" data-dtfin="{{DtFin}}">';
			LinkItem +='    <i class="icone-item"><img class="img-responsive" src="{{url}}" alt="{{label}}"></i>';
			LinkItem +='	<br>';
			LinkItem +='	{{description}}';
			LinkItem +='</a>';

        function GetInnerHTML(obj){
            var result = LinkItem;
            for(var k in obj){
                result = result.replace(new RegExp('{{'+k+'}}', 'g'), obj[k]);
            }
            return result;
        }

        //Adicionar/remover aqui faculdades que estÃ£o com perÃ­odo de rematrÃ­cula
        //ATENÃÃO: A data Ã© composta por (yyyy-MM-dd) ANO-MÃS-DIA
        //ATENÃÃO: Ao incluir a DtFin, somar mais um dia

        var ObjSedeList = [
            //{
            //    name:"devryjp",
            //    label:"JoÃ£o Pessoa",
            //    description:"FBV | WYDEN",
            //    url:"rematricula/img/logos/devryjp.png",
            //    DtIni: "2019/01/16",
            //    DtFin: "2019/01/31"
            //},
            {
                name:"facid",
                label:"FACID",
                description:"FACID | WYDEN",
                url:"rematricula/img/logos/facid.png",
                DtIni: "2020/01/13",
                DtFin: "2020/02/06"
            },
            {
                name:"fanor",
                label:"FANOR",
                description:"UNIFANOR | WYDEN",
                url:"rematricula/img/logos/fanor.png",
                DtIni: "2020/01/13",
                DtFin: "2020/02/06"
            },
            {
                name:"fbv",
                label:"FBV",
                description:"UNIFBV | WYDEN",
                url:"rematricula/img/logos/fbv.png",
                DtIni: "2020/01/15",
                DtFin: "2020/02/08"
            },
            {
                name:"frb",
                label:"Ruy Barbosa",
                description:"Ruy Barbosa | WYDEN",
                url:"rematricula/img/logos/frb.png",
                DtIni: "2020/01/06",
                DtFin: "2020/02/01"
            },
            //{
            //    name:"devrysl",
            //    label:"SÃ£o LuÃ­s",
            //    description:"SÃ£o LuÃ­s | WYDEN",
            //    url:"rematricula/img/logos/devrysl.png",
            //    DtIni: "2019/01/09",
            //    DtFin: "2019/01/24"
            //},
            {
                name:"fmf",
                label:"FMF",
                description:"FMF | WYDEN",
                url:"rematricula/img/logos/fmf.png",
                DtIni: "2020/01/08",
                DtFin: "2020/02/01"
            },
            {
                name:"faci",
                label:"FACI",
                description:"FACI | WYDEN",
                url:"rematricula/img/logos/faci.png",
                DtIni: "2020/01/08",
                DtFin: "2020/02/01"
            },
            {
                name:"area1",
                label:"ÃREA1",
                description:"ÃREA1 | WYDEN",
                url:"rematricula/img/logos/area1.png",
                DtIni: "2020/01/06",
                DtFin: "2020/02/01"
            },
            {
                name:"unifavip",
                label:"UNIFAVIP",
                description:"UNIFAVIP | WYDEN",
                url:"rematricula/img/logos/favip.png",
                DtIni: "2020/01/15",
                DtFin: "2020/02/08"
            },
            {
                name:"damasio",
                label:"DamÃ¡sio",
                description:"DAMÃSIO",
                url:"rematricula/img/logos/damasio.png",
                DtIni: "2020/01/06",
                DtFin: "2020/02/01"
            },
            {
                name:"facimp",
                label:"Facimp",
                description:"FACIMP | WYDEN",
                url:"rematricula/img/logos/facimp.png",
                DtIni: "2020/01/08",
                DtFin: "2020/02/01"
            },
            {
                name:"metrocamp",
                label:"Metrocamp",
                description:"UNIMETROCAMP | WYDEN",
                url:"rematricula/img/logos/metrocamp.png",
                DtIni: "2020/01/08",
                DtFin: "2020/02/01"
            },
            {
                name:"ibmecbh",
                label:"Ibmec BH",
                description:"IBMEC BH",
                url:"rematricula/img/logos/ibmec_bh.png",
                DtIni: "2020/01/06",
                DtFin: "2020/02/01"
            },
            {
                name:"ibmecrj",
                label:"Ibmec RJ",
                description:"IBMEC RJ",
                url:"rematricula/img/logos/ibmec_rj.png",
                DtIni: "2020/01/13",
                DtFin: "2020/02/06"
            },
            {
                name:"ibmecsp",
                label:"Ibmec SP",
                description:"IBMEC SP",
                url:"rematricula/img/logos/ibmec_sp.png",
                DtIni: "2020/01/06",
                DtFin: "2020/02/01"
            },
            //{
            //    name:"dmunifavip",
            //    label:"DamÃ¡sio UNIFAVIP",
            //    description:"DAMÃSIO UNIFAVIP",
            //    url:"rematricula/img/logos/damasio_unifavip.png",
            //    DtIni: "2018/07/25",
            //    DtFin: "2018/08/04"
            //},
            {
                name: "eadwyden",
                label: "EAD Wyden",
                description: "EAD WYDEN",
                url: "rematricula/img/logos/ead_wyden.png",
                DtIni: "2019/12/18",
                DtFin: "2019/12/24"
            }
        ];

        var listaSede = document.getElementById('list-sede');

        for (var index = 0; index < ObjSedeList.length; index++) {
            var element = ObjSedeList[index];
            var li = document.createElement('li');
            li.className = 'item';
            li.innerHTML = GetInnerHTML(element);
            listaSede.appendChild(li);
        }

        (function (w, d) {

            var breadcrumb = d.getElementsByClassName('breadcrumb')[0];
            var $e = function (id) { return d.getElementById(id) };
            var $c = function (id) { return d.getElementsByClassName(id) };
            // var l = "http://www.academusportal.com.br/Login.aspx";
            var l = "https://web.integrees.net";
            var lD = "http://www.academusportal.com.br/Login.aspx";
            var b = d.getElementsByClassName('sede-adtalem');
            for (i = 0; i < b.length; i++) {
                b[i].addEventListener("click", function () {
                    var instituicao = this.dataset.sede;
                    var instituicaoName = this.dataset.name;
                    var DtIni = new Date(this.dataset.dtini);
                    var DtFin = new Date(this.dataset.dtfin);

                    $e('label-title').innerText = 'Selecione um dos links abaixo';

                    breadcrumb.firstElementChild.className = '';
                    var li = d.createElement('li');
                    li.innerHTML = '<a>' + instituicaoName + '</a>';
                    li.className = 'active';
                    breadcrumb.appendChild(li);

                    // if (instituicao == "devryjp" || instituicao == "devrysl" || instituicao == "frb" || instituicao == "area1" || instituicao == "unifavip" || instituicao == "faci") {
                    //     $e('link_video').href = "https://goo.gl/DEkH7m";
                    //     $e('topo_rematricula').src = "./rematricula/img/topo-rematricula-play.png";
                    // }
                    // else if (instituicao == "fanor" || instituicao == "fbv") {
                    //     $e('link_video').href = "https://goo.gl/t1aoU4";
                    //     $e('topo_rematricula').src = "./rematricula/img/topo-rematricula-play.png";
                    // }
                    // else if (instituicao == "fmf" || instituicao == "metrocamp" || instituicao == "facid" || instituicao == "damasio") {
                    //     $e('link_video').href = "https://goo.gl/smeD7a";
                    //     $e('topo_rematricula').src = "./rematricula/img/topo-rematricula-play.png";
                    // }
                    // else if (instituicao == "facimp") {
                    //     $e('link_video').href = "https://goo.gl/WgypeJ";
                    //     $e('topo_rematricula').src = "./rematricula/img/topo-rematricula-play.png";
                    // }

                    switch (instituicao) {
                        case "fmf":
							$e('link_video').href = "https://www.youtube.com/watch?v=1xnq9_G-Xuc";
                            $e('topo_rematricula').src = "./rematricula/img/topo-rematricula-video.jpg";
                            break;
                        case "facimp":
							$e('link_video').href = "https://www.youtube.com/watch?v=1xnq9_G-Xuc";
                            $e('topo_rematricula').src = "./rematricula/img/topo-rematricula-video.jpg";
                            break;
                        case "facid":
                            $e('link_video').href = "https://www.youtube.com/watch?v=1xnq9_G-Xuc";
                            $e('topo_rematricula').src = "./rematricula/img/topo-rematricula-video.jpg";
                            break;
                        case "fanor":
							$e('link_video').href = "https://www.youtube.com/watch?v=1xnq9_G-Xuc";
                            $e('topo_rematricula').src = "./rematricula/img/topo-rematricula-video.jpg";
                            break;
                        //case "devrysl":
                        case "area1":
							$e('link_video').href = "https://www.youtube.com/watch?v=1xnq9_G-Xuc";
                            $e('topo_rematricula').src = "./rematricula/img/topo-rematricula-video.jpg";
                            break;
                        case "frb":
							$e('link_video').href = "https://www.youtube.com/watch?v=1xnq9_G-Xuc";
                            $e('topo_rematricula').src = "./rematricula/img/topo-rematricula-video.jpg";
                            break;
                        case "fbv":
							$e('link_video').href = "https://www.youtube.com/watch?v=1xnq9_G-Xuc";
                            $e('topo_rematricula').src = "./rematricula/img/topo-rematricula-video.jpg";
                            break;
                        case "unifavip":
							$e('link_video').href = "https://www.youtube.com/watch?v=1xnq9_G-Xuc";
                            $e('topo_rematricula').src = "./rematricula/img/topo-rematricula-video.jpg";
                            break;
                        case "faci":
							$e('link_video').href = "https://www.youtube.com/watch?v=1xnq9_G-Xuc";
                            $e('topo_rematricula').src = "./rematricula/img/topo-rematricula-video.jpg";
                            break;
                        case "metrocamp":
							$e('link_video').href = "https://www.youtube.com/watch?v=1xnq9_G-Xuc";
                            $e('topo_rematricula').src = "./rematricula/img/topo-rematricula-video.jpg";
                            break;
                        //case "ibmecrj":
                            $e('link_video').href = "https://www.youtube.com/watch?v=1xnq9_G-Xuc";
                            $e('topo_rematricula').src = "./rematricula/img/topo-rematricula-video.jpg";
                            break;
                        //case "damasio":
                        
                        default:
                            $e('link_video').removeAttribute("href");
                            $e('topo_rematricula').src = "./rematricula/img/topo-rematricula.jpg";
                            break;
                    }
                    var Agora = new Date();
                    if (Agora >= DtIni && Agora <= DtFin)
                    {
                        //if (instituicao == "facimp") {
                        //    $e('link-academus').style.display = 'none';
                        //}
                        $e('list-sede').style.display = 'none';
                        $e('display-faculdade').style.display = 'block';
                    } else
                    {
                        //if (instituicao == "ibmecsp")
                        //{
                        //    window.location.href = lD;
                        //}
                        //else
                        //{
                            window.location.href = l;
                        //}
                    }
                    return false;
                });
            }
            if (w.location.hash == "#rematricula") {
                $e('list-sede').style.display = 'none';
                $e('display-faculdade').style.display = 'block';
            }
            $e('btn-inicio').addEventListener("click", function () {
                $e('label-title').innerText = 'Selecione a sua instituiÃ§Ã£o de ensino';
                breadcrumb.firstElementChild.className = 'active';
                if (breadcrumb.childElementCount > 1) {
                    breadcrumb.removeChild(breadcrumb.lastElementChild);
                }
                $e('list-sede').style.display = 'block';
                $e('display-faculdade').style.display = 'none';
            });
        })(window, document);
    </script>
</body></html>