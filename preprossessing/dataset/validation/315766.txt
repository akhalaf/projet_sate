<!DOCTYPE html>
<html class="no-js" lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.chilis.in/wp-content/themes/chilis/assets/css/owl.carousel.min.css" rel="stylesheet"/>
<link href="https://www.chilis.in/wp-content/themes/chilis/assets/css/owl.theme.default.min.css" rel="stylesheet"/>
<link href="https://www.chilis.in/wp-content/themes/chilis/assets/css/jquery.fancybox.min.css" rel="stylesheet"/>
<link href="https://www.chilis.in/wp-content/themes/chilis/assets/css/selectize.default.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Alfa+Slab+One|Oswald:500|Barlow+Condensed|Montserrat:600,900" rel="stylesheet"/>
<title>Page not found – Chili's</title>
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.chilis.in/feed/" rel="alternate" title="Chili's » Feed" type="application/rss+xml"/>
<link href="https://www.chilis.in/comments/feed/" rel="alternate" title="Chili's » Comments Feed" type="application/rss+xml"/>
<script>
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.chilis.in\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style>
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.chilis.in/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet"/>
<link href="https://www.chilis.in/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.2" id="contact-form-7-css" media="all" rel="stylesheet"/>
<link href="https://www.chilis.in/wp-content/plugins/sitemap/css/page-list.css?ver=4.3" id="page-list-style-css" media="all" rel="stylesheet"/>
<link href="https://www.chilis.in/wp-content/plugins/wp-store-locator/css/styles.min.css?ver=2.2.232" id="wpsl-styles-css" media="all" rel="stylesheet"/>
<link href="https://www.chilis.in/wp-content/themes/twentytwenty/style.css?ver=5.4.4" id="parent-style-css" media="all" rel="stylesheet"/>
<link href="https://www.chilis.in/wp-content/themes/chilis/style.css?ver=1.0.0" id="twentytwenty-style-css" media="all" rel="stylesheet"/>
<style id="twentytwenty-style-inline-css">
.color-accent,.color-accent-hover:hover,.color-accent-hover:focus,:root .has-accent-color,.has-drop-cap:not(:focus):first-letter,.wp-block-button.is-style-outline,a { color: #e22658; }blockquote,.border-color-accent,.border-color-accent-hover:hover,.border-color-accent-hover:focus { border-color: #e22658; }button:not(.toggle),.button,.faux-button,.wp-block-button__link,.wp-block-file .wp-block-file__button,input[type="button"],input[type="reset"],input[type="submit"],.bg-accent,.bg-accent-hover:hover,.bg-accent-hover:focus,:root .has-accent-background-color,.comment-reply-link { background-color: #e22658; }.fill-children-accent,.fill-children-accent * { fill: #e22658; }:root .has-background-color,button,.button,.faux-button,.wp-block-button__link,.wp-block-file__button,input[type="button"],input[type="reset"],input[type="submit"],.wp-block-button,.comment-reply-link,.has-background.has-primary-background-color:not(.has-text-color),.has-background.has-primary-background-color *:not(.has-text-color),.has-background.has-accent-background-color:not(.has-text-color),.has-background.has-accent-background-color *:not(.has-text-color) { color: #ffffff; }:root .has-background-background-color { background-color: #ffffff; }body,.entry-title a,:root .has-primary-color { color: #000000; }:root .has-primary-background-color { background-color: #000000; }cite,figcaption,.wp-caption-text,.post-meta,.entry-content .wp-block-archives li,.entry-content .wp-block-categories li,.entry-content .wp-block-latest-posts li,.wp-block-latest-comments__comment-date,.wp-block-latest-posts__post-date,.wp-block-embed figcaption,.wp-block-image figcaption,.wp-block-pullquote cite,.comment-metadata,.comment-respond .comment-notes,.comment-respond .logged-in-as,.pagination .dots,.entry-content hr:not(.has-background),hr.styled-separator,:root .has-secondary-color { color: #6d6d6d; }:root .has-secondary-background-color { background-color: #6d6d6d; }pre,fieldset,input,textarea,table,table *,hr { border-color: #dbdbdb; }caption,code,code,kbd,samp,.wp-block-table.is-style-stripes tbody tr:nth-child(odd),:root .has-subtle-background-background-color { background-color: #dbdbdb; }.wp-block-table.is-style-stripes { border-bottom-color: #dbdbdb; }.wp-block-latest-posts.is-grid li { border-top-color: #dbdbdb; }:root .has-subtle-background-color { color: #dbdbdb; }body:not(.overlay-header) .primary-menu > li > a,body:not(.overlay-header) .primary-menu > li > .icon,.modal-menu a,.footer-menu a, .footer-widgets a,#site-footer .wp-block-button.is-style-outline,.wp-block-pullquote:before,.singular:not(.overlay-header) .entry-header a,.archive-header a,.header-footer-group .color-accent,.header-footer-group .color-accent-hover:hover { color: #e22658; }.social-icons a,#site-footer button:not(.toggle),#site-footer .button,#site-footer .faux-button,#site-footer .wp-block-button__link,#site-footer .wp-block-file__button,#site-footer input[type="button"],#site-footer input[type="reset"],#site-footer input[type="submit"] { background-color: #e22658; }.social-icons a,body:not(.overlay-header) .primary-menu ul,.header-footer-group button,.header-footer-group .button,.header-footer-group .faux-button,.header-footer-group .wp-block-button:not(.is-style-outline) .wp-block-button__link,.header-footer-group .wp-block-file__button,.header-footer-group input[type="button"],.header-footer-group input[type="reset"],.header-footer-group input[type="submit"] { color: #ffffff; }#site-header,.footer-nav-widgets-wrapper,#site-footer,.menu-modal,.menu-modal-inner,.search-modal-inner,.archive-header,.singular .entry-header,.singular .featured-media:before,.wp-block-pullquote:before { background-color: #ffffff; }.header-footer-group,body:not(.overlay-header) #site-header .toggle,.menu-modal .toggle { color: #000000; }body:not(.overlay-header) .primary-menu ul { background-color: #000000; }body:not(.overlay-header) .primary-menu > li > ul:after { border-bottom-color: #000000; }body:not(.overlay-header) .primary-menu ul ul:after { border-left-color: #000000; }.site-description,body:not(.overlay-header) .toggle-inner .toggle-text,.widget .post-date,.widget .rss-date,.widget_archive li,.widget_categories li,.widget cite,.widget_pages li,.widget_meta li,.widget_nav_menu li,.powered-by-wordpress,.to-the-top,.singular .entry-header .post-meta,.singular:not(.overlay-header) .entry-header .post-meta a { color: #6d6d6d; }.header-footer-group pre,.header-footer-group fieldset,.header-footer-group input,.header-footer-group textarea,.header-footer-group table,.header-footer-group table *,.footer-nav-widgets-wrapper,#site-footer,.menu-modal nav *,.footer-widgets-outer-wrapper,.footer-top { border-color: #dbdbdb; }.header-footer-group table caption,body:not(.overlay-header) .header-inner .toggle-wrapper::before { background-color: #dbdbdb; }
</style>
<link href="https://www.chilis.in/wp-content/themes/twentytwenty/print.css?ver=1.0.0" id="twentytwenty-print-style-css" media="print" rel="stylesheet"/>
<link href="https://www.chilis.in/wp-content/plugins/kingcomposer/assets/frontend/css/kingcomposer.min.css?ver=2.9.5" id="kc-general-css" media="all" rel="stylesheet"/>
<link href="https://www.chilis.in/wp-content/plugins/kingcomposer/assets/css/animate.css?ver=2.9.5" id="kc-animate-css" media="all" rel="stylesheet"/>
<link href="https://www.chilis.in/wp-content/plugins/kingcomposer/assets/css/icons.css?ver=2.9.5" id="kc-icon-1-css" media="all" rel="stylesheet"/>
<script src="https://www.chilis.in/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"></script>
<script src="https://www.chilis.in/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1"></script>
<script async="" src="https://www.chilis.in/wp-content/themes/twentytwenty/assets/js/index.js?ver=1.0.0"></script>
<link href="https://www.chilis.in/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.chilis.in/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.chilis.in/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.4" name="generator"/>
<script type="text/javascript">var kc_script_data={ajax_url:"https://www.chilis.in/wp-admin/admin-ajax.php"}</script> <script>document.documentElement.className = document.documentElement.className.replace( 'no-js', 'js' );</script>
<style>.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><style id="custom-background-css">
body.custom-background { background-color: #ffffff; }
</style>
<link href="https://www.chilis.in/wp-content/uploads/2020/04/logo.png" rel="icon" sizes="32x32"/>
<link href="https://www.chilis.in/wp-content/uploads/2020/04/logo.png" rel="icon" sizes="192x192"/>
<link href="https://www.chilis.in/wp-content/uploads/2020/04/logo.png" rel="apple-touch-icon"/>
<meta content="https://www.chilis.in/wp-content/uploads/2020/04/logo.png" name="msapplication-TileImage"/>
</head>
<body class="error404 custom-background wp-custom-logo wp-embed-responsive kc-css-system has-no-pagination showing-comments show-avatars footer-top-visible reduced-spacing">
<a class="skip-link screen-reader-text" href="#site-content">Skip to the content</a>
<header class="header-footer-group" id="site-header" role="banner">
<div class="header-inner">
<div class="header-titles-wrapper">
<div class="header-titles">
<div class="site-logo faux-heading"><a class="custom-logo-link" href="https://www.chilis.in/" rel="home"><img alt="Chilis" class="custom-logo" height="92" src="https://www.chilis.in/wp-content/uploads/2020/04/logo.png" width="124"/></a><span class="screen-reader-text">Chili's</span></div>
</div><!-- .header-titles -->
<button aria-expanded="false" class="toggle nav-toggle mobile-nav-toggle" data-set-focus=".close-nav-toggle" data-toggle-body-class="showing-menu-modal" data-toggle-target=".menu-modal">
<span class="toggle-inner">
<span class="toggle-icon">
<svg aria-hidden="true" class="svg-icon" focusable="false" height="7" role="img" viewbox="0 0 26 7" width="26" xmlns="http://www.w3.org/2000/svg"><path d="M332.5,45 C330.567003,45 329,43.4329966 329,41.5 C329,39.5670034 330.567003,38 332.5,38 C334.432997,38 336,39.5670034 336,41.5 C336,43.4329966 334.432997,45 332.5,45 Z M342,45 C340.067003,45 338.5,43.4329966 338.5,41.5 C338.5,39.5670034 340.067003,38 342,38 C343.932997,38 345.5,39.5670034 345.5,41.5 C345.5,43.4329966 343.932997,45 342,45 Z M351.5,45 C349.567003,45 348,43.4329966 348,41.5 C348,39.5670034 349.567003,38 351.5,38 C353.432997,38 355,39.5670034 355,41.5 C355,43.4329966 353.432997,45 351.5,45 Z" fill-rule="evenodd" transform="translate(-329 -38)"></path></svg> </span>
<span class="toggle-text">Menu</span>
</span>
</button><!-- .nav-toggle -->
</div><!-- .header-titles-wrapper -->
<div class="header-navigation-wrapper">
<nav aria-label="Horizontal" class="primary-menu-wrapper" role="navigation">
<ul class="primary-menu reset-list-style">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28" id="menu-item-28"><a href="https://www.chilis.in/menu/">Chili’s India Menu</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1058" id="menu-item-1058"><a href="https://www.chilis.in/offers/">Offers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1004" id="menu-item-1004"><a href="https://www.chilis.in/store-locator/">Store Locator</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1003" id="menu-item-1003"><a href="https://www.chilis.in/safety-measures/">Safety Measures</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1005" id="menu-item-1005"><a href="https://www.chilis.in/about-us/">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1002" id="menu-item-1002"><a href="https://www.chilis.in/contact-us/">Contact Us</a></li>
<li class="new-badge menu-item menu-item-type-custom menu-item-object-custom menu-item-1076" id="menu-item-1076"><a href="https://chilis.dotpe.in/order" rel="noopener noreferrer" target="_blank" title="New">Order Now</a></li>
</ul>
</nav><!-- .primary-menu-wrapper -->
</div><!-- .header-navigation-wrapper -->
<!--- STORE LOCATOR -->
<div class="menu-right">
<a class="store-icon" href="store-locator"><img alt="store" src="https://www.chilis.in/wp-content/themes/chilis/assets/images/store-icon.png"/></a>
</div>
</div><!-- .header-inner -->
</header><!-- #site-header -->
<div class="menu-modal cover-modal header-footer-group" data-modal-target-string=".menu-modal">
<div class="menu-modal-inner modal-inner">
<div class="menu-wrapper section-inner">
<div class="menu-top">
<button aria-expanded="false" class="toggle close-nav-toggle fill-children-current-color" data-set-focus=".menu-modal" data-toggle-body-class="showing-menu-modal" data-toggle-target=".menu-modal">
<span class="toggle-text">Close Menu</span>
<svg aria-hidden="true" class="svg-icon" focusable="false" height="16" role="img" viewbox="0 0 16 16" width="16" xmlns="http://www.w3.org/2000/svg"><polygon fill="" fill-rule="evenodd" points="6.852 7.649 .399 1.195 1.445 .149 7.899 6.602 14.352 .149 15.399 1.195 8.945 7.649 15.399 14.102 14.352 15.149 7.899 8.695 1.445 15.149 .399 14.102"></polygon></svg> </button><!-- .nav-toggle -->
<nav aria-label="Mobile" class="mobile-menu" role="navigation">
<ul class="modal-menu reset-list-style">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><div class="ancestor-wrapper"><a href="https://www.chilis.in/menu/">Chili’s India Menu</a></div><!-- .ancestor-wrapper --></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1058"><div class="ancestor-wrapper"><a href="https://www.chilis.in/offers/">Offers</a></div><!-- .ancestor-wrapper --></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1004"><div class="ancestor-wrapper"><a href="https://www.chilis.in/store-locator/">Store Locator</a></div><!-- .ancestor-wrapper --></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1003"><div class="ancestor-wrapper"><a href="https://www.chilis.in/safety-measures/">Safety Measures</a></div><!-- .ancestor-wrapper --></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1005"><div class="ancestor-wrapper"><a href="https://www.chilis.in/about-us/">About Us</a></div><!-- .ancestor-wrapper --></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1002"><div class="ancestor-wrapper"><a href="https://www.chilis.in/contact-us/">Contact Us</a></div><!-- .ancestor-wrapper --></li>
<li class="new-badge menu-item menu-item-type-custom menu-item-object-custom menu-item-1076"><div class="ancestor-wrapper"><a href="https://chilis.dotpe.in/order" rel="noopener noreferrer" target="_blank" title="New">Order Now</a></div><!-- .ancestor-wrapper --></li>
</ul>
</nav>
</div><!-- .menu-top -->
<div class="menu-bottom">
</div><!-- .menu-bottom -->
</div><!-- .menu-wrapper -->
</div><!-- .menu-modal-inner -->
</div><!-- .menu-modal -->
<main id="site-content" role="main">
<div class="section-inner thin error404-content">
<h1 class="entry-title">Page Not Found</h1>
<div class="intro-text"><p>The page you were looking for could not be found. It might have been removed, renamed, or did not exist in the first place.</p></div>
<form action="https://www.chilis.in/" aria-label="404 not found" class="search-form" method="get" role="search">
<label for="search-form-1">
<span class="screen-reader-text">Search for:</span>
<input class="search-field" id="search-form-1" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form>
</div><!-- .section-inner -->
</main><!-- #site-content -->
<footer class="header-footer-group" id="site-footer" role="contentinfo">
<div class="section-inner">
<div class="widget__copyright widget widget_nav_menu" id="nav_menu-2"><div class="menu-main-navigation-container"><ul class="menu" id="menu-main-navigation-2"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="https://www.chilis.in/menu/">Chili’s India Menu</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1058"><a href="https://www.chilis.in/offers/">Offers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1004"><a href="https://www.chilis.in/store-locator/">Store Locator</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1003"><a href="https://www.chilis.in/safety-measures/">Safety Measures</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1005"><a href="https://www.chilis.in/about-us/">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1002"><a href="https://www.chilis.in/contact-us/">Contact Us</a></li>
<li class="new-badge menu-item menu-item-type-custom menu-item-object-custom menu-item-1076"><a href="https://chilis.dotpe.in/order" rel="noopener noreferrer" target="_blank" title="New">Order Now</a></li>
</ul></div></div><div class="widget__copyright widget widget_text" id="text-2"> <div class="textwidget"><div class="social-links">
<a href="https://www.facebook.com/chilis.in/" rel="noopener noreferrer" target="_blank"><i class="fab fa-facebook-square"></i></a><br/>
<a href="https://www.instagram.com/chilis.in/?hl=en" rel="noopener noreferrer" target="_blank"><i class="fab fa-instagram"></i></a><br/>
<a href="https://www.youtube.com/channel/UCYrWtDRGUDgpoklSZ6hPmnA" rel="noopener noreferrer" target="_blank"><i class="fab fa-youtube"></i></a><br/>
<a href="https://twitter.com/chilisneindia" rel="noopener noreferrer" target="_blank"><i class="fab fa-twitter"></i></a>
</div>
<div class="copyright-section"><span class="copyright-text">© 2020 Stellar Concepts Pvt. Ltd. All Rights Resevered</span><br/>
<a class="first-child" href="https://www.chilis.in/terms-conditions">Terms &amp; Conditions</a><a href="https://www.chilis.in/sitemap">Sitemap</a><a href="https://www.chilis.in/privacy-policy">Privacy Policy</a>
</div>
</div>
</div> <div class="footer-credits" style="display:none">
<p class="footer-copyright">©
							2021							<a href="https://www.chilis.in/">Chili's</a>
</p><!-- .footer-copyright -->
<p class="powered-by-wordpress">
<a href="https://wordpress.org/">
								chili							</a>
</p><!-- .powered-by-wordpress -->
</div><!-- .footer-credits -->
<a class="to-the-top" href="#site-header">
<span class="to-the-top-long">
<span aria-hidden="true" class="arrow">↑</span> </span><!-- .to-the-top-long -->
<span class="to-the-top-short">
							Up <span aria-hidden="true" class="arrow">↑</span> </span><!-- .to-the-top-short -->
</a><!-- .to-the-top -->
</div><!-- .section-inner -->
</footer><!-- #site-footer -->
<script>
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.chilis.in\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
</script>
<script src="https://www.chilis.in/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.2"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6Lc12vwUAAAAAFqeQvhW3vgX88LY4SOFXuXgY971&amp;ver=3.0"></script>
<script>
var wpcf7_recaptcha = {"sitekey":"6Lc12vwUAAAAAFqeQvhW3vgX88LY4SOFXuXgY971","actions":{"homepage":"homepage","contactform":"contactform"}};
</script>
<script src="https://www.chilis.in/wp-content/plugins/contact-form-7/modules/recaptcha/script.js?ver=5.2"></script>
<script src="https://www.chilis.in/wp-content/plugins/kingcomposer/assets/frontend/js/kingcomposer.min.js?ver=2.9.5"></script>
<script src="https://www.chilis.in/wp-includes/js/wp-embed.min.js?ver=5.4.4"></script>
<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
<script src="https://www.chilis.in/wp-content/themes/chilis/assets/js/jquery-3.5.1.min.js" type="text/javascript"></script>
<script src="https://www.chilis.in/wp-content/themes/chilis/assets/js/owl.carousel.min.js" type="text/javascript"></script>
<script src="https://www.chilis.in/wp-content/themes/chilis/assets/js/jquery.fancybox.min.js" type="text/javascript"></script>
<script src="https://www.chilis.in/wp-content/themes/chilis/assets/js/selectize.js" type="text/javascript"></script>
<script src="https://www.chilis.in/wp-content/themes/chilis/assets/js/all.min.js" type="text/javascript"></script>
<script src="https://www.chilis.in/wp-content/themes/chilis/assets/js/index.js?v=0.1" type="text/javascript"></script>
</body>
</html>
