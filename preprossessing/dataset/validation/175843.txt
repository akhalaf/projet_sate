<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
			navigator.sayswho= (function(){
					var ua=  navigator.userAgent, tem, 
					M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*([\d\.]+)/i) || [];
					if(/trident/i.test(M[1])){
						tem=  /\brv[ :]+(\d+(\.\d+)?)/g.exec(ua) || [];
						return 'IE '+(tem[1] || '');
					}
					M= M[2]? [M[1], M[2]]:[navigator.appName, navigator.appVersion, '-?'];
					if((tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];
					return M.join(' ');
				})();
				
				var WhoAmI = navigator.sayswho;
				if ((WhoAmI.lastIndexOf("IE",0)==0)||(WhoAmI.lastIndexOf("MSIE",0)==0)){
					window.location='legacy.php';
				}
				
				if (!(window.CSS && window.CSS.supports && window.CSS.supports('--test', 'test'))) 
				window.onload = function(){
					var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
					if (!isSafari){
						window.location='legacy.php';
					}
				}
		</script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width" name="viewport"/>
<title>Особистий кабінет :: Авторизація </title>
<link href="./favicon.ico" rel="icon" type="ico"/>
<link href="./template/templates/sn/css/style.css?" rel="stylesheet" type="text/css"/>
<link href="./template/templates/sn/fonts/fontawesome/css/fontawesome.min.css" rel="stylesheet" type="text/css"/>
<!-- <script type="text/javascript" src="./template/templates/default/js/modernizr.js"></script> -->
</head>
<body class="blur">
<div class="space">
<div style="margin:auto;position: relative;">
<form action="login.php" class="login" method="post">
<center>
<img src="img/logo1.png" style="padding-top:20px;"/>
<div class="h3 m3" id="">Кабінет клієнта</div>
<input autofocus="" class="m3" id="login" maxlength="50" name="login" placeholder="Логін" type="text" value=""/>
<input class="m3" id="pass" maxlength="50" name="pass" placeholder="Пароль" type="password"/>
<button class="m3" name="go" type="submit">Ввійти</button>
</center>
</form>
<div class="a-black" style="font-size:88%;position:absolute;/* width:max-content; */white-space:nowrap;">
<div class="h4 m3 i">
<a href="forgot.php">Забули пароль?</a>
</div>
<i></i>
</div>
</div>
</div>
</body>
</html>