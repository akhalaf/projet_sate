<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>ARMRIT MRI Programs</title>
<meta content="Registry of MRI Technologists Accredited MRI Programs List" name="description"/>
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
<link href="/css/offcanvas.css" rel="stylesheet" type="text/css"/>
<link href="/css/carousel.css" rel="stylesheet" type="text/css"/>
<link href="/css/style.css" rel="stylesheet" type="text/css"/>
<link href="/css/new_armrit.css" rel="stylesheet" type="text/css"/>
<!-- Google Analytics -->
<script>
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', 'UA-57746927-3', 'auto');
ga('send', 'pageview');
        </script>
<script async="" src="https://www.google-analytics.com/analytics.js"></script>
<!-- End Google Analytics -->
</head>
<body>
<!-- ARMRIT schools $Revision: 49 $ -->
<div class="container">
<!-- ARMRIT header $Revision: 49 $ -->
<header class="navbar-fixed-top">
<div class="clearfix" style="max-height:180px">
<div class="pull-left">
<a href="/index.php"><img alt="logo" class="img-responsive" src="/images/mast/mastlogo_new.png"/></a>
<span class="logo-alt"><em>American Registry of Magnetic Resonance Imaging Technologists</em></span>
</div>
<div class="pull-left">
<p class="logo" id="gold"><em>“The Gold Standard of MRI Technologist Certification”<br/>“Because MRI is a Specialty”<br/><span style="font-size:90%">"You are MRI when you are A R <span class="under">M R I</span> T"</span></em></p>
</div>
<img alt="seal" class="img-responsive pull-right" id="logo" src="/images/mast/armrit_seal_2.png" style="padding:5px 0" width="150"/>
</div>
<nav>
<button aria-controls="navbar" aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<div class="collapse navbar-collapse" id="navbar">
<ul class="nav navbar-nav" id="navul">
<li><a href="/index.php">Home</a></li>
<li><a href="/about.php">About ARMRIT</a></li>
<li><a href="/services.php">ARMRIT Services</a></li>
<li><a href="/schools.php">MRI Schools</a></li>
<li><a href="https://courses.icpme.us/class_learn?course=522">MRI CME Lectures</a></li>
<li><a href="/jobbrd.php">Job Board</a></li>
<li><a href="/regalfa.php">MRI Tech Resumes</a></li>
<li><a href="/certveri.php">Verify a Tech's Certification</a></li>
<li><a href="/recognition.php">Recognition</a></li>
<li><a href="/links.php">Links</a></li>
</ul>
</div>
</nav>
</header>
<div class="marketing row tb-buffer">
<div class="featurette">
<h2 class="blue">Accredited MRI Programs
                        <br/><span class="smaller">Commission on Accreditation - COA
                        <br/>of the
                        <br/>American Registry of Magnetic Resonance Imaging Technologists</span>
</h2>
<div id="schools">
<ul class="school">
<li><p class="jfa"><span class="school">American Institute of Medical Sciences &amp; Education</span>
<br/>Piscataway, NJ
                                <br/>Phone: 908-222-0002
                            </p>
<p class="j"><strong>24 Month Certificate Program</strong></p>
<p class="j"><a href="https://www.aimseducation.edu/">https://www.aimseducation.edu/</a></p>
</li>
<li><p class="jfa"><span class="school">Apprenticeship - USDOL</span>
<br/>Washington, DC
                                <br/>Phone: 866-487-2365
                                <br/>MRI Technologist O*NET Code: 29-2035.00  Rapids Code: 1115
                            </p>
<p class="j"><strong>18 Month Certificate Program</strong></p>
<p class="j"><a href="http://www.dol.gov/apprenticeship">www.dol.gov/apprenticeship</a></p>
</li>
<li><p class="jfa"><span class="school">Aquarius Institute of Computer Science</span>
<br/>Des Plaines, IL
                                <br/>Phone: 847-296-8870
                            </p>
<p class="j"><strong>One-Year Certificate Program</strong></p>
<p class="j"><a href="http://www.aquariusinstitute.com">www.aquariusinstitute.com</a></p>
</li>
<li><p class="jfa"><span class="school">Career Networks Institute</span>
<br/>Santa Ana, CA
                                <br/>Phone: 714-437-9697
                            </p>
<p class="j"><strong>Associate Degree Program</strong></p>
<p class="j"><a href="http://www.cnicollege.edu">www.cnicollege.edu</a></p>
</li>
<li><p class="jfa"><span class="school">Casa Loma College</span>
<br/>Van Nuys, CA
                                <br/>Phone: 818-785-2726
                            </p>
<p class="j"><strong>Associate Degree Program</strong></p>
<p class="j"><a href="http://www.casalomacollege.edu">www.casalomacollege.edu</a></p>
</li>
<li><p class="jfa"><span class="school">Gurnick Academy of Medical Arts</span>
<br/>Modesto, CA
                                <br/>Phone: 209-521-1821
                            </p>
<p class="j"><strong>Associate Degree Program</strong></p>
<p class="j"><a href="http://www.gurnick.edu">www.gurnick.edu</a></p>
</li>
<li><p class="jfa"><span class="school">Gurnick Academy of Medical Arts</span>
<br/>Sacramento, CA
                                <br/>Phone: 916-588-2060
                            </p>
<p class="j"><strong>Associate Degree Program</strong></p>
<p class="j"><a href="http://www.gurnick.edu">www.gurnick.edu</a></p>
</li>
<li><p class="jfa"><span class="school">Gurnick Academy of Medical Arts</span>
<br/>San Mateo, CA
                                <br/>Phone: 650-685-6616
                            </p>
<p class="j"><strong>Associate Degree Program</strong></p>
<p class="j"><a href="http://www.gurnick.edu">www.gurnick.edu</a></p>
</li>
<li><p class="jfa"><span class="school">Med Academy</span>
<br/>Hialeah, FL
                                <br/>Phone: 786-792-3350<br/>Email: <a href="mailto:info@medacademy.edu">info@medacademy.edu</a>
</p>
<p class="j"><strong>Associate Degree Program</strong></p>
<p class="j"><a href="http://www.medacademy.education">www.medacademy.education</a></p>
</li>
<li><p class="jfa"><span class="school">Midwestern Career College</span>
<br/>Chicago, IL
                                <br/>Phone: 312-236-9000
                            </p>
<p class="j"><strong>Associate Degree Program</strong></p>
<p class="j"><a href="http://www.mccollege.edu">www.mccollege.edu</a></p>
</li>
<li><p class="jfa"><span class="school">Midwestern Career College</span>
<br/>Naperville, IL
                                <br/>Phone: 630-536-8679
                            </p>
<p class="j"><strong>Associate Degree Program</strong></p>
<p class="j"><a href="http://www.mccollege.edu">www.mccollege.edu</a></p>
</li>
<li><p class="jfa"><span class="school">National Polytechnic College</span>
<br/>Lakewood, CA
                                <br/>Phone: 888-243-2493
                            </p>
<p class="j"><strong>18 month Certificate Program</strong></p>
<p class="j"><a href="http://www.npcollege.edu">www.npcollege.edu</a></p>
</li>
<li><p class="jfa"><span class="school">Tesla Institute of MRI Technology</span>
<br/>Fairfax, VA
                                <br/>Email: <a href="mailto:admissions@teslamrinstitute.org">admissions@teslamrinstitute.org</a>
</p>
<p class="j"><strong>One-Year Certificate Program</strong></p>
<p class="j"><a href="http://www.teslamrinstitute.org">www.teslamrinstitute.org</a></p>
</li>
<li><p class="jfa"><span class="school">West Coast Ultrasound Institute</span>
<br/>Los Angeles, CA
                                <br/>Phone: 310-289-5123
                            </p>
<p class="j"><strong>Associate Degree Program</strong></p>
<p class="j"><a href="http://www.wcui.edu">www.wcui.edu</a></p>
</li>
<li><p class="jfa"><span class="school">West Coast Ultrasound Institute</span>
<br/>Ontario, CA
                                <br/>Phone: 909-483-3808
                            </p>
<p class="j"><strong>Associate Degree Program</strong></p>
<p class="j"><a href="http://www.wcui.edu">www.wcui.edu</a></p>
</li>
<li><p class="jfa"><span class="school">West Coast Ultrasound Institute</span>
<br/>Phoenix, AZ
                                <br/>Phone: 602-954-3834
                            </p>
<p class="j"><strong>Associate Degree Program</strong></p>
<p class="j"><a href="http://www.wcui.edu">www.wcui.edu</a></p>
</li>
</ul>
<p class="center"><span class="star">*</span>Provisional - Pending Site Visit.</p>
</div>
<p class="accred"><strong>Application</strong> - To receive an Application for Accreditation,
                        <br/>mail a Letter of Request (on institutional letterhead) to:
                        <br/><br/>Constance Felice, Manager
                        <br/>Commission on Accreditation
                        <br/>2444 NW 8th Street
                            <br/>Delray Beach, FL 33445
                    </p>
</div>
</div>
</div>
<!-- ARMRIT footer $Revision: 41 $ -->
<footer class="footer navbar">
<div class="container">
<div class="row">
<div class="col-md-6">
<h4>Site Map</h4>
<ul class="list-no-style">
<li><a href="index.php">Home</a></li>
<li><a href="pdf/APP08152020.pdf">Candidate Handbook</a></li>
<li><a href="services.php">ARMRIT Services</a></li>
<li><a href="renew.php?year=2021">RENEWAL 2021-2023</a></li>
<li><a href="https://courses.icpme.us/class_learn?course=522">MRI CME Lectures</a></li>
<li><a href="jobbrd.php">Job Board</a></li>
<li><a href="schools.php">MRI Schools</a></li>
<li><a href="regalfa.php">MRI Tech Resumes</a></li>
<li><a href="news.php">Newsletters</a></li>
<li><a href="test.php">Practice Tests</a></li>
<li><a href="certveri.php">Verify a Tech's Certification</a></li>
<li><a href="links.php">Links</a></li>
<li><a href="pdf/Code_of_Ethics1.pdf">Code of Ethics</a></li>
</ul>
</div>
<div class="center_text col-md-6">
<span id="update"></span>
<p>Phone: 561-450-6880 | Email: <span id="contact"></span> | Fax: 561-265-5045</p>
<p><strong>Mailing Address</strong>: ARMRIT
                            <br/>2444 NW 8th Street
                            <br/>Delray Beach, FL 33445</p>
<p>Problems or questions, please contact <span class="contact"></span>.<br/>HomePage by <a href="https://tstewartsolutions.com/" target="_blank">TStewartSolutions</a></p>
</div>
</div>
</div>
</footer>
<script src="/java/footer_mail.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/offcanvas.js"></script>
<script>
            var liList = document.getElementById("navul").getElementsByTagName("li");
            var largo = liList.length;
            if ( largo > 12 ) {
               navul.setAttribute("class", "nav navbar-nav navs");
            }
        </script>
<!-- ARMRIT scripts $Revision: 52 $ -->
</body>
</html>
