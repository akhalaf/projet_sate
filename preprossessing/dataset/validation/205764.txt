<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<!-- 
	This website is powered by TYPO3 - inspiring people to share!
	TYPO3 is a free open source Content Management Framework initially created by Kasper Skaarhoj and licensed under GNU/GPL.
	TYPO3 is copyright 1998-2019 of Kasper Skaarhoj. Extensions are copyright of their respective owners.
	Information and contribution at https://typo3.org/
-->
<title>beeline | beeline group</title>
<meta content="TYPO3 CMS" name="generator"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="#ffffff" name="theme-color"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="/typo3conf/ext/sr_feuser_register/Resources/Public/StyleSheets/default.css?1511189376" media="all" rel="stylesheet" type="text/css"/>
<link href="/typo3conf/ext/q4u_cookie_consent/Resources/Public/CSS/cookie-consent.css?1586319488" media="all" rel="stylesheet" type="text/css"/>
<link href="/fileadmin/tmpl/dist/css/main.css?1586323741" media="all" rel="stylesheet" type="text/css"/>
<link href="/manifest.json" rel="manifest"/><style>.header { background: url("/fileadmin/user_upload/start_neu_2.jpg") center/cover no-repeat }.header .header-bar .header-bar-bg .header-bar-bg-inner { background: url("/fileadmin/user_upload/start_neu_2.jpg") center/cover no-repeat }</style> <link href="/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/manifest.json" rel="manifest"/>
<link color="#cccc33" href="/safari-pinned-tab.svg" rel="mask-icon"/> <script type="text/javascript">
      var showcookieconsent = true;
    </script>
<meta content="beeline" name="DCTERMS.title"/>
<meta content="2020-12-14T16:57:20+01:00" name="date"/>
<meta content="2020-12-14T16:57:20+01:00" name="DCTERMS.date"/>
<meta content="index,follow" name="robots"/>
<link href="https://www.beeline-group.com/en/" rel="canonical"/>
<meta content="beeline" property="og:title"/>
<meta content="article" property="og:type"/>
<meta content="https://www.beeline-group.com/en/" property="og:url"/>
<link href="/typo3conf/ext/solr/Resources/Css/JQueryUi/jquery-ui.custom.css?1508496843" media="all" rel="stylesheet" type="text/css"/>
</head>
<body class="home">
<header class="header">
<!-- <img src="/fileadmin/user_upload/start_neu_2.jpg" height="1364" width="2953" data-focus-y="0" data-focus-x="0" alt=""/> -->
<div class="cage-header">
<div class="header-bar">
<div class="header-bar-bg">
<div class="header-bar-bg-inner"></div>
</div>
<div class="header-bar-inner">
<div class="logo">
<a href="/en/">
<svg data-name="beeline_logo" id="beeline_logo" viewbox="0 0 101 49" xmlns="http://www.w3.org/2000/svg">
<g fill="none" fill-rule="evenodd">
<path d="M70 41.5c1.4 0 2.3.8 2.3 2.4 0 1.7-1 2.6-2.4 2.6l-1-.1V48h-.2l-.3.2v-6.5l1.6-.2zm-21.3 0l1.5.1.1.1V45.6c0 1.1-.3 1.8-1 2.4h-.5c.3-.3.9-1 1-1.5l-.1-.1h-1.8c-1-.4-1.6-1.2-1.6-2.6 0-1.5 1-2.3 2.4-2.3zm9.4 0c1.4 0 2.4.8 2.4 2.5 0 1.6-1 2.5-2.4 2.5-1.3 0-2.4-.9-2.4-2.5 0-.5.1-1 .3-1.3.3-.7.9-1 1.6-1.2h.5zm4.6 0v3c0 1 .7 1.5 1.6 1.5h1.2l.1-.1v-4.3h.4v4.6c-.4.1-1 .3-1.7.3-1.1 0-2-.7-2-1.9v-3zm-8.4 0v.4a16.9 16.9 0 00-1.3.1V46.4h-.4v-4.7l1.7-.2zm3.8.4c-1 0-2 .6-2 2 0 1.5.9 2.1 2 2.1 1.2 0 2-.6 2-2 0-1.5-.9-2.1-2-2.1zm-9.4 0c-1.2 0-1.9.6-1.9 2 0 1.5.8 2.1 2 2.1h1V42 42h-1.1zm21.2 0h-1v4l1 .1c1.1 0 2-.7 2-2 0-1.4-.7-2-2-2zm12.7-17.4c4.6 0 6.8 2.6 6.8 8.2 0 .1 0 .2-.2.2H78.6c-.1 0-.2 0-.2.3.1 3.2 2.2 3.9 5.4 3.9l3.8-.2.2.1-.2 1.6c0 .2-.2.2-.3.3-1.3.1-2.8.3-4.4.3-4.3 0-7.2-1.8-7.2-7.4 0-5.5 3.1-7.3 6.9-7.3zM1.9 19.4c.2 0 .3 0 .3.2v5c0 .1 0 .2.2.2l3.3-.3 1.5.1c3.3.6 5.3 2.9 5.3 7 0 2-.5 3.6-1.3 4.8-1.2 1.8-3.3 2.7-5.8 2.7L1 38.8c-.2 0-.3-.1-.3-.4V20c0-.1 0-.2.2-.3zm19.6 5.1c1.2 0 2.2.2 3 .6 2.2 1 3.3 3.3 3.3 6v1.2l-.2.2h-11c-.1 0-.2 0-.2.2 0 2.8 1.3 4.6 4 5l1.7.1c1.4 0 3 0 4.1-.2.1 0 .3 0 .3.2v.1l-.2.7a39.3 39.3 0 01-4.6.5c-4.3-.2-6.4-2.1-6.7-6v-1.3c0-1.2 0-2.2.4-3.1.6-1.9 1.8-3 3.4-3.7.8-.3 1.8-.5 2.7-.5zm15.6 0c4.1 0 6.2 2.8 6.2 6.6v1.1c0 .2 0 .3-.2.3H32.2c-.2 0-.3 0-.3.2 0 1 .2 1.8.5 2.5.7 1.7 2.4 2.6 5.2 2.6 1.4 0 3 0 4.2-.2.1 0 .2 0 .2.2v.1l-.1.7a39.3 39.3 0 01-4 .5c-4.4 0-6.6-1.7-7.3-5.1l-.1-2.2c0-5 2.9-7.3 6.6-7.3zm28.9 0h1.6c3.2.4 5 1.8 5 5.5v8.8c0 .1 0 .2-.2.2h-2.2c-.2 0-.2 0-.2-.2V30c0-2.2-1-3.2-2.8-3.4l-1.3-.1c-.9 0-2.1 0-3 .2-.2 0-.2 0-.2.2v11.8c0 .2 0 .2-.2.2h-2.2c-.2 0-.3 0-.3-.2V25.4c0-.2 0-.3.3-.3 2-.3 4.3-.6 5.7-.6zm-10.4 0c.2 0 .3.2.3.3v14c0 .1 0 .2-.2.2h-2.2c-.2 0-.3 0-.3-.2v-14c0-.1.1-.2.3-.2zM5.5 25.9l-3.1.2c-.1 0-.2 0-.2.2v11.2s0 .2.2.2l3.2.2c3.2 0 5.4-2 5.4-6 0-3.7-1.5-5.5-4.3-6H5.5zm15.9 0a5 5 0 00-3 .9c-1.3.9-2 2.4-2 4.3v.2H26c.1 0 .2 0 .2-.2 0-1-.1-1.8-.4-2.5l-.2-.4-.1-.3-.2-.2c-.8-1.1-2.1-1.8-4-1.8zm15.5 0c-3 0-4.9 2-5 5.2 0 .2 0 .3.2.3h9.6c.1 0 .2 0 .2-.3 0-3.1-1.7-5.2-5-5.2zm45.5.7c-2.4 0-4 1.4-4 4.2 0 .2 0 .2.2.2h7.8c.2 0 .3 0 .3-.2 0-2.8-1.7-4.2-4.3-4.2zm-26.7-6.9c.1 0 .2 0 .2.3v2c0 .2 0 .3-.2.3h-2.2c-.2 0-.3 0-.3-.3v-2c0-.2.1-.3.3-.3z" fill="#FFF"></path>
<path d="M45.8.3l.3.1.3.7.1.1v.1l.1.1.1.3-.1 1h-.1l-.1.3-.1.2-.3.4-.3.4-.2.3a55.8 55.8 0 00-3.8 6.9l-.7 2v.3l-.2.5c.3 0 .6-.2 1-.4l1.3-.8.4-.4c.1 0 .2 0 .3-.2a56.1 56.1 0 001.4-1l1-1 2.3-1.4a2.3 2.3 0 011.3-.3h.2a10.6 10.6 0 01.3 0c.6.3 1 .8 1.3 1.3l.1.1.5 1.1.1.4.1.5v.2c.2.7.1 1.5.1 2.1a21.7 21.7 0 000 1.3l.6-.2v-.4l.2-1.3v-.1l.3-1 .2-.2c0-.3.3-.6.4-.9l.2-.3.8-1c.4-.3 1-.7 1.8-.9h1.1a3 3 0 011.5 1.8v.9h-.1l-1 1.5a8 8 0 00-.6.7l-.8.7a7.6 7.6 0 00-.7.5l-.1.5-.3 1 .4-.1a5.7 5.7 0 002-1.2l.9-.7.5-.4 1-.8.2-.1a6.7 6.7 0 011.1-3c.3-.5.7-.9 1-1.1l.3-.1a4 4 0 011-.6 2.1 2.1 0 011.5 0l1.2.9.7.5c.3.4.5.8.5 1.2v.2c.1.2.2.5 0 .7v1.3l-.1.9-.1.7-.2.7-.1.7-.2.6-.1.8.3-.2.4-.4h.1l1.3-1.4.8-1.1a44.5 44.5 0 002.7-4.2l-.1-.1-.5-.6-.7-1.4v-.6l-.1-.2v-.1-.4V7c0-.9.5-1.7.8-2.2.6-1 1.2-1.7 1.9-2.4l1.2-.8H76.2a.3.3 0 01.3 0l.1.2.3.2a1 1 0 01.6.8c0 .4 0 .8-.2 1.3v.1l-.1.3-.4 1.1-.2.6-.5 1.4a28.2 28.2 0 01-.7 1.4h1.2a8.4 8.4 0 00.3 0l.1.1.9.4c.4.2 1 .4 1.3 1v.4c.2.2.2.4.2.5V12.2c0 .4 0 .7-.2 1v.3l-.2.8v.3l-.5 1.6a60.3 60.3 0 00-.4 1.5l-.3 1.1-.2.5v.8l.4-.7a22 22 0 01.3-.4l1.5-1.7.1-.2.2-.2.2-.2.3-.3.4-.5.5-.6c.2-.3.3-.6.6-.8l.2-.3.2-.2.5-.7.2-.3a15 15 0 011.3-1.4l.2-.1v-1c0-.7.3-1.2.5-1.7l.8-1.2.1-.2 1.2-1.3a15.5 15.5 0 012.3-1.7 5.8 5.8 0 01.6-.3l.2-.1.5-.3.3-.2h.1a32.6 32.6 0 001-.4 3.4 3.4 0 011.6-.2h.2l.3.6v.1c.4.5 0 1-.1 1.2l-1.6 1.4a7 7 0 00-.3.3L92 7A96 96 0 0190.5 8a120.9 120.9 0 01-3.2 3.2v.1l-.2.3h.7c.2 0 .5 0 .7.2H89.1l1 .2.3.1h.3l.5.2 1.2.7.4.5.1.2a8.1 8.1 0 00.7-.3l.6-.2c.3 0 .5-.2.8-.3h.3l.6-.3.7-.2 1.1-.4 1-.3.4-.2c.3 0 .5-.2.8-.3H100.5c.2.4.3.7.2 1.1l-.1.2-.2.1a2 2 0 01-.4.2c-.3 0-.5.2-.8.3l-.8.3-.9.3-.7.3c-.2 0-.4.2-.5.3l-.6.2-1.5.6H94l-.4.2v.1l.2.5v.3a17.2 17.2 0 010 1v1.2l-.1 1.2-.1.5a6.7 6.7 0 01-.4 1.3l-.2.7-.6 1.6v.2c-.4.6-.7 1.3-1.2 1.9l-.4.5-1 1.2-.2.1A9.1 9.1 0 0184 30c-1.1 0-2-.4-2.8-.9a2 2 0 01-.8-.5v-.1l-.2-.1-.9-1.4v-.3l-.2-.3-.2-.9v-.4l.3-1.5a5.4 5.4 0 011.1-1.8l.8-1 .5-.5.5-.5.1-.2 1-.7.5-.5.7-.4.3-.3c.3-.1.7-.3 1-.6l.7-.4.3-.1 2-1 .5-.3.7-.3.9-.4h.1l.3-.1c-.2-.2-.5-.2-.8-.3h-.3l-.7-.2h-3.1c-.6 0-1-.2-1.4-.7v.2l-.2.2A32 32 0 0083 16l-.6.9-.3.5-1 1.4-.2.3-.9 1-.5.6-.6 1-.5.6c-.2.3-.6.7-1 .9h-.5-.1-.2v-.1l-.3-.3-.4-.4-.8-1a5 5 0 01-.4-1.2V18c0-.5 0-.9.2-1.3v-.2c.4-1.5 1-2.9 1.7-4l.1-.3a15.6 15.6 0 011-1.8l-.4-.1a.7.7 0 00-.4-.2l-.7.3h-.1l-.8.5a2 2 0 01-.5.3h-.1l-.2.1h-.3l-.1.2-.5.9-.2.3-.8 1.3-1.3 2-.8 1-.4.5-1.3 1.5a6 6 0 01-1.2.9H67h-.2-.1l-.1-.1c-.5-.5-1-1-1.2-1.5-.3-.4-.4-.8-.5-1.4v-.7l-.4.6-.4.6-.4.3-.3.1H62.8l-.1-.1-.4-.3-.5-.7-.4-.5-.3-.7-.7.6-1 1-1 1c-.4.5-1 1-1.6 1.3l-.7.2a5.2 5.2 0 00-.4.1h-.2c-.4-.1-.9-.2-1.2-.7l-.4-1-.4-.8-.1-.5-.2.1-.4.2-.5.2h-.1v.2c0 .3 0 .7-.2 1v.7l-.1.2v.2l-.3 1.1-.1.6-.2.7-.1.4-.2.6a4 4 0 00-.3.8l-.3.7-.3.7-.3.5-.1.3a14.1 14.1 0 01-3 4c-.6.5-1.4 1-2.4 1.4-.1 0-.3 0-.4.2a7 7 0 01-.3 0l-.2.1H43l-.2.2h-1l-.3-.1c-.6 0-1.3-.2-1.9-.6l-1-.8a5.1 5.1 0 01-.8-1.1v-.1l-.4-.7-.4-1c-.1-.4-.2-.9-.1-1.3v-.4c0-.2 0-.5.2-.8v-.2l.1-.2.2-.5v-.2l.6-.9h.1a9.4 9.4 0 013.1-2.8l.5-.3h.1c.3-.3.7-.5 1-.6l.6-.2.2-.1 1-.5.7-.2a17 17 0 011.6-.7l.9-.3.7-.2.6-.3a7.7 7.7 0 00.9-.3l.1-.4.1-.4v-.1l.2-.6.1-1v-.2l.2-.7v-.5-.7-1.2l-.3-.2-.4-.1a2 2 0 00-.7.1h-.1l-1.4.7-1.5 1.2-.3.2-.2.1-.3.4-.3.2-.1.1-.1.2c-.2 0-.4.2-.5.4a2.6 2.6 0 01-.7.6h-.1c-.6.7-1.2 1.2-1.8 1.5l-.6.3a8.9 8.9 0 00-.3.2h-.6-.1c-.8-.3-1.1-1-1.2-1.6l-.1-.1-.4-1.1v-.1l-.1-.9a16 16 0 000-1v-.1-.7l.1-.2.1-.5v-.2l.1-.4.3-.6c0-.2 0-.4.2-.6l.8-1.9A17 17 0 0141.7 4l.2-.3.9-1 .1-.1.5-.6.6-.5c.4-.4.8-.8 1.4-1h.2a9.6 9.6 0 00.2-.1zm3.5 18.2h-.2l-.6.3-.6.2-.9.4-.3.2c-.5 0-1 .3-1.4.5l-.8.4a7.2 7.2 0 00-1.2.6l-.2.1-.5.4-.8.6c-.2 0-.4.2-.5.3-.6.4-1 1-1.5 1.6l-.6 1a4 4 0 00-.4 1.7v.3l-.1.1v.2l.4 1h.2a9.6 9.6 0 00.7.3 1.6 1.6 0 00.3 0l.3-.1 1.2-.3a6 6 0 001.2-.7l.4-.2.8-.6.4-.3.5-.5c0-.2.2-.3.4-.4l.2-.2.4-.5.1-.2.2-.3.4-.5.2-.2.4-.8.3-.6.4-.7.3-.8.2-.4a14 14 0 00.7-2zm43.1-2.6l-.9.4h-.3l-3 1.4H88a17.7 17.7 0 00-2.4 1.4c-.3 0-.5.3-.8.4l-.6.5-.8.6-.3.2c-.4.4-.9.8-1.3 1.4l-.8 1.1a35.2 35.2 0 01-.5.8l-.3 1.8c0 .3.1.5.3.7l1.3.8h.2l.3.1h.5l.4.1h.8l.4-.1.6-.2c.6-.2 1.4-.4 2-.8l1.4-1.1 1-1a15.7 15.7 0 002.3-4V20a32.5 32.5 0 01.2-.5v-.3l.2-.7.1-.2.1-1 .1-.5v-.3-.2-.5zM19 .2h2.7a85.6 85.6 0 011.5.1c.3.1.5.3.5.7a1.1 1.1 0 010 .3l.1.6-.1.7-.3.4A7 7 0 0122 4c-.5.5-1 .9-1.6 1.2l-.8.5-.5.3-.4.3a120.3 120.3 0 01.3 0H19.3c.8 0 1.3.2 1.8.6.2 0 .3.2.4.3a3.8 3.8 0 01.7 1.7l.1.8V13l-.1.2v.4l-.1.1v.3l-.3 1.3v.1a11.8 11.8 0 01-2.1 4l-.7.7a13.9 13.9 0 00-.6.6 24.6 24.6 0 00-.7.5 8.8 8.8 0 01-2.8 1.2h-.5l-.5.2h-1-.1c-.5 0-1-.2-1.4-.4-.5-.1-1-.4-1.6-.7a6.4 6.4 0 01-2-1.6 18 18 0 01-1-1.4v-.1l-.2-.1-.4-1v-.2l-.1-.3v-.3-.6c-.1-.3 0-.6.2-1H7l.1.1c.2 0 .2.2.3.3v.4l.2.1.3.6.2.2a5.2 5.2 0 001.7 1.5l1 .5h.1l.6.2h.1l.6.1a13.1 13.1 0 001 0l1-.1h.1l.7-.2.2-.1.4-.1.3-.2.7-.3.2-.2.2-.1.4-.2.1-.2.6-.5.4-.4.4-.5.2-.2.5-.9.2-.2a10 10 0 001-3l.2-1V10v-.6l-.1-.2-.2-.5c-.2-.3-.4-.5-.7-.6l-.7-.2a2.2 2.2 0 00-.7 0H18.4l-1.3.1-1.6.6-1.9.8-.4.2a5.7 5.7 0 00-.3 0h-.2l-.3-.3-.1-.2v-.9a.3.3 0 01.1-.3c0-.3.2-.5.3-.7V7l.5-.4 1.3-.8h.1l.7-.5 1-.5.5-.3a17.8 17.8 0 001.6-1l.6-.3a82 82 0 001.1-.7 102.7 102.7 0 01-2.3 0H17h-.6l-.8-.1a11 11 0 00-1.2 0 1 1 0 00-.3 0H14l-.3-.1h-.4a5 5 0 01-.7-.1c-.2 0-.5-.1-.7-.5v-.1-.1-.1-.3l.2-.4c0-.1 0-.2.2-.2l.5-.1h1.4a3.4 3.4 0 01.7-.1h.5l1.1-.1h1a12.7 12.7 0 001 0h.4zm11.3.7c.2 0 .4.2.6.4l.2.2.3.4c.4-.3 1-.5 1.4-.6h.4l.3-.1c.6 0 1 .2 1.4.5l.7.8.4.9.1 1.1V5.3c0 .4 0 .8-.2 1.2v.2l-.2 1a31.6 31.6 0 00-.3 1 26 26 0 01-.6 2v.1l-.6 1.3-.3.7-.1.4-.2.3a14.5 14.5 0 00-.4.9 50.1 50.1 0 00-1.1 1.9l-.8 1.1-.2.4a19.9 19.9 0 01-2 2.3 7 7 0 01-.8.6l-.7.4h-.1c-.2.2-.4.2-.6.2h-.2-.2a2 2 0 01-1.3-.6l-.2-.2-.1-.1-.6-.6-.1-.1c-.3-.3-.5-.7-.6-1.2a5 5 0 01-.3-1.3v-1a.3.3 0 010-.2l.1-.7V15l.2-.5v-.3l.1-.4.1-.4v-.2l.3-.7.1-.3a21.5 21.5 0 011-2.7v-.2l.3-.9.2-.3.1-.3.3-.6.4-1 .9-1.7c.3-.7.7-1.4 1.2-2v-.1l1-1.1.2-.2.5-.2zM34 2.8h-.3l-.4.1-.2.1a8.5 8.5 0 00-2.4 1.7l-.1.1-.3.8-.3.7a11.9 11.9 0 00-.6 1.4 37.6 37.6 0 01-.6 1.2l-.2.7-.3.8-.4.9A29 29 0 0127 14l-.3 1-.1.5-.1.4v.3l-.1.4-.1.5-.1.3v.5l-.1.5v1h.4l.3-.2.7-.4h.1l.8-.7a12.9 12.9 0 001.2-1.5l.2-.1a19.9 19.9 0 002.6-4.4l.7-1.4A17.7 17.7 0 0034 8l.4-1.6.1-.3v-.2-.4-.1l.1-.4v-.2a3.2 3.2 0 010-.4 3 3 0 000-.9v-.3l-.3-.3a.6.6 0 00-.4-.1zm32 8.7h-.2l-.2.3-.2.3-.5.7-.3.6a.3.3 0 00.5.3l.5-.6h.1l.2-.4.4-.8a.3.3 0 00-.3-.4zM58 11h-.2l-.3.2-.1.2-.1.2c-.3.2-.5.6-.7 1l-.2.1v.3a.1.1 0 00.2.2c.6-.6 1.2-1.1 1.5-2a.1.1 0 00-.1-.2z" fill="#CAD400" style="fill: #CAD400 !important;"></path>
<path d="M48.8 21c.2 0 .3 0 .3.2v17.6c0 .2-.1.2-.3.2h-2.2c-.1 0-.2 0-.2-.2V21.5c0-.2 0-.2.2-.3zm33.6 3.5v2a4 4 0 00-3 1h-2.8c1.2-2.1 3.3-3 5.8-3zm-45.3 0c2.6 0 4.4 1.1 5.3 2.9h-1.6c-.9-1-2.1-1.6-3.9-1.6-.6 0-1.2 0-1.8.3v-1.3c.7-.2 1.3-.3 2-.3z" fill="#FFF"></path>
</g>
</svg>
</a>
</div>
<nav class="nav-main">
<div class="nav-burger">
<span></span>
<span></span>
<span></span>
</div>
<div class="header-nav-content">
<ul><li class="ifsub"><a href="/en/company/">Company</a><ul><li class="ifsub"><a href="/en/company/brands/">Brands</a></li><li><a href="/en/company/history/">History</a></li><li><a href="/en/company/we-care/">We care</a></li><li><a href="/en/company/vision-and-values/">Vision and values</a></li></ul></li><li class="ifsub"><a href="/en/business-partner/">Business Partner</a><ul><li><a href="/en/business-partner/retail/">Retail</a></li><li><a href="/en/business-partner/e-commerce/">E-Commerce</a></li><li><a href="/en/business-partner/concessions/">Concessions</a></li><li><a href="/en/business-partner/business-excellence/">Business Excellence</a></li></ul></li><li class="ifsub"><a href="/en/career/">Career</a><ul><li><a href="https://jobs.beeline-group.com/content/Open-position-in-englisch/?locale=en_US">Open positions</a></li><li><a href="/en/career/benefits/">Benefits</a></li><li class="ifsub"><a href="/en/career/job-portraits/">Job Portraits</a></li><li class="ifsub"><a href="/en/career/working-areas/">Working Areas</a></li><li><a href="/en/career/working-for-beeline/">Working for beeline</a></li></ul></li><li class="ifsub"><a href="/en/services/">Services</a><ul><li class="ifsub"><a href="/en/services/media/">Media</a></li><li><a href="/en/services/contact/">Contact</a></li><li><a href="/en/services/location-finder/">Location Finder</a></li><li><a href="https://doc.beeline-group.com/en/">EU DECLARATION OF CONFORMITY</a></li></ul></li></ul>
</div>
</nav>
<div class="search">
<div class="search-toggle"></div>
<div class="tx-solr">
<div class="tx-solr-searchbox">
<script type="text/javascript">
				/*<![CDATA[*/
				var tx_solr_suggestUrl = 'https://www.beeline-group.com/?eID=tx_solr_suggest&id=1';
				/*]]>*/
				</script>
<form accept-charset="utf-8" action="/en/search/" class="search-form" id="tx-solr-search-form-pi-search" method="get">
<input name="id" type="hidden" value="43"/>
<input name="L" type="hidden" value="0"/>
<input class="tx-solr-q" id="search-field" name="q" placeholder="Search beeline-group.com" type="text" value=""/>
<label for="search-field">Search beeline-group.com</label>
<button id="search-submit"><span>send</span></button>
</form>
</div>
</div>
</div>
</div>
</div>
<nav class="nav-breadcrump">
</nav>
</div>
</header>
<main class="main">
<!--TYPO3SEARCH_begin-->
<div class="cage-hero">
<div id="c954">
<div class="grid-block-1">
<div class="grid-block-1-11">
<div class="card">
<a href="/en/career/">
<img alt="" class="card__image" height="4320" src="/fileadmin/_processed_/1/b/csm_beeline-138603_5180369786.jpg" title="Creative Buying" width="4320"/>
<div class="card__text">
<h2>Career</h2>
</div>
</a>
</div>
</div>
<div class="grid-block-1-12">
<div class="card">
<a href="/en/company/brands/">
<div class="card__mini-slider">
<div class="card__mini-slider__item">
<img alt="" class="card__image rsImg" height="460" src="/fileadmin/user_upload/Logos/iam.png" width="460"/>
</div>
<div class="card__mini-slider__item">
<img alt="" class="card__image rsImg" height="1089" src="/fileadmin/user_upload/Home/Brands/TOSH/TOSH_Logo_quadrat_01.jpg" width="1089"/>
</div>
<div class="card__mini-slider__item">
<img alt="" class="card__image rsImg" height="460" src="/fileadmin/user_upload/Home/Brands/SIX/six.png" width="460"/>
</div>
</div>
</a>
</div>
</div>
<div class="grid-block-1-13">
<div class="card">
<a href="/en/career/benefits/">
<img alt="" class="card__image" height="1916" src="/fileadmin/_processed_/b/d/csm_Header_Benefits_01_ad27ac1268.jpg" width="1916"/>
<div class="card__text">
<h2>Benefits</h2>
</div>
</a>
</div>
</div>
<div class="grid-block-1-14">
<div class="card">
<a href="/en/company/we-care/">
<img alt="" class="card__image" height="2157" src="/fileadmin/_processed_/f/2/csm_beeline-640800_bbd60f746b.jpg" width="2160"/>
<div class="card__text">
<h2>We care</h2>
</div>
</a>
</div>
</div>
<div class="grid-block-1-15">
<div class="card">
<a href="/en/services/location-finder/">
<img alt="" class="card__image" height="1448" src="/fileadmin/_processed_/a/0/csm_beeline-loc32601_9fc7ed681f.jpg" width="679"/>
<div class="card__text">
<h2>Location Finder</h2>
</div>
</a>
</div>
</div>
<div class="grid-block-1-16">
<div class="card">
<a href="/en/company/history/">
<img alt="" class="card__image" height="378" src="/fileadmin/_processed_/a/d/csm_30_years_quadrat_f420c0cada.jpg" width="378"/>
<div class="card__text">
<h2>History</h2>
</div>
</a>
</div>
</div>
<div class="grid-block-1-17">
<div class="card">
<a href="/en/company/">
<img alt="" class="card__image" height="291" src="/fileadmin/_processed_/d/1/csm_Header_LocationFinder_3dffa9d452.jpg" width="621"/>
<div class="card__text">
<h2>Company</h2>
</div>
</a>
</div>
</div>
</div>
</div>
<div id="c348">
<div class="grid-block-2">
<div class="grid-block-2-11">
<div class="card">
<a href="/en/services/media/press-releases/">
<img alt="" class="card__image" height="225" src="/fileadmin/user_upload/Home/Presse_Icon.png" width="225"/>
<div class="card__text">
<h2>Press Center</h2>
</div>
</a>
</div>
</div>
<div class="grid-block-2-12">
<div class="card">
<a href="/en/business-partner/">
<img alt="" class="card__image" height="2158" src="/fileadmin/_processed_/7/7/csm_beeline-539960_01_8ee45a8deb.jpg" width="3220"/>
<div class="card__text">
<h2>Business Partner</h2>
</div>
</a>
</div>
</div>
<div class="grid-block-2-13">
<div class="card">
<a href="/en/services/contact/">
<img alt="" class="card__image" height="1066" src="/fileadmin/_processed_/2/7/csm_Nr_7_ISC_8e3210a54e.jpg" width="1067"/>
<div class="card__text">
<h2>Contact</h2>
</div>
</a>
</div>
</div>
</div>
</div>
</div>
<div class="cage-main">
</div>
<!--TYPO3SEARCH_end-->
</main>
<footer class="footer">
<div class="cage-footer">
<nav class="nav-sitemap">
<ul><li class="ifsub"><a href="/en/company/">Company</a><ul><li class="ifsub"><a href="/en/company/brands/">Brands</a></li><li><a href="/en/company/history/">History</a></li><li><a href="/en/company/we-care/">We care</a></li><li><a href="/en/company/vision-and-values/">Vision and values</a></li></ul></li><li class="ifsub"><a href="/en/business-partner/">Business Partner</a><ul><li><a href="/en/business-partner/retail/">Retail</a></li><li><a href="/en/business-partner/e-commerce/">E-Commerce</a></li><li><a href="/en/business-partner/concessions/">Concessions</a></li><li><a href="/en/business-partner/business-excellence/">Business Excellence</a></li></ul></li><li class="ifsub"><a href="/en/career/">Career</a><ul><li><a href="https://jobs.beeline-group.com/content/Open-position-in-englisch/?locale=en_US">Open positions</a></li><li><a href="/en/career/benefits/">Benefits</a></li><li class="ifsub"><a href="/en/career/job-portraits/">Job Portraits</a></li><li class="ifsub"><a href="/en/career/working-areas/">Working Areas</a></li><li><a href="/en/career/working-for-beeline/">Working for beeline</a></li></ul></li><li class="ifsub"><a href="/en/services/">Services</a><ul><li class="ifsub"><a href="/en/services/media/">Media</a></li><li><a href="/en/services/contact/">Contact</a></li><li><a href="/en/services/location-finder/">Location Finder</a></li><li><a href="https://doc.beeline-group.com/en/">EU DECLARATION OF CONFORMITY</a></li></ul></li></ul>
</nav>
<div class="bottomline">
<div class="lang">
<select class="lang" onchange="window.location.href=this[this.selectedIndex].value;"><option selected="selected" value="https://www.beeline-group.com/en/">EN</option><option value="https://www.beeline-group.com/de/">DE</option><option value="https://www.beeline-group.com/fr/">FR</option><option value="https://www.beeline-group.com/nl/">NL</option><option value="https://www.beeline-group.com/es/">ES</option><option value="https://www.beeline-group.com/pt/">PT</option><option value="https://www.beeline-group.com/pl/">PL</option><option value="https://www.beeline-group.com/cs/">CS</option><option value="https://www.beeline-group.com/sk/">SK</option><option value="https://www.beeline-group.com/it/">IT</option><option value="https://www.beeline-group.com/hu/">HU</option></select>
</div>
<p class="copyright">Copyright © 2021 Beeline GmbH. All Rights Reserved.</p>
<nav class="nav-service">
<ul><li><a href="/en/sitemap/">Sitemap</a></li><li><a href="/en/imprint/">Imprint</a></li><li><a href="/en/privacy-policy/">Privacy Policy</a></li><li><a href="/en/privacy-notice-for-contractual-partners/">Privacy Notice for Contractual Partners</a></li></ul>
</nav>
</div>
</div>
</footer>
<div id="q4u_cookie_consent" style="display: none">
<div class="q4u_cookie_consent__layer">
<h2>Cookie settings</h2>
<p>We would like to use Cookies and analytics technologies on this page in order to understand the preferences and interests of our site users better. For instance, we identify which pages and what content is of particular interest and which browser settings you use in order to display appropriate offers for you. This also includes the Google analytics service. Data (e.g. IP-addresses) are transferred to the servers of third party providers, which process the information, in part for their own purposes. We do not know exactly what the processing entails.</p>
<p>For this reason, we only implement this technology with your prior consent. When you click on the “Accept” button, you agree to this.</p>
<div class="q4u_cookie_consent__settings">
<div class="q4u_cookie_consent__settings__cookies" style="display: none">
<div class="q4u_cookie_consent__checkbox">
<input id="q4u_cookie_consent_statistics" name="q4uCookieConsentSatistics" type="checkbox"/>
<label for="q4u_cookie_consent_statistics">Accept</label>
</div>
<div class="q4u_cookie_consent__checkbox">
<input checked="checked" disabled="disabled" id="q4u_cookie_consent_required" name="q4uCookieConsentRequired" type="checkbox"/>
<label for="q4u_cookie_consent_required">Continue without Analytics</label>
</div>
</div>
<div class="q4u_cookie_consent__submit">
<button class="q4u_cookie_consent__submit__button q4u_cookie_consent__submit__button--secondary" id="q4u_cookie_consent_save" type="button">Continue without Analytics</button>
<button class="q4u_cookie_consent__submit__button" id="q4u_cookie_consent_save-all" type="button">Accept</button>
</div>
</div>
<p>You can revoke your consent at any time. Find more information in our Privacy Protection Statement: <a href="/en/privacy-policy/" id="q4u_cookie_consent__privacy-policy-link">/en/privacy-policy/</a></p>
</div>
</div>
<script src="/fileadmin/tmpl/dist/js/main.js?1530615165" type="text/javascript"></script>
<script src="/fileadmin/tmpl/dist/js/shariff.js?1530615165" type="text/javascript"></script>
<script src="/typo3conf/ext/q4u_cookie_consent/Resources/Public/JS/cookie-consent.js?1586319488" type="text/javascript"></script>
<script src="/typo3conf/ext/solr/Resources/JavaScript/JQuery/jquery-ui.min.js?1508496843" type="text/javascript"></script>
<script src="/typo3conf/ext/solr/Resources/JavaScript/EidSuggest/suggest.js?1508496843" type="text/javascript"></script>
</body>
</html>