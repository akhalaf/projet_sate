<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "80094",
      cRay: "610e7be5fcf4197f",
      cHash: "8af618b7f85b544",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXJhYmRpY3QuY29tLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "p4Fl2bIJUuTGw4Si0pFp1Wc9iLyKmGS1hxBfoL9hCORyovPGUPaCSCeLiET0QJYySyBChGJItK1ykqKYjadk2yu02MrkG9n3HMBMKJc/1+KTiVQgXCNmHGGDluLjX1Plr4dtlUJgTn8etlgzMNbgTQka2k08ltITCvz+oMfPkP1VXwBe57dSVD4P0uNgmrZEmsFvZmyhoDNsc+LVrX1gKqGU66Exck3Fvb7v6lLm8YJpBCm2JADaCSGk6om9/anoGoZIbfoPlR0QURPrwMQfRBHmYW4/OsoKoAlYOb0IKcr+C/V4A3OQa/PaY6gZaST/ZD6S7smrYVlL0eQIEMkRil7+JqS3KphJFRb4JuLnDhF3SyAy4sq9pKYvVNUYBtFbTXC/KQCESuYhzgbzF/MNCMpra/jORwNPNd1pBhShzSVRdm8NruN8MWcOGrs8ODNL1XjgYmH609UELyCHYaWzN3K4dV0gbrI3iBU+BylirD+YDKCDnF+mXeB/Bp9BtcMNF2jVI3OGcMtPgQhEJ74ZUVyPCfKuHg3g+wAe2xow8GKJ2Eb0yQTpkxuXakVJ5PXPtWSyOnZzdEn8DjdxBSygS2LcvfYqwUn0LI9JT1xvf0qoixUW4WcE2dvPUHbzMAQSubJDr4f4AHQEcjJDt8C5JhGYa/Kuq3NEp9CELmuEUMGdTOHdsOoyrqaxriPSzThBLPtowDnCNNpH14VjIFm9ia6CLxx789BG9PPPPk4K7YyNH8TJJqWTa89TcMzj9Mnu",
        t: "MTYxMDUzNDI2Ny44NTYwMDA=",
        m: "f0Ol/2gbYjbvI6QH82SSzN6pWaswEidHmtcQM6gKU8Q=",
        i1: "CyubgvtLi59vX71Nr3MqgQ==",
        i2: "VloMKpYwi1Qg9xKds7Id2w==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "UHYMRrcC/Fd9rkvLdLqa3ypuBwfhVqoLUXGgCmpxWjk=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.arabdict.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=9d059ec9c6745804ca5b7a1905a31b1e236e9413-1610534267-0-ASdxlwORPYdOP2BKfD3_ndJy_Or34GB6L7KZadpd-lZQGfvVlmyFedP5uCewvR2w2yYlCBbmejiJ4CIN-k7nxXBM3AGIM8NZouapcG8rhEHPvQGs0PAar3kZDPTmUuYrt--gjwhU2FcWxsEsCFRyNTkAX_itV_gskiXZEhb9pOm1cy7F6Y-s6QxaSSA0SL3Zvrbtbm8tSRRR4z2aXNaaPWhDfxJhx1E0Kyupt0XKckMu6k1_kc5rGmesE5UMIj9M9SZ3BxBdP0VDHCEIqJl5z4YiTQh7fct7oEoIdUElDPRyVAxBhtR8CpGBppg6azDrRkzZCYlsHR02L1PbOh-5Ved5gj9gOl0GTjz44CKvzBuOebRAtSF6Vhn0ZuNTEGSamZGxcYt1Nqk1QDFqKxi9S3jzihlPCClTsHp_taPKVWQubVCqfaAMPqo0gDWhnVKIQ7iwgEaI1l0xxow1QEU5hVc0OqodeZW08b2k7KSJVcLko8Xu3vw9Norx7H-vlvr53qaHYgEBq6tESOWIma8ycug" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="52eb50a037fddbdea2729f9234bad679ed174cd2-1610534267-0-ASAp5neOWLmzCiiqtXHQkFStAxRnmFp4CHVu8Ud2HcCrsrChNQLxcdiGZBIzGHnCWJKRgR1wGt6ts1V/21m9+5GBbBuZf9oVUBtfZH+0kOMdZXAta+tn/F+rp8Qs7+Vl/av0KhZMgp8Wq7volie8KubDp1SvAw0gpTiDMJsfD9ZQ8ZEY5hB5F4x0ZHAVoQO9SPupWiu6dPX5skL5B6riv07X3g79YuzmAaejH8tOt46rGZnhA/h7mt/8Ve/gRXgmNWbL9epGJoR+LiBwbk+jlf37V6zNTbrkI4pz+wvrxqe7IQ5nmuw8buAXimqERMUUyRRsjXGSBph9useCyxjS+JpGSvu41rK33Gn9fcRJyeYoZeMGccGdclCuGKuCDPN7jrw58+SttHZDuhpeJSOeFPE23YmU6jyVIhEB4NsfLTlkVT+TbdNZdvd7M+IPj5ON8CVOsquiRHpcxXryKvrjpAGK25QvWI3IXIdCVNWiEzXemcxsM1+JboNdKGhRGkfAoE/R7cwgYjYfjEge+gDFTgbGvPxbMzPL6xm3Q2rQxS1RrStTUj7+bAJvRa8sr5bPgZ7xcj8D7HiLUqcCIDqNVCqIoUs2Tj7Wa2xQsLwnxnEUvFvNGODfPqjBHPom3IHL/kwJhYDM9LOcl5j6nYxIRqNr2CHnSS3Ay2B9f3Q8MkgFcwPTF6vWfDlirqJdyr/HJxJLgLLfYp6Ks16dAn294VZ/ujbjlDzoRRvfJsIswzBpbRFwNyJhEAVAIdfESiLcEkI8tldrs1qRxis1vqvWJsdahIm/1uQBI+MDITnnMFQ3/1G13eMs1eq9v9eIp2ubK9dH/sY5Fld0tJaUO6bn1PEFU/2z6ClrZMwv2EQWKJ/FasKVbGi2tnV6Zw3UAs3n5unMittmZV1S0/UYxmttTwMOWOAvSrHGFKux7f5MfXoheNCcvNHgkLiLolSi3a+RFwOSxjTQ31sijP7VyuffcACkiuV6/0wu7qaKlw5RoZLqNheRfK1dgv5r3DKWvUaiUEB8Oc8xwSogk5qPjWmwlslw0KEQ3kEVKk2QZZwXIRqupGlaQfefFBazRAt413tEMfRDQXosp4IatKY+Gap0/3znf9kK+cQXUjPiX1d/PvY+DgCWkSq2muM0qmVFpVE+Cf0iKkDCZcqtKxs2evB26SQTMns791PLsLuXjkcLR91JWqlnv8aoBA5JcX4UnZrExJxIo3AkFnqahMY9YRBJgOUXigfrTa/z2T90eQVURiltLXcBejZrZq4JXFqXS9SavIGSiVYMthagzQjUqJVhrc6s+2VVPNJzXiG45KGYUYmB"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="5a0cb32921f4e1b208353a69436b254c"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610e7be5fcf4197f')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610e7be5fcf4197f</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
