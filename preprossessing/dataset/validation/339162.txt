<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-gb" xml:lang="en-gb" xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="https://www.coastalcardionj.com/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="index, follow" name="robots"/>
<meta content="cardiovascular, doctors, brick, wall, nj" name="keywords"/>
<meta content="Welcome to Coastal Cardiovascular Consultants, PA" name="title"/>
<meta content="Administrator" name="author"/>
<meta content="Dr Pedro J. Escandon, Dr Thomas M. White, Dr Hojun Yoo, Dr Ahmad Salloum, Dr Virendra Patel - Coastal Cardiovascular Consultants." name="description"/>
<meta content="Joomla! 1.5 - Open Source Content Management" name="generator"/>
<title>Welcome to Coastal Cardiovascular Consultants, PA</title>
<link href="https://www.coastalcardionj.com/plugins/content/attachments.css" rel="stylesheet" type="text/css"/>
<script src="/media/system/js/mootools.js" type="text/javascript"></script>
<script src="/media/system/js/caption.js" type="text/javascript"></script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="/suckerfish/css/menu.css" rel="stylesheet" type="text/css"/>
<link href="/CSS/template.css" rel="stylesheet" type="text/css"/>
<link href="/templates/system/css/editor.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
<!--
/*/this enables transparent PNG files - can delete this if not using /*/
img, div { behavior: url(/iepngfix.htc); }


.chronoform a{
	color: #FFFFFF;
	font-size: 1px;
	
}

#background2-container {
	background-image: url(/images/background2.jpg);
	background-repeat: no-repeat;
	background-position:top;
	height: auto;
	width: 1024px;
	top: 0px;
}
#ap-div1 {
	position: absolute;
	z-index: 888;
	height: 40px;
	width: 800px;
	left: 11px;
	top: -96px;
}
#top-container {
	float: left;
	height: 71px;
	width: 1024px;
}
#left-container {
	float: left;
	height: 256px;
	width: 160px;
	margin: 0px;
	padding: 0px;
}
#middle-container {
	float: left;
	width: 650px;
	height: auto;
	margin: 0px;
	padding: 0px;
	}
#right-container {
	float: left;
	width: 1px;
	margin: 0px;
	padding: 0px;
}
#content-container {
	width: 1024px;
	position: relative;
	margin-left: -512px;;
	padding-top: 0px;
	padding-bottom: 0px;
	left: 50%;
}
#header-container {
	height: 100px;
	width: 100%;
	margin: 0px;
	padding: 0px;
}
#repeater-container {
	
	top: 0px;
	
	background-repeat: repeat-y;
	background-position:top;
	height: 100%;
	margin: 0px;
	padding: 0px;
	
	width: 1024px;
	position: relative;
	margin-left: -512px;
	padding-top: 0px;
	padding-bottom: 0px;
	left: 50%;
	
}
body {
	margin-top: 0px;
	margin-bottom: 0px;
	margin-left: 0px;
	margin-right: 0px;
	background-image: url(/images/background.jpg);
	background-repeat: repeat-x;
	background-color: #FFFFFF;
}
#clearing-div{
    clear: both;
	width: 1024px;
	margin: 0px;
	padding: 0px;
	height: 0;
	display: block;
	line-height: 0px;
	font-size: 0px;}
#footer-container {
	background-image: url(/images/footer.jpg);
	background-position:top;
	height: 196px;
	margin: 0px;
	padding: 0px;
	width:1024px;
	position: relative;
	margin-left: -512px;
	padding-top: 0px;
	padding-bottom: 0px;
	left: 50%;
	visibility:visible;
}
#buttons-container {
	height: 196px;
	width: 1024px;
	margin-right: auto;
	margin-left: auto;
	position: relative;
}
#d-fi-container {
	position: absolute;
	z-index: 10;
	height: 30px;
	width: 289px;
	left: 422px;
	bottom: -40px;
}
.D-Fi-Text {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #333333;
}
#nav-container {
	position: absolute;
	z-index: 9;
	height: 20px;
	width: 1117px;
	left: 43px;
	top: -13px;
}

/*/Traditional vertical menu styling - remove this if using suckerfish nav /*/

 a.mainlevel:link, a.mainlevel:visited {
	display: block;
	background: url(/images/buttons.jpg);
	vertical-align: middle;
	font-size: 11px;
	font-weight: bold;
	color: #ccc;
	text-align: left;
	padding-top: 3px;
	padding-left: 30px;
	height: 22px !important;
	height: 22px;
	width: 100%;
	text-decoration: none;
	}

a.mainlevel:hover {
	background-position: 0px -25px;
	text-decoration: none;
	color: #fff;
}

a.mainlevel#active_menu {
background-position: 0px -50px;
	color:#fff;
	font-weight: bold;
}

a.mainlevel#active_menu:hover {
	color: #fff;
}

a.sublevel:link, a.sublevel:visited {
	padding-left: 1px;
	vertical-align: middle;
	font-size: 11px;
	font-weight: bold;
	color: #c64934;
	text-align: left;
}

a.sublevel:hover {
	color: #900;
	text-decoration: none;
}

a.sublevel#active_menu {
	color: #333;
}
.at_icon { float:left;}
.at_icon  img{border:none;}
/*/ End of traditional vertical menu stylings /*/
-->
html {
overflow-x: hidden;
}
body { behavior: url("csshover3.htc"); } 
span.article_separator{
	display:none;
}
#left-extension {
	background-image: url(/images/footer.jpg);
	position: absolute;
	height: 196px;
	width: 1024px;
	left: -1024px;
	top: 0px;
}
#right-extension {
	background-image: url(/images/footer.jpg);
	position: absolute;
	height: 196px;
	width: 1024px;
	top: 0px;
	z-index: 555;
	left: 1024px;
}
#secondary-nav-container {
	position: absolute;
	z-index: 9;
	height: 31px;
	width: 1009px;
	left: 47px;
	top: 31px;
	padding:0px;
}
#secondary-nav-container div{float:left;
padding:0px; margin:0px;margin-left:20px;}
#secondary-nav-container table{padding:0px; margin:0px;}
tr{padding:0px; margin:0px;height:1px;}
td{padding:0px; margin:0px;height:1px}
/*/Traditional vertical menu styling - remove this if using suckerfish nav /*/

 a.mainlevel:link, a.mainlevel:visited {
	display: none;
	
	
	font-size: 0px;
line-height:0px;
	color: #ccc;
	text-align: left;
	padding-top: 0px;
	padding-left: 0px;
	height: 0px !important;
	height: 0px;
		text-decoration: none;
		visibility:hidden;
		padding:0px;
	
	}

a.mainlevel:hover {
		text-decoration: none;
	color: #fff;
}

a.mainlevel#active_menu {
background-position: 0px -50px;
	color:#fff;
	font-weight: bold;
}

a.mainlevel#active_menu:hover {
	color: #fff;
}

a.sublevel:link, a.sublevel:visited {
	display: inline;
	
	vertical-align: middle;
	font-size: 16px;
	font-weight: normal;
	color: #333;
	text-align: left;
	padding-top: 0px;
	margin-top:0px;
	padding-left: 0px;
	padding-right:32px;
	height: 25px !important;
	height: 25px;
	width: auto;
	text-decoration: none;
	}

a.sublevel:hover {
	
	text-decoration: none;
	color: #fff;
}

a.sublevel#active_menu {

	color:#fff;
	font-weight: normal;
}
.at_icon { float:left;}
.at_icon  img{border:none;}
#srs-logo-container {
	position: absolute;
	z-index: 9999;
	height: 74px;
	width: 131px;
	left: -88px;
	top: 62px;
}
#patient-portal {
	position: absolute;
	z-index: 9999;
	height: 74px;
	width: 131px;
	left: 980px;
	top: 62px;
}
	<p>a.sublevel:link, a.sublevel:visited {padding-right:10px !important;} a.sublevel:link, a.sublevel:visited{font-size:14px !important;font-weight:bold !important;}#secondary-nav-container div{margin-left:10px !important;}#secondary-nav-container{padding-top:3px;}</p>
/*/ End of traditional vertical menu stylings /*/
-->
</style>
<script type="text/javascript">
/*Can remove all this javascript below if no need for slide show /*/
/***********************************************
* Ultimate Fade-In Slideshow (v1.51): © Dynamic Drive (http://www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit http://www.dynamicdrive.com/ for this script and 100s more.
***********************************************/
 
var fadeimages=new Array()
//SET IMAGE PATHS. Extend or contract array as needed
fadeimages[0]=["/images/image1.jpg", "changethislink.htm", ""] 
fadeimages[1]=["/images/image2.jpg", "changethislink.htm", ""] 
 fadeimages[2]=["/images/image3.jpg", "changethislink.htm", ""]
     var fadeimages2=new Array() //2nd array set example. Remove or add more sets as needed.
//SET IMAGE PATHS. Extend or contract array as needed
fadeimages2[0]=["photo1.jpg", "", ""] //plain image syntax
fadeimages2[1]=["photo2.jpg", "http://www.cssdrive.com", ""] //image with link syntax
fadeimages2[2]=["photo3.jpg", "http://www.javascriptkit.com", "_new"] //image with link and target syntax
 
var fadebgcolor="white"

////NO need to edit beyond here/////////////
 
var fadearray=new Array() //array to cache fadeshow instances
var fadeclear=new Array() //array to cache corresponding clearinterval pointers
 
var dom=(document.getElementById) //modern dom browsers
var iebrowser=document.all
 
function fadeshow(theimages, fadewidth, fadeheight, borderwidth, delay, pause, displayorder){
this.pausecheck=pause
this.mouseovercheck=0
this.delay=delay
this.degree=10 //initial opacity degree (10%)
this.curimageindex=0
this.nextimageindex=1
fadearray[fadearray.length]=this
this.slideshowid=fadearray.length-1
this.canvasbase="canvas"+this.slideshowid
this.curcanvas=this.canvasbase+"_0"
if (typeof displayorder!="undefined")
theimages.sort(function() {return 0.5 - Math.random();}) //thanks to Mike (aka Mwinter) :)
this.theimages=theimages
this.imageborder=parseInt(borderwidth)
this.postimages=new Array() //preload images
for (p=0;p<theimages.length;p++){
this.postimages[p]=new Image()
this.postimages[p].src=theimages[p][0]
}
 
var fadewidth=fadewidth+this.imageborder*2
var fadeheight=fadeheight+this.imageborder*2
 
if (iebrowser&&dom||dom) //if IE5+ or modern browsers (ie: Firefox)
document.write('<div id="master'+this.slideshowid+'" style="position:relative;width:'+fadewidth+'px;height:'+fadeheight+'px;overflow:hidden;"><div id="'+this.canvasbase+'_0" style="position:absolute;width:'+fadewidth+'px;height:'+fadeheight+'px;top:0;left:0;filter:progid:DXImageTransform.Microsoft.alpha(opacity=10);opacity:0.1;-moz-opacity:0.1;-khtml-opacity:0.1;background-color:'+fadebgcolor+'"></div><div id="'+this.canvasbase+'_1" style="position:absolute;width:'+fadewidth+'px;height:'+fadeheight+'px;top:0;left:0;filter:progid:DXImageTransform.Microsoft.alpha(opacity=10);opacity:0.1;-moz-opacity:0.1;-khtml-opacity:0.1;background-color:'+fadebgcolor+'"></div></div>')
else
document.write('<div><img name="defaultslide'+this.slideshowid+'" src="'+this.postimages[0].src+'"></div>')
 
if (iebrowser&&dom||dom) //if IE5+ or modern browsers such as Firefox
this.startit()
else{
this.curimageindex++
setInterval("fadearray["+this.slideshowid+"].rotateimage()", this.delay)
}
}

function fadepic(obj){
if (obj.degree<100){
obj.degree+=10
if (obj.tempobj.filters&&obj.tempobj.filters[0]){
if (typeof obj.tempobj.filters[0].opacity=="number") //if IE6+
obj.tempobj.filters[0].opacity=obj.degree
else //else if IE5.5-
obj.tempobj.style.filter="alpha(opacity="+obj.degree+")"
}
else if (obj.tempobj.style.MozOpacity)
obj.tempobj.style.MozOpacity=obj.degree/101
else if (obj.tempobj.style.KhtmlOpacity)
obj.tempobj.style.KhtmlOpacity=obj.degree/100
else if (obj.tempobj.style.opacity&&!obj.tempobj.filters)
obj.tempobj.style.opacity=obj.degree/101
}
else{
clearInterval(fadeclear[obj.slideshowid])
obj.nextcanvas=(obj.curcanvas==obj.canvasbase+"_0")? obj.canvasbase+"_0" : obj.canvasbase+"_1"
obj.tempobj=iebrowser? iebrowser[obj.nextcanvas] : document.getElementById(obj.nextcanvas)
obj.populateslide(obj.tempobj, obj.nextimageindex)
obj.nextimageindex=(obj.nextimageindex<obj.postimages.length-1)? obj.nextimageindex+1 : 0
setTimeout("fadearray["+obj.slideshowid+"].rotateimage()", obj.delay)
}
}
 
fadeshow.prototype.populateslide=function(picobj, picindex){
var slideHTML=""
if (this.theimages[picindex][1]!="") //if associated link exists for image
slideHTML='<a href="'+this.theimages[picindex][1]+'" target="'+this.theimages[picindex][2]+'">'
slideHTML+='<img src="'+this.postimages[picindex].src+'" border="'+this.imageborder+'px">'
if (this.theimages[picindex][1]!="") //if associated link exists for image
slideHTML+='</a>'
picobj.innerHTML=slideHTML
}
 
 
fadeshow.prototype.rotateimage=function(){
if (this.pausecheck==1) //if pause onMouseover enabled, cache object
var cacheobj=this
if (this.mouseovercheck==1)
setTimeout(function(){cacheobj.rotateimage()}, 100)
else if (iebrowser&&dom||dom){
this.resetit()
var crossobj=this.tempobj=iebrowser? iebrowser[this.curcanvas] : document.getElementById(this.curcanvas)
crossobj.style.zIndex++
fadeclear[this.slideshowid]=setInterval("fadepic(fadearray["+this.slideshowid+"])",50)
this.curcanvas=(this.curcanvas==this.canvasbase+"_0")? this.canvasbase+"_1" : this.canvasbase+"_0"
}
else{
var ns4imgobj=document.images['defaultslide'+this.slideshowid]
ns4imgobj.src=this.postimages[this.curimageindex].src
}
this.curimageindex=(this.curimageindex<this.postimages.length-1)? this.curimageindex+1 : 0
}
 
fadeshow.prototype.resetit=function(){
this.degree=10
var crossobj=iebrowser? iebrowser[this.curcanvas] : document.getElementById(this.curcanvas)
if (crossobj.filters&&crossobj.filters[0]){
if (typeof crossobj.filters[0].opacity=="number") //if IE6+
crossobj.filters(0).opacity=this.degree
else //else if IE5.5-
crossobj.style.filter="alpha(opacity="+this.degree+")"
}
else if (crossobj.style.MozOpacity)
crossobj.style.MozOpacity=this.degree/101
else if (crossobj.style.KhtmlOpacity)
crossobj.style.KhtmlOpacity=this.degree/100
else if (crossobj.style.opacity&&!crossobj.filters)
crossobj.style.opacity=this.degree/101
}
 
 
fadeshow.prototype.startit=function(){
var crossobj=iebrowser? iebrowser[this.curcanvas] : document.getElementById(this.curcanvas)
this.populateslide(crossobj, this.curimageindex)
if (this.pausecheck==1){ //IF SLIDESHOW SHOULD PAUSE ONMOUSEOVER
var cacheobj=this
var crossobjcontainer=iebrowser? iebrowser["master"+this.slideshowid] : document.getElementById("master"+this.slideshowid)
crossobjcontainer.onmouseover=function(){cacheobj.mouseovercheck=1}
crossobjcontainer.onmouseout=function(){cacheobj.mouseovercheck=0}
}
this.rotateimage()
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
</head>
<body onload="MM_preloadImages('images/3-buttons-hover_01.jpg','images/3-buttons-hover_02.jpg','images/3-buttons-hover_03.jpg')">
<div id="repeater-container">
<div id="background2-container">
<div id="header-container">
</div>
<div id="content-container">
<div id="top-container"> </div>
<div id="left-container"></div>
<div id="middle-container">
<table class="contentpaneopen">
<tr>
<td class="contentheading" width="100%">
					Welcome to Coastal Cardiovascular Consultants, PA			</td>
</tr>
</table>
<table class="contentpaneopen">
<tr>
<td valign="top">
<p>Our doctors and staff recognize the need for high quality cardiac care without sacrificing personalized service. In order to provide individualized attention we have made the difficult decision to limit the number of insurance plans we accept. It is our goal to see patients on-time and give them the attention they deserve. <br/><br/>Your call to our office during business hours will be answered by a live person. Regardless of whether you have a health concern, billing question, or scheduling issue, our knowledgeable and experienced staff will address your concerns and answer your questions quickly and efficiently. <br/><br/>As a patient at Coastal Cardiovascular Consultants, PA you will receive excellent, up-to-date cardiac care while being treated with dignity and respect.<br/><br/>For a copy of the Department of Health and Human Services Notice of Non-discrimination <a href="/20161027113149.pdf" style="text-decoration:none;color:#000;" target="_blank">click here</a>.</p> </td>
</tr>
</table>
<span class="article_separator"> </span>
</div>
<div id="clearing-div"></div>
<div id="nav-container"><script language="JavaScript" src="https://www.coastalcardionj.com/templates/myhomepage/../../suckerfish/js/menu.js" type="text/javascript"></script>
<div class="menu-son-of-suckerfish-horizontal">
<ul class="mainlevel-son-of-suckerfish-horizontal" id="menulist_root-son-of-suckerfish-horizontal"><li><a class="mainlevel_current-son-of-suckerfish-horizontal" href="/home" id="active_menu-son-of-suckerfish-horizontal"><span class="expanded">Home</span></a><ul id="menulist_1-son-of-suckerfish-horizontal"><li><a class="sublevel-son-of-suckerfish-horizontal" href="/home/dr-pedro-j-escandon">Dr Pedro J. Escandon</a></li><li><a class="sublevel-son-of-suckerfish-horizontal" href="/home/dr-thomas-m-white">Dr Thomas M. White</a></li><li><a class="sublevel-son-of-suckerfish-horizontal" href="/home/dr-hojun-yoo">Dr Hojun Yoo</a></li><li><a class="sublevel-son-of-suckerfish-horizontal" href="/home/dr-ahmad-salloum">Dr Ahmad Salloum</a></li><li><a class="sublevel-son-of-suckerfish-horizontal" href="/home/dr-virendra-patel">Dr Virendra Patel</a></li><li><a class="sublevel-son-of-suckerfish-horizontal" href="/home/dr-harshil-b-patel">Dr. Harshil B. Patel</a></li></ul></li><li><a class="mainlevel-son-of-suckerfish-horizontal" href="/physicians"><span class="expanded">Physicians</span></a><ul id="menulist_2-son-of-suckerfish-horizontal"><li><a class="sublevel-son-of-suckerfish-horizontal" href="/physicians/dr-pedro-j-escandon">Dr Pedro J. Escandon</a></li><li><a class="sublevel-son-of-suckerfish-horizontal" href="/physicians/dr-thomas-m-white">Dr Thomas M. White</a></li><li><a class="sublevel-son-of-suckerfish-horizontal" href="/physicians/dr-hojun-yoo">Dr Hojun Yoo</a></li><li><a class="sublevel-son-of-suckerfish-horizontal" href="/physicians/dr-ahmad-salloum">Dr Ahmad Salloum</a></li><li><a class="sublevel-son-of-suckerfish-horizontal" href="/physicians/dr-virendra-patel">Dr Virendra Patel</a></li><li><a class="sublevel-son-of-suckerfish-horizontal" href="/physicians/dr-harshil-b-patel">Dr. Harshil B. Patel</a></li></ul></li><li><a class="mainlevel-son-of-suckerfish-horizontal" href="/our-locations"><span class="expanded">Our Locations</span></a><ul id="menulist_3-son-of-suckerfish-horizontal"><li><a class="sublevel-son-of-suckerfish-horizontal" href="/our-locations/brick-office">Brick Office</a></li><li><a class="sublevel-son-of-suckerfish-horizontal" href="/our-locations/wall-office">Wall Office</a></li><li><a class="sublevel-son-of-suckerfish-horizontal" href="/our-locations/ocean-medical-center">Hackensack Meridian Health Ocean Medical Center</a></li><li><a class="sublevel-son-of-suckerfish-horizontal" href="/our-locations/jersey-shore-university-medical-center">Hackensack Meridian Health Jersey Shore University Medical Center</a></li></ul></li><li><a class="mainlevel-son-of-suckerfish-horizontal" href="/our-services">Our Services</a></li><li><a class="mainlevel-son-of-suckerfish-horizontal" href="/our-forms">Our Forms</a></li><li><a class="mainlevel-son-of-suckerfish-horizontal" href="/billing">Billing</a></li></ul>
</div></div>
<div id="secondary-nav-container">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><a class="mainlevel" href="/home" id="active_menu">Home</a>
<div style="padding-left: 4px"><a class="sublevel" href="/home/dr-pedro-j-escandon">Dr Pedro J. Escandon</a></div>
<div style="padding-left: 4px"><a class="sublevel" href="/home/dr-thomas-m-white">Dr Thomas M. White</a></div>
<div style="padding-left: 4px"><a class="sublevel" href="/home/dr-hojun-yoo">Dr Hojun Yoo</a></div>
<div style="padding-left: 4px"><a class="sublevel" href="/home/dr-ahmad-salloum">Dr Ahmad Salloum</a></div>
<div style="padding-left: 4px"><a class="sublevel" href="/home/dr-virendra-patel">Dr Virendra Patel</a></div>
<div style="padding-left: 4px"><a class="sublevel" href="/home/dr-harshil-b-patel">Dr. Harshil B. Patel</a></div>
</td></tr>
<tr><td><a class="mainlevel" href="/physicians">Physicians</a></td></tr>
<tr><td><a class="mainlevel" href="/our-locations">Our Locations</a></td></tr>
<tr><td><a class="mainlevel" href="/our-services">Our Services</a></td></tr>
<tr><td><a class="mainlevel" href="/our-forms">Our Forms</a></td></tr>
<tr><td><a class="mainlevel" href="/billing">Billing</a></td></tr>
</table></div>
<div id="ap-div1">
</div>
</div>
</div>
</div>
<div id="footer-container">
<div id="left-extension"></div>
<div id="buttons-container"><a href="/our-forms"><img border="0" height="196" id="Image1" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/3-buttons-hover_01.jpg',1)" src="/images/3-buttons_01.jpg" width="349"/></a><a href="/our-locations"><img border="0" height="196" id="Image2" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','images/3-buttons-hover_02.jpg',1)" src="/images/3-buttons_02.jpg" width="325"/></a><a href="/physicians"><img border="0" height="196" id="Image3" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image3','','images/3-buttons-hover_03.jpg',1)" src="/images/3-buttons_03.jpg" width="350"/></a></div>
<div class="D-Fi-Text" id="d-fi-container"><a href="http://www.dfinj.com" style="color:#999;" target="_blank">Web Design NJ | D-Fi Productions</a></div>
<div id="srs-logo-container"><a href="http://srssoft.com" target="_blank"><img border="0" height="74" src="/images/SRS-logo.jpg" width="131"/></a></div>
<div id="right-extension"></div>
</div>
</body>
</html>
