<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 no-js" lang="en-US"> <![endif]--><!--[if IE 7 ]>    <html class="ie ie7 no-js" lang="en-US"> <![endif]--><!--[if IE 8 ]>    <html class="ie ie8 no-js" lang="en-US"> <![endif]--><!--[if IE 9 ]>    <html class="ie ie9 no-js" lang="en-US"> <![endif]--><!--[if gt IE 9]><!--><html class="no-js" lang="en-US"><!--<![endif]-->
<head id="https://www.accupac.com" profile="http://gmpg.org/xfn/11">
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<link href="https://www.accupac.com/wp-content/upksa/yscom2/Login/%09%0A" hreflang="en-us" rel="alternate"/>
<title></title>
<meta content="We are Makers" name="description"/>
<meta content="" name="keywords"/>
<meta content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" name="viewport"/>
<link href="https://www.accupac.com/wp-content/themes/accupac/images/icons/favicon-228.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://www.accupac.com/wp-content/themes/accupac/images/icons/favicon-195.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="https://www.accupac.com/wp-content/themes/accupac/images/icons/favicon-144.png" rel="apple-touch-icon"/>
<link href="https://www.accupac.com/wp-content/themes/accupac/images/icons/favicon-128.png" rel="apple-touch-icon"/>
<link href="https://www.accupac.com/wp-content/themes/accupac/images/icons/favicon-120.png" rel="shortcut icon"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.accupac.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.accupac.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.accupac.com/wp-json/" rel="https://api.w.org/"/><link href="https://www.accupac.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.accupac.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><link href="https://www.accupac.com/wp-content/uploads/2019/02/cropped-favicon-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://www.accupac.com/wp-content/uploads/2019/02/cropped-favicon-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://www.accupac.com/wp-content/uploads/2019/02/cropped-favicon-180x180.png" rel="apple-touch-icon"/>
<meta content="https://www.accupac.com/wp-content/uploads/2019/02/cropped-favicon-270x270.png" name="msapplication-TileImage"/>
<link href="https://www.accupac.com/wp-content/themes/accupac/style.css?version=4102" rel="stylesheet" type="text/css"/>
<link href="https://www.accupac.com/wp-content/themes/accupac/css/main.css?version=4629" rel="stylesheet" type="text/css"/>
<link crossorigin="anonymous" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" rel="stylesheet"/>
<script src="https://www.accupac.com/wp-content/themes/accupac/js/modernizr.custom.js" type="text/javascript"></script>
<script crossorigin="anonymous" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<meta content="https://www.accupac.com/wp-content/themes/accupac/images/thumbnail-facebook.jpg" property="og:image"/>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W2V3J8K');</script>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=AW-1067952414"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-1067952414');
</script>
</head>
<body class="error">
<section class="marquee--full">
<div class="vertical-align content">
<div class="vertical-align--content">
<div class="container">
<div class="row">
<div class="col-12 col-sm-8 order-2 order-sm-1 color--white">
<div class="vertical-align content">
<div class="vertical-align--content">
<h2 class="default-margin--bottom color--white">
Looks like you could use some help
</h2>
<p>
We understand when things don’t go as planned. In fact, we have problem solving in our veins.
</p>
<p>
If you were looking for a page, it might no longer exist, but you can <a class="color--white" href="https://www.accupac.com">check out the rest of our website here</a>.
</p>
<p>
If you’re interested in how our team of chemists, engineers, and project managers can help solve your manufacturing challenges, <a class="color--white" href="https://www.accupac.com/contact/">talk to us</a>.
</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
<script src="https://www.accupac.com/wp-content/themes/accupac/js/displayMarquee.js" type="text/javascript"></script>
<script src="https://www.accupac.com/wp-content/themes/accupac/js/equalHeight.js" type="text/javascript"></script>
<script src="https://www.accupac.com/wp-content/themes/accupac/js/floatingLabels.js" type="text/javascript"></script>
<script src="https://www.accupac.com/wp-content/themes/accupac/js/parallax.js" type="text/javascript"></script>
<script src="https://www.accupac.com/wp-content/themes/accupac/js/tagManager.js" type="text/javascript"></script>
<script src="https://www.accupac.com/wp-content/themes/accupac/js/targetBlank.js" type="text/javascript"></script>
<script src="https://www.accupac.com/wp-content/themes/accupac/js/mousewheel.js" type="text/javascript"></script>
<script src="https://www.accupac.com/wp-content/themes/accupac/js/toggleItems.js" type="text/javascript"></script>
<script>
	var $animation_elements = $('.appear');
	var $window = $(window);
	function check_if_in_view() {
		var window_height = ( $window.height() / 1.2 );
		//var window_height = $window.height();
		var window_top_position = $window.scrollTop();
		var window_bottom_position = (window_top_position + window_height);

		$.each($animation_elements, function() {
    	  	var $element = $(this);
    	    var element_height = $element.outerHeight();
    	    var element_top_position = $element.offset().top;
    	    var element_bottom_position = (element_top_position + element_height);
    	    if ((element_bottom_position >= window_top_position) && (element_top_position <= window_bottom_position)) {
    	    	$element.addClass('active');
    	    }
		});
	}
	$window.on('scroll resize', check_if_in_view);
	$window.trigger('scroll');
</script>
<script>
    setInterval(function(){
        var slider = $(".slider");
        var active = slider.find(".current");
        var sliderCount = slider.find("div").length;
        var index = active.index();
        active.removeClass("current");
        if (index < sliderCount - 1){
            active.next().addClass("current")
        } else {
            slider.find("div:first").addClass("current")
        }
    }, 10000);
</script>
<script>
    var sliderWidth = $('.slider--container').width();
    var docWidth = $(window).width();
    var mouseX = 0;
    var mouseXPercentage = 0;
    var translateValue = 0;
    $(document.body).mousemove(function(event) {
        mouseX = event.clientX;
        mouseXPercentage = mouseX / docWidth;
        translateValue = (sliderWidth - docWidth) * mouseXPercentage;
        $('div#slider').css('transform', 'translate\(-' + translateValue + 'px' + '\, 0px\)');
    });
</script>
<script>
    $(".openForm").on("click", function() {
        $(".form-modal").addClass("active");
    });
    $(".closeForm").on("click", function() {
        $(".form-modal").removeClass("active");
    });
</script>
<script>
    $(document).ready(function(){
        var getMax = function(){
            return $(document).height() - $(window).height();
        }
        var getValue = function(){
            return $(window).scrollTop();
        }
        if('max' in document.createElement('progress')){
            // Browser supports progress element
            var progressBar = $('progress');
            // Set the Max attr for the first time
            progressBar.attr({ max: getMax() });
            $(document).on('scroll', function(){
                // On scroll only Value attr needs to be calculated
                progressBar.attr({ value: getValue() });
            });
            $(window).resize(function(){
                // On resize, both Max/Value attr needs to be calculated
                progressBar.attr({ max: getMax(), value: getValue() });
            });
        }
        else {
            var progressBar = $('.progress-bar'),
                max = getMax(),
                value, width;
            var getWidth = function(){
                // Calculate width in percentage
                value = getValue();
                width = (value/max) * 100;
                width = width + '%';
                return width;
            }
            var setWidth = function(){
                progressBar.css({ width: getWidth() });
            }
            $(document).on('scroll', setWidth);
            $(window).on('resize', function(){
                // Need to reset the Max attr
                max = getMax();
                setWidth();
            });
        }
    });
</script>
<script>
    // parses query string to assign as variables
     {
        var query = window.location.search.substring(1);
        var tempQuery = query.replace(/%20/g," ");
        //var vars = query.split("&");
        var vars = tempQuery.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            if(pair[0] == variable){
                return pair[1];
            }
        }
        return(false);
    }
</script>
</body>
</html>