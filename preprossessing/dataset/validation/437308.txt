<!DOCTYPE html>
<html lang="en">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-177770195-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-177770195-2');
</script>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Dewan Public School International | Best CBSE School in Meerut</title>
<meta content="Dewan Public School International in Meerut- Top CBSE Affiliated School in Meerut, Uttar Pradesh, India. DPSI is committed in its student’s constructive learning." name="description"/>
<meta content="dewan public school international, dewan public school international Meerut, Schools in Meerut, Best schools in Meerut, Best CBSE Schools in Meerut, Meerut CBSE Schools, cbse schools in Meerut, Top schools in Meerut, Primary School In Meerut, International Schools In Meerut, top cbse schools in india, Best English Medium Schools In Meerut, Meerut schools list" name="keyword"/>
<meta content="FcHp-Ww-7XbJ2WZ_LlXSDyE_GsWd0etZiJFjo2Fx15s" name="google-site-verification"/>
<!-- External CSS -->
<link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
<link href="assets/css/font-awesome.min.css" rel="stylesheet"/>
<link href="assets/css/owl.carousel.css" rel="stylesheet"/>
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet"/>
<link href="css/scheme/primary.css" rel="stylesheet"/>
<link href="css/responsive.css" rel="stylesheet"/>
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700%7CRoboto+Slab:400,700%7CPacifico" rel="stylesheet"/>
<!-- Favicon -->
<link href="images/favicon.png" rel="icon"/>
<link href="images/apple-touch-icon.png" rel="apple-touch-icon"/>
<link href="images/icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="images/icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="https://www.dpsi.edu.in/" rel="canonical"/>
<!--[if lt IE 9]>
<script src="assets/js/html5shiv.min.js"></script>
<script src="assets/js/respond.min.js"></script>
<![endif]-->
</head>
<body>
<!-- Topbar -->
<header class="elh-topbar" id="top"></header>
<!-- Topbar End -->
<!-- Navigation -->
<nav class="navbar affix-top" data-offset-top="60" data-spy="affix" id="menu"></nav>
<!-- Navigation End -->
<!-- Banner -->
<div class="elh-banner style-3" id="elh-banner">
<div class="owl-carousel" id="banner-slider">
<div class="elh-banner-item banner-item-1"></div>
<div class="elh-banner-item banner-item-2"></div>
<div class="elh-banner-item banner-item-3"></div>
<div class="elh-banner-item banner-item-4"></div>
<div class="elh-banner-item banner-item-5"></div>
<div class="elh-banner-item banner-item-6"></div>
<div class="elh-banner-item banner-item-7"></div>
</div>
</div>
<!-- Banner End -->
<!-- Main wrapper start -->
<div class="elh-main-wrap">
<!-- Registration -->
<div class="elh-section-padding-vsmall elh-bottom-0">
<div class="container">
<div class="row">
<div class="col-md-8">
<div class="elh-posts-listing">
<div class="col-md-4 col-xs-12">
<article class="elh-post style-3">
<a class="elh-post-thumb" href="newsletter.php">
<img alt="..." class="img-responsive" src="images/home/1.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="newsletter.php">Newsletter</a></h4>
</div>
</article>
</div>
<div class="col-md-4 col-xs-12">
<article class="elh-post style-3">
<a class="elh-post-thumb" href="games-sports.php"><img alt="..." class="img-responsive" src="images/home/4.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="games-sports.php">Games &amp; Sports</a></h4>
</div>
</article>
</div>
<div class="col-md-4 col-xs-12">
<article class="elh-post style-3">
<a class="elh-post-thumb" href="school-calender.php"><img alt="..." class="img-responsive" src="images/home/3.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="school-calender.php">School/Activity
Calendar</a></h4>
</div>
</article>
</div>
<div class="col-md-4 col-xs-12">
<article class="elh-post style-3">
<a class="elh-post-thumb" href="glittering-stars.php"><img alt="..." class="img-responsive" src="images/home/5.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="glittering-stars.php">Glittering Stars</a></h4>
</div>
</article>
</div>
<div class="col-md-4 col-xs-12">
<article class="elh-post style-3">
<a class="elh-post-thumb" href="events.php"><img alt="..." class="img-responsive" src="images/home/6.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="events.php">Events</a></h4>
</div>
</article>
</div>
<div class="col-md-4 col-xs-12">
<article class="elh-post style-3">
<a class="elh-post-thumb" href="downloads/school-details.pdf" target="_blank"><img alt="..." class="img-responsive" src="images/home/7.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="downloads/school-details.pdf" target="_blank">School Details</a></h4>
</div>
</article>
</div>
<div class="col-md-4 col-xs-12">
<article class="elh-post style-3">
<a class="elh-post-thumb" href="laboratories.php"><img alt="..." class="img-responsive" src="images/home/10.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="laboratories.php">Laboratories</a></h4>
</div>
</article>
</div>
<div class="col-md-4 col-xs-12">
<article class="elh-post style-3">
<a class="elh-post-thumb" href="downloads.php"><img alt="..." class="img-responsive" src="images/home/8.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="downloads.php">Downloads</a></h4>
</div>
</article>
</div>
<div class="col-md-4 col-xs-12">
<article class="elh-post style-3">
<a class="elh-post-thumb" href="career.php"><img alt="..." class="img-responsive" src="images/home/9.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="career.php">Career</a></h4>
</div>
</article>
</div>
</div>
</div>
<div class="col-md-4">
<div class="sidebar" id="rmenu"></div>
</div>
</div>
</div>
</div>
<!-- Registration End -->
<!-- Mini Sections -->
<div class="elh-section elh-img-bg elh-bg-4">
<div class="elh-overlay elh-section-padding-vsmall">
<div class="container">
<div class="row">
<div class="col-md-4">
<div class="elh-mini-section">
<h3 class="elh-mini-section-title">Notice <span>&amp; News</span></h3>
<div class="elh-single-carousel owl-carousel">
<div class="elh-events elh-testimonial-item">
<div class="elh-event">
<div class="elh-event-date">
<span class="elh-event-day">02</span>
<span class="elh-event-month">Apr</span>
</div>
<div class="elh-event-detail">
<h4 class="elh-event-title"><a href="notice-news.php">Online classes for the session 2020-21</a>
</h4>
<p class="elh-event-metas">Dear Students,
Please visit the downloads page to download the study material and exercises. To ask any query contact to the concern teacher mentioned in the study material file between 9:00 am and 12 noon (Whats App only).  Be active towards your studies and solve exercises regularly.</p>
</div>
</div>
<a class="elh-mini-all-link" href="notice-news.php">See All News <i class="fa fa-long-arrow-right"></i></a>
</div>
<div class="elh-events elh-testimonial-item">
<div class="elh-event">
<div class="elh-event-date">
<span class="elh-event-day">31</span>
<span class="elh-event-month">Dec</span>
</div>
<div class="elh-event-detail">
<h4 class="elh-event-title"><a href="notice-news.php">For any query</a>
</h4>
<p class="elh-event-metas">For any query related to IT, kindly mail us at techteam.dpsi@gmail.com</p>
</div>
</div>
<a class="elh-mini-all-link" href="notice-news.php">See All News <i class="fa fa-long-arrow-right"></i></a>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="elh-mini-section">
<h3 class="elh-mini-section-title">Photo <span>Galllery</span></h3>
<div class="elh-single-carousel owl-carousel">
<article class="elh-post">
<a class="elh-post-thumb" href="photo-gallery.php"><img alt="..." class="img-responsive" src="images/event/DSCN3082.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="photo-gallery.php">Food Festival<br/> </a></h4>
</div>
</article>
<article class="elh-post">
<a class="elh-post-thumb" href="photo-gallery.php"><img alt="..." class="img-responsive" src="images/event/DSCN3183.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="photo-gallery.php">Republic Day<br/> </a></h4>
</div>
</article>
<article class="elh-post">
<a class="elh-post-thumb" href="photo-gallery.php"><img alt="..." class="img-responsive" src="images/event/DSCN2844.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="photo-gallery.php">Special Assembly on Human Right Day<br/> </a></h4>
</div>
</article>
<article class="elh-post">
<a class="elh-post-thumb" href="photo-gallery.php"><img alt="..." class="img-responsive" src="images/event/DSC-0050.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="photo-gallery.php">Annual Day 2018 (Karmchakra)<br/> </a></h4>
</div>
</article>
<article class="elh-post">
<a class="elh-post-thumb" href="photo-gallery.php"><img alt="..." class="img-responsive" src="images/event/IMG20181027093545.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="photo-gallery.php">Cyber Wizard (IT Fest) 2018<br/> </a></h4>
</div>
</article>
<article class="elh-post">
<a class="elh-post-thumb" href="photo-gallery.php"><img alt="..." class="img-responsive" src="images/event/4.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="photo-gallery.php">Independence Day Celebration<br/> </a></h4>
</div>
</article>
<article class="elh-post">
<a class="elh-post-thumb" href="photo-gallery.php"><img alt="..." class="img-responsive" src="images/event/3.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="photo-gallery.php">Investiture Ceremony<br/> </a></h4>
</div>
</article>
<article class="elh-post">
<a class="elh-post-thumb" href="photo-gallery.php"><img alt="..." class="img-responsive" src="images/event/2.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="photo-gallery.php">Unesco Club Inaugration<br/> </a></h4>
</div>
</article>
<article class="elh-post">
<a class="elh-post-thumb" href="photo-gallery.php"><img alt="..." class="img-responsive" src="images/event/1.jpg"/></a>
<div class="elh-post-body">
<h4 class="elh-post-title"><a href="photo-gallery.php">Edu Vision<br/> </a></h4>
</div>
</article>
</div>
<a class="elh-mini-all-link" href="photo-gallery.php">See All Photos <i class="fa fa-long-arrow-right"></i></a>
</div>
</div>
<div class="col-md-4">
<div class="elh-mini-section">
<h3 class="elh-mini-section-title">Login <span>Members</span></h3>
<div class="elh-single-carousel owl-carousel">
<div class="elh-event">
<div class="elh-event-date" style="height:60px; width:60px; padding:0px;"><span class="elh-event-day" style="margin:0px"><img src="images/icons/l1.png"/></span>
</div>
<div class="elh-event-detail" style="margin-top:19px">
<!--h4 class="elh-event-title"><a href="http://econnectk12.jupsoft.com/sisLogin.aspx?id=YNOa/lB22Z8=">Teachers Login</a></h4-->
<h4 class="elh-event-title"><a href="http://econnectk12.jupsoft.com/sisLogin.aspx?id=YNOa/lB22Z8=">Teachers Login</a></h4>
</div>
<div class="clearfix" style="margin-bottom:15px"></div>
<div class="elh-event-date" style="height:60px; width:60px; padding:0px;"><span class="elh-event-day" style="margin:0px"><img src="images/icons/l2.png"/></span>
</div>
<div class="elh-event-detail" style="margin-top:19px">
<!--h4 class="elh-event-title"><a href="http://dpsimrt.scholaranalytics.in/">Parents Login</a></h4-->
<h4 class="elh-event-title"><a href="http://econnectk12.jupsoft.com/sisStudentLogin.aspx?id=0BGz6NW+g14=">Parents Login</a></h4>
</div>
<div class="clearfix" style="margin-bottom:15px"></div>
<div class="elh-event-date" style="height:60px; width:60px; padding:0px;"><span class="elh-event-day" style="margin:0px"><img src="images/icons/l3.png"/></span>
</div>
<div class="elh-event-detail" style="margin-top:19px">
<!--h4 class="elh-event-title"><a href="http://dpsimrt.scholaranalytics.in/">Student Login</a></h4-->
<h4 class="elh-event-title"><a href="http://econnectk12.jupsoft.com/sisStudentLogin.aspx?id=0BGz6NW+g14=">Student Login</a></h4>
</div>
<div class="clearfix" style="margin-bottom:15px"></div>
<div class="elh-event-date" style="height:60px; width:60px; padding:0px;"><span class="elh-event-day" style="margin:0px"><img src="images/icons/l4.png"/></span>
</div>
<div class="elh-event-detail" style="margin-top:19px">
<h4 class="elh-event-title"><a href="http://econnectk12.jupsoft.com/sisLogin.aspx?id=YNOa/lB22Z8=">Admin Login</a></h4>
</div>
</div>
</div>
<a class="elh-mini-all-link" href="http://dpsimrt.scholaranalytics.in/">See Login Members <i class="fa fa-long-arrow-right"></i></a>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Mini Sections End -->
<!-- About Us -->
<div class="elh-section elh-section-padding-vsmall elh-bottom-0" style="padding-bottom:0px">
<div class="container">
<div class="row">
<div class="col-md-7">
<div class="elh-about-section sm-bottom-30">
<div class="elh-section-header text-left">
<h3 class="elh-section-title">Principal <span>Message</span></h3>
</div>
<p style="line-height:24px"><strong>It is truly an honor to serve as Principal of Dewan Public
School International.The role of Principal entails many exciting opportunities
associated with student achievement.<br/>
We at DPSI embrace the belief that ALL students can learn.Our goal is to provide them
the essentials needed for success in a safe and warm environment. Redefining the concept
of education, schools are nowadays, high functioning centers of engagement and learning.
We support implementation of engaging practices that foster each student's unique
potential.</strong></p>
<p>We challenge students to think critically, process information and discuss in a
collaborative, open minded manner, through an environment of constructive learning. Emphasis
is on intellectual, creative, social, emotional, technological and physical skills necessary
to prepare them for life. It is this self reliance which forms the basis of rational
analysis and social relationship. <br/><br/>
<a class="elh-mini-all-link" href="principal-message.php">See Complete Message<i class="fa fa-long-arrow-right"></i></a></p>
</div>
</div>
<div class="col-md-5 hidden-xs"><img class="img-responsive" src="images/principal.png"/></div>
</div>
</div>
</div>
<!-- About Us End -->
</div>
<!-- Main wrapper start end -->
<div class="elh-section">
<div class="container">
<div class="row">
<div class="col-md-offset-1 col-md-2 col-sm-3 col-xs-6"><img alt="..." class="img-responsive" src="images/logo/1.png"/></div>
<div class="col-md-2 col-sm-3 col-xs-6"><img alt="..." class="img-responsive" src="images/logo/2.png"/></div>
<div class="col-md-2 col-sm-3 col-xs-6"><img alt="..." class="img-responsive" src="images/logo/3.png"/></div>
<div class="col-md-2 col-sm-3 col-xs-6"><img alt="..." class="img-responsive" src="images/logo/5.png"/></div>
<div class="col-md-2 col-sm-3 col-xs-6"><img alt="..." class="img-responsive" src="images/logo/6.png"/></div>
</div>
</div>
</div>
<footer id="foot"></footer>
<!-- Script -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/imagesloaded.pkgd.min.js"></script>
<script src="assets/js/visible.js"></script>
<script src="assets/js/isotope.pkgd.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/slick.min.js"></script>
<script src="assets/js/jquery.countTo.js"></script>
<script src="assets/js/jquery.countdown.min.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="js/custom.js"></script>
<script>
$(function () {
$("#top").load("include/top.php");
$("#menu").load("include/menu.php");
$("#rmenu").load("include/rmenu.php");
$("#foot").load("include/foot.php");
});
</script>
</body>
</html>
