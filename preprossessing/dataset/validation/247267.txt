<!DOCTYPE html>
<html>
<head>
<title>Page Not Found | Bold Strokes Books</title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="Bold Strokes Books" name="title"/>
<meta content="Bold Strokes Books is a boutique imprint producing quality fiction across a wide range of genres, including fantasy, historical, horror, mystery, paranormal, science fiction, and spiritual fiction." name="description"/>
<link href="https://www.boldstrokesbooks.com/assets/bsb/images/favicon.jpg?1605106476" rel="shortcut icon" type="image/jpg"/>
<link href="https://www.boldstrokesbooks.com/categories.php" rel="canonical"/>
<meta content="Bold Strokes Books" property="og:site_name"/>
<meta content="https://www.boldstrokesbooks.com/categories.php" property="og:url"/>
<meta content="Bold Strokes Books" property="og:title"/>
<meta content="Bold Strokes Books is a boutique imprint producing quality fiction across a wide range of genres, including fantasy, historical, horror, mystery, paranormal, science fiction, and spiritual fiction." property="og:description"/>
<meta content="https://www.boldstrokesbooks.com/assets/bsb/images/logo_social_sharing.png?1605106476" property="og:image"/>
<meta content="image/jpeg" property="og:image:type"/>
<meta content="398" property="og:image:width"/>
<meta content="208" property="og:image:height"/>
<meta content="summary" name="twitter:card"/>
<meta content="@boldstrokebooks" name="twitter:site"/>
<meta content="Bold Strokes Books" name="twitter:title"/>
<meta content="Bold Strokes Books is a boutique imprint producing quality fiction across a wide range of genres, including fantasy, historical, horror, mystery, paranormal, science fiction, and spiritual fiction." name="twitter:description"/>
<meta content="https://www.boldstrokesbooks.com/assets/bsb/images/logo_social_sharing.png?1605106476" name="twitter:image"/>
<meta content="https://www.boldstrokesbooks.com/categories.php" name="twitter:url"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css"/>
<link href="https://www.boldstrokesbooks.com/assets/bsb/css/app.css?1605106571" rel="stylesheet"/>
<link href="https://www.boldstrokesbooks.com/assets/bsb/stylesheets/frontend.css?1605106438" rel="stylesheet"/>
<!--[if lte IE 8]>
	<script type="text/javascript" src="https://www.boldstrokesbooks.com/assets/js/html5shiv.js"></script>
	<script type="text/javascript" src="https://www.boldstrokesbooks.com/assets/js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-71869762-1', 'auto');ga('require', 'displayfeatures');ga('send', 'pageview');</script>
<header>
<!-- MainNav -->
<nav class="navbar">
<div class="container">
<div class="navbar-header">
<button aria-expanded="false" class="navbar-toggle collapsed" data-target="#header-navbar" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="/"><img alt="" src="https://www.boldstrokesbooks.com/assets/bsb/images/logo.jpg"/></a>
</div>
<div class="collapse navbar-collapse" id="header-navbar">
<ul class="nav navbar-nav navbar-right">
<li><a href="https://www.boldstrokesbooks.com/books/lesbian-fiction-1-c">Lesbian Fiction</a></li>
<li><a href="https://www.boldstrokesbooks.com/books/gbt-fiction-2-c">GBT Fiction</a></li>
<li><a href="https://www.boldstrokesbooks.com/books/young-adult-fiction-3-c">YA Fiction</a></li>
<li><a href="https://www.boldstrokesbooks.com/contact-us">Contact us</a></li>
<li class="hidden-xs navbar-search js-navbar-search">
<a class="navbar-search-button js-navbar-search-button bordered-link" href="#"><i class="icon icon-search"></i></a>
<form accept-charset="UTF-8" action="https://www.boldstrokesbooks.com/books/searchresults" class="navbar-search-form js-navbar-search-form" method="GET">
<input class="form-control" name="q" placeholder="Search for..." type="text"/>
</form>
</li>
<li class="hidden-xs"><a class="bordered-link shopping-cart-link" href="https://www.boldstrokesbooks.com/checkout/step1"><i class="icon icon-cart"></i> <span>(<span class="length">0</span>)</span></a></li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle bordered-link" data-toggle="dropdown" href="#" role="button">
<span class="hidden-inline-block-xxs hidden-inline-block-xs"><i class="icon icon-woman"></i></span>
<span class="visible-inline-block-xxs visible-inline-block-xs">Account</span>
</a>
<ul class="dropdown-menu ">
<li><a data-modal="modalLogin" href="https://www.boldstrokesbooks.com/login" rel="modal">Login</a></li>
<li><a data-modal="modalRegister" href="https://www.boldstrokesbooks.com/login" rel="modal">Register</a></li>
</ul> </li>
</ul>
</div>
</div>
</nav>
<!-- /MainNav -->
<div class="modal-login modal fade" id="modalLogin" role="dialog" tabindex="-1">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-body">
<button class="close" data-dismiss="modal" type="button"><i class="fa fa-times-circle"></i></button>
<div class="page-header">
<div class="heading"> Login </div>
</div>
<form accept-charset="UTF-8" action="https://www.boldstrokesbooks.com/login" id="login-form" method="POST" role="form"><input name="_token" type="hidden" value="damrpAYb32bKgQ3fsJxTsGzz8UlRDW94QkhgY0Wf"/>
<div class="row">
<div class="col-md-7 form-group ">
<label class="control-label required" for="email">E-mail</label>
<input class="form-control" id="email" name="email" type="text"/>
</div>
</div>
<div class="row">
<div class="col-md-7 form-group ">
<label class="control-label required" for="password">Password</label>
<input class="form-control" id="password" name="password" type="password" value=""/>
</div>
<div class="col-md-5">
<p class="form-control-link">
<a href="https://www.boldstrokesbooks.com/login/forgot-password">Forgot your password?</a>
</p>
</div>
</div>
<div class="row">
<div class="col-md-7">
<div class="checkbox">
<label>
<input checked="checked" name="remember" type="checkbox" value="1"/>
				Remember me on this computer
			</label>
</div>
</div>
</div>
<div class="row">
<div class="col-xs-12" style="display: flex; flex-direction: row-reverse; justify-content: space-between; align-items: center; padding-bottom: 15px">
<button class="btn btn-lg btn-secondary g-recaptcha-button" type="submit">Sign in</button>
</div>
<div class="visible-xs col-xs-6">
<a class="btn btn-secondary btn-xs" data-modal="modalRegister" href="https://www.boldstrokesbooks.com/login" rel="modal">Register</a>
</div>
</div>
</form> </div>
</div>
</div>
</div>
<div class="modal-login modal fade" id="modalRegister" role="dialog" tabindex="-1">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-body">
<button class="close" data-dismiss="modal" type="button"><i class="fa fa-times-circle"></i></button>
<div class="page-header">
<div class="heading"> Register </div>
</div>
<div>
<form accept-charset="UTF-8" action="https://www.boldstrokesbooks.com/login/add-user" id="login-form" method="POST" role="form"><input name="_token" type="hidden" value="damrpAYb32bKgQ3fsJxTsGzz8UlRDW94QkhgY0Wf"/>
<div class="form-group ">
<label class="control-label required" for="email-register">E-mail</label>
<input class="form-control" id="email-register" name="email-register" type="text"/>
</div>
<div class="form-group ">
<label class="control-label required" for="password-register">Password</label>
<input class="form-control" id="password-register" name="password-register" type="password" value=""/>
</div>
<div class="form-group ">
<label class="control-label required" for="repeat-password">Repeat Password</label>
<input class="form-control" id="repeat-password" name="repeat-password" type="password" value=""/>
</div>
<div style="display: flex; flex-direction: row-reverse; justify-content: space-between; align-items: center;">
<button class="g-recaptcha-button btn btn-lg btn-secondary" type="submit">Sign up</button>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
<!-- SearchBar -->
<div class="searchbar visible-xs">
<div class="container">
<div class="searchbar-content">
<form accept-charset="UTF-8" action="https://www.boldstrokesbooks.com/books/searchresults" class="searchbar-form" method="GET">
<div class="input-group">
<input class="form-control" name="q" placeholder="Search for..." type="text"/>
<span class="input-group-btn">
<button class="btn" type="button"><i class="fa fa-search"></i></button>
</span>
</div>
<!-- /input-group -->
</form>
<div class="searchbar-links">
<a class="btn btn-bordered shopping-cart-link" href="https://www.boldstrokesbooks.com/checkout/step1"><i class="icon icon-cart-white"></i> <span class="length">(0)</span></a>
</div>
</div>
</div>
</div>
<!-- /SearchBar  --></header> <main>
<!-- Bradcrumb -->
<!-- /Bradcrumb -->
<div class="container">
<section id="static-page">
<div class="error-page">
<div class="row">
<div class="col-xs-12 text-center">
<br/>
<h1>WE ARE SORRY, THE PAGE YOU ARE LOOKING FOR COULD NOT BE FOUND</h1>
<br/>
<div>THE PAGE MAY NOT EXIST ANYMORE, MAY HAVE CHANGED ITS NAME,</div>
<div>OR MAY NOT BE AVAILABLE AT THIS TIME.</div>
<br/>
<a class="btn btn-secondary" href="https://www.boldstrokesbooks.com"><i class="fa fa-arrow-left"></i> Back to Homepage</a>
</div>
</div>
</div>
</section>
</div>
</main>
<footer>
<!-- TopFooter -->
<div class="top-footer hidden-xs">
<div class="container">
<div class="row">
<div class="col-sm-2">
<ul>
<li><a href="https://www.boldstrokesbooks.com/books/lesbian-fiction-1-c">Lesbian Fiction</a></li>
<li><a href="https://www.boldstrokesbooks.com/books/gbt-fiction-2-c">GBT Fiction</a></li>
<li><a href="https://www.boldstrokesbooks.com/books/young-adult-fiction-3-c">YA Fiction</a></li>
</ul>
</div>
<div class="col-sm-2">
<ul>
<li><a href="https://www.boldstrokesbooks.com/authors">Authors</a></li>
<li><a href="https://www.boldstrokesbooks.com/blog">Blog</a></li>
<li><a href="https://www.boldstrokesbooks.com/events">Calendar</a></li>
</ul>
</div>
<div class="col-sm-2">
<ul>
<li><a href="https://www.boldstrokesbooks.com/checkout/step1">Cart</a></li>
<li><a href="https://www.boldstrokesbooks.com/profile">Account</a></li>
<li><a href="https://www.boldstrokesbooks.com/gift-cards">Gift cards</a></li>
</ul>
</div>
<div class="col-sm-3">
<ul>
<li><a href="https://www.boldstrokesbooks.com/the-bold-strokes-team">The Bold Strokes Team</a></li>
<li><a href="https://www.boldstrokesbooks.com/submissions">Submissions</a></li>
<li><a href="https://www.boldstrokesbooks.com/returns-policy">Returns Policy</a></li>
</ul>
</div>
<div class="col-sm-3">
<div class="publish-with">
<div class="logo">Publish with <i class="icon icon-bsb-footer"></i></div>
<a href="https://www.boldstrokesbooks.com/publish-with-bsb">Learn More</a>
</div>
</div>
</div>
</div>
</div>
<!-- /TopFooter -->
<!-- CustomeService -->
<div class="cust-service visible-xs">
<div class="container">
<nav class="navbar">
<div class="navbar-header">
<a aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar-cust-service" data-toggle="collapse" href="javascript:void(0)">
                        Customer Service
                        <span class="status-icon">
<i class="fa fa-plus closed"></i>
<i class="fa fa-minus opened"></i>
</span>
</a>
</div>
<div class="collapse navbar-collapse" id="navbar-cust-service">
<ul class="nav navbar-nav">
<li><a href="https://www.boldstrokesbooks.com/contact-us">Contact Us</a></li>
<li><a href="https://www.boldstrokesbooks.com/the-bold-strokes-team">The Bold Strokes Team</a></li>
<li><a href="https://www.boldstrokesbooks.com/submissions">Submissions</a></li>
<li><a href="https://www.boldstrokesbooks.com/terms-of-use">Terms of Use</a></li>
<li><a href="https://www.boldstrokesbooks.com/privacy-policy">Privacy Policy</a></li>
</ul>
</div>
</nav>
</div>
</div>
<!-- /CustomeService -->
<!-- NewsletterSignup -->
<div class="newsletter-signup visible-xs">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="text-uppercase" style="margin-bottom: 10px;">Subscribe</div>
</div>
</div>
<div class="row">
<div class="col-xs-12">
<form accept-charset="UTF-8" action="https://www.boldstrokesbooks.com/subscribe-newsletter" class="newsletter-signup-form-xs" method="POST"><input name="_token" type="hidden" value="damrpAYb32bKgQ3fsJxTsGzz8UlRDW94QkhgY0Wf"/>
<input class="hidden" name="honey-pot" type="text"/> <div class="form-group">
<input class="form-control" id="subscribeNewsletter" name="emailNewsletter" placeholder="Enter your mail" type="text"/>
</div>
<button class="btn btn-primary btn-block" type="submit">Send</button>
<!-- /input-group -->
</form>
</div>
</div>
</div>
</div>
<!-- NewsletterSignup -->
<!-- SocialNetworks -->
<div class="social-networks visible-xs">
<div class="container">
<div class="title">
<h4>FOLLOW US ON</h4>
</div>
<div class="social-networks-links">
<a class="social-network-link" href="https://twitter.com/boldstrokebooks"><i class="fa fa-twitter"></i></a>
<a class="social-network-link" href="https://www.facebook.com/BoldStrokesBooks"><i class="fa fa-facebook"></i></a>
<a class="social-network-link" href="https://www.instagram.com/boldstrokesbooks/"><i class="fa fa-instagram"></i></a>
<a class="social-network-link" href="https://www.youtube.com/channel/UCLVz6T52keZ1VxoMQJdn-DQ" target="_blank"><i class="fa fa-youtube"></i></a>
</div>
</div>
</div>
<!-- /SocialNetworks -->
<!-- BottomFooter -->
<div class="bottom-footer">
<div class="container">
<div class="copy">
<p>©Copyright 2021 by Bold Strokes Books. All Rights Reserved.</p>
</div>
<ul class="bottom-footer-links hidden-xs">
<li><a href="https://www.boldstrokesbooks.com/terms-of-use">Terms of Use</a></li>
<li><a href="https://www.boldstrokesbooks.com/privacy-policy">Privacy Policy</a></li>
<li><a href="https://www.boldstrokesbooks.com/contact-us">Contact Us</a></li>
</ul>
</div>
</div>
<!-- BottomFooter -->
</footer><script src="https://www.boldstrokesbooks.com/assets/bsb/js/app.js?1605106571" type="text/javascript"></script>
<script type="text/javascript">
    var googleInvisibleRecaptcha = {
        'siteKey': '6Lc6N_cUAAAAAH09Br0zMB7Sotavg0PCfvWrmYur',
        'renderWidget': function (button, onSuccessCallback) {
            var widget = grecaptcha.render(button.getAttribute('id'), {
                'sitekey': this.siteKey,
                'callback': function (token) {
                    if (token) {
                        onSuccessCallback(token);
                    }

                    grecaptcha.reset(widget);
                },
                size: 'invisible',
                badge: 'inline'
            });

            button.onmousedown = function () {
                grecaptcha.execute(widget);
            }
        }
    };

    function onloadCallback()
    {
        var widgetId = 0;

        document.querySelectorAll('.g-recaptcha-button').forEach(function (button) {
            if (! button.getAttribute('id')) {
                button.setAttribute('id', 'g-recaptcha-widget-' + (++widgetId));
            }

            googleInvisibleRecaptcha.renderWidget(button, function (token) {
                var form = this.closest('form');
                var recaptchaToken = form.querySelector('[name=g-recaptcha-response]');

                if (! recaptchaToken) {
                    recaptchaToken = document.createElement('input[name=g-recaptcha-response]');
                    recaptchaToken.setAttribute('name', 'g-recaptcha-response');
                    form.appendChild(recaptchaToken);
                }

                recaptchaToken.value = token;
                $(form).trigger('submit');
            }.bind(button));
        });
    }
</script>
<script async="" defer="" src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&amp;render=explicit"></script>
<script type="text/javascript">
            $(function()
            {
                $(document).on('click', '[rel=modal]', function(e)
                {
                    if($(this).data('modal'))
                    {
                        e.preventDefault();
                        $('#'+$(this).data('modal')).modal('show');
                    }
                });
            });
        </script>
</body>
</html>
