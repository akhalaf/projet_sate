<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="de" xml:lang="de" xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="landrover, land rover, land-rover, softtop, rr, lr, 4x4, landy, defender, tdi, td5, discovery, discovery2, discovery3, freelander, rangerover, serie, serie1, serie2, serie3, plattform, Landrover - die Seite für 4X4 Freunde, offroad, geländewagen, 4x4landy, umbauten, technik, foren, forum, portal" name="keywords"/>
<meta content="Landrover - die Seite für 4X4 Freunde. Die private Plattform für alle Landrover Freunde. Landrover News; Termine; Veranstaltungen; Technik; Gallerie; Reise; Forum; Chat u.m." name="description"/>
<meta content="Mathias Luk" name="author"/>
<meta content="Mathias Luk" name="publisher"/>
<meta content="blacklandy.de / Mathias Luk" name="copyright"/>
<meta content="all" name="robots"/>
<meta content="7 days" name="revisit-after"/>
<meta content="de" name="content-language"/>
<meta content="German, Deutsch, de" http-equiv="content-language"/>
<title>Landrover - Die private Plattform für alle Landrover-Freunde.</title>
<link href="/start.css" media="screen" rel="stylesheet" type="text/css"/>
<script charset="UTF-8" src="https://ff.kis.v2.scr.kaspersky-labs.com/2E919D45-F589-3E4E-B7A2-33839E271833/main.js" type="text/javascript"></script></head>
<body>
<div id="farbe1">
<h5>          Die private Plattform zum Thema Landrover...</h5>
</div>
<center>
<div id="container">
<object data="/bllogo.swf" height="101" type="application/x-shockwave-flash" width="280">
<param name="movie" value="/bllogo.swf"/>
<img alt="banner" height="83" src="/logobl1.jpg" width="190"/>
</object>
</div>
</center>
<div id="farbe2">
<h5> ...hier treffen sich 4x4 Freunde.          </h5>
</div>
<center>
<p id="startlink"><a href="https://www.blacklandy.eu/blboard/forum/forum.php" target="blank"><img alt="Weiter zur Startseite" height="30" src="/skip.gif" title="Weiter zur Startseite" width="140"/></a></p>
<a href="https://www.blacklandy.eu/blboard/forum/showthread.php?148383-Datenschutz&amp;p=1515462#post1515462">Datenschutz</a>
<p id="impress"><a href="https://www.blacklandy.eu/blboard/forum/impressum.php" target="blank"><img alt="" height="30" src="/imp.gif" title="impressum" width="140"/></a></p> <br/>
</center>
<div align="center"><p>
</p><table>
<tr>
<td>
<!--
|  chCounter 3.1.3
|  a counter and statistics script written in PHP
|  (c) Christoph Bachner and Bert Koern 2007 - released under the GNU GPL
|  see at [ https://chCounter.org/ ]
-->
<!-- BEGIN chCounter 3.1.3 additional statistics -->
<script type="text/javascript">
// <![CDATA[
document.write("<script type=\"text/javascript\" src=\"https://www.blacklandy.eu/chCounter3.1.1/additional.php?js=true\"><\/script>");
// ]]>
</script>
<!-- END chCounter 3.1.3 additional statistics -->
<table style="width: 300px; border:0px;">
<tr>
<td><font color="#808080"><font face="MS Sans Serif">Besucher gesamt:</font></font></td>
<td style="text-align: right;"><font color="#00753A"><font face="MS Sans Serif">5.225.915</font></font></td>
</tr>
<tr>
<td><font color="#808080"><font face="MS Sans Serif">Besucher heute:</font></font></td>
<td style="text-align: right;"><font color="#808080"><font face="MS Sans Serif">198</font></font></td>
</tr>
<tr>
<td><font color="#808080"><font face="MS Sans Serif">Besucher gestern:</font></font></td>
<td style="text-align: right;"><font color="#808080"><font face="MS Sans Serif">450</font></font></td>
</tr>
<tr>
<td><font color="#808080"><font face="MS Sans Serif">Max. Besucher pro Tag:</font></font></td>
<td style="text-align: right;"><font color="#808080"><font face="MS Sans Serif">4.583</font></font></td>
</tr>
<tr>
<td>
</td>
</tr>
</table>
<script>
function datum(){
tmj=new Array("Januar","Februar","M�rz","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember")
d=new Date(document.lastModified)
m=tmj[d.getMonth()]
t=d.getDate()
jj=d.getYear()
j=(jj>=2000)?jj:((jj<80)?jj+2000:jj+1900)
dat=(t+". "+m+" "+j)}
datum(); document.write("<font color='#000000'>blacklandy ist</font>")
</script>
<script language="JavaScript">
function HowLongSince(startmonth, startdate, startyear) {
sdate=startdate;
smonth=startmonth-1;
syear=startyear;
var DaysInMonth = new Array(31,28,31,30,31,30,31,31,30,31,30,31);
today = new Date()
var thisyear = today.getFullYear();
var thismonth = today.getMonth();
var thisdate = today.getDate();
mstart = new Date(syear,(smonth==12?1:smonth+1),1);
days1 = (mstart - new Date(syear,smonth,sdate))/(24*60*60*1000)-1;
mend = new Date(thisyear,thismonth,1);
days2 = (new Date(thisyear,thismonth,thisdate) - mend)/(24*60*60*1000)+1;
dayst = days1 + days2;
if (dayst >= DaysInMonth[smonth])  {
AddOneMonth = 1;
dayst -= DaysInMonth[smonth];
}
else AddOneMonth = 0;
ydiff1 = thisyear-mstart.getFullYear();
mdiff1 = thismonth-mstart.getMonth()+AddOneMonth;
if (mdiff1 >11) { mdiff1=0; ydiff1++; }
if (mdiff1 < 0) { mdiff1 = mdiff1 + 12; ydiff1--; }
if (ydiff1 >= 100) ( ydiff1 = ydiff1-100)
temp = (ydiff1==0?"":(ydiff1==1?ydiff1+" Jahr, ":ydiff1 + " Jahre, "));
temp += (mdiff1==0?"0 Monat(e), und ":(mdiff1==1?mdiff1+" Monat(e), und ":mdiff1+" Monate und "));
temp += (dayst==0?"0 Tage":(dayst==1 ? " 1 Tag &nbsp;" : Math.floor(dayst) + " Tage&nbsp;" ));
return temp;
}


</script>
<script language="JavaScript">

document.write(HowLongSince(05,16,01));

document.write(" online ");


</script></td>
</tr>
</table></div></body>
</html>