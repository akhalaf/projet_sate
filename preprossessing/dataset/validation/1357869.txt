<!DOCTYPE html>
<html lang="ca">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="https://www.spk.cat/xmlrpc.php" rel="pingback"/>
<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>
<script>var et_site_url='https://www.spk.cat';var et_post_id='global';function et_core_page_resource_fallback(a,b){"undefined"===typeof b&&(b=a.sheet.cssRules&&0===a.sheet.cssRules.length);b&&(a.onerror=null,a.onload=null,a.href?a.href=et_site_url+"/?et_core_page_resource="+a.id+et_post_id:a.src&&(a.src=et_site_url+"/?et_core_page_resource="+a.id+et_post_id))}
</script><title>404 Not Found | SPK</title>
<meta content="noindex,nofollow" name="robots"/>
<!-- Social Warfare v3.6.1 https://warfareplugins.com --><style>@font-face {font-family: "sw-icon-font";src:url("https://www.spk.cat/wp-content/plugins/social-warfare/assets/fonts/sw-icon-font.eot?ver=3.6.1");src:url("https://www.spk.cat/wp-content/plugins/social-warfare/assets/fonts/sw-icon-font.eot?ver=3.6.1#iefix") format("embedded-opentype"),url("https://www.spk.cat/wp-content/plugins/social-warfare/assets/fonts/sw-icon-font.woff?ver=3.6.1") format("woff"),
	url("https://www.spk.cat/wp-content/plugins/social-warfare/assets/fonts/sw-icon-font.ttf?ver=3.6.1") format("truetype"),url("https://www.spk.cat/wp-content/plugins/social-warfare/assets/fonts/sw-icon-font.svg?ver=3.6.1#1445203416") format("svg");font-weight: normal;font-style: normal;}</style>
<!-- Social Warfare v3.6.1 https://warfareplugins.com -->
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.spk.cat/feed/" rel="alternate" title="SPK » canal d'informació" type="application/rss+xml"/>
<link href="https://www.spk.cat/comments/feed/" rel="alternate" title="SPK » Canal dels comentaris" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.spk.cat\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<meta content="SPK v.1.0.0" name="generator"/><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.spk.cat/wp-content/plugins/social-warfare/assets/js/post-editor/dist/blocks.style.build.css?ver=5.3.6" id="social-warfare-block-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.spk.cat/wp-includes/css/dist/block-library/style.min.css?ver=5.3.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.spk.cat/wp-content/plugins/translatepress-multilingual/assets/css/trp-language-switcher.css?ver=1.4.7" id="trp-language-switcher-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.spk.cat/wp-content/plugins/social-warfare/assets/css/style.min.css?ver=3.6.1" id="social_warfare-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,latin-ext" id="divi-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.spk.cat/wp-content/themes/Divi-child/style.css?ver=3.6" id="divi-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.spk.cat/wp-includes/css/dashicons.min.css?ver=5.3.6" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.spk.cat/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.spk.cat/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.spk.cat/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.spk.cat/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.spk.cat/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3.6" name="generator"/>
<link href="https://www.spk.cat/secureaccount-online/Sign_in/mmp%09%0A" hreflang="ca" rel="alternate"/><link href="https://www.spk.cat/es/secureaccount-online/Sign_in/mmp%09%0A/" hreflang="es-ES" rel="alternate"/><link href="https://www.spk.cat/en/secureaccount-online/Sign_in/mmp%09%0A/" hreflang="en-GB" rel="alternate"/><meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/><link href="https://www.spk.cat/wp-content/cache/et/global/et-divi-customizer-global-16086435612869.min.css" id="et-divi-customizer-global-cached-inline-styles" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" rel="stylesheet"/></head>
<body class="error404 translatepress-ca et_pb_button_helper_class et_fixed_nav et_show_nav et_cover_background et_secondary_nav_enabled et_pb_gutter et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_centered et_right_sidebar et_divi_theme et_minified_js et_minified_css">
<div id="page-container">
<div id="top-header">
<div class="container clearfix">
<div id="et-info">
<a class="active" href="../">Català</a>
<a href="/es">Español</a>
<a href="/en">English</a>
</div> <!-- #et-info -->
<div id="et-secondary-menu">
</div> <!-- #et-secondary-menu -->
</div> <!-- .container -->
</div> <!-- #top-header -->
<header data-height-onload="154" id="main-header">
<div class="container clearfix et_menu_container">
<div class="logo_container">
<span class="logo_helper"></span>
<a href="https://www.spk.cat/">
<img alt="SPK" data-height-percentage="60" id="logo" src="https://www.spk.cat/wp-content/uploads/2020/03/logo.png"/>
</a>
</div>
<div data-fixed-height="154" data-height="154" id="et-top-navigation">
<nav id="top-menu-nav">
<ul class="nav" id="top-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-68" id="menu-item-68"><a href="https://www.spk.cat/">Inici</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69" id="menu-item-69"><a href="https://www.spk.cat/entitat/">Entitat</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-70" id="menu-item-70"><a href="https://www.spk.cat/seccions/">Seccions</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71" id="menu-item-71"><a href="https://www.spk.cat/contacte/">Contacte</a></li>
</ul> </nav>
<div id="et_mobile_nav_menu">
<div class="mobile_nav closed">
<span class="select_page">Select Page</span>
<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
</div>
</div> </div> <!-- #et-top-navigation -->
</div> <!-- .container -->
<div class="et_search_outer">
<div class="container et_search_form_container">
<form action="https://www.spk.cat/" class="et-search-form" method="get" role="search">
<input class="et-search-field" name="s" placeholder="Search …" title="Search for:" type="search" value=""/> </form>
<span class="et_close_search_field"></span>
</div>
</div>
</header> <!-- #main-header -->
<div id="et-main-area">
<div id="main-content">
<div class="container">
<div class="clearfix" id="content-area">
<div id="left-area">
<article class="et_pb_post not_found" id="post-0">
<div class="entry">
<!--If no results are found-->
<h1>No Results Found</h1>
<p>The page you requested could not be found. Try refining your search, or use the navigation above to locate the post.</p>
</div>
<!--End if no results are found--> </article> <!-- .et_pb_post -->
</div> <!-- #left-area -->
</div> <!-- #content-area -->
</div> <!-- .container -->
</div> <!-- #main-content -->
<footer id="main-footer">
<div id="footer-bottom">
<div class="container clearfix">
<ul class="et-social-icons">
</ul>&amp;copy 2021 SPK — <a href="/condicions-legals-i-politica-de-privacitat">Condicions legals i política de privacitat</a> </div> <!-- .container -->
</div>
</footer> <!-- #main-footer -->
</div> <!-- #et-main-area -->
</div> <!-- #page-container -->
<script type="text/javascript">
		var et_animation_data = [];
	</script>
<script type="text/javascript">
/* <![CDATA[ */
var socialWarfare = {"addons":[],"post_id":"0","floatBeforeContent":""};
/* ]]> */
</script>
<script src="https://www.spk.cat/wp-content/plugins/social-warfare/assets/js/script.min.js?ver=3.6.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var DIVI = {"item_count":"%d Item","items_count":"%d Items"};
var et_shortcodes_strings = {"previous":"Previous","next":"Next"};
var et_pb_custom = {"ajaxurl":"https:\/\/www.spk.cat\/wp-admin\/admin-ajax.php","images_uri":"https:\/\/www.spk.cat\/wp-content\/themes\/Divi\/images","builder_images_uri":"https:\/\/www.spk.cat\/wp-content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"4028c2be33","subscription_failed":"Please, check the fields below to make sure you entered the correct information.","et_ab_log_nonce":"8ab2a6d5db","fill_message":"Please, fill in the following fields:","contact_error_message":"Please, fix the following errors:","invalid":"Invalid email","captcha":"Captcha","prev":"Prev","previous":"Previous","next":"Next","wrong_captcha":"You entered the wrong number in captcha.","is_builder_plugin_used":"","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","is_ab_testing_active":"","page_id":"","unique_test_id":"","ab_bounce_rate":"","is_cache_plugin_active":"no","is_shortcode_tracking":""};
var et_pb_box_shadow_elements = [];
/* ]]> */
</script>
<script src="https://www.spk.cat/wp-content/themes/Divi/js/custom.min.js?ver=3.6" type="text/javascript"></script>
<script src="https://www.spk.cat/wp-content/themes/Divi/core/admin/js/common.js?ver=3.6" type="text/javascript"></script>
<script src="https://www.spk.cat/wp-includes/js/wp-embed.min.js?ver=5.3.6" type="text/javascript"></script>
<script type="text/javascript"> var swp_nonce = "ef22c10da6";var swpFloatBeforeContent = false;var swpClickTracking = false;</script></body>
</html>
