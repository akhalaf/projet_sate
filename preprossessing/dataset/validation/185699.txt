<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "32689",
      cRay: "610fe713ffd524bb",
      cHash: "bb96114d55a39c7",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly9iYWtlcGxheXNtaWxlLmNvbS8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "iWvtZ0u6O+uRQFylre/LKgVNDzin9gEm+z/4u3sDMUYFPcDfDb5J/JXmajCsr48SGXC/oMsxdw2UPFqjdjOXFP6NEuGeBRcUWaNiHZyCSr3YIrDO+/ppoaVuBsZM632Qdnv/t2LorD4Apj1AcCddXwm2JZOpVU0Jn6GlTxi1A4xsA8vM/DTuS0b8lhT4emwH51gcZ2B3ACYBTn+96HVaz6LM889T805KDoGbVli+g5q3CAmZPierY9L21hmsoCPGEuC2ixOwSUvZ5+C4A+Gbj9MNn6+mSAC8AtWmMf/qM2n1Ep0+z6v2H27afLItamFJFC1l5t7KG44ta0Q1ysIk2JP/j/TO+IMTqRPu39stEZy8uozGadiU97IYE1nJvb8J3/NDKAX6THXCa6JNx3CIGlQKBxIEoL9GC5vB2EUfSgNbXYJtBqyF0dGuFce377SETGPFeZRhou0CGEBOK900mX+ZWwgmAg4Fmk3GSR9be/vkseNyPDlnmCK4QybCpCCwg8p0ul+yb4BDsF3E/r5P5Z7NxQANxpRRGMHRNwjGJruzN0KYAf0jmq8CzM+C2pg51w07PdLcT6OxDw/nOrnwz8WSEN50JS3hGsb+TF9uiyNRZaqh2GqHjTvP2iyLQAu+YX5Q/9D82qbCH76XxPy/uX1TxGZnkaWMhqNZ4YlsTN/0yC+dug53OppXjgkBHs42cEC5qem+G7ItpzIDsmUeKVeLbGhCavZ6c69dYzotZ3g/yUgm+2VUIB7i1sGQZ6kF3hJ5wrfkaFTVBA32eJNG3dcDP6GKmad3yrSIFc2/5+0=",
        t: "MTYxMDU0OTE0My42NzgwMDA=",
        m: "LgczwGqfnC44pETjM9amX/jrgmhEyc2DwChEdIMOGXs=",
        i1: "kid3qhaFEPWLUHOsFdHCyg==",
        i2: "82fZuI6z8xiTNnpYJWNVkQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "Bjcv6VWpkSckWx3cVC6vSaS+LH3BLalqyK7+/WjZ8p8=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610fe713ffd524bb");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> bakeplaysmile.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=1fec08efde576b04cf7e630119e9e2fb24bd17b0-1610549143-0-AR_qx1vMvadAnoTXxQq6X-MYjbKawqKQ-QcYh-e-z2HhG8jnzldUin_bqIRn3KK48cheUIk9rrE5-K-SQUAE60k0Ay1bE6iD9AMXH5ndPFA9XJTnKB-EMTEz39chWxE4owxWrqvSedhpJzj4Rb0J5fZEoc1yfLb0AoGnizFfXNzT5_jKQBvLjwOjQ0sFePvCLLnC0F2Lx44B6W7DS70PSxqw3GUx7f7C42qQMthxK5cQgF-4qbSIhrhHuZM10xgUjnRUsD3HlW_GhXOSJxThYL-MrrxQ6QmQ5PyIMnsBeNkooiGgfjKov7lY0B3gcmc3RlYpduQUqKY_NV93pLWKxMw6--7r5RpTEWtDCEnevqRiipGYpZDoWWPiG0rRx3XRxA" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="23df4891126c53cec24560cfaf945a42376495e4-1610549143-0-Aam+wfZXsd/v3FSGoGFADXZEc8flCjNRDDloSvlQAmS7ePZa7zElGBdt51N1cR1P6WqaJ4QSAYYddDsBmT4IK6Kd4L03McPjBEOHncF+59OxU8extSvUUIv5K8WIt5nticuW7VOQF5sPv/bkXrHXT7jsSvvup1rPKFRhHTKH//pb5zhnYiYBtlOjCR4X7S6QkNakBMIFIX2iotVvA0qeDcrI64xnSeyWzGfWN+tCRISC4V/vGMzP5rWmkOpLPQRwtjfIu4fdopXRAtFXOvJwU00wyF31Hg9DLrZkwV5hUtWrQnoMVii11hZaS4W05pVuKPX8Hx2OT1uLBdBhithsLZhnid7HIqvkEDoB1PIhszN/b9TybKkMhLYiNdhGLlAzp1jHhm4URPMLUCsV2TanNz0gxRUsP1XTESfMqcNyoYMnkl0khkk530Rer7Em9WfUFRWPykGEzyBwc4U6a1aVYpBdKkQsSyxASJjozVoIgE7bL62EagoEpHFElqnBXpzkq4npoxxixeQNIc9+9EDNqQ2SLYkLiqxPjdEY4/E7alxNHU1zHkrZ4kINCSnkBEUEFalQpPJTuc7ypjk8cyqLlrTTzY+JvQ3JgL0EWCbvHtq94S1rxjxEQFHS+8LuoeHZpPdBdbna6uK21i1Z1URvmuQqlIBJlEKND42hIc6gNOx3VLeYUGtrfVsQsyY2jmgrqrBimLpilv+eDHbazyjjk6FywPHbaK4Nk4vq5uL05UOaRaImcTiy5csVyD12dg2suTQDTQ3Azjv+NcCrglAVqMh4B0LPIZiasUMepr6qkXl+rMjbOn2u5742F4h3k6mRSrE8bxGcA81/L1GYSRl0FutT5PnPvZNqYpW7w2CzvHoyrNQ2R34ehN50w81iZkyIa9imAJqFw64hF3OsdiR00uiPLZTFa9PyXcmpu6x9WRXrKuXxcqPL8Surgk1BBDlSY/6CjNM7ERUIa7xebj6/qJGe3A+VbQ025wezuqC7YNScgxG4L2ug2vC1Ji6bXHOkm0lSlByXadXR79Q54RUjXlQ0QpFoAw95zp7R6svog/bs+5WnQ569XFnMoglIi1QZf8Uq6QUn1ZbF5Ppcw/0gQL5mruiCLqK1Py+Rdgrh9H71aevREE8kRwg3sCkufEbL17Cx79+7M2V7qosPCW7hafuxmTewIjQBsC3ZJVL5ShmcBNW27CxWDVjBFbGFJnCjqzkNOAfo2CFqJ55fQTXc3IKWMWGW1opCHviynIeUIsyJ8thmUTdMem073lY4O/KbhcCcoA7/h8TcZttEEdsXmMUAAoFMHDjOv4TvFV/v+KNDHyvwNzxtXNPZqu9LB9vIdN1mb8kk8qeYHdfU4eEo/jHJaZAbmXxrBLfIbVetrGhv"/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="d7a8c55aed74c64d6a48866691941152"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610549147.678-IXGSZDb4Zm"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610fe713ffd524bb')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610fe713ffd524bb</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
