<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
<title>Sun Rock</title>
<link href="css/sun_style.css" rel="stylesheet" type="text/css"/>
<script src="js/jquery.min.js"></script>
<!--------------------- Product Scroller---------------------->
<link href="css/slick.css" rel="stylesheet" type="text/css"/>
<script src="js/slick.js" type="text/javascript"></script>
<!--------------------- Product Scroller---------------------->
</head>
<body>
<header>
<header>
<section id="head_top">
<div class="container">
<div class="htop_left">
<p>Welcome to SunrockImpex</p>
</div>
<div class="ht_right">
<ul class="ht_social">
<li><a href="" target="_blank" title=""><img alt="" src="images/social/h.png" title=""/></a></li>
<li><a href="" target="_blank" title=""><img alt="" src="images/social/yo.png" title=""/></a></li>
<li><a href="" target="_blank" title=""><img alt="" src="images/social/ca.png" title=""/></a></li>
<li><a href="" target="_blank" title=""><img alt="" src="images/social/g.png" title=""/></a></li>
<li><a href="" target="_blank" title=""><img alt="" src="images/social/fb.png" title=""/></a></li>
<li><a href="" target="_blank" title=""><img alt="" src="images/social/tw.png" title=""/></a></li>
</ul>
</div>
</div>
</section>
</header>
<section id="head_box">
<div class="container">
<div class="logo_div">
<a href="index.php" title=""><img alt="" src="images/logo.png" title=""/></a>
</div>
<div class="h_cuntry"><img alt="" src="images/india.png" title="" width="70"/></div>
<div class="h_contact">
<ul>
<li><img alt="" src="images/mail.png" title=""/> <a href="mailto:sunrockimpex@yahoo.co.in" title="">sunrockimpex@yahoo.co.in</a> </li><br/>
<li style=" margin-bottom:0px;"><img alt="" src="images/call.png" style=" width:30px;" title=""/><span> +96770 30102<br/> +96770 30103 </span></li>
</ul>
</div>
</div>
</section>
</header>
<section id="menu">
<div class="container">
<nav class=" ">
<ul class="nav" id="myTopnav">
<li><a class="active" href="index.php">Home</a></li>
<li><a href="about_us.php"> About Us</a></li>
<li><a href="monuments.php"> Gallery  </a></li>
<li><a href="project.php">Project </a></li>
<li><a href="contact_us.php">Contact Us</a></li>
<li class="icon">
<a href="javascript:void(0);" onclick="myFunction()" style="font-size:15px;">☰</a>
</li>
</ul>
</nav>
</div>
</section>
<section id="i_banner">
<div class="slider autoplay ">
<div> <img class="i_banner_image" src="images/slider/slider1.jpg"/> </div>
<div> <img class="i_banner_image" src="images/slider/slider2.jpg"/> </div>
<div> <img class="i_banner_image" src="images/slider/slider3.jpg"/> </div>
</div>
</section>
<section id="i_welcome">
<div class="container">
<div class="i_welcome_box">
<h3>WELCOME TO THE <strong>SUN ROCK IMPEX</strong> </h3>
<p>India is geologists destination for natural stones. Abundant deposits of rare granite varieties are more popular in world market.
Consistant quality, Reliability and Value added services build our customer relationship. Our factories are well equipped with sufficient infrastructure and skilled team.
We export monumental products, Sculptures, Mausoleums, project materials, Kerbs, Kitchen tops, flower vases, urns, Garden and house-hold articles etc.
</p>
<p>We offer our customers:</p>
<span>“THE BEST OF INDIAN GRANITE MONUMENTS AND SCULPTURES”
“PROMPT DELIVERY OF QUALITY GRANITE”.</span>
</div>
</div>
</section>
<section id="" style="float:left; width:100%; ">
<div class="container">
<img src="images/w_bot.png" style="float:left; width:100%; margin-top:0px;"/>
</div>
</section>
<section id="our_product">
<div class="container">
<div class="our_product_div">
<h3>Our Products</h3>
<div class="our_product_scrole responsive">
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_1.jpg" title=""/>
<a href="monuments.php"><h4>Monuments</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_24.jpg" title=""/>
<a href="mausoleums.php"><h4>Mausoleums</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_29.jpg" title=""/>
<a href="sculptures.php"><h4>Sculpures</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_36.jpg" title=""/>
<a href="flower_vases.php"><h4>Flower Vases</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_60.jpg" title=""/>
<a href="project_related_items.php"><h4>Project Related Items</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_65.jpg" title=""/>
<a href="benches.php"><h4>Benches</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_66.JPG" title=""/>
<a href="monuments.php"><h4>Monuments</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_67.JPG" title=""/>
<a href="monuments.php"><h4>Monuments</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_68.JPG" title=""/>
<a href="monuments.php"><h4>Monuments</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_69.JPG" title=""/>
<a href="monuments.php"><h4>Monuments</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_70.JPG" title=""/>
<a href="monuments.php"><h4>Monuments</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_71.JPG" title=""/>
<a href="monuments.php"><h4>Monuments</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_72.JPG" title=""/>
<a href="sculptures.php"><h4>Sculpures</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_73.JPG" title=""/>
<a href="monuments.php"><h4>Monuments</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_74.JPG" title=""/>
<a href="monuments.php"><h4>Monuments</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_77.JPG" title=""/>
<a href="sculptures.php"><h4>Sculpures</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_78.JPG" title=""/>
<a href="sculptures.php"><h4>Sculpures</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_80.JPG" title=""/>
<a href="sculptures.php"><h4>Sculpures</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<div class="our_product_box">
<div class="our_product_box_inner">
<img alt=" " src="uploads/gallery/gallery_85.jpg" title=""/>
<a href="project_related_items.php"><h4>Project Related Items</h4></a>
</div>
<div class="our_product_bot">
<img alt=" " src="images/our_product_bottom.png" title=""/>
</div>
</div>
<!--                        <div class="our_product_box">
                            <div class="our_product_box_inner">
                                <img src="images/our_product/mausoleum.jpg" alt=" " title="">
                                <a href="mausoleums.php"><h4>Mausoleums </h4></a>
                            </div>	
                            <div class="our_product_bot">
                            	<img src="images/our_product_bottom.png" alt=" " title="">
                            </div>	
                        </div>
                        <div class="our_product_box">
                            <div class="our_product_box_inner">
                                <img src="images/our_product/sculpure.jpg" alt=" " title="">
                                <a href="sculptures.php"><h4>Sculptures</h4></a>
                            </div>	
                            <div class="our_product_bot">
                            	<img src="images/our_product_bottom.png" alt=" " title="">
                            </div>	
                        </div>
                        <div class="our_product_box">
                            <div class="our_product_box_inner">
                                <img src="images/our_product/1.jpg" alt=" " title="">
                                <a href="benches.php"><h4>Benches</h4></a>
                            </div>
                            <div class="our_product_bot">
                            	<img src="images/our_product_bottom.png" alt=" " title="">
                            </div>	
                        </div>
                        <div class="our_product_box">
                            <div class="our_product_box_inner">
                                <img src="images/our_product/op3.jpg" alt=" " title="">
                                <a href="flower_vases.php"><h4>Flower Vases</h4></a>
                            </div>	
                            <div class="our_product_bot">
                            	<img src="images/our_product_bottom.png" alt=" " title="">
                            </div>	
                        </div>
                        <div class="our_product_box">
                            <div class="our_product_box_inner">
                                <img src="images/our_product/special.jpg" alt=" " title="">
                                <a href="project_related_items.php"><h4>Project Related Items</h4></a>
                            </div>	
                            <div class="our_product_bot">
                            	<img src="images/our_product_bottom.png" alt=" " title="">
                            </div>	
                        </div>
                        <div class="our_product_box">
                            <div class="our_product_box_inner">
                                <img src="images/our_product/kitchen.jpg" alt=" " title="">
                                <a href="kitchen_tops.php"><h4>Kitchen Tops</h4></a>
                            </div>	
                            <div class="our_product_bot">
                            	<img src="images/our_product_bottom.png" alt=" " title="">
                            </div>	
                        </div>
--> </div>
</div>
</div>
</section>
<section id="i_gallery">
<div class="container">
<div class="i_gallery_div">
<div class="i_gallery_scrole i_gallerry">
<div class="i_gallery_box">
<div class="i_gallery_box_inner">
<img alt=" " src="images/i_gallery/ig1.jpg" title=""/>
</div>
<div class="i_gallery_bot">
<img alt=" " src="images/i_gallery_bottom.png" title=""/>
</div>
</div>
<div class="i_gallery_box">
<div class="i_gallery_box_inner">
<img alt=" " src="images/i_gallery/ig2.jpg" title=""/>
</div>
<div class="i_gallery_bot">
<img alt=" " src="images/i_gallery_bottom.png" title=""/>
</div>
</div>
<div class="i_gallery_box">
<div class="i_gallery_box_inner">
<img alt=" " src="images/i_gallery/ig3.jpg" title=""/>
</div>
<div class="i_gallery_bot">
<img alt=" " src="images/i_gallery_bottom.png" title=""/>
</div>
</div>
<div class="i_gallery_box">
<div class="i_gallery_box_inner">
<img alt=" " src="images/i_gallery/ig4.jpg" title=""/>
</div>
<div class="i_gallery_bot">
<img alt=" " src="images/i_gallery_bottom.png" title=""/>
</div>
</div>
<div class="i_gallery_box">
<div class="i_gallery_box_inner">
<img alt=" " src="images/i_gallery/ig5.jpg" title=""/>
</div>
<div class="i_gallery_bot">
<img alt=" " src="images/i_gallery_bottom.png" title=""/>
</div>
</div>
<div class="i_gallery_box">
<div class="i_gallery_box_inner">
<img alt=" " src="images/i_gallery/ig1.jpg" title=""/>
</div>
<div class="i_gallery_bot">
<img alt=" " src="images/i_gallery_bottom.png" title=""/>
</div>
</div>
<div class="i_gallery_box">
<div class="i_gallery_box_inner">
<img alt=" " src="images/i_gallery/ig2.jpg" title=""/>
</div>
<div class="i_gallery_bot">
<img alt=" " src="images/i_gallery_bottom.png" title=""/>
</div>
</div>
</div>
</div>
</div>
</section>
<footer>
<div class="container">
<section class="footer_div">
<h3>Sunrock Impex</h3>
<p>The founders of SUN ROCK (IMPEX) and its team having strong knowledge in Granite Industry about more then two decades.<br/>Our Factory is located at Krishnagiri in Between Chennai and Bangalore.</p>
</section>
<section class="footer_div">
<h3>GET IN TOUCH</h3>
<p>#18/16, 3rd Cross Street,<br/> Corporation Complex,<br/> R.A. Puram, Chennai-28. </p>
<div class="f_contact">
<ul>
<li style=" margin-top:10px;"><img alt="" src="images/call2.png" style=" width:34px;" title=""/><span><strong>+91 44 2436 0067 <br/> +96770 30102<br/> +96770 30103</strong></span></li>
<li><img alt="" src="images/mail2.png" title=""/> <a href="mailto:sunrockimpex@yahoo.co.in" title="">sunrockimpex@yahoo.co.in</a> </li><br/>
</ul>
</div>
</section>
<section class="footer_div2">
<h3>GET IN TOUCH</h3>
<form action="mail1.php" class="footer_form" enctype="multipart/form-data" id="myform1" method="post" name="myform1" onsubmit="return(validateform1())">
<div id="naerr1" style="color:#F00;"></div>
<div id="moberr1" style="color:#F00;"></div>
<div id="emaerr1" style="color:#F00;"></div>
<div id="reqerr1" style="color:#F00;"> </div>
<input class="text_box" name="name" onkeyup="AllowAlphabet1()" placeholder="Enter Name" type="text" value=""/>
<input class="text_box" name="mobile" onkeyup="this.value = minmax1(this.value, 0, 9999999999)" placeholder="Enter Mobile" type="text" value=""/>
<input class="text_box" id="email" name="email" placeholder="Enter Email" type="text" value=""/>
<textarea class="textarea" name="message" placeholder="Enter Message" value=""></textarea>
<input class="submit" name="submit" type="submit" value="Submit"/>
</form>
</section>
</div>
</footer>
<script type="text/javascript">
	function minmax1(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 0; 
    else if(parseInt(value) > max) 
        return 0; 
    else return value;
}
</script>
<script type="text/javascript">
function validateform1(){
var Name=document.myform1.name.value;    
 if (Name==null || Name==""){  
  document.getElementById("naerr1").innerHTML="Please Enter Your Name"; 
  return false;  
}
 else
  {
  document.getElementById("naerr1").innerHTML="";
  }

var Mobile=document.myform1.mobile.value;    
 if (Mobile==null || Mobile==""){  
  document.getElementById("moberr1").innerHTML="Please Enter Your Mobile Number"; 
  return false;  
} 
else
  {
  document.getElementById("moberr1").innerHTML="";
  }
if (Mobile.length!=10 ){  
  document.getElementById("moberr1").innerHTML="Please Enter Valid 10 digit Mobile Number"; 
  return false;  
} 
else
  {
  document.getElementById("moberr1").innerHTML="";
  }
var Email=document.myform1.email.value;    
 if (Email==null || Email==""){  
  document.getElementById("emaerr1").innerHTML="Please Enter Your Email-Id"; 
  return false;  
}
else
  {
  document.getElementById("emaerr1").innerHTML="";
  }

var emailid = document.getElementById('email');
var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(emailid.value))
	 {
	document.getElementById("emaerr1").innerHTML="Please provide a valid email address"; 
    emailid.focus;
    return false;
	}
else
  {
  document.getElementById("emaerr1").innerHTML="";
  }


var Requirement=document.myform1.message.value;    
 if (Requirement==null || Requirement==""){  
  document.getElementById("reqerr1").innerHTML="Please Enter Your Requirement"; 
  return false;  
} 
else
  {
  document.getElementById("reqerr1").innerHTML="";
  }
}
</script>
<script type="text/javascript">
function AllowAlphabet1(){
if (!myform1.name.value.match(/^[a-zA-Z]+$/) && myform1.name.value !="")
{
myform1.name.value="";
myform1.name.focus();
document.getElementById("naerr1").innerHTML="Please Enter alphabets in Name Field"; 
//alert("Please Enter only alphabets in text");
}
}
</script>
<section class="footer_bot">
<div class="container">
<p>All CopyRights Reserved 2016. By www.sunrockimpex.co.in</p>
<img alt="" src="images/crb_logo.png" title=""/>
</div>
</section>
</body>
<script>
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}
</script>
<script type="text/javascript">
var c=jQuery.noConflict();
c(window).load(function() {
c('.autoplay').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed:4000,
});
 c('.responsive').slick({
  dots: true,
  autoplay: true,
  infinite: true,
  speed:600,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
	  {
      breakpoint:970,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 640,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint:320,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

 c('.i_gallerry').slick({
  dots: true,
  autoplay: true,
  infinite: true,
  speed: 300,
  slidesToShow: 6,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
	  {
      breakpoint:970,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 640,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint:320,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
  ]
});
    
});
</script>
</html>
