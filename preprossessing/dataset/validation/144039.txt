<!DOCTYPE HTML>
<html lang="en">
<head><meta charset="utf-8"/><meta content="ie=edge" http-equiv="x-ua-compatible"/><meta content="width=device-width, initial-scale=1, minimum-scale=1" name="viewport"/><!-- Typekit -->
<script src="https://use.typekit.net/zmi4osf.js" type="text/javascript"></script>
<script type="text/javascript">// <![CDATA[
	try{Typekit.load({ async: true });}catch(e){}
	// ]]></script>
<!-- Typography --> <link href="/_resources/images/favicon.ico" rel="icon" type="image/png"/> <link href="https://cloud.typography.com/6451356/7157372/css/fonts.css" rel="stylesheet" type="text/css"/> <link href="/_resources/css/style.css" rel="stylesheet"/> <link href="/_resources/css/oustyles.css" rel="stylesheet"/>
<script type="text/javascript">// <![CDATA[
	var rl_siteid = "101ed567-eb85-4e93-956b-88bb734eece2";
	// ]]></script>
<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 812309174;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
</script>
<script src="//www.googleadservices.com/pagead/conversion.js" type="text/javascript">
</script>
<noscript>
<div style="display:inline;">
<img alt="" height="1" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/812309174/?guid=ON&amp;script=0" style="border-style:none;" width="1"/>
</div>
</noscript>
<!-- script type="text/javascript" src="http://cdn.rlets.com/capture_static/mms/mms.js" async="async"></script -->
<!-- Facebook Pixel Code -->
<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');


fbq('init', '671324823232628'); 

fbq('track', 'PageView');

</script>
<noscript>
<noscript><img height="”1”" src="https://www.facebook.com/tr?id=671324823232628&amp;ev=PageView

&amp;noscript=1" style="”display:none”" width="”1”"/>
</noscript>
<!-- End Facebook Pixel Code -->
<!-- Global site tag (gtag.js) - AdWords: 795521573 -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=AW-795521573 (https://urldefense.proofpoint.com/v2/url?u=https-3A__www.googletagmanager.com_gtag_js-3Fid-3DAW-2D795521573&amp;d=DwMGaQ&amp;c=pFsveJJPHV1MuWVdwlSI7Q&amp;r=Ji4V-dKnunosHXAtEMuxAsiFqK5Sn0l1J6l3jngMRVE&amp;m=hH1oGamPJczMJEtYNyeWNdoOXHR_Np9YuSx2mGmYa2o&amp;s=OyCzgiebEQfH99ZEzHXj9pS6X4myJ88nLqzdFhbKQOw&amp;e=) "></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-795521573'); </script>
<!-- End Global site tag (gtag.js) - AdWords: 795521573 --> <title>404</title>
</noscript></head>
<body><header class="header">
<div class="header__wrap">
<div class="header__small"><a class="header__small__logo" href="/index.php"><span class="hide">Home</span><span class="svgstore svgstore--APSU_Logo">
<svg><use xlink:href="/_resources/images/svgstore.svg#APSU_Logo"></use></svg></span></a><button class="header__small__menu-toggle"><span class="hide">Toggle Menu</span><span class="header__small__menu-toggle__open"><span class="svgstore svgstore--menu">
<svg><use xlink:href="/_resources/images/svgstore.svg#menu"></use></svg></span></span><span class="header__small__menu-toggle__close"><span class="svgstore svgstore--close">
<svg><use xlink:href="/_resources/images/svgstore.svg#close"></use></svg></span></span></button><button class="header__small__search-toggle"><span class="hide">Toggle Search</span><span class="header__small__search-toggle__open"><span class="svgstore svgstore--search">
<svg><use xlink:href="/_resources/images/svgstore.svg#search"></use></svg></span></span><span class="header__small__search-toggle__close"><span class="svgstore svgstore--close">
<svg><use xlink:href="/_resources/images/svgstore.svg#close"></use></svg></span></span></button><a class="header__small__back" href="#"><span aria-label="Go Back" class="header__small__back__icon"><span class="hide">Go back</span><span class="svgstore svgstore--Arrow_Small_Left">
<svg><use xlink:href="/_resources/images/svgstore.svg#Arrow_Small_Left"></use></svg></span></span><span class="header__small__back__text"></span></a></div>
<div class="header__nav__info__wrap">
<ul class="header__nav__info__list">
<li class="header__nav__info__item"><a class="header__nav__info__link" href="http://www.alumni.apsu.edu/s/1806/17/home.aspx?gid=2&amp;pgid=61">Alumni</a></li>
<li class="header__nav__info__item"><a class="header__nav__info__link" href="/pr-marketing/current.php">Current Students</a></li>
<li class="header__nav__info__item"><a class="header__nav__info__link" href="/fac-staff/index.php">Faculty &amp; Staff</a></li>
<li class="header__nav__info__item"><a class="header__nav__info__link" href="/military/index.php">Military</a></li>
<li class="header__nav__info__item"><a class="header__nav__info__link" href="/parents/index.php">Parents &amp; Families</a></li>
</ul><button class="header__nav__info__close"><span class="hide">Close</span><span class="svgstore svgstore--close">
<svg><use xlink:href="/_resources/images/svgstore.svg#close"></use></svg></span></button></div>
<div class="header__search">
<div class="header__search__wrap">
<form action="/search" class="header__search__form" method="get"><label class="hide" for="website__search">Search</label><div class="header__search__input__wrap"><input class="header__search__input" id="website__search" name="q" placeholder="Search..." type="search"/><button class="header__search__button"><span class="hide">Submit</span><span class="svgstore svgstore--search">
<svg><use xlink:href="/_resources/images/svgstore.svg#search"></use></svg></span></button></div>
</form><button class="header__search__close"><span class="hide">Close Search</span><span class="svgstore svgstore--close">
<svg><use xlink:href="/_resources/images/svgstore.svg#close"></use></svg></span></button><ul class="header__search__list">
<li class="header__search__item"><a class="header__search__link" href="/azindex/index.php">A-Z Index</a></li>
<li class="header__search__item"><a class="header__search__link" href="/directory/">People Search</a></li>
</ul>
</div>
</div>
<div class="header__menu">
<div class="header__scroll"><a class="header__logo" href="/index.php"><span class="hide">Home</span><span class="header__logo--icon"></span></a><ul class="nav">
<li><a href="/about-apsu/index.php" title="About">About APSU</a><ul class="menu">
<li><a href="/president/index.php" title="Leadership">Leadership</a></li>
<li><a href="/about-apsu/mission.php" title="Mission &amp; Vision">Mission &amp; Vision</a></li>
<li><a href="/about-apsu/accreditation.php">Accreditation</a></li>
<li><a href="/about-apsu/history-austin-peay.php">History &amp; Traditions</a></li>
<li><a href="/about-apsu/fast-facts-about-apsu.php">Fast Facts</a></li>
<li><a href="/about-apsu/institutional-disclosures.php">Institutional Disclosures</a></li>
</ul>
</li>
<li><a href="/admissions/index.php">Admissions</a><ul class="menu">
<li><a href="/admissions/applicant/admissions-process.php">Application Process</a></li>
<li><a href="/cost/index.php">Costs to Attend</a></li>
<li><a href="/financialaid/index.php" title="Financial Aid">Financial Aid</a></li>
<li><a href="/admissions/visit/index.php">Visiting Campus</a></li>
<li><a href="/virtual-tour/index.php">Virtual Tour</a></li>
<li><a href="/admissions/recruitment/counselors.php">Meet Your Counselors</a></li>
<li><a href="/admissions/admitted/admitted-checklist.php">For Admitted Students</a></li>
<li><a href="/admissions/admitted/online_forms.php">Admissions Forms</a></li>
<li><a href="/govnow/index.php">Dual Enrollment</a></li>
<li><a href="/grad-studies/index.php" title="Graduate Studies and Admission">Graduate Admissions</a></li>
</ul>
</li>
<li><a href="/academics/index.php">Academics</a><ul class="menu">
<li><a href="/academics/what-sets-apsu-apart/academic-achievement.php">What Sets APSU Apart?</a></li>
<li><a href="/academics/departments.php">Programs for You</a></li>
<li><a href="/programs/index.php" title="Find Your Program">Find Your Program</a></li>
<li><a href="/academics/departments.php">Colleges and Schools</a></li>
<li><a href="/programs/online.php">100% Online Degrees</a></li>
<li><a href="/academic-affairs/index.php">Academic Leadership</a></li>
<li><a href="http://library.apsu.edu/">Felix G. Woodward Library</a></li>
</ul>
</li>
<li><a href="/campus-life/index.php">Campus Life</a><ul class="menu" style="max-width: 225px;">
<li><a href="/housing/index.php">Housing</a></li>
<li><a href="/dining/index.php">Dining</a></li>
<li><a href="/student-life/index.php">Student Life &amp; Engagement</a></li>
<li><a href="/greek-life/index.php">Greek Life</a></li>
<li><a href="/recreation/index.php">Recreation</a></li>
<li><a href="/police/emergency-alerts.php">Campus Safety</a></li>
<li><a href="/health-and-counseling/index.php">Student Wellness</a></li>
</ul>
</li>
<li><a href="http://www.letsgopeay.com/">Athletics</a></li>
<li><a href="/index.php" id="iam">I Am...</a><ul class="menu">
<li><a href="/home-alumni/index.php">Alumni</a></li>
<li><a href="/pr-marketing/current.php">Current Student</a></li>
<li><a href="/fac-staff/index.php">Faculty/Staff</a></li>
<li><a href="/military/index.php">Military</a></li>
<li><a href="/parents/index.php">Parent/Family</a></li>
</ul>
</li>
</ul>
<nav class="header__nav__secondary"><button class="header__nav__info"><span class="header__nav__info__icon"><span class="header__nav__info--open"><span class="svgstore svgstore--people">
<svg><use xlink:href="/_resources/images/svgstore.svg#people"></use></svg></span></span></span><span class="header__nav__info__text">Information For...</span></button><ul class="header__nav__info__list--mobile">
<li class="header__nav__info__item"><a class="header__nav__info__link" href="http://www.alumni.apsu.edu/s/1806/17/home.aspx?gid=2&amp;pgid=61">Alumni</a></li>
<li class="header__nav__info__item"><a class="header__nav__info__link" href="/pr-marketing/current.php">Current Students</a></li>
<li class="header__nav__info__item"><a class="header__nav__info__link" href="/fac-staff/index.php">Faculty &amp; Staff</a></li>
<li class="header__nav__info__item"><a class="header__nav__info__link" href="/military/index.php">Military</a></li>
<li class="header__nav__info__item"><a class="header__nav__info__link" href="/parents/index.php">Parents &amp; Families</a></li>
</ul><button class="header__nav__search"><span class="header__nav__search__icon"><span class="header__nav__search--open"><span class="svgstore svgstore--search">
<svg><use xlink:href="/_resources/images/svgstore.svg#search"></use></svg></span></span><span class="header__nav__search--close"><span class="svgstore svgstore--close">
<svg><use xlink:href="/_resources/images/svgstore.svg#close"></use></svg></span></span></span></button></nav>
</div>
</div>
</div>
</header> <div class="breadcrumbs">
<div class="wrap"><span><a href="/">Home</a></span> » <span>404</span></div>
</div>
<div class="wrap">
<main class="main" id="main-content">
<h1 style="text-align: left;"><span style="font-size: 66.6667px;">Oh nuts, let's try that <a href="http://www.apsu.edu/search">again</a>.</span></h1>
<p><a href="/campus-life/experiences.php" target="_blank" title="Learn more about APSU's campus squirrels and experiences"><img alt="One of APSU's famous campus squirrels sitting in the mulch" class="" height="800" src="/404/campus-squirrel.jpg" width="1200"/></a></p>
<h2><strong>The page or file you are looking for cannot be found.</strong></h2>
<p><span class="p--intro">T</span><span class="p--intro">ry visiting the <a href="/azindex/index.php">A-Z Index</a> or performing a search using the search icon in the header - or go check out more
                  <a href="https://www.instagram.com/p/B8ZovOUpRVe/">campus squirrels and wildlife</a>! </span></p>
</main>
<div class="sidebar">
<div class="sidebar__panel">
<div class="h6">Contact us!</div>
<ul class="sidebar__panel--info">
<li>Phone: <a href="tel:9312217011">(931) 221-7011</a></li>
<li>Email: <a href="mailto:gov@apsu.edu">gov@apsu.edu</a></li>
</ul>
</div>
<div class="sidebar__panel">
<ul class="button__list">
<li><a class="button button--full" href="/azindex/index.php">A-Z Index</a></li>
<li><a class="button button--full" href="/programs/index.php">Explore our programs</a></li>
<li><a class="button button--full" href="/admissions/applicant/apply.php">Apply now</a></li>
</ul>
</div>
<div class="sidebar__panel">
<p><strong>Do you think the page you were looking for is missing? Let us know!</strong></p>
<p>Email us at <a href="mailto:webupdate@apsu.edu?subject=Error 404 broken link">webupdate@apsu.edu</a> with the broken link and where you found it.
               </p>
</div>
</div>
</div><footer class="footer wrap--relative">
<div class="footer__grid wrap wrap--wide"><a class="footer__gov__logo" href="/index.php"><span class="hide">Home</span><span class="svgstore svgstore--TheGov">
<svg><use xlink:href="/_resources/images/svgstore.svg#TheGov"></use></svg></span></a><ul class="footer__locations">
<li><a href="/clarksville/index.php">Clarksville Campus</a></li>
<li><a href="/apfc/index.php" title="Austin Peay Center at Fort Campbell">Austin Peay Center at Fort Campbell</a></li>
<li><a href="/springfield/index.php">Highland Crest Campus</a></li>
<li><a href="http://library.apsu.edu/">Felix G. Woodward Library</a></li>
</ul>
<ul class="footer__link__list">
<li><a href="/human-resources/index.php">Jobs at APSU</a></li>
<li><a href="http://onestop.apsu.edu">OneStop Login</a></li>
<li><a href="/legal-affairs/titleix.php">Non-Discrimination Policy</a></li>
<li><a href="/police/index.php">Emergency Information</a></li>
<li><a href="/pr-marketing/contact-us.php">Contact Us</a></li>
</ul>
<ul class="footer__icons">
<li><a href="https://www.tbr.edu/initiatives/tennessee-transfer-pathway"><span class="hide">Pathway</span><span class="svgstore svgstore--pathway">
<svg><use xlink:href="/_resources/images/svgstore.svg#pathway"></use></svg></span></a></li>
<li><a href="http://www.tnecampus.org/"><span class="hide">TN campus</span><span class="svgstore svgstore--tn_campus">
<svg><use xlink:href="/_resources/images/svgstore.svg#tn_campus"></use></svg></span></a></li>
</ul>
<div style="color:white;clear:left;margin-left: 18rem;">Copyright <span id="directedit">©</span> 2017
      </div>
</div>
<div class="footer__social">
<ul class="footer__social__list">
<li><a class="footer__social__link" href="https://www.facebook.com/austinpeay"><span class="hide">Facebook</span><span class="svgstore svgstore--Social_Footer_FB">
<svg><use xlink:href="/_resources/images/svgstore.svg#Social_Footer_FB"></use></svg></span></a></li>
<li><a class="footer__social__link" href="https://www.youtube.com/user/apsu1927"><span class="hide">Youtube</span><span class="svgstore svgstore--Social_Youtube">
<svg><use xlink:href="/_resources/images/svgstore.svg#Social_Youtube"></use></svg></span></a></li>
<li><a class="footer__social__link" href="https://twitter.com/austinpeay"><span class="hide">Twitter</span><span class="svgstore svgstore--Social_Twitter">
<svg><use xlink:href="/_resources/images/svgstore.svg#Social_Twitter"></use></svg></span></a></li>
<li><a class="footer__social__link" href="https://www.instagram.com/austinpeay/"><span class="hide">Instagram</span><span class="svgstore svgstore--Social_IG">
<svg><use xlink:href="/_resources/images/svgstore.svg#Social_IG"></use></svg></span></a></li>
</ul>
</div>
</footer><script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
<script src="/_resources/js/script.js?v=20190118"></script>
<script src="/_resources/js/direct-edit.js"></script>
<script crossorigin="anonymous" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" src="https://code.jquery.com/jquery-2.2.4.min.js"></script><script type="text/javascript">// <![CDATA[
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-7506525-1', 'auto');
  ga('send', 'pageview');
// ]]></script><script src="/_resources/js/direct-edit.js"></script><div id="hidden" style="display:none;">
<a href="https://a.cms.omniupdate.com/10?skin=oucampus&amp;account=apsu&amp;site=www&amp;action=de&amp;path=/404/index.pcf" id="de" style="color:white;text-decoration:none;">©</a>
</div>
</body>
</html>