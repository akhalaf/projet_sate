<!DOCTYPE html>
<html dir="ltr" lang="pt-br" xml:lang="pt-br" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>404 - Os cookies parecem não estar ativos no seu navegador. Você não poderá instalar o aplicativo com esse recurso desativado. Também, pode haver um problema com o <strong>session.save_path</strong> do servidor. Se este for o caso, consulte seu provedor de hospedagem se não souber como verificar ou resolver isso por conta própria.: 404</title>
<link href="https://www.programaespecial.com.br/templates/jm-commune-offices/css/error.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="jm-error">
<div class="jm-error-title">
<div class="jm-error-code">
<h1>404</h1>
</div>
<div class="jm-error-message">
<h2>Categoria não encontrada</h2>
</div>
</div>
<div class="jm-error-desc">
			The page you are looking for does not exist or an other error occurred.<br/>
			Go back or head over to the home page to choose a new direction.<br/>
<div class="jm-error-buttons">
<a class="jm-error-left" href="javascript:history.go(-1)">Back</a> <a class="jm-error-right" href="https://www.programaespecial.com.br/" title="Home">Home</a>
</div>
</div>
</div>
</body>
</html>
