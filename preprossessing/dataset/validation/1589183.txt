<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>WinDuctÂ® - Malaysia Leader In Quality Fire Rated Duct</title>
<meta content="ductwork system, Active Fire Protection system, fire resistance products, quality assurance, WinDuct" name="keywords"/>
<meta content="WinDuctÂ® able to stand 2 Hours Fire Rated Smoke Extraction Ductwork. Your Design Our Protection" name="description"/>
<meta content="http://www.winduct.com" name="og:url"/>
<meta content="WinDuctÂ® - Leader In Quality Fire Rated Duct" name="og:title"/>
<meta content="WinDuctÂ® able to stand 2 Hours Fire Rated Smoke Extraction Ductwork. Your Design Our Protection" name="og:description"/>
<meta content="WinDuctÂ®" name="rights"/>
<meta content="WinDuctÂ® - Malaysia Leader In Quality Fire Rated Duct" property="og:site_name"/>
<meta content="index,follow" name="googlebot"/>
<meta content="follow, all" name="robots"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<link href="images/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="images/apple-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="images/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="images/apple-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="images/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="images/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="images/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="images/apple-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="images/apple-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="images/android-icon-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="images/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="images/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="images/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/manifest.json" rel="manifest"/>
<meta content="#ffffff" name="msapplication-TileColor"/>
<meta content="images/ms-icon-144x144.png" name="msapplication-TileImage"/>
<meta content="#ffffff" name="theme-color"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="yes" name="apple-touch-fullscreen"/>
<meta content="default" name="apple-mobile-web-app-status-bar-style"/>
<link href="css/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="css/swiper.css" media="screen" rel="stylesheet" type="text/css"/>
<link class="main-stylesheet" href="css/style.css" rel="stylesheet" type="text/css"/>
<link href="css/main.css" rel="stylesheet" type="text/css"/>
<link class="main-stylesheet" href="css/pages-icons.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
        .cf-hidden {
            display: none;
        }
        
        .cf-invisible {
            visibility: hidden;
        }
    </style>
<script async="" src="js/ga.js" type="text/javascript"></script>
<script async="" src="js/slideshow.js" type="text/javascript"></script>
</head><body class="pace-dark windows desktop pace-done"> <div class="pace pace-inactive"><div class="pace-progress" data-progress="99" data-progress-text="100%" style="width: 100%;"> <div class="pace-progress-inner"></div></div><div class="pace-activity"></div></div>
<div class="pace pace-inactive">
<div class="pace-progress" data-progress="99" data-progress-text="100%" style="width: 100%;">
<div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div>
</div>
<nav class="header bg-header transparent-dark" data-pages="header" data-pages-header="autoresize" data-pages-resize-class="dark">
<div class="container relative">
<div class="pull-left">
<div class="header-inner"><img alt="" class="logo" data-src-retina="images/logo_winduct_2x.png" src="images/logo_winduct.png" width="152"/><img alt="" class="alt" data-src-retina="images/logo_winduct_2x.png" src="images/logo_winduct.png" width="152"/></div>
</div>
<div class="pull-right">
<div class="header-inner">
<div class="visible-sm-inline visible-xs-inline menu-toggler pull-right p-l-10" data-pages="header-toggle" data-pages-element="#header">
<div class="one"></div>
<div class="two"></div>
<div class="three"></div>
</div>
</div>
</div>
<div class="menu-content mobile-dark pull-right clearfix" data-pages-direction="slideRight" id="header">
<div class="pull-right"><a class="padding-10 visible-xs-inline visible-sm-inline pull-right m-t-10 m-b-10 m-r-10" data-pages="header-toggle" data-pages-element="#header" href="#"><i class=" pg-close_line"></i></a></div>
<div class="header-inner">
<ul class="menu">
<li><a class="active" href=" #">Home</a></li>
<li class="classic"><a href="javascript:;">About Us <i class="pg-arrow_minimize m-l-5"></i></a>
<nav class="classic "><span class="arrow"></span>
<ul>
<li><a href="about_winduct.html">About WinDuctÂ®</a></li>
<li><a href="why_choose_winduct.html">Why Choose WinDuctÂ®</a></li>
<li><a href="winduct_quality.html">WinductÂ® Quality Assurance</a></li>
</ul>
</nav>
</li>
<li class="classic"><a href="javascript:;">WinDuctÂ® <i class="pg-arrow_minimize m-l-5"></i></a>
<nav class="classic "><span class="arrow"></span>
<ul>
<li><a href="winduct_overview.html">WinDuctÂ® Overview</a></li>
<li><a href="intumescent_coating.html">Intumescent Coating</a></li>
<li><a href="specifications.html">Specifications</a></li>
<li><a href="construction_details.html">Construction Details</a></li>
<li><a href="before_after_test.html">Before &amp; After Test</a></li>
<li><a href="test_report.html">Test Reports</a></li>
<li><a href="awards_recognitions.html">Awards &amp; Recognitions</a></li>
</ul>
</nav>
</li>
<li class="classic"><a href="javascript:;">Project References <i class="pg-arrow_minimize m-l-5"></i></a>
<nav class="classic "><span class="arrow"></span>
<ul>
<li><a href="project_references_local.html">Local</a></li>
<li><a href="project_references_oversea.html">Overseas</a></li>
<li><a href="project_highlights.html">Project Highlights</a></li>
</ul>
</nav>
</li>
<li class="classic"><a href="javascript:;">Highlights <i class="pg-arrow_minimize m-l-5"></i></a>
<nav class="classic "><span class="arrow"></span>
<ul>
<li><a href="winduct_highlights.html">WinDuctÂ® Highlights</a></li>
<li><a href="project_highlights.html">Project Highlights</a></li>
</ul>
</nav>
</li>
<li class=""><a href="contact.html">Contact Us</a></li>
</ul>
</div>
</div>
</div>
</nav>
<div class="page-wrappers">
<section class="jumbotron full-vh" data-pages="parallax">
<div class="inner full-height" style="transform: translateY(0px);">
<div class="swiper-container swiper-container-horizontal" id="hero">
<div class="swiper-wrapper" style="transition-duration: 0ms; transform: translate3d(0px, 0px, 0px);">
<div class="swiper-slide fit swiper-slide-active" style="width: 1903px;">
<div class="slider-wrapper">
<div class="background-wrapper" data-swiper-parallax="30%" style="transform: translate3d(0%, 0px, 0px); transition-duration: 0ms;">
<div class="background" data-pages-bg-image="images/winduct01.jpg" style='background-image: url("images/winduct01.jpg");'>
<div class="bg-overlay" style="opacity: 0;"></div>
</div>
</div>
</div>
<div class="content-layer">
<div class="inner full-height" style="transform: translateY(0px);">
<div class="container-xs-height full-height">
<div class="col-xs-height col-middle text-left">
<div class="container">
<div class="col-md-6 no-padding col-xs-10 col-xs-offset-1">
<h1 class="light sm-text-center" data-swiper-parallax="-15%" style="transform: translate3d(0%, 0px, 0px); transition-duration: 0ms;"><strong>2 Hours</strong> </h1>
<h4 class="sm-text-center">Fire Rated Smoke Extraction Ductwork System</h4></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="swiper-slide fit swiper-slide-next" style="width: 1903px;">
<div class="slider-wrapper">
<div class="background-wrapper" data-swiper-parallax="30%" style="transform: translate3d(-30%, 0px, 0px); transition-duration: 0ms;">
<div class="background" data-pages-bg-image="images/winduct02.jpg" draggable="false" style='background-image: url("images/winduct02.jpg");'>
<div class="bg-overlay" style="opacity: 0;"></div>
</div>
</div>
</div>
<div class="content-layer">
<div class="inner full-height" style="transform: translateY(0px);">
<div class="container-xs-height full-height">
<div class="col-xs-height col-middle text-left">
<div class="container">
<div class="col-md-6 no-padding col-xs-10 col-xs-offset-1">
<h1 class="light sm-text-center" data-swiper-parallax="-15%" style="transform: translate3d(15%, 0px, 0px); transition-duration: 0ms;"><strong>Your Design</strong></h1>
<h4 class="sm-text-center">Our Protection</h4></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="mouse-wrapper">
<div class="mouse">
<div class="mouse-scroll"></div>
</div>
</div>
<div class="swiper-navigation swiper-dark-solid swiper-button-prev auto-reveal swiper-button-disabled"></div>
<div class="swiper-navigation swiper-dark-solid swiper-button-next auto-reveal"></div>
</div>
</div>
</section>
<div class="front_page_background ">
<section class="front_thumbnails p-l-20 p-r-20">
<div class="col-sm-6 front_box shadow m-r-20 m-t-20 m-b-20">
<center>
<h2 class="p-b-5 welcome_title">WELCOME TO <strong>WINDUCT</strong></h2></center>
<p>Passive Fire Protection Sdn Bhd was established in 1996 and has since been actively involved in the Passive Fire Protection as well as Active Fire Protection system. Manned by a group of professional technical staff and ex-fire services officer who are expert and strong skilled in the construction industry, Passive Fire Protection Sdn Bhd represent a wide range of fire resistance products.</p>
</div>
<div class="col-sm-6 front_box shadow m-r-20 m-t-20 m-b-20">
<div>
<a href="http://winduct.com/video/Winduct_Animaution.mp4" target="_blank">
<center><img alt="" class="m-b-20 video_photo" src="images/winduct_video.png"/></center>
</a>
</div>
<a href="http://winduct.com/video/Winduct_Animaution.mp4" target="_blank">
<h3>LEADER IN QUALITY <br/>FIRE RATED DUCT</h3></a>
</div>
</section>
<section class="front_thumbnails p-l-20 p-r-20">
<div class="col-sm-4 front_box_01 shadow m-r-20 m-b-20">
<div class="video_icon">
<a href="winduct_overview.html">
<center><img alt="" class="m-b-20" src="images/winduct_overview.png"/></center>
</a>
</div>
<h6 class="block-title p-b-5"><strong>WinDuct</strong>Â® Overview</h6>
<p><strong>WinDuct</strong>Â® is a fire rated smoke extraction ductwork system tested to British Standard BS476 Part 24 (Duct A and Duct B) and Australian Standard AS1530.4 (Duct A and Duct B) for 120 minutes stability, integrity and insulation.</p><span class="font-arial small-text col-sm-9 no-padding button-text text-white"><a href="winduct_overview.html">Read More</a></span></div>
<div class="col-sm-4 front_box_01 shadow m-r-20 m-b-20">
<div class="video_icon">
<a href="awards_recognitions.html">
<center><img alt="" class="m-b-20" src="images/awards.png"/></center>
</a>
</div>
<h6 class="block-title p-b-5"><strong>Award &amp; Recognition</strong></h6>
<p><strong>WinDuct</strong>Â® is honored to be awarded and recognized by various agencies for its quality assurance and performance.</p><span class="font-arial small-text col-sm-9 no-padding button-text text-white"><a href="awards_recognitions.html">Read More</a></span></div>
<div class="col-sm-4 front_box_01 shadow m-r-20 m-b-20">
<div class="video_icon">
<a href="project_references_local.html">
<center><img alt="" class="m-b-20" src="images/project_ref.png"/></center>
</a>
</div>
<h6 class="block-title p-b-5"><strong>Project References</strong></h6>
<p><strong>WinDuct</strong>Â® System has been installed in commercial and government projects worldwide.</p><span class="font-arial small-text col-sm-9 no-padding button-text text-white"><a href="project_references_local.html">Read More</a></span></div>
</section>
</div>
<section class="p-b-55 p-t-75 xs-p-b-20 bg-master-darker">
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="row">
<div class="col-sm-4" style="float:right">
<a href="https://twitter.com/winductpfp" target="_blank"><img class="social_media m-l-10" src="images/twitter.png"/></a>
<a href="https://www.linkedin.com/in/winduct-passive-fire-protection-765040149/" target="_blank"><img class="social_media m-l-10" src="images/linkedin.png"/></a>
<a href="https://www.facebook.com/WinDuct-201330609883660/" target="_blank"><img class="social_media" src="images/facebook_icon.png"/></a>
</div>
</div>
</div>
<div class="col-sm-4 col-xs-12 xs-m-b-40">
<a href="index.html"><img alt="WinDuct Logo" class="alt" data-src-retina="images/logo_winduct.png" src="images/logo_winduct.png" width="152"/></a>
<h6 class="font-montserrat text-uppercase fs-14 text-white p-b-10" style="font-size: 18px !important;">Passive Fire Protection Sdn. Bhd.</h6>
<p class="text-white ">No 27, Jalan Pengacara U1/48, Section U1, Temasya Industrial Park, 40150 Shah Alam, Selangor Darul Ehsan,Malaysia.
                            <br/><strong>Tel:</strong> + (60)3-5569 5706 / 5707
                            <br/><strong>Fax:</strong> + (60)3-5569 5708
                            <br/><strong>Email: </strong> <a href="mailto:sales@winduct.com" target="_blank">sales@winduct.com</a> / <a href="mailto:winduct@gmail.com" target="_blank">winduct@gmail.com</a>
<br/><strong>Website:</strong> <a href="../index.html">www.winduct.com</a></p>
</div>
<div class="col-sm-2 col-xs-6 xs-m-b-20">
<h6 class="font-montserrat text-uppercase fs-14 text-white p-b-10">About Us </h6>
<ul class="no-style">
<li class="m-b-5 no-padding"><a class="link text-white " href="about_winduct.html">About WinDuctÂ®</a></li>
<li class="m-b-5 no-padding"><a class="link text-white" href="why_choose_winduct.html">Why Choose WinDuctÂ®</a></li>
<li class="m-b-5 no-padding"><a class="link text-white" href="winduct_quality.html">WinDuctÂ® Quality Assurance</a></li>
</ul>
</div>
<div class="col-sm-2 col-xs-6 xs-m-b-20">
<h6 class="font-montserrat text-uppercase fs-14 text-white p-b-10">WinDuctÂ®</h6>
<ul class="no-style">
<li class="m-b-5 no-padding"><a class="link text-white " href="winduct_overview.html">WinDuctÂ® Overview</a></li>
<li class="m-b-5 no-padding"><a class="link text-white " href="intumescent_coating.html">Intumescent Coating</a></li>
<li class="m-b-5 no-padding"><a class="link text-white " href="specifications.html">Specifications </a></li>
<li class="m-b-5 no-padding"><a class="link text-white" href="construction_details.html">Construction Details</a></li>
<li class="m-b-5 no-padding"><a class="link text-white" href="before_after_test.html">Before &amp; After Test</a></li>
<li class="m-b-5 no-padding"><a class="link text-white" href="test_report.html">Test Reports</a></li>
<li class="m-b-5 no-padding"><a class="link text-white" href="awards_recognitions.html">Awards &amp; Recognitions</a></li>
</ul>
</div>
<div class="col-sm-3 col-xs-6 xs-m-b-20">
<h6 class="font-montserrat text-uppercase fs-14 text-white p-b-10">Project References</h6>
<ul class="no-style">
<li class="m-b-5 no-padding"><a class="link text-white " href="project_references_local.html">Local</a></li>
<li class="m-b-5 no-padding"><a class="link text-white " href="project_references_oversea.html">Overseas</a></li>
</ul>
</div>
<div class="col-sm-4 col-xs-6 xs-m-b-20">
<h6 class="font-montserrat text-uppercase fs-14 text-white p-b-10"><a href="contact.html">Contact Us</a> </h6><img class="approved_bottom_logo" src="images/approved_by_bomba.png"/> <img class="brand_bottom_logo" src="images/malaysian_brand.png"/> <img class="ifc_bottom_logo" src="images/ifc.png"/> <img class="iso_bottom_logo" src="images/iso.png"/> <img class="responsible_bottom_logo" src="images/responsible_care.png"/> </div>
</div>
<p class="fs-12 hint-text p-t-10 text-white">Copyright Â© 2007 - 2017 by Passive Fire Protection Sdn. Bhd. - All Rights Reserved.</p>
</div>
</section>
<script src="js/google.js" type="text/javascript"></script>
<script src="js/pace.min.js" type="text/javascript"></script>
<script src="js/pages.image.loader.js" type="text/javascript"></script>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/swiper.jquery.min.js" type="text/javascript"></script>
<script src="js/velocity.min.js" type="text/javascript"></script>
<script src="js/velocity.ui.js" type="text/javascript"></script>
<script src="js/jquery.unveil.min.js" type="text/javascript"></script>
<script src="js/pages.frontend.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
</div></body>
</html>