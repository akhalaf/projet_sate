<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "25023",
      cRay: "610dcae2da5bd18b",
      cHash: "5f32dc213386c25",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYW5pbWVseXJpY3MuY29tL2pwb3Avc2FzdWtlL2FvaWJlbmNoaS5odG0=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "mg1Was7+1cu5NQ9fX5r8FfaD7/DzOenq8c14DIZTTU9eevWE08AgouveT8vs4PkMC2kmIIUQX1Aqptnvko6nfVA7a04klvql+TkoPdFk5uJTkgJT+Gha8EFlW9TLUOgywulb/1bke2HUhMJnTHmz7pM84gTlJAT5Q9eCNMVkraTuIWMO6RLGrzpuPQJDkJ16AMFkkrlEvRXS8JngC5KOmAQMDW5nJXAkEchy2bjaslvtACloGK4ZCFR6Tm9n3xtKZpSKBFVXJci2GVvQ7rwLqji8iM8HE0+Gw0YFLOFyukDKiQfpisyFSWgIpAWpcq8zxZh2895DAe6mO3OUafXZRo1jlQzYGpoe+vDM9JfNyOVyEihFYuMXX80l3wqTxHiTUaX/t3e4EwLt0UV3p5EijNVStLznwujrHGnANrA38iu3o4fn0c/HDdhRJFo/8A1+flf6/xZ7pPfTVoiTqpJj1zwClI82Q2o/kIYjPfN/4CidoDMyO1XWluaeB5aY65VWlIxUrf90XbXREjkFAPyinBwzzMQ+yLILQLQA7xxsscoHdeR/5NVtihphN+Rc4yepxzaoJ7LL52TKCjd2PxhkG8V/TAO3Hwn4gIf4bqxlJLUmmBcUcHgeOpQdSA3upQ4ONZT6qneM7/GOz+oQKm0WihnGttECt0FLdC8rA5D4qA/HIEF4qtraJfuK8w8zdwRhxNj86vsEG/lyd+GitKOqBUKXEQ3+GiFSK1SHthWQXOi2zHhyPywQW58YSow3XztO",
        t: "MTYxMDUyNzAxNy40MTcwMDA=",
        m: "iSU+Qozo4PsuKHn3SuTfYyG/RCpgGGWjbc7GpgIUaAI=",
        i1: "LJc7AknOsmRbTdunwwsi6Q==",
        i2: "fCf5mcv6LavKnNZqAQzeYg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "DDyL0IQNxmRdtTss4zAmYXiwHb+a6njG4Si8OlXEFGo=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.animelyrics.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/jpop/sasuke/aoibenchi.htm?__cf_chl_captcha_tk__=4b2f063b362d23c6fda35560a7808566356dcf6a-1610527017-0-Ab-2EFa9OlnBNgArXTg4BX2tiKktYennt3RD0MpifBZfq8XJSyDRoBIG9ker0bW-a1Y_5l0ZASAuk3cFqePExAVrgHCMs_qDAuAMmxfIA2ApeWX2SENds6ibRHcvJYrbqRmGoDB5d8p6MR8KcWKN44jwV_GwnTNsw8cDEhowf33Bsgf_8zxIc1OJwsp9xqNuShsiXld1sc9CLc1T7ndtahNARwJkJvcRiZjOOj2V80_YCnk2N2GcrLYq9z2TnFQhSTptiG744WcQFk79u4-aINIuJd9zMWktwfPAkoY4k-ZKxI2NROhWpnOqlQnUBNr_lQlGK1BbArt-5heyT0pTCsPkAWCazgqYd_YKD9ATurwFTPL0CzvAURd1pwuVtaY_Ii0Rjinjfz75yAdox2SRYtUS-wOI-ZL1DcUP75s9fcd11qhFz_bbPQoFFEM4yfoD94dZnyIeyFOdOhrSrm_oL0QVmRSP684dRo1g-vh9x36wN4zekah8r1Ixgi9sgIDW8Jgog2bf-E_k7WCfnoYFrhqQMveTowHXsGTKlvLw3jZY5N03Rd5ybQuWOQ7JE_P0yg" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="0cba70b5b99e83ee52c02b25d304319d67b734d9-1610527017-0-ASq/qON4JHw9s1if81rZ8Ugh6rHhbahHaswg/CJywEp+L6/2crmj4YE0kAe9RdiWk3nvK4AnXCGHEiToYqP+1nMBAFijvtrs+v9aDU/LbWmyA2v3wChu9dNCM/o3C+a6nSAhFMlUUbtFDkU6vFWlBe+193tQ+C+VsCbnRd7lzDUuYJd1ID6u6ton9oN7a9KuoVr0JSI/rAq4okfIUu2mI9avNVC/2j9/J+lErYudLMiexHEUakacPH819sR8hMK+0q6yshs9UDfUI6W4raJ6XPXg4ph8STzBZ0CYcq7X2TCik9B/TBkFbvlse6LI9tc6rscimU1GHHQAF0ilOraAd0KMO7q315z24OsW/Nplw9g/RhXn/Ub5RaOPel8OnRvW3M3qqR43cYLkbbvTciD/G5nufYmU59wLJAWB/uHmnJLAqD4nPzWW7wSaS+8Esw5y5d2MrzxchopubxMQIqEcprUasYNwYZBZ8K8A9g81rT7mtAwEVHJ3EXGk2vq7NpZrcFju47iluSJce+YMHBErsZ/9SXZ9g+s677AbzmQQQuQoFor85z1kzBTk/u23fPWNFq9zyV9IUA5t/mO1DIeXZJMiPKlxqDDjmSX1SKx/q0UEK8+n8URC/APKxT/Rb7vu5I2uVYx2UgbSF8i48BFnA6scM5pyXviYNwBk491im3iIG4F9jE3kRujB2kJTzrWXs4Xp6fnsUUaGNNwQSIpklrGOsV4AVZIDCaVM3z4FJj6bznhVYRbgE+wsdf3WjclshUMneU2nFJr8jpftE0IJp4ihiZ9cWReSiDrR/bB+CCn/VtYdskN8d4BQPktpADMwFELmtVdt6QTi7EgLFWJkoGhaUIdx0TLpAyuRqXazWGpmRygZ+oXulf6sreZg+TASkb8fXt0AfnDJpDf3E3ateMmN5+n7kaIIKi/RGL/DfGX6bZiIn8WWxs1cNj2L2zLefet02/YRL70Ioam+rMGksEl6+vOn7j9tcTbw9PWj17IpMs2TcEAcPtT1m8ii+yh7bDl+nngoM/ApR2gPBJNTtjP/hVGhQ/lPt5fFRJ7dgDnchbWtMN0HPxPPl/uBtZQj6af6uDEUslFuu9f8a0wVy+n6X8325dIJQimoCJrGYCedtbuwmzEgDvkrboqp6Sp78ERoFDtoKbnf1Gvk+MLBabRwlMr5Iviims5ezQTRYUe/UPLmvjhyraj2MdLVTVgY1T0LY2oaa07cr7kqciiKBp/tpFk3dXc0bkVA+3blE7EWZRZh+HXkN1v1O2E0NJhIRUnBONUCCF1xFQDSm2L0zJs="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="685555a16f7a9f8df71ea6d999bf1df6"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610dcae2da5bd18b')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610dcae2da5bd18b</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 210.75.253.169</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
