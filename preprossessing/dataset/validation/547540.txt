<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html class="js" dir="ltr" version="XHTML+RDFa 1.0" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/terms/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#"><head profile="http://www.w3.org/1999/xhtml/vocab"><script async="" src="js/ga.js" type="text/javascript"></script><script src="js/analytics.js" type="text/javascript"></script>
<script type="text/javascript">window.addEventListener('DOMContentLoaded',function(){var v=archive_analytics.values;v.service='wb';v.server_name='wwwb-app101.us.archive.org';v.server_ms=625;archive_analytics.send_pageview({});});</script><script charset="utf-8" src="js/ait-client-rewrite.js" type="text/javascript"></script>
<link href="css/banner-styles.css" rel="stylesheet" type="text/css"/>
<link href="css/iconochive.css" rel="stylesheet" type="text/css"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Drupal 7 (http://drupal.org)" name="generator"/>
<link href="http://feldnerproject.com/misc/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<meta content="LEEDS certified Santa Cruz, Sustainable building, Green building Santa Cruz, Office lease Santa Cruz, lease Green building Santa Cruz, Office Rental Santa Cruz" name="keywords"/>
<link href="http://feldnerproject.com/" rel="canonical"/>
<meta content="C3D5654DC2F84293449571BF5E9BD866" name="msvalidate.01"/>
<meta content="The Feldner Project, a new office building in Santa Cruz is available for lease. It features an excellent location, is a model for sustainable green building and will be LEEDS certified." name="description"/>
<title>Feldner Project |</title>
<style media="all" type="text/css">@import url("http://feldnerproject.com/css/system.base.css?p63tu2");
@import url("http://feldnerproject.com/system.menus.css?p63tu2");
@import url("http://feldnerproject.com/system.messages.css?p63tu2");
@import url("http://feldnerproject.com/system.theme.css?p63tu2");</style>
<style media="all" type="text/css">@import url("http://feldnerproject.com/css/date.css?p63tu2");
@import url("http://feldnerproject.com/css/datepicker.1.7.css?p63tu2");
@import url("http://feldnerproject.com/css/field.css?p63tu2");
@import url("http://feldnerproject.com/css/node.css?p63tu2");
@import url("http://feldnerproject.com/css/user.css?p63tu2");
@import url("http://feldnerproject.com/css/views.css?p63tu2");</style>
<style media="all" type="text/css">@import url("http://feldnerproject.com/css/ctools.css?p63tu2");
@import url("http://feldnerproject.com/css/panels.css?p63tu2");
@import url("http://feldnerproject.com/css/views_slideshow_cycle.css?p63tu2");
@import url("http://feldnerproject.com/css/twocol_stacked.css?p63tu2");
@import url("http://feldnerproject.com/css/ad57ff1546b4e3e493a0d16eb09e3ad9.css?p63tu2");
@import url("http://feldnerproject.com/css/flexible.css?p63tu2");
@import url("http://feldnerproject.com/css/bb76fbd2eb838ed16a95d7e77a27c17d.css?p63tu2");</style>
<style media="all" type="text/css">@import url("http://feldnerproject.com/css/reset.css?p63tu2");
@import url("http://feldnerproject.com/css/grid.css?p63tu2");
@import url("http://feldnerproject.com/css/precision.css?p63tu2");
@import url("http://feldnerproject.com/css/page.css?p63tu2");</style>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/jquery_002.js" type="text/javascript"></script>
<script src="js/drupal.js" type="text/javascript"></script>
<script src="js/panels.js" type="text/javascript"></script>
<script src="js/views_slideshow.js" type="text/javascript"></script>
<script src="js/jquery_003.js" type="text/javascript"></script>
<script src="js/views_slideshow_cycle.js" type="text/javascript"></script>
<script src="js/googleanalytics.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var _gaq = _gaq || [];_gaq.push(["_setAccount", "UA-28848632-1"]);_gaq.push(["_trackPageview"]);(function() {var ga = document.createElement("script");ga.type = "text/javascript";ga.async = true;ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ga, s);})();
//--><!]]>
</script>
<script src="js/scripts.js" type="text/javascript"></script>
<script src="js/jquery_004.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"wings","theme_token":"iDONNP7q49MR81dPI14kA8iXYp_JDkgfsBWvLjubhi8","js":{"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/contrib\/panels\/js\/panels.js":1,"sites\/all\/modules\/contrib\/views_slideshow\/js\/views_slideshow.js":1,"sites\/all\/libraries\/jquery.cycle\/jquery.cycle.all.min.js":1,"sites\/all\/modules\/contrib\/views_slideshow\/contrib\/views_slideshow_cycle\/js\/views_slideshow_cycle.js":1,"sites\/all\/modules\/contrib\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/themes\/custom\/wings\/js\/scripts.js":1,"sites\/all\/modules\/contrib\/views\/js\/jquery.ui.dialog.patch.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/contrib\/date\/date_api\/date.css":1,"sites\/all\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/modules\/contrib\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/contrib\/panels\/css\/panels.css":1,"sites\/all\/modules\/contrib\/views_slideshow\/contrib\/views_slideshow_cycle\/views_slideshow_cycle.css":1,"sites\/all\/modules\/contrib\/panels\/plugins\/layouts\/twocol_stacked\/twocol_stacked.css":1,"public:\/\/ctools\/css\/ad57ff1546b4e3e493a0d16eb09e3ad9.css":1,"sites\/all\/modules\/contrib\/panels\/plugins\/layouts\/flexible\/flexible.css":1,"public:\/\/ctools\/css\/bb76fbd2eb838ed16a95d7e77a27c17d.css":1,"sites\/all\/themes\/custom\/wings\/css\/reset.css":1,"sites\/all\/themes\/contrib\/precision\/css\/grid.css":1,"sites\/all\/themes\/contrib\/precision\/css\/precision.css":1,"sites\/all\/themes\/custom\/wings\/css\/page.css":1}},"viewsSlideshow":{"slideshow-panel_pane_1":{"methods":{"goToSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"nextSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"pause":["viewsSlideshowControls","viewsSlideshowCycle"],"play":["viewsSlideshowControls","viewsSlideshowCycle"],"previousSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"transitionBegin":["viewsSlideshowPager","viewsSlideshowSlideCounter"],"transitionEnd":[]},"paused":0}},"viewsSlideshowPagerFields":{"slideshow-panel_pane_1":{"bottom":{"activatePauseOnHover":1}}},"viewsSlideshowCycle":{"#views_slideshow_cycle_main_slideshow-panel_pane_1":{"num_divs":6,"id_prefix":"#views_slideshow_cycle_main_","div_prefix":"#views_slideshow_cycle_div_","vss_id":"slideshow-panel_pane_1","effect":"fade","transition_advanced":0,"timeout":5000,"speed":700,"delay":0,"sync":1,"random":0,"pause":1,"pause_on_click":0,"start_paused":0,"remember_slide":0,"remember_slide_days":1,"pause_when_hidden":0,"pause_when_hidden_type":"full","amount_allowed_visible":"","nowrap":0,"fixed_height":1,"items_per_slide":1,"wait_for_image_load":1,"cleartype":0,"cleartypenobg":0,"advanced_options":"{}","action_advanced":0}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls|xml|z|zip"}});
//--><!]]>
</script>
</head>
<body class="html front not-logged-in no-sidebars page-home">
<style type="text/css">
body {
  margin-top:0 !important;
  padding-top:0 !important;
  /*min-width:800px !important;*/
}
</style>
<div class="panel-flexible panels-flexible-1 clearfix" id="page-wrapper">
<div class="panel-flexible-inside panels-flexible-1-inside">
<div class="panels-flexible-row panels-flexible-row-1-1 panels-flexible-row-first clearfix header">
<div class="inside panels-flexible-row-inside panels-flexible-row-1-1-inside panels-flexible-row-inside-first clearfix">
<div class="panels-flexible-region panels-flexible-region-1-header panels-flexible-region-first panels-flexible-region-last page-header">
<div class="inside panels-flexible-region-inside panels-flexible-region-1-header-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-pane-header">
<div class="pane-content">
<a class="site-title" href="http://feldnerproject.com/" rel="home" title="Home">
<img alt="" class="site-logo" src="images/logo.png"/>
</a>
</div>
</div>
<div class="panel-pane pane-block pane-boxes-lease-button">
<div class="pane-content">
<div class="boxes-box" id="boxes-box-lease_button"><div class="boxes-box-content"><p> </p><p> </p><p> </p></div></div> </div>
</div>
</div>
</div>
</div>
</div>
<div class="panels-flexible-row panels-flexible-row-1-main-row clearfix main-content">
<div class="inside panels-flexible-row-inside panels-flexible-row-1-main-row-inside clearfix">
<div class="panels-flexible-region panels-flexible-region-1-content panels-flexible-region-first panels-flexible-region-last page-content">
<div class="inside panels-flexible-region-inside panels-flexible-region-1-content-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-block pane-system-main-menu main-menu">
<div class="pane-content">
<ul class="menu"><li class="first leaf active-trail"><a class="active-trail active" href="http://feldnerproject.com/index.html" title="">Home</a></li>
<li class="leaf"><a href="http://feldnerproject.com/vision.html" title="">Project Vision</a></li>
<li class="leaf"><a href="http://feldnerproject.com/features.html" title="">Features</a></li>
<li class="expanded"><a href="http://feldnerproject.com/team.html" title="">Professional Team</a><ul class="menu"><li class="first leaf"><a href="http://feldnerproject.com/team/dale.html" title="">Dale Tracy</a></li>
<li class="leaf"><a href="http://feldnerproject.com/team/frank.html" title="">Frank Phanton</a></li>
<li class="leaf"><a href="http://feldnerproject.com/team/dan.html" title="">Dan Satterthwaite</a></li>
<li class="leaf"><a href="http://feldnerproject.com/team/kimberly.html" title="">Kimberly Parrish</a></li>
<li class="last leaf"><a href="http://feldnerproject.com/team/professionals.html" title="">Project Professionals</a></li>
</ul></li>
<li class="leaf"><a href="http://feldnerproject.com/building-plans.html" title="">Building Plans</a></li>
<li class="leaf"><a href="http://feldnerproject.com/map.html" title="">Area Map</a></li>
<li class="leaf"><a href="http://feldnerproject.com/contact.html" title="">Contact</a></li>
<li class="last leaf"><a href="http://feldnerproject.com/amenities.html" title="">Amenities</a></li>
</ul> </div>
</div>
<div class="panel-pane pane-page-tabs">
<div class="pane-content">
<div id="tabs"></div> </div>
</div>
<div class="panel-pane pane-page-content">
<div class="pane-content">
<div class="block block-system" id="block-system-main">
<div class="content">
<div class="panel-2col-stacked clearfix panel-display" id="wings-2col-even">
<div class="panel-col-top panel-panel">
<div class="inside"><div class="panel-pane pane-views-panes pane-slideshow-panel-pane-1">
<div class="pane-content">
<div class="view view-slideshow view-id-slideshow view-display-id-panel_pane_1 wings-slideshow view-dom-id-1">
<div class="view-content">
<div class="skin-default">
<div class="views_slideshow_cycle_main views_slideshow_main viewsSlideshowCycle-processed" id="views_slideshow_cycle_main_slideshow-panel_pane_1"><div class="views-slideshow-cycle-main-frame views_slideshow_cycle_teaser_section" id="views_slideshow_cycle_teaser_section_slideshow-panel_pane_1" style="position: relative; width: 850px; height: 274px;">
<div class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-1 views-row-odd" id="views_slideshow_cycle_div_slideshow-panel_pane_1_0" style="position: absolute; top: 0px; left: 0px; display: none; z-index: 6; opacity: 0;">
<div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-first views-row-odd">
<div class="views-field views-field-field-image"> <div class="field-content"><img alt="" src="images/Feldner-exterior-from-NE_0_0.jpg" typeof="foaf:Image"/></div> </div>
<div class="views-field views-field-title"> <span class="field-content">The Feldner Project</span> </div>
<div class="views-field views-field-body"> <div class="field-content"><p>The New home of the New Bohemian Brewery!</p><p><a href="https://web.archive.org/web/20161024092340/http://www.nubobrew.com/" target="_blank">http://www.nubobrew.com/</a></p></div> </div></div>
</div>
<div class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-2 views_slideshow_cycle_hidden views-row-even" id="views_slideshow_cycle_div_slideshow-panel_pane_1_1" style="position: absolute; top: 0px; left: 0px; display: block; z-index: 7; opacity: 1; width: 850px; height: 274px;">
<div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-first views-row-odd">
<div class="views-field views-field-field-image"> <div class="field-content"><img alt="" src="images/Feldner_upstairs.jpg" typeof="foaf:Image"/></div> </div>
<div class="views-field views-field-title"> <span class="field-content">The Feldner Project</span> </div>
<div class="views-field views-field-body"> <div class="field-content"><p>Plenty of Natural light</p><p>Dynamic Views, Many Windows and 2 Balcony Decks</p></div> </div></div>
</div>
<div class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-3 views_slideshow_cycle_hidden views-row-odd" id="views_slideshow_cycle_div_slideshow-panel_pane_1_2" style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0;">
<div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-first views-row-odd">
<div class="views-field views-field-field-image"> <div class="field-content"><img alt="" src="images/night%2520view-534.jpg" typeof="foaf:Image"/></div> </div>
<div class="views-field views-field-title"> <span class="field-content">NUBO is in the Feldner Building!</span> </div>
<div class="views-field views-field-body"> <div class="field-content">The New Bohemia Brewing Company
www.nubobrew.com</div> </div></div>
</div>
<div class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-4 views_slideshow_cycle_hidden views-row-even" id="views_slideshow_cycle_div_slideshow-panel_pane_1_3" style="position: absolute; top: 0px; left: 0px; display: none; z-index: 3; opacity: 0;">
<div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-first views-row-odd">
<div class="views-field views-field-field-image"> <div class="field-content"><img alt="" src="images/upper%2520looking%2520north-534.jpg" typeof="foaf:Image"/></div> </div>
<div class="views-field views-field-title"> <span class="field-content">The Feldner Project ~ upstairs</span> </div>
<div class="views-field views-field-body"> <div class="field-content">

Upstairs was the site of

several fantastic local art shows....
</div> </div></div>
</div>
<div class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-5 views_slideshow_cycle_hidden views-row-odd" id="views_slideshow_cycle_div_slideshow-panel_pane_1_4" style="position: absolute; top: 0px; left: 0px; display: none; z-index: 2; opacity: 0;">
<div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-first views-row-odd">
<div class="views-field views-field-field-image"> <div class="field-content"><img alt="" src="images/Feldner-exterior-from-NE_0.jpg" typeof="foaf:Image"/></div> </div>
<div class="views-field views-field-title"> <span class="field-content">The Feldner Project </span> </div>
<div class="views-field views-field-body"> <div class="field-content"><p>The perfect place for a new Brewery!</p>
<p>NuBoBrew</p>
</div> </div></div>
</div>
<div class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-6 views_slideshow_cycle_hidden views-row-even" id="views_slideshow_cycle_div_slideshow-panel_pane_1_5" style="position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 0;">
<div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-first views-row-odd">
<div class="views-field views-field-field-image"> <div class="field-content"><img alt="" src="images/Feldner-roof2.jpg" typeof="foaf:Image"/></div> </div>
<div class="views-field views-field-title"> <span class="field-content">The Feldner Project</span> </div>
<div class="views-field views-field-body"> <div class="field-content">A Green Building

Natural Ventilation, Solar Hot Water Collector</div> </div></div>
</div>
</div>
</div>
<div class="views-slideshow-controls-bottom clearfix">
<div class="views-slideshow-pager-fields-render widget_pager widget_pager_bottom views_slideshow_pager_field views-slideshow-pager-field-processed" id="widget_pager_bottom_slideshow-panel_pane_1">
<div class="views-slideshow-pager-field-item views_slideshow_pager_field_item views_slideshow_active_pager_field_item views-row-odd" id="views_slideshow_pager_field_item_slideshow-panel_pane_1_0">
<div class="views-field-nothing">
<div class="views-content-nothing">
</div>
</div>
</div>
<div class="views-slideshow-pager-field-item views_slideshow_pager_field_item views-row-even" id="views_slideshow_pager_field_item_slideshow-panel_pane_1_1">
<div class="views-field-nothing">
<div class="views-content-nothing">
</div>
</div>
</div>
<div class="views-slideshow-pager-field-item views_slideshow_pager_field_item views-row-odd" id="views_slideshow_pager_field_item_slideshow-panel_pane_1_2">
<div class="views-field-nothing">
<div class="views-content-nothing">
</div>
</div>
</div>
<div class="views-slideshow-pager-field-item views_slideshow_pager_field_item views-row-even" id="views_slideshow_pager_field_item_slideshow-panel_pane_1_3">
<div class="views-field-nothing">
<div class="views-content-nothing">
</div>
</div>
</div>
<div class="views-slideshow-pager-field-item views_slideshow_pager_field_item views-row-odd" id="views_slideshow_pager_field_item_slideshow-panel_pane_1_4">
<div class="views-field-nothing">
<div class="views-content-nothing">
</div>
</div>
</div>
<div class="views-slideshow-pager-field-item views_slideshow_pager_field_item views-row-even" id="views_slideshow_pager_field_item_slideshow-panel_pane_1_5">
<div class="views-field-nothing">
<div class="views-content-nothing">
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div> </div>
</div>
</div>
</div>
<div class="center-wrapper">
<div class="panel-col-first panel-panel">
<div class="inside"><div class="panel-pane pane-block pane-boxes-canvas-for-community wings-grey">
<h2 class="pane-title">Amenities &amp; Special Features</h2>
<div class="pane-content">
<div class="boxes-box" id="boxes-box-canvas_for_community"><div class="boxes-box-content"><ul><li>23,000 cars pass here everyday</li><li>Most desirable Retail/Commercial area in Santa Cruz County</li><li>Design
 of the building and grounds puts its emphasis on increasing 
productivity and collaboration while attracting new customers and 
retaining talent.</li><li>Location:  a thriving urban retail 
corridor, blocks from the Monterey Bay in Pleasure Point, Santa  
Cruz, with easy access to bus lines, many restaurants and retail shop</li><li>Pending LEED Gold Certification by the U.S. Green Building Council</li><li>Attractive, Ample Display Windows</li><li>3,200 Square feet of mostly open space</li><li>Two accessible bathrooms with full showers</li><li>12 parking spaces with easy access.   Bicycle parking</li><li>Active Solar Hot Water Collector:  preheats water for showers and hydronic radiant floor space heating system</li><li>Light
 colored roof:  reduces urban âheat island effectâ by reflecting 
rather than absorbing heat  from sun.  Steel roof is recycled,
 recyclable, and durable.</li><li>All landscaping uses minimal water, 
composed of plants appropriate for use in a dry-summer,Mediterranean 
climate. Water fixtures are the lowest flow available.</li><li>Dynamic views from most areas of the building, including many windows and 2 balcony decks</li></ul></div></div> </div>
</div>
</div>
</div>
<div class="panel-col-last panel-panel">
<div class="inside"><div class="panel-pane pane-block pane-boxes-so-cool wings-grey">
<h2 class="pane-title">What's so cool about this building</h2>
<div class="pane-content">
<div class="boxes-box" id="boxes-box-so_cool"><div class="boxes-box-content"><ul><li>Location, Location, Location!</li><li>Easily Adaptable to your Needs</li><li>Many Display Windows</li><li>Great Sign Space for your Logo</li><li>Ample Parking with Easy Access</li><li>Dynamic Building will Inspire, Motivate, &amp; Stimulate Great Thought and Productivity</li></ul><p> </p><p> </p><p> </p><p> <img alt="upstairs in the Feldner Building" height="312" src="images/Feldner-upstairs2.jpg" width="259"/></p><table border="0" style="width: 900px; height: 12px;"><tbody><tr><td style="width: 50%;" valign="top"> </td><td style="width: 50%;" valign="top"> </td></tr></tbody></table></div></div> </div>
</div>
<div class="panel-pane pane-block pane-boxes-leads-info wings-blue wings-blue-title wings-leads">
<div class="pane-content">
<div class="boxes-box" id="boxes-box-leads_info"><div class="boxes-box-content"><p><img alt="Leads" height="60" src="images/leads.png" style="float: left; margin-left: 5px; margin-right: 5px;" title="Leads" width="61"/>What is the Leeds Certification about and why should you care? Go to the official Leeds site to <a href="http://www.usgbc.org/" target="_blank">LEARN MORE &gt;</a></p></div></div> </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="panels-flexible-row panels-flexible-row-1-2 clearfix ">
<div class="inside panels-flexible-row-inside panels-flexible-row-1-2-inside clearfix">
<div class="panels-flexible-region panels-flexible-region-1-footer panels-flexible-region-first panels-flexible-region-last page-footer">
<div class="inside panels-flexible-region-inside panels-flexible-region-1-footer-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-block pane-boxes-foot-vision wings-foot">
<h2 class="pane-title">Vision</h2>
<div class="pane-content">
<div class="boxes-box" id="boxes-box-foot_vision"><div class="boxes-box-content"><p>In
 short, the vision is to create a building that uses the latest 
available technology to provide an educational experience in an 
aesthetically pleasing space.  A structure that will stand the test
 of time in a harsh environment and inspire future generations.  To
 show the beauty that can be achieved through the practical.</p><p>Mr. 
Feldner has been much more than a Realtor and landlord for most of his 
life.  In college, he aspired to be a professor of Industrial 
Arts.  He has always given people well maintained, up to date, and 
healthy spaces as a positive-quality of life issue.  He is an 
extremely well traveled man and had the good fortune of making the 
acquaintance of Joseph Eichler and Claude Oakland during the development
 of the Los Altos area.  He is a man who knows the value of quality
 and is a person who is inwardly driven to do what is right simply 
because it is right...</p><p><a href="http://feldnerproject.com/vision" title="The Vision">the vision &gt;</a></p></div></div> </div>
</div>
<div class="panel-pane pane-block pane-boxes-foot-features wings-foot">
<h2 class="pane-title">Features</h2>
<div class="pane-content">
<div class="boxes-box" id="boxes-box-foot_features"><div class="boxes-box-content"><ul><li>Carbon-Neutral Concrete</li><li>Light Colored Concrete</li><li>Permeable Paving</li><li>Light Colored Roof</li><li>Reduced Water Use</li><li>Light Colored Concrete</li><li>Lowest Flow Water Fixtures</li><li>Photovoltaic Panels</li><li>Charging Station</li><li>Active Solar Hot Water Collector</li><li>Optimized Energy Performance</li><li>Low Wattage Lighting Throughout</li><li>Minimal Construction Waste</li><li>High Recycled Content</li><li>Minimal Maintenance Materials</li><li>Natural Ventilation</li><li>Efficient Mechanical Ventilation</li><li>Low-Emitting Materials</li><li>Thermal Comfort</li><li>Dynamic Views</li></ul><p> </p><p><a href="http://feldnerproject.com/features" title="Features">features &gt;</a></p></div></div> </div>
</div>
<div class="panel-pane pane-block pane-boxes-foot-team wings-foot">
<h2 class="pane-title">Team</h2>
<div class="pane-content">
<div class="boxes-box" id="boxes-box-foot_team"><div class="boxes-box-content"><ul><li>Dale Tracy</li><li>Frank Phanton</li><li>Bob Dumont</li><li>Luke Beautz</li><li>Joe Rafferty</li><li>Brad Steeter</li><li>Pat Splitt</li><li>Sean Ring</li><li>Ruth Stiles</li><li>Lynette Sergius</li><li>Gary Erickson</li></ul><p> </p><p><a href="http://feldnerproject.com/team" title="Professional Team">professional team &gt;</a></p></div></div> </div>
</div>
</div>
</div>
</div>
</div>
<div class="panels-flexible-row panels-flexible-row-1-3 panels-flexible-row-last clearfix ">
<div class="inside panels-flexible-row-inside panels-flexible-row-1-3-inside panels-flexible-row-inside-last clearfix">
<div class="panels-flexible-region panels-flexible-region-1-ending panels-flexible-region-first panels-flexible-region-last page-ending">
<div class="inside panels-flexible-region-inside panels-flexible-region-1-ending-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-block pane-boxes-foot-menu wings-foot-menu">
<div class="pane-content">
<div class="boxes-box" id="boxes-box-foot_menu"><div class="boxes-box-content"><ul><li class="first"><a href="http://maps.google.com/?ie=UTF8&amp;ll=36.966611,-121.964622&amp;spn=0.010372,0.026157&amp;z=16&amp;vpsrc=0&amp;source=embed" target="_blank" title="Google Map It!">Google Map It!</a></li><li><a href="http://feldnerproject.com/building-plans.html" title="Building Plans">Building Plans</a></li><li><a href="http://www.usgbc.org/DisplayPage.aspx?CMSPageID=1988" target="_blank" title="LEEDS Certification Information">LEEDS Certification</a></li><li><a href="http://feldnerproject.com/leasing.html" title="Leasing Information">Leasing Information</a></li><li><a href="http://feldnerproject.com/contact.html" title="Contact">Contact</a></li></ul></div></div> </div>
</div>
<div class="panel-pane pane-block pane-boxes-foot-message wings-foot-message">
<div class="pane-content">
<div class="boxes-box" id="boxes-box-foot_message"><div class="boxes-box-content"><p>Â© the feldner project 2011 all rights reserved. Website design and Drupal development by <a href="http://www.parrisharts.com/" target="_blank" title="Parrish Art &amp; Design">Parrish Art &amp; Design.</a></p></div></div> </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</body></html>
<!--
     FILE ARCHIVED ON 12:20:29 Aug 26, 2018 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 01:12:48 Jan 05, 2020.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
