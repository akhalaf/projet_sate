<!DOCTYPE html>
<html lang="en-us">
<head>
<title>Joe Rosselli Baseball Stats by Baseball Almanac</title>
<meta charset="utf-8"/>
<meta content="joe rosselli Joe Rosselli Stats stats baseball pitching hitting fielding baserunning career biography data lifetime yearly year by year team single season total" name="keywords"/>
<meta content="Joe Rosselli baseball stats with batting stats, pitching stats and fielding stats, along with uniform numbers, salaries, quotes, career stats and biographical data presented by Baseball Almanac." name="description"/>
<meta content="index, follow" name="robots"/>
<meta content="Baseball Almanac, Inc." name="Author"/>
<meta content="https://www.baseball-almanac.com/players/player.php?p=rossejo01" property="og:url"/>
<meta content="article" property="og:type"/>
<meta content="Joe Rosselli Baseball Stats | Baseball Almanac" property="og:title"/>
<meta content="Joe Rosselli baseball stats, biographical data, history, trivia, and research by Baseball Almanac." property="og:description"/>
<meta content="https://www.baseball-almanac.com/images/SocialMedia/Baseball_Almanac_MLB_Players.jpg" property="og:image"/>
<meta content="image/jpeg" property="og:image:type"/>
<meta content="1200" property="og:image:width"/>
<meta content="625" property="og:image:height"/>
<meta content="Joe Rosselli Baseball Stats by Baseball Almanac" property="og:image:alt"/>
<!-- Mobile viewport -->
<meta content="width=device-width; initial-scale=1.0" name="viewport"/>
<link href="/css/styles1.1.css" rel="stylesheet"/>
<link href="/css/menu.css" rel="stylesheet"/>
<style><!--  --></style>
<!-- BEGIN GOOGLE ANALYTICS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-1805063-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-1805063-1');
</script>
<!-- END GOOGLE ANALYTICS -->
</head>
<body>
<!-- BEGIN GOOGLE ADSENSE -->
<div class="google-adsense" style="text-align: center; margin: 10px 0">
<script type="text/javascript">
    google_ad_client = "ca-pub-7150597730414049";
    google_ad_slot = "0964786609";
    google_ad_width = 728;
    google_ad_height = 90;
</script>
<script src="https://pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript"></script>
</div>
<!-- END GOOGLE ADSENSE -->
<div id="wrapper">
<div class="header-container">
<!-- START _site-header-v2.html -->
<div class="header">
<div class="container">
<a class="flex-none" href="/">
<span class="hidden">Baseball Almanac</span>
<img alt="Baseball Almanac" class="logo" src="/images/baseball-almanac-logo.png"/>
</a>
<div class="menu-items hidden" data-menu="">
<div>
<a data-flip="" data-toggle="menu-1" href="#">
                    History
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-1="">
<li><a href="/asgmenu.shtml">All-Star Game</a></li>
<li><a href="/League_Championship_Series.shtml">A.L.C.S. &amp; N.L.C.S.</a></li>
<li><a href="/me_award.shtml">Awards</a></li>
<li><a href="/stadium.shtml">Ballparks</a></li>
<li><a href="/college/colleges.shtml">College Baseball</a></li>
<li><a href="/division_series/division_series.shtml">Division Series</a></li>
<li><a href="/draft/baseball_draft.shtml">Draft</a></li>
<li><a href="/mgrmenu.shtml">Managers</a></li>
<li><a href="/opening_day/opening_day.shtml">Opening Day</a></li>
<li><a href="/teammenu.shtml">Team by Team</a></li>
<li><a href="/umpiresmenu.shtml">Umpires</a></li>
<li><a href="/wild_card/MLB_Wild_Card_Game.shtml">Wild Card Game</a></li>
<li><a href="/ws/wsmenu.shtml">World Series</a></li>
<li><a href="/yearmenu.shtml">Year by Year</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-2" href="#">
                    Players
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-2="">
<li><a href="/fammenu.shtml">Baseball Families</a></li>
<li><a href="/players/baseball_biographies.shtml">Biographies</a></li>
<li><a href="/players/birthplace.php">Birthplace Analysis</a></li>
<li><a href="/featmenu.shtml">Fabulous Feats</a></li>
<li><a href="/frstmenu.shtml">Famous Firsts</a></li>
<li><a href="/graves/baseball_graves.shtml">Grave Sites</a></li>
<li><a href="/hofmenu.shtml">Hall of Fame</a></li>
<li><a href="/players/baseball_interviews.shtml">Interviews</a></li>
<li><a href="/deaths/chart/baseball_player_obituaries.shtml">Obituaries</a></li>
<li><a href="/players/deathplace.php">Place of Death Analysis</a></li>
<li><a href="/players/ballplayer.shtml">The Ballplayers</a></li>
<li><a href="/quomenu.shtml">Quotes</a></li>
<li><a href="/players/baseball_births.php">Year of Birth Analysis</a></li>
<li><a href="/players/baseball_deaths.php">Year of Death Analysis</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-3" href="#">
                    Leaders
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-3="">
<li><a href="/baseball_attendance.shtml">Attendance Data</a></li>
<li><a href="/himenu.shtml">Hitting Charts</a></li>
<li><a href="/pimenu.shtml">Pitching Charts</a></li>
<li><a href="/rb_menu.shtml">Record Books</a></li>
<li><a href="/teamstats/statmaster.php">Statmaster</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-4" href="#">
                    Left Field
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-4="">
<li><a href="/players/Oldest_Living_Baseball_Players.php">500 Oldest Players</a></li>
<li><a href="/automenu.shtml">Autographs</a></li>
<li><a href="/baseball_cards/baseball_cards.php">Baseball Cards</a></li>
<li><a href="/charts/baseball_charts.shtml">Baseball Charts</a></li>
<li><a href="/limenu.shtml">Baseball Lists</a></li>
<li><a href="/bookmenu.shtml">Book Shelf</a></li>
<li><a href="/players/Cups_of_Coffee.php">Cups of Coffee</a></li>
<li><a href="/gam_menu.shtml">Fun &amp; Games</a></li>
<li><a href="/humomenu.shtml">Humor &amp; Jokes</a></li>
<li><a href="/mve_time.shtml">Movie Time</a></li>
<li><a href="/baseball_news.shtml">News Feeds</a></li>
<li><a href="/poems.shtml">Poetry &amp; Song</a></li>
<li><a href="/articles/articles.shtml">Research Articles</a></li>
<li><a href="/baseball_uniform_numbers.shtml">Uniform Numbers</a></li>
<li><a href="/prz_menu.shtml">U.S. Presidents</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-5" href="#">
                    Help
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-5="">
<li><a href="/about.shtml">Advertising</a></li>
<li><a href="/blog/">Blog</a></li>
<li><a href="/feedmenu.shtml">Feedback</a></li>
<li><a href="/mlmstart.shtml">Newsletter</a></li>
<li><a href="/rulemenu.shtml">Rules</a></li>
<li><a href="/scoring.shtml">Scoring</a></li>
<li><a href="/Search.shtml">Search &amp; Find</a></li>
<li><a href="/bstatmen.shtml">Stats 101</a></li>
</ul>
</div>
<div class="extra">
<a class="bg-blue-500" href="https://twitter.com/BaseballAlmanac" target="_blank">
<i class="fab fa-twitter"></i>
                    Follow @BaseballAlmanac
                </a>
<a class="bg-blue-700" href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank">
<i class="fab fa-facebook"></i>
                    Find us on Facebook
                </a>
</div>
</div>
<div class="overlay hidden" data-menu="" data-proxy="menu"></div>
<form class="search-form">
<div class="social">
<div data-search="">
<a href="https://twitter.com/BaseballAlmanac" target="_blank"><i class="fab fa-twitter"></i></a>
<a href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank"><i class="fab fa-facebook-f"></i></a>
<a href="https://www.instagram.com/baseballalmanac/" target="_blank"><i class="fab fa-instagram"></i></a>
<span></span>
</div>
<input class="search hidden" data-search="" data-toggle-focus="" id="q" name="q" placeholder="Custom Search" type="text"/>
<a data-toggle="search" data-toggle-bg=""><i class="fas fa-search"></i></a>
<a class="" data-toggle="menu" data-toggle-bg="" data-toggle-scroll=""><i class="fas fa-bars"></i></a>
</div>
</form>
<script>
            // JavaScript code that should follow the search box code (must part)
  			// add a listener for form submission, i.e. when user hits Enter or clicks to any submit button form has
  			document.querySelector('.search-form').addEventListener('submit', function(e) {
    			// do not actually submit the form, we'll do something else :)
    			e.preventDefault();
    			// read the search query for input tag, i.e. user searches for "django" let's say
    			var q = document.querySelector('input[name="q"]').value;
    			// just proceed if user has typed something
    			if (q.length > 0) {
      				// go to search results page which is search.html here but can be anything you like with "gsc.q" hash parameter equal to search query
      				window.open('/custom_search.shtml?q=' + q, '_self');
    			}
  			});

			// check if there is any text in the search string
			if (window.location.search.length > 0) {
  				// retrieve the "q" keyed search string value
  				var q = window.location.search.substring(1).split('&').filter(function(x) {
    				return x.substring(0, 2) === 'q=';
  				})[0].substring(2);
  				// put the value to the search box if it is not empty
  				if (q.length > 0) {
    				document.querySelector('input[name="q"]').value = q;
  				}
			}
  		</script>
</div>
</div>
<script src="/js/navigation.min.js" type="text/javascript"></script>
<!-- END _site-header-v2.html -->
</div>
<div class="container">
<div class="intro">
<h1>Joe Rosselli Stats</h1>
<p>Joe Rosselli was born on Sunday, May 28, 1972, in Burbank, California. Rosselli was 22 years old when he broke into the big leagues on April 30, 1995, with the San Francisco Giants. His biographical data, year-by-year hitting stats, fielding stats, pitching stats (where applicable), career totals, uniform numbers, salary data and miscellaneous items-of-interest are presented by Baseball Almanac on this comprehensive Joe Rosselli baseball stats page.</p>
<!-- Are we using flycast?
 -->
</div>
<!-- 	End Intro Box -->
<!-- 	Begin Quote Box -->
<div class="topquote">
<img alt="Baseball Almanac Top Quote" src="/images/typewriter-with-paper.png"/>
<p>"Do you have a favorite quote from or about this particular <a href="/players/ballplayer.shtml">Major League Baseball player</a> that you would like to see here?  If so, please <a href="/feedmenu.shtml">send it to us</a>, and we'll update this page immediately." - <a href="/index.shtml">Baseball Almanac</a></p>
</div>
<!-- 	End Quote Box -->
<!-- Begin Sponsor Box -->
<div class="sponsor-box">
<div class="s2nPlayer k-INqKbLx9" data-type="float"></div><script data-type="s2nScript" src="//embed.sendtonews.com/player3/embedcode.js?fk=INqKbLx9&amp;cid=8557&amp;offsetx=0&amp;offsety=0&amp;floatwidth=400&amp;floatposition=bottom-right" type="text/javascript"></script>
</div>
<!-- End Sponsor Box -->
<!-- Begin Page Body -->
<!-- Personal Statistics -->
<!---BEGIN-PF--->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header" colspan="2"><h2>Joe Rosselli</h2><img alt="Joe Rosselli Autograph on a 1994 Fleer Ultra (#593)" src="/players/pics/joe_rosselli_autograph.jpg" style="max-width: 212px; margin: 0 auto; border: solid 1px black"/><p class="grey">Joe Rosselli Autograph on a 1994 Fleer Ultra (#593)</p></td>
</tr>
<tr>
<td class="datacolBox" colspan="2"><table class="post-nav">
<tr>
<td class="postnavon">Career</td>
<td class="postnavoff">All-Star</td>
<td class="postnavoff">Wild Card</td>
<td class="postnavoff">Division</td>
<td class="postnavoff">LCS</td>
<td class="postnavoff">World Series</td>
<td class="postnavoff"><a href="trades.php?p=rossejo01">Trades</a></td>
<td class="postnavoff">Awards</td>
<td class="postnavoff">Videos</td>
<td class="postnavoff"><a href="cards.php?p=rossejo01">Cards</a></td>
<td class="postnavoff"><a alt="Joseph Donald Rosselli on MLB.com" href="http://mlb.com/team/player.jsp?player_id=121482"><img alt="mlb" src="/images/mlb_tiny.gif" style="max-width: 20px; margin: 0 auto; display: inline-block; vertical-align:middle;"/></a> <a alt="Joseph Donald Rosselli on ESPN.com" href="http://www.espn.com/mlb/player/_/id/3207"><img alt="espn" src="/images/espn_tiny.png" style="max-width: 20px; margin: 0 auto; display: inline-block; vertical-align:middle;"/></a></td></tr>
</table></td>
</tr>
<tr>
<td class="banner" colspan="2">Biographical Data</td>
</tr>
<tr>
<td class="datacolBox" width="50%">
<table>
<tr>
<td class="biocolpad">Birth Name:</td>
<td class="biocolpad">  Joseph Donald Rosselli</td>
</tr>
<tr>
<td class="biocolpad">Nickname:</td>
<td class="biocolpad">  Joe</td>
</tr>
<tr>
<td class="biocolpad">Born On:</td>
<td class="biocolpad">  05-28-1972  (Gemini)</td>
</tr>
<tr>
<td class="biocolpad"><a href="birthplace.php"><img alt="Place of Birth Data" src="/charts/barchart.gif" style="max-width: 12px; display: inline; vertical-align: top"/></a> Born In:</td>
<td class="biocolpad">  Burbank, California</td>
</tr>
<tr>
<td class="biocolpad"><a href="baseball_deaths.php"><img alt="Year of Death Data" src="/charts/barchart.gif" style="max-width: 12px; display: inline; vertical-align: top"/></a> Died On:</td>
<td class="biocolpad">  Still Living (<a href="/players/Oldest_Living_Baseball_Players.php" title="The 500 Oldest Living Major League Baseball Players">500 Oldest Living</a>)</td>
</tr>
<tr>
<td class="biocolpad"><a href="deathplace.php"><img alt="Place of Death Data" src="/charts/barchart.gif" style="max-width: 12px; display: inline; vertical-align: top"/></a> Died In:</td>
<td class="biocolpad">  Still Living</td>
</tr>
<tr>
<td class="biocolpad">Cemetery:</td>
<td class="biocolpad">  n/a</td>
</tr>
</table>
</td>
<td class="datacolBox" width="50%">
<table>
<tr>
<td class="biocolpad">High School:</td>
<td class="biocolpad" colspan="4">  Bishop Alemany High School (Mission Hills, CA)</td>
</tr>
<tr>
<td class="biocolpad">College:</td>
<td class="biocolpad" colspan="4">  None Attended</td>
</tr>
<tr>
<td class="biocolpad"><a href="/charts/bats/bats.shtml"><img alt="Batting Stances Chart" src="/charts/barchart.gif" style="max-width: 12px; display: inline; vertical-align: top"/></a> Bats:</td>
<td class="biocolpad">  Right</td>
<td width="20"> </td>
<td class="biocolpad"><a href="/charts/throws/throws.shtml"><img alt="Throwing Arms Chart" src="/charts/barchart.gif" style="max-width: 12px; display: inline; vertical-align: top"/></a> Throws:</td>
<td class="biocolpad">  Left</td>
</tr>
<tr>
<td class="biocolpad"><a href="/charts/heights/heights.shtml"><img alt="Player Height Chart" src="/charts/barchart.gif" style="max-width: 12px; display: inline; vertical-align: top"/></a> Height:</td>
<td class="biocolpad">  6-01</td>
<td> </td>
<td class="biocolpad"><a href="/charts/weights/weights.shtml"><img alt="Player Weight Chart" src="/charts/barchart.gif" style="max-width: 12px; display: inline; vertical-align: top"/></a> Weight:</td>
<td class="biocolpad">  170</td>
</tr>
<tr>
<td class="biocolpad">First Game:</td>
<td class="biocolpad" colspan="4">  <a href="../box-scores/boxscore.php?boxid=199504300SFN">04-30-1995</a> (Age 22)</td>
</tr>
<tr>
<td class="biocolpad">Last Game:</td>
<td class="biocolpad" colspan="4">  10-01-1995</td>
</tr>
<tr>
<td class="biocolpad">Draft:</td>
<td class="biocolpad" colspan="4">  <a href="/draft/baseball-draft.php?yr=1990">1990</a> : 2nd Round (70th)</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!-- Pitching Statistics -->
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header" colspan="26"><h2>Joe Rosselli</h2><p>Joe Rosselli  Pitching Stats</p></td>
</tr>
<tr>
<td class="banner">Year</td>
<td class="banner">Age</td>
<td class="banner">Team</td>
<td class="banner right">G</td>
<td class="banner right">GS</td>
<td class="banner right">GF</td>
<td class="banner right">W</td>
<td class="banner right">L</td>
<td class="banner right">PCT</td>
<td class="banner right">ERA</td>
<td class="banner right">CG</td>
<td class="banner right">SHO</td>
<td class="banner right">SV</td>
<td class="banner right">IP</td>
<td class="banner right">BFP</td>
<td class="banner right">H</td>
<td class="banner right">ER</td>
<td class="banner right">R</td>
<td class="banner right">HR</td>
<td class="banner right">BB</td>
<td class="banner right">IBB</td>
<td class="banner right">SO</td>
<td class="banner right">WP</td>
<td class="banner right">HB</td>
<td class="banner right">BK</td>
<td class="banner right">HLD</td>
</tr>
<tr>
<td class="datacolBox"><a href="../yearly/yr1995n.shtml" title="Show Year in Review">1995</a></td>
<td class="datacolBox">23</td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1995&amp;t=SFN" title="Show Team Roster">Giants</a></td>
<td class="datacolBox right"><a href="pitchinglogs.php?p=rossejo01&amp;y=1995" title="Show Pitching Logs">9</a></td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">2</td>
<!-- td class='datacolBox' ><a title="Show Team Roster" href="../teamstats/roster.php?y=1995&t=SFN">Giants</a></td>
<td class='datacolBox right'>2</td -->
<td class="datacolBox right">1</td>
<td class="datacolBox right">.667</td>
<td class="datacolBox right">8.70</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">30.0</td>
<td class="datacolBox right">140</td>
<td class="datacolBox right">39</td>
<td class="datacolBox right">29</td>
<td class="datacolBox right">29</td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">20</td>
<td class="datacolBox right">2</td>
<td class="datacolBox right">7</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">1</td>
<td class="datacolBox right">-</td>
</tr>
<tr>
<td class="banner center" colspan="3">Career</td>
<td class="banner right">G</td>
<td class="banner right">GS</td>
<td class="banner right">GF</td>
<td class="banner right">W</td>
<td class="banner right">L</td>
<td class="banner right">PCT</td>
<td class="banner right">ERA</td>
<td class="banner right">CG</td>
<td class="banner right">SHO</td>
<td class="banner right">SV</td>
<td class="banner right">IP</td>
<td class="banner right">BFP</td>
<td class="banner right">H</td>
<td class="banner right">ER</td>
<td class="banner right">R</td>
<td class="banner right">HR</td>
<td class="banner right">BB</td>
<td class="banner right">IBB</td>
<td class="banner right">SO</td>
<td class="banner right">WP</td>
<td class="banner right">HB</td>
<td class="banner right">BK</td>
<td class="banner right">HLD</td>
</tr>
<tr>
<td class="datacolBox center" colspan="3"> 1 Year</td>
<td class="datacolBox right">9</td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">2</td>
<td class="datacolBox right">1</td>
<td class="datacolBox right">.667</td>
<td class="datacolBox right">8.70</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">30.0</td>
<td class="datacolBox right">140</td>
<td class="datacolBox right">39</td>
<td class="datacolBox right">29</td>
<td class="datacolBox right">29</td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">20</td>
<td class="datacolBox right">2</td>
<td class="datacolBox right">7</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">1</td>
<td class="datacolBox right">-</td>
</tr>
</table>
</div>
<!-- Hitting Statistics / Batting Statistics -->
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header" colspan="22"><h2>Joe Rosselli</h2><p>Joe Rosselli  Hitting Stats</p></td>
</tr>
<tr>
<td class="banner">Yr</td>
<td class="banner">Age</td>
<td class="banner">Team</td>
<td class="banner right">G</td>
<td class="banner right">AB</td>
<td class="banner right">R</td>
<td class="banner right">H</td>
<td class="banner right">2B</td>
<td class="banner right">3B</td>
<td class="banner right">HR</td>
<td class="banner right">GRSL</td>
<td class="banner right">RBI</td>
<td class="banner right">BB</td>
<td class="banner right">IBB</td>
<td class="banner right">SO</td>
<td class="banner right">SH</td>
<td class="banner right">SF</td>
<td class="banner right">HBP</td>
<td class="banner right">GIDP</td>
<td class="banner right">AVG</td>
<td class="banner right">OBP</td>
<td class="banner right">SLG</td>
</tr>
<tr>
<td class="datacolBox"><a href="../yearly/yr1995n.shtml" title="Show Year in Review">1995</a></td>
<td class="datacolBox">23</td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1995&amp;t=SFN" title="Show Team Roster">Giants</a></td>
<td class="datacolBox right"><a href="hittinglogs.php?p=rossejo01&amp;y=1995" title="Show Hitting Logs">9</a></td>
<td class="datacolBox right">10</td>
<td class="datacolBox right">1</td>
<td class="datacolBox right">2</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">1</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">3</td>
<td class="datacolBox right">1</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">.200</td>
<td class="datacolBox right">.200</td>
<td class="datacolBox right">.200</td>
</tr>
<tr>
<td class="banner center" colspan="3">Career</td>
<td class="banner right">G</td>
<td class="banner right">AB</td>
<td class="banner right">R</td>
<td class="banner right">H</td>
<td class="banner right">2B</td>
<td class="banner right">3B</td>
<td class="banner right">HR</td>
<td class="banner right">GRSL</td>
<td class="banner right">RBI</td>
<td class="banner right">BB</td>
<td class="banner right">IBB</td>
<td class="banner right">SO</td>
<td class="banner right">SH</td>
<td class="banner right">SF</td>
<td class="banner right">HBP</td>
<td class="banner right">GIDP</td>
<td class="banner right">AVG</td>
<td class="banner right">OBP</td>
<td class="banner right">SLG</td>
</tr>
<tr>
<td class="datacolBox center" colspan="3"> 1 Year</td>
<td class="datacolBox right">9</td>
<td class="datacolBox right">10</td>
<td class="datacolBox right">1</td>
<td class="datacolBox right">2</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">1</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">3</td>
<td class="datacolBox right">1</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">.200</td>
<td class="datacolBox right">.200</td>
<td class="datacolBox right">.200</td>
</tr>
</table>
</div>
<!-- Fielding Statistics -->
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header" colspan="17"><h2>Joe Rosselli</h2><p>Joe Rosselli  Fielding Stats</p></td>
</tr>
<tr>
<td class="banner">Team</td>
<td class="banner right">POS</td>
<td class="banner right">G</td>
<td class="banner right">GS</td>
<td class="banner right">OUTS</td>
<td class="banner right">TC</td>
<td class="banner right">TC/G</td>
<td class="banner right">CH</td>
<td class="banner right">PO</td>
<td class="banner right">A</td>
<td class="banner right">E</td>
<td class="banner right">DP</td>
<td class="banner right">PB</td>
<td class="banner right">CASB</td>
<td class="banner right">CACS</td>
<td class="banner right">FLD%</td>
<td class="banner right">RF</td>
</tr>
<tr>
<td class="datacolBox"><a href="../teamstats/statmaster.php?d=F&amp;l=NL&amp;t=SFN&amp;y=1995&amp;a=set" title="Show Team Fielding Stats">1995 Giants</a></td>
<td class="datacolBox right">P</td>
<td class="datacolBox right">9</td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">90</td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">0.6</td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">1</td>
<td class="datacolBox right">n/a</td>
<td class="datacolBox right">n/a</td>
<td class="datacolBox right">n/a</td>
<td class="datacolBox right">1.000</td>
<td class="datacolBox right">1.50</td>
</tr>
<tr>
<td class="banner center">Career</td>
<td class="banner right">POS</td>
<td class="banner right">G</td>
<td class="banner right">GS</td>
<td class="banner right">OUTS</td>
<td class="banner right">TC</td>
<td class="banner right">TC/G</td>
<td class="banner right">CH</td>
<td class="banner right">PO</td>
<td class="banner right">A</td>
<td class="banner right">E</td>
<td class="banner right">DP</td>
<td class="banner right">PB</td>
<td class="banner right">CASB</td>
<td class="banner right">CACS</td>
<td class="banner right">FLD%</td>
<td class="banner right">RF</td>
</tr>
<tr>
<td class="datacolBox center">P Totals</td>
<td class="datacolBox right"></td>
<td class="datacolBox right">9</td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">90</td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">0.6</td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">1</td>
<td class="datacolBox right">n/a</td>
<td class="datacolBox right">n/a</td>
<td class="datacolBox right">n/a</td>
<td class="datacolBox right">1.000</td>
<td class="datacolBox right">1.50</td>
</tr>
<tr>
<td class="datacolBox center"> 1 Year</td>
<td class="datacolBox right"></td>
<td class="datacolBox right">9</td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">90</td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">0.6</td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">5</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">1</td>
<td class="datacolBox right">n/a</td>
<td class="datacolBox right">n/a</td>
<td class="datacolBox right">n/a</td>
<td class="datacolBox right">1.000</td>
<td class="datacolBox right">1.50</td>
</tr>
</table>
</div>
<!-- Miscellaneous Statistics -->
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header" colspan="13"><h2>Joe Rosselli</h2><p>Joe Rosselli Miscellaneous Stats</p></td>
</tr>
<tr>
<td class="banner" colspan="4">Baserunning Statistics</td>
<td class="banner" colspan="3">Other Positions</td>
<td class="banner" colspan="3">Common Hitting Ratios</td>
<td class="banner" colspan="3">Common Pitching Ratios</td>
</tr>
<tr>
<td class="banner">Team</td>
<td class="banner right">SB</td>
<td class="banner right">CS</td>
<td class="banner right">SB%</td>
<td class="banner right">PH</td>
<td class="banner right">PR</td>
<td class="banner right">DH</td>
<td class="banner right">AB/HR</td>
<td class="banner right">AB/K</td>
<td class="banner right">AB/RBI</td>
<td class="banner right">K/BB</td>
<td class="banner right">K/9</td>
<td class="banner right">BB/9</td>
</tr>
<tr>
<td class="datacolBox"><a href="../teamstats/statmaster.php?d=H&amp;l=NL&amp;t=SFN&amp;y=1995&amp;a=set" title="Show Team Base Running Stats">1995 Giants</a></td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">.000</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">n/a</td>
<td class="datacolBox right">0.0</td>
<td class="datacolBox right">3.3</td>
<td class="datacolBox right">10.0</td>
<td class="datacolBox right">0.35</td>
<td class="datacolBox right">2.10</td>
<td class="datacolBox right">6.00</td>
</tr>
<tr>
<td class="banner center">Career</td>
<td class="banner right">SB</td>
<td class="banner right">CS</td>
<td class="banner right">SB%</td>
<td class="banner right">PH</td>
<td class="banner right">PR</td>
<td class="banner right">DH</td>
<td class="banner right">AB/HR</td>
<td class="banner right">AB/K</td>
<td class="banner right">AB/RBI</td>
<td class="banner right">K/BB</td>
<td class="banner right">K/9</td>
<td class="banner right">BB/9</td>
</tr>
<tr>
<td class="datacolBox center"> 1 Year</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">.000</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">0</td>
<td class="datacolBox right">n/a</td>
<td class="datacolBox right">0.0</td>
<td class="datacolBox right">3.3</td>
<td class="datacolBox right">10.0</td>
<td class="datacolBox right">0.35</td>
<td class="datacolBox right">2.10</td>
<td class="datacolBox right">6.00</td>
</tr>
</table>
</div>
<!-- Miscellaneous Interest -->
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header" colspan="5"><h2>Joe Rosselli</h2><p>Joe Rosselli Miscellaneous Items of Interest</p></td>
</tr>
<tr>
<td class="banner">Team [Click for Roster]</td>
<td class="banner">Uniform Numbers</td>
<td class="banner">Salary</td>
<td class="banner">All-Star</td>
<td class="banner">World Series</td>
</tr>
<tr>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1995&amp;t=SFN" title="Show Team Base Running Stats">1995 San Francisco Giants</a></td>
<td class="datacolBox center"><a href="/teams/baseball_uniform_numbers.php?t=SFN#uni-53">53</a></td>
<td class="datacolBox right">$109,000.00</td>
<td class="datacolBox">-</td>
<td class="datacolBox">-</td>
</tr>
<tr>
<td class="banner" colspan="5">Joe Rosselli Stats by Baseball Almanac</td>
</tr>
</table>
</div>
<!---END-PF--->
<!-- End Page Body -->
<!-- Begin Related Pages -->
<div class="notes">
<p><a href="/custom_search.shtml?q=Joe Rosselli"><img alt="search this site" class="ba-notes-icon" src="/images/ba-search-btn.jpg" title="Search this Site"/></a>
<a href="/teamstats/glossary.shtml"><img alt="site glossary" class="ba-notes-icon" src="/images/ba-glossary-btn.jpg" title="Glossary"/></a>
<script>var pfHeaderImgUrl = '';var pfHeaderTagline = '';var pfdisableClickToDel = 0;var pfHideImages = 0;var pfImageDisplayStyle = 'right';var pfDisablePDF = 0;var pfDisableEmail = 0;var pfDisablePrint = 0;var pfCustomCSS = 'https://www.baseball-almanac.com/css/print.css';var pfBtVersion='2';(function(){var js,pf;pf=document.createElement('script');pf.type='text/javascript';pf.src='//cdn.printfriendly.com/printfriendly.js';document.getElementsByTagName('head')[0].appendChild(pf)})();</script><a class="printfriendly" href="https://www.printfriendly.com" onclick="window.print();return false;" style="color:#6D9F00;text-decoration:none;" title="Printer Friendly and PDF"><img alt="Print Friendly and PDF" src="/images/ba-print-friendly-btn.jpg" style="border:none;-webkit-box-shadow:none;box-shadow:none; width: auto; vertical-align: middle; display: inline-block"/></a></p></div>
<!-- End Related Pages -->
<img alt="baseball almanac flat baseball" class="flat-baseball-img" src="/images/ball-red.png"/>
<div class="fast-facts">
<h3><img alt="baseball almanac fast facts" src="/images/fast-facts-logo.png"/></h3>
<p>Did you know that you can compare Joe Rosselli to other rookies who also had their Major League debut during the 
                      <a href="../yearly/debut.php?y=1995&amp;l=NL">1995 National League season</a>?</p>
<p>You can follow the team links in the chart above to locate common statistics (singles),
                      advanced statistics (WHIP Ratio &amp; Isolated Power), and unique
                      statistics (plate appearances &amp; times on bases) not found
                      on any other website.</p>
<p>If you find this type of "free" data useful please consider 
                      <a href="/support.shtml">making a donation</a> to <i>Baseball Almanac</i> : 
                      a privately run / non-commercial site in need of financial assistance.</p>
</div>
</div>
<div class="footer">
<!-- START _footer-v2.txt" -->
<div class="container">
<div class="notes">
<a href="/"><img alt="Baseball Almanac" src="/images/baseball-almanac-logo.png" style="max-width: 180px; margin; 0 auto"/></a>
<p>Where what happened yesterday<br/> is being preserved today.</p>
<div class="social">
<a href="https://twitter.com/BaseballAlmanac" target="_blank"><i class="fab fa-twitter"></i></a>
<a href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank"><i class="fab fa-facebook-f"></i></a>
<a href="https://www.instagram.com/baseballalmanac/" target="_blank"><i class="fab fa-instagram"></i></a>
</div>
</div>
<div class="links">
<div>
<span>Stats</span>
<a href="/me_award.shtml">Awards</a>
<a href="/featmenu.shtml">Fabulous Feats</a>
<a href="/frstmenu.shtml">Famous Firsts</a>
<a href="/hofmenu.shtml">Hall of Fame</a>
<a href="/himenu.shtml">Hitting Charts</a>
<a href="/limenu.shtml">Legendary Lists</a>
<a href="/pimenu.shtml">Pitching Charts</a>
<a href="/rb_menu.shtml">Record Books</a>
<a href="/rulemenu.shtml">Rules</a>
<a href="/scoring.shtml">Scoring</a>
<a href="/teamstats/statmaster.php">Statmaster</a>
<a href="/bstatmen.shtml">Stats 101</a>
<a href="/yearmenu.shtml">Year by Year</a>
</div>
<div>
<span>People</span>
<a href="/automenu.shtml">Autographs</a>
<a href="/players/ballplayer.shtml">Ballplayers</a>
<a href="/fammenu.shtml">Baseball Families</a>
<a href="/players/baseball_interviews.shtml">Interviews</a>
<a href="/mgrmenu.shtml">Managers</a>
<a href="/deaths/chart/baseball_player_obituaries.shtml">Obituaries</a>
<a href="/quomenu.shtml">Quotes</a>
<a href="/teammenu.shtml">Team by Team</a>
<a href="/umpiresmenu.shtml">Umpires</a>
<a href="/prz_menu.shtml">US Presidents</a>
</div>
<div>
<span>Places</span>
<a href="/asgmenu.shtml">All-Star Game</a>
<a href="/stadium.shtml">Ballparks</a>
<a href="/division_series/division_series.shtml">Division Series</a>
<a href="/graves/baseball_graves.shtml">Grave Sites</a>
<a href="/League_Championship_Series.shtml">LCS</a>
<a href="/opening_day/opening_day.shtml">Opening Day</a>
<a href="/ws/wsmenu.shtml">World Series</a>
</div>
<div>
<span>Other</span>
<a href="/about.shtml">Advertising</a>
<a href="/baseball_cards/baseball_cards.php">Baseball Cards</a>
<a href="/bookmenu.shtml">Book Shelf</a>
<a href="/feedmenu.shtml">Feedback</a>
<a href="/gam_menu.shtml">Fun &amp; Games</a>
<a href="/humomenu.shtml">Humor &amp; Jokes</a>
<a href="/mve_time.shtml">Movie Time</a>
<a href="/mlmstart.shtml">Newsletter</a>
<a href="/baseball_news.shtml">News Feeds</a>
<a href="/poems.shtml">Poetry &amp; Song</a>
<a href="/privacy-policy.shtml">Privacy Policy</a>
<a href="/Search.shtml">Search &amp; Find</a>
<a href="/support.shtml">Support</a>
</div>
</div>
<div class="copyright">
<div class="legal">
<p>Copyright 1999-<script type="text/javascript">
			copyright=new Date();
			update=copyright.getFullYear();
			document.write(update);
		</script>. All Rights Reserved by Baseball Almanac, Inc.<br/>Hosted by <a href="https://www.hosting4less.com" rel="nofollow" target="_blank">Hosting 4 Less</a>. Part of the <a href="https://www.baseball-almanac.com/">Baseball Almanac</a> Family</p>
</div>
<div class="external">
<a href="http://www.755homeruns.com/" rel="nofollow" target="_blank">755 Home Runs</a>
<a href="http://www.baseball-boxscores.com/" rel="nofollow" target="_blank">Baseball Box Scores</a>
<a href="http://www.baseball-fever.com/" rel="nofollow" target="_blank">Baseball Fever</a>
<a href="http://www.todayinbaseballhistory.com/" rel="nofollow" target="_blank">Today in Baseball History</a>
</div>
</div>
</div>
<div class="google-adsense" style="text-align: center; margin: 0 auto; display: block !important">
<!-- BEGIN GOOGLE ADSENSE -->
<script type="text/javascript">
    google_ad_client = "ca-pub-7150597730414049";
    google_ad_slot = "0964786609";
    google_ad_width = 728;
    google_ad_height = 90;
</script>
<script src="https://pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript"></script>
<!-- END GOOGLE ADSENSE -->
</div>
<!-- END _footer-v2.txt" -->
<p class="last-modified">Last-Modified: January 11, 2021 6:14 AM EST</p>
</div>
</div>
</body>
</html>
