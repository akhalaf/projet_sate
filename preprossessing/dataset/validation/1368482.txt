<!DOCTYPE html>
<html class="html__responsive html__unpinned-leftnav">
<head>
<title>Log In - Stack Overflow</title>
<link href="https://cdn.sstatic.net/Sites/stackoverflow/Img/favicon.ico?v=ec617d715196" rel="shortcut icon"/>
<link href="https://cdn.sstatic.net/Sites/stackoverflow/Img/apple-touch-icon.png?v=c78bd457575a" rel="apple-touch-icon"/>
<link href="https://cdn.sstatic.net/Sites/stackoverflow/Img/apple-touch-icon.png?v=c78bd457575a" rel="image_src"/>
<link href="/opensearch.xml" rel="search" title="Stack Overflow" type="application/opensearchdescription+xml"/>
<meta content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0" name="viewport"/>
<meta content="website" property="og:type"/>
<meta content="https://stackoverflow.com/users/login?ssrc=story-join-login&amp;returnurl=%09%0A" property="og:url"/>
<meta content="Stack Overflow" property="og:site_name"/>
<meta content="https://cdn.sstatic.net/Sites/stackoverflow/Img/apple-touch-icon@2.png?v=73d79a89bded" itemprop="image primaryImageOfPage" property="og:image"/>
<meta content="summary" name="twitter:card"/>
<meta content="stackoverflow.com" name="twitter:domain"/>
<meta content="Log In" itemprop="name" name="twitter:title" property="og:title"/>
<meta content="Stack Overflow | The World’s Largest Online Community for Developers" itemprop="description" name="twitter:description" property="og:description"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.sstatic.net/Js/stub.en.js?v=a22ec6581a9d"></script>
<link href="https://cdn.sstatic.net/Shared/stacks.css?v=50b8afc7d7a9" rel="stylesheet" type="text/css"/>
<link href="https://cdn.sstatic.net/Sites/stackoverflow/primary.css?v=f6feec977fe7" rel="stylesheet" type="text/css"/>
<link href="https://cdn.sstatic.net/Sites/stackoverflow/secondary.css?v=d614bb0f1de9" rel="stylesheet" type="text/css"/>
<script src="https://cdn.sstatic.net/Js/auth.en.js?v=eea5b2cc06f9"></script>
<script>
        StackExchange.init({"locale":"en","serverTime":1610476055,"routeName":"Users/Login","stackAuthUrl":"https://stackauth.com","networkMetaHostname":"meta.stackexchange.com","site":{"name":"Stack Overflow","description":"Q&A for professional and enthusiast programmers","isNoticesTabEnabled":true,"enableNewTagCreationWarning":true,"insertSpaceAfterNameTabCompletion":false,"id":1,"childUrl":"https://meta.stackoverflow.com","styleCodeWithHighlightjs":true,"negativeVoteScoreFloor":null,"enableSocialMediaInSharePopup":true,"protocol":"https"},"user":{"fkey":"0f3cb44ba421614c025d94dcf64812e32a7cfd368014e116db4b76f3ee4a866b","tid":"975d93d1-a151-cc7e-40e6-3ec0c9d8d32d","rep":0,"isAnonymous":true,"isAnonymousNetworkWide":true},"events":{"postType":{"question":1},"postEditionSection":{"title":1,"body":2,"tags":3}},"story":{"minCompleteBodyLength":75,"likedTagsMaxLength":300,"dislikedTagsMaxLength":300},"jobPreferences":{"maxNumDeveloperRoles":2,"maxNumIndustries":4},"svgIconPath":"https://cdn.sstatic.net/Img/svg-icons","svgIconHash":"5acef7872715"}, {"userProfile":{"openGraphAPIKey":"4a307e43-b625-49bb-af15-ffadf2bda017"},"userMessaging":{"showNewFeatureNotice":true},"tags":{},"subscriptions":{"defaultMaxTrueUpSeats":1000},"snippets":{"renderDomain":"stacksnippets.net","snippetsEnabled":true},"slack":{"sidebarAdDismissCookie":"slack-sidebar-ad","sidebarAdDismissCookieExpirationDays":60.0},"site":{"allowImageUploads":true,"enableImgurHttps":true,"enableUserHovercards":true,"forceHttpsImages":true,"styleCode":true},"intercom":{"appId":"inf0secd","hostBaseUrl":"https://stacksnippets.net"},"paths":{},"monitoring":{"clientTimingsAbsoluteTimeout":30000,"clientTimingsDebounceTimeout":1000},"mentions":{"maxNumUsersInDropdown":50},"markdown":{"enableTables":true},"legal":{"oneTrustConfigId":"c3d9f1e3-55f3-4eba-b268-46cee4c6789c"},"flags":{"allowRetractingCommentFlags":true,"allowRetractingFlags":true},"comments":{},"accounts":{"currentPasswordRequiredForChangingStackIdPassword":true}});
        StackExchange.using.setCacheBreakers({"js/adops.en.js":"22a9bd59b1e9","js/ask.en.js":"e287ffdc77d4","js/begin-edit-event.en.js":"cb9965ad8784","js/events.en.js":"b53fd0fd591f","js/explore-qlist.en.js":"22610455f68f","js/full-anon.en.js":"0ae3ba862996","js/full.en.js":"fcb31ca169bc","js/help.en.js":"0925b0214fb9","js/highlightjs-loader.en.js":"90aca6a4a3b9","js/inline-tag-editing.en.js":"1b88a7bb274f","js/keyboard-shortcuts.en.js":"cb7a8b5155c3","js/markdown-it-loader.en.js":"54392c3e00fb","js/mobile.en.js":"a9e14b0b698e","js/moderator.en.js":"de189e64d875","js/postCollections-transpiled.en.js":"0da8fae4fd92","js/post-validation.en.js":"79628bd53d34","js/prettify-full.en.js":"ed1572b7d922","js/question-editor.en.js":"","js/review.en.js":"ffbf36d44008","js/review-v2-transpiled.en.js":"d0bf3c7fb063","js/revisions.en.js":"0079186f9745","js/stacks-editor.en.js":"7fd8feba5b87","js/tageditor.en.js":"0ffdcf3fc9c4","js/tageditornew.en.js":"aa46bb4f969a","js/tagsuggestions.en.js":"9b2c5d9791d2","js/wmd.en.js":"c6971c732064","js/snippet-javascript-codemirror.en.js":"e7e14edff46f"});
        StackExchange.using("gps", function() {
             StackExchange.gps.init(true);
        });
    </script>
<noscript id="noscript-css"><style>body,.top-bar{margin-top:1.9em}</style></noscript>
</head>
<body class="universal-auth-page unified-theme floating-content">
<div id="notify-container"></div>
<div id="custom-header"></div>
<header class="top-bar js-top-bar top-bar__network _fixed">
<div class="wmx12 mx-auto grid ai-center h100" role="menubar">
<div class="-main grid--cell">
<a aria-controls="left-sidebar" aria-expanded="false" aria-haspopup="true" class="left-sidebar-toggle p0 ai-center jc-center js-left-sidebar-toggle" href="#" role="menuitem"><span class="ps-relative"></span></a>
<div class="topbar-dialog leftnav-dialog js-leftnav-dialog dno">
<div class="left-sidebar js-unpinned-left-sidebar" data-can-be="left-sidebar" data-is-here-when="sm md lg"></div>
</div>
<a class="-logo js-gps-track" data-gps-track="top_nav.click({is_current:false, location:22, destination:8})" href="https://stackoverflow.com">
<span class="-img _glyph">Stack Overflow</span>
</a>
</div>
<ol class="list-reset grid gs4" role="presentation">
<li class="grid--cell md:d-none">
<a class="-marketing-link js-gps-track" data-ga='["top navigation","about menu click",null,null,null]' data-gps-track="top_nav.products.click({location:22, destination:7})" href="/company">About</a>
</li>
<li class="grid--cell">
<a aria-controls="products-popover" class="-marketing-link js-gps-track js-products-menu" data-action="s-popover#toggle" data-controller="s-popover" data-ga='["top navigation","products menu click",null,null,null]' data-gps-track="top_nav.products.click({location:22, destination:1})" data-s-popover-placement="bottom" data-s-popover-toggle-class="is-selected" href="#">
                        Products
                    </a>
</li>
<li class="grid--cell md:d-none">
<a class="-marketing-link js-gps-track" data-ga='["top navigation","learn more - teams",null,null,null]' data-gps-track="top_nav.products.click({location:22, destination:7})" href="/teams">For Teams</a>
</li>
</ol>
<div aria-hidden="true" class="s-popover ws2 mtn2 p0" id="products-popover" role="menu">
<div class="s-popover--arrow"></div>
<ol class="list-reset s-anchors s-anchors__inherit">
<li class="m6">
<a class="bar-sm p6 d-block h:bg-black-100 js-gps-track" data-ga='["top navigation","public qa submenu click",null,null,null]' data-gps-track="top_nav.products.click({location:22, destination:2})" href="/questions">
<span class="fs-body1 d-block">Stack Overflow</span>
<span class="fs-caption d-block fc-light">Public questions &amp; answers</span>
</a>
</li>
<li class="m6">
<a class="bar-sm p6 d-block h:bg-black-100 js-gps-track" data-ga='["top navigation","teams submenu click",null,null,null]' data-gps-track="top_nav.products.click({location:22, destination:3})" href="/teams">
<span class="fs-body1 d-block">Stack Overflow for Teams</span>
<span class="fs-caption d-block fc-light">Where developers &amp; technologists share private knowledge with coworkers</span>
</a>
</li>
<li class="m6">
<a class="bar-sm p6 d-block h:bg-black-100 js-gps-track" data-ga='["top navigation","jobs submenu click",null,null,null]' data-gps-track="top_nav.products.click({location:22, destination:9})" href="/jobs?so_source=ProductsMenu&amp;so_medium=StackOverflow">
<span class="fs-body1 d-block">Jobs</span>
<span class="fs-caption d-block fc-light">Programming &amp; related technical career opportunities</span>
</a>
</li>
<li class="m6">
<a class="bar-sm p6 d-block h:bg-black-100 js-gps-track" data-ga='["top navigation","talent submenu click",null,null,null]' data-gps-track="top_nav.products.click({location:22, destination:5})" href="https://stackoverflow.com/talent">
<span class="fs-body1 d-block">Talent</span>
<span class="fs-caption d-block fc-light">Recruit tech talent &amp; build your employer brand</span>
</a>
</li>
<li class="m6">
<a class="bar-sm p6 d-block h:bg-black-100 js-gps-track" data-ga='["top navigation","advertising submenu click",null,null,null]' data-gps-track="top_nav.products.click({location:22, destination:6})" href="https://stackoverflow.com/advertising">
<span class="fs-body1 d-block">Advertising</span>
<span class="fs-caption d-block fc-light">Reach developers &amp; technologists worldwide</span>
</a>
</li>
<li class="bg-black-025 bt bc-black-075 py6 px6 bbr-sm">
<a class="fc-light d-block py6 px6 h:fc-black-800 js-gps-track" data-ga='["top navigation","about submenu click",null,null,null]' data-gps-track="top_nav.products.click({location:22, destination:7})" href="/company">About the company</a>
</li>
</ol>
</div>
<form action="/search" autocomplete="off" class="grid--cell fl-grow1 searchbar px12 js-searchbar " id="search" role="search">
<div class="ps-relative">
<input aria-controls="top-search" aria-label="Search" autocomplete="off" class="s-input s-input__search js-search-field " data-action="focus-&gt;s-popover#show" data-controller="s-popover" data-s-popover-placement="bottom-start" maxlength="240" name="q" placeholder="Search…" type="text" value=""/>
<svg aria-hidden="true" class="s-input-icon s-input-icon__search svg-icon iconSearch" height="18" viewbox="0 0 18 18" width="18"><path d="M18 16.5l-5.14-5.18h-.35a7 7 0 10-1.19 1.19v.35L16.5 18l1.5-1.5zM12 7A5 5 0 112 7a5 5 0 0110 0z"></path></svg>
<div class="s-popover p0 wmx100 wmn4 sm:wmn-initial js-top-search-popover s-popover--arrow__tl" id="top-search" role="menu">
<div class="js-spinner p24 grid ai-center jc-center d-none">
<div class="s-spinner s-spinner__sm fc-orange-400">
<div class="v-visible-sr">Loading…</div>
</div>
</div>
<span class="v-visible-sr js-screen-reader-info"></span>
<div class="js-ac-results overflow-y-auto hmx3 d-none"></div>
<div aria-describedby="Tips for searching" class="js-search-hints"></div>
</div>
</div>
</form>
<ol class="overflow-x-auto ml-auto -secondary grid ai-center list-reset h100 user-logged-out" role="presentation">
<li class="-item searchbar-trigger"><a aria-controls="search" aria-haspopup="true" aria-label="Search" class="-link js-searchbar-trigger" href="#" role="button" title="Click to show search"><svg aria-hidden="true" class="svg-icon iconSearch" height="18" viewbox="0 0 18 18" width="18"><path d="M18 16.5l-5.14-5.18h-.35a7 7 0 10-1.19 1.19v.35L16.5 18l1.5-1.5zM12 7A5 5 0 112 7a5 5 0 0110 0z"></path></svg></a></li>
<li class="-ctas">
<a class="login-link s-btn s-btn__filled py8 js-gps-track" data-ga='["top navigation","login button click",null,null,null]' data-gps-track="login.click" href="https://stackoverflow.com/users/login?ssrc=head" rel="nofollow">Log in</a>
<a class="login-link s-btn s-btn__primary py8" data-ga='["sign up","Sign Up Navigation","Header",null,null]' href="https://stackoverflow.com/users/signup?ssrc=head&amp;returnurl=%2fusers%2fstory%2fcurrent" rel="nofollow">Sign up</a>
</li>
<li class="js-topbar-dialog-corral" role="presentation">
<div class="topbar-dialog siteSwitcher-dialog dno" role="menu">
<div class="header">
<h3>
<a href="https://stackoverflow.com">current community</a>
</h3>
</div>
<div class="modal-content bg-powder-050">
<ul class="current-site">
<li class="grid">
<div class="fl1">
<a class="current-site-link site-link js-gps-track grid gs8 gsx" data-gps-track="site_switcher.click({ item_type:3 })" data-id="1" href="https://stackoverflow.com">
<div class="favicon favicon-stackoverflow site-icon grid--cell" title="Stack Overflow"></div>
<span class="grid--cell fl1">
            Stack Overflow
        </span>
</a>
</div>
<div class="related-links">
<a class="js-gps-track" data-gps-track="site_switcher.click({ item_type:14 })" href="https://stackoverflow.com/help">help</a>
<a class="js-gps-track" data-gps-track="site_switcher.click({ item_type:6 })" href="https://chat.stackoverflow.com/?tab=site&amp;host=stackoverflow.com">chat</a>
</div>
</li>
<li class="related-site grid">
<div class="L-shaped-icon-container">
<span class="L-shaped-icon"></span>
</div>
<a class=" site-link js-gps-track grid gs8 gsx" data-gps-track="site.switch({ target_site:552, item_type:3 }),site_switcher.click({ item_type:4 })" data-id="552" href="https://meta.stackoverflow.com">
<div class="favicon favicon-stackoverflowmeta site-icon grid--cell" title="Meta Stack Overflow"></div>
<span class="grid--cell fl1">
            Meta Stack Overflow
        </span>
</a>
</li>
</ul>
</div>
<div class="header" id="your-communities-header">
<h3>
your communities            </h3>
</div>
<div class="modal-content" id="your-communities-section">
<div class="call-to-login">
<a class="login-link js-gps-track" data-gps-track="site_switcher.click({ item_type:10 })" href="https://stackoverflow.com/users/signup?ssrc=site_switcher&amp;returnurl=%2fusers%2fstory%2fcurrent">Sign up</a> or <a class="login-link js-gps-track" data-gps-track="site_switcher.click({ item_type:11 })" href="https://stackoverflow.com/users/login?ssrc=site_switcher">log in</a> to customize your list.                </div>
</div>
<div class="header">
<h3><a href="https://stackexchange.com/sites">more stack exchange communities</a>
</h3>
<a class="fr" href="https://stackoverflow.blog">company blog</a>
</div>
<div class="modal-content">
<div class="child-content"></div>
</div>
</div>
</li>
</ol>
</div>
</header>
<script>
        StackExchange.ready(function () { StackExchange.topbar.init(); });
StackExchange.scrollPadding.setPaddingTop(50, 10);    </script>
<div class="container">
<div class="left-sidebar js-pinned-left-sidebar ps-relative" data-is-here-when="" id="left-sidebar">
<div class="left-sidebar--sticky-container js-sticky-leftnav">
<nav role="navigation">
<ol class="nav-links">
<li class="">
<a class="pl8 js-gps-track nav-links--link" data-gps-track="top_nav.click({is_current:false, location:22, destination:8})" href="/">
<div class="grid ai-center">
<div class="grid--cell truncate">
                            Home
                        </div>
</div>
</a>
</li>
<li>
<ol class="nav-links">
<li class="fs-fine tt-uppercase ml8 mt16 mb4 fc-light">Public</li>
<li class="">
<a class="pl8 js-gps-track nav-links--link -link__with-icon" data-gps-track="top_nav.click({is_current:false, location:22, destination:1})" href="/questions" id="nav-questions">
<svg aria-hidden="true" class="svg-icon iconGlobe" height="18" viewbox="0 0 18 18" width="18"><path d="M9 1a8 8 0 100 16A8 8 0 009 1zM8 15.32a6.4 6.4 0 01-5.23-7.75L7 11.68v.8c0 .88.12 1.32 1 1.32v1.52zm5.72-2c-.2-.66-1-1.32-1.72-1.32h-1v-2c0-.44-.56-1-1-1H6V7h1c.44 0 1-.56 1-1V5h2c.88 0 1.4-.72 1.4-1.6v-.33a6.4 6.4 0 012.32 10.24v.01z"></path></svg> <span class="-link--channel-name">Stack Overflow</span>
</a>
</li>
<li class="">
<a class=" js-gps-track nav-links--link" data-gps-track="top_nav.click({is_current:false, location:22, destination:2})" href="/tags" id="nav-tags">
<div class="grid ai-center">
<div class="grid--cell truncate">
                            Tags
                        </div>
</div>
</a>
</li>
<li class=" youarehere">
<a class=" js-gps-track nav-links--link" data-gps-track="top_nav.click({is_current:true, location:22, destination:3})" href="/users" id="nav-users">
<div class="grid ai-center">
<div class="grid--cell truncate">
                            Users
                        </div>
</div>
</a>
</li>
<li class="fs-fine tt-uppercase ml8 mt16 mb4 fc-light">Find a Job</li>
<li class="">
<a class=" js-gps-track nav-links--link" data-gps-track="top_nav.click({is_current:false, location:22, destination:6})" href="/jobs?so_medium=StackOverflow&amp;so_source=SiteNav" id="nav-jobs">
<div class="grid ai-center">
<div class="grid--cell truncate">
                            Jobs
                        </div>
</div>
</a>
</li>
<li class="">
<a class=" js-gps-track nav-links--link" data-gps-track="top_nav.click({is_current:false, location:22, destination:12})" href="/jobs/companies?so_medium=StackOverflow&amp;so_source=SiteNav" id="nav-companies">
<div class="grid ai-center">
<div class="grid--cell truncate">
                            Companies
                        </div>
</div>
</a>
</li>
</ol>
</li>
<li>
<ol class="nav-links">
<li class="grid ai-center jc-space-between ml8 mt24 mb4">
<div class="grid--cell tt-uppercase fs-fine fc-light">Teams</div>
<div class="grid--cell fs-fine fc-light mr4">
<a aria-controls="popover-teams-create-cta" class="s-link s-link__inherit js-gps-track" data-action="s-popover#toggle" data-controller="s-popover" data-ga='["teams left navigation - anonymous","left nav show teams info",null,null,null]' data-gps-track="teams.create.left-sidenav.click({ Action: ShowInfo })" data-s-popover-placement="bottom-start" data-s-popover-toggle-class="is-selected" href="javascript:void(0)" role="button">
                                            What’s this?
                                        </a>
</div>
</li>
<li class="ps-relative">
<a class="pl8 js-gps-track nav-links--link" data-ga='["teams left navigation - anonymous","left nav team click","stackoverflow.com/teams",null,null]' data-gps-track="teams.create.left-sidenav.click({ Action: TeamsClick })" href="https://stackoverflow.com/teams" title="Stack Overflow for Teams is a private, secure spot for your organization's questions and answers.">
<div class="grid ai-center">
<div class="grid--cell s-avatar va-middle bg-orange-400">
<div class="s-avatar--letter mtn1">
<svg aria-hidden="true" class="svg-icon iconBriefcaseSm" height="14" viewbox="0 0 14 14" width="14"><path d="M4 3a1 1 0 011-1h4a1 1 0 011 1v1h.5c.83 0 1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5h-7A1.5 1.5 0 012 10.5v-5C2 4.67 2.67 4 3.5 4H4V3zm5 1V3H5v1h4z"></path></svg>
</div>
<svg aria-hidden="true" class="native s-avatar--badge svg-icon iconShieldXSm" height="10" viewbox="0 0 9 10" width="9"><path d="M0 1.84L4.5 0 9 1.84v3.17C9 7.53 6.3 10 4.5 10 2.7 10 0 7.53 0 5.01V1.84z" fill="var(--white)"></path><path d="M1 2.5L4.5 1 8 2.5v2.51C8 7.34 5.34 9 4.5 9 3.65 9 1 7.34 1 5.01V2.5zm2.98 3.02L3.2 7h2.6l-.78-1.48a.4.4 0 01.15-.38c.34-.24.73-.7.73-1.14 0-.71-.5-1.23-1.41-1.23-.92 0-1.39.52-1.39 1.23 0 .44.4.9.73 1.14.12.08.18.23.15.38z" fill="var(--black-500)"></path></svg>
</div>
<div class="grid--cell pl6">
Free 30 Day Trial                                            </div>
</div>
</a>
</li>
</ol>
</li>
</ol>
</nav>
</div>
<div aria-hidden="true" class="s-popover w-auto p16" id="popover-teams-create-cta" role="menu">
<div class="s-popover--arrow"></div>
<div class="ps-relative overflow-hidden">
<p class="mb2"><strong>Teams</strong></p>
<p class="mb16 fs-caption fc-medium">Q&amp;A for Work</p>
<p class="mb8 fs-caption fc-medium">

                            Stack Overflow for Teams is a private, secure spot for you and
                            your coworkers to find and share information.
                                        </p>
<a class="js-gps-track ws-nowrap d-block" data-ga='["teams left navigation - anonymous","left nav cta","stackoverflow.com/teams",null,null]' data-gps-track="teams.create.left-sidenav.click({ Action: CtaClick })" href="https://stackoverflow.com/teams">
Learn more                </a>
</div>
<div class="ps-absolute t8 r8">
<svg fill="none" height="49" width="53" xmlns="http://www.w3.org/2000/svg"><path d="M49 11l.2 31H18.9L9 49v-7H4V8h31" fill="#CCEAFF"></path><path d="M44.5 19v-.3l-.2-.1-18-13-.1-.1H.5v33h4V46l.8-.6 9.9-6.9h29.3V19z" stroke="#1060E1" stroke-miterlimit="10"></path><path d="M31 2l6-1.5 7 2V38H14.9L5 45v-7H1V6h25l5-4z" fill="#fff"></path><path d="M7 16.5h13m-13 6h14m-14 6h18" stroke="#1060E1" stroke-miterlimit="10"></path><path d="M39 30a14 14 0 1 0 0-28 14 14 0 0 0 0 28z" fill="#FFB935"></path><path d="M50.5 14a13.5 13.5 0 1 1-27 0 13.5 13.5 0 0 1 27 0z" stroke="#F48024" stroke-miterlimit="10"></path><path d="M32.5 21.5v-8h9v8h-9zm2-9.5V9.3A2.5 2.5 0 0 1 37 6.8a2.5 2.5 0 0 1 2.5 2.5V12h-5zm2 3v2m1-2v2" stroke="#fff" stroke-miterlimit="10"></path></svg>
</div>
</div>
</div>
<div class="grid grid__center snippet-hidden" id="content">
<div class="grid--cell">
<div class="ta-center fs-title mx-auto mb24">
<a href="https://stackoverflow.com">
<svg aria-hidden="true" class="native svg-icon iconLogoGlyphMd" height="37" viewbox="0 0 32 37" width="32"><path d="M26 33v-9h4v13H0V24h4v9h22z" fill="#BCBBBB"></path><path d="M21.5 0l-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4L13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79l.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z" fill="#F48024"></path></svg>
</a>
</div>
<div class="mx-auto grid grid__fl1 fd-column gs8 gsy mb16 wmx3" id="openid-buttons">
<button class="grid--cell s-btn s-btn__icon s-btn__google bar-md ba bc-black-100" data-oauthserver="https://accounts.google.com/o/oauth2/auth" data-oauthversion="2.0" data-provider="google">
<svg aria-hidden="true" class="native svg-icon iconGoogle" height="18" viewbox="0 0 18 18" width="18"><path d="M16.51 8H8.98v3h4.3c-.18 1-.74 1.48-1.6 2.04v2.01h2.6a7.8 7.8 0 002.38-5.88c0-.57-.05-.66-.15-1.18z" fill="#4285F4"></path><path d="M8.98 17c2.16 0 3.97-.72 5.3-1.94l-2.6-2a4.8 4.8 0 01-7.18-2.54H1.83v2.07A8 8 0 008.98 17z" fill="#34A853"></path><path d="M4.5 10.52a4.8 4.8 0 010-3.04V5.41H1.83a8 8 0 000 7.18l2.67-2.07z" fill="#FBBC05"></path><path d="M8.98 4.18c1.17 0 2.23.4 3.06 1.2l2.3-2.3A8 8 0 001.83 5.4L4.5 7.49a4.77 4.77 0 014.48-3.3z" fill="#EA4335"></path></svg>
Log in with Google        </button>
<button class="grid--cell s-btn s-btn__icon s-btn__github bar-md ba bc-black-100" data-oauthserver="https://github.com/login/oauth/authorize" data-oauthversion="2.0" data-provider="github">
<svg aria-hidden="true" class="svg-icon iconGitHub" height="18" viewbox="0 0 18 18" width="18"><path d="M9 1a8 8 0 00-2.53 15.59c.4.07.55-.17.55-.38l-.01-1.49c-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82a7.42 7.42 0 014 0c1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48l-.01 2.2c0 .21.15.46.55.38A8.01 8.01 0 009 1z" fill="#010101"></path></svg>
Log in with GitHub        </button>
<button class="grid--cell s-btn s-btn__icon s-btn__facebook bar-md" data-oauthserver="https://www.facebook.com/v2.0/dialog/oauth" data-oauthversion="2.0" data-provider="facebook">
<svg aria-hidden="true" class="svg-icon iconFacebook" height="18" viewbox="0 0 18 18" width="18"><path d="M3 1a2 2 0 00-2 2v12c0 1.1.9 2 2 2h12a2 2 0 002-2V3a2 2 0 00-2-2H3zm6.55 16v-6.2H7.46V8.4h2.09V6.61c0-2.07 1.26-3.2 3.1-3.2.88 0 1.64.07 1.87.1v2.16h-1.29c-1 0-1.19.48-1.19 1.18V8.4h2.39l-.31 2.42h-2.08V17h-2.5z" fill="#4167B2"></path></svg>
Log in with Facebook        </button>
</div>
<div class="mx-auto mb24 p24 wmx3 bg-white bar-lg auth-shadow mb24" id="formContainer">
<form action="/users/login?ssrc=story-join-login&amp;returnurl=%09%0a" class="grid fd-column gs12 gsy" id="login-form" method="POST">
<input name="fkey" type="hidden" value="0f3cb44ba421614c025d94dcf64812e32a7cfd368014e116db4b76f3ee4a866b"/>
<input name="ssrc" type="hidden" value="story-join-login"/>
<div class="grid fd-column gs4 gsy js-auth-item ">
<label class="grid--cell s-label" for="email">Email</label>
<div class="grid ps-relative">
<input class="s-input" id="email" maxlength="100" name="email" size="30" type="email"/>
<svg aria-hidden="true" class="s-input-icon js-alert-icon d-none svg-icon iconAlertCircle" height="18" viewbox="0 0 18 18" width="18"><path d="M9 17A8 8 0 119 1a8 8 0 010 16zM8 4v6h2V4H8zm0 8v2h2v-2H8z"></path></svg>
</div>
<p class="grid--cell s-input-message js-error-message d-none">
</p>
</div>
<div class="grid fd-column-reverse gs4 gsy js-auth-item ">
<p class="grid--cell s-input-message js-error-message d-none">
</p>
<div class="grid ps-relative js-password">
<input autocomplete="off" class="grid--cell s-input" id="password" name="password" type="password"/>
<svg aria-hidden="true" class="s-input-icon js-alert-icon d-none svg-icon iconAlertCircle" height="18" viewbox="0 0 18 18" width="18"><path d="M9 17A8 8 0 119 1a8 8 0 010 16zM8 4v6h2V4H8zm0 8v2h2v-2H8z"></path></svg>
</div>
<div class="grid ai-center ps-relative jc-space-between">
<label class="grid--cell s-label" for="password">Password</label>
<a class="grid--cell s-link fs-caption" href="/users/account-recovery">Forgot password?</a>
</div>
</div>
<div class="grid gs4 gsy fd-column js-auth-item ">
<button class="grid--cell s-btn s-btn__primary" id="submit-button" name="submit-button">Log in</button>
<p class="grid--cell s-input-message js-error-message d-none">
</p>
</div>
<input id="oauth_version" name="oauth_version" type="hidden"/>
<input id="oauth_server" name="oauth_server" type="hidden"/>
</form>
</div>
<script>
    StackExchange.ready(function () {

        StackExchange.using("gps", function() { StackExchange.gps.sendPending(); });

    });

</script>
<div class="mx-auto ta-center fs-body1 p16 pb0 mb24 w100 wmx3">
                Don’t have an account? <a href="/users/signup?ssrc=story-join-login&amp;returnurl=%09%0a">Sign up</a>
<div class="mt12">
                        Are you an employer? <a href="https://careers.stackoverflow.com/employer/login" name="talent">Sign up on Talent <svg aria-hidden="true" class="va-text-bottom sm:d-none svg-icon iconShareSm" height="14" viewbox="0 0 14 14" width="14"><path d="M5 1H3a2 2 0 00-2 2v8c0 1.1.9 2 2 2h8a2 2 0 002-2V9h-2v2H3V3h2V1zm2 0h6v6h-2V4.5L6.5 9 5 7.5 9.5 3H7V1z"></path></svg></a>
</div>
</div>
</div>
<script>
    StackExchange.ready(function () {
        auth.init(auth.pages.Login);
    });
</script>
</div>
</div>
<noscript>
<div id="noscript-warning">Stack Overflow works best with JavaScript enabled
            <img alt="" class="dno" src="https://pixel.quantserve.com/pixel/p-c1rF4kxgLUzNc.gif"/>
</div>
</noscript>
<script>
(function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function() { (i[r].q = i[r].q || []).push(arguments) }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m);
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            StackExchange.ready(function () {

                StackExchange.ga.init({
                    sendTitles: true,
                    tracker: window.ga,
                    trackingCodes: [
                        'UA-108242619-1'
                    ],
                        checkDimension: 'dimension42'
                });




                    StackExchange.ga.setDimension('dimension3', 'Users/Login');


                StackExchange.ga.trackPageView();
            });
            
            var _qevents = _qevents || [],
            _comscore = _comscore || [];
            (function() {
                var s = document.getElementsByTagName('script')[0],
                    qc = document.createElement('script');
 qc.async = true;
                    qc.src = 'https://secure.quantserve.com/quant.js';
                    s.parentNode.insertBefore(qc, s);
                    _qevents.push({ qacct: "p-c1rF4kxgLUzNc" }); var sc = document.createElement('script');
                    sc.async = true;
                    sc.src = 'https://sb.scorecardresearch.com/beacon.js';
                    s.parentNode.insertBefore(sc, s);
                    _comscore.push({ c1: "2", c2: "17440561" });            })();
                </script>
<style>#consent-footer-link { display: none; visibility: hidden; }</style>
</body>
</html>
