<!DOCTYPE html>
<html lang="ja">
<head>
<meta content="新しいアプリを見つける、またはお気に入りのソフトウェアを無料でダウンロードしましょう。" name="description"/>
<title>Windows用の無料ソフトウェアをダウンロード - Jaleco.com</title>
<link href="//cdn.download.it/dit/favicon/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="//cdn.download.it/dit/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="//cdn.download.it/dit/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="//cdn.download.it/dit/favicon/site.webmanifest" rel="manifest"/>
<link color="#5bbad5" href="//cdn.download.it/dit/favicon/safari-pinned-tab.svg" rel="mask-icon"/>
<meta content="#da532c" name="msapplication-TileColor"/>
<meta content="#ffffff" name="theme-color"/>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<meta content="ie=edge" http-equiv="X-UA-Compatible"/>
<link href="https://jp.download.it/" rel="canonical"/>
<link href="https://fonts.googleapis.com" rel="dns-prefetch"/>
<link crossorigin="" href="https://fonts.gstatic.com/" rel="preconnect"/>
<link href="https://fonts.googleapis.com/css?family=Dosis:400,600&amp;display=swap" rel="stylesheet"/>
<script crossorigin="anonymous" defer="" src="//cdn.download.it/dit/js/jquery-3.4.1.min.js"></script>
<script crossorigin="anonymous" defer="" src="//cdn.download.it/dit/js/popper.min.js"></script>
<script crossorigin="anonymous" defer="" src="//cdn.download.it/dit/js/bootstrap.min.js"></script>
<link crossorigin="anonymous" href="//cdn.download.it/dit/css/bootstrap.min.css" rel="stylesheet"/>
<link href="//cdn.download.it/dit/css/hover-min.css" rel="stylesheet" type="text/css"/>
<link href="//cdn.download.it/dit/fontawesome/css/all.min.css" rel="stylesheet"/>
<link href="//cdn.download.it/dit/css/main.css?v=1610449852840" rel="stylesheet"/>
<script defer="" src="//cdn.download.it/dit/js/search-header.js?v=1610449852840" type="text/javascript"></script>
<script type="text/javascript">
  window.addEventListener('DOMContentLoaded', function() {
    $.sheader(
      '.swarpper',
      'input',
      '.glass-wrap',
      'https://jp.download.it/s/search_placehodler'
    );

	$(function () {
	  $('[data-toggle="popover"]').popover()
	});
  });
</script>
<link href="https://jp.download.it/" hreflang="ja" rel="alternate"/>
<link href="https://fa.download.it/" hreflang="fa" rel="alternate"/>
<link href="https://th.download.it/" hreflang="th" rel="alternate"/>
<link href="https://sw.download.it/" hreflang="sw" rel="alternate"/>
<link href="https://ru.download.it/" hreflang="ru" rel="alternate"/>
<link href="https://ph.download.it/" hreflang="tl" rel="alternate"/>
<link href="https://es.download.it/" hreflang="es" rel="alternate"/>
<link href="https://ar.download.it/" hreflang="ar" rel="alternate"/>
<link href="https://br.download.it/" hreflang="pt" rel="alternate"/>
<link href="https://de.download.it/" hreflang="de" rel="alternate"/>
<link href="https://nl.download.it/" hreflang="nl" rel="alternate"/>
<link href="https://kr.download.it/" hreflang="ko" rel="alternate"/>
<link href="https://tr.download.it/" hreflang="tr" rel="alternate"/>
<link href="https://in.download.it/" hreflang="hi" rel="alternate"/>
<link href="https://pl.download.it/" hreflang="pl" rel="alternate"/>
<link href="https://se.download.it/" hreflang="sv" rel="alternate"/>
<link href="https://my.download.it/" hreflang="ms" rel="alternate"/>
<link href="https://fr.download.it/" hreflang="fr" rel="alternate"/>
<link href="https://id.download.it/" hreflang="id" rel="alternate"/>
<link href="https://download.it/" hreflang="it" rel="alternate"/>
<meta content="no-referrer-when-downgrade" name="referrer"/>
<meta content="#1a7dff" name="theme-color"/>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "name": "Download.it",
        "url": "https://jp.download.it/",
        "potentialAction": {
            "@type": "SearchAction",
            "target": "https://jp.download.it/s/{search_placehodler}",
            "query-input": "required name=search_placehodler"
        }
	}
</script>
<script async="" src="/cdn-cgi/bm/cv/669835187/api.js"></script></head>
<body>
<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
			ga('create', 'UA-30374496-1', 'auto');
			ga('send', 'pageview');
		</script>
<header class="container-fluid">
<div class="container head-in-wrap">
<div class="row no-gutters">
<div class="col-6 no-gutters">
<div class="col-4 col-lg-3 text-left" id="head-title">
<p><a href="//jp.download.it/">Download.it</a></p>
</div>
</div>
<div class="col-6">
<div class="row justify-content-end no-gutters ">
<div class="search-misc col-8 d-none d-lg-block" id="head-search">
<form class="swarpper">
<div class="form-glass-wrap">
<input class="form-control" id="search_input" placeholder="プログラムを検索" type="text"/>
<div class="glass-wrap" data-inputselector="#search_input">
<img src="//cdn.download.it/dit/images/maglass-blue.svg" width="20"/>
</div>
</div>
</form>
</div>
<nav class="search-misc col-auto d-none d-lg-block" id="head-platform-lang">
<div class="no-gutters h-100 row justify-content-around justify-content-md-end">
<div class="dropdown col-auto" title="Windows">
<div class="text-right lp-selector-wrapper " data-toggle="dropdown" id="ddPlatformDesktop">
<div class="platform-wrapper">
<i class="fab fa-windows"></i><span></span>
</div>
</div>
</div>
<div class="dropdown col-auto" title="日本語">
<div class=" ditclickable text-right lp-selector-wrapper" data-toggle="dropdown" id="ddLangDesktop">
<div class="lang-wrapper">
<span class="content">JA</span><span><i class="fas fa-angle-down" style="margin-right: 0;"></i></span>
</div>
</div>
<div aria-labelledby="ddLangDesktop" class="dropdown-menu dropdown-menu-right">
<a class="dropdown-item" href="https://fa.download.it/" title="فارسی">فارسی</a>
<a class="dropdown-item" href="https://th.download.it/" title="ไทย">ไทย</a>
<a class="dropdown-item" href="https://sw.download.it/" title="Kiswahili">Kiswahili</a>
<a class="dropdown-item" href="https://ru.download.it/" title="Русский">Русский</a>
<a class="dropdown-item" href="https://ph.download.it/" title="Tagalog">Tagalog</a>
<a class="dropdown-item" href="https://es.download.it/" title="Español">Español</a>
<a class="dropdown-item" href="https://ar.download.it/" title="العربية">العربية</a>
<a class="dropdown-item" href="https://br.download.it/" title="Português">Português</a>
<a class="dropdown-item" href="https://de.download.it/" title="Deutsch">Deutsch</a>
<a class="dropdown-item" href="https://nl.download.it/" title="Nederlands">Nederlands</a>
<a class="dropdown-item" href="https://kr.download.it/" title="한국어">한국어</a>
<a class="dropdown-item" href="https://tr.download.it/" title="Türkçe">Türkçe</a>
<a class="dropdown-item" href="https://in.download.it/" title="हिन्दी">हिन्दी</a>
<a class="dropdown-item" href="https://pl.download.it/" title="Polski">Polski</a>
<a class="dropdown-item" href="https://se.download.it/" title="Svenska">Svenska</a>
<a class="dropdown-item" href="https://my.download.it/" title="Melayu">Melayu</a>
<a class="dropdown-item" href="https://fr.download.it/" title="Français">Français</a>
<a class="dropdown-item" href="https://id.download.it/" title="Bahasa Indonesia">Bahasa Indonesia</a>
<a class="dropdown-item" href="https://download.it/" title="Italiano">Italiano</a>
</div>
</div>
</div>
</nav>
</div>
<div class="d-lg-none " id="head-search-menu-mobile">
<div class="row justify-content-end">
<div class="col-auto">
<div class="head-topright-element" data-target="#searchCollapse" data-toggle="collapse">
<img height="15" src="//cdn.download.it/dit/images/maglass-white.svg" width="15"/>
</div>
</div>
<div class="col-auto">
<div class="head-topright-element" data-target="#menuCollapse" data-toggle="collapse">
<img height="21" src="//cdn.download.it/dit/images/burger-21.png" width="21"/>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="collapse" id="searchCollapse">
<div class="container-fluid">
<div class="row">
<div class="col-12">
<form class="swarpper">
<div class="form-glass-wrap mobile">
<input class="form-control" id="search_input_mobile" placeholder="プログラムを検索" type="text"/>
<div class="glass-wrap" data-inputselector="#search_input_mobile">
<img src="//cdn.download.it/dit/images/maglass-blue.svg" width="20"/>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
<div class="collapse" id="menuCollapse">
<nav class="">
<div class="row m-platforms">
</div>
<div class="row m-locales">
<div class="col-12 m-locale-row">
<p title="فارسی"><a href="https://fa.download.it/">فارسی</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="ไทย"><a href="https://th.download.it/">ไทย</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="Kiswahili"><a href="https://sw.download.it/">Kiswahili</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="Русский"><a href="https://ru.download.it/">Русский</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="Tagalog"><a href="https://ph.download.it/">Tagalog</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="Español"><a href="https://es.download.it/">Español</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="العربية"><a href="https://ar.download.it/">العربية</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="Português"><a href="https://br.download.it/">Português</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="Deutsch"><a href="https://de.download.it/">Deutsch</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="Nederlands"><a href="https://nl.download.it/">Nederlands</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="한국어"><a href="https://kr.download.it/">한국어</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="Türkçe"><a href="https://tr.download.it/">Türkçe</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="हिन्दी"><a href="https://in.download.it/">हिन्दी</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="Polski"><a href="https://pl.download.it/">Polski</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="Svenska"><a href="https://se.download.it/">Svenska</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="Melayu"><a href="https://my.download.it/">Melayu</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="Français"><a href="https://fr.download.it/">Français</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="Bahasa Indonesia"><a href="https://id.download.it/">Bahasa Indonesia</a></p>
</div>
<div class="col-12 m-locale-row">
<p title="Italiano"><a href="https://download.it/">Italiano</a></p>
</div>
</div>
</nav>
</div>
</header>
<div id="main-container">
<section class="container im-radont" id="home-rank">
<div class="row pt-lg-3 mb-lg-2">
<div class="col-12 text-center">
<h1>最もダウンロードされたプログラム</h1>
</div>
</div>
<div class="row">
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//easy-adblocker.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Easy Ad Blocker" height="80" src="//cdn.download.it/gen/easy-adblocker-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Easy Ad Blocker">Easy Ad Blocker</h2>
<p class="text-limit-2" title="クリックひとつでオン/オフ切り替え可能な、使いやすい広告ブロッカー。">クリックひとつでオン/オフ切り替え可能な、使いやすい広告ブロッカー。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//line.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di LINE" height="80" src="//cdn.download.it/gen/line-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="LINE">LINE</h2>
<p class="text-limit-2" title="日本でもっとも人気のメッセンジャー。決済、ゲーム機能など、様々な機能を提供。">日本でもっとも人気のメッセンジャー。決済、ゲーム機能など、様々な機能を提供。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//google-chrome-browser.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Google Chrome" height="80" src="//cdn.download.it/gen/google-chrome-browser-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Google Chrome">Google Chrome</h2>
<p class="text-limit-2" title="軽快さが人気のWebブラウザーGoogle Chrome最新版　Androidなど他端末とタブの同期が可能に">軽快さが人気のWebブラウザーGoogle Chrome最新版　Androidなど他端末とタブの同期が可能に</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//chrome.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Google Chrome" height="80" src="//cdn.download.it/gen/chrome-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Google Chrome">Google Chrome</h2>
<p class="text-limit-2" title="拡張機能により「ブラウザ以上」になるブラウザ">拡張機能により「ブラウザ以上」になるブラウザ</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//vlc-windows.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di VLCメディアプレイヤー" height="80" src="//cdn.download.it/gen/vlc-windows-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="VLCメディアプレイヤー">VLCメディアプレイヤー</h2>
<p class="text-limit-2" title="Blu-rayや DVD、YouTubeまであらゆるコンテンツ対応する万能メディアプレイヤー">Blu-rayや DVD、YouTubeまであらゆるコンテンツ対応する万能メディアプレイヤー</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//lhaplus.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Lhaplus" height="80" src="//cdn.download.it/gen/lhaplus-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Lhaplus">Lhaplus</h2>
<p class="text-limit-2" title="ファイルの圧縮・解凍ソフト。様々な形式に対応し、またパスワードロックも可能。">ファイルの圧縮・解凍ソフト。様々な形式に対応し、またパスワードロックも可能。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//free-5kplayer.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di 5KPlayer" height="80" src="//cdn.download.it/gen/free-5kplayer-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="5KPlayer">5KPlayer</h2>
<p class="text-limit-2" title="YouTubeの動画をダウンロードし、テレビで高品質でそれらを表示するソフト。">YouTubeの動画をダウンロードし、テレビで高品質でそれらを表示するソフト。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//chrome-windows.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Google Chrome" height="80" src="//cdn.download.it/gen/chrome-windows-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Google Chrome">Google Chrome</h2>
<p class="text-limit-2" title="Googleアカウントとの連携がスムーズなWebブラウザー">Googleアカウントとの連携がスムーズなWebブラウザー</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//discord.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Discord" height="80" src="//cdn.download.it/gen/discord-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Discord">Discord</h2>
<p class="text-limit-2" title="ゲーマー向けの無料でチャットツール。">ゲーマー向けの無料でチャットツール。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//audacity.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Audacity" height="80" src="//cdn.download.it/gen/audacity-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Audacity">Audacity</h2>
<p class="text-limit-2" title="無料で、オーディオファイルの編集、サウンドのインポートができるバーチャルスタジオソフト。">無料で、オーディオファイルの編集、サウンドのインポートができるバーチャルスタジオソフト。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//windows-media-player-12.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Windows Media Player" height="80" src="//cdn.download.it/gen/windows-media-player-12-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Windows Media Player">Windows Media Player</h2>
<p class="text-limit-2" title="マイクロソフト製メディアプレーヤー、以前は軽快さに欠けていたが、アップデートされてユーザビリティが向上している。">マイクロソフト製メディアプレーヤー、以前は軽快さに欠けていたが、アップデートされてユーザビリティが向上している。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//windows-live-mail.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Windows Live Mail" height="80" src="//cdn.download.it/gen/windows-live-mail-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Windows Live Mail">Windows Live Mail</h2>
<p class="text-limit-2" title="パソコン初心者でも簡単に使えるマイクロソフト製のメールソフト。機能も豊富。">パソコン初心者でも簡単に使えるマイクロソフト製のメールソフト。機能も豊富。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//vlc-media-player.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di VLC media player" height="80" src="//cdn.download.it/gen/vlc-media-player-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="VLC media player">VLC media player</h2>
<p class="text-limit-2" title="あらゆる形式に対応するメディアプレイヤー">あらゆる形式に対応するメディアプレイヤー</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//powerpoint-2013.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Microsoft PowerPoint 2013" height="80" src="//cdn.download.it/gen/powerpoint-2013-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Microsoft PowerPoint 2013">Microsoft PowerPoint 2013</h2>
<p class="text-limit-2" title="マイクロソフトのプレゼンテーション作成ソフト。">マイクロソフトのプレゼンテーション作成ソフト。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//shukusen.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di 縮小専用。" height="80" src="//cdn.download.it/gen/shukusen-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="縮小専用。">縮小専用。</h2>
<p class="text-limit-2" title="画像サイズの圧縮ソフト。多くの写真や画像をまとめて圧縮可能。">画像サイズの圧縮ソフト。多くの写真や画像をまとめて圧縮可能。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//label-san-home.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di ラベル屋さん" height="80" src="//cdn.download.it/gen/label-san-home-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="ラベル屋さん">ラベル屋さん</h2>
<p class="text-limit-2" title="ラベル・カード作成ソフト。テンプレート機能が強力。">ラベル・カード作成ソフト。テンプレート機能が強力。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//line-windows.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di LINE" height="80" src="//cdn.download.it/gen/line-windows-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="LINE">LINE</h2>
<p class="text-limit-2" title="スタンプを使ったチャットと音声・ビデオ通話をPCで">スタンプを使ったチャットと音声・ビデオ通話をPCで</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//realplayer.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di RealPlayer Cloud for Windows" height="80" src="//cdn.download.it/gen/realplayer-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="RealPlayer Cloud for Windows">RealPlayer Cloud for Windows</h2>
<p class="text-limit-2" title="リアル形式のほか多彩なフォーマット再生に対応、クラウドとも連携したメディアプレーヤー">リアル形式のほか多彩なフォーマット再生に対応、クラウドとも連携したメディアプレーヤー</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//soundengine.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di SoundEngine Free" height="80" src="//cdn.download.it/gen/soundengine-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="SoundEngine Free">SoundEngine Free</h2>
<p class="text-limit-2" title="波形表示に始まり、再生、録音、切り貼り編集、フォーマット変換、音声解析にエフェクト、音響特性の可視化と様々なサウンド編集機能がある、Windows向け波形編集ソフト。">波形表示に始まり、再生、録音、切り貼り編集、フォーマット変換、音声解析にエフェクト、音響特性の可視化と様々なサウンド編集機能がある、Windows向け波形編集ソフト。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//photoscape-free.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Photoscape" height="80" src="//cdn.download.it/gen/photoscape-free-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Photoscape">Photoscape</h2>
<p class="text-limit-2" title="携帯電話やデジタルカメラで撮った写真を簡単に補正、編集できる「おもしろい画像編集ソフト」です。">携帯電話やデジタルカメラで撮った写真を簡単に補正、編集できる「おもしろい画像編集ソフト」です。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//hamachi.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Hamachi" height="80" src="//cdn.download.it/gen/hamachi-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Hamachi">Hamachi</h2>
<p class="text-limit-2" title="仮想プライベートネットワーク（VPN）作成ソフト。">仮想プライベートネットワーク（VPN）作成ソフト。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//google-earth.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Google Earth" height="80" src="//cdn.download.it/gen/google-earth-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Google Earth">Google Earth</h2>
<p class="text-limit-2" title="Googleの衛星画像や航空写真による地球儀ソフト。">Googleの衛星画像や航空写真による地球儀ソフト。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//aviutl.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di AviUtl" height="80" src="//cdn.download.it/gen/aviutl-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="AviUtl">AviUtl</h2>
<p class="text-limit-2" title="HD動画の制作や編集に最適で、拡張機能も豊富な動画編集ソフト">HD動画の制作や編集に最適で、拡張機能も豊富な動画編集ソフト</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//outlook-2013.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Outlook 2013" height="80" src="//cdn.download.it/gen/outlook-2013-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Outlook 2013">Outlook 2013</h2>
<p class="text-limit-2" title="新たな機能が追加されたOutlookの最新版！メール操作や予定の管理がより簡単に">新たな機能が追加されたOutlookの最新版！メール操作や予定の管理がより簡単に</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//winshot.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di WinShot" height="80" src="//cdn.download.it/gen/winshot-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="WinShot">WinShot</h2>
<p class="text-limit-2" title="スクリーンショット、加工、印刷できるキャプチャーソフト。様々な形式に対応。">スクリーンショット、加工、印刷できるキャプチャーソフト。様々な形式に対応。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//wechat.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di WeChat" height="80" src="//cdn.download.it/gen/wechat-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="WeChat">WeChat</h2>
<p class="text-limit-2" title="中国の人気チャットアプリ。">中国の人気チャットアプリ。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//videopad-video-editor.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di VideoPad Video Editor" height="80" src="//cdn.download.it/gen/videopad-jp-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="VideoPad Video Editor">VideoPad Video Editor</h2>
<p class="text-limit-2" title="操作がシンプルな動画編集ソフト。動画の切り貼りが得意。">操作がシンプルな動画編集ソフト。動画の切り貼りが得意。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//thunderbird-7-win.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Thunderbird" height="80" src="//cdn.download.it/gen/thunderbird-7-win-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Thunderbird">Thunderbird</h2>
<p class="text-limit-2" title="Firefoxの開発者、Mozillaのメールクライアント。Outlook同様、カレンダーやTodoなどの機能もある。">Firefoxの開発者、Mozillaのメールクライアント。Outlook同様、カレンダーやTodoなどの機能もある。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//steam.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Steam" height="80" src="//cdn.download.it/gen/steam-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Steam">Steam</h2>
<p class="text-limit-2" title="ゲーマー向けのオールインワンソフト。ダウロードからプレイまで。">ゲーマー向けのオールインワンソフト。ダウロードからプレイまで。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//quicktime.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di QuickTime" height="80" src="//cdn.download.it/gen/quicktime-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="QuickTime">QuickTime</h2>
<p class="text-limit-2" title=".MOVファイルを表示するためのAppleのメディアプレーヤー">.MOVファイルを表示するためのAppleのメディアプレーヤー</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//paint-net.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Paint.NET" height="80" src="//cdn.download.it/gen/paint-net-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Paint.NET">Paint.NET</h2>
<p class="text-limit-2" title="無料写真編集ソフト。写真の色や明るさの繊細に補正を、簡単操作で実現。">無料写真編集ソフト。写真の色や明るさの繊細に補正を、簡単操作で実現。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//microsoft-word.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Microsoft Word 2010" height="80" src="//cdn.download.it/gen/microsoft-word-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Microsoft Word 2010">Microsoft Word 2010</h2>
<p class="text-limit-2" title="マイクロソフトの文書作成・編集ソフト。">マイクロソフトの文書作成・編集ソフト。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//microsoft-powerpoint-2010.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Microsoft PowerPoint" height="80" src="//cdn.download.it/gen/microsoft-powerpoint-2010-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Microsoft PowerPoint">Microsoft PowerPoint</h2>
<p class="text-limit-2" title="ビジネスや個人的な使用のためのMicrosoftのスライドショーやプレゼンテーションソフトウェア">ビジネスや個人的な使用のためのMicrosoftのスライドショーやプレゼンテーションソフトウェア</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//line-for-windows-10.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di LINE for Windows 10" height="80" src="//cdn.download.it/gen/line-for-windows-10-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="LINE for Windows 10">LINE for Windows 10</h2>
<p class="text-limit-2" title="日本でもっとも人気のメッセンジャーLINEのWindows版。">日本でもっとも人気のメッセンジャーLINEのWindows版。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//jw-cad.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Jw_cad" height="80" src="//cdn.download.it/gen/jw-cad-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Jw_cad">Jw_cad</h2>
<p class="text-limit-2" title="自由に線種をカスタマイズできる2次元汎用CADソフト。">自由に線種をカスタマイズできる2次元汎用CADソフト。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//imgburn.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di ImgBurn" height="80" src="//cdn.download.it/gen/imgburn-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="ImgBurn">ImgBurn</h2>
<p class="text-limit-2" title="CD、DVD、HDとブルーレイの画像を記録できる無料ソフト">CD、DVD、HDとブルーレイの画像を記録できる無料ソフト</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//chrome-64-bit.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Google Chrome (64-bit)" height="80" src="//cdn.download.it/gen/chrome-64-bit-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Google Chrome (64-bit)">Google Chrome (64-bit)</h2>
<p class="text-limit-2" title="世界で最も人気のあるWebブラウザの64ビット版">世界で最も人気のあるWebブラウザの64ビット版</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//avast-free-antivirus.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di アバスト! 無料アンチウイルス" height="80" src="//cdn.download.it/gen/avast-free-antivirus-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="アバスト! 無料アンチウイルス">アバスト! 無料アンチウイルス</h2>
<p class="text-limit-2" title="定番の無料セキュリティーソフト。">定番の無料セキュリティーソフト。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//alzip-1.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di ALZip" height="80" src="//cdn.download.it/gen/alzip-1-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="ALZip">ALZip</h2>
<p class="text-limit-2" title="高機能なファイル圧縮・解凍ソフト。多数の形式に対応。">高機能なファイル圧縮・解凍ソフト。多数の形式に対応。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//media-player-classic.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Media Player Classic" height="80" src="//cdn.download.it/gen/media-player-classic-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Media Player Classic">Media Player Classic</h2>
<p class="text-limit-2" title="インストール不要！シンプルで軽快な動作が魅力のメディアプレーヤー 動画も音楽もDVDも再生可能">インストール不要！シンプルで軽快な動作が魅力のメディアプレーヤー 動画も音楽もDVDも再生可能</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//microsoft-office-2013-365-home-premium-preview.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Microsoft Office 2013" height="80" src="//cdn.download.it/gen/microsoft-office-2013-365-home-premium-preview-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Microsoft Office 2013">Microsoft Office 2013</h2>
<p class="text-limit-2" title="タッチ操作やクラウドとの統合などの機能が追加されたMicrosoftのオフィスソフト。">タッチ操作やクラウドとの統合などの機能が追加されたMicrosoftのオフィスソフト。</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//kingsoft-office-suite-free.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Kingsoft Office Free" height="80" src="//cdn.download.it/gen/kingsoft-office-suite-free-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Kingsoft Office Free">Kingsoft Office Free</h2>
<p class="text-limit-2" title="Kingsoft Office Free">Kingsoft Office Free</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//virus-buster.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di ウイルスバスター クラウド" height="80" src="//cdn.download.it/gen/virus-buster-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="ウイルスバスター クラウド">ウイルスバスター クラウド</h2>
<p class="text-limit-2" title="ウイルス対策のほかプライバシー保護も手厚い総合セキュリティ対策ソフトの最新版">ウイルス対策のほかプライバシー保護も手厚い総合セキュリティ対策ソフトの最新版</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//kingsoft-office-free.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Kingsoft Office Free" height="80" src="//cdn.download.it/gen/kingsoft-office-free-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Kingsoft Office Free">Kingsoft Office Free</h2>
<p class="text-limit-2" title="Kingsoft Office Free">Kingsoft Office Free</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//windows-media-player-9.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Windows Media Player" height="80" src="//cdn.download.it/gen/windows-media-player-9-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Windows Media Player">Windows Media Player</h2>
<p class="text-limit-2" title="Windows PC用の最新のメディアプレーヤー">Windows PC用の最新のメディアプレーヤー</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow lower-left-corner ">
<a href="//tencent-gaming-buddy.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Tencent Gaming Buddy" height="80" src="//cdn.download.it/gen/tencent-gaming-buddy-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Tencent Gaming Buddy">Tencent Gaming Buddy</h2>
<p class="text-limit-2" title="PUBGモバイル専用のPC Androidのエミュレータ">PUBGモバイル専用のPC Androidのエミュレータ</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow ">
<a href="//safari.jp.download.it/">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<img alt="Icona di Safari" height="80" src="//cdn.download.it/gen/safari-100x100.png" width="80"/>
</div>
<div class="col">
<h2 class="text-limit-1" title="Safari">Safari</h2>
<p class="text-limit-2" title="あなたはワールドワイドウェブを検索してナビゲートできるユーザーフレンドリーなインターネットブラウザ">あなたはワールドワイドウェブを検索してナビゲートできるユーザーフレンドリーなインターネットブラウザ</p>
</div>
</div>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-tile hvr-glow home-more-wrapper">
<a href="//jp.download.it/windows/popular">
<div class="row mb-3 mb-xl-0">
<div class="col-auto">
<p style="width: 80px;"><i class="fas fa-ellipsis-h" style="width: 100%;margin-top: 10px;"></i></p>
</div>
<div class="col">
<h2 style="margin-top: 27px;color: #999;">その他のプログラム</h2>
</div>
</div>
</a>
</div>
</div>
</section>
<section class="mt-4 pb-4 container im-radont" id="home-cats">
<div class="row justify-content-center mb-lg-3 mt-lg-4">
<div class="col-auto">
<h3>カテゴリ</h3>
</div>
</div>
<div class="row">
<div class="col-12 col-md-6 col-xl-4 home-cat-tile">
<a class="inline-link" href="//jp.download.it/windows/audio">
<span class="hcat-name">音楽・音声・オーディオ</span>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-cat-tile">
<a class="inline-link" href="//jp.download.it/windows/business">
<span class="hcat-name">ビジネス</span>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-cat-tile">
<a class="inline-link" href="//jp.download.it/windows/customize-your-pc">
<span class="hcat-name">PCカスタマイズ</span>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-cat-tile">
<a class="inline-link" href="//jp.download.it/windows/design-photography">
<span class="hcat-name">画像・デザイン関係</span>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-cat-tile">
<a class="inline-link" href="//jp.download.it/windows/development">
<span class="hcat-name">ソフトウェア開発</span>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-cat-tile">
<a class="inline-link" href="//jp.download.it/windows/games">
<span class="hcat-name">ゲーム</span>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-cat-tile">
<a class="inline-link" href="//jp.download.it/windows/home-hobbies">
<span class="hcat-name">家庭・趣味</span>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-cat-tile">
<a class="inline-link" href="//jp.download.it/windows/internet">
<span class="hcat-name">インターネット</span>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-cat-tile">
<a class="inline-link" href="//jp.download.it/windows/microsoft">
<span class="hcat-name">Microsoft</span>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-cat-tile">
<a class="inline-link" href="//jp.download.it/windows/productivity">
<span class="hcat-name">仕事効率化</span>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-cat-tile">
<a class="inline-link" href="//jp.download.it/windows/science-education">
<span class="hcat-name">教育・学術</span>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-cat-tile">
<a class="inline-link" href="//jp.download.it/windows/security">
<span class="hcat-name">セキュリティ</span>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-cat-tile">
<a class="inline-link" href="//jp.download.it/windows/utilities">
<span class="hcat-name">ユーティリティ</span>
</a>
</div>
<div class="col-12 col-md-6 col-xl-4 home-cat-tile">
<a class="inline-link" href="//jp.download.it/windows/video-win">
<span class="hcat-name">動画</span>
</a>
</div>
</div>
</section>
</div>
<footer class="container pt-3">
<div class="row justify-content-center">
<nav class="col-auto dit-flinks-wrap">
<div class="float-left ml-3"><p><a href="//jp.download.it/contact" rel="nofollow" target="_blank">Contact Us</a></p></div>
<div class="float-left ml-3"><a href="//www.iubenda.com/privacy-policy/61913038" rel="nofollow" target="_blank">Privacy policy</a></div>
</nav>
</div>
<div class="row justify-content-around">
<div class="col-auto"><p>Copyright © 2020 download.it. All rights reserved.</p></div>
</div>
</footer>
<script type="text/javascript">
	var sc_project=8652972;
	var sc_invisible=1;
	var sc_security="bad59033";
	var sc_https=1;
	var sc_remove_link=1;
</script>
<script async="" src="https://www.statcounter.com/counter/counter.js" type="text/javascript"></script>
<noscript><div class="statcounter"><img class="statcounter" src="https://c.statcounter.com/8652972/0/bad59033/1/"/></div></noscript>
<script type="text/javascript">(function(){window['__CF$cv$params']={r:'61197e289ac90be0',m:'364aabe1d8921525b57a714ca084ed02e4589987-1610649704-1800-AY7/e7ojY1/LN/wM2u0Eb0PaFUb3pfs8wVdwlzQKIE3XXGrqhpQTBU87hFIxk/xWi2CGvsov++GXcKbPt9tBBTd1vR4t09BtD3AHtdvH0G5myXhUo/Bj2PwZMTGRFqo/UA==',s:[0x603d28608f,0x51d56ce6c4],}})();</script></body>
</html>