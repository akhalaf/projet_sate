<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<link href="/favicon.png" rel="icon" type="image/png"/>
<link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" rel="stylesheet"/>
<link href="CSS/custom.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,800" rel="stylesheet"/>
<title>Zealcon Corporation</title>
</head>
<body>
<nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light">
<div class="container">
<a class="navbar-brand" href="./">
<img alt="Zealcon Logo" src="images/logo-final-web-black-zealcon.png" width="200px"/>
</a>
<button aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarCollapse" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarCollapse">
<ul class="navbar-nav ml-auto">
<li class="nav-item"><a class="nav-link" href="./capabilities">CAPABILITIES</a></li>
<li class="nav-item"><a class="nav-link" href="./contact-us">CONTACT US</a></li>
</ul>
</div>
</div>
</nav>
<main role="main">
<div class="jumbotron hero">
<div class="container">
<h1 class="display-1 text-center hero-heading">EMPOWERING COMPANIES TO SUCCEED</h1>
<p class="hero-subheading text-center">Zealcon has you covered</p>
<p class="text-center">
<a class="btn btn-outline-light" href="./capabilities" role="button">Learn More</a>
</p>
</div>
</div>
<!--		<div class="powered-by-zealcon text-center">
			POWERED BY ZEALCON
		</div>-->
<!-- Jssor Slider Begin -->
<!--	<div class="container-fluid d-none d-lg-block">
			<script src="js/jssor.slider-23.1.6.min.js" type="text/javascript"></script>
			<script type="text/javascript">
					jssor_1_slider_init = function() {

					var jssor_1_options = {
					  $AutoPlay: 1,
					  $Idle: 0,
					  $SlideDuration: 5000,
					  $SlideEasing: $Jease$.$Linear,
					  $PauseOnHover: 12,
					  $SlideWidth: 50,
					  $SlideHeight: 16.66,
					  $Cols: 20,
					  $Align: 300
					};

					var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

					/*responsive code begin*/
					/*remove responsive code if you don't want the slider scales while window resizing*/
					function ScaleSlider() {
						var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
						if (refSize) {
							jssor_1_slider.$ScaleWidth(refSize);
						}
						else {
							window.setTimeout(ScaleSlider, 30);
						}
					}
					ScaleSlider();
					$Jssor$.$AddEvent(window, "load", ScaleSlider);
					$Jssor$.$AddEvent(window, "resize", ScaleSlider);
					$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
					/*responsive code end*/
				};
			</script>

			<div id="jssor_1" style="position:relative;margin:6px auto 6px -15px;top:0px;left:0px;width:600px;height:35px;overflow:hidden;visibility:visible;">
				<!-- Loading Screen -->
<div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
<div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
<div style="position:absolute;display:block;background:url('./images/Icon-White-Zealcon.png') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
</div>
<!--	<div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:700px;height:35px;overflow:hidden;">
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-vf.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-arctic-cat.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-monsanto.jpg" />
					</div>
				   <div class="slider">
						<img data-u="image" src="images/Slider/slider-US-DOD.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-lennox.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-harland-clarke.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-scotts.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-ibm.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-att.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-verizon.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-discovery.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-dart.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-CardinalHealth.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-joy.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-amcor.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-meadjohnson.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-brown.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-reuters.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-pentair.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-firstenergy.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-rockwell.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-lsi.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-tamko.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-conoco.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-ula.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-boeing.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-l3.jpg" />
					</div>

					<div class="slider">
						<img data-u="image" src="images/Slider/slider-bu.jpg" />
					</div>

					<div class="slider">
						<img data-u="image" src="images/Slider/slider-loves.jpg" />
					</div>

					<div class="slider">
						<img data-u="image" src="images/Slider/slider-bridgestone.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-mcg.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-briggsandstratton.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-officedepot.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-centurylink.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-nielsen.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-cooper.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-nike.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-westwardpharma.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-homedepot.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-kindred.jpg" />
					</div>
					<div class="slider">
						<img data-u="image" src="images/Slider/slider-octanner.jpg" />
					</div>
				</div>
			</div>-->
<script type="text/javascript">jssor_1_slider_init();</script>
<!-- #endregion Jssor Slider End -->
<!-- Icons -->
<div class="container">
<div class="row text-center my-5">
<div class="col"><img alt="ERP Solutions Icon" src="images/home/Icons/ERPSolutions.png"/>
<p>ERP Solutions</p>
</div>
<div class="col"><img alt="Application Design Icon" src="images/home/Icons/ApplicationDesign.png"/>
<p>Application Design</p>
</div>
<div class="col"><img alt="Integration Services Icon" src="images/home/Icons/IntegrationServices.png"/>
<p>Integration Services</p>
</div>
<div class="col"><img alt="Graphic Design Icon" src="images/home/Icons/GraphicDesign.png"/>
<p>Graphic Design</p>
</div>
<div class="col"><img alt="Social Media Icon" src="images/home/Icons/SocialMedia.png"/>
<p>Social Media</p>
</div>
</div>
</div>
<!-- Who is Zealcon -->
<div class="jumbotron about-zealcon-bg">
<div class="container">
<h1 class="text-center">WHO IS ZEALCON?</h1>
<p class="text-center about-subheading">Zealcon Application Development &amp; Application Services</p>
<p>Zealcon is an IT and Software Development Firm specializing in a variety of corporate solutions to help companies succeed and remain competitive. Our methodology maximizes and accelerates the return on investment in SAP, Microsoft, Oracle, and Sun technologies across the board. Our methodology implements the right solutions in the right way.
				</p>
<img alt="White Zealcon Icon" class="about-img" src="images/home/Icons/Icon-White-Zealcon.png"/>
<p>
				Zealcon identifies weaknesses and strengths in IT environments and optimizes those systems to meet new challenges and incorporate new technologies. With Zealcon you will meet your system needs of today, and be ready for the breakthroughs of tomorrow.
				</p>
</div>
</div>
<!-- Get Started Now -->
<div class="jumbotron cta-home">
<div class="container">
<h1>GET STARTED NOW</h1>
<p>We offer solutions for businesses of all sizes</p>
<a class="btn btn-outline-light" href="./contact-us" role="button">Get Started</a>
</div>
</div>
<!-- Solutions Grid -->
<!--	<div class="container">
			<h2 class="solutions-title">DISCOVER HOW ZEALCON CAN HELP YOU</h2>
			<div class="row">
				<div class="col-lg-4 solutions-col">
					<div class="card solutions-card">
						<div class="card-body">
							<div class="row">
								<div class="col-9">
									<h5 class="card-title">Intrexx</h5>
									<p class="card-text">Intrexx provides straightforward and manageable access to consolodated data, whether it's for an intranet, extranet, or social collaboration. <br>
										<a href="./capabilities/intrexx.html">Learn More &gt&gt</a></p>
								</div>
								<div class="col-3">
									<img src="images/home/Icons/Card%20-%20Intrexx%20-%20Zealcon.png"/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 solutions-col">
					<div class="card solutions-card">
						<div class="card-body">
							<div class="row">
								<div class="col-9">
									<h5 class="card-title">SAP</h5>
									<p class="card-text">SAP offers real-time analytics to assist with decision making and anticipation business outcomes. <br>
										<a href="./capabilities/sap.html">Learn More &gt&gt</a></p>
								</div>
								<div class="col-3">
									<img src="images/home/Icons/Card%20-%20SAP%20-%20Zealcon.png"/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 solutions-col">
					<div class="card solutions-card">
						<div class="card-body">
							<div class="row">
								<div class="col-9">
									<h5 class="card-title">Mobile</h5>
									<p class="card-text">Mobile friendly sites are a necessity in today's climate, requiring every enterprise to add responsive functionality to their legacy assets. <br>
										<a href="./capabilities">Learn More &gt&gt</a></p>
								</div>
								<div class="col-3">
									<img src="images/home/Icons/Card%20-%20Mobile%20-%20Zealcon.png"/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 solutions-col">
					<div class="card solutions-card">
						<div class="card-body">
							<div class="row">
								<div class="col-9">
									<h5 class="card-title">Java & .Net</h5>
									<p class="card-text">Java is behind human-to-digital interfacing and the world's best applications. <br>
										<a href="./capabilities/java-net.html">Learn More &gt&gt</a></p>
								</div>
								<div class="col-3">
									<img src="images/home/Icons/Card%20-%20Java%20-%20Zealcon.png"/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 solutions-col">
					<div class="card solutions-card">
						<div class="card-body">
							<div class="row">
								<div class="col-9">
									<h5 class="card-title">Shopify</h5>
									<p class="card-text">Shopify is the world's most scalable and robust platform for enterprise eCommerce. Trusted by the best brands in the world. <br>
										<a href="./capabilities/shopify.html">Learn More &gt&gt</a></p>
								</div>
								<div class="col-3">
									<img src="images/home/Icons/Card%20-%20Shopify%20-%20Zealcon.png"/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 solutions-col">
					<div class="card solutions-card">
						<div class="card-body">
							<div class="row">
								<div class="col-9">
									<h5 class="card-title">JOOC Media</h5>
									<p class="card-text">JOOC Media provides graphic design services, digital and print marketing solutions, and social media marketing. <br>
										<a href="./capabilities">Learn More &gt&gt</a></p>
								</div>
								<div class="col-3">
									<img src="images/home/Icons/Card%20-%20JOOC%20-%20Zealcon.png"/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>-->
<!-- Contact US -->
<div class="jumbotron contact-us-bg">
<div class="container">
<div class="row">
<div class="col-lg-7">
<h2 class="mt-5">REACH US</h2>
<p>Please use the contact form on the right side for questions or requests concerning our services. We will respond to messages within 24 hours. <br/>
<br/>ZEALCON Corporation<br/>
						contactus@zealcon.com</p>
</div>
<div class="col-lg-4 text-center offset-lg-1">
<h2 class="mt-4 font-weight-normal">GET IN TOUCH</h2>
<form class="contact-us-form" id="form">
<div class="form-group">
<input class="form-control" id="contactName" name="contactName" placeholder="Name"/>
</div>
<div class="form-group">
<input class="form-control" id="contactEmail" name="contactEmail" placeholder="Email" type="email"/>
</div>
<div class="form-group">
<input class="form-control" id="contactPhone" name="contactPhone" placeholder="Phone Number" type="phone"/>
</div>
<div class="form-group">
<textarea class="form-control" id="contactNotes" name="contactNotes" placeholder="How can we help?" rows="3"></textarea>
</div>
<div class="form-group">
<button class="btn btn-custom btn-block btn-lg" id="submit" type="submit">Submit</button>
</div>
</form>
<div id="returnmessage"></div>
</div>
</div>
</div>
</div>
<footer>
<div class="container">
<div class="mt-5 text-center">
<a href="./">
<img alt="Zealcon Logo" src="images/logo-final-web-black-zealcon.png" width="200px"/>
</a>
</div>
<ul class="nav justify-content-center">
<li class="nav-item">
<a class="nav-link active" href="./">Home</a>
</li>
<li class="nav-item">
<a class="nav-link" href="./capabilities">Capabilities</a>
</li>
<li class="nav-item">
<a class="nav-link" href="./contact-us">Contact Us</a>
</li>
</ul>
</div>
</footer>
</main>
<script crossorigin="anonymous" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script src="./js/contact_form.js"></script>
</body>
</html>
