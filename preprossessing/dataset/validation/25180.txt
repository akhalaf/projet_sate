<!DOCTYPE html>
<html dir="ltr" lang="de-de">
<head>
<meta charset="utf-8"/>
<title>404 - Kategorie nicht gefunden</title>
<link href="/templates/system/css/error.css" rel="stylesheet"/>
<!--[if lt IE 9]><script src="/media/jui/js/html5.js"></script><![endif]-->
</head>
<body>
<div class="error">
<div id="outline">
<div id="errorboxoutline">
<div id="errorboxheader">404 - Kategorie nicht gefunden</div>
<div id="errorboxbody">
<p><strong>Die Seite kann nicht angezeigt werden, weil:</strong></p>
<ol>
<li>Sie ein <strong>veraltetes Lesezeichen</strong> aufgerufen haben.</li>
<li>Sie über eine Suchmaschine einen <strong>veralteten Index dieser Website</strong> aufgerufen haben.</li>
<li>Sie eine <strong>falsche Adresse</strong> aufgerufen haben.</li>
<li>Sie <strong>keinen Zugriff</strong> auf diese Seite haben.</li>
<li>Die angefragte Quelle wurde nicht gefunden!</li>
<li>Während der Anfrage ist ein Fehler aufgetreten!</li>
</ol>
<p><strong>Bitte eine der folgenden Seiten ausprobieren:</strong></p>
<ul>
<li><a href="/index.php" title="Zur Startseite wechseln">Startseite</a></li>
</ul>
<p>Bei Problemen ist der Administrator dieser Website zuständig.</p>
<div id="techinfo">
<p>
				Kategorie nicht gefunden							</p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
