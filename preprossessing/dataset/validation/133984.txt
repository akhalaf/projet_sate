<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/header_only_template.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!--<script id="scantherMobileRedirect" type="text/javascript" src="http://cdn.scanther.com/embed.js?url=http://m.aplusphysics.com/"></script>
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.aplusphysics.com/"> -->
<script type="text/javascript">


  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-3633710-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- InstanceBeginEditable name="doctitle" -->
<title>APlusPhysics - High School Physics and AP Physics Online</title>
<meta content="Anyone can learn and use physics.  We help.  APlusPhysics is an online resource for students taking AP Physics 1, AP Physics 2, AP Physics C, Regents Physics, and Honors Physics" name="description"/>
<meta content="AP Physics C, AP Physics 1, AP Physics 2, physics, AP Physics, Regents Physics, high school physics, physics tutorials, physics videos" name="keywords"/>
<meta content="en" http-equiv="Content-Language"/>
<!-- InstanceEndEditable -->
<!-- <link href="../aplus_styles.css" rel="stylesheet" type="text/css" /> -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="assets/js/iepngfix_tilebg.js" type="text/javascript"></script>
<link href="assets/style/style.css" media="screen, projection" rel="stylesheet"/>
<script src="assets/js/jquery-1.7.1.min.js"></script>
<script src="assets/js/slides.min.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
	$('#slides').slides({
		preload: true,
		preloadImage: 'assets/images/loading.gif',
		play: 5000,
		pause: 2500
	});
});
</script>
<link href="assets/style/superfish.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="assets/js/hoverIntent.js" type="text/javascript"></script>
<script src="assets/js/superfish.js" type="text/javascript"></script>
<script type="text/javascript">
	// initialise plugins
	jQuery(function(){
		jQuery('ul.sf-menu').superfish();
	});
</script>
<!--[if lte IE 7]>
<link rel="stylesheet" type="text/css" href="assets/style/ie7.css">
<![endif]-->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>
<body>
<section class="wrapper">
<header class="pageHeader">
<figure><a href="index.html"><img alt="A Plus Physics" src="assets/images/logo.png"/></a></figure>
<aside>
<h3><a href="mailto:info@aplusphysics.com">info@aplusphysics.com</a></h3>
</aside>
<div class="searchNode">
<script>
  (function() {
    var cx = '010283188818503645754:cyr9fh1prdg';
    var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx; 
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
</div>
<nav>
<ul class="sf-menu">
<li class="firstLevel"><a class="firstLevel" href="index.html"><span class="curve">Home</span></a></li>
<li class="firstLevel"><a class="firstLevel" href="books.html"><span class="curve">Books</span></a>
<ul>
<li><a href="http://aplusphysics.com/ap1/">AP Physics 1</a></li>
<li><a href="http://aplusphysics.com/ap2/">AP Physics 2</a></li>
<li><a href="http://aplusphysics.com/apcm/">AP Physics C</a></li>
<li><a href="http://aplusphysics.com/honors/">Honors</a></li>
<li><a href="http://aplusphysics.com/regents/index.html">Regents</a></li>
<li><a href="http://aplusphysics.com/regents/wb/index.html">Regents Workbook</a></li>
<li><a href="https://itunes.apple.com/us/book/physics/id533859435?mt=13">iPad Book</a></li>
</ul>
</li>
<li class="firstLevel"><a class="firstLevel" href="courses.html"><span class="curve">Courses</span></a>
<ul>
<li><a href="courses/regents/regents-physics.html">Regents</a>
<ul>
<li><a href="http://www.aplusphysics.com/regents/">Book</a></li>
<li><a href="http://aplusphysics.com/courses/calendar/">Calendar</a></li>
<li><a href="http://www.aplusphysics.com/wordpress/regents">Course Notes</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/files/category/3-regents-honors-physics/">Downloads</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/forum/16-honors-and-regents-physics/">Forum</a></li>
<li><a href="courses/regents/Reassessment%20Request%20Form.pdf">Reassessment Request</a></li>
<li><a href="courses/regents/regents-physics.html">Tutorials</a></li>
<li><a href="courses/regents/videos/vid_index.html">Videos</a></li>
<li><a href="courses/regents/worksheets/ws_index.html">Worksheets</a></li>
</ul>
</li>
<li><a href="#">Honors</a>
<ul>
<li><a href="http://www.aplusphysics.com/honors/">Book</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/files/category/3-regents-honors-physics/">Downloads</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/forum/16-honors-and-regents-physics/">Forum</a></li>
<li><a href="courses/honors/honors_physics.html">Tutorials</a></li>
<li><a href="courses/honors/videos/vid_index.html">Videos</a></li>
<li><a href="courses/regents/worksheets/ws_index.html">Worksheets</a></li>
</ul>
</li>
<li><a href="courses/ap-1/AP1_Physics.html">AP Physics 1</a>
<ul>
<li><a href="http://www.aplusphysics.com/ap1/">Book</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/calendar/3-ap-physics-b/">Calendar</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/files/category/4-ap-physics-b12/">Downloads</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/forum/21-ap-physics-b12/">Forum</a></li>
<li><a href="courses/ap-1/AP1_Physics.html#links">Links</a></li>
<li><a href="ap1/ap1-supp.html">Problem Sets</a></li>
<li><a href="educators/AP1Outline.html/index.html">Roadmap</a></li>
<li><a href="courses/ap-1/AP1_Physics.html#ap1">Videos</a></li>
</ul>
</li>
<li><a href="courses/ap-2/AP2_Physics.html">AP Physics 2</a>
<ul>
<li><a href="http://www.aplusphysics.com/ap2/">Book</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/calendar/3-ap-physics-b/">Calendar</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/files/category/4-ap-physics-b12/">Downloads</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/forum/21-ap-physics-b12/">Forum</a></li>
<li><a href="courses/ap-1/AP1_Physics.html#links">Links</a></li>
<li><a href="ap2/ap2-supp.html">Problem Sets</a></li>
<li><a href="educators/AP2Outline.html/index.html">Roadmap</a></li>
<li><a href="courses/ap-1/AP1_Physics.html#ap2">Videos</a></li>
</ul>
</li>
<li><a href="#">AP Physics C</a>
<ul>
<li><a href="http://aplusphysics.com/apcm/">Book</a></li>
<li><a href="http://canvas.instructure.com">CMS (IHS Only)</a></li>
<li><a href="http://aplusphysics.com/courses/calendar/">Calendar</a></li>
<li><a href="http://aplusphysics.com/wordpress/apc/">Course Notes</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/files/category/5-ap-physics-c/">Downloads</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/forum/19-ap-physics-c/">Forum</a></li>
<li><a href="courses/ap-c/APC_Physics.html">Guide Sheets</a></li>
<li><a href="courses/ap-c/videos/APCVidIndex.html">Videos</a></li>
</ul>
</li>
</ul>
</li>
<li class="firstLevel"><a class="firstLevel" href="http://aplusphysics.com/community/"><span class="curve">Community</span></a>
<ul>
<li><a href="http://aplusphysics.com/community/">Forums</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/files/">Downloads</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/blogs/">Blogs</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/videos/">Videos</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/calendar/">Calendar</a></li>
</ul>
</li>
<li class="firstLevel"><a class="firstLevel" href="educators.html"><span class="curve">Educators</span></a>
<ul>
<li><a href="educators/activities/activities_home.html">Activities</a></li>
<li><a href="educators/AP1Outline.html/index.html">AP Roadmaps</a>
<ul>
<li><a href="educators/AP1Outline.html/index.html">AP1 Roadmap</a></li>
<li><a href="educators/AP2Outline.html/index.html">AP2 Roadmap</a></li>
</ul>
</li>
<li><a href="books.html">Books</a>
<ul>
<li><a href="honors/index.html">Honors</a></li>
<li><a href="regents/index.html">Regents</a></li>
<li><a href="https://itunes.apple.com/us/book/physics/id533859435?mt=13">iPad Physics</a></li>
</ul>
</li>
<li><a href="http://aplusphysics.com/community/index.php/files/">Downloads</a></li>
<li><a href="http://aplusphysics.com/community/index.php/forum/20-teachers-lounge/">Forum</a></li>
<li><a href="educators/newsupdates.html">News &amp; Updates</a></li>
<li><a href="http://aplusphysics.com/flux/">Physics in Flux Blog</a></li>
<li><a href="http://aplusphysics.com/educators/simulations/">Simulations</a></li>
<li><a href="educators/STEP.html">STEP Program</a></li>
<li><a href="#">Worksheets</a>
<ul>
<li><a href="courses/regents/worksheets/ws_index.html">Regents</a></li>
</ul>
</li>
</ul>
</li>
<li class="firstLevel"><a class="firstLevel" href="projects.html"><span class="curve">Projects</span></a>
<ul>
<li><a href="projects/catapult.html">Catapults</a></li>
<li><a href="projects/speakers.html">iPod Speakers</a></li>
<li><a href="projects/kerbal.html">Kerbal Space Program</a></li>
<li><a href="projects/mousetrap_car.html">Mousetrap Cars</a></li>
<li><a href="podcasts/pia.html">PIA Podcast</a></li>
<li><a href="projects/rube_goldberg.html">Rube Goldberg Machines</a></li>
<li><a href="projects/water_rockets.html">Water Bottle Rockets</a></li>
<li><a href="http://aplusphysics.com/community/index.php?/forum/14-labs-and-projects/">Project Forum</a></li>
</ul>
</li>
<li class="firstLevel"><a class="firstLevel" href="#"><span class="curve">About</span></a>
<ul>
<li><a href="about/amazon-associate.html">Advertising &amp; Privacy</a></li>
<li><a href="about/background.html">Background</a></li>
<li><a href="about/contributors.html">Contributors</a></li>
<li><a href="about/LaTeX.html">LaTeX Guide</a></li>
<li><a href="https://www.cafepress.com/aplusphysics?nocache=yes">Merchandise &amp; Swag</a></li>
<li><a href="http://www.sillybeagle.com/" target="_blank">Silly Beagle Productions</a></li>
<li><a href="about/terms.html">Policies</a></li>
</ul>
</li>
</ul>
</nav>
</header>
<figure class="pageBanner"> <!-- InstanceBeginEditable name="PageBanner" -->
<div class="slidesWrapper">
<div id="slides">
<div class="slides_container">
<div class="slide"> <a href="http://aplusphysics.com/courses.html"><img alt="Physics Courses" src="assets/images/slide05.png"/></a>
<div class="caption captionShort">
<h2>Physics Courses</h2>
<ul>
<li>AP Physics 1</li>
<li>AP Physics 2</li>
<li>AP Physics C: Mechanics</li>
<li>AP Physics C: E&amp;M</li>
<li>Honors Physics</li>
<li>NY Regents Physics</li>
</ul>
</div>
</div>
<div class="slide"> <a href="http://aplusphysics.com/apcm"><img alt="AP Physics C Companion" src="assets/images/APCCompSlider.png"/></a>
<div class="caption">
<h2>AP Physics C Companion</h2>
<p>The AP Physics C Companion is an easy-to-read companion to the AP Physics C: Mechanics curriculum featuring 350 sample problems with fully worked-out solutions.  This book covers all major topics of the AP Physics C: Mechanics Course.</p><br/>
<h6>AP and Advanced Placement Program are registered trademarks of the College Board, which does not sponsor or endorse this product.</h6>
</div>
</div>
<div class="slide"> <a href="http://aplusphysics.com/ap1"><img alt="AP Physics 1 Essentials" src="assets/images/AP1-slider-2nd.png"/></a>
<div class="caption">
<h2>AP Physics 1</h2>
<p>AP Physics 1 Essentials is a fun and easy-to-read guide covering the essential concepts and applications required for mastery of the AP Physics 1 course, including more than 600 worked-out problems with full solutions and deeper understanding questions.</p><br/>
<h6>AP and Advanced Placement Program are registered trademarks of the College Board, which does not sponsor or endorse this product.</h6>
</div>
</div>
<div class="slide"> <a href="http://aplusphysics.com/ap2"><img alt="AP Physics 2 Essentials" src="assets/images/sliderAP2.png"/></a>
<div class="caption">
<h2>AP Physics 2</h2>
<p>AP Physics 2 Essentials is an easy-to-read companion to the AP Physics 2 curriculum, featuring more than 450 worked-out problems with full solutions covering all major topics of the course such as fluids, thermal physics, electrostatics, circuits, magnetism, optics, and modern physics.</p>
<h6>AP and Advanced Placement Program are registered trademarks of the College Board, which does not sponsor or endorse this product.</h6>
</div>
</div>
<!-- Schrodinger's Cat Game 
         <div class="slide"> <a href="http://store.steampowered.com/app/295490/"><img src="assets/images/SchCatBanner.png" alt="" /></a>
          </div>
         -->
<div class="slide"> <a href="http://aplusphysics.com/educators.html"><img alt="Educator Resources" src="assets/images/slide04.png"/></a>
<div class="caption captionShort">
<h2>Educator Resources</h2>
<ul>
<li>Student Projects</li>
<li>Forensic Investigations</li>
<li>WebQuests</li>
<li>Debates</li>
<li>Worksheets</li>
<li>Forums and Blogs</li>
<li>STEP Program</li>
</ul>
</div>
</div>
<div class="slide"> <a href="http://aplusphysics.com/regents/wb"><img alt="The Ultimate Regents Physics Question and Answer Book" src="assets/images/RegentsQASlider.png"/></a>
<div class="caption">
<h2>The Ultimate Regents Physics Question and Answer Book</h2>
<p>More than 1200 questions and answers from recent Regents Physics exams, organized by topic. Problems are presented in workbook / worksheet format for easy distribution and use in a classroom or at home.</p>
</div>
</div>
<!--
          <div class="slide"> <a href="https://www.roku.com/channels#!details/39766/a-plus-physics"><img src="assets/images/rokubanner.png" alt="" /></a>
            <div class="caption">
              <h2>APlusPhysics Roku App</h2>
              <p>APlusPhysics now has its own app on Roku!</p>
              <p>If you have a Roku player, you can install the app for free and watch all your Regents Physics, Honors Physics, AP Physics 1, AP Physics 2, and AP Physics C physics tutorial videos right on your television.</p>
              <p>Click on the logo at left for more information.</p>
            </div>
          </div>
          -->
<!--<div class="slide"> <a href="https://itunes.apple.com/us/app/physics-regents-buddy/id504592572?ls=1&mt=8"><img src="assets/images/slide06.png" alt="" /></a>
          </div> -->
<div class="slide"> <a href="http://aplusphysics.com/regents"><img alt="Regents Physics Essentials" src="assets/images/slide02.png"/></a>
<div class="caption">
<h2>Regents Physics Essentials</h2>
<p>APlusPhysics: Regents Physics Essentials is a clear and concise roadmap to the entire New York State Regents Physics curriculum, preparing students for success in their high school physics class as well as review for high marks on the Regents Physics Exam.</p>
</div>
</div>
<!--<div class="slide"> <a href="https://play.google.com/store/apps/details?id=com.rhosoft.aplusphysics"><img src="assets/images/GoogleAppSlide.png" alt="" /></a>
            <div class="caption">
            </div>
          </div>-->
<!-- <div class="slide"> <a href="http://aplusphysics.com/honors"><img src="assets/images/slide03.png" alt="" /></a>
            <div class="caption">
              <h2>Honors Physics Essentials</h2>
              <p>Honors Physics Essentials is an easy-to-read guide to algebra-based introductory physics, featuring more than 500 worked-out problems with full solutions.  HPE is designed to assist beginning physics students in high school and college courses as well as standardized exam preparation.</p>
            </div>
          </div> -->
<!--<div class="slide"> <a href="https://itunes.apple.com/us/book/physics/id533859435?mt=13"><img src="assets/images/slide01.png" alt="" /></a>
            <div class="caption">
              <h2>Physics: Fundamentals and Problem Solving</h2>
              <p>Fundamentals and Problem Solving is an interactive book for the iPad featuring more than 500 questions and fully worked out solutions, detailed illustrations, and integrated tutorial videos to help you succeed in introductory physics!</p>
            </div>
          </div> -->
<!--<div class="slide"> <a href="http://www.theuniverseandmore.com" rel="nofollow"><img src="assets/images/polarity.png" alt="" /></a>
          </div> -->
</div>
</div>
</div>
<!-- InstanceEndEditable --></figure>
<article class="contentWrapper">
<section class="Main_Content"> <!-- InstanceBeginEditable name="Main_Content" -->
<h1>Welcome to APlusPhysics</h1>
<img alt="APlusPhysics" class="alignleft" src="assets/images/welcome-thumb.png"/>
<p><strong>Anyone can learn physics.  We help!</strong></p>
<p>Designed to assist high school and college physics students, APlusPhysics is a free online resource that focuses on problem solving, understanding, and real-world applications in the context of courses such as NY Regents Physics, Honors Physics, and AP<sup>*</sup> Physics courses (AP Physics 1, AP Physics 2, AP Physics C - Mechanics, and AP Physics C - Electricity &amp; Magnetism).</p>
<p>We cover key topics in our physics tutorials and physics videos, and demonstrate how the principles and applications of introductory physics extend outside the classroom through:</p>
<dl>
<dt><a href="http://aplusphysics.com/courses.html">Courses</a>:</dt>
<dd>Physics courses targeted toward specific high school physics curricula such as AP Physics 1, AP Physics 2, AP Physics C, NY Regents Physics, and Honors Physics with sample problems, videos, course notes, interactive tests, and tons of sample problems.</dd>
<dt><a href="http://aplusphysics.com/videos.html">Tutorials</a>:</dt>
<dd>Video tutorials for AP Physics 1, AP Physics 2, AP Physics C, Regents Physics, and Honors Physics, walking you through fundamentals and basic problem solving methodologies step-by-step.</dd>
<dt><a href="http://aplusphysics.com/projects.html">Projects</a>:</dt>
<dd>Hands-on applications of physics principles to build skills, understanding and confidence with real-world applications.</dd>
<dt><a href="http://aplusphysics.com/community/">Community</a>:</dt>
<dd>Integrated community providing students and instructors a haven to discuss and debate current topics in physics; seek out and provide help with challenging high school physics problems; build a better understanding of key concepts; and explore current research and frontiers in physics.</dd>
<dt><a href="http://aplusphysics.com/educators.html">Educators</a>:</dt>
<dd>Resources and activities for physics teachers including forensic labs, WebQuests, an educator-only discussion forum and the Semiconductor Technology Enrichment Program.</dd>
<dt><a href="http://aplusphysics.com/community/index.php?/blogs/">Blogs</a>:</dt>
<dd>Individual and group blogs for students and instructors, providing further opportunities to reflect on concepts and applications while fostering our physics learning community, as well as the Physics In Flux blog which explores redefining the traditional high school physics classroom.</dd>
<dt><a href="http://aplusphysics.com/community/index.php?/videos/">Videos</a>:</dt>
<dd>Detailed video repository containing hundreds of lessons, tutorials, demonstrations, and explanations, all sorted by topic and level.</dd>
<dt><a href="http://aplusphysics.com/community/index.php?/files/">Downloads</a>:</dt>
<dd>Worksheets, guidesheets, lessons, practice exams, and electronic books all targeted toward high school physics, including educator-only sections.</dd>
</dl>
<hr/>
<!-- InstanceEndEditable --></section>
<!-- InstanceBeginEditable name="Callouts" -->
<section class="calloutsWrapper">
<aside>
<h3><span>APlusPhysics Forums</span></h3>
<script src="http://feeds.feedburner.com/aplusphysics/VPcW?format=sigpro" type="text/javascript"></script><noscript><p>Subscribe to RSS headline updates from: <a href="http://feeds.feedburner.com/aplusphysics/VPcW"></a><br/>Powered by FeedBurner</p> </noscript>
<p class="readMore"><a href="http://aplusphysics.com/community/">Read All</a></p>
</aside>
<aside>
<h3 class="yellow"><span>Latest Blog Posts</span></h3>
<script src="http://feeds.feedburner.com/APlusPhysicsCommBlogs?format=sigpro" type="text/javascript"></script><noscript><p>Subscribe to RSS headline updates from: <a href="http://feeds.feedburner.com/APlusPhysicsCommBlogs"></a><br/>Powered by FeedBurner</p> </noscript>
<p class="readMore"><a href="http://aplusphysics.com/community/index.php/blogs/">Read All</a></p>
</aside>
<aside>
<h3 class="green"><span>APlusPhysics Tweets</span></h3>
<!-- <script src="http://feeds.feedburner.com/APlusPhysicsNews?format=sigpro" type="text/javascript" ></script><noscript><p>Subscribe to RSS headline updates from: <a href="http://feeds.feedburner.com/APlusPhysicsNews"></a><br/>Powered by FeedBurner</p> </noscript>-->
<a class="twitter-timeline" data-widget-id="291518628745056256" href="https://twitter.com/aplusphysics/favorites" width="280">Favorite Tweets by @aplusphysics</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</aside>
</section>
<p><sup>*AP and Advanced Placement Program are registered trademarks of the College Board, which does not sponsor or endorse this website.</sup></p>
<!-- InstanceEndEditable --></article>
<div class="clear"></div>
</section>
<footer class="pageFooter">
<section class="wrapper">
<figure><a href="http://www.sillybeagle.com" target="_blank"><img alt="Silly Beagle Productions" src="assets/images/silly-beagle-logo.png"/></a></figure>
<figure>
<figcaption>Follow Us</figcaption>
<a href="https://twitter.com/aplusphysics"><img alt="Twitter" src="assets/images/twitter-icon.png"/></a><a href="https://www.facebook.com/pages/Aplusphysics/217361071607226"><img alt="Facebook" src="assets/images/facebook-icon.png"/></a><a href="http://www.linkedin.com/in/aplusphysics/"><img alt="LinkedIn" src="assets/images/linkedin-icon.png"/></a><a href="http://www.youtube.com/user/fizziksguy"><img alt="YouTube" src="assets/images/youtube-icon.png"/></a><a href="https://plus.google.com/+DanFullerton"><img alt="Google+" src="assets/images/google-icon.png"/></a></figure>
<nav>
<ul>
<li><a href="index.html">Home</a></li>
<li><a href="books.html">Books</a></li>
<li><a href="courses.html">Courses</a></li>
<li><a href="http://aplusphysics.com/community/">Community</a></li>
<li><a href="educators.html">Educators</a></li>
<li><a href="projects.html">Projects</a></li>
<li><a href="mailto:info@aplusphysics.com">Contact</a></li>
</ul>
</nav>
<summary>Copyright © 2017 <a href="http://www.sillybeagle.com">Silly Beagle Productions</a> | All Rights Reserved.</summary>
<div class="clear"></div>
</section>
</footer>
</body>
<!-- InstanceEnd --></html>