<!DOCTYPE html>
<html dir="ltr" lang="ru">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Пропан .Ру - Информация</title>
<link href="/forum/app.php/feed?sid=df941e759ecae9f1edc76e5ad068641c" rel="alternate" title="Канал - Пропан .Ру" type="application/atom+xml"/> <link href="/forum/app.php/feed/news?sid=df941e759ecae9f1edc76e5ad068641c" rel="alternate" title="Канал - Новости" type="application/atom+xml"/> <link href="/forum/app.php/feed/forums?sid=df941e759ecae9f1edc76e5ad068641c" rel="alternate" title="Канал - Все форумы" type="application/atom+xml"/> <link href="/forum/app.php/feed/topics?sid=df941e759ecae9f1edc76e5ad068641c" rel="alternate" title="Канал - Новые темы" type="application/atom+xml"/> <link href="/forum/app.php/feed/topics_active?sid=df941e759ecae9f1edc76e5ad068641c" rel="alternate" title="Канал - Активные темы" type="application/atom+xml"/>
<!--
	phpBB style name: prosilver
	Based on style:   prosilver (this is the default phpBB3 style)
	Original author:  Tom Beddard ( http://www.subBlue.com/ )
	Modified by:
-->
<link href="./assets/css/font-awesome.min.css?assets_version=162" rel="stylesheet"/>
<link href="./styles/prosilver/theme/stylesheet.css?assets_version=162" rel="stylesheet"/>
<link href="./styles/prosilver/theme/ru/stylesheet.css?assets_version=162" rel="stylesheet"/>
<link href="./assets/cookieconsent/cookieconsent.min.css?assets_version=162" rel="stylesheet"/>
<!--[if lte IE 9]>
	<link href="./styles/prosilver/theme/tweaks.css?assets_version=162" rel="stylesheet">
<![endif]-->
<script>
	var ajax_path = '';
	</script>
<link href="/forum/app.php/smartfeed/feed?sid=df941e759ecae9f1edc76e5ad068641c" rel="alternate" title="ATOM" type="application/atom+xml"/>
<link href="/forum/app.php/smartfeed/feed?y=2&amp;sid=df941e759ecae9f1edc76e5ad068641c" rel="alternate" title="RSS" type="application/rss+xml"/>
<style>
</style>
<link href="./ext/david63/sitelogo/styles/prosilver/theme/site_logo_common.css?assets_version=162" media="screen" rel="stylesheet"/>
<link href="./ext/galandas/lasttopics/styles/all/theme/style.css?assets_version=162" media="screen" rel="stylesheet"/>
<link href="./ext/oxpus/dlext/styles/prosilver/theme/dl_ext.css?assets_version=162" media="screen" rel="stylesheet"/>
<link href="./ext/tatiana5/newyear/styles/all/theme/newyear.css?assets_version=162" media="screen" rel="stylesheet"/>
<link href="./ext/tatiana5/popuppm/styles/all/theme/popuppm.css?assets_version=162" media="screen" rel="stylesheet"/>
<link href="./ext/phpbb/ads/styles/all/theme/phpbbads.css?assets_version=162" media="screen" rel="stylesheet"/>
<link href="./ext/vse/lightbox/styles/all/template/lightbox/css/lightbox.min.css?assets_version=162" media="screen" rel="stylesheet"/>
<style>
		@media (min-width: 900px) {
						.content img.postimage {
				max-height: 800px !important;
				max-width: 1024px !important;
			}
		}
	</style>
</head>
<body class="nojs notouch section-app/downloads ltr " id="phpbb">
<div class="elka_responsive">
<i class="fa fa-power-off garland-on"></i>
<div class="elka">
<div class="garland_3" id="garland">
<div id="nums_1">4</div>
</div>
</div>
</div>
<div class="wrap" id="wrap">
<a accesskey="t" class="top-anchor" id="top"></a>
<div id="page-header">
<div class="headerbar" role="banner">
<span aria-hidden="true" style="position: absolute; overflow: hidden; z-index: -10; width: 1px; height: 1px; padding: 0; margin: 0; border: none;">
<a href="./ucp.php?mode=register&amp;ref=d984045c&amp;sid=df941e759ecae9f1edc76e5ad068641c" rel="nofollow" tabindex="-1">
<i class="icon fa-pencil-square-o fa-fw"></i><span>Регистрация</span>
</a>
</span>
<div class="inner">
<div class="site-description" id="site-description">
<a class="logo" href="https://www.propan.ru/" id="logo" title="Пропан ГБО">
<span class="site_logo"></span>
</a>
<h1>Пропан .Ру</h1>
<p>Газобаллонное оборудование для автомобилей</p>
<p class="skiplink"><a href="#start_here">Пропустить</a></p>
</div>
</div>
</div>
<div data-phpbb-ads-id="2" style="margin: 10px 0;">
<div style="text-align:center;">
<!-- Yandex.RTB R-A-139749-1 -->
<div id="yandex_rtb_R-A-139749-1"></div></div>
<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-139749-1",
                renderTo: "yandex_rtb_R-A-139749-1",
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script>
</div>
<div class="navbar" role="navigation">
<div class="inner">
<ul class="nav-main linklist" id="nav-main" role="menubar">
<li class="quick-links dropdown-container responsive-menu hidden" data-skip-responsive="true" id="quick-links">
<a class="dropdown-trigger" href="#">
<i aria-hidden="true" class="icon fa-bars fa-fw"></i><span>Ссылки</span>
</a>
<div class="dropdown">
<div class="pointer"><div class="pointer-inner"></div></div>
<ul class="dropdown-contents" role="menu">
<li class="separator"></li>
</ul>
</div>
</li>
<li data-last-responsive="true">
<a href="/forum/app.php/smartfeed/ui?sid=df941e759ecae9f1edc76e5ad068641c" role="menuitem" title="Канал Новостей">
<i aria-hidden="true" class="icon fa-rss fa-fw"></i><span>Канал Новостей</span>
</a>
</li>
<li data-skip-responsive="true">
<a href="/forum/app.php/help/faq?sid=df941e759ecae9f1edc76e5ad068641c" rel="help" role="menuitem" title="Часто задаваемые вопросы">
<i aria-hidden="true" class="icon fa-question-circle fa-fw"></i><span>FAQ</span>
</a>
</li>
<li data-last-responsive="true">
<a href="/forum/app.php/dlext/index?sid=df941e759ecae9f1edc76e5ad068641c" role="menuitem" title="Загрузки">
<i aria-hidden="true" class="icon fa-download fa-fw"></i><span>Загрузки</span>
</a>
</li>
<li class="rightside" data-skip-responsive="true">
<a accesskey="x" href="./ucp.php?mode=login&amp;redirect=app.php%2Fdownloads.php&amp;sid=df941e759ecae9f1edc76e5ad068641c" role="menuitem" title="Вход">
<i aria-hidden="true" class="icon fa-power-off fa-fw"></i><span>Вход</span>
</a>
</li>
<li class="rightside" data-skip-responsive="true">
<a href="./udomtzpr?sid=df941e759ecae9f1edc76e5ad068641c" role="menuitem">
<i aria-hidden="true" class="icon fa-pencil-square-o fa-fw"></i><span><b style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">Р</b><big style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">е</big><b style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">г</b><s style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">и</s><big style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">с</big><u style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">т</u><i style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">р</i><s style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">а</s><span style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">ц</span><small style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">и</small><s style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">я</s></span>
</a>
</li>
</ul>
<ul class="nav-breadcrumbs linklist navlinks" id="nav-breadcrumbs" role="menubar">
<li class="breadcrumbs" itemscope="" itemtype="https://schema.org/BreadcrumbList">
<span class="crumb" itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem"><a data-navbar-reference="home" href="https://www.propan.ru/" itemprop="item"><i aria-hidden="true" class="icon fa-home fa-fw"></i><span itemprop="name">Пропан ГБО</span></a><meta content="1" itemprop="position"/></span>
<span class="crumb" itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem"><a accesskey="h" data-navbar-reference="index" href="./index.php?sid=df941e759ecae9f1edc76e5ad068641c" itemprop="item"><span itemprop="name">Газобаллонное оборудование для автомобилей</span></a><meta content="2" itemprop="position"/></span>
</li>
</ul>
</div>
</div>
</div>
<a class="anchor" id="start_here"></a>
<div class="page-body" id="page-body" role="main">
		
		﻿		<div class="noshow" id="dl_help_popup">
<div class="dl_help_close"></div>
<div id="dl_help_content">
<h2 class="dl-text-bold" id="dl_help_title"> </h2>
<h2 id="dl_help_option"> </h2>
<p id="dl_help_string"> </p>
</div>
</div>
<div id="dl_help_bg"></div>
<div class="panel" id="message">
<div class="inner">
<h2 class="message-title">Информация</h2>
<p>Запрошенная страница не найдена.</p>
</div>
</div>
</div>
<div data-phpbb-ads-id="2" style="margin: 10px 0; clear: both;">
<div style="text-align:center;">
<!-- Yandex.RTB R-A-139749-1 -->
<div id="yandex_rtb_R-A-139749-1"></div></div>
<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-139749-1",
                renderTo: "yandex_rtb_R-A-139749-1",
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script>
</div>
<div class="page-footer" id="page-footer" role="contentinfo">
<div class="navbar" role="navigation">
<div class="inner">
<ul class="nav-footer linklist" id="nav-footer" role="menubar">
<li class="breadcrumbs">
<span class="crumb"><a data-navbar-reference="home" href="https://www.propan.ru/"><i aria-hidden="true" class="icon fa-home fa-fw"></i><span>Пропан ГБО</span></a></span> <span class="crumb"><a data-navbar-reference="index" href="./index.php?sid=df941e759ecae9f1edc76e5ad068641c"><span>Газобаллонное оборудование для автомобилей</span></a></span> </li>
<li class="rightside">Часовой пояс: <span title="Европа/Москва">UTC+03:00</span></li>
<li class="rightside">
<a data-ajax="true" data-refresh="true" href="./ucp.php?mode=delete_cookies&amp;sid=df941e759ecae9f1edc76e5ad068641c" role="menuitem">
<i aria-hidden="true" class="icon fa-trash fa-fw"></i><span>Удалить cookies</span>
</a>
</li>
<li aria-hidden="true" class="rightside" data-last-responsive="true" style="position: absolute; overflow: hidden; z-index: -10; width: 1px; height: 1px; padding: 0; margin: 0; border: none;">
<a href="./memberlist.php?mode=contactwithadmin&amp;sid=df941e759ecae9f1edc76e5ad068641c" rel="nofollow" role="menuitem" tabindex="-1">
<i aria-hidden="true" class="icon fa-envelope fa-fw"></i><span>Связаться с администрацией</span>
</a>
</li>
<li class="rightside" data-last-responsive="true">
<a href="./memberlist.php?mode=contactadmin&amp;sid=df941e759ecae9f1edc76e5ad068641c" role="menuitem">
<i aria-hidden="true" class="icon fa-envelope fa-fw"></i><span><small style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">С</small><small style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">в</small><b style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">я</b><samp style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">з</samp><s style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">а</s><big style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">т</big><s style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">ь</s><samp style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">с</samp><samp style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">я</samp><span style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;"> </span><s style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">с</s><b style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;"> </b><i style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">а</i><mark style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">д</mark><span style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">м</span><s style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">и</s><samp style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">н</samp><mark style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">и</mark><u style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">с</u><big style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">т</big><b style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">р</b><small style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">а</small><s style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">ц</s><big style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">и</big><mark style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">е</mark><b style="font: inherit; text-decoration: inherit; color: inherit; background: none; padding: 0; margin: 0; display: inline;">й</b></span>
</a>
</li>
</ul>
</div>
</div>
<div class="copyright">
<p class="footer-row">
<span class="footer-copyright">Создано на основе <a href="https://www.phpbb.com/">phpBB</a>® Forum Software © phpBB Limited</span>
</p>
<p class="footer-row">
<span class="footer-copyright"><a href="https://www.phpbbguru.net">Русская поддержка phpBB</a></span>
</p>
<br/><a href="http://www.phpbb-work.ru/">Моды и расширения phpBB</a> <p class="footer-row" role="menu">
<a class="footer-link" href="./ucp.php?mode=privacy&amp;sid=df941e759ecae9f1edc76e5ad068641c" role="menuitem" title="Конфиденциальность">
<span class="footer-link-text">Конфиденциальность</span>
</a>
			|
			<a class="footer-link" href="./ucp.php?mode=terms&amp;sid=df941e759ecae9f1edc76e5ad068641c" role="menuitem" title="Правила">
<span class="footer-link-text">Правила</span>
</a>
</p>
</div>
<div class="darkenwrapper" data-ajax-error-text="При обработке запроса произошла ошибка." data-ajax-error-text-abort="Запрос прерван пользователем." data-ajax-error-text-parsererror="При выполнении запроса возникла непредвиденная ошибка, и сервер вернул неверный ответ." data-ajax-error-text-timeout="Время запроса истекло; повторите попытку." data-ajax-error-title="Ошибка AJAX" id="darkenwrapper">
<div class="darken" id="darken"> </div>
</div>
<div class="phpbb_alert" data-l-err="Ошибка" data-l-timeout-processing-req="Время выполнения запроса истекло." id="phpbb_alert">
<a class="alert_close" href="#">
<i aria-hidden="true" class="icon fa-times-circle fa-fw"></i>
</a>
<h3 class="alert_title"> </h3><p class="alert_text"></p>
</div>
<div class="phpbb_alert" id="phpbb_confirm">
<a class="alert_close" href="#">
<i aria-hidden="true" class="icon fa-times-circle fa-fw"></i>
</a>
<div class="alert_text"></div>
</div>
</div>
</div>
<div>
<a accesskey="z" class="anchor" id="bottom"></a>
<img alt="cron" height="1" src="/forum/app.php/cron/cron.task.feedpostbot_cron?sid=df941e759ecae9f1edc76e5ad068641c" width="1"/></div>
<script src="./assets/javascript/jquery-3.5.1.min.js?assets_version=162"></script>
<script src="./assets/javascript/core.js?assets_version=162"></script>
<script src="./assets/cookieconsent/cookieconsent.min.js?assets_version=162"></script>
<script>
		if (typeof window.cookieconsent === "object") {
			window.addEventListener("load", function(){
				window.cookieconsent.initialise({
					"palette": {
						"popup": {
							"background": "#0F538A"
						},
						"button": {
							"background": "#E5E5E5"
						}
					},
					"theme": "classic",
					"content": {
						"message": "\u042D\u0442\u043E\u0442\u0020\u0441\u0430\u0439\u0442\u0020\u0438\u0441\u043F\u043E\u043B\u044C\u0437\u0443\u0435\u0442\u0020cookies\u0020\u0434\u043B\u044F\u0020\u043E\u0431\u0435\u0441\u043F\u0435\u0447\u0435\u043D\u0438\u044F\u0020\u0441\u0432\u043E\u0435\u0439\u0020\u043A\u043E\u0440\u0440\u0435\u043A\u0442\u043D\u043E\u0439\u0020\u0440\u0430\u0431\u043E\u0442\u044B.",
						"dismiss": "\u0421\u043E\u0433\u043B\u0430\u0441\u0435\u043D",
						"link": "\u041F\u043E\u0434\u0440\u043E\u0431\u043D\u0435\u0435",
						"href": "./ucp.php?mode=privacy&amp;sid=df941e759ecae9f1edc76e5ad068641c"
					}
				});
			});
		}
	</script>
<!-- Send the variables to the js file -->
<script>
	var backgroundImage		= '.\/styles\/prosilver\/theme\/images\/bg_header.gif\u003Fsid\u003Ddf941e759ecae9f1edc76e5ad068641c';
	var bannerHeight		= '100';
	var borderRadius		= '10';
	var headerColour		= '\u002312a3eb';
	var headerColour1		= '\u00236aceff';
	var headerColour2		= '\u002376b1';
	var logoCorners 		= '7px\u00207px\u00207px\u00207px';
	var logoHeight 			= '138';
	var logoWidth 			= '250';
	var overrideColour		= '\u0023000000';
	var removeHeaderBar		= '0';
	var repeatBackground	= '0';
	var responsive			= '1';
	var searchBelow			= '';
	var siteLogo 			= 'url\u0028\u0022.\/images\/lpgcng.png\u003Fsid\u003Ddf941e759ecae9f1edc76e5ad068641c\u0022\u0029';
	var siteLogoUrl			= '.\/\u003Fsid\u003Ddf941e759ecae9f1edc76e5ad068641c';
	var siteLogoBanner		= '.\/\u003Fsid\u003Ddf941e759ecae9f1edc76e5ad068641c';
	var siteLogoCentre		= '';
	var siteLogoRemove		= '0';
	var siteLogoRight		= '';
	var siteNameSupress		= '';
	var useBackground		= '';
	var useBanner			= '';
	var useHeaderColour		= '';
	var useLogoUrl			= '';
	var useOverrideColour	= '0';
</script>
<script type="text/javascript">
// <![CDATA[
    $( document ).ready(function () {
        $('#vertical-ticker').totemticker({
        row_height  :   '20px',
		next		:	'#ticker-next',
		previous	:	'#ticker-previous',
		stop		:	'#stop',
		start		:	'#start',
		mousestop	:	true,
		direction	:	'down'
    });
});
// ]]>	
</script>
<script>
	function n4H7xYfFKi8lVqq9(){var a=document.getElementsByName('form_token');if(document.getElementById('uWgKUigev1RVxLON')==null){var b="0bkv23bnvwqxmgwm"}else{var b=document.getElementById('uWgKUigev1RVxLON').value;document.getElementById('uWgKUigev1RVxLON').remove()}for(var i=0;i<a.length;i++){a[i].value=a[i].value.replace(b,'')}}n4H7xYfFKi8lVqq9();$('#qr_posts').on('qr_loaded',n4H7xYfFKi8lVqq9);$('#qr_postform').on('qr_outdated_form',n4H7xYfFKi8lVqq9);$('#qr_postform').on('qr_captcha_refreshed',n4H7xYfFKi8lVqq9);
</script>
<script>
	var garland_steps = [3, 0, 3, 0, 3, 3, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 0];

	function garland(garland_img) {
		var nums = $('#nums_1').html();

		//garland_img.attr('class', 'garland_' + (nums % 4));
		//$('#nums_1').html((nums % 4) + 1);
		
		garland_img.attr('class', 'garland_' + garland_steps[nums - 1]);
		$('#nums_1').html((nums % 21) + 1);
	}

	function stop_garland() {
		clearInterval(garland_interval);
		$('#garland').attr('class', 'garland_3');
		$('#nums_1').html('4');
	}

	function start_garland() {
		var garland_img = $('#garland');
		garland_interval = setInterval(function(){garland(garland_img)}, 550);
	}

	$('i.garland-on, i.garland-off').click(function() {
		if ($(this).hasClass('garland-on')) {
			stop_garland();
			localStorage['garland'] = 'off';
		} else {
			start_garland();
			localStorage['garland'] = 'on';
		}

		$(this).toggleClass('garland-on').toggleClass('garland-off');
	});

	function ball_animate()	{
		$('.christmas-ball').remove();
		var balls_count = Math.floor($(window).width() / 330);

		if ($(window).width() % 330 > 130) {
			balls_count += 1;
		}

		for (let i = 0; i < balls_count; i++)
		{
			$('.elka').append('<div class="christmas-ball" style="left: ' + (330*i+81) + 'px;"></div>');
		}

		$('.elka').attr('style', 'background-image: url("./ext/tatiana5/newyear/styles/all/theme/images/elochka2.png");');		
	}

	$(window).on('resize', ball_animate);

	$('.elka').on('click', '.christmas-ball',  function() {
		if (localStorage['garland'] == 'on')
		{
			old_next_position = 147;
			start_ball_animation($(this), 3000, 147);
		}
	});

	function start_ball_animation(ball, time, position) {
		var pos_step = 12,
			time_step = -70,
			position_start = 147,
			next_position = (position > 0) ? position : (position_start + position),
			is_stop_animation = false;

		ball.animate({
			'background-position-x': next_position + 'px',
		}, time, 'swing', function() {
			time+= time_step;

			if (position > 0)
			{
				position-= pos_step;
				is_stop_animation = (Math.abs(next_position - position_start) > position_start/2 + 2*pos_step);
			} else {
				position += pos_step;
				is_stop_animation = (Math.abs(next_position - position_start) < position_start/2 + 2*pos_step);
			}

			position = -position;

			if (is_stop_animation) {
				ball.stop();
				next_position = (position > 0) ? (next_position + Math.round(3*pos_step/4)) : (next_position - Math.round(3*pos_step/4));
				ball.animate({
					'background-position-x': next_position + 'px'
				}, 1000, 'swing');
			} else {
				start_ball_animation(ball, time, position);
			}
		});
	}

	$(document).ready(function() {
		if ((typeof localStorage['garland'] === "undefined") || localStorage['garland'] == 'on') {
			localStorage['garland'] = 'on'
			start_garland();
		} else {
			$('i.garland-on').toggleClass('garland-on').toggleClass('garland-off');
		}

		ball_animate();
	});
</script> <script>
			var nyd_snow_position = false,
				nyd_url = 'https://www.propan.ru/forum/' + 'ext/tatiana5/newyeardecor/styles/all/theme/';
		</script>
<script>
		var vseLightbox = {};
		vseLightbox.resizeHeight = 800;
		vseLightbox.resizeWidth = 1024;
		vseLightbox.lightboxGal = 1;
		vseLightbox.lightboxSig = 0;
		vseLightbox.imageTitles = 0;
		vseLightbox.lightboxAll = 1;
		vseLightbox.downloadFile = 'download/file.php';
	</script>
<script src="./ext/oxpus/dlext/assets/javascript/dl_footer.js?assets_version=162"></script>
<script src="./styles/prosilver/template/forum_fn.js?assets_version=162"></script>
<script src="./styles/prosilver/template/ajax.js?assets_version=162"></script>
<script src="./ext/david63/sitelogo/styles/all/template/site_logo_min.js?assets_version=162"></script>
<script src="./ext/galandas/lasttopics/styles/all/template/jquery.totemticker.js?assets_version=162"></script>
<script src="./ext/vse/lightbox/styles/all/template/js/resizer.js?assets_version=162"></script>
<script src="./ext/vse/lightbox/styles/all/template/lightbox/js/lightbox.min.js?assets_version=162"></script>
<script>
	lightbox.option({
		'albumLabel': '\u0418\u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u0438\u0435\u0020\u00251\u0020\u0438\u0437\u0020\u00252'
	});
</script>
</body>
</html>
