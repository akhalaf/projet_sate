<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" lang="ro-ro" xml:lang="ro-ro" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="ro-ro" name="language"/>
<title>404 - Eroare: 404</title>
<link href="/templates/system/css/system.css" rel="stylesheet" type="text/css"/>
<link href="/templates/contexp/css/position.css" media="screen,projection" rel="stylesheet" type="text/css"/>
<link href="/templates/contexp/css/layout.css" media="screen,projection" rel="stylesheet" type="text/css"/>
<link href="/templates/contexp/css/print.css" media="Print" rel="stylesheet" type="text/css"/>
<link href="/templates/contexp/css/.css" rel="stylesheet" type="text/css"/>
<link href="/templates/contexp/css/.css" rel="stylesheet" type="text/css"/>
<!--[if lte IE 6]>
			<link href="/templates/contexp/css/ieonly.css" rel="stylesheet" type="text/css" />
		<![endif]-->
<!--[if IE 7]>
			<link href="/templates/contexp/css/ie7only.css" rel="stylesheet" type="text/css" />
		<![endif]-->
<!--[if lt IE 9]>
			<script src="/media/jui/js/html5.js"></script>
		<![endif]-->
<style type="text/css">
			<!--
			#errorboxbody
			{margin:30px}
			#errorboxbody h2
			{font-weight:normal;
			font-size:1.5em}
			#searchbox
			{background:#eee;
			padding:10px;
			margin-top:20px;
			border:solid 1px #ddd
			}
			-->
</style>
</head>
<body>
<div id="all">
<div id="back">
<div id="header">
<div class="logoheader">
<h1 id="logo">
<span class="header1">
</span></h1>
</div><!-- end logoheader -->
<ul class="skiplinks">
<li><a class="u2" href="#wrapper2">TPL_BEEZ3_SKIP_TO_ERROR_CONTENT</a></li>
<li><a class="u2" href="#nav">TPL_BEEZ3_ERROR_JUMP_TO_NAV</a></li>
</ul>
<div id="line"></div>
</div><!-- end header -->
<div id="contentarea2">
<div class="left1" id="nav">
<h2 class="unseen">TPL_BEEZ3_NAVIGATION</h2>
<ul class="nav menu">
<li class="item-103"><a class="first" href="/ro/termeni-si-conditii">Termeni și Condiții</a></li><li class="item-114"><a href="/ro/sitemap">Sitemap</a></li><li class="item-115"><a href="/ro/contact">Contact</a></li></ul>
</div>
<!-- end navi -->
<div id="wrapper2">
<div id="errorboxbody">
<h2>A apărut o eroare.<br/>
								Pagina solicitată nu a fost găsită.</h2>
<div>
<p><a href="/index.php" title="Mergeți la prima pagină">Prima pagină</a></p>
</div>
<h3>Dacă dificultățile persistă, va rugăm să contactați administratorul de sistem al acestui site și să raportați eroarea de mai jos.</h3>
<h2>#404 Articolul nu a fost găsit</h2> <br/>
</div><!-- end wrapper -->
</div><!-- end contentarea -->
</div> <!--end all -->
</div>
</div>
<div id="footer-outer">
<div id="footer-sub">
<div id="footer">
<p>
                                                TPL_BEEZ3_POWERED_BY <a href="http://www.joomla.org/">Joomla!®</a>
</p>
</div><!-- end footer -->
</div><!-- end footer-sub -->
</div>
</body>
</html>
