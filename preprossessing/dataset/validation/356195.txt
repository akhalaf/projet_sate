<html><body><p>ï»¿<!DOCTYPE html>

</p>
<meta charset="utf-8"/>
<meta content="index,nofollow" name="robots"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<title>Consulta na Receita Federal - SituaÃ§Ã£o Cadastral, CND, Simples Nacional</title>
<meta content="ServiÃ§o automatizado atravÃ©s da troca de arquivos ou WebService - API. Agilize sua Ã¡rea fiscal e atualize seu cadastro." name="description"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://www.consultareceitafederal.com.br" rel="canonical"/>
<link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="css/all.css" rel="stylesheet"/>
<link href="style_rf.css" rel="stylesheet"/>
<script src="https://www.google.com/recaptcha/api.js?render=6LcLtBoaAAAAACRGHTIG7r94CWNmVOw4_IGAD_Ya"></script>
<script crossorigin="anonymous" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<!--Header Area Start-->
<header class="header-four">
<!--Mainmenu Start-->
<div class="sticker" id="main-menu">
<div class="container">
<div class="row">
<div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
<div class="logo">
<a href="#banner"><img alt="Logotipo do Site" src="img/logo/logo.png" title="Ir para a Home"/></a>
</div>
</div>
<div class="col-lg-10 col-md-10 hidden-sm hidden-xs">
<div class="main-menu float-right">
<nav>
<ul>
<li class="active"><a href="#banner">Home</a></li>
<li><a href="#aboutEMP">Empresa</a></li>
<li><a href="#aboutRF">Consulta Receita Federal</a></li>
<li><a href="#reviews">Clientes</a></li>
<li><a href="#contact" title="Clique e solicite um orÃ§amento!">Contato</a></li>
</ul>
</nav>
</div>
</div>
<div class="col-sm-12 hidden-lg hidden-md">
<div class="mobile-menu">
<nav>
<ul>
<li class="active"><a href="#banner">Home</a></li>
<li><a href="#aboutEMP">Empresa</a></li>
<li><a href="#aboutRF">Consulta Receita Federal</a></li>
<li><a href="#reviews">Clientes</a></li>
<li><a href="#contact" title="Clique e solicite um orÃ§amento!">Contato</a></li>
</ul>
</nav>
</div>
</div>
</div>
</div>
</div>
<!--End of Mainmenu-->
</header>
<!--End of Header Area-->
<!--Banner Area Start-->
<div class="banner-area banner-image" id="banner">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="banner-text">
<div class="banner-table-cell">
<h1>CONSULTA<span> RECEITA FEDERAL</span></h1>
<p>
                                Empresa especializada em sistemas de pesquisas a Ã³rgÃ£os pÃºblicos hÃ¡ 10 anos no mercado
                                <br/>Dispomos de diversos tipos de serviÃ§os para agilizar os processos de sua empresa
                                <br/>Consulte na Receita Federal diretamente de seu sistema ou ERP
                            </p>
<div class="banner-buttons">
<a href="#lnkcontact"><button class="button-default button-olive" type="button">Solicitar OrÃ§amento</button></a>
</div>
</div>
</div>
<div class="banner-apps">
<div class="single-app wow zoomIn" data-wow-delay="2s" data-wow-duration="3s">
<div class="single-app-table-cell">
<i class="zmdi zmdi-apple"></i>
<h4>ios</h4>
<h3>102K</h3>
</div>
</div>
<div class="single-app wow zoomIn" data-wow-delay="1s" data-wow-duration="3s">
<div class="single-app-table-cell">
<i class="zmdi zmdi-cloud-download"></i>
<h4>Download</h4>
<h3>202K</h3>
</div>
</div>
<div class="single-app wow zoomIn" data-wow-delay=".5s" data-wow-duration="3s">
<div class="single-app-table-cell">
<i class="zmdi zmdi-android"></i>
<h4>Android</h4>
<h3>100K</h3>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!--End of Banner Area-->
<!--Features Area Start EMPRESA-->
<div class="about-area about-four horizontal" id="aboutEMP">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="section-title text-center">
<h2>A <span>EMPRESA </span></h2>
<img alt="" src="img/icon/title-icon-1.png"/>
</div>
</div>
</div>
<div class="row">
<div class="col-md-8 col-md-offset-0 col-sm-8 col-sm-offset-1 col-xs-12">
<div class="about-text">
<div class="service-info-container" style="text-align: justify;">
<p>Somos uma empresa brasileira associada Ã  ABEMD (AssociaÃ§Ã£o Brasileira de Marketing Direto) especializada no fornecimento de informaÃ§Ãµes comerciais, tratamento de dados e serviÃ§os de validaÃ§Ã£o de dados cadastrais que hÃ¡ 10 anos disponibiliza seu conhecimento e tecnologia para empresas de diversas atividades.</p>
<p>Fornecemos informaÃ§Ãµes precisas e soluÃ§Ãµes tecnolÃ³gicas para auxiliar nossos clientes em cada fase do ciclo de negÃ³cio.</p>
</div>
</div>
</div>
<div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
<div class="about-image wow fadeInRight" data-wow-delay="0s" data-wow-duration="3s">
<img alt="Logotipo da ABEMD" class="floatleft" src="img/banner/abemd_2.png" title="ABEMD"/>
</div>
</div>
</div>
</div>
</div>
<!--End of Features Area EMPRESA-->
<!--Domain Name Area Start-->
<div class="domain-name-area horizontal">
<div class="download-bg"></div>
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 col-xs-12">
<div class="section-title text-center">
<div class="about-buttons">
<a href="#lnkcontact"><button class="button-default button-olive" type="button">Solicite um OrÃ§amento!</button></a>
</div>
</div>
</div>
</div>
</div>
</div>
<!--End of Area Start AtualizaÃ§Ã£o-->
<!-- Consulta RF -->
<div class="about-area about-four horizontal" id="aboutRF">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="section-title text-center">
<h2>CONSULTA <span>RECEITA FEDERAL</span></h2>
<img alt="" src="img/icon/title-icon-1.png"/>
<div style="text-align: justify;">
<p>AtravÃ©s das melhores soluÃ§Ãµes de tecnologia, colocamos a sua disposiÃ§Ã£o sistemas capazes de automatizar o processo de pesquisa na Receita Federal, para obter informaÃ§Ãµes disponÃ­veis na Internet e agilizar o processo de cadastro de clientes e fornecedores, como tambÃ©m validar as informaÃ§Ãµes cadastrais para atender a legislaÃ§Ã£o do SPED Fiscal e evitar autuaÃ§Ãµes fiscais.</p>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div style="text-align: justify; margin-top: -30px;">
<b>GovernanÃ§a das informaÃ§Ãµes fiscais</b>
<p>
                            A partir da implantaÃ§Ã£o da Nota Fiscal eletrÃ´nica e do SPED fiscal tornou-se obrigatÃ³ria a validaÃ§Ã£o da situaÃ§Ã£o cadastral de clientes e fornecedores, de acordo com a nova legislaÃ§Ã£o somente serÃ£o permitidas transaÃ§Ãµes comerciais com empresas em situaÃ§Ã£o cadastral ativa e a emissÃ£o da Nota Fiscal deverÃ¡ obrigatoriamente utilizar as informaÃ§Ãµes de cadastro disponibilizadas pela Receita Federal.
                            <br/>As informaÃ§Ãµes poderam ser utilizadas para o cadastramento de novos clientes e fornecedores, emissÃ£o da NF-e, geraÃ§Ã£o dos arquivos EFD e ECD, emissÃ£o do CT-e entre outras aplicaÃ§Ãµes.
                        </p>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-1 col-xs-12">
<div class="about-text" style="text-align: justify;">
<div class="service-info-container floatleft">
<strong>Principais pesquisas:</strong>
<p></p>
<span>SituaÃ§Ã£o Cadastral</span>
<span>Empresas optantes pelo Simples Nacional</span>
<span>CertidÃ£o Negativa de DÃ©bito</span>
<span>RestituiÃ§Ã£o do IRPF</span>
<span>Nota fiscal eletrÃ´nica (NF-e)</span>
<br/>
</div>
</div>
</div>
<div class="col-md-6 col-md-offset-0 col-sm-8 col-sm-offset-3 col-xs-12">
<div class="about-image wow fadeInRight" data-wow-delay="0s" data-wow-duration="3s" style="margin-top: 20px;">
<img alt="Exemplo de Consulta Online na Receita Federal pessoa jurÃ­dica" class="floatleft" src="img/banner/rf_1.png" title="Comprovante de pessoa jurÃ­dica"/>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-1 col-xs-12">
<div class="about-text" style="text-align: justify;">
<div class="service-info-container floatleft">
<strong>BenefÃ­cios de AutomatizaÃ§Ã£o:</strong>
<p></p>
<span>PrevenÃ§Ã£o de fraudes</span>
<span>AperfeiÃ§oar os processos de sua Ã¡rea fiscal e de cadastro</span>
<span>Reduzir custos operacionais</span>
<span>Inibir a emissÃ£o de notas para empresas em situaÃ§Ã£o irregular</span>
<span>Armazenar a imagem das certidÃµes em formato eletrÃ´nico</span>
<span>ReduÃ§Ã£o de riscos junto ao Fisco</span>
<br/>
</div>
</div>
</div>
<div class="col-md-6 col-md-offset-0 col-sm-8 col-sm-offset-3 col-xs-12">
<div class="about-image wow fadeInRight" data-wow-delay="0s" data-wow-duration="3s" style="margin-top: 20px;">
<img alt="Exemplo de Consulta Online na Receita Federal pessoa fÃ­sica" class="floatleft" src="img/banner/rf_2.png" title="Comprovante de pessoa fÃ­sica"/>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="about-text text-center">
<p>
<b>
                                Dispomos tambÃ©m de outros tipos de serviÃ§os para atender as Ã¡reas de cadastro e faturamento de sua empresa.
                                <br/>Entre em contato e solicite a lista completa disponÃ­vel.
                            </b>
</p>
</div>
<div class="about-text" style="text-align: justify; margin-right: 0px">
<p>A consulta poderÃ¡ ser realizada em lote atravÃ©s da troca de arquivos ou Webservice / API para integraÃ§Ã£o no sistema existente:</p>
<strong>Em lote</strong>
<p>
                            Os parÃ¢metros de entrada deverÃ£o ser enviados atravÃ©s de mensagem de email ou serviÃ§o de FTP.
                            <br/>A partir das informaÃ§Ãµes fornecidas, as pesquisas serÃ£o realizadas e os dados de retorno entregues em arquivo eletrÃ´nico formato Excel ou texto (CSV ou TXT).
                        </p>
<strong>WebService / API</strong>
<p>
                            SerÃ¡ fornecido um endereÃ§o na Internet (URL) e uma senha para acesso ao serviÃ§o.
                            <br/>Basicamente deverÃ¡ ser realizada uma chamada em um endereÃ§o web onde vocÃª deverÃ¡ postar os parÃ¢metros de entrada e a senha individual fornecida.
                            <br/>A integraÃ§Ã£o poderÃ¡ ser realizada utilizando os protocolos SOAP ou REST com retorno nos formatos XML, JSON e JSONP.
                            <br/>A imagem da certidÃ£o retornarÃ¡ incorporada ao retorno em formato HTML para fÃ¡cil armazenamento.
                        </p>
</div>
<div class="col-md-12 col-md-offset-0 col-sm-9 col-sm-offset-3 col-xs-12">
<b>Diferenciais:</b>
<div class="service-info-container" style="text-align: justify;">
<p></p>
<span>Menor tempo de retorno do mercado</span>
<span>Data Center de Ãºltima geraÃ§Ã£o</span>
<span>Servidores de alta performance para melhor desempenho do serviÃ§o</span>
<span>Controle de timeout por serviÃ§o e usuÃ¡rio</span>
<span>ManutenÃ§Ã£o 24x7 para garantia de disponibilidade e estabilidade</span>
<span>SLA de 24 horas para manutenÃ§Ã£o de questÃµes tÃ©cnicas</span>
</div>
</div>
</div>
</div>
</div>
</div>
<!--End of Pricing Area-->
<!-- END -->
<!--Domain Name Area Start-->
<div class="domain-name-area horizontal">
<div class="download-bg"></div>
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 col-xs-12">
<div class="section-title text-center">
<div class="about-buttons">
<a href="#lnkcontact"><button class="button-default button-olive" type="button">Solicite um OrÃ§amento!</button></a>
</div>
</div>
</div>
</div>
</div>
</div>
<!--End of Area Start AtualizaÃ§Ã£o-->
<!--Testimonial Area Start-->
<div class="testimonial-area testimonial-four horizontal" id="reviews">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="section-title text-center">
<h2>Alguns <span> Clientes</span></h2>
<img alt="" src="img/icon/title-icon-1.png"/>
<div style="text-align: justify;"></div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12 col-lg-offset-0 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
<div class="row">
<div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
<div class="testimonial-image-slider text-center">
<div class="sin-testiImage">
<img alt="FIAT" src="img/testimonial/fiat.png"/>
</div>
<div class="sin-testiImage">
<img alt="TOYOTA" src="img/testimonial/toyota.png"/>
</div>
<div class="sin-testiImage">
<img alt="DELL" src="img/testimonial/dell.png"/>
</div>
<div class="sin-testiImage">
<img alt="CNA" src="img/testimonial/cna.png"/>
</div>
<div class="sin-testiImage">
<img alt="FIEP" src="img/testimonial/fiep.jpg"/>
</div>
<div class="sin-testiImage">
<img alt="AON" src="img/testimonial/aon.jpg"/>
</div>
<div class="sin-testiImage">
<img alt="HSM" src="img/testimonial/hsm.jpg"/>
</div>
<div class="sin-testiImage">
<img alt="BROOKFIELD" src="img/testimonial/brookfield.jpg"/>
</div>
<div class="sin-testiImage">
<img alt="SUNSET" src="img/testimonial/sunset.jpg"/>
</div>
<div class="sin-testiImage">
<img alt="IBDES" src="img/testimonial/ibdes.png"/>
</div>
<div class="sin-testiImage">
<img alt="SABESP" src="img/testimonial/sabesp.png"/>
</div>
</div>
</div>
</div>
<br/>
</div>
</div>
<div class="row">
<div class="col-md-8 col-md-offset-2 col-xs-12">
<div class="section-title text-center">
<div class="about-buttons">
<a href="#lnkcontact"><button class="button-default button-olive" type="button">Solicite um OrÃ§amento!</button></a>
</div>
</div>
</div>
</div>
</div>
</div>
<!--End of Testimonial Area-->
<div class="pricing-area horizontal" id="lnkcontact"></div>
<!--Footer Area Start-->
<footer class="footer-area footer-solid-color horizontal" id="contact">
<div class="footer-bg"></div>
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="section-title text-center" style="margin-bottom: 30px;">
<h2 class="text-white">DÃVIDAS <span>ORÃAMENTOS</span></h2>
<img alt="" src="img/icon/title-icon-1.png"/>
</div>
</div>
</div>
<div class="row">
<div class="col-md-4 col-md-offset-1">
<h3>FormulÃ¡rio</h3>
<form action="mail.php" id="newsletterForm" method="post">
<div class="form-top">
<div class="row">
<div class="form-group col-md-12">
<input class="form-control" name="name" placeholder="Nome" required="" type="text"/>
</div>
<div class="form-group col-md-12">
<input class="form-control" name="empresa" placeholder="Empresa" required="" type="text"/>
</div>
<div class="form-group col-md-12">
<input class="form-control" name="phone" placeholder="Telefone" required="" type="number"/>
</div>
<div class="form-group col-md-12">
<input class="form-control" name="email" placeholder="Email" required="" type="email"/>
</div>
<div class="form-group col-md-12">
<textarea class="form-control" name="message" placeholder="Mensagem" required="" type="yourmessage"></textarea>
</div>
<div class="submit-form form-group col-sm-12">
<button class="button" id="btn-validate" type="submit"><span>Enviar</span></button>
</div>
</div>
</div>
</form>
<script type="text/javascript">
                        $('#newsletterForm').submit(function (event) {
                            event.preventDefault();
                            grecaptcha.ready(function () {
                                grecaptcha.execute('6LcLtBoaAAAAACRGHTIG7r94CWNmVOw4_IGAD_Ya', { action: 'subscribe_newsletter' }).then(function (token) {
                                    $('#newsletterForm').prepend('<input type="hidden" name="token" value="' + token + '">');
                                    $('#newsletterForm').prepend('<input type="hidden" name="action" value="subscribe_newsletter">');
                                    $('#newsletterForm').unbind('submit').submit();
                                });;
                            });
                        });
                    </script>
</div>
<div class="col-md-4 col-md-offset-2">
<h3>Contate-nos</h3>
<div class="contact-info">
<div class="single-contact-info">
<a class="contact-icon" href="mailto:contato@consultareceitafederal.com.br">
<i class="zmdi zmdi-phone"></i>
</a>
<div class="contact-text">
<span>
                                    +55 (11) 3443-7750<br/>
<a href="mailto:contato@consultareceitafederal.com.br">contato@consultareceitafederal.com.br</a>
</span>
</div>
</div>
<div class="single-contact-info">
<a class="contact-icon" href="https://goo.gl/maps/5yQszAMEz9n4Svp1A" rel="noopener" target="_blank">
<i class="zmdi zmdi-globe-alt"></i>
</a>
<div class="contact-text">
<span>Av das NaÃ§Ãµes Unidas, 12.495<br/> 15Âº Andar - SÃ£o Paulo SP.</span>
</div>
</div>
<div class="single-contact-info">
<a class="contact-icon" href="https://goo.gl/maps/SUq6J6JVLHgJvXdP6" rel="noopener" target="_blank">
<i class="zmdi zmdi-pin"></i>
</a>
<div class="contact-text">
<span>
                                    LocalizaÃ§Ã£o<br/>
<a class="linktext" href="https://goo.gl/maps/SUq6J6JVLHgJvXdP6" rel="noopener" target="_blank">Google Maps</a>
</span>
</div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
</div>
</div>
<div class="row">
<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3">
<div class="footer-text text-center">
<br/>
<img alt="Empresa" src="img/banner/rs2.png"/>
</div>
<div class="footer-text text-center">
<img alt="Empresa filiada Ã  ABEMD" src="img/banner/abemd_4.png"/>
</div>
</div>
</div>
</div>
</footer>
<!--End of Footer Area-->
<link href="css/responsive.css" rel="stylesheet"/>
<style> body, td, th {font-family: latoregular} .msg-error {color: white; font-family: latoregular; font-size: smaller}
        .grecaptcha-badge {
            width: 70px !important;
            overflow: hidden !important;
            transition: all 0.3s ease !important;
            left: -2px !important;
            bottom: 20px !important;
        }
            .grecaptcha-badge:hover {
                width: 256px !important;
            } </style>
<script src="js/all.js"></script> <link href="https://fonts.googleapis.com/css?family=Raleway:400,800,700,600,500,300" rel="stylesheet"/>
</body></html>