<!DOCTYPE html>
<html dir="ltr" lang="en-US" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="https://www.facebook.com/2008/fbml">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<meta content="width=device-width, initial-scale=1,minimal-ui" name="viewport"/>
<meta charset="utf-8"/>
<title>Bollywood News, Pictures and Reviews | Bollywood Mantra</title>
<meta content="Get all the latest news on Bollywood celebrities. We provide Latest Bollywood News, Reviews and Bollywood Pictures on Bollywood Mantra." name="description"/>
<link href="https://www.bollywoodmantra.com/" rel="canonical"/>
<link href="https://www.bollywoodmantra.com/static/front/css/styles.css" rel="stylesheet"/>
<script src="https://www.bollywoodmantra.com/static/front/js/jquery.min.js"></script>
<script src="https://www.bollywoodmantra.com/static/front/js/bootstrap.min.js"></script>
<script src="https://www.bollywoodmantra.com/static/front/js/script.js"></script>
<script type="text/javascript">
                    var googletag = googletag || {};
                    googletag.cmd = googletag.cmd || [];
                    (function() {
                    var gads = document.createElement('script');
                    gads.async = true;
                    gads.type = 'text/javascript';
                    var useSSL = 'https:' == document.location.protocol;
                    gads.src = (useSSL ? 'https:' : 'http:') + 
                    '//www.googletagservices.com/tag/js/gpt.js';
                    var node = document.getElementsByTagName('script')[0];
                    node.parentNode.insertBefore(gads, node);
                    })();
                    </script>
<script type="text/javascript">
                        googletag.cmd.push(function () {
                            
                        var ad_size_leader = googletag.sizeMapping().
                    addSize([320, 250], [320, 50]).
                    addSize([480, 200], [320, 50]).
                    addSize([768, 200], [728, 90]).
                    addSize([1200, 300], [728, 90]).
                    build();var ad_size_rectangle = googletag.sizeMapping().
                    addSize([320, 250], [300, 250]).
                    addSize([480, 300], [300, 250]).
                    addSize([992, 300], [336, 280]).
                    addSize([1000, 300], [336, 280]).
                    build();googletag.defineSlot('/1007582/universal_header', [728, 90], 'div-gpt-ad-1339452528404-0').defineSizeMapping(ad_size_leader).addService(googletag.pubads());googletag.defineSlot('/1007582/homepage_right', [336 ,280], 'div-gpt-ad-1339452941259-1').defineSizeMapping(ad_size_rectangle).addService(googletag.pubads());                            googletag.pubads().enableSingleRequest();
                            googletag.enableServices();
                        });
                    </script>
<script>
                      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
                      ga('create', 'UA-272803-1', 'auto');
                      ga('send', 'pageview');
                    </script>
<script type="text/javascript">
                        var base_url = 'https://www.bollywoodmantra.com/';
                    </script>
<link href="https://www.bollywoodmantra.com/rss/latest-news/" rel="alternate" title="Bollywood Mantra" type="application/rss+xml"/>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
                        (adsbygoogle = window.adsbygoogle || []).push({
                        google_ad_client: "ca-pub-3668318415449114",
                        enable_page_level_ads: true
                        });
                    </script>
</head>
<body>
<div class="navbar hidden-xs">
<div class="navbar-inner">
<div class="container">
<div class="dropdown pull-right" onclick="_gaq.push(['_trackEvent', 'Auth', 'Click', 'Top Nav Dropdown']);">
<a class="dropdown-toggle btn btn-primary dropdown-login-label" data-toggle="dropdown" href="#">
<i class="glyphicon glyphicon-user glyphicon-white"></i>
Login / Signup
<b class="caret"></b>
</a>
<ul aria-labelledby="dLabel" class="dropdown-menu" role="menu">
<li>
<form action="https://www.bollywoodmantra.com/login/" class="dropdown-login" method="post">
<input class="form-control" name="identity" placeholder="Email / Username" type="text" value=""/>
<input class="form-control" name="password" placeholder="Password" type="password" value=""/>
<div>
<div class="checkbox">
<label class="checkbox ">
<input checked="" type="checkbox"/>
<span class="remember">Remember me</span>
<a class="forgot pull-right" href="https://www.bollywoodmantra.com/forgot-password/"> Forgot?</a>
</label>
</div>
</div>
<button class="btn btn-warning btn-login" onclick="_gaq.push(['_trackEvent', 'Auth', 'Click', 'Top Nav Login']);" type="submit">Login</button>
<div class="or">or</div>
<a class="zocial facebook fb-login-button" href="https://www.facebook.com/dialog/oauth?client_id=18446659712&amp;redirect_uri=https%3A%2F%2Fwww.bollywoodmantra.com%2Ffront%2Ffacebook_controller%2Ffb_login%2F&amp;state=0db23c43146bcef05c5797e9b66358af&amp;scope=email%2Cpublish_actions&amp;display=popup" id="fb_signup_button" onclick="_gaq.push(['_trackEvent', 'Auth', 'Click', 'Top Nav Facebook Login']);">
<img src="https://www.bollywoodmantra.com/static/front/img/fb_login_image.png"/>
</a>
<div class="or">
or
</div>
<a class="btn btn-warning register" href="https://www.bollywoodmantra.com/signup/" onclick="_gaq.push(['_trackEvent', 'Auth', 'Click', 'Top Nav Signup']);">Signup here</a>
</form>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="container">
<header>
<a class="logo hidden-xs" href="/" title="Bollywood News, Pictures and Reviews">
<img alt="Bollywood News, Pictures and Reviews | Bollywood Mantra" src="https://www.bollywoodmantra.com/static/front/img/bm-logo.png"/>
</a>
<div class="leaderboard-ad">
<div id="div-gpt-ad-1339452528404-0">
<script type="text/javascript">
                                            googletag.cmd.push(function () {
                                                googletag.display('div-gpt-ad-1339452528404-0');
                                            });

                                        </script>
</div>
</div>
</header>
<div class="main-nav-border">
<a class="logo visible-xs pull-left" href="https://www.bollywoodmantra.com/" title="Bollywood News, Pictures and Reviews">
<img alt="Bollywood News, Pictures and Reviews | Bollywood Mantra" src="https://www.bollywoodmantra.com/static/front/img/bm-logo-small.png"/>
</a>
<div class="navbar-header">
<button class="btn-lg navbar-toggle glyphicon glyphicon-align-justify" data-target=".navbar-collapse" data-toggle="collapse" type="button">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="navbar-collapse collapse">
<ul class="nav navbar-nav">
<li class="active"><a class="selected" href="https://www.bollywoodmantra.com/"><i class="glyphicon glyphicon-home"></i>Home</a></li>
<li><a href="https://www.bollywoodmantra.com/photo-gallery/" title="Photos">Photo Gallery</a></li>
<li><a href="https://www.bollywoodmantra.com/bollywood-news/" title="News">News</a></li>
<li><a href="https://www.bollywoodmantra.com/category/events/" title="Bollywood Events">Bollywood Events</a></li>
<li><a href="https://www.bollywoodmantra.com/category/actresses/" title="Bollywood Actresses">Bollywood Actresses</a></li>
</ul>
</div>
</div>
<div class="content">
<div class="row">
<div class="col-md-8 peopleCarouselImg">
<div class="carousel slide" data-ride="carousel" id="carousel-example-generic">
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#carousel-example-generic"></li>
<li class="" data-slide-to="1" data-target="#carousel-example-generic"></li>
<li class="" data-slide-to="2" data-target="#carousel-example-generic"></li>
<li class="" data-slide-to="3" data-target="#carousel-example-generic"></li>
<li class="" data-slide-to="4" data-target="#carousel-example-generic"></li>
<li class="" data-slide-to="5" data-target="#carousel-example-generic"></li>
</ol>
<div class="carousel-inner" role="listbox">
<div class="item active">
<a href="https://www.bollywoodmantra.com/news/neha-kakkar-rohanpreet-singh-excited-to-celebrate-first-lohri/34295/">
<img alt="..." class="slider-img" src="https://www.bollywoodmantra.com/thumbnails/unsafe/736x300/smart/i.bollywoodmantra.com/news/2021/01/13/maxresdefault.jpg"/>
<div class="carousel-caption">
<h1>Neha Kakkar excited to celebrate first Lohri with hubby Rohanpreet</h1>
</div> </a>
</div>
<div class="item ">
<a href="https://www.bollywoodmantra.com/news/saif-kareena-all-set-to-move-into-their-new-home/34293/">
<img alt="..." class="slider-img" src="https://www.bollywoodmantra.com/thumbnails/unsafe/736x300/smart/i.bollywoodmantra.com/albums/candids/actors/saif-ali-khan/kareena-kapoor_saif-ali-khan__741948.jpg"/>
<div class="carousel-caption">
<h1>Saif, Kareena all set to move into their new home</h1>
</div> </a>
</div>
<div class="item ">
<a href="https://www.bollywoodmantra.com/news/sagarika-ghagtes-father-passes-away-at-64/34292/">
<img alt="..." class="slider-img" src="https://www.bollywoodmantra.com/thumbnails/unsafe/736x300/smart/i.bollywoodmantra.com/news/2021/01/13/download.jfif"/>
<div class="carousel-caption">
<h1>Sagarika Ghatgeâs father passes away at 64</h1>
</div> </a>
</div>
<div class="item ">
<a href="https://www.bollywoodmantra.com/news/happy-lohri-2021-kangana-taapsee-kjo-and-others-send-warm-wishes/34291/">
<img alt="..." class="slider-img" src="https://www.bollywoodmantra.com/thumbnails/unsafe/736x300/smart/i.bollywoodmantra.com/news/2021/01/13/lohri-1610514275.jpg"/>
<div class="carousel-caption">
<h1>Happy Lohri 2021: Kangana, Taapsee, Kjo and others send warm wishes</h1>
</div> </a>
</div>
<div class="item ">
<a href="https://www.bollywoodmantra.com/news/kapil-sharma-celebrates-mothers-birthday/34294/">
<img alt="..." class="slider-img" src="https://www.bollywoodmantra.com/thumbnails/unsafe/736x300/smart/i.bollywoodmantra.com/news/2021/01/13/kapil-1610548824.jpg"/>
<div class="carousel-caption">
<h1>Kapil Sharma celebrates motherâs birthday</h1>
</div> </a>
</div>
<div class="item ">
<a href="https://www.bollywoodmantra.com/news/guru-clocks-14-years-aishwarya-shares-throwback-pic-with-abhishek/34290/">
<img alt="..." class="slider-img" src="https://www.bollywoodmantra.com/thumbnails/unsafe/736x300/smart/i.bollywoodmantra.com/news/2021/01/13/jpg.jfif"/>
<div class="carousel-caption">
<h1>âGuruâ clocks 14 years: Aishwarya shares throwback pic with Abhishek</h1>
</div> </a>
</div>
</div>
<a class="left carousel-control" data-slide="prev" href="#carousel-example-generic" role="button"> <span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span> <span class="sr-only">Previous</span> </a>
<a class="right carousel-control" data-slide="next" href="#carousel-example-generic" role="button"> <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span> <span class="sr-only">Next</span> </a>
</div>
</div>
<div class="col-md-4">
<div class="large-mrec-ad">
<div id="div-gpt-ad-1339452941259-1">
<script type="text/javascript">
						googletag.cmd.push(function() {
							googletag.display('div-gpt-ad-1339452941259-1');
						});
					</script>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-8">
<div class="greybox">
<h3 class="greybox-header orange-strip">Latest Bollywood News</h3>
<div class="innerbox">
<div class="col-md-6 padding-none">
<ul class="list-group bullet-list latest-news">
<li><a href="/news/neha-kakkar-rohanpreet-singh-excited-to-celebrate-first-lohri/34295/">Neha Kakkar excited to celebrate first Lohri with hubby Rohanpreet</a></li> <li><a href="/news/kapil-sharma-celebrates-mothers-birthday/34294/">Kapil Sharma celebrates motherâs birthday</a></li> <li><a href="/news/saif-kareena-all-set-to-move-into-their-new-home/34293/">Saif, Kareena all set to move into their new home</a></li> <li><a href="/news/sagarika-ghagtes-father-passes-away-at-64/34292/">Sagarika Ghatgeâs father passes away at 64</a></li> <li><a href="/news/happy-lohri-2021-kangana-taapsee-kjo-and-others-send-warm-wishes/34291/">Happy Lohri 2021: Kangana, Taapsee, Kjo and others send warm wishes</a></li> <li><a href="/news/guru-clocks-14-years-aishwarya-shares-throwback-pic-with-abhishek/34290/">âGuruâ clocks 14 years: Aishwarya shares throwback pic with Abhishek</a></li> <li><a href="/news/no-photos-of-baby-please-anushka-sharma-virat-kohli-to-paparazzi/34289/">No photos of baby please, Anushka Sharma-Virat Kohli to paparazzi</a></li> </ul>
</div>
<div class="col-md-6 padding-none">
<ul class="list-group bullet-list latest-news">
<li><a href="/news/pregnant-kareenas-chills-with-karisma-malaika-amrita-mallika/34288/">Pregnant Kareenaâs chills with Karisma, Malaika, Amrita, Mallika</a></li> <li><a href="/news/boney-kapoor-to-play-ranbir-kapoors-father-in-luv-ranjans-next/34287/">Boney Kapoor to play Ranbir Kapoorâs father in Luv Ranjanâs next</a></li> <li><a href="/news/vivek-oberois-brother-in-law-aditya-alva-arrested-in-drug-case/34286/">Vivek Oberoiâs brother-in-law Aditya Alva arrested in drug case</a></li> <li><a href="/news/anushka-virat-welcome-baby-girl-no-visitors-flowers-gifts-allowed-at-hospital/34285/">Anushka-Virat welcome baby girl: No visitors, flowers, gifts allowed at hospital</a></li> <li><a href="/news/first-pic-of-virat-kohli-anushka-sharmas-baby-goes-viral/34284/">First pic of Virat Kohli-Anushka Sharma's baby goes viral</a></li> <li><a href="/news/good-luck-jerry-first-look-janhvi-kapoor-plays-a-next-door-girl/34283/">âGood Luck Jerryâ First Look: Janhvi Kapoor plays a next-door-girl</a></li> <li><a href="/news/easy-peasy-breezy-sunday-malaika-arora-shares-sun-kissed-pic/34282/">'Easy, Peasy, Breezy Sunday', Malaika Arora shares sun-kissed pic</a></li> </ul>
</div>
<a class="orange-link pull-right" href="/latest-bollywood-news/">More Bollywood news</a>
</div>
</div>
</div>
<div class="col-md-4">
<div class="greybox">
<h3 class="greybox-header orange-strip">Feature Articles</h3>
<div class="innerbox">
<ul class="bullet-list margin-none flashback">
<li><a href="/news/anushka-virat-kohli-welcome-a-baby-girl-wishes-pour-in/34281/">Anushka-Virat Kohli welcome a baby girl, wishes pour in</a></li><li><a href="/news/in-photos-celebs-step-out-of-their-house-as-lockdown-curb-ease/33198/">In Photos: Celebs step outside as lockdown curb ease</a></li><li><a href="/news/unseen-pictures-from-rana-daggubati-miheeka-bajajs-roka-ceremony/33131/">Unseen pictures from Rana Daggubati-Miheeka Bajaj's roka ceremony</a></li><li><a href="/news/35-unseen-pictures-from-amitabh-bachchans-lavish-diwali-bash-at-jalsa/32126/">35 unseen pictures from Amitabh Bachchanâs lavish Diwali bash at Jalsa</a></li><li><a href="/news/karwa-chauth-2019-15-stunning-pictures-of-bollywood-stars/32054/">Karwa Chauth 2019: 15 stunning pictures of Bollywood stars</a></li><li><a href="/news/a-sneak-peak-into-bigg-boss-13-filmcity-in-mumbai/31952/">A Sneak Peak Into âBigg Boss 13â house in Mumbai Filmcity</a></li><li><a href="/news/vogue-beauty-awards-2019-complete-winners-list-alia-shahid-sara-win-big/31939/">Vogue Beauty Awards 2019 complete winners list: Alia, Shahid, Sara win big</a></li> </ul>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-3">
<div class="categorybox">
<a href="/category/first-look/" title="First Look"><h2 class="categorybox-header first-look">First Look<i class="glyphicon glyphicon-star glyphicon-white"></i></h2></a>
<figure>
<a href="/album/satyameva-jayate-2-1/">
<img src="https://www.bollywoodmantra.com/thumbnails/unsafe/230x230/smart/i.bollywoodmantra.com/albums/first-look/satyameva-jayate-2-1/john-abraham__1086357.jpg"/>
</a>
</figure>
<ul class="bullet-list bullet-purple categories">
<li><a href="/album/satyameva-jayate-2-1/" title="Satyameva Jayate 2">Satyameva Jayate 2</a></li><li><a href="/album/harami/" title="Harami">Harami</a></li><li><a href="/album/rashmi-rocket/" title="Rashmi Rocket">Rashmi Rocket</a></li><li><a href="/album/the-big-bull/" title="The Big Bull">The Big Bull</a></li><li><a href="/album/sadak-2/" title="Sadak 2">Sadak 2</a></li> </ul>
</div>
</div>
<div class="col-md-3">
<div class="categorybox">
<a href="/category/wallpapers/" title="Wallpapers"><h2 class="categorybox-header wallpapers">Wallpapers<i class="glyphicon glyphicon-picture glyphicon-white"></i></h2></a>
<figure>
<a href="/album/daboo-ratnani-photoshoot/">
<img src="https://www.bollywoodmantra.com/thumbnails/unsafe/230x230/smart/i.bollywoodmantra.com/albums/wallpapers/daboo-ratnani-photoshoot/kareena-kapoor-khan__1086068.jpg"/>
</a>
</figure>
<ul class="bullet-list bullet-pink categories">
<li><a href="/album/daboo-ratnani-photoshoot/" title="Daboo Ratnani PhotoShoot">Daboo Ratnani PhotoShoot</a></li><li><a href="/album/nayanthara-1/" title="Nayanthara">Nayanthara</a></li><li><a href="/album/kareena-kapoor-19/" title="Kareena Kapoor">Kareena Kapoor</a></li><li><a href="/album/salman-khan-1/" title="Salman Khan">Salman Khan</a></li><li><a href="/album/malaika-arora-khan-16/" title="Malaika Arora Khan">Malaika Arora Khan</a></li> </ul>
</div>
</div>
<div class="col-md-3">
<div class="categorybox">
<a href="/category/events/" title="Events"><h2 class="categorybox-header events">Events<i class="glyphicon glyphicon-calendar glyphicon-white"></i></h2></a>
<figure>
<a href="/album/magazine/">
<img src="https://www.bollywoodmantra.com/thumbnails/unsafe/230x230/smart/i.bollywoodmantra.com/albums/events/miscellaneous/magazine/tara-sutaria__1086358.jpg"/>
</a>
</figure>
<ul class="bullet-list bullet-blue categories">
<li><a href="/album/magazine/" title="Magazine">Magazine</a></li><li><a href="/album/celebrity-shared-social-media-images/" title="Celebrity Shared Social Media Images">Celebrity Shared Social Media Images</a></li><li><a href="/album/raksha-bandhan-2020/" title="Raksha Bandhan 2020">Raksha Bandhan 2020</a></li><li><a href="/album/celebrity-spotted-2020/" title="Celebrity Spotted 2020">Celebrity Spotted 2020</a></li><li><a href="/album/saroj-khans-funeral-held-in-malad/" title="Saroj Khan's Funeral held in Malad">Saroj Khan's Funeral held in Malad</a></li> </ul>
</div>
</div>
<div class="col-md-3">
<div class="categorybox">
<a href="/category/candids/" title="Candids"><h2 class="categorybox-header candids">Candids<i class="glyphicon glyphicon-camera glyphicon-white"></i></h2></a>
<figure>
<a href="/album/bhumi-pednekar/">
<img src="https://www.bollywoodmantra.com/thumbnails/unsafe/230x230/smart/i.bollywoodmantra.com/albums/candids/actresses/bhumi-pednekar/bhumi-pednekar__1086344.jpg"/>
</a>
</figure>
<ul class="bullet-list bullet-green categories">
<li><a href="/album/bhumi-pednekar/" title="Bhumi Pednekar">Bhumi Pednekar</a></li><li><a href="/album/aamir-khan-1/" title="Aamir Khan">Aamir Khan</a></li><li><a href="/album/vaani-kapoor/" title="Vaani Kapoor">Vaani Kapoor</a></li><li><a href="/album/tara-sutaria-1/" title="Tara Sutaria">Tara Sutaria</a></li><li><a href="/album/esha-gupta-1/" title="Esha Gupta">Esha Gupta</a></li> </ul>
</div>
</div>
</div>
<div class="row">
<div class="col-md-3">
<div class="categorybox">
<a href="/category/movie-stills/" title="Movie Stills"><h2 class="categorybox-header movie-stills">Movie Stills<i class="glyphicon glyphicon-film glyphicon-white"></i></h2></a>
<figure>
<a href="/album/khaali-peeli/">
<img src="https://www.bollywoodmantra.com/thumbnails/unsafe/230x230/smart/i.bollywoodmantra.com/albums/movie-stills/khaali-peeli/ishaan-khattar_ananya-pandey__1086356.jpg"/>
</a>
</figure>
<ul class="bullet-list bullet-orange categories">
<li><a href="/album/khaali-peeli/" title="Khaali Peeli">Khaali Peeli</a></li><li><a href="/album/dil-bechara-1/" title="Dil Bechara">Dil Bechara</a></li><li><a href="/album/gunjan-saxena-the-kargil-girl-1/" title="Gunjan Saxena - The Kargil Girl">Gunjan Saxena - The Kargil Girl</a></li><li><a href="/album/class-of-83/" title="Class of 83">Class of 83</a></li><li><a href="/album/bell-bottom/" title="Bell Bottom">Bell Bottom</a></li> </ul>
</div>
</div>
<div class="col-md-3">
<div class="categorybox">
<h2 class="categorybox-header movie-previews">Movie Previews <i class="glyphicon glyphicon-facetime-video glyphicon-white"></i></h2>
<figure>
<a href="/movie-preview/chhapaak/">
<img src="https://www.bollywoodmantra.com/thumbnails/unsafe/230x230/smart/i.bollywoodmantra.com/celebrities/180/chhapaak.jpg"/>
</a>
</figure>
<ul class="bullet-list bullet-yellow categories">
<li><a href="/movie-preview/chhapaak/" title="Chhapaak">Chhapaak</a></li><li><a href="/movie-preview/good-newwz/" title="Good Newwz">Good Newwz</a></li><li><a href="/movie-preview/pati-patni-aur-woh/" title="Pati Patni Aur Woh">Pati Patni Aur Woh</a></li><li><a href="/movie-preview/panipat/" title="Panipat">Panipat</a></li><li><a href="/movie-preview/mission-mangal/" title="Mission Mangal">Mission Mangal</a></li> </ul>
</div>
</div>
<div class="col-md-3">
<div class="categorybox">
<h2 class="categorybox-header movie-reviews">Movie Reviews <i class="glyphicon glyphicon-file glyphicon-white"></i></h2>
<figure>
<a href="/movie-review/gunjan-saxena-the-kargil-girl/">
<img src="http://placehold.it/230/ffffff/566488&amp;text=BM"/>
</a>
</figure>
<ul class="bullet-list bullet-bluedark categories">
<li><a href="/movie-review/gunjan-saxena-the-kargil-girl/" title="Gunjan Saxena - The Kargil Girl">Gunjan Saxena - The Kargil Girl</a></li><li><a href="/movie-review/chhapaak-1/" title="Chhapaak">Chhapaak</a></li><li><a href="/movie-review/gully-boys/" title="Gully Boys">Gully Boys</a></li><li><a href="/movie-review/manikarnika-the-queen-of-jhansi-1/" title="Manikarnika-The Queen of Jhansi">Manikarnika-The Queen of Jhansi</a></li><li><a href="/movie-review/simmba-1/" title="Simmba">Simmba</a></li> </ul>
</div>
</div>
<div class="col-md-3">
<div class="categorybox">
<h2 class="categorybox-header music-reviews">Music Reviews <i class="glyphicon glyphicon-music glyphicon-white"></i></h2>
<figure>
<a href="/music-review/panipat-1/">
<img src="https://www.bollywoodmantra.com/thumbnails/unsafe/230x230/smart/i.bollywoodmantra.com/celebrities/180/panipat.jpg"/>
</a>
</figure>
<ul class="bullet-list bullet-red categories">
<li><a href="/music-review/panipat-1/" title="Panipat">Panipat</a></li><li><a href="/music-review/race-3-1/" title="Race 3">Race 3</a></li><li><a href="/music-review/veere-di-wedding/" title="Veere Di Wedding">Veere Di Wedding</a></li><li><a href="/music-review/pad-man/" title="Pad Man">Pad Man</a></li><li><a href="/music-review/golmaal-again/" title="Golmaal Again">Golmaal Again</a></li> </ul>
</div>
</div>
</div>
<script type="text/javascript">
  <!-- fancybox add code from here,when user not logged in display fancybox for login-->
	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="https://www.bollywoodmantra.com/static/front/fancybox/source/jquery.fancybox.js?v=2.1.4"></script>
<link href="https://www.bollywoodmantra.com/static/front/fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen" rel="stylesheet" type="text/css"/>
<script src="https://www.bollywoodmantra.com/static/front/js/jquery.cookie.js" type="text/javascript"></script>
<style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}
	</style>
<div id="inline1" style="width:800px;display: none;">
<div class="content">
<div class="row">
<div class="col-md-12">
<div class="greybox">
<div class="innerbox formbox signup">
<div class="row">
<div class="col-md-12">
<h2>Join Bollywood Mantra</h2>
<p>Get Latest Bollywood News and Pictures in Your Inbox</p>
<a class="fb-login-button btn-fb" href="https://www.facebook.com/dialog/oauth?client_id=18446659712&amp;redirect_uri=https%3A%2F%2Fwww.bollywoodmantra.com%2Ffront%2Ffacebook_controller%2Ffb_login%2F&amp;state=0db23c43146bcef05c5797e9b66358af&amp;scope=email%2Cpublish_actions&amp;display=popup" id="fb_signup_button_pop">Connect with Facebok</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<a class="fancybox" href="#inline1" title=""></a>
<script type="text/javascript">
	$(document).ready(function() {
	 		//openfancyboxdialog();
		fb_connect();
	});</script> </div>
</div>
<div class="navbar">
<div class="navbar-inner" style="background-color: #0a0609;">
<div class="container">
<div class="row">
<div class="col-md-4 categorybox2 ">
<h2 class="categorybox-header first-look">Bollywood Directory</h2>
<div class="row">
<div class="col-md-6 pull-left">
<h2 class="categorybox-header">Celebrities</h2>
<nav class="footer_dir">
<span><a href="https://www.bollywoodmantra.com/celebrity/a/directory/">A</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/b/directory/">B</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/c/directory/">C</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/d/directory/">D</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/e/directory/">E</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/f/directory/">F</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/g/directory/">G</a></span></nav><nav class="footer_dir"><span><a href="https://www.bollywoodmantra.com/celebrity/h/directory/">H</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/i/directory/">I</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/j/directory/">J</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/k/directory/">K</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/l/directory/">L</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/m/directory/">M</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/n/directory/">N</a></span></nav><nav class="footer_dir"><span><a href="https://www.bollywoodmantra.com/celebrity/o/directory/">O</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/p/directory/">P</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/q/directory/">Q</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/r/directory/">R</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/s/directory/">S</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/t/directory/">T</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/u/directory/">U</a></span></nav><nav class="footer_dir"><span><a href="https://www.bollywoodmantra.com/celebrity/v/directory/">V</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/w/directory/">W</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/x/directory/">X</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/y/directory/">Y</a></span><span><a href="https://www.bollywoodmantra.com/celebrity/z/directory/">Z</a></span> </nav></div>
<div class="col-md-6 pull-right">
<h2 class="categorybox-header">Movies</h2>
<nav class="footer_dir">
<span><a href="https://www.bollywoodmantra.com/movie/a/directory/">A</a></span><span><a href="https://www.bollywoodmantra.com/movie/b/directory/">B</a></span><span><a href="https://www.bollywoodmantra.com/movie/c/directory/">C</a></span><span><a href="https://www.bollywoodmantra.com/movie/d/directory/">D</a></span><span><a href="https://www.bollywoodmantra.com/movie/e/directory/">E</a></span><span><a href="https://www.bollywoodmantra.com/movie/f/directory/">F</a></span><span><a href="https://www.bollywoodmantra.com/movie/g/directory/">G</a></span></nav><nav class="footer_dir"><span><a href="https://www.bollywoodmantra.com/movie/h/directory/">H</a></span><span><a href="https://www.bollywoodmantra.com/movie/i/directory/">I</a></span><span><a href="https://www.bollywoodmantra.com/movie/j/directory/">J</a></span><span><a href="https://www.bollywoodmantra.com/movie/k/directory/">K</a></span><span><a href="https://www.bollywoodmantra.com/movie/l/directory/">L</a></span><span><a href="https://www.bollywoodmantra.com/movie/m/directory/">M</a></span><span><a href="https://www.bollywoodmantra.com/movie/n/directory/">N</a></span></nav><nav class="footer_dir"><span><a href="https://www.bollywoodmantra.com/movie/o/directory/">O</a></span><span><a href="https://www.bollywoodmantra.com/movie/p/directory/">P</a></span><span><a href="https://www.bollywoodmantra.com/movie/q/directory/">Q</a></span><span><a href="https://www.bollywoodmantra.com/movie/r/directory/">R</a></span><span><a href="https://www.bollywoodmantra.com/movie/s/directory/">S</a></span><span><a href="https://www.bollywoodmantra.com/movie/t/directory/">T</a></span><span><a href="https://www.bollywoodmantra.com/movie/u/directory/">U</a></span></nav><nav class="footer_dir"><span><a href="https://www.bollywoodmantra.com/movie/v/directory/">V</a></span><span><a href="https://www.bollywoodmantra.com/movie/w/directory/">W</a></span><span><a href="https://www.bollywoodmantra.com/movie/x/directory/">X</a></span><span><a href="https://www.bollywoodmantra.com/movie/y/directory/">Y</a></span><span><a href="https://www.bollywoodmantra.com/movie/z/directory/">Z</a></span> <span><a href="https://www.bollywoodmantra.com/movie/num/directory/">#</a></span>
</nav></div>
</div>
</div>
<div class="col-md-4 categorybox2">
<h2 class="categorybox-header candids">Trending Articles</h2>
<ul class="bullet-list bullet-green latest-news">
<li><a class="footer_color" href="https://www.bollywoodmantra.com/news/sagarika-ghagtes-father-passes-away-at-64/34292/" title="Sagarika Ghatgeâs father passes away at 64">Sagarika Ghatgeâs father passes away at 64</a></li>
<li><a class="footer_color" href="https://www.bollywoodmantra.com/news/saif-kareena-all-set-to-move-into-their-new-home/34293/" title="Saif, Kareena all set to move into their new home">Saif, Kareena all set to move into their new home</a></li>
<li><a class="footer_color" href="https://www.bollywoodmantra.com/news/no-photos-of-baby-please-anushka-sharma-virat-kohli-to-paparazzi/34289/" title="No photos of baby please, Anushka Sharma-Virat Kohli to paparazzi">No photos of baby please, Anushka Sharma-Virat Kohli to paparazzi</a></li>
<li><a class="footer_color" href="https://www.bollywoodmantra.com/news/guru-clocks-14-years-aishwarya-shares-throwback-pic-with-abhishek/34290/" title="âGuruâ clocks 14 years: Aishwarya shares throwback pic with Abhishek">âGuruâ clocks 14 years: Aishwarya shares throwback pic with Abhishek</a></li>
<li><a class="footer_color" href="https://www.bollywoodmantra.com/news/happy-lohri-2021-kangana-taapsee-kjo-and-others-send-warm-wishes/34291/" title="Happy Lohri 2021: Kangana, Taapsee, Kjo and others send warm wishes">Happy Lohri 2021: Kangana, Taapsee, Kjo and others send warm wishes</a></li>
</ul>
</div>
<div class="col-md-2 categorybox2">
<h2 class="categorybox-header music-reviews">Social Media</h2>
<ul class="bullet-list bullet-red">
<li><a class="footer_color" href="http://www.facebook.com/bollywoodmantra" target="_blank">Facebook</a></li>
<li class="footer_color"><a class="footer_color" href="https://twitter.com/bollywoodmantra" target="_blank">Twitter</a></li>
<li class="footer_color"><a class="footer_color" href="http://www.youtube.com/user/bmantra" target="_blank">Youtube</a></li>
<li class="footer_color"><a class="footer_color" href="http://www.pinterest.com/bollywoodmantra/" target="_blank">Pinterest</a></li>
<li class="footer_color"><a class="footer_color" href=" https://plus.google.com/+Bollywoodmantra/posts" target="_blank">Google Plus</a></li>
</ul>
</div>
</div>
<footer>
<ul>
<li><a href="/">Home</a></li>
<li><a href="https://www.bollywoodmantra.com/photo-gallery/">Photos</a></li>
<li><a href="https://www.bollywoodmantra.com/bollywood-news/">News</a></li>
<li><a href="https://www.bollywoodmantra.com/category/events/">Bollywood Events</a></li>
<li><a href="https://www.bollywoodmantra.com/category/actresses/">Bollywood Actresses</a></li>
</ul>
<div class="row">
<div class="col-md-6">
<ul style="float: left;">
<li><a href="#">Disclaimer</a></li>
<li><a href="https://www.bollywoodmantra.com/privacy/">Privacy</a></li>
<li><a href="https://www.bollywoodmantra.com/advertise/">Advertise</a></li>
<li><a href="#">Terms of Service</a></li>
<li><a href="/cdn-cgi/l/email-protection#84ece1e8e8ebc4e6ebe8e8fdf3ebebe0e9e5eaf0f6e5aae7ebe9">Contact</a></li>
</ul>
</div>
<div class="col-md-6">
<ul style="float: right;color:#aaa;">
<li>All Rights Reserved. © 2003 - 2021 Bollywood Mantra</li>
</ul>
</div>
</div>
</footer>
</div>
</div>
</div>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/javascript">
                                //celebrity_auto_complete();
                                $(document).ready(function () {
                                    $("#fb_signup_button").click(function (e) {
                                        e.preventDefault();
                                        fbpop(this.href);
                                    });
                                    var fbpop = function (url) {
                                        var popup = window.open(url, "facebook_popup", "width=620,height=400,status=no,scrollbars=no,resizable=no");
                                        popup.focus();
                                    };
                                });
                            </script>
<div id="fb-root"></div>
<script type="text/javascript">
                                var app_id = '18446659712';
                                                                    var user_logged_in = false;
                                                                fb_login(app_id, user_logged_in);
                            </script>
</body>
</html>
