<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Status code 404</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="/customerrorpages/assets/scss/main.min.css" rel="stylesheet"/>
</head>
<body>
<div class="wrapper">
<a class="logo" href="/"><img alt="" src="/customerrorpages/assets/images/logo.png"/></a>
<div class="content">
<div class="text">
<h1>Status code: 404 - Not Found</h1>
<p>The page you are looking for was not found.</p>
</div>
</div>
</div>
</body>
</html>
