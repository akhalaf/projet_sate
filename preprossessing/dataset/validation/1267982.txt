<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="http://www.santiagoimoveispadua.com.br/santiagoimoveis" property="og:url"/>
<meta content="website" property="og:type"/>
<meta content="https://corretagem.immobileweb.com.br/images/imagens-banners/https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/imagem637080390859044509.jpg" property="og:image"/>
<meta content="Home | SANTIAGO I. PÁDUA EIRELI ME" property="og:title"/>
<meta content="Compre, venda ou alugue seu imóvel na SANTIAGO I. PÁDUA EIRELI ME. As melhores ofertas você encontra aqui." property="og:description"/>
<meta content="Compre, venda ou alugue seu imóvel na SANTIAGO I. PÁDUA EIRELI ME. As melhores ofertas você encontra aqui." name="description"/>
<meta content="SANTIAGO I. PÁDUA EIRELI ME,santiagoimoveis" name="keywords"/>
<meta content="#c0392b" name="theme-color"/>
<meta content="#c0392b" name="msapplication-navbutton-color"/>
<meta content="#c0392b" name="apple-mobile-web-app-status-bar-style"/>
<title>Home | SANTIAGO I. PÁDUA EIRELI ME</title>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"/>
<link href="/css/min/site.min.css?v7&amp;v=j1EiFTplvuRQK5yvhek8L3MkqZTElhsBhgPHwn3gZbQ" rel="stylesheet"/>
<link href="/css/templates/topo/topo_001.css?v7" rel="stylesheet"/>
<link href="/css/templates/rodape/rodape_003.css?v7" rel="stylesheet"/>
<link href="/css/templates/home/banner/banner_002.css?v7" rel="stylesheet"/>
<link href="/css/templates/home/galeria/galeria_002.css?v7" rel="stylesheet"/>
<link href="/css/color/alt.color.pomegranate.css?v7" rel="stylesheet"/>
<link href="/css/templates/home/home.css?v2" media="screen" rel="stylesheet" type="text/css"/>
<link href="/css/aplica-marca-dagua.css" rel="stylesheet"/>
</head>
<body class="alt-tipo-transacao-todas">
<div class="alt-carregando">
<i class="fa fa-refresh fa-spin"></i>
</div>
<div class="alt-fade-live-chat-container"></div>
<nav class="alt-live-chat-container">
<a class="buttons" href="https://api.whatsapp.com/send?phone=5522981560036" target="_blank" tooltip="Whatsapp">
<i class="fa fa-whatsapp" style="margin-left: 11px;"></i>
</a>
<a class="buttons" href="#" onclick="return false;">
<span class="buttons" tooltip="Precisa de ajuda? Clique aqui e fale conosco.">
<span class="rotate">
<i class="fa fa-whatsapp"></i> </span>
</span>
</a>
</nav>
<div class="alt-js-identificador-topo">
<div>
<div class="container header" style="min-height:90px;">
<div class="alt-logo-row row-fluid">
<div class="col-lg-10 col-md-9 col-xs-9">
<a href="/santiagoimoveis">
<img alt="SANTIAGO I. PÁDUA EIRELI ME" src="https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg"/>
</a>
</div>
<div class="col-lg-2 col-md-3 col-xs-3">
<a class="btn btn-default alt-texto-responsivo" href="https://painel.immobileweb.com.br/santiagoimoveis" target="_blank" title="Faça o login para acessar a área restrita">
<i aria-hidden="true" class="fa fa-2x fa-lock alt-conteudo-btn-login"></i>
<span class="alt-texton-button-login ">ÁREA DO CLIENTE</span>
</a>
</div>
</div>
</div>
<nav class="navbar navbar-default navbar-topo-001 alt-fixar-menu" role="navigation">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".side-collapse" data-target-2=".side-collapse-container" data-toggle="collapse-side" id="btnColapseMenuTopo_001" type="button">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="/santiagoimoveis">
<img alt="SANTIAGO I. PÁDUA EIRELI ME" src="https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg"/>
</a>
</div>
<div class="navbar-default navbar-topo-001 side-collapse in" id="navbar-collapse-1">
<nav class="navbar-collapse" role="navigation">
<ul class="nav navbar-nav js-alt-nav ">
<li><a href="/santiagoimoveis" title="Home Page">HOME</a></li>
<li class="alt-tipo-transacao-item-aluguel"><a href="/santiagoimoveis/aluguel" title="Imóveis para aluguel">ALUGAR</a></li>
<li class="alt-tipo-transacao-item-venda"><a href="/santiagoimoveis/venda" title="Imóveis para venda">COMPRAR</a></li>
<li><a href="/santiagoimoveis/anunciar" title="Anuncie o seu imóvel">ANUNCIAR IMÓVEL</a></li>
<li><a href="/santiagoimoveis/sobre" title="Quem somos">SOBRE</a></li>
<li><a href="/santiagoimoveis/contato" title="Contato">CONTATO</a></li>
<li class="alt-js-link-favoritos-lista"><a href="#" title="Favoritos">FAVORITOS <i class="fa fa-heart"></i></a></li>
<li class="alt-menu-adm">
<a href="https://painel.immobileweb.com.br/santiagoimoveis" target="_blank">                            
                            ÁREA DO CLIENTE
                        </a>
</li>
</ul>
</nav>
</div>
</nav>
</div>
</div>
<div class="alt-banner-home-page">
<div class="carousel slide" data-ride="carousel" id="carousel-example-generic">
<div class="carousel-inner">
<div class="alt-banner-posicao item active" style="background-image: url(https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/imagem637080390859044509.jpg)">
<div class="carousel-caption">
<h3>Aqui você</h3>
<p>Encontra a casa dos seus sonhos.</p>
</div>
</div>
</div>
</div>
<div class="container alt-js-fixar-pesquisa-banner">
<div class="row alt-busca-linha">
<div class="carousel slide alt-tabs-search" data-ride="carousel" id="myCarouselSearchHome">
<!-- Wrapper for slides -->
<div class="carousel-inner">
<div class="item active">
<form>
<div class="col-md-3 col-xs-6 alt-busca-coluna alt-tipo-transacao-item-select">
<select class="form-control " id="transacao" name="transacao">
<option class="transacao" value="">O que deseja fazer?</option>
<option class="transacao" value="venda">Comprar</option>
<option class="transacao" value="aluguel">Alugar</option>
</select>
</div>
<div class="col-md-3 col-xs-6 alt-busca-coluna">
<select class="form-control js-example-basic-single" id="tipo" name="tipo">
<option value="">Selecione o tipo</option>
<option class="tiposimoveis" value="apartamento-">Apartamento </option>
<option class="tiposimoveis" value="casa">Casa</option>
<option class="tiposimoveis" value="loja">Loja</option>
<option class="tiposimoveis" value="quitinete">Quitinete</option>
<option class="tiposimoveis" value="sala-comercial">Sala Comercial</option>
<option class="tiposimoveis" value="sobrado">Sobrado</option>
<option class="tiposimoveis" value="terreno">Terreno</option>
</select>
</div>
<div class="col-md-4 col-xs-12 alt-busca-coluna">
<select class="js-example-basic-single form-control" id="localidade" name="localidade">
<option value="">Selecione a localidade</option>
<option value="/miracema">Miracema</option>
<option value="/miracema/bairro-hospital">Miracema, Bairro Hospital</option>
<option value="/miracema/centro">Miracema, Centro</option>
<option value="/rio-de-janeiro">Rio de Janeiro</option>
<option value="/rio-de-janeiro/icarai">Rio de Janeiro, Icaraí</option>
<option value="/rio-de-janeiro/praca-da-bandeira">Rio de Janeiro, Praça da Bandeira</option>
<option value="/rio-de-janeiro/sao-cristovao-">Rio de Janeiro, São Cristovão </option>
<option value="/santo-antonio-de-padua">Santo Antônio de Pádua</option>
<option value="/santo-antonio-de-padua/aeroporto">Santo Antônio de Pádua, Aeroporto</option>
<option value="/santo-antonio-de-padua/alexis">Santo Antônio de Pádua, Alexis</option>
<option value="/santo-antonio-de-padua/arraialzinho">Santo Antônio de Pádua, Arraialzinho</option>
<option value="/santo-antonio-de-padua/beira-rio-">Santo Antônio de Pádua, Beira Rio </option>
<option value="/santo-antonio-de-padua/cafe-garoto">Santo Antônio de Pádua, Café Garoto</option>
<option value="/santo-antonio-de-padua/caixa-d'agua">Santo Antônio de Pádua, Caixa D'água</option>
<option value="/santo-antonio-de-padua/campo-alegre">Santo Antônio de Pádua, Campo Alegre</option>
<option value="/santo-antonio-de-padua/cehab">Santo Antônio de Pádua, CEHAB</option>
<option value="/santo-antonio-de-padua/centro">Santo Antônio de Pádua, Centro</option>
<option value="/santo-antonio-de-padua/cidade-nova">Santo Antônio de Pádua, Cidade Nova</option>
<option value="/santo-antonio-de-padua/dezessete">Santo Antônio de Pádua, Dezessete</option>
<option value="/santo-antonio-de-padua/divineia">Santo Antônio de Pádua, Divinéia</option>
<option value="/santo-antonio-de-padua/farol">Santo Antônio de Pádua, Farol</option>
<option value="/santo-antonio-de-padua/ferreira">Santo Antônio de Pádua, Ferreira</option>
<option value="/santo-antonio-de-padua/fonseca">Santo Antônio de Pádua, Fonseca</option>
<option value="/santo-antonio-de-padua/gabry">Santo Antônio de Pádua, Gabry</option>
<option value="/santo-antonio-de-padua/gerador">Santo Antônio de Pádua, Gerador</option>
<option value="/santo-antonio-de-padua/gloria">Santo Antônio de Pádua, Glória</option>
<option value="/santo-antonio-de-padua/hotel-das-aguas-">Santo Antônio de Pádua, Hotel das Águas </option>
<option value="/santo-antonio-de-padua/loteamento-alphaville">Santo Antônio de Pádua, Loteamento Alphaville</option>
<option value="/santo-antonio-de-padua/loteamento-alvim">Santo Antônio de Pádua, Loteamento Alvim</option>
<option value="/santo-antonio-de-padua/loteamento-carvalhos">Santo Antônio de Pádua, Loteamento Carvalhos</option>
<option value="/santo-antonio-de-padua/loteamento-celso-sardemberg">Santo Antônio de Pádua, Loteamento Celso Sardemberg</option>
<option value="/santo-antonio-de-padua/loteamento-eccard">Santo Antônio de Pádua, Loteamento Eccard</option>
<option value="/santo-antonio-de-padua/loteamento-vale-do-ype">Santo Antônio de Pádua, Loteamento Vale do Ypê</option>
<option value="/santo-antonio-de-padua/mirante">Santo Antônio de Pádua, Mirante</option>
<option value="/santo-antonio-de-padua/monte-libano">Santo Antônio de Pádua, Monte Líbano</option>
<option value="/santo-antonio-de-padua/parque-das-aguas">Santo Antônio de Pádua, Parque das Águas</option>
<option value="/santo-antonio-de-padua/pereira">Santo Antônio de Pádua, Pereira</option>
<option value="/santo-antonio-de-padua/santa-afra">Santo Antônio de Pádua, Santa Afra</option>
<option value="/santo-antonio-de-padua/sao-felix">Santo Antônio de Pádua, São Félix</option>
<option value="/santo-antonio-de-padua/sao-joao-batista">Santo Antônio de Pádua, São João Batista</option>
<option value="/santo-antonio-de-padua/sao-jose">Santo Antônio de Pádua, São José</option>
<option value="/santo-antonio-de-padua/sao-luiz">Santo Antônio de Pádua, São Luiz</option>
<option value="/santo-antonio-de-padua/tavares">Santo Antônio de Pádua, Tavares</option>
</select>
</div>
</form>
<div class="col-md-2 col-xs-12 alt-busca-coluna">
<button class="btn btn-primary btn-search btn-block" id="pesquisar">
<i aria-hidden="true" class="fa fa-search"></i>
<strong>BUSCAR</strong>
</button>
</div>
</div>
<div class="item">
<form>
<div class="col-md-5 col-xs-6 alt-busca-coluna alt-tipo-transacao-item-select">
<select class="form-control " id="transacaoByCodigo" name="transacao">
<option class="transacao" value="">O que deseja fazer?</option>
<option class="transacao" value="venda">Comprar</option>
<option class="transacao" value="aluguel">Alugar</option>
</select>
</div>
<div class="col-md-5 col-xs-6 alt-busca-coluna">
<input class="form-control" id="codigo" name="codigo" placeholder="Qual o código do imóvel que procura?" type="text"/>
</div>
</form>
<div class="col-md-2 col-xs-12 alt-busca-coluna">
<button class="btn btn-primary btn-search btn-block" id="pesquisarByCodigo">
<i aria-hidden="true" class="fa fa-search"></i>
<strong>BUSCAR</strong>
</button>
</div>
</div>
</div>
<ul class="nav nav-pills nav-justified">
<li class="alt-btn-tabs-search active" data-slide-to="0" data-target="#myCarouselSearchHome"><a href="#">Buscar por localização</a></li>
<li class="alt-btn-tabs-search" data-slide-to="1" data-target="#myCarouselSearchHome"><a href="#">Buscar por código do anúncio</a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="container">
<div class="title-box">
<h3>Destaques</h3>
<div class="bordered">
</div>
</div>
<div class="alt-galeria-002 row listings-items-wrapper center-block">
<div class=" col-md-3 col-sm-6 col-xs-12 alt-galeria-item ">
<div class="item-inner center-block">
<div class="image-wrapper">
<a class="alt-js-aplica-marca-dagua" href="/santiagoimoveis/imovel/casa-aluguel-tavares-santo-antonio-de-padua-rj/57350">
<canvas class="canvas"></canvas>
<img alt="" src="https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/imagem-imovel-1602173686850637377598181163450.jpg"/>
</a>
<span class="fa fa-heart alt-galeria-icone-favorito alt-btn-favorito" data-imovel="57350" id="alt-btn-favorito-57350"></span>
</div>
<div class="desc-box ">
<h4 class="alt-texto-responsivo">
<a href="/santiagoimoveis/imovel/casa-aluguel-tavares-santo-antonio-de-padua-rj/57350">
                                Casa para Aluguel
                            </a>
</h4>
<ul class="col-md-12 alt-galeria-item-features item-features alt-texto-responsivo">
<li class="col-md-4 col-sm-4"><span class="icon-banheira"></span><br/>2 banheiros</li>
<li class="col-md-4 col-sm-4"><span class="icon-terreno"></span><br/>0 m²</li>
<li class="col-md-4 col-sm-4"><span class="icon-dormitorio"></span><br/>3 quartos</li>
</ul>
<div class="buttons-wrapper text-center ">
<div class="alt-texto-responsivo alt-line-tags">
</div>
<a class="btn alt-galeria-item-btn-valor col-lg-6 col-md-6 alt-js-carregando" href="/santiagoimoveis/imovel/casa-aluguel-tavares-santo-antonio-de-padua-rj/57350">R$750</a>
<div class="location">
<a class="alt-js-carregando" href="/santiagoimoveis/imovel/casa-aluguel-tavares-santo-antonio-de-padua-rj/57350">
<i class="fa fa-map-marker"></i> Santo Antônio de Pádua - Tavares <br/>Rua Professor Ismael de L. Coutinho </a>
</div>
</div>
<div class="clearfix">
</div>
</div>
</div>
</div>
<script type="application/ld+json">
{
	"@context": "https://schema.org",
	"@type": "RentAction",
	"realEstateAgent": {
		"@type": "RealEstateAgent",
		"name": "SANTIAGO I. PÁDUA EIRELI ME",
		"url": "https://www.santiagoimoveispadua.com.br",
		"logo": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg",
		"image": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg",
		"address": "Rua dos Leites, nº 39, sobreloja, Centro - Santo Antônio de Pádua ",
		"priceRange": "R$750",
		"telephone": "2238533147"
	},
	"price": "R$750",
	"priceSpecification": {
		"@type": "PriceSpecification",
		"priceCurrency": "BRL"
	},
	"object": {
		"@type": "SingleFamilyResidence",
		"address": {
		"@type": "PostalAddress",
		"addressCountry": {
			"@type": "Country",
			"name": "BR"
		},
		"addressLocality": "Santo Antônio de Pádua",
		"addressRegion": "19",
		"streetAddress": "Santo Antônio de Pádua"
		},
		"name": "Casa para Aluguel",
		"image": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/imagem-imovel-1602173686850637377598181163450.jpg",
		"url": "https://www.santiagoimoveispadua.com.br/santiagoimoveis/imovel/casa-aluguel-tavares-santo-antonio-de-padua-rj/57350"
	}
}
</script> <div class=" col-md-3 col-sm-6 col-xs-12 alt-galeria-item ">
<div class="item-inner center-block">
<div class="image-wrapper">
<a class="alt-js-aplica-marca-dagua" href="/santiagoimoveis/imovel/apartamento--aluguel-parque-das-aguas-santo-antonio-de-padua-rj/26498">
<canvas class="canvas"></canvas>
<img alt="" src="https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/dscn7356636815278272549243636994001484642301.jpg"/>
</a>
<span class="fa fa-heart alt-galeria-icone-favorito alt-btn-favorito" data-imovel="26498" id="alt-btn-favorito-26498"></span>
</div>
<div class="desc-box ">
<h4 class="alt-texto-responsivo">
<a href="/santiagoimoveis/imovel/apartamento--aluguel-parque-das-aguas-santo-antonio-de-padua-rj/26498">
                                Apartamento  para Aluguel
                            </a>
</h4>
<ul class="col-md-12 alt-galeria-item-features item-features alt-texto-responsivo">
<li class="col-md-4 col-sm-4"><span class="icon-banheira"></span><br/>2 banheiros</li>
<li class="col-md-4 col-sm-4"><span class="icon-terreno"></span><br/>0 m²</li>
<li class="col-md-4 col-sm-4"><span class="icon-dormitorio"></span><br/>2 quartos</li>
</ul>
<div class="buttons-wrapper text-center ">
<div class="alt-texto-responsivo alt-line-tags">
</div>
<a class="btn alt-galeria-item-btn-valor col-lg-6 col-md-6 alt-js-carregando" href="/santiagoimoveis/imovel/apartamento--aluguel-parque-das-aguas-santo-antonio-de-padua-rj/26498">R$790</a>
<div class="location">
<a class="alt-js-carregando" href="/santiagoimoveis/imovel/apartamento--aluguel-parque-das-aguas-santo-antonio-de-padua-rj/26498">
<i class="fa fa-map-marker"></i> Santo Antônio de Pádua - Parque das Águas <br/>Rua Abelardo Joaquim dos Santos, nº25, aptº304, F </a>
</div>
</div>
<div class="clearfix">
</div>
</div>
</div>
</div>
<script type="application/ld+json">
{
	"@context": "https://schema.org",
	"@type": "RentAction",
	"realEstateAgent": {
		"@type": "RealEstateAgent",
		"name": "SANTIAGO I. PÁDUA EIRELI ME",
		"url": "https://www.santiagoimoveispadua.com.br",
		"logo": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg",
		"image": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg",
		"address": "Rua dos Leites, nº 39, sobreloja, Centro - Santo Antônio de Pádua ",
		"priceRange": "R$790",
		"telephone": "2238533147"
	},
	"price": "R$790",
	"priceSpecification": {
		"@type": "PriceSpecification",
		"priceCurrency": "BRL"
	},
	"object": {
		"@type": "SingleFamilyResidence",
		"address": {
		"@type": "PostalAddress",
		"addressCountry": {
			"@type": "Country",
			"name": "BR"
		},
		"addressLocality": "Santo Antônio de Pádua",
		"addressRegion": "19",
		"streetAddress": "Santo Antônio de Pádua"
		},
		"name": "Apartamento  para Aluguel",
		"image": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/dscn7356636815278272549243636994001484642301.jpg",
		"url": "https://www.santiagoimoveispadua.com.br/santiagoimoveis/imovel/apartamento--aluguel-parque-das-aguas-santo-antonio-de-padua-rj/26498"
	}
}
</script> <div class=" col-md-3 col-sm-6 col-xs-12 alt-galeria-item ">
<div class="item-inner center-block">
<div class="image-wrapper">
<a class="alt-js-aplica-marca-dagua" href="/santiagoimoveis/imovel/apartamento--aluguel-ferreira-santo-antonio-de-padua-rj/84233">
<canvas class="canvas"></canvas>
<img alt="" src="https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/imagem-imovel-1599155795239637347420783056073.jpg"/>
</a>
<span class="fa fa-heart alt-galeria-icone-favorito alt-btn-favorito" data-imovel="84233" id="alt-btn-favorito-84233"></span>
</div>
<div class="desc-box ">
<h4 class="alt-texto-responsivo">
<a href="/santiagoimoveis/imovel/apartamento--aluguel-ferreira-santo-antonio-de-padua-rj/84233">
                                Apartamento  para Aluguel
                            </a>
</h4>
<ul class="col-md-12 alt-galeria-item-features item-features alt-texto-responsivo">
<li class="col-md-4 col-sm-4"><span class="icon-terreno"></span><br/>0 m²</li>
</ul>
<div class="buttons-wrapper text-center ">
<div class="alt-texto-responsivo alt-line-tags">
</div>
<a class="btn alt-galeria-item-btn-valor col-lg-6 col-md-6 alt-js-carregando" href="/santiagoimoveis/imovel/apartamento--aluguel-ferreira-santo-antonio-de-padua-rj/84233">R$800</a>
<div class="location">
<a class="alt-js-carregando" href="/santiagoimoveis/imovel/apartamento--aluguel-ferreira-santo-antonio-de-padua-rj/84233">
<i class="fa fa-map-marker"></i> Santo Antônio de Pádua - Ferreira <br/> Av. Alcides de Souza Castro,  N°188, apt° 101 </a>
</div>
</div>
<div class="clearfix">
</div>
</div>
</div>
</div>
<script type="application/ld+json">
{
	"@context": "https://schema.org",
	"@type": "RentAction",
	"realEstateAgent": {
		"@type": "RealEstateAgent",
		"name": "SANTIAGO I. PÁDUA EIRELI ME",
		"url": "https://www.santiagoimoveispadua.com.br",
		"logo": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg",
		"image": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg",
		"address": "Rua dos Leites, nº 39, sobreloja, Centro - Santo Antônio de Pádua ",
		"priceRange": "R$800",
		"telephone": "2238533147"
	},
	"price": "R$800",
	"priceSpecification": {
		"@type": "PriceSpecification",
		"priceCurrency": "BRL"
	},
	"object": {
		"@type": "SingleFamilyResidence",
		"address": {
		"@type": "PostalAddress",
		"addressCountry": {
			"@type": "Country",
			"name": "BR"
		},
		"addressLocality": "Santo Antônio de Pádua",
		"addressRegion": "19",
		"streetAddress": "Santo Antônio de Pádua"
		},
		"name": "Apartamento  para Aluguel",
		"image": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/imagem-imovel-1599155795239637347420783056073.jpg",
		"url": "https://www.santiagoimoveispadua.com.br/santiagoimoveis/imovel/apartamento--aluguel-ferreira-santo-antonio-de-padua-rj/84233"
	}
}
</script> <div class=" col-md-3 col-sm-6 col-xs-12 alt-galeria-item ">
<div class="item-inner center-block">
<div class="image-wrapper">
<a class="alt-js-aplica-marca-dagua" href="/santiagoimoveis/imovel/apartamento--aluguel-centro-santo-antonio-de-padua-rj/22641">
<canvas class="canvas"></canvas>
<img alt="" src="https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/imagem-imovel-1608224856632637438144592498584.jpg"/>
</a>
<span class="fa fa-heart alt-galeria-icone-favorito alt-btn-favorito" data-imovel="22641" id="alt-btn-favorito-22641"></span>
</div>
<div class="desc-box ">
<h4 class="alt-texto-responsivo">
<a href="/santiagoimoveis/imovel/apartamento--aluguel-centro-santo-antonio-de-padua-rj/22641">
                                Apartamento  para Aluguel
                            </a>
</h4>
<ul class="col-md-12 alt-galeria-item-features item-features alt-texto-responsivo">
<li class="col-md-4 col-sm-4"><span class="icon-banheira"></span><br/>2 banheiros</li>
<li class="col-md-4 col-sm-4"><span class="icon-terreno"></span><br/>0 m²</li>
<li class="col-md-4 col-sm-4"><span class="icon-dormitorio"></span><br/>2 quartos</li>
</ul>
<div class="buttons-wrapper text-center ">
<div class="alt-texto-responsivo alt-line-tags">
</div>
<a class="btn alt-galeria-item-btn-valor col-lg-6 col-md-6 alt-js-carregando" href="/santiagoimoveis/imovel/apartamento--aluguel-centro-santo-antonio-de-padua-rj/22641">R$950</a>
<div class="location">
<a class="alt-js-carregando" href="/santiagoimoveis/imovel/apartamento--aluguel-centro-santo-antonio-de-padua-rj/22641">
<i class="fa fa-map-marker"></i> Santo Antônio de Pádua - Centro <br/>Rua Coronel Olivier, nº204, aptº202 </a>
</div>
</div>
<div class="clearfix">
</div>
</div>
</div>
</div>
<script type="application/ld+json">
{
	"@context": "https://schema.org",
	"@type": "RentAction",
	"realEstateAgent": {
		"@type": "RealEstateAgent",
		"name": "SANTIAGO I. PÁDUA EIRELI ME",
		"url": "https://www.santiagoimoveispadua.com.br",
		"logo": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg",
		"image": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg",
		"address": "Rua dos Leites, nº 39, sobreloja, Centro - Santo Antônio de Pádua ",
		"priceRange": "R$950",
		"telephone": "2238533147"
	},
	"price": "R$950",
	"priceSpecification": {
		"@type": "PriceSpecification",
		"priceCurrency": "BRL"
	},
	"object": {
		"@type": "SingleFamilyResidence",
		"address": {
		"@type": "PostalAddress",
		"addressCountry": {
			"@type": "Country",
			"name": "BR"
		},
		"addressLocality": "Santo Antônio de Pádua",
		"addressRegion": "19",
		"streetAddress": "Santo Antônio de Pádua"
		},
		"name": "Apartamento  para Aluguel",
		"image": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/imagem-imovel-1608224856632637438144592498584.jpg",
		"url": "https://www.santiagoimoveispadua.com.br/santiagoimoveis/imovel/apartamento--aluguel-centro-santo-antonio-de-padua-rj/22641"
	}
}
</script> <div class=" col-md-3 col-sm-6 col-xs-12 alt-galeria-item ">
<div class="item-inner center-block">
<div class="image-wrapper">
<a class="alt-js-aplica-marca-dagua" href="/santiagoimoveis/imovel/casa-aluguel-mirante-santo-antonio-de-padua-rj/41935">
<canvas class="canvas"></canvas>
<img alt="" src="https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/DSCN9421637147041403077212.JPG"/>
</a>
<span class="fa fa-heart alt-galeria-icone-favorito alt-btn-favorito" data-imovel="41935" id="alt-btn-favorito-41935"></span>
</div>
<div class="desc-box ">
<h4 class="alt-texto-responsivo">
<a href="/santiagoimoveis/imovel/casa-aluguel-mirante-santo-antonio-de-padua-rj/41935">
                                Casa para Aluguel
                            </a>
</h4>
<ul class="col-md-12 alt-galeria-item-features item-features alt-texto-responsivo">
<li class="col-md-4 col-sm-4"><span class="icon-banheira"></span><br/>2 banheiros</li>
<li class="col-md-4 col-sm-4"><span class="icon-terreno"></span><br/>0 m²</li>
<li class="col-md-4 col-sm-4"><span class="icon-dormitorio"></span><br/>3 quartos</li>
</ul>
<div class="buttons-wrapper text-center ">
<div class="alt-texto-responsivo alt-line-tags">
</div>
<a class="btn alt-galeria-item-btn-valor col-lg-6 col-md-6 alt-js-carregando" href="/santiagoimoveis/imovel/casa-aluguel-mirante-santo-antonio-de-padua-rj/41935">R$1.000</a>
<div class="location">
<a class="alt-js-carregando" href="/santiagoimoveis/imovel/casa-aluguel-mirante-santo-antonio-de-padua-rj/41935">
<i class="fa fa-map-marker"></i> Santo Antônio de Pádua - Mirante <br/>Rua Dalton Andrade Barros, nº170. Referência: rua do depósito de gás. </a>
</div>
</div>
<div class="clearfix">
</div>
</div>
</div>
</div>
<script type="application/ld+json">
{
	"@context": "https://schema.org",
	"@type": "RentAction",
	"realEstateAgent": {
		"@type": "RealEstateAgent",
		"name": "SANTIAGO I. PÁDUA EIRELI ME",
		"url": "https://www.santiagoimoveispadua.com.br",
		"logo": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg",
		"image": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg",
		"address": "Rua dos Leites, nº 39, sobreloja, Centro - Santo Antônio de Pádua ",
		"priceRange": "R$1.000",
		"telephone": "2238533147"
	},
	"price": "R$1.000",
	"priceSpecification": {
		"@type": "PriceSpecification",
		"priceCurrency": "BRL"
	},
	"object": {
		"@type": "SingleFamilyResidence",
		"address": {
		"@type": "PostalAddress",
		"addressCountry": {
			"@type": "Country",
			"name": "BR"
		},
		"addressLocality": "Santo Antônio de Pádua",
		"addressRegion": "19",
		"streetAddress": "Santo Antônio de Pádua"
		},
		"name": "Casa para Aluguel",
		"image": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/DSCN9421637147041403077212.JPG",
		"url": "https://www.santiagoimoveispadua.com.br/santiagoimoveis/imovel/casa-aluguel-mirante-santo-antonio-de-padua-rj/41935"
	}
}
</script> <div class=" col-md-3 col-sm-6 col-xs-12 alt-galeria-item ">
<div class="item-inner center-block">
<div class="image-wrapper">
<a class="alt-js-aplica-marca-dagua" href="/santiagoimoveis/imovel/apartamento--aluguel-sao-felix-santo-antonio-de-padua-rj/50187">
<canvas class="canvas"></canvas>
<img alt="" src="https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/imagem-imovel-1583267166609637188532472321667.JPG"/>
</a>
<span class="fa fa-heart alt-galeria-icone-favorito alt-btn-favorito" data-imovel="50187" id="alt-btn-favorito-50187"></span>
</div>
<div class="desc-box ">
<h4 class="alt-texto-responsivo">
<a href="/santiagoimoveis/imovel/apartamento--aluguel-sao-felix-santo-antonio-de-padua-rj/50187">
                                Apartamento  para Aluguel
                            </a>
</h4>
<ul class="col-md-12 alt-galeria-item-features item-features alt-texto-responsivo">
<li class="col-md-4 col-sm-4"><span class="icon-banheira"></span><br/>2 banheiros</li>
<li class="col-md-4 col-sm-4"><span class="icon-terreno"></span><br/>0 m²</li>
<li class="col-md-4 col-sm-4"><span class="icon-dormitorio"></span><br/>2 quartos</li>
</ul>
<div class="buttons-wrapper text-center ">
<div class="alt-texto-responsivo alt-line-tags">
</div>
<a class="btn alt-galeria-item-btn-valor col-lg-6 col-md-6 alt-js-carregando" href="/santiagoimoveis/imovel/apartamento--aluguel-sao-felix-santo-antonio-de-padua-rj/50187">R$1.200</a>
<div class="location">
<a class="alt-js-carregando" href="/santiagoimoveis/imovel/apartamento--aluguel-sao-felix-santo-antonio-de-padua-rj/50187">
<i class="fa fa-map-marker"></i> Santo Antônio de Pádua - São Félix <br/>Rua General Odílio Dinis, s/nº, Aptº 201 </a>
</div>
</div>
<div class="clearfix">
</div>
</div>
</div>
</div>
<script type="application/ld+json">
{
	"@context": "https://schema.org",
	"@type": "RentAction",
	"realEstateAgent": {
		"@type": "RealEstateAgent",
		"name": "SANTIAGO I. PÁDUA EIRELI ME",
		"url": "https://www.santiagoimoveispadua.com.br",
		"logo": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg",
		"image": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg",
		"address": "Rua dos Leites, nº 39, sobreloja, Centro - Santo Antônio de Pádua ",
		"priceRange": "R$1.200",
		"telephone": "2238533147"
	},
	"price": "R$1.200",
	"priceSpecification": {
		"@type": "PriceSpecification",
		"priceCurrency": "BRL"
	},
	"object": {
		"@type": "SingleFamilyResidence",
		"address": {
		"@type": "PostalAddress",
		"addressCountry": {
			"@type": "Country",
			"name": "BR"
		},
		"addressLocality": "Santo Antônio de Pádua",
		"addressRegion": "19",
		"streetAddress": "Santo Antônio de Pádua"
		},
		"name": "Apartamento  para Aluguel",
		"image": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/imagem-imovel-1583267166609637188532472321667.JPG",
		"url": "https://www.santiagoimoveispadua.com.br/santiagoimoveis/imovel/apartamento--aluguel-sao-felix-santo-antonio-de-padua-rj/50187"
	}
}
</script> </div>
</div>
<div class="alt-anuncie-aqui alt-margem-top-40" style="background-image: url(../images/anuncio-banner.png);">
<div class="alt-anuncie-shade"></div>
<div class="alt-anuncie-text-container">
<div class="alt-anuncie-text">
<p class="alt-fonte-size-32 roboto">Anuncie seu imóvel em nosso site e receba mais contatos</p>
<a class="btn alt-btn-corretagem-anuncio" href="/santiagoimoveis/anunciar">ANUNCIE AQUI</a>
</div>
</div>
</div>
<div class="alt-js-identificador-rodape">
<footer class="main-footer clearfix">
<div class="container">
<div class="row">
<div class="col-md-4 col-sm-6 alt-footer-item">
<div class="alt-footer-item-content">
<a class="alt-footer-alt-img-logo" href="index.html" title="SANTIAGO I. PÁDUA EIRELI ME">
<img alt="SANTIAGO I. PÁDUA EIRELI ME" src="https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg"/>
</a>
<div class="clearfix"></div>
<p>Compre, venda ou alugue seu imóvel na SANTIAGO I. PÁDUA EIRELI ME. As melhores ofertas você encontra aqui.</p>
</div>
</div>
<div class="col-md-4 col-sm-6 alt-footer-item">
<div class="alt-footer-item-content">
<h2 class="title">Endereços</h2>
<ul class="contact-info">
<li class="clearfix">
<i class="fa fa-map-marker fa-lg"></i>
<label>Rua dos Leites, nº 39, sobreloja, Centro - Santo Antônio de Pádua </label>
</li>
<li class="clearfix">
<i class="fa fa-mobile fa-lg"></i>
<label>2238533147
                                    <span>/ 22981560036</span>
</label>
</li>
<li class="clearfix">
<i class="fa fa-envelope-o fa-lg"></i>
<label>
<a href="mailto:atendimento@santiagoimoveispadua.com.br" title="Entre em contato">
                                    atendimento@santiagoimoveispadua.com.br
                                </a>
</label>
</li>
</ul>
</div>
</div>
<div class="col-md-4 col-sm-6 alt-footer-item clearfix">
<div class="alt-footer-item-content js-alt-nav-footer">
<h2 class="title">Mapa do site</h2>
<ul class="links js-alt-nav">
<li class="alt-tipo-transacao-item-aluguel"><a href="/santiagoimoveis/aluguel" title="Imóveis para aluguel">Alugar</a></li>
<li class="alt-tipo-transacao-item-venda"><a href="/santiagoimoveis/venda" title="Imóveis para venda">Comprar</a></li>
<li><a href="/santiagoimoveis/anunciar" title="Anuncie o seu imóvel">Anunciar imóvel</a></li>
<li><a href="/santiagoimoveis/sobre" title="Quem somos">Sobre</a></li>
<li><a href="/santiagoimoveis/contato" title="Contato">Contato</a></li>
<li class="alt-js-link-favoritos-lista"><a href="#" title="Favoritos">Favoritos</a></li>
</ul>
</div>
</div>
</div>
</div>
</footer>
<div class="alt-sub-footer">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="social-list">
<ul class="clearfix">
<li class="facebook">
<a href="https://www.facebook.com/santiagoimoveispadua/?ref=bookmarks" target="_blank" title="Facebook">
<i class="fa fa-facebook"></i>
</a>
</li>
<li class="instagram">
<a href="@santiagoimoveispadua" target="_blank" title="Instagram">
<i class="fa fa-instagram"></i>
</a>
</li>
</ul>
</div>
</div>
</div>
<div class="row alt-identificacao-de-desenvolvimento">
<div class="pull-right hidden-xs">
<div>
<div class="pull-right alt-margem-left-20">
<a href="https://www.alterdata.com.br" target="_blank">
<img src="/images/logo-alterdata-branco-nova.png"/>
</a>
</div>
<div class="pull-right">
<a href="https://www.alterdata.com.br/contato" target="_blank">
<b>Contato</b>
</a>
<span>|</span>
<a href="https://cliente.alterdata.com.br/pg/login/central-login" target="_blank">
<b>Suporte</b>
</a>
</div>
</div>
</div>
<strong>© 1989 - 2021 - <a href="https://www.alterdata.com.br" target="_blank">Alterdata Software</a> - </strong> Direitos reservados.
        </div>
</div>
</div>
<a class="alt-toTop" href="#" id="alt-toTop" title="Voltar para o topo">
<i class="fa fa-chevron-up"></i>
</a>
</div>
<input id="validation-msg" type="hidden" value=""/>
<input id="imovel-msg" type="hidden" value=""/>
<script>
		var urlImagemMarcaDagua, posicaoMaradagua, nomecurto = 'santiagoimoveis',
            urlChatAtendimento = '', codigoCRM = '549688',
            templateTopoLayout = 'Topo_001.cshtml',
            COOKIE_NAME_FAVORITOS = 'FAVORITOS_CORRETAGEM_CRM_' + '549688';
	</script>
<script src="/js/min/site.min.js?v=0fQswVSUraooOTvQMUOIWYg4y230PuHbel-mvARa11s"></script>
<script>var tempDataSucesso = '', tipoTransacaoAdministradora = "todas";</script>
<script src="/js/paginas/banner.js?v7"></script>
<script src="/js/paginas/home.js?v8"></script>
<script src="/js/paginas/favoritos.js"></script>
<script>		
            urlImagemMarcaDagua = 'https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/04636809245224473654.jpg',
            posicaoMaradagua = '0';       
        </script>
<script src="/js/aplica-marca-dagua.js"></script>
<script type="application/ld+json">
{    
    "@context": "https://schema.org",
    "@type": "Organization",
    "name": "SANTIAGO I. PÁDUA EIRELI ME",
    "description": "Compre, venda ou alugue seu imóvel na SANTIAGO I. PÁDUA EIRELI ME. As melhores ofertas você encontra aqui.",
    "address": {
        "@type": "PostalAddress",
        "addressCountry": {
            "@type": "Country",
            "name": "BR" 
        }, 
        "addressLocality": "Santo Antônio de Pádua ",
        "addressRegion": "RJ",
        "postalCode": "",
        "streetAddress": "Rua dos Leites, nº 39, sobreloja, Centro - Santo Antônio de Pádua "
    },
    "brand": {
        "@type": "Brand",
        "logo": "https://web-immobileweb.s3.amazonaws.com/corretagem/publica/683ac911-43c4-45b9-b0ae-8d21d57b8747/logo636809942668633797.jpg"
    },
    "telephone": "+55 2238533147",
    "URL": "https://www.santiagoimoveispadua.com.br"
}
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-78061149-3"></script>
<script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-78061149-3');         
    </script>
</body>
</html>