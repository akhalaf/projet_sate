<!DOCTYPE html>
<!--[if (lte IE 9)&!(IEMobile)]><html class="no-js oldie" lang="nb-NO"><![endif]--><!--[if (IE 10)&!(IEMobile)]><html class="no-js ie10" lang="nb-NO"><![endif]--><!--[if (IE 10)&(IEMobile)]><html class="no-js ie10m" lang="nb-NO"><![endif]--><!--[if (!IE)]><!--><html class="no-js" lang="nb-NO"><!--<![endif]-->
<head>
<meta charset="utf-8"/>
<title>
		Side ikke funnet - skanke.no	</title>
<script>
		//Tell the css that we are in fact using javascript
		var html = document.getElementsByTagName("html")[0];
		html.className = html.className.replace("no-js", "js");

		var sircon = window.sircon || {};
			sircon.objects = sircon.objects || {};
			sircon.plugins = sircon.plugins || {};
			sircon.funcs = sircon.funcs || {};
			sircon.vars = sircon.vars || {};
		</script>
<!-- This site is optimized with the Yoast SEO plugin v15.6.2 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex, follow" name="robots"/>
<meta content="nb_NO" property="og:locale"/>
<meta content="Side ikke funnet - skanke.no" property="og:title"/>
<meta content="skanke.no" property="og:site_name"/>
<!-- script moved to footer -->
<!-- / Yoast SEO plugin. -->
<link href="//s.w.org" rel="dns-prefetch"/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.14.0 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<!-- script moved to footer -->
<!-- / Google Analytics by MonsterInsights -->
<!-- script moved to footer -->
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://skanke.no/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://skanke.no/wp-content/plugins/widget-options/assets/css/widget-options.css" id="widgetopts-styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://skanke.no/wp-content/plugins/google-analytics-for-wordpress/assets/css/frontend.min.css?ver=7.14.0" id="monsterinsights-popular-posts-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://skanke.no/wp-content/plugins/sircon-simple-showcase/style/simple-slideshow.css?ver=5.6" id="simple-sircon-slideshow-styles-css" media="all" rel="stylesheet" type="text/css"/>
<!-- script moved to footer -->
<!-- script moved to footer -->
<link href="https://skanke.no/wp-json/" rel="https://api.w.org/"/><!-- windows mobile 8 viewport bugfix -->
<!-- script moved to footer -->
<!-- disable vendor formatting of geo:, mailto: and tel: links -->
<meta content="address=no" name="format-detection"/>
<meta content="email=no" name="format-detection"/>
<meta content="telephone=no" name="format-detection"/>
<!-- set mobile friendly viewport -->
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://skanke.no/wp-content/themes/sircon-evolution/style/reset-and-preset.css" rel="stylesheet" type="text/css"/>
<link href="https://skanke.no/wp-content/themes/sircon-evolution/style/main-styles.css" rel="stylesheet" type="text/css"/>
<link href="https://skanke.no/wp-content/themes/sircon-evolution/style/optimized-cache/sassy.scss" rel="stylesheet" type="text/css"/>
<link href="https://skanke.no/wp-content/themes/sircon-evolution/style/print-styles.css" rel="stylesheet" type="text/css"/>
<!-- FAVICONS --><!-- NOTO SANS --><link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic'" rel="stylesheet" type="text/css"/> <link href="http://fonts.googleapis.com/css?family=Orbitron:700,400" rel="stylesheet" type="text/css"/>
</head>
<body class="error404 has-sidebar-header-1 has-sidebar-center has-sidebar-footer documentfont-noto-sans">
<div data-sirconajax="top-header-image" id="sirconajax-top-header-image"><!-- #sirconajax="top-header-image" --></div> <div aria-live="assertive" class="push-away" id="aria-status" role="status"></div>
<header class="matshad" id="top">
<div class="max headertop">
<div id="main-logo" role="banner"><a aria-label="Klikk for forside" class="ir logo desktop-only" href="https://skanke.no/">Til forsiden</a><a aria-label="Klikk for forside" class="ir logo mobile-only" href="https://skanke.no/">Til forsiden</a></div>
<div data-sirconajax="header-menus" id="sirconajax-header-menus"><div aria-hidden="true" class="default nav click-toggle phone-menu" data-sirconclicktogglemenu="1" id="phone-menu"><button class="trigger-phonemenu" data-sirconclick="trigger-phonemenu">Åpne meny</button><ul class="top-lvl" id="phone-menu-items"><li class="menu-item-has-children menu-item"><a class="menu-clickable" href="https://skanke.no/tjenester/">VÅRE TJENESTER</a><ul class="submenu">
<span class="menu-clickable submenu-back" data-sirconclick="submenu-back">Tilbake</span><li class="menu-item"><a class="menu-clickable" href="https://skanke.no/tjenester/vannskjaering/">Vannskjæring</a></li><li class="menu-item"><a class="menu-clickable" href="https://skanke.no/stalkonstruksjoner/">Stålkonstruksjoner</a></li><li class="menu-item"><a class="menu-clickable" href="https://skanke.no/staltrapper-og-rekkverk/">Ståltrapper og rekkverk</a></li><li class="menu-item"><a class="menu-clickable" href="https://skanke.no/tjenester/">Andre tjenester</a></li></ul></li><li class="menu-item"><a class="menu-clickable" href="https://skanke.no/om-oss/">OM OSS</a></li><li class="menu-item"><a class="menu-clickable" href="https://skanke.no/kontakt/">KONTAKT</a></li></ul></div><!-- #sirconajax="header-menus" --></div>
<div data-sirconajax="header-1" id="sirconajax-header-1"><aside class="has-1-widgets" id="sidebar-header-1"><div class="widget widget_black_studio_tinymce" id="black-studio-tinymce-5"><div class="textwidget"><p><strong>Skanke Stål har spisskompetanse<br/>innenfor alle typer stålkonstruksjoner.</strong> <a class="btn btn-readmore" href="https://skanke.no/stalkonstruksjoner/">LES MER</a></p>
</div></div></aside><!-- #sirconajax="header-1" --></div>
</div>
</header>
<div class="clear" id="middle">
<div data-sirconajax="showcase" id="sirconajax-showcase"><!-- #sirconajax="showcase" --></div>
<div data-sirconajax="main-content" id="sirconajax-main-content"> <div class="max">
<div id="center-content">
<main role="main">
<div class="read-max">
<h3>404 - Not found</h3>
<p>Sorry, this content seems to not exist!<br/>You may try finding what you are looking for by visiting the <a href='\"https://skanke.no\"'>frontpage</a></p>
<p>You may also try searching for your content.</p> </div>
</main>
</div><!-- #center-content -->
</div><!-- .max -->
<!-- #sirconajax="main-content" --></div> </div><!-- #middle -->
<div data-sirconajax="featured_map" id="sirconajax-featured_map"><!-- #sirconajax="featured_map" --></div>
<footer class="matshad" id="bottom">
<div class="max">
<a href="#" id="goto-top"></a>
<div id="logo-bottom"></div>
<div data-sirconajax="footer" id="sirconajax-footer"><aside class="horizontal-widgets has-2-widgets" id="sidebar-footer"><div class="widget widget_black_studio_tinymce" id="black-studio-tinymce-7"><div class="textwidget"><p><a href="tel:72884440">Tlf 72 88 44 40</a><br/>
Fax 72 88 44 42<br/>
<a href="mailto:firmapost@skanke.no">firmapost@skanke.no</a></p>
</div></div><div class="widget widget_black_studio_tinymce" id="black-studio-tinymce-8"><div class="textwidget"><p>Jan Magne, <a href="tel:92495922">tlf 924 95 922</a><br/>
<a href="mailto:jan.magne@skanke.no">jan.magne@skanke.no</a></p>
</div></div></aside><!-- #sirconajax="footer" --></div>
<div id="colophon">
<div class="content"><a href="http://rosenborgreklame.no" target="_blank">Webdesign og konseptutvikling: Rosenborg Reklame as</a> | <a href="https://www.sircon.no" target="_blank">Programmering og Webhotell: Sircon Norge as</a></div>
</div>
</div>
</footer>
<div data-sirconajax="background-image" id="sirconajax-background-image"><!-- #sirconajax="background-image" --></div>
<script src="https://skanke.no/wp-content/themes/sircon-evolution/scripts/default/jquery2.js"></script>
<script src="https://skanke.no/wp-content/themes/sircon-evolution/scripts/optimized-early-cache/optimized-early-scripts.js"></script><script>sircon.vars.is404 = true;</script><script src="https://skanke.no/wp-content/plugins/sircon-simple-showcase/script/simple-slideshow.js"></script><script>sircon.vars.activateAjax = true;</script><script>sircon.vars.wpFastestCacheLoaded = true;</script><script id="wp-embed-js" src="https://skanke.no/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://skanke.no/#website","url":"https://skanke.no/","name":"skanke.no","description":"Skanke St\u00e5l &amp; Sveis","potentialAction":[{"@type":"SearchAction","target":"https://skanke.no/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"nb-NO"}]}</script>
<script data-cfasync="false" type="text/javascript">
    (window.gaDevIds=window.gaDevIds||[]).push("dZGIzZG");
	var mi_version         = '7.14.0';
	var mi_track_user      = true;
	var mi_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-66445280-1';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}

	if ( 'undefined' === typeof gaOptout ) {
		function gaOptout() {
			__gaTrackerOptout();
		}
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-66445280-1', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('send','pageview','/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer);
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Kjører ikke funksjon __gaTracker(' + arguments[0] + " ....) fordi du ikke blir sporet. %s " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/skanke.no\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<script id="monsterinsights-frontend-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[]","home_url":"https:\/\/skanke.no","hash_tracking":"false"};
/* ]]> */
</script>
<script id="monsterinsights-frontend-script-js" src="https://skanke.no/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.14.0" type="text/javascript"></script>
<script>if (navigator.userAgent.match(/IEMobile\/10\.0/)) {var msViewportStyle = document.createElement("style");msViewportStyle.appendChild(document.createTextNode("@-ms-viewport{width:auto!important}"));document.getElementsByTagName("head")[0].appendChild(msViewportStyle);}</script>
<script src="https://skanke.no/wp-content/themes/sircon-evolution/scripts/optimized-cache/optimized-scripts.js"></script>
<script>
	sircon.loadScripts(); //Run theme scripts
	sircon.loadSiteScripts(); //Run site specific scripts
</script>
</body>
</html>
