<!DOCTYPE html>
<html><body><p>ang="en-US"&gt;
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <!--<![endif]-->
</p>
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<title>EnviousWeb is coming soon</title>
<meta content="We are doing some work on our site. Please be patient. Thank you." name="description"/>
<link href="https://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/css/normalize.css" rel="stylesheet"/>
<link href="https://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/css/foundation.css" rel="stylesheet"/>
<link href="https://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/css/animate.css" rel="stylesheet"/>
<link href="https://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/css/icomoon.css" rel="stylesheet"/>
<link href="https://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/css/style.css" rel="stylesheet"/>
<script src="https://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/vendor/custom.modernizr.js"></script>
<!-- Default WordPress jQuery lib (sorry, it just have to go this way because of template system limitations) -->
<script src="https://www.enviousweb.com/wp-includes/js/jquery/jquery.js"></script>
<link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/><link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/><link href="https://fonts.googleapis.com/css?family=Abel&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/><link href="https://fonts.googleapis.com/css?family=Raleway&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/><style>body {background: !important;}.button {background:#9e0039 !important;}.button:hover {background:#9e0039 !important;}.nifty-title {font-family:'Lato' !important;}.nifty-coming-soon-message {font-family:'Lato' !important;}.timer-item {font-family:'Raleway' !important;}body p, .nifty-inform, .nifty-success, .nifty-error, input {font-family:'Abel' !important;}</style>
<div class="nifty-main-wrapper" id="nifty-full-wrapper">
<!-- Page Preloader -->
<div id="preloader"></div>
<div class="nifty-content-wrapper">
<header class="nifty-row ">
<div class="large-12 columns text-center">
<!-- Logo and navigation  -->
<div class="nifty-logo"><a href="https://www.enviousweb.com"><img alt="EnviousWeb" src="http://www.enviousweb.com/wp-content/uploads/2018/07/EnviousWeb.png"/></a></div>
</div>
</header></div>
<div class="nifty-row">
<div class="large-10 small-centered columns text-center">
<div class="nifty-coming-soon-message">
<div class="tlt" id="animated_intro">
<ul class="texts" style="display: none">
<li>Website Is Under Reconstruction.</li>
<li>1</li>
</ul>
</div>
</div>
</div>
</div>
<!-- Timer Section -->
<!-- Content Section -->
<div class="nifty-content-full">
<div class="nifty-row">
<!-- Slider Section -->
<ul class="bxslider">
<!-- Slide One - Subscribe Here -->
<li>
<section class="large-12 columns"> <form action="https://www.enviousweb.com/wp-admin/admin-ajax.php" id="contact" method="post">
<div class="large-4 small-centered columns">
<div class="nifty-inform">Sign up to find out when we launch </div> <div class="nifty-row collapse">
<div class="small-8 columns">
<input autocomplete="off" id="email" name="email" placeholder="Enter Email..." type="text"/>
</div>
<div class="small-4 columns">
<input class="button prefix" name="submit" type="submit" value=" Sign Up"/>
</div>
<div class="nifty-success" style="display:none">You will be notified, thanks.</div>
<div class="nifty-error" style="display:none"> Please, enter valid email address.</div>
</div>
</div>
</form>
</section>
</li>
<!-- Slide Two - About Us -->
<li>
<section class="small-12 small-centered columns">
<div class="nifty-contact-details">
<p><strong>ACME COMPANY
				</strong></p>
<p><span aria-hidden="true" class="icon-house"></span>
				230 New Found lane, 8900 New City</p>
<p><span aria-hidden="true" class="icon-phone"></span>
				+555 53211 777</p><p> <span aria-hidden="true" class="icon-mail"></span> <a href="mailto:someone@example.com">someone@example.com</a></p></div>
</section>
</li>
<!-- Slide Three - Social links -->
<section class="large-12 columns"><div class="nifty-row"></div></section>
</ul>
</div>
</div>
</div>
<!-- jQuery Vegas Background Slider -->
<script>
	jQuery(document).ready(function($){
	$('#nifty, body').vegas({
		 animation: 'random',
		 cover: true,
		 animationDuration: '2000',
		 timer: false,
		 transition: 'random',
		 delay:10000,
		 opacity:0.5,
		 overlay:'https://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/admin//assets/images/patterns/16.png',
			slides: [
				{ src:'http://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/admin/assets/slideshow/1.jpg'},
				{ src:'http://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/admin/assets/slideshow/2.jpg'},
				{ src:'http://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/admin/assets/slideshow/3.jpg'},
				{ src:'http://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/admin/assets/slideshow/4.jpg'}
			 ]
    }); });
	</script>
<script>

// Timer Settings  //

jQuery(function($) {
  $('div#clock').countdown("1970/01/01 00:00", function(event) {
    var $this = $(this);
    switch(event.type) {
      case "seconds":
      case "minutes":
      case "hours":
      case "days":
      case "weeks":
      case "daysLeft":
        $this.find('span#'+event.type).html(event.value);

        break;
      case "finished":
        $this.hide();
        break;
    }
  });
});
</script>
<!-- Footer js scripts -->
<script src="https://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/scripts.js"></script>
<script src="https://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/jquery.countdown.js"></script>
<script src="https://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/jquery.bxslider.min.js"></script>
<script src="https://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/vegas.min.js"></script>
<script src="https://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/jquery.fittext.js"></script>
<script src="https://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/jquery.textillate.js"></script>
<script src="https://www.enviousweb.com/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/jquery.lettering.js"></script>
<script>
	jQuery(document).ready(function($){
	 $('.tlt').textillate({
  selector: '.texts',
  loop: true,
  minDisplayTime: 2500,
  autoStart: true,
  outEffects: [ 'bounceOut' ],

  // in animation settings
  in: {
    // set the effect name
    effect: 'fadeIn',
    delayScale: 1.5,
    delay: 50,
    sync: false,
    shuffle: true
  },

  // out animation settings.
  out: {
    effect: 'bounceOut',
    delayScale: 1.5,
    delay: 150,
    sync: false,
    shuffle: true,
  }
});
	});
	</script>
<!-- Google Analytics Code -->
<!-- Additional CSS -->
<style></style>
</body></html>