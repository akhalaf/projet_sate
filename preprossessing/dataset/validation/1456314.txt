<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="https://tmaw.com/xmlrpc.php" rel="pingback"/>
<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>
<script>var et_site_url='https://tmaw.com';var et_post_id='0';function et_core_page_resource_fallback(a,b){"undefined"===typeof b&&(b=a.sheet.cssRules&&0===a.sheet.cssRules.length);b&&(a.onerror=null,a.onload=null,a.href?a.href=et_site_url+"/?et_core_page_resource="+a.id+et_post_id:a.src&&(a.src=et_site_url+"/?et_core_page_resource="+a.id+et_post_id))}
</script><title>404 Not Found | TMAW</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://tmaw.com/feed/" rel="alternate" title="TMAW » Feed" type="application/rss+xml"/>
<link href="https://tmaw.com/comments/feed/" rel="alternate" title="TMAW » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/tmaw.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<meta content="Divi v.4.5.3" name="generator"/><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://tmaw.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tmaw.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.7" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tmaw.com/wp-content/themes/Divi/style.css?ver=4.5.3" id="divi-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tmaw.com/wp-content/plugins/tablepress/css/default.min.css?ver=1.8" id="tablepress-default-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tmaw.com/wp-includes/css/dashicons.min.css?ver=5.6" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://tmaw.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://tmaw.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="es6-promise-js" src="https://tmaw.com/wp-content/themes/Divi/core/admin/js/es6-promise.auto.min.js?ver=5.6" type="text/javascript"></script>
<script id="et-core-api-spam-recaptcha-js-extra" type="text/javascript">
/* <![CDATA[ */
var et_core_api_spam_recaptcha = {"site_key":"","page_action":{"action":""}};
/* ]]> */
</script>
<script id="et-core-api-spam-recaptcha-js" src="https://tmaw.com/wp-content/themes/Divi/core/admin/js/recaptcha.js?ver=5.6" type="text/javascript"></script>
<link href="https://tmaw.com/wp-json/" rel="https://api.w.org/"/><link href="https://tmaw.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://tmaw.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/><link href="http://tmaw.mwrtba.org/wp-content/uploads/2016/04/tmaw-favicon.jpg" rel="shortcut icon"/><link href="https://tmaw.com/wp-content/et-cache/global/et-divi-customizer-global-160953204621.min.css" id="et-divi-customizer-global-cached-inline-styles" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" rel="stylesheet"/></head>
<body class="error404 do-etfw et_pb_button_helper_class et_fixed_nav et_show_nav et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_footer_columns3 et_pb_gutters1 et_right_sidebar et_divi_theme et-db et_minified_js et_minified_css">
<div id="page-container">
<header data-height-onload="100" id="main-header">
<div class="container clearfix et_menu_container">
<div class="logo_container">
<span class="logo_helper"></span>
<a href="https://tmaw.com/">
<img alt="TMAW" data-height-percentage="75" id="logo" src="http://tmaw.mwrtba.org/wp-content/uploads/2016/02/TMAW_Logo_website.jpg"/>
</a>
</div>
<div data-fixed-height="100" data-height="100" id="et-top-navigation">
<nav id="top-menu-nav">
<ul class="nav" id="top-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-435" id="menu-item-435"><a href="https://tmaw.com/issues/">Issues</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71" id="menu-item-71"><a href="https://tmaw.com/take-action/">Take Action</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-65" id="menu-item-65"><a href="https://tmaw.com/your-state/">Your State</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-218" id="menu-item-218"><a href="https://tmaw.com/research/">Research</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26" id="menu-item-26"><a href="https://tmaw.com/advertising/">Advertising</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-469" id="menu-item-469"><a href="https://tmaw.com/advocacy-center/">Advocacy Center</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-406" id="menu-item-406"><a href="https://tmaw.com/coalitions/">Coalitions</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-409" id="menu-item-409"><a href="https://tmaw.com/tmaw-supporters/">Supporters</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-76" id="menu-item-76"><a href="https://tmaw.com/contact/">Contact</a></li>
</ul> </nav>
<div id="et_top_search">
<span id="et_search_icon"></span>
</div>
<div id="et_mobile_nav_menu">
<div class="mobile_nav closed">
<span class="select_page">Select Page</span>
<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
</div>
</div> </div> <!-- #et-top-navigation -->
</div> <!-- .container -->
<div class="et_search_outer">
<div class="container et_search_form_container">
<form action="https://tmaw.com/" class="et-search-form" method="get" role="search">
<input class="et-search-field" name="s" placeholder="Search …" title="Search for:" type="search" value=""/> </form>
<span class="et_close_search_field"></span>
</div>
</div>
</header> <!-- #main-header -->
<div id="et-main-area">
<div id="main-content">
<div class="container">
<div class="clearfix" id="content-area">
<div id="left-area">
<article class="et_pb_post not_found" id="post-0">
<div class="entry">
<!--If no results are found-->
<h1 class="not-found-title">No Results Found</h1>
<p>The page you requested could not be found. Try refining your search, or use the navigation above to locate the post.</p>
</div>
<!--End if no results are found--> </article> <!-- .et_pb_post -->
</div> <!-- #left-area -->
<div id="sidebar">
<div class="et_pb_widget widget_search" id="search-2"><form action="https://tmaw.com/" class="searchform" id="searchform" method="get" role="search">
<div>
<label class="screen-reader-text" for="s">Search for:</label>
<input id="s" name="s" type="text" value=""/>
<input id="searchsubmit" type="submit" value="Search"/>
</div>
</form></div> <!-- end .et_pb_widget -->
<div class="et_pb_widget widget_recent_entries" id="recent-posts-2">
<h4 class="widgettitle">Recent Posts</h4>
<ul>
<li>
<a href="https://tmaw.com/2017/05/28/the-number-one-place-to-look/">The number one place to look</a>
</li>
<li>
<a href="https://tmaw.com/2016/02/13/transportation-and-locks/">Transportation and Locks</a>
</li>
<li>
<a href="https://tmaw.com/2016/02/08/hello-world/">Hello world!</a>
</li>
<li>
<a href="https://tmaw.com/2015/12/12/location-location-location/">Location, Location, Location</a>
</li>
<li>
<a href="https://tmaw.com/2015/07/04/sequestration/">sequestration</a>
</li>
</ul>
</div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_recent_comments" id="recent-comments-2"><h4 class="widgettitle">Recent Comments</h4><ul id="recentcomments"></ul></div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_archive" id="archives-2"><h4 class="widgettitle">Archives</h4>
<ul>
<li><a href="https://tmaw.com/2017/05/">May 2017</a></li>
<li><a href="https://tmaw.com/2016/02/">February 2016</a></li>
<li><a href="https://tmaw.com/2015/12/">December 2015</a></li>
<li><a href="https://tmaw.com/2015/07/">July 2015</a></li>
<li><a href="https://tmaw.com/2015/06/">June 2015</a></li>
<li><a href="https://tmaw.com/2015/03/">March 2015</a></li>
<li><a href="https://tmaw.com/2015/01/">January 2015</a></li>
<li><a href="https://tmaw.com/2014/08/">August 2014</a></li>
<li><a href="https://tmaw.com/2014/07/">July 2014</a></li>
<li><a href="https://tmaw.com/2014/06/">June 2014</a></li>
<li><a href="https://tmaw.com/2014/04/">April 2014</a></li>
<li><a href="https://tmaw.com/2014/01/">January 2014</a></li>
<li><a href="https://tmaw.com/2013/07/">July 2013</a></li>
<li><a href="https://tmaw.com/2013/06/">June 2013</a></li>
<li><a href="https://tmaw.com/2012/07/">July 2012</a></li>
<li><a href="https://tmaw.com/2012/06/">June 2012</a></li>
<li><a href="https://tmaw.com/2011/07/">July 2011</a></li>
<li><a href="https://tmaw.com/2011/06/">June 2011</a></li>
<li><a href="https://tmaw.com/2011/05/">May 2011</a></li>
<li><a href="https://tmaw.com/2011/04/">April 2011</a></li>
<li><a href="https://tmaw.com/2010/08/">August 2010</a></li>
<li><a href="https://tmaw.com/2010/07/">July 2010</a></li>
<li><a href="https://tmaw.com/2010/06/">June 2010</a></li>
</ul>
</div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_categories" id="categories-2"><h4 class="widgettitle">Categories</h4>
<ul>
<li class="cat-item cat-item-1"><a href="https://tmaw.com/category/uncategorized/">Uncategorized</a>
</li>
</ul>
</div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_meta" id="meta-2"><h4 class="widgettitle">Meta</h4>
<ul>
<li><a href="https://tmaw.com/wp-login.php">Log in</a></li>
<li><a href="https://tmaw.com/feed/">Entries feed</a></li>
<li><a href="https://tmaw.com/comments/feed/">Comments feed</a></li>
<li><a href="https://wordpress.org/">WordPress.org</a></li>
</ul>
</div> <!-- end .et_pb_widget --> </div> <!-- end #sidebar -->
</div> <!-- #content-area -->
</div> <!-- .container -->
</div> <!-- #main-content -->
<footer id="main-footer">
<div class="container">
<div class="clearfix" id="footer-widgets">
<div class="footer-widget"></div> <!-- end .footer-widget --><div class="footer-widget"><div class="fwidget et_pb_widget widget_text" id="text-2"><h4 class="title">About Us</h4> <div class="textwidget">The American Road &amp; Transportation Builders Association’s (ARTBA) award-winning “Transportation Makes America Work” (TMAW) program is the industry’s comprehensive advocacy communications initiative. TMAW is aimed at building public and political support for increased surface transportation capital investments to meet public and business demand for safe and efficient mobility. - See more at: http://www.tmaw.com/#sthash.AssGRJ32.dpuf</div>
</div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div class="fwidget et_pb_widget widget_text" id="text-3"><h4 class="title">Contact Us</h4> <div class="textwidget">All Contents © 2019<br/>
The American Road &amp; Transportation Builders Association<br/>
250 E Street, N.W.<br/>
Suite 900
Washington, D.C. 20024<br/>
202.289.4434<br/>
<a href="http://www.artba.org/privacy-policy/" rel="noopener" target="_blank">Privacy &amp; Cookies Policy</a></div>
</div> <!-- end .fwidget --></div> <!-- end .footer-widget --> </div> <!-- #footer-widgets -->
</div> <!-- .container -->
<div id="footer-bottom">
<div class="container clearfix">
<ul class="et-social-icons">
</ul><p id="footer-info">Designed by <a href="http://www.elegantthemes.com" title="Premium WordPress Themes">Elegant Themes</a> | Powered by <a href="http://www.wordpress.org">WordPress</a></p> </div> <!-- .container -->
</div>
</footer> <!-- #main-footer -->
</div> <!-- #et-main-area -->
</div> <!-- #page-container -->
<script>
   jQuery(document).ready(function ($) 
   {
   var currentYear = (new Date()).getFullYear();
   $('.et_pb_text').children('.textwidget').first().text("All Contents © " + currentYear);
   });
</script><link href="https://fonts.googleapis.com/css?family=Nunito:200,200italic,300,300italic,regular,italic,600,600italic,700,700italic,800,800italic,900,900italic|Lato:100,100italic,300,300italic,regular,italic,700,700italic,900,900italic&amp;subset=latin-ext,vietnamese,cyrillic,latin,cyrillic-ext&amp;display=swap" id="et-builder-googlefonts-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-form-js" src="https://tmaw.com/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20" type="text/javascript"></script>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var _wpcf7 = {"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://tmaw.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.7" type="text/javascript"></script>
<script id="divi-custom-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var DIVI = {"item_count":"%d Item","items_count":"%d Items"};
var et_shortcodes_strings = {"previous":"Previous","next":"Next"};
var et_pb_custom = {"ajaxurl":"https:\/\/tmaw.com\/wp-admin\/admin-ajax.php","images_uri":"https:\/\/tmaw.com\/wp-content\/themes\/Divi\/images","builder_images_uri":"https:\/\/tmaw.com\/wp-content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"1d80b3d6a1","subscription_failed":"Please, check the fields below to make sure you entered the correct information.","et_ab_log_nonce":"038eee41f6","fill_message":"Please, fill in the following fields:","contact_error_message":"Please, fix the following errors:","invalid":"Invalid email","captcha":"Captcha","prev":"Prev","previous":"Previous","next":"Next","wrong_captcha":"You entered the wrong number in captcha.","wrong_checkbox":"Checkbox","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","ab_tests":[],"is_ab_testing_active":"","page_id":"","unique_test_id":"","ab_bounce_rate":"","is_cache_plugin_active":"no","is_shortcode_tracking":"","tinymce_uri":""}; var et_frontend_scripts = {"builderCssContainerPrefix":"#et-boc","builderCssLayoutPrefix":"#et-boc .et-l"};
var et_pb_box_shadow_elements = [];
var et_pb_motion_elements = {"desktop":[],"tablet":[],"phone":[]}; var su_magnific_popup = {"close":"Close (Esc)","loading":"Loading...","prev":"Previous (Left arrow key)","next":"Next (Right arrow key)","counter":"%curr% of %total%","error":"Failed to load this link. <a href=\"%url%\" target=\"_blank\"><u>Open link<\/u><\/a>."};
/* ]]> */
</script>
<script id="divi-custom-script-js" src="https://tmaw.com/wp-content/themes/Divi/js/custom.unified.js?ver=4.5.3" type="text/javascript"></script>
<script id="do-etfw-twitter-widgets-js" src="https://tmaw.com/wp-content/plugins/easy-twitter-feed-widget//js/twitter-widgets.js?ver=1.0" type="text/javascript"></script>
<script id="et-core-common-js" src="https://tmaw.com/wp-content/themes/Divi/core/admin/js/common.js?ver=4.5.3" type="text/javascript"></script>
<script id="wp-embed-js" src="https://tmaw.com/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
</body>
</html>
