<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-US" prefix="og: http://ogp.me/ns#">
<![endif]--><!--[if IE 8]>
<html class="ie ie8" lang="en-US" prefix="og: http://ogp.me/ns#">
<![endif]--><!--[if !(IE 7) | !(IE 8) ]><!--><html lang="en-US" prefix="og: http://ogp.me/ns#">
<!--<![endif]-->
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-125946075-5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125946075-5');
</script>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width" name="viewport"/>
<title>Page Not Found | Athlete Promotions</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.athletepromotions.com/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
	<script src="https://www.athletepromotions.com/wp-content/themes/celebrity-talent/js/html5.js"></script>
	<![endif]-->
<!-- This site is optimized with the Yoast WordPress SEO plugin v1.7.3.3 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="en_US" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Page Not Found | Athlete Promotions" property="og:title"/>
<meta content="Athlete Promotions" property="og:site_name"/>
<!-- / Yoast WordPress SEO plugin. -->
<link href="https://www.athletepromotions.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.1" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.athletepromotions.com/wp-content/plugins/related-youtube-videos/css/themes.css?ver=4.1.24" id="relatedyoutubevideos_frontendStyles-css" media="all" rel="stylesheet" type="text/css"/>
<meta content="WordPress 4.1.24" name="generator"/>
<!-- Call Now Button 0.1.2 by Jerry Rietveld (callnowbutton.com) -->
<style>#callnowbutton {display:none;} @media screen and (max-width:650px){#callnowbutton {display:block; width:100px;left:0;border-bottom-right-radius:40px; border-top-right-radius:40px; height:80px; position:fixed; bottom:-20px; border-top:2px solid #00d600; background:url(https://www.athletepromotions.com/wp-content/plugins/call-now-button/callbutton01.png) center 10px no-repeat #009900; text-decoration:none; -webkit-box-shadow:0 0 5px #888; z-index:9999;}}</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-3208484-9', 'auto');
  ga('send', 'pageview');

</script>
<link href="https://www.athletepromotions.com/wp-content/themes/celebrity-talent/css/style.css" rel="stylesheet" type="text/css"/>
<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>-->
<style type="text/css">
    body{padding-top:60px;}
  </style>
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
<!-- Le fav and touch icons -->
<link href="../assets/ico/favicon.ico" rel="shortcut icon"/>
<link href="../assets/ico/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144"/>
<link href="../assets/ico/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114"/>
<link href="../assets/ico/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72"/>
<link href="../assets/ico/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon-precomposed"/>
<script src="https://www.athletepromotions.com/wp-content/themes/celebrity-talent/js/jquery.min.js"></script>
<script type="text/javascript">
	if(window.outerWidth < 749) {
		var ele = document.createElement("meta");
		ele.name = "viewport";
		ele.content = "width=device-width, initial-scale=1.0";
		document.head.appendChild(ele);
	 }
	 

  /* Animinate header */
  var isShown = false;

  $(window).scroll(function(e){

    var top = 105;

    var scroll = $(this).scrollTop();

    if(!isShown && scroll > top){

      isShown = true;

      $('.floater').animate({

        top: '10px'

      }, 500, function() {

        

      });

    }

    if(isShown && scroll < top){

      isShown = false;

      $('.floater').animate({

        top: '-50px'

      }, 500, function() {

        

      });

    }    

  });
	  /* End animination header */
	 
 <!-- Booking Request submit --> 
/* $("document").ready(function() {
  
    $("#bookingsubbtn").click(function() {
        var fullname=$("#fullname").val();
        var frmdata=$("#BookingReqfrm").serializeArray();
         $.each( frmdata, function( i, frmdata1 ) {
            //alert( i+"##"+frmdata1.value);
            //alert($(frmdata1).attr('name'));
            if($(frmdata1).attr('name')=='fullname' && frmdata1.value=='') {
                 alert("Please Enter Your Full Name");
                $(frmdata1).attr('name').focus();  
                 return false;
            }
            if($(frmdata1).attr('name')=='company' && frmdata1.value=='') {
                 alert("Please Enter Organisation Name");
                 $(frmdata1).attr('name').focus();
                 return false;
            }
            if($(frmdata1).attr('name')=='phone' && frmdata1.value=='') {
                 alert("Please Enter Phone No.");
                 $(frmdata1).attr('name').focus();
                 return false;
            }
            if($(frmdata1).attr('name')=='phone' && frmdata1.value!='') {
				 var PhoneRegxp = /(^[0-9\s\-\(\)\+]{5,20})$/;
				 if (PhoneRegxp.test(frmdata1.value) != true)
				 {	
				 	alert("Enter Correct Phone number");
					$(frmdata1).attr('name').focus();
					 return false;
				}
			
			}
            if($(frmdata1).attr('name')=='email' && frmdata1.value=='') {
                 alert("Please Enter Email ID ");				 
                 $(frmdata1).attr('name').focus();
                 return false;
            }
			if($(frmdata1).attr('name')=='email' && frmdata1.value!='') {
					apos=frmdata1.value.indexOf("@");
					 dotpos=frmdata1.value.lastIndexOf(".");
					if (apos<1||dotpos-apos<2) 
					{
						 alert("Not a valid e-mail address!");
						$(frmdata1).attr('name').focus();
						 return false;
					}			
			}
			
            if($(frmdata1).attr('name')=='feeRange' && frmdata1.value=='0') {
                 alert("Please Select Fee Range ");
                 $(frmdata1).attr('name').focus();
                 return false;
            }
			
			
        });
		
		
		
		
        //alert("Hellooooo"+fullname+frmdata);
            	$.ajax({
        					type: "POST",
        					url: "https://www.athletepromotions.com/wp-content/themes/celebrity-talent/ajax_bookingreq.php",
        					//data: { BookingReq: "YES", catname : catname, catId: catId, limitstart : limitstart , search_char : search_char },
                  data: frmdata, 
        					success: function(response)
        					{	
        						if(response=='1') {
                      alert("Your Form has been submitted!!!");
                      $("#BookingReqfrm")[0].reset();
                    }else {
                      alert("Sorry an Error has Occured Please try again latter!!!");
                    }
        						//$("#BookingReqfrm")[0].reset();
        						
        					}
    				});
    });
 
 });*/
 <!-- end of booking request submit -->

 </script>
<link href="https://www.athletepromotions.com/wp-content/themes/celebrity-talent/css/mobilemenu.css" rel="stylesheet" type="text/css"/>
<script>
	$(document).ready(function(){
		$(".nav-button").click(function () {
		$(".nav-button,#menu-menu-1.menu").toggleClass("open");
		});    
	});
</script>
</head>
<body class="error404 custom-background group-blog masthead-fixed footer-widgets">
<div class="navbar navbar-fixed-top">
<div class="navbar-inner">
<div class="container">
<a class="brand" href="">Athlete Promotions</a>
<ul class="nav">
<li class="divider-vertical"></li>
<li><a href="http://www.celebritytalentpromotions.com/"><!--<img src="http://www.celebritytalentpromotions.com/wp-content/uploads/athlete-speakers-site.jpg">-->
<strong>
		View Our Celebrity  Speakers Site
	</strong>
</a></li>
<li class="divider-vertical"></li>
</ul>
<div class="floater">
<p class="contact">
<span>Call to book a Sports Celebrity</span>
<strong><a class="tel" href="tel:1.888.246.7141">1.888.246.7141</a></strong>
<a class="btn btn-danger btn-mini" href="/?page_id=434">Start Your Athlete Booking Request</a>
</p>
</div>
<form action="/site/search.php" class="navbar-search pull-right form-search" id="searchform" method="get">
<input class="autocomplete search-query span3" id="search" name="search" placeholder="Search by Name, Team or Sport" type="text"/>
<button class="btn" id="searchsubmit" type="submit"><i class="icon-search"></i></button>
</form>
</div>
</div>
</div>
<div class="container">
<!-- Header -->
<header class="jumbotron subhead" id="mainHeader">
<div class="logo"><a href="/">Celebrity Talent Promotions</a></div>
<p class="lead">A Celebrity Booking Agency</p>
<p class="contact">
          CALL TO BOOK A SPORTS CELEBRITY 
          <strong><a class="tel" href="tel:1.888.246.7141">1.888.246.7141</a></strong>
</p>
<p class="share">
<a class="btn btn-danger" href="/?page_id=434" style="margin: 0 15px 2px 0;">Start Your Athlete Booking Request</a>
<a href="http://www.Facebook.com/AthletePromotions" target="_Blank">
<img height="32" src="https://www.athletepromotions.com/wp-content/themes/celebrity-talent/images/CelebrityPromotions.png" width="32"/>
</a>
<a href="http://www.LinkedIn.com/in/RyanTotka" target="_Blank">
<img height="32" src="https://www.athletepromotions.com/wp-content/themes/celebrity-talent/images/RyanTotka.png" width="32"/>
</a>
<a href="https://twitter.com/CelebrityAgents" target="_Blank">
<img height="32" src="https://www.athletepromotions.com/wp-content/themes/celebrity-talent/images/CelebrityAgents.png" width="32"/>
</a>
<a href="http://feeds.feedburner.com/Athletepromotionscom" target="_Blank">
<img height="32" src="https://www.athletepromotions.com/wp-content/themes/celebrity-talent/images/rss.png" width="32"/>
</a>
</p>
</header>
<div class="page-content">
<div class="subnav subnav-fixed">
<button class="nav-button">Toggle Navigation</button>
<div class="menu-menu-1-container"><ul class="menu" id="menu-menu-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33" id="menu-item-33"><a href="https://www.athletepromotions.com/celebrity-marketing-agency.php">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35" id="menu-item-35"><a href="https://www.athletepromotions.com/recent-celebrity-athlete-bookings.php">Recent Bookings</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-42" id="menu-item-42"><a href="https://www.athletepromotions.com/corporate-athlete-appearances.php">Appearances</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51" id="menu-item-51"><a href="https://www.athletepromotions.com/speaker.php">Speaking Engagements</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52" id="menu-item-52"><a href="https://www.athletepromotions.com/celebrity-product-endorsements.php">Endorsements</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53" id="menu-item-53"><a href="https://www.athletepromotions.com/athlete-marketing.php">Athlete Marketing</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-56436" id="menu-item-56436"><a href="/blog/">BLOG</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-54" id="menu-item-54"><a href="https://www.athletepromotions.com/contact.php">Contact</a></li>
</ul></div> </div>
<div class="dynamic-content-area">
<div class="row">
<div class="span8">
<h2>404: The page you requested does not exist</h2>
<p>Tell us what you're looking for.</p>
<div class="row">
<div class="span" style="width: 97%;">
<form action="/site/search.php" class="well form-search" method="get">
<input aria-autocomplete="list" aria-haspopup="true" autocomplete="off" class="input-medium search-query autocomplete ui-autocomplete-input" name="search" placeholder="Search..." role="textbox" style="width: 100%;" type="text"/>
</form>
</div>
</div>
<h2>Contact Us</h2>
<div class="row">
<div class="span4">
<address>
<p>
<strong>Phone:</strong> 1.888.752.3532<br/>
<strong>Fax:</strong> 407.909.6202          
            </p>
</address>
</div>
<div class="span4" style="text-align:right;">
<p><strong>for the latest celebrity appearance news<br/>make sure to follow us on</strong></p>
<p class="share pull-right">
<a href="http://www.Facebook.com/AthletePromotions">
<img src="/wp-content/uploads/2014/07/AthletePromotions.png"/>
</a>
<a href="http://www.LinkedIn.com/in/RyanTotka">
<img src="/wp-content/uploads/2014/07/RyanTotka.png"/>
</a>
<a href="http://feeds.feedburner.com/Athletepromotionscom">
<img src="/wp-content/uploads/2014/07/rss.png"/>
</a>
<a href="mailto:bookings@celebritytalentpromotions.com">
<img alt="" src="/wp-content/themes/celebrity-talent/images/email.png"/>
</a>
</p>
</div>
</div>
</div>
<div class="span4 sidebar">
<!-- BOOKING REQUEST -->
<form action="/?page_id=434" class="form-horizontal booking-request v2" id="BookingReqfrm" method="post">
<fieldset>
<div class="control-group">
<label class="control-label" for="fullname"><strong>Your Name:</strong></label>
<div class="controls">
<input class="span2" id="fullname" name="fullname" style="height:16px;" type="text"/>
</div>
</div>
<div class="control-group">
<label class="control-label" for="company"><strong>Organization:</strong></label>
<div class="controls">
<input class="span2" id="company" name="company" style="height:16px;" type="text"/>
</div>
</div>
<div class="control-group">
<label class="control-label" for="phone"><strong>Phone:</strong></label>
<div class="controls">
<input class="span2" id="phone" name="phone" style="height:16px;" type="text"/>
</div>
</div>
<div class="control-group">
<label class="control-label" for="email"><strong>Email:</strong></label>
<div class="controls">
<input class="span2" id="email" name="email" style="height:16px; width:180px;" type="text"/>
</div>
</div>
<div class="control-group">
<label class="control-label" for="feeRange"><strong>Fee Range:</strong></label>
<div class="controls">
<select name="feeRange" style="width: 190px;">
<option value="0">Please Select</option>
<option value="0">Please Select</option>
<option value="$5,000-$10,000">$5,000-$10,000</option>
<option value="$10,000-$20,000">$10,000-$20,000</option>
<option value="$20,000-$30,000">$20,000-$30,000</option>
<option value="$30,000-$50,000">$30,000-$50,000</option>
<option value="$50,000-$100,000">$50,000-$100,000</option>
<option value="$100,000 and Up">$100,000 and Up</option>
</select>
</div>
</div>
<div class="control-group">
<label class="control-label" for="email"><strong>Captcha</strong></label>
<!--<div class="controls">
                <input name="captcha" id="captcha" maxlength="5" type="text" value="enter code" class="rightSubcribe2" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value==''){this.value=this.defaultValue;this.style.color='#BAB6B6'}" onkeypress="this.style.color='#373737'" style="width:95px; height:16px;"/>
          
           <img src="https://www.athletepromotions.com/captcha/register_php_captcha.php" style='padding-right:8px;padding-top:0px;'>
            <input type="hidden" name="hid_captcha" id="hid_captcha" value="__capthca_val__"/>
          </div>
        </div>  -->
<input name="quick" type="hidden" value="1"/>
<div class="form-actions">
<!-- <button type="submit" class="btn btn-booking">SUBMIT BOOKING NOW</button>-->
<button class="btn btn-booking" id="bookingsubbtn" type="button">SUBMIT BOOKING NOW</button>
</div>
<input name="referer" type="hidden" value="http://www.celebritytalentpromotions.com/"/>
<!-- talent news -->
<h3 class="tight">Latest Celebrity Appearance News</h3>
<div class="well small-blog">
</div></div></fieldset></form></div></div></div></div></div></body></html>