<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<meta content="D9VfaG5tvrVJd9QOZEJQnAwC-SIDbLO7fdXqaOUSTM0" name="google-site-verification"/>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-102481040-1', 'auto');
  ga('send', 'pageview');

</script>
<title>Ayurvedic Medicine,Ayurvedic treatment,Clinic,Herbal products</title>
<link href="assets/img/favicon.png" rel="shortcut icon"/>
<meta content="Ayurved Solution is a group of Ayurvedic Medicine &amp; wide variety of Herbal products for all types of Health Problem." name="description"/>
<meta content="Weight loss,Motapa Kam Karna,Ayurvedic Fat Burner,Fat Cutter,Fast Weight loss,Fat Burna By Ayurveda,Natural Weight Loss Pills,Slimming Pills, Best Weight Loss Product, Ayurvedic Weight Loss Pills, Natural Products for Weight Loss, Ayurvedic Medicine for Weight Loss, Weight Loss Medicines" name="keywords"/>
<!-- CSS -->
<link href="assets/css/preload.css" rel="stylesheet"/>
<!-- Compiled in vendors.js -->
<!--
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/bootstrap-switch.min.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">
    <link href="assets/css/slidebars.min.css" rel="stylesheet">
    <link href="assets/css/lightbox.css" rel="stylesheet">
    <link href="assets/css/jquery.bxslider.css" rel="stylesheet" />
    <link href="assets/css/buttons.css" rel="stylesheet">
    -->
<link href="assets/css/vendors.css" rel="stylesheet"/>
<link href="assets/css/syntaxhighlighter/shCore.css" rel="stylesheet"/>
<link href="assets/css/style-blue.css" rel="stylesheet" title="default"/>
<link href="assets/css/width-full.css" rel="stylesheet" title="default"/>
<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->
<link href="blink.css" rel="stylesheet"/>
</head>
<body>
<div class="hidden-xs" id="theme-options">
<div id="icon-options">
<i class="fa fa-gears fa-2x fa-flip-horizontal"></i>
</div>
</div>
<div id="sb-site">
<div class="boxed">
<header class="hidden-xs header-full" id="header-full-top">
<div class="container">
<div class="header-full-title">
<h1 class="animated fadeInRight"><a href="index.html"><span>Ayurved  Solution</span></a></h1>
<p class="animated fadeInRight">An ayurveda consultancy</p>
</div>
<nav class="top-nav">
<ul class="top-nav-social hidden-sm">
<li><a class="animated fadeIn animation-delay-7 twitter" href="#"><i class="fa fa-twitter"></i></a></li>
<li><a class="animated fadeIn animation-delay-8 facebook" href="#"><i class="fa fa-facebook"></i></a></li>
<li><a class="animated fadeIn animation-delay-9 google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
<li><a class="animated fadeIn animation-delay-7 linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
</ul>
<!-- dropdown -->
<!-- dropdown -->
</nav>
</div> <!-- container -->
</header> <!-- header-full -->
<nav class="navbar navbar-default navbar-header-full navbar-dark yamm navbar-static-top" id="header" role="navigation">
<div class="container">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<button class="navbar-toggle" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<i class="fa fa-bars"></i>
</button>
<a class="navbar-brand hidden-lg hidden-md hidden-sm" href="index.html" id="ar-brand"><span>Ayurved  Solution</span></a>
</div> <!-- navbar-header -->
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav">
<li><a href="index.html">Home</a></li>
<li><a href="why-ayurveda.html">Why Ayurveda ?</a></li>
<li class="dropdown">
<a class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="javascript:void(0);">Products</a>
<ul class="dropdown-menu dropdown-menu-left animated-2x animated fadeIn">
<!--								<li><a href="height-gainer.html">Height Gainer</a></li>
                                <li><a href="diabetes.html">Diabetes</a></li>
                                <li><a href="hair-loss.html">Hair Loss</a></li>
                                <li><a href="acne.html">Acne</a></li>
--> <li><a href="weight-loss.html">Weight Loss</a></li>
<li><a href="male-sex-problems.html">Male Sex Problems</a></li>
</ul>
</li>
<li class="blink"><a href="http://gethuge-stamina.com/" style="color:#FF0; font-weight:bold;" target="_blank">More about Men's Sexual Problem</a></li>
<li><a href="online-consultancy.html">Online Consultancy</a></li>
<li><a href="contact-us.html">Contact Us</a></li>
</ul>
</div><!-- navbar-collapse -->
</div><!-- container -->
</nav>
<section class="carousel-section">
<div class="carousel carousel-razon slide" data-interval="5000" data-ride="carousel" id="carousel-example-generic">
<!-- Indicators -->
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#carousel-example-generic"></li>
<li data-slide-to="1" data-target="#carousel-example-generic"></li>
<li data-slide-to="2" data-target="#carousel-example-generic"></li>
</ol>
<!-- Wrapper for slides -->
<div class="carousel-inner">
<div class="item active">
<div class="container">
<div class="row">
<div class="col-md-6 col-sm-7">
<div class="carousel-caption">
<div class="carousel-text">
<h1 class="animated fadeInDownBig animation-delay-7 carousel-title">Benefits of Ayurvedic Medicine</h1>
<h2 class="animated fadeInDownBig animation-delay-5 crousel-subtitle">Ayurveda is a 5,000-year-old system of natural healing thatâs truly stood the test of time. </h2>
<ul class="list-unstyled carousel-list">
<li class="animated bounceInLeft animation-delay-11"><i class="fa fa-check"></i>Helps Lower Stress and Anxiety</li>
<li class="animated bounceInLeft animation-delay-13"><i class="fa fa-check"></i>Lowers Blood Pressure and Cholesterol</li>
<li class="animated bounceInLeft animation-delay-15"><i class="fa fa-check"></i>Helps with Recovery from Injuries and Illnesses</li>
</ul>
<p class="animated fadeInUpBig animation-delay-17">The oldest healing science there is â which is amazing considering the fact that Ayurveda is still practiced effectively today.</p>
</div>
</div>
</div>
<div class="col-md-6 col-sm-5 hidden-xs carousel-img-wrap">
<div class="carousel-img">
<img alt="Weight loss by Ayurvedic medicine" class="img-responsive animated bounceInUp animation-delay-3" src="assets/img/demo/pre.jpg"/>
</div>
</div>
</div>
</div>
</div>
<div class="item">
<div class="container">
<div class="row">
<div class="col-md-6 col-sm-8">
<div class="carousel-caption">
<div class="carousel-text">
<h1 class="animated fadeInDownBig animation-delay-7 carousel-title">Great health involves simple steps</h1>
<h2 class="animated fadeInDownBig animation-delay-5 crousel-subtitle">The rhythm of the body, the melody of the mind &amp; the harmony of the soul create the symphony of life..</h2>
<ul class="list-unstyled carousel-list">
<li class="animated bounceInLeft animation-delay-11"><i class="fa fa-check"></i>Building Immunity</li>
<li class="animated bounceInLeft animation-delay-13"><i class="fa fa-check"></i>Getting Better</li>
<li class="animated bounceInLeft animation-delay-15"><i class="fa fa-check"></i>Reducing Stress</li>
</ul>
<p class="animated fadeInUpBig animation-delay-17">The great thing about Ayurveda is that its treatments always yield side benefits, not side effects.</p>
</div>
</div>
</div>
<div class="col-md-6 col-sm-4 hidden-xs carousel-img-wrap">
<div class="carousel-img">
<img alt="Motapa kam karna" class="img-responsive animated bounceInUp animation-delay-3" src="assets/img/demo/pre2.jpg"/>
</div>
</div>
</div>
</div>
</div>
<div class="item">
<div class="container">
<div class="row">
<div class="col-lg-6 col-md-7 col-sm-9">
<div class="carousel-caption">
<div class="carousel-text">
<h1 class="animated fadeInDownBig animation-delay-7 carousel-title">Anything can be used as medicine.</h1>
<h2 class="animated fadeInDownBig animation-delay-5 crousel-subtitle">Nature has already given us proper instincts for recovering and maintaining balance.</h2>
<ul class="list-unstyled carousel-list">
<li class="animated bounceInLeft animation-delay-11"><i class="fa fa-check"></i>Increases stamina</li>
<li class="animated bounceInLeft animation-delay-13"><i class="fa fa-check"></i>Enhances vision</li>
<li class="animated bounceInLeft animation-delay-15"><i class="fa fa-check"></i>Benefits sleepâbetter, deeper sleep</li>
</ul>
<p class="animated fadeInUpBig animation-delay-17">You are a knower, your body is the object you form with your knowledge, and the millions of cellular functions taking place inside you are the process of knowing.</p>
</div>
</div>
</div>
<div class="col-lg-6 col-md-5 col-sm-3 hidden-xs carousel-img-wrap">
<div class="carousel-img">
<img alt="Fat burner" class="img-responsive animated bounceInUp animation-delay-3" src="assets/img/demo/pre3.jpg"/>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Controls -->
<a class="left carousel-control" data-slide="prev" href="#carousel-example-generic">
<span class="glyphicon glyphicon-chevron-left"></span>
</a>
<a class="right carousel-control" data-slide="next" href="#carousel-example-generic">
<span class="glyphicon glyphicon-chevron-right"></span>
</a>
</div>
</section> <!-- carousel -->
<section class="margin-bottom">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-6">
<div class="content-box box-default animated fadeInUp animation-delay-10">
<span class="icon-ar icon-ar-lg icon-ar-round icon-ar-inverse"><i class="fa fa-leaf"></i></span>
<h4 class="content-box-title">Sexual Stamina Enhancer</h4>
<p align="justify">An atyurvedic product, with no side-effect, gives you amazing sex power. This product for men only.</p>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="content-box box-default animated fadeInUp animation-delay-14">
<span class="icon-ar icon-ar-lg icon-ar-round icon-ar-inverse"><i class="fa fa-leaf"></i></span>
<h4 class="content-box-title">Height Gainer Medicine</h4>
<p align="justify">An atyurvedic product, with no side-effect, gives you a tall, attractive and perfect body.</p>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="content-box box-default animated fadeInUp animation-delay-16">
<span class="icon-ar icon-ar-lg icon-ar-round icon-ar-inverse"><i class="fa fa-leaf"></i></span>
<h4 class="content-box-title">Diabetes Medicine</h4>
<p align="justify">Ayurveda medicine that helps to cure from the diabetes with zero percent side effect.
</p>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="content-box box-default animated fadeInUp animation-delay-12">
<span class="icon-ar icon-ar-lg icon-ar-round icon-ar-inverse"><i class="fa fa-leaf"></i></span>
<h4 class="content-box-title">Weight Loss Medicine</h4>
<p align="justify">An atyurvedic product, with no side-effect, gives you a slim, shaped and strong body.</p>
</div>
</div>
</div>
</div>
</section>
<div class="container">
<section class="margin-bottom">
<p class="lead lead-lg text-center primary-color margin-bottom">When diet is wrong, medicine is of no use. When diet is correct, medicine is of no need. <strong>~Ayurveda</strong></p>
<div class="row">
<div class="col-md-6" style="width:100%;">
<h2 class="no-margin-top">About us</h2>
<p align="justify">
                
                We are one of the providers of  - Height Gainer, Diabetes, Hair Loss, Acne, Weight Loss, Male Sex Problems relaed medicines. We are not providing just medicines but serving a 90 years old experience.</p>
<p align="justify">We are Ayurveda  Centre. We are mainly engaged in Ayurveda traditional  treatment - treatments for all sorts of health related problems at our centre. We have - an experienced panel of doctors for consultation, the best quality medicines for treatments, highly qualified and trained staff for Ayurveda treatments. Our clinic has a calm and quiet and hygienic atmosphere.</p>
</div>
</div>
</section>
<section class="margin-bottom">
<h2 class="section-title">Photo Gallery</h2>
<div class="bxslider-controls">
<span id="bx-prev4"></span>
<span id="bx-next4"></span>
</div>
<ul class="bxslider" id="latest-works">
<li>
<div class="img-caption-ar">
<img alt="Natural fat burner" class="img-responsive" src="assets/img/demo/w1.jpg"/>
</div>
</li>
<li>
<div class="img-caption-ar">
<img alt="Lose belly fat" class="img-responsive" src="assets/img/demo/w2.jpg"/>
</div>
</li>
<li>
<div class="img-caption-ar">
<img alt="Weight kam karna" class="img-responsive" src="assets/img/demo/w3.jpg"/>
</div>
</li>
<li>
<div class="img-caption-ar">
<img alt="Tips for weight loss" class="img-responsive" src="assets/img/demo/w4.jpg"/>
</div>
</li>
<li>
<div class="img-caption-ar">
<img alt="Weight loss pills" class="img-responsive" src="assets/img/demo/w5.jpg"/>
</div>
</li>
<li>
<div class="img-caption-ar">
<img alt="Weight loss program" class="img-responsive" src="assets/img/demo/w6.jpg"/>
</div>
</li>
<li>
<div class="img-caption-ar">
<img alt="Weight loss supplements" class="img-responsive" src="assets/img/demo/w7.jpg"/>
</div>
</li>
<li>
<div class="img-caption-ar">
<img alt="Fast weight loss" class="img-responsive" src="assets/img/demo/w8.jpg"/>
</div>
</li>
</ul>
</section>
</div>
<aside id="footer-widgets">
<div class="container">
<div class="row">
<div class="col-md-4">
<h3 class="footer-widget-title">Quick Links</h3>
<ul class="list-unstyled">
<li><a href="index.html">Home</a></li>
<li><a href="why-ayurveda.html">Why Ayurveda ?</a></li>
<li><a href="online-consultancy.html">Online Consultancy</a></li>
<li><a href="weight-loss.html">Weight Loss</a></li>
<li><a href="male-sex-problems.html">Male Sex Problems</a></li>
<li><a href="fat-burner-ayurved.html">Product Showcase</a></li>
<li><a href="contact-us.html">Contact Us</a></li>
</ul>
</div>
<div class="col-md-4">
<div class="footer-widget">
<h3 class="footer-widget-title">Featured Products</h3>
<div class="media">
<a class="pull-left" href="weight-loss.html"><img alt="Weight loss plans" class="media-object" height="75" src="prod-1.jpg" width="75"/></a>
<div class="media-body">
<h4 class="media-heading"><a href="weight-loss.html">Garcinia Cambogia</a></h4>
</div>
</div>
<div class="media">
<a class="pull-left" href="male-sex-problems.html"><img alt="Best way to weight loss" class="media-object" height="75" src="prod-2.jpg" width="75"/></a>
<div class="media-body">
<h4 class="media-heading"><a href="male-sex-problems.html">Enlargement Oil, Vigour Plus, Shilajit Power Capsule</a></h4>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="footer-widget">
<h3 class="footer-widget-title">Request a call back</h3>
<div class="row">
<form class="shake" data-toggle="validator" id="contactForm" role="form">
<div class="row">
<div class="form-group col-sm-6">
<input class="form-control" data-error="Enter Full Name" id="name" placeholder="Enter Full Name" required="" type="text"/>
<div class="help-block with-errors"></div>
</div>
<div class="form-group col-sm-6">
<input class="form-control" data-error="Enter Your Phone" id="phone" placeholder="Enter Phone No." required="" type="text"/>
<div class="help-block with-errors"></div>
</div>
</div>
<div class="row">
<div class="form-group col-sm-6">
<input class="form-control" data-error="Enter Your Email" id="email" placeholder="Enter Email" required="" type="text"/>
<div class="help-block with-errors"></div>
</div>
<div class="form-group col-sm-6">
<input class="form-control" data-error="Enter Your City" id="city" placeholder="Enter City" required="" type="text"/>
<div class="help-block with-errors"></div>
</div>
</div>
<div class="form-group">
<textarea class="form-control" data-error="Enter Your Message" id="message" placeholder="Enter Your Message" required="" rows="2"></textarea>
<div class="help-block with-errors"></div>
</div>
<button class="btn btn-success btn-lg pull-right " id="form-submit" type="submit">Submit</button>
<div class="h3 text-center hidden" id="msgSubmit"></div>
<div class="clearfix"></div>
</form>
</div>
</div>
</div>
</div> <!-- row -->
</div> <!-- container -->
</aside> <!-- footer-widgets -->
<footer id="footer">
<p>©  Ayurved Solution, All rights reserved.</p>
</footer>
</div> <!-- boxed -->
</div> <!-- sb-site -->
<!-- Scripts -->
<!-- Compiled in vendors.js -->
<!--
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.cookie.js"></script>
<script src="assets/js/imagesloaded.pkgd.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/bootstrap-switch.min.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/slidebars.min.js"></script>
<script src="assets/js/jquery.bxslider.min.js"></script>
<script src="assets/js/holder.js"></script>
<script src="assets/js/buttons.js"></script>
<script src="assets/js/jquery.mixitup.min.js"></script>
<script src="assets/js/circles.min.js"></script>
<script src="assets/js/masonry.pkgd.min.js"></script>
<script src="assets/js/jquery.matchHeight-min.js"></script>
-->
<script src="assets/js/vendors.js"></script>
<script src="assets/js/styleswitcher.js"></script>
<!-- Syntaxhighlighter -->
<script src="assets/js/syntaxhighlighter/shCore.js"></script>
<script src="assets/js/syntaxhighlighter/shBrushXml.js"></script>
<script src="assets/js/syntaxhighlighter/shBrushJScript.js"></script>
<script src="assets/js/DropdownHover.js"></script>
<script src="assets/js/app.js"></script>
<script src="assets/js/holder.js"></script>
<script src="assets/js/index.js"></script>
<!-- External Contact From -->
<!--<script  type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
--><script src="js/validator.min.js" type="text/javascript"></script>
<script src="js/form-scripts.js" type="text/javascript"></script>
</body>
</html>
