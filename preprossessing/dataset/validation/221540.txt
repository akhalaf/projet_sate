<!DOCTYPE html>
<!--[if IEMobile 7]>
  <html class="iem7"  lang="fr" dir="ltr"><![endif]--><!--[if lte IE 6]>
  <html class="lt-ie9 lt-ie8 lt-ie7"  lang="fr" dir="ltr"><![endif]--><!--[if (IE 7)&(!IEMobile)]>
  <html class="lt-ie9 lt-ie8"  lang="fr" dir="ltr"><![endif]--><!--[if IE 8]>
  <html class="lt-ie9"  lang="fr" dir="ltr"><![endif]--><!--[if (gte IE 9)|(gt IEMobile 7)]><!--><!-- test de merge --><html dir="ltr" lang="fr" prefix="og: http://ogp.me/ns#"><!--<![endif]-->
<head>
<meta charset="utf-8"/>
<link href="https://bio-c-bon.eu/sites/default/files/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<meta content="Fini le bio triste, fade et cher ! Bio c’ Bon a pour ambition de rendre le bio accessible à tous, par sa proximité, ses prix et son conseil personnalisé en magasin." name="description"/>
<link href="https://bio-c-bon.eu/fr" rel="canonical"/>
<link href="https://bio-c-bon.eu/fr" rel="shortlink"/>
<meta content="website" property="og:type"/>
<meta content="https://bio-c-bon.eu/fr" property="og:url"/>
<meta content="Bio c’ Bon votre supermarché pour un quotidien bio enfin accessible" property="og:title"/>
<meta content="http://www.bio-c-bon.fr/sites/all/themes/biocbon/visuel_facebook.jpg" property="og:image"/>
<noscript><img height="1" src="https://www.facebook.com/tr?id=2352101481781607&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript> <title>Bio c’ Bon votre supermarché pour un quotidien bio enfin accessible |</title>
<meta content="width" name="MobileOptimized"/>
<meta content="true" name="HandheldFriendly"/>
<meta content="width=device-width" id="viewport" name="viewport"/>
<meta content="on" http-equiv="cleartype"/>
<script type="text/javascript">
      (function (d, t) {
        var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
        bh.type = 'text/javascript';
        bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=vxqail7b1bmrnlhii15tea';
        s.parentNode.insertBefore(bh, s);
      })(document, 'script');
    </script>
<link href="https://bio-c-bon.eu/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bio-c-bon.eu/sites/default/files/css/css_SiHdHsYahENEDUndz9prz2HR7NlSsJneghadfllScxY.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bio-c-bon.eu/sites/default/files/css/css_Qjtpv9SoMrtcDYPLs2CvgJoyKGZ2yOT69qjIuoPqJ8U.css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" media="all" rel="stylesheet" type="text/css"/>
<style>#sliding-popup.sliding-popup-top,#sliding-popup.sliding-popup-top .eu-cookie-withdraw-banner,.eu-cookie-withdraw-tab{background:#8cc63e;}#sliding-popup.sliding-popup-top.eu-cookie-withdraw-wrapper{background:transparent}#sliding-popup .popup-content #popup-text h1,#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text h3,#sliding-popup .popup-content #popup-text p,#sliding-popup label,#sliding-popup div,.eu-cookie-compliance-secondary-button,.eu-cookie-withdraw-tab{color:#096029 !important;}.eu-cookie-withdraw-tab{border-color:#096029;}.eu-cookie-compliance-more-button{color:#096029 !important;}
</style>
<link href="https://bio-c-bon.eu/sites/default/files/css/css_QXFVOq0KVY9pHmBeUVUhDKP33aXuSlVOlCp5vyOl0NM.css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://bio-c-bon.eu/sites/default/files/js/js_EebRuRXFlkaf356V0T2K_8cnUVfCKesNTxdvvPSEhCM.js"></script>
<script src="https://bio-c-bon.eu/sites/default/files/js/js_H7q2xORKmR9AN8Qx5spKEIBp7R_wG2apAswJoCUZY7I.js"></script>
<script src="https://bio-c-bon.eu/sites/default/files/js/js_2YRc7q2JQ5pVtVM4GZJM9ySdalC7YU3ASNvQhJ_w1Xo.js"></script>
<script src="https://bio-c-bon.eu/sites/default/files/js/js_Nw4yYTUlg-SrF7skVoTVJZpqNBO9G3sBDVoHLaiVMP0.js"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-11769967-1", {"cookieDomain":"auto","allowLinker":true});ga("require", "linker");ga("linker:autoLink", ["bio-c-bon.eu","bio-c-bon.fr","bio-c-bon.es","bio-c-bon.pt","bio-c-bon.it","bio-c-bon.ch","bio-c-bon.be"]);ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<script>jQuery.extend(Drupal.settings, { "pathToTheme": "/sites/all/themes/biocbon" });</script>
<script src="https://bio-c-bon.eu/sites/default/files/js/js_tH2I-8bTiEJG_cOgu-rK-rqPfQIt_R7jIJ6PigRJdDo.js"></script>
<script src="//maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyBhpTRiEl4kLca_75TRFCc_Dopzj5iMCY8"></script>
<script src="https://bio-c-bon.eu/sites/default/files/js/js_tvrnt04jq5sva3N06I1DI1nSdYYSTrWpdMbCHxIlZbA.js"></script>
<script src="https://bio-c-bon.eu/sites/default/files/js/js_7NuqTdkkyR1WVhOVJLIZxhEjrBJNcB-56Jt9aCS9ESY.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"fr\/","ajaxPageState":{"theme":"biocbon","theme_token":"HUQBRxtg2tuyXE5Kn-wc98w7Jr9RIGtqhmA6G6W6aEA","js":{"0":1,"sites\/all\/modules\/contrib\/eu_cookie_compliance\/js\/eu_cookie_compliance.js":1,"sites\/all\/modules\/contrib\/browserclass\/js\/browserclass.js":1,"sites\/all\/themes\/biocbon\/js\/jquery.autosize.min.js":1,"sites\/all\/themes\/biocbon\/js\/tool.js":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/ui\/external\/jquery.cookie.js":1,"sites\/all\/modules\/custom\/recettes_et_conseils\/js\/recettes-et-conseils.js":1,"public:\/\/languages\/fr_BIuU-T3Bnzaxx7JFzfKStp7BhOXmw1R9eUmlnOT5tT0.js":1,"sites\/all\/modules\/custom\/bio_slides\/js\/slideHome.js":1,"sites\/all\/modules\/custom\/bio_slides\/js\/owl.carousel2.min.js":1,"sites\/all\/modules\/custom\/bio_block\/js\/language_switch.js":1,"sites\/all\/modules\/contrib\/google_analytics\/googleanalytics.js":1,"1":1,"2":1,"sites\/all\/themes\/biocbon\/js\/frontpage.js":1,"\/\/maps.googleapis.com\/maps\/api\/js?libraries=places\u0026key=AIzaSyBhpTRiEl4kLca_75TRFCc_Dopzj5iMCY8":1,"sites\/all\/themes\/biocbon\/js\/mobile.js":1,"sites\/all\/themes\/biocbon\/js\/send-analytics.js":1,"public:\/\/facebook_tracking_pixel\/fb_tkpx.2352101481781607.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/contrib\/simplenews\/simplenews.css":1,"sites\/all\/modules\/custom\/bio_user\/css\/bio_user.css":1,"sites\/all\/modules\/contrib\/date\/date_api\/date.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/contrib\/menu_language_filter\/menu_language_filter.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/custom\/recettes_et_conseils\/css\/recettes-et-conseils.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/modules\/contrib\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/contrib\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/custom\/bio_slides\/css\/owl.carousel2.css":1,"sites\/all\/modules\/contrib\/eu_cookie_compliance\/css\/eu_cookie_compliance.css":1,"\/\/fonts.googleapis.com\/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800":1,"0":1,"sites\/all\/themes\/biocbon\/system.menus.css":1,"sites\/all\/themes\/biocbon\/system.messages.css":1,"sites\/all\/themes\/biocbon\/system.theme.css":1,"sites\/all\/themes\/biocbon\/css\/fonts.css":1,"sites\/all\/themes\/biocbon\/css\/styles.css":1,"sites\/all\/themes\/biocbon\/css\/layouts\/responsive_1200.css":1,"sites\/all\/themes\/biocbon\/css\/layouts\/responsive_mobile.css":1,"sites\/all\/themes\/biocbon\/css\/animation.css":1}},"urlIsAjaxTrusted":{"\/fr":true},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":0,"popup_clicking_confirmation":false,"popup_scrolling_confirmation":false,"popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--opt-in\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003ENous utilisons des cookies pour am\u00e9liorer votre exp\u00e9rience de navigation sur notre site.\u003Cbr \/\u003EEn naviguant sur ce site, vous acceptez la politique d\u0027utilisation des cookies.\u003C\/p\u003E              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003ENon, donnez moi plus d\u0027informations\u003C\/button\u003E\n          \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-secondary-button\u0022\u003EOK, je suis d\u0027accord\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022decline-button eu-cookie-compliance-default-button\u0022 \u003ERefuser\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","use_mobile_message":false,"mobile_popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--opt-in\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n                    \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003ENon, donnez moi plus d\u0027informations\u003C\/button\u003E\n          \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-secondary-button\u0022\u003EOK, je suis d\u0027accord\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022decline-button eu-cookie-compliance-default-button\u0022 \u003ERefuser\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","mobile_breakpoint":"768","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class=\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EThank you for accepting cookies\u003C\/h2\u003E\u003Cp\u003EYou can now hide this message or find out more about cookies.\u003C\/p\u003E    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button eu-cookie-compliance-hide-button\u0022\u003EMasquer\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button-thank-you\u0022 \u003EPlus d\u0027infos\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_use_bare_css":false,"popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"\/fr\/cookies","popup_link_new_window":1,"popup_position":true,"fixed_top_position":false,"popup_language":"fr","store_consent":false,"better_support_for_screen_readers":0,"reload_page":0,"domain":"","domain_all_sites":null,"popup_eu_only_js":0,"cookie_lifetime":"100","cookie_session":false,"disagree_do_not_show_popup":0,"method":"opt_in","whitelisted_cookies":"","withdraw_markup":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003E\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-withdraw-banner\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n          \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button\u0022\u003E\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","withdraw_enabled":false,"withdraw_button_on_info_popup":0,"cookie_categories":[],"enable_save_preferences_button":1,"fix_first_cookie_category":1,"select_all_categories_by_default":0},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackDomainMode":2,"trackCrossDomains":["bio-c-bon.eu","bio-c-bon.fr","bio-c-bon.es","bio-c-bon.pt","bio-c-bon.it","bio-c-bon.ch","bio-c-bon.be"]},"lang":"fr","password":{"strengthTitle":"Conformit\u00e9 du mot de passe :"},"type":"setting"});</script>
<!--[if lt IE 9]>
    <script src="/sites/all/themes/zen/js/html5-respond.js"></script>
    <![endif]-->
<meta content="406b1fede415ade1a914b1f9d6876b32" name="p:domain_verify"/>
</head>
<body class="html front not-logged-in no-sidebars page-home i18n-fr">
<p id="skip-link">
<a class="element-invisible element-focusable" href="#main-menu">Jump to navigation</a>
</p>
<div class="header-card-banner txColorVertFoncer">
<div class="header-card-banner-close fontSizeBig txColorBlanc">X</div>
<span class="card-billet"></span><b>Félicitations !</b> Vous avez atteint les 30 000 points vous donnant droit à votre remise de 10€  <a class="link puce txColorBlanc fontSizeNormal" href="/fr/carte_fidelite/status">En savoir plus</a>
</div>
<div class="sizePage" id="page">
<header class="header fixElement sizePage" id="header" role="banner">
<div class="zoneHeader" id="logo">
<a class="header__logo" href="/fr" id="logo" rel="home" title="Accueil"><img alt="Accueil" class="header__logo-image" src="https://bio-c-bon.eu/sites/all/themes/biocbon/logo.png"/></a>
</div>
<div class="zoneHeader" id="menu_site">
<div class="header__region region region-header">
<div class="block block-bio-menu first last odd" id="block-bio-menu-bio-menu-header">
<h2 class="block__title block-title"><none></none></h2>
<nav id="menu_header" role="navigation">
<ul class="contentMenu">
<li class="item gotoUrl _bio_c_bon_view" x-href="/fr/bio_c_bon">
<div class="text fontSizeMiddle txColorGrisFoncer">
          Bio c’ Bon &amp; Actus        </div>
<div class="sousMenu">
<div class="sousMenuDefault bgColorVertFoncer">
<div class="image">
<a href="/fr/bio_c_bon" id="menuBioCBon"></a>
</div>
<div class="lien">
<ul>
<li><a class="link txColorBlanc puce" href="/fr/bio_c_bon/actualites">Actualités Bio c'Bon</a>
</li>
</ul>
</div>
</div>
</div> </li>
<li class="item gotoUrl recettes_et_conseils_view" x-href="/fr/recettes_et_conseils">
<div class="text fontSizeMiddle txColorGrisFoncer">
          Recettes &amp; conseils        </div>
<div class="sousMenu">
<div class="sousMenuDefault bgColorVertFoncer">
<div class="image">
<a href="/fr/recettes_et_conseils" id="menuRC"></a>
</div>
<div class="lien">
<ul>
<li><a class="link txColorBlanc puce" href="/fr/recettes_et_conseils/recettes">Les recettes</a>
</li>
<li><a class="link txColorBlanc puce" href="/fr/recettes_et_conseils/conseils">Les conseils</a>
</li>
<li><a class="link txColorBlanc puce" href="/fr/recettes_et_conseils/calendriers_de_saison">Calendriers de saison</a>
</li>
</ul>
</div>
</div>
</div> </li>
<li class="item gotoUrl _bio_c_quoi_view" x-href="/fr/bio_c_quoi">
<div class="text fontSizeMiddle txColorGrisFoncer">
          Bio c' Quoi        </div>
<div class="sousMenu">
<div class="sousMenuDefault bgColorVertFoncer">
<div class="image">
<a href="/fr/bio_c_quoi" id="menuBioCQuoi"></a>
</div>
<div class="lien">
<ul>
<li><a class="link txColorBlanc puce" href="/fr/bio_c_quoi/lagriculture-biologique-cest-quoi">L'agriculture biologique c'est quoi ?</a>
</li>
<li><a class="link txColorBlanc puce" href="/fr/bio_c_quoi/se-faire-du-bien-et-retrouver-le-gout-des-aliments">Se faire du bien et retrouver<br/>le goût des aliments</a>
</li>
<li><a class="link txColorBlanc puce" href="/fr/bio_c_quoi/lhumain-au-coeurde-la-consommation">L'humain au cœur de la consommation</a>
</li>
<li><a class="link txColorBlanc puce" href="/fr/bio_c_quoi/les-labels-et-certifications">Les labels et certifications</a>
</li>
</ul>
</div>
</div>
</div> </li>
<li class="item gotoUrl bio_magasins_view" x-href="/fr/nos_magasins">
<div class="text fontSizeMiddle txColorGrisFoncer">
          Magasins        </div>
</li>
<li class="item gotoUrl _carte_de_fidelite_view " x-href="/fr/carte_fidelite">
<div class="text fontSizeMiddle txColorGrisFoncer">
          Club Bio c' Bon        </div>
</li>
</ul>
</nav>
</div>
</div>
</div>
</header>
<div class="region region-highlighted">
<div class="block block-bio-slides first last odd" id="block-bio-slides-bio-slides">
<h2 class="block__title block-title"><none></none></h2>
<div id="bio_slider">
<div class="owl-carousel" id="owl-slide">
<div class="item">
<img alt="" height="595" src="https://bio-c-bon.eu/sites/default/files/styles/visuel_carrousel/public/module/bio_slides/hero-homepage-bio-c-bon-recrute.png?itok=qO7uFCX_" width="1500"/></div> <div class="item">
<img alt="" height="595" src="https://bio-c-bon.eu/sites/default/files/styles/visuel_carrousel/public/module/bio_slides/bio-cbon-herovisuel_reouverture.png?itok=XjYh8OEf" width="1500"/></div> <div class="item">
<img alt="" height="595" src="https://bio-c-bon.eu/sites/default/files/styles/visuel_carrousel/public/module/bio_slides/bio-cbonhero_nouvelleannee2021_homepage_v2.png?itok=A_W8_4PF" width="1500"/></div> </div>
<div id="zoneSlideAnnexe">
<div class="overlay"></div>
<div class="content" id="panneau0">
<div class="element">
<h3 class="txColorVertClair fontSizeBigUltra">On recrute ! </h3>
<pre class="txColorBlanc fontSizeMiddle borderBottom borderTop">Retrouvez toutes nos offres d'emploi. </pre>
<a class="link txColorVertFoncer bgColorVertClair puce" href="https://bio-c-bon.eu/fr/rejoignez-nous">En savoir plus </a> </div>
</div>
<div class="content" id="panneau1">
<div class="element">
<h3 class="txColorVertClair fontSizeBigUltra">RÉOUVERTURE</h3>
<pre class="txColorBlanc fontSizeMiddle borderBottom borderTop">Nos magasins rouvrent petit à petit leurs portes. Pour retrouver la liste des ouvertures, c'est par ici : 
</pre>
<a class="link txColorVertFoncer bgColorVertClair puce" href="https://www.bio-c-bon.eu/fr/nos_magasins">En savoir plus</a> </div>
</div>
<div class="content" id="panneau2">
<div class="element">
<h3 class="txColorVertClair fontSizeBigUltra">2021</h3>
<pre class="txColorBlanc fontSizeMiddle borderBottom borderTop">Nous vous souhaitons une excellente année 2021 !</pre>
<a class="link txColorVertFoncer bgColorVertClair puce" href="https://bio-c-bon.eu/fr/belle-annee-2021">Le petit mot</a> </div>
</div>
<div id="slideHomeAnim1"></div>
<div id="slideHomeAnim2"></div>
</div>
</div>
</div>
</div>
<div id="main">
<div class="bgColorBlanc">
<div class="region region-underslider">
<div class="block block-bio-block first odd" id="block-bio-block-bio-block-us-bio-magasin">
<div class="blockUs">
<div class="borderBottom">
<h3 class="txColorVertFoncer fontSizeUltra">Trouvez <span class="txColorVertClair">le magasin</span><br/> le plus proche de chez vous</h3>
</div>
<div class="txColorGrisFoncer">
<div class="autocomplete-front">
<input autocomplete="off" class="map-autocomplete" placeholder="Code postal, ville..." type="text"/><span class="map-autocomplete-btn"></span>
</div>
</div>
</div>
</div>
<div class="block block-bio-block even" id="block-bio-block-bio-block-us-bio-carte-fideliter">
<h2 class="block__title block-title"><none></none></h2>
<div class="blockUs">
<div class="borderBottom">
<h3 class="txColorVertFoncer fontSizeUltra">Club Fidélité<br/> <span class="txColorVertClair">Bio c' Bon</span> !</h3>
</div>
<div class="txColorGrisFoncer">
<p>Découvrez le Club Fidélité Bio c' Bon,<br/>
<br/>
et profitez de votre avantage en magasin.<br/>
 </p>
</div>
<a class="link bgColorVertClair txColorVertFoncer puce" href="https://bio-c-bon.eu/fr/carte_fidelite"><b>Découvrir</b></a>
</div>
</div>
<div class="block block-bio-block last odd" id="block-bio-block-bio-block-us-bio-recrutement">
<h2 class="block__title block-title"><none></none></h2>
<div class="blockUs">
<div class="borderBottom">
<h3 class="txColorVertFoncer fontSizeUltra">Bio c' Bon recrute,<br/><span class="txColorVertClair">rejoignez-nous</span></h3>
</div>
<div class="txColorGrisFoncer">
<p>Nous recherchons des personnes</p>
<p>motivées et entreprenantes pour participer</p>
<p>avec nous à l'aventure Bio c' Bon</p>
</div>
<a class="link bgColorVertClair txColorVertFoncer puce" href="Rejoignez-nous"><b>Découvrir</b> les offres</a>
</div>
</div>
</div>
</div>
<div class="column" id="content" role="main">
<a id="main-content"></a>
<div class="block block-bio-produits first last odd" id="block-bio-produits-bio-produits-du-moment">
<h2 class="block__title block-title"><none></none></h2>
<div id="tblProdtuisDuMoment">
<div class="blockPush">
<h3 class="negaratio">
<div>Nos produits <span class="borderBottom">tout bio tout bon</span></div>
</h3>
</div>
<div class="produits itemSlideProduit" data-category="Gemmothérapie">
<h3><a class="borderBottom txColorVertFoncer fontSizeMiddle" href="/fr/beaute-bien-etre/bien-etre?node=1594">Gemmothérapie</a>
</h3>
<div class="gotoUrl" x-href="/fr/beaute-bien-etre/bien-etre?node=1594">
<img alt="" height="200" src="https://bio-c-bon.eu/sites/default/files/styles/produit_home/public/magasins//02-gemmotherapie.png?itok=z5-l7Jp7" width="250"/> </div>
</div>
<div class="produits itemSlideProduit" data-category="Fruits secs">
<h3><a class="borderBottom txColorVertFoncer fontSizeMiddle" href="/fr/alimentation-boisson/epicerie-sucree?node=1585">Fruits secs</a>
</h3>
<div class="gotoUrl" x-href="/fr/alimentation-boisson/epicerie-sucree?node=1585">
<img alt="" height="200" src="https://bio-c-bon.eu/sites/default/files/styles/produit_home/public/magasins//09-fruitsSecs.png?itok=qwim32if" width="250"/> </div>
</div>
<div class="produits itemSlideProduit" data-category="Café et chocolat">
<h3><a class="borderBottom txColorVertFoncer fontSizeMiddle" href="/fr/alimentation-boisson/epicerie-sucree?node=1579">Café et chocolat</a>
</h3>
<div class="gotoUrl" x-href="/fr/alimentation-boisson/epicerie-sucree?node=1579">
<img alt="" height="200" src="https://bio-c-bon.eu/sites/default/files/styles/produit_home/public/magasins//03-cafeChocolat.png?itok=PSAQ2IuN" width="250"/> </div>
</div>
<div class="produits itemSlideProduit" data-category="Jus de fruits et légumes">
<h3><a class="borderBottom txColorVertFoncer fontSizeMiddle" href="/fr/alimentation-boisson/boissons-non-alcoolisees?node=1559">Jus de fruits et légumes</a>
</h3>
<div class="gotoUrl" x-href="/fr/alimentation-boisson/boissons-non-alcoolisees?node=1559">
<img alt="" height="200" src="https://bio-c-bon.eu/sites/default/files/styles/produit_home/public/magasins//02-jusFruitsLegumes.png?itok=tcwy0Eao" width="250"/> </div>
</div>
<div class="produits itemSlideProduit" data-category="Légumes verts">
<h3><a class="borderBottom txColorVertFoncer fontSizeMiddle" href="/fr/alimentation-boisson/legumes?node=1528">Légumes verts</a>
</h3>
<div class="gotoUrl" x-href="/fr/alimentation-boisson/legumes?node=1528">
<img alt="" height="200" src="https://bio-c-bon.eu/sites/default/files/styles/produit_home/public/magasins//08-legumesVerts.png?itok=rdpYTXcr" width="250"/> </div>
</div>
<div class="produits itemSlideProduit" data-category="Champignons">
<h3><a class="borderBottom txColorVertFoncer fontSizeMiddle" href="/fr/alimentation-boisson/legumes?node=1521">Champignons</a>
</h3>
<div class="gotoUrl" x-href="/fr/alimentation-boisson/legumes?node=1521">
<img alt="" height="200" src="https://bio-c-bon.eu/sites/default/files/styles/produit_home/public/magasins//01-champignons.png?itok=mMtgIl5_" width="250"/> </div>
</div>
<div class="produits itemSlideProduit" data-category="Fruits exotiques">
<h3><a class="borderBottom txColorVertFoncer fontSizeMiddle" href="/fr/alimentation-boisson/fruits?node=1519">Fruits exotiques</a>
</h3>
<div class="gotoUrl" x-href="/fr/alimentation-boisson/fruits?node=1519">
<img alt="" height="200" src="https://bio-c-bon.eu/sites/default/files/styles/produit_home/public/magasins//05-fruitsExotiques.png?itok=QHnC9PQ0" width="250"/> </div>
</div>
<div class="produits itemSlideProduit" data-category="Fruits à pépins">
<h3><a class="borderBottom txColorVertFoncer fontSizeMiddle" href="/fr/alimentation-boisson/fruits?node=1518">Fruits à pépins</a>
</h3>
<div class="gotoUrl" x-href="/fr/alimentation-boisson/fruits?node=1518">
<img alt="" height="200" src="https://bio-c-bon.eu/sites/default/files/styles/produit_home/public/magasins//04-fruitsPepin.png?itok=9hIpzH6I" width="250"/> </div>
</div>
</div>
</div>
</div>
</div>
<div id="whitezone">
<div class="content">
<div class="region region-whitezone">
<div class="block block-recettes-et-conseils first odd" id="block-recettes-et-conseils-push-recettes">
<h2 class="block__title block-title">Laissez-vous tenter...</h2>
<section class="btn gotoUrl" x-href="/fr/recettes_et_conseils/recettes/brochettes-de-fruits-caramelises">
<div class="item recettes">
<div class="contentImg">
<img alt="" height="365" src="https://bio-c-bon.eu/sites/default/files/styles/promo_home/public/module/bio_push/recettes/brochette-fruit.png?itok=Fg7jW8qH" width="465"/> </div>
<div class="content">
<a class="fontSizeUltra txColorVertFoncer title" href="/fr/recettes_et_conseils/recettes/brochettes-de-fruits-caramelises">
        Brochettes de fruits caramélisés      </a>
<div class="extendContent">
<hr class="borderBottom"/>
<ul>
<li class="ico ico-nb-personne txColorGrisFoncer">4</li>
<li class="ico ico-nb-preparation txColorGrisFoncer">10</li>
<li><a class="link bgColorVertClair txColorVertFoncer puce" href="/fr/recettes_et_conseils/recettes/brochettes-de-fruits-caramelises">En savoir +</a></li>
</ul>
</div>
</div>
<a class="linkHomeType" href="/fr/recettes_et_conseils/recettes"><em></em>Voir toutes les <b>recettes</b> </a>
</div>
</section>
</div>
<div class="block block-recettes-et-conseils even" id="block-recettes-et-conseils-push-conseils">
<h2 class="block__title block-title">On vous conseille</h2>
<section class="btn gotoUrl" x-href="/fr/recettes_et_conseils/conseils/proteines-vegetales-vers-une-alimentation-plus-responsable">
<div class="item recettes">
<div class="contentImg">
<img alt="" height="365" src="https://bio-c-bon.eu/sites/default/files/styles/promo_home/public/module/bio_push/conseils/photo_0.png?itok=c_Aac9z5" width="465"/> </div>
<div class="content">
<a class="fontSizeUltra txColorVertFoncer title" href="/fr/recettes_et_conseils/conseils/proteines-vegetales-vers-une-alimentation-plus-responsable">
        Protéines végétales : vers une alimentation plus responsable      </a>
<div class="extendContent">
<hr class="borderBottom"/>
<p>Découvrez les nombreux atouts des protéines végétales ! </p>
</div>
</div>
<a class="linkHomeType" href="/fr/recettes_et_conseils/conseils"><em></em>Voir tous les <b>conseils</b> </a>
</div>
</section>
</div>
<div class="block block-bio-calendrier last odd" id="block-bio-calendrier-push-calendrier-home">
<h2 class="block__title block-title">Le calendrier des fruits et légumes</h2>
<div id="pushHomeCalendrier"><a href="/fr/recettes_et_conseils/calendriers_de_saison"><img alt="" height="415" src="https://bio-c-bon.eu/sites/default/files/styles/calendrier_home/public/nouveau_panneau_465x415.png?itok=Y6PD-f74" width="465"/></a><div class="bgColorVertClair"><a class="link txColorVertFoncer puce" href="/fr/recettes_et_conseils/calendriers_de_saison">Voir les calendriers</a></div></div>
</div>
</div>
</div>
</div>
<div class="bgColorVertFoncer" id="greenZone">
<div class="region region-greenzone">
<div class="block block-bio-block first odd" id="block-bio-block-bio-block-bio-c-quoi">
<h2 class="block__title block-title"><none></none></h2>
<div>
<section class="blockPromo">
<h2 class="fontSizeBigUltra txColorBlanc borderBottom">Bio c' Quoi</h2>
<p class="txColorBlanc">Le monde du bio semble parfois bien mystérieux... <br/> Finalement, le bio, pourquoi c'est bon ? </p>
<a class="link txColorGrisFoncer bgColorBlanc puce puceVerte" href="/fr/bio_c_quoi">
       On vous <b>Explique tout !</b>
</a>
</section>
</div>
</div>
<div class="block block-bio-block even" id="block-bio-block-bio-block-bio-c-bon">
<h2 class="block__title block-title"><none></none></h2>
<div>
<section class="blockPromo">
<h2 class="fontSizeBigUltra txColorBlanc borderBottom">Bio c'bon</h2>
<p class="txColorBlanc">Vous voulez en savoir plus sur nous, sur notre philosophie, sur notre histoire ?</p>
<a class="link txColorGrisFoncer bgColorBlanc puce puceVerte" href="/fr/bio_c_bon">Découvrez <b>Notre charte</b></a> </section>
</div>
</div>
<div class="block block-bio-block last odd" id="block-bio-block-bio-block-subscribe-simplenews2">
<h2 class="block__title block-title"><none></none></h2>
<div>
<section class="blockPromo" id="newsletter">
<h2 class="fontSizeBigUltra txColorBlanc borderBottom">Abonnez-vous </h2>
<p class="txColorBlanc"> Recevez des infos et des bons plans pour vous rendre la vie plus facile. Nous ne ferons jamais déborder votre boîte aux lettres, promis ! </p>
<form accept-charset="UTF-8" action="/fr" class="simplenews-subscribe" id="simplenews-block-form-9" method="post"><div><div class="form-item form-type-textfield form-item-mail">
<label for="edit-mail"> <span class="form-required" title="Ce champ est requis.">*</span></label>
<input class="form-text required" id="edit-mail" maxlength="128" name="mail" placeholder="Votre email " size="20" type="text" value=""/>
</div>
<input class="form-submit" id="edit-submit" name="op" type="submit" value="OK"/><input name="form_build_id" type="hidden" value="form-cHNul3boxUXw2ZWYkDTQKy0q1jnHFnzURvK_lgK852g"/>
<input name="form_id" type="hidden" value="simplenews_block_form_9"/>
</div></form> </section>
</div>
</div>
</div>
</div>
<div class="txColorGrisFoncer fontSizeMiddle" id="postionBottom">
<div class="region region-bottom">
<div class="block block-bio-block first odd" id="block-bio-block-bio-block-subscribe-simplenews">
<h2 class="block__title block-title"><none></none></h2>
<form accept-charset="UTF-8" action="/fr" class="simplenews-subscribe" id="simplenews-block-form-9--2" method="post"><div><div class="form-item form-type-textfield form-item-mail">
<label for="edit-mail--2">Abonnez-vous aux bons plans <span class="form-required" title="Ce champ est requis.">*</span></label>
<input class="form-text required" id="edit-mail--2" maxlength="128" name="mail" placeholder="Votre email " size="20" type="text" value=""/>
</div>
<input class="form-submit" id="edit-submit--2" name="op" type="submit" value="OK"/><input name="form_build_id" type="hidden" value="form-BE21XT4pv2LIvZe1_C3EKcwYWIcVjlOsHV4TZByTl-8"/>
<input name="form_id" type="hidden" value="simplenews_block_form_9"/>
</div></form>
</div>
<div class="block block-bio-block even" id="block-bio-block-bio-block-share">
<h2 class="block__title block-title"><none></none></h2>
<aside id="sharingLink">
<label>
    Suivez-nous  </label>
<ul>
<li>
<a class="pinterest" href="https://fr.pinterest.com/biocbon/" target="_blank"></a>
</li>
<li>
<a class="facebook" href="https://www.facebook.com/biocbonfrance/" target="_blank"></a>
</li>
<li>
<a class="twitter" href="https://twitter.com/biocbon" target="_blank"></a>
</li>
<li>
<a class="linkedin" href="https://fr.linkedin.com/company/bio-c%27%E2%80%8B-bon" target="_blank"></a>
</li>
<li>
<a class="instagram" href="https://www.instagram.com/bio_c_bon/" target="_blank"></a>
</li>
</ul>
</aside>
</div>
<div class="block block-bio-block last odd" id="block-bio-block-bio-block-contact">
<h2 class="block__title block-title"><none></none></h2>
<aside id="footerContact">
<a class="txColorGrisFoncer" href="/fr/contact">Nous contacter</a>
</aside>
</div>
</div>
</div>
<footer class="region region-footer" id="footer">
<div class="block block-bio-block first odd" id="block-bio-block-bio-block-language">
<h2 class="block__title block-title"><none></none></h2>
<span>Langue : </span>
<select class="select-language">
<option value="fr">Français</option>
<option value="it">Italiano</option>
<option value="es">Español</option>
<option value="pt-pt">Português</option>
</select>
<span>.</span>
</div>
<div class="block block-bio-menu last even" id="block-bio-menu-bio-menu-footer">
<h2 class="block__title block-title"><none></none></h2>
<footer id="footer">
<ul>
<li class="_rejoignez_nous_view">
<a href="/fr/rejoignez-nous">
        Recrutement      </a>
      .
    </li>
<li class="_bio_configuration_form_view">
<a href="/fr/contact">
        Contact      </a>
      .
    </li>
<li class="node_page_view">
<a href="/fr/cgu-et-mentions-legales">
        CGU &amp; Mentions légales      </a>
      .
    </li>
<li class="node_page_view">
<a href="/fr/cookies">
        Cookies      </a>
      .
    </li>
<li class="node_page_view">
<a href="/fr/plan-du-site">
        Plan du site      </a>
      .
    </li>
<li>
<span>© Bio c' Bon 2014 - 2021</span>
</li>
</ul>
</footer>
</div>
</footer>
</div>
<div id="popup-old-nav">
<h1 class="txColorGrisFoncer fontSizeBigUltra">Bienvenue<br/> sur le site <span class="txColorVertClair">Bio c' Bon</span></h1>
<p>
    La version de votre navigateur est obsolète et ne vous permet pas une navigation optimisée et sécurisée.   </p>
<p>
    Pour profiter pleinement de notre site, nous vous invitons à mettre à jour votre navigateur :  </p>
<table>
<tr>
<td class="ie"><a href="http://windows.microsoft.com/fr-fr/internet-explorer/download-ie">Download IE</a></td>
<td class="firefox"><a href="https://www.mozilla.org/fr/firefox/">Download Firefox</a></td>
<td class="chrome"><a href="https://www.google.com/chrome">Download Chrome</a></td>
<td class="opera"><a href="http://www.opera.com/fr/download">Download opera</a></td>
</tr>
</table>
</div>
<div id="popup-old-nav-overlay"></div>
<script>var eu_cookie_compliance_cookie_name = "";</script>
<script src="https://bio-c-bon.eu/sites/default/files/js/js_AcGzZCsBqHbdwE8a5qhmE7hTrWx__OxyAey9xSrmqVc.js"></script>
</body>
</html>
