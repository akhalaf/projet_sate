<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="telephone=no" name="format-detection"/>
<meta content="" name="author"/>
<link href="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/favicon.png" rel="icon"/>
<link href="https://fonts.googleapis.com/css?family=Barlow+Condensed:300,400,700" rel="stylesheet"/>
<!-- Start SmartBanner configuration -->
<meta content="Bongo's Bingo" name="smartbanner:title"/>
<meta content="24h Early Ticket Access" name="smartbanner:author"/>
<meta content="&amp;" name="smartbanner:price"/>
<meta content=" App Exclusive Early Birds" name="smartbanner:price-suffix-apple"/>
<meta content=" App Exclusive Early Birds" name="smartbanner:price-suffix-google"/>
<meta content="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/favicon.png" name="smartbanner:icon-apple"/>
<meta content="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/favicon.png" name="smartbanner:icon-google"/>
<meta content="VIEW" name="smartbanner:button"/>
<meta content="https://appstore.com/bongosbingo" name="smartbanner:button-url-apple"/>
<meta content="https://play.google.com/store/apps/details?id=uk.co.bongosbingo" name="smartbanner:button-url-google"/>
<meta content="android,ios" name="smartbanner:enabled-platforms"/>
<meta content="Close" name="smartbanner:close-label"/>
<!-- End SmartBanner configuration -->
<!--<meta http-equiv="Content-Security-Policy" content="default-src * 'self' data: 'unsafe-inline' 'unsafe-eval' https://bongosbingos3.s3.amazonaws.com; script-src * 'self' data: 'unsafe-inline' 'unsafe-eval' https://js.stripe.com https://bongosbingos3.s3.amazonaws.com; style-src * 'self' data: 'unsafe-inline' https://bongosbingos3.s3.amazonaws.com; img-src * 'self' data:; font-src * 'self' data:; connect-src * 'self' https://api.stripe.com; media-src * 'self'; object-src * 'self'; prefetch-src * 'self'; child-src * 'self'; frame-src * 'self' https://js.stripe.com https://hooks.stripe.com https://vars.hotjar.com/; form-action * 'self' https://hooks.stripe.com/redirect"> -->
<script type="application/ld+json">
  [{
    "@context": "http://schema.org",
      "@type": "WebSite",
      "name": "Bongo's Bingo",
      "url": "https://bongosbingo.co.uk",
      "sameAs":["https://www.facebook.com/BongosBingo","https://twitter.com/BongosBingo","https://www.instagram.com/BongosBingo/","https://www.youtube.com/channel/UCHlz4LI1eZXuskb7OPt-2wQ"]
  },
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "Bongo's Bingo",
    "url": "https://bongosbingo.co.uk",
    "logo": "https://s3.eu-west-2.amazonaws.com/bongosbingos3/static/img/logo-pink.png",
    "contactPoint": [{
          "@type": "ContactPoint",
          "email": "info@bongosbingo.co.uk",
          "contactType": "customer support",
          "areaServed": ["GB", "FR", "ES", "AE", "AU"],
          "availableLanguage":"English"
        }]
  }]
  </script>
<!-- Facebook -->
<meta content="website" property="og:type"/>
<meta content="Bongo's Bingo" property="og:site_name"/>
<meta content="Bongo's Bingo - Home" property="og:title"/><!-- -->
<meta content="" property="og:description"/>
<meta content="https://www.bongosbingo.co.uk" property="og:url"/>
<meta content="" property="og:image"/>
<meta content="" property="og:updated_time"/>
<meta content="" property="fb:app_id"/>
<!-- Twitter -->
<meta content="" property="twitter:card"/>
<meta content="" property="twitter:site"/>
<meta content="Bongo's Bingo - Home" property="twitter:title"/><!-- -->
<meta content="" property="twitter:description"/>
<meta content="" property="twitter:image"/>
<meta content="https://www.bongosbingo.co.uk" property="twitter:url"/>
<meta content="a1a2e77d4f7215e78b6ae7e656a7e897472c0e52" name="B-verify"/>
<noscript>
<link href="https://s3-ew2-production-bb.s3.amazonaws.com/static/css/ds-custom-no-js.css" rel="stylesheet"/>
</noscript>
<title>Bongo's Bingo - Home</title>
<!-- Bootstrap core CSS -->
<link href="https://s3-ew2-production-bb.s3.amazonaws.com/static/css/bootstrap.min.css" rel="stylesheet"/>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link href="https://s3-ew2-production-bb.s3.amazonaws.com/static/css/ie10-viewport-bug-workaround.css" rel="stylesheet"/>
<!-- Font awesome core  -->
<link href="https://s3-ew2-production-bb.s3.amazonaws.com/static/css/font-awesome.min.css" rel="stylesheet"/>
<!-- Chosen select boxes -->
<link href="https://s3-ew2-production-bb.s3.amazonaws.com/static/css/chosen.css" rel="stylesheet"/>
<!-- Custom styles for this template -->
<link href="https://s3-ew2-production-bb.s3.amazonaws.com/static/fonts/ss-social-regular.css" rel="stylesheet"/>
<link href="https://s3-ew2-production-bb.s3.amazonaws.com/static/css/lightbox.min.css" rel="stylesheet"/>
<link href="https://s3-ew2-production-bb.s3.amazonaws.com/static/css/base.css" rel="stylesheet"/>
<!--[if IE]><link href="https://s3-ew2-production-bb.s3.amazonaws.com/static/css/ds-ie-custom.css" rel="stylesheet"><![endif]-->
<link href="https://s3-ew2-production-bb.s3.amazonaws.com/static/css/smartbanner.min.css" rel="stylesheet"/>
<link href="https://s3-ew2-production-bb.s3.amazonaws.com/static/css/ds-custom.css?v=1.05" rel="stylesheet"/>
<!-- Facebook Pixel Code -->
<script>
  /* eslint-disable */
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window,document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1050819665351700'); 
  fbq('track', 'PageView');
  /* eslint-enable */
  </script>
<noscript>
<img height="1" src="https://www.facebook.com/tr?id=1050819665351700&amp;ev=PageView
  &amp;noscript=1" width="1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<style type="text/css">
</style>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://s3-ew2-production-bb.s3.amazonaws.com/static/js/html5shiv.min.js"></script>
    <script src="https://s3-ew2-production-bb.s3.amazonaws.com/static/js/respond.min.js"></script>
  <![endif]-->
<!-- Google Analytics Start -->
<script>
    /* eslint-disable */
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga("create", "UA-87345744-3");
    ga("require", "ec");
    /* eslint-enable */
</script>
<!-- Google Analytics End -->
<script>
    /* eslint-disable */
    ga('send', 'pageview');
    /* eslint-enable */
</script>
</head>
<body>
<header>
<nav class="navbar navbar-fixed-top" id="header">
<div class="container">
<div class="header-content">
<div class="navbar-header">
<a class="logo-container" href="/">
<img alt="Bongo's Bingo" class="logo" src="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/logo-white.png"/>
</a>
</div>
<div class="social-container">
<a href="https://www.facebook.com/BongosBingo/" style="height: 30px; width: 30px;" target="_blank">
<img alt="Facebook" src="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/icons/facebook.svg" style="height: 30px; width: 30px;"/>
</a>
          
           
          
            <a href="https://twitter.com/BongosBingo" style="height: 30px; width: 30px;" target="_blank">
<img alt="Twitter" src="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/icons/twitter.svg" style="height: 30px; width: 30px;"/>
</a>
          
           
          
            <a href="https://www.instagram.com/bongosbingo/" style="height: 30px; width: 30px;" target="_blank">
<img alt="Instagram" src="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/icons/instagram.svg" style="height: 30px; width: 30px;"/>
</a>
          
           
          
            <a href="https://www.youtube.com/channel/UCHlz4LI1eZXuskb7OPt-2wQ" style="height: 30px; width: 30px;" target="_blank">
<img alt="YouTube" src="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/icons/youtube.svg" style="height: 30px; width: 30px;"/>
</a>
</div>
<a class="button btn-join btn-find-events" href="/events/">FIND EVENTS </a>
<button aria-expanded="false" class="navbar-toggle" id="mobile-menu-icon" type="button">
<span class="mobile-toggle-line"></span>
<span class="mobile-toggle-line"></span>
<span class="mobile-toggle-line"></span>
<span class="mobile-toggle-line"></span>
</button>
<div class="navbar-collapse" id="mobile-menu">
<ul class="nav navbar-nav">
<li class="mobile-social">
<a href="https://www.facebook.com/BongosBingo/" style="height: 30px; width: 30px; display: inline-block; background: transparent !important; margin-bottom: 3px; padding: 0;" target="_blank">
<img alt="Facebook" src="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/icons/facebook-white.svg" style="height: 30px; width: 30px;"/>
</a>
              
               
              
                <a href="https://twitter.com/BongosBingo" style="height: 30px; width: 30px; display: inline-block; background: transparent !important; margin-bottom: 3px; padding: 0;" target="_blank">
<img alt="Twitter" src="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/icons/twitter-white.svg" style="height: 30px; width: 30px;"/>
</a>
              
               
              
                <a href="https://www.instagram.com/bongosbingo/" style="height: 30px; width: 30px; display: inline-block; background: transparent !important; margin-bottom: 3px; padding: 0;" target="_blank">
<img alt="Instagram" src="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/icons/instagram-white.svg" style="height: 30px; width: 30px;"/>
</a>
              
               
              
                <a href="https://www.youtube.com/channel/UCHlz4LI1eZXuskb7OPt-2wQ" style="height: 30px; width: 30px; display: inline-block; background: transparent !important; margin-bottom: 3px; padding: 0;" target="_blank">
<img alt="YouTube" src="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/icons/youtube-white.svg" style="height: 30px; width: 30px;"/>
</a>
</li>
<li><h3>MY BONGO'S</h3></li>
<li><a href="/signup/">SIGN UP</a></li>
<li><a href="/login/">LOGIN</a></li>
<li><h3>BUY TICKETS &amp; STUFF</h3></li>
<li><a class="menu-link" href="/events/">FIND EVENTS</a></li>
<li><a class="menu-link" href="/emergency-services/">NHS FREE EVENTS</a></li>
<li><a class="menu-link" href="/mobile-app/">DOWNLOAD APP</a></li>
<li><a class="menu-link" href="/subscribe/">SUBSCRIBE</a></li>
<li><a class="menu-link" href="/press/">ANNOUNCEMENTS</a></li>
<li><a class="menu-link" href="/private/">PRIVATE HIRE</a></li>
<li><h3>THE F**K IS BONGO'S?</h3></li>
<li><a class="menu-link" href="/blog/" style="display: none;">BLOG</a></li>
<li><a class="menu-link" href="/hosts-and-dancers/">HOSTS &amp; DANCERS</a></li>
<li><h3>CONTACT US</h3></li>
<li><a class="menu-link" href="/help/contact/">FAQ/CONTACT</a></li>
</ul>
</div>
</div>
</div>
</nav>
</header>
<section class="home">
<div class="video-container">
<div class="intro-video" id="intro-video">
<video autoplay="" class="fullscreen fill" height="100%" id="video" loop="" muted="" width="100%">
</video>
<div class="video-bg" style="background-image: url(https://s3-ew2-production-bb.s3.amazonaws.com/media/videos/new_video_still.png                                                                                                                                                                                                                                     ); background-size: cover; background-repeat: no-repeat;"></div>
<div class="sound-toggle"></div>
<canvas class="confetti-cannon" id="confetti-cannon"></canvas>
</div>
<div class="mobile-video" id="mobile-video">
<video autoplay="" class="fullscreen fill" height="100%" id="full_video" loop="" muted="" playsinline="" preload="" width="100%">
<source src="https://s3-ew2-production-bb.s3.amazonaws.com/media/videos/General_v4_dan_no_logo__2.mp4" type="video/mp4">
</source></video>
<div class="video-bg" style="background-image: url(https://s3-ew2-production-bb.s3.amazonaws.com/media/videos/mobile_new_video_still.png                                                                                                                                                                                                                                                                           ); background-size: cover; background-repeat: no-repeat;"></div>
<canvas class="confetti-cannon" id="mob-confetti-cannon"></canvas>
<div class="mobile-links"><a class="button btn btn-join btn-find-events-home" href="/events/">FIND EVENTS  </a><a class="button btn pink-link btn-find-events-home see-more-link flipped" href="https://twitch.tv/bongosbingo" target="_blank">WATCH LIVE!</a></div>
<div class="sound-toggle"></div>
<a class="scroll-down" href="#highlights"><img src="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/arrow-down.svg"/></a>
</div>
</div>
</section>
<section class="highlights">
<div class="container">
<div class="row">
<div class="col-xs-12">
<h2>Bongo's Highlights</h2>
<div class="carousel slide" data-ride="carousel" id="myCarousel">
<!-- Indicators -->
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#myCarousel"></li>
<li data-slide-to="1" data-target="#myCarousel"></li>
<li data-slide-to="2" data-target="#myCarousel"></li>
</ol>
<!-- Wrapper for slides -->
<div class="carousel-inner" role="listbox">
<div class="item active">
<div class="row row-eq-height flex-center">
<div class="col-xs-12">
<a href="https://bongos.bingo/SUMMER2021">
<picture>
<source media="(max-width: 799px)" srcset="https://s3-ew2-production-bb.s3.amazonaws.com/media/videos/Bongos_Bingo_Summer_Tour_Dates_102020_v2-08.jpeg">
<img src="https://s3-ew2-production-bb.s3.amazonaws.com/media/videos/Bongos_Bingo_Summer_Tour_Dates_102020_v2-08.jpeg" style="width: 100%; height: auto;"/>
</source></picture>
</a>
</div>
</div>
</div>
<div class="item ">
<div class="row row-eq-height flex-center">
<div class="col-xs-12">
<a href="/press/49/WECANBEHEROES/">
<picture>
<source media="(max-width: 799px)" srcset="https://s3-ew2-production-bb.s3.amazonaws.com/media/posts/WhatsApp_Image_2020-08-25_at_13.48.51.jpeg">
<source media="(min-width: 800px)" srcset="https://s3-ew2-production-bb.s3.amazonaws.com/media/posts/Bongos_Bingo_Superhero_Theme_082020_v2-03.jpg">
<img alt="LIVERPOOL DECEMBER DATES / WE CAN BE HEROES!" src="https://s3-ew2-production-bb.s3.amazonaws.com/media/posts/Bongos_Bingo_Superhero_Theme_082020_v2-03.jpg" style="width: 100%; height: auto;"/>
</source></source></picture>
</a>
</div>
</div>
</div>
<div class="item">
<div class="row row-eq-height flex-center">
<div class="col-xs-12">
<a href="/press/54/bongos-bingo-dubai-november/">
<picture>
<source media="(max-width: 799px)" srcset="https://s3-ew2-production-bb.s3.amazonaws.com/media/posts/73d9017a-c186-474f-b74b-faa51024cbe9.jpeg">
<source media="(min-width: 800px)" srcset="https://s3-ew2-production-bb.s3.amazonaws.com/media/posts/7f46f0ec-a67a-4229-aff9-575f8a7dd253.jpeg">
<img alt="Bongo's Bingo is coming back to Dubai" src="https://s3-ew2-production-bb.s3.amazonaws.com/media/posts/7f46f0ec-a67a-4229-aff9-575f8a7dd253.jpeg" style="width: 100%; height: auto;"/>
</source></source></picture>
</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="about">
<div class="container">
<div class="row">
<div class="col-xs-12">
<h2>About Bongo's</h2>
<p><strong>Bongo’s Bingo is <i>the</i> definitive bingo experience.</strong></p>
<p>It’s a crazy mix of traditional bingo, dance-offs, rave intervals, audience participation and countless magical moments, currently taking place in almost 50 locations around the world.</p><p>Bongo’s Bingo is a wild shared social extravaganza and a night of pure nostalgic escapism, with the chance to win iconic prizes from giant pink unicorns and Henry Hoovers to mobility scooters and karaoke machines at each and every show.</p>
<p>It’s immersive, inclusive and incredible. Everyone from 18 to 92 plays and parties together. There are no barriers and no limits.</p>
<p><strong>Quite simply, there’s nothing else like it.</strong></p>
</div>
</div>
</div>
</section>
<section>
<div class="fluid-container">
<div class="" id="instafeed"></div>
</div>
</section>
<section class="friends">
<div class="container">
<div style="text-align: center; margin: 40px 0;">
<h2 style="font-weight: bold; color: #8a67a2; text-transform: uppercase; margin-bottom: 20px;">Friends of Bongo's Bingo</h2>
<img alt="" class="gc-logo" src="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/southern-comfort.png" style="height: 100px; width: auto; margin-left: 40px;"/>
</div>
</div>
</section>
<footer class="footer">
<div class="container">
<div style="text-align: center; margin-bottom: 20px;">
        A participation/entrance fee is not charged if you wish to only play bingo,
        providing you have purchased your gambling stake prior to attending.<br/>
        Bingo only activities will be held in separate areas and whilst no participation/entrance
        fee will be required, only those individuals placing stakes will be permitted to play the bingo provided.<br/>
        For more T&amp;Cs click the link below.
      </div>
<div style="text-align: center; margin-bottom: 20px;">
<a href="/how-to-play/" style="color: #000 !important;"><b>How To Play</b></a>    
        <a href="/legal/terms-and-conditions/" style="color: #000 !important;"><b>Terms and Conditions</b></a>    
        <a href="/legal/privacy-policy/" style="color: #000 !important;"><b>Privacy Policy</b></a>    
        <a href="/legal/responsible-play/" style="color: #000 !important;"><b>Responsible Play</b></a>    
        <a href="/legal/self-exclusion/" style="color: #000 !important;"><b>Self Exclusion</b></a>
</div>
<div style="text-align: center; margin-bottom: 20px;">
<img alt="" class="gc-logo" src="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/drinkaware.png"/>  
        <img alt="" class="gc-logo" src="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/begambleaware.png"/>  
        <img alt="" class="gc-logo" src="https://s3-ew2-production-bb.s3.amazonaws.com/static/img/gambling_commission.png"/>
</div>
<div style="text-align: center; margin-bottom: 10px; font-size: 75%;">
        Bongo's Bingo 2021 | Company Number: 09858266 | VAT Number: GB 248 494 370 | Regulated by the <a href="https://secure.gamblingcommission.gov.uk/PublicRegister" style="color: black; text-decoration: underline;">Gambling Commission</a>, Operating Licence Number 047699-N-326687
      </div>
</div>
</footer>
<div class="modal terms-modal" data-backdrop="static" data-keyboard="false" role="dialog" tabindex="-1">
<div class="modal-dialog" role="document">
<div class="modal-content">
<h3 class="modal-title">Terms &amp; Conditions Update

        <div class="modal-body">
<p>Before continuing using the Bongo's Bingo site, you must read and accept our updated <a href="/legal/terms-and-conditions/" style="color: #222; text-decoration: underline;">Terms &amp; Conditions (click here to view)</a>.</p>
<p></p><form action="/terms-agree/" method="post"><input name="csrfmiddlewaretoken" type="hidden" value="qjqCFrvojJ0k9EUzk40YDMgFX33Y7SxJ1uymQAjrJueSWQMEDgsxD544KM8BQBXW"/><button class="btn btn-primary pink-link" name="terms" style="text-align: center;" value="agree">Agree</button></form>
<a class="btn btn-primary pink-link " href="/logout/">Decline</a>
</div>
</h3></div>
</div>
</div>
<div class="login-modal modal fade" id="login-modal" role="dialog">
<div class="modal-dialog" role="document">
<div class="modal-content">
<button aria-label="Close" class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">×</span>
</button>
<div class="modal-body">
<h3 class="modal-title">MY ACCOUNT</h3>
<form action="/login/" method="post">
<input name="csrfmiddlewaretoken" type="hidden" value="qjqCFrvojJ0k9EUzk40YDMgFX33Y7SxJ1uymQAjrJueSWQMEDgsxD544KM8BQBXW"/>
<div class="form-group">
<input autofocus="" class="bb-form-input bb-form-input-contrast" id="username" name="username" placeholder="EMAIL" type="text"/>
</div>
<div class="form-group">
<input class="bb-form-input bb-form-input-contrast" id="password" name="password" placeholder="PASSWORD" type="password"/>
</div>
<button class="btn btn-primary pink-link" type="submit">SIGN IN</button>
</form>
<hr/>
<p><a class="btn btn-primary pink-link" href="/signup/" style="text-align: center;">MAKE AN ACCOUNT</a></p>
<a class="btn btn-primary pink-link forgot-password-link-page" href="/forgot-password/">Forgot your password?</a>
</div>
</div>
</div>
</div>
<script src="https://s3-ew2-production-bb.s3.amazonaws.com/static/js/jquery-2.2.3.min.js"></script>
<script src="https://s3-ew2-production-bb.s3.amazonaws.com/static/js/bootstrap.min.js"></script>
<!--<script src="https://s3-ew2-production-bb.s3.amazonaws.com/static/js/jquery.instagramFeed.min.js"></script>-->
<script src="https://s3-ew2-production-bb.s3.amazonaws.com/static/js/smartbanner.min.js"></script>
<script src="https://s3-ew2-production-bb.s3.amazonaws.com/static/js/base.js"></script>
<script>
    $('.subscribe-dropdown').on('click tap', (e) => {
      e.preventDefault();
    });
    
    $('header #mobile-menu-icon').bind('click', () => {
      const header = $('header');
      $(header).toggleClass('active');
      return false;
    });

    $('header.active #mobile-menu-icon').bind('click', () => {
      const header = $('header');
      $(header).removeClass('active');
      return false;
    });

    $(document).ready(() => {
      
      function toggleLogin() {
        $('#loginContainer').toggle();
        $('#forgotPasswordContainer').toggle();
      }
      $('.forgot-password-link').on('click', toggleLogin);
      

      $('a, button').click(function (evt) {
        if ($(this).attr('disabled')) evt.preventDefault();
      });
    });

    function getMobileOperatingSystem() { // eslint-disable-line no-unused-vars
      const userAgent = navigator.userAgent || navigator.vendor || window.opera;
      let agent = 'unknown';
      // Windows Phone must come first because its UA also contains 'Android'
      if (/windows phone/i.test(userAgent)) {
        agent = 'Windows Phone';
      }

      if (/android/i.test(userAgent)) {
        agent = 'Android';
      }

      // iOS detection from: http://stackoverflow.com/a/9039885/177710
      if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        agent = 'iOS';
      }
      return agent;
    }
    
    // eslint-disable-next-line max-len
    
</script>
<!--<script type="text/javascript" src="https://s3-ew2-production-bb.s3.amazonaws.com/static/js/chosen.jquery.js"></script> -->
<script src="https://s3-ew2-production-bb.s3.amazonaws.com/static/js/tweenlite.min.js"></script>
<script src="https://s3-ew2-production-bb.s3.amazonaws.com/static/js/Physics2DPlugin.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.13.1/lodash.min.js"></script>
<script src="https://s3-ew2-production-bb.s3.amazonaws.com/static/js/confetti.js?v=1.01" type="text/javascript"></script>
<script type="text/javascript">
  // $(window).on('load', () => {
  //   $.instagramFeed({
  //     username: 'BongosBingo',
  //     container: '#instafeed',
  //     items: 6,
  //     styling: true,
  //     items_per_row: 4,
  //     margin: 1,
  //   });
  // });

  $(document).ready(() => {
    
    let videoId = 'full_video';

    if (Math.round($(window).width()) > 768) {
      videoId = 'video';
    }
    const video = document.getElementById(videoId);

    if (Math.round($(window).width()) > 768) {
      video.setAttribute('src', 'https://s3-ew2-production-bb.s3.amazonaws.com/media/videos/General_v4_dan_logo.mp4');
      video.setAttribute('poster', 'https://s3-ew2-production-bb.s3.amazonaws.com/media/videos/new_video_still.png');
    } else {
      video.setAttribute('poster', 'https://s3-ew2-production-bb.s3.amazonaws.com/media/videos/mobile_new_video_still.png');
    }
    video.load();
    video.addEventListener('canplay', () => {
      // Video is loaded and can be played
      video.play();
    }, false);
    // video.play();
    // $('.video-container').slideDown();
    $('.sound-toggle').on('click tap', () => {
      $(`#${videoId}`).prop('muted', !$(`#${videoId}`).prop('muted'));
    });
    
  });
</script>
</body>
</html>
