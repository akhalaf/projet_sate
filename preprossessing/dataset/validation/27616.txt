<!DOCTYPE html>
<!--[if lt IE 9 ]>    <html class="lt-ie9 no-js"  lang="en" dir="ltr"> <![endif]--><!--[if gte IE 9]><!--><html class="no-js" dir="ltr" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="Drupal 7 (http://drupal.org)" name="Generator"/>
<link href="https://3rivers.net/sites/default/files/3rivers_favicon.png" rel="shortcut icon"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<title>3 Rivers Communications</title>
<link href="https://fonts.googleapis.com/css?family=Cabin:400,400i,700,700i|Roboto:900" rel="stylesheet"/>
<style>
@import url("https://3rivers.net/modules/system/system.base.css?qml7ab");
@import url("https://3rivers.net/modules/system/system.menus.css?qml7ab");
@import url("https://3rivers.net/modules/system/system.messages.css?qml7ab");
@import url("https://3rivers.net/modules/system/system.theme.css?qml7ab");
</style>
<style>
@import url("https://3rivers.net/sites/all/modules/contrib/views_slideshow/views_slideshow.css?qml7ab");
</style>
<style>
@import url("https://3rivers.net/modules/book/book.css?qml7ab");
@import url("https://3rivers.net/sites/all/modules/contrib/calendar/css/calendar_multiday.css?qml7ab");
@import url("https://3rivers.net/sites/all/modules/contrib/date/date_api/date.css?qml7ab");
@import url("https://3rivers.net/sites/all/modules/contrib/date/date_popup/themes/datepicker.1.7.css?qml7ab");
@import url("https://3rivers.net/sites/all/modules/contrib/date/date_repeat_field/date_repeat_field.css?qml7ab");
@import url("https://3rivers.net/modules/field/theme/field.css?qml7ab");
@import url("https://3rivers.net/modules/node/node.css?qml7ab");
@import url("https://3rivers.net/sites/all/modules/contrib/picture/picture_wysiwyg.css?qml7ab");
@import url("https://3rivers.net/modules/search/search.css?qml7ab");
@import url("https://3rivers.net/modules/user/user.css?qml7ab");
@import url("https://3rivers.net/sites/all/modules/contrib/views/css/views.css?qml7ab");
@import url("https://3rivers.net/sites/all/modules/contrib/media/modules/media_wysiwyg/css/media_wysiwyg.base.css?qml7ab");
</style>
<style>
@import url("https://3rivers.net/sites/all/modules/contrib/ctools/css/ctools.css?qml7ab");
@import url("https://3rivers.net/sites/all/modules/contrib/panels/css/panels.css?qml7ab");
@import url("https://3rivers.net/sites/all/themes/tr2017/layouts/tr_home/tr_home.css?qml7ab");
@import url("https://3rivers.net/sites/all/modules/contrib/views_slideshow/views_slideshow_controls_text.css?qml7ab");
@import url("https://3rivers.net/sites/all/modules/contrib/views_slideshow/contrib/views_slideshow_cycle/views_slideshow_cycle.css?qml7ab");
@import url("https://3rivers.net/sites/all/modules/contrib/nice_menus/css/nice_menus.css?qml7ab");
@import url("https://3rivers.net/sites/all/themes/tr2017/css/style.css?qml7ab");
</style>
<style>
@import url("https://3rivers.net/sites/all/themes/tr2017/css/fix-media.css?qml7ab");
</style>
<script src="https://3rivers.net/sites/all/libraries/modernizr/modernizr.min.js?qml7ab"></script>
<script src="https://3rivers.net/sites/all/modules/contrib/jquery_update/replace/jquery/1.8/jquery.min.js?v=1.8.3"></script>
<script src="https://3rivers.net/misc/jquery-extend-3.4.0.js?v=1.8.3"></script>
<script src="https://3rivers.net/misc/jquery-html-prefilter-3.5.0-backport.js?v=1.8.3"></script>
<script src="https://3rivers.net/misc/jquery.once.js?v=1.2"></script>
<script src="https://3rivers.net/misc/drupal.js?qml7ab"></script>
<script src="https://3rivers.net/sites/all/modules/contrib/views_slideshow/js/views_slideshow.js?v=1.0"></script>
<script src="https://3rivers.net/sites/all/modules/contrib/nice_menus/js/jquery.bgiframe.js?v=2.1"></script>
<script src="https://3rivers.net/libraries/jquery.hoverIntent/jquery.hoverIntent.js?qml7ab"></script>
<script src="https://3rivers.net/sites/all/modules/contrib/nice_menus/js/superfish.js?v=1.4.8"></script>
<script src="https://3rivers.net/sites/all/modules/contrib/nice_menus/js/nice_menus.js?v=1.0"></script>
<script>document.createElement( "picture" );</script>
<script src="https://3rivers.net/libraries/jquery.cycle/jquery.cycle.all.js?qml7ab"></script>
<script src="https://3rivers.net/libraries/json2/json2.js?qml7ab"></script>
<script src="https://3rivers.net/sites/all/modules/contrib/views_slideshow/contrib/views_slideshow_cycle/js/views_slideshow_cycle.js?qml7ab"></script>
<script src="https://3rivers.net/sites/all/modules/contrib/google_analytics/googleanalytics.js?qml7ab"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-40562024-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<script src="https://3rivers.net/sites/all/themes/tr2017/js/script.js?qml7ab"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"tr2017","theme_token":"LIS_DEBuCHVbsEBP5gR1bxeB52wBC9Uw3VWoVpBOKRs","css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/contrib\/views_slideshow\/views_slideshow.css":1,"modules\/book\/book.css":1,"sites\/all\/modules\/contrib\/calendar\/css\/calendar_multiday.css":1,"sites\/all\/modules\/contrib\/date\/date_api\/date.css":1,"sites\/all\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/modules\/contrib\/date\/date_repeat_field\/date_repeat_field.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/contrib\/picture\/picture_wysiwyg.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/modules\/contrib\/media\/modules\/media_wysiwyg\/css\/media_wysiwyg.base.css":1,"sites\/all\/modules\/contrib\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/contrib\/panels\/css\/panels.css":1,"sites\/all\/themes\/tr2017\/layouts\/tr_home\/tr_home.css":1,"sites\/all\/modules\/contrib\/views_slideshow\/views_slideshow_controls_text.css":1,"sites\/all\/modules\/contrib\/views_slideshow\/contrib\/views_slideshow_cycle\/views_slideshow_cycle.css":1,"sites\/all\/modules\/contrib\/nice_menus\/css\/nice_menus.css":1,"sites\/all\/themes\/tr2017\/css\/style.css":1,"sites\/all\/themes\/tr2017\/css\/fix-media.css":1,"sites\/all\/themes\/tr2017\/css\/print.css":1},"js":{"sites\/all\/modules\/contrib\/picture\/picturefill2\/picturefill.min.js":1,"sites\/all\/modules\/contrib\/picture\/picture.min.js":1,"sites\/all\/libraries\/modernizr\/modernizr.min.js":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/jquery\/1.8\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery-html-prefilter-3.5.0-backport.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/contrib\/views_slideshow\/js\/views_slideshow.js":1,"sites\/all\/modules\/contrib\/nice_menus\/js\/jquery.bgiframe.js":1,"libraries\/jquery.hoverIntent\/jquery.hoverIntent.js":1,"sites\/all\/modules\/contrib\/nice_menus\/js\/superfish.js":1,"sites\/all\/modules\/contrib\/nice_menus\/js\/nice_menus.js":1,"0":1,"libraries\/jquery.cycle\/jquery.cycle.all.js":1,"libraries\/json2\/json2.js":1,"sites\/all\/modules\/contrib\/views_slideshow\/contrib\/views_slideshow_cycle\/js\/views_slideshow_cycle.js":1,"sites\/all\/modules\/contrib\/google_analytics\/googleanalytics.js":1,"1":1,"sites\/all\/themes\/tr2017\/js\/script.js":1}},"viewsSlideshow":{"home_banners-panel_pane_1_1":{"methods":{"goToSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"nextSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"pause":["viewsSlideshowControls","viewsSlideshowCycle"],"play":["viewsSlideshowControls","viewsSlideshowCycle"],"previousSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"transitionBegin":["viewsSlideshowPager","viewsSlideshowSlideCounter"],"transitionEnd":[]},"paused":0}},"viewsSlideshowPager":{"home_banners-panel_pane_1_1":{"bottom":{"type":"0","master_pager":"0"}}},"viewsSlideshowControls":{"home_banners-panel_pane_1_1":{"bottom":{"type":"viewsSlideshowControlsText"}}},"viewsSlideshowCycle":{"#views_slideshow_cycle_main_home_banners-panel_pane_1_1":{"num_divs":4,"id_prefix":"#views_slideshow_cycle_main_","div_prefix":"#views_slideshow_cycle_div_","vss_id":"home_banners-panel_pane_1_1","effect":"fade","transition_advanced":0,"timeout":5000,"speed":700,"delay":0,"sync":1,"random":0,"pause":1,"pause_on_click":0,"play_on_hover":0,"action_advanced":0,"start_paused":0,"remember_slide":0,"remember_slide_days":1,"pause_in_middle":0,"pause_when_hidden":0,"pause_when_hidden_type":"full","amount_allowed_visible":"","nowrap":0,"pause_after_slideshow":0,"fixed_height":1,"items_per_slide":1,"wait_for_image_load":1,"wait_for_image_load_timeout":3000,"cleartype":0,"cleartypenobg":0,"advanced_options":"{}"}},"nice_menus_options":{"delay":"800","speed":"slow"},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"urlIsAjaxTrusted":{"\/":true}});</script>
</head>
<body class="html front not-logged-in no-sidebars page-front-page-2017">
<div id="skip-link">
<a class="element-invisible element-focusable" href="#main" role="link">Skip to main content</a>
</div>
<div id="page-wrap">
<header id="header" role="banner">
<div id="pre-nav">
<span id="logo">
<a href="/" rel="home" title="3 Rivers Communications">
<img alt="3 Rivers Communications Logo" src="https://3rivers.net/sites/default/files/3rivers_logo.jpg"/> </a>
</span>
<nav id="top-menu">
<ul class="nice-menu nice-menu-down nice-menu-menu-top-menu" id="nice-menu-2"><li class="menu-3193 menu-path-node-414 first odd "><a href="/fiber-to-the-home">Fiber to the Home</a></li>
<li class="menu-3955 menuparent menu-path-nolink even "><span class="nolink" title="">My Account</span><ul><li class="menu-3957 menu-path-s3riverssmarthubcoop-loginhtmllogin first odd "><a href="https://3rivers.smarthub.coop/Login.html#login" target="_blank">SmartHub Login</a></li>
<li class="menu-4159 menu-path-s3riverssmarthubcoop-loginhtmlregistration even "><a href="https://3rivers.smarthub.coop/Login.html#registration:" target="_blank">SmartHub Registration</a></li>
<li class="menu-3958 menu-path-3riversnet-content-account-access-authorization-0 odd "><a href="http://3rivers.net/content/account-access-authorization-0">Account Access Authorization</a></li>
<li class="menu-3959 menu-path-node-560 even "><a href="/Payment%20Options/Information">Payment Options/Information</a></li>
<li class="menu-4232 menu-path-s3riverssmarthubcoop-shophtmlselectaddressandcity odd last" id="Rockcress Commons Services"><a href="https://3rivers.smarthub.coop/Shop.html#SelectAddressAndCity:" id="Rockcress Commons Services" name="Rockcress Commons Services" target="_blank" title="Rockcress Commons Services">Rockcress Commons Services</a></li>
</ul></li>
<li class="menu-3195 menuparent menu-path-nolink odd "><span class="nolink" title="">Check Your Email</span><ul><li class="menu-3196 menu-path-webmail3riversnet- first odd last"><a href="http://webmail.3rivers.net/">3 Rivers email</a></li>
</ul></li>
<li class="menu-4010 menuparent menu-path-nolink even "><span class="nolink" title="">Community</span><ul><li class="menu-3937 menu-path-calendar first odd "><a href="/calendar">Calendar</a></li>
<li class="menu-3940 menu-path-node-451 even "><a href="/lifeline-special-needs">Lifeline-Special- Needs</a></li>
<li class="menu-3960 menu-path-node-271 odd "><a href="/grants%20and%20sponsorships">Grants &amp; Sponsorships</a></li>
<li class="menu-3979 menu-path-newsletter-archives even last"><a href="/newsletter-archives" title="">Newsletter Archives</a></li>
</ul></li>
<li class="menu-3094 menuparent menu-path-node-59 odd "><a href="/About.php">About</a><ul><li class="menu-3102 menu-path-node-65 first odd last"><a href="/Corporate.php">Corporate</a></li>
</ul></li>
<li class="menu-4008 menuparent menu-path-node-10386 even last"><a href="/contact">Contact</a><ul><li class="menu-4009 menu-path-node-10386 first odd "><a href="/contact" title="">Email Support</a></li>
<li class="menu-3192 menu-path-node-91 even last"><a href="/voice/contact-numbers">Contact Numbers</a></li>
</ul></li>
</ul>
</nav>
<div class="block block-search" id="block-search-form">
<div class="content">
<form accept-charset="UTF-8" action="/" id="search-block-form" method="post"><div><div class="container-inline">
<h2 class="element-invisible">Search form</h2>
<div class="form-item form-type-searchfield form-item-search-block-form">
<label class="element-invisible" for="edit-search-block-form--2">Search </label>
<input class="form-text form-search" id="edit-search-block-form--2" maxlength="128" name="search_block_form" size="15" title="Enter the terms you wish to search for." type="search" value=""/>
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input class="form-submit" id="edit-submit" name="op" type="submit" value="Search"/></div><input name="form_build_id" type="hidden" value="form-B0Z01CXynfJpT66ncoWrD3Vq1jxojb_cqz5xw7biG18"/>
<input name="form_id" type="hidden" value="search_block_form"/>
</div>
</div></form> </div>
</div>
</div>
<nav id="main-menu">
<ul class="nice-menu nice-menu-down nice-menu-main-menu" id="nice-menu-1"><li class="menu-569 menu-path-front first odd "><a class="active" href="/" title="">Home</a></li>
<li class="menu-3191 menu-path-node-979 even "><a href="/promotions">Promotions</a></li>
<li class="menu-3112 menuparent menu-path-node-539 odd "><a href="/internetservices">Internet</a><ul><li class="menu-3113 menu-path-node-78 first odd "><a href="/internet/features">Features</a></li>
<li class="menu-3118 menu-path-node-84 even last"><a href="/internet/terms">Terms</a></li>
</ul></li>
<li class="menu-3954 menuparent menu-path-node-10008 even "><a href="/voice">Voice</a><ul><li class="menu-3104 menu-path-node-66 first odd "><a href="/CustomCallingFeatures.php">Custom Calling Features</a></li>
<li class="menu-3129 menu-path-node-98 even "><a href="/LongDistanceSupport.php">Long Distance Support</a></li>
<li class="menu-3134 menu-path-node-104 odd last"><a href="/SubscriberInformation.php">Subscriber Information</a></li>
</ul></li>
<li class="menu-3097 menuparent menu-path-node-61 odd "><a href="/business">Business</a><ul><li class="menu-3179 menu-path-node-637 first odd "><a href="/business-services/cloud-services">Cloud Services</a></li>
<li class="menu-3174 menu-path-node-535 even last"><a href="/Business-Services/Conference-Bridge">Conference Bridge</a></li>
</ul></li>
<li class="menu-4049 menu-path-node-10404 even "><a href="/support">Support</a></li>
<li class="menu-4387 menu-path-node-10637 odd "><a href="/covid-19-updates">COVID-19 Updates</a></li>
<li class="menu-4389 menu-path-node-10670 even last"><a href="/content/fairfield-construction" title="Fairfield Construction">Fairfield Construction</a></li>
</ul>
</nav>
</header>
<main id="main" role="main">
<div class="block block-system" id="block-system-main">
<div class="content">
<div class="panel-tr-home clearfix panel-display">
<div class="panel-col-top panel-panel">
<div class="inside"><div class="panel-pane pane-views-panes pane-home-banners-panel-pane-1">
<div class="pane-content">
<div class="view view-home-banners view-id-home_banners view-display-id-panel_pane_1 view-dom-id-c7c449f5419a3b62b91307292423a686">
<div class="view-content">
<div class="skin-default">
<div class="views_slideshow_cycle_main views_slideshow_main" id="views_slideshow_cycle_main_home_banners-panel_pane_1_1"><div class="views-slideshow-cycle-main-frame views_slideshow_cycle_teaser_section" id="views_slideshow_cycle_teaser_section_home_banners-panel_pane_1_1">
<div aria-labelledby="views_slideshow_pager_field_item_bottom_home_banners-panel_pane_1_1_0" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-1 views-row-first views-row-odd" id="views_slideshow_cycle_div_home_banners-panel_pane_1_1_0">
<div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd views-row-first">
<a href="http://3rivers.net/grants%20and%20sponsorships"><div class="slide"><div class="content-wrap"><div class="content-center"><div class="content-box left"><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><h2><a href="http://3rivers.net/grants%20%26%20sponsorships">3 Rivers Supports Our<br/>Local Communities</a></h2>
</div></div></div></div></div></div><div class="field field-name-field-banner-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><picture>
<!--[if IE 9]><video style="display: none;"><![endif]-->
<source sizes="100vw" srcset="https://3rivers.net/sites/default/files/styles/banner_desktop/public/banner_large/Supporting%20Local%20Communities%20copy.jpg?itok=HLIY3gQu&amp;timestamp=1590607206 1280w, https://3rivers.net/sites/default/files/banner_large/Supporting%20Local%20Communities%20copy.jpg?timestamp=1590607206 1680w"/>
<!--[if IE 9]></video><![endif]-->
<img alt="" src="https://3rivers.net/sites/default/files/banner_large/Supporting%20Local%20Communities%20copy.jpg?timestamp=1590607206" title=""/>
</picture></div></div></div></div>
</a></div>
</div>
<div aria-labelledby="views_slideshow_pager_field_item_bottom_home_banners-panel_pane_1_1_1" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-2 views_slideshow_cycle_hidden views-row-even" id="views_slideshow_cycle_div_home_banners-panel_pane_1_1_1">
<div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd">
<a href="http://3rivers.net/news/3-rivers-gig-certified"><div class="slide"><div class="content-wrap"><div class="content-center"></div></div><div class="field field-name-field-banner-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><picture>
<!--[if IE 9]><video style="display: none;"><![endif]-->
<source sizes="100vw" srcset="https://3rivers.net/sites/default/files/styles/banner_desktop/public/banner_large/GIG%20certified%20large%20banner.jpg?itok=N8Cl3fg_&amp;timestamp=1544136023 1280w, https://3rivers.net/sites/default/files/banner_large/GIG%20certified%20large%20banner.jpg?timestamp=1544136023 1680w"/>
<!--[if IE 9]></video><![endif]-->
<img alt="" src="https://3rivers.net/sites/default/files/banner_large/GIG%20certified%20large%20banner.jpg?timestamp=1544136023" title=""/>
</picture></div></div></div></div>
</a></div>
</div>
<div aria-labelledby="views_slideshow_pager_field_item_bottom_home_banners-panel_pane_1_1_2" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-3 views_slideshow_cycle_hidden views-row-odd" id="views_slideshow_cycle_div_home_banners-panel_pane_1_1_2">
<div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd">
<a href="http://3rivers.net/Payment%20Options/Information"><div class="slide"><div class="content-wrap"><div class="content-center"></div></div><div class="field field-name-field-banner-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><picture>
<!--[if IE 9]><video style="display: none;"><![endif]-->
<source sizes="100vw" srcset="https://3rivers.net/sites/default/files/styles/banner_desktop/public/banner_large/SmartHub%20--%20Oct%202020.jpg?itok=T4l8okBM&amp;timestamp=1603476321 1280w, https://3rivers.net/sites/default/files/banner_large/SmartHub%20--%20Oct%202020.jpg?timestamp=1603476321 1680w"/>
<!--[if IE 9]></video><![endif]-->
<img alt="" src="https://3rivers.net/sites/default/files/banner_large/SmartHub%20--%20Oct%202020.jpg?timestamp=1603476321" title=""/>
</picture></div></div></div></div>
</a></div>
</div>
<div aria-labelledby="views_slideshow_pager_field_item_bottom_home_banners-panel_pane_1_1_3" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-4 views_slideshow_cycle_hidden views-row-last views-row-even" id="views_slideshow_cycle_div_home_banners-panel_pane_1_1_3">
<div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd">
<a href="http://www.3rivers.net/content/3-rivers-smart-home-app"><div class="slide"><div class="content-wrap"><div class="content-center"></div></div><div class="field field-name-field-banner-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><picture>
<!--[if IE 9]><video style="display: none;"><![endif]-->
<source sizes="100vw" srcset="https://3rivers.net/sites/default/files/styles/banner_desktop/public/banner_large/3%20Rivers%20Smart%20Home%20app%20large%20banner.jpg?itok=g608Wl7M&amp;timestamp=1603724040 1280w, https://3rivers.net/sites/default/files/banner_large/3%20Rivers%20Smart%20Home%20app%20large%20banner.jpg?timestamp=1603724040 1680w"/>
<!--[if IE 9]></video><![endif]-->
<img alt="" src="https://3rivers.net/sites/default/files/banner_large/3%20Rivers%20Smart%20Home%20app%20large%20banner.jpg?timestamp=1603724040" title=""/>
</picture></div></div></div></div>
</a></div>
</div>
</div>
</div>
<div class="views-slideshow-controls-bottom clearfix">
<div class="views-slideshow-controls-text views_slideshow_controls_text" id="views_slideshow_controls_text_home_banners-panel_pane_1_1">
<span class="views-slideshow-controls-text-previous views_slideshow_controls_text_previous" id="views_slideshow_controls_text_previous_home_banners-panel_pane_1_1">
<a href="#" rel="prev">Previous</a>
</span>
<span class="views-slideshow-controls-text-pause views_slideshow_controls_text_pause views-slideshow-controls-text-status-play" id="views_slideshow_controls_text_pause_home_banners-panel_pane_1_1"><a href="#">Pause</a></span>
<span class="views-slideshow-controls-text-next views_slideshow_controls_text_next" id="views_slideshow_controls_text_next_home_banners-panel_pane_1_1">
<a href="#" rel="next">Next</a>
</span>
</div>
</div>
</div>
</div>
</div> </div>
</div>
</div>
</div>
<div class="panel-col-top2 panel-panel">
<div class="inside"><div class="panel-pane pane-fieldable-panels-pane pane-current-1 pane-bundle-icon-box">
<div class="pane-content">
<div class="fieldable-panels-pane">
<a href="/promotions">
<div class="top">
<div class="field field-name-field-icon-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><picture>
<!--[if IE 9]><video style="display: none;"><![endif]-->
<source media="(min-width: 641px)" srcset="https://3rivers.net/sites/default/files/styles/icon_image/public/box_icons/promotions.png?itok=d7NJmIjp&amp;timestamp=1497373789 1x"/>
<source media="(min-width: 851px)" srcset="https://3rivers.net/sites/default/files/styles/icon_image/public/box_icons/promotions.png?itok=d7NJmIjp&amp;timestamp=1497373789 1x"/>
<!--[if IE 9]></video><![endif]-->
<img alt="Promotions" src="https://3rivers.net/sites/default/files/styles/icon_image/public/box_icons/promotions.png?itok=d7NJmIjp&amp;timestamp=1497373789" title=""/>
</picture></div></div></div> <h2>Promotions</h2>
</div>
<div class="field field-name-field-icon-text field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><p style="margin:0px"><span style="line-height:normal"><span style="margin:0px"><font face="Calibri">Bundles, special offers and more!</font></span></span></p>
</div></div></div> </a>
</div>
</div>
</div>
<div class="panel-pane pane-fieldable-panels-pane pane-current-2 pane-bundle-icon-box">
<div class="pane-content">
<div class="fieldable-panels-pane">
<a href="/internet/services">
<div class="top">
<div class="field field-name-field-icon-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><picture>
<!--[if IE 9]><video style="display: none;"><![endif]-->
<source media="(min-width: 641px)" srcset="https://3rivers.net/sites/default/files/styles/icon_image/public/box_icons/internet.png?itok=2VBug43B&amp;timestamp=1497373829 1x"/>
<source media="(min-width: 851px)" srcset="https://3rivers.net/sites/default/files/styles/icon_image/public/box_icons/internet.png?itok=2VBug43B&amp;timestamp=1497373829 1x"/>
<!--[if IE 9]></video><![endif]-->
<img alt="Internet" src="https://3rivers.net/sites/default/files/styles/icon_image/public/box_icons/internet.png?itok=2VBug43B&amp;timestamp=1497373829" title=""/>
</picture></div></div></div> <h2>Internet</h2>
</div>
<div class="field field-name-field-icon-text field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><p style="margin:0px"><span style="line-height:normal"><span style="margin:0px"><font face="Calibri">Are you a social Internet user? More of a streamer? Or do you do it all AND have multiple family members on their own connected devices?<br/>3 Rivers has a plan to fit your needs. </font></span></span></p>
</div></div></div> </a>
</div>
</div>
</div>
<div class="panel-pane pane-fieldable-panels-pane pane-current-3 pane-bundle-icon-box">
<div class="pane-content">
<div class="fieldable-panels-pane">
<a href="/voice">
<div class="top">
<div class="field field-name-field-icon-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><picture>
<!--[if IE 9]><video style="display: none;"><![endif]-->
<source media="(min-width: 641px)" srcset="https://3rivers.net/sites/default/files/styles/icon_image/public/box_icons/phone.png?itok=m9Aeb1Dz&amp;timestamp=1497375832 1x"/>
<source media="(min-width: 851px)" srcset="https://3rivers.net/sites/default/files/styles/icon_image/public/box_icons/phone.png?itok=m9Aeb1Dz&amp;timestamp=1497375832 1x"/>
<!--[if IE 9]></video><![endif]-->
<img alt="Phone" src="https://3rivers.net/sites/default/files/styles/icon_image/public/box_icons/phone.png?itok=m9Aeb1Dz&amp;timestamp=1497375832" title=""/>
</picture></div></div></div> <h2>Voice</h2>
</div>
<div class="field field-name-field-icon-text field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><div>
<div>
<div>
<p style="margin:0px"><span style="line-height:normal"><span style="margin:0px"><font face="Calibri">3 Rivers offers local and long distance voice service and dozens of features like Caller ID and Call Forwarding.</font></span></span></p>
</div>
</div>
</div>
</div></div></div> </a>
</div>
</div>
</div>
<div class="panel-pane pane-fieldable-panels-pane pane-current-5 pane-bundle-icon-box">
<div class="pane-content">
<div class="fieldable-panels-pane">
<a href="/business">
<div class="top">
<div class="field field-name-field-icon-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><picture>
<!--[if IE 9]><video style="display: none;"><![endif]-->
<source media="(min-width: 641px)" srcset="https://3rivers.net/sites/default/files/styles/icon_image/public/box_icons/business.png?itok=S74QfJtw&amp;timestamp=1497375913 1x"/>
<source media="(min-width: 851px)" srcset="https://3rivers.net/sites/default/files/styles/icon_image/public/box_icons/business.png?itok=S74QfJtw&amp;timestamp=1497375913 1x"/>
<!--[if IE 9]></video><![endif]-->
<img alt="Business" src="https://3rivers.net/sites/default/files/styles/icon_image/public/box_icons/business.png?itok=S74QfJtw&amp;timestamp=1497375913" title=""/>
</picture></div></div></div> <h2>Business</h2>
</div>
<div class="field field-name-field-icon-text field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><div class="field field-name-field-icon-text field-type-text-long field-label-hidden">
<div class="field-items">
<div class="field-item even">
<p style="margin:0px"><span style="line-height:normal"><span lang="EN" style="margin:0px" xml:lang="EN"><font face="Calibri">3 Rivers can help you get the most out of your business phone and Internet service with consultative analysis of your existing services and solutions tailored to fit your needs. </font></span></span></p>
</div>
</div>
</div>
</div></div></div> </a>
</div>
</div>
</div>
</div>
</div>
<div class="center-wrapper clearfix">
<div class="panel-col-first panel-panel">
<div class="inside"><div class="panel-pane pane-views-panes pane-front-stories-panel-pane-1">
<div class="pane-content">
<div class="view view-front-stories view-id-front_stories view-display-id-panel_pane_1 view-dom-id-c38c8dbd4585a32a7b0db954dc5d1af7">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first">
<article class="node node-story node-promoted node-sticky node-teaser clearfix" id="node-10691" role="article">
<header>
<h2><a href="/news/browning-exchange-update" rel="bookmark">Browning Exchange Update</a></h2>
<p class="submitted">Posted Friday, January 8th, 2021</p>
</header>
<div class="content">
<div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><p style="margin-bottom:.0001pt"><span style="line-height:150%"><span style="font-size:12.0pt"><span style="line-height:150%"><span style='font-family:"Arial",sans-serif'>Siyeh Communications (SiyCom) has recently completed its purchase of the Browning Telephone/Internet Exchange from 3 Rivers Communications.</span></span></span></span></p></div></div></div> </div>
<footer>
<ul class="links inline"><li class="node-readmore first last"><a href="/news/browning-exchange-update" rel="tag" title="Browning Exchange Update">Read more<span class="element-invisible"> about Browning Exchange Update</span></a></li>
</ul> </footer>
</article>
</div>
<div class="views-row views-row-2 views-row-even views-row-last">
<article class="node node-story node-promoted node-sticky node-teaser clearfix" id="node-10686" role="article">
<header>
<h2><a href="/news/3-rivers-radioshack-permanently-closed" rel="bookmark">3 Rivers RadioShack is Permanently Closed</a></h2>
<p class="submitted">Posted Friday, November 6th, 2020</p>
</header>
<div class="content">
<div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><p><span style="font-size:12pt"><span style='font-family:"Times New Roman",serif'><span style='font-family:"Calibri",sans-serif'>3 Rivers Communications recently decided to permanently close the 3 Rivers RadioShack store in Browning, MT. The store, along with the 3 Rivers customer service office located within, has been closed since mid-March due to the COVID-19 pandemic, and the RadioShack store will not reopen.</span></span></span></p></div></div></div> </div>
<footer>
<ul class="links inline"><li class="node-readmore first last"><a href="/news/3-rivers-radioshack-permanently-closed" rel="tag" title="3 Rivers RadioShack is Permanently Closed">Read more<span class="element-invisible"> about 3 Rivers RadioShack is Permanently Closed</span></a></li>
</ul> </footer>
</article>
</div>
</div>
</div> </div>
</div>
</div>
</div>
<div class="panel-col-last panel-panel">
<div class="inside"><div class="panel-pane pane-views-panes pane-front-stories-panel-pane-1">
<div class="pane-content">
<div class="view view-front-stories view-id-front_stories view-display-id-panel_pane_1 view-dom-id-599a1385e43afc63c905c38cf2e4cf4c">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first">
<article class="node node-story node-promoted node-sticky node-teaser clearfix" id="node-10682" role="article">
<header>
<h2><a href="/news/3-rivers-smart-home-app-now-available-1" rel="bookmark">3 Rivers Smart Home App is now available!</a></h2>
<p class="submitted">Posted Thursday, October 29th, 2020</p>
</header>
<div class="content">
<div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;&lt;br /&gt;
line-height:normal;background:white"><span style='mso-fareast-font-family:"Times New Roman";&lt;br /&gt;
mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black'>Download the 3 Rivers Smart Home app for easy access to a snapshot of your home network. Through the app, you can view all the WiFi-connected devices on your network, set up basic parental controls or a guest network, change your SSID and password, and more.</span></p></div></div></div> </div>
<footer>
<ul class="links inline"><li class="node-readmore first last"><a href="/news/3-rivers-smart-home-app-now-available-1" rel="tag" title="3 Rivers Smart Home App is now available!">Read more<span class="element-invisible"> about 3 Rivers Smart Home App is now available!</span></a></li>
</ul> </footer>
</article>
</div>
<div class="views-row views-row-2 views-row-even views-row-last">
<article class="node node-story node-promoted node-sticky node-teaser clearfix" id="node-10674" role="article">
<header>
<h2><a href="/news/annual-survey-coming-your-way" rel="bookmark">Annual Survey Coming Your Way!</a></h2>
<p class="submitted">Posted Friday, September 18th, 2020</p>
</header>
<div class="content">
<div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><p>3 Rivers' Annual Customer Satisfaction Survey will be mailed out the week of September 21 to customers selected at random. The results will have a significant impact on the products and services we offer and how we serve you and the community you live in.<br/><br/>Your input is valuable to us in developing new products and services and providing a positive customer experience. The questionnaire should take about 10 minutes to complete.<br/><br/>We'd like to thank you in advance for taking the time out of your busy day to give us some feedback! </p>
<!-- End B1 -->
</div></div></div> </div>
<footer>
<ul class="links inline"><li class="node-readmore first last"><a href="/news/annual-survey-coming-your-way" rel="tag" title="Annual Survey Coming Your Way!">Read more<span class="element-invisible"> about Annual Survey Coming Your Way!</span></a></li>
</ul> </footer>
</article>
</div>
</div>
<div class="more-link">
<a href="/news-archive">
    News Archive  </a>
</div>
</div> </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</main>
<footer id="footer" role="contentinfo">
<div class="footer-wrap">
<div class="block block-block" id="block-block-1">
<div class="content">
<div class="findit"><a href="http://3riversfindit.com"><div>3riversfindit.com</div><div>3 Rivers Online Telephone Directory</div></a></div>
<p><a href="https://3rivers.net/content/privacy-statement">Privacy Statement</a><br/><a href="https://3rivers.net/content/network-support-non-discrimination-and-interconnect-policies">Network Support, Non-Discrimination and Interconnect Policies</a><br/><a href="http://www.3rivers.net/content/online-copyright-infringement-notice">Online Copyright Infringment Notice</a><br/><a href="https://3rivers.net/content/open-internet-policy-0">Open Internet Policy</a><br/><a href="/Employment.php">Employment</a></p>
<p> </p>
</div>
</div>
<div class="block block-block" id="block-block-2">
<div class="content">
<p>3 Rivers Communications<br/>202 5th St. South/P.O. Box 429<br/>Fairfield, MT 59436<br/><a href="tel:+1-406-467-2535">406-467-2535</a><br/><a href="tel:+1-800-796-4567">1-800-796-4567</a><br/>FAX: <a href="tel:+1-406-467-3490">406-467-3490</a><br/>e-mail: <a href="mailto:3rt@3rivers.net">3rt@3rivers.net</a></p>
</div>
</div>
<div class="block block-block" id="block-block-3">
<div class="content">
<p>Big Sky: <a href="tel:+1-406-995-2600">406-995-2600</a><br/>Browning: <a href="tel:+1-406-338-2535">406-338-2535</a><br/>Browning Radio Shack: <a href="tel:+1-406-338-2345">406-338-2345</a><br/>Conrad: <a href="tel:+1-406-271-2535">406-271-2535</a><br/>Shelby: <a href="tel:+1-406-424-8535">406-424-8535</a><br/>Support: <a href="tel:+1-406-467-2535">406-467-2535</a> or <a href="tel:+1-800-796-4567">1-800-796-4567</a><br/>Call Before You Dig: <a href="tel:+1-800-424-5555">1-800-424-5555</a></p>
</div>
</div>
</div> </footer>
<!-- page bottom -->
<script src="https://3rivers.net/sites/all/modules/contrib/picture/picturefill2/picturefill.min.js?v=2.3.1"></script>
<script src="https://3rivers.net/sites/all/modules/contrib/picture/picture.min.js?v=7.77"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-40562024-1"></script>
<script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-40562024-1');
      </script>
</div>
<script id="e2ma-embed">window.e2ma=window.e2ma||{};e2ma.accountId='25536';</script><script async="async" src="//dk98ddgl0znzm.cloudfront.net/e2ma.js"></script>
</body>
</html>
