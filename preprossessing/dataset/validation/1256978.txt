<!DOCTYPE html>
<html class="no-js no-svg" lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<title>saarj.com</title>
<!-- Bootstrap Core CSS -->
<!-- <link href="https://saarj.com/wp-content/themes/sarj/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /> -->
<!-- <link href="https://saarj.com/wp-content/themes/sarj/style.css" rel="stylesheet"> -->
<link href="//saarj.com/wp-content/cache/wpfc-minified/die1mfew/3y7hv.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet"/>
<!-- Custom Fonts -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
<title>HOME - South Asian Academic Research Journals</title>
<!-- This site is optimized with the Yoast SEO plugin v9.6 - https://yoast.com/wordpress/plugins/seo/ -->
<link href="https://saarj.com/" rel="canonical"/>
<meta content="en_US" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="HOME - South Asian Academic Research Journals" property="og:title"/>
<meta content='Welcome to South Asian Academic Research Journals The vision of the journals is to provide an academic platform to scholars all over the world to publish their novel, original, empirical and high quality research work. It propose to encourage research relating to latest trends and practices in international business, finance, banking, service marketing, human resource … Continue reading "HOME"' property="og:description"/>
<meta content="https://saarj.com/" property="og:url"/>
<meta content="South Asian Academic Research Journals" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content='Welcome to South Asian Academic Research Journals The vision of the journals is to provide an academic platform to scholars all over the world to publish their novel, original, empirical and high quality research work. It propose to encourage research relating to latest trends and practices in international business, finance, banking, service marketing, human resource … Continue reading "HOME"' name="twitter:description"/>
<meta content="HOME - South Asian Academic Research Journals" name="twitter:title"/>
<meta content="https://saarj.com/wp-content/uploads/2017/08/Kurukshetra-University-Images-4.jpg" name="twitter:image"/>
<script type="application/ld+json">{"@context":"https://schema.org","@type":"WebSite","@id":"https://saarj.com/#website","url":"https://saarj.com/","name":"South Asian Academic Research Journals","potentialAction":{"@type":"SearchAction","target":"https://saarj.com/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<!-- / Yoast SEO plugin. -->
<link href="//netdna.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link crossorigin="" href="https://fonts.gstatic.com" rel="preconnect"/>
<link href="https://saarj.com/feed/" rel="alternate" title="South Asian Academic Research Journals » Feed" type="application/rss+xml"/>
<link href="https://saarj.com/comments/feed/" rel="alternate" title="South Asian Academic Research Journals » Comments Feed" type="application/rss+xml"/>
<!-- <link rel='stylesheet' id='wp-block-library-css'  href='https://saarj.com/wp-includes/css/dist/block-library/style.min.css?ver=5.1.8' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='contact-form-7-css'  href='https://saarj.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.1' type='text/css' media='all' /> -->
<link href="//saarj.com/wp-content/cache/wpfc-minified/duus27qp/3y7hv.css" media="all" rel="stylesheet" type="text/css"/>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" id="font-awesome-css" media="screen" rel="stylesheet" type="text/css"/>
<!-- <link rel='stylesheet' id='stripe-handler-ng-style-css'  href='https://saarj.com/wp-content/plugins/stripe-payments/public/assets/css/public.css?ver=2.0.25' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='wpsm_tabs_r-font-awesome-front-css'  href='https://saarj.com/wp-content/plugins/tabs-responsive/assets/css/font-awesome/css/font-awesome.min.css?ver=5.1.8' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='wpsm_tabs_r_bootstrap-front-css'  href='https://saarj.com/wp-content/plugins/tabs-responsive/assets/css/bootstrap-front.css?ver=5.1.8' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='wpsm_tabs_r_animate-css'  href='https://saarj.com/wp-content/plugins/tabs-responsive/assets/css/animate.css?ver=5.1.8' type='text/css' media='all' /> -->
<link href="//saarj.com/wp-content/cache/wpfc-minified/8xppp4ic/3y7hv.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Libre+Franklin%3A300%2C300i%2C400%2C400i%2C600%2C600i%2C800%2C800i&amp;subset=latin%2Clatin-ext" id="twentyseventeen-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<!-- <link rel='stylesheet' id='twentyseventeen-style-css'  href='https://saarj.com/wp-content/themes/sarj/style.css?ver=5.1.8' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='tablepress-default-css'  href='https://saarj.com/wp-content/tablepress-combined.min.css?ver=9' type='text/css' media='all' /> -->
<link href="//saarj.com/wp-content/cache/wpfc-minified/qwpspq5v/3y7hv.css" media="all" rel="stylesheet" type="text/css"/>
<script src="//saarj.com/wp-content/cache/wpfc-minified/47p22vc/3y7hv.js" type="text/javascript"></script>
<!-- <script type='text/javascript' src='https://saarj.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script> -->
<!-- <script type='text/javascript' src='https://saarj.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script> -->
<link href="https://saarj.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://saarj.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://saarj.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.1.8" name="generator"/>
<link href="https://saarj.com/" rel="shortlink"/>
<link href="https://saarj.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fsaarj.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://saarj.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fsaarj.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<style>
.scroll-back-to-top-wrapper {
position: fixed;
opacity: 0;
visibility: hidden;
overflow: hidden;
text-align: center;
z-index: 99999999;
background-color: #2ca7f9;
color: #ffffff;
width: 50px;
height: 48px;
line-height: 48px;
right: 30px;
bottom: 30px;
padding-top: 2px;
border-top-left-radius: 0px;
border-top-right-radius: 0px;
border-bottom-right-radius: 0px;
border-bottom-left-radius: 0px;
-webkit-transition: all 0.5s ease-in-out;
-moz-transition: all 0.5s ease-in-out;
-ms-transition: all 0.5s ease-in-out;
-o-transition: all 0.5s ease-in-out;
transition: all 0.5s ease-in-out;
}
.scroll-back-to-top-wrapper:hover {
background-color: #227cb9;
color: #ffffff;
}
.scroll-back-to-top-wrapper.show {
visibility:visible;
cursor:pointer;
opacity: 1.0;
}
.scroll-back-to-top-wrapper i.fa {
line-height: inherit;
}
.scroll-back-to-top-wrapper .fa-lg {
vertical-align: 0;
}
</style> <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<style id="twentyseventeen-custom-header-styles" type="text/css">
.site-title,
.site-description {
position: absolute;
clip: rect(1px, 1px, 1px, 1px);
}
</style>
<link href="https://saarj.com/wp-content/uploads/cropped-favicon-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://saarj.com/wp-content/uploads/cropped-favicon-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://saarj.com/wp-content/uploads/cropped-favicon-180x180.png" rel="apple-touch-icon-precomposed"/>
<meta content="https://saarj.com/wp-content/uploads/cropped-favicon-270x270.png" name="msapplication-TileImage"/>
<style id="wp-custom-css" type="text/css">
.tg-wrap table td {
padding: 10px;
}
.navbar-nav {
margin: 7.5px 5%;
}
.nav > li {padding: 0px 15px;}		</style>
</head>
<body class="home page-template-default page page-id-5 twentyseventeen-front-page has-header-image page-two-column title-tagline-hidden colors-light">
<div class="container-fluid" style="margin-top: 10px;">
<div class="row text-center">
<div class="col-md-12">
<h1 style="font-family: -webkit-body;font-size:55px;text-shadow: 2px 2px #2a8fcf;"><img src="https://saarj.com/wp-content/themes/sarj/images/logo.png"/>South Asian Academic Research Journals (SAARJ)</h1>
<strong><p style="text-shadow: 1px 1px #2a8fcf;font-size:21px;">A Publication of CDL College of Education, Jagadhri.</p>
<p style="text-shadow: 1px 1px #2a8fcf;font-size:17px;">(Affiliated to Kurukshetra University Kurukshetra, Haryana, India)</p></strong>
</div>
</div>
</div>
<nav class="navbar navbar-default mega-nav">
<div class="container-fluid">
<div class="navbar-header">
<button aria-expanded="false" class="navbar-toggle collapsed" data-target="#MainMenu" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<!--<div class="collapse navbar-collapse" id="MainMenu">
<ul class="nav navbar-nav" style="font-family: 'Roboto Slab', serif;">
<li><a href="index">HOME</a></li>
<li><a href="about">ABOUT US</a></li>
<li><a href="patron">PATRON</a></li>
<li class="dropdown list-category">
<a href="journal" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">JOURNALS<i class="fa fa-angle-down" aria-hidden="true"></i>
</a>
<ul class="dropdown-menu mega-dropdown-menu">
<li><a href="Aims">AIMS & SCOPE</a></li>
<li><a href="Journal_information">JOURNALS INFORMATION</a></li>
</ul>
</li>
<li class="dropdown list-category">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Submission Guidelines<i class="fa fa-angle-down" aria-hidden="true"></i>
</a>
<ul class="dropdown-menu mega-dropdown-menu">
<li><a href="Author.html">Authors Desk</a></li>
<li><a href="Review_Process.html">Review Process</a></li>
</ul>
</li>
<li class="dropdown list-category">
<a href="Editiorial_board.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Editiorial Board<i class="fa fa-angle-down" aria-hidden="true"></i>
</a>
<ul class="dropdown-menu mega-dropdown-menu">
<li><a href="ACADEMICIA.html">ACADEMICIA</a></li>
<li><a href="SAJMMR.html">SAJMMR</a></li>
<li><a href="SJBIR.html">SJBIR</a></li>
</ul>
</li>
<li><a href="Publication_Ethics.html">Publication Ethics</a></li>
<li><a href="Publication_charges.html">Publication Policy</a></li>
<li><a href="Submit_article.html">Submit article</a></li>
<li class="dropdown list-category">
<a href="Catagoery.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Category<i class="fa fa-angle-down" aria-hidden="true"></i>
</a>
<ul class="dropdown-menu mega-dropdown-menu">
<li><a href="Business_Management.html">Commerce & Business Management </a></li>
<li><a href="Social_Science_Humanities.html">Social Science & Humanities</a></li> 
<li><a href="Information_Technology.html">Information Technology</a></li>
<li><a href="Scientific_Fields.html">Scientific Fields</a></li>
<li><a href="Education.html">Education</a></li>
</ul>
</li>
<li><a href="Contacts_us.html">Contacts us</a></li>
</ul>
</div>-->
<div class="collapse navbar-collapse" id="MainMenu"><ul class="nav navbar-nav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-51" id="menu-item-51"><a aria-current="page" href="https://saarj.com/">HOME</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52" id="menu-item-52"><a href="https://saarj.com/about-us/">ABOUT US</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-57" id="menu-item-57"><a class="dropdown-toggle" data-toggle="dropdown" href="https://saarj.com/authors-desk/">AUTHOR’S DESK <b class="caret"></b></a>
<ul class="dropdown-menu mega-dropdown-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53" id="menu-item-53"><a href="https://saarj.com/aims-scope/">AIMS &amp; SCOPE</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59" id="menu-item-59"><a href="https://saarj.com/submission-guidelines/">SUBMISSION GUIDELINES</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-58" id="menu-item-58"><a href="https://saarj.com/review-process/">REVIEW PROCESS</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-65" id="menu-item-65"><a href="https://saarj.com/publication-policy/">PUBLICATION POLICY</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4838" id="menu-item-4838"><a href="https://saarj.com/disclaimer/">DISCLAIMER</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-67" id="menu-item-67"><a class="dropdown-toggle" data-toggle="dropdown" href="https://saarj.com/category/">CATEGORIES <b class="caret"></b></a>
<ul class="dropdown-menu mega-dropdown-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-68" id="menu-item-68"><a href="https://saarj.com/commerce-business-management/">COMMERCE &amp; BUSINESS MANAGEMENT</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-72" id="menu-item-72"><a href="https://saarj.com/social-science-humanities/">SOCIAL SCIENCES &amp; HUMANITIES</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-70" id="menu-item-70"><a href="https://saarj.com/information-technology/">INFORMATION TECHNOLOGY</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71" id="menu-item-71"><a href="https://saarj.com/scientific-fields/">SCIENTIFIC FIELDS</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69" id="menu-item-69"><a href="https://saarj.com/education/">EDUCATION</a></li>
</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64" id="menu-item-64"><a href="https://saarj.com/publication-ethics/">PUBLICATION ETHICS</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-61" id="menu-item-61"><a class="dropdown-toggle" data-toggle="dropdown" href="https://saarj.com/editiorial-board/">EDITORIAL BOARD <b class="caret"></b></a>
<ul class="dropdown-menu mega-dropdown-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-60" id="menu-item-60"><a href="https://saarj.com/academicia/">ACADEMICIA</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-62" id="menu-item-62"><a href="https://saarj.com/sajmmr/">SAJMMR</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63" id="menu-item-63"><a href="https://saarj.com/sjbir/">SJBIR</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66" id="menu-item-66"><a href="https://saarj.com/submit-article/">SUBMIT ARTICLE</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73" id="menu-item-73"><a href="https://saarj.com/contacts-us/">CONTACT US</a></li>
</ul></div>
</div>
</nav>
<div class="content-area" id="primary">
<main class="site-main" id="main" role="main">
<article class="twentyseventeen-panel post-5 page type-page status-publish hentry" id="post-5">
<div class="panel-content">
<div class="wrap">
<header class="entry-header">
<h2 class="entry-title">HOME</h2>
</header><!-- .entry-header -->
<div class="entry-content">
<div><img class="img-responsive" src="https://saarj.com/wp-content/uploads/2017/08/Kurukshetra-University-Images-4.jpg" style="width: 100%;"/></div>
<div class="container">
<!-- Marketing Icons Section -->
<div class="row">
<div class="col-md-12 text-center">
<h1 class="page-header" style="font-family: 'Roboto Slab', serif;">Welcome to South Asian Academic Research Journals</h1>
<p class="font" style="font-family: 'Roboto Slab', serif; font-size: 17px; text-align: justify;">The vision of the journals is to provide an academic platform to scholars all over the world to publish their novel, original, empirical and high quality research work. It propose to encourage research relating to latest trends and practices in international business, finance, banking, service marketing, human resource management, corporate governance, social responsibility and emerging paradigms in allied areas of management including social sciences , education and information &amp; technology. It intends to reach the researcher’s with plethora of knowledge to generate a pool of research content and propose problem solving models to address the current and emerging issues at the national and international level. Further, it aims to share and disseminate the empirical research findings with academia, industry, policy makers, and consultants with an approach to incorporate the research recommendations for the benefit of one and all.</p>
 
</div>
</div>
<div class="row" style="box-shadow: 1px 1px 1px 1px; padding-top: 10px; margin-top: 20px; padding-bottom: 10px;">
<div class="col-md-9">
<h2 class="text-center" style="font-family: 'Roboto Slab'; font-weight: bold; font-size: 25px;">ACADEMICIA :
An International Multidisciplinary Research Journal</h2>
<table class="tablepress tablepress-id-4 font" id="tablepress-4">
<tbody>
<tr class="row-1">
<td class="column-1"><strong>Editor-in-Chief</strong> <br/>
<span style="font-size: 20px; color: red;"><a href="#">E-mail</a></span></td><td class="column-2"><strong>:</strong>    </td><td class="column-3">Dr. B.S.Rai<br/>
Former Principal,  G.N.Khalsa College, Yamunanagar, Haryana, India.<br/>
<span style="font-size: 20px; color: red;"><a href="#">balbirsinghrai@yahoo.ca</a></span></td>
</tr>
<tr class="row-2">
<td class="column-1"><strong>ISSN (online)</strong> </td><td class="column-2"><strong>:</strong></td><td class="column-3">2249 –7137 </td>
</tr>
<tr class="row-3">
<td class="column-1"><strong>Impact Factor</strong> </td><td class="column-2"><strong>:</strong></td><td class="column-3"><span class="impact_color"><strong>SJIF 2020 = 7.13</strong>, SJIF 2019 = 6.656, SJIF 2018 = 6.152, SJIF 2017 = 6.049, SJIF 2016 = 5.964</span></td>
</tr>
<tr class="row-4">
<td class="column-1"><strong>Language </strong> </td><td class="column-2"><strong>:</strong></td><td class="column-3">English</td>
</tr>
<tr class="row-5">
<td class="column-1"> <strong>Start Year </strong> </td><td class="column-2"><strong>:</strong></td><td class="column-3">2011</td>
</tr>
<tr class="row-6">
<td class="column-1"><strong>Publication<br/>
Frequency</strong> </td><td class="column-2"><strong>:</strong></td><td class="column-3"> Last Week Of Every Month</td>
</tr>
<tr class="row-7">
<td class="column-1"><strong>Indexed and Abstracted</strong></td><td class="column-2"><strong>:</strong></td><td class="column-3">Indian Citation index (ICI) ,Scientific Journal Impact Factor(SJIF - 7.13), Google Scholar, CNKI Scholar, EBSCO Discovery, Summon(ProQuest), Primo and Primo Central, I2OR, ESJI, IIJIF, DRJI, Indian Science and ISRA-JIF.<br/>
<br/>
</td>
</tr>
<tr class="row-8">
<td class="column-1"><strong>Published by</strong></td><td class="column-2"><strong>:</strong></td><td class="column-3">South Asian Academic Research Journals.<br/> A Publication of  CDL College of Education, Jagadhri, Haryana<br/> (Affiliated to Kurukshetra University Kurukshetra , India)</td>
</tr>
<tr class="row-9">
<td class="column-1"><span style="font-size: 20px; color: red;"><a href="https://good.thehostingserver.com:2096/" target="_blank">E-mail</a></span></td><td class="column-2"><strong>:</strong></td><td class="column-3"><span style="font-size: 20px; color: red;"><a href="https://good.thehostingserver.com:2096/" target="_blank">saarjjournal@gmail.com</a></span></td>
</tr>
</tbody>
</table>
<!-- #tablepress-4 from cache -->
<div class="col-md-3">
<a class="btn btn-lg btn-default btn-block" href="https://saarj.com/academicia-current-issue/" rel="noopener noreferrer" style="font-family: 'Roboto Slab', serif;" target="_blank">Current Issue</a>
</div>
<div class="col-md-3">
<a class="btn btn-lg btn-default btn-block" href="https://saarj.com/past-issue-acadimicia/" rel="noopener noreferrer" style="font-family: 'Roboto Slab', serif;" target="_blank">Past Issue</a>
</div>
<div class="col-md-6">
<a class="btn btn-lg btn-default btn-block" href="http://www.indianjournals.com/ijor.aspx?target=ijor:ajrm&amp;type=onlinesubmission" rel="noopener noreferrer" style="font-family: 'Roboto Slab', serif;" target="_blank">Submission Through Indianjournals.com</a>
</div>
</div>
<div class="col-md-3" style="padding-top: 16%;"><img class="img-responsive" src="https://saarj.com/wp-content/uploads/2017/08/Academcia-Title_1.jpg"/><a class="btn btn-lg btn-default btn-block special-issue" href="https://saarj.com/academicia-special-issue-2020/" rel="noopener noreferrer">Special Issue</a></div>
</div>
<div class="row" style="box-shadow: 1px 1px 1px 1px; padding-top: 10px; margin-top: 20px; padding-bottom: 10px;">
<div class="col-md-9">
<h2 style="font-family: 'Roboto Slab'; font-weight: bold; font-size: 25px;">South Asian Journal of Marketing &amp; Management Research (SAJMMR)</h2>
<table class="tablepress tablepress-id-5 font" id="tablepress-5">
<tbody>
<tr class="row-1">
<td class="column-1"><strong>Editor-in-Chief</strong><br/>
<br/>
<span style="font-size: 20px; color: red;"><a href="#">E-mail</a></span> </td><td class="column-2"><strong>:</strong>    </td><td class="column-3">Dr. Dalbir Singh<br/>
Haryana School of Business, G.J.U.S &amp; T, Hisar, Haryana, INDIA<br/>
<br/>
<span style="font-size: 20px; color: red;"><a href="#">dalbirhsb@gmail.com</a></span> </td>
</tr>
<tr class="row-2">
<td class="column-1"><strong>ISSN (online)</strong> </td><td class="column-2"><strong>:</strong>    </td><td class="column-3">2249-877X </td>
</tr>
<tr class="row-3">
<td class="column-1"><strong>Impact Factor</strong></td><td class="column-2"><strong>:</strong>    </td><td class="column-3"><span class="impact_color"><strong>SJIF 2020 = 7.11</strong>, SJIF 2019 = 6.427, SJIF 2018 = 6.206, SJIF 2017 = 5.169, SJIF 2016 = 4.926</span></td>
</tr>
<tr class="row-4">
<td class="column-1"><strong>Language         </strong> </td><td class="column-2"><strong>:</strong>    </td><td class="column-3">English</td>
</tr>
<tr class="row-5">
<td class="column-1"><strong>Start Year      </strong> </td><td class="column-2"><strong>:</strong>    </td><td class="column-3">2011</td>
</tr>
<tr class="row-6">
<td class="column-1"><strong>Publicatio  Frequency</strong></td><td class="column-2"><strong>:</strong>    </td><td class="column-3">Last Week Of Every Month</td>
</tr>
<tr class="row-7">
<td class="column-1"><strong>Indexed and Abstracted</strong> </td><td class="column-2"><strong>:</strong>    </td><td class="column-3">Indian Citation index (ICI) , Scientific Journal Impact Factor(SJIF - 7.11),  Google Scholar, Islamic World Science Citation Center (ISC) IRAN, CNKI Scholar, EBSCO Discovery, Summon(ProQuest), Primo and Primo Central, I2OR, ESJI, IIJIF, Indian Science and ISRA-JIF.</td>
</tr>
<tr class="row-8">
<td class="column-1"><strong>Published by</strong></td><td class="column-2"><strong>:</strong>    </td><td class="column-3">South Asian Academic Research Journals. <br/>A Publication of  CDL College of Education, Jagadhri, Haryana<br/> (Affiliated to Kurukshetra University Kurukshetra , India)</td>
</tr>
<tr class="row-9">
<td class="column-1"> <span style="font-size: 20px; color: red;"><a href="https://good.thehostingserver.com:2096/" target="_blank">E-mail</a></span> </td><td class="column-2"><strong>:</strong>    </td><td class="column-3"> <span style="font-size: 20px; color: red;"><a href="https://good.thehostingserver.com:2096/" target="_blank">saarjjournal@gmail.com</a></span> </td>
</tr>
</tbody>
</table>
<!-- #tablepress-5 from cache -->
<div class="col-md-3">
<a class="btn btn-lg btn-default btn-block" href="https://saarj.com/sajmmr_current-issue/" rel="noopener noreferrer" style="font-family: 'Roboto Slab', serif;" target="_blank">Current Issue</a>
</div>
<div class="col-md-3">
<a class="btn btn-lg btn-default btn-block" href="https://saarj.com/sajmmr-2/" rel="noopener noreferrer" style="font-family: 'Roboto Slab', serif;" target="_blank">Past Issue</a>
</div>
<div class="col-md-6">
<a class="btn btn-lg btn-default btn-block" href="http://www.indianjournals.com/ijor.aspx?target=ijor:ajrm&amp;type=onlinesubmission" rel="noopener noreferrer" style="font-family: 'Roboto Slab', serif;" target="_blank">Submission Through Indianjournals.com</a>
</div>
</div>
<div class="col-md-3" style="padding-top: 14%;"><img class="img-responsive" src="https://saarj.com/wp-content/uploads/2017/08/2.jpg"/><a class="btn btn-lg btn-default btn-block special-issue" href="https://saarj.com/sajmmr-special-issue-2020/" rel="noopener noreferrer">Special Issue</a></div>
</div>
<div class="row" style="box-shadow: 1px 1px 1px 1px; padding-top: 10px; margin-top: 20px; padding-bottom: 10px;">
<div class="col-md-9">
<h2 style="font-family: 'Roboto Slab'; font-weight: bold; font-size: 25px;">SAARJ Journal on Banking &amp; Insurance Research (SJBIR)</h2>
<table class="tablepress tablepress-id-6 font" id="tablepress-6">
<tbody>
<tr class="row-1">
<td class="column-1"><strong>Editor-in-Chief</strong><br/>
<br/>
<span style="font-size: 20px; color: red;"><a href="">E-mail</a></span></td><td class="column-2"><strong>:</strong>    </td><td class="column-3">Dr.Priti Pandey<br/>
Associate Professor, School of Real Estate and Infrastructure, RICS School of Built Environment, Amity University, Mumbai, INDIA<br/>
<span style="font-size: 20px; color: red;"><a href="#">ppandey@ricssbe.edu.in</a></span></td>
</tr>
<tr class="row-2">
<td class="column-1"><strong>ISSN (online)</strong></td><td class="column-2"><strong>:</strong>    </td><td class="column-3">2319 – 1422</td>
</tr>
<tr class="row-3">
<td class="column-1"><strong>Impact Factor</strong> </td><td class="column-2"><strong>:</strong>    </td><td class="column-3"><span class="impact_color"><strong>SJIF 2020 = 7.126</strong>, SJIF 2019 = 6.686, SJIF 2018 = 5.97, SJIF 2017 = 3.845, SJIF 2016 = 3.3</span></td>
</tr>
<tr class="row-4">
<td class="column-1"><strong>Language</strong></td><td class="column-2"><strong>:</strong>    </td><td class="column-3">English</td>
</tr>
<tr class="row-5">
<td class="column-1"><strong>Start Year</strong></td><td class="column-2"><strong>:</strong>    </td><td class="column-3">2012</td>
</tr>
<tr class="row-6">
<td class="column-1"><strong>Frequency</strong> </td><td class="column-2"><strong>:</strong>    </td><td class="column-3">Bi-Monthly</td>
</tr>
<tr class="row-7">
<td class="column-1"><strong>Publication</strong></td><td class="column-2"><strong>:</strong>    </td><td class="column-3">Last Week Of Every Second Month<br/>
</td>
</tr>
<tr class="row-8">
<td class="column-1"><strong>Indexed and Abstracted</strong></td><td class="column-2"><strong>:</strong>    </td><td class="column-3">Scientific Journal Impact Factor(SJIF - 7.126), Google Scholar, Islamic World Science Citation Center (ISC) IRAN,  CNKI Scholar, EBSCO Discovery, <br/> Summon (ProQuest), Primo and Primo Cent. ISRA-JIF, GIF, IIJIF.<br/>Ulrich’s Periodicals Directory (ProQuest), USA.</td>
</tr>
<tr class="row-9">
<td class="column-1"><strong>Published<br/>by</strong></td><td class="column-2"><strong>:</strong>    </td><td class="column-3">South Asian Academic Research Journals. <br/>A Publication of  CDL College of Education, Jagadhri, Haryana <br/>(Affiliated to Kurukshetra University Kurukshetra , India)</td>
</tr>
<tr class="row-10">
<td class="column-1"><span style="font-size: 20px; color: red;"><a href="https://good.thehostingserver.com:2096/" target="_blank">E-mail</a></span></td><td class="column-2"><strong>:</strong>    </td><td class="column-3"><span style="font-size: 20px; color: red;"><a href="https://good.thehostingserver.com:2096/" target="_blank">saarjjournal@gmail.com</a></span></td>
</tr>
</tbody>
</table>
<!-- #tablepress-6 from cache -->
<div class="col-md-3">
<a class="btn btn-lg btn-default btn-block" href="https://saarj.com/sjbir_current-issue/" rel="noopener noreferrer" style="font-family: 'Roboto Slab', serif;" target="_blank">Current Issue</a>
</div>
<div class="col-md-3">
<a class="btn btn-lg btn-default btn-block" href="https://saarj.com/sjbir-past-issues/" rel="noopener noreferrer" style="font-family: 'Roboto Slab', serif;" target="_blank">Past Issue</a>
</div>
<div class="col-md-6">
<a class="btn btn-lg btn-default btn-block" href="http://www.indianjournals.com/ijor.aspx?target=ijor:ajrm&amp;type=onlinesubmission" rel="noopener noreferrer" style="font-family: 'Roboto Slab', serif;" target="_blank">Submission Through Indianjournals.com</a>
</div>
</div>
<div class="col-md-3" style="padding-top: 14%;"><img class="img-responsive" src="https://saarj.com/wp-content/uploads/2017/08/3.jpg"/><a class="btn btn-lg btn-default btn-block special-issue" href="https://saarj.com/sjbir-special-issue-2020/" rel="noopener noreferrer">Special Issue</a></div>
</div>
<div class="row" style="margin-bottom: 30px; margin-top: 30px; border: solid 2px #d6d6d6; padding-top: 17px; padding-bottom: 17px;">
<div class="col-md-12">
<p style="font-family: 'Roboto Slab', serif; padding-top: 12px; color: #227cb9; font-size: 25px;"><strong style="font-size: 25px;"><a class="btn btn-lg btn-default btn-block" href="https://saarj.com/wp-content/images/2017/11/SAARJ-COPYRIGHT-FORM.pdf" rel="noopener noreferrer" style="font-size: 18px;" target="_blank">COPYRIGHT WARRANTY AND AUTHORISATION FORM</a></strong></p>
</div>
</div>
<div class="row" style="margin-bottom: 30px; margin-top: 30px;">
<div class="col-md-4 text-center" style="background-color: #227cb9;">
<h2 style="font-family: 'Roboto Slab', serif; color: #fff;">NEWS &amp; EVENT</h2>
<marquee direction="up" onmouseout="this.start()" onmouseover="this.stop()" scrolldelay="200" vscrollamount="5"><span style="font-size: 22px; font-weight: bold; color: #227cb9;">SUBMIT YOUR
RESEARCH PAPERS FOR<br/>
<span style="color: red;">January, 2021</span><br/>
ISSUE NOW<br/>
Authors can submit<br/>
research papers<br/>
for publication anytime<br/>
during this month. </span>
</marquee>
</div>
<div class="col-md-4"></div>
<div class="col-md-4 text-center" style="background-color: #227cb9; padding-bottom: 74px;">
<h2 style="font-family: 'Roboto Slab', serif; color: #fff;">COUNTER</h2>
<!-- DUPLICHECKER Code START -->
<img border="0" src="https://counter8.freecounter.ovh/private/freecounterstat.php?c=tsu5bxy75dfbmukqh74u3pu54ttzt7fd" style="padding-top: 10px;"/>
</div>
</div>
<!-- Features Section -->
<div class="row" style="margin-top: 30px; margin-bottom: 30px;">
<div class="col-md-8">
<h2 style="font-family: 'Roboto Slab', serif;">Why Choose SAARJ</h2>
<ul style="font-family: 'Roboto Slab', serif; font-size: 17px;">
<li class="why">International Journals with Impact Factor</li>
<li class="why">Refereed &amp; Blind Peer Review Process</li>
<li class="why">High &amp; Top Level Indexing / Listing</li>
<li class="why">Fast Response &amp; Rapid Review</li>
<li class="why">Global International knowledge Sharing</li>
</ul>
</div>
<div class="col-md-4">
<figure><a href="http://cdlinstitutes.com" rel="noopener noreferrer" target="_blank"><img alt="" class="img-responsive" src="https://saarj.com/wp-content/uploads/2017/08/gate.jpg" style="height: 139px; width: 100%;"/></a><figcaption><a href="http://cdlinstitutes.com" rel="noopener noreferrer" target="_blank">www.cdlinstitutes.com</a></figcaption><figcaption></figcaption></figure>
</div>
</div>
<!-- /.row -->
</div>
<!-- /.container --> </div><!-- .entry-content -->
</div><!-- .wrap -->
</div><!-- .panel-content -->
</article><!-- #post-## -->
</main><!-- #main -->
</div><!-- #primary -->
<!-- #content -->
<footer>
<div class="container">
<div class="col-md-12 text-center">
<p>Copyright © South Asian Academic Research Journals 2011 - 2021</p>
<p>A Publication of CDL College of Education, Jagadhri (Affiliated to Kurukshetra University Kurukshetra, Haryana, India)</p><p>
</p></div>
</div>
</footer>
<div class="scroll-back-to-top-wrapper">
<span class="scroll-back-to-top-inner">
<i class="fa fa-2x fa-chevron-up"></i>
</span>
</div><script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/saarj.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://saarj.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var scrollBackToTop = {"scrollDuration":"500","fadeDuration":"0.5"};
/* ]]> */
</script>
<script src="https://saarj.com/wp-content/plugins/scroll-back-to-top/assets/js/scroll-back-to-top.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpASPNG = {"iframeUrl":"https:\/\/saarj.com\/?asp_action=show_pp"};
/* ]]> */
</script>
<script src="https://saarj.com/wp-content/plugins/stripe-payments/public/assets/js/stripe-handler-ng.js?ver=2.0.25" type="text/javascript"></script>
<script src="https://saarj.com/wp-content/plugins/tabs-responsive/assets/js/bootstrap.js?ver=5.1.8" type="text/javascript"></script>
<script src="https://saarj.com/wp-includes/js/wp-embed.min.js?ver=5.1.8" type="text/javascript"></script>
<!-- Script to Activate the Carousel -->
<script>
jQuery(document).ready(function () {
//Hover Menu in Header
jQuery('ul.nav li.dropdown').hover(function () {
jQuery(this).find('.mega-dropdown-menu').stop(true, true).delay(200).fadeIn(200);
jQuery('.darkness').stop(true, true).fadeIn();
}, function () {
jQuery(this).find('.mega-dropdown-menu').stop(true, true).delay(200).fadeOut(200);
jQuery('.darkness').stop(true, true).delay(200).fadeOut();
});
});
</script>
<script>
jQuery(document).ready(function(){
jQuery(".dropdown").hover(            
function() {
jQuery('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
jQuery(this).toggleClass('open');        
},
function() {
jQuery('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
jQuery(this).toggleClass('open');       
}
);
});
</script>
<script>
jQuery(document).ready(function() {
jQuery('#media').carousel({
pause: true,
interval: false,
});
});
</script>
</body>
</html><!-- WP Fastest Cache file was created in 0.13669490814209 seconds, on 13-01-21 20:17:58 --><!-- via php -->