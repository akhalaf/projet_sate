<!DOCTYPE html>
<html data-oe-company-name="Aubretia Software Inc" data-website-id="2" lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport"/>
<title>Home Page | AUBRETIA</title>
<link href="/web/image/website/2/favicon/" rel="shortcut icon" type="image/x-icon"/>
<script type="text/javascript">
                    var odoo = {
                        csrf_token: "ccce7bc4dcb084921290e147c3fb1b0e0cb24cf5o",
                    };
                </script>
<meta content="Aubretia" name="generator"/>
<meta content="http://www.aubretia.ca/web/image/res.company/1/logo" property="og:image"/>
<meta content="http://www.aubretia.ca/home-business" property="og:url"/>
<meta content="website" property="og:type"/>
<meta content="Aubretia Software Inc" property="og:site_name"/>
<meta content="Home Page | AUBRETIA" property="og:title"/>
<meta content="http://www.aubretia.ca/web/image/res.company/1/logo" name="twitter:image"/>
<meta content="Home Page | AUBRETIA" name="twitter:title"/>
<meta content="summary_large_image" name="twitter:card"/>
<link href="http://www.aubretia.ca/" hreflang="en" rel="alternate"/>
<script type="text/javascript">
                odoo.session_info = {
                    is_admin: false,
                    is_system: false,
                    is_frontend: true,
                    translationURL: '/website/translations',
                    is_website_user: true,
                    user_id: 4
                };
                
            </script>
<link href="/web/content/5125-acdf3dd/2/web.assets_common.0.css" rel="stylesheet" type="text/css"/>
<link href="/web/content/5365-77d7aa6/2/web.assets_frontend.0.css" rel="stylesheet" type="text/css"/>
<link href="/web/content/5366-77d7aa6/2/web.assets_frontend.1.css" rel="stylesheet" type="text/css"/>
<script src="/web/content/5128-acdf3dd/2/web.assets_common.js" type="text/javascript"></script>
<script src="/web/content/5367-77d7aa6/2/web.assets_frontend.js" type="text/javascript"></script>
</head>
<body>
<div class=" " id="wrapwrap">
<header id="menu_odoo" style="display:none;">
<div class="navbar navbar-default navbar-static-top">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-top-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="/">AUBRETIA</a>
</div>
<div class="collapse navbar-collapse navbar-top-collapse">
<ul class="nav navbar-nav navbar-right" id="top_menu">
<li class="">
<a class=" " href="/#client" role="menuitem">
<span>Client</span>
</a>
</li>
<li class="">
<a class=" " href="/#services" role="menuitem">
<span>Services</span>
</a>
</li>
<li class="">
<a class=" " href="/forum" role="menuitem">
<span>Forum</span>
</a>
</li>
<li class="">
<a class=" " href="/helpdesk/rating" role="menuitem">
<span>Helpdesk Customer Satisfaction</span>
</a>
</li>
<li class="">
<a class=" " href="/contactus" role="menuitem">
<span>Contact us</span>
</a>
</li>
</ul>
<ul class="nav navbar-nav navbar-right" id="top_menu_sec">
<li class="nav-item">
<a aria-describedby="tooltip845642" class="nav-link" data-original-title="" href="mailto:info@aubretia.ca" title=""><i class="fa fa-comments"></i> Need help? </a>
</li>
<li>
<a data-original-title="" href="#contact" title=""> </a>
</li>
</ul>
</div>
</div>
</div>
</header>
<header class="header fixed clearfix">
<div class="header-container container">
<div class="header-top-panel">
<div class="row">
<div class="col-sm-12">
<div class="info-panel">
<ul class="list-inline">
<li>
<i class="fa fa-phone"></i>
<span>519-686-0095</span>
</li>
<li>
<i class="fa fa-envelope"></i>
<span>hello@aubretia.ca</span>
</li>
</ul>
</div>
<div class="login-header">
<ul class="list-inline">
<li><a href="/web/login"><i class="fa fa-key"></i> Login</a></li>
<li></li>
<li></li>
</ul>
</div>
<div class="social-icons small color-hover">
<ul class="social-links clearfix hidden-xs">
</ul>
</div>
</div>
</div>
</div>
<div class="header-wrap">
<div class="row">
<div class="col-md-12">
<div class="header-logos">
<a class="logo" href="/">
<span aria-label="Logo of Aubretia Software Inc" role="img" title="Aubretia Software Inc"><img class="img img-fluid" src="/web/image/res.company/1/logo?unique=395782c"/></span>
</a>
</div>
<div class="header-buttons">
<div class="header-top-dropdown">
<a href="#search-but"><i class="fa fa-search"></i></a>
<div class="hm-search-box" id="search-box">
<form action="/shop" method="get">
<input class="search-input" name="search" placeholder="Search" type="text"/>
<button class="btn btn-default">Search <i class="fa fa-search"></i></button>
<button class="close" type="button">×</button>
</form>
</div>
<div class="btn-group dropdown">
<button class="dropdown-toggle" data-toggle="dropdown" type="button">
<i class="fa fa-shopping-cart"></i>
</button>
<ul class="dropdown-menu dropdown-menu-right dropdown-animation cart" id="shop-cart">
<li>
<div class="alert alert-info">
          Your cart is empty!
        </div>
</li>
</ul>
</div>
<a href="/web/login"><i class="fa fa-key"></i></a>
</div>
</div>
<nav class="navbar navbar-expand-md navbar-default" role="navigation">
<div class="container-fluid">
<div class="navbar-header">
<button class="navbar-toggle" data-target="#navbar-collapse-1" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="collapse navbar-collapse" id="navbar-collapse-1">
<ul class="nav navbar-nav navbar-right">
<li class="">
<a class=" " href="/#client" role="menuitem">
<span>Client</span>
</a>
</li>
<li class="">
<a class=" " href="/#services" role="menuitem">
<span>Services</span>
</a>
</li>
<li class="">
<a class=" " href="/forum" role="menuitem">
<span>Forum</span>
</a>
</li>
<li class="">
<a class=" " href="/helpdesk/rating" role="menuitem">
<span>Helpdesk Customer Satisfaction</span>
</a>
</li>
<li class="">
<a class=" " href="/contactus" role="menuitem">
<span>Contact us</span>
</a>
</li>
</ul>
</div>
</div>
</nav>
</div>
</div>
</div>
</div>
</header>
<main>
<div class="page-wrapper oe_structure">
<section class="as-animated-slider">
<div class="carousel slide text-center banner indicators-right" data-arrow="none" data-ride="carousel" id="carousel-1">
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#carousel-1"></li>
<li data-slide-to="1" data-target="#carousel-1"></li>
</ol>
<div class="carousel-inner" role="listbox">
<div class="carousel-item active">
<div class="slide-item" style="background-image: url(/theme_impacto/static/src/img/bussiness/banner-01.jpg);">
<div class="slider-text">
<div class="container">
<div class="row">
<div class="col-md-6 offset-sm-3">
<h1 class="white-text title-extra-large">Business</h1>
<p class="white-text mb-3"> Donec vel sagittis neque, sit amet porttitor ex. Phasellus metus sem, efficitur eu accumsan eu aliquam id ligula.  </p>
<a class="btn btn-default btn-sm btn-outline white" href="#">Read More <i class="fa fa-arrow-circle-right"></i></a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="carousel-item">
<div class="slide-item" style="background-image: url(/theme_impacto/static/src/img/bussiness/banner-02.jpg);">
<div class="slider-text">
<div class="container">
<div class="row">
<div class="col-md-6 offset-sm-3">
<h1 class="white-text title-extra-large">Success</h1>
<p class="white-text mb-3"> Donec vel sagittis neque, sit amet porttitor ex. Phasellus metus sem, efficitur eu accumsan eu aliquam id ligula.  </p>
<a class="btn btn-default btn-sm btn-outline white" href="#">Read More <i class="fa fa-arrow-circle-right"></i></a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section-md">
<div class="container">
<div class="row">
<div class="col-md-8 offset-md-2 text-center">
<div class="heading-block">
<div class="sub-heading">We're Pecto Agency</div>
<h2>We Make Creative Solutions</h2>
</div>
<p>We are creative and interactive business agency, we make creative projects. View some of the most successful projects. Passion, ideas, and ambition.</p>
</div>
</div>
<div class="row mt-7">
<div class="col-md-4">
<div class="feature text-center px-2">
<div class="feature-icon theme-text"><i class="fa fa-paint-brush"></i></div>
<div class="feature-info">
<h5>Website Design</h5>
<p>Web design encompasses many different skills and disciplines in the production and maintenance of websites.</p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="feature text-center px-2">
<div class="feature-icon theme-text"><i class="fa fa-user"></i></div>
<div class="feature-info">
<h5>Customer Support</h5>
<p>It is a range of customer services to assist customers in making cost effective and correct use of a product.</p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="feature text-center px-2">
<div class="feature-icon theme-text"><i class="fa fa-cog"></i></div>
<div class="feature-info">
<h5>Development</h5>
<p>The process of economic and social transformation that is based on complex cultural factors and their interactions.</p>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section-md grey-bg">
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="heading-block">
<div class="sub-heading">ABOUT US</div>
<h2>Unlimited Skills for Super <br/>Projects</h2>
</div>
<p class="lead">Typical ad agency clients include businesses and corporations, non-profit organizations and private agencies.</p>
<p>Lorem ipsum dolor s it amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ipsum dolor sit amet, consectetur elit adipisicing elitLorem ipsum dolor s it ametr</p>
<div class="mt-3">
<a class="btn btn-default" href="#">Ream more</a>
<a class="btn btn-default btn-outline" href="#">Know more</a>
</div>
</div>
<div class="col-md-6">
<div class="row">
<div class="col-md-7">
<img alt="" class="img-fluid img-shadow" src="/theme_impacto/static/src/img/bussiness/img-01.jpg"/>
</div>
<div class="col-md-5">
<img alt="" class="img-fluid mt-4 img-shadow" src="/theme_impacto/static/src/img/bussiness/img-02.jpg"/>
<img alt="" class="img-fluid mt-3 img-shadow" src="/theme_impacto/static/src/img/bussiness/img-03.jpg"/>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section-md theme-bg" style="background-image: url('/theme_impacto/static/src/img/bussiness/bg-03.png'); background-size: cover; background-repeat: no-repeat; background-position: center center;">
<div class="container">
<div class="row">
<div class="col-md-6 offset-md-6">
<div class="heading-block">
<div class="sub-heading">WHAT WE DO</div>
<h2>We Make Websites for Great Companies</h2>
</div>
<p>We make websites are the number one ranked design, build and marketing team. We build custom, unique, creative and beautiful websites that will make you more successful.</p>
<ul class="list round white-text mt-2">
<li>We provide free initial consultation and support.</li>
<li>We work with some of the most successful businesses.</li>
<li>We have the professional designers team.</li>
<li>We provide free initial consultation and support.</li>
</ul>
</div>
</div>
</div>
</section>
<section class="section-md dark-bg pos-r" style="background: url('/theme_impacto/static/src/img/bussiness/bg-01.jpg') no-repeat 0 0; background-size: cover; padding-bottom: 150px;">
<div class="container">
<div class="row">
<div class="col-md-8 offset-md-2 text-center">
<a class="videopopup-youtube" href="http://www.youtube.com/watch?v=7HKoqNJtMTQ" title="Video">
<img alt="" src="/theme_impacto/static/src/img/icon-play-white.png"/>
</a>
</div>
</div>
<div class="row">
<div class="col-md-8 offset-md-2 text-center">
<div class="heading-block">
<h2>We Are Entrepreneurs &amp; Innovators <br/>of Website Building</h2>
</div>
</div>
</div>
</div>
</section>
<section class="section-md pt-0 grey-bg">
<div class="container box-top box-shadow">
<div class="row no-gutter">
<div class="col-md-4 white-bg">
<div class="feature pxy-5 text-center">
<div class="feature-icon">
<i class="fa fa-spinner"></i>
</div>
<div class="feature-info">
<h5 class="title">Product Research</h5>
<p>It is a vital part of developing new products, helping you avoid expensive mistakes.</p>
</div>
</div>
</div>
<div class="col-md-4 theme-bg">
<div class="feature pxy-5 text-center">
<div class="feature-icon white-text">
<i class="fa fa-wifi"></i>
</div>
<div class="feature-info">
<h5 class="title">Design &amp; Development</h5>
<p>Encompasses many skills and disciplines in the production and service of websites.</p>
</div>
</div>
</div>
<div class="col-md-4 dark-bg">
<div class="feature pxy-5 text-center">
<div class="feature-icon white-text">
<i class="fa fa-bell-slash"></i>
</div>
<div class="feature-info">
<h5 class="title">Testing &amp; Support</h5>
<p>Testing is the name given to software testing that focuses on web applications.</p>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section-md grey-bg pt-0">
<div class="container">
<div class="row">
<div class="col-md-5">
<div class="heading-block">
<div class="sub-heading">CONTACTS</div>
<h2>We Have Offices Allround <br/>the World</h2>
</div>
<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great.</p>
<div class="mt-3">
<a class="btn btn-default" href="#">Ream more</a>
</div>
</div>
<div class="col-md-6 offset-md-1">
<img alt="" class="img-fluid" src="/theme_impacto/static/src/img/creative/map.png"/>
</div>
</div>
</div>
</section>
<section class="section-md">
<div class="container">
<div class="row">
<div class="col-md-8 offset-md-2 text-center">
<div class="heading-block">
<div class="sub-heading">Team</div>
<h2>Our Creative Staff</h2>
</div>
<p class="mb-2 lead">We work to make your business start effectively working for you. Meet the financial and marketing specialists. These guys create a magic.</p>
</div>
</div>
<div class="row mt-4">
<div class="col-md-4">
<div class="team text-center">
<div class="member-image">
<img alt="" class="img-fluid" src="/theme_impacto/static/src/img/corporate/team-01.jpg"/>
</div>
<div class="team-info">
<h4 class="member-name l-space-1">Antonio Creighton</h4>
<h6 class="member-position">CREATIVE DIRECTOR</h6>
<div class="social">
<a href="#"><i class="fa fa-facebook"></i></a>
<a href="#"><i class="fa fa-twitter"></i></a>
<a href="#"><i class="fa fa-linkedin"></i></a>
<a href="#"><i class="fa fa-google-plus"></i></a>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="team text-center">
<div class="member-image">
<img alt="" class="img-fluid" src="/theme_impacto/static/src/img/corporate/team-02.jpg"/>
</div>
<div class="team-info">
<h4 class="member-name l-space-1">Samanta Jonson</h4>
<h6 class="member-position">PARTNER &amp; CEO</h6>
<div class="social">
<a href="#"><i class="fa fa-facebook"></i></a>
<a href="#"><i class="fa fa-twitter"></i></a>
<a href="#"><i class="fa fa-linkedin"></i></a>
<a href="#"><i class="fa fa-google-plus"></i></a>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="team text-center">
<div class="member-image">
<img alt="" class="img-fluid" src="/theme_impacto/static/src/img/corporate/team-03.jpg"/>
</div>
<div class="team-info">
<h4 class="member-name l-space-1">Frederick Smith</h4>
<h6 class="member-position">DIGITAL DIRECTOR</h6>
<div class="social">
<a href="#"><i class="fa fa-facebook"></i></a>
<a href="#"><i class="fa fa-twitter"></i></a>
<a href="#"><i class="fa fa-linkedin"></i></a>
<a href="#"><i class="fa fa-google-plus"></i></a>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="o-hidden">
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
<div class="isotope no-padding columns-4 popup-gallery">
<div class="grid-item">
<div class="portfolio-item">
<img alt="Design" src="/theme_impacto/static/src/img/gallery/gallery-1.jpg"/>
<div class="portfolio-overlay">
<h4><a href="#">Creative Digital Solutions</a></h4>
<span class="theme-text">BRANDING</span>
</div>
<a class="popup popup-img" href="/theme_impacto/static/src/img/gallery/gallery-1.jpg" title="Project"><i class="fa fa-arrow-right"></i></a>
</div>
</div>
<div class="grid-item">
<div class="portfolio-item">
<img alt="Design" src="/theme_impacto/static/src/img/gallery/gallery-2.jpg"/>
<div class="portfolio-overlay">
<h4><a href="#">App Development</a></h4>
<span class="theme-text">BRANDING</span>
</div>
<a class="popup popup-img" href="/theme_impacto/static/src/img/gallery/gallery-2.jpg" title="Project"><i class="fa fa-arrow-right"></i></a>
</div>
</div>
<div class="grid-item">
<div class="portfolio-item">
<img alt="Design" src="/theme_impacto/static/src/img/gallery/gallery-3.jpg"/>
<div class="portfolio-overlay">
<h4><a href="#">Portrait Printing</a></h4>
<span class="theme-text">BRANDING</span>
</div>
<a class="popup popup-img" href="/theme_impacto/static/src/img/gallery/gallery-3.jpg" title="Project"><i class="fa fa-arrow-right"></i></a>
</div>
</div>
<div class="grid-item">
<div class="portfolio-item">
<img alt="Design" src="/theme_impacto/static/src/img/gallery/gallery-4.jpg"/>
<div class="portfolio-overlay">
<h4><a href="#">Office Stationery</a></h4>
<span class="theme-text">BRANDING</span>
</div>
<a class="popup popup-img" href="/theme_impacto/static/src/img/gallery/gallery-4.jpg" title="Project"><i class="fa fa-arrow-right"></i></a>
</div>
</div>
<div class="grid-item">
<div class="portfolio-item">
<img alt="Design" src="/theme_impacto/static/src/img/gallery/gallery-5.jpg"/>
<div class="portfolio-overlay">
<h4><a href="#">Booklet Printing</a></h4>
<span class="theme-text">BRANDING</span>
</div>
<a class="popup popup-img" href="/theme_impacto/static/src/img/gallery/gallery-5.jpg" title="Project"><i class="fa fa-arrow-right"></i></a>
</div>
</div>
<div class="grid-item">
<div class="portfolio-item">
<img alt="Design" src="/theme_impacto/static/src/img/gallery/gallery-6.jpg"/>
<div class="portfolio-overlay">
<h4><a href="#">Business Arrangements </a></h4>
<span class="theme-text">BRANDING</span>
</div>
<a class="popup popup-img" href="/theme_impacto/static/src/img/gallery/gallery-6.jpg" title="Project"><i class="fa fa-arrow-right"></i></a>
</div>
</div>
<div class="grid-item">
<div class="portfolio-item">
<img alt="Design" src="/theme_impacto/static/src/img/gallery/gallery-7.jpg"/>
<div class="portfolio-overlay">
<h4><a href="#">Embroidered Packaging</a></h4>
<span class="theme-text">BRANDING</span>
</div>
<a class="popup popup-img" href="/theme_impacto/static/src/img/gallery/gallery-7.jpg" title="Project"><i class="fa fa-arrow-right"></i></a>
</div>
</div>
<div class="grid-item">
<div class="portfolio-item">
<img alt="Design" src="/theme_impacto/static/src/img/gallery/gallery-8.jpg"/>
<div class="portfolio-overlay">
<h4><a href="#">Business Cards</a></h4>
<span class="theme-text">BRANDING</span>
</div>
<a class="popup popup-img" href="/theme_impacto/static/src/img/gallery/gallery-8.jpg" title="Project"><i class="fa fa-arrow-right"></i></a>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="dark-bg section-lg" style="background: url('/theme_impacto/static/src/img/bussiness/bg-02.jpg') no-repeat 0 0; background-size: cover;">
<div class="container-fluid">
<div class="row">
<div class="col-md-10 offset-md-1">
<div class="carousel slide" data-ride="carousel" id="team-carousel-5">
<div class="carousel-inner">
<div class="carousel-item">
<div class="row">
<div class="col-md-8 offset-md-2">
<div class="testimonials text-center">
<i class="fa fa-quote-left round"></i>
<div class="author-description f-weight-300">Your customer support is the best I have experienced with any theme I have purchased. You have a theme that far exceeds all others. Thanks for providing such a fantastic theme, all your efforts are greatly appreciated on our end.</div>
<div class="author-img my-2"><img alt="" class="img-fluid rounded-circle center-block" src="/theme_impacto/static/src/img/testimonials/image1.jpg"/></div>
<span class="author-name">- John Doe </span><br/>
<label>Company CEO</label>
</div>
</div>
</div>
</div>
<div class="carousel-item active">
<div class="row">
<div class="col-md-8 offset-md-2">
<div class="testimonials text-center">
<i class="fa fa-quote-left round"></i>
<div class="author-description f-weight-300">Your customer support is the best I have experienced with any theme I have purchased. You have a theme that far exceeds all others. Thanks for providing such a fantastic theme, all your efforts are greatly appreciated on our end.</div>
<div class="author-img my-2"><img alt="" class="img-fluid rounded-circle center-block" src="/theme_impacto/static/src/img/testimonials/image2.jpg"/></div>
<span class="author-name">- John Doe </span><br/>
<label>Company CEO</label>
</div>
</div>
</div>
</div>
</div>
<a class="left carousel-control" data-slide="prev" href="#team-carousel-5">
<i class="fa fa-long-arrow-left"></i>
</a>
<a class="right carousel-control" data-slide="next" href="#team-carousel-5">
<i class="fa fa-long-arrow-right"></i>
</a>
</div>
</div>
</div>
</div>
</section>
</div>
</main>
<footer class="bg-light o_footer">
<div class="container hidden-print" id="footer" style="display:none;">
<div class="row">
<div class="col-md-4 col-lg-3">
<h4>Our Products &amp; Services</h4>
<ul class="list-unstyled" id="products">
<li>
<a href="/">Home</a>
</li>
</ul>
</div>
<div class="col-md-4 col-lg-3" id="info">
<h4>Connect with us</h4>
<ul class="list-unstyled">
<li>
<a href="/page/website.contactus">Contact us</a>
</li>
</ul>
<ul class="list-unstyled">
<li>
<i class="fa fa-phone"></i>
<span>519-686-0095</span>
</li>
<li>
<i class="fa fa-envelope"></i>
<span>hello@aubretia.ca</span>
</li>
</ul>
<h2>
</h2>
</div>
<div class="col-md-4 col-lg-5 col-lg-offset-1">
<h4>
<span>Aubretia Software Inc</span>
<small>-
                                    <a href="/page/website.aboutus">About us</a>
</small>
</h4>
<div>
<p>
                                    We are a team of passionate people whose goal is to improve everyone's
                                    life through disruptive products. We build great products to solve your
                                    business problems.
                                </p>
<p>
                                    Our products are designed for small to medium size companies willing to optimize
                                    their performance.
                                </p>
</div>
</div>
</div>
</div>
<footer class="site-footer">
<div class="footer-top">
<div class="container">
<div class="row">
<div class="col-md-4">
<a class="logo" href="/">
<span aria-label="Logo of Aubretia Software Inc" role="img" title="Aubretia Software Inc"><img class="img img-fluid" src="/web/image/res.company/1/logo?unique=395782c"/></span>
</a>
<div class="about-text">Lorem ipsum dolor sit amet, consectetur adipi sicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim, Etiam mollis libero.</div>
<ul class="social-link ">
<li class="facebook">
</li>
<li class="twitter">
</li>
<li class="googleplus">
</li>
<li class="linkedin">
</li>
<li class="youtube">
</li>
</ul>
</div>
<div class="col-md-2">
<h6 class="footer-title">Navigation</h6>
<ul class="footer-ul footer-menu">
<li>
<a href="#">Home</a>
</li>
<li>
<a href="#">About Us</a>
</li>
<li>
<a href="#">Services</a>
</li>
<li>
<a href="#">Blog</a>
</li>
<li>
<a href="#">Contact us</a>
</li>
</ul>
</div>
<div class="col-md-3">
<h6 class="footer-title">Usefull Link</h6>
<ul class="footer-ul footer-menu">
<li>
<a href="#">Create Account</a>
</li>
<li>
<a href="#">Faq</a>
</li>
<li>
<a href="#">Team</a>
</li>
<li>
<a href="#">Privacy Policy</a>
</li>
<li>
<a href="#">Terms &amp; Conditions</a>
</li>
</ul>
</div>
<div class="col-md-3">
<h6 class="footer-title">Contact Info</h6>
<ul class="footer-ul contact-info">
<li>
<i class="fa fa-map-marker"></i>
<span>607 Queens Ave</span>
<span>London</span>
<span>Canada</span>
</li>
<li>
<i class="fa fa-phone"></i>
<span>519-686-0095</span>
</li>
<li>
<i class="fa fa-envelope-o"></i>
<span>hello@aubretia.ca</span>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="site-info">
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 text-center col-lg-12">© Copyright 2018 All Rights Reserved.</div>
</div>
</div>
</div>
</footer>
<div id="to-top">
<a class="img-circle" href="#">
<i class="fa fa-chevron-up"></i>
</a>
</div>
</footer>
</div>
</body>
</html>
