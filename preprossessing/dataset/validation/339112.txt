<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "78168",
      cRay: "610a602eedd2020a",
      cHash: "059d56f7a2f9fdc",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuY29hbHJpdmVyY29hY2hlcy5jb20uYXUv",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "jTFZQP+oJJ7gz0rw198uIe+na/d6OubLIhjBn3R7NvvhkBXH8LU9/23EZU6YM/+tdhHCM9IMnBdNu8D8dDGsJJDmkqtQhkP3HsVghUeK/Ke8uTRd7zBi5fNDEA+o1kv9PK8EUyx8BgJXuXb/KOSsJ47DH5vVUkME+e/kvGz7yjbPNIfdxV72mx9DIt5b0Qudiduqs/MfZW6N4Usm4mw3n6VsKXfsqiLtyY2z0wzLu+WJTQWrr2KhywexV7C9HYp8VvlITQsFyrZjqT1cYU64+Q4buRBgsLBjSOcUeaRu5TDf3e9WfHoryL8/XMJ24ul9w/pBU6Ukhbr/JQreJ30vI7x4u2dx9jOoSVxXqlck7jPzsPHzE2NAeX+rWR+DP2g+s0Vl6pqdr6iQ9kEJhWHBbqYw9Ind+ZlZmkafXQjqn4WDRSy/3oUoigsTWbIZ77r40iEkszv3LQlzQD4V4nMgdWI0FEuOjjlk4gtx0brZ/DgEK4OnBKoKPzhp0HVya6562EMmCK6uGKlB8LhMPTOI5JDC495vS+8lQiM30+F63bVQd6WtXFf+zbj1oCI+IQx37GOEEt91kl50dfLu36sJUMBJlm/zsB1RCVXx6vw9lKm80tQMNr2A/Ge5Pp5RAj+n9PnOiTzwQBdmslb36aEJirwkAQs7ZeSLSJFWGm4XMz4xQjpyXvu0Rd/YSDrPImDbPT8Cq8v27Hb0PzzdL0d4d5kRaB9FaguLnV/Eyl42RRqKAIbYt5wH4pMc9oDgSA4O",
        t: "MTYxMDQ5MTE4OS41ODkwMDA=",
        m: "qZWfbMzWy4iUoG1Cqi5wfIIMbG0P67JTOmcJzkrmmeQ=",
        i1: "sJNtaRjqYeRYeMOG3rAxZA==",
        i2: "Av3K7b1JUomATQ1s9Lh0bg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "DHD+g073DfNZBOUmdc1sF90JngIiRIjAcPz9969mBWs=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610a602eedd2020a");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> coalrivercoaches.com.au.</h1>
<div style="display: none;"><a href="https://robinsonsdrlg.com/direct.php?tag=7">table</a></div>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=e667d1e14100d28370b365a6df4bf0932b984b1e-1610491189-0-AXbAhn6EuDMVXFIjygqXyjO6K-kR87RxtzW9ACo1SDekfJEc6tghlzbWN0_y4dGbUbfreMPxQGz2VE7HheoFrhgUzl7pdVG4igS7sqdcHSECH8qswIUQdotSWHaeoVWGZ4y21buY9UACyjjRzUVLhjOxO3ew_1MuT2bcUNHGJM4sMO8q-yn0iMNDEP2h5IOLU4yPkOtsvio3K0qVqe4ldNuDRRcNWTOqaqU50C9pecl4GkA8SJtn85R_8ilVo4YBJjhC1PffQa4eMFQV0o63u_q4he3wiuR52T2hIGNVbWjQtuF0QkxYorl23nwnJKqnkg" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="b327be6fe3247e76990fbf71868fd3628e7cb68b-1610491189-0-AZQZZfTWwJTwfigZCEjnaPnM6otOMcHLIYXoZUDwTXkNjthQv3ujBUQ3t//es7iHz7uaibYHuVM+O9Mmsd3k7IjpRutENYuaxhOR293mHNqx7XUOHHc8C/nS70nSdMauYl0Lo1v37V2crjEi1GUbj3YW4MSASJjAVRetMOMws4ICzcZT4llD6fM9K+BwhhttbFCCsta0alwt4EWkagMUWGQ929SwrNzPGTlAT2yEP3wvIOvvndRf1LyFM9TMUKxuA12SNUXnfC1adcMtVQe6O1hz6wDhaIjYFkvwhvEjyzkLT5whpDkkWwGSQrekS7sZ+47iswiE7pUEKOiKCkHAuAGDjyZnKzx11qdwxRHNKBeoYi1GKq7LZiLj8r+CA5ho/N2byaDgTYb0z5OhmPLvLMsxcLab+vnyGMqShg/BHxv9OGcnQSCvT1QWA/f3DXOdmdNGfBu2xl5XcLEpxssP/xcJN5Pfjc1tKAQenGwl3dwUP9cSLbkjBfU4A/CIjq+IlBYiNBQRIveS1JOhksF8gGAHusL72sdd/+0DpyVzEESebsiQdlKsivDCgPndRcimkNlUIhKzSWgwNIS2GiJi9iQvS7HOZn4ighz1Ggt1sYZwgE0ZAFaVpZZjtOWWvCMTmxGSDmihnm20TuDkmfP6F1SKWKa9K4ggLygzLAU93SIkR1WHiPDOhLuFeV3+kYIwehhACA/TctlsdKh1yyKgddvnZRCy1+tauilX810yIAxeEHuhFT5F2EqrrYY6FOgBZAVHWDMxQrfRGjICi/8kM6lwBEQgMtQ/OQSGCMAncPtwPqwEAlE93+QUMrsDVpkLTP/xg/QbbxGwkk1Km4dFGZ1VHw3GXbLlDf5zO6NWW8hyafdwgoiyxGKeZ8eLMaVrwL+LMBXbSfJdQ0TjlZlFFtJxSjzZr+5SJO7Jzf/UKwC/nY11NZm+2YI8KK3UQKT//wld6Hj8Z73tf+hBItukm/bLQRYslPTIPk8rGUsSv/MeJeDMcsilvfCaKX6k3AxsY42Oou75Z1ifE/WWWDVcqC2NTkiwsMWEnTj0PQrNTmacOsUWKf7JkVHPGDbr7V3UAW2RfmrWjZGEAQSeonKJjI6USkNmQXGakyc8yw+EBX9lZTY2WVXPGw9eAwP04faN2nFgX+r1hGS1Uemkdqa2d66mD+MJktEu7KZfQjbcSqjToD1aI86r02eMGEstDSMGwtFd3eqHniJH1Z/esH1FqoViukXKFMAy8C1luMsRwCagLpTm6IMzyEQP6LwKp7f+3QU1znYj/ho0AOmQnbW5XYItFNiD5XUu7uAtxJIfqvm6wF/p0j0kXJSBa8qTKymrcnE2cLCNMo3njRABnAVhxnQ="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="bf3c47545d43a3a709b35d4e20122ca9"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610491193.589-mcz5XSD7sm"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610a602eedd2020a')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610a602eedd2020a</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
