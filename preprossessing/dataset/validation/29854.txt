<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "90188",
      cRay: "610b26bc6d81c34a",
      cHash: "9a921a410ce447d",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuNGQ4OC5jb20v",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "bZ6tE1K2eag7W9sav8ZjyUjNf+ozEWoFWkzO98WD3c2KwpSwptrkK76i3mtwvBtxDnpRv6aekIjkTBkdLWSZ4DEEwVIKN2SKuR0mJommcZhURkgH+wBcLW3NUV2XjTGImB7fqUHP531KmDlJBJ604crPFTrORwxExANGtuME6287grCITCbFrEfetnxIciuPUkzD1v9YaYyMi5M0Dmge5oQ8ymaGDYIATcP6Zdt8aIk9l6s1901Y2aPQkipR0lvT9W064i/W/NxOC8Me4/X/vqGPQkEIve3sqgUH7c36kvvpo+5YLpExW8dSCTBZkYvOJInuMdhM8IPt2iwAcnCbK/7iT1X7BM1BEk4tR1x/rFrbAQ42OYPHbLnkNHKPovrNOEU/lwCAKw4yoK0zXHRMgTe0oNOVrrFtE3LYHXb6dAIFsj6qkpl+jWvtb6oI8rN+qwZF+oBEJqyDG5WZzclf0I5H/TF3QNyLN3HcCD+L36DptmgXkQAV6u1IcIZOcWq4Nzlq3r7OBdP/871wwZLG36Pb4J3kQBENbeNg7enA1Wrcd0X/SSyrwD9g23fowd2sacfknZqH6rwExOfOaXxhMrTVye7i3n3GeKyT+VF6YebWCjovavGUKDVkb4wcwRIbxeBuVWMt7ldr49lY53pKrK9xf1p9SZFWLM5662xy29OHJsGkyuDJj4WMPpI8PdTBr42PScZhXHcEXBEyf3Avv84bw3t6lfDuVL9m0Qap1WSilhsaHBn6FaGgNbCUfyzw",
        t: "MTYxMDQ5OTMyMi4zMTAwMDA=",
        m: "HS0rmsLpmRAU/TcEwcoHxEB6OPy0khkL4VQ+AstYtUY=",
        i1: "6Yn6gF9SE4S6ojlsl9eTlg==",
        i2: "or+73mI3t/WIodmv2oG0PA==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "/ozu7FLdQEItln4R73y/0V+qX6g6A/B7YDu8w/UwikA=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610b26bc6d81c34a");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> 4d88.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<!-- <a href="https://salvagepc.com/composer.php?add=6089">table</a> -->
<form action="/?__cf_chl_jschl_tk__=cfa932cbbb627f007cd243fc8f7d92df428f5271-1610499322-0-AdfvbECT8LE9cO98jxZL5_0fZBGWxtrHVfxv3YnbcCFWjzVFkWHgMR0dMyQ-s0_68rK30VGUV0jpylipOzyFADfJINSeOOPrL2gMouMdRqg9DQ4hRm3Hz9ZXXPgnRHjPVNKMe4YyRQuK0-koaZfpd3ykLKocwnhKkdKVTMoosN9scPZEfVh30gYHprG9QP_pn0sk0J7jJpv5v3UCiUg7e71TYZ8CoN4JI6PX_C997lQkeBVyDmAG9fZ2rMk-FPBT2_H9TbEA4pRJWyxDmU4xYpHAvZuWF8fZImNS4EWZfPCCNW2uXgQPXBZSlAcA5vLbVQ" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="dcb376bb955868961a2e6dd8e83a2c4900be14ea-1610499322-0-AWHZMwHGCwNQ9NWczLMCn6keY81XbdG+hAWUhfO8RWEQTNpVysF4xzOlRBvNDB6qJW0KVx9AOyS8SI5rmWcLLvi/MaPApnWRvsuJI2fyW/QDitXb0uafm+P3Klq/U+MAY7kiIcD1w8qx89k/u4LV07/M7g5jTjQc5QlOn1BtUAZYXpCe/1P0udHzrd0hzOg/LJQaB2kmP/aD142rNNvjSkEkADQH3tGYfUMCQ2DByEFJgKnqXu9W+UKvdCJCnqYSW7ntRu52Q9CWv0HZ72+iixiG0Mh5uUIjh0fSUubEoeDZEWmE+QX5LEC648X+Q1ardJVAXshEve49o1vZRWTkXnEE1XdZiVzz8rB8UJGbJ96LMP8wXLotBFLYJST0ENDK13iIiN3CZdzBmQossF8RD3vIv/X7PVQdbOvScWNVsvRL8apv0b8EbhtE5epm/ArGpNaZgo215YAapsfsbFDv4M8Ta7Gzw/MIKEcqa56n6naSigqoWGwb3RCuvFzX+GzZp0DplPTjJJrtHc6BZyHh6Q0nkwvXMum+UD9CM213sKDUCItaPU+xgvdRXM65vJzSsDq8sqKZVUmnOuF7jlEHfoBDzvIckHUe2kx+Q36BgDo9CY4094a1Ud1NOes3UpFpt1scLHN/fwPrspsiW/8lPW0j+zFp7Nk/3ayH8t651X4gapkUoFQ9C1VAEYo5/Z9+8bQNPVoOFDOujbE6ddHNj38x9fjc4u3VE76p9yNaWwwk0GI8yGd2cn9L2LOHvXdIILo+EOGXBf0iKubMrHhPmFh5kMnHPn2X3GywF6zQhkzA/v4rdkaQmT9AQCpx9RuVo3N4sRsLQIRdV/YUsasARrxDaTkM1/wL1osyn26ps61JwucuA+9BRzszWXi0m+/iw9ZqUFLz68Qz9qKCLWYZydINZ/Yj8thiSvqfaNV8S51T/ShWKFDZrrwWTdMVY5LTN9NpZS+eqz1TX/mpTvzfabfiKICKHHnCgCY7LqZCWhJe3jRX5WQ79yr4LSzrLbsbN7xs2olbsrxzomD+RLrjLUDcklepq71V6DJoOQPyws925Od0ujxbGfqzNlp9mJnW0l3O1MqXOJLwjJceAhGT6X/NgJJ7bCQxsnrYOCxxG8KDEdOCEaNhDFPO6958oZP+BsDiEIPRkz32KYLvdzEfPrKQ+4tk/OP+E4cqL7rrdwP3NJtBbKDZfbH9Qttwqy5mL8V4aeFPPg0l/ZUKVO/Apz1s5piQpR1jHdUdS1tpZX+KBaT7mHJBs7uykNlGW/zd7Q4nyPS0Xty7UtkB3YXplT0z+k5asLxfYrh4k+Y/kClM"/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="279d7c9a5d26fe91064344dbee2e79df"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610499326.31-fkxxQDjz5D"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610b26bc6d81c34a')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610b26bc6d81c34a</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
