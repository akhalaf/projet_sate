<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6 ie" lang="ru" dir="ltr"> <![endif]--><!--[if IE 7]>    <html class="ie7 ie" lang="ru" dir="ltr"> <![endif]--><!--[if IE 8]>    <html class="ie8 ie" lang="ru" dir="ltr"> <![endif]--><!--[if gt IE 8]> <!--><html class="" dir="ltr" lang="ru"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<link href="https://www.veteranov130.ru/sites/default/files/kaskad_1.png" rel="shortcut icon" type="image/png"/>
<meta content="Drupal 7 (https://www.drupal.org)" name="generator"/>
<link href="https://www.veteranov130.ru/" rel="canonical"/>
<link href="https://www.veteranov130.ru/" rel="shortlink"/>
<!-- Set the viewport width to device width for mobile -->
<meta content="width=device-width" name="viewport"/>
<title>Страница не найдена | Лиговский каскад</title>
<style media="all" type="text/css">
@import url("https://www.veteranov130.ru/modules/system/system.base.css?qi6ka8");
@import url("https://www.veteranov130.ru/modules/system/system.menus.css?qi6ka8");
@import url("https://www.veteranov130.ru/modules/system/system.messages.css?qi6ka8");
@import url("https://www.veteranov130.ru/modules/system/system.theme.css?qi6ka8");
</style>
<style media="all" type="text/css">
@import url("https://www.veteranov130.ru/modules/comment/comment.css?qi6ka8");
@import url("https://www.veteranov130.ru/sites/all/modules/date/date_api/date.css?qi6ka8");
@import url("https://www.veteranov130.ru/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?qi6ka8");
@import url("https://www.veteranov130.ru/modules/field/theme/field.css?qi6ka8");
@import url("https://www.veteranov130.ru/sites/all/modules/logintoboggan/logintoboggan.css?qi6ka8");
@import url("https://www.veteranov130.ru/modules/node/node.css?qi6ka8");
@import url("https://www.veteranov130.ru/modules/poll/poll.css?qi6ka8");
@import url("https://www.veteranov130.ru/modules/search/search.css?qi6ka8");
@import url("https://www.veteranov130.ru/modules/user/user.css?qi6ka8");
@import url("https://www.veteranov130.ru/modules/forum/forum.css?qi6ka8");
@import url("https://www.veteranov130.ru/sites/all/modules/views/css/views.css?qi6ka8");
</style>
<style media="all" type="text/css">
@import url("https://www.veteranov130.ru/sites/all/libraries/colorbox/example5/colorbox.css?qi6ka8");
@import url("https://www.veteranov130.ru/sites/all/modules/ctools/css/ctools.css?qi6ka8");
@import url("https://www.veteranov130.ru/sites/all/modules/msmessage/css/msmessage.css?qi6ka8");
@import url("https://www.veteranov130.ru/sites/all/modules/yandex_metrics/css/yandex_metrics.css?qi6ka8");
</style>
<style media="all" type="text/css">
@import url("https://www.veteranov130.ru/sites/all/themes/batman/style.css?qi6ka8");
</style>
<script src="https://www.veteranov130.ru/sites/all/modules/jquery_update/replace/jquery/1.8/jquery.min.js?v=1.8.3" type="text/javascript"></script>
<script src="https://www.veteranov130.ru/misc/jquery-extend-3.4.0.js?v=1.8.3" type="text/javascript"></script>
<script src="https://www.veteranov130.ru/misc/jquery-html-prefilter-3.5.0-backport.js?v=1.8.3" type="text/javascript"></script>
<script src="https://www.veteranov130.ru/misc/jquery.once.js?v=1.2" type="text/javascript"></script>
<script src="https://www.veteranov130.ru/misc/drupal.js?qi6ka8" type="text/javascript"></script>
<script src="https://www.veteranov130.ru/sites/all/modules/admin_menu/admin_devel/admin_devel.js?qi6ka8" type="text/javascript"></script>
<script src="https://www.veteranov130.ru/sites/default/files/languages/ru_cJxfysJoixazMg4HTSShv9vHtH3LBeYxgME5-pAmJ_8.js?qi6ka8" type="text/javascript"></script>
<script src="https://www.veteranov130.ru/sites/all/libraries/colorbox/jquery.colorbox-min.js?qi6ka8" type="text/javascript"></script>
<script src="https://www.veteranov130.ru/sites/all/modules/colorbox/js/colorbox.js?qi6ka8" type="text/javascript"></script>
<script src="https://www.veteranov130.ru/sites/all/modules/msmessage/js/msmessage.js?qi6ka8" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"batman","theme_token":"u2HPE1BbXTKx_m5E4e41IKdvDMQKEh_oqoqEcUr_jQ8","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.8\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery-html-prefilter-3.5.0-backport.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"public:\/\/languages\/ru_cJxfysJoixazMg4HTSShv9vHtH3LBeYxgME5-pAmJ_8.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/msmessage\/js\/msmessage.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/logintoboggan\/logintoboggan.css":1,"modules\/node\/node.css":1,"modules\/poll\/poll.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"modules\/forum\/forum.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/libraries\/colorbox\/example5\/colorbox.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/msmessage\/css\/msmessage.css":1,"sites\/all\/modules\/yandex_metrics\/css\/yandex_metrics.css":1,"sites\/all\/themes\/batman\/style.css":1}},"colorbox":{"opacity":"0.85","current":"{current} \u0438\u0437 {total}","previous":"\u00ab \u041f\u0440\u0435\u0434\u044b\u0434\u0443\u0449\u0438\u0439","next":"\u0421\u043b\u0435\u0434\u0443\u044e\u0449\u0438\u0439 \u00bb","close":"\u0417\u0430\u043a\u0440\u044b\u0442\u044c","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px","specificPagesDefaultValue":"admin*\nimagebrowser*\nimg_assist*\nimce*\nnode\/add\/*\nnode\/*\/edit\nprint\/*\nprintpdf\/*\nsystem\/ajax\nsystem\/ajax\/*"},"urlIsAjaxTrusted":{"\/mambots\/editors\/login.html?destination=mambots\/editors\/login.html":true}});
//--><!]]>
</script>
<!-- IE Fix for HTML5 Tags -->
<!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
<!--[if lt IE 9]>
<style type="text/css" media="all">
@import url("/sites/all/themes/batman/ie.css");
</style>
  <![endif]-->
</head>
<body class="html not-front not-logged-in one-sidebar sidebar-second page-mambots page-mambots-editors page-mambots-editors-loginhtml">
<div id="border1">
<header class="clearfix" id="header" role="banner">
<div id="shlyapa">
<div class="login_buttons">
<div class="region region-login-buttons">
<section class="block block-block" id="block-block-7">
<div class="content">
<a href="/user/login">Вход</a> | <a href="/user/register">Зарегистрироваться</a> </div>
</section> <!-- /.block -->
</div>
<!-- /.region -->
</div>
<div style="height: 37px; float: left; width: 700px;">
</div>
</div> <!-- /#shlyapa -->
<div style="float:right; text-align: right; height: 80px; width: 355px; margin: 10px 22px 0 0;">
</div>
<div style="height: 100px; float: left; width: 600px;">
<a href="/" id="logo">
<img src="https://www.veteranov130.ru/sites/default/files/kaskad_0.png"/>
</a>
</div>
</header> <!-- /#header -->
<div class="clearfix" id="container">
<div id="skip-link">
<a class="element-invisible element-focusable" href="#main-content">Перейти к основному содержанию</a>
</div>
<section class="clearfix" id="main" role="main">
<a id="main-content"></a>
<h1 class="title" id="page-title">Страница не найдена</h1>                    
Запрашиваемая страница "/mambots/editors/login.html" не найдена.  </section> <!-- /#main -->
<aside class="sidebar clearfix" id="sidebar-second" role="complementary">
<div class="region region-sidebar-second">
<section class="block block-user" id="block-user-login">
<h2 class="block-title">Вход на сайт</h2>
<div class="content">
<form accept-charset="UTF-8" action="/mambots/editors/login.html?destination=mambots/editors/login.html" id="user-login-form" method="post"><div><div class="form-item form-type-textfield form-item-name">
<label for="edit-name">Имя пользователя <span class="form-required" title="Это поле обязательно для заполнения.">*</span></label>
<input class="form-text required" id="edit-name" maxlength="60" name="name" size="15" type="text" value=""/>
</div>
<div class="form-item form-type-password form-item-pass">
<label for="edit-pass">Пароль <span class="form-required" title="Это поле обязательно для заполнения.">*</span></label>
<input class="form-text required" id="edit-pass" maxlength="128" name="pass" size="15" type="password"/>
</div>
<div class="item-list"><ul><li class="first"><a href="/user/register" title="Создать новую учётную запись пользователя.">Регистрация</a></li>
<li class="last"><a href="/user/password" title="Запросить новый пароль по e-mail.">Забыли пароль?</a></li>
</ul></div><input name="form_build_id" type="hidden" value="form-cCKAvQ3ECVE9VuwQI_aHy5_L7HUcjV2KJI-7mQsd400"/>
<input name="form_id" type="hidden" value="user_login_block"/>
<div class="form-actions form-wrapper" id="edit-actions"><input class="form-submit" id="edit-submit" name="op" type="submit" value="Войти"/></div></div></form> </div>
</section> <!-- /.block -->
<section class="block block-block" id="block-block-8">
<h2 class="block-title">Председатель</h2>
<div class="content">
<section>
<figure>
<div><a href="http://pr-tsj.ru/" rel="noopener noreferrer" target="_blank" title="ПОСМОТРЕТЬ"><img alt="pr tsj logo" height="77" src="http://pr-tsj.ru/images/logotip/pr_tsj_logo.png" style="margin: 10px 10px 10px 5px; vertical-align: top;" width="200"/></a></div>
<div><span style="font-size: 12pt;"><em><strong>Краткое содержание свежих номеров журнала "Председатель ТСЖ"</strong></em></span></div>
<div><a href="http://pr-tsj.ru/index.php/2011-01-30-21-48-41/anonsy/256-dva-poslednikh-nomera/2040-poslednij-nomer" rel="noopener noreferrer" target="_blank" title="Читать анонс свежего номера"><span style="font-size: 12pt;"><em><strong><img alt="" src="http://pr-tsj.ru/images/logotip/p_nomer.jpg" style="margin: 5px 20px 10px; float: left;"/></strong></em></span></a> <a href="http://pr-tsj.ru/index.php/2011-01-30-21-48-41/anonsy/256-dva-poslednikh-nomera/2041-predposlednij-nomer" rel="noopener noreferrer" target="_blank" title="Читать анонс предпоследнего номера"><img alt="" src="http://pr-tsj.ru/images/logotip/pp_nomer.jpg" style="margin: 5px 20px 10px 10px; float: left;"/></a></div>
</figure>
</section> </div>
</section> <!-- /.block -->
<section class="block block-user" id="block-user-online">
<h2 class="block-title">Сейчас на сайте</h2>
<div class="content">
<p>Пользователей онлайн: 0.</p> </div>
</section> <!-- /.block -->
<section class="block block-views" id="block-views-newsblock-block">
<h2 class="block-title">Популярные новости</h2>
<div class="content">
<div class="view view-newsblock view-id-newsblock view-display-id-block view-dom-id-b5062a7e62e4b8d2c9029adf0eafc044">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first">
<div class="views-field views-field-title"> <span class="field-content"><a href="/novosti/spisok-transliruemyh-na-segodnya-rostelekomom-tv-kanalov">Список транслируемых  на сегодня Ростелекомом ТВ каналов</a></span> </div> </div>
<div class="views-row views-row-2 views-row-even">
<div class="views-field views-field-title"> <span class="field-content"><a href="/news/novaya-struktura-platy-za-zhku-dlya-potrebiteley">Новая структура платы за ЖКУ для потребителей</a></span> </div> </div>
<div class="views-row views-row-3 views-row-odd">
<div class="views-field views-field-title"> <span class="field-content"><a href="/novosti/informaciya-po-novym-cifrovym-kanalam-tv">Информация по новым цифровым каналам ТВ.</a></span> </div> </div>
<div class="views-row views-row-4 views-row-even">
<div class="views-field views-field-title"> <span class="field-content"><a href="/publikacii-o-tszh/vstrecha-druzey-v-ligovskom-kaskade">Встреча друзей в Лиговском каскаде</a></span> </div> </div>
<div class="views-row views-row-5 views-row-odd views-row-last">
<div class="views-field views-field-title"> <span class="field-content"><a href="/novosti/tarify-s-01012021">Тарифы с 01.01.2021</a></span> </div> </div>
</div>
</div> </div>
</section> <!-- /.block -->
</div>
<!-- /.region -->
</aside> <!-- /#sidebar-second -->
</div> <!-- /#container -->
<footer class="clearfix" id="footer" role="contentinfo">
<div class="footer-tail">
</div>
<div class="footer-bottom">
<div class="region region-footer">
<section class="block block-block" id="block-block-4">
<div class="content">
<p class="rtecenter">
	© 2017 Официальный сайт ТСЖ «Лиговский каскад»</p>
</div>
</section> <!-- /.block -->
</div>
<!-- /.region -->
</div>
</footer> <!-- /#footer -->
</div> <!-- /#border --> <div class="ym-counter"><!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter46287702 = new Ya.Metrika({
                    id:46287702,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/46287702" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter --></div></body>
</html>