<!DOCTYPE html>
<html><head><meta content="width=device-width, initial-scale=1" maximum-scale="1" name="viewport"/><meta charset="utf-8"/><meta content="max-age=0" http-equiv="cache-control"/>
<meta content="no-cache" http-equiv="cache-control"/>
<meta content="0" http-equiv="expires"/>
<meta content="Tue, 01 Jan 1980 1:00:00 GMT" http-equiv="expires"/>
<meta content="no-cache" http-equiv="pragma"/><link href="../css/normalize.css" rel="stylesheet" type="/text/css"/><link href="../css/styles.css" rel="stylesheet" type="text/css"/><!-- --><!-- --><!-- --><!--                         .':.                               --><!--                       #######+                             --><!--                     +##########                            --><!--                    #############                           --><!--                  #####     ####'                          --><!--                  ;###,       ####                          --><!--                  ###`         ###.                         --><!--                 +##+          :###                         --><!--                 ###            ###                         --><!--                 ###:           ###                         --><!--               ,########        ###                         --><!--             ;############      ###                         --><!--            ########,######     ###   .::.                  --><!--           #####;###   +###+    ### #########,              --><!--          ####`  ###    ####   ,###############`            --><!--         :###    ###     ###'  #######;,,,;#####:           --><!--         ###:    +###    +###  #####,,,,,,,,:####`          --><!--         ###      ###:    ### #####,,,,,,,,,,,####          --><!--        ;##.      '###;   ###+###+,,,,,,,,,,,,,###+         --><!--        ###        #####. ########;,,,,,,,,,,,,,###         --><!--        ###         '################;,,,,,,,,,,+##:        --><!--       ###           #################:,,,,,,,,,###        --><!--        ###              ####',,:+#######,,,,,,,,###        --><!--        '##;            ####',,,,,,,######,,,,,,,+##        --><!--        `###           .###',,,,,,,,,;#####,,,,,,;##        --><!--         ###;          ####,,,,,,,,,,,;#####,,,,,:##        --><!--         +###;        ####;:,,,,,,,,,,,;####:,,,,:##        --><!--          #####      +########',,,,,,,,,'####,,#,+##        --><!--           ######''#############+,,,,,,,,####',#,##'        --><!--            ######################,,,,,,,,####,,,##         --><!--             `#########,,,,;#######:,,,,,,####,,###         --><!--                 :''##:,,,,,,,######,,,,,,,###',##'         --><!--                   ###,,,,,,,,,######,,,,,,#######          --><!--                   ##:,,,,,,,,,,'####',,,,,:#####'          --><!--                   ##,,,,,,,,,,,,#####,,,,,,#####           --><!--                  :##,,,,,,,,,,,,,####+,,#',####.           --><!--                  ###,,,,,,,,,,,,,;####,;+,,###;            --><!--                  ##+,,,,,,,,,,,,,,####,,,,###;             --><!--                  ###,,,,,,,,,,,,,,;###',,###+              --><!--                  ######',,,,,,,,,,,####,####               --><!--                  ########,,,,,,,,,,#######+                --><!--                  ###:+####,,,,,;',,;#####'                 --><!--                  ###  #####,,,+#:,,,####`                  --><!--                  ##'  #####',,:,,,'####                    --><!--                  ##:  ######,,,,,####+                     --><!--                  ##+ #######,,,;####                       --><!--                  ###########';####;                        --><!--                  ;###############                          --><!--                   #############,                           --><!--                   .##########`                             --><!--                     :+###:                                 --><!-- --><!-- --><!--      All artwork and content on this site is Copyright Â© 2017 Maayan Malka. Please don't steal.--><!-- --><!-- --><!-- --><meta content="The Catbears is a great place to find fun activities with Printable PDFs filled with step-by-step craft tips and DIY projects for kids and parents." name="description"/>
<meta content="noodp" name="robots"/>
<meta content="website" property="og:type"/>
<meta content="The Catbears - Kids Activities" property="og:title"/>
<meta content="Fun Learning Activities for Kids" property="og:description"/>
<meta content="http://catbears.com/" property="og:url"/>
<meta content="The Catbears - Kids Activities" property="og:site_name"/>
<meta content="http://catbears/img/share-img/thecatbears-logo.png" property="og:image"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="The Catbears is a great place to find fun activities with Printable PDFs filled with step-by-step craft tips and DIY projects for kids and parents." name="twitter:description"/>
<meta content="The Catbears - Kids Activities, Tips &amp; Tricks" name="twitter:title"/>
<meta content="@thecatbears" name="twitter:site"/>
<meta content="http://catbears/img/share-img/thecatbears-logo.png" name="twitter:image"/>
<link href="http://catbears.com/" rel="canonical"/>
<meta content="en_US" property="og:locale"/>
<script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"http:\/\/catbears.com\/","name":"The Catbears - Kids Activities","potentialAction":{"@type":"SearchAction","target":"http:\/\/catbears.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"Organization","url":"http:\/\/catbears.com\/","sameAs":["http:\/\/facebook.com\/thecatbears","http:\/\/instagram.com\/thecatbears","http:\/\/youtube.com\/thecatbears","http:\/\/pinterest.com\/thecatbears","https:\/\/twitter.com\/4thecatbears"],"@id":"#organization","name":"The Catbears - Kids Activities","logo":"http:\/\/catbears.com\/img\/share-img\/thecatbears-logo.jpg"}</script>
<meta content="craft, kids activities, kids, diy, pdf, printable, pdfs, instant download, do it yourself, toddler, all ages, inspiration, kids create, puppets, pattern, scissors, coloring, coloring book, coloring pages, artsy, art, pre-school, speach therapy, kindergarten, crayons, sketch, doodle, catbears, cat, bear, fox, duck, " name="keywords"/><link href="img/logo-bee_small.png" rel="icon" sizes="16x16" type="image/png"/><link href="../img/logo_180x180.png" rel="apple-touch-icon"/><link href="../img/logo_180x180.png" rel="apple-touch-icon" sizes="76x76"/><link href="../img/logo_180x180.png" rel="apple-touch-icon" sizes="120x120"/><link href="../img/logo_180x180.png" rel="apple-touch-icon" sizes="152x152"/><link href="../img/logo_180x180.png" rel="apple-touch-icon" sizes="180x180"/><link href="../img/logo_192x192.png" rel="icon" sizes="192x192"/><link href="../img/logo_180x180.png" rel="icon" sizes="128x128"/>
<!-- head.js--><script src="../scripts/head.js" type="text/javascript"></script><script src="../lib/jquery/dist/jquery.js" type="text/javascript"></script><!-- pinterest save button--><script async="" data-pin-tall="true" defer="" src="//assets.pinterest.com/js/pinit.js" type="text/javascript"></script><!-- facebook pixel--><!-- Facebook Pixel Code--><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1400607970012908'); // Insert your pixel ID here.
fbq('track', 'PageView');</script><noscript><img height="1" src="https://www.facebook.com/tr?id=1400607970012908&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript><!-- DO NOT MODIFY--><!-- End Facebook Pixel Code-->
<!-- Add event to SUBSCRIBE button's click handler--><script type="text/javascript">$( '#mc-embedded-subscribe' ).click(function() {
fbq('track', 'Subscribe', {
//- content_ids: ['1234'],
content_type: 'SubscribeToNewsLetter-btn',
});
});</script><link href="./css/homepage.css?v=2" rel="stylesheet" type="text/css"/></head><body><link href="../css/websiteNav.css?v=2" rel="stylesheet" type="text/css"/><div class="website-navigation"><div class="wrapper"><a class="navigation__logo noMarkup" href="/"><img alt="The Catbears Logo - Online Kids Activities" src="../img/hp/thecatbears-logo-bee-yellowcircle.png"/><h2>The Catbears</h2></a><div class="navigation__links"> <a class="a-felt noMarkup" href="/felt/">Felt</a><a class="a-playtime noMarkup" href="/activities">Playtime  </a></div></div></div><link href="./css/poster-felt.css?v=2.2" rel="stylesheet" type="text/css"/><link href="https://fonts.googleapis.com/css?family=Jua" rel="stylesheet"/><div class="poster-felt"> <div class="card"><div class="content"><div class="catbears-angry"><img alt="Felt by The Catbears" src="../img/homepage/catbears-angry.png"/></div><div class="left"><img alt="Felt by The Catbears" class="iphone-on-web-only" src="../img/homepage/felt-iphone.png"/></div><div class="right"><div class="logo"><img alt="Felt by The Catbears" src="../img/homepage/felt-logo.png"/><h2>Felt	</h2></div><h1>Developing Emotional Intelligence In Children</h1><p>Quickly recognize emotions in ourselves and in others. Learn how to respond intelligently, kindly and with empathy.</p><a class="appstore-btn noMarkup" href="https://itunes.apple.com/us/app/felt-by-the-catbears/id1446535490?mt=8" target="_blank"><img alt="Felt by The Catbears" src="../img/homepage/appstore-btn.png"/></a><a class="link-learn-more noMarkup" href="/felt/">Learn more</a><img alt="Felt by The Catbears" class="iphone-on-mobile-only" src="../img/homepage/felt-iphone.png"/></div></div></div></div><link href="./css/poster-activities.css?v=2.3" rel="stylesheet" type="text/css"/><div class="poster-activities"><div class="card"><div class="content"><div class="left"><div class="logo"><img alt="Fun Offline Activities by The Catbears" src="../img/homepage/playtime-icon.png"/></div><h2>Catbears Playtime</h2><h1>Kids Create All By Themselves</h1><p>A huge library of offline projects, step by step video tutorials, available for all ages</p><a class="btn btn__icon btn__large btn__primary noMarkup" href="/activities/">Browse Projects</a></div><div class="right"><img alt="Fun Offline Activities by The Catbears" src="../img/homepage/activities-poster.png"/></div></div></div></div><link href="./css/activities-examples.css?v=2.3" rel="stylesheet" type="text/css"/><div class="activities-examples"><div class="content" id="left-right_bounce"><img alt="Fun Offline Activities by The Catbears" src="../img/homepage/activities-examples.png"/><img alt="Fun Offline Activities by The Catbears" src="../img/homepage/activities-examples.png"/></div></div><link href="../css/testemonial.css?v=2" rel="stylesheet" type="text/css"/><div class="testemonial-container"><div class="stars5"><img alt="Fun Offline Activities by The Catbears" src="../img/homepage/5stars.png"/></div><p>âThe Catbears Playtime is fantastic. It engages my sonâs creativity and nurtures his talent, without the frustration of challenging his very short attention span. The broad nature of the projects - for example "make a racing carâ allows for so many projects to be created and for him to see improvement. He can create and challenge himself over and over, and interact with other kids to encourage them and be encouraged by them.â</p><div class="testemonial-profile-container"><div class="testemonial-profile-text"><h1>Lori Smith</h1><p>Mom from California</p></div></div></div><link href="../css/pbl-poster.css?v=2" rel="stylesheet" type="text/css"/><div id="pbl-poster-container"><div class="wrapper"><div id="pbl-poster-txt"><div class="pdf-car_s"></div><h1>Make a Racing Car</h1><p>AGES 10-16</p><a class="btn btn__icon btn__large btn__primary noMarkup" href="/lesson/racing-car.html">Make</a><a class="btn btn__icon btn__small btn__primary noMarkup" href="/lesson/racing-car.html">Make</a></div></div><div id="pbl-poster-img"></div></div><link href="./css/promo-shop.css?v=2" rel="stylesheet" type="text/css"/><div class="promo-shop"><div class="container"><div class="left"><img alt="The Catbears - Shop" src="./img/homepage/catbears-shop.gif"/></div><div class="right"><div class="txt-container"><h1>Shop Catbears</h1><p>Visit our shop at Sociaty6 where you can find all sorts of crazy little catbears on mugs, tees, pillows and more.. </p></div><div class="btn btn__icon btn__large btn__primary noMarkup" href="https://society6.com/thecatbears">Visit Shop</div></div></div></div><link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"/><div class="signup-form-flat"><div class="signup-form-flat__main"><div class="signup-form-flat__section--form"><div class="form__logo"><img alt="The Catbears Playtime - Fun Offline Projects" src="../img/homepage/playtime-icon-L.png"/></div><div class="form__txt"><h1>Playtime projects to your mailbox</h1><p>Get notified when new projects are ready</p></div><div class="form__form"><!-- Begin MailChimp Signup Form--><link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css"/><link href="../css/signup-form-flat.css?v=2.2" rel="stylesheet" type="text/css"/><style>#mc_embed_signup input {
    font-family: 'Nunito', sans-serif;
    padding: 0;
    margin: 0;
    width: 100%;
    max-width: 350px;
    background: white;
    border: 1px solid #e2e2e2;
    height: 60px;
    padding-left: 28px;
    padding-right: 20px;
    margin-bottom: 20px;
    margin-top: 4px;
    font-size: 20px;
    border-radius: 100px;
    outline: none;
    color : black;
}
.mce_inline_error {
  font-family: 'Nunito', sans-serif;
  width: 100%;
  max-width: 350px;
  height: auto;
  margin: 0;
  padding: 20px 20px 13px;
  border-radius: 10px;
  margin-top: -30px;
  color: white;
}
.response {
  font-family: 'Nunito', sans-serif;
  color: white;
  padding-bottom: 10px;
  margin-top: -11px;
  text-align: center;
}
/*.response:before {
  content:"ð";
  font-size:24px;
  margin-right: 10px;
}*/
.mc-field-group {
    width: 100%;
    max-width: 350px;
   http://localhost:3000/ float: left;
}

.clear {
    width: 100%;
    max-width: 350px;
    float: left;
}
#mc_embed_signup .mc-field-group {
  width: 100%;
}
#mc_embed_signup #mc-embedded-subscribe-form div.mce_inline_error {
  background-color: rgba(255, 158, 189, 0.81);
  border-radius: 4px;
  margin: 0 auto;
  width: 100%;
  text-align: center;
  color: #ff2900;
  margin-top: 0;
}
#mc_embed_signup .mc-field-group input {
  padding-left: 15px;
}
#mc_embed_signup input[type="submit"] {
  margin: 0;
  margin-left: 0 !important;
  margin-right: 0 !important;
  margin-top: 0px;
  padding: 0 30px;
  width: 100%;
  height: 60px;
  font-size: 24px;
  background-color: #8314EC;
  color : #fff;
  border: none;
  border-radius: 100px;
  max-width:350px;
  }
  #mc_embed_signup input[type="submit"]:hover{
    background-color: #992EFF;      
  }
  #mc_embed_signup input[type="submit"]:active{
    background-color: #8314EC;
  }
@media(max-width: 580px) {
  #mc_embed_signup input {
    max-width: 315px;
  }
  #mc_embed_signup input[type="submit"] {
    margin-left: 0;
    max-width: 315px;
  }
  #mc_embed_signup input[type="submit"]:hover{
    width: 100%;
    max-width: 315px;
  }
  #mc_embed_signup input[type="submit"]:active{
    width: 100%;
    max-width: 100%;
    max-width: 315px;
  }
  .mce_inline_error {
    width: 100%;
    max-width: 100%;
  }
  .mc-field-group {
      width: 100%;
      max-width: 100%;
  }
  .clear {
      width: 100%;
      max-width: 100%;
  }

}
#mc_embed_signup
/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */

</style><div id="mc_embed_signup"><form action="//thecatbears.us14.list-manage.com/subscribe/post?u=a7426f90aa46ed0d3d13700af&amp;amp;id=759f0584e8" class="validate" id="mc-embedded-subscribe-form" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank"><div id="mc_embed_signup_scroll"><div class="indicates-required"></div><div class="mc-field-group"><label for="mce-EMAIL"></label><input class="required email" id="mce-EMAIL" name="EMAIL" placeholder="Your Email..." type="email" value=""/></div><div class="clear" id="mce-responses"><div class="response" id="mce-error-response" style="display:none"></div><div class="response" id="mce-success-response" style="display:none"></div></div><!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups--><div aria-hidden="true" style="position: absolute; left: -5000px;"><input name="b_a7426f90aa46ed0d3d13700af_759f0584e8" tabindex="-1" type="text" value=""/></div><div class="clear"><input class="btn btn__medium" id="mc-embedded-subscribe" name="subscribe" onclick="setUserSubscribed()" type="submit" value="Sign Up"/></div></div></form></div><script src="//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js" type="text/javascript"></script><script type="text/javascript">(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script><!-- End mc_embed_signup--></div></div></div><img alt="The Catbears Playtime - Fun Offline Projects" class="footer-img-catbears" src="../img/homepage/footer-img-catbears.jpg"/></div><link href="../css/about-footer.css?v=2" rel="stylesheet" type="text/css"/><div id="about-footer-container"><div class="about-footer-grey-logo"></div><div class="about-footer-txt-container"></div><div class="footer__liner"><div class="footer__liner--left"><a class="a-felt" href="/felt">Felt</a><p>/</p><a class="a-playtime" href="/activities">Playtime</a><p>/</p><a class="a-shop" href="https://society6.com/thecatbears" target="_blank">Shop</a><p>/</p><a class="a-instagram" href="https://www.instagram.com/thecatbears/" target="_blank">Instagram</a><p>/</p><a class="a-facebook" href="https://www.facebook.com/theCatbears" target="_blank"> Facebook</a><p>/</p><a class="a-stickers" href="https://itunes.apple.com/us/app/fox-duck-by-the-catbears/id1158843136?mt=8" target="_blank">iMessage Stickers</a></div><div class="footer__liner--right"><p>The Catbears Â© 2019</p></div></div></div><!-- Jaco --><script type="text/javascript">(function(e, t) {
function i(e, t) {
e[t] = function() {
e.push([ t ].concat(Array.prototype.slice.call(arguments, 0)));
};
}
function s() {
var e = t.location.hostname.match(/[a-z0-9][a-z0-9\\-]+\.[a-z\.]{2,6}$/i), n = e ? e[0] : null, i = "; domain=." + n + "; path=/";
t.referrer && t.referrer.indexOf(n) === -1 ? t.cookie = "jaco_referer=" + t.referrer + i : t.cookie = "jaco_referer=" + r + i;
}
var n = "JacoRecorder", r = "none";
(function(e, t, r, o) {
if (!r.__VERSION) {
e[n] = r;
var u = [ "init", "identify", "startRecording", "stopRecording", "removeUserTracking", "setUserInfo" ];
for (var a = 0; a < u.length; a++) i(r, u[a]);
s(), r.__VERSION = 2.1, r.__INIT_TIME = 1 * new Date;
var f = t.createElement("script");
f.async = !0, f.setAttribute("crossorigin", "anonymous"), f.src = o;
var l = t.getElementsByTagName("head")[0];
l.appendChild(f);
}
})(e, t, e[n] || [], "https://recorder-assets.getjaco.com/recorder_v2.js");
}).call(window, window, document), window.JacoRecorder.push([ "init", "d47a94c4-3719-40c2-aa36-6d0cb512a0d3", {} ]);</script><!-- GA--><script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-84548039-1', 'auto');
ga('send', 'pageview');</script><!-- heap-analytics--><script type="text/javascript">window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
heap.load("3036450273");</script><script src="../scripts/localStorage.js" type="text/javascript"></script></body></html>