<!DOCTYPE html>
<html>
<head>
<title>Coming Soon</title>
<meta charset="utf-8"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<meta content="Coming Soon" name="description"/>
<meta content="" property="og:image"/>
<link href="https://www.canspace.ca/images/landing/favicon.ico" rel="shortcut icon"/>
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400" rel="stylesheet"/>
<link href="https://www.canspace.ca/images/landing/canspacelanding.css" rel="stylesheet"/>
</head>
<body class="font-modern" style="overflow-x:hidden">
<div class="red-white">
<header class="header">
<div class="container-lrg">
<div class="flex col-12 spread"><a class="logo"><img alt="Logo" src="https://www.canspace.ca/images/landing/mapleleaf.png"/></a>
</div>
</div>
</header>
<section class="section">
<div class="container-lrg flex">
<div class="col-6 flex flex-column center-vertical mobile-text-center mb40">
<h1 class="heading-lrg primary-color"><br/>Welcome to<br/>commonnsystems.ca</h1>
<h2 class="subheading secondary-color mt20">This website is coming soon.</h2>
</div>
<div class="col-6 mobile-text-center">
<div class="iphone">
<div class="mask"><img alt="Screenshot" class="mask-img" src="https://www.canspace.ca/images/landing/leaves.png"/></div>
</div>
</div>
</div>
</section>
</div>
<br/><br/>
<footer activepage="Landing" class="section text-center red-white" id="footer-1" sitemeta="[object Object]">
<div class="container col-12">
<nav class="mb50"><a class="nav-link secondary-color" href="https://www.canspace.ca" target="_blank">Hosted by CanSpace Solutions</a></nav>
</div>
</footer>
</body></html>
