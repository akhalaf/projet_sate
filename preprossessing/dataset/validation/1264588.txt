<!DOCTYPE html>
<html dir="ltr" lang="es-es" xml:lang="es-es" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>Error: 404 CategorÃ­a no encontrada</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
<style type="text/css">
			h1,h2,h3,h4,h5,h6,.site-title{
				font-family: 'Open Sans', sans-serif;
			}
		</style>
<link href="/templates/protostar/css/template.css" rel="stylesheet" type="text/css"/>
<link href="/templates/protostar/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<!--[if lt IE 9]>
		<script src="/media/jui/js/html5.js"></script>
	<![endif]-->
</head>
<body class="site com_content view-category no-layout no-task itemid-101">
<!-- Body -->
<div class="body">
<div class="container">
<!-- Header -->
<div class="header">
<div class="header-inner clearfix">
<a class="brand pull-left" href="">
<span class="site-title" title="Parroquia de San Antonio de Padua de DÃ©nia">Parroquia de San Antonio de Padua de DÃ©nia</span> </a>
<div class="header-search pull-right">
</div>
</div>
</div>
<div class="navigation">
<ul class="nav menu nav-pills">
<li class="item-101 current active"><a href="/">Inicio</a></li><li class="item-111"><a href="/index.php/historia">Historia</a></li><li class="item-108"><a href="/index.php/horarios">Horarios</a></li><li class="item-112 deeper parent"><a href="/index.php/hoja-parroquial">Hoja parroquial</a><ul class="nav-child unstyled small"><li class="item-130"><a href="/index.php/hoja-parroquial/enero-2015">Enero 2015</a></li><li class="item-129"><a href="/index.php/hoja-parroquial/diciembre-2014">Diciembre 2014</a></li><li class="item-126"><a href="/index.php/hoja-parroquial/noviembre-2014">Noviembre 2014</a></li><li class="item-113"><a href="/index.php/hoja-parroquial/septiembre">Octubre 2014</a></li></ul></li><li class="item-110"><a href="/index.php/contacto">Contacto</a></li><li class="item-132"><a href="http://sanantoniodedenia.blogspot.com/" target="_blank">Blog</a></li></ul>
</div>
<!-- Banner -->
<div class="banner">
</div>
<div class="row-fluid">
<div class="span12" id="content">
<!-- Begin Content -->
<h1 class="page-header">La pÃ¡gina solicitada no se puede encontrar.</h1>
<div class="well">
<div class="row-fluid">
<div class="span6">
<p><strong>Se produjo un error al procesar su solicitud.</strong></p>
<p>Es posible que no pueda visitar esta pÃ¡gina por:</p>
<ul>
<li>un marcador/favorito <strong>fuera de fecha</strong></li>
<li>una <strong>direcciÃ³n mal escrita</strong></li>
<li>un motor de bÃºsqueda que tiene una lista <strong>fuera de fecha para este sitio</strong></li>
<li>usted <strong>no tiene acceso</strong> a esta pÃ¡gina</li>
</ul>
</div>
<div class="span6">
<p>Ir a la pÃ¡gina de Inicio</p>
<p><a class="btn" href="/index.php"><i class="icon-home"></i> PÃ¡gina de inicio</a></p>
</div>
</div>
<hr/>
<p>Si las dificultades persisten, pÃ³ngase en contacto con el administrador de este sitio.</p>
<blockquote>
<span class="label label-inverse">404</span> CategorÃ­a no encontrada						</blockquote>
</div>
<!-- End Content -->
</div>
</div>
</div>
</div>
<!-- Footer -->
<div class="footer">
<div class="container">
<hr/>
<p class="pull-right">
<a href="#top" id="back-top">
					Ir arriba				</a>
</p>
<p>
				© 2021 Parroquia de San Antonio de Padua de DÃ©nia			</p>
</div>
</div>
</body>
</html>
