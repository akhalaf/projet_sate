<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "23555",
      cRay: "61109c365de41aa0",
      cHash: "b5649f008330ae5",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmVpcnV0b2JzZXJ2ZXIuY29tLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "+QezizlDTyZWxW7TfmNOVMBt+Ob2WPzR6OZrcKdRVnpQMQ3mAA2Smgr1QlxlkOSaQIx4uPMdb6hZV3BD6ZFmops54NLd4jEuZL8uZgB+0mRRIeLmL0BTJq0tAdlpR/vbw+G7o53CCENYF02lU+6HqQzT2MBad8PQ5sYmWULogknBZjooAVq27jXvREJKR6pKw9jh/BRMkQHotXFqxg2zGpVxgAFB66xze/NHY6je/xgDD3EwUnNkiVRqpttRwNHvkZnEOvtTIX5FBeUbEPh3JXlluJWOmi5USFHV35aYIVONbWlGYNNy/4HBlvFMZVIJRzcvGKXP9W3Jjarin+6NbXAfTdk3IdHFZ3ymiducM0v5FGfYfGXKpYc/6uNpcQ87B9nQ9OFq5TWV+qRnDqk8ra0qOGmDEzMor6uNEjHd6LBmhlnJXiZ+VHjSWZVMR9DbOM+f1Ju3VbK/Yb/OlfJd/zYl9Afh8lyIKwocAUyX1HvnDDRlxT7sy9KIBfkXwaKfLaZgXOsmScPn0ozvtnZ7rCWrnqGXskoOaf0CyJ3W5VHzM4xudmXxbvp6xWkr63TvDNhpTj3rm+9y3obXZ6TYF1BgInrXJXLoR+FTP2HHBR1+YS1B3gmWbIstcaqMUAZEuzd2Vv5AAfVrdwwyif5xJGfE1ahIaPiFjhWgJbP0mT9JENVwPAbmySUGkrxvoydtgoV95HeRrcgnAWh4o1EuuMHdx5HiH7lIGvgg0uVJU2SKJRpgla09OPBAs/TwNiV6",
        t: "MTYxMDU1NjU2Mi45MzcwMDA=",
        m: "lLWyFZHfKgFarAAl6HOEVfqDKjlwhJ4AuxmaIIG+T2I=",
        i1: "i0/CNllFOPINn3s10IIhvA==",
        i2: "XB6kni134qhO6mcGC4sobQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "WvIpWvQ4WHqQ+dU8mRvk5D8lW6BgbM0A5P62l5+p308=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.beirutobserver.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=4023ffeff9b5a3b4dbfd92972d404bdbdb0df1c7-1610556562-0-AVRD3-oD5mQOBd9BYXoNUqxhpoM3M0_iG2IxKU8EQUki_4bxhzM6THsYptTk4ecJOMvqbgKAi_mAx2XpqVfAVC3rmfz_wfmsSYh_EvAvwbI06zZqUlYG52sv1rxZ-ASgoDm18tFzvIogC8zZJHgD3kVJgXaV7QX9XsltmxhkRbozKWJQmEs6Pkm6IYA-Vk5YJcoAoPenZKOV8H-E8CL57sofcVkOQheUIo6AOXDm74Y72x0-62kDDepGYuWg66x0WDTe5FOItBvO7mtrqt3yXtDxvuldDcZlWCZtNoytoZgIdG-bTNrfbAp7iKa4jhVe3Crrtt0IjeRSYSeXMEoKVNfdDZihSOerzSEbZ7nVcNKQS1lZKgcndmDSTp2zepkKOYCfnhTiveBV1EM__HrfBeu4bitf6wBhjyLJAfRvQtIvot6ZotaTGRpCgnPxxtSwFSZPMwUPEKyGkfNCKpPvBPgMe1rTu_R6PrUdNESY1ESh-2YxdEogHDRPAYNbg6phlTVX-rLSPtvEMhHlu0VqZt8" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="ffa85f6bfbfcaa663f3b364c5d077f427819f595-1610556562-0-Ac6CVHlXlbSOynDjWhQU9+2OXnCa0ze65HdCyOvXj6tEqo/kd4JPjahRDUnB6yQyx2NvGDEYzHE5NFS09CDvTSGpwH5aI08r2XJ+v5zFlX4bytVgbTqyc20/Mf6aJ2t34vn2DhDuIH3+YWcMLjRPR8c8xUlMREsDiMc4/nFkXlTcIO4NX9TAv19iVUqO8j1iqywOtG6iR+o5eVt+LEsER8vIdXGZZd1N4m3UHUTZdlrKJJB/GEJUgw9JOGdTTX7Gd/kfSnvz/mvdnzWtDWAyiX6z1ay5k7JzNnX7u1ymhblU8znLqJ4z7X6RDAGr101yP5DZgEqvOpgODXCCeMaIe1a3APimQaFrlS4n1OmqePuT5qeyo/h569/VWJYcg9YXT++YdRrnbFGQEERDMCxMyKEKh8rQaA0VzNe+UPpnjWxwCT/Qjtoom8MgjReKPGrx5COF9Ccz6jEJSdO7MWX2umJYw5JBbWGpl6SiXAeqD0+V52r/ciY0jfQwNrSKI6GGMB8LPJ+449LhLBpaKLdYhfwZ2Qe7i1HcQvKt9Vz9eo63Z+CzUfVDSXnlUq8HzB7GkWfrE8PgIxtPGuQzexZhFsFUXVgZe3HqjjlfTDhH4RVC+EdHykoe5gOOzQ/a0P6bsNbZMgXk4YCqa0Xetv5+gMP/qyWUtlXGQKNqadpA+dfegu6sNCAavpuOzloeWqh/uyfU6MXirt56XQvm1hS1vKi4khoQrMY7wkPSP0nUzIIpS0tej32i6ZCQli2R9+TAjRUkm1xCFymymSpMNJiVnhVzdW4Fk4D/Nkk1Y5VkDOnrkwuqAlgKFF6VPjjvY/dENQlwjIscufpdGgXif0BsK2VGMqIG3xEIR0omh0kggpbpCuBZnnuM+WegoK3LmR/XS8v652my4FDn6cH5GCFEuUw0e8tFPpK12HP4Oly6k2TkqvuV0p39uyOhkeobfJkjB6zXIATZRakjD1f1uTPybjx6V0k1HjWDiPiR68O8TjaUIuzqepe/dALyyBEK6fc1sCfmbRp+3pCnOIc5bRmrE0atlOyQrODijLBClQC51DcDEibUcHPMY3qX5tVghI+MRk5Lx3vZvyyr0QbkB3QPSfKiCa4NJjlBFPtM2xgGn/PSPf92S5AI2pchaVMJJwsOhyTxQAWIoySSUPZS2HseOWhGRjVoC5/ihAg4xHq+Ba8sGxp+d+TbLIDNwTPfOp+yJ9aVHCIf/ggqKa5fDV++ueZPR8K4aoVNP4e5eojkQVClh9iwIByRD7ITdy+dd3hHHeADbG6GitCGqwTsWB9zUIBWAPUZ9+z3oC59HMAHTtoRegoit4fkpudYLVtiBVWMLQ=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="5de531066d6bb6747fd769959ee3aa64"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=61109c365de41aa0')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<a href="https://thepaulrobinson.com/printing.php?showtopic=10" style="display: none;">table</a>
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">61109c365de41aa0</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:8d2b:d30c:bf6f:658f</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
