<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8"/>
<title>Servicios de IngenierÃ­a, MediciÃ³n y AutomatizaciÃ³n S.A. de C.V. | Coatzacoalcos, Veracruz.</title>
<base href="/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="favicon.ico" rel="icon" type="image/x-icon"/>
<meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport"/>
<link href="https://simasoluciones.com.mx/" rel="canonical"/>
<meta content="Somos una empresa especializada en ingenierÃ­a, fabricaciÃ³n, integraciÃ³n y puesta en operaciÃ³n de sistemas de mediciÃ³n de lÃ­quidos y gases para transferencia de custodia y de referencia. Sima Soluciones, Coatzacoalcos, Veracruz, MX." name="description"/>
<meta content="Edgar IvÃ¡n MartÃ­nez Leal, lennon182@hotmail.com, https://pilder.net, 921-116-6469" name="author"/>
<meta content="Paneles Solares, Medidores para gas natural, Medidores para lÃ­quidos, Medidores para corte de agua, Transmisores de presiÃ³n,Transmisores de temperatura, Transmisores de presiÃ³n diferencial, Transmisores de nivel, Indicadores de presiÃ³n, Indicadores de temperatura, VÃ¡lvulas, Filtros, Analizadores de H2S, Analizadores de humedad, Cromatografos, VÃ¡lvulas de seguridad, DiseÃ±o, fabricaciÃ³n, instalaciÃ³n y puesta en operaciÃ³n de Estaciones de FiltraciÃ³n, MediciÃ³n y RegulaciÃ³n para Gas Natural, DiseÃ±o, fabricaciÃ³n, instalaciÃ³n y puesta en operaciÃ³n de City Gate para Gas Natural, DiseÃ±o, fabricaciÃ³n, instalaciÃ³n y puesta en operaciÃ³n de Patines de MediciÃ³n para Hidrocarburos en estado LÃ­quidos y Gas para transferencia de custodia y Referencia, CalibraciÃ³n de instrumentos de presiÃ³n, temperatura y flujo, ConfiguraciÃ³n de Computadores de flujo, ConfiguraciÃ³n de sistemas de control supervisorio SCADA utilizando software de programaciÃ³n, iFix, Wonderware, RsView32, National Instrument, InstalaciÃ³n de sistemas de Monitoreo de DetecciÃ³n de gas, fuego y supresiÃ³n en oficinas administrativas, sistemas de proceso y de Ã¡reas riesgosas de las marcas MSA, Dragger, Dettronics, Detcon Inc, Mantenimiento correctivo y preventivo a EstaciÃ³n De MediciÃ³n, RegulaciÃ³n Y Control, Trampas de Diablo, SCASA y sistemas de comunicaciÃ³n y /o vÃ¡lvulas de doble sello y actuadores." name="keywords"/>
<link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Architects+Daughter" rel="stylesheet"/>
<link href="styles.8f398d7fcc520c59f6de.css" rel="stylesheet"/></head>
<body>
<app-root>
<div class="center-align" style="text-align: center!important; padding-top: 10rem; font-family: Verdana, Geneva, Tahoma, sans-serif">
<p>
<img alt="" src="assets/img/loading.gif" style="width:150px;"/>
</p>
<h4> Sima Soluciones estÃ¡ cargando... </h4>
<p> pilder.net - 2019 </p>
</div>
</app-root>
<script src="runtime.ec2944dd8b20ec099bf3.js" type="text/javascript"></script><script src="polyfills.38cfeb63b91a963d9fbf.js" type="text/javascript"></script><script src="scripts.48ebb7766b739076b4b5.js" type="text/javascript"></script><script src="main.f43f4e427d04dc66995f.js" type="text/javascript"></script></body>
</html>