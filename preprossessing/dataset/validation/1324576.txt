<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-US">
<![endif]--><!--[if IE 8]>
<html class="ie ie8" lang="en-US">
<![endif]--><!--[if !(IE 7) & !(IE 8)]><!--><html lang="en-US">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<title>Page not found | Simple Closet (S) Pte Ltd</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.simplecloset.com.sg/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
	<script src="https://www.simplecloset.com.sg/wp-content/themes/top3themes/js/html5.js"></script>
	<![endif]-->
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Page not found « Simple Closet (S) Pte Ltd Simple Closet (S) Pte Ltd</title>
<link href="https://www.simplecloset.com.sg/xmlrpc.php" rel="pingback"/>
<link href="https://www.simplecloset.com.sg/wp-content/themes/top3themes/favicon.ico" rel="shortcut icon"/>
<link href="https://www.simplecloset.com.sg/wp-content/themes/top3themes/css/slideshow/themes/default/default.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.simplecloset.com.sg/wp-content/themes/top3themes/css/slideshow/themes/light/light.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.simplecloset.com.sg/wp-content/themes/top3themes/css/slideshow/themes/dark/dark.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.simplecloset.com.sg/wp-content/themes/top3themes/css/slideshow/themes/bar/bar.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.simplecloset.com.sg/wp-content/themes/top3themes/css/slideshow/nivo-slider.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="https://www.simplecloset.com.sg/wp-content/themes/top3themes/js/slideshow/jquery-1.9.0.min.js" type="text/javascript"></script>
<script src="https://www.simplecloset.com.sg/wp-content/themes/top3themes/js/slideshow/jquery.nivo.slider.js" type="text/javascript"></script>
<script src="https://www.simplecloset.com.sg/wp-content/themes/top3themes/js/js.js"></script>
<link href="https://www.simplecloset.com.sg/wp-content/themes/top3themes/boxplus/css/boxplus.min.css" rel="stylesheet" type="text/css"/>
<link href="https://www.simplecloset.com.sg/wp-content/themes/top3themes/boxplus/css/boxplus.lightsquare.css" rel="stylesheet" title="boxplus-lightsquare" type="text/css"/>
<script src="https://www.simplecloset.com.sg/wp-content/themes/top3themes/js/mootools-core.js" type="text/javascript"></script>
<script src="https://www.simplecloset.com.sg/wp-content/themes/top3themes/boxplus/js/jsonp.mootools.min.js" type="text/javascript"></script>
<script src="https://www.simplecloset.com.sg/wp-content/themes/top3themes/boxplus/js/boxplus.min.js" type="text/javascript"></script>
<script src="https://www.simplecloset.com.sg/wp-content/themes/top3themes/boxplus/js/boxplus.lang.min.js?lang=en-GB" type="text/javascript"></script>
<script type="text/javascript">
		boxplus.autodiscover(false,{"theme":"lightsquare","autocenter":true,"autofit":true,"slideshow":0,"loop":false,"captions":"bottom","thumbs":"inside","width":800,"height":600,"duration":250,"transition":"linear","contextmenu":true});
		window.addEvent("domready", function() {
			SqueezeBox.initialize({});
			$$("a.modal").each(function(el) {
				el.addEvent("click", function(e) {
			new Event(e).stop();
			SqueezeBox.fromElement(el);
				});
			});
		});
	</script>
<link href="https://www.simplecloset.com.sg/wp-content/themes/top3themes/css/temp.css" rel="stylesheet" type="text/css"/>
<link href="https://www.simplecloset.com.sg/wp-content/themes/top3themes/css/mtemp.css" rel="stylesheet" type="text/css"/>
<link href="https://www.simplecloset.com.sg/wp-content/themes/top3themes/css/slideshow/slideshow.css" rel="stylesheet"/>
<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
</head>
<body><div class="content-area" id="primary">
<div class="site-content" id="content" role="main">
<header class="page-header">
<h1 class="page-title">Not Found</h1>
</header>
<div class="page-wrapper">
<div class="page-content">
<h2>This is somewhat embarrassing, isn’t it?</h2>
<p>It looks like nothing was found at this location. Maybe try a search?</p>
<form action="https://www.simplecloset.com.sg/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form> </div><!-- .page-content -->
</div><!-- .page-wrapper -->
</div><!-- #content -->
</div><!-- #primary -->
<hr/>
<div id="footer" role="contentinfo">
<!-- If you'd like to support WordPress, having the "powered by" link somewhere on your blog is the best way; it's our only promotion or advertising. -->
<p>
		Simple Closet (S) Pte Ltd is proudly powered by <a href="https://wordpress.org/">WordPress</a> </p>
</div>
<!-- Gorgeous design by Michael Heilemann - http://binarybonsai.com/kubrick/ -->
</body>
</html>
