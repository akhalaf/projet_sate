<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--[if IE 7]>

	<link rel="stylesheet" type="text/css" href="ie7.css">

<![endif]-->
<style type="text/css">

.iframecls{

margin-right:30px;

}

</style>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>404 Page Not Found | EkoFuel</title>
<meta content="Ekofuel | 404 Page not found" name="description"/>
<meta content="ekofuel" name="keywords"/>
<meta content="INDEX,FOLLOW" name="robots"/>
<!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<meta name="viewport" content="width=device-width, initial-scale=1"/>-->
<meta content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=1;" name="viewport"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="black-translucent" names="apple-mobile-web-app-status-bar-style"/>
<link href="https://ekofuel.org/skin/frontend/default/hellowired/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="https://ekofuel.org/skin/frontend/default/hellowired/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://ekofuel.org/media/css_secure/ed3913ae8ff7ed8d60053adef5a7c614.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ekofuel.org/media/css_secure/18011001d12d4c020b2458c825ff1e8d.css" media="print" rel="stylesheet" type="text/css"/>
<link href="https://ekofuel.org/rss/catalog/new/store_id/1/" rel="alternate" title="New Products" type="application/rss+xml"/>
<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="https://ekofuel.org/media/css_secure/54470b4bdc024dbcc43e457c95d8ed41.css" media="all" />
<![endif]-->
<!-- jQuery LightBoxes -->
<!-- FancyBox -->
<link href="https://ekofuel.org/js/lightboxes/fancybox/jquery.fancybox-1.3.4.css" media="screen" rel="stylesheet" type="text/css"/>
<!-- FancyBox -->
<!-- //jQuery LightBoxes -->
</head>
<body class=" cms-index-noroute cms-no-route">
<!-- BEGIN GOOGLE ANALYTICS CODEs -->
<!-- END GOOGLE ANALYTICS CODE -->
<div class="wrapper">
<div class="top-border">
<div class="left"></div>
<div class="right"></div>
</div>
<div class="inner-wrapper">
<noscript>
<div class="global-site-notice noscript">
<div class="notice-inner">
<p>
<strong>JavaScript seems to be disabled in your browser.</strong><br/>
                    You must have JavaScript enabled in your browser to utilize the functionality of this website.                </p>
</div>
</div>
</noscript>
<div class="page">
<div class="header-container">
<div class="header">
<a class="logo" href="https://ekofuel.org/" title="EkoFuel Bioethanol"><strong>EkoFuel Bioethanol</strong><img alt="EkoFuel Bioethanol" src="https://ekofuel.org/skin/frontend/default/hellowired/images/logo.png"/></a>
<div class="access">
<div class="ph-no"><span class="icon-16"></span>01709 524 162</div>
<form action="https://ekofuel.org/catalogsearch/result/" id="search_mini_form" method="get">
<div class="form-search">
<label for="search">Search</label>
<input class="input-text" id="search" name="q" type="text" value=""/>
<input class="search_btn" src="https://ekofuel.org/skin/frontend/default/hellowired/images/icons/search_ico.gif" type="image" value="Go"/> <div class="search-autocomplete" id="search_autocomplete"></div>
</div>
</form>
<div class="cartshop">
<div class="carttxt">
<span>
<a href="https://ekofuel.org/checkout/cart/">0 item(s)</a> - 0.0		  </span>
</div>
</div>
<div class="cartshop">
<div class="comparetxt">
<span>
<a href="#" onclick="alert('Please select one or more products to compare.'); return false;" target="_blank">Compare Items</a>
</span>
</div>
</div>
</div>
</div>
<div id="navigation">
<div class="show-nav hidden-desktop"><span class="text">Menu</span>
<span class="btn-show-nav"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </span>
</div>
<div class="nav-container">
<ul id="nav">
<li class="home "><a href="https://ekofuel.org/"><span>Home</span></a></li>
<link href="https://ekofuel.org/skin/frontend/default/default/css/magebuzz/catsidebarnav/fly-out.css" media="screen" rel="stylesheet" type="text/css"/>
<li class="level0 nav-1 first level-top parent "><a class="level-top" href="https://ekofuel.org/bioethanol-fuel-for-fireplaces.html"><span>Bioethanol for Fires</span></a>
<ul class="sf-menu" id="left-nav">
<li class="level1 nav-1-1 first">
<a href="https://ekofuel.org/bioethanol-fuel-for-fireplaces/all-bioethanol-fuel.html">
<span>

								Bioethanol Fuel - Delivered
								</span>
</a>
<ul class="level1">
<li class="level2 nav-1-1 first">
<a href="https://ekofuel.org/bioethanol-fuel-for-fireplaces/all-bioethanol-fuel/1l-bioethanol-fuel-for-fires.html">
<img alt="1L Bottles of Bioethanol Fuel" height="50" src="https://ekofuel.org/media/catalog/category/resized/50x50/crop_green_category.jpg" width="50"/>
<div>
<h3>1L Bottles of Bioethanol Fuel</h3>
<p>

												1L Bottles of Bioethanol Fuel
											 </p>
</div>
</a>
</li>
<li class="level2 nav-1-1 first">
<a href="https://ekofuel.org/bioethanol-fuel-for-fireplaces/bulk-deals-fuel-and-sanitiser.html">
<img alt="Bulk Deals Fuel &amp; Sanitiser" height="50" src="https://ekofuel.org/media/catalog/category/resized/50x50/Bottle_Images_-_1xEF_-_1x500_BS_-_1x100_BS.jpg" width="50"/>
<div>
<h3>Bulk Deals Fuel &amp; Sanitiser</h3>
<p>

												Bulk Deals of Bioethanol Fuel &amp; Hand Sanitiser
											 </p>
</div>
</a>
</li>
</ul>
</li>
<li class="level1 nav-1-1 first">
<a href="https://ekofuel.org/bioethanol-fuel-for-fireplaces/bioethanol-collect.html">
<span>

								Bioethanol Fuel - Collect
								</span>
</a>
</li>
<li class="level1 nav-1-1 first">
<a href="https://ekofuel.org/bioethanol-fuel-for-fireplaces/what-is-bioethanol-fuel-faqs.html">
<span>

								What is Bioethanol Fuel? - FAQs
								</span>
</a>
</li>
<li class="level1 nav-1-1 first">
<a href="https://ekofuel.org/bioethanol-fuel-for-fireplaces/safety-1.html">
<span>

								Safety Information
								</span>
</a>
</li>
</ul>
</li>
<li class="level0 nav-1 first level-top parent "><a class="level-top" href="https://ekofuel.org/bioethanol-fuel-for-alcohol-marine-stove.html"><span>Marine &amp; Alcohol Camping Fuel</span></a>
<ul class="sf-menu" id="left-nav">
<li class="level1 nav-1-1 first">
<a href="https://ekofuel.org/bioethanol-fuel-for-alcohol-marine-stove/all-marine-alcohol-fuel.html">
<span>

								All Marine &amp; Alcohol Camping Fuel
								</span>
</a>
<ul class="level1">
<li class="level2 nav-1-1 first">
<a href="https://ekofuel.org/bioethanol-fuel-for-alcohol-marine-stove/all-marine-alcohol-fuel/1l-marine-alcohol-fuel.html">
<img alt="1L Bottles of Marine Alcohol Fuel" height="50" src="https://ekofuel.org/media/catalog/category/resized/50x50/crop_blue_category.jpg" width="50"/>
<div>
<h3>1L Bottles of Marine Alcohol Fuel</h3>
<p>

												1L Bottles of Marine Alcohol Fuel
											 </p>
</div>
</a>
</li>
<li class="level2 nav-1-1 first">
<a href="https://ekofuel.org/bioethanol-fuel-for-alcohol-marine-stove/all-marine-alcohol-fuel/1l-alcohol-camping-fuel.html">
<img alt="1L Bottles of Alcohol Camping Fuel" height="50" src="https://ekofuel.org/media/catalog/category/resized/50x50/crop_orange_category.jpg" width="50"/>
<div>
<h3>1L Bottles of Alcohol Camping Fuel</h3>
<p>

												1L Bottles of Alcohol Camping Fuel
											 </p>
</div>
</a>
</li>
<li class="level2 nav-1-1 first">
<a href="https://ekofuel.org/bioethanol-fuel-for-alcohol-marine-stove/all-marine-alcohol-fuel/half-1l-alcohol-camping-fuel.html">
<img alt="0.5L Bottles of Alcohol Camping Fuel" height="50" src="https://ekofuel.org/media/catalog/category/resized/50x50/1L_Camping_Bioethanol_2.jpg" width="50"/>
<div>
<h3>0.5L Bottles of Alcohol Camping Fuel</h3>
<p>

												0.5L Bottles of Alcohol Camping Fuel
											 </p>
</div>
</a>
</li>
</ul>
</li>
<li class="level1 nav-1-1 first">
<a href="https://ekofuel.org/bioethanol-fuel-for-alcohol-marine-stove/safety-information-1.html">
<span>

								Safety Information
								</span>
</a>
</li>
</ul>
</li>
<li class="level0 nav-1 first level-top parent "><a class="level-top" href="https://ekofuel.org/blitz-spirit-hand-sanitiser.html"><span>Blitz Spirit Hand Sanitiser</span></a>
<ul class="sf-menu" id="left-nav">
<li class="level1 nav-1-1 first">
<a href="https://ekofuel.org/blitz-spirit-hand-sanitiser/100ml-blitz-spirit.html">
<span>

								100ml Blitz Spirit
								</span>
</a>
</li>
<li class="level1 nav-1-1 first">
<a href="https://ekofuel.org/blitz-spirit-hand-sanitiser/500ml-blitz-spirit.html">
<span>

								500ml Blitz Spirit
								</span>
</a>
</li>
<li class="level1 nav-1-1 first">
<a href="https://ekofuel.org/blitz-spirit-hand-sanitiser/mixed-bottles-blitz-spirit.html">
<span>

								Mixed Bottles Blitz Spirit
								</span>
</a>
</li>
</ul>
</li>
<li class="cus_sup level0 nav-1 first level-top parent "><a class="level-top"><span>Customer support</span></a>
<ul class="sf-menu" id="left-nav">
<li class="level1 nav-1-1 first">
<a href="http://www.ekofuel.org/contact-us/">
<span>
								Contact Us								</span>
</a></li>
<li class="level1 nav-1-1 first">
<a href="http://www.ekofuel.org/testimonial">
<span>
								Testimonials								</span>
</a></li>
<li class="level1 nav-1-1 first">
<a href="https://feedback.ebay.co.uk/ws/eBayISAPI.dll?ViewFeedback2&amp;userid=ekofuel&amp;ftab=AllFeedback">
<span>
								eBay Feedback								</span>
</a></li>
<li class="level1 nav-1-1 first">
<a href="http://www.ekofuel.org/faq">
<span>
								FAQs								</span>
</a></li>
<li class="level1 nav-1-1 first">
<a href="http://www.ekofuel.org/where-to-buy-bio-ethanol/">
<span>
								Where to Buy								</span>
</a></li>
<li class="level1 nav-1-1 first">
<a href="http://www.ekofuel.org/trade-sales/">
<span>
								Trade Sales								</span>
</a></li>
<li class="level1 nav-1-1 first">
<a href="http://www.ekofuel.org/blog/">
<span>
								Blog								</span>
</a></li>
<li class="level1 nav-1-1 first">
<a href="http://www.ekofuel.org/video/">
<span>
								Videos								</span>
</a></li>
<li class="level1 nav-1-1 first">
<a href="http://www.ekofuel.org/about-us/">
<span>
								About Us								</span>
</a></li>
<li class="level1 nav-1-1 first">
<a href="http://www.ekofuel.org/bioethanol-fuel-for-fireplaces/bioethanol-collect.html">
<span>
								Collect Bioethanol Fuel								</span>
</a></li>
</ul>
</li>
</ul>
</div>
<div class="clearfix"></div>
</div>
<div class="icon-delivery">
<p><a href="https://ekofuel.org/delivery/" title="next day delivery"><img alt="next day delivery" src="https://ekofuel.org/skin/frontend/default/default/images/icon-delivery.png"/></a></p>
<div class="icon-deliveryshow animated bounceInDown">FOR ORDERS PLACED BEFORE 3.30pm<br/> Click for full <a href="https://ekofuel.org/terms-conditions/#delivery" id="Show_delivery">terms and conditions</a></div>
</div>
<div class="account-Login"><span class="icon-16"></span><a href="https://ekofuel.org/customer/account/login"></a>Account Login</div>
</div>
<div class="main-container col2-left-layout">
<div class="main row">
<div class="col-main span9">
<div class="page-title">
<h1>Ekofuel | 404 Page not Found</h1>
</div>
<div class="std"><div class="page-head-alt">
<h2><span style="color: #663333;"><strong>Ekofuel 404: Page not Found</strong></span></h2>
</div>
<p>Sorry, but the page you were looking for has not been found. </p>
<p>This may be due to an error in the URL or an outdated link. Please check the spelling of the URL and hit the refresh button on your browser.</p>
<p>To get you back on track, please go back to the previous page or use the following links below:</p>
<p> </p>
<p>                                        <a href="http://www.ekofuel.org/index.php/"><img alt="" src="https://ekofuel.org/media/wysiwyg/404errorpage/house.png" width="23"/></a>   <a href="http://www.ekofuel.org/index.php/">Ekofuel Homepage</a></p>
<p>                                        <a href="http://www.ekofuel.org/index.php/browse-by.html/"><img alt="" src="https://ekofuel.org/media/wysiwyg/404errorpage/shopping-basket-2-256.gif" width="25"/></a>  <a href="http://www.ekofuel.org/index.php/browse-by.html/">Continue shopping</a></p>
<p> </p>
<p>If you are still experiencing problems, please call Ekofuel  on 01709 524 162.</p></div> </div>
<div class="col-left sidebar span3"><link href="https://ekofuel.org/skin/frontend/default/default/unibanner/css/glider.css" media="all" rel="stylesheet" type="text/css"/>
<!--[if IE ]>
<link rel="stylesheet" type="text/css" href="https://ekofuel.org/skin/frontend/default/default/unibanner/css/opacityother.css" media="all" />
<![endif]-->
<link href="https://ekofuel.org/skin/frontend/default/default/unibanner/css/noieopacity.css" media="all" rel="stylesheet" type="text/css"/>
<div id="slider">
<style type="text/css">
</style>
<div class="scroller">
<span class="content">
<div class="section" id="section1"><a href="http://www.ekofuel.org/where-to-buy-bio-ethanol/" target="_blank"><img alt="Vertical banner1" id="imgglid1" src="https://ekofuel.org/media/custom/banners/resize/left_banner/File-1548423905.jpg"/></a></div>
<div class="section" id="section2"><a href="http://www.ekofuel.org/bioethanol-fuel-for-fireplaces/bioethanol-collect.html" target="_blank"><img alt="Collect fuel" id="imgglid2" src="https://ekofuel.org/media/custom/banners/resize/left_banner/File-1445941085.jpg"/></a></div>
</span>
</div>
<div class="bannerlinerglider"></div>
<div class="slidercontrol">
<a class="aprev" href="javascript:" onclick="my_glider.previous();return false;" title="Previous">Previous</a>
<a class="astop" href="javascript:" onclick="my_glider.stop();return false" title="Stop">Stop</a>
<a class="aplay" href="javascript:" onclick="my_glider.start();return false" title="Start">Start</a>
<a class="anext" href="javascript:" onclick="my_glider.next();return false" title="Next">Next</a>
</div>
</div><div class="left-cont-r">
</div><p></p></div>
</div>
</div>
<div class="footer-container">
<div class="footer"> <div class="site_link box"><ul>
<li><a href="https://ekofuel.org/"> Home </a></li>
<li><a href="https://ekofuel.org/delivery">Delivery</a></li>
<li><a href="https://ekofuel.org/trade-sales">Commercial clients</a></li>
<li><a href="https://ekofuel.org/privacy-policy">Privacy policy</a></li>
<li><a href="https://ekofuel.org/terms-conditions"> Terms and conditions</a></li>
<li><a href="https://ekofuel.org/vacancies">Vacancies</a></li>
<li><a href="https://ekofuel.org/contact-us">Contact us</a></li>
<li><a href="/sitemap.xml">Sitemap</a></li>
</ul></div>
<div class="newsletter1 box"><form action="https://ekofuel.org/newsletter/subscriber/new/" id="newsletter-validate-detail" method="post">
<div class="form-subscribe">
<label for="newsletter">Sign up to our newsletter now for EkoFuel discounts &amp; price updates</label>
<div class="input-box">
<input class="input-text required-entry validate-email" id="newsletter" name="email" title="Sign up for our newsletter" type="text"/>
<button title="Submit" type="submit">Submit</button>
</div>
</div>
</form>
</div>
<div class="address box"><p>Tel: 01709 524 162</p>
<p>Int: +44 1709 524 162</p>
<p>ekofuel@primaind.co.uk<br/> www.ekofuel.org</p>
<div class="social-icon">
<ul>
<li class="tw"><a href="http://twitter.com/ekofuel">twitter</a></li>
<li class="fb"><a href="http://www.facebook.com/pages/Ekofuel/305796786216674?fref=ts">facebook</a></li>
<li class="yt"><a href="http://www.youtube.com/user/EkoFuel1/feed">youtube</a></li>
</ul>
</div></div>
</div>
</div>
<div class="copyright">
<div class="f-left">
<address>
    Copyright © EkoFuel 2013. All Rights Reserved.
    </address>
</div>
<div class="f-right">
<div class="payment"> <img alt="We Accept all Major Credit Cards" src="https://ekofuel.org/skin/frontend/default/hellowired/images/creditcards.gif"/> </div>
</div>
</div>
</div>
</div>
</div>
<!-- Google Code for Remarketing Tag -->
<!--
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
-->
<noscript>
<div style="display:inline;">
<img alt="" height="1" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1037587671/?value=0&amp;guid=ON&amp;script=0" style="border-style:none;" width="1"/>
</div>
</noscript>
</body>
</html>
<!--[if lt IE 7]>

<script type="text/javascript">

//<![CDATA[

    var BLANK_URL = 'https://ekofuel.org/js/blank.html';

    var BLANK_IMG = 'https://ekofuel.org/js/spacer.gif';

//]]>

</script>

<![endif]-->