<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html dir="ltr" lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Drupal 7 (http://drupal.org)" name="Generator"/>
<link href="http://www.astuin.org/sites/default/files/favicon1.jpg" rel="shortcut icon" type="image/jpeg"/>
<title>Page not found | Astuin.org</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="http://www.astuin.org/sites/all/themes/serenity/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="http://www.astuin.org/sites/all/themes/serenity/css/bootstrap-responsive.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="http://www.astuin.org/sites/all/themes/serenity/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="http://www.astuin.org/sites/all/themes/serenity/css/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<!--[if lte IE 8]> <link type="text/css" rel="stylesheet" href="http://www.astuin.org/sites/all/themes/serenity/css/lte-ie8.css" /> <![endif]-->
<link href="http://www.astuin.org/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://www.astuin.org/sites/default/files/css/css_LNFWCadJXjszADlL3JW8KTAo07Avl-NlJc3AAQNy6E0.css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://www.astuin.org/sites/default/files/css/css_NCOSdpoQnWWQHVcfG2o2skDLf8YSjSAlOAadPoELzbo.css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://www.astuin.org/sites/default/files/css/css_l6aoDIk3lvo7MjK-poRCAoo25SvkJF98HkMaWTCTzHA.css" media="all" rel="stylesheet" type="text/css"/>
<script src="http://www.astuin.org/sites/default/files/js/js_Ua98zxjH9I2U2rTee9IFt0Bz0daoaQQTSnaDxCxt3Kc.js" type="text/javascript"></script>
<script src="http://www.astuin.org/sites/default/files/js/js_R9UbiVw2xuTUI0GZoaqMDOdX0lrZtgX-ono8RVOUEVc.js" type="text/javascript"></script>
<script src="http://www.astuin.org/sites/default/files/js/js_rsGiM5M1ffe6EhN-RnhM5f3pDyJ8ZAPFJNKpfjtepLk.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-94010544-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("set", "page", "/404.html?page=" + document.location.pathname + document.location.search + "&from=" + document.referrer);ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"serenity","theme_token":"lm-aCu3udRrvg63ZMebyaxejr-j6LkW9mxc4APgeUO4","js":{"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery-html-prefilter-3.5.0-backport.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"modules\/forum\/forum.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"sites\/all\/themes\/serenity\/css\/style.css":1,"sites\/all\/themes\/serenity\/css\/color.css":1}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"}});
//--><!]]>
</script>
<script type="text/javascript">jQuery.noConflict();</script>
<script src="http://www.astuin.org/sites/all/themes/serenity/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="http://www.astuin.org/sites/all/themes/serenity/js/idangerous.swiper-2.1.min.js" type="text/javascript"></script>
<script src="http://www.astuin.org/sites/all/themes/serenity/js/default.min.js" type="text/javascript"></script>
<script src="http://www.astuin.org/sites/all/themes/serenity/js/plugins.js" type="text/javascript"></script>
<script src="http://www.astuin.org/sites/all/themes/serenity/js/bootstrap.js" type="text/javascript"></script>
<script src="http://www.astuin.org/sites/all/themes/serenity/js/jquery.fancybox.js" type="text/javascript"></script>
<script src="http://www.astuin.org/sites/all/themes/serenity/js/custom.js" type="text/javascript"></script>
<style type="text/css">
  body {
    background-image: url(http://www.astuin.org/sites/all/themes/serenity/images/none.png);
    background-repeat: repeat;
    font-family: Open Sans, sans-serif;
  }
  a {
    font-family: Open Sans, sans-serif;
    text-decoration: none;
  }
  a:hover {text-decoration: none;}
  .mainMenu li a {
    font-family: Open Sans, sans-serif;
    text-decoration: none;
  }
  .mainMenu li a:hover {text-decoration: none;}
  #footer a {
    font-family: Open Sans, sans-serif;
    text-decoration: none;
  }
  #footer a:hover {text-decoration: none;}
  
  h1 {font-family: Open Sans, sans-serif;}
  h2 {font-family: Open Sans, sans-serif;}
  h3 {font-family: Open Sans, sans-serif;}
  h4 {font-family: Open Sans, sans-serif;}
  h5 {font-family: Open Sans, sans-serif;}
  h6 {font-family: Open Sans, sans-serif;}
</style>
</head>
<body class="html not-front not-logged-in no-sidebars page-navigation404 i18n-en">
<div class="default" id="wrapper">
<div class="otherPage row-fluid" id="page1">
<div class="row-fluid" id="topLine">
<div class="container">
<div class="mail_block">
<p><i class="fa fa-envelope-o"></i>  info@astuin.org | Phone : +62 21 6685301</p>
</div>
<div class="socBox">
<ul class="socIcons">
<li><a href="http://www.twitter.com" target="_blank">
<i class="fa fa-twitter"></i>
</a></li>
<li><a href="http://www.facebook.com" target="_blank">
<i class="fa fa-facebook"></i>
</a></li>
<li><a href="http://www.linkedin.com" target="_blank">
<i class="fa fa-linkedin"></i>
</a></li>
<li><a href="http://www.vimeo.com" target="_blank">
<i class="fa fa-vimeo-square"></i>
</a></li>
<li><a href="http://www.instagram.com" target="_blank">
<i class="fa fa-instagram"></i>
</a></li>
</ul>
</div>
</div>
</div>
<div class="row-fluid" id="staticPanel">
<div class="container">
<div class="logoPanel">
<a href="/" title="Home">
<img alt="Home" src="http://www.astuin.org/sites/default/files/lg1.png"/>
</a>
</div>
<div class="navbar navButton">
<a class="btn btn-navbar" data-target=".main-collapse" data-toggle="collapse">
<i class="fa fa-bars"></i>
</a>
</div>
<div class="mainMenu">
<div class="main-collapse nav-collapse collapse">
<ul class="menu"><li class="first leaf"><a href="/" title="">Home</a></li>
<li class="expanded"><a href="/content/profile">About ASTUIN</a><ul class="menu"><li class="first leaf"><a href="/content/profile">Profile</a></li>
<li class="leaf"><a href="http://astuin.org/content/articles-association" title="">Articles of Association</a></li>
<li class="leaf"><a href="/content/organization-structure">Organization Structure</a></li>
<li class="last leaf"><a href="/content/membership">Membership</a></li>
</ul></li>
<li class="expanded"><a class="nolink" href="#" tabindex="0" title="">Regulations</a><ul class="menu"><li class="first expanded"><a href="/fishing-quota-and-landings" title="">Fishing Quota and Landings</a><ul class="menu"><li class="first leaf"><a href="/content/current-allocation-ccsbts-member-and-cooperating-non-member-2015-2017">Current Allocation CCSBT's Member and to Cooperating Non Member 2015-2017</a></li>
<li class="leaf"><a href="/content/resolution-limited-carry-forward-unfished-annual-total-allowable-catch-southern-bluefin-tuna">Resolution on Limited Carry-forward of Unfished Annual Total Allowable Catch of Southern Bluefin Tuna</a></li>
<li class="leaf"><a href="/content/distribusi-kuota-hasil-tangkap-sbt">Distribusi Kuota Hasil Tangkap SBT</a></li>
<li class="leaf"><a href="/content/petunjuk-teknis-distribusi-alokasi-kuota-nasional-sbt">Petunjuk Teknis Distribusi Alokasi Kuota Nasional SBT</a></li>
<li class="last leaf"><a href="/content/penetapan-asosiasi-dan-distribusi-quota-hasil-tangkapan-sbt-tahun-2015">Penetapan Asosiasi dan Distribusi Quota Hasil Tangkapan SBT Tahun 2015</a></li>
</ul></li>
<li class="expanded"><a href="/domestic" title="">Domestic</a><ul class="menu"><li class="first leaf"><a href="/content/pp-no-9-tahun-2018-tata-cara-pengendalian-impor-komoditas-perikanan">PP No. 9 Tahun 2018 Tata Cara Pengendalian Impor Komoditas Perikanan</a></li>
<li class="leaf"><a href="/content/permen-kp-no-39-tahun-2017-kartu-pelaku-usaha-kelautan-dan-perikanan" title="">PERMEN-KP No. 39 Tahun 2017 Kartu Pelaku Usaha Kelautan dan Perikanan</a></li>
<li class="leaf"><a href="/content/perpres-no-3-tahun-2017-rencana-aksi-percepatan-pembangunan-industri-perikanan-nasional">Perpres No. 3 Tahun 2017 Rencana Aksi Percepatan Pembangunan Industri Perikanan Nasional</a></li>
<li class="leaf"><a href="/content/lampiran-perpres-no-3-tahun-2017-distribusi">Lampiran Perpres No. 3 Tahun 2017 - Distribusi</a></li>
<li class="leaf"><a href="/content/inpres-no-7-tahun-2016-percepatan-pembangunan-industri-perikanan-nasional">Inpres No. 7 Tahun 2016 Percepatan Pembangunan Industri Perikanan Nasional</a></li>
<li class="leaf"><a href="/content/perpres-no44-tahun-2016-daftar-negatif-investasi-dni" title="">Perpres No. 44 Tahun 2016 Daftar Negatif Investasi (DNI)</a></li>
<li class="leaf"><a href="/content/sk-djpt-tentang-petugas-validasi-tuna-mata-besar-ikan-pedang-produk-tuna-beku-dan-olahan">SK DJPT Tentang Petugas Validasi Tuna Mata Besar, Ikan Pedang, Produk Tuna Beku dan Olahan Tuna</a></li>
<li class="leaf"><a href="/content/ketentuan-pendaftaranperpanjangan-keanggotaan-kapal-di-rfmo">Ketentuan Pendaftaran/Perpanjangan Keanggotaan Kapal di RFMO</a></li>
<li class="leaf"><a href="/content/permen-kp-no-45-tahun-2015-rencana-strategis-kkp-tahun-2015-2019" title="">PERMEN-KP No. 45 Tahun 2015 Rencana Strategis KKP Tahun 2015 - 2019</a></li>
<li class="leaf"><a href="/content/permen-kp-no-4-tahun-2015-larangan-penangkapan-ikan-di-wpp-nri-714">PERMEN-KP No. 4 Tahun 2015 Larangan Penangkapan Ikan di WPP-NRI 714</a></li>
<li class="leaf"><a href="/content/pp-ri-no-75-tahun-2015-jenis-dan-tarif-atas-jenis-penerimaan-negara-bukan-pajak-yang-berlaku">PP RI No. 75 Tahun 2015 Jenis Dan Tarif Atas Jenis Penerimaan Negara Bukan Pajak Yang Berlaku Pada Kementrian Kelautan Dan Perikanan</a></li>
<li class="leaf"><a href="/content/permen-kp-no-58-tahun-2014-disiplin-pegawai-aparatur-sipil-negara-di-lingkungan-kkp-terkait">PERMEN-KP No. 58 Tahun 2014 Disiplin Pegawai Aparatur Sipil Negara Di Lingkungan KKP Terkait Moratorium, Transhipment, Penggunaan Nak</a></li>
<li class="leaf"><a href="/content/permen-kp-no-57-tahun-2014-menghentikan-kegiatan-transhipment-di-laut-wpp-nri">PERMEN-KP No. 57 Tahun 2014 Menghentikan Kegiatan Transhipment Di Laut WPP-NRI</a></li>
<li class="leaf"><a href="/content/permen-kp-no-56-tahun-2014-moratorium-usaha-perikanan-tangkap-di-wpp-nri">PERMEN-KP No. 56 Tahun 2014 Moratorium Usaha Perikanan Tangkap di WPP-NRI</a></li>
<li class="leaf"><a href="/content/permen-kp-no-18-tahun-2014-wilayah-pengelolaan-perikanan-negara-republik-indonesia">PERMEN-KP No. 18 Tahun 2014 Wilayah Pengelolaan Negara Perikanan Republik Indonesia</a></li>
<li class="leaf"><a href="/content/permen-kp-no-per30men2012-usaha-perikanan-tangkap-di-wpp-nri">PERMEN-KP No. PER.30/MEN/2012 Usaha Perikanan Tangkap di WPP-NRI</a></li>
<li class="leaf"><a href="/content/permen-kp-no-per12men2012-usaha-perikanan-tangkap-di-laut-lepas">PERMEN-KP No. PER.12/MEN/2012 Usaha Perikanan Tangkap di Laut Lepas</a></li>
<li class="last leaf"><a href="/content/uu-no-31-tahun-2004-tentang-perikanan">UU No. 31 Tahun 2004 Tentang Perikanan</a></li>
</ul></li>
<li class="last expanded"><a href="/international" title="">International</a><ul class="menu"><li class="first leaf"><a href="/content/resolution-1406-establishing-programme-transhipment-large-scale-fishing-vessels-iotc">IOTC Resolution 14/06 On establishing a programme for transhipment by large-scale fishing vessels | IOTCCMM 14/06</a></li>
<li class="leaf"><a href="/content/resolution-1404-concerning-iotc-record-vessels-authorised-operate-iotc-area-competence">Resolution 14/04 Concerning The IOTC Record Of Vessels Authorised To Operate In The IOTC Area Of Competence</a></li>
<li class="leaf"><a href="/content/informasi-pemberlakuan-nomor-imo-untuk-kapal-kapal-yang-tercantum-pada-rfmo-record-fishing">Informasi Pemberlakuan Nomor IMO Untuk Kapal-Kapal yang Tercantum pada RFMO Record of Fishing Vessels</a></li>
<li class="leaf"><a href="/content/standards-specifications-and-procedures-western-and-central-pacific-fisheries-commision">Standards, Specifications And Procedures For The Western And Central Pacific Fisheries Commision Record Of Fishing Vessels</a></li>
<li class="last leaf"><a href="/content/iotc-procedure-requesting-observer-deployment">IOTC Procedure for Requesting Observer Deployment</a></li>
</ul></li>
</ul></li>
<li class="expanded"><a href="/content/sustainability">Sustainability</a><ul class="menu"><li class="first leaf"><a href="/content/latest-status-summary-species-tuna-under-iotc-2016" title="">The Latest Status Summary for Species of Tuna under IOTC 2016</a></li>
<li class="last leaf"><a href="http://m.bisnis.com/industri/read/20170220/99/630005/tim-ilmiah-tuna-belum-di-ambang-kepunahan" title="">Tuna Belum Di Ambang Kepunahan</a></li>
</ul></li>
<li class="expanded"><a class="nolink" href="#" tabindex="0" title="">Statistics</a><ul class="menu"><li class="first expanded"><a class="nolink" href="#" tabindex="0" title="">Market</a><ul class="menu"><li class="first leaf"><a href="/content/fda-refusals-indonesian-tuna-and-seafood-products-2015-july-2020" title="Silahkan hubungi sekretariat ASTUIN untuk data yang paling update">FDA Refusals of Indonesian Tuna and Seafood Products 2015 - July 2020</a></li>
<li class="leaf"><a href="/content/japan-tuna-import-all-countries-2012-2017">Japan Tuna Import From All Countries 2012 - 2017</a></li>
<li class="leaf"><a href="/content/japan-all-tuna-import-indonesia-2012-2017" title="">Japan Tuna Import From Indonesia 2012 - 2017</a></li>
<li class="leaf"><a href="/content/japan-fresh-and-frozen-tuna-import-indonesia-2012-2017-excl-skipjack">Japan Fresh and Frozen Tuna Import from Indonesia 2012 - 2017 (excl. Skipjack)</a></li>
<li class="leaf"><a href="/content/us-tuna-imports-2012-2017">U.S. Tuna Imports 2012 - 2017</a></li>
<li class="leaf"><a href="/content/us-import-all-tuna-asean-countries-2012-2017">U.S. Tuna Import from ASEAN Countries 2012 - 2017</a></li>
<li class="leaf"><a href="/content/world-top-fishery-and-tuna-exporters-2012-2016" title="">World Top Fishery and Tuna Exporters 2012 - 2016</a></li>
<li class="leaf"><a href="/content/indonesian-tuna-exports-2014-q1-april-2017">Indonesian Tuna Exports 2014- Q1 (April) 2017</a></li>
<li class="leaf"><a href="/content/fda-import-refusal-indonesian-tuna-and-all-fishery-products-2016">FDA Import Refusal Of Indonesian Tuna and All Fishery Products 2016</a></li>
<li class="leaf"><a href="/content/fda-import-refusal-report-indonesian-tuna-others-seafood-products-2015">FDA Import Refusal Report of Indonesian Tuna &amp; Others Seafood Products (2015)</a></li>
<li class="leaf"><a href="/content/us-imports-all-seafood-products-indonesia-2014-2016">U.S. Imports of All Seafood Products from Indonesia 2014-2016</a></li>
<li class="leaf"><a href="/content/us-imports-all-tuna-products-indonesia-amount-kilograms-jan-jul-2016">U.S. Imports of All Tuna Products from Indonesia by Amount in Kilograms (Jan-Jul 2016)</a></li>
<li class="leaf"><a href="/content/us-imports-all-tuna-products-indonesia-value-usd-jan-jul-2016">U.S. Imports of All Tuna Products from Indonesia by Value in USD (Jan-Jul 2016)</a></li>
<li class="leaf"><a href="/content/us-import-tuna-indonesia-1st-semester-annual-2013-2016">US Import of Tuna from Indonesia (1st semester annual 2013-2016)</a></li>
<li class="leaf"><a href="/content/market-analysis-and-fda-import-refusal-report-2006-2012">Market Analysis and FDA Import Refusal Report 2006-2012</a></li>
<li class="leaf"><a href="/content/asean-fishery-exports-and-imports-ranked-value-2011-2015">ASEAN Fishery Exports and Imports Ranked By Value 2011-2015</a></li>
<li class="last leaf"><a href="/content/world%E2%80%99s-largest-fishery-exporting-and-importing-countries-2011-2015">World’s Largest Fishery Exporting and Importing Countries 2011-2015</a></li>
</ul></li>
<li class="last expanded"><a class="nolink" href="#" tabindex="0" title="">Fishing</a><ul class="menu"><li class="first leaf"><a href="/content/threat-against-indonesia%E2%80%99s-freedom-fishing-3-rfmos">The Threat Against Indonesia’s Freedom of Fishing in 3 RFMOs'</a></li>
<li class="expanded"><a class="nolink" href="#" tabindex="0" title="">Comparison Analysis Between Indonesia and Other RFMOs' Members, 12/10/2016 </a><ul class="menu"><li class="first leaf"><a href="/content/iotc">IOTC</a></li>
<li class="leaf"><a href="/content/ccsbt">CCSBT</a></li>
<li class="last leaf"><a href="/content/wcpfc">WCPFC</a></li>
</ul></li>
<li class="leaf"><a href="/content/currently-active-indonesian-vessels-based-momaf-license-12102016">Currently Active Indonesian Vessels based on MOMAF License 12/10/2016</a></li>
<li class="last leaf"><a href="/content/latest-update-iotc-authorized-vessels-23092016">Latest Update IOTC  Authorized Vessels 23/09/2016</a></li>
</ul></li>
</ul></li>
<li class="expanded"><a class="nolink" href="#" tabindex="0" title="">Opinion</a><ul class="menu"><li class="first leaf"><a href="/content/labirin-sumber-daya-ikan">Labirin Sumber Daya Ikan</a></li>
<li class="leaf"><a href="/content/quo-vadis-tuna-indonesia" title="#HendraSugandhi
#EkportirTunaTerbesarDunia
#KKP
#Tuna
#SusiPudjiastuti
#QuoVadisTunaIndonesia">Quo Vadis Tuna Indonesia</a></li>
<li class="leaf"><a href="/content/dialektika-perikanan-indonesia" title="#HendraSugandhi
#Perikanan
#KKP
#Jokowi
#SusiPudjiastuti
#DialektikaPerikananIndonesia">Dialektika Perikanan Indonesia</a></li>
<li class="leaf"><a href="/content/perikanan-nasional-fatamorgana-potensi-perikanan">PERIKANAN NASIONAL : Fatamorgana Potensi Perikanan</a></li>
<li class="leaf"><a href="/content/paradoks-kebijakan-tuna-0" title="echnology-indonesia.com/lain-lain/umum-lain-lain/menristekdikti-serahkan-piala-bergilir-lomba-penulisan-iptek/">Paradoks Kebijakan Tuna</a></li>
<li class="leaf"><a href="/content/paradox-tuna-policy">Paradox of Tuna Policy</a></li>
<li class="leaf"><a href="/content/paradoks-infografis">Paradoks Infografis</a></li>
<li class="last leaf"><a href="/content/indonesia-kehilangan-devisa-5-miliar-yen-tahun-karena-tidak-memanfaatkan-laut-lepas">Indonesia Kehilangan Devisa 5 Miliar Yen Per Tahun Karena Tidak Memanfaatkan Laut Lepas</a></li>
</ul></li>
<li class="expanded"><a href="/blog" title="">News</a><ul class="menu"><li class="first leaf"><a href="/content/sumber-daya-ikan-pemanfaatan-makin-tidak-optimal" title="#hendrasugandhi #perikanan #asosiasi tuna indonesia #astuin #sumberdaya #ikan">Sumber Daya Ikan : Pemanfaatan Makin Tidak Optimal</a></li>
<li class="leaf"><a href="/content/industri-perikanan-jerman-siap-mendukung-kerjasama-peningkatan-perdagangan-dan-investasi-di">Industri Perikanan Jerman Siap Mendukung Kerjasama Peningkatan Perdagangan dan Investasi di Sektor Perikanan Indonesia</a></li>
<li class="leaf"><a href="/content/ekspor-tuna-terganjal-ke-pebisnis-minta-kepastian">EKSPOR TUNA: Terganjal ke AS, Pebisnis Minta Kepastian</a></li>
<li class="leaf"><a href="/content/seminar-dan-temu-bisnis-%E2%80%98opportunities-indonesia%E2%80%99s-fisheries-sector%E2%80%99">Seminar dan Temu Bisnis ‘Opportunities in Indonesia’s Fisheries Sector’</a></li>
<li class="leaf"><a href="/content/kapal-beralat-tangkap-ikan-ramah-lingkungan-susut-dalam-2-tahun">Kapal Beralat Tangkap Ikan Ramah Lingkungan Susut dalam 2 Tahun</a></li>
<li class="leaf"><a href="/content/duh-bobot-kapal-ikan-indonesia-terus-turun">Duh, Bobot Kapal Ikan Indonesia Terus Turun</a></li>
<li class="leaf"><a href="/content/asosiasi-tuna-indonesia-ekspor-ikan-jeblok-rp-114-triliun">Asosiasi Tuna Indonesia: Ekspor Ikan Jeblok Rp 11,4 Triliun</a></li>
<li class="leaf"><a href="/content/ekspor-perikanan-naik-signifikan-menteri-susi-merinding">Ekspor Perikanan Naik Signifikan, Menteri Susi Merinding</a></li>
<li class="leaf"><a href="/content/penurunan-ekspor-perikanan-indonesia-capai-rp11-triliun">Penurunan Ekspor Perikanan Indonesia Capai Rp11 Triliun</a></li>
<li class="leaf"><a href="http://m.bisnis.com/industri/read/20170220/99/630005/tim-ilmiah-tuna-belum-di-ambang-kepunahan" title="">Tuna Belum Terancam Punah</a></li>
<li class="leaf"><a href="/content/menteri-susi-pertanyakan-kuota-tuna-negara-tak-berpantai">Menteri Susi Pertanyakan Kuota Tuna Negara Tak Berpantai</a></li>
<li class="leaf"><a href="/content/astuin-kuota-tuna-ri-di-samudra-pasifik-percuma">Astuin: Kuota Tuna RI di Samudra Pasifik Percuma</a></li>
<li class="leaf"><a href="/content/kapal-ikan-eks-asing-terancam-mangkrak-ini-usulan-astuin">Kapal Ikan Eks-Asing Terancam Mangkrak, Ini Usulan Astuin</a></li>
<li class="leaf"><a href="/content/pengusaha-tuna-minta-pemerintah-adil-terhadap-industri">Pengusaha Tuna Minta Pemerintah Adil Terhadap Industri</a></li>
<li class="leaf"><a href="/content/menelusuri-keberadaan-tuna-yang-terancam-punah-di-indonesia" title="Pemanfaatan sumber daya di laut lepas terabaikan sehingga kedaulatan negara justru yang terancam punah">Menelusuri Keberadaan Tuna yang Terancam Punah di Indonesia</a></li>
<li class="last leaf"><a href="/content/perikanan-tangkap-ekspor-tuna-kian-loyo">Perikanan Tangkap : Ekspor Tuna Kian Loyo</a></li>
</ul></li>
<li class="leaf"><a href="/gallery-4-column" title="">Photos Gallery</a></li>
<li class="leaf"><a href="/video">Video</a></li>
<li class="leaf"><a href="/content/resources-links">Resources &amp; Links</a></li>
<li class="leaf"><a href="/content/donations">Donations</a></li>
<li class="last leaf"><a href="/contact" title="">Contact Us</a></li>
</ul> </div>
</div>
</div>
</div> <!-- staticPanel -->
<div class="container contBox">
<div class="gCont">
<div class="region region-content">
<div class="block block-system" id="block-system-main">
<div class="content clearfix">
    The requested page could not be found.  </div>
</div> </div>
</div>
</div>
</div> <!-- page1 -->
<div class="row-fluid" id="footer">
<div class="socBox">
<ul class="socIcons">
<li><a href="http://www.twitter.com" target="_blank">
<i class="fa fa-twitter"></i>
</a></li>
<li><a href="http://www.facebook.com" target="_blank">
<i class="fa fa-facebook"></i>
</a></li>
<li><a href="http://www.linkedin.com" target="_blank">
<i class="fa fa-linkedin"></i>
</a></li>
<li><a href="http://www.vimeo.com" target="_blank">
<i class="fa fa-vimeo-square"></i>
</a></li>
<li><a href="http://www.instagram.com" target="_blank">
<i class="fa fa-instagram"></i>
</a></li>
</ul>
</div>
<div class="copyright">
<a href="http://www.greywhiteandblack.com " target="_blank">
				    Copyright © 2021 Astuin.org | Web Develop By GWAB 				</a>
</div>
<div class="switchButton">
<span class="bt-blue scrollup"></span>
<span class="bt-lghtGre scrollup"></span>
<span class="bt-orange scrollup"></span>
<span class="bt-red scrollup"></span>
<span class="bt-green scrollup"></span>
<span class="bt-purple scrollup"></span>
</div>
</div> <!--footer-->
</div> <!--wrapper--> </body>
</html>