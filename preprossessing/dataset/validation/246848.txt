<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]--><!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]--><!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<title></title>
<meta content="" name="description"/>
<meta content="width=device-width" name="viewport"/>
<link href="http://boileri-matev.com/catalog/view/theme/default/css/normalize.min.css" rel="stylesheet"/>
<link href="http://boileri-matev.com/catalog/view/theme/default/css/main.css" rel="stylesheet"/>
<link href="http://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css"/>
<link href="http://boileri-matev.com/catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css"/>
<script src="http://boileri-matev.com/catalog/view/theme/default/js/vendor/modernizr-2.6.2.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
<script src="http://boileri-matev.com/catalog/view/javascript/common.js" type="text/javascript"></script>
<link href="http://boileri-matev.com/catalog/view/theme/default/css/cookie.css" rel="stylesheet"/>
<script src="http://boileri-matev.com/catalog/view/theme/default/js/jquery.cookie.js"></script>
<script src="http://boileri-matev.com/catalog/view/theme/default/js/main-cookie.js"></script>
</head>
<body>
<!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
<header>
<div class="full center relative">
<section class="left" id="hLeft">
<div class="tCenter" id="workHours">
                        Обадете се Понеделник – Петък: 8:30ч. – 18:00ч.                    </div>
<div class="tCenter" id="logo">
<a href="http://boileri-matev.com/"><img alt="" src="http://boileri-matev.com/catalog/view/theme/default/img/logo.png"/></a>
<div class="contacts">
<a class="white" href="tel:+35937122264">+359 3712 22 64</a>
</div>
</div>
</section>
<section class="right" id="rightSide">
<div class="button left"></div>
<input type="text"/>
<a class="fbImg" href="#"></a>
</section>
<div class="clearfix"></div>
<nav class="mainNav">
<a href="http://boileri-matev.com/index.php?route=common/home">Начало</a>
<a href="http://boileri-matev.com/емайлиран-вертикален ">Продукти</a>
<a href="http://boileri-matev.com/about_us">За Нас</a>
<a href="http://boileri-matev.com/services">Сервизи</a>
<a href="http://boileri-matev.com/index.php?route=news/archive">Интересно</a>
<a class="last" href="http://boileri-matev.com/index.php?route=information/contact">Контакти</a>
</nav>
</div>
</header>
<div class="full center">
<div class="left">
<div class="lang">
<form action="http://boileri-matev.com/index.php?route=module/language" enctype="multipart/form-data" method="post">
<div class="lang" id="language">
<a class="bg" onclick="$('input[name=\'language_code\']').attr('value', 'bg'); $(this).parent().parent().submit();">BG</a>
<a class="en" onclick="$('input[name=\'language_code\']').attr('value', 'en'); $(this).parent().parent().submit();">EN</a>
<input name="language_code" type="hidden" value=""/>
<input name="redirect" type="hidden" value="http://boileri-matev.com/index.php?route=error/not_found"/>
</div>
</form>
</div>
</div>
<div class="right subMenu">
<div class="left">
<div class="left">
</div>
<div class="right currency">
</div>
</div>
<div class="right cart-total">
                                     </div>
</div>
</div>
<div id="notification"></div>
<div class="clearfix"></div>
<div class="full center">
<h1 class="blue">Заявената страница не бе намерена!</h1>
</div>
<section class="full featured center">
<strong class="blue">За съжаление, заявената от вас страница не бе намерена. Може би е била премахната или сме променили адреса й. Вероятно, използването на търсачката в магазина ще ви помогне да откриете търсеното.</strong>
</section>
<div class="clearfix"></div>
<hr class="minimal"/>
<div class="bigTitle red center tCenter">
            Бойлери <span class="accent">Матев ООД</span> </div>
<footer>
<nav class="footerNav center full">
<a href="http://boileri-matev.com/index.php?route=common/home">Начало</a>
<a href="about_us">За Нас</a>
<a href="services">Сервизи</a>
<a href="news/archive">Интересно</a>
<a class="last" href="http://boileri-matev.com/index.php?route=information/contact">Контакти</a>
</nav>
<div class="lower">
<div class="center full">
<ul>
<li><a href="#">Бойлери</a></li>
<li><a href="http://boileri-matev.com//index.php?route=product/category&amp;path=82">Камини за твърдо гориво</a></li>
<li><a href="http://boileri-matev.com//index.php?route=product/category&amp;path=81">Разширителни буферни съдове</a></li>
<li><a href="http://boileri-matev.com//index.php?route=information/contact">Услуги по заявка на клиента</a></li>
<li><a class="fancybox fancybox.iframe" href="http://www.slideshare.net/slideshow/embed_code/31577366?rel=0">Рекламни материали</a></li>
<!-- <li><a href="http://boileri-matev.com/емайлиран-вертикален ">Емайлиран вертикален </a></li> -->
<!-- <li><a href="http://boileri-matev.com/Емайлиран-с-топлообменник-вертикален">Емайлиран с топлообменник вертикален</a></li> -->
<!-- <li><a href="http://boileri-matev.com/боилери-с-два-топлообменника">Емайлиран с два топлообменника вертикален</a></li> -->
<!-- <li><a href="http://boileri-matev.com/Емайлиран-хоризонтален">Емайлиран хоризонтален</a></li> -->
<!-- <li><a href="http://boileri-matev.com/hrom-nickel-vertikalen">Хром-никел вертикален</a></li> -->
<!-- <li><a href="http://boileri-matev.com/hrom-nickel-exchanger-vertical">Хром-никел с топлообменник вертикален</a></li> -->
<!-- <li><a href="http://boileri-matev.com/hrom-nickel-two-exchangers">Хром-никел с два топлообменника</a></li> -->
<!-- <li><a href="http://boileri-matev.com/Поцинкован-вертикален">Поцинкован вертикален</a></li> -->
<!-- <li><a href="http://boileri-matev.com/index.php?route=product/category&amp;path=81">Разширителни буферни съдове</a></li> -->
<!-- <li><a href="http://boileri-matev.com/index.php?route=product/category&amp;path=82">Камини за твърдо гориво</a></li> -->
</ul>
<div class="clearfix"></div>
</div>
</div>
<div class="bottom">
<div class="full center">
<div class="left">
                        © Copyright 2010 MATEV.BG | Всички права запазени.                    </div>
<div class="right">
                        Designed by Anjela advertising.
                    </div>
</div>
</div>
</footer>
<script src="http://boileri-matev.com/catalog/view/theme/default/js/plugins.js"></script>
<script src="http://boileri-matev.com/catalog/view/theme/default/js/main.js"></script>
<div id="cookie_policy_button"><div>Политика за бисквитки</div></div>
<div id="cookie_policy_box">
<div class="cookie_policy_text">
    Този уебсайт използва "бисквитки" (cookies). Без тях основни части от уебсайта няма да функционират правилно.<br/>
<a href="cookies-policy.html" target="_blank">Политика за бисквитки</a><br/>
<a href="privacy-policy.html" target="_blank">Политика за лични данни</a><br/>
</div>
<div class="cookie_policy_buttons">
<input id="cookies_accept" name="cookies_accept" type="button" value="Разбрах"/>
</div>
</div>
</body>
</html>
