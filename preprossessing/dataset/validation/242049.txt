<!DOCTYPE html>
<html>
<head>
<base href="/"/>
<title>Blue Book of Gun Values</title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/manifest.json" rel="manifest"/>
<link color="#5bbad5" href="/safari-pinned-tab.svg" rel="mask-icon"/>
<meta content="#ffffff" name="theme-color"/>
<meta content="Finding the Blue Book value of your new and used firearms, including pistols, rifles, shotguns, airguns, and blackpowder guns is easy with the number one source of gun pricing. This site provides values and information on firearms in a convenient online pricing guide format, and allows you to find out what your used guns are worth." name="description"/>
<meta content="gun, firearm, airgun, black powder, value, price, blue book, used, antique, gun value, gun price, gun blue book, firearm value, firearm price, firearm blue book, Blue Book of Gun Values, Blue Book of Airguns, Blue Book of Modern Black Powder Arms, airgun price, airgun value, powder price, powder value, Beretta, Browning, Colt, Remington, Ruger, Winchester" name="keywords"/>
<link href="Content/bootstrap.min.css" rel="stylesheet"/>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet"/>
<link href="dist/app.4024e12117d80b0222a5.css" rel="stylesheet"/>
<script async="" data-ad-client="ca-pub-5547792956585020" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<body>
<my-app>Loading Blue Book of Gun Values...</my-app>
<script crossorigin="anonymous" integrity="sha256-C6CB9UYIS9UJeqinPHWTHVqh/E1uhG5Twh+Y5qFQmYg=" src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="dist/polyfills.4024e12117d80b0222a5.js" type="text/javascript"></script><script src="dist/vendor.4024e12117d80b0222a5.js" type="text/javascript"></script><script src="dist/app.4024e12117d80b0222a5.js" type="text/javascript"></script></body>
</html>