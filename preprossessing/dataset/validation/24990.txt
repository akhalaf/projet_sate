<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>ООО «Газпром межрегионгаз Волгоград» - Главная страница</title>
<link href="https://34regiongaz.ru/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="https://34regiongaz.ru/index.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="https://34regiongaz.ru/include/JsHttpRequest/JsHttpRequest.js" type="text/javascript"></script>
<script src="https://34regiongaz.ru/include/jquery/vendor/jquery-1.9.1.min.js" type="text/javascript"></script>
<script language="javascript" src="https://34regiongaz.ru/include/functions.js" type="text/javascript"></script>
<link href="https://34regiongaz.ru/include/jquery/thickbox.css" rel="stylesheet" type="text/css"/>
<script src="https://34regiongaz.ru/include/jquery/thickbox.js" type="text/javascript"></script>
<script src="https://34regiongaz.ru/include/jquery/jtip.js" type="text/javascript"></script>
<script src="https://34regiongaz.ru/include/jquery/jquery.maskedinput.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $(".ls").mask("9999999999");
    $(".dat").mask("99.99.9999");
    $(".ser").mask("99 99");
    $(".num").mask("999999");
    $(".inn").mask("999999999999");
    $(".cod").mask("999-999");
    $(".phone-mobile").mask("(999) 999-99-99");
  });
</script>
</head>
<body>
<div id="container">
<div class="header">
<div class="menu-logo">
<div class="logo"><img alt="Волгоградрегионгаз" border="0" height="108" src="images/index-logo.gif" title="Волгоградрегионгаз" width="181"/></div>
<div class="top-menu"><a class="sitemap" href="https://34regiongaz.ru/mapsite/">карта сайта</a> <a class="rss" href="https://34regiongaz.ru/rss/">rss</a></div>
<div class="menu">
<div>
<a href="https://34regiongaz.ru/o-kompanii/">О компании</a>
<a href="https://34regiongaz.ru/informatsiya-dlya-potrebiteley/">Информация для потребителей</a>
<a href="https://34regiongaz.ru/zakupki/">Закупки</a>
<a href="https://34regiongaz.ru/sotsialnyie-proektyi/">Социальные проекты</a>
<a href="https://34regiongaz.ru/rukovodstvo/">Пресс-центр</a>
<a href="https://34regiongaz.ru/dopolnitelnyie-uslugi/">Дополнительные услуги</a>
<a href="https://34regiongaz.ru/debtors/">Должники</a>
<a href="https://34regiongaz.ru/obratnaya-svyaz/">Обратная связь</a>
<a href="https://34regiongaz.ru/realizatsiya-neprofilnyih-aktivov/">Реализация непрофильных активов</a>
</div> </div> <div class="lcab"><a href="https://34regiongaz.ru/choice/"><img src="https://34regiongaz.ru/images/lk_button.png"/></a></div>
</div>
<div class="slogan-search">
<div class="slogan"><p>В интересах отрасли, на благо общества!</p></div>
<div class="search">
<form action="https://34regiongaz.ru/search/" method="post" name="mfsearch">
<input name="searchstring" onfocus="this.value='';" onkeydown="javascript:if(event.keyCode==13) document.forms['mfsearch'].submit();" type="text" value="поиск по сайту"/><a href="#" onclick="document.forms['mfsearch'].submit();">найти</a>
</form>
</div>
</div>
</div>
<div class="bodypage">
<div class="leftside">
<h1>Новости</h1>
<dl class="news">
<dt>29 декабря 2020</dt>
<dd class="img"><a href="https://34regiongaz.ru/rukovodstvo/news/1830"><img border="0" height="70" src="https://34regiongaz.ru/contents/modules/_getimgpreview.php?f=gustov_001.jpg&amp;w=70&amp;h=70&amp;t=png&amp;q=" width="70"/></a></dd>
<dd class="news-title"><a href="https://34regiongaz.ru/rukovodstvo/news/1830">На вопросы журнала «ГАЗПРОМ» отвечает генеральный директор «Газпром межрегионгаз» Сергей Густов</a></dd>
<dd class="text">– Сергей Вадимович, в этом году завершается очередной этап в газификации регионов России. Какие результаты получены? Каких целевых показателей...</dd>
</dl>
<dl class="news">
<dt>28 декабря 2020</dt>
<dd class="img"><a href="https://34regiongaz.ru/rukovodstvo/news/1829"><img border="0" height="70" src="https://34regiongaz.ru/contents/modules/_getimgpreview.php?f=gustov_000.jpg&amp;w=70&amp;h=70&amp;t=png&amp;q=" width="70"/></a></dd>
<dd class="news-title"><a href="https://34regiongaz.ru/rukovodstvo/news/1829">Поздравление генерального директора ООО "Газпром межрегионгаз" С.В. Густова с Новым годом и Рождеством</a></dd>
<dd class="text">Уважаемые коллеги, дорогие друзья! 
 От всей души поздравляю вас с наступающим Новым Годом и Рождеством!

Подводя  итоги, я с гордостью говорю,...</dd>
</dl>
<dl class="news">
<dt>28 декабря 2020</dt>
<dd class="img"><a href="https://34regiongaz.ru/rukovodstvo/news/1827"><img border="0" height="70" src="https://34regiongaz.ru/contents/modules/_getimgpreview.php?f=w300_b638e2_000.jpg&amp;w=70&amp;h=70&amp;t=png&amp;q=" width="70"/></a></dd>
<dd class="news-title"><a href="https://34regiongaz.ru/rukovodstvo/news/1827">Поздравление Председателя Правления ПАО «Газпром» А.Б. Миллера в адрес коллектива компании с Новым годом</a></dd>
<dd class="text">Уважаемые коллеги! Дорогие друзья!
От имени Правления ПАО «Газпром» и от себя лично сердечно поздравляю вас с наступающим Новым...</dd>
</dl>
<dl class="news">
<dt>24 декабря 2020</dt>
<dd class="img"><a href="https://34regiongaz.ru/rukovodstvo/news/1824"><img border="0" height="70" src="https://34regiongaz.ru/contents/modules/_getimgpreview.php?f=gaz_ng.jpg&amp;w=70&amp;h=70&amp;t=png&amp;q=" width="70"/></a></dd>
<dd class="news-title"><a href="https://34regiongaz.ru/rukovodstvo/news/1824">«Газпром межрегионгаз Волгоград» напоминает своим абонентам о важной новогодней традиции</a></dd>
<dd class="text">Существует добрая примета: чтобы весь год был успешным, нужно вступать в Новый год без долгов, встречая теплый семейный праздник в атмосфере хорошего...</dd>
</dl>
<dl class="news">
<dt>23 декабря 2020</dt>
<dd class="img"><a href="https://34regiongaz.ru/rukovodstvo/news/1825"><img border="0" height="70" src="https://34regiongaz.ru/contents/modules/_getimgpreview.php?f=i_gaz.jpg&amp;w=70&amp;h=70&amp;t=png&amp;q=" width="70"/></a></dd>
<dd class="news-title"><a href="https://34regiongaz.ru/rukovodstvo/news/1825">В Волгоградской области завершено строительство всех объектов газификации 2020 года</a></dd>
<dd class="text">Более 90 километров газопроводов построено в уходящем году в Волгоградской области — возможность подключения к голубому топливу получили более...</dd>
</dl>
<dl class="news">
<dt>18 декабря 2020</dt>
<dd class="img"><a href="https://34regiongaz.ru/rukovodstvo/news/1822"><img border="0" height="70" src="https://34regiongaz.ru/contents/modules/_getimgpreview.php?f=_.jpg&amp;w=70&amp;h=70&amp;t=png&amp;q=" width="70"/></a></dd>
<dd class="news-title"><a href="https://34regiongaz.ru/rukovodstvo/news/1822">О ликвидации несанкционированных подключений!</a></dd>
<dd class="text">В рамках проведения проверочных мероприятий ООО "Газпром межрегионгаз Волгоград" и ООО "Газпром газораспределение Волгоград" 01 и...</dd>
</dl>
<dl class="news">
<dt>15 декабря 2020</dt>
<dd class="img"><a href="https://34regiongaz.ru/rukovodstvo/news/1821"><img border="0" height="70" src="https://34regiongaz.ru/contents/modules/_getimgpreview.php?f=15.12.2020-mrg-02.jpg&amp;w=70&amp;h=70&amp;t=png&amp;q=" width="70"/></a></dd>
<dd class="news-title"><a href="https://34regiongaz.ru/rukovodstvo/news/1821">В Волгоградской области ведется подготовка к газификации социальных объектов</a></dd>
<dd class="text">В Волгоградской области началась подготовка к выполнению Программы развития газоснабжения и газификации Волгоградской области на период...</dd>
</dl>
<dl class="news">
<dt>09 декабря 2020</dt>
<dd class="img"><a href="https://34regiongaz.ru/rukovodstvo/news/1823"><img border="0" height="70" src="https://34regiongaz.ru/contents/modules/_getimgpreview.php?f=co_getimgpreview.jpg&amp;w=70&amp;h=70&amp;t=png&amp;q=" width="70"/></a></dd>
<dd class="news-title"><a href="https://34regiongaz.ru/rukovodstvo/news/1823">ООО «Газпром межрегионгаз Волгоград» напоминает о необходимости строгого соблюдения правил эксплуатации газового оборудования!</a></dd>
<dd class="text">ООО «Газпром межрегионгаз Волгоград» напоминает о необходимости строгого соблюдения правил эксплуатации газового оборудования!...</dd>
</dl>
<dl class="news">
<dt>08 декабря 2020</dt>
<dd class="img"><a href="https://34regiongaz.ru/rukovodstvo/news/1820"><img border="0" height="70" src="https://34regiongaz.ru/contents/modules/_getimgpreview.php?f=foto.jpg&amp;w=70&amp;h=70&amp;t=png&amp;q=" width="70"/></a></dd>
<dd class="news-title"><a href="https://34regiongaz.ru/rukovodstvo/news/1820">«Газпром межрегионгаз» подвел итоги конкурентных закупочных процедур по объектам газификации на 72 млрд рублей</a></dd>
<dd class="text">В ООО «Газпром межрегионгаз» подведены итоги закупок по Программе развития газоснабжения и газификации регионов Российской Федерации ПАО...</dd>
</dl>
<dl class="news">
<dt>03 декабря 2020</dt>
<dd class="img"><a href="https://34regiongaz.ru/rukovodstvo/news/1819"><img border="0" height="70" src="https://34regiongaz.ru/contents/modules/_getimgpreview.php?f=ilovlinskiy-rayon-1-_1_.jpg&amp;w=70&amp;h=70&amp;t=png&amp;q=" width="70"/></a></dd>
<dd class="news-title"><a href="https://34regiongaz.ru/rukovodstvo/news/1819">В Волгоградской области идут подготовительные работы для газификации отдаленных сел</a></dd>
<dd class="text">Комплекс проектно-изыскательских работ для дальнейшего строительства газовых сетей стартовал в Иловлинском районе. Мероприятия входят в программу...</dd>
</dl>
<div class="newsp">
<p><a href="https://34regiongaz.ru/rukovodstvo/news/">Архив новостей</a></p>
<p></p><form action="https://34regiongaz.ru/subscribe/" method="post" name="msubscribe"><input name="email" onfocus="this.value='';" size="40" type="text" value="e-mail"/><br/><a href="#" onclick="document.forms['msubscribe'].submit();">подписаться на новости</a></form>
</div>
<h2>Пресс-дайджест</h2>
<dl class="press">
<dt>11 декабря 2020</dt>
<dd><a href="https://34regiongaz.ru/reviews/1446">Внимание! Стартовала онлайн-викторина «История Победы»!</a></dd>
<dd><em>Предлагаем  принять  участие в онлайн-викторине «История Победы»!  




 
Подходит к концу 2020-й год, посвященный 75-летию Победы...</em></dd>
</dl>
<div class="newsp">
<p><a href="https://34regiongaz.ru/reviews/">Архив обзоров</a></p>
</div>
<h2>Видеохроника</h2>
<div class="video">
<div>
<p><video controls="" src="/attachments/media/IMG_9710.mp4" width="100%"></video></p>
<p><video controls="" src="/attachments/media/VID-20200901-WA0002_Trim.mp4" width="100%"></video></p>
<p><video controls="" src="/attachments/media/VID-20200901-WA0003_Trim.mp4" width="100%"></video></p>
<p><video controls="" src="/attachments/media/VID-20200901-WA0004_Trim.mp4" width="100%"></video></p>
<p><video controls="" src="/attachments/media/VID-20200901-WA0005_Trim.mp4" width="100%"></video></p>
<p><video controls="" src="/attachments/media/VID-20200901-WA0006_Trim.mp4" width="100%"></video></p>
<p><video controls="" src="/attachments/media/VID-20200901-WA0007_Trim.mp4" width="100%"></video></p>
<p><video controls="" src="/attachments/media/VID-20200901-WA0010_Trim.mp4" width="100%"></video></p>
<p><video controls="" src="/attachments/media/VID-20200901-WA0011_Trim.mp4" width="100%"></video></p>
<p><video controls="" src="/attachments/media/VID-20200901-WA0012_Trim.mp4" width="100%"></video></p>
<p>Безопасное использование газового оборудования в быту</p> <p><a href="https://34regiongaz.ru/press-tsentr/video">Архив видео</a></p>
</div>
</div>
</div>
<div class="rightside">
<div class="direktor-page">
<div class="tlt"><h3><i>Колонка <br/>Олега<br/> Гаранина</i></h3></div>
<div class="dir-info">
<p><i><a href="https://34regiongaz.ru/column/130">Поздравление с Новым годом от генерального директора ООО «Газпром межрегионгаз Волгоград» Олега Гаранина</a></i></p>
</div>
</div>
<div class="banner">
<a href="https://34regiongaz.ru/gaz/"><img border="0" height="65" src="https://34regiongaz.ru/attachments/gazbanner.jpg" width="290"/></a><br/><br/>
<a href="https://34regiongaz.ru/informatsiya-dlya-potrebiteley/fizicheskim-litsam/oplata-gaza/oplata-cherez-platezhnyie-servisyi/"><img border="0" height="65" src="https://34regiongaz.ru/attachments/pay.jpg" width="290"/></a><br/><br/>
<a href="https://34regiongaz.ru/7-prichin/"><img border="0" src="https://34regiongaz.ru/attachments/7prichin.jpg" width="290"/></a><br/><br/>
<a href="https://34regiongaz.ru/informatsiya-dlya-potrebiteley/fizicheskim-litsam/poryadok-rascheta-platyi-za-gaz/peredat-pokazaniya-priborov-ucheta/"><img border="0" height="115" src="https://34regiongaz.ru/images/1sps.png" width="290"/></a><br/><br/>
<a href="https://34regiongaz.ru/orders-003/"><img border="0" height="62" src="https://34regiongaz.ru/images/orders-003.png" width="291"/></a>
</div>
<div class="cabinet">
<h4>Личный кабинет абонента *</h4>
<form action="https://34regiongaz.ru/cabinet/enter/" id="mfslogin" method="post" name="mfslogin">
<input class="login" name="t1" onfocus="if(this.value=='логин'){this.value='';}" type="text" value="логин"/>
<div id="obl_t2" name="obl_t2"><input class="pass" id="t2" name="t2" onfocus="newinput();" type="text" value="пароль"/></div>
<a class="enter" href="#" onclick="$('#mfslogin').submit();"> </a>
</form>
<script language="JavaScript" type="text/javascript">
    function savelinkclick(keyname) {
      $("#"+keyname).load('https://34regiongaz.ru/contents/link_count_ink.php', {name: keyname});
    }
     function newinput() {
      $("#obl_t2").html("");
      try {
       var input = document.createElement('<input type="password">');// Для IE
      }
      catch (e) {
       var input = document.createElement('input');
       input.setAttribute('type', 'password');
      }
      input.setAttribute('id', 't2'); input.setAttribute('name', 't2');
      $("#obl_t2").append(input);
      $("#t2").focus(function(){$(this).attr("value", "");});
      $("#t2").keydown(function(event){if(event.keyCode==13) $('#mfslogin').submit();});
      $("#t2").focus();
     }
    </script>
<a class="forgot" href="https://34regiongaz.ru/cabinet/enter/sendpassword">забыли пароль?</a> <a class="reg" href="https://34regiongaz.ru/choice/">регистрация</a> <a class="about" href="https://34regiongaz.ru/cabinet/about-cab/">о проекте</a>
</div>
<p><a href="https://34regiongaz.ru/cabinet/enter/"><img border="0" src="https://34regiongaz.ru/attachments/2sps.png" width="290"/></a></p>
<p><a href="https://34regiongaz.ru/debtors-gas"><img border="0" src="https://34regiongaz.ru/attachments/debtors.jpg" width="290"/></a></p>
<p><a href="https://34regiongaz.ru/informatsiya-dlya-potrebiteley/yuridicheskim-litsam-i-ip/elektronnyiy-dokumentooborot/"><img border="0" src="https://34regiongaz.ru/attachments/document.png" width="290"/></a></p>
<div class="gazregion">
<div class="stt"><h4>Памятка абонента</h4></div>
<ul>
<li><a href="https://34regiongaz.ru/informatsiya-dlya-potrebiteley/fizicheskim-litsam/normativno-pravovaya-baza/">Нормативно-правовая база</a></li>
<li><a href="https://34regiongaz.ru/informatsiya-dlya-potrebiteley/fizicheskim-litsam/zaklyuchenie-dogovora-postavki-gaza/">Заключение договора поставки газа</a></li>
<li><a href="https://34regiongaz.ru/informatsiya-dlya-potrebiteley/fizicheskim-litsam/poryadok-rascheta-platyi-za-gaz/">Порядок расчета платы за газ</a></li>
<li><a href="https://34regiongaz.ru/informatsiya-dlya-potrebiteley/fizicheskim-litsam/oplata-gaza/">Оплата поставки газа</a></li>
<li><a href="https://34regiongaz.ru/informatsiya-dlya-potrebiteley/fizicheskim-litsam/lgotyi-i-sotsialnaya-podderzhka/">Льготы и социальная поддержка</a></li>
<li><a href="https://34regiongaz.ru/informatsiya-dlya-potrebiteley/fizicheskim-litsam/rabota-s-dolzhnikami/">Работа с должниками</a></li>
<li><a href="https://34regiongaz.ru/informatsiya-dlya-potrebiteley/fizicheskim-litsam/priboryi-ucheta-gaza/">Приборы учета газа</a></li>
<li><a href="https://34regiongaz.ru/informatsiya-dlya-potrebiteley/fizicheskim-litsam/bezopasnost-ispolzovaniya-gaza-v-byitu/">Безопасность использования газа в быту</a></li>
<li><a href="https://34regiongaz.ru/informatsiya-dlya-potrebiteley/fizicheskim-litsam/posledstviya-nedopuska-predstavitelya-postavschika/">Последствия недопуска представителя поставщика газа</a></li>
</ul>
</div>
<p><a href="https://34regiongaz.ru/obratnaya-svyaz/sendfaq/"><img border="0" src="https://34regiongaz.ru/attachments/faq2.png" width="290"/></a></p>
<p><a href="https://34regiongaz.ru/obratnaya-svyaz/spravochnoe-byuro/"><img border="0" src="https://34regiongaz.ru/attachments/spr3.jpg" width="290"/></a></p>
<!-- <p><a href="http://gas-forum.ru"><img border="0" src="https://34regiongaz.ru/attachments/logo_gf_2018.png" width="290"></a></p> -->
<!-- <p><a href="https://rusenergyweek.com"><img border="0" src="https://34regiongaz.ru/attachments/rgn-small.jpg" width="290"></a></p> -->
<!-- <p><a href="http://www.gazprom.ru/about/ms/quality-management-system/year-quality/"><img border="0" src="https://34regiongaz.ru/attachments/gazprom2018.png" width="290"></a></p> -->
<!-- <p><a href="http://www.gazprom-gmt.ru/special_projects/gazovy_kamaz"><img border="0" src="https://34regiongaz.ru/attachments/6701.jpg" width="290"></a></p> -->
<p><a href="http://www.myvistory.ru/"><img border="0" height="163" src="https://34regiongaz.ru/attachments/myvistory.jpg" width="290"/></a></p>
<!--p><a href="http://www.gazprom.ru/nature/ems/"><img border="0" src="https://34regiongaz.ru/attachments/god_ekologii.jpg" width="290"></a></p>
<p><a href="http://www.gazpromspartakiada.ru"><img border="0" src="https://34regiongaz.ru/attachments/spartakiada_290x136.png" width="290"></a></p-->
<p><a href="http://matchtv.ru"><img border="0" src="https://34regiongaz.ru/attachments/match-tv.png" width="290"/></a></p>
<!-- <p><a href="http://xn--b1agaa6a0afi1cwe.xn--p1ai/"><img border="0" src="https://34regiongaz.ru/attachments/banner-0001.jpg" width="290"></a></p> -->
<!-- <p><a href="https://медиатэк.рф"><img border="0" src="https://34regiongaz.ru/attachments/Mediatek_banner2020.png" width="290"></a></p> -->
<!-- <p><a href="https://экодиктант.рус"><img border="0" src="https://34regiongaz.ru/attachments/d_65290e023c.png" width="290"></a></p> -->
<p><a href="https://mrg.gazprom.ru/_ah/img/7EHo6aBZXzcoihJCcmaaXw"><img border="0" src="https://34regiongaz.ru/attachments/banner_ng_mrg.jpg" width="290"/></a></p>
<!-- <p><a href="https://volgograd-college.gazprom.ru/abiturientu/"><img border="0" src="https://34regiongaz.ru/attachments/banner-0002.jpg" width="290"></a></p> -->
<!-- <p><a href="https://конституция2020.рф/"><img border="0" src="https://34regiongaz.ru/attachments/og-banner.png" width="290"></a></p> -->
<!-- <p><a href="https://www.вместеярче.рф"><img border="0" src="https://34regiongaz.ru/attachments/vy.jpg" width="290"></a></p> -->
<!-- <p><a target="_blank" href="http://cikrf.ru/analog/ediny-den-golosovaniya-2020/kategorii-viborov/" onclick="javascript:savelinkclick('ediny-den-golosovaniya-2020');"><img border="0" src="https://34regiongaz.ru/attachments/EDG_2020_web_600х600.jpg" width="290"></a><div id="ediny-den-golosovaniya-2020"></div></p> -->
<div class="banner">
</div>
<div class="plinks">
<h3>Полезные ссылки</h3>
<p>ОАО «Газпром»<br/>
<a href="http://www.gazprom.ru">www.gazprom.ru</a></p>
<p>ООО «Газпром межрегионгаз»<br/>
<a href=" http://www.mrg.gazprom.ru">www.mrg.gazprom.ru</a></p>
</div>
<div class="plinks">
<p style="text-align: center;"><a href="https://t.me/news_mrg" target="_blank"><img border="0" src="https://34regiongaz.ru/attachments/image001n.png"/></a><a href="https://www.instagram.com/gazprom_mrg_life/" target="_blank"><img border="0" src="https://34regiongaz.ru/attachments/image002n.png"/></a></p>
</div>
<div class="count">
</div>
</div>
</div>
<div class="footer">
<div>
<div class="lf">
<p><b>© ООО «Газпром межрегионгаз Волгоград»</b><br/><b>Почтовый адрес</b>: 400001, г. Волгоград, ул. Ковровская, 13</p>
<p><b>Телефон</b>: (8442) 96-91-30<br/><b>Факс</b>: (8442) 96-06-96</p>
<p><a href="/o-kompanii/map/">Абонентские отделы</a></p>
<p>Режим работы: пн-пт, с 8:00 до 17:00, перерыв на обед с 12:00 до 13:00. Выходной - суббота, воскресенье.</p>
</div>
<div class="rg">
<p>Дизайн и разработка сайта — <a href="http://www.mfrog.ru" target="_blank">Mechanicalfrog</a></p>
</div>
</div>
</div>
</div>
</body>
</html>