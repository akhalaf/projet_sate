<!DOCTYPE html>
<html channel="125" data-client="0" delivery="" district="" lang="ru-RU" location="22" location_name="Архангельск" location_url="arhangelsk">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0 user-scalable=0, viewport-fit=cover" name="viewport"/>
<meta content="telephone=YES" name="format-detection"/>
<meta content="#bebebe" name="theme-color"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="yes" name="mobile-web-app-capable"/>
<title>Заказ еды с бесплатной доставкой: пицца, роллы, вок</title>
<link href="//cdpiz1.pizzasoft.ru/pizzafab/favicons/favicon.ico" rel="shortcut icon"/>
<link href="//cdpiz1.pizzasoft.ru/pizzafab/favicons/favicon-16x16.png" rel="icon" sizes="16x16"/>
<link href="//cdpiz1.pizzasoft.ru/pizzafab/favicons/favicon-32x32.png" rel="icon" sizes="32x32"/>
<link href="//cdpiz1.pizzasoft.ru/pizzafab/favicons/apple-touch-icon.png" rel="apple-touch-icon"/>
<link href="//cdpiz1.pizzasoft.ru/pizzafab/favicons/safari-pinned-tab.svg" rel="mask-icon"/>
<link href="/manifest.json" rel="manifest"/>
<meta content="_csrf" name="csrf-param"/>
<meta content="DhDERYF0NjZdT2Wl_5kxjsgz-oovE7SVY02nJyP2rYZpWKNotQ5nWBh5EMStw1bIqmONx0x34coPHZNeVc706A==" name="csrf-token"/>
<meta content="6e0ef9f4a67a8979" name="yandex-verification"/>
<meta content="bc33b1566a3de37040b688a39dbedde3" name="wmail-verification"/>
<meta content="fifeMKDrP3I1H4ivOIyGH8eVZvNOrEAFjptLiHxKhr0" name="google-site-verification"/>
<script>
        var AppVars = {
            districts: '[{"id":43,"name":"\u0428\u0430\u0431\u0430\u043b\u0438\u043d\u0430","address":"\u0420\u0435\u0441\u0442\u043e\u0440\u0430\u043d, \u0443\u043b\u0438\u0446\u0430 \u0428\u0430\u0431\u0430\u043b\u0438\u043d\u0430, \u0434. 20 \u043a1"}]',
            locations: '[{"id":22,"location_name":"\u0410\u0440\u0445\u0430\u043d\u0433\u0435\u043b\u044c\u0441\u043a","location_url":"arhangelsk","page_url":"\/arhangelsk\/"},{"id":13,"location_name":"\u0412\u0435\u043b\u0438\u043a\u0438\u0439 \u041d\u043e\u0432\u0433\u043e\u0440\u043e\u0434","location_url":"velikijnovgorod","page_url":"\/velikijnovgorod\/"},{"id":7,"location_name":"\u0412\u043b\u0430\u0434\u0438\u043c\u0438\u0440","location_url":"vladimir","page_url":"\/vladimir\/"},{"id":1,"location_name":"\u0412\u043e\u043b\u043e\u0433\u0434\u0430","location_url":"vologda","page_url":"\/vologda\/"},{"id":35,"location_name":"\u0415\u043a\u0430\u0442\u0435\u0440\u0438\u043d\u0431\u0443\u0440\u0433","location_url":"ekaterinburg","page_url":"\/ekaterinburg\/"},{"id":39,"location_name":"\u0417\u0430\u0441\u0435\u0447\u043d\u043e\u0435 (\u041f\u0435\u043d\u0437\u0435\u043d\u0441\u043a\u0430\u044f \u043e\u0431\u043b.)","location_url":"zasechnoe","page_url":"\/zasechnoe\/"},{"id":5,"location_name":"\u0418\u0432\u0430\u043d\u043e\u0432\u043e","location_url":"ivanovo","page_url":"\/ivanovo\/"},{"id":30,"location_name":"\u041a\u0430\u043b\u0438\u043d\u0438\u043d\u0433\u0440\u0430\u0434","location_url":"kaliningrad","page_url":"\/kaliningrad\/"},{"id":25,"location_name":"\u041a\u043e\u043b\u043f\u0438\u043d\u043e","location_url":"kolpino","page_url":"\/kolpino\/"},{"id":6,"location_name":"\u041a\u043e\u0441\u0442\u0440\u043e\u043c\u0430","location_url":"kostroma","page_url":"\/kostroma\/"},{"id":40,"location_name":"\u041d\u0438\u0436\u043d\u0438\u0439 \u041d\u043e\u0432\u0433\u043e\u0440\u043e\u0434","location_url":"nizhnijnovgorod","page_url":"\/nizhnijnovgorod\/"},{"id":20,"location_name":"\u041d\u0438\u0436\u043d\u0438\u0439 \u0422\u0430\u0433\u0438\u043b","location_url":"nizhnijtagil","page_url":"\/nizhnijtagil\/"},{"id":32,"location_name":"\u041d\u043e\u0432\u043e\u043a\u0443\u0437\u043d\u0435\u0446\u043a","location_url":"novokuzneck","page_url":"\/novokuzneck\/"},{"id":23,"location_name":"\u041f\u0435\u043d\u0437\u0430","location_url":"penza","page_url":"\/penza\/"},{"id":4,"location_name":"\u0420\u044b\u0431\u0438\u043d\u0441\u043a","location_url":"rybinsk","page_url":"\/rybinsk\/"},{"id":34,"location_name":"\u0420\u044f\u0437\u0430\u043d\u044c","location_url":"ryazan","page_url":"\/ryazan\/"},{"id":36,"location_name":"\u0421\u0430\u043c\u0430\u0440\u0430","location_url":"samara","page_url":"\/samara\/"},{"id":42,"location_name":"\u0421\u0430\u043d\u043a\u0442-\u041f\u0435\u0442\u0435\u0440\u0431\u0443\u0440\u0433","location_url":"sanktpeterburg","page_url":"\/sanktpeterburg\/"},{"id":37,"location_name":"\u0421\u0435\u0432\u0435\u0440\u043e\u0434\u0432\u0438\u043d\u0441\u043a","location_url":"severodvinsk","page_url":"\/severodvinsk\/"},{"id":41,"location_name":"\u0421\u043c\u043e\u043b\u0435\u043d\u0441\u043a","location_url":"smolensk","page_url":"\/smolensk\/"},{"id":38,"location_name":"\u0421\u043e\u0447\u0438","location_url":"sochi","page_url":"\/sochi\/"},{"id":27,"location_name":"\u0421\u0443\u0440\u0433\u0443\u0442","location_url":"surgut","page_url":"\/surgut\/"},{"id":18,"location_name":"\u0422\u0432\u0435\u0440\u044c","location_url":"tver","page_url":"\/tver\/"},{"id":19,"location_name":"\u0422\u043e\u043b\u044c\u044f\u0442\u0442\u0438","location_url":"tolyatti","page_url":"\/tolyatti\/"},{"id":31,"location_name":"\u0422\u0443\u043b\u0430","location_url":"tula","page_url":"\/tula\/"},{"id":33,"location_name":"\u0422\u044e\u043c\u0435\u043d\u044c","location_url":"tyumen","page_url":"\/tyumen\/"},{"id":28,"location_name":"\u0423\u0441\u0438\u043d\u0441\u043a","location_url":"usinsk","page_url":"\/usinsk\/"},{"id":8,"location_name":"\u0427\u0435\u0431\u043e\u043a\u0441\u0430\u0440\u044b","location_url":"cheboksary","page_url":"\/cheboksary\/"},{"id":2,"location_name":"\u0427\u0435\u0440\u0435\u043f\u043e\u0432\u0435\u0446","location_url":"cherepovets","page_url":"\/cherepovets\/"},{"id":3,"location_name":"\u042f\u0440\u043e\u0441\u043b\u0430\u0432\u043b\u044c","location_url":"yaroslavl","page_url":"\/yaroslavl\/"}]',
            isMainPage: true,
            isItemPage: false,
            isCategoryPage: false,
            isFirst: true,
            itemPageData: null,
            itemTitleSuff: " - \u041f\u0438\u0446\u0446\u0430\u0424\u0430\u0431\u0440\u0438\u043a\u0430 ",
            enable_chat: true,
            indexPage: {
                title: 'Заказ еды с бесплатной доставкой: пицца, роллы, вок',
                url: '/',
            },
            langs: {"subscription_tab_title_desktop":"\u041f\u0438\u0446\u0446\u0430\u0444\u0430\u0431","subscription_tab_title_tablet":"\u041f\u0438\u0446\u0446\u0430\u0444\u0430\u0431","subscription_tab_title_mobile":"\u041f\u0438\u0446\u0446\u0430\u0444\u0430\u0431","subscription_delivery_title":"\u041f\u0438\u0446\u0446\u0430\u0444\u0430\u0431","subscription_delivery_preview":"\u041f\u0438\u0446\u0446\u0430\u0444\u0430\u0431","subscription_delivery_text":"\u041f\u0438\u0446\u0446\u0430\u0444\u0430\u0431","subscription_bonuses_title":"\u041f\u0438\u0446\u0446\u0430\u0444\u0430\u0431","subscription_bonuses_preview":"\u041f\u0438\u0446\u0446\u0430\u0444\u0430\u0431","subscription_bonuses_text":"\u041f\u0438\u0446\u0446\u0430\u0444\u0430\u0431","subscription_partners_title":"\u041f\u0438\u0446\u0446\u0430\u0444\u0430\u0431","subscription_partners_preview":"\u041f\u0438\u0446\u0446\u0430\u0444\u0430\u0431","subscription_partners_text":"\u041f\u0438\u0446\u0446\u0430\u0444\u0430\u0431"},
            subscription_tab_enable: false        }
    </script>
<script src="/config.js?v=2" type="text/javascript"></script>
<script src="/manup.min.js" type="text/javascript"></script>
<meta content="пицца, пицца фабрика, 500600, доставка пиццы, доставка роллов, доставка еды, заказ пиццы" name="keywords"/>
<meta content="ПиццаФабрика - быстрая доставка от 30 минут. Меню на любой вкус: пицца, роллы, воки, напитки. Заказывайте недорого горячую вкусную еду!" name="description"/>
<meta content="64.5472507,40.5601553" name="ICBM"/>
<meta content="https://cdpiz1.pizzasoft.ru/pizzafab/design1/opengraph_image.jpg?t=1583213250" property="og:image"/>
<meta content="summary_large_image" property="twitter:card"/>
<meta content="Заказ еды с бесплатной доставкой: пицца, роллы, вок" property="og:title"/>
<meta content="noyaca" name="robots"/>
<link href="/bundle/vendor/common/style.css?v=1610521103" rel="stylesheet"/>
<link href="/bundle/app/style.css?v=1610521103" rel="stylesheet"/>
<script charset="UTF-8" defer="" src="/bundle/vendor/main.js?v=1610521103"></script>
<script charset="UTF-8" defer="" src="/bundle/vendor/common/main.js?v=1610521103"></script>
<script charset="UTF-8" defer="" src="/bundle/vendor/react/main.js?v=1610521103"></script>
<script charset="UTF-8" defer="" src="/bundle/vendor/jquery/main.js?v=1610521103"></script>
<script charset="UTF-8" defer="" src="/bundle/app/main.js?v=1610521103"></script></head>
<body>
<div class="wrapper-outer">
<div class="sidemenu-animation js-sidemenu-modal">
<div class="sidemenu">
<div class="sidemenu-inner">
<div class="sidemenu__closebutton js-close"></div>
<a aria-label="ПиццаФабрика.рф" class="sidemenu__logo anchor-logo-hidden" href="/">ПиццаФабрика.рф</a>
<nav class="sidemenu__itemlist js-sidemenu"><div class="sidemenu__item js-nav-category"><div class="sidemenu__title">Меню</div><div class="sidemenu__submenu"><a class="sidemenu__submenu-item" href="/pizza/" rel="nofollow">Пицца</a>
<a class="sidemenu__submenu-item" href="/combo/" rel="nofollow">Комбо</a>
<a class="sidemenu__submenu-item" href="/rolls/" rel="nofollow">Роллы</a>
<a class="sidemenu__submenu-item" href="/snacks/" rel="nofollow">Закуски</a>
<a class="sidemenu__submenu-item" href="/wok/" rel="nofollow">Вок</a>
<a class="sidemenu__submenu-item" href="/pasta/" rel="nofollow">Пасты</a>
<a class="sidemenu__submenu-item" href="/salad/" rel="nofollow">Салаты</a>
<a class="sidemenu__submenu-item" href="/soups/" rel="nofollow">Супы</a>
<a class="sidemenu__submenu-item" href="/streetfood/" rel="nofollow">Стритфуд</a>
<a class="sidemenu__submenu-item" href="/not-pizza/" rel="nofollow">Не Пицца :)</a>
<a class="sidemenu__submenu-item" href="/dessert/" rel="nofollow">Десерты</a>
<a class="sidemenu__submenu-item" href="/drinks/" rel="nofollow">Напитки</a>
<a class="sidemenu__submenu-item" href="/sauce/" rel="nofollow">Соусы</a>
<a class="sidemenu__submenu-item" href="/additionally/" rel="nofollow">Дополнительно</a></div></div>
<div class="sidemenu__item sidemenu__item_link"><a class="sidemenu__title sidemenu__title_link" href="/promo/actions/" rel="nofollow">Акции</a></div>
<div class="sidemenu__item js-nav-category"><div class="sidemenu__title">Ещё</div><div class="sidemenu__submenu"><a class="sidemenu__submenu-item" href="/company/zona-dostavki.html" rel="nofollow">Зона доставки</a>
<a class="sidemenu__submenu-item" href="/menyu-i-bron-arh.html" rel="nofollow">Меню и бронь</a>
<a class="sidemenu__submenu-item" href="/company/news/" rel="nofollow">Новости</a>
<a class="sidemenu__submenu-item" href="/company/job/" rel="nofollow">Вакансии</a>
<a class="sidemenu__submenu-item" href="/company/schedule.html" rel="nofollow">График работы</a>
<a class="sidemenu__submenu-item" href="/company/about.html" rel="nofollow">О компании</a>
<a class="sidemenu__submenu-item" href="/onlajn-oplata-bankovskoj-kartoj.html" rel="nofollow">Онлайн оплата</a>
<a class="sidemenu__submenu-item" href="/company/news/ishem-postavshikov.html" rel="nofollow">Ищем поставщиков</a>
<a class="sidemenu__submenu-item" href="https://pfinvest.ru/" rel="noopener noreferrer" target="_blank">Инвестиции</a></div></div>
<div class="sidemenu__item sidemenu__item_link"><a class="sidemenu__title sidemenu__title_link" href="https://xn--80aaudyq1a9a.xn--80aaad6ado8an1bua.xn--p1ai/" rel="noopener noreferrer" target="_blank">Франшиза</a></div></nav> <div class="sidemenu__bottom">
<div class="sidemenu__cabinet">
<a aria-label="Личный кабинет" class="sidemenu__cabinet-link js-cabinet" href="/client.html" rel="nofollow">Личный кабинет</a>
</div>
<div class="sidemenu__current-city">
<a class="sidemenu__current-city-link js-city-select">Архангельск</a>
</div>
</div>
<div class="sidemenu__social">
<a aria-label="Мы в " class="icon-vk" href="https://vk.com/pizzafab_arh" rel="nofollow noreferrer" target="_blank"></a><a aria-label="Мы в " class="icon-yb" href="https://www.youtube.com/c/PizzaFactoryOfficial" rel="nofollow noreferrer" target="_blank"></a><a aria-label="Мы в " class="icon-in" href="https://instagram.com/pizzafab_arh/" rel="nofollow noreferrer" target="_blank"></a><a aria-label="Мы в " class="icon-fb" href="https://www.facebook.com/pizzafabrica/" rel="nofollow noreferrer" target="_blank"></a> </div>
</div>
</div>
</div>
<div class="wrapper-inner wrapper-inner_background_gray">
<div class="container header-top">
<div class="header-top__current-city js-city-list">
                Выберите город            </div>
<div class="header-top__cabinet js-cabinet">
<a aria-label="Личный кабинет" class="header-top__cabinet-button header-top__personal-page-icon" href="/client.html" rel="nofollow">
                Личный кабинет
            </a>
</div>
</div>
</div>
<div class="wrapper-inner wrapper-inner_header-bottom">
<div class="container">
<a aria-label="ПиццаФабрика.рф" class="header-bottom__logo js-site-logo anchor-logo-hidden" href="/">
            ПиццаФабрика.рф
        </a>
</div>
<div class="container header-bottom">
<nav class="header-bottom__topmenu js-topnav"><div class="header-bottom__topmenu-item js-nav-category"><div class="header-bottom__topmenu-item-title">Меню</div><div class="header-bottom__topmenu-submenu header-bottom__topmenu-submenu_double"><a class="header-bottom__topmenu-submenu-item" href="/pizza/" rel="nofollow">Пицца</a>
<a class="header-bottom__topmenu-submenu-item" href="/combo/" rel="nofollow">Комбо</a>
<a class="header-bottom__topmenu-submenu-item" href="/rolls/" rel="nofollow">Роллы</a>
<a class="header-bottom__topmenu-submenu-item" href="/snacks/" rel="nofollow">Закуски</a>
<a class="header-bottom__topmenu-submenu-item" href="/wok/" rel="nofollow">Вок</a>
<a class="header-bottom__topmenu-submenu-item" href="/pasta/" rel="nofollow">Пасты</a>
<a class="header-bottom__topmenu-submenu-item" href="/salad/" rel="nofollow">Салаты</a>
<a class="header-bottom__topmenu-submenu-item" href="/soups/" rel="nofollow">Супы</a>
<a class="header-bottom__topmenu-submenu-item" href="/streetfood/" rel="nofollow">Стритфуд</a>
<a class="header-bottom__topmenu-submenu-item" href="/not-pizza/" rel="nofollow">Не Пицца :)</a>
<a class="header-bottom__topmenu-submenu-item" href="/dessert/" rel="nofollow">Десерты</a>
<a class="header-bottom__topmenu-submenu-item" href="/drinks/" rel="nofollow">Напитки</a>
<a class="header-bottom__topmenu-submenu-item" href="/sauce/" rel="nofollow">Соусы</a>
<a class="header-bottom__topmenu-submenu-item" href="/additionally/" rel="nofollow">Дополнительно</a></div></div>
<a class="header-bottom__topmenu-item header-bottom__topmenu-item_link" href="/promo/actions/" rel="nofollow">Акции</a>
<div class="header-bottom__topmenu-item js-nav-category"><div class="header-bottom__topmenu-item-title">Ещё</div><div class="header-bottom__topmenu-submenu header-bottom__topmenu-submenu_double"><a class="header-bottom__topmenu-submenu-item" href="/company/zona-dostavki.html" rel="nofollow">Зона доставки</a>
<a class="header-bottom__topmenu-submenu-item" href="/menyu-i-bron-arh.html" rel="nofollow">Меню и бронь</a>
<a class="header-bottom__topmenu-submenu-item" href="/company/news/" rel="nofollow">Новости</a>
<a class="header-bottom__topmenu-submenu-item" href="/company/job/" rel="nofollow">Вакансии</a>
<a class="header-bottom__topmenu-submenu-item" href="/company/schedule.html" rel="nofollow">График работы</a>
<a class="header-bottom__topmenu-submenu-item" href="/company/about.html" rel="nofollow">О компании</a>
<a class="header-bottom__topmenu-submenu-item" href="/onlajn-oplata-bankovskoj-kartoj.html" rel="nofollow">Онлайн оплата</a>
<a class="header-bottom__topmenu-submenu-item" href="/company/news/ishem-postavshikov.html" rel="nofollow">Ищем поставщиков</a>
<a class="header-bottom__topmenu-submenu-item" href="https://pfinvest.ru/" rel="noopener noreferrer" target="_blank">Инвестиции</a></div></div>
<a class="header-bottom__topmenu-item header-bottom__topmenu-item_link" href="https://xn--80aaudyq1a9a.xn--80aaad6ado8an1bua.xn--p1ai/" rel="noopener noreferrer" target="_blank">Франшиза</a></nav>
<span class="header-bottom__mobile-nav-back header-bottom__mobile-nav-back_hidden js-nav-back"></span>
<span class="header-bottom__mobile-nav-gamburger js-sidemenu-toggle"></span>
<div class="header-bottom__mobile-nav-logo"></div>
<div class="header-bottom__basket header-bottom__basket-single">
<a aria-label="basketInfoLink" class="header-bottom__basket-info" href="/order.html" rel="nofollow">
<span class="header-bottom__basket-icon">
<span class="header-bottom__basket-counter" style="display: none">0</span>
</span>
<div class="header-bottom__basket-info-text">Корзина</div>
</a>
</div>
</div>
</div>
<div class="wrapper-inner wrapper-inner--superhack">
<div class="banner" data-count="3" id="banner">
<div class="banner__slick_carousel" id="slick_carousel">
<div class="banner__content preloader banner__content--active-point" data-link="/">
<picture>
<source data-srcset="//cdpiz1.pizzasoft.ru/pizzafab/banners/1/c09482555617553cf6e8750483f79ad3.jpg" media="tablet">
<img alt="Бесконтактная доставка" class="banner__img lazyload" data-src="//cdpiz1.pizzasoft.ru/cr/670x268/pizzafab/banners/1/c09482555617553cf6e8750483f79ad3.jpg"/>
</source></picture>
</div>
<div class="banner__content preloader">
<picture>
<source data-srcset="//cdpiz1.pizzasoft.ru/pizzafab/banners/1/28d71ed4350fca594525f338c2eefd34.jpg" media="tablet">
<img alt="Выбор опций " class="banner__img lazyload" data-src="//cdpiz1.pizzasoft.ru/cr/670x268/pizzafab/banners/1/28d71ed4350fca594525f338c2eefd34.jpg"/>
</source></picture>
</div>
<div class="banner__content preloader banner__content--active-point" data-link="https://pizzafabrika.ru/promo/actions/priglasite-druga-pepperoni.html?pf=1">
<picture>
<source data-srcset="//cdpiz1.pizzasoft.ru/pizzafab/banners/1/3f611e93513e415b555e53ca1f15e49e.jpg" media="tablet">
<img alt="реферальная система с пепперони" class="banner__img lazyload" data-src="//cdpiz1.pizzasoft.ru/cr/670x268/pizzafab/banners/1/3f611e93513e415b555e53ca1f15e49e.jpg"/>
</source></picture>
</div>
</div>
</div>
<div class="videoframe" id="videoframe">
<div class="videoframe__modal" id="vid_modal">
<div class="videoframe__exit" id="vid_exit"></div>
<div class="videoframe__cont" id="vid_cont"></div>
</div>
</div>
</div>
<h1 class="visually-hidden">ПиццаФабрика</h1>
<div class="container copyright">
    ПиццаФабрика © 2011-2021 ООО «Все и сразу». </div>
<div class="visually-hidden" itemscope="" itemtype="http://schema.org/Organization">
<span content="ПиццаФабрика" itemprop="name">
<span content="ПиццаФабрика - быстрая доставка от 30 минут. Меню на любой вкус: пицца, роллы, воки, напитки. Заказывайте недорого горячую вкусную еду!" itemprop="description">
<div itemprop="address" itemscope="" itemtype="https://schema.org/PostalAddress">
<span itemprop="addressCountry">Россия</span>
</div>
<span itemprop="telephone">8-800-5-500-600</span>
</span></span></div> <!--[if lt IE 10]>
<script type="text/javascript">
    document.getElementsByTagName('html')[0].style.overflow = 'hidden';
    document.getElementsByTagName('body')[0].style.overflow = 'hidden';
</script>

<div class="ie-bg"></div>
<div class="modal active ie">
    <div class="modal-header">
        <h3>Ваш браузер устарел</h3>
    </div>
    <div class="modal-body">
        <div class="msg">К сожалению, ваш браузер устарел и не может нормально отображать сайт. Пожалуйста, скачайте любой из следующих браузеров:</div>
        <div class="list">
            <a href="http://www.google.com/chrome" title="Google Chrome" class="chrome"></a>
            <a href="http://www.mozilla.org/firefox" title="Mozilla Firefox" class="firefox"></a>
            <a href="http://www.opera.com/download" title="Opera" class="opera"></a>
            <a href="http://ie.microsoft.com" title="Microsoft Internet Explorer" class="ie"></a>
        </div>
    </div>
</div>
<![endif]--> </div>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBK9k_8LALztYpnJNijUshvFtMzneJCia0"></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter36491655 = new Ya.Metrika2({
                    id:36491655,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img alt="metrika" src="https://mc.yandex.ru/watch/36491655" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- vk.com pixel -->
<script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?159",t.onload=function(){VK.Retargeting.Init("VK-RTRG-300876-1dlQe"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img alt="metrika" src="https://vk.com/rtrg?p=VK-RTRG-300876-1dlQe" style="position:fixed; left:-999px;"/></noscript>
<!-- /vk.com pixel -->
<!-- Global site tag (gtag.js) - Google Ads: 605900952 -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=AW-605900952"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-605900952');
</script>
<script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?168",t.onload=function(){VK.Retargeting.Init("VK-RTRG-532669-c6CQ"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img alt="metrika" src="https://vk.com/rtrg?p=VK-RTRG-532669-c6CQ" style="position:fixed; left:-999px;"/></noscript>
<!-- VK946373 --><script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-93557529-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93557529-1');
</script>
<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, 'script', 'cloud.roistat.com', 'a661d14ed323c31677c01b403664dc8d');
</script></body>
</html>
