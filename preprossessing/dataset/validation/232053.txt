<!DOCTYPE html>
<html lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>BitConnect pool - Home</title>
<!--[if lt IE 9]>
  <link rel="stylesheet" href="site_assets/bootstrap/css/ie.css" type="text/css" media="screen" />
   <![endif]-->
<!--[if IE]><script type="text/javascript" src="site_assets/bootstrap/js/excanvas.js"></script><![endif]-->
<link href="site_assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<link href="site_assets/bootstrap/css/bootstrap-switch.min.css" rel="stylesheet"/>
<link href="site_assets/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<link href="site_assets/bootstrap/css/plugins/morris/morris-0.5.1.css" rel="stylesheet"/>
<link href="site_assets/bootstrap/css/plugins/timeline/timeline.css" rel="stylesheet"/>
<link href="site_assets/bootstrap/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet"/>
<link href="site_assets/bootstrap/css/mpos.css" rel="stylesheet"/>
<link href="site_assets/bootstrap/css/sparklines.css" rel="stylesheet"/>
<script src="site_assets/bootstrap/js/jquery-2.1.3.min.js"></script>
<script src="site_assets/bootstrap/js/jquery.cookie.js"></script>
<script src="site_assets/bootstrap/js/jquery.md5.js"></script>
<script src="site_assets/bootstrap/js/bootstrap.min.js"></script>
<script src="site_assets/bootstrap/js/bootstrap-switch.min.js"></script>
<script src="site_assets/bootstrap/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="site_assets/bootstrap/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="site_assets/bootstrap/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="site_assets/bootstrap/js/plugins/raphael-2.1.2.min.js"></script>
<script src="site_assets/bootstrap/js/plugins/morris/morris-0.5.1.min.js"></script>
<script src="site_assets/bootstrap/js/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="site_assets/bootstrap/js/mpos.js"></script>
</head>
<body>
<div id="wrapper">
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;background:#3c3c3c">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".sidebar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="index_php.html"><img class="img-responsive " src="themes/default/images/bg/bitconnect-coin.png" style="margin-top: -9px;"/></a>
</div>
<ul class="nav navbar-top-links navbar-right">
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">
<i class="fa fa-user fa-fw"></i> Guest <i class="fa fa-caret-down"></i>
</a>
<ul class="dropdown-menu dropdown-user">
<li><a href="/"><i class="fa fa-sign-in fa-fw"></i> Login</a>
</li><li><a href="/"><i class="fa fa-pencil fa-fw"></i> Sign Up</a>
</li>
</ul>
</li>
</ul>
</nav>
<nav class="navbar-default navbar-static-side" role="navigation">
<div class="sidebar-collapse">
<ul class="nav" id="side-menu">
<li>
<a href="index_php.html"><i class="fa fa-home fa-fw"></i> Home</a>
</li>
<li>
<a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Statistics<span class="fa arrow"></span></a>
<ul class="nav nav-second-level">
<li><a href="/"><i class="fa fa-align-left fa-fw"></i> Pool</a></li>
<li><a href="index_page_statistics_action_blocks.html"><i class="fa fa-th-large fa-fw"></i> Blocks</a></li>
<li><a href="/"><i class="fa fa-refresh fa-fw"></i> Round</a></li>
<li><a href="/"><i class="fa fa-search fa-fw"></i> Blockfinder</a></li>
<li><a href="index_page_statistics_action_uptime.html"><i class="fa fa-clock-o fa-fw"></i> Uptime</a></li>
<li><a href="/"><i class="fa fa-signal fa-fw"></i> Graphs</a></li>
<li><a href="index_page_statistics_action_donors.html"><i class="fa fa-bitbucket fa-fw"></i> Donors</a></li>
</ul>
<!-- /.nav-second-level -->
</li>
<li>
<a href="#"><i class="fa fa-question fa-fw"></i> Help<span class="fa arrow"></span></a>
<ul class="nav nav-second-level">
<li><a href="/"><i class="fa fa-question fa-fw"></i> Getting Started</a></li>
<li><a href="/"><i class="fa fa-info fa-fw"></i> About</a></li>
</ul>
<!-- /.nav-second-level -->
</li>
<li>
<a href="#"><i class="fa fa-tasks fa-fw"></i> Other<span class="fa arrow"></span></a>
<ul class="nav nav-second-level">
<li><a href="/"><i class="fa fa-sign-in fa-fw"></i> Login</a></li>
<li><a href="/"><i class="fa fa-pencil fa-fw"></i> Sign Up</a></li>
<li><a href="/"><i class="fa fa-envelope fa-fw"></i> Contact</a></li>
<li><a href="/"><i class="fa fa-book fa-fw"></i> Terms and Conditions</a></li>
</ul>
<!-- /.nav-second-level -->
</li>
</ul>
<!-- /#side-menu -->
</div>
<!-- /.sidebar-collapse -->
</nav>
<!-- /.navbar-static-side -->
<div id="page-wrapper"><br/>
<div class="row">
<div class="col-md-12" style="background: #ffcaca;border-radius: 5px;margin-bottom: 5px;">
<h3>PoW mining is no more. PoS will take care of all transaction confirmations.</h3>
<p>Proof of work mining end at block 262,800 on BCC blockchain. The only way to verify new block and get rewarded is BitConnect Coin Staking. Know more about staking <a href="https://bitconnect.co/bitcoin-information/18/investing-in-bitconnect-coin-and-staking" target="_blank">here</a>.</p>
<p><b>- BitConnect Team </b></p>
</div>
<div class="col-md-12">
<div class="col-md-6" style="text-align:center;">
<h4 style="text-align:right;color:#3c3c3c;font-weight:900;">1 <span style="color:#fa890f;">BCC</span> =   <span style="color:#fa890f;">USD</span></h4>
<h4 style="text-align:right;color:#3c3c3c;font-weight:900;">1 <span style="color:#fa890f;">BCC</span> =   <span style="color:#fa890f;">BTC</span></h4>
<img alt="Bitconnect" src="assets/images/pool/bitconnect-big.png" style="width:300px;" title="Bitconnect"/>
<h3 style="text-align:center;">Welcome to BitConnect(BCC) Pool</h3>
<p style="margin-bottom: 50px;font-size: 15px;">BitConnect (BCC) Mining pool is a way for BitConnect coin miners to pool their resources together and share their hashing power while splitting the reward equally according to the amount of shares they contributed to solving a block.</p>
<div class="col-md-12" style="margin-bottom:50px;">
<div class="col-md-6">
<div style="background: #fa890f;border-radius: 5px;color: #fff;padding: 1px;">
<h4 style="text-align:center;"><a href="https://bitconnect.co/" style="color: #2e2e2e;font-weight: 700;" target="_blank">Network Hashrate</a></h4>
<p><b><span class="overview" id="b-nethashrate">n/a</span>
<span class="overview-mhs"> MH/s</span>
</b></p></div>
</div>
<div class="col-md-6">
<div style="background: #b7b7b7;border-radius: 5px;color: #fff;padding: 1px;">
<h4 style="text-align:center;"><a href="#" style="color: #2e2e2e;font-weight: 700;" target="_blank">Pool Hashrate</a></h4>
<p><b><span class="overview" id="b-poolhashrate">0.00</span><span class="overview-mhs"> MH/s</span></b></p>
</div>
</div>
</div>
<p>Bitconnect is a platform for Bitcoin or other electronic currency users to make a profit, buy and sell and exchange Bitcoins directly between the other members. For Germany based users we advise to use <a href="https://kryptoszene.de/handel/kryptowaehrungen-kaufen/bitcoin-kaufen/">bitcoin kaufen mit Kryptoszene</a> for a full variety of services.
 Bitconnect provides many features with the aim of creating more financial resources for the people and make the world become more open and better connectivity.</p>
<div class="col-md-12">
<div class="col-md-4">
<img alt="Bitconnect" src="assets/images/pool/bitconnectcoin.png" style="width: 70px;" title="Bitconnect"/>
<h4 style="text-align:center;"><a href="https://bitconnectcoin.co/" style="color: #2e2e2e;font-weight: 700;" target="_blank">BitConnect Coin</a></h4>
<p>THE CRYPTOCURRENCY REVOLUTION</p>
</div>
<div class="col-md-4">
<img alt="Bitconnect" src="assets/images/pool/bitconnect-lending.png" style="width: 70px;" title="Bitconnect"/>
<h4 style="text-align:center;"><a href="https://bitconnect.co/" style="color: #2e2e2e;font-weight: 700;" target="_blank">BitConnect Lending</a></h4>
<p>Build trust and reputation in bitcoin ecosystem</p>
</div>
<div class="col-md-4">
<img alt="Bitconnect" src="assets/images/pool/bitconnect-trading.png" style="width: 70px;" title="Bitconnect"/>
<h4 style="text-align:center;"><a href="https://bitconnect.co/user/trade?Market=BCC" style="color: #2e2e2e;font-weight: 700;" target="_blank">BitConnect Trading</a></h4>
<p>This investment option can be used to profit on price fluctuation of BitConnect Coin.</p>
</div>
</div>
</div>
<div class="col-md-6">
<div class="row">
<div class="col-lg-12">
<div class="panel panel-info">
<div class="panel-heading">
<i class="fa fa-bookmark"></i> Satoshi Labs Just Launched The Trezor Model T 
        <br/>
<font size="1px">posted 11/07/2017 07:32:35</font>
</div>
<div class="panel-body">
<p><span style="font-weight: bold;"><span style="color: rgb(53, 53, 53); font-family: Lato, sans-serif; font-size: 16px; letter-spacing: 0.52px; background-color: rgb(255, 255, 255);">As </span><span style="box-sizing: border-box; outline: 0px; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; font-family: Lato, sans-serif; font-size: 16px; color: rgb(53, 53, 53); letter-spacing: 0.52px; background-color: rgb(255, 255, 255);">decentralization</span><span style="color: rgb(53, 53, 53); font-family: Lato, sans-serif; font-size: 16px; letter-spacing: 0.52px; background-color: rgb(255, 255, 255);"> becomes takes a hold of the financial space, the </span><span style="box-sizing: border-box; outline: 0px; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; font-family: Lato, sans-serif; font-size: 16px; color: rgb(53, 53, 53); letter-spacing: 0.52px; background-color: rgb(255, 255, 255);">benefits of third party-less financial transactions are being realized.</span><span style="color: rgb(53, 53, 53); font-family: Lato, sans-serif; font-size: 16px; letter-spacing: 0.52px; background-color: rgb(255, 255, 255);"> No fees, no intermediary, all that sort of thing.<a href="https://bitconnect.co/bitcoin-news/802/satoshi-labs-just-launched-the-trezor-model-t/">Read More</a></span></span></p>
</div>
</div>
</div>
</div>
<div style="display:none;">
1
</div>
<div class="row">
<div class="col-lg-12">
<div class="panel panel-info">
<div class="panel-heading">
<i class="fa fa-bookmark"></i> Bitcoin: From Most Popular Cryptocurrency To Legitimate Financial Asset 
        <br/>
<font size="1px">posted 11/07/2017 07:31:19</font>
</div>
<div class="panel-body">
<p><span style="font-weight: bold;"><span style="color: rgb(53, 53, 53); font-family: Lato, sans-serif; font-size: 16px; letter-spacing: 0.52px; background-color: rgb(255, 255, 255);">Since its humble beginnings, a lot has been discussed about the future of Bitcoin and whether it in fact does have a future. Visit <a href="https://bitcoinsupersplit.com/">bitcoinsupersplit.com</a> to know how bitcoin trading bots can support Bitcoin's future. </span><span style="color: rgb(53, 53, 53); font-family: Lato, sans-serif; font-size: 16px; letter-spacing: 0.52px; background-color: rgb(255, 255, 255);">A lot of corporations donât think so. </span><span style="box-sizing: border-box; outline: 0px; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; font-family: Lato, sans-serif; font-size: 16px; color: rgb(53, 53, 53); letter-spacing: 0.52px; background-color: rgb(255, 255, 255);">Jamie Dimon, CEO of JPMorgan Chase,<a href="https://bitconnect.co/bitcoin-news/801/bitcoin-from-most-popular-cryptocurrency-to-legitimate-financial-asset/">Read More</a></span></span></p>
</div>
</div>
</div>
</div>
<div style="display:none;">
2
</div>
<div class="row">
<div class="col-lg-12">
<div class="panel panel-info">
<div class="panel-heading">
<i class="fa fa-bookmark"></i> Nick Szabo Developed a Method of Sending Bitcoin Transactions Over Radio 
        <br/>
<font size="1px">posted 11/07/2017 07:28:55</font>
</div>
<div class="panel-body">
<p><span style="font-weight: bold;"><span style="color: rgb(53, 53, 53); font-family: Lato, sans-serif; font-size: 16px; letter-spacing: 0.52px; background-color: rgb(255, 255, 255);">At the </span><span style="box-sizing: border-box; outline: 0px; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; font-family: Lato, sans-serif; font-size: 16px; color: rgb(53, 53, 53); letter-spacing: 0.52px; background-color: rgb(255, 255, 255);">Scaling Bitcoin Conference</span><span style="color: rgb(53, 53, 53); font-family: Lato, sans-serif; font-size: 16px; letter-spacing: 0.52px; background-color: rgb(255, 255, 255);"> held at </span><span style="box-sizing: border-box; outline: 0px; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; font-family: Lato, sans-serif; font-size: 16px; color: rgb(53, 53, 53); letter-spacing: 0.52px; background-color: rgb(255, 255, 255);">Stanford University,</span><span style="color: rgb(53, 53, 53); font-family: Lato, sans-serif; font-size: 16px; letter-spacing: 0.52px; background-color: rgb(255, 255, 255);"> Bitcoin pioneer </span><span style="box-sizing: border-box; outline: 0px; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; font-family: Lato, sans-serif; font-size: 16px; color: rgb(53, 53, 53); letter-spacing: 0.52px; background-color: rgb(255, 255, 255);">Nick Szabo</span><span style="color: rgb(53, 53, 53); font-family: Lato, sans-serif; font-size: 16px; letter-spacing: 0.52px; background-color: rgb(255, 255, 255);"> and former Stanford engineer Elaine Ou introduced a working method of securely sending Bitcoin transactions over radio signals.<a href="https://bitconnect.co/bitcoin-news/800/nick-szabo-developed-a-method-of-sending-bitcoin-transactions-over-radio/">Read More</a></span></span></p>
</div>
</div>
</div>
</div>
<div style="display:none;">
3
</div>
<div class="row">
<div class="col-lg-12">
<div class="panel panel-info">
<div class="panel-heading">
<i class="fa fa-bookmark"></i> This Company Just Gained 35% On Bitcoin Adoption 
        <br/>
<font size="1px">posted 11/04/2017 08:44:11</font>
</div>
<div class="panel-body">
<p><span style="color: rgb(53, 53, 53); font-family: Lato, sans-serif; font-size: 16px; letter-spacing: 0.52px; background-color: rgb(255, 255, 255); font-weight: bold;">Shifting towards adoption can be a great thing for a company that operates in a country where adoption is on the up whatever industry you are in but when that industry involves finance <a href="https://bitconnect.co/bitcoin-news/799/this-company-just-gained-35-on-bitcoin-adoption/">Read More</a></span></p>
</div>
</div>
</div>
</div>
<div style="display:none;">
4
</div>
<div class="col-md-12" style="text-align:center;">
<a class="btn btn-warning btn-sm" href="https://bitconnect.co/bitcoin-news/" style="width: 150px;background: #fa890f;border: #fa890f;font-size: 15px;" target="_blank">View More</a>
</div>
</div>
</div>
</div>
</div>
<div class="footer" style="color:#fff;">
<font size="1">
</font><center>
             Powered by <a href="https://bitconnect.co/" target="_blank">BitConnect</a> | Donate to 8VKbzETBUDXeGeEC2kujSKE2kojK1e6HEm
            </center>
</div>
</div></body>
</html>