<!DOCTYPE html>
<html>
<head>
<title>Nothing found</title>
<meta charset="utf-8"/>
<style>
      @font-face {
        font-family: 'BrownStd-Bold';
        src: url('https://www.billboard.com/assets/1608590758/fonts/BrownStd/BrownStd-Bold.woff?d78fa00e3ca1a63edbdf');
        font-weight: normal;
        font-style: normal;
      }

      @font-face {
        font-family: 'BrownStd-Regular';
        src: url('https://www.billboard.com/assets/1608590758/fonts/BrownStd/BrownStd-Regular.woff?d78fa00e3ca1a63edbdf');
        font-weight: normal;
        font-style: normal;
      }
      * {
        margin: 0;
        padding: 0;
      }
      html, body{
            height: 100%;
            width: 100%;
          }
      body {
        font: 17px/24px 'BrownStd-Regular', sans-serif;
      }
      p {
        margin: 15px 0;
      }
      a {
        color: #3db1f4;
        text-decoration: none;
      }
        a:hover {
          text-decoration: underline;
        }
      #xouter{
        height:100%;
        width:100%;
        display: table;
        vertical-align: middle;
        padding: 0
      }
      #xcontainer {
        text-align: center;
        position: relative;
        vertical-align: middle;
        display: table-cell;
        *margin-top: 100px;
      }
      #xinner {
        padding: 15px 30px;
        text-align: left;
        margin: 0 auto 75px;
        position: relative;
        width: 650px;
        border: 1px solid #e6e6e6;
        text-align: center;
      }
      #logo {
        background: url("https://www.billboard.com/assets/1608590758/images/sprite-2013-01-25.png?d78fa00e3ca1a63edbdf") no-repeat scroll 0 -2400px transparent;
        width: 147px;
        height: 30px;
        margin: 0 auto;
        text-indent: -9999px;
      }
      #beta {
        background: url("https://www.billboard.com/assets/1608590758/images/sprite-2013-01-25.png?d78fa00e3ca1a63edbdf") no-repeat scroll -274px -67px transparent;
        width: 24px;
        height: 7px;
        margin: 0 auto;
        text-indent: -9999px;
        position: relative;
        right: -43px;
      }
      #copyright {
        text-align: center;
        color: #b2b2b2;
        font-size: 13px;
        line-height: 17px;
        font-family: sans-serif;
      }
      #hold_footer {
        border-top: 1px solid #e6e6e6;
        padding-top: 30px;
        margin-left: -30px;
        margin-right: -30px;
        margin-top: 30px;
      }
      h1 {
        font-size: 35px;
        line-height: 42px;
        margin-bottom: 15px;
        margin-top: 15px;
      }
    </style>
<script async="" src="/cdn-cgi/bm/cv/669835187/api.js"></script></head>
<body>
<div id="xouter">
<div id="xcontainer">
<div id="xinner">
<h1>Looking for something on<br/>Billboard?</h1>
<p>Sorry, whatever you were looking for is no longer here, or you typed in the wrong URL. Please double-check and try again.</p>
<p><a href="/">Homepage</a></p>
<div id="hold_footer">
<p id="logo">Billboard</p>
<p id="beta">beta</p>
<p id="copyright">© 2021 Billboard Media, LLC. All Rights Reserved.</p>
</div>
</div>
</div>
</div>
<script type="text/javascript">(function(){window['__CF$cv$params']={r:'611a42ac9d7522d8',m:'88c890c72337f58e8f0437d0bb0fa9b358e2b2ef-1610657753-1800-AUF5gUZL6XABOXI2PTFrJPa5xed4QgyPy6+cy5cQg7ddn65Lu2MrkkzYTNQkxYAfEBo5UtF38OFkU1dGknAjhtLG473U1a0ZkWyuuDpQIYcMS8SSDIKj8flR6AVAb/J+yEYXorZk/DVLGCVhYRd7gg4=',s:[0xe6eaa96af6,0x9ec9cbe4da],}})();</script></body>
</html>
