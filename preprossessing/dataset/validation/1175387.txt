<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" lang="pl-pl" xml:lang="pl-pl" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>404 - ArtykuÅu nie znaleziono</title>
<link href="/templates/system/css/error.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="error">
<div id="outline">
<div id="errorboxoutline">
<div id="errorboxheader">404 - ArtykuÅu nie znaleziono</div>
<div id="errorboxbody">
<p><strong><strong>MoÅ¼liwe, Å¼e nie moÅ¼esz zobaczyÄ tej strony, poniewaÅ¼</strong></strong></p>
<ol>
<li>UÅ¼yta zakÅadka jest nieaktualna</li>
<li>Twoja wyszukiwarka <strong>nie odÅwieÅ¼yÅa jeszcze mapy naszej witryny</strong></li>
<li>Adres zostaÅ wpisany z bÅÄdem</li>
<li>Nie masz uprawnieÅ do obejrzenia tej strony</li>
<li>Joomla! nie moÅ¼e zlokalizowaÄ wskazanego zasobu.</li>
<li>WystÄpiÅ bÅÄd podczas wykonywania powierzonego zadania.</li>
</ol>
<p><strong>SprÃ³buj jednej z nastÄpujÄcych stron:</strong></p>
<ul>
<li><a href="/index.php" title="PrzejdÅº na stronÄ startowÄ">Strona startowa</a></li>
</ul>
<p>JeÅli problem siÄ powtarza, skontaktuj siÄ z administratorem witryny.</p>
<div id="techinfo">
<p>ArtykuÅu nie znaleziono</p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
