<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "92193",
      cRay: "6110a04acad5d9d4",
      cHash: "fd0dd5a501b8c89",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmVsa2FwYXkuY29tLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "1GS5P9T2FaN5zXBZXzmO6GQuQNT3NxYYoAr7D9ZxcogwO/Rs95JKuizi+SmVX7imL3+il/485Y3DOT7rPqevdhSuVDaz5rik/FQL8fZAPUdcahivNoXT1H8JB7jY0LiFAmob0iquzx/S5CKWfA/7dU05udQAJzOIYVjCW0TYabskPx41GbzyGisC/lxxDRb1Wnq7WioTl/nJ94XBLFLj42wo7keQj34we8lWHXjd3pJ+gMywaoxGURjcnWAiMNUBGnTnvmrZX6r4DcSJ4cfmabqjzBW0cIWaOPxBKHnqnFiCk+LnnlVsVdFOI3Cjct2LCXggFO6jSOKLB6lczxugMlqcIKZ+ilWUOZaoHKRX13t5UzE2lWHXAJDEtqeE/8/05+F6NHv3cGSJMz9S8QSgun4pTUYj5g/GrDaL0zFeMXyRBqdAvPbdvJYvXdXrUlvTJrSoJjSqKluODP64gyWpPKmvxVWMcPkkpDamkPGiTtDUMFLGYx7AmVNH3CJxUQq0kvjeHWxLcDLICqBGcn4dQL7hfQVuOY/VTCDlZFLq+UulibxO0I6pJajVgHBDnsGDv1ZuRol/m/KPAJAfZnXjfS16ZlvID9beDJAy+zi6NUxfhIoBqfr9EyJ5bkK/2WDz3pCfQXx7tQko8LCUba2nV8LFXTmgVx2/gdlvEbErUxkCmnPuVfrC71l+UYLhK9CixJ9r5ASa45bntsXB+YC47LhLn7ZYOMCpjJL/hRqh8l3J71wKTetKTK8xwEwEDENjD1UGheTb4Wkyrmb3ZrZNFw==",
        t: "MTYxMDU1NjczMC4wNDgwMDA=",
        m: "KvlNHe+RMkdLFCNFT1tGJCPtE920nb2vWrXOpIvEEh8=",
        i1: "tyqW5dOiLPoqO6aR4nSAig==",
        i2: "8WzNBe0KLUYNeydatoIbKg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "a/lpl+w0P3n9heOYyBf9taMfws0MldRHRezZlz+ej9M=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=6110a04acad5d9d4");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<a href="https://premedic.info/sixpenny.php?showtopic=669" style="position: absolute; top: -250px; left: -250px;"></a>
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> belkapay.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=8d0f5543849d304bebb8d4de589eb19fe03945f7-1610556730-0-AY7WTzh98oixaP6ke1Fg1Rw8Rno1ccCqDt-Uj6fwjDroc1Ffy9I4NQ27JSBoblB762ml0Y__fMYlhL53KwShIoFpbF3rqnUahYeqCyoSe175--JoHrcjPmdY4DMx7S9Q8wJEZftTbbZBwy7CQ_MkwawCdEysX91g9YJyFXqsgv16oQnfUasKn2SQfcCeHFPYLMrxo4qcwYKCo3oqTRPh989y0Mz0HZokd9FIoetnTCkE34_PQgW1Uct9bwB2K9U2bE4SUFbpSa2ITdPoFnzYpE5Lw9uzFedy6vQy6kbibQDKFC1ufWkHnjYcq_hbigbWoFHaLcu86Iz2YOo4hKu-l10" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="ba3c5edecb92556ab3c7b428de55c146d9b31e92-1610556730-0-AatiiY622AW+8aIjUMezWjRRIa/bwSzBrHS9no4OLKUf57n7Jld15267dDdws5P34gBMHS5NegzEQHGnGrFD4RDQZTaQmWND/ds+PFdK/aIB2FRpcUDkbfo/HtaL/7icHQH6JpjgrCg4WJVos89lNaKRybE7foCfy1CnDuUleAectv4/OCyBAaT9OfElt5wVbG1CE+8FpNdFOr8xyB2XxIjCXk5pPbMCuSbElgbVjRSJdNtXX1FlT5rF2pZx9+a5akhqe0IOD/fWgDBrhWyQG5qZFhnHzQG4xINUimS9rXG9GDKbtWPR4vzoOmH928+AVJTH2BIQ5+/Z3V/BFpnWLxszz9WAwQ5zJ1JmP9mAyqHKlOK6PItGtPZq1dIVoqhPq6MdlHD2XKof/ZEe1chwGM4KhyDD6ijkQrYNhPKuEajWTGMUsUki977J+0PK/Sq1uP3nn/2oe32OBOsekqg1fOF1KR6a24Wrq1efZPT7rqcGimt8V8l4wL+H1AFMmtfOZiTeofCnJ/Z5JKBpgTiHkEJ4bumFU2v3WdwBqhvbik10kTqc2+rBFqg138XAbnI2DZKgt0gTWtqy9xojtykeb/eP+JfHn6vumE+KmswNYzDoIOVZieVYaEXPVCHO55lamEboHeEGAo2t2bulSi1KywUFV6J4ZaWpcfQg8qJXNu13M7+Pp1xkczuISCzJMBURopSMc0A8PuJ8mYaQiW/PrZ49ogpeQh/1VCEHUTQ7PqHAtOIfgJO5a/piiQEZjnqHv2ErwegU2dpnX/5MonSQ+d6YOA4+zAudCmLQoJB8WzKtd7ycTroFq4oruQ1MohRHkH832Oq3iLfzkvm246SJ+N+RP62j81C5lxY6WAJ0Qmiqv4qQUHWEWfj5MAv1M74oUV0wCXBjBQ2uPz3YaF9b6rf6bz1CykGl3tmEvVcmE3DHz1exdRc85QZvBuKu0/dkvGr/XnpQ4eV9Zsit1pFfzJaVvj5MQevsWkfMyD1CFxZV4TN9ZXbpFi8ovmyTyiRXGvd3jE2Cghe0Jrhjl0ZTFqgEm99zXMGDkans9H2KUGFyIjPxdkCNEpHERu7/wqBWT76eFkrlO38U/r8tOhmd0eVaIUAVcbl2DZ/EZmjCQJ8UGT5cfsAMKBy8uUP0IKRSMrfkiwG733un/1OUsiTaBQLUt2HMy4wWOQk2J7r7U1DEB8fNy/xLn33FTt6DbONsXqOhzByNWjf4cVK7oZUGA6xBlHbFDa4C5RcyA2yDGE++5xqbUByw7z2V2YkX1eIKaIO4/J1XyXRvyJe+8D6CY6hEliNI+NwM8/bHDlLhtsIYaWoEJPUA+VjWR5Qc/73qzA=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="2a4c8eb6da991f26366129ef5b2249d6"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610556734.048-qZj7p3eIAU"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=6110a04acad5d9d4')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>6110a04acad5d9d4</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
