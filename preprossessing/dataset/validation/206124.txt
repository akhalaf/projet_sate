<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "378",
      cRay: "6110952eead322d8",
      cHash: "5bbebf6c13cfd20",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmVlcmFkdm9jYXRlLmNvbS9iZWVyL3Byb2ZpbGUvMjIwLzkxNi8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "+n5gW69t/Bqf0OGd0AA1EC6WAaA+j3eE7J2KuyC+XBNnZqKOxQ7FJ+neCQ1xd6O3r102NSljPq9FNaPhXcHzV/yZ0i+DCB/2rbsM3LFp6z0yhaSX+34Vkqr/tyT5fFOpZCmcv9I4qFjja7lk295Men+VFwbfMn5tRCWGIhEjczW0ZIkg/bAnAFKuF5jlGSsj4WFqcImW6AKdCkkON8lZnunUQR9qDsiHKMMC2+u8hH0eINvj9ssANSMaCXBZ/7kMRsTq6FVO4GJY/7iVZZAeC67fDo6iPFJlB2kT97b/o3BCAsmBFht0s0zCT5ufXRTQnhAdMYCrSKbSEjoaaCBfIrhVDUthPk8KLnAsnDE2vYS5E+jrg1DT8bx4NUsfpfyOKSqDkuBE9jR56JrlHtSWnOQg4N5RIkkKyGJiANo0qlwtQR+xDbbP9O5355Nv8b18UBWKIC57zLmKdadPE+gdk1RQYzqMmotW5GOrHvBqJH3L4ZVtRoUgw1wso65zvkiR8The9gDMzJCdxNznd0DUzxA2zg/KPceFmA1qembvXzAnwyyYeoLFAcFOivRR7nQyTswsamAZpp5Awke6/OPyMad989cgwB143t6xTn+lLZZP5Dk4OFvZVWXDGX3i+wtUR7pYUm2CxwigCoCJ1xTqRrUslJMSXa2Fu2dN+0H04ghiSczM8Y0XEo4n2L0FzDZJZrbsBkvvDhftXmAJGhV7u6TdrM7v/mWxVAkMenKc11P5p45BkeOE8FO5cEqJJXeBH6CNd2XuUD5lzm5vmTEYzIGcxGdFKnBlqT8Rxw++zxs=",
        t: "MTYxMDU1NjI3NS4wMjUwMDA=",
        m: "fylCnjPlaNam/hJsObiM48f2uPvKN3f4A6H1+VeUnno=",
        i1: "AA+Y1bHdo4HE3U9uo6dQEg==",
        i2: "HDE+9Z/NqFmVcAFifpU5+w==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "zYASn4idax+sKdNUwa/ZIjZSEcxjjuQZWzVVFx7SL/Q=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=6110952eead322d8");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> beeradvocate.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/beer/profile/220/916/?__cf_chl_jschl_tk__=71642064452fec094053e3444444fd60e7833743-1610556275-0-AcUQ2ogHM-HA4m9LYHbrCsZ4GvLRhBhj6ZjQ6Da5imtA34YavORb0Enjy8TAICfVZbMoAr8YIEMAViQcTHPNp6HOoHlpAI4_2V0cxNFguFi57pXc0h3QO8gb2dbgrRnv9xdRIPiZE2y2sgnIuZ7z8UxAyna-nnHHos-jLJsPCGjrCZxKhdzI1uxdXUWRmhk5F1fAj0_SZJGV7MFtrBoHbm4zjGTJN2hrwg7zDVaL-qRxz3R3GYvYRCazhEWER3dwjS4rpdKgrioAJj2PXDZ7xDva-5dsdxP2N2KfG2xkFozYyW-m7XDLUegk91QpxYf9ROSMRVZEWyvLGSnd7EklSSuafHFkFCibK1M-L_aKlDJ2dC7KXYZkwEgQs5djTB1QUKmrUxjNu0dTeNQ3cvYFrug" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="82fc8baa3d7b9744d5d65dbcf72145f54cee3be4-1610556275-0-ARnqmMtM24d4Fi0vjDwINCMcrfB05RdLxd04XuCwlgwjL7O+1GXGQr+n5E4+EwTLJ0H67BerzE2O+hZQ24VnYZ3gthOgHbOl+tO5d3+2Alk8YhvvQ5+A1pG+/uORmM2ZkHA92l31hcd5Yhocvdu65FCr22dfjVxaZjdZs6OIlyEBV9Mf3BHq+EHROm1NZGenotRYm5rXQMlfQVIyyQhjX7hTAyYnCIFSYDcD5s32g/scPR5vjx+l5dKZwN4JRW/f0ESMihy5SnCtiQi6VMeI+SGqvk9yEF3beiTgNROiif5elkLGJs4VvnBGiX/5t4PKdcKYFX4AI1au57YD/qKzbI6ZdDPuyfqmxXzBdQ/1EAeRqWDfrR4RFKR90eIx5GMINObWe6YnJ8697XrDdAabX00b8rnUAZYUoGWi61Xv+j8vsuvMbZ3IjIqDk35eBggIfxKukA6H0zp3ofmtY8SEIhbvF2zHnzURowBo2AUckEgO4kfGKUCE/yXGXkLGiFG8DaRVVZL7cYAbF48Ih7gdrMl//WTHf8u8ZP0f5zrcB/t2CDM1jR6N2N5Hu0Y66jSmli0iBZlCfNTM06W9aAqEXJWGPlUraXOwN9bp/n1H3cgY3tPyQFdDVcpdJnABIV47ldtJ7Oceer38uSTKx2Zy3jeSa6XaMoiV/fJUT2lRhSFRKvqQxC/LwjDeCpjl1jS83RqLjwI4N2XxWlnl8g4bx9jfSe/9IMlhMB9GMDoLQzNjSpN8imXym5K51nku/aFYLJxoSHWe+9fy9duDc43e0s5PaPVTBXFGTy7KwTl6Ietv3p+bWDdZ5rrDaGN3AQs4wW198H3cFPF3GcPxjk/9LqAl5kwvDNhRrarDNpv+K2v17/6vFLtCBnKut+B0B9d2A+9GxX/24wWf65Q+UwzeR5HSizYAWamtnvRmmlq1l6oQsJn0PKcT/koLuVpllxP9JvqArBJacocSyoEJ/cFwWROtzWxi8xM4Wu5kOs+gOy4Q4tX+ISrYXvbBYGhUPbrdzWSUT7lViE7xanrBhLUmmUpnuGS711WAl7D+hj9kCMV9saAVhA/wahK1ANqyaNHeSp7ACv3kJx3INxo7c2OFhfCAD2f75IGZRgNBJikmqNo/LPWHTaZljvxZP4KjjozR6Gt2VXjrBp2fjnr/LUJH8pTgZx3oRbpTAor9+7Dj65WsOE5Ayb5QHDt6FpB65I2JFsKaMnVxUyOqACLWOyrT4HdWRgqoPDQcyA/jFM+HXZGrlbu2n46R2I++fk4aRFofvllvvK/T5LyJiJH8iZMBYZCuB/i5RlEtUAtfBrBgXhpnxAQfRMQFk9DYEpYYRTyuOQ=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="f39f08d7f2027835f681ac0ef90930f4"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610556279.025-tocgriRnDU"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=6110952eead322d8')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>6110952eead322d8</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
