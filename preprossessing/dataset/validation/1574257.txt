<html><body><p>ï»¿<!DOCTYPE HTML>

</p><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<!-- Header Base -->
<title>Web und Mehr</title>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- Viewport Meta-Tag -->
<!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
<meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport"/>
<!-- Stylesheets -->
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<link href="css/main_style.css" rel="stylesheet"/>
<link href="css/responsive.css" rel="stylesheet"/>
<link href="css/hover.css" media="all" rel="stylesheet"/>
<link href="css/demo-page.css" media="all" rel="stylesheet"/>
<!-- NAVIGATION -->
<link href="css/menu_styles.css" rel="stylesheet"/>
<script src="js/jquery-latest.min.js" type="text/javascript"></script>
<script src="js/script.js"></script>
<!-- -->
<!-- SLIDER -->
<link href="css/demo.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="css/flexslider.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" rel="stylesheet" type="text/css"/>
<!-- Modernizr -->
<script src="js/modernizr.js"></script>
<!-- SLIDER END -->
<style>
        .js_enabled #cookie-message {
            display: none;
        }
       
        #cookie-message {
            background-color: #d5e6ed;
            color: #662d91;
            margin: 0 auto;
            padding: 1em;
        }
    </style>
<script>
    // Detect JS support
    document.body.className = document.body.className + " js_enabled";
</script>
<div class="top_header">
<div class="container">
<div class="top_header_left_list">
<ul>
<li>Ihr Online Partner </li>
<li class="top_phone">062 555 21 56  </li>
<li class="top_email">info@webundmehr.ch </li>
</ul>
</div>
<div class="top_header_right_menu">
<ul>
<li> <a href="https://www.webundmehr.ch"> Home</a> </li>
<li> <a href="kontakt.html"> Kontakt  </a> </li>
<!--<li> <a href="#"> Blog</a> </li>-->
</ul>
</div>
</div>
</div>
<!-- MAIN CONTAINER START -->
<div class="container_main">
<div class="container">
<!-- HEADER START -->
<div class="header_main">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
<div class="logo"> <a href="https://www.webundmehr.ch"> <img src="images/logo.png"/> </a> </div>
<div class="logo_sm"> <a href="https://www.webundmehr.ch"> <img src="images/logo-sm.png"/> </a> </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
<div class="header_btn_2"> <a href="kontakt.html" title="kontakt"> <img src="images/free-quote-btn.png"/> </a> </div>
</div>
</div>
</div>
</div>
<!-- HEADER END -->
</div>
<div class="navigation_full">
<div class="nav">
<div class="container">
<div class="menu_main">
<div id="cssmenu">
<ul>
<li><a class="menuactive" href="index.html">Home</a></li>
<li><a href="uber-uns.html">Ãber uns</a></li>
<li><a href="web-design.html">Web Design</a></li>
<li><a href="web-shop.html">Webshop</a></li>
<li><a href="online-marketing.html">Online Marketing </a></li>
<li><a href="apps.html">Apps</a></li>
<li><a href="illustrations.html">Illustrationen </a></li>
<li><a href="logos.html">Logos</a></li>
<!--<li class='active'><a href='#'>Inspiration</a>
                                  <ul>
                                     <li><a href='#'>Jainism</a> </li>
                                     <li><a href='#'>Bhagwan Mahavir</a> </li>
                                     <li><a href='#'>Shrimad Rajchandraji</a> </li>
                                     <li><a href='#'>"Bhaishree" Popatbhai Mohakamchand Shah</a> </li>
                                  </ul>
                               </li>
                               
                               
                               
                               <li ><a href='#'>Gujarat Raj Yatra </a>
                               </li>
                               
                                <!-- THREEE STAGE  <li class='active'><a href='#'>Events & Activities  </a>
                                  <ul>
                                  	<li><a href='#'>Upcoming events</a> </li>
                                      <li><a href='#'>Raj Seva (Youth) Activities</a> </li>
                                      <li><a href='#'>Upcoming Projects</a> </li>
                                      <li><a href='#'>Schedules</a> </li>
                                      
                                     <li><a href='#'>Download</a>
                                        <ul>
                                           <li><a href='#'>Videos</a></li>
                                        </ul>
                                     </li>
                                  </ul>
                               </li>
                               
                               <!--<li class='active'><a href='#'>Products</a>
                                  <ul>
                                     <li><a href='#'>Product 1</a>
                                        <ul>
                                           <li><a href='#'>Sub Product</a></li>
                                           <li><a href='#'>Sub Product</a></li>
                                        </ul>
                                     </li>
                                     <li><a href='#'>Product 2</a>
                                        <ul>
                                           <li><a href='#'>Sub Product</a></li>
                                           <li><a href='#'>Sub Product</a></li>
                                        </ul>
                                     </li>
                                  </ul>
                               </li>-->
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="cf" id="container">
<div id="main" role="main">
<section class="slider">
<div class="flexslider">
<ul class="slides">
<li>
<img src="images/slider-1.jpg"/>
</li>
<li>
<img src="images/slider-2.jpg"/>
</li>
<li>
<img src="images/slider-3.jpg"/>
</li>
<li>
<img src="images/slider-5.jpg"/>
</li>
</ul>
</div>
</section>
</div>
</div>
<div class="clearfix"> </div>
<div class="content_area_main">
<div class="container">
<div class="content_area">
<div class="home_welcome">
<h1 class="center"> <span class="heading_small">Herzlich Willkommen<br/>
</span>WEB UND MEHR</h1>
<div class="home_welcome_text">
<p>
                            Ob KMU, Ã¶ffentliche Verwaltung, Verein, Stiftung oder Einzelperson, wir bieten Ihnen ein umfassendes Angebot beim Erstellen von Kommunikations- und AbsatzkanÃ¤len auf dem Internet. Von einer einfachen, aber ansprechenden Webseite bis zu komplexen digitalen Projekten tragen wir unseren Teil zu Ihrem Erfolg bei.
                            </p>
</div>
<div class="more_center_btn" style="display:none;"> <a class="hvr-shutter-out-horizontal" href="#">Weiterlesen</a>
</div>
</div>
</div>
</div>
</div>
<div class="clearfix"> </div>
<div class="service_main_container">
<div class="container">
<h1 class="center"> <span class="heading_small">Was Wir Tun<br/>
</span>UNSERE DIENSTLEISTUNGEN </h1>
<div class="service_box_row">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
<div class="servicebox_main">
<div class="servicebox_icon"> <a href="web-design.html"> <img src="images/webd.png"/> </a> </div>
<div class="servicebox_title"> <a href="web-design.html"> <h2> Web Design </h2> </a> </div>
<div class="servicebox_text"> <p>BenÃ¶tigen Sie eine neue oder Ã¼berarbeitete Webseite? Wir erstellen Ihnen einen modernen, gut navigierbaren, Ã¤sthetisch ansprechendenden Internetauftritt.</p></div>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
<div class="servicebox_main">
<div class="servicebox_icon"> <a href="web-shop.html"> <img src="images/webshop.png"/> </a> </div>
<div class="servicebox_title"> <a href="web-shop.html"> <h2> WEB SHOP </h2> </a> </div>
<div class="servicebox_text"> <p>Mit diesem schnellwachsenden Absatzkanal kÃ¶nnen Sie den Umsatz Ihrer Waren und Dienstleistungen steigern. Wir beraten Sie und setzen Ihre WÃ¼nsche um.</p></div>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
<div class="servicebox_main">
<div class="servicebox_icon"> <a href="online-marketing.html"> <img src="images/intmarketing.png"/> </a> </div>
<div class="servicebox_title"> <a href="online-marketing.html"> <h2> ONLINE MARKETING </h2> </a> </div>
<div class="servicebox_text"> <p>Soll Ihre Webseite bei Google mÃ¶glichst auf der ersten Seite erscheinen? MÃ¶chten Sie Werbung bei den Suchmaschinen Google und Bing oder den Sozialmedien wie z.B. Facebook, Pinterest, Twitter schalten? Wir erarbeiten mit Ihnen ein Werbekonzept und setzen es um.</p></div>
</div>
</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
<div class="servicebox_main">
<div class="servicebox_icon"> <a href="mobile-apps.html"> <img src="images/mobile.png"/> </a> </div>
<div class="servicebox_title"> <a href='apps.html"'> <h2> APPS </h2> </a> </div>
<div class="servicebox_text"> <p> Sagen Sie uns, welche Anforderungen Ihre Software erfÃ¼llen soll, und wir erarbeiten zusammen mit Ihnen die optimale LÃ¶sung fÃ¼r Sie. </p></div>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
<div class="servicebox_main">
<div class="servicebox_icon"> <a href="illustrations.html"> <img src="images/graphicd.png"/> </a> </div>
<div class="servicebox_title"> <a href="illustrations.html"> <h2> GRAFIKDESIGN</h2> </a> </div>
<div class="servicebox_text"> <p>FÃ¼r Logos und Illustrationen jeglicher Art sind wir Ihr Ansprechpartner.</p></div>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
<div class="servicebox_main free_quote_box">
<div class="servicebox_title free_quote_box_title "> <h2> <span class="free_quote_box_title_small"> MÃCHTEN SIE MEHR WISSEN?</span></h2></div>
<div class="servicebox_icon"> <img src="images/quote-email.png"/> </div>
<div class="servicebox_text" style="min-height: 316px!important;text-align:center!important;"> <a href="kontakt.html"> <img src="images/free-quote-btn.png"/> </a> </div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="about_main">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<div class="home_about_img"> <img src="images/about-img.png"/>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<div class="home_about_content">
<div class="home_about_content_heading">
<h1 class="center"> <span class="heading_small"> Wer Wir Sind </span> <br/> ÃBER WEB UND MEHR </h1>
</div>
<div class="home_about_content_text">
<p>Unser Team von Spezialisten auf den Gebieten Webdesign, Web Shops, Illustrationen, Apps und Online Marketing wollen Ihnen und Ihren Kunden ein befriedigendes Erlebnis in der digitalen Welt bieten. Benutzerfreundlichkeit und ZuverlÃ¤ssigkeit stehen bei uns im Vordergrund. NatÃ¼rlich darf die Ãsthetik nicht zu kurz kommen.</p>
<br/>
<br/>
<div class="home_news_more"> <a class="hvr-shutter-out-horizontal border_btn" href="uber-uns.html">Weiterlesen</a> </div>
</div>
</div>
</div>
</div>
</div>
</div>
<!--<div class="botom_content_main">
        	<div class="container">
            	<div class="row">
                	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    	<div class="home_blog_main">
                        	<div class="home_about_content_heading">
                            	<h2> UNSER BLOG MIT INFOS, TRENDS UND MEINUNGEN</h2>
	                        </div>
                            <div class="row">
	                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            	<div class="home_blog_box">
                                	<div class="home_blog_box_img"> <img src="images/blog-img-1.png">
	                                </div>
                                    <div class="home_blog_box_date"> 03 Apr, 2016
	                                </div>
                                    <div class="home_blog_box_title">  <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt utoreetdolore... </p>
	                                </div>
                                    <div class="home_news_more">  <a href="#" class="hvr-shutter-out-horizontal border_btn">Weiterlesen</a> </div>
                                    
                                </div>
                            </div>
    	                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            	<div class="home_blog_box">
                                	<div class="home_blog_box_img"> <img src="images/blog-img-2.png">
	                                </div>
                                    <div class="home_blog_box_date"> 03 Apr, 2016
	                                </div>
                                    <div class="home_blog_box_title">  <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt utoreetdolore... </p>
	                                </div>
                                    <div class="home_news_more">  <a href="#" class="hvr-shutter-out-horizontal border_btn">Weiterlesen</a> </div>
                                    
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    	<div class="home_blog_main">
                        	<div class="home_about_content_heading">
                            	<h2> Powerpoint</h2>
	                        </div>
                            
                            <div class="home_blog_box">
                            	<div class="home_blog_box_title">  <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt utoreetdolore... </p>
	                                </div>
                                <div class="home_blog_box_img home_blog_ppt_img"> <a href="#"> <img src="images/ppt.png"> </a>
	                                </div>
                                </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
<div class="advantage_main">
<div class="container">
<h1 class="center" style="margin-bottom:20px;"><span class="heading_small"><br/>
</span>Ihre Vorteile</h1>
<div class="advantage_content_main">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<div class="home_about_img" style="padding-top:65px!important;"><img src="images/thumb.png"/></div>
</div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<div class="advantage_content_box">
<div class="advantage_content_text advantage_ioon_1">Wir vermeiden Fachjargon.</div>
</div>
<div class="advantage_content_box">
<div class="advantage_content_text advantage_ioon_2">Die erste Beratung ist kostenlos.</div>
</div>
<div class="advantage_content_box">
<div class="advantage_content_text advantage_ioon_3">Unser Angebot ist ein Festpreis.</div>
</div>
<div class="advantage_content_box" style="margin-bottom:0px!important;">
<div class="advantage_content_text advantage_ioon_4">Wir sind immer fÃ¼r Sie da!</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!--  MAIN CONTAINER END -->
<!-- FOOTER START -->
<div class="footer_main">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
<div class="footer_links">
<div class="footer_links_title"> ÃBER DAS UNTERNEHMEN</div>
<div class="footer_links_list">
<ul>
<li> <a href="uber-uns.html"> Ãber uns </a> </li>
<li> <a href="agb-datenschutz.html">AGB / Datenschutz </a> </li>
<li> <a href="kontakt.html">Kontakt </a> </li>
<li> <a href="sitemap.xml">Site Map </a> </li>
</ul>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
<div class="footer_links">
<div class="footer_links_title"> UNSERE DIENSTLEISTUNGEN</div>
<div class="footer_links_list">
<ul>
<li> <a href="web-design.html"> Web Design  </a> </li>
<li> <a href="web-shop.html">Webshop </a> </li>
<li> <a href="online-marketing.html">Online Marketing </a> </li>
<li> <a href="mobile-apps.html">Apps </a> </li>
<li> <a href="illustrations.html">Illustrationen </a> </li>
<li> <a href="logos.html">Logos </a> </li>
</ul>
</div>
</div>
</div>
<!--<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="footer_links">
                	<div class="footer_links_title no_border"> &nbsp; </div>
                    <div class="footer_links_list">
                    	<ul>
                        	<li> <a href="#"> Search Engine Optimization </a> </li>
                            <li> <a href="#">Dedicated Support </a> </li>
                            <li> <a href="#">Graphic Design  </a> </li>
                            <li> <a href="#">Ecommerce Development </a> </li>
                            <li> <a href="#">Some Development </a> </li>
                        </ul>
	                </div>
                </div> 
        	</div>-->
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
<div class="footer_links">
<div class="footer_links_title"> KONTAKTIEREN SIE UNS</div>
<div class="footer_links_list_adress">
<ul>
<li class="footer_adress"> Hauptstrasse 53,     
     CH-5074 Eiken</li>
<li class="footer_email"> info@webundmehr.ch  </li>
<li class="footer_phone"> +41 (0)62 555 21 56</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="last_footer">
<div class="container">
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<!--<div class="social-links">
                        <a href="#"><img src="images/fb.png"> </a>
                        <a href="#"><img src="images/gp.png"> </a>
                        <a href="#"><img src="images/twt.png"> </a>
                    </div>--><!-- .social -->
</div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<div class="copy"> Â© 2019-2020 Alle Rechte vorbehalten, Web und Mehr

            </div>
</div>
</div>
</div>
</div>
<!-- FOOTER END -->
<!--<script src="js/jquery.min.js"></script>-->
<script src="js/bootstrap.min.js"></script>
<!-- FlexSlider -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script defer="" src="js/jquery.flexslider.js"></script>
<script type="text/javascript">
    $(function(){
      SyntaxHighlighter.all();
    });
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>
<script data-cfasync="false" src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js"></script>
<script>
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#cda55f"
    },
    "button": {
      "background": "#017e99"
    }
  },
  "theme": "classic",
  "content": {
    "message": "Wir verwenden Cookies um die Webseite fÃ¼r Sie stetig verbessern zu kÃ¶nnen. Weitere Informationen finden Sie unter âDatenschutzâ. Mit Ihrem\nKlick auf dem âOKâ Knopf erklÃ¤ren Sie sich mit den Bedingungen einverstanden. ",
    "dismiss": "OK",
    "link": "Mehr zum Datenschutz",
    "href": "agb-datenschutz.html"
  }
});
</script>
</body></html>