<!DOCTYPE html>
<html lang="id-ID"><head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://smpn2bawen.sch.id/xmlrpc.php" rel="pingback"/>
<title>SMP NEGERI 2 BAWEN – Membangun Negeri Dari Desa</title>
<link href="//www.googletagmanager.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://smpn2bawen.sch.id/index.php/feed/" rel="alternate" title="SMP NEGERI 2 BAWEN » Feed" type="application/rss+xml"/>
<link href="https://smpn2bawen.sch.id/index.php/comments/feed/" rel="alternate" title="SMP NEGERI 2 BAWEN » Umpan Komentar" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/smpn2bawen.sch.id\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://smpn2bawen.sch.id/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://smpn2bawen.sch.id/wp-includes/css/dist/block-library/theme.min.css?ver=5.6" id="wp-block-library-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://smpn2bawen.sch.id/wp-content/themes/education-hub/third-party/font-awesome/css/font-awesome.min.css?ver=4.7.0" id="fontawesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans%3A600%2C400%2C400italic%2C300%2C100%2C700%7CMerriweather+Sans%3A400%2C700&amp;ver=5.6" id="education-hub-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://smpn2bawen.sch.id/wp-content/themes/education-hub/style.css?ver=2.3" id="education-hub-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="education-hub-style-inline-css" type="text/css">
#masthead{ background-image: url("https://smpn2bawen.sch.id/wp-content/uploads/2018/11/cropped-sa.png"); background-repeat: no-repeat; background-position: center center; }@media only screen and (max-width:767px) {
		    #page #masthead {
		        background-position: center top;
		        background-size: 100% auto;
		        padding-top: 40px;
		    }
		 }
</style>
<link href="https://smpn2bawen.sch.id/wp-content/themes/education-hub/css/blocks.css?ver=20201208" id="education-hub-block-style-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://smpn2bawen.sch.id/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://smpn2bawen.sch.id/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script async="" id="google_gtagjs-js" src="https://www.googletagmanager.com/gtag/js?id=UA-186436210-1" type="text/javascript"></script>
<script id="google_gtagjs-js-after" type="text/javascript">
window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('set', 'developer_id.dZTNiMT', true);
gtag('config', 'UA-186436210-1', {"anonymize_ip":true} );
</script>
<link href="https://smpn2bawen.sch.id/index.php/wp-json/" rel="https://api.w.org/"/><link href="https://smpn2bawen.sch.id/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://smpn2bawen.sch.id/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<meta content="Site Kit by Google 1.23.0" name="generator"/><meta content="uxXwAj2aS7K18gQUo06KdpN1JpyAFEYFTQlLpR4ob9Q" name="google-site-verification"/><style id="custom-background-css" type="text/css">
body.custom-background { background-color: #ededed; }
</style>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><script>(adsbygoogle = window.adsbygoogle || []).push({"google_ad_client":"ca-pub-8926265988065337","enable_page_level_ads":true,"tag_partner":"site_kit"});</script><link href="https://smpn2bawen.sch.id/wp-content/uploads/2018/11/cropped-cropped-download-10-1-1-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://smpn2bawen.sch.id/wp-content/uploads/2018/11/cropped-cropped-download-10-1-1-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://smpn2bawen.sch.id/wp-content/uploads/2018/11/cropped-cropped-download-10-1-1-180x180.png" rel="apple-touch-icon"/>
<meta content="https://smpn2bawen.sch.id/wp-content/uploads/2018/11/cropped-cropped-download-10-1-1-270x270.png" name="msapplication-TileImage"/>
</head>
<body class="home blog custom-background wp-custom-logo wp-embed-responsive site-layout-fluid global-layout-right-sidebar">
<div class="container hfeed site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<div id="tophead">
<div class="container">
<div id="quick-contact">
<ul>
<li class="quick-call"><a href="tel:085100100605">085100100605</a></li>
<li class="quick-email"><a href="mailto:tu@smpn2bawen.sch.id">tu@smpn2bawen.sch.id</a></li>
</ul>
<div class="top-news border-left">
<span class="top-news-title">
															Notice:													</span>
<a href="#">SMPN 2 BAWEN							</a>
</div>
</div>
<div class="quick-links">
<a class="links-btn" href="#">Quick Links</a>
<ul><li class="page_item page-item-18"><a href="https://smpn2bawen.sch.id/index.php/gallery/">GALERY</a></li>
<li class="page_item page-item-404"><a href="https://smpn2bawen.sch.id/index.php/kegiatan/">KEGIATAN</a></li>
<li class="page_item page-item-399 current_page_parent"><a href="https://smpn2bawen.sch.id/index.php/prestasi/">PRESTASI</a></li>
<li class="page_item page-item-17"><a href="https://smpn2bawen.sch.id/index.php/about/">PROFIL SEKOLAH</a></li>
<li class="page_item page-item-76"><a href="https://smpn2bawen.sch.id/index.php/visi-misi/">STATISKTIK</a></li>
<li class="page_item page-item-307"><a href="https://smpn2bawen.sch.id/index.php/pengumuman/">TENTANG</a></li>
</ul> </div>
</div> <!-- .container -->
</div><!--  #tophead -->
<header class="site-header" id="masthead" role="banner"><div class="container"> <div class="site-branding">
<a aria-current="page" class="custom-logo-link" href="https://smpn2bawen.sch.id/" rel="home"><img alt="SMP NEGERI 2 BAWEN" class="custom-logo" height="224" sizes="(max-width: 224px) 100vw, 224px" src="https://smpn2bawen.sch.id/wp-content/uploads/2018/11/cropped-download-10-1-1.png" srcset="https://smpn2bawen.sch.id/wp-content/uploads/2018/11/cropped-download-10-1-1.png 224w, https://smpn2bawen.sch.id/wp-content/uploads/2018/11/cropped-download-10-1-1-150x150.png 150w" width="224"/></a>
<div id="site-identity">
<h1 class="site-title"><a href="https://smpn2bawen.sch.id/" rel="home">SMP NEGERI 2 BAWEN</a></h1>
<p class="site-description">Membangun Negeri Dari Desa</p>
</div><!-- #site-identity -->
</div><!-- .site-branding -->
<div class="search-section">
<form action="https://smpn2bawen.sch.id/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search..." title="Search for:" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form> </div>
</div><!-- .container --></header><!-- #masthead --> <div class="clear-fix" id="main-nav">
<div class="container">
<nav class="main-navigation" id="site-navigation" role="navigation">
<button aria-controls="primary-menu" aria-expanded="false" class="menu-toggle">
<i class="fa fa-bars"></i>
<i class="fa fa-close"></i>
			Menu</button>
<div class="wrap-menu-content">
<div class="menu-menu-container"><ul class="menu" id="primary-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-434" id="menu-item-434"><a aria-current="page" href="http://smpn2bawen.sch.id">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-436" id="menu-item-436"><a href="https://smpn2bawen.sch.id/index.php/pengumuman/">TENTANG</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-437" id="menu-item-437"><a href="https://smpn2bawen.sch.id/index.php/visi-misi/">STATISKTIK</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-438" id="menu-item-438"><a href="https://smpn2bawen.sch.id/index.php/gallery/">GALERY</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-469" id="menu-item-469"><a href="https://smpn2bawen.sch.id/index.php/unduhan/">UNDUHAN</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-439" id="menu-item-439"><a href="https://smpn2bawen.sch.id/index.php/about/">PROFIL SEKOLAH</a></li>
</ul></div> </div><!-- .menu-content -->
</nav><!-- #site-navigation -->
</div> <!-- .container -->
</div> <!-- #main-nav -->
<div class="site-content" id="content"><div class="container"><div class="inner-wrapper">
<div class="content-area" id="primary">
<main class="site-main" id="main" role="main">
<article class="post-521 post type-post status-publish format-standard hentry category-uncategorized" id="post-521">
<header class="entry-header">
<h2 class="entry-title"><a href="https://smpn2bawen.sch.id/index.php/2021/01/12/rilis-pembaruan-aplikasi-dapodik-versi-2021-c/" rel="bookmark">Rilis Pembaruan Aplikasi Dapodik Versi 2021.c</a></h2>
<div class="entry-meta">
<span class="posted-on"><a href="https://smpn2bawen.sch.id/index.php/2021/01/12/rilis-pembaruan-aplikasi-dapodik-versi-2021-c/" rel="bookmark"><time class="entry-date published" datetime="2021-01-12T02:31:38+00:00">Januari 12, 2021</time><time class="updated" datetime="2021-01-12T02:31:39+00:00">Januari 12, 2021</time></a></span><span class="byline"> <span class="author vcard"><a class="url fn n" href="https://smpn2bawen.sch.id/index.php/author/pk9yjvmhtd/">Admin</a></span></span> </div><!-- .entry-meta -->
</header><!-- .entry-header -->
<div class="entry-content">
<p>Yth. Bapak/Ibu Kepala Dinas Pendidikan Provinsi Kepala Cabang Dinas Pendidikan Provinsi Kepala Dinas Pendidikan Kabupaten/Kota Kepala LPMP Kepala PP/BP PAUD dan Dikmas Kepala PAUD, SD, SMP, SMA, SMK, SLB, PKBM, dan SKB di seluruh Indonesia  Assalamualaikum warahmatullahi wabarakatuh Dengan hormat <a class="read-more" href="https://smpn2bawen.sch.id/index.php/2021/01/12/rilis-pembaruan-aplikasi-dapodik-versi-2021-c/">Read More …</a></p>
</div><!-- .entry-content -->
<footer class="entry-footer">
<span class="comments-link"><a href="https://smpn2bawen.sch.id/index.php/2021/01/12/rilis-pembaruan-aplikasi-dapodik-versi-2021-c/#respond">Leave a comment</a></span> </footer><!-- .entry-footer -->
</article><!-- #post-## -->
<article class="post-514 post type-post status-publish format-standard hentry category-uncategorized" id="post-514">
<header class="entry-header">
<h2 class="entry-title"><a href="https://smpn2bawen.sch.id/index.php/2021/01/09/syarat-mendaftar-pppk-guru-2021-sesuai-aturan-terbaru/" rel="bookmark">Syarat Mendaftar PPPK Guru 2021 Sesuai Aturan terbaru</a></h2>
<div class="entry-meta">
<span class="posted-on"><a href="https://smpn2bawen.sch.id/index.php/2021/01/09/syarat-mendaftar-pppk-guru-2021-sesuai-aturan-terbaru/" rel="bookmark"><time class="entry-date published" datetime="2021-01-09T03:38:04+00:00">Januari 9, 2021</time><time class="updated" datetime="2021-01-09T03:54:26+00:00">Januari 9, 2021</time></a></span><span class="byline"> <span class="author vcard"><a class="url fn n" href="https://smpn2bawen.sch.id/index.php/author/pk9yjvmhtd/">Admin</a></span></span> </div><!-- .entry-meta -->
</header><!-- .entry-header -->
<div class="entry-content">
<p>Apa itu PPPK atau p3k ? sobat guru PPPK merupakan program pemerintah untuk mengangkat pegawai dengan kontrak kerja pada waktu tertentu sesuai peraturan yang berlaku. Tahukah anda, Tahun 2021 PPPK diprioritaskan kepada Guru honorer yang memenuhi syarat saja. Melalui Pidato <a class="read-more" href="https://smpn2bawen.sch.id/index.php/2021/01/09/syarat-mendaftar-pppk-guru-2021-sesuai-aturan-terbaru/">Read More …</a></p>
</div><!-- .entry-content -->
<footer class="entry-footer">
<span class="comments-link"><a href="https://smpn2bawen.sch.id/index.php/2021/01/09/syarat-mendaftar-pppk-guru-2021-sesuai-aturan-terbaru/#respond">Leave a comment</a></span> </footer><!-- .entry-footer -->
</article><!-- #post-## -->
<article class="post-505 post type-post status-publish format-standard hentry category-uncategorized" id="post-505">
<header class="entry-header">
<h2 class="entry-title"><a href="https://smpn2bawen.sch.id/index.php/2021/01/09/diskusi-dengan-pemerintah-daerah-mengenai-seleksi-guru-pppk-tahun-2021/" rel="bookmark">DISKUSI DENGAN PEMERINTAH DAERAH MENGENAI SELEKSI GURU PPPK TAHUN 2021</a></h2>
<div class="entry-meta">
<span class="posted-on"><a href="https://smpn2bawen.sch.id/index.php/2021/01/09/diskusi-dengan-pemerintah-daerah-mengenai-seleksi-guru-pppk-tahun-2021/" rel="bookmark"><time class="entry-date published" datetime="2021-01-09T03:04:48+00:00">Januari 9, 2021</time><time class="updated" datetime="2021-01-09T03:10:21+00:00">Januari 9, 2021</time></a></span><span class="byline"> <span class="author vcard"><a class="url fn n" href="https://smpn2bawen.sch.id/index.php/author/pk9yjvmhtd/">Admin</a></span></span> </div><!-- .entry-meta -->
</header><!-- .entry-header -->
<div class="entry-content">
<p>Teknis pengajuan formasi 1.    Hal-hal yang perlu dilakukan Pemerintah Daerah untuk mengajukan formasi dan mencapai kuota formasi • Formasi sudah pernah ditampung dan ditutup tanggal 31 Agustus 2020 • Formasi saat ini dibuka lagi khusus formasi guru PPPK • Pemerintah <a class="read-more" href="https://smpn2bawen.sch.id/index.php/2021/01/09/diskusi-dengan-pemerintah-daerah-mengenai-seleksi-guru-pppk-tahun-2021/">Read More …</a></p>
</div><!-- .entry-content -->
<footer class="entry-footer">
<span class="comments-link"><a href="https://smpn2bawen.sch.id/index.php/2021/01/09/diskusi-dengan-pemerintah-daerah-mengenai-seleksi-guru-pppk-tahun-2021/#respond">Leave a comment</a></span> </footer><!-- .entry-footer -->
</article><!-- #post-## -->
<article class="post-490 post type-post status-publish format-standard hentry category-uncategorized" id="post-490">
<header class="entry-header">
<h2 class="entry-title"><a href="https://smpn2bawen.sch.id/index.php/2021/01/08/panduan-membuat-kelas-di-aplikasi-zeneus-dan-mengimport-kelas-dari-google-classroom-ke-aplikasi-zeneus/" rel="bookmark">Panduan Membuat Kelas di Aplikasi Zeneus dan Mengimport Kelas dari Google Classroom ke Aplikasi Zeneus</a></h2>
<div class="entry-meta">
<span class="posted-on"><a href="https://smpn2bawen.sch.id/index.php/2021/01/08/panduan-membuat-kelas-di-aplikasi-zeneus-dan-mengimport-kelas-dari-google-classroom-ke-aplikasi-zeneus/" rel="bookmark"><time class="entry-date published" datetime="2021-01-08T02:36:54+00:00">Januari 8, 2021</time><time class="updated" datetime="2021-01-08T02:36:56+00:00">Januari 8, 2021</time></a></span><span class="byline"> <span class="author vcard"><a class="url fn n" href="https://smpn2bawen.sch.id/index.php/author/pk9yjvmhtd/">Admin</a></span></span> </div><!-- .entry-meta -->
</header><!-- .entry-header -->
<div class="entry-content">
<p>Membuat kelas Langkah 1 : Pilih menu Kelas. Langkah 2 : Klik tombol Buat Kelas. Langkah 3 : Isi Nama Kelas, Tingkat Kelas (Kelas 1 – Kelas 12), dan Mata Pelajaran. Lalu, tekan tombol Simpan. Kelas yang berhasil ditambahkan akan muncul dan dapat dilihat di menu <a class="read-more" href="https://smpn2bawen.sch.id/index.php/2021/01/08/panduan-membuat-kelas-di-aplikasi-zeneus-dan-mengimport-kelas-dari-google-classroom-ke-aplikasi-zeneus/">Read More …</a></p>
</div><!-- .entry-content -->
<footer class="entry-footer">
<span class="comments-link"><a href="https://smpn2bawen.sch.id/index.php/2021/01/08/panduan-membuat-kelas-di-aplikasi-zeneus-dan-mengimport-kelas-dari-google-classroom-ke-aplikasi-zeneus/#respond">Leave a comment</a></span> </footer><!-- .entry-footer -->
</article><!-- #post-## -->
<article class="post-499 post type-post status-publish format-standard hentry category-uncategorized" id="post-499">
<header class="entry-header">
<h2 class="entry-title"><a href="https://smpn2bawen.sch.id/index.php/2021/01/07/keunggulan-aplikasi-zeneus-dibandingkan-aplikasi-pembelajaran-lain/" rel="bookmark">Keunggulan Aplikasi Zeneus dibandingkan Aplikasi Pembelajaran Lain</a></h2>
<div class="entry-meta">
<span class="posted-on"><a href="https://smpn2bawen.sch.id/index.php/2021/01/07/keunggulan-aplikasi-zeneus-dibandingkan-aplikasi-pembelajaran-lain/" rel="bookmark"><time class="entry-date published" datetime="2021-01-07T02:27:02+00:00">Januari 7, 2021</time><time class="updated" datetime="2021-01-07T02:27:04+00:00">Januari 7, 2021</time></a></span><span class="byline"> <span class="author vcard"><a class="url fn n" href="https://smpn2bawen.sch.id/index.php/author/pk9yjvmhtd/">Admin</a></span></span> </div><!-- .entry-meta -->
</header><!-- .entry-header -->
<div class="entry-content">
<p>Penyedia layanan pendidikan Zenius Education meluncurkan Zenius App sebagai aplikasi mobile untuk belajar bagi siswa secara daring (online). Peluncuran aplikasi ini sebagai tindak lanjut keseriusan Zenius menghadirkan materi belajar berkualitas yang selama ini diberikan melalui situs web zenius.net. Tujuannya, membuat <a class="read-more" href="https://smpn2bawen.sch.id/index.php/2021/01/07/keunggulan-aplikasi-zeneus-dibandingkan-aplikasi-pembelajaran-lain/">Read More …</a></p>
</div><!-- .entry-content -->
<footer class="entry-footer">
<span class="comments-link"><a href="https://smpn2bawen.sch.id/index.php/2021/01/07/keunggulan-aplikasi-zeneus-dibandingkan-aplikasi-pembelajaran-lain/#respond">Leave a comment</a></span> </footer><!-- .entry-footer -->
</article><!-- #post-## -->
<article class="post-486 post type-post status-publish format-standard hentry category-uncategorized" id="post-486">
<header class="entry-header">
<h2 class="entry-title"><a href="https://smpn2bawen.sch.id/index.php/2021/01/07/mengenal-zenius-aplikasi-pembelajaran-online-bisa-diakses-offline/" rel="bookmark">Mengenal Zenius Aplikasi Pembelajaran Online, Bisa Diakses Offline</a></h2>
<div class="entry-meta">
<span class="posted-on"><a href="https://smpn2bawen.sch.id/index.php/2021/01/07/mengenal-zenius-aplikasi-pembelajaran-online-bisa-diakses-offline/" rel="bookmark"><time class="entry-date published" datetime="2021-01-07T01:56:14+00:00">Januari 7, 2021</time><time class="updated" datetime="2021-01-07T01:56:16+00:00">Januari 7, 2021</time></a></span><span class="byline"> <span class="author vcard"><a class="url fn n" href="https://smpn2bawen.sch.id/index.php/author/pk9yjvmhtd/">Admin</a></span></span> </div><!-- .entry-meta -->
</header><!-- .entry-header -->
<div class="entry-content">
<p>Aplikasi belajar online sedang marak. Para pembuatnya mencoba memanfaatkan masifnya penggunaan gawai di kalangan pelajar. Mereka menawarkan aneka program untuk membantu belajar. Para penyedia aplikasi belajar online itu antara lain Quipper, Zenius Education, Ruang Guru, Prime Mobile, dan banyak lainnya. <a class="read-more" href="https://smpn2bawen.sch.id/index.php/2021/01/07/mengenal-zenius-aplikasi-pembelajaran-online-bisa-diakses-offline/">Read More …</a></p>
</div><!-- .entry-content -->
<footer class="entry-footer">
<span class="comments-link"><a href="https://smpn2bawen.sch.id/index.php/2021/01/07/mengenal-zenius-aplikasi-pembelajaran-online-bisa-diakses-offline/#respond">Leave a comment</a></span> </footer><!-- .entry-footer -->
</article><!-- #post-## -->
<article class="post-483 post type-post status-publish format-standard hentry category-uncategorized" id="post-483">
<header class="entry-header">
<h2 class="entry-title"><a href="https://smpn2bawen.sch.id/index.php/2021/01/06/mpls-masa-pengenalan-lingkungan-sekolah/" rel="bookmark">MPLS (Masa Pengenalan Lingkungan Sekolah)</a></h2>
<div class="entry-meta">
<span class="posted-on"><a href="https://smpn2bawen.sch.id/index.php/2021/01/06/mpls-masa-pengenalan-lingkungan-sekolah/" rel="bookmark"><time class="entry-date published" datetime="2021-01-06T04:05:46+00:00">Januari 6, 2021</time><time class="updated" datetime="2021-01-06T04:05:48+00:00">Januari 6, 2021</time></a></span><span class="byline"> <span class="author vcard"><a class="url fn n" href="https://smpn2bawen.sch.id/index.php/author/pk9yjvmhtd/">Admin</a></span></span> </div><!-- .entry-meta -->
</header><!-- .entry-header -->
<div class="entry-content">
<p>Saat memulai tahun ajaran baru, ada waktu pengenalan lingkungan sekolah atau yang dikenal Masa Orientasi Siswa atau MOS. Namun, tahun ajaran baru 2020/2021 yang akan dimulai Senin, 13 Juli 2020 masih diselimuti pandemi Covid-19. Maka dari itu, dalam memasuki tahun <a class="read-more" href="https://smpn2bawen.sch.id/index.php/2021/01/06/mpls-masa-pengenalan-lingkungan-sekolah/">Read More …</a></p>
</div><!-- .entry-content -->
<footer class="entry-footer">
<span class="comments-link"><a href="https://smpn2bawen.sch.id/index.php/2021/01/06/mpls-masa-pengenalan-lingkungan-sekolah/#respond">Leave a comment</a></span> </footer><!-- .entry-footer -->
</article><!-- #post-## -->
<article class="post-475 post type-post status-publish format-standard hentry category-uncategorized" id="post-475">
<header class="entry-header">
<h2 class="entry-title"><a href="https://smpn2bawen.sch.id/index.php/2021/01/05/e-rapor-yang-terintegrasi-dengan-data-dapodik/" rel="bookmark">E-Rapor yang terintegrasi dengan data Dapodik</a></h2>
<div class="entry-meta">
<span class="posted-on"><a href="https://smpn2bawen.sch.id/index.php/2021/01/05/e-rapor-yang-terintegrasi-dengan-data-dapodik/" rel="bookmark"><time class="entry-date published" datetime="2021-01-05T03:30:11+00:00">Januari 5, 2021</time><time class="updated" datetime="2021-01-05T05:36:58+00:00">Januari 5, 2021</time></a></span><span class="byline"> <span class="author vcard"><a class="url fn n" href="https://smpn2bawen.sch.id/index.php/author/pk9yjvmhtd/">Admin</a></span></span> </div><!-- .entry-meta -->
</header><!-- .entry-header -->
<div class="entry-content">
<p>Dalam perkembangan dunia teknologi sat ini, berbagai macam aplikasi dan pembelajaran sudah merambah ke kegiatan daring, hal ini supaya publik atau masyarakat dapat mengakses maupun menjalankan informasi secara Online. Salah satu pengolahan data yang mendasar di satuan pendidikan adalah Dapodik, <a class="read-more" href="https://smpn2bawen.sch.id/index.php/2021/01/05/e-rapor-yang-terintegrasi-dengan-data-dapodik/">Read More …</a></p>
</div><!-- .entry-content -->
<footer class="entry-footer">
<span class="comments-link"><a href="https://smpn2bawen.sch.id/index.php/2021/01/05/e-rapor-yang-terintegrasi-dengan-data-dapodik/#respond">Leave a comment</a></span> </footer><!-- .entry-footer -->
</article><!-- #post-## -->
</main><!-- #main -->
</div><!-- #primary -->
<div class="widget-area" id="sidebar-primary" role="complementary">
<aside class="widget widget_nav_menu" id="nav_menu-3"><h2 class="widget-title">MENU</h2><div class="menu-primary-menu-container"><ul class="menu" id="menu-primary-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-425" id="menu-item-425"><a href="https://smpn2bawen.sch.id/index.php/kegiatan/">KEGIATAN</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-426" id="menu-item-426"><a href="https://smpn2bawen.sch.id/index.php/prestasi/">PRESTASI</a></li>
</ul></div></aside><aside class="widget widget_media_gallery" id="media_gallery-10"><h2 class="widget-title">KEPALA SEKOLAH</h2><div class="gallery galleryid-475 gallery-columns-1 gallery-size-thumbnail" id="gallery-1"><figure class="gallery-item">
<div class="gallery-icon portrait">
<a href="https://smpn2bawen.sch.id/34/"><img alt="" aria-describedby="gallery-1-452" class="attachment-thumbnail size-thumbnail" height="150" loading="lazy" src="https://smpn2bawen.sch.id/wp-content/uploads/2020/12/34-150x150.jpg" width="150"/></a>
</div>
<figcaption class="wp-caption-text gallery-caption" id="gallery-1-452">
				Heri Kristantoro, S.Pd, M.Pd.
				</figcaption></figure>
</div>
</aside><aside class="widget widget_calendar" id="calendar-29"><div class="calendar_wrap" id="calendar_wrap"><table class="wp-calendar-table" id="wp-calendar">
<caption>Januari 2021</caption>
<thead>
<tr>
<th scope="col" title="Senin">S</th>
<th scope="col" title="Selasa">S</th>
<th scope="col" title="Rabu">R</th>
<th scope="col" title="Kamis">K</th>
<th scope="col" title="Jumat">J</th>
<th scope="col" title="Sabtu">S</th>
<th scope="col" title="Minggu">M</th>
</tr>
</thead>
<tbody>
<tr>
<td class="pad" colspan="4"> </td><td>1</td><td>2</td><td>3</td>
</tr>
<tr>
<td>4</td><td><a aria-label="Pos diterbitkan pada 5 January 2021" href="https://smpn2bawen.sch.id/index.php/2021/01/05/">5</a></td><td><a aria-label="Pos diterbitkan pada 6 January 2021" href="https://smpn2bawen.sch.id/index.php/2021/01/06/">6</a></td><td><a aria-label="Pos diterbitkan pada 7 January 2021" href="https://smpn2bawen.sch.id/index.php/2021/01/07/">7</a></td><td><a aria-label="Pos diterbitkan pada 8 January 2021" href="https://smpn2bawen.sch.id/index.php/2021/01/08/">8</a></td><td><a aria-label="Pos diterbitkan pada 9 January 2021" href="https://smpn2bawen.sch.id/index.php/2021/01/09/">9</a></td><td>10</td>
</tr>
<tr>
<td>11</td><td id="today"><a aria-label="Pos diterbitkan pada 12 January 2021" href="https://smpn2bawen.sch.id/index.php/2021/01/12/">12</a></td><td>13</td><td>14</td><td>15</td><td>16</td><td>17</td>
</tr>
<tr>
<td>18</td><td>19</td><td>20</td><td>21</td><td>22</td><td>23</td><td>24</td>
</tr>
<tr>
<td>25</td><td>26</td><td>27</td><td>28</td><td>29</td><td>30</td><td>31</td>
</tr>
</tbody>
</table><nav aria-label="Bulan sebelumnya dan selanjutnya" class="wp-calendar-nav">
<span class="wp-calendar-nav-prev"> </span>
<span class="pad"> </span>
<span class="wp-calendar-nav-next"> </span>
</nav></div></aside><aside class="widget widget_text" id="text-7"><h2 class="widget-title">SMPN2 BAWEN</h2> <div class="textwidget"></div>
</aside> </div><!-- #sidebar-primary -->
</div><!-- .inner-wrapper --></div><!-- .container --></div><!-- #content -->
<footer class="site-footer" id="colophon" role="contentinfo"><div class="container">
<div class="copyright">
	        Copyright. All rights reserved.	      </div><!-- .copyright -->
<div class="site-info">
<a href="https://wordpress.org/">Proudly powered by WordPress</a>
<span class="sep"> | </span>
			Education Hub by <a href="https://wenthemes.com/" rel="designer" target="_blank">WEN Themes</a> </div><!-- .site-info -->
</div><!-- .container --></footer><!-- #colophon -->
</div><!-- #page --><a class="scrollup" href="#page" id="btn-scrollup"><i class="fa fa-chevron-up"></i></a>
<script id="education-hub-skip-link-focus-fix-js" src="https://smpn2bawen.sch.id/wp-content/themes/education-hub/js/skip-link-focus-fix.min.js?ver=20130115" type="text/javascript"></script>
<script id="cycle2-js" src="https://smpn2bawen.sch.id/wp-content/themes/education-hub/third-party/cycle2/js/jquery.cycle2.min.js?ver=2.1.6" type="text/javascript"></script>
<script id="education-hub-custom-js" src="https://smpn2bawen.sch.id/wp-content/themes/education-hub/js/custom.min.js?ver=1.0" type="text/javascript"></script>
<script id="education-hub-navigation-js-extra" type="text/javascript">
/* <![CDATA[ */
var EducationHubScreenReaderText = {"expand":"<span class=\"screen-reader-text\">expand child menu<\/span>","collapse":"<span class=\"screen-reader-text\">collapse child menu<\/span>"};
/* ]]> */
</script>
<script id="education-hub-navigation-js" src="https://smpn2bawen.sch.id/wp-content/themes/education-hub/js/navigation.min.js?ver=20120206" type="text/javascript"></script>
<script id="wp-embed-js" src="https://smpn2bawen.sch.id/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
</body>
</html>
