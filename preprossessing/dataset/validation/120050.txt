<!DOCTYPE html>
<html class="no-js" lang="pt-BR">
<head>
<meta charset="utf-8"/>
<meta content="pt-BR" http-equiv="content-language"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="#000000" name="theme-color"/>
<link href="https://09a2b8de54eab32bbb9c-25627fe6dff48d6c3f108bb9a2eb5fbd.ssl.cf1.rackcdn.com/Favicon/fotografo-de-casamento-ana-f1556744257.png" rel="apple-touch-icon"/>
<link href="https://09a2b8de54eab32bbb9c-25627fe6dff48d6c3f108bb9a2eb5fbd.ssl.cf1.rackcdn.com/Favicon/fotografo-de-casamento-ana-f1556744257.png" rel="icon"/>
<meta content="https://www.anafabrin.com.br/" name="base_url"/>
<link href="https://www.anafabrin.com.br/office/remote.php%09%0A" hreflang="pt" rel="alternate"/>
<title>Página não Encontrada | Fotógrafo de Casamento, 15 Anos, Gestante, Newborn , Maringa, Ana Fabrin</title>
<meta content="NO-CACHE" http-equiv="CACHE-CONTROL"/>
<meta content="Mon, 28 AUG 2016 11:12:01 GMT" http-equiv="expires"/>
<meta content="Fotógrafo de Casamento, 15 Anos, Gestante, Newborn, Maringá - Pr, Ana Fabrin - Página não Encontrada" name="description"/>
<meta content="Fotografo em Maringa - Pr, Fotografo em Apucarana - Pr, Fotografo em Londrina - Pr, Fotografo de Casamento, Fotografo de 15 anos, Fotografo de Newborn, Fotografo de Familia, Fotografo de Gestante, Casamento em Maringa, casamento Maringa, Casamento" name="keywords"/>
<meta content="Ana Fabrin" name="author"/>
<meta content="noindex,nofollow" name="ROBOTS"/>
<meta content="981719288608892" property="fb:app_id"/>
<meta content="Fotógrafo de Casamento, 15 Anos, Gestante, Newborn , Maringa, Ana Fabrin" property="og:site_name"/>
<meta content="Fotógrafo de Casamento, 15 Anos, Gestante, Newborn , Maringa, Ana Fabrin" property="og:title"/>
<meta content="Fotógrafo de Casamento, 15 Anos, Gestante, Newborn, Maringá - Pr, Ana Fabrin" property="og:description"/>
<meta content="https://09a2b8de54eab32bbb9c-25627fe6dff48d6c3f108bb9a2eb5fbd.ssl.cf1.rackcdn.com/LogoMarca/fotografo-de-casamento-ana-f1556744243.png" property="og:image"/>
<meta content="https://www.anafabrin.com.br/office/remote.php%09%0A" property="og:url"/>
<meta content="website" property="og:type"/>
<meta content="750" property="og:image:width"/>
<meta content="500" property="og:image:height"/>
<meta content="pt_br" property="og:locale"/>
<link href="https://www.anafabrin.com.br/assets/siteone//build/css/css.min.css?v=0.60.15" rel="stylesheet"/><link href="https://www.anafabrin.com.br/assets/siteone/css/titulo_novo.css?version=2.7" rel="stylesheet"/><link href="https://www.anafabrin.com.br/css/01e04033f2cea65db059d248dbd588db_51345.css?version=0.0.1" rel="stylesheet"/><style type="text/css">
    .max-1200{
        max-width: 1200px; margin: 0 auto !important;
    }
    </style></head>
<body class=" tam-fullwidth borda-none buttons-social-fixed-bottom ">
<!-- loader --><!-- <div id="loader_bloco"></div> --><div class="site corSite site-menu1 site-menu-medio" id="site"><nav><style>
	.custom-color {
		border-bottom-color: #ffffff; 
		border-color: #ffffff;
    	color: #ffffff;
	}
	
</style><div class="menu_modal menuSemTransparencia"><div class="menu_close corTxtMenu corDestaque"><i class="material-icons">clear</i></div><ul class="menusite"><!-- INICIO: AJUSTES ESPECIFICOS DO MENU 12 --><!-- adiciona o menu de busca, logo (favicon no lugar do logo) e reposiciona o botao fechar --><!-- FIM: AJUSTES ESPECIFICOS DO MENU 12 --><li class="custom-nav-Home"><a class="corTxtMenu fonte_1 corDestaque " href="/">
						Home
					</a></li><li class="custom-nav-Ana Fabrin"><a class="corTxtMenu fonte_1 corDestaque " href="/ana-fabrin">
						Ana Fabrin
					</a></li><li class="custom-nav-TRABALHOS"><a class="corTxtMenu fonte_1 corDestaque " href="/trabalhos">
						TRABALHOS
					</a></li><li class="custom-nav-FILMES"><a class="corTxtMenu fonte_1 corDestaque " href="/filmes">
						FILMES
					</a></li><li class="custom-nav-DEPOIMENTOS"><a class="corTxtMenu fonte_1 corDestaque " href="/depoimentos">
						DEPOIMENTOS
					</a></li><li class="custom-nav-CONTATO"><a class="corTxtMenu fonte_1 corDestaque " href="/contato">
						CONTATO
					</a></li></ul></div><div class="busca_modal menuSemTransparencia modal-fundo"><div class="busca_close corTxtMenu corDestaque busca-icon custom-color"><i class="material-icons">clear</i></div><form action="/busca" class="formbuscaresult" method="get"><input class="fonte_2 corTxtMenu corDestaque custom-color" name="busca" placeholder="Buscar... " type="text"/><button class="btn-close corTxtMenu corDestaque custom-color" type="submit"><i aria-hidden="true" class="fa fa-search"></i></button></form></div><!-- <div class="busca_modal menuSemTransparencia"><div class="busca_close corTxtMenu corDestaque busca-icon"><i class="material-icons">clear</i></div><form action="/busca" method="get" class="formbuscaresult"><input type="text" class="fonte_2 corTxtMenu" name="busca" placeholder="Buscar... " style="border-color:#ffffff;"><button type="submit" class="btn-close corTxtMenu custom-color" style="border-color:#ffffff;"><i class="fa fa-search" aria-hidden="true"></i></button></form></div> --><link href="https://www.anafabrin.com.br/assets/siteone/css/menu1.css?version='2.4'" rel="stylesheet"/><style type="text/css">
              .capa, .ColPaddingTop{*padding-top: 90px !important;}
      .capa{*padding-bottom: 90px !important }
        @media only screen and (max-width: 751px){
    .site, .capa, .ColPaddingTop{
      padding-top: 0px !important;
    }
  }
</style><header class="container-fluid navigation-header corFundoMenu menu1 menu-medio menu-absolute" data-menu="1" id="headerprinc"><div class="row"><div class="container"><div class="brand_logo" id="div_logo">
          <a class="linkLogo " data-src="/" href="/" title="Ir para a Home de Fotógrafo de Casamento, 15 Anos, Gestante, Newborn , Maringa, Ana Fabrin"><img alt="Logo Fotógrafo de Casamento, 15 Anos, Gestante, Newborn , Maringa, Ana Fabrin" class="tamanhoLogo" src="https://09a2b8de54eab32bbb9c-25627fe6dff48d6c3f108bb9a2eb5fbd.ssl.cf1.rackcdn.com/LogoMarca/fotografo-de-casamento-ana-f1556744243.png" title="Logo Fotógrafo de Casamento, 15 Anos, Gestante, Newborn , Maringa, Ana Fabrin"/></a></div><ul class="menusite"><li class="espaco-menu-nenhum"><a class="corTxtMenu fonte_1 corDestaque " href="/" title="Home Fotógrafo de Casamento, 15 Anos, Gestante, Newborn , Maringa, Ana Fabrin">
                    Home
                  </a></li><li class="espaco-menu-nenhum"><a class="corTxtMenu fonte_1 corDestaque " href="/ana-fabrin" title="Ana Fabrin Fotógrafo de Casamento, 15 Anos, Gestante, Newborn , Maringa, Ana Fabrin">
                    Ana Fabrin
                  </a></li><li class="espaco-menu-nenhum"><a class="corTxtMenu fonte_1 corDestaque " href="/trabalhos" title="TRABALHOS Fotógrafo de Casamento, 15 Anos, Gestante, Newborn , Maringa, Ana Fabrin">
                    TRABALHOS
                  </a><ul class="hover-menu corSite"><span class="corSite"></span><li><a class="fonte_2 corTxt" href="https://www.anafabrin.com.br/trabalhos/categoria/15-anos">15 Anos</a></li><li><a class="fonte_2 corTxt" href="https://www.anafabrin.com.br/trabalhos/categoria/gestante">Gestante</a></li><li><a class="fonte_2 corTxt" href="https://www.anafabrin.com.br/trabalhos/categoria/new-born">New Born</a></li><li><a class="fonte_2 corTxt" href="https://www.anafabrin.com.br/trabalhos/categoria/pre-wedding">Pré Wedding</a></li><li><a class="fonte_2 corTxt" href="https://www.anafabrin.com.br/trabalhos/categoria/wedding">Wedding</a></li></ul></li><li class="espaco-menu-nenhum"><a class="corTxtMenu fonte_1 corDestaque " href="/filmes" title="FILMES Fotógrafo de Casamento, 15 Anos, Gestante, Newborn , Maringa, Ana Fabrin">
                    FILMES
                  </a></li><li class="espaco-menu-nenhum"><a class="corTxtMenu fonte_1 corDestaque " href="/depoimentos" title="DEPOIMENTOS Fotógrafo de Casamento, 15 Anos, Gestante, Newborn , Maringa, Ana Fabrin">
                    DEPOIMENTOS
                  </a></li><li class="espaco-menu-nenhum"><a class="corTxtMenu fonte_1 corDestaque " href="/contato" title="CONTATO Fotógrafo de Casamento, 15 Anos, Gestante, Newborn , Maringa, Ana Fabrin">
                    CONTATO
                  </a></li></ul><!--</nav>--></div></div></header><!-- Menu Mobile --><header class="container-fluid navigation-header corFundoMenu menu1 menu-medio main_header-clone main_header-stick" data-menu="1"><div class="row"><div class="container"><div class="brand_logo" id="div_logo"><a class="linkLogo " data-src="/" href="/"><img alt="Fotógrafo de Casamento, 15 Anos, Gestante, Newborn , Maringa, Ana Fabrin" class="tamanhoLogo" src="https://09a2b8de54eab32bbb9c-25627fe6dff48d6c3f108bb9a2eb5fbd.ssl.cf1.rackcdn.com/LogoMarca/fotografo-de-casamento-ana-f1556744243.png" title="Fotógrafo de Casamento, 15 Anos, Gestante, Newborn , Maringa, Ana Fabrin"/></a></div><!--<nav id="nav_menu">--><ul class="menusite"><li><a class="corTxtMenu fonte_1 corDestaque " href="/">
                    Home
                  </a></li><li><a class="corTxtMenu fonte_1 corDestaque " href="/ana-fabrin">
                    Ana Fabrin
                  </a></li><li><a class="corTxtMenu fonte_1 corDestaque " href="/trabalhos">
                    TRABALHOS
                  </a><ul class="hover-menu corSite"><span class="corSite"></span><li><a class="fonte_2 corTxt" href="https://www.anafabrin.com.br/trabalhos/categoria/15-anos">15 Anos</a></li><li><a class="fonte_2 corTxt" href="https://www.anafabrin.com.br/trabalhos/categoria/gestante">Gestante</a></li><li><a class="fonte_2 corTxt" href="https://www.anafabrin.com.br/trabalhos/categoria/new-born">New Born</a></li><li><a class="fonte_2 corTxt" href="https://www.anafabrin.com.br/trabalhos/categoria/pre-wedding">Pré Wedding</a></li><li><a class="fonte_2 corTxt" href="https://www.anafabrin.com.br/trabalhos/categoria/wedding">Wedding</a></li></ul></li><li><a class="corTxtMenu fonte_1 corDestaque " href="/filmes">
                    FILMES
                  </a></li><li><a class="corTxtMenu fonte_1 corDestaque " href="/depoimentos">
                    DEPOIMENTOS
                  </a></li><li><a class="corTxtMenu fonte_1 corDestaque " href="/contato">
                    CONTATO
                  </a></li></ul><!--</nav>--></div></div></header></nav><!-- INÍCIO Solicitação de orçamento --><div class="corSite" id="orcamento-chat"><!-- SOMENTE WHATSAPP --><a class="whatsapp-default" data-whatsapp="" data-whatsapp-number="5544999182428" data-whatsapp-text="Olá, visualizei o seu site e gostaria de entrar em contato!" onclick="acesso_contato('C_WHATSAPP')" rel="noreferrer" style=" background: #4ab83b " target="_blank"><i class="fa fa-whatsapp"></i><span class="trn">Vamos Conversar</span></a><input id="ativadoMessenger" name="ativadoMessenger" type="hidden" value=""/></div><!-- FIM Solicitação de orçamento -->
<main>
<div id="main">
<div class="container-fluid">
<div class="clear mg-30"></div>
<div class="row">
<div class="col-sm-12 text-center">
<section class="titulosessao titulo">
<h2 class="title corSec2 fonte_1 corTitulo estilo0">
                    OPS!<br/>
<span class="trn">Página não Encontrada</span>
<br/><br/>...</h2>
</section>
</div>
</div>
</div>
</div>
</main>
<footer class="footer pb-0 "><div class="fullwidth content-footer"><link href="https://www.anafabrin.com.br/assets/siteone/css/rodape10.css?version=2.6" rel="stylesheet"/><style type="text/css"></style><!-- INICIO: RODAPE 10 --><div class="container-fluid no-padding"><div class="footer10 corFundoRodape fonte_2 " id="bloco_rodape"><div class="max-1200 text-center"><div class="clear mg-60"></div><h2 class="title corSec2 fonte_1 corTitulo estilo0 text-center trn">Últimos posts do Instagram</h2><div class="clear mg-60"></div><div class="container instaTitle style_full_width" style="background: #000;"><div class="col-sm-12 text-center no-padding instaPluginBloco" id="novo_insta"><!--
                         <div data-is
                            data-is-api="https://www.anafabrin.com.br/assets/siteone/js/instashow/api/index.php"
                            data-is-source="@anafabrinfotografia"
                            data-is-width="auto"
                            data-is-columns="5"
                            data-is-rows="3"
                            data-is-direction="vertical"
                            data-is-lang="pt-BR"
                            data-is-arrows-control="false" 
                            data-is-post-elements="likesCount, commentsCount"
                            data-is-responsive='{ "970": { "columns": 3, "rows": 2 }, "480": { "columns": 2, "rows": 2}}'
                            data-is-image-click-action="popup"
                            data-is-cache-time="300"
                        ></div>
                        --><div class="row" data-cols="6" data-instap="" data-rows="2" data-user="anafabrinfotografia"></div></div></div><div class="clear mg-30"></div></div><div class="container style_full_width"><div class="max-width"><div class="clear mg-15"></div></div></div><div class="container bottom style_full_width"><div class="col-xs-12 col-sm-12 text-center"><h1 id="footer-desc"></h1></div><div class="col-xs-12 col-sm-12 text-center footer-social-media"><!-- --><a class="linkRodape" href="https://facebook.com/AnaFabrinFotografia" rel="noreferrer" target="_blank" title="Facebook"><span class="fa fa-facebook tipoic2 footer-share corIcones corDestaque"></span></a><a class="linkRodape" href="https://instagram.com/anafabrinfotografia" rel="noreferrer" target="_blank" title="Instagram"><span class="fa fa-instagram tipoic2 footer-share corIcones corDestaque"></span></a><a class="linkRodape" href="https://vimeo.com/anafabrin" rel="noreferrer" target="_blank" title="Vimeo"><span class="fa fa-vimeo tipoic2 footer-share corIcones corDestaque"></span></a></div><div class="col-sm-12"><div class="copyright-novo"><a class="assinatura-epics" href="https://www.epics.com.br" target="_blank" title="Ir para EPICS - Soluções para fotógrafos e videomakers"><img class="ajuste-assinatura" src="https://www.anafabrin.com.br/assets/siteone//img/criado-por-EPICS.svg"/></a></div></div></div></div></div><!-- FIM: RODAPE 10 --><div class="col-xs-12 text-center" id="byEpicsSign"><!-- <div class="copyright font-texto corTxt text-center"><a href="https://www.epics.com.br" class="assinatura-epics" title="Ir para EPICS - Soluções para fotógrafos e videomakers" target="_blank">EPICS</a></div> --><div class="copyright-novo"><a class="assinatura-epics" href="https://www.epics.com.br" target="_blank" title="Ir para EPICS - Soluções para fotógrafos e videomakers"><img class="lazyload" data-src="https://www.anafabrin.com.br/assets/siteone//img/criado-por-EPICS.svg"/></a></div></div></div></footer><div class="master-loading"><div class="windows8"><div class="wBall" id="wBall_1"><div class="wInnerBall"></div></div><div class="wBall" id="wBall_2"><div class="wInnerBall"></div></div><div class="wBall" id="wBall_3"><div class="wInnerBall"></div></div><div class="wBall" id="wBall_4"><div class="wInnerBall"></div></div><div class="wBall" id="wBall_5"><div class="wInnerBall"></div></div></div></div><script type="text/javascript">

            function createElementAssync(element, url){
                var css = document.createElement('link');
                css.href = url;
                css.rel = 'stylesheet';
                css.type = 'text/css';
                document.getElementsByTagName('head')[0].appendChild(css);
            }
            
            var JSD_ALTURASLIDE     = '100';
            var JSD_TEMPOTRANSICAO  = '4000';
            var JSD_CODIGOHEAD      = '';
            var JSD_CODIGO_BODY     = '';
            var JSD_ASSETS_URL      = 'https://www.anafabrin.com.br/assets/siteone/';
            var JSD_DETALHESITE     = '';
            var JSD_VIEWTYPE        = 'PAGINANAOENCONTRADA404';
            var JSD_RODAPEHOME      = '1';
            var JSD_OPCAOBUSCA      = '';
            var JSD_STARTAJAX       = '0';
            var JSD_MENU            = 'menu1';
            var JSD_TITULOFOTO      = '1';
            var JSD_ESTILOTITULO    = 'estilo0';
            var JSD_PAINELURL       = 'https://epics.site/';
            var JSD_ACESSOSITE      = '5590';
            var JSD_HOSTURL         = '';
            var JSD_IDSITE          = '5590';
            var JSD_ACESSOTIPO      = '1';
            var JSD_ACESSOIDALVO    = 'p404';
            var JSD_ACESSOUNIQUE    = '1';
            var JSD_FACEID          = '';
            var JSD_BOTAODIREITO    = '0';
            var JSD_CODIGOBODY      = '';
            var JSD_CORDESTAQUE     = '#dbdbdb';
        </script><script src="https://www.anafabrin.com.br/assets/siteone/build/js/app.min.js?v=0.60.15" type="text/javascript"></script><script>
                $(window).load(function(){
                     !function(f,b,e,v,n,t,s)
                    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                    n.queue=[];t=b.createElement(e);t.async=!0;
                    t.src=v;s=b.getElementsByTagName(e)[0];
                    s.parentNode.insertBefore(t,s)}(window, document,'script',
                    'https://connect.facebook.net/pt_BR/fbevents.js');
                    fbq('init', '684780539038221');
                    fbq('track', 'PageView');
                });
            </script><noscript><img height="1" src="https://www.facebook.com/tr?id=684780539038221&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript><script>
                $( document ).ready(function() {
                    acesso_contato('PAGINANAOENCONTRADA404', 0);
                });
            </script><script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-162347570-1"></script><script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());

				gtag('config', 'UA-162347570-1');
			</script><script>
            $(window).on('load', function(){
                setTimeout(function(){ 
                    $.ajaxSetup({ cache: true });
                    $.getScript('https://connect.facebook.net/pt_BR/sdk.js', function(){
                        FB.init({
                            appId: '981719288608892',
                            autoLogAppEvents : true,
                            xfbml            : true,
                            version: 'v7.0' // or v2.1, v2.2, v2.3, ...
                        });     
                    });
                }, 3000);
            });
        </script><style type="text/css">
	.contato-form .input,
	.input{
		padding: 10px;
		font-size: 12px;
		border-width: 1px;
		border-style: solid;
		border-color: #b1b1b1;
		color: #b1b1b1;
		width: 100%;
		font-family: inherit;
		background-color: #fff;
	}

	.contato-form select,
	select{
		width: 100%;
		background-color: #fff;
		color: #b1b1b1;
		font-size: 11px;
		padding-bottom: 11px;
		float: left;
		height: 45px;
		border-radius: 0px;
		-moz-border-radius: 0px;
		-webkit-border-radius: 0px;
		-webkit-appearance: none;
		-webkit-border-radius: 0px;
	}
	.contato-form button,
	.select-form button,
	button{width: 100%;border:0px;padding: 17px 25px;font-size: 12px;display: block;}

	.contatoTxtCenter{
		text-align: justify;
		padding-bottom: 30px;
		display: block;
	}

	.campo {
		margin-bottom: 30px;
	}

	select {
		color: #aaa !important;
	}

	::-webkit-input-placeholder {
		color: #aaa;
		opacity: 1;
	}

	:-moz-placeholder { /* Firefox 18- */
		color: #aaa;
		opacity: 1;
	}

	::-moz-placeholder {  /* Firefox 19+ */
		color: #aaa; 
		opacity: 1;
	}

	:-ms-input-placeholder {  
		color: #aaa; 
		opacity: 1;
	}

</style><script>
        // oculta e mostra o texto "entrar em contato" quando ocorre o scroll da tela.
        $(window).scroll(function() {
            if ($(this).scrollTop() > 0) {
                $('.texto-botao-contato').fadeOut();
                $('.btn-chat-reduzido').removeClass('active');
            } else {
                $('.texto-botao-contato').fadeIn();
            }
        });
        </script><script type="text/javascript">
        $(window).on('load', function(){
            var googlefont          = document.createElement('link');
                googlefont.href     = 'https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans|Material+Icons&display=swap';
                googlefont.rel      = 'stylesheet';
                googlefont.type     = 'text/css';
            document.getElementsByTagName('head')[0].appendChild(googlefont);            
        });
        </script><script src="https://oportunidades-api.epics.com.br/webhook_opportunity.js?v=1.2"></script>
</div></body>
</html>