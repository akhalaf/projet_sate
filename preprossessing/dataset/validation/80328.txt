<html>
<head profile="http://gmpg.org/xfn/11">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Aedxs</title>
<link href="css/style.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="css/print.css" media="print" rel="stylesheet" type="text/css"/>
<script src="js/jquery-1.6.1.min.js" type="text/javascript"></script>
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/Myriad_Pro_400.font.js" type="text/javascript"></script>
<script src="js/validation.js" type="text/javascript"></script>
<!--- MULTI UPLOAD -->
<script src="js/multi_upload.js" type="text/javascript"></script>
<link href="uploadify/uploadify.css" rel="stylesheet" type="text/css"/>
<script src="uploadify/swfobject.js" type="text/javascript"></script>
<script src="uploadify/jquery.uploadify.v2.1.4.min.js" type="text/javascript"></script>
<script type="text/javascript">
	Cufon.replace('.list1'); // Works without a selector engine
</script>
</head>
<body>
<div id="container">
<!--header-->
<div id="header">
<div id="header_cont">
<div id="logo"> <img src="images/logo.png"/> </div>
<div id="headr_right"> <img src="images/top_cont.png"/> </div>
</div>
</div>
<!--NAVIGATION-->
<div id="nav">
<div class="menu-header_nav-container">
<ul class="menu" id="menu-header_nav">
<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-46 current_page_item menu-item-102" id="menu-item-102"><a href="http://www.aedelectronics.com/" target="_blank">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-110" id="menu-item-110"><a href="http://www.aedelectronics.com/what-we-do/" target="_blank">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-108" id="menu-item-108"><a href="http://www.aedelectronics.com/quality-assurance/" target="_blank">Quality</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-109" id="menu-item-109"><a href="http://www.aedelectronics.com/services/" target="_blank">Services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-106" id="menu-item-106"><a href="http://www.aedelectronics.com/inventory-solutions/" target="_blank">Inventory Solutions</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-107" id="menu-item-107"><a href="http://www.aedelectronics.com/line-card/" target="_blank">Line Card</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-103" id="menu-item-103"><a href="http://www.aedelectronics.com/careers/" target="_blank">Careers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-104" id="menu-item-104"><a href="http://www.aedelectronics.com/contact-us/" target="_blank">Contact</a></li>
</ul>
</div>
</div><div id="main_cont">
<div class="container">
<div id="inner_cont">
<img src="images/top_banner.png"/>
<div id="content">
<div class="post" id="post-46">
<div class="top_cureve"></div>
<div class="postentry">
<ul class="list1">
<li><b>Welcome to AEDXS.com</b></li>
<li>Thank you for visiting AED Inc's web based platform designed to provide answers specific to the challenges faced by OEMs and CEMs regarding their annual problems of mounting excess inventories of electronic components and peripherals.</li>
<li>You may be familiar with any number of the 5 programs offered by AED Inc, but we guarantee you that the terms and conditions are unmatched by any of our competitors. Our goal is to partner with you in realizing your excess inventory objectives forever. AED Inc works as partners, not adversaries, with our clients. </li>
<li>Please invest 15 minutes of your time now. AED Inc. assures you that we will provide you the best possible solution for all your Excess Inventory objectives</li>
<li>Please enter your user name and password as provided to you by your AED Account Representative. If you have not received a username and password, please call 1-800-700-1728 and ask for Liege Codd ext 143 to gain immediate entry.</li>
</ul>
</div>
<div class="bottom_cureve"></div>
</div>
<div id="shadow">
<div id="sidebar">
<ul>
<li class="widget widget_customizable_search" id="customizable-search">
<h2 class="widgettitle">client login</h2>
<form action="" id="partsearch" method="post" onsubmit="return LoginValidation();">
<input id="hdAction" name="hdAction" type="hidden" value=""/>
<div>
<div align="center" style="color:#ffffff; padding:0px 0px 10px 0px"><b>Sign In to AEDX</b></div>
</div>
<div class="submit_but">
<table border="0" cellpadding="2" cellspacing="2" width="100%">
<tr>
<td>
<span id="error_msg"></span>
<p>Login Name</p>
</td>
</tr>
<tr><td><input id="name" name="username" type="text" value=""/></td></tr>
<tr><td><p>Password</p></td></tr>
<tr><td><input id="password" name="password" type="password" value=""/></td></tr>
<tr height="5"><td></td></tr>
<tr><td align="center"><input class="btn" name="Submit" type="submit" value=""/></td></tr>
</table>
</div>
</form>
</li>
</ul>
</div>
</div>
</div>
</div>
<div id="top-footer"> <img class="logoimg" src="images/client_aed.png"/> <img class="logoimg" src="images/nqa.png"/> <img class="logoimg" src="images/anab.png"/> <img class="logoimg" src="images/iso.png"/> <img class="logoimg" src="images/gidep.png"/> <img class="logoimg" src="images/visa.png"/> </div>
</div>
</div>
<div id="footer">
<div id="footer_cont">
<p class="copy_right">Copyright © 2012 AED Inc. All rights reserved.</p>
<div id="footer_menu">
<div class="menu-footer_nav-container">
<ul class="menu" id="menu-footer_nav">
<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-46 current_page_item menu-item-95" id="menu-item-95"><a href="http://www.aedelectronics.com/" target="_blank">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-96" id="menu-item-96"><a href="http://www.aedelectronics.com/what-we-do/" target="_blank">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-97" id="menu-item-97"><a href="http://www.aedelectronics.com/quality-assurance/" target="_blank">Quality</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-98" id="menu-item-98"><a href="http://www.aedelectronics.com/services/" target="_blank">Services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-99" id="menu-item-99"><a href="http://www.aedelectronics.com/inventory-solutions/" target="_blank">Inventory Solutions</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-100" id="menu-item-100"><a href="http://www.aedelectronics.com/line-card/" target="_blank">Line Card</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-101" id="menu-item-101"><a href="http://www.aedelectronics.com/careers/" target="_blank">Careers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25" id="menu-item-25"><a href="http://www.aedelectronics.com/contact-us/" target="_blank">Contact</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</body>
</html>