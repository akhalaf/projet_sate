<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>Home</title>
<meta content="same-origin" name="referrer"/>
<meta content="ACPC Home Page" name="description"/>
<link href="http://www.pictouanglicans.ca/" rel="canonical"/>
<meta content="width=960" name="viewport"/>
<style>html,body{-webkit-text-zoom:reset !important;-ms-text-size-adjust:none !important;-moz-text-size-adjust:none !important;-webkit-text-size-adjust:none !important}@font-face{font-display:block;font-family:"EB Garamond";src:url('css/EBGaramond-Regular.woff2') format('woff2'),url('css/EBGaramond-Regular.woff') format('woff');font-weight:400}@font-face{font-display:block;font-family:"Lato";src:url('css/Lato-Regular.woff2') format('woff2'),url('css/Lato-Regular.woff') format('woff');font-weight:400}@font-face{font-display:block;font-family:"PT Sans";src:url('css/PT_Sans-Web-Bold.woff2') format('woff2'),url('css/PT_Sans-Web-Bold.woff') format('woff');font-weight:700}@font-face{font-display:block;font-family:"PT Sans";src:url('css/PT_Sans-Web-Regular.woff2') format('woff2'),url('css/PT_Sans-Web-Regular.woff') format('woff');font-weight:400}body>div{font-size:0}p, span,h1,h2,h3,h4,h5,h6{margin:0;word-spacing:normal;word-wrap:break-word;-ms-word-wrap:break-word;pointer-events:auto;max-height:1000000000px}sup{font-size:inherit;vertical-align:baseline;position:relative;top:-0.4em}sub{font-size:inherit;vertical-align:baseline;position:relative;top:0.4em}ul{display:block;word-spacing:normal;word-wrap:break-word;list-style-type:none;padding:0;margin:0;-moz-padding-start:0;-khtml-padding-start:0;-webkit-padding-start:0;-o-padding-start:0;-padding-start:0;-webkit-margin-before:0;-webkit-margin-after:0}li{display:block;white-space:normal}li p{-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;-o-user-select:none;user-select:none}form{display:inline-block}a{text-decoration:inherit;color:inherit;-webkit-tap-highlight-color:rgba(0,0,0,0)}textarea{resize:none}.shm-l{float:left;clear:left}.shm-r{float:right;clear:right}.whitespacefix{word-spacing:-1px}html{font-family:sans-serif}body{font-size:0;margin:0}audio,video{display:inline-block;vertical-align:baseline}audio:not([controls]){display:none;height:0}[hidden],template{display:none}a{background:0 0;outline:0}b,strong{font-weight:700}dfn{font-style:italic}h1,h2,h3,h4,h5,h6{font-size:1em;line-height:1;margin:0}img{border:0}svg:not(:root){overflow:hidden}button,input,optgroup,select,textarea{color:inherit;font:inherit;margin:0}button{overflow:visible}button,select{text-transform:none}button,html input[type=button],input[type=submit]{-webkit-appearance:button;cursor:pointer;box-sizing:border-box;white-space:normal}input[type=text],input[type=password],textarea{-webkit-appearance:none;appearance:none;box-sizing:border-box}button[disabled],html input[disabled]{cursor:default}button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0}input{line-height:normal}input[type=checkbox],input[type=radio]{box-sizing:border-box;padding:0}input[type=number]::-webkit-inner-spin-button,input[type=number]::-webkit-outer-spin-button{height:auto}input[type=search]{-webkit-appearance:textfield;-moz-box-sizing:content-box;-webkit-box-sizing:content-box;box-sizing:content-box}input[type=search]::-webkit-search-cancel-button,input[type=search]::-webkit-search-decoration{-webkit-appearance:none}textarea{overflow:auto;box-sizing:border-box;border-color:#ddd}optgroup{font-weight:700}table{border-collapse:collapse;border-spacing:0}td,th{padding:0}blockquote{margin-block-start:0;margin-block-end:0;margin-inline-start:0;margin-inline-end:0}:-webkit-full-screen-ancestor:not(iframe){-webkit-clip-path:initial!important}
html { -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; }.menu-content{cursor:pointer;position:relative}li{-webkit-tap-highlight-color:rgba(0,0,0,0)}
#b{background-color:#fff}.ps5{position:relative;margin-top:0}.v2{display:block;*display:block;zoom:1;vertical-align:top}.s5{pointer-events:none;min-width:960px;width:960px;margin-left:auto;margin-right:auto}.s6{width:960px;margin-left:0}.v3{display:inline-block;*display:inline;zoom:1;vertical-align:top}.ps6{position:relative;margin-left:0;margin-top:0}.s7{min-width:960px;width:960px;min-height:2819px}.ps7{position:relative;margin-left:0;margin-top:10px}.s8{min-width:275px;width:275px;min-height:1450px}.w1{line-height:0}.ps8{position:relative;margin-left:10px;margin-top:0}.s9{min-width:265px;width:265px;min-height:142px}.s10{min-width:93px;width:93px;min-height:100px;height:100px}.c6{z-index:2;pointer-events:auto}.a1{display:block}.i1{position:absolute;left:0;width:93px;height:95px;top:2px;border:0}.i2{width:100%;height:100%;display:inline-block;-webkit-transform:translate3d(0,0,0)}.v4{display:inline-block;*display:inline;zoom:1;vertical-align:top;overflow:hidden}.ps9{position:relative;margin-left:79px;margin-top:-12px}.s11{min-width:186px;width:186px;min-height:54px;height:54px}.c7{z-index:6;pointer-events:auto}.p2{padding-top:0;text-indent:0;padding-bottom:0;padding-right:0;text-align:left}.f10{font-family:"EB Garamond";font-size:15px;font-weight:400;font-style:normal;text-decoration:none;text-transform:none;color:#000;background-color:initial;line-height:27px;letter-spacing:normal;text-shadow:none}.v5{display:inline-block;*display:inline;zoom:1;vertical-align:top;overflow:visible}.ps10{position:relative;margin-left:0;margin-top:6px}.s12{min-width:275px;width:275px;min-height:1302px;height:1302px}.c8{z-index:5;pointer-events:auto}.v6{display:inline-block;*display:inline-block;zoom:1;vertical-align:top}.m1{padding:0px 0px 0px 0px}.ml1{outline:0}.s13{min-width:275px;width:275px;min-height:62px;height:62px;box-shadow:8px 15px 4px rgba(0,0,0,.40);-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px}.mcv1{display:inline-block}.s14{min-width:273px;width:273px;min-height:60px;height:60px}.c9{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.ps11{position:relative;margin-left:0;margin-top:20px}.s15{min-width:273px;width:273px;min-height:19px;height:19px}.c10{pointer-events:auto}.p3{padding-top:0;text-indent:0;padding-bottom:0;padding-right:0;text-align:center}.f11{font-family:Lato;font-size:12px;font-weight:400;font-style:normal;text-decoration:none;text-transform:none;color:#000;background-color:initial;line-height:16px;letter-spacing:normal;text-shadow:none}.c11{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c12{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c13{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c14{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c15{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c16{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c17{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c18{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c19{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c20{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c21{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c22{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c23{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c24{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c25{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c26{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c27{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c28{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c29{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.c30{pointer-events:none;border:1px solid #000;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;background-color:#ddedf7;background-clip:padding-box}.ps12{position:relative;margin-left:139px;margin-top:-1460px}.s16{min-width:758px;width:758px;min-height:2819px}.s17{min-width:758px;width:758px;min-height:2819px;height:2819px}.c31{z-index:1;pointer-events:auto}.f12{font-family:"EB Garamond";font-size:29px;font-weight:400;font-style:normal;text-decoration:underline;text-transform:none;color:#268bd2;background-color:initial;line-height:53px;letter-spacing:normal;text-shadow:none}.f13{font-family:"EB Garamond";font-size:23px;font-weight:400;font-style:normal;text-decoration:underline;text-transform:none;color:#268bd2;background-color:initial;line-height:42px;letter-spacing:normal;text-shadow:none}.f14{font-family:"EB Garamond";font-size:23px;font-weight:400;font-style:normal;text-decoration:none;text-transform:none;color:#268bd2;background-color:initial;line-height:42px;letter-spacing:normal;text-shadow:none}.f15{font-family:"EB Garamond";font-size:23px;font-weight:400;font-style:normal;text-decoration:none;text-transform:none;color:#000;background-color:initial;line-height:42px;letter-spacing:normal;text-shadow:none}.f16{font-family:"EB Garamond";font-size:23px;font-weight:400;font-style:normal;text-decoration:underline;text-transform:none;color:#000;background-color:initial;line-height:42px;letter-spacing:normal;text-shadow:none}.f17{font-family:"Helvetica Neue", sans-serif;font-size:12px;font-weight:700;font-style:normal;text-decoration:underline;text-transform:none;color:#268bd2;background-color:initial;line-height:20px;letter-spacing:normal;text-shadow:none}.f18{font-family:"EB Garamond";font-size:23px;font-weight:400;font-style:normal;text-decoration:line-through;text-transform:none;color:#000;background-color:initial;line-height:42px;letter-spacing:normal;text-shadow:none}.f19{font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;font-size:14px;font-weight:700;font-style:normal;text-decoration:underline;text-transform:none;color:#268bd2;background-color:initial;line-height:20px;letter-spacing:normal;text-shadow:none}.ps13{position:relative;margin-left:326px;margin-top:-2769px}.s18{min-width:134px;width:134px;min-height:108px;height:108px}.c32{z-index:4;pointer-events:auto}.i3{position:absolute;left:18px;width:98px;height:108px;top:0;border:0}.ps14{position:relative;margin-left:792px;margin-top:-2809px}.s19{min-width:168px;width:168px;min-height:799px}.ps15{position:relative;margin-left:62px;margin-top:0}.s20{min-width:85px;width:85px;min-height:100px;height:100px}.c33{z-index:3;pointer-events:auto}.i4{position:absolute;left:0;width:84px;height:100px;top:0;border:0}.ps16{position:relative;margin-left:47px;margin-top:47px}.s21{min-width:113px;width:113px;min-height:40px}.c34{z-index:10;pointer-events:auto}.f20{font-family:"Helvetica Neue", sans-serif;font-size:12px;font-weight:700;font-style:normal;text-decoration:none;text-transform:none;background-color:initial;line-height:15px;letter-spacing:normal;text-shadow:none;text-indent:0;text-align:center;padding-top:12px;padding-bottom:11px;margin-top:0;margin-bottom:0}.btn1{border:1px double #677a85;-webkit-border-radius:20px;-moz-border-radius:20px;border-radius:20px;background-color:#ddedf7;background-clip:padding-box;box-shadow:8px 7px 4px rgba(0,0,0,.40);color:#000}.btn1:hover{background-color:#82939e;border-color:#000;color:#000}.btn1:active{background-color:#52646f;border-color:#000;color:#fff}.v7{display:inline-block;overflow:hidden;outline:0}.s22{width:111px;padding-right:0;height:15px}.ps17{position:relative;margin-left:40px;margin-top:16px}.s23{min-width:128px;width:128px;min-height:55px}.c35{z-index:7;pointer-events:auto}.f21{font-family:"Helvetica Neue", sans-serif;font-size:12px;font-weight:700;font-style:normal;text-decoration:none;text-transform:none;background-color:initial;line-height:15px;letter-spacing:normal;text-shadow:none;text-indent:0;text-align:center;padding-top:5px;padding-bottom:5px;margin-top:0;margin-bottom:0}.btn2{border:0;-webkit-border-radius:28px;-moz-border-radius:28px;border-radius:28px;background-color:#ddedf7;background-clip:padding-box;box-shadow:8px 4px 4px rgba(0,0,0,.40);color:#000}.btn2:hover{background-color:#82939e;border-color:#000;color:#000}.btn2:active{background-color:#52646f;border-color:#000;color:#fff}.s24{width:128px;padding-right:0;height:45px}.ps18{position:relative;margin-left:44px;margin-top:10px}.s25{min-width:115px;width:115px;min-height:50px}.c36{z-index:14;pointer-events:auto}.f22{font-family:"Helvetica Neue", sans-serif;font-size:12px;font-weight:700;font-style:normal;text-decoration:none;text-transform:none;background-color:initial;line-height:15px;letter-spacing:normal;text-shadow:none;text-indent:0;text-align:center;padding-top:8px;padding-bottom:8px;margin-top:0;margin-bottom:0}.btn3{border:2px solid #677a85;-webkit-border-radius:25px;-moz-border-radius:25px;border-radius:25px;background-color:#ddedf7;background-clip:padding-box;box-shadow:11px 8px 4px rgba(0,0,0,.40);color:#000}.btn3:hover{background-color:#82939e;border-color:#000;color:#000}.btn3:active{background-color:#52646f;border-color:#000;color:#fff}.s26{width:111px;padding-right:0;height:30px}.ps19{position:relative;margin-left:38px;margin-top:15px}.s27{min-width:130px;width:130px;min-height:64px}.c37{z-index:15;pointer-events:auto}.f23{font-family:"Helvetica Neue", sans-serif;font-size:12px;font-weight:700;font-style:normal;text-decoration:none;text-transform:none;background-color:initial;line-height:15px;letter-spacing:normal;text-shadow:none;text-indent:0;text-align:center;padding-top:2px;padding-bottom:2px;margin-top:0;margin-bottom:0}.btn4{border:0;-webkit-border-radius:32px;-moz-border-radius:32px;border-radius:32px;background-color:#ddedf7;background-clip:padding-box;box-shadow:12px 8px 4px rgba(0,0,0,.40);color:#000}.btn4:hover{background-color:#c0c0c0;border-color:#000;color:#000}.btn4:active{background-color:#52646f;border-color:#000;color:#fff}.s28{width:130px;padding-right:0;height:60px}.ps20{position:relative;margin-left:45px;margin-top:14px}.c38{z-index:12;pointer-events:auto}.btn5{border:1px double #677a85;-webkit-border-radius:20px;-moz-border-radius:20px;border-radius:20px;background-color:#ddedf7;background-clip:padding-box;box-shadow:8px 7px 4px rgba(0,0,0,.40);color:#000}.btn5:hover{background-color:#82939e;border-color:#000;color:#000}.btn5:active{background-color:#52646f;border-color:#000;color:#fff}.ps21{position:relative;margin-left:45px;margin-top:24px}.c39{z-index:8;pointer-events:auto}.btn6{border:1px double #677a85;-webkit-border-radius:20px;-moz-border-radius:20px;border-radius:20px;background-color:#ddedf7;background-clip:padding-box;box-shadow:8px 7px 4px rgba(0,0,0,.40);color:#000}.btn6:hover{background-color:#82939e;border-color:#000;color:#000}.btn6:active{background-color:#52646f;border-color:#000;color:#fff}.ps22{position:relative;margin-left:0;margin-top:131px}.s29{min-width:168px;width:168px;min-height:153px;height:153px}.c40{z-index:9;pointer-events:auto}.f24{font-family:"PT Sans";font-size:16px;font-weight:400;font-style:normal;text-decoration:none;text-transform:none;color:#dc322f;background-color:initial;line-height:20px;letter-spacing:normal;text-shadow:none}.f25{font-family:"PT Sans";font-size:16px;font-weight:400;font-style:normal;text-decoration:none;text-transform:none;color:#000;background-color:initial;line-height:20px;letter-spacing:normal;text-shadow:none}.menu-device{background-color:rgb(0,0,0);display:none}</style>
<meta content="Anglican Churches of Pictou County, ACPC, Nova Scotia," name="keywords"/>
<link href="css/site.4bc48b.css" media="print" onload="this.media='all';this.onload=null;" rel="stylesheet"/>
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/site.4bc48b-lteIE7.css" type="text/css">
<![endif]-->
</head>
<body id="b">
<div class="ps5 v2 s5">
<div class="s6">
<div class="v3 ps6 s7 c5">
<div class="v3 ps7 s8 w1">
<div class="v3 ps8 s9 c5">
<div class="v3 ps6 s10 c6">
<a class="a1" href="https:www.anglican.ca" rel="noopener" target="_blank"><picture class="i2"><source srcset="images/anglican_church_of_canada-93.png 1x, images/anglican_church_of_canada-186.png 2x"><img class="js1 i1" src="images/anglican_church_of_canada-93.png"/></source></picture></a>
</div>
<div class="v4 ps9 s11 c7">
<p class="p2 f10">Last update: January 10, 2021</p>
</div>
</div>
<div class="v5 ps10 s12 c8">
<ul class="menu-dropdown v6 ps6 s12 m1" id="m1">
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="#"><div class="menu-content mcv1"><div class="v3 ps6 s14 c9"><div class="v4 ps11 s15 c10"><p class="p3 f11">Home</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="rectors-christmas-message.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c11"><div class="v4 ps11 s15 c10"><p class="p3 f11">Rectors Christmas Message</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="prayer-shawl-ministry.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c12"><div class="v4 ps11 s15 c10"><p class="p3 f11">Prayer Shawl Ministry</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="sunday-worship-special-services.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c13"><div class="v4 ps11 s15 c10"><p class="p3 f11">Sunday Worship &amp; Special Services</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="service-for-the-homes-for-special-care.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c14"><div class="v4 ps11 s15 c10"><p class="p3 f11">Service for the Homes for Special Care</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="announcements.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c15"><div class="v4 ps11 s15 c10"><p class="p3 f11">Announcements</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="worship-resources.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c16"><div class="v4 ps11 s15 c10"><p class="p3 f11">Worship Resources</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="christ-church-stellarton.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c17"><div class="v4 ps11 s15 c10"><p class="p3 f11">Christ Church</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="st-james-pictou.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c18"><div class="v4 ps11 s15 c10"><p class="p3 f11">St James'</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="how-to-contact-us.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c19"><div class="v4 ps11 s15 c10"><p class="p3 f11">How to Contact Us</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="contact.htm"><div class="menu-content mcv1"><div class="v3 ps6 s14 c20"><div class="v4 ps11 s15 c10"><p class="p3 f11">ACPC Contact Information</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="contact-form.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c21"><div class="v4 ps11 s15 c10"><p class="p3 f11">Contact Us</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="st-james-grads.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c22"><div class="v4 ps11 s15 c10"><p class="p3 f11">St James' Grads</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="st-bees.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c23"><div class="v4 ps11 s15 c10"><p class="p3 f11">St Bees'</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="#"><div class="menu-content mcv1"><div class="v3 ps6 s14 c24"><div class="v4 ps11 s15 c10"><p class="p3 f11">St Bees' Photos</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="Volumes/Church Services/augustines/augustine.htm"><div class="menu-content mcv1"><div class="v3 ps6 s14 c25"><div class="v4 ps11 s15 c10"><p class="p3 f11">St Augustine's, Trenton</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="acpc-photographs.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c26"><div class="v4 ps11 s15 c10"><p class="p3 f11">ACPC Photos</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="men-s-choir-nursing-home-services.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c27"><div class="v4 ps11 s15 c10"><p class="p3 f11">Men's Choir &amp; Nursing Homes</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="education-for-ministry.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c28"><div class="v4 ps11 s15 c10"><p class="p3 f11">Education for Ministry</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="external-links.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c29"><div class="v4 ps11 s15 c10"><p class="p3 f11">External Links</p></div></div></div></a>
</li>
<li class="v3 ps6 s13 mit1">
<a class="ml1" href="contact-form.html"><div class="menu-content mcv1"><div class="v3 ps6 s14 c30"><div class="v4 ps11 s15 c10"><p class="p3 f11">Contact Form</p></div></div></div></a>
</li>
</ul>
</div>
</div>
<div class="v3 ps12 s16 w1">
<div class="v4 ps6 s17 c31">
<p class="p3"><span class="f12"><a href="#">The Anglican Churches of Pictou County</a></span></p>
<p class="p3"><span class="f12"><a href="#"><br/></a></span></p>
<p class="p3"><span class="f12"><a href="#"><br/></a></span></p>
<p class="p3"><span class="f13"><a href="javascript:em1();">The Reverend Darlene Jewers</a></span><span class="f14">Â </span></p>
<p class="p3 f15">Â <span class="f16"><a href="#">Phone: 902-752-7213</a></span></p>
<p class="p3"><span class="f16"><a href="#">Cell: 902-921-1100</a></span><span class="f17"><a href="#"><br/></a></span><span class="f13"><a href="javascript:em2();">ACPC Central Office</a></span></p>
<p class="p3"><span class="f18"><a href="#">St Jamesâ Hall</a></span></p>
<p class="p3"><span class="f18"><a href="#">8 Denoon Street<br/></a></span><span class="f15"><a href="#">(Temporarily Closed)</a></span></p>
<p class="p3 f15"><a href="#">Pictou, NS</a></p>
<p class="p3 f15"><a href="#">Phone: 902-485-9322<br/>Â Â Â Â Â Â Mailing address: Anglican Churches of Pictou County<br/>PO Box 490<br/>Pictou, NS, B0K 1H0<br/></a><span class="f16"><a href="#"><br/></a></span></p>
<h3 class="p3"><span class="f19"><a href="#"><br/></a></span></h3>
<p class="p3"><span class="f13"><a href="download/acpc-covenant-2019-signed.pdf">The ACPC Covenant in Ministry</a></span></p>
<p class="p3"><span class="f13"><a href="download/2019-acpc-annual-report.pdf">2019 Annual Report</a></span></p>
</div>
<div class="v3 ps13 s18 c32">
<picture class="i2">
<source srcset="images/celtic-cross-98-1.jpg 1x">
<img class="js2 i3" src="images/celtic-cross-98-1.jpg"/>
</source></picture>
</div>
<div class="v1 ps1 s1 c1">
<h3 class="p1"><span class="f1"><a href="https://www.youtube.com/channel/UCo2cA8G-2pEN_dh0-RUEIjg/videos?view=57">Christ Church, Stellarton YouTube Channel</a></span><span class="f2">.Â </span></h3>
<p class="p1 f3">(This is where ACPC services can be accessed.)</p>
</div>
<div class="v1 ps2 s2 c2">
<h3 class="p1 f4">If you have a video you would like to have placed on this website, <span class="f5">please upload it to YouTube and then send the link to me. </span> Please do not attempt to send files to me or the Listserverâs by email as they are far too large!Â </h3>
</div>
<div class="v1 ps3 s3 c3">
<h3 class="p1 f6">Notice: Links to Zoom worship and and announcementsÂ Â and other communications are sent to the ACPC Listserver. If you are not on the ACPC parishioners Listserver, please email <span class="f7"><a href="javascript:em3();">djh@djharrison.ca</a></span></h3>
</div>
<div class="v1 ps4 s4 c4">
<p class="p1"><span class="f8"><a href="download/acpc-covid-19-letter-2.docx">Letter to Parishioners </a></span><span class="f9">regarding COVID-19</span></p>
</div>
</div>
<div class="v3 ps14 s19 w1">
<div class="v3 ps15 s20 c33">
<a class="a1" href="https://www.nspeidiocese.ca/" rel="noopener" target="_blank"><picture class="i2"><source srcset="images/diocesancrest-84.jpg 1x"><img class="js3 i4" src="images/diocesancrest-84.jpg"/></source></picture></a>
</div>
<div class="v3 ps16 s21 c34">
<a class="f20 btn1 v7 s22" href="http://www.pictouanglicans.ca/bulletins/bulletin.pdf">Sunday Bulletin</a>
</div>
<div class="v3 ps17 s23 c35">
<a class="f21 btn2 v7 s24" href="https://www.nspeidiocese.ca/covid-19-in-the-diocese-of-nova-scotia-and-prince-edward-island--183/pages/local-anglican-live-worship?fbclid=IwAR3bPjZO5ryz8Vo_tLhiiR-8-gfj6YzMiDCAk18yJagk2Dlt59n_Xqg6Oyk">Anglican Live Video Worship in the Diocese</a>
</div>
<div class="v3 ps18 s25 c36">
<a class="f22 btn3 v7 s26" href="download/advent-prayers.pdf">Advent Prayers 2020</a>
</div>
<div class="v3 ps19 s27 c37">
<a class="f23 btn4 v7 s28" href="https://nspeilayministers.ca/"><p>Diocesan Licensed Lay Ministers Association</p></a>
</div>
<div class="v3 ps20 s21 c38">
<a class="f20 btn5 v7 s22" href="http://www.pictouanglicans.ca/bulletins/calendar.pdf" rel="noopener" target="_blank">Monthly Activities</a>
</div>
<div class="v3 ps21 s21 c39">
<a class="f20 btn6 v7 s22" href="http://www.pictouanglicans.ca/bulletins/schedule.pdf" rel="noopener" target="_blank">Service Schedule</a>
</div>
<div class="v4 ps22 s29 c40">
<h3 class="p2 f24">Effective, March 25<span class="f25">, the ACPC Office is closed. Susan will be working from home. She is available via email and will be checking messages.</span></h3>
</div>
</div>
</div>
</div>
</div>
<script>dpth="/";!function(){var s=["js/jquery.909e49.js","js/jqueryui.909e49.js","js/menu.909e49.js","js/menu-dropdown-animations.909e49.js","js/menu-dropdown.4bc48b.js","js/index.4bc48b.js"],n={},j=0,e=function(e){var o=new XMLHttpRequest;o.open("GET",s[e],!0),o.onload=function(){if(n[e]=o.responseText,6==++j)for(var t in s){var i=document.createElement("script");i.textContent=n[t],document.body.appendChild(i)}},o.send()};for(var o in s)e(o)}();
</script>
<script type="text/javascript">
var ver=RegExp(/Mozilla\/5\.0 \(Linux; .; Android ([\d.]+)/).exec(navigator.userAgent);if(ver&&parseFloat(ver[1])<5){document.getElementsByTagName('body')[0].className+=' whitespacefix';}
</script>
</body>
</html>