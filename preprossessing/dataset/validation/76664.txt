<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Performance Marketing Platform</title>
<style>
        body {
            background-color: #111d30;
            color: #FFF;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        }

        a {
            color: #FFF;
        }
    </style>
</head>
<body>
<div style="background-color: #293c5a; margin: 10px auto; width: 600px; padding: 15px; border: 1px solid #d3d3d3">
<h1>Performance Tracking</h1>
<p>
            This is part of a performance marketing platform. This server handles up to millions of events per day.
        </p>
<h2>Key features include</h2>
<ul>
<li>Fraud Prevention</li>
<li>Smart Link Campaigns</li>
<li>Smart Alerts</li>
<li>Deep Analytics and Reporting</li>
<li>View Through Attribution</li>
<li>e-Commerce Support</li>
</ul>
</div>
</body>
</html>