<!DOCTYPE html>
<html lang="fr-FR">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="/wp-content/uploads/sites/2/2020/03/favicon.ico" rel="shortcut icon"/>
<link href="/wp-content/themes/yootheme/vendor/yootheme/theme-wordpress/assets/images/apple-touch-icon.png" rel="apple-touch-icon"/>
<!-- This site is optimized with the Yoast SEO plugin v15.5 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Page non trouvée - Emmaüs Bourgoin-Jallieu</title>
<meta content="noindex, follow" name="robots"/>
<meta content="fr_FR" property="og:locale"/>
<meta content="Page non trouvée - Emmaüs Bourgoin-Jallieu" property="og:title"/>
<meta content="Emmaüs Bourgoin-Jallieu" property="og:site_name"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://emmaus-bourgoin.org/#organization","name":"Emma\u00fcs Bourgoin-Jallieu","url":"https://emmaus-bourgoin.org/","sameAs":[],"logo":{"@type":"ImageObject","@id":"https://emmaus-bourgoin.org/#logo","inLanguage":"fr-FR","url":"https://emmaus-bourgoin.org/wp-content/uploads/sites/2/2020/03/emmausr2aban.png","width":228,"height":86,"caption":"Emma\u00fcs Bourgoin-Jallieu"},"image":{"@id":"https://emmaus-bourgoin.org/#logo"}},{"@type":"WebSite","@id":"https://emmaus-bourgoin.org/#website","url":"https://emmaus-bourgoin.org/","name":"Emma\u00fcs Bourgoin-Jallieu","description":"Le site Emma\u00fcs d&#039;Emma\u00fcs Bourgoin-Jallieu","publisher":{"@id":"https://emmaus-bourgoin.org/#organization"},"potentialAction":[{"@type":"SearchAction","target":"https://emmaus-bourgoin.org/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"fr-FR"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://emmaus-bourgoin.org/feed/" rel="alternate" title="Emmaüs Bourgoin-Jallieu » Flux" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/emmaus-bourgoin.org\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://emmaus-bourgoin.org/wp-content/plugins/simple-google-recaptcha/sgr.css?ver=1606569544" id="sgr_main-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/packages/select2/select2.min.css?ver=5.15.5" id="mec-select2-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/css/iconfonts.css?ver=5.6" id="mec-font-icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/css/frontend.min.css?ver=5.15.5" id="mec-frontend-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/packages/tooltip/tooltip.css?ver=5.6" id="mec-tooltip-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/packages/tooltip/tooltipster-sideTip-shadow.min.css?ver=5.6" id="mec-tooltip-shadow-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/packages/featherlight/featherlight.css?ver=5.6" id="mec-featherlight-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/css/dyncss.css?ver=5.6" id="mec-dynamic-styles-css" media="all" rel="stylesheet" type="text/css"/>
<style id="mec-dynamic-styles-inline-css" type="text/css">
.mec-event-grid-minimal .mec-modal-booking-button:hover, .mec-events-timeline-wrap .mec-organizer-item a, .mec-events-timeline-wrap .mec-organizer-item:after, .mec-events-timeline-wrap .mec-shortcode-organizers i, .mec-timeline-event .mec-modal-booking-button, .mec-wrap .mec-map-lightbox-wp.mec-event-list-classic .mec-event-date, .mec-timetable-t2-col .mec-modal-booking-button:hover, .mec-event-container-classic .mec-modal-booking-button:hover, .mec-calendar-events-side .mec-modal-booking-button:hover, .mec-event-grid-yearly  .mec-modal-booking-button, .mec-events-agenda .mec-modal-booking-button, .mec-event-grid-simple .mec-modal-booking-button, .mec-event-list-minimal  .mec-modal-booking-button:hover, .mec-timeline-month-divider,  .mec-wrap.colorskin-custom .mec-totalcal-box .mec-totalcal-view span:hover,.mec-wrap.colorskin-custom .mec-calendar.mec-event-calendar-classic .mec-selected-day,.mec-wrap.colorskin-custom .mec-color, .mec-wrap.colorskin-custom .mec-event-sharing-wrap .mec-event-sharing > li:hover a, .mec-wrap.colorskin-custom .mec-color-hover:hover, .mec-wrap.colorskin-custom .mec-color-before *:before ,.mec-wrap.colorskin-custom .mec-widget .mec-event-grid-classic.owl-carousel .owl-nav i,.mec-wrap.colorskin-custom .mec-event-list-classic a.magicmore:hover,.mec-wrap.colorskin-custom .mec-event-grid-simple:hover .mec-event-title,.mec-wrap.colorskin-custom .mec-single-event .mec-event-meta dd.mec-events-event-categories:before,.mec-wrap.colorskin-custom .mec-single-event-date:before,.mec-wrap.colorskin-custom .mec-single-event-time:before,.mec-wrap.colorskin-custom .mec-events-meta-group.mec-events-meta-group-venue:before,.mec-wrap.colorskin-custom .mec-calendar .mec-calendar-side .mec-previous-month i,.mec-wrap.colorskin-custom .mec-calendar .mec-calendar-side .mec-next-month,.mec-wrap.colorskin-custom .mec-calendar .mec-calendar-side .mec-previous-month:hover,.mec-wrap.colorskin-custom .mec-calendar .mec-calendar-side .mec-next-month:hover,.mec-wrap.colorskin-custom .mec-calendar.mec-event-calendar-classic dt.mec-selected-day:hover,.mec-wrap.colorskin-custom .mec-infowindow-wp h5 a:hover, .colorskin-custom .mec-events-meta-group-countdown .mec-end-counts h3,.mec-calendar .mec-calendar-side .mec-next-month i,.mec-wrap .mec-totalcal-box i,.mec-calendar .mec-event-article .mec-event-title a:hover,.mec-attendees-list-details .mec-attendee-profile-link a:hover,.mec-wrap.colorskin-custom .mec-next-event-details li i, .mec-next-event-details i:before, .mec-marker-infowindow-wp .mec-marker-infowindow-count, .mec-next-event-details a,.mec-wrap.colorskin-custom .mec-events-masonry-cats a.mec-masonry-cat-selected,.lity .mec-color,.lity .mec-color-before :before,.lity .mec-color-hover:hover,.lity .mec-wrap .mec-color,.lity .mec-wrap .mec-color-before :before,.lity .mec-wrap .mec-color-hover:hover,.leaflet-popup-content .mec-color,.leaflet-popup-content .mec-color-before :before,.leaflet-popup-content .mec-color-hover:hover,.leaflet-popup-content .mec-wrap .mec-color,.leaflet-popup-content .mec-wrap .mec-color-before :before,.leaflet-popup-content .mec-wrap .mec-color-hover:hover, .mec-calendar.mec-calendar-daily .mec-calendar-d-table .mec-daily-view-day.mec-daily-view-day-active.mec-color, .mec-map-boxshow div .mec-map-view-event-detail.mec-event-detail i,.mec-map-boxshow div .mec-map-view-event-detail.mec-event-detail:hover,.mec-map-boxshow .mec-color,.mec-map-boxshow .mec-color-before :before,.mec-map-boxshow .mec-color-hover:hover,.mec-map-boxshow .mec-wrap .mec-color,.mec-map-boxshow .mec-wrap .mec-color-before :before,.mec-map-boxshow .mec-wrap .mec-color-hover:hover, .mec-choosen-time-message, .mec-booking-calendar-month-navigation .mec-next-month:hover, .mec-booking-calendar-month-navigation .mec-previous-month:hover{color: #f65ca1}.mec-skin-carousel-container .mec-event-footer-carousel-type3 .mec-modal-booking-button:hover, .mec-wrap.colorskin-custom .mec-event-sharing .mec-event-share:hover .event-sharing-icon,.mec-wrap.colorskin-custom .mec-event-grid-clean .mec-event-date,.mec-wrap.colorskin-custom .mec-event-list-modern .mec-event-sharing > li:hover a i,.mec-wrap.colorskin-custom .mec-event-list-modern .mec-event-sharing .mec-event-share:hover .mec-event-sharing-icon,.mec-wrap.colorskin-custom .mec-event-list-modern .mec-event-sharing li:hover a i,.mec-wrap.colorskin-custom .mec-calendar:not(.mec-event-calendar-classic) .mec-selected-day,.mec-wrap.colorskin-custom .mec-calendar .mec-selected-day:hover,.mec-wrap.colorskin-custom .mec-calendar .mec-calendar-row  dt.mec-has-event:hover,.mec-wrap.colorskin-custom .mec-calendar .mec-has-event:after, .mec-wrap.colorskin-custom .mec-bg-color, .mec-wrap.colorskin-custom .mec-bg-color-hover:hover, .colorskin-custom .mec-event-sharing-wrap:hover > li, .mec-wrap.colorskin-custom .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected,.mec-wrap .flip-clock-wrapper ul li a div div.inn,.mec-wrap .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected,.event-carousel-type1-head .mec-event-date-carousel,.mec-event-countdown-style3 .mec-event-date,#wrap .mec-wrap article.mec-event-countdown-style1,.mec-event-countdown-style1 .mec-event-countdown-part3 a.mec-event-button,.mec-wrap .mec-event-countdown-style2,.mec-map-get-direction-btn-cnt input[type="submit"],.mec-booking button,span.mec-marker-wrap,.mec-wrap.colorskin-custom .mec-timeline-events-container .mec-timeline-event-date:before, .mec-has-event-for-booking.mec-active .mec-calendar-novel-selected-day, .mec-booking-tooltip.multiple-time .mec-booking-calendar-date.mec-active, .mec-booking-tooltip.multiple-time .mec-booking-calendar-date:hover{background-color: #f65ca1;}.mec-skin-carousel-container .mec-event-footer-carousel-type3 .mec-modal-booking-button:hover, .mec-timeline-month-divider, .mec-wrap.colorskin-custom .mec-single-event .mec-speakers-details ul li .mec-speaker-avatar a:hover img,.mec-wrap.colorskin-custom .mec-event-list-modern .mec-event-sharing > li:hover a i,.mec-wrap.colorskin-custom .mec-event-list-modern .mec-event-sharing .mec-event-share:hover .mec-event-sharing-icon,.mec-wrap.colorskin-custom .mec-event-list-standard .mec-month-divider span:before,.mec-wrap.colorskin-custom .mec-single-event .mec-social-single:before,.mec-wrap.colorskin-custom .mec-single-event .mec-frontbox-title:before,.mec-wrap.colorskin-custom .mec-calendar .mec-calendar-events-side .mec-table-side-day, .mec-wrap.colorskin-custom .mec-border-color, .mec-wrap.colorskin-custom .mec-border-color-hover:hover, .colorskin-custom .mec-single-event .mec-frontbox-title:before, .colorskin-custom .mec-single-event .mec-events-meta-group-booking form > h4:before, .mec-wrap.colorskin-custom .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected,.mec-wrap .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected,.event-carousel-type1-head .mec-event-date-carousel:after,.mec-wrap.colorskin-custom .mec-events-masonry-cats a.mec-masonry-cat-selected, .mec-marker-infowindow-wp .mec-marker-infowindow-count, .mec-wrap.colorskin-custom .mec-events-masonry-cats a:hover, .mec-has-event-for-booking .mec-calendar-novel-selected-day, .mec-booking-tooltip.multiple-time .mec-booking-calendar-date.mec-active, .mec-booking-tooltip.multiple-time .mec-booking-calendar-date:hover{border-color: #f65ca1;}.mec-wrap.colorskin-custom .mec-event-countdown-style3 .mec-event-date:after,.mec-wrap.colorskin-custom .mec-month-divider span:before{border-bottom-color:#f65ca1;}.mec-wrap.colorskin-custom  article.mec-event-countdown-style1 .mec-event-countdown-part2:after{border-color: transparent transparent transparent #f65ca1;}.mec-wrap.colorskin-custom .mec-box-shadow-color { box-shadow: 0 4px 22px -7px #f65ca1;}.mec-events-timeline-wrap .mec-shortcode-organizers, .mec-timeline-event .mec-modal-booking-button, .mec-events-timeline-wrap:before, .mec-wrap.colorskin-custom .mec-timeline-event-local-time, .mec-wrap.colorskin-custom .mec-timeline-event-time ,.mec-wrap.colorskin-custom .mec-timeline-event-location,.mec-choosen-time-message { background: rgba(246,92,161,.11);}.mec-wrap.colorskin-custom .mec-timeline-events-container .mec-timeline-event-date:after { background: rgba(246,92,161,.3);}.mec-wrap h1 a, .mec-wrap h2 a, .mec-wrap h3 a, .mec-wrap h4 a, .mec-wrap h5 a, .mec-wrap h6 a,.entry-content .mec-wrap h1 a, .entry-content .mec-wrap h2 a, .entry-content .mec-wrap h3 a,.entry-content  .mec-wrap h4 a, .entry-content .mec-wrap h5 a, .entry-content .mec-wrap h6 a {color: #2c3d85 !important;}.mec-wrap.colorskin-custom h1 a:hover, .mec-wrap.colorskin-custom h2 a:hover, .mec-wrap.colorskin-custom h3 a:hover, .mec-wrap.colorskin-custom h4 a:hover, .mec-wrap.colorskin-custom h5 a:hover, .mec-wrap.colorskin-custom h6 a:hover,.entry-content .mec-wrap.colorskin-custom h1 a:hover, .entry-content .mec-wrap.colorskin-custom h2 a:hover, .entry-content .mec-wrap.colorskin-custom h3 a:hover,.entry-content  .mec-wrap.colorskin-custom h4 a:hover, .entry-content .mec-wrap.colorskin-custom h5 a:hover, .entry-content .mec-wrap.colorskin-custom h6 a:hover {color: #38c0f6 !important;}
</style>
<link href="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/packages/lity/lity.min.css?ver=5.6" id="mec-lity-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://emmaus-bourgoin.org/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://emmaus-bourgoin.org/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://emmaus-bourgoin.org/wp-content/themes/yootheme/css/theme.2.css?ver=1607484965" rel="stylesheet"/>
<script id="sgr_main-js-extra" type="text/javascript">
/* <![CDATA[ */
var sgr_main = {"sgr_site_key":"6LeamaEUAAAAAAv5wr7wbQyhxwJ7n-M46dNFk4Iv"};
/* ]]> */
</script>
<script id="sgr_main-js" src="https://emmaus-bourgoin.org/wp-content/plugins/simple-google-recaptcha/sgr.js?ver=1606569544" type="text/javascript"></script>
<script id="jquery-core-js" src="https://emmaus-bourgoin.org/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://emmaus-bourgoin.org/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="mec-frontend-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var mecdata = {"day":"jour","days":"jours","hour":"heure","hours":"heures","minute":"minute","minutes":"minutes","second":"seconde","seconds":"secondes","elementor_edit_mode":"no","recapcha_key":"","ajax_url":"https:\/\/emmaus-bourgoin.org\/wp-admin\/admin-ajax.php","fes_nonce":"ccc88adf57","current_year":"2021","current_month":"01","datepicker_format":"dd.mm.yy&d.m.Y"};
/* ]]> */
</script>
<script id="mec-frontend-script-js" src="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/js/frontend.js?ver=5.15.5" type="text/javascript"></script>
<script id="mec-events-script-js" src="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/js/events.js?ver=5.15.5" type="text/javascript"></script>
<link href="https://emmaus-bourgoin.org/wp-json/" rel="https://api.w.org/"/><link href="https://emmaus-bourgoin.org/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://emmaus-bourgoin.org/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<script defer="" src="https://emmaus-bourgoin.org/wp-content/themes/yootheme/vendor/yootheme/theme-analytics/app/analytics.min.js?ver=2.3.24"></script>
<script defer="" src="https://emmaus-bourgoin.org/wp-content/themes/yootheme/vendor/yootheme/theme-cookie/app/cookie.min.js?ver=2.3.24"></script>
<script src="https://emmaus-bourgoin.org/wp-content/themes/yootheme/vendor/assets/uikit/dist/js/uikit.min.js?ver=2.3.24"></script>
<script src="https://emmaus-bourgoin.org/wp-content/themes/yootheme/vendor/assets/uikit/dist/js/uikit-icons.min.js?ver=2.3.24"></script>
<script src="https://emmaus-bourgoin.org/wp-content/themes/yootheme/js/theme.js?ver=2.3.24"></script>
<script>var $theme = {"google_analytics":"UA-66920247-2","google_analytics_anonymize":"","cookie":{"mode":"notification","template":"<div class=\"tm-cookie-banner uk-section uk-section-xsmall uk-section-muted uk-position-bottom uk-position-fixed uk-position-z-index\">\n        <div class=\"uk-container uk-container-expand uk-text-center\">\n\n            <p>En utilisant ce site Web, vous acceptez l'utilisation de cookies comme d\u00e9crit dans notre politique de confidentialit\u00e9.<\/p>\n                            <button type=\"button\" class=\"js-accept uk-button uk-button-secondary uk-margin-small-left\" data-uk-toggle=\"target: !.uk-section; animation: true\">Ok<\/button>\n            \n            \n        <\/div>\n    <\/div>","position":"bottom"}};</script>
<style id="wp-custom-css" type="text/css">
			.piedpage {
	background-color:#2c3d85;
}
.sliseacc .uk-container {
	padding-left:0px;
	padding-right:0px;
}
.el-overlay  {
	margin-left:0px;
}
.uk-navbar-container{
	border-bottom: 1px solid #B3B3B3;
}
.tribe-common--breakpoint-medium.tribe-events .tribe-events-l-container {
    padding-top: 0px; 
}		</style>
</head>
<body class="error404 dpt">
<div class="tm-page">
<div class="tm-header-mobile uk-hidden@l">
<div class="uk-navbar-container">
<nav uk-navbar="">
<div class="uk-navbar-left">
<a class="uk-navbar-toggle" href="#tm-mobile" uk-toggle="">
<div uk-navbar-toggle-icon=""></div>
</a>
</div>
<div class="uk-navbar-center">
<a class="uk-navbar-item uk-logo" href="https://emmaus-bourgoin.org">
<img alt="" src="/wp-content/uploads/sites/2/2020/03/favicon.ico#thumbnail=%2C&amp;srcset=1"/></a>
</div>
</nav>
</div>
<div id="tm-mobile" mode="slide" overlay="" uk-offcanvas="">
<div class="uk-offcanvas-bar">
<button class="uk-offcanvas-close" type="button" uk-close=""></button>
<div class="uk-child-width-1-1" uk-grid=""> <div>
<div class="uk-panel">
<ul class="uk-nav uk-nav-default">
<li class="uk-parent"><a class=" menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children" href="https://emmaus-bourgoin.org/dans-votre-region/">Actualités et évènements</a>
<ul class="uk-nav-sub">
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/dans-votre-region/">Dans votre région</a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/calendrier-des-evenements/">Calendrier des évènements</a></li></ul></li>
<li class="uk-parent"><a class=" menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children" href="https://emmaus-bourgoin.org/acheter-et-donner/">Agir avec emmaüs</a>
<ul class="uk-nav-sub">
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/acheter-et-donner/">Acheter et donner</a></li>
<li><a class=" menu-item menu-item-type-custom menu-item-object-custom" href="https://www.label-emmaus.co/fr/" target="_blank" title="Label Emmaüs">label emmaüs</a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/devenir-benevole/">Devenir bénévole</a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/partenaires-entreprises/">Partenaires entreprises</a></li></ul></li>
<li class="uk-parent"><a class=" menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children" href="https://emmaus-bourgoin.org/le-mouvement-emmaus/">Qui sommes-nous ?</a>
<ul class="uk-nav-sub">
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/le-mouvement-emmaus/">Le mouvement Emmaüs</a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/emmaus-bourgoin-jallieu/">Emmaüs Bourgoin-Jallieu</a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/emmaus-dans-votre-region/">Emmaüs dans votre région</a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/nos-actions/">Nos actions</a></li></ul></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/trouver-bourgoin/"><div uk-icon="location"></div></a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/calendrier-des-evenements/"><div uk-icon="calendar"></div></a></li>
<li><a class=" menu-item menu-item-type-custom menu-item-object-custom"><div uk-icon="folder"></div></a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/contact-bj/"><div uk-icon="mail"></div></a></li></ul>
</div>
</div> <div>
<div class="uk-panel widget-search" id="widget-search-1">
<form action="https://emmaus-bourgoin.org/" class="uk-search uk-search-default" id="search-648" method="get" role="search">
<span uk-search-icon=""></span>
<input class="uk-search-input" name="s" placeholder="Rechercher…" type="search"/>
</form>
</div>
</div></div>
</div>
</div>
</div>
<div class="tm-header uk-visible@l" uk-header="">
<div class="uk-navbar-container">
<div class="uk-container uk-container-expand">
<nav class="uk-navbar" uk-navbar='{"align":"left","boundary":"!.uk-navbar-container"}'>
<div class="uk-navbar-left">
<a class="uk-navbar-item uk-logo" href="https://emmaus-bourgoin.org">
<img alt="" data-height="86" data-width="228" sizes="(min-width: 228px) 228px" src="/wp-content/themes/yootheme/cache/emmausr2aban-d6710f1a.png" srcset="/wp-content/themes/yootheme/cache/emmausr2aban-d6710f1a.png 228w"/></a>
</div>
<div class="uk-navbar-right">
<ul class="uk-navbar-nav">
<li class="uk-parent"><a class=" menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children" href="https://emmaus-bourgoin.org/dans-votre-region/">Actualités et évènements</a>
<div class="uk-navbar-dropdown"><div class="uk-navbar-dropdown-grid uk-child-width-1-1" uk-grid=""><div><ul class="uk-nav uk-navbar-dropdown-nav">
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/dans-votre-region/">Dans votre région</a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/calendrier-des-evenements/">Calendrier des évènements</a></li></ul></div></div></div></li>
<li class="uk-parent"><a class=" menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children" href="https://emmaus-bourgoin.org/acheter-et-donner/">Agir avec emmaüs</a>
<div class="uk-navbar-dropdown"><div class="uk-navbar-dropdown-grid uk-child-width-1-1" uk-grid=""><div><ul class="uk-nav uk-navbar-dropdown-nav">
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/acheter-et-donner/">Acheter et donner</a></li>
<li><a class=" menu-item menu-item-type-custom menu-item-object-custom" href="https://www.label-emmaus.co/fr/" target="_blank" title="Label Emmaüs">label emmaüs</a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/devenir-benevole/">Devenir bénévole</a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/partenaires-entreprises/">Partenaires entreprises</a></li></ul></div></div></div></li>
<li class="uk-parent"><a class=" menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children" href="https://emmaus-bourgoin.org/le-mouvement-emmaus/">Qui sommes-nous ?</a>
<div class="uk-navbar-dropdown"><div class="uk-navbar-dropdown-grid uk-child-width-1-1" uk-grid=""><div><ul class="uk-nav uk-navbar-dropdown-nav">
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/le-mouvement-emmaus/">Le mouvement Emmaüs</a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/emmaus-bourgoin-jallieu/">Emmaüs Bourgoin-Jallieu</a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/emmaus-dans-votre-region/">Emmaüs dans votre région</a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/nos-actions/">Nos actions</a></li></ul></div></div></div></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/trouver-bourgoin/"><div uk-icon="location"></div></a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/calendrier-des-evenements/"><div uk-icon="calendar"></div></a></li>
<li><a class=" menu-item menu-item-type-custom menu-item-object-custom"><div uk-icon="folder"></div></a></li>
<li><a class=" menu-item menu-item-type-post_type menu-item-object-page" href="https://emmaus-bourgoin.org/contact-bj/"><div uk-icon="mail"></div></a></li></ul>
<div class="uk-navbar-item widget-search" id="widget-search-1">
<a class="uk-search-toggle" href="#search-531-modal" uk-search-icon="" uk-toggle=""></a>
<div class="uk-modal-full" id="search-531-modal" uk-modal="">
<div class="uk-modal-dialog uk-flex uk-flex-center uk-flex-middle" uk-height-viewport="">
<button class="uk-modal-close-full" type="button" uk-close=""></button>
<div class="uk-search uk-search-large">
<form action="https://emmaus-bourgoin.org/" class="uk-search uk-search-large" id="search-531" method="get" role="search">
<input autofocus="" class="uk-search-input uk-text-center" name="s" placeholder="Rechercher…" type="search"/>
</form> </div>
</div>
</div>
</div>
</div>
</nav>
</div>
</div>
</div>
<div class="tm-main uk-section uk-section-default" id="tm-main" uk-height-viewport="expand: true">
<div class="uk-container">
<h1 class="uk-heading-medium uk-text-center">Oups ! Cette page est introuvable.</h1>
<p class="uk-text-large uk-text-center uk-margin-large-bottom">Rien n'a été trouvé. Veuillez réessayer une recherche.</p>
<div class="uk-text-center">
<form action="https://emmaus-bourgoin.org/" class="uk-search uk-search-default" id="search-806" method="get" role="search">
<span uk-search-icon=""></span>
<input class="uk-search-input" name="s" placeholder="Rechercher…" type="search"/>
</form>
</div>
</div>
</div>
<!-- Builder #footer -->
<div class="piedpage uk-section-default uk-section uk-section-small">
<div class="uk-container uk-container-expand">
<div class="uk-margin-remove-bottom tm-grid-expand uk-grid-column-large uk-margin-medium uk-margin-remove-bottom" uk-grid="">
<div class="uk-width-1-3@m">
<h2 class="uk-text-center"> <span style="color: #ffffff;">Newsletter</span> </h2>
<div class="uk-width-medium uk-margin-auto">
<form action="https://emmaus-bourgoin.org/wp-admin/admin-ajax.php?p=theme%2Fnewsletter%2Fsubscribe&amp;action=kernel" class="uk-form uk-panel js-form-newsletter" method="post">
<div class="uk-grid-small uk-child-width-1-1" uk-grid="">
<div class="uk-position-relative">
<button class="el-button uk-form-icon uk-form-icon-flip" title="Subscribe" uk-icon="icon: mail;"></button> <input class="el-input uk-input uk-form-small" name="email" placeholder="Email" required="" type="email"/> </div>
</div>
<input name="settings" type="hidden" value="jVcF9LbGd+EgrEsgJMSsTA==.ZXJZS2JUclA4RDhyQ3hkSmt6VDh3TTRFa0oyVlJUOENxelAzTEZmZEw0WGh1YXNLSndZSW55aFh6TXc3L2xZOTF2c0NxUE9nSHg2d25rWlp4NWR2VzBkTis5TGdjSCtZNVJvOWZXdEhVVlJYS3lmWmgwQ0g3VG42ZEx3bjdpMERobkF0RnM0TDBlbjhqbkNxQWFBL2dNMTdINDlKTzMxTU80VTZ0aWUzTE11SUVzdkRhQ1Jhakw5WFdJdEdLTW1zbkJlTlN5UkFLZ2VnVlhoL3JNd0l3YzNwK0hOcEUxK3B3SS9pNTFLNU9za0VQOXZWcW45L09tajNQVEFNMFRoSmhwZkdoOExwVDdlR0NGQ3dUcVJSZXdKTFNibGlocFNSKzRzVVh4Ym10Y3dXUjAvNlFaUUppQzYySG1nc09INEk=.MDk0NjY5YjVmNTI5ZmM1OTVkZjRlMjY2ZWQzZDBhNzU5ZThkMGRlMjc5MzJmMjQ2OGJhYWUwNmM5YmIwYzYwMQ=="/>
<div class="message uk-margin uk-hidden"></div>
</form>
</div>
</div>
<div class="uk-width-1-3@m">
<h1> </h1>
<h2 class="uk-text-center"> <a href="https://emmaus-bourgoin.org/nous-trouver/"><img alt="" class="alignnone size-full wp-image-65" height="55" src="https://emmaus-bourgoin.org/wp-content/uploads/sites/2/2020/03/04.png" width="177"/></a> </h2>
</div>
<div class="uk-width-1-3@m">
<h2 class="uk-text-center"> <span style="color: #ffffff;">Suivez-nous</span> </h2>
<div class="uk-margin uk-text-center"> <div class="uk-child-width-auto uk-grid-small uk-flex-center" uk-grid="">
<div>
<a class="el-link uk-icon-button" href="https://www.facebook.com/emmausrhonealpes/?fref=ts" rel="noreferrer" uk-icon="icon: facebook;"></a></div>
<div>
<a class="el-link uk-icon-button" href="https://www.youtube.com/channel/UCVuTFPhjWWy4nGVXz3xJjxQ" rel="noreferrer" uk-icon="icon: youtube;"></a></div>
</div></div>
</div>
</div>
</div>
</div>
<div class="piedpage uk-section-default uk-section uk-section-xsmall uk-padding-remove-top uk-padding-remove-bottom">
<div class="uk-container">
<div class="tm-grid-expand uk-child-width-1-1 uk-grid-margin" uk-grid="">
<div class="uk-width-1-1@m">
<div class="uk-panel uk-margin"><p style="text-align: center;"><a href="https://emmaus-bourgoin.org/dans-votre-region/">Actualités &amp; Évènements</a> | <a href="https://emmaus-bourgoin.org/acheter-et-donner/">Agir avec Emmaüs</a> | <a href="https://emmaus-bourgoin.org/le-mouvement-emmaus/">Qui sommes-nous</a> | <a href="https://emmaus-bourgoin.org/contact/">Contact</a><br/><span style="color: #999999;"><a href="https://emmaus-bourgoin.org/mentions-legales/" style="color: #999999;">Réalisé par TRIRA</a></span><br/><span style="color: #000080;"><a href="/wp-admin" style="color: #2C3D85;">--</a></span></p></div>
</div>
</div>
</div>
</div>
</div>
<script id="jquery-ui-core-js" src="https://emmaus-bourgoin.org/wp-includes/js/jquery/ui/core.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="jquery-ui-datepicker-js" src="https://emmaus-bourgoin.org/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="jquery-ui-datepicker-js-after" type="text/javascript">
jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Fermer","currentText":"Aujourd\u2019hui","monthNames":["janvier","f\u00e9vrier","mars","avril","mai","juin","juillet","ao\u00fbt","septembre","octobre","novembre","d\u00e9cembre"],"monthNamesShort":["Jan","F\u00e9v","Mar","Avr","Mai","Juin","Juil","Ao\u00fbt","Sep","Oct","Nov","D\u00e9c"],"nextText":"Suivant","prevText":"Pr\u00e9c\u00e9dent","dayNames":["dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi"],"dayNamesShort":["dim","lun","mar","mer","jeu","ven","sam"],"dayNamesMin":["D","L","M","M","J","V","S"],"dateFormat":"d MM yy","firstDay":1,"isRTL":false});});
</script>
<script id="mec-typekit-script-js" src="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/js/jquery.typewatch.js?ver=5.15.5" type="text/javascript"></script>
<script id="mec-featherlight-script-js" src="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/packages/featherlight/featherlight.js?ver=5.15.5" type="text/javascript"></script>
<script id="mec-select2-script-js" src="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/packages/select2/select2.full.min.js?ver=5.15.5" type="text/javascript"></script>
<script id="mec-tooltip-script-js" src="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/packages/tooltip/tooltip.js?ver=5.15.5" type="text/javascript"></script>
<script id="mec-lity-script-js" src="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/packages/lity/lity.min.js?ver=5.15.5" type="text/javascript"></script>
<script id="mec-colorbrightness-script-js" src="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/packages/colorbrightness/colorbrightness.min.js?ver=5.15.5" type="text/javascript"></script>
<script id="mec-owl-carousel-script-js" src="https://emmaus-bourgoin.org/wp-content/plugins/modern-events-calendar-lite/assets/packages/owl-carousel/owl.carousel.min.js?ver=5.15.5" type="text/javascript"></script>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/emmaus-bourgoin.org\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://emmaus-bourgoin.org/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="google-recaptcha-js" src="https://www.google.com/recaptcha/api.js?render=6Le6j64UAAAAAD5pQLcBFzU62eu2kNI5hoMzau89&amp;ver=3.0" type="text/javascript"></script>
<script id="wpcf7-recaptcha-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7_recaptcha = {"sitekey":"6Le6j64UAAAAAD5pQLcBFzU62eu2kNI5hoMzau89","actions":{"homepage":"homepage","contactform":"contactform"}};
/* ]]> */
</script>
<script id="wpcf7-recaptcha-js" src="https://emmaus-bourgoin.org/wp-content/plugins/contact-form-7/modules/recaptcha/script.js?ver=5.3.2" type="text/javascript"></script>
<script id="wp-embed-js" src="https://emmaus-bourgoin.org/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
<script id="script:newsletter-js" src="https://emmaus-bourgoin.org/wp-content/themes/yootheme/vendor/yootheme/builder-newsletter/app/newsletter.min.js?ver=2.3.24" type="text/javascript"></script>
</body>
</html>
