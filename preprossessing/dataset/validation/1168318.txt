<!DOCTYPE html>
<html dir="ltr" lang="es-es">
<head>
<meta charset="utf-8"/>
<title>404 - Categoría no encontrada</title>
<link href="/templates/system/css/error.css" rel="stylesheet"/>
<!--[if lt IE 9]><script src="/media/jui/js/html5.js"></script><![endif]-->
</head>
<body>
<div class="error">
<div id="outline">
<div id="errorboxoutline">
<div id="errorboxheader">404 - Categoría no encontrada</div>
<div id="errorboxbody">
<p><strong>Es posible que no pueda visitar esta página por:</strong></p>
<ol>
<li>un marcador/favorito <strong>fuera de fecha</strong></li>
<li>un motor de búsqueda que tiene una lista <strong>fuera de fecha para este sitio</strong></li>
<li>una <strong>dirección mal escrita</strong></li>
<li>usted <strong>no tiene acceso</strong> a esta página</li>
<li>El recurso solicitado no fue encontrado.</li>
<li>Se produjo un error al procesar su solicitud.</li>
</ol>
<p><strong>Por favor, intente una de las páginas siguientes:</strong></p>
<ul>
<li><a href="/index.php" title="Ir a la página de Inicio">Página de inicio</a></li>
</ul>
<p>Si las dificultades persisten, póngase en contacto con el administrador de este sitio.</p>
<div id="techinfo">
<p>Categoría no encontrada</p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
