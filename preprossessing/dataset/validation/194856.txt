<!DOCTYPE html>
<html lang="en-us">
<head>
<title>John Montefusco Awards by Baseball Almanac</title>
<meta charset="utf-8"/>
<meta content="John Montefusco Awards john montefusco awards" name="keywords"/>
<meta content="John Montefusco baseball awards won list by Baseball Almanac." name="description"/>
<meta content="index, follow" name="robots"/>
<meta content="Baseball Almanac, Inc." name="Author"/>
<!-- ' / -->
<!-- Mobile viewport -->
<meta content="width=device-width; initial-scale=1.0" name="viewport"/>
<link href="/css/styles1.1.css" rel="stylesheet"/>
<link href="/css/menu.css" rel="stylesheet"/>
<style><!--  --></style>
<!-- BEGIN GOOGLE ANALYTICS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-1805063-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-1805063-1');
</script>
<!-- END GOOGLE ANALYTICS -->
</head>
<body>
<!-- BEGIN FREESTAR -->
<div class="google-adsense" style="text-align: center; margin: 10px 0">
<script data-cfasync="false" type="text/javascript">
  var freestar = freestar || new Object();
  freestar.hitTime = Date.now();
  freestar.queue = freestar.queue || [];
  freestar.config = freestar.config || new Object();
  freestar.debug = window.location.search.indexOf('fsdebug') === -1 ? false : true;
  freestar.config.enabled_slots = [];
  !function(a,b){var c=b.getElementsByTagName("script")[0],d=b.createElement("script"),e="https://a.pub.network/baseball-almanac-com";e+=freestar.debug?"/qa/pubfig.min.js":"/pubfig.min.js",d.async=!0,d.src=e,c.parentNode.insertBefore(d,c)}(window,document);
  freestar.initCallback = function () { (freestar.config.enabled_slots.length === 0) ? freestar.initCallbackCalled = false : freestar.newAdSlots(freestar.config.enabled_slots) }
</script>
<!-- Tag ID: Baseballalmanac_leaderboard_ATF -->
<div align="center" id="Baseballalmanac_leaderboard_ATF">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_leaderboard_ATF", slotId: "Baseballalmanac_leaderboard_ATF" });
</script>
</div>
</div>
<!-- END FREESTAR -->
<div id="wrapper">
<div class="header-container">
<!-- START _site-header-v2.html -->
<div class="header">
<div class="container">
<a class="flex-none" href="/">
<span class="hidden">Baseball Almanac</span>
<img alt="Baseball Almanac" class="logo" src="/images/baseball-almanac-logo.png"/>
</a>
<div class="menu-items hidden" data-menu="">
<div>
<a data-flip="" data-toggle="menu-1" href="#">
                    History
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-1="">
<li><a href="/asgmenu.shtml">All-Star Game</a></li>
<li><a href="/League_Championship_Series.shtml">A.L.C.S. &amp; N.L.C.S.</a></li>
<li><a href="/me_award.shtml">Awards</a></li>
<li><a href="/stadium.shtml">Ballparks</a></li>
<li><a href="/college/colleges.shtml">College Baseball</a></li>
<li><a href="/division_series/division_series.shtml">Division Series</a></li>
<li><a href="/draft/baseball_draft.shtml">Draft</a></li>
<li><a href="/mgrmenu.shtml">Managers</a></li>
<li><a href="/opening_day/opening_day.shtml">Opening Day</a></li>
<li><a href="/teammenu.shtml">Team by Team</a></li>
<li><a href="/umpiresmenu.shtml">Umpires</a></li>
<li><a href="/wild_card/MLB_Wild_Card_Game.shtml">Wild Card Game</a></li>
<li><a href="/ws/wsmenu.shtml">World Series</a></li>
<li><a href="/yearmenu.shtml">Year by Year</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-2" href="#">
                    Players
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-2="">
<li><a href="/fammenu.shtml">Baseball Families</a></li>
<li><a href="/players/baseball_biographies.shtml">Biographies</a></li>
<li><a href="/players/birthplace.php">Birthplace Analysis</a></li>
<li><a href="/featmenu.shtml">Fabulous Feats</a></li>
<li><a href="/frstmenu.shtml">Famous Firsts</a></li>
<li><a href="/graves/baseball_graves.shtml">Grave Sites</a></li>
<li><a href="/hofmenu.shtml">Hall of Fame</a></li>
<li><a href="/players/baseball_interviews.shtml">Interviews</a></li>
<li><a href="/deaths/chart/baseball_player_obituaries.shtml">Obituaries</a></li>
<li><a href="/players/deathplace.php">Place of Death Analysis</a></li>
<li><a href="/players/ballplayer.shtml">The Ballplayers</a></li>
<li><a href="/quomenu.shtml">Quotes</a></li>
<li><a href="/players/baseball_births.php">Year of Birth Analysis</a></li>
<li><a href="/players/baseball_deaths.php">Year of Death Analysis</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-3" href="#">
                    Leaders
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-3="">
<li><a href="/baseball_attendance.shtml">Attendance Data</a></li>
<li><a href="/himenu.shtml">Hitting Charts</a></li>
<li><a href="/pimenu.shtml">Pitching Charts</a></li>
<li><a href="/rb_menu.shtml">Record Books</a></li>
<li><a href="/teamstats/statmaster.php">Statmaster</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-4" href="#">
                    Left Field
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-4="">
<li><a href="/players/Oldest_Living_Baseball_Players.php">500 Oldest Players</a></li>
<li><a href="/automenu.shtml">Autographs</a></li>
<li><a href="/baseball_cards/baseball_cards.php">Baseball Cards</a></li>
<li><a href="/charts/baseball_charts.shtml">Baseball Charts</a></li>
<li><a href="/limenu.shtml">Baseball Lists</a></li>
<li><a href="/bookmenu.shtml">Book Shelf</a></li>
<li><a href="/players/Cups_of_Coffee.php">Cups of Coffee</a></li>
<li><a href="/gam_menu.shtml">Fun &amp; Games</a></li>
<li><a href="/humomenu.shtml">Humor &amp; Jokes</a></li>
<li><a href="/mve_time.shtml">Movie Time</a></li>
<li><a href="/baseball_news.shtml">News Feeds</a></li>
<li><a href="/poems.shtml">Poetry &amp; Song</a></li>
<li><a href="/articles/articles.shtml">Research Articles</a></li>
<li><a href="/baseball_uniform_numbers.shtml">Uniform Numbers</a></li>
<li><a href="/prz_menu.shtml">U.S. Presidents</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-5" href="#">
                    Help
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-5="">
<li><a href="/about.shtml">Advertising</a></li>
<li><a href="/blog/">Blog</a></li>
<li><a href="/feedmenu.shtml">Feedback</a></li>
<li><a href="/mlmstart.shtml">Newsletter</a></li>
<li><a href="/rulemenu.shtml">Rules</a></li>
<li><a href="/scoring.shtml">Scoring</a></li>
<li><a href="/Search.shtml">Search &amp; Find</a></li>
<li><a href="/bstatmen.shtml">Stats 101</a></li>
</ul>
</div>
<div class="extra">
<a class="bg-blue-500" href="https://twitter.com/BaseballAlmanac" target="_blank">
<i class="fab fa-twitter"></i>
                    Follow @BaseballAlmanac
                </a>
<a class="bg-blue-700" href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank">
<i class="fab fa-facebook"></i>
                    Find us on Facebook
                </a>
</div>
</div>
<div class="overlay hidden" data-menu="" data-proxy="menu"></div>
<form class="search-form">
<div class="social">
<div data-search="">
<a href="https://twitter.com/BaseballAlmanac" target="_blank"><i class="fab fa-twitter"></i></a>
<a href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank"><i class="fab fa-facebook-f"></i></a>
<a href="https://www.instagram.com/baseballalmanac/" target="_blank"><i class="fab fa-instagram"></i></a>
<span></span>
</div>
<input class="search hidden" data-search="" data-toggle-focus="" id="q" name="q" placeholder="Custom Search" type="text"/>
<a data-toggle="search" data-toggle-bg=""><i class="fas fa-search"></i></a>
<a class="" data-toggle="menu" data-toggle-bg="" data-toggle-scroll=""><i class="fas fa-bars"></i></a>
</div>
</form>
<script>
            // JavaScript code that should follow the search box code (must part)
  			// add a listener for form submission, i.e. when user hits Enter or clicks to any submit button form has
  			document.querySelector('.search-form').addEventListener('submit', function(e) {
    			// do not actually submit the form, we'll do something else :)
    			e.preventDefault();
    			// read the search query for input tag, i.e. user searches for "django" let's say
    			var q = document.querySelector('input[name="q"]').value;
    			// just proceed if user has typed something
    			if (q.length > 0) {
      				// go to search results page which is search.html here but can be anything you like with "gsc.q" hash parameter equal to search query
      				window.open('/custom_search.shtml?q=' + q, '_self');
    			}
  			});

			// check if there is any text in the search string
			if (window.location.search.length > 0) {
  				// retrieve the "q" keyed search string value
  				var q = window.location.search.substring(1).split('&').filter(function(x) {
    				return x.substring(0, 2) === 'q=';
  				})[0].substring(2);
  				// put the value to the search box if it is not empty
  				if (q.length > 0) {
    				document.querySelector('input[name="q"]').value = q;
  				}
			}
  		</script>
</div>
</div>
<script src="/js/navigation.min.js" type="text/javascript"></script>
<!-- END _site-header-v2.html -->
</div>
<div class="container">
<div class="intro">
<h1>John Montefusco Awards</h1>
<p>What awards did John Montefusco win? This page lists every known baseball award won by John Montefusco in chronological order. Click the award name and up comes a comprehensive history of that award along with every winner of that particular baseball award.</p>
<!-- Are we using flycast?
 -->
</div>
<!-- 	End Intro Box -->
<!-- 	Begin Quote Box -->
<div class="topquote">
<img alt="Baseball Almanac Top Quote" src="/images/typewriter-with-paper.png"/>
<p>"At the end of the game (<a href="../box-scores/boxscore.php?boxid=197409030LAN">09/03/1974</a>), it was the greatest feeling in the world. It was the greatest high that I have ever experienced in my life. The adrenalin that I felt in the ninth inning was incredible. I didn't care who in the hell came up to bat in the ninth inning, because the adrenalin that was rushing through my body would have allowed me to get <a href="/players/player.php?p=mayswi01" title="Willie Mays">Willie Mays</a>, <a href="/players/player.php?p=dimagjo01" title="Joe DiMaggio">Joe DiMaggio</a>, <a href="/players/player.php?p=ruthba01" title="Babe Ruth">Babe Ruth</a>, <a href="/players/player.php?p=mantlmi01" title="Mickey Mantle">Mickey Mantle</a>, <a href="/players/player.php?p=gehrilo01" title="Lou Gehrig">Lou Gehrig</a> and anybody else out in that ninth inning. I felt no pain and like I was 10 feet off the ground in that ninth inning." - John Montefusco in <a href="http://www.amazon.com/exec/obidos/asin/0786413476/baseballalmanac" rel="nofollow">The Pastime in the Seventies: Oral Histories of 16 Major Leaguers</a> (Bill Ballew, McFarland Publishing, 10/01/2002, "13. John Montefusco', Page 150)</p>
</div>
<!-- 	End Quote Box -->
<!-- Begin Sponsor Box -->
<div class="sponsor-box">
<div class="s2nPlayer k-INqKbLx9" data-type="float"></div><script data-type="s2nScript" src="//embed.sendtonews.com/player3/embedcode.js?fk=INqKbLx9&amp;cid=8557&amp;offsetx=0&amp;offsety=0&amp;floatwidth=400&amp;floatposition=bottom-right" type="text/javascript"></script>
</div>
<!-- End Sponsor Box -->
<!-- Begin Page Body -->
<!-- Personal Statistics -->
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header" colspan="2">
<h2>John Montefusco</h2>
<img a="" alt="John " autograph="" count="" montefusco="" on="" src="/players/pics/john_montefusco_autograph.jpg" style="max-width: 215px; margin: 0 auto; border: solid 1px black" the="" topps=""/><p class="grey">John "The Count" Montefusco Autograph on a 1984 Topps (#761)</p>
</td>
</tr>
<tr>
<td class="datacolBox" colspan="2"><table class="post-nav">
<tr>
<td class="postnavoff"><a href="player.php?p=montejo01">Career</a></td>
<td class="postnavoff"><a href="playerpost.php?p=montejo01&amp;ps=asg">All-Star</a></td>
<td class="postnavoff">Wild Card</td>
<td class="postnavoff">Division</td>
<td class="postnavoff">LCS</td>
<td class="postnavoff">World Series</td>
<td class="postnavoff"><a href="trades.php?p=montejo01">Trades</a></td>
<td class="postnavon">Awards</td>
<td class="postnavoff">Videos</td>
<td class="postnavoff"><a href="cards.php?p=montejo01">Cards</a></td>
<td class="postnavoff"><a alt="John Joseph Montefusco, Jr. on MLB.com" href="http://mlb.com/team/player.jsp?player_id=119261"><img alt="mlb" src="/images/mlb_tiny.gif" style="max-width: 20px; margin: 0 auto; display: inline-block; vertical-align:middle;"/></a> <a alt="John Joseph Montefusco, Jr. on ESPN.com" href="http://www.espn.com/mlb/player/_/id/604"><img alt="espn" src="/images/espn_tiny.png" style="max-width: 20px; margin: 0 auto; display: inline-block; vertical-align:middle;"/></a> <a data-fancybox="" data-src="/players/player-address.php?p=montejo01" data-type="iframe" href="javascript:;"><img alt="envelope" src="/images/envelope.png" style="max-width: 20px; margin: 0 auto; display: inline-block; vertical-align:middle;"/></a></td></tr>
</table></td>
</tr>
<tr>
<td class="banner" colspan="2">Biographical Data</td>
</tr>
<tr>
<td class="datacolBox" width="50%">
<table>
<tr>
<td class="biocolpad">Birth Name:</td>
<td class="biocolpad">  John Joseph Montefusco, Jr.</td>
</tr>
<tr>
<td class="biocolpad">Nickname:</td>
<td class="biocolpad">  The Count - The Count of Montefusco</td>
</tr>
<tr>
<td class="biocolpad">Born On:</td>
<td class="biocolpad">  05-25-1950  (Gemini)</td>
</tr>
<tr>
<td class="biocolpad"><a href="birthplace.php"><img alt="Place of Birth Data" border="0" src="/charts/barchart.gif"/></a> Born In:</td>
<td class="biocolpad">  Long Branch, New Jersey</td>
</tr>
<tr>
<td class="biocolpad"><a href="baseball_deaths.php"><img alt="Year of Death Data" border="0" src="/charts/barchart.gif"/></a> Died On:</td>
<td class="biocolpad">  Still Living</td>
</tr>
<tr>
<td class="biocolpad"><a href="deathplace.php"><img alt="Place of Death Data" border="0" src="/charts/barchart.gif"/></a> Died In:</td>
<td class="biocolpad">  Still Living</td>
</tr>
<tr>
<td class="biocolpad">Cemetery:</td>
<td class="biocolpad">  n/a</td>
</tr>
</table>
</td>
<td class="datacolBox" width="50%">
<table>
<tr>
<td class="biocolpad">High School:</td>
<td class="biocolpad" colspan="4">  Long Branch High School (Long Branch, NJ)</td>
</tr>
<tr>
<td class="biocolpad">College:</td>
<td class="biocolpad" colspan="4">  Brookdale Community College</td>
</tr>
<tr>
<td class="biocolpad"><a href="/charts/bats/bats.shtml"><img alt="Batting Stances Chart" border="0" src="/charts/barchart.gif"/></a> Bats:</td>
<td class="biocolpad">  Right</td>
<td width="20"> </td>
<td class="biocolpad"><a href="/charts/throws/throws.shtml"><img alt="Throwing Arms Chart" border="0" src="/charts/barchart.gif"/></a> Throws:</td>
<td class="biocolpad">  Right</td>
</tr>
<tr>
<td class="biocolpad"><a href="/charts/heights/heights.shtml"><img alt="Player Height Chart" border="0" src="/charts/barchart.gif"/></a> Height:</td>
<td class="biocolpad">  6-01</td>
<td> </td>
<td class="biocolpad"><a href="/charts/weights/weights.shtml"><img alt="Player Weight Chart" border="0" src="/charts/barchart.gif"/></a> Weight:</td>
<td class="biocolpad">  180</td>
</tr>
<tr>
<td class="biocolpad">First Game:</td>
<td class="biocolpad" colspan="4">  <a href="../box-scores/boxscore.php?boxid=197409030LAN">09-03-1974</a> (Age 24)</td>
</tr>
<tr>
<td class="biocolpad">Last Game:</td>
<td class="biocolpad" colspan="4">  05-01-1986</td>
</tr>
<tr>
<td class="biocolpad">Draft:</td>
<td class="biocolpad" colspan="4">  Undrafted Free Agent</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!-- Awards Statistics -->
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header" colspan="2">
<h2>John Montefusco</h2>
<p>Awards</p></td>
</tr>
<tr>
<td class="banner" colspan="2">John Montefusco Awards</td>
</tr>
<tr>
<td class="datacolBox">1975</td>
<td class="datacolBox"><a href="https://www.baseball-almanac.com/awards/aw_roy.shtml">Rookie of the Year Award (BBWAA)</a></td>
</tr>
<tr>
<td class="datacolBox">1975</td>
<td class="datacolBox"><a href="https://www.baseball-almanac.com/awards/aw_snrp.shtml">Sporting News Rookie Pitcher of the Year Award</a></td>
</tr>
<tr>
<td class="banner" colspan="2">John Montefusco Awards</td>
</tr>
</table>
</div>
<!-- End Page Body -->
<!-- Begin Related Pages -->
<div class="notes">
<p><a href="/custom_search.shtml?q=John Montefusco"><img alt="search this site" class="ba-notes-icon" src="/images/ba-search-btn.jpg" title="Search this Site"/></a>
<a href="/teamstats/glossary.shtml"><img alt="site glossary" class="ba-notes-icon" src="/images/ba-glossary-btn.jpg" title="Glossary"/></a>
<script>var pfHeaderImgUrl = '';var pfHeaderTagline = '';var pfdisableClickToDel = 0;var pfHideImages = 0;var pfImageDisplayStyle = 'right';var pfDisablePDF = 0;var pfDisableEmail = 0;var pfDisablePrint = 0;var pfCustomCSS = 'https://www.baseball-almanac.com/css/print.css';var pfBtVersion='2';(function(){var js,pf;pf=document.createElement('script');pf.type='text/javascript';pf.src='//cdn.printfriendly.com/printfriendly.js';document.getElementsByTagName('head')[0].appendChild(pf)})();</script><a class="printfriendly" href="https://www.printfriendly.com" onclick="window.print();return false;" style="color:#6D9F00;text-decoration:none;" title="Printer Friendly and PDF"><img alt="Print Friendly and PDF" src="/images/ba-print-friendly-btn.jpg" style="border:none;-webkit-box-shadow:none;box-shadow:none; width: auto; vertical-align: middle; display: inline-block"/></a></p></div>
<!-- End Related Pages -->
<img alt="baseball almanac flat baseball" class="flat-baseball-img" src="/images/ball-red.png"/>
<!-- Tag ID: Baseballalmanac_Medrec_A -->
<div align="center" id="Baseballalmanac_Medrec_A">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_Medrec_A", slotId: "Baseballalmanac_Medrec_A" });
</script>
</div>
<div class="fast-facts">
<h3><img alt="baseball almanac fast facts" src="/images/fast-facts-logo.png"/></h3>
<p>Do you enjoy reading and learning about <a href="/me_award.shtml">baseball awards</a>, their histories, details about each winner, answers to trivia to and other similar data? If so check out our truly comprehensive <a href="/me_award.shtml">baseball awards</a> section today.</p>
<p>If you are aware of any other unusual, less known, not as publicized baseball awards won by <a href="/players/player.php?p=montejo01">John Montefusco</a> please <a href="/feedmenu.shtml">contact us</a> with details.</p>
<p>If you are the type of fan who truly enjoys intelligent debate and thinks you can teach others why John Montefusco did or did not deserve the <a href="https://www.baseball-almanac.com/awards/aw_roy.shtml">Rookie of the Year Award (BBWAA)</a> in 1975, join us on <a href="http://www.baseball-fever.com/">Baseball Fever</a>.</p>
</div>
</div>
<div class="footer">
<!-- START _footer-v2.txt" -->
<!-- Tag ID: Baseballalmanac_leaderboard_BTF -->
<div align="center" id="Baseballalmanac_leaderboard_BTF">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_leaderboard_BTF", slotId: "Baseballalmanac_leaderboard_BTF" });
</script>
</div>
<div class="container">
<div class="notes">
<a href="/"><img alt="Baseball Almanac" src="/images/baseball-almanac-logo.png" style="max-width: 180px; margin; 0 auto"/></a>
<p>Where what happened yesterday<br/> is being preserved today.</p>
<div class="social">
<a href="https://twitter.com/BaseballAlmanac" target="_blank"><i class="fab fa-twitter"></i></a>
<a href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank"><i class="fab fa-facebook-f"></i></a>
<a href="https://www.instagram.com/baseballalmanac/" target="_blank"><i class="fab fa-instagram"></i></a>
</div>
</div>
<div class="links">
<div>
<span>Stats</span>
<a href="/me_award.shtml">Awards</a>
<a href="/featmenu.shtml">Fabulous Feats</a>
<a href="/frstmenu.shtml">Famous Firsts</a>
<a href="/hofmenu.shtml">Hall of Fame</a>
<a href="/himenu.shtml">Hitting Charts</a>
<a href="/limenu.shtml">Legendary Lists</a>
<a href="/pimenu.shtml">Pitching Charts</a>
<a href="/rb_menu.shtml">Record Books</a>
<a href="/rulemenu.shtml">Rules</a>
<a href="/scoring.shtml">Scoring</a>
<a href="/teamstats/statmaster.php">Statmaster</a>
<a href="/bstatmen.shtml">Stats 101</a>
<a href="/yearmenu.shtml">Year by Year</a>
</div>
<div>
<span>People</span>
<a href="/automenu.shtml">Autographs</a>
<a href="/players/ballplayer.shtml">Ballplayers</a>
<a href="/fammenu.shtml">Baseball Families</a>
<a href="/players/baseball_interviews.shtml">Interviews</a>
<a href="/mgrmenu.shtml">Managers</a>
<a href="/deaths/chart/baseball_player_obituaries.shtml">Obituaries</a>
<a href="/quomenu.shtml">Quotes</a>
<a href="/teammenu.shtml">Team by Team</a>
<a href="/umpiresmenu.shtml">Umpires</a>
<a href="/prz_menu.shtml">US Presidents</a>
</div>
<div>
<span>Places</span>
<a href="/asgmenu.shtml">All-Star Game</a>
<a href="/stadium.shtml">Ballparks</a>
<a href="/division_series/division_series.shtml">Division Series</a>
<a href="/graves/baseball_graves.shtml">Grave Sites</a>
<a href="/League_Championship_Series.shtml">LCS</a>
<a href="/opening_day/opening_day.shtml">Opening Day</a>
<a href="/ws/wsmenu.shtml">World Series</a>
</div>
<div>
<span>Other</span>
<a href="/about.shtml">Advertising</a>
<a href="/baseball_cards/baseball_cards.php">Baseball Cards</a>
<a href="/bookmenu.shtml">Book Shelf</a>
<a href="/feedmenu.shtml">Feedback</a>
<a href="/gam_menu.shtml">Fun &amp; Games</a>
<a href="/humomenu.shtml">Humor &amp; Jokes</a>
<a href="/mve_time.shtml">Movie Time</a>
<a href="/mlmstart.shtml">Newsletter</a>
<a href="/baseball_news.shtml">News Feeds</a>
<a href="/poems.shtml">Poetry &amp; Song</a>
<a href="/privacy-policy.shtml">Privacy Policy</a>
<a href="/Search.shtml">Search &amp; Find</a>
<a href="/support.shtml">Support</a>
</div>
</div>
<div class="copyright">
<div class="legal">
<p>Copyright 1999-<script type="text/javascript">
			copyright=new Date();
			update=copyright.getFullYear();
			document.write(update);
		</script>. All Rights Reserved by Baseball Almanac, Inc.<br/>Hosted by <a href="https://www.hosting4less.com" rel="nofollow" target="_blank">Hosting 4 Less</a>. Part of the <a href="https://www.baseball-almanac.com/">Baseball Almanac</a> Family</p>
</div>
<div class="external">
<a href="http://www.755homeruns.com/" rel="nofollow" target="_blank">755 Home Runs</a>
<a href="http://www.baseball-boxscores.com/" rel="nofollow" target="_blank">Baseball Box Scores</a>
<a href="http://www.baseball-fever.com/" rel="nofollow" target="_blank">Baseball Fever</a>
<a href="http://www.todayinbaseballhistory.com/" rel="nofollow" target="_blank">Today in Baseball History</a>
</div>
</div>
</div><br/><br/><br/><br/><br/>
</div>
<!-- END _footer-v2.txt" -->
</div>
</body>
</html>
