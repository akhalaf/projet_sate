<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es-ES" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>
trikua.com | Botellas Lifefactory, Portabebés Bykay, Pañales Gnappies | trikua.com</title>
<script>(function(d, s, id){
				 var js, fjs = d.getElementsByTagName(s)[0];
				 if (d.getElementById(id)) {return;}
				 js = d.createElement(s); js.id = id;
				 js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
				 fjs.parentNode.insertBefore(js, fjs);
			   }(document, 'script', 'facebook-jssdk'));</script>
<!-- This site is optimized with the Yoast SEO plugin v6.2 - https://yoa.st/1yg?utm_content=6.2 -->
<meta content="Trikua Distributors es una tienda online y distribuidora de las marcas de biberones y botellas Lifefactory, portabebés Bykay, y pañales de tela Gnappies." name="description"/>
<link href="https://www.trikua.com/" rel="canonical"/>
<meta content="es_ES" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="trikua.com | Botellas Lifefactory, Portabebés Bykay, Pañales Gnappies" property="og:title"/>
<meta content="Trikua Distributors es una tienda online y distribuidora de las marcas de biberones y botellas Lifefactory, portabebés Bykay, y pañales de tela Gnappies." property="og:description"/>
<meta content="https://www.trikua.com/" property="og:url"/>
<meta content="trikua.com" property="og:site_name"/>
<script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/www.trikua.com\/","name":"trikua.com","potentialAction":{"@type":"SearchAction","target":"https:\/\/www.trikua.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<!-- / Yoast SEO plugin. -->
<link href="//ws.sharethis.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.trikua.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.8.15"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b!==c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.trikua.com/wp-content/plugins/cookie-notice/css/front.min.css?ver=4.8.15" id="cookie-notice-front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Indie+Flower&amp;ver=4.8.15" id="simple-share-buttons-adder-indie-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.trikua.com/wp-content/plugins/sitemap/css/page-list.css?ver=4.3" id="page-list-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//www.trikua.com/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=3.1.1" id="woocommerce-layout-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//www.trikua.com/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=3.1.1" id="woocommerce-smallscreen-css" media="only screen and (max-width: 768px)" rel="stylesheet" type="text/css"/>
<link href="//www.trikua.com/wp-content/plugins/woocommerce/assets/css/woocommerce.css?ver=3.1.1" id="woocommerce-general-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.trikua.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.trikua.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var cnArgs = {"ajaxurl":"https:\/\/www.trikua.com\/wp-admin\/admin-ajax.php","hideEffect":"fade","onScroll":"","onScrollOffset":"100","cookieName":"cookie_notice_accepted","cookieValue":"TRUE","cookieTime":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"","cache":""};
/* ]]> */
</script>
<script src="https://www.trikua.com/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.2.41" type="text/javascript"></script>
<script id="st_insights_js" src="https://ws.sharethis.com/button/st_insights.js?publisher=4d48b7c5-0ae3-43d4-bfbe-3ff8c17a8ae6&amp;product=simpleshare" type="text/javascript"></script>
<script src="//www.trikua.com/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70" type="text/javascript"></script>
<script src="//www.trikua.com/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script src="//www.trikua.com/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=3.1.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var add_fee_vars = {"add_fee_ajaxurl":"https:\/\/www.trikua.com\/wp-admin\/admin-ajax.php","add_fee_nonce":"b0b2cb5545","alert_ajax_error":"An internal server error occured in processing a request. Please try again or contact us. Thank you. "};
/* ]]> */
</script>
<script src="https://www.trikua.com/wp-content/plugins/woocommerce-additional-fees/js/wc_additional_fees.js?ver=4.8.15" type="text/javascript"></script>
<link href="https://www.trikua.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.trikua.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.trikua.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<link href="https://www.trikua.com/" rel="shortlink"/>
<link href="https://www.trikua.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.trikua.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://www.trikua.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.trikua.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
<script async="" src="/wp-includes/js/wp-api-minjs.js?v=1"></script>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.trikua.com/wp-content/themes/trikua/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.trikua.com/xmlrpc.php" rel="pingback"/>
<link href="https://fonts.googleapis.com/css?family=Arvo:400" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Asul:400,700" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,200" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600" rel="stylesheet" type="text/css"/>
<script defer="" src="https://www.trikua.com/wp-content/themes/trikua/js/jquery.flexslider-min.js"></script>
<script defer="" src="https://www.trikua.com/wp-content/themes/trikua/js/zscripts.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-70818006-1', 'auto');
  ga('create', 'UA-78650914-6', {'name':'b'});
  ga('send', 'pageview');
  ga('b.send', 'pageview');
</script>
</head>
<body class="home page-template page-template-page-tienda page-template-page-tienda-php page page-id-5">
<div id="wrapper">
<div class="margin">
<div id="header100">
<div id="header">
<div class="deco uno"></div>
<div class="deco dos"></div>
<div class="deco tres"></div>
<div class="deco cuatro"></div>
<div class="deco cinco"></div>
<div class="deco seis"></div>
<div class="deco uno"></div>
<div class="deco dos"></div>
<div class="deco tres"></div>
<div class="deco cuatro"></div>
<div class="deco cinco"></div>
<div class="deco seis"></div>
<div class="deco uno final"></div>
<div class="buscar">
<div class="widget" id="woocommerce_product_search-2"><form action="https://www.trikua.com/" class="woocommerce-product-search" method="get" role="search">
<label class="screen-reader-text" for="woocommerce-product-search-field-0">Buscar por:</label>
<input class="search-field" id="woocommerce-product-search-field-0" name="s" placeholder="Buscar productos…" type="search" value=""/>
<input type="submit" value="Buscar"/>
<input name="post_type" type="hidden" value="product"/>
</form>
</div> </div>
<a href="/"><div class="logo">Trikua.</div></a>
<div class="showhide"></div>
<div class="font1"><div class="menu-menu-secundario-container"><ul class="menu" id="menu-menu-secundario"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-42" id="menu-item-42"><a href="https://www.trikua.com/nosotros/">Trikua</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-3295" id="menu-item-3295"><a href="https://www.trikua.com/">Tienda online</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-has-children menu-item-3296" id="menu-item-3296"><a href="/">Marcas</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3297" id="menu-item-3297"><a href="/tienda/lifefactory">Lifefactory</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3298" id="menu-item-3298"><a href="/tienda/bykay">Bykay</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3299" id="menu-item-3299"><a href="/tienda/gnappies">Gnappies</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4794" id="menu-item-4794"><a href="/tienda/nvey-baby">Nvey Baby</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3856" id="menu-item-3856"><a href="https://www.trikua.com/blog/">Blog</a></li>
</ul></div></div>
<div class="right">
<a class="login" href="/mi-cuenta" title="Mi cuenta">/mi-cuenta</a>
<div class="buscar"><div class="boton_buscar">Buscar</div></div>
<a class="cart-contents font1 cero" href="https://www.trikua.com/carro/" title="Ver carrito"><div class="conteo uno">0</div></a>
<div class="pause" style="width:30px; height:30px; background:#fff; cursor:pointer; float:right; display:none;"></div>
</div>
</div>
<div style="clear:both"></div>
</div><!--header -->
<div id="slider100">
<div id="slider">
<div class="flexslider">
<div class="slides">
<!--	<li><a href="/tienda/lifefactory" title="Lifefactory"><img src="slides/trikua-tienda-online-botellas-vasos-cristal-lifefactory-stocks.jpg" /></a> </li> -->
<li><a href="/tienda/lifefactory" title="Lifefactory"><img src="slides/trikua-lifefactory-botellas-cristal-landing-01.jpg"/></a> </li>
<li><a href="/tienda/bykay" title="ByKay"><img src="slides/trikua-bykay-portabebes-landing-00.jpg"/></a> </li>
<li><a href="/tienda/gnappies" title="gNappies"><img src="slides/trikua-gnappies-panales-de-tela-landing-00.jpg"/></a> </li>
<li><a href="/tienda/nvey-baby" title="Nvey"><img src="slides/trikua-nvey-slide-01.jpg"/></a> </li>
</div>
</div>
</div>
</div>
<div id="container100">
<div class="container single portada">
<!--banners -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -->
<div class="banners">
<div class="banner uno"><a href="/tienda/lifefactory" title="Lifefactory">
<img src="slides/trikua-lifefactory-botellas-cristal-banner-01.jpg"/></a>
<div class="descripcion">
<div class="texto font2">
   		Lifefactory es la marca saludable de botellas, biberones, vasos, y tupers de cristal libres de substancias tóxicas plásticas.
   		</div>
</div>
</div>
<div class="banner dos"><a href="/tienda/bykay" title="Bykay">
<img src="slides/trikua-bykay-portabebes-banner-01.jpg"/></a>
<div class="descripcion">
<div class="texto font2">
   		Portabebés Bykay de la holandesa Kay Poelen, lineas de porteo a la moda, tradicion ancestral actualizada a nuestros tiempos.
   		</div>
</div>
</div>
<div class="banner tres"><a href="/tienda/gnappies" title="gNappies">
<img src="slides/trikua-gnappies-panales-banner-01.jpg"/></a>
<div class="descripcion">
<div class="texto font2">
   		Los pañales lavables gNappies son la revolución actualizada de volver a los métodos que usaban nuestras abuelas.
   		</div>
</div>
</div>
<div class="banner cuatro"><a href="/tienda/nvey-baby" title="Nvey">
<img src="slides/trikua-banner-nvey.jpg"/></a>
<div class="descripcion">
<div class="texto font2">
   		Nvey Baby presenta una gama de cosmética infantil de productos formulados con ingredientes suaves y seguros para bebés y niños.
   		</div>
</div>
</div>
<div class="presentacion">
<div class="texto font2"><p>Trikua Distributors somos una empresa joven asentada en el País Vasco, que distribuye y vende online productos relacionados con la alimentación saludable, la crianza respetuosa, y cualquier producto que nos parezca útil, eco-friendly y positivo.</p>
</div>
</div>
<div style="clear:both"></div>
</div>
<!--content -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -->
<div class="sep" style="clear:both"></div>
<div class="single" id="contenido">
<div class="entrada">
<div class="portada">
<div class="woocommerce columns-0"></div></div>
</div>
</div>
</div><!-- container100 -->
</div><!-- container -->
<div style="clear:both"></div>
<div class="cuatro" id="container100">
<div class="container">
<div class="font1" id="footer">
<div class="banner una">
<div class="fcontent">
<p class="titulo">Comprar en trikua.com</p>
<ul>
<li><a href="/envios-pagos-y-devoluciones" rel="noindex,nofollow">Envíos, pagos y devoluciones</a></li>
<li><a href="/pago-seguro" rel="noindex,nofollow">Pago seguro</a></li>
<li><a href="/aviso-legal" rel="noindex,nofollow">Avíso legal</a></li>
<li><a href="/politica-de-privacidad-y-cookies" rel="noindex,nofollow">Política de privacidad y Cookies</a></li>
<li class="last"><a href="/sitemap" rel="noindex,nofollow">Mapa del sitio</a></li>
</ul>
</div>
</div>
<div class="banner dos">
<div class="fcontent">
<p class="titulo">Contacto</p>
<ul>
<li><a href="mailto:info@trikua.com">info@trikua.com</a></li>
<li>tel. 635 700 986</li>
<li><a href="https://www.facebook.com/LifefactoryEspana/?fref=ts" title="Facebook de Lifefactory">Facebook</a></li>
<li><a href="https://www.instagram.com/lifefactoryspain/" title="Instagram Lifefactory">Instagram</a></li>
<li><a href="https://twitter.com/lifefactory?lang=es" title="Twitter Lifefactory">Twitter</a></li>
</ul>
</div>
</div>
<div class="banner tres">
<div class="fcontent">
<p class="titulo">Mi cuenta</p>
<li><a href="/mi-cuenta/" title="Identificarse">Identificarse</a></li>
<li><a href="/mi-cuenta/" title="Identificarse">Mi cuenta</a></li>
<li>Finalizar Compra</li>
<li><a href="/mi-cuenta/orders/" target="Pedidos">Pedidos</a></li>
<li><a href="/mi-cuenta/" title="Salir">salir</a></li>
</div>
</div>
</div>
</div><!-- container100 -->
</div><!-- container -->
<div id="subfooter">
<div class="container"><a href="/"><div class="logo">Trikua.</div></a>
<p class="copy font1">© Trikua. 2021 </p>
</div>
</div>
<script src="https://www.trikua.com/wp-content/plugins/simple-share-buttons-adder/js/ssba.js?ver=4.8.15" type="text/javascript"></script>
<script type="text/javascript">
Main.boot( [] );
</script>
<script type="text/javascript">
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"Ver carrito","cart_url":"https:\/\/www.trikua.com\/carro\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script src="//www.trikua.com/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=3.1.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","fragment_name":"wc_fragments_c4807d2d31d9aea9c7b6862566433050"};
/* ]]> */
</script>
<script src="//www.trikua.com/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=3.1.1" type="text/javascript"></script>
<script src="https://www.trikua.com/wp-includes/js/wp-embed.min.js?ver=4.8.15" type="text/javascript"></script>
<div class="cn-bottom bootstrap" id="cookie-notice" role="banner" style="color: #fff; background-color: #000;"><div class="cookie-notice-container"><span id="cn-notice-text">Utilizamos cookies para asegurar que damos la mejor experiencia al usuario en nuestro sitio web. Si continúa utilizando este sitio asumiremos que está de acuerdo.</span><a class="cn-set-cookie button bootstrap" data-cookie-set="accept" href="#" id="cn-accept-cookie">Estoy de acuerdo</a>
</div>
</div>
<!--footer -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -->
</div>
</div> <!--wrapper -->
</body>
</html>
