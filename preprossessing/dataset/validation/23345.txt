<html lang="it">
<head>
<meta charset="utf-8"/>
<title>Visitor anti-robot validation</title>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<style>
      * {
        box-sizing: border-box;
      }

      html,
      body {
        margin: 0;
        padding: 0;
        font-family: "Open Sans", "Segoe UI", Tahoma, sans-serif;
        font-size: 14px;
        line-height: 20px;
        -webkit-font-smoothing: antialiased;
      }

      body {
        background-color: #f0f0f0;
        color: #212121;
      }

      .header {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 150px;
        text-align: center;
      }

      .container {
        margin-right: auto;
        margin-left: auto;
        padding-left: 15px;
        padding-right: 15px;
        width: 100%;
        max-width: 800px;
      }

      .card {
        padding: 16px;
        margin-bottom: 20px;
        background: #fff;
        box-shadow: 0 1px 3px 0 rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 2px 1px -1px rgba(0,0,0,.12);
      }

      h3 {
        margin: 0;
        font-size: 18px;
      }

      p {
        font-size: 12px;
        line-height: 17px;
      }

      input[type=submit] {
        display: block;
        padding: 8px 15px;
        margin-top: 10px;
        border: none;
        font-size: 14px;
        color: #fff;
        background: #1c77ba;
        -webkit-appearance: button;
        cursor: pointer;
      }
    </style>
</head>
<body>
<header class="header">
<h1 style="display: none;">Host.it</h1>
<img alt="Host.it" src="./host-logo.svg"/>
</header>
<div class="container">
<div class="card">
<div>
<h3>Gentile Visitatore,</h3>
<p>
            Per continuare a navigare e per aiutarci a combattere la criminalità informatica, ti preghiamo di risolvere il CAPTCHA che vedi sotto.
          </p>
<p><strong></strong></p>
</div>
<div class="right">
<form action="/verify.php" method="post">
<script async="" defer="" src="https://recaptcha.net/recaptcha/api.js"></script>
<div class="g-recaptcha" data-sitekey="6LdvpRAUAAAAAJkr4psZnXC4TeOEVPwP_bEQrP24"></div>
<input name="origin_url" type="hidden" value="https://www.2pay.it/images/contract/GD/index.php	
"/>
<input type="submit" value="Elimina il mio IP"/>
</form>
</div>
<div class="clear"></div>
<div>
<h3>Perché è necessario?</h3>
<p>
            Il tuo indirizzo IP (210.75.253.169) è stato bloccato per ragioni di sicurezza.
            E' probabile che il tuo indirizzo IP sia stato usato in precedenza per la violazione delle norme di sicurezza del server.
          </p>
<p>
            Dobbiamo assicurarci che questa non sia una visita da parte di un robot automatico.
            Dopo la verifica della tua identità attraverso il CAPTCHA, il tuo IP verrà rimosso dalla greylist di Cerbero.
          </p>
<p>
            Grazie.
            <br/>
<strong>Il Team di Host.it</strong>
</p>
<!-- <hr />
          <pre>
  Remote address: 210.75.253.169
  URI: /images/contract/GD/index.php%09%0A
  Agent: python-requests/2.22.0
                              </pre> -->
</div>
</div>
</div>
</body>
</html>
