<!DOCTYPE html>
<html lang="en-GB" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="telephone=no" name="format-detection"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="NOODP" name="ROBOTS"/>
<meta content="noydir" name="robots"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-type"/>
<title>3G | Three Store and Blog | Latest Mobile Phone and SIM Only deals</title>
<meta content="Buy mobile phones, SIM cards, Tablets and Mobile Broadband on Three network. Free phones from £7, SIM plans from £9 and dongles from just £7 a month." name="description"/>
<link href="https://3g.co.uk" rel="canonical"/>
<!-- For Chrome for Android: -->
<link href="/img/touch-icon-192x192.png" rel="icon" sizes="192x192"/>
<!-- For iPhone 6 Plus with @3× display: -->
<link href="/img/apple-touch-icon-180x180-precomposed.png" rel="apple-touch-icon-precomposed" sizes="180x180"/>
<!-- For iPad with @2× display running iOS ≥ 7: -->
<link href="/img/apple-touch-icon-152x152-precomposed.png" rel="apple-touch-icon-precomposed" sizes="152x152"/>
<!-- For iPad with @2× display running iOS ≤ 6: -->
<link href="/img/apple-touch-icon-144x144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144"/>
<!-- For iPhone with @2× display running iOS ≥ 7: -->
<link href="/img/apple-touch-icon-120x120-precomposed.png" rel="apple-touch-icon-precomposed" sizes="120x120"/>
<!-- For iPhone with @2× display running iOS ≤ 6: -->
<link href="/img/apple-touch-icon-114x114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114"/>
<!-- For the iPad mini and the first- and second-generation iPad (@1× display) on iOS ≥ 7: -->
<link href="/img/apple-touch-icon-76x76-precomposed.png" rel="apple-touch-icon-precomposed" sizes="76x76"/>
<!-- For the iPad mini and the first- and second-generation iPad (@1× display) on iOS ≤ 6: -->
<link href="/img/apple-touch-icon-72x72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72"/>
<!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
<link href="/img/apple-touch-icon-precomposed.png" rel="apple-touch-icon-precomposed"/><!-- 57×57px -->
<link href="/favicon.png" rel="shortcut icon" type="image/x-icon"/>
<link href="/stylesheets/screen.css?v=202101130008" rel="stylesheet" type="text/css"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@3gcouk" name="twitter:creator"/>
<meta content="3G" name="twitter:site"/>
<meta content="3G" name="twitter:title"/>
<meta content="Save with a SIM Only deal. Exclusive deals you won't find elsewhere. Pay As You Go. Pay Monthly. Unlimited data plans. Highlights: In Business Since 1999, Independent Mobile Phone Blog." name="twitter:description"/>
<meta content="https://3g.co.uk/img/3g_logo.jpg" name="twitter:image"/>
<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-5905699-1', 'auto');
  ga('send', 'pageview');

</script>
<script type="application/ld+json">
	        {
	        "@context": "http://schema.org",
	        "@type": "Organization",
	        "name": "3G",
	        "logo": "https://3g.co.uk/img/3g_logo.jpg",
	        "url": "https://3g.co.uk",
	        "address": {
		    "@type": "PostalAddress",
		    "addressLocality": "Newport",
		    "addressRegion": "Wales",
		    "streetAddress": "16 Gold Tops"
		        },
		    "description": "Buy mobile phones, SIM cards, Tablets and Mobile Broadband on Three network. Free phones from £7, SIM plans from £9 and dongles from just £7 a month.",             
		    "sameAs" : [ 
		    "https://www.facebook.com/3g.co.uk/",
			"https://twitter.com/3gcouk"]
		        }
		</script>
</head>
<body>
<!--
-->
<div class="headerRow">
<div class="container">
<div class="header">
<div class="row">
<div class="col-sm-3">
<div class="logo">
<a href="https://3g.co.uk"><img alt="3G.co.uk" class="responsive" src="/img/3g_logo.jpg"/></a>
</div>
</div>
<div class="col-sm-9 hidden-xs text-right">
<span class="phone"><i class="fa fa-globe"></i>Online since 1999</span>
<span class="phone"><i class="fa fa-lock"></i>Secure site</span>
</div>
</div>
</div>
</div>
</div>
<div class="navRow">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="nav" style="display: none;">
<div class="search-show">
<div class="search-bar">
<form action="/search" method="post">
<input class="search" name="q" placeholder="Search..." type="text" value=""/>
<button name="submit">
<span class="hidden-xs">Go</span>
</button>
</form>
</div>
</div>
<ul class="nav" id="menu">
<li class="home"><a class="underlineMenu active" href="/"><i class="fa fa-home"></i></a></li>
<li><a class="underlineMenu " href="/deals">Deals <span class="hidden-lg hidden-md hidden-sm">&gt;</span></a>
</li><li><a class="underlineMenu " href="/sim-only">Sim Only <span class="hidden-lg hidden-md hidden-sm">&gt;</span></a>
<div class="mm">
<div class="row">
<div class="col-sm-3">
<span class="title">SIM Only for Phones</span>
<a href="/sim-only/contract">Pay Monthly</a>
<a href="/sim-only/pay-as-you-go">Pay As You Go</a>
</div>
<div class="col-sm-3">
<span class="title">Data Only SIM</span>
<a href="/data-only-sim/contract">Pay Monthly</a>
<a href="/data-only-sim/pay-as-you-go">Pay As You Go</a>
<a href="https://3g.co.uk/three-data-reward-sim">Data Reward SIM</a>
</div>
<div class="col-sm-3">
<span class="title">More</span>
<a href="/free-sim-cards">Free SIM Card</a>
</div>
</div>
</div>
<ul class="mobileSub">
<li>SIM Only for Phones</li>
<li><a href="/sim-only/contract">Pay Monthly</a></li>
<li><a href="/sim-only/pay-as-you-go">Pay As You Go</a></li>
<li>Data Only SIM</li>
<li><a href="/data-only-sim/contract">Pay Monthly</a></li>
<li><a href="/data-only-sim/pay-as-you-go">Pay As You Go</a></li>
<li><a href="https://3g.co.uk/three-data-reward-sim">Data Reward SIM</a></li>
<li>More</li>
<li><a href="/free-sim-cards">Free SIM Card</a></li>
</ul>
</li>
<li><a class="underlineMenu " href="/mobile-phones">Phones <span class="hidden-lg hidden-md hidden-sm">&gt;</span></a>
<div class="mm">
<div class="row">
<div class="col-sm-3">
<span class="title">Phone Plans</span>
<a href="/mobile-phones/contract">Pay Monthly</a>
<a href="https://3g.co.uk/mobile-phones/pay-as-you-go?order=price_asc">Pay As You Go</a>
</div>
<div class="col-sm-3">
<span class="title">Popular phones</span>
<a href="https://3g.co.uk/mobile-phones/apple-iphone-12-64gb-red">iPhone 12</a>
<a href="https://3g.co.uk/mobile-phones/apple-iphone-12-pro-128gb-gold">iPhone 12 Pro</a>
<a href="https://3g.co.uk/mobile-phones/apple-iphone-12-pro-max-128gb-gold">iPhone 12 Pro Max</a>
<a href="https://3g.co.uk/mobile-phones/apple-iphone-11-64gb-red">iPhone 11</a>
<a href="https://3g.co.uk/mobile-phones/samsung-galaxy-s20-5g-128gb-cloud-blue">Samsung Galaxy S20 5G</a>
</div>
<div class="col-sm-3">
<span class="title">Popular</span>
<a href="https://3g.co.uk/5g-phones">5G Phones</a>
<a href="https://3g.co.uk/mobile-phones/contract/apple">Apple iPhones</a>
<a href="https://3g.co.uk/mobile-phones/contract/samsung">Samsung Phones</a>
</div>
</div>
</div>
<ul class="mobileSub">
<li>Phone Plans</li>
<li><a href="/mobile-phones/contract">Pay Monthly</a></li>
<li><a href="https://3g.co.uk/mobile-phones/pay-as-you-go?order=price_asc">Pay As You Go</a></li>
<li>Popular phones</li>
<li><a href="https://3g.co.uk/mobile-phones/apple-iphone-12-64gb-red">iPhone 12</a></li>
<li><a href="https://3g.co.uk/mobile-phones/apple-iphone-12-pro-128gb-gold">iPhone 12 Pro</a></li>
<li><a href="https://3g.co.uk/mobile-phones/apple-iphone-12-pro-max-128gb-gold">iPhone 12 Pro Max</a></li>
<li><a href="https://3g.co.uk/mobile-phones/apple-iphone-11-64gb-red">iPhone 11</a></li>
<li><a href="https://3g.co.uk/mobile-phones/samsung-galaxy-s20-5g-128gb-cloud-blue">Samsung Galaxy S20 5G</a></li>
<li>Popular</li>
<li><a href="https://3g.co.uk/5g-phones">5G Phones</a></li>
<li><a href="https://3g.co.uk/mobile-phones/contract/apple">Apple iPhones</a></li>
<li><a href="https://3g.co.uk/mobile-phones/contract/samsung">Samsung Phones</a></li>
</ul>
</li>
<li><a class="underlineMenu " href="/mobile-broadband">Mobile Broadband <span class="hidden-lg hidden-md hidden-sm">&gt;</span></a>
<div class="mm">
<div class="row">
<div class="col-sm-3">
<span class="title">Mobile Broadband Plans</span>
<a href="/mobile-broadband/contract">Pay Monthly</a>
<a href="/mobile-broadband/pay-as-you-go">Pay As You Go</a>
</div>
<div class="col-sm-3">
<span class="title">Popular</span>
<a href="https://3g.co.uk/mobile-broadband/huawei-e5573bs-322-4g-mobile-wi-fi">Mobile WiFi (MiFi)</a>
<a href="/unlimited-mobile-broadband">Unlimited Mobile Broadband</a>
<a href="/data-only-sim/contract">Data Only SIM</a>
</div>
</div>
</div>
<ul class="mobileSub">
<li>Mobile Broadband Plans</li>
<li><a href="/mobile-broadband/contract">Pay Monthly</a></li>
<li><a href="/mobile-broadband/pay-as-you-go">Pay As You Go</a></li>
<li>Popular</li>
<li><a href="https://3g.co.uk/mobile-broadband/huawei-e5573bs-322-4g-mobile-wi-fi">Mobile WiFi (MiFi)</a></li>
<li><a href="/unlimited-mobile-broadband">Unlimited Mobile Broadband</a></li>
<li><a href="/data-only-sim/contract">Data Only SIM</a></li>
</ul>
</li>
<li><a class="underlineMenu " href="/home-broadband">Home Broadband <span class="hidden-lg hidden-md hidden-sm">&gt;</span></a>
</li><li><a class="underlineMenu " href="/tablets">Tablets <span class="hidden-lg hidden-md hidden-sm">&gt;</span></a>
<div class="mm">
<div class="row">
<div class="col-sm-3">
<span class="title">Tablet Plans</span>
<a href="https://3g.co.uk/tablets/contract">Pay Monthly</a>
<a href="/tablets/pay-as-you-go">Pay As You Go</a>
</div>
<div class="col-sm-3">
<span class="title">More</span>
<a href="https://3g.co.uk/tablets/contract?order=price_asc">Cheapest Tablets</a>
<a href="/data-only-sim/contract">Tablet SIM Only Deals</a>
</div>
</div>
</div>
<ul class="mobileSub">
<li>Tablet Plans</li>
<li><a href="https://3g.co.uk/tablets/contract">Pay Monthly</a></li>
<li><a href="/tablets/pay-as-you-go">Pay As You Go</a></li>
<li>More</li>
<li><a href="https://3g.co.uk/tablets/contract?order=price_asc">Cheapest Tablets</a></li>
<li><a href="/data-only-sim/contract">Tablet SIM Only Deals</a></li>
</ul>
</li>
<li><a class="underlineMenu " href="https://3g.co.uk/unlimited-data-plans">Unlimited Data <span class="hidden-lg hidden-md hidden-sm">&gt;</span></a>
</li><li><a class="underlineMenu " href="#">Blog <span class="hidden-lg hidden-md hidden-sm">&gt;</span></a>
<div class="mm">
<div class="row">
<div class="col-sm-3">
<span class="title">Categories</span>
<a href="/reviews">Reviews</a>
<a href="/news">News</a>
<a href="/guides">Guides</a>
</div>
</div>
</div>
<ul class="mobileSub">
<li>Categories</li>
<li><a href="/reviews">Reviews</a></li>
<li><a href="/news">News</a></li>
<li><a href="/guides">Guides</a></li>
</ul>
</li>
<li><a class="underlineMenu " href="/help">Help <span class="hidden-lg hidden-md hidden-sm">&gt;</span></a>
<div class="mm">
<div class="row">
<div class="col-sm-3">
<span class="title">Network</span>
<a href="https://3g.co.uk/three-coverage">Coverage Checker</a>
<a href="https://3g.co.uk/guides/how-to-improve-your-three-signal">Improve Signal</a>
</div>
<div class="col-sm-3">
<span class="title">Existing 3 Customers</span>
<a href="/my3">My3 Account</a>
<a href="/order-by-phone">Customer Service</a>
<a href="https://3g.co.uk/existing-customer-deals">Existing Customer Deals</a>
<a href="/top-up">Top-Up</a>
<a href="https://3g.co.uk/upgrade">Upgrades</a>
</div>
<div class="col-sm-3">
<span class="title">Three Plans &amp; Services</span>
<a href="/three-advanced-plans">Advanced Plans</a>
<a href="/feel-at-home">Roaming</a>
<a href="/tethering">Tethering</a>
<a href="https://3g.co.uk/guides/three-go-binge">Go Binge</a>
<a href="https://3g.co.uk/news/three-4g-super-voice-everything-you-need-to-know">4G Super-Voice</a>
</div>
<div class="col-sm-3">
<span class="title">Guides</span>
<a href="/keep-your-number">Keep Your Number</a>
<a href="/guides/unlocking-a-three-phone-tablet">Unlocking</a>
<a href="/store-locator">Store Locator</a>
</div>
</div>
</div>
<ul class="mobileSub">
<li>Network</li>
<li><a href="https://3g.co.uk/three-coverage">Coverage Checker</a></li>
<li><a href="https://3g.co.uk/guides/how-to-improve-your-three-signal">Improve Signal</a></li>
<li>Existing 3 Customers</li>
<li><a href="/my3">My3 Account</a></li>
<li><a href="/order-by-phone">Customer Service</a></li>
<li><a href="https://3g.co.uk/existing-customer-deals">Existing Customer Deals</a></li>
<li><a href="/top-up">Top-Up</a></li>
<li><a href="https://3g.co.uk/upgrade">Upgrades</a></li>
<li>Three Plans &amp; Services</li>
<li><a href="/three-advanced-plans">Advanced Plans</a></li>
<li><a href="/feel-at-home">Roaming</a></li>
<li><a href="/tethering">Tethering</a></li>
<li><a href="https://3g.co.uk/guides/three-go-binge">Go Binge</a></li>
<li><a href="https://3g.co.uk/news/three-4g-super-voice-everything-you-need-to-know">4G Super-Voice</a></li>
<li>Guides</li>
<li><a href="/keep-your-number">Keep Your Number</a></li>
<li><a href="/guides/unlocking-a-three-phone-tablet">Unlocking</a></li>
<li><a href="/store-locator">Store Locator</a></li>
</ul>
</li>
<li class="hidden-sm searchParent"><a class="underlineMenu parent" href="/search"><i class="fa fa-search"></i>
<!-- <i class="fa fa-caret-down"></i> -->
</a>
<!-- <ul>
								<li class="search hidden-sm">
									<div class="headerSearch">

										<form action="/search" method="post">
											<fieldset>
												<input type="text" name="q" value="" class="search" placeholder="Search..."/>
												<input class="submit" type="submit" value="&#xf002;" />
											</fieldset>
										</form>
									</div>
								</li>
							</ul> -->
</li>
<li class="toggle searchIcon hidden-md hidden-lg hidden-xs"><i class="fa fa-search"></i></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<!-- Base MasterSlider style sheet -->
<!-- <link rel="stylesheet" href="/css/masterslider/masterslider-2.css" /> -->
<!--<link href="/css/masterslider/ms-tabs-style.css" rel='stylesheet' type='text/css'>-->
<!--
<div class="sliderRow">
	<div class="container">		
		<div class="row">
			<div class="col-sm-12">
-->
<!-- template -->
<!-- 			<div class="ms-tabs-template"> -->
<!-- masterslider -->
<!--
					<div class="master-slider ms-skin-default" id="masterslider">
					   
					    <div class="ms-slide">
					        <a href="https://3g.co.uk/sim-only/contract"></a>
						        <img src="/img/blank.gif" data-src="/userfiles/slideshows/96-5G-SIM.jpg" alt="SIM Only Deals"/>
						    
					        <div class="ms-thumb">
					        	<h3>SIM Only Deals</h3>
					        </div>
					    </div>
					   
					    <div class="ms-slide">
					        <a href="https://3g.co.uk/mobile-phones"></a>
						        <img src="/img/blank.gif" data-src="/userfiles/slideshows/95-Mobile-Phones.jpg" alt="Phone deals"/>
						    
					        <div class="ms-thumb">
					        	<h3>Phone deals</h3>
					        </div>
					    </div>
					   
					    <div class="ms-slide">
					        <a href="https://3g.co.uk/mobile-broadband/huawei-b535"></a>
						        <img src="/img/blank.gif" data-src="/userfiles/slideshows/94-Home-Broadband.jpg" alt="4G Home Broadband"/>
						    
					        <div class="ms-thumb">
					        	<h3>4G Home Broadband</h3>
					        </div>
					    </div>
					   
					    <div class="ms-slide">
					        <a href="https://3g.co.uk/unlimited-data-plans"></a>
						        <img src="/img/blank.gif" data-src="/userfiles/slideshows/82-Unlimited-Data.jpg" alt="Unlimited Data"/>
						    
					        <div class="ms-thumb">
					        	<h3>Unlimited Data</h3>
					        </div>
					    </div>
					</div>
-->
<!-- end of masterslider -->
<!-- 			</div> -->
<!-- end of template -->
<!--
			</div>
		</div>
	</div>
</div>
-->
<div class="sliderRow">
<div class="container">
<div class="row">
<div class="col-xs-12">
<ul class="rslides">
<li>
<div class="row">
<div class="col-sm-4 col-sm-push-8">
<div class="image cf">
<img alt="SIM Only Deals" class="responsive" src="/userfiles/slideshows/96-5G-SIM.jpg"/>
</div>
</div>
<div class="col-sm-8 col-sm-pull-4">
<div class="text">
<span class="large">SIM Only Deals</span>
<span class="small">Exclusive deals on SIM plans with 5G at no extra cost.</span>
<a class="deals" href="https://3g.co.uk/sim-only/contract">See Deals</a>
</div>
</div>
</div>
<a aria-label="Link to SIM Only Deals deal" class="link link3" href="https://3g.co.uk/sim-only/contract"></a>
</li>
<li>
<div class="row">
<div class="col-sm-4 col-sm-push-8">
<div class="image cf">
<img alt="Phone deals" class="responsive" src="/userfiles/slideshows/95-Mobile-Phones.jpg"/>
</div>
</div>
<div class="col-sm-8 col-sm-pull-4">
<div class="text">
<span class="large">Phone deals</span>
<span class="small">Exclusive deals on latest phones including iPhone 12</span>
<a class="deals" href="https://3g.co.uk/mobile-phones">See Deals</a>
</div>
</div>
</div>
<a aria-label="Link to Phone deals deal" class="link link3" href="https://3g.co.uk/mobile-phones"></a>
</li>
<li>
<div class="row">
<div class="col-sm-4 col-sm-push-8">
<div class="image cf">
<img alt="4G Home Broadband" class="responsive" src="/userfiles/slideshows/94-Home-Broadband.jpg"/>
</div>
</div>
<div class="col-sm-8 col-sm-pull-4">
<div class="text">
<span class="large">4G Home Broadband</span>
<span class="small">6 months half price. Unlimited data just £10 a month.</span>
<a class="deals" href="https://3g.co.uk/mobile-broadband/huawei-b535">See Deals</a>
</div>
</div>
</div>
<a aria-label="Link to 4G Home Broadband deal" class="link link3" href="https://3g.co.uk/mobile-broadband/huawei-b535"></a>
</li>
<li>
<div class="row">
<div class="col-sm-4 col-sm-push-8">
<div class="image cf">
<img alt="Unlimited Data" class="responsive" src="/userfiles/slideshows/82-Unlimited-Data.jpg"/>
</div>
</div>
<div class="col-sm-8 col-sm-pull-4">
<div class="text">
<span class="large">Unlimited Data</span>
<span class="small">Truly unlimited data now from £10 a month.</span>
<a class="deals" href="https://3g.co.uk/unlimited-data-plans">See Deals</a>
</div>
</div>
</div>
<a aria-label="Link to Unlimited Data deal" class="link link3" href="https://3g.co.uk/unlimited-data-plans"></a>
</li>
</ul>
<ul id="pager">
<li style="width: calc((100% / 4) - 4px);"><a aria-label="link to SIM Only Deals" href="#"><span class="tab_pager">SIM Only Deals</span></a></li>
<li style="width: calc((100% / 4) - 4px);"><a aria-label="link to Phone deals" href="#"><span class="tab_pager">Phone deals</span></a></li>
<li style="width: calc((100% / 4) - 4px);"><a aria-label="link to 4G Home Broadband" href="#"><span class="tab_pager">4G Home Broadband</span></a></li>
<li style="width: calc((100% / 4) - 4px);"><a aria-label="link to Unlimited Data" href="#"><span class="tab_pager">Unlimited Data</span></a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="contentRow">
<div class="container">
<div class="content" id="contentHome">
<div class="row">
<div class="col-xs-12">
<div class="sidelines">
<div class="lines linesHomepage"></div>
<h2 id="home">Find a Product</h2>
</div>
</div>
</div>
<div class="row">
<div class="col-md-3 col-xs-6">
<div class="icon cf">
<a href="/sim-only" title="Sim Only"></a>
<i class="icon-sim"></i>
<h2 class="sim"><span>SIM only</span></h2>
</div>
</div>
<div class="col-md-3 col-xs-6">
<div class="icon cf">
<a href="/mobile-phones" title="Mobile Phones"></a>
<i class="icon-phones"></i>
<h2 class="phone"><span>Mobile Phones</span></h2>
</div>
</div>
<div class="col-md-3 col-xs-6">
<div class="icon cf">
<a href="/mobile-broadband" title="Mobile Broadband"></a>
<i class="icon-broadband"></i>
<h2 class="broadband"><span>Mobile Broadband</span></h2>
</div>
</div>
<div class="col-md-3 col-xs-6">
<div class="icon cf">
<a href="/home-broadband" title="Home Broadband"></a>
<!-- <i class="icon-tablets"></i> -->
<i aria-hidden="true" class="fa fa-wifi"></i>
<h2 class="broadband"><span>Home Broadband</span></h2>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="contentRowGrey home">
<div class="container">
<div class="row">
<div class="col-md-8">
<h3 class="latest">News, Reviews &amp; Guides</h3>
<div class="tab_container tabContainerHome">
<div class="tab_content" id="tab1" style="display: block">
<div class="newsFeed cf">
<div class="image">
<a href="/guides/what-is-5g-and-how-fast-is-it-">
<img alt="What is 5G and how fast is it?" src="https://cdn.3g.co.uk/userfiles/guides/t/What-is-5G.jpg"/>
</a>
</div>
<div class="text">
<a class="title" href="/guides/what-is-5g-and-how-fast-is-it-">What is 5G and how fast is it?</a>
<span class="date">07 Jan 2021</span><a href="/guides"><span class="tag tagguides">Guides</span></a>
<p class="hidden-xs">5G is now here, but what is it? Where is it available? And why would you want it? We answer all that and more.
</p>
</div>
</div>
<div class="newsFeed cf">
<div class="image">
<a href="/guides/best-camera-phones">
<img alt="Best camera phones on the market in 2021" src="https://cdn.3g.co.uk/userfiles/guides/t/Best-Camera-Smartphones.jpg"/>
</a>
</div>
<div class="text">
<a class="title" href="/guides/best-camera-phones">Best camera phones on the market in 2021</a>
<span class="date">18 Dec 2020</span><a href="/guides"><span class="tag tagguides">Guides</span></a>
<p class="hidden-xs">Whether it’s a high megapixel count, lots of lenses, an optical zoom or any number of other things, there are a lot of interesting and powerful smartphone cameras around.
</p>
</div>
</div>
<div class="newsFeed cf">
<div class="image">
<a href="/guides/hdr-mobile">
<img alt="What is Mobile HDR and which phones support HDR?" src="https://cdn.3g.co.uk/userfiles/guides/t/LG-G6-HDR.jpg"/>
</a>
</div>
<div class="text">
<a class="title" href="/guides/hdr-mobile">What is Mobile HDR and which phones support HDR?</a>
<span class="date">17 Dec 2020</span><a href="/guides"><span class="tag tagguides">Guides</span></a>
<p class="hidden-xs">HDR has arrived on mobile and it’s set to transform your viewing experience. Here’s everything you need to know about mobile HDR and how to get it.
</p>
</div>
</div>
<div class="newsFeed cf">
<div class="image">
<a href="/guides/the-best-smartphones-for-selfies">
<img alt="Best selfie camera phone 2021" src="https://cdn.3g.co.uk/userfiles/guides/t/Best-Selfie-Camera.jpg"/>
</a>
</div>
<div class="text">
<a class="title" href="/guides/the-best-smartphones-for-selfies">Best selfie camera phone 2021</a>
<span class="date">16 Dec 2020</span><a href="/guides"><span class="tag tagguides">Guides</span></a>
<p class="hidden-xs">There’s a surprising amount of choice for the discerning selfie fan, but to make the decision a little easier here are the best smartphones for selfies.
</p>
</div>
</div>
<div class="newsFeed cf">
<div class="image">
<a href="/reviews/huawei-mate-40-pro">
<img alt="Huawei Mate 40 Pro review" src="https://cdn.3g.co.uk/userfiles/reviews/t/Huawei-Mate-40-Pro-Review.jpg"/>
</a>
</div>
<div class="text">
<a class="title" href="/reviews/huawei-mate-40-pro">Huawei Mate 40 Pro review</a>
<span class="date">16 Dec 2020</span><a href="/reviews"><span class="tag tagreviews">Reviews</span></a>
<p class="hidden-xs">The Huawei Mate 40 Pro is a tremendously exciting and stylish handset, with a stunning design that you won’t see on many other phones, some of the...</p>
</div>
</div>
</div>
</div>
</div>
<div class="cf visible-xs"></div>
<div class="col-md-4 col-sm-6">
<h3 class="latest">Latest Phones</h3>
<div class="featured latestPhones">
<ul>
<li class="row">
<div class="col-sm-4 col-xs-2">
<img alt="Huawei Mate 40 Pro" class="full" src="https://cdn.3g.co.uk/userfiles/products/t_1493-1.jpg?v=0.1"/>
</div>
<div class="col-sm-8 col-xs-10 text-center">
<span>Huawei Mate 40 Pro</span>
</div>
<a href="/mobile-phones/huawei-mate-40-pro-silver"></a>
</li>
<li class="row">
<div class="col-sm-4 col-xs-2">
<img alt="Xiaomi Redmi 9AT" class="full" src="https://cdn.3g.co.uk/userfiles/products/t_1492-1.jpg?v=0.1"/>
</div>
<div class="col-sm-8 col-xs-10 text-center">
<span>Xiaomi Redmi 9AT</span>
</div>
<a href="/mobile-phones/xiaomi-redmi-9at-granite-grey"></a>
</li>
<li class="row">
<div class="col-sm-4 col-xs-2">
<img alt="Xiaomi Mi 10T Pro" class="full" src="https://cdn.3g.co.uk/userfiles/products/t_1491-1.jpg?v=0.1"/>
</div>
<div class="col-sm-8 col-xs-10 text-center">
<span>Xiaomi Mi 10T Pro</span>
</div>
<a href="/mobile-phones/xiaomi-mi-10t-pro-cosmic-black"></a>
</li>
</ul>
</div>
<div class="featured newsletter cf hidden-sm hidden-xs">
<h3>Newsletter</h3>
<p>Be the first to hear about the latest devices and get news of breaking deals before anyone else</p>
<!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup">
<form action="//www.us7.list-manage.com/subscribe/post?u=e97924e9d00d5bc436bf39a9b&amp;id=f2b1c69252" class="validate" id="mc-embedded-subscribe-form" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
<div id="mc_embed_signup_scroll">
<div class="mc-field-group">
<label for="mce-EMAIL">Email Address </label>
<input class="required email" id="mce-EMAIL" name="EMAIL" type="email" value=""/>
</div>
<div class="clear" id="mce-responses">
<div class="response" id="mce-error-response" style="display:none"></div>
<div class="response" id="mce-success-response" style="display:none"></div>
</div> <!-- real people should not fill this in and expect good things -->
<div aria-hidden="true" style="position: absolute; left: -5000px;">
<input name="b_e97924e9d00d5bc436bf39a9b_f2b1c69252" tabindex="-1" type="text" value=""/>
</div>
<div class="clear">
<input class="signup" id="mc-embedded-subscribe" name="subscribe" style="width: auto;" type="submit" value="Subscribe"/>
</div>
</div>
</form>
</div>
</div>
<div class="featured social hidden-sm hidden-xs">
<span>Follow Us</span>
<div class="icons">
<a href="https://www.facebook.com/3g.co.uk" target="_blank"><i class="fa fa-facebook"></i></a>
<a href="https://twitter.com/3gcouk" target="_blank"><i class="fa fa-twitter"></i></a>
</div>
</div>
</div>
<div class="col-sm-6 visible-sm visible-xs">
<div class="featured newsletter cf">
<h3>Newsletter</h3>
<p>Be the first to hear about the latest devices and get news of breaking deals before anyone else</p>
<!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup">
<form action="//www.us7.list-manage.com/subscribe/post?u=e97924e9d00d5bc436bf39a9b&amp;id=f2b1c69252" class="validate" id="mc-embedded-subscribe-form" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
<div id="mc_embed_signup_scroll">
<div class="mc-field-group">
<label for="mce-EMAIL">Email Address </label>
<input class="required email" id="mce-EMAIL" name="EMAIL" type="email" value=""/>
</div>
<div class="clear" id="mce-responses">
<div class="response" id="mce-error-response" style="display:none"></div>
<div class="response" id="mce-success-response" style="display:none"></div>
</div> <!-- real people should not fill this in and expect good things -->
<div aria-hidden="true" style="position: absolute; left: -5000px;">
<input name="b_e97924e9d00d5bc436bf39a9b_f2b1c69252" tabindex="-1" type="text" value=""/>
</div>
<div class="clear">
<input class="signup" id="mc-embedded-subscribe" name="subscribe" type="submit" value="Subscribe"/>
</div>
</div>
</form>
</div>
<!--End mc_embed_signup-->
</div>
<div class="featured social hidden-xs">
<span>Follow Us</span>
<div class="icons">
<a href="https://www.facebook.com/3g.co.uk" target="_blank"><i class="fa fa-facebook"></i></a>
<a href="https://twitter.com/3gcouk" target="_blank"><i class="fa fa-twitter"></i></a>
</div>
</div>
</div>
</div>
</div>
<div class="container" style="margin-bottom: 30px;">
<div class="row">
<div class="col-xs-12">
<div class="featured textArea cf">
<div class="content">
<div class="homeText">
<h3 style="text-align: justify;">Welcome to 3G</h3>
<p style="text-align: justify;"><span style="line-height: 1.6em;">Our aim is to bring you breaking news and comprehensive reviews of all the latest mobile phones, tablets and mobile broadband devices. We are an independent mobile phone blog and have been online since 1999 and have been an affiliate of Three UK since 2007. This means you can buy with total confidence as all purchases made after clicking through our website are made directly with Three (3). </span></p>
<p style="text-align: justify;"><span style="line-height: 1.6em;">The information contained on this website has been written to assist our readers. We do not represent Three or speak on its behalf and are entirely independent of Three.</span></p>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="featured popularPhones cf">
<div class="col-xs-12">
<h4 class="popular">Popular Phones</h4>
<a class="view" href="/mobile-phones">Browse All Phones</a>
</div>
<div class="cf"></div>
<div class="col-md-3 col-sm-6">
<div class="featuredWrapper">
<a class="fullLink" href="/mobile-phones/apple-iphone-12-64gb-red" title="Apple iPhone 12 64GB (PRODUCT) Red "></a>
<div class="soldOut">
<div class="banner">
<span>Popular</span>
</div>
</div>
<div class="featuredImage">
<img alt="Apple iPhone 12 64GB (PRODUCT) Red " class="responsive" src="https://3g.co.uk/userfiles/products/t_1443-1.jpg?v=0.1"/>
<span class="tag new">NEW</span>
</div>
<div class="featuredText">
<h3>Apple iPhone 12</h3>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="featuredWrapper">
<a class="fullLink" href="/mobile-phones/samsung-galaxy-s20-5g-128gb-cloud-blue" title="Samsung Galaxy S20 128GB 5G Cloud Blue "></a>
<div class="soldOut">
<div class="banner">
<span>Popular</span>
</div>
</div>
<div class="featuredImage">
<img alt="Samsung Galaxy S20 128GB 5G Cloud Blue " class="responsive" src="https://3g.co.uk/userfiles/products/t_1281-1.jpg?v=0.1"/>
<span class="tag new">NEW</span>
</div>
<div class="featuredText">
<h3>Samsung Galaxy S20 5G</h3>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="featuredWrapper">
<a class="fullLink" href="/mobile-phones/apple-iphone-12-pro-128gb-gold" title="Apple iPhone 12 Pro 128GB Gold "></a>
<div class="soldOut">
<div class="banner">
<span>Popular</span>
</div>
</div>
<div class="featuredImage">
<img alt="Apple iPhone 12 Pro 128GB Gold " class="responsive" src="https://3g.co.uk/userfiles/products/t_1448-1.jpg?v=0.1"/>
<span class="tag new">NEW</span>
</div>
<div class="featuredText">
<h3>Apple iPhone 12 Pro</h3>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="featuredWrapper">
<a class="fullLink" href="/mobile-phones/samsung-galaxy-s20-ultra-5g-128gb-cosmic-black" title="Samsung Galaxy S20 Ultra 128GB 5G Cosmic Black "></a>
<div class="soldOut">
<div class="banner">
<span>Popular</span>
</div>
</div>
<div class="featuredImage">
<img alt="Samsung Galaxy S20 Ultra 128GB 5G Cosmic Black " class="responsive" src="https://3g.co.uk/userfiles/products/t_1284-1.jpg?v=0.1"/>
<span class="tag new">NEW</span>
</div>
<div class="featuredText">
<h3>Samsung Galaxy S20 Ultra 5G</h3>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script>
			function SetCookie(c_name, value, days) {
				
				var date, expires;	    
		        date = new Date();
		        date.setTime(date.getTime()+(days*24*60*60*1000));
		        expires = "; expires="+date.toGMTString();
			   
				document.cookie = c_name+"="+value+expires+"; path=/";
		
				//console.log(exdate);
				//location.reload()
			}
			
		</script>
<div class="footerRow">
<div class="container">
<div class="bannerFoot">
<a class="close" href="#" id="scrollClose" onclick="SetCookie('banner', '1', '1');"><span class="scrollX">x</span></a>
<p><span class="desktop"><span class="text"><strong><span style="color:#FF8C00;">EPIC SIM DEAL </span></strong> </span> <span class="box"> <span class="boxTop">12GB</span><span class="boxBottom"> data</span> </span> <span class="box"> <span class="boxTop">£8</span> <span class="boxBottom">a month</span> </span> <a href="https://3g.co.uk/transfer?18324" rel="nofollow" target="_blank">See Deal</a> </span> <span class="mobile hidden-sm hidden-md hidden-lg"> <span class="text"> <span style="color:#FF8C00;"> <strong>EPIC SIM DEAL </strong> </span> </span> <span class="box"> <span class="boxTop">12GB</span><span class="boxBottom"> data</span> </span> <span class="box"> <span class="boxTop">£8</span> <span class="boxBottom">a month</span> </span> <a href="https://3g.co.uk/transfer?18324" rel="nofollow" target="_blank">See Deal</a> </span></p>
</div>
<div class="row">
<div class="col-md-2 col-sm-12 linkList cf">
<h4><span style="color:#FFFFFF;"><strong>Deals</strong></span></h4>
<ul class="footerLinks">
<li><a href="https://3g.co.uk/deals">Best Deals</a></li>
<li><a href="https://3g.co.uk/mobile-phones">Mobile Phones</a></li>
<li><a href="https://3g.co.uk/sim-only">SIM Only</a></li>
<li><a href="https://3g.co.uk/tablets">Tablets</a></li>
<li><a href="https://3g.co.uk/mobile-broadband">Mobile Broadband</a></li>
</ul>
</div>
<div class="col-md-2 col-sm-12 linkList cf">
<h4><strong><span style="color:#FFFFFF;">Blog</span></strong></h4>
<ul class="footerLinks">
<li><a href="https://3g.co.uk/news">News</a></li>
<li><a href="https://3g.co.uk/reviews">Reviews</a></li>
<li><a href="https://3g.co.uk/guides">Guides</a></li>
</ul>
</div>
<div class="col-md-2 col-sm-12 linkList cf">
<h4><strong><span style="color:#FFFFFF;">Popular Guides</span></strong></h4>
<ul class="footerLinks">
<li><a href="https://3g.co.uk/unlimited-data-plans">Unlimited Data</a></li>
<li><a href="https://3g.co.uk/three-coverage">Coverage Checker</a></li>
<li><a href="https://3g.co.uk/my3">My3</a></li>
<li><a href="https://3g.co.uk/feel-at-home">Go Roam</a></li>
<li><a href="https://3g.co.uk/tethering">Tethering</a></li>
</ul>
</div>
<div class="col-md-2 col-sm-12 linkList cf">
<h4><strong><span style="color:#FFFFFF;">Useful Info</span></strong></h4>
<ul class="footerLinks">
<li><a href="https://3g.co.uk/contact">Contact Us</a></li>
<li><a href="https://3g.co.uk/about-us">About Us</a></li>
<li><a href="https://3g.co.uk/privacy-policy">Privacy Policy</a></li>
<li><a href="https://3g.co.uk/how-we-use-cookies">Cookie Policy</a></li>
</ul>
</div>
<div class="visible-xs visible-sm cf"></div>
<div class="col-md-3 col-md-offset-1">
<div class="newsletter">
<img alt="3G Phones Tablets and Mobile Broadband UK logo" class="responsive" itemprop="logo" src="/img/3g_trans.png"/>
<div class="cf"></div>
<a href="https://www.facebook.com/3g.co.uk" target="_blank"><i class="fa fa-facebook"></i></a><a href="https://twitter.com/3gcouk" target="_blank"><i class="fa fa-twitter"></i></a>
</div>
</div>
</div>
</div>
<a class="arrowUp hidden-xs" href="#"><i class="fa fa-arrow-up"></i></a>
</div>
<div class="copyrightRow">
<div class="container">
<div class="row">
<div class="copyright cf">
<div class="col-md-5">
<p>©Copyright 2021 www.3G.co.uk - All Rights Reserved</p>
</div>
</div>
</div>
</div>
</div>
<div id="cookieRow">
</div>
<script src="/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
			$(document).ready(function() {
								// cookie acceptance
				var $cookiePane = $('<div id="cookiePane" style="background-color: #FFFFFF; background-color: rgba(255, 255, 255, 0.95); border-top: 1px solid #02478E; display: none; padding: 5px 5px 10px;  width: 100%; z-index: 999;" />').appendTo('#cookieRow');
				
				$cookiePane.html('<div class="container"><h5>Cookie Usage</h5><p>We use cookies to ensure that we give you the best experience on our website.  <a href="/how-we-use-cookies">Find out more</a> here.</p><a class="cookieAgree" href="#">OK</a><div class="clear"></div></div>').delay(1500).slideDown();
				
				$('.cookieAgree').click(function() {
					$cookiePane.slideUp();
					return false;
				});
							});
		</script>
<script type="text/javascript">
			WebFontConfig = {
				google: { families: [ 'Montserrat:400,700:latin', 'Source Sans Pro:200,300,400' ] }
			};
			 
			var cb = function() {
				var wf = document.createElement('script');
				wf.src = '//ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
				wf.type = 'text/javascript';
				wf.async = 'true';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(wf, s);
			};
			 
			var raf = requestAnimationFrame || mozRequestAnimationFrame || webkitRequestAnimationFrame || msRequestAnimationFrame;
			 
			if(raf){
				raf(cb);
			}else{
				window.addEventListener('load', cb);
			}
		</script>
<script src="https://3g.co.uk/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/menu2.js?=v1.4"></script>
<script src="/js/jquery.slicknav.min.js?v=1.2"></script>
<script>
			$(document).ready(function(){
				$('#menu').slicknav({
					allowParentLinks:true,
				}); 
			});
		</script>
<script>
			$(document).ready(function() {
				if($(window).width() < 767) {
					$(".slicknav_nav LI A").removeClass("underlineMenu");
				}	
			});	
		</script>
<script async="" src="/js/jquery.transit.min.js" type="text/javascript"></script>
<!--[if IE]>
	    <script src="/js/respond.min.js" type="text/javascript"></script>
	    <![endif]-->
<script>
		var show_on_scroll = true;
		$(document).ready(function() {
		 
			$(window).scroll(function() {
			  if( $(this).scrollTop() > 350 && show_on_scroll === true) {
		  
				$(".bannerFoot").fadeIn('slow');
				
			    $("#scrollClose").click(function(e) {
					e.preventDefault();
					$(".bannerFoot").fadeOut('fast');
					show_on_scroll = false;
			    });
		      }
		    });  
		  
		});
		</script>
<script src="/js/home-2.js?v=7"></script>
<script src="/js/jquery.mobile-1.4.5.min.js"></script>
<script src="/js/main.js?v=2.8" type="text/javascript"></script>
<script>
		    fluidvids.init({
		      selector: ['iframe'],
		      players: ['www.youtube.com', 'player.vimeo.com']
		    });
		    </script>
<script src="/js/mailchimp.js" type="text/javascript"></script>
<script type="text/javascript">(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='text';fnames[4]='MMERGE4';ftypes[4]='number';fnames[5]='MMERGE5';ftypes[5]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
</body>
</html>
