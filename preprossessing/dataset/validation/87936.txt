<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>AIMS INDIA Pvt. Ltd.</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<style type="text/css">
<!--
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.style2 {
	font-size: 14;
	font-weight: bold;
}
.style4 {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 36px;
	font-weight: bold;
	color: #FFFFFF;
}
a:link {
	color: #990000;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #990000;
}
a:hover {
	text-decoration: underline;
	color: #333333;
}
a:active {
	text-decoration: none;
	color: #990000;
}
-->
</style>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td bgcolor="#5498FE" valign="top"><div align="right">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><div align="center"><span class="style4">AIMS INDIA Pvt. Ltd. </span></div></td>
<td width="124"><img height="181" src="images/logo1.jpg" width="162"/></td>
</tr>
</table>
<span class="style1"></span></div></td>
</tr>
<tr>
<td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td bgcolor="#94D7FE" width="200"><table bgcolor="#FFFFFF" border="0" cellpadding="1" cellspacing="2" width="100%">
<tr>
<td bgcolor="#94D7FE" class="style1"><p><strong><a href="index.html">About Us</a> </strong></p></td>
</tr>
</table>
<table bgcolor="#FFFFFF" border="0" cellpadding="1" cellspacing="2" width="100%">
<tr>
<td bgcolor="#94D7FE" class="style1"><p><strong><a href="product.html">Product Range </a></strong></p></td>
</tr>
</table>
<table bgcolor="#FFFFFF" border="0" cellpadding="1" cellspacing="2" width="100%">
<tr>
<td bgcolor="#94D7FE" class="style1"><p><strong><a href="list.html">List of Equipment</a></strong></p></td>
</tr>
</table>
<table bgcolor="#FFFFFF" border="0" cellpadding="1" cellspacing="2" width="100%">
<tr>
<td bgcolor="#94D7FE" class="style1"><p><strong><a href="enquiry.html">Enquiry</a></strong></p></td>
</tr>
</table>
<p> </p></td>
<td><div align="justify">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="90%">
<tr>
<td><div align="justify" class="style1"><span class="style2"><br/>
                AIMS <br/>
  Innovation &amp; Adoption of Technology </span><br/>
<br/>
                Aims India Pvt. Ltd. – Kolkata(India)was formed in the year 1987, by a Group of Engineers belonging to the rank of Senior Executives,Ex –Employees of Larsen &amp; Toubro Ltd- India, which had technical collaboration with J. M. Voith – Germany.</div>
<p align="justify" class="style1">The Chief Executive- Mr A. K. Dikshit, the founder of the engineering company – “Aims India Pvt. Ltd.” carry with him a working experience of over 15 years in Larsen &amp; Toubro Ltd. and almost the same period of successfully running the business of AIMS with a focused long term vision.</p>
<p align="justify" class="style1">The main products of “AIMS” include equipments for Waste Paper Pulping, Screening &amp; Cleaning systems, Power efficient Agitators, Centrifugal Pumps, Vacuum Pumps and complete Paper Machine sections/individual components. All these equipments incorporate advance technology based on sound design principles &amp; proven performance characteristics.</p>
<p align="justify" class="style1">In order to excel in the business profession, AIMS has updated the latest CAD/CAM technology in designing the equipments &amp; machineries. The manufacturing capabilities have been expanded with the addition of latest machine tools to achieve high quality product accuracy. </p>
<p align="justify" class="style1">“AIMS” widening client base in India &amp; neighboring countries speaks for its leading brand equipments -all manufactured in accordance with ISO 9000 QA.</p>
</td>
</tr>
</table>
<p> </p>
</div> </td>
</tr>
</table></td>
</tr>
<tr>
<td bgcolor="#BCE0FE" valign="top"><div align="center">
<p class="style1"><strong>AIMS INDIA Pvt. Ltd. 292A, Jodhpur Park , Kolkata – 700 068 INDIA </strong><br/>
<strong>Mobile :</strong> +91 9830016794 <strong>Tel/fax:</strong> +91(033) 24733983 <strong>Tel:</strong> +91 (033) 24726148<br/>
<strong>Emails :</strong> dikshit@aimsindia.org            ,<br/>
                        dikshit@papiertech.com </p>
</div></td>
</tr>
</table>
</body>
</html>
