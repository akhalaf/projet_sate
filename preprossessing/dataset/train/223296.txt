<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<title>Search Movies, TV Series, Video BIQLE Video</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="1ff5c4ab791b3453" name="yandex-verification"/>
<meta content="all" name="robots"/>
<meta content="noarchive" name="robots"/>
<link href="/favicon.ico" rel="shortcut icon"/>
<meta content="Ton videos for everyone for free. Watch in HD quality, add to favorites and be happy." name="description"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css"/>
<link href="/css/common.css?cb6249" rel="stylesheet"/>
<script src="/js/jquery-2.1.1.min.js?61fe79" type="text/javascript"></script>
<script src="/js/nprogress.js?9a7341" type="text/javascript"></script>
<script src="/js/history.js?81d955" type="text/javascript"></script>
<script src="/js/common.js?dd4141" type="text/javascript"></script>
<script src="/js/comments.js?855012" type="text/javascript"></script>
<script src="/js/auth.js?3cd62a" type="text/javascript"></script>
<script src="/js/share42.js?83d881" type="text/javascript"></script>
<script src="/js/barcode.js?24a599" type="text/javascript"></script>
<style type="text/css">
html {
  overflow: hidden;
}

body {
  background-image: url(/img/mountainscape.jpg);
  background-repeat: no-repeat;
  background-size: 100%;
}
</style>
<script type="text/javascript">
if (window.parent.frames.length > 0) {
window.stop();
}
</script>
<svg id="barcode" style="z-index: 999;display: block;width: 100%;position: fixed;bottom: -10px;"></svg>
<script>
    JsBarcode("#barcode", "cqdMDLZSXNdthGCWQFhw=AEtTN", {
  format: "CODE128",
  lineColor: "#999",
  background: "#171717",
  width: 3,
  height: 3,
  marginTop:1,
  displayValue: false
});
  </script>
<script type="text/javascript">
window._stv = 'dd4141';
window.is_logged = false;
</script>
</head>
<!--  <noindex> <a style="text-align: left;
    float: left;
    background: #2a2b2c;
    color: #ff5454;
    font-size: 15px;
    text-decoration: none;
    padding: 10px;" href="https://biqle.org" target="_blank">Attention. The new address biqle.ru → biqle<span style="background: #ffd300">.org</span></a></noindex> -->
<body class="main">
<div class="main_width">
<div class="main_header">
<form class="main_search_form" onsubmit="return search(true, true);">
<input id="q" placeholder="Search..." type="text" value=""/>
<button type="submit">Find</button>
</form>
<div class="clear"></div>
</div>
<div class="main_body">
<h1 class="main_logo"> <a href="/" onclick="return true;"><div alt="BIQLE" class="center_logo"></div></a></h1>
<p style="font-size: 21px;color:#fff;">Open video entertainment platform and the best collections</p>
<div class="main_other">
<b>By joining us you get:</b>
<p>Best videos</p>
<p>Less advertising</p>
<p>High quality and stability</p>
<p>Own favorites</p>
<p>Opportunity to comment</p>
<p>The ability to put likes</p>
</div>
<div class="main_forms">
<iframe frameborder="0" id="callback_frame" name="callback_frame" style="display: none;"></iframe>
<form action="/auth/register" class="main_form" id="form_reg" method="POST" onsubmit="return Auth.formSubmit(this);" target="callback_frame">
<input name="callback" type="hidden" value="Auth.onRegister"/>
<div class="form-row"><input id="reg_login" name="login" placeholder="Your name" type="text"/></div>
<div class="form-row"><input id="reg_email" name="email" placeholder="Your email" type="text"/></div>
<div class="form-row"><input id="reg_pass" name="pass" placeholder="Your password" type="password"/></div>
<div class="form-row"><div id="recaptcha_register" style="width: 300px; height: 76px; margin: 0; overflow:hidden; border-radius: 5px;"></div></div>
<div class="form-row" id="error_message" style="display: none;"></div>
<div class="form-row">
<button disabled="" id="register_btn" type="submit">Join us</button>
<span>or <a onclick="Auth.SwitchForm('form_auth');">Log in</a></span>
<p>By registering you agree to <a href="/legal/" onclick="return true;">the terms</a>. </p>
</div>
</form>
<form action="/auth/login" class="main_form" id="form_auth" method="POST" onsubmit="return Auth.formSubmit(this);" style="display: none;" target="callback_frame">
<input name="callback" type="hidden" value="Auth.onLogin"/>
<div class="form-row"><input id="login_email" name="email" placeholder="Your email" type="text"/></div>
<div class="form-row"><input id="login_pass" name="pass" placeholder="Your password" type="password"/></div>
<div class="form-row"><div id="recaptcha_login" style="width: 300px; height: 76px; margin: 0; overflow:hidden; border-radius: 5px;"></div></div>
<div class="form-row" id="error_message" style="display: none;"></div>
<div class="form-row">
<button class="auth" disabled="" id="login_btn" type="submit">Log in</button>
<span>or <a onclick="Auth.SwitchForm('form_reg');">Join us</a></span>
</div>
<div class="form-row">
<span><a onclick="Auth.SwitchForm('form_restore');">Forgot your password?</a></span>
<!--p>Login using social networks</p>
            <div class="form-row social">
            <a class="facebook" title="Facebook login" onclick="Auth.Facebook();"></a>
            <a class="vk" title="ВКонтакте login" onclick="Auth.VK();"></a>
            <a class="google" title="Google login" onclick="Auth.Google();"></a>
            <a class="twitter" title="Twitter login" onclick="Auth.Twitter();"></a>
          </div-->
</div>
</form>
<form action="/auth/restore" class="main_form" id="form_restore" method="POST" onsubmit="return Auth.formSubmit(this);" style="display: none;" target="callback_frame">
<input name="callback" type="hidden" value="Auth.onRestore"/>
<div class="form-row"><input id="restore_email" name="email" placeholder="Your email" type="text"/></div>
<div class="form-row"><div id="recaptcha_restore" style="width: 300px; height: 76px; margin: 0; overflow:hidden; border-radius: 5px;"></div></div>
<div class="form-row" id="error_message" style="display: none;"></div>
<div class="form-row">
<button disabled="" id="restore_btn" type="submit">Restore password</button>
<span>or <a onclick="Auth.SwitchForm('form_auth');">Log in</a></span>
</div>
</form>
</div>
</div>
<div class="main_footer">
<a href="/" onclick="return false;">BIQLE</a>
<a href="/legal/" onclick="return true;">Terms</a>
<a href="/holders/" onclick="return true;">DMCA</a>
<a href="/contact/" onclick="return true;">Contact us</a>
</div>
</div>
<div class="bg_layer"></div>
<script type="text/javascript">
$('#q').val('').focus();

window.reCaptchaShow = function() {
  try {
    grecaptcha.reset();
  } catch (e) {}

  grecaptcha.render('recaptcha_login', {
    sitekey: '6LdwvK8UAAAAADn6r5tZbjeGUuj633gdfWyUOmaR',
    callback: function() {
      $('#login_btn').prop('disabled', false);
    }
  });

  grecaptcha.render('recaptcha_restore', {
    sitekey: '6LdwvK8UAAAAADn6r5tZbjeGUuj633gdfWyUOmaR',
    callback: function() {
      $('#restore_btn').prop('disabled', false);
    }
  });

  grecaptcha.render('recaptcha_register', {
    sitekey: '6LdwvK8UAAAAADn6r5tZbjeGUuj633gdfWyUOmaR',
    callback: function() {
      $('#register_btn').prop('disabled', false);
    }
  });
}

if (!window.grecaptcha) {
  var js = document.createElement('script');
  js.src = '//www.google.com/recaptcha/api.js?onload=reCaptchaShow&render=explicit&hl=en';
  js.async = true;

  $('head').append(js);
} else {
  reCaptchaShow();
}
</script>
<script src="//4690y10pvpq8.com/d5/61/18/d561181177a776f3d5a38102426f1462.js" type="text/javascript"></script>
<div style="display:none;">
<!--LiveInternet counter-->
<script type="text/javascript">
previewEvents();
<!--
document.write("<a href='http://www.liveinternet.ru/click' " +
"target=_blank><img src='//counter.yadro.ru/hit?t26.6;r" +
escape(document.referrer) + ((typeof (screen) == "undefined") ? "" :
";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ?
screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) +
";" + Math.random() +
"' alt='' title='LiveInternet: shows the number of visitors for" +
" today' " +
"border='0' width='88' height='15'><\/a>")
//-->
</script>
<!--/LiveInternet-->
</div>
<script async="" src="/js/app.js?f74331" type="text/javascript"></script>
</body>
</html>