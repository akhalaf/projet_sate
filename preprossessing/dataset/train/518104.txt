<!DOCTYPE html>
<html class="html_stretched responsive av-preloader-disabled av-default-lightbox html_header_top html_logo_left html_main_nav_header html_menu_right html_slim html_header_sticky html_header_shrinking_disabled html_header_topbar_active html_mobile_menu_phone html_header_searchicon_disabled html_content_align_center html_header_unstick_top html_header_stretch html_minimal_header html_av-submenu-hidden html_av-submenu-display-click html_av-overlay-side html_av-overlay-side-classic html_av-submenu-noclone av-cookies-no-cookie-consent av-no-preview html_text_menu_active " lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="noindex, follow" name="robots"/>
<!-- mobile setting -->
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- Scripts/CSS and wp_head hook -->
<title>Page not found – The Educational, Welfare and Research Foundation Malaysia</title>
<link href="//js.stripe.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://ewrf.org.my/feed/" rel="alternate" title="The Educational, Welfare and Research Foundation Malaysia » Feed" type="application/rss+xml"/>
<link href="https://ewrf.org.my/comments/feed/" rel="alternate" title="The Educational, Welfare and Research Foundation Malaysia » Comments Feed" type="application/rss+xml"/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.13.2 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script data-cfasync="false" type="text/javascript">
    (window.gaDevIds=window.gaDevIds||[]).push("dZGIzZG");
	var mi_version         = '7.13.2';
	var mi_track_user      = true;
	var mi_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-180656561-1';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}

	if ( 'undefined' === typeof gaOptout ) {
		function gaOptout() {
			__gaTrackerOptout();
		}
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-180656561-1', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('require', 'displayfeatures');
		__gaTracker('require', 'linkid', 'linkid.js');
		__gaTracker('send','pageview','/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer);
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<!-- / Google Analytics by MonsterInsights -->
<!-- google webfont font replacement -->
<script type="text/javascript">

				(function() {
					
					/*	check if webfonts are disabled by user setting via cookie - or user must opt in.	*/
					var html = document.getElementsByTagName('html')[0];
					var cookie_check = html.className.indexOf('av-cookies-needs-opt-in') >= 0 || html.className.indexOf('av-cookies-can-opt-out') >= 0;
					var allow_continue = true;
					var silent_accept_cookie = html.className.indexOf('av-cookies-user-silent-accept') >= 0;

					if( cookie_check && ! silent_accept_cookie )
					{
						if( ! document.cookie.match(/aviaCookieConsent/) || html.className.indexOf('av-cookies-session-refused') >= 0 )
						{
							allow_continue = false;
						}
						else
						{
							if( ! document.cookie.match(/aviaPrivacyRefuseCookiesHideBar/) )
							{
								allow_continue = false;
							}
							else if( ! document.cookie.match(/aviaPrivacyEssentialCookiesEnabled/) )
							{
								allow_continue = false;
							}
							else if( document.cookie.match(/aviaPrivacyGoogleWebfontsDisabled/) )
							{
								allow_continue = false;
							}
						}
					}
					
					if( allow_continue )
					{
						var f = document.createElement('link');
					
						f.type 	= 'text/css';
						f.rel 	= 'stylesheet';
						f.href 	= '//fonts.googleapis.com/css?family=Open+Sans:400,600';
						f.id 	= 'avia-google-webfont';

						document.getElementsByTagName('head')[0].appendChild(f);
					}
				})();
			
			</script>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/ewrf.org.my\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://ewrf.org.my/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ewrf.org.my/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.1" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ewrf.org.my/wp-content/plugins/give/assets/dist/css/give.css?ver=2.9.5" id="give-styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ewrf.org.my/wp-content/plugins/give-form-field-manager/assets/css/give-ffm-frontend.min.css?ver=1.4.9" id="give_ffm_frontend_styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ewrf.org.my/wp-content/plugins/give-form-field-manager/assets/css/give-ffm-datepicker.min.css?ver=1.4.9" id="give_ffm_datepicker_styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ewrf.org.my/wp-content/plugins/google-analytics-for-wordpress/assets/css/frontend.min.css?ver=7.13.2" id="monsterinsights-popular-posts-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ewrf.org.my/wp-content/plugins/give-recurring/assets/css/give-recurring.css?ver=1.11.2" id="give_recurring_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ewrf.org.my/wp-content/uploads/dynamic_avia/avia-merged-styles-6380c512da73b6911e570b8bac88d55e---5fe3b2e5031d6.css" id="avia-merged-styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ewrf.org.my/wp-content/uploads/2020/12/csshero-static-style-enfold-child.css?ver=172" id="csshero-main-stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://ewrf.org.my/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://ewrf.org.my/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="jquery-ui-core-js" src="https://ewrf.org.my/wp-includes/js/jquery/ui/core.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="jquery-ui-datepicker-js" src="https://ewrf.org.my/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="jquery-ui-datepicker-js-after" type="text/javascript">
jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Close","currentText":"Today","monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Previous","dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"dateFormat":"MM d, yy","firstDay":1,"isRTL":false});});
</script>
<script id="jquery-ui-mouse-js" src="https://ewrf.org.my/wp-includes/js/jquery/ui/mouse.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="jquery-ui-slider-js" src="https://ewrf.org.my/wp-includes/js/jquery/ui/slider.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="moxiejs-js" src="https://ewrf.org.my/wp-includes/js/plupload/moxie.min.js?ver=1.3.5" type="text/javascript"></script>
<script id="plupload-js" src="https://ewrf.org.my/wp-includes/js/plupload/plupload.min.js?ver=2.1.9" type="text/javascript"></script>
<script id="plupload-handlers-js-extra" type="text/javascript">
/* <![CDATA[ */
var pluploadL10n = {"queue_limit_exceeded":"You have attempted to queue too many files.","file_exceeds_size_limit":"%s exceeds the maximum upload size for this site.","zero_byte_file":"This file is empty. Please try another.","invalid_filetype":"Sorry, this file type is not permitted for security reasons.","not_an_image":"This file is not an image. Please try another.","image_memory_exceeded":"Memory exceeded. Please try another smaller file.","image_dimensions_exceeded":"This is larger than the maximum size. Please try another.","default_error":"An error occurred in the upload. Please try again later.","missing_upload_url":"There was a configuration error. Please contact the server administrator.","upload_limit_exceeded":"You may only upload 1 file.","http_error":"Unexpected response from the server. The file may have been uploaded successfully. Check in the Media Library or reload the page.","http_error_image":"Post-processing of the image failed likely because the server is busy or does not have enough resources. Uploading a smaller image may help. Suggested maximum size is 2500 pixels.","upload_failed":"Upload failed.","big_upload_failed":"Please try uploading this file with the %1$sbrowser uploader%2$s.","big_upload_queued":"%s exceeds the maximum upload size for the multi-file uploader when used in your browser.","io_error":"IO error.","security_error":"Security error.","file_cancelled":"File canceled.","upload_stopped":"Upload stopped.","dismiss":"Dismiss","crunching":"Crunching\u2026","deleted":"moved to the Trash.","error_uploading":"\u201c%s\u201d has failed to upload.","unsupported_image":"This image cannot be displayed in a web browser. For best results convert it to JPEG before uploading."};
/* ]]> */
</script>
<script id="plupload-handlers-js" src="https://ewrf.org.my/wp-includes/js/plupload/handlers.min.js?ver=5.6" type="text/javascript"></script>
<script id="give_ffm_frontend-js-extra" type="text/javascript">
/* <![CDATA[ */
var give_ffm_frontend = {"ajaxurl":"https:\/\/ewrf.org.my\/wp-admin\/admin-ajax.php","error_message":"Please complete all required fields","submit_button_text":"Donate Now","nonce":"b40d52e923","confirmMsg":"Are you sure?","i18n":{"timepicker":{"choose_time":"Choose Time","time":"Time","hour":"Hour","minute":"Minute","second":"Second","done":"Done","now":"Now"},"repeater":{"max_rows":"You have added the maximum number of fields allowed."}},"plupload":{"url":"https:\/\/ewrf.org.my\/wp-admin\/admin-ajax.php?nonce=aa35a82ab1","flash_swf_url":"https:\/\/ewrf.org.my\/wp-includes\/js\/plupload\/plupload.flash.swf","filters":[{"title":"Allowed Files","extensions":"*"}],"multipart":true,"urlstream_upload":true}};
/* ]]> */
</script>
<script id="give_ffm_frontend-js" src="https://ewrf.org.my/wp-content/plugins/give-form-field-manager/assets/js/frontend/give-ffm-frontend.min.js?ver=1.4.9" type="text/javascript"></script>
<script id="monsterinsights-frontend-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[{\"path\":\"\\\/go\\\/\",\"label\":\"affiliate\"},{\"path\":\"\\\/recommend\\\/\",\"label\":\"affiliate\"}]","home_url":"https:\/\/ewrf.org.my","hash_tracking":"false"};
/* ]]> */
</script>
<script id="monsterinsights-frontend-script-js" src="https://ewrf.org.my/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.13.2" type="text/javascript"></script>
<script id="give-stripe-js-js-extra" type="text/javascript">
/* <![CDATA[ */
var give_stripe_vars = {"zero_based_currency":"","zero_based_currencies_list":["JPY","KRW","CLP","ISK","BIF","DJF","GNF","KHR","KPW","LAK","LKR","MGA","MZN","VUV"],"sitename":"The Educational, Welfare and Research Foundation Malaysia","checkoutBtnTitle":"Donate","publishable_key":"pk_live_51HEVWTBOXfuo3gmwDUCjYs5Ga9izNc1lviWHisWZAcGmOgYc5L84gsi1Ir1x1OvgH9ZuKqIua1TUUcifIZ6bD8PU00iHscsVGf","checkout_image":"https:\/\/ewrf.org.my\/wp-content\/uploads\/2020\/08\/EWRF-01-1-e1597213674487.png","checkout_address":"on","checkout_processing_text":"Donation Processing...","give_version":"2.9.5","cc_fields_format":"multi","card_number_placeholder_text":"Card Number","card_cvc_placeholder_text":"CVC","donate_button_text":"Donate Now","element_font_styles":{"cssSrc":false},"element_base_styles":{"color":"#32325D","fontWeight":500,"fontSize":"16px","fontSmoothing":"antialiased","::placeholder":{"color":"#CCCCCC"},":-webkit-autofill":{"color":"#e39f48"}},"element_complete_styles":{},"element_empty_styles":{},"element_invalid_styles":{},"float_labels":"1","base_country":"MY","preferred_locale":"en","stripe_card_update":"","stripe_becs_update":""};
/* ]]> */
</script>
<script id="give-stripe-js-js" src="https://js.stripe.com/v3/?ver=2.9.5" type="text/javascript"></script>
<script id="give-stripe-onpage-js-js" src="https://ewrf.org.my/wp-content/plugins/give/assets/dist/js/give-stripe.js?ver=2.9.5" type="text/javascript"></script>
<script id="babel-polyfill-js" src="https://ewrf.org.my/wp-content/plugins/give/assets/dist/js/babel-polyfill.js?ver=2.9.5" type="text/javascript"></script>
<script id="give-js-extra" type="text/javascript">
/* <![CDATA[ */
var give_global_vars = {"ajaxurl":"https:\/\/ewrf.org.my\/wp-admin\/admin-ajax.php","checkout_nonce":"b1ef514c8d","currency":"MYR","currency_sign":"RM","currency_pos":"before","thousands_separator":",","decimal_separator":".","no_gateway":"Please select a payment method.","bad_minimum":"The minimum custom donation amount for this form is","bad_maximum":"The maximum custom donation amount for this form is","general_loading":"Loading...","purchase_loading":"Please Wait...","textForOverlayScreen":"<h3>Processing...<\/h3><p>This will only take a second!<\/p>","number_decimals":"2","is_test_mode":"","give_version":"2.9.5","magnific_options":{"main_class":"give-modal","close_on_bg_click":false},"form_translation":{"payment-mode":"Please select payment mode.","give_first":"Please enter your first name.","give_email":"Please enter a valid email address.","give_user_login":"Invalid email address or username.","give_user_pass":"Enter a password.","give_user_pass_confirm":"Enter the password confirmation.","give_agree_to_terms":"You must agree to the terms and conditions."},"confirm_email_sent_message":"Please check your email and click on the link to access your complete donation history.","ajax_vars":{"ajaxurl":"https:\/\/ewrf.org.my\/wp-admin\/admin-ajax.php","ajaxNonce":"34e2a0eddc","loading":"Loading","select_option":"Please select an option","default_gateway":"stripe","permalinks":"1","number_decimals":2},"cookie_hash":"a88e3a6611f00fdb7b4d09812c560674","session_nonce_cookie_name":"wp-give_session_reset_nonce_a88e3a6611f00fdb7b4d09812c560674","session_cookie_name":"wp-give_session_a88e3a6611f00fdb7b4d09812c560674","delete_session_nonce_cookie":"0"};
var giveApiSettings = {"root":"https:\/\/ewrf.org.my\/wp-json\/give-api\/v2\/","rest_base":"give-api\/v2"};
/* ]]> */
</script>
<script id="give-js" src="https://ewrf.org.my/wp-content/plugins/give/assets/dist/js/give.js?ver=2.9.5" type="text/javascript"></script>
<script id="give_recurring_script-js-extra" type="text/javascript">
/* <![CDATA[ */
var Give_Recurring_Vars = {"email_access":"","pretty_intervals":{"1":"Every","2":"Every two","3":"Every three","4":"Every four","5":"Every five","6":"Every six"},"pretty_periods":{"day":"Daily","week":"Weekly","month":"Monthly","quarter":"Quarterly","half-year":"Semi-Annually","year":"Yearly"},"messages":{"daily_forbidden":"The selected payment method does not support daily recurring giving. Please select another payment method or supported giving frequency.","confirm_cancel":"Are you sure you want to cancel this subscription?"},"multi_level_message_pre_text":"You have chosen to donate"};
/* ]]> */
</script>
<script id="give_recurring_script-js" src="https://ewrf.org.my/wp-content/plugins/give-recurring/assets/js/give-recurring.js?ver=1.11.2" type="text/javascript"></script>
<link href="https://ewrf.org.my/wp-json/" rel="https://api.w.org/"/><link href="https://ewrf.org.my/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://ewrf.org.my/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<meta content="Give v2.9.5" name="generator"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<link href="https://ewrf.org.my/feed/" rel="alternate" title="The Educational, Welfare and Research Foundation Malaysia RSS2 Feed" type="application/rss+xml"/>
<link href="https://ewrf.org.my/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]><script src="https://ewrf.org.my/wp-content/themes/enfold/js/html5shiv.js"></script><![endif]-->
<!-- To speed up the rendering and to display the site as fast as possible to the user we include some styles and scripts for above the fold content inline -->
<script type="text/javascript">'use strict';var avia_is_mobile=!1;if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)&&'ontouchstart' in document.documentElement){avia_is_mobile=!0;document.documentElement.className+=' avia_mobile '}
else{document.documentElement.className+=' avia_desktop '};document.documentElement.className+=' js_active ';(function(){var e=['-webkit-','-moz-','-ms-',''],n='';for(var t in e){if(e[t]+'transform' in document.documentElement.style){document.documentElement.className+=' avia_transform ';n=e[t]+'transform'};if(e[t]+'perspective' in document.documentElement.style)document.documentElement.className+=' avia_transform3d '};if(typeof document.getElementsByClassName=='function'&&typeof document.documentElement.getBoundingClientRect=='function'&&avia_is_mobile==!1){if(n&&window.innerHeight>0){setTimeout(function(){var e=0,o={},a=0,t=document.getElementsByClassName('av-parallax'),i=window.pageYOffset||document.documentElement.scrollTop;for(e=0;e<t.length;e++){t[e].style.top='0px';o=t[e].getBoundingClientRect();a=Math.ceil((window.innerHeight+i-o.top)*0.3);t[e].style[n]='translate(0px, '+a+'px)';t[e].style.top='auto';t[e].className+=' enabled-parallax '}},50)}}})();</script><link href="https://ewrf.org.my/wp-content/uploads/2018/01/cropped-favicon-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://ewrf.org.my/wp-content/uploads/2018/01/cropped-favicon-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://ewrf.org.my/wp-content/uploads/2018/01/cropped-favicon-180x180.png" rel="apple-touch-icon"/>
<meta content="https://ewrf.org.my/wp-content/uploads/2018/01/cropped-favicon-270x270.png" name="msapplication-TileImage"/>
<style id="wp-custom-css" type="text/css">
			@media screen and (max-width: 568px)
#top.page-id-3381 #after_full_slider_1 .av_textblock_section .avia_textblock > p {
    /* FONT-WEIGHT: 100; */
    text-align: justify;
    letter-spacing: 0px;
    margin-left: -10px;
    padding-right: 10px;
    padding-left: 10px;
    margin-right: 10px;
    margin-top: -3px;
}


/*-- Membership Form Subheading Font size (Membership Form Geevan) 

#top.page-id-6107 #av_section_1 .av-special-heading .av-subheading p {
    margin-bottom: 40px;
    color: black;
    font-size: 18px;
    padding-right: 10px;
}

*/


/* Membership Form -Padding--
html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
    margin: 0;
    padding: 0px;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
}

*/

#membership_geevan {
 background-color:grey;
}	

#contact_form_7 {
	background-color: grey;
	padding:10px 10px;
	color: #ffffff
}

	
			</style>
<style type="text/css">
@font-face {font-family: 'entypo-fontello'; font-weight: normal; font-style: normal; font-display: auto;
src: url('https://ewrf.org.my/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.woff2') format('woff2'),
url('https://ewrf.org.my/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.woff') format('woff'),
url('https://ewrf.org.my/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.ttf') format('truetype'), 
url('https://ewrf.org.my/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.svg#entypo-fontello') format('svg'),
url('https://ewrf.org.my/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.eot'),
url('https://ewrf.org.my/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.eot?#iefix') format('embedded-opentype');
} #top .avia-font-entypo-fontello, body .avia-font-entypo-fontello, html body [data-av_iconfont='entypo-fontello']:before{ font-family: 'entypo-fontello'; }
</style>
<!--
Debugging Info for Theme support: 

Theme: Enfold
Version: 4.7.6.1
Installed: enfold
AviaFramework Version: 5.0
AviaBuilder Version: 4.7.1.1
aviaElementManager Version: 1.0.1
- - - - - - - - - - -
ChildTheme: Enfold Child
ChildTheme Version: 1.0
ChildTheme Installed: enfold

ML:512-PU:16-PLA:19
WP:5.6
Compress: CSS:all theme files - JS:all theme files
Updates: disabled
PLAu:15
-->
</head>
<body class="error404 rtl_columns stretched helvetica-websave helvetica open_sans give-recurring" data-rsssl="1" id="top" itemscope="itemscope" itemtype="https://schema.org/WebPage">
<div id="wrap_all">
<header class="all_colors header_color light_bg_color av_header_top av_logo_left av_main_nav_header av_menu_right av_slim av_header_sticky av_header_shrinking_disabled av_header_stretch av_mobile_menu_phone av_header_searchicon_disabled av_header_unstick_top av_minimal_header av_bottom_nav_disabled av_alternate_logo_active av_header_border_disabled" id="header" itemscope="itemscope" itemtype="https://schema.org/WPHeader" role="banner">
<div class="container_wrap container_wrap_meta av_icon_active_right av_extra_header_active av_phone_active_left" id="header_meta">
<div class="container">
<ul class="noLightbox social_bookmarks icon_count_4"><li class="social_bookmarks_twitter av-social-link-twitter social_icon_1"><a aria-hidden="false" aria-label="Link to Twitter" data-av_icon="" data-av_iconfont="entypo-fontello" href="https://twitter.com/EWRFMalaysia" rel="noopener" target="_blank" title="Twitter"><span class="avia_hidden_link_text">Twitter</span></a></li><li class="social_bookmarks_youtube av-social-link-youtube social_icon_2"><a aria-hidden="false" aria-label="Link to Youtube" data-av_icon="" data-av_iconfont="entypo-fontello" href="https://www.youtube.com/channel/UCIaptV4N7qMROTl_njwbYPg/videos" rel="noopener" target="_blank" title="Youtube"><span class="avia_hidden_link_text">Youtube</span></a></li><li class="social_bookmarks_facebook av-social-link-facebook social_icon_3"><a aria-hidden="false" aria-label="Link to Facebook" data-av_icon="" data-av_iconfont="entypo-fontello" href="https://www.facebook.com/EWRFMALAYSIA/" rel="noopener" target="_blank" title="Facebook"><span class="avia_hidden_link_text">Facebook</span></a></li><li class="social_bookmarks_instagram av-social-link-instagram social_icon_4"><a aria-hidden="false" aria-label="Link to Instagram" data-av_icon="" data-av_iconfont="entypo-fontello" href="https://www.instagram.com/ewrfofficial/" rel="noopener" target="_blank" title="Instagram"><span class="avia_hidden_link_text">Instagram</span></a></li></ul><div class="phone-info "><span>The Educational, Welfare and Research Foundation Malaysia (PPM-001-14-02011979)</span></div> </div>
</div>
<div class="container_wrap container_wrap_logo" id="header_main">
<div class="container av-logo-container"><div class="inner-container"><span class="logo"><a href="https://ewrf.org.my/"><img alt="The Educational, Welfare and Research Foundation Malaysia" height="100" src="https://ewrf.org.my/wp-content/uploads/2020/08/EWRF-01-1-300x300.png" title="" width="300"/></a></span><nav class="main_menu" data-selectname="Select a page" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement" role="navigation"><div class="avia-menu av-main-nav-wrap"><ul class="menu av-main-nav" id="avia-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-top-level menu-item-top-level-1" id="menu-item-3397"><a href="https://ewrf.org.my/" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">HOME</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-top-level menu-item-top-level-2" id="menu-item-3698"><a href="https://ewrf.org.my/about-us/" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">ABOUT US</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-top-level menu-item-top-level-3" id="menu-item-4564"><a itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">PROJECTS</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-3696"><a href="https://ewrf.org.my/projects/english-for-juniors/" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">ENGLISH FOR JUNIORS (E4J)</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-4126"><a href="https://ewrf.org.my/projects/civil-society-intervention/" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">CIVIL SOCIETY INTERVENTION</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-5379"><a href="https://ewrf.org.my/projects/skill-training-initiative/" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">SKILL TRAINING INITIATIVE</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-4125"><a href="https://ewrf.org.my/projects/counseling/" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">COUNSELING</span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-mega-parent menu-item-top-level menu-item-top-level-4" id="menu-item-6579"><a href="https://ewrf.org.my/projects/resources-2/" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">RESOURCES</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-top-level menu-item-top-level-5" id="menu-item-3395"><a href="https://ewrf.org.my/contact-us/" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">CONTACT US</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-give_forms menu-item-top-level menu-item-top-level-6" id="menu-item-5522"><a href="https://ewrf.org.my/donations/donation-form/" itemprop="url" title="DONATION"><span class="avia-bullet"></span><span class="avia-menu-text">DONATION</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
<li class="av-burger-menu-main menu-item-avia-special ">
<a aria-hidden="false" aria-label="Menu" href="#">
<span class="av-hamburger av-hamburger--spin av-js-hamburger">
<span class="av-hamburger-box">
<span class="av-hamburger-inner"></span>
<strong>Menu</strong>
</span>
</span>
<span class="avia_hidden_link_text">Menu</span>
</a>
</li></ul></div></nav></div> </div>
<!-- end container_wrap-->
</div>
<div class="header_bg"></div>
<!-- end header -->
</header>
<div class="all_colors" data-scroll-offset="88" id="main">
<div class="stretch_full container_wrap alternate_color light_bg_color title_container"><div class="container"><h1 class="main-title entry-title ">Error 404 - page not found</h1></div></div>
<div class="container_wrap container_wrap_first main_color fullsize">
<div class="container">
<main class="template-page content av-content-full alpha units" itemprop="mainContentOfPage" role="main">
<div class="entry entry-content-wrapper clearfix" id="search-fail">
<p class="entry-content"><strong>Nothing Found</strong><br/>

Sorry, the post you are looking for is not available. Maybe you want to perform a search?</p>
<form action="https://ewrf.org.my/" class="" id="searchform" method="get">
<div>
<input class="button avia-font-entypo-fontello" id="searchsubmit" type="submit" value=""/>
<input id="s" name="s" placeholder="Search" type="text" value=""/>
</div>
</form>
<div class="hr_invisible"></div>
<section class="404_recommendation">
<p><strong>For best search results, mind the following suggestions:</strong></p>
<ul class="borderlist-not">
<li>Always double check your spelling.</li>
<li>Try similar keywords, for example: tablet instead of laptop.</li>
<li>Try using more than one keyword.</li>
</ul>
<div class="hr_invisible"></div>
</section>
</div>
<!--end content-->
</main>
</div><!--end container-->
</div><!-- close default .container_wrap element -->
<div class="container_wrap footer_color" id="footer">
<div class="container">
<div class="flex_column av_one_third first el_before_av_one_third"><section class="widget clearfix widget_text" id="text-2"><h3 class="widgettitle">EWRF Headquarters</h3> <div class="textwidget"><p>4B, PERSIARAN ZAABA,</p>
<p>TAMAN TUN DR. ISMAIL,</p>
<p>60000 KUALA LUMPUR,</p>
<p>MALAYSIA</p>
<p>TEL: 03-7710 0140/42</p>
<p>FAX: 03-7710 0096</p>
<p>E-MAIL: <a href="mailto:admin@ewrf.org.my">admin@ewrf.org.my</a></p>
</div>
<span class="seperator extralight-border"></span></section></div><div class="flex_column av_one_third el_after_av_one_third el_before_av_one_third "><section class="widget clearfix widget_pages" id="pages-3"><h3 class="widgettitle">Site Map</h3>
<ul>
<li class="page_item page-item-3381"><a href="https://ewrf.org.my/">HOME</a></li>
<li class="page_item page-item-3390"><a href="https://ewrf.org.my/contact-us/">CONTACT US</a></li>
<li class="page_item page-item-3677 page_item_has_children"><a href="https://ewrf.org.my/projects/">PROJECTS</a>
<ul class="children">
<li class="page_item page-item-3691"><a href="https://ewrf.org.my/projects/english-for-juniors/">ENGLISH FOR JUNIORS</a></li>
<li class="page_item page-item-4117"><a href="https://ewrf.org.my/projects/civil-society-intervention/">CIVIL SOCIETY INTERVENTION</a></li>
<li class="page_item page-item-4121"><a href="https://ewrf.org.my/projects/counseling/">COUNSELING</a></li>
<li class="page_item page-item-5375"><a href="https://ewrf.org.my/projects/skill-training-initiative/">SKILL TRAINING INITIATIVE</a></li>
<li class="page_item page-item-6511"><a href="https://ewrf.org.my/projects/resources-2/">RESOURCES</a></li>
</ul>
</li>
<li class="page_item page-item-3687"><a href="https://ewrf.org.my/about-us/">ABOUT US</a></li>
</ul>
<span class="seperator extralight-border"></span></section></div><div class="flex_column av_one_third el_after_av_one_third el_before_av_one_third "><section class="widget clearfix widget_text" id="text-4"><h3 class="widgettitle">Our Working Hours</h3> <div class="textwidget"><p>Mondays to Fridays: 8:30 a.m.- 5.00 p.m.<br/>
Saturdays: 8:30 p.m. – 1.00 p.m.</p>
<p>We are closed on Sundays and Public Holidays</p>
</div>
<span class="seperator extralight-border"></span></section></div>
</div>
<!-- ####### END FOOTER CONTAINER ####### -->
</div>
<footer class="container_wrap socket_color" id="socket" itemscope="itemscope" itemtype="https://schema.org/WPFooter" role="contentinfo">
<div class="container">
<span class="copyright">© Copyright 2020- The Educational, Welfare &amp; Research Foundation Malaysia </span>
<ul class="noLightbox social_bookmarks icon_count_4"><li class="social_bookmarks_twitter av-social-link-twitter social_icon_1"><a aria-hidden="false" aria-label="Link to Twitter" data-av_icon="" data-av_iconfont="entypo-fontello" href="https://twitter.com/EWRFMalaysia" rel="noopener" target="_blank" title="Twitter"><span class="avia_hidden_link_text">Twitter</span></a></li><li class="social_bookmarks_youtube av-social-link-youtube social_icon_2"><a aria-hidden="false" aria-label="Link to Youtube" data-av_icon="" data-av_iconfont="entypo-fontello" href="https://www.youtube.com/channel/UCIaptV4N7qMROTl_njwbYPg/videos" rel="noopener" target="_blank" title="Youtube"><span class="avia_hidden_link_text">Youtube</span></a></li><li class="social_bookmarks_facebook av-social-link-facebook social_icon_3"><a aria-hidden="false" aria-label="Link to Facebook" data-av_icon="" data-av_iconfont="entypo-fontello" href="https://www.facebook.com/EWRFMALAYSIA/" rel="noopener" target="_blank" title="Facebook"><span class="avia_hidden_link_text">Facebook</span></a></li><li class="social_bookmarks_instagram av-social-link-instagram social_icon_4"><a aria-hidden="false" aria-label="Link to Instagram" data-av_icon="" data-av_iconfont="entypo-fontello" href="https://www.instagram.com/ewrfofficial/" rel="noopener" target="_blank" title="Instagram"><span class="avia_hidden_link_text">Instagram</span></a></li></ul>
</div>
<!-- ####### END SOCKET CONTAINER ####### -->
</footer>
<!-- end main -->
</div>
<!-- end wrap_all --></div>
<a aria-hidden="true" data-av_icon="" data-av_iconfont="entypo-fontello" href="#top" id="scroll-top-link" title="Scroll to top"><span class="avia_hidden_link_text">Scroll to top</span></a>
<div id="fb-root"></div>
<script type="text/javascript">
 /* <![CDATA[ */  
var avia_framework_globals = avia_framework_globals || {};
    avia_framework_globals.frameworkUrl = 'https://ewrf.org.my/wp-content/themes/enfold/framework/';
    avia_framework_globals.installedAt = 'https://ewrf.org.my/wp-content/themes/enfold/';
    avia_framework_globals.ajaxurl = 'https://ewrf.org.my/wp-admin/admin-ajax.php';
/* ]]> */ 
</script>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/ewrf.org.my\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://ewrf.org.my/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.1" type="text/javascript"></script>
<script id="wp-embed-js" src="https://ewrf.org.my/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
<script id="avia-footer-scripts-js" src="https://ewrf.org.my/wp-content/uploads/dynamic_avia/avia-footer-scripts-0874a5b4e8e63229997eeebe696841b3---5fe3b2e532662.js" type="text/javascript"></script>
</body>
</html>
