<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "66805",
      cRay: "61112047ab750bae",
      cHash: "a8b95c93970d718",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYml0cmFkLmlvLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "1nfrQwLu71YOMs0RWJozn4sVSeNralw52OFNQGB5IMYzAVQMlZP8gtgrbdOZtDrtTca4524TmL51QoRIwOOzZsZ6oSpBL89ftV858UG4sQV38OG9JahqicXgMd0epxOvuoc+Wf7chjYDw+6Im86e+LXs61tkzvyDYsWRae6nKubYbfDRjPf+Zf3aRaV8VI51mL+FOZzvM6dfi6zXKMu13u2hbaNWejv0jXWic98UtO19V9GxZjUpQK4vcAPsvT7LB0NWC3N+m/WQc+cCBmNSfU0jUAzsGrcKyu2hdH+dCb1J5rF9ORAi7zMhICznQ4kXeTlBN9VIPDSCIYba1zFm3OcO2/q6PxRo6QAqUuA+4fT9Uf6nOO2WvFKvaI1R7hZH3kNcC9apjMmYvYZ/Me4djSHygmwww6X9wEjRhckZualwuF8Omf9X/w5Zv4qy3MfuNspY+uIEEjKePucb6LLBzzEmv/SXIUmlAKIpy5IOPAF678mW/uR6Y96/1k8BxsCyYE+/7lYO86tGnL2fGRjwVI5+QiDiaV1tkFImgWWMJxt3Mq6D4YCzgKMC724HnVvnxh0K2V9dHLPYfGu+BF4Cwkcwbb5w2/lSGaXi7XkPFIDnPrXGD+3sBGMhQLNpWU7Jn3R0QwwExYE6035zqqSpz+UKYwc2nocBior0qD3kt5kT2E7tPsGxNhk593swa6s7cFSf3RiP21Z8eFE7Twt1LO1muoHTy8X6CA6C+tWXjzJvtznExtKVldSl9dktiKz9",
        t: "MTYxMDU2MTk3Mi40MjcwMDA=",
        m: "ZHcElbfsSkGA1m+DV5UP1b59lJZaQIvWBeuWfgysVzY=",
        i1: "Xcv+YOPbmdU4OoFOOq+PGA==",
        i2: "V1/LaeKB8uVvpliHFA1IQQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "If/zlpdzMP7b8a0LkdALvxitU4uanoviGl+UyqxeB4g=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=61112047ab750bae");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> bitrad.io.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=81f7e94e4fcc1b3e154ccec212b02d99be986aa5-1610561972-0-Ad5SJfD8w8MMm_fsPJT0d_OLAhGBjiY--nsZnNA38RBnZ7BTeZ6rw2n-u2pq7eltH3awcHsweg-p6s6aykGI7xGc6057NoJkiR331Kw1bj8pcm64sxvEJ10oEf0zq5JoO9Ie5G9bnRxYJt3yPypMHGYNYsadYTtY8QFg8qL0QE9AQ8epO9jMQ4ULswsZ-qx9xM5RqfRVXyHYrRFHAAsb0DfnGVuls7-NPKtMGYnYQlHRBM22ijSJ8rLEdDtWIJdY6mUg_0ggAYQ2pVqkqIYWyPH83MdxTf2GMSJt84KNV-V9QruDwWtVJKhSXLNJ2E4LKQ" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="0b8a43842a6978682848e8382d645d6634681296-1610561972-0-Aczg3B7j+h/sWAucRYqiNL9+fYlysM0Y98jdwlPOVZ0L1aJhjBR8oOm0BUv0UAcpqVa9zG1CPvre7+NMQywoEM5pq+G298zvreiEdw/aosfKGKJ/rBdp9UNtZBT/ZvqN0pOlVaHlr7FgayCYLyd1824pEAdMUXeWQbMY5eh3fCB4zrfuU6fGO1C/zqVsEOQb5duQFZKyvrar1pS17fDNFFclnolelyXSO4fT0lm1TRdf9lvfk2jmRJValA/J4F3uWEiJoYW725BWXr54ZTqwaViyaOGmKXizlzkzx0fwrxkjVMQNpncJU+8au1QTGs4F/uqey6JyXw3kVbtJW2GVGFKpRl1k96CZOU5+xE4f07TUw5RDTekwtR7E3Hfe5oJah+yG/vQeZUin0ir/TjLltfRY80lLtmr7Mfe57ivzpqd+KSUkHb/Qdabk0e/nCJZCnOS1Kmqbe4mbj6ulwprtOdy2egL8/n9tdSVRz+LHagQTNTyBW+FRbH+9XhaaBAtTZaIjAOYBru0KXjCVOWblZ3TyBPh2dQ8bIflVbJCN6OC4RENbW6FY7iKiE57VcGjq1sLse7LBP4cBuhL6VA/k1ioAQUM0vJaRtkVTtW/i5TzkziRRiiyBq65O37FeszG+bXqmkdkyDKHTLawa3IeT1ETBlhFhwBfrNbWfD4RgnsrOPraKDMTqUZzD2b3057yeB5KLaG5eBoSBj1eEly00f7DPIJzR7IgqIn29h6OOnBQpPq3lYxGIjSWYszKiN9zJ7Y1uBe4v1O3dkM5qaFu69uj6I/Ht874OK/4KvSAt/nqw7l6oMBTpVG6exi9+puFr5yFooQrpEXX5bv31ojIitiJ/qCLb9opKWgrfnucodJNiSNvKlL6/uy3IkB2NdfAAn1PpbTh+cdw2kxiQm5lEWyJ8Bg8Ox6qk8SHDU2Qj3C0xFzKWJFMHzZqoC6J+KqI3tNdHm0fRT2pBGstRZut5aErpbZDgqlRGJ5dp+sssWaqzWMU/XjtmUbC5IXQRQ0fo341r+ghNWOkKk/2YgfF1yAkq5NumhoEosTv7actJ2ybZ2PQ3aY6Vb/sTBBUxxM/LAgT6BWEOPEM03Z36bb0fQv+1OkyT8TrHWf9f4fVffNzqD2mLvzysdkBWARfNF5sxyAQHCpDQ003/R4QNHIlDg2jr0QpsEufB6mgTgX66G0tUosgfMnqVFu9eHuU2B+Zx0sDnzJ3gLO0dZqn7rvsP23kNRYQhxSljc7avWTY7C5VWw3xXbG07sd3zhKzrC07b3jeqjOCY/jMF2fgJ0QscpiTY/gMMneOYWVIDq24nhSjv"/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="0e16e527c2dfa7d267cd741c43db1b6e"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610561976.427-UAK+hVIqky"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=61112047ab750bae')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>61112047ab750bae</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
