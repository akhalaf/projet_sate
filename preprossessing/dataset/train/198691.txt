<!DOCTYPE html>
<html lang="ru"><head><meta content="width=device-width, initial-scale=1" name="viewport"/><meta content="IE=Edge" http-equiv="X-UA-Compatible"/><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><base href="https://bazaman.ru/"/><link href="design/bz/favicon.ico" rel="shortcut icon" type="image/x-icon"/><title>Поиск людей, бесплатная база данных людей</title><meta content="поиск людей, база данных людей, бесплатно, поиск людей" name="keywords"/><meta content="bazaman.ru - бесплатная online-база данных людей для поиска нужного человека по всему миру" name="description"/><meta content="index, follow" name="robots"/><meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport"/><link as="style" href="lib/fancy/jquery.fancybox.css" rel="preload"/><link as="style" href="lib/pack.css" rel="preload"/><link as="font" crossorigin="" href="design/bz/fonts/ProximaNova-Bold.woff2" rel="preload"/><link href="design/bz/css/css.css?v.4" rel="stylesheet" type="text/css"/><meta content="CwMUCi9Vt4N5Rda5hJVyqKUtpeZ0XBSKjA2HnA1Cuao" name="google-site-verification"/><meta content="481cfbdb0d9260d2" name="yandex-verification"/><meta content="2c82dad20c5e3af07989ff09dbcaf62a332ea040" name="openstat-verification"/> <meta content="zpgu01Er4X8kCK87" name="mailru-domain"/>
<script type="text/javascript">var site_referer = '';</script>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-7746697709453826",
		    enable_page_level_ads: true
		  });
		</script>
</head><body><div id="layuot"><!--header--><div class="clear" id="header"><div class="floor"><a href=""><img alt="BazaMan.ru - бесплатный сервис по поиску людей" id="logo" src="design/bz/images/logotip.svg"/></a><div id="top-navigation"><input id="user-menu" type="checkbox" value=""/><label class="user-menu" for="user-menu"><span class="fa"></span></label><ul class="nonelist clear slow"><li><a class="slow" href="rossiya/">Россия</a></li><li><a class="slow" href="ukraina/">Украина</a></li><li class="active"><a class="slow" href="kazaxstan/">Казахстан</a></li><li><a class="slow" href="belarus/">Беларусь</a></li><li><a class="slow" href="strany/">Все страны</a></li></ul></div></div></div><!--header--><div class="clear" id="main-body"><div class="floor"><div class="clear" id="mans-search"><form action="poisk/" method="GET"><div class="inputs clear"><div class="input"><input class="input" name="country" placeholder="Страна" type="text" value=""/></div><div class="input"><input class="input" name="town" placeholder="Город" type="text" value=""/></div><div class="input"><input class="input" name="last_name" placeholder="Фамилия" type="text" value=""/></div><div class="input"><input class="input" name="first_name" placeholder="Имя" type="text" value=""/></div></div><div class="submit"><button class="fa slow" type="submit"></button></div><input name="country_id" type="hidden" value="0"/><input name="city_id" type="hidden" value="0"/><input name="fname_id" type="hidden" value="0"/><input name="lname_id" type="hidden" value="0"/></form></div><div class="g-block header-gblock"> <script type="text/javascript">
						if( site_referer.indexOf('yandex') != -1 ) {
							if( document.body.clientWidth < 400 ) {
								document.write('<div id="yandex_rtb_R-A-221963-4"></div>');
								(function(w, d, n, s, t) {
								    w[n] = w[n] || [];
								    w[n].push(function() {
								        Ya.Context.AdvManager.render({
								            blockId: "R-A-221963-4",
								            renderTo: "yandex_rtb_R-A-221963-4",
								            horizontalAlign: false,
								            async: true
								        });
								    });
								    t = d.getElementsByTagName("script")[0];
								    s = d.createElement("script");
								    s.type = "text/javascript";
								    s.src = "//an.yandex.ru/system/context.js";
								    s.async = true;
								    t.parentNode.insertBefore(s, t);
								})(this, this.document, "yandexContextAsyncCallbacks");
							} else {
								document.write('<div id="yandex_rtb_R-A-221963-2"></div>');
							    (function(w, d, n, s, t) {
							        w[n] = w[n] || [];
							        w[n].push(function() {
							            Ya.Context.AdvManager.render({
							                blockId: "R-A-221963-2",
							                renderTo: "yandex_rtb_R-A-221963-2",
							                horizontalAlign: false,
							                async: true
							            });
							        });
							        t = d.getElementsByTagName("script")[0];
							        s = d.createElement("script");
							        s.type = "text/javascript";
							        s.src = "//an.yandex.ru/system/context.js";
							        s.async = true;
							        t.parentNode.insertBefore(s, t);
							    })(this, this.document, "yandexContextAsyncCallbacks");
							}
						} else {
							document.write('<script'+' async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></'+'script>');
							document.write('<ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-7746697709453826" data-ad-slot="8712673948" data-ad-format="auto"></ins>');
							(adsbygoogle = window.adsbygoogle || []).push({});
						}
						</script>
</div><!--content--><div><div class="clear" id="main-column"><div class="controller-floor"><div class="mod"><h1 class="mod-title">Поиск людей, бесплатная база данных людей</h1><div class="mod-body clear msg"><p>База данных людей по городам и странам совершенно бесплатно необходима для поиска нужного человека по фамилии и имени по всему миру — такую возможность предоставляет online сервис Bazaman.ru.</p><p>Поисковый робот постоянно индексирует и обновляет открытые социальные страницы людей: фамилии и имена, адреса и телефоны, фотографии и заметки, место работы, учебы и воинской службы.</p><p>Для того, чтобы поиск был для Вас удобным и быстрым, мы добавили фильтры <a href="strany/">людей по странам</a>, <a href="odnoklassniki/">одноклассникам</a>, <a href="odnokursniki/">однокурсникам</a>, <a href="familii/">поиск по фамилии</a> и <a href="imena/">имени</a>, <a href="data/">по дате рождения</a>. Чтобы увидеть подробную информацию о человеке, перейдите на его профиль на нашем сервисе. Каждая страница содержит ссылку на страницу-источник информации в социальной сети.</p><p>Сегодня каждый человек пытается найти через интернет своих друзей и знакомых вне зависимости от возраста и географии — в этом Вам поможет наш бесплатный сервис.</p><p>Приятного поиска и знакомства!</p></div></div><div class="g-block">
<script type="text/javascript">
			if( document.body.clientWidth < 400 ) {
				document.write('<div id="yandex_rtb_R-A-221963-4"></div>');
				(function(w, d, n, s, t) {
				    w[n] = w[n] || [];
				    w[n].push(function() {
				        Ya.Context.AdvManager.render({
				            blockId: "R-A-221963-4",
				            renderTo: "yandex_rtb_R-A-221963-4",
				            horizontalAlign: false,
				            async: true
				        });
				    });
				    t = d.getElementsByTagName("script")[0];
				    s = d.createElement("script");
				    s.type = "text/javascript";
				    s.src = "//an.yandex.ru/system/context.js";
				    s.async = true;
				    t.parentNode.insertBefore(s, t);
				})(this, this.document, "yandexContextAsyncCallbacks");
			} else {
				document.write('<div id="yandex_rtb_R-A-221963-1"></div>');
				(function(w, d, n, s, t) {
				    w[n] = w[n] || [];
				    w[n].push(function() {
				        Ya.Context.AdvManager.render({
				            blockId: "R-A-221963-1",
				            renderTo: "yandex_rtb_R-A-221963-1",
				            horizontalAlign: false,
				            async: true
				        });
				    });
				    t = d.getElementsByTagName("script")[0];
				    s = d.createElement("script");
				    s.type = "text/javascript";
				    s.src = "//an.yandex.ru/system/context.js";
				    s.async = true;
				    t.parentNode.insertBefore(s, t);
				})(this, this.document, "yandexContextAsyncCallbacks");
			}
			</script>
</div><div class="clear"><div class="mod"><p class="mod-title">Обновленные профили людей</p><div class="mod-body clear"><div class="clear gor-man-list"><ul class="clear"><li><div class="man-item clear"><div class="ava"><a href="rossiya/volgograd/krasilnikov_vitaliy_sergeevich-288417804/"><img alt="Красильников Виталий-Сергеевич" src="vkimgs/pp.userapi.com/c621829/v621829804/c520/eQQ6issj4A0.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/volgograd/krasilnikov_vitaliy_sergeevich-288417804/">Красильников Виталий-Сергеевич</a></p><p class="address">Россия, Волгоград</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="rossiya/novosibirsk/ilina_yana_krasotka-161967723/"><img alt="Ильина Яна-Красотка" src="vkimgs/pp.userapi.com/c307513/v307513723/7021/LUQn9GHDaS0.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/novosibirsk/ilina_yana_krasotka-161967723/">Ильина Яна-Красотка</a></p><p class="address">Россия, Новосибирск</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="rossiya/saransk/ilina_yanochka-12496676/"><img alt="Ильина Яночка" src="vkimgs/pp.userapi.com/c841432/v841432802/d87e/VA7e0CsA6Vs.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/saransk/ilina_yanochka-12496676/">Ильина Яночка</a></p><p class="address">Россия, Саранск</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="rossiya/moskva/volodin_vladimir-156399717/"><img alt="Володин Владимир" src="vkimgs/pp.userapi.com/c411217/v411217717/752e/k0SdoFSOj9c.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/moskva/volodin_vladimir-156399717/">Володин Владимир</a></p><p class="address">Россия, Москва</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="ukraina/evpatoriya/ilina_yana-66385218/"><img alt="Ильина Яна" src="vkimgs/pp.userapi.com/c638830/v638830496/6915a/GTD3nwR13_0.jpg"/></a></div><div class="desc"><p class="title"><a href="ukraina/evpatoriya/ilina_yana-66385218/">Ильина Яна</a></p><p class="address">Украина, Евпатория</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="rossiya/chaykovskiy/ilina_yana-76049817/"><img alt="Ильина Яна" src="vkimgs/pp.userapi.com/c639729/v639729817/2314c/66Rk04MfQOE.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/chaykovskiy/ilina_yana-76049817/">Ильина Яна</a></p><p class="address">Россия, Чайковский</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="usa/los_angeles/ilina_yana-138538590/"><img alt="Ильина Яна" src="vkimgs/pp.userapi.com/c837239/v837239389/4ed9e/r00BEjBeNcU.jpg"/></a></div><div class="desc"><p class="title"><a href="usa/los_angeles/ilina_yana-138538590/">Ильина Яна</a></p><p class="address">США, Los Angeles</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="rossiya/ermakovskoe/ilina_yana-157874651/"><img alt="Ильина Яна" src="vkimgs/pp.userapi.com/c834203/v834203175/1720c/778kv0Ct4Q0.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/ermakovskoe/ilina_yana-157874651/">Ильина Яна</a></p><p class="address">Россия, Ермаковское</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="rossiya/jeleznogorsk/volodin_vladimir-166580577/"><img alt="Володин Владимир" src="vkimgs/pp.userapi.com/c836235/v836235577/29dac/l8FD9QE1rsY.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/jeleznogorsk/volodin_vladimir-166580577/">Володин Владимир</a></p><p class="address">Россия, Железногорск</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="rossiya/ostrov/ilina_yana-67176436/"><img alt="Ильина Яна" src="vkimgs/pp.userapi.com/c638930/v638930436/65621/ns_kc8ifs6Y.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/ostrov/ilina_yana-67176436/">Ильина Яна</a></p><p class="address">Россия, Остров</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="rossiya/lipetsk/volodin_vladimir-168779401/"><img alt="Володин Владимир" src="vkimgs/pp.userapi.com/c836531/v836531401/4f0e8/F8T7OpZWxx4.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/lipetsk/volodin_vladimir-168779401/">Володин Владимир</a></p><p class="address">Россия, Липецк</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="rossiya/samara/ilina_yana-134521609/"><img alt="Ильина Яна" src="vkimgs/pp.userapi.com/c841523/v841523082/2972d/hgpjUalRw_0.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/samara/ilina_yana-134521609/">Ильина Яна</a></p><p class="address">Россия, Самара</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="rossiya/zelenodolsk/ilina_yana-159123539/"><img alt="Ильина Яна" src="vkimgs/pp.userapi.com/c614624/v614624539/f5df/CsgYpGutEBQ.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/zelenodolsk/ilina_yana-159123539/">Ильина Яна</a></p><p class="address">Россия, Зеленодольск</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="ispaniya/barcelona/ilina_yana-94769963/"><img alt="Ильина Яна" src="vkimgs/pp.userapi.com/c639822/v639822219/62cd1/oN5z5dappMs.jpg"/></a></div><div class="desc"><p class="title"><a href="ispaniya/barcelona/ilina_yana-94769963/">Ильина Яна</a></p><p class="address">Испания, Barcelona</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="rossiya/stavropol/ilina_yana-82523775/"><img alt="Ильина Яна" src="vkimgs/pp.userapi.com/c834301/v834301415/29673/1zsIh-IWhSk.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/stavropol/ilina_yana-82523775/">Ильина Яна</a></p><p class="address">Россия, Ставрополь</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="rossiya/samara/ilina_yana-115445837/"><img alt="Ильина Яна" src="vkimgs/pp.userapi.com/c639617/v639617314/4e9cc/NPXmXb8NFXQ.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/samara/ilina_yana-115445837/">Ильина Яна</a></p><p class="address">Россия, Самара</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="ukraina/kerch/volodin_vladimir-237874189/"><img alt="Володин Владимир" src="vkimgs/pp.userapi.com/c841421/v841421114/22ff4/N0y5QqEz8Qk.jpg"/></a></div><div class="desc"><p class="title"><a href="ukraina/kerch/volodin_vladimir-237874189/">Володин Владимир</a></p><p class="address">Украина, Керчь</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="rossiya/moskva/volodin_vladimir-238781967/"><img alt="Володин Владимир" src="vkimgs/pp.userapi.com/c623926/v623926967/5419e/jxpu2d6yWdQ.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/moskva/volodin_vladimir-238781967/">Володин Владимир</a></p><p class="address">Россия, Москва</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="rossiya/lyudinovo/volodin_vladimir-246321493/"><img alt="Володин Владимир" src="vkimgs/pp.userapi.com/c617121/v617121493/1f330/tO0xLCDGkyg.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/lyudinovo/volodin_vladimir-246321493/">Володин Владимир</a></p><p class="address">Россия, Людиново</p></div></div></li><li><div class="man-item clear"><div class="ava"><a href="rossiya/saratov/volodin_vova-248063431/"><img alt="Володин Вова" src="vkimgs/pp.userapi.com/c629405/v629405431/1e6a6/NzInTj5-dac.jpg"/></a></div><div class="desc"><p class="title"><a href="rossiya/saratov/volodin_vova-248063431/">Володин Вова</a></p><p class="address">Россия, Саратов</p></div></div></li></ul></div></div></div></div><div class="g-block">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Шапка - Базаман -->
<ins class="adsbygoogle" data-ad-client="ca-pub-7746697709453826" data-ad-format="auto" data-ad-slot="8712673948" style="display:block"></ins>
<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
</div><div class="clear"><div class="mod"><p class="mod-title">Популярные города</p><div class="mod-body clear"><div class="clear place-list"><ul class="clear"><li><a href="kazaxstan/almaty/">Алматы</a></li><li><a href="rossiya/barnaul/">Барнаул</a></li><li><a href="rossiya/volgograd/">Волгоград</a></li><li><a href="rossiya/voronej/">Воронеж</a></li><li><a href="ukraina/dnepropetrovsk_dnepr/">Днепропетровск (Днепр)</a></li><li><a href="ukraina/donetsk/">Донецк</a></li><li><a href="rossiya/ekaterinburg/">Екатеринбург</a></li><li><a href="ukraina/zaporoje/">Запорожье</a></li><li><a href="rossiya/ijevsk/">Ижевск</a></li><li><a href="rossiya/irkutsk/">Иркутск</a></li><li><a href="rossiya/kazan/">Казань</a></li><li><a href="rossiya/kaliningrad/">Калининград</a></li><li><a href="rossiya/kemerovo/">Кемерово</a></li><li><a href="ukraina/kiev/">Киев</a></li><li><a href="rossiya/kirov/">Киров</a></li><li><a href="rossiya/krasnodar/">Краснодар</a></li><li><a href="rossiya/krasnoyarsk/">Красноярск</a></li><li><a href="ukraina/lvov/">Львов</a></li><li><a href="belarus/minsk/">Минск</a></li><li><a href="rossiya/moskva/">Москва</a></li><li><a href="rossiya/nijniy_novgorod/">Нижний Новгород</a></li><li><a href="rossiya/novokuznetsk/">Новокузнецк</a></li><li><a href="rossiya/novosibirsk/">Новосибирск</a></li><li><a href="ukraina/odessa/">Одесса</a></li><li><a href="rossiya/omsk/">Омск</a></li><li><a href="rossiya/perm/">Пермь</a></li><li><a href="rossiya/rostov_na_donu/">Ростов-на-Дону</a></li><li><a href="rossiya/samara/">Самара</a></li><li><a href="rossiya/sankt_peterburg/">Санкт-Петербург</a></li><li><a href="rossiya/saratov/">Саратов</a></li><li><a href="ukraina/simferopol/">Симферополь</a></li><li><a href="rossiya/sochi/">Сочи</a></li><li><a href="rossiya/surgut/">Сургут</a></li><li><a href="rossiya/tolyatti/">Тольятти</a></li><li><a href="rossiya/tomsk/">Томск</a></li><li><a href="rossiya/tyumen/">Тюмень</a></li><li><a href="rossiya/ufa/">Уфа</a></li><li><a href="ukraina/harkov/">Харьков</a></li><li><a href="rossiya/chelyabinsk/">Челябинск</a></li><li><a href="rossiya/yaroslavl/">Ярославль</a></li></ul></div></div></div></div></div></div></div><!--content--></div></div><!--footer--><div id="footer-container"><div class="floor clear"><ul class="four-list nonelist clear"><li class="box"><input id="footer-menu-15" type="checkbox" value=""/><label class="title" for="footer-menu-15">Главные разделы</label><ul class="nonelist"><li class="box"><a class="slow" href="odnoklassniki/">Поиск одноклассников</a></li><li class="box"><a class="slow" href="odnokursniki/">Поиск однокурсников</a></li><li class="box"><a class="slow" href="familii/">Поиск по фамилии</a></li><li class="box"><a class="slow" href="imena/">Поиск по имени</a></li><li class="box"><a class="slow" href="data/">Поиск по дате рождения</a></li></ul></li><li class="box"><input id="footer-menu-20" type="checkbox" value=""/><label class="title" for="footer-menu-20">Проект</label><ul class="nonelist"><li class="box"><a class="slow" href="page-about/">О проекте</a></li><li class="box"><span class="slow check-url">Проверить URL</span></li></ul></li></ul><p>Bazaman.ru - бесплатный сервис по поиску друзей и знакомых среди открытых страниц ВКонтакте.ру. Вся информация на сайте собрана автоматически из открытых интернет-источников: социальная сеть ВКонтакте.ру. За достоверность информации, администрация сайта ответственности не несет.</p><p style="text-align: center;"><a href="page-privacy/">Политика обработки персональных данных</a></p><div id="counters"><div><!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t14.10;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число просмотров за 24"+
" часа, посетителей за 24 часа и за сегодня' "+
"border='0' width='88' height='31'><\/a>")
//--></script><!--/LiveInternet--></div><div><!--Openstat-->
<span id="openstat1"></span>
<script type="text/javascript">
var openstat = { counter: 1, image: 87, color: "c3c3c3", next: openstat };
(function(d, t, p) {
var j = d.createElement(t); j.async = true; j.type = "text/javascript";
j.src = ("https:" == p ? "https:" : "http:") + "//openstat.net/cnt.js";
var s = d.getElementsByTagName(t)[0]; s.parentNode.insertBefore(j, s);
})(document, "script", document.location.protocol);
</script>
<!--/Openstat--></div></div></div></div><!--footer--></div><script src="lib/jquery-1.12.3.min.js" type="text/javascript"></script><script src="lib/fancy/jquery.fancybox.pack.js" type="text/javascript"></script><script src="lib/jquery.autocomplete.js" type="text/javascript"></script><script src="lib/pack.js?v.6" type="text/javascript"></script><!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter42151194 = new Ya.Metrika({ id:42151194, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img alt="" src="https://mc.yandex.ru/watch/42151194" style="position:absolute; left:-9999px;"/></div></noscript> <!-- /Yandex.Metrika counter --><!-- Rating@Mail.ru counter -->
<script type="text/javascript">
var _tmr = window._tmr || (window._tmr = []);
_tmr.push({id: "2852818", type: "pageView", start: (new Date()).getTime()});
(function (d, w, id) {
  if (d.getElementById(id)) return;
  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
  ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window, "topmailru-code");
</script><noscript><div style="position:absolute;left:-10000px;">
<img alt="Рейтинг@Mail.ru" height="1" src="//top-fwz1.mail.ru/counter?id=2852818;js=na" style="border:0;" width="1"/>
</div></noscript>
<!-- //Rating@Mail.ru counter --><script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-91377745-1', 'auto');
  ga('send', 'pageview');
</script>
<div class="thescrolltop slow" id="scrollToTop"><span>Наверх</span><em class="fa"></em></div></body></html><!-- index -->
<!-- [0.0098409652709961s,168200b] -->