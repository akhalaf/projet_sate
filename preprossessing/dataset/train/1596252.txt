<!DOCTYPE html>
<html>
<head>
<style>
/* The parent container */
    main {
      display: grid;
      grid-template-columns: repeat(auto-fill, minmax(1000px, 1fr));
    }
    img {
      max-width: 100%;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
    $('.lazy').lazy();
});
</script>
</head>
<body>
<main>
<figure>
<img alt="" src="1.png"/>
</figure>
<figure>
<img alt="" class="lazy" data-src="2.jpg"/>
</figure>
<figure>
<img alt="" class="lazy" data-src="3.jpg"/>
</figure>
<figure>
<img alt="" class="lazy" data-src="4.png"/>
</figure>
<figure>
<img alt="" class="lazy" data-src="5.png"/>
</figure>
<figure>
<img alt="" class="lazy" data-src="6.png"/>
</figure>
<figure>
<img alt="" class="lazy" data-src="7.png"/>
</figure>
<figure>
<img alt="" class="lazy" data-src="8.png"/>
</figure>
<figure>
<img alt="" class="lazy" data-src="9.png"/>
</figure>
<figure>
<img alt="" class="lazy" data-src="10.png"/>
</figure>
<figure>
<img alt="" class="lazy" data-src="11.png"/>
</figure>
</main>
</body>
</html>