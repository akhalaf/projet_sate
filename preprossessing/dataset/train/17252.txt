<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>ã¯ã³ãããã·ã¼ãã¹</title>
<link href="//www.1ft-seabass.jp/1ft.ico" rel="Shortcut Icon" type="image/x-icon"/>
<!-- OGP -->
<meta content="ã¯ã³ãããã·ã¼ãã¹" name="description"/>
<link href="https://www.1ft-seabass.jp/" rel="canonical"/>
<meta content="1ft-seabass.jp" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="https://www.1ft-seabass.jp/" property="og:url"/>
<meta content="https://www.1ft-seabass.jp/memo/uploads/2015/03/ogp_base.png" property="og:image"/>
<meta content="1ft-seabass.jp" property="og:site_name"/>
<meta content="151031792288682" property="fb:app_id"/>
<meta content="ã¯ã³ãããã·ã¼ãã¹" property="og:description"/>
<meta content="summary" name="twitter:card"/>
<meta content="1ft-seabass.jp" name="twitter:title"/>
<meta content="ã¯ã³ãããã·ã¼ãã¹" name="twitter:description"/>
<meta content="https://www.1ft-seabass.jp/memo/uploads/2015/03/ogp_base.png" name="twitter:image"/>
<!-- /OGP -->
<link href="//fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/milligram/1.3.0/milligram.css" rel="stylesheet"/>
<!-- viewport -->
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<style>
        body {
            margin: 10px;
        }

        ul {
            margin: 30px;
        }

        li {
            padding-left: 1.5em;
            text-indent: -1.5em;
        }
    </style>
</head>
<body>
<div class="container">
<div class="row">
<div class="column">
<h1>1ft-seabass.jp</h1>
</div>
</div>
<div class="row">
<div class="column">
<h2 id="blog">Blog</h2>
<ul>
<li><a href="http://www.1ft-seabass.jp/memo/">1ft-seabass.jp.MEMO</a>
<ul>
<li>ã¯ã³ãããã·ã¼ãã¹.ã¡ã¢ &gt; memo [å] è¦ãæ¸ã, æ§ãæ¸ã, èæ¸, ç«¯æ¸ã</li>
</ul>
</li>
</ul>
<h2 id="support">Support</h2>
<ul>
<li><a href="https://protoout.studio/" target="_blank">ProtoOut Studioï¼ãã­ãã¢ã¦ãã¹ã¿ã¸ãªï¼</a></li>
</ul>
<h2 id="community">Community</h2>
<ul>
<li><a href="https://www.facebook.com/groups/yurumect/" target="_blank">ããã¡ã«ãã­</a> éå¶</li>
<li><a href="https://nodered.jp/" target="_blank">Node-REDæ¥æ¬ã¦ã¼ã¶ä¼</a> éå¶</li>
<li><a href="https://dist.connpass.com/" target="_blank">DIST</a> ã¹ã¿ãã</li>
<li><a href="https://jsfes.connpass.com/" target="_blank">JavaScriptç¥­</a> ã¹ã¿ãã</li>
</ul>
<h2 id="action">Action</h2>
<ul>
<li>ç¶ç¶ä¸­
                        <ul>
<li>æIoTãµã¼ãã¹ã®Developer Relation(DevRel)æ´»å</li>
<li>æIoTãµã¼ãã¹ã®ç«ã¡ä¸ãæ¯æ´ããã³æè¡æ¯æ´
                                <ul>
<li>ãã¯ãã«ã«ã¢ããã¤ã¶ã¼ / ãã­ã³ãã¨ã³ãé¨åæ¤è¨¼ ãªã©</li>
</ul>
</li>
</ul>
</li>
</ul>
<h3 id="2020">2020</h3>
<ul>
<li><a href="https://www.1ft-seabass.jp/memo/2020/12/10/summary-devrel-meetup-in-tokyo-59/" target="_blank">DevRel Meetup in Tokyo vol.59 ããã­ã°ã»ããã¯ãã­ã°ã ã«ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/12/04/summary-20201202-enebular-developer-meetup/" target="_blank">enebular developer meetup ã§ HoloLens 2 + enebular ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/11/30/summary-node-red-handson-vol-01/" target="_blank">11/19ã«Node-RED åå¿èåãåå¼·ä¼ãéå¬</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/11/26/summary-devrel-asia-2020/" target="_blank">DevRel/Asia 2020 ã§ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/11/23/summary-champ-dojo-vol04/" target="_blank">Champ Dojo vol.4 ã§ IBM Champion ã¨ãã¦ Node-RED ãã³ãºãªã³è¬å¸«</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/10/28/online-lt-hack-lt-vol01/" target="_blank">ãªã³ã©ã¤ã³LTããã¯LTãéå¬</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/10/25/node-red-con-tokyo-2020/" target="_blank">Node-RED Con Tokyo 2020ã10/10ã«éå¬</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/10/13/usecase-research-and-development-pattern-1/" target="_blank">æè¡ç ç©¶ãã­ã¸ã§ã¯ãããµãã¼ããã¦ããæè¿ã®äºä¾</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/10/05/summary-community-broadcast-folks-event-vol-2/" target="_blank">ã³ãã¥ããã£æ¾éé¨ vol.2 ã§ Zoom ã® Tips ã§ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/10/01/summary-adobexd-user-festival-2020/" target="_blank">Adobe XD ã¦ã¼ã¶ã¼ãã§ã¹2020ã§ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/09/30/summary-freee-ibmcloud-lineapi-event/" target="_blank">freee / IBM Cloud / LINE API ã¢ããªéçºåå¼·ä¼ã§ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/09/25/selva-event-output-driven-summary/" target="_blank">æ ªå¼ä¼ç¤¾ã»ã«ãæ§ã®ã¢ã¦ããããæ§ç¯æ¦ç¥ã¤ãã³ãã§ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/08/22/viotlt-vol4-summary/" target="_blank">ãã¸ã¥ã¢ã«ãã­ã°ã©ãã³ã°IoTLT vol4 ã§éå¶ï¼ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/08/06/summary-devrelcon-earth-2020/" target="_blank">DevRelCon Earth 2020 ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/08/02/summary-techfeed-summit-vol-2/" target="_blank">TechFeed Summit vol.2 ããã«ãã£ã¹ã«ãã·ã§ã³ï¼LTç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/07/13/summary-of-oyako-tech-lt/" target="_blank">ãã¯ãã­ã¸ã¼ã§å­è²ã¦ããã£ã¨æ¥½ããï¼ãããããã¯LT ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/07/06/ibm-user-group-conference-2020/" target="_blank">IBM User Group Conference 2020ã§1æ¥ç®Keynoteã»ãªã³ã©ã¤ã³å¸ä¼ã» Zoom éä¿¡ï¼è¨é²ãæå½</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/07/05/microsoft-mvp-2020/" target="_blank">2020-2021 Microsoft MVP ( Windows Development ) </a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/06/21/nodered-book-released-2/" target="_blank">å·¥å­¦ç¤¾åè¡ãå®è·µNode-REDæ´»ç¨ããã¥ã¢ã«ãæ¸ç±ãå±åå·ç­</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/06/04/tokai-university-special-lecture-202005/" target="_blank">2020å¹´ã®æ±æµ·å¤§å­¦çµè¾¼ã¿ã½ããã¦ã§ã¢éçºç¹å¥è¬ç¾©ï¼ ãªã³ã©ã¤ã³</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/06/02/iotlt-63-family-tech-own-report/" target="_blank">IoTç¸ãã®åå¼·ä¼! IoTLT vol.63 ã«ãã¡ããªã¼ããã¯ã§ç»å£</a></li>
<li><a href="https://medium.com/@tnk.seigo/my-report-about-ibm-champion-japan-meetup-online-vol-1-96eea6c343e7" target="_blank">IBM Champion Japan Meetup Online vol.1</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/05/13/online-community-event-memo/" target="_blank">ãªã³ã©ã¤ã³ã®ååLTåå¼·ä¼ã§éå¶ã¨ Zoom + YouTube Live åç»éä¿¡ãæå½</a>
<ul>
<li><a href="https://www.1ft-seabass.jp/memo/2020/03/17/challenge-at-mtg-using-zoom-and-youtube-live/" target="_blank">Zoomãä½¿ã£ããã¼ãã£ã³ã°ãã¯ã­ã¼ãºãã«YouTube Liveéä¿¡å®é¨</a></li>
</ul>
</li>
<li><a href="https://speakerdeck.com/1ftseabass/remote-work-hack-vol-dot-1" target="_blank">ãªã¢ã¼ãã¯ã¼ã¯ããã¯LT - vol.1ãæ¯å­ãçã¾ããããªã¼ã©ã³ã¹ã®æè¿ã®ãªã¢ã¼ãã¯ã¼ã¯ã§ã®ã¿ã¹ã¯ç®¡çãã®ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/04/01/book-io-node-red-ug-202004/" target="_blank">å·¥å­¦ç¤¾æ§ æåèª I/O 2020å¹´4æå·Node-REDé£è¼ã«ã¦Raspberryã¨WEBã«ã¡ã©ã¨Gyazoã®é£æºè¨äº</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/04/16/talking-at-tech-learn-english-meetup-as-a-speaker/" target="_blank">Tech Learn w/English vol.3 ã§ãªã³ã©ã¤ã³ï¼è±èªç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/03/13/node-red-ug-online-mc-memo/" target="_blank">Node-RED UG åå¼·ä¼ ãªã³ã©ã¤ã³éä¿¡ã®å¸ä¼</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2020/02/21/granks-lecture-ai/" target="_blank">å¼æ¾ã°ã©ã³ã¯ã¹æ§ã«Microsoft Custom Vision Serviceã®ã¬ã¯ãã£ã¼</a></li>
</ul>
<h3 id="2019">2019</h3>
<ul>
<li>2020-2021 IBM Champion
                        <ul>
<li><a href="https://twitter.com/1ft_seabass/status/1222769851191582720" target="_blank">https://twitter.com/1ft_seabass/status/1222769851191582720</a></li>
</ul>
</li>
<li><a href="https://speakerdeck.com/1ftseabass/20191225-devrel-community-vol-dot-6" target="_blank">DevRel/Community #6 ãã¦ã¼ã¶ã«ã³ãã¡ã¬ã³ã¹ããã¯ããã¦ã®Node-RED UGã«ã³ãã¡ã¬ã³ã¹ï¼Node-RED Con Tokyo 2019éå¶ãããã¾è©±ï¼ã ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/12/23/bmxug-meetup-20191216-resume/" target="_blank">BMXUGå¥³å­é¨åå¼·ä¼ ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/12/10/adobe-max-japan-2019-resume/" target="_blank">Adobe MAX Japan 2019ã§Adobe XDã¨IoTã¨VR(HoloLens) ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/11/07/devrel-meetup-in-tokyo-47-resume/" target="_blank">DevRel Meetup in Tokyo 47 ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/09/08/devrel-japan-con-2019/" target="_blank">DEVREL/JAPAN CONFERENCE 2019ã«åå ï¼æè¦ªä¼ LT</a></li>
<li><a href="https://speakerdeck.com/1ftseabass/20190926-visual-programing-meetup-885d612e-eb85-4fca-8ef0-e22f4ed02787" target="_blank">ãã¸ã¥ã¢ã«ãã­ã°ã©ãã³ã°äº¤æµä¼ãenebular+Node-RED+Noodlã ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/08/22/adobe-xd-fes-2019-final-exhibision/" target="_blank">Adobe XD ã¦ã¼ã¶ã¼ãã§ã¹ 2019 Finalã§Adobe XDãã©ã°ã¤ã³ï¼IoTï¼HoloLens å±ç¤º</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/08/13/e-textile-enebular-event-resume/" target="_blank">e-textile+ãã³ã³ã¼ãã£ã³ã°IoTã®åå¼·ä¼ã§e-textile+enebular+HoloLens LT</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/08/07/node-red-con-tokyo-2019/" target="_blank">Node-RED Con Tokyo 2019ã7/18ã«éå¶ã¡ã³ãã¼ã¨ãã¦éå¬</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/08/06/maker-faire-tokyo-seeed-help-staff/" target="_blank">Maker Faire Tokyo 2019 Seeedãã¼ã¹ã§ 8/3 å±ç¤ºï¼èª¬æå¡</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/08/05/spzcolab-nodered-handson-20190725-report/" target="_blank">ãµãã¼ã¿ã¼ãºåå¼·ä¼ã§Node-REDãã³ãºãªã³ãéå¬</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/07/03/think-summit-2019-ibm-champion-session/" target="_blank">Think Summit 2019ã«ã¦IBMChampion ã®çããã§èªãããã«ãã£ã¹ã«ãã·ã§ã³ã«åå </a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/07/02/microsoft-mvp-2019/" target="_blank">2019-2020 Microsoft MVP ( Windows Development ) </a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/06/18/granks-iot-201906/" target="_blank">å¼æ¾ã°ã©ã³ã¯ã¹æ§ IoTè¬ç¾©</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/06/14/osaka-nodered-meetup-vol03-resume/" target="_blank">Node-RED UG Osakaåå¼·ä¼ vol.3ã§HoloLensã¨Node-RED(RedMobile)ãªç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/05/31/decode2019-resume/" target="_blank">de:code 2019ã«ã¦IoTã«ã¤ãã¦ã®ãã§ã¼ã¯ãã¼ã¯ç»å£</a></li>
<li>å·¥å­¦ç¤¾æ§ã®æåèªI/O Node-REDã¨ #SORACOM Harvest ã¨ã®é£æºãæ²è¼ â <a href="https://twitter.com/1ft_seabass/status/1129194649401217024" target="_blank">Twitter</a></li>
<li><a href="https://speakerdeck.com/1ftseabass/temonatechnight-vol-dot-18" target="_blank">TemonaTechNight Vol.18 LTãSORACOM LTE-M Button Plus ããã­ã¼ã³ã¨é£åããã¦ã¿ã¦èãããã¨ã</a></li>
<li>å·¥å­¦ç¤¾æ§ã®æåèªI/O obnizãã¼ã ã¨ã®é£æºãæ²è¼ â <a href="https://twitter.com/1ft_seabass/status/1119041022682521601" target="_blank">Twitter</a></li>
<li><a href="https://speakerdeck.com/1ftseabass/0190403-temonatechnight-vol-dot-17" target="_blank">TemonaTechNight Vol.17ãæè¿Todoãªã¹ãï¼IoTãçµã¿åããã¦ã¿ã¦æããè²ããªãã¨ã</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/05/05/start-enebular-for-iot-2019-resume/" target="_blank">Start enebular for IoT 2019 ã§å±ç¤ºã¨ãã¼ã¯ã»ãã·ã§ã³</a></li>
<li>æè¡æ¸å¸ 6 åå 
                        <ul>
<li>ããã­ã°ãæ¸ãã ããã­ã°ãæ¸ãæè¡ãã â <a href="https://twitter.com/1ft_seabass/status/1116159296658522112" target="_blank">Twitter</a></li>
<li>ããã¬ã¼ã³ãã¼ã·ã§ã³ãæ¯ããæè¡ã â <a href="https://twitter.com/1ft_seabass/status/1116158334363586560" target="_blank">Twitter</a></li>
</ul>
</li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/04/14/tokai-university-special-lecture-201904/" target="_blank">2019å¹´ãæ±æµ·å¤§å­¦é«è¼ªã­ã£ã³ãã¹ã«ã¦çµè¾¼ã¿ã½ããã¦ã§ã¢éçºç¹å¥è¬ç¾©</a></li>
<li><a href="https://speakerdeck.com/1ftseabass/community-vol-dot-1" target="_blank">DevRel/Community #1ãNode-RED UG ã¤ãã³ãéå¶ããã¨ãã®ä¼å ´ãæºåã®ãããã¾è©±ã</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/03/10/devrelcon-tokyo-2019-resume/" target="_blank">DevRelCon Tokyo 2019 ã§è±èªç»å£ï¼ãã¢</a></li>
<li><a href="https://speakerdeck.com/1ftseabass/20190219-control-iotlt-vol-dot-3" target="_blank">Control(å¶å¾¡ï¼ x IoTç¸ãã®åå¼·ä¼! CIoTLT Vol.3ãRevPi æ¥­åç¨Raspberry Piï¼EAO éµã¹ã¤ãããåããã¦ã¿ã¦æããä¸çã</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/02/23/osaka-201902-2session-per-1day-resume/" target="_blank">å¤§éªçIoTç¸ãã®åå¼·ä¼ IoTLT Osaka Vol.10ã¨Start IoT with enebular 2019 OSAKAã¨ã§2ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2019/02/17/ibm-think-2019-my-activity/" target="_blank">ãµã³ãã©ã³ã·ã¹ã³ã«ã¦IBM Think 2019ã«IBM Championã¨ãã¦åå ãã¦TJBot zeroãã¢</a></li>
</ul>
<h3 id="2018">2018</h3>
<ul>
<li><a href="http://mist.mvmt.jp/" target="_blank">MISTÃTECH</a> ãã¯ãã­ã¸ã¼çæ¾é ãã¹ãå½¹
                        <ul>
<li>æ¾éã¢ã¼ã«ã¤ã &gt;&gt; <a href="https://www.1ft-seabass.jp/memo/2017/12/04/mist-tech-vol00-resume/" target="_blank">0</a> <a href="https://www.1ft-seabass.jp/memo/2018/02/15/mist-tech-vol01-resume/" target="_blank">1</a> <a href="https://www.1ft-seabass.jp/memo/2018/02/24/mist-tech-vol02-resume/" target="_blank">2</a> <a href="https://www.1ft-seabass.jp/memo/2018/03/15/mist-tech-vol03-resume/" target="_blank">3</a> <a href="https://www.1ft-seabass.jp/memo/2018/04/22/mist-tech-vol04-resume/" target="_blank">4</a> <a href="https://www.1ft-seabass.jp/memo/2018/06/02/mist-tech-vol05-resume/" target="_blank">5</a></li>
</ul>
</li>
<li>2019-2020 IBM Champion</li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/12/23/devrel-in-english-vol04-resume/" target="_blank">DevRel Meetup in English vol.4 è±èªç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/12/10/ibm-cloud-community-2018-resume/" target="_blank">IBM Cloud Communityç»å£ï¼ãã¹ãã¹ãã¼ã«ã¼è³</a></li>
<li><a href="https://speakerdeck.com/1ftseabass/20181127-obnizxm5stackxnefrybt-3devices-meetup" target="_blank">obnizÃM5StackÃNefryBT 3Devices Meetupç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/12/11/fukuyama-handson-20181114-resume/" target="_blank">åºå³¶çç¤¾åIoTã¨ã­ã¹ãã¼ãè²æè¬åº§ã®ç¦å±±ãã¼ããã³ãºãªã³</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/11/30/wingarc-1st-2018-resume/" target="_blank">ã¦ã¤ã³ã°ã¢ã¼ã¯ãã©ã¼ã©ã  2018 ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/11/07/node-red-ug-jp-ibm-hursley-mtg-resume/" target="_blank">IBM Hursleyã§ Node-REDéçºãã¼ã ã¨ã®ãã¼ãã£ã³ã°ã«ã¦Node-RED UG Japanã®ä¸å¡ã¨ãã¦æ´»åå ±åï¼ãã¢ãæå½</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/11/01/sendai-iotlt-vol05-resume/" target="_blank">IoTLT ä»å° vol.5 ç»å£</a></li>
<li><a href="https://speakerdeck.com/1ftseabass/enebular-developer-meetup-vol-dot-4" target="_blank">enebular developer Meetup Vol.4 ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/10/01/enebular-meetup-vol03-resume/" target="_blank">enebular Meetup vol.3 ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/09/25/iotlt43-resume/" target="_blank">IoTç¸ãã®åå¼·ä¼! IoTLT vol.43 ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/09/10/devrel-meetup-in-english-vol02/" target="_blank">DevRel Meetup in English vol.2 è±èªç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/08/22/granks-ai-and-mr-talk-20180817/" target="_blank">å¼æ¾ã°ã©ã³ã¯ã¹æ§ AIã¨HoloLensãäº¤ãããã¯ãã­ã¸ã¼è¬ç¾©</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/08/21/nagaoka-zokei-univ-2018-summer/" target="_blank">é·å²¡é å½¢å¤§å­¦ IoTã¨MixedRealityã«ã¤ãã¦ãã¢ã¨HoloLensä½é¨ä¼</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/08/20/hiroshima-iot-handson-201808/" target="_blank">åºå³¶ç¤¾åIoTã¨ã­ã¹ãã¼ãè²æäºæ¥­ãã³ãºãªã³è¬å¸«</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/08/01/iot-event-moderator-20180717/" target="_blank">ãµã¼ã­ã¥ã¬ã¼ã·ã§ã³ããCTO meetupãIoTã§å¤æ§åãããã¼ã¿ã¨ã®é¢ããæ¹ãã¢ãã¬ã¼ã¿</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/07/20/tokai-university-special-lecture-201807/" target="_blank">æ±æµ·å¤§å­¦é«è¼ªã­ã£ã³ãã¹ã«ã¦çµè¾¼ã¿ã½ããã¦ã§ã¢éçºç¹å¥è¬ç¾©</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/07/15/knx-event-201807-resume/" target="_blank">æ¥æ¬KNXåä¼5å¨å¹´è¨å¿µï¼KNX SECURE Roadshowã¤ãã³ãç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/06/22/iotlt-aiotlt-vol01-resume/" target="_blank">AI x IoTç¸ãã®åå¼·ä¼! AIoTLT vol.1 ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/06/15/think-japan-developer-day-lt-resume/" target="_blank">Think Japan Developer Day LTå¤§ä¼ ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/06/04/cloud-developers-circle-vol07-iot-bash-resume/" target="_blank">Cloud Developers Circle vol.7 â IoT Bash ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/06/07/microsoft-mvp-2018/" target="_blank">2018-2019 Microsoft MVP ( Windows Development  )</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/05/30/caadria2018-hololens-technical-support/" target="_blank">CAADRIA18è«æ HoloLensæè¡ãµãã¼ã</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/05/26/sier-iotlt-vol8-resume/" target="_blank">IoTç¸ãã®åå¼·ä¼ SIerIoTLT vol8 ç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/05/23/m5stack-user-meeting-vol01-resume/" target="_blank">M5Stackã¦ã¼ã¶ã¼ãã¼ãã£ã³ã° vol.1 ç»å£</a></li>
<li>æMixed Realityéçºæ¯æ´
                        <ul>
<li>ãã¯ãã«ã«ãã£ã¬ã¯ã·ã§ã³ / å¶ä½é²è¡ / å®éçº C# ã»ã</li>
</ul>
</li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/04/03/myojowaraku-2018-report01/" target="_blank">ææåæ¥½2018ã§IoTï¼MixedRealityå±ç¤º</a>
<ul>
<li>ããã³ãå°ã<a href="https://www.1ft-seabass.jp/memo/2018/04/06/myojowaraku-2018-report02/" target="_blank">é²è¡ãµãã¼ã</a></li>
</ul>
</li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/03/04/hiroshima-20180303-camps-event-resume/" target="_blank">åºå³¶IoTã¤ãã³ãã§ãã³ãºãªã³ã®ãæä¼ã</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/02/11/ibm-champion-2018/" target="_blank">2018-2019 IBM Champion</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/02/08/univ-hololens-firststep-short-support/" target="_blank">å¤§å­¦ã¼ã HoloLenséçºå°å¥æ¯æ´</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2018/01/20/2018-innocafe33-resume/" target="_blank">InnoCAFE33 æ°å¹´ã®æ±è² LTå¤§ä¼ç»å£</a></li>
<li>æç ç©¶è«æMixed Realityæ¤è¨¼ãµãã¼ãï¼2017/12ï¼</li>
</ul>
<h3 id="2017">2017</h3>
<ul>
<li><a href="https://www.1ft-seabass.jp/memo/2017/12/29/advent-calendar-2017/" target="_blank">ã¢ããã³ãã«ã¬ã³ãã¼2017 åã°ã«ã¼ãåå </a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2017/12/17/atl-showcase-fes-2017-resume/" target="_blank">ATL SHOWCASE ãã§ã¹2017ã§HoloTakibiãå±ç¤º</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2017/12/05/wio-lte-event-resume/" target="_blank">Wio LTE ã¦ã¼ã¶ã¼ã¤ãã³ãç»å£</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2017/12/04/mist-tech-vol00-resume/" target="_blank">ãã¯ãã­ã¸ã¼æå ±çªçµ MISTÃTECH Vol.0</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2017/12/11/node-red-serial-node-jp-translate/" target="_blank">Node-RED serialportãã¼ã æ¥æ¬èªç¿»è¨³åå 2017/11</a></li>
<li><a href="https://atl-hiroo.recruit-tech.co.jp/showcase/archives/742" target="_blank">ATL SHOWCASE HoloTakibiå¶ä½</a>
<ul>
<li>HoloLens / Unity C# / IoT Nefry BT / Node-RED</li>
</ul>
</li>
<li>ãããã½ã³2017æ±ã®é£ æè¡ãã©ã­ã¼ â <a href="https://www.slideshare.net/seigotanaka/2017-pepper-x-enebular-x-milkcocoa-nodered" target="_blank">slideshare</a></li>
<li>Facebook AR Studio ã¯ã¼ã¯ã·ã§ããï¼ããã«ã½ã³ 2017 æè¡ãã©ã­ã¼ â <a href="https://www.slideshare.net/seigotanaka/facebook-ar-studio-2017" target="_blank">slideshare</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2017/11/26/atl_hololens_seminar_001_resume/">Advanced Technology Lab HoloLensåå¼·ä¼è¬å¸«</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2017/09/18/nodered-book-released/">å·¥å­¦ç¤¾åè¡ãã¯ããã¦ã®Node-REDãæ¸ç±å·ç­</a>
<ul>
<li>å·ç­å®åã»å·ç­å¨ä½ã®èª¿æ´å½¹ã»é²è¡ç®¡ç</li>
</ul>
</li>
<li><a href="https://www.1ft-seabass.jp/memo/2017/10/18/book_io_node-red_ug_201711/">å·¥å­¦ç¤¾æ§ æåèª I/O 2017å¹´11æå· Node-RED è¨äºå·ç­</a>
<ul>
<li>å·ç­</li>
</ul>
</li>
<li>æIoTè©¦ä½ç°å¢æ§ç¯æ¯æ´
                        <ul>
<li>ãã­ã¸ã§ã¯ãé²è¡ç®¡çã»ã¿ã¹ã¯åãã£ã¹ã«ãã·ã§ã³ãªã©ã®ãã£ã¬ã¯ã·ã§ã³æ¥­å</li>
<li>AWS IoT / Node-RED / C(Arduino)</li>
</ul>
</li>
<li>æMixed Realityéçºæ¯æ´ãHoloLens BLEé¨å
                        <ul>
<li>ãã¬ãã¸æ§ç¯ / ãã¯ãã«ã«ã¢ããã¤ã¶ã¼ / å®éçº C#</li>
</ul>
</li>
<li>æFlashæ¼åºHTML5ç§»æ¤å¯¾å¿ãã©ã­ã¼
                        <ul>
<li>ãã­ã³ãã¨ã³ã¸ãã¢æ¥­å / ãã¯ãã«ã«ã¢ããã¤ã¶ã¼ / CreateJS </li>
</ul>
</li>
<li>æIoTã»ã³ã·ã³ã°ããã³ã¢ãã¿ãªã³ã°ãã¢æ¯æ´
                        <ul>
<li>ãã¯ãã«ã«ã¢ããã¤ã¶ã¼ / ã¢ãã¿ãªã³ã°é¨åãã­ã³ãã¨ã³ãå´å¶ä½ / Node-RED / C(Arduino) ã»ã</li>
</ul>
</li>
<li><a href="https://www.1ft-seabass.jp/memo/2017/06/26/tokai-university-special-lecture-201706/">æ±æµ·å¤§å­¦é«è¼ªã­ã£ã³ãã¹ çµè¾¼ã¿ã½ããã¦ã§ã¢éçºç¹å¥è¬ç¾©</a></li>
<li><a href="https://www.1ft-seabass.jp/memo/2017/04/25/samurai-island-expo-17/">SAMURAI ISLAND EXPO17ã«ã¦ç§æè¨­åã¨HoloLensã®é£æºå±ç¤º</a>
<ul>
<li>HoloLensé¨åéçº Unity / C#</li>
<li>IoTé¨åé£æºå¶ä½ Node.js / Node-RED</li>
</ul>
</li>
<li><a href="https://www.1ft-seabass.jp/memo/2017/04/20/book_io_node-red_ug_201705/">å·¥å­¦ç¤¾æ§ æåèª IO 2017å¹´5æå·ã«Node-RED å·ç­</a>
<ul>
<li>å·ç­</li>
</ul>
</li>
<li><a href="https://www.1ft-seabass.jp/memo/2017/04/18/keio_edge_2017_private_report/">æ¶æç¾©å¡¾å¤§å­¦ EDGE éä¸­ã¯ã¼ã¯ã·ã§ãã IoTé¨å</a>
<ul>
<li>ãã¯ãã«ã«ã¢ããã¤ã¶ã¼ / IoTã«éç¹ãç½®ããã¡ã³ã¿ã¼</li>
<li>æè¡çãªãã¬ã¼ã³ãã¼ã·ã§ã³ï¼ç¿»è¨³ãã©ã­ã¼ããã ãã¤ã¤ã®è±èªï¼</li>
</ul>
</li>
</ul>
<h3 id="2016">2016</h3>
<ul>
<li><a href="https://www.1ft-seabass.jp/memo/2016/11/14/mojowaraku2016-fukuoka-event-report/">ææåæ¥½2016 Tokyo IoT Devãã¼ã å±ç¤º</a>
<ul>
<li>Node.js / Node-RED / CreateJS / Create.js å®å¶ä½ã»ãå¨è¾º</li>
</ul>
</li>
<li><a href="https://www.1ft-seabass.jp/memo/2016/10/11/littlebits-book-release/">å·¥å­¦ç¤¾åè¡ããlittleBitsãã§ã¯ãããé»å­å·¥ä½ãæ¸ç±å·ç­</a>
<ul>
<li>å·ç­</li>
</ul>
</li>
<li><a href="https://www.1ft-seabass.jp/memo/2016/10/10/codezine-bmxug-relay-archive/">ç¿æ³³ç¤¾æ§ Codezineã«ã¦IBM Bluemix User Groupï¼BMXUGï¼ãªã¬ã¼å¯ç¨¿</a>
<ul>
<li>å·ç­</li>
</ul>
</li>
<li>Raspberry Pi æ¥­åç«¯æ«éçº
                        <ul>
<li>Node.js / Node-RED ã»ã</li>
</ul>
</li>
<li>IoTé¢é£æè¡ããã³ãã­ã³ãã¨ã³ãã®éçºã»éå¶ã«é¢é£ããæ¥­åæ¯æ´
                        <ul>
<li>JavaScript / Node.js</li>
<li>Monaca / Bluemix / MQTT / BLE</li>
</ul>
</li>
<li>æããã¤ã¹å®è£ãã­ãã¿ã¤ãæ¥­åæ¯æ´
                        <ul>
<li>JavaScript / Node.js</li>
<li>AWS IoT / MQTT / Raspberry Pi / Grove</li>
</ul>
</li>
<li>æè²ç³»ããã¤ã¹ã®å¤é¨æè¡é¡§åæ´»å
                        <ul>
<li>JavaScript / Node.js / C(Arduino)</li>
</ul>
</li>
<li>æãã¼ã¿ã¢ãã¿ãªã³ã°ãµã¤ãã¼ã¸å¶ä½
                        <ul>
<li>JavaScript / Node.js / CreateJS / Heroku</li>
</ul>
</li>
<li><a href="https://www.1ft-seabass.jp/memo/2016/06/14/learning-technology-digitalhike-exhibition/">ã©ã¼ãã³ã°ãã¯ãã­ã¸ã¼2016 ãã¸ã¿ã«ãã¤ã¯ç¤¾æ§å±ç¤ºæè¡åå</a>
<ul>
<li>CreateJS / Node-RED / Raspberry Pi / littleBits</li>
</ul>
</li>
<li><a href="https://www.1ft-seabass.jp/memo/2016/05/21/edix2016-littlebits-device-case/">æè²ITã½ãªã¥ã¼ã·ã§ã³EXPO2016(EDIX2016)ã®littleBitsãã¼ã¹ ããã¤ã¹é£æºãã¢æè¡åå</a>
<ul>
<li>CreateJS / Node-RED / Raspberry Pi / littleBits</li>
</ul>
</li>
</ul>
<h3 id="2015">2015</h3>
<ul>
<li><a href="https://www.1ft-seabass.jp/memo/2015/11/11/playgroung2015-littlebits-workshop/">Playground2015ã§ã¯ã¼ã¯ã·ã§ããç£ä¿®ï¼å±ç¤º</a></li>
<li>ææ¼«ç»ã³ã³ãã³ãç¹è¨­å ããµã¤ãã®HTML5(Canvas)æ¼åºå®è£
                        <ul>
<li>JavaScript / Node.js / CreateJS</li>
</ul>
</li>
<li>æå¤©æ°äºå ±APIã»MQTTã²ã¼ãã¦ã§ã¤ã®ä½æ
                        <ul>
<li>JavaScript / Node.js / MQTT / WebSocket / Heroku</li>
</ul>
</li>
<li>æããã¤ã¹ã»ã³ãµã¼ç·æ¬ç®¡çMQTTç®¡çç»é¢ã®å¶ä½
                        <ul>
<li>JavaScript / Node.js / MQTT / WebSocket</li>
</ul>
</li>
<li>æã½ã¼ã·ã£ã«ã²ã¼ã ä¼ç¤¾ãã­ã³ãã¨ã³ãéçºæ¯æ´ããã³å®å¶ä½
                        <ul>
<li>JavaScript / Node.js / FLASH / Redmine</li>
</ul>
</li>
<li>æå®é¨æ£ã»ã³ãµã¼æå ±éç¥ Facebookãã¼ã¸å¶ä½
                        <ul>
<li>Node.js</li>
</ul>
</li>
<li>æHTML5ã¯ã¤ãºãµã¤ãä½æ
                        <ul>
<li>JavaScript / Node.js / FLASH</li>
</ul>
</li>
</ul>
<h3 id="2014">2014</h3>
<ul>
<li>æååºè¡ãã¸ã¿ã«ãµã¤ãã¼ã¸å¶ä½
                        <ul>
<li>JavaScript / Node.js / FLASH / PHP / MySQL</li>
</ul>
</li>
<li>æèªåè»ç³»å¶æ¥­ç¨iPadã¢ããªå¶ä½</li>
<li>æèªåè»ç³»ã¤ãã³ãã¿ããããã«ã³ã³ãã³ãå¶ä½</li>
<li>æã¤ã³ã¿ã¼ãããã¦ã§ããµã¤ãã»ã¹ãã¼ããã©ã³ã¢ããªã®éçºã»éå¶ã«é¢é£ããæ¥­åæ¯æ´å¨è¬
                        <ul>
<li>æè¡çãªãã¬ãã¸èª¿æ»ã»å±é / æå ±å±æãã¼ã«æ¯æ´ ã»ã</li>
<li>ï½2016</li>
</ul>
</li>
</ul>
<p>ãã®ä»ãããã¤ãã®æ¡ä»¶ã«æºããã</p>
<h3 id="2013">2013</h3>
<ul>
<li>æã½ã¼ã·ã£ã«ã²ã¼ã ä¼ç¤¾ãã­ã³ãã¨ã³ãéçºæ¯æ´ããã³å®å¶ä½</li>
<li>æèªåè»ç³»ã¤ãã³ãã¿ããããã«ã³ã³ãã³ãå¶ä½</li>
<li>ã­ã£ã³ãã¼ã³ç¨ãã¤ã³ãå©ç¨ããå¼ãã²ã¼ã ä½æ</li>
</ul>
<p>ãã®ä»ãããã¤ãã®æ¡ä»¶ã«æºããã</p>
<h3 id="2004-2012">2004-2012</h3>
<ul>
<li>2004å¹´ããããªã¼ã©ã³ã¹ãFLASHãä¸­å¿ã«ã¤ã³ã¿ã©ã¯ãã£ãã³ã³ãã³ããå¶ä½ããã¸ã¿ã«ãµã¤ãã¼ã¸ãAIRã«ããã¢ããªå¶ä½ãçµé¨ã2011å¹´ä»¥éã¯ãå ãã¦JavaScriptãHTML5ã¢ãã¡ã¼ã·ã§ã³ãè¤éåããã¹ãã¼ããã©ã³æ¼åºå¶ä½ã®ã¯ã¼ã¯ãã­ã¼æ¹åã«é¢ããã¾ããã</li>
</ul>
<h2 id="-">å¾æã¨ãããã¨</h2>
<p><strong>ãã¤ã³ã¿ã¼ãã§ã¼ã¹ãå¥½ãã</strong><br/><strong>ããã­ãã¿ã¤ãããæ°æã¡ã</strong><br/><strong>ããã¸ãã£ããªé¢ããã</strong> </p>
<ul>
<li>ã¤ã³ã¿ã©ã¯ãã£ãã³ã³ãã³ãå¶ä½ã®çµé¨ããã¼ã¹ã«ããã­ã³ãã¨ã³ãéçºã«ããã¦ãã¼ã ãã«ãã£ã³ã°ãUIã¾ããã®éçºãè¦ä»¶å®ç¾©ãªã©ã«é¢ãããã¨ãã§ãã¾ãã</li>
<li>ã¨ããããè§¦ã£ã¦ã¿ããã­ãã¿ã¤ãã®å§¿å¢ã§ãæåã«ãã­ãã¯ããè§¦ã£ã¦ãã£ã¼ãããã¯ãè§£æ±ºç­ãæ¨¡ç´¢ããããæ¢å­ã®æè¡ãæ°ããæè¡ãæ··ãã¦ã¿ã¦æ°ããä¾¡å¤ãæ¢ããã¨ãå¾æã¨ãã¦ãã¾ãã</li>
<li>ããããããã¯ãã­ã¸ã¼ãæ¨ªæ­çã«ãã¾ãåã¿ç ãã¦ãç®ä¸ã®å¶ä½ã¸æ´»ãããç³¸å£ãæ¢ãã¾ãã</li>
<li>å¤ãã®ãã¼ããé¢ãããã­ã¸ã§ã¯ãã«ããã¦ã¹ã ã¼ãºã«é²è¡ããããã³ãã¥ãã±ã¼ã·ã§ã³ãããã¨ãå¿ããã¾ãã</li>
<li>æè¿ã¯éçºã ãã§ãªããã¯ãã«ã«ãã£ã¬ã¯ã·ã§ã³é¨åãæå½ãã¤ã¤ãå°è¦æ¨¡ãªãã¼ã ãææ®ããªããè¡ãæ¡ä»¶å½¢æãè¡ã£ã¦ããã¾ãã</li>
</ul>
</div>
</div>
</div>
<script src="//www.google-analytics.com/urchin.js" type="text/javascript"></script>
<script type="text/javascript">
        _uacct = "UA-8318805-1";
        urchinTracker();
    </script>
</body>
</html>