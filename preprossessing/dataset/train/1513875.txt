<!DOCTYPE html>
<html lang="en-US">
<head>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-57423675-4"></script>
<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-57423675-4');
	</script>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://upodmedia.com/wp-content/uploads/2020/06/upod_favicon.png" rel="shortcut icon"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<title>Page not found – Digital Marketing Solutions Provider</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://upodmedia.com/feed/" rel="alternate" title="UPOD MEDIA » Feed" type="application/rss+xml"/>
<link href="https://upodmedia.com/comments/feed/" rel="alternate" title="UPOD MEDIA » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/upodmedia.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://upodmedia.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://upodmedia.com/wp-content/plugins/lazy-load-for-videos/public/css/lazyload-shared.css?ver=2.12.2" id="lazyload-video-css-css" media="all" rel="stylesheet" type="text/css"/>
<style id="lazyload-video-css-inline-css" type="text/css">
.entry-content a.lazy-load-youtube, a.lazy-load-youtube, .lazy-load-vimeo{ background-size: cover; }.titletext.youtube { display: none; }.lazy-load-div:before { content: "\25B6"; text-shadow: 0px 0px 60px rgba(0,0,0,0.8); }
</style>
<link href="https://upodmedia.com/wp-content/themes/acabado/style.css?ver=5.6" id="income-school-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="custom-style-inline-css" type="text/css">
body { color: ; }
			h1, h2, h3, h4, h5, h6, .header { color: ; }
			a { transition: color .2s; color:  };
			a:hover, a:focus, a:active { color: ; }
			.wp-block-button__link, button,	input[type='button'], input[type='reset'], input[type='submit'], .button { background: ; color: ; }
			.antibounce-container { background: ; }
			.antibounce-card { color: ; }
			.antibounce-card .button { background: ; color:  }

</style>
<link href="https://upodmedia.com/wp-json/" rel="https://api.w.org/"/><link href="https://upodmedia.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://upodmedia.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<meta content="" name="description"/> <!-- Fonts Plugin CSS - https://fontsplugin.com/ -->
<style>
</style>
<!-- Fonts Plugin CSS -->
</head>
<body class="error404 hfeed no-sidebar">
<div class="site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header" id="masthead">
<div class="inner-wrap">
<div class="hamburger-wrapper">
<button aria-controls="primary-menu" aria-expanded="false" aria-label="Menu" class="hamburger hamburger--squeeze menu-toggle" type="button">
<span class="hamburger-box">
<span class="hamburger-inner"></span>
</span>
<span class="label">MENU</span>
</button>
</div>
<div class="site-branding">
<a class="custom-logo-link" href="https://upodmedia.com/" itemprop="url" rel="home"><img alt="upod media" class="attachment-medium size-medium" height="201" loading="lazy" sizes="(max-width: 300px) 100vw, 300px" src="https://upodmedia.com/wp-content/uploads/2020/06/logo-300x201.png" srcset="https://upodmedia.com/wp-content/uploads/2020/06/logo-300x201.png 300w, https://upodmedia.com/wp-content/uploads/2020/06/logo.png 350w" width="300"/></a>
<!-- <h1 class="site-title"><a tabindex="-1" href="" rel="home"></a></h1> -->
<!-- <p class="site-title"><a tabindex="-1" href="" rel="home"></a></p> -->
</div><!-- .site-branding -->
<div class="search-wrapper">
<a href="#open" id="search-icon"><span class="sr-only">Search</span></a>
<form action="https://upodmedia.com/" class="search-form" method="get" role="search">
<style>
	.search-wrapper.search-active .search-field {
		width: 200px;
		display: inline-block;
		vertical-align: top;
	}
	.search-wrapper button[type="submit"] {
		display: inline-block;
		vertical-align: top;
		top: -35px;
		position: relative;
		background-color: transparent;
		height: 30px;
		width: 30px;
		padding: 0;
		margin: 0;
		background-image: url("https://upodmedia.com/wp-content/themes/acabado/img/search-icon.png");
		background-position: center;
		background-repeat: no-repeat;
		background-size: contain;
	}
	.search-wrapper.search-active button[type="submit"] {
		display: inline-block !important;
	}
	</style>
<label for="s">
<span class="screen-reader-text">Search for:</span>
</label>
<input class="search-field" id="search-field" name="s" placeholder="Search …" type="search" value=""/>
<button class="search-submit" style="display:none;" type="submit"><span class="screen-reader-text"></span></button>
</form>
</div>
</div>
<nav class="main-navigation" id="site-navigation">
<!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"></button> -->
<div class="inner-wrap" id="primary-menu"><ul>
<li class="menu-item-8" id="menu-item-8"><a></a></li>
<li class="menu-item-7" id="menu-item-7"><a></a></li>
</ul></div>
</nav><!-- #site-navigation -->
</header><!-- #masthead -->
<div class="site-content" id="content">
<section class="antibounce-container" id="antibounce" style="display: none;">
<article class="antibounce-card">
<div class="stop-sign-wrapper">
<img alt="traffic stop sign" height="140" src="https://upodmedia.com/wp-content/themes/acabado/img/stop-sign.svg" width="140"/>
</div>
<div class="copy-wrapper">
<p class="header">Want to get more traffics to your website? </p>
<p>Good quality website traffics are the key to the success of most businesses, both online and offline. Uncover the top traffic secrets for your business now in this article!</p>
</div>
<button onclick="window.location.href='https://upodmedia.com/website-traffics-matter/';">READ MORE</button>
<!-- <a href="" class="button">READ MORE</a> -->
</article>
</section>
<div class="content-area" id="primary">
<main class="site-main" id="main">
<section class="error-404 not-found">
<div class="page-content">
<img src="https://upodmedia.com/wp-content/themes/acabado/img/404.jpg" width="480"/>
<p>Sorry, we couldn't find the page you're looking for. Please click on menu or view our <a href="/sitemap">site map</a> to continue.</p>
</div><!-- .page-content -->
</section><!-- .error-404 -->
</main><!-- #main -->
</div><!-- #primary -->
<aside class="widget-area" id="secondary">
<div class="about-wrapper">
<h2 class="widget-title">About Us</h2> <div class="about-image" style="background-image: url('https://upodmedia.com/wp-content/uploads/2020/06/about-upodmedia.png')"></div>
<p class="about-copy">UPOD MEDIA is an established digital marketing solutions provider with 10 years of experience in helping businesses achieve their digital goals. Our team helps traditional business to transform to digital, create strong online presence, drive online traffics, generate high quality leads, and ultimately, improving sales figure. </p>
</div>
</aside><!-- #secondary -->
</div><!-- #content -->
<footer class="site-footer" id="colophon">
<div class="inner-wrap">
<div class="site-info">
					© Copyright 2011 - 2021  UPOD MEDIA SDN BHD | All Rights Reserved. 			</div><!-- .site-info -->
<div class="footer-ad">
<p><a href="https://www.bluehost.com/track/offer70/upodmedia" target="_blank"> <img border="0" src="https://bluehost-cdn.com/media/partner/images/offer70/728x90/728x90BW.png"/> </a></p>
</div>
</div>
</footer><!-- #colophon -->
</div><!-- #page -->
<script async="" defer="" id="lazyload-video-js-js" src="https://upodmedia.com/wp-content/plugins/lazy-load-for-videos/public/js/lazyload-shared.js?ver=2.12.2" type="text/javascript"></script>
<script id="lazyload-youtube-js-js-before" type="text/javascript">
window.llvConfig=window.llvConfig||{};window.llvConfig.youtube={"colour":"red","buttonstyle":"","controls":true,"loadpolicy":true,"thumbnailquality":"0","preroll":"","postroll":"","overlaytext":"","loadthumbnail":true,"callback":"<!--YOUTUBE_CALLBACK-->"};
</script>
<script async="" defer="" id="lazyload-youtube-js-js" src="https://upodmedia.com/wp-content/plugins/lazy-load-for-videos/public/js/lazyload-youtube.js?ver=2.12.2" type="text/javascript"></script>
<script id="lazyload-vimeo-js-js-before" type="text/javascript">
window.llvConfig=window.llvConfig||{};window.llvConfig.vimeo={"buttonstyle":"","playercolour":"","preroll":"","postroll":"","show_title":false,"overlaytext":"","loadthumbnail":true,"callback":"<!--VIMEO_CALLBACK-->"};
</script>
<script async="" defer="" id="lazyload-vimeo-js-js" src="https://upodmedia.com/wp-content/plugins/lazy-load-for-videos/public/js/lazyload-vimeo.js?ver=2.12.2" type="text/javascript"></script>
<script async="" defer="" id="income-school-js-js" src="https://upodmedia.com/wp-content/themes/acabado/js/app.min.js?ver=1.0.10" type="text/javascript"></script>
<script async="" defer="" id="wp-embed-js" src="https://upodmedia.com/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
</body>
</html>
