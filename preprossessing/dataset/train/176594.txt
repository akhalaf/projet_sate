<html>
<head></head><body><p>
﻿

</p>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="nopin" name="pinterest"/>
<link href="thumbnailviewer.css" rel="stylesheet" type="text/css"/>
<script src="thumbnailviewer.js" type="text/javascript">
/***********************************************
* Image Thumbnail Viewer Script- © Dynamic Drive (www.dynamicdrive.com)
* This notice must stay intact for legal use.
* Visit https://www.dynamicdrive.com/ for full source code
***********************************************/
</script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<div align="center">
<center>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="center" bgcolor="#FFFFFF" rowspan="2" valign="top" width="24%">
<p align="center"><a href="https://www.aviationartprints.com">
<img alt="Aviation Art Prints .com Home Page" border="0" src="https://www.aviationartprints.com/images/aaplogo5.jpg"/> </a><br/><font color="#000000"><b>Order Helpline (UK) : 01436 820269</b></font></p></td>
<td bgcolor="#FFFFFF" rowspan="2" width="50%">
<p align="center"></p>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<p>
</p><center><font color="#000000" size="2"><i>You currently have no items in your basket</i></font><br/><br/>
<br/><b><font color="#000000" size="4">Coronavirus Info</font> - <font color="#00FF00" size="4">Orders are still being delivered</font></b><br/><font size="4"><a href="corona.php">Click here for details.</a></font><br/></center></td>
<td align="center" bgcolor="#000000" valign="bottom" width="5%">
<p align="center"><a href="https://www.facebook.com/aviationartprints"><img alt="Join us on Facebook!" border="0" height="40" src="https://www.directart.co.uk/mall/images/fblogo.jpg" width="40"/></a></p></td>
<td bgcolor="#FFFFFF" rowspan="2" valign="top" width="13%">
<p align="center"><font color="#000000"><img alt="Payment Options Display" src="https://www.aviationartprints.com/images/cards.jpg" title="Credit Cards and Paypal Accepted Here"/><br/><b>Buy with confidence and security!</b><br/><i><font color="#3342C2">Publishing historical art since 1985</font></i></font></p></td>
</tr>
<tr>
<td align="center" bgcolor="#000000" valign="top" width="5%">
<a href="https://twitter.com/#!/Aviation_Prints"><img alt="Follow us on Twitter!" border="0" height="40" src="https://www.directart.co.uk/mall/images/tlogo.jpg" width="40"/></a></td>
</tr>
<tr>
</tr>
</table>
</center>
</div>
<center>
<div align="center">
<table border="1" cellpadding="1" cellspacing="1" width="100%">
<tr>
<td align="center" bgcolor="#FFFFFF" colspan="5" width="40%">
<a href="https://www.aviationartprints.com/aviation_art_postcards.php"><img src="https://www.aviationartprints.com/images/aapso2.jpg"/></a>
</td>
<td align="center" bgcolor="#FEFEE0" colspan="5" width="60%">
<a href="https://eepurl.com/cmRkj"><b><i>Don't Miss Any Special Deals - Sign Up To Our Newsletter!</i></b></a>
<br/>
<table border="0" bordersize="1" cellpadding="1" cellspacing="1" width="100%">
<tr>
<td align="center" bgcolor="#FEFEE0" width="10%">
<b><i><a href="https://www.aviationartprints.com/aircraft_index.php">Aircraft<br/>Search</a></i></b>
</td>
<td align="center" bgcolor="#FEFEE0" width="10%">
<b><i><a href="https://www.aviationartprints.com/squadron_index.php">Squadron<br/>Search</a></i></b>
</td>
<td align="center" bgcolor="#FEFEE0" width="10%">
<b><i><a href="https://www.aviationartprints.com/artist_index.php">Artist<br/>Search</a></i></b>
</td>
<td align="center" bgcolor="#FEFEE0" width="10%">
<b><i><a href="https://www.aviationartprints.com/signatures_index.php">Signature<br/>Search</a></i>
</b>
</td>
<td align="center" bgcolor="#FEFEE0" width="10%">
<b><i><a href="https://www.aviationartprints.com/country-index.php">Air Force<br/>Search</a></i>
</b>
</td>
<td align="center" valign="middle" width="50%">
<form action="searchresults.php" method="get" name="search">
<br/><b>Product Search     </b><input name="Search" size="20" type="text"/>    <input name="Submit" type="submit" value="Search..."/></form></td>
</tr>
</table>
</td>
</tr>
</table>
</div>
</center>
<center>
<table bordersize="0" width="100%">
<tr>
<td align="center" bgcolor="#F2FCF2" colspan="7" width="50%">
<b><i><a href="https://www.aviationartprints.com/aviation_artist_print_lists.php">Click Here For Full Artist Print Indexes</a></i></b>
</td>
<td align="center" bgcolor="#CCFFFF" colspan="7" width="50%">
<b><i><a href="https://www.aviationartprints.com/aviation_history_search.htm">Aviation History Archive</a></i></b>
</td>
</tr></table>
</center>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Harry Koch " name="description"/>
<title>Harry Koch - Pilot Profile -  Harry Koch</title>
<p>
</p><table align="center" border="0" cellpadding="2" cellspacing="0" width="95%">
<tr>
<td align="center" valign="top">
<p align="center"><b> <font color="#0033CC"> 
        Harry Koch 
        </font></b> </p>
<p> 
		No Photo Available 
	</p>
<p> <font color="#000000">
<b>Victories : 13 
        <br/>----------------------------- 
        <br/>Country : Germany 
        <br/>Fought in : WW2 
        <br/>Fought for : Axis 

 
 
        		 
         
        </b></font> </p>
<p> <font color="#000000">
</font> </p>
</td>
</tr>
</table>
<br/><table bordercolor="#ffffff" bordersize="1" width="100%"><tr><td bgcolor="#ffffff" colspan="4" width="100%"><b><font color="#000000"></font></b><center>Latest Axis Aviation Artwork !</center></td></tr><tr><td align="center" valign="top" width="25%"><b><a href="https://www.aviationartprints.com/aviation_art.php?ProdID=29676"><img alt=" Shortly after mid day on 26th August 1940, a Bolton-Paul Defiant of 264 Sqn claimed a victory that was to make history many decades later.  Dornier Do.17Z2, Wk No 1160 of 7/III KG.3 had been part of a raiding force sent to attack targets in Essex.  Attacked from below, the Do.17 suffered terminal damage and came to rest in the shallow waters of the Goodwin Sands, near Deal in Kent.  Two of her crew died in the incident, but two others survived and became prisoners of war.  In June 2013, over seventy years later, 5K+AR was raised from the water to be put on display at the RAF Museum in Hendon, becoming the only example of its type to survive anywhere in the world. " src="https://www.directart.co.uk/mall/images/small/dhm6425pc.jpg" title=" Shortly after mid day on 26th August 1940, a Bolton-Paul Defiant of 264 Sqn claimed a victory that was to make history many decades later.  Dornier Do.17Z2, Wk No 1160 of 7/III KG.3 had been part of a raiding force sent to attack targets in Essex.  Attacked from below, the Do.17 suffered terminal damage and came to rest in the shallow waters of the Goodwin Sands, near Deal in Kent.  Two of her crew died in the incident, but two others survived and became prisoners of war.  In June 2013, over seventy years later, 5K+AR was raised from the water to be put on display at the RAF Museum in Hendon, becoming the only example of its type to survive anywhere in the world. "/></a></b><br/><a href="https://www.directart.co.uk/mall/images/800s/dhm6425pc.jpg" rel="thumbnail"><img src="https://www.directart.co.uk/mall/images/small/enlarge.jpg" title="5K+AR Sole Survivor by Ivan Berryman. (PC)"/></a><br/><font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"> <b>5K+AR Sole Survivor by Ivan Berryman. (PC)</b></font></td><td align="center" valign="top" width="25%"><b><a href="https://www.aviationartprints.com/aviation_art.php?ProdID=29672"><img alt=" Arriving too late to take any meaningful part in the war, Dornier's extraordinary Do.335 &lt;i&gt;Pfeil&lt;/i&gt; (Arrow) might have made a significant contribution to the war in the air, had it gone into mass production.  Drawing on the company's considerable experience with pull-push engine arrangements, the Do.335 represented a radical change in fighter design, resulting in an aircraft with a top speed of 763 Km/h at 6500m.  Here the seventh of ten Do.335A-Os gets airborne from Oberpfaffenhofen en route for evaluation by the Erprobungskommando 335 in September 1944. " src="https://www.directart.co.uk/mall/images/small/dhm6378pc.jpg" title=" Arriving too late to take any meaningful part in the war, Dornier's extraordinary Do.335 &lt;i&gt;Pfeil&lt;/i&gt; (Arrow) might have made a significant contribution to the war in the air, had it gone into mass production.  Drawing on the company's considerable experience with pull-push engine arrangements, the Do.335 represented a radical change in fighter design, resulting in an aircraft with a top speed of 763 Km/h at 6500m.  Here the seventh of ten Do.335A-Os gets airborne from Oberpfaffenhofen en route for evaluation by the Erprobungskommando 335 in September 1944. "/></a></b><br/><a href="https://www.directart.co.uk/mall/images/800s/dhm6378pc.jpg" rel="thumbnail"><img src="https://www.directart.co.uk/mall/images/small/enlarge.jpg" title="Dornier's Golden Arrow by Ivan Berryman. (PC)"/></a><br/><font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"> <b>Dornier's Golden Arrow by Ivan Berryman. (PC)</b></font></td><td align="center" valign="top" width="25%"><b><a href="https://www.aviationartprints.com/aviation_art.php?ProdID=29671"><img alt=" Messerschmitt Bf.109G-2s of 6./JG 5 sit quietly following a fresh snowfall in March 1943, the aircraft swept down and ready for action, should the call come.  Nearest aircraft is that of August Mors, 'Yellow 7', whilst the mount of Heinrich Ehrler, 'Yellow 12' sits nearby.  Just three months later, on 12th June, Mors was forced to abandon his stricken machine, baling out over enemy territory.  Despite this, he managed to evade capture and was back with his unit only six days later. " src="https://www.directart.co.uk/mall/images/small/dhm6377pc.jpg" title=" Messerschmitt Bf.109G-2s of 6./JG 5 sit quietly following a fresh snowfall in March 1943, the aircraft swept down and ready for action, should the call come.  Nearest aircraft is that of August Mors, 'Yellow 7', whilst the mount of Heinrich Ehrler, 'Yellow 12' sits nearby.  Just three months later, on 12th June, Mors was forced to abandon his stricken machine, baling out over enemy territory.  Despite this, he managed to evade capture and was back with his unit only six days later. "/></a></b><br/><a href="https://www.directart.co.uk/mall/images/800s/dhm6377pc.jpg" rel="thumbnail"><img src="https://www.directart.co.uk/mall/images/small/enlarge.jpg" title="Eagle's Rest by Ivan Berryman. (PC)"/></a><br/><font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"> <b>Eagle's Rest by Ivan Berryman. (PC)</b></font></td><td align="center" valign="top" width="25%"><b><a href="https://www.aviationartprints.com/aviation_art.php?ProdID=29670"><img alt=" Serving in a huge variety of roles throughout WW2, the Dornier Do.17's origins lay in a design for a high speed mail plane capable of carrying six passengers.  Numerous developments saw it mature into one of the world's most iconic bombers, typified here by these three Do.17Z-2s of 1 Gruppe, KG 2, based at Tatoi in Greece during 1941. " src="https://www.directart.co.uk/mall/images/small/dhm6376pc.jpg" title=" Serving in a huge variety of roles throughout WW2, the Dornier Do.17's origins lay in a design for a high speed mail plane capable of carrying six passengers.  Numerous developments saw it mature into one of the world's most iconic bombers, typified here by these three Do.17Z-2s of 1 Gruppe, KG 2, based at Tatoi in Greece during 1941. "/></a></b><br/><a href="https://www.directart.co.uk/mall/images/800s/dhm6376pc.jpg" rel="thumbnail"><img src="https://www.directart.co.uk/mall/images/small/enlarge.jpg" title="The Ubiquitous Raider - Dornier Do.17s of 1 Gruppe KG2 by Ivan Berryman. (PC)"/></a><br/><font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"> <b>The Ubiquitous Raider - Dornier Do.17s of 1 Gruppe KG2 by Ivan Berryman. (PC)</b></font></td></tr></table>
<p align="center"> 
Harry Koch 
</p><p align="center">
</p><p align="center">
</p><center><table border="1" width="100%"><tr bgcolor="#000000"><td align="center" colspan="2" width="100%"><font color="#FFFFFF"><b>Squadrons for : Harry Koch</b></font></td></tr><tr bgcolor="#000000"><td align="center" colspan="3" width="100%"><font color="#FFFFFF"><b>A list of all squadrons known to have been served with by Harry Koch.  A profile page is available by clicking the squadron name.</b></font></td></tr><tr bgcolor="#808080"><b></b><td align="center" width="20%">Squadron</td><td align="center" width="80%">Info</td></tr><tr bgcolor="#000000"><td align="center" valign="top" width="20%"><br/><table width="95%"><tr bgcolor="#FFFFFF"><td align="center"><a href="https://www.aviationartprints.com/squadron_history.php?Squadron=257">JG1</a></td></tr></table><br/><font color="#FFFFFF"><br/>Country : Germany<br/>'Ace of Hearts'<br/><br/><font color="#FFFFFF" size="2"><i>Click the name above to see prints featuring aircraft of JG1</i></font></font></td><td align="center" width="80%"><font color="#FFFFFF"><b>JG1</b><br/><br/>German World War II fighter unit or wing which used the Messerschmitt Bf 109 and Focke-Wulf Fw 190 aircraft, between 1940–1944. The name of the unit derives from Jagd, meaning hunt and Geschwader, meaning wing. First formed in May 1939 in eastern Prussia, I./JG 1 was one of the original groups created by the Luftwaffe as part of its expansion plans.<br/><br/>Between 1940 and 1942, JG 1 operated primarily over the Western Front and northern occupied Europe. During the initial days of the war, JG 1 faced little resistance, apart from occasional Royal Air Force (RAF) excursions. The unit was rarely engaged in large-scale confrontations during this time. From late 1942 onwards it was tasked with defense of the Reich duties. After D-Day, elements of JG 1 were moved to France and were tasked with air support to the army Wehrmacht, along with their air defense role. Operation Bodenplatte severely reduced the strength of JG 1.<br/><br/>Towards the end of the war, the unit was disbanded and its remaining pilots and aircraft were re-organized. What remained of these groups surrendered to Allied forces at the end of the war.<br/><br/>JG 1 was the first unit to attempt 'aerial bombing' techniques against the United States Army Air Forces (USAAF) heavy bomber formations. It was the only unit to be equipped with the Heinkel He 162 jet fighter.<br/><br/>In 1944 the Oesau suffix was added to the unit's title, after its late Geschwaderkommodore Oberst Walter Oesau (127 kills), who was killed in action. Some 700 enemy aircraft were claimed shot down during the war.</font></td></tr><tr bgcolor="#22443F"><td align="center" valign="top" width="20%"><br/><table width="95%"><tr bgcolor="#FFFFFF"><td align="center"><a href="https://www.aviationartprints.com/squadron_history.php?Squadron=224">JG26</a></td></tr></table><br/><font color="#FFFFFF"><br/>Country : Germany<br/>'Ace of Hearts'<br/><br/><font color="#FFFFFF" size="2"><i>Click the name above to see prints featuring aircraft of JG26</i></font></font></td><td align="center" width="80%"><font color="#FFFFFF"><b>JG26</b><br/><br/>Jagdgeschwader 26 Schlageter was a Luftwaffe fighter-wing of World War II. It operated mainly in Western Europe against Great Britain, France and the United States but also saw service against Russia. It was named after Albert Leo Schlageter, a World War I veteran and Freikorps member arrested and executed by the French for sabotage in 1923.<b><br/><br/>Commanders of II. Gruppe JG 26<br/><br/></b>Hptm. Werner Palm, 1 May 1939 – 27 June 1939<br/>Hptm Herwig Knüppel, 28 June 1939 – 19 May 1940<br/>Hptm Karl Ebbighausen, 20 May 1940 – 31 May 1940<br/>Hptm. Erich Noack, 1 June 1940 – 24 July 1940<br/>Hptm Karl Ebbighausen, 25 July 1940 – 16 August 1940<br/>Hptm Erich Bode, 17 August 1940 – 3.10.40<br/>Hptm Walter Adolph, 4 October 1940 – 18 September 1941<br/>Hptm Joachim Müncheberg, 19 September 1941 – 21 July 1942<br/>Hptm Conny Meyer, 22 July 1942 – 2 January 1943<br/>Maj Wilhelm-Ferdinand Galland, 3 January 43 – 17 August 1943<br/>Hptm Hans Naumann, 18 August 1943 – 8 September 1943<br/>ObLt Johannes Seifert, 9 September 1943 – 25 November 1943<br/>Maj Wilhelm Gäth, 26 November 1943 – 1 March 1944<br/>Hptm Hans Naumann, 2 March 1944 – 28 June 1944<br/><b>Hptm Emil Lang, 29 June 1944 – 3 September 1944<br/></b>Hptm Georg-Peter Eder, 4 September 1944 – 8 October 1944<br/>Maj Anton Hackl, 9 October 1944 – 29 January 45<br/>ObLt Waldemar Radener, 30 January 1945 – 22 February 1945<br/>Hptm Paul Schauder, 23 February 1945 – 1 May 1945</font></td></tr></table></center><table border="1" width="100%"><tr><td bgcolor="#000000" colspan="8" width="100%"><p align="center"><b><font color="#FFFFFF" size="3">Known Victory Claims - Harry Koch</font></b></p></td></tr><tr><td bgcolor="#C0C0C0" width="10%"><p align="center"><b><font size="2">DATE</font></b></p></td><td bgcolor="#C0C0C0" width="25%"><p align="center"><b><font size="2">PILOT</font></b></p></td><td bgcolor="#C0C0C0" width="5%"><p align="center"><b>UNIT</b></p></td><td bgcolor="#C0C0C0" width="10%"><p align="center"><b>JG</b></p></td><td bgcolor="#C0C0C0" width="15%"><p align="center"><b>CLAIMED</b></p></td><td bgcolor="#C0C0C0" width="15%"><p align="center"><b>LOCATION</b></p></td><td bgcolor="#C0C0C0" width="5%"><p align="center"><b>TIME</b></p></td><td bgcolor="#C0C0C0" width="15%"><p align="center"><b>FRONT</b></p></td></tr></table><table width="100%"><tr bgcolor="#000000"><td align="center" width="10%"><font color="#FFFFFF">30/08/1940</font></td><td align="center" width="25%"><font color="#FFFFFF">Fw. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">5</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 26</font></td><td align="left" width="15%"><font color="#FFFFFF">Hurricane</font></td><td align="left" width="15%"><font color="#FFFFFF">Dungeness</font></td><td align="left" width="5%"><font color="#FFFFFF">18.15</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#22443F"><td align="center" width="10%"><font color="#FFFFFF">01/10/1940</font></td><td align="center" width="25%"><font color="#FFFFFF">Fw. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">5</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 26</font></td><td align="left" width="15%"><font color="#FFFFFF">Hurricane</font></td><td align="left" width="15%"><font color="#FFFFFF">Brighton</font></td><td align="left" width="5%"><font color="#FFFFFF">15.3</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#000000"><td align="center" width="10%"><font color="#FFFFFF">29/11/1940</font></td><td align="center" width="25%"><font color="#FFFFFF">Fw. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">5</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 26</font></td><td align="left" width="15%"><font color="#FFFFFF">Hurricane</font></td><td align="left" width="15%"><font color="#FFFFFF">Tonbridge</font></td><td align="left" width="5%"><font color="#FFFFFF">13.5</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#22443F"><td align="center" width="10%"><font color="#FFFFFF">08/04/1941</font></td><td align="center" width="25%"><font color="#FFFFFF">Ofw. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">5</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 26</font></td><td align="left" width="15%"><font color="#FFFFFF">Blenheim</font></td><td align="left" width="15%"><font color="#FFFFFF">Ile de Batz NE Brest</font></td><td align="left" width="5%"><font color="#FFFFFF">13.25</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#000000"><td align="center" width="10%"><font color="#FFFFFF">07/08/1941</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">5</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 26</font></td><td align="left" width="15%"><font color="#FFFFFF">Spitfire</font></td><td align="left" width="15%"><font color="#FFFFFF">Guînes</font></td><td align="left" width="5%"><font color="#FFFFFF">11.3</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#22443F"><td align="center" width="10%"><font color="#FFFFFF">16/08/1941</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">5</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 26</font></td><td align="left" width="15%"><font color="#FFFFFF">Spitfire</font></td><td align="left" width="15%"><font color="#FFFFFF">E. Gravelines: 5-6000m</font></td><td align="left" width="5%"><font color="#FFFFFF">9.3</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#000000"><td align="center" width="10%"><font color="#FFFFFF">19/08/1941</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">5</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 26</font></td><td align="left" width="15%"><font color="#FFFFFF">Spitfire</font></td><td align="left" width="15%"><font color="#FFFFFF">Wormhoudt</font></td><td align="left" width="5%"><font color="#FFFFFF">12</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#22443F"><td align="center" width="10%"><font color="#FFFFFF">27/09/1941</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">5</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 26</font></td><td align="left" width="15%"><font color="#FFFFFF">Spitfire</font></td><td align="left" width="15%"><font color="#FFFFFF">Gravelines</font></td><td align="left" width="5%"><font color="#FFFFFF">15.35</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#000000"><td align="center" width="10%"><font color="#FFFFFF">08/11/1941</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">5</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 26</font></td><td align="left" width="15%"><font color="#FFFFFF">Spitfire</font></td><td align="left" width="15%"><font color="#FFFFFF">Hardifort</font></td><td align="left" width="5%"><font color="#FFFFFF">13.05-10</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#22443F"><td align="center" width="10%"><font color="#FFFFFF">10/03/1943</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">6</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 1</font></td><td align="left" width="15%"><font color="#FFFFFF">Spitfire</font></td><td align="left" width="15%"><font color="#FFFFFF">7km W. Westkapellen</font></td><td align="left" width="5%"><font color="#FFFFFF">16.13</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#000000"><td align="center" width="10%"><font color="#FFFFFF">18/03/1943</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">6</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 1</font></td><td align="left" width="15%"><font color="#FFFFFF">Spitfire</font></td><td align="left" width="15%"><font color="#FFFFFF">10km W. Schouwen</font></td><td align="left" width="5%"><font color="#FFFFFF">15.45</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#22443F"><td align="center" width="10%"><font color="#FFFFFF">16/05/1943</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">6</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 1</font></td><td align="left" width="15%"><font color="#FFFFFF">P-47</font></td><td align="left" width="15%"><font color="#FFFFFF">10km north Zeebrugge</font></td><td align="left" width="5%"><font color="#FFFFFF">13.13</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#000000"><td align="center" width="10%"><font color="#FFFFFF">22/06/1943</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">6</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 1</font></td><td align="left" width="15%"><font color="#FFFFFF">B-17</font></td><td align="left" width="15%"><font color="#FFFFFF">-</font></td><td align="left" width="5%"><font color="#FFFFFF">-</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#22443F"><td align="center" width="10%"><font color="#FFFFFF">25/07/1943</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">6</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 1</font></td><td align="left" width="15%"><font color="#FFFFFF">B-17</font></td><td align="left" width="15%"><font color="#FFFFFF">-</font></td><td align="left" width="5%"><font color="#FFFFFF">-</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#000000"><td align="center" width="10%"><font color="#FFFFFF">26/07/1943</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">6</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 1</font></td><td align="left" width="15%"><font color="#FFFFFF">B-17</font></td><td align="left" width="15%"><font color="#FFFFFF">HU-7: 7400m [S. Hildesheim]</font></td><td align="left" width="5%"><font color="#FFFFFF">12</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#22443F"><td align="center" width="10%"><font color="#FFFFFF">28/07/1943</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">6</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 1</font></td><td align="left" width="15%"><font color="#FFFFFF">B-17</font></td><td align="left" width="15%"><font color="#FFFFFF">HS-6: 5000m [Herford]</font></td><td align="left" width="5%"><font color="#FFFFFF">10.28</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#000000"><td align="center" width="10%"><font color="#FFFFFF">28/07/1943</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">6</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 1</font></td><td align="left" width="15%"><font color="#FFFFFF">B-17</font></td><td align="left" width="15%"><font color="#FFFFFF">05 Ost S/HU-8: 5000m</font></td><td align="left" width="5%"><font color="#FFFFFF">10.46</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#22443F"><td align="center" width="10%"><font color="#FFFFFF">30/07/1943</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">6</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 1</font></td><td align="left" width="15%"><font color="#FFFFFF">B-17</font></td><td align="left" width="15%"><font color="#FFFFFF">HJ-9: 5000m [S. Utrecht]</font></td><td align="left" width="5%"><font color="#FFFFFF">10.46</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#000000"><td align="center" width="10%"><font color="#FFFFFF">12/08/1943</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">6</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 1</font></td><td align="left" width="15%"><font color="#FFFFFF">B-17</font></td><td align="left" width="15%"><font color="#FFFFFF">OP-47: 7000m [Bad Neuenahr]</font></td><td align="left" width="5%"><font color="#FFFFFF">9.21</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr><tr bgcolor="#22443F"><td align="center" width="10%"><font color="#FFFFFF">17/08/1943</font></td><td align="center" width="25%"><font color="#FFFFFF">Oblt. Harry Koch</font></td><td align="left" width="5%"><font color="#FFFFFF">6</font></td><td align="left" width="10%"><font color="#FFFFFF">JG 1</font></td><td align="left" width="15%"><font color="#FFFFFF">B-17</font></td><td align="left" width="15%"><font color="#FFFFFF">05 Ost S/NM-9: 6000m [Aachen]</font></td><td align="left" width="5%"><font color="#FFFFFF">15</font></td><td align="center" width="15%"><font color="#FFFFFF">Western Front</font></td></tr></table><p align="left"><b><font color="#FFFFFF">Known Claims : 20</font>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<div align="center">
<center>
<table border="1" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td bgcolor="#EEEEEE" rowspan="2" width="35%">
<p align="center">
<b><i><a href="contact_details.php">Contact Details</a></i></b><br/>
<b><i><a href="shipping_info.php">Shipping Info</a></i></b><br/>
<i><b><a href="terms_and_conditions.php">Terms and Conditions</a></b></i><br/>
<i><b><a href="cookies.php">Cookie Policy</a></b></i><br/>
<i><b><a href="privacy.php">Privacy Policy</a></b></i><br/>
<i><b><a href="classifieds.php">Classifieds</a></b></i><br/>
</p>
</td>
<td align="center" bgcolor="#000000" valign="bottom" width="5%">
<p align="center"><a href="https://www.facebook.com/aviationartprints"><img alt="Join us on Facebook!" border="0" height="40" src="https://www.directart.co.uk/mall/images/fblogo.jpg" width="40"/></a></p></td>
<td bgcolor="#EEEEEE" rowspan="2" width="60%">
<p align="center">
<font size="4"><a href="https://eepurl.com/cmRkj"><b><i>Sign Up To Our Newsletter!</i></b></a></font>
<br/><br/><i>Stay up to date with all our latest offers, deals and events as well as new releases and exclusive subscriber content!</i><br/>
</p><p align="center"><font color="#000000" size="1">This website is owned by
        Cranston Fine Arts.  Torwood House, Torwoodhill Road, Rhu,
        Helensburgh, Scotland, G848LE</font></p>
<p align="center"><font size="1"><font color="#000000">Contact: Tel: (+44) (0) 1436 820269.  Fax:
        (+44) (0) 1436 820473. Email: cranstonorders -at- outlook.com
        

      <br/>
</font>
</font></p></td>
</tr>
<tr>
<td align="center" bgcolor="#000000" valign="top" width="5%">
<a href="https://twitter.com/#!/Aviation_Prints"><img alt="Follow us on Twitter!" border="0" height="40" src="https://www.directart.co.uk/mall/images/tlogo.jpg" width="40"/></a></td>
</tr>
</table>
<p align="center"><a href="https://www.aviationartprints.com">Return to Home Page</a>
</p></center>
</div>
</b></p></body></html>