<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "12728",
      cRay: "6116bc9918371985",
      cHash: "7a6b637ad3c435d",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly9hZmZpbGlhdGVib290Y2FtcC5jb20v",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "PuJ7le8wqyqIJM+m1tSED48/0XnjFwhEbMc6SzTh4hhI5avLPZZJzWzMY8tJcZYKTM9sBJIGBT/watZmDGt67YfrVhaedh9m900Y0xwtpmQNhgGYLIvl+Dw8BH5gGTiGxI+lE3Gwz0xtIy0DQD6EdJ40/xTHMMZHxSBsI8idoday/prWmbAg6hVSTXKKyFD7Ap8bDeGU87FApkFQlG6Qt3KNaCeBcHbbQhjyyxEFDMQEQviDIc/GRvSvK/x+O207s9ArjfltLjf91Z1w+OX3zKwTv3En1vn6Yasu6Qz0iTl7HjZxcllrSTHLdew6yH8XSba2U6RgrZwfJ8sv9O5dh4m13F9B/oZ7gMk7zndh79gU4EIYbLKUs9EmrdYHURg2nOC20SF544DxwJw3EyIa7g4VU2ft9uHrnRZm7LD7cz/JWSf5ACI7MYWvhLEaDWibjc9T6cWjFN7Q6hzCxeO65wsDRvzhbsD2uG5R+eqfffuAP/Ep8I4iaIf7njf8B8Emh2uu1QJIKiRzbNjJ+BL7mWXg7D8EELNqDokEy6iTs5/yWhjpHe5Mbi5oivsEJQjsVqduYCWdouYxStZxK8jEKs1nT6ZC1V7jexJLKlskKeV6JibGVhkw74TEDCCmkWQnwE/I6aoaUH2bQmchk0k12IBti2PBXRBYTUiRomJ2m0XhyRbC2PTjWzHU0jdcDbJ3CAiPXf5bbnuX1pOprzybQ7VlsjMgKt2iUYh4RaQQrJodW7Y+rbyX+U6l+brWuVvboeyS6d0xYwf+RpBGJf2ku0BhxnjnS+bZLVKvUKfayVw=",
        t: "MTYxMDYyMDgwNC4wMjEwMDA=",
        m: "lC/0Wjw0W94I3pxQMeepVBJ4eNz0Dcir5QsrUTef79k=",
        i1: "TI5nUN32vHf/E2gxOs+jxw==",
        i2: "EeZ9LIbsBsn0gzcnwUfBeg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "T52ypZfaop0NVJw3+dsYZBKDULn2xe6HdRkQFfka3TE=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=6116bc9918371985");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> affiliatebootcamp.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=247e087911b64ef11745c7cca1696ffc8347e02e-1610620804-0-Ae0yJzBf3XDIRhhEhZr-jAz0bP_Mo8pOjKw3GrcyAZVhaKUHSt_fFI9duBbBOWESARIUmKi9swHKodGveoz5eYl175j24HZhGYSm-nvkzeoNn-akh50QUE-OtsTtkCIs8-sQWNmX8_xh67QNxMQuIqbraeDfGs_nyxHCYszdoUZVymkp41NswY55ZPG9I1mFNnTjCdQY7IPPSzGmLhCHr1MLQVd5ysQ-PQSxAjcxof78W8KjXYiEoERg54yls5fvkN8b_jw0AMe8PSzFTNQ4VJXY8oMbCjUAZ4YU3HPCX-WMK3Gc6Vdvn7nausag_OC1mMzAvgkMPucYEMhF8jeIF9yChCr8V72HqLfhJwRAcNyLt0e3lcu1B_CRgFpSPy_cpQ" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="a27dbfef227db6e90922168983ed77c2ce14cb30-1610620804-0-AUcVLfHeCluJbBvWUraMOFfW6WvRQMpF7UBp85GKgi+8MsyrW/FwLYoTF2rPKIoGI3V0rxNjNtWDDSyzh06GxpjQZOH5XmDTTq2sNpGVIsZDzhm4ikoL8B3C5BsNgvIzlWVSllHmyuP3dywpVXOKtqOtAy55MxExZgN/SMXEHi3XqisL+wGiUF5Ri2/k/9o3lI26mJN9pFWZaTOyvsSTkFZ2igZbDzN+R6ZE/pH1BX+W3TrfrexQKn4DcC0/2D3v0dzMJE2VkeDRIT9u95EdJKWgCl2eT2l7cA1ydFsndubsRQ7E4AsNHmyY1lqwDonYoInD4Fab2ZrmxZOdoRvWXF3s5FeIVht/R9O7isjFijnstcaObc2KdAFI0oKHrF0xzXjXJJwUs2HnbxQPgnzWoz6Y8+LUWtem7IbaAq3avTIKHPthed4bzx6wJOS5QAB1zNfj+W+CGb6MIx4qPEytpK3NMsWu1l2XYbflZ/JwXORhKorQQqdl9494weyJjaXV20dvFb7qdhV0K4raW47cxZcU6RTM4L8VVH+8moLZWlQHu3XOj+ZCusEwZ4gaNwgfXEAJPJhWR8tL6KrdRi3lNmD8npkWL3/3VEBfA1splyFrQkHNDeebSGkCAO+QOBwcLwC0LS8GIW/HG6ItUC3W2nE3jGw4r+zznGVIM8ADXCTPX8eWdI+Ow6ifutHczIpPbnkTQlAl0FH+9K98ioOiJ/jZKMOcJwxVPJ6NLwrrfsTj2jHpZMaU1xiSb6PVMjZjTYPBr0bGFF37A+NFodvZ2uaxQX7QAO5yCkt6lcNYZLGRNsVaSo8puh0N4UKWukTpaGRtTrqow27m9u0rfSenxAZ7tC1ncx773SYgUwry+BEHnlhGtYiNkL//WWI5nIfHd/tgAGtzdv3P84JV8lEa5YtnFRn1+910PxUrRurutsZm8KYGoYUA1j/iDMudCFUQeAb7ttKBXDwtaKo/MNMq6FtxAfcL2uzXwW1WDgt4aKnDv8UpabwORN0PCS7V3WGkTBi0MzFDd6tlSbKHnZkjKuMlPqv2NlZ57kfxArxW57Dro9MrqDxaBRzUck/FJoMj7PEMwpWMlWVWAr/WkYazomSgbROgX6yCyJmPmYhXo0JQPfPV52xVX7LnRZOKVXrLq4bvdPiAKwOrFN6Dk8elpJGySpbwlcTd9dDjAzn4jLyALmeXG7Ulh6dW54mPnNURqet8PekqRhVVhpevXRSLxs9ovKBJ8nrcdtGYcQp17j4cJKtP3Xf7v/ZK4o8vevP597FNk4K3gtOSdLOxcjfnII3QOc+HbowEJYitHw0z8qMatCcVmwk+CgoEXV3rqytRB0NAGTXNJJjJ+6fHyZ13lTk="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="eb2486b9932d185b814b9e409c468b04"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610620808.021-ga9sdFjQM0"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=6116bc9918371985')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>6116bc9918371985</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
