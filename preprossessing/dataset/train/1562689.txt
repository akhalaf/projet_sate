<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>WaspStudio – WaspStudio productora Audiovisual</title>
<!-- Favicons-->
<!-- https://www.waspstudio.es/assets/vendor/bootstrap/css/bootstrap.min.css -->
<link height="auto" href="https://www.waspstudio.es/assets/icon/icon.png" rel="shortcut icon" width="2px"/>
<!-- Web Fonts-->
<link href="https://fonts.googleapis.com/css?family=Hind:400,700%7cLora:400i%7cPoppins:500,600,700" rel="stylesheet"/>
<!-- Bootstrap core CSS-->
<link href="https://www.waspstudio.es/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Plugins and Icon Fonts-->
<link href="https://www.waspstudio.es/assets/css/plugins.min.css" rel="stylesheet"/>
<!-- Template core CSS-->
<link href="https://www.waspstudio.es/assets/css/template.min.css" rel="stylesheet"/>
<link href="https://www.waspstudio.es/css/cssPage.css" rel="stylesheet"/>
<!-- Styles -->
<style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
</head>
<body style="background-color: black;">
<div class="flex-center position-ref full-height">
<div class="content">
<div class="title m-b-md">
<!-- Laravel -->
<img src="https://www.waspstudio.es/assets/images_wasp/Logo/logo.png" style="max-width: 50%;"/>
</div>
<div>
<p style="color: lightgray">No s'ha pogut trobar la pàgina que cerqueu.</p>
<p style="color: white;"><b>ERROR (404)</b></p>
<br/>
<a class="btn btn-outline-light" href="https://www.waspstudio.es" style="-webkit-appearance: inherit" type="button">WaspStudio</a>
</div>
</div>
</div>
</body>
</html>
