<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport"/>
<title>Armpower.NET - the Home of armwrestling  # Armwrestling # Armpower.net</title>
<meta content="index,follow" name="Robots"/>
<meta content="0" http-equiv="Expires"/>
<meta content="NO-CACHE" http-equiv="Cache-Control"/>
<meta content="no-cache" http-equiv="Pragma"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="rrn2tsZawCK5VbrsvYvWfiB3vUws6jzPDEjXNaqD" name="csrf-token"/>
<meta content="100000078934487" property="fb:admins"/>
<meta content="Mazurenko Promotion" name="author"/>
<meta content="armwrestling armsport armpower armwars ironarm armsport internet multimedia interviews news trainings reports siłowanie wywiady wiadomości trening" name="Keywords"/>
<meta content="Professional Armwrestling Site - Professional Armwrestling Site - forum, armsport, competitions, training equipment, rules, trainings and technique, world rankings and hall of fame" name="Description"/>
<meta content="# Armwrestling # Armpower.net" property="og:site_name"/>
<meta content="http://pl.armpower.net/img/site/default/default_675x350.jpg" property="og:image"/>
<meta content="Professional Armwrestling Site - Professional Armwrestling Site - forum, armsport, competitions, training equipment, rules, trainings and technique, world rankings and hall of fame" property="og:description"/>
<meta content="Armpower.NET - the Home of armwrestling" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="general" name="rating"/>
<meta content="index, follow" name="robots"/>
<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
<link href="http://pl.armpower.net/img/site/default/default_675x350.jpg" rel="image_src"/>
<link href="/favicon/16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/favicon/32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/favicon/57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="/favicon/60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="/favicon/72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/favicon/76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/favicon/96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="/favicon/114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="/favicon/120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/favicon/144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="/favicon/152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/favicon/180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/favicon/192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<script src="https://www.google.com/recaptcha/api.js"></script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300i,700" rel="stylesheet"/>
<link href="https://en.armpower.net/css/owl.carousel.min.css?t=1610536051" rel="stylesheet"/>
<link href="https://en.armpower.net/css/owl.theme.default.min.css?t=1610536051" rel="stylesheet"/>
<link href="https://en.armpower.net/css/style.css?t=1610536051" rel="stylesheet"/>
<script type="text/javascript">
        var language_site = 'en';
    </script>
<!-- Hotjar Tracking Code for http://www.armpower.net -->
<script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:595467,hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-0262811558318975",
            enable_page_level_ads: true
        });
    </script>
</head>
<body>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-12370703-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-12370703-2');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter48547961 = new Ya.Metrika({
                    id:48547961,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
            });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/48547961" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<header>
<div class="header-container">
<div class="header-container__left">
<a href="/">
<img alt="# Armwrestling # Armpower.net" src="/img/site/logo.png"/>
</a>
</div>
<div class="header-container__right">
<div class="header-container__right--top">
<div class="header-container__right--tl">
<span class="header-container__right--lang">Language</span>
<div>
<span class="current-flags" id="change-lang" style="background-position: -202px -39px;"></span>
<div class="choose-flags" id="choose-lang">
<a class="choose-flags__link" href="http://pl.armpower.net/" style="background-position: -202px 0;"></a>
<a class="choose-flags__link" href="http://ru.armpower.net/" style="background-position: -202px -20px;"></a>
</div>
</div>
<span class="header-slash">/</span>
<a href="/contact">Contact</a>
<span class="header-slash">/</span>
</div>
<div class="header-container__right--sm">
<a class="header-facebook" href="https://www.facebook.com/pal.armwrestling/" target="_blank"></a>
<a class="header-twitter" href="https://twitter.com/wac_world" target="_blank"></a>
<a class="header-vkontakte" href="https://vk.com/armpowernet" target="_blank"></a>
<a class="header-youtube" href="https://www.youtube.com/user/armpowernet" target="_blank"></a>
</div>
</div>
<div class="header-container__right--bottom search-xl">
<form action="http://www.google.com" class="header-container__right--btm-form" target="_blank">
<input name="cx" type="hidden" value="partner-pub-0262811558318975:5662049021"/>
<input name="ie" type="hidden" value="UTF-8"/>
<input class="header-search-input" name="q" placeholder="search" type="text"/>
<input class="header-search-btn" name="sa" type="submit" value=""/>
</form>
</div>
</div>
<div class="header-container__bottom-xs">
<div class="header-container__right--bottom">
<form action="http://www.google.com" class="header-container__right--btm-form" target="_blank">
<input name="cx" type="hidden" value="partner-pub-0262811558318975:5662049021"/>
<input name="ie" type="hidden" value="UTF-8"/>
<input class="header-search-input" name="q" placeholder="search" type="text"/>
<input class="header-search-btn" name="sa" type="submit" value=""/>
</form>
<span class="nav-btn" id="nav-btn"></span>
</div>
</div>
</div>
</header>
<nav>
<ul>
<li class="no-active"><a href="/archive/articles">news</a></li>
<li class="no-active">
<a class="sub-nav-link" href="javascript:void(0)">Competitions</a>
<div class="transparent-space-nav">
<ul class="sub-nav">
<li><a href="/calendar">&gt; Calendar</a></li>
<li><a href="/score">&gt; Results</a></li>
<li><a href="http://www.zlotytur.com/" target="_blank">&gt; Złoty tur</a></li>
</ul>
</div>
</li>
<li class="no-active"><a href="/galleries">gallery</a></li>
<li class="no-active">
<a class="sub-nav-link" href="javascript:void(0)">Armwrestling</a>
<div class="transparent-space-nav">
<ul class="sub-nav">
<li><a href="/about">&gt; About armwrestling</a></li>
<li><a href="/history">&gt; History</a></li>
<li><a href="/rules">&gt; Rules</a></li>
<li><a href="/clubs">&gt; Clubs</a></li>
<li><a href="/hall-of-fame">&gt; Hall of fame</a></li>
<li><a href="/trainings">&gt; Trainings</a></li>
<li><a href="/magazines">&gt; Armpower Magazine</a></li>
</ul>
</div>
</li>
<li class="no-active">
<a href="https://armbets.tv" target="_blank">Armbets.tv</a>
</li>
<li class="no-active">
<a href="/shop">Shop</a>
</li>
</ul>
</nav>
<div id="fb-root"></div>
<script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.8&appId=1604744616512779";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<section class="hot-news">
<div class="hot-news__container">
<h3 class="hot-news__container--title">hot news <span class="three-arows">&gt;&gt;&gt;</span></h3>
<div class="hot-news__content">
<div class="main-hot-news hover-effect">
<a class="main-hot-news__link" href="/article/5101/kamil-jablonski-armpowernet-hero-2020" style="background-image: url('/img/725x620/mCjToG_sandomierz.jpg');">
</a>
<div class="main-hot-news__content">
<h2 class="main-hot-news__title"><a href="/article/5101/kamil-jablonski-armpowernet-hero-2020">KAMIL JABŁOŃSKI – ARMPOWER.NET HERO 2020</a>
</h2>
<p><a href="/article/5101/kamil-jablonski-armpowernet-hero-2020">The English version of our page and the numbers show that the title Hero of the Year belongs to Malin Kleinsmith. The interview with this athlete was read by a record-breaking number of readers in its English form. Record-breaking is an understatement! Overwhelmingly record-breaking, mega and totally record-breaking. I'll add modestly that I was the one to hold this interview, and that the athlete was suggested to me by Anna Mazurenko.</a></p>
<span class="main-hot-news__date">(<span data-date="1610473500"></span>)</span>
</div>
<div class="main-hot-news__content-xs">
<a href="/article/5101/kamil-jablonski-armpowernet-hero-2020">
<span class="bold-title">KAMIL JABŁOŃSKI – ARMPOWER.NET HERO 2020</span>
                                    The English version of our page and the numbers show that the title Hero of the Year belongs to Malin Kleinsmith. The interview with this athlete was read by a record-breaking number of readers in its English form. Record-breaking is an understatement! Overwhelmingly record-breaking, mega and totally record-breaking. I'll add modestly that I was the one to hold this interview, and that the athlete was suggested to me by Anna Mazurenko.
                                </a>
</div>
</div>
<div class="other-hot-news">
<div class="other-hot-news__top">
<div class="other-hot-news__top--one-div hover-effect">
<a class="one-article__link" href="/article/5100/pal-2021-plans-hopes-real-prospects" style="background-image: url('/img/465x340/e8KGGt_pal-otw.jpg');">
</a>
<div class="one-article__content">
<h3 class="one-article__content--title"><a href="/article/5100/pal-2021-plans-hopes-real-prospects">PAL 2021 - PLANS, HOPES, REAL PROSPECTS</a></h3>
<p class="lid-xs"><a href="/article/5100/pal-2021-plans-hopes-real-prospects">2021 is here. Each of us subconsciously hoped that with the departure of 2020 all the problems it brought to us would evaporate immediately. But, unfortunately, the problems of the past year were and are so fundamental that to overcome them it is not enough just to turn over the sheet of the calendar.</a></p>
<a class="one-article__content--date" href="/article/5100/pal-2021-plans-hopes-real-prospects">(<span data-date="1610222820"></span>)</a>
</div>
</div> <div class="other-hot-news__top--one-div hover-effect">
<a class="one-article__link" href="/article/5099/dmitry-trubin-armpowernet-hero" style="background-image: url('/img/465x340/XUpbNH_hero-rus.jpg');">
</a>
<div class="one-article__content">
<h3 class="one-article__content--title"><a href="/article/5099/dmitry-trubin-armpowernet-hero">DMITRY TRUBIN - ARMPOWER.NET HERO!</a></h3>
<p class="lid-xs"><a href="/article/5099/dmitry-trubin-armpowernet-hero">The highest viewership results for articles published in 2020 on armpower.net in English have already been announced. The interview with Malin Kleinsmith won. Time for the publication results of the Russian site armpower.net. The interview with Dmitry Trubin was the most read here.</a></p>
<a class="one-article__content--date" href="/article/5099/dmitry-trubin-armpowernet-hero">(<span data-date="1610192340"></span>)</a>
</div>
</div> </div>
<div class="other-hot-news__bottom">
<div class="other-hot-news__top--one-div hover-effect">
<a class="one-article__link" href="/article/5098/a-few-sentences-from-ifa" style="background-image: url('/img/465x340/FBnHf5_otw-ifa.jpg');">
</a>
<div class="one-article__content">
<h3 class="one-article__content--title"><a href="/article/5098/a-few-sentences-from-ifa">A FEW SENTENCES FROM IFA</a></h3>
<p class="lid-xs"><a href="/article/5098/a-few-sentences-from-ifa">What was, how was and what awaits us in 2021? Anders Axklo has a voice.</a></p>
<a class="one-article__content--date" href="/article/5098/a-few-sentences-from-ifa">(<span data-date="1610038860"></span>)</a>
</div>
</div> <div class="other-hot-news__top--one-div hover-effect">
<a class="one-article__link" href="/article/5097/the-heroes-of-armpowernet-2020" style="background-image: url('/img/465x340/6VwnqL_malin-hero.jpg');">
</a>
<div class="one-article__content">
<h3 class="one-article__content--title"><a href="/article/5097/the-heroes-of-armpowernet-2020">THE HEROES OF ARMPOWER.NET 2020</a></h3>
<p class="lid-xs"><a href="/article/5097/the-heroes-of-armpowernet-2020">The heroes of armpower.net so... who brought the website the most views? In the first place, with an astounding advantage over any other materials, came the interview with Malin Kleinsmith published in September last year.</a></p>
<a class="one-article__content--date" href="/article/5097/the-heroes-of-armpowernet-2020">(<span data-date="1610005080"></span>)</a>
</div>
</div> </div>
</div>
</div>
</div>
</section>
<section class="other-news">
<div class="other-news__container">
<h3 class="other-news__container--title"><a href="/archive/articles">news <span class="three-arows">&gt;&gt;&gt;</span></a></h3>
<div class="other-news__container--all">
<div class="container-all__top">
<div class="container-all__top--left">
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5096/to-get-vaccinated-or-not-so" style="background-image: url('/img/360x360/ismLZA_szczepienie.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5096/to-get-vaccinated-or-not-so">To get vaccinated or not, so...</a></h3>
</div>
</div>
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5095/something-funny" style="background-image: url('/img/360x360/srPYne_bukiet.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5095/something-funny">Something funny…</a></h3>
</div>
</div> </div>
<div class="container-all__top--right">
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5094/happy-new-year-2021" style="background-image: url('/img/360x360/dukDfn_2021angl.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5094/happy-new-year-2021">Happy New Year 2021!</a></h3>
</div>
</div>
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5093/pavlo-tank-derbedyenyev" style="background-image: url('/img/360x360/zcaEGO_untitled-1.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5093/pavlo-tank-derbedyenyev">Pavlo "Tank" Derbedyenyev</a></h3>
</div>
</div> </div>
</div>
<div class="container-all__bottom">
<div class="container-all__top--left">
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5092/have-a-blessed-and-merry-christmas" style="background-image: url('/img/360x360/duRJzO_prezenty.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5092/have-a-blessed-and-merry-christmas">Have a blessed and merry Christmas!</a></h3>
</div>
</div>
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5091/dave-chaffee-my-life-is-pretty-uneventful" style="background-image: url('/img/360x360/pq4eAn_dave-1.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5091/dave-chaffee-my-life-is-pretty-uneventful">Dave Chaffee: “My life is pretty uneventful”</a></h3>
</div>
</div> </div>
<div class="container-all__top--right other-news-xs">
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5090/do-we-need-the-olympic-games" style="background-image: url('/img/360x360/OroHiX_bojery.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5090/do-we-need-the-olympic-games">Do we need the Olympic Games?</a></h3>
</div>
</div>
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5089/anders-axklo-we-are-looking-forward" style="background-image: url('/img/360x360/VZSTcr_axlo-aa.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5089/anders-axklo-we-are-looking-forward">Anders Axklo: We are looking forward...</a></h3>
</div>
</div> </div>
</div>
</div>
</div>
</section>
<section class="main-events">
<div class="events__container">
<div class="events__container--all">
<h3 class="promo-title"><a href="/calendar">Calendar <span class="three-arows">&gt;&gt;&gt;</span></a></h3>
<div class="main-events__list">
<h4 class="newsletter-partners__container--title">No upcoming events</h4>
</div>
<div class="main-events__other">
<a href="/calendar">more &gt;</a>
</div>
</div>
<div class="events__container--promo">
<h3 class="promo-title"><a href="/calendar">Recommended competitions <span class="three-arows">&gt;&gt;&gt;</span></a></h3>
<h4 class="newsletter-partners__container--title">No upcoming events</h4>
</div>
</div>
</section>
<section class="other-news">
<div class="other-news__container">
<div class="other-news__container--all">
<div class="container-all__top">
<div class="container-all__top--left">
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5088/happy-birthday-igor-mazurenko" style="background-image: url('/img/360x360/m2IUFC_igor_urodzinowa2020_armpower_news_rotator.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5088/happy-birthday-igor-mazurenko">Happy birthday, Igor Mazurenko!</a></h3>
</div>
</div>
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5087/sean-boom-hancock-i-went-to-my-first-tournament" style="background-image: url('/img/360x360/77TwnX_01.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5087/sean-boom-hancock-i-went-to-my-first-tournament">Sean "BOOM" Hancock: “I went to my first tournament...</a></h3>
</div>
</div> </div>
<div class="container-all__top--right">
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5086/nuno-hampy-because-over-the-top" style="background-image: url('/img/360x360/eF0iAF_dziewczynka.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5086/nuno-hampy-because-over-the-top">Nuno Hampy: Because  “Over the Top”</a></h3>
</div>
</div>
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5085/its-not-good-but-it-will-get-better" style="background-image: url('/img/360x360/on7g2e_ifa-raport.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5085/its-not-good-but-it-will-get-better">It's not good, but it will get better!</a></h3>
</div>
</div> </div>
</div>
<div class="container-all__bottom">
<div class="container-all__top--left">
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5084/daniel-mosier-armwrestling-has-taken-me-around-the-world" style="background-image: url('/img/360x360/n0ke8W_otwdan.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5084/daniel-mosier-armwrestling-has-taken-me-around-the-world">Daniel Mosier: Armwrestling has  taken me around the world...</a></h3>
</div>
</div>
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5083/engin-terzi-using-this-genetic-as-good-as-possible" style="background-image: url('/img/360x360/8zEP3Q_trubin.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5083/engin-terzi-using-this-genetic-as-good-as-possible">Engin Terzi: “Using this genetic as good as possible!”</a></h3>
</div>
</div> </div>
<div class="container-all__top--right other-news-xs">
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5082/andriy-pushkar-it-was-two-years-ago" style="background-image: url('/img/360x360/Vu0P1s_andrzej-p.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5082/andriy-pushkar-it-was-two-years-ago">Andriy Pushkar... It was two years ago...</a></h3>
</div>
</div>
<div class="other-news__one-div hover-effect">
<a class="other-news__one-div--link" href="/article/5081/kyiv-open-cup-colorful-fights-and-crazy-emotions" style="background-image: url('/img/360x360/rWqGgY_rtotw.jpg');">
</a>
<div class="other-news__one-div--content">
<h3><a href="/article/5081/kyiv-open-cup-colorful-fights-and-crazy-emotions">KYIV OPEN CUP - COLORFUL FIGHTS AND CRAZY EMOTIONS!</a></h3>
</div>
</div> </div>
</div>
</div>
</div>
</section>
<section class="main-gallery">
<div class="main-gallery__container">
<h3 class="other-news__container--title gallery-home-title"><a href="/gallery">gallery <span class="three-arows">&gt;&gt;&gt;</span></a></h3>
<div class="main-gallery__container--all">
<a class="main-gallery-link hover-effect" href="/gallery/460/xx-mistrzostwa-polski-w-silowaniu-na-rece">
<img alt="XX MISTRZOSTWA POLSKI W SIŁOWANIU NA RĘCE # Armwrestling # Armpower.net" class="" src="http://g3.armpower.net/photos/gal_00460/200x200/xx-mistrzostwa-polski-w-silowaniu-na-rece-310274.JPG"/>
<span>XX MISTRZOSTWA POLSKI W SIŁOWANIU NA RĘCE</span>
</a>
<a class="main-gallery-link hover-effect" href="/gallery/459/top8-zloty-tur-2019">
<img alt="TOP8 &amp; Zloty Tur 2019 # Armwrestling # Armpower.net" class="" src="http://g1.armpower.net/photos/gal_00459/200x200/top8-zloty-tur-2019-305907.JPG"/>
<span>TOP8 &amp; Zloty Tur 2019</span>
</a>
<a class="main-gallery-link hover-effect" href="/gallery/458/ifa-world-championship-2019">
<img alt="IFA World Championship 2019 # Armwrestling # Armpower.net" class="" src="http://g2.armpower.net/photos/gal_00458/200x200/ifa-world-championship-2019-300100.JPG"/>
<span>IFA World Championship 2019</span>
</a>
<a class="main-gallery-link hover-effect" href="/gallery/456/kiev-open-2019-autumn-section">
<img alt="Kiev Open 2019 - Autumn section # Armwrestling # Armpower.net" class="" src="http://g2.armpower.net/photos/gal_00456/200x200/kiev-open-2019-autumn-section-298044.JPG"/>
<span>Kiev Open 2019 - Autumn section</span>
</a>
<a class="main-gallery-link hover-effect" href="/gallery/455/puchar-polski-2019-reda">
<img alt="Puchar Polski 2019 - Reda # Armwrestling # Armpower.net" class="" src="http://g3.armpower.net/photos/gal_00455/200x200/puchar-polski-2019-reda-297789.jpg"/>
<span>Puchar Polski 2019 - Reda</span>
</a>
<a class="main-gallery-link hover-effect" href="/gallery/454/d1-china-open-top8-stage-2">
<img alt="D1 China Open &amp; TOP8 - Stage 2 # Armwrestling # Armpower.net" class="" src="http://g1.armpower.net/photos/gal_00454/200x200/d1-china-open-top8-stage-2-297683.JPG"/>
<span>D1 China Open &amp; TOP8 - Stage 2</span>
</a>
</div>
<div class="main-events__other other-gallery__link">
<a href="/gallery">more &gt;</a>
</div>
</div>
</section>
<section class="advert">
<div class="advert__container">
<div class="advert-one" style="
        display: flex;
    justify-content: center;
    align-items: center;">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Nowy Arm box -->
<ins class="adsbygoogle" data-ad-client="ca-pub-0262811558318975" data-ad-slot="2612081788" style="display:inline-block;width:300px;height:250px"></ins>
<script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
</div>
<div class="advert-one advert-training">
<a href="/banner/12" target="_blank">
<img alt="# Armwrestling # Armpower.net" src="/img/o/ZKMgfd_bkegyz_startowa_3d_04.jpg"/>
</a>
</div>
<div class="advert-one last-advert" style="
        display: flex;
    justify-content: center;
    align-items: center;">
<a href="/banner/12" target="_blank">
<img alt="# Armwrestling # Armpower.net" src="/img/o/ZKMgfd_bkegyz_startowa_3d_04.jpg"/>
</a>
</div>
</div>
</section>
<section class="main-social-media">
<h3 class="hot-news__container--title title-social-media">look for us us on <span class="three-arows">&gt;&gt;&gt;</span></h3>
<div class="main-social-media__container">
<div class="main-social-media__container--facebook">
<div class="facebook-xl">
<div class="fb-page" data-adapt-container-width="true" data-height="575" data-hide-cover="false" data-href="https://www.facebook.com/pal.armwrestling/" data-show-facepile="true" data-small-header="false" data-tabs="timeline" data-width="500">
<blockquote cite="https://www.facebook.com/pal.armwrestling/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pal.armwrestling/">PAL -
                                Professional
                                Armwrestling League</a></blockquote>
</div>
</div>
<a class="banner-social-media__xs" href="#">
<img alt="Facebook # Armwrestling # Armpower.net" class="banner-social-media__xs" src="/img/site/banner-facebook.jpg"/>
</a>
</div>
<div class="main-social-media__container--vkontakte">
<script src="//vk.com/js/api/openapi.js?116" type="text/javascript"></script>
<div class="vkontakte-xl">
<div id="vk_groups" style="width: 100%;"></div>
</div>
<a class="banner-social-media__xs" href="#">
<img alt="Vkontakte # Armwrestling # Armpower.net" class="banner-social-media__xs" src="/img/site/banner-vkontakte.jpg"/>
</a>
<script type="text/javascript">
                    var width = 'auto';
                    var height = '575px';
                    VK.Widgets.Group("vk_groups", {mode: 2, width: width, height: height}, 35257496);
                </script>
</div>
</div>
</section>
<section class="bottom-navbar">
<div class="bottom-navbar__container">
<div class="bottom-navbar__container--link">
<a class="bottom-navbar__container--link-img" href="/">
<img alt="# Armwrestling # Armpower.net" src="/img/site/logo_sm.png"/>
</a>
<a href="/armpower">About the site</a>
<span>|</span>
<a href="/advert">Advertisement</a>
<span>|</span>
<a href="/site-map">Site map</a>
<span class="xs-none">|</span>
<a href="/site-rules">Terms of use</a>
<span>|</span>
<a href="/contact">Contact</a>
</div>
<div class="bottom-navbar__container--lang">
<span>Language</span>
<div>
<span class="current-flags current-flags-bottom" id="change-lang-bottom" style="background-position: -202px -39px;"></span>
<div class="bottom-navbar__choose-flags" id="choose-lang-bottom">
<a class="choose-flags__link" href="http://pl./" style="background-position: -202px 0;"></a>
<a class="choose-flags__link" href="http://ru./" style="background-position: -202px -20px;"></a>
</div>
</div>
</div>
</div>
</section>
<section class="newsletter-partners">
<div class="newsletter-partners__container">
<div class="newsletter__container">
<h4 class="newsletter-partners__container--title">Newsletter</h4>
<p>
                Want to be up to date with Armwrestling?<br/>Subscribe to our newsletter!
            </p>
<form accept-charset="UTF-8" action="https://en.armpower.net/add-newsletter" id="newsletter-form" method="POST"><input name="_token" type="hidden" value="rrn2tsZawCK5VbrsvYvWfiB3vUws6jzPDEjXNaqD"/>
<input class="newsletter__container--input" name="user_email" placeholder="Your email" required="" type="email"/>
<input class="newsletter__container--btn" type="submit" value="&gt;"/>
</form>
<script type="text/javascript">
                var newsletter_scroll = 0;
            </script>
</div>
<div class="partners__container">
<h4 class="newsletter-partners__container--title partners-title">our partners</h4>
<div class="partners__container--logotype">
<a href="http://www.zlotytur.com/" target="_blank">
<img alt="# Armwrestling # Armpower.net" src="/img/site/partners/zlotytur.jpg"/>
</a>
<a href="https://vk.com/armfight_market" target="_blank">
<img alt="# Armwrestling # Armpower.net" src="/img/site/partners/market.jpg"/>
</a>
<a href="http://wac-armwrestling.com/" target="_blank">
<img alt="# Armwrestling # Armpower.net" src="/img/site/partners/wac.jpg"/>
</a>
<a href="http://www.armbets.tv/index.php" target="_blank">
<img alt="# Armwrestling # Armpower.net" src="/img/site/partners/armbets.jpg"/>
</a>
</div>
</div>
</div>
</section>
<footer>
<div class="footer__container">
<div class="footer__container--div">
<div class="footer__container--subdiv">
<div class="footer__container--div-title">
<a class="footer__container--main-title" href="/archive">news</a>
</div>
<div class="footer__container--sec-div-title">
<a class="footer__container--main-title" href="/calendar">competitions and results</a>
<ul class="footer__container--list mt5">
<li><a href="/calendar">&gt; Calendar</a></li>
<li><a href="/score">&gt; Results</a></li>
<li><a href="http://www.zlotytur.com/">&gt; Zloty Tur</a></li>
</ul>
</div>
</div>
</div>
<div class="footer__container--div">
<div class="footer__container--subdiv footer-gallery-link">
<a class="footer__container--main-title" href="/galleries">gallery</a>
<a class="hover-effect" href="/gallery/460/xx-mistrzostwa-polski-w-silowaniu-na-rece" style="display: inline-block;">
<img alt="XX MISTRZOSTWA POLSKI W SIŁOWANIU NA RĘCE # Armwrestling # Armpower.net" class="foooter__gallery--link-img" src="http://g1.armpower.net/photos/gal_00460/200x200/xx-mistrzostwa-polski-w-silowaniu-na-rece-306667.JPG"/>
</a>
</div>
</div>
<div class="footer__container--div">
<div class="footer__container--subdiv">
<a class="footer__container--main-title" href="/about">Armwrestling</a>
</div>
<div class="footer__container--subdiv">
<ul class="footer__container--list mt5">
<li><a href="/about">&gt; About armwrestling</a></li>
<li><a href="/history">&gt; History</a></li>
<li><a href="/rules">&gt; Rules</a></li>
<li><a href="/clubs">&gt; Clubs</a></li>
<li><a href="/hall-of-fame">&gt; Hall of fame</a></li>
<li><a href="/trainings">&gt; Trainings</a></li>
<li><a href="/magazines">&gt; Armpower Magazine</a></li>
</ul>
</div>
</div>
<div class="footer__container--div">
<div class="footer__container--subdiv footer_players">
<a class="footer__container--main-title" href="#">Competitors</a>
</div>
<div class="footer__container--subdiv">
<ul class="footer__container--list mt5">
<li><a href="#">&gt; Profiles</a></li>
<li><a href="/trainings">&gt; Trainings</a></li>
<li><a href="#">&gt; Ranking</a></li>
<li><a href="/clubs">&gt; Clubs</a></li>
</ul>
<div class="footer__gallery-xs">
<a href="/galleries">gallery</a>
</div>
</div>
</div>
<div class="footer__container--div">
<div class="footer__container--subdiv">
<div class="footer__container--div-title xs-border-bottom-none footer_shop">
<a class="footer__container--main-title" href="http://armfight.eu" target="_blank">Shop</a>
</div>
<div class="footer__container--sec-div-title last-footer-div">
<a class="footer__container--main-title" href="https://www.facebook.com/pal.armwrestling/" target="_blank">follow us on</a>
<ul class="footer__container--list mt5">
<li><a href="https://www.facebook.com/pal.armwrestling/" target="_blank">&gt; Facebook</a></li>
<li><a href="https://vk.com/armpowernet" target="_blank">&gt; Vkontakte</a></li>
<li><a href="https://www.youtube.com/user/armpowernet" target="_blank">&gt; Youtube</a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="footer__social-media">
<div class="footer__social-media--div">
<a class="contact-link-footer" href="/contact">Contact</a>
<a class="header-facebook" href="https://www.facebook.com/pal.armwrestling/" target="_blank"></a>
<a class="header-twitter" href="https://twitter.com/wac_world" target="_blank"></a>
<a class="header-vkontakte" href="https://vk.com/armpowernet" target="_blank"></a>
<a class="header-youtube" href="https://www.youtube.com/user/armpowernet" target="_blank"></a>
</div>
</div>
</footer>
<script src="https://en.armpower.net/js/jquery.min.js?t=1610536051" type="text/javascript"></script>
<script src="https://en.armpower.net/js/owl.carousel.min.js?t=1610536051" type="text/javascript"></script>
<script src="https://en.armpower.net/js/moment-with-locales.min.js?t=1610536051" type="text/javascript"></script>
<script src="https://en.armpower.net/js/script.js?t=1610536051" type="text/javascript"></script>
<script>
    moment.locale('en');
</script>
<script type="text/javascript">
    $(document).ready(function(){
        if(newsletter_scroll == 1){
            $('html, body').animate({
                scrollTop: $("#newsletter-form").offset().top
            }, 200);
        }
    });
</script>
</body>
</html>