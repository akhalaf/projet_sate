<!DOCTYPE html>
<html lang="en">
<head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="public" http-equiv="Cache-control"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="ie=edge" http-equiv="X-UA-Compatible"/>
<title>(v2) Welcome - Peterchandra.com</title>
<link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" rel="stylesheet"/>
<script src="https://www.w3counter.com/tracker.js?id=129934"></script>
<script>var clicky_site_ids = clicky_site_ids || []; clicky_site_ids.push(101233837);</script>
<script async="" src="//static.getclicky.com/js"></script>
<style>

    body {
        background-image: url("images/home1.png"); 
        /* Full height */
        height: 100%;
        /* Center and scale the image nicely */
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
    }
   
    .ml-auto {
        left: auto !important;
        right:0;
    }

    .jendelabumi {
        position:absolute; 
        top:40%; 
        left:0; 
        transform: translate(50%,50%);
    }
    
    .jendelabumi .header {
        font-size: 72px; margin-bottom:-50px
    }

    @media (max-width: 740px) {
        body {
            background-image: url("images/mobile.png");
            background-position: right;
        }
        .jendelabumi {
            position:absolute; 
            top: 70%; 
            left:0; 
            transform: translate(10%, 100%);
            color: #ffffff;
        }
        .jendelabumi .header {
            font-size: 32px;
        }
    }
    </style>
</head>
<body oncontextmenu="return false;">
<div class="container-fluid" style="height:100vh; width:100vw;">
<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-transparent bg-sm-light">
<button aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler ml-auto" data-target="#navbarNav" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarNav">
<ul class="navbar-nav ml-auto text-left font-weight-bold">
<li class="nav-item active">
<a class="nav-link " href="index.html">Home <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
<a class="nav-link " href="latestupdate.html">Update</a>
</li>
<li class="nav-item">
<a class="nav-link " href="gallery.html">Gallery</a>
</li>
<li class="nav-item">
<a class="nav-link " href="photobook.html">Photobook</a>
</li>
<li class="nav-item">
<a class="nav-link " href="contact.html">Contact</a>
</li>
</ul>
</div>
</nav>
<div class="jendelabumi">
<label class="header">Jendela Bumi</label><br/>
<label>PHOTOGRAPHY ENTHUSIAST</label>
</div>
</div>
<script crossorigin="anonymous" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script crossorigin="anonymous" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>