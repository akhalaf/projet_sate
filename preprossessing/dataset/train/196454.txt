<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1, user-scalable=0" name="viewport"/>
<meta content="basic Aktiengesellschaft Lebensmittelhandel" name="publisher"/>
<meta content="" name="description"/>
<meta content="Unverträglichkeit, Basic, Bio-Online-Shop, Bio-Rezepte, Bio, Bioanbau, Bioland, Bioprodukte, Biosiegel, Demeter, Ernährung, Bio-Supermarkt, Genuss, Lebensmittel, Naturkosmetik, Naturkost, regional" name="keywords"/>
<meta content="" name="author"/>
<title>basic Bio-Genuss für alle - </title>
<link href="/-/media/BasicBio/Resources/favicon.ashx" rel="SHORTCUT ICON"/>
<link href="/-/media/BasicBio/Resources/favicon.ashx" rel="icon" type="image/ico"/>
<script data-blockingmode="auto" data-cbid="1dc465b3-eaaf-47d2-b162-c6248cb378c2" id="Cookiebot" src="https://consent.cookiebot.com/uc.js" type="text/javascript"></script>
<link href="/bundles/Basic/css?v=Eu6w7MQMIDqYjS9lomOQu2ftO00UVxxnQpL8IMX4yM01" rel="stylesheet"/>
<script src="/bundles/basic/js?v=NOsmlzzd6LWKUvJ5whGRdeH-qCgOIzVcaVjUbr1OKS81"></script>
<script src="//use.typekit.net/fjc3rcf.js"></script>
<script>try { Typekit.load(); } catch (e) { }</script>
</head>
<body>
<div class="container topbar">
<a class="logo" href="/de-DE"><span class="logo-svg"><img class="logo-png" src="/-/media/BasicBio/Resources/fallback/basic.ashx"/></span></a>
<div class="top-nav">
<ul class="nav navbar-nav">
<li><a href="/de-DE/TopNavigation/Unternehmen">Unternehmen</a></li>
<li><a href="/de-DE/TopNavigation/Karriere">Karriere und Ausbildung</a></li>
<li><a href="/de-DE/TopNavigation/Kontakt">Kontakt</a></li>
<li><a href="/de-DE/TopNavigation/Presse">Presse</a></li>
<li><a href="/de-DE/TopNavigation/2020_Unternehmensportrait">Unternehmensportrait</a></li>
<li><a href="/de-DE/TopNavigation/Fact Sheet basic">Fact Sheet basic</a></li>
</ul>
</div> <!--/.nav-collapse -->
</div>
<div class="container tophead">
<h3>Abenteuer Bio: Wir arbeiten daran,<br/>dieses Land zu verändern.</h3>
</div>
<div class="container navbar" style="">
<div class="navbar-collapse collapse">
<ul class="nav navbar-nav">
<li><a href="/de-DE/Angebote">Angebote</a></li> <li><a href="/de-DE/Maerkte">Märkte</a></li> <li class="dropdown ">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Warum Bio?</a> <ul class="dropdown-menu">
<li><a href="/de-DE/Warum Bio">Übersicht</a></li> <li><a href="/de-DE/Warum Bio/Was bedeutet Bio">Was bedeutet Bio?</a></li> <li><a href="/de-DE/Warum Bio/FAQs">FAQs</a></li> <li><a href="/de-DE/Warum Bio/On_Enkeltauglich">Enkeltauglich</a></li> <li><a href="/de-DE/Warum Bio/Bio-Siegel">Bio-Siegel</a></li> </ul>
</li>
<li class="dropdown ">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">basic Top-Themen</a> <ul class="dropdown-menu">
<li><a href="/de-DE/basic Top-Themen">Übersicht</a></li> <li><a href="/de-DE/basic Top-Themen/Corona-Massnahmen">Corona-Maßnahmen und Hygieneregeln</a></li> <li><a href="/de-DE/basic Top-Themen/Offener Brief der Bio-Branche">Offener Brief der Bio-Branche</a></li> <li><a href="/de-DE/basic Top-Themen/basic gegen Ackergifte">basic gegen Ackergifte</a></li> <li><a href="/de-DE/basic Top-Themen/Bienen und Bauern">Bienen und Bauern retten</a></li> <li><a href="/de-DE/basic Top-Themen/Enkeltauglich">Enkeltauglich</a></li> <li><a href="/de-DE/basic Top-Themen/Waschmittel_DE">Wasch- und Spülmittel zum Nachfüllen</a></li> <li><a href="/de-DE/basic Top-Themen/Regionalitaet">Regionalität </a></li> <li><a href="/de-DE/basic Top-Themen/Mikroplastik">Naturkosmetik ohne Mikroplastik</a></li> <li><a href="/de-DE/basic Top-Themen/OETZ Qualitaet von Anfang an">ÖTZ: Qualität von Anfang an</a></li> <li><a href="/de-DE/basic Top-Themen/Saubere_Eier_bei_basic">Saubere Bio-Eier bei basic</a></li> <li><a href="/de-DE/basic Top-Themen/BNN">BNN</a></li> <li class="dropdown-submenu ">
<a href="#">Gluten- und laktosefreie Lebensmittel</a> <ul class="dropdown-menu">
<li><a href="/de-DE/basic Top-Themen/Gluten- und laktosefreie Lebensmittel">Übersicht</a></li>
<li><a href="/de-DE/basic Top-Themen/Gluten- und laktosefreie Lebensmittel/Allergien und Unvertraeglichkeiten">Allergien und Unverträglichkeiten</a></li>
<li><a href="/de-DE/basic Top-Themen/Gluten- und laktosefreie Lebensmittel/Bio bei Allergien und Unvertraeglichkeiten">"Bio" bei Allergien und Unverträglichkeiten</a></li>
</ul>
</li>
<li class="dropdown-submenu ">
<a href="#">Veganes Sortiment</a> <ul class="dropdown-menu">
<li><a href="/de-DE/basic Top-Themen/Veganes Sortiment">Übersicht</a></li>
<li><a href="/de-DE/basic Top-Themen/Veganes Sortiment/Vegetarisch - vegan - vegane Rohkost">Vegetarisch, vegan, vegane Rohkost</a></li>
</ul>
</li>
<li class="dropdown-submenu ">
<a href="#">Bio-Fleisch und -Geflügel</a> <ul class="dropdown-menu">
<li><a href="/de-DE/basic Top-Themen/Bio-Fleisch und -Gefluegel">Übersicht</a></li>
<li><a href="/de-DE/basic Top-Themen/Bio-Fleisch und -Gefluegel/Zurueck zu alten Rassen">Zurück zu alten Rassen</a></li>
<li><a href="/de-DE/basic Top-Themen/Bio-Fleisch und -Gefluegel/basic Gefluegel-Partnerkonzept">basic Geflügel-Partnerkonzept</a></li>
<li><a href="/de-DE/basic Top-Themen/Bio-Fleisch und -Gefluegel/Aktion Bruderherz">basic Bruderherz-Initiative</a></li>
</ul>
</li>
<li class="dropdown-submenu ">
<a href="#">Naturkosmetik</a> <ul class="dropdown-menu">
<li><a href="/de-DE/basic Top-Themen/Naturkosmetik">Übersicht</a></li>
<li><a href="/de-DE/basic Top-Themen/Naturkosmetik/Guetesiegel_v2">Naturkosmetik – Gütesiegel</a></li>
<li><a href="/de-DE/basic Top-Themen/Naturkosmetik/Mikroplastik">Naturkosmetik – Mikroplastik</a></li>
</ul>
</li>
<li><a href="/de-DE/basic Top-Themen/Palmoel in basic Markenprodukten">Palmöl in basic Markenprodukten</a></li> <li><a href="/de-DE/basic Top-Themen/Saisonkalender">Saisonkalender</a></li> <li><a href="/de-DE/basic Top-Themen/Kundeninformation_Bio_besser_fuer_die_Gesundheit">Kundeninformation Bio ist besser für die Gesundheit</a></li> <li><a href="/de-DE/basic Top-Themen/Was ist Glyphosat">Was ist Glyphosat?</a></li> </ul>
</li>
<li><a href="/de-DE/Wein des Monats">Wein des Monats</a></li> <li class="hide "><a href="/de-DE/TopNavigation/Unternehmen">Unternehmen</a></li>
<li class="hide "><a href="/de-DE/TopNavigation/Karriere">Karriere und Ausbildung</a></li>
<li class="hide "><a href="/de-DE/TopNavigation/Kontakt">Kontakt</a></li>
<li class="hide "><a href="/de-DE/TopNavigation/Presse">Presse</a></li>
<li class="hide "><a href="/de-DE/TopNavigation/2020_Unternehmensportrait">Unternehmensportrait</a></li>
<li class="hide "><a href="/de-DE/TopNavigation/Fact Sheet basic">Fact Sheet basic</a></li>
</ul>
</div> <!--/.nav-collapse -->
<button class="navbar-toggle collapsed" data-target=".navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="slide-top" href="#"><span class="icon icon-slidetop"></span></a>
<a class="search" href="/Suche.aspx"><span class="icon icon-search"></span></a>
</div> <!-- /container -->
<div class="container b-crumb">
<div class="bcrumb">
<a href="/de-DE">Startseite</a><span> &gt;</span>
<a href="/de-DE"></a><span> &gt;</span>
Startseite    </div>
</div>
<div class="container homeimg">
<div class="home-slider">
<ul class="slides">
<li>
<a href="/de-DE/basic Top-Themen/Corona-Massnahmen">
<img class="desktop" src="/-/media/BasicBio/Images/Tafeln/Coronaregeln_1880x540_SLIDER_200520_M5.ashx"/>
<img class="mobile" src="/-/media/BasicBio/Images/Tafeln/Coronaregeln_900x460_SLIDER_200520_M5.ashx"/>
</a>
</li>
<li>
<a href="/de-DE/basic Top-Themen/basic gegen Ackergifte">
<img class="desktop" src="/-/media/BasicBio/Images/Tafeln/BAS180253_Tafel_basic_gegen_Ackergifte_1880x540_181023.ashx"/>
<img class="mobile" src="/-/media/BasicBio/Images/Tafeln/BAS180253_Tafel_basic_gegen_Ackergifte_900x460_181023.ashx"/>
</a>
</li>
<li>
<a href="/de-DE/Warum Bio">
<img class="desktop" src="/-/media/BasicBio/Images/Tafeln/Tafel_01_Webseite_1880x540px_HD.ashx"/>
<img class="mobile" src="/-/media/BasicBio/Images/Tafeln/Tafel_01_Webseite_900x460px_HD.ashx"/>
</a>
</li>
</ul>
</div>
</div>
<!-- FlexSlider -->
<script defer="" src="/Scripts/BasicBio/jquery.flexslider-min.js"></script>
<script type="text/javascript">
    $(window).load(function () {
        $('.home-slider').flexslider({
            animation: "slide",
            slideshow: true,
            slideshowSpeed: 4000,
            touch: true,
            directionNav: false,
            controlNav: true,
            useCSS: false
        });
    });
</script>
<div class="container content">
<div class="row teaser-3">
<div class="clearfix col-sm-4 col-xs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/TopNavigation/Presse/Pressemitteilungen/Children Aktion 2020">
<img src="/-/media/BasicBio/Images/Teaser Bilder/201215_Pressefoto_Children_800x600px.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/TopNavigation/Presse/Pressemitteilungen/Children Aktion 2020">
<h5>Soziales Engagement</h5>
<h3>Erfolgreiche Aktion für CHILDREN</h3>
<p>51.520 Euro für „Children for a better World“ gesammelt <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
<div class="clearfix col-sm-4 col-xs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/basic Top-Themen/Waschmittel_DE">
<img src="/-/media/BasicBio/Images/Teaser Bilder/WPR_800x600.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/basic Top-Themen/Waschmittel_DE">
<h5>Wasch- und Spülmittel zum Nachfüllen</h5>
<h3>basic setzt weiter auf Mehrweg  </h3>
<p>Jetzt mitmachen! <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
<div class="clearfix col-sm-4 col-xs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/TopNavigation/Unternehmen/Soziales Engagement/2020_Scheckuebergabe_Kenia_2020">
<img src="/-/media/BasicBio/Images/Teaser Bilder/Teaser_Aidswaisen_2020_800x600.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/TopNavigation/Unternehmen/Soziales Engagement/2020_Scheckuebergabe_Kenia_2020">
<h5>Soziales Engagement</h5>
<h3>Spendenaktion</h3>
<p>21.000 € für Aidswaisen in Kenia <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
</div>
<hr/>
<div class="row teaser-4">
<div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/Angebote"><img src="/-/media/BasicBio/Images/Teaser Bilder/basic200131BioAngeboteExotischVitaminreichJanuar01800x600Vers1.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/Angebote">
<h5>EXOTISCH UND VITAMINREICH GENIESSEN</h5>
<h3>Unsere aktuellen Angebote </h3>
</a>
</div>
<div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/Wein des Monats"><img src="/-/media/BasicBio/Images/Teaser Bilder/Rezepte/basic190288WeindesMonatsBioRebelTinto800x600pxVers1.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/Wein des Monats">
<h5>Unser Wein des Monats</h5>
<h3>Rebel.lia Tinto und Rebel.lia Blanco</h3>
<p> Eine dezente Fruchtnote rundet das Geschmacksbild ab <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
<div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/basic Top-Themen/Regionalitaet"><img src="/-/media/BasicBio/Images/Teaser Bilder/basic_Regionalitaet_Frau_800x600_1406.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/basic Top-Themen/Regionalitaet">
<h5>basic Top-Thema</h5>
<h3>Regionalität </h3>
<p>Genuss aus der Region <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
<div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/basic Top-Themen/Offener Brief der Bio-Branche"><img src="/-/media/BasicBio/Images/Teaser Bilder/BNN_Lieferkettengesetz1_NEU.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/basic Top-Themen/Offener Brief der Bio-Branche">
<h5>Umsetzung der UTP-Richtlinie in deutsches Recht</h5>
<h3>Offener Brief des BNN</h3>
<p>an Politik und Lebensmittelwirtschaft <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
</div><hr/><div class="row teaser-4"> <div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/TopNavigation/Unternehmen/Markenprodukte"><img src="/-/media/BasicBio/Images/Teaser Bilder/Titel_Flyer/basic200009basicMarkenmarkenprodukte800x600Vers2.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/TopNavigation/Unternehmen/Markenprodukte">
<h5>basic Markenprodukte</h5>
<h3>Qualität von basic</h3>
<p>Erfahren Sie mehr über die basic Markenprodukte <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
<div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/basic Top-Themen/Corona-Massnahmen"><img src="/-/media/BasicBio/Images/Teaser Bilder/Coronaregeln_800x600_TEASER_200520.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/basic Top-Themen/Corona-Massnahmen">
<h5>Bitte beachten!</h5>
<h3>Corona-Maßnahmen und Hygieneregeln </h3>
<p>Bleiben Sie gesund <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
<div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/basic Top-Themen/basic gegen Ackergifte"><img src="/-/media/BasicBio/Images/Teaser Bilder/Artikel/Glyphosat_Webseite_800x600px_1003_v9.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/basic Top-Themen/basic gegen Ackergifte">
<h5>basic Top-Thema</h5>
<h3>basic gegen Ackergifte</h3>
<p>Gemeinsam für eine saubere Landwirtschaft <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
<div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/basic Top-Themen/Enkeltauglich"><img src="/-/media/BasicBio/Images/Teaser Bilder/Artikel/Enkeltauglich/Kachel_800x600_V05.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/basic Top-Themen/Enkeltauglich">
<h5>Warum Bio?</h5>
<h3>Enkeltauglicher Lebensstil</h3>
<p>Machen Sie mit! <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
</div><hr/><div class="row teaser-4"> <div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/basic Top-Themen/Bienen und Bauern"><img src="/-/media/BasicBio/Images/Teaser Bilder/Bienen_und_Bauern_retten_800x600.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/basic Top-Themen/Bienen und Bauern">
<h5>Ihre Stimme zählt!</h5>
<h3>Wir brauchen 1.000.000 Unterschriften</h3>
<p>Eine bienenfreundliche Landwirtschaft für eine gesunde Umwelt <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
<div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/basic Top-Themen/Mikroplastik"><img src="/-/media/BasicBio/Images/Teaser Bilder/Pressebilder/Teaser_V4.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/basic Top-Themen/Mikroplastik">
<h5>Naturkosmetik ohne Mikroplastik</h5>
<h3>basic sagt „Nein“ zu Mikroplastik!</h3>
<p>Weiter Informationen finden Sie hier <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
<div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/basic Top-Themen/Ausbildung bei basic"><img src="/-/media/BasicBio/Images/Teaser Bilder/Artikel/basic_azubiwebseite_kachel800x600px.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/basic Top-Themen/Ausbildung bei basic">
<h5>Karriere &amp; Ausbildung</h5>
<h3>Ausbildung bei basic</h3>
<p>Hier erfahren Sie mehr <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
<div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/TopNavigation/Unternehmen/Start-up"><img src="/-/media/BasicBio/Images/Teaser Bilder/Pressebilder/170831_Startup_Webseite_800x600px_0509_FINAL.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/TopNavigation/Unternehmen/Start-up">
<h5>Innovationen fördern</h5>
<h3>basic unterstützt Ihr Bio-Start-up</h3>
<p>Auf der Suche nach Produktneuheiten <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
</div><hr/><div class="row teaser-4"> <div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/basic Top-Themen/BNN"><img src="/-/media/BasicBio/Images/Teaser Bilder/Artikel/basic-BNN-800x600.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/basic Top-Themen/BNN">
<h5>basic Top-Thema</h5>
<h3>Die BNN-Sortiments- richtlinien </h3>
<p>Geprüft und für BIO <br/>befunden <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
<div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/Maerkte"><img src="/-/media/BasicBio/Images/Teaser Bilder/Artikel/AEZ_Ansicht_1_800x600.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/Maerkte">
<h5>Unternehmen</h5>
<h3>Unsere Märkte</h3>
<p>Hier finden Sie uns <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
<div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/TopNavigation/Unternehmen/Expansion"><img src="/-/media/BasicBio/Images/Teaser Bilder/Artikel/Aussenansicht_800x600.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/TopNavigation/Unternehmen/Expansion">
<h5>Unternehmen</h5>
<h3>Expansion</h3>
<p>Auf der Suche nach neuen Standorten <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
<div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/basic Top-Themen/Gluten- und laktosefreie Lebensmittel"><img src="/-/media/BasicBio/Images/Teaser Bilder/Artikel/basic-LaktoseundGlutenfrei-quer.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/basic Top-Themen/Gluten- und laktosefreie Lebensmittel">
<h5>basic Top-Thema</h5>
<h3>Genuss trotz Verzicht</h3>
<p>Gluten- und laktosefreie Lebensmittel <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
</div><hr/><div class="row teaser-4"> <div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/basic Top-Themen/Bio-Fleisch und -Gefluegel"><img src="/-/media/BasicBio/Images/Teaser Bilder/Artikel/basic-BioFleisch-quer.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/basic Top-Themen/Bio-Fleisch und -Gefluegel">
<h5>basic Top-Thema</h5>
<h3>Bio-Fleisch und -Geflügel</h3>
<p>Mit gutem Gewissen <br/>genießen! <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
<div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/basic Top-Themen/Veganes Sortiment"><img src="/-/media/BasicBio/Images/Teaser Bilder/Artikel/basic-VeganesSortiment-quer.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/basic Top-Themen/Veganes Sortiment">
<h5>basic Top-Thema</h5>
<h3>Unser veganes Sortiment</h3>
<p>Köstlich und rein pflanzlich <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
<div class="clearfix col-sm-3 col-xs-6 col-xxs-12">
<div class="teaser-img">
<a class="framed-img" href="/de-DE/basic Top-Themen/Saubere_Eier_bei_basic"><img src="/-/media/BasicBio/Images/Teaser Bilder/Pressebilder/HP-Bilder_Sonderflyer-Saubere-Eier_2017-08-07_04.ashx"/></a>
</div>
<a class="teaser-txt link" href="/de-DE/basic Top-Themen/Saubere_Eier_bei_basic">
<h5>Kundeninformation</h5>
<h3>Saubere Bio-Eier bei basic</h3>
<p>Kein Fipronil in unseren Bio-Eiern <span class="dots-more">...</span><span class="icon icon-more"></span></p>
</a>
</div>
</div>
<hr/>
<!-- /row -->
</div>
<div class="container footer">
<a class="pageup" href="#"><i class="icon icon-pageup"></i> Seitenanfang</a>
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-4 col-xxs-6">
<ul>
<li><a class="linkhead" href="/de-DE/TopNavigation/Kontakt">Kontakt</a></li>
<li><a class="linkhead" href="/de-DE/TopNavigation/Unternehmen">Unternehmen</a></li>
</ul>
</div>
<div class="col-md-2 col-sm-3 col-xs-4 col-xxs-6">
<ul>
<li><a class="linkhead" href="/de-DE/Mediathek">Mediathek</a></li>
<li><a class="linkhead" href="/de-DE/Newsletter">Newsletter</a></li>
</ul>
</div>
<div class="col-md-2 col-sm-3 col-xs-4 col-xxs-6">
<ul>
<li><a class="linkhead" href="/de-DE/TopNavigation/Karriere">Karriere und Ausbildung</a></li>
</ul>
</div>
<div class="col-md-3 hidden-sm"></div>
<div class="col-sm-3 col-xs-12 col-xxs-6 button-links">
<a class="shoplink" href="/Maerkte.aspx">Markt-Suche <span class="icon icon-search-gray"></span></a>
</div>
</div> <!-- /row -->
</div>
<div class="container">
<div class="footerlinks">
<div class="col-xs-12">
                © 2021, basic AG, München   |   
                    <a href="/de-DE/TopNavigation/Impressum">Impressum</a>
                                       |   
                    <a href="/de-DE/TopNavigation/Datenschutz">Datenschutzerklärung</a>
</div>
</div>
</div>
</body>
</html>
