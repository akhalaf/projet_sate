<!DOCTYPE html>
<!--[if lt IE 9]><html class="no-js lt-ie9" lang="en" dir="ltr"><![endif]--><!--[if gt IE 8]><!--><html class="no-js" dir="ltr" lang="en">
<!--<![endif]-->
<head>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta charset="utf-8"/>
<script src="//assets.adobedtm.com/be5dfd287373/0127575cd23a/launch-f7c3e6060667.min.js"></script><!-- Web Experience Toolkit (WET) / Boîte à outils de l'expérience Web (BOEW)
		wet-boew.github.io/wet-boew/License-en.html / wet-boew.github.io/wet-boew/Licence-fr.html -->
<title>404 - We couldn't find that Web page - Natural Resources Canada / Nous ne pouvons trouver cette page Web - Ressources naturelles Canada</title>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<!-- Meta data canadaca 404 -->
<meta content="" name="description"/>
<meta content="404 - We couldn't find that Web page - Natural Resources Canada" property="dcterms.title"/>
<meta content="404 - Nous ne pouvons trouver cette page Web - Ressources naturelles Canada" lang="fr" property="dcterms.title"/>
<meta content="eng" property="dcterms.language" title="ISO639-2"/>
<meta content="fra" property="dcterms.language" title="ISO639-2"/>
<meta content="Natural Resources Canada" property="dcterms.creator"/>
<meta content="Ressources naturelles Canada" lang="fr" property="dcterms.creator"/>
<meta content="NRCan-RNCan" property="dcterms:service"/>
<meta content="2" property="dcterms:accessRights"/>
<meta content="noindex, nofollow, noarchive" name="robots"/>
<!--[if gte IE 9 | !IE ]><!-->
<link href="/sites/all/themes/canadaca/icons/apple-touch-icon.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="/sites/all/themes/canadaca/icons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/sites/all/themes/canadaca/icons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/sites/all/themes/canadaca/icons/site.webmanifest" rel="manifest"/>
<link color="#ba1115" href="/sites/all/themes/canadaca/icons/safari-pinned-tab.svg" rel="mask-icon"/>
<meta content="#ba1115" name="msapplication-TileColor"/>
<meta content="#ffffff" name="theme-color"/>
<link href="/sites/all/libraries/canadaca/GCWeb/css/theme-srv.min.css" rel="stylesheet"/>
<!--<![endif]-->
<!--[if lt IE 9]>
		<link href="/sites/all/libraries/canadaca/GCWeb/assets/favicon.ico" rel="shortcut icon" />
		<link rel="stylesheet" href="/sites/all/libraries/canadaca/GCWeb/css/ie8-theme-srv.min.css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.js"></script>
		<script src="/sites/all/libraries/canadaca/wet-boew/js/ie8-wet-boew.min.js"></script>
		<![endif]-->
<noscript><link href="/sites/all/libraries/canadaca/wet-boew/css/noscript.min.css" rel="stylesheet"/></noscript>
<link href="https://www.nrcan.gc.ca/sites/nrcan/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.nrcan.gc.ca/sites/nrcan/files/css/css_rCsq3RxAUvt6N6c5eX-4oKluAWC4NJvTm_HYKw15epI.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.nrcan.gc.ca/sites/nrcan/files/css/css_iWzMQ11_K-ws-B7w5URa45WUaNRfKoiUq8SG6h3ngDo.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.nrcan.gc.ca/sites/nrcan/files/css/css_bqgoBjUE6gHK31poOLufLMo8Mm5mzIfU0jfCEYMnlfo.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.nrcan.gc.ca/sites/nrcan/files/css/css_7eBCCZgMM0JPtO05v2D690Xwy9qqrb5NQERtTF-cyVM.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.nrcan.gc.ca/sites/nrcan/files/css/css_PGbJgHCUCBf4dg7K9Kt8aAwsApndP4GZ9RuToPy3-Fk.css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.js?v=2.2.4" type="text/javascript"></script>
<script src="https://www.nrcan.gc.ca/sites/nrcan/files/js/js_38VWQ3jjQx0wRFj7gkntZr077GgJoGn5nv3v05IeLLo.js" type="text/javascript"></script>
<script src="https://www.nrcan.gc.ca/sites/nrcan/files/js/js_rsGiM5M1ffe6EhN-RnhM5f3pDyJ8ZAPFJNKpfjtepLk.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-40966744-1", {"cookieDomain":"auto","allowLinker":true});ga("require", "linker");ga("linker:autoLink", ["www.nrcan.gc.ca","www.rncan.gc.ca"]);ga("set", "anonymizeIp", true);ga("set", "page", "/404.html?page=" + document.location.pathname + document.location.search + "&from=" + document.referrer);ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"canadaca","theme_token":"oJMsN3zu119Zqi1qBu-AGeWUEYlUoRKMhiIEiMUn8Ws","js":{"sites\/all\/modules\/nrcan\/theme_upgrades\/match_height\/dist\/jquery.matchHeight-min.js":1,"sites\/all\/modules\/nrcan\/theme_upgrades\/js\/custom_bullets.js":1,"sites\/all\/modules\/nrcan\/theme_upgrades\/js\/nrcan_datatables.js":1,"sites\/all\/modules\/nrcan\/theme_upgrades\/js\/ga_links.js":1,"https:\/\/www.nrcan.gc.ca\/sites\/nrcan\/files\/invitation_manager\/Overlay.js":1,"https:\/\/www.nrcan.gc.ca\/sites\/nrcan\/files\/invitation_manager\/InvitationManager.js":1,"sites\/all\/themes\/canadaca\/js\/canadaca_webform.js":1,"sites\/all\/themes\/canadaca\/js\/wet5.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery-html-prefilter-3.5.0-backport.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/libraries\/canadaca\/GCWeb\/css\/theme.css":1,"public:\/\/invitation_manager\/Overlay.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/nrcan\/theme_upgrades\/css\/cards.css":1,"sites\/all\/modules\/nrcan\/theme_upgrades\/css\/custom_bullets.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/themes\/canadaca\/css\/nrcan.css":1,"sites\/all\/themes\/canadaca\/css\/canadaca_webform.css":1,"sites\/all\/libraries\/canadaca\/css\/messages.css":1,"sites\/all\/modules\/nrcan\/content_health\/css\/content_health.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"bin|csv|doc(x|m)?|exe|flv|gif|jpe?g|js|mp(2|3|4|e?g)|pdf|png|ppt(x|m)?|pps(x|m)?|txt|wav|wma|wmv|xls(x|m|b)?|xml|zip","trackDomainMode":2,"trackCrossDomains":["www.nrcan.gc.ca","www.rncan.gc.ca"]},"content_health":{"feedback_post_url":"\/report-problem"}});
//--><!]]>
</script>
</head>
<body class="" typeof="WebPage" vocab="http://schema.org/">
<header class="container" id="wb-bnr" role="banner">
<div class="row">
<div class="col-sm-6">
<img alt="Government of Canada / Gouvernement du Canada" id="gcwu-sig" src="/sites/all/libraries/canadaca/GCWeb/assets/sig-blk-en.svg"/>
</div>
<div class="col-sm-6">
<img alt="Symbol of the Government of Canada" id="wmms" src="/sites/all/libraries/canadaca/GCWeb/assets/wmms-blk.svg"/>
</div>
</div>
</header>
<main class="container" property="mainContentOfPage" role="main">
<header>
<div class="row" id="messages">
<div class="col-md-12">
</div>
</div>
</header>
<div class="row mrgn-tp-lg">
<h1 class="wb-inv">We couldn't find that Web page (Error 404) / <span lang="fr">Nous ne pouvons trouver cette page Web (Erreur 404)</span></h1>
<section class="col-md-6">
<h2><span class="glyphicon glyphicon-warning-sign mrgn-rght-md"></span> We couldn't find that Web page (Error 404)</h2>
<p>We're sorry you ended up here. Sometimes a page gets moved or deleted, but hopefully we can help you find what you're looking for.</p>
<ul>
<li>Return to the <a href="https://www.nrcan.gc.ca/home">home page</a></li>
<li>Consult the <a href="https://www.nrcan.gc.ca/sitemap">site map</a></li>
<li><a href="https://contact-contactez.nrcan-rncan.gc.ca/index.cfm?lang=eng">Contact us</a> and we'll help you out</li>
</ul>
</section>
<section class="col-md-6" lang="fr">
<h2><span class="glyphicon glyphicon-warning-sign mrgn-rght-md"></span> Nous ne pouvons trouver cette page Web (Erreur 404)</h2>
<p>Nous sommes désolés que vous ayez abouti ici. Il arrive parfois qu'une page ait été déplacée ou supprimée. Heureusement, nous pouvons vous aider à trouver ce que vous cherchez.</p>
<ul>
<li>Retournez à la <a href="https://www.rncan.gc.ca/accueil">page d'accueil</a></li>
<li>Consultez le <a href="https://www.rncan.gc.ca/plan">plan du site</a></li>
<li><a href="https://contact-contactez.nrcan-rncan.gc.ca/index.cfm?lang=fra">Communiquez avec nous</a> pour obtenir de l'aide</li>
</ul>
</section>
</div>
</main>
<!-- Footer -->
<!--[if gte IE 9 | !IE ]><!-->
<!-- wet-boew js -->
<script src="/sites/all/libraries/canadaca/wet-boew/js/wet-boew.js"></script>
<!-- theme js -->
<script src="/sites/all/libraries/canadaca/GCWeb/js/theme.js"></script>
<!--<![endif]-->
<!--[if lt IE 9]>
      <script src="/sites/all/themes/canadaca/js/iecheck.js"></script>
      <script src="/sites/all/libraries/canadaca/wet-boew/js/ie8-wet-boew2.min.js"></script>
    <![endif]-->
<!-- End Footer -->
<script type="text/javascript">_satellite.pageBottom();</script></body>
</html>
