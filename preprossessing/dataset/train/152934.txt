<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta charset="utf-8"/>
<title>art curious</title>
<base href="http://art-curious.com/artcurious/"/>
<meta content="art curious" name="description"/>
<link href="http://art-curious.com/artcurious/image/data/icon4.jpg" rel="icon"/>
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet" type="text/css"/>
<link href="catalog/view/theme/default/stylesheet/slideshow.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="catalog/view/theme/default/stylesheet/carousel.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="catalog/view/javascript/jquery/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css"/>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/nivo-slider/jquery.nivo.slider.pack.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/jquery.jcarousel.min.js" type="text/javascript"></script>
<!--[if IE 7]> 
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
</head>
<body>
<div id="container">
<div id="header">
<div id="logo"><a href="http://art-curious.com/artcurious/index.php?route=common/home"><img alt="Art Curious" src="http://art-curious.com/artcurious/image/data/ah logo2.jpg" title="Art Curious"/></a></div>
<form action="http://art-curious.com/artcurious/index.php?route=module/currency" enctype="multipart/form-data" method="post">
<div id="currency">Currency<br/>
<a onclick="$('input[name=\'currency_code\']').attr('value', 'EUR'); $(this).parent().parent().submit();" title="Euro">€</a>
<a onclick="$('input[name=\'currency_code\']').attr('value', 'GBP'); $(this).parent().parent().submit();" title="Pound Sterling">£</a>
<a title="US Dollar"><b>$</b></a>
<input name="currency_code" type="hidden" value=""/>
<input name="redirect" type="hidden" value="http://art-curious.com/artcurious/index.php?route=common/home"/>
</div>
</form>
<div id="cart">
<div class="heading">
<h4>Shopping Cart</h4>
<a><span id="cart-total">0 item(s) - $0.00</span></a></div>
<div class="content">
<div class="empty">Your shopping cart is empty!</div>
</div>
</div> <div id="search">
<div class="button-search"></div>
<input name="search" placeholder="Search" type="text" value=""/>
</div>
<div id="welcome">
        Welcome visitor you can <a href="http://art-curious.com/artcurious/index.php?route=account/login">login</a> or <a href="http://art-curious.com/artcurious/index.php?route=account/register">create an account</a>.      </div>
<div class="links"><a href="http://art-curious.com/artcurious/index.php?route=common/home">Home</a><a href="http://art-curious.com/artcurious/index.php?route=account/wishlist" id="wishlist-total">Wish List (0)</a><a href="http://art-curious.com/artcurious/index.php?route=account/account">My Account</a><a href="http://art-curious.com/artcurious/index.php?route=checkout/cart">Shopping Cart</a><a href="http://art-curious.com/artcurious/index.php?route=checkout/checkout">Checkout</a></div>
</div>
<div id="menu">
<ul>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=59">Wooden Religious Products</a>
<div>
<ul>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=59_62">Key Chain Pictures (14)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=59_82">Nativity Scenes (3)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=59_101">Religious Pictures (26)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=59_70">Wooden Cross (14)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=59_63">Wooden Personal &amp; Cultural Pictures (11)</a></li>
</ul>
</div>
</li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60">Wooden Products</a>
<div>
<ul>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_64">Animals Pictures (5)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_93">Bagpipe (2)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_77">Ball Point (3)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_76">Bow Ties (7)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_90">Bowl &amp; Plate (7)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_75">Brooches (5)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_97">Candle Holders (3)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_72">Car Hanging (5)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_79">Coaster (3)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_84">Cutting Board &amp; Rolling Pin (1)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_67">Engrave Key Chains (13)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_74">Fourteen Stations Frames (5)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_89">Furniture Legs (2)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_98">Handle Skipping Rope (3)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_78">Hangers (3)</a></li>
</ul>
<ul>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_81">Musical Models (7)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_68">Painted Key Chains (9)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_94">Santa Felt Hat (2)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_91">Table Napkin &amp; Wooden Napkin Ring (3)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_73">Tambourine (7)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_100">Wall Hanging (8)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_80">Wooden Box (2)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_87">Wooden Comb (2)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_83">Wooden jewelry (15)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_99">Wooden Letters (1)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_86">Wooden Models (2)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_85">Wooden Shield (2)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_88">Wooden Stand (4)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_96">Artificial Leathers (6)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=60_92">Acrylic Products (5)</a></li>
</ul>
</div>
</li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=61">Wooden Painted &amp; Non Painted Pictures</a>
<div>
<ul>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=61_65">Engrave Holy Scripture non painted Pictures (11)</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=61_66">Engrave Holy Scripture Painted Pictures (10)</a></li>
</ul>
</div>
</li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/category&amp;path=71">Lathe Items</a>
</li>
</ul>
</div>
<div id="notification"></div>
<div id="content"><div class="slideshow">
<div class="nivoSlider" id="slideshow0" style="width: 980px; height: 400px;">
<img alt="art" src="http://art-curious.com/artcurious/image/cache/data/bb1-980x400.jpg"/>
<img alt="art" src="http://art-curious.com/artcurious/image/cache/data/bb3-980x400.jpg"/>
<img alt="art" src="http://art-curious.com/artcurious/image/cache/data/bb2-980x400.jpg"/>
</div>
</div>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('#slideshow0').nivoSlider();
});
--></script><div class="welcome">Welcome to Art Curious</div>
<table align="center" border="0" cellpadding="0" cellspacing="0" style="width: 900px;">
<tbody>
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" style="font-size: 15px; width: 900px;">
<tbody>
<tr>
<td>
<p><span style="font-size: 12px;">We are from Sialkot - Pakistan and our Company is ART’S HUB ,We are engaged in the manufacturing, exporting, a wide array of Gift Items. We are manufacturing &amp; exporting Art &amp; Craft gifts and promotional on Wood, Acrylic, Fabric, Leather, Embroidery for more than 15 Years.<br/>
						We work on any theme like Religious, Promotional, Personalized , Sports Gifts, Miniature, Models, Logo of Companies , Seasonal gifts etc;<br/>
						We are offering these Gift and promotional items in Wood, Acrylic, Fabric, Leather, Embroidery etc. Our aim is to be creative and unique with our gifts and promotional items,and ideas. We are confident that we can provide the best hand crafted item for you. The gifts can be handmade according to your choice.<br/>
						If you are looking for art &amp; craft items then look no further.<br/>
						We look forward to creating something meaningful for you.<br/>
						It is our immense pleasure to introduce our company products to you. We make sure every item meets your complete satisfaction..</span></p>
<p><span style="font-size: 12px;">Tel: + 92 52 4353024                   <br/>
						Cell: + 92 346 6699577                  <br/>
						Skype: rockwell.nathaniel        <br/>
						Email: </span>artshub33@gmail.com<br/>
<span style="font-size: 12px;">             rockwellnathaniel77@gmail.com</span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<div class="box">
<div class="box-heading">Latest</div>
<div class="box-content">
<div class="box-product">
<div>
<div class="image"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=314"><img alt="AH-777-WSH-02       Wooden Shield" src="http://art-curious.com/artcurious/image/cache/data/wooden-shield/AH-777-WSh-02-150x100.jpg"/></a></div>
<div class="name"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=314">AH-777-WSH-02       Wooden Shield</a></div>
<div class="cart"><input class="button" onclick="addToCart('314');" type="button" value="Add to Cart"/></div>
</div>
<div>
<div class="image"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=313"><img alt="AH-777-WSH-01       Wooden Trophy" src="http://art-curious.com/artcurious/image/cache/data/wooden-shield/AH-777-WSh-01-150x100.JPG"/></a></div>
<div class="name"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=313">AH-777-WSH-01       Wooden Trophy</a></div>
<div class="cart"><input class="button" onclick="addToCart('313');" type="button" value="Add to Cart"/></div>
</div>
<div>
<div class="image"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=312"><img alt="AH-777-WS-05    Wooden Wine Stand" src="http://art-curious.com/artcurious/image/cache/data/wooden-stand/AH-777-WS-05-150x100.JPG"/></a></div>
<div class="name"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=312">AH-777-WS-05    Wooden Wine Stand</a></div>
<div class="cart"><input class="button" onclick="addToCart('312');" type="button" value="Add to Cart"/></div>
</div>
<div>
<div class="image"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=311"><img alt="AH-777-WS-03     Wooden  display Stand for 6 to 10 inches" src="http://art-curious.com/artcurious/image/cache/data/wooden-stand/AH-777-WS-03-150x100.JPG"/></a></div>
<div class="name"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=311">AH-777-WS-03     Wooden  display Stand for 6 to 10 inches</a></div>
<div class="cart"><input class="button" onclick="addToCart('311');" type="button" value="Add to Cart"/></div>
</div>
<div>
<div class="image"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=310"><img alt="AH-777-WS-02   Wooden Cake Stand without painted" src="http://art-curious.com/artcurious/image/cache/data/wooden-stand/AH-777-WS-02-150x100.JPG"/></a></div>
<div class="name"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=310">AH-777-WS-02   Wooden Cake Stand without painted</a></div>
<div class="cart"><input class="button" onclick="addToCart('310');" type="button" value="Add to Cart"/></div>
</div>
<div>
<div class="image"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=309"><img alt="AH-777-WS-01   Wooden Cake Stand Painted" src="http://art-curious.com/artcurious/image/cache/data/wooden-stand/AH-777-WS-01-150x100.JPG"/></a></div>
<div class="name"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=309">AH-777-WS-01   Wooden Cake Stand Painted</a></div>
<div class="cart"><input class="button" onclick="addToCart('309');" type="button" value="Add to Cart"/></div>
</div>
<div>
<div class="image"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=307"><img alt="AH-777-WM-02  Wooden Model Cow" src="http://art-curious.com/artcurious/image/cache/data/wooden-model/AH-777-WM-02    Wooden    Model Cow-150x100.JPG"/></a></div>
<div class="name"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=307">AH-777-WM-02  Wooden Model Cow</a></div>
<div class="cart"><input class="button" onclick="addToCart('307');" type="button" value="Add to Cart"/></div>
</div>
<div>
<div class="image"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=306"><img alt="AH-777-WM-01  Wooden Model Bicycle " src="http://art-curious.com/artcurious/image/cache/data/wooden-model/AH-777-WM-01-150x100.JPG"/></a></div>
<div class="name"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=306">AH-777-WM-01  Wooden Model Bicycle </a></div>
<div class="cart"><input class="button" onclick="addToCart('306');" type="button" value="Add to Cart"/></div>
</div>
<div>
<div class="image"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=305"><img alt="AH-777-WL-01        Wooden Letter" src="http://art-curious.com/artcurious/image/cache/data/wooden-letters/AH-777-WL-01-150x100.JPG"/></a></div>
<div class="name"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=305">AH-777-WL-01        Wooden Letter</a></div>
<div class="cart"><input class="button" onclick="addToCart('305');" type="button" value="Add to Cart"/></div>
</div>
<div>
<div class="image"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=304"><img alt="AH-777-WJ-15     Wooden Pendant Without Paint" src="http://art-curious.com/artcurious/image/cache/data/wooden-jewelry/AH-777-WJ-15-150x100.JPG"/></a></div>
<div class="name"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=304">AH-777-WJ-15     Wooden Pendant Without Paint</a></div>
<div class="cart"><input class="button" onclick="addToCart('304');" type="button" value="Add to Cart"/></div>
</div>
<div>
<div class="image"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=303"><img alt="AH-777-WJ-14     Wooden Pendant and earrings " src="http://art-curious.com/artcurious/image/cache/data/wooden-jewelry/AH-777-WJ-14-150x100.JPG"/></a></div>
<div class="name"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=303">AH-777-WJ-14     Wooden Pendant and earrings </a></div>
<div class="cart"><input class="button" onclick="addToCart('303');" type="button" value="Add to Cart"/></div>
</div>
<div>
<div class="image"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=302"><img alt="AH-777-WJ-13     Wooden Pendant and earrings " src="http://art-curious.com/artcurious/image/cache/data/wooden-jewelry/AH-777-WJ-13     )-150x100.JPG"/></a></div>
<div class="name"><a href="http://art-curious.com/artcurious/index.php?route=product/product&amp;product_id=302">AH-777-WJ-13     Wooden Pendant and earrings </a></div>
<div class="cart"><input class="button" onclick="addToCart('302');" type="button" value="Add to Cart"/></div>
</div>
</div>
</div>
</div>
<h1 style="display: none;">art curious</h1>
<div id="carousel0">
<ul class="jcarousel-skin-opencart">
<li><a href=""><img alt="Jesus" src="http://art-curious.com/artcurious/image/cache/data/wooden-religious-pictures/AH-777-WRP-16-220x200.JPG" title="Jesus"/></a></li>
<li><a href=""><img alt="Cross" src="http://art-curious.com/artcurious/image/cache/data/wooden-engrave-painted-holy-scripture/AH-777-WEHSP-05-220x200.JPG" title="Cross"/></a></li>
<li><a href=""><img alt="Wooden art" src="http://art-curious.com/artcurious/image/cache/data/wooden-religious-pictures/AH-777-WRP-01-220x200.JPG" title="Wooden art"/></a></li>
<li><a href=""><img alt="Cross" src="http://art-curious.com/artcurious/image/cache/data/wooden-cross/AH-777-WC-07-220x200.JPG" title="Cross"/></a></li>
<li><a href=""><img alt="Art" src="http://art-curious.com/artcurious/image/cache/data/wooden-nativity-scene/AH-777-WNS-04-220x200.JPG" title="Art"/></a></li>
</ul>
</div>
<script type="text/javascript"><!--
$('#carousel0 ul').jcarousel({
	vertical: false,
	visible: 4,
	scroll: 2});
//--></script></div>
<div id="footer">
<div class="column">
<h3>Information</h3>
<ul>
<li><a href="http://art-curious.com/artcurious/index.php?route=information/information&amp;information_id=4">About Us</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=information/information&amp;information_id=6">Delivery Information</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=information/information&amp;information_id=3">Privacy Policy</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=information/information&amp;information_id=5">Terms &amp; Conditions</a></li>
</ul>
</div>
<div class="column">
<h3>Customer Service</h3>
<ul>
<li><a href="http://art-curious.com/artcurious/index.php?route=information/contact">Contact Us</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=account/return/insert">Returns</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=information/sitemap">Site Map</a></li>
</ul>
</div>
<div class="column">
<h3>Extras</h3>
<ul>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/manufacturer">Brands</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=account/voucher">Gift Vouchers</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=affiliate/account">Affiliates</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=product/special">Specials</a></li>
</ul>
</div>
<div class="column">
<h3>My Account</h3>
<ul>
<li><a href="http://art-curious.com/artcurious/index.php?route=account/account">My Account</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=account/order">Order History</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=account/wishlist">Wish List</a></li>
<li><a href="http://art-curious.com/artcurious/index.php?route=account/newsletter">Newsletter</a></li>
</ul>
<div id="google_translate_element"></div>
<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}
</script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>
</div>
<script src="//counter.websiteout.net/js/22/0/134044/0" type="text/javascript">
</script>
</div>
<div id="powered"><h3>Copyrights © 2018 Art Curious. All rights reserved.           <img alt="Skype" src="/img/emaxlogo1.jpg"/></h3>
</div>
</div></body></html>