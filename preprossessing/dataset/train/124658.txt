<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "40603",
      cRay: "610dcad55bff329d",
      cHash: "3283e1862765b77",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYW5pbWVseXJpY3MuY29tL2RvdWppbi92b2NhbG9pZC9zb3JlZ2Fib2t1cmFub2p1c3RpY2UuaHRt",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "71fheepZF2/r8ubFk283xh2ub8VnuvPNNSk68ol154cPcwDqq/Y2/lg3qoEwJntxcls3jGrJRDdOqq4YNN3ZZEiNU6emWgiq+Ijm95186zLicVTAYJQL0wZAv4rHIt++t03OLVMEK3K6GrhF51+ua+VRO2vvMd9Fkeap4ApjeISn8+ljuSiOzugmaIQxSzIk4Mglj3LqwFfWZztDUZMAa0ogzTzh9dM5ESxid6sMbujFc7xJGGVYB/gsppG1IAXBQqFMhBfnLfyB9hbilDSvfmypco5SkzzyP6L5sDuH6a/m10TmKHoidbUKoUgyGoJxGZfDpL+X9p8mD00AHGzR7Yh1ZIPGYeM+e+9PrPbwQiu3auGqMcU5If9a8C2rkiPgpNL0YZccqrZAlhju/q1iPOxQOgw2ozz8GhH7aD9N+rgTg591Xzwq1ohQvCjz1KXJHKrr0TQC8kq4+geGbe+R0vCMWPTup2E3RzLF11eWFD6XnK67aT7ErNvBh/g+u8ue+PzQTPxyYOpZmbKqqLw69MXNJPR1ut9bN6HsXZtqNRSO0ukDqgGbvreHNsjlb3bMkOwTnflB+CqQ/u6+a+ERVcUjs7kTm8ZVR1OsRMGT/FaZJwyEL8kL7jEEyLlFYszeVF7fpJvVqPUrLwgkzTlHmNsQwK0GEqsSiPzXtr/RwsMgDbpHmgCw4P1HZP0Mz+yfZYxk80E+MGxnj0lnmuxfFy5NA9YuqLvXr2JAdcakEPyg+TZtvuPTLzR0Asjjug/S",
        t: "MTYxMDUyNzAxNS4yNTkwMDA=",
        m: "M6K99U4gD3ITGg1wEQzSduF5WCaJ057HuWwomzISFwg=",
        i1: "pw4pP7tIAGLa5yRcbIHgrw==",
        i2: "CVzR38tsDa+etXGvVVMmNQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "DDyL0IQNxmRdtTss4zAmYXiwHb+a6njG4Si8OlXEFGo=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.animelyrics.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/doujin/vocaloid/soregabokuranojustice.htm?__cf_chl_captcha_tk__=eff9a4122bc6e57615b88ceb6ec77f15c9ac0116-1610527015-0-AW7Sb0W60_yTCBpBI91um9V8-tZx-DTgHLvgwacaa3CmQ-2Qbbuv1y--dVcZYQaepq7Uzhi7NUVoyRhUDT8TMfDbjMeSaxekLW5CUC806TJ2T0RBrmoWS3jbyAJCC0EqJg54JUXXeKkNx3HspGNKNEKLvJvZAGWF3WV_2ThkyI_r6EwBfuzB9eff2AjyOZDvqHdJMo0Fci2DnSkdzLRy9v40lvDTCQkGZ-kKjOp3ab7s2NGTTcWub8XbTw1-7LUB7EXLYc_stUzRowcZf5e6OlIAaKrIqyO1YyRpo0hNs2b8ZfUTqzidxZXWqGzfMkfsC4IWF_SiW5Lz7jigcGJOjtQjLHS7KPKcMlm_R8O_QUsowstTwKPCpctvohp42AB_bMyrsdF3EOjcdxveAUCldl3kUFzx01YAN5dutLYkcnT7jdT1jbzNDMfqtcwyBr1izLHb6kQKR1zDG7NrzwEoTspbEMbNUkXC7GsbadpwpnsSyOmFknlB63DTqkqWRrRsbmmzG83c1G3VD4jpRGLUicKwTJ_rE-b64tjAgKZIoZpdy1g7r9dKfASuvAF0ZDb5yA885vp8p61Z9Qe7jcYUHao" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="8ec5822ffc4fd8763076636c747da1591fb67864-1610527015-0-AVkz4OzCq0FF3RNO782S1LZ9eXEI+HHid79TBw650Ih/hzHy3HN3//n8CuAYCVsKkFZxoHdomjg/3yGeLAIfvri/gPPFe7awkzirT1u+CEUoRQKzQjG4HpeNSkRBQAHNkOalgKh0xiMcTp3zS6UJaTzkW9Ok0bkU0DV0CRhLItRFmFgiapz5DfZd5KPk2hK9/Z9+NDDbO+cuuqTiQMoxQTBFMZHlds0N2iEkM0+kgCh9+OittuWgqXmj3e8vw/iW34DQTxXhXSULaXbi2JPRGXds30u9TpV9O4S0DAZfuV1SRBT/Nox8Z4XTFjIPqCeALXmbqsnmuBXlymDC0vRQc89qIICW/DHY0d+lbV3/axRQah53NWywxkcwThJE0+l6V2B7tR47uTzY2leqL1hXYMn39uDkQ0lwTMmAA2wByS/Kgv66CKdRhzYHSz1mdwnqJTXbGo41GbwLHD1mOBxh5jl1n4DIuIgXKR4kPLBp1rWhm+mow3oSvLQJo8VOttLvC7p/xAJ1RoouYR0e9GdBrSZkd+PbMIysrksDhJ8cseEAWbcWuX3RUwWFCJDS1l26RTCEaC8iDA5KlgOGi/k54eXauJylZ/LdME4Jt0MLK7uy0DkrceS9O9HjCSMrChQKqfJpOHsqAtq6xfzqgkRS4oCu8smrHrq2yVyvUUWqPiHMxeLvCVK/CxI42ipBCYaIAzfwWTVigM+BwF4GZ5whZ2zT/TYxhM38WqhQNpyBq9auk9x2H13AtGBP0Gi3l0TnW4hI+nEJWWU15fHaOZ8jSZl7dEzfh9HVrzV619Vb2x/UeUjpVCRMeKw092NQTw96NGrZRv2cHdiK3TaCrmkH79euwxzs22SsEH+/Sa2Rf9wtmGX2Z3bvi/uix6w0MdnpRaWby8blNgPyQpCakS91wHrFJecFKi3ad+oMqIvU9X3GZMaYNIMEkn8ETaeN+c7fElMcEU2ngxNPtqyxYgCfXojdidb0pdOjfjhYO98NPj0hYlv2CyHPEgY51T4lAOwd1xEr2CK0FmeennDHvXlGfLkJZJRrjYhA3SLllv+LINN+qqSachZ/6Ibxjt//Ac81MPLlckxP0a5lphHkV9iO5re1BGvqVqBmktixGfCzIBksC8prKHlV1t+G/R4i+T+2cnPzEcKfEaq0WPPQhXiSLZIUJAyMPlQnomEstXCKEzWbrlvZXqX7bdsscrHrdINSZio3pqrxLpV7ejad9ykZKkPNkdekfc/lQGwO2oN90x1walZA7yXFHXOnSNn9O/kwCRD35h+57hFd8EKDorKqXM3eim5OPyBGHsKf2Aub92fm"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="49f1cae1f1f06e8b8ca01dc7b955de93"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610dcad55bff329d')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610dcad55bff329d</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 210.75.253.169</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
