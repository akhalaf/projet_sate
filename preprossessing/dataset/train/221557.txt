<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="TC1B39yq0CEyuFa7IyT5ykZRr514yjbvSow3XjqRWRg" name="google-site-verification"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.bio-hutanea.com/xmlrpc.php" rel="pingback"/>
<title>Page not found - Bio Hutanea</title>
<!-- This site is optimized with the Yoast SEO plugin v10.0.1 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex,follow" name="robots"/>
<meta content="en_US" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Page not found - Bio Hutanea" property="og:title"/>
<meta content="Bio Hutanea" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Page not found - Bio Hutanea" name="twitter:title"/>
<!-- / Yoast SEO plugin. -->
<link href="//s0.wp.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.bio-hutanea.com/feed/" rel="alternate" title="Bio Hutanea » Feed" type="application/rss+xml"/>
<link href="https://www.bio-hutanea.com/comments/feed/" rel="alternate" title="Bio Hutanea » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.bio-hutanea.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.0.11"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.bio-hutanea.com/wp-includes/css/dist/block-library/style.min.css?ver=5.0.11" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Roboto%3A300%2C400%2C600%2C700%2C800%2C900%7CRoboto+Condensed%3A400%2C300%2C700%2C300italic%2C400italic%2C700italic%7COpen+Sans%3A400%2C300%2C300italic%2C600%2C400italic%2C600italic%2C700%2C800%2C700italic%2C800italic%7CLato%3A400%2C400italic%2C700%2C700italic%2C900%2C900italic%7CMontserrat%3A400%2C700&amp;ver=5.0.11" id="spiritedlite-font-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bio-hutanea.com/wp-content/themes/spirited-lite/style.css?ver=5.0.11" id="spiritedlite-basic-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bio-hutanea.com/wp-content/themes/spirited-lite/editor-style.css?ver=5.0.11" id="spiritedlite-editor-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bio-hutanea.com/wp-content/themes/spirited-lite/css/nivo-slider.css?ver=5.0.11" id="spiritedlite-nivoslider-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bio-hutanea.com/wp-content/themes/spirited-lite/css/responsive.css?ver=5.0.11" id="spiritedlite-main-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bio-hutanea.com/wp-content/themes/spirited-lite/css/style_base.css?ver=5.0.11" id="spiritedlite-base-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bio-hutanea.com/wp-content/themes/spirited-lite/css/font-awesome.css?ver=5.0.11" id="spiritedlite-font-awesome-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bio-hutanea.com/wp-content/themes/spirited-lite/css/animation.css?ver=5.0.11" id="spiritedlite-animation-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bio-hutanea.com/wp-content/plugins/advanced-wp-columns/assets/css/awp-columns.css?ver=5.0.11" id="dry_awp_theme_style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="dry_awp_theme_style-inline-css" type="text/css">
@media screen and (max-width: 1024px) {	.csColumn {		clear: both !important;		float: none !important;		text-align: center !important;		margin-left:  10% !important;		margin-right: 10% !important;		width: 80% !important;	}	.csColumnGap {		display: none !important;	}}
</style>
<link href="https://www.bio-hutanea.com/wp-content/plugins/jetpack/css/jetpack.css?ver=7.1.2" id="jetpack_css-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.bio-hutanea.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.bio-hutanea.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.bio-hutanea.com/wp-content/themes/spirited-lite/js/jquery.nivo.slider.js?ver=5.0.11" type="text/javascript"></script>
<script src="https://www.bio-hutanea.com/wp-content/themes/spirited-lite/js/custom.js?ver=5.0.11" type="text/javascript"></script>
<link href="https://www.bio-hutanea.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.bio-hutanea.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.bio-hutanea.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.0.11" name="generator"/>
<link href="//v0.wordpress.com" rel="dns-prefetch"/>
<style type="text/css">img#wpstats{display:none}</style> <style type="text/css">
					
					a, .header .header-inner .nav ul li a:hover, 
					.signin_wrap a:hover,				
					.services-wrap .one_fourth:hover h3,
					.blog_lists h2 a:hover,
					#sidebar ul li a:hover,
					.recent-post h6:hover,
					.MoreLink:hover,
					.cols-3 ul li a:hover,.wedobox:hover .btn-small,.wedobox:hover .boxtitle,.slide_more
					{ color:#262626;}
					
					.pagination ul li .current, .pagination ul li a:hover, 
					#commentform input#submit:hover
					{ background-color:#262626;}
					
					.MoreLink:hover
					{ border-color:#262626;}
					
			</style>
<style type="text/css">
</style>
<style id="wp-custom-css" type="text/css">
				.entry-title { padding: 20px 0 15px 0;}
.boxdescription { display: none;}
.wedobox {min-height: 220px;}
.boxicon {width: 95%;}			</style>
</head>
<body class="error404">
<div class="header">
<div class="signin_wrap">
<div class="topfirstbar">
<div class="topbarleft">
<div class="icon-left-top">
<a href="mailto:cs@bio-hutanea.com"><i class="fa fa-envelope fa-1x"></i>cs@bio-hutanea.com</a>
</div>
<div class="icon-left-top bgnone">
<a><i class="fa fa-phone fa-1x"></i>+62-81398972500                    </a></div>
</div>
<div class="topbarright">
<div class="top-phonearea">
<div class="social-top">
<div class="social-icons">
<a class="fa fa-facebook fa-1x" href="#facebook" target="_blank" title="facebook"></a>
<a class="fa fa-twitter fa-1x" href="https://twitter.com/biohutanea" target="_blank" title="twitter"></a>
<a class="fa fa-google-plus fa-1x" href="https://plus.google.com/u/0/115398878091190711771" target="_blank" title="google-plus"></a>
<a class="fa fa-linkedin fa-1x" href="#linkedin" target="_blank" title="linkedin"></a>
</div>
</div>
</div>
</div>
<div class="clear"></div>
</div>
</div>
<!--end signin_wrap-->
<div class="header-inner">
<div class="logo">
<a href="https://www.bio-hutanea.com/">
<h1>
        Bio Hutanea      </h1>
<span class="tagline">
      Biotechnology Company      </span> </a> </div>
<!-- logo -->
<div class="toggle"> <a class="toggleMenu" href="#">
      Menu      </a> </div>
<!-- toggle -->
<div class="nav">
<div class="menu-bio-container"><ul class="menu" id="menu-bio"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-52" id="menu-item-52"><a href="https://www.bio-hutanea.com/">HOME</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53" id="menu-item-53"><a href="https://www.bio-hutanea.com/who-we-are/">WHO WE ARE</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-403" id="menu-item-403"><a href="https://www.bio-hutanea.com/what-we-do/">WHAT WE DO</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-309" id="menu-item-309"><a href="#">PRODUCT</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-907" id="menu-item-907"><a href="https://www.bio-hutanea.com/pengadaan-alat-laboratorium-persemaian-modern/">EQUIPMENT PRODUCT</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-55" id="menu-item-55"><a href="https://www.bio-hutanea.com/seedlings-product/">SEEDLINGS PRODUCT</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-780" id="menu-item-780"><a href="https://www.bio-hutanea.com/produksi-bibit-jati-bio-solomon/">Jati Solomon</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-908" id="menu-item-908"><a href="https://www.bio-hutanea.com/somatic-embryogenesis-kelapa-sawit/">Bibit Klon Kelapa Sawit</a></li>
</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-783" id="menu-item-783"><a href="https://www.bio-hutanea.com/pembangunan-laboratorium-persemaian-modern/">Design &amp; Build</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-688" id="menu-item-688"><a href="https://www.bio-hutanea.com/forestry-plantation-consulting-services/">Consulting</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-970" id="menu-item-970"><a href="https://www.bio-hutanea.com/contact-us/">CONTACT US</a></li>
</ul></div> </div>
<!-- nav -->
<div class="clear"></div>
</div> <!-- header-inner -->
</div><!-- header -->
<div class="container">
<div class="page_content">
<section class="site-main" id="sitemain">
<header class="page-header">
<h1 class="entry-title">404 Not Found</h1>
</header><!-- .page-header -->
<div class="page-content">
<p class="text-404">Looks like you have taken a wrong turn..... Don't worry... it happens to the best of us.</p>
</div><!-- .page-content -->
</section>
<div class="clear"></div>
</div>
</div>
<div id="footer-wrapper">
<div class="container">
<div class="cols-4 widget-column-1">
<h5>Bio Hutanea</h5>
<div class="clear"></div>
<div class="footeradrs"><i class="fa fa-map-marker fa-2x"></i><span>Head Office, Lab &amp; Mini Nursery Kavling Melati No 11 Jl. Gas Alam Raya Cimanggis Depok Jawa Barat. 16453</span></div>
<div class="footeradrs"><i class="fa fa-phone fa-2x"></i><span>+62-81398972500</span></div>
</div>
<div class="cols-4 widget-column-2">
<h5>Recent Posts</h5>
<div class="recent-post">
<a href="https://www.bio-hutanea.com/penyesuaian-harga/"><h6>Penyesuaian Harga</h6></a>
</div>
<div class="recent-post">
<a href="https://www.bio-hutanea.com/our-consulting-project/"><h6>OUR CONSULTING PROJECT</h6></a>
</div>
</div>
<div class="cols-4 widget-column-3">
<h5>Get Some Social</h5>
<div class="somesocial">
<a class="fa fa-facebook fa-2x" href="#facebook" target="_blank" title="facebook"></a>
<a class="fa fa-twitter fa-2x" href="https://twitter.com/biohutanea" target="_blank" title="twitter"></a>
<a class="fa fa-google-plus fa-2x" href="https://plus.google.com/u/0/115398878091190711771" target="_blank" title="google-plus"></a>
<a class="fa fa-linkedin fa-2x" href="#linkedin" target="_blank" title="linkedin"></a>
</div>
</div><!--end .widget-column-4-->
<div class="clear"></div>
</div><!--end .container-->
<div class="copyright-wrapper">
<div class="container">
<div class="copyright-txt">© 2019 Bio Hutanea. All Rights Reserved</div>
<div class="design-by"><a href="http://www.sktthemes.net/product-category/free-wordpress-themes/" rel="nofollow" target="_blank">Spirited Lite Theme</a></div>
</div>
<div class="clear"></div>
</div>
</div>
<script src="https://s0.wp.com/wp-content/js/devicepx-jetpack.js?ver=202102" type="text/javascript"></script>
<script src="https://www.bio-hutanea.com/wp-includes/js/wp-embed.min.js?ver=5.0.11" type="text/javascript"></script>
<script async="async" defer="defer" src="https://stats.wp.com/e-202102.js" type="text/javascript"></script>
<script type="text/javascript">
	_stq = window._stq || [];
	_stq.push([ 'view', {v:'ext',j:'1:7.1.2',blog:'157775630',post:'0',tz:'8',srv:'www.bio-hutanea.com'} ]);
	_stq.push([ 'clickTrackerInit', '157775630', '0' ]);
</script>
</body>
</html>