<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<title>WordPress Hosting | Website Maintenance Services | ClickHOST | Atlanta Web Hosting</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://clickhost.com/wp-content/plugins/jetpack/modules/likes/style.css?ver=2.9.3" id="jetpack_likes-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://clickhost.com/wp-content/plugins/jetpack/modules/subscriptions/subscriptions.css?ver=3.9" id="jetpack-subscriptions-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&amp;subset=latin%2Clatin-ext&amp;ver=3.9" id="open-sans-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://clickhost.com/wp-includes/css/dashicons.min.css?ver=3.9" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://clickhost.com/wp-content/themes/clickhost-2014/style.css?ver=1397836666" id="child-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://clickhost.com/wp-content/plugins/be-icon-shortcode/be-icon-shortcode.css?ver=3.9" id="be-icon-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://clickhost.com/wp-content/plugins/chameleon-buttons/chameleon-buttons.css?ver=3.9" id="chameleon-buttons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://clickhost.com/wp-content/plugins/jetpack/modules/widgets/widgets.css?ver=20121003" id="jetpack-widgets-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Lato%3A400%2C700&amp;ver=3.9" id="be-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://clickhost.com/wp-content/plugins/simple-social-icons/css/style.css?ver=1.0.5" id="simple-social-icons-font-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://clickhost.com/wp-content/plugins/jetpack/modules/sharedaddy/sharing.css?ver=2.9.3" id="sharedaddy-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://clickhost.com/wp-includes/js/jquery/jquery.js?ver=1.11.0" type="text/javascript"></script>
<script src="https://clickhost.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1" type="text/javascript"></script>
<script src="https://clickhost.com/wp-content/plugins/powies-whois//pwhois.js?ver=3.9" type="text/javascript"></script>
<script src="https://clickhost.com/wp-content/plugins/jetpack/_inc/postmessage.js?ver=2.9.3" type="text/javascript"></script>
<script src="https://clickhost.com/wp-content/plugins/jetpack/_inc/jquery.inview.js?ver=2.9.3" type="text/javascript"></script>
<script src="https://clickhost.com/wp-content/plugins/jetpack/_inc/jquery.jetpack-resize.js?ver=2.9.3" type="text/javascript"></script>
<link href="https://clickhost.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://clickhost.com/wp-content/themes/clickhost-2014/images/favicon.png" rel="Shortcut Icon" type="image/x-icon"/>
<link href="https://www.clickhost.com/feed/" rel="alternate" title="ClickHOST RSS Feed" type="application/rss+xml"/>
<link href="https://www.clickhost.com/feed/atom/" rel="alternate" title="ClickHOST Atom Feed" type="application/atom+xml"/>
<link href="https://www.clickhost.com/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<style media="screen" type="text/css"> .simple-social-icons ul li a, .simple-social-icons ul li a:hover { background-color: #fe7623 !important; border-radius: 100px; color: #ffffff !important; font-size: 18px; padding: 9px; }  .simple-social-icons ul li a:hover { background-color: #1a1a1a !important; color: #ffffff !important; }</style><style media="print" type="text/css">#wpadminbar { display:none; }</style>
<!-- Jetpack Open Graph Tags -->
<meta content="article" property="og:type"/>
<meta content="Simple. Honest. Safe." property="og:title"/>
<meta content="https://clickhost.com/about/" property="og:url"/>
<meta content="Where we came from makes us what we are today. It all started in the year 2000 ClickHOST was founded as a family-owned business in the historic town of Stratford, Canada. From the beginning, the foâ¦" property="og:description"/>
<meta content="2011-06-14T15:51:21+00:00" property="article:published_time"/>
<meta content="2014-04-16T00:32:48+00:00" property="article:modified_time"/>
<meta content="https://clickhost.com/author/clickhost/" property="article:author"/>
<meta content="ClickHOST" property="og:site_name"/>
<meta content="https://wordpress.com/i/blank.jpg" property="og:image"/>
<meta content="@jetpack" name="twitter:site"/>
<meta content="summary" name="twitter:card"/>
</head>
<body class="page page-id-34 page-template-default logged-in admin-bar no-customize-support full-width-content" itemscope="itemscope" itemtype="https://schema.org/WebPage"><div class="site-container"><header class="site-header" itemscope="itemscope" itemtype="https://schema.org/WPHeader" role="banner"><div class="wrap"><div class="title-area"><h1 class="site-title" itemprop="headline"><a href="https://clickhost.com/" title="ClickHOST">ClickHOST</a></h1></div><nav class="nav-header" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement" role="navigation"><ul class="menu genesis-nav-menu menu-header" id="menu-primary"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3299" id="menu-item-3299"><a href="https://www.clickhost.com/web-hosting/hosting-packages/">Plans</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3311" id="menu-item-3311"><a href="https://www.clickhost.com/web-hosting/hosting-packages/">Compare Plans</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3312" id="menu-item-3312"><a href="https://www.clickhost.com/web-hosting/">Free Features</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3221" id="menu-item-3221"><a href="https://www.clickhost.com/domain-search/">Domains</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3314" id="menu-item-3314"><a href="https://www.clickhost.com/domain-search/">Domain Search</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3222" id="menu-item-3222"><a href="https://www.clickhost.com/domain-search/domain-faq/">Domain Name FAQ</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3156" id="menu-item-3156"><a href="https://www.clickhost.com/order/cart.php?a=add&amp;domain=register">Register a Domain</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3157" id="menu-item-3157"><a href="https://www.clickhost.com/order/cart.php?a=add&amp;domain=transfer">Transfer a Domain</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3011" id="menu-item-3011"><a href="https://www.clickhost.com/services/">Services</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3154" id="menu-item-3154"><a href="https://www.clickhost.com/mobile-websites/">Mobile Websites</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3158" id="menu-item-3158"><a href="https://www.clickhost.com/order/cart.php?gid=5">SSL Certificates</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3147" id="menu-item-3147"><a href="https://www.clickhost.com/about/">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3140" id="menu-item-3140"><a href="https://www.clickhost.com/help/">Help</a></li>
<li class="signup menu-item menu-item-type-custom menu-item-object-custom menu-item-2992" id="menu-item-2992"><a href="https://www.clickhost.com/order/cart.php">Sign Up</a></li>
<li class="login menu-item menu-item-type-custom menu-item-object-custom menu-item-2993" id="menu-item-2993"><a href="https://www.clickhost.com/order/clientarea.php">Login</a></li>
</ul></nav><div class="menu-primary-container"><div class="mobile-menu"><form><select onchange="if (this.value) window.location.href=this.value"><option value="">Go To…</option><option value="https://clickhost.com/web-hosting/hosting-packages/">Plans</option>
<option value="https://www.clickhost.com/domain-search/">Domains</option>
<option selected="selected" value="https://www.clickhost.com/services/">Services</option>
<option value="https://www.clickhost.com/order/cart.php">Sign Up</option>
<option value="https://www.clickhost.com/order/clientarea.php">Login</option>
</select></form></div></div></div></header><div class="section-header"><div class="wrap"><p>Welcome to ClickHOST Web hosting!<span class="right">Simple. Honest. Safe.</span></p></div></div><div class="site-inner"><div class="wrap"><div class="content-sidebar-wrap"><main class="content" itemprop="mainContentOfPage" role="main"><article class="post-34 page type-page status-publish entry" itemscope="itemscope" itemtype="https://schema.org/CreativeWork"><header class="entry-header"></header>
<div id="category-name">
<div id="category-inner">
</div> <!-- end #category-inner -->
</div> <!-- end #category-name -->
<div class="clearfix fullwidth" id="content">
<div id="left-area">
<div class="entry post clearfix">
<h1 align="center">Congratulations!</h1>
<p align="center">Your web hosting account is now active.<br/>
This is a temprorary page created for your account.<br/>
This page is named index.html. Do not forget to delete this file (index.html) later<br/>
after you uploaded/published your own website files.

<br/>To get started you may visit our How-To guides and Knowledge base by visiting:<br/>
<a href="https://www.clickhost.com/order/knowledgebase.php"> ClickHOST Knowledge Base and How-To Guides.</a>
<br/>
<br/>
If you need any assistance or have questions feel free to submit a ticket at <a href="https://www.clickhost.com/order/submitticket.php" target="_blank">ClickHOST support</a> or <br/>
send an email to <a href="mailto:support@clickhost.com" target="_blank">support@clickhost.com</a> for immediate assistance.
</p>
<br/>
<br/>
<h4 align="center">Order ClickHOST products and services</h4>
<p align="center">If you need additional ClickHOST products and services you may visit our order page at<br/>
<a href="https://www.clickhost.com/order/cart.php">ClickHOST Secure Ordering System</a>.</p>
</div> <!-- end .entry -->
</div> <!-- end #left-area -->
</div> <!-- end #content -->
</article></main></div></div></div>
<div class="footer-widgets"><div class="wrap"><div class="footer-widgets-1 widget-area"> <section class="widget widget_recent_entries" id="recent-posts-2"><div class="widget-wrap"> <h4 class="widget-title widgettitle">From the Blog</h4>
<ul>
<li>
<a href="https://www.clickhost.com/top-5-wordpress-beginner-tips/">Top 5 WordPress beginner tips</a>
</li>
<li>
<a href="https://www.clickhost.com/wordcamp-atlanta-2014/">Itâs WordCamp Atlanta 2014!</a>
</li>
<li>
<a href="https://www.clickhost.com/how-to-order-a-hosting-account/">How to order a ClickHOST hosting account</a>
</li>
<li>
<a href="https://www.clickhost.com/hosting-matters/">8 Reasons Hosting Matters More in 2014â¦</a>
</li>
<li>
<a href="https://www.clickhost.com/atlanta-braves-and-wordpress/">Atlanta Braves and WordPress â what a combination!</a>
</li>
</ul>
</div></section>
<section class="widget widget_text" id="text-51"><div class="widget-wrap"> <div class="textwidget"><br/>
<a href="https://monitor6.sucuri.net/verify.php?r=10752285ecb3fca7967a680c66ee1727587ea3de59"><img src="https://sucuri.net/sucuri-verified-badge1-medium.png"/></a></div>
</div></section>
</div><div class="footer-widgets-2 widget-area"><section class="widget widget_nav_menu" id="nav_menu-5"><div class="widget-wrap"><h4 class="widget-title widgettitle">Quick Links</h4>
<div class="menu-quick-links-container"><ul class="menu" id="menu-quick-links"><li class="one-half first menu-item menu-item-type-post_type menu-item-object-page menu-item-3300" id="menu-item-3300"><a href="https://www.clickhost.com/web-hosting/">Free Hosting Features</a></li>
<li class="one-half menu-item menu-item-type-post_type menu-item-object-page menu-item-81" id="menu-item-81"><a href="https://www.clickhost.com/blog/">Web Hosting Blog</a></li>
<li class="one-half first menu-item menu-item-type-post_type menu-item-object-page menu-item-72" id="menu-item-72"><a href="https://www.clickhost.com/web-hosting/affiliate-program/">Affiliate Program</a></li>
<li class="one-half menu-item menu-item-type-post_type menu-item-object-page menu-item-74" id="menu-item-74"><a href="https://www.clickhost.com/help/">Support</a></li>
<li class="one-half first menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-3087 current_page_item menu-item-3097" id="menu-item-3097"><a href="https://www.clickhost.com/whois-domain-lookup/">WhoIS Lookup</a></li>
<li class="one-half menu-item menu-item-type-custom menu-item-object-custom menu-item-1273" id="menu-item-1273"><a href="https://www.clickhost.com/whatismyip/">What is my IP address?</a></li>
<li class="one-half first menu-item menu-item-type-post_type menu-item-object-page menu-item-77" id="menu-item-77"><a href="https://www.clickhost.com/site-map/">Site Map</a></li>
<li class="one-half menu-item menu-item-type-custom menu-item-object-custom menu-item-3196" id="menu-item-3196"><a href="https://www.clickhost.com/web-hosting/wordpress-hosting/">WordPress friendly hosting</a></li>
</ul></div></div></section>
</div><div class="footer-widgets-3 widget-area"><section class="widget widget_black_studio_tinymce" id="black-studio-tinymce-2"><div class="widget-wrap"><h4 class="widget-title widgettitle">Connect with us</h4>
<div class="textwidget"><p>Email support:<br/> 
24 hours a day, 7 days a week!<br/>
<a href="mailto:support@clickhost.com">support@clickhost.com</a></p>
<p>Billing/Domain name support:<br/>
9am-5pm EDT,  Mondayï¼Friday</p>
<p>Phone support:<br/>
9am-5pm EDT,  Mondayï¼Friday<br/>
404.220.8110 x 1</p></div>
</div></section>
<section class="widget simple-social-icons" id="simple-social-icons-2"><div class="widget-wrap"><ul class="alignleft"><li class="social-facebook"><a href="https://www.facebook.com/ClickHOST">î </a></li><li class="social-gplus"><a href="https://plus.google.com/+Clickhost">î </a></li><li class="social-rss"><a href="https://www.clickhost.com/feed/">î </a></li><li class="social-twitter"><a href="https://twitter.com/clickhost">î </a></li><li class="social-youtube"><a href="https://www.youtube.com/clickhost">î </a></li></ul></div></section>
</div></div></div>
<footer class="site-footer" itemscope="itemscope" itemtype="https://schema.org/WPFooter" role="contentinfo"><div class="wrap"><p>COPYRIGHT © 2000- 2014 ClickHOST | <a href="/terms-of-service/">Terms of Service</a> | <a href="/privacy/">Privacy Policy</a> <a class="backtotop" href="#">Back to the Top</a></p></div></footer></div> <div style="display:none">
</div>
<link href="https://s0.wp.com/wp-content/mu-plugins/notes/admin-bar-v2.css?ver=2.9.3-201417" id="wpcom-notes-admin-bar-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://s0.wp.com/i/noticons/noticons.css?ver=2.9.3-201417" id="noticons-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://clickhost.com/wp-includes/js/admin-bar.min.js?ver=3.9" type="text/javascript"></script>
<script src="https://s0.wp.com/wp-content/js/devicepx-jetpack.js?ver=201417" type="text/javascript"></script>
<script src="https://s.gravatar.com/js/gprofiles.js?ver=2014Apraa" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var WPGroHo = {"my_hash":"ae510affa31e5b946623bda4ff969b67"};
/* ]]> */
</script>
<script src="https://clickhost.com/wp-content/plugins/jetpack/modules/wpgroho.js?ver=3.9" type="text/javascript"></script>
<script src="https://clickhost.com/wp-includes/js/comment-reply.min.js?ver=3.9" type="text/javascript"></script>
<script src="https://clickhost.com/wp-content/themes/clickhost-2014/js/jquery.fitvids.js?ver=1.1" type="text/javascript"></script>
<script src="https://clickhost.com/wp-content/themes/clickhost-2014/js/global.js?ver=1.0" type="text/javascript"></script>
<script src="https://clickhost.com/wp-includes/js/underscore.min.js?ver=1.6.0" type="text/javascript"></script>
<script src="https://clickhost.com/wp-includes/js/backbone.min.js?ver=1.1.2" type="text/javascript"></script>
<script src="https://s1.wp.com/wp-content/js/mustache.js?ver=2.9.3-201417" type="text/javascript"></script>
<script src="https://clickhost.com/wp-content/plugins/jetpack/_inc/spin.js?ver=1.3" type="text/javascript"></script>
<script src="https://clickhost.com/wp-content/plugins/jetpack/_inc/jquery.spin.js?ver=1.3" type="text/javascript"></script>
<script src="https://s1.wp.com/wp-content/mu-plugins/notes/notes-common-v2.js?ver=2.9.3-201417" type="text/javascript"></script>
<script src="https://s0.wp.com/wp-content/mu-plugins/notes/admin-bar-v2.js?ver=2.9.3-201417" type="text/javascript"></script>
</body>
</html>
<!-- begin olark code -->