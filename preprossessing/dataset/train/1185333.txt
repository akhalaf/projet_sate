<!DOCTYPE html>
<html ng-app="YouTrackLogin" ng-strict-di="">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="noindex" name="robots"/>
<base href="/"/>
</head>
<body>
<div message="Loading YouTrack..." yt-loader="screen"></div>
<script src="static/vendor.8ca7b035bd22846cdbfe.js"></script>
<script src="static/oauth.bd137ec3cb18c7f490e5.js"></script>
</body>
</html>
