<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" dir="ltr" lang="sk-SK">
<![endif]--><!--[if IE 7]>
<html id="ie7" dir="ltr" lang="sk-SK">
<![endif]--><!--[if IE 8]>
<html id="ie8" dir="ltr" lang="sk-SK">
<![endif]--><!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!--><html dir="ltr" lang="sk-SK">
<!--<![endif]-->
<head>
<title>Didaktik Electronic | </title>
<meta content="IdeaCorp s.r.o." name="author"/>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="" name="keywords"/>
<meta content="" name="description"/>
<meta content="index, follow" name="robots"/>
<link href="" rel="shortcut icon" type="image/x-icon"/>
<link href="/skin/frontend/images/favicon.png" rel="shortcut icon" type="image/png"/>
<script src="/skin/frontend/js/jquery-1.10.2.js"></script>
<script src="/skin/frontend/js/jquery.barrating.js"></script>
<script src="/skin/frontend/js/jquery.validate.js"></script>
<link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,300i,400,400i,600,600i&amp;subset=latin-ext" rel="stylesheet"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-135626601-1"></script>
<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-135626601-1');
		</script>
</head>
<body>
<style>
			.st0{display:none;}
			.st1{stroke:#06659E;fill:none;}
			.st2{fill-rule:evenodd;clip-rule:evenodd;fill:none;}

			svg	.vysek-fill{fill:#06659E;}
			svg .vysek-fill {
			  opacity:0;
			  transform-origin: 50% 50%;
			  transform:rotate(-90deg);
			}
			svg .vysek-fill {
			  animation: vysekROTATEanim 1s cubic-bezier(.19,.74,.86,.37) forwards;
			  animation-delay: 0.7s;
			}
			@keyframes vysekROTATEanim {
			  0% {transform:rotate(-90deg);opacity:0;}
			  100% {transform:rotate(0deg);opacity:1;}
			}

			.st1 {
			  stroke-dasharray: 990;
			  stroke-dashoffset: 990;
			  animation: lt00dash 1s cubic-bezier(.19,.74,.86,.37) forwards;
			}
			@keyframes lt00dash {
			  0% {opacity: 1;}
			  100% {
				opacity: 1;
				stroke-dasharray: 990;
				stroke-dashoffset: 0;
			  }
			}
		</style>
<div class="page-loader" style="position: fixed; background: #fff; left: 0px; top: 0px; width: 100%; height: 100%; text-align: center; z-index: 9999;"></div>
<!--div class="page-loader" style="position: fixed; background: #fff; left: 0px; top: 0px; width: 100%; height: 100%; text-align: center; z-index: 9999;">
			<svg  x="0px" y="0px" width="300px" viewBox="0 0 300 100" style="position: absolute; left: 50%; margin-left: -117px; margin-top: -80px; display: inline-block; top: 50%; width: 250px; height: auto;">
				<defs>
				 <clipPath id="maska-vyseku">
					<path class="st1" d="M119.4,33.5L119.4,33.5l-0.7,0H92.3l-7.2,34.5h26.3v0c0,0,0,0,0,0c3,0,5.9-2.4,6.5-5.4l4.9-23.7
						C123.5,36.1,121.9,33.9,119.4,33.5z M111.5,40.3l-4.2,20.5c-0.2,0.8-1.2,1.5-2.3,1.5h-8l4.8-23.5h8
						C110.9,38.8,111.7,39.5,111.5,40.3z"/>
					<polygon class="st1" points="66.5,67.9 77.3,67.9 84.5,33.5 73.7,33.5 		"/>
					<path class="st1" d="M61.2,33.5L61.2,33.5l-0.7,0H34.1L27,67.9h26.3v0c0,0,0,0,0,0c3,0,5.9-2.4,6.5-5.4l4.9-23.7
						C65.3,36.1,63.7,33.9,61.2,33.5z M53.3,40.3l-4.2,20.5c-0.2,0.8-1.2,1.5-2.3,1.5h-8l4.8-23.5h8C52.7,38.8,53.5,39.5,53.3,40.3z"/>
					<polygon class="st1" points="255.4,51 259.1,33.5 248.3,33.5 241.1,67.9 241.1,67.9 246.3,67.9 251.9,67.9 253.7,59.1 257.4,54.8 
						262.1,67.9 272.8,67.9 265,45.7 275.3,33.5 270.2,33.5 		"/>
					<path class="st1" d="M288.2,24c-0.9-1.1-2.1-1.9-3.7-2.2c-0.5-0.1-1-0.2-1.6-0.2H29.6c-2.1,0-4.1,0.8-5.8,2.2
						C22.2,25.1,21,27,20.5,29l-8.9,41.3c-0.1,0.6-0.2,1.1-0.2,1.6c0,1.6,0.5,3,1.4,4.1c0.9,1.1,2.1,1.9,3.7,2.2c0.5,0.1,1,0.2,1.6,0.2
						h253.4c2.1,0,4.1-0.8,5.8-2.2c1.6-1.3,2.9-3.2,3.3-5.2l8.9-41.3c0.1-0.6,0.2-1.1,0.2-1.6C289.5,26.5,289.1,25.1,288.2,24z
						 M286.2,29l-8.9,41.3c-0.3,1.3-1.1,2.5-2.2,3.4c-1.1,0.9-2.4,1.4-3.7,1.4H18c-0.3,0-0.6,0-0.9-0.1c-0.8-0.2-1.4-0.5-1.8-1.1
						c-0.4-0.5-0.7-1.2-0.7-2c0-0.3,0-0.6,0.1-0.9l8.9-41.3c0.3-1.3,1.1-2.5,2.2-3.4c1.1-0.9,2.4-1.4,3.7-1.4H283c0.3,0,0.6,0,0.9,0.1
						c0.8,0.2,1.4,0.5,1.8,1.1c0.4,0.5,0.7,1.2,0.7,2C286.3,28.4,286.2,28.7,286.2,29z"/>
					<polygon class="st1" points="220.2,67.9 231,67.9 238.1,33.5 227.3,33.5 		"/>
					<polygon class="st1" points="186.4,33.5 171.6,51 175.3,33.5 164.5,33.5 157.3,67.9 157.4,67.9 162.5,67.9 168.1,67.9 169.9,59.1 
						173.6,54.8 178.3,67.9 189.1,67.9 181.2,45.7 191.5,33.5 		"/>
					<path class="st1" d="M142.8,33.5h-4.7h-0.4l-18.2,34.5h5.1l5.7-10.8h9.9l1,10.8H152l-3.1-34.5H142.8z M132.1,53.7l6.7-12.6
						l1.1,12.6H132.1z"/>
					<polygon class="st1" points="221.3,33.7 221.3,33.7 221.3,33.4 195.8,33.4 194.6,38.7 194.6,38.7 194.6,38.7 202.9,38.7 
						196.9,67.9 207.7,67.9 213.7,38.7 220.1,38.7 220.1,38.7 220.1,38.7 220.7,35.9 		"/>
				 </clipPath>
			   </defs>
			  
				<g clip-path="url(#maska-vyseku)">
					<rect class="vysek-fill" width="301" height="100"/>
				</g>
			  
				<g id="didaktik-logo-okraj">
										
					<path class="st1" d="M288.2,24c-0.9-1.1-2.1-1.9-3.7-2.2c-0.5-0.1-1-0.2-1.6-0.2H29.6c-2.1,0-4.1,0.8-5.8,2.2
						C22.2,25.1,21,27,20.5,29l-8.9,41.3c-0.1,0.6-0.2,1.1-0.2,1.6c0,1.6,0.5,3,1.4,4.1c0.9,1.1,2.1,1.9,3.7,2.2c0.5,0.1,1,0.2,1.6,0.2
						h253.4c2.1,0,4.1-0.8,5.8-2.2c1.6-1.3,2.9-3.2,3.3-5.2l8.9-41.3c0.1-0.6,0.2-1.1,0.2-1.6C289.5,26.5,289.1,25.1,288.2,24z
						 M286.2,29l-8.9,41.3c-0.3,1.3-1.1,2.5-2.2,3.4c-1.1,0.9-2.4,1.4-3.7,1.4H18c-0.3,0-0.6,0-0.9-0.1c-0.8-0.2-1.4-0.5-1.8-1.1
						c-0.4-0.5-0.7-1.2-0.7-2c0-0.3,0-0.6,0.1-0.9l8.9-41.3c0.3-1.3,1.1-2.5,2.2-3.4c1.1-0.9,2.4-1.4,3.7-1.4H283c0.3,0,0.6,0,0.9,0.1
						c0.8,0.2,1.4,0.5,1.8,1.1c0.4,0.5,0.7,1.2,0.7,2C286.3,28.4,286.2,28.7,286.2,29z"/>
					
				</g>
			</svg>
		</div-->
<div id="top_page"></div>
<header>
<div class="top-header">
<a class="left" href="mailto:martinkovic@didaktik.sk"><i aria-hidden="true" class="fa fa-envelope-o"></i> martinkovic@didaktik.sk</a>
<span class="separator left"></span>
<a class="left" href="tel:+4210346646818"><i aria-hidden="true" class="fa fa-phone"></i> 034 664 68 18</a>
<a class="right" href="/moj-profil"><span class="main-menu-text"><i aria-hidden="true" class="fa fa-user-o"></i> Môj účet</span></a>
<span class="separator right"></span>
<form action="/vyhladavanie" class="right" method="get">
<input name="search_value" placeholder="Zadajte hľadaný výraz..." type="text" value=""/>
<button type="submit"><i aria-hidden="true" class="fa fa-search"></i></button>
</form>
<div class="clear"></div>
</div>
<nav>
<a class="logo left desktop" href="/"><img alt="Didaktik Electronic Logo" src="/skin/frontend/images/logo.png" title="Didaktik Electronic Logo"/></a>
<div class="wrapper">
<ul>
<li><a href="/">Domov</a></li>
<li><a href="/produkty">Naše produkty</a></li>
<li><a href="/o-nas">O nás</a></li>
<li><a href="/obchodne-podmienky">Obchodné a reklamačné podmienky</a></li>
<li><a href="/kontakt">Kontakt</a></li>
</ul>
</div>
<!--a href="/kosik" class="cart-button"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Nákupný košík</a-->
<div class="currency-box">
<form class="currency-form" method="post">
<label>Výber meny: </label>   
						<select name="currency">
<option selected="selected" value="1">EUR</option>
<option value="2">CZK</option>
</select>
</form>
<script>
						jQuery(document).ready(function($) {
							$('.currency-form SELECT').change(function() {
								$('.currency-form').submit();
							});
						});
					</script>
</div>
<div class="cart-box"></div>
</nav>
<a class="logo left mobile" href="/"><img alt="Didaktik Electronic Logo" src="/skin/frontend/images/logo.png" title="Didaktik Electronic Logo"/></a>
<div class="cart-box mobile"></div>
<div id="nav-icon3">
<span></span>
<span></span>
<span></span>
<span></span>
</div>
<form action="/vyhladavanie" class="mobile" method="get">
<input name="search_value" placeholder="Zadajte hľadaný výraz..." type="text" value=""/>
<button type="submit"><i aria-hidden="true" class="fa fa-search"></i></button>
</form>
</header>
<div class="overlay"></div>
<section class="breadcrumb">
<div class="wrapper">
<a href="/"><i aria-hidden="true" class="fa fa-home"></i></a>
<span class="separator">|</span>
<span>Error 404</span>
</div>
</section>
<p> </p>
<article>
<div class="wrapper">
<div class="content no-margin">
<h1 style="color: #ff0000; text-align: center;">Error 404</h1>
<p style="text-align: center;">Požadovaná stránka nebola nájdená.</p>
<p style="text-align: center;"><a class="btn btn-primary" href="/">Pokračovať</a></p>
<p> </p>
<p> </p>
<p> </p>
</div>
</div>
</article> <footer>
<div class="wrapper">
<div class="row">
<div class="col-md-3">
<a class="logo left" href="/"><img alt="Didaktik Electronic Logo" src="/skin/frontend/images/logo.png" title="Didaktik Electronic Logo"/></a>
</div>
<div class="col-md-3">
<h2>Kontakt</h2>
<p>
<strong>Didaktik electronic s.r.o.</strong><br/>
							Pod Kalváriou 22, 909 01 Skalica, Slovenská republika<br/>
							IČO: 45957291<br/>
							DIČ: 2023164231<br/>
							Tel.: +421 34 664 68 18<br/>
							Fax: +421 34 664 68 58<br/>
							martinkovic@didaktik.sk
						</p>
</div>
<div class="col-md-3">
<h2>Info</h2>
<ul>
<li><a href="/obchodne-podmienky">Ochrana osobných údajov</a></li>
<li><a href="/obchodne-podmienky">Obchodné podmienky</a></li>
<li><a href="/obchodne-podmienky">Informácie o dodaní</a></li>
<li><a href="/kontakt">Kontaktujte nás</a></li>
</ul>
</div>
<div class="col-md-3">
<h2>Newsletter</h2>
<p>Pre odber newslettera prosím vyplňte nasledovný formulár</p>
<form method="post">
<input name="email" placeholder="Vaša E-mailová adresa" type="text" value=""/>
<button type="submit"><i aria-hidden="true" class="fa fa-paper-plane"></i></button>
<p><small>Viac o ochrane osobných údajov <a href="">tu</a>.</small></p>
</form>
</div>
</div>
</div>
</footer>
<div class="copyright">
<p>Copyright © 2019. Created by <a href="http://www.ideacorp.sk" target="_blank">IdeaCorp s.r.o.</a> All rights reserved</p>
</div>
<script src="/skin/frontend/js/jquery-ui.js" type="text/javascript"></script>
<script src="/skin/frontend/js/jquery.hoverIntent.js" type="text/javascript"></script>
<script src="/skin/frontend/js/animatescroll.min.js" type="text/javascript"></script>
<!--script src="/skin/frontend/bootstrap/js/bootstrap.min.js"></script>
		<link href="/skin/frontend/bootstrap/css/bootstrap.min.css" rel="stylesheet"-->
<link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" rel="stylesheet"/>
<script crossorigin="anonymous" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link href="/skin/frontend/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<script src="/skin/frontend/fancybox/jquery.fancybox.js"></script>
<link href="/skin/frontend/css/reset.css" rel="stylesheet"/>
<link href="/skin/frontend/css/jquery-ui.css" rel="stylesheet"/>
<link href="/skin/frontend/css/form.css" rel="stylesheet"/>
<link href="/skin/frontend/css/slick.css" rel="stylesheet"/>
<link href="/skin/frontend/css/hamburger.css" rel="stylesheet"/>
<link href="/skin/frontend/css/slider.css" rel="stylesheet"/>
<link href="/skin/frontend/css/animate.css" rel="stylesheet"/>
<link href="/skin/frontend/css/style.css?v=70" rel="stylesheet"/>
<link href="/skin/frontend/css/responsive.css?v=87" rel="stylesheet"/>
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<script src="/skin/frontend/js/slick.min.js" type="text/javascript"></script>
<script src="/skin/frontend/js/wow.min.js"></script>
<script src="/skin/frontend/js/main.js" type="text/javascript"></script>
<script type="text/javascript">
			new WOW().init();
		</script>
<script>
			function flyToElement(flyer, flyingTo) {
										var $func = $(this);
										var divider = 3;
										var flyerClone = $(flyer).clone();
										$(flyerClone).css( { position: 'absolute', top: $(flyer).offset().top + "px", left: $(flyer).offset().left + "px", opacity: 1, 'z-index': 1000 } );
										$('body').append($(flyerClone));
										var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width()/divider)/2;
										var gotoY = $(flyingTo).offset().top + ($(flyingTo).height() / 2) - ($(flyer).height()/divider)/2;
										 
										$(flyerClone).animate( {
											opacity: 0.4,
											left: gotoX,
											top: gotoY,
											width: $(flyer).width()/divider,
											height: $(flyer).height()/divider
										}, 700,
										function () {
											$(flyingTo).fadeOut('fast', function () {
												$(flyingTo).fadeIn('fast', function () {
													$(flyerClone).fadeOut('fast', function () {
														$(flyerClone).remove();
													});
												});
											});
										});
			}
			
			jQuery(document).ready(function($) {
				$('.main-slider').slick({
					prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>',
					nextArrow: '<button type="button" class="slick-next"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>',
					arrows: false,
					dots: true
				});

				/*$(window).load(function() {
					setTimeout(function() {
						$('.page-loader').fadeOut();
					}, 750);
				});*/
				
				$('.page-loader').fadeOut();
			});
		</script>
<div id="add_to_cart_popup">
<div class="cart-popup">
<button class="close" type="button">×</button>
<h2>Produkt bol pridaný do košíka</h2>
<div class="add_to_cart_popup_content"></div>
<a class="btn btn-primary" href="/kosik"><i aria-hidden="true" class="fa fa-shopping-cart"></i> Prejsť do košíka</a>
</div>
</div>
<!--Cookie banner-->
<script data-bg="#29ace2" data-close-style="	background-color: #001689;
								cursor: pointer;
								padding: 7px 20px;
								position: absolute;
								right: 10px;
								bottom: 10px;
								text-align: center;
								color: #fff;
								font-size: 12px;
								border-radius: 15px;" data-close-text="Rozumiem" data-cookie="CookieInfoScript" data-divlink="#FFFFFF" data-divlinkbg="#001689" data-fg="#000" data-font-family="'Exo', sans-serif" data-font-size="14px" data-height="auto" data-link="#001689" data-linkmsg="tu" data-max-height="50px" data-message="Webová stránka používa súbory cookies, ktoré sú potrebné k zabezpečeniu funkčnosti stránky a pre vylepšovanie užívateľskej skúsenosti.&lt;br&gt;Ďalším prehliadaním súhlasíte s používaním súborov cookies. Viac informácií " data-moreinfo="/ochrana-osobnych-udajov.pdf" data-padding="20px 16px" data-text-align="left" data-z-index="10000000000" id="cookiebanner" src="https://cdnjs.cloudflare.com/ajax/libs/cookie-banner/1.2.2/cookiebanner.min.js" type="text/javascript">
</script>
<!--end Cookie banner-->
</body>
</html>