<!DOCTYPE html>
<html class="no-touch" lang="ja" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://eiken-net.com/cms/xmlrpc.php" rel="pingback"/>
<title>404 - eiken – 栄建</title>
<!-- The SEO Framework by Sybre Waaijer -->
<meta content="max-snippet:-1,max-image-preview:standard,max-video-preview:-1" name="robots"/>
<!-- / The SEO Framework by Sybre Waaijer | 0.12ms meta | 2.38ms boot -->
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://eiken-net.com/feed/" rel="alternate" title="eiken - 栄建 » フィード" type="application/rss+xml"/>
<link href="https://eiken-net.com/comments/feed/" rel="alternate" title="eiken - 栄建 » コメントフィード" type="application/rss+xml"/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.12.2 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script data-cfasync="false" type="text/javascript">
	var mi_version         = '7.12.2';
	var mi_track_user      = true;
	var mi_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-61854489-1';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}

	if ( 'undefined' === typeof gaOptout ) {
		function gaOptout() {
			__gaTrackerOptout();
		}
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-61854489-1', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('require', 'displayfeatures');
		__gaTracker('require', 'linkid', 'linkid.js');
		__gaTracker('send','pageview','/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer);
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<!-- / Google Analytics by MonsterInsights -->
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/eiken-net.com\/cms\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://eiken-net.com/cms/wp-content/plugins/LayerSlider/assets/static/layerslider/css/layerslider.css?ver=6.11.2" id="layerslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eiken-net.com/cms/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eiken-net.com/cms/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.2.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eiken-net.com/cms/wp-content/plugins/revslider/public/assets/css/rs6.css?ver=6.2.21" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
#rs-demo-id {}
</style>
<link href="//fonts.googleapis.com/css?family=Poppins%3A300%2Cregular%2C500%2C600%2C700%7CDroid+Serif%3Aregular%2Citalic%2C700%2C700italic%7CDosis%3A200%2C300%2Cregular%2C500%2C600%2C700%2C800%7CPlayfair+Display%3Aregular%2Citalic%2C700%2C700italic%2C900%2C900italic%7COswald%3A300%2Cregular%2C700%7CRoboto%3A100%2C100italic%2C300%2C300italic%2Cregular%2Citalic%2C500%2C500italic%2C700%2C700italic%2C900%2C900italic%7CNunito%3A200%2C200italic%2C300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C900%2C900italic&amp;subset=devanagari%2Clatin-ext%2Clatin%2Ccyrillic%2Cvietnamese%2Cgreek%2Ccyrillic-ext%2Cgreek-ext&amp;ver=2.2.8" id="uncodefont-google-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eiken-net.com/cms/wp-content/plugins/uncode-privacy/assets/css/uncode-privacy-public.css?ver=2.1.1" id="uncode-privacy-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eiken-net.com/cms/wp-content/themes/uncode/library/css/style.css?ver=907015489" id="uncode-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="uncode-style-inline-css" type="text/css">

@media (min-width: 960px) { .limit-width { max-width: 1200px; margin: auto;}}
</style>
<link href="https://eiken-net.com/cms/wp-content/themes/uncode/library/css/style-custom.css?ver=1000146494" id="uncode-custom-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="uncode-custom-style-inline-css" type="text/css">
p {line-height:2.5!important;}.vertical-text {-webkit-writing-mode:vertical-rl;-ms-writing-mode:tb-rl;writing-mode:vertical-rl;font-family:"游明朝", YuMincho, "ヒラギノ明朝 ProN W3", "Hiragino Mincho ProN", "HG明朝E", "ＭＳ Ｐ明朝", "ＭＳ 明朝", serif;height:450px;padding-left:18px;background-color:white;padding-right:18px;}.icon-box-right .icon-box-heading {display:table;table-layout:fixed;width:auto;margin-top:2px !important;margin-bottom:2px !important;padding-top:2px;}.icon-box-right .icon-box-content {padding-right:10px;}.vertical-text-mobile {-webkit-writing-mode:vertical-rl;-ms-writing-mode:tb-rl;writing-mode:vertical-rl;font-family:"游明朝", YuMincho, "ヒラギノ明朝 ProN W3", "Hiragino Mincho ProN", "HG明朝E", "ＭＳ Ｐ明朝", "ＭＳ 明朝", serif;height:300px;padding-left:18px;background-color:white;padding-right:18px;padding-top:30px}.menu-shadows {box-shadow:none;}h1 {line-height:1.75em!important;}h2 {line-height:1.75em!important;}.text-lead, .text-lead > * {font-size:18px;line-height:1.75;margin:18px 0 0;margin:0px 10px 0px 0px;}.top-menu {top:30px;background-color:rgba(255, 0, 0, 0);}.menu-mini.menu-smart a {padding:4px 25px 4px 9px;font-size:16px;font-weight:600;}.menu-smart i.fa-dropdown {display:inline-block;position:relative;padding:0px 0px 0px 9px;margin:0px 0px 0px 0px;margin-right:0;box-sizing:content-box !important;font-size:.8em;text-align:right !important;display:none;}#menu-item-73979:before {background-image:url('https://eiken-net.com/cms/wp-content/uploads/2019/12/menu-icon-1.svg)');background-size:17px 17px;display:inline-block;width:17px; height:17px;content:"";}#menu-item-73980:before {background-image:url('https://eiken-net.com/cms/wp-content/uploads/2019/12/eiken-calender.svg)');background-size:17px 17px;display:inline-block;width:17px; height:17px;content:"";}#menu-item-73981:before {background-image:url('https://eiken-net.com/cms/wp-content/uploads/2019/12/menu-icon-3.svg)');background-size:17px 17px;display:inline-block;width:17px; height:17px;content:"";}#menu-item-73982 {border:2px solid;padding-left:15px;padding-top:4px;padding-bottom:4px;}.footer-button {font-size:16px;}.style-light .btn-default.btn-outline {font-family:ten-mincho-text,serif!important;}hr.separator-break.separator-accent {border-color:#000000 !important;margin-top:0px!important;}.main-container .row-container .row-parent .column_child.single-internal-gutter .uncont > *:not(:first-child) .footer-nav {margin-top:0px!important;}.top-menu {position:fixed;width:100%;top:-5px !important;}.row-menu-inner{top:20px;}.map-responsive{overflow:hidden;padding-bottom:65%;position:relative;height:0;}.map-responsive iframe{left:0;top:0;height:100%;width:100%;position:absolute;}.footer-scroll-top {z-index:100;display:none;position:fixed;bottom:35px;right:100px;}.grecaptcha-badge {width:70px !important;overflow:hidden !important;transition:all 0.3s ease !important;left:4px !important;bottom:-100px !important;}.grecaptcha-badge:hover {width:256px !important;}.author-info {display:none !important;}.tmb .t-entry p.t-entry-author .tmb-username-wrap .tmb-username-text {display:none !important;}.tmb .t-entry p.t-entry-author img {width:20px;height:20px;vertical-align:middle;display:inline-block;position:relative;border-radius:50%;display:none !important;}.column-center {display:block;margin-left:auto;margin-right:auto;}.responsiveCal { position:relative; padding-bottom:130%; height:0; overflow:hidden; } .responsiveCal iframe { position:absolute; top:0; left:0; width:100%; height:100%; }@media screen and (min-width:768px) {.five-columns.vc_row .vc_col-sm-2 {float:left;width:18.5%;padding:0;margin-right:1.5%;min-height:0;}.five-columns.vc_row .vc_col-sm-2:nth-last-child(2) {margin-right:0;}}
</style>
<link href="https://eiken-net.com/cms/wp-content/themes/uncode-child/style.css?ver=907015489" id="child-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eiken-net.com/cms/wp-content/plugins/date-time-picker-field/assets/js/vendor/datetimepicker/jquery.datetimepicker.min.css?ver=5.4.4" id="dtpicker-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eiken-net.com/cms/wp-content/themes/uncode/library/css/uncode-icons.css?ver=1000146494" id="uncode-icons-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://eiken-net.com/cms/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://eiken-net.com/cms/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var LS_Meta = {"v":"6.11.2","fixGSAP":"1"};
/* ]]> */
</script>
<script src="https://eiken-net.com/cms/wp-content/plugins/LayerSlider/assets/static/layerslider/js/layerslider.utils.js?ver=6.11.2" type="text/javascript"></script>
<script src="https://eiken-net.com/cms/wp-content/plugins/LayerSlider/assets/static/layerslider/js/layerslider.kreaturamedia.jquery.js?ver=6.11.2" type="text/javascript"></script>
<script src="https://eiken-net.com/cms/wp-content/plugins/LayerSlider/assets/static/layerslider/js/layerslider.transitions.js?ver=6.11.2" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[{\"path\":\"\\\/go\\\/\",\"label\":\"affiliate\"},{\"path\":\"\\\/recommend\\\/\",\"label\":\"affiliate\"}]","home_url":"https:\/\/eiken-net.com","hash_tracking":"false"};
/* ]]> */
</script>
<script src="https://eiken-net.com/cms/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.12.2" type="text/javascript"></script>
<script src="https://eiken-net.com/cms/wp-content/plugins/revslider/public/assets/js/rbtools.min.js?ver=6.2.21" type="text/javascript"></script>
<script src="https://eiken-net.com/cms/wp-content/plugins/revslider/public/assets/js/rs6.min.js?ver=6.2.21" type="text/javascript"></script>
<script data-breakpoints-images="258,516,720,1032,1440,2064,2880" data-home="/" data-path="/cms/" id="uncodeAI" src="/cms/wp-content/themes/uncode/library/js/ai-uncode.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var SiteParameters = {"days":"days","hours":"hours","minutes":"minutes","seconds":"seconds","constant_scroll":"on","scroll_speed":"2","parallax_factor":"0.25","loading":"Loading\u2026","slide_name":"slide","slide_footer":"\u30d5\u30c3\u30bf\u30fc","ajax_url":"https:\/\/eiken-net.com\/cms\/wp-admin\/admin-ajax.php","nonce_adaptive_images":"0751d408ed","enable_debug":"","block_mobile_videos":"","is_frontend_editor":"","mobile_parallax_allowed":"","wireframes_plugin_active":"1"};
/* ]]> */
</script>
<script src="https://eiken-net.com/cms/wp-content/themes/uncode/library/js/init.js?ver=1000146494" type="text/javascript"></script>
<meta content="Powered by LayerSlider 6.11.2 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress." name="generator"/>
<!-- LayerSlider updates and docs at: https://layerslider.kreaturamedia.com -->
<link href="https://eiken-net.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://eiken-net.com/cms/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://eiken-net.com/cms/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><meta content="Powered by Slider Revolution 6.2.21 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." name="generator"/>
<link href="https://eiken-net.com/cms/wp-content/uploads/2020/04/cropped-eiken-favicon-01-1-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://eiken-net.com/cms/wp-content/uploads/2020/04/cropped-eiken-favicon-01-1-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://eiken-net.com/cms/wp-content/uploads/2020/04/cropped-eiken-favicon-01-1-180x180.png" rel="apple-touch-icon"/>
<meta content="https://eiken-net.com/cms/wp-content/uploads/2020/04/cropped-eiken-favicon-01-1-270x270.png" name="msapplication-TileImage"/>
<script type="text/javascript">function setREVStartSize(e){
			//window.requestAnimationFrame(function() {				 
				window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;	
				window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;	
				try {								
					var pw = document.getElementById(e.c).parentNode.offsetWidth,
						newh;
					pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
					e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
					e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
					e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
					e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
					e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
					e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
					e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);		
					if(e.layout==="fullscreen" || e.l==="fullscreen") 						
						newh = Math.max(e.mh,window.RSIH);					
					else{					
						e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
						for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];					
						e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
						e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
						for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];
											
						var nl = new Array(e.rl.length),
							ix = 0,						
							sl;					
						e.tabw = e.tabhide>=pw ? 0 : e.tabw;
						e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
						e.tabh = e.tabhide>=pw ? 0 : e.tabh;
						e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;					
						for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
						sl = nl[0];									
						for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}															
						var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);					
						newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
					}				
					if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));					
					document.getElementById(e.c).height = newh+"px";
					window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";				
				} catch(e){
					console.log("Failure at Presize of Slider:" + e)
				}					   
			//});
		  };</script>
<noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>
<body class="error404 style-color-xsdn-bg group-blog hormenu-position-left hmenu hmenu-position-right header-full-width main-center-align menu-mobile-animated menu-mobile-centered mobile-parallax-not-allowed ilb-no-bounce unreg wpb-js-composer js-comp-ver-6.2.0 vc_responsive" data-border="0">
<div class="body-borders" data-border="0"><div class="top-border body-border-shadow"></div><div class="right-border body-border-shadow"></div><div class="bottom-border body-border-shadow"></div><div class="left-border body-border-shadow"></div><div class="top-border style-light-bg"></div><div class="right-border style-light-bg"></div><div class="bottom-border style-light-bg"></div><div class="left-border style-light-bg"></div></div> <div class="box-wrapper">
<div class="box-container">
<script type="text/javascript">UNCODE.initBox();</script>
<div class="menu-wrapper menu-sticky">
<div class="top-menu mobile-hidden tablet-hidden navbar menu-secondary menu-light submenu-light style-color-xsdn-bg">
<div class="row-menu">
<div class="row-menu-inner">
<div class="col-lg-0 middle">
<div class="menu-bloginfo">
<div class="menu-bloginfo-inner style-light">
</div>
</div>
</div>
<div class="col-lg-12 menu-horizontal">
<div class="navbar-topmenu navbar-nav-last"><ul class="menu-smart menu-mini sm" id="menu-top-menu"><li class="marche-menu menu-item menu-item-type-custom menu-item-object-custom menu-item-74524 menu-item-link" id="menu-item-74524"><a href="https://eiken-net.com/cms/eiken-marche/" title="eiken marche">eiken marche<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="visit-menu menu-item menu-item-type-custom menu-item-object-custom menu-item-74525 menu-item-link" id="menu-item-74525"><a href="https://eiken-net.com/cms/visitors-information/" title="見学会情報">見学会情報<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="consultation-menu menu-item menu-item-type-custom menu-item-object-custom menu-item-74527 menu-item-link" id="menu-item-74527"><a href="https://eiken-net.com/cms/consultation-flow/" title="相談の流れ">相談の流れ<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="siryo menu-item menu-item-type-custom menu-item-object-custom menu-item-74529 menu-item-link" id="menu-item-74529"><a href="https://eiken-net.com/cms/contact/" title="資料請求">資料請求<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-75683 menu-item-link" id="menu-item-75683"><a href="https://eiken-net.com/online-consultation/" title="オンライン相談">オンライン相談<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-74692 menu-item-link" id="menu-item-74692"><a href="tel:%200120-459-849" title="0120-459-849">0120-459-849<i class="fa fa-angle-right fa-dropdown"></i></a></li>
</ul></div>
</div>
</div>
</div>
</div>
<header class="navbar menu-primary menu-light submenu-dark menu-transparent menu-add-padding style-light-original menu-absolute menu-animated menu-with-logo" id="masthead">
<div class="menu-container style-color-xsdn-bg menu-no-borders menu-shadows">
<div class="row-menu">
<div class="row-menu-inner">
<div class="col-lg-0 logo-container middle" id="logo-container-mobile">
<div class="navbar-header style-light" id="main-logo">
<a class="navbar-brand" data-minheight="50" href="https://eiken-net.com/"><div class="logo-image main-logo logo-skinnable" data-maxheight="70" style="height: 70px;"><img alt="logo" class="img-responsive" height="1" src="https://eiken-net.com/cms/wp-content/uploads/2019/12/logo.svg" width="1"/></div></a>
</div>
<div class="mmb-container"><div class="mobile-menu-button 1 mobile-menu-button-light lines-button x2"><span class="lines"></span></div></div>
</div>
<div class="col-lg-12 main-menu-container middle">
<div class="menu-horizontal menu-dd-shadow-lg">
<div class="menu-horizontal-inner">
<div class="nav navbar-nav navbar-main navbar-nav-last"><ul class="menu-primary-inner menu-smart sm" id="menu-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73970 menu-item-link" id="menu-item-73970"><a href="https://eiken-net.com/values/" title="大切にしていること">大切にしていること<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73971 menu-item-link" id="menu-item-73971"><a href="https://eiken-net.com/construction-examples/" title="施工事例">施工事例<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73978 menu-item-link" id="menu-item-73978"><a href="https://eiken-net.com/basic-specifications/" title="標準仕様">標準仕様<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73974 menu-item-link" id="menu-item-73974"><a href="https://eiken-net.com/about-structure/" title="構造について">構造について<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73975 menu-item-link" id="menu-item-73975"><a href="https://eiken-net.com/renovation/" title="リフォーム">リフォーム<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73976 menu-item-link" id="menu-item-73976"><a href="https://eiken-net.com/company-information/" title="会社案内">会社案内<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73977 menu-item-link" id="menu-item-73977"><a href="https://eiken-net.com/contact/" title="お問い合わせ">お問い合わせ<i class="fa fa-angle-right fa-dropdown"></i></a></li>
</ul></div><div class="desktop-hidden">
<div class="menu-accordion"><ul class="menu-smart sm sm-vertical mobile-secondary-menu" id="menu-top-menu-1"><li class="marche-menu menu-item menu-item-type-custom menu-item-object-custom menu-item-74524 menu-item-link"><a href="https://eiken-net.com/cms/eiken-marche/" title="eiken marche">eiken marche<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="visit-menu menu-item menu-item-type-custom menu-item-object-custom menu-item-74525 menu-item-link"><a href="https://eiken-net.com/cms/visitors-information/" title="見学会情報">見学会情報<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="consultation-menu menu-item menu-item-type-custom menu-item-object-custom menu-item-74527 menu-item-link"><a href="https://eiken-net.com/cms/consultation-flow/" title="相談の流れ">相談の流れ<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="siryo menu-item menu-item-type-custom menu-item-object-custom menu-item-74529 menu-item-link"><a href="https://eiken-net.com/cms/contact/" title="資料請求">資料請求<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-75683 menu-item-link"><a href="https://eiken-net.com/online-consultation/" title="オンライン相談">オンライン相談<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-74692 menu-item-link"><a href="tel:%200120-459-849" title="0120-459-849">0120-459-849<i class="fa fa-angle-right fa-dropdown"></i></a></li>
</ul></div></div></div>
</div>
</div>
</div>
</div>
</div>
</header>
</div> <script type="text/javascript">UNCODE.fixMenuHeight();</script>
<div class="main-wrapper">
<div class="main-container">
<div class="page-wrapper">
<div class="sections-container">
<div id="page-header"><div class="header-wrapper header-uncode-block">
<div class="header-content-block row-container style-color-xsdn-bg style-light"><div class="header-content-block-inner limit-width row row-parent"></div><!-- /.header-content-block --></div><!-- /.header-content-block-inner --></div></div><script type="text/javascript">UNCODE.initHeader();</script><div class="page-body style-color-xsdn-bg">
<div class="post-wrapper">
<div class="post-body"></div>
</div>
</div> </div><!-- sections container -->
</div><!-- page wrapper -->
<footer class="site-footer" id="colophon">
<div class="row-container style-color-xsdn-bg footer-last">
<div class="row row-parent style-light limit-width no-top-padding no-h-padding no-bottom-padding">
<div class="site-info uncell col-lg-6 pos-middle text-left">© 2021 eiken - 栄建. All rights reserved</div><!-- site info -->
</div>
</div> </footer>
</div><!-- main container -->
</div><!-- main wrapper -->
</div><!-- box container -->
</div><!-- box wrapper -->
<div class="style-light footer-scroll-top"><a class="scroll-top" href="#"><i class="fa fa-angle-up fa-stack fa-rounded btn-default btn-hover-nobg"></i></a></div> <div class="overlay overlay-sequential style-dark style-dark-bg overlay-search" data-area="search" data-container="box-container">
<div class="mmb-container"><div class="menu-close-search mobile-menu-button menu-button-offcanvas mobile-menu-button-dark lines-button x2 overlay-close close" data-area="search" data-container="box-container"><span class="lines"></span></div></div>
<div class="search-container"><form action="https://eiken-net.com/" method="get">
<div class="search-container-inner">
<input class="search-field form-fluid no-livesearch" name="s" placeholder="検索" title="Search for:" type="search" value=""/>
<i class="fa fa-search3"></i>
</div>
</form>
</div>
</div>
<!--Start of Tawk.to Script (0.3.3)-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{};
var Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5ec7765a8ee2956d73a387ac/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script (0.3.3)--><div class="gdpr-overlay"></div><div class="gdpr gdpr-privacy-preferences">
<div class="gdpr-wrapper">
<form action="https://eiken-net.com/cms/wp-admin/admin-post.php" class="gdpr-privacy-preferences-frm" method="post">
<input name="action" type="hidden" value="uncode_privacy_update_privacy_preferences"/>
<input id="update-privacy-preferences-nonce" name="update-privacy-preferences-nonce" type="hidden" value="f4f2e4edf6"/><input name="_wp_http_referer" type="hidden" value="/japanese/images/freshbox147/docs/772f50d8de05727f1f05afe7417758bc/%09"/> <header>
<div class="gdpr-box-title">
<h3>Privacy Preference Center</h3>
<span class="gdpr-close"></span>
</div>
</header>
<div class="gdpr-content">
<div class="gdpr-tab-content">
<div class="gdpr-consent-management gdpr-active">
<header>
<h4>Privacy Preferences</h4>
</header>
<div class="gdpr-info">
<p></p>
</div>
</div>
</div>
</div>
<footer>
<input class="btn-accent btn-flat" type="submit" value="Save Preferences"/>
</footer>
</form>
</div>
</div>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/eiken-net.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://eiken-net.com/cms/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.2.2" type="text/javascript"></script>
<script src="https://eiken-net.com/cms/wp-content/plugins/uncode-privacy/assets/js/js-cookie.min.js?ver=2.2.0" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var Uncode_Privacy_Parameters = {"accent_color":"#101213"};
/* ]]> */
</script>
<script src="https://eiken-net.com/cms/wp-content/plugins/uncode-privacy/assets/js/uncode-privacy-public.min.js?ver=2.1.1" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6LeyyN8UAAAAANSFq5fiksc9GSx5KwSRORbPn5CY&amp;ver=3.0" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7_recaptcha = {"sitekey":"6LeyyN8UAAAAANSFq5fiksc9GSx5KwSRORbPn5CY","actions":{"homepage":"homepage","contactform":"contactform"}};
/* ]]> */
</script>
<script src="https://eiken-net.com/cms/wp-content/plugins/contact-form-7/modules/recaptcha/script.js?ver=5.2.2" type="text/javascript"></script>
<script src="https://eiken-net.com/cms/wp-content/plugins/date-time-picker-field/assets/js/vendor/moment/moment.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://eiken-net.com/cms/wp-content/plugins/date-time-picker-field/assets/js/vendor/datetimepicker/jquery.datetimepicker.full.min.js?ver=5.4.4" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var datepickeropts = {"selector":".datafield","locale":"en","theme":"default","datepicker":"on","timepicker":"on","inline":"off","placeholder":"off","preventkeyboard":"off","minDate":"off","step":"60","minTime":"00:00","maxTime":"23:59","offset":"0","min_date":"","max_date":"","dateformat":"YYYY-MM-DD","hourformat":"HH:mm","load":"full","disabled_days":"","disabled_calendar_days":"","allowed_times":["09:00","09:30","10:00","10:30","11:00","11:30","12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00"],"sunday_times":"","monday_times":"","tuesday_times":"","wednesday_times":"","thursday_times":"","friday_times":"","saturday_times":"","dayOfWeekStart":"1","i18n":{"en":{"months":["1\u6708","2\u6708","3\u6708","4\u6708","5\u6708","6\u6708","7\u6708","8\u6708","9\u6708","10\u6708","11\u6708","12\u6708"],"dayOfWeekShort":["\u65e5","\u6708","\u706b","\u6c34","\u6728","\u91d1","\u571f"],"dayOfWeek":["\u65e5\u66dc\u65e5","\u6708\u66dc\u65e5","\u706b\u66dc\u65e5","\u6c34\u66dc\u65e5","\u6728\u66dc\u65e5","\u91d1\u66dc\u65e5","\u571f\u66dc\u65e5"]}},"format":"YYYY-MM-DD HH:mm","clean_format":"Y-m-d H:i","value":"2021-01-13 09:00","timezone":"Asia\/Tokyo","utc_offset":"9","now":"2021-01-13 00:09"};
/* ]]> */
</script>
<script src="https://eiken-net.com/cms/wp-content/plugins/date-time-picker-field/assets/js/dtpicker.js?ver=5.4.4" type="text/javascript"></script>
<script type="text/javascript">
var mejsL10n = {"language":"ja","strings":{"mejs.download-file":"\u30d5\u30a1\u30a4\u30eb\u3092\u30c0\u30a6\u30f3\u30ed\u30fc\u30c9","mejs.install-flash":"\u3054\u5229\u7528\u306e\u30d6\u30e9\u30a6\u30b6\u30fc\u306f Flash Player \u304c\u7121\u52b9\u306b\u306a\u3063\u3066\u3044\u308b\u304b\u3001\u30a4\u30f3\u30b9\u30c8\u30fc\u30eb\u3055\u308c\u3066\u3044\u307e\u305b\u3093\u3002Flash Player \u30d7\u30e9\u30b0\u30a4\u30f3\u3092\u6709\u52b9\u306b\u3059\u308b\u304b\u3001\u6700\u65b0\u30d0\u30fc\u30b8\u30e7\u30f3\u3092 https:\/\/get.adobe.com\/jp\/flashplayer\/ \u304b\u3089\u30a4\u30f3\u30b9\u30c8\u30fc\u30eb\u3057\u3066\u304f\u3060\u3055\u3044\u3002","mejs.fullscreen":"\u30d5\u30eb\u30b9\u30af\u30ea\u30fc\u30f3","mejs.play":"\u518d\u751f","mejs.pause":"\u505c\u6b62","mejs.time-slider":"\u30bf\u30a4\u30e0\u30b9\u30e9\u30a4\u30c0\u30fc","mejs.time-help-text":"1\u79d2\u9032\u3080\u306b\u306f\u5de6\u53f3\u77e2\u5370\u30ad\u30fc\u3092\u300110\u79d2\u9032\u3080\u306b\u306f\u4e0a\u4e0b\u77e2\u5370\u30ad\u30fc\u3092\u4f7f\u3063\u3066\u304f\u3060\u3055\u3044\u3002","mejs.live-broadcast":"\u751f\u653e\u9001","mejs.volume-help-text":"\u30dc\u30ea\u30e5\u30fc\u30e0\u8abf\u7bc0\u306b\u306f\u4e0a\u4e0b\u77e2\u5370\u30ad\u30fc\u3092\u4f7f\u3063\u3066\u304f\u3060\u3055\u3044\u3002","mejs.unmute":"\u30df\u30e5\u30fc\u30c8\u89e3\u9664","mejs.mute":"\u30df\u30e5\u30fc\u30c8","mejs.volume-slider":"\u30dc\u30ea\u30e5\u30fc\u30e0\u30b9\u30e9\u30a4\u30c0\u30fc","mejs.video-player":"\u52d5\u753b\u30d7\u30ec\u30fc\u30e4\u30fc","mejs.audio-player":"\u97f3\u58f0\u30d7\u30ec\u30fc\u30e4\u30fc","mejs.captions-subtitles":"\u30ad\u30e3\u30d7\u30b7\u30e7\u30f3\/\u5b57\u5e55","mejs.captions-chapters":"\u30c1\u30e3\u30d7\u30bf\u30fc","mejs.none":"\u306a\u3057","mejs.afrikaans":"\u30a2\u30d5\u30ea\u30ab\u30fc\u30f3\u30b9\u8a9e","mejs.albanian":"\u30a2\u30eb\u30d0\u30cb\u30a2\u8a9e","mejs.arabic":"\u30a2\u30e9\u30d3\u30a2\u8a9e","mejs.belarusian":"\u30d9\u30e9\u30eb\u30fc\u30b7\u8a9e","mejs.bulgarian":"\u30d6\u30eb\u30ac\u30ea\u30a2\u8a9e","mejs.catalan":"\u30ab\u30bf\u30ed\u30cb\u30a2\u8a9e","mejs.chinese":"\u4e2d\u56fd\u8a9e","mejs.chinese-simplified":"\u4e2d\u56fd\u8a9e (\u7c21\u4f53\u5b57)","mejs.chinese-traditional":"\u4e2d\u56fd\u8a9e (\u7e41\u4f53\u5b57)","mejs.croatian":"\u30af\u30ed\u30a2\u30c1\u30a2\u8a9e","mejs.czech":"\u30c1\u30a7\u30b3\u8a9e","mejs.danish":"\u30c7\u30f3\u30de\u30fc\u30af\u8a9e","mejs.dutch":"\u30aa\u30e9\u30f3\u30c0\u8a9e","mejs.english":"\u82f1\u8a9e","mejs.estonian":"\u30a8\u30b9\u30c8\u30cb\u30a2\u8a9e","mejs.filipino":"\u30d5\u30a3\u30ea\u30d4\u30f3\u8a9e","mejs.finnish":"\u30d5\u30a3\u30f3\u30e9\u30f3\u30c9\u8a9e","mejs.french":"\u30d5\u30e9\u30f3\u30b9\u8a9e","mejs.galician":"\u30ac\u30ea\u30b7\u30a2\u8a9e","mejs.german":"\u30c9\u30a4\u30c4\u8a9e","mejs.greek":"\u30ae\u30ea\u30b7\u30e3\u8a9e","mejs.haitian-creole":"\u30cf\u30a4\u30c1\u8a9e","mejs.hebrew":"\u30d8\u30d6\u30e9\u30a4\u8a9e","mejs.hindi":"\u30d2\u30f3\u30c7\u30a3\u30fc\u8a9e","mejs.hungarian":"\u30cf\u30f3\u30ac\u30ea\u30fc\u8a9e","mejs.icelandic":"\u30a2\u30a4\u30b9\u30e9\u30f3\u30c9\u8a9e","mejs.indonesian":"\u30a4\u30f3\u30c9\u30cd\u30b7\u30a2\u8a9e","mejs.irish":"\u30a2\u30a4\u30eb\u30e9\u30f3\u30c9\u8a9e","mejs.italian":"\u30a4\u30bf\u30ea\u30a2\u8a9e","mejs.japanese":"\u65e5\u672c\u8a9e","mejs.korean":"\u97d3\u56fd\u8a9e","mejs.latvian":"\u30e9\u30c8\u30d3\u30a2\u8a9e","mejs.lithuanian":"\u30ea\u30c8\u30a2\u30cb\u30a2\u8a9e","mejs.macedonian":"\u30de\u30b1\u30c9\u30cb\u30a2\u8a9e","mejs.malay":"\u30de\u30ec\u30fc\u8a9e","mejs.maltese":"\u30de\u30eb\u30bf\u8a9e","mejs.norwegian":"\u30ce\u30eb\u30a6\u30a7\u30fc\u8a9e","mejs.persian":"\u30da\u30eb\u30b7\u30a2\u8a9e","mejs.polish":"\u30dd\u30fc\u30e9\u30f3\u30c9\u8a9e","mejs.portuguese":"\u30dd\u30eb\u30c8\u30ac\u30eb\u8a9e","mejs.romanian":"\u30eb\u30fc\u30de\u30cb\u30a2\u8a9e","mejs.russian":"\u30ed\u30b7\u30a2\u8a9e","mejs.serbian":"\u30bb\u30eb\u30d3\u30a2\u8a9e","mejs.slovak":"\u30b9\u30ed\u30d0\u30ad\u30a2\u8a9e","mejs.slovenian":"\u30b9\u30ed\u30d9\u30cb\u30a2\u8a9e","mejs.spanish":"\u30b9\u30da\u30a4\u30f3\u8a9e","mejs.swahili":"\u30b9\u30ef\u30d2\u30ea\u8a9e","mejs.swedish":"\u30b9\u30a6\u30a7\u30fc\u30c7\u30f3\u8a9e","mejs.tagalog":"\u30bf\u30ac\u30ed\u30b0\u8a9e","mejs.thai":"\u30bf\u30a4\u8a9e","mejs.turkish":"\u30c8\u30eb\u30b3\u8a9e","mejs.ukrainian":"\u30a6\u30af\u30e9\u30a4\u30ca\u8a9e","mejs.vietnamese":"\u30d9\u30c8\u30ca\u30e0\u8a9e","mejs.welsh":"\u30a6\u30a7\u30fc\u30eb\u30ba\u8a9e","mejs.yiddish":"\u30a4\u30c7\u30a3\u30c3\u30b7\u30e5\u8a9e"}};
</script>
<script src="https://eiken-net.com/cms/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.13-9993131" type="text/javascript"></script>
<script src="https://eiken-net.com/cms/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=5.4.4" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpmejsSettings = {"pluginPath":"\/cms\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
/* ]]> */
</script>
<script src="https://eiken-net.com/cms/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://eiken-net.com/cms/wp-content/themes/uncode/library/js/plugins.js?ver=1000146494" type="text/javascript"></script>
<script src="https://eiken-net.com/cms/wp-content/themes/uncode/library/js/app.js?ver=1000146494" type="text/javascript"></script>
<script src="https://eiken-net.com/cms/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
</body>
</html>
