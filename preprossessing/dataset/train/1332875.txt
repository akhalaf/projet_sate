<!DOCTYPE html>
<html lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>Home</title>
<base href="https://www.skynetix.co.za/"/>
<meta content="width=992" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="keywords"/>
<!-- Facebook Open Graph -->
<meta content="Home" name="og:title"/>
<meta content="" name="og:description"/>
<meta content="" name="og:image"/>
<meta content="article" name="og:type"/>
<meta content="https://www.skynetix.co.za/" name="og:url"/>
<!-- Facebook Open Graph end -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/main.js?v=20171208160953" type="text/javascript"></script>
<link href="css/font-awesome/font-awesome.min.css?v=4.7.0" rel="stylesheet" type="text/css"/>
<link href="css/site.css?v=20171227143500" rel="stylesheet" type="text/css"/>
<link href="css/common.css?ts=1514988158" rel="stylesheet" type="text/css"/>
<link href="css/1.css?ts=1514988158" rel="stylesheet" type="text/css"/>
<script type="text/javascript">var currLang = '';</script>
<!--[if lt IE 9]>
	<script src="js/html5shiv.min.js"></script>
	<![endif]-->
</head>
<body><div class="root"><div class="vbox wb_container" id="wb_header">
<div class="wb_cont_inner"><div class="wb_element wb_element_picture" id="wb_element_instance0"><img alt="gallery/clock" src="gallery_gen/2e0735645625471706fdb7957cda376a.png"/></div><div class="wb_element" id="wb_element_instance1" style=" line-height: normal;"><h5 class="wb-stl-subtitle">Our website is coming soon</h5></div></div><div class="wb_cont_outer"></div><div class="wb_cont_bg"></div></div>
<div class="vbox wb_container" id="wb_main">
<div class="wb_cont_inner"><div class="wb_element" id="wb_element_instance2" style="width: 100%;">
<script type="text/javascript">
				$(function() {
					$("#wb_element_instance2").hide();
				});
			</script>
</div></div><div class="wb_cont_outer"></div><div class="wb_cont_bg"></div></div>
<div class="vbox wb_container" id="wb_footer">
<div class="wb_cont_inner" style="height: 80px;"><div class="wb_element" id="wb_element_instance3" style="text-align: center; width: 100%;"><div class="wb_footer"></div><script type="text/javascript">
			$(function() {
				var footer = $(".wb_footer");
				var html = (footer.html() + "").replace(/^\s+|\s+$/g, "");
				if (!html) {
					footer.parent().remove();
					footer = $("#wb_footer, #wb_footer .wb_cont_inner");
					footer.css({height: ""});
				}
			});
			</script></div></div><div class="wb_cont_outer"></div><div class="wb_cont_bg"></div></div><div class="wb_sbg"></div></div></body>
</html>
