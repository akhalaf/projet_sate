<!DOCTYPE HTML>
<html lang="en-US">
<head>
<!-- META DATA -->
<meta content="text/html;charset=utf-8" http-equiv="content-type"/>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<meta content="Djatawo - First Haitian Superhero" property="og:title"/>
<meta content="http://www.djatawo.com" property="og:url"/>
<meta content="http://djatawo.com/wp-content/uploads/2016/07/Djatawo-to-send.jpg" property="og:image"/>
<meta content="website" property="og:type"/>
<meta content="Djatawo" property="og:site_name"/>
<link href="https://djatawo.com/feed/" rel="alternate" title="Djatawo » Feed" type="application/rss+xml"/>
<link href="https://djatawo.com/comments/feed/" rel="alternate" title="Djatawo » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"https:\/\/djatawo.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.5.23"}};
			!function(a,b,c){function d(a){var c,d,e,f=b.createElement("canvas"),g=f.getContext&&f.getContext("2d"),h=String.fromCharCode;if(!g||!g.fillText)return!1;switch(g.textBaseline="top",g.font="600 32px Arial",a){case"flag":return g.fillText(h(55356,56806,55356,56826),0,0),f.toDataURL().length>3e3;case"diversity":return g.fillText(h(55356,57221),0,0),c=g.getImageData(16,16,1,1).data,d=c[0]+","+c[1]+","+c[2]+","+c[3],g.fillText(h(55356,57221,55356,57343),0,0),c=g.getImageData(16,16,1,1).data,e=c[0]+","+c[1]+","+c[2]+","+c[3],d!==e;case"simple":return g.fillText(h(55357,56835),0,0),0!==g.getImageData(16,16,1,1).data[0];case"unicode8":return g.fillText(h(55356,57135),0,0),0!==g.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://djatawo.com/wp-content/plugins/LayerSlider/static/css/layerslider.css?ver=5.4.0" id="layerslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&amp;subset=latin%2Clatin-ext" id="ls-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://djatawo.com/wp-content/plugins/revslider/rs-plugin/css/settings.css?ver=4.6.93" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
</style>
<link href="https://djatawo.com/wp-content/themes/inestast/style.css?ver=4.5.23" id="style-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='ie-css-css'  href='https://djatawo.com/wp-content/themes/inestast/assets/css/ie.css?ver=4.5.23' type='text/css' media='all' />
<![endif]-->
<link href="https://djatawo.com/wp-content/themes/inestast/assets/css/bootstrap.css?ver=4.5.23" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://djatawo.com/wp-content/themes/inestast/assets/css/linecons.css?ver=4.5.23" id="linecons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://djatawo.com/wp-content/themes/inestast/assets/css/font-awesome.min.css?ver=4.5.23" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://djatawo.com/wp-content/themes/inestast/assets/css/jquery.bxslider.css?ver=4.5.23" id="bxslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://djatawo.com/wp-content/themes/inestast/assets/css/animate.min.css?ver=4.5.23" id="animate-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans%3A400%2C600%2C300&amp;ver=4.5.23" id="Open+Sans-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://djatawo.com/wp-content/plugins/youtube-embed-plus/styles/ytprefs.min.css?ver=4.5.23" id="__EPYT__style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="__EPYT__style-inline-css" type="text/css">

                .epyt-gallery-thumb {
                        width: 33.333%;
                }
</style>
<script src="https://djatawo.com/wp-content/plugins/LayerSlider/static/js/greensock.js?ver=1.11.8" type="text/javascript"></script>
<script src="https://djatawo.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://djatawo.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://djatawo.com/wp-content/plugins/LayerSlider/static/js/layerslider.kreaturamedia.jquery.js?ver=5.4.0" type="text/javascript"></script>
<script src="https://djatawo.com/wp-content/plugins/LayerSlider/static/js/layerslider.transitions.js?ver=5.4.0" type="text/javascript"></script>
<script src="https://djatawo.com/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.tools.min.js?ver=4.6.93" type="text/javascript"></script>
<script src="https://djatawo.com/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js?ver=4.6.93" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _EPYT_ = {"ajaxurl":"https:\/\/djatawo.com\/wp-admin\/admin-ajax.php","security":"1a6ba9ddcc","gallery_scrolloffset":"20","eppathtoscripts":"https:\/\/djatawo.com\/wp-content\/plugins\/youtube-embed-plus\/scripts\/","epresponsiveselector":"[\"iframe.__youtube_prefs_widget__\"]","epdovol":"1","version":"11.2","evselector":"iframe.__youtube_prefs__[src], iframe[src*=\"youtube.com\/embed\/\"], iframe[src*=\"youtube-nocookie.com\/embed\/\"]"};
/* ]]> */
</script>
<script src="https://djatawo.com/wp-content/plugins/youtube-embed-plus/scripts/ytprefs.min.js?ver=4.5.23" type="text/javascript"></script>
<link href="https://djatawo.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://djatawo.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://djatawo.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.5.23" name="generator"/>
<script type="text/javascript">
			jQuery(document).ready(function() {
				// CUSTOM AJAX CONTENT LOADING FUNCTION
				var ajaxRevslider = function(obj) {
				
					// obj.type : Post Type
					// obj.id : ID of Content to Load
					// obj.aspectratio : The Aspect Ratio of the Container / Media
					// obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content
					
					var content = "";

					data = {};
					
					data.action = 'revslider_ajax_call_front';
					data.client_action = 'get_slider_html';
					data.token = '7aaf619c40';
					data.type = obj.type;
					data.id = obj.id;
					data.aspectratio = obj.aspectratio;
					
					// SYNC AJAX REQUEST
					jQuery.ajax({
						type:"post",
						url:"https://djatawo.com/wp-admin/admin-ajax.php",
						dataType: 'json',
						data:data,
						async:false,
						success: function(ret, textStatus, XMLHttpRequest) {
							if(ret.success == true)
								content = ret.data;								
						},
						error: function(e) {
							console.log(e);
						}
					});
					
					 // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
					 return content;						 
				};
				
				// CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
				var ajaxRemoveRevslider = function(obj) {
					return jQuery(obj.selector+" .rev_slider").revkill();
				};

				// EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
				var extendessential = setInterval(function() {
					if (jQuery.fn.tpessential != undefined) {
						clearInterval(extendessential);
						if(typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
							jQuery.fn.tpessential.defaults.ajaxTypes.push({type:"revslider",func:ajaxRevslider,killfunc:ajaxRemoveRevslider,openAnimationSpeed:0.3});   
							// type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
							// func: the Function Name which is Called once the Item with the Post Type has been clicked
							// killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
							// openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
						}
					}
				},30);
			});
		</script>
<!-- CUSTOM STYLE -->
<style type="text/css">
h1 {
 font-size:40px;
 color:#343434;
}
h2 {
 font-size:34px;
 color:#343434;
}
h3 {
 font-size:30px;
 color:#343434;
}
h4 {
 font-size:24px;
 color:#343434;
}
h5 {
 font-size:20px;
 color:#343434;
}
h6 {
 font-size:15px;
 color:#343434;
}
.general-color, #desktop-menu .active a, .infobox-2 header div i, .social-1:hover,  .text-edit .social-1:hover, #portfolio-categories ul li a:hover, .readmore a:hover, .post header h1 a:hover, .post header h2 a:hover, .post header h3 a:hover, .post header h4 a:hover, .post header h5 a:hover, .post header h6 a:hover, .widget ul li a:hover, .post header p a:hover, #tags a:hover, #list-comments li a:hover, #post-nav a:hover, .media .rslides_nav:hover, #desktop-menu .sub-menu .current_menu_item a, .text-edit a, .comment-body a, .infobox-1 i, .number-container .number, .contact-box:hover i, #portfolio-categories .current-cat a, #desktop-menu > .current-menu-item > a, #mobile-menu .current-menu-item > a, .portfolio-slideshow .rslides_nav:hover {
		color:#dd9933 !important;
 }.general-bg, #submit-button-contact, #comment-form input[type='submit']:hover, .text-edit thead th, .comment-body thead th, #desktop-menu .sub-menu li:hover > a, #desktop-menu .sub-menu .current-menu-item > a, #home-button:hover { 
		background-color:#dd9933 !important;
 }.general-border, .text-edit blockquote, #contact-form-script input[type='text']:focus, #contact-form-script textarea:focus, .style-input:focus, #portfolio-categories .current-cat a, #portfolio-categories .current-cat:hover a { 
		border-color:#dd9933 !important;

		}

		.active-tab {
			border-top-color:#dd9933 !important;

		}

		body {

	color:#343434;
	font:normal 12px Open Sans;
}h1,h2,h3,h4,h5,h6 {

	font-family:Open Sans;
}#desktop-menu > li > a {

	color:#343434 !important;
	font-size:12px;
	font-style:normal;
	font-family:Open Sans;
}#desktop-menu > li > a:hover, #desktop-menu .active a,#desktop-menu > li:hover > a, #desktop-menu .current-menu-item > a {

	color:#2ac4ea !important;
	font-size:12px;
	font-style:normal;
	font-family:Open Sans;
}#copyright {

	color:#fff !important;
	font-size:13px;
	font-style:normal;
	font-family:Open Sans;
}#nav-main {

	background-color:#fff !important;
}#foot-page {

	background-color:#000000 !important;
} #desktop-menu .sub-menu li:hover > a, #desktop-menu .sub-menu .current-menu-item > a {

	background-color:#2ac4ea !important;
}#desktop-menu .sub-menu, #mobile-menu {

	background-color:#222 !important;
}#desktop-menu .sub-menu li a  {

	color:#fff !important;
	font-size:12px;
	font-style:normal;
	font-family:Open Sans;
}#desktop-menu .sub-menu li:hover > a, #desktop-menu .sub-menu .current-menu-item > a   {

	color:#2ac4ea !important;
	font-size:12px;
	font-style:normal;
	font-family:Open Sans;
}

</style>
<!-- END CUSTOM STYLE -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
		<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<![endif]-->
<!--[if IE 7]>
		<script src="https://djatawo.com/wp-content/themes/inestast/assets/js/lte-ie7.js"></script>
	<![endif]-->
<!--[if lt IE 9]>
		<script src="https://djatawo.com/wp-content/themes/inestast/assets/js/html5shiv.js"></script>
		<script src="https://djatawo.com/wp-content/themes/inestast/assets/js/respond.js"></script>
	<![endif]--> <script src="https://djatawo.com/wp-content/themes/inestast/assets/js/mobile_var.js"></script>
<title>Djatawo -  Page not found</title>
<link href="http://djatawo.com/wp-content/uploads/2015/12/djatawofavicon.png" rel="shortcut icon"/>
<meta content="Djatawo, Created by premiere comics artist Anthony Louis-Jeune, is a hero for today's troubled times and his mission is to bring hope to the Haitian population and build awareness of the problems affecting his beloved country.  Haitian culture is promoted through his heroic adventures which raise awareness of the problems affecting all Haitians:  Education, environmental issues and pervasive violence." name="description"/><style type="text/css">#nav-main {
    background-color: #000 !important;
    border-bottom: 0px solid #eeeeee; */
}

#desktop-menu .active a {
    color: #D2A337 !important;
}

#desktop-menu > li > a:hover {
    color: #D2A337 !important;
}

#desktop-menu > li > a {
    color: #FFFFFF !important;
}

.text-edit p, .comment-body p, .widget p {
    font-size: 14px;
}

#portfolio {
    padding-bottom: 0px;
    padding-top: 0px;
}

.infobox-2 header h4 {
    color: #ffffff;
}

#name {
    padding-bottom: 0px;
}

#stay-tuned {
    padding-bottom: 0px;
}

#contact-info {
    color: white;
}

#youtube {
    padding-top: 20px;
    padding-bottom: 100px;
}</style> <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
</head>
<body class="error404"> <!-- HEADER -->
<!-- MAIN MENU --> <div class="sticky-menu" id="nav-main"> <div class="container"> <div class="row"> <div class="col-md-12"> <div id="logo-container"><div class="tb"><div class="tb-cell"> <a href="https://djatawo.com"> <img alt="Djatawo" class="img-responsive" data-at2x="http://djatawo.com/wp-content/uploads/2016/07/DJATAWO-LOGO5.png" id="logo" src="http://djatawo.com/wp-content/uploads/2016/07/DJATAWO-LOGO5.png" title="logo"/> </a> </div></div></div> <nav class="menu_container"><ul class="visible-md visible-lg" id="desktop-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2809" id="menu-item-2809"><a href="https://djatawo.com/#header-main">HOME</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2810" id="menu-item-2810"><a href="https://djatawo.com/#powers">POWERS</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2866" id="menu-item-2866"><a href="https://djatawo.com/#djatawo">ABOUT</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-onepage menu-item-2818" id="menu-item-2818"><a href="https://djatawo.com/#portfolio">GALLERY</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2811" id="menu-item-2811"><a href="https://djatawo.com/#contact">CONTACT</a></li>
</ul></nav> <div class="hidden-md hidden-lg" id="mobile-menu-button"> <i class="fa fa-bars"></i> </div> <div class="clear"></div> </div> </div> </div> <!-- MOBILE MENU --> <nav id="mobile-menu"> <div class="container hidden-lg hidden-md"> <div class="row"> <div class="col-md-12"> <div class="menu-menu-2-container"><ul class="" id="menu-menu-2"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2809"><a href="https://djatawo.com/#header-main">HOME</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2810"><a href="https://djatawo.com/#powers">POWERS</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2866"><a href="https://djatawo.com/#djatawo">ABOUT</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-onepage menu-item-2818"><a href="https://djatawo.com/#portfolio">GALLERY</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2811"><a href="https://djatawo.com/#contact">CONTACT</a></li>
</ul></div> </div> </div> </div> </nav> </div>
<div id="error"> <div>
<h1>ooops... error 404</h1>
<p class="error-text">We`re sorry, but the page you are looking for doesn`t exist.</p>
<a class="button-style-3" href="https://djatawo.com">Go to homepage</a>
</div>
</div>
<!-- ===== FOOTER ===== -->
<footer id="foot-page">
<div class="container">
<div class="row">
<div class="col-md-12">
<p id="copyright"><img src="http://djatawo.com/kr/wp-content/uploads/2016/07/DJATAWO-LOGO5.png" style="max-width: 250px;"/>© Djatawo and all other characters, with all the elements represented are creations of A. Anthony Louis-Jeune. All rights reserved.Developed by <a href="http://www.modularcollective.com/" target="_blank">Modular</a>.</p>
</div>
</div>
</div>
</footer>
<script src="https://djatawo.com/wp-content/themes/inestast/assets/js/plugins.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://djatawo.com/wp-content/themes/inestast/assets/js/scripts.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://djatawo.com/wp-content/themes/inestast/assets/js/sticky-menu.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://djatawo.com/wp-content/plugins/youtube-embed-plus/scripts/fitvids.min.js?ver=4.5.23" type="text/javascript"></script>
<script src="https://djatawo.com/wp-includes/js/wp-embed.min.js?ver=4.5.23" type="text/javascript"></script>
<script type="text/javascript">
jQuery(window).load(function () {
	jQuery(".parallax-1775").css("background-attachment", "fixed");
	jQuery(".parallax-1775").parallax("50%", "0.3");
	jQuery(".parallax-2434").css("background-attachment", "fixed");
	jQuery(".parallax-2434").parallax("50%", "0.3");
	jQuery(".parallax-2038").css("background-attachment", "fixed");
	jQuery(".parallax-2038").parallax("50%", "0.3");
	jQuery("#bg1").css("background-attachment", "fixed");
	jQuery("#bg1").parallax("50%", "0.4");
});

</script>
</body>
</html>