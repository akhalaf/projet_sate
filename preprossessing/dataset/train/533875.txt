<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<title>Page not found – Faces of California</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://facesofcalifornia.com/feed/" rel="alternate" title="Faces of California » Feed" type="application/rss+xml"/>
<link href="https://facesofcalifornia.com/comments/feed/" rel="alternate" title="Faces of California » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/facesofcalifornia.com\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://facesofcalifornia.com/wp-includes/css/dist/block-library/style.min.css" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i|Fira%20Sans:300,300i,400,400i,500,500i,600,600i,700,700i" id="infinity-news-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://facesofcalifornia.com/wp-content/themes/infinity-news/assets/lib/ionicons/css/ionicons.min.css" id="ionicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://facesofcalifornia.com/wp-content/themes/infinity-news/assets/lib/slick/css/slick.min.css" id="slick-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://facesofcalifornia.com/wp-content/themes/infinity-news/assets/lib/magnific-popup/magnific-popup.css" id="magnific-popup-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://facesofcalifornia.com/wp-content/themes/infinity-news/assets/lib/sidr/css/jquery.sidr.dark.css" id="sidr-nav-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://facesofcalifornia.com/wp-content/themes/infinity-news/assets/lib/aos/css/aos.css" id="aos-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://facesofcalifornia.com/wp-content/themes/infinity-news/style.css" id="infinity-news-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="infinity-news-style-inline-css" type="text/css">
.block-title-wrapper .block-title-bg, .block-title-wrapper .title-controls-bg{background: #f5f5f5}
</style>
<script id="jquery-core-js" src="https://facesofcalifornia.com/wp-includes/js/jquery/jquery.js" type="text/javascript"></script>
<link href="https://facesofcalifornia.com/wp-json/" rel="https://api.w.org/"/> <style type="text/css">
					.site-title,
			.site-description {
				position: absolute;
				clip: rect(1px, 1px, 1px, 1px);
			}
				</style>
<link href="https://facesofcalifornia.com/wp-content/uploads/2020/11/cropped-foc-logo-lg-sq-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://facesofcalifornia.com/wp-content/uploads/2020/11/cropped-foc-logo-lg-sq-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://facesofcalifornia.com/wp-content/uploads/2020/11/cropped-foc-logo-lg-sq-180x180.png" rel="apple-touch-icon"/>
<meta content="https://facesofcalifornia.com/wp-content/uploads/2020/11/cropped-foc-logo-lg-sq-270x270.png" name="msapplication-TileImage"/>
</head>
<body class="error404 wp-custom-logo hfeed no-sidebar no-offcanvas">
<div class="preloader">
<div class="preloader-background"></div>
<div class="preloader-status-wrapper">
<span>
<span class="loader-circle loader-animation"></span>
<span class="loader-circle loader-animation"></span>
<span class="loader-circle loader-animation"></span>
</span>
<div class="preloader-status">
<span>
<span class="loader-circle loader-animation"></span>
<span class="loader-circle loader-animation"></span>
<span class="loader-circle loader-animation"></span>
</span>
</div>
</div>
</div>
<div class="site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header " id="masthead">
<div class="site-middlebar flex-block twp-align-center">
<div class="wrapper">
<div class="middlebar-items flex-block-items">
<div class="site-branding">
<a class="custom-logo-link" href="https://facesofcalifornia.com/" rel="home"><img alt="Faces of California" class="custom-logo" height="800" sizes="(max-width: 800px) 100vw, 800px" src="https://facesofcalifornia.com/wp-content/uploads/2020/11/foc-logo-lg-sq.png" srcset="https://facesofcalifornia.com/wp-content/uploads/2020/11/foc-logo-lg-sq.png 800w, https://facesofcalifornia.com/wp-content/uploads/2020/11/foc-logo-lg-sq-300x300.png 300w, https://facesofcalifornia.com/wp-content/uploads/2020/11/foc-logo-lg-sq-150x150.png 150w, https://facesofcalifornia.com/wp-content/uploads/2020/11/foc-logo-lg-sq-768x768.png 768w" width="800"/></a> <a class="custom-logo-link custom-logo-link-dark" href="https://facesofcalifornia.com/" rel="home">
<img alt="Faces of California" class="custom-logo" src="https://facesofcalifornia.com/wp-content/uploads/2020/11/foc-logo-lg-lt.png"/>
</a>
<p class="site-title">
<a href="https://facesofcalifornia.com/" rel="home">Faces of California</a>
</p>
<p class="site-description">
<span>California News and Information</span>
</p>
</div><!-- .site-branding -->
</div>
</div>
</div>
<nav class="main-navigation" id="site-navigation">
<div class="wrapper">
<div class="navigation-area">
<div aria-controls="primary-menu" aria-expanded="false" class="toggle-menu">
<a class="offcanvas-toggle" href="#">
<div class="trigger-icon">
<span class="menu-label">
                                    Menu                                </span>
</div>
</a>
</div>
<div class="menu" id="primary-menu"></div>
<div class="nav-right">
<div class="twp-color-scheme">
<div id="night-mode">
<a class="colour-switcher-btn" href="#" role="button">
<span class="twp-toggle-tooltip"><span class="twp-tooltip-wrapper"></span></span> <i class=""></i>
</a>
</div>
</div>
</div>
</div>
</div>
</nav><!-- #site-navigation -->
</header><!-- #masthead -->
<div class="twp-inner-banner">
<div class="wrapper">
<nav aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs" itemprop="breadcrumb" role="navigation"><ul class="trail-items" itemscope="" itemtype="http://schema.org/BreadcrumbList"><meta content="2" name="numberOfItems"/><meta content="Ascending" name="itemListOrder"/><li class="trail-item trail-begin" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a href="https://facesofcalifornia.com/" itemprop="item" rel="home"><span itemprop="name">Home</span></a><meta content="1" itemprop="position"/></li><li class="trail-item trail-end" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a href="https://facesofcalifornia.com" itemprop="item"><span itemprop="name">404 Not Found</span></a><meta content="2" itemprop="position"/></li></ul></nav>
</div>
</div>
<div class="site-content" id="content">
<div class="content-area" id="primary">
<main class="site-main" id="main">
<section class="error-404 not-found">
<header class="page-header">
<h1 class="page-title"><i class="ion ion-ios-warning"></i> Oops! That page can’t be found.</h1>
</header><!-- .page-header -->
<div class="page-content">
<p>It looks like nothing was found at this location. Maybe try one of the links below or a search?</p>
</div><!-- .page-content -->
</section><!-- .error-404 -->
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- #content -->
<div id="offcanvas-menu">
<div class="close-offcanvas-menu offcanvas-item">
<a class="skip-link-offcanvas-start" href="javascript:void(0)"></a>
<a class="offcanvas-close" href="javascript:void(0)">
<span>
               Close            </span>
</a>
</div>
<div class="offcanvas-item">
<div class="search-bar-offcanvas">
<form action="https://facesofcalifornia.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form> </div>
</div>
<a class="skip-link-offcanvas-end" href="javascript:void(0)"></a>
</div>
<div class="drawer-handle">
<div class="drawer-handle-open">
<i class="ion ion-ios-add"></i>
</div>
</div>
<div class="recommendation-panel-content">
<div class="drawer-handle-close">
<i class="ion ion-ios-close"></i>
</div>
<div class="recommendation-panel-slider">
<div class="wrapper">
<div class="drawer-carousel" data-slick='{"autoplay": true, "dots": false, "arrows": true, "rtl": false}'>
<div class="slide-item">
<article class="story-list">
<div class="post-panel">
<div class="post-thumb">
<a class="data-bg data-bg-xs" data-background="https://facesofcalifornia.com/wp-content/uploads/2021/01/l-a-county-not-yet-offering-covid-19-vaccines-to-those-65-and-older-as-many-health-care-workers-still-not-vaccinated-300x195.jpg" href="https://facesofcalifornia.com/l-a-county-not-yet-offering-covid-19-vaccines-to-those-65-and-older-as-many-health-care-workers-still-not-vaccinated/"></a>
</div>
<div class="entry-content">
<h3 class="entry-title entry-title-small">
<a href="https://facesofcalifornia.com/l-a-county-not-yet-offering-covid-19-vaccines-to-those-65-and-older-as-many-health-care-workers-still-not-vaccinated/">L.A. County not yet offering COVID-19 vaccines to those 65 and older as many health care workers still not vaccinated</a>
</h3>
</div>
</div>
</article>
</div>
<div class="slide-item">
<article class="story-list">
<div class="post-panel">
<div class="post-thumb">
<a class="data-bg data-bg-xs" data-background="https://facesofcalifornia.com/wp-content/uploads/2021/01/trump-is-1st-u-s-president-impeached-twice-after-bipartisan-house-vote-on-incitement-of-insurrection-charge-300x208.jpg" href="https://facesofcalifornia.com/trump-is-1st-u-s-president-impeached-twice-after-bipartisan-house-vote-on-incitement-of-insurrection-charge/"></a>
</div>
<div class="entry-content">
<h3 class="entry-title entry-title-small">
<a href="https://facesofcalifornia.com/trump-is-1st-u-s-president-impeached-twice-after-bipartisan-house-vote-on-incitement-of-insurrection-charge/">Trump is 1st U.S. president impeached twice after bipartisan House vote on incitement of insurrection charge</a>
</h3>
</div>
</div>
</article>
</div>
<div class="slide-item">
<article class="story-list">
<div class="post-panel">
<div class="post-thumb">
<a class="data-bg data-bg-xs" data-background="https://facesofcalifornia.com/wp-content/uploads/2021/01/in-major-expansion-california-allows-everyone-65-and-older-to-get-covid-19-vaccine-300x200.jpg" href="https://facesofcalifornia.com/in-major-expansion-california-allows-everyone-65-and-older-to-get-covid-19-vaccine/"></a>
</div>
<div class="entry-content">
<h3 class="entry-title entry-title-small">
<a href="https://facesofcalifornia.com/in-major-expansion-california-allows-everyone-65-and-older-to-get-covid-19-vaccine/">In major expansion, California allows everyone 65 and older to get COVID-19 vaccine</a>
</h3>
</div>
</div>
</article>
</div>
<div class="slide-item">
<article class="story-list">
<div class="post-panel">
<div class="post-thumb">
<a class="data-bg data-bg-xs" data-background="https://facesofcalifornia.com/wp-content/uploads/2021/01/all-passengers-flying-to-u-s-will-have-to-show-proof-of-negative-coronavirus-test-300x200.jpg" href="https://facesofcalifornia.com/all-passengers-flying-to-u-s-will-have-to-show-proof-of-negative-coronavirus-test/"></a>
</div>
<div class="entry-content">
<h3 class="entry-title entry-title-small">
<a href="https://facesofcalifornia.com/all-passengers-flying-to-u-s-will-have-to-show-proof-of-negative-coronavirus-test/">All passengers flying to U.S. will have to show proof of negative coronavirus test</a>
</h3>
</div>
</div>
</article>
</div>
<div class="slide-item">
<article class="story-list">
<div class="post-panel">
<div class="post-thumb">
<a class="data-bg data-bg-xs" data-background="https://facesofcalifornia.com/wp-content/uploads/2021/01/top-10-real-estate-agents-in-arizona-for-2021-300x125.png" href="https://facesofcalifornia.com/top-10-real-estate-agents-in-arizona-for-2021/"></a>
</div>
<div class="entry-content">
<h3 class="entry-title entry-title-small">
<a href="https://facesofcalifornia.com/top-10-real-estate-agents-in-arizona-for-2021/">Top 10 Real Estate Agents in Arizona for 2021</a>
</h3>
</div>
</div>
</article>
</div>
<div class="slide-item">
<article class="story-list">
<div class="post-panel">
<div class="post-thumb">
<a class="data-bg data-bg-xs" data-background="https://facesofcalifornia.com/wp-content/uploads/2021/01/top-10-real-estate-agents-in-arkansas-for-2021-300x125.png" href="https://facesofcalifornia.com/top-10-real-estate-agents-in-arkansas-for-2021/"></a>
</div>
<div class="entry-content">
<h3 class="entry-title entry-title-small">
<a href="https://facesofcalifornia.com/top-10-real-estate-agents-in-arkansas-for-2021/">Top 10 Real Estate Agents in Arkansas for 2021</a>
</h3>
</div>
</div>
</article>
</div>
<div class="slide-item">
<article class="story-list">
<div class="post-panel">
<div class="post-thumb">
<a class="data-bg data-bg-xs" data-background="https://facesofcalifornia.com/wp-content/uploads/2021/01/top-10-real-estate-agents-in-california-for-2021-300x125.png" href="https://facesofcalifornia.com/top-10-real-estate-agents-in-california-for-2021/"></a>
</div>
<div class="entry-content">
<h3 class="entry-title entry-title-small">
<a href="https://facesofcalifornia.com/top-10-real-estate-agents-in-california-for-2021/">Top 10 Real Estate Agents in California for 2021</a>
</h3>
</div>
</div>
</article>
</div>
<div class="slide-item">
<article class="story-list">
<div class="post-panel">
<div class="post-thumb">
<a class="data-bg data-bg-xs" data-background="https://facesofcalifornia.com/wp-content/uploads/2021/01/top-10-real-estate-agents-in-colorado-for-2021-300x125.png" href="https://facesofcalifornia.com/top-10-real-estate-agents-in-colorado-for-2021/"></a>
</div>
<div class="entry-content">
<h3 class="entry-title entry-title-small">
<a href="https://facesofcalifornia.com/top-10-real-estate-agents-in-colorado-for-2021/">Top 10 Real Estate Agents in Colorado for 2021</a>
</h3>
</div>
</div>
</article>
</div>
<div class="slide-item">
<article class="story-list">
<div class="post-panel">
<div class="post-thumb">
<a class="data-bg data-bg-xs" data-background="https://facesofcalifornia.com/wp-content/uploads/2021/01/top-10-real-estate-agents-in-connecticut-for-2021.png" href="https://facesofcalifornia.com/top-10-real-estate-agents-in-connecticut-for-2021/"></a>
</div>
<div class="entry-content">
<h3 class="entry-title entry-title-small">
<a href="https://facesofcalifornia.com/top-10-real-estate-agents-in-connecticut-for-2021/">Top 10 Real Estate Agents in Connecticut for 2021</a>
</h3>
</div>
</div>
</article>
</div>
<div class="slide-item">
<article class="story-list">
<div class="post-panel">
<div class="post-thumb">
<a class="data-bg data-bg-xs" data-background="https://facesofcalifornia.com/wp-content/uploads/2021/01/top-10-real-estate-agents-in-delaware-for-2021.png" href="https://facesofcalifornia.com/top-10-real-estate-agents-in-delaware-for-2021/"></a>
</div>
<div class="entry-content">
<h3 class="entry-title entry-title-small">
<a href="https://facesofcalifornia.com/top-10-real-estate-agents-in-delaware-for-2021/">Top 10 Real Estate Agents in Delaware for 2021</a>
</h3>
</div>
</div>
</article>
</div>
<div class="slide-item">
<article class="story-list">
<div class="post-panel">
<div class="post-thumb">
<a class="data-bg data-bg-xs" data-background="https://facesofcalifornia.com/wp-content/uploads/2021/01/top-10-real-estate-agents-in-florida-for-2021-300x125.png" href="https://facesofcalifornia.com/top-10-real-estate-agents-in-florida-for-2021/"></a>
</div>
<div class="entry-content">
<h3 class="entry-title entry-title-small">
<a href="https://facesofcalifornia.com/top-10-real-estate-agents-in-florida-for-2021/">Top 10 Real Estate Agents in Florida for 2021</a>
</h3>
</div>
</div>
</article>
</div>
<div class="slide-item">
<article class="story-list">
<div class="post-panel">
<div class="post-thumb">
<a class="data-bg data-bg-xs" data-background="https://facesofcalifornia.com/wp-content/uploads/2021/01/top-10-real-estate-agents-in-georgia-for-2021.png" href="https://facesofcalifornia.com/top-10-real-estate-agents-in-georgia-for-2021/"></a>
</div>
<div class="entry-content">
<h3 class="entry-title entry-title-small">
<a href="https://facesofcalifornia.com/top-10-real-estate-agents-in-georgia-for-2021/">Top 10 Real Estate Agents in Georgia for 2021</a>
</h3>
</div>
</div>
</article>
</div>
</div>
</div>
</div>
</div>
<footer class="site-footer" id="colophon">
<div class="footer-top flex-block">
<div class="wrapper">
<div class="footer-items flex-block-items">
<div class="footer-right">
<div class="footer-items-right search-bar">
<form action="https://facesofcalifornia.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form> </div>
<div class="footer-items-right scroll-up">
<i class="ion ion-ios-arrow-round-up"></i>
</div>
</div>
</div>
</div>
</div>
<div class="footer-bottom">
<div class="site-copyright">
<div class="wrapper">
<div class="site-info">
                    Copyright All rights reserved                    <span class="sep"> | </span>
                    Theme: <strong>Infinity News</strong> by <a href="https://www.themeinwp.com/">Themeinwp</a>.                </div>
</div><!-- .site-info -->
</div>
</div>
</footer><!-- #colophon -->
</div><!-- #page -->
<script id="infinity-news-skip-link-focus-fix-js" src="https://facesofcalifornia.com/wp-content/themes/infinity-news/assets/lib/default/js/skip-link-focus-fix.js" type="text/javascript"></script>
<script id="jquery-slick-js" src="https://facesofcalifornia.com/wp-content/themes/infinity-news/assets/lib/slick/js/slick.min.js" type="text/javascript"></script>
<script id="jquery-magnific-popup-js" src="https://facesofcalifornia.com/wp-content/themes/infinity-news/assets/lib/magnific-popup/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script id="jquery-sidr-js" src="https://facesofcalifornia.com/wp-content/themes/infinity-news/assets/lib/sidr/js/jquery.sidr.min.js" type="text/javascript"></script>
<script id="theiaStickySidebar-js" src="https://facesofcalifornia.com/wp-content/themes/infinity-news/assets/lib/theiaStickySidebar/theia-sticky-sidebar.min.js" type="text/javascript"></script>
<script id="match-height-js" src="https://facesofcalifornia.com/wp-content/themes/infinity-news/assets/lib/jquery-match-height/js/jquery.matchHeight.min.js" type="text/javascript"></script>
<script id="aos-js" src="https://facesofcalifornia.com/wp-content/themes/infinity-news/assets/lib/aos/js/aos.js" type="text/javascript"></script>
<script id="infinity-news-custom-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var infinity_news_custom_script = {"daymod":"Light Mode","nightmod":"Dark Mode"};
/* ]]> */
</script>
<script id="infinity-news-custom-script-js" src="https://facesofcalifornia.com/wp-content/themes/infinity-news/assets/lib/twp/js/script.js" type="text/javascript"></script>
<script id="infinity-news-ajax-js-extra" type="text/javascript">
/* <![CDATA[ */
var infinity_news_ajax = {"ajax_url":"https:\/\/facesofcalifornia.com\/wp-admin\/admin-ajax.php","loadmore":"Load More","nomore":"No More Posts","loading":"Loading..."};
/* ]]> */
</script>
<script id="infinity-news-ajax-js" src="https://facesofcalifornia.com/wp-content/themes/infinity-news/assets/lib/twp/js/ajax.js" type="text/javascript"></script>
<script id="wp-embed-js" src="https://facesofcalifornia.com/wp-includes/js/wp-embed.min.js" type="text/javascript"></script>
</body>
</html>
