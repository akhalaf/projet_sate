<!DOCTYPE html>
<html lang="zh-Hans">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="ie=edge" http-equiv="X-UA-Compatible"/>
<link href="https://www.15201.com/zb_users/theme/15201/layui/css/layui.css" rel="stylesheet"/>
<link href="https://www.15201.com/zb_users/theme/15201/style/style.css" rel="stylesheet"/>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://www.15201.com/zb_users/theme/15201/layui/layui.js"></script>
<script src="https://www.15201.com/zb_system/script/zblogphp.js" type="text/javascript"></script>
<script src="https://www.15201.com/zb_system/script/c_html_js_add.php" type="text/javascript"></script>
<link href="https://www.15201.com/zb_users/plugin/mochu_button/style.css" rel="stylesheet"/></head>
<body> <header class="header">
<div class="logo">
<img height="60px" src="https://www.15201.com/zb_users/theme/15201/style/images/logo.png" width="60px"/>
</div>
<ul class="nav">
<li>
<a href="https://www.15201.com/">
<i class="layui-icon layui-icon-home" style="font-size: 30px; color: #1E9FFF;"></i>
<br/>
<span>写作台</span>
</a>
</li>
<li>
<a href="https://thoughts.teambition.com/sharespace/5f6c42e61db108001643f553/docs/5f6c43454574aa00161c9d7d">
<i class="layui-icon layui-icon-survey" style="font-size: 30px; color: #1E9FFF;"></i>
<br/>
<span>帮助支持</span>
</a>
</li>
<li>
<a href="https://www.15201.com/?login">
<i class="layui-icon layui-icon-username" style="font-size: 30px; color: #1E9FFF;"></i>
<br/>
<span>登陆</span>
</a>
</li>
</ul>
</header><div class="main">
<div class="layui-fluid">
<div style="text-align: center;">
<div class="layui-inline layui-bg-gray error layui-text">
<h3 style="margin: 15px;">404错误 - 找不到此页面！</h3>
<p>抱歉，当前页面不存在！返回 <a href="https://www.15201.com/">网站首页</a> 查看更多信息..</p>
</div>
</div>
</div>
<style>
.error{
	height: 100px;
    width: 835px;
    margin-left: 20px;
    box-shadow: rgba(0, 0, 0, 0.08) 0px 1px 7px 0px;
    border-radius: 8px;
    border-left: 6px solid rgb(255, 122, 122);
	text-align: left;
    margin: 15px;
	
}
.error h3 + p{
	margin: 15px;
}
</style>
</div>
<div class="footer">
<script>var _hmt = _hmt || [];(function() {  var hm = document.createElement("script");  hm.src = "https://hm.baidu.com/hm.js?6a32f937ea459e4d110390181372c686";  var s = document.getElementsByTagName("script")[0];   s.parentNode.insertBefore(hm, s);})();</script> © 2003-2021 15201.COM 版权所有 15201专注于内容创作智能创新 ┊ ICP备案: <a href="http://www.beian.miit.gov.cn/" rel="nofollow" style="color: #777;" target="_blank">鲁ICP备18044594号-2</a></div>
<script>
    var mh = $('.main').height() + 150;
    var lh = $(window).height();
    if(mh < lh){
        $('.footer').addClass('footer-f');
    }
</script>
<link href="https://www.15201.com/zb_users/plugin/mochu_us/style/mochu_us.css" rel="stylesheet"/><script src="https://www.15201.com/zb_users/plugin/mochu_us/style/mochu_us.js?r=v3.7"></script>
<div class="mochu_button_aside-nav" id="mochu_button_aside-nav">
<label class="mochu_button_aside-menu" style="background-color:#0093d3" title="按住拖动">小助手</label>
<a class="mochu_button_menu-item mochu_button_menu-first" href="tencent://message/?uin=193650808&amp;Site=QQ咨询&amp;Menu=yes" style="background-color:#0093d3" target="_blank" title="合作咨询 意见反馈  我们会尽快和你联系">龙猫</a>
<a class="mochu_button_menu-item mochu_button_menu-second" href="点击链接加入群聊【AI智能写作交流群[陆]】：https://jq.qq.com/?_wv=1027&amp;k=fobDjfh7" style="background-color:#0093d3" target="_blank" title="加QQ群  加入智能创作交流群进行讨论">QQ群</a>
<a class="mochu_button_menu-item mochu_button_menu-third" href="https://www.15201.com/?login" style="background-color:#0093d3" target="_blank" title="登录 OR 注册 本系统">登录</a><a class="mochu_button_menu-item mochu_button_menu-fourth" href="https://www.15201.com/" style="background-color:#0093d3" target="_blank" title="返回首页">Home</a>
</div>
<script :="" src="https://www.15201.com/zb_users/plugin/mochu_button/script.js?ter=63"></script></body>
</html>