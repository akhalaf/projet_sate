<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- Favicon icon -->
<link href="https://www.colfecar.net/../assets/images/favicon.png" rel="icon" sizes="16x16" type="image/png"/>
<title>Colfecar - Lo sentimos esta pagina no existe</title>
<!-- Custom CSS -->
<link href="https://www.colfecar.net/dist/css/style.min.css" rel="stylesheet"/>
<!-- page css -->
<link href="https://www.colfecar.net/dist/css/pages/error-pages.css" rel="stylesheet"/>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="skin-blue fixed-layout">
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<section class="error-page" id="wrapper">
<div class="error-box">
<div class="error-body text-center">
<h1>404</h1>
<h3 class="text-uppercase">No encontramos lo que buscas!</h3>
<p class="text-muted m-t-30 m-b-30">Parece que buscas la pagina principal</p>
<a class="btn btn-info btn-rounded waves-effect waves-light m-b-40" href="https://www.colfecar.net/home">Regresar al inicio</a> </div>
</div>
</section>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="https://www.colfecar.net/../assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="https://www.colfecar.net/../assets/node_modules/popper/popper.min.js"></script>
<script src="https://www.colfecar.net/../assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<!--Wave Effects -->
<script src="https://www.colfecar.net/dist/js/waves.js"></script>
</body>
</html>