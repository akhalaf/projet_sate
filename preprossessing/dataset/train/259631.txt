<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<title>Page not found – Britain Outdoors Travel Blog</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.britainoutdoors.co.uk/feed/" rel="alternate" title="Britain Outdoors Travel Blog » Feed" type="application/rss+xml"/>
<link href="https://www.britainoutdoors.co.uk/comments/feed/" rel="alternate" title="Britain Outdoors Travel Blog » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.britainoutdoors.co.uk\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.britainoutdoors.co.uk/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.britainoutdoors.co.uk/wp-content/plugins/cookie-law-info/public/css/cookie-law-info-public.css?ver=1.8.8" id="cookie-law-info-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.britainoutdoors.co.uk/wp-content/plugins/cookie-law-info/public/css/cookie-law-info-gdpr.css?ver=1.8.8" id="cookie-law-info-gdpr-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.britainoutdoors.co.uk/wp-content/themes/generatepress/css/unsemantic-grid.min.css?ver=2.4.2" id="generate-style-grid-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.britainoutdoors.co.uk/wp-content/themes/generatepress/style.min.css?ver=2.4.2" id="generate-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="generate-style-inline-css" type="text/css">
.blog footer.entry-meta, .archive footer.entry-meta {display:none;}
body{background-color:#efefef;color:#3a3a3a;}a, a:visited{color:#e60000;}a:hover, a:focus, a:active{color:#000000;}body .grid-container{max-width:1100px;}.wp-block-group__inner-container{max-width:1100px;margin-left:auto;margin-right:auto;}body, button, input, select, textarea{font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";}.entry-content > [class*="wp-block-"]:not(:last-child){margin-bottom:1.5em;}.main-navigation .main-nav ul ul li a{font-size:14px;}@media (max-width:768px){.main-title{font-size:30px;}h1{font-size:30px;}h2{font-size:25px;}}.top-bar{background-color:#636363;color:#ffffff;}.top-bar a,.top-bar a:visited{color:#ffffff;}.top-bar a:hover{color:#303030;}.site-header{background-color:#ffffff;color:#3a3a3a;}.site-header a,.site-header a:visited{color:#3a3a3a;}.main-title a,.main-title a:hover,.main-title a:visited{color:#222222;}.site-description{color:#757575;}.main-navigation,.main-navigation ul ul{background-color:#082040;}.main-navigation .main-nav ul li a,.menu-toggle{color:#ffffff;}.main-navigation .main-nav ul li:hover > a,.main-navigation .main-nav ul li:focus > a, .main-navigation .main-nav ul li.sfHover > a{color:#ffffff;background-color:#3f3f3f;}button.menu-toggle:hover,button.menu-toggle:focus,.main-navigation .mobile-bar-items a,.main-navigation .mobile-bar-items a:hover,.main-navigation .mobile-bar-items a:focus{color:#ffffff;}.main-navigation .main-nav ul li[class*="current-menu-"] > a{color:#ffffff;background-color:#3f3f3f;}.main-navigation .main-nav ul li[class*="current-menu-"] > a:hover,.main-navigation .main-nav ul li[class*="current-menu-"].sfHover > a{color:#ffffff;background-color:#3f3f3f;}.navigation-search input[type="search"],.navigation-search input[type="search"]:active, .navigation-search input[type="search"]:focus, .main-navigation .main-nav ul li.search-item.active > a{color:#ffffff;background-color:#3f3f3f;}.main-navigation ul ul{background-color:#3f3f3f;}.main-navigation .main-nav ul ul li a{color:#ffffff;}.main-navigation .main-nav ul ul li:hover > a,.main-navigation .main-nav ul ul li:focus > a,.main-navigation .main-nav ul ul li.sfHover > a{color:#ffffff;background-color:#4f4f4f;}.main-navigation .main-nav ul ul li[class*="current-menu-"] > a{color:#ffffff;background-color:#4f4f4f;}.main-navigation .main-nav ul ul li[class*="current-menu-"] > a:hover,.main-navigation .main-nav ul ul li[class*="current-menu-"].sfHover > a{color:#ffffff;background-color:#4f4f4f;}.separate-containers .inside-article, .separate-containers .comments-area, .separate-containers .page-header, .one-container .container, .separate-containers .paging-navigation, .inside-page-header{background-color:#ffffff;}.entry-meta{color:#595959;}.entry-meta a,.entry-meta a:visited{color:#595959;}.entry-meta a:hover{color:#e60000;}.sidebar .widget{background-color:#ffffff;}.sidebar .widget .widget-title{color:#000000;}.footer-widgets{color:#ffffff;background-color:#061d41;}.footer-widgets a:hover{color:#ffffff;}.footer-widgets .widget-title{color:#ffffff;}.site-info{color:#ffffff;background-color:#222222;}.site-info a,.site-info a:visited{color:#ffffff;}.site-info a:hover{color:#606060;}.footer-bar .widget_nav_menu .current-menu-item a{color:#606060;}input[type="text"],input[type="email"],input[type="url"],input[type="password"],input[type="search"],input[type="tel"],input[type="number"],textarea,select{color:#666666;background-color:#fafafa;border-color:#cccccc;}input[type="text"]:focus,input[type="email"]:focus,input[type="url"]:focus,input[type="password"]:focus,input[type="search"]:focus,input[type="tel"]:focus,input[type="number"]:focus,textarea:focus,select:focus{color:#666666;background-color:#ffffff;border-color:#bfbfbf;}button,html input[type="button"],input[type="reset"],input[type="submit"],a.button,a.button:visited,a.wp-block-button__link:not(.has-background){color:#ffffff;background-color:#339933;}button:hover,html input[type="button"]:hover,input[type="reset"]:hover,input[type="submit"]:hover,a.button:hover,button:focus,html input[type="button"]:focus,input[type="reset"]:focus,input[type="submit"]:focus,a.button:focus,a.wp-block-button__link:not(.has-background):active,a.wp-block-button__link:not(.has-background):focus,a.wp-block-button__link:not(.has-background):hover{color:#ffffff;background-color:#3f3f3f;}.generate-back-to-top,.generate-back-to-top:visited{background-color:rgba( 0,0,0,0.4 );color:#ffffff;}.generate-back-to-top:hover,.generate-back-to-top:focus{background-color:rgba( 0,0,0,0.6 );color:#ffffff;}.entry-content .alignwide, body:not(.no-sidebar) .entry-content .alignfull{margin-left:-40px;width:calc(100% + 80px);max-width:calc(100% + 80px);}.separate-containers .widget, .separate-containers .site-main > *, .separate-containers .page-header, .widget-area .main-navigation{margin-bottom:0px;}.separate-containers .site-main{margin:0px;}.both-right.separate-containers .inside-left-sidebar{margin-right:0px;}.both-right.separate-containers .inside-right-sidebar{margin-left:0px;}.both-left.separate-containers .inside-left-sidebar{margin-right:0px;}.both-left.separate-containers .inside-right-sidebar{margin-left:0px;}.separate-containers .page-header-image, .separate-containers .page-header-contained, .separate-containers .page-header-image-single, .separate-containers .page-header-content-single{margin-top:0px;}.separate-containers .inside-right-sidebar, .separate-containers .inside-left-sidebar{margin-top:0px;margin-bottom:0px;}.main-navigation .main-nav ul li a,.menu-toggle,.main-navigation .mobile-bar-items a{padding-left:26px;padding-right:26px;}.main-navigation .main-nav ul ul li a{padding:10px 26px 10px 26px;}.rtl .menu-item-has-children .dropdown-menu-toggle{padding-left:26px;}.menu-item-has-children .dropdown-menu-toggle{padding-right:26px;}.rtl .main-navigation .main-nav ul li.menu-item-has-children > a{padding-right:26px;}@media (max-width:768px){.separate-containers .inside-article, .separate-containers .comments-area, .separate-containers .page-header, .separate-containers .paging-navigation, .one-container .site-content, .inside-page-header, .wp-block-group__inner-container{padding:30px;}.entry-content .alignwide, body:not(.no-sidebar) .entry-content .alignfull{margin-left:-30px;width:calc(100% + 60px);max-width:calc(100% + 60px);}}.one-container .sidebar .widget{padding:0px;}/* End cached CSS */@media (max-width: 768px){.main-navigation .menu-toggle,.main-navigation .mobile-bar-items,.sidebar-nav-mobile:not(#sticky-placeholder){display:block;}.main-navigation ul,.gen-sidebar-nav{display:none;}[class*="nav-float-"] .site-header .inside-header > *{float:none;clear:both;}}@font-face {font-family: "GeneratePress";src:  url("https://www.britainoutdoors.co.uk/wp-content/themes/generatepress/fonts/generatepress.eot");src:  url("https://www.britainoutdoors.co.uk/wp-content/themes/generatepress/fonts/generatepress.eot#iefix") format("embedded-opentype"),  url("https://www.britainoutdoors.co.uk/wp-content/themes/generatepress/fonts/generatepress.woff2") format("woff2"),  url("https://www.britainoutdoors.co.uk/wp-content/themes/generatepress/fonts/generatepress.woff") format("woff"),  url("https://www.britainoutdoors.co.uk/wp-content/themes/generatepress/fonts/generatepress.ttf") format("truetype"),  url("https://www.britainoutdoors.co.uk/wp-content/themes/generatepress/fonts/generatepress.svg#GeneratePress") format("svg");font-weight: normal;font-style: normal;}.main-navigation .slideout-toggle a:before,.slide-opened .slideout-overlay .slideout-exit:before {font-family: GeneratePress;}.slideout-navigation .dropdown-menu-toggle:before {content: "\f107" !important;}.slideout-navigation .sfHover > a .dropdown-menu-toggle:before {content: "\f106" !important;}
body{background-image:url('https://www.britainoutdoors.co.uk/wp-content/uploads/2017/12/mountain-3.jpg');background-attachment:fixed;}
.navigation-branding .main-title{font-weight:bold;text-transform:none;font-size:45px;}@media (max-width: 768px){.navigation-branding .main-title{font-size:30px;}}
</style>
<link href="https://www.britainoutdoors.co.uk/wp-content/themes/generatepress/css/mobile.min.css?ver=2.4.2" id="generate-mobile-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.britainoutdoors.co.uk/wp-content/plugins/gp-premium/blog/functions/css/style-min.css?ver=1.10.0" id="generate-blog-css" media="all" rel="stylesheet" type="text/css"/>
<script>if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script><script src="https://www.britainoutdoors.co.uk/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.britainoutdoors.co.uk/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var Cli_Data = {"nn_cookie_ids":[],"cookielist":[]};
var log_object = {"ajax_url":"https:\/\/www.britainoutdoors.co.uk\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script src="https://www.britainoutdoors.co.uk/wp-content/plugins/cookie-law-info/public/js/cookie-law-info-public.js?ver=1.8.8" type="text/javascript"></script>
<link href="https://www.britainoutdoors.co.uk/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.britainoutdoors.co.uk/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.britainoutdoors.co.uk/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.4" name="generator"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/></head>
<body class="error404 wp-custom-logo wp-embed-responsive post-image-below-header post-image-aligned-center sticky-menu-fade right-sidebar nav-below-header contained-header one-container active-footer-widgets-3 nav-aligned-left header-aligned-left dropdown-hover" data-rsssl="1" itemscope="" itemtype="https://schema.org/WebPage">
<a class="screen-reader-text skip-link" href="#content" title="Skip to content">Skip to content</a> <header class="site-header grid-container grid-parent" id="masthead" itemscope="" itemtype="https://schema.org/WPHeader">
<div class="inside-header grid-container grid-parent">
<div class="site-logo">
<a href="https://www.britainoutdoors.co.uk/" rel="home" title="Britain Outdoors Travel Blog">
<img alt="Britain Outdoors Travel Blog" class="header-image" src="https://www.britainoutdoors.co.uk/wp-content/uploads/2017/12/new-logo.png" title="Britain Outdoors Travel Blog"/>
</a>
</div> </div><!-- .inside-header -->
</header><!-- #masthead -->
<nav class="main-navigation grid-container grid-parent" id="site-navigation" itemscope="" itemtype="https://schema.org/SiteNavigationElement">
<div class="inside-navigation grid-container grid-parent">
<button aria-controls="primary-menu" aria-expanded="false" class="menu-toggle">
<span class="mobile-menu">Menu</span> </button>
<div class="main-nav" id="primary-menu"><ul class=" menu sf-menu" id="menu-top-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-1452" id="menu-item-1452"><a href="https://www.britainoutdoors.co.uk/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1449" id="menu-item-1449"><a href="https://www.britainoutdoors.co.uk/about-us/">About us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1448" id="menu-item-1448"><a href="https://www.britainoutdoors.co.uk/links/">Links</a></li>
</ul></div> </div><!-- .inside-navigation -->
</nav><!-- #site-navigation -->
<div class="hfeed site grid-container container grid-parent" id="page">
<div class="site-content" id="content">
<div class="content-area grid-parent mobile-grid-100 grid-75 tablet-grid-75" id="primary">
<main class="site-main" id="main">
<div class="inside-article">
<header class="entry-header">
<h1 class="entry-title" itemprop="headline">Oops! That page can’t be found.</h1>
</header><!-- .entry-header -->
<div class="entry-content" itemprop="text">
<p>It looks like nothing was found at this location. Maybe try searching?</p><form action="https://www.britainoutdoors.co.uk/" class="search-form" method="get">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" title="Search for:" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form>
</div><!-- .entry-content -->
</div><!-- .inside-article -->
</main><!-- #main -->
</div><!-- #primary -->
<div class="widget-area grid-25 tablet-grid-25 grid-parent sidebar" id="right-sidebar" itemscope="" itemtype="https://schema.org/WPSideBar">
<div class="inside-right-sidebar">
<aside class="widget inner-padding widget_recent_entries" id="recent-posts-3"> <h2 class="widget-title">Recent Posts</h2> <ul>
<li>
<a href="https://www.britainoutdoors.co.uk/should-you-consider-air-source-heat-pumps/">Should You Consider Air Source Heat Pumps?</a>
</li>
<li>
<a href="https://www.britainoutdoors.co.uk/things-to-do-in-wiltshire-this-spring/">Things to Do In Wiltshire This Spring</a>
</li>
<li>
<a href="https://www.britainoutdoors.co.uk/the-real-benefits-of-using-decorative-stones-and-gravel-in-your-garden/">The Real Benefits of Using Decorative Stones and Gravel In Your Garden</a>
</li>
<li>
<a href="https://www.britainoutdoors.co.uk/benefits-of-investing-in-a-generator-or-power-station-for-camping-trip/">Benefits of Investing in a Generator or Power Station for Camping Trip</a>
</li>
<li>
<a href="https://www.britainoutdoors.co.uk/when-can-pocket-knives-come-in-handy-in-daily-life-apart-from-camping-trips/">When Can Pocket Knives Come In Handy In Daily Life Apart From Camping Trips</a>
</li>
<li>
<a href="https://www.britainoutdoors.co.uk/the-best-spots-to-shop-london-calling/">The Best Spots to Shop – London Calling</a>
</li>
<li>
<a href="https://www.britainoutdoors.co.uk/top-5-things-to-do-in-key-west/">Top 5 Things to Do in Key West</a>
</li>
<li>
<a href="https://www.britainoutdoors.co.uk/walking-london-in-a-day-the-ultimate-london-walking-tour/">Walking London in a Day: The Ultimate London Walking Tour</a>
</li>
<li>
<a href="https://www.britainoutdoors.co.uk/the-uk-outdoor-workwear-regulations-you-might-not-know-about-but-definitely-should/">The UK Outdoor Workwear Regulations You Might Not Know About – But Definitely Should</a>
</li>
<li>
<a href="https://www.britainoutdoors.co.uk/how-to-plan-the-perfect-holiday/">How To Plan The Perfect Holiday</a>
</li>
<li>
<a href="https://www.britainoutdoors.co.uk/5-top-tips-for-keeping-your-home-safe-whilst-youre-on-holiday/">5 Top Tips for Keeping Your Home Safe Whilst You’re on Holiday</a>
</li>
<li>
<a href="https://www.britainoutdoors.co.uk/activities-to-do-in-the-isle-of-wight/">Activities to Do in the Isle of Wight</a>
</li>
<li>
<a href="https://www.britainoutdoors.co.uk/the-best-places-in-britain-to-buy-a-holiday-home/">The Best Places in Britain to Buy a Holiday Home</a>
</li>
<li>
<a href="https://www.britainoutdoors.co.uk/top-5-must-visit-places-in-london/">Top 5 Must Visit Places In London</a>
</li>
<li>
<a href="https://www.britainoutdoors.co.uk/top-5-things-to-do-when-visiting-estonia/">Top 5 Things to Do When Visiting Estonia</a>
</li>
</ul>
</aside><aside class="widget inner-padding widget_categories" id="categories-3"><h2 class="widget-title">Categories</h2> <ul>
<li class="cat-item cat-item-23"><a href="https://www.britainoutdoors.co.uk/category/adventure/">Adventure</a>
</li>
<li class="cat-item cat-item-28"><a href="https://www.britainoutdoors.co.uk/category/backpacking/">Backpacking</a>
</li>
<li class="cat-item cat-item-25"><a href="https://www.britainoutdoors.co.uk/category/beach/">Beach</a>
</li>
<li class="cat-item cat-item-14"><a href="https://www.britainoutdoors.co.uk/category/camping/">Camping</a>
</li>
<li class="cat-item cat-item-5"><a href="https://www.britainoutdoors.co.uk/category/climbing/">Climbing</a>
</li>
<li class="cat-item cat-item-15"><a href="https://www.britainoutdoors.co.uk/category/clothing/">Clothing</a>
</li>
<li class="cat-item cat-item-4"><a href="https://www.britainoutdoors.co.uk/category/cycling/">Cycling</a>
</li>
<li class="cat-item cat-item-11"><a href="https://www.britainoutdoors.co.uk/category/family/">Family</a>
</li>
<li class="cat-item cat-item-3"><a href="https://www.britainoutdoors.co.uk/category/featured/">featured</a>
</li>
<li class="cat-item cat-item-29"><a href="https://www.britainoutdoors.co.uk/category/fishing/">Fishing</a>
</li>
<li class="cat-item cat-item-21"><a href="https://www.britainoutdoors.co.uk/category/food/">Food</a>
</li>
<li class="cat-item cat-item-30"><a href="https://www.britainoutdoors.co.uk/category/holiday/">Holiday</a>
</li>
<li class="cat-item cat-item-13"><a href="https://www.britainoutdoors.co.uk/category/holiday-homes/">Holiday homes</a>
</li>
<li class="cat-item cat-item-32"><a href="https://www.britainoutdoors.co.uk/category/home/">Home</a>
</li>
<li class="cat-item cat-item-8"><a href="https://www.britainoutdoors.co.uk/category/other/">Other</a>
</li>
<li class="cat-item cat-item-24"><a href="https://www.britainoutdoors.co.uk/category/outdoors/">Outdoors</a>
</li>
<li class="cat-item cat-item-10"><a href="https://www.britainoutdoors.co.uk/category/photography/">Photography</a>
</li>
<li class="cat-item cat-item-22"><a href="https://www.britainoutdoors.co.uk/category/review/">Review</a>
</li>
<li class="cat-item cat-item-26"><a href="https://www.britainoutdoors.co.uk/category/skiing/">Skiing</a>
</li>
<li class="cat-item cat-item-31"><a href="https://www.britainoutdoors.co.uk/category/snowboarding/">Snowboarding</a>
</li>
<li class="cat-item cat-item-27"><a href="https://www.britainoutdoors.co.uk/category/sports/">Sports</a>
</li>
<li class="cat-item cat-item-20"><a href="https://www.britainoutdoors.co.uk/category/tips/">Tips</a>
</li>
<li class="cat-item cat-item-17"><a href="https://www.britainoutdoors.co.uk/category/travel-tips/">Travel Tips</a>
</li>
<li class="cat-item cat-item-1"><a href="https://www.britainoutdoors.co.uk/category/uncategorized/">Uncategorized</a>
</li>
<li class="cat-item cat-item-7"><a href="https://www.britainoutdoors.co.uk/category/walking/">Walking</a>
</li>
<li class="cat-item cat-item-12"><a href="https://www.britainoutdoors.co.uk/category/water/">Water</a>
</li>
</ul>
</aside> </div><!-- .inside-right-sidebar -->
</div><!-- #secondary -->
</div><!-- #content -->
</div><!-- #page -->
<div class="site-footer">
<div class="site footer-widgets" id="footer-widgets">
<div class="footer-widgets-container grid-container grid-parent">
<div class="inside-footer-widgets">
<div class="footer-widget-1 grid-parent grid-33 tablet-grid-50 mobile-grid-100">
<aside class="widget inner-padding widget_nav_menu" id="nav_menu-3"><h2 class="widget-title">Quick Links</h2><div class="menu-top-menu-container"><ul class="menu" id="menu-top-menu-1"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-1452"><a href="https://www.britainoutdoors.co.uk/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1449"><a href="https://www.britainoutdoors.co.uk/about-us/">About us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1448"><a href="https://www.britainoutdoors.co.uk/links/">Links</a></li>
</ul></div></aside> </div>
<div class="footer-widget-2 grid-parent grid-33 tablet-grid-50 mobile-grid-100">
<aside class="widget inner-padding widget_nav_menu" id="nav_menu-4"><h2 class="widget-title">Catagories</h2><div class="menu-category-menu-container"><ul class="menu" id="menu-category-menu"><li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-756" id="menu-item-756"><a href="https://www.britainoutdoors.co.uk/category/family/">Family</a></li>
<li class="walking menu-item menu-item-type-taxonomy menu-item-object-category menu-item-757" id="menu-item-757"><a href="https://www.britainoutdoors.co.uk/category/walking/">Walking</a></li>
<li class="holidays menu-item menu-item-type-taxonomy menu-item-object-category menu-item-759" id="menu-item-759"><a href="https://www.britainoutdoors.co.uk/category/holiday-homes/">Holiday homes</a></li>
<li class="Photography menu-item menu-item-type-taxonomy menu-item-object-category menu-item-760" id="menu-item-760"><a href="https://www.britainoutdoors.co.uk/category/photography/">Photography</a></li>
<li class="cycle menu-item menu-item-type-taxonomy menu-item-object-category menu-item-761" id="menu-item-761"><a href="https://www.britainoutdoors.co.uk/category/cycling/">Cycling</a></li>
<li class="climbing menu-item menu-item-type-taxonomy menu-item-object-category menu-item-762" id="menu-item-762"><a href="https://www.britainoutdoors.co.uk/category/climbing/">Climbing</a></li>
<li class="fire menu-item menu-item-type-taxonomy menu-item-object-category menu-item-763" id="menu-item-763"><a href="https://www.britainoutdoors.co.uk/category/camping/">Camping</a></li>
<li class="runing menu-item menu-item-type-taxonomy menu-item-object-category menu-item-764" id="menu-item-764"><a href="https://www.britainoutdoors.co.uk/category/running/">Running</a></li>
</ul></div></aside> </div>
<div class="footer-widget-3 grid-parent grid-33 tablet-grid-50 mobile-grid-100">
<aside class="widget inner-padding widget_text" id="text-9"> <div class="textwidget"><div class="social_link">
<ul>
<li><a href="#"><img src="https://www.britainoutdoors.co.uk/wp-content/themes/britainoutdoors/images/facebook.png"/></a></li>
<li><a href="#"><img src="https://www.britainoutdoors.co.uk/wp-content/themes/britainoutdoors/images/twitter.png"/></a></li>
<li><a href="#"><img src="https://www.britainoutdoors.co.uk/wp-content/themes/britainoutdoors/images/google_plus.png"/></a></li></ul>
</div>
<div class="follow_us"><img src="https://www.britainoutdoors.co.uk/wp-content/themes/britainoutdoors/images/follow.png"/></div></div>
</aside> </div>
</div>
</div>
</div>
<footer class="site-info" itemscope="" itemtype="https://schema.org/WPFooter">
<div class="inside-site-info grid-container grid-parent">
<div class="copyright-bar">
<span class="copyright">© 2021 Britain Outdoors Travel Blog</span> • Powered by <a href="https://generatepress.com" itemprop="url">GeneratePress</a> </div>
</div>
</footer><!-- .site-info -->
</div><!-- .site-footer -->
<!--googleoff: all--><div id="cookie-law-info-bar"><span>This website uses cookies to improve your experience. We'll assume you're ok with this, but you can opt-out if you wish.<a class="medium cli-plugin-button cli-plugin-main-button cookie_action_close_header cli_action_button" data-cli_action="accept" id="cookie_action_close_header" role="button" style="display:inline-block; " tabindex="0">Accept</a> <a class="cli-plugin-main-link" href="https://www.britainoutdoors.co.uk" id="CONSTANT_OPEN_URL" style="display:inline-block;" target="_blank">Read More</a></span></div><div id="cookie-law-info-again" style="display:none;"><span id="cookie_hdr_showagain">Privacy &amp; Cookies Policy</span></div><div aria-hidden="true" aria-labelledby="cliSettingsPopup" class="cli-modal" id="cliSettingsPopup" role="dialog" tabindex="-1">
<div class="cli-modal-dialog" role="document">
<div class="cli-modal-content cli-bar-popup">
<button class="cli-modal-close" id="cliModalClose" type="button">
<svg class="" viewbox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
<span class="wt-cli-sr-only">Close</span>
</button>
<div class="cli-modal-body">
<div class="cli-container-fluid cli-tab-container">
<div class="cli-row">
<div class="cli-col-12 cli-align-items-stretch cli-px-0">
<div class="cli-privacy-overview">
<div class="cli-privacy-content">
<div class="cli-privacy-content-text"></div>
</div>
<a class="cli-privacy-readmore" data-readless-text="Show less" data-readmore-text="Show more"></a> </div>
</div>
<div class="cli-col-12 cli-align-items-stretch cli-px-0 cli-tab-section-container">
<div class="cli-tab-section">
<div class="cli-tab-header">
<a class="cli-nav-link cli-settings-mobile" data-target="necessary" data-toggle="cli-toggle-tab" role="button" tabindex="0">
                            Necessary 
                        </a>
<span class="cli-necessary-caption">Always Enabled</span> </div>
<div class="cli-tab-content">
<div class="cli-tab-pane cli-fade" data-id="necessary">
<p></p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
<div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>
<script type="text/javascript">
  /* <![CDATA[ */
  cli_cookiebar_settings='{"animate_speed_hide":"500","animate_speed_show":"500","background":"#fff","border":"#444","border_on":false,"button_1_button_colour":"#000","button_1_button_hover":"#000000","button_1_link_colour":"#fff","button_1_as_button":true,"button_1_new_win":false,"button_2_button_colour":"#333","button_2_button_hover":"#292929","button_2_link_colour":"#444","button_2_as_button":false,"button_2_hidebar":true,"button_3_button_colour":"#000","button_3_button_hover":"#000000","button_3_link_colour":"#fff","button_3_as_button":true,"button_3_new_win":false,"button_4_button_colour":"#000","button_4_button_hover":"#000000","button_4_link_colour":"#fff","button_4_as_button":true,"font_family":"inherit","header_fix":false,"notify_animate_hide":true,"notify_animate_show":false,"notify_div_id":"#cookie-law-info-bar","notify_position_horizontal":"right","notify_position_vertical":"bottom","scroll_close":false,"scroll_close_reload":false,"accept_close_reload":false,"reject_close_reload":false,"showagain_tab":true,"showagain_background":"#fff","showagain_border":"#000","showagain_div_id":"#cookie-law-info-again","showagain_x_position":"100px","text":"#000","show_once_yn":false,"show_once":"10000","logging_on":false,"as_popup":false,"popup_overlay":true,"bar_heading_text":"","cookie_bar_as":"banner","popup_showagain_position":"bottom-right","widget_position":"left"}';
  /* ]]> */
</script>
<!--googleon: all--><!--[if lte IE 11]>
<script type='text/javascript' src='https://www.britainoutdoors.co.uk/wp-content/themes/generatepress/js/classList.min.js?ver=2.4.2'></script>
<![endif]-->
<script src="https://www.britainoutdoors.co.uk/wp-content/themes/generatepress/js/menu.min.js?ver=2.4.2" type="text/javascript"></script>
<script src="https://www.britainoutdoors.co.uk/wp-content/themes/generatepress/js/a11y.min.js?ver=2.4.2" type="text/javascript"></script>
<script src="https://www.britainoutdoors.co.uk/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
</body>
</html>
