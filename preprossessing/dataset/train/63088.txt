<!DOCTYPE html>
<html data-ng-app="accApp">
<head>
<meta content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<title>Prisijungti</title>
<link href="/Content/favicon.ico" rel="shortcut icon" type="image/ico"/>
<script>
        (function () {
            localStorage.setItem('cookieDomain', '.accdistribution.net');
        })()
    </script>
<link href="/bundles/less?v=AVkZPJ8TJ8_i7sk-S4b2H5gTyJnVL6lUavnmzR330AI1" rel="stylesheet"/>
<script src="/bundles/vendor?v=dKsUryJs2xPPate4mBkdduaYAtEdQlUq5H3qL04zxeI1"></script>
<script src="/bundles/login?v=0KKnLk3vRxFghLUboNRkY3yOxFJrmgzICmuPW4nWlg01"></script>
<link href="/bundles/styleguide-acc?v=XMrkDniX9antfRer-ANVxgX9hmYCBlIAUke-0DoE0tA1" rel="stylesheet"/>
</head>
<body>
<div class="login-view" ng-controller="MainController" style="background-image: url('/_al/lt-lt/File/ShowImageByType?type=login_page_image&amp;v=c57f57b0-d023-421e-b6df-b9d648b3251a');">
<div id="content">
<div class="text-center logo-container">
<a href="/"><img alt="login logo" src="/Content/img/login_logo.png"/></a>
</div>
<div class="login-container col-xs-12 col-sm-8 col-md-6 col-lg-5" onload="onload()" partial-view="partial">
<div ng-cloak="" ng-controller="LoginController as lc" ng-show="!oldBrowser">
<form class="login-form" name="info" novalidate="">
<div alerts="" class="login-notifications"></div>
<h2>Prisijungimas</h2>
<div class="dropdown pull-right" uib-dropdown="">
<button class="current-lang dropdown-toggle btn-link" type="button" uib-dropdown-toggle="">
                LT
                <span class="caret"></span>
</button>
<ul class="language-select dropdown-menu" role="menu" uib-dropdown-menu="">
<li><a href="" ng-click="changeLang('en-us')">English</a></li>
<li><a href="" ng-click="changeLang('lt-lt')">Lietuvių</a></li>
<li><a href="" ng-click="changeLang('lv-lv')">Latviešu</a></li>
<li><a href="" ng-click="changeLang('et-ee')">Eesti</a></li>
<li><a href="" ng-click="changeLang('hy-am')">Հայերեն</a></li>
<li><a href="" ng-click="changeLang('ru-ru')">Pусский</a></li>
<li><a href="" ng-click="changeLang('pl-pl')">Polski</a></li>
<li><a href="" ng-click="changeLang('ka-ge')">ქართული</a></li>
</ul>
</div>
<div class="form-group">
<input autocapitalize="off" autocorrect="off" class="form-control" maxlength="9" name="username" ng-model="lc.username" placeholder="Kliento ID" required="" type="text"/>
<div class="form-error" form-errors="info.username"></div>
</div>
<div class="form-group">
<input autocapitalize="off" autocorrect="off" class="form-control" maxlength="64" name="password" ng-model="lc.password" password-toggle2="" placeholder="Slaptažodis" required="" type="password"/>
<div class="form-error" form-errors="info.password"></div>
</div>
<div class="row buttons-container">
<div class="col-xs-12 col-sm-6">
<button class="btn btn-primary btn-lg form-control" ng-click="login()" ng-disabled="loading">Prisijungti</button>
</div>
<div class="col-xs-12 col-sm-6">
<a class="btn btn-secondary form-control btn-lg company-registration-button" ng-href="{{prefix}}/companyregistration">Įmonės registracija</a>
</div>
</div>
</form>
<div class="login-footer">
<div class="privacy-policy">
<a ng-href="http://accdistribution.lt/lt/privacy-policy">Privatumo politika</a>
</div>
<div class="password-reminder">
<span>Pamiršote slaptažodį?</span>
<a ng-href="{{prefix}}/passwordreset">Priminti slaptažodį</a>
</div>
</div>
<span ng-cloak="" ng-show="error">Prisijungimo klaida</span>
<script id="alerts.html" type="text/ng-template">
        <uib-alert ng-repeat="alert in alerts track by $index" type="{{alert.type}}" close="closeAlert($index)" dismiss-on-timeout="{{alert.timeout}}">
            <span ng-repeat="msg in alert.msgs track by $index">{{msg | translate}}</span>
        </uib-alert>
    </script>
</div>
<div ng-cloak="" ng-show="oldBrowser" style="text-align: center;">
    Atsinaujinkite naršykle jei norite naudotis šiuo puslapiu
</div>
</div>
<div class="copyright-text">
<span>© 1998 - {{currentYear}}, ACC Distribution. All right reserved</span>
</div>
</div>
</div>
<script id="passwordToggle.html" type="text/ng-template">
        <div class="input-group password-toggle">
            <ng-transclude></ng-transclude>
            <button type="button" class="btn btn-link" ng-click="change()">
                <i class="icon-show-password"></i>
            </button>
        </div>
    </script>
<script id="alerts.html" type="text/ng-template">
        <uib-alert ng-repeat="alert in alerts track by $index" type="{{alert.type}}" close="closeAlert($index)" dismiss-on-timeout="{{alert.timeout}}">
            <span ng-repeat="msg in alert.msgs track by $index">{{msg | translate}}</span>
        </uib-alert>
    </script>
</body>
</html>
