<!DOCTYPE html>
<html class="unknown unknownunknown journal-desktop is-guest skin-2 responsive-layout infinite-scroll center-header lang-full lang-full-mobile currency-full currency-symbol-mobile no-top-on-mobile collapse-footer-columns mobile-menu-on-tablet extended-layout header-center header-sticky sticky-menu backface product-grid-second-image product-list-second-image hide-cart layout-4 route-error-not_found oc2 oc23 oc3 no-currency" data-j2v="2.16.8" dir="ltr" lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="telephone=no" name="format-detection"/>
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/><![endif]-->
<title>The page you requested cannot be found!</title>
<base href="https://shabaka.biz/"/>
<meta content="Al Shabakah Universal Trading" property="og:title"/>
<meta content="Al Shabakah Universal Trading" property="og:site_name"/>
<meta content="https://shabaka.biz/" property="og:url"/>
<meta content="Al Shabakah Universal Trading..." property="og:description"/>
<meta content="website" property="og:type"/>
<meta content="https://shabaka.biz/image/cache/catalog/logo-600x315.png" property="og:image"/>
<meta content="600" property="og:image:width"/>
<meta content="315" property="og:image:height"/>
<meta content="summary" name="twitter:card"/>
<meta content="Al Shabakah Universal Trading" name="twitter:title"/>
<meta content="Al Shabakah Universal Trading..." name="twitter:description"/>
<meta content="https://shabaka.biz/image/cache/catalog/logo-200x200.png" name="twitter:image"/>
<meta content="200" name="twitter:image:width"/>
<meta content="200" name="twitter:image:height"/>
<link href="https://shabaka.biz/image/catalog/logo.png" rel="icon"/>
<link href="//fonts.googleapis.com/css?family=Droid+Serif:%7CCairo:200,regular&amp;subset=latin,arabic" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/j-strap.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/javascript/font-awesome/css/font-awesome.min.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/lib/jquery.ui/jquery-ui-slider.min.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/lib/swiper/css/swiper.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/lib/lightgallery/css/lightgallery.min.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/lib/magnific-popup/magnific-popup.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/hint.min.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/journal.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/features.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/header.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/module.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/pages.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/account.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/blog-manager.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/side-column.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/product.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/category.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/footer.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/icons.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/responsive.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/flex.css?j2v=2.16.8" rel="stylesheet"/>
<link href="https://shabaka.biz/catalog/view/theme/journal2/css/rtl.css?j2v=2.16.8" rel="stylesheet"/>
<link href="index.php?route=journal2/assets/css" rel="stylesheet"/>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/modernizr/modernizr.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/javascript/jquery/jquery-2.1.1.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/javascript/bootstrap/js/bootstrap.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/jquery/jquery-migrate-1.2.1.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/jquery.ui/jquery-ui-slider.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/javascript/common.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/javascript/jquery/jquery.total-storage.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/jquery.tabs/tabs.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/swiper/js/swiper.jquery.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/ias/jquery-ias.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/intense/intense.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/lightgallery/js/lightgallery.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/magnific-popup/jquery.magnific-popup.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/actual/jquery.actual.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/countdown/jquery.countdown.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/image-zoom/jquery.imagezoom.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/lazy/jquery.lazy.1.6.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/js/journal.js?j2v=2.16.8" type="text/javascript"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5c48676cab5284048d0e3719/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<script>
Journal.COUNTDOWN = {
  DAYS    : "Days",
  HOURS   : "Hours",
  MINUTES : "Min",
  SECONDS : "Sec"
};
Journal.NOTIFICATION_BUTTONS = '<div class="notification-buttons"><a class="button notification-cart" href="https://shabaka.biz/index.php?route=checkout/cart">View Cart</a><a class="button notification-checkout" href="https://shabaka.biz/index.php?route=checkout/checkout">Checkout</a></div>';
</script>
<!-- Google Tag Manager -->
<script nitro-exclude="" type="text/javascript"> var dataLayer = window.dataLayer = window.dataLayer || [];var delayInMilliseconds = 500; function whenAvailable(name, callback) {var interval = 10; window.setTimeout(function() {if (window[name]) {callback(window[name]);} else {window.setTimeout(arguments.callee, interval);}}, interval);}
dataLayer.push({ 'cc_enabled' : '0','cc_accepted' : '','cc_analytics' : '0','cc_marketing' : '0','adwordEnable' : '1','adwordConversionID' : '880887717','adwordConversionLabel' : 'TRl-CLre-M0BEKWPhaQD','adwordCurrency':'AED','RemarketingEnable' : '1','facebookPixelID' : '1303634636647232','facebookPixel' : '1','currencyCode': 'AED','snappixelenable' : '1','snappixelid' : '26778438-ecc2-4d69-bbec-113876c035af','gid' : 'UA-136785642-1','alt_currency' : 'AED','ver' : '3.0.2.0- 6.3','r' : 'error/not_found' });</script><script nitro-exclude="">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-MWV45KH');</script><!-- End Google Tag Manager -->
</head>
<body> <noscript><iframe height="0" src="https://www.googletagmanager.com/ns.html?id=GTM-MWV45KH" style="display:none;visibility:hidden" width="0"></iframe></noscript> <!--[if lt IE 9]>
<div class="old-browser">You are using an old browser. Please <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">upgrade to a newer version</a> or <a href="http://browsehappy.com/">try a different browser</a>.</div>
<![endif]-->
<header class="journal-header-center">
<div class="header">
<div class="journal-top-header j-min z-1"></div>
<div class="journal-menu-bg z-0"></div>
<div class="journal-center-bg j-100 z-0"></div>
<div class="journal-header z-2" id="header">
<div class="header-assets top-bar">
<div class="journal-links j-min xs-100 sm-100 md-50 lg-50 xl-50">
<div class="links">
<ul class="top-menu">
<li>
<a class="m-item hide-on-phone wishlist-total" href="https://shabaka.biz/index.php?route=account/wishlist"><i data-icon="" style="margin-right: 5px; "></i>
<span class="top-menu-link">Wish List (<span class="product-count">0</span>)</span>
</a>
</li>
<li>
<a class="m-item " href="https://shabaka.biz/index.php?route=account/account"><i data-icon="" style="margin-right: 5px; "></i>
<span class="top-menu-link">Account</span>
</a>
</li>
<li>
<a class="m-item " href="https://shabaka.biz/index.php?route=checkout/cart"><i data-icon="" style="margin-right: 5px; "></i>
<span class="top-menu-link">Shopping Cart</span>
</a>
</li>
<li>
<a class="m-item " href="https://shabaka.biz/index.php?route=checkout/checkout"><i data-icon="" style="margin-right: 5px; "></i>
<span class="top-menu-link">Checkout</span>
</a>
</li>
</ul>
</div>
</div>
<div class="journal-language j-min">
<form action="https://shabaka.biz/index.php?route=common/language/language" enctype="multipart/form-data" method="post">
<div class="full-text" id="language">
<div>
<a onclick="$(this).closest('form').find('input[name=\'code\']').val('ar'); $(this).closest('form').submit();">
<span class="language-text">العربية</span>
<img alt="العربية" height="11" src="https://shabaka.biz/catalog/language/ar/ar.png" width="16"/>
</a>
</div>
<input name="code" type="hidden" value=""/>
<input name="redirect" type="hidden" value="https://shabaka.biz/index.php?route=error/not_found"/>
</div>
</form>
</div>
<div class="journal-secondary j-min xs-100 sm-100 md-50 lg-50 xl-50">
<div class="links">
<ul class="top-menu">
<li>
<a class="m-item " href="https://shabaka.biz/index.php?route=account/login"><i data-icon="" style="margin-right: 5px; "></i>
<span class="top-menu-link">Login</span>
</a>
</li>
<li>
<a class="m-item " href="https://shabaka.biz/index.php?route=account/register"><i data-icon="" style="margin-right: 5px; "></i>
<span class="top-menu-link">Register</span>
</a>
</li>
</ul>
</div>
</div>
</div>
<div class="header-assets">
<div class="journal-search j-min xs-100 sm-50 md-25 lg-25 xl-25">
<div class="input-group j-min" id="search">
<input autocomplete="off" class="form-control input-lg" name="search" placeholder="Search entire store..." type="text" value=""/>
<div class="button-search">
<button type="button"><i></i></button>
</div>
</div>
</div>
<div class="journal-logo j-100 xs-100 sm-100 md-50 lg-50 xl-50">
<div id="logo">
<a href="https://shabaka.biz/index.php?route=common/home">
<img alt="Al Shabakah Universal Trading" class="logo-1x" height="260" src="https://shabaka.biz/image/cache/catalog/logo-300x260.png" title="Al Shabakah Universal Trading" width="300"/>
</a>
</div>
</div>
<div class="journal-cart j-min xs-100 sm-50 md-25 lg-25 xl-25">
<div class="btn-group btn-block" id="cart">
<button class="btn btn-inverse btn-block btn-lg dropdown-toggle heading" data-toggle="dropdown" type="button"><a><span data-loading-text="Loading...  " id="cart-total">0 item(s) - 0  AED</span> <i></i></a></button>
<div class="content">
<ul class="cart-wrapper">
<li>
<p class="text-center empty">Your shopping cart is empty!</p>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="journal-menu j-min xs-100 sm-100 md-100 lg-100 xl-100">
<style>#main-menu-item-9 a { color: rgb(255, 255, 255) !important; } #main-menu-item-9 { background-color: rgb(88, 143, 39) !important; }</style>
<div class="mobile-trigger">MENU</div>
<ul class="super-menu mobile-menu menu-centered" style="table-layout: ">
<li class="drop-down " id="main-menu-item-1">
<a href="https://shabaka.biz/"><i data-icon="" style="margin-right: 5px; "></i><span class="main-menu-text">Home</span></a>
<span class="mobile-plus">+</span>
</li>
<li class="drop-down " id="main-menu-item-2">
<a href="https://shabaka.biz/index.php?route=product/category&amp;path=34"><i data-icon="" style="margin-right: 5px; "></i><span class="main-menu-text">Trampoline</span></a>
<ul>
<li>
<a class="" href="https://shabaka.biz/index.php?route=product/category&amp;path=34_44">
        SpringFree Trampoline
              </a>
</li>
<li>
<a class="" href="https://shabaka.biz/index.php?route=product/category&amp;path=34_43">
        Australian Kangaroo Trampoline
              </a>
</li>
<li>
<a class="" href="https://shabaka.biz/index.php?route=product/category&amp;path=34_25">
        Trampoline Accessories
              </a>
</li>
</ul>
<span class="mobile-plus">+</span>
</li>
<li class="drop-down " id="main-menu-item-3">
<a href="https://shabaka.biz/index.php?route=product/category&amp;path=24"><i data-icon="" style="margin-right: 5px; "></i><span class="main-menu-text">Outdoor Furniture</span></a>
<span class="mobile-plus">+</span>
</li>
<li class="mega-menu-categories " id="main-menu-item-4">
<a href="https://shabaka.biz/index.php?route=product/category&amp;path=17"><i data-icon="" style="margin-right: 5px; "></i><span class="main-menu-text">Hills Clothesline</span></a>
<span class="mobile-plus">+</span>
</li>
<li class="drop-down " id="main-menu-item-5">
<a href="https://shabaka.biz/index.php?route=product/category&amp;path=60"><i data-icon="" style="margin-right: 5px; "></i><span class="main-menu-text">Doormat</span></a>
<span class="mobile-plus">+</span>
</li>
<li class="drop-down " id="main-menu-item-6">
<a href="https://shabaka.biz/index.php?route=product/category&amp;path=18"><i data-icon="" style="margin-right: 5px; "></i><span class="main-menu-text">Baby Moto</span></a>
<span class="mobile-plus">+</span>
</li>
<li class="drop-down " id="main-menu-item-7">
<a href="https://shabaka.biz/index.php?route=product/category&amp;path=20"><i data-icon="" style="margin-right: 5px; "></i><span class="main-menu-text">Air Coolers</span></a>
<span class="mobile-plus">+</span>
</li>
<li class="drop-down " id="main-menu-item-8">
<a href="https://shabaka.biz/index.php?route=product/category&amp;path=59"><i data-icon="" style="margin-right: 5px; "></i><span class="main-menu-text">Water Pump</span></a>
<span class="mobile-plus">+</span>
</li>
<li class="drop-down " id="main-menu-item-9">
<a href="https://shabaka.biz/index.php?route=information/information&amp;information_id=7"><i data-icon="" style="margin-right: 5px; "></i><span class="main-menu-text">17 Benefits of Trampoline</span></a>
<span class="mobile-plus">+</span>
</li>
</ul>
</div>
</div>
</div>
</header>
<div class="extended-container">
<div class="container j-container not-found-page" id="container">
<ul class="breadcrumb">
<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="https://shabaka.biz/index.php?route=common/home" itemprop="url"><span itemprop="title">Home</span></a></li>
<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="https://shabaka.biz/index.php?route=error/not_found" itemprop="url"><span itemprop="title">The page you requested cannot be found!</span></a></li>
</ul>
<div class="row">
<div class="col-sm-12" id="content">
<h1 class="heading-title">The page you requested cannot be found!</h1>
<p>The page you requested cannot be found.</p>
<div class="buttons">
<div class="pull-right"><a class="btn btn-primary button" href="https://shabaka.biz/index.php?route=common/home">Continue</a></div>
</div>
</div>
</div>
</div>
</div>
<footer class="fullwidth-footer">
<div id="footer">
<div class="row columns " style="padding-left: 30px">
<div class="column text xs-100 sm-50 md-33 lg-25 xl-25 ">
<h3>Our Branches</h3>
<div class="column-text-wrap block-icon-top" style="">
<span><p><i aria-hidden="true" class="fa fa-shopping-cart fa-2x"></i> Sadi Arabia.<br/>
<i aria-hidden="true" class="fa fa-shopping-cart fa-2x"></i> United Arab emirates.<br/>
<i aria-hidden="true" class="fa fa-shopping-cart fa-2x"></i> Kuwait.<br/>
<i aria-hidden="true" class="fa fa-shopping-cart fa-2x"></i> Oman.<br/>
<i aria-hidden="true" class="fa fa-shopping-cart fa-2x"></i> Bahrain.<br/>
<i aria-hidden="true" class="fa fa-phone-square fa-2x"></i> <a href="tel:+97142977932">+97142977932</a></p></span>
</div>
</div>
<div class="column menu xs-100 sm-50 md-33 lg-25 xl-25 ">
<h3>Informations</h3>
<div class="column-menu-wrap" style="">
<ul>
<li><a class="m-item " href="https://shabaka.biz/index.php?route=information/information&amp;information_id=7">17 Benefits of Trampoline</a></li>
<li><a class="m-item " href="https://shabaka.biz/index.php?route=information/information&amp;information_id=4">About Us</a></li>
<li><a class="m-item " href="https://shabaka.biz/index.php?route=information/contact">Contact Us</a></li>
<li><a class="m-item " href="https://shabaka.biz/index.php?route=information/information&amp;information_id=3">Privacy Policy</a></li>
<li><a class="m-item " href="https://shabaka.biz/index.php?route=information/information&amp;information_id=5">Terms &amp; Conditions</a></li>
<li></li>
<li><a class="m-item " href="https://shabaka.biz/index.php?route=information/information&amp;information_id=6">Delivery Information</a></li>
<li><a class="m-item " href="https://shabaka.biz/index.php?route=information/sitemap">Site Map</a></li>
</ul>
</div>
</div>
<div class="column text xs-100 sm-50 md-33 lg-25 xl-25 ">
<h3>Social Media</h3>
<div class="column-text-wrap block-icon-top" style="">
<span><p><a href="https://www.instagram.com/shabakaUniversal/" target="_blank"><i class="fa fa-instagram fa-2x"></i></a> <a href="https://www.facebook.com/ShabakaUniversal" target="_blank"><i class="fa fa-facebook-square fa-2x"></i></a> <a href="https://www.youtube.com/channel/UCtWVVL9eDtRtJ-ZknjHeSoA" target="_blank"><i class="fa fa-youtube-square fa-2x"></i></a> <a href="#" target="_blank"><i class="fa fa-google-plus-square fa-2x"></i></a> <a href="https://twitter.com/ShabakaUniv" target="_blank"><i class="fa fa-twitter-square fa-2x"></i></a></p></span>
</div>
</div>
<div class="column menu xs-100 sm-50 md-33 lg-25 xl-25 ">
<h3>My Account</h3>
<div class="column-menu-wrap" style="">
<ul>
<li><a class="m-item " href="https://shabaka.biz/index.php?route=account/account">Account</a></li>
<li><a class="m-item " href="https://shabaka.biz/index.php?route=account/order">Order History</a></li>
<li><a class="m-item wishlist-total" href="https://shabaka.biz/index.php?route=account/wishlist">Wish List (<span class="product-count">0</span>)</a></li>
<li><a class="m-item " href="https://shabaka.biz/index.php?route=account/newsletter">Newsletter</a></li>
<li><a class="m-item " href="https://shabaka.biz/index.php?route=account/return/add">Returns</a></li>
<li><a class="m-item " href="https://shabaka.biz/index.php?route=product/special">Specials</a></li>
<li><a class="m-item " href="https://shabaka.biz/index.php?route=product/search">Search</a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="bottom-footer fullwidth-bar">
<div class="">
<div class="copyright">© 2019 Al Shabakah Universal Trading. All Rights Reserved.</div>
<div class="payments">
<img alt="" height="40" src="https://shabaka.biz/image/cache/catalog/payments-280x40.png" width="280"/>
</div>
</div>
</div>
</footer>
<div class="scroll-top"></div>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/lightgallery/js/lg-thumbnail.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/hover-intent/jquery.hoverIntent.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/pnotify/jquery.pnotify.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/vide/jquery.vide.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/respond/respond.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/lib/autocomplete2/jquery.autocomplete2.min.js?j2v=2.16.8" type="text/javascript"></script>
<script src="https://shabaka.biz/catalog/view/theme/journal2/js/init.js?j2v=2.16.8" type="text/javascript"></script>
<script src="index.php?route=journal2/assets/js" type="text/javascript"></script>
<!-- Google Tag Manager --><script nitro-exclude="" type="text/javascript"> window.onload = function () { var dataLayer = window.dataLayer = window.dataLayer || []; $(document).ajaxSuccess(function(event, xhr, settings, json) { if (json) { if (!json.error) { if(json['action'] && json['ec_data']['action'] == "addToCart") { var ec = json['ec_data']['data'];dataLayer.push({ 'event': 'addToCart', 'eventAction': 'addToCart', 'eventLabel': 'addToCart', 'ecommerce': { 'currencyCode': ec['currency'], 'add': { 'products': [{ 'name' : ec['name'], 'id'   : ec['id'], 'price': ec['price'], 'brand': ec['brand'], 'category': ec['category'], 'quantity': ec['quantity'], 'currency': ec['currency'] }] } } });dataLayer.push({ 'event': 'ADD_CART', 'eventAction': 'ADD_CART', 'eventLabel': 'AddToCart', 'contents': [{ 'id' : ec['id'], 'quantity' : ec['quantity'] }], 'content_name' : ec['name'], 'content_type' : 'product', 'brand': ec['brand'], 'category': ec['category'], 'quantity':ec['quantity'], 'pixel_value': ec['fprice'], 'fb_currency': ec['fcurrency'], 'number_items': ec['quantity'], 'content_ids' : ec['id'], 'remarketing_ids' : [{'id' : ec['id'],'google_business_vertical': 'retail'}], 'value' : ec['price'], 'currency' : ec['currency'] }); console.log('Tagmanager AddToCart Event Sent'); }

if(json['action'] && json['ec_data']['action'] == "addToWishlist") { var ec = json['ec_data']['data'];dataLayer.push({ 'event': 'addToWishlist', 'eventAction': 'addToWishlist', 'eventLabel': 'addToWishlist', 'ecommerce': { 'currencyCode': ec['currency'], 'add': { 'products': [{ 'name' : ec['name'], 'id'   : ec['id'], 'price': ec['price'], 'brand': ec['brand'], 'category': ec['category'], 'quantity': ec['quantity'], 'currency': ec['currency'] }] } } });dataLayer.push({ 'event': 'ADD_WISHLIST', 'eventAction': 'ADD_WISHLIST', 'eventLabel': 'AddToWishlist', 'content_ids' : ec['id'], 'content_name' : ec['name'], 'content_type' : 'product', 'brand': ec['brand'], 'category': ec['category'], 'pixel_value': ec['fprice'], 'fb_currency': ec['fcurrency'], 'value' : ec['price'], 'currency' : ec['currency'] });console.log('Tagmanager Wishlist Event Sent'); }

if(json['action'] && json['ec_data']['action'] == "addToCompare") { var ec = json['ec_data']['data'];dataLayer.push({ 'event': 'addToCompare', 'eventAction': 'addToCompare', 'eventLabel': 'addToCompare', 'ecommerce': { 'currencyCode': ec['currency'], 'add': { 'products': [{ 'name' : ec['name'], 'id'   : ec['id'], 'price': ec['price'], 'brand': ec['brand'], 'category': ec['category'], 'quantity': ec['quantity'], 'currency': ec['currency'] }] } } }); console.log('Tagmanager Comapre Event Sent'); } if(json['action'] && json['ec_data']['action'] == "RemoveCart") { var ec = json['ec_data']['data'];dataLayer.push({ 'event': 'removeFromCart', 'eventAction': 'removeFromCart', 'eventLabel': 'removeFromCart', 'ecommerce': { 'remove': { 'products': [{ 'name' : ec['name'], 'id'   : ec['id'], 'price': ec['price'], 'brand': ec['brand'], 'category': ec['category'], 'quantity': ec['quantity'], 'currency': ec['currency'] }] } } }); console.log('Tagmanager Remove Cart Event Sent'); } } } });};

function GAClick(name,sku,price,brand,category) {dataLayer.push({'event': 'productClick','eventAction': 'productClick','eventLabel': 'productClick','ecommerce': {'click': {'actionField': {'list': category},'products': [{'name': name,'id': sku,'price': price,'brand': brand,'category': category,'currency': 'AED'}]}}});}</script><!-- Google Tag Manager -->
</body>
</html>
