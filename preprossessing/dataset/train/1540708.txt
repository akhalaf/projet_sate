<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<title>Vidup Agrahari | Singer</title>
<!--=================================
Meta tags
=================================-->
<meta content="" name="description"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="minimum-scale=1.0, width=device-width, maximum-scale=1, user-scalable=no" name="viewport"/>
<!--=================================
Style Sheets
=================================-->
<link href="http://fonts.googleapis.com/css?family=Oswald:400,700,300" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Roboto:400,400italic,700" rel="stylesheet" type="text/css"/>
<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/font-awesome.min.css" rel="stylesheet"/>
<link href="assets/css/flexslider.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/prettyPhoto.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/jquery.vegas.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/main.css" rel="stylesheet"/>
<!--<link rel="stylesheet" type="text/css" href="assets/css/blue.css">
<link rel="stylesheet" type="text/css" href="assets/css/orange.css">
<link rel="stylesheet" type="text/css" href="assets/css/red.css">
<link rel="stylesheet" type="text/css" href="assets/css/green.css">
<link rel="stylesheet" type="text/css" href="assets/css/purple.css">-->
<script async="" src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script defer="" src="assets/js/jquery.js"></script>
<script defer="" src="assets/js/ajaxify.min.js"></script>
</head>
<body>
<!--=================================
	Header
	=================================-->
<header>
<div class="social-links">
<div class="container">
<ul class="social">
<li><a href="#"><span class="fa fa-facebook"></span></a></li>
<li><a href="#"><span class="fa fa-twitter"></span></a></li>
<li><a href="#"><span class="fa fa-youtube"></span></a></li>
</ul>
</div>
</div>
<nav class="navbar yamm navbar-default">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle fa fa-navicon" type="button"></button>
<a class="navbar-brand" href="index.html">
<img alt="logo" src="assets/img/basic/logo.png"/>
</a>
</div>
<div class="nav_wrapper">
<div class="nav_scroll">
<div class="nav-search">
<form>
<input placeholder="Search" type="text"/>
<button type="submit"><i class="fa fa-search"></i></button>
</form>
</div>
<ul class="nav navbar-nav">
<li class="active dropdown"><a href="index.html">Home <i class="fa fa-caret-right"></i></a>
</li>
<li class="yamm-fw dropdown"><a href="biography.html">Biography <i class="fa fa-caret-right"></i></a>
</li>
<li class="dropdown"><a href="#">Events <i class="fa fa-caret-right"></i></a>
<ul class="dropdown-menu">
<li><a href="#">Events</a> </li>
<li><a href="#">Event Detail</a> </li>
</ul>
</li>
<li class="dropdown"><a href="#">Albums <i class="fa fa-caret-right"></i></a>
<ul class="dropdown-menu">
<li><a href="album.html">Albums</a> </li>
<li><a href="album-detail.html">Album Detail</a> </li>
</ul>
</li>
<li class="yamm-fw dropdown"><a href="#">Videos <i class="fa fa-caret-right"></i></a>
<ul class="dropdown-menu">
<li>
<div class="yamm-content">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
<div class="video-feed"> <img alt="" src="assets/img/videos/video1.jpg"/> <a href="#"><span class="fa fa-play"></span></a>
<h5>Jane Kya Dekh Liya</h5>
</div>
<!--\\video-feed-->
<a class="to-page" href="videos.html">View All videos</a> </div>
<!--col-->
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
<div class="video-feed"> <img alt="" src="assets/img/videos/1.jpg"/> <a href="video-detail.html"><span class="fa fa-play"></span></a>
<h6>1 week ago</h6>
</div>
<!--\\video-feed-->
<h4>Vidup Agrahari</h4>
<p>Jane Kya Dekh Liya</p>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
<div class="video-feed"> <img alt="" src="assets/img/videos/2.jpg"/> <a href="video-detail.html"><span class="fa fa-play"></span></a>
<h6>1 week ago</h6>
</div>
<!--\\video-feed-->
<h4>Vidup Agrahari</h4>
<p>Chori Chori</p>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
<div class="video-feed"> <img alt="" src="assets/img/videos/3.jpg"/> <a href="video-detail.html"><span class="fa fa-play"></span></a>
<h6>1 week ago</h6>
</div>
<!--\\video-feed-->
<h4>Vidup Agrahari</h4>
<p>Halke Halke</p>
</div>
</div>
<!--row-->
</div>
<!--col-->
</div>
<!--row-->
</div>
<!--yamm content-->
</li>
</ul>
</li>
<li><a href="gallery.html">Gallery</a>
<ul class="dropdown-menu">
<li><a href="#">Print Media</a> </li>
<li><a href="#">Digital Media</a> </li>
</ul>
</li>
<li><a href="codes.pdf">Download Codes</a></li>
<li><a href="contact.html">Contact</a></li>
</ul>
</div>
<!--/.nav-collapse -->
</div>
</div>
</nav>
</header>
<!--=================================
Vegas Slider Images
=================================-->
<ul class="vegas-slides hidden">
<li><img alt="" data-fade="2000" src="assets/img/BG/bg1.jpg"/></li>
<li><img alt="" data-fade="2000" src="assets/img/BG/bg2.jpg"/></li>
<li><img alt="" data-fade="2000" src="assets/img/BG/bg3.jpg"/></li>
<li><img alt="" data-fade="2000" src="assets/img/BG/bg4.jpg"/></li>
</ul>
<!--=================================
JPlayer (Audio Player)
=================================-->
<section id="audio-player">
<div class="container">
<div class="rock-player">
<div class="playListTrigger">
<a href="#"><i class="fa fa-list"></i></a>
</div><!--triggerPlayList in responsive-->
<div class="row">
<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="jp-jplayer" id="player-instance"></div>
<div class="controls">
<div class="jp-prev"></div>
<div class="play-pause jp-play"></div>
<div class="play-pause jp-pause" style="display:none"></div>
<div class="jp-next"></div>
</div>
<!--controls-->
</div>
<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
<div class="player-status">
<h5 class="audio-title">Vidup Agrahari</h5>
<div class="audio-timer"><span class="current-time jp-current-time">01:02</span> / <span class="total-time jp-duration">4:05</span></div>
<div class="audio-progress">
<div class="jp-seek-bar">
<div class="audio-play-bar jp-play-bar" style="width:20%;"></div>
</div>
<!--jp-seek-bar-->
</div>
<!--audio-progress-->
</div>
<!--player-status-->
</div><!--column-->
</div>
<!--inner-row-->
</div>
<!--column-->
<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
<div class="audio-list">
<div class="audio-list-icon"></div>
<div class="jp-playlist">
<!--Add Songs In mp3 formate here-->
<ul class="hidden playlist-files">
<li data-artist="Vidup Agrahari" data-mp3="jane_kya/audio1.mp3" data-title="Jane Kya Dekh Liya - Jane Kya Dekh Liya"></li>
<li data-artist="Vidup Agrahari" data-mp3="jane_kya/audio2.mp3" data-title="Jane Kya Dekh Liya - Jane Kya Dekh Liya"></li>
<li data-artist="Vidup Agrahari" data-mp3="jane_kya/audio3.mp3" data-title="Jane Kya Dekh Liya"></li>
<li data-artist="Vidup Agrahari" data-mp3="jane_kya/audio4.mp3" data-title="Jane Kya Dekh Liya"></li>
<li data-artist="Vidup Agrahari" data-mp3="jane_kya/audio5.mp3" data-title="Jane Kya Dekh Liya"></li>
<li data-artist="Vidup Agrahari" data-mp3="jane_kya/audio6.mp3" data-title="Jane Kya Dekh Liya"></li>
<li data-artist="Vidup Agrahari" data-mp3="jane_kya/audio7.mp3" data-title="Jane Kya Dekh Liya"></li>
</ul>
<!--Playlist ends-->
<h5>Audio Playlist</h5>
<div class="audio-track">
<ul>
<li></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<!--row-->
</div>
</div>
</section>
<div id="ajaxArea">
<div class="pageContentArea">
<!--=================================
      Home
      =================================-->
<section id="home-slider">
<div class="container">
<div class="home-inner">
<div class="slider-nav" id="homeSliderNav">
<a class="prev fa fa-chevron-left" href="#" id="home-prev"></a>
<a class="next fa fa-chevron-right" href="#" id="home-next"></a>
</div><!--sliderNav-->
<div class="flexslider" data-animation="slide" data-animationspeed="1000" data-autoplay="true" data-slideshowspeed="7000" id="flex-home">
<ul class="slides">
<li> <img alt="Vidup Agrahari" src="assets/img/slider/slider.jpg"/>
</li>
<li> <img alt="Vidup Agrahari" src="assets/img/slider/slider-1.jpg"/>
</li>
<li> <img alt="Vidup Agrahari" src="assets/img/slider/slider-2.jpg"/>
</li>
<li> <img alt="Vidup Agrahari" src="assets/img/slider/slider-3.jpg"/>
</li>
</ul>
</div>
</div>
</div>
</section>
<!--=================================
      Player Wraper
      =================================-->
<div class="rockPlayerHolder"></div>
<!--=================================
      Latest Albumbs
      =================================-->
<section id="albums">
<div class="container">
<h1></h1>
<div class="top-carouselnav"> <a class="prev-album" href="#"><span class="fa fa-caret-left"></span></a> <a class="next-album" href="#"><span class="fa fa-caret-right"></span></a> </div>
<div class="albums-carousel">
<div class="album"> <img alt="Gallery" src="assets/img/albums/1.jpg"/>
<div class="hover">
<ul>
<li><a data-rel="prettyPhoto" href="assets/img/albums/1.jpg"><span class="fa fa-search"></span></a></li>
<li><a href="album-detail.html"><span class="fa fa-link"></span></a></li>
</ul>
<h3>Gallery</h3>
<h2>Click to see Photo Gallery </h2>
</div>
</div>
<!--\\album-->
<div class="album"> <img alt="Vidup Agrahari-Jaane Kya Dekh Liya" src="assets/img/albums/2.jpg"/>
<div class="hover">
<ul>
<li><a href="album-detail.html"><span class="fa fa-link"></span></a></li>
</ul>
<h3>Video</h3>
<h2>Jaane Kya Dekh Liya</h2>
</div>
</div>
<!--\\album-->
<div class="album"> <img alt="Vidup Agrahari Biography" src="assets/img/albums/3.jpg"/>
<div class="hover">
<ul>
<li><a href="biography.html"><span class="fa fa-link"></span></a></li>
</ul>
<h3>Biography</h3>
<h2>Read More...</h2>
</div>
</div>
<!--\\album-->
<div class="album"> <img alt="Win a Free CD" src="assets/img/albums/4.jpg"/>
<div class="hover">
<ul>
<li><a href="codes.pdf"><span class="fa fa-download"></span></a></li>
</ul>
<h3>Win a Free CD</h3>
<h2>Download Codes</h2>
</div>
</div>
<!--\\album-->
</div>
</div>
</section>
<div class="clearfix"></div>
<!--=================================
      Upcoming events
      =================================-->
</div><!--pageContent-->
</div><!--ajaxwrap-->
<!--=================================
	Latest blog
	=================================-->
<!--=================================
	Footer
	=================================-->
<footer>
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-3">
<h4><span class="fa fa-sitemap"></span>browse around</h4>
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6">
<ul class="sitemap">
<li><a href="#">HOME</a></li>
<li><a href="#">NEWS</a></li>
<li><a href="#">events</a></li>
</ul>
</div>
<div class="col-lg-6 col-md-6 col-sm-6">
<ul class="sitemap">
<li><a href="#">VIDEOS</a></li>
<li><a href="#">GALLERY</a></li>
<li><a href="#">CONTACT</a></li>
</ul>
</div>
</div>
</div>
<!--\\column-->
<div class="col-lg-3 col-md-3 col-sm-3">
<h4><span class="fa fa-facebook"></span>Latest </h4>
</div>
<!--\\column-->
<div class="col-lg-3 col-md-3 col-sm-3">
<h4><span class="fa fa-facebook-square"></span>Facebook Feed</h4>
</div>
<!--\\column-->
<div class="col-lg-3 col-md-3 col-sm-3">
<h4><span class="fa fa-envelope"></span>newsletter signup</h4>
<form id="newsletter">
<input placeholder="Enter your email address here"/>
<button class="submit-btn">submit</button>
</form>
</div>
<!--\\column-->
</div>
<!--\\row-->
</div>
<!--\\container-->
</footer>
<!--=================================
Script Source
=================================-->
<script defer="" src="assets/js/bootstrap.min.js"></script>
<script defer="" src="assets/js/jquery.easing-1.3.pack.js"></script>
<script defer="" src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script defer="" src="assets/js/jquery.mousewheel.min.js"></script>
<script defer="" src="assets/js/jflickrfeed.min.js"></script>
<script defer="" src="assets/js/jquery.flexslider-min.js"></script>
<script defer="" src="assets/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script defer="" src="assets/js/tweetie.min.js"></script>
<script defer="" src="assets/js/jquery.prettyPhoto.js"></script>
<script defer="" src="assets/jPlayer/jquery.jplayer.min.js"></script>
<script defer="" src="assets/jPlayer/add-on/jplayer.playlist.min.js"></script>
<script defer="" src="assets/js/jquery.vegas.min.js"></script>
<script defer="" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
<script defer="" src="assets/js/jquery.calendar-widget.js"></script>
<script defer="" src="assets/js/isotope.js"></script>
<script defer="" src="assets/js/main.js"></script>
<script>/*Place Your Google Analytics code here*/</script>
</body>
</html>
