<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>[1509.05637] The Shortest Path Problem with Edge Information Reuse is NP-Complete</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://static.arxiv.org/static/browse/0.3.2.6/images/icons/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://static.arxiv.org/static/browse/0.3.2.6/css/arXiv.css?v=20200727" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://static.arxiv.org/static/browse/0.3.2.6/css/arXiv-print.css?v=20200611" media="print" rel="stylesheet" type="text/css"/>
<link href="https://static.arxiv.org/static/browse/0.3.2.6/css/browse_search.css" media="screen" rel="stylesheet" type="text/css"/>
<script language="javascript" src="https://static.arxiv.org/static/browse/0.3.2.6/js/accordion.js"></script>
<!-- Pendo -->
<script>
  (function(apiKey){
      (function(p,e,n,d,o){var v,w,x,y,z;o=p[d]=p[d]||{};o._q=[];
      v=['initialize','identify','updateOptions','pageLoad','track'];for(w=0,x=v.length;w<x;++w)(function(m){
          o[m]=o[m]||function(){o._q[m===v[0]?'unshift':'push']([m].concat([].slice.call(arguments,0)));};})(v[w]);
          y=e.createElement(n);y.async=!0;y.src='https://content.analytics.arxiv.org/agent/static/'+apiKey+'/pendo.js';
          z=e.getElementsByTagName(n)[0];z.parentNode.insertBefore(y,z);})(window,document,'script','pendo');

          // Call this whenever information about your visitors becomes available
          // Please use Strings, Numbers, or Bools for value types.
          pendo.initialize({
              visitor: {
                  

                  
                  continent:       'AS',
                  

                  
                  country:         'CN',
                  

                  

                  

                  id:              'VISITOR-UNIQUE-ID'
                  // email:        // Recommended if using Pendo Feedback, or NPS Email
                  // full_name:    // Recommended if using Pendo Feedback
                  // role:         // Optional

                  // You can add any additional visitor level key-values here,
                  // as long as it's not one of the above reserved names.
              },

              account: {
                  

                  id:              'ACCOUNT-UNIQUE-ID'

                  // name:         // Optional
                  // is_paying:    // Recommended if using Pendo Feedback
                  // monthly_value:// Recommended if using Pendo Feedback
                  // planLevel:    // Optional
                  // planPrice:    // Optional
                  // creationDate: // Optional

                  // You can add any additional account level key-values here,
                  // as long as it's not one of the above reserved names.
              }
          });
  })('d6494389-b427-4103-7c76-03182ecc8e60');
  </script>
<link href="https://static.arxiv.org/js/bibex-dev/bibex.css?20200709" media="screen" rel="stylesheet" type="text/css"/>
<script src="https://static.arxiv.org/static/browse/0.3.2.6/js/mathjaxToggle.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js" type="text/javascript"></script>
<script src="https://static.arxiv.org/static/browse/0.3.2.6/js/toggle-labs.js" type="text/javascript"></script>
<script src="https://static.arxiv.org/static/browse/0.3.2.6/js/cite.js" type="text/javascript"></script><script src="https://arxiv-org.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/zca7yc/b/13/a44af77267a987a660377e5c46e0fb64/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&amp;collectorId=7a8da419" type="text/javascript"></script>
<script type="text/javascript">window.ATL_JQ_PAGE_PROPS =  {
  "triggerFunction": function(showCollectorDialog) {
    //Requires that jQuery is available!
    jQuery("#feedback-button").click(function(e) {
      e.preventDefault();
      showCollectorDialog();
    });
  },
  fieldValues: {
    "components": ["15700"],  // Jira ID for browse component
    "versions": ["14251"],    // Jira ID for browse-0.3.2 release
    "customfield_11401": window.location.href
  }
  };
</script>
<meta content="The Shortest Path Problem with Edge Information Reuse is NP-Complete" name="citation_title"/>
<meta content="Träff, Jesper Larsson" name="citation_author"/>
<meta content="2015/09/18" name="citation_date"/>
<meta content="2016/07/15" name="citation_online_date"/>
<meta content="https://arxiv.org/pdf/1509.05637" name="citation_pdf_url"/>
<meta content="1509.05637" name="citation_arxiv_id"/><meta content="@arxiv" name="twitter:site"/>
<meta content="The Shortest Path Problem with Edge Information Reuse is NP-Complete" property="twitter:title"/>
<meta content="We show that the following variation of the single-source shortest path
problem is NP-complete. Let a weighted, directed, acyclic graph $G=(V,E,w)$
with source and sink vertices $s$ and $t$ be..." property="twitter:description"/>
<meta content="arXiv.org" property="og:site_name"/>
<meta content="The Shortest Path Problem with Edge Information Reuse is NP-Complete" property="og:title"/>
<meta content="https://arxiv.org/abs/1509.05637v3" property="og:url"/>
<meta content="We show that the following variation of the single-source shortest path
problem is NP-complete. Let a weighted, directed, acyclic graph $G=(V,E,w)$
with source and sink vertices $s$ and $t$ be given. Let in addition a mapping
$f$ on $E$ be given that associates information with the edges (e.g., a
pointer), such that $f(e)=f(e')$ means that edges $e$ and $e'$ carry the same
information; for such edges it is required that $w(e)=w(e')$. The length of a
simple $st$ path $U$ is the sum of the weights of the edges on $U$ but edges
with $f(e)=f(e')$ are counted only once. The problem is to determine a shortest
such $st$ path. We call this problem the \emph{edge information reuse shortest
path problem}. It is NP-complete by reduction from 3SAT." property="og:description"/>
</head>
<body class="with-cu-identity">
<aside class="slider-wrapper" style="display:none">
<a class="close-slider" href="#"><img alt="close this message" src="https://static.arxiv.org/static/browse/0.3.2.6/images/icons/close-slider.png"/></a>
<div class="copy-donation">
<h1>Donate to arXiv</h1>
<p>
      Please join the <a href="https://simonsfoundation.org">Simons Foundation</a> and our
      generous <a href="https://arxiv.org/about/ourmembers">member organizations</a>
      in supporting arXiv during our giving campaign September 23-27. 100% of your contribution will fund
      improvements and new initiatives to benefit arXiv's global scientific community.
    </p>
</div>
<div class="amount-donation">
<div class="wrapper">
<div class="donate-cta"><a class="banner_link" href="https://bit.ly/arXivDONATE1"><b>DONATE</b></a>
<p>[secure site, no need to create account]</p>
</div>
</div>
</div>
</aside><div class="flex-wrap-footer">
<header>
<a class="is-sr-only" href="#content">Skip to main content</a>
<!-- start desktop header -->
<div class="columns is-vcentered is-hidden-mobile" id="cu-identity">
<div class="column" id="cu-logo">
<a href="https://www.cornell.edu/"><img alt="Cornell University" src="https://static.arxiv.org/static/browse/0.3.2.6/images/icons/cu/cornell-reduced-white-SMALL.svg"/></a>
</div>
<!-- timeboxed column --><!-- /end timeboxed column -->
<div class="column" id="support-ack">
<a href="https://confluence.cornell.edu/x/ALlRF">We gratefully acknowledge support from<br/>the Simons Foundation and member institutions.</a>
</div>
</div>
<div class="is-hidden-mobile" id="header">
<a aria-hidden="true" href="{url_path('ignore_me')}"></a>
<div class="header-breadcrumbs is-hidden-mobile"><a href="/">arXiv.org</a> &gt; <a href="/list/cs/recent">cs</a> &gt; arXiv:1509.05637</div>
<div class="search-block level-right">
<form action="https://arxiv.org/search" class="level-item mini-search" method="GET">
<div class="field has-addons">
<div class="control">
<input aria-label="Search term or terms" class="input is-small" name="query" placeholder="Search..." type="text"/>
<p class="help"><a href="https://arxiv.org/help">Help</a> | <a href="https://arxiv.org/search/advanced">Advanced Search</a></p>
</div>
<div class="control">
<div class="select is-small">
<select aria-label="Field to search" name="searchtype">
<option selected="selected" value="all">All fields</option>
<option value="title">Title</option>
<option value="author">Author</option>
<option value="abstract">Abstract</option>
<option value="comments">Comments</option>
<option value="journal_ref">Journal reference</option>
<option value="acm_class">ACM classification</option>
<option value="msc_class">MSC classification</option>
<option value="report_num">Report number</option>
<option value="paper_id">arXiv identifier</option>
<option value="doi">DOI</option>
<option value="orcid">ORCID</option>
<option value="author_id">arXiv author ID</option>
<option value="help">Help pages</option>
<option value="full_text">Full text</option>
</select>
</div>
</div>
<input name="source" type="hidden" value="header"/>
<button class="button is-small is-cul-darker">Search</button>
</div>
</form>
</div>
</div><!-- /end desktop header -->
<div class="mobile-header">
<div class="columns is-mobile">
<div class="column logo-arxiv"><a href="https://arxiv.org/"><img alt="arXiv" src="https://static.arxiv.org/static/browse/0.3.2.6/images/arxiv-logo.png"/></a></div>
<div class="column logo-cornell"><a href="https://www.cornell.edu/">
<picture>
<source media="(min-width: 501px)" sizes="400w" srcset="https://static.arxiv.org/static/browse/0.3.2.6/images/icons/cu/cornell-reduced-white-SMALL.svg  400w"/>
<source srcset="https://static.arxiv.org/static/browse/0.3.2.6/images/icons/cu/cornell_seal_simple_black.svg 2x"/>
<img alt="Cornell University Logo" src="https://static.arxiv.org/static/browse/0.3.2.6/images/icons/cu/cornell-reduced-white-SMALL.svg"/>
</picture>
</a></div>
<div class="column nav" id="toggle-container" role="menubar">
<button class="toggle-control"><svg class="icon filter-white" viewbox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><title>open search</title><path d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path></svg></button>
<div class="mobile-toggle-block toggle-target">
<form action="https://arxiv.org/search" class="mobile-search-form" method="GET">
<div class="field has-addons">
<input aria-label="Search term or terms" class="input" name="query" placeholder="Search..." type="text"/>
<input name="source" type="hidden" value="header"/>
<input name="searchtype" type="hidden" value="all"/>
<button class="button">GO</button>
</div>
</form>
</div>
<button class="toggle-control"><svg class="icon filter-white" role="menu" viewbox="0 0 448 512" xmlns="http://www.w3.org/2000/svg"><title>open navigation menu</title><path d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"></path></svg></button>
<div class="mobile-toggle-block toggle-target">
<nav aria-labelledby="mobilemenulabel" class="mobile-menu">
<h2 id="mobilemenulabel">quick links</h2>
<ul>
<li><a href="https://arxiv.org/login">Login</a></li>
<li><a href="https://arxiv.org/help">Help Pages</a></li>
<li><a href="https://arxiv.org/about">About</a></li>
</ul>
</nav>
</div>
</div>
</div>
</div><!-- /end mobile-header -->
</header>
<main>
<div id="content">
<!--
rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:dc="http://purl.org/dc/elements/1.1/"
         xmlns:trackback="http://madskills.com/public/xml/rss/module/trackback/">
    <rdf:Description
        rdf:about="/abs/1509.05637"
        dc:identifier="/abs/1509.05637"
        dc:title="The Shortest Path Problem with Edge Information Reuse is NP-Complete"
        trackback:ping="/trackback/1509.05637" />
    </rdf:RDF>
-->
<div id="abs-outer">
<div class="leftcolumn">
<div class="subheader">
<h1>Computer Science &gt; Computational Complexity</h1>
</div>
<div class="header-breadcrumbs-mobile">
<strong>arXiv:1509.05637</strong> (cs)
    </div>
<link href="https://static.arxiv.org/static/base/0.16.9/css/abs.css" rel="stylesheet" type="text/css"/>
<div id="content-inner">
<div id="abs">
<div class="dateline">
  
  
  
    
  
  
    
    
  

  [Submitted on 18 Sep 2015 (<a href="https://arxiv.org/abs/1509.05637v1">v1</a>), last revised 15 Jul 2016 (this version, v3)]</div>
<h1 class="title mathjax"><span class="descriptor">Title:</span>The Shortest Path Problem with Edge Information Reuse is NP-Complete</h1>
<div class="authors"><span class="descriptor">Authors:</span><a href="https://arxiv.org/search/cs?searchtype=author&amp;query=Tr%C3%A4ff%2C+J+L">Jesper Larsson Träff</a></div>
<a class="mobile-submission-download" href="/pdf/1509.05637">Download PDF</a>
<blockquote class="abstract mathjax">
<span class="descriptor">Abstract:</span>  We show that the following variation of the single-source shortest path
problem is NP-complete. Let a weighted, directed, acyclic graph $G=(V,E,w)$
with source and sink vertices $s$ and $t$ be given. Let in addition a mapping
$f$ on $E$ be given that associates information with the edges (e.g., a
pointer), such that $f(e)=f(e')$ means that edges $e$ and $e'$ carry the same
information; for such edges it is required that $w(e)=w(e')$. The length of a
simple $st$ path $U$ is the sum of the weights of the edges on $U$ but edges
with $f(e)=f(e')$ are counted only once. The problem is to determine a shortest
such $st$ path. We call this problem the \emph{edge information reuse shortest
path problem}. It is NP-complete by reduction from 3SAT.

    </blockquote>
<!--CONTEXT-->
<div class="metatable">
<table summary="Additional metadata"><tr>
<td class="tablecell label">Subjects:</td>
<td class="tablecell subjects">
<span class="primary-subject">Computational Complexity (cs.CC)</span></td>
</tr><tr>
<td class="tablecell label">Cite as:</td>
<td class="tablecell arxivid"><span class="arxivid"><a href="https://arxiv.org/abs/1509.05637">arXiv:1509.05637</a> [cs.CC]</span></td>
</tr>
<tr>
<td class="tablecell label"> </td>
<td class="tablecell arxividv">(or <span class="arxivid">
<a href="https://arxiv.org/abs/1509.05637v3">arXiv:1509.05637v3</a> [cs.CC]</span> for this version)
          </td>
</tr>
</table>
</div>
</div>
</div>
<div class="submission-history">
<h2>Submission history</h2> From: Jesper Larsson Träff [<a href="/show-email/b31b0076/1509.05637">view email</a>]
      <br/>
<strong><a href="/abs/1509.05637v1">[v1]</a></strong>
  Fri, 18 Sep 2015 14:17:59 UTC (5 KB)<br/>
<strong><a href="/abs/1509.05637v2">[v2]</a></strong>
  Thu, 9 Jun 2016 14:39:53 UTC (6 KB)<br/><strong>[v3]</strong>
Fri, 15 Jul 2016 10:34:08 UTC (6 KB)<br/></div>
</div>
<!--end leftcolumn-->
<div class="extra-services">
<div class="full-text">
<a name="other"></a>
<span class="descriptor">Full-text links:</span>
<h2>Download:</h2>
<ul>
<li><a accesskey="f" class="abs-button download-pdf" href="/pdf/1509.05637">PDF</a></li>
<li><a class="abs-button download-ps" href="/ps/1509.05637">PostScript</a></li>
<li><a class="abs-button download-format" href="/format/1509.05637">Other formats</a></li></ul>
<div class="abs-license">(<a href="http://arxiv.org/licenses/nonexclusive-distrib/1.0/" title="Rights to this article">license</a>)</div>
</div>
<!--end full-text-->
<div class="browse">
    Current browse context: <div class="current">cs.CC</div>
<div class="prevnext">
<span class="arrow">
<a accesskey="p" class="abs-button prev-url" href="/prevnext?id=1509.05637&amp;function=prev&amp;context=cs.CC" title="previous in cs.CC (accesskey p)">&lt; prev</a>
</span>
<span class="is-hidden-mobile">  |  </span>
<span class="arrow">
<a accesskey="n" class="abs-button next-url" href="/prevnext?id=1509.05637&amp;function=next&amp;context=cs.CC" title="next in cs.CC (accesskey n)">next &gt;</a>
</span><br/>
</div><div class="list">
<a class="abs-button abs-button-grey abs-button-small context-new" href="/list/cs.CC/new">new</a>
<span class="is-hidden-mobile"> | </span>
<a class="abs-button abs-button-grey abs-button-small context-recent" href="/list/cs.CC/recent">recent</a>
<span class="is-hidden-mobile"> | </span>
<a class="abs-button abs-button-grey abs-button-small context-id" href="/list/cs.CC/1509">1509</a>
</div><div class="abs-switch-cat">
    Change to browse by:
    <div class="switch context-change">
<a href="/abs/1509.05637?context=cs">cs</a><br class="is-hidden-mobile"/>
</div>
</div>
</div>
<div class="extra-ref-cite">
<h3>References &amp; Citations</h3>
<ul>
<li><a class="abs-button abs-button-small cite-ads" href="https://ui.adsabs.harvard.edu/abs/arXiv:1509.05637">NASA ADS</a></li><li><a class="abs-button abs-button-small cite-google-scholar" href="https://scholar.google.com/scholar?q=The%20Shortest%20Path%20Problem%20with%20Edge%20Information%20Reuse%20is%20NP-Complete.%20arXiv%202016" rel="noopener" target="_blank">Google Scholar</a></li>
<li><a class="abs-button abs-button-small cite-semantic-scholar" href="https://api.semanticscholar.org/arXiv:1509.05637" rel="noopener" target="_blank">Semantic Scholar</a></li>
</ul>
<div style="clear:both;"></div>
</div>
<div class="dblp">
<h3><a href="https://dblp.uni-trier.de">DBLP</a> - CS Bibliography</h3>
<div class="list">
<a href="https://dblp.uni-trier.de/db/journals/corr/corr1509.html#Traff15" title="listing on DBLP">listing</a> | <a href="https://dblp.uni-trier.de/rec/bibtex/journals/corr/Traff15" title="DBLP bibtex record">bibtex</a>
</div>
<div class="list">
<a href="https://dblp.uni-trier.de/search/author?author=Jesper%20Larsson%20Tr%C3%A4ff" title="DBLP author search">Jesper Larsson Träff</a>
</div>
</div><div class="extra-ref-cite">
<a hidden="true" href="https://static.arxiv.org/static/browse/0.3.2.6/css/cite.css" id="bib-cite-css">a</a>
<span class="bib-cite-button abs-button" id="bib-cite-trigger">export bibtex citation</span>
<span hidden="true" id="bib-cite-loading">Loading...</span>
</div>
<div class="bib-modal" hidden="true" id="bib-cite-modal">
<div class="bib-modal-content">
<div class="bib-modal-title">
<h2>Bibtex formatted citation</h2>
<span class="bib-modal-close">×</span>
</div>
<div>
<textarea aria-label="loading the citation" class="bib-citation-content" id="bib-cite-target">loading...</textarea>
</div>
<div>
<span>Data provided by: </span>
<a id="bib-cite-source-api"></a>
</div>
</div>
</div>
<div class="bookmarks">
<div><h3>Bookmark</h3></div><a class="abs-button abs-button-grey abs-button-small" href="https://arxiv.org/ct?url=http%3A%2F%2Fwww.bibsonomy.org%2FBibtexHandler%3FrequTask%3Dupload%26url%3Dhttps%3A%2F%2Farxiv.org%2Fabs%2F1509.05637%26description%3DThe+Shortest+Path+Problem+with+Edge+Information+Reuse+is+NP-Complete&amp;v=11638319" title="Bookmark on BibSonomy">
<img alt="BibSonomy logo" src="https://static.arxiv.org/static/browse/0.3.2.6/images/icons/social/bibsonomy.png"/>
</a>
<a class="abs-button abs-button-grey abs-button-small" href="https://arxiv.org/ct?url=https%3A%2F%2Fwww.mendeley.com%2Fimport%2F%3Furl%3Dhttps%3A%2F%2Farxiv.org%2Fabs%2F1509.05637&amp;v=dfe9a4f2" title="Bookmark on Mendeley">
<img alt="Mendeley logo" src="https://static.arxiv.org/static/browse/0.3.2.6/images/icons/social/mendeley.png"/>
</a>
<a class="abs-button abs-button-grey abs-button-small" href="https://arxiv.org/ct?url=https%3A%2F%2Freddit.com%2Fsubmit%3Furl%3Dhttps%3A%2F%2Farxiv.org%2Fabs%2F1509.05637%26title%3DThe+Shortest+Path+Problem+with+Edge+Information+Reuse+is+NP-Complete&amp;v=8cc9bbbb" title="Bookmark on Reddit">
<img alt="Reddit logo" src="https://static.arxiv.org/static/browse/0.3.2.6/images/icons/social/reddit.png"/>
</a>
<a class="abs-button abs-button-grey abs-button-small" href="https://arxiv.org/ct?url=http%3A%2F%2Fsciencewise.info%2Fbookmarks%2Fadd%3Furl%3Dhttps%3A%2F%2Farxiv.org%2Fabs%2F1509.05637&amp;v=8e740670" title="Bookmark on ScienceWISE">
<img alt="ScienceWISE logo" src="https://static.arxiv.org/static/browse/0.3.2.6/images/icons/social/sciencewise.png"/>
</a>
</div>
</div>
<!--end extra-services-->
<!-- LABS AREA -->
<div id="labstabs">
<div class="labstabs"><input checked="checked" id="tabone" name="tabs" type="radio"/>
<label for="tabone">
      About arXivLabs
    </label>
<div class="tab">
<h1>arXivLabs: experimental projects with community collaborators</h1>
<div class="toggle">
<p>arXivLabs is a framework that allows collaborators to develop and share new arXiv features directly on our website.</p>
<p>Both individuals and organizations that work with arXivLabs have embraced and accepted our values of openness, community, excellence, and user data privacy. arXiv is committed to these values and only works with partners that adhere to them.</p>
<p>Have an idea for a project that will add value for arXiv's community? <a href="https://labs.arxiv.org/"><strong>Learn more about arXivLabs</strong></a> and <a href="https://arxiv.org/about/people/developers"><strong>how to get involved</strong></a>.</p>
</div>
</div>
<input id="tabtwo" name="tabs" type="radio"/>
<label for="tabtwo">Bibliographic Tools</label>
<div class="tab labs-display-bib">
<h1>Bibliographic and Citation Tools</h1>
<div class="toggle">
<div class="columns is-mobile lab-row">
<div class="column lab-switch">
<label class="switch">
<input class="lab-toggle" id="bibex-toggle" type="checkbox"/>
<span class="slider"></span>
<span class="is-sr-only">Bibliographic Explorer Toggle</span>
</label>
</div>
<div class="column lab-name">
<span id="label-for-bibex">Bibliographic Explorer</span> <em>(<a href="https://labs.arxiv.org/">What is the Explorer?</a>)</em>
</div>
</div>
</div>
<div class="labs-content-placeholder labs-display" style="display: none;"></div>
</div>
<input id="tabthree" name="tabs" type="radio"/>
<label for="tabthree">Code</label>
<div class="tab">
<h1>Code Associated with this Article</h1>
<div class="toggle">
<div class="columns is-mobile lab-row">
<div class="column lab-switch">
<label class="switch">
<input aria-labelledby="label-for-pwc" class="lab-toggle" data-script-url="https://static.arxiv.org/static/browse/0.3.2.6/js/paperswithcode.js" id="paperwithcode-toggle" type="checkbox"/>
<span class="slider"></span>
<span class="is-sr-only">arXiv Links to Code Toggle</span>
</label>
</div>
<div class="column lab-name">
<span id="label-for-pwc">arXiv Links to Code</span> <em>(<a href="https://labs.arxiv.org/" target="_blank">What is Links to Code?</a>)</em>
</div>
</div>
</div>
<div id="pwc-output"></div>
</div>
<input id="tabfour" name="tabs" type="radio"/>
<label for="tabfour">Recommenders</label>
<div class="tab">
<h1>Recommenders and Search Tools</h1>
<div class="toggle">
<div class="columns is-mobile lab-row">
<div class="column lab-switch">
<label class="switch">
<input aria-labelledby="label-for-core" class="lab-toggle" id="core-recommender-toggle" type="checkbox"/>
<span class="slider"></span>
<span class="is-sr-only">Core recommender toggle</span>
</label>
</div>
<div class="column lab-name">
<span id="label-for-core">CORE Recommender</span> <em>(<a href="https://labs.arxiv.org/">What is CORE?</a>)</em>
</div>
</div>
</div>
<div id="coreRecommenderOutput"></div>
</div>
</div>
</div>
<!-- END LABS AREA -->
<div class="endorsers">
<a class="endorser-who" href="/auth/show-endorsers/1509.05637">Which authors of this paper are endorsers?</a> |
    <a href="javascript:setMathjaxCookie()" id="mathjax_toggle">Disable MathJax</a> (<a href="https://arxiv.org/help/mathjax">What is MathJax?</a>)
    <span class="help" style="font-style: normal; float: right; margin-top: 0; margin-right: 1em;"></span>
</div>
<script language="javascript" type="text/javascript">mathjaxToggle();</script>
</div>
</div>
</main>
<footer style="clear: both;">
<div aria-label="Secondary" class="columns is-desktop" role="navigation" style="margin: -0.75em -0.75em 0.75em -0.75em">
<!-- Macro-Column 1 -->
<div class="column" style="padding: 0;">
<div class="columns">
<div class="column">
<ul style="list-style: none; line-height: 2;">
<li><a href="https://arxiv.org/about">About</a></li>
<li><a href="https://arxiv.org/help">Help</a></li>
</ul>
</div>
<div class="column">
<ul style="list-style: none; line-height: 2;">
<li>
<svg class="icon filter-black" role="presentation" viewbox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><title>contact arXiv</title><desc>Click here to contact arXiv</desc><path d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z"></path></svg>
<a href="https://arxiv.org/help/contact"> Contact</a>
</li>
<li>
<svg class="icon filter-black" role="presentation" viewbox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><title>subscribe to arXiv mailings</title><desc>Click here to subscribe</desc><path d="M476 3.2L12.5 270.6c-18.1 10.4-15.8 35.6 2.2 43.2L121 358.4l287.3-253.2c5.5-4.9 13.3 2.6 8.6 8.3L176 407v80.5c0 23.6 28.5 32.9 42.5 15.8L282 426l124.6 52.2c14.2 6 30.4-2.9 33-18.2l72-432C515 7.8 493.3-6.8 476 3.2z"></path></svg>
<a href="https://arxiv.org/help/subscribe"> Subscribe</a>
</li>
</ul>
</div>
</div>
</div>
<!-- End Macro-Column 1 -->
<!-- Macro-Column 2 -->
<div class="column" style="padding: 0;">
<div class="columns">
<div class="column">
<ul style="list-style: none; line-height: 2;">
<li><a href="https://arxiv.org/help/license">Copyright</a></li>
<li><a href="https://arxiv.org/help/policies/privacy_policy">Privacy Policy</a></li>
</ul>
</div>
<div class="column sorry-app-links">
<ul style="list-style: none; line-height: 2;">
<li><a href="https://arxiv.org/help/web_accessibility">Web Accessibility Assistance</a></li>
<li>
<p class="help">
<a class="a11y-main-link" href="https://status.arxiv.org" target="_blank">arXiv Operational Status <svg class="icon filter-dark_grey" role="presentation" viewbox="0 0 256 512" xmlns="http://www.w3.org/2000/svg"><path d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path></svg></a><br/>
                    Get status notifications via
                    <a class="is-link" href="https://subscribe.sorryapp.com/24846f03/email/new" target="_blank"><svg class="icon filter-black" role="presentation" viewbox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z"></path></svg>email</a>
                    or <a class="is-link" href="https://subscribe.sorryapp.com/24846f03/slack/new" target="_blank"><svg class="icon filter-black" role="presentation" viewbox="0 0 448 512" xmlns="http://www.w3.org/2000/svg"><path d="M94.12 315.1c0 25.9-21.16 47.06-47.06 47.06S0 341 0 315.1c0-25.9 21.16-47.06 47.06-47.06h47.06v47.06zm23.72 0c0-25.9 21.16-47.06 47.06-47.06s47.06 21.16 47.06 47.06v117.84c0 25.9-21.16 47.06-47.06 47.06s-47.06-21.16-47.06-47.06V315.1zm47.06-188.98c-25.9 0-47.06-21.16-47.06-47.06S139 32 164.9 32s47.06 21.16 47.06 47.06v47.06H164.9zm0 23.72c25.9 0 47.06 21.16 47.06 47.06s-21.16 47.06-47.06 47.06H47.06C21.16 243.96 0 222.8 0 196.9s21.16-47.06 47.06-47.06H164.9zm188.98 47.06c0-25.9 21.16-47.06 47.06-47.06 25.9 0 47.06 21.16 47.06 47.06s-21.16 47.06-47.06 47.06h-47.06V196.9zm-23.72 0c0 25.9-21.16 47.06-47.06 47.06-25.9 0-47.06-21.16-47.06-47.06V79.06c0-25.9 21.16-47.06 47.06-47.06 25.9 0 47.06 21.16 47.06 47.06V196.9zM283.1 385.88c25.9 0 47.06 21.16 47.06 47.06 0 25.9-21.16 47.06-47.06 47.06-25.9 0-47.06-21.16-47.06-47.06v-47.06h47.06zm0-23.72c-25.9 0-47.06-21.16-47.06-47.06 0-25.9 21.16-47.06 47.06-47.06h117.84c25.9 0 47.06 21.16 47.06 47.06 0 25.9-21.16 47.06-47.06 47.06H283.1z"></path></svg>slack</a>
</p>
</li>
</ul>
</div>
</div>
</div> <!-- end MetaColumn 2 -->
<!-- End Macro-Column 2 -->
</div>
</footer>
</div>
</body>
</html>