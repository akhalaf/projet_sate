<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Punto Pymes - Desarrolladores de Software</title>
<base href="/"/>
<!-- Meta Facebook -->
<meta content="website" property="og:type"/>
<meta content="https://www.sofpymes.com/" property="og:url"/>
<meta content="Punto Pymes - Desarrolladores de Software" property="og:title"/>
<meta content="FÃ¡brica de software reconocida a nivel nacional y lÃ­der en nuestra regiÃ³n por la creaciÃ³n de soluciones empresariales para pymes por la creatividad, innovaciÃ³n en nuestro portafolio de productos" property="og:description"/>
<meta content="https://drive.google.com/open?id=15Rqx2hoxpiQoR7bN5eioTO2WIjJaFv8r" property="og:image"/>
<!-- Meta en google -->
<meta content="Punto Pymes, punto pymes, Billingsof, billingsof" name="keywords"/>
<meta content="Punto Pymes - Desarrolladores de Software" name="title"/>
<meta content="FÃ¡brica de software reconocida a nivel nacional y lÃ­der en nuestra regiÃ³n por la creaciÃ³n de soluciones empresariales para pymes por la creatividad, innovaciÃ³n en nuestro portafolio de productos" name="description"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- kit de font awesome -->
<script crossorigin="anonymous" src="https://kit.fontawesome.com/18a6bf84f4.js"></script>
<!-- <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v6.0"></script> -->
<!-- Tipografias -->
<!-- Antigua Interfaz -->
<link href="https://fonts.googleapis.com/css2?family=Amatic+SC&amp;family=Archivo+Narrow&amp;family=Josefin+Slab:wght@100&amp;family=Monoton&amp;family=MuseoModerno:wght@200&amp;family=Oswald:wght@300;400&amp;family=Play:wght@700&amp;family=Poiret+One&amp;family=Rowdies:wght@300&amp;display=swap" rel="stylesheet"/>
<!-- Nueva Interfaza -->
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900" rel="stylesheet"/>
<link href="favicon.ico" rel="icon" type="image/x-icon"/>
<link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
<link href="assets/css/font-awesome.min.css" rel="stylesheet"/>
<link href="assets/css/flaticon.css" rel="stylesheet"/>
<link href="assets/css/slicknav.min.css" rel="stylesheet"/>
<link href="assets/css/jquery-ui.min.css" rel="stylesheet"/>
<link href="assets/css/owl.carousel.min.css" rel="stylesheet"/>
<link href="assets/css/animate.css" rel="stylesheet"/>
<link href="assets/css/style.css" rel="stylesheet"/>
<script src="assets/js/jquery-3.2.1.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.slicknav.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/jquery.nicescroll.min.js"></script>
<script src="assets/js/jquery.zoom.min.js"></script>
<script src="assets/js/jquery-ui.min.js"></script>
<script src="assets/js/main.js"></script>
<script src="https://apis.google.com/js/api.js"></script>
<script src="assets/googleuploader.js"></script>
<script async="" jv-id="JZH7BFoCYw" src="//code.jivosite.com/widget.js"></script>
<!-- Facebook Pixel Code -->
<script>
		!function (f, b, e, v, n, t, s) {
			if (f.fbq) return; n = f.fbq = function () {
				n.callMethod ?
				n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0';
			n.queue = []; t = b.createElement(e); t.async = !0;
			t.src = v; s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window, document, 'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '2823637241229257');
		fbq('track', 'PageView');
	</script>
<noscript>
<img height="1" src="https://www.facebook.com/tr?id=2823637241229257&amp;ev=PageView
	&amp;noscript=1" width="1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<link href="styles.1f3fc51fc983b260e2da.css" rel="stylesheet"/></head>
<body>
<app-root></app-root>
<script src="runtime.26209474bfa8dc87a77c.js" type="text/javascript"></script><script nomodule="" src="es2015-polyfills.e17997aea7f522b8d49d.js" type="text/javascript"></script><script src="polyfills.8bbb231b43165d65d357.js" type="text/javascript"></script><script src="scripts.d8c81eaf49f6bbb2ae5a.js" type="text/javascript"></script><script src="main.5026b569673af0eeea97.js" type="text/javascript"></script></body>
</html>