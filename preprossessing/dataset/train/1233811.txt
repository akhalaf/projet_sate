<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<title></title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://estudoshumeanos.com/wp-content/themes/minibuzz3/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://estudoshumeanos.com/wp-content/themes/minibuzz3/prettyPhoto.css" rel="stylesheet" type="text/css"/>
<link href="https://estudoshumeanos.com/wp-content/themes/minibuzz3/jqueryslidemenu.css" rel="stylesheet" type="text/css"/>
<link href="https://estudoshumeanos.com/wp-content/themes/minibuzz3/s3slider.css" rel="stylesheet" type="text/css"/>
<link href="https://estudoshumeanos.com/wp-content/themes/minibuzz3/fancy.css" rel="stylesheet" type="text/css"/>
<link href="https://estudoshumeanos.com/xmlrpc.php" rel="pingback"/>
<link href="http://estudoshumeanos.com/wp-content/uploads/2011/03/favicon.ico" rel="shortcut icon"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://estudoshumeanos.com/feed/" rel="alternate" title="Laboratório de Estudos Hum(e)anos » Feed" type="application/rss+xml"/>
<link href="https://estudoshumeanos.com/comments/feed/" rel="alternate" title="Laboratório de Estudos Hum(e)anos » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/estudoshumeanos.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://estudoshumeanos.com/wp-includes/css/dist/block-library/style.min.css?ver=5.3.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://estudoshumeanos.com/wp-content/themes/minibuzz3/js/jquery-1.4.2.min.js?ver=1.4.2" type="text/javascript"></script>
<script src="https://estudoshumeanos.com/wp-content/themes/minibuzz3/js/jquery.prettyPhoto.js?ver=2.5.6" type="text/javascript"></script>
<script src="https://estudoshumeanos.com/wp-content/themes/minibuzz3/js/cufon-yui.js?ver=1.0.9" type="text/javascript"></script>
<script src="https://estudoshumeanos.com/wp-content/themes/minibuzz3/js/Tuffy_500-Tuffy_700.font.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://estudoshumeanos.com/wp-content/themes/minibuzz3/js/fade.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://estudoshumeanos.com/wp-content/themes/minibuzz3/js/jqFancyTransitions.1.8.min.js?ver=1.8" type="text/javascript"></script>
<script src="https://estudoshumeanos.com/wp-content/themes/minibuzz3/js/s3Slider.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://estudoshumeanos.com/wp-content/themes/minibuzz3/js/jquery.cycle.all.min.js?ver=5.3.6" type="text/javascript"></script>
<link href="https://estudoshumeanos.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://estudoshumeanos.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<meta content="WordPress 5.3.6" name="generator"/>
<link href="https://estudoshumeanos.com/" rel="canonical"/>
<link href="https://estudoshumeanos.com/" rel="shortlink"/>
<link href="https://estudoshumeanos.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Festudoshumeanos.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://estudoshumeanos.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Festudoshumeanos.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<style id="wp-custom-css" type="text/css">
			.texto-justificado {
	text-align: justify
}		</style>
<!-- ////////////////////////////////// -->
<!-- //      Javascript Files        // -->
<!-- ////////////////////////////////// -->
<!-- dropdown menu -->
<!-- dropdown menu -->
<script type="text/javascript">
//Specify full URL to down and right arrow images (23 is padding-right to add to top level LIs with drop downs):
var arrowimages={down:['downarrowclass', 'https://estudoshumeanos.com/wp-content/themes/minibuzz3/images/down.gif', 38], right:['rightarrowclass', 'https://estudoshumeanos.com/wp-content/themes/minibuzz3/images/right.gif']}
</script>
<script src="https://estudoshumeanos.com/wp-content/themes/minibuzz3/js/jqueryslidemenu.js" type="text/javascript"></script>
<script type="text/javascript">
/* for s3Slider homepage slider */
var $ = jQuery.noConflict();
    $(document).ready(function() {
        $('#s3slider').s3Slider({
            timeOut:6000        });
    });
</script>
<script type="text/javascript">
var $ = jQuery.noConflict();
    jQuery(document).ready(function($) {

		/* for Testimonial cycle */
		$('.boxslideshow').cycle({
		timeout: 6000,  // milliseconds between slide transitions (0 to disable auto advance)
		fx:      'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...            
		pause:   0,	  // true to enable "pause on hover"
		pauseOnPagerHover: 0 // true to pause when hovering over pager link
		});
		

		/* for portfolio prettyPhoto */
		$("#main-gallery a[rel^='prettyPhoto']").prettyPhoto({theme:'light_rounded'});

		
    });
</script>
<script type="text/javascript">
	 Cufon.replace('h1') ('h1 a') ('h2') ('h3') ('h4') ('h5') ('h6') ('.desc') ('.largetext')
</script>
<style type="text/css">
body{background-color:#919191; }
a, a:visited{color:#ed0909;}
h1, h2, h3, h4, h5, h6, h1 a, h1 a:visited, h2 a, h2 a:visited, h3 a, h3 a:visited, h4 a, h4 a:visited, h5 a, h5 a:visited, h6 a, h6 a:visited, span.tblue,
span.tblue a, span.tblue a:visited, span.tblue2, .name-testi .user
{color:#000000 !important}
#searchresult .post h3 a, .current-menu-parent .current-menu-item a, .current_page_parent .current_page_item a{color:#000000 !important;}
#top{border-top:solid 2px #000000;}
code{border-left:solid 5px #000000;}
#topnav ul li.current_page_item a, #topnav ul li.current-menu-item a,  #topnav-full ul li.current_page_item a, #topnav-full ul li.current-menu-item a{border-top:solid 3px #000000;}
.current_page_parent a, .current-menu-item a, .current-menu-parent a{border-top:solid 3px #000000; padding-top:4px !important;}
#topnav ul li.current_page_item ul li a, #topnav ul li.current-menu-item ul li a,
#topnav ul li ul li.current_page_item a, #topnav ul li ul li.current-menu-item a,
#topnav-full ul li.current_page_item ul li a, #topnav-full ul li.current-menu-item ul li a, 
#topnav-full ul li ul li.current_page_item a, #topnav-full ul li ul li.current-menu-item a {border-top:0px !important;}
#sideright ul li.recentcomments a, #sideright ul li.recentcomments a:visited,
.boxslideshow a, .boxslideshow a:visited, .wp-pagenavi a:hover, .wp-pagenavi span.current 
{color:#ed0909 !important;}

#header-inner h1.pagetitle{color:#000000 !important;}
.pagination a{color:#656253; }
.pagination .current{color:#ed0909;}

.hentry {
	margin: 0 0 0px 0;
	padding:0px 0px 0px 0px;
}

.s3sliderImage div {background-color:#000;}
</style>
<!--[if IE 6]>
<script src="https://estudoshumeanos.com/wp-content/themes/minibuzz3/js/DD_belatedPNG.js"></script>
<script>
  DD_belatedPNG.fix('img');
</script>
<![endif]-->
</head>
<body>
<div id="wrapper">
<div id="container">
<div id="top">
<div id="logo">
<h1><a href="http://estudoshumeanos.com/" title="Click for Home">L(E)H</a></h1>
<span class="desc">Laboratório de Estudos Hum(e)anos, UFF</span>
</div><!-- end #logo -->
</div><!-- end #top -->
<div id="topnavigation">
<div id="topnav">
<div class="jqueryslidemenu" id="myslidemenu">
<ul class="menu"><li class="current_page_item"><a href="https://estudoshumeanos.com/">Home</a></li><li class="page_item page-item-472"><a href="https://estudoshumeanos.com/breviario-de-filosofia-publica/">Breviário de Filosofia Pública</a></li>
<li class="page_item page-item-3147 page_item_has_children"><a href="https://estudoshumeanos.com/informacoes-editoriais/">Informações Editoriais</a>
<ul class="children">
<li class="page_item page-item-4281 page_item_has_children"><a href="https://estudoshumeanos.com/informacoes-editoriais/laboratorio-criticas-e-alternativas-a-prisao/">Laboratório Críticas e Alternativas à Prisão</a>
<ul class="children">
<li class="page_item page-item-4294"><a href="https://estudoshumeanos.com/informacoes-editoriais/laboratorio-criticas-e-alternativas-a-prisao/quem-somos-2/">Grupo de Pesquisa</a></li>
</ul>
</li>
<li class="page_item page-item-3163"><a href="https://estudoshumeanos.com/informacoes-editoriais/outras-publicacoes/">Outras Publicações</a></li>
<li class="page_item page-item-2428"><a href="https://estudoshumeanos.com/informacoes-editoriais/quem-somos/">Quem Somos</a></li>
</ul>
</li>
<li class="page_item page-item-638 page_item_has_children"><a href="https://estudoshumeanos.com/numeros-antigos/">Números Antigos</a>
<ul class="children">
<li class="page_item page-item-1602"><a href="https://estudoshumeanos.com/numeros-antigos/ano-1-2011/">Ano 1 – 2011</a></li>
<li class="page_item page-item-3137"><a href="https://estudoshumeanos.com/numeros-antigos/ano-2-2012/">Ano 2 – 2012</a></li>
<li class="page_item page-item-3223"><a href="https://estudoshumeanos.com/numeros-antigos/ano-3-2013/">Ano 3 – 2013</a></li>
<li class="page_item page-item-3686"><a href="https://estudoshumeanos.com/numeros-antigos/ano-4-2014/">Ano 4 – 2014</a></li>
<li class="page_item page-item-3688"><a href="https://estudoshumeanos.com/numeros-antigos/ano-5-2015/">Ano 5 – 2015</a></li>
</ul>
</li>
</ul>
</div>
</div><!-- end #topnav -->
<div id="topsearch">
<form action="https://estudoshumeanos.com/" id="searchform" method="get">
<p><input class="inputbox" id="s" name="s" type="text"/><input class="but" type="submit" value="Search"/></p>
</form>
</div><!-- end #topsearch -->
</div><!-- end #topnavigation -->
<div id="header">
<div id="slideshow">
<div id="s3slider">
<ul id="s3sliderContent">
<li class="s3sliderImage">
<img alt="" class="attachment-slider-post-thumbnail size-slider-post-thumbnail wp-post-image" height="340" src="https://estudoshumeanos.com/wp-content/uploads/2018/08/Sanches-940x340.png" width="940"/>
<div>
<h1 class="title-slider">IV Colóquio sobre Ceticismo</h1>
<p>eis a programação <a href="http://estudoshumeanos.com/2018/08/22/iv-coloquio-sobre-ceticismo/">completa</a>....																</p>
</div>
</li>
<li class="s3sliderImage">
<img alt="" class="attachment-slider-post-thumbnail size-slider-post-thumbnail wp-post-image" height="340" src="https://estudoshumeanos.com/wp-content/uploads/2018/01/breviario-prisao-940x340.jpg" width="940"/>
<div>
<h1 class="title-slider">O Cárcere Precisa ser a Exceção e não a Regra</h1>
<p>inclusive para os nossos desafetos, por Tamires Alves no <a href="http://estudoshumeanos.com/2017/12/24/o-carcere-precisa-ser-a-excecao-e-nao-a-regra-inclusive-para-os-nossos-desafetos/">Breviário</a>...																</p>
</div>
</li>
<li class="s3sliderImage">
<img alt="" class="attachment-slider-post-thumbnail size-slider-post-thumbnail wp-post-image" height="340" sizes="(max-width: 895px) 100vw, 895px" src="https://estudoshumeanos.com/wp-content/uploads/2018/01/manoel-895x340.png" srcset="https://estudoshumeanos.com/wp-content/uploads/2018/01/manoel-895x340.png 895w, https://estudoshumeanos.com/wp-content/uploads/2018/01/manoel-300x115.png 300w, https://estudoshumeanos.com/wp-content/uploads/2018/01/manoel-768x293.png 768w" width="895"/>
<div>
<h1 class="title-slider">Manoel de Barros ou nas Mínimas Expressões está Toda a Poesia</h1>
<p>leia o <a href="http://estudoshumeanos.com/2017/01/05/manoel-de-barros-ou-nas-minimas-expressoes/">Breviário</a> de Pedro Fernandes...																</p>
</div>
</li>
<li class="s3sliderImage">
<img alt="" class="attachment-slider-post-thumbnail size-slider-post-thumbnail wp-post-image" height="340" src="https://estudoshumeanos.com/wp-content/uploads/2015/06/sextus-940x340.jpg" width="940"/>
<div>
<h1 class="title-slider">Sexto Empírico e Saussure</h1>
<p>entre o Cético e o Linguista por Ana El-Jaick no <a href="http://estudoshumeanos.com/2015/05/02/sexto-empirico-e-saussure/">Breviário</a>...																</p>
</div>
</li>
<li class="s3sliderImage">
<img alt="" class="attachment-slider-post-thumbnail size-slider-post-thumbnail wp-post-image" height="340" src="https://estudoshumeanos.com/wp-content/uploads/2015/01/denis-940x340.jpg" width="940"/>
<div>
<h1 class="title-slider">Diderot e o Ceticismo</h1>
<p>leia o <a href="http://estudoshumeanos.com/2015/01/31/diderot-e-o-ceticismo/">Breviário</a> de Marcela Fernandes...																</p>
</div>
</li>
<li class="s3sliderImage">
<img alt="" class="attachment-slider-post-thumbnail size-slider-post-thumbnail wp-post-image" height="340" sizes="(max-width: 940px) 100vw, 940px" src="https://estudoshumeanos.com/wp-content/uploads/2014/07/m.-940x340.jpg" srcset="https://estudoshumeanos.com/wp-content/uploads/2014/07/m.-940x340.jpg 940w, https://estudoshumeanos.com/wp-content/uploads/2014/07/m.-300x109.jpg 300w" width="940"/>
<div>
<h1 class="title-slider">Montaigne e o Ensaio</h1>
<p>leia este <a href="http://estudoshumeanos.com/2014/05/15/montaigne-e-o-ensaio/">Breviário</a> de autoria do Filippi Fernandes...																</p>
</div>
</li>
<li class="s3sliderImage">
<img alt="" class="attachment-slider-post-thumbnail size-slider-post-thumbnail wp-post-image" height="340" sizes="(max-width: 940px) 100vw, 940px" src="https://estudoshumeanos.com/wp-content/uploads/2011/09/david-hume.jpg" srcset="https://estudoshumeanos.com/wp-content/uploads/2011/09/david-hume.jpg 940w, https://estudoshumeanos.com/wp-content/uploads/2011/09/david-hume-300x108.jpg 300w" width="940"/>
<div>
<h1 class="title-slider">A Outra Modernidade de Hume</h1>
<p>Leia o ensaio de Cesar Kiraly, no <a href="http://estudoshumeanos.com/2012/06/18/a-outra-modernidade-de-hume/">Breviário</a>....																</p>
</div>
</li>
<li class="clear s3sliderImage"></li>
</ul>
</div>
</div><!-- end slideshow -->
</div><!-- end #header -->
<div id="content">
<div id="content-left">
<div id="maintext">
<h1>Apresentação</h1>
<div class="post-42 page type-page status-publish hentry" id="post-42">
<div class="entry-content">
<p style="text-align: justify;">Iniciado em 2002, o Laboratório de Estudos Hum(e)anos é um espaço de reflexão, trabalho e inquirição filosófica, com vinculações multidisciplinares com os campos da teoria política,  da ética, da moralidade e da estética.</p>
<p style="text-align: justify;">O laboratório pretende estimular investigações no campo da filosofia política (clássica, moderna e contemporânea) que levem em conta as pretensões cognitivas e os desenhos de mundo presentes nos diferentes esforços de invenção e de representação da vida social. A premissa que informa esta orientação deriva de uma perspectiva fundada na tradição do ceticismo filosófico. Quer isto dizer que aquele campo é percebido como marcado por uma diversidade irredutível à operação de critérios de verdade capazes de estabelecer os termos efetivos da realidade. Uma realidade diante da qual as diferentes versões de mundo, presentes na tradição da filosofia política, devem ser cotejadas ou testadas.</p>
<p style="text-align: justify;">A perspectiva adotada pelo laboratório demarca-se dos diversos tratamentos contextualistas – presentes em diferentes versões da história das idéias ou dos conceitos – e busca fixar uma tradição de trabalho filosófico sobre o campo da reflexão política, ética e moral. A orientação cética pode, ainda, ser detectada no reconhecimento do papel desempenhado pela crença no processo de invenção de mundos que constitui a matéria nobre da filosofia política. Como modalidade particular da criatividade humana, a filosofia política – assim como a ética e a filosofia moral – possui, ainda, forte interação – formal e substantiva – com questões presentes nos campos da arte e da estética. Vários dos problemas filosóficos relevantes para esses campos encontram equivalências – quando não identidades – em questões que incidem sobre a fabricação da filosofia política: forma, representação, referencialidade, mimetismo. O Laboratório interessa-se, com particular ênfase, por essa conexão, assim como na que se estabelece entre arte e moralidade.</p>
<p style="text-align: justify;">Em vários sentidos, o Laboratório de Estudos Hum(e)anos evoca a figura de David Hume como referência:</p>
<p style="text-align: justify;">1. Em sentido estrito, como espaço de reflexão e investigação sobre a tradição filosófica do ceticismo, e do papel central que ocupa em sua configuração moderna a filosofia de David Hume;</p>
<p style="text-align: justify;">2. Como espaço de trabalho no campo da filosofia política e moral, percebido como domínio constituído por atos de crença;</p>
<p style="text-align: justify;">3. Como lugar de uma reflexão experimental, voltada para questões de natureza pública, no sentido empregado por Hume na expressão ciência experimental da natureza humana: uma forma de conhecimento atenta aos efeitos das crenças sobre a configuração da experiência do mundo.</p>
<p style="text-align: justify;">A inscrição filosófica presente no desenho do Laboratório, por suas conexões céticas, comporta, ainda, a consideração de temas que constituem experiência da vida comum. Para além de uma agenda cognitivista, o Laboratório ocupa-se, portanto, de questões de natureza prática, presentes em uma possível agenda de filosofia pública, voltada para a reflexão sobre fenômenos, dilemas e eventos traumáticos, e suas respectivas implicações normativas. Em outros termos, trata-se de fazer da filosofia política uma potência dotada da capacidade de intervenção cognitiva, crítica e normativa, diante de fenômenos da vida pública. O Laboratório apresenta-se como alternativa teórica e investigativa aos limites normativos presentes no projeto, ainda hegemônico, de uma ciência social positiva.</p>
<p style="text-align: justify;"><strong><a href="http://estudoshumeanos.com/author/renatolessa/">Renato Lessa</a></strong></p>
<hr size="1"/>
</div><!-- .entry-content -->
</div><!-- #post-## -->
<div id="comments">
<p class="nocomments">Comments are closed.</p>
</div><!-- #comments -->
</div><!-- end #maintext -->
</div><!-- end #content-left -->
<div id="content-right">
<div id="sideright">
<div class="widget-area" role="complementary">
<ul><li class="widget-container widget_text" id="text-49"><div class="box"> <div class="textwidget"><span style="color: #ff0000;"><a href="http://estudoshumeanos.com/breviario-de-filosofia-publica/quem-somos/"><span style="color: #ff0000;">Quem Somos</span></a></span>
</div>
</div></li><li class="widget-container widget_text" id="text-60"><div class="box"> <div class="textwidget"><span style="color: #ff0000;"><a href="http://estudoshumeanos.com/informacoes-editoriais/outras-publicacoes"><span style="color: #ff0000;">Outras Publicações</span></a></span></div>
</div></li><li class="widget-container widget_text" id="text-61"><div class="box"><h2 class="widget-title">Receba por Email</h2> <div class="textwidget"><p><!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/slim-081711.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
</p><div id="mc_embed_signup">
<form action="//blogspot.us8.list-manage.com/subscribe/post?u=7e18c7c2a2727ea676bc2a924&amp;id=d64bf42a04" class="validate" id="mc-embedded-subscribe-form" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
<p> <input class="email" id="mce-EMAIL" name="EMAIL" placeholder="email address" required="" type="email" value=""/><br/>
<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups--></p>
<div style="position: absolute; left: -5000px;"><input name="b_7e18c7c2a2727ea676bc2a924_d64bf42a04" tabindex="-1" type="text" value=""/></div>
<div class="clear"><input class="button" id="mc-embedded-subscribe" name="subscribe" type="submit" value="Subscribe"/></div>
</form>
</div>
<p><!--End mc_embed_signup--></p>
</div>
</div></li></ul>
</div>
</div><!-- end #sideright -->
</div><!-- end #content-right -->
<div class="clr"></div><!-- end clear float -->
</div><!-- end #content -->
<div id="footer">
<div id="footer-text">
									Copyright ©
			2011-2020 Laboratório de Estudos Hum(e)anos. All rights reserved.		
						</div><!-- end #footer-text -->
</div><!-- end #footer -->
</div><!-- end #container -->
</div><!-- end #wrapper -->
<script src="https://estudoshumeanos.com/wp-includes/js/wp-embed.min.js?ver=5.3.6" type="text/javascript"></script>
</body>
</html>
