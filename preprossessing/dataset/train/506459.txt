<!DOCTYPE html>
<html lang="pt-BR" xml:lang="pt-BR" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<meta content="IE=Edge" http-equiv="X-UA-Compatible"/>
<title>Ops! 404... - Esecon | Empreendimentos imobiliÃ¡rios</title>
<!-- MOBILE META -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
<!-- SEO BASIC META -->
<meta content="key1, key2, key3, " name="keywords"/>
<meta content=" - Base site cliente, Base site cliente metadescript" name="description"/>
<meta content="FlyUp Web Marketing" name="author"/>
<meta content="PT-BR" name="language"/>
<meta content="no-cache" name="pragma"/>
<meta content="Public" name="cache-control"/>
<meta content="0" name="expires"/>
<!-- HEAD SEO -->
<meta content="all" name="robots"/>
<meta content="index,follow" name="robots"/>
<meta content="day" name="revisit-after"/>
<meta content="Global" name="distribution"/>
<meta content="" name="url"/>
<meta content="" name="classification"/>
<meta content="general" name="rating"/>
<meta content="Campinas - SP" name="city"/>
<meta content="Brasil" name="country"/>
<meta content="SÃ£o Paulo/SP-BR" name="geo.region"/>
<meta content="" name="geo.placename"/>
<meta content="Campinas - SP-BR" name="geo.region"/>
<meta content="FlyUp Web Marketing" name="creator"/>
<meta content="Copyright " name="copyright"/>
<!--<link rel="canonical" href="" />-->
<meta content="" name="google-site-verification"/>
<link href="fonts.googleapis.com" rel="dns-prefetch"/>
<link href="facebook.com" rel="dns-prefetch"/>
<link href="twitter.com" rel="dns-prefetch"/>
<link href="youtube.com" rel="dns-prefetch"/>
<link href="embed.tawk.to" rel="dns-prefetch"/>
<!-- HEAD SOCIAL MEDIA -->
<meta content="1412513582172353" property="fb:app_id"/>
<meta content="" property="og:title"/>
<meta content="" property="og:url"/>
<meta content="article" property="og:type"/>
<meta content="" property="og:image"/>
<meta content="" property="og:description"/>
<meta content="" property="og:site_name"/>
<meta content="2017-06-21T11:51:17+00:00" property="article:published_time"/>
<meta content="" property="article:tag"/>
<meta content="pt-BR" name="dc.language"/>
<meta content="" name="dcterms.rightsHolder"/>
<meta content="" name="dc.publisher"/>
<meta content="" name="dc.title"/>
<meta content="" name="dc.description"/>
<!-- Favicon-->
<link href="https://core.flyupweb.com.br/baseCliente/images/20/logos/favicon.png" rel="shortcut icon"/>
<!-- Stylesheets & Fonts -->
<!-- BEGIN GLOBAL STYLES -->
<link href="https://core.flyupweb.com.br/baseTheme/05/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="https://core.flyupweb.com.br/baseTheme/05/assets/css/all.min.css" rel="stylesheet" type="text/css"/>
<link href="https://core.flyupweb.com.br/baseTheme/05/assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
<link href="https://core.flyupweb.com.br/baseTheme/05/assets/css/owl.carousel.min.css" rel="stylesheet" type="text/css"/>
<link href="https://core.flyupweb.com.br/baseTheme/05/assets/css/jquery.fancybox.min.css" rel="stylesheet" type="text/css"/>
<link href="https://core.flyupweb.com.br/baseTheme/05/assets/css/tooltipster.min.css" rel="stylesheet" type="text/css"/>
<link href="https://core.flyupweb.com.br/baseTheme/05/assets/css/cubeportfolio.min.css" rel="stylesheet" type="text/css"/>
<link href="https://core.flyupweb.com.br/baseTheme/05/assets/css/revolution/navigation.css" rel="stylesheet" type="text/css"/>
<link href="https://core.flyupweb.com.br/baseTheme/05/assets/css/revolution/settings.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="https://core.flyupweb.com.br/baseTheme/05/assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="https://core.flyupweb.com.br/baseTheme/05/assets/css/tm-colororange.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<!-- THEME STYLES -->
<link href="https://www.esecon.com.br/func/css_custon.php?8087289ee3077d119cc88a3625db9888FlyUp" rel="stylesheet"/>
<link href="https://www.esecon.com.br/templates/basic.css?8087289ee3077d119cc88a3625db9888FlyUp" rel="stylesheet"/>
<!-- Analytics -->
</head></html>