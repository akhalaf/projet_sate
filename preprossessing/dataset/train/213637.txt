<!DOCTYPE html>
<html lang="en">
<head data-cast-api-enabled="true">
<meta charset="utf-8"/>
<title></title>
<meta content="Bethel" property="og:title"/>
<meta content="Bethel" property="og:site_name"/>
<meta content="https://betheltv.tv" property="og:url"/>
<meta content="https://streann.akamaized.net/upload2/1582907702290.png" property="og:image"/>
<meta content="image/png" property="og:image:type"/>
<meta content="Bethel Plus es la aplicaciÃ³n oficial de Bethel TelevisiÃ³n donde podrÃ¡s disfrutar de nuevo contenido exclusivo para ti y toda la familia, descubre las funciones interactivas que tenemos aÃ±adidas para darte una experiencia de entretenimiento en la comodidad de tu dispositivo mÃ³vil, tableta o TV." property="og:description"/>
<meta content="1004393323258653" property="fb:app_id"/>
<meta content="website" property="og:type"/>
<base href="/"/>
<!-- <meta property="og:title" content="Bienvenido al ruedo del Entretenimiento">
    <meta property="og:site_name" content="Bienvenido al ruedo del Entretenimiento"> -->
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="assets/favicon/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="assets/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="assets/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="assets/favicon/site.webmanifest" rel="manifest"/>
<link color="#5bbad5" href="assets/favicon/safari-pinned-tab.svg" rel="mask-icon"/>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet"/>
<meta content="#da532c" name="msapplication-TileColor"/>
<meta content="#ffffff" name="theme-color"/>
<link href="https://api6.streann.com" rel="preconnect"/>
<script async="" src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<script async="" src="âhttps://www.googletagmanager.com/gtag/js?id=UA-175900443-1â"></script>
<script src="https://appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/en_US/appleid.auth.js" type="text/javascript"></script>
<script async="" defer="" src="https://connect.facebook.net/en_US/sdk.js"></script>
<script async="" src="https://platform.twitter.com/widgets.js" type="text/javascript"></script>
<script>
        window.googletag = window.googletag || {
            cmd: []
        };
        googletag.cmd.push(function() {
            // googletag.defineSlot('/184419101/Display', [728, 90], 'div-gpt-ad-1595338915445-0').addService(googletag.pubads());
            // googletag.defineSlot('/184419101/Display/rctv', [728, 90], 'div-gpt-ad-1595338915445-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>
<script src="https://js.stripe.com/v3/"></script>
<link href="styles.52a37d5207f9a6b240b1.css" rel="stylesheet"/></head>
<body>
<div id="fb-root"></div>
<script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<script src="https://www.gstatic.com/firebasejs/4.12.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.12.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.12.0/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.12.0/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.12.0/firebase-messaging.js"></script>
<streann-root></streann-root>
<script src="runtime-es2015.5a829bb13de86dc7fcdd.js" type="module"></script><script src="polyfills-es2015.b650a8384318e9e90493.js" type="module"></script><script defer="" nomodule="" src="runtime-es5.fa88a55aec30771a484c.js"></script><script defer="" nomodule="" src="polyfills-es5.2e36a3f2c79509958d18.js"></script><script defer="" src="scripts.3565e7968009dd27235a.js"></script><script src="main-es2015.5c1310fff580aa996d24.js" type="module"></script><script defer="" nomodule="" src="main-es5.cb6702318a35cb2bb436.js"></script></body>
</html>