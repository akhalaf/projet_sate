<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "72251",
      cRay: "611177c80bddd9c0",
      cHash: "d180e21411c057f",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYm14bm9uc3RvcC5jb20vYm14L2FiYWhpc3QuaHRt",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "f31MH4+kltDKPKVLa6f/YC9/Ed15w4GcanpSBeYCvL8pMzKz7Lomv0b1lwzOVSO+MPja+mhLJOpOAnS/QFh6VZSiO/xltA4o3rA6C5VIJWgld6dPLbIuAGmCaEL4dvr+vwWM8WnGtSu7YeA9cBJnjkh33c6s1J7zExhD6xPI3axyF3Tw7TC2uJRGFAGSGE+cDbkBp0XlUtq+cfx2NoAgoZfmdNT+qhBy03xETTqQEdPBXEyBkTWw2fjpHSHMlP4ncu/ZLw49Nse/OR5ItfSBy2gAW7lbbYsjnF1lBHle/pOPUgNWHZCu0PSmFxJZcZ790OfiO1ER0wxjxWJS7RLQO/jVniJ9FpEeSuDD16lZJ7lUpB4oa17Mej57IhCQyHSH4qMzfh1svMXKMDUWVtC5+vyB53qlR7nJfTbiUX/bhYnOvAVh19pK+iSE6meEJZekjCoh/65RZfmZWk8EQbLuWktu6CZvdJI/JnsIciqxiIItXRaaix/IBdPl62q6PYSgsN6/qlegdUFFamZEmdUUoNeERRu1a7/9ykRkWCyoJfxvVIJge1we8e59e7u9cgFlvVERIi7FRAoz8jzdQgeM1O0r350LJdwvkCbP1SoCl9GAHvQrIqnPWtl1hK7SZKTwDb7pVUZxo8CQgyBy67OX7nGvrkillWaPIpTucVx++COUNpnQGcL4bJwjtH4TsR0TsoLVGGjUZEhLwWOZYsf+3iHO5JSw2729ZRbEkgYRJwNd08SZaGNjzrgTMor83jkXVCzbFw/id+7AYX3Ezn5x8Q==",
        t: "MTYxMDU2NTU1Ni40OTcwMDA=",
        m: "0cyqUBXsMNGYtPgaoVOqwR/QYupt2mng0CQJhoOHO3c=",
        i1: "O4nrRkgqQMoX0bIxkZn0YA==",
        i2: "EGrv//wuw51ylZDsJR8wVA==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "Jzsfeuv07smaajSBigMdIE0kqDilmoZEboURXiG8JVA=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.bmxnonstop.com</h2>
</div><!-- /.header -->
<div style="display: none;"><a href="https://uswest-qwest.com/massive.php?rss=0">table</a></div>
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/bmx/abahist.htm?__cf_chl_captcha_tk__=3adacf25024ba8918ebb09263636aa11f59aab68-1610565556-0-AZCnd6JxpbNwp25kBMozelyPTCyT4Bo99SA12N4L_LFH-jsWpU9LtHOI_yRZTESoqKOBav9oLY1PNJHgW-67aHP9vNIFZrSWf6gh9YrldPjJ3SzoIojLw8yFpZXBFuxziNkZG04qIjiDeO8wqOOm-B3tX7SpJqeRv75Nc18W19m_WNVptKqGtQFhVAdNTMMdjwdBh5wqAQwt7CRRfjkxOzdzUzNJwluuC8myfGsYz0IHbtiv0xicmAn9WbHYcLB8ieVHAv3AxpPC2EiJ4e9Ipi_uIY-AzaIJ0kKQt916nzjmrIGlcL_hmf6EtzIgjZ7PxAkzpdlFHMDU5WYcHU_zrv8ybg7bQSzP1AzEoyinWRczxbfrVM15NMR5VtzLLZsvTujC_339OYPnuk5Y8Liw788cHilnQA5R59_OPlWqAJaE2jn72v-oIqtIX6cKGtWQDy30z7oIXdXzJMe9ML1OR2Jl5RDs7qkotJw857YEnVdWblqKHYy-_A-5MOg87AuUp9xT9_odxCL3Oeu2agUZSaicV5rSwlwu51FoKZZ1uSnN3bLXJqJGCFRRlMs-8ctwVykUplDNQnDKZ2DWwKdYgizwaG8VeDNx4rPs5rkprxbn" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="ca8639d363503e1937f00ae791b3bda99b8f828a-1610565556-0-AcT3/HpQfiWDDVTiFqf2ua85Is44rxc0buVUNCiM1Gr2Td5X7FNmEncjlreC1P4Icr21MJv7LzMr6SNdP4LGhN2wYFVtWY83kq7W/ZIxja2v1O2cOI07P7AC5kPnlxEXJRKxxiFVaMQmAprJnsanLXvAwlFxt9gTcsxGQU9YzEOILuzmSg/R4p4rVJE7pfZy4TxYIsR3uAQ6UjVjgc0VSelIEl//ZpVi/USBxQww7naxw+hMBKsMARdyfsyHxpdhRWNYF8hWBXD9v1lAX68e0pD0Yqrg9nhADpDbqPWdRUqbyiHfwYqPux40Uv9AVgG9EYjsjh1G5SsZ/w+NjsgwuyVxmLaplQXDfNWbZt28vLtNn5ZgcVEaeIubHHVMBv3iu3SMAiB8POSBvpBhWKVHZx56pAMxSc2MOuOTbdn4LT4Ew3mYNGFTDdYcWq1ApDPaZMcWcXF58yi/yIiFFDrkSh1MfbUcUt+eRhjmWxQ2UatMvfCbUZBMjbcpt/ZsPPf9JxFXk2JTUkpq624wZ5i/xUVYbD+0LfjjLuqUH/c4dUR68vTzCGfX+wWD0qD2aBJQZP/gie3I3mwiXwDKEcAdLkLr7sgNO3ng1hBUprxVyFwFnW90ggbEpV8D6PrVJIUpbTGaKy+zmF8zv/5D00bWY7QPRAgRLoqCTkPfCHA/E4FItsjddWTtLV0oPzTThwtWQhAzLmjlooomkGQe0x+RlOCaQ7lG/v5ZT7z5abglv514Md6rAO47qUnLeHrtDOXTnwpfT9bkcKI3EqojAYLCrFAYJZEpw8B3uLPGzSarSPXIFf3CgmhYRTR0P5SGSJnB6mBAUeDEgtdu+iR2Ppcfry3IQjVuTep/lUzsofAEkTXgcMWMEelRQsAIcCvT3cax2dyicF+KNtb/T4FNOOYSUALFM1UPfVfIoBQR8Kq7M+g7vywxufin6DYS/58h4fHPGZFQmRamOPysC4o6uBGsytckeZb0V2+hCpqtifEYiefJ76WTD3OIwWSxAQvcn4EZx+pwOoLCHbAsLj+dPZPVw8vnM8Akzjj1YBZ9q/rLCwq20IPBSUhf8iOoNr5xWJzJ8nRrKnwdzniC2buHEsW8f2GQoJCCbOm2YVRf5JawSmTmbk41OLUG7NY6/qiTrZZsXyZIUvWbD01L1dfoPQhSNjOOa/yYRxHoBStKBpRHDAD1wiwfuSoI5b3C6BKHhyXNdwz1u6foy7DV3TtCauLVO8OdKThjoRooucK89E0PJn5nogkWU2+K2EWKHH4kcD0fet/PDP+Uel3xt/nbjRr0wqsLFNmEEdJ9nFFkr6L5Kt5Tx2+otc8hOs5WN8B1a+ybqg=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="613181abca0fe4dba153e45de5163591"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=611177c80bddd9c0')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">611177c80bddd9c0</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:8d2b:d30c:bf6f:658f</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
