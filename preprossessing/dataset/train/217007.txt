<!DOCTYPE html>
<html dir="ltr" lang="de">
<head>
<title>BIC.at - BerufsInformationsComputer</title>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
<meta content="initial-scale=1" name="viewport"/>
<meta content="Berufe, Lehrberufe, Berufsliste, Berufsbeschreibung, Synonyme, Filme, Video, Bilder" name="keywords"/>
<meta content="Auf BIC.at sind Informationen zu über 1500 Berufen gespeichert. Zur ersten Orientierung empfehlen wir Ihnen den Menüpunkt Interessenprofil: Finden Sie heraus, welche Berufsgruppen und Berufe Sie besonders interessieren." name="description"/>
<link href="images/favicon/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="images/favicon/apple-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="images/favicon/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="images/favicon/apple-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="images/favicon/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="images/favicon/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="images/favicon/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="images/favicon/apple-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="images/favicon/apple-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="images/favicon/android-icon-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="images/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="images/favicon/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="images/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="images/favicon/manifest.json" rel="manifest"/>
<meta content="#ffffff" name="msapplication-TileColor"/>
<meta content="images/favicon/ms-icon-144x144.png" name="msapplication-TileImage"/>
<meta content="#ffffff" name="theme-color"/>
<link href="/images/favicon/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="template/css/bootstrap.min.css" rel="stylesheet"/>
<link href="template/css/font-awesome.min.css" rel="stylesheet"/>
<link href="template/css/cookiecuttr.css" rel="stylesheet"/>
<script src="template/js/jquery.min.js" type="text/javascript"></script>
<script src="template/js/jquery.mobile.custom.min.js" type="text/javascript"></script>
<script src="template/js/bootstrap.min.js"></script>
<script src="template/js/jquery.cookie.js"></script>
<script src="template/js/owl.carousel.js"></script>
<script src="template/js/nivo-lightbox.js"></script>
<script src="template/js/custom.js" type="text/javascript"></script>
<link href="template/css/demo.css" media="all" rel="stylesheet" type="text/css"/>
<link href="template/css/color-theme.css" media="all" rel="stylesheet" type="text/css"/>
<link href="template/css/webslidemenu.css" media="all" rel="stylesheet" type="text/css"/>
<script language="javascript" src="template/js/webslidemenu.js" type="text/javascript"></script>
<link href="template/css/docs.theme.min.css" rel="stylesheet"/>
<link href="template/css/owl.carousel.min.css" rel="stylesheet"/>
<link href="template/css/owl.theme.default.min.css" rel="stylesheet"/>
<link href="template/css/hover.css" rel="stylesheet"/>
<link href="template/css/nivo-lightbox.css" rel="stylesheet"/>
<link href="template/themes/default/default.css" rel="stylesheet" type="text/css"/>
<link href="template/css/languages.min.css" media="all" rel="stylesheet" type="text/css"/>
<link href="template/css/custom.css" rel="stylesheet"/>
</head>
<script>
function showHelp(id,language_id) {
	api = 'bic_help.php?id='+id+'&language_id='+language_id;
	$.getJSON(api, function(r){
		$('#HelpLabel').html ('Hilfe: ' + r.titel);
		$('#help-body').html (r.text);
	});
	$('#myHelp').modal('show');
}
</script>
<body style="background-color:#00cc66">
<div class="container" style="width: 100%;background-color:#2d71b5;">
<div class="helper wrapper row" style="padding-top:20px; padding-bottom:20px;">
<div class="col-lg-8 col-md-6 col-xs-5" style="padding-left:-15px !important; padding-right:-15px;">
<a class="active bic_logo" href="index.php" title="Home">
<img alt="BIC BerufsInformationsComputer" src="images/logos/bic.png"/>
</a>
</div>
<div class="col-lg-4 col-md-6 col-xs-7 no-print">
<div class="btn-group pull-right">
<button aria-expanded="false" aria-haspopup="true" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" type="button">
<span class="hidden-xs">My BIC.at  </span><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>
</button>
<ul class="dropdown-menu dropdown-menu-right">
<li><a href="mybic_login.php"><span class="hidden-xs"><span class="glyphicon glyphicon-pencil"></span></span> My BIC.at Login</a></li>
</ul>
</div>
<div class="input-group pull-right" style="margin-right:10px;">
<div class="btn-group dropdown">
<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button">
<span class="lang-sm lang-lbl hidden-xs" lang="de"></span><span class="lang-sm visible-xs" lang="de"></span> <span class="caret"></span>
</button>
<ul class="dropdown-menu" role="menu">
<li><a href="/?lg=ar"><span class="lang-sm lang-lbl-full" lang="ar"></span></a></li>
<li><a href="/?lg=de"><span class="lang-sm lang-lbl-full" lang="de"></span></a></li>
<li><a href="/?lg=en"><span class="lang-sm lang-lbl-full" lang="en"></span></a></li>
<li><a href="/?lg=hr"><span class="lang-sm lang-lbl-full" lang="hr"></span></a></li>
<li><a href="/?lg=sr"><span class="lang-sm lang-lbl-full" lang="sr"></span></a></li>
<li><a href="/?lg=tr"><span class="lang-sm lang-lbl-full" lang="tr"></span></a></li>
<li><a href="/?lg=fa"><span class="lang-sm lang-lbl-full" lang="fa"></span></a></li>
</ul>
</div>
</div>
<div class="btn-group pull-right" style="margin-right:10px;">
<a href="javascript:showHelp(1,1)">
<button aria-expanded="false" class="btn btn-success" type="button">
<span aria-hidden="true" class="fa fa-question"></span>
</button>
</a>
</div>
</div>
</div>
</div> <div class="wsmenucontainer clearfix no-print">
<div class="header">
<div class="wrapper clearfix bigmegamenu">
<div class="row">
<nav class="wsmenu clearfix col-lg-12">
<ul class="mobile-sub wsmenu-list">
<li>
<a class="active" href="index.php">
<i class="fa fa-home"></i>
<span class="hometext">  Home</span>
</a>
</li>
<li id="berufswahl">
<a href="#"><i class="fa fa-align-justify"></i>  Berufswahl <span class="arrow"></span></a>
<div class="megamenu clearfix">
<div class="col-lg-12 col-md-12 col-xs-12 hidden-xs">
<p>Schritt für Schritt zum passenden Beruf</p>
<hr style="margin-top:0px;"/>
</div>
<div class="col-lg-4 col-md-4 col-xs-12">
<a href="berufswahltipps" target="_blank"><button class="btn btn-primary btn-lg btn-block" type="button"><i class="fa fa-arrow-circle-right"></i> Tipps zur Berufswahl</button></a>
<p></p>
<p>Acht Schritte zum passenden Beruf und Bildungsweg!</p>
</div>
<div class="col-lg-4 col-md-4 col-xs-12">
<a href="bewerbungstipps" target="_blank"><button class="btn btn-primary btn-lg btn-block" type="button"><i class="fa fa-arrow-circle-right"></i> Bewerbungstipps</button></a>
<p></p>
<p>Bewerbungsprozess und Berufseinstieg!</p>
</div>
<div class="col-lg-4 col-md-4 col-xs-12">
<a href="materialsammlung.php"><button class="btn btn-primary btn-lg btn-block" type="button"><i class="fa fa-arrow-circle-right"></i> Materialsammlung</button></a>
<p></p>
<p>Informations- und Arbeitsblätter, Checklisten!</p>
</div>
</div>
</li>
<li><a href="#"><i class="fa fa-align-justify"></i>  Interessenprofil <span class="arrow"></span></a>
<div class="megamenu clearfix">
<div class="col-lg-12 col-md-12 col-xs-12 hidden-xs">
<p>Finde heraus, welche Berufsgruppen und Berufe zu deinen Interessen und Neigungen passen.</p>
<p>Das Interessenprofil ist KEIN Test!</p>
<hr style="margin-top:0px;"/>
</div>
<div class="col-lg-12 col-md-12 col-xs-12">
<a href="bic_interessenprofil_intro.php"><button class="btn btn-primary btn-lg btn-block" type="button"><i class="fa fa-arrow-circle-right"></i> Los geht´s ...</button></a>
</div>
</div>
</li>
<li>
<a href="#"><i class="fa fa-align-justify"></i>  Berufsinformation <span class="arrow"></span></a>
<div class="megamenu clearfix">
<div class="col-lg-12 col-md-12 col-xs-12 hidden-xs">
<p>Finden deinen Wunschberuf und die passende Ausbildung dazu. Unterschiedliche Zugäng helfen dir dabei.</p>
<hr style="margin-top:0px;"/>
</div>
<div class="col-lg-6 col-md-6 col-xs-12">
<a href="berufe_von_a_bis_z.php"><button class="btn btn-primary btn-lg btn-block" type="button"><img alt="Berufe von A - Z" src="images\icons\berufe_a_z.png"/> Berufe von A - Z</button></a>
<p style="text-align:center">alle Berufe alphabetisch</p>
</div>
<div class="col-lg-6 col-md-6 col-xs-12">
<a href="berufsgruppen.php"><button class="btn btn-primary btn-lg btn-block" type="button"><img alt="Berufsgruppen" src="images\icons\berufsgruppen.png"/> Berufsgruppen</button></a>
<p style="text-align:center">Berufe in Gruppen: nach Wirtschaftsbereichen</p>
</div>
<div class="col-lg-6 col-md-6 col-xs-12">
<a href="arbeitsfelder.php"><button class="btn btn-primary btn-lg btn-block" type="button"><img alt="Arbeitsfelder" src="images\icons\arbeitsfelder.png"/> Arbeitsfelder</button></a>
<p style="text-align:center">Berufe in Gruppen: nach ähnlichen Tätigkeiten, Arbeitsorten, Materialien</p>
</div>
<div class="col-lg-6 col-md-6 col-xs-12">
<a href="bildungswege.php"><button class="btn btn-primary btn-lg btn-block" type="button"><img alt="Bildungswege" src="images\icons\bildungswege.png"/> Bildungswege</button></a>
<p style="text-align:center">Lehrberufe, Beruf mit schulischer Ausbildung, akademische Berufe usw.</p>
</div>
<div class="col-lg-6 col-md-6 col-xs-12">
<a href="berufsfilme_von_a_bis_z.php"><button class="btn btn-primary btn-lg btn-block" type="button"><img alt="Berufsinfofilme" src="images\icons\berufsinfofilme.png"/> Berufsinfofilme</button></a>
<p style="text-align:center">Berufsvideos von A bis Z &amp; Filme zu Berufswahl, Ausbildung und Bewerbung</p>
</div>
<div class="col-lg-6 col-md-6 col-xs-12">
<a href="aus_und_weiterbildung.php"><button class="btn btn-primary btn-lg btn-block" type="button"><img alt="Aus- und Weiterbildung" src="images\icons\aus_weiterbildung.png"/> Aus- und Weiterbildung</button></a>
<p style="text-align:center">Adressen und Angebote der Bildungseinrichtungen</p>
</div>
</div>
</li>
<li>
<a href="#"><i class="fa fa-align-justify"></i>  Service <span class="arrow"></span></a>
<div class="megamenu clearfix">
<div class="col-lg-12 col-md-12 col-xs-12 hidden-xs">
<p>Zusatzinfos und Datenbanken:</p>
<hr style="margin-top:0px;"/>
</div>
<div class="row">
<div class="col-lg-4 col-md-6 col-xs-12">
<a href="bic_adressen.php"><button class="btn btn-primary btn-lg btn-block" type="button"><i class="fa fa-arrow-circle-right"></i> Adressen &amp; Links</button></a>
<p style="text-align:center">Berufsinfo, Schule, Studium, Lehre, Jobsuche u.v.m</p>
</div>
<div class="col-lg-4 col-md-6 col-xs-12">
<a href="https://www.ams.at/arbeitsuchende/arbeitslos-was-tun/lehrstellenboerse" target="_blank"><button class="btn btn-primary btn-lg btn-block" type="button"><i class="fa fa-arrow-circle-right"></i> Lehrstellenbörse</button></a>
<p style="text-align:center">der Wirtschaftskammer und des AMS</p>
</div>
<div class="col-lg-4 col-md-6 col-xs-12">
<a href="https://deinelehre.bic.at" target="_blank"><button class="btn btn-primary btn-lg btn-block" type="button"><i class="fa fa-arrow-circle-right"></i> deinelehre.bic.at</button></a>
<p style="text-align:center">Lehrberufsinfo, Lehrstellensuche, Beratung, FAQs usw.</p>
</div>
<div class="col-lg-4 col-md-6 col-xs-12">
<a href="bic_tdot.php"><button class="btn btn-primary btn-lg btn-block" type="button"><i class="fa fa-arrow-circle-right"></i> Tage der offenen Tür</button></a>
<p style="text-align:center">an österreichischen Bildungseinrichtungen</p>
</div>
<div class="col-lg-4 col-md-6 col-xs-12">
<a href="newsletter_berufsinformation.php"><button class="btn btn-primary btn-lg btn-block" type="button"><i class="fa fa-arrow-circle-right"></i> NEWsletter Berufsinformation</button></a>
<p style="text-align:center">Archiv und Registrierung</p>
</div>
<div class="col-lg-4 col-md-6 col-xs-12">
<a href="https://www.berufsinfo.at/02_berufsberatung/wk/berufsberater_wk.htm" target="_blank"><button class="btn btn-primary btn-lg btn-block" type="button"><i class="fa fa-arrow-circle-right"></i> Berufszentren d. Wirtschaftskammern</button></a>
<p style="text-align:center">Kontaktdaten</p>
</div>
<div class="col-lg-4 col-md-6 col-xs-12">
<a href="themen.php"><button class="btn btn-primary btn-lg btn-block" type="button"><i class="fa fa-arrow-circle-right"></i> Im Fokus</button></a>
<p style="text-align:center">Weiterführende Infos rund um Bildung und Beruf</p>
</div>
<div class="col-lg-4 col-md-6 col-xs-12">
<a href="https://bildungsfoerderung.bic.at" rel="noreferrer noopener" target="_blank"><button class="btn btn-primary btn-lg btn-block" type="button"><i class="fa fa-arrow-circle-right"></i> Bildungsförderungen</button></a>
<p style="text-align:center">Datenbank zu Aus- und Weiterbildungsförderungen</p>
</div>
<div class="col-lg-4 col-md-6 col-xs-12">
<a href="bic_broschuren.php" target="_blank"><button class="btn btn-primary btn-lg btn-block" type="button"><i class="fa fa-arrow-circle-right"></i> Broschüren</button></a>
<p style="text-align:center">Downloadmöglichkeit für Materialien und Broschüren</p>
</div>
<ul class="col-lg-12 col-md-12 col-xs-12 link-list">
<li class="title">Mehr Service</li>
<li><a href="bic_abclist.php"><i class="fa fa-arrow-circle-right"></i>Bildung &amp; Beruf von A bis Z</a></li>
<li><a href="https://www.wko.at/service/bildung-lehre/Lehrlingsstellen-der-Wirtschaftskammern.html" rel="noreferrer noopener" target="_blank"><i class="fa fa-arrow-circle-right"></i>Lehrlingsstellen der Wirtschaftskammern</a></li>
<li><a href="https://lehrbetriebsuebersicht.wko.at" rel="noreferrer noopener" target="_blank"><i class="fa fa-arrow-circle-right"></i>Lehrbetriebsübersicht</a></li>
<li><a href="https://www.berufsinfo.at/00_all/termine.htm" target="_blank"><i class="fa fa-arrow-circle-right"></i>Veranstaltungen und Termine</a></li>
<li><a href="https://www.wko.at/site/ufs_de/Unternehmerfuehrerschein.html" rel="noreferrer noopener" target="_blank"><i class="fa fa-arrow-circle-right"></i>Unternehmerführerschein</a></li>
<li><a href="https://aws.ibw.at" rel="noreferrer noopener" target="_blank"><i class="fa fa-arrow-circle-right"></i>AWS - Arbeitsgemeinschaft Wirtschaft und Schule</a></li>
<li><a href="bic_feedback.php"><i class="fa fa-arrow-circle-right"></i>Feedback</a></li>
</ul>
</div>
</div>
</li>
<li class="rightmenu">
<form action="bic_search.php" class="topmenusearch" id="searchform" method="post">
<input id="search" name="searchword" placeholder="Suchen" style="padding-left:5px;padding-right:45px;"/>
<button class="btnstyle">
<i aria-hidden="true" class="searchicon fa fa-search"></i>
</button>
</form>
</li>
</ul> </nav>
<div class="wsmobileheader clearfix pull-right col-lg-12">
<a class="animated-arrow" id="wsnavtoggle"><span></span></a>
</div>
</div>
</div>
</div>
</div> <div aria-labelledby="myModalLabel" class="modal fade" id="myHelp" role="dialog" tabindex="-1">
<div class="modal-dialog" role="document" style="width:80%;max-width:1200px;">
<div class="modal-content">
<div class="modal-header">
<button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
<h4 class="modal-title" id="HelpLabel">Hilfe</h4>
</div>
<div class="modal-body" id="help-body">
					...
				</div>
<div class="modal-footer">
<button class="btn btn-default" data-dismiss="modal" type="button">Schließen</button>
</div>
</div>
</div>
</div>
<div class="carousel slide" data-ride="carousel" id="myCarousel">
<!-- Indicators -->
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#myCarousel"></li>
<li data-slide-to="1" data-target="#myCarousel"></li>
<li data-slide-to="2" data-target="#myCarousel"></li>
<li data-slide-to="3" data-target="#myCarousel"></li>
<li data-slide-to="4" data-target="#myCarousel"></li>
</ol>
<div class="carousel-inner" role="listbox">
<div class="item active slide1">
<div class="container">
<div class="carousel-caption">
<h1>Herzlich willkommen auf BIC.at!</h1>
<p>Das Online-Portal für deine Berufswegplanung. Alle Information zur Berufswahl, Berufen und Ausbildungen.</p>
</div>
</div>
</div>
<div class="item slide2">
<div class="container">
<div class="carousel-caption">
<h1>BERUFSINFORMATION zu 2.000 Berufen</h1>
<p>Finde Beschreibungen, Anforderungen, Ausbildungsmöglichkeiten, Fotos und Filmen zu 2.000 Berufen. Unterschiedliche Zugänge helfen dir dabei.</p>
</div>
</div>
</div>
<div class="item slide3">
<div class="container">
<div class="carousel-caption">
<h1>INTERESSENPROFIL zur ersten Orientierung</h1>
<p>Welche Berufsbereiche interessieren dich am meisten? Welche Berufe und Ausbildungsmöglichkeiten gibt es in diesen Bereichen? 66 Fragen helfen dir das herauszufinden.</p>
<p><a class="btn btn-lg btn-primary" href="bic_interessenprofil_intro.php" role="button">Los geht´s</a></p>
</div>
</div>
</div>
<div class="item slide4">
<div class="container">
<div class="carousel-caption">
<h1>BERUFSWAHL – Dein Weg zum Wunschberuf</h1>
<p>Wie wähle ich eine passende Ausbildung, einen passenden Beruf? Wie bewerbe ich mich richtig?<br/>Unsere Tipps zur Berufswahl und Bewerbung helfen dir dabei.</p>
</div>
</div>
</div>
<div class="item slide5">
<div class="container">
<div class="carousel-caption">
<h1>SERVICE &amp; THEMA</h1>
<p>Viele Zusatzinfos für Jugendliche &amp; Erwachsene, Eltern, Lehrer*innen und Berufsberater*innen.</p>
</div>
</div>
</div>
</div>
<a class="left carousel-control" data-slide="prev" href="#myCarousel" role="button">
<span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>
<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" data-slide="next" href="#myCarousel" role="button">
<span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>
<span class="sr-only">Next</span>
</a>
</div>
<div class="container" style="width: 100%;background-color:#00cc66;">
<div class="container marketing news">
<div class="row">
<h2 style="color:#ffffff;font-weight:700;font-size:36px;margin-bottom:10px;width:100%;padding-left:5px;background-color:#0055a4;">News</h2>
</div>
<section id="demos">
<div class="row">
<div class="large-12 columns">
<div class="owl-carousel owl-theme">
<div class="item hvr-sweep-to-right">
<img alt="Foto: ibw-Fotowettbewerb" src="images/news/news_newsletter_1.png" title="Foto: ibw-Fotowettbewerb"/>
<div style="margin-top:15px;">
<h3>NEWSletter Berufsinformation 06/2020</h3>
<p>Der aktuelle NEWSletter Berufsinformation berichtet u. a. über eine neuen ETF-Bericht zu Career Guidance und die ibw-Studie zum Fachkräfteradar und enthält einige spannende Link-Tipps.</p>
<p>
<a href="https://99890.seu2.cleverreach.com/m/12302868/987289-3df694d2d15654102ce90f69e066902a87d37dcac0c2c085a0dac1da060c27ec2a79fa32c1c6feda006d35f6ee29a1ab" rel="noreferrer noopener" target="_blank"><span aria-hidden="true" class="fa fa-chevron-circle-right"></span> aktueller NEWSletter</a></p>
<a href="newsletter_berufsinformation.php"><span aria-hidden="true" class="fa fa-chevron-circle-right"></span> Archiv</a>
</div>
</div>
<div class="item hvr-sweep-to-right">
<img alt="Foto: fotolia.com" src="images/news/news_interviews_1.png" title="Foto: fotolia.com"/>
<div style="margin-top:15px;">
<h3>Interview: Rechtsanwalt/Rechtsanwältin</h3>
<p>Dr. Oliver Peschel erzählt in unserem  NEWSletter Interview über seinen Werdegang zum Rechtsanwalt, seinen Arbeitsalltag und worauf es in diesem Berufs besonders ankommt.</p>
<p><a href="downloads/de/archiv/interviews/interview_peschel_rechtsanwalt.pdf" rel="noreferrer noopener" target="_blank"><span aria-hidden="true" class="fa fa-chevron-circle-right"></span> Interview</a></p>
</div>
</div>
<div class="item hvr-sweep-to-right">
<img alt="Foto: ibw-Fotowettbewerb" src="images/news/news_broschueren_1.png" title="Foto: ibw-Fotowettbewerb"/>
<div style="margin-top:15px;">
<h3>Lehrberufslexikon - Ausgabe 2020</h3>
<p>Die Neuauflage der Broschüre „Lehrberufe in Österreich – Ausbildungen mit Zukunft“ ist erschienen und als Print und Download erhältlich.  Die Publikation gibt mit Kurzinfos einen Überblick über alle Lehrberufe die in Österreich erlernt werden können.<br/>
</p><p><span aria-hidden="true" class="fa fa-chevron-circle-right"></span> <a href="mailto:bliem@ibw.at">Bestellinfo</a><br/>
<span aria-hidden="true" class="fa fa-chevron-circle-right"></span> <a href="downloads/de/broschueren/lehrberufe_in_oesterreich_2020.pdf" rel="noreferrer noopener" target="_blank">Download</a></p>
</div>
</div>
<div class="item hvr-sweep-to-right">
<img alt="Foto: ibw-Fotowettbewerb" src="images/news/news_vermischtes_2.png" title="Foto: ibw-Fotowettbewerb"/>
<div style="margin-top:15px;">
<h3>Neue Filterfunktion auf BIC.at</h3>
<p>Unter Berufe von A bis Z steht ab sofort eine neue Filtermöglichkeit nach Interessen zur Verfügung.<br/>Du kannst dort aus 6 Interessenbereichen deinen persönlichen Interessentyp zusammenstellen und dir anschauen, welche Berufe dazu passen.</p>
<p>
<a href="berufe_von_a_bis_z.php"><span aria-hidden="true" class="fa fa-chevron-circle-right"></span> Berufe von A bis Z</a></p>
</div>
</div>
<div class="item hvr-sweep-to-right">
<img alt="Foto: dieindustrie.at / Mathias Kniepeiss" src="images/news/news_neue_berufe_1.png" title="Foto: dieindustrie.at / Mathias Kniepeiss"/>
<div style="margin-top:15px;">
<h3>Neue Lehrberufe seit 1. Juli</h3>
<p>Seit 1. Juli 2020 gibt es zwei neue Lehrberufe mit unterschiedlichen Spezialisierungen.<br/>
									Finde heraus, was hinter den Bezeichnungen steckt:
									</p>
<p><a href="berufsinformation.php?beruf=buchbindetechnik-und-postpresstechnologie_lehrberuf&amp;brfid=2847"><span aria-hidden="true" class="fa fa-chevron-circle-right"></span> Buchbindetechnik und Postpresstechnologie</a><br/>
<a href="berufsinformation.php?beruf=fertigungsmesstechnik_lehrberuf&amp;brfid=2848"><span aria-hidden="true" class="fa fa-chevron-circle-right"></span> Fertigungsmesstechnik</a><br/>
</p>
</div>
</div>
<div class="item hvr-sweep-to-right">
<img alt="Foto: dieindustrie.at / Mathias Kniepeiss" src="images/news/news_neue_berufe_1.png" title="Foto: dieindustrie.at / Mathias Kniepeiss"/>
<div style="margin-top:15px;">
<h3>Neue Lehrberufe auf BIC.at</h3>
<p>Seit 1. Mai gibt es spannende neue Lehrberufe. Informiere dich auf BIC.at:</p>
<p><a href="berufsinformation.php?beruf=assistentin-in-der-sicherheitsverwaltung_lehrberuf&amp;brfid=2796"><span aria-hidden="true" class="fa fa-chevron-circle-right"></span> AssistentIn in der Sicherheitsverwaltung</a><br/>
<a href="berufsinformation.php?beruf=eventkaufmann-eventkauffrau_lehrberuf&amp;brfid=2795"><span aria-hidden="true" class="fa fa-chevron-circle-right"></span> Eventkaufmann / Eventkauffrau</a><br/>
<a href="berufsinformation.php?beruf=hotel-und-restaurantfachmann-hotel-und-restaurantfachfrau_lehrberuf&amp;brfid=2794"><span aria-hidden="true" class="fa fa-chevron-circle-right"></span> Hotel- und Restaurantfachmann / Hotel- und Restaurantfachfrau</a><br/>
</p></div>
</div>
<div class="item hvr-sweep-to-right">
<img alt="Foto: ibw-Fotowettbewerb" src="images/news/news_vermischtes_2.png" title="Foto: ibw-Fotowettbewerb"/>
<div style="margin-top:15px;">
<h3>Lehrstellensuchende aufgepasst!</h3>
<p>Du suchst eine Lehrstelle, bist aber unsicher, ob du dich derzeit bewerben sollst? Viele Betriebe suchen auch jetzt Jugendliche für einen Ausbildungsbeginn im Herbst.</p>
<p>Wichtige Lehrstellenbörsen:</p>
<ul>
<li><a href="https://www.lehrberuf.info/" rel="noreferrer noopener" target="_blank">https://www.lehrberuf.info/</a></li>
<li><a href="https://www.ams.at/lehrstellen" rel="noreferrer noopener" target="_blank">https://www.ams.at/lehrstellen</a></li>
</ul>
<p>oder direkt bei Betrieben anfragen!</p>
</div>
</div>
<div class="item hvr-sweep-to-right">
<img alt="Polarstern Logo" src="images/news/polarstern.jpg" title="Polarstern Logo"/>
<div style="margin-top:15px;">
<h3>Polarstern-App</h3>
<p>Finde jetzt deine Stärken und werde dadurch selbstbewusster und mutiger - ein wichtiger Schritt in der Berufsorientierung!<br/>
									Gratis im App Store, Google Play Store und unter <a href="https://www.polarstern.me" rel="noreferrer noopener" target="_blank"><span aria-hidden="true" class="fa fa-chevron-circle-right"></span> www.polarstern.me</a>!</p>
<p>
</p></div>
</div>
<div class="item hvr-sweep-to-right">
<img alt="Foto: fotolia.com" src="images/news/news_termine_1.png" title="Foto: fotolia.com"/>
<div style="margin-top:15px;">
<h3>Messen für Bildung &amp; Beruf 2020</h3>
<p>
<span aria-hidden="true" class="fa fa-chevron-circle-right"></span> 04. – 07.03.2021 <a href="https://www.bestinfo.at/de/" rel="noreferrer noopener" target="_blank">BeSt</a>- digital<br/>
<span aria-hidden="true" class="fa fa-chevron-circle-right"></span> <a href="https://www.berufsinfo.at/00_all/messe.htm" rel="noreferrer noopener" target="_blank"> weitere Termine</a>
</p>
</div>
</div> </div>
<script>
						$(document).ready(function() {
						$('.owl-carousel').owlCarousel({
						rtl: false,
							loop: true,
							margin: 10,
							responsiveClass: true,
							responsive: {
								0: {
									items: 1,
									nav: true
								},
								600: {
									items: 2,
									nav: false
								},
								1200: {
									items: 3,
									nav: true,
									loop: false,
									margin: 20
									}
								}
							})
						})
						</script>
</div>
</div>
</section>
</div>
</div>
<div class="container" style="width: 100%;background-color:#2d71b5;padding-top:40px;">
<div class="container marketing">
<div class="row">
<h2 style="color:#ffffff;font-weight:700;font-size:36px;margin-bottom:25px;width:100%;padding-left:5px;background-color:#0055a4;">Berufsinformation</h2>
</div>
<div class="row">
<div class="col-lg-4 col-md-6 item1">
<a href="berufe_von_a_bis_z.php" title="Berufe von A bis Z - Foto: dieindustrie.at / Mathias Kniepeiss"><img alt="Berufe von A bis Z" src="images/startseite/berufe_von_a_bis_z.png"/></a>
<h2><img alt="Berufe von A bis Z" src="images/icons/berufe_a_z_black.png"/>Berufe von A bis Z</h2>
<p>2.000 Berufe in alphabetischer Liste</p>
<p><a class="btn btn-default hvr-sweep-to-right" href="berufe_von_a_bis_z.php" role="button">Weiter »</a></p>
</div>
<div class="col-lg-4 col-md-6 item2">
<a href="berufsgruppen.php" title="Berufsgruppen - Foto: fotolia.com"><img alt="Berufsgruppen" src="images/startseite/berufsgruppen.png"/></a>
<h2><img alt="Berufsgruppen" src="images/icons/berufsgruppen_black.png"/>Berufsgruppen</h2>
<p>Wähle deine Berufe und Ausbildungen aus 22 Berufsgruppen.</p>
<p><a class="btn btn-default hvr-sweep-to-right" href="berufsgruppen.php" role="button">Weiter »</a></p>
</div>
<div class="col-lg-4 col-md-6 item3">
<a href="arbeitsfelder.php" title="Arbeitsfelder - Foto: fotolia.com"><img alt="Arbeitsfelder" src="images/startseite/arbeitsfelder.png"/></a>
<h2><img alt="Arbeitsfelder" src="images/icons/arbeitsfelder_black.png"/>Arbeitsfelder</h2>
<p>Wähle deine Berufe aus 32 Arbeitsfeldern.</p>
<p><a class="btn btn-default hvr-sweep-to-right" href="arbeitsfelder.php" role="button">Weiter »</a></p>
</div>
<div class="col-lg-4 col-md-6 item4">
<a href="bildungswege.php" title="Bildungswege - Foto: dieindustrie.at / Mathias Kniepeiss"><img alt="Bildungswege" src="images/startseite/bildungswege.png"/></a>
<h2><img alt="Bildungswege" src="images/icons/bildungswege_black.png"/>Bildungswege</h2>
<p>Alle Berufe sortiert nach Bildungsweg: Lehrberufe, schulische Berufe, akademische Berufe und andere.</p>
<p><a class="btn btn-default hvr-sweep-to-right" href="bildungswege.php" role="button">Weiter »</a></p>
</div>
<div class="col-lg-4 col-md-6 item5">
<a href="berufsfilme_von_a_bis_z.php" title="Berufsinfofilme - Foto: dieindustrie.at / Mathias Kniepeiss"><img alt="Berufsinfofilme" src="images/startseite/berufsinformationsfilme.png"/></a>
<h2><img alt="Berufsinfofilme" src="images/icons/berufsinfofilme_black.png"/>Berufsinfofilme</h2>
<p>Berufsvideos von A bis Z &amp; Themenvideos zu Berufswahl, Ausbildung und Bewerbung</p>
<p><a class="btn btn-default hvr-sweep-to-right" href="berufsfilme_von_a_bis_z.php" role="button">Weiter »</a></p>
</div>
<div class="col-lg-4 col-md-6 item6">
<a href="aus_und_weiterbildung.php" title="Aus- und Weiterbildung - Foto: dieindustrie.at / Mathias Kniepeiss"><img alt="Aus- und Weiterbildung" src="images/startseite/aus_und_weiterbildung.png"/></a>
<h2><img alt="Aus- und Weiterbildung" src="images/icons/aus_weiterbildung_black.png"/>Aus- und Weiterbildung</h2>
<p>Suche nach Aus- und Weiterbildungsmöglichkeiten und Anbieter</p>
<p><a class="btn btn-default hvr-sweep-to-right" href="aus_und_weiterbildung.php" role="button">Weiter »</a></p>
</div>
</div>
<p> </p>
<p> </p>
<p> </p>
</div>
</div>
<footer id="footer">
<div class="container">
<div class="row" style="margin-top:5px;">
<div class="col-lg-3 col-md-12 col-sm-12 footer1">
<a href="https://www.wko.at" target="_blank" title="Wirtschaftskammer Österreich"><img alt="Wirtschaftskammer Österreich" src="images/logos/wko.png" title="Wirtschaftskammer Österreich"/></a>
</div>
<div class="col-lg-5 col-md-12 col-sm-12 footer2">
<ul class="list-inline text-center no-print">
<li>
<a href="https://twitter.com/home?status=BIC.at BerufsInformationsComputer" target="_blank">
<span class="fa-stack fa-lg">
<span class="fa fa-circle fa-stack-2x"></span>
<span class="fa fa-twitter fa-stack-1x fa-inverse"></span>
</span>
</a>
</li>
<li>
<a href="https://www.facebook.com/sharer/sharer.php?u=https://www.bic.at" target="_blank">
<span class="fa-stack fa-lg">
<span class="fa fa-circle fa-stack-2x"></span>
<span class="fa fa-facebook fa-stack-1x fa-inverse"></span>
</span>
</a>
</li>
</ul>
<p class="copyright" style="color:#ffffff;text-align:center;">© 2021 ibw - Institut für Bildungsforschung der Wirtschaft</p>
</div>
<div class="col-lg-4 col-md-12 col-sm-12 footer3">
<div class="row">
<div class="col-lg-12" style="text-align:center;">
<div style="display:inline-block;">
<ul class="list-inline text-center no-print">
<li>
<a href="bic_feedback.php">
<span class="fa-stack fa-lg">
<span class="fa fa-circle fa-stack-2x"></span>
<span aria-hidden="true" class="fa fa-comments fa-stack-1x fa-inverse"></span>
</span>
</a>
</li>
</ul>
<p style="text-align:center;">  <a class="inverse" href="bic_feedback.php">Feedback</a>  </p>
</div>
<div style="display:inline-block;">
<ul class="list-inline text-center no-print">
<li>
<a href="datenschutz.php">
<span class="fa-stack fa-lg">
<span class="fa fa-circle fa-stack-2x"></span>
<span aria-hidden="true" class="fa fa-eye fa-stack-1x fa-inverse"></span>
</span>
</a>
</li>
</ul>
<p style="text-align:center;">  <a class="inverse" href="datenschutz.php">Datenschutz</a>  </p>
</div>
<div style="display:inline-block;">
<ul class="list-inline text-center no-print">
<li>
<a href="impressum.php">
<span class="fa-stack fa-lg">
<span class="fa fa-circle fa-stack-2x"></span>
<span aria-hidden="true" class="fa fa-eye fa-stack-1x fa-inverse"></span>
</span>
</a>
</li>
</ul>
<p style="text-align:center;">  <a class="inverse" href="impressum.php">Impressum</a>  </p>
</div>
</div>
</div>
</div>
</div>
</div>
<a class="go-top no-print" href="#"><span class="glyphicon glyphicon-chevron-up"></span></a>
</footer>
<script src="template/js/googletags.js"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'UA-116463844-1', { 'anonymize_ip': true });
</script>
<script>
  jQuery(function() {
    jQuery('a[href$=".pdf"]').click(function(){
      var pdf=jQuery(this).attr('href');
	  gtag('event', 'download', { 'event_category': 'download', 'event_action': 'pdf', 'event_label': pdf});
    });
    jQuery('a[href^="mailto:"]').click(function(){
      var mailto=jQuery(this).attr('href');
      gtag('event', 'contact', { event_category: 'contact', event_action: 'mailto', event_label: mailto});
    });
  });
</script> </body>
</html>