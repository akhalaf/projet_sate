<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]--><!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]--><!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]--><!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"/>
<!-- Google Web Fonts
================================================== -->
<link href="//fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"/>
<meta content="website" property="og:type"/>
<meta content="404 Not Found" property="og:title"/>
<meta content="" property="og:url"/>
<meta content="DrevalAir" property="og:site_name"/>
<meta content="summary" name="twitter:card"/>
<meta content="@#" name="twitter:site"/>
<meta content="jhGGKLBxT1sCN9fRsgIHF4JDkzw4AiubDjat3u9Ug4I" name="google-site-verification"/>
<!-- Meta -->
<title>
    Page Not Found
  </title>
<link href="" rel="canonical"/>
<!-- Viewport -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<!-- CSS -->
<link href="//cdn.shopifycdn.net/s/files/1/2988/5360/t/2/assets/stylesheet.css?v=3338576889678656443" media="all" rel="stylesheet" type="text/css"/>
<link href="//cdn.shopifycdn.net/s/files/1/2988/5360/t/2/assets/queries.css?v=6139348626509800279" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
<!-- JS -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js" type="text/javascript"></script>
<script src="//cdn.shopifycdn.net/s/files/1/2988/5360/t/2/assets/theme.js?v=11820072094513705464" type="text/javascript"></script>
<script src="//cdn.shopifycdn.net/shopifycloud/shopify/assets/themes_support/option_selection-fe6b72c2bbdd3369ac0bfefe8648e3c889efca213baefd4cfb0dd9363563831f.js" type="text/javascript"></script>
<script>window.performance && window.performance.mark && window.performance.mark('shopify.content_for_header.start');</script><meta content="/29885360/digital_wallets/dialog" id="shopify-digital-wallet" name="shopify-digital-wallet"/>
<meta content="3449a34e6b5622de8b6f75c81b4fbb6b" name="shopify-checkout-api-token"/>
<meta data-currency="USD" data-environment="production" data-locale="en_US" data-paypal-v4="true" data-shop-id="29885360" data-venmo-supported="true" id="in-context-paypal-metadata"/>
<meta data-amazon-payments="true" data-amazon-payments-callback-url="https://drevalair.com/29885360/amazon_payments/callback" data-amazon-payments-client-id="amzn1.application-oa2-client.a278e9f3746a4a2d9933c62d6ce39cf2" data-amazon-payments-language="en-US" data-amazon-payments-region="US" data-amazon-payments-sandbox-mode="false" data-amazon-payments-seller-id="A2G2AZPLCTQA0B" data-amazon-payments-widget-library-url="https://static-na.payments-amazon.com/OffAmazonPayments/us/js/Widgets.js" id="amazon-payments-metadata"/>
<link href="https://monorail-edge.shopifysvc.com" rel="dns-prefetch"/>
<script id="apple-pay-shop-capabilities" type="application/json">{"shopId":29885360,"countryCode":"US","currencyCode":"USD","merchantCapabilities":["supports3DS"],"merchantId":"gid:\/\/shopify\/Shop\/29885360","merchantName":"DrevalAir","requiredBillingContactFields":["postalAddress","email","phone"],"requiredShippingContactFields":["postalAddress","email","phone"],"shippingType":"shipping","supportedNetworks":["visa","masterCard","amex","jcb","discover"],"total":{"type":"pending","label":"DrevalAir","amount":"1.00"}}</script>
<script id="shopify-features" type="application/json">{"accessToken":"3449a34e6b5622de8b6f75c81b4fbb6b","betas":["rich-media-storefront-analytics"],"domain":"drevalair.com","predictiveSearch":true,"shopId":29885360,"smart_payment_buttons_url":"https:\/\/cdn.shopifycdn.net\/shopifycloud\/payment-sheet\/assets\/latest\/spb.en.js","dynamic_checkout_cart_url":"https:\/\/cdn.shopifycdn.net\/shopifycloud\/payment-sheet\/assets\/latest\/dynamic-checkout-cart.en.js","locale":"en"}</script>
<script>var Shopify = Shopify || {};
Shopify.shop = "drevalair.myshopify.com";
Shopify.locale = "en";
Shopify.currency = {"active":"USD","rate":"1.0"};
Shopify.theme = {"name":"Vantage","id":15655174188,"theme_store_id":459,"role":"main"};
Shopify.theme.handle = "null";
Shopify.theme.style = {"id":null,"handle":null};
Shopify.cdnHost = "cdn.shopifycdn.net";</script>
<script type="module">!function(o){(o.Shopify=o.Shopify||{}).modules=!0}(window);</script>
<script>!function(o){function n(){var o=[];function n(){o.push(Array.prototype.slice.apply(arguments))}return n.q=o,n}var t=o.Shopify=o.Shopify||{};t.loadFeatures=n(),t.autoloadFeatures=n()}(window);</script>
<script>(function() {
  function asyncLoad() {
    var urls = ["https:\/\/error404.atomseo.com\/scripts\/shopify-404-real-time-monitoring.js?shop=drevalair.myshopify.com"];
    for (var i = 0; i < urls.length; i++) {
      var s = document.createElement('script');
      s.type = 'text/javascript';
      s.async = true;
      s.src = urls[i];
      var x = document.getElementsByTagName('script')[0];
      x.parentNode.insertBefore(s, x);
    }
  };
  if(window.attachEvent) {
    window.attachEvent('onload', asyncLoad);
  } else {
    window.addEventListener('load', asyncLoad, false);
  }
})();</script>
<script id="__st">var __st={"a":29885360,"offset":-18000,"reqid":"3aa5ccfd-dae0-415e-a4aa-6f24fc58ac94","pageurl":"drevalair.com\/404","u":"8f4c34ffce78"};</script>
<script>window.ShopifyPaypalV4VisibilityTracking = true;</script>
<script>window.ShopifyAnalytics = window.ShopifyAnalytics || {};
window.ShopifyAnalytics.meta = window.ShopifyAnalytics.meta || {};
window.ShopifyAnalytics.meta.currency = 'USD';
var meta = {"page":{}};
for (var attr in meta) {
  window.ShopifyAnalytics.meta[attr] = meta[attr];
}</script>
<script>window.ShopifyAnalytics.merchantGoogleAnalytics = function() {
  
};
</script>
<script class="analytics">(function () {
  var customDocumentWrite = function(content) {
    var jquery = null;

    if (window.jQuery) {
      jquery = window.jQuery;
    } else if (window.Checkout && window.Checkout.$) {
      jquery = window.Checkout.$;
    }

    if (jquery) {
      jquery('body').append(content);
    }
  };

  var hasLoggedConversion = function(token) {
    if (document.cookie.indexOf('loggedConversion=' + window.location.pathname) !== -1) {
      return true;
    }
    if (token) {
      return document.cookie.indexOf('loggedConversion=' + token) !== -1;
    }
    return false;
  }

  var setCookieIfConversion = function(token) {
    if (token) {
      var twoMonthsFromNow = new Date(Date.now());
      twoMonthsFromNow.setMonth(twoMonthsFromNow.getMonth() + 2);

      document.cookie = 'loggedConversion=' + token + '; expires=' + twoMonthsFromNow;
    }
  }

  var trekkie = window.ShopifyAnalytics.lib = window.trekkie = window.trekkie || [];
  if (trekkie.integrations) {
    return;
  }
  trekkie.methods = [
    'identify',
    'page',
    'ready',
    'track',
    'trackForm',
    'trackLink'
  ];
  trekkie.factory = function(method) {
    return function() {
      var args = Array.prototype.slice.call(arguments);
      args.unshift(method);
      trekkie.push(args);
      return trekkie;
    };
  };
  for (var i = 0; i < trekkie.methods.length; i++) {
    var key = trekkie.methods[i];
    trekkie[key] = trekkie.factory(key);
  }
  trekkie.load = function(config) {
    trekkie.config = config;
    var first = document.getElementsByTagName('script')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.onerror = function(e) {
      var scriptFallback = document.createElement('script');
      scriptFallback.type = 'text/javascript';
      scriptFallback.onerror = function(error) {
              var Monorail = {
      produce: function produce(monorailDomain, schemaId, payload) {
        var currentMs = new Date().getTime();
        var event = {
          schema_id: schemaId,
          payload: payload,
          metadata: {
            event_created_at_ms: currentMs,
            event_sent_at_ms: currentMs
          }
        };
        return Monorail.sendRequest("https://" + monorailDomain + "/v1/produce", JSON.stringify(event));
      },
      sendRequest: function sendRequest(endpointUrl, payload) {
        // Try the sendBeacon API
        if (window && window.navigator && typeof window.navigator.sendBeacon === 'function' && typeof window.Blob === 'function' && !Monorail.isIos12()) {
          var blobData = new window.Blob([payload], {
            type: 'text/plain'
          });
    
          if (window.navigator.sendBeacon(endpointUrl, blobData)) {
            return true;
          } // sendBeacon was not successful
    
        } // XHR beacon   
    
        var xhr = new XMLHttpRequest();
    
        try {
          xhr.open('POST', endpointUrl);
          xhr.setRequestHeader('Content-Type', 'text/plain');
          xhr.send(payload);
        } catch (e) {
          console.log(e);
        }
    
        return false;
      },
      isIos12: function isIos12() {
        return window.navigator.userAgent.lastIndexOf('iPhone; CPU iPhone OS 12_') !== -1 || window.navigator.userAgent.lastIndexOf('iPad; CPU OS 12_') !== -1;
      }
    };
    Monorail.produce('monorail-edge.shopifysvc.com',
      'trekkie_storefront_load_errors/1.1',
      {shop_id: 29885360,
      theme_id: 15655174188,
      app_name: "storefront",
      context_url: window.location.href,
      source_url: "https://cdn.shopifycdn.net/s/trekkie.storefront.5f7bf96405492b9c1557a597e8c96d3d65752676.min.js"});

      };
      scriptFallback.async = true;
      scriptFallback.src = 'https://cdn.shopifycdn.net/s/trekkie.storefront.5f7bf96405492b9c1557a597e8c96d3d65752676.min.js';
      first.parentNode.insertBefore(scriptFallback, first);
    };
    script.async = true;
    script.src = 'https://cdn.shopifycdn.net/s/trekkie.storefront.5f7bf96405492b9c1557a597e8c96d3d65752676.min.js';
    first.parentNode.insertBefore(script, first);
  };
  trekkie.load(
    {"Trekkie":{"appName":"storefront","development":false,"defaultAttributes":{"shopId":29885360,"isMerchantRequest":null,"themeId":15655174188,"themeCityHash":"8327668835388990308","contentLanguage":"en","currency":"USD"},"isServerSideCookieWritingEnabled":true,"isPixelGateEnabled":true},"Performance":{"navigationTimingApiMeasurementsEnabled":true,"navigationTimingApiMeasurementsSampleRate":1},"Session Attribution":{}}
  );

  var loaded = false;
  trekkie.ready(function() {
    if (loaded) return;
    loaded = true;

    window.ShopifyAnalytics.lib = window.trekkie;
    

    var originalDocumentWrite = document.write;
    document.write = customDocumentWrite;
    try { window.ShopifyAnalytics.merchantGoogleAnalytics.call(this); } catch(error) {};
    document.write = originalDocumentWrite;
      (function () {
        if (window.BOOMR && (window.BOOMR.version || window.BOOMR.snippetExecuted)) {
          return;
        }
        window.BOOMR = window.BOOMR || {};
        window.BOOMR.snippetStart = new Date().getTime();
        window.BOOMR.snippetExecuted = true;
        window.BOOMR.snippetVersion = 12;
        window.BOOMR.application = "core";
        window.BOOMR.shopId = 29885360;
        window.BOOMR.themeId = 15655174188;
        window.BOOMR.themeName = "Vantage";
        window.BOOMR.themeVersion = "5.3.2";
        window.BOOMR.url =
          "https://cdn.shopifycdn.net/shopifycloud/boomerang/shopify-boomerang-1.0.0.min.js";
        var where = document.currentScript || document.getElementsByTagName("script")[0];
        var parentNode = where.parentNode;
        var promoted = false;
        var LOADER_TIMEOUT = 3000;
        function promote() {
          if (promoted) {
            return;
          }
          var script = document.createElement("script");
          script.id = "boomr-scr-as";
          script.src = window.BOOMR.url;
          script.async = true;
          parentNode.appendChild(script);
          promoted = true;
        }
        function iframeLoader(wasFallback) {
          promoted = true;
          var dom, bootstrap, iframe, iframeStyle;
          var doc = document;
          var win = window;
          window.BOOMR.snippetMethod = wasFallback ? "if" : "i";
          bootstrap = function(parent, scriptId) {
            var script = doc.createElement("script");
            script.id = scriptId || "boomr-if-as";
            script.src = window.BOOMR.url;
            BOOMR_lstart = new Date().getTime();
            parent = parent || doc.body;
            parent.appendChild(script);
          };
          if (!window.addEventListener && window.attachEvent && navigator.userAgent.match(/MSIE [67]./)) {
            window.BOOMR.snippetMethod = "s";
            bootstrap(parentNode, "boomr-async");
            return;
          }
          iframe = document.createElement("IFRAME");
          iframe.src = "about:blank";
          iframe.title = "";
          iframe.role = "presentation";
          iframe.loading = "eager";
          iframeStyle = (iframe.frameElement || iframe).style;
          iframeStyle.width = 0;
          iframeStyle.height = 0;
          iframeStyle.border = 0;
          iframeStyle.display = "none";
          parentNode.appendChild(iframe);
          try {
            win = iframe.contentWindow;
            doc = win.document.open();
          } catch (e) {
            dom = document.domain;
            iframe.src = "javascript:var d=document.open();d.domain='" + dom + "';void(0);";
            win = iframe.contentWindow;
            doc = win.document.open();
          }
          if (dom) {
            doc._boomrl = function() {
              this.domain = dom;
              bootstrap();
            };
            doc.write("<body onload='document._boomrl();'>");
          } else {
            win._boomrl = function() {
              bootstrap();
            };
            if (win.addEventListener) {
              win.addEventListener("load", win._boomrl, false);
            } else if (win.attachEvent) {
              win.attachEvent("onload", win._boomrl);
            }
          }
          doc.close();
        }
        var link = document.createElement("link");
        if (link.relList &&
          typeof link.relList.supports === "function" &&
          link.relList.supports("preload") &&
          ("as" in link)) {
          window.BOOMR.snippetMethod = "p";
          link.href = window.BOOMR.url;
          link.rel = "preload";
          link.as = "script";
          link.addEventListener("load", promote);
          link.addEventListener("error", function() {
            iframeLoader(true);
          });
          setTimeout(function() {
            if (!promoted) {
              iframeLoader(true);
            }
          }, LOADER_TIMEOUT);
          BOOMR_lstart = new Date().getTime();
          parentNode.appendChild(link);
        } else {
          iframeLoader(false);
        }
        function boomerangSaveLoadTime(e) {
          window.BOOMR_onload = (e && e.timeStamp) || new Date().getTime();
        }
        if (window.addEventListener) {
          window.addEventListener("load", boomerangSaveLoadTime, false);
        } else if (window.attachEvent) {
          window.attachEvent("onload", boomerangSaveLoadTime);
        }
        if (document.addEventListener) {
          document.addEventListener("onBoomerangLoaded", function(e) {
            e.detail.BOOMR.init({
              producer_url: "https://monorail-edge.shopifysvc.com/v1/produce",
              ResourceTiming: {
                enabled: true,
                trackedResourceTypes: ["script", "img", "css"]
              },
            });
            e.detail.BOOMR.t_end = new Date().getTime();
          });
        } else if (document.attachEvent) {
          document.attachEvent("onpropertychange", function(e) {
            if (!e) e=event;
            if (e.propertyName === "onBoomerangLoaded") {
              e.detail.BOOMR.init({
                producer_url: "https://monorail-edge.shopifysvc.com/v1/produce",
                ResourceTiming: {
                  enabled: true,
                  trackedResourceTypes: ["script", "img", "css"]
                },
              });
              e.detail.BOOMR.t_end = new Date().getTime();
            }
          });
        }
      })();
    

    
        window.ShopifyAnalytics.lib.page(
          null,
          {}
        );
      

    var match = window.location.pathname.match(/checkouts\/(.+)\/(thank_you|post_purchase)/)
    var token = match? match[1]: undefined;
    if (!hasLoggedConversion(token)) {
      setCookieIfConversion(token);
      
    }
  });

  
      var eventsListenerScript = document.createElement('script');
      eventsListenerScript.async = true;
      eventsListenerScript.src = "//cdn.shopifycdn.net/shopifycloud/shopify/assets/shop_events_listener-68ba3f1321f00bf07cb78a03841621079812265e950cdccade3463749ea2705e.js";
      document.getElementsByTagName('head')[0].appendChild(eventsListenerScript);
    
})();</script>
<script>!function(e){e.addEventListener("DOMContentLoaded",function(){var t;null!==e.querySelector('form[action^="/contact"] input[name="form_type"][value="contact"]')&&(window.Shopify=window.Shopify||{},window.Shopify.recaptchaV3=window.Shopify.recaptchaV3||{siteKey:"6LcCR2cUAAAAANS1Gpq_mDIJ2pQuJphsSQaUEuc9"},(t=e.createElement("script")).setAttribute("src","https://cdn.shopifycdn.net/shopifycloud/storefront-recaptcha-v3/v0.1/index.js"),e.body.appendChild(t))})}(document);</script>
<script crossorigin="anonymous" data-source-attribution="shopify.loadfeatures" defer="defer" integrity="sha256-JP8SIsmqE7shdlPA0+ooxAp5aigObaKa1CHuwqYHXIY=" src="//cdn.shopifycdn.net/shopifycloud/shopify/assets/storefront/load_feature-24ff1222c9aa13bb217653c0d3ea28c40a796a280e6da29ad421eec2a6075c86.js"></script>
<script crossorigin="anonymous" data-source-attribution="shopify.dynamic-checkout" defer="defer" integrity="sha256-h+g5mYiIAULyxidxudjy/2wpCz/3Rd1CbrDf4NudHa4=" src="//cdn.shopifycdn.net/shopifycloud/shopify/assets/storefront/features-87e8399988880142f2c62771b9d8f2ff6c290b3ff745dd426eb0dfe0db9d1dae.js"></script>
<style id="shopify-dynamic-checkout-cart">@media screen and (min-width: 750px) {
  #dynamic-checkout-cart {
    min-height: 50px;
  }
}

@media screen and (max-width: 750px) {
  #dynamic-checkout-cart {
    min-height: 240px;
  }
}
</style><script>window.performance && window.performance.mark && window.performance.mark('shopify.content_for_header.end');</script>
<!-- Favicon -->
<link href="//cdn.shopifycdn.net/s/files/1/2988/5360/files/logo_6x3inch_32x32.png?v=1522695380" rel="shortcut icon" type="image/png"/>
<!---fontawesome--->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<!--fontawesome end-->
<script> 
$(document).ready(function(){
    $(".toggle-title").click(function(){
        var $thi = $(this).parent().toggleClass('acco-active');
        $(".toggle").not($thi).removeClass('acco-active');
        var $this = $(this).siblings('.toggle-inner').slideToggle();
        $(".toggle-inner").not($this).slideUp();
        
    });
});
</script>
<script>
$(document).ready(function(){
  // Add smooth scrolling to all links
  $(".about_page a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>
<!-- Start of intelgadgets Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,this.zendeskHost="intelgadgets.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();
/*]]>*/</script>
<!-- End of intelgadgets Zendesk Widget script -->
<meta content="https://cdn.shopifycdn.net/s/files/1/2988/5360/files/logo_6x3inch.png?height=628&amp;pad_color=fff&amp;v=1522695380&amp;width=1200" property="og:image"/>
<meta content="https://cdn.shopifycdn.net/s/files/1/2988/5360/files/logo_6x3inch.png?height=628&amp;pad_color=fff&amp;v=1522695380&amp;width=1200" property="og:image:secure_url"/>
<meta content="1200" property="og:image:width"/>
<meta content="628" property="og:image:height"/>
</head>
<body class="gridlock shifter shifter-left ">
<div class="shopify-section" id="shopify-section-mobile-navigation"><nav class="shifter-navigation no-fouc" data-section-id="mobile-navigation" data-section-type="mobile-navigation">
<form action="/search" class="search-form" method="get">
<input class="search-field" id="q" name="q" placeholder="Search" type="text"/>
<button type="submit"><i class="fa fa-search"></i></button>
</form>
<div class="clear"></div>
<ul id="accordion">
<li><a href="/collections">Shop</a></li>
<li><a href="/pages/contact">Contact</a></li>
</ul>
</nav>
</div>
<div class="shifter-page">
<div id="wrapper">
<div class="shopify-section" id="shopify-section-header"><div class="header-section" data-section-id="header" data-section-type="header-section">
<div id="top-bar">
<div class="row">
<div class="phone_email ">
<ul>
<li class="phone"><a href="tel:888-200-3370">888-200-3370</a></li>
<li class="email"><a href="mailto:help@drevalair.com">help@drevalair.com</a></li>
</ul>
</div>
<div class="desktop-12 tablet-6 mobile-3">
</div>
</div>
</div>
<div class="main_header" id="mobile-header">
<div class="row">
<ul class="mobile-3" id="mobile-menu">
<li><span class="shifter-handle"><i aria-hidden="true" class="fa fa-bars"></i></span></li>
<li><a href="/account/login"><i class="fa fa-user"></i></a></li>
<li>
<div class="desktop-7 tablet-6 mobile-3" id="logo">
<a href="/"><img itemprop="logo" src="//cdn.shopifycdn.net/s/files/1/2988/5360/files/logo_6x3inch_600x.png?v=1522695380"/></a>
</div>
</li>
<li><a href="/cart"><i aria-hidden="true" class="fa fa-shopping-cart"></i> <span class="cart-count">0</span></a></li>
</ul>
</div>
</div>
<div class="main_header inline-header" id="header-wrapper">
<div class="row" id="header">
<div id="flex-header">
<div id="searchbox">
<form action="/search" method="get">
<input id="q" name="q" placeholder="search..." type="text"/>
</form>
</div>
<div class="desktop-3 tablet-4 tablet-push-1 mobile-3" id="logo">
<a href="/"><img itemprop="logo" src="//cdn.shopifycdn.net/s/files/1/2988/5360/files/logo_6x3inch_600x.png?v=1522695380"/></a>
</div>
<div class="navigation desktop-6 tablet-hide mobile-hide" role="navigation">
<ul class="nav">
<li><a href="/collections">Shop</a></li>
<li><a href="/pages/contact">Contact</a></li>
</ul>
</div>
<div class="desktop-3 tablet-hide mobile-hide" id="search">
<ul class=" desktop-push-2 tablet-hide mobile-hide" id="cart">
<li class="seeks"><a href="#"><i class="fa fa-search"></i></a></li>
<li><a href="/account/login">Account</a></li>
<li><a href="/cart"><i aria-hidden="true" class="fa fa-shopping-cart"></i> Cart: (<span class="cart-count">0</span>)</a></li>
</ul>
</div>
</div>
</div>
<meta content="d1a6697f02fd2846760cd29cd6fe80c4" name="p:domain_verify"/>
<!-- End Header -->
</div> <!-- End wrapper -->
</div>
<div class="clear"></div>
<style>

  
  @media screen and ( min-width: 981px ) { 
    #logo { text-align: left; } }
  .search-form { display: inline-block; width: 65%; }
  ul#cart { display: inline-block; float: right; }
  
  
  #logo a { 
    font-size: 18px; 
    text-transform: uppercase; 
    font-weight: 700; 
  }
  
  @media screen and ( min-width: 740px ) {
    #logo img { max-width: 200px; }
  }
  
  ul.nav li.dropdown ul.submenu { top: 55px; }
  ul.nav { line-height: 55px; }
  
 
  .navigation { 
    
    background: #ffffff; 
  }
  
  
  
  ul.nav { text-align: center; }
  
  
  
  #top-bar p { text-align: center; }
  
  
  
  .inline-header div#search {
    line-height: 55px;
  }
  
  #logo a { font-size: 0; display: block; }
  
</style>
<script type="text/javascript">
$(window).scroll(function(){
  var sticky = $('.main_header'),
      scroll = $(window).scrollTop();

  if (scroll >=500) sticky.addClass('fixed');
  else sticky.removeClass('fixed');
});		
		</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-117920631-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117920631-1');
</script>
</div>
<div class="desktop-12" id="breadcrumb">
<div class="row">
<a class="homepage-link" href="/" title="Home">Home</a>
<span class="separator">»</span>
<span class="page-title">404 Not Found</span>
</div>
</div>
<div class="clear"></div>
<div id="content">
<div class="desktop-4 desktop-push-4" id="error-page">
<h2 class="section-title">Oops - We couldn't find that one</h2>
<p style="text-align: center; margin-top: 40px;">This page does not exist. Try searching for what you would like to find:</p><br/>
<form accept-charset="utf-8" action="https://drevalair.com/search" method="get">
<input name="q" placeholder="Search" type="text"/>
</form>
</div>
</div>
<div class="clear"></div>
</div> <!-- End wrapper -->
<div class="shopify-section" id="shopify-section-footer"><div class="footer-section" data-section-id="footer" data-section-type="footer-section" id="footer">
<div class="container row">
<div class="desktop-12" id="big-footer">
<div class="ft_logo desktop-3 tablet-6 mobile-3 footer-sect">
<div class="ft_logo_inner">
<img src="//cdn.shopifycdn.net/s/files/1/2988/5360/files/logo_6x3inch.png?v=1522695380"/>
<div id="social-icons">
<a href="#"><i class="fa fa-facebook"></i></a>
<a href="#"><i class="fa fa-twitter"></i></a>
<a href="#"><i class="fa fa-instagram"></i></a>
</div>
</div>
</div>
<div class="desktop-3 tablet-6 mobile-3 footer-sect">
<h4>#drevalair</h4>
<ul>
<li><a href="/pages/about" title="">About Us</a></li>
<li><a href="/blogs/news" title="">Blog</a></li>
<li><a href="/pages/shipping-and-returns" title="">Shipping and Return</a></li>
</ul>
</div>
<div class="desktop-3 tablet-6 mobile-3 footer-sect">
<h4>CUSTOMER CARE</h4>
<ul>
<li><a href="#" title="">My Account</a></li>
<li><a href="/pages/contact" title="">Contact Us</a></li>
<li><a href="/pages/faq" title="">FAQ</a></li>
</ul>
</div>
<div class="desktop-3 tablet-6 mobile-3 footer-sect">
<h4>STAY CONNECTED!</h4>
<p>Join our mailing list for exclusive offers and promotions</p>
<div id="footer-signup">
<form accept-charset="UTF-8" action="/contact#contact_form" class="contact-form" id="contact_form" method="post"><input name="form_type" type="hidden" value="customer"/><input name="utf8" type="hidden" value="✓"/>
<input name="contact[tags]" type="hidden" value="prospect, password page"/>
<input id="email-address" name="contact[email]" placeholder="Enter Your Email Address" type="email"/>
<button type="submit"><i aria-hidden="true" class="fa fa-paper-plane-o"></i></button>
</form>
</div>
<div class="clear"></div>
</div>
</div>
</div>
<!--copyright-->
<div class="copyright_section">
<div class="row">
<div class="cp" id="close">
<p>Copyright © 2021 dreval</p>
</div>
<!-- <div class="desktop-8 mobile-3 footer_menu">
        <div class="footer-copright_menu">
           
          <ul>
            
              
          <ul>
            
              
          <ul>
            
              
          <ul>
            
             
          </ul>
        </div>
      </div> -->
</div>
</div>
<!--copyright end-->
</div>
<style>
  #footer { text-align: left; }
</style>
</div>
</div>
<script>

/**
 * Module to ajaxify all add to cart forms on the page.
 *
 * Copyright (c) 2014 Caroline Schnapp (11heavens.com)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */
Shopify.AjaxifyCart = (function($) {
  var _config = {
    addedToCartBtnLabel: '"Thank You!"',
    addingToCartBtnLabel: '"Adding..."',
    soldOutBtnLabel: '"Sold Out"',
    howLongTillBtnReturnsToNormal: 1000, // in milliseconds.
    cartCountSelector: '.cart-count',
    cartTotalSelector: '#cart-price',
    feedbackPosition: 'belowForm', // 'aboveForm' for top of add to cart form, 'belowForm' for below the add to cart form, and 'nextButton' for next to add to cart button.
    shopifyAjaxAddURL: '/cart/add.js',
    shopifyAjaxCartURL: '/cart.js'
  };
  var _showFeedback = function(success, html, addToCartForm) {
    $('.ajaxified-cart-feedback').remove();
    var feedback = '<p class="ajaxified-cart-feedback ' + success + '">' + html + '</p>';
    switch (_config.feedbackPosition) {
      case 'aboveForm':
        addToCartForm.before(feedback);
        break;
      case 'belowForm':
        addToCartForm.after(feedback);
        break;
      case 'nextButton':
      default:
        addToCartForm.find('input[type="submit"]').after(feedback);
        break;   
    }
    $('.ajaxified-cart-feedback').slideDown();
  };
  var _init = function() {   
    $(document).ready(function() { 
      $('form[action="/cart/add"]').submit(function(e) {
        e.preventDefault();
        var addToCartForm = $(this);  
        // Disable add to cart button.
        var addToCartBtn = addToCartForm.find('input[type="submit"]');
        addToCartBtn.attr('data-label', addToCartBtn.val());
        addToCartBtn.val(_config.addingToCartBtnLabel).addClass('disabled').attr('disabled', 'disabled');
        // Add to cart.
        $.ajax({
          url: _config.shopifyAjaxAddURL,
          dataType: 'json',
          type: 'post',
          data: addToCartForm.serialize(),
          success: function(itemData) {
            // Re-enable add to cart button.
            addToCartBtn.addClass('inverted').val(_config.addedToCartBtnLabel);
            _showFeedback('success','<i class="fa fa-check"></i> Added to Cart <a href="/cart">View Cart</a> or <a href="/collections/all">Continue Shopping</a>.',addToCartForm);
            window.setTimeout(function(){
              addToCartBtn.removeAttr('disabled').removeClass('disabled').removeClass('inverted').val(addToCartBtn.attr('data-label'));
            }, _config.howLongTillBtnReturnsToNormal);
            // Update cart count and show cart link.
            $.getJSON(_config.shopifyAjaxCartURL, function(cart) {
              if (_config.cartCountSelector && $(_config.cartCountSelector).size()) {
                var value = $(_config.cartCountSelector).html();
                $(_config.cartCountSelector).html(value.replace(/[0-9]+/,cart.item_count));
              }
              if (_config.cartTotalSelector && $(_config.cartTotalSelector).size()) {
                if (typeof Currency !== 'undefined' && typeof Currency.money_format !== 'undefined') {
                  var newCurrency = '';
                  if ($('[name="currencies"]').size()) {
                    newCurrency = $('[name="currencies"]').val();
                  }
                  else if ($('#currencies span.selected').size()) {
                    newCurrency = $('#currencies span.selected').attr('data-currency');
                  }
                  if (newCurrency) {
                    $(_config.cartTotalSelector).html('<span class=money>' + Shopify.formatMoney(Currency.convert(cart.total_price, "USD", newCurrency), Currency.money_format[newCurrency]) + '</span>');
                  } 
                  else {
                    $(_config.cartTotalSelector).html(Shopify.formatMoney(cart.total_price, "${{amount}}"));
                  }
                }
                else {
                  $(_config.cartTotalSelector).html(Shopify.formatMoney(cart.total_price, "${{amount}}"));
                }
              };
            });        
          }, 
          error: function(XMLHttpRequest) {
            var response = eval('(' + XMLHttpRequest.responseText + ')');
            response = response.description;
            if (response.slice(0,4) === 'All ') {
              _showFeedback('error', response.replace('All 1 ', 'All '), addToCartForm);
              addToCartBtn.removeAttr('disabled').val(_config.soldOutBtnLabel).attr('disabled','disabled');
            }
            else {
              _showFeedback('error', '<i class="fa fa-warning"></i> ' + response, addToCartForm);
              addToCartBtn.removeAttr('disabled').removeClass('disabled').removeClass('inverted').val(addToCartBtn.attr('data-label'));
            }
          }
        });   
        return false;    
      });
    });
  };
  return {
    init: function(params) {
        // Configuration
        params = params || {};
        // Merging with defaults.
        $.extend(_config, params);
        // Action
        $(function() {
          _init();
        });
    },    
    getConfig: function() {
      return _config;
    }
  }  
})(jQuery);

Shopify.AjaxifyCart.init();

</script>
<style>
.ajaxified-cart-feedback.error { color: #FF4136; } 
</style>
</body>
</html>