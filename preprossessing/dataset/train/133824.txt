<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<!-- start inc_head code -->
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="apple-touch-icon.png?v=20" rel="apple-touch-icon" sizes="180x180"/>
<link href="favicon-32x32.png?v=20" rel="icon" sizes="32x32" type="image/png"/>
<link href="favicon-16x16.png?v=20" rel="icon" sizes="16x16" type="image/png"/>
<link href="manifest.json?v=20" rel="manifest"/>
<link color="#002b5c" href="safari-pinned-tab.svg?v=20" rel="mask-icon"/>
<link href="favicon.ico?v=20" rel="shortcut icon"/>
<meta content="#002b5c" name="msapplication-TileColor"/>
<meta content="#002b5c" name="theme-color"/>
<link href="css/normalize.css" rel="stylesheet" type="text/css"/>
<link href="css/main.css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700|Roboto:100,300,400,500,900" rel="stylesheet"/>
<link href="css/lightcase.css" rel="stylesheet" type="text/css"/>
<link href="css/font-awesome-4.7.0-min.css" rel="stylesheet"/>
<link href="css/style.css?v1.0" rel="stylesheet" type="text/css"/>
<link href="css/mainnav.css" rel="stylesheet" type="text/css"/>
<script async="" src="//www.google-analytics.com/analytics.js"></script>
<script src="js/vendor/modernizr-2.8.3.min.js"></script>
<!-- [if lt IE 9]>
	<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif] -->
<link href="humans.txt" rel="author" type="text/plain"/>
<!-- end inc_head code -->
<script src="js/vendor/jquery-1.11.3.min.js" type="text/javascript"></script>
<link href="css/slideshow.css" rel="stylesheet" type="text/css"/>
<link href="weather/weather.css" rel="stylesheet" type="text/css"/>
<link href="css/mooSelecta.css" rel="stylesheet"/>
<meta content="0b02bfb1c618139ab6d8f92df8b9daaf" name="SiteCheck"/>
<title>Welcome to APL Federal Credit Union	</title>
<meta content="APL Federal Credit Union, Laurel, MD" name="description"/>
<meta content="APL Federal Credit Union, Laurel, MD" name="keywords"/>
</head>
<body class="home enableweather enhancenotice">
<div class="notice" id="notice">
<div style="position: relative">
<div class="noticeHtml">
<p style="text-align: center;"><strong>JANUARY COVID-19 UPDATE:</strong><br/> <a href="Coronavirus">Restricted Lobby Access</a> - <a href="ways-to-access-your-account">Please Use Remote Services When Possible</a> - <a href="Coronavirus">More Information</a></p>
<p style="text-align: center;"> </p>
</div>
</div>
</div>
<h1 class="visuallyhidden">APL Federal Credit Union</h1>
<a id="top"></a>
<header id="header">
<section class="inner-content">
<div id="logo">
<a href="./">
<img alt="" src="images/logo-apl.svg"/>
</a>
</div><!--/logo-->
<nav class="secondary">
<ul>
<li><a href="Why-APL-FCU"><i class="fa-question-circle"></i>Why APL FCU?</a></li>
<li><a href="Apply-Now"><i class="fa-edit"></i>Apply Now</a></li>
<li><a href="Rates"><i class="fa-percent"></i>Rates</a></li>
<li><a href="Contact-Us"><i class="fa-phone"></i>Contact Us</a></li>
</ul>
</nav>
<!--
                <div id="search" class="searchbox search-icon">
	            <div class="searchfield"> 
		            <form action="searchResults.aspx" id="cse-search-box" style="padding: 0px"> 
		                <input type="hidden" name="cx" value="000588689579482676488:3o29qexxaok" />
		                <input type="hidden" name="cof" value="FORID:11" /> 
		                <input type="hidden" name="ie" value="UTF-8" /> 
                        <label for="searchquery" class="visuallyhidden">Search</label>
		                <input type="text" name="q" class="searchquery" id="searchquery" placeholder="Search"/> 
		                <input type="submit" class="btn_go" value="Search" />
		            </form>
	            </div>
                   
            </div>
                 -->
</section>
<nav class="nav" id="nav">
<div class="inner-content">
<div id="nav-container">
<div id="logomark">
<a href="./"><img alt="" src="images/logo-apl.svg"/></a>
</div><!--/logomark-->
<div id="login-button">
<h3>Login</h3>
</div>
<div class="menubutton" id="menuopen">
<i class="fa fa-bars"></i>
<h2>MENU</h2>
</div>
<div class="clear"></div>
</div>
<div id="mainnav">
<ul class="panelnav">
<li class="menuitem1">
<a class="category" href="javascript:void(0)">Bank</a>
<div class="navpanel item1panel">
<div class="panel-content">
<div class="panelxy">
<div>
<h2>Accounts</h2>
<ul>
<li><a href="Checking">Checking</a></li>
<li><a href="Savings">Savings</a></li>
<li><a href="Money-Market">Money Market</a></li>
<li><a href="CDs-and-IRAs">CDs &amp; IRAs</a></li>
</ul>
</div>
<div>
<h2>Access</h2>
<ul>
<li><a href="eBranch-and-Mobile-Banking">eBranch and Mobile Banking</a></li>
<li><a href="Mobile-Deposit">Mobile Deposit</a></li>
<li><a href="Bill-Pay">Bill Pay</a></li>
<li><a href="Mobile-Wallets">Apple Pay™ and Google Pay™</a></li>
<li><a href="Remote-Transfer">Remote Transfer</a></li>
<li><a href="Telephone-Banking">Telephone Banking</a></li>
<li><a href="ATM-Locator">ATM Locator</a></li>
</ul>
</div>
<div>
<h2>Account Services</h2>
<ul>
<li><a href="Visa-Debit-Cards">Visa Debit Cards</a></li>
<li><a href="Direct-Deposit">Direct Deposit </a></li>
<li><a href="Overdraft-Protection">Overdraft Protection</a></li>
<li><a href="Stop-Payment">Stop Payment</a></li>
<li><a href="Fee-Schedule">Fee Schedule</a></li>
<li><a href="Visa-Purchase-Alerts">Visa Purchase Alerts</a></li>
</ul>
</div>
<div class="clear"></div>
</div>
</div>
</div>
</li><!--/ li menuitem1 -->
<li class="menuitem2">
<a class="category" href="javascript:void(0)">Borrow</a>
<div class="navpanel item2panel">
<div class="panel-content">
<div class="panelxy">
<div>
<h2>Vehicle Loans</h2>
<ul>
<li><a href="Vehicle-Loans">Auto Loans</a></li>
<li><a href="Vehicle-Loans?expand=Classic-Cars">Classic Cars</a></li>
<li><a href="Vehicle-Loans?expand=Motorcycles">Motorcycles</a></li>
<li><a href="Car-Buying-Resources">Car Buying Resources</a></li>
</ul>
</div>
<div>
<h2>Real Estate</h2>
<ul>
<li><a href="Mortgages">Mortgages</a></li>
<li><a href="Home-Equity">Home Equity</a></li>
<li><a href="Specialty-Mortgages">Specialty Mortgages</a></li>
<li><a href="Home-Search-and-Rebate">Home Search and Rebate</a></li>
<li><a href="Home-Buying-Resources">Home Buying Resources</a></li>
</ul>
</div>
<div>
<h2>Credit Cards &amp; Personal Loans</h2>
<ul>
<li><a href="Card-Options">Card Options</a></li>
<li><a href="Personal-Loans">Personal Loans</a></li>
<li><a href="Balance-Transfers">Balance Transfers</a></li>
<li><a href="Visa-Purchase-Alerts">Visa Purchase Alerts</a></li>
<li><a href="Travel-Alerts">Travel Alerts</a></li>
<li><a href="Lost-or-Stolen-Cards">Lost or Stolen Cards</a></li>
</ul>
</div>
<div class="clear"></div>
</div>
</div>
</div>
</li><!--/ li menuitem2 -->
<li class="menuitem3">
<a class="category" href="javascript:void(0)">Belong</a>
<div class="navpanel item3panel">
<div class="panel-content">
<div class="panelxy">
<div>
<h2>About Us</h2>
<ul>
<li><a href="Join-APL-FCU">Join APL FCU</a></li>
<li><a href="Member-Referral">Member Referral</a></li>
<li><a href="Our-History">Our History</a></li>
<li><a href="About-Credit-Unions">About Credit Unions</a></li>
<li><a href="Switch-to-APL">Switch to APL FCU</a></li>
</ul>
</div>
<div>
<h2>Member Discounts</h2>
<ul>
<li><a href="Member-Discounts#Airport-Parking">Airport Parking</a></li>
<li><a href="Member-Discounts#Bowie-Baysox-Tickets">Bowie Baysox Tickets</a></li>
<li><a href="Member-Discounts#Car-Rental">Car Rental</a></li>
<li><a href="Financial-Planning">Financial Planning</a></li>
<li><a href="Member-Discounts#Realtor-Rebate">Realtor Rebate</a></li>
<li><a href="Member-Discounts#TurboTax">TurboTax</a></li>
</ul>
</div>
<div>
<h2>Contact Us</h2>
<ul>
<li><a href="Contact-Us">Call or Email</a></li>
<li><a href="Locations-and-Hours">Locations and Hours</a></li>
<li><a href="Holiday-Schedule">Holiday Schedule</a></li>
<li><a href="Employment">Employment</a></li>
</ul>
</div>
<div class="clear"></div>
</div>
</div>
</div>
</li><!--/ li menuitem3 -->
<li class="menuitem4">
<a class="category" href="javascript:void(0)">Resources</a>
<div class="navpanel item4panel">
<div class="panel-content">
<div class="panelxy">
<div>
<h2>Services</h2>
<ul>
<li><a href="Address-Change">Address Change</a></li>
<li><a href="Safe-Deposit-Boxes">Safe Deposit Boxes</a></li>
<li><a href="Gift-Cards">Gift Cards</a></li>
<li><a href="Foreign-Currency">Foreign Currency</a></li>
<li><a href="Money-Orders">Money Orders</a></li>
<li><a href="Wire-Transfers">Wire Transfers</a></li>
<li><a href="Online-Applications">Online Applications</a></li>
<li><a href="Additional-Services">Additional Services</a></li>
</ul>
</div>
<div>
<h2>Learn</h2>
<ul>
<li><a href="Financial-Planning">Financial Planning</a></li>
<li><a href="Car-Buying-Resources">Car Buying Resources</a></li>
<li><a href="Home-Buying-Resources">Home Buying Services</a></li>
<li><a href="Credit-and-Debit-Card-Tips">Credit and Debit Card Tips</a></li>
<li><a href="Free-Credit-Reports">Free Credit Reports</a></li>
<li><a href="Loan-Consolidation">Loan Consolidation</a></li>
<li><a href="Calculators">Calculators</a></li>
</ul>
</div>
<div>
<h2>News</h2>
<ul>
<li><a href="Annual-Reports">Annual Reports</a></li>
<li><a href="Monthly-News">Monthly News</a></li>
<li><a href="Quarterly-Newsletter">Quarterly Newsletter</a></li>
<li><a href="Fraud-Alerts">Fraud Alerts</a></li>
<li><a href="Credit-Union-Events">Credit Union Events</a></li>
</ul>
</div>
<div class="clear"></div>
</div>
</div>
</div>
</li><!--/ li menuitem4 -->
</ul>
</div>
</div><!--/nav inner content-->
</nav><!--/nav-->
</header><!--/header-->
<div class="overlapped" id="page">
<div class="inner-content">
<section id="hero-main">
<div class="personal" id="login">
<div id="login-inner-content">
<div id="obContainer">
<h2>eBranch Login</h2>
<div id="accessfieldxy">
<form action="https://aplfcu.onlinebank.com/login.aspx" id="HBLOGINFORM" method="post">
<input name="FromWebFederal" type="hidden" value="YES"/>
<label><span class="visuallyhidden">Enter Member Number or User ID</span><input class="hbuser placeholder obaccess" id="LoginName" maxlength="20" name="LoginName" placeholder="Member Number / User ID" type="password" value=""/></label>
<label><span class="visuallyhidden">Enter Password</span><input class="hbuser placeholder obpass" id="Password" maxlength="16" name="Password" placeholder="Password" type="password" value=""/></label>
<input class="btn_login" id="Submit" name="Submit" type="submit" value="GO"/>
</form>
</div>
</div><!--/obContainer-->
<div id="oblinks">
<div class="oblinks rob" id="oblinks1">
<ul>
<li><a href="https://aplfcu.onlinebank.com/ForgotUserID.aspx">Forgot User ID?</a>          <a href="https://aplfcu.onlinebank.com/ForgotPassword.aspx">Forgot Password?</a></li>
<li><a href="eBranch-registration">Register</a></li>
</ul>
<div class="clear"></div>
</div><!--/oblinks1-->
</div><!--/oblinks-->
</div><!--/login-inner-content-->
</div><!--/login-->
<div id="slideshow-container">
<table class="Table-Slide">
<tbody>
<tr>
<td valign="top">
<p><img alt="CCLowRates" border="0" src="ContentImageHandler.ashx?ImageId=156179"/></p>
<h3>Credit Cards</h3>
<h2>Low Rates</h2>
<p>Transfer Balances Now!</p>
<p><a class="Button1" href="Rates?expand=Credit-Cards">More Info</a></p>
</td>
</tr>
</tbody>
</table>
<table class="Table-Slide">
<tbody>
<tr>
<td valign="top">
<p><img alt="Auto Banner" border="0" src="ContentImageHandler.ashx?ImageId=115874"/></p>
<h3>Auto Loans</h3>
<h2>New, Used <br/> or Refinance.</h2>
<p>Just 1.99%</p>
<p><a class="Button1" href="Vehicle-Loans?expand=Auto-Loans">Apply Today</a></p>
</td>
</tr>
</tbody>
</table>
<table class="Table-Slide">
<tbody>
<tr>
<td valign="top">
<p><img alt="MobileWallets" border="0" src="ContentImageHandler.ashx?ImageId=143349"/></p>
<h3>Safe Payments with</h3>
<h2>Apple Pay and<br/> Google Pay</h2>
<p>With APL FCU Debit and Credit Cards.</p>
<p><a class="Button1" href="Mobile-Wallets">More Info</a></p>
</td>
</tr>
</tbody>
</table>
<table class="Table-Slide">
<tbody>
<tr>
<td valign="top">
<p><img alt="Best of Howard" border="0" src="ContentImageHandler.ashx?ImageId=123235"/></p>
<h3>Best of Howard</h3>
<h2>We're #1 - AGAIN -<br/> Thanks to You!</h2>
<p>Bank/Credit Union Category</p>
<p><a class="Button1" href="speedbump.aspx?link=https://www.baltimoresun.com/news/maryland/howard/howard-magazine/bs-mg-ho-best-of-howard-2018-winners-20181109-story.html" target="_blank">More Info</a></p>
</td>
</tr>
</tbody>
</table>
</div>
</section>
<!--/hero-main-->
<div id="stripe">
<div class="inner-content">
<div class="info" id="weather">
<div id="weatherXY1">
<form action="./" id="Form1" method="post">
<input id="__VIEWSTATE" name="__VIEWSTATE" type="hidden" value="/wEPDwULLTExNDU4ODcwOTYPZBYCAgUPZBYCAgEPZBaIAWYPFgIeBFRleHQFGzxkaXYgY2xhc3M9IndlYXRoZXIgQ2xlYXIiPmQCAg8WAh8AZWQCBA8WAh8ABYsBPGRpdiBjbGFzcz0ibG9jYXRpb25UaXRsZSBMYXVyZWxNYXJ5bGFuZCI+PHNwYW4gY2xhc3M9IndlYXRoZXJMYWJlbCI+TG9jYXRpb246IDwvc3Bhbj48c3BhbiBpZD0ibG9jYXRpb25EYXRhIj5MYXVyZWwsIE1hcnlsYW5kPC9zcGFuPjwvZGl2PmQCBg8WAh8ABSg8ZGl2IGNsYXNzPSJjb25kaXRpb25JbWFnZSBDbGVhciI+PC9kaXY+ZAIIDxYCHwAFlQE8ZGl2IGNsYXNzPSJ0ZW1wRmFlbmhlaXQiPjxzcGFuIGNsYXNzPSJ3ZWF0aGVyTGFiZWwiPlRlbXBlcmF0dXJlOiA8L3NwYW4+PHNwYW4gaWQ9ImN1cnJlbnRUZW1wRkRhdGEiPjIzJmRlZzs8c3BhbiBjbGFzcz0idW5pdCI+IEY8L3NwYW4+PC9zcGFuPjwvZGl2PmQCCg8WAh8ABZQBPGRpdiBjbGFzcz0idGVtcENlbGNpdXMiPjxzcGFuIGNsYXNzPSJ3ZWF0aGVyTGFiZWwiPlRlbXBlcmF0dXJlOiA8L3NwYW4+PHNwYW4gaWQ9ImN1cnJlbnRUZW1wQ0RhdGEiPi01JmRlZzs8c3BhbiBjbGFzcz0idW5pdCI+IEM8L3NwYW4+PC9zcGFuPjwvZGl2PmQCDA8WAh8ABXY8ZGl2IGNsYXNzPSJjb25kaXRpb24iPjxzcGFuIGNsYXNzPSJ3ZWF0aGVyTGFiZWwiPkNvbmRpdGlvbjogPC9zcGFuPjxzcGFuIGlkPSJjdXJyZW50Q29uZGl0aW9uRGF0YSI+Q2xlYXI8L3NwYW4+PC9kaXY+ZAIODxYCHwAFbDxkaXYgY2xhc3M9IndpbmQiPjxzcGFuIGNsYXNzPSJ3ZWF0aGVyTGFiZWwiPldpbmQ6IDwvc3Bhbj48c3BhbiBpZD0iY3VycmVudFdpbmREYXRhIj5OIGF0IDAgbXBoPC9zcGFuPjwvZGl2PmQCEA8WAh8ABYEBPGRpdiBjbGFzcz0icHJlY2lwaXRhdGlvbiI+PHNwYW4gY2xhc3M9IndlYXRoZXJMYWJlbCI+UHJlY2lwaXRhdGlvbjogPC9zcGFuPjxzcGFuIGlkPSJjdXJyZW50UHJlY2lwaXRhdGlvbkRhdGEiPjAgbW08L3NwYW4+PC9kaXY+ZAISDxYCHwAFcTxkaXYgY2xhc3M9Imh1bWlkaXR5Ij48c3BhbiBjbGFzcz0id2VhdGhlckxhYmVsIj5IdW1pZGl0eTogPC9zcGFuPjxzcGFuIGlkPSJjdXJyZW50SHVtaWRpdHlEYXRhIj45MyU8L3NwYW4+PC9kaXY+ZAIUDxYCHwAFeTxkaXYgY2xhc3M9InZpc2liaWxpdHkiPjxzcGFuIGNsYXNzPSJ3ZWF0aGVyTGFiZWwiPlZpc2liaWxpdHk6IDwvc3Bhbj48c3BhbiBpZD0iY3VycmVudFZpc2liaWxpdHlEYXRhIj4xNiBrbTwvc3Bhbj48L2Rpdj5kAhYPFgIfAAWOATxkaXYgY2xhc3M9ImF0bW9zcGhlcmljUHJlc3N1cmUiPjxzcGFuIGNsYXNzPSJ3ZWF0aGVyTGFiZWwiPkF0bW9zcGhlcmljIFByZXNzdXJlOiA8L3NwYW4+PHNwYW4gaWQ9ImN1cnJlbnRQcmVzc3VyZURhdGEiPjEwMjAgbW1IZzwvc3Bhbj48L2Rpdj5kAhgPFgIfAAVyPGRpdiBjbGFzcz0iY2xvdWRDb3ZlciI+PHNwYW4gY2xhc3M9IndlYXRoZXJMYWJlbCI+Q2xvdWQgQ292ZXI6IDwvc3Bhbj48c3BhbiBpZD0iY3VycmVudENsb3VkRGF0YSI+MCU8L3NwYW4+PC9kaXY+ZAIaDxYCHwAFEzxkaXYgaWQ9ImZvcmVjYXN0Ij5kAhwPFgIfAAVAPGRpdiBpZD0iZm9yZWNhc3REYXlXZWRuZXNkYXkiIGNsYXNzPSJmb3JlY2FzdERheSBQYXJ0bHlDbG91ZHkiPmQCHg8WAh8ABS88ZGl2IGNsYXNzPSJjb25kaXRpb25JbWFnZSBQYXJ0bHlDbG91ZHkiPjwvZGl2PmQCIA8WAh8ABSY8ZGl2IGNsYXNzPSJkYXlPZldlZWsiPldlZG5lc2RheTwvZGl2PmQCIg8WAh8ABSw8ZGl2IGNsYXNzPSJ0ZW1wRmFyZW5oZWl0SGlnaCI+NTEmZGVnOzwvZGl2PmQCJA8WAh8ABSo8ZGl2IGNsYXNzPSJ0ZW1wQ2VsY2l1c0hpZ2giPjExJmRlZzs8L2Rpdj5kAiYPFgIfAAUrPGRpdiBjbGFzcz0idGVtcEZhcmVuaGVpdExvdyI+MzUmZGVnOzwvZGl2PmQCKA8WAh8ABSg8ZGl2IGNsYXNzPSJ0ZW1wQ2VsY2l1c0xvdyI+MiZkZWc7PC9kaXY+ZAIqDxYCHwAFKjxkaXYgY2xhc3M9ImNvbmRpdGlvbiI+UGFydGx5IGNsb3VkeTwvZGl2PmQCLA8WAh8ABXE8ZGl2IGNsYXNzPSJ3aW5kIj48c3BhbiBjbGFzcz0id2VhdGhlckxhYmVsIj5XaW5kOiA8L3NwYW4+PHNwYW4gaWQ9ImZvcmVjYXN0T25lV2luZERhdGEiPlNXIGF0IDYgbXBoPC9zcGFuPjwvZGl2PmQCLg8WAh8ABQY8L2Rpdj5kAjAPFgIfAAU/PGRpdiBpZD0iZm9yZWNhc3REYXlUaHVyc2RheSIgY2xhc3M9ImZvcmVjYXN0RGF5IFBhcnRseUNsb3VkeSI+ZAIyDxYCHwAFLzxkaXYgY2xhc3M9ImNvbmRpdGlvbkltYWdlIFBhcnRseUNsb3VkeSI+PC9kaXY+ZAI0DxYCHwAFJTxkaXYgY2xhc3M9ImRheU9mV2VlayI+VGh1cnNkYXk8L2Rpdj5kAjYPFgIfAAUsPGRpdiBjbGFzcz0idGVtcEZhcmVuaGVpdEhpZ2giPjUyJmRlZzs8L2Rpdj5kAjgPFgIfAAUqPGRpdiBjbGFzcz0idGVtcENlbGNpdXNIaWdoIj4xMSZkZWc7PC9kaXY+ZAI6DxYCHwAFKzxkaXYgY2xhc3M9InRlbXBGYXJlbmhlaXRMb3ciPjM3JmRlZzs8L2Rpdj5kAjwPFgIfAAUoPGRpdiBjbGFzcz0idGVtcENlbGNpdXNMb3ciPjMmZGVnOzwvZGl2PmQCPg8WAh8ABSo8ZGl2IGNsYXNzPSJjb25kaXRpb24iPlBhcnRseSBjbG91ZHk8L2Rpdj5kAkAPFgIfAAVyPGRpdiBjbGFzcz0id2luZCI+PHNwYW4gY2xhc3M9IndlYXRoZXJMYWJlbCI+V2luZDogPC9zcGFuPjxzcGFuIGlkPSJmb3JlY2FzdFR3b1dpbmREYXRhIj5XU1cgYXQgNyBtcGg8L3NwYW4+PC9kaXY+ZAJCDxYCHwAFBjwvZGl2PmQCRA8WAh8ABTo8ZGl2IGlkPSJmb3JlY2FzdERheUZyaWRheSIgY2xhc3M9ImZvcmVjYXN0RGF5IExpZ2h0UmFpbiI+ZAJGDxYCHwAFLDxkaXYgY2xhc3M9ImNvbmRpdGlvbkltYWdlIExpZ2h0UmFpbiI+PC9kaXY+ZAJIDxYCHwAFIzxkaXYgY2xhc3M9ImRheU9mV2VlayI+RnJpZGF5PC9kaXY+ZAJKDxYCHwAFLDxkaXYgY2xhc3M9InRlbXBGYXJlbmhlaXRIaWdoIj40OSZkZWc7PC9kaXY+ZAJMDxYCHwAFKTxkaXYgY2xhc3M9InRlbXBDZWxjaXVzSGlnaCI+OSZkZWc7PC9kaXY+ZAJODxYCHwAFKzxkaXYgY2xhc3M9InRlbXBGYXJlbmhlaXRMb3ciPjM2JmRlZzs8L2Rpdj5kAlAPFgIfAAUoPGRpdiBjbGFzcz0idGVtcENlbGNpdXNMb3ciPjImZGVnOzwvZGl2PmQCUg8WAh8ABTE8ZGl2IGNsYXNzPSJjb25kaXRpb24iPlBhdGNoeSByYWluIHBvc3NpYmxlPC9kaXY+ZAJUDxYCHwAFczxkaXYgY2xhc3M9IndpbmQiPjxzcGFuIGNsYXNzPSJ3ZWF0aGVyTGFiZWwiPldpbmQ6IDwvc3Bhbj48c3BhbiBpZD0iZm9yZWNhc3RUaHJlZVdpbmREYXRhIj5TIGF0IDEwIG1waDwvc3Bhbj48L2Rpdj5kAlYPFgIfAAUGPC9kaXY+ZAJYDxYCHwAFPDxkaXYgaWQ9ImZvcmVjYXN0RGF5U2F0dXJkYXkiIGNsYXNzPSJmb3JlY2FzdERheSBMaWdodFJhaW4iPmQCWg8WAh8ABSw8ZGl2IGNsYXNzPSJjb25kaXRpb25JbWFnZSBMaWdodFJhaW4iPjwvZGl2PmQCXA8WAh8ABSU8ZGl2IGNsYXNzPSJkYXlPZldlZWsiPlNhdHVyZGF5PC9kaXY+ZAJeDxYCHwAFLDxkaXYgY2xhc3M9InRlbXBGYXJlbmhlaXRIaWdoIj40NCZkZWc7PC9kaXY+ZAJgDxYCHwAFKTxkaXYgY2xhc3M9InRlbXBDZWxjaXVzSGlnaCI+NyZkZWc7PC9kaXY+ZAJiDxYCHwAFKzxkaXYgY2xhc3M9InRlbXBGYXJlbmhlaXRMb3ciPjM2JmRlZzs8L2Rpdj5kAmQPFgIfAAUoPGRpdiBjbGFzcz0idGVtcENlbGNpdXNMb3ciPjImZGVnOzwvZGl2PmQCZg8WAh8ABTE8ZGl2IGNsYXNzPSJjb25kaXRpb24iPlBhdGNoeSByYWluIHBvc3NpYmxlPC9kaXY+ZAJoDxYCHwAFdDxkaXYgY2xhc3M9IndpbmQiPjxzcGFuIGNsYXNzPSJ3ZWF0aGVyTGFiZWwiPldpbmQ6IDwvc3Bhbj48c3BhbiBpZD0iZm9yZWNhc3RGb3VyV2luZERhdGEiPldOVyBhdCAxMSBtcGg8L3NwYW4+PC9kaXY+ZAJqDxYCHwAFBjwvZGl2PmQCbA8WAh8ABT08ZGl2IGlkPSJmb3JlY2FzdERheVN1bmRheSIgY2xhc3M9ImZvcmVjYXN0RGF5IFBhcnRseUNsb3VkeSI+ZAJuDxYCHwAFLzxkaXYgY2xhc3M9ImNvbmRpdGlvbkltYWdlIFBhcnRseUNsb3VkeSI+PC9kaXY+ZAJwDxYCHwAFIzxkaXYgY2xhc3M9ImRheU9mV2VlayI+U3VuZGF5PC9kaXY+ZAJyDxYCHwAFLDxkaXYgY2xhc3M9InRlbXBGYXJlbmhlaXRIaWdoIj40MCZkZWc7PC9kaXY+ZAJ0DxYCHwAFKTxkaXYgY2xhc3M9InRlbXBDZWxjaXVzSGlnaCI+NSZkZWc7PC9kaXY+ZAJ2DxYCHwAFKzxkaXYgY2xhc3M9InRlbXBGYXJlbmhlaXRMb3ciPjMxJmRlZzs8L2Rpdj5kAngPFgIfAAUoPGRpdiBjbGFzcz0idGVtcENlbGNpdXNMb3ciPjAmZGVnOzwvZGl2PmQCeg8WAh8ABSo8ZGl2IGNsYXNzPSJjb25kaXRpb24iPlBhcnRseSBjbG91ZHk8L2Rpdj5kAnwPFgIfAAV0PGRpdiBjbGFzcz0id2luZCI+PHNwYW4gY2xhc3M9IndlYXRoZXJMYWJlbCI+V2luZDogPC9zcGFuPjxzcGFuIGlkPSJmb3JlY2FzdEZpdmVXaW5kRGF0YSI+V05XIGF0IDEzIG1waDwvc3Bhbj48L2Rpdj5kAn4PFgIfAAUGPC9kaXY+ZAKAAQ8WAh8ABQY8L2Rpdj5kAoIBDxYCHwAFTDxkaXYgaWQ9ImZvcmVjYXN0TGlua1hZIj48YSBocmVmPSIjIiBjbGFzcz0iZm9yZWNhc3RMaW5rIj5Gb3JlY2FzdDwvYT48L2Rpdj5kAoQBDxYCHwAFvAE8ZGl2IGlkPSJwb3dlcmVkQnkiIGNsYXNzPSJwb3dlcmVkQnkiPlBvd2VyZWQgYnkgPGEgaHJlZj0ic3BlZWRidW1wLmFzcHg/bGluaz1odHRwOi8vd3d3Lndvcmxkd2VhdGhlcm9ubGluZS5jb20vIiB0aXRsZT0iRnJlZSBsb2NhbCB3ZWF0aGVyIGNvbnRlbnQgcHJvdmlkZXIiPldvcmxkIFdlYXRoZXIgT25saW5lPC9hPjwvZGl2PmQChgEPD2QWAh4Fc3R5bGUFDWRpc3BsYXk6bm9uZTtkZPPN8+PFIKEEu/5Qa+dlR8rktl4b"/>
<input id="__VIEWSTATEGENERATOR" name="__VIEWSTATEGENERATOR" type="hidden" value="CA0B0334"/>
<input id="__EVENTVALIDATION" name="__EVENTVALIDATION" type="hidden" value="/wEdAAIYaqhdJOkv21PQwtkqZXCLHZHzngjDVTGM+LvPcclLiYldJ66J/5KnDqeUW/MNADhOS/Ud"/>
<div class="weather Clear">
<div class="locationTitle LaurelMaryland"><span class="weatherLabel">Location: </span><span id="locationData">Laurel, Maryland</span></div>
<div class="conditionImage Clear"></div>
<div class="tempFaenheit"><span class="weatherLabel">Temperature: </span><span id="currentTempFData">23°<span class="unit"> F</span></span></div>
<div class="tempCelcius"><span class="weatherLabel">Temperature: </span><span id="currentTempCData">-5°<span class="unit"> C</span></span></div>
<div class="condition"><span class="weatherLabel">Condition: </span><span id="currentConditionData">Clear</span></div>
<div class="wind"><span class="weatherLabel">Wind: </span><span id="currentWindData">N at 0 mph</span></div>
<div class="precipitation"><span class="weatherLabel">Precipitation: </span><span id="currentPrecipitationData">0 mm</span></div>
<div class="humidity"><span class="weatherLabel">Humidity: </span><span id="currentHumidityData">93%</span></div>
<div class="visibility"><span class="weatherLabel">Visibility: </span><span id="currentVisibilityData">16 km</span></div>
<div class="atmosphericPressure"><span class="weatherLabel">Atmospheric Pressure: </span><span id="currentPressureData">1020 mmHg</span></div>
<div class="cloudCover"><span class="weatherLabel">Cloud Cover: </span><span id="currentCloudData">0%</span></div>
<div id="forecast">
<div class="forecastDay PartlyCloudy" id="forecastDayWednesday">
<div class="conditionImage PartlyCloudy"></div>
<div class="dayOfWeek">Wednesday</div>
<div class="tempFarenheitHigh">51°</div>
<div class="tempCelciusHigh">11°</div>
<div class="tempFarenheitLow">35°</div>
<div class="tempCelciusLow">2°</div>
<div class="condition">Partly cloudy</div>
<div class="wind"><span class="weatherLabel">Wind: </span><span id="forecastOneWindData">SW at 6 mph</span></div>
</div>
<div class="forecastDay PartlyCloudy" id="forecastDayThursday">
<div class="conditionImage PartlyCloudy"></div>
<div class="dayOfWeek">Thursday</div>
<div class="tempFarenheitHigh">52°</div>
<div class="tempCelciusHigh">11°</div>
<div class="tempFarenheitLow">37°</div>
<div class="tempCelciusLow">3°</div>
<div class="condition">Partly cloudy</div>
<div class="wind"><span class="weatherLabel">Wind: </span><span id="forecastTwoWindData">WSW at 7 mph</span></div>
</div>
<div class="forecastDay LightRain" id="forecastDayFriday">
<div class="conditionImage LightRain"></div>
<div class="dayOfWeek">Friday</div>
<div class="tempFarenheitHigh">49°</div>
<div class="tempCelciusHigh">9°</div>
<div class="tempFarenheitLow">36°</div>
<div class="tempCelciusLow">2°</div>
<div class="condition">Patchy rain possible</div>
<div class="wind"><span class="weatherLabel">Wind: </span><span id="forecastThreeWindData">S at 10 mph</span></div>
</div>
<div class="forecastDay LightRain" id="forecastDaySaturday">
<div class="conditionImage LightRain"></div>
<div class="dayOfWeek">Saturday</div>
<div class="tempFarenheitHigh">44°</div>
<div class="tempCelciusHigh">7°</div>
<div class="tempFarenheitLow">36°</div>
<div class="tempCelciusLow">2°</div>
<div class="condition">Patchy rain possible</div>
<div class="wind"><span class="weatherLabel">Wind: </span><span id="forecastFourWindData">WNW at 11 mph</span></div>
</div>
<div class="forecastDay PartlyCloudy" id="forecastDaySunday">
<div class="conditionImage PartlyCloudy"></div>
<div class="dayOfWeek">Sunday</div>
<div class="tempFarenheitHigh">40°</div>
<div class="tempCelciusHigh">5°</div>
<div class="tempFarenheitLow">31°</div>
<div class="tempCelciusLow">0°</div>
<div class="condition">Partly cloudy</div>
<div class="wind"><span class="weatherLabel">Wind: </span><span id="forecastFiveWindData">WNW at 13 mph</span></div>
</div>
</div>
<div id="forecastLinkXY"><a class="forecastLink" href="#">Forecast</a></div>
<div class="poweredBy" id="poweredBy">Powered by <a href="speedbump.aspx?link=http://www.worldweatheronline.com/" title="Free local weather content provider">World Weather Online</a></div>
<div class="weatherLocation">
<form id="weatherLocation1" name="weatherLocation1">
<label for="ctl08_weatherZipText" id="ctl08_weatherZipLabel" style="display:none;">Weather Location</label>
<input id="ctl08_weatherZipText" name="ctl08$weatherZipText" placeholder="ZIP" type="text"/>
<input id="weatherZipSubmit" name="weatherZipSubmit" type="submit" value="Go"/>
</form>
</div>
</div>
</form>
</div>
<a class="button-weather" href="javascript:" id="changeloc">Change Location</a>
</div>
<!--/weather-->
<div class="info">
<div class="info-inner-content">
<i class="quicklink-dollar-sign"></i>
<a href="Fee-Schedule">Fee Schedule</a>
</div>
<!--/info-inner-content-->
</div>
<!--/info-->
<div class="info lost" id="lost-card">
<div class="info-inner-content">
<i class="quicklink-cards"></i>
<a href="Lost-or-Stolen-Cards">Report a Lost or<br/>
                                    Stolen Card</a>
</div>
<!--/info-inner-content-->
</div>
<!--/info-->
<div class="clear"></div>
</div>
</div>
<div class="feature-promo" id="promo1">
<table class="Promo-Home">
<tbody>
<tr>
<td>
<table>
<tbody>
<tr>
<td style="width: 40%;">
<p><img alt="homeCCPromo" border="0" src="ContentImageHandler.ashx?ImageId=156181" style="display: block; margin-left: auto; margin-right: auto;"/></p>
</td>
<td>
<h2>Credit Card Balance Transfers</h2>
<p>Transfer your balance from your high interest rate credit card or loan and start saving today! The transfer is easy and you'll get our low rate, no fees card!</p>
<p><a class="Button1" href="Balance-Transfers">Learn More</a></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p> </p>
</div>
<!--/promo1-->
</div>
<!--/inner-content-->
</div>
<!--/page-->
<div class="feature-promo" id="promo2-wide">
<table class="Promo-Home">
<tbody>
<tr>
<td>
<table>
<tbody>
<tr>
<td style="width: 40%;">
<p><img alt="Allpoint Home Promo" border="0" height="256" src="ContentImageHandler.ashx?ImageId=114556" style="display: block; margin-left: auto; margin-right: auto;" width="281"/></p>
</td>
<td>
<h1><span>Around the corner or <br/> around the world</span></h1>
<p>We're part of the Allpoint ATM network. That means our cardholders have access to 55,000 Surcharge-Free ATMs at retailers like Target, CVS, and Walgreens.</p>
<p><a class="Button1" href="ATM-Locator">Learn More</a></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
<!--/promo2-wide-->
<section class="feature-promo" id="features">
<table class="Feature-Home">
<tbody>
<tr>
<td>
<table>
<tbody>
<tr>
<td>
<p><a class="icon-personal-2" href="Join-APL-FCU">Membership</a></p>
</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
<td>
<p><a href="eBranch-and-Mobile-Banking"><img alt="Account Access Icon" border="0" src="ContentImageHandler.ashx?ImageId=114575"/>Account Access</a></p>
</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
<td>
<p><a class="icon-security" href="Fraud-Alerts">Fraud Alerts</a></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</section>
<!--/features-->
<footer id="footer">
<section class="inner-content">
<div id="footer-logo">
<img alt="United Bank Logo" src="images/logo-apl.svg"/>
</div>
<nav class="secondary">
<ul>
<li>Routing Number #255077998</li>
<li><a href="tel:+14437785250">Contact Us at 443.778.5250</a></li>
<li><a href="Privacy-Policy">Privacy Policy</a></li>
<li><a href="Fee-Schedule">Fee Schedule</a></li>
</ul>
</nav>
<div id="footer-utility-logos">
<div><a href="speedbump.aspx?link=https://www.ncua.gov/Pages/default.aspx"><i class="icon-ncua"><span class="visuallyhidden">Member NCUA</span></i></a></div>
<div><i class="icon-ehl"><span class="visuallyhidden">Equal Housing Lender</span></i></div>
<div><a href="ATM-Locator"><img alt="Allpoint ATM Network Logo" src="images/allpoint-logo.png"/></a></div>
</div>
<div class="clear"></div>
<p class="copyright">
                    Copyright ©
                    <script language="JavaScript" type="text/javascript">
                        now = new Date;
                        theYear = now.getYear();
                        if (theYear < 1900) {
                            theYear = theYear + 1900;
                        };
                        document.write(theYear);
                    </script> APL Federal Credit Union.  All rights reserved.</p>
</section><!--/footer inner content-->
</footer><!--/footer-->
<a class="gototophide" href="#top" id="gototop">
<div id="gototopContainer">
<i class="fa fa-arrow-circle-up"></i>
<p>Back to Top</p>
</div></a>
<div id="tabtoexpander"></div>
<script src="js/vendor/jquery-ui-1.11.4.min.js" type="text/javascript"></script>
<script src="js/vendor/lightcase.js" type="text/javascript"></script>
<script src="js/vendor/mootools-core-1.4.5-full-compat.js" type="text/javascript"></script>
<script src="js/vendor/mootools-more-1.4.0.1.js" type="text/javascript"></script>
<script src="js/vendor/jquery.cookie.js" type="text/javascript"></script>
<script src="js/plugins.js" type="text/javascript"></script>
<script src="js/jquery-scripts.js?v2" type="text/javascript"></script>
<script type="text/javascript">
		var links = document.getElementsByTagName("a");
			for(var i=0; i<links.length; i++) {
				if(links[i].href.match(/speedbump/i) && links[i].href.match(/\?link\=/i) && !links[i].target){
					links[i].target = '_blank';
				}
		}
	</script>
<script>
	    <!-- Universal Analytics -->

	    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	    ga('create', 'UA-39907239-3', 'auto');
	    ga('require', 'displayfeatures');
	    ga('send', 'pageview');
	    <!-- End Universal Analytics -->
    // ADD PDF's   
    $(document).ready(function(){ 
       $("a[href$='pdf']").each(function(index) {     
                $(this).on("click", function() {
                    ga('send', 'event', 'PDF', 'Download', decodeURI($(this).attr('href')));
                    //_gaq.push(['_trackPageview', unescape($(this).attr('href')) ]);  
                });             
        });
    });

    </script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script src="js/slideshow.js" type="text/javascript"></script>
<script src="weather/weather.js" type="text/javascript"></script>
<script src="js/vendor/mooSelecta.js" type="text/javascript"></script>
<script type="text/javascript">
				window.addEvent('domready', function() {
					new mooSelecta({
						wrapperHeight: 194,
						positionRelativeSelector: 'select'
					});		
					
				});	
	</script>
</body>
</html>
