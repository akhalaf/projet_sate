<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://www.airportshuttles.net/images/favicon.ico" rel="shortcut icon"/>
<title>Gary, South Bend, Fort Wayne airport shuttle and bus service
</title>
<meta content="Northern Indiana Airport Shuttle and public transit to/from South Bend (SBN), Fort Wayne (FWA), Indianapolis (IND), and Chicago (ORD/MDW) airports.
" name="description"/>
<meta content="airport, airport shuttle, van, bus, airporter, bus line, scheduled bus, bus station, transit, mass transit, limo, limousine, taxi, cab, taxicab, public transportation, airport transportation, school transportation, private transportation, rideshare, share ride, door-to-door, school, college, university" name="keywords"/>
<link href="https://fonts.googleapis.com/css?family=Signika:400,300,700" rel="stylesheet" type="text/css"/>
<script src="/includes/js/jquery.min.js?23052017" type="text/javascript"></script>
<script src="/includes/js/bootstrap.js?23052017" type="text/javascript"></script>
<script src="/includes/js/jquery-ui.min.js?23052017" type="text/javascript"></script>
<script type="text/javascript">
			var _gaq = _gaq || [];
		   _gaq.push(['_setAccount', 'UA-8948551-8']);
		   _gaq.push(['_trackPageview']);
		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>
<link href="/includes/css/reset.css?23052017" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/css/icons.css?23052017" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/css/styles.css?23052017" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/css/font-awesome.min.css?23052017" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/css/bootstrap.min.css?23052017" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/css/style.css?23052017" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/css/responsive.css?23052017" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/css/jquery_ui_1_11_4.css?23052017" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/css/my-custom.css?23052017" media="all" rel="stylesheet" type="text/css"/>
<style>
.has_menu{
                
} 
.clear{
  visibility:visible;  
}
.form.modern-browser {
  margin-bottom: 15px;
}
            </style>
<style type="text/css">
</style>
</head>
<body>
<div class="container custom-container">
<div class="container">
<!-- Brand and toggle get grouped for better mobile display -->
<!--<div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only"></span> <i class="fa fa-bars"></i>
                </button>                
            </div>-->
<!-- Collect the nav links, forms, and other content for toggling -->
<div>
<ul class="nav navbar-nav navbar-right">
<li class=""><a href="https://www.airportshuttles.net/"> Home</a></li>
<li class=""><a href="https://www.airportshuttles.net/sitemap"> Sitemap</a></li>
</ul>
</div>
<!-- /.navbar-collapse -->
</div>
<header class="header">
<div class="custom-container container top-main-nav">
<nav class="navbar navbar-default">
<div class="custom-container container-fluid text-center">
<img class="wide_screen_logo" src="/images/airport_shuttle_network.png"/>
<img class="small_screen_logo" src="/images/logo_dark.png" style="width: 40%;"/>
<!--/.nav-collapse -->
</div>
<!--/.container-fluid -->
</nav>
</div>
</header>
<div class="main_page main_container">
<div class="shuttle-info-google-ad clearfix">
<div class="col-md-9 ggl-ad">
<div id="adcontainernew">
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Airport Shuttles -->
<ins class="adsbygoogle adslot_1" data-ad-client="ca-pub-4094493662013555" data-ad-format="auto" data-ad-slot="8567893267" data-full-width-responsive="true" style="display:block"></ins>
<script>

							(adsbygoogle = window.adsbygoogle || []).push({});
							</script>
</div>
</div>
<div class="col-md-3 col-sm-12 col-xs-12 arpt-img text-center">
<img class="ad-airport-img" src="https://www.airportshuttles.net/airport/Fort_Wayne.png"/>
</div>
</div>
<div> </div>
<!--<div class="col-md-12 clearfix" style="background-color: rgb(241, 242, 246); padding: 10px 5px;">	  
	  <span id="call_to_acion_text">Get an instant quote for airport transportation. Compare and save, book online! </span>
	  <a rel="nofollow" href="javascript:void(0);" class="button btn btn-danger" data-target="#modalGetQuotes" data-toggle="modal"> <span class="tab blink">Click ME!</span></a>
	  
 	</div>-->
<div class="shuttle-info clearfix">
<h1 class="h1_custom">Fort Wayne International Airport Shuttle listings	  </h1>
<p>
	  This section cover airport shuttle, bus service and public transit in Gary, South Bend, Fort Wayne and Northern Indiana. There are three main airports in the area: Ft. Wayne International Airport (FWA), Gary Chicago International Airport (GYY) and South B	  Shuttle listings are as follow.	   	</p></div>
<div class="neighbour-area clearfix">
<h5>See also neighboring areas of:</h5>
<p><a href="https://www.airportshuttles.net/indianapolis-in/">Indianapolis, IN</a></p>
<p><a href="https://www.airportshuttles.net/chicago-il/">Chicago, IL</a></p>
<p><a href="https://www.airportshuttles.net/grand-rapids-mi/">Kalamazoo - Battle Creek MI</a></p>
<p><a href="https://www.airportshuttles.net/akron-oh/">Toledo, OH</a></p>
</div>
<div class="transit-list clearfix">
<h4> Transit Listings:</h4>
<div>
<p style="font-size:14px; font-weight: 500;">Miller Transportation (Miller Trailways)</p>
<p>Offering intercity bus service directly from South Bend to Indianapolis and Kalamazoo Mi. The South Bend main route includes stops in Elkhart, LaPaz, Rochester, Peru, and Kokomo. Service also connects to other routes with various stops throughout Indiana.</p>
<p>111 Outer Loop, Louisville, KY 40214</p>
<p>800-544-2383, 502-368-5644</p>
<p class="end-point"><a href="http://www.hoosierride.com" rel="nofollow" target="_blank">www.hoosierride.com</a></p>
</div>
<div>
<p style="font-size:14px; font-weight: 500;">Transpo</p>
<p>Offers city bus service to destinations throughout the South Bend area</p>
<p>1401 S. Lafayette Blvd, South Bend, IN 46613</p>
<p>574-233-2131</p>
<p class="end-point"><a href="http://www.sbtranspo.com" rel="nofollow" target="_blank">www.sbtranspo.com</a></p>
</div>
</div>
<div class="shuttle-list clearfix">
<h4> Shuttle Listings:</h4>
<div>
<p style="font-size:14px; font-weight: 500;">Hoosier Shuttle</p>
<p>Airprot shuttle service between Fort Wayne and Indianapolis airports</p>
<p></p>
<p>260-469-8747, 877-392-2463</p>
<p class="end-point"><a href="http://www.hoosiershuttle.com" rel="nofollow" target="_blank">www.hoosiershuttle.com</a></p>
</div>
<div>
<p style="font-size:14px; font-weight: 500;">Northern Indiana Commuter Transportation District</p>
<p>Train service with stations in Gary/Chicago Airport and South Bend Airport</p>
<p>33 East U.S. Highway 12, Chesterton, IN 46304</p>
<p>219-926-5744</p>
<p class="end-point"><a href="http://www.nictd.com" rel="nofollow" target="_blank">www.nictd.com</a></p>
</div>
</div>
<div class="limo-list clearfix">
<h4> Limo Listings:</h4>
<p><strong>Enter your travel details in the form below to get up to eight offers.</strong>
<br/><small>Please note that only private transportation companies like limo and car service (not share ride or carpool) will provide you with a price quote.</small></p>
<div class="post-content"> <!-- confines heading font to this content -->
<p align="center" class="limo-form-head"><script src="https://www.airportservice.com/affiliate.js?id=2416&amp;size=0.75" type="text/javascript"></script></p>
</div>
</div>
<div class="airport-region clearfix">
<h5>Airports in the region:</h5>
 	Chicago Midway International Airport (MDW)<br/>Chicago O'Hare International Airport (ORD)<br/>Chicago Rockford International Airport (RFD)<br/>Ft. Wayne International Airport (FWA)<br/>Gary Chicago International Airport (GYY)<br/> Indianapolis International Airport (IND)<br/>Kalamazoo-Battle Creek International Airport (AZO)<br/>South Bend Regional Airport (SBN)<br/>Toledo Express Airport (TOL) 	</div>
<div class="town-city clearfix">
<h5>Cities and Towns in the area:</h5>
<ul class="col-md-3">
<li class="">Albion</li>
<li class="">Angola</li>
<li class="">Ashley</li>
<li class="">Auburn</li>
<li class="">Berne</li>
<li class="">Beverly Shores</li>
<li class="">Bluffton</li>
<li class="">Butler</li>
<li class="">Chesterton</li>
<li class="">Churubusco</li>
<li class="">Columbia City</li>
<li class="">Corunna</li>
<li class="">Cromwell</li>
<li class="">Crown Point</li>
<li class="">Culver</li>
<li class="">Decatur</li>
<li class="">Demotte</li>
<li class="">Elkhart</li>
<li class="">Fort Wayne</li>
<li class="">Fremont</li>
<li class="">Garrett</li>
</ul><ul class="col-md-3"> <li class="">Gary</li>
<li class="">Geneva</li>
<li class="">Goshen</li>
<li class="">Grass Creek</li>
<li class="">Griffith</li>
<li class="">Hamilton</li>
<li class="">Hammond</li>
<li class="">Highland</li>
<li class="">Hobart</li>
<li class="">Howe</li>
<li class="">Hudson</li>
<li class="">Huntington</li>
<li class="">Kendallville</li>
<li class="">Kimmell</li>
<li class="">Kingsford Heights</li>
<li class="">Knox</li>
<li class="">Lagrange</li>
<li class="">Lake Station</li>
<li class="">Laotto</li>
<li class="">Larwill</li>
<li class="">Leo</li>
</ul><ul class="col-md-3"> <li class="">Liberty Center</li>
<li class="">Linn Grove</li>
<li class="">Lowell</li>
<li class="">Merrillville</li>
<li class="">Michigan City</li>
<li class="">Monroe</li>
<li class="">Monroeville</li>
<li class="">Munster</li>
<li class="">New Carlisle</li>
<li class="">New Haven</li>
<li class="">North Manchester</li>
<li class="">Notre Dame</li>
<li class="">Orland</li>
<li class="">Ossian</li>
<li class="">Pierceton</li>
<li class="">Pleasant Lake</li>
<li class="">Pleasant Mills</li>
<li class="">Plymouth</li>
<li class="">Portage</li>
<li class="">Rensselaer</li>
<li class="">Rolling Prairie</li>
</ul><ul class="col-md-3"> <li class="">Saint Joe</li>
<li class="">Saint John</li>
<li class="">South Bend</li>
<li class="">South Whitley</li>
<li class="">Spencerville</li>
<li class="">Stroh</li>
<li class="">Sumava Resorts</li>
<li class="">Swayzee</li>
<li class="">Tefft</li>
<li class="">Union Mills</li>
<li class="">Valparaiso</li>
<li class="">Van Buren</li>
<li class="">Wanatah</li>
<li class="">Warren</li>
<li class="">Waterloo</li>
<li class="">Wawaka</li>
<li class="">Westville</li>
<li class="">Whiting</li>
<li class="">Winamac</li>
<li class="">Wolcottville</li>
<li class="">Wolflake</li>
</ul><ul class="col-md-3"> <li class="">Woodburn</li>
</ul>
</div>
</div>
<div class="div_gap" style="clear:both;height: 1px;"></div>
<!--============FOOTER======================-->
<footer class="footer">
<div class="container">
<div class="text-center">
<p>
<a>©</a>
<a>Airport Shuttles 
					2021				</a>
</p>
</div>
</div>
</footer>
<!-- Modal -->
<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalGetQuotes" role="dialog" style="z-index: 9999;">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button class="close" data-dismiss="modal" type="button">×</button>
<h4 class="modal-title" id="">Get an instant quote for airport transportation. Compare and save, book online!</h4>
</div>
<div class="modal-body text-center" id="">
<div class="get-quote">
<div id="airportshuttle">
<div class="col-md-9 ggl-ad" id="sf-search-form"></div>
<div class="one_third col-md-3 col-sm-10 ggl-ad">
<p align="center">
<img alt="SuperShuttle airport transportation" src="
									/images/search1.gif" style="width:100px;"/>
<img alt="Go Airport Shuttle network" src="
										/images/search2.gif" style="width:100px;"/>
<br/>
<span style="text-transform: uppercase">
<b>
<font color="#AE0000">and more!!!</font>
</b>
</span>
</p>
<p align="center">
<img alt="Blue van - share ride airport shuttle service" src="
												/images/search3.gif" style="width:150px;"/>
</p>
</div>
</div>
<!-- add css and js script belwo for new atlant search form -->
<link href="https://www.airportshuttles.net/css/ga_atlanta_new.css" rel="stylesheet" text=""/>
<script src="//www.shuttlefare.com/sf/js_libs.js" type="text/javascript"></script>
<script>
					$(function(){
						new ShuttlefareLib.createSearchForm('#sf-search-form', '');
					});
					</script>
</div>
</div>
<div class="modal-footer"></div>
</div>
</div>
</div>
</div>
<!--===========================================-->
</body>
</html>