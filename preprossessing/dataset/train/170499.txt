<!DOCTYPE html>
<html>
<head>
<link href="https://s3.amazonaws.com/thinkific/site_themes/favicon_000/039/814/1487859062.original.ico?1487859062" rel="shortcut icon" type="image/x-icon"/>
<link href="https://s3.amazonaws.com/thinkific/site_themes/favicon_000/039/814/1487859062.original.ico?1487859062" rel="apple-touch-icon" type="image/x-icon"/>
<meta content="authenticity_token" name="csrf-param"/>
<meta content="YQahFzqvxgqeoajMf7Riow8BdCsypsccWJrQ3uuV8TUzPVnvk+HOFVNrzfy60Q9v0rxZKF2Q6BJZVQjnIT4w/Q==" name="csrf-token"/>
<link href="https://assets.thinkific.com/assets/custom_site_theme_required-b92dc23d6ba07c920151d861a6132b5aebbd0e221e79659c99e4044115871a7f.css" media="all" rel="stylesheet"/>
<script>
  window.Thinkific = window.Thinkific || {};
  window.Thinkific.current_user = null;
</script>
<script>
  window.Thinkific = window.Thinkific || {};
  window.Thinkific.translations = {"pt-BR.js_app_upload_image":"Pré-visualização (clique em \"salvar alterações\" para manter esta alteração)","pt-BR.js_app_checkout_coupons_please_wait":"Por favor, aguarde...","pt-BR.js_app_checkout_manager_sign_up":"Cadastrar-se com LinkedIn","pt-BR.js_app_checkout_manager_sign_in":"Entrar com LinkedIn","pt-BR.js_app_invalid_coupon_code":"Código de cupom inválido","pt-BR.js_app_checkout_please_wait":"Por favor, espere...","pt-BR.js_app_checkout_account_created":"Your account has been created.","pt-BR.js_app_payment_errors.invalid_number":"O número do cartão não é um número de cartão de crédito válido.","pt-BR.js_app_payment_errors.invalid_expiry_month":"O mês de vencimento do cartão é inválido.","pt-BR.js_app_payment_errors.invalid_expiry_year":"O ano de vencimento do cartão é inválido.","pt-BR.js_app_payment_errors.invalid_cvc":"O código de segurança do cartão é inválido.","pt-BR.js_app_payment_errors.incorrect_number":"O número do cartão está incorreto.","pt-BR.js_app_payment_errors.expired_card":"O cartão expirou.","pt-BR.js_app_payment_errors.incorrect_cvc":"O código de segurança do cartão está incorreto.","pt-BR.js_app_payment_errors.incorrect_zip":"O código postal do cartão falhou na validação.","pt-BR.js_app_payment_errors.card_declined":"O pagamento foi recusado. Entre em contato com seu banco e tente novamente.","pt-BR.js_app_payment_errors.missing":"Não foi possível encontrar um cartão de crédito para esse cliente.","pt-BR.js_app_payment_errors.processing_error":"Ocorreu um erro durante o processamento do cartão.","pt-BR.js_app_payment_errors.incomplete_number":"O número do cartão está incompleto.","pt-BR.js_app_payment_errors.incomplete_expiry":"A data de validade está incompleta.","pt-BR.js_app_payment_errors.incomplete_cvc":"O código de segurança está incompleto.","pt-BR.js_app_payment_errors.payment_intent_authentication_failure":"Não foi possível concluir a autenticação segura. Por favor, tente novamente.","pt-BR.js_app_payment_errors.setup_intent_authentication_failure":"Não foi possível concluir a autenticação segura. Por favor, tente novamente.","pt-BR.js_app_payment_errors.transaction_declined":"A transação foi recusada. Por favor, tente novamente.","pt-BR.js_app_payment_errors.add_card_contact_bank_error":"Não foi possível adicionar o cartão. Entre em contato com seu banco.","pt-BR.js_app_payment_errors.add_card_try_again_error":"Não foi possível adicionar o cartão. Por favor, tente novamente."};
  
  window.Thinkific.t = function(key) {
    var locale = "pt-BR";
    var localeAndKey = locale + key
    translation = this.translations[localeAndKey];
    if (!translation) {
      // Currently stripe credit card form uses a regex to test for this prefix:
      // `Translation missing`, to indicate that the translation does not exist.
      // If this is changed then the regex in
      // app/assets/javascripts/libs/stripe_credit_card_form.js needs to change too.
      return "Translation missing: " + localeAndKey + "!"
    }
    return translation;
  };
</script>
<link href="https://cdn.thinkific.com/assets/toga-css/0.32.0/fonts/toga-icons.css" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  
  let thinkific_google_analytics_disabled = ""
  if (!thinkific_google_analytics_disabled) {
    var tcd = "thinkific.com";
    ga('create', 'UA-30557184-1', 'auto', { 'cookieDomain': tcd });
    ga('send', 'pageview');
  }
  
  var tenantGoogleAnalyticsKey = "UA-68642946-1";
  
  if(tenantGoogleAnalyticsKey) {
    var tenantCD = "www.aulaead.com";
    ga('create', tenantGoogleAnalyticsKey, 'auto', { 'name': 'tenantTracker', 'cookieDomain': tenantCD });
    ga('tenantTracker.send', 'pageview');
  }
</script>
<script src="https://assets.thinkific.com/assets/application-fb15de0d936cfa39c2a29447590d25bc79e86b771e6c31d940d6fb6dc0212eac.js"></script>
<script src="https://assets.thinkific.com/assets/course-review-rating-2c40867f318e59563344a2a1b4a9a2317b896a91ae97724a5c3d357f86de9cdc.js"></script>
<script async="" src="//fast.wistia.net/assets/external/E-v1.js" type="text/javascript"></script>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="text/html;charset=utf-8" http-equiv="Content-type"/>
<meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" name="viewport"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="black" name="apple-mobile-web-app-status-bar-style"/>
<title>aulaEAD</title>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet"/>
<link href="https://thinkific.s3.amazonaws.com/themes/custom/39810-48400-f0b1dd2f920bd794eb819cf13e34c05b.css" rel="stylesheet"/>
<style type="text/css">
        .page-banner {
          background: url(https://s3.amazonaws.com/thinkific/site_themes/banner_image_000/039/814/1596888592.original.jpg?1596888592);
          background-size: cover;
        }
      </style>
</head>
<body>
<div id="wrap">
<header>
<div class="column">
<div class="header-logo__container">
<a class="header-logo" href="/">
<img class="header-logo__image" src="https://s3.amazonaws.com/thinkific/site_themes/logo_000/039/814/1506563028.medium.png?1506563028"/>
</a>
<button class="header-nav__mobile-btn">
        Menu
      </button>
</div>
<div class="header-nav__container">
<nav class="header-nav">
<ul class="header-nav__list header-nav__left">
<li class="header-nav__item">
<a class="header-nav__link" href="/collections">
                Todos os cursos
              </a>
</li>
<li class="header-nav__item">
<a class="header-nav__link" href="/pages/faq" target="_parent"><b>FAQ</b></a>
</li>
</ul>
<ul class="header-nav__list header-nav__right">
<li class="header-nav__item">
<a class="header-nav__link" href="https://www.linkedin.com/company/aulaead" target="_blank"><img src="https://s3.amazonaws.com/thinkific/file_uploads/39810/images/999/1fc/ad7/in.png"/> </a>
</li>
<li class="header-nav__item sign-in">
<a class="my-account" href="/users/sign_in">
                Entrar
              </a>
</li>
</ul>
</nav>
</div>
</div>
</header>
<div class="page-content-container" id="page-content">
<div class="site-landing" id="pages">
<section class="site-landing__banner page-banner">
<div class="column site-landing__banner-content">
<div class="row">
<div class="col-md-12 site-landing__banner-content-inner">
<h1 class="site-landing__banner--title">
</h1>
<h2 class="site-landing__banner--subtitle">
</h2>
</div>
</div>
</div>
</section>
<section class="site-landing__courses">
<div class="container">
<div class="row product-cards__container">
</div>
</div>
</section>
</div>
</div>
<div id="push"></div>
</div>
<footer class="footer hidden-print" id="global-footer">
<div class="column">
<div class="row">
<div class="col-sm-12">
<ul class="footer-nav">
<li class="footer-nav__item">
<a href="/">
              Início
            </a>
</li>
<li class="footer-nav__item">
<a data-uv-trigger="true" href="mailto:aulaead@professorramos.com">
              Suporte
            </a>
</li>
<li class="footer-nav__item">
<a href="/pages/terms">
                Termos
              </a>
</li>
<li class="footer-nav__item">
<a href="/pages/privacy">
                Política de privacidade
              </a>
</li>
</ul>
</div>
</div>
</div>
<div class="column">
<div class="row">
<div class="col-sm-12 text-center">
<div class="footer-copyright">
          © Copyright aulaEAD 2021
        </div>
</div>
</div>
</div>
</footer>
<!-- FastClick -->
<!-- prevents hover interference and 300ms delay on click for touch devices -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"></script>
<script>
  if ("addEventListener" in document) {
    document.addEventListener("DOMContentLoaded", function () {
      FastClick.attach(document.body);
    })
  }
  $(document).ready(function() {
    function isTouchDevice () {
      try {
        document.createEvent("TouchEvent");
        return true;
      } catch (e) {
        return false;
      }
    }
    var mobileMenuBtn = $(".header-nav__mobile-btn");
    var navContainer = $(".header-nav__container");
    if (isTouchDevice()) {
      mobileMenuBtn.on("touchend", function () {
        navContainer.toggleClass("open");
      });
    } else {
      mobileMenuBtn.click(function() {
        navContainer.toggleClass("open")
      });
    }
  });
</script>
<div id="notifications">
</div>
<script>
  $(function() {
    var isMobile = false;
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      isMobile = true;
    }
    if(!isMobile) {
      if(window.opener && window.opener !== window &&
        window.name && window.name == "__thinkific-sales-widget-product__") {
        var url = window.location.href;
        $(".move-this-party__btn").attr("href", url).attr("target", "_blank").on("click", function() {
          setTimeout(function() {
            window.close();
          }, 500);
        });
        $(".move-this-party__container").removeClass("hidden");
      }
      $(".close-move-this-party__btn").on("click", function(e) {
        e.preventDefault();
        $(".move-this-party__container").addClass("hidden");
      });
    }
  });
</script>
<div class="move-this-party__container hidden">
<div class="row move-this-party__text">
<div class="col-xs-12">
Sugerimos transferir esta parte para uma janela em tela cheia.
<br/>
Você a apreciará muito mais.
</div>
</div>
<div class="row move-this-party__buttons">
<div class="col-xs-4">
<a class="close-move-this-party__btn btn pull-left" href="#">
Fechar
</a>
</div>
<div class="col-xs-8">
<a class="move-this-party__btn btn btn-success pull-right" href="#">
Ir para tela cheia
</a>
</div>
</div>
</div>
<script>
  $(document).ready(function(){
    //We only care about browsers that report themselves as being Chrome
    //We only apply this when the browser supports the CSS necessary to
    //mask the plain text.
    //We only apply this when NOT an SSL connection.
    var isChromeBrowser = /crmo|chrom(e|ium)/i.test(navigator.userAgent);
    var supportsCssProperty = '-webkit-text-security' in document.body.style;
    var isNotSecure = document.location.protocol !== 'https:'
    if (isChromeBrowser && isNotSecure && supportsCssProperty) {
      var password_fields = $('input[type="password"]');
      if (password_fields) {
        password_fields.attr({type:"text"});
        password_fields.css("-webkit-text-security", "disc")
      }
    }
  });
</script>
<input name="custom-theme-version" type="hidden" value="1.9.0"/>
</body>
</html>
