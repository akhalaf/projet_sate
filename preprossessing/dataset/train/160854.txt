<!DOCTYPE html>
<!--[if lte IE 8]> <html class="oldIE" lang="en"> <![endif]--><!--[if gt IE 8]><!--><html itemscope="itemscope" itemtype="http://schema.org/Thing" lang="en" prefix="og: http://ogp.me/ns#">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://www.asm-autos.co.uk/" rel="canonical"/>
<meta content="index,follow" name="robots"/>
<title>Car Salvage Auction, Auto Breakers &amp; Scrap Yard | ASM Auto Recycling</title>
<meta content="ASM Auto Recycling" name="author"/>
<meta content="general" name="rating"/>
<meta content="ASM Auto Recycling Ltd is one of the UK's professional vehicle salvage agents. Our salvage auction offers over 2,500 cars, vans and motorbikes for sale every week." name="description"/>
<meta content="website" property="og:type"/>
<meta content="Car Salvage Auction, Auto Breakers &amp; Scrap Yard | ASM Auto Recycling" property="og:title"/>
<meta content="https://www.asm-autos.co.uk/" property="og:url"/>
<meta content="en_GB" property="og:locale"/>
<meta content="ASM Auto Recycling" property="og:site_name"/>
<meta content="ASM Auto Recycling Ltd is one of the UK's professional vehicle salvage agents. Our salvage auction offers over 2,500 cars, vans and motorbikes for sale every week." property="og:description"/>
<meta content="https://www.asm-autos.co.uk/large-icon.png" property="og:image"/>
<meta content="summary" name="twitter:card"/>
<meta content="Car Salvage Auction, Auto Breakers &amp; Scrap Yard | ASM Auto Recycling" name="twitter:title"/>
<meta content="https://www.asm-autos.co.uk/" name="twitter:url"/>
<meta content="ASM Auto Recycling Ltd is one of the UK's professional vehicle salvage agents. Our salvage auction offers over 2,500 cars, vans and motorbikes for sale every week." name="twitter:description"/>
<meta content="https://www.asm-autos.co.uk/large-icon.png" name="twitter:image"/>
<meta content="@ASM_Autos" name="twitter:site"/>
<meta content="Car Salvage Auction, Auto Breakers &amp; Scrap Yard | ASM Auto Recycling" itemprop="name"/>
<meta content="https://www.asm-autos.co.uk/" itemprop="url"/>
<meta content="ASM Auto Recycling Ltd is one of the UK's professional vehicle salvage agents. Our salvage auction offers over 2,500 cars, vans and motorbikes for sale every week." itemprop="description"/>
<meta content="https://www.asm-autos.co.uk/large-icon.png" itemprop="image"/>
<link href="https://www.asm-autos.co.uk/apple-touch-icon.png?v=20200129" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://www.asm-autos.co.uk/site.webmanifest?v=20200129" rel="manifest"/>
<link color="#e72727" href="https://www.asm-autos.co.uk/safari-pinned-tab.svg?v=20200129" rel="mask-icon"/>
<link href="https://www.asm-autos.co.uk/favicon.ico?v=20200129" rel="shortcut icon"/>
<meta content="ASM Autos" name="apple-mobile-web-app-title"/>
<meta content="ASM Autos" name="application-name"/>
<meta content="#ffc40d" name="msapplication-TileColor"/>
<meta content="#e72727" name="theme-color"/>
<link href="https://www.asm-autos.co.uk/workspace/fontawesome-free-5.11.2-web/css/all.min.css" rel="stylesheet"/>
<link href="https://www.asm-autos.co.uk/workspace/bootstrap-3.4.1-custom/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://www.asm-autos.co.uk/workspace/css/shared.css?v=20201210" rel="stylesheet"/>
<link href="https://www.asm-autos.co.uk/workspace/css/main.css?v=20200817" rel="stylesheet"/>
<script async="true" src="https://www.googletagmanager.com/gtag/js?id=UA-2228086-1"></script>
<script>
		var ga_tracking_id = 'UA-2228086-1';

		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-2228086-1', { 'anonymize_ip': true });
		gtag('config', 'UA-2228086-7', {
			'anonymize_ip': true,
			'linker': {
				'domains': ['www.asm-autos.co.uk', 'parts.asm-autos.co.uk', 'sell-your-car.asm-autos.co.uk', 'auctions.asm-autos.co.uk'],
				'decorate_forms': true
			}
		});
	</script>
<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '1085475421802810');
		fbq('track', 'PageView');
	</script>
<noscript>
<img height="1" src="https://www.facebook.com/tr?id=1085475421802810&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/>
</noscript>
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="home index">
<div class="visible-xs-block" style="height: 30px;"></div>
<nav class="navbar navbar-inverse" id="navbarWrapper">
<div class="container">
<div class="row">
<div class="col-xs-7 col-sm-5">
<a href="https://www.asm-autos.co.uk/">
<img alt="ASM Auto Recycling logo" src="https://www.asm-autos.co.uk/workspace/images/asm-auto-recycling.976x384.tiny.png"/>
</a>
</div>
<div class="col-xs-5 col-sm-7">
<div class="collapse navbar-collapse navbar-reveal" id="asm-contact-icons">
<ul class="list-unstyled">
<li>
<a class="asm-social" href="https://twitter.com/ASM_Autos" title="Connect with us on Twitter">
<span class="fab fa-fw fa-lg fa-twitter">
<span class="sr-only">Twitter</span>
</span>
</a>
</li>
<li>
<a class="asm-social" href="https://www.facebook.com/Asm.auto.recycling/" title="Connect with us on Facebook">
<span class="fab fa-fw fa-lg fa-facebook-square">
<span class="sr-only">Facebook</span>
</span>
</a>
</li>
<li>
<a class="asm-social" href="https://www.instagram.com/asm_autos/" title="Connect with us on Instagram">
<span class="fab fa-fw fa-lg fa-instagram">
<span class="sr-only">Instagram</span>
</span>
</a>
</li>
<li>
<a class="asm-social" href="https://www.linkedin.com/company/asm-auto-recycling-ltd/" title="Connect with us on LinkedIn">
<span class="fab fa-fw fa-lg fa-linkedin">
<span class="sr-only">LinkedIn</span>
</span>
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="navbar-header">
<button aria-expanded="false" class="navbar-toggle collapsed" data-target=".navbar-reveal" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-toggle" href="tel:+441844268940">
<span class="fa fa-phone"></span>
<span class="sr-only">Call ASM</span>
</a>
</div>
<div id="emergency_message" style="position: relative; min-width: 100%; margin: 0px; padding: 2.5px 20px 1.5px; font-size: 18px; font-weight: bold; line-height: 2; text-align: center; background-color: #FFEB0D; background-image: linear-gradient(135deg, rgba(255, 235, 13, 0) 0%, rgba(255, 235, 13, 0) 7%, rgba(255, 235, 13, 0.97) 23%, rgba(255, 235, 13, 0.97) 77%, rgba(255, 235, 13, 0) 93%, rgba(255, 235, 13, 0) 100%), repeating-linear-gradient(45deg, #FFEB0D, #FFEB0D 20px, #000 20px, #000 40px);">
<span class="hidden-xs d-none d-md-inline-block">IMPORTANT – Please read our </span>
<a href="https://www.asm-autos.co.uk/covid-19-coronavirus-updates/" style="color: #e72727; text-decoration: underline; white-space: nowrap;">COVID-19 updates<span style="position: absolute; top: 0; right: 0; bottom: 0; left: 0; z-index: 1; pointer-events: auto; background-color: rgba(0, 0, 0, 0);"></span></a>
<span class="hidden-xs d-none d-md-inline-block"> before proceeding.</span>
</div>
<div class="collapse navbar-collapse navbar-reveal" id="navbar">
<ul class="nav navbar-nav">
<li>
<a class="active" href="https://www.asm-autos.co.uk/"><span aria-hidden="true" class="fa fa-lg fa-fw fa-home"></span> Home</a>
</li>
<li>
<a href="https://sell-your-car.asm-autos.co.uk/scrap-my-car/"><span aria-hidden="true" class="fa fa-lg fa-fw fa-car"></span> Sell Your Car</a>
</li>
<li>
<a href="https://auctions.asm-autos.co.uk/auction/"><span aria-hidden="true" class="fa fa-lg fa-fw fa-gavel"></span> Auction</a>
</li>
<li>
<a href="https://parts.asm-autos.co.uk/parts-breaking-tyres/"><span aria-hidden="true" class="fa fa-lg fa-fw fa-wrench"></span> Car Parts</a>
</li>
<li>
<a href="https://parts.asm-autos.co.uk/engines/"><span aria-hidden="true" class="fa fa-lg fa-fw fa-wrench"></span> Engines</a>
</li>
<li>
<a href="https://parts.asm-autos.co.uk/tyre-bay/"><span class="fa fa-stack fa-fw" style="font-size: 0.8em; width: 36px; height: 27px; margin-top: -7px;"><span aria-hidden="true" class="far fa-circle fa-stack-2x"></span><span aria-hidden="true" class="far fa-life-ring fa-stack-1x" style="font-size: 135%;"></span></span> Tyre Bay</a>
</li>
<li>
<a href="https://www.asm-autos.co.uk/about-us/"><span aria-hidden="true" class="fa fa-lg fa-fw fa-info-circle"></span> About Us</a>
</li>
<li>
<a href="https://www.asm-autos.co.uk/blog/"><span aria-hidden="true" class="fa fa-lg fa-fw fa-rss-square"></span> Blog</a>
</li>
<li>
<a href="https://www.asm-autos.co.uk/careers/"><span aria-hidden="true" class="fa fa-lg fa-fw fa-briefcase"></span> Careers</a>
</li>
<li>
<a href="https://www.asm-autos.co.uk/contact-us/"><span aria-hidden="true" class="fa fa-lg fa-fw fa-phone"></span> Contact Us</a>
</li>
</ul>
<div class="visible-xs-block" style="height: 30px;"></div>
</div>
</nav>
<section id="content">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="row">
<div class="col-md-8 col-lg-7">
<h1>ASM Auto Recycling</h1>
<p>Established for over 35 years, ASM Auto Recycling is widely regarded as one of the UK’s most modern and professional vehicle salvage agents and parts recyclers.  We currently process over 30,000 vehicles every year.  Our <a href="/online-salvage-auction/">online salvage auction</a> offers over 2,500 cars, vans and motorbikes for sale every week and we have a further 2,500 vehicles for <span class="text-nowrap">parts dismantling</span>.</p>
</div>
<div class="col-md-4 col-lg-offset-1">
<a class="well impactful video-tour" href="https://www.asm-autos.co.uk/about-us/video-tour-of-asm/">
<h2>Video Tour of ASM</h2>
<div class="play-icon">
<span class="fa fa-3x fa-play"></span>
</div>
</a>
</div>
</div>
<div class="row">
<div class="col-sm-6 col-md-4" id="syc">
<div class="syc-form well well-full impactful">
<h2>Sell Your Car</h2>
<p>
<strong>Best prices paid on any car!</strong>
</p>
<p>All cars wanted, including MOT failures and non-runners.  We offer prompt collection and immediate payment.</p>
<form action="/null.html" class="dh-data" data-action="https://sell-your-car.asm-autos.co.uk/data/smc/" data-alttext="Sell Your Car page" data-alturl="https://sell-your-car.asm-autos.co.uk/scrap-my-car/" data-handle="syc" method="post" onsubmit="return doSubmission(this);">
<div class="form-group form-group-lg required">
<label class="sr-only" for="syc1">Vehicle registration</label>
<input class="form-control special reg-plate" id="syc1" maxlength="8" name="regno" placeholder="Your reg" spellcheck="false" title="Input your vehicle registration" type="text"/>
</div>
<div class="form-group form-group-lg required">
<label class="sr-only" for="syc2">Post code</label>
<input class="form-control special postcode" id="syc2" maxlength="10" name="postcode" placeholder="Post code" spellcheck="false" type="text"/>
</div>
<div class="form-group form-group-lg required">
<label class="sr-only" for="syc3">Email address</label>
<div class="input-group">
<div class="input-group-addon">
<span class="fa fa-fw fa-envelope"></span>
</div>
<input class="form-control" id="syc3" maxlength="80" name="email" placeholder="Email address" type="email"/>
</div>
</div>
<div class="form-group form-group-lg required">
<label class="sr-only" for="syc4">Telephone number</label>
<div class="input-group">
<div class="input-group-addon">
<span class="fa fa-fw fa-phone"></span>
</div>
<input class="form-control" id="syc4" maxlength="80" name="telno" placeholder="Telephone number" type="tel"/>
</div>
</div>
<button class="btn btn-warning" type="submit">Get price <span class="fa fa-chevron-right"></span></button>
</form>
</div>
</div>
<div class="col-sm-6 col-md-4" id="osa">
<div class="well well-full impactful">
<h2>Online Auction</h2>
<p>
<strong>Featured vehicles</strong>
</p>
<div class="osa-list dh-data" data-alttext="Online Auction pages" data-alturl="https://auctions.asm-autos.co.uk/auction/" data-handle="osa" data-qty="6" style="display: block; position: relative; min-height: 321px;"></div>
<a class="btn btn-warning" href="https://auctions.asm-autos.co.uk/auction/">
<span class="text">View vehicles</span>
<span class="fa fa-chevron-right"></span>
</a>
</div>
</div>
<div class="clearfix visible-sm-block"></div>
<div class="col-md-4">
<div class="row">
<div class="col-sm-6 col-md-12" id="ucp">
<div class="well well-half impactful">
<h2>Car Parts</h2>
<p>
<strong>Over 1<abbr title="million">m</abbr> searchable parts!</strong>
</p>
<p>Please visit the <a href="https://parts.asm-autos.co.uk/parts-breaking-tyres/">Car Parts pages</a>.</p>
<a class="btn btn-warning" href="https://parts.asm-autos.co.uk/parts-breaking-tyres/">Find parts <span class="fa fa-chevron-right"></span></a>
</div>
</div>
<div class="col-sm-6 col-md-12" id="utb">
<div class="well well-half impactful">
<h2>Tyre Bay</h2>
<p>
<strong>New &amp; part-worn</strong>
</p>
<p>Please visit the <a href="https://parts.asm-autos.co.uk/tyre-bay/">Tyre Bay pages</a>.</p>
<a class="btn btn-warning" href="https://parts.asm-autos.co.uk/tyre-bay/">Find tyres <span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-4 col-md-push-8">
<div class="row">
<div class="col-sm-6 col-md-12">
<div class="well impactful" id="contacts">
<h2>Contact Us</h2>
<dl class="dl-horizontal no-xs-wrap">
<dt>
<span class="fas fa-fw fa-phone" title="Telephone"></span>
<span class="sr-only">Telephone:</span>
</dt>
<dd>
<a href="tel:+441844268940">01844 268 940</a>
</dd>
<dt>
<span class="fas fa-fw fa-envelope" title="Email"></span>
<span class="sr-only">Email:</span>
</dt>
<dd>
<span class="action-email" data-alias="admin" data-domain="asm-autos.co.uk">
<em data-c="1">admin at asm-autos dot co dot uk</em>
</span>
</dd>
<dt class="visible-sm-block"> </dt>
<dd class="visible-sm-block"> </dd>
<dt class="visible-sm-block"> </dt>
<dd class="visible-sm-block"> </dd>
</dl>
</div>
</div>
<div class="col-sm-6 col-md-12">
<div class="well impactful" id="opening">
<h2>
<span class="text-nowrap">Opening Hours</span>
</h2>
<div data-hours-name="thame-oxford">
<p>Temporarily reduced hours. Please read <a href="https://www.asm-autos.co.uk/covid-19-coronavirus-updates/">COVID-19 updates</a> for details.</p>
<table class="oh-table">
<tbody>
<tr>
<th class="oh-day"><abbr title="Monday to Thursday">Mon - Thu</abbr></th>
<td class="oh-from">8:00am</td>
<td class="oh-to">5:00pm</td>
</tr>
<tr>
<th class="oh-day">Friday</th>
<td class="oh-from">8:00am</td>
<td class="oh-to">4:45pm</td>
</tr>
<tr>
<th class="oh-day">Saturday</th>
<td class="oh-closed" colspan="2">Closed</td>
</tr>
<tr>
<th class="oh-day">Sunday</th>
<td class="oh-closed" colspan="2">Closed</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-8 col-md-pull-4">
<p>Supporting the <a href="/used-car-parts/">used car parts</a> market is an integral part of ASM’s business and, as part of our auto solutions strategy, we have an impressive ‘green recycled parts’ department which stocks over 100,000 recycled car parts – and all of our green parts are sold with a <span class="text-nowrap">90-day guarantee</span>.</p>
<p>If you are looking to realise the value of either a roadworthy or end-of-life vehicle, we can guide you through the process of <a href="https://auctions.asm-autos.co.uk/auction/">auctioning your vehicle</a> and everything that’s involved with <a href="/scrap-my-car/">scrapping your car</a>, from disposal to final paperwork.  Additionally, whether you’re trying to find a specific part for your car, or purchase a used vehicle for restoration purposes, we will help to solve your requirements as quickly and cost-effectively <span class="text-nowrap">as possible</span>.</p>
<p>Whether you are looking to realise the value of a roadworthy or non-roadworthy vehicle, or trying to find a specific part for your car, or even to purchase a used vehicle for restoration purposes, we will help to solve your requirements as quickly and cost-effectively <span class="text-nowrap">as possible</span>.</p>
<div class="row" id="blog">
<div class="col-sm-3">
<h2>The blog</h2>
<p>Aside from our core services, we also offer customers a timely and informative news service via our <a href="/blog/">ASM blog</a>, which will allow you to keep up with all the latest and most relevant <span class="text-nowrap">industry news</span>.</p>
</div>
<div class="col-sm-8 col-sm-offset-1 col-md-9 col-md-offset-0 col-lg-8 col-lg-offset-1">
<h3 class="h4">Recent posts</h3>
<ul class="list-compact">
<li>
<a href="https://www.asm-autos.co.uk/blog/car-battery-disposal-tips/" title="Car Battery Disposal Tips">Car Battery Disposal Tips</a>
</li>
<li>
<a href="https://www.asm-autos.co.uk/blog/what-is-cat-d/" title="What is Cat D?">What is Cat D?</a>
</li>
<li>
<a href="https://www.asm-autos.co.uk/blog/can-i-scrap-my-car-without-keys/" title="Can I scrap my car without keys?">Can I scrap my car without keys?</a>
</li>
<li>
<a href="https://www.asm-autos.co.uk/blog/best-second-hand-small-car/" title="Best second-hand small car">Best second-hand small car</a>
</li>
</ul>
<h3 class="h4">Featured posts</h3>
<ul class="list-compact">
<li>
<a href="https://www.asm-autos.co.uk/blog/the-complete-guide-to-scrapping-your-car/" title="The complete guide to scrapping your car">The complete guide to scrapping your car</a>
</li>
<li>
<a href="https://www.asm-autos.co.uk/blog/a-guide-to-buying-a-salvage-car/" title="A guide to buying a salvage car">A guide to buying a salvage car</a>
</li>
<li>
<a href="https://www.asm-autos.co.uk/blog/the-pitfalls-of-buying-used-cars-asm-s-new-infographic/" title="The pitfalls of buying used cars – ASM’s new infographic">The pitfalls of buying used cars – ASM’s new infographic</a>
</li>
<li>
<a href="https://www.asm-autos.co.uk/blog/vic-scheme-abolished-how-does-it-impact-you/" title="VIC scheme abolished: how does it impact you?">VIC scheme abolished: how does it impact you?</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<footer class="small">
<div class="container">
<p class="text-center" id="translateThis" style="height: 22px;"></p>
<p class="text-center" id="logos">
<a href="https://www.asm-autos.co.uk/about-us/iso-certificates/">
<img alt="ISO 9001 Certificate of Quality Assurance" class="iso-badge" src="https://www.asm-autos.co.uk/workspace/images/bab-ukas-iso-9001.300.tiny.png" title="ISO 9001 Certificate of Quality Assurance"/>
</a>
<a href="https://www.asm-autos.co.uk/about-us/iso-certificates/">
<img alt="ISO 14001 Certificate of Environmental Assurance" class="iso-badge" src="https://www.asm-autos.co.uk/workspace/images/bab-ukas-iso-14001.300.tiny.png" title="ISO 14001 Certificate of Environmental Assurance"/>
</a>
<a href="https://www.asm-autos.co.uk/about-us/iso-certificates/">
<img alt="ISO 27001 Certificate of Information Security" class="iso-badge" src="https://www.asm-autos.co.uk/workspace/images/bab-ukas-iso-27001.300.tiny.png" title="ISO 27001 Certificate of Information Security"/>
</a>
<br class="hidden-md hidden-lg d-block d-lg-none"/>
<a href="https://www.vrauk.org/" rel="nofollow">
<img alt="Vehicle Recyclers’ Association" src="https://www.asm-autos.co.uk/workspace/images/logo-vra.300.tiny.png" title="Vehicle Recyclers’ Association"/>
</a>
<a href="https://www.e2etotalloss.com/" rel="nofollow">
<img alt="End-to-End Total Loss Vehicle Management" src="https://www.asm-autos.co.uk/workspace/images/logo-e2e.300.tiny.png" title="End-to-End Total Loss Vehicle Management"/>
</a>
<img alt="VRAC Certified Vehicle Recycler" src="https://www.asm-autos.co.uk/workspace/images/logo-vrac.300.tiny.png" title="VRAC Certified Vehicle Recycler"/>
</p>
<p class="text-center">© <a href="https://www.asm-autos.co.uk/" target="_top">ASM Auto Recycling Ltd.</a>  <abbr title="Registered">Reg.</abbr> in England <abbr title="Number">no.</abbr> 01721363.  <abbr title="Registered">Reg.</abbr> office: 55 Station Road, Beaconsfield, HP9 1QL.  <a class="text-nowrap" href="https://www.asm-autos.co.uk/terms-and-conditions/" style="color: #E72727;">Terms &amp; Conditions</a>.  <a class="text-nowrap" href="https://www.asm-autos.co.uk/about-us/policies/">Privacy Policy</a>.  <br class="hidden-xs hidden-sm d-none d-lg-block"/><a href="https://www.asm-autos.co.uk/site-map/">Website map</a>.  <span class="visible-xs-inline d-sm-none">Mobile</span> Web Design by <a class="text-nowrap" href="https://www.activemediaforge.co.uk/">Active MediaForge</a>.  <br class="visible-sm-block d-none d-sm-block d-md-none"/>Parts &amp; reg look-up software by <a class="text-nowrap" href="https://hollanderinternational.com/">Hollander International</a>.  Auction by <a href="https://www.synetiq.co.uk/">SYNETIQ</a>.</p>
<div class="row">
<div class="col-xs-6 col-sm-4 col-md-2" style="padding-top: 6px;">
<a href="https://sell-your-car.asm-autos.co.uk/scrap-my-car/">
<img alt="We buy any type of car." height="120" src="https://www.asm-autos.co.uk/workspace/images/we-buy-any-car-2.3.120x120.tiny.png" width="120"/>
</a>
</div>
<div class="col-xs-6 col-sm-4 col-md-2">
<h5>
<a href="https://sell-your-car.asm-autos.co.uk/scrap-my-car/">Sell Your Car</a>
</h5>
<ul class="list-unstyled list-compact">
<li>
<a href="https://www.asm-autos.co.uk/sell-your-car/value-my-car/">Value My Car</a>
</li>
<li>
<a href="https://sell-your-car.asm-autos.co.uk/scrap-my-car/">Instant Quotation</a>
</li>
</ul>
</div>
<div class="clearfix visible-xs-block"></div>
<div class="col-xs-6 col-sm-4 col-md-2">
<h5>
<a href="https://auctions.asm-autos.co.uk/auction/" title="Auto salvage auction">Auction</a>
</h5>
<ul class="list-unstyled list-compact">
<li>
<a href="https://auctions.asm-autos.co.uk/auction/items/?seller=32&amp;tab=1">Cars</a>
</li>
<li>
<a href="https://auctions.asm-autos.co.uk/auction/items/?seller=32&amp;tab=4">Motorbikes</a>
</li>
<li>
<a href="https://auctions.asm-autos.co.uk/auction/items/?seller=32&amp;tab=2">Commercials</a>
</li>
<li>
<a href="https://auctions.asm-autos.co.uk/auction/items/?seller=32&amp;tab=3">Plant &amp; HGV</a>
</li>
</ul>
</div>
<div class="clearfix visible-sm-block"></div>
<div class="col-xs-6 col-sm-4 col-md-2">
<h5>
<a href="https://parts.asm-autos.co.uk/parts-breaking-tyres/" title="Used Car Parts">Car Parts</a>
</h5>
<ul class="list-unstyled list-compact">
<li>
<a href="https://parts.asm-autos.co.uk/part-info-request/">Parts Enquiry Form</a>
</li>
<li>
<a href="https://parts.asm-autos.co.uk/tyre-bay/">Tyre Bay</a>
</li>
<li>
<a href="https://parts.asm-autos.co.uk/breaking-vehicles/">Breaking Vehicles</a>
</li>
</ul>
</div>
<div class="clearfix visible-xs-block"></div>
<div class="col-xs-6 col-sm-4 col-md-2">
<h5>
<a href="https://www.asm-autos.co.uk/asm-guides/">ASM Guides</a>
</h5>
<ul class="list-unstyled list-compact">
<li>
<a href="https://www.asm-autos.co.uk/asm-guides/how-it-works/">How it Works</a>
</li>
<li>
<a href="https://www.asm-autos.co.uk/asm-guides/used-car-document-checklist/">Used Car Document Checklist</a>
</li>
<li>
<a href="https://www.asm-autos.co.uk/asm-guides/car-recycling/">Car Recycling</a>
</li>
</ul>
</div>
<div class="col-xs-6 col-sm-4 col-md-2">
<h5>
<a href="https://www.asm-autos.co.uk/about-us/" title="About ASM">About Us</a>
</h5>
<ul class="list-unstyled list-compact">
<li>
<a href="https://www.asm-autos.co.uk/contact-us/">Contact Us</a>
</li>
<li>
<a href="https://www.asm-autos.co.uk/about-us/in-the-community/">In The Community</a>
</li>
<li>
<a href="https://www.asm-autos.co.uk/about-us/policies/">Policies</a>
</li>
<li>
<a href="https://www.asm-autos.co.uk/about-us/licences/">Licences</a>
</li>
</ul>
</div>
</div>
</div>
</footer>
<a href="#" id="back-to-top">
<span class="fas fa-arrow-up"></span>
</a>
<script src="https://www.asm-autos.co.uk/workspace/js/jquery-1.12.4.min.js"></script>
<script src="https://www.asm-autos.co.uk/workspace/bootstrap-3.4.1-custom/js/bootstrap.min.js"></script>
<script src="https://www.asm-autos.co.uk/workspace/js/shared.js?v=20200511"></script>
<script src="https://www.asm-autos.co.uk/workspace/js/responsive.js?v=20200916"></script>
<script type="application/ld+json">
		{
			"@context" : "http://schema.org",
			"@type" : "LocalBusiness",
			"image" : [
				"https://www.asm-autos.co.uk/workspace/images/schema-thame-oxford-hq-1-by-1-.jpg",
				"https://www.asm-autos.co.uk/workspace/images/schema-thame-oxford-hq-4-by-3-.jpg",
				"https://www.asm-autos.co.uk/workspace/images/schema-thame-oxford-hq-16-by-9-.jpg"
			],
			"name" : "ASM Auto Recycling Ltd.",
			"areaServed" : "Thame",
			"address" : {
				"@type" : "PostalAddress",
				"streetAddress" : "Menlo Industrial Park",
				"addressLocality" : "Rycote Lane",
				"addressRegion" : "Thame",
				"postalCode" : "OX9 2BY"
			},
			"openingHoursSpecification": [
				{
					"@type" : "OpeningHoursSpecification",
					"dayOfWeek" : [
						"Monday",
						"Tuesday",
						"Wednesday", 
						"Thursday"
					],
					"opens" : "08:00",
					"closes" : "17:00"
				},
				{
					"@type" : "OpeningHoursSpecification",
					"dayOfWeek": "Friday",
					"opens" : "08:00",
					"closes" : "16:45"
				}
			],
			"geo": {
				"@type" : "GeoCoordinates",
				"latitude" : "51.743312",
				"longitude" : "-1.000575"
			},
			"hasMap" : "https://www.google.co.uk/maps/place/ASM+Auto+Recycling+Ltd/@51.7435376,-1.0043276,15.75z/data=!4m5!3m4!1s0x4876edca8890dcc9:0xefeb8ee8560cd3b6!8m2!3d51.743312!4d-1.000575",
			"telephone" : "01844 268940",
			"email" : "admin@asm-autos.co.uk",
			"url" : "https://www.asm-autos.co.uk/",
			"logo" : "https://www.asm-autos.co.uk/schema-logo.jpg",
			"sameAs" : [
				"https://twitter.com/ASM_Autos",
				"https://www.facebook.com/Asm.auto.recycling/",
				"https://www.instagram.com/asm_autos/",
				"https://www.linkedin.com/company/asm-auto-recycling-ltd/",
				"https://www.youtube.com/channel/UC1eRrmLXf4kjhN4uCpkscXw"
			]
		}
	</script>
</body>
</html>
