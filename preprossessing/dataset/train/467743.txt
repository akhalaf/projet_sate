<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="pl-PL"> <![endif]--><!--[if IE 7 ]><html class="ie ie7" lang="pl-PL"> <![endif]--><!--[if IE 8 ]><html class="ie ie8" lang="pl-PL"> <![endif]--><!--[if IE 9 ]><html class="ie ie9" lang="pl-PL"> <![endif]--><!--[if (gt IE 9)|!(IE)]><!--><html lang="pl-PL"> <!--<![endif]-->
<head>
<title>Error 404 Not Found | El-mark</title>
<meta content="Strony nie znaleziono - El-mark | " name="description"/>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="//gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.el-mark.com.pl/wp-content/themes/theme50494/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="https://www.el-mark.com.pl/xmlrpc.php" rel="pingback"/>
<link href="https://www.el-mark.com.pl/feed/" rel="alternate" title="El-mark" type="application/rss+xml"/>
<link href="https://www.el-mark.com.pl/feed/atom/" rel="alternate" title="El-mark" type="application/atom+xml"/>
<link href="https://www.el-mark.com.pl/wp-content/themes/theme50494/bootstrap/css/bootstrap.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.el-mark.com.pl/wp-content/themes/theme50494/bootstrap/css/responsive.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/css/camera.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.el-mark.com.pl/wp-content/themes/theme50494/style.css" media="all" rel="stylesheet" type="text/css"/>
<!-- This site is optimized with the Yoast SEO plugin v15.4 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex, follow" name="robots"/>
<meta content="pl_PL" property="og:locale"/>
<meta content="Strony nie znaleziono - El-mark" property="og:title"/>
<meta content="El-mark" property="og:site_name"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://www.el-mark.com.pl/#website","url":"https://www.el-mark.com.pl/","name":"El-mark","description":"","potentialAction":[{"@type":"SearchAction","target":"https://www.el-mark.com.pl/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"pl-PL"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//maps.googleapis.com" rel="dns-prefetch"/>
<link href="//netdna.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.el-mark.com.pl/feed/" rel="alternate" title="El-mark » Kanał z wpisami" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.el-mark.com.pl\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.el-mark.com.pl/wp-content/plugins/cherry-plugin/lib/js/FlexSlider/flexslider.css?ver=2.2.0" id="flexslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.el-mark.com.pl/wp-content/plugins/cherry-plugin/lib/js/owl-carousel/owl.carousel.css?ver=1.24" id="owl-carousel-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.el-mark.com.pl/wp-content/plugins/cherry-plugin/lib/js/owl-carousel/owl.theme.css?ver=1.24" id="owl-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css?ver=3.2.1" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.el-mark.com.pl/wp-content/plugins/cherry-plugin/includes/css/cherry-plugin.css?ver=1.2.8.2" id="cherry-plugin-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.el-mark.com.pl/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.el-mark.com.pl/wp-content/plugins/cherry-parallax/css/parallax.css?ver=1.0" id="cherry-parallax-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.el-mark.com.pl/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.1" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.el-mark.com.pl/wp-content/plugins/responsive-lightbox/assets/swipebox/swipebox.min.css?ver=2.2.3" id="responsive-lightbox-swipebox-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.el-mark.com.pl/wp-content/themes/theme50494/main-style.css" id="theme50494-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/css/magnific-popup.css?ver=0.9.3" id="magnific-popup-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Istok+Web&amp;subset=latin" id="options_typography_Istok+Web-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.el-mark.com.pl/wp-content/plugins/motopress-content-editor/bootstrap/bootstrap-grid.min.css?ver=1.4.6" id="mpce-bootstrap-grid-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.el-mark.com.pl/wp-content/plugins/motopress-content-editor/includes/css/theme.css?ver=1.4.6" id="mpce-theme-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-js" src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/js/jquery-1.7.2.min.js?ver=1.7.2" type="text/javascript"></script>
<script id="easing-js" src="https://www.el-mark.com.pl/wp-content/plugins/cherry-plugin/lib/js/jquery.easing.1.3.js?ver=1.3" type="text/javascript"></script>
<script id="elastislide-js" src="https://www.el-mark.com.pl/wp-content/plugins/cherry-plugin/lib/js/elasti-carousel/jquery.elastislide.js?ver=1.2.8.2" type="text/javascript"></script>
<script id="googlemapapis-js" src="//maps.googleapis.com/maps/api/js?v=3&amp;signed_in=false&amp;key&amp;ver=5.6" type="text/javascript"></script>
<script id="responsive-lightbox-swipebox-js" src="https://www.el-mark.com.pl/wp-content/plugins/responsive-lightbox/assets/swipebox/jquery.swipebox.min.js?ver=2.2.3" type="text/javascript"></script>
<script id="responsive-lightbox-infinite-scroll-js" src="https://www.el-mark.com.pl/wp-content/plugins/responsive-lightbox/assets/infinitescroll/infinite-scroll.pkgd.min.js?ver=5.6" type="text/javascript"></script>
<script id="responsive-lightbox-js-extra" type="text/javascript">
/* <![CDATA[ */
var rlArgs = {"script":"swipebox","selector":"lightbox","customEvents":"","activeGalleries":"1","animation":"1","hideCloseButtonOnMobile":"0","removeBarsOnMobile":"0","hideBars":"1","hideBarsDelay":"5000","videoMaxWidth":"1080","useSVG":"1","loopAtEnd":"0","woocommerce_gallery":"0","ajaxurl":"https:\/\/www.el-mark.com.pl\/wp-admin\/admin-ajax.php","nonce":"2205cbb583"};
/* ]]> */
</script>
<script id="responsive-lightbox-js" src="https://www.el-mark.com.pl/wp-content/plugins/responsive-lightbox/js/front.js?ver=2.2.3" type="text/javascript"></script>
<script id="parallaxSlider-js" src="https://www.el-mark.com.pl/wp-content/themes/theme50494/js/parallaxSlider.js?ver=1.0" type="text/javascript"></script>
<script id="wpcf7-js" src="https://www.el-mark.com.pl/wp-content/themes/theme50494/js/wpcf7.js?ver=1.0" type="text/javascript"></script>
<script id="migrate-js" src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/js/jquery-migrate-1.2.1.min.js?ver=1.2.1" type="text/javascript"></script>
<script id="swfobject-js" src="https://www.el-mark.com.pl/wp-includes/js/swfobject.js?ver=2.2-20120417" type="text/javascript"></script>
<script id="modernizr-js" src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/js/modernizr.js?ver=2.0.6" type="text/javascript"></script>
<script id="jflickrfeed-js" src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/js/jflickrfeed.js?ver=1.0" type="text/javascript"></script>
<script id="custom-js" src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/js/custom.js?ver=1.0" type="text/javascript"></script>
<script id="bootstrap-js" src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/bootstrap/js/bootstrap.min.js?ver=2.3.0" type="text/javascript"></script>
<script id="google-charts-api-js" src="//www.google.com/jsapi?ver=1.4.6" type="text/javascript"></script>
<script id="mp-google-charts-js-extra" type="text/javascript">
/* <![CDATA[ */
var motopressGoogleChartsPHPData = {"motopressCE":"0"};
/* ]]> */
</script>
<script id="mp-google-charts-js" src="https://www.el-mark.com.pl/wp-content/plugins/motopress-content-editor/includes/js/mp-google-charts.js?ver=1.4.6" type="text/javascript"></script>
<link href="https://www.el-mark.com.pl/wp-json/" rel="https://api.w.org/"/><link href="https://www.el-mark.com.pl/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.el-mark.com.pl/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<script>
 var system_folder = 'https://www.el-mark.com.pl/wp-content/themes/CherryFramework/admin/data_management/',
	 CHILD_URL ='https://www.el-mark.com.pl/wp-content/themes/theme50494',
	 PARENT_URL = 'https://www.el-mark.com.pl/wp-content/themes/CherryFramework', 
	 CURRENT_THEME = 'theme50494'</script>
<style type="text/css">
</style>
<style type="text/css">
h1 { font: bold 24px/30px Istok Web;  color:#1e2731; }
h2 { font: normal 24px/30px Istok Web;  color:#e22004; }
h3 { font: bold 24px/30px Istok Web;  color:#1e2731; }
h4 { font: normal 16px/26px Istok Web;  color:#e22004; }
h5 { font: bold 24px/21px Istok Web;  color:#1e2731; }
h6 { font: normal 16px/21px Istok Web;  color:#424f5e; }
body { font-weight: normal;}
.logo_h__txt, .logo_link { font: bold 60px/60px Istok Web;  color:#1e2731; }
.sf-menu > li > a { font: normal 13px/18px Istok Web;  color:#252d2c; }
.nav.footer-nav a { font: normal 18px/26px Istok Web;  color:#ffffff; }
</style>
<!--[if lt IE 9]>
		<div id="ie7-alert" style="width: 100%; text-align:center;">
			<img src="https://tmbhtest.com/images/ie7.jpg" alt="Upgrade IE 8" width="640" height="344" border="0" usemap="#Map" />
			<map name="Map" id="Map"><area shape="rect" coords="496,201,604,329" href="http://www.microsoft.com/windows/internet-explorer/default.aspx" target="_blank" alt="Download Interent Explorer" /><area shape="rect" coords="380,201,488,329" href="http://www.apple.com/safari/download/" target="_blank" alt="Download Apple Safari" /><area shape="rect" coords="268,202,376,330" href="http://www.opera.com/download/" target="_blank" alt="Download Opera" /><area shape="rect" coords="155,202,263,330" href="http://www.mozilla.com/" target="_blank" alt="Download Firefox" /><area shape="rect" coords="35,201,143,329" href="http://www.google.com/chrome" target="_blank" alt="Download Google Chrome" />
			</map>
		</div>
	<![endif]-->
<!--[if gte IE 9]><!-->
<script src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/js/jquery.mobile.customized.min.js" type="text/javascript"></script>
<script type="text/javascript">
			jQuery(function(){
				jQuery('.sf-menu').mobileMenu({defaultText: "Navigate to..."});
			});
		</script>
<!--<![endif]-->
<script type="text/javascript">
		// Init navigation menu
		jQuery(function(){
		// main navigation init
			jQuery('ul.sf-menu').superfish({
				delay: 1000, // the delay in milliseconds that the mouse can remain outside a sub-menu without it closing
				animation: {
					opacity: "show",
					height: "show"
				}, // used to animate the sub-menu open
				speed: "normal", // animation speed
				autoArrows: true, // generation of arrow mark-up (for submenu)
				disableHI: true // to disable hoverIntent detection
			});

		//Zoom fix
		//IPad/IPhone
			var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
				ua = navigator.userAgent,
				gestureStart = function () {
					viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0";
				},
				scaleFix = function () {
					if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
						viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
						document.addEventListener("gesturestart", gestureStart, false);
					}
				};
			scaleFix();
		})
	</script>
<!-- stick up menu -->
<script type="text/javascript">
		jQuery(document).ready(function(){
			if(!device.mobile() && !device.tablet()){
				jQuery('.header_block').tmStickUp({
					correctionSelector: jQuery('#wpadminbar')
				,	listenSelector: jQuery('.listenSelector')
				,	active: true				,	pseudo: true				});
			}
		})
	</script>
<script type="application/ld+json">{
	"@context": "http://schema.org",
	"@type": "LocalBusiness",
	"address": {"@type": "PostalAddress",
	"addressLocality": "Zalesie Górne",
	"streetAddress": "Leśna 59",
	"postalCode": "05-540",
	"addressRegion": "mazowieckie"
	},
	"name": "EL-MARK",
	"description": "Zajmujemy się sprzedaż opraw i modułów oświetlenia awaryjnego, instalacjami i pomiarami elektrycznymi, oświetleniami dróg ewakuacyjnych LED.",
	"email": "el.mark@vp.pl",
	"telephone": "605-085-529",
	"image": "https://www.el-mark.com.pl/wp-content/uploads/2014/12/logo.png"
	}</script>
</head>
<body class="error404" data-rsssl="1">
<div class="main-holder" id="motopress-main">
<!--Begin #motopress-main-->
<header class="motopress-wrapper header">
<div class="container">
<div class="row">
<div class="span12" data-motopress-id="5ffdbbf983483" data-motopress-wrapper-file="wrapper/wrapper-header.php" data-motopress-wrapper-type="header">
<div class="header_box">
<div class="row">
<div class="span9">
<div class="header_widget"><div class="visible-all-devices " id="text-7"> <div class="textwidget"><b>Uwieliny, 05-540 Zalesie Górne ,ul. Szlachecka 51</b>
<i><a href="kontakt">el.mark@vp.pl</a></i>
<em>22 750-26-44</em></div>
</div></div>
</div>
<div class="span3">
<!-- Social Links -->
<div class="social-nets-wrapper" data-motopress-static-file="static/static-social-networks.php" data-motopress-type="static">
<ul class="social">
</ul> </div>
<!-- /Social Links -->
</div>
</div>
</div>
<div class="header_block">
<div class="row">
<div class="span4" data-motopress-static-file="static/static-logo.php" data-motopress-type="static">
<!-- BEGIN LOGO -->
<div class="logo pull-left">
<a class="logo_h logo_h__img" href="https://www.el-mark.com.pl/"><img alt="El-mark" src="https://www.el-mark.com.pl/wp-content/uploads/2014/12/logo.png" title=""/></a>
</div>
<!-- END LOGO --> </div>
<div class="span8">
<div data-motopress-static-file="static/static-nav.php" data-motopress-type="static">
<!-- BEGIN MAIN NAVIGATION -->
<nav class="nav nav__primary clearfix">
<ul class="sf-menu" id="topnav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home" id="menu-item-1807"><a href="https://www.el-mark.com.pl/">Strona główna</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children" id="menu-item-2050"><a href="https://www.el-mark.com.pl/o-firmie/">O firmie</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2196"><a href="https://www.el-mark.com.pl/o-firmie/zrealizowane-inwestycje/">Zrealizowane inwestycje</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2049"><a href="https://www.el-mark.com.pl/certyfikaty/">Certyfikaty</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children" id="menu-item-2048"><a href="https://www.el-mark.com.pl/oferta-handlowa/">Oferta <br/> handlowa</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2082"><a href="https://www.el-mark.com.pl/oferta-handlowa/oswietlenie-awaryjne/">Oświetlenie awaryjne LED</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2081"><a href="https://www.el-mark.com.pl/oferta-handlowa/instalacje-elektryczne/">Instalacje elektryczne</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2079"><a href="https://www.el-mark.com.pl/oferta-handlowa/oswietlenie-awaryjne-z-monitoringiem-wireless-professional/">Oświetlenie awaryjne z monitoringiem Wireless Professional</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2077"><a href="https://www.el-mark.com.pl/oferta-handlowa/centralne-baterie/">Centralne baterie</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2047"><a href="https://www.el-mark.com.pl/sprawdz-cenniki/">Sprawdź <br/>  cenniki</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-1804"><a href="https://www.el-mark.com.pl/kontakt/">Kontakt</a></li>
</ul></nav><!-- END MAIN NAVIGATION --> </div>
<div class="hidden-phone" data-motopress-static-file="static/static-search.php" data-motopress-type="static">
<!-- BEGIN SEARCH FORM -->
<!-- END SEARCH FORM --> </div>
</div>
</div>
</div> </div>
</div>
</div>
</header>
<div class="motopress-wrapper content-holder clearfix">
<div class="container">
<div class="row">
<div class="span12" data-motopress-wrapper-file="404.php" data-motopress-wrapper-type="content">
<div class="row error404-holder">
<div class="span7 error404-holder_num" data-motopress-static-file="static/static-404.php" data-motopress-type="static">
						404					</div>
<div class="span5" data-motopress-static-file="static/static-not-found.php" data-motopress-type="static">
<div class="hgroup_404">
<h1>Sorry!</h1> <h2>Page Not Found</h2></div>
<h4>The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</h4><p>Please try using our search box below to look for information on the internet.</p>
<div class="search-form">
<form accept-charset="utf-8" action="https://www.el-mark.com.pl" id="searchform" method="get">
<input class="search-form_it" id="s" name="s" type="text" value=""/>
<input class="search-form_is btn btn-primary" id="search-submit" type="submit" value="search"/>
</form>
</div> </div>
</div>
</div>
</div>
</div>
</div>
<footer class="motopress-wrapper footer">
<div class="container">
<div class="row">
<div class="span12" data-motopress-id="5ffdbbf984e4e" data-motopress-wrapper-file="wrapper/wrapper-footer.php" data-motopress-wrapper-type="footer">
<div class="footer-widgets">
<div class="row">
<div class="span4" data-motopress-sidebar-id="footer-sidebar-1" data-motopress-type="dynamic-sidebar">
<div class="block_1"><div class="visible-all-devices " id="text-8"><h4>El-mark</h4> <div class="textwidget">Uwieliny<br/> 05-540 Zalesie Górne <br/> ul. Szlachecka 51</div>
</div></div>
</div>
<div class="span4" data-motopress-sidebar-id="footer-sidebar-2" data-motopress-type="dynamic-sidebar">
<div class="block_2"><div class="visible-all-devices " id="text-9"><h4>Zadzwoń do nas:</h4> <div class="textwidget"><strong><em>605-085-529</em>Info Serwis:</strong>
<strong><em>22 750-26-44</em>Biuro:</strong>
<strong><em>664-707-244</em>Elektryk:</strong></div>
</div></div>
</div>
<div class="span4" data-motopress-sidebar-id="footer-sidebar-3" data-motopress-type="dynamic-sidebar">
<a class="icon_contact" href="/kontakt"><img src="https://www.el-mark.com.pl/wp-content/themes/theme50494/images/icon_3.png"/></a>
<div class="block_3"><div class="visible-all-devices " id="text-10"><h4>Wyślij wiadomość:</h4> <div class="textwidget"><a href="https://www.el-mark.com.pl/kontakt">el.mark@vp.pl</a></div>
</div></div>
</div>
</div>
</div>
<div class="copyright">
<div class="row">
<div class="span12" data-motopress-static-file="static/static-footer-text.php" data-motopress-type="static">
<div class="footer-text" id="footer-text">
	
		
		© 2021 
	
		
</div> </div>
</div>
<!-- Social Links -->
<div class="social-nets-wrapper" data-motopress-static-file="static/static-social-networks.php" data-motopress-type="static">
<ul class="social">
</ul> </div>
<!-- /Social Links -->
<div class="row">
<div class="span12" data-motopress-static-file="static/static-footer-nav.php" data-motopress-type="static">
</div>
</div>
</div> </div>
</div>
</div>
</footer>
<!--End #motopress-main-->
</div>
<div class="visible-desktop" id="back-top-wrapper">
<p id="back-top">
<a href="#top"><span></span></a> </p>
</div>
<script id="flexslider-js" src="https://www.el-mark.com.pl/wp-content/plugins/cherry-plugin/lib/js/FlexSlider/jquery.flexslider-min.js?ver=2.2.2" type="text/javascript"></script>
<script id="cherry-plugin-js-extra" type="text/javascript">
/* <![CDATA[ */
var items_custom = [[0,1],[480,2],[768,3],[980,4],[1170,5]];
/* ]]> */
</script>
<script id="cherry-plugin-js" src="https://www.el-mark.com.pl/wp-content/plugins/cherry-plugin/includes/js/cherry-plugin.js?ver=1.2.8.2" type="text/javascript"></script>
<script id="cherry-parallax-js" src="https://www.el-mark.com.pl/wp-content/plugins/cherry-parallax/js/cherry.parallax.js?ver=1.0" type="text/javascript"></script>
<script id="device-check-js" src="https://www.el-mark.com.pl/wp-content/plugins/cherry-parallax/js/device.min.js?ver=1.0.0" type="text/javascript"></script>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.el-mark.com.pl\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://www.el-mark.com.pl/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.1" type="text/javascript"></script>
<script id="chrome-smoothing-scroll-js" src="https://www.el-mark.com.pl/wp-content/themes/theme50494/js/smoothing-scroll.js?ver=1.0" type="text/javascript"></script>
<script id="superfish-js" src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/js/superfish.js?ver=1.5.3" type="text/javascript"></script>
<script id="mobilemenu-js" src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/js/jquery.mobilemenu.js?ver=1.0" type="text/javascript"></script>
<script id="magnific-popup-js" src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/js/jquery.magnific-popup.min.js?ver=0.9.3" type="text/javascript"></script>
<script id="playlist-js" src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/js/jplayer.playlist.min.js?ver=2.3.0" type="text/javascript"></script>
<script id="jplayer-js" src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/js/jquery.jplayer.min.js?ver=2.6.0" type="text/javascript"></script>
<script id="tmstickup-js" src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/js/tmstickup.js?ver=1.0.0" type="text/javascript"></script>
<script id="device-js" src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/js/device.min.js?ver=1.0.0" type="text/javascript"></script>
<script id="zaccordion-js" src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/js/jquery.zaccordion.min.js?ver=2.1.0" type="text/javascript"></script>
<script id="camera-js" src="https://www.el-mark.com.pl/wp-content/themes/CherryFramework/js/camera.min.js?ver=1.3.4" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.el-mark.com.pl/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
<script type="text/javascript">
				deleteCookie('cf-cookie-banner');
			</script>
<!-- this is used by many Wordpress features and for plugins to work properly -->
</body>
</html>