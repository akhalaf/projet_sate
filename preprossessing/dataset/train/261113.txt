<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]--><!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]--><!--[if IE 8]><html class="lt-ie9" lang="en" dir="ltr"><![endif]--><!--[if gt IE 8]><!--><html dir="ltr" lang="en" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head>
<meta charset="utf-8"/>
<link href="https://www.brooklineeventcenter.com/rss.xml" rel="alternate" title="Brookline Event Center and Auction Gallery - Events and Auctions in Brookline N.H. RSS" type="application/rss+xml"/>
<link href="https://www.brooklineeventcenter.com/sites/default/files/brookline_ico.jpg" rel="shortcut icon" type="image/jpeg"/>
<meta content="Drupal 7 (https://www.drupal.org)" name="generator"/>
<link href="https://www.brooklineeventcenter.com/" rel="canonical"/>
<link href="https://www.brooklineeventcenter.com/" rel="shortlink"/>
<title>Brookline Event Center and Auction Gallery - Events and Auctions in Brookline N.H. |</title>
<link href="https://www.brooklineeventcenter.com/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.brooklineeventcenter.com/sites/default/files/css/css_y7g5fl6ewcrYi8CNa0GXrWi0NUQYiXj2ofF_labMO9Q.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.brooklineeventcenter.com/sites/default/files/css/css_52u1u54ROfKz5yyy-c6cruumEClZFKUrRP3xiz888XQ.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.brooklineeventcenter.com/sites/default/files/css/css_jN8H1Qg3WK5dNDEPKvgCRKmD7GdT9q3KXoipKDNZIt4.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.brooklineeventcenter.com/sites/default/files/css/css_iaIHD6b_Q1yrGIc0iiU81WW_NPa5cTmaImYSCXJI55c.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.brooklineeventcenter.com/sites/default/files/css/css_HFOy16fIyr8bXM_l5NHFVp8l9FNRdmBHvZtxZ9nVg70.css" media="only screen" rel="stylesheet" type="text/css"/>
<link href="https://www.brooklineeventcenter.com/sites/default/files/css/css_nyhz1Nuu5DVZqcgDUZlJm_CoVpZrbb20pkPIUxui7cg.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.brooklineeventcenter.com/sites/default/files/css/css_4zsJ2F2Zhgs5-XLIesMzycwCCgwt9wrTIzvXBABWTRY.css" media="only screen" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link type="text/css" rel="stylesheet" href="https://www.brooklineeventcenter.com/sites/default/files/css/css_mzfLeQbN2v8foWx1N5q90GVZIBMNctasiTggBiqop88.css" media="screen" />
<![endif]-->
<link href="https://www.brooklineeventcenter.com/sites/default/files/css/css_Bc7X39saBQQdtRHdWknjJhAaC7rGrguQiFohzT77j2U.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.brooklineeventcenter.com/sites/default/files/css_injector/css_injector_2.css?qgszu9" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.brooklineeventcenter.com/sites/default/files/js/js_VecHkdFFzHmI10lNWW0NMmhQ47_3u8gBu9iBjil2vAY.js"></script>
<script src="https://www.brooklineeventcenter.com/sites/default/files/js/js_udkOcakdctKFPUZBHPFyWS631LPR38lZByz2pepiBKQ.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"pixture_reloaded","theme_token":"jOv3N6rd9Re3p8jdH1wSO9j743G0NT4CkMbd0n6suj8","js":{"sites\/default\/modules\/flexslider\/assets\/js\/flexslider.load.js":1,"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery-html-prefilter-3.5.0-backport.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_load.js":1,"sites\/all\/libraries\/flexslider\/jquery.flexslider-min.js":1,"sites\/all\/libraries\/superfish\/jquery.hoverIntent.minified.js":1,"sites\/all\/libraries\/superfish\/sfautomaticwidth.js":1,"sites\/all\/libraries\/superfish\/sftouchscreen.js":1,"sites\/all\/libraries\/superfish\/sfsmallscreen.js":1,"sites\/all\/libraries\/superfish\/superfish.js":1,"sites\/all\/libraries\/superfish\/supersubs.js":1,"sites\/all\/modules\/superfish\/superfish.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/calendar\/css\/calendar_multiday.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_repeat_field\/date_repeat_field.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"sites\/default\/modules\/ubercart\/uc_order\/uc_order.css":1,"sites\/default\/modules\/ubercart\/uc_product\/uc_product.css":1,"sites\/default\/modules\/ubercart\/uc_store\/uc_store.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/print\/print_ui\/css\/print_ui.theme.css":1,"sites\/default\/modules\/flexslider\/assets\/css\/flexslider_img.css":1,"sites\/all\/libraries\/flexslider\/flexslider.css":1,"sites\/all\/libraries\/superfish\/css\/superfish.css":1,"sites\/all\/libraries\/superfish\/style\/blue.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.image.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.floatblocks.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.layout.css":1,"sites\/default\/themes\/pixture_reloaded\/color\/colors.css":1,"sites\/default\/themes\/pixture_reloaded\/css\/pixture_reloaded.css":1,"sites\/default\/themes\/pixture_reloaded\/css\/pixture_reloaded.settings.style.css":1,"sites\/default\/themes\/pixture_reloaded\/css\/local.css":1,"public:\/\/adaptivetheme\/pixture_reloaded_files\/pixture_reloaded.responsive.layout.css":1,"public:\/\/adaptivetheme\/pixture_reloaded_files\/pixture_reloaded.fonts.css":1,"public:\/\/adaptivetheme\/pixture_reloaded_files\/pixture_reloaded.responsive.styles.css":1,"public:\/\/adaptivetheme\/pixture_reloaded_files\/pixture_reloaded.lt-ie9.layout.css":1,"sites\/default\/files\/css_injector\/css_injector_1.css":1,"sites\/default\/files\/css_injector\/css_injector_2.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"urlIsAjaxTrusted":{"\/search\/node":true},"superfish":{"1":{"id":"1","sf":{"animation":{"opacity":"show","width":"show"},"speed":"fast"},"plugins":{"automaticwidth":true,"touchscreen":{"mode":"window_width","breakpointUnit":"px"},"smallscreen":{"mode":"window_width","breakpointUnit":"px","type":"select","title":"Main menu"},"supersubs":{"minWidth":"15","maxWidth":"30"}}}},"flexslider":{"optionsets":{"default":{"namespace":"flex-","selector":".slides \u003E li","easing":"swing","direction":"horizontal","reverse":false,"smoothHeight":false,"startAt":0,"animationSpeed":600,"initDelay":0,"useCSS":true,"touch":true,"video":false,"keyboard":true,"multipleKeyboard":false,"mousewheel":0,"controlsContainer":".flex-control-nav-container","sync":"","asNavFor":"","itemWidth":0,"itemMargin":0,"minItems":0,"maxItems":0,"move":0,"animation":"fade","slideshow":true,"slideshowSpeed":"7000","directionNav":true,"controlNav":false,"prevText":"Previous","nextText":"Next","pausePlay":false,"pauseText":"Pause","playText":"Play","randomize":false,"thumbCaptions":false,"thumbCaptionsBoth":false,"animationLoop":true,"pauseOnAction":true,"pauseOnHover":false,"manualControls":""}},"instances":{"flexslider-1":"default"}},"adaptivetheme":{"pixture_reloaded":{"layout_settings":{"bigscreen":"three-col-grail","tablet_landscape":"three-col-grail","tablet_portrait":"one-col-stack","smalltouch_landscape":"one-col-stack","smalltouch_portrait":"one-col-stack"},"media_query_settings":{"bigscreen":"only screen and (min-width:900px)","tablet_landscape":"only screen and (min-width:769px) and (max-width:899px)","tablet_portrait":"only screen and (min-width:481px) and (max-width:768px)","smalltouch_landscape":"only screen and (min-width:321px) and (max-width:480px)","smalltouch_portrait":"only screen and (max-width:320px)"}}}});</script>
</head>
<body class="html front not-logged-in one-sidebar sidebar-second page-node site-name-hidden atr-7.x-3.x atv-7.x-3.0-rc1 site-name-brookline-event-center-and-auction-gallery---events-and-auctions-in-brookline-nh color-scheme-custom pixture-reloaded bs-l bb-n mb-dd mbp-l rc-0">
<div class="nocontent" id="skip-link">
<a class="element-invisible element-focusable" href="#main-content">Skip to main content</a>
</div>
<div class="texture-overlay">
<div class="container page" id="page">
<header class="clearfix" id="header" role="banner">
<div class="header-inner clearfix">
<div class="clearfix" id="headerimage"> <img src="/sites/default/themes/pixture_reloaded/header-images//hi1.jpg"/> </div>
</div>
</header> <!-- /header -->
<div class="nav clearfix" id="menu-bar"><nav class="block block-superfish no-title menu-wrapper menu-bar-wrapper clearfix odd first last block-count-1 block-region-menu-bar block-1" id="block-superfish-1">
<ul class="menu sf-menu sf-main-menu sf-horizontal sf-style-blue sf-total-items-9 sf-parent-items-0 sf-single-items-9" id="superfish-1"><li class="active-trail first odd sf-item-1 sf-depth-1 sf-no-children" id="menu-2107-1"><a class="sf-depth-1 active" href="/" title="">Home</a></li><li class="middle even sf-item-2 sf-depth-1 sf-no-children" id="menu-2713-1"><a class="sf-depth-1" href="/calendar-node-field-date">Calendar</a></li><li class="middle odd sf-item-3 sf-depth-1 sf-no-children" id="menu-2496-1"><a class="sf-depth-1" href="/content/center-details-and-rates">Details and Rates</a></li><li class="middle even sf-item-4 sf-depth-1 sf-no-children" id="menu-2498-1"><a class="sf-depth-1" href="/content/directions">Directions</a></li><li class="middle odd sf-item-5 sf-depth-1 sf-no-children" id="menu-2499-1"><a class="sf-depth-1" href="/content/party-tents">Party Tents</a></li><li class="middle even sf-item-6 sf-depth-1 sf-no-children" id="menu-2791-1"><a class="sf-depth-1" href="/content/wedding-chapel">Wedding Chapel</a></li><li class="middle odd sf-item-7 sf-depth-1 sf-no-children" id="menu-3144-1"><a class="sf-depth-1" href="/auctions" title="">Auctions</a></li><li class="middle even sf-item-8 sf-depth-1 sf-no-children" id="menu-2102-1"><a class="sf-depth-1" href="/contact" title="">Contact</a></li><li class="last odd sf-item-9 sf-depth-1 sf-no-children" id="menu-2860-1"><a class="sf-depth-1" href="/content/pay-now">Pay Now</a></li></ul>
</nav></div>
<!-- Messages and Help -->
<!-- Breadcrumbs -->
<!-- Two column 2x50 -->
<div class="at-panel gpanel panel-display two-50 clearfix">
<div class="region region-two-50-first"><div class="region-inner clearfix"><div class="block block-nodeblock no-title odd first last block-count-2 block-region-two-50-first block-87" id="block-nodeblock-87"><div class="block-inner clearfix">
<div class="block-content content"><div about="/content/hps" class="node node-hp-slideshow-image article odd node-full 1 ia-r clearfix" id="node-87" role="article" typeof="sioc:Item foaf:Document">
<span class="rdf-meta element-hidden" content="HPS" property="dc:title"></span><span class="rdf-meta element-hidden" content="0" datatype="xsd:integer" property="sioc:num_replies"></span>
<div class="content">
<div class="field field-name-field-image field-type-image field-label-hidden view-mode-full"><div class="field-items"><figure class="clearfix field-item even"><div class="flexslider optionset-default" id="flexslider-1">
<ul class="slides"><li><img alt="" class="image-style-none" height="240" src="https://www.brooklineeventcenter.com/sites/default/files/home_images/Hall%20Front%202.JPG" title="Brookline Event Center and Auction Gallery" typeof="foaf:Image" width="411"/><div class="flex-caption">Brookline Event Center and Auction Gallery</div></li>
<li><img alt="" class="image-style-none" height="240" src="https://www.brooklineeventcenter.com/sites/default/files/Outdoor%20ceremony%20area%201.jpg" title="Outdoor Picture Area" typeof="foaf:Image" width="411"/><div class="flex-caption">Outdoor Picture Area</div></li>
<li><img alt="" class="image-style-none" height="210" src="https://www.brooklineeventcenter.com/sites/default/files/New%20outdoor%20picture%20area.tickets.jpg" title="Outdoor Picture Area" typeof="foaf:Image" width="360"/><div class="flex-caption">Outdoor Picture Area</div></li>
<li><img alt="" class="image-style-none" height="240" src="https://www.brooklineeventcenter.com/sites/default/files/home_images/wedding.jpg" title="Wedding Reception" typeof="foaf:Image" width="408"/><div class="flex-caption">Wedding Reception</div></li>
<li><img alt="" class="image-style-none" height="240" src="https://www.brooklineeventcenter.com/sites/default/files/home_images/Football%201.JPG" title="Football Party" typeof="foaf:Image" width="411"/><div class="flex-caption">Football Party</div></li>
<li><img alt="" class="image-style-none" height="210" src="https://www.brooklineeventcenter.com/sites/default/files/home_images/Boat%202.tickets.jpg" title="Boat Show" typeof="foaf:Image" width="360"/><div class="flex-caption">Boat Show</div></li>
<li><img alt="" class="image-style-none" height="240" src="https://www.brooklineeventcenter.com/sites/default/files/home_images/Tom%20Dixon%20Band%202.JPG" title="Tom Dixon Band" typeof="foaf:Image" width="411"/><div class="flex-caption">Tom Dixon Band</div></li>
<li><img alt="" class="image-style-none" height="240" src="https://www.brooklineeventcenter.com/sites/default/files/web1_0.JPG" title="Wedding reception" typeof="foaf:Image" width="414"/><div class="flex-caption">Wedding reception</div></li>
</ul></div>
</figure></div></div> </div>
</div>
</div>
</div></div></div></div> <div class="region region-two-50-second"><div class="region-inner clearfix"><div class="block block-nodeblock no-title odd first last block-count-3 block-region-two-50-second block-88" id="block-nodeblock-88"><div class="block-inner clearfix">
<div class="block-content content"><div about="/content/brookline-event-center-auction-gallery" class="node node-nodeblock article even node-full 1 ia-r clearfix" id="node-88" role="article" typeof="sioc:Item foaf:Document">
<span class="rdf-meta element-hidden" content="Brookline Event Center &amp; Auction Gallery" property="dc:title"></span><span class="rdf-meta element-hidden" content="0" datatype="xsd:integer" property="sioc:num_replies"></span>
<div class="content">
<div class="field field-name-body field-type-text-with-summary field-label-hidden view-mode-full"><div class="field-items"><div class="field-item even" property="content:encoded"><h3>Brookline Event Center and Auction Gallery</h3>
<p>Our Brookline, N.H. facility, is only  ten miles west of Nashua NH, &amp; provides a clean and spacious hall for your: wedding, reception, banquet, party, dance, general function or auction.</p>
<p>We take great pleasure in being able to provide the site for the perfect events. We are pleased to offer very low and affordable rates for all events, and we are here to brainstorm and provide support to ensure that your event runs smoothly.</p>
<p>Download our flyer.  Email or call all us at 603-673-4474, and let us help you make your function a success.</p>
<h3>Join us for auctions and functions!</h3>
</div></div></div> </div>
</div>
</div>
</div></div></div></div> </div>
<!-- four-4x25 Gpanel 
      -->
<div id="columns">
<div class="columns-inner clearfix">
<div id="content-column">
<div class="content-inner">
<div id="main-content" role="main">
<div id="content">
<div class="block block-system no-title odd first block-count-4 block-region-content block-main" id="block-system-main">
<article about="/content/winter-antique-show-flea-market" class="node node-date4 node-promoted node-teaser article odd iat-n clearfix" id="node-706" role="article" typeof="sioc:Item foaf:Document">
<header class="node-header">
<h1 class="node-title">
<a href="/content/winter-antique-show-flea-market" rel="bookmark">Winter Antique show &amp; Flea Market</a>
</h1>
</header>
<div class="node-content">
<div class="field field-name-field-date field-type-datetime field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even"><span class="date-display-single" content="2020-12-06T07:00:00-05:00" datatype="xsd:dateTime" property="dc:date">Sun 12/6 7:00am</span></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even" property="content:encoded"><p><strong> </strong></p>
<p>Our next winter Antique show &amp; Flea market will be Sunday December 13.</p>
<p>Doors open to the Public at 7Am.</p>
<p>Early buying is at 5:30 AM. $20.00 entry fee for Early buying. Free at 7am</p>
<p>Free Parking. Food available.</p>
<p> </p>
<p>Our next Auction is Saturday January 9 @ 10: 30 AM.</p>
<p>  <span class="read-more-link"><a class="read-more" href="/content/winter-antique-show-flea-market" rel="tag" title="Winter Antique show &amp; Flea Market">Read more<span class="element-invisible"> about Winter Antique show &amp; Flea Market</span></a></span></p></div></div></div> </div>
<span class="rdf-meta element-hidden" content="Winter Antique show &amp; Flea Market" property="dc:title"></span><span class="rdf-meta element-hidden" content="0" datatype="xsd:integer" property="sioc:num_replies"></span></article>
</div><div class="block block-nodeblock no-title even last block-count-5 block-region-content block-354" id="block-nodeblock-354">
<div about="/node/354" class="node node-nodeblock article even node-full 1 ia-r clearfix" id="node-354" role="article" typeof="sioc:Item foaf:Document">
<span class="rdf-meta element-hidden" content="Auction Zip" property="dc:title"></span><span class="rdf-meta element-hidden" content="0" datatype="xsd:integer" property="sioc:num_replies"></span>
<div class="content">
<div class="field field-name-body field-type-text-with-summary field-label-hidden view-mode-full"><div class="field-items"><div class="field-item even" property="content:encoded"><script type="text/javascript">
<!--//--><![CDATA[// ><!--

// <![CDATA[
var az_feed_uid=192;
var az_feed=129;
// ]]]]><![CDATA[>

//--><!]]>
</script><script src="http://www.auctionzip.com/includes/xmlfeed.js" type="text/javascript"></script></div></div></div> </div>
</div>
</div> </div>
<!-- Feed icons (RSS, Atom icons etc -->
<a class="feed-icon" href="/rss.xml" title="Subscribe to Brookline Event Center and Auction Gallery - Events and Auctions in Brookline N.H. RSS"><img alt="Subscribe to Brookline Event Center and Auction Gallery - Events and Auctions in Brookline N.H. RSS" class="image-style-none" height="16" src="https://www.brooklineeventcenter.com/misc/feed.png" typeof="foaf:Image" width="16"/></a>
</div> <!-- /main-content -->
</div>
</div> <!-- /content-column -->
<div class="region region-sidebar-second sidebar"><div class="region-inner clearfix"><section class="block block-nodeblock odd first block-count-6 block-region-sidebar-second block-232" id="block-nodeblock-232"><div class="block-inner clearfix">
<h2 class="block-title">Pay Now</h2>
<div class="block-content content"><div about="/content/pay-now" class="node node-nodeblock article odd node-full 1 ia-r clearfix" id="node-232" role="article" typeof="sioc:Item foaf:Document">
<span class="rdf-meta element-hidden" content="Pay Now" property="dc:title"></span><span class="rdf-meta element-hidden" content="0" datatype="xsd:integer" property="sioc:num_replies"></span>
<div class="content">
<div class="field field-name-body field-type-text-with-summary field-label-hidden view-mode-full"><div class="field-items"><div class="field-item even" property="content:encoded"><form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input name="cmd" type="hidden" value="_s-xclick"/><input name="hosted_button_id" type="hidden" value="ZTMB7UWGFT29E"/><table><tr><td><input name="on0" type="hidden" value="Payment Description"/>Payment Description</td></tr><tr><td><input maxlength="200" name="os0" type="text"/></td></tr></table><input alt="PayPal - The safer, easier way to pay online!" border="0" name="submit" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" type="image"/><img alt="" border="0" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1"/></form>
</div></div></div> </div>
</div>
</div>
</div></section><section class="block block-views even block-count-7 block-region-sidebar-second block-calendar-2-block-2" id="block-views-calendar-2-block-2"><div class="block-inner clearfix">
<h2 class="block-title">Events</h2>
<div class="block-content content"><div class="view view-calendar-2 view-id-calendar_2 view-display-id-block_2 view-dom-id-7b3231cb47f81103d154a89c96f5968c">
<div class="view-content">
<div class="item-list"> <ul> <li class="">
<div class="views-field views-field-title"> <span class="field-content"><a href="/content/winter-antique-show-flea-market-0">Winter Antique Show &amp; Flea Market</a></span> </div>
<div class="views-field views-field-field-date"> <div class="field-content"><span class="date-display-single" content="2021-01-17T07:00:00-05:00" datatype="xsd:dateTime" property="dc:date">Sun 1/17 7:00am</span></div> </div></li>
<li class="">
<div class="views-field views-field-title"> <span class="field-content"><a href="/content/winter-antique-show-flea-market-0">Winter Antique Show &amp; Flea Market</a></span> </div>
<div class="views-field views-field-field-date"> <div class="field-content"><span class="date-display-single" content="2021-01-24T07:00:00-05:00" datatype="xsd:dateTime" property="dc:date">Sun 1/24 7:00am</span></div> </div></li>
<li class="">
<div class="views-field views-field-title"> <span class="field-content"><a href="/content/winter-antique-show-flea-market-0">Winter Antique Show &amp; Flea Market</a></span> </div>
<div class="views-field views-field-field-date"> <div class="field-content"><span class="date-display-single" content="2021-01-31T07:00:00-05:00" datatype="xsd:dateTime" property="dc:date">Sun 1/31 7:00am</span></div> </div></li>
<li class="">
<div class="views-field views-field-title"> <span class="field-content"><a href="/content/winter-antique-show-flea-market-0">Winter Antique Show &amp; Flea Market</a></span> </div>
<div class="views-field views-field-field-date"> <div class="field-content"><span class="date-display-single" content="2021-02-07T07:00:00-05:00" datatype="xsd:dateTime" property="dc:date">Sun 2/7 7:00am</span></div> </div></li>
<li class="">
<div class="views-field views-field-title"> <span class="field-content"><a href="/content/winter-antique-show-flea-market-0">Winter Antique Show &amp; Flea Market</a></span> </div>
<div class="views-field views-field-field-date"> <div class="field-content"><span class="date-display-single" content="2021-02-14T07:00:00-05:00" datatype="xsd:dateTime" property="dc:date">Sun 2/14 7:00am</span></div> </div></li>
</ul></div> </div>
<div class="more-link">
<a href="/calendar-node-field-date/month">
    more  </a>
</div>
</div></div>
</div></section><section class="block block-views odd last block-count-8 block-region-sidebar-second block-calendar-2-block-3" id="block-views-calendar-2-block-3"><div class="block-inner clearfix">
<h2 class="block-title">Auctions</h2>
<div class="block-content content"><div class="view view-calendar-2 view-id-calendar_2 view-display-id-block_3 view-dom-id-857608c63271d364073b546e8f2f6b7f">
<div class="view-content">
<div class="item-list"> <ul> <li class="">
<div class="views-field views-field-title"> <span class="field-content"><a href="/auction/first-year-antique-auction">First of the Year Antique Auction</a></span> </div>
<div class="views-field views-field-field-date-preview"> <div class="field-content">Preview: <span class="date-display-single">Wed 1/8/20 <span class="date-display-range"><span class="date-display-start" content="2020-01-08T12:00:00-05:00" datatype="xsd:dateTime" property="dc:date">12:00pm</span> to <span class="date-display-end" content="2020-01-08T19:00:00-05:00" datatype="xsd:dateTime" property="dc:date">7:00pm</span></span></span>, Preview: <span class="date-display-single" content="2021-01-09T08:00:00-05:00" datatype="xsd:dateTime" property="dc:date">Sat 1/9/21 8:00am</span></div> </div>
<div class="views-field views-field-field-auction"> <div class="field-content">Auction: <span class="date-display-single" content="2021-01-09T10:30:00-05:00" datatype="xsd:dateTime" property="dc:date">Sat 1/9/21 10:30am</span></div> </div></li>
<li class="">
<div class="views-field views-field-title"> <span class="field-content"><a href="/auction/antique-auction">Antique Auction</a></span> </div>
<div class="views-field views-field-field-date-preview"> <div class="field-content">Preview: <span class="date-display-single">Fri 1/8/21 <span class="date-display-range"><span class="date-display-start" content="2021-01-08T12:00:00-05:00" datatype="xsd:dateTime" property="dc:date">12:00pm</span> to <span class="date-display-end" content="2021-01-08T19:00:00-05:00" datatype="xsd:dateTime" property="dc:date">7:00pm</span></span></span></div> </div>
<div class="views-field views-field-field-auction"> <div class="field-content">Auction: <span class="date-display-single" content="2020-01-09T10:30:00-05:00" datatype="xsd:dateTime" property="dc:date">Thu 1/9/20 10:30am</span></div> </div></li>
</ul></div> </div>
</div></div>
</div></section></div></div>
</div>
</div> <!-- /columns -->
<footer id="footer" role="contentinfo">
<div class="clearfix" id="footer-inner">
<div class="region region-footer"><div class="region-inner clearfix"><div class="block block-nodeblock no-title odd first last block-count-9 block-region-footer block-80" id="block-nodeblock-80"><div class="block-inner clearfix">
<div class="block-content content"><div about="/content/footer" class="node node-nodeblock article even node-full 1 ia-r clearfix" id="node-80" role="article" typeof="sioc:Item foaf:Document">
<span class="rdf-meta element-hidden" content="Footer" property="dc:title"></span><span class="rdf-meta element-hidden" content="0" datatype="xsd:integer" property="sioc:num_replies"></span>
<div class="content">
<div class="field field-name-body field-type-text-with-summary field-label-hidden view-mode-full"><div class="field-items"><div class="field-item even" property="content:encoded"><p style="text-align: center;">Brookline Event Center and Auction Gallery    32 Proctor Hill Rd, Rte 130      Brookline, NH 03033      603.673.4474      <a href="/contact">contact</a>          <a href="https://www.facebook.com/pages/Brookline-Event-Center/151659054883546"><img alt="" src="/sites/default/files/facebook.jpg"/></a></p>
</div></div></div> </div>
</div>
</div>
</div></div></div></div> </div>
</footer>
</div> <div class="bb container"><a href="/user">staff</a> | site by<a href="http://www.bluebassdesign.com">Blue Bass Design</a></div><!-- /page -->
</div> <!-- /texture overlay -->
<script src="https://www.brooklineeventcenter.com/sites/default/files/js/js_uTpGZRbRZm_lrt5640lI88hN-6jGIe3E3hxZcagIuss.js"></script>
</body>
</html>
