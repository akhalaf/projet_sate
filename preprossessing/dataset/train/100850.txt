<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<link href="https://www.alllaw.com" rel="canonical"/>
<link href="/themes/default/favicon/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/themes/default/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/themes/default/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/themes/default/favicon/manifest.json" rel="manifest"/>
<link color="#01314c" href="/themes/default/favicon/safari-pinned-tab.svg" rel="mask-icon"/>
<link href="/themes/default/favicon/favicon.ico" rel="shortcut icon"/>
<meta content="#2b5797" name="msapplication-TileColor"/>
<meta content="/themes/default/favicon/mstile-144x144.png" name="msapplication-TileImage"/>
<meta content="/themes/default/favicon/browserconfig.xml" name="msapplication-config"/>
<meta content="#01314c" name="theme-color"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<meta content="telephone=no" name="format-detection"/>
<meta content="AllLaw.com. Laws &amp; Legal Information. Legal Forms. Lawyers." name="title"/>
<meta content="AllLaw.com. Laws &amp; Legal Information. Legal Forms. Lawyers." name="description"/>
<meta content="AllLaw.com. Laws &amp; Legal Information. Legal Forms. Lawyers." property="og:title"/>
<meta content="" property="og:image"/>
<meta content="https://www.alllaw.com" property="og:url"/>
<meta content="website" property="og:type"/>
<meta content="www.alllaw.com" property="og:site_name"/>
<title>AllLaw.com. Laws &amp; Legal Information. Legal Forms. Lawyers. | AllLaw</title>
<script crossorigin="anonymous" src="https://polyfill.io/v3/polyfill.min.js?features=IntersectionObserver"></script>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link crossorigin="" href="https://fonts.gstatic.com/" rel="preconnect"/>
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800&amp;display=swap&amp;ext=.css" media="screen, print" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons&amp;ext=.css" media="screen, print" rel="stylesheet" type="text/css"/>
<link href="/themes/quasar/min/273109f97bb0d0d3ab95024473e885ea.css" media="screen, print" rel="stylesheet" type="text/css"/>
<script src="/themes/quasar/min/ce644ffca97a5f3dd177a2420425165a.js" type="text/javascript"></script>
<script type="text/javascript">
    var NCMS = new noloGlobal({"aopId":0,"aopName":"undefined","device":{"type":"desktop","isMobile":false,"isTablet":false,"isDesktop":true,"isBot":true},"pageType":"front","theme":"quasar"});
</script>
<script type="text/javascript">
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-4757830-25', 'alllaw.com');
        ga('send', 'pageview');
</script>
</head><body><div id="consent_blackbar"></div>
<script async="async" src="//consent.trustarc.com/notice?domain=internetbrands.com&amp;c=teconsent&amp;js=nj&amp;noticeType=bb&amp;text=true&amp;cookieLink=http%3A%2F%2Fwww.internetbrands.com%2Fprivacy%2Fprivacy-main.html%23CaliforniaPrivacyRights&amp;privacypolicylink=https%3A%2F%2Fwww.internetbrands.com%2Fprivacy%2Fprivacy-main.html"></script>
<script type="text/javascript">
   // Facebook Universal Tracking Code
   !function(f,b,e,v,n,t,s)
   {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};
   if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
   n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];
   s.parentNode.insertBefore(t,s)}(window,document,'script',
   'https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '344858165663318');
   fbq('track', 'PageView');
</script>
<noscript>
<img height="1" src="https://www.facebook.com/tr?id=344858165663318&amp;ev=PageView&amp;noscript=1" width="1"/>
</noscript>
<script>
// ===== Adobe Target Property Parameter =====
function targetPageParams() { return { "at_property": "7cbea1d3-22b5-9a21-eea4-24bcb6535b29" }; }

(function(g,b,d,f){(function(a,c,d){if(a){var e=b.createElement("style");e.id=c;e.innerHTML=d;a.appendChild(e)}})(b.getElementsByTagName("head")[0],"at-body-style",d);setTimeout(function(){var a=b.getElementsByTagName("head")[0];if(a){var c=b.getElementById("at-body-style");c&&a.removeChild(c)}},f)})(window,document,"body {opacity: 0 !important}",3000);
</script>
<script async="" src="https://www.nolo.com/files/adobe/at_modified_v2.js"></script>
<div class="region grid-region-page-container" id="page-container">
<div class="region grid-region-page" id="page">
<div class="region grid-region-page-hd" id="page-hd">
<div class="region grid-region-header" id="region-header">
<div class="top-bar">
<div class="top-bar__container">
<ul class="top-bar__nav-list">
<li class="top-bar__legal">
<a class="practice" href="https://www.nolo.com/advertisers" title="Market Your Law Firm">Market Your Law Firm</a>
</li>
<li class="top-bar__legal">
<a class="directory" href="https://www.nolo.com/lawyers" title="Lawyer Directory">Lawyer Directory</a>
</li>
</ul>
</div>
</div>
<div class="container">
<nav class="navbar navbar-expand-lg navbar-light static-top">
<div class="navbar-column">
<a class="navbar-brand" href="/">
<img alt="AllLaw" height="44" src="/themes/quasar/images/logos/vector/all_law.svg"/>
</a>
<button aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarResponsive" data-toggle="collapse" type="button">
<i class="material-icons">menu</i>
<i class="material-icons" style="display: none">clear</i>
</button>
</div>
<div class="collapse navbar-collapse" id="navbarResponsive">
<ul class="navbar-nav mr-auto mt-lg-0">
<li class="nav-item active">
<a class="nav-link" href="/path2/min-path4" title="Find a Lawyer">Find a Lawyer</a>
</li>
<li class="nav-item">
<a class="nav-link" href="/topics" title="Law Topics">Law Topics</a>
</li>
</ul>
<div class="navbar__footer">
<div class="navbar__legal">
<div class="practice">
<a href="https://www.nolo.com/advertisers" title="Market Your Law Firm">Market Your Law Firm</a>
</div>
<div class="directory">
<a href="https://www.nolo.com/lawyers" title="Lawyer Directory">Lawyer Directory</a>
</div>
</div>
</div>
</div>
</nav>
</div>
<div class="ctn-phone-container switchboard-jack" style="display: none;">
<a class="ctn-phone-link switchboard-plug" href="tel:+10000000000">Call us at <span class="ctn-phone-text switchboard-line">1 (000) 000-0000</span></a>
</div>
<div aria-expanded="true" class="navbar-branded-search collapse in" data-search-type="ldir" id="nolo-search-form" style="">
<div class="container">
<form action="/search2" class="search-bar search-bar--nolo" method="get">
<div class="search-bar__type dropdown dropdown--dropshadow" id="searchBarType">
<select class="form-control form-select select form-control site-search__select" name="type">
<option class="site-search__select__option" value="all">All</option>
<option class="site-search__select__option" value="product">DIY Products</option>
<option class="site-search__select__option" value="ldir">Lawyers</option>
<option class="site-search__select__option" value="article">Articles</option>
</select>
</div>
<div class="search-bar__nolo">
<input name="query" placeholder="Search Nolo Here" type="text"/>
<button class="btn-search search-icon" title="search" type="submit">
<i class="material-icons">search</i>
</button>
</div>
<div class="search-bar__lawyer" style="display: none">
<div class="issue">
<span>Issue: </span>
<i class="material-icons search-icon">search</i>
<input disabled="disabled" name="query" placeholder="e.g:Bankruptcy, etc." type="text"/>
</div>
<div class="near">
<span>Near: </span>
<input name="location" placeholder="City &amp; State or Zip Code" type="text"/>
</div>
</div>
</form>
</div>
</div></div>
</div>
<div class="region grid-region-page-bd container" id="page-bd">
<div class="content-blade-front region grid-region-content-row clearfix layout-column-1" id="region-content-row-1">
<div class="region grid-region-content-center ct" id="region-content-center-1">
<div class="region block-region grid-region-content-top " id="region-content-top">
<div class="block block-331" data-controller="HomePageSlider" data-display-desktop="true" data-display-mobile="true" data-display-tablet="true" data-name="Home Slider: www.alllaw.com" data-programatic="true" data-weight="3" id="block-331-content-top">
<div class="block-bd clearfix">
<div class="ncms-home-carousel-started clearfix">
<div class="curated-content-slider slider-count-4 clearfix">
<div class="slick-carousel">
<div class="carousel-image slide-1">
<a href="/articles/nolo/workers-compensation/when-should-hire-lawyer.html" title="Workers Comp: When to Lawyer Up">
<img alt="Workers Comp: When to Lawyer Up" data-lazy="//www.nolo.com/sites/default/files/curated_items/lawyer_large.jpg" title="Workers Comp: When to Lawyer Up"/>
</a>
</div>
<div class="carousel-image slide-2">
<a href="/articles/nolo/bankruptcy/when-consider-filing.html" title="Should I Consider Filing Bankruptcy?">
<img alt="Should I Consider Filing Bankruptcy?" data-lazy="//www.nolo.com/sites/default/files/curated_items/Bankruptcy_Fresh_Start.jpg" title="Should I Consider Filing Bankruptcy?"/>
</a>
</div>
<div class="carousel-image slide-3">
<a href="/articles/nolo/us-immigration/undocumented-illegal-immigrant-get-green-card-marriage-citizen-resident.html" title="Can Undocumented Immigrants Get a Green Card Through Marriage?">
<img alt="Can Undocumented Immigrants Get a Green Card Through Marriage?" data-lazy="//www.nolo.com/sites/default/files/curated_items/USCIS_Immigr_App.jpg" title="Can Undocumented Immigrants Get a Green Card Through Marriage?"/>
</a>
</div>
<div class="carousel-image slide-4">
<a href="/articles/nolo/personal-injury/accidents-claim-settlements-faq.html" title="Personal Injury Settlements: FAQs">
<img alt="Personal Injury Settlements: FAQs" data-lazy="//www.nolo.com/sites/default/files/curated_items/legal_money.jpg" title="Personal Injury Settlements: FAQs"/>
</a>
</div>
</div>
<ol class="slick-tabs">
<li class="carousel-link slide-1 active ">
<a class="slide-url" href="/articles/nolo/workers-compensation/when-should-hire-lawyer.html" title="Workers Comp: When to Lawyer Up">
<h4 class="slick-tab-title">Workers Comp: When to Lawyer Up</h4>
</a>
<p class="slick-tab-content">
Injured at work? Should you lawyer up?
<a class="slide-more" href="/articles/nolo/workers-compensation/when-should-hire-lawyer.html" title="Workers Comp: When to Lawyer Up">
<span class="slide-action">Read</span>
</a>
</p>
</li>
<li class="carousel-link slide-2 ">
<a class="slide-url" href="/articles/nolo/bankruptcy/when-consider-filing.html" title="Should I Consider Filing Bankruptcy?">
<h4 class="slick-tab-title">Should I Consider Filing Bankruptcy?</h4>
</a>
<p class="slick-tab-content">
Here's when bankruptcy might work for you.
<a class="slide-more" href="/articles/nolo/bankruptcy/when-consider-filing.html" title="Should I Consider Filing Bankruptcy?">
<span class="slide-action">Read</span>
</a>
</p>
</li>
<li class="carousel-link slide-3 ">
<a class="slide-url" href="/articles/nolo/us-immigration/undocumented-illegal-immigrant-get-green-card-marriage-citizen-resident.html" title="Can Undocumented Immigrants Get a Green Card Through Marriage?">
<h4 class="slick-tab-title">Can Undocumented Immigrants Get a Green Card Through Marriage?</h4>
</a>
<p class="slick-tab-content">
Yes, "illegal" aliens may get permanent legal status this way.
<a class="slide-more" href="/articles/nolo/us-immigration/undocumented-illegal-immigrant-get-green-card-marriage-citizen-resident.html" title="Can Undocumented Immigrants Get a Green Card Through Marriage?">
<span class="slide-action">Read</span>
</a>
</p>
</li>
<li class="carousel-link slide-4 ">
<a class="slide-url" href="/articles/nolo/personal-injury/accidents-claim-settlements-faq.html" title="Personal Injury Settlements: FAQs">
<h4 class="slick-tab-title">Personal Injury Settlements: FAQs</h4>
</a>
<p class="slick-tab-content">
Answers to common questions about settling your accident claim.
<a class="slide-more" href="/articles/nolo/personal-injury/accidents-claim-settlements-faq.html" title="Personal Injury Settlements: FAQs">
<span class="slide-action">Read</span>
</a>
</p>
</li>
</ol>
</div>
<div class="home-get-started hidden-xs hidden-sm">
<h4>Legal Calculators</h4>
<p>Our Ever-Popular Legal Calculators</p>
<div class="media media-01">
<div class="media-head">
<img alt="Find a local lawyer" class="media-object" src="/themes/quasar/images/icons/lawyer.png"/>
</div>
<div class="media-body">
<h4 class="media-heading">Personal Injury</h4>
<p><a href="/articles/nolo/personal-injury/calculator.html">Settlement Calculator</a></p>
</div>
</div>
<div class="media media-02">
<div class="media-head">
<img alt="Read Articles" class="media-object" src="/themes/quasar/images/icons/information.png"/>
</div>
<div class="media-body">
<h4 class="media-heading">Child Support</h4>
<p><a href="/calculators/childsupport/">Payment Calculators</a></p>
</div>
</div>
<div class="media media-03">
<div class="media-head">
<img alt="Problems Solved" class="media-object" src="/themes/quasar/images/icons/solutions.png"/>
</div>
<div class="media-body">
<h4 class="media-heading">Chapter 13 Bankruptcy</h4>
<p><a href="/articles/nolo/bankruptcy/chapter-13-plan-payment-calculator.html">Payment Calculator</a></p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="content-blade-front region grid-region-content-row clearfix layout-column-1" id="region-content-row-3">
<div class="region grid-region-content-center ct" id="region-content-center-3">
<div class="region block-region grid-region-content-bottom " id="region-content-bottom">
<div class="block block-239" data-controller="PromoBlock" data-display-desktop="true" data-display-mobile="true" data-display-tablet="true" data-name="Home Promo: Alllaw" data-programatic="true" data-weight="50" id="block-239-content-bottom">
<div class="block-bd clearfix">
<div class="ncms-home-promo clearfix">
<div class="card card-flat expert-leadgen-advanced-form-stacked original">
<div class="card-body">
<h3 class="card-title">Talk to a Lawyer</h3>
<p class="card-text">Want to talk to an attorney? Start here.</p>
<div class="leadgen-form">
<div class="expert-leadgen-basic-form clearfix">
<form accept-charset="UTF-8" action="https://iq.alllaw.com/path/min-path4" class="form-horizontal" data-aop="Internal Server Error" data-aop-id="0" data-path-id="min-path4" method="GET">
<div class="form-group expert-leadgen-basic-form-practice-area">
<label class="control-label" for="expert-leadgen-basic-form-practice-area">Practice Area</label>
<div class="input-box">
<div class="select-box w-100">
<select class="form-control" name="practice_area"><option value="">Please select...</option><option value="125">Auto Accident</option><option value="397546">Auto Accident (Spanish)</option><option value="4">Bankruptcy</option><option value="397617">Bedsores</option><option value="12">Business</option><option value="397620">Carbon Monoxide</option><option value="94">Child Custody</option><option value="95">Child Support</option><option value="397619">Church Sexual Abuse</option><option value="397616">Covid-19</option><option value="14">Criminal Defense</option><option value="74">DUI and DWI</option><option value="10581">Debt Settlement</option><option value="96">Divorce</option><option value="397607">Earplugs</option><option value="397614">Elmiron</option><option value="16">Employment</option><option value="18">Estate Planning</option><option value="8937">Expungement</option><option value="1093">Foreclosure</option><option value="397609">Heartburn</option><option value="397593">Hernia Mesh</option><option value="397606">IVC</option><option value="24">Immigration Law</option><option value="27">Intellectual Property</option><option value="121">Litigation</option><option value="128">Medical Malpractice</option><option value="397602">Mesothelioma</option><option value="30">Personal Injury</option><option value="88">Probate</option><option value="1077">Real Estate</option><option value="397618">Rideshare Assault</option><option value="397612">Rideshare Injury</option><option value="169071">SSDI</option><option value="397613">Talcum Powder</option><option value="67">Tax</option><option value="397608">Vaping</option><option value="397604">Weed Killer</option><option value="83">Workers Compensation</option></select>
</div>
</div>
</div>
<div class="form-group expert-leadgen-basic-form-location">
<label class="control-label" for="expert-leadgen-basic-form-location">Zip Code</label>
<span class="input-box">
<input class="form-control" name="location" placeholder="Enter Zip Code" type="tel" value=""/>
</span>
</div>
<input name="domain" type="hidden" value="alllaw.com"/>
<input name="surl" type="hidden" value="/"/>
<input name="path_url" type="hidden" value="https://iq.alllaw.com/path/min-path4"/>
<input name="region_id" type="hidden" value="none"/>
<input name="block_id" type="hidden" value="none"/>
<button class="btn btn-expert-leadgen-submit btn-warning" title="Search" type="submit"><span>Search</span></button>
</form>
</div>
</div>
<div class="how-it-works">
<h4 class="section-title">How it Works</h4>
<ol class="list-group list-unstyled clearfix">
<li class="how-it-works-1">Briefly tell us about your case</li>
<li class="how-it-works-2">Provide your contact information</li>
<li class="how-it-works-3">Choose attorneys to contact you</li>
</ol>
</div>
</div>
</div>
<div class="card expert-html-container-content-panel topic-links ">
<div class="card-body">
<h4 class="card-title">
Free Legal Information
<a class="see-all-topics" href="/topics">See All Topics</a>
</h4>
<div class="card-text">
<div class="list-group list-type-links">
<a class="section-title list-group-title" href="/topics/personal_injury" title="Personal Injury">Personal Injury</a>
<a class="list-group-item" href="/topics/auto-accidents" title="Car Accidents">Car Accidents</a>
<a class="list-group-item" href="/resources/personal-injury/work-related-injuries" title="Workers Compensation">Workers Compensation</a>
<a class="list-group-item" href="/topics/medical-malpractice" title="Medical Malpractice">Medical Malpractice</a>
<a class="list-group-item" href="/topics/disability" title="Disability Claims">Disability Claims</a>
</div>
<div class="list-group list-type-links">
<a class="section-title list-group-title" href="/topics/business_corporate" title="Business &amp; Money">Business &amp; Money</a>
<a class="list-group-item" href="/topics/bankruptcy" title="Bankruptcy Law">Bankruptcy Law</a>
<a class="list-group-item" href="/topics/home-foreclosures" title="Foreclosure">Foreclosure</a>
<a class="list-group-item" href="/topics/tax" title="Tax Law">Tax Law</a>
<a class="list-group-item" href="/topics/wills_and_trusts" title="Wills &amp; Trusts">Wills &amp; Trusts</a>
</div>
<div class="list-group list-type-links">
<a class="section-title list-group-title" href="/topics/family" title="Family">Family</a>
<a class="list-group-item" href="/topics/divorce" title="Divorce">Divorce</a>
<a class="list-group-item" href="/articles/family/child_custody" title="Child Custody">Child Custody</a>
<a class="list-group-item" href="/topics/us-immigration" title="Immigration Law">Immigration Law</a>
<a class="list-group-item" href="/topics/criminal" title="Criminal Law">Criminal Law</a>
</div>
</div>
</div>
</div>
<div class="card expert-html-container-content-panel products-block">
<div class="card-body">
<h4 class="card-title">
Legal Forms, Law Books, &amp; Software from Nolo
<a class="see-all-topics" href="https://store.nolo.com/products/">See All Products</a>
</h4>
<div class="card-text">
<div class="list-group">
<a class="list-group-item col-md-3" href="https://store.nolo.com/products/quicken-willmaker-plus-wqp.html">
<img alt="WillMaker Plus" src="https://store.nolo.com/products/media/catalog/product/w/q/wqp17_1.gif" title="WillMaker Plus"/>
<div class="caption">WillMaker Plus</div>
</a>
<a class="list-group-item col-md-3" href="https://store.nolo.com/products/online-living-trust-nntrus.html">
<img alt="Create a Living Trust" src="https://store.nolo.com/products/media/catalog/product/n/n/nntrus_1.gif" title="Create a Living Trust"/>
<div class="caption">Create a Living Trust</div>
</a>
<a class="list-group-item col-md-3" href="https://store.nolo.com/products/how-to-file-for-chapter-7-bankruptcy-hfb.html">
<img alt="File for Chapter 7 Bankruptcy" src="https://store.nolo.com/products/media/catalog/product/h/f/hfb19_1.gif" title="File for Chapter 7 Bankruptcy"/>
<div class="caption">File for Chapter 7 Bankruptcy</div>
</a>
<a class="list-group-item col-md-3" href="https://store.nolo.com/products/online-llc-tccllcus.html">
<img alt="Form an LLC" src="https://store.nolo.com/products/skin/frontend/hubv6_nolo/default/images/catalog/cms/TCCLLCUS.gif" title="Form an LLC"/>
<div class="caption">Form an LLC</div>
</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="content-blade-front region grid-region-content-row clearfix layout-column-2-right" id="region-content-row-4">
<div class="region grid-region-content-center ct" id="region-content-center-4">
<div class="region block-region grid-region-content-main " id="region-content-main">
<div class="block block-117" data-display-desktop="true" data-display-mobile="true" data-display-tablet="true" data-name="AL: Home About" data-programatic="false" data-weight="25" id="block-117-content-main">
<div class="block-bd clearfix">
<div class="non-nolo-home-about">
<h2>Popular Legal Topics</h2>
<h3><a href="https://www.alllaw.com/topics/coronavirus">Legal Issues Relating to Coronavirus</a></h3>
<div class="responsive-state-list-large">
<a href="https://www.alllaw.com/topics/auto-accidents">Auto Accidents</a>
<a href="https://www.alllaw.com/topics/bankruptcy">Bankruptcy Law</a>
<a href="https://www.alllaw.com/topics/criminal">Criminal Law</a>
<a href="https://www.alllaw.com/topics/family">Divorce &amp; Family</a>
<a href="https://www.alllaw.com/topics/home-foreclosures">Foreclosure</a>
<a href="https://www.alllaw.com/topics/us-immigration">Immigration Law</a>
<a href="https://www.alllaw.com/topics/medical-malpractice">Medical Malpractice</a>
<a href="https://www.alllaw.com/topics/personal_injury">Personal Injury</a>
<a href="https://www.alllaw.com/topics/wills_and_trusts">Wills and Trusts</a>
<a href="https://www.alllaw.com/resources/personal-injury/work-related-injuries">Workers Compensation</a>
</div>
<hr/>
<h2>Legal Forms &amp; Applications</h2>
<h3>Legal Calculators</h3>
<a href="https://www.alllaw.com/articles/nolo/personal-injury/calculator.html">Accident Settlement Calculator</a><br/>
<a href="https://www.alllaw.com/articles/nolo/bankruptcy/chapter-13-plan-payment-calculator.html">Chapter 13 Bankruptcy Calculator</a><br/>
<a href="https://www.alllaw.com/calculators/childsupport">Child Support Calculators</a>
<h3>Estate Planning</h3>
<a href="https://www.alllaw.com/articles/wills_and_trusts/online-estate-planning-software.htm">Do-It-Yourself Estate Planning</a><br/>
<a href="https://www.alllaw.com/articles/wills_and_trusts/online-will-software.htm">Online Wills &amp; Do-It-Yourself Will Making Software</a><br/>
<a href="https://www.alllaw.com/articles/wills_and_trusts/online-living-trust.htm">Creating Your Own Living Trust</a>
</div>
</div>
</div>
</div>
</div>
<div class="region block-region grid-region-content-right rt" id="region-content-right">
<div class="block block-40" data-controller="FrontPageArticlesBlock" data-display-desktop="true" data-display-mobile="true" data-display-tablet="true" data-name="NCMS Front Page Articles" data-programatic="true" data-weight="1" id="block-40-content-right">
<div class="block-bd clearfix">
<div class="expert-frontpage-articles-v2 panel panel-nolo-plain">
<div class="panel-heading">
<h3 class="panel-title">Recent Legal Articles</h3>
</div>
<div class="list-group">
<div class="list-group-item">
<h4 class="article-title"><a href="//www.alllaw.com/roundup-glyphosate-cases/what-if-a-roundup-plaintiff-dies-before-filing-resolving-a-lawsuit.html" title="What If a Roundup Plaintiff Dies Before Filing/Resolving a Lawsuit?">What If a Roundup Plaintiff Dies Before Filing/Resolving a Lawsuit?</a></h4>
<p class="article-teaser">
The death of a Roundup plaintiff doesn't mean the end of leg...
</p>
</div>
<div class="list-group-item">
<h4 class="article-title"><a href="//www.alllaw.com/criminal-law/common-defenses-to-a-criminal-charge.html" title="Common Defenses to a Criminal Charge">Common Defenses to a Criminal Charge</a></h4>
<p class="article-teaser">
An experienced criminal defense attorney holds many tools to...
</p>
</div>
<div class="list-group-item">
<h4 class="article-title"><a href="//www.alllaw.com/criminal-law/what-are-the-consequences-of-a-probation-violation.html" title="What Are the Consequences of a Probation Violation?">What Are the Consequences of a Probation Violation?</a></h4>
<p class="article-teaser">
A probation violation can carry serious consequences. The ju...
</p>
</div>
<div class="list-group-item">
<h4 class="article-title"><a href="//www.alllaw.com/criminal-law/how-plea-bargains-work-in-a-criminal-case.html" title="How Plea Bargains Work in a Criminal Case">How Plea Bargains Work in a Criminal Case</a></h4>
<p class="article-teaser">
A plea bargain is an agreement between the prosecutor and de...
</p>
</div>
<div class="list-group-item">
<h4 class="article-title"><a href="//www.alllaw.com/criminal-law/should-i-plead-guilty-at-arraignment.html" title="Should I Plead Guilty at Arraignment?">Should I Plead Guilty at Arraignment?</a></h4>
<p class="article-teaser">
Many reasons exist for a defendant to plead not guilty at th...
</p>
</div>
<div class="list-group-item">
<h4 class="article-title"><a href="//www.alllaw.com/criminal/what-happens-at-your-criminal-arraignment.html" title="What Happens at Your Criminal Arraignment?">What Happens at Your Criminal Arraignment?</a></h4>
<p class="article-teaser">
The arraignment, often considered the official start of a cr...
</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="region grid-region-page-ft" id="page-ft">
<div class="region grid-region-footer container" id="region-footer">
<footer>
<div class="container container-footer-outermost">
<div class="row">
<div class="col-lg-2 col-md-3">
<div class="logo"></div>
</div>
<div class="col-lg-10 col-md-9 menu">
<ul class="main-menu-group" id="footer-menu">
<li class="col-md-4 col-lg-auto submenu-column">
<a class="submenu-title" href="#footer-links-1" title="Company Information">Company Information <i class="arrow"></i></a>
<ul class="submenu" id="footer-links-1">
<li><a href="https://www.nolo.com/about/about.html" rel="nofollow" title="About Nolo">About Nolo</a></li>
<li><a href="https://www.nolo.com/about/jobs.html" rel="nofollow" title="Careers">Careers</a></li>
<li><a href="https://www.nolo.com/about/press-room/" rel="nofollow" title="Press Room">Press Room</a></li>
<li><a href="https://blog.nolo.com/" rel="nofollow" title="Blog">Blog</a></li>
<li><a href="https://www.nolo.com/customer-support/contact" rel="nofollow" title="Contact Us">Contact Us</a></li>
<li><a href="https://www.nolo.com/customer-support/" rel="nofollow" title="Customer Service">Customer Service</a></li>
<li><a href="https://www.nolo.com/technical-support/" rel="nofollow" title="Tech Support">Tech Support</a></li>
<li><a href="https://www.nolo.com/about/staff.html#editorbios" rel="nofollow" title="Meet The Editors">Meet The Editors</a></li>
</ul>
</li>
<li class="col-md-4 col-lg-auto submenu-column">
<a class="submenu-title" href="#footer-links-2" title="Products &amp; Services">Products &amp; Services <i class="arrow"></i></a>
<ul class="submenu" id="footer-links-2">
<li><a href="https://store.nolo.com/products/" rel="nofollow" title="Books &amp; Software">Books &amp; Software</a></li>
<li><a href="https://store.nolo.com/products/new-arrivals" rel="nofollow" title="New Arrivals &amp; Coupons">New Arrivals &amp; <br/>Coupons</a></li>
<li><a href="https://store.nolo.com/products/bestsellers" rel="nofollow" title="Bestsellers">Bestsellers</a></li>
<li><a href="https://www.nolo.com/legal-updates" rel="nofollow" title="Legal Updates">Legal Updates</a></li>
</ul>
</li>
<li class="col-md-4 col-lg-auto submenu-column">
<a class="submenu-title" href="#footer-links-3" title="Lawyer Directory">Lawyer Directory <i class="arrow"></i></a>
<ul class="submenu" id="footer-links-3">
<li><a href="https://www.nolo.com/advertisers" rel="nofollow" title="Grow Your Practice">Grow Your Practice</a></li>
<li><a href="https://www.nolo.com/lawyers/" rel="nofollow" title="Find a lawyer">Find a Lawyer</a></li>
<li><a href="https://www.nolo.com/lawyers/all-locations" rel="nofollow" title="Lawyers by location">Lawyers by Location</a></li>
<li><a href="https://www.nolo.com/legal-encyclopedia/lawyers-lawfirms/tips.html" rel="nofollow" title="Tips on Hiring Lawyers">Tips on Hiring Lawyers</a></li>
</ul>
</li>
<li class="col-md-4 col-lg-auto submenu-column">
<a class="submenu-title" href="#footer-links-4" title="Free legal Information">Free Legal Information <i class="arrow"></i></a>
<ul class="submenu" id="footer-links-4">
<li><a href="/" title="Articles &amp; FAQs">Articles &amp; FAQs</a></li>
<li><a href="https://www.nolo.com/legal-updates" rel="nofollow" title="Legal Updates">Legal Updates</a></li>
<li><a href="https://www.alllaw.com/calculators" rel="nofollow" title="Calculators">Calculators</a></li>
<li><a href="https://www.nolo.com/law-blogs/" rel="nofollow" title="Law Blogs">Law Blogs</a></li>
<li><a href="https://www.nolo.com/lawyers" rel="nofollow" title="Lawyer Directory">Lawyer Directory</a></li>
<li><a href="https://www.nolo.com/legal-research/" rel="nofollow" title="Legal Research">Legal Research</a></li>
<li><a href="/recent-articles.html" title="Newest Articles">Newest Articles</a></li>
</ul>
</li>
<li class="col-md-4 col-lg-auto submenu-column">
<a class="submenu-title" href="#footer-links-5" title="Sales">Sales <i class="arrow"></i></a>
<ul class="submenu" id="footer-links-5">
<li><a href="https://www.nolo.com/affiliates/" rel="nofollow" title="Affiliates">Affiliates</a></li>
<li><a href="https://www.nolo.com/library/" rel="nofollow" title="Library">Library</a></li>
<li><a href="https://www.nolo.com/trade" rel="nofollow" title="Trade">Trade</a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
</footer>
<div class="container container-footer-copyright">
<div class="row justify-content-end">
<div class="copyright col-lg-10 col-md-12">
<p class="copyright-paragraph-text">
<a class="copyright-policy-text" href="https://www.nolo.com/copyright-policy.html" target="_blank" title="Copyright Policy">Copyright <span class="font-family-base-sans-serif">©</span>2021 MH Sub I, LLC dba Nolo <span class="font-family-base-sans-serif">®</span></a> Self-help services may not be permitted in all states. The information provided on this site is not legal advice, does not constitute a lawyer referral service, and no attorney-client or confidential relationship is or will be formed by use of the site. The attorney listings on this site are paid attorney advertising. In some states, the information on this website may be considered a lawyer referral service. Please reference the Terms of Use and the Supplemental Terms for specific information related to your state. Your use of this website constitutes acceptance of the <a class="copyright-footer-supplemental" href="https://www.internetbrands.com/ibterms/" target="_blank" title="Terms of Use">Terms of Use</a>, <a class="copyright-footer-supplemental" href="https://www.internetbrands.com/ibterms/supplementallegalterms/" target="_blank" title="Disclaimer — Legal information is not legal advice">Supplemental Terms</a>, <a class="copyright-footer-supplemental" href="https://www.internetbrands.com/privacy/privacy-main.html" target="_blank" title="Privacy Policy">Privacy Policy</a> and <a class="copyright-footer-supplemental" href="https://www.internetbrands.com/privacy/cookie-policy.html" target="_blank" title="Cookie Policy">Cookie Policy</a>. <a class="copyright-footer-supplemental" href="https://www.internetbrands.com/privacy/privacy-contact-form.php" target="_blank" title="Do Not Sell My Personal Information">Do Not Sell My Personal Information</a>
</p>
</div>
</div>
</div>
</div> </div>
</div>
</div>
<script src="/themes/quasar/min/0a227c383c049077907624fa66a51c8f.js" type="text/javascript"></script>
<script type="text/javascript">
var ibJsHost = (("https:" == document.location.protocol) ? "https://pxlssl." : "http://pxl.");
document.write(unescape("%3Cscript src='" + ibJsHost + "ibpxl.com/privacy/pp.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
window.IBPrivacy.init();
</script>
<script type="text/javascript">
  (function() {
    window._pa = window._pa || {};
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.perfectaudience.com/serve/54d3c793a961c98a3f000143.js";
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
  })();
</script>
<script type="text/javascript">
  var _kiq = _kiq || [];
  (function(){
    setTimeout(function(){
    var d = document, f = d.getElementsByTagName('script')[0], s = d.createElement('script'); s.type = 'text/javascript';
    s.async = true; s.src = 'https://cl.qualaroo.com/ki.js/38969/htJ.js'; f.parentNode.insertBefore(s, f);
    }, 1);
  })();
</script>
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "8138560" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=8138560&amp;cv=2.0&amp;cj=1"/>
</noscript>
<script type="text/javascript">
(function(window, document, $, undefined){
    if ($(".top-bar__call-us").length > 0){
        $(".top-bar__call-us").click(function() {
            var phone = $('.top-bar__call-us a span').text();
            ga('send', 'event', 'phone', phone, window.location.href);
        });
    }

    if ($(".ctn-phone-link").length > 0){
        $(".ctn-phone-link").click(function() {
            var phone = $('.ctn-phone-text').text();
            ga('send', 'event', 'phone', phone, window.location.href);
        });
    }
})(window, document, jQuery);
</script>
<script>
$(document).ready(function () {
    var disableZipcodeForPIDs = ["397609", "397604"];
    if (disableZipcodeForPIDs.indexOf($('.expert-leadgen-basic-form .form-control[name="practice_area"]').val()) > -1) {
        $('.expert-leadgen-basic-form-location .form-control[name="location"]').val('');
        $('.expert-leadgen-basic-form-location .form-control[name="location"]').attr('disabled', true);
    }
    $('body').on('change', '.expert-leadgen-basic-form .form-control[name="practice_area"]', function (e) {
        var value = $(e.target).val();
        if (disableZipcodeForPIDs.indexOf(value) > -1) {
            $('.expert-leadgen-basic-form-location .form-control[name="location"]').val('');
            $('.expert-leadgen-basic-form-location .form-control[name="location"]').attr('disabled', true);
        } else {
            $('.expert-leadgen-basic-form-location .form-control[name="location"]').attr('disabled', false);
        }
    });
});
</script>
<script src="//gdpr.internetbrands.com/v1/ibeugdpr.min.js" type="text/javascript"></script>
<script type="text/javascript">
    IBEUGDPR.displayBanner();
</script>
<script type="text/javascript">
(function(src,config){var script=document.createElement('script');script.onload=script.onreadystatechange=function(e){var event=e||window.event;if(event.type==='load'||/loaded|complete/.test(script.readyState)&&document.documentMode<=11){script.onload=script.onreadystatechange=script.onerror=null;new IBTracker(config);}};script.onerror=function(){script.onload=script.onreadystatechange=script.onerror=null;};script.async=1;script.src=src;script.setAttribute('crossorigin','anonymous');document.getElementsByTagName('head')[0].appendChild(script);}('https://ibclick.stream/assets/js/track/dist/js/v1/tracker.min.js',{site:'alllaw.com',vertical:'legal',autoPageViewTracking:true,autoClickTracking:true,snippetVersion:'1.2'}));
</script>
</body></html>
