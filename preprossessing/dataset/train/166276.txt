<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="아틀란" property="og:site_name"/>
<meta content="article" property="og:type"/>
<meta content="대한민국 No.1 프리미엄 맵 아틀란" property="og:title"/>
<meta content="" property="og:image"/>
<meta content="아틀란 홈페이지" property="og:description"/>
<meta content="http://www.atlan.co.kr/" property="og:url"/>
<title>대한민국 No.1 프리미엄 맵 아틀란</title>
<script src="http://ajax.googleapis.com/ajax/libs/webfont/1.4.10/webfont.js"></script>
<script type="text/javascript">
	WebFont.load({
		custom: {
			families: ['NanumGothic'],
			urls: ['/include/css/import/font.css']
		}
	});
	</script>
<link href="/favicon_2015.ico" rel="icon" type="image/x-icon"/>
<link href="/include/css/import/import.css" rel="stylesheet" type="text/css"/>
<link href="/include/css/layout.css" rel="stylesheet" type="text/css"/>
<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="/include/css/ie7.css">
	<![endif]-->
<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" href="/include/css/ie8.css">
	<![endif]-->
<link href="/include/css/main.css" rel="stylesheet" type="text/css"/>
<script src="/include/js/jquery/jquery-1.10.2.js" type="text/javascript"></script>
<script src="/include/js/jquery/jquery.MultiFile.js" type="text/javascript"></script>
<script src="/include/js/jquery/jquery.validate.js" type="text/javascript"></script>
<script src="/include/js/jquery/jquery.xml2json.js" type="text/javascript"></script>
<script src="/include/js/jquery/jquery.browser.js" type="text/javascript"></script>
<script src="/include/js/config.js" type="text/javascript"></script>
<script src="/include/js/layout.js" type="text/javascript"></script>
<script src="/include/js/sms.js" type="text/javascript"></script>
<script src="/include/js/common.js" type="text/javascript"></script>
<script src="/include/js/jquery/jssor.slider.mini.js" type="text/javascript"></script>
<script src="/include/js/main.js" type="text/javascript"></script>
</head>
<body>
<div id="wrapper">
<div id="header">
<div class="top">
<h1 class="logo"><a href="/"><img alt="아틀란 로고" src="http://img.atlan.co.kr/_www2015/common/gnb/atlan_logo.png"/></a></h1>
<div>
<ul class="menu curr_main curr_main">
<li class="m_product">
<a class="item" href="/products/main.do">제품소개</a>
<ul class="nav_product">
<li class="products_mobile">
<a href="/products/mobile/atlan3d.do">내비앱</a>
<p>
<span><a href="/products/mobile/atlan3d.do">3D지도 아틀란</a></span>
</p>
</li>
<li class="products_atlan">
<a href="/products/pnd/atlanAuto.do">전용기 내비SW</a>
<p>
<span><a href="/products/pnd/atlanAuto.do">아틀란 AUTO</a></span>
<span><a href="/products/pnd/atlan5.do">아틀란5</a></span>
<span><a href="/products/pnd/atlan5EV.do">아틀란5 EV모드</a></span>
<span><a href="/products/pnd/atlanTruck.do">아틀란 Truck2</a></span>
<span><a href="/products/pnd/atlanHybrid.do">아틀란 Hybrid</a></span>
<span><a href="/products/pnd/atlanWiz.do">아틀란 Wiz</a></span>
<!--span><a href="/products/pnd/atlanWizPC.do">아틀란 Wiz PC</a></span-->
</p>
</li>
<!--li class="products_story"><a href="/products/story/list.do">팁 &amp; 리뷰</a></li-->
<!-- li class="products_partner"><a href="/products/partner.do">탑재단말</a></li -->
<!--li class="products_news"><a href="/products/news/list.do">보도자료</a></li-->
<li class="products_brand"><a href="/products/brand.do">BI소개</a></li>
</ul>
</li>
<li class="m_update">
<a class="item" href="/update/main.do">업데이트</a>
<ul class="nav_update">
<li class="update_notice"><a href="/update/notice/list.do">업데이트 공지</a></li>
<li class="update_aum"><a href="/update/aum.do">아틀란업데이트매니저</a></li>
<li class="update_gum"><a href="/update/gum.do">하이패스용 GPS업데이트</a></li>
<li class="update_product_reg"><a href="/update/productReg.do">제품인증</a></li>
<li class="update_my_products"><a href="/update/myProduct.do">제품인증 내역</a></li>
</ul>
</li>
<li class="m_support">
<a class="item" href="/support/main.do">고객지원</a>
<ul class="nav_support">
<li class="support_notice"><a href="/support/notice/list.do">공지사항</a></li>
<li class="support_faq"><a href="/support/faq/list.do">FAQ</a></li>
<li class="support_qna"><a href="/support/qna/write.do">이용문의</a></li>
<li class="support_my_qna"><a href="/support/qna/list.do">이용문의 내역</a></li>
<li class="support_my_report"><a href="/support/report/list.do">지도수정요청 내역</a></li>
<li class="support_manual"><a href="/support/manual.do">매뉴얼다운로드</a></li>
</ul>
</li>
<li class="m_service">
<a class="item" href="/support/main.do" style="display:none">서비스</a>
<ul class="impo_service">
<li class="service_cloud"><a href="/products/mobile/atlan3d.do">아틀란3D 클라우드</a></li>
<li class="service_kakao"><a href="/support/main.do">카카오톡 실시간 1:1 상담</a></li>
<!-- <li class="service_update"><a href="/update/main.do">지도 업데이트</a></li> -->
<li class="service_atlanshop"><a href="https://smartstore.naver.com/atlanshop" target="_blank" title="아틀란샵 새창으로 열림">Atlan shop</a></li>
</ul>
</li>
</ul>
</div>
<div class="sub_gnb">
<p class="sub_top_menu">
<span class="main_member"><a href="https://member.atlan.co.kr/login/login.do">로그인</a></span>
<span class="main_member"><a href="https://member.atlan.co.kr">회원가입</a></span>
<span><a href="/event/list.do">이벤트</a></span>
<span><a href="http://jebo.atlan.co.kr/" onclick="return openErrorReport(this);" target="_blank">지도수정요청</a></span>
</p>
</div>
<div class="gnb_btn">
<span class="button allMenu" id="btnSiteMap">전체메뉴</span>
</div>
</div>
</div>
<div id="container">
<div class="contents_wrapper">
<!-- 메인 페이지 시작 -->
<div class="main_contents" id="contents">
<h1 class="onlyheading">아틀란 메인</h1>
<div class="card01 mainban slider_arrow" id="slider1_container">
<!-- Slides Container -->
<div class="slider_list" u="slides">
<!-- div>
						<a href="http://www.atlan.co.kr/event/view.do?seq=461"><img src="http://img.atlan.co.kr/_www2015/main/atlan_main_0907.jpg" alt="고향가는 빠른길! 아틀란 Log-in event"/></a>
					</div -->
<!-- div>
						<a href="http://itunes.apple.com/us/app/ateullan-keullaudeu-naebi/id542287735?l=ko&amp;ls=1&amp;mt=8" target="_blank"><img src="http://img.atlan.co.kr/_www2015/main/atlan_main_0924_2.jpg?v=1" alt="드디어! 아이폰에서 만나는 '3D' 아틀란 "/></a>
					</div -->
<div>
<a href="/products/pnd/atlanAuto.do"><img alt="아틀란 오토 출시" src="http://img.atlan.co.kr/_www2015/main/atlan_main_190619.jpg?v=1"/></a>
</div>
<div>
<a href="/products/mobile/atlan3d.do"><img alt="V 2.5 아틀란 업데이트" src="http://img.atlan.co.kr/_www2015/main/atlan_main_20180625.jpg?v=1"/></a>
</div>
<div>
<a href="/products/pnd/atlan5.do"><img alt="아틀란5" src="http://img.atlan.co.kr/_www2015/main/main_v2_update_20171120.jpg?v=1"/></a>
</div>
<div>
<a href="/products/pnd/atlan5EV.do"><img alt="아틀란EV모드" src="http://img.atlan.co.kr/_www2015/main/main_Hybrid_20180115.jpg?v=1"/></a>
</div>
<!-- div>
						<a href="/products/mobile/atlan3d.do"><img src="http://img.atlan.co.kr/_www2015/main/main_v2_update_20170717_02.jpg?v=1" alt="3D지도 아틀란 업데이트"/></a>
					</div -->
<div>
<a href="/products/pnd/atlanTruck.do"><img alt="아틀란 트럭" src="http://img.atlan.co.kr/_www2015/main/main_truck_20170717_01.jpg?v=1"/></a>
</div>
</div>
<!-- bullet navigator container -->
<div class="jssorb21" u="navigator">
<div u="prototype"></div>
</div>
<div class="jssorb03" u="navigator">
<!-- Arrow Left -->
<span class="jssorb03l" u="arrowleft"></span>
<!-- Arrow Right -->
<span class="jssorb03r" u="arrowright"></span>
</div>
</div>
<div class="card03 atlan_guide">
<p class="title">아틀란, 처음이세요?</p>
<p class="guide_easy">아틀란 서비스 쉽게 이용하기</p>
<ul class="guide_step">
<li class="join"><a href="https://member.atlan.co.kr/signup/main.do">회원가입</a></li>
<li class="product_reg"><a href="/update/productReg.do">제품인증</a></li>
<li class="product_update"><a href="/update/main.do">업데이트</a></li>
</ul>
</div>
<div class="card06 atlan3d_cloud">
<a href="/products/mobile/atlan3d.do">
<p class="title">지도 아틀란</p>
<p class="cont">
					모든 자동차를 위한 내비. <br/>
					더욱 빠르고<br/>
					편리하게 진화합니다
					</p>
</a>
</div>
<div class="card07 update_list">
<h2 class="title">업데이트 소식</h2>
<ul>
<li class="new_update"><a href="/update/notice/view.do?no=1,586&amp;cate=1">2020년 11월 정기업데이트</a><span class="time">2020.11.13</span></li>
<li class="next_list"><a href="/update/notice/view.do?no=10,003">Accord(18년식~) 업데이트 진행방법 안내</a></li>
</ul>
<span class="more"><a href="/update/notice/list.do">더보기</a></span>
</div>
<div class="card08 notice_list">
<h2 class="title">공지사항</h2>
<ul>
<li class="new_notice"><a href="/support/notice/view.do?no=1,597">시스템 점검 안내(1/12 00시~05시)</a><span class="time">Jan 6, 2021</span></li>
</ul>
<span class="more"><a href="/support/notice/list.do">더보기</a></span>
</div>
<!-- div class="card09 atlan3d_v3">
			<a href="/products/pnd/atlan3d.do">
				<p>
					<span class="title">아틀란3D</span>
					<span>세상에 없던 즐거움을 누려라</span>
				</p>
			</a>
			</div -->
</div>
<div class="member_impo">
<div class="login_start">
<form action="https://member.atlan.co.kr/login/login.do" id="login" method="post">
<fieldset>
<input id="rUrl" name="rUrl" type="hidden" value="http://www.atlan.co.kr"/>
<input name="work" type="hidden" value="login"/>
<p class="id_pw">
<label for="userId">아이디</label><input id="userId" maxlength="80" name="userId" placeholder="아이디" type="text" value=""/>
<label for="pw">비밀번호</label><input class="login_pw" id="pw" maxlength="20" name="pw" placeholder="비밀번호" type="password" value=""/>
</p>
<p class="check_save"><input id="loginSave" name="loginSave" type="checkbox" value="Y"/> <label for="loginSave">아이디 저장</label></p>
<p class="btn_login"><input type="submit" value="로그인"/></p>
</fieldset>
</form>
<p class="search_join">
<a href="https://member.atlan.co.kr">회원가입</a>
<a href="https://member.atlan.co.kr/find/id.do">아이디/</a>
<a href="https://member.atlan.co.kr/find/pw.do">비밀번호찾기</a>
</p>
</div>
<div class="impo_banner">
<p><a class="product_reg" href="/update/productReg.do">제품인증</a></p>
<p><a class="one_update" href="/update/aum.do">아틀란 업데이트 매니저 다운로드</a></p>
<p><a class="qna" href="/support/qna/write.do">이용문의</a></p>
<p><a class="shop" href="https://smartstore.naver.com/atlanshop" target="_blank" title="아틀란샵 새창으로 열림">아틀란 샵<br/><span>(시리얼넘버)</span></a></p>
<!-- <p><a href="http://jebo.atlan.co.kr" target="_blank" onclick="return openErrorReport(this);" class="map_modify">지도수정요청</a></p> -->
</div>
<div class="yellow_id">
<a href="/support/main.do">
<span class="friends_service">아틀란 프렌즈 서비스</span>
<span class="atlan_id">아이디: 아틀란</span>
</a>
</div>
</div>
<!-- 메인 페이지 종료 -->
<!-- <div class="sns_quick">
				<h1 class="onlyheading">SNS 바로가기</h1>
				<ul class="sns_btn">
					<li><a href="http://www.facebook.com/atlanmap" target="_blank" class="go_facebook"><img src="http://img.atlan.co.kr/_www2015/common/sns_facebook.png" alt="페이스북" /></a></li>
					<li><a href="http://twitter.com/atlan3d" target="_blank" class="go_twitter"><img src="http://img.atlan.co.kr/_www2015/common/sns_twitter.png" alt="트위터" /></a></li>
					<li><a href="http://blog.naver.com/atlan3d" target="_blank" class="go_blog"><img src="http://img.atlan.co.kr/_www2015/common/sns_blog.png" alt="아틀란 브랜드 블로그" /></a></li>
					<li><a href="#header" class="go_top"><img src="http://img.atlan.co.kr/_www2015/common/aside_top.png" alt="맨위로" /></a></li>
				</ul>
			</div> -->
</div>
</div>
<div id="footer">
<div class="footer-con">
<div class="leftcol">
<div class="info_community">
<div class="terms_new">
<p><a href="/policies/terms.do"><span>이용약관</span></a> ㅣ <a href="/policies/privacy.do"><span>개인정보취급방침</span></a> ㅣ <a href="/policies/lbs.do"><span>위치기반서비스 이용약관</span></a></p>
</div>
<div class="fsbanners">
<span class="btn_mappers_eng"><a href="http://www.mapperscorp.com/en-US/who-we-are.do" target="_blank">MAPPERS English site</a></span>
<p class="btn_fmsites" id="btnFamilySite"><span>Family Site</span><!-- img id="btnFamilySite" src="http://img.atlan.co.kr/_www2015/main/familysite_bg.png" alt="familysite"/ --></p>
<div class="fsList">
<dl>
<dt>Family sites</dt>
<!-- <dd><a href="http://3d.atlan.co.kr/" onclick="openAtlan3D();return false;">아틀란3D</a></dd> -->
<dd><a href="http://www.mapperscorp.com/" target="_blank">맵퍼스</a></dd>
<dd><a href="http://www.fine-drive.com/" target="_blank">파인드라이브</a></dd>
<dd><a href="http://www.finedigital.com/" target="_blank">(주)파인디지털</a></dd>
</dl>
</div>
</div>
</div>
<div class="address">
<p class="mappers_logo">
<a href="http://www.mapperscorp.com/" target="_blank"><span><img alt="맵퍼스 로고" src="http://img.atlan.co.kr/_www2015/common/mappers_logo.png"/></span></a>
<span>(주)맵퍼스</span>
</p>
<p class="contact">
<span>대표 : 김명준</span> <span>사업자등록번호:215-86-90506</span> <span>FAX: (맵퍼스)02-2146-3100 </span>
</p>
<p class="copybox">
<span>서울시 송파구 오금로 182(송파동, M&amp;S프라자 2층)  |  지번주소:서울시 송파구 송파동 141-2 M&amp;S 프라자 2층</span>
<span class="copyright">Copyright © MAPPERS. All rights reserved.</span>
</p>
<!-- 이동 <span class="btn_mappers_eng"><a href="http://www.mapperscorp.com/" target="_blank">MAPPERS English site</a></span> -->
</div>
</div>
<div class="family_sites">
<p class="affiliates"><a class="inquire" href="http://www.mappers.kr" target="_blank">제휴문의</a></p>
<p class="btn_map_api_b2b"><a href="http://map.apis.atlan.co.kr" target="_blank">지도 서비스</a></p>
<!-- 이동
				<p><img id="btnFamilySite" src="http://img.atlan.co.kr/_www2015/main/familysite_bg.png" alt="familysite"/></p>
				<div class="fsList">
					<dl>
						<dt>Family sites</dt>
						<dd><a href="http://3d.atlan.co.kr/" onclick="openAtlan3D();return false;">아틀란3D</a></dd>
						<dd><a href="http://www.mapperscorp.com/" target="_blank">맵퍼스</a></dd>
						<dd><a href="http://www.fine-drive.com/" target="_blank">파인드라이브</a></dd>
						<dd><a href="http://www.finedigital.com/" target="_blank">(주)파인디지털</a></dd>
					</dl>
				</div> -->
</div>
</div>
</div>
<!-- footer end -->
</div>
<script type="text/javascript">
//<![CDATA[
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
//]]>
</script>
<script type="text/javascript">
//<![CDATA[
var pageTracker = _gat._getTracker("UA-4430501-1");
pageTracker._initData();
pageTracker._trackPageview();
//]]>
</script>
</body>
</html>
