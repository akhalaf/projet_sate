<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "95148",
      cRay: "610ffdf31eba1975",
      cHash: "20982317994e7af",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmFuZGVuY29uY3VycmVudC5ubC8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "pVGrBgEIPigLyh44MAMvWJcjML57opqtl89Hqqm1ajg+vWRsWiKcqfesC6mjf2zUpuNDLkourZCv8tdtzZl7dSy91o97XQC7JZMvBJAMeB3TETDQ9c/yLOnN8Tb4Sj4Xy21STFh7AdzRZKOVq8/dKFz6dzMpaLuiIU8Eu+UpaNIO4uNMCF7OvPqfYJ/7ph8Lmn3w+sTNdROxSHyL/lSHDbfQmFJXVug0ftL725T0pdcsodDiiVc0TsJQwNg3K2+y9TZgpdlZdz9NCTRRRh5znhDqvVss7DrbAFAt9FdCr9h0egt27tfu0OT30E9A7nZbcfPErUipF7U8fUjT/PbNo3fpIYfbI/zTfhBmYuNEttpX+PlRJAPwY1U8lDEOUhvlb/x+M3banyzMeusghlWCe2e+hPEwRz/wgX1su+/3YQswK27ZkZRZFGiAjrRCArQKZtSUy5xxIjNIJauMYWjT6mkRCW6WcoAXyX+M0Oo2twWs8yVV46uM6LK9bDlJsrJ21BMGivI5sX21z/MsQo3wWZ/OQyUcHBUDTsO+q2crdLhrqNP4pRX0uTJrV41uOYTZRs3nt/W/wR+pqdWUd7Ri9yHTMQnhx2KOlRbyVTCnr8I9rfcQYvUfCxRIXr6R2B3gb83bwYgSMuWSWtQw86Rkna0tDbS/P5jpBoro71nFaCXLd0sFqBZ0/+UrZKDQAoVNZo0yLsVVyKjeSyVoDIXXI0UQf42+1ueAlJxAiSlG9v+flrowbxQJ96Ehi5AHAQQBNqFyUJmXkV+c1lX18No91CLLU9D5+FhqsnWElXVXvXw=",
        t: "MTYxMDU1MDA4MC41MTAwMDA=",
        m: "q2MFR7hA52k2O6WnjgWpCBqXg6S8jeXVACLw9umYXA4=",
        i1: "sdA+RQpWnC7DyncNrSHa/A==",
        i2: "oNseo/Q0AxRm2mjsav7jBg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "iJXRU2STwMp6nXz6NtqecU+zDxwvWBdC1YVoPrjQ6/c=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610ffdf31eba1975");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> bandenconcurrent.nl.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=e08e409ee173d22e276d33e1afdab98d85cb2f60-1610550080-0-AdPFgB5MoSzBZBCyKvs0AriOiFCW7ctFSiK-fPjmtWS44ogNrK7Ra1VcQVoPQvPICS1dRb_JKkp854YPFmkkSRrVVMI4181HsG4SN2EHe_-pQMJd6UP7JDuc558lmMfLz-oPDvxYRfki0bL_NeQjBpBFH84AshWCNqqRxQEK-ku49rZPkIgfyJnMCCYe3vu5BleP-DjaG6FJeNvO53GOAbQNJBiRBXIHi9dU3MxUZIDKxiuJ5sZQneQin3-xczvkQmCjwHratFY_CrgrOjlKdueGNJ0B1swWKTkluQRypKlGhl5VSvu9USnzEKKwYMclUf1Xr5z1gsajmsl8T_e7CDuhvcXm2ayzXttiO8ecXpSKYuworFSD12H2mTJKt3lrRw" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="833f774b572f5ced4e5a508aeca41f2ce9b83494-1610550080-0-AWEhUWn9JxzAEU3AnQRvrVFEF5DdGEob4zloxBtuYHggNRvwC21aEPG/wxAz6OE8tiI7pyftVwo1DHNStkCRqr0WyiDvXn78d2sk3rAA+pqsFeR4vkZP2/IZbhomdM8USPIYvEI7n0lonjKS+kLLimZi3rVryPbGtwM5m5vj3MVV6vRsozZlHezNCkp2k7C/+1hdbFZntLhJJuCfwiaT0xrGnVhBptkTqAv1s9I1nxEIb2+ow+2t0MqSPAj2SZGyz8OiSiqERX/07ha23yYQ8flTzejyP8k01XH0A6V7zOioICTr8mBQ55nDR3WcOlirFHrSAZU6EbsPO3KgWwlCkKxtS3O8hcgs0Dxt6o9qBD6pJE2aftzvA9kqdqhVWQlz8c+ctuZXCKyrY1KVM5jgyjcTcqkRkj/7cVBz3nJz/zmNpETeHtAVs+n7W+fRDxWJApw76C3eB+Q3IEjZ2tloY9vIUqq9BcSLKkZ3a9S0i28hF6VWhMyiBHLu3cK/G+k5M4x3bVNi69A4OKmeYhU0dzJkvLtm+CL3LhioH0H7QKf4T5Zz7CXVkhVn4PvwzB9N/3T/J2sCSnYIgXLqhSlo1lKn3gmcRMkyGxggPwMhFe6xGPgKMtTmh3IBaHgTSytsKjmC+uvA4xJp2IYVTkwfI/OjqrcMQgLPxP3IAb67qHCQdCHvBR0YpPq1b9pLInhjrQsc7bb7fyBpraJzr3NoJsJEOV29apjzp010BEuS6USGiAhkuvDBoXnYEJS2avPUxPrnxAqEEwnNF+a2H2GZphuN42YO1j7VnTMAov23VPl9c40+nd7t70c4jQJcIPYkIHelRRNBmoAMlncvYHj8znplbto7GuAVNLZnNlmOF63mgeO5v+ddZObW25b3HpRv2MhSSSjbZqlVDxvIw3ru2kugiJwTi2i8gEmeK4c9+UXDzpxCp0fGBTyOL7LLUf9OiiwzUDIENMzxuTl1Tb+ZCSUBr5K7oSQijKf0dra5+P2J4uFDAjQCScg6agoxR6l+sPXzQ/sp0iruX9WG0YrMAfrr28ozz7GUrOLY9sbR0h1zyhEaYlSO40Dt5s2t/Fw/CvzRlT82HsV7rYRA8Ovd8CsL1q5tXsuraIeIf2pvsb3XFBote8kJ9MSHieff1hk4XJX+W/y8ivxH0GyY+Sok9zJA+lwtwmvREFWJcOLxIJYLUJuzvyMQMlCpT54Jj9GrgNiJFrABD5EgF4Dl2rhL3aRKYSgYHiihGajmPG1W2BQ3wW91iM8PeUK8pO/tqJiVAw2q+KoRPR0AdHOcpbOFVuoqtD/JgXvcPskOcC0vvpEqwpVIfYaPxwPy86oNGkpwbw=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="e1e5403bb5c652c96067d07c47fabc54"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610550084.51-HcNTFK3VJw"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610ffdf31eba1975')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610ffdf31eba1975</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
