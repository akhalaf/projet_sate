<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/strict.dtd">
<html xmlns="http://www.w3.org/TR/xhtml1">
<head>
<title> AboutMyLoan </title>
<meta content="EditPlus" name="Generator"/>
<meta content="" name="Author"/>
<meta content="AboutMyLoan" name="Keywords"/>
<meta content="" name="Description"/>
<link href="PageColors.css" id="csslink" rel="stylesheet" type="text/css"/>
<link href="styles.css" rel="stylesheet" type="text/css"/>
<script src="analytics.js" type="text/javascript"></script>
<style type="text/css">
         .style3
         {
             height: 12px;
         }
     </style>
</head>
<body align="center">
<form action="#" id="Form1" method="post" name="Form1">
<input id="__VIEWSTATE" name="__VIEWSTATE" type="hidden" value="/wEPDwUKLTE2Njk2NzkxOGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFC2FjY2VwdHRlcm1zTugbXaJ+qziad/0XGymBgResJMPS4+09ofSTLcnZQ+I="/>
<input id="__VIEWSTATEGENERATOR" name="__VIEWSTATEGENERATOR" type="hidden" value="C2EE9ABB"/>
<div class="header"><div class="header">
<div align="left" style="margin-top:10px;margin-left:20px;">
<a href="login.aspx">
<img border="0" src="Images/AMLLogo2.png" width="250"/>
</a>
</div>
<div align="right" style="padding-right:10px;">
<span id="PageHeaderImage" style="font-family:Verdana,Tahoma;"><b><font size="5"></font></b></span>
</div>
</div></div>
<table cellpadding="0" cellspacing="0" class="maintable" id="Table1">
<tr height="100%">
<td align="left" style="width:100%;" valign="top">
<div class="allofpage" style="width:100%;height:100%;">
<table cellpadding="0" cellspacing="0" class="pagebackground" id="Table2">
<tr>
<td width="15"></td>
<td align="left" colspan="2" style="height:100%;" valign="top">
<br/><h2>Welcome to AboutMyLoan.com</h2>
</td>
<td></td>
</tr>
<tr>
<td width="20"></td>
<td>
<div style="width:100%; vertical-align:top; border-right: 1px solid #E0E0E0; margin-right:10px;">
<h1><font color="#1B2C85">Sign In</font></h1>
<br/>
To access your account please sign in below:
<br/><br/>
<div style="width:70px">User ID:</div>
<input id="userid" maxlength="12" name="userid" style="width:300px;background-color:white;height:25px;" tabindex="1" type="text"/> 
<br/>
<div align="right" style="width:300px; margin-top:5px; margin-bottom:15px;"><a class="regularLink" href="ForgotUserID.aspx">Forgot User ID?</a> </div>
<div style="width:70px">Password:</div>
<input id="userpassword" maxlength="12" name="userpassword" style="width:300px;background-color:white;height:20px;" tabindex="2" type="password"/>

 
<br/>
<div align="right" style="width:300px; margin-top:5px;"><a class="regularLink" href="ForgotPassword.aspx">Forgot Password?</a> </div>
<div align="right" style="width:300px; margin-top:5px; margin-bottom:15px;"><a class="regularLink" href="ChangePassword.aspx">Change Password</a> </div>
<span title="Checking the Remember My User ID box will securely store your User ID on the computer you are currently using. For your security, only your User ID will be saved. You will still be required to enter your Password to access your accounts. To turn off this feature, simply remove the check at any time.

When you use the Remember My User ID function, your User ID is stored on the computer you are using. To protect your account, you should not use this feature when accessing your account from a public computer such as those located in a library or Internet cafe."><input id="acceptterms" name="acceptterms" type="checkbox"/></span>Remember My User ID on this Computer

<br/><br/>
<input class="loginbuttons" id="Loginbutton" name="Loginbutton" onclick='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("Loginbutton", "", true, "", "", false, false))' tabindex="3" type="submit" value="Sign In"/><br/>
</div>
</td>
<td style="width: 60px;"></td>
<td style="vertical-align:top;">
<div style="width:100%; vertical-align:top;">
<h1><font color="#1B2C85">Register User</font></h1>
<br/>
If you have not registered, you must do so to access your account.
<br/><br/>
<font style="font-weight:bold">During User Registration you will be asked to:</font>
<br/><br/>
<div>
- Provide account information from your statement<br/>
- Provide your personal information<br/>
- Select your User ID and Password
</div>
<br/><br/>
Your AboutMyLoan access is a safe and easy way to manage your account.
<br/><br/><input class="loginbuttons" id="CreateButton" name="CreateButton" tabindex="4" type="submit" value="Register User"/><br/>
</div>
</td>
</tr>
<tr>
<td width="15"></td>
<td colspan="3">
<div class="logindiv"><font size="1">
<br/>
</font>
</div>
</td>
</tr>
</table>
</div>
</td>
</tr>
</table>
<div class="bodyBottom">
<div style="padding-top: 10px;">
<font style="color:White;font-family: Arial, Verdana, sans-serif; font-size: 12px;">
<a href="LoginIssues.aspx" style="color:white;text-decoration:underline;">Login Issues</a>?
      <span id="loginissues">Call 1-800-225-3563</span>
      or 
    </font>
<a href="HelpEmail.aspx" style="color:white;text-decoration:underline;" target="_blank">Email</a>
<div style="float:right;">
<font style="color:White;font-family: Arial, Verdana, sans-serif; font-size: 12px;">Need some</font> <a href="WebManual.pdf" style="color:white;text-decoration:underline;" target="_blank">Help</a> <font style="color:White;font-family: Arial, Verdana, sans-serif; font-size: 12px;">or want to leave a</font> 
      <a href="DownPageComments.aspx" style="color:white;text-decoration:underline;">Comment</a> <font style="color:White;font-family: Arial, Verdana, sans-serif; font-size: 12px;">?</font>  
    </div>
</div>
<div style="float:right;padding-top:10px; padding-right:5px;">
<font style="color:White;font-family: Arial, Verdana, sans-serif; font-size: 11px;">Powered By</font>  
    <img align="right" alt="CSC" src="Images/csclogo.jpg" style="height:30px;width:50px; "/><br/><br/>
</div>
</div>
<script language="javascript">document.getElementById('userid').focus() </script></form>
</body>
</html>
