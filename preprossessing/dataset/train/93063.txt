<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]--><!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]--><!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js"> <!--<![endif]-->
<head><script src="/cdn-cgi/apps/head/f3dG_rw4EnE-rFSLGUjFkBQsJfg.js"></script><style>

.third-level-menu
{
    position: absolute;
    top: 0;
    right: -150px;
    width: 150px;
    list-style: none;
    padding: 0;
    margin: 0;
    display: none;
}
.third-level-men a:hover{
	color:#fff;
	}
.third-level-menu > li
{
    height: 35px;
    background: #fff;
}
.third-level-menu > li:hover  { background: #DBB54C; color:#fff;}

.second-level-menu
{
    position: absolute;
    
    left: 10px;
    width: 180px;
    list-style: none;
    padding: 0;
    margin: 0;
    display: none;
}

.second-level-menu > li
{
    position: relative;
    height: 35px;
    background: #fff;
}
.second-level-menu a:hover{
	color:#fff;
	}
.second-level-menu > li:hover { background: #DBB54C; color:#fff;}

.top-level-menu
{
    list-style: none;
    padding: 0;
    margin: 0;
}

.top-level-menu > li
{
    position: relative;
    float: left;
}

.top-level-menu li:hover > ul
{
    /* On hover, display the next level's menu */
    display: inline;
}


/* Menu Link Styles */

.top-level-menu a /* Apply to all links inside the multi-level menu */
{
	padding:10px;
    /* Make the link cover the entire list item-container */
    display: block;
	color:#000;
  
}

</style>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<title>Al Ali Engineering Co. W.L.L.</title>
<meta content="" name="description"/>
<meta content="width=device-width" name="viewport"/>
<link href="galleryslider/css/galleryslider.css" rel="stylesheet" type="text/css"/>
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<link href="css/bootstrap-responsive.min.css" rel="stylesheet"/>
<link href="gallery/css/owl.carousel.css" rel="stylesheet" type="text/css"/>
<link href="gallery/css/owl.theme.css" rel="stylesheet" type="text/css"/>
<link href="css/font-awesome.min.css" rel="stylesheet"/>
<link href="css/main.css" rel="stylesheet"/>
<link href="css/sl-slide.css" rel="stylesheet"/>
<link href="css/animate.min.css" rel="stylesheet"/>
<link href="css/custom.css" rel="stylesheet" type="text/css"/>
<script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<!-- Le fav and touch icons -->
<link href="images/ico/favicon.ico" rel="shortcut icon"/>
<link href="images/ico/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144"/>
<link href="images/ico/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114"/>
<link href="images/ico/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72"/>
<link href="images/ico/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon-precomposed"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-137864346-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137864346-1');
</script>
<script async="" data-ad-client="ca-pub-2862338553418746" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head><body>
<!--Header-->
<header class="navbar navbar-fixed-top">
<div class="navbar-inner">
<div class="container">
<a class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</a>
<div style="width:100%">
<div class="logo" style="float:left">
<a class="pull-left" href="index.php" id="logo"><img alt="" src="images/aaelogo.gif"/></a>
</div>
<div class="login" style="float:right;">
<ul class="">
<li> <a class="btn btn-primary btn-large" href="https://outlook.office.com" target="_blank">AAE Email</a></li>
<li><a class="btn btn-primary2 btn-large" data-toggle="modal" href="#loginForm"><i class="icon-lock"></i></a> </li>
</ul>
</div>
</div>
<div class="clearfix"></div>
<div class="nav-collapse collapse pull-right">
<ul class="nav top-level-menu">
<li><a href="index.php">Home</a></li>
<li><a href="#">About Company <em class="icon-angle-down"></em></a>
<ul class="second-level-menu">
<li><a href="about.php">About Company</a></li>
<li><a href="certification.php">Certification</a></li>
<li><a href="chairman.php">Chairmans Message</a></li>
<li><a href="#">Policies</a></li>
<li><a href="#">Capabilities</a></li>
</ul>
</li>
<li><a href="#"> Projects <em class="icon-angle-down"></em></a>
<ul class="second-level-menu">
<li><a href="projectCategory.php?projectCategory=1">Commercial &amp; Residential</a></li> <li><a href="projectCategory.php?projectCategory=2">Heritage &amp; Historical </a></li> <li><a href="projectCategory.php?projectCategory=3">Infrastructure &amp; Roads</a></li>
<li><a href="on_going_projects.php">On Going Project </a></li>
</ul>
</li>
<!--      <li><a href="#" >Services <em class="icon-angle-down"></em></a>
                             <ul class="second-level-menu">
                          <li> <a href='serviceCategory.php?serviceCategory=1'>Civil Engineering</a></li> <li> <a href='serviceCategory.php?serviceCategory=2'>Architecture</a></li> <li> <a href='serviceCategory.php?serviceCategory=3'>MEP</a></li>  
                                
                                
                            </ul>
                            </li>
                            
                          -->
<li><a href="#">Affiliated Service Providers <em class="icon-angle-down"></em></a>
<ul class="second-level-menu">
<li class=""><a href="#">Sister Companies<em class="icon-angle-right" style="margin-left:10px"></em></a>
<ul class="third-level-menu">
<li><a href="makkah_integratting.php">Makkah Integratting</a></li>
<li><a href="sabeareadymix.php">SABEA Ready Mix</a></li>
<li><a href="sabeareadymix.php">SABEA Block Factory </a></li>
<li><a href="sabeareadymix.php">SABEA Hollow Core </a></li>
<li><a href="sabeareadymix.php">SHAT AL HAMAMAH </a></li>
<li><a href="sabeareadymix.php">Siporex</a></li>
</ul>
</li>
<li class=""><a href="#">Affiliated Companies <em class="icon-angle-right" style="margin-left:10px"></em></a>
<ul class="third-level-menu">
<li><a href="#">Lightning</a></li>
<li><a href="#">Al Ali Road Division</a></li>
</ul>
</li>
<li class=""><a href="#">Service Providers <em class="icon-angle-right" style="margin-left:10px"></em></a>
<ul class="third-level-menu">
<li><a href="#">Survey Technologies</a></li>
<li><a href="muraykh.php">Muraykh Medical  </a></li>
</ul>
</li>
</ul>
</li>
<ul class="dropdown-menu">
<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"></a><a href="#"> Sister Companies <em class="icon-angle-down"></em></a>
<ul>
<li><a href="sabeareadymix.php">SABEA Ready Mix</a></li>
<li><a href="#">SABEA Block Factory </a></li>
<li><a href="#">SABEA Hollow Core </a></li>
<li><a href="#">SHAT AL HAMAMAH </a></li>
<li><a href="#">Supporex</a></li>
</ul>
</li>
</ul>
<li><a .php="" href="submit_resume.php">Careers</a></li>
<!--/

                               <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> Careers<em class="icon-angle-down"></em></a>
                          <ul class="dropdown-menu">
                              <li><a href="jobs_opening.php">Job Openings</a></li>
                              <li><a href="submit_resume.php".php">Submit Resume</a></li>  
                             
                              </ul> -->
<li><a href="contact.php">Contact</a></li>
</ul>
</div><!--/.nav-collapse -->
</div>
</div>
</header>
<!-- /header -->
<!--Slider-->
<section id="slide-show">
<div class="sl-slider-wrapper" id="slider">
<!--Slider Items-->
<div class="sl-slider">
<!--Slider Item1-->
<div class="sl-slide item1" data-orientation="horizontal" data-slice1-rotation="-25" data-slice1-scale="2" data-slice2-rotation="-25" data-slice2-scale="2">
<div class="sl-slide-inner">
<div class="container">
<div class="container">
</div>
</div>
</div>
</div>
<!--/Slider Item1-->
<!--Slider Item2-->
<div class="sl-slide item2" data-orientation="vertical" data-slice1-rotation="10" data-slice1-scale="1.5" data-slice2-rotation="-15" data-slice2-scale="1.5">
<div class="sl-slide-inner">
<div class="container">
</div>
</div>
</div>
<!--Slider Item2-->
<!--Slider Item3-->
<div class="sl-slide item3" data-orientation="horizontal" data-slice1-rotation="3" data-slice1-scale="2" data-slice2-rotation="3" data-slice2-scale="1">
<div class="sl-slide-inner">
</div>
</div>
<!--Slider Item3-->
</div>
<!--/Slider Items-->
<!--Slider Next Prev button-->
<nav class="nav-arrows" id="nav-arrows">
<span class="nav-arrow-prev"><i class="icon-angle-left"></i></span>
<span class="nav-arrow-next"><i class="icon-angle-right"></i></span>
</nav>
<!--/Slider Next Prev button-->
<section class="main-info">
<div class="container">
<div class="row-fluid">
<div class="span9">
<div class="wow fadeInLeft" data-wow-delay=".4s">
<h1>Welcome to AL ALI ENGINEERING .CO. W.L.L</h1>
<p>We are AL ALI ENGINEERING one of the pioneers in the construction industry, in the State of Qatar. Our company has been in the business of construction for more than two decades. While there are number of commercial &amp; residential buildings dotted on the Qatar skyline, we are also known for our specialization in Heritage Projects.
</p>
<p>   Over time we have grown into respected diverse general contractor with dynamic specialist teams strive to deliver exceptional customer service to Industrial and Landmark Buildings, Infrastructure and Roads, Interior Design, Fit-Out and Landscape, as well as the upgrade of existing important landmarks and sites </p>
</div> </div>
<div class="span3 wow fadeInRight" data-wow-delay=".4s">
<div class="widget widget-popular ">
<h3>Ongoing  Projects</h3>
<div class="widget-blog-items">
<div class="widget-blog-item media">
<div class="pull-left">
<div class="date">
<span class="day">1</span>
</div>
</div>
<div class="media-body">
<a href="projects.php?projects=4"><h5>Salwa Resort</h5></a>
</div>
</div>
<div class="widget-blog-item media">
<div class="pull-left">
<div class="date">
<span class="day">2</span>
</div>
</div>
<div class="media-body">
<a href="projects.php?projects=14"><h5>Lusail Development Project</h5></a>
</div>
</div>
<div class="widget-blog-item media">
<div class="pull-left">
<div class="date">
<span class="day">3</span>
</div>
</div>
<div class="media-body">
<a href="projects.php?projects=7"><h5>Al-Asmakh Heritage Restoration</h5></a>
</div>
</div>
</div>
</div>
</div> </div>
</div>
</section>
<!--/Services-->
<div class="text-center" id="tf-clients">
<div class="overlay">
<div class="center">
<h3>Clients / Consultant </h3>
</div>
<div class="owl-carousel owl-theme " id="clients" style="height:200px;background:none;">
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/clients/1.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/clients/2.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/clients/Ecg.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/clients/ceg.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/clients/dar.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/clients/dewan1.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/clients/dewan2.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/clients/hill.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/clients/keo.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/clients/lusail.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/clients/mz.jpg"/>
</div>
</div>
</div>
</div>
<section id="recent-works">
<div class="container">
<div class="center">
<h3>Our Recent Works</h3>
<p class="lead">Look at some of the recent projects we have completed for our valuble clients</p>
</div>
<div class="gap"></div>
<ul class="gallery col-4">
<!--Item 1-->
<li>
<div class="wow bounceIn" data-wow-delay=".4s">
<div class="preview">
<img alt=" " src="images/projects/completed/1.jpg"/>
<div class="overlay">
</div>
<div class="links">
<a data-toggle="modal" href="#modal-1"><i class="icon-eye-open"></i></a><a href="projects.php?projects=8"><i class="icon-link"></i></a>
</div>
</div>
<div class="desc">
<h5>Wakra Heritage</h5>
</div>
</div>
<div class="modal hide fade" id="modal-1">
<a aria-hidden="true" class="close-modal" data-dismiss="modal" href="javascript:;"><i class="icon-remove"></i></a>
<div class="modal-body">
<img alt=" " src="images/projects/completed/1.jpg" style="max-height:400px" width="100%"/>
</div>
</div>
</li>
<!--/Item 1-->
<!--Item 2-->
<li>
<div class="wow bounceIn" data-wow-delay=".5s">
<div class="preview">
<img alt=" " src="images/projects/completed/2.jpg"/>
<div class="overlay">
</div>
<div class="links">
<a data-toggle="modal" href="#modal-2"><i class="icon-eye-open"></i></a><a href="projects.php?projects=3"><i class="icon-link"></i></a>
</div>
</div>
<div class="desc">
<h5>Premier Inn Hotel</h5>
</div>
</div>
<div class="modal hide fade" id="modal-2">
<a aria-hidden="true" class="close-modal" data-dismiss="modal" href="javascript:;"><i class="icon-remove"></i></a>
<div class="modal-body">
<img alt=" " src="images/projects/completed/2.jpg" style="max-height:400px" width="100%"/>
</div>
</div>
</li>
<!--/Item 2-->
<!--Item 3-->
<li>
<div class="wow bounceIn" data-wow-delay=".6s">
<div class="preview">
<img alt=" " src="images/projects/completed/3.jpg"/>
<div class="overlay">
</div>
<div class="links">
<a data-toggle="modal" href="#modal-3"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>
</div>
</div>
<div class="desc">
<h5>Souq Waqif</h5>
</div>
</div>
<div class="modal hide fade" id="modal-3">
<a aria-hidden="true" class="close-modal" data-dismiss="modal" href="javascript:;"><i class="icon-remove"></i></a>
<div class="modal-body">
<img alt=" " src="images/projects/completed/3.jpg" style="max-height:400px" width="100%"/>
</div>
</div>
</li>
<!--/Item 3-->
<!--Item 4-->
<li>
<div class="wow bounceIn" data-wow-delay=".7s">
<div class="preview">
<img alt=" " src="images/projects/commercial/laborcity/1.jpg"/>
<div class="overlay">r 
                    </div>
<div class="links">
<a data-toggle="modal" href="#modal-4"><i class="icon-eye-open"></i></a><a href="projects.php?projects=1"><i class="icon-link"></i></a>
</div>
</div>
<div class="desc">
<h5>Labor City</h5>
</div>
</div>
<div class="modal hide fade" id="modal-4">
<a aria-hidden="true" class="close-modal" data-dismiss="modal" href="javascript:;"><i class="icon-remove"></i></a>
<div class="modal-body">
<img alt=" " src="images/projects/commercial/laborcity/1.jpg" style="max-height:400px" width="100%"/>
</div>
</div>
</li>
<!--/Item 4-->
</ul>
<div class="login" style="float:right;">
<ul class="">
<li><a class="btn btn-primary2 btn-large" href="projectCategory.php" style="background:#555;">See More</a></li>
</ul>
</div>
</div>
</section>
<div class="text-center" id="tf-affliated">
<div class="overlay">
<div class="center">
<h3>Affiliated / Divisions</h3>
</div>
<div class="owl-carousel owl-theme " id="affiliated" style="height:200px;background:none;">
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/affiliated/1.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/affiliated/2.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/affiliated/3.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/affiliated/4.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/affiliated/7.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/affiliated/8.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/affiliated/makkah.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/affiliated/muraykh.jpg"/>
</div>
<div class="item wow fadeInUp" data-wow-delay=".3s">
<img src="images/affiliated/salwa.jpg"/>
</div>
</div>
</div>
</div>
<section class="main" id="bottom">
<!--Container-->
<div class="container">
<!--row-fluids-->
<div class="row-fluid">
<!--Contact Form-->
<div class="span3 wow fadeInLeft" data-wow-delay=".4s">
<h4>ADDRESS</h4>
<ul class="unstyled address">
<li>
<i class="icon-home"></i><strong>Address:</strong> <br/>Al Ali Engineering .Co. W.L.L<br/>Building No:13, Al Salata Area, 109 , <br/>Jaber Bin Mohammed Street <br/>
P.O.Box 82614 , 
Doha, Qatar
                    </li>
</ul>
</div>
<!--End Contact Form-->
<!--Contact Form-->
<div class="span3 wow fadeInLeft" data-wow-delay=".6s">
<h4>CONTACT INFO</h4>
<ul class="unstyled address">
<li>
<i class="icon-envelope"></i>
<strong>Email: </strong> <span class="__cf_email__" data-cfemail="ec85828a83ac8d8d89c28f8381c29d8d">[email protected]</span>
</li>
<li>
<i class="icon-globe"></i>
<strong>Website:</strong> <a href="https://www.alaliengineering.net"> www.alaliengineering.net                </a> </li>
<li>
<i class="icon-phone"></i>
<strong>Phone :</strong> +974 - 44431533
                    </li>
<li>
<i class="icon-phone"></i>
<strong>Fax :</strong> +974 - 44438576
                    </li>
</ul>
</div>
<!--End Contact Form-->
<!--Important Links-->
<div class="span2 wow fadeInRight" data-wow-delay=".8s" id="tweets">
<h4>OUR COMPANY</h4>
<div>
<ul class="arrow">
<li><a href="about.php">About Company</a></li>
<li><a href="#">Capabilities</a></li>
<li><a href="certification.php">Certification</a></li>
<li><a href="#">Policies</a></li>
<li><a href="chairman.php">Chairmans Message</a></li>
</ul>
</div>
</div>
<!--Important Links-->
<!--Important Links-->
<div class="span2 wow fadeInRight" data-wow-delay=".8s">
<h4>Important Links</h4>
<div>
<ul class="arrow">
<li><a href="jobs_opening.php">Jobs Openings</a></li>
<li><a href="news.php">News and Events</a></li>
<li><a href="on_going_projects.php">On Going Projects</a></li>
</ul>
</div>
</div>
<!--Important Links-->
<!--/bottom-->
<!--Footer-->
<footer id="footer">
<div class="container">
<div class="row-fluid">
<div class="span5 cp">
<a href="http://tousfed.com">©</a> 2013 - 2016 <a href="http://alaliengineering.net/" target="_blank" title="alaliengineering">Al Ali Engineering</a>. All Rights Reserved.<br/> Designed By : <a href="http://tesarech.com" style="color:#B68D22" target="_blank">Towseef Ahmed</a>
</div>
<!--/Copyright-->
<div class="span6">
<ul class="social pull-right">
<li><a href="https://www.facebook.com/Al-Ali-Engineering-102038338244517/" target="_blank"><i class="icon-facebook"></i></a></li>
<li><a href="https://www.linkedin.com/company/al-ali-engineering-co-wll" target="_blank"><i class="icon-linkedin" target="_blank"></i></a></li>
<li><a href="https://www.youtube.com/channel/UCFHQTJQFdE_8OxXhEtDNimA" target="_blank"><i class="icon-youtube"></i></a></li>
</ul>
</div>
<div class="span1">
<a class="gototop pull-right" href="#" id="gototop"><i class="icon-angle-up"></i></a>
</div>
<!--/Goto Top-->
</div>
</div>
</footer>
<!--/Footer-->
<!--  Login form -->
<div aria-hidden="false" class="modal hide fade in" id="loginForm">
<div class="modal-header">
<i aria-hidden="true" class="icon-remove" data-dismiss="modal"></i>
<h4>Login Form</h4>
</div>
<!--Modal Body-->
<div class="modal-body">
<form action="index.php" class="form-inline" id="form-login" method="post">
<input class="input-small" placeholder="Email" type="text"/>
<input class="input-small" placeholder="Password" type="password"/>
<label class="checkbox">
<input type="checkbox"/> Remember me
            </label>
<button class="btn btn-primary" type="submit">Sign in</button>
</form>
<a href="#">Forgot your password?</a>
</div>
<!--/Modal Body-->
</div>
<!--  /Login form -->
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/jquery-1.11.1.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<!-- Required javascript files for Slider -->
<script src="js/jquery.ba-cond.min.js"></script>
<script src="js/jquery.slitslider.js"></script>
<!-- /Required javascript files for Slider -->
<script src="js/plugins-min.js"></script>
<script src="js/app-min.js"></script>
<script src="gallery/js/owl.carousel.js"></script>
<script src="gallery/js/main.js"></script>
<!-- SL Slider -->
<script type="text/javascript"> 
$(function() {
    var Page = (function() {

        var $navArrows = $( '#nav-arrows' ),
        slitslider = $( '#slider' ).slitslider( {
            autoplay : true
        } ),

        init = function() {
            initEvents();
        },
        initEvents = function() {
            $navArrows.children( ':last' ).on( 'click', function() {
                slitslider.next();
                return false;
            });

            $navArrows.children( ':first' ).on( 'click', function() {
                slitslider.previous();
                return false;
            });
        };

        return { init : init };

    })();

    Page.init();
});



</script>
<!-- /SL Slider -->
<script src="galleryslider/js/jssor.slider.mini.js"></script>
<script src="galleryslider/js/galeryslider.js"></script>
</div></div></section></div></section></body>
</html>