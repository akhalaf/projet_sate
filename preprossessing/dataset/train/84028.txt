<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="68E62642FA226E6005E06FEF67DE2B18" name="msvalidate.01"/>
<title>AgÃªncia Sys - Sistema para agÃªncias de publicidade e propaganda</title>
<meta content="AgÃªncia Sys - Sistema para agÃªncias de publicidade e propaganda" name="title"/>
<meta content="AgÃªncia Sys agora Ã© Operand que Ã© um software de gestÃ£o para empresas que precisam gerenciar atividades e pessoas. Foi planejado especialmente para as agÃªncias de publicidade, agÃªncias digitais, escritÃ³rios de design, marketing, entre outros." name="description"/>
<meta content="gerenciamento de agÃªncias de publicidade, controle de jobs, agencia de propaganda, gerenciador de agÃªncia de publicidade, sistema para gerenciamento de atividades, agÃªncia de propaganda, Operand, agencia sys, software, sistema agencia, online, software como serviÃ§o, Saas, cloud, fÃ¡cil, simples, objetivo, gestÃ£o, gantt, timelime, pauta de jobs, pedido de inserÃ§Ã£o, PI, PP, autorizaÃ§Ã£o de serviÃ§o" name="keywords"/>
<meta content="Operand" name="author"/>
<meta content="GENERAL" name="rating"/>
<meta content="NL5szALcLA0lmMZMuS-EvVSosZ1Li7-6cb6OKai4aRQ" name="google-site-verification"/>
<meta content="index,follow" name="robots"/>
<meta content="index,follow" name="googlebot"/>
<meta content="all" name="audience"/>
<meta content="pt-br" http-equiv="Content-Language"/>
<meta content="pt-br" name="language"/>
<link href="img/favicon/favicon.ico" rel="shortcut icon"/>
<style type="text/css">
        @import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700');
        * {font-family: 'Open Sans', sans-serif;}
        div.container {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            text-align: center;
            max-width: 550px;
            margin: 0 auto;
        }
        h1 { font-family: 'Open Sans', sans-serif; font-size: 50px; font-weight: 300; color: #666666; margin-bottom: 5px; line-height: 1.2; }
        p { font-family: 'Open Sans', sans-serif; font-size: 22px; font-weight: 300; color: #666666; margin-top: 20px; margin-bottom: 5px; }
        p.mb-0 { margin-bottom: 0; }
        p.copy { font-family: 'Open Sans', sans-serif; font-size: 11px; font-weight: 300; color: #666666; margin-top: 0; }
        a { font-family: 'Open Sans', sans-serif; font-size: 22px; font-weight: 300; color: #666666; }
    </style>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MWWKX72');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<div class="container">
<h1>AgÃªncia Sys agora<br/>Ã© Operand</h1>
<p>No inÃ­cio de 2017 fizemos uma grande mudanÃ§a em nosso brand, em especial a alteraÃ§Ã£o do nome, que mudou de AgÃªncia Sys para Operand.</p>
<p>Nosso objetivo foi criar uma âmarcaâ que represente melhor o produto e permita toda a evoluÃ§Ã£o prevista para os prÃ³ximos meses, assim como, acompanhar as mudanÃ§as de nosso mercado.</p>
<p class="mb-0">ConheÃ§a o Operand - <a href="https://www.operand.com.br/software-agencia-publicidade">Software de gestÃ£o para agÃªncias de publicidade</a>.</p><p>
<a href="https://www.operand.com.br"><img alt="AgÃªncia Sys agora Ã© Operand" border="0" src="//www.operand.com.br/img/sobre000b.png"/></a>
</p><p class="copy">Copyright 2020. OperandÂ®</p>
</div>
</body>
</html>
