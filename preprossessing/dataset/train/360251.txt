<!DOCTYPE HTML>
<html data-config='{"twitter":0,"plusone":0,"facebook":0,"style":"modern"}' dir="ltr" lang="fr-fr">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<base href="https://copadel.fr/fr/"/>
<meta content="producteur , expéditeur , fruits , légumes , exportateur , copadel , provence , chateaurenard" name="keywords"/>
<meta content="Super User" name="author"/>
<meta content="Copadel est producteur, expéditeur et exportateur de fruits et légumes à Chateaurenard, en Provence depuis 1993" name="description"/>
<title>Accueil</title>
<link href="https://copadel.fr/fr/" hreflang="fr-FR" rel="alternate"/>
<link href="https://copadel.fr/en/" hreflang="en-GB" rel="alternate"/>
<link href="https://copadel.fr/fr/component/search/?id=7&amp;Itemid=261&amp;format=opensearch" rel="search" title="Valider Copadel" type="application/opensearchdescription+xml"/>
<link href="/templates/yoo_avanti/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<script src="/media/jui/js/jquery.min.js?b19eb81f5c67cac081d1e427999b2b74" type="text/javascript"></script>
<script src="/media/jui/js/jquery-noconflict.js?b19eb81f5c67cac081d1e427999b2b74" type="text/javascript"></script>
<script src="/media/jui/js/jquery-migrate.min.js?b19eb81f5c67cac081d1e427999b2b74" type="text/javascript"></script>
<script src="/media/jui/js/bootstrap.min.js?b19eb81f5c67cac081d1e427999b2b74" type="text/javascript"></script>
<script src="/media/widgetkit/uikit2-9ab55025.js" type="text/javascript"></script>
<script src="/media/widgetkit/wk-scripts-f91dc9f5.js" type="text/javascript"></script>
<link href="https://copadel.fr/fr/" hreflang="x-default" rel="alternate"/>
<link href="/templates/yoo_avanti/apple_touch_icon.png" rel="apple-touch-icon-precomposed"/>
<link href="/templates/yoo_avanti/styles/modern/css/bootstrap.css" rel="stylesheet"/>
<link href="/templates/yoo_avanti/styles/modern/css/theme.css" rel="stylesheet"/>
<link href="/templates/yoo_avanti/css/custom.css" rel="stylesheet"/>
<script src="/templates/yoo_avanti/warp/vendor/uikit/js/uikit.js"></script>
<script src="/templates/yoo_avanti/warp/vendor/uikit/js/components/autocomplete.js"></script>
<script src="/templates/yoo_avanti/warp/vendor/uikit/js/components/datepicker.js"></script>
<script src="/templates/yoo_avanti/warp/vendor/uikit/js/components/search.js"></script>
<script src="/templates/yoo_avanti/warp/vendor/uikit/js/components/sticky.js"></script>
<script src="/templates/yoo_avanti/warp/vendor/uikit/js/components/timepicker.js"></script>
<script src="/templates/yoo_avanti/warp/vendor/uikit/js/components/tooltip.js"></script>
<script src="/templates/yoo_avanti/warp/js/social.js"></script>
<script src="/templates/yoo_avanti/js/theme.js"></script>
<script src="/templates/yoo_avanti/js/animated-text.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74691316-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Universal Google Analytics Plugin by PB Web Development -->
</head>
<body class="tm-isblog tm-navbar-sticky tm-footer-fixed ">
<div class="tm-block-header" id="tm-header">
<div class="tm-navbar-wrapper tm-navbar-wrapper-animate" data-uk-sticky="{media: 767,top: -250,clsinactive: 'tm-navbar-wrapper'}">
<div class="tm-navbar uk-navbar">
<div class="uk-container uk-container-center tm-navbar-container">
<div class="tm-navbar-left uk-flex uk-flex-middle">
<a class="uk-navbar-brand uk-flex uk-flex-middle uk-hidden-small" href="https://copadel.fr">
<p><img alt="Copadel" class="uk-margin-small-right" height="150" src="/templates/yoo_avanti/images/copadel.svg" width="150"/></p></a>
<a class="tm-logo-small uk-visible-small" href="https://copadel.fr">
<p><img alt="Copadel" class="uk-margin-small-right" height="60" src="/templates/yoo_avanti/images/copadel.svg" width="60"/></p></a>
</div>
<div class="tm-navbar-center uk-flex uk-flex-center uk-hidden-small uk-hidden-medium">
<ul class="uk-navbar-nav uk-hidden-small"><li class="uk-active"><a href="/fr/">Accueil</a></li><li><a href="/fr/notre-entreprise">Notre entreprise</a></li><li><a href="/fr/nos-produits">Nos produits</a></li><li><a href="/fr/nos-marques">Nos marques</a></li><li><a href="/fr/notre-equipe">Notre équipe</a></li><li><a href="/fr/contact">Contact</a></li></ul> </div>
<div class="tm-navbar-right uk-flex uk-flex-middle">
<div class="uk-hidden-small">
<div class="">
<p><a href="/index.php/fr/"><img alt="" src="/media/mod_languages/images/fr_fr.gif"/></a><br/><a href="/index.php/en/"><img alt="" src="/media/mod_languages/images/en.gif"/></a></p></div>
</div>
<a class="uk-navbar-toggle uk-hidden-large" data-uk-offcanvas="" href="#offcanvas"></a>
</div>
</div>
</div>
</div>
<div class="tm-header-container">
<div class="uk-panel">
<div class="tm-slideshow-avanti " data-uk-slideshow="{animation: 'swipe',autoplay: true ,autoplayInterval: 5000}">
<div class="uk-slidenav-position">
<ul class="uk-slideshow uk-overlay-active">
<li style="min-height: 700px;">
<img alt="Compagnie des Productions Agricoles du Grand Delta" src="/images/copadel-accueil-04.jpg"/>
<div class="uk-overlay-panel uk-overlay-fade">
<div class="uk-container uk-container-center uk-height-1-1 uk-width-1-1 uk-flex uk-flex-middle">
<div class="uk-width-medium-1-2">
<div class="tm-slideshow-content-panel">
<div class="uk-margin">
<ul class="tm-numnav uk-flex-left">
<li data-uk-slideshow-item="0"><a href="#">Compagnie des Productions Agricoles <br/>du Grand Delta</a></li>
<li data-uk-slideshow-item="1"><a href="#">Copadel : Producteur - Vente directe</a></li>
<li data-uk-slideshow-item="2"><a href="#">Bio</a></li>
</ul>
</div>
<div class="uk-text-large uk-margin">Producteur, expéditeur et exportateur <br/>de fruits et légumes depuis 1993.
<br/>Agriculture conventionnelle et Bio.</div>
<p><a class="uk-button" href="/index.php/fr/notre-entreprise">+ d'infos</a></p>
</div>
</div>
</div>
</div>
</li>
<li style="min-height: 700px;">
<img alt="Copadel : Producteur - Vente directe" src="/images/copadel-accueil-01.jpg"/>
<div class="uk-overlay-panel uk-overlay-fade">
<div class="uk-container uk-container-center uk-height-1-1 uk-width-1-1 uk-flex uk-flex-middle">
<div class="uk-width-medium-1-2">
<div class="tm-slideshow-content-panel">
<div class="uk-margin">
<ul class="tm-numnav uk-flex-left">
<li data-uk-slideshow-item="0"><a href="#">Compagnie des Productions Agricoles <br/>du Grand Delta</a></li>
<li data-uk-slideshow-item="1"><a href="#">Copadel : Producteur - Vente directe</a></li>
<li data-uk-slideshow-item="2"><a href="#">Bio</a></li>
</ul>
</div>
<div class="uk-text-large uk-margin"><span style="display:block;background-color:rgba(0,0,0, 0.5); ;color:#fff;padding: 20px 10px;">En plus de notre activité de négoce nous sommes également producteur de fruits et légumes</span></div>
<p><a class="uk-button" href="/index.php/fr/nos-produits">+ d'infos</a></p>
</div>
</div>
</div>
</div>
</li>
<li style="min-height: 700px;">
<img alt="Bio" src="/images/copadel-accueil-bio-2.jpg"/>
<div class="uk-overlay-panel uk-overlay-fade">
<div class="uk-container uk-container-center uk-height-1-1 uk-width-1-1 uk-flex uk-flex-middle">
<div class="uk-width-medium-1-2">
<div class="tm-slideshow-content-panel">
<div class="uk-margin">
<ul class="tm-numnav uk-flex-left">
<li data-uk-slideshow-item="0"><a href="#">Compagnie des Productions Agricoles <br/>du Grand Delta</a></li>
<li data-uk-slideshow-item="1"><a href="#">Copadel : Producteur - Vente directe</a></li>
<li data-uk-slideshow-item="2"><a href="#">Bio</a></li>
</ul>
</div>
<div class="uk-text-large uk-margin"><div class="uk-grid" style="display:block;background-color:rgba(0,0,0, 0.5); ;color:#fff;padding: 20px 10px;">
<div class="uk-width-1-3">
<img src="/images/agriculture-biologique-150.jpg"/>
</div>
<div class="uk-width-2-3">
        Tous nos produits : POIRES, ABRICOTS, CERISES, RAISINS, SALADES,
COURGETTES et ASPERGES sont disponibles en agriculture 100% Biologique
    </div></div>
</div>
</div>
</div>
</div>
<a class="uk-slidenav uk-slidenav-previous uk-hidden-touch" data-uk-slideshow-item="previous" href="#"></a>
<a class="uk-slidenav uk-slidenav-next uk-hidden-touch" data-uk-slideshow-item="next" href="#"></a>
</div>
</li></ul></div>
</div> </div>
</div>
<div class="tm-block-top-a uk-block uk-block-secondary uk-contrast tm-block-fullwidth tm-grid-collapse" id="tm-top-a">
<div class="uk-container uk-container-center">
<section class="tm-top-a uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'&gt; div &gt; .uk-panel'}">
<div class="uk-width-1-1"><div class="uk-panel">
<div class="tm-slideshow-panel-avanti " id="wk-57c">
<div class="uk-panel uk-panel uk-padding-remove uk-overflow-hidden">
<div class="uk-grid uk-grid-collapse uk-flex-middle" data-uk-grid-margin="">
<div class="uk-width-large-1-2 uk-text-center">
<div class="uk-slidenav-position" data-uk-slideshow="{animation: 'swipe'}">
<ul class="uk-slideshow">
<li style="min-height: 420px;">
<span class="tm-slideshow-panel-badge uk-badge">Nos marques</span>
<img alt="Nationale 7" src="/images/nationale-7.jpg"/> </li>
<li style="min-height: 420px;">
<span class="tm-slideshow-panel-badge uk-badge">Nos marques</span>
<img alt="Les 2 Marmots" src="/images/les-2-marmots-salade.jpg"/> </li>
</ul>
<a class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous uk-hidden-touch" data-uk-slideshow-item="previous" href="#"></a>
<a class="uk-slidenav uk-slidenav-contrast uk-slidenav-next uk-hidden-touch" data-uk-slideshow-item="next" href="#"></a>
<div class="uk-position-bottom uk-margin-large-bottom uk-margin-large-right">
<ul class="uk-dotnav uk-dotnav-contrast uk-flex-right uk-margin-bottom-remove">
<li data-uk-slideshow-item="0"><a href="#">Nationale 7</a></li>
<li data-uk-slideshow-item="1"><a href="#">Les 2 Marmots</a></li>
</ul> </div>
</div>
</div>
<div class="uk-width-large-1-2">
<div class="uk-panel-body uk-text-left" data-uk-slideshow="{}">
<ul class="uk-slideshow">
<li>
<div class="uk-width-xlarge-3-5">
<h3 class="uk-h1 uk-margin-top-remove">
<a class="uk-link-reset" href="/index.php/fr/nos-marques">Nationale 7</a>
</h3>
<div class=" uk-margin-top"><p>La marque du groupe COPADEL qui regroupe l'ensemble de notre production de fruits d'été.</p></div>
<p class="uk-margin-bottom-remove"><a class="uk-button uk-button-link" href="/index.php/fr/nos-marques">lire la suite ...</a></p>
</div>
</li>
<li>
<div class="uk-width-xlarge-3-5">
<h3 class="uk-h1 uk-margin-top-remove">
<a class="uk-link-reset" href="/index.php/fr/nos-marques">Les 2 Marmots</a>
</h3>
<div class=" uk-margin-top"><p>Notre marque développée spécialement pour notre production de salade destinée à l'exportation.</p></div>
<p class="uk-margin-bottom-remove"><a class="uk-button uk-button-link" href="/index.php/fr/nos-marques">lire la suite ...</a></p>
</div>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<script>

    (function($){

        var container  = $('#wk-57c'),
            slideshows = container.find('[data-uk-slideshow]');

        container.on('beforeshow.uk.slideshow', function(e, next) {
            slideshows.not(next.closest('[data-uk-slideshow]')[0]).data('slideshow').show(next.index());
        });

    })(jQuery);

</script>
</div></div>
</section>
</div>
</div>
<div class="tm-block-top-b uk-block uk-block-muted uk-block-large " id="tm-top-b">
<div class="uk-container uk-container-center">
<section class="tm-top-b uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'&gt; div &gt; .uk-panel'}">
<div class="uk-width-1-1"><div class="uk-panel uk-text-center"><h3 class="uk-h5 uk-margin-top-remove">nos métiers / nos compétences</h3>
<div class="uk-grid-width-1-1 uk-grid-width-small-1-2 uk-grid-width-large-1-4 uk-grid uk-grid-match uk-text-center " data-uk-grid-margin="" data-uk-grid-match="{target:'&gt; div &gt; .uk-panel', row:true}" data-uk-scrollspy="{cls:'uk-animation-scale-up uk-invisible', target:'&gt; div &gt; .uk-panel', delay:300}" id="wk-gridf1a">
<div>
<div class="uk-panel uk-invisible">
<div class="uk-text-center uk-panel-teaser"><img alt="Production" class=" uk-overlay-scale" src="/images/icones/copadel-production.svg"/></div>
<h3 class="uk-panel-title">

                                    Production                
                
            </h3>
<div class="uk-margin">Tous nos produits sont issus des producteurs locaux de la région de Chateaurenard</div>
</div>
</div>
<div>
<div class="uk-panel uk-invisible">
<div class="uk-text-center uk-panel-teaser"><img alt="Conditionnement" class=" uk-overlay-scale" src="/images/icones/copadel-conditionnement.svg"/></div>
<h3 class="uk-panel-title">

                                    Conditionnement                
                
            </h3>
<div class="uk-margin">Tous nos produits sont conditionnés dans nos locaux selon des normes strictes d'hygiène et de qualité.</div>
</div>
</div>
<div>
<div class="uk-panel uk-invisible">
<div class="uk-text-center uk-panel-teaser"><img alt="Expédition" class=" uk-overlay-scale" src="/images/icones/copadel-expedition.svg"/></div>
<h3 class="uk-panel-title">

                                    Expédition                
                
            </h3>
<div class="uk-margin">Nous assurons l'expédition et le suivi de toute notre production.</div>
</div>
</div>
<div>
<div class="uk-panel uk-invisible">
<div class="uk-text-center uk-panel-teaser"><img alt="Exportation" class=" uk-overlay-scale" src="/images/icones/copadel-exportation.svg"/></div>
<h3 class="uk-panel-title">

                                    Exportation                
                
            </h3>
<div class="uk-margin">Nous exportons dans plus de 25 pays à l'international.</div>
</div>
</div>
</div>
<script>
(function($){

    // get the images of the gallery and replace it by a canvas of the same size to fix the problem with overlapping images on load.
    $('img[width][height]:not(.uk-overlay-panel)', $('#wk-gridf1a')).each(function() {

        var $img = $(this);

        if (this.width == 'auto' || this.height == 'auto' || !$img.is(':visible')) {
            return;
        }

        var $canvas = $('<canvas class="uk-responsive-width"></canvas>').attr({width:$img.attr('width'), height:$img.attr('height')}),
            img = new Image,
            release = function() {
                $canvas.remove();
                $img.css('display', '');
                release = function(){};
            };

        $img.css('display', 'none').after($canvas);

        $(img).on('load', function(){ release(); });
        setTimeout(function(){ release(); }, 1000);

        img.src = this.src;

    });

})(jQuery);
</script>
</div></div>
</section>
</div>
</div>
<div class="tm-block-top-c uk-block uk-block-default uk-block-large " id="tm-top-c">
<div class="uk-container uk-container-center">
<section class="tm-top-c uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'&gt; div &gt; .uk-panel'}">
<div class="uk-width-1-1">
<div class="uk-panel"><h3 class="uk-h5 uk-margin-top-remove">COPADEL</h3>
<h1>Fruits &amp; Légumes de Provence</h1>
<p>Producteur, expéditeur et exportateur de fruits et légumes, nous sommes basés à Chateaurenard, au coeur de la Provence et de sa forte production agricole.<br/><br/> Notre gamme se compose de fruits et légumes de saison, ce qui nous permet de garantir des produits frais et de qualité à nos clients exclusivement professionnels : grossistes, centrales d'achat et importateurs.<br/><br/> Découvrez notre gamme de fruits et légumes (salade, courgette, Raisin, Abricot, Pomme...), ainsi que les services que nous vous proposons (conditionnement, packaging...)</p></div></div> </section>
</div>
</div>
<div class="tm-block-bottom-a uk-block uk-block-muted uk-block-large uk-cover-background" id="tm-bottom-a" style="background-image: url('/images/yootheme/demo/default/home-quote.jpg');">
<div class="uk-container uk-container-center">
<section class="tm-bottom-a uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'&gt; div &gt; .uk-panel'}">
<div class="uk-width-1-1"><div class="uk-panel">
<div class="uk-width-medium-2-3" data-uk-scrollspy="{cls:'uk-animation-slide-bottom', repeat: true}">
<h3 class="uk-h1"><i>COPADEL, une équipe dynamique<br/>pour des fruits &amp; légumes <br/>de qualité au fil des saisons.<br/></i></h3>
<p class="uk-column-medium-1-2">Achat / Production / Conditionnement / Préparation / Expédition / Exportation ...                                        <br/>Une équipe de professionnels du secteur des fruits et légumes de la production à l'exportation !</p>
<p><small class="uk-h6"> Vous souhaitez prendre contact avec nous :</small><br/> <a href="/index.php/fr/contact">CLIQUEZ ICI</a></p>
</div></div></div>
</section>
</div>
</div>
<div class="tm-block-bottom-c uk-block uk-block-default uk-block-large " id="tm-bottom-c">
<div class="uk-container uk-container-center">
<section class="tm-bottom-c uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'&gt; div &gt; .uk-panel'}">
<div class="uk-width-1-1"><div class="uk-panel uk-text-center"><h3 class="uk-h5 uk-margin-top-remove">derniers articles parus sur notre blog ...</h3>
<div class="uk-grid-width-1-1 uk-grid-width-medium-1-3 uk-grid uk-grid-match tm-grid-divider uk-text-left tm-grid-avanti " data-uk-grid-margin="" data-uk-grid-match="{target:'&gt; div &gt; .uk-panel', row:true}" data-uk-scrollspy="{cls:'uk-animation-slide-top uk-invisible', target:'&gt; div &gt; .uk-panel', delay:300}" id="wk-grid2c3">
<div>
<div class="uk-panel tm-panel-space-horizontal uk-invisible">
<div class="uk-margin uk-margin-bottom-small">
<span class="uk-badge tm-badge-muted">provence</span>
<span class="uk-badge tm-badge-muted">producteur</span>
<span class="uk-badge tm-badge-muted">asperge</span>
<span class="uk-badge tm-badge-muted">Chateaurenard</span>
<span class="uk-badge tm-badge-muted">expéditeur</span>
<span class="uk-badge tm-badge-muted">fruits et légumes</span>
</div>
<h3 class="uk-panel-title">
<a class="uk-link-reset" href="/fr/blog/91-la-saison-des-asperges-de-provence-est-lancee">La saison des asperges de Provence est lancée</a>
</h3>
<div class="uk-margin">La Saison des asperges de Provence est lancée !! 
Copadel, producteur et expéditeur de fruits...</div>
<p><a class="uk-button uk-button-link" href="/fr/blog/91-la-saison-des-asperges-de-provence-est-lancee">Lire l'article</a></p>
</div>
</div>
<div>
<div class="uk-panel tm-panel-space-horizontal uk-invisible">
<div class="uk-margin uk-margin-bottom-small">
<span class="uk-badge tm-badge-muted">fruitattraction</span>
</div>
<h3 class="uk-panel-title">
<a class="uk-link-reset" href="/fr/blog/89-copadel-au-fruit-attraction-2016">Copadel au Fruit Attraction 2016</a>
</h3>
<div class="uk-margin">Copadel sera présent au salon Fruit Attraction 2016 qui se tiendra du 5 au 7 octobre 2016 à...</div>
<p><a class="uk-button uk-button-link" href="/fr/blog/89-copadel-au-fruit-attraction-2016">Lire l'article</a></p>
</div>
</div>
<div>
<div class="uk-panel tm-panel-space-horizontal uk-invisible">
<div class="uk-margin uk-margin-bottom-small">
<span class="uk-badge tm-badge-muted">courgettes</span>
<span class="uk-badge tm-badge-muted">provence</span>
<span class="uk-badge tm-badge-muted">producteur</span>
</div>
<h3 class="uk-panel-title">
<a class="uk-link-reset" href="/fr/blog/87-la-saison-de-courgettes-de-provence-est-lancee">La saison de courgettes de Provence est lancée</a>
</h3>
<div class="uk-margin">La saison de courgettes de Provence est lancée !!
Copadel producteur, expéditeur et exportateur...</div>
<p><a class="uk-button uk-button-link" href="/fr/blog/87-la-saison-de-courgettes-de-provence-est-lancee">Lire l'article</a></p>
</div>
</div>
</div>
<script>
    (function($){

        $('img', $('#wk-grid2c3')).each(function() {
            var $img = $(this),
                $canvas = $('<canvas class="uk-responsive-width"></canvas>').attr({width:$img.attr('width'), height:$img.attr('height')}),
                img = new Image;

            $img.css('display', 'none').after($canvas);

            img.onload = function(){
                $canvas.remove();
                $img.css('display', '');
            };

            img.src = this.src;
        });

    })(jQuery);
</script>
</div></div>
</section>
</div>
</div>
<div class="tm-block-footer tm-footer-centered uk-contrast" id="tm-footer">
<div class="uk-container uk-container-center uk-flex uk-flex-middle uk-flex-center uk-height-1-1 uk-width-1-1">
<footer class="tm-footer uk-text-center">
<div class="uk-panel">
<p>
<img alt="COPADEL" height="142" src="/images/copadel_blanc.svg" width="150"/>
<br/>474, chemin du Barret - BP 10074<br/> 13832 CHATEAURENARD - FRANCE</p>
<div style="float: left; width: 100%; text-align: center;margin-top:-30px;"><img alt="telephone COPADEL" src="/images/icones/copadel-telephone.svg" width="200"/> <a href="mailto:contact@copadel.fr" rel="noopener noreferrer" target="_blank"><img alt="email COPADEL" src="/images/icones/copadel-email.svg" width="200"/></a><br/>
  Entreprise certifiée :<br/><span style="padding-top:25px;"><img alt="Global Gap" src="/images/logo_global_gap_100.jpg"/> <span style="paddng-left:25px;"><img alt="Agriculture biologique" src="/images/agriculture-biologique-100.jpg"/></span></span>
</div></div>
<a class="tm-totop-scroller" data-uk-smooth-scroll="" href="#" id="tm-anchor-bottom"></a>
<div class="uk-panel"><ul class="uk-subnav uk-subnav-line uk-flex-center"><li class="uk-active"><a href="/fr/">Accueil</a></li><li><a href="/fr/blog-copadel">Blog</a></li><li><a href="/fr/mentions-legales-copadel">Mentions légales</a></li></ul></div>
</footer>
</div>
</div>
<div class="uk-offcanvas" id="offcanvas">
<div class="uk-offcanvas-bar uk-offcanvas-bar-flip"><ul class="uk-nav uk-nav-offcanvas"><li class="uk-active"><a href="/fr/">Accueil</a></li><li><a href="/fr/notre-entreprise">Notre entreprise</a></li><li><a href="/fr/nos-produits">Nos produits</a></li><li><a href="/fr/nos-marques">Nos marques</a></li><li><a href="/fr/notre-equipe">Notre équipe</a></li><li><a href="/fr/contact">Contact</a></li></ul>
<div class="uk-panel">
<form action="/fr/" class="uk-search" id="search-211" method="post" role="search">
<input class="uk-search-field" name="searchword" placeholder="recherche ..." type="text"/>
<input name="task" type="hidden" value="search"/>
<input name="option" type="hidden" value="com_search"/>
<input name="Itemid" type="hidden" value="261"/>
</form>
</div></div>
</div>
</div></body>
</html>
