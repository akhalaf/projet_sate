<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="IE=Edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="en" http-equiv="Content-language"/>
<title>Page Not Found</title>
<!-- CSS Start -->
<link href="/css/bootstrap.css" media="all" rel="stylesheet" type="text/css"/>
<link href="/css/bootstrap_overriden_styles.css" media="all" rel="stylesheet" type="text/css"/>
<link href="/css/responsive_style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="/js/prettyCheckable/prettyCheckable.css" media="all" rel="stylesheet" type="text/css"/>
<link href="/css/responsive-system-menu.css" media="all" rel="stylesheet" type="text/css"/>
<link href="/css/defaults.css" media="all" rel="stylesheet" type="text/css"/>
<!--<link type="text/css" rel="stylesheet" media="all" href="/css/jquery.selectbox.css" />-->
<link href="/css/print.css" media="print" rel="stylesheet" type="text/css"/>
<link href="/css/shadowbox.css" media="all" rel="stylesheet" type="text/css"/>
<!-- CSS End -->
<!-- Javascript start -->
<script src="/js/jquery.10.0.3.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.rsv.js" type="text/javascript"></script>
<script src="/js/prettyCheckable/prettyCheckable.min.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="/js/bootstrap.js"></script>-->
<script src="/js/jquery/jquery.form.js" type="text/javascript"></script>
<script src="/js/library.js" type="text/javascript"></script>
<script src="/js/shadowbox.js" type="text/javascript"></script>
<!-- Javascript end -->
<!--[if lt IE 9]>
  <script src="/js/html5shiv.js"></script>
  <script src="/js/respond.min.js"></script>
<style>
.response-center-div{width:46%;}
/*.interor-panel-div div{padding:0px !important;}
.interor-panel-div{padding:0px 15px !important;}*/

</style>
<![endif]-->
<script language="JavaScript" type="text/JavaScript">
    function DWlarge(dwURL) {
        popupWin = window.open(dwURL, 'DWlarge', 'scrollbars,resizable=yes,width=650,height=500,left=25,top=25');
        window.top.name = 'opener';
    }
    function DW(dwURL) {
        popupWin = window.open(dwURL, 'DW', 'scrollbars,resizable=yes,width=517,height=400,left=25,top=25');
        window.top.name = 'opener';
    }
    function DWPrivacy(dwURL) {
        popupWin = window.open(dwURL, 'DW', 'scrollbars,resizable=yes,width=517,height=400,left=25,top=25');
        window.top.name = 'opener';
    }
</script>
<!--<script>
 $(document).ready(function(){
             $('.sbHolder').mouseleave(function(){$('.sbOptions').hide()});
 });
 </script>-->
<!--[if lte IE 6]>
<link type="text/css" rel="stylesheet" media="all" href="/css/ie6.css" />
<![endif]-->
<!--[if lte IE 7]>
<link type="text/css" rel="stylesheet" media="all" href="/css/ie7.css" />
<![endif]-->
<style> html {display:none } </style>
<script> 
    if (self == top) 
    { 
        document.documentElement.style.display = "block";
    }
    else
    { 
        top.location = self.location;
    }
</script>
<!-- //-XFS -->
<script language="javascript" type="text/javascript">
            document.documentElement.style.visibility='hidden';
            if ( self == top ) {
                document.documentElement.style.visibility='visible';
            } else {
                top.location = self.location;
            }
        </script>
<!-- //-XFS -->
<script>
         $(document).ready(function(){
            $("#optOutForm #reset").click(function(){
                    $('.checkimg').removeClass('checkedimg');
                    $('.lastcheckimgall').removeClass('lastcheckedimgall');
                    $(".alert-error,.email-err-txt").remove();
                    $(".alert-block-error").removeClass("alert-block-error");
                    $(".red-border").removeClass("red-border");
                    $(".checkbox-red-border").removeClass("checkbox-red-border");
                    $('.address_region_ind .sbSelector').text($('.address_region_ind .sbSelector').parent().find('ul li').first().find('a').text());
                    $('.specialtyCode_ind .sbSelector').text($('.specialtyCode_ind .sbSelector').parent().find('ul li').first().find('a').text());
                    $('.timeOfDay_ind .sbSelector').text($('.timeOfDay_ind .sbSelector').parent().find('ul li').first().find('a').text());
                    $('.product_ind .sbSelector').text($('.product_ind .sbSelector').parent().find('ul li').first().find('a').text());
                    $('.stateCode_ind .sbSelector').text($('.stateCode_ind .sbSelector').parent().find('ul li').first().find('a').text());
            });
            $('.lastcheckimgall').parent("div").find("input[type='checkbox']").prop('class','hiddencheckbox');
            var checkedcnt = 0;
            var intotalAll = (parseInt($('#optout-chxbox-div .column1').last().find('.checkimg').length) + parseInt($('#optout-chxbox-div .column2').last().find('.checkimg').length));
           // $('.checkimg').each(function(){checkedcnt++;});


            $('.checkimg').click(function(){
               $(this).toggleClass('checkedimg');
               if($(this).parent("div").find("input[type='checkbox']").prop('checked')){
                    $(this).parent("div").find("input[type='checkbox']").prop('checked', false);
                    checkedcnt--;
               }
               else{
                   $(this).parent("div").find("input[type='checkbox']").prop('checked', true);
                   checkedcnt++;
               }
               if(checkedcnt == intotalAll)
               {
                   $('.lastcheckimgall').addClass('lastcheckedimgall');
                   $('.lastcheckimgall').parent("div").find("input[type='checkbox']").prop('checked', true);
               }
               else
               {
                    $('.lastcheckimgall').removeClass('lastcheckedimgall');
                    $('.lastcheckimgall').parent("div").find("input[type='checkbox']").prop('checked', false);
               }
           });
           $('.lastcheckimgall').click(function(){
            $(this).toggleClass('lastcheckedimgall');
               //$('.checkimg').toggleClass('checkedimg');

            if($(this).parent("div").find("input[type='checkbox']").prop('checked')){
                $('.checkimg').parent("div").find("input[type='checkbox']").prop('checked',false);
                $(this).parent("div").find("input[type='checkbox']").prop('checked',false);
                $('.checkimg').removeClass('checkedimg');
                checkedcnt = 0;
            }
            else{
               $('.checkimg').parent("div").find("input[type='checkbox']").prop('checked',true);
                $(this).parent("div").find("input[type='checkbox']").prop('checked',true);
                $('.checkimg').addClass('checkedimg');
                checkedcnt = intotalAll ;
            } });
        });
    </script>
</head>
<body>
<!--START MSPT - 443 -->
<script type="text/javascript">
var utag_data = {
}
</script>
<!-- Loading script asynchronously -->
<script type="text/javascript">
    (function(a,b,c,d){
    a='//tags.tiqcdn.com/utag/gsk/profile-rx-us/prod/utag.js';
    b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
    a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
    })();
</script>
<!--END MSPT - 443 -->
<div class="container" id="wrapper">
<div class="header-navigation">
<!-- header -->
<div id="header">
<div class="row">
<div class="col-lg-12 col-sm-12">
<div class="img-responsive col-sm-6 col-lg-4" id="site-name">
<a class="header-link" href="/index.html">Contact GSK</a>
</div>
<div class="col-lg-7 visible-lg visible-md visible-sm" id="tools">
<div id="header-nav">
<div class="right col-lg-4"><p class="header-right-txt"><span class="header-txt2 home-header1"><strong>GSK Response Center</strong></span><br/><span class="header-txt1 home-header2"><u>Hours of Operation</u></span><span class="header-txt3">M-T, Th-F 8:30 am - 5:30 pm ET<br/>W 8:30 am - 4:00 pm ET<br/>Closed holidays</span></p></div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="oranger-border-div col-lg-12"></div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<div class="adjustable-font clear-block" id="content-container">
<div class="content-right visible-lg visible-md visible-sm content-right-error">
<h4 class="content-right-add">Additional Resources</h4>
<img src="/images/right-arrow.png"/> <a class="content-right-hyperlink" href="http://www.gskforyou.com/" target="_blank">Financial Assistance for Patients</a><br/>
<img src="/images/right-arrow.png"/> <a class="content-right-hyperlink" href="http://us.gsk.com/" target="_blank">GSK in the United States</a>
</div>
<div class="h1-above-para"><p>GlaxoSmithKline respects the privacy of visitors to its Web sites. Please see our <a href="https://privacy.gsk.com/en-us/" target="_blank">privacy notice</a> for more information.</p></div>
<h1 class="H1-space-class-404">Page Not Found</h1>
<p class="para-space-class-404"><strong>We could not find the page you requested.  Use one of the links above to return to the site.</strong></p>
<div class="hours-of-oper-div visible-xs">
<p class="header-txt1">Additional Resources</p>
<a href="http://www.gskforyou.com/">Financial Assistance for Patients</a><br/>
<a href="http://us.gsk.com/">GSK in the United States</a>
</div>
</div>
</div>
</div>
<div class="med-watch-container visible-lg visible-md visible-sm row">
<div class="col-lg-12">
<div class="img-responsive" id="gsk-logo-div">
<a href="http://us.gsk.com" target="_blank"><img alt="GSK Corporation" border="0" src="https://assets.gskstatic.com/pharma/us/global/images/gsk-logo.png" title="GSK Corporation"/></a>
</div>
<div id="footer-links-div">
<ul class="links-ul">
<li><a href="http://us.gsk.com/en-us/legal-notices" target="_blank"><u>Legal Notices</u></a></li>
<li><a class="privacy-link" href="https://privacy.gsk.com/en-us/" target="_blank"><u>Privacy Notice</u></a></li>
<li><a href="http://us.gsk.com/en-us/about-our-ads/" target="_blank"><u>Interest-based Ads</u></a></li>
<li><a href="/optout"><u>Unsubscribe Consumer</u></a></li>
<li><a href="/globalsalesoptout.html"><u>Unsubscribe HCP</u></a></li>
<li class="li-last"><a href="/req-callback.html"><u>Request a Callback</u></a></li>
</ul>
<p>This Web site is funded and developed by GSK.<br/>
                This site is intended for US residents only.<br/>
                ©2020 GSK group of companies. All rights reserved.<br/>
</p>
</div>
</div>
</div>
<div class="visible-xs row footer-div">
<div class="col-lg-12">
<div class="" id="footer-links-div">
<ul class="links-ul">
<li><a href="http://us.gsk.com/en-us/legal-notices" target="_blank"><u>Legal Notices</u></a></li>
<li><a class="privacy-link" href="https://privacy.gsk.com/en-us/" target="_blank"><u>Privacy Notice</u></a></li>
<li><a href="http://us.gsk.com/en-us/about-our-ads/" target="_blank"><u>Interest-based Ads</u></a></li>
<li><a href="/optout"><u>Unsubscribe Consumer</u></a></li>
<li><a href="/globalsalesoptout.html"><u>Unsubscribe HCP</u></a></li>
<li class="li-last"><a href="/req-callback.html"><u>Request a Callback</u></a></li>
</ul>
<p>This Web site is funded and developed by GSK.<br/>
                This site is intended for US residents only.<br/>
                ©2020 GSK group of companies.<br/>
                All rights reserved.<br/>
</p>
</div>
<div class="img-responsive">
<a href="http://us.gsk.com" target="_blank"><img alt="GSK Corporation" border="0" src="https://assets.gskstatic.com/pharma/us/global/images/gsk-logo.png" title="GSK Corporation"/></a>
</div>
</div>
</div> </div>
<script src="https://assets.gskstatic.com/pharma/us/global/components/wizard.js" type="text/javascript"></script>
</body>
</html>