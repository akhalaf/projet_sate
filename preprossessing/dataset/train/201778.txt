<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en-US"> <![endif]--><!--[if IE 7 ]><html class="ie ie7" lang="en-US"> <![endif]--><!--[if IE 8 ]><html class="ie ie8" lang="en-US"> <![endif]--><!--[if IE 9 ]><html class="ie ie9" lang="en-US"> <![endif]--><!--[if (gt IE 9)|!(IE)]><!--><html lang="en-US"> <!--<![endif]-->
<head>
<title>Error 404 Not Found | Bulgarian Cultural Institute London</title>
<meta content=" » Page not found | Bulgarian Cultural Institute in London is Bulgarias official organisation in the UK which promotes Bulgarian culture" name="description"/>
<meta charset="utf-8"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.bcilondon.co.uk/wp-content/themes/bci/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="https://www.bcilondon.co.uk/xmlrpc.php" rel="pingback"/>
<link href="https://www.bcilondon.co.uk/feed/" rel="alternate" title="Bulgarian Cultural Institute London" type="application/rss+xml"/>
<link href="https://www.bcilondon.co.uk/feed/atom/" rel="alternate" title="Bulgarian Cultural Institute London" type="application/atom+xml"/>
<!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
    	<a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" alt="" /></a>
    </div>
  <![endif]-->
<link href="https://www.bcilondon.co.uk/wp-content/themes/bci/css/normalize.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bcilondon.co.uk/wp-content/themes/bci/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bcilondon.co.uk/wp-content/themes/bci/css/prettyPhoto.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bcilondon.co.uk/wp-content/themes/bci/css/grid.css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:700" media="all" rel="stylesheet" type="text/css"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.bcilondon.co.uk/feed/" rel="alternate" title="Bulgarian Cultural Institute London » Feed" type="application/rss+xml"/>
<link href="https://www.bcilondon.co.uk/comments/feed/" rel="alternate" title="Bulgarian Cultural Institute London » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.bcilondon.co.uk\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7.2"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.bcilondon.co.uk/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.6" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.bcilondon.co.uk/wp-content/themes/bci/js/jquery-1.7.2.min.js?ver=1.7.2" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-content/themes/bci/js/modernizr.js?ver=2.0.6" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-content/themes/bci/js/superfish.js?ver=1.4.8" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-content/themes/bci/js/jquery.easing.1.3.js?ver=1.3" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-content/themes/bci/js/jquery.prettyPhoto.js?ver=3.1.3" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-content/themes/bci/js/jquery.nivo.slider.js?ver=2.5.2" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-content/themes/bci/js/jquery.loader.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-content/themes/bci/js/jquery.elastislide.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-includes/js/swfobject.js?ver=2.2-20120417" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-content/themes/bci/js/jquery.cycle.all.js?ver=2.99" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-content/themes/bci/js/jquery.twitter.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-content/themes/bci/js/jquery.flickrush.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-content/themes/bci/js/si.files.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-content/themes/bci/js/audiojs/audio.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-content/themes/bci/js/custom.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-content/themes/bci/js/slides.jquery.js?ver=1.1.9" type="text/javascript"></script>
<link href="https://www.bcilondon.co.uk/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.bcilondon.co.uk/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.bcilondon.co.uk/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.7.2" name="generator"/>
<style>
h1 { font: normal 32px/32px Arial, Helvetica, sans-serif;  color:; }
h2 { font: normal 24px/24px Arial, Helvetica, sans-serif;  color:; }
h3 { font: normal 18px/18px Arial, Helvetica, sans-serif;  color:; }
h4 { font: normal 14px/18px Arial, Helvetica, sans-serif;  color:; }
h5 { font: normal 12px/18px Arial, Helvetica, sans-serif;  color:; }
h6 { font: normal 10px/18px Arial, Helvetica, sans-serif;  color:; }
#main { font: normal 12px/18px Arial, Helvetica, sans-serif;  color:; }
</style>
<script type="text/javascript">
  	// initialise plugins
		jQuery(function(){
			// main navigation init
			jQuery('ul.sf-menu').superfish({
				delay:       1000, 		// one second delay on mouseout 
				animation:   {opacity:'show',height:'show'}, // fade-in and slide-down animation
				speed:       'normal',  // faster animation speed 
				autoArrows:  false,   // generation of arrow mark-up (for submenu) 
				dropShadows: false
			});
			
		});
		
		// Init for audiojs
		audiojs.events.ready(function() {
			var as = audiojs.createAll();
		});
		
		// Init for si.files
		SI.Files.stylizeAll();
  </script>
<script type="text/javascript">
		jQuery(window).load(function() {
			// nivoslider init
			jQuery('#slider').nivoSlider({
				effect: 'random',
				slices:15,
				boxCols:8,
				boxRows:8,
				animSpeed:500,
				pauseTime:5000,
				directionNav:false,
				directionNavHide:false,
				controlNav:true,
				captionOpacity:1			});
		});
	</script>
<style type="text/css">
</style>
</head>
<body class="error404">
<div class="tail-top">
<div id="main"><!-- this encompasses the entire Web site -->
<header class="clearfix" id="header">
<a href="www.google.com">
<div class="logo">
<a href="https://www.bcilondon.co.uk/" id="logo"><img alt="Bulgarian Cultural Institute London" src="https://www.bcilondon.co.uk/wp-content/themes/bci/images/logo.png" title="Bulgarian Cultural Institute in London is Bulgarias official organisation in the UK which promotes Bulgarian culture"/></a>
<p class="tagline">Bulgarian Cultural Institute in London is Bulgarias official organisation in the UK which promotes Bulgarian culture</p>
</div></a>
<nav class="primary">
<ul class="sf-menu" id="topnav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-772" id="menu-item-772"><a href="https://www.bcilondon.co.uk/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4698" id="menu-item-4698"><a href="https://www.bcilondon.co.uk/news/">NEWS</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-773" id="menu-item-773"><a href="https://www.bcilondon.co.uk/about-us/">About Us</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-812" id="menu-item-812"><a href="https://www.bcilondon.co.uk/about-us/discover-bulgaria/">Discover Bulgaria</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-941" id="menu-item-941"><a href="https://www.bcilondon.co.uk/about-us/discover-bulgaria/about-bulgaria/">About Bulgaria</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-945" id="menu-item-945"><a href="https://www.bcilondon.co.uk/about-us/discover-bulgaria/curious-facts/">Curious Facts</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-949" id="menu-item-949"><a href="https://www.bcilondon.co.uk/about-us/discover-bulgaria/the-warriors-of-thrace/">The Warriors of Thrace</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1027" id="menu-item-1027"><a href="https://www.bcilondon.co.uk/about-us/sofia-gallery/">Sofia Gallery</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-819" id="menu-item-819"><a href="https://www.bcilondon.co.uk/about-us/library/">Library</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-822" id="menu-item-822"><a href="https://www.bcilondon.co.uk/about-us/your-ideas/">Your Comments</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-802" id="menu-item-802"><a href="https://www.bcilondon.co.uk/membership/">Membership</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-834" id="menu-item-834"><a href="https://www.bcilondon.co.uk/membership/donors-and-partners/">Donors and Partners</a></li>
</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-has-children menu-item-778" id="menu-item-778"><a href="https://www.bcilondon.co.uk/events/">Events</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-830" id="menu-item-830"><a href="https://www.bcilondon.co.uk/events/volunteering/">Important Events</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831" id="menu-item-831"><a href="https://www.bcilondon.co.uk/events/courses-workshops/">Courses</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-779" id="menu-item-779"><a href="https://www.bcilondon.co.uk/gallery/">Photos</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1362" id="menu-item-1362"><a href="https://www.bcilondon.co.uk/gallery/exhibitions/">Exhibitions</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1363" id="menu-item-1363"><a href="https://www.bcilondon.co.uk/gallery/film-screening/">Film Screening</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1565" id="menu-item-1565"><a href="https://www.bcilondon.co.uk/gallery/concerts/">Concerts</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2058" id="menu-item-2058"><a href="https://www.bcilondon.co.uk/spectacles/">Spectacles</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1567" id="menu-item-1567"><a href="https://www.bcilondon.co.uk/gallery/videos/">Videos</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-787" id="menu-item-787"><a href="https://www.bcilondon.co.uk/contacts/">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4713" id="menu-item-4713"><a href="https://www.bcilondon.co.uk/archives/">Archive</a></li>
</ul> </nav><!--.primary-->
<div id="top-search">
<form action="http://www.bcilondon.co.uk/" method="get">
<input class="input-search" name="s" type="text"/><input id="submit" type="submit" value=""/>
</form>
</div>
<div id="widget-header">
<div class="widget-header widget_text" id="text-3"> <div class="textwidget"><a href="http://bg.bcilondon.co.uk"><img src="http://bg.bcilondon.co.uk/wp-content/themes/bci/images/bulgariaflag.png" style="margin-left:580px;margin-top:140px"/></a>
<a href="http://bcilondon.co.uk"><img src="http://bg.bcilondon.co.uk/wp-content/themes/bci/images/ukflag.png" style="margin-left:10px;margin-top:140px"/></a></div>
</div><div class="widget-header social_networks_widget" id="social_networks-4">
<ul class="social-networks">
<li>
<a href="http://www.facebook.com/bcilondon?sk=wall" rel="external">
<img alt="" src="https://www.bcilondon.co.uk/wp-content/themes/bci/images/icons/facebook.png"/>
<span>Facebook</span> </a>
</li>
<li>
<a href="https://twitter.com/BciLondon" rel="external">
<img alt="" src="https://www.bcilondon.co.uk/wp-content/themes/bci/images/icons/twitter.png"/>
<span>Twitter</span> </a>
</li>
<li>
<a href="https://www.youtube.com/channel/UCBmtcOhG2YVHsiolOxCrrfg" rel="external">
<img alt="" src="https://www.bcilondon.co.uk/wp-content/themes/bci/images/icons/youtube.png"/>
<span>YouTube</span> </a>
</li>
</ul>
</div> </div><!--#widget-header-->
</header>
<div class="primary_content_wrap clearfix blog_sidebar_pos_right">
<div class="container_12"><div id="content">
<div class="clearfix" id="error404">
<div class="error404-num">404</div>
<hgroup>
<h1>Sorry!</h1> <h2>Page Not Found</h2> </hgroup>
<h4>The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</h4> <p>Please try using our search box below to look for information on the internet.</p> <form action="https://www.bcilondon.co.uk" id="searchform" method="get">
<input class="searching" id="s" name="s" type="text" value=""/><input class="submit" type="submit" value="Search"/>
</form>
</div><!--#error404 .post-->
</div><!--#content-->
</div><!--.container-->
</div>
<footer id="footer">
<div class="clearfix" id="copyright">
<div id="back-top-wrapper">
<p id="back-top">
<a href="#top"><span></span></a>
</p>
</div>
<nav class="footer">
<ul class="footer-nav" id="menu-footer-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-836" id="menu-item-836"><a href="https://www.bcilondon.co.uk/archives/">Archives</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-837" id="menu-item-837"><a href="https://www.bcilondon.co.uk/faqs/">FAQs</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-835" id="menu-item-835"><a href="https://www.bcilondon.co.uk/contacts/">Contacts</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-840" id="menu-item-840"><a href="https://www.bcilondon.co.uk/useful-links/">Useful Links</a></li>
</ul> </nav>
<div id="footer-text">
</div>
</div>
</footer>
</div><!--#main-->
</div>
<script src="https://www.bcilondon.co.uk/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpcf7 = {"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script src="https://www.bcilondon.co.uk/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.6" type="text/javascript"></script>
<script src="https://www.bcilondon.co.uk/wp-includes/js/wp-embed.min.js?ver=4.7.2" type="text/javascript"></script>
<!-- this is used by many Wordpress features and for plugins to work properly -->
<div align="center"><a href="http://www.umiteasets.com/gongfu-tea-cups.html">Gongfu Tea Cups</a><a href="http://www.ambering.com/amber-bracelets/amber-beads-bracelets.html">Amber Beads Bracelets</a><a href="http://www.domitea.com/wholesale-yixing-tea-cups.html">Wholesale Yixing Tea Cups</a><a href="http://www.umisleepingbags.com/winter-sleeping-bags.html">Winter Sleeping Bags</a></div>
</body>
</html>