<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="نماء للحلول البرمجية" name="author"/>
<meta content="شبكة غربتنا التفاعلية قائم عليها مجموعة من الشباب السوري ليقدملكن الأخبار و المعلومات بطريقة سهلة ومختلفة لتكون غربتكن أسهل" name="description"/>
<title>شبكة غربتنا</title>
<meta content="شبكة غربتنا" property="og:title"/>
<meta content="شبكة غربتنا" property="og:site_name"/>
<meta content="http://8rbtna.com" property="og:url"/>
<meta content="شبكة غربتنا تقدم المعلومات والاخبار للاجئين والمغتربين السوريين في تركيا" property="og:description"/>
<meta content="article" property="og:type"/>
<meta content="ar_AR" property="og:locale:alternate"/>
<!-- CSS -->
<link href="/v3/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/v3/css/font-awesome.min.css" rel="stylesheet"/>
<link href="/v3/css/magnific-popup.css" rel="stylesheet"/>
<link href="/v3/css/animate.css" rel="stylesheet"/>
<link href="/v3/css/slick.css" rel="stylesheet"/>
<link href="/v3/css/jplayer.css" rel="stylesheet"/>
<link href="/v3/css/main.css?id=sd2322" rel="stylesheet"/>
<link href="/v3/css/responsive.css?id=sd2322" rel="stylesheet"/>
<!-- fonts -->
<link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,600,700,800%7CRoboto:100,300,400,500,700,900%7CSignika+Negative:300,400,600,700" rel="stylesheet"/>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NQ4HVK4');</script>
<!-- End Google Tag Manager -->
<!-- icons -->
<link href="/images/favicon.ico" rel="icon"/>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<!-- Template Developed By ThemeRegion -->
</head>
<body class="homepage-1">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe height="0" src="https://www.googletagmanager.com/ns.html?id=GTM-NQ4HVK4" style="display:none;visibility:hidden" width="0"></iframe></noscript>
<!-- End Google Tag Manager (noscript) --> <div class="main-wrapper">
<!-- <div class="MobileApp">

	<div class="">

					<div class="widget widget-menu-3">

						<p>لتصفح أفضل على الموبايل حمل التطبيق الأن</p>

						<ul>

							<a href="https://play.google.com/store/apps/details?id=namaa.ghrbtna.syria" target="_blank">

								<img src="/v3/images\google-paly.png">

							</a>

							<a href="https://itunes.apple.com/tr/app/shbkt-ghrbtna/id1137022172?mt=8&amp;ign-mpt=uo%3D4" target="_blank">

								<img src="/v3/images/apple-store.png">

							</a>

						</ul>

					</div>

				</div>

</div> -->
<div class="tr-topbar topbar-width-container topbar-bg-color-1 clearfix">
<div class="container">
<div class="row">
<div class="col-sm-6">
<div class="topbar-left">
<!--

								<div class="user">

									<div class="user-image">

										<img class="img-responsive img-circle" src="/v3/images/others/user.png" alt="Image">

									</div>

									<div class="dropdown user-dropdown">

										Hello,

										<a href="#" aria-expanded="true">Jhon Player<i class="fa fa-caret-down" aria-hidden="true"></i></a>

										<ul class="sub-menu text-left">

											<li><a href="#">My Profile</a></li>

											<li><a href="#">Settings</a></li>

											<li><a href="#">Log Out</a></li>

										</ul>

									</div>

								</div>

							-->
<div class="searchNlogin">
<a href="/ads_clickers.php?id=21"><img src="/images/turk_logo.png"/></a>
</div>
</div><!-- /.topbar-right -->
</div>
<div class="col-sm-6">
<a class="navbar-brand" href="/">
<img alt="Logo" class="img-responsive" src="/v3/images/logo2.png"/>
</a>
</div>
</div><!-- /.row --> </div><!-- /.container -->
</div><!-- /.tr-topbar -->
<div class="container">
<div class="row">
<div class="col-sm-3 tr-sidebar tr-sticky " id="MyFloat">
<div class="theiaStickySidebar">
<div class="tr-menu sidebar-menu sidebar-menu-two menu-responsive">
<nav class="navbar navbar-default">
<div class="navbar-header">
<button class="navbar-toggle toggle-white collapsed" data-target="#navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div><!-- /navbar-header -->
<div class="collapse navbar-collapse" id="navbar-collapse">
<span class="discover">شبكة غربتنا</span>
<ul class="nav navbar-nav">
<li><a href="/"><i aria-hidden="true" class="fa fa-home"></i>الرئيسية</a></li>
<li class="dropdown"><a href="/قسم-الاخبار-1"><i aria-hidden="true" class="fa fa-newspaper-o"></i>أخبار</a>
<ul class="sub-menu">
<li><a href="/قسم--124" title="غربتنا وكلاء مبيعات">غربتنا وكلاء مبيعات</a></li>
<li><a href="/قسم-أخبار-ترك-تيليكم-121" title="أخبار ترك تيليكم">أخبار ترك تيليكم</a></li>
<li><a href="/قسم-فعاليات-غربتنا-117" title="فعاليات غربتنا">فعاليات غربتنا</a></li>
<li><a href="/قسم-إشاعات-115" title="إشاعات">إشاعات</a></li>
<li><a href="/قسم-مقالات-94" title="مقالات">مقالات</a></li>
<li><a href="/قسم-اقتصاد-92" title="اقتصادية">اقتصادية</a></li>
<li><a href="/قسم-سياسة-87" title="سياسية">سياسية</a></li>
<li><a href="/قسم-قوانين-75" title="فرمانات">فرمانات</a></li>
<li><a href="/قسم-اجتماعي-27" title="مجتمعنا">مجتمعنا</a></li>
<li><a href="/قسم-التعليم-المدارس-والجامعات-17" title="قلم و محاية">قلم و محاية</a></li>
<li><a href="/قسم-ولاد-البلد-16" title="نحن السوريين ناجحين">نحن السوريين ناجحين</a></li>
<li><a href="/قسم-أخبار-الحوادث-15" title="هيك عبقولوا!">هيك عبقولوا!</a></li>
<li><a href="/قسم-رياضة-14" title="رياضية">رياضية</a></li>
</ul>
</li>
<li class="dropdown"><a href="/قسم-المعلومات-68"><i aria-hidden="true" class="fa fa-info-circle"></i>معلومات</a>
<ul class="sub-menu">
<li><a href="/قسم-اوضاع-المعيشة-79" title="رغيف خبز">رغيف خبز</a></li>
<li><a href="/قسم-اوراق-رسمية-ومعاملات-قانونية-78" title="ختم وتوقيع">ختم وتوقيع</a></li>
<li><a href="/قسم-الهجرة-إلى-اوروبا-77" title="الهجرة">الهجرة</a></li>
<li><a href="/قسم-تعليم-مدارس-وجامعات-76" title="قلم ومحاية">قلم ومحاية</a></li>
<li><a href="/قسم-هي-حكايتي-20" title="هي حكايتي">هي حكايتي</a></li>
</ul>
</li>
<li><a href="/v3/TurkTelecome.php" style="color:#ef3f2f"><i aria-hidden="true" class="fa fa-home" style="color:#ef3f2f"></i>ترك تيليكوم بالعربي</a></li>
<li class="dropdown"><a href="/دليل-غربتلي"><i aria-hidden="true" class="fa fa-building"></i>دليل الغربتلي</a>
<ul class="sub-menu">
<li><a href="/دليل-غربتلي" title="">الدليل التجاري</a></li>
<li><a href="/CompaniesAdd.php" title="">أدرج فعاليتك التجارية</a></li>
</ul>
</li>
<li class="dropdown">
<a href="/فرص-العمل"><i aria-hidden="true" class="fa fa-deaf"></i>فرص عمل</a>
<ul class="sub-menu">
<li><a href="/فرص-العمل" title="">عرض فرص العمل</a></li>
<li><a href="/إضافة-فرصة-عمل" title="">نشر فرصة عمل</a></li>
</ul>
</li>
<li><a href="http://8rbtna.com/%D9%82%D8%B3%D9%85-%D9%88%D9%84%D8%A7%D8%AF-%D8%A7%D9%84%D8%A8%D9%84%D8%AF-16"><i aria-hidden="true" class="fa fa-smile-o"></i>نحن السورييين ناجحين</a></li>
<li><a href="http://8rbtna.com/new_piv.php" style="color:#d9ce00"><i aria-hidden="true" class="fa fa-home" style="color:#d9ce00"></i>نصحية قانون</a></li>
<li><a href="#"><i aria-hidden="true" class="fa fa-camera"></i>فعاليات شبكة غربتنا</a></li>
<li><a href="/v3/contacts.php"><i aria-hidden="true" class="fa fa-paper-plane"></i>تواصل معنا</a></li>
</ul>
</div>
</nav><!-- /navbar -->
</div><!-- /left-memu -->
</div><!-- /.theiaStickySidebar --> </div>
<div>
<div class="tr-home-slider home-slider-1 tr-section">
<div class="carousel slide" data-ride="carousel" id="home-carousel">
<ol class="carousel-indicators">
<li data-slide-to="0" data-target="#home-carousel">
<span class="catagory">أخبار</span>
<span class="indicators-title">تركيا تطلق تطبيقاً للتسجيل على لقاح كورونا .. أصبح أخذ اللقاح أكثر سهولة</span>
</li>
<li data-slide-to="1" data-target="#home-carousel">
<span class="catagory">أخبار</span>
<span class="indicators-title">زيادة الغرامة على أصحاب السيارات في تركيا</span>
</li>
<li data-slide-to="2" data-target="#home-carousel">
<span class="catagory">أخبار</span>
<span class="indicators-title">كم هو عار أن تصل العنصرية إلى هذا المكان فهل يبقى جثمان السوري بلا أرض؟</span>
</li>
<li class="" data-slide-to="3" data-target="#home-carousel">
<span class="catagory">معلومات</span>
<span class="indicators-title">أفضل 10 نصائح لتوظيف الشخص المناسب</span>
</li>
</ol>
<div class="carousel-inner" role="listbox">
<div class="item" style=" background-image: url(/pic/images/news/1672819248.jpg)">
<div class="post-content">
<span class="catagory" data-animation="animated fadeInUp"><a href="#">أخبار</a></span>
<h2 class="entry-title extra_slider" data-animation="animated fadeInUp">
<a href="/تفاصيل-الخبر-0/تركيا-تطلق-تطبيقاً-للتسجيل-على-لقاح-كورونا-..-أصبح-أخذ-اللقاح-أكثر-سهولة-2511">تركيا تطلق تطبيقاً للتسجيل على لقاح كورونا .. أصبح أخذ اللقاح أكثر سهولة</a>
</h2>
<div class="entry-meta" data-animation="animated fadeInUp">
<ul>
<li>المصدر: <a href="https://8rbtna.com/" target="_blank">شبكة غربتنا</a></li>
<li>443 قراءة</li>
<li>
<ul>
<li>تاريخ النشر: 2021-12-01</li>
</ul>
</li>
</ul>
</div>
</div><!-- /.post-content -->
</div><!-- /.item -->
<div class="item" style=" background-image: url(/pic/images/news/2341885133.jpg)">
<div class="post-content">
<span class="catagory" data-animation="animated fadeInUp"><a href="#">أخبار</a></span>
<h2 class="entry-title extra_slider" data-animation="animated fadeInUp">
<a href="/تفاصيل-الخبر-0/زيادة-الغرامة-على-أصحاب-السيارات-في-تركيا-2510">زيادة الغرامة على أصحاب السيارات في تركيا</a>
</h2>
<div class="entry-meta" data-animation="animated fadeInUp">
<ul>
<li>المصدر: <a href="https://8rbtna.com/" target="_blank">شبكة غربتنا</a></li>
<li>468 قراءة</li>
<li>
<ul>
<li>تاريخ النشر: 2021-12-01</li>
</ul>
</li>
</ul>
</div>
</div><!-- /.post-content -->
</div><!-- /.item -->
<div class="item" style=" background-image: url(/pic/images/news/1708224012.jpg)">
<div class="post-content">
<span class="catagory" data-animation="animated fadeInUp"><a href="#">أخبار</a></span>
<h2 class="entry-title extra_slider" data-animation="animated fadeInUp">
<a href="/تفاصيل-الخبر-0/كم-هو-عار-أن-تصل-العنصرية-إلى-هذا-المكان-فهل-يبقى-جثمان-السوري-بلا-أرض؟-2509">كم هو عار أن تصل العنصرية إلى هذا المكان فهل يبقى جثمان السوري بلا أرض؟</a>
</h2>
<div class="entry-meta" data-animation="animated fadeInUp">
<ul>
<li>المصدر: <a href="https://8rbtna.com/" target="_blank">شبكة غربتنا</a></li>
<li>470 قراءة</li>
<li>
<ul>
<li>تاريخ النشر: 2021-10-01</li>
</ul>
</li>
</ul>
</div>
</div><!-- /.post-content -->
</div><!-- /.item -->
<div class="item active" style=" background-image: url(/pic/images/information/2917035531.jpg)">
<div class="post-content">
<span class="catagory" data-animation="animated fadeInUp"><a href="#">معلومات</a></span>
<h2 class="entry-title extra_slider" data-animation="animated fadeInUp">
<a href="/تفاصيل-المعلومات-1/أفضل-10-نصائح-لتوظيف-الشخص-المناسب-414">أفضل 10 نصائح لتوظيف الشخص المناسب</a>
</h2>
<div class="entry-meta" data-animation="animated fadeInUp">
<ul>
<li>المصدر: <a href="https://8rbtna.com/" target="_blank">شبكة غربتنا</a></li>
<li>923 قراءة</li>
<li>
<ul>
<li>تاريخ النشر: 2020-13-10</li>
</ul>
</li>
</ul>
</div>
</div><!-- /.post-content -->
</div><!-- /.item -->
</div><!-- /#home-carousel -->
</div><!-- /.tr-home-slider -->
</div> <div>
<div class="col-sm-6 tr-cont">
<div class="tr-content">
<div class="tr-section bg-transparent">
<div class="latest-result text-center tr-ad ad-before">
<a href="/ads_clickers.php?id=27">
<img src="/banners/ramzy.jpeg"/>
</a>
</div>
</div>
<div class="tr-section bg-transparent">
<div class="section-title">
<div class="icon">
<i aria-hidden="true" class="fa fa-newspaper-o"></i>
</div>
<h1><a href="#">آخر الأخبار</a></h1>
</div>
<div class="tr-post">
<div class="entry-header">
<div class="entry-thumbnail">
<a href="/تفاصيل-الخبر-0/الترند-رقم-1-في-تركيا-لليوم-...-احذفوا-الواتس-آب-،-فما-هو-البديل-2508"><img alt="الترند رقم 1 في تركيا لليوم ... احذفوا الواتس آب ، فما هو البديل" class="img-responsive" src="/pic/images/news/500/2641000569.jpg" title="الترند رقم 1 في تركيا لليوم ... احذفوا الواتس آب ، فما هو البديل"/></a>
</div>
</div>
<div class="post-content">
<div class="author">
<a href="https://8rbtna.com/" target="_blank"><img alt="Image" class="img-responsive img-circle" src="/pic/images/source/1802019256.jpeg"/></a>
</div>
<div class="entry-meta">
<ul>
<li> المصدر: <a href="https://8rbtna.com/" target="_blank">شبكة غربتنا</a></li>
<li>2011 قراءة</li>
<li>
<ul>
<li>تاريخ النشر: 2021-10-01</li>
</ul>
</li>
</ul>
</div><!-- /.entry-meta -->
<h2 class="entry-title">
<a href="/تفاصيل-الخبر-0/الترند-رقم-1-في-تركيا-لليوم-...-احذفوا-الواتس-آب-،-فما-هو-البديل-2508">الترند رقم 1 في تركيا لليوم ... احذفوا الواتس آب ، فما هو البديل</a>
</h2>
</div>
</div><!-- /.tr-post -->
<div class="tr-post">
<div class="entry-header">
<div class="entry-thumbnail">
<a href="/تفاصيل-الخبر-0/منحة-تركية-جديدة-من-الحكومة-التركية-لمرحلتي-البكالوريوس-و-الدراسات-العليا-2507"><img alt="منحة تركية جديدة من الحكومة التركية لمرحلتي البكالوريوس و الدراسات العليا" class="img-responsive" src="/pic/images/news/500/3087885506.jpg" title="منحة تركية جديدة من الحكومة التركية لمرحلتي البكالوريوس و الدراسات العليا"/></a>
</div>
</div>
<div class="post-content">
<div class="author">
<a href="https://8rbtna.com/" target="_blank"><img alt="Image" class="img-responsive img-circle" src="/pic/images/source/1802019256.jpeg"/></a>
</div>
<div class="entry-meta">
<ul>
<li> المصدر: <a href="https://8rbtna.com/" target="_blank">شبكة غربتنا</a></li>
<li>549 قراءة</li>
<li>
<ul>
<li>تاريخ النشر: 2021-08-01</li>
</ul>
</li>
</ul>
</div><!-- /.entry-meta -->
<h2 class="entry-title">
<a href="/تفاصيل-الخبر-0/منحة-تركية-جديدة-من-الحكومة-التركية-لمرحلتي-البكالوريوس-و-الدراسات-العليا-2507">منحة تركية جديدة من الحكومة التركية لمرحلتي البكالوريوس و الدراسات العليا</a>
</h2>
</div>
</div><!-- /.tr-post -->
</div><!-- /.technology-section --><!--
								<div class="tr-section bg-transparent">
									<div class="latest-result text-center tr-ad ad-before">
										<a href="/ads_clickers.php?id=22">
											<img src="/images/banner_ramzy.jpg">
										</a>

									</div>
								</div>
									<div class="tr-section bg-transparent">
										<div class="latest-result text-center tr-ad ad-before">
													<img src="/images/ads_big.jpg">
										</div>

									</div> -->
<!-- /.sports-section -->
<div class="tr-section bg-transparent">
<div class="section-title">
<div class="icon">
<i aria-hidden="true" class="fa fa-info-circle"></i>
</div>
<h1><a href="#">معلومات</a></h1>
</div>
<div class="tr-post">
<div class="entry-header">
<div class="entry-thumbnail">
<a href="/تفاصيل-المعلومات-1/رويال-شارت-Royal-Chart-نظرة-عن-كثب-..-وإنجازات-مشهوده-413">
<img alt="رويال شارت Royal Chart نظرة عن كثب .. وإنجازات مشهوده" class="img-responsive" src="/pic/images/information/500/2002324098.jpeg" title="رويال شارت Royal Chart نظرة عن كثب .. وإنجازات مشهوده"/></a>
</div>
</div>
<div class="post-content">
<div class="author">
<a href="http://8rbtna.com/" target="_blank"><img alt="Image" class="img-responsive img-circle" src="/pic/images/source/1728426962.jpg"/></a>
</div>
<div class="entry-meta">
<ul>
<li> المصدر: <a href="http://8rbtna.com/" target="_blank">شبكة غربتنا</a></li>
<li>427281 قراءة</li>
<li>
<ul>
<li>تاريخ النشر: 2020-27-04</li>
</ul>
</li>
</ul>
</div><!-- /.entry-meta -->
<h2 class="entry-title">
<a href="/تفاصيل-المعلومات-1/رويال-شارت-Royal-Chart-نظرة-عن-كثب-..-وإنجازات-مشهوده-413">رويال شارت Royal Chart نظرة عن كثب .. وإنجازات مشهوده</a>
</h2>
</div>
</div><!-- /.tr-post -->
<div class="tr-post">
<div class="entry-header">
<div class="entry-thumbnail">
<a href="/تفاصيل-المعلومات-1/ضوابط-التجارة-الالكترونية-في-تركيا-412">
<img alt="ضوابط التجارة الالكترونية في تركيا" class="img-responsive" src="/pic/images/information/500/3347666130.jpg" title="ضوابط التجارة الالكترونية في تركيا"/></a>
</div>
</div>
<div class="post-content">
<div class="author">
<a href="http://8rbtna.com/" target="_blank"><img alt="Image" class="img-responsive img-circle" src="/pic/images/source/1728426962.jpg"/></a>
</div>
<div class="entry-meta">
<ul>
<li> المصدر: <a href="http://8rbtna.com/" target="_blank">شبكة غربتنا</a></li>
<li>5497 قراءة</li>
<li>
<ul>
<li>تاريخ النشر: -0001-30-11</li>
</ul>
</li>
</ul>
</div><!-- /.entry-meta -->
<h2 class="entry-title">
<a href="/تفاصيل-المعلومات-1/ضوابط-التجارة-الالكترونية-في-تركيا-412">ضوابط التجارة الالكترونية في تركيا</a>
</h2>
</div>
</div><!-- /.tr-post -->
</div><!-- /.entertainment-section -->
<div class="tr-section bg-transparent photo-gallery-two">
</div><!-- /.photo-gallery-section -->
</div><!-- /.tr-content -->
</div>
<div class="col-sm-3 tr-sidebar tr-sticky">
<div class="theiaStickySidebar">
<!-- <div class="tr-section tr-widget tr-ad ad-before">
														<a href="/ads_clickers.php?id=19">
																<img src="/images/nrc/poster_arapca112.png">
														</a>
													</div>   -->
<div class="tr-section tr-widget tr-ad ad-before">
<!-- <a href="/v3/TurkTelecome.php">
	<img class="img-responsive" src="/images/turktelecom/280_285.jpg" alt="Image">
</a>
-->
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 8rbtna bloks -->
<ins class="adsbygoogle" data-ad-client="ca-pub-7544799430842498" data-ad-slot="5520822207" style="display:inline-block;width:250px;height:250px"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
<div class="tr-section tr-widget">
<div class="widget-title">
<span><i aria-hidden="true" class="fa fa-camera"></i> فعاليات غربتنا</span>
</div>
<ul class="medium-post-list">
<li class="tr-post">
<div class="entry-header">
<div class="entry-thumbnail">
<a href="/تفاصيل-الخبر-0/غربتنا-وكلاء-مبيعات-تقيم-مؤتمرها-السنوي-الأول-بحضور-إدارة-ترك-تيليكوم-و-وكلاء-غربتنا-في-الولايات-التركية.-1660">
<img alt="غربتنا وكلاء مبيعات تقيم مؤتمرها السنوي الأول بحضور إدارة ترك تيليكوم و وكلاء غربتنا في الولايات التركية." class="img-responsive" src="/pic/images/news/250/2729555569.png" title="غربتنا وكلاء مبيعات تقيم مؤتمرها السنوي الأول بحضور إدارة ترك تيليكوم و وكلاء غربتنا في الولايات التركية."/></a>
</div>
</div>
<div class="post-content">
<h2 class="entry-title extra_small">
<a href="/تفاصيل-الخبر-0/غربتنا-وكلاء-مبيعات-تقيم-مؤتمرها-السنوي-الأول-بحضور-إدارة-ترك-تيليكوم-و-وكلاء-غربتنا-في-الولايات-التركية.-1660">غربتنا وكلاء مبيعات تقيم مؤتمرها السنوي الأول بحضور إدارة ترك تيليكوم و وكلاء غربتنا في الولايات التركية.</a>
</h2>
</div><!-- /.post-content -->
</li>
</ul> </div><!-- /.tr-section -->
<div class="tr-section tr-widget">
</div><!-- /.tr-section -->
<!-- 	<div class="tr-kanon">
														<a href="https://8rbtna.com/%D8%AA%D9%81%D8%A7%D8%B5%D9%8A%D9%84-%D8%A7%D9%84%D9%85%D8%B9%D9%84%D9%88%D9%85%D8%A7%D8%AA-1/%D8%B1%D9%88%D9%8A%D8%A7%D9%84-%D8%B4%D8%A7%D8%B1%D8%AA-Royal-Chart-%D9%86%D8%B8%D8%B1%D8%A9-%D8%B9%D9%86-%D9%83%D8%AB%D8%A8-..-%D9%88%D8%A5%D9%86%D8%AC%D8%A7%D8%B2%D8%A7%D8%AA-%D9%85%D8%B4%D9%87%D9%88%D8%AF%D9%87-413"><img src="/images/asfari.jpeg"></a>
													</div> -->
<!-- /.weather-widget -->
<div class="tr-section tr-widget">
<div class="widget-title">
<span><i aria-hidden="true" class="fa fa-smile-o"></i> نحن السوريين ناجحين</span>
</div>
<ul class="medium-post-list">
<li class="tr-post">
<div class="entry-header">
<div class="entry-thumbnail">
<a href="/تفاصيل-الخبر-0/تطبيق-جوعان:-شغف-الطعام،-جعلنا-نصدر-تطبيق-جديد-لمحبي-الطعام-في-تركيا-2477">
<img alt="تطبيق جوعان: شغف الطعام، جعلنا نصدر تطبيق جديد لمحبي الطعام في تركيا" class="img-responsive" src="/pic/images/news/250/2626340895.jpg" title="تطبيق جوعان: شغف الطعام، جعلنا نصدر تطبيق جديد لمحبي الطعام في تركيا"/></a>
</div>
</div>
<div class="post-content">
<h2 class="entry-title extra_small">
<a href="/تفاصيل-الخبر-0/تطبيق-جوعان:-شغف-الطعام،-جعلنا-نصدر-تطبيق-جديد-لمحبي-الطعام-في-تركيا-2477">تطبيق جوعان: شغف الطعام، جعلنا نصدر تطبيق جديد لمحبي الطعام في تركيا</a>
</h2>
</div><!-- /.post-content -->
</li>
<li class="tr-post">
<div class="entry-header">
<div class="entry-thumbnail">
<a href='/تفاصيل-الخبر-0/الرئاسة-التركية-تنشر-لوحة-لفنان-سوري-"تخليدا-لشهداء-15-تموز"-2456'>
<img alt='الرئاسة التركية تنشر لوحة لفنان سوري "تخليدا لشهداء 15 تموز"' class="img-responsive" src="/pic/images/news/250/3015976440.jpg" title='الرئاسة التركية تنشر لوحة لفنان سوري "تخليدا لشهداء 15 تموز"'/></a>
</div>
</div>
<div class="post-content">
<h2 class="entry-title extra_small">
<a href='/تفاصيل-الخبر-0/الرئاسة-التركية-تنشر-لوحة-لفنان-سوري-"تخليدا-لشهداء-15-تموز"-2456'>الرئاسة التركية تنشر لوحة لفنان سوري "تخليدا لشهداء 15 تموز"</a>
</h2>
</div><!-- /.post-content -->
</li>
</ul> </div><!-- /.tr-section -->
<!-- <div class="tr-section tr-widget tr-ad ad-before">
																	<a href="#"><img class="img-responsive" src="images/advertise/2.jpg" alt="Image"></a>
																</div>	 -->
</div><!-- /.theiaStickySidebar -->
</div>
</div>
</div><!-- /.row -->
</div><!-- /.container -->
</div><!-- main-wrapper -->
<footer id="footer">
<div class="footer-menu">
<div class="tr-weekly-top">
<div class="container">
<div class="section-title extra">
<span>شركاء شبكة غربتنا</span>
</div>
</div>
<div class="entertainment-content">
<div class="container">
<ul class="entertainment-slider tr-before">
<li>
<div class="entertainment">
<div class="entertainment-image">
<a href="https://firatnet.com.tr/" target="_blank">
<img alt="firatnet" class="img-responsive" src="/v3/images/post/logo55.png"/></a>
</div>
<div class="entertainment-info">
<a href="https://firatnet.com.tr/" target="_blank"><h2>شركة الفرات نت</h2></a>
<p>وكيل شبكة غربتنا في مدينة أورفا</p>
</div>
</div>
</li>
<li>
<div class="entertainment">
<div class="entertainment-image">
<a href="http://namaa-solutions.com"><img alt="Image" class="img-responsive" src="/v3/images/post/logo2.png"/></a>
</div>
<div class="entertainment-info">
<a href="http://namaa-solutions.com"><h2>نماء للحلول البرمجية</h2></a>
<p>شركة متخصصة ببناء المواقع الالكترونية وتطبيقات الموبايل</p>
</div>
</div>
</li>
<li>
<div class="entertainment">
<div class="entertainment-image">
<a href="/ads_clickers.php?id=20"><img alt="Image" class="img-responsive" src="/v3/images/post/logo1.png"/></a>
</div>
<div class="entertainment-info">
<a href="http://8rbtna.com/v3/TurkTelecome.php"><h2>تورك تيليكوم</h2></a>
<p>مشغل الانترنت والموبايل والتلفزيون في تركيا</p>
</div>
</div>
</li>
<li>
<div class="entertainment">
<div class="entertainment-image">
<a href="#"><img alt="Image" class="img-responsive" src="/v3/images/post/silkroad.png"/></a>
</div>
<div class="entertainment-info">
<a href="#"><h2>سيلك رود للاتصالات</h2></a>
<p>شريك غربتن في سوريا لبيع خطوط أهلاً</p>
</div>
</div>
</li>
<li>
<div class="entertainment">
<div class="entertainment">
<div class="entertainment-image">
<a href="http://8rbtna-magazine.com"><img alt="Image" class="img-responsive" src="/v3/images/post/logo3.png"/></a>
</div>
<div class="entertainment-info">
<a href="http://8rbtna-magazine.com"><h2>مجلة غربتنا</h2></a>
<p>مجلة اجتماعية ثقافية تصدر في تركيا ولعدة محافظات</p>
</div>
</div>
</div>
</li>
</ul><!-- /.entertainment-slider -->
</div><!-- /.container -->
</div><!-- /.entertainment-content -->
</div><!-- /.tr-weekly-top -->
</div>
<div class="footer-menu">
<div class="container">
<ul class="nav navbar-nav">
<li class="active right-dir"><a href="#">الرئيسية</a></li>
<li class="right-dir"><a href="#">من نحن؟</a></li>
<li class="right-dir"><a href="#">أعلن معنا</a></li>
<li class="right-dir"><a href="#">فريق العمل</a></li>
<li class="right-dir"><a href="/v3/contacts.php">اتصل بنا</a></li>
<li class="right-dir"><a href="/Privacy-Policy.php">سياسة الخصوصية</a></li>
</ul>
</div>
</div>
<div class="footer-widgets">
<div class="container">
<div class="row">
<div class="col-sm-4 right-dir">
<div class="widget widget-menu-2">
<h2>الأخبار</h2>
<ul>
<li><a href="/قسم--124">غربتنا وكلاء مبيعات</a></li>
<li><a href="/قسم-أخبار-ترك-تيليكم-121">أخبار ترك تيليكم</a></li>
<li><a href="/قسم-فعاليات-غربتنا-117">فعاليات غربتنا</a></li>
<li><a href="/قسم-إشاعات-115">إشاعات</a></li>
<li><a href="/قسم-مقالات-94">مقالات</a></li>
<li><a href="/قسم-اقتصاد-92">اقتصادية</a></li>
<li><a href="/قسم-سياسة-87">سياسية</a></li>
<li><a href="/قسم-قوانين-75">فرمانات</a></li>
<li><a href="/قسم-اجتماعي-27">مجتمعنا</a></li>
<li><a href="/قسم-التعليم-المدارس-والجامعات-17">قلم و محاية</a></li>
<li><a href="/قسم-ولاد-البلد-16">نحن السوريين ناجحين</a></li>
<li><a href="/قسم-أخبار-الحوادث-15">هيك عبقولوا!</a></li>
<li><a href="/قسم-رياضة-14">رياضية</a></li>
</ul>
</div>
</div>
<div class="col-sm-2 right-dir">
<div class="widget">
<h2>المعلومات</h2>
<ul>
<li><a href="/قسم-اوضاع-المعيشة-79">رغيف خبز</a></li>
<li><a href="/قسم-اوراق-رسمية-ومعاملات-قانونية-78">ختم وتوقيع</a></li>
<li><a href="/قسم-الهجرة-إلى-اوروبا-77">الهجرة</a></li>
<li><a href="/قسم-تعليم-مدارس-وجامعات-76">قلم ومحاية</a></li>
<li><a href="/قسم-هي-حكايتي-20">هي حكايتي</a></li>
</ul>
</div>
</div>
<div class="col-sm-3 right-dir">
<div class="widget">
<h2>شبكة غربتنا</h2>
<ul>
<li><a href="#">مجلة غربتنا</a></li>
<li><a href="#">غربتنا للتدريب والتطوير</a></li>
<li><a href="#">غربتنا كافيه</a></li>
</ul>
</div>
</div>
<div class="col-sm-3 right-dir">
<div class="widget widget-menu-3">
<h2>حمل التطبيق</h2>
<p>لتصفح أفضل على الموبايل حمل التطبيق الأن</p>
<ul>
<a href="https://play.google.com/store/apps/details?id=namaa.ghrbtna.syria" target="_blank">
<img src="/v3/images\google-paly.png"/>
</a>
<a href="https://itunes.apple.com/tr/app/shbkt-ghrbtna/id1137022172?mt=8&amp;ign-mpt=uo%3D4" target="_blank">
<img src="/v3/images\apple-store.png"/>
</a>
</ul>
</div>
</div>
</div><!-- /.row -->
</div>
</div>
<div class="footer-bottom text-center">
<div class="container">
<div class="footer-bottom-content">
<div class="footer-logo">
<a href="/"><img alt="شبكة غربتنا" class="img-responsive" src="/v3/images/footer-logo2.png" title="شبكة غربتنا"/></a>
</div>
<div class="social social_head">
<a class="bottomtip" href="https://www.twitter.com/8rbtna" original-title="تويتر" target="_blank"><i class="fa fa-twitter"></i></a>
<a class="bottomtip" href="https://www.facebook.com/gherbetna" original-title="فيس بوك" target="_blank"><i class="fa fa-facebook"></i></a>
<a class="bottomtip" href="http://www.youtube.com/channel/UCIC4a-NXQWhszV9SG5Hi62g" original-title="يوتيوب" target="_blank"><i class="fa fa-youtube"></i></a>
<a class="bottomtip" href="https://plus.google.com/u/0/+%D9%85%D9%86%D8%B8%D9%85%D8%A9%D8%BA%D8%B1%D8%A8%D8%AA%D9%86%D8%A7/posts" original-title="جوجل بلس" target="_blank"><i class="fa fa-google-plus"></i></a>
<a class="bottomtip" href="https://instagram.com/8rbtna_network" original-title="انتسغرام" target="_blank"><i class="fa fa-instagram"></i></a>
</div>
<!--<p> 
					شبكة غربتنا هي شبكة خدمية اجتماعية تقدم أهم الأخبار والمعلومات للاجئين السوريين في تركيا <br />عن طريق موقعها الالكتروني وتطبيقاتها على الأيفون والاندرويد. <br />
					تمتلك شبكة غربتنا العديد من الفعاليات الاجتماعية وهي:<br /> مجلة غربتنا - غربتنا للتسويق - غربتنا للتدريب والتطوير - غربتنا كافيه
				</p> -->
<br/>
<address>
<p>© 2017 جميع الحقوق محفوظة لشبكة غربتنا | طور بواسطة: 
						<a href="http://www.namaa-solutions.com" style="color:#00c2a9" target="_blank">
							نماء للحلول البرمجية
						</a>
</p>
</address>
</div><!-- /.footer-bottom-content -->
</div><!-- /.container -->
</div><!-- /.footer-bottom -->
</footer><!-- /#footer -->
<!-- JS -->
<script src="/v3/js/jquery.min.js"></script>
<script src="/v3/js/bootstrap.min.js"></script>
<script src="/v3/js/marquee.js"></script>
<script src="/v3/js/moment.min.js"></script>
<script src="/v3/js/jquery.jplayer.min.js"></script>
<script src="/v3/js/jplayer.playlist.min.js"></script>
<script src="/v3/js/slick.min.js"></script>
<script src="/v3/js/jquery.magnific-popup.js"></script>
<script src="/v3/js/carouFredSel.js"></script>
<script src="/v3/js/theia-sticky-sidebar.js"></script>
<script src="/v3/js/main.js"></script>
</div></body>
</html>