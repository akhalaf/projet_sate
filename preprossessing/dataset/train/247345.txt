<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en"><head><title>Bolide Software Products - Official web site</title>
<meta content="Bolide Software Company. The developer of All My Movies, All My Books and other programs. Download Bolide apps here." name="description"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="(C) 2003-2021 Bolide Software" name="copyright"/>
<meta content="en" http-equiv="content-language"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="noodp" name="robots"/>
<meta content="Bolide Software" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="https://www.bolidesoft.com/" property="og:url"/>
<meta content="https://www.bolidesoft.com/images/bolide-logo-text.svg" property="og:image"/>
<meta content="Bolide Software Website. Publishes All My Movies and All My Books software for collectors." property="og:description"/>
<link href="text2.css" rel="stylesheet" type="text/css"/>
<link href="https://www.bolidesoft.com/" rel="canonical"/>
<link href="https://www.bolidesoft.com/rus/" hreflang="ru" rel="alternate"/>
<script src="/myanalytics.js" type="text/javascript"></script>
<script src="/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function() {
  $.get('/offers.php', { src: 'index-eng' }, function(data) {
    document.getElementById('offer').innerHTML = data;
  });
});
</script>
</head>
<body>
<div class="header">
<ul>
<a href="/">
<li class="logo">
<p style="margin-top: 48px; float: right; font-size: 26px;"></p>
</li>
</a>
<li>
<a href="/allmymovies.html" title="Movie Organizer">
<img alt="All My Movies" class="img-responsive" src="/images/amm-logo-hlop.svg"/>
<div class="hideme">
         All My Movies
        </div>
</a>
</li>
<li>
<a href="/allmybooks.html" title="Personal Book Database">
<img alt="All My Books" class="img-responsive" src="/images/amb-logo-book.svg"/>
<div class="hideme">
         All My Books
        </div>
</a>
</li>
<li>
<a href="/movie-creator.html" title="Video Editor">
<img alt="Bolide Movie Creator" class="img-responsive" src="/images/bmc-logo.svg"/>
<div class="hideme">
         Bolide Movie Creator
        </div>
</a>
</li>
<li>
<a href="https://altercam.com/" target="_blank" title="Virtual Webcam">
<img alt="AlterCam" class="img-responsive" src="/images/altercam-logo.svg"/>
<div class="hideme">
         AlterCam
        </div>
</a>
</li>
<li>
<a href="https://duplicatevideosearch.com/" target="_blank" title="Delete Duplicate Video Files">
<img alt="Duplicate Video Search" class="img-responsive" src="/images/dvs-logo.svg"/>
<div class="hideme">
         Duplicate Video Search
        </div>
</a>
</li>
</ul>
</div>
<!-- enf of header -->
<div class="content">
<div class="maincontent">
<h1>Download Bolide® Software Products</h1>
<div id="offer"></div>
<p>Here you will find good <b>movie and book cataloging</b> software, smart <b>duplicate removers</b>, ultimate <b>webcam software</b>, and excellent <b>video editing software</b> developed by Bolide®.</p>
<p> </p>
<div class="blue-container">
<a href="/allmymovies.html"><h2>All My Movies v8.9 - Movie Organizer</h2></a>
<div>
<img alt="All My Movies by Bolide" src="images/tn_amm_shelf1.png" style="float:left;  border: none; text-align: left; margin-right: 10px; margin-top: 5px;"/>
<strong>The best way to archive, organize, track and maintain your movie collection</strong><br/>
                Whether you're looking to catalog your collection or <a href="/allmymovies/keep-track-movies.html">track which movies</a> you've watched, All My Movies has the tools you need to begin building your own movie database. Our flexible, user-friendly interface makes archiving, organizing, and tracking your movies faster than ever. With access to all the major online movie repositories (Amazon.com, IMDb, etc.) adding your first movie is as simple as typing in the title. AMM will work behind the scenes; automatically gathering information about that movie! See firsthand how fun it is to build your own <a href="/movie_database.html">movie database</a>! Similar to what you've seen at your local library and movie rental shop, AMM is even better because it's based 100% on your own unique collection.
            </div>
<div>
                Localized pages: <a href="/rus/allmymovies.html">Russian</a> | <a href="/allmymovies_de.html">German</a> | <a href="/allmymovies_es.html">Spanish</a> | <a href="/allmymovies_fr.html">French</a> | <a href="/allmymovies_pl.html">Polish</a> | <a href="/allmymovies_tr.html">Turkish</a>
</div>
<div class="centered">
<a class="myButton" href="/software/amm_setup.exe" onclick="ga('send', 'pageview', '/AMM_Download');">FREE DOWNLOAD</a>
</div>
</div>
<div class="blue-container">
<a href="/allmybooks.html"><h2>All My Books v5.2 - Personal Book Catalog</h2></a>
<div>
<img alt="All My Books by Bolide" src="images/tn_amb_shelf1.png" style="float:left;  border: none; text-align: left; margin-right: 10px; margin-top: 5px;"/>
<strong>Digitize your collection and begin building your personal library</strong>
<br/>The only cataloging software you'll ever need. All My Books helps you archive, organize and track your collection through an easy to use, flexible interface. Whether you're working with printed, audio, e-books — or a combination of all three; All My Books has exactly what you need to catalogue your entire collection. If you've ever imagined how cool it would be to have access to a book cataloging program as powerful as your local library, there's no need to wonder anymore. Take your private collection to the next level through familiar functionality and dozens of fun features.
            </div>
<div>
                Localized pages: <a href="/rus/allmybooks.html">Russian</a> | <a href="/allmybooks_de.html">German</a>
</div>
<div class="centered">
<a class="myButton" href="/software/amb_setup.exe" onclick="ga('send', 'pageview', '/AMB_Download');">FREE DOWNLOAD</a>
</div>
</div>
<div class="blue-container">
<a href="https://AlterCam.com/"><h2>AlterCam v5.4 - Live Webcam Effects</h2></a>
<div>
<img alt="AlterCam by Bolide" src="images/altercam200.png" style="float:left;  border: none; text-align: left; margin-right: 10px; margin-top: 5px;"/>
<strong>Customize the appearance and functionality of your webcasts</strong>
<br/>Are you tired of every video feed looking the same? Wish that you could change the way your webcam videos appear? Now you can! AlterCam lets you add lots of cool visual effects to any Live webcam feed in real-time. Capable of handling HD resolutions, AlterCam is ready to help you edit everything from one-on-one video chats to large conference room meetings. Whether you're Skyping from a laptop or video conferencing over your PC, AlterCam helps you customize your video presentations.
            </div>
<div>
                Localized pages: <a href="https://AlterCam.com/rus/">Russian</a> | <a href="https://AlterCam.com/es/">Spanish</a>
</div>
<div class="centered">
<a class="myButton" href="https://AlterCam.com/software/altercam-setup.exe" onclick="ga('send', 'pageview', '/ALC_Download');" rel="nofollow">FREE DOWNLOAD</a>
</div>
</div>
<div class="blue-container">
<a href="/movie-creator.html"><h2>Bolide® Movie Creator v4.1 - HD Video Editor</h2></a>
<div>
<img alt="Bolide Movie Creator" src="images/bmc_scr_main200.png" style="float:left;  border: none; text-align: left; margin-right: 10px; margin-top: 5px;"/>
<strong>A simple, yet powerful introduction to the world of video editing</strong><br/>
                The perfect video editor for both amateur and advanced videographers. Making your own movies has never been simpler or more fun. Most video editoring software come with a high learning curve  not Bolide Movie Creator though! Featuring all the tools necessary to begin editing videos, the BMC lets you join, split, and trim videoclips. You can even add text commentaries, photos and background music. Choose from dozens of transition effects and  begin creating your own videos today! No previous video editing knowledge is needed to take advatage of our user-friendly interface. With support for AVI, FLV, MP4, MKV and WMV the Bolide Movie Creator is ready to help boost your video projects to the next level.
            </div>
<div>
                Localized pages: <a href="/rus/movie-creator.html">Russian</a> | <a href="https://movie-creator.com/de/">German</a> | <a href="https://movie-creator.com/es/">Spanish</a>
</div>
<div class="centered">
<a class="myButton" href="https://movie-creator.com/software/bmc_setup.exe" onclick="ga('send', 'pageview', '/BMC_Download');" rel="nofollow">FREE DOWNLOAD</a>
</div>
</div>
<div class="blue-container">
<a href="https://slideshow-creator.com"><h2>Bolide® Slideshow Creator v2.2 - <span style="color: red">Free</span></h2></a>
<div>
<img alt="Bolide Slideshow Creator" src="images/bsc_common_thumb.png" style="float:left;  border: none; text-align: left; margin-right: 10px; margin-top: 5px;"/>
<strong>Create beautiful video slideshows</strong><br/>
                Why spend hours setting up a slideshow when our program can help you do it in minutes? All you need to do is drop photos onto the timeline, choose your transitions, add some music and you're ready to go! Our easy-to-use interface makes creatign slideshows simple, fast and fun. Capable of handing resolutions ranging from 128x160 to 1920x1080(FullHD) the Bolide Slideshow Creator lets you work with just about any image format you can imagine. Featuring dozens of transition effects and audio support, the Bolide Slideshow Creator is ready to help you take your slideshows to the next level!
            </div>
<div>
                Localized pages: <a href="https://rus.slideshow-creator.com/">Russian</a> | <a href="https://slideshow-creator.com/de/">German</a> | <a href="https://slideshow-creator.com/es/">Spanish</a> | <a href="https://slideshow-creator.com/br/">Brazillian</a> | <a href="https://slideshow-creator.com/fr/">French</a> | <a href="https://slideshow-creator.com/it/">Italian</a> | <a href="https://slideshow-creator.com/nl/">Dutch</a>
</div>
<div class="centered">
<a class="myButton" href="https://slideshow-creator.com/software/bsc_setup.exe" onclick="ga('send', 'pageview', '/BSC_Download');" rel="nofollow">FREE DOWNLOAD</a>
</div>
</div>
<div class="blue-container">
<a href="/audiocomparer/"><h2>Audio Comparer v1.7 - Duplicate Song Remover</h2></a>
<div>
<img alt="Audio Comparer by Bolide" src="images/ac_main198.png" style="float:left;  border: none; text-align: left; margin-right: 10px; margin-top: 5px;"/>
<strong>Clean up your drive by removing duplicate audio files</strong><br/>
                With the Audio Comparer you'll soon be organizing your digital audio collection faster than you ever dreamt possible. Using unique recognition technology, Audio Comparer doesn't just scan your files; it actually "listens" to them and is capable of comparing different audio files note for note. Able to work with MP3, MP2, MP1, WMA, AIF, WAV, WavPack, FLAC, APE, AAC and OGG, the Audio Comparer offers unprecedented file comparison abilities. You will never experience the hassle of duplicate audio files again!
            </div>
<div>
                Localized pages: <a href="https://audiocomparer.com/rus/">Russian</a> | <a href="https://audiocomparer.com/de/">German</a> | <a href="https://audiocomparer.com/es/">Spanish</a> | <a href="https://audiocomparer.com/fr/">French</a> | <a href="https://audiocomparer.com/nl/">Dutch</a>
</div>
<div class="centered">
<a class="myButton" href="https://AudioComparer.com/software/ac_setup.exe" onclick="ga('send', 'pageview', '/AC_Download');" rel="nofollow">FREE DOWNLOAD</a>
</div>
</div>
<div class="blue-container">
<a href="/imagecomparer.html"><h2>Image Comparer v3.8 - Duplicate Photo Remover</h2></a>
<div>
<img alt="Image Comparer by Bolide" src="images/tn_ic_tree.png" style="float:left;  border: none; text-align: left; margin-right: 10px; margin-top: 5px;"/>
<strong>Save space by searching for and removing duplicate images</strong><br/>
                No more having to slowly sift through huge image collections; taking notes on which photos to keep or delete. Image Comparer scans your image collections, analyzes their content and locates the files that look alike. With the newest version of Image Comparer you can even highlight image differences! If you need a tool that will help you clean up your hard drive, organize your digital photos, and detect/remove duplicate images all at the same time — then look no further.
            </div>
<div>
                Localized pages: <a href="/rus/imagecomparer.html">Russian</a> | <a href="https://www.imagecomparer.com/de/">German</a> | <a href="https://www.imagecomparer.com/es/">Spanish</a> | <a href="https://www.imagecomparer.com/nl/">Dutch</a>
</div>
<div class="centered">
<a class="myButton" href="/software/ic_setup.exe" onclick="ga('send', 'pageview', '/IC_Download');">FREE DOWNLOAD</a>
</div>
</div>
<div class="blue-container">
<a href="https://duplicatevideosearch.com/"><h2>Duplicate Video Search</h2></a>
<div>
<img alt="Duplicate Video Search by Bolide" src="images/dvs_screen200.png" style="float:left;  border: none; text-align: left; margin-right: 10px; margin-top: 5px;"/>
<strong>Rescue drive space by removing duplicate video files</strong><br/>
                You might think that a 4Gb video file is a difficult thing to lose track of, but these days it happens all too often. It isn't just the big files either. Hundreds of small video files build up over time; eating up our free hd space. When space is precious, it's time to start cleaning up your drive(s) and the last you think you want  are dozens of duplicate videos. The Duplicate Video Search tool can quickly locate any video duplicates and delete them in just a few clicks. Scan your drive based on bit-rates, formats, and resolutions. Look for converted, scaled, cropped, low and high quality videos, and so much more. Wherever your duplicates are hiding, the Duplicate Video Search tool will find them!
            </div>
<div>
                Localized pages: <a href="https://duplicatevideosearch.com/rus/">Russian</a>
</div>
<div class="centered">
<a class="myButton" href="https://duplicatevideosearch.com/download/dvs_setup.exe" onclick="ga('send', 'pageview', '/DVS_Download');" rel="nofollow">FREE DOWNLOAD</a>
</div>
</div>
</div>
<div class="rightcol">
<h3 class="centered">Bolide® Software News</h3>
<ul>
<li>December 17, 2020 Website updated to a new design. Lighter and simpler. </li>
<li>December 05, 2020 <a href="https://AlterCam.com/">AlterCam 5.4</a> <a href="https://AlterCam.com/history.html" rel="nofollow">Changes list.</a></li>
<li>October 19, 2020  We released <a href="/allmybooks.html">All My Books 5.2</a>! Browse the <a href="/allmybooks/whatsnew.html">changes list.</a></li>
<li>August 14, 2020  AlterCam 5.3 - now you can crop main video layer as well as the overlays. </li>
<li>July 10, 2020  AlterCam 5.2 - with better Chromakey and enhanced audio functions.</li>
<li>May 31, 2020  AlterCam 5.1 - new version, with 4k resolution support and better virtual sound card driver.</li>
<li>April 27, 2020  AlterCam 5.0 - major upgrade with the better virtual webcam driver.</li>
<li>March 21, 2020  We released <a href="/allmybooks.html">All My Books 5.1</a></li>
<li>August 15, 2019  Finally! All My Books 5.0 is live! With Goodreads support and more!</li>
<li>August 12, 2019  New <a href="https://DuplicateVideoSearch.com/">Duplicate Video Search</a> version. Now you can scan for video duplicates in multithreaded mode!</li>
<li>August 12, 2019  AlterCam 4.9 -  with new amazing effects.</li>
<li>May 04, 2019  <a href="https://movie-creator.com/">Bolide Movie Creator 4.1</a> - now with project templates and more! <a href="https://movie-creator.com/whatsnew.html" rel="nofollow">Changes list.</a></li>
<li>April 15, 2019  AlterCam 4.8 -  new effects!</li>
<li>January 28, 2019  AlterCam 4.7 -  with ability to shoot photo series!</li>
<li>January 10, 2019  Bolide Movie Creator 4.0 - with video reverse and lots of new stuff!</li>
<li>October 03, 2018  AlterCam 4.6 - now with lower-third overlays!</li>
<li>September 11, 2018  Bolide Movie Creator 3.9 - improved a lot!</li>
<li>June 08, 2018  Bolide Movie Creator 3.8</li>
<li>May 01, 2018  Bolide Movie Creator 3.7</li>
<li>April 03, 2018  Bolide Movie Creator 3.6</li>
<li>March 27, 2018  AlterCam 4.5</li>
<li>February 25, 2018  AlterCam 4.4</li>
<li>January 15, 2018  Bolide Movie Creator 3.5 is available for download!</li>
<li>December 24, 2017  New AlterCam 4.3 is available!</li>
<li>September 13, 2017  New version of our video editing software Bolide Movie Creator is available for download!</li>
<li>March 03, 2017  AlterCam 4.1 has been released!</li>
<li>February 09, 2017  New All My Movies 8.9 version has been released! <a href="/allmymovies/whatsnew.html">Changes list.</a></li>
<li>January 22, 2017  Popular video editing software Bolide Movie Creator has been updated!</li>
<li>October 27, 2016  Celebrate All My Books 4.9 release!</li>
<li>September 22, 2016  AlterCam 3.9 has been released!</li>
<li>September 04, 2016  We released Bolide Movie Creator 3.0 with 50+ new effects and a lot of improvements.</li>
<li>September 01, 2016  New AlterCam 3.8 version is available for download!</li>
<li>June 14, 2016  Good news, everyone! We just released All My Movies 8.8!</li>
<li>May 19, 2016  All My Books 4.8 is live!</li>
<li>April 14, 2016  All My Movies 8.7 has been released!</li>
<li>March 24, 2016  A long-awaited <a href="/allmybooks/android.html">All My Books for Android</a> has been released!</li>
<li>March 02, 2016  We released All My Books 4.7!</li>
</ul>
<p> </p>
<p><a href="news.html">Archived Bolide® Software news... (nobody read them, really)</a></p>
</div> <!-- rightcol -->
</div> <!-- content -->
<div class="footer">
<div class="footer-logo">
<img alt="Bolide Square Logo" height="120" src="/images/bolide-logo-sq.svg" width="120"/>
</div>
<ul>
<li><a href="/about.html">About Us</a></li>
<li><a href="/purchase.html">Purchase</a></li>
<li><a href="/feedback.html">Contact Us</a></li>
<li><a href="/affiliates.html">Affiliates</a></li>
<li><a href="/sitemap/">Site map</a></li>
</ul>
<ul>
<li><a href="/privacy.html">Privacy Policy</a></li>
<li><a href="/tos.html">Terms of Use</a></li>
<li><a href="/rus/">Switch to Russian</a></li>
</ul>
<div class="footer-copy">Copyright ©2003-2021 by Bolide® Software</div>
</div>
</body></html>
