<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>Page not found «  dPVS</title>
<link href="https://digitalvirgil.co.uk/wp-content/themes/wikiwp/images/icon.png" rel="shortcut icon" type="image/x-icon"/>
<link href="https://digitalvirgil.co.uk/wp-content/themes/wikiwp/style.css" media="screen,projection" rel="stylesheet" type="text/css"/>
<link href="https://digitalvirgil.co.uk/xmlrpc.php" rel="pingback"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://digitalvirgil.co.uk/feed/" rel="alternate" title="dPVS » Feed" type="application/rss+xml"/>
<link href="https://digitalvirgil.co.uk/comments/feed/" rel="alternate" title="dPVS » Comments Feed" type="application/rss+xml"/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.10.4 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script data-cfasync="false" type="text/javascript">
	var mi_version         = '7.10.4';
	var mi_track_user      = true;
	var mi_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-1618440-8';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-1618440-8', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('require', 'displayfeatures');
		__gaTracker('send','pageview','/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer);
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<!-- / Google Analytics by MonsterInsights -->
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/digitalvirgil.co.uk\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://digitalvirgil.co.uk/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://digitalvirgil.co.uk/wp-content/plugins/wpfront-notification-bar/css/wpfront-notification-bar.css?ver=1.7.1" id="wpfront-notification-bar-css" media="all" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
//<![CDATA[
var pdfpptWidth = 400;
var pdfpptHeight = 500;
//]]>
</script>
<script id="monsterinsights-frontend-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[{\"path\":\"\\\/go\\\/\",\"label\":\"affiliate\"},{\"path\":\"\\\/recommend\\\/\",\"label\":\"affiliate\"}]","home_url":"https:\/\/digitalvirgil.co.uk","hash_tracking":"false"};
/* ]]> */
</script>
<script id="monsterinsights-frontend-script-js" src="https://digitalvirgil.co.uk/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.10.4" type="text/javascript"></script>
<script id="jquery-core-js" src="https://digitalvirgil.co.uk/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="jquery.cookie-js" src="https://digitalvirgil.co.uk/wp-content/plugins/wpfront-notification-bar/jquery-plugins/jquery.c.js?ver=1.4.0" type="text/javascript"></script>
<script id="wpfront-notification-bar-js" src="https://digitalvirgil.co.uk/wp-content/plugins/wpfront-notification-bar/js/wpfront-notification-bar.js?ver=1.7.1" type="text/javascript"></script>
<script id="pdfppt_settings-js" src="https://digitalvirgil.co.uk/wp-content/plugins/pdf-ppt-viewer/pdfppt-settings.js.php?ver=5.5.3" type="text/javascript"></script>
<script id="pdfppt_renderer-js" src="https://digitalvirgil.co.uk/wp-content/plugins/pdf-ppt-viewer/pdfppt-renderer.js?ver=5.5.3" type="text/javascript"></script>
<link href="https://digitalvirgil.co.uk/wp-json/" rel="https://api.w.org/"/><link href="https://digitalvirgil.co.uk/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://digitalvirgil.co.uk/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
</head>
<body>
<div id="logo">
<a href="https://digitalvirgil.co.uk"><img src="https://digitalvirgil.co.uk/wp-content/themes/wikiwp/images/logo.png"/></a>
<!-- Change the logo with your own. Use 155x155px sized image, preferably .PNG -->
</div>
<div id="header">
<h1><a href="https://digitalvirgil.co.uk/"><font color="993300">dPVS</font></a></h1>
<h4>the digital repository for the Proceedings of the Virgil Society</h4>
</div><div id="leftside">
<ul class="sideul">
<li class="widget widget_search" id="search-2"><form action="https://digitalvirgil.co.uk/" class="searchform" id="searchform" method="get" role="search">
<div>
<label class="screen-reader-text" for="s">Search for:</label>
<input id="s" name="s" type="text" value=""/>
<input id="searchsubmit" type="submit" value="Search"/>
</div>
</form></li><li class="widget widget_text" id="text-2"> <div class="textwidget"><p><a href="http://digitalvirgil.co.uk/?p=979">Presidential Address, 1948</a><a href="http://digitalvirgil.co.uk/?page_id=210" title="Volume 1 (1961)"><br/>
VS1 (1961-1962)</a><br/>
<a href="http://digitalvirgil.co.uk/?page_id=235" title="Volume 2 (1962)">VS2 (1962-1963)</a><br/>
<a href="http://digitalvirgil.co.uk/?page_id=260" title="Volume 3 (1963)">VS3 (1963-1964)</a><br/>
<a href="http://digitalvirgil.co.uk/?page_id=283" title="Volume 4 (1964)">VS4 (1964-1965)</a><br/>
<a href="http://digitalvirgil.co.uk/?page_id=316" title="Volume 5 (1965)">VS5 (1965-1966)</a><br/>
<a href="http://digitalvirgil.co.uk/?page_id=341" title="Volume 6 (1966)">VS6 (1966-1967)</a><br/>
<a href="http://digitalvirgil.co.uk/?page_id=370" title="Volume 7 (1967)">VS7 (1967-1968)</a><br/>
<a href="http://digitalvirgil.co.uk/?page_id=453" title="Volume 8 (1968)">VS8 (1968-1969)</a><br/>
<a href="http://digitalvirgil.co.uk/?page_id=482" title="Volume 9 (1969)">VS9 (1969-1970)<br/>
</a><a href="http://digitalvirgil.co.uk/?page_id=507" title="Volume 10 (1970)">VS10 (1970-1971)<br/>
</a><a href="http://digitalvirgil.co.uk/?page_id=531" title="Volume 11 (1971)">VS11 (1971-1972)<br/>
</a><a href="http://digitalvirgil.co.uk/?page_id=559" title="Volume 12 (1972)">VS12 (1972-1973)<br/>
</a><a href="http://digitalvirgil.co.uk/?page_id=579" title="Volume 13 (1973)">VS13 (1973-1974)<br/>
</a><a href="http://digitalvirgil.co.uk/?page_id=655" title="Volume 14 (1974)">VS14 (1974-1975)<br/>
</a><a href="http://digitalvirgil.co.uk/?page_id=675" title="Volume 15 (1975)">VS15 (1975-1976)<br/>
</a><a href="http://digitalvirgil.co.uk/?page_id=701" title="Volume 16 (1976)">VS16 (1976-1977)<br/>
</a><a href="http://digitalvirgil.co.uk/?page_id=783" title="Volume 17 (1978)">VS17 (1978-1979)</a><br/>
<a href="http://digitalvirgil.co.uk/?page_id=724" title="Volume 18 (1986)">VS18 (1986-1987)<br/>
</a><a href="http://digitalvirgil.co.uk/?page_id=861" title="Volume 19 (1988)">VS19 (1988-1989)<br/>
</a><a href="http://digitalvirgil.co.uk/?page_id=890" title="Volume 20 (1991)">VS20 (1991-1992)</a><br/>
<a href="http://digitalvirgil.co.uk/?page_id=748" title="Volume 21 (1993)">VS21 (1993-1994)<br/>
</a><a href="http://digitalvirgil.co.uk/?page_id=803" title="Volume 22 (1996)">VS22 (1996-1997)<br/>
</a><a href="http://digitalvirgil.co.uk/?page_id=827" title="Volume 23 (1998)">VS23 (1998-1999)<br/>
</a><a href="http://digitalvirgil.co.uk/?page_id=916" title="Volume 24 (2001)">VS24 (2001-2002)</a><br/>
<a href="http://digitalvirgil.co.uk/?page_id=31" title="Volume 25 (2004)">VS25 (2004-2005)<br/>
</a><a href="http://digitalvirgil.co.uk/?page_id=101" title="Volume 26 (2008)">VS26 (2008-2009)</a><br/>
<a href="http://digitalvirgil.co.uk/?page_id=177" title="Volume 27 (2011)">VS27 (2011-2012)<br/>
</a><a href="http://digitalvirgil.co.uk/?page_id=961" title="Volume 28 (2013)">VS28 (2013-2014)</a><br/>
<a href="http://digitalvirgil.co.uk/volume-29-2017/">VS29 (2017)</a></p>
</div>
</li>
<li class="widget">Meta<ul>
<li><a href="https://digitalvirgil.co.uk/wp-login.php">Log in</a></li>
</ul></li>
</ul>
</div>
<div id="home">
<h1>
  Page not found</h1>
<div class="navigation">
<div class="alignleft"></div>
<div class="alignright"></div>
</div>
<br/>
</div><!--home-->
<div id="footer">
<strong><a href="https://digitalvirgil.co.uk/">dPVS</a></strong>.  Copyright<a href="http://www.virgilsociety.org.uk"> The Virgil Society</a> Webmaster<a href="vaemihi.wordpress.com"> Cristian Ispir</a> 2017
</div>
<style type="text/css">
    #wpfront-notification-bar 
    {
        background: #ff0000;
        background: -moz-linear-gradient(top, #ff0000 0%, #ff0000 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ff0000), color-stop(100%,#ff0000));
        background: -webkit-linear-gradient(top, #ff0000 0%,#ff0000 100%);
        background: -o-linear-gradient(top, #ff0000 0%,#ff0000 100%);
        background: -ms-linear-gradient(top, #ff0000 0%,#ff0000 100%);
        background: linear-gradient(to bottom, #ff0000 0%, #ff0000 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff0000', endColorstr='#ff0000',GradientType=0 );
    }

    #wpfront-notification-bar div.wpfront-message
    {
        color: #ffffff;
    }

    #wpfront-notification-bar a.wpfront-button
    {
        background: #00b7ea;
        background: -moz-linear-gradient(top, #00b7ea 0%, #009ec3 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#00b7ea), color-stop(100%,#009ec3));
        background: -webkit-linear-gradient(top, #00b7ea 0%,#009ec3 100%);
        background: -o-linear-gradient(top, #00b7ea 0%,#009ec3 100%);
        background: -ms-linear-gradient(top, #00b7ea 0%,#009ec3 100%);
        background: linear-gradient(to bottom, #00b7ea 0%, #009ec3 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00b7ea', endColorstr='#009ec3',GradientType=0 );

        color: #ffffff;
    }

    #wpfront-notification-bar-open-button
    {
        background-color: #00b7ea;
    }

    #wpfront-notification-bar  div.wpfront-close 
    {
        border: 1px solid #555555;
        background-color: #555555;
        color: #000000;
    }

    #wpfront-notification-bar  div.wpfront-close:hover 
    {
        border: 1px solid #aaaaaa;
        background-color: #aaaaaa;
    }
</style>
<div id="wpfront-notification-bar-spacer" style="display: none;">
<div class="top wpfront-bottom-shadow" id="wpfront-notification-bar-open-button"></div>
<div class="wpfront-fixed " id="wpfront-notification-bar">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td>
<div class="wpfront-message">
                        Volume 29 (2017) of the Proceedings of the Virgil Society has just been added. Click <a href="volume-29-2017">here</a> to open.                    </div>
<div>
</div>
</td>
</tr>
</table>
</div>
</div>
<style type="text/css">
</style>
<script type="text/javascript">if(typeof wpfront_notification_bar == "function") wpfront_notification_bar({"position":1,"height":0,"fixed_position":false,"animate_delay":0.5,"close_button":false,"button_action_close_bar":false,"auto_close_after":0,"display_after":1,"is_admin_bar_showing":false,"display_open_button":false,"keep_closed":false,"keep_closed_for":0,"position_offset":0,"display_scroll":false,"display_scroll_offset":100});</script><script id="wp-embed-js" src="https://digitalvirgil.co.uk/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
</body>
</html>