<html>
<head>
<meta content="nl" http-equiv="Content-Language"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Western Riding - Art of Control</title>
</head><body background="wood3.gif">
<table border="0" cellpadding="0" cellspacing="0" height="3631" width="1262">
<!-- MSTableType="layout" -->
<tr>
<td rowspan="2" valign="top">
<!-- MSCellType="DecArea" -->
<p align="center">
<img border="0" height="27" src="compu1.gif" width="211"/><a href="mailto:frooij@gmail.com"><img align="center" border="0" height="81" src="e-mailpaal.gif" style="float: left" width="93"/></a></p></td>
<td colspan="2" valign="top">
<!-- MSCellType="ContentHead" -->
<p align="center">
<img border="0" height="70" src="westernridingbannernew.gif" width="705"/></p></td>
<td height="71"></td>
</tr>
<tr>
<td height="37"></td>
<td bordercolor="#FFFF00" bordercolordark="#00FF00" bordercolorlight="#FFFF00" colspan="2" rowspan="2" valign="top">
<!-- MSCellType="ContentBody" -->
<div><div align="left">
 </div>
<div align="center"><font color="#008080" face="Alba Super" size="6">Last updated:
	<!--webbot bot="Timestamp" s-type="REGENERATED" s-format="%d.%m.%Y" startspan -->06.11.2013<!--webbot bot="Timestamp" i-checksum="12483" endspan --></font>
</div>
<p align="center">
<img border="0" src="index11.gif"/></p>
<p align="center">
<img border="0" height="395" src="website-under-construction.gif" width="385"/></p>
<table border="1" width="100%">
<tr>
<td bordercolor="#FFFF00" bordercolordark="#FFFF00" bordercolorlight="#FFFF00" height="0" width="0">
<p align="center"><font face="Arial Black">
<span style="background-color: #FFFF00">Momenteel vinden er 
			werkzaamheden plaats aan deze website.</span></font></p>
<p align="center"><font face="Arial Black">
<span style="background-color: #FFFF00">Excuse voor het ongemak.</span></font></p></td>
</tr>
</table>
<center>
<p align="center"> </p>
<table border="1" cellpadding="10" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0" width="78%">
<tr>
<td bordercolor="#FF0000" bordercolordark="#800000" bordercolorlight="#FF0000" style="line-height: 100%; word-spacing: 0; margin-top: 0; margin-bottom: 0" width="100%">
<p align="center"><font color="#FF0000" face="Alba Super" size="6">
		AKTUEEL</font></p>
<ol>
<li>
<p align="center" style="line-height: 150%; margin-top: 5; margin-bottom: 5">
<font face="Arial">Vanaf 23 april 2011 staan onze paarden op
			<a href="http://www.stalbalemans.com" target="_blank">Stal 
			Balemans</a>.</font></p></li>
<li>
<p align="center" style="line-height: 150%; margin-top: 5; margin-bottom: 5">
<font face="Arial">September 2012: Reminic's Girl is verkocht.</font></p></li>
<li>
<p align="center" style="line-height: 150%; margin-top: 5; margin-bottom: 5">
<font face="Arial">Marvel is twee maanden in training geweest bij 
			JP. Sinds 01 september 2013 weer thuis.</font></p></li>
</ol>
</td>
</tr>
</table>
</center>
<table border="0" cellpadding="0" cellspacing="0" height="254" width="100%">
<tr>
<td background="wood3.gif" bgcolor="#FEEBC5" height="162" width="100%">
<p align="center"> <!-- \/ GuestGEAR Code by http://htmlgear.com \/ -->                            
		</p>
<p align="center">                            
		<img border="0" height="438" src="fred1.jpg" width="526"/>                                </p>
<p class="MsoNormal" style="line-height: 100%; margin-top: 0; margin-bottom: 0"> </p>
<p align="center" class="MsoNormal" style="line-height: 100%; margin-top: 0; margin-bottom: 0">
 </p>
<p align="center" class="MsoNormal" style="line-height: 100%; margin-top: 0; margin-bottom: 0">
<a href="Indianenverhaal.htm" target="_blank">
<img alt="Een Indianenverhaal" border="0" height="137" src="Indiantalk.gif" width="125"/></a></p>
<p align="left" class="MsoNormal" style="line-height: 100%; margin-top: 0; margin-bottom: 0"> </p>
<p align="center" class="MsoNormal" style="line-height: 100%; margin-top: 0; margin-bottom: 0">
<b><font color="#800000" face="Monotype Corsiva" size="5">Indiantalk</font></b>
</p>
<p align="center" class="MsoNormal" style="line-height: 100%; margin-top: 0; margin-bottom: 0">     </p>
<p align="left" class="MsoNormal" style="line-height: 100%; margin-top: 0; margin-bottom: 0"> </p>
<p align="center" class="MsoNormal" style="line-height: 100%; margin-top: 0; margin-bottom: 0">
<img border="0" height="26" src="Febar1.jpg" width="481"/></p>
<p align="center" class="MsoNormal" style="line-height: 100%; margin-top: 0; margin-bottom: 0">
 </p>
<p align="center">
 </p>
</td>
</tr>
</table>
</div>
 </td>
</tr>
<tr>
<td valign="top">
<!-- MSCellType="NavBody" -->
<table border="5" bordercolordark="#800000" bordercolorlight="#CC3300" height="348" width="220">
<!-- MSTableType="layout" -->
<tr>
<td height="23"><font face="Arial" size="3"><u><b>Inhoud:</b></u></font></td>
</tr>
<tr>
<td height="23">
<a href="http://www.westernriding-frooy.com/Westernriding.htm">Home</a></td>
</tr>
<tr>
<td height="20"><font face="Arial" size="2">
<a href="Even%20voorstellen.htm" target="_blank">Even 
		voorstellen.</a></font></td>
</tr>
<tr>
<td height="20"><font face="Arial" size="2">
<a href="Woord%20vooraf.htm" target="_blank">Woord vooraf</a></font></td>
</tr>
<tr>
<td height="20"><font face="Arial" size="2">
<a href="Wat%20is%20Western%20Riding.htm" target="_blank">Wat 
		is Western Riding?</a></font></td>
</tr>
<tr>
<td height="20"><font face="Arial" size="2">
<a href="De%20Geschiedenis.htm" target="_blank">De 
		geschiedenis.</a></font></td>
</tr>
<tr>
<td height="20"><font face="Arial" size="2">
<a href="De%20Verschillende%20Stijlen.htm" target="_blank">De 
		verschillende stijlen.</a></font></td>
</tr>
<tr>
<td height="20"><font face="Arial" size="2">
<a href="Paardenrassen.htm" target="_blank">Paardenrassen.</a></font></td>
</tr>
<tr>
<td height="20"><font face="Arial" size="2">
<a href="Wedstrijd-onderdelen.htm" target="_blank">De 
		wedstrijdonderdelen.</a></font></td>
</tr>
<tr>
<td height="20">
<p align="left"><font face="Arial" size="2">
<a href="Western-harnachement.htm" target="_blank">Western-harnachement</a><a href="http://www.westernriding-frooy.com/Westernuitrusting.htm">.</a></font></p></td>
</tr>
<tr>
<td height="20">
<p align="left"><font face="Arial" size="2">
<a href="Wetenswaardigheden.htm" target="_blank">Wetenswaardigheden.</a></font></p></td>
</tr>
<tr>
<td height="20">
<p align="left"><font face="Arial" size="2">
<a href="Naar%20de%20foto's.htm" target="_blank">Naar de 
		foto's.</a></font></p></td>
</tr>
<tr>
<td height="20">
<p align="left">
<font face="Arial" size="2"><a href="Agenda.htm" target="_blank">Agenda.</a></font></p></td>
</tr>
<tr>
<td height="20">
<p align="left"><font face="Arial" size="2">
<a href="Links%20en%20advertenties.htm" target="_blank">Links en 
		advertenties</a></font></p></td>
</tr>
<tr>
<td height="20" width="202">
<p align="left"><font face="Arial" size="2">
<a href="Onze%20hond%20Chamook.htm" target="_blank">Onze hond Chamook</a></font></p></td>
</tr>
</table>
<p align="center"> </p>
<p align="center">
		 </p>
<p align="center">
<a href="http://www.tboek.nl/gastenboek/frooij" target="_blank">
<img alt="Gastenboek" border="0" height="146" src="Gastenboek.jpg" width="151"/></a></p>
<p align="center">
		 </p>
<p align="center"><font color="#FF0000" face="Arial"><b>If you don't 
		sign then....</b></font> </p>
<p align="center">
<img border="0" height="84" src="gunfight.gif" width="112"/></p><p align="center">
		 </p><p align="center" style="line-height: 100%; margin-top: 0; margin-bottom: 0">
		 <!-- Topstat tellercode --> <a href="http://www.nedstatbasic.net/s?id=3092551" target="_blank"><img align="middle" height="35" src="tab1d.gif" vspace="3" width="35"/></a>
</p>
<p align="center" class="MsoNormal" style="line-height: 100%; margin-top: 0; margin-bottom: 0">
<b><font color="#800000" face="Monotype Corsiva" size="3">Bezoekersteller</font></b></p>
<p align="center"> </p><p align="center"> </p><p align="center">
<img border="0" height="160" src="trls.gif" width="160"/></p><p align="center">
 </p><p align="center">
<a href="http://www.facebook.com/plugins/like.php?href=http://www.westernriding-frooy.com/Westernriding.htm&amp;width&amp;height=80&amp;colorscheme=light&amp;layout=standard&amp;action=like&amp;show_faces=true&amp;send=false" target="_blank">
<img border="0" height="43" src="Facebook%20Like%20button.jpg" width="112"/></a></p></td>
<td height="3523"></td>
</tr>
<tr>
<td width="220"></td>
<td width="1"></td>
<td width="1040"></td>
<td height="1" width="1"></td>
</tr>
</table>
</body>
</html>