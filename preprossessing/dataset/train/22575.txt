<!DOCTYPE HTML>
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/> <title>2Bet.ag - Sportsbook, Casino, Live Casino &amp; Racebook</title>
<meta content="Enjoy our Sportsbook, Live Betting, Mobile Betting, Casino, Live Casino &amp; Horse betting products." name="description"/>
<meta content="index, follow" name="robots"/>
<meta content="en-us" name="Language"/>
<link href="images/skin/fav.png" rel="icon" type="image/png"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<link href="css/style.css?v=2" rel="stylesheet" type="text/css"/>
<link href="css/animate/animate.css" rel="stylesheet" type="text/css"/>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"/> </head>
<body>
<header>
<div id="header-top">
<div class="wrap">
<a href="index.php" id="logo"><img alt="2Bet" height="100" src="images/logo.png" title="2Bet" width="210"/></a>
<div class="wrap-menu-responsive">
<span class="menu-responsive">
<i class="fa fa-list fa-3"></i>
<span>Menu</span>
</span>
<span class="menu-phone">
<i class="fa fa-phone fa-3"></i>
<span>Phone</span>
</span>
</div>
<nav id="main-menu">
<ul id="main-menu-list">
<li><a class="active" href="index.php"><i class="fa fa-home fa-lg"></i></a></li>
<li><a href="sportsbook.php">Sportsbook</a></li>
<li><a href="racebook.php">Racebook</a></li>
<li><a href="casino.php">Casino</a></li>
<li><a href="live-casino.php">Live Casino</a></li>
</ul>
</nav>
</div>
</div>
<div id="header-bottom">
<div id="sub-menu">
<ul id="sub-menu-list">
<li><a href="contact-us.php">Contact Us</a></li>
<li><a href="faq.php">FAQ's</a></li>
<li><a href="house-policies.php">House Policies</a></li>
</ul>
</div>
</div> <div id="border-phones">
<div id="contact-phones">
<div class="wrap">
<div class="contact-phone-item">
<table class="phone-list">
<thead>
<tr><td>Wagering:</td></tr>
</thead>
<tr><td>1.888.238.8018</td></tr>
</table>
</div>
<div class="contact-phone-item">
<table class="phone-list">
<thead>
<tr><td>Agents Toll Free:</td></tr>
</thead>
<tr><td>888-328-6957</td></tr>
</table>
</div>
<div class="contact-phone-item">
<table class="phone-list">
<thead>
<tr><td>Chinese:</td></tr>
</thead>
<tr><td>1.888.238.8198</td></tr>
<tr><td>1.888.889.4312</td></tr>
</table>
</div>
<div class="contact-phone-item last">
<table class="phone-list">
<thead>
<tr><td>Vietnamese:</td></tr>
</thead>
<tr><td>1.888.378.0888</td></tr>
<tr><td>1.888.889.4219</td></tr>
</table>
</div>
</div>
</div>
</div> </header>
<section id="main">
<article>
<div id="main-content">
<div id="index-banners">
<div class="index-banner-item">
<div class="wrap">
<h2>Sportsbook</h2>
</div>
<div class="index-banner-img animated fadeIn"><img alt="2Bet Sportsbook" src="images/index-banners/index-banner-sportsbook.jpg"/></div>
</div>
<div class="index-banner-item">
<div class="wrap">
<h2>Live Casino</h2>
</div>
<div class="index-banner-img animated fadeIn"><img alt="2Bet Live Casino" src="images/index-banners/index-banner-live_casino.jpg"/></div>
</div>
<div class="index-banner-item">
<div class="wrap">
<h2>Casino</h2>
</div>
<div class="index-banner-img animated fadeIn"><img alt="2Bet Casino" src="images/index-banners/index-banner-casino.jpg"/></div>
</div>
<div class="index-banner-item">
<div class="wrap">
<h2>Racebook</h2>
</div>
<div class="index-banner-img animated fadeIn"><img alt="2Bet Racebook" src="images/index-banners/index-banner-racebook.jpg"/></div>
</div>
</div> <div id="login">
<div id="login-container">
<div id="login-box">
<span id="title-login">Login to your account</span>
<div class="myLogin" id="bp_login_div">...</div>
</div>
</div> </div>
<div id="border-index"></div>
</div>
<div class="product-item">
<div class="wrap">
<div class="intro-img left">
<img alt="2Bet Live Casino, Sportsbook, Casino &amp; Horses" src="images/banner.png"/>
</div>
<div class="intro-text right">
<h1>Welcome to 2Bet.ag</h1>
<p>Your home for world sports and online wagering entertainment 24 hours a day, 365 days a year.</p>
</div>
</div>
</div>
</article>
</section>
<footer>
<div id="copyright">
<div class="wrap">
<p>2Bet.ag ® 2021 - Live Casino, Sportsbook, Casino &amp; Racebook</p>
</div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="//api.securecashiersystem.ag/js/Login.js?hid=78412d4fb189466ff138bf6d5e4fbf08" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
	bpLogin.build(); });
	bpLogin.autoFocus = false;
</script>
<script src="js/jquery.bxslider.min.js" type="text/javascript"></script>
<script src="js/scripts.js" type="text/javascript"></script> </footer>
</body>
</html>