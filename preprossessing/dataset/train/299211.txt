<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "38284",
      cRay: "610a31ba4ade1acc",
      cHash: "9f4eb046fb5508f",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuY2VkYXNweS5jb20uYnIvd3AtYWRtaW4vLS80OGQ4YjYyMGMwMGE0MmIyYTJhZmE1YTIyNjg3NGJmZS8lMDklMEE=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "b27rZ3IDj0Fl7Qbj8yzCPPsdpDnYPkjZSCi/57u07sX/L2PjJGtKVo/lVVcBtaMDmGHc2cXbeBg699ZkKQDuavpkGfXQTzOFy2lSad6J2Oz7zNUd69sLBYcZpmtEOUMMzMEr/lpaPcjG83a5nEquaVNP5lhRNIvEEXFNLIC+xL3vhWYZCWM37MmMS+E1cMQo7x+RBIkmBtZYDIrQtUdh9DAFbDt9y2kY5N1w5TMBQuQqZ+erdDhyGLIyhqSuBdGz3//j9yP/tYXcajufD58S6T0oQLj2qVkbY6QuQDAbuHBdiniWZpCFoTuNXUbFEB0wBi3gQMR0Z+/sAgN0NdCIpDrhS3555mLr58Zfx6B92sgxtbEVijz9xdJqBvaE5/MJZxUb78qMclQXy935/vHHryTRaZKferx3cKgiZJWSzhcWsDdqkShdbnlRLoqf7jGPOCXGFRQ6/aIYjKbr3Asq13NME/bbQegqVyndmFTrzggtv5F0Xfe+CamWr257Z2Up/B7gndsJjJirtYxqfWY4ujyw8pe5lD02o2kbXn+2e44mWR2KMGyQXpBPDpv2QG2+ynZCy8p09ayY5Em53UCzonCd+HvZ3gV4MpvNgaHfFr3JkTIutIl4/EMk7dn8mriFzjsF8hyu3f9c55Rddge3Ke5+PX16PQnlNL0NNA68rVmbnB0bAXsMehl9QneL7tcZZ09yTXVFuzpd0yCZFsyjqumssJpx8G+4s8Gu/OuLWwm9w+jzEIopv4JlJ6sbiKEH",
        t: "MTYxMDQ4OTI4Ni43NzEwMDA=",
        m: "IZi3ZeLyWVqGZ4BZZZBoKsG10QxIvVJeuG9LkTkHRiE=",
        i1: "j6g4rCXGMo9PDELONDwN6Q==",
        i2: "NX3GlMOyklGQa21/J2uvsQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "HkXh7i1rtsnA0g2/I3SBEL61bT2NsJNojiXRskRgcSo=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.cedaspy.com.br</h2>
</div><!-- /.header -->
<a href="https://derfueller.com/cautiouslongitudina.php?e=27" style="display: none;">table</a>
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/wp-admin/-/48d8b620c00a42b2a2afa5a226874bfe/%09%0A?__cf_chl_captcha_tk__=06bd8e74fe51623cee553bd2a79256575de65ebb-1610489286-0-ATtfDEaumJO1eBE2spVHszkrwRILFDkvcNy8m0fPiwVEaY0qD6GSXBh-kXum-7JrFF4-U7VK6U0flmnRqXr05SaYTkwLM5kHsejkK6xIXs-n1MHq-Y9RJXq2mE1B2yLHTWnEnL224GO2brdL_u65btJWDb7HUQoi0rFCSU-UWDOez4W2Pcp3em4H3_bG3Ex1gyYb2y1Yo4yYATMa4Vd8OSuEJ6em4GJjtZFRast7S4Zo737oaQL-OxoWdNt3PS-bbHUmTjPs3vO0uW1Xucnu8jwT9jPycvaVgjmYBwIFjNoCDqHRkCrT88fRHbb9SD8S1uiEbXEzZi6xlJc0XbPyiJ0BMhDWMchsNMzvys6Z7xn9CrI2TnN6-PZBEkIfY0ffU620xi16bHqj0DIh7zU1-zpZyWAgl3lyVlDx0IvrYzgrQryd1-aFs48yysxkZj7kSrPMcxBt4EJqbNT6VAaHJuuT-Xt71wXi4kz9iTvexzvoG1ddIB6NKlLVsFqd5XvN8WOH_vTSDRiQfkBhvNTGGvAtHl57IkweHUFTVkWahM-ma69wwuXrMu_BXI2rdKxPjfSLxI3MrfpcxOoGB6OKDUQ" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="5aba1403b457e267e5643e527633627adc510675-1610489286-0-AX6eLw8ULth8GyP3bT5itbXaQuuFcTFnjxcdbOwuSPqhPKfs23fEEyydZQ0jTg5MzZRreiWrv86Jil3MwzjzmswS9EwNWDfjtIOOv3WdS/i+M0QL5rAyWWdVc8+nHz/lVxWRe4dQKR0/P6bYM9Wd5aW2ovE9Ledu1vUAVV4cOVyVeyJ5XxP+614+staJzTY3C927xsheXQTYXDvdzMYShvuXr5iSuZ/IYNIi7oE3JzJY9CracC/HJ4+ZVGdbyw8f+ChvWdehmZcj/o54TUvJ4qsiRpIK2GjeOqz6vOqDrgJ2RT3dvTjExBEl5XX4BJWC8Lp89MZa37N+KT9pk76ypbGoalA8EjR2B/ZZr6b0izz3qKsmQXPpEf1GMhYeAVrKjJMBiWYUi8pVuMQtVm7oMAXtHdMvgBi2QfizjrjJUw3n+K0sNxnynmiKxQd6A6bkgepv/SlXl6CZl2xoqTBPW6RIWPILgiGd2nqPdC4kc7h0AUmQKcn+epMVjCg6ZCgDOHsPeKp8HY7ZgT+Y+j2x5sOihXxbOA97YejyJrmi3sBP93Og2iu/3+rOZBBqUx1hKfsNyXv9oatqJYCnAaX1rOE68gYK3c5CFmmkyY62BN5k0E1QFOKjADECDmKHgUVGnBgZh55oE8JHt5GoXhF3mGjuVy0r1x2bxIvZ1XZlB550HFP42USji6+oJ63wPmD7DZnUMO/Pkl67AoqMnDgieANkgU6LPm3QTxAk/3YIx5ydyRc5X2vI1O5WDOeUU/xfvqHsxtZ/+Rn2J3xIEZl0364TYi0kFaLS9UYVhz5/qV4N0JacjQ4+vuK2UaffVn1DGoQQuhgQbrhy0b6p2wIfYe16UwwdIbd/WPOomCRX7MiCewIbw8cng4rUFLWAI/7ddZYSDkJxSBmCEpzOXWT9h+H/awGkLKgeLv2YEBA+JG0mswUWC674OeS0NcKhmg5hDz8ZBqzPBCuF5aiSRkanhdXcij7FrAVgtrY+NBTWAni9Zzmx2+bEr8K6Mfami+dmQqD8mhvC74DMTuZEDiDtBdyq4sVswC1cDrZQP7Vol6dmnC3KGsg0MkdKv+2hBTQ00XOMsk/YJgQaPLOit+PnsYVBDLTlRXiI44jb90mdAdWnh0MA3gmnVtBDMAolZP8M9DE2yz+FH4MNQ6Cb5eaQ+JrpzjeCvhUKuJB2vqCcZFYpmvp5/iOq1ryhz4xbYjaBsDRmmrA8f9GxEMGY+UrsumXQzEULstdjzMVqKXfiLZRyuckru2TKR44GH0vrdpV/Kx84qraEm3O8gXTIT3gP6cuERx1+QZZLY386bT10wqW70RyZHID4KWc7K5IufpVrLg=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="773566869afb14ffd765dbc4e3092455"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610a31ba4ade1acc')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610a31ba4ade1acc</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
