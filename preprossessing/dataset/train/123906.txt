<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "35594",
      cRay: "610dbe497b9cddfd",
      cHash: "e40a315170bd545",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cyLmFuaWZsaXgudHYv",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "UX/KPpfF9vrdkqaU7t4Wzwnz9WMGwlcFRwl6D5nTz4wltl1qj7oWhL9IY72mpZNxRtDSesCJAMOIhrrTUvd1KQ+CMD8F+81L1Qk62McP788RU4CQWd1auYicZsxE5n0Gc8hKWSUGklY3a3hAGk8Z92WDGUQCxs4lUy/PnvT8CXkAytXQemCjTtIz1bSmNcDqHnh6p28Vx89oRiig/SdXLv+86NwR0oNWcvLT+GknfMyS5AcbMG7TPrZ0WctWNWM8yi6rQWltmltIHfA59eTdaV60aprCiSeiqftGySARX7KhF3MtmF60D4J2RqdJH1Tc14FrLej33CI83Kq3vtal/nlrCjS7gNJKtvYv3DxFa9PYRtprPfa6cIIhHt6nKIJspuSA8VbyOVmo2dPp0F2MBcarxdqCWWl/nesuIlRAkb8hT6AL5HsRcPrl8hXAEITLDvqscS0Rjhn9wdEnKwcZqbOODtfZYyKLNhAhWxd/ejqJE6XEmO4YrwbQQ9yCej9fMfnJLEl4CebmghjwTJgq53UbVXEUxH5eb/14coW6Xzp+P1jZOzHqdiTfODYhdc51FAacDFssojtEkTOqaZ36zWuY43jvEf9K8E6ADbEFYBg0odRettCCradNLj5jxztlVU7GmMmt/+9VHaerCbPdjHqMJZBvU4ZaSFmy3wNQGt5dOmZMFN2mETu69EggNAHD2jou4pWu9Uay3lwDCTu8c2uLjqZ7o5tsQbkzGtmLFzGpbsVQ4uG3CRp09wjhljU/g+2om8oe3bvCMdAqq435DQ==",
        t: "MTYxMDUyNjUwMS4zNTkwMDA=",
        m: "eVVdMIqx1f3psw513/nFVfp2Hplx0NMhZEs4D9jIU5U=",
        i1: "MQloNTq7TxglwvOEeP4DmA==",
        i2: "cHuDX0i3RE4Llcj8jlCtog==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "TQ7dYiawmGghMsYUtQujXoUFQd3dIVv0XliLNKQTwZw=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www2.aniflix.tv</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<!-- <a href="https://salvagepc.com/composer.php?s=37">table</a> -->
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=05c3c377fac720f3800f7626324d554cc77349c6-1610526501-0-AWFmkPPHpCUjDl2OZrvvslgSbRjioqLZjc4bq1mmoUvsRrb_XaXj5ooAawvPFfzaoP6F6i_8_SDf6vbGlTQpNMh0SemO4WEIy66nouSEgH0MF_SZcFSZODcpHUP3gpgOUrinb4VFNzMbhZCI6LAApWyWlW6C_Dxp_w82jvZhB8Ndj3o4NpYcyWQlHUbVvTiTR87Ui6j4ETkrdPdG5nMe1tQ5RPpdwnRH1b7zqBzhu8I_mLYuno8ExKKWG3xBNNsCOyB38Iky_mKWRwgFLzKMT1cwLw3tF1XsG08b48jAHXE_eDPlKvAfEb-L6z9MXewS9OwHg13XD1jM8cYLEQ1oeMnR574papIcIYSzxvFCfqMTN-aOjkZ6yrP5v9aSzpRUBX2M_HQw1feM83rkhA6veXNGVhXBd1RzoNkTJPlXJd1y4JZhTqp5XOjWuIROT4blXm1mlomxCZi6SXLbIn327Gav1tUl3k97_nedaNlksfOnjhlMWm0B_yGrfQOh-jZG-7VtB46CggiNndxTnWZf1jmeFRiZ0L7YDjvdLLhhxGL9SoKk5RTkh_gXKaa9f6U6RHZIPHKKebPudEM6T5QmMS8" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="7b4009973766503a419d2071cdf0c93ecdae5a93-1610526501-0-AUiO4Zj7iozvbLUOOfU7MyCti3FaVkn5kNHpZNKPkipm18XMl837rZtl2mjqBy1xgtGlJPISiLIqDsLodeatOUQNCxLxlvmmWCg8nTh1KzLVkHQPZl/fc49LhWjB5fyXaM54SNRweX47sq0t3Si7x24QnFSKFmNXqzLKeGj0BVDBW6gL63f9g/a41q7V4BrYrtDEIjv7cggqMnCHp58Kli/CnNHwh2nBWcZhSX3D29Wrb6XrFKL6eFSfItUlJgHpWDOB+BDhhRywDLAysNgRcnAs4peHfAJIuRExrMq60YvtGUDATLDlbV6VS2HRfJ7wYoQ4faCG0oJVOtIEmmCRigXUBnRttcwFYo9yzEDQLnQzFjfFn5/ZTYO4eS8DZCv4UEd2RSRH3WDEzACLXM3emVSouU6dshyKviTLF3QpEBbUNNlJLYmQnD7ky3LGY5zPC+NtXVmDWkggiwPs78S/nj0uermhcW//Oe4f2J3UWjMPoSXT3IDk7E25yXTfpGWLRpDy6nQ+3JsvoluuA+oH4dm4RLOWXs2IJwhqNBlT8WgFrPHzDWnwEFIFQcCfiypBJy3qAbh4Vie66MXfIXAuJQl2dOOdJyDixrmTonqwsQDH6OpurfG84Ef7gmugRhUtRBLBwGlaJO36oM6NZrZsB3J6Mlp1Ph6FtPjW7a3PmQn4qe46qOiKLbrPDJq+ohXHDUbN5/9LnwBQS36ayxhCTHfyAdvgpPTGQvBhWzwuZQDj9Mg1rqDju97lQEFan9s/lGJWRZL4L7bDWmyT4jVsTfta+jmtdpzYcjsiW9URhDcgNlqj4FNO2HBtDPr+e9aaDxz4yw/39OWvj1U/mgzHyCPxuCoriIqiphLH/DT1Qw3vBADmyk1nmKPbOti/TDten27fKXiWRVXfZaJDTIHHOG3GYCm/uqoFYlOesOAOOo4023JJN+VqnYmmm0Pvqse//eEIslTkyN12loFIIIiN45mqbcKlTptTPI/bYBMIpjl7ikcqlVEPWocdHrca3drPfgDs2kS8uWMB6NAVpw3LeI6naSsw0LszeJ8PQCywh3BSiDBFqrUkJ02F3iPdYrPGh9hcLcH9m2gRbjchYqWKPpkVrv+p2DoHTxpc9Na9oA+c1NaY0c1ovOyWis9tM6GLRVrdmSRM/e0o5ymY8W2k1rZTTPFQ2CaU4dlq0k2PKFB1ys/2QXfKSr+/waxGTrXvrdlNq1g4Mkzv46eUTGiBIMWdYBR9MPYNKY2P66q6jiy8N0TGEgeIA+Aiuw2Odd4tHA/bS1Lc+fbBbLgZf0hG6hxr4YxdjKXpLLTgJ5aBj7DLwBz91+NAW7faEz2fTfRZnA=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="f2814efbd40ee722c739f940300a0718"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610dbe497b9cddfd')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610dbe497b9cddfd</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
