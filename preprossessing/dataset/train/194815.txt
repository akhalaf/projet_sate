<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Base64 Encode and Decode - Online</title>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<meta content="#5fa926" name="theme-color"/>
<meta content="Encode to Base64 or Decode from Base64 with advanced formatting options. Enter our site for an easy-to-use online tool." name="description"/>
<meta content="base64, encode, online, tool" name="keywords"/>
<meta content="index, follow" name="robots"/>
<meta content="UqQ5a5sfjSW2rokOvmrFfQ-DRx9bZ8x6Y32uwxxL97M" name="google-site-verification"/>
<meta content="839B703E7C748A99BF2CDD181494114F" name="msvalidate.01"/>
<meta content="none" name="msapplication-config"/>
<link href="https://cdn.base64encode.org" rel="preconnect"/>
<link href="https://www.google-analytics.com" rel="preconnect"/>
<link as="font" crossorigin="anonymous" href="https://cdn.base64encode.org/assets/fonts/fa-regular-400.woff?v=2" rel="preload" type="font/woff"/>
<link as="font" crossorigin="anonymous" href="https://cdn.base64encode.org/assets/fonts/fa-solid-900.woff?v=2" rel="preload" type="font/woff"/>
<link href="https://pagead2.googlesyndication.com" rel="dns-prefetch"/>
<link href="https://tpc.googlesyndication.com" rel="dns-prefetch"/>
<meta content="https://www.base64encode.org/" property="og:url"/>
<meta content="Base64 Encode" property="og:site_name"/>
<meta content="website" property="og:type"/>
<meta content="https://cdn.base64encode.org/assets/images/b64-fb.png" property="og:image"/>
<meta content="Base64 Encode and Decode - Online" property="og:title"/>
<meta content="Encode to Base64 or Decode from Base64 with advanced formatting options. Enter our site for an easy-to-use online tool." property="og:description"/>
<meta content="en_US" property="og:locale"/>
<link href="https://cdn.base64encode.org/assets/images/b64-180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://cdn.base64encode.org/assets/images/b64-32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="https://cdn.base64encode.org/assets/images/b64-16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="https://www.base64encode.org/manifest.json" rel="manifest"/>
<link href="https://www.base64encode.org/" rel="canonical"/>
<link href="https://amp.base64encode.org/" rel="amphtml"/>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebSite",
    "url": "https://www.base64encode.org/",
    "name": "Base64 Encode and Decode - Online",
    "description": "Encode to Base64 or Decode from Base64 with advanced formatting options. Enter our site for an easy-to-use online tool.",
    "image": "https://cdn.base64encode.org/assets/images/b64-180.png",
    "keywords": "base64, encode, online, tool",
    "inLanguage": "English",
    "dateCreated": 2010,
    "isAccessibleForFree": true
  }
  </script>
<style>body{background-color:#439a00;background-image:url('https://cdn.base64encode.org/assets/images/pattern.png')}div.wrapper{background-color:#439a00;background-image:url('https://cdn.base64encode.org/assets/images/swirl.png')}section a,article a{color:#275900}span.option button{background:#275900}span.option button.live.active{border:none;background:#275900}div.file span.counter.active{background:#275900}@font-face{font-family:'Font Awesome 5 Free';font-style:normal;font-weight:400;font-display:swap;src:url('https://cdn.base64encode.org/assets/fonts/fa-regular-400.woff?v=2') format('woff')}@font-face{font-family:'Font Awesome 5 Free';font-style:normal;font-weight:900;font-display:swap;src:url('https://cdn.base64encode.org/assets/fonts/fa-solid-900.woff?v=2') format('woff')}.fa,.fas,.far,.fal,.fad,.fab{-moz-osx-font-smoothing:grayscale;-webkit-font-smoothing:antialiased;display:inline-block;font-style:normal;font-variant:normal;text-rendering:auto;line-height:1}.fa-spin{-webkit-animation:fa-spin 2s infinite linear;animation:fa-spin 2s infinite linear}@-webkit-keyframes fa-spin{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes fa-spin{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}.fa-rotate-90{-ms-filter:"progid:DXImageTransform.Microsoft.BasicImage(rotation=1)";-webkit-transform:rotate(90deg);transform:rotate(90deg)}.fa-rotate-180{-ms-filter:"progid:DXImageTransform.Microsoft.BasicImage(rotation=2)";-webkit-transform:rotate(180deg);transform:rotate(180deg)}.fa-rotate-270{-ms-filter:"progid:DXImageTransform.Microsoft.BasicImage(rotation=3)";-webkit-transform:rotate(270deg);transform:rotate(270deg)}.fa-flip-horizontal{-ms-filter:"progid:DXImageTransform.Microsoft.BasicImage(rotation=0, mirror=1)";-webkit-transform:scale(-1,1);transform:scale(-1,1)}.fa-flip-vertical{-ms-filter:"progid:DXImageTransform.Microsoft.BasicImage(rotation=2, mirror=1)";-webkit-transform:scale(1,-1);transform:scale(1,-1)}.fa-flip-both,.fa-flip-horizontal.fa-flip-vertical{-ms-filter:"progid:DXImageTransform.Microsoft.BasicImage(rotation=2, mirror=1)";-webkit-transform:scale(-1,-1);transform:scale(-1,-1)}:root .fa-rotate-90,:root .fa-rotate-180,:root .fa-rotate-270,:root .fa-flip-horizontal,:root .fa-flip-vertical,:root .fa-flip-both{-webkit-filter:none;filter:none}.fa-arrow-left:before{content:"\f060"}.fa-arrow-right:before{content:"\f061"}.fa-chart-line:before{content:"\f201"}.fa-check-circle:before{content:"\f058"}.fa-chevron-left:before{content:"\f053"}.fa-chevron-right:before{content:"\f054"}.fa-cog:before{content:"\f013"}.fa-copyright:before{content:"\f1f9"}.fa-envelope:before{content:"\f0e0"}.fa-envelope-open:before{content:"\f2b6"}.fa-exchange-alt:before{content:"\f362"}.fa-exclamation-triangle:before{content:"\f071"}.fa-file:before{content:"\f15b"}.fa-file-alt:before{content:"\f15c"}.fa-file-archive:before{content:"\f1c6"}.fa-file-code:before{content:"\f1c9"}.fa-folder:before{content:"\f07b"}.fa-folder-open:before{content:"\f07c"}.fa-hand-paper:before{content:"\f256"}.fa-hand-rock:before{content:"\f255"}.fa-info-circle:before{content:"\f05a"}.fa-laptop:before{content:"\f109"}.fa-lock:before{content:"\f023"}.fa-meh:before{content:"\f11a"}.fa-pen-nib:before{content:"\f5ad"}.fa-question-circle:before{content:"\f059"}.fa-random:before{content:"\f074"}.fa-shield-alt:before{content:"\f3ed"}.fa-smile:before{content:"\f118"}.fa-spinner:before{content:"\f110"}.fa-star:before{content:"\f005"}.fa-tablet-alt:before{content:"\f3fa"}.fa-toggle-off:before{content:"\f204"}.fa-toggle-on:before{content:"\f205"}.fa-tv:before{content:"\f26c"}.fa-film:before{content:"\f008"}.far{font-family:'Font Awesome 5 Free';font-weight:400}.fa,.fas{font-family:'Font Awesome 5 Free';font-weight:900}html *{max-height:999999px}body{color:#000;margin:0;padding:0;font-size:13px;font-style:normal;text-align:left;font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-weight:400;line-height:normal;background-repeat:repeat}html,body{-ms-text-size-adjust:none;-moz-text-size-adjust:none;-webkit-text-size-adjust:none}a,ul,li,input,button,select,textarea{resize:none;outline:none;-webkit-tap-highlight-color:transparent}textarea::-webkit-scrollbar{width:8px;height:8px}textarea::-webkit-scrollbar-thumb{border-radius:4px;background-color:rgba(0,0,0,.5)}div.clear{clear:both;margin:0;padding:0;display:block;position:relative}::-webkit-input-placeholder{color:gray;opacity:1}::-moz-placeholder{color:gray;opacity:1}:-moz-placeholder{color:gray;opacity:1}::-ms-input-placeholder{color:gray;opacity:1}:-ms-input-placeholder{color:gray;opacity:1}div.wrapper{margin:0 auto;padding:12px;display:block;position:relative;min-width:320px;max-width:1134px;box-sizing:border-box;font-style:normal;text-align:center;font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-weight:400;background-size:auto;background-repeat:no-repeat;background-position:left top}header{color:#fff;margin:0 0 12px;padding:0;display:block;text-shadow:1px 1px 0px rgba(0,0,0,.1)}header a.logo{color:#fff;float:left;width:180px;height:auto;margin:0 0 16px;padding:0;display:block;overflow:hidden;text-decoration:none}header a.logo span.large{width:180px;height:40px;margin:0;padding:0;display:block;font-size:28px;font-family:'Arial Black','Arial Bold',Gadget,sans-serif;font-weight:900;line-height:40px}header a.logo span.small{width:180px;height:24px;margin:0;padding:0;display:block;font-size:18px;font-weight:700;line-height:24px}header nav,aside nav{margin:0 0 16px 192px;padding:0;display:block}aside nav{margin:0;padding:12px 8px 0}header nav a,aside nav a{color:#fff;width:auto;height:auto;margin:0;padding:0 12px;display:block;overflow:hidden;font-size:16px;text-align:left;box-sizing:border-box;background:rgba(255,255,255,.25);font-weight:700;line-height:26px;text-shadow:1px 1px 0px rgba(0,0,0,.1);text-decoration:none}header nav a:first-of-type{margin-bottom:12px}header nav a.active{color:#606060;background:linear-gradient(135deg,#fff 0%,silver 100%)}header nav a i[class],aside nav a i[class]{float:right;width:18px;height:26px;margin:0 0 0 8px;display:block;text-align:center;line-height:26px}header nav a i[class]{float:left;margin:0 8px 0 0}aside nav a{margin:0 0 8px;font-size:14px}aside nav a:last-of-type{margin:0}header div.intro{margin:0;padding:0;font-size:14px;text-align:justify;line-height:19px}header div.intro b{white-space:nowrap}header div.intro b i[class]{margin:0 6px}main{float:left;width:100%;margin:0;padding:0;display:block;max-width:766px}section,article,aside{color:#000;margin:0 0 16px;padding:8px;display:block;font-size:13px;text-align:justify;background:#fff;box-sizing:border-box;line-height:18px}section{margin-bottom:8px}article{overflow-x:auto}aside{float:left;width:336px;height:auto;padding:0;background:0 0;margin-left:8px}@media(max-width:1152px){main{max-width:none}article{margin-bottom:8px}aside{width:100%;margin-left:0}}section h1,section h2,article h2,aside h3{margin:0;padding:0;display:block;font-size:18px;text-align:left;font-weight:700;line-height:normal;text-shadow:1px 1px 0px rgba(0,0,0,.1)}section h2,article h2{margin:0 0 8px;padding:0 0 8px;font-size:15px;border-bottom:1px dashed #ccc}section h2{margin:16px 0 0;padding:0;border-bottom:none}aside h3{color:#fff;margin:0 0 8px;padding:0 0 8px;font-size:15px;border-bottom:1px dashed rgba(255,255,255,.8)}aside h3.bookmark{color:#ffb;margin:0;font-weight:400;border-bottom:none}article h2 i[class],aside h3 i[class]{width:17px;text-align:center;margin-right:6px}section label,section span.info{color:#606060;margin:0 0 12px;padding:4px 0 8px;display:block;font-size:12px;line-height:17px;border-bottom:1px dashed #ccc}section span.info{padding:0;line-height:18px;border-bottom:none}section span.info i[class]{margin-right:6px}section ul,section ol,article ul,article ol{margin:8px 16px;padding:8px}section a,article a{text-decoration:none}article table{margin:0;border-collapse:collapse}article table th,article table td{border:1px solid #ccc;padding:2px 4px;text-align:center}article table th{background:#eee;font-weight:400}form,div.form{margin:0;display:block}textarea{width:100%;height:auto;border:1px solid #ccc;resize:vertical;margin:0 0 8px;padding:4px;display:block;overflow:auto;font-size:13px;word-break:break-all;box-sizing:border-box;min-height:220px;background:#eee;font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;white-space:break-spaces}textarea[name=output]{margin:12px 0 0}textarea.error{background:#fee}@media(max-width:1152px){textarea{min-height:160px}}span.option{height:auto;margin:0 0 8px;display:block;min-height:22px;line-height:22px}span.option:last-of-type{margin:0;min-height:26px;line-height:26px}span.option:nth-last-of-type(2){margin:0 0 12px}span.option input[type=checkbox]{height:13px;cursor:pointer;margin:0 8px 0 0;display:inline-block;vertical-align:text-top}span.option select{width:136px;height:22px;border:1px solid #ccc;margin:0 8px 0 0;padding:0 0 0 4px;display:inline-block;font-size:12px;box-sizing:border-box;background:#eee;line-height:normal}span.option label{color:#000;margin:0;padding:0;display:inline-block;line-height:22px;border-bottom:none}span.option:last-of-type label{line-height:26px}span.option button{color:#fff;width:136px;height:26px;cursor:pointer;border:none;margin:0 8px 0 0;padding:0;display:inline-block;font-size:14px;text-align:center;box-sizing:border-box;font-weight:700;line-height:normal;text-shadow:1px 1px 0px rgba(0,0,0,.1)}span.option button i[class]:first-of-type{margin-right:6px}span.option button i[class]:last-of-type{margin-left:6px}span.option input[type=checkbox][disabled],span.option button[disabled],span.option select[disabled]{color:#ccc;cursor:not-allowed;border:1px solid #ccc;background:#eee}span.option button.live{color:#000;height:22px;border:1px solid #ccc;font-size:12px;text-align:left;background:#eee;font-weight:400;text-shadow:none}span.option button.live.active{color:#fff}span.option button.live i[class]{width:14px;margin:0 10px;text-align:center}div.file{color:#606060;width:100%;height:160px;border:1px solid #ccc;margin:0 0 8px;padding:0 8px;display:block;position:relative;overflow:hidden;font-size:16px;text-align:center;box-sizing:border-box;font-weight:700;line-height:160px;white-space:nowrap;text-overflow:ellipsis;background:#eee}div.file.drag{background:#ccc}div.file span.counter{right:2px;bottom:2px;color:#eee;margin:0;padding:2px 6px;position:absolute;font-size:12px;font-weight:700;line-height:14px;background:#ccc}div.file span.counter.active{color:#fff}div.file i{margin-right:8px}div.file input[type=file]{top:0;left:0;width:100%;height:100%;cursor:pointer;margin:0;padding:0;opacity:0;display:block;position:absolute}div.state{margin:12px 0 0}div.state>div,div.state[data-state]{width:100%;border:1px solid #ccc;padding:8px 12px;display:block;box-sizing:border-box;background:#eee;line-height:21px}div.state>div{margin:0}div.state[data-state]{display:none}div.state[submit-error]>div,div.state[data-state=error]{background:#fee}div.state i[class]{margin-right:6px}div.state[submit-error]>div i[class],div.state[data-state=error] i[class]{color:#b00}div.state label{margin:0 0 8px;padding:2px 0 6px}div.state textarea{border:none;margin:0;padding:0 4px 0 0}div.state a{font-weight:700}a.switch{color:#fff;width:100%;height:26px;margin:0 0 12px;padding:0;display:block;font-size:14px;text-align:center;box-sizing:border-box;background:rgba(255,255,255,.25);font-weight:700;line-height:26px;text-shadow:1px 1px 0px rgba(0,0,0,.1);text-decoration:none}a.switch i[class]{margin-right:8px}footer{color:#fff;margin:0;padding:8px;display:block;font-size:13px;text-align:justify;text-shadow:1px 1px 0px rgba(0,0,0,.1);line-height:22px}footer i[class]{margin-right:6px}footer a{color:#fff;width:auto;height:26px;cursor:pointer;margin:6px 12px 6px 0;padding:0 12px;display:inline-block;font-size:14px;text-align:left;box-sizing:border-box;background:rgba(255,255,255,.25);font-weight:700;line-height:26px;text-decoration:none}footer a:last-of-type{margin-right:0}header nav a,aside nav a,span.option button,span.option label,a.switch,footer a{user-select:none;-ms-user-select:none;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-webkit-touch-callout:none;-webkit-tap-highlight-color:rgba(255,255,255,0)}.ad_top,.ad_left,.ad_right,.ad_corner{width:auto;height:90px;margin:0 auto 8px auto;padding:0;display:block;position:relative;text-align:center}.ad_left{top:0;left:0;width:160px;height:600px;margin:0;position:fixed}.ad_right{height:auto;margin:8px auto 0px auto;min-height:600px}.ad_corner{height:auto;margin:20px auto 0px auto;min-height:280px}@media(max-width:1152px){.ad_top{height:112px;overflow:hidden}.ad_right,.ad_corner{height:90px;min-height:0px}}.ad_crispox{width:auto;height:auto;margin:0;padding:0;display:block;position:relative}.ad_left .ad_crispox{top:auto !important;left:auto !important;z-index:auto !important;position:relative !important}.ad_mobile{width:auto;height:auto;margin:0 auto;padding:0;display:block;position:relative;text-align:center}.ad_mobile.top{height:100px;margin:0 auto 8px auto}.ad_mobile.right{height:280px;margin:8px auto 0px auto}.ad_mobile.corner{height:280px;margin:20px auto 0px auto}.qc-cmp2-persistent-link{background-color:#276399}</style>
</head>
<body>
<div class="wrapper">
<header>
<a class="logo" href="https://www.base64encode.org/"><span class="large">BASE64</span><span class="small">Decode and Encode</span></a>
<nav>
<a href="https://www.base64decode.org/" rel="external"><i class="far fa-folder-open"></i>Decode</a>
<a class="active" href="https://www.base64encode.org/"><i class="far fa-folder"></i>Encode</a>
</nav>
<div class="clear"></div>
<div class="intro">Have to deal with <strong>Base64</strong> format? Then this site is made for you! Use our super handy online tool to decode or <strong>encode</strong> your data.</div>
</header>
<ins class="ad_top"></ins>
<main>
<section>
<h1>Encode to Base64 format</h1>
<label for="input">Simply enter your data then push the encode button.</label>
<form action="https://www.base64encode.org/" method="post" name="text">
<textarea id="input" name="input" placeholder="Type (or paste) here..." spellcheck="false"></textarea>
<span class="info"><i class="fas fa-info-circle"></i>To encode binaries (like images, documents, etc.) use the file upload form a bit further down on this page.</span>
<span class="option"><label for="option_text_charset"><select id="option_text_charset" name="charset"><optgroup label="Populars"><option selected="" value="UTF-8">UTF-8</option><option value="ASCII">ASCII</option><option value="ISO-8859-1">ISO-8859-1</option><option value="ISO-8859-2">ISO-8859-2</option><option value="ISO-8859-6">ISO-8859-6</option><option value="ISO-8859-15">ISO-8859-15</option><option value="Windows-1252">Windows-1252</option></optgroup><optgroup label="Others"><option value="ArmSCII-8">ArmSCII-8</option><option value="BIG-5">BIG-5</option><option value="CP850">CP850</option><option value="CP866">CP866</option><option value="CP932">CP932</option><option value="CP936">CP936</option><option value="CP950">CP950</option><option value="CP50220">CP50220</option><option value="CP50221">CP50221</option><option value="CP50222">CP50222</option><option value="CP51932">CP51932</option><option value="EUC-CN">EUC-CN</option><option value="EUC-JP">EUC-JP</option><option value="EUC-KR">EUC-KR</option><option value="EUC-TW">EUC-TW</option><option value="GB18030">GB18030</option><option value="HZ">HZ</option><option value="ISO-2022-JP">ISO-2022-JP</option><option value="ISO-2022-KR">ISO-2022-KR</option><option value="ISO-8859-3">ISO-8859-3</option><option value="ISO-8859-4">ISO-8859-4</option><option value="ISO-8859-5">ISO-8859-5</option><option value="ISO-8859-7">ISO-8859-7</option><option value="ISO-8859-8">ISO-8859-8</option><option value="ISO-8859-9">ISO-8859-9</option><option value="ISO-8859-10">ISO-8859-10</option><option value="ISO-8859-13">ISO-8859-13</option><option value="ISO-8859-14">ISO-8859-14</option><option value="ISO-8859-16">ISO-8859-16</option><option value="JIS">JIS</option><option value="KOI8-R">KOI8-R</option><option value="KOI8-U">KOI8-U</option><option value="SJIS">SJIS</option><option value="UCS-2">UCS-2</option><option value="UCS-2BE">UCS-2BE</option><option value="UCS-2LE">UCS-2LE</option><option value="UCS-4">UCS-4</option><option value="UCS-4BE">UCS-4BE</option><option value="UCS-4LE">UCS-4LE</option><option value="UHC">UHC</option><option value="UTF-7">UTF-7</option><option value="UTF-16">UTF-16</option><option value="UTF-16BE">UTF-16BE</option><option value="UTF-16LE">UTF-16LE</option><option value="UTF-32">UTF-32</option><option value="UTF-32BE">UTF-32BE</option><option value="UTF-32LE">UTF-32LE</option><option value="UTF7-IMAP">UTF7-IMAP</option><option value="Windows-1251">Windows-1251</option><option value="Windows-1254">Windows-1254</option></optgroup></select>Destination character set.</label></span>
<span class="option"><label for="option_text_separator"><select id="option_text_separator" name="separator"><option value="lf">LF (Unix)</option><option value="crlf">CRLF (Windows)</option></select>Destination newline separator.</label></span>
<span class="option"><label for="option_text_newlines"><input id="option_text_newlines" name="newlines" type="checkbox"/>Encode each line separately (useful for multiple entries).</label></span>
<span class="option"><label for="option_text_chunks"><input id="option_text_chunks" name="chunks" type="checkbox"/>Split lines into 76 character wide chunks (useful for MIME).</label></span>
<span class="option"><label for="option_text_urlsafe"><input id="option_text_urlsafe" name="urlsafe" type="checkbox"/>Perform URL safe encoding (uses Base64URL format).</label></span>
<span class="option"><label for="option_text_live"><button class="live" id="option_text_live" type="button"><i class="fas fa-toggle-off"></i>Live mode OFF</button>Encodes in real-time when you type or paste (supports only UTF-8 character set).</label></span>
<span class="option"><button id="submit_text" type="submit"><i class="fas fa-chevron-right"></i>ENCODE<i class="fas fa-chevron-left"></i></button><label for="output">Encodes your data into the textarea below.</label></span>
</form>
<textarea id="output" name="output" placeholder="Result goes here..." spellcheck="false"></textarea>
<h2>Encode files to Base64 format</h2>
<label for="upload">Select a file to upload and process, then you can download the encoded result.</label>
<div class="form" data-action="https://www.base64encode.org/">
<div class="file">
<span class="counter">0</span>
<i class="fas fa-file-alt"></i>Click (or tap) here to select a file    <input id="upload" name="upload[]" type="file"/>
</div>
<span class="info"><i class="fas fa-info-circle"></i>Maximum file size is 192MB.</span>
<span class="option"><label for="option_file_charset"><select id="option_file_charset" name="charset"><option selected="" value="BINARY">BINARY (no conv.)</option><optgroup label="Populars"><option value="UTF-8">UTF-8</option><option value="ASCII">ASCII</option><option value="ISO-8859-1">ISO-8859-1</option><option value="ISO-8859-2">ISO-8859-2</option><option value="ISO-8859-6">ISO-8859-6</option><option value="ISO-8859-15">ISO-8859-15</option><option value="Windows-1252">Windows-1252</option></optgroup><optgroup label="Others"><option value="ArmSCII-8">ArmSCII-8</option><option value="BIG-5">BIG-5</option><option value="CP850">CP850</option><option value="CP866">CP866</option><option value="CP932">CP932</option><option value="CP936">CP936</option><option value="CP950">CP950</option><option value="CP50220">CP50220</option><option value="CP50221">CP50221</option><option value="CP50222">CP50222</option><option value="CP51932">CP51932</option><option value="EUC-CN">EUC-CN</option><option value="EUC-JP">EUC-JP</option><option value="EUC-KR">EUC-KR</option><option value="EUC-TW">EUC-TW</option><option value="GB18030">GB18030</option><option value="HZ">HZ</option><option value="ISO-2022-JP">ISO-2022-JP</option><option value="ISO-2022-KR">ISO-2022-KR</option><option value="ISO-8859-3">ISO-8859-3</option><option value="ISO-8859-4">ISO-8859-4</option><option value="ISO-8859-5">ISO-8859-5</option><option value="ISO-8859-7">ISO-8859-7</option><option value="ISO-8859-8">ISO-8859-8</option><option value="ISO-8859-9">ISO-8859-9</option><option value="ISO-8859-10">ISO-8859-10</option><option value="ISO-8859-13">ISO-8859-13</option><option value="ISO-8859-14">ISO-8859-14</option><option value="ISO-8859-16">ISO-8859-16</option><option value="JIS">JIS</option><option value="KOI8-R">KOI8-R</option><option value="KOI8-U">KOI8-U</option><option value="SJIS">SJIS</option><option value="UCS-2">UCS-2</option><option value="UCS-2BE">UCS-2BE</option><option value="UCS-2LE">UCS-2LE</option><option value="UCS-4">UCS-4</option><option value="UCS-4BE">UCS-4BE</option><option value="UCS-4LE">UCS-4LE</option><option value="UHC">UHC</option><option value="UTF-7">UTF-7</option><option value="UTF-16">UTF-16</option><option value="UTF-16BE">UTF-16BE</option><option value="UTF-16LE">UTF-16LE</option><option value="UTF-32">UTF-32</option><option value="UTF-32BE">UTF-32BE</option><option value="UTF-32LE">UTF-32LE</option><option value="UTF7-IMAP">UTF7-IMAP</option><option value="Windows-1251">Windows-1251</option><option value="Windows-1254">Windows-1254</option></optgroup></select>Destination character set for text files.</label></span>
<span class="option"><label for="option_file_separator"><select disabled="" id="option_file_separator" name="separator"><option value="lf">LF (Unix)</option><option value="crlf">CRLF (Windows)</option></select>Newline separator for encode each line separately and split lines into chunks.</label></span>
<span class="option"><label for="option_file_newlines"><input id="option_file_newlines" name="newlines" type="checkbox"/>Encode each line separately (useful for multiple entries).</label></span>
<span class="option"><label for="option_file_chunks"><input id="option_file_chunks" name="chunks" type="checkbox"/>Split lines into 76 character wide chunks (useful for MIME).</label></span>
<span class="option"><label for="option_file_urlsafe"><input id="option_file_urlsafe" name="urlsafe" type="checkbox"/>Perform URL safe encoding (uses Base64URL format).</label></span>
<span class="option"><button id="submit_file" type="button"><i class="fas fa-chevron-right"></i>ENCODE<i class="fas fa-chevron-left"></i></button></span>
<div class="state" data-state="submitting">
<i class="fas fa-spinner fa-spin"></i><b>Working...</b><br/><i>Please wait until the encoding process completes.</i>
</div>
<div class="state" data-state="success">
<i class="fas fa-check-circle"></i><b>Success!</b><br/><a href="https://www.base64encode.org/download/{{ output }}" rel="nofollow" target="_blank">CLICK OR TAP HERE</a> to download the encoded file.<br/><i>Please note that this file is removed from our system right after the first download attempt or 15 minutes of inactivity.</i>
</div>
<div class="state" data-state="error">
<i class="fas fa-exclamation-triangle"></i><b>Error!</b><label>Something went wrong:</label>{{ error }}
    </div>
</div>
</section>
<article>
<h2><i class="fas fa-question-circle"></i>About</h2>
Meet Base64 Decode and Encode, a simple online tool that does exactly what it says; decodes from Base64 encoding and encodes into it quickly and easily. Base64 encode your data in a hassle-free way, or decode it into human-readable format.<br/>
<br/>
Base64 encoding schemes are commonly used when there is a need to encode binary data that needs be stored and transferred over media that are designed to deal with textual data. This is to ensure that the data remains intact without modification during transport. Base64 is used commonly in a number of applications including email via MIME, and storing complex data in XML or JSON.<br/>
<br/>
<b>Advanced options</b><br/>
<ul>
<li><u>Character set:</u> Our website uses UTF-8 character set, your input data is transmitted in that format. Change this option if you want to convert it to another one before encoding. Note that in case of textual data the encoding scheme does not contain their character set, so you may have to specify the selected one during the decoding process. As for files a binary option is the default, which will omit any conversion; this is required for everything except plain text documents.</li>
<li><u>Newline separator:</u> Unix and Windows systems uses different line break characters, prior encoding either variants will be replaced within your data to the selected option. At the files section this is partially irrelevant since they contain intended versions, but you can define which one to use for the encode each line separately and split lines into chunks functions.</li>
<li><u>Encode each line separately:</u> Even newline characters are converted to their base64 encoded forms. Use this option if you want to encode multiple independent data entries separated with line breaks. <i>(*)</i></li>
<li><u>Split lines into chunks:</u> The encoded data will be a continuous text without any whitespaces, check this option if you want to break it up into multiple lines. The applied character limit is defined in the MIME (RFC 2045) specification, which states that the encoded lines must be no more than 76 characters long. <i>(*)</i></li>
<li><u>Perform URL safe encoding:</u> Using standard Base64 in URLs requires encoding of "+", "/" and "=" characters into their percent-encoded form, which makes the string unnecessarily longer. Enable this option to encode into an URL and file name friendly Base64 variant (RFC 4648 / Base64URL) where the "+" and "/" characters are respectively replaced by "-" and "_", as well as the padding "=" signs are omitted.</li>
<li><u>Live mode:</u> When you turn on this option the entered data is encoded immediately with your browser's built-in JavaScript functions - without sending any information to our servers. Currently this mode supports only the UTF-8 character set.</li>
</ul>
<i>(*) These options cannot be enabled simultaneously, since the resulting output would not be valid for the majority of applications.</i><br/>
<br/>
<b>Safe and secure</b><br/>
<br/>
All communications with our servers are made through secure SSL encrypted connections (https). Uploaded files are deleted from our servers immediately after being processed, and the resulting downloadable file is deleted right after the first download attempt, or 15 minutes of inactivity. We do not keep or inspect the contents of the entered data or uploaded files in any way. Read our privacy policy below for more details.<br/>
<br/>
<b>Completely free</b><br/>
<br/>
Our tool is free to use. From now you don't have to download any software for such tasks.<br/>
<br/>
<b>Details of the Base64 encoding</b><br/>
<br/>
Base64 is a generic term for a number of similar encoding schemes that encode binary data by treating it numerically and translating it into a base 64 representation. The Base64 term originates from a specific MIME content transfer encoding.<br/>
<br/>
<u>Design</u><br/>
<br/>
The particular choice of characters to make up the 64 characters required for base varies between implementations. The general rule is to choose a set of 64 characters that is both part of a subset common to most encodings, and also printable. This combination leaves the data unlikely to be modified in transit through systems, such as email, which were traditionally not 8-bit clean. For example, MIME's Base64 implementation uses A-Z, a-z, and 0-9 for the first 62 values, "+" and "/" for the last two. Other variations, usually derived from Base64, share this property but differ in the symbols chosen for the last two values; an example is the URL and file name safe (RFC 4648 / Base64URL) variant, which uses "-" and "_".<br/>
<br/>
<u>Example</u><br/>
<br/>
A quote snippet from Thomas Hobbes's Leviathan:<br/>
<br/>
"<i>Man is distinguished, not only by his reason, but ...</i>"<br/>
<br/>
represented as an ASCII byte sequence is encoded in MIME's Base64 scheme as follows:<br/>
<br/>
TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCAuLi4=<br/>
<br/>
In the above quote the encoded value of <i>Man</i> is <i>TWFu</i>. Encoded in ASCII, <i>M</i>, <i>a</i>, <i>n</i> are stored as the bytes 77, 97, 110, which are 01001101, 01100001, 01101110 in base 2. These three bytes are joined together in a 24 bit buffer producing 010011010110000101101110. Packs of 6 bits (6 bits have a maximum of 64 different binary values) are converted into 4 numbers (24 = 4 * 6 bits) which are then converted to their corresponding values in Base64.<br/>
<br/>
<table>
<tr>
<th>Text content</th>
<td colspan="8">M</td>
<td colspan="8">a</td>
<td colspan="8">n</td>
</tr>
<tr>
<th>ASCII</th>
<td colspan="8">77</td>
<td colspan="8">97</td>
<td colspan="8">110</td>
</tr>
<tr>
<th>Bit pattern</th>
<td>0</td>
<td>1</td>
<td>0</td>
<td>0</td>
<td>1</td>
<td>1</td>
<td>0</td>
<td>1</td>
<td>0</td>
<td>1</td>
<td>1</td>
<td>0</td>
<td>0</td>
<td>0</td>
<td>0</td>
<td>1</td>
<td>0</td>
<td>1</td>
<td>1</td>
<td>0</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>0</td>
</tr>
<tr>
<th>Index</th>
<td colspan="6">19</td>
<td colspan="6">22</td>
<td colspan="6">5</td>
<td colspan="6">46</td>
</tr>
<tr>
<th>Base64-encoded</th>
<td colspan="6">T</td>
<td colspan="6">W</td>
<td colspan="6">F</td>
<td colspan="6">u</td>
</tr>
</table>
<br/>
As this example illustrates, Base64 encoding converts 3 uncoded bytes (in this case, ASCII characters) into 4 encoded ASCII characters.      </article>
</main>
<aside>
<nav>
<h3 class="bookmark"><i class="fas fa-star"></i>Bonus tip: Bookmark us!</h3>
</nav>
<ins class="ad_right"></ins>
<nav>
<h3><i class="fas fa-cog"></i>Other tools</h3>
<a href="https://www.urldecoder.org/" rel="external">URL Decode<i class="far fa-envelope-open"></i></a>
<a href="https://www.urlencoder.org/" rel="external">URL Encode<i class="far fa-envelope"></i></a>
<a href="https://www.minifyjson.org/" rel="external">JSON Minify<i class="far fa-file-archive"></i></a>
<a href="https://www.beautifyjson.org/" rel="external">JSON Beautify<i class="far fa-file-code"></i></a>
<a href="https://www.uglifyjs.net/" rel="external">JS Minify<i class="far fa-hand-rock"></i></a>
<a href="https://www.prettifyjs.net/" rel="external">JS Beautify<i class="far fa-hand-paper"></i></a>
<a href="https://www.uglifycss.com/" rel="external">CSS Minify<i class="far fa-meh"></i></a>
<a href="https://www.prettifycss.com/" rel="external">CSS Beautify<i class="far fa-smile"></i></a>
</nav>
<nav>
<h3><i class="fas fa-random"></i>Partner sites</h3>
<a href="https://www.convzone.com/" rel="external">Number System Converter<i class="fas fa-exchange-alt"></i></a>
<a href="https://www.ratingraph.com/" rel="external">TV Show and Movie Ratings<i class="fas fa-chart-line"></i></a>
<a href="https://www.chatcrypt.com/" rel="external">Secure Group Chat<i class="fas fa-shield-alt"></i></a>
</nav>
<ins class="ad_corner"></ins>
</aside>
<div class="clear"></div>
<a class="switch" href="https://amp.base64encode.org/"><i class="fas fa-tablet-alt"></i>Switch to mobile version</a>
<footer><i class="far fa-copyright"></i>2010-2021 base64encode.org<br/><a href="https://www.base64encode.org/privacy/"><i class="fas fa-lock"></i>Privacy policy</a><a href="mailto:contact@base64encode.org"><i class="fas fa-envelope"></i>Contact</a><br/><b>This website uses cookies.</b> We use cookies to personalise content and ads, and to analyse our traffic.</footer>
</div>
<ins class="ad_left"></ins>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-74823759-31"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-74823759-31');
  </script>
<script async="" src="https://cdn.base64encode.org/assets/build/bundle.5d4a46dd39df095d91b383ca475850fe951f8ef4.js"></script>
</body>
</html>