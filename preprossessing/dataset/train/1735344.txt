<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Zydus Frontline</title>
<style type="text/css">
        body {
	        margin-left: 0px;
	        margin-top: 0px;
	        margin-right: 0px;
	        margin-bottom: 0px;
	        background-color: #EADDA8;
	        font-family: Verdana;
	        font-size: 12px;
        }
        .style2 {
	        font-family: Verdana;
	        font-size: 12px;
	        color: Navy;
        }
    </style>
<script language="JavaScript" type="text/javascript">
        function validate()
        {
            var UserName = document.getElementById('txt_id').value;
            var Password = document.getElementById('txt_pwd').value;
            if (UserName == "")
            {
	            alert("Enter User Id");
	            document.getElementById('txt_id').focus();
	            return false;
            }
            if (Password == "")
            {
	            alert("Enter Password");
	            document.getElementById('txt_pwd').focus();
	            return false;
            }
         }

        function window_onload()
        {
             window.document.log_form.txt_id.value = "";
             window.document.log_form.txt_pwd.value = "";
             window.document.log_form.txt_id.focus();
        }
    </script>
</head>
<body>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="1002">
<tr>
<td>
<img alt="" src="images/banner.jpg" width="1002"/></td>
</tr>
<tr>
<td align="center" style="background-color: #FFFFFF;">
<img alt="" src="images/FrontlineLogo.jpg" width="223"/>
</td>
</tr>
<tr>
<td>
<img alt="" src="images/banner2.jpg" width="1002"/></td>
</tr>
<tr>
<td style="height: 100%; background-image: url(images/bg.jpg);">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="center" style="width: 45%;" valign="top">
<img alt="" src="images/Zynext.png"/></td>
<td class="style2" style="width: 50%;" valign="middle">
<form action="pwd_check.asp" id="log_form" method="post" name="log_form">
<table border="0" cellpadding="3" cellspacing="0" width="500">
<tr>
<td align="left" colspan="2">
<span style="font-size:12px;font-family:Verdana;font-weight:bold;color: red;">
</span>
</td>
</tr>
<tr>
<td align="left" style="width: 60px;">User ID:</td>
<td align="left" style="width: 440px;">
<input autocomplete="off" id="txt_id" name="txt_id" style="width: 120px;TEXT-TRANSFORM: uppercase; font-family: Verdana;"/>
</td>
</tr>
<tr>
<td align="left">Password:</td>
<td align="left">
<input id="txt_pwd" name="txt_pwd" style="width: 120px; font-family: Verdana;" type="password"/>
</td>
</tr>
<tr>
<td> </td>
<td align="left">
<input id="imgLogin" onclick="javascript:return validate();" src="../images/loginButton.gif" type="image"/>
</td>
</tr>
<tr>
<td> </td>
<td align="left"><a href="frontlinenet\UserSecurity\ResetPassword.aspx" style="text-decoration: none;">Forgot Password</a> </td>
</tr>
</table>
</form>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td style="background-image: url(images/bg.jpg);" valign="middle">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="center" style="width: 90%;">Zydus Frontline is best viewed in IE (Internet Explorer) Version :- 7.0/8.0/9.0<br/><br/>@ 2002 Zydus Cadila. All right reserved. Developed and maintained by InfoTech Dept., Zydus Cadila, Ahmedabad.</td>
<td>
<img alt="" src="images/Zydus.png" width="89"/></td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
