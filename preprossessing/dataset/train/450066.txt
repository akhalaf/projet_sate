<!DOCTYPE html>
<html> <head> <meta charset="utf-8"/> <title>e-riadmarrakech.com is registered with Mailchimp</title> <meta content="Mailchimp domain page for e-riadmarrakech.com" name="description"/> <meta content="width=device-width, initial-scale=1.0" name="viewport"/> <meta content="e-riadmarrakech.com is registered with Mailchimp" property="og:title"/> <meta content="" property="og:type"/> <meta content="https://mailchimp.com/features/domains/?utm_source=parked_page&amp;utm_medium=user_domain&amp;utm_campaign=domain_logo" property="og:url"/> <meta content="https://static.mailchimpsites.com/images/parked-pages/Hero-Jazzband.gif" property="og:image"/> <style type="text/css">

        @font-face {
            font-family: "Cooper Light";
            src: url("https://static.mailchimpsites.com/fonts/cooper/CooperBTW03-Light.eot?#iefix");
            src:
                url("https://static.mailchimpsites.com/fonts/cooper/CooperBTW03-Light.eot?#iefix") format("eot"),
                url("https://static.mailchimpsites.com/fonts/cooper/CooperBTW03-Light.woff2") format("woff2"),
                url("https://static.mailchimpsites.com/fonts/cooper/CooperBTW03-Light.woff") format("woff"),
                url("https://static.mailchimpsites.com/fonts/cooper/CooperBTW03-Light.ttf") format("truetype");
        }

        @font-face {
            font-family: 'Graphik Web';
            src: url('https://static.mailchimpsites.com/fonts/graphik/Graphik-Regular-Web.eot');
            src:
                url('https://static.mailchimpsites.com/fonts/graphik/Graphik-Regular-Web.eot?#iefix') format('embedded-opentype'),
                url('https://static.mailchimpsites.com/fonts/graphik/Graphik-Regular-Web.woff2') format('woff2'),
                url('https://static.mailchimpsites.com/fonts/graphik/Graphik-Regular-Web.woff') format('woff');
            font-weight: 400;
            font-style: normal;
            font-stretch: normal;
        }

        @font-face {
            font-family: 'Graphik Web';
            src: url('https://static.mailchimpsites.com/fonts/graphik/Graphik-Medium-Web.eot');
            src:
                url('https://static.mailchimpsites.com/fonts/graphik/Graphik-Medium-Web.eot?#iefix') format('embedded-opentype'),
                url('https://static.mailchimpsites.com/fonts/graphik/Graphik-Medium-Web.woff2') format('woff2'),
                url('https://static.mailchimpsites.com/fonts/graphik/Graphik-Medium-Web.woff') format('woff');
            font-weight: 500;
            font-style: normal;
            font-stretch: normal;
        }

        *,
        *::before,
        *::after {
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }
        html, body {
            min-height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            border: 0;
        }
        body {
          font-family: "Graphik Web", "Helvetica Neue", Helvetica, Arial, Verdana, sans-serif;
          -webkit-font-smoothing: antialiased;
          -moz-osx-font-smoothing: grayscale;
          -webkit-text-size-adjust: 100%;
          -ms-text-size-adjust: 100%;
          font-size: 1.6rem;
          line-height: 1.5;
          text-rendering: optimizelegibility;
          vertical-align: baseline;

        }
        .container {
            display: flex;
            flex-direction: column;
            justify-content: center;
        }

        h1 {
            font-family: "Cooper Light", Georgia, Times, "Times New Roman", serif;
            font-size: 40px;
            line-height: 1;
            margin: 0;
            font-weight: 300;
        }

        p {
            font-style: normal;
            font-weight: 300;
            font-size: 24px;
            line-height: 1.2;
            margin: 0;
            padding: 24px 0 30px 0;
        }

        .format {
            display: flex;
            flex-direction: column;
            max-width: 1300px;
            justify-content: center;
            flex: 1 1 auto;
            align-items: center;
            align-self: center;
        }

        .format > div {
            flex: 1;
            max-width: 635px;
            word-break: break-word;
        }

        .headingAndDescription {
            padding: 0 36px;
        }

        .gifContainer {
            margin-left: 10%;
        }

        .gif {
            text-align: center;
            width: 100%;
        }

        .logo {
            width: 162px;
            height: 45px;
            margin: 24px 24px 60px 24px;
        }

        .button {
            display: inline-flex;
            border: 1px solid transparent;
            border-radius: 2px;
            align-items: center;
            justify-content: center;
            font-weight: 500;
            font-size: 14px;
            line-height: 14px;
            white-space: nowrap;
            padding: 16px 32px;
            user-select: none;
            font-family: "Graphik Web", Helvetica, Arial, Verdana, sans-serif, bold;
            font-weight: bold;
            text-transform: capitalize;
            text-align: center;
            color: #FFFFFF;
            background: #007c89;
            cursor: pointer;
            text-decoration: none;
        }

        @media only screen and (min-width: 960px) {
            .container {
                height: 100vh;
                padding: 16px;
                overflow: hidden;
            }
            .logo {
                margin-bottom: 0;
            }
            h1 {
                font-size: 48px;
            }
            .format {
                flex-direction: row;
                padding-right: 0;
            }
            .headingAndDescription {
                padding-right: 0;
            }
            .gif {
                width: 522px;
                height: 522px;
            }
        }

        @media only screen and (min-width: 1200px) {
            .container {
                margin: 0 auto;
            }
            .headingAndDescription {
                padding: 0;
            }
            .gifContainer {
                margin-left: 0;
                margin-right: -15%;
            }
        }
    </style> </head> <body style="background-color:#FBEECA"> <div class="container"> <a class="logo" href="https://mailchimp.com/features/domains/?utm_source=parked_page&amp;utm_medium=user_domain&amp;utm_campaign=domain_logo" title="Mailchimp"> <img alt="Mailchimp logo" height="100%" src="https://eep.io/mc-cdn-images/template_images/branding_logo_text_dark.svg" width="100%"/> </a> <div class="format"> <div class="headingAndDescription"> <h1>e-riadmarrakech.com is registered with Mailchimp</h1> <p>Put your catchiest ideas online with your own domain.</p> <a class="button" href="https://mailchimp.com/features/domains/?utm_source=parked_page&amp;utm_medium=user_domain&amp;utm_campaign=domain_cta" role="button">Find Your Domain</a> </div> <div class="gifContainer"> <img class="gif" height="100%" src="https://static.mailchimpsites.com/images/parked-pages/Hero-Jazzband.gif" width="100%"/> </div> </div> </div> </body> </html>
