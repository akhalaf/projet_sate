<!DOCTYPE html>
<!-- Last Published: Wed Jan 13 2021 13:52:42 GMT+0000 (Coordinated Universal Time) --><html data-wf-domain="www.basketball.ca" data-wf-page="5fceb0f3309185de65eb6644" data-wf-site="5d24fc966ad064837947a33b" lang="en"><head><meta charset="utf-8"/><title>Not Found</title><meta content="Not Found" property="og:title"/><meta content="Not Found" property="twitter:title"/><meta content="width=device-width, initial-scale=1" name="viewport"/><link href="https://assets-global.website-files.com/5d24fc966ad064837947a33b/css/canada-basketball.8e8639cd3.css" rel="stylesheet" type="text/css"/><script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script><script type="text/javascript">WebFont.load({  google: {    families: ["Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic"]  }});</script><script src="https://use.typekit.net/urv2amn.js" type="text/javascript"></script><script type="text/javascript">try{Typekit.load();}catch(e){}</script><!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif]--><script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script><link href="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5d51eea9d4a63020f237263f_32-fav.png" rel="shortcut icon" type="image/x-icon"/><link href="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5d51ee8b2ca59a1a38929b9a_256-fav.png" rel="apple-touch-icon"/><script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-52036200-2"></script><script type="text/javascript">window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-52036200-2', {'anonymize_ip': false});</script><style>
  .article:hover .article-img-bg, .article-big:hover .article-img-bg  {
    transform: scale(1.03);
  }
  
  .wrapper-right-cover {
   	display:block; 
  }
  
body {
  background-color: #f4f4f4;
  }

.w-dropdown {
  	z-index: 1;
  }
  
div[style*="background-color: rgb(186, 12, 47);"] .nav-link:hover, div[style*="background-color: rgb(186, 12, 47);"] .nav-link.w--current {
   border-bottom-color: white;
}

div[style*="background-color: rgb(186, 12, 47);"] {
  border-bottom: 0;
  }
  
  
.dropdown {
  border-bottom: 1px solid #dadada!important;
  }
  
@media (max-width: 767px) {
  .score-box, .tab-flex, .vid-flex, .international-results {
    /*display: flex;*/
    flex-wrap: nowrap;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
  }
}
  

@media (min-width: 1440px) { 
.wrapper-right-cover  {
    display: block;
    position: fixed;
    height: 100vh;
    width: calc((100vw - 1440px) / 2);
    right: 0px;
    z-index: 1999; 
  	background-color: #eaeaea;
    top: 0;
 }
  
.overlay-menu, .cbf-iframe-container {
	right:calc((100vw - 1440px) / 2);

  }  
}
  
</style>
<style>
	#tab-select {
  	-webkit-appearance: none;
   	-moz-appearance: none;
   	appearance: none;
    text-align-last: center;
    font-weight: 600;
    padding: 1rem;
    margin-bottom: 2rem;
    text-align: center;
	border: 0; 
    border-top: 1px solid #dadada;
   	border-bottom: 1px solid #dadada;
    width: 100%;
    border-radius: 0;
    background-color: #fff;
    background-image: url("https://assets-global.website-files.com/5d24fc966ad064837947a33b/5ed8272b37221c9f5dd0e324_down-chevron.svg");
    background-position: 97.5% 50%;
    background-size: 12px;
    background-repeat: no-repeat;
  }
  
  #tab-select:focus {
    outline: none;
  }
  
  @media screen and (min-width: 991px) {
      #tab-select {
     	display: none;   
    }
  }
</style></head><body><div class="body-wrapper"><div class="navigation"><div class="navbar w-nav" data-animation="default" data-collapse="medium" data-duration="400" role="banner"><div class="logo"><div class="hamburger-container-mobile"><a class="lang w-nav-link" href="#" id="language">FR</a></div><a class="canada-basketball-logo w-inline-block" href="/"><img alt="" class="top-logo" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5d52f6c5d4a63059d33d7155_cb-logo.svg"/><img alt="" class="bottom-logo" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5d52f6e87d6b7b06ecede69d_cb-text.svg" width="215"/></a><div class="hamburger-container-mobile"><a class="hamburger w-inline-block" href="#"><div class="hamburger-icon"><div class="hamburger-top"></div><div class="hamburger-middle"></div><div class="hamburger-bottom"></div></div></a></div></div><div class="nav-container"><form action="/search" class="global-search w-form"><input class="global-search-input w-input" id="search" maxlength="256" name="query" placeholder="Searchâ¦" required="" type="search"/><input class="global-search-button w-button" type="submit" value=" "/><a class="search-close" href="#"></a></form><nav class="nav-utility w-nav-menu" role="navigation"><ul class="utility-nav w-list-unstyled" role="list"><li><a class="icon-search w-inline-block" href="#"><img alt="" class="icon" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5d278e6fa01d9f8f7eb7a840_magnify.svg"/></a></li><li><a class="icon-a w-inline-block" href="/en/events"><img alt="" class="icon" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5d278eacf06a31a628a102de_ticket.svg"/></a></li><li><a class="icon-a w-inline-block" href="http://shop.basketball.ca" target="_blank"><img alt="" class="icon" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5d278eacfc344f47ddddd9da_cart.svg"/></a></li><li><a class="donate w-nav-link" href="/en/about/canada-basketball-foundation">donate</a></li><li><a class="lang w-nav-link" href="#" id="language">FR</a></li></ul></nav><nav class="nav-menu w-nav-menu" role="navigation"><div class="nav-main"><div class="nav-link special-nav-link w-dropdown" data-delay="0"><div class="dropdown-toggle w-dropdown-toggle"><div class="text">team canada</div></div><nav class="dropdown w-dropdown-list"><div class="container-header"><div class="_40"><img alt="" sizes="100vw" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5d4352b6d5bffd77ae24c7cd_stand-on-gaurd.png" srcset="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5d4352b6d5bffd77ae24c7cd_stand-on-gaurd-p-500.png 500w, https://assets-global.website-files.com/5d24fc966ad064837947a33b/5d4352b6d5bffd77ae24c7cd_stand-on-gaurd.png 600w" width="380"/></div><div class="_20"><h5 class="dropdown-link-header">WoMEN</h5><a class="dropdown-link w-dropdown-link" href="/team-canada-en/team-canada-senior-womens-national-team-fiba-womens-olympic-qualifying-tournament">senior</a><a class="dropdown-link w-dropdown-link" href="/team-canada-en/team-canada-u19-womens-national-team">u19</a><a class="dropdown-link w-dropdown-link" href="/team-canada-en/team-canada-u18-womens-national-team">u18</a><a class="dropdown-link w-dropdown-link" href="/team-canada-en/team-canada-u17-womens-national-team">u17</a><a class="dropdown-link w-dropdown-link" href="/team-canada-en/team-canada-u16-womens-national-team">u16</a><a class="dropdown-link hide w-dropdown-link" href="/team-canada-en/3x3-womens-national-team">3x3</a></div><div class="_20"><h5 class="dropdown-link-header">men</h5><a class="dropdown-link w-dropdown-link" href="/team-canada-en/team-canada-senior-mens-national-team-fiba-americup-2021-qualifiers-2021-window-1">senior</a><a class="dropdown-link w-dropdown-link" href="/team-canada-en/u19-mens-national-team">u19</a><a class="dropdown-link w-dropdown-link" href="/team-canada-en/team-canada-u18-mens-national-team">u18</a><a class="dropdown-link w-dropdown-link" href="/team-canada-en/team-canada-u17-mens-national-team">u17</a><a class="dropdown-link w-dropdown-link" href="/team-canada-en/team-canada-u16-mens-national-team">u16</a><a class="dropdown-link hide w-dropdown-link" href="/team-canada-en/3x3-mens-national-team">3x3</a></div><div class="_20 justify-start"><a class="dropdown-link w-dropdown-link" href="/en/team-canada/player-profiles"><span class="dropdown-link-header no-border">player profiles</span></a><a class="dropdown-link hide w-dropdown-link" href="/en/alumni/hall-of-fame"><span class="dropdown-link-header no-border">Alumni</span></a><a class="dropdown-link w-dropdown-link" href="/en/events"><span class="dropdown-link-header no-border">Events</span></a></div></div></nav></div><div class="nav-link special-nav-link w-dropdown" data-delay="0"><div class="dropdown-toggle w-dropdown-toggle"><div class="text">development</div></div><nav class="dropdown w-dropdown-list"><div class="container-header"><div class="_40"><img alt="" sizes="100vw" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5d4352b6d5bffd77ae24c7cd_stand-on-gaurd.png" srcset="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5d4352b6d5bffd77ae24c7cd_stand-on-gaurd-p-500.png 500w, https://assets-global.website-files.com/5d24fc966ad064837947a33b/5d4352b6d5bffd77ae24c7cd_stand-on-gaurd.png 600w" width="380"/></div><div class="_20 justify-start"><a class="dropdown-link w-dropdown-link" href="/en/development/coaches"><span class="dropdown-link-header no-border">coaches</span></a><a class="dropdown-link w-dropdown-link" href="/en/development/officials"><span class="dropdown-link-header no-border">officials</span></a><a class="dropdown-link w-dropdown-link" href="http://www.jrnba.ca/" target="_blank"><span class="dropdown-link-header no-border">jr. nba youth basketball</span></a><a class="dropdown-link w-dropdown-link" href="/en/development/3x3-canada-quest"><span class="dropdown-link-header no-border">3x3 Canada Quest</span></a><a class="dropdown-link w-dropdown-link" href="/en/development/safe-sport"><span class="dropdown-link-header no-border">Safe Sport</span></a><a class="dropdown-link hide w-dropdown-link" href="/en/development/3x3-canada-quest"><span class="dropdown-link-header no-border">3x3 canada quest</span></a></div><div class="_20 justify-start"><a class="dropdown-link w-dropdown-link" href="/en/development/junior-academy"><span class="dropdown-link-header no-border">junior academy</span></a><a class="dropdown-link w-dropdown-link" href="/en/development/national-championships"><span class="dropdown-link-header no-border">national championships</span></a><a class="dropdown-link w-dropdown-link" href="/en/development/targeted-athlete-strategy"><span class="dropdown-link-header no-border">targeted athlete strategy</span></a><a class="dropdown-link w-dropdown-link" href="/en/development/ltad"><span class="dropdown-link-header no-border">ltad</span></a></div></div></nav></div><a class="nav-link w-nav-link" href="/en/news">news</a><a class="nav-link w-nav-link" href="https://shop.basketball.ca/" target="_blank">shop</a></div><div class="hamburger-container"><a class="hamburger w-inline-block" href="#"><div class="hamburger-icon"><div class="hamburger-top"></div><div class="hamburger-middle"></div><div class="hamburger-bottom"></div></div></a></div></nav></div></div><div class="wrapper-right-cover"></div><div class="overlay-menu"><div class="overlay-menu-container"><ul class="list w-list-unstyled" role="list"><li class="list-dropdown"><a class="overlay-menu-link w-inline-block" href="#"><div class="menu-accordion"><div>team canada</div><div class="plus-icon"><div class="plus-line-vert"></div><div class="plus-line"></div></div></div></a><div class="list-submenu-container"><ul class="list-sub-menu w-list-unstyled" role="list"><li class="overlay-menu-link sub-menu-list-heading"><div class="overlay-menu-link sub-menu-list-heading">Women</div></li><li><a class="overlay-menu-link smaller" href="/team-canada-en/team-canada-senior-womens-national-team-fiba-womens-olympic-qualifying-tournament">Senior</a></li><li><a class="overlay-menu-link smaller" href="/team-canada-en/team-canada-u19-womens-national-team">U19</a></li><li><a class="overlay-menu-link smaller" href="/team-canada-en/team-canada-u18-womens-national-team">U18</a></li><li><a class="overlay-menu-link smaller" href="/team-canada-en/team-canada-u17-womens-national-team">U17</a></li><li><a class="overlay-menu-link smaller" href="/team-canada-en/team-canada-u16-womens-national-team">U16</a></li><li><a class="overlay-menu-link smaller hide" href="/team-canada-en/3x3-womens-national-team">3X3</a></li></ul><ul class="list-sub-menu w-list-unstyled" role="list"><li class="overlay-menu-link sub-menu-list-heading"><div class="overlay-menu-link sub-menu-list-heading">men</div></li><li><a class="overlay-menu-link smaller" href="/team-canada-en/team-canada-senior-mens-national-team-fiba-americup-2021-qualifiers-2021-window-1">Senior</a></li><li><a class="overlay-menu-link smaller" href="/team-canada-en/u19-mens-national-team">U19</a></li><li><a class="overlay-menu-link smaller" href="/team-canada-en/team-canada-u18-mens-national-team">U18</a></li><li><a class="overlay-menu-link smaller" href="/team-canada-en/team-canada-u17-mens-national-team">U17</a></li><li><a class="overlay-menu-link smaller" href="/team-canada-en/team-canada-u16-mens-national-team">U16</a></li><li><a class="overlay-menu-link smaller hide" href="/team-canada-en/3x3-mens-national-team">3X3</a></li></ul><ul class="list-sub-menu w-list-unstyled" role="list"><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/team-canada/player-profiles">Player Profiles</a></li></ul></div></li><li class="list-dropdown"><a class="overlay-menu-link w-inline-block" href="#"><div class="menu-accordion"><div>DEVELOPMENT</div><div class="plus-icon"><div class="plus-line-vert"></div><div class="plus-line"></div></div></div></a><div class="list-submenu-container"><ul class="list-sub-menu" role="list"><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/development/coaches">coaches</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/development/officials">officials</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="http://www.jrnba.ca/" target="_blank">Jr. NBA Youth Basketball</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/development/3x3-canada-quest">3x3 Canada Quest</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/development/safe-sport">Safe Sport</a></li><li class="hide"><a class="overlay-menu-link sub-menu-list-heading" href="/en/development/3x3-canada-quest">3x3 Canada Quest</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/development/junior-academy">Junior Academy</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/development/national-championships">National Championships</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/development/targeted-athlete-strategy">Targeted Athlete Strategy</a></li><li class="hide"><a class="overlay-menu-link sub-menu-list-heading" href="/en/development/basketball-club-and-association-excellence">Basketball Club and Association Excellence</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/development/ltad">LTAD</a></li></ul></div></li><li class="list-dropdown"><a class="overlay-menu-link w-inline-block" href="/en/news"><div>News</div></a></li><li class="list-dropdown"><a class="overlay-menu-link w-inline-block" href="/en/events"><div>Events</div></a></li><li class="list-dropdown"><a class="overlay-menu-link w-inline-block" href="#"><div class="menu-accordion"><div>ALUMNI</div><div class="plus-icon"><div class="plus-line-vert"></div><div class="plus-line"></div></div></div></a><div class="list-submenu-container"><ul class="list-sub-menu" role="list"><li><a class="overlay-menu-link sub-menu-list-heading hide" href="/en/alumni/special-events">special events</a></li><li><a class="overlay-menu-link sub-menu-list-heading hide" href="/en/alumni/nbtaa">nbtaa</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/alumni/hall-of-fame">hall of fame</a></li></ul></div></li><li class="list-dropdown"><a class="overlay-menu-link w-inline-block" href="#"><div class="menu-accordion"><div>ABOUT</div><div class="plus-icon"><div class="plus-line-vert"></div><div class="plus-line"></div></div></div></a><div class="list-submenu-container"><ul class="list-sub-menu" role="list"><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/about/governance">Governance</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/about/mission">Mission</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/about/careers">Careers</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/about/partners">Partners</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/about/ptsos">PTSOS</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/about/brand">Brand</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/about/canada-basketball-foundation">canada basketball foundation</a></li><li><a class="overlay-menu-link sub-menu-list-heading" href="/en/about/contact-us">Contact us</a></li></ul></div></li><li class="list-dropdown"><a class="overlay-menu-link w-inline-block" href="http://shop.basketball.ca" target="_blank"><div>Shop</div></a></li></ul></div><a class="menu-close" href="#">ï</a></div></div><div class="p4 mt-125"><div class="container"><div class="_404"><img alt="" class="_404-img" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5edaf61bb414c74940a3b657_cabb-logo-map.svg"/><h1 class="h1 red">Page Not FOund</h1><p class="p">The page you are looking for doesn't exist or has been moved.</p><a class="athlete-button w-button" href="#" id="go-back">Go Back</a></div></div></div><div class="footer"><div class="container"><div class="footer-bot"><div class="footer-left"><div class="footer-section _2-small"><a class="footer-link head" href="/en/news">News</a><a class="footer-link head" href="/en/events">Events</a><a class="footer-link head" href="http://shop.basketball.ca" target="_blank">Shop</a></div><div class="footer-section _1-5"><a class="footer-link head" href="#">Women's <span class="plus">+</span></a><div class="footer-expanded-menu"><a class="footer-link" href="#">Senior</a><a class="footer-link" href="/team-canada-en/team-canada-u19-womens-national-team">U19</a><a class="footer-link" href="/team-canada-en/team-canada-u18-womens-national-team">U18</a><a class="footer-link" href="/team-canada-en/team-canada-u17-womens-national-team">U17</a><a class="footer-link" href="/team-canada-en/team-canada-u16-womens-national-team">U16</a><a class="footer-link hide" href="/team-canada-en/3x3-womens-national-team">3x3</a></div></div><div class="footer-section _1-5"><a class="footer-link head" href="#">Men's <span class="plus">+</span></a><div class="footer-expanded-menu"><a class="footer-link" href="/team-canada-en/team-canada-senior-mens-national-team-fiba-americup-2021-qualifiers-2021-window-1">Senior</a><a class="footer-link" href="/team-canada-en/u19-mens-national-team">U19</a><a class="footer-link" href="/team-canada-en/team-canada-u18-mens-national-team">U18</a><a class="footer-link" href="/team-canada-en/team-canada-u17-mens-national-team">U17</a><a class="footer-link" href="/team-canada-en/team-canada-u16-mens-national-team">U16</a><a class="footer-link hide" href="/team-canada-en/3x3-mens-national-team">3x3</a></div></div><div class="footer-section _2"><div class="footer-stacked"><a class="footer-link head" href="#">Development <span class="plus">+</span></a><div class="footer-expanded-menu"><a class="footer-link" href="/en/development/coaches">Coaches</a><a class="footer-link" href="/en/development/officials">Officials</a><a class="footer-link" href="http://www.jrnba.ca/" target="_blank">Jr. NBAÂ Youth Basketball</a><a class="footer-link" href="/en/development/3x3-canada-quest">3x3 Canada Quest</a><a class="footer-link" href="/en/development/safe-sport">Safe Sport</a><a class="footer-link hide" href="/en/development/3x3-canada-quest">3x3 Canada Quest</a><a class="footer-link" href="/en/development/junior-academy">Junior Academy<br/></a><a class="footer-link" href="/en/development/national-championships">National Championships<br/></a><a class="footer-link" href="/en/development/targeted-athlete-strategy">Targeted Athlete Strategy<br/></a><a class="footer-link" href="/en/development/ltad">LTAD</a></div></div><div class="footer-stacked mb-0"><a class="footer-link head" href="#">Alumni <span class="plus">+</span></a><div class="footer-expanded-menu"><a class="footer-link" href="/en/alumni/hall-of-fame">Hall of Fame</a></div></div></div><div class="footer-section _2"><a class="footer-link head" href="#">About <span class="plus">+</span></a><div class="footer-expanded-menu"><a class="footer-link" href="/en/about/governance">Governance</a><a class="footer-link" href="/en/about/mission">Mission</a><a class="footer-link" href="/en/about/careers">Careers</a><a class="footer-link" href="/en/about/partners">Partners</a><a class="footer-link" href="/en/about/ptsos">PTSOs</a><a class="footer-link" href="/en/about/brand">Brand</a><a class="footer-link" href="/en/about/canada-basketball-foundation">Canada BasketballÂ Foundation<br/></a><a class="footer-link" href="/en/about/contact-us">Contact Us</a></div></div></div><div class="footer-section _3"><div class="footer-top-right"><a class="social" href="https://www.instagram.com/canadabasketball/" target="_blank">ï­</a><a class="social" href="https://www.facebook.com/CanadaBasketball/" target="_blank">ï</a><a class="social" href="https://twitter.com/CanBball" target="_blank">ï</a></div></div></div></div><div class="footer-ptso"><div class="container"><div class="footer-link partners">Proudly supporting our Provincial Sport Organization Partners</div><ul class="ptso-ul" role="list"><li class="ptso-li"><a class="psto-logo w-inline-block" href="http://www.basketball.bc.ca/" target="_blank"><img alt="" loading="lazy" sizes="(max-width: 479px) 100vw, 62.5px" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5ec41b946d54cb0d221ee225_basketball-bc.png" srcset="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5ec41b946d54cb0d221ee225_basketball-bc-p-500.png 500w, https://assets-global.website-files.com/5d24fc966ad064837947a33b/5ec41b946d54cb0d221ee225_basketball-bc-p-800.png 800w, https://assets-global.website-files.com/5d24fc966ad064837947a33b/5ec41b946d54cb0d221ee225_basketball-bc.png 1735w"/></a></li><li class="ptso-li"><a class="psto-logo w-inline-block" href="http://www.basketballalberta.ca/" target="_blank"><img alt="" loading="lazy" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5f90af6e35f79ffe7d835bdf_assocLogo.svg"/></a></li><li class="ptso-li"><a class="psto-logo sm w-inline-block" href="http://www.basketballsask.com/" target="_blank"><img alt="" loading="lazy" sizes="(max-width: 479px) 100vw, 40px" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5ec41ebbf74696c607706d66_basketball-sk.png" srcset="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5ec41ebbf74696c607706d66_basketball-sk-p-500.png 500w, https://assets-global.website-files.com/5d24fc966ad064837947a33b/5ec41ebbf74696c607706d66_basketball-sk.png 1200w"/></a></li><li class="ptso-li"><a class="psto-logo w-inline-block" href="http://www.basketballmanitoba.ca/" target="_blank"><img alt="" loading="lazy" sizes="(max-width: 479px) 100vw, 62.5px" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5ec31e635d56a448f0155382_basketball-mb.png" srcset="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5ec31e635d56a448f0155382_basketball-mb-p-500.png 500w, https://assets-global.website-files.com/5d24fc966ad064837947a33b/5ec31e635d56a448f0155382_basketball-mb.png 768w"/></a></li><li class="ptso-li"><a class="psto-logo w-inline-block" href="http://www.basketball.on.ca/" target="_blank"><img alt="" loading="lazy" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5ec31ebba10776f84d31e76f_basketball-on.png"/></a></li><li class="ptso-li"><a class="psto-logo w-inline-block" href="http://www.basketball.qc.ca/" target="_blank"><img alt="" loading="lazy" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5e0e749e77ff446af165ee78_basketball-qc.png"/></a></li><li class="ptso-li"><a class="psto-logo sm w-inline-block" href="http://www.bnwt.ca/" target="_blank"><img alt="" loading="lazy" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5e0e749ff72aff1df3454f20_basketball-nwt.png"/></a></li><li class="ptso-li"><a class="psto-logo w-inline-block" href="http://www.basketball.nb.ca/" target="_blank"><img alt="" loading="lazy" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5e0e75a8d4243b16cc2f5ec8_basketball-nb.png"/></a></li><li class="ptso-li"><a class="psto-logo sm w-inline-block" href="http://www.basketballnovascotia.com/" target="_blank"><img alt="" loading="lazy" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5f4981100d7ddc0ee0318539_basketball-ns.png"/></a></li><li class="ptso-li"><a class="psto-logo sm w-inline-block" href="http://www.basketballpei.ca/" target="_blank"><img alt="" loading="lazy" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5e0e749e77ff44440765ee79_basketball-pei.png"/></a></li><li class="ptso-li"><a class="psto-logo sm w-inline-block" href="http://www.nlba.nf.ca/" target="_blank"><img alt="" loading="lazy" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5e0e749df72aff3d14454f1e_basketball-nl.png"/></a></li><li class="ptso-li"><a class="psto-logo w-inline-block" href="http://www.basketballyukon.ca/" target="_blank"><img alt="" loading="lazy" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5ec31fee55c767ffc116072c_basketball-yk-blk.png"/></a></li><li class="ptso-li"><a class="psto-logo sm w-inline-block" href="http://www.wheelchairbasketball.ca/" target="_blank"><img alt="" loading="lazy" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5e0e749fd4243b1f8e2f59af_basketball-wheelchair.png"/></a></li></ul></div></div><div class="container"><div class="footer-top"><div class="footer-top-left"><img alt="" class="canada-flag" src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/5d6595d090a66cbef2762cc1_s.png"/><div class="small-subhead-white footer">Â©2020 CANADA BASKETBALL<br/></div></div></div></div></div></div><script crossorigin="anonymous" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=5d24fc966ad064837947a33b" type="text/javascript"></script><script src="https://assets-global.website-files.com/5d24fc966ad064837947a33b/js/canada-basketball.f3df02d4a.js" type="text/javascript"></script><!--[if lte IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]--><script>
  
<!-- HAMBURGER MENU - CLOSE BUTTON POSITIONING   -->  
window.setInterval(function(){
  if($(window).width() >= 992){
    if ($(".navbar, .navbar-dark").css('background-color') == 'rgb(186, 12, 47)') {
     $(".overlay-menu-container").css('padding-top', '100px');
    }
    else {
      $(".overlay-menu-container").css('padding-top', '154px');
    }
  }
}, 500);
  
  
$(document).ready(function(){
$('.icon-search').click(function(){
    $('input[type="search"]').focus();
});
});  
  
</script>
<script>
if(document.querySelector('.menu-en #language')){  
    var url = window.location;
    var str = url.pathname;
    var string = str.replace(/^\/[\w\d]+\//, '');
    var newURL = url.protocol + "//" + url.hostname + "/fr/" + string;  	
    var setLink= document.querySelectorAll('.menu-en #language');
  	setLink[0].setAttribute("href", newURL);
  	setLink[1].setAttribute("href", newURL);
}
  
if(document.querySelector('.menu-fr #language')){  
  var url = window.location;
  var str = url.pathname;
  var string = str.replace(/^\/[\w\d]+\//, '');
  var newURL = url.protocol + "//" + url.hostname + "/en/" + string;
  var setLink = document.querySelectorAll('.menu-fr #language');
  setLink[0].setAttribute("href", newURL);
  setLink[1].setAttribute("href", newURL);
}
  
if(document.querySelector('.home-fr-nav #language')){
  var url = window.location;
  var newURL = url.protocol + "//" + url.hostname + "/en/";
  var setLink = document.querySelectorAll('.home-fr-nav #language');
  setLink[0].setAttribute("href", newURL);
  setLink[1].setAttribute("href", newURL);
}
  
if(document.querySelector('.home-en-nav #language')){
  var url = window.location;
  var newURL = url.protocol + "//" + url.hostname + "/fr/";
  var setLink = document.querySelectorAll('.home-en-nav #language');
  setLink[0].setAttribute("href", newURL);
  setLink[1].setAttribute("href", newURL);
}
  
if(document.querySelector('.news-en-nav #language')){  
  var url = window.location;
  var str = url.pathname;
  var string = str.replace(/^\/[\w\d]+\//, '/news-fr/');
  var newURL = url.protocol + "//" + url.hostname + string;
  var setLink = document.querySelectorAll('.news-en-nav #language');
  setLink[0].setAttribute("href", newURL);
  setLink[1].setAttribute("href", newURL);
}

if(document.querySelector('.news-fr-nav #language')){  
  var url = window.location;
  var str = url.pathname;
  var string = str.replace(/^\/[\w-\d]+\//, '/news/');
  var newURL = url.protocol + "//" + url.hostname + string;
  var setLink = document.querySelectorAll('.news-fr-nav #language');
  setLink[0].setAttribute("href", newURL);
  setLink[1].setAttribute("href", newURL);
}
  
if(document.querySelector('.coaches-en-nav #language')){  
  var url = window.location;
  var str = url.pathname;
  var string = str.replace('/coaches/', '/coaches-fr/');
  var newURL = url.protocol + "//" + url.hostname + string;
  var setLink = document.querySelectorAll('.coaches-en-nav #language');
  setLink[0].setAttribute("href", newURL);
  setLink[1].setAttribute("href", newURL);
}
  
if(document.querySelector('.coaches-fr-nav #language')){  
  var url = window.location;
  var str = url.pathname;
  var string = str.replace('/coaches-fr/', '/coaches/');
  var newURL = url.protocol + "//" + url.hostname + string;
  var setLink = document.querySelectorAll('.coaches-fr-nav #language');
  setLink[0].setAttribute("href", newURL);
  setLink[1].setAttribute("href", newURL);
}
  
if(document.querySelector('.hof-en-nav #language')){  
  var url = window.location;
  var str = url.pathname;
  var string = str.replace('/hall-of-fame/', '/hall-of-fame-fr/');
  var newURL = url.protocol + "//" + url.hostname + string;
  var setLink = document.querySelectorAll('.hof-en-nav #language');
  setLink[0].setAttribute("href", newURL);
  setLink[1].setAttribute("href", newURL);
}
  
if(document.querySelector('.hof-fr-nav #language')){  
  var url = window.location;
  var str = url.pathname;
  var string = str.replace('/hall-of-fame-fr/', '/hall-of-fame/');
  var newURL = url.protocol + "//" + url.hostname + string;
  var setLink = document.querySelectorAll('.hof-fr-nav #language');
  setLink[0].setAttribute("href", newURL);
  setLink[1].setAttribute("href", newURL);
}
  
if(document.querySelector('.athlete-en-nav #language')){  
  var url = window.location;
  var str = url.pathname;
  var string = str.replace('/athlete/', '/athlete-fr/');
  var newURL = url.protocol + "//" + url.hostname + string;
  var setLink = document.querySelectorAll('.athlete-en-nav #language');
  setLink[0].setAttribute("href", newURL);
  setLink[1].setAttribute("href", newURL);
}
  
if(document.querySelector('.athlete-fr-nav #language')){  
  var url = window.location;
  var str = url.pathname;
  var string = str.replace('/athlete-fr/', '/athlete/');
  var newURL = url.protocol + "//" + url.hostname + string;
  var setLink = document.querySelectorAll('.athlete-fr-nav #language');
  setLink[0].setAttribute("href", newURL);
  setLink[1].setAttribute("href", newURL);
}

  
if(document.querySelector('.team-en-nav #language')){  
  var url = window.location;
  var str = url.pathname;
  var string = str.replace('/team-canada-en/', '/team-canada-fr/');
  var newURL = url.protocol + "//" + url.hostname + string;
  var setLink = document.querySelectorAll('.team-en-nav #language');
  setLink[0].setAttribute("href", newURL);
  setLink[1].setAttribute("href", newURL);
}
  
  
if(document.querySelector('.team-fr-nav #language')){  
  var url = window.location;
  var str = url.pathname;
  var string = str.replace('/team-canada-fr/', '/team-canada-en/');
  var newURL = url.protocol + "//" + url.hostname + string;
  var setLink = document.querySelectorAll('.team-fr-nav #language');
  setLink[0].setAttribute("href", newURL);
  setLink[1].setAttribute("href", newURL);
}
  
</script>
<script>
$(document).ready(function() {
	$('#go-back').click(function(e){
		e.preventDefault();
		// Go back 1 page 
		window.history.back();

 		// window.history.go(-1);
	});

});
</script></body></html>