<!DOCTYPE html>
<html>
<head>
<meta content="width=device-width" name="viewport"/>
<title>Affiliate Future</title>
<link href="../../favicon.ico.png" rel="shortcut icon" type="image/x-icon"/>
<link href="/content/css/af_styles.css" rel="stylesheet"/>
<!--[if IE 8]><link rel="stylesheet" type="text/css" media="screen" href="content/css/ie8.css"  /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" type="text/css" media="screen" href="content/css/ie9.css"  /><![endif]-->
<link href="/content/css/af_styles.css" rel="stylesheet"/>
<link href="/content/css/font-awesome.min.css" rel="stylesheet"/>
<script src="/Scripts/jquery-1.7.1.min.js"></script>
<script src="/Scripts/jquery.unobtrusive-ajax.js"></script>
<script src="/Scripts/jquery.validate.js"></script>
<script src="/Scripts/jquery.validate.unobtrusive.js"></script>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-31029215-1" type="text/javascript"></script>
<script type="text/javascript">
        window.dataLayer = window.dataLayer || [];
        function gtag()
    
        { dataLayer.push(arguments); }
        gtag('js', new Date());
    
        gtag('config', 'UA-31029215-1');
    </script>
<script>
        !function (e, n, t, r, o, i) {
            if (!n) {
                n = n || {}, window.permutive = n, n.q = [], n.config = i || {}, n.config.projectId = r, n.config.apiKey = o, n.config.environment = n.config.environment || "production";
                for (var c = ["addon", "identify", "track", "trigger", "query", "segment", "segments", "ready", "on", "once", "user"], a = 0; a < c.length; a++)
                {
                    var s = c[a]; n[s] = function (e) {
                        return function ()
                        {
                            var t = Array.prototype.slice.call(arguments, 0); n.q.push({ functionName: e, arguments: t })
                        }
                    }(s)
                }
                var p = window.Worker ? "async" : "blocking", g = e.createElement("script");
                g.type = "text/javascript", g.async = !0;
                var m = ("https:" == e.location.protocol ? "https://" : "http://") + "cdn.permutive.com";
                g.src = m + "/" + r + "-" + p + ".js";
                var u = e.getElementsByTagName(t)[0]; u.parentNode.insertBefore(g, u)
            }
        }
        (document, window.permutive, "script", "e82dc6a7-79a5-49b7-b1ed-a89a37f2fe8b", "62905692-4299-42af-951f-aa610546b2b1", {});
        permutive.addon("web", {});
    </script>
<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="/content/scripts/script.js"></script>
<meta content="Affiliate Future,affiliate network and tools,affiliate marketing,performance based marketing,marketing solution,affiliates,merchants,advertisres,publishers,Boost your online sales,effective marketing solution,low risk environment,business growth,online business,Publisher Network," itemprop="keywords" name="keywords"/>
<meta content="Affiliate Future provides advertisers with an effective marketing solution through its affiliate network and tools. AF delivers millions of transactions per month to hundreds of advertisers from SME’s to major brands. AF operates on a pay on performance basis, giving our advertisers a low risk environment to grow their online business with the ability to achieve an excellent ROI." name="description"/>
</head>
<body>
<div class="header">
<div class="core_wrapper">
<div class="logo">
<a href="/" title="Affiliate Future"></a>
</div>
<div class="accountdetails">
<p>
<span>£</span> <a href="http://blog.affiliatefuture.co.uk" target="_blank">Blog</a>
                    | <a href="https://affiliatefuture.freshdesk.com/support/home" target="_blank">Support Centre</a>
                    | <a href="/Contact" length="7">Contact Us</a>
</p>
<div class="login">
<script>
                        $(document).ready(function () {
                            $("a.login_button").click(function () {
                                $("div.login_panel").slideToggle();
                            });
                        });
                    </script>
<a class="login_button">Login</a>
<div class="login_panel" style="display: none">
<a class="c_advertisers" href="https://advertisers.affiliatefuture.com">Advertisers
                            &gt;</a> <a class="c_publishers" href="https://afuk-affiliate.affiliatefuture.com">Publishers
                                &gt;</a>
</div>
<a class="register" href="/register/publishers">Register</a>
</div>
</div>
</div>
</div>
<div class="interfacenav">
<div class="interfacenav core_wrapper" id="cssmenu">
<ul>
<li class="home"><a class="navactive" href="/">Home</a></li>
<li><a href="/Advertisers">Advertisers</a>
<ul>
<li><a href="/Advertisers">Why AF</a></li>
<li><a href="/Advertisers/what-we-do">What We Do</a></li>
<li><a href="/Advertisers/af-service-levels">AF Service Levels</a></li>
</ul>
</li>
<li><a href="/Publishers">Publishers</a>
<ul>
<li><a href="/Publishers">Why AF</a></li>
<li><a href="/Publishers/the-basics">The Basics</a></li>
</ul>
</li>
<li><a href="/Agencies">Agencies</a> </li>
<li><a href="/About">About AF</a> </li>
</ul>
</div>
</div>
<div class="clr">
</div>
<div id="body">
<section class="content-wrapper main-content clear-fix">
<!--carousel-->
<!--carousel-->
<link href="content/scripts/owlcarousel/assets/owl.carousel.css" rel="stylesheet" type="text/css"/>
<link href="content/scripts/owlcarousel/assets/owl.theme.css" rel="stylesheet" type="text/css"/>
<div class="owl-theme owl_homepage">
<div class="owl-carousel">
<div class="item" id="animate-home" style="background: url(content/images/slider_01.jpg)">
<div class="flick_wrap l_advertisers">
<div class="flick-title">
                    Advertisers</div>
<div class="flick-sub-text">
<span>Open up your market.</span><br/>
<a class="button color_three" href="advertisers/index">Find out more &gt;</a></div>
</div>
</div>
<div class="item" id="animate-home" style="background: url(content/images/slider_02.jpg)">
<div class="flick_wrap l_publishers">
<div class="flick-title">
                    Publishers</div>
<div class="flick-sub-text">
<span>Increase revenue from your website.</span><br/>
<a class="button color_five" href="publishers/index">Find out more &gt;</a></div>
</div>
</div>
<div class="item" id="animate-home" style="background: url(content/images/slider_03.jpg)">
<div class="flick_wrap l_agencies">
<div class="flick-title">
                    Agencies</div>
<div class="flick-sub-text">
<span>Grow sales &amp; increase ROI.</span><br/>
<a class="button color_two" href="agencies/index">Find out more &gt;</a></div>
</div>
</div>
</div>
</div>
<script src="content/scripts/owlcarousel/owl.carousel.js"></script>
<script>
    $(document).ready(function () {
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            loop: true,
            navigation: true, // Show next and prev buttons
            slideSpeed: 6000,
            paginationSpeed: 6000,
            autoplay: true,
            autoplayTimeout: 6000,
            autoplayHoverPause: true,
            items: 1,
            itemsDesktop: false,
            itemsDesktopSmall: false,
            itemsTablet: false,
            itemsMobile: false
        });
    })
</script>
<!--end carousel-->
<div class="content" id="mainbody">
<div class="core_wrapper">
<div class="container">
<div class="row featuretop">
<div class="col-thirds b1 advertiser_feature">
<img alt="advertiser" class="img-responsive" src="content/images/home_advertiser.png"/>
<h2 style="text-align: center">
                        Advertiser</h2>
<p>
                        Boost online sales by expanding your customer reach.</p>
<p>
<a class="button color_grey" href="advertisers/index">More &gt;</a> <a class="button color_three" href="register/advertisers">Apply Online &gt;</a></p>
</div>
<div class="col-thirds publisher_feature">
<img class="img-responsive" src="content/images/home_publisher.png"/>
<h2 style="text-align: center">
                        Publisher</h2>
<p>
                        Increase revenue for your website by accessing a breadth of brands alongside niche
                        retailers.</p>
<p>
<a class="button color_grey" href="publishers/index">More &gt;</a> <a class="button color_five" href="register/publishers">Sign Up Free &gt;</a></p>
</div>
<div class="col-thirds b3 agency_feature">
<img class="img-responsive" src="content/images/home_agency.png"/>
<h2 style="text-align: center">
                        Agency</h2>
<p>
                        Grow your client's online sales, whilst increasing the ROI.</p>
<p>
<a class="button color_grey" href="agencies/index">More &gt;</a> <a class="button color_two" href="contact/index">Contact Us &gt;</a></p>
</div>
</div>
</div>
</div>
<div class="row_spacer">
</div>
<span class="gradient"></span>
<div class="grey">
<div class="core_wrapper advantage">
<div class="img-fullwidth col_twothirds">
<img src="content/images/af_advantage.png"/></div>
<div class="col-thirds padd_top">
<h2>
                    The AF Advantage</h2>
<p>
                    With over 600 advertisers and 300,000 publishers our network has an unrivalled reach.</p>
<p>
                    At AF we pride ourselves on the personalised services we provide and our sector
                    specific account managers are highly trained to guide you all the way.
                </p>
<p>
<br/>
<a class="button color_white" href="about/index">Find out more &gt;</a></p>
</div>
<div class="clearfix">
</div>
</div>
</div>
<div class="core_wrapper">
<h2 class="title_row">
            What Our Clients Say</h2>
<div style="position: relative;">
<div class="container">
<div class="row about gridded">
<div class="col-differ">
<div class="about1 ImgCss">
<img alt="Topdeck" class="pic1Ab" src="content/images/logos/Topdeck.png" style="margin-top: 15px !important;width: 106px !important;left: 457px !important;"/>
<h3> Topdeck Travel </h3>
<p>
                                "Topdeck has been working with Affiliate Future since 2014 and has seen consistent growth 
								both in transactions and in new business opportunities every year. An easy to use platform 
								and impressive account management we've very much enjoyed the success of our partnership and 
								look forward to what the future years will bring in terms of continued growth and technology development."</p>
</div>
</div>
<div class="col-differ">
<div class="about2 ImgCss">
<img alt="JV" class="pic2Ab" src="content/images/logos/singaporeairlines.png"/>
<h3> Singapore Airlines </h3>
<p>
                                "Singapore Airlines saw its affiliate sales grow by over 100% in the first 6 months
                                of the programme going live on AF. We continue to see very positive results whilst
                                working closely with the AF team, who are very quick to address our queries and
                                provide support where necessary."</p>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row about gridded last_grid">
<div class="col-differ">
<div class="about1">
<img class="pic1Ab" src="content/images/logos/logobens.png"/>
<h3>
                                Bensons for Beds</h3>
<p>
                                "As the UK's number 1 bed retailer, Bensons for Beds have been working with AF since
                                2009. Our affiliate campaigns are now recognised as a key sales and performance
                                channel for the business. We have found the account management team to be second
                                to none, extremely professional &amp; responsive, delivering insight, guidance &amp; continued
                                success to the programme."</p>
</div>
</div>
<div class="col-differ">
<div class="about2 ImgCss">
<img alt="Trespass" class="pic2Ab" src="https://banners.affiliatefuture.com/logos/Trespass%20Logo%20120x60.png" style="width: 79px;margin-top: 10px;"/>
<h3> Trespass </h3>
<p>
								"We have worked with AF for over 15 months now, since joining our affiliate channel 
								has grown month on month with it now being one of our biggest channels for web. 
								We have received amazing support from our affiliate manager, who has helped us every step of the way."</p>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="clearfix">
</div>
</div>
<div class="grey">
<div class="core_wrapper">
<h2 class="title_row">
            What's New</h2>
<div id="news">
<div class="container">
<div class="row news">
<div class="col-thirds">
<img class="img-responsive" src="content/images/news_01.jpg"/>
<h2 style="text-align: center">
                            Enhanced Visual Reporting</h2>
<p>
                            AF continues to improve usability of our platform. As a result of advertiser and
                            publisher feedback, 2015 has seen the launch of the AF dashboard, allowing you to
                            view all top line information on one screen.</p>
</div>
<div class="col-thirds">
<img class="img-responsive" src="content/images/news_02.jpg"/>
<h2 style="text-align: center">
                            Expanding Travel Network</h2>
<p>
                            AF is excited to announce that Cathay Pacific have just exclusively launched on
                            our platform. AF has always had a strong offering in the travel sector and we are
                            looking to continue this growth in 2015, supported by our specialist travel account
                            management team.</p>
</div>
<div class="col-thirds">
<img class="img-responsive" src="content/images/af_fasterpayments.jpg"/>
<h2 style="text-align: center">
                            Faster Payments</h2>
<p>
                            Affiliate Future now pays publishers twice a month, in our ongoing commitment to
                            getting money to publishers quicker.</p>
</div>
<div class="clearfix">
</div>
<p>
<a class="button color_white" href="http://blog.affiliatefuture.co.uk" target="_blank">
                            More News From Our Blog &gt;</a> <a class="button color_twitter" href="https://twitter.com/AffFutureUK" target="_blank">Twitter <span class="fa fa-twitter"></span></a>
</p>
</div>
</div>
</div>
</div>
</div>
<div class="clr">
</div>
</section>
</div>
<div class="footer">
<span class="gradient"></span>
<div class="core_wrapper">
<ul>
<li><a class="footer_logo" href="/"></a></li>
<li class="contact_footer"><span></span><b>Contact Us</b> <a href="https://affiliatefuture.freshdesk.com/support/home" target="_blank">Support Centre</a><br/>
<a href="/registration/advertisers">New Advertisers</a><br/>
<a href="/main/contact-uk.asp">Urgent Enquiries</a>
</li>
<li><b>Keep Updated</b> <a class="fa fa-facebook-square" href="https://www.facebook.com/pages/AffiliateFuture_UK/1477135035871873" target="_blank"></a><a class="fa fa-twitter-square" href="https://twitter.com/AffFutureUK" target="_blank"></a><a class="fa fa-linkedin-square" href="https://www.linkedin.com/company/affiliatefuture" target="_blank"></a></li>
<li>©Affiliate Future. All rights reserved.<br/>
					Standard House, 12-13 Essex Street, London, WC2R 3AA

                    <br/>
<a href="/Legal/privacy">Privacy</a>| <a href="/Legal/terms">Terms</a>
</li>
</ul>
<div class="clr">
</div>
</div>
</div>
</body>
</html>
