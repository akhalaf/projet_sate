<html>
<head>
<title>AboutFilm.com - The Pianist (2002)</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<script language="JavaScript">
<!--

function new_window(url) {
link =
window.open(url,"Link","toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0,width=500,height=600,left=80,top=25");
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<meta content="Movie review of THE PIANIST (2002), starring Adrien Brody and Thomas Kretschmann. Directed by Roman Polanski." name="description"/>
<meta content="The Pianist, Roman Polanski, Adrien Brody, Thomas Kretschmann, Frank Finlay, Maureen Lipman, Emilia Fox, Ed Stoppard, Julia Rayner, Jessica Kate Meyer, Ronald Harwood, Wladyslaw Szpilman, Wojciech Kilar, Pawel Edelman, Robert Benmussa, Alain Sarde, movie review, drama, Holocaust, Carlo Cavagna" name="keywords"/>
</head>
<body bgcolor="#FFFFFF" onload="MM_preloadImages('../../graphics/redball.gif')">
<center>
</center>
<p>
</p><table border="0" cellpadding="4" width="100%">
<tr valign="bottom">
<td align="CENTER" colspan="3">
<p><font face="Arial" size="+3">The Pianist</font></p>
</td>
</tr>
<tr>
<td colspan="2"> </td>
</tr>
<tr>
<td colspan="2" valign="TOP"><img alt="The Pianist" height="226" src="pianist1.jpg" width="180"/></td>
<td bgcolor="#EBEBEB" valign="TOP" width="99%">
<p><b><font face="Arial" size="-1">UK/France/Germany/Poland/Netherlands, 2002. 
        Rated R. 148 minutes.</font></b> </p>
<p><b><font face="Arial" size="-1">Cast:</font></b><font face="Arial" size="-1"> 
        Adrien Brody, Thomas Kretschmann, Frank Finlay, Maureen Lipman, Emilia 
        Fox, Ed Stoppard, Julia Rayner, Jessica Kate Meyer<br/>
<b>Writer:</b> Ronald Harwood, based on the book by Wladyslaw Szpilman<br/>
<b>Music:</b> Wojciech Kilar<br/>
<b>Cinematographers:</b> Pawel Edelman, Roman Polanski (uncredited)<br/>
<b>Producers: </b>Roman Polanski, Robert Benmussa, Alain Sarde<br/>
<b>Director:</b> Roman Polanski </font>
</p><p><b><font face="Arial" size="-1"><a href="#links">LINKS</a></font></b>
</p></td>
</tr>
</table>
<table border="0" cellpadding="4" width="100%">
<tr>
<td nowrap=""><font face="Arial"><a href="javascript:new_window('../../ratings2.htm')" style="color:#0000CC;text-decoration:none">Grade:<font size="+1"> 
      A-</font></a></font></td>
<td align="RIGHT" nowrap=""><font face="Arial">Review by <font size="+1">Carlo Cavagna</font></font></td>
</tr>
</table>
<p align="center"><i><a href="../../features/pianist/interview.htm">Read the AboutFilm 
  interviews with Adrien Brody and Thomas Kretschmann. </a></i></p>
<p align="left"><img alt="R" height="47" src="../../graphics/letters/r.gif" width="45"/> oman 
  Polanski continues to be remembered as one of cinema's most accomplished directors 
  despite a career that has seemed directionless ever since his ignominious exile 
  from the United States in 1976. A couple films have found success (notably <i>Tess</i>), 
  while others, like <i>Pirates</i> and <i><a href="../n/ninthgate.htm">The Ninth 
  Gate</a></i>, have bombed. He has been a director searching for renewed focus, 
  and with the indirectly autobiographical Holocaust film <i>The Pianist</i>, 
  he has found it. </p>
<p>Polanski is himself a Holocaust survivor. Though born in Paris in 1933, he 
  grew up in Krakow, Poland, where he lived in the Jewish ghetto during the war. 
  When the Germans took his parents away (his mother died at Auschwitz and his 
  father survived), Polanski escaped through a hole in the ghetto's barbed wire 
  fence. Thereafter, he hid successfully with help from Catholic families, often 
  finding refuge in movie theaters. 
</p><p>Polanski has always known, he says, that he would make a movie about this era 
  of Polish history, but did not want it to be based on his own life. In Wladyslaw 
  Szpilman, he has found the perfect surrogate to communicate his own experiences. 
</p><p>Szpilman was a celebrated pianist and composer at the time of the German invasion. 
  Since 1935, he had been performing live on Polish radio. He was playing Chopin's 
  "Nocturne in C Sharp" on September 23rd, when the Luftwaffe destroyed the station. 
  Thereafter, Szpilman's life in Warsaw, detailed in his memoir, paralleled that 
  of Polanski in Krakow. Szpilman's family was relocated to the Jewish ghetto, 
  and when they were taken away, he eluded deportation with help from outsiders--including 
  from some unexpected sources--barely avoiding capture several times and living 
  constantly on the razor's edge. It is a story of both luck and endurance, both 
  of which were indispensable to survival.<img align="right" alt="Adrien Brody in THE PIANIST" height="170" hspace="12" src="pianist2.jpg" vspace="12" width="220"/>
</p><p><i>The Pianist</i> recounts Szpilman's astonishing story in careful, attentive 
  detail, but it is filled with particulars from Polanski's own life, from the 
  reality of life in the Jewish ghetto to the awkward position a woman's corpse 
  assumes after she is shot in the head while Szpilman observes from his hiding 
  place. Instead of relying primarily on digital effects, Polanski has made <i>The 
  Pianist</i> the old-fashioned way, constructing huge elaborate sets to recreate 
  wartime Warsaw, shaped not only by old documentaries, but by Polanski's own 
  memories. 
</p><p><i>The Pianist</i> is Polanski's most personal work to date, and perhaps because 
  of that, Polanski seems to have rediscovered the cinematic voice he had as a 
  young man. Polanski always had a remarkable talent for portraying psychological 
  deterioration and instability in a quiet, believably unexaggerated fashion while 
  at the same time maintaining a high level of suspense. <i>The Pianist</i> recalls 
  movies like <i>Repulsion</i> and <i>Rosemary's Baby</i>, particularly when the 
  protagonist is alone for long stretches of time. The sequence when the starving 
  Szpilman is locked helplessly in an apartment while an uprising rages outside 
  the window bears the director's unmistakable stamp. 
</p><p>Szpilman is played by Adrien Brody (<i>Summer of Sam, The Affair of the Necklace</i>), 
  whom Polanski specifically sought out after a broad casting call for an actor 
  with no experience failed to yield any candidates. To prepare for the six-month 
  shoot, Brody spent six weeks starving himself and intensively studying the piano 
  (with which he was already somewhat familiar) in order to play along with the 
  soundtrack without cutting away. Shooting began with the critical final scenes, 
  in which Szpilman encounters a German officer with unknown motives played commandingly 
  by Thomas Kretschmann (<i><a href="../u/u571.htm">U-571</a>, Stalingrad</i>), 
  whose critical role earned him second billing despite a relative lack of screen 
  time. Shooting then continued in reverse chronological order, because it's easier 
  to put weight back on and trim a beard gradually than to do the reverse. Knowing 
  this makes Brody's transformation from debonair musician to something resembling 
  Tom Hanks in the latter half of <i><a href="../c/castaway.htm">Cast Away</a></i> 
  all the more remarkable. 
</p><p>The Holocaust is a period of history well-mined by filmmakers, and that may 
  be off-putting to some moviegoers. But <i>The Pianist</i> doesn't try to be 
  a movie with important things to say, going out of its way to examine questions 
  of ethics and choices. A film that focuses unwaveringly on its protagonist, 
  <i>The Pianist</i> only records events, often dispassionately. The often heartbreaking 
  details (look for the scene where the Szpilman family shares a piece of caramel 
  in their last moments together) accumulate into a narrative, and the narrative 
  becomes a testament to human capacity for both good and evil, weakness and strength. 
  By retelling specific experiences and not seeking to be a movie "about" the 
  whole Holocaust, <i>The Pianist</i> is one of the best Holocaust movies ever 
  made. 
</p><p align="center"><b><font face="Arial, Helvetica, sans-serif"><font face="Arial" size="-2">Review 
  © <b>December 2002</b> by AboutFilm.Com and the author.</font></font></b><font face="Arial" size="-2"><br/>
<b>Images © 2002 Focus Features. All Rights Reserved.</b></font> </p>
<p>
</p><hr size="1" width="80%"/>
<table border="0" width="100%">
<tr>
<td align="left" nowrap="" valign="middle"><font size="-1"><b><font face="Arial"> <a href="http://www.aboutfilm.com/cgi-bin/boards/show.cgi?33/39" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image15','','../../graphics/redball.gif',1)" style="color:darkblue;text-decoration:none" target="_parent"><img align="absmiddle" border="0" height="10" name="Image15" src="../../graphics/greenball.gif" width="10"/> Comment 
      on this review on the boards</a></font></b></font></td>
<td align="right" rowspan="5" valign="bottom" width="623"> </td>
<td align="right" rowspan="5" valign="bottom" width="152">
<p><a href="../../index.htm" target="_parent"><img border="0" height="55" src="../../graphics/home1.gif" width="150"/></a><a href="../../index.htm" target="_parent"><img border="0" height="24" src="../../graphics/home2.gif" width="150"/></a><b><font face="Arial" size="-1"></font></b></p>
</td>
</tr>
<tr>
<td align="left" height="17" nowrap="" valign="middle"><font size="-1"><b><font face="Arial"> <a href="http://www.thepianist-themovie.com/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image14','','../../graphics/redball.gif',1)" style="color:darkblue;text-decoration:none" target="_blank"><img align="absmiddle" border="0" height="10" name="Image14" src="../../graphics/greenball.gif" width="10"/> Official 
      site</a></font></b></font></td>
</tr>
<tr>
<td align="left" nowrap="" valign="middle"><font size="-1"><b><font face="Arial"> <a href="http://us.imdb.com/Title?0253474" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image17','','../../graphics/redball.gif',1)" style="color:darkblue;text-decoration:none" target="_blank"><img align="absmiddle" border="0" height="10" name="Image17" src="../../graphics/greenball.gif" width="10"/> IMDB 
      page</a></font></b></font></td>
</tr>
<tr>
<td align="left" nowrap="" valign="middle"><b><font face="Arial" size="-1"> <a href="http://www.mrqe.com/lookup?%5EPianist,+The+(2002)" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image16','','../../graphics/redball.gif',1)" style="color:darkblue;text-decoration:none" target="_blank"><img align="absmiddle" border="0" height="10" name="Image16" src="../../graphics/greenball.gif" width="10"/> MRQE 
      page</a> </font></b></td>
</tr>
<tr>
<td align="left" nowrap="" valign="middle"><b><font face="Arial" size="-1"> <a href="http://www.rottentomatoes.com/m/ThePianist-1116005/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image52','','../../graphics/redball.gif',1)" style="color:darkblue;text-decoration:none" target="_blank"><img align="absmiddle" border="0" height="9" name="Image52" src="../../graphics/greenball.gif" width="10"/> Rotten 
      Tomatoes page</a></font></b></td>
</tr>
</table>
</body>
</html>
