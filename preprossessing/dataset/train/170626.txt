<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "93474",
      cRay: "610f34adeac503a1",
      cHash: "b36c168de23a564",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXVudGp1ZHlzLmNvbS8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "oTM/1jpLU6Vy6i3gaYAfDNfnQkJgmfLlCn1HUT+wN+TqG/9YL8Ao5jvjqc3aBG6QpDKhUnV+H8W3qZHkpBaudrFOjSRTNe7LjqkAcsvhgHU2lJh0kKcKqQS+x+hLiRuQwgaho4CuLfvIpz2/vTcG1pqhisIMo8X7h9oMPb2af2gjctfG2z2MJNHQSN2Tk0Z0cK2fQf5cY0pn+FqwzdE8+jTKB6TYDsR83BVcxQV8w6CGRH3LpfVTBUz7jN0awO5pWqeDo0qNtY1xp98Nf3JT91v5ek3tCJNcNHUG5zyOLUG4g3qREvcLHW/DQuim/sm9UaJYgdACJWbkRZ18FsIcZHIeMqBgxfBxZ96gVblRU+d/hZfh1nKdewn77b/8LOV3BPE0Vne95S/qPPZ8WH+N1W7wmuVS5eVt8d66bYSE/3zQcSCLUkET/ZK6eXgB8kMxtk2F0szgu9ikcfGsNolZZmNtxo5NojJdxR3fgB3v6z6lzYsaU11EPxKZQpjjRamz75aLkKRpaR5Qdd0cFCES1fvLe8nR0JJzuG97MxSiQWXY1yQ8CR+MDMFN4qrHWRnIvE7dRl/pqjMcMI2PB/54sSWw6YuwaX7A8/+OYt4l27fr2C2w51XL1glTvdXHeErEuvSLZ/BiTx9zmDv0vVCPFSazvjYwRjGm9AXQz60UuaEQtfMeLsXLzrW3Vhg4DrZYk7macYkizQwZkiBbUMexvdhAenkOf1EpxNvkKEdpVAq8m//j079E86Ec9X8hSnFE",
        t: "MTYxMDU0MTgzNi40NzMwMDA=",
        m: "LlJx7pWxoD92M/ARp79FUf0QuLj7+WiUN9YSLfy71dc=",
        i1: "FrXfKoF9gSYjRVDVsnD0DQ==",
        i2: "uv081U9cfqP8MffAeDcDPg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "qjT8T7RqNeip4WV/6DvY2RLR6C0ZhZvjdMx/Megvw8Q=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.auntjudys.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=d8bdc79160bc8ab8863585e71e591df0e30ca62c-1610541836-0-AavWZVlr1A0wHleh0Kz7lcKi5dkSeHT20LT8u3vZHdaWcpL_S6_i9e9_DuqCWFB88zTyCSlKVG61dpBTeR6Nzd0dYqId5FkBKsks05xyHsJk6fcbK4RVnWh6DrZYXk4XiPy2HdxC4GJkF8MMAd4ThqXE7SRycE09WjZrDBIVDlrJNSS-Dpl9XEmX4kZzJK7A2rbaSew__xWWZVFGVkobwOOLxollUO6mJpAXsZidgtggKI-TOLNMCnaOu0aHcYWc-7_2IUkZLA3iUBqHW1uJJlbqmmaoT20EooAGqqpu_s5zOacOqF7PCvmimCB9wBKbrTq_diF8ETIaWzkLX7c6Fml8nDvw0wID1fnx2Rkh6ho_XbFwpUyX6VvCgxC1HuKfFtIowWyiqH2D8rNRGqnDs7QC2gAS4WEJ3Iodu5pIwiEss4YBndP8nufLlTQKUI6PEItXsL7bh5595GKa-Lt-5hT_NQDJTP76yun13ptULgPMTrVa1jdqvAaIIPS7p-i0SEVL17KzocJoxEgv8-KiG-w" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="df08b19b4cd616f1be80ef8c2fbebd66cc4efa76-1610541836-0-AeH+Z9UEydxP7NomyJDzB7OEl8HrP3dQSNGfFcyKCg+Knw372MZTOqcXjsCEnt4L84UgI8seZD9Z7CzYkccv/sAbvxOTBgNhreZHDf65MKbWQ/XzuBrttBiyGAA/oVefTcYmv65m+/TV19EbUP+mwityjVJo/JYoobiVi/r4BWUidAvnfOknXuPSJgXibCCXxcCXkyb8XQZMn79wNZZQIVyC9VCq1bIaUAPQNF/Y3nHdDMSyrqljXQGLGSs5/92f5qCnvR3Wy4JOXS5t35bYUMYisl9NCOPLLbR74rn2zCsme5zPByypbfiQUcXxq/kM5H8f5qisIIcWPcuFqqlTxh5Nur+kkegZK7mE+qnZMCkNmi3nctsTS6sSCcgEK8LUYC8oZBoHIs3kpMyhSldnx7HPTTMJACxrI0ncQe1d+0Ryy+kAndxcjYQHSibbhODeIGkduHn/TapLdcmhxa2XL2kPxyTtZkrU7vP+cCEWR6aFHyweURhYhc/izRNB2M+tPKbGleArhOUGQTwTVoPI3o2XGTO7GnXh3sYmErD/zxB/177GcE+LglkhPBRYZRPDtvF2rfu2fjxXMu/IYPHg8nIpi+xdiMcovBWzK8kS1vuuqdM32kVm52+2QaoZgx+L6xdCZkW+uADwnjJ7upbaI/pHMITmFpxnzyvI5jL7NBLmj+nK+MlNTkjWxS3hZeBJEyg+LBwzzyO4Grrpn+UDPXrDf7XgPh9HSuXs/ThdCwFJLlvSGwAn2CspZubyZvTklwL9DrNtH0ECZbTPUBaxolllTrfzrnmBuTueAQnwkDx+OYtJqCSVTRhHPRTGg4ZysRT3XM8HpXdzkHkvYUKPUJAhZbcZRdaZ6f0z6iZjzovCZGHhDpJwo8AmsLmJYKi0mGlONV5L9oz8To328eNqNp+VrWt0S7GusrKueoMPr0gkFQvKSbs8isrTmbOF8XOgzGbv7SuLLxwyrff/6XnHmer8OQMBzSG4jsDksMz0gLwB9PGcX9vunOs9T0ZLq4Kxd5D1e452jCg0vct9CsZIXxATy1HoURKTKh6DOpl/ShwIXxFb/6eADIECLikMfxX9382f3lsBnwKt0gQ6v7KFYxqtnmvgVX4d3v4aWFqhtC5EfCVsoOP+vRQJt905V3m29JUtJ/88sr3ncj+xixmQkBiDCLA4wJJ0tnHVyy2EqEoTiMFW3cEw3WUaGqO47SpfO2XGKPSFKZEZ0BgSYFS5smhBkZMcizA6+l2cxswlT2fpjk/zcXqFqUYDBXi7ZwKlvA5zyHPWXx8KKsVjrsOuL/H1WxsoqJPYbRxaiM8y90Hz"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="4f4715d75fe28b059fefecc63373822d"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610f34adeac503a1')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<div style="position: absolute; top: -250px; left: -250px;"><a href="https://beatlemail.net/picture.php?answer=0">table</a></div>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610f34adeac503a1</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
