<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta content="uH7uzx1fqic9zu1kUGHIkN002U1Srm0lBfwfoxP16cg" name="google-site-verification"/>
<meta content="width=device-width,initial-scale=1.0" name="viewport"/>
<title>Dalston Hall Holiday Park and Golf Club</title>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<link href="https://www.dalstonhallholidaypark.co.uk/wp-content/themes/dalston/style.css" rel="stylesheet" type="text/css"/>
<!-- This site is optimized with the Yoast SEO plugin v15.6.2 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Dalston Hall Holiday Park and Golf Club</title>
<meta content="Family run park near Lake District &amp; Carlisle, Cumbria. Static Caravans and Luxury Lodges For Sale.  Adult Only Seasonal Tourer Pitches Available. Golf Course and Fishing. Fully Serviced Touring Hook Ups." name="description"/>
<meta content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" name="robots"/>
<link href="https://www.dalstonhallholidaypark.co.uk/" rel="canonical"/>
<meta content="en_US" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="Dalston Hall Holiday Park and Golf Club" property="og:title"/>
<meta content="Family run park near Lake District &amp; Carlisle, Cumbria. Static Caravans and Luxury Lodges For Sale.  Adult Only Seasonal Tourer Pitches Available. Golf Course and Fishing. Fully Serviced Touring Hook Ups." property="og:description"/>
<meta content="https://www.dalstonhallholidaypark.co.uk/" property="og:url"/>
<meta content="Dalston Hall Holiday Park" property="og:site_name"/>
<meta content="2020-12-31T10:39:29+00:00" property="article:modified_time"/>
<meta content="summary" name="twitter:card"/>
<meta content="Est. reading time" name="twitter:label1"/>
<meta content="1 minute" name="twitter:data1"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://www.dalstonhallholidaypark.co.uk/#website","url":"https://www.dalstonhallholidaypark.co.uk/","name":"Dalston Hall Holiday Park","description":"Dalston Hall Holiday Park","potentialAction":[{"@type":"SearchAction","target":"https://www.dalstonhallholidaypark.co.uk/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-US"},{"@type":"WebPage","@id":"https://www.dalstonhallholidaypark.co.uk/#webpage","url":"https://www.dalstonhallholidaypark.co.uk/","name":"Dalston Hall Holiday Park and Golf Club","isPartOf":{"@id":"https://www.dalstonhallholidaypark.co.uk/#website"},"datePublished":"2016-04-14T10:16:27+00:00","dateModified":"2020-12-31T10:39:29+00:00","description":"Family run park near Lake District & Carlisle, Cumbria. Static Caravans and Luxury Lodges For Sale.\u00a0 Adult Only Seasonal Tourer Pitches Available. Golf Course and Fishing. Fully Serviced Touring Hook Ups.","inLanguage":"en-US","potentialAction":[{"@type":"ReadAction","target":["https://www.dalstonhallholidaypark.co.uk/"]}]}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//ajax.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.dalstonhallholidaypark.co.uk/feed/" rel="alternate" title="Dalston Hall Holiday Park » Feed" type="application/rss+xml"/>
<link href="https://www.dalstonhallholidaypark.co.uk/comments/feed/" rel="alternate" title="Dalston Hall Holiday Park » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.dalstonhallholidaypark.co.uk\/wp-includes\/js\/wp-emoji-release.min.js?ver=defb4289a6bb44519d25299f94462a5f"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css?ver=defb4289a6bb44519d25299f94462a5f" id="jquery-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.dalstonhallholidaypark.co.uk/wp-includes/css/dist/block-library/style.min.css?ver=defb4289a6bb44519d25299f94462a5f" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.dalstonhallholidaypark.co.uk/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css?ver=1.11.4" id="jquery-ui-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.dalstonhallholidaypark.co.uk/wp-content/plugins/contact-form-7-datepicker/js/jquery-ui-timepicker/jquery-ui-timepicker-addon.min.css?ver=defb4289a6bb44519d25299f94462a5f" id="jquery-ui-timepicker-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://www.dalstonhallholidaypark.co.uk/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://www.dalstonhallholidaypark.co.uk/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="custom-script-js" src="https://www.dalstonhallholidaypark.co.uk/wp-content/themes/dalston/js/jquery.flexslider.js?ver=defb4289a6bb44519d25299f94462a5f" type="text/javascript"></script>
<script id="custom-script2-js" src="https://www.dalstonhallholidaypark.co.uk/wp-content/themes/dalston/js/jquery.sidr.min.js?ver=defb4289a6bb44519d25299f94462a5f" type="text/javascript"></script>
<link href="https://www.dalstonhallholidaypark.co.uk/wp-json/" rel="https://api.w.org/"/><link href="https://www.dalstonhallholidaypark.co.uk/wp-json/wp/v2/pages/5" rel="alternate" type="application/json"/><link href="https://www.dalstonhallholidaypark.co.uk/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.dalstonhallholidaypark.co.uk/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<link href="https://www.dalstonhallholidaypark.co.uk/" rel="shortlink"/>
<link href="https://www.dalstonhallholidaypark.co.uk/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.dalstonhallholidaypark.co.uk%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://www.dalstonhallholidaypark.co.uk/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.dalstonhallholidaypark.co.uk%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		if (window.wfLogHumanRan) { return; }
		window.wfLogHumanRan = true;
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//www.dalstonhallholidaypark.co.uk/?wordfence_lh=1&hid=596A0804099954B04C737E0BE83ADD06');
</script><style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style> <link href="https://www.dalstonhallholidaypark.co.uk/feed/" rel="alternate" title="Dalston Hall Holiday Park latest posts" type="application/rss+xml"/>
<link href="https://www.dalstonhallholidaypark.co.uk/comments/feed/" rel="alternate" title="Dalston Hall Holiday Park latest comments" type="application/rss+xml"/>
<link href="https://www.dalstonhallholidaypark.co.uk/xmlrpc.php" rel="pingback"/>
<meta content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" name="SKYPE_TOOLBAR"/>
<!--[if IE]>
<style type="text/css">
 
 .phone{font-size: 1em;font-family:Arial;}
</style>
<![endif]-->
</head>
<body class="wordpress y2021 m01 d14 h13 home page pageid-5 page-author-dhhp page-template page-template-page-home-php">
<div class="hfeed" id="wrapper">
<div id="header">
<div class="in padt20">
<div class="nudge clear">
<div class="col-1-5 fwi">
<p class="email"><a href="mailto:info@dalstonholidaypark.com">Email us</a></p>
</div>
<div class="col-3-5 fwi">
<a href="https://www.dalstonhallholidaypark.co.uk"><img alt="Dalston Hall Holiday Park" src="https://www.dalstonhallholidaypark.co.uk/wp-content/themes/dalston/images/dhhp-logo.png" title="Dalston Hall Holiday Park"/></a>
</div>
<div class="col-1-5 fwi">
<p class="phone">01228 710165</p>
</div>
</div>
</div>
</div><!--  #header -->
<div id="mainmenu">
<div class="in">
<div id="forsidr">
<div id="menu">
<div class="menu-main-menu-container"><ul class="menu" id="menu-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-69" id="menu-item-69"><a aria-current="page" href="https://www.dalstonhallholidaypark.co.uk/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-66" id="menu-item-66"><a href="https://www.dalstonhallholidaypark.co.uk/static-holiday-homes/">Static Holiday Homes</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73" id="menu-item-73"><a href="https://www.dalstonhallholidaypark.co.uk/static-holiday-homes/for-sale/">Caravans For Sale</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-65" id="menu-item-65"><a href="https://www.dalstonhallholidaypark.co.uk/lodges/">Lodge Ownership</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71" id="menu-item-71"><a href="https://www.dalstonhallholidaypark.co.uk/lodges/for-sale/">Lodges For Sale</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-67" id="menu-item-67"><a href="https://www.dalstonhallholidaypark.co.uk/touring-motorhomes/">Tourers &amp; Motorhomes</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-166" id="menu-item-166"><a href="https://www.dalstonhallholidaypark.co.uk/touring-motorhomes/booking-form/">Booking Form</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1692" id="menu-item-1692"><a href="https://www.dalstonhallholidaypark.co.uk/seasonal-tourers/">Seasonal Tourers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63" id="menu-item-63"><a href="https://www.dalstonhallholidaypark.co.uk/contact-us/">Contact</a></li>
</ul></div>
</div>
</div>
<div id="mobtrigger"><p><i class="fa fa-bars"></i> Menu</p></div>
</div>
</div>
<div class="in padlr20">
<div class="flexslider" id="top">
<ul class="slides">
<li><img alt="" class="attachment-slide size-slide wp-post-image" height="410" loading="lazy" src="https://www.dalstonhallholidaypark.co.uk/wp-content/uploads/2018/04/slide-980x410.jpg" width="980"/></li>
<li><img alt="static caravan for sale cumbria" class="attachment-slide size-slide wp-post-image" height="410" loading="lazy" sizes="(max-width: 980px) 100vw, 980px" src="https://www.dalstonhallholidaypark.co.uk/wp-content/uploads/2016/04/camping-park-980x410.jpg" srcset="https://www.dalstonhallholidaypark.co.uk/wp-content/uploads/2016/04/camping-park-980x410.jpg 980w, https://www.dalstonhallholidaypark.co.uk/wp-content/uploads/2016/04/camping-park-300x126.jpg 300w" width="980"/></li>
<li><img alt="Seasonal Touring Pitch" class="attachment-slide size-slide wp-post-image" height="410" loading="lazy" src="https://www.dalstonhallholidaypark.co.uk/wp-content/uploads/2019/05/f-980x410.jpg" width="980"/></li>
<li><img alt="" class="attachment-slide size-slide wp-post-image" height="410" loading="lazy" src="https://www.dalstonhallholidaypark.co.uk/wp-content/uploads/2016/05/golf-course-980x410.png" width="980"/></li>
<li><img alt="New Lodges For Sale" class="attachment-slide size-slide wp-post-image" height="410" loading="lazy" src="https://www.dalstonhallholidaypark.co.uk/wp-content/uploads/2016/04/z1-980x410.jpg" width="980"/></li>
<li><img alt="static caravan" class="attachment-slide size-slide wp-post-image" height="410" loading="lazy" src="https://www.dalstonhallholidaypark.co.uk/wp-content/uploads/2016/04/static-caravans-carlisle-980x410.jpg" width="980"/></li>
</ul>
</div>
</div>
<div class="in pad20" id="container">
<div id="content">
<div class="hentry p1 page publish author-dhhp untagged y2016 m04 d14 h10" id="post-5">
<div class="entry-content">
<h4 style="text-align: center;"><strong>UPDATE 31/12/2020: </strong></h4>
<h4 style="text-align: center;"><strong>Following Government’s announcement Cumbria is now in Tier 4. As a result the park is now closed until further notice. </strong><strong>Please do not travel to the park for any reason.</strong></h4>
<h4 style="text-align: center;"><strong>Reception and sales office will be open for all enquiries.</strong></h4>
<hr/>
<h1 style="text-align: center;">Welcome to Dalston Hall Holiday Park</h1>
<p> </p>
<hr/>
<h3 style="text-align: center;">Ideal location for visiting the Northern Lake District Cumbria and historic Carlisle.</h3>
<p> </p>
<p style="text-align: center;"><strong>Offering Static Holiday Homes and Lodges for sale, also Seasonal Touring Pitches and Touring/Motorhome stays. Plus a Golf Course and Fishing.</strong></p>
<hr/>
<p style="text-align: center;">The holiday park is set in 90 acres of secluded and peaceful Cumbrian countryside, with easy access to Dalston village and historic Carlisle. We are pet friendly and have 40 acres of specially designated footpaths and woodland areas to walk in, as well as a lovely riverside walk accessed directly off the park.</p>
<p style="text-align: center;">Our location is ideal for those wanting a quiet weekend retreat but with the benefit of local shops and amenities close by; plus the option of visiting all the attractions the Lake District and Borders have to offer with hotspots such as Keswick, Cockermouth, Ullswater all easily accessible.</p>
<p style="text-align: center;">For more information email <a href="mailto:info@dalstonholidaypark.com">info@dalstonholidaypark.com</a> or call 01228 710165.</p>
<p style="text-align: center;"></p><div class="nudge clear features"><div class="col-1-3 fwm feature feature1"><div class="inside">
<a href="https://www.dalstonhallholidaypark.co.uk/touring-motorhomes/"><img alt="" class="attachment-feature size-feature wp-post-image" height="210" loading="lazy" src="https://www.dalstonhallholidaypark.co.uk/wp-content/uploads/2016/04/camping-touring-caravans-dalston-320x210.jpg" width="320"/></a>
<div class="feature_title"><a href="https://www.dalstonhallholidaypark.co.uk/touring-motorhomes/">Tourers &amp; Motorhomes</a></div></div>
</div><div class="col-1-3 fwm feature feature2"><div class="inside">
<a href="https://www.dalstonhallholidaypark.co.uk/static-holiday-homes/"><img alt="Views around the park" class="attachment-feature size-feature wp-post-image" height="210" loading="lazy" src="https://www.dalstonhallholidaypark.co.uk/wp-content/uploads/2016/04/h-320x210.jpg" width="320"/></a>
<div class="feature_title"><a href="https://www.dalstonhallholidaypark.co.uk/static-holiday-homes/">New &amp; Used Static Caravans</a></div></div>
</div><div class="col-1-3 fwm feature feature3"><div class="inside">
<a href="https://www.dalstonhallholidaypark.co.uk/lodges/"><img alt="New Lodges For Sale" class="attachment-feature size-feature wp-post-image" height="210" loading="lazy" src="https://www.dalstonhallholidaypark.co.uk/wp-content/uploads/2016/04/z1-320x210.jpg" width="320"/></a>
<div class="feature_title"><a href="https://www.dalstonhallholidaypark.co.uk/lodges/">Luxury Lodges</a></div></div>
</div></div>
<p style="text-align: center;">
</p></div>
</div><!-- .post -->
</div><!-- #content -->
</div><!-- #container -->
<div id="footer">
<div class="in pad20">
<div class="nudge clear">
<div class="col-1-4 hwi">
Dalston Hall Holiday Park<br/> Dalston<br/> Carlisle<br/> Cumbria<br/> CA5 7JX<br/>
t: 01228 710165<br/>
e: <a href="mailto:info@dalstonholidaypark.com">info@dalstonholidaypark.com</a>
</div>
<div class="col-1-4 hwi">
<div class="menu-footer-1-container"><ul class="menu" id="menu-footer-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-58" id="menu-item-58"><a aria-current="page" href="https://www.dalstonhallholidaypark.co.uk/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56" id="menu-item-56"><a href="https://www.dalstonhallholidaypark.co.uk/touring-motorhomes/">Touring &amp; Motorhomes</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55" id="menu-item-55"><a href="https://www.dalstonhallholidaypark.co.uk/static-holiday-homes/">Static Holiday Homes</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-54" id="menu-item-54"><a href="https://www.dalstonhallholidaypark.co.uk/lodges/">Lodges</a></li>
</ul></div> </div>
<div class="col-1-4 hwi">
<div class="menu-footer-2-container"><ul class="menu" id="menu-footer-2"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59" id="menu-item-59"><a href="https://www.dalstonhallholidaypark.co.uk/contact-us/">Contact Us</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-950" id="menu-item-950"><a href="http://www.dalstonhallholidaypark.co.uk/wp-content/uploads/2018/03/Privacy-and-Cookie-Policy.pdf">Privacy and Cookie Policy</a></li>
</ul></div> </div>
<div class="col-1-4 hwi socials">
<a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-instagram"></i></a> <p><br/><a href="http://www.netconnexions.co.uk/" target="_blank">Web Design Cumbria</a></p>
</div>
</div>
</div>
</div><!-- #footer -->
</div><!-- #wrapper .hfeed -->
<script id="jquery-ui-core-js" src="https://www.dalstonhallholidaypark.co.uk/wp-includes/js/jquery/ui/core.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="jquery-ui-datepicker-js" src="https://www.dalstonhallholidaypark.co.uk/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="jquery-ui-datepicker-js-after" type="text/javascript">
jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Close","currentText":"Today","monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Previous","dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"dateFormat":"MM d, yy","firstDay":1,"isRTL":false});});
</script>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.dalstonhallholidaypark.co.uk\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://www.dalstonhallholidaypark.co.uk/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="jquery-ui-timepicker-js" src="https://www.dalstonhallholidaypark.co.uk/wp-content/plugins/contact-form-7-datepicker/js/jquery-ui-timepicker/jquery-ui-timepicker-addon.min.js?ver=defb4289a6bb44519d25299f94462a5f" type="text/javascript"></script>
<script id="jquery-ui-mouse-js" src="https://www.dalstonhallholidaypark.co.uk/wp-includes/js/jquery/ui/mouse.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="jquery-ui-slider-js" src="https://www.dalstonhallholidaypark.co.uk/wp-includes/js/jquery/ui/slider.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="jquery-ui-controlgroup-js" src="https://www.dalstonhallholidaypark.co.uk/wp-includes/js/jquery/ui/controlgroup.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="jquery-ui-checkboxradio-js" src="https://www.dalstonhallholidaypark.co.uk/wp-includes/js/jquery/ui/checkboxradio.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="jquery-ui-button-js" src="https://www.dalstonhallholidaypark.co.uk/wp-includes/js/jquery/ui/button.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="jquery-ui-slider-access-js" src="https://www.dalstonhallholidaypark.co.uk/wp-content/plugins/contact-form-7-datepicker/js/jquery-ui-sliderAccess.js?ver=defb4289a6bb44519d25299f94462a5f" type="text/javascript"></script>
<script id="custom-script3-js" src="https://www.dalstonhallholidaypark.co.uk/wp-content/themes/dalston/js/lightbox/js/lightbox.js?ver=defb4289a6bb44519d25299f94462a5f" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.dalstonhallholidaypark.co.uk/wp-includes/js/wp-embed.min.js?ver=defb4289a6bb44519d25299f94462a5f" type="text/javascript"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-77331722-1', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript">

     jQuery('#mobtrigger').sidr({
                name: 'sidr',
                source: '#forsidr'
            });

  
  jQuery(document).ready(function(){
jQuery("body").on("click",function(e) { jQuery.sidr('close'); }); 
});



jQuery(window).load(function() {


  jQuery('#top').flexslider({
    animation: "slide",
    controlNav:false
  });

jQuery('#pagebanner').flexslider({
    animation: "slide",
    controlNav:false
  });

jQuery('#salesc').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 210,
    itemMargin: 5,
    asNavFor: '#sales'
  });
 
  jQuery('#sales').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#salesc"
  });


});

jQuery(document).ready(function() {
    jQuery('.MyDate').datepicker({
        dateFormat : 'yy-mm-dd'
    });
});

	
</script>
</body>
</html>
