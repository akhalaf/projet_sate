<!DOCTYPE HTML>
<html itemscope="" itemtype="https://schema.org/WebPage" lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="GUITAR TABS &amp; CHORDS | AZ Chords.com" property="og:title"/>
<meta content="AZchords.com - Guitar tabs and chords. Bass, drum, ukulele tablatures. Guitar lessons" property="og:description"/>
<meta content="https://www.azchords.com/logo.gif" property="og:image"/>
<meta content="https://www.azchords.com/" property="og:url"/>
<meta content="133000596876438" property="fb:app_id"/>
<meta content="website" property="og:type"/>
<link href="https://www.azchords.com/" rel="canonical"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/style-2e4f9656.css?1579529462" rel="stylesheet" type="text/css"/>
<script src="/themes/front/js/clipboard.js" type="text/javascript"></script>
<script type="text/javascript">
/*<![CDATA[*/
        var tab = {
                artist_name: "",
                song_name: ""
        }  
/*]]>*/
</script>
<title>GUITAR TABS &amp; CHORDS | AZ Chords.com</title><meta content="AZchords.com - Guitar tabs and chords. Bass, drum, ukulele tablatures. Guitar lessons" name="description"/>
<meta content="tablatures,chords,tabs,guitar,artist,music,bass,drum, guitar pro" name="keywords"/>
<meta content="summary" property="twitter:card"/>
<meta content="@azchords" property="twitter:site"/>
<meta content="@azchords" property="twitter:creator"/>
<meta content="https://www.azchords.com/" property="twitter:domain"/>
<meta content="GUITAR TABS &amp; CHORDS | AZ Chords.com" itemprop="name"/>
<meta content="AZchords.com - Guitar tabs and chords. Bass, drum, ukulele tablatures. Guitar lessons" itemprop="description"/>
<meta content="https://www.azchords.com/logo.gif" itemprop="image"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="ALL" name="robots"/>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-6585116-3']);
    _gaq.push(['_setDomainName', 'azchords.com']);
     
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</head>
<body>
<div class="container _header">
<div class="row-fluid up-header">
<div class="span12">
<div class="pull-right">
<div class="components-header-breadcrumbs">
<h1><span itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem"><span itemprop="item" style="color: #75859A"><span itemprop="name"> Chords, Tabs | AZ Chords.com</span></span><a itemprop="url" style="display:none"></a></span></h1></div> </div>
</div>
</div>
</div>
<div class="container _header">
<div class="row-fluid components-header-banner">
<div class="span2">
<!-- Azchords 160x90 TXT -->
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-1248082249428003" data-ad-slot="7590358573" style="display:inline-block;width:160px;height:90px"></ins>
<script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
</div>
<div class="span10 right">
<div>
<script>
                    cf_page_artist = tab.artist_name;
                    cf_page_song = tab.song_name;
                    cf_adunit_id = "39380721";
                </script>
<script src="//srv.clickfuse.com/showads/showad.js"></script>
</div>
</div>
</div>
</div>
<div class="container greyrow header-greyline">
<div class="row-fluid">
<div class="span2 components-header-logo">
<a href="/">
<img alt=" chords, tabs, tablatures, lyrics" height="40" src="/images/logo-134x40.gif" width="134"/>
</a>
</div>
<div class="span7 components-header-abc">
            Artists: 
            <a class="top" href="/a.html">A</a> <a class="top" href="/b.html">B</a> <a class="top" href="/c.html">C</a> <a class="top" href="/d.html">D</a> <a class="top" href="/e.html">E</a> <a class="top" href="/f.html">F</a> <a class="top" href="/g.html">G</a> <a class="top" href="/h.html">H</a> <a class="top" href="/i.html">I</a> <a class="top" href="/j.html">J</a> <a class="top" href="/k.html">K</a> <a class="top" href="/l.html">L</a> <a class="top" href="/m.html">M</a> <a class="top" href="/n.html">N</a> <a class="top" href="/o.html">O</a> <a class="top" href="/p.html">P</a> <a class="top" href="/q.html">Q</a> <a class="top" href="/r.html">R</a> <a class="top" href="/s.html">S</a> <a class="top" href="/t.html">T</a> <a class="top" href="/u.html">U</a> <a class="top" href="/v.html">V</a> <a class="top" href="/w.html">W</a> <a class="top" href="/x.html">X</a> <a class="top" href="/y.html">Y</a> <a class="top" href="/z.html">Z</a> <a class="top" href="/0-9.html">#</a>         </div>
<div class="span3 components-header-search">
<div class="pull-right">
<form action="https://www.google.com" id="cse-search-box">
<input name="cx" type="hidden" value="partner-pub-1248082249428003:8224518360"/>
<input name="ie" type="hidden" value="UTF-8"/>
<input name="q" size="55" style="width: 190px;" type="text"/>
<input name="sa" onclick="js:document.getElementById('cse-search-box').submit();" src="/images/icons/24/search.png" type="Image"/>
</form>
<script src="https://www.google.com/coop/cse/brand?form=cse-search-box&amp;lang=en" type="text/javascript"></script>
</div>
</div>
</div>
</div>
<div class="container main">
<div class="row-fluid">
<div class="span12">
<div class="layers-mainlayer-container">
<div class="row-fluid">
<div class="span2">
<div class="row-fluid">
<div class="span12 center">
<script>
                        /* AZChords 160X600 Brand Ads */
                        cf_page_artist = tab.artist_name;
                        cf_page_song = tab.song_name;
                        cf_adunit_id = "39380722";
                    </script>
<script src="//srv.clickfuse.com/showads/showad.js"></script>
<div style="margin:3px 0;"></div>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Azchords 160x600 v2 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-1248082249428003" data-ad-slot="2531600178" style="display:inline-block;width:160px;height:600px"></ins>
<script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
<div class="widgets-popular">
<div class="widgets-popular-header">
        Best Songs
    </div>
<a href="/d/davidbowie-tabs-1015/spaceoddity-tabs-10543.html">
<div class="pop-item">
<div class="pop-item-link">
<span class="lg-hover">Space Oddity</span>
</div>
<div class="grey tdn"> David Bowie</div>
</div>
</a>
<a href="/b/bowiedavid-tabs-577/heroes-tabs-51324.html">
<div class="pop-item">
<div class="pop-item-link">
<span class="lg-hover">Heroes</span>
</div>
<div class="grey tdn"> Bowie David</div>
</div>
</a>
<a href="/b/bowiedavid-tabs-577/lifeonmars-tabs-130010.html">
<div class="pop-item">
<div class="pop-item-link">
<span class="lg-hover">Life On Mars</span>
</div>
<div class="grey tdn"> Bowie David</div>
</div>
</a>
<a href="/b/bowiedavid-tabs-577/starman-tabs-101903.html">
<div class="pop-item">
<div class="pop-item-link">
<span class="lg-hover">Starman</span>
</div>
<div class="grey tdn"> Bowie David</div>
</div>
</a>
<a href="/d/davidbowie-tabs-1015/moonagedaydream-tabs-213943.html">
<div class="pop-item">
<div class="pop-item-link">
<span class="lg-hover">Moonage Daydream</span>
</div>
<div class="grey tdn"> David Bowie</div>
</div>
</a>
<a href="/o/oasis-tabs-2851/wonderwallchords-tabs-30062.html">
<div class="pop-item">
<div class="pop-item-link">
<span class="lg-hover">Wonderwall Chords</span>
</div>
<div class="grey tdn"> Oasis</div>
</div>
</a>
<a href="/p/pinkfloyd-tabs-3040/wishyouwerehere-tabs-112875.html">
<div class="pop-item">
<div class="pop-item-link">
<span class="lg-hover">Wish You Were Here</span>
</div>
<div class="grey tdn"> Pink Floyd</div>
</div>
</a>
<a href="/m/metallica-tabs-2536/nothingelesmatters-tabs-110725.html">
<div class="pop-item">
<div class="pop-item-link">
<span class="lg-hover">Nothing Eles Matters</span>
</div>
<div class="grey tdn"> Metallica</div>
</div>
</a>
<a href="/c/cashjohnny-tabs-717/hurt-tabs-102772.html">
<div class="pop-item">
<div class="pop-item-link">
<span class="lg-hover">Hurt</span>
</div>
<div class="grey tdn"> Cash Johnny</div>
</div>
</a>
<a href="/h/hozier-tabs-54616/takemetochurch-tabs-817381.html">
<div class="pop-item">
<div class="pop-item-link">
<span class="lg-hover">Take Me To Church </span>
</div>
<div class="grey tdn"> Hozier</div>
</div>
</a>
</div>
<!-- <div class='widgets-popular'>
    <div class="widgets-popular-header">Follow us on</div>
    <a class="top" title="Follow on Facebook" href="https://www.facebook.com/azchords" target="_blank" rel="nofollow"><img src="/images/icons/38/facebook-icon.png" alt="" width="38" height="38" /></a>
    <a class="top" title="Follow on Twitter" href="https://www.twitter.com/azchordscom" target="_blank" rel="nofollow"><img src="/images/icons/38/twitter-icon.png" alt="" width="38" height="38" /> </a>
</div>
-->
</div>
</div>
</div>
<div class="span10 layers-mainlayer-content">
<div class="row-fluid">
<div class="span12 h2title">
<h2>Guitar chords and tabs, guitar pro, bass tabs, drum tabs, ukulele</h2>
<div class="pull-left header-social fl">
<!-- Facebook like2 -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.async=true;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=133000596876438&version=v2.3";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-like" data-action="like" data-href="https://www.facebook.com/azchords" data-layout="button" data-share="false" data-show-faces="false"></div>
<!-- End Facebook like2 -->
</div>
</div>
</div>
<div class="row-fluid">
<div class="span12">
<br/>
<div class="row-fluid">
<div class="span12">
<div class="row-fluid">
<div class="span5 header">
         Popular artists, bands 
    </div>
</div>
<div class="row-fluid">
<div class="span12 lcu-table">
<div class="row-fluid lcu-table-cell">
<div class="span4">
<a class="ringto2" href="/s/samsmith-tabs-50952.html">Sam Smith</a> </div>
<div class="span4">
<a class="ringto2" href="/i/idinamenzel-tabs-37059.html">Idina Menzel</a> </div>
<div class="span4">
<a class="ringto2" href="/v/vancejoy-tabs-49166.html">Vance Joy</a> </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span4">
<a class="ringto2" href="/j/johnlegend-tabs-24730.html">John Legend</a> </div>
<div class="span4">
<a class="ringto2" href="/p/pharrellwilliams-tabs-49123.html">Pharrell Williams</a> </div>
<div class="span4">
<a class="ringto2" href="/l/lynyrdskynyrd-tabs-2399.html">Lynyrd Skynyrd</a> </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span4">
<a class="ringto2" href="/p/pinkfloyd-tabs-3040.html">Pink Floyd</a> </div>
<div class="span4">
<a class="ringto2" href="/j/jimihendrix-tabs-2024.html">Jimi Hendrix</a> </div>
<div class="span4">
<a class="ringto2" href="/i/ironmaiden-tabs-1939.html">Iron Maiden</a> </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span4">
<a class="ringto2" href="/s/scorpions-tabs-3462.html">Scorpions</a> </div>
<div class="span4">
<a class="ringto2" href="/r/redhotchilipeppers-tabs-3232.html">Red Hot Chili Peppers</a> </div>
<div class="span4">
<a class="ringto2" href="/c/cranberries-tabs-900.html">Cranberries</a> </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span4">
<a class="ringto2" href="/w/weezer-tabs-4713.html">Weezer</a> </div>
<div class="span4">
<a class="ringto2" href="/p/pearljam-tabs-2995.html">Pearl Jam</a> </div>
<div class="span4">
<a class="ringto2" href="/u/u2-tabs-4506.html">U2</a> </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span4">
<a class="ringto2" href="/j/jeffbuckley-tabs-1995.html">Jeff Buckley</a> </div>
<div class="span4">
<a class="ringto2" href="/l/ledzeppelin-tabs-2269.html">Led Zeppelin</a> </div>
<div class="span4">
<a class="ringto2" href="/a/adele-tabs-29679.html">Adele</a> </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span4">
<a class="ringto2" href="/p/passenger-tabs-34849.html">Passenger</a> </div>
<div class="span4">
<a class="ringto2" href="/j/jasonmraz-tabs-1983.html">Jason Mraz</a> </div>
<div class="span4">
<a class="ringto2" href="/c/coldplay-tabs-836.html">Coldplay</a> </div>
</div>
</div>
</div>
</div>
</div>
<br/>
<div class="row-fluid">
<div class="span5 header">
         Last updates 
    </div>
</div>
<div class="row-fluid">
<div class="span12">
<div class="lcu-table">
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/j/jeansablon-tabs-59969/puisquevouspartezenvoyage-tabs-940519.html">Puisque Vous Partez En Voyage Chords <span class="muted">(ver.1)</span></a><br/> by Jean Sablon        </div>
<div class="span6">
<a class="ringto" href="/b/bobmarley-tabs-548/sunisshining-tabs-940518.html">Sun Is Shining Chords <span class="muted">(ver.1)</span></a><br/> by Bob Marley        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/s/sleepingwithsirens-tabs-31332/ineedtoknow-tabs-940517.html">I Need To Know Chords <span class="muted">(ver.1)</span></a><br/> by Sleeping With Sirens        </div>
<div class="span6">
<a class="ringto" href="/m/mina-tabs-5369/lavocedelsilenzio-tabs-940516.html">La Voce Del Silenzio Chords <span class="muted">(ver.1)</span></a><br/> by Mina        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/r/rivermaya-tabs-3326/prodigaldub-tabs-940515.html">Prodigal Dub Chords <span class="muted">(ver.1)</span></a><br/> by Rivermaya        </div>
<div class="span6">
<a class="ringto" href="/o/ozarkhenry-tabs-59499/icondj-tabs-940514.html">Icon Dj Chords <span class="muted">(ver.1)</span></a><br/> by Ozark Henry        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/e/emmamarrone-tabs-59755/sottovoce-tabs-940513.html">Sottovoce Chords <span class="muted">(ver.1)</span></a><br/> by Emma Marrone        </div>
<div class="span6">
<a class="ringto" href="/g/guidedbyvoices-tabs-5165/thehead-tabs-940512.html">The Head Chords <span class="muted">(ver.1)</span></a><br/> by Guided By Voices        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/g/guidedbyvoices-tabs-5165/thehead-tabs-940511.html">The Head Tabs <span class="muted">(ver.1)</span></a><br/> by Guided By Voices        </div>
<div class="span6">
<a class="ringto" href="/b/bigblack-tabs-30037/thepowerofindependenttrucking-tabs-940510.html">The Power Of Independent Trucking Chords <span class="muted">(ver.1)</span></a><br/> by Big Black        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/b/bigblack-tabs-30037/pavementsaw-tabs-940509.html">Pavement Saw Chords <span class="muted">(ver.1)</span></a><br/> by Big Black        </div>
<div class="span6">
<a class="ringto" href="/t/themongols-tabs-27870/keeper-tabs-940508.html">Keeper Chords <span class="muted">(ver.1)</span></a><br/> by The Mongols        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/b/bigblack-tabs-30037/ldopa-tabs-940507.html">L Dopa Chords <span class="muted">(ver.1)</span></a><br/> by Big Black        </div>
<div class="span6">
<a class="ringto" href="/a/amongthethirsty-tabs-31539/whoyousayiam-tabs-940506.html">Who You Say I Am Chords <span class="muted">(ver.1)</span></a><br/> by Among The Thirsty        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/e/ericlapointe-tabs-23515/magueule-tabs-940505.html">Ma Gueule Chords <span class="muted">(ver.1)</span></a><br/> by Eric Lapointe        </div>
<div class="span6">
<a class="ringto" href="/d/diearzte-tabs-6397/wieesgeht-tabs-940504.html">Wie Es Geht Chords <span class="muted">(ver.1)</span></a><br/> by Die Arzte        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/c/clash-tabs-814/charliedontsurf-tabs-940503.html">Charlie Dont Surf Chords <span class="muted">(ver.1)</span></a><br/> by Clash        </div>
<div class="span6">
<a class="ringto" href="/e/eltonjohn-tabs-1316/whitemandanger-tabs-940502.html">White Man Danger Chords <span class="muted">(ver.1)</span></a><br/> by Elton John        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/b/blackcrowes-tabs-487/wyomingandme-tabs-940501.html">Wyoming And Me Chords <span class="muted">(ver.1)</span></a><br/> by Black Crowes        </div>
<div class="span6">
<a class="ringto" href="/l/lorettalynn-tabs-39085/aintnotimetogo-tabs-940500.html">Aint No Time To Go Chords <span class="muted">(ver.1)</span></a><br/> by Loretta Lynn        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/e/eltonjohn-tabs-1316/stealawaychild-tabs-940499.html">Steal Away Child Chords <span class="muted">(ver.1)</span></a><br/> by Elton John        </div>
<div class="span6">
<a class="ringto" href="/s/stevelacy-tabs-63251/outofmehead-tabs-940498.html">Out Of Me Head Bass <span class="muted">(ver.1)</span></a><br/> by Steve Lacy        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/h/hector-tabs-49185/perjantaionmielessin-tabs-940497.html">Perjantai On Mielessäin Chords <span class="muted">(ver.1)</span></a><br/> by Hector        </div>
<div class="span6">
<a class="ringto" href="/b/blackveilbrides-tabs-39508/shadowsdie-tabs-940496.html">Shadows Die Tabs <span class="muted">(ver.2)</span></a><br/> by Black Veil Brides        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/d/danielleatethesandwich-tabs-48789/stillbyyourside-tabs-940495.html">Still By Your Side Chords <span class="muted">(ver.1)</span></a><br/> by Danielle Ate The Sandwich        </div>
<div class="span6">
<a class="ringto" href="/s/survivor-tabs-3915/populargirl-tabs-940494.html">Popular Girl Chords <span class="muted">(ver.1)</span></a><br/> by Survivor        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/t/tigersjaw-tabs-41857/charmer-tabs-940493.html">Charmer Chords <span class="muted">(ver.1)</span></a><br/> by Tigers Jaw        </div>
<div class="span6">
<a class="ringto" href="/r/ramones-tabs-3195/iwantedeverything-tabs-940492.html">I Wanted Everything Chords <span class="muted">(ver.1)</span></a><br/> by Ramones        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/t/tigersjaw-tabs-41857/softspoken-tabs-940491.html">Soft Spoken Chords <span class="muted">(ver.1)</span></a><br/> by Tigers Jaw        </div>
<div class="span6">
<a class="ringto" href="/c/cake-tabs-679/shortskirtlongjacket-tabs-940490.html">Short Skirt Long Jacket Chords <span class="muted">(ver.2)</span></a><br/> by Cake        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/t/tigersjaw-tabs-41857/teenrocket-tabs-940489.html">Teen Rocket Chords <span class="muted">(ver.1)</span></a><br/> by Tigers Jaw        </div>
<div class="span6">
<a class="ringto" href="/b/burlives-tabs-37953/greercountybachelor-tabs-940488.html">Greer County Bachelor Chords <span class="muted">(ver.1)</span></a><br/> by Burl Ives        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/j/johnbutlertrio-tabs-2053/somethingsgottagive-tabs-940487.html">Somethings Gotta Give Chords <span class="muted">(ver.1)</span></a><br/> by John Butler Trio        </div>
<div class="span6">
<a class="ringto" href="/g/guidedbyvoices-tabs-5165/theunsinkablefatsdomino-tabs-940486.html">The Unsinkable Fats Domino Chords <span class="muted">(ver.1)</span></a><br/> by Guided By Voices        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/f/franciscabrel-tabs-23773/peupledesfontaines-tabs-940485.html">Peuple Des Fontaines Chords <span class="muted">(ver.1)</span></a><br/> by Francis Cabrel        </div>
<div class="span6">
<a class="ringto" href="/j/jeffbeck-tabs-1994/spanishboots-tabs-940484.html">Spanish Boots Chords <span class="muted">(ver.1)</span></a><br/> by Jeff Beck        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/s/skillet-tabs-3608/cycledown-tabs-940483.html">Cycle Down Chords <span class="muted">(ver.1)</span></a><br/> by Skillet        </div>
<div class="span6">
<a class="ringto" href="/t/twiddle-tabs-59613/frankenfoote-tabs-940482.html">Frankenfoote Chords <span class="muted">(ver.1)</span></a><br/> by Twiddle        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/b/burlives-tabs-37953/lilymunroe-tabs-940481.html">Lily Munroe Chords <span class="muted">(ver.1)</span></a><br/> by Burl Ives        </div>
<div class="span6">
<a class="ringto" href="/u/ulflundell-tabs-42870/jessejamesmterkrleken-tabs-940480.html">Jesse James Möter Kärleken Chords <span class="muted">(ver.1)</span></a><br/> by Ulf Lundell        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/j/joanbaez-tabs-24667/stillwatersatnight-tabs-940479.html">Still Waters At Night Chords <span class="muted">(ver.1)</span></a><br/> by Joan Baez        </div>
<div class="span6">
<a class="ringto" href="/d/dame-tabs-59017/lowlife-tabs-940478.html">Low Life Chords <span class="muted">(ver.1)</span></a><br/> by Dame        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/j/jimmybuffett-tabs-24640/pencilthinmustache-tabs-940477.html">Pencil Thin Mustache Chords <span class="muted">(ver.1)</span></a><br/> by Jimmy Buffett        </div>
<div class="span6">
<a class="ringto" href="/d/davidbowie-tabs-1015/glassspider-tabs-940476.html">Glass Spider Chords <span class="muted">(ver.1)</span></a><br/> by David Bowie        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/s/stevencurtischapman-tabs-27522/adesperatebenediction-tabs-940475.html">A Desperate Benediction Chords <span class="muted">(ver.1)</span></a><br/> by Steven Curtis Chapman        </div>
<div class="span6">
<a class="ringto" href="/t/tonightalive-tabs-44237/bookoflove-tabs-940474.html">Book Of Love Tabs <span class="muted">(ver.1)</span></a><br/> by Tonight Alive        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/y/youmeatsix-tabs-42791/heavysoul-tabs-940473.html">Heavy Soul Tabs <span class="muted">(ver.1)</span></a><br/> by You Me At Six        </div>
<div class="span6">
<a class="ringto" href="/s/setitoff-tabs-48066/foryouforever-tabs-940472.html">For You Forever Tabs <span class="muted">(ver.1)</span></a><br/> by Set It Off        </div>
</div>
<div class="row-fluid lcu-table-cell">
<div class="span6">
<a class="ringto" href="/w/withconfidence-tabs-62540/dinnerbell-tabs-940471.html">Dinner Bell Tabs <span class="muted">(ver.1)</span></a><br/> by With Confidence        </div>
<div class="span6">
<a class="ringto" href="/0-9/5secondsofsummer-tabs-49032/midnight-tabs-940470.html">Midnight Tabs <span class="muted">(ver.1)</span></a><br/> by 5 Seconds Of Summer        </div>
</div>
<div class="row-fluid">
<div class="span3">
<br/>
<a class="more" href="/new.html">More Updates</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="row-fluid">
<div class="span12 ftrbutt">
</div>
</div>
<div class="row-fluid ad-margin">
<div class="span12 left">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Azchords 728x90 - 2 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-1248082249428003" data-ad-slot="5440430939" style="display:inline-block;width:728px;height:90px"></ins>
<script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
</div>
</div>
<div class="row-fluid">
<div class="span12">
<span class="small"><a href="#top">↑ Back to top</a> |  Tablatures and chords for acoustic guitar and electric guitar, ukulele, drums are parodies/interpretations of the original songs. You may use it for private study, scholarship, research or language learning purposes only</span>
</div>
</div> <div class="_footer">
<div class="row-fluid">
<div class="pull-left">
<a href="/add.html">Add song</a> |
            <a href="/contact.html">Contact us</a> | 
            <a href="/privacy.html">Privacy policy</a> | 
            <a href="/dmca.html">DMCA</a> | 
            <a href="/links.html">Links</a>
</div>
<div class="pull-right">
<span style="color:#BBB">© 2021 AZChords.com</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>
