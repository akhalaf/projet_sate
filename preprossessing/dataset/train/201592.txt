<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "14160",
      cRay: "61107525fe7732c1",
      cHash: "2048ccd30233699",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmNkYi5jb20vY2FydG9vbnMvV2FybmVyX0Jyb3NfLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "fA42YEw2DIb84B9oI79jfNSc2y4u9HaeJUBa5+wSnPEYbrbAhedffHbvQ69tcQqKuUUeIDEvS8dVkKchImiVjByCnyd1OSKPtT0u/xQVskt7MoeTyk1H4eP3DqTso9iGhP7QF1r9UqLtEGS4oMTsopGvGdJ8m0mZfql2em392L2cJaBIM6vhO/fX8k2xIvPcyibrVIJjZCY7SXqb0GdowgKL8HyuvICl48LqUKHTNhqW56J+RGd/CncSNk3W2KY/mESiPQg0Btn0jfedGRhcQJNEuJMLaxBTMRo+66UksILyYCVNHixpbwGgep5D/DVHNOC5OLAyJQGm8BDjK/7wP099IcKQqTgNu2BRE0KCr1LKRE0I/Jtx4Zu8wgDze5dD7Iy/gAeIecdgMZO9BHLqn85DZo/cZYMed+Pvnz2pZj3Iz9DwaQBONjWBys8tzYz8KcTYuBq8Du7+OiRBtLzzPlSUdawp9eU8evfRUkAWVmqgCH20a9mLOv5IxleuJSeByfqLBaV699GsWRPbJL0fe8hjf2sGP60y/7GNYi2aLx+3eSBp8RBe5G8ID8VGexEwRgEKR+62x0c6ntzB0sJ5b/pYC+ByZ1HQuf8rN3bdT76klzzWxUFZEEAIBBNEUqEkn4kvRw4lyJ+c+QvXj0soIkJuajtxFmL17cUXRiFMPhuF4vNJJE8DvAxn40ou1a9UhKtUkGQdifUAnz9urQcW716y5fTkjMkDx/dvULRjhRtW1FoR7ZTsjwcyONZLddDr",
        t: "MTYxMDU1NDk2Mi44ODcwMDA=",
        m: "KmcI8HzbKEeebCbG3RHc3MjD3Nj1HgQZjQARUl+oJpI=",
        i1: "N8Gg/J7pBtzXt51kEpxgRQ==",
        i2: "jBy7UGqNQYHrp+izwA0vgg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "cXDaOma3wiBrP6kg81PYP0GD5VWUQPtDIIDjSKLqBr4=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.bcdb.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/cartoons/Warner_Bros_/?__cf_chl_captcha_tk__=4e16376d1c2423907dec1e979d1000286ea18e53-1610554962-0-AdzvweaprjiJEkVBcoToepOOTkAFe_N4Np3vymp9G2neDqW2mwxKk19kaOhfsz8xUMzPSSmpmRSC1ooaUMwY0rDI6iG1CrAfoAuoiEkjo737Tyic6o-3VOcycT9ADHbfm2WUCmfLyWj7cVmFg3DgaZTJYl8d9Cdfaey0DpNZy8auaqGsjqWtpBYyaeYN7HWxZcZzwGUO622xxYWpe7LEq1cKJcW_yiZZKxxmVxXY5KHVJNBsbpuChF07q2t-E70KqvyeYobU1osBpWLtC53_Un16y52cCJdQRNI7gAJpQUXbE3MAiZ9Uil2tweS-Ydmhj4_M1lwDVfiF0ITWtjY_-IlDVZggr8vclOAO5CVqOuv42D7bi0ozdVxbKDmgq0Uvsbqo9u8oDzE_O3lWd1dJ0PWvftQw-jEmHNipcfoLLZjOlB_TCX4sapUHhRqTnfJaoo7XZ9VpC3APgIYAgTEyV-yKFzmTWd7HR-o3KW7Jw8Bcg9WhbkcUyadZyFaPIiMHYMp31vXLHT1H8fkUGvQGS-RUHSjAwKxhCKMJ-oBm90S9" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="812b484b29cbf07a8be20060b2726e6c3b10ef8f-1610554962-0-AdFrD3EQY8ImSKy4dMyT+unfG7WpUUd5+pXEgMoUmNl6JnmMgK1mZoRq+ju3wgms39WCj/SWLdqAID43ds8DIol3X4sa3dsFWKv4CilJ2elxJslCo7FVokFnoG1Ho7QumR+i9G9Jif8B/lwXHof3hKkX6jw+GBSmNozm4iZR3Nv5cf5YtsRBa5IJ3kDTms0V6FxOFcKJjWvYruyhOQPMzt03AkOtWCfH/IdNj2ddhBhOwA+RtC0Plrqsy7WePpfugiBZprsWjhYEapQcCPvIFsHfWoo5clupOhltDKIPOdEc3BkWpX18hqydupUUMJ7yyemGKHwabgYk0pF0mpW745cuRd9oEgTjqI3enXAIk8QlUxy09SWdDTlh5AjAtLyDwwIgNH3X43m+tMSce+4fJwEdEXyDUl/itbTt1C3csuJoEpi9UyOa0KRoqzJKRy7bMwWNJYbyHET9YVDYpceWGPeSLgQ8wOWWPquV9Ds/HzFV8xZm/uHFLTFmklSgpM+xS+tXf5oJ0tZPP9uxpAJGmDV8LrQJQAH0oHDZ7EpWIT606+2jhh/Cocf7Scb/MOMi5ZK6lakPHInoXm+MnTQskCa7/IdNYJbe+DkzqETbssKTjim2mtMJw04OO66JcgE4I4EOgoXeF+zUToloa/hzJH2+T+6M+JPruju94OGFT0RZO8HadkjXfQffrfLZMRFtEukenTkrDrHBfT218ZCF1Sh07flgmt/LTGqEjDgdDtEQNIKrn73iTK8AmKjdkPTu+helsHFF1agrr0wc2AwmBWivrYGAP2ImMUFD1r2nAFACmQ8iANNej7FexPMuV3RAfZ1XEM1zVP2YEcIBNxrxZMXh7FdJhwWRfGPJ3cMgwdMN8GJcK7uFm9Q9DdXyhA+mMvDlIv9rGl3nBYYGM9oceoFO/j/qimYS/1VtCSOFiUkuoGtGklD954W7MoKuO1UA8bsTuMeaz09LDsv7/kWhA3LA/dSC1jhPDM8YH6vAHjtudurAB46uX0fNlIYisglgZsYJynBDMNve32Cl22GMuZVjkym2qZ+oyT7gEZ2nrD/JOyPIu1VhaWToJ9fJ3yzkQSjTj48YGS4F4cEYmLwXBJbRrJH10HB+/IwdE5FSzCWDFAcjdCHZq1x23QnPy5RiTXoDevVsbKkZtkbSMWv93tpRX4QjFro21POT6q5RXyz+KNJifaq8Cfw92B2Gi90pvMi4paYjd8vjgdmfjx10bkEcdXz6l+k/09fJv3+7CsiiuNw5OVTo0weL2LtjGkhBa1FeEMIJIeTaFxa/vBBE0nw="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="609105a6e712510bfbbfc8dd963e7813"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=61107525fe7732c1')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<a href="https://tinwatch.net/inclusivecool.php?tag=9"></a>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">61107525fe7732c1</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 210.75.253.169</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
