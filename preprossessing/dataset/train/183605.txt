<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "60839",
      cRay: "610fd4ab59361a46",
      cHash: "bc38d27ce22739d",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmFja3N0YWdlbWFnaWNzaG93LmNvbS9lcmljLz9wYWdlX2lkPTgy",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "awTCKxvagT1HTv/fMmToBNY8rWob3joRekQp7q4vR2vO9T6JCHr1TDYP5VRkbOilP2zLHlNCTTKq8yly9DUWD8OXpx7f7Embjb7/msuTERN9njGQO9GKt+noWe+Hi+xBdD7k/dcmSFurhB4zYOCRNpCvLkJwVPVPBr2lZVYua3LZQu9XMa1t+XV67l1EEkPPWPjQQfNkHWwulY0vqPc0oEOuWJKIhw0GyVOxIg1pjUFG6fCURR1yMY+YGpVDX/EtV7qGpvN4vOBCunQMMO2tU2y06+HB0DPbtiYEvN07QAhXTbCjQ2MLW+lpbNw4rtWbdSkx50s3Dv6JcCtGmU6spmkDEww6sGnOwtAMd8i34FnmG9yOEFZjHkw/mqNdndhoHZPssefctUlXg/TcMXG2SSrYKvn1AYstY0D4fWkV0D2TAXbLz1ZF3FOARGYkTTpd+DQzzArEFfnbfa4r0ZesJ07YEWXJGdpIMG1rKTPj31ol7Yd524RiDlr0cgYBo4GL5JJ/92j6jkSW7sTby2VOitA+JnrnT0LYgP4VPnL0FWrIOhxJBYqlDjYA0DFN6Q2Z99+ADk9fBmeWd0CHpzUgKrSFY5bnqM+CtwLjLj+4x78uDBQeAkRicp5QEQpdboEZRpD/eRK51mSmyKiqNZCtesvgsmxO+31eFoeZ/IjhPizQCJSMmB7k29ym5GZRSVhTe0B5clqBVgVsglK2AZXkgljV1n75N9rrJhqAGYSUnVwAhb+BrCtp/w4xRbJQV5Uk",
        t: "MTYxMDU0ODM4OS42NjMwMDA=",
        m: "HtLTz3TL12C+SumqPnKOcoZ0h3hhv4FQuRXV1VWgZvE=",
        i1: "EK+z3ArYygB9NO3E2kL4HQ==",
        i2: "AnPUU2ClAuXLvHXOoOxFXg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "B3FCVgR6RtaOUH2fNjZS3Pi2j4cuNkITWzfBHp4sNH8=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.backstagemagicshow.com</h2>
</div><!-- /.header -->
<a href="https://derchris.net/fungoidintensity.php?guid=91"><span style="display: none;">table</span></a>
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/eric/?page_id=82&amp;__cf_chl_captcha_tk__=cb3bb5dee2a14e8e2151092ba31fe83de994758f-1610548389-0-ASheYlzxtmMEYZMjxZ16w4PuVClJMRWHQVQJ_4HW4Na-AnQE2vQ9TGkVvrEqImR8o6mtZJGzap3Sf7SFX5V6OIlurE8WsHiy0M9UBlltKsX2P2dnL-t-T7KxzKSRTn2BPnq6BzY2Ya-Yr3INLly14NSpvrVQFJnee_JPWEDgEzgNy3g7AXQ97WLObE5Yh6oXB0Xx4SENiFf_tk60gzG0H8qIlOIkqXc6Zh6xCwl0xl7yJuvghJwUMHoLGlcDNbRfKq1VM1RqM7Y_JA5qGyjpHltlFt7BUFgZgt0VAXikw8eN8MtEgLfV3Ku7eEI3KOkkypgcA62s9q1rzkT1fgBgCuvk9nCDEdan5RkLdtVpVOiiNMhAiZFRKKLFDs1U7JuRXrzJjfF8-AU1YZBX7OUq8AWxx03vjzXlyYcEQmZWGbFsIpgvgI-8pSJlutphFewwB8U5Z0GBgpPYYJADazhTh0-U8u5C84gUgg6bTNZ262hXhPzjWY7Px8rHjh8FYnksIC9HnnfCn5flFOqnU_1Etbtcg7wRj7uaOn_bFGAJ128u" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="246b1205ddcbee3a5fa19f63c21d8a02efcfdb34-1610548389-0-AdaXiCO7DaAeEBs9viqMDgF+5rTOfABnh2XQsb8eup0s2y/erWGtYmBNNN5jo6cnjEoYWmms23iPp2vQtCrrChtX6jnrEHIF8iHdIfcU7F0pez08nf90CZjBpepGGBD2Yelj7w5rz/yhKBJZDK7CMs124uNYcWgrYnn98844cctZvaWHkkGSkPGsRhvS9JGGjH7/yqifa9FCnRXKej2R/A5ZpQPFMYUWdveb7ZRMspq13RnC/Nd/2TpisOPcJIM0d1qkE09MKRySOpaigm+8vxhjE+oq5K31duxi0C68RGDp/TOgoHSiVKkOJM24op8viqtL9jZ6KuzLff5W5rP650HTxQPYExGlBS+F5vde3YIjP870CfbEKsI5WF3rE3TqBkNcuWQMndopxm/q1jmj1cAja0PDO1S0wqblTZdDeCKm8lITybFdqA4vsn62jQm+SViZnxpIF1ROAbMZG77SNSDcmj6riJWenib4otMCZuvE2ww86ptecRyPqkkhCeKsSSOSHq7BD4BBAkIOPgiU2Ev+tBplFkgjCanMocowHEnRAV2rkPxLEYv3J1W9rShIVZtZhWHhiz35ZEQ8LGBh4a4AQnm8WK0ScsmgVkAkHLb0d0tJ9Kk4X/VdHqNi0j49wcrFz8P6W7sfyDlVuDfeDXpySE5lm//l2odrgRdLJT/087/BMuaikLA9AFK77w3GVpNgh+Z+eNh8EfNknb32/wsCr3ObVexenr9dmooplOmkWDikKcndqQMZNZJz6/CfJGVfNYX9tJbXuYIXyGk81wTp3rKvRBQl2O8B112Bchno3CkgM2LBaKtcncz57o3f0EgjokN2nUrREHEAiszJwFRItL6Ibod4EeBTZ+Qh0gaxwy1DhWkoMYZWVaw2MVyk5g3nIbUViz0cemUlxF/P7vjXsEKa6BLFPBQEAP91Uyydzh+/QKyaq7DQ10t7suq63r19dm/PynVJ4tcLJbC/lHSnF3BnA1jirInI9c/dtA4pgTW1PVEu+0xD31ZV2OiV5TUyz3m+lNNSRan0Y6A7KQ9pw5yCrWn6//z9ptC64eUqPCCYci9rjsJSBUjl5dw0wALpunmPDW/AGx0vBbjI9ylJRV+aLcdgryYX0FgLpvVB5hCgTPKRlAL5JGwU4ulTtKNiE4syf8KtKax4v4URc1mn8HJwYq7E3ZFo6Fo/U40InX3BfpdChybYQwP2jlESnunGpns+o992PKsoZYHWSpZ4FIqjOC5Ikz/3ClfSWLFu/wwRSvBz8ZCZgKgxcZpHo4VUFqSVDpxKRhswmrjdL4RdWfwpMDxXKYZvjTHDqDC2Vjwz2+061iyQlMWkYy1x5ddrIaKES2wPHhx3/CaPnAk="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="839b82fea50cecc9e1ce81f77806c523"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610fd4ab59361a46')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610fd4ab59361a46</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
