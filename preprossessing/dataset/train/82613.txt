<html><head><title>Facts About the Republic of Benin: Official Document</title>
</head><body><h1>Facts About the Republic of Benin: Official Document</h1>
<p>
</p><h2>Geography</h2>
<p>
</p><h4>Location</h4>

Benin is located in West Africa and covers a land area Of 112,622 Sq. 
km. and constitutes a long stretch of hand perpendicular to the Coast 
of the 
Gulf of Guinea. It is bordered on the North by Burkina Faso and the 
Republic of Niger, on the East by the Federal Republic of Nigeria and 
on the 
West by the Republic of Togo. With a 124 kilometers long coastline, it 
stretches North to South some 672 kilometers while its breath 
extends 324 
kilometers at the evident point. It is above two third the Size of 
Portugal.
<p>
</p><h4>Topography</h4>

Benin can be divided into five natural regions:   A coastal area, low, 
sandy and about 2 to 5 kilometers wide, bounded by lagoons;   A 
plateau zone 
called "La terre de barre" made of iron clay cut with marshy dips;   A 
silica clayey plateau with wooded savannah extending North of 
Abomey to the 
foothills of the Atakora hills; - A hilly region in the Northwest, the 
Atakora, with elevation ranging from 500 to 800 metres and 
constituting the water 
reservoir for Benin and Niger Republics.  Niger plains which are vast 
fertile silica-clayey areas.
<p>
</p><h4>Vegetation</h4>

The forest thins out considerably in the center and gives way lo 
grassland. Elsewhere, cultivated crops predominate, including the 
immense palmgroves 
of lower Benin and the coconut plantations on the 124 kilometers 
long coastline and along the lagoons.
<p>
</p><h2>Climatology</h2>
<p>



Benin is characterized by unusually dry conditions. This is due 
primarily to two very important factors. First, the situation of the 
coast which is 
rather well protected from the western winds; second, the Atakora 
Barrier in the West and North West which decreases the amount of 
rainfall.
</p><p>
The great part of the country is under the influence of transitional 
tropical conditions. Rainfall is not as abundant as found in areas with 
the same 
latitude thereby giving rise to tropical conditions known as the 
BENIN variant. These conditions are marked by a dry season from 
November to the 
beginning of April and a rainy season from the latter part of April to 
October.
</p><p>
The Southern portion of the Republic of Benin, i.e. the Coastal zone, is 
under the influence of a Northern transitional equatorial climate 
characterized 
by a long dry season from November to the end of March, a first 
rainy season from April to July, a small dry period in August and a 
second rainy 
season in September and October.
</p><p>
The Northern portion of the country is subject to a true tropical 
climate. A long dry season in winter can be observed with a long 
rainy season in the 
summer.
</p><p>
The mean temperature is between 77oF and 82oF (25o to 28oC).
</p><p>
The best time to visit the Southern area is from December to March 
and July/August while visiting period for the Northern part of the 
Country is 
between December and April.
</p><p>
</p><h2>Communications</h2>
<p>


The geographic setting of Benin serves to integrate the region and 
provides direct access to the bordering states by water, rail, airlines 
and railways. 
</p><p>
5-seaters 
ply between Cotonou and Lome, Cotonou and Lagos to name the 
nearest other capital cities while buses and lorries are available for 
such long distance trips as 
Cotonou-Parakou, Parakou-Kandi, Parakou-Malanville, Parakou-
Djougou . 
</p><p>
Roads: 8,000 kilometers will 1,000 km or bitumenized 
roads 
</p><p>
Railways: 570 
kilometers in joint venture with the Republic of NIGER. 
</p><p>
Airport: Main 
airport is in Cotonou with many foreign air companies. 
</p><p>
Port: 
International harbor with 
modern facilities in Cotonou. Telecommunications: Infrastructures are 
performing more than 6,000 lines with direct contact with the 
external world. 
</p><p>
There is a 
full range of postal services in most towns and localities. Telex and 
Fax facilities are available in Cotonou.
</p><p>
</p><h2>The Constitution at a glance</h2>
<p>
</p><h3>A multi-party system country</h3>
<p>
</p><h4>President/ Head of State/ Head of Government</h4>

A one-man-one-vote suffrage to elect the President/Head of 
Government who may be a member to a Party. 
His tenure of office is 
five (5) years 
and is renewed only once. 
He should be of Beninese nationality for at 
least ten (10) years. 
The vacancy (resignation, death...) of the 
presidency 
is filled by the Speaker of the National Assembly. The new Head of 
State is elected within forty (40) days. The President/ Head of State/ 
Head 
of Government addresses the Nation on the state of the Nation in the 
National Assembly Hall once in a year
<p>
</p><h4>National Assembly</h4>

A one-man-one-vote suffrage to elect the Members of Parliament 
(MP). His 4-year mandate is renewable. There is one MP for 70,000 
inhabitants. The vacancy (resignation, death...) at the speakership is 
filled by his successor elected within fifteen (15) days when the 
House is in 
session or at an immediate meeting held in compliance with its rules 
of procedure. The vacancy of an MP is filled by his substitute also 
elected in 
the same manner. There are two ordinary sessions starting within 
the first fortnight of April and the second fortnight of October 
respectively. 
Each session cannot exceed three (3) months. The decision is taken 
by a simple majority.
<p>
</p><h2>The Flag</h2>
<p>

The National Flag was, for the first time, hoisted formally on the 
independence day, August 1, 1960 to replace the French Flag.
</p><p>
The colors are green, red and yellow.
</p><p>
As explained in the second verse of the National Anthem, the green 
denotes hope for renewal, the red evokes the ancestors' courage 
while the 
yellow calls to mind the country's richest treasures.
</p><p>
But when the country went red in 1975 after a military coup on 
October 26,1972 the then one-party regime, with its Marxist-Leninist 
ideology, 
decided to change the National Flag. It became a plain green flag with 
a red star on its upper left part.
</p><p>
Fed up with a centrally governed State whose ideology had but 
retarded the country's development in all fields, a National 
Conference in which all 
walks of life participated from February 19 to 28, 1990 decided 
among other things to re-establish the above-mentioned flag of 
August 1,1960.
</p><p>
</p><h2>Demography and Languages</h2>
<p>
</p><h4>Demography</h4>
 The population of BENIN is estimated at 4,500,000 
inhabitants largely concentrated in Southern coastal region near the 
major 
port city of Cotonou (450,000 inhabitants) the chief town of the 
Atlantic Department, the capital city of Porto Novo (200,000 
inhabitants) in the 
OUEME Department as well as the "Royal City" of Abomey (80,000 
inhabitants) in the Central Department of ZOU. 
The annual growth 
rate is 
3.1%. Other important towns are Ouidah, Allada, Abomey, Grand 
Popo, Lokossa, Save, Savalou, Parakou, Djougou, Natitingou, 
Malanville, Kandi.
<p>
</p><h4>Languages </h4>
Over half the people speak Fon. Yoruba, Mina, Bariba and 
Dendi are the other important languages. French is the official 
language. Beside the French language, English is necessarily one of 
the two foreign languages taught in secondary schools. 
<p>
Greetings in Fon 
 
- Good morning: AH-FON Ghan-Jee-Ah <br/>
- Good evening: Kou Do Bah Dah <br/>
- How are you: Ah-Doh Ghan-Jee-Ah <br/>
- Thank you: Ah-Wah-Nou <br/>
- Good bye: OH-Dah-Boh <br/>
</p><p>
Languages spoken: 
<br/>
The ATLANTIC "department" <br/>
- Fon, Alada, Ayizo, Seto, Tofin, Toli. <br/>
The ATAKORA "department" <br/>
- Basila, Cabrai, Dendi, Dompago, Dyerma, Fulfulde, Gourmantche, 
Kotokoli, Mossi, Natember; others are ouinzi-ouingi, Peul, Pila, 
Somba, Waama, Ditamari. <br/>
The BORGU "department" <br/>
- Bargu, Bariba, 
Bouko, Dendi; others are Dyerma, Fulfulde, Peul, Niendi. <br/>
The MONO "department" <br/>
- Adja, Guin, Mina, Nago; others are Popo, Saxwe, Waci, 
Xweda, Xwela. <br/>
The OUEME "department" 
- Ayizo, Gun, Holi, 
Idaca, Ife; others are Nago, Weme, Yoruba. The Zou "department" - 
Fon, Idaca, Ife, Mahi; others are Nago, Seto.<br/>
</p><p>
</p><h2>Places of Interest</h2>
<p>
</p><h4>Ouidah:</h4>

Spelt "WHYDAH" in history books written in English, it is the 
"Museum City". It is evocative of European penetration with its 
ancient 
Portuguese, English, Danish and French trading posts or strongholds.
There can be seen the remains of the ancient port from which slaves 
were boarded and shipped to the Americas.
<p>
</p><h4>Abomey:</h4>

Referred to as the "Royal City", it is the capital of Dan-Home, the 
ancient Kingdom. It has one of the most impressive museums of 
Africa. Its 
artists and craftsmen, be they weavers, jewelers, woodcarvers, iron 
and brass workers are famous far beyond the boundaries of the 
Republic of 
BENIN.
<p>
</p><h4>Allada:</h4>

It is the city, the cradle of "voodoos" in vogue in the Americas, 
namely in Brazil, West Indies, the Caribbean countries.
<p>
</p><h4>Porto Novo:</h4>

The "City with three Names" (Porto Novo,Hogbonou,Adjatche) . It is 
Benin's administrative capital, right in
the middle of the Yoruba land.
<p>
</p><h4>Nikki:</h4>

The historic capital of the Baatonu people.
<p>
</p><h4>Natitiogou:</h4>

Its castle-type "TATA-Sombas" and the traditional huts of the 
Tanekas and other tribes in the North where there are the richly 
varied fauna of the 
National Parks of Pendjari and "W".
<p>
</p><h4>Ganvie:</h4>

AFRICA's unique floating villages built on stilts. A population of 
several thousand. Motorboats or dugouts are available for the trips 
across the 
lake to the Ganvie. During the trip, there are Akadjas made of stakes 
and bushes in the shape of open circles or triangles driven into the 
bed of 
the plantless Lake. Seeking shelter among the foliage, the fish can 
thus be easily caught or kept for breeding.
<p>
</p><h2>Visa Requirements</h2>
<p>

1/ Two (2) application forms in legible writing. 
</p><p>
2/ Two (2) passport size photos.
</p><p>
3/ International certificate of vaccination (yellow fever and cholera).
</p><p>
4/ Visa is issued for fifteen (15) days: Entry and transit within 3 
months. Extensions may be obtained at the Immigration Office.
</p><p>
5/ A $ 20.00 (twenty dollars) fee for each applicant (cash, money 
order of certified cheque only. No personal cheque please.) 
</p><p>
6/ A letter of guarantee from employer of travel agency or Xerox of 
round trip ticket or a Bank letter of guarantee.<br/>
- Join your passport to the forms. <br/>
- Please allow 48 hours for 
issuance of visa.<br/>
- Passport must be valid for at least six (6) months and if it is to be 
sent back by mail, please enclose self addressed certified envelope or 
an express mail 
envelope.<br/>
</p><p>
</p><h2>History</h2>
<p>

It may be recalled that Benin, former Dahomey, is perhaps the "most 
beaten track by Europeans of any Africa". The history of Benin is a 
succession of kingdoms.
In 1704, France received permission to erect a port at Ouidah, and in 
1752 the Portuguese founded Porto Novo. On June 22, 1894, the 
territory 
was named by decree the "Colony of Dahomey and its dependences" 
and was granted autonomy which it retained until October 18, 1904 
when it 
became part of French West Africa.
On December 4, 1958 the Republic was proclaimed. Dahomey became 
independent on August 1, 1960 and is a UN member country.
</p><p>
If the first independent Government was ousted by a military coup 
on October 28, 1963, Dahomey, during the ensuing years up to 1972, 
went 
through a lot of political upheavals that always climaxed in military 
coups.
That of October 26, 1972 was the starting point of a 17-year regime 
which three years later went red with a Marxist Leninist ideology. In 
other 
words, on November 30, 1975 Dahomey was under a centrally 
controlled government and eventually became the People's Republic 
of Benin.
At the National Conference held in Cotonou (February 19-28, 1990) 
and at which all walks of life were represented, fundamental 
decisions were 
taken, namely:<br/>
- abolition of Marxist ideology as the State philosophy. <br/>
- the 
reversion to the genuine flag.<br/>
- the reversion to the multi party system.<br/>
- the dissolution of all one-party structures.<br/>
- the release of all political detainees and prisoners. <br/>
- the respect of 
all Human Rights.<br/>
</p><p>
</p><h2>Coat of Arms</h2>
<p>


It is an escutcheon with:
- in the first quarter, a gold Somba castle.<br/>
- in the second quarter in silver, the Star of Benin painted to the life, 
that is to say an eight azure point cross 
  with, at its angle, silver radiuses and sand in abyss.<br/>
- in the third quarter, a sinople silver palmtree laden with 
heralds.<br/>
- in the fourth quarter, a ship evocative of European penetration into 
the Country.<br/>
</p><p>
Supporters:  Two gold brindled panthers.<br/>
Crest:  Two horns full of sand with maize in the ear.<br/>
Motto:  Fraternity, Justice, Labor sandwritten on a lancepennon.<br/>
</p><p>
</p><h2>Industry and Trading</h2>
<p>

Industry accounts for only a small percentage of the gross domestic 
product. Fishing industry meets only local consumption, so does 
textile 
industry. Palm processing facility needs improving; a sugar complex 
and a cement factory are jointly owned with Negeria. Breweries, soap 
unit... meet only local demand. Possotome village is, however, known 
for its internationally recognized mineral water. 
</p><p>
Apart from 
limestone 
found in open quarry at ONIGBOLO, deposits of gold, phosphates, iron 
ore, marble, clay... are yet to be explored. The development of off-
shore fields at SEME and elsewhere are underway. A Benin/Togo 
hydroelectric power has just been completed on the Mono river (the 
NANGBETO dam). There are attractive industrial projects and 
feasibility studies are available for some of them. The Beninese code 
of 
investment has been reviewed to insert, among other things, more 
incentives for investors. Benin is the natural gateway to Togo and 
Nigeria 
and to such landlocked countries as Burkina Faso, Niger and Mali. 
</p><p>
Direct investments by American companies are promoted and 
strongly 
encouraged by the Benin Government. There can be joint ventures 
between Benin private sector and American companies to carry out 
such 
industrial undertakings as canning, paper processing units, glass 
manufacturing, salt processing units, agribusiness, pharmaceuticals, 
clothing, 
palm oil, building materials,chemicals and any other items reflective 
of an industrial developing nation.
</p><p>
</p><h2>Banks, Working Time, Public Holidays</h2>
<p>
</p><h4>Banks</h4>
- Central Bank of West Africa
  P.O. Box 325 Cotonou<br/>
- Bank of Africa<br/>
- Financial Bank<br/>
- International Bank of Benin (B.I.B.)<br/>
  The currency is the CFA franc divided into 100 centimes; the parity 
with the French franc is fixed:<br/>
  1 FF = 50 CFA francs.<br/>
<p>
</p><h4>Working Time</h4>
<pre>
			Business hours
				Monday - Friday:	8a.m. to 12:30 a.m
	 						3:30 p.m. to 7 p.m.
				Saturday:		9 a.m. to 1 p.m.

			Government
				Monday - Friday:	8 a.m. to 12:30 3 p.m.
							3 p.m. to 6:30 p.m.

 </pre>
<p>
</p><h4>Public Holidays</h4>
<pre>
January 1:           New Year's Day
Easter Monday
Ascension Day
May 1:               	May Day
Whitmonday
August 1:           	National Day
August 15:         	Assumption Day
November 1:      	All Saints' Day
December 25:      	Christmas Day
Ramadan
Ad-el-FlTR
Maouloud
</pre>
<p>
The details regarding Muslim Holidays are but approximative since 
they are observed following the sightings of the moon.
</p><p>
</p><h2>Hotel Industry</h2>
<p>
</p><pre>
Benin Sheraton Cotonou		Hotel de L'etoile Cotonou
P.O. Box 1901 Cotonou			P.O. Box 1866
Telex: 511-5112			Tel. (229) 31-56-41
Tel. (229) 30-01-00 and		Telex: 5340
(229) 30-12-56      

PLM Aledjo Cotonou			Hotel de France Cotonou
P.O. Box 2292				P.O. Box 921
Telex: 5180				Tel.(229) 32-19-44
Tel. (229) 33-05-61 and 
(229)33-05-62	

Hotel de La Plage Cotonou		Hotel Beaurivage Porto Novo	   
P.O. Box 36				P.O. Box 387	   
Tel. (229) 33-25-60 and		Tel. (229) 21-23-99
(229) 31-25-61          	

Hotel du Port Cotonou			Hotel Dona Porto Novo	   
P.O. Box 7067				P.O. Box 95
Tel. (229) 31-44-43			Tel. (229) 21-30-52 and 
Telex: 5377				(229) 21-30-38
   
Hotel du Golfe Cotonou		Hotel Dako 1er Bohicon/Abomey                                    
P.O. Box 37				P.O. Box 218 
Tel. (229) 33-09-55			Tel. (229) 51-01-38 and 
Telex. 5321				(229) 51-02-38

El Dorado Cotonou			Hotel Gbena Ouidah
P.O. Box 37				P.O. Box 208
Tel. (229) 33-09-23			Tel (229)  34-12-15

Hotel G. L. Cotonou			Etoile Rouge Hotel Lokossa
P.O. Box 1226				P.O. Box 17
Tel. (229) 33-16-17			Tel. (229) 41-12-30
Telex: 5311

Hotel de L'Union Cotonou		Le Relais des Routiers Parakou
P.O. Box 921				P.O. Box 81
Tel. (229) 31-27-66 and		Tel. (229) 61-04-01
(229) 31-55-60                                   

Croix du Sud Hotel Cotonou		Hotel Tata Somba (PLM)
P.O. Box 280				P.O. Box 82
Tel. (229) 30-09-54 and 		Tel. (229) 82-11-24
(229) 30-09-55	   
     	       
Village Club Aheme
B.P. 2090 Cotonou
</pre>
</body></html>