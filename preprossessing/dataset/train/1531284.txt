<!DOCTYPE html>
<html lang="es">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-127092484-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127092484-1');
</script>
<meta content="DTkC8Mizyc3XUCALz7UkHmJRUiboR-sqys4Ygd0B5dc" name="google-site-verification"/>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport"/>
<title>
Event Spaces DR | Encuentra el espacio perfecto para tu evento </title>
<meta content="Encuentra el espacio perfecto para tu evento" name="description"/>
<meta content="" name="keywords"/>
<meta content="index, follow" name="robots"/> <meta content="2021, Event Spaces DR. Merit Designs" name="copyright"/>
<meta content="Merit Designs 2021, Web developer: Andrés J. Villar" name="author"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://eventspacesdr.com/xmlrpc.php" rel="pingback"/>
<link href="https://fonts.googleapis.com/css?family=Zilla+Slab:300,400,500,600,700" rel="stylesheet"/>
<link href="https://eventspacesdr.com/wp-content/themes/eventvenues/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://eventspacesdr.com/wp-content/themes/eventvenues/style.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet"/>
<link href="https://eventspacesdr.com/wp-content/themes/eventvenues/css/lightbox.min.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" media="all" rel="stylesheet"/>
<link href="https://eventspacesdr.com/wp-content/themes/eventvenues/favicon.png" rel="icon" type="image/x-icon"/>
<!--[if lt IE 9]>
<script src="https://eventspacesdr.com/wp-content/themes/eventvenues/js/html5.js" type="text/javascript"></script>
<![endif]-->
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>
<!-- reCaptcha -->
<script src="https://www.google.com/recaptcha/api.js?hl=es"></script>
</head>
<body class="home">
<div class="desktop_menu menu_home" id="menu">
<div class="container-fluid">
<div class="row">
<div class="col-sm-12">
<nav class="navbar navbar-default" role="navigation">
<div class="navbar-header">
<button class="navbar-toggle" data-target="#navbar" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="https://eventspacesdr.com" title="Event Spaces DR">
<img class="logo_home" src="https://eventspacesdr.com/wp-content/themes/eventvenues/images/logo_2xnew.png"/>
<img class="logo" src="https://eventspacesdr.com/wp-content/themes/eventvenues/images/logonew.png"/>
</a>
</div>
<div class="collapse navbar-collapse" id="navbar">
<ul class="nav navbar-nav navbar-right">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-16 current_page_item menu-item-27 active " id="menu-item-27"><a aria-current="page" href="https://eventspacesdr.com/">Inicio</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26" id="menu-item-26"><a href="https://eventspacesdr.com/venues/">Busca tu Venue</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25" id="menu-item-25"><a href="https://eventspacesdr.com/contactenos/">Contáctenos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24" id="menu-item-24"><a href="https://eventspacesdr.com/publica-tu-venue/">Publica tu Venue</a></li>
</ul>
</div>
</nav>
</div> <!-- .col-sm-12 -->
</div> <!-- .row -->
</div> <!-- container-fluid -->
</div> <!-- /.desktop_menu -->
<div class="carousel bs-slider fade control-round indicators-line" data-interval="5000" data-pause="hover" data-ride="carousel" id="bootstrap-touch-slider">
<!--
	<ol class="carousel-indicators">
    	<li data-target="#bootstrap-touch-slider" data-slide-to="0" class="active"></li>
        <li data-target="#bootstrap-touch-slider" data-slide-to="1"></li>
        <li data-target="#bootstrap-touch-slider" data-slide-to="2"></li>
   	</ol>
   -->
<!-- Wrapper -->
<div class="carousel-inner" role="listbox">
<div class="item active" style="background: url(https://eventspacesdr.com/wp-content/uploads/2017/10/slide.jpg) no-repeat center center; background-size: cover;">
<!-- Slide Background -->
<div class="bs-slider-overlay"></div>
</div>
<div class="item " style="background: url(https://eventspacesdr.com/wp-content/uploads/2017/10/slide2.jpg) no-repeat center center; background-size: cover;">
<!-- Slide Background -->
<div class="bs-slider-overlay"></div>
</div>
</div><!-- End of Wrapper For Slides -->
<!-- Controls -->
<!--
	<a class="left carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="prev">
	<span class="fa fa-angle-left" aria-hidden="true"></span>
	<span class="sr-only">Previous</span>
	</a>

	<a class="right carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="next">
    <span class="fa fa-angle-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
    </a>
	-->
<div class="buscador">
<div class="container-fluid">
<div class="row">
<div class="col-sm-12">
<h1>Encuentra el espacio perfecto para tu evento</h1>
<h2>Deja que nuestro equipo te asista en todo lo que necesites</h2>
<form action="https://eventspacesdr.com/venue/" method="get">
<select class="tipo" name="categoria" required="">
<option value="">¿Qué tipo de evento planeas?</option>
<option value="bodas">Bodas</option>
<option value="baby-showers-bautizos">Baby Showers / Bautizos</option>
<option value="cumpleanos">Cumpleaños</option>
<option value="corporativos">Corporativos</option>
<option value="reuniones">Reuniones</option>
<option value="filmacion">Filmación</option>
<option value="sesiones-fotograficas">Sesiones fotográficas</option>
<option value="luna-de-miel-y-viajes">Luna de Miel y Viajes</option>
<option value="iglesias">Iglesias</option>
<option value="getting-ready">Getting Ready</option>
<option value="propuestas-de-matrimonio">Propuestas de Matrimonio</option>
<option value="indoor-infantiles">Indoor Playgrounds/Infantiles</option>
</select>
<select class="lugar" name="provincia" required="">
<option value="">¿Donde?</option>
<!--
							<option value="azua">Azua</option>
							<option value="bahoruco">Bahoruco</option>
							<option value="barahona">Barahona</option>
							<option value="dajabon">Dajabón</option>
							<option value="distrito-nacional">Distrito Nacional</option>
							<option value="duarte">Duarte</option>
							<option value="elias-pina">Elías Piña</option>
							<option value="el-seibo">El Seibo</option>
							<option value="espaillat">Espaillat</option>
							<option value="hato-mayor">Hato Mayor</option>
							<option value="hermanas-mirabal">Hermanas Mirabal</option>
							<option value="independencia">Independencia</option>
							<option value="la-altagracia">La Altagracia</option>
							<option value="la-romana">La Romana</option>
							<option value="la-vega">La Vega</option>
							<option value="maria-trinidad-sanchez">María Trinidad Sánchez</option>
							<option value="monsenor-nouel">Monseñor Nouel</option>
							<option value="monte-cristi">Monte Cristi</option>
							<option value="monte-plata">Monte Plata</option>
							<option value="pedernales">Pedernales</option>
							<option value="peravia">Peravia</option>
							<option value="puerto-plata">Puerto Plata</option>
							<option value="samana">Samaná</option>
							<option value="san-cristobal">San Cristóbal</option>
							<option value="san-jose-de-ocoa">San José de Ocoa</option>
							<option value="san-juan">San Juan</option>
							<option value="san-pedro-de-macoris">San Pedro de Macorís</option>
							<option value="sanchez-ramirez">Sánchez Ramírez</option>
							<option value="santiago">Santiago</option>
							<option value="santiago-rodriguez">Santiago Rodríguez</option>
							<option value="santo-domingo-este">Santo Domingo Este</option>
							<option value="santo-domingo-oeste">Santo Domingo Oeste</option>
							<option value="santo-domingo-norte">Santo Domingo Norte</option>
							<option value="valverde">Valverde</option>
							-->
<option value="santo-domingo">Santo Domingo</option>
<option value="zona-colonial">Zona Colonial</option>
<option value="boca-chica">Boca Chica</option>
<option value="juan-dolio">Juan Dolio</option>
<option value="barahona">Barahona</option>
<option value="jarabacoa">Jarabacoa</option>
<option value="constanza">Constanza</option>
<option value="santiago">Santiago</option>
<option value="puerto-plata">Puerto Plata</option>
<option value="samana">Samana/Las Terrenas</option>
<option value="punta-cana">Punta Cana</option>
<option value="sosua">Cabarete/Sosua</option>
<option value="la-romana">La Romana</option>
<option value="san-cristobal">San Cristobal</option>
<option value="ocoa">Ocoa</option>
<option value="bani">Bani</option>
</select>
<input type="submit" value="Buscar"/>
</form>
</div>
</div>
</div>
</div>
</div>
<div class="bg-grey section maspadding">
<div class="decoration1"><img src="https://eventspacesdr.com/wp-content/themes/eventvenues/images/decoration1.png"/></div>
<div class="decoration2"><img src="https://eventspacesdr.com/wp-content/themes/eventvenues/images/decoration2.png"/></div>
<div class="container-fluid">
<div class="row">
<div class="col-sm-12 text-center">
<big>Busca tu espacio</big>
<div class="line"><span></span></div>
<h3 class="slim grey">¿Qué tipo de evento planeas?</h3>
</div>
<div class="clear"></div>
<div class="featured">
<div class="col-sm-4 col-xs-6">
<div class="categorias">
<a href="https://eventspacesdr.com/venues/bodas">
<div class="overlay"></div>
<img alt="" class="attachment-full size-full" height="146" sizes="(max-width: 307px) 100vw, 307px" src="https://eventspacesdr.com/wp-content/uploads/2017/10/bodas.jpg" srcset="https://eventspacesdr.com/wp-content/uploads/2017/10/bodas.jpg 307w, https://eventspacesdr.com/wp-content/uploads/2017/10/bodas-300x143.jpg 300w" width="307"/> <div class="titulo"><h3>Bodas</h3></div>
</a>
</div>
</div>
<div class="col-sm-4 col-xs-6">
<div class="categorias">
<a href="https://eventspacesdr.com/venues/baby-showers-bautizos">
<div class="overlay"></div>
<img alt="" class="attachment-full size-full" height="146" sizes="(max-width: 307px) 100vw, 307px" src="https://eventspacesdr.com/wp-content/uploads/2017/10/bautizos.jpg" srcset="https://eventspacesdr.com/wp-content/uploads/2017/10/bautizos.jpg 307w, https://eventspacesdr.com/wp-content/uploads/2017/10/bautizos-300x143.jpg 300w" width="307"/> <div class="titulo"><h3>Baby Showers / Bautizos</h3></div>
</a>
</div>
</div>
<div class="col-sm-4 col-xs-6">
<div class="categorias">
<a href="https://eventspacesdr.com/venues/cumpleanos">
<div class="overlay"></div>
<img alt="" class="attachment-full size-full" height="146" sizes="(max-width: 307px) 100vw, 307px" src="https://eventspacesdr.com/wp-content/uploads/2017/10/cumpleanos.jpg" srcset="https://eventspacesdr.com/wp-content/uploads/2017/10/cumpleanos.jpg 307w, https://eventspacesdr.com/wp-content/uploads/2017/10/cumpleanos-300x143.jpg 300w" width="307"/> <div class="titulo"><h3>Cumpleaños</h3></div>
</a>
</div>
</div>
<div class="col-sm-4 col-xs-6">
<div class="categorias">
<a href="https://eventspacesdr.com/venues/corporativos">
<div class="overlay"></div>
<img alt="" class="attachment-full size-full" height="146" sizes="(max-width: 307px) 100vw, 307px" src="https://eventspacesdr.com/wp-content/uploads/2017/10/corporativo.jpg" srcset="https://eventspacesdr.com/wp-content/uploads/2017/10/corporativo.jpg 307w, https://eventspacesdr.com/wp-content/uploads/2017/10/corporativo-300x143.jpg 300w" width="307"/> <div class="titulo"><h3>Corporativos</h3></div>
</a>
</div>
</div>
<div class="col-sm-4 col-xs-6">
<div class="categorias">
<a href="https://eventspacesdr.com/venues/reuniones">
<div class="overlay"></div>
<img alt="" class="attachment-full size-full" height="146" sizes="(max-width: 307px) 100vw, 307px" src="https://eventspacesdr.com/wp-content/uploads/2017/10/reuniones.jpg" srcset="https://eventspacesdr.com/wp-content/uploads/2017/10/reuniones.jpg 307w, https://eventspacesdr.com/wp-content/uploads/2017/10/reuniones-300x143.jpg 300w" width="307"/> <div class="titulo"><h3>Reuniones</h3></div>
</a>
</div>
</div>
<div class="col-sm-4 col-xs-6">
<div class="categorias">
<a href="https://eventspacesdr.com/venues/filmacion">
<div class="overlay"></div>
<img alt="" class="attachment-full size-full" height="146" sizes="(max-width: 307px) 100vw, 307px" src="https://eventspacesdr.com/wp-content/uploads/2017/10/filmacion.jpg" srcset="https://eventspacesdr.com/wp-content/uploads/2017/10/filmacion.jpg 307w, https://eventspacesdr.com/wp-content/uploads/2017/10/filmacion-300x143.jpg 300w" width="307"/> <div class="titulo"><h3>Filmación</h3></div>
</a>
</div>
</div>
<div class="col-sm-4 col-xs-6">
<div class="categorias">
<a href="https://eventspacesdr.com/venues/sesiones-fotograficas">
<div class="overlay"></div>
<img alt="" class="attachment-full size-full" height="146" sizes="(max-width: 307px) 100vw, 307px" src="https://eventspacesdr.com/wp-content/uploads/2020/03/fotos.jpg" srcset="https://eventspacesdr.com/wp-content/uploads/2020/03/fotos.jpg 307w, https://eventspacesdr.com/wp-content/uploads/2020/03/fotos-300x143.jpg 300w" width="307"/> <div class="titulo"><h3>Sesiones fotográficas</h3></div>
</a>
</div>
</div>
<div class="col-sm-4 col-xs-6">
<div class="categorias">
<a href="https://eventspacesdr.com/venues/luna-de-miel-y-viajes">
<div class="overlay"></div>
<img alt="" class="attachment-full size-full" height="146" sizes="(max-width: 307px) 100vw, 307px" src="https://eventspacesdr.com/wp-content/uploads/2020/03/honeymoon.jpg" srcset="https://eventspacesdr.com/wp-content/uploads/2020/03/honeymoon.jpg 307w, https://eventspacesdr.com/wp-content/uploads/2020/03/honeymoon-300x143.jpg 300w" width="307"/> <div class="titulo"><h3>Luna de Miel y Viajes</h3></div>
</a>
</div>
</div>
<div class="col-sm-4 col-xs-6">
<div class="categorias">
<a href="https://eventspacesdr.com/venues/iglesias">
<div class="overlay"></div>
<img alt="" class="attachment-full size-full" height="146" sizes="(max-width: 307px) 100vw, 307px" src="https://eventspacesdr.com/wp-content/uploads/2020/03/iglesias.jpg" srcset="https://eventspacesdr.com/wp-content/uploads/2020/03/iglesias.jpg 307w, https://eventspacesdr.com/wp-content/uploads/2020/03/iglesias-300x143.jpg 300w" width="307"/> <div class="titulo"><h3>Iglesias</h3></div>
</a>
</div>
</div>
<div class="col-sm-4 col-xs-6">
<div class="categorias">
<a href="https://eventspacesdr.com/venues/getting-ready">
<div class="overlay"></div>
<img alt="" class="attachment-full size-full" height="146" sizes="(max-width: 307px) 100vw, 307px" src="https://eventspacesdr.com/wp-content/uploads/2020/03/getting-ready.jpg" srcset="https://eventspacesdr.com/wp-content/uploads/2020/03/getting-ready.jpg 307w, https://eventspacesdr.com/wp-content/uploads/2020/03/getting-ready-300x143.jpg 300w" width="307"/> <div class="titulo"><h3>Getting Ready</h3></div>
</a>
</div>
</div>
<div class="col-sm-4 col-xs-6">
<div class="categorias">
<a href="https://eventspacesdr.com/venues/propuestas-de-matrimonio">
<div class="overlay"></div>
<img alt="" class="attachment-full size-full" height="146" sizes="(max-width: 307px) 100vw, 307px" src="https://eventspacesdr.com/wp-content/uploads/2020/03/propuestas.jpg" srcset="https://eventspacesdr.com/wp-content/uploads/2020/03/propuestas.jpg 307w, https://eventspacesdr.com/wp-content/uploads/2020/03/propuestas-300x143.jpg 300w" width="307"/> <div class="titulo"><h3>Propuestas de Matrimonio</h3></div>
</a>
</div>
</div>
<div class="col-sm-4 col-xs-6">
<div class="categorias">
<a href="https://eventspacesdr.com/venues/indoor-infantiles">
<div class="overlay"></div>
<img alt="" class="attachment-full size-full" height="146" sizes="(max-width: 307px) 100vw, 307px" src="https://eventspacesdr.com/wp-content/uploads/2020/03/infantiles.jpg" srcset="https://eventspacesdr.com/wp-content/uploads/2020/03/infantiles.jpg 307w, https://eventspacesdr.com/wp-content/uploads/2020/03/infantiles-300x143.jpg 300w" width="307"/> <div class="titulo"><h3>Indoor Playgrounds/Infantiles</h3></div>
</a>
</div>
</div>
</div>
<div class="clear"></div>
</div>
</div>
</div>
<div class="section">
<div class="container-fluid">
<div class="row">
<div class="col-sm-12 text-center">
<big>Venues Destacados</big>
<div class="line"><span></span></div>
<h3 class="slim grey">Algunos de nuestros sitios preferidos</h3>
</div>
<div class="clear"></div>
<div class="featured">
<div class="col-sm-12">
<div class="carousel carousel-showmanymoveone slide" id="carousel123">
<div class="carousel-inner">
<div class="item active">
<div class="col-xs-12 col-sm-6 col-md-3">
<div class="ficha">
<div class="imagen">
<a href="https://eventspacesdr.com/venue/hotel-radisson/">
<img alt="" class="attachment-venue_thumb size-venue_thumb wp-post-image" height="440" src="https://eventspacesdr.com/wp-content/uploads/2018/10/Fachada-600x440.jpg" width="600"/> </a>
</div>
<div class="datos">
<p class="nombre"><a href="https://eventspacesdr.com/venue/hotel-radisson/">Hotel  Radisson</a></p>
<p class="ciudad">
			
			Distrito Nacional
		</p>
</div>
<div class="capacidad">
<div>
<img src="https://eventspacesdr.com/wp-content/themes/eventvenues/images/sentados.png"/><br/>
<span>250</span><br/>
				sentados
			</div>
<div>
<img src="https://eventspacesdr.com/wp-content/themes/eventvenues/images/pie.png"/><br/>
<span>600</span><br/>
				de pie
			</div>
</div>
</div>
</div>
</div>
<div class="item ">
<div class="col-xs-12 col-sm-6 col-md-3">
<div class="ficha">
<div class="imagen">
<a href="https://eventspacesdr.com/venue/atracciones-el-lago/">
<img alt="" class="attachment-venue_thumb size-venue_thumb wp-post-image" height="440" src="https://eventspacesdr.com/wp-content/uploads/2018/06/23130885_1173187896144890_5236821311497476625_n-600x440.jpg" width="600"/> </a>
</div>
<div class="datos">
<p class="nombre"><a href="https://eventspacesdr.com/venue/atracciones-el-lago/">Atracciones el lago</a></p>
<p class="ciudad">
			
			Distrito Nacional
		</p>
</div>
<div class="capacidad">
</div>
</div>
</div>
</div>
<div class="item ">
<div class="col-xs-12 col-sm-6 col-md-3">
<div class="ficha">
<div class="imagen">
<a href="https://eventspacesdr.com/venue/388/">
<img alt="" class="attachment-venue_thumb size-venue_thumb wp-post-image" height="440" src="https://eventspacesdr.com/wp-content/uploads/2018/04/hotel-ibiza-3_149021842013-600x440.jpg" width="600"/> </a>
</div>
<div class="datos">
<p class="nombre"><a href="https://eventspacesdr.com/venue/388/">Hotel Ibiza Palmar de Ocoa</a></p>
<p class="ciudad">
			
			Distrito Nacional
		</p>
</div>
<div class="capacidad">
</div>
</div>
</div>
</div>
<div class="item ">
<div class="col-xs-12 col-sm-6 col-md-3">
<div class="ficha">
<div class="imagen">
<a href="https://eventspacesdr.com/venue/hotel-salinas/">
<img alt="" class="attachment-venue_thumb size-venue_thumb wp-post-image" height="440" src="https://eventspacesdr.com/wp-content/uploads/2018/04/124618386-600x440.jpg" width="600"/> </a>
</div>
<div class="datos">
<p class="nombre"><a href="https://eventspacesdr.com/venue/hotel-salinas/">Hotel Salinas</a></p>
<p class="ciudad">
			
			Distrito Nacional
		</p>
</div>
<div class="capacidad">
</div>
</div>
</div>
</div>
</div>
<a class="left carousel-control" data-slide="prev" href="#carousel123">
<img src="https://eventspacesdr.com/wp-content/themes/eventvenues/images/left.png"/>
</a>
<a class="right carousel-control" data-slide="next" href="#carousel123">
<img src="https://eventspacesdr.com/wp-content/themes/eventvenues/images/right.png"/>
</a>
</div>
</div>
</div>
<div class="clear"></div>
</div>
</div>
</div>
<div class="bg-yellow call-to-action">
<div class="img">
<div class="marco"></div>
</div>
<div class="container-fluid">
<div class="row">
<div class="col-sm-6"></div>
<div class="col-sm-6 section text-center">
<img src="https://eventspacesdr.com/wp-content/themes/eventvenues/images/icons.png"/>
<big>¿Planeas un evento?</big>
<p>Deja que te asesoren con tu evento, desde la eleccion del espacio hasta tus suplidores. Te asignaremos un organizador para asistirte en todo el proceso </p>
<a class="boton" href="https://eventspacesdr.com/contactenos/">Cuéntanos qué buscas</a>
</div>
</div>
</div>
</div>
<div class="section steps">
<div class="container-fluid">
<div class="row">
<div class="col-sm-12 text-center">
<big>Lo que hacemos</big>
<div class="line"><span></span></div>
<h3 class="slim grey">Cuenta con nosotros en todo el proceso</h3>
</div>
<div class="maspadding">
<div class="col-sm-4 text-center">
<img src="https://eventspacesdr.com/wp-content/themes/eventvenues/images/step1.jpg"/><br/>
<small>step 1</small><br/>
<strong>Elige tu espacio</strong><br/>
<p>Selecciona el espacio que mas se ajuste a tus necesidades, segun invitados y tipo de evento</p>
</div>
<div class="col-sm-4 text-center">
<img src="https://eventspacesdr.com/wp-content/themes/eventvenues/images/step2.jpg"/><br/>
<small>step 2</small><br/>
<strong>Solicita más información</strong><br/>
<p>Rellena el formulario de solicitud, especificando los datos de tu evento junto a tus datos de contacto</p>
</div>
<div class="col-sm-4 text-center">
<img src="https://eventspacesdr.com/wp-content/themes/eventvenues/images/step3.jpg"/><br/>
<small>step 3</small><br/>
<strong>Deja que te asesoremos</strong><br/>
<p>De inmediato, uno de nuestros especialistas te contactara para asesorarte en todo lo que necesitas sin costo alguno </p>
</div>
</div>
<div class="clear"></div>
</div>
</div>
</div>
<div class="testimonios section maspadding" style="display:none;">
<div class="container-fluid">
<div class="row">
<div class="col-sm-4">
<big>Testimonios</big><br/>
				Lo que dice la gente
			</div>
<div class="col-sm-8">
<div class="carousel slide" id="carousel-testimonios">
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#carousel-testimonios"></li>
<li data-slide-to="1" data-target="#carousel-testimonios"></li>
<li data-slide-to="2" data-target="#carousel-testimonios"></li>
</ol>
<div class="carousel-inner">
<div class="item active">
<p>Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi</p>
</div>
<div class="item ">
<p>Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
</div>
<div class="item ">
<p>Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="space30"></div>
<footer>
<div class="container-fluid">
<div class="row">
<div class="col-sm-4">
<div class="content-logo">
<a href="https://eventspacesdr.com"><img src="https://eventspacesdr.com/wp-content/themes/eventvenues/images/logo_2xnew.png"/></a><br/>
<p class="redes">
            Síguenos<br/>
<a href="https://www.facebook.com/Event-Spaces-104183727679395/?modal=admin_todo_tour" target="_blank"><i class="fa fa-facebook"></i></a>
<a href="https://www.instagram.com/eventspacesdr/" target="_blank"><i class="fa fa-instagram"></i></a>
</p>
</div>
</div>
<div class="col-sm-8">
<div class="row">
<div class="col-sm-3 col-xs-3">
<strong>Inicio</strong>
<p><a href="https://eventspacesdr.com/venues/">Busca tu venue</a></p>
<p><a href="https://eventspacesdr.com/contactenos/">Contáctenos</a></p>
<a class="boton" href="https://eventspacesdr.com/publica-tu-venue/">Publica tu Venue</a>
</div>
<div class="col-sm-3 col-xs-3">
<strong>Venues</strong>
<p><a href="https://eventspacesdr.com/venues/bodas/">Bodas</a></p>
<p><a href="https://eventspacesdr.com/venues/baby-showers-bautizos/">Baby Showers / Bautizos</a></p>
<p><a href="https://eventspacesdr.com/venues/cumpleanos/">Cumpleaños</a></p>
<p><a href="https://eventspacesdr.com/venues/corporativos/">Corporativos</a></p>
<p><a href="https://eventspacesdr.com/venues/reuniones/">Reuniones</a></p>
<p><a href="https://eventspacesdr.com/venues/filmacion/">Filmación</a></p>
<p><a href="https://eventspacesdr.com/venues/sesiones-fotograficas/">Sesiones Fotográficas</a></p>
<p><a href="https://eventspacesdr.com/venues/luna-de-miel-y-viajes/">Luna de miel y viajes</a></p>
<p><a href="https://eventspacesdr.com/venues/iglesias/">Iglesias</a></p>
getting-ready              <p><a href="https://eventspacesdr.com/venues/propuestas-de-matrimonio/">Propuestas de matrimonio</a></p>
<p><a href="https://eventspacesdr.com/venues/indoor-infantiles/">Indoor Playgrounds/Infantiles</a></p>
</div>
<div class="col-sm-6 col-xs-6">
<strong>¿Necesitas ayuda?</strong>
<p>Deja que te asesoremos con tu evento, desde la elección del espacio hasta tus suplidores. Te asignaremos un organizador para asistirte en todo el proceso.</p>
<a class="boton2" href="https://eventspacesdr.com/contactenos/">Cuéntanos qué buscas</a>
<p><a href="mailto:hello@eventspacesdr.com">hello@eventspacesdr.com</a></p>
</div>
</div>
</div>
<div class="clear"></div>
<div class="copyright">
        ©2021 Venuesrd.com. Todos los derechos reservados.<br/>Desarrollado por <a href="https://meritdesigns.com" target="_blank">Merit Designs</a>
</div>
</div>
</div>
</footer>
<script src="https://eventspacesdr.com/wp-content/themes/eventvenues/js/jquery.min.js"></script>
<script src="https://eventspacesdr.com/wp-content/themes/eventvenues/js/bootstrap.min.js"></script>
<script src="https://eventspacesdr.com/wp-content/themes/eventvenues/js/scripts.js"></script>
<script src="https://eventspacesdr.com/wp-content/themes/eventvenues/js/jquery.numeric.js"></script>
<script src="https://eventspacesdr.com/wp-content/themes/eventvenues/js/lightbox.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$(document).ready(function () {
	<!-- Fechas -->
	$( "#fecha" ).datepicker({
		dateFormat: "dd-mm-yy",
		dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
		monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
		changeYear: true,
		changeMonth: true,
		yearRange: '+0:+1',
		minDate: "+0d",
		defaultDate: "+0d"
	});
	
	$(".numeric").numeric();
});
</script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
<script>
(function(){
  // setup your carousels as you normally would using JS
  // or via data attributes according to the documentation
  // http://getbootstrap.com/javascript/#carousel
  $('#carousel123').carousel({ interval: 2000 });
}());

(function(){
  $('.carousel-showmanymoveone .item').each(function(){
    var itemToClone = $(this);

    for (var i=1;i<4;i++) {
      itemToClone = itemToClone.next();

      // wrap around if at end of item collection
      if (!itemToClone.length) {
        itemToClone = $(this).siblings(':first');
      }

      // grab item, clone, add marker class, add to collection
      itemToClone.children(':first-child').clone()
        .addClass("cloneditem-"+(i))
        .appendTo($(this));
    }
  });
}());
</script>
<script>
var $document = $(document);
    $document.scroll(function() {
      if ($document.scrollTop() >= 182) {
        $("#menu").removeClass("menu_home");
            
      } else {
        $("#menu").addClass("menu_home"); 
      }
});
</script>
</body>
</html>