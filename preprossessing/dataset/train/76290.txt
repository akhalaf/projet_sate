<!DOCTYPE html>
<html lang="pl-PL">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://administrator.gdansk.pl/xmlrpc.php" rel="pingback"/>
<title>Strona nie została znaleziona – BZN Administrator</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://administrator.gdansk.pl/feed/" rel="alternate" title="BZN Administrator » Kanał z wpisami" type="application/rss+xml"/>
<link href="https://administrator.gdansk.pl/comments/feed/" rel="alternate" title="BZN Administrator » Kanał z komentarzami" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/administrator.gdansk.pl\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://administrator.gdansk.pl/wp-includes/css/dist/block-library/style.min.css?ver=5.3.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://administrator.gdansk.pl/wp-content/themes/isola/style.css?ver=5.3.6" id="isola-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://administrator.gdansk.pl/wp-content/themes/isola/genericons/genericons.css?ver=3.0.3" id="genericons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://administrator.gdansk.pl/wp-content/plugins/easy-fancybox/css/jquery.fancybox.1.3.23.min.css" id="fancybox-css" media="screen" rel="stylesheet" type="text/css"/>
<script src="https://administrator.gdansk.pl/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://administrator.gdansk.pl/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://administrator.gdansk.pl/wp-json/" rel="https://api.w.org/"/>
<link href="https://administrator.gdansk.pl/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://administrator.gdansk.pl/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3.6" name="generator"/>
<link href="https://administrator.gdansk.pl/wp-content/uploads/2015/09/cropped-logo_512-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://administrator.gdansk.pl/wp-content/uploads/2015/09/cropped-logo_512-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://administrator.gdansk.pl/wp-content/uploads/2015/09/cropped-logo_512-180x180.png" rel="apple-touch-icon-precomposed"/>
<meta content="https://administrator.gdansk.pl/wp-content/uploads/2015/09/cropped-logo_512-270x270.png" name="msapplication-TileImage"/>
</head>
<body class="error404">
<div class="hfeed site" id="page">
<a class="skip-link screen-reader-text" href="#content">Przeskocz do treści</a>
<header class="site-header" id="masthead" role="banner">
<div class="site-header-inner">
<div class="site-branding">
<button class="toggle" id="menu-toggle">
<!--<svg version="1.1" class="menu-toggle-image" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
						<g id="menu">
							<g>
								<rect x="3" y="4" width="18" height="3"/>
								<rect x="3" y="10" width="18" height="3"/>
								<rect x="3" y="16" width="18" height="3"/>
							</g>
						</g>
					</svg><br>-->
<span style="color: #777777">Menu</span>
<span class="screen-reader-text">Menu</span>
</button>
<h1 class="site-title">   <a href="https://administrator.gdansk.pl/" rel="home"><img src="https://administrator.gdansk.pl/wp-content/uploads/nazwa.png" width="160"/></a></h1>
<h2 class="site-description">Biuro Zarządcy Nieruchomości „ADMINISTRATOR” – Andrzej Klonecki</h2>
</div>
<div class="header-search" id="site-search">
<div class="header-search-form">
<form action="https://administrator.gdansk.pl/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Szukaj:</span>
<span class="search-icon">
<svg enable-background="new 0 0 24 24" height="24px" version="1.1" viewbox="0 0 24 24" width="24px" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<g id="search">
<path class="icon" d="M15.846,13.846C16.573,12.742,17,11.421,17,10c0-3.866-3.134-7-7-7s-7,3.134-7,7s3.134,7,7,7
					c1.421,0,2.742-0.427,3.846-1.154L19,21l2-2L15.846,13.846z M10,15c-2.761,0-5-2.238-5-5c0-2.761,2.239-5,5-5c2.762,0,5,2.239,5,5
					C15,12.762,12.762,15,10,15z"></path>
</g>
</svg>
</span>
<input class="search-field" name="s" placeholder="Search …" title="Szukaj:" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Szukaj"/>
</form> </div><!-- .header-search-form -->
</div><!-- #site-navigation -->
</div><!-- .site-header-inner -->
</header><!-- #masthead -->
<div id="toggle-sidebar">
<button id="menu-close">
<span class="screen-reader-text">Close Menu</span>
</button>
<nav class="main-navigation" id="site-navigation" role="navigation">
<div class="menu-wrapper">
<div class="menu-menu-rozwijane-z-lewej-container"><ul class="menu" id="menu-menu-rozwijane-z-lewej"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-386" id="menu-item-386"><a href="https://administrator.gdansk.pl/telefony-alarmowe/">Telefony alarmowe</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-409" id="menu-item-409"><a href="https://administrator.gdansk.pl/o-nas/">O nas</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69" id="menu-item-69"><a href="https://administrator.gdansk.pl/uprawnienia/">Uprawnienia</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78" id="menu-item-78"><a href="https://administrator.gdansk.pl/oferta/">Oferta</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-221" id="menu-item-221"><a href="https://administrator.gdansk.pl/galeria/">Galeria</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-176" id="menu-item-176"><a href="https://administrator.gdansk.pl/do-pobrania/">Do pobrania</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71" id="menu-item-71"><a href="https://administrator.gdansk.pl/wykup-lokalu-co-dalej/">Wykup lokalu, co dalej?</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-150" id="menu-item-150"><a href="https://administrator.gdansk.pl/odczyty-wodomierzy/">Odczyty wodomierzy</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158" id="menu-item-158"><a href="https://administrator.gdansk.pl/ogloszenia-komornicze/">Ogłoszenia komornicze</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-68" id="menu-item-68"><a href="https://administrator.gdansk.pl/rejestracja-w-systemie-kartoteka-online/">Rejestracja w systemie „Kartoteka online”</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-485" id="menu-item-485"><a href="https://administrator.gdansk.pl/nowe-zasady-segregowania-odpadow-w-gdansku-od-1-kwietnia-2018r/">Nowe zasady segregowania odpadów w Gdańsku od 1 kwietnia 2018r.</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-594" id="menu-item-594"><a href="https://administrator.gdansk.pl/ochrona-danych-osobowych/">Ochrona danych osobowych</a></li>
</ul></div> </div>
</nav><!-- #site-navigation -->
<div class="widget-area" id="secondary" role="complementary">
<aside class="widget widget_text" id="text-3"> <div class="textwidget"><h2><a href="https://www.administrator.gdansk.pl/websil/"><img align="left" border="0" hspace="15" src="/wp-content/uploads/zaloguj.png"/>Zaloguj do systemu<br/>"Kartoteka online"</a></h2></div>
</aside> </div><!-- #secondary -->
</div>
<div class="site-content" id="content">
<div align="right">
<a href="/?page_id=161">telefony alarmowe</a>     
		</div>
<div class="content-area" id="primary">
<main class="site-main" id="main" role="main">
<section class="error-404 not-found">
<header class="page-header">
<h1 class="page-title">Ojej! Nie można znaleźć tej strony.</h1>
</header><!-- .page-header -->
<div class="page-content">
<p>It looks like nothing was found at this location. Maybe try a search?</p>
<form action="https://administrator.gdansk.pl/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Szukaj:</span>
<span class="search-icon">
<svg enable-background="new 0 0 24 24" height="24px" version="1.1" viewbox="0 0 24 24" width="24px" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<g id="search">
<path class="icon" d="M15.846,13.846C16.573,12.742,17,11.421,17,10c0-3.866-3.134-7-7-7s-7,3.134-7,7s3.134,7,7,7
					c1.421,0,2.742-0.427,3.846-1.154L19,21l2-2L15.846,13.846z M10,15c-2.761,0-5-2.238-5-5c0-2.761,2.239-5,5-5c2.762,0,5,2.239,5,5
					C15,12.762,12.762,15,10,15z"></path>
</g>
</svg>
</span>
<input class="search-field" name="s" placeholder="Search …" title="Szukaj:" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Szukaj"/>
</form>
</div><!-- .page-content -->
</section><!-- .error-404 -->
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- #content -->
<footer class="site-footer" id="colophon" role="contentinfo">
<div class="site-info">
			Biuro Zarządcy Nieruchomości "ADMINISTRATOR" - Andrzej Klonecki
			<br/>
<span class="genericon genericon-home"></span>80-506 Gdańsk Brzeźno, ul. Dworska 27B 
			<span class="sep"> | </span>
<span class="genericon genericon-handset"></span>58 342 86 76
			<span class="sep"> | </span>
<span class="genericon genericon-mail"></span><a href="mailto:biuro@administrator.gdansk.pl">biuro@administrator.gdansk.pl</a>
</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->
<script src="https://administrator.gdansk.pl/wp-content/themes/isola/js/isola.js?ver=20140623" type="text/javascript"></script>
<script src="https://administrator.gdansk.pl/wp-content/themes/isola/js/navigation.js?ver=20120206" type="text/javascript"></script>
<script src="https://administrator.gdansk.pl/wp-content/themes/isola/js/skip-link-focus-fix.js?ver=20130115" type="text/javascript"></script>
<script src="https://administrator.gdansk.pl/wp-content/plugins/easy-fancybox/js/jquery.fancybox.1.3.23.min.js" type="text/javascript"></script>
<script type="text/javascript">
var fb_timeout, fb_opts={'overlayShow':true,'hideOnOverlayClick':true,'showCloseButton':true,'margin':20,'centerOnScroll':true,'enableEscapeButton':true,'autoScale':true };
if(typeof easy_fancybox_handler==='undefined'){
var easy_fancybox_handler=function(){
jQuery('.nofancybox,a.wp-block-file__button,a.pin-it-button,a[href*="pinterest.com/pin/create"],a[href*="facebook.com/share"],a[href*="twitter.com/share"]').addClass('nolightbox');
/* IMG */
var fb_IMG_select='a[href*=".jpg"]:not(.nolightbox,li.nolightbox>a),area[href*=".jpg"]:not(.nolightbox),a[href*=".jpeg"]:not(.nolightbox,li.nolightbox>a),area[href*=".jpeg"]:not(.nolightbox),a[href*=".png"]:not(.nolightbox,li.nolightbox>a),area[href*=".png"]:not(.nolightbox)';
jQuery(fb_IMG_select).addClass('fancybox image');
var fb_IMG_sections=jQuery('.gallery,.wp-block-gallery,.tiled-gallery');
fb_IMG_sections.each(function(){jQuery(this).find(fb_IMG_select).attr('rel','gallery-'+fb_IMG_sections.index(this));});
jQuery('a.fancybox,area.fancybox,li.fancybox a').each(function(){jQuery(this).fancybox(jQuery.extend({},fb_opts,{'transitionIn':'elastic','easingIn':'easeOutBack','transitionOut':'elastic','easingOut':'easeInBack','opacity':false,'hideOnContentClick':false,'titleShow':true,'titlePosition':'over','titleFromAlt':true,'showNavArrows':true,'enableKeyboardNav':true,'cyclic':false}))});};
jQuery('a.fancybox-close').on('click',function(e){e.preventDefault();jQuery.fancybox.close()});
};
var easy_fancybox_auto=function(){setTimeout(function(){jQuery('#fancybox-auto').trigger('click')},1000);};
jQuery(easy_fancybox_handler);jQuery(document).on('post-load',easy_fancybox_handler);
jQuery(easy_fancybox_auto);
</script>
<script src="https://administrator.gdansk.pl/wp-content/plugins/easy-fancybox/js/jquery.easing.1.4.1.min.js" type="text/javascript"></script>
<script src="https://administrator.gdansk.pl/wp-content/plugins/easy-fancybox/js/jquery.mousewheel.3.1.13.min.js" type="text/javascript"></script>
<script src="https://administrator.gdansk.pl/wp-includes/js/wp-embed.min.js?ver=5.3.6" type="text/javascript"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67493502-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
