<!DOCTYPE html>
<html xmlns:wb="http://open.weibo.com/wb">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content=" maximum-scale=1.0, user-scalable=yes" name="viewport"/>
<title>转运公司,海淘转运,顺丰海淘,八达网海淘转运公司</title>
<meta content="八达网海淘转运公司，提供自助在线免税州海淘和海淘返利，囊括全美上千品牌折扣码及海淘攻略，采用先进的仓储物流智能终端扫描系统，自动拍照定位和全程包裹追踪，为您美国代购海淘及转运中国保驾护航。" name="description"/>
<meta content="转运公司,海淘转运,顺丰海淘" name="keywords"/>
<link href="/Statics/Images/NewWebsiteImg/logo.ico" rel="shortcut icon"/>
<link href="/Statics/Css/Index.css?13.8" rel="stylesheet" type="text/css"/>
<link href="/Statics/Css/Member.css?13.8" rel="stylesheet" type="text/css"/>
<script language="javascript" src="/Statics/Js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script language="javascript" src="/Statics/Js/jquery.cookie.js" type="text/javascript"></script>
<script language="javascript" src="/Statics/Js/hotdealsscroll.js" type="text/javascript"></script>
<script src="/Statics/Js/Front/Home.js?13.8" type="text/javascript"></script>
<script language="javascript" src="/Statics/Js/layer/layer.js" type="text/javascript"></script>
<script src="/Statics/Js/Front/libs/noty/jquery.noty.min.js" type="text/javascript"></script>
<script src="/Statics/Js/Util.js?13.8" type="text/javascript"></script>
<script src="/Statics/Js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="../../Statics/Js/layer/layer.js" type="text/javascript"></script>
<script src="/Statics/Js/Front/Index.js?13.8" type="text/javascript"></script>
<script src="/Statics/Js/jquery.SuperSlide.js" type="text/javascript"></script>
<link href="/Statics/Css/Buyer/buyer.css?13.8" rel="stylesheet" type="text/css"/>
</head>
<script>
    (function (b, a, e, h, f, c, g, s) {
        b[h] = b[h] || function () { (b[h].c = b[h].c || []).push(arguments) };
        b[h].s = !!c; g = a.getElementsByTagName(e)[0]; s = a.createElement(e);
        s.src = "//s.union.360.cn/" + f + ".js"; s.defer = !0; s.async = !0; g.parentNode.insertBefore(s, g)
    })(window, document, "script", "_qha", 320764, false);
</script>
<body>
<div class="bada">
<!--导航开始-->
<div class="top">
<a href="/Index.html"><div class="left_logo"></div> </a>
<div class="nav">
<ul>
<li><a class="cur" href="/Index.html">首页</a></li>
<li>
<a href="/Login.html">会员中心</a>
</li>
<li><a href="/Price.html">服务与价格</a> </li>
<li><a href="/MemberShowOrder.html">用户晒单</a>
</li><li><a href="/ReplaceBuy.html">美网代付</a></li>
<li><a href="/HaiTaoInformation/1">海淘情报</a></li>
<li>海淘中心<span>∨</span>
<ul class="submenu">
<li><a href="/HowToUse.html">快速入门</a></li>
<li><a href="/Help">帮助中心</a></li>
<li><a href="/Help/13/245">保险理赔</a></li>
<li><a href="/Policies.html/26">禁运物品</a></li>
<li><a href="/Help/5/31">海关税率</a></li>
<li><a href="/ShopWebSite.html">购物网站</a></li>
</ul>
</li>
</ul>
</div>
<div class="right_btn">
<a class="logoin" href="/Login.html">
                        登录</a>
<a class="registere" href="/Register.html">
                        注册</a>
</div>
<div class="divTime">
                美国：<span id="spanUs"></span>   
                德国：<span id="spanDe"></span>   
                日本：<span id="spanJp"></span>
</div>
</div>
<div class="line"></div>
<!--导航结束-->
<!--中间内容-->
<div class="effect">
<div class="slideBox" id="slideBox">
<div class="hd">
<ul>
<li>1</li>
<li>2</li>
<li>3</li>
<li>4</li>
<li>5</li>
<li>6</li>
</ul>
</div>
<div class="bd">
<ul>
<li>
<!-- 八达网2021元旦祝福 -->
<img src="https://statics.8dexpress.com/images/Banner/202101YuanDanActivityBanner.jpg"/>
</li>
<li>
<!-- 八达网202012 圣诞、元旦双节活动 20201222上线 -->
<a href="/MarketBanner.html?id=13">
<img src="https://statics.8dexpress.com/images/Banner/202012DoubleDDActivityBanner.png"/></a>
</li>
<li>
<!-- 八达网202007新用户福利大升级 20200721上线 -->
<a href="/MarketBanner.html?id=5">
<img src="https://statics.8dexpress.com/images/Banner/202007RegisterActivityBanner.jpg"/></a>
</li>
<li>
<!-- 八达网202009法国兰蔻攻略轮播跳转 -->
<a href="/Help/14/282" target="_blank">
<img src="https://statics.8dexpress.com/images/Banner/20200903LKGLBanner.jpg"/></a>
</li>
<li>
<!-- 八达网202008海淘情报轮播跳转 -->
<a href="/HaiTaoInformation/1" target="_blank">
<img src="https://statics.8dexpress.com/images/Banner/202008HTInfoBanner.jpg"/></a>
</li>
<li><a href="/showorderact.html">
<img src="https://statics.8dexpress.com/images/Banner/banner_showorder20200115.png"/></a>
</li>
</ul>
</div>
</div>
</div>
<div class="box_bg">
<div class="box_right">
<div style="width: 750px">
<div class="search_left">
<input id="searchorderids" placeholder="请输入要查询的运单号" type="text"/>
<button id="select" type="submit" value="">
                    物流查询</button>
</div>
<div style="float: left; margin-top: 30px; padding-top: 5px; padding-bottom: 5px;
                font-family: 微软雅黑; padding-right: 10px; margin-left: 39px">
<a href="/UserCenter/UpLoadPicSingle">
<img src="/Statics/Images/NewWebsiteImg/upLoadIDico.png"/>
<span style="margin-left: 13px; vertical-align: middle">上传身份证</span> </a>
</div>
</div>
<div class="announcement">
<div class="ico">
<div class="announce">
                    公告</div>
</div>
<div class="txtScroll-top" style="font-family: 微软雅黑; margin-top: 4px;">
<div class="bd">
<ul>
<li class="announce_con"><a href="/IndexNewsInfo/324.html" target="_blank">2021.1月汇率调整通知</a></li>
<li class="announce_con"><a href="/IndexNewsInfo/323.html" target="_blank">日本仓库变更通知</a></li>
<li class="announce_con"><a href="/IndexNewsInfo/322.html" target="_blank">2020年圣诞&amp;2021年元旦放假通知</a></li>
<li class="announce_con"><a href="/IndexNewsInfo/321.html" target="_blank">关于升级优化日本仓的通知</a></li>
<li class="announce_con"><a href="/IndexNewsInfo/320.html" target="_blank">关于关闭德国仓的通知</a></li>
</ul>
<div style="float: right; display: block; width: 60px; margin-top: -20px;">
<a href="/IndexNewsList.html" style="text-decoration: underline; cursor: pointer">更多...</a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="choose">
<div class="bg_pic">
</div>
<div class="content">
<ul>
<li>
<div class="sever">
</div>
<div class="Sever">
                    服务贴心</div>
<p>
                    7*12小时专业在线客服</p>
<p>
                    呈现愉悦的海淘体验</p>
</li>
<li>
<div class="jisu">
</div>
<div class="Sever">
                    时效稳定</div>
<p>
                    免税州每天直飞</p>
<p>
                    多口岸多渠道</p>
</li>
<li>
<div class="qingguan">
</div>
<div class="Sever">
                    清关稳定</div>
<p>
                    阳光快速清关</p>
<p>
                    关税补贴可选</p>
</li>
<li>
<div class="price">
</div>
<div class="Sever">
                    价格实惠</div>
<p>
                    免体积费1磅就运</p>
<p>
                    实重收费 价格清晰</p>
</li>
<li>
<div class="anquan">
</div>
<div class="Sever">
                    安全放心</div>
<p>
                    多种保险理赔机制</p>
<p>
                    货物损失全额赔付</p>
</li>
<li>
<div class="gongkai">
</div>
<div class="Sever">
                    公开透明</div>
<p>
                    全程物流跟踪体系</p>
<p>
                    专业转运一单到底</p>
</li>
</ul>
</div>
</div>
<div class="details_bg">
<div class="details">
</div>
</div>
<div class="shopping">
<div class="shopp_bg">
</div>
<div class="step_left">
<ul>
<li>
<div class="step_pic">
<img src="/Statics/Images/NewWebsiteImg/jian_tou.png"/></div>
<div class="step_tit">
                    Step1</div>
<p>
                    注册八达网账号</p>
<p>
                    获取相应仓库地址</p>
</li>
<li>
<div class="step1_pic">
<img src="/Statics/Images/NewWebsiteImg/jian_tou.png"/></div>
<div class="step_tit">
                    Step2</div>
<p>
                    国外购物网站下单</p>
<p>
                    填写八达网海外仓地址</p>
</li>
<li>
<div class="step2_pic">
</div>
<div class="step_tit">
                    Step3</div>
<p>
                    国外网站发货后，在八达网</p>
<p>
                    填写预报，支付费用等待收货</p>
</li>
</ul>
</div>
<p class="view">
<a href="/HowToUse.html/28">查看详情 &gt;&gt;</a></p>
</div>
<div class="baDaActivity">
<div class="baDaActivity_bg">
</div>
<div class="baDaActivity_left">
<ul>
<li style="width: 380px; margin-right: 30px; margin-bottom:70px; float:left;"><a href="http://www.haitaobaicai.com/article?id=44 " rel="nofollow" target="_blank">
<img alt="八达网海淘转运公司优惠活动" src="https://statics.8dexpress.com/images/Banner/foot_banner1.jpg" style="max-width: 380px; border-radius:8px;"/></a></li>
<li style="width: 380px; margin-right: 30px; margin-bottom:70px; float:left;"><a href="https://www.rebatesme.com/promotions/newyearsneakerspecial?r=ref-newyearsneakerspecial-8x" rel="nofollow" target="_blank">
<img alt="八达网海淘转运公司优惠活动" src="https://statics.8dexpress.com/images/Banner/foot_banner2_20210111.jpg" style="max-width: 380px;  border-radius:8px;"/></a></li>
<li style="width: 380px; margin-bottom:70px; float:left;"><a href="http://www.haiinfo.com" target="_blank">
<img alt="八达网海淘转运公司优惠活动" src="https://statics.8dexpress.com/images/Banner/foot_banner3.jpg" style="max-width: 380px; border-radius:8px;"/></a></li>
</ul>
</div>
</div>
<div class="friendly_link">
<div class="friend_conten">
<div class="friend_bg">
</div>
<div class="friend_border">
<ul>
<li><a href="http://www.lmymd.com/" target="_blank">辣妈羊毛党</a></li>
<li><a href="http://www.9iyouhui.com/" target="_blank">久爱优惠海淘网</a></li>
<li><a href="http://www.zbzdm.com" target="_blank">值不值得买</a></li>
<li><a href="http://www.rebatesme.com/" target="_blank">RebatesMe</a></li>
<li><a href="http://www.123haitao.com/" target="_blank">极客海淘</a></li>
<li><a href="http://www.maishoudang.com/" target="_blank">买手党</a></li>
<li><a href="http://www.nbmai.com/" target="_blank">NB买海淘</a></li>
<li><a href="http://www.haitaolab.com/" target="_blank">海淘实验室</a></li>
<li><a href="http://www.walatao.com/" target="_blank">瓦拉淘</a></li>
<li><a href="http://www.nlzpy.com/" target="_blank">哪里最便宜</a></li>
<li><a href="http://bbs.gowu8.net" target="_blank">购物吧论坛</a></li>
<li><a href="http://www.mgpyh.com/" target="_blank">买个便宜货</a></li>
<li><a href="http://a.haitaozu.org/" target="_blank">美亚</a></li>
<li><a href="http://www.haiinfo.com/portal.php" target="_blank">海讯网</a></li>
<li><a href="https://www.xidibuy.com/" target="_blank">喜地商城</a></li>
<li><a href="http://www.qunlt.com/" target="_blank">去哪里淘</a></li>
<li><a href="http://www.yxeht.com/" target="_blank">洋小二海淘</a></li>
<li><a href="https://www.52by.com/" target="_blank">邦阅-外贸知识平台</a></li>
<li><a href="http://www.sllai.com/wangzhan.html" target="_blank">海淘网站</a></li>
<li><a href="https://www.360daigou.com" target="_blank">代购网</a></li>
<li><a href="http://www.shihuowang.com" target="_blank">免费试用</a></li>
<li><a href="http://www.zhuanyun.cc/" target="_blank">转运公司</a></li>
<li><a href="https://www.haitaohou.com/" target="_blank">海淘攻略</a></li>
<li><a href="http://www.qluu.com/" target="_blank">悠悠海淘</a></li>
</ul>
</div>
</div>
</div>
<script type="text/javascript">
    jQuery(".leftLoop").slide({ mainCell: ".bd ul", effect: "leftLoop", vis: 4, scroll: 3, autoPlay: true });
    jQuery(".slideBox").slide({ mainCell: ".bd ul", autoPlay: true });
    jQuery(".txtScroll-top").slide({ titCell: ".hd ul", mainCell: ".bd ul", autoPage: true, effect: "top", autoPlay: true, vis: 1 });

    function onClickNewsNext() {
        //$(".lastNews").load(location.href + " .lastNews");
        var result = false;
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Home/UpdateQuryNewsInformation',
            async: false,
            data: {
            },
            success: function (json) {
                if (json.success) {
                    var resString = json.data;
                    var resObj = JSON.parse(resString);

                    // 更新Tittle信息
                    var lastNews_Title_ContentHtml = '<div style="margin-top:50px;"><a style="font-size:24px; font-weight:900;" target="_blank" href="/IndexNewsInfo/' + resObj.NewsID + '.html">' + resObj.Titile + '</a></div>'
                    $(".lastNews_Title_Content").html(lastNews_Title_ContentHtml);
                    // 更新时间信息
                    var tempTime = resObj.CreateDate.slice(6, 19);
                    var cTime = new Date(parseInt(tempTime));
                    var cTimeStr = cTime.getFullYear() + '-' + (cTime.getMonth() + 1) + '-' + cTime.getDate();
                    var lastNews_Title_TimeHtlm = '<h1 style="font-size:20px; font-weight:800; text-align:center; color:white; text-anchor:middle; margin-top:15px;">' + cTimeStr + '</h1>'
                    $(".lastNews_Title_Time").html(lastNews_Title_TimeHtlm);
                    // 更新正文信息
                    $(".lastNews_Content").html(resObj.Content);

                    result = true;
                }
            },
            error: function () {
            }
        });
        return result;

    }

</script>
<!--中间结束-->
<!--底部开始-->
<div id="Footer">
<div class="about">
<div class="about_con">
<ul>
<li>
<p>
                                联系我们</p>
<p class="QQ">
<span>800066213</span></p>
</li>
<li>
<p>
                                市场合作</p>
<p>
                                tanlun@8d-edm.com</p>
</li>
<li>
<p>
                                投诉建议</p>
<p>
                                tousu@8dexpress.com</p>
</li>
</ul>
<div class="about_right" style="display:none;">
<div class="about_txt">
<img src="/Statics/Images/NewWebsiteImg/erweim.jpg"/>
<p>
                                关注八达网微信</p>
</div>
</div>
</div>
</div>
<div class="footer">
<div class="footer_con">
<div class="copy">
                        Copyright @2014 All Right Reserved. 八达网 <a href="http://www.8dexpress.com">海淘转运公司 </a>
<a href="http://www.beian.miit.gov.cn" target="_blank">网站备案/许可证号:沪ICP备18022437号-3</a>
</div>
<div class="cooperation">
<a href="/Partner.html">合作伙伴</a> | 
		            <a href="/ContactUs.html">联系我们</a> | 
		            <a href="/AboutUs.html">关于我们</a>
</div>
</div>
</div>
<!--底部结束-->
<!--浮动微信-->
<div class="fixd" id="tDragFixed">
<ul>
<li class="weixin">
<ul class="submenu">
<li class="bdwx"><img src="/Statics/Images/NewWebsiteImg/weixing_btn2.jpg"/></li>
</ul>
</li>
<li class="qq_pic">
</li>
<li class="weibo">
</li>
</ul>
</div>
<!--浮动微信-->
</div>
</div></body>
</html>
