<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	Invalid URL
</title><link href="https://fonts.googleapis.com/css?family=Roboto:100" rel="stylesheet"/>
<style>
        * {
            font-family: 'Roboto', sans-serif;
        }
        body {
            background: #333;
        }
        h1 {
            color: #ff7979;
        }
        p {
            font-size: 14pt;
            color: #f1f1f1;
        }
    </style>
</head>
<body>
<form action="InvalidUrlError.aspx" id="form1" method="post" style="text-align: center">
<input id="__VIEWSTATE" name="__VIEWSTATE" type="hidden" value="/wEPDwUKMTcwODU1NDQyM2Rkgd+972BitB3h06wYuRydqMMbOGQ="/>
<input id="__VIEWSTATEGENERATOR" name="__VIEWSTATEGENERATOR" type="hidden" value="B9C6A768"/>
<div style="max-width: 800px; margin: 50px auto;">
<h1>Oops. You may have used an outdated link or may have typed the address (URL) incorrectly.</h1>
<p>Please contact your system administrator or check the link you used.</p>
</div>
</form>
</body>
</html>
