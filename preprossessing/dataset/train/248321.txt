<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie ie9" lang="en-US">
<![endif]--><html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<title>Welcome | Bo'ness Community Council</title>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,800" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Audiowide" rel="stylesheet" type="text/css"/>
<!-- CSS -->
<link href="/assets/css/bootstrap.css" rel="stylesheet"/>
<link href="/assets/css/animate.css" rel="stylesheet"/>
<link href="/assets/css/simple-line-icons.css" rel="stylesheet"/>
<link href="/assets/css/font-awesome.min.css" rel="stylesheet"/>
<link href="/assets/css/miller.css" rel="stylesheet"/>
<link href="/assets/css/bcc.css?v=1.3.1" rel="stylesheet"/>
<link href="/assets/rs-plugin/css/settings.css" rel="stylesheet"/>
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-221081-54', 'auto');
	  ga('send', 'pageview');
	
	</script>
<link href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" rel="stylesheet" type="text/css"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#d9d9d9",
	      "text": "#5f7182"
	    },
	    "button": {
	      "background": "#48799d"
	    }
	  },
	  "theme": "classic",
	  "position": "bottom-right"
	})});
	</script>
<style>	
		.cc-window {
			z-index: 999999 !important;
		}
	</style>
</head>
<body>
<div class="sidebar-menu-container" id="sidebar-menu-container">
<div class="sidebar-menu-push">
<div class="sidebar-menu-overlay"></div>
<div class="sidebar-menu-inner">
<header class="site-header">
<div class="main-header header-sticky" id="main-header">
<div class="inner-header clearfix">
<div class="logo">
<a href="/home"><img src="/assets/images/BCC-logo1.png"/></a>
</div>
<div class="header-right-toggle pull-right hidden-md hidden-lg">
<a class="side-menu-button" href="javascript:void(0)"><i class="fa fa-bars"></i></a>
</div>
<nav class="main-navigation pull-right hidden-xs hidden-sm">
<ul>
<li><a href="/home">Home</a></li>
<li><a href="/news">News</a></li>
<li><a class="has-submenu" href="">Media</a>
<ul class="sub-menu">
<li><a href="/media/harcus-strachan">Harcus Strachan</a></li>
<li><a href="/media/mclaren-trophy">McLaren Trophy</a></li>
<li><a href="/media/armistice-2020">Remembrance 2020</a></li>
</ul>
</li>
<li><a class="has-submenu" href="/meetings">Meetings</a>
<ul class="sub-menu">
<li><a href="/meetings/minutes">Minutes Archive</a></li>
</ul>
</li>
<li><a class="has-submenu" href="/about/what-we-do">About Us</a>
<ul class="sub-menu">
<li><a href="/about/constitution">Constitution</a></li>
<li><a href="/about/what-we-do">What We Do</a></li>
<li><a href="/about/what-we-do#members">Our Members</a></li>
</ul>
</li>
<li><a href="/contact">Contact</a></li>
<li class="social"><div><a href="https://www.twitter.com/bonesscc" target="_blank"><i class="fa fa-twitter"></i></a> <a href="https://www.facebook.com/bonesscc" target="_blank"><i class="fa fa-facebook"></i></a></div></li>
</ul>
</nav>
</div>
</div>
</header>
<div class="slider">
<div class="fullwidthbanner-container">
<div class="fullwidthbanner">
<ul>
<li class="first-slide" data-masterspeed="300" data-slotamount="10" data-transition="fade">
<img alt="slide" data-fullwidthcentering="on" src="/assets/uploads/images/ID2.jpg"/>
</li>
<li class="first-slide" data-masterspeed="300" data-slotamount="10" data-transition="fade">
<img alt="slide" data-fullwidthcentering="on" src="/assets/uploads/images/ID1.jpg"/>
</li>
<li class="first-slide" data-masterspeed="300" data-slotamount="10" data-transition="fade">
<img alt="slide" data-fullwidthcentering="on" src="/assets/uploads/images/ID4.jpg"/>
</li>
<li class="first-slide" data-masterspeed="300" data-slotamount="10" data-transition="fade">
<img alt="slide" data-fullwidthcentering="on" src="/assets/uploads/images/ID3.jpg"/>
</li>
</ul>
<!--<div class="copy-id"><img src="/assets/images/copy-id.png" /></div>-->
</div>
</div>
</div>
<section class="action">
<div class="container">
<div class="row">
<div class="col-xs-9 col-sm-8 col-md-9">
<h5>No public CC meetings during COVID-19 pandemic</h5>
<p>You can still send comments or questions for discussion in advance of our meetings.</p>
</div>
<div class="col-xs-12 col-sm-4 col-md-3">
<a class="btn btn-large" href="mailto:info@bonesscc.org.uk">Email Us</a>
</div>
</div>
</div>
</section>
<section class="">
<div class="container">
<div class="row">
<div class="col-md-5">
<h4>Welcome to our website.</h4>
<p>We represent the views of local people to Falkirk Council and other public bodies. We meet on the <strong>second Wednesday of every month</strong> in the meeting rooms above Bo'ness Library; normally starting at <strong>7pm</strong>, and members of the public are always welcome to attend.</p>
<p>On this website you can find out about the role of the Community Council, read the latest minutes, and find out more about our members; as well as some useful links to other local sites which may be of interest to you.</p>
<!--<p><strong>Our new website is still under construction, however the majority of news and announcements can still be found on our <a href="http://fb.me/bonesscc">Facebook Page</a>.</strong></p>-->
<p>If you wish to contact Bo'ness Community Council please email <a href="mailto:info@bonesscc.org.uk">info@bonesscc.org.uk</a>.</p>
<p>
<a class="btn btn-large" href="/news">News</a>   <a class="btn btn-large" href="/meetings">Meetings</a>   <a class="btn btn-large" href="/contact">Contact</a>
</p>
<p> </p>
<h3 class="meeting_header">Upcoming Meetings</h3>
<br/>
<p>Our meetings usually take place on the second Wednesday of the month; to which members of the public are welcome.  Our next meetings take place on:</p>
<ul>
<script>
				function print(s) {
				  var day = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
				  var month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
				  document.write('<li>' + day[s.getDay()] + ' ' + s.getDate() + ' ' + month[s.getMonth()] + ' ' + s.getFullYear() + ', 7:00pm</li>');
				}
				
				function nthWeekdayOfMonth(weekday, n, date) {
				  var count = 0,
				      idate = new Date(date.getFullYear(), date.getMonth(), 1);
				  while (true) {
				    if (idate.getDay() === weekday) {
				      if (++count == n) {break;}
				    }
				    idate.setDate(idate.getDate() + 1);
				  }
				  return idate;
		
				}
				
				// Second Wednesday of the current month.
				var date = new Date();
				print(date = nthWeekdayOfMonth(3, 2, date)); 
				// Second Wednesday of next month.
				date.setMonth(date.getMonth() + 1);
				print(date = nthWeekdayOfMonth(3, 2, date));
				// Second Wednesday of the month after.
				date.setMonth(date.getMonth() + 1);
				print(date = nthWeekdayOfMonth(3, 2, date));
				</script>
</ul>
<p> </p>
<h3 class="meeting_header">Latest Meeting Documents <span style="font-size: 14px;">(9th Dec 2020)</span></h3>
<table class="minutes">
<tr>
<td>
<span class="download">
<a class="btn" href="/assets/uploads/docs/Agenda_9th_December_2020..pdf"><i class="fa fa-arrow-down"></i></a>
						 Agenda
						</span>
</td>
<td>
<span class="status approved">Approved</span>
</td>
<td>
</td>
</tr>
</table>
</div>
<div class="col-md-6 col-md-offset-1">
<article class="news row">
<div class="col-xs-4">
<a href="/news/falkirk-council-meetings">
<img src="/assets/uploads/images/_news-feat/Screenshot_2020-10-14_at_20.34.12.png"/>
</a>
</div>
<div class="col-xs-8">
<h2><a href="/news/falkirk-council-meetings">Falkirk Council Meetings</a> <span class="sub"><i class="fa fa-calendar-plus-o"></i> 14 Oct 2020</span></h2>
<p>Falkirk Council continue to meet during COVID, members of the public have previously been able to attend meetings but due to COVID 19 restrictions meetings have been unable to take place…
				<a href="/news/falkirk-council-meetings">read more</a></p>
</div>
</article>
<article class="news row">
<div class="col-xs-4">
<a href="/news/council-urged-to-listen-to-boness-community-amid-row-over-property-review-consultation">
<img src="/assets/uploads/images/_news-feat/image_1.jpg"/>
</a>
</div>
<div class="col-xs-8">
<h2><a href="/news/council-urged-to-listen-to-boness-community-amid-row-over-property-review-consultation">Council urged to “listen” to Bo’ness community amid row over property review consultation</a> <span class="sub"><i class="fa fa-calendar-plus-o"></i> 31 Aug 2020</span></h2>
<p>Bo’Ness Community Council has called on Falkirk Council to review its working practices and “listen” to local residents.

The Bo’ness group spoke out after it asked FC officials to meet with it…
				<a href="/news/council-urged-to-listen-to-boness-community-amid-row-over-property-review-consultation">read more</a></p>
</div>
</article>
<article class="news row">
<div class="col-xs-4">
<a href="/news/covid-19-recovery-community-engagement-survey">
<img src="/assets/images/sinemetu-ph.jpg"/>
</a>
</div>
<div class="col-xs-8">
<h2><a href="/news/covid-19-recovery-community-engagement-survey">COVID-19 Recovery: Community Engagement Survey</a> <span class="sub"><i class="fa fa-calendar-plus-o"></i> 07 Aug 2020</span></h2>
<p>Falkirk Council want to work in partnership with our community in developing their recovery plan, which will be part of the Councils 5-year business plan.

To do this Falkirk Council need to…
				<a href="/news/covid-19-recovery-community-engagement-survey">read more</a></p>
</div>
</article>
</div>
</div>
</div>
</section>
<footer>
<div class="sub-footer">
<p>2021 © Bo'ness Community Council.   Website hosted by: <a href="http://www.sanctusmedia.com">Sanctus Media Ltd</a></p>
</div>
</footer>
<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
</div>
</div>
<nav class="sidebar-menu slide-from-left">
<div class="nano">
<div class="content">
<nav class="responsive-menu">
<ul>
<li><a href="/home/index_dev">Home</a></li>
<li class="menu-item-has-children"><a href="#">About Us</a>
<ul class="sub-menu">
<li><a href="/about/constitution">Constitution</a></li>
<li><a href="/about/what-we-do">What We Do</a></li>
<li><a href="/about/what-we-do#members">Our Members</a></li>
</ul>
</li>
<li class="menu-item-has-children"><a href="#">Meetings</a>
<ul class="sub-menu">
<li><a href="/meetings#minutes">Minutes</a></li>
</ul>
</li>
<li><a href="/about/mclaren-trophy">McLaren Trophy</a></li>
<li><a href="/contact">Contact</a></li>
</ul>
</nav>
</div>
</div>
</nav>
</div>
<script src="/assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script src="/assets/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="/assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="/assets/js/plugins.js" type="text/javascript"></script>
<script src="/assets/js/custom.js" type="text/javascript"></script>
</body>
</html>