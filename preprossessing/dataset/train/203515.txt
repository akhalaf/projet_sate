<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "78818",
      cRay: "6110801dfb51198c",
      cHash: "591e4ca280cbc52",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuY2FsaWZvcm5pYWJlYWNoZXMuY29tLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "3vHa84mHGycE0Q4Fx3GzyYJNaZEuEWH1oCIUQ/YnsTAmfzEGIr/T3qDvR2kYr1iZ6IrBwk6LRJSE84mfSYsKH95vGy+va6DW5fLj5GtqVTHIBE4y55BZpYBvDVdBlbpXulxitzNqlf+YhTcYNh13exnP8SVa1X4KOfqNDUVy3kVOJYc45iMG0RK+gjmNkjcoMfbUSkLcrPNKL0UepmNE5BUizq3vSEz1biWg2Jm1eM7jhIwF8i+zRFRdnjP8CwcdhBSAa+W94Tp0WMoiGGerQpRchZkpXuFNfPWUH58Tx8U8vFf4EGdDb4BXvRmQaDuNGXCzPJKewdIp7rzjB0PC2lHOwzGF8oFUkrszfKaAAyQPR+y0jA+Ihw1t+x+uEvzrLQ7QGdPRNvPDDNsEmN+MAL7NwYkaOGPyQqrJY+aW+XFz5NKmUlY8EbVXAoqqSzEa9zLq0XteuC2hmKMAdXnxmXw9nfj6+5+MMX4Ty8QL6woThGmSxldzfGq+L+ajZ1iNAjgVXGy5kzNbeeFy8z9CE+W+X3DBV3m4iWOB/iUuBN79uTvdK82K4f7XIo2F/3EA/DVtfG7hWUuApjzD6HfRIHwouRpUcv2QVQ3hFJWeP5hprnTIzb6Kmsr+ekpS2s+pYK8/i47SLQ6oiJSrfG/oz9Oz9wf/IAstyU2+gvV3TX+raguSthDgrLMwPGJLZ+hBHajQeZEbGLcvw7f0wFYoRTYifp61cgpdQMAnY/nT8VBU9U/JlrZ3kT6f4CmBAhs5",
        t: "MTYxMDU1NTQxMi4xNjEwMDA=",
        m: "GfJFPS8MaXNmKkRhdukRUIYbtYUj0gG82QEK682fSho=",
        i1: "UR8sVEYWDceEkpsnRTLSSA==",
        i2: "3mjHdQ8GecEciSNI7jkmwQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "MO0dV88updyJRe38MLNudPbYbUmrnR2k1jGkljkVG24=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=6110801dfb51198c");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> californiabeaches.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=e6cf2bff5d489428e037bd58ae082847a5c7c61a-1610555412-0-AZKbT88j-vwGhP2esBrRM60eEOKygtEyGiLGpkDueARzVLYRViDcdfsT4TSTbJoGRhI3LtC0z7jodKYigIchmAsYWLigns_s1NM9wDrQ1qTgCiM4odVxXlsJunmxGC30MpYZhVXCPJTDv3u4ufGuibyuuN_xYyp7Kpwxl60UnErrArblnody8gmdeamBSwu1KqTZcoNw5354DOW3dyQYYzsKkyxIwhho_v8zZp89ms_TsWCSwftnn12LBvVsNtAnLnuUl5SRx0NphP_rrYPkLHvxdO5kZbmAxxnQGNl67s14lagfxuBPWG05T_R_7QfiAA" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="2c9b3eeb0c9f3bdbc428eecc17e1061fae3e9c4d-1610555412-0-Aci90ApP27nae5jLi/3WT1j4ZxEOH7ZI0J3Svn3uJc+BuDZFd8GNMW0vkZOUUUTYk3WhJdqLFjBKWYzilkbdVue77PL8vbPrDF3s7ADCBoIWJPsursm5xaYhSHXf2bgfAAiXAET/I85MYX7ZztmyOD0NaiQ7z5EGN5JoU/KBwUNiBCUi6TAJ3TiBYieN0ObaWUX6qcqxjRfj+qFRk3u2A1VplVYscSKhMbyM3zumYZwxq7BBQrrvbh4lwNR39fy4WfbaIDYNqeVhzkmLFnHvDKkndL+eADkiwJffdFXOnpGCp7zi2Fp5Yj/S3XULj2KDTvZ4aBkq/voY3fXsSYuGNRNMmuEEFbTvdHEurULv3byEzW97jTY3q8ariajDPVqrE+efLu1znApR3wgUErAUhDab2u3cdsBDvoFG90p/J/BhFRjAg8GdjalAP5wHR7ta0T19D47V7vH/VeP7y83lTUBingMybS76WG6l69jBAYbScUyikseEfVM5do5pALxMNovfVMUODxaC+XlN9qEbgHnj9xqYXXEhu4utI5Hx+NF0WAnj9AMC2Yg7uOsDpo2SE1llw/KzOGXt46HVzO2uzhAZL88BaTwMlu18FtLapugjiS27HPv68DEWusKiPRCphntiZ2dXaI6r28in3RRxrJrhtp5enQkhsMujeZEpEreCB8Iv6z0LwlQGrVzExXqznH7771rjrpWBb2yx+g+qyBS6Qf7RuyM/wb4R3EPuY8bB2FHCiDG/1yjcHNq83dk7r2gPKFs3uRvFomCuW9UmnmOwoG4x8+TXmQiIAYKOImRxoWcyYoCarov2sbGhdP99IaCx2Zkfue687qcHvmAr3GIXh4z40OWtKlnr5wK6jaXaCerivFaWbhPKBr2Z1s2omrxwe1aLyVJlKRyYvt/TDTSL0B0dbNQBlpuW6dnL8KWZ6Ct8+2EuRpRNlAiKtcss6/b4Mxudy8eZ8jHWaV+VsIWTuRYD5gsOnl6B4fvp/z4bFZWeQhEfktRY+je1+BiYJdbKzythbnz2cVOODkpykGBuVudVKqlzQKsRyKdj20IT+M4cJBFr/4xJwnRc9X77kja7AgI12tIpDiBYRYKiRDfbDYf3fgKNWNfnGDxgOifz1WnQusG0bMpe1IVFXsoTdp4qq8yn0uLcVDWNWSOseTrhH91ulpuF3Vnz+bIz9tEsab839KsZfak7suP6F9r9neo8wuz4W5gD2YL+mjOWj/1HlPWoeh5Ime3QIuDzq7UdroQBQFpxSozhZsI018diHE8e37qLc4Q4fcBHngYYBwxQx/DvCIcW9ya6d92jkUJDdFw8NpcpzZleQIwrdCCR4A=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="5e9c6709fc947bde579fcbac75e7dca2"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610555416.161-fN9Ti7g7Zg"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=6110801dfb51198c')"> </div>
</div>
<a href="https://tornado-networks.com/lupineabbey.php?goto=465" style="display: none;">table</a>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>6110801dfb51198c</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
