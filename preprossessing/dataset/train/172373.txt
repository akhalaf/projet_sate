<!DOCTYPE html>
<html lang="en">
<head>
<title>Outgoing authenticated SMTP server and email relay service (solves most SMTP and email relay errors).</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta charset="utf-8"/>
<meta content="noodp" name="robots"/>
<meta content="GetOnline - AuthSMTP" name="author"/>
<link href="https://www.authsmtp.com/images/favicon/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="https://www.authsmtp.com/images/favicon/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="https://www.authsmtp.com/images/favicon/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="https://www.authsmtp.com/images/favicon/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="https://www.authsmtp.com/images/favicon/apple-touch-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="https://www.authsmtp.com/images/favicon/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="https://www.authsmtp.com/images/favicon/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="https://www.authsmtp.com/images/favicon/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="https://www.authsmtp.com/images/favicon/favicon-196x196.png" rel="icon" sizes="196x196" type="image/png"/>
<link href="https://www.authsmtp.com/images/favicon/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="https://www.authsmtp.com/images/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="https://www.authsmtp.com/images/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="https://www.authsmtp.com/images/favicon/favicon-128.png" rel="icon" sizes="128x128" type="image/png"/>
<meta content="AuthSMTP" name="application-name"/>
<meta content="#000000" name="msapplication-TileColor"/>
<meta content="https://www.authsmtp.com/images/favicon/mstile-144x144.png" name="msapplication-TileImage"/>
<meta content="https://www.authsmtp.com/images/favicon/mstile-70x70.png" name="msapplication-square70x70logo"/>
<meta content="https://www.authsmtp.com/images/favicon/mstile-150x150.png" name="msapplication-square150x150logo"/>
<meta content="https://www.authsmtp.com/images/favicon/mstile-310x150.png" name="msapplication-wide310x150logo"/>
<meta content="https://www.authsmtp.com/images/favicon/mstile-310x310.png" name="msapplication-square310x310logo"/>
<!--[if lt IE 9]>

	<script src="/js/html5shiv.js"></script>

	<![endif]-->
<!-- CSS Files

    ================================================== -->
<link href="/css/main.css" id="main-css" rel="stylesheet" type="text/css"/>
<link href="/css/as-custom.css" id="as-custom" rel="stylesheet" type="text/css"/>
<!-- Javascript Files

    ================================================== -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.isotope.min.js"></script>
<!--<script src="/js/jquery.prettyPhoto.js"></script>
-->
<script src="/js/easing.js"></script>
<script src="/js/jquery.ui.totop.js"></script>
<script src="/js/selectnav.js"></script>
<script src="/js/ender.js"></script>
<script src="/js/jquery.lazyload.js"></script>
<script src="/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="/js/custom.js"></script>
<script src="/js/as-custom.js"></script>
<script src="/js/breakpoint.js"></script>
<script src="/js/rev-setting-1.js" type="text/javascript"></script>
<script src="/js/as-home.js" type="text/javascript"></script>
<script src="/js/jquery.flexslider-min.js" type="text/javascript"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" rel="stylesheet" type="text/css"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script>
		window.addEventListener("load", function(){
		window.cookieconsent.initialise({
		  "palette": {
		    "popup": {
		      "background": "#000000",
		      "text": "#ffffff"
		    },
		    "button": {
		      "background": "#409cff",
		      "text": "#ffffff"
		    }
		  },
		  "content": {
		    "message": "This website uses cookies - please review our cookie &amp; privacy policies before proceeding",
		    "dismiss": "Accept",
		    "link": "Cookie Policy",
		    "href": "/documentation/cookie-policy.html"
		    
		  }
		})});
	</script>
<!-- Hotjar Tracking Code for www.authsmtp.com -->
<!-- <script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:1393551,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script> -->
</head>
<body>
<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-63175-1', 'auto');
		ga('send', 'pageview');
	</script>
<noscript><p><b>IMPORTANT</b>: JavaScript is required to signup for and use AuthSMTP, please enable JavaScript in your web browser and refresh the page.</p></noscript>
<div id="wrapper">
<!-- header begin -->
<header>
<div class="container">
<div id="logo">
<div class="inner">
<a href="/">
<img alt="logo" src="/images/as-logo2.png"/></a>
</div>
</div>
<span class="menu-btn"></span>
<!-- main menu begin -->
<div class="de_menu">
<div class="mainmenu" id="mainmenu">
<ul>
<li><a href="/features.php">Features</a></li>
<li><a href="/auth-smtp/pricing.php">Pricing</a></li>
<li><a href="/support/">Support</a>
<ul>
<li><a href="/status/">Service Status</a></li>
<li><a href="/support/">Help &amp; Support</a></li>
<li><a href="/support/getting-started-guide.html">Getting Started</a></li>
<li><a href="/auth-smtp/how-to-setup.html">Setup Guides</a></li>
<li><a href="/search/">Knowledge Base</a></li>
<li><a href="/support/troubleshooting.html">Troubleshooting</a></li>
<li><a href="/api/">API</a></li>
</ul>
</li>
<li><a href="/contact/">Contact</a>
<ul>
<li><a href="/login/">I have an account</a></li>
<li><a href="/contact/general.html">I do not have an account</a></li>
</ul>
</li>
<li><a href="/login/"><span class="glyphicon glyphicon-lock"></span> Login</a></li>
</ul>
</div>
<!-- close 'mainmenu' -->
</div>
<!-- close 'de_menu' -->
</div>
<!-- close 'container' -->
</header>
<!-- page header close -->
<!-- slider -->
<div class="fullwidthbanner-container hidden-xs">
<div id="revolution-slider">
<ul>
<li data-masterspeed="300" data-slotamount="10" data-thumb="images-slider/thumbs/thumb1.jpg" data-transition="fade">
<!--  BACKGROUND IMAGE -->
<img alt="" src="images-slider/curvature_of_earth.jpg"/>
<div class="tp-caption big-white sfb" data-easing="easeOutExpo" data-speed="300" data-start="800" data-x="0" data-y="100">
                            Global Outgoing SMTP Email Service
                        </div>
<div class="tp-caption med-white sfb" data-easing="easeOutExpo" data-speed="300" data-start="1000" data-x="0" data-y="150">
                            Delivering what is important to you
                        </div>
<div class="tp-caption modern_small_text_dark sfl" data-easing="easeOutExpo" data-speed="300" data-start="1200" data-x="0" data-y="200">
<i class="fa fa-check fa-green"></i>Professional outgoing SMTP email service
                        </div>
<div class="tp-caption modern_small_text_dark sfl" data-easing="easeOutExpo" data-speed="300" data-start="1400" data-x="0" data-y="230">
<i class="fa fa-check fa-green"></i>Instant setup - start sending email in minutes
                        </div>
<div class="tp-caption modern_small_text_dark sfl" data-easing="easeOutExpo" data-speed="300" data-start="1600" data-x="0" data-y="260">
<i class="fa fa-check fa-green"></i>Responsive &amp; dedicated support
                        </div>
<div class="tp-caption modern_small_text_dark sfl" data-easing="easeOutExpo" data-speed="300" data-start="1800" data-x="0" data-y="290">
<i class="fa fa-check fa-green"></i>Flexible &amp; scalable packages
                        </div>
<div class="tp-caption modern_small_text_dark sfr" data-easing="easeOutExpo" data-speed="300" data-start="2000" data-x="0" data-y="330">
<a class="btn btn-large btn-success" href="/features.php">View Features</a>
</div>
</li>
<li data-masterspeed="300" data-slotamount="10" data-thumb="images-slider/thumbs/thumb1.jpg" data-transition="fade">
<!--  BACKGROUND IMAGE -->
<img alt="" src="images-slider/fibre-optics-lic.jpg"/>
<div class="tp-caption big-white sfb" data-easing="easeOutExpo" data-speed="300" data-start="800" data-x="0" data-y="100"><strong>Availability</strong>
</div>
<div class="tp-caption med-white sfb" data-easing="easeOutExpo" data-speed="300" data-start="1000" data-x="0" data-y="150">Robust &amp; reliable network custom built for SMTP delivery
                        </div>
<div class="tp-caption small_text blue_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="1200" data-x="0" data-y="200">
<i class="fa fa-check fa-green"></i>High availability and redundancy
                        </div>
<div class="tp-caption small_text blue_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="1400" data-x="0" data-y="230">
<i class="fa fa-check fa-green"></i>Multiple Tier-1 network connections
                        </div>
<div class="tp-caption small_text blue_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="1600" data-x="0" data-y="260">
<i class="fa fa-check fa-green"></i>High performance but planet friendly hardware
                        </div>
<div class="tp-caption small_text blue_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="1800" data-x="0" data-y="290">
<i class="fa fa-check fa-green"></i>Round-the-clock remote monitoring with on-call engineers
                        </div>
<div class="tp-caption small_text sfr" data-easing="easeOutExpo" data-speed="300" data-start="2000" data-x="0" data-y="330">
<a class="btn btn-large btn-success" href="/features.php">View Features</a>
</div>
</li>
<li data-masterspeed="300" data-slotamount="10" data-thumb="images-slider/thumbs/thumb1.jpg" data-transition="fade">
<!--  BACKGROUND IMAGE -->
<img alt="" src="images-slider/straws.jpg"/>
<div class="tp-caption big-white sfb" data-easing="easeOutExpo" data-speed="300" data-start="800" data-x="0" data-y="100">
<strong>Flexibility</strong>
</div>
<div class="tp-caption med-white sfb" data-easing="easeOutExpo" data-speed="300" data-start="1000" data-x="0" data-y="150">
                            Everything you need from an SMTP Service
                        </div>
<div class="tp-caption small_text blue_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="1200" data-x="0" data-y="200">
<i class="fa fa-check"></i>Low cost starter accounts
                        </div>
<div class="tp-caption small_text blue_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="1400" data-x="0" data-y="230">
<i class="fa fa-check"></i>Flexible upgrade options
                        </div>
<div class="tp-caption small_text blue_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="1600" data-x="0" data-y="260">
<i class="fa fa-check"></i>Control panel management
                        </div>
<div class="tp-caption small_text blue_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="1800" data-x="0" data-y="290">
<i class="fa fa-check"></i>Responsive support
                        </div>
<div class="tp-caption small_text blue_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="2000" data-x="0" data-y="320">
<i class="fa fa-check"></i>...and much more!
                        </div>
<div class="tp-caption small_text sfr" data-easing="easeOutExpo" data-speed="300" data-start="2200" data-x="0" data-y="350">
<a class="btn btn-large btn-success" href="/features.php">View Features</a>
</div>
</li>
<li data-masterspeed="300" data-slotamount="10" data-thumb="images-slider/thumbs/thumb1.jpg" data-transition="fade">
<!--  BACKGROUND IMAGE -->
<img alt="" src="images-slider/taipei-postboxes.jpg"/>
<div class="tp-caption big-white sfb" data-easing="easeOutExpo" data-speed="300" data-start="800" data-x="0" data-y="100">
<strong>Deliverability</strong>
</div>
<div class="tp-caption med-white sfb" data-easing="easeOutExpo" data-speed="300" data-start="1000" data-x="0" data-y="150">
                            Over 10 years email delivery expertise
                        </div>
<div class="tp-caption small_text sfl" data-easing="easeOutExpo" data-speed="300" data-start="1200" data-x="0" data-y="200">
<i class="fa fa-check"></i>Reputation monitoring
                        </div>
<div class="tp-caption small_text sfl" data-easing="easeOutExpo" data-speed="300" data-start="1400" data-x="0" data-y="230">
<i class="fa fa-check"></i>High deliverability rates
                        </div>
<div class="tp-caption small_text sfl" data-easing="easeOutExpo" data-speed="300" data-start="1600" data-x="0" data-y="260">
<i class="fa fa-check"></i>SPF / DKIM support
                        </div>
<div class="tp-caption small_text sfl" data-easing="easeOutExpo" data-speed="300" data-start="1800" data-x="0" data-y="290">
<i class="fa fa-check"></i>Dedicated IP address options
                        </div>
<div class="tp-caption small_text sfl" data-easing="easeOutExpo" data-speed="300" data-start="2000" data-x="0" data-y="320">
<i class="fa fa-check"></i>...and much more!
                        </div>
<div class="tp-caption small_text sfr" data-easing="easeOutExpo" data-speed="300" data-start="2200" data-x="0" data-y="350">
<a class="btn btn-large btn-success" href="/features.php">View Features</a>
</div>
</li>
<li data-masterspeed="300" data-slotamount="10" data-thumb="images-slider/thumbs/thumb1.jpg" data-transition="fade">
<!--  BACKGROUND IMAGE -->
<img alt="" src="images-slider/digital-padlock.jpg"/>
<div class="tp-caption big-white sfb" data-easing="easeOutExpo" data-speed="300" data-start="800" data-x="0" data-y="100">
<strong>Security</strong>
</div>
<div class="tp-caption med-white sfb" data-easing="easeOutExpo" data-speed="300" data-start="1000" data-x="0" data-y="150">
                            Send email with peace of mind
                        </div>
<div class="tp-caption small_text blue_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="1200" data-x="0" data-y="200">
<i class="fa fa-check fa-green"></i>SMTP authentication
                        </div>
<div class="tp-caption small_text blue_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="1400" data-x="0" data-y="230">
<i class="fa fa-check fa-green"></i>SSL / TLS SMTP encryption
                        </div>
<!-- <div class="tp-caption small_text blue_shadow sfl"
                            data-x="0"
                            data-y="260"
                            data-speed="300"
                            data-start="1600"
                            data-easing="easeOutExpo">
                            <i class="fa fa-check fa-green"></i>HTTPS control panel
                        </div> -->
<div class="tp-caption small_text blue_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="1600" data-x="0" data-y="260">
<i class="fa fa-check fa-green"></i>Strict sender address policy
                        </div>
<div class="tp-caption small_text blue_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="1800" data-x="0" data-y="290">
<i class="fa fa-check fa-green"></i>...and much more!
                        </div>
<div class="tp-caption small_text sfr" data-easing="easeOutExpo" data-speed="300" data-start="2000" data-x="0" data-y="320">
<a class="btn btn-large btn-success" href="/features.php">View Features</a>
</div>
</li>
<li data-masterspeed="300" data-slotamount="10" data-thumb="images-slider/thumbs/thumb1.jpg" data-transition="fade">
<!--  BACKGROUND IMAGE -->
<img alt="" src="images-slider/bridge.jpg"/>
<div class="tp-caption big-white sfb" data-easing="easeOutExpo" data-speed="300" data-start="800" data-x="0" data-y="100">
<strong>Customer Support</strong>
</div>
<div class="tp-caption med-white sfb" data-easing="easeOutExpo" data-speed="300" data-start="1000" data-x="0" data-y="150">
                            Providing you with the support you need
                        </div>
<div class="tp-caption small_text sfl" data-easing="easeOutExpo" data-speed="300" data-start="1200" data-x="0" data-y="200">
<i class="fa fa-check"></i>Responsive web &amp; email support
                        </div>
<div class="tp-caption small_text red_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="1400" data-x="0" data-y="230">
<i class="fa fa-check"></i>Online account management
                        </div>
<div class="tp-caption small_text red_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="1600" data-x="0" data-y="260">
<i class="fa fa-check"></i>Extensive setup guides &amp; knowledge base
                        </div>
<div class="tp-caption small_text red_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="1800" data-x="0" data-y="290">
<i class="fa fa-check"></i>10+ years experience
                        </div>
<div class="tp-caption small_text red_shadow sfl" data-easing="easeOutExpo" data-speed="300" data-start="2000" data-x="0" data-y="320">
<i class="fa fa-check"></i>...and much more!
                        </div>
<div class="tp-caption small_text sfr" data-easing="easeOutExpo" data-speed="300" data-start="2200" data-x="0" data-y="350">
<a class="btn btn-large btn-success" href="/features.php">View Features</a>
</div>
</li>
</ul>
</div>
<p class="imagecredit"><a href="/documentation/copyright.html">IMAGE CREDIT</a></p>
</div>
<!-- slider close -->
<!-- replacement for call-to-action and banner on mobiles screen size -->
<div class="no-bottom indextitle">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<h1>Global Outgoing SMTP Email Service</h1>
<p>AuthSMTP is the outgoing SMTP email service for your e-commerce website, mailing list or email application on most current computers and mobile devices. With instant setup, a money back guarantee and very low cost starter accounts you can test and start sending email in minutes with no risk.</p>
</div>
</div>
</div>
</div>
<!-- content begin -->
<div class="no-bottom index" id="content">
<div class="container">
<div class="row">
<!-- feature box begin -->
<div class="feature-box-small-icon col-md-4">
<div class="inner">
<i class="glyphicon glyphicon-globe circle"></i>
<div class="text">
<h2>Availability</h2>
                                Our systems are custom designed / built for reliability and performance when processing and delivering very high volumes of email. We use high performance and redundant systems to help ensure we can provide the highest levels of availability.
                            </div>
</div>
</div>
<!-- feature box close -->
<!-- feature box begin -->
<div class="feature-box-small-icon col-md-4">
<div class="inner">
<i class="glyphicon glyphicon-align-left circle"></i>
<div class="text">
<h2>Flexible Pricing</h2>
                                AuthSMTP accounts start from as little as $36 / £29 / €32 per year for up to 1000 messages per month and you can upgrade your account instantly via our Control Panel. For full details see our Pricing Calculator.
                            </div>
</div>
</div>
<!-- feature box close -->
<!-- feature box begin -->
<div class="feature-box-small-icon col-md-4">
<div class="inner">
<i class="fa fa-medkit"></i>
<div class="text">
<h2>Responsive Support</h2>
                                We provide an online Control Panel plus email and web based support backed by knowledgable support staff on hand to provide help if required.
                            </div>
</div>
</div>
<!-- feature box close -->
<div class="spacer-single"></div>
<!-- feature box begin -->
<div class="feature-box-small-icon col-md-4">
<div class="inner">
<i class="glyphicon glyphicon-inbox circle"></i>
<div class="text">
<h2>Deliverability</h2>
                                We have over 10 years experience providing email services and getting emails delivered quickly and accurately to your recipients.
                            </div>
</div>
</div>
<!-- feature box close -->
<!-- feature box begin -->
<div class="feature-box-small-icon col-md-4">
<div class="inner">
<i class="glyphicon glyphicon-lock circle"></i>
<div class="text">
<h2>Security</h2>
                                We understand security is important to our customers - we provide a number of security features including SSL options when sending SMTP email.
                            </div>
</div>
</div>
<!-- feature box close -->
<!-- feature box begin -->
<div class="feature-box-small-icon col-md-4">
<div class="inner">
<!-- <i class="glyphicon glyphicon-wrench circle"></i> -->
<i class="fa fa-clock-o"></i>
<div class="text">
<h2>Instant Setup</h2>
                                Accounts are created instantly when you sign up so with our setup guides you can start sending email within minutes.
                            </div>
</div>
</div>
<!-- feature box close -->
</div>
<div class="row">
<hr/>
<!-- feature box begin -->
<div class="col-md-4">
<div class="inner"></div>
</div>
<div class="col-md-4">
<div class="inner">
<div class="text">
<a class="center-block btn btn-extra-large btn-success" href="/features.php">View Features</a>
</div>
</div>
</div>
<div class="col-md-4">
<div class="inner"></div>
</div>
<!-- feature box close -->
</div>
</div>
<!-- content close -->
<!-- testimonials start -->
<section data-speed="8" data-type="background" id="testimonial-full">
<div class="container">
<div class="row">
<h2>What our customers think...</h2>
<div class="flexslider testi-slider">
<ul class="slides">
<li>
<blockquote><a href="/auth-smtp/testimonials.php">"Thank you, that's FANTASTIC! - If ever there was a business that 'does exactly what it says on the tin' - then it would be AuthSMTP. Well done - very impressed!."<span>S. Loughridge</span></a></blockquote>
</li>
<li>
<blockquote><a href="/auth-smtp/testimonials.php">"No problem just thought I would let you know that your product works very well indeed. The support website is a pleasure to use. Keep up the good service."<span>John G.</span></a></blockquote>
</li>
<li>
<blockquote><a href="/auth-smtp/testimonials.php">"As always a very rapid, comprehensive and helpful response. Thank you very much for your support and providing an excellent flexible and value for money service."<span>J. Bancroft</span></a></blockquote>
</li>
</ul>
</div>
</div>
</div>
</section>
<!-- testimonials end -->
</div>
<!-- content close -->
<!-- call to action -->
<div class="call-to-action">
<div class="container">
<div class="row">
<div class="col-md-9">
<h3>Not sure? We offer a <span class="id-color"><a href="/auth-smtp/money-back-guarantee.html"> money back guarantee</a></span> on all new accounts.</h3>
</div>
<div class="col-md-3">
<a class="btn btn-extra-large btn-success" href="/auth-smtp/pricing.php">Sign up with no risk</a>
</div>
</div>
</div>
</div>
<!-- call to action close -->
<!-- footer begin -->
<footer>
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
<div class="widget widget_recent_post">
<h3>The Service</h3>
<ul>
<li><a href="/">Home</a></li>
<li><a href="/features.php">Features</a></li>
<li><a href="/auth-smtp/pricing.php">Pricing</a></li>
<li><a href="/auth-smtp/why-us.html">Why Choose Us</a></li>
<li><a href="/auth-smtp/testimonials.php">Testimonials</a></li>
<li><a href="/auth-smtp/money-back-guarantee.html">Money Back Guarantee</a></li>
<li><a href="/signup/">Sign Up</a></li>
</ul>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
<div class="widget widget_recent_post">
<h3>The Help</h3>
<ul>
<li><a href="/status/">Service Status</a></li>
<li><a href="/support/">Help &amp; Support</a></li>
<li><a href="/support/getting-started-guide.html">Getting Started</a></li>
<li><a href="/auth-smtp/how-to-setup.html">Setup Guides</a></li>
<li><a href="/support/">Knowledge Base</a></li>
<li><a href="/support/troubleshooting.html">Troubleshooting</a></li>
<li><a href="/login/">Control Panel</a></li>
</ul>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
<div class="widget widget_recent_post">
<h3>The Legals</h3>
<ul>
<li><a href="/documentation/gdpr-statement.html">GDPR</a></li>
<li><a href="/documentation/terms-of-service.html">Terms of Service</a></li>
<li><a href="/documentation/acceptable-usage-policy.html">Acceptable Usage Policy</a></li>
<li><a href="/abuse/">Report Spam</a></li>
<li><a href="/documentation/privacy-policy.html">Privacy</a></li>
<li><a href="/documentation/disclaimer.html">Disclaimer</a></li>
<li><a href="/documentation/copyright.html">Copyright Notice</a></li>
</ul>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
<form action="/search/index.php" class="form-horizontal" id="footerform" method="post">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<h3><span class="glyphicon glyphicon-search"> </span>Search</h3>
<div class="input-group input-group-sm">
<input class="form-control input-sm" id="footerSearch" name="search" placeholder="search" type="search" value=""/>
<span class="input-group-btn">
<button class="btn btn-default" id="singlebutton" name="singlebutton">Go</button>
</span>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<!-- PayPal Logo --><table style="float:right; margin-top:20px; border:none; padding:10px;"><tr><td></td></tr><tr><td><a href="https://www.paypal.com/uk/webapps/mpp/paypal-popup" onclick="javascript:window.open('https://www.paypal.com/uk/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;" title="How PayPal Works"><img alt="Secured by PayPal" src="https://www.paypalobjects.com/webstatic/mktg/logo/bdg_secured_by_pp_2line.png"/></a><div style="text-align:center"></div></td></tr></table><!-- PayPal Logo -->
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<div class="pull-right" style="margin-top:20px;"><script src="https://sealserver.trustwave.com/seal.js?style=invert&amp;code=6403d31bfaee4647a897d2c899d6a9e4" type="text/javascript"></script></div>
</div>
</form></div>
</div>
</div>
</footer></div>
<div class="subfooter">
<div class="container">
<div class="row">
<div class="col-md-6">
<nav>
<ul class="pull-left">
<li><a href="/documentation/copyright.html"><span class="glyphicon glyphicon-copyright-mark"></span> Copyright 2003 - 2021 AuthSMTP</a></li>
<li><a href="/sitemap.php"><span class="glyphicon glyphicon-globe"></span> Sitemap</a></li>
</ul>
</nav>
</div>
<div class="col-md-6">
<nav>
<ul class="pull-right">
<li><a href="http://www.getonline.co.uk">Web hosting and design by GetOnline</a></li>
</ul>
</nav>
</div>
</div>
</div>
</div>
<!-- footer close -->
<!-- used by the isBreakpoint JS function to detext screen mode / width -->
<div class="device-xs visible-xs"></div>
<div class="device-sm visible-sm"></div>
<div class="device-md visible-md"></div>
<div class="device-lg visible-lg"></div>
<!-- used by the isBreakpoint JS function to detext screen mode / width -->
</body>
</html>
