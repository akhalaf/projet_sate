<!DOCTYPE html>
<html lang="de-DE">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="https://die-lichtbildwerkstatt.com/xmlrpc.php" rel="pingback"/>
<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>
<script>var et_site_url='https://die-lichtbildwerkstatt.com';var et_post_id='0';function et_core_page_resource_fallback(a,b){"undefined"===typeof b&&(b=a.sheet.cssRules&&0===a.sheet.cssRules.length);b&&(a.onerror=null,a.onload=null,a.href?a.href=et_site_url+"/?et_core_page_resource="+a.id+et_post_id:a.src&&(a.src=et_site_url+"/?et_core_page_resource="+a.id+et_post_id))}
</script><title>404 Nicht gefunden | Die Lichtbildwerkstatt</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://die-lichtbildwerkstatt.com/feed" rel="alternate" title="Die Lichtbildwerkstatt » Feed" type="application/rss+xml"/>
<link href="https://die-lichtbildwerkstatt.com/comments/feed" rel="alternate" title="Die Lichtbildwerkstatt » Kommentar-Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/die-lichtbildwerkstatt.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<meta content="Die Lichtbildwerkstatt v.3.13.1" name="generator"/><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://die-lichtbildwerkstatt.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://die-lichtbildwerkstatt.com/wp-content/plugins/popup-builder/public/css/theme.css?ver=3.7.1" id="theme.css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://die-lichtbildwerkstatt.com/wp-content/themes/Divi/style.css?ver=5.6" id="parent-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,latin-ext&amp;display=swap" id="divi-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://die-lichtbildwerkstatt.com/wp-content/themes/dielichtbildwerkstatt/style.css?ver=4.7.7" id="divi-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://die-lichtbildwerkstatt.com/wp-includes/css/dashicons.min.css?ver=5.6" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://die-lichtbildwerkstatt.com/wp-content/plugins/gdpr-cookie-compliance/dist/styles/gdpr-main.css?ver=4.4.5" id="moove_gdpr_frontend-css" media="all" rel="stylesheet" type="text/css"/>
<style id="moove_gdpr_frontend-inline-css" type="text/css">
#moove_gdpr_cookie_modal,#moove_gdpr_cookie_info_bar,.gdpr_cookie_settings_shortcode_content{font-family:Nunito,sans-serif}#moove_gdpr_save_popup_settings_button{background-color:#373737;color:#fff}#moove_gdpr_save_popup_settings_button:hover{background-color:#000}#moove_gdpr_cookie_info_bar .moove-gdpr-info-bar-container .moove-gdpr-info-bar-content a.mgbutton,#moove_gdpr_cookie_info_bar .moove-gdpr-info-bar-container .moove-gdpr-info-bar-content button.mgbutton{background-color:#0a0a0a}#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-footer-content .moove-gdpr-button-holder a.mgbutton,#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-footer-content .moove-gdpr-button-holder button.mgbutton,.gdpr_cookie_settings_shortcode_content .gdpr-shr-button.button-green{background-color:#0a0a0a;border-color:#0a0a0a}#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-footer-content .moove-gdpr-button-holder a.mgbutton:hover,#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-footer-content .moove-gdpr-button-holder button.mgbutton:hover,.gdpr_cookie_settings_shortcode_content .gdpr-shr-button.button-green:hover{background-color:#fff;color:#0a0a0a}#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-close i,#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-close span.gdpr-icon{background-color:#0a0a0a;border:1px solid #0a0a0a}#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-close i:hover,#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-close span.gdpr-icon:hover,#moove_gdpr_cookie_info_bar span[data-href]>u.change-settings-button{color:#0a0a0a}#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-left-content #moove-gdpr-menu li.menu-item-selected a span.gdpr-icon,#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-left-content #moove-gdpr-menu li.menu-item-selected button span.gdpr-icon{color:inherit}#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-left-content #moove-gdpr-menu li:hover a,#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-left-content #moove-gdpr-menu li:hover button{color:#b61f29}#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-left-content #moove-gdpr-menu li a span.gdpr-icon,#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-left-content #moove-gdpr-menu li button span.gdpr-icon{color:inherit}#moove_gdpr_cookie_modal .gdpr-acc-link{line-height:0;font-size:0;color:transparent;position:absolute}#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-close:hover i,#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-left-content #moove-gdpr-menu li a,#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-left-content #moove-gdpr-menu li button,#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-left-content #moove-gdpr-menu li button i,#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-left-content #moove-gdpr-menu li a i,#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-tab-main .moove-gdpr-tab-main-content a:hover,#moove_gdpr_cookie_info_bar.moove-gdpr-dark-scheme .moove-gdpr-info-bar-container .moove-gdpr-info-bar-content a.mgbutton:hover,#moove_gdpr_cookie_info_bar.moove-gdpr-dark-scheme .moove-gdpr-info-bar-container .moove-gdpr-info-bar-content button.mgbutton:hover,#moove_gdpr_cookie_info_bar.moove-gdpr-dark-scheme .moove-gdpr-info-bar-container .moove-gdpr-info-bar-content a:hover,#moove_gdpr_cookie_info_bar.moove-gdpr-dark-scheme .moove-gdpr-info-bar-container .moove-gdpr-info-bar-content button:hover,#moove_gdpr_cookie_info_bar.moove-gdpr-dark-scheme .moove-gdpr-info-bar-container .moove-gdpr-info-bar-content span.change-settings-button:hover,#moove_gdpr_cookie_info_bar.moove-gdpr-dark-scheme .moove-gdpr-info-bar-container .moove-gdpr-info-bar-content u.change-settings-button:hover,#moove_gdpr_cookie_info_bar span[data-href]>u.change-settings-button{color:#0a0a0a}#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-left-content #moove-gdpr-menu li.menu-item-selected a,#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-left-content #moove-gdpr-menu li.menu-item-selected button{color:#b61f29}#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-left-content #moove-gdpr-menu li.menu-item-selected a i,#moove_gdpr_cookie_modal .moove-gdpr-modal-content .moove-gdpr-modal-left-content #moove-gdpr-menu li.menu-item-selected button i{color:#b61f29}#moove_gdpr_cookie_modal.gdpr_lightbox-hide{display:none}
</style>
<script id="ionos-assistant-wp-cookies-js" src="https://die-lichtbildwerkstatt.com/wp-content/mu-plugins/ionos-assistant/js/cookies.js?ver=5.6" type="text/javascript"></script>
<script id="jquery-core-js" src="https://die-lichtbildwerkstatt.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://die-lichtbildwerkstatt.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="Popup.js-js-extra" type="text/javascript">
/* <![CDATA[ */
var sgpbPublicUrl = "https:\/\/die-lichtbildwerkstatt.com\/wp-content\/plugins\/popup-builder\/public\/";
var SGPB_JS_LOCALIZATION = {"imageSupportAlertMessage":"Only image files supported","areYouSure":"Are you sure?","addButtonSpinner":"Add","audioSupportAlertMessage":"Only audio files supported (e.g.: mp3, wav, m4a, ogg)","publishPopupBeforeElementor":"Please, publish the popup before starting to use Elementor with it!","publishPopupBeforeDivi":"Please, publish the popup before starting to use Divi Builder with it!","closeButtonAltText":"Close"};
/* ]]> */
</script>
<script id="Popup.js-js" src="https://die-lichtbildwerkstatt.com/wp-content/plugins/popup-builder/public/js/Popup.js?ver=3.7.1" type="text/javascript"></script>
<script id="PopupConfig.js-js" src="https://die-lichtbildwerkstatt.com/wp-content/plugins/popup-builder/public/js/PopupConfig.js?ver=3.7.1" type="text/javascript"></script>
<script id="PopupBuilder.js-js-extra" type="text/javascript">
/* <![CDATA[ */
var SGPB_POPUP_PARAMS = {"popupTypeAgeRestriction":"ageRestriction","defaultThemeImages":{"1":"https:\/\/die-lichtbildwerkstatt.com\/wp-content\/plugins\/popup-builder\/public\/img\/theme_1\/close.png","2":"https:\/\/die-lichtbildwerkstatt.com\/wp-content\/plugins\/popup-builder\/public\/img\/theme_2\/close.png","3":"https:\/\/die-lichtbildwerkstatt.com\/wp-content\/plugins\/popup-builder\/public\/img\/theme_3\/close.png","5":"https:\/\/die-lichtbildwerkstatt.com\/wp-content\/plugins\/popup-builder\/public\/img\/theme_5\/close.png","6":"https:\/\/die-lichtbildwerkstatt.com\/wp-content\/plugins\/popup-builder\/public\/img\/theme_6\/close.png"},"homePageUrl":"https:\/\/die-lichtbildwerkstatt.com\/","isPreview":"","convertedIdsReverse":[],"dontShowPopupExpireTime":"365","conditionalJsClasses":[]};
var SGPB_JS_PACKAGES = {"packages":{"current":1,"free":1,"silver":2,"gold":3,"platinum":4},"extensions":{"geo-targeting":false,"advanced-closing":false}};
var SGPB_JS_PARAMS = {"ajaxUrl":"https:\/\/die-lichtbildwerkstatt.com\/wp-admin\/admin-ajax.php","nonce":"5fc26d7f1d"};
/* ]]> */
</script>
<script id="PopupBuilder.js-js" src="https://die-lichtbildwerkstatt.com/wp-content/plugins/popup-builder/public/js/PopupBuilder.js?ver=3.7.1" type="text/javascript"></script>
<link href="https://die-lichtbildwerkstatt.com/wp-json/" rel="https://api.w.org/"/><link href="https://die-lichtbildwerkstatt.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://die-lichtbildwerkstatt.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<script type="text/javascript">
            var jQueryMigrateHelperHasSentDowngrade = false;

			window.onerror = function( msg, url, line, col, error ) {
				// Break out early, do not processing if a downgrade reqeust was already sent.
				if ( jQueryMigrateHelperHasSentDowngrade ) {
					return true;
                }

				var xhr = new XMLHttpRequest();
				var nonce = '8111b079b9';
				var jQueryFunctions = [
					'andSelf',
					'browser',
					'live',
					'boxModel',
					'support.boxModel',
					'size',
					'swap',
					'clean',
					'sub',
                ];
				var match_pattern = /\)\.(.+?) is not a function/;
                var erroredFunction = msg.match( match_pattern );

                // If there was no matching functions, do not try to downgrade.
                if ( typeof erroredFunction !== 'object' || typeof erroredFunction[1] === "undefined" || -1 === jQueryFunctions.indexOf( erroredFunction[1] ) ) {
                    return true;
                }

                // Set that we've now attempted a downgrade request.
                jQueryMigrateHelperHasSentDowngrade = true;

				xhr.open( 'POST', 'https://die-lichtbildwerkstatt.com/wp-admin/admin-ajax.php' );
				xhr.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' );
				xhr.onload = function () {
					var response,
                        reload = false;

					if ( 200 === xhr.status ) {
                        try {
                        	response = JSON.parse( xhr.response );

                        	reload = response.data.reload;
                        } catch ( e ) {
                        	reload = false;
                        }
                    }

					// Automatically reload the page if a deprecation caused an automatic downgrade, ensure visitors get the best possible experience.
					if ( reload ) {
						location.reload();
                    }
				};

				xhr.send( encodeURI( 'action=jquery-migrate-downgrade-version&_wpnonce=' + nonce ) );

				// Suppress error alerts in older browsers
				return true;
			}
        </script>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/><link as="font" crossorigin="anonymous" href="https://die-lichtbildwerkstatt.com/wp-content/themes/Divi/core/admin/fonts/modules.ttf" rel="preload"/><link href="https://fonts.googleapis.com/css?family=Biryani:400,700&amp;display=swap&amp;subset=latin-ext" rel="stylesheet"/><link href="https://die-lichtbildwerkstatt.com/wp-content/uploads/2019/09/cropped-logo_kamera-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://die-lichtbildwerkstatt.com/wp-content/uploads/2019/09/cropped-logo_kamera-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://die-lichtbildwerkstatt.com/wp-content/uploads/2019/09/cropped-logo_kamera-180x180.png" rel="apple-touch-icon"/>
<meta content="https://die-lichtbildwerkstatt.com/wp-content/uploads/2019/09/cropped-logo_kamera-270x270.png" name="msapplication-TileImage"/>
<link href="https://die-lichtbildwerkstatt.com/wp-content/et-cache/global/et-divi-customizer-global-16085787173644.min.css" id="et-divi-customizer-global-cached-inline-styles" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" rel="stylesheet"/></head>
<body class="error404 et_pb_button_helper_class et_fixed_nav et_show_nav et_secondary_nav_enabled et_secondary_nav_two_panels et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_footer_columns2 et_cover_background et_pb_gutter et_pb_gutters3 et_smooth_scroll et_right_sidebar et_divi_theme et-db et_minified_js et_minified_css">
<div id="page-container">
<div id="top-header">
<div class="container clearfix">
<div id="et-info">
<span id="et-info-phone">0202 / 75 850 900</span>
<a href="mailto:info@die-lichtbildwerkstatt.com"><span id="et-info-email">info@die-lichtbildwerkstatt.com</span></a>
<ul class="et-social-icons">
<li class="et-social-icon et-social-facebook">
<a class="icon" href="https://www.facebook.com/Fotografenmeister/">
<span>Facebook</span>
</a>
</li>
<li class="et-social-icon et-social-instagram">
<a class="icon" href="#">
<span>Instagram</span>
</a>
</li>
</ul> </div> <!-- #et-info -->
<div id="et-secondary-menu">
<div class="et_duplicate_social_icons">
<ul class="et-social-icons">
<li class="et-social-icon et-social-facebook">
<a class="icon" href="https://www.facebook.com/Fotografenmeister/">
<span>Facebook</span>
</a>
</li>
<li class="et-social-icon et-social-instagram">
<a class="icon" href="#">
<span>Instagram</span>
</a>
</li>
</ul>
</div> </div> <!-- #et-secondary-menu -->
</div> <!-- .container -->
</div> <!-- #top-header -->
<header data-height-onload="80" id="main-header">
<div class="container clearfix et_menu_container">
<div class="logo_container">
<span class="logo_helper"></span>
<a href="https://die-lichtbildwerkstatt.com/">
<img alt="Die Lichtbildwerkstatt" data-height-percentage="70" id="logo" src="https://die-lichtbildwerkstatt.com/wp-content/uploads/2019/10/logo_header.png"/>
</a>
</div>
<div data-fixed-height="51" data-height="80" id="et-top-navigation">
<nav id="top-menu-nav">
<ul class="nav et_disable_top_tier" id="top-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-97" id="menu-item-97"><a href="https://die-lichtbildwerkstatt.com/">STARTSEITE</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-96" id="menu-item-96"><a href="https://die-lichtbildwerkstatt.com/fotografie">FOTOGRAFIE</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-422" id="menu-item-422"><a href="https://die-lichtbildwerkstatt.com/erotikfotografie">EROTIK</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-458" id="menu-item-458"><a href="https://die-lichtbildwerkstatt.com/babybauch">BABY &amp; BAUCH</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-431" id="menu-item-431"><a href="https://die-lichtbildwerkstatt.com/hochzeitsfotografie">HOCHZEIT</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-430" id="menu-item-430"><a href="https://die-lichtbildwerkstatt.com/portraitfotos">PORTRAIT</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-434" id="menu-item-434"><a href="https://die-lichtbildwerkstatt.com/bewerbungsfotos">BEWERBUNG</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-433" id="menu-item-433"><a href="https://die-lichtbildwerkstatt.com/businessfotos-fotograf-wuppertal">BUSINESS</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-93" id="menu-item-93"><a href="https://die-lichtbildwerkstatt.com/uebermich">ÜBER MICH</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-496" id="menu-item-496"><a href="https://die-lichtbildwerkstatt.com/ausstellungen">AUSSTELLUNGEN</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-454" id="menu-item-454"><a href="https://die-lichtbildwerkstatt.com/stickichicy">STRICKICHICY</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-94" id="menu-item-94"><a href="https://die-lichtbildwerkstatt.com/kontakt">KONTAKT</a></li>
</ul> </nav>
<div id="et_mobile_nav_menu">
<div class="mobile_nav closed">
<span class="select_page">Seite wählen</span>
<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
</div>
</div> </div> <!-- #et-top-navigation -->
</div> <!-- .container -->
<div class="et_search_outer">
<div class="container et_search_form_container">
<form action="https://die-lichtbildwerkstatt.com/" class="et-search-form" method="get" role="search">
<input class="et-search-field" name="s" placeholder="Suchen …" title="Suchen nach:" type="search" value=""/> </form>
<span class="et_close_search_field"></span>
</div>
</div>
</header> <!-- #main-header -->
<div id="et-main-area">
<div id="main-content">
<div class="container">
<div class="clearfix" id="content-area">
<div id="left-area">
<article class="et_pb_post not_found post-24 post type-post status-publish format-standard hentry category-uncategorized" id="post-0">
<div class="entry">
<!--If no results are found-->
<h1 class="not-found-title">Keine Ergebnisse gefunden</h1>
<p>Die angefragte Seite konnte nicht gefunden werden. Verfeinern Sie Ihre Suche oder verwenden Sie die Navigation oben, um den Beitrag zu finden.</p>
</div>
<!--End if no results are found--> </article> <!-- .et_pb_post -->
</div> <!-- #left-area -->
<div id="sidebar">
<div class="et_pb_widget widget_search" id="search-2"><form action="https://die-lichtbildwerkstatt.com/" class="searchform" id="searchform" method="get" role="search">
<div>
<label class="screen-reader-text" for="s">Suche nach:</label>
<input id="s" name="s" type="text" value=""/>
<input id="searchsubmit" type="submit" value="Suche"/>
</div>
</form></div> <!-- end .et_pb_widget -->
<div class="et_pb_widget widget_recent_entries" id="recent-posts-2">
<h4 class="widgettitle">Neueste Beiträge</h4>
<ul>
<li>
<a href="https://die-lichtbildwerkstatt.com/archiv">archiv</a>
</li>
<li>
<a href="https://die-lichtbildwerkstatt.com/hello-world">Hello world!</a>
</li>
</ul>
</div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_recent_comments" id="recent-comments-2"><h4 class="widgettitle">Neueste Kommentare</h4><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link"><a class="url" href="https://wordpress.org/" rel="external nofollow ugc">A WordPress Commenter</a></span> bei <a href="https://die-lichtbildwerkstatt.com/hello-world#comment-1">Hello world!</a></li></ul></div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_archive" id="archives-2"><h4 class="widgettitle">Archive</h4>
<ul>
<li><a href="https://die-lichtbildwerkstatt.com/2019/08">August 2019</a></li>
</ul>
</div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_categories" id="categories-2"><h4 class="widgettitle">Kategorien</h4>
<ul>
<li class="cat-item cat-item-1"><a href="https://die-lichtbildwerkstatt.com/category/uncategorized">Uncategorized</a>
</li>
</ul>
</div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_meta" id="meta-2"><h4 class="widgettitle">Meta</h4>
<ul>
<li><a href="https://die-lichtbildwerkstatt.com/wp-login.php">Anmelden</a></li>
<li><a href="https://die-lichtbildwerkstatt.com/feed">Feed der Einträge</a></li>
<li><a href="https://die-lichtbildwerkstatt.com/comments/feed">Kommentare-Feed</a></li>
<li><a href="https://de.wordpress.org/">WordPress.org</a></li>
</ul>
</div> <!-- end .et_pb_widget --> </div> <!-- end #sidebar -->
</div> <!-- #content-area -->
</div> <!-- .container -->
</div> <!-- #main-content -->
<span class="et_pb_scroll_top et-pb-icon"></span>
<footer id="main-footer">
<div id="et-footer-nav">
<div class="container">
<ul class="bottom-nav" id="menu-footer"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-privacy-policy menu-item-100" id="menu-item-100"><a href="https://die-lichtbildwerkstatt.com/datenschutzgrundverordnung">DATENSCHUTZ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-98" id="menu-item-98"><a href="https://die-lichtbildwerkstatt.com/kontakt">KONTAKT</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-99" id="menu-item-99"><a href="https://die-lichtbildwerkstatt.com/impressum">IMPRESSUM</a></li>
</ul> </div>
</div> <!-- #et-footer-nav -->
<div id="footer-bottom">
<div class="container clearfix">
<ul class="et-social-icons">
<li class="et-social-icon et-social-facebook">
<a class="icon" href="https://www.facebook.com/Fotografenmeister/">
<span>Facebook</span>
</a>
</li>
<li class="et-social-icon et-social-instagram">
<a class="icon" href="#">
<span>Instagram</span>
</a>
</li>
</ul><p id="footer-info">© Die Lichtbildwerkstatt</p> </div> <!-- .container -->
</div>
</footer> <!-- #main-footer -->
</div> <!-- #et-main-area -->
</div> <!-- #page-container -->
<aside aria-label="GDPR Cookie Banner" class="moove-gdpr-info-bar-hidden moove-gdpr-align-center moove-gdpr-light-scheme gdpr_infobar_postion_bottom" id="moove_gdpr_cookie_info_bar" role="note">
<div class="moove-gdpr-info-bar-container">
<div class="moove-gdpr-info-bar-content">
<div class="moove-gdpr-cookie-notice">
<p>Wir verwenden Cookies, um dir die bestmögliche Erfahrung auf unserer Website zu bieten.</p>
<p>Du kannst mehr darüber erfahren, welche Cookies wir verwenden, oder sie unter <span class="change-settings-button" data-href="#moove_gdpr_cookie_modal">Einstellungen</span> deaktivieren.</p>
</div>
<!--  .moove-gdpr-cookie-notice -->
<div class="moove-gdpr-button-holder">
<button aria-label="Zustimmen" class="mgbutton moove-gdpr-infobar-allow-all">Zustimmen</button>
</div>
<!--  .button-container --> </div>
<!-- moove-gdpr-info-bar-content -->
</div>
<!-- moove-gdpr-info-bar-container -->
</aside>
<!-- #moove_gdpr_cookie_info_bar  -->
<div style="position:fixed;left: -999999999999999999999px;">
<div class="sg-popup-builder-content" data-events='[{"param":"load","value":"","hiddenOption":[]}]' data-id="4562" data-options="YTo0OTp7czoyNjoic2dwYi1mbG9hdGluZy1idXR0b24tc3R5bGUiO3M6NjoiY29ybmVyIjtzOjI5OiJzZ3BiLWZsb2F0aW5nLWJ1dHRvbi1wb3NpdGlvbiI7czoxMjoiYm90dG9tLXJpZ2h0IjtzOjE0OiJzZ3BiLWltYWdlLXVybCI7czo4ODoiaHR0cHM6Ly9kaWUtbGljaHRiaWxkd2Vya3N0YXR0LmNvbS93cC1jb250ZW50L3VwbG9hZHMvMjAyMC8xMC9kbHdfZ2VzY2hsb3NzZW4tc2NhbGVkLmpwZyI7czo5OiJzZ3BiLXR5cGUiO3M6NToiaW1hZ2UiO3M6MTU6InNncGItaXMtcHJldmlldyI7czoxOiIwIjtzOjE0OiJzZ3BiLWlzLWFjdGl2ZSI7czo3OiJjaGVja2VkIjtzOjM0OiJzZ3BiLWJlaGF2aW9yLWFmdGVyLXNwZWNpYWwtZXZlbnRzIjthOjE6e2k6MDthOjE6e2k6MDthOjE6e3M6NToicGFyYW0iO3M6MTI6InNlbGVjdF9ldmVudCI7fX19czoxODoic2dwYi1wb3B1cC16LWluZGV4IjtzOjQ6Ijk5OTkiO3M6MTc6InNncGItcG9wdXAtdGhlbWVzIjtzOjEyOiJzZ3BiLXRoZW1lLTEiO3M6MjU6InNncGItb3ZlcmxheS1jdXN0b20tY2xhc3MiO3M6MTg6InNncGItcG9wdXAtb3ZlcmxheSI7czoxODoic2dwYi1vdmVybGF5LWNvbG9yIjtzOjA6IiI7czoyMDoic2dwYi1vdmVybGF5LW9wYWNpdHkiO3M6MzoiMC44IjtzOjI1OiJzZ3BiLWNvbnRlbnQtY3VzdG9tLWNsYXNzIjtzOjE2OiJzZy1wb3B1cC1jb250ZW50IjtzOjEyOiJzZ3BiLWVzYy1rZXkiO3M6Mjoib24iO3M6MjQ6InNncGItZW5hYmxlLWNsb3NlLWJ1dHRvbiI7czoyOiJvbiI7czoyMzoic2dwYi1jbG9zZS1idXR0b24tZGVsYXkiO3M6MToiMCI7czoyNjoic2dwYi1jbG9zZS1idXR0b24tcG9zaXRpb24iO3M6MTE6ImJvdHRvbVJpZ2h0IjtzOjI0OiJzZ3BiLWJ1dHRvbi1wb3NpdGlvbi10b3AiO3M6MDoiIjtzOjI2OiJzZ3BiLWJ1dHRvbi1wb3NpdGlvbi1yaWdodCI7czoxOiI5IjtzOjI3OiJzZ3BiLWJ1dHRvbi1wb3NpdGlvbi1ib3R0b20iO3M6MToiOSI7czoyNToic2dwYi1idXR0b24tcG9zaXRpb24tbGVmdCI7czowOiIiO3M6MTc6InNncGItYnV0dG9uLWltYWdlIjtzOjA6IiI7czoyMzoic2dwYi1idXR0b24taW1hZ2Utd2lkdGgiO3M6MjoiMjEiO3M6MjQ6InNncGItYnV0dG9uLWltYWdlLWhlaWdodCI7czoyOiIyMSI7czoxNzoic2dwYi1ib3JkZXItY29sb3IiO3M6NzoiIzAwMDAwMCI7czoxODoic2dwYi1ib3JkZXItcmFkaXVzIjtzOjE6IjAiO3M6MjM6InNncGItYm9yZGVyLXJhZGl1cy10eXBlIjtzOjE6IiUiO3M6MTY6InNncGItYnV0dG9uLXRleHQiO3M6NToiQ2xvc2UiO3M6MTg6InNncGItb3ZlcmxheS1jbGljayI7czoyOiJvbiI7czoyNToic2dwYi1wb3B1cC1kaW1lbnNpb24tbW9kZSI7czoxNDoicmVzcG9uc2l2ZU1vZGUiO3M6MzM6InNncGItcmVzcG9uc2l2ZS1kaW1lbnNpb24tbWVhc3VyZSI7czo0OiJhdXRvIjtzOjEwOiJzZ3BiLXdpZHRoIjtzOjU6IjY0MHB4IjtzOjExOiJzZ3BiLWhlaWdodCI7czo1OiI0ODBweCI7czoxNDoic2dwYi1tYXgtd2lkdGgiO3M6MDoiIjtzOjE1OiJzZ3BiLW1heC1oZWlnaHQiO3M6MDoiIjtzOjE0OiJzZ3BiLW1pbi13aWR0aCI7czozOiIxMjAiO3M6MTU6InNncGItbWluLWhlaWdodCI7czowOiIiO3M6MjY6InNncGItb3Blbi1hbmltYXRpb24tZWZmZWN0IjtzOjk6Ik5vIGVmZmVjdCI7czoyNzoic2dwYi1jbG9zZS1hbmltYXRpb24tZWZmZWN0IjtzOjk6Ik5vIGVmZmVjdCI7czoyOToic2dwYi1lbmFibGUtY29udGVudC1zY3JvbGxpbmciO3M6Mjoib24iO3M6MTY6InNncGItcG9wdXAtb3JkZXIiO3M6MToiMCI7czoxNjoic2dwYi1wb3B1cC1kZWxheSI7czoxOiIwIjtzOjc6InNncGItanMiO3M6MjoiSlMiO3M6ODoic2dwYi1jc3MiO3M6MzoiQ1NTIjtzOjEyOiJzZ3BiLXBvc3QtaWQiO3M6NDoiNDU2MiI7czoyNToic2dwYi1lbmFibGUtcG9wdXAtb3ZlcmxheSI7czoyOiJvbiI7czoyMjoic2dwYi1idXR0b24taW1hZ2UtZGF0YSI7czowOiIiO3M6MjY6InNncGItYmFja2dyb3VuZC1pbWFnZS1kYXRhIjtzOjA6IiI7czoxNDoic2dwYkNvbmRpdGlvbnMiO047fQ==" id="sg-popup-content-wrapper-4562">
<div class="sgpb-popup-builder-content-4562 sgpb-popup-builder-content-html"><img alt="" class="sgpb-preloaded-image-4562" height="1" src="https://die-lichtbildwerkstatt.com/wp-content/uploads/2020/10/dlw_geschlossen-scaled.jpg" style="position:absolute;right:9999999999999px;" width="1"/></div>
</div>
</div><script id="divi-custom-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var DIVI = {"item_count":"%d Item","items_count":"%d Items"};
var et_shortcodes_strings = {"previous":"Vorherige","next":"N\u00e4chste"};
var et_pb_custom = {"ajaxurl":"https:\/\/die-lichtbildwerkstatt.com\/wp-admin\/admin-ajax.php","images_uri":"https:\/\/die-lichtbildwerkstatt.com\/wp-content\/themes\/Divi\/images","builder_images_uri":"https:\/\/die-lichtbildwerkstatt.com\/wp-content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"47aa09a52d","subscription_failed":"Bitte \u00fcberpr\u00fcfen Sie die Felder unten aus, um sicherzustellen, dass Sie die richtigen Informationen eingegeben.","et_ab_log_nonce":"01139e0a08","fill_message":"Bitte f\u00fcllen Sie die folgenden Felder aus:","contact_error_message":"Bitte folgende Fehler beheben:","invalid":"Ung\u00fcltige E-Mail","captcha":"Captcha","prev":"Vorherige","previous":"Vorherige","next":"Weiter","wrong_captcha":"Sie haben die falsche Zahl im Captcha eingegeben.","wrong_checkbox":"Checkbox","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","ab_tests":[],"is_ab_testing_active":"","page_id":"24","unique_test_id":"","ab_bounce_rate":"5","is_cache_plugin_active":"no","is_shortcode_tracking":"","tinymce_uri":""}; var et_builder_utils_params = {"condition":{"diviTheme":true,"extraTheme":false},"scrollLocations":["app","top"],"builderScrollLocations":{"desktop":"app","tablet":"app","phone":"app"},"onloadScrollLocation":"app","builderType":"fe"}; var et_frontend_scripts = {"builderCssContainerPrefix":"#et-boc","builderCssLayoutPrefix":"#et-boc .et-l"};
var et_pb_box_shadow_elements = [];
var et_pb_motion_elements = {"desktop":[],"tablet":[],"phone":[]};
var et_pb_sticky_elements = [];
/* ]]> */
</script>
<script id="divi-custom-script-js" src="https://die-lichtbildwerkstatt.com/wp-content/themes/Divi/js/custom.unified.js?ver=4.7.7" type="text/javascript"></script>
<script id="et-core-common-js" src="https://die-lichtbildwerkstatt.com/wp-content/themes/Divi/core/admin/js/common.js?ver=4.7.7" type="text/javascript"></script>
<script id="moove_gdpr_frontend-js-extra" type="text/javascript">
/* <![CDATA[ */
var moove_frontend_gdpr_scripts = {"ajaxurl":"https:\/\/die-lichtbildwerkstatt.com\/wp-admin\/admin-ajax.php","post_id":"24","plugin_dir":"https:\/\/die-lichtbildwerkstatt.com\/wp-content\/plugins\/gdpr-cookie-compliance","show_icons":"all","is_page":"","strict_init":"1","enabled_default":{"third_party":0,"advanced":0},"geo_location":"false","force_reload":"false","is_single":"","hide_save_btn":"false","current_user":"0","cookie_expiration":"365"};
/* ]]> */
</script>
<script id="moove_gdpr_frontend-js" src="https://die-lichtbildwerkstatt.com/wp-content/plugins/gdpr-cookie-compliance/dist/scripts/main.js?ver=4.4.5" type="text/javascript"></script>
<script id="wp-embed-js" src="https://die-lichtbildwerkstatt.com/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
<!-- V1 -->
<div aria-label="GDPR Settings Screen" class="gdpr_lightbox-hide" id="moove_gdpr_cookie_modal" role="complementary">
<div class="moove-gdpr-modal-content moove-clearfix logo-position-left moove_gdpr_modal_theme_v1">
<button aria-label="Close GDPR Cookie Settings" class="moove-gdpr-modal-close">
<span class="gdpr-sr-only">Close GDPR Cookie Settings</span>
<span class="gdpr-icon moovegdpr-arrow-close"></span>
</button>
<div class="moove-gdpr-modal-left-content">
<div class="moove-gdpr-company-logo-holder">
<img alt="Die Lichtbildwerkstatt" class="img-responsive" height="82" src="https://die-lichtbildwerkstatt.com/wp-content/uploads/2019/10/logo_header.png" title="Die Lichtbildwerkstatt" width="236"/>
</div>
<!--  .moove-gdpr-company-logo-holder --> <ul id="moove-gdpr-menu">
<li class="menu-item-on menu-item-privacy_overview menu-item-selected">
<button aria-label="Datenschutz-Übersicht" class="moove-gdpr-tab-nav" data-href="#privacy_overview">
<span class="gdpr-svg-icon">
<svg class="icon icon-privacy-overview" viewbox="0 0 26 32">
<path d="M11.082 27.443l1.536 0.666 1.715-0.717c5.018-2.099 8.294-7.014 8.294-12.442v-5.734l-9.958-5.325-9.702 5.325v5.862c0 5.376 3.2 10.24 8.115 12.365zM4.502 10.138l8.166-4.506 8.397 4.506v4.813c0 4.838-2.893 9.19-7.347 11.034l-1.101 0.461-0.922-0.41c-4.352-1.894-7.194-6.195-7.194-10.957v-4.941zM12.029 14.259h1.536v7.347h-1.536v-7.347zM12.029 10.394h1.536v2.483h-1.536v-2.483z" fill="currentColor"></path>
</svg>
</span>
<span class="gdpr-nav-tab-title">Datenschutz-Übersicht</span>
</button>
</li>
<li class="menu-item-strict-necessary-cookies menu-item-off">
<button aria-label="Unbedingt notwendige Cookies" class="moove-gdpr-tab-nav" data-href="#strict-necessary-cookies">
<span class="gdpr-svg-icon">
<svg class="icon icon-strict-necessary" viewbox="0 0 26 32">
<path d="M22.685 5.478l-9.984 10.752-2.97-4.070c-0.333-0.461-0.973-0.538-1.434-0.205-0.435 0.333-0.538 0.947-0.23 1.408l3.686 5.094c0.179 0.256 0.461 0.41 0.768 0.435h0.051c0.282 0 0.538-0.102 0.742-0.307l10.854-11.699c0.358-0.435 0.333-1.075-0.102-1.434-0.384-0.384-0.998-0.358-1.382 0.026v0zM22.301 12.954c-0.563 0.102-0.922 0.64-0.794 1.203 0.128 0.614 0.179 1.229 0.179 1.843 0 5.094-4.122 9.216-9.216 9.216s-9.216-4.122-9.216-9.216 4.122-9.216 9.216-9.216c1.536 0 3.021 0.384 4.378 1.101 0.512 0.23 1.126 0 1.357-0.538 0.205-0.461 0.051-0.998-0.384-1.254-5.478-2.944-12.314-0.922-15.283 4.557s-0.922 12.314 4.557 15.258 12.314 0.922 15.258-4.557c0.896-1.638 1.357-3.482 1.357-5.35 0-0.768-0.077-1.51-0.23-2.253-0.102-0.538-0.64-0.896-1.178-0.794z" fill="currentColor"></path>
</svg>
</span>
<span class="gdpr-nav-tab-title">Unbedingt notwendige Cookies</span>
</button>
</li>
</ul>
<div class="moove-gdpr-branding-cnt">
<a class="moove-gdpr-branding" href="https://wordpress.org/plugins/gdpr-cookie-compliance" rel="noopener noreferrer nofollow" target="_blank">Powered by  <span>GDPR Cookie Compliance</span></a>
</div>
<!--  .moove-gdpr-branding --> </div>
<!--  .moove-gdpr-modal-left-content -->
<div class="moove-gdpr-modal-right-content">
<div class="moove-gdpr-modal-title">
</div>
<!-- .moove-gdpr-modal-ritle -->
<div class="main-modal-content">
<div class="moove-gdpr-tab-content">
<div class="moove-gdpr-tab-main" id="privacy_overview">
<span class="tab-title">Datenschutz-Übersicht</span>
<div class="moove-gdpr-tab-main-content">
<p>Diese Website verwendet Cookies, damit wir dir die bestmögliche Benutzererfahrung bieten können. Cookie-Informationen werden in deinem Browser gespeichert und führen Funktionen aus, wie das Wiedererkennen von dir, wenn du auf unsere Website zurückkehrst, und hilft unserem Team zu verstehen, welche Abschnitte der Website für dich am interessantesten und nützlichsten sind.</p>
</div>
<!--  .moove-gdpr-tab-main-content -->
</div>
<!-- #privacy_overview -->
<div class="moove-gdpr-tab-main" id="strict-necessary-cookies" style="display:none">
<span class="tab-title">Unbedingt notwendige Cookies</span>
<div class="moove-gdpr-tab-main-content">
<p>Unbedingt notwendige Cookies sollten jederzeit aktiviert sein, damit wir deine Einstellungen für die Cookie-Einstellungen speichern können.</p>
<div class="moove-gdpr-status-bar ">
<div class="gdpr-cc-form-wrap">
<div class="gdpr-cc-form-fieldset">
<label class="cookie-switch" for="moove_gdpr_strict_cookies">
<span class="gdpr-sr-only">Enable or Disable Cookies</span>
<input aria-label="Unbedingt notwendige Cookies" id="moove_gdpr_strict_cookies" name="moove_gdpr_strict_cookies" type="checkbox" value="check"/>
<span class="cookie-slider cookie-round" data-text-disabled="Deaktiviert" data-text-enable="Aktiviert"></span>
</label>
</div>
<!-- .gdpr-cc-form-fieldset -->
</div>
<!-- .gdpr-cc-form-wrap -->
</div>
<!-- .moove-gdpr-status-bar -->
<div class="moove-gdpr-strict-warning-message" style="margin-top: 10px;">
<p>Wenn du diesen Cookie deaktivierst, können wir die Einstellungen nicht speichern. Dies bedeutet, dass du jedes Mal, wenn du diese Website besuchst, die Cookies erneut aktivieren oder deaktivieren musst.</p>
</div>
<!--  .moove-gdpr-tab-main-content -->
</div>
<!--  .moove-gdpr-tab-main-content -->
</div>
<!-- #strict-necesarry-cookies -->
</div>
<!--  .moove-gdpr-tab-content -->
</div>
<!--  .main-modal-content -->
<div class="moove-gdpr-modal-footer-content">
<div class="moove-gdpr-button-holder">
<button aria-label="Alle aktivieren" class="mgbutton moove-gdpr-modal-allow-all button-visible">Alle aktivieren</button>
<button aria-label="Einstellungen speichern" class="mgbutton moove-gdpr-modal-save-settings button-visible">Einstellungen speichern</button>
</div>
<!--  .moove-gdpr-button-holder --> </div>
<!--  .moove-gdpr-modal-footer-content -->
</div>
<!--  .moove-gdpr-modal-right-content -->
<div class="moove-clearfix"></div>
</div>
<!--  .moove-gdpr-modal-content -->
</div>
<!-- #moove_gdpr_cookie_modal  --></body>
</html>
