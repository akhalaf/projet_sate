<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "59292",
      cRay: "610c05ce9fbf1a05",
      cHash: "789fb9acd428d1b",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYWdub3Rpcy5jb20v",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "FJqxfwfaLzUrV22N80hvdJVp3F9Hlde2rXjR7u+3QHDg9wWLZS0WWohinYEjU8Ncdkjx3xxhfDEkJYXC/RpkMeMK3MAoONx9dlsZmeH6+tvY8X5x4A9D1jRx3GyssN240a3tOXTpAwH5pFM6CklK5oKGIh4y6WLfm0hJRX77lyo0+/V2qCk6m3xdmcVOLqE0sb6JZr4IR1v6XEZGHTTgjhvNZ5RdEkxKZDG7gXts7HUNHwSSl8lpoIHf1xr2b1Fq/SKArfMBVr0ZsVTDmoCBhwCZ4bqEev6RWrOvXuPpMgi4pRqw077E6e7hxdJ88BD3eMYXJAZH2jXpRWTVKLITkxBroMNE7rf8bR6ijg/kKaOeCszclHBbVVzzcCfx6uizBH/d1M4l/KEdDo5Ow/tTPZZ7PZi4cNQOTJ3g0pO16gXvvxyHvs0ZAdssrT9IU7XvsGgtu8gu1MhZBfdBTXJ8YWfa7ezm5audeJItqUNXbJvaV7xcrcU3aH6n8j/Ov7cIm3lhFb4ZPoraCHlSVmJ08p+KIhlQ+2eSaxg4nEo2zYklfBtExTClWuA5OpktPtJLL8NkzCF7sDYwAmmRJ4zPqtN2hQxvZjILzq5QC2VAfiymKOfS9ucMdllVPBmVpCKdbNgK6YmFbKuSOnSU3mr+t+hzskKGZ6+c8APvsUOn16uCvJayFrNZPIGdQT1k0iWkxjSraJtoHoQwFpMS7zsN3BU/W1inmLesPUXkp7GlqWDILW4Me7x7Jo70ZGCcmjnK",
        t: "MTYxMDUwODQ1OS4zMjcwMDA=",
        m: "UDr0fN2yPHJG+puzy4ZenLlFUDq0ffVtlLciyQOlBWE=",
        i1: "/pVy2hwLI6IupdHIwhJc4g==",
        i2: "s8zFCqKII0QADpl9Lf7tcw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "6AXM9km+GvHXqt0cj/txbgx6ekeEZMY1czx3AKVBwDQ=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.agnotis.com</h2>
</div><!-- /.header -->
<a href="https://simtelnet.com/mudstealthy.php?option=0" style="display: none;">table</a>
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=3cd5987a81351d4af60c4fa3e2d296f1eb15d702-1610508459-0-AZjUYY7RC1b7jAoQWfSODCNwUWU1-yl1_3v9IYUE2-JDARfjIC28nXzyrS2b_Z0yKGTyJdUmGpgJZK-bKN9rlQUkvWdT08d84LLIIMJNRnVY24j-QTTN85xDAy2jd-jKeiYlakhUHpKFOhrMwt8BED-NAjAIzKdFKC5tcwy5AdP9Ytrh4DUMpoYk3vzblMyN9Sket-y2diSp9_e7jXUBlMsMxNw0X0igcwJ5ZbghIdtCkw7MMo4D_v760pd7_Ky8huvacH4bLc-6g7nzhsLc8xEaQdoQfXA9ntp990hxzi7UbybZZA4nBU6rN1v_9DH7TyCCyG-pDSlsBsgsdcMy9f4JaaDwUPy2-vhO1RqqLn2ECIQdlzRAiu692QHSp4vim1dPik5qkbrZX_Afjfd1TLh55fz5eYtDNcnUgepDhRuOSjTctYq5NWwDzliq5AML5F9AmHv87UlrfGqPYltCT9a1uNdRhGVkYNe6pNvgDXv1vCM8ADoV6CT-MbQGltZKAoKIbzELz6Ss14wuGRgiJUg" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="ca16941f09ee2fe79a51bdd3aaf8c5274b72f2d4-1610508459-0-Ac0lhL4ChnsKqpFEwdVM1rH6iIJ08iwGcAybsOpfX0gqsVM6QCZ/JViUsboLm/8ayQaeO+CJc8Y8Fjw0Db0oHA0yXn+wRjKhs2Vh090IA8Fbops3pumZROf4xgV5WyfMnm4yXP/lqhvsJpoZZ/s5PKAoTwVKaA3vpJiISg450gf7DBXhpKJtOWdZSPinzvmXngU5UxvQO9R8kMten9OaIw9ztalh+D/7qB2rz96znDY2sIekJh5eBYUXKVhN7UouDWBdX2mbJ6asMYeC7AseOHaCTph1KfEL8CMxTPzLgWFkqYnwlsUUx42Kxih9r/26r+BHETGr18xreDp0eF+1+oDvZ1HmVs5Nz7s6k+mq+wCs9mNIuy5eOzFV3O7HnsSuXGFGU25Dfe4lyZJ6DHEDaRVkNwcx9cRaN93BLqS3wY9Ysaq+LC1lJYuluj/CHi0ubFJzLUNsNO7CAYgJt3txdqgjIWbzBYuvflhduG6PNR0vo9SVMDqjPben3ECx9O7Alpplirnft0mlhUgRYAQWCpqKzpGumNE/2yqS5LPY/MPYXKm8NE1YHLHKo0nMY82woRn3IdY68pKWUj8K4QmvfSvhfYJdwx3XtEO7YSe9mrAWu3CxUBhKbAt0+Gv64Mx28+yAUJrgKYUkKG8ZxwRcVzUTgIMWg/SMC1Bs7/g6YahUx6EB1IRxKsSFdycYsIveukeIEHBNMhuamh1wp+NdgUKb36teHVLEZ3QHQq9cCFGfq48mIY0/Sdoyvc9Q6Sq6jXafo7kFpUgtBaZPkweKJlqtpZaSYSnle/pKdICgssamUSjD6ss0IiBBMe3UJ0w0xU1RrR7XvG7lYh4Q1P/kr2s9e4fQc6jXJbIXQ/9s5X+YC9c3mSuRX9CSY2yFG/WretnpAUzQki0g3S5tizsUbAHv/+pD2hZlZUBqDgIumXHXuV0JseSYjJL1ToEAJb851g5ygcBZZLEpVALxtOl+SvRtYlqeSiPzm2HAtlC6dl5v4meNwVZC/4QeE/43/WCV+xDR6AKIflSjep3a5afrZp3QSAm34Rq9BIpnBJ/q+ulqj87lQN8WQ0WFvPueVIexV21z2Hnb4Kzc/KMLhrL1AL/SZFLQkTqXDK59rYQ4kw6UkgM4Fy1NUVjg4+n+Nu4/pR1IW9oNENLeUjM9FE4+RurnToVhmBEMqpxBeX4xOdfxZINQDzKzr6Uqj6I6cDq4Ou+U1MXs4QbW9wetccTGz/Ye9ncNOl9NoE5/jTmI5kqZnUrYl42XN4XekhbHGDx9V33aE285n4bpaMOAPVKWiiHHI69apQrwP76LrA52WdsQ"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="e9e27cbe5ff724969489381609492f79"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610c05ce9fbf1a05')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610c05ce9fbf1a05</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
