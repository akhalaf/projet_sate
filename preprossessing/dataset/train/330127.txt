<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Короткий URL для всех!</title>
<meta content="Укорачивалка урлов. Лучшая в мире. Делает короткие ссылки, короткий урл." name="Description"/>
<meta content="urls, shortener, короткие ссылки, укорачиватель ссылок, сократить ссылку, короткий урл, clck" name="Keywords"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="../static/style.css?2" media="screen" rel="stylesheet" type="text/css"/>
<link href="../static/favicon.ico" rel="icon" type="image/x-icon"/>
<script src="//yandex.st/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>
<script charset="utf-8" src="//yandex.st/share/share.js" type="text/javascript"></script>
</head>
<body>
<script language="javascript">
        
    (function() {

        function refocus() {
            $('#input').focus();
            $('#input').select();
        };

        function filldivider(fl) {
            $('#divider').html(fl);
        };

        function post_callback(data) {
            $('#input').val(data[0]);
            $('#input').select();
            if (data.length == 2 && data[1]) {
                $('#not_changed').show();
            };
            $('#click').val('✂'); $('#click').show();
            $('.descp2').show();
            $("#qrimage").attr('src','http://disk.yandex.net/qr/?clean=1&text=' + data[0]);
            filldivider('<a href="/">Добавить еще одну ссылку</a>');
            $('#form').submit( function () {
                $('#input').select();
                console.log($('#input').val());
                document.execCommand("copy");
                return false;
            });
            Ya.share({
                elementID: 'ya_share1',
                link: data[0]
            });
        };

        function error_callback() {
            var message = $('#message');
            message.text('Ссылка не соответствует ограничениям сервиса');
        };

        function put_url() {
            $('.descp').hide();
            filldivider(' ');
            $('#click').hide();
            $('#form').unbind();
            $.ajax({
                dataType: "json",
                url: '/--?json=true&url=' + encodeURIComponent($('#input').val()),
                success: post_callback,
                error: error_callback
            });
            return false;
        };

        $(document).ready(function() {
            refocus();
            $('#form').submit(put_url);
        });
    })();

    </script>
<div id="head">
<div id="header">
<h1><a href="/">Кликер</a></h1>
<h2>Серьёзный укорачиватель URL</h2>
</div>
</div>
<div id="wrap">
<div id="content">
<div class="flash" id="message"></div>
<form action="/" id="form" method="GET">
<input id="input" name="url" size="40" tabindex="1" type="text"/>
<input id="click" tabindex="2" type="submit" value="Клик"/>
<p id="not_changed" style="display: none; color: #99140B;font-size: 14px">К сожалению, мы не можем сократить эту ссылку</p>
</form><br/>
<p class="descp">Довольно часто при общении в интернете люди пересылают друг другу ссылки. Иногда эти ссылки бывают длинными. Совсем иногда — очень длинными. Но почти всегда — слишком длинными. Для того чтобы избавиться от этой проблемы, был создан этот сайт. Да, я знаю, что таких тысячи, но этот же лучше!</p>
<p class="descp">А дальше все просто: вставляете ссылку в поле для ввода, нажимаете "Клик" и получаете короткий, совсем короткий URL.</p>
<p class="descp">Для удобства работы с сервисом можно воспользоваться букмарклетом — перетащите вот эту ссылку (<a href="javascript:location.href='?url='+encodeURIComponent(location.href)">Click↑</a>
) на панель вашего браузера. Нажимайте на эту закладку, когда хотите укоротить адрес просматриваемого вами сайта.</p>
<p class="descp">Иногда (нередко) данным сервисом пользуются злоумышленники: хакеры, крекеры, спамы, куки, закладки, троянские кони…
    <!-- подробнее см. http://lurkmore.ru/Хакеры, крекеры, спамы, куки!-->
    Очевидно, что это противоречит всем <a href="/nospamplease">правилам использования сервиса</a>, а также здравому смыслу.
    Если вы столкнулись с такой cитуацией, пожалуйста, <a href="mailto:abuse@yandex.ru">напишите письмо нашим специалистам</a>.</p>
<p class="descp2" style="display: none"><img id="qrimage" src=""/>Новая ссылка запомнена, теперь вы можете скопировать и отправить адрес своим друзьям и знакомым. Чтобы поместить его в буфер обмена используйте системную функцию копирования текста. Посмотрите направо. Это <a href="http://ru.wikipedia.org/wiki/QR-%D0%BA%D0%BE%D0%B4">QR-код</a>. Нажмите на него правой кнопкой, сохраните картинку и распечатайте на стену. Ну или просто не обращайте внимания.<br/>
<span class="descp2" id="ya_share1" style="display: none"></span>
</p><div class="readit" style="display: none"></div>
</div>
<div id="divider"><a href="/--">Для настоящих гиков: использование API</a></div>
<!-- -->
<div id="footer">Версия 5, © 2010-2021 «<a href="http://www.yandex.ru/">"Яндекс"</a>»</div>
<!-- Yandex.Metrika -->
<script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
<div style="display:none;">
<script type="text/javascript">
                try { var yaCounter1173931 = new Ya.Metrika(1173931); } catch(e){}
            </script>
</div>
<noscript><div style="position:absolute"><img alt="" src="//mc.yandex.ru/watch/1173931"/></div></noscript>
<!-- /Yandex.Metrika -->
</div>
</body>
</html>