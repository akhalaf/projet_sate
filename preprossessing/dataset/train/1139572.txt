<!DOCTYPE html>
<html class="no-js scheme_default" lang="en-US">
<head>
<!-- [BEGIN] Scripts added via Head-and-Footer-Scripts-Inserter plugin by Space X-Chimp Studio ( https://www.spacexchimp.com ) -->
<!-- [END] Scripts added via Head-and-Footer-Scripts-Inserter plugin by Space X-Chimp Studio ( https://www.spacexchimp.com ) -->
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<meta content="telephone=no" name="format-detection"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://phillipspainting.com.au/xmlrpc.php" rel="pingback"/>
<title>Page not found – Phillip's Painting</title>
<meta content="noindex,follow" name="robots"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//maxcdn.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://phillipspainting.com.au/feed/" rel="alternate" title="Phillip's Painting » Feed" type="application/rss+xml"/>
<link href="https://phillipspainting.com.au/comments/feed/" rel="alternate" title="Phillip's Painting » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/phillipspainting.com.au\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.8.15"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b!==c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://phillipspainting.com.au/wp-content/plugins/vc-extensions-bundle/css/admin_icon.css?ver=4.8.15" id="vc_extensions_cqbundle_adminicon-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://phillipspainting.com.au/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.9" id="contact-form-7-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://phillipspainting.com.au/wp-content/plugins/essential-grid/public/assets/css/settings.css?ver=2.1.6.1" id="essential-grid-plugin-settings-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&amp;ver=4.8.15" id="tp-open-sans-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&amp;ver=4.8.15" id="tp-raleway-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&amp;ver=4.8.15" id="tp-droid-serif-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://phillipspainting.com.au/wp-content/plugins/instagram-feed/css/sb-instagram.min.css?ver=1.5" id="sb_instagram_styles-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" id="sb-font-awesome-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://phillipspainting.com.au/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.5.2" id="rs-plugin-settings-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
#rs-demo-id {}
</style>
<link href="https://phillipspainting.com.au/wp-content/plugins/trx_addons/css/font-icons/css/trx_addons_icons-embedded.css?ver=4.8.15" id="trx_addons-icons-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://phillipspainting.com.au/wp-content/plugins/trx_addons/js/swiper/swiper.min.css" id="swiperslider-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://phillipspainting.com.au/wp-content/plugins/trx_addons/js/magnific/magnific-popup.min.css" id="magnific-popup-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://phillipspainting.com.au/wp-content/plugins/trx_addons/css/trx_addons.css" id="trx_addons-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://phillipspainting.com.au/wp-content/plugins/trx_addons/css/trx_addons.animation.css?ver=4.8.15" id="trx_addons-animation-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A400%2C700%2C600%7CLibre+Baskerville%3A400%2C400italic%2C700&amp;subset=latin%2Clatin-ext&amp;ver=4.8.15" id="prorange-font-google_fonts-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://phillipspainting.com.au/wp-content/themes/prorange/css/fontello/css/fontello-embedded.css?ver=4.8.15" id="prorange-fontello-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://phillipspainting.com.au/wp-content/themes/prorange/style.css" id="prorange-main-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://phillipspainting.com.au/wp-content/themes/prorange/css/__styles.css?ver=4.8.15" id="prorange-styles-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://phillipspainting.com.au/wp-content/themes/prorange/css/__colors.css?ver=4.8.15" id="prorange-colors-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://phillipspainting.com.au/wp-includes/js/mediaelement/mediaelementplayer.min.css?ver=2.22.0" id="mediaelement-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://phillipspainting.com.au/wp-includes/js/mediaelement/wp-mediaelement.min.css?ver=4.8.15" id="wp-mediaelement-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<link href="https://phillipspainting.com.au/wp-content/themes/prorange/css/responsive.css?ver=4.8.15" id="prorange-responsive-css" media="all" property="stylesheet" rel="stylesheet" type="text/css"/>
<script src="https://phillipspainting.com.au/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://phillipspainting.com.au/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://phillipspainting.com.au/wp-content/plugins/essential-grid/public/assets/js/lightbox.js?ver=2.1.6.1" type="text/javascript"></script>
<script src="https://phillipspainting.com.au/wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.tools.min.js?ver=2.1.6.1" type="text/javascript"></script>
<script src="https://phillipspainting.com.au/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.5.2" type="text/javascript"></script>
<link href="https://phillipspainting.com.au/wp-json/" rel="https://api.w.org/"/>
<link href="https://phillipspainting.com.au/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://phillipspainting.com.au/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.8.15" name="generator"/>
<script type="text/javascript">
			var ajaxRevslider;
			
			jQuery(document).ready(function() {
				// CUSTOM AJAX CONTENT LOADING FUNCTION
				ajaxRevslider = function(obj) {
				
					// obj.type : Post Type
					// obj.id : ID of Content to Load
					// obj.aspectratio : The Aspect Ratio of the Container / Media
					// obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content
					
					var content = "";

					data = {};
					
					data.action = 'revslider_ajax_call_front';
					data.client_action = 'get_slider_html';
					data.token = '7732e7e9bb';
					data.type = obj.type;
					data.id = obj.id;
					data.aspectratio = obj.aspectratio;
					
					// SYNC AJAX REQUEST
					jQuery.ajax({
						type:"post",
						url:"https://phillipspainting.com.au/wp-admin/admin-ajax.php",
						dataType: 'json',
						data:data,
						async:false,
						success: function(ret, textStatus, XMLHttpRequest) {
							if(ret.success == true)
								content = ret.data;								
						},
						error: function(e) {
							console.log(e);
						}
					});
					
					 // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
					 return content;						 
				};
				
				// CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
				var ajaxRemoveRevslider = function(obj) {
					return jQuery(obj.selector+" .rev_slider").revkill();
				};

				// EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
				var extendessential = setInterval(function() {
					if (jQuery.fn.tpessential != undefined) {
						clearInterval(extendessential);
						if(typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
							jQuery.fn.tpessential.defaults.ajaxTypes.push({type:"revslider",func:ajaxRevslider,killfunc:ajaxRemoveRevslider,openAnimationSpeed:0.3});   
							// type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
							// func: the Function Name which is Called once the Item with the Post Type has been clicked
							// killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
							// openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
						}
					}
				},30);
			});
		</script>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<meta content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." name="generator"/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://phillipspainting.com.au/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><style id="custom-background-css" type="text/css">
body.custom-background { background-color: #e7edf1; }
</style>
<meta content="Powered by Slider Revolution 5.4.5.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." name="generator"/>
<script type="text/javascript">function setREVStartSize(e){
				try{ var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;					
					if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
				}catch(d){console.log("Failure at Presize of Slider:"+d)}
			};</script>
<style id="wp-custom-css" type="text/css">
			/*
You can add your own CSS here.

Click the help icon above to learn more.
*/

.sc_layouts_logo img{
	max-height:70px !important;
}

.date-field{
	width: 90%;
    padding-left: 10%;
    padding-top: 5%;
    padding-bottom: 5%;
    color: grey;
background:white !important;
}

input[type="checkbox"] {
height: 20px;
position: relative;
top: 6px;
display: -webkit-inline-box;
}

.checkbox-field{
	color:grey;
}
.top_panel_title{
display:none
}

.eg-icon-link{
  display:none;
}		</style>
<!-- [BEGIN] Scripts added via Head-and-Footer-Scripts-Inserter plugin by Space X-Chimp Studio ( https://www.spacexchimp.com ) -->
<!-- [END] Scripts added via Head-and-Footer-Scripts-Inserter plugin by Space X-Chimp Studio ( https://www.spacexchimp.com ) -->
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>
<body class="error404 custom-background body_tag scheme_default blog_mode_blog body_style_wide is_stream blog_style_excerpt sidebar_hide expand_content header_style_header-default header_position_default menu_style_top no_layout wpb-js-composer js-comp-ver-5.3 vc_responsive">
<div class="body_wrap">
<div class="page_wrap">
<header class="top_panel top_panel_default without_bg_image scheme_default"><div class="top_panel_navi sc_layouts_row sc_layouts_row_type_compact sc_layouts_row_fixed sc_layouts_row_delimiter scheme_default">
<div class="content_wrap">
<div class="columns_wrap">
<div class="sc_layouts_column sc_layouts_column_align_left sc_layouts_column_icons_position_left column-1_4">
<div class="sc_layouts_item"><a class="sc_layouts_logo" href="https://phillipspainting.com.au/"><img alt="" src="http://staging1.rubix-studio.com/wp-content/uploads/2017/10/logo.png"/></a></div>
</div><div class="sc_layouts_column sc_layouts_column_align_right sc_layouts_column_icons_position_left column-3_4">
<div class="sc_layouts_item">
<nav class="menu_main_nav_area sc_layouts_menu sc_layouts_menu_default sc_layouts_hide_on_mobile"><ul class="sc_layouts_menu_nav menu_main_nav" id="menu_main"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-92" id="menu-item-92"><a href="https://phillipspainting.com.au/grid/"><span>Gallery</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-89" id="menu-item-89"><a href="https://phillipspainting.com.au/commercial/"><span>Services</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-80" id="menu-item-80"><a href="https://phillipspainting.com.au/about-1/"><span>About</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-90" id="menu-item-90"><a href="https://phillipspainting.com.au/contact/"><span>Contact</span></a></li></ul></nav> <div class="sc_layouts_iconed_text sc_layouts_menu_mobile_button">
<a class="sc_layouts_item_link sc_layouts_iconed_text_link" href="#">
<span class="sc_layouts_item_icon sc_layouts_iconed_text_icon trx_addons_icon-menu"></span>
</a>
</div>
</div> <div class="sc_layouts_item">
<div class="search_wrap search_style_fullscreen header_search">
<div class="search_form_wrap">
<form action="https://phillipspainting.com.au/" class="search_form" method="get" role="search">
<input class="search_field" name="s" placeholder="Search" type="text" value=""/>
<button class="search_submit trx_addons_icon-search" type="submit"></button>
<a class="search_close trx_addons_icon-delete"></a>
</form>
</div>
</div> </div>
</div>
</div><!-- /.sc_layouts_row -->
</div><!-- /.content_wrap -->
</div><!-- /.top_panel_navi --> <div class="top_panel_title sc_layouts_row sc_layouts_row_type_normal scheme_dark">
<div class="content_wrap">
<div class="sc_layouts_column sc_layouts_column_align_center">
<div class="sc_layouts_item">
<div class="sc_layouts_title sc_align_center">
<div class="sc_layouts_title_title"> <h1 class="sc_layouts_title_caption">URL not found</h1>
</div><div class="sc_layouts_title_breadcrumbs"><div class="breadcrumbs"><a class="breadcrumbs_item home" href="https://phillipspainting.com.au/">Home</a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current">URL not found</span></div></div>
</div>
</div>
</div>
</div>
</div>
</header><div class="menu_mobile_overlay"></div>
<div class="menu_mobile menu_mobile_fullscreen scheme_dark">
<div class="menu_mobile_inner">
<a class="menu_mobile_close icon-cancel"></a><nav class="menu_mobile_nav_area sc_layouts_menu sc_layouts_menu_default "><ul class=" menu_mobile_nav" id="menu_mobile"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-92" id="menu_mobile-item-92"><a href="https://phillipspainting.com.au/grid/"><span>Gallery</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-89" id="menu_mobile-item-89"><a href="https://phillipspainting.com.au/commercial/"><span>Services</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-80" id="menu_mobile-item-80"><a href="https://phillipspainting.com.au/about-1/"><span>About</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-90" id="menu_mobile-item-90"><a href="https://phillipspainting.com.au/contact/"><span>Contact</span></a></li></ul></nav><div class="search_wrap search_style_normal search_mobile">
<div class="search_form_wrap">
<form action="https://phillipspainting.com.au/" class="search_form" method="get" role="search">
<input class="search_field" name="s" placeholder="Search" type="text" value=""/>
<button class="search_submit trx_addons_icon-search" type="submit"></button>
</form>
</div>
</div><div class="socials_mobile"><a class="social_item social_item_style_icons social_item_type_icons" href="#" target="_blank"><span class="social_icon social_twitter"><span class="icon-twitter"></span></span></a><a class="social_item social_item_style_icons social_item_type_icons" href="#" target="_blank"><span class="social_icon social_gplus"><span class="icon-gplus"></span></span></a><a class="social_item social_item_style_icons social_item_type_icons" href="#" target="_blank"><span class="social_icon social_facebook"><span class="icon-facebook"></span></span></a></div> </div>
</div>
<div class="page_content_wrap scheme_default">
<div class="content_wrap">
<div class="content">
<article class="post_item_single post_item_404 post-0 unknown type-unknown status-publish hentry">
<div class="post_content">
<h1 class="page_title">404</h1>
<div class="page_info">
<h1 class="page_subtitle">Oops...</h1>
<p class="page_description">We're sorry, but <br/>something went wrong.</p>
<a class="go_home theme_button" href="https://phillipspainting.com.au/">Homepage</a>
</div>
</div>
</article>
</div><!-- </.content> -->
</div><!-- </.content_wrap> --> </div><!-- </.page_content_wrap> -->
<footer class="footer_wrap footer_default scheme_dark">
<div class="footer_copyright_wrap scheme_">
<div class="footer_copyright_inner">
<div class="content_wrap">
<div class="copyright_text">Copyright ©  <a href="http://horizonppc.com/">Horizon PPC </a> All Rights Reserved.

</div>
</div>
</div>
</div>
</footer><!-- /.footer_wrap -->
</div><!-- /.page_wrap -->
</div><!-- /.body_wrap -->
<!-- [BEGIN] Scripts added via Head-and-Footer-Scripts-Inserter plugin by Space X-Chimp Studio ( https://www.spacexchimp.com ) -->
<!-- [END] Scripts added via Head-and-Footer-Scripts-Inserter plugin by Space X-Chimp Studio ( https://www.spacexchimp.com ) -->
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/phillipspainting.com.au\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script src="https://phillipspainting.com.au/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.9" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var sb_instagram_js_options = {"sb_instagram_at":""};
/* ]]> */
</script>
<script src="https://phillipspainting.com.au/wp-content/plugins/instagram-feed/js/sb-instagram.min.js?ver=1.5" type="text/javascript"></script>
<script src="https://phillipspainting.com.au/wp-content/plugins/trx_addons/js/swiper/swiper.jquery.min.js" type="text/javascript"></script>
<script src="https://phillipspainting.com.au/wp-content/plugins/trx_addons/js/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var TRX_ADDONS_STORAGE = {"ajax_url":"https:\/\/phillipspainting.com.au\/wp-admin\/admin-ajax.php","ajax_nonce":"774c182ed6","site_url":"https:\/\/phillipspainting.com.au","post_id":"0","vc_edit_mode":"0","popup_engine":"magnific","animate_inner_links":"0","user_logged_in":"0","email_mask":"^([a-zA-Z0-9_\\-]+\\.)*[a-zA-Z0-9_\\-]+@[a-z0-9_\\-]+(\\.[a-z0-9_\\-]+)*\\.[a-z]{2,6}$","msg_ajax_error":"Invalid server answer!","msg_magnific_loading":"Loading image","msg_magnific_error":"Error loading image","msg_error_like":"Error saving your like! Please, try again later.","msg_field_name_empty":"The name can't be empty","msg_field_email_empty":"Too short (or empty) email address","msg_field_email_not_valid":"Invalid email address","msg_field_text_empty":"The message text can't be empty","msg_search_error":"Search error! Try again later.","msg_send_complete":"Send message complete!","msg_send_error":"Transmit failed!","ajax_views":"","menu_cache":[".menu_mobile_inner > nav > ul"],"login_via_ajax":"1","msg_login_empty":"The Login field can't be empty","msg_login_long":"The Login field is too long","msg_password_empty":"The password can't be empty and shorter then 4 characters","msg_password_long":"The password is too long","msg_login_success":"Login success! The page should be reloaded in 3 sec.","msg_login_error":"Login failed!","msg_not_agree":"Please, read and check 'Terms and Conditions'","msg_email_long":"E-mail address is too long","msg_email_not_valid":"E-mail address is invalid","msg_password_not_equal":"The passwords in both fields are not equal","msg_registration_success":"Registration success! Please log in!","msg_registration_error":"Registration failed!","msg_sc_googlemap_not_avail":"Googlemap service is not available","msg_sc_googlemap_geocoder_error":"Error while geocode address"};
/* ]]> */
</script>
<script src="https://phillipspainting.com.au/wp-content/plugins/trx_addons/js/trx_addons.js" type="text/javascript"></script>
<script src="https://phillipspainting.com.au/wp-content/themes/prorange/js/superfish.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var PRORANGE_STORAGE = {"ajax_url":"https:\/\/phillipspainting.com.au\/wp-admin\/admin-ajax.php","ajax_nonce":"774c182ed6","site_url":"https:\/\/phillipspainting.com.au","site_scheme":"scheme_default","user_logged_in":"","mobile_layout_width":"767","mobile_device":"","menu_side_stretch":"1","menu_side_icons":"1","background_video":"","use_mediaelements":"1","comment_maxlength":"1000","admin_mode":"","email_mask":"^([a-zA-Z0-9_\\-]+\\.)*[a-zA-Z0-9_\\-]+@[a-z0-9_\\-]+(\\.[a-z0-9_\\-]+)*\\.[a-z]{2,6}$","strings":{"ajax_error":"Invalid server answer!","error_global":"Error data validation!","name_empty":"The name can&#039;t be empty","name_long":"Too long name","email_empty":"Too short (or empty) email address","email_long":"Too long email address","email_not_valid":"Invalid email address","text_empty":"The message text can&#039;t be empty","text_long":"Too long message text"},"alter_link_color":"#fe7259","button_hover":"slide_left"};
/* ]]> */
</script>
<script src="https://phillipspainting.com.au/wp-content/themes/prorange/js/__scripts.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var mejsL10n = {"language":"en-US","strings":{"Close":"Close","Fullscreen":"Fullscreen","Turn off Fullscreen":"Turn off Fullscreen","Go Fullscreen":"Go Fullscreen","Download File":"Download File","Download Video":"Download Video","Play":"Play","Pause":"Pause","Captions\/Subtitles":"Captions\/Subtitles","None":"None","Time Slider":"Time Slider","Skip back %1 seconds":"Skip back %1 seconds","Video Player":"Video Player","Audio Player":"Audio Player","Volume Slider":"Volume Slider","Mute Toggle":"Mute Toggle","Unmute":"Unmute","Mute":"Mute","Use Up\/Down Arrow keys to increase or decrease volume.":"Use Up\/Down Arrow keys to increase or decrease volume.","Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds."}};
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/"};
/* ]]> */
</script>
<script src="https://phillipspainting.com.au/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=2.22.0" type="text/javascript"></script>
<script src="https://phillipspainting.com.au/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=4.8.15" type="text/javascript"></script>
<script src="https://phillipspainting.com.au/wp-includes/js/wp-embed.min.js?ver=4.8.15" type="text/javascript"></script>
<a class="trx_addons_scroll_to_top trx_addons_icon-up" href="#" title="Scroll to top"></a>
<!-- [BEGIN] Scripts added via Head-and-Footer-Scripts-Inserter plugin by Space X-Chimp Studio ( https://www.spacexchimp.com ) -->
<script>jQuery(".copyright_text a").attr('target', '_blank');</script>
<!-- [END] Scripts added via Head-and-Footer-Scripts-Inserter plugin by Space X-Chimp Studio ( https://www.spacexchimp.com ) -->
</body>
</html>