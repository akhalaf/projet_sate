<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Willow Street Fire Company - Lancaster County, PA</title>
<meta content="The Willow Street Fire Company was organized in 1924. Willow Street is located in Lancaster County in the Commonwealth of Pennsylvania. The original station was a two-bay building that was built on rented land located on Main St in downtown Willow Street. It became home to the department's first Reo fire truck. The department at that time ran 25-30 calls a year.  As the community grew both in population and number of buildings, so did the demand for additional emergency equipment.  In June of 1961 the first ambulance was purchased.  In March of 1977, a fund drive began to raise money for a larger station to be built along the Willow Street Pike.  In 1978 the department moved from its Main Street location to 2901 Willow Street Pike where the company is located today.  The current station is a 7 bay engine house with offices, a lounge, kitchen and meeting room." name="Description"/>
<meta content="Adobe ColdFusion" name="Generator"/>
<meta content="Willow Street Fire Company, Lancaster County, PA" name="Keywords"/>
<meta content="Willow Street Fire Company" name="Author"/>
<link href="https://www.wsfc512.com/images/favicon.ico" rel="Shortcut Icon"/>
<link href="https://www.wsfc512.com/layouts/fsStyle.css" rel="Stylesheet" type="text/css"/>
</head>
<body alink="#FFCC00" background="https://www.wsfc512.com/images/" bgcolor="#FFFFFF" leftmargin="0" link="#FFCC00" marginheight="0" marginwidth="0" text="#FFFFFF" topmargin="0" vlink="#FFCC00">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/apps/scripts/milonic/milonic_src.js" type="text/javascript"></script>
<script src="/apps/scripts/milonic/mmenudom.js" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6Le9Ea0UAAAAAC5QrxyPLnw9PA1w971QexOWCWoJ"></script>
<script>
grecaptcha.ready(function() {
  grecaptcha.execute('6Le9Ea0UAAAAAC5QrxyPLnw9PA1w971QexOWCWoJ', {action: 'Homepage'}).then(function(token) {
	 $("#recaptchaToken").val(token);
  });
});
</script>
<style>
	.grecaptcha-badge {
	display: none !important;
	}
	</style>
<script language="JavaScript" type="text/javascript">

_menuCloseDelay=500           // The time delay for menus to remain visible on mouse out
_menuOpenDelay=50            // The time delay before menus open on mouse over
_followSpeed=90                // Follow scrolling speed
_followRate=20                // Follow scrolling Rate
_subOffsetTop=0              // Sub menu top offset
_subOffsetLeft=4            // Sub menu left offset
_scrollAmount=3               // Only needed for Netscape 4.x
_scrollDelay=20               // Only needed for Netcsape 4.x


with(menuStyle=new mm_style()){
onbgcolor="#222222";
oncolor="#FFFFFF";
offbgcolor="#990000";
offcolor="#FFFFFF";
bordercolor="#FFFFFF";
borderstyle="solid";
borderwidth=0;
separatorcolor="#FFFFFF";
separatorsize=1;
padding=2;
fontsize="17px";
fontstyle="normal";
fontweight="bold";
fontfamily="Trebuchet MS, Verdana, Tahoma, Arial";
headercolor="#000000";
headerbgcolor="#FFFFFF";
//subimage="https://www.wsfc512.com/images/arrow.png";
subimagepadding="2";
overfilter="GradientWipe(duration=0.4);Alpha(opacity=90)";
outfilter="";
}

with(subMenu=new mm_style()){
onbgcolor="#222222";
oncolor="#FFFFFF";
offbgcolor="#990000";
offcolor="#FFFFFF";
bordercolor="#FFFFFF";
borderstyle="solid";
borderwidth=1;
separatorcolor="#FFFFFF";
separatorsize=1;
padding=2;
fontsize="17px";
fontstyle="normal";
fontweight="bold";
fontfamily="Trebuchet MS, Verdana, Tahoma, Arial";
headercolor="#000000";
headerbgcolor="#FFFFFF";
//subimage="https://www.wsfc512.com/images/arrow.png";
subimagepadding="2";
overfilter="GradientWipe(duration=0.4);Alpha(opacity=90)";
outfilter="";
}

with(milonic=new menuname("About Us")){
style=subMenu;
top="offset=3";
left="offset=-1";
borderwidth=1;
itemwidth=200;
itemheight=22;
aI("text=&nbsp;Coverage Area;url=https://www.wsfc512.com/content/coverage;status=Coverage Area;");
aI("text=&nbsp;History;url=https://www.wsfc512.com/content/history;status=History;");
aI("text=&nbsp;Department Members;url=https://www.wsfc512.com/content/members;status=Department Members;");
aI("text=&nbsp;Apparatus;showmenu=Apparatus;status=Apparatus;");
}

with(milonic=new menuname("Apparatus")){
style=subMenu;
left="offset=-1";
borderwidth=1;
itemwidth=200;
itemheight=22;
aI("text=&nbsp;Current;url=https://www.wsfc512.com/content/current;status=Current;");
aI("text=&nbsp;Retired;url=https://www.wsfc512.com/content/retired;status=Retired;");
}	
	
with(milonic=new menuname("Public Info")){
style=subMenu;
top="offset=3";
left="offset=-1";
borderwidth=1;
itemwidth=200;
itemheight=22;
aI("text=&nbsp;Address Signs;url=https://www.wsfc512.com/content/signs;status=Address Signs;");
aI("text=&nbsp;Contact Us;url=https://www.wsfc512.com/content/contact;status=Contact Us;");
aI("text=&nbsp;Guestbook;url=https://www.wsfc512.com/apps/public/guest;status=Guestbook;");
aI("text=&nbsp;Al Starr Building Rental;url=https://www.wsfc512.com/content/rental;status=Al Starr Building Rental;");
aI("text=&nbsp;Become a Member;url=https://www.wsfc512.com/content/become;status=Become a Member;");
aI("text=&nbsp;Knox Box Info;url=https://www.wsfc512.com/content/knox;status=Knox Box Info;");
}
	
with(milonic=new menuname("Galleries")){
style=subMenu;
top="offset=3";
left="offset=-1";
borderwidth=1;
itemwidth=200;
itemheight=22;
aI("text=&nbsp;Around the Hall;url=https://www.wsfc512.com/apps/public/gallery/?Gallery_ID=1;status=Around the Hall;");
aI("text=&nbsp;Incidents;url=https://www.wsfc512.com/apps/public/gallery/?Gallery_ID=2;status=Incidents;");
aI("text=&nbsp;Station Photos;url=https://www.wsfc512.com/apps/public/gallery/?Gallery_ID=3;status=Station Photos;");
aI("text=&nbsp;Classic Photos;url=https://www.wsfc512.com/apps/public/gallery/?Gallery_ID=4;status=Classic Photos;");
}

drawMenus();

</script>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="1200">
<tr><td height="10"></td></tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="1200">
<tr>
<td><img alt="Willow Street Fire Company" border="0" height="320" src="https://www.wsfc512.com/images/wsfc_banner.jpg" width="1200"/></td>
</tr>
</table>
<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="1200">
<tr>
<td bgcolor="#FFFFFF" height="1" width="1"></td>
<td bgcolor="#FFFFFF" height="1" width="1198"></td>
<td bgcolor="#FFFFFF" height="1" width="1"></td>
</tr>
<tr>
<td bgcolor="#FFFFFF" width="1"></td>
<td align="center" bgcolor="#000000" height="18" width="1198">
<script>
			with(milonic=new menuname("Main Menu")){
			style=menuStyle;
			itemwidth=180;
			itemheight=26;
			align="center";
			alwaysvisible=1;
			orientation="horizontal";
			position="relative";
			aI("text=Home;url=https://www.wsfc512.com;status=Home;");
			aI("text=About Us;showmenu=About Us;status=About Us;");
			aI("text=Public Info;showmenu=Public Info;status=Public Info;");
			aI("text=Galleries;showmenu=Galleries;status=Galleries;");
			aI("text=Incident Log;url=https://www.wsfc512.com/apps/public/incidentlog/;status=Incident Log;");
            aI("text=Links;url=https://www.wsfc512.com/content/links;status=Links;");
			aI("text=News Archives;url=https://www.wsfc512.com/apps/public/news;status=News Archives;");
			aI("text=&nbsp;Line of Duty;url=https://www.wsfc512.com/content/lodd;status=Line of Duty;");
			aI("text=Members Area;url=https://www.wsfc512.com/apps/public/login;status=Members Area;");
			}
		drawMenus();
		</script>
</td>
<td bgcolor="#FFFFFF" width="1"></td>
</tr>
<tr>
<td bgcolor="#FFFFFF" height="1" width="1"></td>
<td bgcolor="#FFFFFF" height="1" width="1198"></td>
<td bgcolor="#FFFFFF" height="1" width="1"></td>
</tr>
</table>
<table align="center" bgcolor="#000000" border="0" cellpadding="0" cellspacing="0" width="1200">
<tr>
<td><img alt="Site" border="0" height="23" src="https://www.wsfc512.com/images/menu_header.jpg" width="1200"/></td>
</tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="1200">
<tr>
<td bgcolor="#FFFFFF" width="1"></td>
<td align="center" bgcolor="#000000" valign="top" width="200">
<table align="center" border="0" cellpadding="0" cellspacing="0">
<tr>
<td>
<style>
		.social-slide {
			height: 48px;
			width: 48px;
			margin: 5px;
			float: left;
			-webkit-transition: all ease 0.3s;
			-moz-transition: all ease 0.3s;
			-o-transition: all ease 0.3s;
			-ms-transition: all ease 0.3s;
			transition: all ease 0.3s;
		}
		.social-slide:hover {
			background-position: 0px -48px;
			box-shadow: 0px 0px 4px 1px rgba(0,0,0,0.8);
		}
		.facebook-hover {
			background-image: url('https://www.wsfc512.com/images/facebook-hover.png');
		}
		.twitter-hover {
			background-image: url('https://www.wsfc512.com/images/twitter-hover.png');
		}
		.instagram-hover {
			background-image: url('https://www.wsfc512.com/images/instagram-hover.png');
		}
		</style>
<a href="https://www.facebook.com/Willow-Street-Fire-Co-209772672431027/" target="_blank"><div class="facebook-hover social-slide"></div></a>
<a href="https://twitter.com/wsfc50/" target="_blank"><div class="twitter-hover social-slide"></div></a>
<a href="https://www.instagram.com/willow_street_fire/" target="_blank"><div class="instagram-hover social-slide"></div></a>
</td>
</tr>
</table>
<br/>
<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="1" width="93%">
<tr>
<td align="center" bgcolor="#990000" class="BoldWhite" colspan="2">Upcoming Events</td>
</tr>
</table>
<br/>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="90%">
<tr>
<td align="Center">
<font class="White">
			There are currently no events
			</font></td>
</tr>
<tr>
<td height="10"></td>
</tr>
<tr>
<td align="center"><font class="BoldWhite"><a href="https://www.wsfc512.com/apps/public/events/">View All Events</a></font></td>
</tr>
</table>
<br/>
<table align="center" cellpadding="2" cellspacing="1" class="StatsBoxTable">
<tr>
<td align="center" class="StatsBoxTitle" colspan="2">2020 Incidents</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Jan </td> <td align="center" class="StatsBoxCell" width="50%">41</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Feb</td> <td align="center" class="StatsBoxCell" width="50%">26</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Mar</td> <td align="center" class="StatsBoxCell" width="50%">29</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Apr</td> <td align="center" class="StatsBoxCell" width="50%">20</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">May</td> <td align="center" class="StatsBoxCell" width="50%">33</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">June</td> <td align="center" class="StatsBoxCell" width="50%">44</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">July</td> <td align="center" class="StatsBoxCell" width="50%">41</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Aug</td> <td align="center" class="StatsBoxCell" width="50%">35</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Sept</td> <td align="center" class="StatsBoxCell" width="50%">30</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Oct</td> <td align="center" class="StatsBoxCell" width="50%">29</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Nov</td> <td align="center" class="StatsBoxCell" width="50%">31</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Dec</td> <td align="center" class="StatsBoxCell" width="50%">26</td>
</tr>
</table>
<br/>
<table align="center" cellpadding="2" cellspacing="1" class="StatsBoxTable">
<tr>
<td align="center" class="StatsBoxTitle" colspan="2">2019 Incidents</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Jan</td> <td align="center" class="StatsBoxCell" width="50%">43</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Feb</td> <td align="center" class="StatsBoxCell" width="50%">40</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Mar</td> <td align="center" class="StatsBoxCell" width="50%">36</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Apr</td> <td align="center" class="StatsBoxCell" width="50%">42</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">May</td> <td align="center" class="StatsBoxCell" width="50%">37</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Jun</td> <td align="center" class="StatsBoxCell" width="50%">39</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Jul</td> <td align="center" class="StatsBoxCell" width="50%">35</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Aug</td> <td align="center" class="StatsBoxCell" width="50%">43</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Sep</td> <td align="center" class="StatsBoxCell" width="50%">37</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Oct</td> <td align="center" class="StatsBoxCell" width="50%">46</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Nov</td> <td align="center" class="StatsBoxCell" width="50%">52</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Dec</td> <td align="center" class="StatsBoxCell" width="50%">38</td>
</tr>
<tr>
<td align="center" class="StatsBoxCell" width="50%">Total</td> <td align="center" class="StatsBoxCell" width="50%">488</td>
</tr>
</table>
<br/>
<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="1" width="93%">
<tr>
<td align="center" bgcolor="#990000" class="BoldWhite">Web Counters</td>
</tr>
<tr>
<td align="center" bgcolor="#222222">
<table border="0" cellpadding="2" cellspacing="2">
<tr><td align="center">
<font class="White">
				Website Visitors<br/>Since<br/>February 20, 2018<br/></font>
<table bgcolor="#000000" border="0" cellpadding="1" cellspacing="0">
<tr><td align="center">
<table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="80">
<tr><td align="center">
<font color="#000000" face="MS Sans Serif" size="1"><b>
				350,315<br/>
</b></font>
</td></tr>
</table>
</td></tr>
</table>
</td></tr>
</table>
<table border="0" cellpadding="2" cellspacing="2">
<tr><td align="center">
<font class="White">
				Visitors Today<br/>
				Jan 14, 2021<br/>
</font>
<table bgcolor="#000000" border="0" cellpadding="1" cellspacing="0">
<tr><td align="center">
<table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="80">
<tr><td align="center">
<font color="#000000" face="MS Sans Serif" size="1"><b>
				89
				</b></font>
</td></tr>
</table>
</td></tr>
</table>
</td></tr>
</table>
</td></tr>
</table>
</td>
<td bgcolor="#FFFFFF" width="1"></td>
<td bgcolor="#222222" height="400" valign="top" width="997">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="960">
<tr>
<td height="500" valign="top">
<div style="border: 1px solid; width: 80%; margin: 20px auto; text-align: center; border-radius: 5px;">
<p align="center" class="Title">Error 404<br/>
The requested page cannot be found</p>
<p align="center" class="Bold"><a href="/">Return to Home</a></p>
</div>
</td></tr>
</table>
</td>
<td bgcolor="#FFFFFF" width="1"></td>
</tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="1200">
<tr>
<td background="https://www.wsfc512.com/images/footer.jpg" height="160" valign="top" width="1200">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="1200">
<tr><td height="35"></td></tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="1200">
<tr>
<td align="center" height="10" width="550"><font class="BoldWhite">Website Designed and Hosted By:</font></td>
<td align="left" height="10" width="350"><font class="BoldWhite">Content Proudly Maintained By:</font></td>
<td align="left" height="10" width="300"><font class="BoldWhite">Contact Info:</font></td>
</tr>
<tr>
<td align="center" valign="top">
<a href="http://www.FirehouseSolutions.com"><img alt="Firehouse Solutions" border="0" height="50" src="https://www.wsfc512.com/apps/images/resources/fslogo.png" width="300"/></a><br/>
<font class="BoldWhite"><a href="http://www.FirehouseSolutions.com">www.FirehouseSolutions.com</a></font></td>
<td align="left" valign="top">
<font class="BoldWhite">Willow Street Fire Company</font><br/>
<font class="BoldWhite">PO Box 495<br/>
         Willow Street, PA 17584</font></td>
<td align="left" valign="top">
<font class="BoldWhite">Emergency Dial 911</font><br/>
<font class="BoldWhite">Phone: </font><font class="BoldWhite">717-464-4622</font><br/>
<font class="BoldWhite">E-mail: </font><font class="BoldWhite"><a href="mailto:info@wsfc512.com">info@wsfc512.com</a></font></td>
</tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="1200">
<tr><td height="30"></td></tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="1200">
<tr>
<td width="130"></td>
<td align="center" height="15" valign="middle" width="634"><font class="Small">Copyright © 2021 Firehouse Solutions (A Service of </font><font class="SmallBold">Technology Reflections, Inc.</font><font class="Small">)</font></td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>