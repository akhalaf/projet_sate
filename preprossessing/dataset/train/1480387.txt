<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-ZA" prefix="og: http://ogp.me/ns#">
<![endif]--><!--[if IE 7]>
<html id="ie7" lang="en-ZA" prefix="og: http://ogp.me/ns#">
<![endif]--><!--[if IE 8]>
<html id="ie8" lang="en-ZA" prefix="og: http://ogp.me/ns#">
<![endif]--><!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!--><html lang="en-ZA" prefix="og: http://ogp.me/ns#">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="https://truepoint.co.za/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
	<script src="https://truepoint.co.za/wp-content/themes/Divi/js/html5.js" type="text/javascript"></script>
	<![endif]-->
<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>
<title>Page not found - truePoint</title>
<!-- This site is optimized with the Yoast SEO plugin v5.2 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex,follow" name="robots"/>
<meta content="en_US" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Page not found - truePoint" property="og:title"/>
<meta content="truePoint" property="og:site_name"/>
<meta content="summary" name="twitter:card"/>
<meta content="Page not found - truePoint" name="twitter:title"/>
<!-- / Yoast SEO plugin. -->
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://truepoint.co.za/feed/" rel="alternate" title="truePoint » Feed" type="application/rss+xml"/>
<link href="https://truepoint.co.za/comments/feed/" rel="alternate" title="truePoint » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/truepoint.co.za\/wp-includes\/js\/wp-emoji-release.min.js?ver=9efe99cd7d248b162cd34d3fd324f2e7"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b!==c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<meta content="Divi v.3.0.51" name="generator"/><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,latin-ext" id="divi-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://truepoint.co.za/wp-content/themes/Divi/style.css?ver=3.0.51" id="divi-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://truepoint.co.za/wp-content/themes/Divi/epanel/shortcodes/css/shortcodes.css?ver=3.0.51" id="et-shortcodes-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://truepoint.co.za/wp-content/themes/Divi/epanel/shortcodes/css/shortcodes_responsive.css?ver=3.0.51" id="et-shortcodes-responsive-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://truepoint.co.za/wp-content/themes/Divi/includes/builder/styles/magnific_popup.css?ver=3.0.51" id="magnific-popup-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://truepoint.co.za/wp-includes/css/dashicons.min.css?ver=9efe99cd7d248b162cd34d3fd324f2e7" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://truepoint.co.za/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://truepoint.co.za/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://truepoint.co.za/wp-json/" rel="https://api.w.org/"/>
<link href="https://truepoint.co.za/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://truepoint.co.za/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//truepoint.co.za/?wordfence_logHuman=1&hid=F2869146B0469DBFE6E7C41D03811FF5');
</script><meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/> <style id="theme-customizer-css">
																																				
		
																										
		
																														
		@media only screen and ( min-width: 981px ) {
																																																															
					}
					@media only screen and ( min-width: 1350px) {
				.et_pb_row { padding: 27px 0; }
				.et_pb_section { padding: 54px 0; }
				.single.et_pb_pagebuilder_layout.et_full_width_page .et_post_meta_wrapper { padding-top: 81px; }
				.et_pb_section.et_pb_section_first { padding-top: inherit; }
				.et_pb_fullwidth_section { padding: 0; }
			}
		
		@media only screen and ( max-width: 980px ) {
																				}
		@media only screen and ( max-width: 767px ) {
														}
	</style>
<style id="module-customizer-css">
</style>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<meta content="Btdbtn4XKw-dzqMX9YjuqOQpNUQZH34UDM_872Bi0SM" name="google-site-verification"/></head>
<body class="error404 et_pb_button_helper_class et_fixed_nav et_show_nav et_cover_background et_pb_gutter et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_right_sidebar et_divi_theme unknown">
<div id="page-container">
<header data-height-onload="66" id="main-header">
<div class="container clearfix et_menu_container">
<div class="logo_container">
<span class="logo_helper"></span>
<a href="https://truepoint.co.za/">
<img alt="truePoint" data-height-percentage="54" id="logo" src="https://truepoint.co.za/wp-content/themes/Divi/images/logo.png"/>
</a>
</div>
<div data-fixed-height="40" data-height="66" id="et-top-navigation">
<nav id="top-menu-nav">
<ul class="nav" id="top-menu">
<li><a href="https://truepoint.co.za/">Home</a></li>
<li class="page_item page-item-39"><a href="https://truepoint.co.za/">Landing</a></li>
<li class="page_item page-item-2"><a href="https://truepoint.co.za/sample-page/">Sample Page</a></li>
<li class="cat-item cat-item-1"><a href="https://truepoint.co.za/truePoint/uncategorized/">Uncategorized</a>
</li>
</ul>
</nav>
<div id="et_top_search">
<span id="et_search_icon"></span>
</div>
<div id="et_mobile_nav_menu">
<div class="mobile_nav closed">
<span class="select_page">Select Page</span>
<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
</div>
</div> </div> <!-- #et-top-navigation -->
</div> <!-- .container -->
<div class="et_search_outer">
<div class="container et_search_form_container">
<form action="https://truepoint.co.za/" class="et-search-form" method="get" role="search">
<input class="et-search-field" name="s" placeholder="Search …" title="Search for:" type="search" value=""/> </form>
<span class="et_close_search_field"></span>
</div>
</div>
</header> <!-- #main-header -->
<div id="et-main-area">
<div id="main-content">
<div class="container">
<div class="clearfix" id="content-area">
<div id="left-area">
<article class="et_pb_post not_found post-1 post type-post status-publish format-standard hentry category-uncategorized" id="post-0">
<div class="entry">
<!--If no results are found-->
<h1>No Results Found</h1>
<p>The page you requested could not be found. Try refining your search, or use the navigation above to locate the post.</p>
</div>
<!--End if no results are found--> </article> <!-- .et_pb_post -->
</div> <!-- #left-area -->
<div id="sidebar">
<div class="et_pb_widget widget_search" id="search-2"><form action="https://truepoint.co.za/" class="searchform" id="searchform" method="get" role="search">
<div>
<label class="screen-reader-text" for="s">Search for:</label>
<input id="s" name="s" type="text" value=""/>
<input id="searchsubmit" type="submit" value="Search"/>
</div>
</form></div> <!-- end .et_pb_widget --> <div class="et_pb_widget widget_recent_entries" id="recent-posts-2"> <h4 class="widgettitle">Recent Posts</h4> <ul>
<li>
<a href="https://truepoint.co.za/hello-world/">Hello world!</a>
</li>
</ul>
</div> <!-- end .et_pb_widget --> <div class="et_pb_widget widget_recent_comments" id="recent-comments-2"><h4 class="widgettitle">Recent Comments</h4><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link"><a class="url" href="https://wordpress.org/" rel="external nofollow">A WordPress Commenter</a></span> on <a href="https://truepoint.co.za/hello-world/#comment-1">Hello world!</a></li></ul></div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_archive" id="archives-2"><h4 class="widgettitle">Archives</h4> <ul>
<li><a href="https://truepoint.co.za/2017/08/">August 2017</a></li>
</ul>
</div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_categories" id="categories-2"><h4 class="widgettitle">Categories</h4> <ul>
<li class="cat-item cat-item-1"><a href="https://truepoint.co.za/truePoint/uncategorized/">Uncategorized</a>
</li>
</ul>
</div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_meta" id="meta-2"><h4 class="widgettitle">Meta</h4> <ul>
<li><a href="https://truepoint.co.za/wp-login.php" rel="nofollow">Log in</a></li>
<li><a href="https://truepoint.co.za/feed/">Entries <abbr title="Really Simple Syndication">RSS</abbr></a></li>
<li><a href="https://truepoint.co.za/comments/feed/">Comments <abbr title="Really Simple Syndication">RSS</abbr></a></li>
<li><a href="https://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress.org</a></li> </ul>
</div> <!-- end .et_pb_widget --> </div> <!-- end #sidebar -->
</div> <!-- #content-area -->
</div> <!-- .container -->
</div> <!-- #main-content -->
<footer id="main-footer">
<div id="footer-bottom">
<div class="container clearfix">
<ul class="et-social-icons">
<li class="et-social-icon et-social-facebook">
<a class="icon" href="#">
<span>Facebook</span>
</a>
</li>
<li class="et-social-icon et-social-twitter">
<a class="icon" href="#">
<span>Twitter</span>
</a>
</li>
<li class="et-social-icon et-social-google-plus">
<a class="icon" href="#">
<span>Google</span>
</a>
</li>
<li class="et-social-icon et-social-rss">
<a class="icon" href="https://truepoint.co.za/feed/">
<span>RSS</span>
</a>
</li>
</ul><p id="footer-info">Designed by <a href="http://www.elegantthemes.com" title="Premium WordPress Themes">Elegant Themes</a> | Powered by <a href="http://www.wordpress.org">WordPress</a></p> </div> <!-- .container -->
</div>
</footer> <!-- #main-footer -->
</div> <!-- #et-main-area -->
</div> <!-- #page-container -->
<style id="et-builder-page-custom-style" type="text/css">
				 .et_pb_section { background-color: ; }
			</style><script src="https://truepoint.co.za/wp-content/themes/Divi/includes/builder/scripts/frontend-builder-global-functions.js?ver=3.0.51" type="text/javascript"></script>
<script src="https://truepoint.co.za/wp-content/themes/Divi/includes/builder/scripts/jquery.mobile.custom.min.js?ver=3.0.51" type="text/javascript"></script>
<script src="https://truepoint.co.za/wp-content/themes/Divi/js/custom.js?ver=3.0.51" type="text/javascript"></script>
<script src="https://truepoint.co.za/wp-content/themes/Divi/includes/builder/scripts/jquery.fitvids.js?ver=3.0.51" type="text/javascript"></script>
<script src="https://truepoint.co.za/wp-content/themes/Divi/includes/builder/scripts/waypoints.min.js?ver=3.0.51" type="text/javascript"></script>
<script src="https://truepoint.co.za/wp-content/themes/Divi/includes/builder/scripts/jquery.magnific-popup.js?ver=3.0.51" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var et_pb_custom = {"ajaxurl":"https:\/\/truepoint.co.za\/wp-admin\/admin-ajax.php","images_uri":"https:\/\/truepoint.co.za\/wp-content\/themes\/Divi\/images","builder_images_uri":"https:\/\/truepoint.co.za\/wp-content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"e397500f35","subscription_failed":"Please, check the fields below to make sure you entered the correct information.","et_ab_log_nonce":"687647d64b","fill_message":"Please, fill in the following fields:","contact_error_message":"Please, fix the following errors:","invalid":"Invalid email","captcha":"Captcha","prev":"Prev","previous":"Previous","next":"Next","wrong_captcha":"You entered the wrong number in captcha.","is_builder_plugin_used":"","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","is_ab_testing_active":"","page_id":"1","unique_test_id":"","ab_bounce_rate":"5","is_cache_plugin_active":"yes","is_shortcode_tracking":""};
/* ]]> */
</script>
<script src="https://truepoint.co.za/wp-content/themes/Divi/includes/builder/scripts/frontend-builder-scripts.js?ver=3.0.51" type="text/javascript"></script>
<script src="https://truepoint.co.za/wp-includes/js/wp-embed.min.js?ver=9efe99cd7d248b162cd34d3fd324f2e7" type="text/javascript"></script>
</body>
</html>