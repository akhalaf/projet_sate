<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://xaaslabs.com/xmlrpc.php" rel="pingback"/>
<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
<title>Powering Robotic Inspection</title>
<link href="//cdnjs.cloudflare.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link crossorigin="" href="https://fonts.gstatic.com" rel="preconnect"/>
<link href="https://xaaslabs.com/feed/" rel="alternate" title="Powering Robotic Inspection » Feed" type="application/rss+xml"/>
<link href="https://xaaslabs.com/comments/feed/" rel="alternate" title="Powering Robotic Inspection » Comments Feed" type="application/rss+xml"/>
<link href="https://xaaslabs.com/home/feed/" rel="alternate" title="Powering Robotic Inspection » Analytics for Asset Integrity Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/xaaslabs.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.2"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://xaaslabs.com/wp-includes/css/dist/block-library/style.min.css?ver=5.3.2" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xaaslabs.com/wp-includes/css/dist/block-library/theme.min.css?ver=5.3.2" id="wp-block-library-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xaaslabs.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.6" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xaaslabs.com/wp-content/plugins/wp-live-chat-support/css/wplcstyle.css?ver=8.1.5" id="wplc-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="wplc-style-inline-css" type="text/css">
#wp-live-chat-header { background:url('https://xaaslabs.com/wp-content/plugins/wp-live-chat-support/images/iconRetina.png') no-repeat; background-size: cover; }  #wp-live-chat-header.active { background:url('https://xaaslabs.com/wp-content/plugins/wp-live-chat-support/images/iconCloseRetina.png') no-repeat; background-size: cover; } #wp-live-chat-4 { background:url('https://xaaslabs.com/wp-content/plugins/wp-live-chat-support/images/bg/cloudy.jpg') repeat; background-size: cover; }
</style>
<link href="https://xaaslabs.com/wp-content/plugins/wp-live-chat-support/css/themes/theme-default.css?ver=8.1.5" id="wplc-theme-palette-default-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xaaslabs.com/wp-content/plugins/wp-live-chat-support/css/themes/modern.css?ver=8.1.5" id="wplc-theme-modern-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xaaslabs.com/wp-content/plugins/wp-live-chat-support/css/themes/position-bottom-right.css?ver=8.1.5" id="wplc-theme-position-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xaaslabs.com/wp-content/plugins/wp-live-chat-support/includes/blocks/wplc-chat-box/wplc_gutenberg_template_styles.css?ver=8.1.5" id="wplc-gutenberg-template-styles-user-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xaaslabs.com/wp-content/themes/NDE/css/bootstrap.min.css" id="nde-bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xaaslabs.com/wp-content/themes/NDE/css/fonts.css" id="nde-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xaaslabs.com/wp-content/themes/NDE/css/style.css" id="nde-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xaaslabs.com/wp-content/themes/NDE/css/responsive.css" id="nde-responsive-css" media="" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" id="nde-font-awesome-css" media="" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Merriweather%3A400%2C700%2C900%2C400italic%2C700italic%2C900italic%7CMontserrat%3A400%2C700%7CInconsolata%3A400&amp;subset=latin%2Clatin-ext" id="twentysixteen-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xaaslabs.com/wp-content/themes/twentysixteen/genericons/genericons.css?ver=3.4.1" id="genericons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xaaslabs.com/wp-content/themes/NDE/style.css?ver=5.3.2" id="twentysixteen-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xaaslabs.com/wp-content/themes/twentysixteen/css/blocks.css?ver=20181230" id="twentysixteen-block-style-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 10]>
<link rel='stylesheet' id='twentysixteen-ie-css'  href='https://xaaslabs.com/wp-content/themes/twentysixteen/css/ie.css?ver=20160816' type='text/css' media='all' />
<![endif]-->
<!--[if lt IE 9]>
<link rel='stylesheet' id='twentysixteen-ie8-css'  href='https://xaaslabs.com/wp-content/themes/twentysixteen/css/ie8.css?ver=20160816' type='text/css' media='all' />
<![endif]-->
<!--[if lt IE 8]>
<link rel='stylesheet' id='twentysixteen-ie7-css'  href='https://xaaslabs.com/wp-content/themes/twentysixteen/css/ie7.css?ver=20160816' type='text/css' media='all' />
<![endif]-->
<script src="https://xaaslabs.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://xaaslabs.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='https://xaaslabs.com/wp-content/themes/twentysixteen/js/html5.js?ver=3.7.3'></script>
<![endif]-->
<link href="https://xaaslabs.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://xaaslabs.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://xaaslabs.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3.2" name="generator"/>
<link href="https://xaaslabs.com/" rel="canonical"/>
<link href="https://xaaslabs.com/" rel="shortlink"/>
<link href="https://xaaslabs.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fxaaslabs.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://xaaslabs.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fxaaslabs.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<script type="text/javascript">
    var wplc_ajaxurl = 'https://xaaslabs.com/wp-admin/admin-ajax.php';
    var wplc_nonce = '06d4c00e38';
  </script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-86395881-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-86395881-4');
</script>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
</head>
<body class="home page-template page-template-page-templates page-template-page-template-home page-template-page-templatespage-template-home-php page page-id-2 wp-embed-responsive">
<div class="header">
<div class="container-fluid">
<div class="row">
<div class="col-lg-4">
<div class="nde-wizard text-left">
<h3><a href="https://xaaslabs.com">Powering Robotic Inspection</a></h3>
</div>
</div>
<div class="col-lg-8">
<nav class="navbar navbar-expand-lg navbar-light navgation bg-light">
<button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button">
<span class="menu-bar"><i aria-hidden="true" class="fa fa-bars"></i></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
<div class="menu-header-container"><ul class="navbar-nav mr-auto" id="menu-header"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6" id="menu-item-6"><a href="#product">Product</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8" id="menu-item-8"><a href="#contact">Contact</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-9" id="menu-item-9"><a href="#contact">Learn More</a></li>
</ul></div> </div>
</nav>
</div>
</div>
</div>
</div>
<div class="main-box">
<div class="carousel slide" data-ride="carousel" id="carouselExampleIndicators">
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#carouselExampleIndicators"></li>
<li class="other" data-slide-to="1" data-target="#carouselExampleIndicators"></li>
<li class="other" data-slide-to="2" data-target="#carouselExampleIndicators"></li>
</ol>
<div class="carousel-inner" role="listbox">
<div class="carousel-item active">
<div class="slider-hide-on-mobile">
<div class="overlay" style="background: #1e1e1e"></div>
<video autoplay="" class="video" loop="" muted="" poster="https://xaaslabs.com/wp-content/uploads/2018/06/poster1.jpg" preload="true" webkitenterfullscreen="" width="100%">
<source codecs="avc1.42E01E, mp4a.40.2" src="https://xaaslabs.com/wp-content/uploads/2018/06/NDE001_ORef01.mp4" type="video/mp4">
</source></video>
</div>
</div>
<div class="carousel-item other">
<div class="slider-hide-on-mobile">
<div class="overlay" style="background: #0a0a0a"></div>
<video autoplay="" class="video" loop="" muted="" poster="https://xaaslabs.com/wp-content/uploads/2018/06/poster2.jpg" preload="true" webkitenterfullscreen="" width="100%">
<source codecs="avc1.42E01E, mp4a.40.2" src="https://xaaslabs.com/wp-content/uploads/2018/06/NDE002_OffS.mp4" type="video/mp4">
</source></video>
</div>
</div>
<div class="carousel-item other">
<div class="slider-hide-on-mobile">
<div class="overlay" style="background: #020202"></div>
<video autoplay="" class="video" loop="" muted="" poster="https://xaaslabs.com/wp-content/uploads/2018/06/poster3.jpg" preload="true" webkitenterfullscreen="" width="100%">
<source codecs="avc1.42E01E, mp4a.40.2" src="https://xaaslabs.com/wp-content/uploads/2018/06/NDE005_AstOA.mp4" type="video/mp4">
</source></video>
</div>
</div>
</div>
</div>
<div class="startup-today text-center">
<h2 style="text-align: center;">COGNITIVE ANALYTICS</h2>
<h2 style="text-align: center;">for Asset and Site Inspection &amp; Maintenance</h2>
<p style="text-align: center;">
</p><p><a href="#contact">Learn More</a></p>
</div>
</div>
<div class="section-do-not-wait" id="contact">
<div class="container">
<div class="row">
<div class="col-lg-6">
<div class="do-not-wait">
<h2>Analytics for Asset Integrity</h2>
<p>The Industry’s First and Most Comprehensive AI powered Analytics library targeting Asset owners and Inspection Service Providers.</p>
</div>
<div class="read-more">
<a href="https://cognitivendt01.xaaslabs.com/">Sign up for a free trial</a>
</div>
</div>
<div class="col-lg-6">
<div class="the-form-below text-center">
<h4>TO LEARN MORE, PLEASE FILL OUT THE FORM BELOW </h4>
<div class="text-center">
<div class="wpcf7" dir="ltr" id="wpcf7-f14-p2-o1" lang="en-US" role="form">
<div class="screen-reader-response"></div>
<form action="/#wpcf7-f14-p2-o1" class="wpcf7-form" method="post" novalidate="novalidate">
<div style="display: none;">
<input name="_wpcf7" type="hidden" value="14"/>
<input name="_wpcf7_version" type="hidden" value="5.1.6"/>
<input name="_wpcf7_locale" type="hidden" value="en_US"/>
<input name="_wpcf7_unit_tag" type="hidden" value="wpcf7-f14-p2-o1"/>
<input name="_wpcf7_container_post" type="hidden" value="2"/>
</div>
<div class="row">
<div class="col-md-6">
<div class="form-group">
<span class="wpcf7-form-control-wrap text-375"><input aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" name="text-375" placeholder="Name" size="40" type="text" value=""/></span>
</div>
</div>
<div class="col-md-6">
<div class="form-group">
<span class="wpcf7-form-control-wrap text-376"><input aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" name="text-376" placeholder="Company" size="40" type="text" value=""/></span>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<div class="form-group">
<span class="wpcf7-form-control-wrap tel-377"><input aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel form-control" name="tel-377" placeholder="Phone" size="40" type="tel" value=""/></span>
</div>
</div>
<div class="col-md-6">
<div class="form-group">
<span class="wpcf7-form-control-wrap email-378"><input aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" name="email-378" placeholder="Email" size="40" type="email" value=""/></span>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<span class="wpcf7-form-control-wrap textarea-154"><textarea aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required form-control" cols="40" name="textarea-154" placeholder="Message" rows="10"></textarea></span>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<input class="wpcf7-form-control wpcf7-submit btn btn-primary s-btn" type="submit" value="Submit"/>
</div>
</div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div> </div>
</div>
</div>
</div>
</div>
</div>
<div class="section-web-analytics" id="product">
<div class="container custom-con">
<div class="row">
<div class="col-lg-12">
<div class="web-analytics text-center">
<h2>INSERT INSIGHTFUL ANALYTICS INTO YOUR ASSET INTEGRITY MANAGEMENT WORKFLOWS</h2>
<p><span style="color: #ff9900;"><strong>Designed in Close collaboration with Asset Owner/Operators, Service Providers and Inspection Equipment Vendors.  We serve Oil &amp; Gas, Chemicals &amp; Power Generation Infrastructure.</strong></span></p>
<a href="#"><i aria-hidden="true" class="fa fa-angle-down"></i></a>
</div>
</div>
</div>
<div class="typesetting">
<div class="row">
<div class="col-lg-5 order-last">
<div class="profiling text-right">
<img src="https://xaaslabs.com/wp-content/uploads/2020/10/ElectricTDElp.png"/>
</div>
</div>
<div class="col-lg-7">
<div class="customer-profiling">
<h2></h2>
<h1 style="text-align: center;"><span style="color: #ff9900;"><strong>SuperCharging Robotic Inspection</strong></span></h1>
</div>
</div>
</div>
</div>
<div class="typesetting">
<div class="row">
<div class="col-lg-5 text-left">
<div class="profiling text-right">
<img src="https://xaaslabs.com/wp-content/uploads/2018/07/FieldInspectorsEllip.png"/>
</div>
</div>
<div class="col-lg-7">
<div class="customer-profiling">
<h2></h2>
<h1 style="text-align: center;"><span style="color: #ff9900;">Empowering the Inspection &amp; Maintenance Professional</span></h1>
</div>
</div>
</div>
</div>
<div class="typesetting">
<div class="row">
<div class="col-lg-5 order-last">
<div class="profiling text-right">
<img src="https://xaaslabs.com/wp-content/uploads/2018/06/LidarSonarScan_001.png"/>
</div>
</div>
<div class="col-lg-7">
<div class="customer-profiling">
<h2></h2>
<h1><span style="color: #ff9900;"><strong>Insightful Reporting</strong></span></h1>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="section-footer" style="background: url(https://xaaslabs.com/wp-content/uploads/2018/06/TM_4_50x150.jpg">
<div class="container">
<div class="row">
<div class="col-lg-5">
</div>
<div class="col-lg-7">
<div class="we-succeed text-left">
<h4>We Succeed When You Succeed</h4>
<h5>Sign up for a free trial </h5>
<div class="we-succeed text-left">
<p class="p1">During the trial period You have access to baseline features on supported standard platforms,  regular updates and bug fixes.</p>
</div>
</div>
<div class="get-started-now">
<a href="">GET STARTED NOW</a>
</div>
</div>
</div>
</div>
</div>
<div class="section-all-rights-reserved-footer">
<div class="container">
<div class="row">
<div class="col-lg-3">
<div class="copyright">
							            © Copyright 2018-2019, All Rights Reserved.        						</div>
</div>
<div class="col-lg-6">
<div class="footer-menu">
<div class="menu-footer-menu-container"><ul class="menu" id="menu-footer-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10" id="menu-item-10"><a href="#">Support</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11" id="menu-item-11"><a href="#contact">Contact Us</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12" id="menu-item-12"><a href="#">Disclaimer</a></li>
</ul></div> </div>
</div>
<div class="col-lg-3">
<div class="socail-sec">
<a href="            https://www.facebook.com/        " target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a>
<a href="            https://twitter.com/        " target="_blank"><i aria-hidden="true" class="fa fa-twitter"></i></a>
<a href="            https://www.youtube.com/        " target="_blank"><i aria-hidden="true" class="fa fa-youtube"></i></a>
<a href="            https://www.instagram.com/        " target="_blank"><i aria-hidden="true" class="fa fa-instagram"></i></a>
</div>
</div>
</div>
</div>
</div>
<script>
</script>
<script type="text/javascript">
var $j = jQuery.noConflict();

   $j(document).ready(function() {
   // $j(".video").trigger('click');
   $j(".video").bind("click", function() {
   var vid = $j(this).get(0);
   if (vid.paused) { vid.play(); }
   else { vid.pause(); }
   }); 
    $j('#carouselExampleIndicators').carousel()
}); 


$j( ".file a" ).on( "click", function() {
    $j('.download-message').text($j(this).text());
    setTimeout( function(){ 
        $j('.download-btn').trigger("click")
    }, 1000);
    setTimeout( function(){ 
        location.reload();
    }, 1500);
    
    
});
</script>
<script src="https://xaaslabs.com/wp-content/plugins/wp-live-chat-support/js/jquery-cookie.js?ver=8.1.5" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var config = {"baseurl":"https:\/\/xaaslabs.com\/wp-content\/plugins\/wp-live-chat-support\/","serverurl":"https:\/\/tcx-live-chat.appspot.com","enable_typing_preview":"","ring_override":"https:\/\/xaaslabs.com\/wp-content\/plugins\/wp-live-chat-support\/includes\/sounds\/general\/ring.wav","message_override":"https:\/\/xaaslabs.com\/wp-content\/plugins\/wp-live-chat-support\/includes\/sounds\/general\/ding.mp3","current_wpuserid":"0","allowed_upload_extensions":"jpg|jpeg|png|gif|bmp|mp4|mp3|mpeg|ogg|ogm|ogv|webm|avi|wav|mov|doc|docx|xls|xlsx|pub|pubx|pdf|csv|txt","wplc_use_node_server":"1","wplc_localized_string_is_typing_single":" is typing...","wplc_user_default_visitor_name":"Guest","wplc_text_no_visitors":"There are no visitors on your site at the moment","wplc_text_connection_lost":"Connection to the server lost, reconnecting...","wplc_text_not_accepting_chats":"Agent offline - not accepting chats","wplc_text_minimizechat":"Minimize Chat","wplc_text_endchat":"End Chat","country_code":"","country_name":"","date_days":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"date_months":["January","February","March","April","May","June","July","August","September","October","November","December"]};
/* ]]> */
</script>
<script src="https://xaaslabs.com/wp-content/plugins/wp-live-chat-support/js/wplc_common_node.js?ver=8.1.5" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/xaaslabs.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://xaaslabs.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.6" type="text/javascript"></script>
<script src="https://xaaslabs.com/wp-content/themes/NDE/js/jquery.min.js" type="text/javascript"></script>
<script src="https://xaaslabs.com/wp-content/themes/NDE/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://xaaslabs.com/wp-content/themes/NDE/js/custom.js" type="text/javascript"></script>
<script src="https://xaaslabs.com/wp-content/themes/twentysixteen/js/skip-link-focus-fix.js?ver=20160816" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var screenReaderText = {"expand":"expand child menu","collapse":"collapse child menu"};
/* ]]> */
</script>
<script src="https://xaaslabs.com/wp-content/themes/twentysixteen/js/functions.js?ver=20181230" type="text/javascript"></script>
<script src="https://xaaslabs.com/wp-includes/js/wp-embed.min.js?ver=5.3.2" type="text/javascript"></script>
</body>
<script>'undefined'=== typeof _trfq || (window._trfq = []);'undefined'=== typeof _trfd && (window._trfd=[]),_trfd.push({'tccl.baseHost':'secureserver.net'}),_trfd.push({'ap':'cpsh'},{'server':'p3plcpnl0929'}) // Monitoring performance to make your website faster. If you want to opt-out, please contact web hosting support.</script><script src="https://img1.wsimg.com/tcc/tcc_l.combined.1.0.6.min.js"></script></html>
