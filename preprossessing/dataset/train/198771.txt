<!DOCTYPE html>
<html lang="fa-IR">
<head id="Head">
<!--*********************************************-->
<!-- DNN Platform - http://www.dnnsoftware.com   -->
<!-- Copyright (c) 2002-2017, by DNN Corporation -->
<!--*********************************************-->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<script type="text/javascript">!function(){function t(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,localStorage.getItem("rayToken")?t.src="https://app.raychat.io/scripts/js/"+o+"?rid="+localStorage.getItem("rayToken")+"&href="+window.location.href:t.src="https://app.raychat.io/scripts/js/"+o;var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=document,a=window,o="aafd58e2-300b-4af0-96f4-d029792bc83c";"complete"==e.readyState?t():a.attachEvent?a.attachEvent("onload",t):a.addEventListener("load",t,!1)}();</script> <title>
	در حال حاضر بازاریاب فعال نمی‌باشد.
</title><meta content="در حال حاضر بازاریاب فعال نمی‌باشد." id="MetaDescription" name="DESCRIPTION"/><meta content="شبکه,بازاریاب,تجارت,الکترونیک,تجاری,خرید,فروش,کالا,خدمات,فروشگاه,آنلاین,اینترنتی,بهترین,ارزان‌ترین,نزدیک‌ترین,سریع‌ترین,گارانتی,ضمانت,راه‌اندازی,قیمت,آدرس,نقشه,معرفی,بررسی,مقایسه,نقد,مشخصات,ویژگی,شبکه,تجاری,DotNetNuke,DNN" id="MetaKeywords" name="KEYWORDS"/><meta content="DotNetNuke " id="MetaGenerator" name="GENERATOR"/><meta content="INDEX, FOLLOW" id="MetaRobots" name="ROBOTS"/><link href="/Resources/Shared/stylesheets/dnndefault/7.0.0/default.rtl.css?cdv=320" rel="stylesheet" type="text/css"/><link href="/Portals/_default/skins/xcillion/skin.rtl.css?cdv=320" rel="stylesheet" type="text/css"/><link href="/Portals/0/portal.css?cdv=320" rel="stylesheet" type="text/css"/><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js?cdv=320" type="text/javascript"></script><script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js?cdv=320" type="text/javascript"></script><script type="text/javascript">
	if (typeof jQuery == 'undefined') {document.write('<script src="/Resources/libraries/jQuery/01_09_01/jquery.js" type="text/javascript"></' + 'script>');}
</script><link href="/Portals/0/favicon.ico?ver=1397-07-07-225720-373" rel="SHORTCUT ICON" type="image/x-icon"/>
<script type="text/javascript">
			      var _gaq = _gaq || [];
			      _gaq.push(['_setAccount', 'UA-84865865-1']);
			      _gaq.push(['_trackPageview']);
			      
			 
			      (function() {
				    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			      })();
			    </script>
</head>
<body class="rtl" id="Body">
<form action="/" enctype="multipart/form-data" id="Form" method="post">
<div class="aspNetHidden">
<input id="__VIEWSTATE" name="__VIEWSTATE" type="hidden" value="spIoAEtFSMNkPwpqbfa7d7fC0sMyfelkAe4vOsi30u4W/m6ZSPIJAL7waeAlcGtgWACssPVAfr+lUI/RuEbDGzr7PJtwtjx44D3MrGjOfVbffuiFta/k1nPoJVM="/>
</div>
<script type="text/javascript">
//<![CDATA[
var __cultureInfo = {"name":"fa-IR","numberFormat":{"CurrencyDecimalDigits":0,"CurrencyDecimalSeparator":"/","IsReadOnly":false,"CurrencyGroupSizes":[3],"NumberGroupSizes":[3],"PercentGroupSizes":[3],"CurrencyGroupSeparator":",","CurrencySymbol":"","NaNSymbol":"ناعدد","CurrencyNegativePattern":6,"NumberNegativePattern":3,"PercentPositivePattern":0,"PercentNegativePattern":11,"NegativeInfinitySymbol":"-∞","NegativeSign":"-","NumberDecimalDigits":2,"NumberDecimalSeparator":".","NumberGroupSeparator":",","CurrencyPositivePattern":1,"PositiveInfinitySymbol":"∞","PositiveSign":"+","PercentDecimalDigits":2,"PercentDecimalSeparator":"/","PercentGroupSeparator":",","PercentSymbol":"%","PerMilleSymbol":"‰","NativeDigits":["۰","۱","۲","۳","۴","۵","۶","۷","۸","۹"],"DigitSubstitution":0},"dateTimeFormat":{"AMDesignator":"ق.ظ","Calendar":{"MinSupportedDateTime":"\/Date(-42531885000000)\/","MaxSupportedDateTime":"\/Date(253402288199999)\/","AlgorithmType":1,"Eras":[1],"TwoDigitYearMax":1409,"IsReadOnly":false},"DateSeparator":"/","FirstDayOfWeek":6,"CalendarWeekRule":0,"FullDateTimePattern":"dddd, dd MMMM,yyyy hh:mm:ss tt","LongDatePattern":"dddd, dd MMMM,yyyy","LongTimePattern":"hh:mm:ss tt","MonthDayPattern":"dd MMMM","PMDesignator":"ب.ظ","RFC1123Pattern":"ddd, dd MMM yyyy HH\u0027:\u0027mm\u0027:\u0027ss \u0027GMT\u0027","ShortDatePattern":"yyyy/MM/dd","ShortTimePattern":"hh:mm tt","SortableDateTimePattern":"yyyy\u0027-\u0027MM\u0027-\u0027dd\u0027T\u0027HH\u0027:\u0027mm\u0027:\u0027ss","TimeSeparator":":","UniversalSortableDateTimePattern":"yyyy\u0027-\u0027MM\u0027-\u0027dd HH\u0027:\u0027mm\u0027:\u0027ss\u0027Z\u0027","YearMonthPattern":"yyyy, MMMM","AbbreviatedDayNames":["ی","د","س","چ","پ","ج","ش"],"ShortestDayNames":["ی","د","س","چ","پ","ج","ش"],"DayNames":["یکشنبه","دوشنبه","ﺳﻪشنبه","چهارشنبه","پنجشنبه","جمعه","شنبه"],"AbbreviatedMonthNames":["فروردین","اردیبهشت","خرداد","تیر","مرداد","شهریور","مهر","آبان","آذر","دی","بهمن","اسفند",""],"MonthNames":["فروردین","اردیبهشت","خرداد","تیر","مرداد","شهریور","مهر","آبان","آذر","دی","بهمن","اسفند",""],"IsReadOnly":false,"NativeCalendarName":"تقویم هجری شمسی","AbbreviatedMonthGenitiveNames":["فروردین","اردیبهشت","خرداد","تیر","مرداد","شهریور","مهر","آبان","آذر","دی","بهمن","اسفند",""],"MonthGenitiveNames":["فروردین","اردیبهشت","خرداد","تیر","مرداد","شهریور","مهر","آبان","آذر","دی","بهمن","اسفند",""]},"eras":[1,"ه.ش",null,0]};//]]>
</script>
<script src="https://ajax.aspnetcdn.com/ajax/4.6/1/MicrosoftAjax.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
(window.Sys && Sys._Application && Sys.Observer)||document.write('<script type="text/javascript" src="/ScriptResource.axd?d=NJmAwtEo3IqjaUMHRGG5hD3jYJQFnKhH31df_LA3tuX9ZJHr7DmBIj4cUEa71Lt2BdSpP7Z_rHltfXG_purog0BNlIe-c3oB5F-tgXbtx5jJKEFOuvcmOGKdNTOmvjCSgsqcQw2&t=ffffffffb4e3605f"><\/script>');//]]>
</script>
<script src="https://ajax.aspnetcdn.com/ajax/4.6/1/MicrosoftAjaxWebForms.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
(window.Sys && Sys.WebForms)||document.write('<script type="text/javascript" src="/ScriptResource.axd?d=dwY9oWetJoIdDIxUeP5QPW7XFerO5xzSRwIXgcz7q-7FTh18g6X3bpHIdrTPsPmRx5Y6rqxitE153oOCiQFtaAH-C7WQ4DgtmbZiAbHlNMrfyLgmbG6v65K3OVOsvzv4lb7kde7rzFA7t_Vj0&t=ffffffffb4e3605f"><\/script>');//]]>
</script>
<div class="aspNetHidden">
<input id="__VIEWSTATEGENERATOR" name="__VIEWSTATEGENERATOR" type="hidden" value="CA0B0334"/>
<input id="__VIEWSTATEENCRYPTED" name="__VIEWSTATEENCRYPTED" type="hidden" value=""/>
<input id="__EVENTVALIDATION" name="__EVENTVALIDATION" type="hidden" value="CXklsCn30sD43aOemcLNgRolZxZI5vniDq1dw7d1sbcn2T2z7qSGhHnFpALAN+DrFUJlTf5PvR/8DZ8FLy7fTnmIZeXbuK7S6sS4xvAEgvhN2+e7"/>
</div><script src="/js/dnn.modalpopup.js?cdv=320" type="text/javascript"></script><script src="/js/dnncore.js?cdv=320" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 600, '');
//]]>
</script>
<div style="display:none;"><a href="http://www.ezweb.ir">طراحی سایت</a></div>
<style type="text/css">
    body#Body {background:#fff;}
</style>
<div id="dnn_ContentPane"><div class="DnnModule DnnModule-DNN_HTML DnnModule-645"><a name="645"></a>
<div class="DNNContainer_noTitle">
<div id="dnn_ctr645_ContentPane"><!-- Start_Module_645 --><div class="DNNModuleContent ModDNNHTMLC" id="dnn_ctr645_ModuleContent">
<div class="Normal" id="dnn_ctr645_HtmlModule_lblContent">
<p class="center pt-60"><strong>در حال حاضر بازاریاب غیرفعال است.</strong></p>
</div>
</div><!-- End_Module_645 --></div>
<div class="clear"></div>
</div>
</div></div>
<input id="ScrollTop" name="ScrollTop" type="hidden"/>
<input autocomplete="off" id="__dnnVariable" name="__dnnVariable" type="hidden"/>
<script type="text/javascript">$(document).ready(function () { if ($('div').hasClass('RadPicker')) {$("#Body").append("<script src='/js/PersianRadCalendar.js' type='text/javascript'><\/script>");}});</script><script type="text/javascript">$(document).ready(function () { if ($('div').hasClass('RadEditor')) {$("#Body").append("<script src='/js/PersianRadEditor.js' type='text/javascript'><\/script>");}});</script></form>
<!--CDF(Javascript|/js/dnncore.js?cdv=320)--><!--CDF(Javascript|/js/dnn.modalpopup.js?cdv=320)--><!--CDF(Css|/Resources/Shared/stylesheets/dnndefault/7.0.0/default.rtl.css?cdv=320)--><!--CDF(Css|/Portals/_default/skins/xcillion/skin.rtl.css?cdv=320)--><!--CDF(Css|/Portals/0/portal.css?cdv=320)--><!--CDF(Javascript|https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js?cdv=320)--><!--CDF(Javascript|https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js?cdv=320)-->
</body>
</html>