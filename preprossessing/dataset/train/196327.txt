<html><head><title>baseportal - Web-Datenbank für HTML-Seiten - einfach, schnell und kostenlos!</title>
<meta content="Professionell HTML-Seiten erstellen mit Web-Datenbanken. Einfach, schnell, kostenlos!" name="description"/>
<meta content="web-datenbank, web-datenbanken, html, kostenlos, webmaster, tools, webbasiert, web-basiert, datenbank, datenbanken, webseiten, webseite, website, homepage, homepages, php, perl, mysql, sql, asp, access, erstellen, generieren, dynamisch, erzeugen, bauen, anwendung, anwendungen, web-service, web-services, einfach, schnell, leistungsfähig, programme, programmierung, cms, content management system" name="keywords"/>
<link href="/favicon.ico" rel="shortcut icon"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="index,follow" name="robots"/>
<meta content="de" name="language"/>
<script>if(top!=self) top.location=self.location</script>
<!-- Quantcast Choice. Consent Manager Tag v2.0 (for TCF 2.0) -->
<script async="true" type="text/javascript">
(function() {
    var host = window.location.hostname;
    var element = document.createElement('script');
    var firstScript = document.getElementsByTagName('script')[0];
    var url = 'https://quantcast.mgr.consensu.org'
        .concat('/choice/', '6Fv0cGNfc_bw8', '/', host, '/choice.js')
    var uspTries = 0;
    var uspTriesLimit = 3;
    element.async = true;
    element.type = 'text/javascript';
    element.src = url;

    firstScript.parentNode.insertBefore(element, firstScript);

    function makeStub() {
        var TCF_LOCATOR_NAME = '__tcfapiLocator';
        var queue = [];
        var win = window;
        var cmpFrame;

        function addFrame() {
            var doc = win.document;
            var otherCMP = !!(win.frames[TCF_LOCATOR_NAME]);

            if (!otherCMP) {
                if (doc.body) {
                    var iframe = doc.createElement('iframe');

                    iframe.style.cssText = 'display:none';
                    iframe.name = TCF_LOCATOR_NAME;
                    doc.body.appendChild(iframe);
                } else {
                    setTimeout(addFrame, 5);
                }
            }
            return !otherCMP;
        }

        function tcfAPIHandler() {
            var gdprApplies;
            var args = arguments;

            if (!args.length) {
                return queue;
            } else if (args[0] === 'setGdprApplies') {
                if (
                    args.length > 3 &&
                    args[2] === 2 &&
                    typeof args[3] === 'boolean'
                ) {
                    gdprApplies = args[3];
                    if (typeof args[2] === 'function') {
                        args[2]('set', true);
                    }
                }
            } else if (args[0] === 'ping') {
                var retr = {
                    gdprApplies: gdprApplies,
                    cmpLoaded: false,
                    cmpStatus: 'stub'
                };

                if (typeof args[2] === 'function') {
                    args[2](retr);
                }
            } else {
                queue.push(args);
            }
        }

        function postMessageEventHandler(event) {
            var msgIsString = typeof event.data === 'string';
            var json = {};

            try {
                if (msgIsString) {
                    json = JSON.parse(event.data);
                } else {
                    json = event.data;
                }
            } catch (ignore) {}

            var payload = json.__tcfapiCall;

            if (payload) {
                window.__tcfapi(
                    payload.command,
                    payload.version,
                    function(retValue, success) {
                        var returnMsg = {
                            __tcfapiReturn: {
                                returnValue: retValue,
                                success: success,
                                callId: payload.callId
                            }
                        };
                        if (msgIsString) {
                            returnMsg = JSON.stringify(returnMsg);
                        }
                        event.source.postMessage(returnMsg, '*');
                    },
                    payload.parameter
                );
            }
        }

        while (win) {
            try {
                if (win.frames[TCF_LOCATOR_NAME]) {
                    cmpFrame = win;
                    break;
                }
            } catch (ignore) {}

            if (win === window.top) {
                break;
            }
            win = win.parent;
        }
        if (!cmpFrame) {
            addFrame();
            win.__tcfapi = tcfAPIHandler;
            win.addEventListener('message', postMessageEventHandler, false);
        }
    };

    if (typeof module !== 'undefined') {
        module.exports = makeStub;
    } else {
        makeStub();
    }

    var uspStubFunction = function() {
        var arg = arguments;
        if (typeof window.__uspapi !== uspStubFunction) {
            setTimeout(function() {
                if (typeof window.__uspapi !== 'undefined') {
                    window.__uspapi.apply(window.__uspapi, arg);
                }
            }, 500);
        }
    };

    var checkIfUspIsReady = function() {
        uspTries++;
        if (window.__uspapi === uspStubFunction && uspTries < uspTriesLimit) {
            console.warn('USP is not accessible');
        } else {
            clearInterval(uspInterval);
        }
    };

    if (typeof window.__uspapi === 'undefined') {
        window.__uspapi = uspStubFunction;
        var uspInterval = setInterval(checkIfUspIsReady, 6000);
    }
})();
</script>
<!-- End Quantcast Choice. Consent Manager Tag v2.0 (for TCF 2.0) -->
</head>
<body bgcolor="ffffff" vlink="0000f0">
<font face="arial" size="2">
</font><center>
<table border="0" cellpadding="2" cellspacing="0" width="95%"><tr>
<td valign="bottom" width="33%"><font face="arial" size="6"><b><font color="d80000">base</font>portal</b>   </font></td>
<td align="middle" valign="bottom" width="33%"><font face="arial" size="1"><a href="http://baseportal.com">English</a> - Deutsch</font></td>
<td align="right" valign="bottom" width="33%"><font face="arial,helvetica" size="2"><i>"Helping you build a new world..."</i></font></td>
</tr><tr><td bgcolor="b0c0e0" colspan="3"><table border="0" cellpadding="0" cellspacing="0"><tr><td height="1"></td></tr></table></td></tr></table>
<br/>
<table border="0" cellpadding="4" cellspacing="0" width="95%">
<tr><td valign="top">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr><td bgcolor="e0e0e0"><font face="arial" size="3"> <b>Was ist baseportal?</b></font></td></tr>
<tr><td valign="top"><font face="arial"><font size="2">
<br/><ul>Erstellen Sie in wenigen Minuten Ihre eigene Web-Datenbank - einfach, schnell und völlig kostenlos!<p align="right"> . . . . . <font size="3"><a href="mit-baseportal-webseiten-erstellen.html">Mehr Info?</a>     </font><br/><br/></p></ul></font></font></td></tr>
<tr>
<td bgcolor="e0e0e0"><font face="arial" size="3"> <b>Wie fange ich an?</b></font></td>
</tr><tr><td>
<a href="tour/index.html"></a>
<font face="arial" size="2"><br/>
<ol><li>In unserer <font size="3"><a href="tour/index.html">Guided Tour</a></font> sehen Sie, was Sie mit baseportal alles machen können.
<p></p></li><li>Lernen Sie im <font size="3"><a href="/cgi-bin/baseportal.pl?htx=/main&amp;mode=regi&amp;_easystart=0">Easy Start</a></font> wie Sie in 5 Minuten Ihre eigene Datenbank erstellen.
<p></p></li><li>Lesen Sie die <font size="3"><a href="http://doku.baseportal.de">Dokumentation</a></font> und werden Sie ein baseportal-Profi.
<p></p></li><li>Bei Problemen wird Ihnen im <font size="3"><a href="http://forum.baseportal.de">Forum</a></font> schnell und kompetent geholfen.</li></ol></font>
<p><br/>
</p></td></tr>
<tr>
<td bgcolor="e0e0e0"><font face="arial" size="3"> <b>Was sagen die Fachleute?</b></font></td>
</tr><tr><td align="middle">
<table width="96%"><tr>
<td align="middle" width="20%"><img height="63" src="pics/pr/pcpro.gif" width="50"/></td>
<td align="middle" width="20%"><img height="40" src="pics/pr/coolspot.gif" width="40"/></td>
<td align="middle" width="20%"><img height="38" src="pics/pr/cbild.gif" width="90"/></td>
<td align="middle" width="20%"><img height="60" src="pics/pr/ct.gif" width="96"/></td>
<td align="middle" width="20%"><img height="45" src="pics/pr/ciao.gif" width="78"/></td>
</tr><tr>
<td align="middle" valign="top">
<font face="arial,helvetica"><font size="1">
Gesamtwertung</font><br/>
<font size="2"><b>SEHR GUT</b></font>
</font></td>
<td align="middle" valign="top"><font face="arial,helvetica" size="1">
Surftip von Web.de</font></td>
<td align="middle" valign="top"><font face="arial,helvetica" size="1">
<font face="arial,helvetica"><font size="1">
Testergebnis</font><br/>
<font size="2"><b>GUT</b></font></font></font></td>
<td align="middle" valign="top"><font face="arial,helvetica" size="1">
"Datenbank<br/>massgeschneidert"</font></td>
<td align="middle" valign="top"><font face="arial,helvetica" size="1">
volle 5 Sterne<br/>von 100% empfohlen</font></td>
</tr><tr>
<td colspan="3"><br/><font face="arial" size="2"></font></td><td align="right" colspan="2"><br/> . . . . . <a href="/baseportal/baseportal/presse"><font face="arial,helvetica" size="2">Mehr Presseartikel</font></a> </td>
</tr></table>
<br/>
</td></tr></table>
</td><td valign="top" width="33%">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td>
<table bgcolor="c0c0c0" border="0" cellpadding="0" cellspacing="1" width="100%"><tr><td><table bgcolor="f8f8f8" border="0" cellpadding="4" cellspacing="1" width="100%">
<tr><td bgcolor="e0e0e0"><font face="arial" size="3"> <b>Neu bei baseportal?</b></font></td></tr>
<tr><td align="middle" bgcolor="f8f8f8"><font face="arial" size="3">
<b><a href="/baseportal/main&amp;mode=regi">Kostenlos registrieren</a></b>
<br/><br/>
<font size="2">Für Einsteiger: <a href="/cgi-bin/baseportal.pl?htx=/main&amp;mode=regi&amp;_easystart=0">Easy Start</a></font><br/>
<br/>
</font></td></tr>
<tr><td bgcolor="e0e0e0"><font face="arial" size="3"> <b>Bereits registriert?</b></font></td></tr>
<tr><td align="middle" bgcolor="f8f8f8" valign="top">
<table>
<form action="https://baseportal.de/cgi-bin/baseportal.pl?htx=/main" enctype="multipart/form-data" method="post"><script>document.writeln('<inp'+'ut type=hidden na'+'me="_bas'+'eportal_as531.157270870265" value="1">')</script>
<input name="htx=" type="hidden" value="/main"/>
<tr><td align="right"><font face="arial" size="3">Name:</font></td><td><input name="uid=" size="12" type="text"/></td></tr>
<tr><td align="right"><font face="arial" size="3">Passwort:</font></td><td><input name="upw=" size="12" type="password"/></td></tr>
<tr><td></td><td align="middle"><input type="submit" value="Anmelden"/></td></tr>
<tr><td></td><td align="middle" height="30"><font face="arial" size="1"><a href="/baseportal/_baseportal/lostpw">Zugang vergessen?</a></font></td></tr>
</form>
</table>
</td></tr></table>
</td></tr></table>
</td></tr><tr><td height="8"></td></tr><tr><td valign="top">
<table border="0" cellpadding="4" cellspacing="1" width="100%">
<tr><td bgcolor="e0e0e0"><font face="arial" size="3"> <b>Statistik</b></font></td></tr>
<tr><td><font face="arial,helvetica"><font size="2">
<font size="1">Registrierte Nutzer:</font> 179368<br/><font size="1">Gerade angemeldet:</font> 504
<br/><font size="1">Forumbeiträge:</font> 68985<br/><font size="1">Letzter Beitrag:</font> 13.1, 15:33<br/>
<br/><br/>
</font></font></td></tr></table></td></tr><tr><td valign="top">
<table border="0" cellpadding="4" cellspacing="1" width="100%">
<tr><td bgcolor="e0e0e0"><font face="arial" size="3"> <b>Pressezitate</b></font></td></tr><td bgcolor="f8f8f8">
<font face="arial,helvetica" size="1">

"...klicken Sie sich mit baseportal Ihre eigene Online-Datenbank zusammen. Besonders lobenswert ist, dass Sie dafür nicht programmieren können müssen. Auch Einsteigern fällt es leicht, eine Datenbank mit Hilfe der Vorlagen und der Hilfe-Seiten anzulegen..."<div style="text-align:right"><font size="2">PC Magazin</font></div>
</font></td></table>
</td></tr></table>
</td></tr></table>
<table border="0" cellpadding="4" cellspacing="6" width="95%">
<tr><td bgcolor="e0e0e0" width="66%"><font face="arial" size="3"> <b>baseportal mieten: Keine Werbung und viele Zusatzfunktionen</b></font></td>
<td bgcolor="e0e0e0" width="33%"><font face="arial" size="3"> <b>baseportal kaufen</b></font></td>
</tr>
<tr><td valign="top" width="66%">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td valign="top">
<font face="arial" size="2">
Nutzen Sie die vielen Vorteile gegenüber der kostenlosen Version:<br/><font size="1">
<li type="square">baseportal unter der eigenen Domain<br/>
</li><li type="square">Keine Werbung, kein "powered by baseportal" mehr
</li><li type="square">Bilder+Dateien durch File-Upload
</li><li type="square">Freier EMail-Versand, eingebaute Volltextsuche, Chat!
</li><li type="square">Erweiterte Archivfunktionen, automatische Backups
</li><li type="square">Vertrags- und Betriebssicherheit</li></font>
</font></td><td valign="top"><font face="arial" size="2">
</font><center><a href="bpws.html"><img border="0" height="60" src="/pics/pakete/bpws.gif" vspace="0" width="116"/></a></center>
<br/><li type="square">mit und ohne Webspace
</li><li type="square">schon <a href="bpws.html">ab 3,99 Euro/Monat</a> !
</li></td></tr></table>
</td>
<td valign="top" width="33%">
<center><a href="https://back.baseportal.de/cgi-bin/baseportal.pl?htx=/baseportal/kauf/index"><img border="0" height="60" src="/pics/pakete/bpliz.gif" vspace="0" width="116"/></a></center>
<br/><font face="arial" size="2">
<font size="3"><a href="https://back.baseportal.de/cgi-bin/baseportal.pl?htx=/baseportal/kauf/index">Kaufen Sie eine Lizenz</a></font> und setzen Sie baseportal beim Provider Ihrer Wahl, lokal oder in Ihrem Intranet ein.<br/>
</font></td></tr></table>
<br/>
<table cellpadding="4" cellspacing="6" width="95%">
<tr>
<td bgcolor="e0e0e0" colspan="2" width="66%"><font face="arial" size="3"> <b>Verzeichnis Nutzer-Datenbanken</b></font></td>
<td bgcolor="e0e0e0" width="33%"><font face="arial" size="3"> <b>Neueste Einträge</b></font></td>
</tr>
<tr>
<td valign="top" width="33%"><font face="arial,helvetica">
<font size="2"><a href="/Auto-Verkehr.html">Auto &amp; Verkehr</a></font><font size="1"> (47)<br/><br/><font size="2"><a href="/Bildung-Wissenschaft.html">Bildung &amp; Wissenschaft</a></font><font size="1"> (38)<br/><br/><font size="2"><a href="/Computer-Internet.html">Computer &amp; Internet</a></font><font size="1"> (72)<br/><br/><font size="2"><a href="/Freizeit-Unterhaltung.html">Freizeit &amp; Unterhaltung</a></font><font size="1"> (142)<br/><br/><font size="2"><a href="/Gesundheit-Soziales.html">Gesundheit &amp; Soziales</a></font><font size="1"> (38)<br/><br/><font size="2"><a href="/Information.html">Information</a></font><font size="1"> (81)<br/><br/></font></font></font></font></font></font></font></td><td valign="top" width="33%"><font face="arial,helvetica"><font size="2"><a href="/Kunst-Kultur.html">Kunst &amp; Kultur</a></font><font size="1"> (58)<br/><br/><font size="2"><a href="/Regional-Politik.html">Regional &amp; Politik</a></font><font size="1"> (81)<br/><br/><font size="2"><a href="/Reisen.html">Reisen</a></font><font size="1"> (45)<br/><br/><font size="2"><a href="/Shops.html">Shops</a></font><font size="1"> (16)<br/><br/><font size="2"><a href="/Sport.html">Sport</a></font><font size="1"> (95)<br/><br/><font size="2"><a href="/Wirtschaft-Finanzen.html">Wirtschaft &amp; Finanzen</a></font><font size="1"> (48)<br/><br/>
</font></font></font></font></font></font></font></td>
<td valign="top">
<table cellspacing="0" width="95%">
<tr><td colspan="2"><font face="arial" size="1"><a href="/baseportal/baseportal/verzklick?cmd=mod&amp;Id=2726&amp;link=https://baseportal.de/cgi-bin/baseportal.pl?htx%3d/main" target="_blank">Büroreinigung gesucht</a></font></td><td align="right" nowrap="" valign="top"><font face="arial" size="1">1.12.20, 23:43</font></td></tr>
<tr><td><font face="arial" size="1">(Auto &amp; Verkehr)</font></td><td align="right" colspan="2" nowrap=""><font face="arial" size="1">von TonyJohnson</font></td></tr>
<tr><td height="14"></td></tr>
<tr><td colspan="2"><font face="arial" size="1"><a href="/baseportal/baseportal/verzklick?cmd=mod&amp;Id=2725&amp;link=www.baseportal.de/baseportal/Immobilienmakler/main" target="_blank">Immobilienmakler München</a></font></td><td align="right" nowrap="" valign="top"><font face="arial" size="1">9.11.20, 15:51</font></td></tr>
<tr><td><font face="arial" size="1">(Auto &amp; Verkehr)</font></td><td align="right" colspan="2" nowrap=""><font face="arial" size="1">von Lohmüller &amp; Company GmbH</font></td></tr>
<tr><td height="14"></td></tr>
<tr><td colspan="2"><font face="arial" size="1"><a href="/baseportal/baseportal/verzklick?cmd=mod&amp;Id=2724&amp;link=baseportal.de/" target="_blank">Abschlepp-Wagen24</a></font></td><td align="right" nowrap="" valign="top"><font face="arial" size="1">2.10.20, 01:18</font></td></tr>
<tr><td><font face="arial" size="1">(Auto &amp; Verkehr)</font></td><td align="right" colspan="2" nowrap=""><font face="arial" size="1">von TonyJohnson</font></td></tr>
<tr><td height="14"></td></tr>
<tr><td colspan="2"><font face="arial" size="1"><a href="/baseportal/baseportal/verzklick?cmd=mod&amp;Id=2723&amp;link=www.baseportal.de/baseportal/activity/linkliste" target="_blank">Gesundheit im Leben</a></font></td><td align="right" nowrap="" valign="top"><font face="arial" size="1">14.9.20, 15:54</font></td></tr>
<tr><td><font face="arial" size="1">(Gesundheit &amp; Soziales)</font></td><td align="right" colspan="2" nowrap=""><font face="arial" size="1">von Gesundheit im Leben</font></td></tr>
<tr><td height="14"></td></tr>
</table>
</td></tr>
</table>
<br/>
<table bgcolor="c0c0c0" border="0" cellpadding="0" cellspacing="1" width="95%"><tr><td align="middle"><table bgcolor="f8f8f8" border="0" cellpadding="4" cellspacing="1" width="100%"><tr><td>
<font face="arial,helvetica" size="1">
 Partner &amp; Friends:
<a href="http://acc.de" target="_blank">acc.de</a> -
<a href="http://www.himmel.de" target="_blank">himmel</a> -
<a href="http://www.snowpage.de" target="_blank">snowpage</a> -
<a href="http://pixelbasis.de" target="_blank">pixelbasis</a> -
<a href="http://www.bikeparkz.de" target="_blank">bikeparks</a>
</font></td></tr></table>
</td></tr></table>
<br/>
<table border="0" cellpadding="2" cellspacing="0" width="95%"><tr><td align="middle" bgcolor="b0c0e0"><font face="arial" size="1">© baseportal GmbH. Alle Rechte vorbehalten.</font></td></tr></table>
<br/>
<font size="2">
<a href="/baseportal/baseportal/presse">Presseartikel</a>
- <a href="/baseportal/baseportal/pr">Pressemitteilungen</a>
- <a href="/baseportal/baseportal/newsletter">Newsletter</a>
- <a href="nutzb.html">Nutzungsbedingungen</a>
- <a href="http://baseportal.spreadshirt.net">Hardware</a>
- <a href="kontakt.html">Impressum</a>
- <a href="datenschutz.html">Datenschutz</a>
<br/><br/><br/>
</font>
<table cellpadding="0" cellspacing="0"><tr align="middle" valign="bottom"><td><font face="arial" size="1">Logo zum Verlinken: </font></td><td> <font face="arial" size="1">baseportal Trailer:</font></td></tr><tr align="middle" valign="top"><td>
<a href="http://baseportal.de/linkme.html"><img border="0" height="45" src="pics/logo/basep.gif" vspace="5" width="100"/></a></td><td><a href="http://seite.net/baseportal.zip"><img border="0" height="45" src="pics/trailer.gif" vspace="5" width="90"/></a><br/><font face="arial" size="1">(MPG 3,2 MB)
</font></td></tr></table>
<a href="http://baseportal.de/linkme.html">Mehr Logos und Banner</a>
<br/><br/>
<br clear="all"/><center><font face="arial,helvetica" size="1">powered in 0.07s by baseportal.de<br/><a href="http://baseportal.de" style="text-decoration:underline;font-size:12px" target="_blank">Erstellen Sie Ihre eigene Web-Datenbank - kostenlos!</a></font></center><br/></center></body></html>
