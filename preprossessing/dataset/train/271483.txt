<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
--><html>
<head>
<title>TODO supply a title</title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="/assets/bybofront/css/style.css" rel="stylesheet" type="text/css"/>
<style>
            .wrap-404
            {
                width: 480px;
                /* margin: 0 auto; */
                /* max-width: 470px; */
                /* margin-top: 80px; */
                height: 100%;
                /* float: left; */
                width: 100%;
                display: table;
                max-width: 470px;
                padding:0 10px;
                margin: 0 auto;
                box-sizing: border-box;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
            }
            .sec-top span
            {
                width:33.33%;
                float:left;
                font-size:222px;
                color:#9c3;
            }
            .sec-top
            {
                width: 100%;
                float: none;
                max-width: 470px;
                margin: 0 auto;
                vertical-align: middle;
                display: table-cell;
            }
            .sec-top img
            {
                float:left;
                width:100%;
            }
            .butn-outer
            {
                width:100%;
                float:left;
                text-align:center;
                margin-top:40px;
            }
            .get-started
            {
                font-size: 1em;
                padding: 10px 30px;
                font-size:16px;
                font-family: 'latoregular';
                color: #fff;
                -webkit-transition: background 0.1s;
                -moz-transition: background 0.1s;
                transition: background 0.1s;
                -moz-border-radius: 3px;
                border-radius: 3px;
                background: url('/assets/bybofront/images/action.png') repeat-x 0 -53px;
                //background:#39c;
            }
            .get-started:hover
            {
                background-position: 0 0px;
            }

        </style>
<script>
            function goBack() {
                window.history.back()
            }
        </script>
</head>
<body style="float:left;
          width:100%; height:100% ;display:table;">
<div class="wrap-404">
<div class="sec-top">
<img alt="" src="/assets/bybofront/images/er-404.png"/>
<div class="butn-outer">
<a class="get-started" href="javascript:void(0)" onclick="goBack();">Go Back</a>
</div>
</div>
</div>
</body>
</html>
