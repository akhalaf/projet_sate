<html>
<head>
<meta content="zh-tw" http-equiv="Content-Language"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="G17r1NAmJDsgpPWHnX07H9MAukVON8gpJf+araNerTI=" name="verify-v1"/>
<meta content="03180d8ef31b68ce" name="y_key"/>
<meta content="PPT(ppt.cc) 短網址，不是 PTT (ptt.cc)唷，幫縮找我就對啦!" name="description"/>
<meta content="短網址,縮網址,縮文章,網站快照,ppt,ptt,0rz,tinyurl,台泥,鄉民,縮址,幫縮" name="keywords"/>
<link href="favicon.ico" rel="shortcut icon"/>
<link href="/css/style.css" rel="stylesheet" type="text/css"/>
<title>來個 PPT 短網址 - 不記名，不排名，膴廣告，真正低調的短網址</title>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script async="" charset="utf-8" src="//a.breaktime.com.tw/js/au.js?spj=NTNHQjE5SjNRMTRRUU9LVVpWR1Q3WkYwOVVHNQ==" type="text/javascript"></script>
<script>
function onSubmit() {
	document.getElementById("x").submit();
}
</script>
<script language="JavaScript">
function openwin()
{
        OpenWindow=window.open("", "newwin", "height=250, width=250,toolbar=no,scrollbars="+scroll+",menubar=no");
        OpenWindow.document.write("<TITLE>檔案保護</TITLE>");
        OpenWindow.document.write("<BODY BGCOLOR=#ffffff>");
        OpenWindow.document.write("<h1>檔案保護</h1>");
        OpenWindow.document.write("<font color=red>使用步驟</font>");
        OpenWindow.document.write("<ol><li>挑選電腦中的任一檔案</li><li>上傳檔案 (500K 以下)</li><li>檔案正確即可通關</li></ol>");
        OpenWindow.document.write("<a href=# onclick='window.close()'><font size=2 color=blue>關閉視窗</font></a>");
        OpenWindow.document.write("</BODY>");
        OpenWindow.document.write("</HTML>");
        OpenWindow.document.close();

        OpenWindow.moveTo(screen.width/2-125,screen.height/2-125);
}
function rulewin()
{
        OpenWindow=window.open("", "newwin", "height=500, width=540,toolbar=no,scrollbars=yes");
        OpenWindow.document.write("<TITLE>使用規範</TITLE>");
        OpenWindow.document.write("<BODY BGCOLOR=#ffffff>");
        OpenWindow.document.write("<h1>使用規範</h1>");
        OpenWindow.document.write("<font color=red>您必須了解</font>");
        OpenWindow.document.write("<ol><li>禁止利用本服務進行中華民國(台灣)法律所禁止之非法行為。</li><li>禁止轉址非自願公開存取之網頁內容連結 (如私人信件、部落格、私人照片)，<a href=reg>註冊會員</a>不在此限。</li><li>本服務有權移除使用者所產生的短網址，並不保證其永久有效。</li><li>任何網址建立的行為責任，須由行為人自行負責，必要時候將會配合調閱相關系統紀錄</li><li>本服務為免費公益性質，不保證連線有效性與穩定度。</li><li>轉址後的頁面內容，屬該網址內容提供者所有，請自行判斷其內容之正確性與合法性。</li><li>通關密碼因安全性考量，將編碼儲存，若擔心遺忘，請多利用<font color=#FF0000>網址說明</font>欄位作為密碼提醒。</li><li>請慎選檔案來作為檔案保護之用，一旦檔案遺失或變更，將再也無法通關。</li><li>為提升服務品質，<a href=reg>註冊會員</a>將可以有專屬空間，檢視您所建立的短網址，並做修改或刪除。</li></ol>");
        OpenWindow.document.write("<a href=# onclick='window.close()'><font size=2 color=blue>關閉視窗</font></a>");
        OpenWindow.document.write("</BODY>");
        OpenWindow.document.write("</HTML>");
        OpenWindow.document.close();

        OpenWindow.moveTo(screen.width/2-270,screen.height/2-250);
}

function fc()
{
        document.x.s.focus();
}

function checkCoords()
{
	document.getElementById("x").submit();
};
</script>
<style type="text/css">
.styled-button-8 {
	background: #25A6E1;
	background: -moz-linear-gradient(top,#25A6E1 0%,#188BC0 100%);
	background: -webkit-gradient(linear,left top,left bottom,color-stop(0%,#25A6E1),color-stop(100%,#188BC0));
	background: -webkit-linear-gradient(top,#25A6E1 0%,#188BC0 100%);
	background: -o-linear-gradient(top,#25A6E1 0%,#188BC0 100%);
	background: -ms-linear-gradient(top,#25A6E1 0%,#188BC0 100%);
	background: linear-gradient(top,#25A6E1 0%,#188BC0 100%);
	filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#25A6E1',endColorstr='#188BC0',GradientType=0);
	padding:8px 13px;
	color:#fff;
	font-family:'Helvetica Neue',sans-serif;
	font-size:17px;
	border-radius:4px;
	-moz-border-radius:4px;
	-webkit-border-radius:4px;
	border:1px solid #1A87B9
}                
</style>
</head>
<script src="https://connect.facebook.net/zh_TW/all.js#xfbml=1"></script>
<!--<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	    js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/zh_TW/all.js#xfbml=1";
		    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>-->
<body onload="memo.innerHTML='&lt;center&gt;&lt;table border=0 cellpadding=3 cellspacing=0&gt;&lt;tr&gt;&lt;td bgcolor=#E5ECF9&gt;&lt;font size=2&gt;&lt;b&gt;網址說明&lt;/font&gt;&lt;/b&gt;&lt;/td&gt;&lt;td bgcolor=#E5ECF9&gt;&lt;input type=text name=m&gt;&lt;/td&gt;&lt;td bgcolor=#E5ECF9&gt;&lt;font size=2&gt;(隨意填寫！)&lt;/font&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;';fc();"><div align="center"><table border="0" cellpadding="0" cellspacing="0" id="table1" width="100%">
<div id="fb-root"></div>
<div class="fbfans">
<div class="fb-like-box" data-header="false" data-height="200" data-href="https://www.facebook.com/ppt.cc" data-show-faces="true" data-stream="false" data-width="292"></div>
</div>
<img border="0" height="2" src="space.gif" width="1"/>
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" id="table2">
<tr>
<td><b><font size="2">縮網址</font></b>  <font size="2"><a href="text.php">縮文章</a></font>  <font size="2"><a href="cut/">縮圖片</a></font>  <font size="2"><a href="mov/">縮影片</a></font>  <font size="2"><a href="fun/">歡譯</a></font>  </td>
</tr>
</table>
</td>
<td align="right"><font size="2">隱私權聲明</font></td>
</tr>
<tr>
<td bgcolor="#E5ECF9" colspan="2">
<img border="0" height="1" src="space.gif" width="1"/></td>
</tr>
</table>
<a href="."><img alt="來個PPT短網址" border="0" src="PPT.png" title="來個PPT短網址"/></a>
<br/>
</div>
<center><font color="red"><h3><a href="mov/">[New!] PPT 縮影片服務公測中</a></h3></font></center>
<form action="gen.php" enctype="multipart/form-data" id="x" method="POST" name="x">
<p></p><center>來個 PPT 短網址: <input name="s" size="60" type="text" value=""/></center>
<p></p><center><button class="g-recaptcha styled-button-8" data-callback="checkCoords" data-sitekey="6Lf9RSkUAAAAABJ05R9XWEg5OLkXrIqu65bQzp6k"> 產 生 </button></center>
<p align="center"><font size="2"><input checked="" name="t" onclick="result.innerHTML='';" type="radio" value="1"/>不用密碼　<input name="t" onclick="result.innerHTML='&lt;center&gt;&lt;table border=0 cellpadding=3 cellspacing=0&gt;&lt;tr&gt;&lt;td bgcolor=#E5ECF9&gt;&lt;font size=2&gt;&lt;b&gt;通關密碼&lt;/font&gt;&lt;/b&gt;&lt;/td&gt;&lt;td bgcolor=#E5ECF9&gt;&lt;input type=password name=p size=21&gt;&lt;/td&gt;&lt;td bgcolor=#E5ECF9&gt;&lt;font size=2&gt;(中文也行！)&lt;/font&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td bgcolor=#E5ECF9&gt;&lt;font size=2&gt;&lt;b&gt;密碼確認&lt;/font&gt;&lt;/b&gt;&lt;/td&gt;&lt;td bgcolor=#E5ECF9&gt;&lt;input type=password name=rp size=21&gt;&lt;/td&gt;&lt;td bgcolor=#E5ECF9&gt;&lt;font size=2&gt;(再敲一次！)&lt;/font&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;font size=2&gt;系統將編碼儲存通關密碼，忘記密碼就掰掰啦！（我也幫不了你）&lt;/center&gt;';" type="radio" value="2"/>密碼保護　<input name="t" onclick="result.innerHTML='&lt;center&gt;&lt;table border=0 cellpadding=3 cellspacing=0&gt;&lt;tr&gt;&lt;td bgcolor=#E5ECF9&gt;&lt;font size=2&gt;&lt;b&gt;上傳檔案&lt;/font&gt;&lt;/b&gt;&lt;/td&gt;&lt;td bgcolor=#E5ECF9&gt;&lt;input type=file name=f size=14&gt;&lt;/td&gt;&lt;td bgcolor=#E5ECF9&gt;&lt;font size=2&gt;(500K以內)&lt;/font&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;font size=2&gt;請任意上傳一個小檔案，當作通關密碼，只有握有檔案的人，才能通關。&lt;/font&gt;&lt;/center&gt;';" type="radio" value="3"/>檔案保護 (<a href="javascript:openwin();"><font color="#0000FF">說明</font></a>)　<input checked="" name="m" onclick="this.checked == true ? memo.innerHTML='&lt;center&gt;&lt;table border=0 cellpadding=3 cellspacing=0&gt;&lt;tr&gt;&lt;td bgcolor=#E5ECF9&gt;&lt;font size=2&gt;&lt;b&gt;網址說明&lt;/font&gt;&lt;/b&gt;&lt;/td&gt;&lt;td bgcolor=#E5ECF9&gt;&lt;input type=text name=m&gt;&lt;/td&gt;&lt;td bgcolor=#E5ECF9&gt;&lt;font size=2&gt;(隨意填寫！)&lt;/font&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;':  memo.innerHTML='';" type="checkbox"/>網址說明(選填)</font></p>
<p align="center"></p><table><tr><td><div id="memo"></div></td></tr></table><table><tr><td><div id="result"></div></td></tr></table> <input checked="" name="r" type="checkbox" value="1"/><a href="javascript:rulewin();"><font color="red" size="2">我同意使用規範</font></a>
<p align="center"><font size="2">服務條款 - <a href="x/">完全手冊</a> - <a href="reg.php">加入會員(免費)</a> - <a href="report.php?g=ps4VP">回報問題網址</a> - <a href="mailto:admix@ppt.cc">聯絡偶們</a> - <a href="http://blog.ppt.cc/" target="_black">開發部落格</a></font>
</p><p align="center">© 2021 PPT.cc </p>
</form></body>
</html>
