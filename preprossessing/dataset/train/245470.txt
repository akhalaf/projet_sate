<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "89761",
      cRay: "61117c5df92e3de5",
      cHash: "a0632c636a0ec94",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYm9iYmFpdGFsaWEuaXQv",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "E6aDWOP862Yo9E4/loCd456pgrfw+2ohhRqwUUy7QIpCpqpoa8MFAHH2Or0TPsJO4+utFujfl+bZNF+L8/74PRlpYXAD/+XZDqh9v34rxItT+fSCwBnHFQF+1duFufr+J2b1uVLQeSiC7Z6rlgmGjSgqpko70vskkIEG3tVTA7eBe1P24jliFrw9utv1ouUU/6sVg1hB3Sdn2QkXCGd5A/cByEh/X4yPuh8KcB/rjTHJfme0V9Y/IHd0d6vK1M8JSUMAFCvB71cstmn8JXH1xh6ozTAW1kTVx5uK6XgLs2uixJds3/Djc9noAlQZehnTfpZQdpDXQkDFtCTdKYulgMr0khuHg1/sVthKzh6hAINXIZRWqV1ckX3iMqby9/LUFNMBn2MOSkGtQHGNZCVS9cES4CoGKea/OrP6HWZUqhdlw9ikqRlIdH/7U7dgb5IsUC8BlWMQO1PjfKsqPB7agj+PQBqdeS5WWDzK9k+5JjbRuFEhyX3xqWJiqCIEyl0rCPoKd6RulDNT6qXY7Q/rysxXR1sl40eFAjlrSDCDrF0ZvNE6/nT+n29BlJppsgevV6HgIQ5U0c1ZIgbUmnRrCsBCg6DO7ZK/YNNUgJ9L5bqgpkVAuKwl3eXKMWopw0d4bVeLtERH/mYwpdm3fOffB01X7og5l1dvvEQhh4dQXL9uU3dj7O+SUdMYAf25POoYTcZYEUuTz/MSW7gGsLGRZKMzeMYPJfFNWGaM0YdWGGwjv1d2sqG37Fog6oGgf+HD",
        t: "MTYxMDU2NTc0NC4zMTUwMDA=",
        m: "moK7qwlp61SGCmEeDxFhsW9UlX9ov3Kdb+HPYM7dZdY=",
        i1: "ThP95jQAIG3+AwH8rvqrLQ==",
        i2: "q8tLMOfDS6Jm6W2LQPA6WQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "hARhD6a09HcHWhuoLnFgy2wjRmOPMtZ80jQeiB/OsOE=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=61117c5df92e3de5");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> bobbaitalia.it.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<a href="https://rkrmpg.com/americanactinoid.php?faqid=252"><span style="display: none;">table</span></a>
<form action="/?__cf_chl_jschl_tk__=21a84fb8d0f2502c7468f7877467651de42e00a5-1610565744-0-AUNlqkPEZZ3Es8UJc6DxgMZ44ACCPGS4Ch7Injix_KLHS3cy_yELZ-HeO7BF0YVkSbnpB88i52Ki23pTN4_jjkAlNhafLGGISc_KQtmmTS28h0CsEcU1OW5nJZFik379e68aIJIGFtjmr0b_KDq2NAurlk4izcSFXdQvKn1k4bAQ7Ug9fXovqcTFb6kYY03YnOxJr6_P0iQbX_yqHJUA9dnvUW8iRUGC_8k2kvUC0YscwRUWbgyOYvwAFB8lyedYZ3rkC-CnyBjU6nWOuaDiBT84cNZB7KAc7BZE2PokyiwHXZN9tLup3mKnTE2M23znvQ" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="21ee6f9853dd7cc45fa9008b70048b6ae5e04c2d-1610565744-0-AatTo+UhPbjDl9BQtXZqtr7k3kmQwNx31ma2IUlXM4ywNMIfgdzziJQpspJhaZKNzz4w05aPQJsuNJqyHdsANHZslw/44jD9Ola6Sq63dF8tr7vN+puR0bDzGYVreAyBlXwHR7/wpEwLdW6veUfjJG9nelPTSCwSioSJRRCOj4Imz1DZuDOMhR/g3in4z9H0VkLLqFi6j3Fj8s6uFUC8/nxUZ93ouloJnR4G5heDPy7SFOiZZM2dl+FuHtEYc8n42NU9n/wq2Q3DbQ1gLLWmBFq8n4MzoF1gMG8wcKOAMAEyOHWLwYzBCr73QzjY3jOpSQ6eCkKm6lYJs660fYUfMNkJfFQ+lcaxtsCx5HW6yxAss0vcJ8V8PtElejp2k3IZ2AGti/MHkgGu6Txe+5eZPNMwJFReAA1KkPgrO3DFi6x3Vw5WSjwC4FN0jtf22A4fED9CTL66habNEfb+kyWZeTRWzLomkuAV6LxF8LU/c6lqFJjkXznHj7KL+oI0gbiVoZIhKzbtf4wFBIpJ6IG4/8v8hP+/hbol+kAhgR1oUtVHb3/2jpj3geIt33lyXhOk93/Hlqk2iIozgNe5QSzjmq+aiVrRYk9sJgnISf4Iu/ITRSUaFee8F05usNgNITwRpFR+EiOmvQgOU/YdFD7Cwn7BcQln4/iVVSZxDdsl5TdiTnjljlpSq8Nay2oamUv67xGU0gsXGL77Qq0Q/+X7OyYAT3+p5sI9ck1yUQqJHKcUdtETilq1d/n3ZDppswrEuy1B1li8/137S8Caitn/Ye1kP345P7RVQUqsDDEwOki4cF/72xyrocth6u0oSDNe/h/IkombxEa8GNcaYOsqqf2AnfeesoDFS0Dj7u0LVRhi0cj+cLeKkNcAxMW4ao3JhxYHY/FwdVtdcvyh4OplmPEa78OKyq7s/ZvwKV7Ele079A5pb+Irmdg1MPa9ryXgKLCW3d0qhNbhKJhycq1YlC4wvnB8iAMWLoPE6zl/6vqg1SkMMcIB8HX0Uo4CMJBPquUCaHQHMgB5YLwfv7PJXb/s3sxEqw5SCFdcWkMDDkQPExA/Ra7rkExp/s4rOGXp6FrPaXF7/3iaBG56SBoLRiO7Y0HK+y6FozpZUT7qNZ9jifdn+rkD/lgBcnWnPTnMDp4Y9lcnJefFOaWx0Mr9xEtfM4RDXjEyaNWSHVEM2ACuizJa5yZRvhI3GlF8jZc8mslNGqFW2s9DMgMV3bkHXxMN525xCh53yPmigHjfaBAtgW/hZkd/8Z76BYadgyrPyQdYIXfxLIsUJeLiJKQs0dI="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="5e16e358f301e02c7449beb35622db79"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610565748.315-jYs1y4R4Fb"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=61117c5df92e3de5')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>61117c5df92e3de5</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
