<!DOCTYPE html>
<html lang="en">
<head>
<script async="" data-ad-client="ca-pub-0985167923590473" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=5.0" name="viewport"/>
<title>AREAVIS.COM - Your world in one place</title>
<meta content="google0d995a53d2dfef2d.html" name="google-site-verification"/>
<title>Error</title>
<base href="https://www.areavis.com/"/>
<meta content="summary" name="twitter:card"/>
<meta content="Error" property="og:title"/>
<meta content="" property="og:description"/><link href="https://www.areavis.com/s/sys_images_resized/cvy5tdhakkfjpgcghrshn8p6z32n2g3a.png" rel="shortcut icon" type="image/x-icon"/><link href="https://www.areavis.com/s/sys_images_resized/zgieivgukiyhl2m6vtkgg8kb5szyjsg6.png" rel="image_src" sizes="100x100"/><link href="https://www.areavis.com/s/sys_images_resized/4yhzruq2mwlaykpefyhac8ulrzng5rze.png" rel="apple-touch-icon" sizes="152x152"/>
<style>.bx-hide-when-logged-out {
	display: none !important;
}
</style>
<link href="https://www.areavis.com/gzip_loader.php?file=bx_templ_css_4_15596c006bc66eb84adcafefafb739e5.css" rel="stylesheet" type="text/css"/>
<link href="https://www.areavis.com/gzip_loader.php?file=bx_templ_css_4_f989bd8ef0852a11fd1437aa0ae29a4b.css" rel="stylesheet" type="text/css"/>
<script language="javascript">var aDolImages = {};</script> <script language="javascript">var aDolLang = {'_Are_you_sure': 'Are you sure?','_error occured': 'Error occurred','_sys_loading': 'Loading...','_copyright': '© {0} Areavis'};</script> <script language="javascript">var aDolOptions = {};</script>
<script language="javascript" src="https://www.areavis.com/gzip_loader.php?file=bx_templ_js_ddd1c89628a011fa1e66d903f4eb44ad.js"></script>
<script language="javascript" type="text/javascript">
    var sUrlRoot = 'https://www.areavis.com/';

    $(document).ready(function () {
        bx_time('en');
    });

    (function(w) {
        var dpr = (w.devicePixelRatio === undefined ? 1 : Math.round(w.devicePixelRatio));
        if ($.cookie('devicePixelRatio') == dpr || dpr < 2)
            return;
        $.cookie('devicePixelRatio', dpr, {expires: 365, path: '/'});
        window.location.reload();
    })(window);

    var oMediaPhone = window.matchMedia('(max-width:720px)');
    var oMediaPhone2 = window.matchMedia('(min-width:533px) and (max-width:720px)');
    var oMediaTablet = window.matchMedia('(min-width:720px) and (max-width:1024px)');
    var oMediaTablet2 = window.matchMedia('(min-width:533px) and (max-width:1024px)');
    var oMediaDesktop = window.matchMedia('(min-width:1024px)');

    function fMediaCallback(e) {
        if (oMediaPhone.matches)
            $('html').addClass('bx-media-phone');
        else
            $('html').removeClass('bx-media-phone');
        
        if (oMediaPhone2.matches)
            $('html').addClass('bx-media-phone2');
        else
            $('html').removeClass('bx-media-phone2');
        
        if (oMediaTablet.matches)
            $('html').addClass('bx-media-tablet');
        else
            $('html').removeClass('bx-media-tablet');
        
        if (oMediaTablet2.matches)
            $('html').addClass('bx-media-tablet2');
        else
            $('html').removeClass('bx-media-tablet2');

        if (oMediaDesktop.matches)
            $('html').addClass('bx-media-desktop');
        else
            $('html').removeClass('bx-media-desktop');
    }

    oMediaPhone.addListener(fMediaCallback);
    oMediaPhone2.addListener(fMediaCallback);
    oMediaTablet.addListener(fMediaCallback);
    oMediaTablet2.addListener(fMediaCallback);
    oMediaDesktop.addListener(fMediaCallback);

    fMediaCallback(null);

</script>
<script language="javascript" type="text/javascript">
    var bUseSvgLoading = false;
    var sUseSvgLoading = '';
    if(!bUseSvgLoading) {
        var aSpinnerOpts = {
            lines: 7, // The number of lines to draw
            length: 0, // The length of each line
            width: 8, // The line thickness
            radius: 7, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            color: '#283C50', // #rgb or #rrggbb or array of colors
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'bx-sys-spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: '50%', // Top position relative to parent in px
            left: '50%' // Left position relative to parent in px
        };

        var aSpinnerSmallOpts = $.extend({}, aSpinnerOpts, {
            lines: 6,
            width: 6,
            radius: 4,
            color: '#333',
            top: '50%',
            left: 'calc(100% - 20px)'
        });
    }
</script>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        // hide toolbar on scrolldown, show - on scroll up
        new Headroom($('#bx-toolbar').get(0), {
            tolerance: {
                down: 10,
                up: 20
            },
            offset: 200,
            classes: {
                initial: "bx-toolbar-anim",
                pinned: "bx-toolbar-reset",
                unpinned: "bx-toolbar-up"
            },
            onUnpin: function() {
                bx_menu_slide_close_all_opened();
            }
        }).init();
    });
</script>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        bx_activate_anim_icons('#283C50');
    });
</script>
<style>div.bx-market-unit-cover div.bx-base-text-unit-no-thumb {
 border-width: 0px;
}</style> <meta content="#f5faff" name="theme-color"/>
</head>
<body class="bx-def-font bx-def-color-bg-page bx-def-image-bg-page bx-user-unauthorized" dir="LTR">
<div class="bx-popup-wrapper bx-popup-responsive bx-popup-trans-wrapper bx-def-box-sizing" id="bx-popup-loading" role="alert" style="display:none;">
<!--<div class="bx-popup-box-pointer"><i class="bx-popup-border"></i></div>-->
<div class="bx-popup bx-popup-trans bx-popup-border bx-popup-color-bg bx-def-round-corners">
<div class="bx-popup-close-wrapper bx-def-padding bx-def-media-desktop-hide bx-def-media-tablet-hide">
<a class="bx-popup-element-close" href="javascript:void(0);"><i class="sys-icon times"></i></a>
</div>
<div class="bx-popup-content" style=""><div class="bx-popup-content-indent bx-def-padding">
<div class="bx-popup-content-wrapped"></div>
<div class="bx-popup-loading-wrapped"></div>
</div></div>
</div>
</div> <noscript>
<div class="bx-def-padding" style="text-align:center; color:red;background-color: rgba(255,234,193,.5);    border-radius: .1875rem;">
       		 This site requires JavaScript! Please, enable it in the browser!       </div>
</noscript>
<div class="bx-main ">
<div class="bx-header bx-def-image-bg-header bx-def-color-bg-header bx-def-border-header bx-shadow-header bx-def-z-index-nav" id="bx-toolbar">
<div class="bx-def-centered bx-def-padding-sec-leftright bx-def-page-width bx-def-box-sizing">
<div class="bx-toolbar-content-wrapper bx-content-padding-header">
<div class="bx-clearfix" id="bx-toolbar-content">
<div id="bx-menu-toolbar-1-container">
<ul class="bx-menu-toolbar bx-menu-object-sys_toolbar_site">
<li class="bx-menu-toolbar-item bx-menu-item-main-menu bx-def-padding-thd-leftright " id="bx-menu-toolbar-item-main-menu">
<a class="bx-def-font-contrasted" href="javascript:void(0);" onclick="bx_menu_slide_inline('#bx-sliding-menu-sys_site', this, 'site');" target=""><i class="sys-icon bars bx-def-margin-thd-leftright"></i></a>
</li>
<li class="bx-menu-toolbar-item bx-menu-item-search bx-def-padding-thd-leftright bx-def-media-phone-hide" id="bx-menu-toolbar-item-search">
<a class="bx-def-font-contrasted" href="javascript:void(0);" onclick="bx_menu_slide_inline('#bx-sliding-menu-search', this, 'site');" target=""><i class="sys-icon search bx-def-margin-thd-leftright"></i></a>
</li>
</ul> </div>
<div id="bx-logo-container">
<a class="bx-def-font-contrasted" href="https://www.areavis.com/" title="AREAVIS.COM - Your world in one place"><img alt="AREAVIS.COM - Your world in one place" id="bx-logo" src="https://www.areavis.com/image_transcoder.php?o=sys_custom_images&amp;h=1&amp;x=240&amp;y=48" style="max-width:15rem;max-height:3rem;"/></a> </div>
<div id="bx-menu-toolbar-2-container">
<ul class="bx-menu-toolbar bx-menu-object-sys_toolbar_member">
<li class="bx-menu-toolbar-item bx-menu-item-login bx-def-padding-thd-leftright " id="bx-menu-toolbar-item-login">
<a class="bx-def-font-contrasted" href="page/login" onclick="" target=""><i class="sys-icon user bx-def-margin-thd-leftright"></i></a>
</li>
</ul> </div>
</div>
</div>
</div>
</div>
<div class="bx-menu-toolbar-padding"></div>
<div id="bx-content-wrapper">
<div class="bx-page-wrapper bx-def-centered bx-def-padding-sec-leftright bx-def-page-width bx-def-box-sizing">
<div class="bx-content-container bx-def-padding-sec-leftright" id="bx-content-container">
<div class="bx-content-main bx-def-margin-sec-topbottom" id="bx-content-main">
<!-- Design Box 11 [start]: Content + title + background + padding -->
<div class="bx-db-container bx-def-image-bg-block bx-def-color-bg-block bx-def-border-block bx-shadow-block">
<div class="bx-db-header"><div class="bx-db-title">Error</div><div class="bx-db-menu"><div class="bx-db-menu-tab bx-db-menu-tab-btn"></div></div><div class="bx-clear"></div></div>
<div class="bx-db-divider bx-def-hr"></div>
<div class="bx-db-content bx-content-padding-block bx-clearfix"><div class="bx-msg-box-container bx-def-margin-top bx-def-margin-bottom" id="1610484472747">
<div class="bx-msg-box bx-def-padding bx-def-round-corners bx-def-color-bg-hl">
        Page was not found    </div>
</div></div>
</div>
<!-- Design Box 11 [ end ] -->
<div class="bx-clear"></div>
</div> <!-- #bx-content-main -->
</div> <!-- #bx-content-container -->
</div>
</div> <!-- #bx-content-wrapper -->
<div class="bx-def-image-bg-footer bx-def-color-bg-footer bx-def-border-footer bx-shadow-footer" id="bx-footer-wrapper">
<div class="bx-def-centered bx-def-padding-sec-leftright bx-def-page-width bx-def-box-sizing">
<div class="bx-footer bx-def-padding-sec-left bx-def-padding-sec-right bx-clearfix" id="bx-footer">
<div class="bx-footer-cnt bx-content-padding-footer">
<div id="bx-menu-bottom">
<ul class="bx-menu-hor bx-menu-object-sys_footer bx-clearfix" id="bx-menu-footer">
<li class="bx-def-margin-right-auto ">
<a href="page/about" onclick="" target="" title="About">
                                                About            </a>
</li>
<li class="bx-def-margin-right-auto ">
<a href="page/terms" onclick="" target="" title="Terms">
                                                Terms            </a>
</li>
<li class="bx-def-margin-right-auto ">
<a href="page/privacy" onclick="" target="" title="Privacy">
                                                Privacy            </a>
</li>
<li class="bx-def-margin-right-auto ">
<a href="javascript:void(0)" onclick="on_copyright_click()" target="" title="© 2021 Areavis">
                                                Copyright            </a>
</li>
</ul><!-- 11 2 0 75 3 --> </div>
</div>
</div>
</div>
</div>
</div>
<div class="bx-sliding-menu bx-sliding-menu-fullheight bx-def-z-index-nav bx-def-color-bg-block" id="bx-sliding-menu-loading" style="display:none; opacity:1;">
<div class="bx-def-page-width bx-def-centered">
<a class="bx-sliding-menu-close bx-def-margin-sec-top bx-def-margin-right bx-def-font-h1" href="javascript:void(0);" onclick="bx_menu_slide_inline($(this).parents('.bx-sliding-menu:visible:first'))"><i class="sys-icon times"></i></a>
<div class="bx-sliding-menu-cnt bx-def-padding">
<div class="bx-sliding-menu-content"></div>
<div class="bx-sliding-menu-loading"></div>
</div>
</div>
</div><div class="bx-sliding-menu-main bx-sliding-menu-fullheight bx-def-z-index-nav bx-def-color-bg-block bx-search-site" id="bx-sliding-menu-search" style="display:none; opacity:1;">
<div class="bx-def-page-width bx-def-centered">
<a class="bx-sliding-menu-close bx-def-margin-sec-top bx-def-margin-right bx-def-font-h1" href="javascript:void(0);" onclick="bx_menu_slide_inline('#bx-sliding-menu-search')"><i class="sys-icon times"></i></a>
<div class="bx-def-padding">
<div class="bx-def-font-h2 bx-menu-search-popup-title">Search</div>
<div class="bx-page-block-container bx-def-padding-sec-topbottom bx-clearfix"><!-- Design Box 0 [start]: Content only -->
<div class="bx-db-content bx-clearfix">
<form action="https://www.areavis.com/searchKeyword.php" class="bx-form-advanced" id="sys_search_form_quick" method="post">
<input class="bx-def-font-inputs bx-form-input-hidden" name="live_search" type="hidden" value="1"/>
<div class="bx-form-advanced-wrapper sys_search_form_quick_wrapper">
<!-- form header content begins -->
<div class="bx-form-section-wrapper bx-def-margin-top"> <div class="bx-form-section bx-def-padding-sec-top bx-def-border-top bx-form-section-divider"> <div class="bx-form-section-content bx-def-padding-top bx-def-padding-bottom">
<div class="bx-form-element-wrapper bx-def-margin-top" id="bx-form-element-keyword"><div class="bx-form-element"><div class="bx-form-value bx-clearfix"><div class="bx-form-input-wrapper bx-form-input-wrapper-text"><input class="bx-def-font-inputs bx-form-input-text" name="keyword" onkeydown="return bx_search_on_type(event, 5, '#sys_search_form_quick', '#sys_search_results_quick', '#sys_search_results_quick', '1', 1);" onpaste="return bx_search_on_type(event, 5, '#sys_search_form_quick', '#sys_search_results_quick', '#sys_search_results_quick', '1', 1);" placeholder="Search..." type="text" value=""/>
</div></div></div><div class="bx-form-warn" style="display:none;"></div></div></div> </div> </div>
<!-- form header content ends -->
</div>
</form>
<script>
                    $(document).ready(function() {
                        $(this).addWebForms();
                    });
                    
                </script>
</div>
<!-- Design Box 0 [ end ] --></div> <div id="sys_search_results_quick"></div> </div>
</div>
</div><div class="bx-sliding-menu-main bx-def-z-index-nav bx-def-border-bottom" id="bx-sliding-menu-sys_site" style="display:none;"><div class="bx-sliding-menu-main-cnt"><ul class="bx-menu-floating-blocks bx-menu-object-sys_site bx-clearfix">
<li class="bx-def-round-corners bx-def-color-bg-hl-hover bx-def-box-sizing ">
<a href="index.php" onclick="" target="">
<div class="bx-menu-floating-blocks-icon">
<i class="sys-icon home col-gray-dark"></i> </div>
<div class="bx-menu-floating-blocks-title">Home</div>
</a>
</li><li class="bx-def-round-corners bx-def-color-bg-hl-hover bx-def-box-sizing ">
<a href="page/about" onclick="" target="">
<div class="bx-menu-floating-blocks-icon">
<i class="sys-icon info-circle col-blue3-dark"></i> </div>
<div class="bx-menu-floating-blocks-title">About</div>
</a>
</li><li class="bx-def-round-corners bx-def-color-bg-hl-hover bx-def-box-sizing ">
<a href="javascript:void(0);" onclick="bx_menu_slide_inline('#bx-sliding-menu-search', this, 'site');" target="">
<div class="bx-menu-floating-blocks-icon">
<i class="sys-icon search"></i> </div>
<div class="bx-menu-floating-blocks-title">Search</div>
</a>
</li><li class="bx-def-round-corners bx-def-color-bg-hl-hover bx-def-box-sizing ">
<a href="page/persons-home" onclick="" target="">
<div class="bx-menu-floating-blocks-icon">
<i class="sys-icon user col-blue3"></i> </div>
<div class="bx-menu-floating-blocks-title">People</div>
</a>
</li><li class="bx-def-round-corners bx-def-color-bg-hl-hover bx-def-box-sizing ">
<a href="page/albums-home" onclick="" target="">
<div class="bx-menu-floating-blocks-icon">
<i class="sys-icon far image col-blue1"></i> </div>
<div class="bx-menu-floating-blocks-title">Albums</div>
</a>
</li><li class="bx-def-round-corners bx-def-color-bg-hl-hover bx-def-box-sizing ">
<a href="page/posts-home" onclick="" target="">
<div class="bx-menu-floating-blocks-icon">
<i class="sys-icon file-alt col-red3"></i> </div>
<div class="bx-menu-floating-blocks-title">Posts</div>
</a>
</li><li class="bx-def-round-corners bx-def-color-bg-hl-hover bx-def-box-sizing ">
<a href="page/channels-home" onclick="" target="">
<div class="bx-menu-floating-blocks-icon">
<i class="sys-icon hashtag col-red2"></i> </div>
<div class="bx-menu-floating-blocks-title">Channels</div>
</a>
</li><li class="bx-def-round-corners bx-def-color-bg-hl-hover bx-def-box-sizing ">
<a href="page/organizations-home" onclick="" target="">
<div class="bx-menu-floating-blocks-icon">
<i class="sys-icon briefcase col-red2"></i> </div>
<div class="bx-menu-floating-blocks-title">Orgs</div>
</a>
</li></ul>
<script language="javascript">
    $(document).ready(function () {
        // add default class to active menu items
        $('.bx-menu-floating-blocks > li.bx-menu-tab-active').not('.bx-def-color-bg-active').addClass('bx-def-color-bg-active');
    });
</script></div></div><div class="bx-popup-wrapper bx-popup-responsive bx-popup-trans-wrapper bx-def-box-sizing" id="bx-popup-alert" role="alert" style="display:none;">
<!--<div class="bx-popup-box-pointer"><i class="bx-popup-border"></i></div>-->
<div class="bx-popup bx-popup-trans bx-popup-border bx-popup-color-bg bx-def-round-corners">
<div class="bx-popup-close-wrapper bx-def-padding bx-def-media-desktop-hide bx-def-media-tablet-hide">
<a class="bx-popup-element-close" href="javascript:void(0);"><i class="sys-icon times"></i></a>
</div>
<div class="bx-popup-content" style=""><div class="popup_alert_text bx-def-padding bx-def-font-align-center"></div>
<div class="popup_alert_actions bx-def-padding-rightbottomleft bx-def-font-align-center bx-clearfix">
<button class="bx-btn popup_alert_ok" type="button">OK</button>
</div></div>
</div>
</div><div class="bx-popup-wrapper bx-popup-responsive bx-popup-trans-wrapper bx-def-box-sizing" id="bx-popup-confirm" role="alert" style="display:none;">
<!--<div class="bx-popup-box-pointer"><i class="bx-popup-border"></i></div>-->
<div class="bx-popup bx-popup-trans bx-popup-border bx-popup-color-bg bx-def-round-corners">
<div class="bx-popup-close-wrapper bx-def-padding bx-def-media-desktop-hide bx-def-media-tablet-hide">
<a class="bx-popup-element-close" href="javascript:void(0);"><i class="sys-icon times"></i></a>
</div>
<div class="bx-popup-content" style=""><div class="popup_confirm_text bx-def-padding bx-def-font-align-center">Are you sure?</div>
<div class="popup_confirm_actions bx-def-padding-rightbottomleft bx-def-font-align-center bx-clearfix">
<button class="bx-btn bx-def-margin-sec-left-auto popup_confirm_yes" type="button">Yes</button><button class="bx-btn bx-def-margin-sec-left-auto popup_confirm_no" type="button">No</button>
</div></div>
</div>
</div><div class="bx-popup-wrapper bx-popup-responsive bx-popup-trans-wrapper bx-def-box-sizing" id="bx-popup-prompt" role="alert" style="display:none;">
<!--<div class="bx-popup-box-pointer"><i class="bx-popup-border"></i></div>-->
<div class="bx-popup bx-popup-trans bx-popup-border bx-popup-color-bg bx-def-round-corners">
<div class="bx-popup-close-wrapper bx-def-padding bx-def-media-desktop-hide bx-def-media-tablet-hide">
<a class="bx-popup-element-close" href="javascript:void(0);"><i class="sys-icon times"></i></a>
</div>
<div class="bx-popup-content" style=""><div class="popup_prompt_text bx-def-padding-lefttopright bx-def-font-align-center">Please, enter a value here</div>
<div class="popup_prompt_input bx-def-padding"><div class="bx-form-element-wrapper bx-def-margin-top" id="bx-form-element-bx-popup-prompt-value"><div class="bx-form-element"><div class="bx-form-value bx-clearfix"><div class="bx-form-input-wrapper bx-form-input-wrapper-text"><input class="bx-def-font-inputs bx-form-input-text" id="bx-popup-prompt-value" name="bx-popup-prompt-value" type="text" value=""/>
</div></div></div><div class="bx-form-warn" style="display:none;"></div></div></div>
<div class="popup_prompt_actions bx-def-padding-rightbottomleft bx-def-font-align-center bx-clearfix">
<button class="bx-btn bx-def-margin-sec-left-auto popup_prompt_ok" type="button">OK</button><button class="bx-btn bx-def-margin-sec-left-auto popup_prompt_cancel" type="button">Cancel</button>
</div></div>
</div>
</div><script>
(function(w, d){
    var id='embedly-platform', n = 'script';
    if (!d.getElementById(id)){
        w.embedly = w.embedly || function() {(w.embedly.q = w.embedly.q || []).push(arguments);};
        var e = d.createElement(n); e.id = id; e.async=1;
        e.src = ('https:' === document.location.protocol ? 'https' : 'http') + '://cdn.embedly.com/widgets/platform.js';
        var s = d.getElementsByTagName(n)[0];
        s.parentNode.insertBefore(e, s);
    }
})(window, document);
function bx_embed_link(e)
{
    embedly('card', e);
}
</script> </body>
</html>