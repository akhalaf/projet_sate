<!DOCTYPE html>
<html>
<head profile="http://www.w3.org/1999/xhtml/vocab">
<title>АГРОДОМ - продажа семян и минеральных удобрений по России.</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="https://www.agrodom-group.ru/sites/all/themes/d7forispro/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<meta content="Компания АГРОДОМ занимается продажей минеральных и водорастворимых удобрений, средств защиты растений и различных семян." name="description"/>
<meta content="Компания АГРОДОМ занимается продажей минеральных и водорастворимых удобрений, средств защиты растений и различных семян." name="abstract"/>
<meta content="удобрения цена, агродом, агродом магазин, агродом интернет магазин, ооо агродом, сайт агродом, минеральные удобрения купить, удобрения минеральные и органические, минеральные удобрения цена, купить удобрения цена, цена удобрений за тонну, жидкое удобрение цена, удобрения оптом, купить удобрения оптом, минеральные удобрения оптом, удобрение оптом от производителя, минеральные удобрения купить оптом, купить удобрения оптом от производителя, удобрения оптом цена, удобрения оптом россия, водорастворимые удобрения, водорастворимые удобрения купить, водорастворимое минеральное удобрение, агринос, грогрин, купить семена, семена кукурузы, семена подсолничка, средства защиты растений, средства защиты растений купить, гербицид, инсектициды, фунгицид, удобрение, акарициды, гелеобразные удобрения, микроудобрения, жидкие удобрения купить, жидкие удобрения, жидкие органические удобрения, листовые подкормки" name="keywords"/>
<link href="https://www.agrodom-group.ru/" rel="canonical"/>
<link href="https://www.agrodom-group.ru/" rel="shortlink"/>
<link href="https://www.agrodom-group.ru/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.agrodom-group.ru/sites/default/files/css/css_YJo600u5DslEXHHEBzo7Whs7zYvH8oZK7MuO9r6-Ktg.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.agrodom-group.ru/sites/default/files/css/css_XnT6VCNljf2qWU4IJIFrZCeR20nb4Dhi8xEmpck0i-I.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.agrodom-group.ru/sites/default/files/css/css_PPrugjH7CUf-XfrujCnUcoQDncyMkkfLn0DUHOwB7hM.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.agrodom-group.ru/sites/default/files/css/css_DFpy4IbPE_bJEEmy5Ynr8ppwHxqt_JT078n5loaAFs8.css" media="all" rel="stylesheet" type="text/css"/>
<style media="all" type="text/css">
<!--/*--><![CDATA[/*><!--*/
#back-top{right:40px;}#back-top span#button{background-color:#CCCCCC;}#back-top span#button:hover{opacity:1;filter:alpha(opacity = 1);background-color:#777777;}

/*]]>*/-->
</style>
<link href="https://www.agrodom-group.ru/sites/default/files/css/css_y5MLzwl-jutZuuszqsaMKeOiuhzmTs3PzfMytwSKoNw.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.agrodom-group.ru/sites/default/files/css/css_fHlFxgeb_Ig1Pq4sAQ27Nbm2aqYXtKXW5yCD2QHWmiQ.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.agrodom-group.ru/sites/default/files/css/css_mDxCF8ImVBb6oiL7Rq6KarwvxmvDV5PjnhYNE29pt_0.css" media="print" rel="stylesheet" type="text/css"/>
<script src="https://www.agrodom-group.ru/sites/default/files/js/js_jR5PzzcdiXzXPPH6viGlnjfCU9GOHnfV-niDQ-QSgnA.js" type="text/javascript"></script>
<script src="//api-maps.yandex.ru/2.1/?lang=ru-RU&amp;coordorder=longlat&amp;apikey=" type="text/javascript"></script>
<script src="https://www.agrodom-group.ru/sites/default/files/js/js_5KwAG5ti3--IInzzrixUheUe0c3CaM20-Pfi0mAH1D8.js" type="text/javascript"></script>
<script src="https://www.agrodom-group.ru/sites/default/files/js/js_YJIPWRp6h8EVoYAnUpjv8S-fOkwT0FzL_qdXpZfitH0.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-136019087-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script src="https://www.agrodom-group.ru/sites/default/files/js/js_KzPcIQ3qN9ct63YvzR7n2F-rasRh9LIKSUq53zHdeRY.js" type="text/javascript"></script>
<script src="https://www.agrodom-group.ru/sites/default/files/js/js_qFgtd1b-LffP_-nz0FSWIr_J9XxHvpSu4lcDvxW6foM.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"d7forispro","theme_token":"Hg5XzshdaEbNwG1akbZsQuUydDTe8-QlcuYqeFXxoDg","jquery_version":"1.7","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.7\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/tipsy\/javascripts\/jquery.tipsy.js":1,"sites\/all\/modules\/tipsy\/javascripts\/tipsy.js":1,"\/\/api-maps.yandex.ru\/2.1\/?lang=ru-RU\u0026coordorder=longlat\u0026apikey=":1,"sites\/all\/modules\/geofield_ymap\/js\/geofield_ymap.js":1,"misc\/ajax.js":1,"sites\/all\/modules\/jquery_update\/js\/jquery_update.js":1,"public:\/\/languages\/ru_csZoibYXwaB-VOAnnNBNbikKx-050Zf9RZRmokFagwg.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_load.js":1,"sites\/all\/modules\/scroll_to_top\/scroll_to_top.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"misc\/progress.js":1,"sites\/all\/modules\/colorbox_node\/colorbox_node.js":1,"sites\/all\/libraries\/superfish\/superfish.js":1,"sites\/all\/modules\/superfish\/superfish.js":1,"sites\/all\/themes\/d7forispro\/owl-carousel\/owl.carousel.min.js":1,"sites\/all\/themes\/d7forispro\/js\/setting.js":1,"sites\/all\/themes\/d7forispro\/js\/bootstrap.min.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/tipsy\/stylesheets\/tipsy.css":1,"sites\/all\/modules\/geofield_ymap\/geofield_ymap.css":1,"sites\/all\/modules\/scroll_to_top\/scroll_to_top.css":1,"sites\/all\/modules\/colorbox_node\/colorbox_node.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/office_hours\/office_hours.css":1,"modules\/search\/search.css":1,"sites\/all\/modules\/ubercart\/uc_order\/uc_order.css":1,"sites\/all\/modules\/ubercart\/uc_product\/uc_product.css":1,"sites\/all\/modules\/ubercart\/uc_store\/uc_store.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/youtube\/css\/youtube.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"0":1,"sites\/all\/libraries\/superfish\/css\/superfish.css":1,"sites\/all\/libraries\/superfish\/style\/vchmotors.css":1,"sites\/all\/themes\/d7forispro\/font-awesome\/css\/font-awesome.css":1,"sites\/all\/themes\/d7forispro\/css\/layout.css":1,"sites\/all\/themes\/d7forispro\/css\/style.css":1,"sites\/all\/themes\/d7forispro\/css\/slider.css":1,"sites\/all\/themes\/d7forispro\/css\/bootstrap.min.css":1,"sites\/all\/themes\/d7forispro\/owl-carousel\/owl.carousel.css":1,"sites\/all\/themes\/d7forispro\/css\/print.css":1}},"colorbox":{"opacity":"0.85","current":"{current} \u0438\u0437 {total}","previous":"\u00ab \u041f\u0440\u0435\u0434\u044b\u0434\u0443\u0449\u0438\u0439","next":"\u0421\u043b\u0435\u0434\u0443\u044e\u0449\u0438\u0439 \u00bb","close":"\u0417\u0430\u043a\u0440\u044b\u0442\u044c","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":false,"mobiledevicewidth":"480px","specificPagesDefaultValue":"admin*\nimagebrowser*\nimg_assist*\nimce*\nnode\/add\/*\nnode\/*\/edit\nprint\/*\nprintpdf\/*\nsystem\/ajax\nsystem\/ajax\/*"},"jcarousel":{"ajaxPath":"\/jcarousel\/ajax\/views"},"scroll_to_top":{"label":"\u041d\u0430\u0432\u0435\u0440\u0445"},"tipsy":{"custom_selectors":[{"selector":".tipsy","options":{"fade":1,"gravity":"w","delayIn":0,"delayOut":0,"trigger":"hover","opacity":"0.8","offset":0,"html":0,"tooltip_content":{"source":"attribute","selector":"title"}}}]},"better_exposed_filters":{"views":{"arenda_terms":{"displays":{"block":{"filters":[]}}},"contact":{"displays":{"block":{"filters":[]}}}}},"geofieldYmap":{"modulePath":"\/sites\/all\/modules\/geofield_ymap","objectPreset":""},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1},"superfish":{"1":{"id":"1","sf":{"delay":"400","animation":{"opacity":"show","height":"show"},"speed":"\u0027fast\u0027","autoArrows":true,"dropShadows":false,"disableHI":false},"plugins":{"supposition":false,"bgiframe":false}}},"colorbox_node":{"width":"600px","height":"600px"}});
//--><!]]>
</script>
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700&amp;subset=cyrillic" rel="stylesheet"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-431 node-type-page">
<header id="header">
<hgroup>
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-4 col-sm-2 col-xs-6"> <!-- логотип-->
<div id="logo">
<a class="logo" href="/" rel="home" title="Главная">
<img alt="Главная" src="https://www.agrodom-group.ru/sites/all/themes/d7forispro/logo.png"/>
</a>
</div>
</div>
<div class="col-lg-3 hidden-md hidden-sm hidden-xs"> <!-- адрес-->
<div id="header1">
<div class="region region-header1">
<div class="block block-block adres block-odd first last" id="block-block-74">
<div class="block-inner">
<div class="content">
<p><span>г. Краснодар, </span> ул. Московская 133/1 стр. 3 оф. № 21<br/>
<a class="mail-link" href="">office@agrodom-group.ru</a> <a href="https://www.facebook.com/AgroDom.group"><img alt="" src="/sites/default/files/1/fb.png"/></a>  <a href="https://www.instagram.com/agrodom_group/" target="_blank"><img alt="" src="/sites/default/files/1/insta.png" style="width: 21px; height: 21px;"/></a></p>
</div>
</div>
</div> </div>
</div>
</div>
<div class="col-lg-6 col-md-8 col-sm-10 col-xs-6"> <!-- блок с прочей инфой-->
<div class="row">
<div id="header2">
<div class="region region-header2">
<div class="block block-block col-lg-4 col-md-4 col-sm-4 hidden-xs block-odd first" id="block-block-75">
<div class="block-inner">
<div class="content">
<div class="time ">
<span>мы работаем:</span> пн - пт 9:00 - 18:00<br/>
	сб, вс выходной</div>
</div>
</div>
</div><div class="block block-block col-lg-4 col-md-4 col-sm-4 col-xs-12 block-even" id="block-block-76">
<div class="block-inner">
<div class="content">
<div class="tel ">
<span>Розничный отдел</span><span class="teltel">8(918)689-55-44</span></div>
</div>
</div>
</div><div class="block block-block col-lg-4 col-md-4 col-sm-4 col-xs-12 block-odd last" id="block-block-77">
<div class="block-inner">
<div class="content">
<div class="tel ">
<span>Отдел опта</span><span class="teltel">8(861)991-03-73</span></div>
</div>
</div>
</div> </div>
</div>
</div>
<div class="row">
<div id="header3">
<div class="region region-header3">
<div class="block block-block col-lg-4 col-md-4 col-sm-4 hidden-xs block-odd first" id="block-block-78">
<div class="block-inner">
<div class="content">
<div class="price ">
<a class="button-y" href="/node/438">Скачать прайс</a></div>
</div>
</div>
</div><div class="block block-block col-lg-4 col-md-4 col-sm-4 col-xs-12 block-even last" id="block-block-79">
<div class="block-inner">
<div class="content">
<!--<div class="user "><p>	<a class="button-g" href="">Личный кабинет</a></div>
<p>-->
</div>
</div>
</div> </div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12 col-md-12 hidden-sm hidden-xs"> <!-- меню-->
<div id="header-region-menu">
<div class="region region-header-menu">
<div class="block block-superfish block-odd first last" id="block-superfish-1">
<div class="block-inner">
<div class="content">
<ul class="menu sf-menu sf-main-menu sf-horizontal sf-style-vchmotors sf-total-items-6 sf-parent-items-0 sf-single-items-6" id="superfish-1"><li class="active-trail first odd sf-item-1 sf-depth-1 sf-no-children" id="menu-221-1"><a class="sf-depth-1 active" href="/">Главная</a></li><li class="middle even sf-item-2 sf-depth-1 sf-no-children" id="menu-4013-1"><a class="sf-depth-1" href="/about">О компании</a></li><li class="middle odd sf-item-3 sf-depth-1 sf-no-children" id="menu-4022-1"><a class="sf-depth-1" href="/catalog">Продукция</a></li><li class="middle even sf-item-4 sf-depth-1 sf-no-children" id="menu-4171-1"><a class="sf-depth-1" href="/praisy">Прайсы</a></li><li class="middle odd sf-item-5 sf-depth-1 sf-no-children" id="menu-4172-1"><a class="sf-depth-1" href="/catalog/agrodrony">Агродроны</a></li><li class="last even sf-item-6 sf-depth-1 sf-no-children" id="menu-3992-1"><a class="sf-depth-1" href="/kontakty">Контакты</a></li></ul> </div>
</div>
</div> </div>
</div>
</div>
<div class="hidde-lg hidde-mg col-sm-12 col-xs-12"> <!-- меню-->
<div id="header-region-menu">
<div class="region region-header-mobilmenu">
<div class="block block-block menuclick block-odd first last" id="block-block-65">
<div class="block-inner">
<div class="content">
<p>Меню</p>
</div>
</div>
</div> </div>
</div>
</div>
</div>
</div>
<div class="container allwhite">
<div class="row">
<div class="hidden-lg hidden-md col-sm-12 col-xs-12">
<div class="mobilmenu" id="header-region-menu">
<div class="region region-header-menu">
<div class="region region-header-menu">
<div class="block block-superfish block-odd first last" id="block-superfish-1">
<div class="block-inner">
<div class="content">
<ul class="menu sf-menu sf-main-menu sf-horizontal sf-style-vchmotors sf-total-items-6 sf-parent-items-0 sf-single-items-6" id="superfish-1"><li class="active-trail first odd sf-item-1 sf-depth-1 sf-no-children" id="menu-221-1"><a class="sf-depth-1 active" href="/">Главная</a></li><li class="middle even sf-item-2 sf-depth-1 sf-no-children" id="menu-4013-1"><a class="sf-depth-1" href="/about">О компании</a></li><li class="middle odd sf-item-3 sf-depth-1 sf-no-children" id="menu-4022-1"><a class="sf-depth-1" href="/catalog">Продукция</a></li><li class="middle even sf-item-4 sf-depth-1 sf-no-children" id="menu-4171-1"><a class="sf-depth-1" href="/praisy">Прайсы</a></li><li class="middle odd sf-item-5 sf-depth-1 sf-no-children" id="menu-4172-1"><a class="sf-depth-1" href="/catalog/agrodrony">Агродроны</a></li><li class="last even sf-item-6 sf-depth-1 sf-no-children" id="menu-3992-1"><a class="sf-depth-1" href="/kontakty">Контакты</a></li></ul> </div>
</div>
</div> </div>
</div>
</div>
</div>
</div>
</div>
</hgroup>
</header>
<div class="bg" id="ctop">
<div class="container">
<div class="row">
<div class="region region-contenttop">
<div class="block block-block col-lg-12 col-md-12 col-sm-12 col-xs-12 block-odd first last" id="block-block-62">
<div class="block-inner">
<div class="content">
<div class="topimg ">
<img alt="" src="/sites/default/files/1/glavnaya_agrodom.jpg" style="width: 100%;"/></div>
</div>
</div>
</div> </div>
</div>
</div>
</div>
<div class="bg" id="page">
<div class="container">
<div class="row"><!-- хлебные крошки -->
<div class="inner column center col-lg-12 col-md-12 col-sm-12 col-xs-12">
</div>
</div>
<div class="row"><!-- вывод основного контента -->
<div class="inner column center col-lg-12 col-md-12 col-sm-12 col-xs-12" id="content-inner">
<div class="region region-content">
<div class="block block-system block-odd first" id="block-system-main">
<div class="block-inner">
<div class="content">
<article class="node node-page node-odd">
<div class="node-inner">
<div class="content">
</div> <!-- /content -->
</div> <!-- /node-inner -->
</article> <!-- /article #node -->
</div>
</div>
</div><div class="block block-views block-even last" id="block-views-arenda_terms-block">
<div class="block-inner">
<div class="block-title">Продукция</div>
<div class="content">
<div class="view view-arenda-terms view-id-arenda_terms view-display-id-block view-dom-id-125d9f9b81a234492eced5ea6d992a4e">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="views-field views-field-field-razdelimg"> <div class="field-content"><a href="/catalog/sredstva-zashchity-rasteniy"><img alt="" height="170" src="https://www.agrodom-group.ru/sites/default/files/styles/termrazdel/public/catalog/marka/bez_imeni-1.jpg?itok=wKPFxjsJ" width="500"/></a></div> </div>
<div class="views-field views-field-name"> <span class="field-content"><a href="/catalog/sredstva-zashchity-rasteniy">Средства защиты растений</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content"><div>
<span style="font-size: 14px;">Мы сотрудничаем с ведущими иностранными и отечественными производителями средств защиты растений.</span></div>
</div> </div> </div>
<div class="views-row views-row-2 views-row-even col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="views-field views-field-field-razdelimg"> <div class="field-content"><a href="/catalog/semena"><img alt="" height="120" src="https://www.agrodom-group.ru/sites/default/files/styles/termrazdel/public/catalog/marka/vodorastvor-ud.jpg?itok=WcIIG10L" width="360"/></a></div> </div>
<div class="views-field views-field-name"> <span class="field-content"><a href="/catalog/semena">Семена</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content"><div>
<span style="text-align: justify; font-size: 14px;">Мы работаем с производителями только оригинальных семян полевых культур. Гибриды и сорта, которые производят представляемые нами компании – это: высокий потенциал урожайности; высокая технология производства; устойчивость к болезням</span></div>
</div> </div> </div>
<div class="views-row views-row-3 views-row-odd col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="views-field views-field-field-razdelimg"> <div class="field-content"><a href="/catalog/gel-produkty-grogreen"><img alt="" height="153" src="https://www.agrodom-group.ru/sites/default/files/styles/termrazdel/public/catalog/marka/000000000000000000.jpg?itok=0Pr-ICme" width="460"/></a></div> </div>
<div class="views-field views-field-name"> <span class="field-content"><a href="/catalog/gel-produkty-grogreen">Гелеобразные удобрения  GroGreen</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content"><div>
<span style="font-size: 14px;">Полноценное и сбалансированное питание культур, повышение иммунитета, повышение стрессоустойчивости, повышение урожайности, повышение качества продукции</span></div>
</div> </div> </div>
<div class="views-row views-row-4 views-row-even col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="views-field views-field-field-razdelimg"> <div class="field-content"><a href="/catalog/vodorastvorimye-udobreniya"><img alt="" height="120" src="https://www.agrodom-group.ru/sites/default/files/styles/termrazdel/public/catalog/marka/vodorastvorimye_udobreniya_kartinka1.jpg?itok=v0IqEbeH" width="360"/></a></div> </div>
<div class="views-field views-field-name"> <span class="field-content"><a href="/catalog/vodorastvorimye-udobreniya">Водорастворимые удобрения</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content"><div dir="ltr" style="line-height: 1.2; margin-top: 0pt; margin-bottom: 0pt;">
<span style="font-size: 14px; background-color: transparent; font-family: Arial; color: rgb(0, 0, 0); font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">Это залог высокого иммунитета и мощной корневой системы. Увеличение кол-ва цветков и завязей. </span><span style="font-size: 14px; font-family: Arial; color: rgb(0, 0, 0); font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">Устойчивость к болезням, вредителям и высоким температурам.</span><span style="font-size: 14px; background-color: transparent; font-family: Arial; color: rgb(0, 0, 0); font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;"> </span><span style="font-size: 14px; font-family: Arial; color: rgb(0, 0, 0); font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">Снижение содержания нитратов. Улучшение вкусовых качеств и увеличение размеров плодов</span></div>
<div>
	 </div>
</div> </div> </div>
<div class="views-row views-row-5 views-row-odd col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="views-field views-field-field-razdelimg"> <div class="field-content"><a href="/catalog/agrinos-innovacii-ot-prirody"><img alt="" height="166" src="https://www.agrodom-group.ru/sites/default/files/styles/termrazdel/public/catalog/marka/agrinos_risunok33.jpg?itok=l8tjAFJV" width="500"/></a></div> </div>
<div class="views-field views-field-name"> <span class="field-content"><a href="/catalog/agrinos-innovacii-ot-prirody">Agrinos инновации от природы</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content"><div class="rtejustify">
	Два революционных препарата с названием Агринос принципиально отличаются друг от друга:</div>
<div>
	Агринос1 – это консорциум микроорганизмов</div>
<div>
	Агринос2 – набор обработанных ими аминокислот и микроэлементов.</div>
</div> </div> </div>
<div class="views-row views-row-6 views-row-even views-row-last col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="views-field views-field-field-razdelimg"> <div class="field-content"><a href="/catalog/agrodrony"><img alt="" height="217" src="https://www.agrodom-group.ru/sites/default/files/styles/termrazdel/public/catalog/marka/bespilotnik1.jpg?itok=R---ME3j" width="500"/></a></div> </div>
<div class="views-field views-field-name"> <span class="field-content"><a href="/catalog/agrodrony">Агродроны</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content"><p>Преимущества использования дронов</p>
<h3 align="center">
	 </h3>
</div> </div> </div>
</div>
</div> </div>
</div>
</div> </div>
</div>
</div>
</div>
</div>
<footer id="footer">
<div class="container">
<div class="row"><!-- вывод меню футера -->
<div class="region region-content-foot2">
<div class="block block-block col-lg-12 col-md-12 col-sm-12 col-xs-12 block-odd first" id="block-block-66">
<div class="block-inner">
<div class="content">
<p class="rtecenter"><img alt="" src="/sites/default/files/1/1280px-xag_logo.jpg" style="width: 116px; height: 60px;"/><img alt="" src="/sites/default/files/1/unnamed.jpg" style="width: 130px; height: 60px;"/><img alt="" src="/sites/default/files/1/dju.png" style="width: 60px; height: 60px;"/><img alt="" src="/sites/default/files/1/1956515.jpg" style="width: 60px; height: 60px;"/><img alt="" src="/sites/default/files/1/adama-mahteshim-logo-7cde960de3-seeklogo.jpg" style="width: 60px; height: 60px;"/><img alt="" src="/sites/default/files/1/agroexpertgrup.jpg" style="width: 347px; height: 60px;"/><img alt="" src="/sites/default/files/1/basf.jpg" style="width: 180px; height: 60px;"/><img alt="" src="/sites/default/files/1/fmrus.jpg" style="width: 60px; height: 60px;"/><img alt="" src="/sites/default/files/1/sumiagro.jpg" style="width: 60px; height: 60px;"/><img alt="" src="/sites/default/files/1/agrorus_0.jpg" style="width: 267px; height: 60px;"/><img alt="" src="/sites/default/files/1/corteva.jpg" style="width: 261px; height: 60px;"/><img alt="" src="/sites/default/files/1/fmc.jpg" style="width: 287px; height: 67px;"/><img alt="" src="/sites/default/files/1/upl.jpg" style="width: 143px; height: 60px;"/><img alt="" src="/sites/default/files/1/logo11_0.png" style="width: 182px; height: 60px;"/></p>
</div>
</div>
</div><div class="block block-views col-lg-6 col-md-6 col-sm-6 col-xs-12 block-even last" id="block-views-contact-block">
<div class="block-inner">
<div class="content">
<div class="view view-contact view-id-contact view-display-id-block view-dom-id-6af7914d0981d6215b0ab088311a2902">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first views-row-last contextual-links-region">
<div class="views-field views-field-field-contactfoot"> <div class="field-content"><!--<p class="rteright"><p>тел. +7 918 689 55 44<br />
	тел. +7 918 689 44 55</p>
<p>-->
</div> </div>
<div class="views-field views-field-contextual-links"> <span class="field-content"></span> </div> </div>
</div>
</div> </div>
</div>
</div> </div>
</div>
<div class="row"><!-- вывод второго футера -->
<div class="copyright">
<div class="grid col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="live"> <div class="region region-footer">
<div class="block block-block block-odd first" id="block-block-44">
<div class="block-inner">
<div class="content">
<div>
	© 2020, “Агродом”</div>
</div>
</div>
</div><div class="block block-block block-even last" id="block-block-80">
<div class="block-inner">
<div class="content">
<!-- Yandex.Metrika counter --><script type="text/javascript">
<!--//--><![CDATA[// ><!--

   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(52533271, "init", {
        id:52533271,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });

//--><!]]>
</script><p></p><noscript>
<div><img alt="" src="https://mc.yandex.ru/watch/52533271" style="position:absolute; left:-9999px;"/></div>
<p></p></noscript>
<!-- /Yandex.Metrika counter --> </div>
</div>
</div> </div>
</div>
</div>
<div class="grid socseti col-lg-4 col-md-4 col-sm-4 col-xs-12">
</div>
<div class="grid col-lg-4 col-md-4 col-sm-4 col-xs-12" id="great">
<div class="index"><a href="http://airoweb.ru" target="_blank">Создание сайтов</a></div>
</div>
</div>
</div>
</div>
</footer> <!-- /footer --> </body>
</html>