<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html;charset=utf-8" http-equiv="content-type"/>
<meta content="ideation, online suggestion box, open source suggestion box, crowdsourcing ideas, social suggestion, idea comparison, prioritize ideas, nonprofit, free idea management" name="keywords"/>
<meta content="All Our Ideas is a platform that enables groups to collect and prioritize ideas in a transparent, democratic, and bottom-up way. It’s a suggestion box for the digital age." name="description"/>
<meta content="All Our Ideas is a platform that enables groups to collect and prioritize ideas in a transparent, democratic, and bottom-up way. It’s a suggestion box for the digital age." property="og:description"/>
<meta content="https://www.allourideas.org/images/favicon.png?1572885143" property="og:image"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="All Our Ideas" property="og:site_name"/>
<link href="https://www.allourideas.org/favicon.ico" rel="shortcut icon"/>
<title>All Our Ideas - Bringing survey research into the digital age</title>
<link href="/stylesheets/glyphicon.css?1572885143" media="screen" rel="stylesheet" type="text/css"/>
<link href="/stylesheets/flexslider/flexslider.css?1572885143" media="screen" rel="stylesheet" type="text/css"/>
<link href="/stylesheets/styles.css?1572885143" media="screen" rel="stylesheet" type="text/css"/>
<link href="/stylesheets/screen.css?1572885143" media="screen" rel="stylesheet" type="text/css"/>
<link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.7.3/themes/ui-lightness/jquery-ui.css" media="all" rel="stylesheet" type="text/css"/>
<link href="/stylesheets/ui.theme.css?1572885143" media="all" rel="stylesheet" type="text/css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.7.3/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {

});
</script>
</head>
<script type="text/javascript">
  //<![CDATA[
    $(document).ready(function() {
      // Fix for iPhone scale/rotation bug
      // http://adactio.com/journal/4470/
      if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
        var viewportmeta = document.querySelector('meta[name="viewport"]');
        if (viewportmeta) {
          viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0';
          document.body.addEventListener('gesturestart', function() {
            viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1.6';
          }, false);
        }
        /**
           * Prevent iOS from zooming onfocus
           * http://nerd.vasilis.nl/prevent-ios-from-zooming-onfocus/
           */
        $('input, select, textarea').bind('focus blur', function(event) {
        viewportmeta.attr('content', 'width=device-width,initial-scale=1,maximum-scale=' + (event.type == 'blur' ? 10 : 1));
        });
      }
    });
  //]]>
</script>
<body class="en">
<div class="navbar navbar-inverse navbar-aoi">
<div class="navbar-inner">
<button class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="brand" href="/"><img alt="All Our Ideas" src="/images/logo_head.png?1572885143"/></a>
<div class="nav-collapse collapse">
<ul class="nav">
<li class="active">
<a href="/">Home</a>
</li>
<li>
<a href="/questions/new">Create</a>
</li>
<li>
<a href="/about">About</a>
</li>
<li><a href="http://blog.allourideas.org">Blog</a></li>
</ul>
<ul class="nav pull-right">
<li><a href="/sign_in">Log In</a></li>
</ul>
</div>
</div>
</div>
<script type="text/javascript">
//<![CDATA[
var AUTH_TOKEN = "RapmpwlNnv0IEtJU4yEOPSKep8P2MQ5ydt1djec0pEI=";
//]]>
</script>
<div class="content container-fluid" style="">
<div class="body">
<div class="row-fluid">
<div class="span10 offset1">
<div class="aoi-hero-unit">
<h1 class="headline">Bringing survey research into the digital age.</h1>
<h3 class="sub-headline">
Mix core ideas from survey research with new insights from crowdsourcing.
Add a heavy dose of statistics. Stir in a bit of fresh thinking. Enjoy.
</h3>
<p class="headline-buttons">
<a class="btn btn-large" href="/planyc_example?guides=true">
Try a Wiki Survey
</a>
<a class="btn btn-large btn-success" href="/questions/new">
Create a Wiki Survey
</a>
</p>
</div>
</div>
</div>
<div class="how-it-works">
<div class="row-fluid">
<div class="span12">
<h3 class="title">How a Wiki Survey works</h3>
</div>
</div>
<div class="row-fluid boxes">
<div class="span4 divider-holder">
<div class="divider hidden-phone"></div>
<a href="/questions/new">
<img alt="Create" src="/images/homepage/create.png?1572885143"/>
</a>
<h3>
<a href="/questions/new">
Create
</a>
</h3>
<p>Start with a question and some seed ideas, and you can create a wiki survey in moments.</p>
</div>
<div class="span4 divider-holder">
<div class="divider hidden-phone"></div>
<div class="participate-holder">
<a href="/planyc_example?guides=true">
<img alt="Participate" src="/images/homepage/participate.png?1572885143"/>
</a>
</div>
<h3>
<a href="/planyc_example?guides=true">
Participate
</a>
</h3>
<p>The participants you invite will enjoy our simple process of voting and adding new ideas.</p>
</div>
<div class="span4">
<a href="/planyc_example/results">
<img alt="Discover" src="/images/homepage/discover.png?1572885143"/>
</a>
<h3>
<a href="/planyc_example/results">
Discover
</a>
</h3>
<p>The best ideas will bubble to the top using our system that is open, transparent, and powerful.</p>
</div>
</div>
</div>
<div class="row-fluid">
<div class="span10 offset1">
<div class="descriptions">
<div class="row-fluid">
<div class="span5 image">
<img alt="Devices-with-site" src="/images/homepage/devices-with-site.jpg?1572885143"/>
</div>
<div class="span7">
<h3>Easy to use</h3>
<p>
Creating and running a wiki survey at All Our Ideas is quick and easy. Just start with a question and some seed ideas and in a few moments you'll have your own wiki survey. Your participants will enjoy it, and all you have to do is sit back and watch the best ideas bubble to the top.
</p>
</div>
</div>
<div class="row-fluid">
<div class="span5 pull-right image math-formula">
<img alt="Math-formula" src="/images/homepage/math-formula.png?1572885143"/>
</div>
<div class="span7">
<h3>Backed by research</h3>
<p>
All Our Ideas is a research project based at Princeton University that is dedicated to creating new ways of collecting social data. You can learn more about the theory and methods behind our project by <a href="http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0123483">reading our paper</a>, <a href="https://www.youtube.com/watch?v=ogryxTAgjTc">watching our talk</a>, or
<a href="http://www.bitbybitbook.com/en/asking-questions/how/wiki/">reading about wiki surveys</a>
in Matthew Salganik’s book <a href="http://www.bitbybitbook.com/">Bit by Bit: Social Research in the Digital Age</a>. Thanks to Google, the National Science Foundation, and Princeton for supporting this research.
</p>
</div>
</div>
<div class="row-fluid">
<div class="span5 image">
<img alt="Present" src="/images/homepage/present.jpg?1572885143"/>
</div>
<div class="span7">
<h3>Packed with features</h3>
<p>
We've built in lots of powerful features into All Our Ideas. For example you can
<a href="http://blog.allourideas.org/post/912665189/improved-widget-gives-you-more-control">embed your wiki survey in a different website,</a>
<a href="http://blog.allourideas.org/post/2739358388/download-your-data">download raw data for offline analysis,</a>
and
<a href="http://blog.allourideas.org/post/947544304/add-google-analytics-on-your-idea-marketplace">integrate your wiki survey with Google Analytics.</a>
And, our community of volunteers has translated the site into
<a href="http://blog.allourideas.org/post/5137327337/languages-and-all-our-ideas">more than 10 languages</a>
—including Arabic, Chinese, and Spanish.
</p>
</div>
</div>
<div class="row-fluid">
<div class="span5 pull-right image tux">
<img alt="Tux" src="/images/homepage/tux.png?1572885143"/>
</div>
<div class="span7">
<h3>Open source</h3>
<p>
The code that powers All Our Ideas is available
<a href="https://github.com/allourideas">open-source.</a>
That means that you can learn how it works, customize it for your own needs, and even
<a href="http://blog.allourideas.org/post/33581685629/bringing-our-code-to-your-server">install it on your own servers.</a>
We've already had people from all over the world contribute code to the project, and you are welcome to join our developer community.
</p>
</div>
</div>
</div>
</div>
</div>
<div class="row-fluid">
<div class="span12">
<h3 class="overall-stats">
Currently hosting
<span class="wiki-surveys-count">
20,025
</span>
wiki surveys with
<span class="ideas-count">
1,107,394
</span>
ideas and
<span class="votes-count">
38.2
million
</span>
votes including:
</h3>
<div class="flexslider">
<ul class="slides">
<li>
<a href="http://blog.allourideas.org/post/54853444073/un-global-sustainability-report-2013"><img alt="L_united-nations" src="/images/homepage/l_united-nations.jpg?1572885143"/></a>
</li>
<li>
<a href="http://blog.allourideas.org/post/14520279310/wikipedia-banner-challenge"><img alt="L_wikipedia" src="/images/homepage/l_wikipedia.jpg?1572885143"/></a>
</li>
<li>
<a href="http://blog.allourideas.org/post/1121098355/oecd-and-allourideas"><img alt="L_oecd" src="/images/homepage/l_oecd.jpg?1572885143"/></a>
</li>
<li>
<a href="http://blog.allourideas.org/post/14248022671/governor-genro-tops-president-obama-on-citizen"><img alt="L_rio_grande-do-sul" src="/images/homepage/l_rio_grande-do-sul.jpg?1572885143"/></a>
</li>
<li>
<a href="http://blog.allourideas.org/post/1601483272/washingtonpost-com-using-allourideas-widget"><img alt="L_washington_post" src="/images/homepage/l_washington_post.jpg?1572885143"/></a>
</li>
<li>
<a href="http://blog.allourideas.org/post/4526601799/calgary-all-our-ideas-and-civic-participation"><img alt="L_calgary" src="/images/homepage/l_calgary.jpg?1572885143"/></a>
</li>
<li>
<a href="http://blog.allourideas.org/post/7300721979/catholic-relief-services-and-allourideas"><img alt="L_catholic_relief" src="/images/homepage/l_catholic_relief.jpg?1572885143"/></a>
</li>
<li>
<a href="http://blog.allourideas.org/post/3425270094/what-does-democracy-mean"><img alt="L_digital_demo" src="/images/homepage/l_digital_demo.jpg?1572885143"/></a>
</li>
<li>
<a href="http://blog.allourideas.org/post/49023446765/new-york-city-parks"><img alt="L_ny-cp" src="/images/homepage/l_ny-cp.jpg?1572885143"/></a>
</li>
<li>
<a href="http://blog.allourideas.org/post/11281070104/the-junior-league-and-all-our-ideas"><img alt="L_junior-league" src="/images/homepage/l_junior-league.jpg?1572885143"/></a>
</li>
<li>
<a href="http://blog.allourideas.org/post/30177243497/matter-tries-collaborative-commissioning"><img alt="L_matter" src="/images/homepage/l_matter.jpg?1572885143"/></a>
</li>
<li>
<a href="http://blog.allourideas.org/post/6326304438/making-new-york-greener-and-greater"><img alt="L_nyc" src="/images/homepage/l_nyc.jpg?1572885143"/></a>
</li>
</ul>
</div>
</div>
</div>
<script src="/javascripts/jquery.flexslider-min.js?1572885143" type="text/javascript"></script>
<script type="text/javascript">
  //<![CDATA[
    $(document).ready(function() {
      $('.flexslider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: true,
        itemWidth: 190,
        prevText: '&#9664;',
        nextText: '&#9654;',
        slideshow: false
      });
    });
  //]]>
</script>
</div>
</div>
<div class="footer">
<div class="row-fluid">
<div class="span9">
<p>This project is supported by generous grants from:</p>
<p class="generous-grantees">
Google
<br/>
The National Science Foundation
<br/>
The World Bank
<br/>
The Center For Information Technology at Princeton University
</p>
</div>
<div class="span3">
<div class="footer-right pull-right">
<p>
Follow us:
</p>
<ul class="footer-social-icons">
<li class="fb"><a href="https://www.facebook.com/allourideas">Facebook</a></li>
<li class="twitter"><a href="https://twitter.com/allourideas">Twitter</a></li>
<li class="github"><a href="https://github.com/allourideas">GitHub</a></li>
<li class="blog"><a href="http://blog.allourideas.org/">Blog</a></li>
</ul>
<p>
<a href="/about">About</a>
<br/>
<a href="/privacy">Privacy and Consent Policy</a>
</p>
</div>
</div>
</div>
</div>
<script src="/javascripts/jquery.ba-url.min.js?1572885143" type="text/javascript"></script>
<script src="/javascripts/jquery.taconite.js?1572885143" type="text/javascript"></script>
<script src="/javascripts/jquery.hint.js?1572885143" type="text/javascript"></script>
<script src="/javascripts/label_over.js?1572885143" type="text/javascript"></script>
<script src="/javascripts/jquery.jqEasyCharCounter.js?1572885143" type="text/javascript"></script>
<script src="/javascripts/application.js?1572885143" type="text/javascript"></script>
<script src="/javascripts/jquery.form.js?1572885143" type="text/javascript"></script>
<script src="/javascripts/bootstrap/bootstrap.min.js?1572885143" type="text/javascript"></script>
<script src="/javascripts/retina.js?1572885143" type="text/javascript"></script>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-11703548-1']);
_gaq.push(['_trackPageview']);
</script>
<script type="text/javascript">
  //<![CDATA[
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  //]]>
</script>
</body>
</html>
