<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>www.unimore.gr   » Page not found</title>
<link href="https://www.unimore.gr/wp-content/themes/outdoorsy_fixed/style.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.unimore.gr/feed/" rel="alternate" title="www.unimore.gr RSS Feed" type="application/rss+xml"/>
<link href="https://www.unimore.gr/xmlrpc.php" rel="pingback"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.unimore.gr\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.unimore.gr/wp-includes/css/dist/block-library/style.min.css?ver=5.3.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.unimore.gr/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.unimore.gr/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.unimore.gr/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3.6" name="generator"/>
</head>
<body>
<div id="container">
<div id="header">
<h1><a href="https://www.unimore.gr">www.unimore.gr</a></h1>
<ul>
<li><a href="https://www.unimore.gr">home</a></li>
<li class="page_item page-item-80"><a href="https://www.unimore.gr/about-me/">About me</a></li>
<li class="page_item page-item-1891"><a href="https://www.unimore.gr/%ce%b5%cf%80%ce%b9%ce%ba%ce%bf%ce%b9%ce%bd%cf%89%ce%bd%ce%af%ce%b1/">Contact</a></li>
</ul>
</div>
<div id="content">
<div id="content-top">
<div id="content-container"> <div id="leftcol">
<h2>404 - Page Not Found</h2>
<p>Sorry, this page can not be located</p>
<form action="https://www.unimore.gr/" id="searchform" method="get">
<input class="input" id="s" name="s" size="16" style="display:inline;" type="text" value=""/>
<input class="submit" id="searchsubmit" style="display:inline;" type="submit" value="Search"/>
</form>
</div>
<div id="rightcol">
<ul id="sidebar">
<li class="widget widget_search" id="search-2"><form action="https://www.unimore.gr/" id="searchform" method="get">
<input class="input" id="s" name="s" size="16" style="display:inline;" type="text" value=""/>
<input class="submit" id="searchsubmit" style="display:inline;" type="submit" value="Search"/>
</form>
</li><li class="widget widget_categories" id="categories-2"><h4 class="widgettitle">Categories</h4> <ul>
<li class="cat-item cat-item-1"><a href="https://www.unimore.gr/category/art/">Art</a>
</li>
<li class="cat-item cat-item-20"><a href="https://www.unimore.gr/category/beauty/">Beauty</a>
</li>
<li class="cat-item cat-item-3"><a href="https://www.unimore.gr/category/books/">Books</a>
</li>
<li class="cat-item cat-item-19"><a href="https://www.unimore.gr/category/deco/">Deco</a>
</li>
<li class="cat-item cat-item-5"><a href="https://www.unimore.gr/category/escape/">Escape</a>
</li>
<li class="cat-item cat-item-9"><a href="https://www.unimore.gr/category/events/">Events</a>
</li>
<li class="cat-item cat-item-4"><a href="https://www.unimore.gr/category/fashion/">Fashion</a>
</li>
<li class="cat-item cat-item-7"><a href="https://www.unimore.gr/category/gourmet/">Gourmet</a>
</li>
<li class="cat-item cat-item-8"><a href="https://www.unimore.gr/category/hot-list-of-the-month/">Hot list of the month</a>
</li>
<li class="cat-item cat-item-18"><a href="https://www.unimore.gr/category/news/">News</a>
</li>
<li class="cat-item cat-item-10"><a href="https://www.unimore.gr/category/thoughts/">Thoughts</a>
</li>
<li class="cat-item cat-item-6"><a href="https://www.unimore.gr/category/unilife/">Unilife</a>
</li>
</ul>
</li><li class="widget widget_text" id="text-2"><h4 class="widgettitle">Μηχανογράφηση Επιχειρήσεων MiniSystems ABEE  SynergyERP</h4> <div class="textwidget"><a id="a_a87ff679a2f3e71d9181a67b7542122c"></a>
<!--[if IE]>
<script type="text/javascript" src="http://www.unimore.gr/wp-content/plugins/wp-banners-lite/wpbanners_show.php?id=4&cid=a_a87ff679a2f3e71d9181a67b7542122c"></script>
<![endif]-->
<script defer="defer" src="http://www.unimore.gr/wp-content/plugins/wp-banners-lite/wpbanners_show.php?id=4&amp;cid=a_a87ff679a2f3e71d9181a67b7542122c" type="text/javascript"></script></div>
</li></ul> </div>
</div>
<div id="footer">
<div class="function">
<!-- Free Theme brought to you by www.wefunction.com - Please be courteous and leave a link back to us! -->
<a href="http://www.wefunction.com">Theme by Function</a>
</div>
<div class="foot">
<ul>
<li class="first"><a href="https://www.unimore.gr">Home</a></li>
<li class="page_item page-item-80"><a href="https://www.unimore.gr/about-me/">About me</a></li>
<li class="page_item page-item-1891"><a href="https://www.unimore.gr/%ce%b5%cf%80%ce%b9%ce%ba%ce%bf%ce%b9%ce%bd%cf%89%ce%bd%ce%af%ce%b1/">Contact</a></li>
</ul>
<span class="copyright">© Copyright 2008 www.unimore.gr 
<!-- Start -->Wordpress themes<!-- End --></span>
</div>
</div>
</div>
</div>
</div>
<script src="https://www.unimore.gr/wp-includes/js/wp-embed.min.js?ver=5.3.6" type="text/javascript"></script>
</body>
</html>
