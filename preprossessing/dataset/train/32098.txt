<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>日本通_介绍日本资讯大型中文门户网站</title>
<meta content="日本,日本通,日本新闻,日本资讯,日本旅游,日本留学,日本动漫,日本文化,日本时尚" name="keywords"/>
<meta content="日本通是一个涵盖日本资讯、旅游、留学、动漫、文化、时尚、娱乐八卦等各方面新闻信息的综合性门户网站，立志打造最全、最新的日本新闻以及资讯的综合平台（www.517japan.com )。" name="description"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="webkit" name="renderer"/>
<link href="//at.alicdn.com/t/font_791978_voupi7qed9p.css" rel="stylesheet" type="text/css"/>
<link href="/dist/css/manifest.90785812.css" rel="stylesheet"/><link href="/dist/css/index-newsflashes.416ce38a.css" rel="stylesheet"/></head>
<body>
<div class="rbt_top">
<div class="top_wrap">
<div class="top_left">介绍日本资讯大型中文门户网站</div>
<div class="top_right">
<a class="reg" href="/index/user/register">注册</a>
<div class="line">|</div>
<a class="login" href="/index/user/login">登录</a>
<a class="tou" href="javascript:;"><span><i class="iconfont icon-tougao"></i>投 稿</span></a>
</div>
</div>
</div>
<div class="head">
<div class="head_wrap">
<div class="logo">
<a alt="日本通" href="/" title="日本通">
<img alt="日本通" height="76" src="/files/logo.png" width="194"/>
</a>
</div>
<div class="search">
<form action="/search.html" id="searchForm" method="get">
<input class="search_txt" name="searchkey" placeholder="" type="text" value=""/>
<a class="btn" href="javascript:void(0);" id="searchAction" title="搜索">
<i class="iconfont icon-search"></i>
</a>
</form>
</div>
<div class="code">
<div class="title">公众号</div>
<a class="wx" href="javascript:;">
<div class="box">
<i class="arrow1"></i>
<i class="arrow2"></i>
</div>
</a>
<div class="line"></div>
<a class="wb" href="https://weibo.com/517japancom" target="_blank"></a>
</div>
</div>
</div>
<div class="menu">
<div class="menu_wrap">
<div class="item item1">
<ul class="clear">
<li><a href="/">首页</a></li>
<li><a href="/home/">门户</a></li>
<li class="f"><a href="/newslist/">最新快讯</a></li>
<li><a href="/newsflash-2.html#box">经济</a></li>
<li class="t"><a href="/newsflash-3.html#box">社会</a></li>
<li class="t"><a href="/newsflash-10.html#box">动漫</a></li>
<li class="t"><a href="/newsflash-8.html#box">娱乐</a></li>
</ul>
</div>
<div class="item item2">
<ul class="clear">
<li><a href="/travel/">日本旅游</a></li>
<li><a href="/category/19.html">赴日航线</a></li>
<li><a href="/category/21.html">日本酒店</a></li>
<li><a href="/regionlist/">都道府县</a></li>
<li><a href="/category/32.html">赴日指南</a></li>
<li><a href="/category/20.html">日本交通</a></li>
</ul>
</div>
<div class="item item3">
<ul class="clear">
<li><a href="/qiyelist/">企业机构</a></li>
<li><a href="/category/27.html">日本医院</a></li>
<li><a href="/category/13.html">日本留学</a></li>
<li><a href="/themelist/">作家专栏</a></li>
<li><a href="/category/28.html">日本大学</a></li>
<li><a href="/category/30.html">日语学习</a></li>
</ul>
</div>
<div class="item item4">
<ul class="clear">
<li><a href="/category/33.html">中国在日本</a></li>
<li><a href="/category/34.html">日本在中国</a></li>
<li><a href="/single-1.html">关于日本通</a></li>
<li><a class="toMng" href="https://weidian.com/?userid=160609582&amp;spider_token=19d2=517japan" target="_blank">美能购</a></li>
</ul>
</div>
</div>
</div>
<div class="newsBanner">
<div class="f1">
<a href="/focus-726.html" target="_blank">
<img height="586" src="/upload/img/20210108/83b9f111702b0465baf6049fd341b662.jpg" width="584"/>
<div class="bg"></div>
<div class="txt">郭敬明的阴阳师，为什么如此尴尬？</div>
</a>
</div>
<div class="f2">
<ul class="clear">
<li>
<a href="/focus-723.html" target="_blank">
<img height="293" src="/upload/img/20210108/5b0da1b043028e6eb5a7b3b20d336608.jpg" width="293"/>
<div class="txt">在日本应该去哪里买文具？</div>
</a>
</li>
<li>
<a href="/focus-721.html" target="_blank">
<img height="293" src="/upload/img/20210103/ebe0f206058136ad725e6b2b7ce872d6.jpg" width="293"/>
<div class="txt">为什么日本人会为了炸鸡吵起来？</div>
</a>
</li>
<li>
<a href="/focus-724.html" target="_blank">
<img height="293" src="/upload/img/20210108/cc088f1514948ed70519b08746626a88.jpg" width="293"/>
<div class="txt">日本女装大佬图鉴</div>
</a>
</li>
<li>
<a href="/focus-725.html" target="_blank">
<img height="293" src="/upload/img/20210108/34f55e3f69d782c640c2b5c38b5b172c.jpg" width="293"/>
<div class="txt">日本人常用的APP都有啥？</div>
</a>
</li>
</ul>
</div>
</div>
<div class="newsflashes clear">
<div class="newsflashes_left">
<div class="main">
<div class="item">
<a href="/viewnews-113665.html" target="_blank">
<h1>
                        专访丨教日本传统企业赢得中国消费者好感的九零后新华侨                    </h1>
<div class="desc">
                        一位九零后新华侨企业家与众不同的人格魅力。                    </div>
</a>
</div>
<div class="line"></div>
<div class="item">
<a href="/viewnews-113758.html" target="_blank">
<h1>
                        第12期：日本最早2月底开始接种新冠疫苗；日版《微微一笑很倾城》预告丨百通板                    </h1>
<div class="desc">
                        大家好，欢迎来到【百通板】栏目，在这里你可以看到近期有关日本的大事小事。为便于读者及时了解日本疫情状况，百通板新增疫情速报板块，汇总每周日本疫情动态。                    </div>
</a>
</div>
</div>
<div class="news_box" id="box">
<ul class="clear">
<li>
<a class="news_nav active" data-url="/newsflashes/" href="javascript:;">最新文章</a>
</li>
<li>
<a class="news_nav " data-id="3" data-url="/newsflashes/" href="javascript:;">社会</a>
</li>
<li>
<a class="news_nav " data-id="5" data-url="/newsflashes/" href="javascript:;">文化</a>
</li>
<li>
<a class="news_nav " data-id="2" data-url="/newsflashes/" href="javascript:;">经济</a>
</li>
<li>
<a class="news_nav " data-id="6" data-url="/newsflashes/" href="javascript:;">企业</a>
</li>
<li>
<a class="news_nav " data-id="8" data-url="/newsflashes/" href="javascript:;">娱乐</a>
</li>
<li>
<a class="news_nav " data-id="10" data-url="/newsflashes/" href="javascript:;">动漫</a>
</li>
<li>
<a class="news_nav " data-id="14" data-url="/newsflashes/" href="javascript:;">图书</a>
</li>
<li>
<a class="news_nav " data-id="7" data-url="/newsflashes/" href="javascript:;">旅游</a>
</li>
</ul>
<div class="more_btn"></div>
<div class="more_list">
<ul>
<li><a class="news_nav " data-id="12" data-url="/newsflashes/" href="javascript:;">体育</a></li>
<li><a class="news_nav " data-id="11" data-url="/newsflashes/" href="javascript:;">游戏</a></li>
<li><a class="news_nav " data-id="13" data-url="/newsflashes/" href="javascript:;">留学</a></li>
<li><a class="news_nav " data-id="4" data-url="/newsflashes/" href="javascript:;">科技</a></li>
<li><a class="news_nav " data-id="9" data-url="/newsflashes/" href="javascript:;">时尚</a></li>
<li><a class="news_nav " data-id="532" data-url="/newsflashes/" href="javascript:;">金融</a></li>
</ul>
</div>
</div>
<div class="news_list">
<div class="n_item">
<a class="n_a" href="/viewnews-113609.html" target="_blank">
<div class="imgbox">
<img alt="9万日本人的钱，都砸在ta们这了！" class="lazy" data-original="/upload/img/20201208/0ff695b065b2374f5be90267f2f2de5b_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>9万日本人的钱，都砸在ta们这了！</h2>
<p>2020年cosme美妆大赏出炉，看你种草了哪几样？</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">时尚</a>
<span class="author"><a href="/author-49.html" target="_blank">九州旅游信息网</a></span>
<span class="time">2021-01-12 13:05:00</span>
</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113776.html" target="_blank">
<div class="imgbox">
<img alt="特评：在日中国公民感染新冠三位数增长的警钟" class="lazy" data-original="/upload/img/20210112/f92d71fc57a78ea2f5f23c0c0727e00a_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>特评：在日中国公民感染新冠三位数增长的警钟</h2>
<p>近两个星期来，大约已经有100多名在日中国公民确诊感染新冠。这个骤然突破的三位数，再次给旅日华侨华人敲响警钟：切莫大意，必须严格防控！</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">社会</a>
<span class="author"><a href="/author-90.html" target="_blank">蒋丰</a></span>
<span class="time">2021-01-12 11:14:12</span>
</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113757.html" target="_blank">
<div class="imgbox">
<img alt="混血美女，在日本是二等公民？" class="lazy" data-original="/upload/img/20210107/8358ea2be0945b22511eb46dc652dd03_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>混血美女，在日本是二等公民？</h2>
<p>“大多数日本人不希望自己的社会中存在种族歧视，但他们被告知日本只有一个种族。”</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">社会</a>
<span class="author"><a href="/author-1.html" target="_blank">日本通</a></span>
<span class="time">2021-01-12 11:07:00</span>
</div>
</div>
<div class="z_item">
<a class="z_a" href="/ad/895" target="_blank">
<img alt="" class="lazy" data-original="/upload/img/20201222/68493505eb50aa04b09722e3fc457f95.jpg" src="/files/blank.png"/>
</a>
<div class="tag">专题</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113764.html" target="_blank">
<div class="imgbox">
<img alt="怀念身为“中国通”和“日本通”的傅高义" class="lazy" data-original="/upload/img/20210108/d496a6a7bed85812244af5ef60134a51_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>怀念身为“中国通”和“日本通”的傅高义</h2>
<p>在海外最先报道出来这个消息的国家，居然是日本。</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">社会</a>
<span class="author"><a href="/author-90.html" target="_blank">蒋丰</a></span>
<span class="time">2021-01-12 09:06:00</span>
</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113769.html" target="_blank">
<div class="imgbox">
<img alt="冬季档来袭！2021年都有哪些日剧值得追？" class="lazy" data-original="/upload/img/20210111/5bdbeaa8f15b2d872fd485411e87f0b1_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>冬季档来袭！2021年都有哪些日剧值得追？</h2>
<p>神仙打架！</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">娱乐</a>
<span class="author"><a href="/author-49.html" target="_blank">九州旅游信息网</a></span>
<span class="time">2021-01-11 13:05:00</span>
</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113712.html" target="_blank">
<div class="imgbox">
<img alt="日本酒就是清酒？那你就错了" class="lazy" data-original="/upload/img/20201228/159bc58b93838fb602ee2128a8411f02_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>日本酒就是清酒？那你就错了</h2>
<p>你分得清楚：清酒、烧酒和泡盛，还有清酒中的大吟酿、吟酿、本酿造吗？</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">文化</a>
<span class="author"><a href="/author-1.html" target="_blank">日本通</a></span>
<span class="time">2021-01-11 11:04:00</span>
</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113686.html" target="_blank">
<div class="imgbox">
<img alt="京都最易错过却最不该错过的知恩院" class="lazy" data-original="/upload/img/20201223/e5d470e48e5a417b09d29445d8fe15bc_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>京都最易错过却最不该错过的知恩院</h2>
<p>在难得的“噪音”中，满怀欣喜地翻开千年古都历史的新的一页。</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">旅游</a>
<span class="author"><a href="/author-90.html" target="_blank">蒋丰</a></span>
<span class="time">2021-01-11 09:03:00</span>
</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113768.html" target="_blank">
<div class="imgbox">
<img alt="离谱吗？日43岁老宅男为了宝可梦卡，伪造中奖冰棍！" class="lazy" data-original="/upload/img/20210109/4f59461c7f7784783ce5868f489868e0_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>离谱吗？日43岁老宅男为了宝可梦卡，伪造中奖冰棍！</h2>
<p>橙心社——诚心制作每一期动漫节目</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">动漫</a>
<span class="author"><a href="/author-80.html" target="_blank">橙心社</a></span>
<span class="time">2021-01-10 19:08:00</span>
</div>
</div>
<div class="z_item">
<a class="z_a" href="/ad/892" target="_blank">
<img alt="" class="lazy" data-original="/upload/img/20200319/973fd7e87ca20932f5c13ad44215bee6.jpg" src="/files/blank.png"/>
</a>
<div class="tag">专题</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113523.html" target="_blank">
<div class="imgbox">
<img alt="英超中超都要降薪，为何日本联赛能说不？" class="lazy" data-original="/upload/img/20201123/f2f7e5fd4968c388e32e276e4ec679ea_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>英超中超都要降薪，为何日本联赛能说不？</h2>
<p>集体不降薪，日本球队怎么做到的？</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">体育</a>
<span class="author"><a href="/author-618.html" target="_blank">西北望看台</a></span>
<span class="time">2021-01-10 13:04:00</span>
</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113705.html" target="_blank">
<div class="imgbox">
<img alt="日本推出防走神头套，摸鱼的打工人看完快哭了" class="lazy" data-original="/upload/img/20201228/8edf381472b4863d1949529194990f99_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>日本推出防走神头套，摸鱼的打工人看完快哭了</h2>
<p>「走神」能被人为控制吗？半个月前，日本有公司准备试一试，他们推出了一款让打工人看完沉默，学习人看完泪目的设备：「WEAR SPACE」。</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">科技</a>
<span class="author"><a href="/author-1.html" target="_blank">日本通</a></span>
<span class="time">2021-01-10 11:02:00</span>
</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113660.html" target="_blank">
<div class="imgbox">
<img alt="专访丨打造全球再生医学科研基地的励志教授" class="lazy" data-original="/upload/img/20201218/c770deb134af4d1606cd1df1b3a0e17a_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>专访丨打造全球再生医学科研基地的励志教授</h2>
<p>健康、幸福、乐观、美丽的活到老，不再只是美好的祝福。</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">企业</a>
<span class="author"><a href="/author-90.html" target="_blank">蒋丰</a></span>
<span class="time">2021-01-10 09:04:00</span>
</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113571.html" target="_blank">
<div class="imgbox">
<img alt="平泉，在这里追忆金色国度的黄粱一梦" class="lazy" data-original="/upload/img/20201203/371162b5dd24502796e9c956cdf6378b_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>平泉，在这里追忆金色国度的黄粱一梦</h2>
<p>轰轰烈烈荡气回肠的故事，才讲了个开头……</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">旅游</a>
<span class="author"><a href="/author-625.html" target="_blank">日活</a></span>
<span class="time">2021-01-09 13:05:00</span>
</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113472.html" target="_blank">
<div class="imgbox">
<img alt="日本电子产业大败局" class="lazy" data-original="/upload/img/20201116/4c1e1af7294c974e17ed6a370ef6e0f0_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>日本电子产业大败局</h2>
<p>传说仍存、辉煌不再。</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">经济</a>
<span class="author"><a href="/author-1.html" target="_blank">日本通</a></span>
<span class="time">2021-01-09 11:05:00</span>
</div>
</div>
<div class="z_item">
<a class="z_a" href="/ad/848" target="_blank">
<img alt="" class="lazy" data-original="/upload/img/20190329/9774a46d96cdc2a4243b3e1f96c29c01.jpg" src="/files/blank.png"/>
</a>
<div class="tag">专题</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113729.html" target="_blank">
<div class="imgbox">
<img alt="日本“老字号”企业缘何历经危机而不倒" class="lazy" data-original="/upload/img/20210103/a719dbdbfa862d7d515a81d08cd11c57_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>日本“老字号”企业缘何历经危机而不倒</h2>
<p>在全球拥有百年历史的企业中， 大约40%是日本企业。</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">经济</a>
<span class="author"><a href="/author-43.html" target="_blank">日本新华侨报</a></span>
<span class="time">2021-01-09 09:08:00</span>
</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113760.html" target="_blank">
<div class="imgbox">
<img alt="《巨人》只剩四话！4月9日完结！结局能够撒花吗？" class="lazy" data-original="/upload/img/20210107/fd9e9766d1a5065d0a69cbc2ef401f70_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>《巨人》只剩四话！4月9日完结！结局能够撒花吗？</h2>
<p>橙心社——诚心制作每一期动漫节目</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">动漫</a>
<span class="author"><a href="/author-80.html" target="_blank">橙心社</a></span>
<span class="time">2021-01-08 19:05:00</span>
</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113754.html" target="_blank">
<div class="imgbox">
<img alt="郭敬明的阴阳师，为什么如此尴尬？" class="lazy" data-original="/upload/img/20210107/3b7e5792260079d2dbe9f8afe2d2ed68_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>郭敬明的阴阳师，为什么如此尴尬？</h2>
<p>最近，由郭敬明执导的《晴雅集》引发众多热议。</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">文化</a>
<span class="author"><a href="/author-1.html" target="_blank">日本通</a></span>
<span class="time">2021-01-08 11:05:00</span>
</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113747.html" target="_blank">
<div class="imgbox">
<img alt="日本天皇与“赤牛”" class="lazy" data-original="/upload/img/20210106/8e3e6353bce647d4b80032ea35c4520f_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>日本天皇与“赤牛”</h2>
<p>2021年元旦，日本宫内厅公布的德仁天皇全家福中，出现了“赤牛”与一对牛和兔。</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">社会</a>
<span class="author"><a href="/author-561.html" target="_blank">人民中国</a></span>
<span class="time">2021-01-08 09:06:00</span>
</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113748.html" target="_blank">
<div class="imgbox">
<img alt="比村奇石聚人心！小册子2020销售排行一家独大！" class="lazy" data-original="/upload/img/20210106/fa4510b00c4822dcf1f1ee9e39eb1427_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>比村奇石聚人心！小册子2020销售排行一家独大！</h2>
<p>橙心社——诚心制作每一期动漫节目</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">动漫</a>
<span class="author"><a href="/author-80.html" target="_blank">橙心社</a></span>
<span class="time">2021-01-07 19:04:00</span>
</div>
</div>
<div class="z_item">
<a class="z_a" href="/ad/883" target="_blank">
<img alt="" class="lazy" data-original="/upload/img/20190719/8cc9e14996447db4b297e413d1ba47cb.jpg" src="/files/blank.png"/>
</a>
<div class="tag">专题</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113761.html" target="_blank">
<div class="imgbox">
<img alt="日本导演竹内亮：中国人的抗疫成就是对14亿人共同努力的回报" class="lazy" data-original="/upload/img/20210107/addeb1d0163ab2b49a9eb0dbed0ea993_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>日本导演竹内亮：中国人的抗疫成就是对14亿人共同努力的回报</h2>
<p>1月6日，外交部发言人华春莹称赞纪录片《后疫情时代》：不带偏见地真实记录了中国走过的非凡历程。</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">社会</a>
<span class="author"><a href="/author-561.html" target="_blank">人民中国</a></span>
<span class="time">2021-01-07 18:18:47</span>
</div>
</div>
<div class="n_item">
<a class="n_a" href="/viewnews-113758.html" target="_blank">
<div class="imgbox">
<img alt="第12期：日本最早2月底开始接种新冠疫苗；日版《微微一笑很倾城》预告丨百通板" class="lazy" data-original="/upload/img/20210107/654d78d3d194aad973936270bb7c5bec_mediu.jpeg" src="/files/blank.png"/>
</div>
<div class="txt">
<h2>第12期：日本最早2月底开始接种新冠疫苗；日版《微微一笑很倾城》预告丨百通板</h2>
<p>大家好，欢迎来到【百通板】栏目，在这里你可以看到近期有关日本的大事小事。为便于读者及时了解日本疫情状况，百通板新增疫情速报板块，汇总每周日本疫情动态。</p>
</div>
</a>
<div class="info">
<a class="tag" href="javascript:;">社会</a>
<span class="author"><a href="/author-622.html" target="_blank">百通板</a></span>
<span class="time">2021-01-07 14:12:01</span>
</div>
</div>
</div>
<div class="loading">
<a class="loadingAction" data-id="" data-page="2" data-url="/newsflashes/" href="javascript:;">
                继续加载
            </a>
</div>
</div>
<div class="newsflashes_right">
<div class="news_tui1">
<div class="tag"></div>
<div class="hd">
<ul>
<li></li>
</ul>
</div>
<div class="bd">
<ul>
<li>
<a href="/ad/802" target="_blank">
<img height="227" src="/upload/img/20181108/09fe6660cc8ec3da68189d1a13b16c60.jpg" width="362"/>
</a>
</li>
</ul>
</div>
</div>
<div class="height20"></div>
<div class="hours_24">
<div class="title">
<span>最新快讯<i class="line"></i></span>
<a class="more" href="/newslist/" target="_blank">+</a>
</div>
<div class="timeline">
<div class="line"></div>
<ul class="clear">
<li class="active"><i></i><a href="/viewnews-113777.html" target="_blank">银魂登顶周末观影人次榜，终结鬼灭之刃12连冠</a><div class="time">2021-01-12 15:53:39</div></li>
<li class="active"><i></i><a href="/viewnews-113774.html" target="_blank">乃木坂46毕业生齐藤优里确诊新冠</a><div class="time">2021-01-12 10:04:00</div></li>
<li class="active"><i></i><a href="/viewnews-113771.html" target="_blank">日本发现新型变异新冠病毒</a><div class="time">2021-01-11 10:51:32</div></li>
<li><i></i><a href="/viewnews-113770.html" target="_blank">东京政府为新生儿提供10万日元补贴</a><div class="time">2021-01-11 09:47:53</div></li>
<li><i></i><a href="/viewnews-113767.html" target="_blank">东京暂停奥运圣火展览活动</a><div class="time">2021-01-10 11:02:00</div></li>
<li><i></i><a href="/viewnews-113766.html" target="_blank">日本要求所有入境人员提供阴性检测证明</a><div class="time">2021-01-09 10:38:48</div></li>
<li><i></i><a href="/viewnews-113763.html" target="_blank">日本不会暂停和中韩商务往来</a><div class="time">2021-01-08 10:59:48</div></li>
<li><i></i><a href="/viewnews-113762.html" target="_blank">最新！1月份中日航班信息汇总</a><div class="time">2021-01-08 10:01:46</div></li>
</ul>
</div>
</div>
<div class="news_tui2">
<a href="/ad/801" target="_blank">
<img height="196" src="/upload/img/20190426/085a0c5888d4528e538e23c99c4dfcc8.jpg" width="362"/>
</a>
<div class="tag"></div>
</div>
<div class="height10"></div>
<div class="hot_article">
<h2>热门文章</h2>
<ul class="clear">
<li><a href="/viewnews-113698.html" target="_blank"><i class="hot_icon hot1"></i>日剧《弥留之国的爱丽丝》第二季决定</a></li>
<li><a href="/viewnews-112560.html" target="_blank"><i class="hot_icon hot2"></i>日本女体盛宴：你以为的情色，是我用心成就的艺术</a></li>
<li><a href="/viewnews-113551.html" target="_blank"><i class="hot_icon hot3"></i>我们何时才能看到《FATE HF》最终章？ 最迟明年4月应该可以！</a></li>
<li><a href="/viewnews-113566.html" target="_blank"><i class="hot_icon hot4"></i>有钱人怎么打游戏？石油王直接买下《拳皇》公司！成最大股东！</a></li>
<li><a href="/viewnews-113580.html" target="_blank"><i class="hot_icon hot5"></i>《斩服少女》有后续故事？动画总作画监督在推特放图了！你们看过没？</a></li>
<li><a href="/viewnews-113611.html" target="_blank"><i class="hot_icon hot6"></i>日本用AI进行婚配筛选?现实魔幻主义又来了！这情节我看过！</a></li>
<li><a href="/viewnews-113616.html" target="_blank"><i class="hot_icon hot7"></i>少佐没有死？《紫罗兰》完美结局？20亿票房突破贺图是这样的！</a></li>
<li><a href="/viewnews-113628.html" target="_blank"><i class="hot_icon hot8"></i>日网友投诉大雄偷窥静香洗澡画面 表示应该删除！该骂该赞？</a></li>
<li><a href="/viewnews-113573.html" target="_blank"><i class="hot_icon hot9"></i>最新！12月份中日航班信息汇总</a></li>
<li><a href="/viewnews-113630.html" target="_blank"><i class="hot_icon hot10"></i>《疯狂动物城》新作要来了！2022年上线Disney+</a></li>
</ul>
</div>
<div class="height15"></div>
<div class="rbt_zl">
<h2>专栏入口</h2>
<div class="msg">
<div class="item">
<a class="author" href="/theme-101930.html" target="_blank">
<img src="/upload/img/20181203/e7f64c92daeaf7a3d547d99650a10db9_mediu.jpeg"/>
<h3>橙心社</h3>
</a>
<div class="desc">诚心制作每一期动漫节目，联系邮箱：i@cxacg.com 官方微博：weibo.com/cxacg 贴吧：橙心社吧 </div>
<div class="ct">
                        橙心社——诚心制作每一期动漫节目                        <a href="/viewnews-112521.html" target="_blank">[阅读全文]</a>
</div>
<div class="time">2020-05-08 19:03:00</div>
</div>
</div>
<div class="msg">
<div class="item">
<a class="author" href="/theme-101125.html" target="_blank">
<img src="/upload/img/20191121/0a2890ee2b76519cd5661163dfd8201c.jpg"/>
<h3>日本通丨微信公众号</h3>
</a>
<div class="desc">致力于做新鲜有趣的日本相关科普，给大家还原一个真实的日本~</div>
<div class="ct">
                        今年的春季档日剧，居然出现了不少有生之年系列的“神剧回归”！惹得日剧迷们正盼星星盼月亮地等待着四月的到来。                        <a href="/viewnews-112172.html" target="_blank">[阅读全文]</a>
</div>
<div class="time">2020-03-25 10:05:00</div>
</div>
</div>
<div class="msg">
<div class="item">
<a class="author" href="/theme-101172.html" target="_blank">
<img src="/upload/img/20181106/002944704c81be346f960af540f786a3_mediu.jpeg"/>
<h3>九州旅游信息网</h3>
</a>
<div class="desc">九州相关新闻、活动、游记。</div>
<div class="ct">
                        品味长崎之美                        <a href="/viewnews-112510.html" target="_blank">[阅读全文]</a>
</div>
<div class="time">2020-05-06 13:36:09</div>
</div>
</div>
</div>
<div class="height20"></div>
<div class="baike">
<h2>小科普</h2>
<div id="tagscloud"></div>
</div>
<script type="text/javascript">
	var entries = []</script>
<div class="height10"></div>
<div class="rbt_wx">
<h2>日本通微信</h2>
<div class="desc">文化丨八卦丨吸猫丨开车</div>
<img height="154" src="/files/code.jpg" width="154"/>
</div>
</div>
</div>
<div class="footer">
<div class="footer_wrap">
<div class="part1">
<h2>产品和服务</h2>
<div class="item">
<a class="toMng" href="https://weidian.com/?userid=160609582&amp;spider_token=19d2=517japan" target="_blank">
<div class="pd_icon">
<img alt="美能购" src="/files/mn_logo.png"/>
</div>
<div class="pd_name">美能购</div>
<div class="pd_desc">直通日本 淘尽好货</div>
</a>
</div>
</div>
<div class="part2">
<h2>关于日本通</h2>
<ul>
<li><a href="/single-1.html" target="_blank">关于我们</a></li>
<li><a class="tou" href="javascript:;">我要投稿</a></li>
<li><a href="javascript:;" target="_blank">加入我们</a></li>
<li><a href="/single-3.html" target="_blank">服务条款</a></li>
<li><a href="/single-4.html" target="_blank">免责声明</a></li>
<li><a href="javascript:;" target="_blank">网站导航</a></li>
</ul>
</div>
<div class="part3">
<h2>合作伙伴</h2>
<ul>
<li>
<a href="http://www.welcomekyushu.com.cn" rel="nofollow" target="_blank">
<img alt="九州旅行信息" src="/files/hz_1.png"/><span>——</span>九州旅游信息网
					</a>
</li>
<li>
<a href="http://japan.people.com.cn/" rel="nofollow" target="_blank">
<img alt="人民网日本频道" src="/files/hz_2.png"/><span>——</span>日本频道
					</a>
</li>
<li>
<a href="http://www.peoplechina.com.cn/" rel="nofollow" target="_blank">
<img alt="人民中国" src="/files/hz_5.png"/><span>——</span>人民中国
					</a>
</li>
<li>
<a href="http://www.huxiu.com/" rel="nofollow" target="_blank">
<img alt="虎嗅网" src="/files/hz_6.png"/><span>——</span>虎嗅网
					</a>
</li>
<li class="big">
<a href="/tongxuehui/" rel="nofollow" target="_blank">
<img alt="中国长崎同学会" src="/files/hz_3.png"/><span>——</span>中国长崎同学会
					</a>
</li>
<li class="big">
<a href="/nagasaki-issc/" rel="nofollow" target="_blank">
<img alt="长崎留学生支援中心" src="/files/hz_4.png"/><span>——</span>长崎留学生支援中心
					</a>
</li>
</ul>
</div>
<div class="part4">
<div class="item">
<div class="title">
<span>日本通微博<b>V</b></span>
</div>
<a href="https://weibo.com/517japancom" rel="nofollow" target="_blank">+ 关注</a>
</div>
<div class="item">
<div class="title">
<span>客户端切换</span>
</div>
<a class="toWap" href="javascript:;">手机版</a>
</div>
</div>
</div>
<div class="copyright">
<a class="footer_logo" href="/"><img height="34" src="/files/footer_logo.png" width="83"/></a>
<span class="txt">Copyright © 2010-2021 日本通, 美科信息科技（厦门）有限公司 All Rights Reserved <a href="http://www.beian.miit.gov.cn/" target="_blank">闽ICP备12011161号</a></span>
<!--		<a href="https://v.yunaq.com/certificate/?domain=www.517japan.com" target="_blank" rel="nofollow">-->
<!--			<img src="/files/smyz.png" width="47" height="18">-->
<!--		</a>-->
</div>
</div>
<div class="fixed_nav">
<ul>
<li class="tou">
<i class="iconfont icon-tougao"></i>
<a class="floor_name" href="javascript:;">投 稿</a>
</li>
<li>
<i class="iconfont icon-erweima"></i>
<a class="floor_name" href="javascript:;">扫一扫</a>
<div class="bar_pop">
<div class="ewm">
<i class="arrow1"></i>
<i class="arrow2"></i>
<img alt="微信公众号" src="/files/gz.jpg"/>
<p>微信公众号</p>
</div>
</div>
</li>
<li id="backTop">
<i class="iconfont icon-fanhuidingbu"></i>
<a class="floor_name" href="javascript:;">回到顶部</a>
</li>
</ul>
</div>
<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?138bda5a75cfda5678497f184a4d2b6b";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>
<div style="display:none;">
<script src="https://s95.cnzz.com/z_stat.php?id=1259885373&amp;web_id=1259885373"></script>
</div>
<script src="/dist/js/vendor-9f116a16.bundle.js" type="text/javascript"></script><script src="/dist/js/manifest-81ab42ef.bundle.js" type="text/javascript"></script><script src="/dist/js/index-newsflashes-b2dec3a2.bundle.js" type="text/javascript"></script></body>
</html>