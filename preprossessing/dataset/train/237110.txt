<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "83503",
      cRay: "61115336e93d0fc3",
      cHash: "92b3943bb1dbbfe",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmxlZXBpbmdjb21wdXRlci5jb20vZm9ydW1zL3RvcGljMzEyNzExLmh0bWw=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "LE5tSOi5DAQx3zrcMycn9f6KQmysD4Z7X/GoAG+T/IBSTkavXOOBtNISZsti4ZltiV8vh2y9dApcNZYV+OVEdKb5Ir0ksL1KQ8J5g49t7t+JwX2FGsHjvlmYR/lpb3g6wMR/GbMViPbixGrLNrXJsk5phdHq1gRfewYoIlDJi2TlHH2WUjPxWpGTclbmJGnuRuE7fIcpZgqMdBLI92I2/e18fMogfi0IXIZbt8jpgnnJa0wlvRr7IzkOIV6o9Xrz4HuF88q1NGyQzsjxq1BgKDMiXXl7L4wzRC3yUu6clLQfhnPX54KUHWpJea1ZlsGMvxrsJCCAft0KygL0JNf4Ya9wazHMahXKytcldBzVWc7whRt0BVWQ/zGywwsv1mnKkwbGUCOnl+rzvIr64s0uJmFOZvbG03BTWs5KBAk/ACsRuHkxzYtjFgf19fYlkHuKRpdKuXS7KqBN1r+N5WsHX/zvzsUT3yIk0q36cNDGI3rzQmB8EmRh2C1aPLhojG2zzJWlJVyHbUgSaGmTddNueUNhDRcDpDPkFMhBQuhjzd5HULRv43aAzorCPCSJ3kAJXzCjR8L6/yazweCEf1ugZuUTOIdz0J5SZ+8WIL5QK3xZ59u7yS6POp/7NwSrI3rlCrNVGPlQBfo9B0iwgR8S09/6SKGFsUthqJmJKlb9+tySYQAcVSrzUlG0NmQiygQTF5edIB8rq80yv+kPEcVnK7Y2aqNiUjDjIVHHAXDn/cSa8qD0jn+8Ia3UObUhi/ao",
        t: "MTYxMDU2NDA1OC43MTAwMDA=",
        m: "rRDcycKWU7watgO8DIEjd1v/aBC2XSMHMTnT1Ef8Hbc=",
        i1: "NAGFURlLVR7Ooqz44IuTlA==",
        i2: "rOskOPkO+lGvGb5UIfvf1w==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "dJN5fMttq0gX4bQSBjrRPzGVBSKZhY8SbMFBG73bxXs=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=61115336e93d0fc3");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> bleepingcomputer.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/forums/topic312711.html?__cf_chl_jschl_tk__=55ca9a02493df0ba77c22c531d5321ae34fc25b7-1610564058-0-AcarmDe8clWJ_QWDFolp1_o4C1_LzHITTKFg59X7kc8QHcDnUr0U_dS_eFuu-e8WwKkx4FLyL_a-eHCDVjz2Up3R-HBK-0cezMb1ir2fQAGIoaZY7_WKkF8HfX5mJlkfxtIEcpYKGV9hzXAeuRFb1v-ayL7kOy3HdUBB_u7GbQ1QqytVCf0jrMuKxH2dn0bqEYQ3BuSlkobFEHdb2VbrGOPvoG4_s-aa4oIAqodgfwWVaOJqSxIIMlgHqiy0v8KO3FeA07fkOl0PfUW6pLtkhxuFFSg2Shs13qwAU55AvpuegfK4iE04yOYSVVh1Q7-kBuL9JC0EqDcvUGtJCdnCLc0" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="974f97af17e558e48b03a594ab914682342175d8-1610564058-0-AXCXx6cNO8B0rlpDJntFqsYQLET/WyD5W4bNzc/YeQAvCt7IFVSMclpo68lhd30KvsZbV4NFn+CclISOYToJCG4MCuxvSHXjp2ijnC0QnYoCBaK/gPu3rQgnNZ7VnVGfcloFR6S/8uXprzd5bZWebmoPeEa+FWYKNSxVFsxP/S+EbTi5f/vjG28wiiZj5SAonNfVWS/SFzR32eFHehiog/jvbtNzrXmcBTK2X3QkCaqlJOvo6OBh39XEFYcP6nokNexbbYYXkHQA9Ple0+14xpoSX4NSdMjsaXJC3MAzH7MYSf5hJ//Pou92IA4LYY83mbjUQWf3tqssx7rUpt6r4cKl6ThdTgZgVpUn81x4NZcchb+4jFwJgWpDht9JI9y//BcvbBG1TR/8MOJO6NVqInxlaUB3xkzio4e4XLEWkwFkfu8j+fWdkj0yf3G5WkV4HA4zioCf1pEQ5jyFC2Kjywsr39gsuXDhuAQPuDwD3rYFlqri0ZZR4KhZ++poRmvKopZOIkO7/iP3HyFO2RGqIonnb3NNoHpVMTrGmBMuQGLuD3uPHPo3c/jm8EhRbzZOIIzuaqUdrQVHff04qGnnDSEUhth6UDPbJOvsDRncgH8a+xRcuien9wW9Xpc5JXkrwFhfYGo/5eBkUu6gckosoE/h4iCpSy5dV5Fu8FbCgS9kieyc59abtB0ULtmTPFPANewW6ixGl81rPqUk8Eg1Nm9dEVpX4xqqg34BLpcsWX/hsU1eo6OMPmEx9YqhWSjHAWDz2uUM19t2D6Y17OGai1Jpb8JkF4JR0iGeEUPlIXnwY9sIfVDvAxX2WhN81s2gCp5Ysp8xlwB6gHf/eODHLBbD1qGyKHogR1Xf6k7+/yoy8MhoOTniudX56fiR/J+gubnZQXS2rjYib62g1dkV5jklEYBGIlfiX+7aZcEf9kaACSkImoZfTg3FLbzjtn9j1Qu5lz/xSJJX4/nN7MWlLQpIWYc+xagHeshrfc9Zde1tDxw1pPqul5XqCYJqnTPrVuorjWP9V2hGjn4OLZLQgi7ZTfXQmgKtInghzbvceop6q+y1dQwFH3Vn9dsCxI5gXl1McX51MUQdi2SSWsbOQWwOIQdKLePjV/NQkH+W3tqrWEN/7MN/p4SERyfw+0NbE4pJs0e4xP5gB2YtsR6oiIW1OD1llllTBLrHAsB736P6/BzZeUFsPL3T2h1Wc2NNL0Xuby8XEFxWY+pKuXjkH+ZE869UjGVIrfChtWwBfeuRmtA6SXMoa3uEGrE4uOCx1UscKAak+jtVRbRD3gprs+SueaDSaJUyumb0Q371NH+C"/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="232c31e9ec647e35f9edbdf6e10b2520"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610564062.71-7Tg6akzTDn"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=61115336e93d0fc3')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>61115336e93d0fc3</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
