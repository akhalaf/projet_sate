<!DOCTYPE html>
<html dir="ltr" lang="de">
<head>
<title>Agrarhandel Langholt</title>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes" name="viewport"/>
<meta content="global" name="dcterms.audience"/>
<meta content="Agrarhandel Langholt" name="author"/>
<meta content="Agrarhandel Langholt" name="publisher"/>
<meta content="Agrarhandel Langholt, Startseite, Rhauderfehn, Neudorff, Bayer Garten, Kerbl, Gardena, Agrar, Landwirtschaft, Futtermittel, Pflanzenschutzmittel, Schauml;dlingsbekauml;mpfung, Heimtierbedarf, Garten, Pferd, Reiter, Stall- Hofbedarf, Tierzucht, Weidezaunbedarf, Insektenbekauml;mpfungsmittel, Nahrungsergauml;nzung, Unkrautmittel" name="keywords"/>
<meta content="Agrarhandel Langholt, mit Artikeln aus Haus, Tier, Garten: Spiess Urania, Bayer Garten, Neudorff, Gardena, Kerbl, AKO, Havens, Frunol, rotie-pharm" name="description"/>
<meta content="Wir bieten Ihnen alles rund um Haus, Tier und Garten in Rhauderfehn und Umgebung" name="abstract"/>
<meta content="index,follow" name="robots"/>
<meta content="5 days" name="revisit-after"/>
<meta content="de" name="content-language"/>
<meta content="de" name="language"/>
<meta content="yes" name="allow-search"/>
<meta content="all" name="audience"/>
<link href="https://www.agrarhandel-langholt.de/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/android-chrome-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/site.webmanifest" rel="manifest"/>
<link color="#5bbad5" href="/safari-pinned-tab.svg" rel="mask-icon"/>
<meta content="#da532c" name="msapplication-TileColor"/>
<meta content="#ffffff" name="theme-color"/>
<link href="https://www.agrarhandel-langholt.de/" rel="canonical"/>
<link href="https://www.agrarhandel-langholt.de/humans.txt" rel="author" type="text/plain"/>
</head>
<body>
<header class="header-section" id="header">
<div class="container-fluid">
<div class="row">
<div class="navbar navbar-fixed-top">
<div class="l-dark" id="quick-access"> 
							 
                  		</div>
</div><!-- nav -->
</div><!-- Row /- -->
</div>
<div class="container">
<div class="row">
<div>
<div class="ow-navigation navbar-brand">
<div></div>
<a class="navbar-brand" href="shop/index.html" title="Firmenlogo Agrarhandel Langholt GbR"><img alt="Logo" src="shop/images/logo_kopf_04_2018_d.jpg" title="Firmenlogo Agrarhandel Langholt GbR"/></a>
</div> <!--/.nav-collapse -->
</div>
</div><!-- nav /- -->
</div>
</header>
<main>
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="padding-50"></div>
<div>
<h1>Agrarhandel Langholt</h1>
<div class="padding-60"></div>
<p>
<strong>Aus aktuellem Anlass ist der Webshop offline!</strong>
<br/>
<br/>
<br/>
<br/>
                        		Ihr Fachmarkt für Haus, Tier und Garten<br/>Buchweizenkamp 3 - 26817 Rhauderfehn / Langholt<br/>
<a href="mailto:info@agrarhandel-langholt.de" title="Schicken Sie uns eine Mail">info@agrarhandel-langholt.de</a><br/>
</p>
<br/>
<h2>
								Über uns
							</h2>
<p>
								Wir sind im klassischen Sinn ein Agrarhandel und bieten Produkte aus den Bereichen Haus und Garten, Heimtierbedarf, Pferd und Reiter, Stall- und Hofbedarf, Tierzucht und Haltung, Weidezaunbedarf und Zubehör. 
								Auch eine telefonische oder schriftliche (per Mail) Beratung zu den angegebenen Öffnungszeiten sowohl zu den Produkten als auch zu deren Anwendung ist selbstverständlich möglich. Wir freuen uns auf Ihren Einkauf und beraten Sie gerne!
								 - Vielen Dank!
							</p>
<div class="padding-100"></div>
<div class="style5">
								Agrarhandel Langholt, Buchweizenkamp 3, 26817 Rhauderfehn, Tel.: 04952 / 2932
							</div>
<div class="style3">
<p>Agrarhandel Langholt − Ihr Fachmarkt für Haus, Tier und Garten. Mit Produkten u.a. aus Haus und Garten, Heimtierbedarf, Pferd und Reiter, Stall und Hofbedarf, Tierzucht und Haltung, Weidezaunbedarf und Zubehör.
								<br/>
							  	Brauchen Sie Beratung zu den Produkten? Wir sind Mo. - Fr. durchgehend von 8:30-17:30 Uhr und Sa. von 8:30-12:00 Uhr telefonisch erreichbar oder schicken Sie uns einfach eine Mail.
							  	Wir sind im klassischen Sinn ein Agrarhandel und bieten Produkte aus den Bereichen Haus und Garten, Heimtierbedarf, Pferd und Reiter, Stall- und Hofbedarf, Tierzucht und Haltung, Weidezaunbedarf und Zubehör. <br/>
							  	Auch eine telefonische oder schriftliche (per Mail) Beratung zu den angegebenen Öffnungszeiten sowohl zu den Produkten als auch zu deren Anwendung ist selbstverständlich möglich.
							  	Wir freuen uns auf Ihren Einkauf und beraten Sie gerne!<br/>
							  	Wir erhoffen uns auch Rückmeldung vom Kunden, wenn es Probleme oder andere Ungereimtheiten im Shop gibt, damit das Einkaufen für Sie optimiert werden kann! - Vielen Dank!<br/>
<br/>
<br/>
</p>
<br/>
<br/>
<br/>
</div>
<div class="padding-50"></div>
</div><!--/.col-xs-12.col-sm-9--><!--/row-->
</div>
</div>
</div>
</main>
<footer class="l-dark l-dark__reversed" id="footer">
<div class="container container-fluid__v-padding">
<div class="row">
<div class="col-md-4 col-sm-6 col-xs-12">
                  		Kontakt
                  		<div> <address>
								Agrarhandel Langholt GbR<br/>
								Buchweizenkamp 3<br/>
								26817 Rhauderfehn<br/>
</address>
<ul class="list-unstyled">
<li><span class="glyphicon glyphicon-earphone"></span> 04952 2932</li>
<li><span class="glyphicon glyphicon-envelope"></span><a href="mailto:nfo@agrarhandel-langholt.de" title="Schicken Sie uns eine Mail"> info@agrarhandel-langholt.de</a></li>
</ul>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<ul class="list-unstyled">
<li><a href="https://www.agrarhandel-langholt.de/shop/impressum.html" title="Unsere Daten und Haftungsausschluss/Disclaimer"> Impressum</a></li>
<li><a href="https://www.agrarhandel-langholt.de/shop/versandkosten.html" title="Detaillierte Versandinformationen"> Versandkosten</a></li> <li><a href="https://www.agrarhandel-langholt.de/shop/agb.html" title="Wichtig: Unsere AGB, bitte lesen"> AGB</a></li>
<li><a href="https://www.agrarhandel-langholt.de/shop/widerruf.html" title="Infos zum Widerrufsrecht"> Widerrufsrecht &amp; Muster-Widerrufsformular</a></li>
<li><a href="https://www.agrarhandel-langholt.de/shop/datenschutz.html" title="Infos zum Datenschutz (wie behandeln wir Ihre Daten)"> Datenschutzerklärung </a></li>
</ul>
</div>
<div class="col-md-4 col-sm-12 col-xs-12">
<ul class="list-unstyled">
<li>© 2018 by Agrarhandel Langholt GbR. Bei Fragen oder Anregungen kontaktieren Sie bitte unseren <a href="mailto:info@agrarhandel-langholt.de?subject=Feedback%20auf%20Homepage&amp;body=Hallo.%20Zu%20Ihrer%20Homepage%20habe%20ich%20folgendes%20anzumerken:" title="Schicken Sie unserem Webmaster eine Mail">Webmaster</a>.<br/>Wir sind von Mo. - Fr. durchgehend ab 8:00-17:30 Uhr und Sa. ab 8:00-12:00 Uhr telefonisch erreichbar oder schicken Sie uns einfach eine E-Mail.</li>
</ul>
</div>
</div>
</div>
</footer>
<link href="index.css" rel="stylesheet" type="text/css"/>
</body>
</html>