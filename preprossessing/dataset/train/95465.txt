<!DOCTYPE html>
<html class="" lang="en-US" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<!-- This is Squarespace. --><!-- philippe-alexandre -->
<base href=""/>
<meta charset="utf-8"/>
<title>Alexandre</title>
<link href="https://images.squarespace-cdn.com/content/v1/51e5921ee4b0c6df9a7cb5df/1606582065318-LNAGFCFK0BYGGXIV9E3L/ke17ZwdGBToddI8pDm48kKVx4iaAMB8cUt5FBVj64TB7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QHyNOqBUUEtDDsRWrJLTmfvA29c-QhvrZHuvofdMeycU3v23_7YOVVNrb4qLIzC3xpYkGQgVJAnQfeZwpfss8/favicon.ico?format=100w" rel="shortcut icon" type="image/x-icon"/>
<meta content="Alexandre" property="og:site_name"/>
<meta content="Alexandre" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="http://static1.squarespace.com/static/51e5921ee4b0c6df9a7cb5df/t/5fc27e7b5147b1480483ab6a/1610644033341/?format=1500w" property="og:image"/>
<meta content="Alexandre" itemprop="name"/>
<meta content="http://static1.squarespace.com/static/51e5921ee4b0c6df9a7cb5df/t/5fc27e7b5147b1480483ab6a/1610644033341/?format=1500w" itemprop="thumbnailUrl"/>
<link href="http://static1.squarespace.com/static/51e5921ee4b0c6df9a7cb5df/t/5fc27e7b5147b1480483ab6a/1610644033341/?format=1500w" rel="image_src"/>
<meta content="http://static1.squarespace.com/static/51e5921ee4b0c6df9a7cb5df/t/5fc27e7b5147b1480483ab6a/1610644033341/?format=1500w" itemprop="image"/>
<meta content="Alexandre" name="twitter:title"/>
<meta content="http://static1.squarespace.com/static/51e5921ee4b0c6df9a7cb5df/t/5fc27e7b5147b1480483ab6a/1610644033341/?format=1500w" name="twitter:image"/>
<meta content="summary" name="twitter:card"/>
<meta content="" name="description"/>
<link href="https://images.squarespace-cdn.com" rel="preconnect"/>
<script src="//use.typekit.net/ik/jSBNXIZiCzcp0JtS9IybfE-Sy8It7Ma78UdRWmsf0c6feTwIfFHN4UJLFRbh52jhWD9UZQ9owRB8ZQsKwewhZejojQiRjRmqZy7SMkG0jAFu-WsoShFGZAsude80ZkoRdhXCHKoyjamTiY8Djhy8ZYmC-Ao1Oco8if37OcBDOcu8OfG0SeyTde83Se9ljAU8peB0ZY48O1FUiABkZWF3jAF8OcFzdP37O1FUiABkZWF3jAF8ShFGZAsude80ZkoRdhXCjAFu-WsoShFGZAsude80ZkoRdhXCjAFu-WsoShFGZAsude80Zko0ZWbCjAU8peB0ZY48ZhBCdemkpPoRdhXCjWw0dA9CdeNRjAUGdaFXOYgG-AUTSYg8OABCZWyydcskZPoDSWmyScmDSeBRZPoRdhXCSaBujW48SagyjhmDjhy8ZYmC-Ao1OcFzdPUaiaS0jAFu-WsoShFGZAsude80ZkoRdhXCiaiaOcBRiA8XpWFR-emqiAUTdcS0dcmXOYiaikoydemtjAoqScm1jAUCZW4oOcFzdPUaiaS0jWw0dA9CiaiaO1gG-AUTSYg8OABCZWyydcskZPoDSWmyScmDSeBRZPoRdhXCiaiaO1FUiABkZWF3jAF8ShFGZAsude80ZkoRdhXKIcBqdh48OAiyScBldhoqOWgkdkJsj14ydcszdKu1ScNXZWFUiA97fbRkFgMMeMt6MKG4fOZbIMIjgfMfH6qJz8MbMs6BJMHbMSVGKLCB.js" type="text/javascript"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<link href="//fonts.googleapis.com/css?family=Bitter:400" rel="stylesheet" type="text/css"/>
<script type="text/javascript">SQUARESPACE_ROLLUPS = {};</script>
<script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/cldr-resource-pack-d2b6094e965c882c2de89-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-cldr_resource_pack');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/cldr-resource-pack-d2b6094e965c882c2de89-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-vendors-2ec094db00b9e6d92d8fd-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common_vendors');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/common-vendors-2ec094db00b9e6d92d8fd-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-2c32d110777189549ff4d-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/common-2c32d110777189549ff4d-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/commerce-29ceb73d72817ae44cf08-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-commerce');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/commerce-29ceb73d72817ae44cf08-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].css = ["//assets.squarespace.com/universal/styles-compressed/commerce-c6a9de0bd3119cb26a512448db40c590-min.en-US.css"]; })(SQUARESPACE_ROLLUPS, 'squarespace-commerce');</script>
<link href="//assets.squarespace.com/universal/styles-compressed/commerce-c6a9de0bd3119cb26a512448db40c590-min.en-US.css" rel="stylesheet" type="text/css"/><script data-name="static-context">Static = window.Static || {}; Static.SQUARESPACE_CONTEXT = {"facebookAppId":"314192535267336","facebookApiVersion":"v6.0","rollups":{"squarespace-announcement-bar":{"js":"//assets.squarespace.com/universal/scripts-compressed/announcement-bar-8b244fce99594deac3684-min.en-US.js"},"squarespace-audio-player":{"css":"//assets.squarespace.com/universal/styles-compressed/audio-player-03a5305221e9f3857f5d3fbff2cd9bbe-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/audio-player-9d33505677455a9b22add-min.en-US.js"},"squarespace-blog-collection-list":{"css":"//assets.squarespace.com/universal/styles-compressed/blog-collection-list-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/blog-collection-list-ab4142fcacca918cf4e2d-min.en-US.js"},"squarespace-calendar-block-renderer":{"css":"//assets.squarespace.com/universal/styles-compressed/calendar-block-renderer-1e8a762808391e4b0bd8945da50793ac-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/calendar-block-renderer-82b361c64e6e75913711e-min.en-US.js"},"squarespace-chartjs-helpers":{"css":"//assets.squarespace.com/universal/styles-compressed/chartjs-helpers-9935a41d63cf08ca108505d288c1712e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/chartjs-helpers-270e1573dd28dff07fc7c-min.en-US.js"},"squarespace-comments":{"css":"//assets.squarespace.com/universal/styles-compressed/comments-f794dccd3bb871fc0cbc0bb7ad024168-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/comments-1b8d1adb275f098d8dc6d-min.en-US.js"},"squarespace-commerce-cart":{"js":"//assets.squarespace.com/universal/scripts-compressed/commerce-cart-9fd3c3147f23827502474-min.en-US.js"},"squarespace-dialog":{"css":"//assets.squarespace.com/universal/styles-compressed/dialog-4c984bcaacc45888f9092057493234b6-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/dialog-614b07c3f84e1e3b30662-min.en-US.js"},"squarespace-events-collection":{"css":"//assets.squarespace.com/universal/styles-compressed/events-collection-1e8a762808391e4b0bd8945da50793ac-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/events-collection-415312b37ef6978cf711b-min.en-US.js"},"squarespace-form-rendering-utils":{"js":"//assets.squarespace.com/universal/scripts-compressed/form-rendering-utils-da71321e53a08371a214c-min.en-US.js"},"squarespace-forms":{"css":"//assets.squarespace.com/universal/styles-compressed/forms-763d974ea7719bb18959e8f0a891abe6-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/forms-9fe4eb33891804b4464fe-min.en-US.js"},"squarespace-gallery-collection-list":{"css":"//assets.squarespace.com/universal/styles-compressed/gallery-collection-list-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/gallery-collection-list-5d5ff461e8bba64f298dc-min.en-US.js"},"squarespace-image-zoom":{"css":"//assets.squarespace.com/universal/styles-compressed/image-zoom-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/image-zoom-f7178bbfa97ac5234c120-min.en-US.js"},"squarespace-pinterest":{"css":"//assets.squarespace.com/universal/styles-compressed/pinterest-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/pinterest-9dd1acd10aa47a7154983-min.en-US.js"},"squarespace-popup-overlay":{"css":"//assets.squarespace.com/universal/styles-compressed/popup-overlay-68d60e7bd84500af34df575998cc00d0-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/popup-overlay-0149a748bc121034a13df-min.en-US.js"},"squarespace-product-quick-view":{"css":"//assets.squarespace.com/universal/styles-compressed/product-quick-view-eedd090fe95cd960919fcf1e0a4293c3-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/product-quick-view-ce2b51182ccd8ac6accc6-min.en-US.js"},"squarespace-products-collection-item-v2":{"css":"//assets.squarespace.com/universal/styles-compressed/products-collection-item-v2-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/products-collection-item-v2-5426bac76cb8bd5a92e8b-min.en-US.js"},"squarespace-products-collection-list-v2":{"css":"//assets.squarespace.com/universal/styles-compressed/products-collection-list-v2-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/products-collection-list-v2-0f57d23347251b2736f90-min.en-US.js"},"squarespace-search-page":{"css":"//assets.squarespace.com/universal/styles-compressed/search-page-207da8872118254c0a795bf9b187c205-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/search-page-ac207ca39a69cb84f84f0-min.en-US.js"},"squarespace-search-preview":{"js":"//assets.squarespace.com/universal/scripts-compressed/search-preview-638ba2bf8ec524b820947-min.en-US.js"},"squarespace-share-buttons":{"js":"//assets.squarespace.com/universal/scripts-compressed/share-buttons-32cc08f2dd7137611cfc4-min.en-US.js"},"squarespace-simple-liking":{"css":"//assets.squarespace.com/universal/styles-compressed/simple-liking-9ef41bf7ba753d65ec1acf18e093b88a-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/simple-liking-146a4d691830282d9ce5a-min.en-US.js"},"squarespace-social-buttons":{"css":"//assets.squarespace.com/universal/styles-compressed/social-buttons-bf7788a87c794b73afd9d5c49f72f4f3-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/social-buttons-aa06a0ebdaa97ed82d7ae-min.en-US.js"},"squarespace-tourdates":{"css":"//assets.squarespace.com/universal/styles-compressed/tourdates-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/tourdates-ee4869629f6a684b35f94-min.en-US.js"},"squarespace-website-overlays-manager":{"css":"//assets.squarespace.com/universal/styles-compressed/website-overlays-manager-4f212ab97f9bc590002bb2ff55f69409-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/website-overlays-manager-9e5a6a309dfd7e877bf6f-min.en-US.js"}},"pageType":100,"website":{"id":"51e5921ee4b0c6df9a7cb5df","identifier":"philippe-alexandre","websiteType":1,"contentModifiedOn":1610644033341,"cloneable":false,"hasBeenCloneable":false,"siteStatus":{},"language":"en-US","timeZone":"America/New_York","machineTimeZoneOffset":-18000000,"timeZoneOffset":-18000000,"timeZoneAbbr":"EST","siteTitle":"Alexandre","fullSiteTitle":"Alexandre","siteDescription":"<p>ALEXANDRE GALLERY represents and exhibits contemporary American artists and specializes in works by early 20th century American artists, with a focus on the Stieglitz Group. Publisher of contemporary prints and photographs. Alexandre Fine Art, Inc. established 1996.</p><br style=\"margin: 0px; padding: 0px; color: rgb(132, 128, 112); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 11px; letter-spacing: normal; line-height: normal; text-align: left; \">","location":{"mapZoom":12.0,"mapLat":40.762682,"mapLng":-73.97459099999998,"markerLat":40.762682,"markerLng":-73.97459099999998,"addressTitle":"Alexandre Gallery","addressLine1":"724 5th Avenue, 4th Floor","addressLine2":"New York, NY, 10019","addressCountry":"United States"},"logoImageId":"5fc27e7b5147b1480483ab6a","socialLogoImageId":"5fc27f49f3de5e49b5b53fe0","shareButtonOptions":{"2":true,"3":true,"8":true,"1":true},"logoImageUrl":"//static1.squarespace.com/static/51e5921ee4b0c6df9a7cb5df/t/5fc27e7b5147b1480483ab6a/1610644033341/","socialLogoImageUrl":"//static1.squarespace.com/static/51e5921ee4b0c6df9a7cb5df/t/5fc27f49f3de5e49b5b53fe0/1610644033341/","authenticUrl":"https://www.alexandregallery.com","internalUrl":"https://philippe-alexandre.squarespace.com","baseUrl":"https://www.alexandregallery.com","primaryDomain":"www.alexandregallery.com","sslSetting":3,"isHstsEnabled":false,"socialAccounts":[{"serviceId":20,"userId":"inquiries@alexandregallery.com","screenname":"inquiries@alexandregallery.com","addedOn":1374608553618,"profileUrl":"mailto:inquiries@alexandregallery.com","iconEnabled":true,"serviceName":"email"},{"serviceId":2,"userId":"1409679526","userName":"paalx","screenname":"Alexandre Gallery","addedOn":1378498110887,"profileUrl":"https://www.facebook.com/pages/Alexandre-Gallery/252275055072","iconUrl":"http://graph.facebook.com/1409679526/picture?type=square","metaData":{"service":"facebook"},"iconEnabled":true,"serviceName":"facebook"},{"serviceId":10,"userId":"468106644","userName":"alexandregallery","screenname":"Alexandre Gallery","addedOn":1525382568057,"profileUrl":"http://instagram.com/alexandregallery","iconUrl":"https://scontent.cdninstagram.com/vp/ee6d3c2704c0cd48a1b916e4ce37d4b8/5B8E0049/t51.2885-19/s150x150/15801943_246172235805816_6444387020767756288_a.jpg","collectionId":"5aeb7da87e3c3a2bedb603c9","iconEnabled":true,"serviceName":"instagram"}],"typekitId":"","statsMigrated":true,"imageMetadataProcessingEnabled":true,"screenshotId":"68d61610c0e1e5d2300363461963b7368676b6b66529326d1431497fdcfbb0f0","captchaSettings":{"enabledForDonations":false},"showOwnerLogin":false},"websiteSettings":{"id":"51e5921ee4b0c6df9a7cb5e0","websiteId":"51e5921ee4b0c6df9a7cb5df","type":"Business","subjects":[{"systemSubject":"other","otherSubject":"Painting and Sculpture"},{"systemSubject":"fine_art"}],"country":"US","state":"NY","simpleLikingEnabled":true,"mobileInfoBarSettings":{"style":2,"isContactEmailEnabled":true,"isContactPhoneNumberEnabled":true,"isLocationEnabled":true,"isBusinessHoursEnabled":true},"announcementBarSettings":{"style":2,"text":"<p class=\"\" style=\"white-space:pre-wrap;\">The gallery is open by advanced appointment Wednesdays through Fridays from 10:00 to 5:00 and Saturdays from 11:00 to 5:00. Please contact us at inquiries@alexandregallery.com or at 212-755-2828 to schedule a visit to our new location at 25 East 73rd Street. The gallery continues to follow New York State safety guidelines for Covid-19.</p>","clickthroughUrl":{"url":"/contact","newWindow":false}},"commentLikesAllowed":false,"commentAnonAllowed":false,"commentThreaded":false,"commentApprovalRequired":false,"commentAvatarsOn":false,"commentSortType":2,"commentFlagThreshold":0,"commentFlagsAllowed":false,"commentEnableByDefault":false,"commentDisableAfterDaysDefault":0,"disqusShortname":"","commentsEnabled":false,"contactPhoneNumber":"212-755-2828","businessHours":{"monday":{"text":"","ranges":[{}]},"tuesday":{"text":"10:00 to 5:00","ranges":[{"from":600,"to":300}]},"wednesday":{"text":"10:00 to 5:00","ranges":[{"from":600,"to":300}]},"thursday":{"text":"10:00 to 5:00","ranges":[{"from":600,"to":300}]},"friday":{"text":"10:00 to 5:00","ranges":[{"from":600,"to":300}]},"saturday":{"text":"11:00 to 5:00","ranges":[{"from":660,"to":300}]},"sunday":{"text":"","ranges":[{}]}},"contactEmail":"inquiries@alexandregallery.com","storeSettings":{"returnPolicy":"","termsOfService":"","privacyPolicy":"","orderRefundedSubjectFormat":"%s: Order #%o Refunded","orderRefundedTitleFormat":"Order Refunded: #%o","expressCheckout":false,"continueShoppingLinkUrl":"/","useLightCart":false,"showNoteField":false,"shippingCountryDefaultValue":"US","billToShippingDefaultValue":false,"showShippingPhoneNumber":true,"isShippingPhoneRequired":false,"showBillingPhoneNumber":false,"isBillingPhoneRequired":false,"currenciesSupported":["USD","AUD","CAD","CHF","CZK","DKK","EUR","GBP","HKD","ILS","MXN","MYR","NOK","NZD","PHP","PLN","RUB","SEK","SGD","THB"],"defaultCurrency":"USD","selectedCurrency":"USD","measurementStandard":1,"showCustomCheckoutForm":false,"enableMailingListOptInByDefault":true,"contactLocation":{"mapLat":40.762682,"mapLng":-73.97459099999998,"markerLat":40.762682,"markerLng":-73.97459099999998,"addressLine1":"724 5th Avenue, 4th Floor","addressLine2":"New York, NY, 10019","addressCountry":"United States"},"businessName":"Alexandre Fine Art, Inc","sameAsRetailLocation":false,"merchandisingSettings":{"scarcityEnabledOnProductItems":false,"scarcityEnabledOnProductBlocks":false,"scarcityMessageType":"DEFAULT_SCARCITY_MESSAGE","scarcityThreshold":10,"multipleQuantityAllowedForServices":true,"restockNotificationsEnabled":false,"restockNotificationsMailingListSignUpEnabled":false,"relatedProductsEnabled":false,"relatedProductsOrdering":"random","soldOutVariantsDropdownDisabled":false,"productComposerOptedIn":false,"productComposerABTestOptedOut":false},"isLive":true,"multipleQuantityAllowedForServices":true},"useEscapeKeyToLogin":true,"trialAssistantEnabled":true,"ssBadgeType":1,"ssBadgePosition":4,"ssBadgeVisibility":1,"ssBadgeDevices":1,"pinterestOverlayOptions":{"mode":"disabled","size":"small","shape":"rect","color":"white"},"ampEnabled":false},"cookieSettings":{"isCookieBannerEnabled":false,"isRestrictiveCookiePolicyEnabled":false,"isRestrictiveCookiePolicyAbsolute":false,"cookieBannerText":"","cookieBannerTheme":"","cookieBannerVariant":"","cookieBannerPosition":"","cookieBannerCtaVariant":"","cookieBannerCtaText":""},"websiteCloneable":false,"subscribed":false,"appDomain":"squarespace.com","templateTweakable":true,"tweakJSON":{"aspect-ratio":"Auto","contentBgColor":"rgba(255,255,255,1)","first-index-image-fullscreen":"true","fixed-header":"false","footerBgColor":"rgba(23,23,23,1)","gallery-arrow-style":"No Background","gallery-auto-crop":"true","gallery-autoplay":"false","gallery-design":"Slideshow","gallery-info-overlay":"Always Show","gallery-loop":"false","gallery-navigation":"Bullets","gallery-show-arrows":"true","galleryArrowBackground":"rgba(34,34,34,1)","galleryArrowColor":"rgba(99,99,99,1)","galleryAutoplaySpeed":"3","galleryCircleColor":"rgba(255,255,255,1)","galleryInfoBackground":"rgba(38,38,38,0.7)","galleryThumbnailSize":"100px","gridSize":"350px","gridSpacing":"20px","headerBgColor":"rgba(255,255,255,1)","imageOverlayColor":"rgba(0,0,0,0.4)","index-image-height":"Two Thirds","parallax-scrolling":"true","product-gallery-auto-crop":"false","product-image-auto-crop":"false","title--description-alignment":"Left","title--description-position":"Over Image","titleBgColor":"rgba(227,227,227,1)","transparent-header":"false","tweak-v1-related-products-title-spacing":"50px"},"templateId":"515c7bd0e4b054dae3fcf003","templateVersion":"7","pageFeatures":[1,4],"gmRenderKey":"QUl6YVN5Q0JUUk9xNkx1dkZfSUUxcjQ2LVQ0QWVUU1YtMGQ3bXk4","templateScriptsRootUrl":"https://static1.squarespace.com/static/ta/515c7b5ae4b0875140c3d94a/2779/scripts/","betaFeatureFlags":["commerce_afterpay_toggle","page_interactions_improvements","nested_categories_migration_enabled","commerce_setup_wizard","campaigns_email_reuse_template_flow","gallery_captions_71","list_sent_to_groups","domain_deletion_via_registrar_service","domain_info_via_registrar_service","dg_downloads_from_fastly","domains_universal_search","commerce_reduce_cart_calculations","seven-one-main-content-preview-api","generic_iframe_loader_for_campaigns","commerce_afterpay","seven-one-content-preview-section-api","ORDERS-SERVICE-reset-digital-goods-access-with-service","commerce_pdp_edit_mode","crm_campaigns_sending","campaigns_new_sender_profile_page","commerce_activation_experiment_add_payment_processor_card","ORDER_SERVICE-submit-subscription-order-through-service","seven_one_header_editor_update","omit_tweakengine_tweakvalues","domains_use_new_domain_connect_strategy","domains_transfer_flow_improvements","commerce-recaptcha-enterprise","commerce_instagram_product_checkout_links","seven_one_frontend_render_gallery_section","commerce_tax_panel_v2","commerce_restock_notifications","domain_locking_via_registrar_service","campaigns_single_opt_in","local_listings","commerce_minimum_order_amount","ORDERS-SERVICE-check-digital-good-access-with-service","seven_one_image_effects","donations_customer_accounts","events_panel_70","customer_notifications_panel_v2","seven_one_frontend_render_page_section","commerce_subscription_order_delay","domains_transfer_flow_hide_preface","commerce_category_id_discounts_enabled","commerce_add_to_cart_rate_limiting","domains_allow_async_gsuite","seven_one_portfolio_hover_layouts","seven-one-menu-overlay-theme-switcher","animations_august_2020_new_preset","member_areas_ga","ORDER_SERVICE-submit-reoccurring-subscription-order-through-service","campaigns_user_templates_in_sidebar","newsletter_block_captcha","seven_one_image_overlay_opacity","commerce_orders_elasticsearch_migration"],"yuiEliminationExperimentList":[{"name":"statsMigrationJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"ContributionConfirmed-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"TextPusher-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"MenuItemWithProgress-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"imageProcJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"QuantityChangePreview-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"CompositeModel-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"HasPusherMixin-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"INACTIVE"},{"name":"ProviderList-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"MediaTracker-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"pushJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"internal-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"BillingPanel-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"PopupOverlayEditor-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"CoverPagePicker-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"}],"impersonatedSession":false,"tzData":{"zones":[[-300,"US","E%sT",null]],"rules":{"US":[[1967,2006,null,"Oct","lastSun","2:00","0","S"],[1987,2006,null,"Apr","Sun>=1","2:00","1:00","D"],[2007,"max",null,"Mar","Sun>=8","2:00","1:00","D"],[2007,"max",null,"Nov","Sun>=1","2:00","0","S"]]}}};</script><script type="text/javascript"> SquarespaceFonts.loadViaContext(); Squarespace.load(window);</script><script type="application/ld+json">{"url":"https://www.alexandregallery.com","name":"Alexandre","description":"<p>ALEXANDRE GALLERY represents and exhibits contemporary American artists and specializes in works by early 20th century American artists, with a focus on the Stieglitz Group. Publisher of contemporary prints and photographs. Alexandre Fine Art, Inc. established 1996.</p><br style=\"margin: 0px; padding: 0px; color: rgb(132, 128, 112); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 11px; letter-spacing: normal; line-height: normal; text-align: left; \">","image":"//static1.squarespace.com/static/51e5921ee4b0c6df9a7cb5df/t/5fc27e7b5147b1480483ab6a/1610644033341/","@context":"http://schema.org","@type":"WebSite"}</script><script type="application/ld+json">{"legalName":"Alexandre Fine Art, Inc","address":"724 5th Avenue, 4th Floor\nNew York, NY, 10019\nUnited States","email":"inquiries@alexandregallery.com","telephone":"212-755-2828","sameAs":["mailto:inquiries@alexandregallery.com","https://www.facebook.com/pages/Alexandre-Gallery/252275055072","http://instagram.com/alexandregallery"],"@context":"http://schema.org","@type":"Organization"}</script><script type="application/ld+json">{"address":"724 5th Avenue, 4th Floor\nNew York, NY, 10019\nUnited States","image":"https://static1.squarespace.com/static/51e5921ee4b0c6df9a7cb5df/t/5fc27e7b5147b1480483ab6a/1610644033341/","name":"Alexandre Fine Art, Inc","openingHours":", Tu 10:00-05:00, We 10:00-05:00, Th 10:00-05:00, Fr 10:00-05:00, Sa 11:00-05:00, ","@context":"http://schema.org","@type":"LocalBusiness"}</script><link href="//static1.squarespace.com/static/sitecss/51e5921ee4b0c6df9a7cb5df/114/515c7bd0e4b054dae3fcf003/51e6c85ee4b0ce4430c7b72d/2779-05142015/1610634387254/site.css?&amp;filterFeatures=false" rel="stylesheet" type="text/css"/><script>Static.COOKIE_BANNER_CAPABLE = true;</script>
<!-- End of Squarespace Headers -->
<script>
      Y.Squarespace.FollowButtonUtils = {};
      Y.Squarespace.FollowButtonUtils.renderAll = function(){};
    </script>
</head>
<body class="event-excerpts event-list-date event-list-time event-list-address event-icalgcal-links events-layout-columns events-width-full gallery-design-slideshow aspect-ratio-auto lightbox-style-dark gallery-navigation-bullets gallery-info-overlay-always-show gallery-arrow-style-no-background gallery-show-arrows gallery-auto-crop parallax-scrolling first-index-image-fullscreen index-image-height-two-thirds title--description-alignment-left title--description-position-over-image title-background description-button-style-solid description-button-corner-style-square underline-body-links blog-layout-columns blog-width-full product-list-titles-under product-list-alignment-center product-item-size-23-standard-vertical product-gallery-size-11-square show-product-item-nav product-social-sharing tweak-v1-related-products-image-aspect-ratio-11-square tweak-v1-related-products-details-alignment-center newsletter-style-dark opentable-style-light small-button-style-solid small-button-shape-square medium-button-style-solid medium-button-shape-square large-button-style-solid large-button-shape-square image-block-poster-text-alignment-center image-block-card-dynamic-font-sizing image-block-card-content-position-center image-block-card-text-alignment-left image-block-overlap-dynamic-font-sizing image-block-overlap-content-position-center image-block-overlap-text-alignment-left image-block-collage-dynamic-font-sizing image-block-collage-content-position-top image-block-collage-text-alignment-left image-block-stack-dynamic-font-sizing image-block-stack-text-alignment-left button-style-default button-corner-style-square tweak-product-quick-view-button-style-floating tweak-product-quick-view-button-position-bottom tweak-product-quick-view-lightbox-excerpt-display-truncate tweak-product-quick-view-lightbox-show-arrows tweak-product-quick-view-lightbox-show-close-button tweak-product-quick-view-lightbox-controls-weight-light native-currency-code-usd not-found-page mobile-style-available " id="not-found">
<div data-content-field="navigation-mobileNav" id="mobileNav">
<nav class="main-nav">
<div class="nav-wrapper">
<ul class="cf">
<li class="gallery-collection">
<a href="/"><span>Home</span></a>
</li>
<li class="page-collection">
<a href="/virtual-viewing-room"><span>Virtual Viewing Room</span></a>
</li>
<li class="page-collection">
<a href="/current-exhibition-"><span>Current Exhibition</span></a>
</li>
<li class="page-collection">
<a href="/upcoming-exhibitions"><span>Upcoming Exhibitions</span></a>
</li>
<li class="page-collection">
<a href="/notable-sales"><span>Notable 20th Century Sales</span></a>
</li>
<li class="products-collection">
<a href="/catalogues"><span>Catalogues</span></a>
</li>
<li class="page-collection">
<a href="/contact"><span>Contact</span></a>
</li>
<li class="blog-collection">
<a href="/news"><span>News</span></a>
</li>
<li class="page-collection">
<a href="/past-exhibitions-index"><span>Past Exhibitions</span></a>
</li>
<li class="page-collection">
<a href="/artists-and-work-index"><span>Artists and Work</span></a>
</li>
<li class="folders-collection folder">
<!--FOLDER-->
<div class="folder-parent">
<a aria-haspopup="true"><span>Prints</span></a>
<div class="folder-child-wrapper">
<ul class="folder-child">
<li class="">
<a href="/will-barnet-prints">
<span>Will Barnet</span>
</a>
</li>
<li class="">
<a href="/brett-bigbee-prints">
<span>Brett Bigbee</span>
</a>
</li>
<li class="">
<a href="/lois-dodd-prints">
<span>Lois Dodd</span>
</a>
</li>
<li class="">
<a href="/neil-welliver-prints">
<span>Neil Welliver</span>
</a>
</li>
</ul>
</div>
</div>
</li>
</ul>
</div>
</nav>
</div>
<div class="site-wrapper">
<div class="site-inner-wrapper">
<header class="sqs-announcement-bar-dropzone" id="header">
<div class="sqs-cart-dropzone"></div>
<div class="title-nav-wrapper">
<h1 class="site-title" data-content-field="site-title">
<a href="/" id="top">
<img alt="Alexandre" src="//static1.squarespace.com/static/51e5921ee4b0c6df9a7cb5df/t/5fc27e7b5147b1480483ab6a/1610644033341/?format=1500w"/>
</a>
</h1>
<a class="icon-menu" id="mobileMenu"></a>
<!--MAIN NAVIGATION-->
<div data-annotation-alignment="bottom left" data-content-field="navigation-mainNav" id="desktopNav">
<nav class="main-nav">
<div class="nav-wrapper">
<ul class="cf">
<li class="gallery-collection">
<a href="/"><span>Home</span></a>
</li>
<li class="page-collection">
<a href="/virtual-viewing-room"><span>Virtual Viewing Room</span></a>
</li>
<li class="page-collection">
<a href="/current-exhibition-"><span>Current Exhibition</span></a>
</li>
<li class="page-collection">
<a href="/upcoming-exhibitions"><span>Upcoming Exhibitions</span></a>
</li>
<li class="page-collection">
<a href="/notable-sales"><span>Notable 20th Century Sales</span></a>
</li>
<li class="products-collection">
<a href="/catalogues"><span>Catalogues</span></a>
</li>
<li class="page-collection">
<a href="/contact"><span>Contact</span></a>
</li>
<li class="blog-collection">
<a href="/news"><span>News</span></a>
</li>
<li class="page-collection">
<a href="/past-exhibitions-index"><span>Past Exhibitions</span></a>
</li>
<li class="page-collection">
<a href="/artists-and-work-index"><span>Artists and Work</span></a>
</li>
<li class="folders-collection folder">
<!--FOLDER-->
<div class="folder-parent">
<a aria-haspopup="true"><span>Prints</span></a>
<div class="folder-child-wrapper">
<ul class="folder-child">
<li class="">
<a href="/will-barnet-prints">
<span>Will Barnet</span>
</a>
</li>
<li class="">
<a href="/brett-bigbee-prints">
<span>Brett Bigbee</span>
</a>
</li>
<li class="">
<a href="/lois-dodd-prints">
<span>Lois Dodd</span>
</a>
</li>
<li class="">
<a href="/neil-welliver-prints">
<span>Neil Welliver</span>
</a>
</li>
</ul>
</div>
</div>
</li>
</ul>
</div>
</nav>
</div>
</div>
</header>
<section id="content-wrapper">
<div class="content">
<div class="content-inner" data-content-field="main-content">
<p>We couldn't find the page you were looking for. This is either because:</p>
<ul>
<li>There is an error in the URL entered into your web browser. Please check the URL and try again.</li>
<li>The page you are looking for has been moved or deleted.</li>
</ul>
<p>
  You can return to our homepage by <a href="/">clicking here</a>, or you can try searching for the
  content you are seeking by <a href="/search">clicking here</a>.
</p>
</div>
</div>
</section>
<!--FOOTER WITH OPEN BLOCK FIELD-->
<footer id="footer">
<div class="back-to-top-link"><a href="#top"><span class="arrow"></span>Top</a></div>
<div class="footer-wrapper">
<div data-content-field="navigation-secondaryNav" id="secondaryNav">
</div>
<div class="sqs-layout sqs-grid-12 columns-12 empty" data-layout-label="Footer Content" data-type="block-field" id="footerBlocks"><div class="row sqs-row"><div class="col sqs-col-12 span-12"></div></div></div>
</div>
</footer>
</div>
</div>
<script type="text/javascript">
var _userway_config = {
// uncomment the following line to override default position
// position: '3',
// uncomment the following line to override default size (values: small, large)
// size: 'small', 
// uncomment the following line to override default language (e.g., fr, de, es, he, nl, etc.)
// language: 'en-US',
// uncomment the following line to override color set via widget
// color: 'null', 
// uncomment the following line to override type set via widget(1=man, 2=chair, 3=eye)
// type: 'null', 
account: 'EF1YhTTIAs'
};
</script>
<script src="https://cdn.userway.org/widget.js" type="text/javascript"></script><script data-sqs-type="imageloader-bootstrapper" type="text/javascript">(function() {if(window.ImageLoader) { window.ImageLoader.bootstrap({}, document); }})();</script><script>Squarespace.afterBodyLoad(Y);</script><svg data-usage="social-icons-svg" style="display:none" version="1.1" xmlns="http://www.w3.org/2000/svg"><symbol id="email-icon" viewbox="0 0 64 64"><path d="M17,22v20h30V22H17z M41.1,25L32,32.1L22.9,25H41.1z M20,39V26.6l12,9.3l12-9.3V39H20z"></path></symbol><symbol id="email-mask" viewbox="0 0 64 64"><path d="M41.1,25H22.9l9.1,7.1L41.1,25z M44,26.6l-12,9.3l-12-9.3V39h24V26.6z M0,0v64h64V0H0z M47,42H17V22h30V42z"></path></symbol><symbol id="facebook-icon" viewbox="0 0 64 64"><path d="M34.1,47V33.3h4.6l0.7-5.3h-5.3v-3.4c0-1.5,0.4-2.6,2.6-2.6l2.8,0v-4.8c-0.5-0.1-2.2-0.2-4.1-0.2 c-4.1,0-6.9,2.5-6.9,7V28H24v5.3h4.6V47H34.1z"></path></symbol><symbol id="facebook-mask" viewbox="0 0 64 64"><path d="M0,0v64h64V0H0z M39.6,22l-2.8,0c-2.2,0-2.6,1.1-2.6,2.6V28h5.3l-0.7,5.3h-4.6V47h-5.5V33.3H24V28h4.6V24 c0-4.6,2.8-7,6.9-7c2,0,3.6,0.1,4.1,0.2V22z"></path></symbol><symbol id="instagram-icon" viewbox="0 0 64 64"><path d="M46.91,25.816c-0.073-1.597-0.326-2.687-0.697-3.641c-0.383-0.986-0.896-1.823-1.73-2.657c-0.834-0.834-1.67-1.347-2.657-1.73c-0.954-0.371-2.045-0.624-3.641-0.697C36.585,17.017,36.074,17,32,17s-4.585,0.017-6.184,0.09c-1.597,0.073-2.687,0.326-3.641,0.697c-0.986,0.383-1.823,0.896-2.657,1.73c-0.834,0.834-1.347,1.67-1.73,2.657c-0.371,0.954-0.624,2.045-0.697,3.641C17.017,27.415,17,27.926,17,32c0,4.074,0.017,4.585,0.09,6.184c0.073,1.597,0.326,2.687,0.697,3.641c0.383,0.986,0.896,1.823,1.73,2.657c0.834,0.834,1.67,1.347,2.657,1.73c0.954,0.371,2.045,0.624,3.641,0.697C27.415,46.983,27.926,47,32,47s4.585-0.017,6.184-0.09c1.597-0.073,2.687-0.326,3.641-0.697c0.986-0.383,1.823-0.896,2.657-1.73c0.834-0.834,1.347-1.67,1.73-2.657c0.371-0.954,0.624-2.045,0.697-3.641C46.983,36.585,47,36.074,47,32S46.983,27.415,46.91,25.816z M44.21,38.061c-0.067,1.462-0.311,2.257-0.516,2.785c-0.272,0.7-0.597,1.2-1.122,1.725c-0.525,0.525-1.025,0.85-1.725,1.122c-0.529,0.205-1.323,0.45-2.785,0.516c-1.581,0.072-2.056,0.087-6.061,0.087s-4.48-0.015-6.061-0.087c-1.462-0.067-2.257-0.311-2.785-0.516c-0.7-0.272-1.2-0.597-1.725-1.122c-0.525-0.525-0.85-1.025-1.122-1.725c-0.205-0.529-0.45-1.323-0.516-2.785c-0.072-1.582-0.087-2.056-0.087-6.061s0.015-4.48,0.087-6.061c0.067-1.462,0.311-2.257,0.516-2.785c0.272-0.7,0.597-1.2,1.122-1.725c0.525-0.525,1.025-0.85,1.725-1.122c0.529-0.205,1.323-0.45,2.785-0.516c1.582-0.072,2.056-0.087,6.061-0.087s4.48,0.015,6.061,0.087c1.462,0.067,2.257,0.311,2.785,0.516c0.7,0.272,1.2,0.597,1.725,1.122c0.525,0.525,0.85,1.025,1.122,1.725c0.205,0.529,0.45,1.323,0.516,2.785c0.072,1.582,0.087,2.056,0.087,6.061S44.282,36.48,44.21,38.061z M32,24.297c-4.254,0-7.703,3.449-7.703,7.703c0,4.254,3.449,7.703,7.703,7.703c4.254,0,7.703-3.449,7.703-7.703C39.703,27.746,36.254,24.297,32,24.297z M32,37c-2.761,0-5-2.239-5-5c0-2.761,2.239-5,5-5s5,2.239,5,5C37,34.761,34.761,37,32,37z M40.007,22.193c-0.994,0-1.8,0.806-1.8,1.8c0,0.994,0.806,1.8,1.8,1.8c0.994,0,1.8-0.806,1.8-1.8C41.807,22.999,41.001,22.193,40.007,22.193z"></path></symbol><symbol id="instagram-mask" viewbox="0 0 64 64"><path d="M43.693,23.153c-0.272-0.7-0.597-1.2-1.122-1.725c-0.525-0.525-1.025-0.85-1.725-1.122c-0.529-0.205-1.323-0.45-2.785-0.517c-1.582-0.072-2.056-0.087-6.061-0.087s-4.48,0.015-6.061,0.087c-1.462,0.067-2.257,0.311-2.785,0.517c-0.7,0.272-1.2,0.597-1.725,1.122c-0.525,0.525-0.85,1.025-1.122,1.725c-0.205,0.529-0.45,1.323-0.516,2.785c-0.072,1.582-0.087,2.056-0.087,6.061s0.015,4.48,0.087,6.061c0.067,1.462,0.311,2.257,0.516,2.785c0.272,0.7,0.597,1.2,1.122,1.725s1.025,0.85,1.725,1.122c0.529,0.205,1.323,0.45,2.785,0.516c1.581,0.072,2.056,0.087,6.061,0.087s4.48-0.015,6.061-0.087c1.462-0.067,2.257-0.311,2.785-0.516c0.7-0.272,1.2-0.597,1.725-1.122s0.85-1.025,1.122-1.725c0.205-0.529,0.45-1.323,0.516-2.785c0.072-1.582,0.087-2.056,0.087-6.061s-0.015-4.48-0.087-6.061C44.143,24.476,43.899,23.682,43.693,23.153z M32,39.703c-4.254,0-7.703-3.449-7.703-7.703s3.449-7.703,7.703-7.703s7.703,3.449,7.703,7.703S36.254,39.703,32,39.703z M40.007,25.793c-0.994,0-1.8-0.806-1.8-1.8c0-0.994,0.806-1.8,1.8-1.8c0.994,0,1.8,0.806,1.8,1.8C41.807,24.987,41.001,25.793,40.007,25.793z M0,0v64h64V0H0z M46.91,38.184c-0.073,1.597-0.326,2.687-0.697,3.641c-0.383,0.986-0.896,1.823-1.73,2.657c-0.834,0.834-1.67,1.347-2.657,1.73c-0.954,0.371-2.044,0.624-3.641,0.697C36.585,46.983,36.074,47,32,47s-4.585-0.017-6.184-0.09c-1.597-0.073-2.687-0.326-3.641-0.697c-0.986-0.383-1.823-0.896-2.657-1.73c-0.834-0.834-1.347-1.67-1.73-2.657c-0.371-0.954-0.624-2.044-0.697-3.641C17.017,36.585,17,36.074,17,32c0-4.074,0.017-4.585,0.09-6.185c0.073-1.597,0.326-2.687,0.697-3.641c0.383-0.986,0.896-1.823,1.73-2.657c0.834-0.834,1.67-1.347,2.657-1.73c0.954-0.371,2.045-0.624,3.641-0.697C27.415,17.017,27.926,17,32,17s4.585,0.017,6.184,0.09c1.597,0.073,2.687,0.326,3.641,0.697c0.986,0.383,1.823,0.896,2.657,1.73c0.834,0.834,1.347,1.67,1.73,2.657c0.371,0.954,0.624,2.044,0.697,3.641C46.983,27.415,47,27.926,47,32C47,36.074,46.983,36.585,46.91,38.184z M32,27c-2.761,0-5,2.239-5,5s2.239,5,5,5s5-2.239,5-5S34.761,27,32,27z"></path></symbol></svg>
<script src="https://static1.squarespace.com/static/ta/515c7b5ae4b0875140c3d94a/2779/scripts/site-bundle.js" type="text/javascript"></script>
</body>
</html>
