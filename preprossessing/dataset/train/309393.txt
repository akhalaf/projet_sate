<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-US">
<![endif]--><!--[if IE 8]>
<html class="ie ie8" lang="en-US">
<![endif]--><!--[if !(IE 7) | !(IE 8)  ]><!--><html lang="en-US">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<title>Charith Cosmetic (Pvt) Ltd</title> <meta content="width=device-width, user-scalable=no, maximum-scale=1, initial-scale=1, minimum-scale=1" name="viewport"/>
<meta content="Official site" name="description"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="Charith Cosmetic - Official site" property="og:site_name"/>
<meta content="Charith Cosmetic (Pvt) Ltd" property="og:title"/>
<meta content="Maintenance" property="og:type"/>
<meta content="https://www.charithcosmetic.com" property="og:url"/>
<meta content="Website+will+be+available+soon" property="og:description"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.charithcosmetic.com/xmlrpc.php" rel="pingback"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800%2C300italic%2C400italic%2C600italic%2C700italic%2C800italic&amp;subset=devanagari&amp;ver=4.7.19" id="_custom_fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?ver=4.7.19" id="_iconstyle_fa-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdn.jsdelivr.net/foundation-icons/3.0/foundation-icons.min.css?ver=4.7.19" id="_iconstyle_fi-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.charithcosmetic.com/wp-content/plugins/maintenance/load/style.css?ver=4.7.19" id="_style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="_style-inline-css" type="text/css">
body {background-color: #e8e8e8}.preloader {background-color: #e8e8e8}body {font-family: Open Sans; }.site-title, .preloader i, .login-form, .login-form a.lost-pass, .btn-open-login-form, .site-content, .user-content-wrapper, .user-content, footer, .maintenance a {color: #47b215;} .ie7 .login-form input[type="text"], .ie7 .login-form input[type="password"], .ie7 .login-form input[type="submit"]  {color: #47b215} a.close-user-content, #mailchimp-box form input[type="submit"], .login-form input#submit.button  {border-color:#47b215} .ie7 .company-name {color: #47b215} 
</style>
<script src="https://www.charithcosmetic.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.charithcosmetic.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.charithcosmetic.com/wp-content/plugins/maintenance/load/js/jquery.placeholder.js?ver=4.7.19" type="text/javascript"></script>
<script src="https://www.charithcosmetic.com/wp-content/plugins/maintenance/load/js/jquery.backstretch.min.js?ver=4.7.19" type="text/javascript"></script>
<script src="https://www.charithcosmetic.com/wp-content/plugins/maintenance/load/js/jquery.blur.min.js?ver=4.7.19" type="text/javascript"></script>
<script src="https://www.charithcosmetic.com/wp-content/plugins/maintenance/load/js/jquery.frontend.min.js?ver=4.7.19" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function() { 
if (jQuery(window).height() < 768) {
jQuery("body").backstretch("https://www.charithcosmetic.com/wp-content/uploads/2017/03/mt-sample-background.jpg");
}	else {
jQuery(".main-container").backstretch("https://www.charithcosmetic.com/wp-content/uploads/2017/03/mt-sample-background.jpg");
}
});</script> <script type="text/javascript">
			(function(i,s,o,g,r,a,m){
				i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ 
			   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];
				a.async=1;
				a.src=g;
				m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', '', 'auto');
			ga('send', 'pageview');

		</script>
</head>
<body class="home blog maintenance hfeed has-header-image has-sidebar colors-light">
<div class="main-container">
<div class="preloader"><i aria-hidden="true" class="fi-widget"></i></div> <div id="wrapper">
<div class="center logotype">
<header>
<div class="logo-box istext" rel="home"><h1 class="site-title">Charith Cosmetic</h1></div><div class="logo-box-retina istext" rel="home"><h1 class="site-title">Charith Cosmetic</h1></div> </header>
</div>
<div class="site-content" id="content">
<div class="center">
<div class="description"><p>Website will be available soon</p>
</div> </div>
</div>
</div> <!-- end wrapper -->
<footer role="contentinfo">
<div class="center">
				© Charith Cosmetic 2017			</div>
</footer>
</div>
</body>
</html>