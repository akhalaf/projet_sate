<!DOCTYPE html>
<html lang="de-DE" prefix="og: http://ogp.me/ns#">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="https://rj-capital.de/wp-content/uploads/2016/12/RJ_black_fav.png" rel="shortcut icon"/>
<!-- This site is optimized with the Yoast SEO plugin v5.7.1 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Page not found | RJ Capital Group</title>
<meta content="noindex,follow" name="robots"/>
<meta content="de_DE" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Page not found | RJ Capital Group" property="og:title"/>
<meta content="RJ Capital Group" property="og:site_name"/>
<meta content="summary" name="twitter:card"/>
<meta content="Page not found | RJ Capital Group" name="twitter:title"/>
<script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/rj-capital.de\/","name":"RJ Capital Group","potentialAction":{"@type":"SearchAction","target":"https:\/\/rj-capital.de\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"Organization","url":false,"sameAs":["https:\/\/www.facebook.com\/RJ-Capital-Group-235157696991839\/","https:\/\/www.linkedin.com\/company-beta\/18080667\/"],"@id":"#organization","name":"RJ Capital Goup","logo":"http:\/\/rj-capital.de\/wp-content\/uploads\/2017\/07\/Logo_vert_for_white.png"}</script>
<!-- / Yoast SEO plugin. -->
<link href="//rj-capital.de" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://rj-capital.de/feed/" rel="alternate" title="RJ Capital Group » Feed" type="application/rss+xml"/>
<link href="https://rj-capital.de/comments/feed/" rel="alternate" title="RJ Capital Group » Kommentar-Feed" type="application/rss+xml"/>
<meta content="RJ Capital Group" property="og:site_name"/><meta content="" property="og:url"/> <script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/rj-capital.de\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://rj-capital.de/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rj-capital.de/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rj-capital.de/wp-content/themes/newave-theme/css/bootstrap.min.css?ver=5.6" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rj-capital.de/wp-content/themes/newave-theme/style.css?ver=5.6" id="theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rj-capital.de/wp-content/themes/newave-theme/css/colors/color-gray.css?ver=5.6" id="colorskin-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rj-capital.de/wp-content/themes/newave-theme/css/flexslider.css?ver=5.6" id="newave_flexslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rj-capital.de/wp-content/themes/newave-theme/css/prettyPhoto.css?ver=5.6" id="prettyPhoto-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rj-capital.de/wp-content/themes/newave-theme/css/jquery.bxslider.css?ver=5.6" id="bxslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rj-capital.de/wp-content/themes/newave-theme/css/font-awesome.min.css?ver=5.6" id="fontawesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rj-capital.de/wp-content/themes/newave-theme/css/animsition.css?ver=5.6" id="newave_animsition-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rj-capital.de/wp-content/themes/newave-theme/css/sliders.css?ver=5.6" id="newave_sliders-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rj-capital.de/wp-content/themes/newave-theme/css/navigation-style-1.css?ver=5.6" id="navigation-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700&amp;ver=5.6" id="newave-open-sans-font-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat%3A400%2C700&amp;ver=5.6" id="newave-montserrat-font-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://rj-capital.de/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://rj-capital.de/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<link href="https://rj-capital.de/wp-json/" rel="https://api.w.org/"/><link href="https://rj-capital.de/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://rj-capital.de/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<!-- Markup (JSON-LD) structured in schema.org ver.4.7.0 START -->
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "LocalBusiness",
    "name": "RJ Capital Group",
    "image": "http://rj-capital.de/wp-content/uploads/2017/07/Logo_vert_for_white.png",
    "url": "http://rj-capital.de",
    "telephone": "+49-30-5679-4992",
    "address": {
        "@type": "PostalAddress",
        "streetAddress": "Platzl 1a",
        "addressLocality": "München",
        "postalCode": "80331",
        "addressCountry": "DE",
        "addressRegion": "Deutschland"
    }
}
</script>
<!-- Markup (JSON-LD) structured in schema.org END -->
<style type="text/css">.animsition-loading:after { content: url(https://clapat.ro/themes/newave-reloaded/wp-content/themes/newave-theme/images/newave-loading.gif) }html { background-color:#ffffff; }</style><style type="text/css">#home {

     height: 100%!important;


}

.slides li {
-webkit-transition: all 0s ease-in-out 0s!important;
-moz-transition: all 0s ease-in-out  0s!important;
-o-transition: all 0s ease-in-out  0s!important;
-ms-transition: all 0s ease-in-out  0s!important;
transition: all 0s ease-in-out 0s!important;
}</style><style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><meta content="Powered by Visual Composer - drag and drop page builder for WordPress." name="generator"/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://rj-capital.de/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>
<body class="error404 animsition small-border-title wpb-js-composer js-comp-ver-5.0 vc_responsive" data-offset="75" data-spy="scroll" data-target=".navbar">
<!-- Home Section -->
<div id="not-found">
<div class="home-pattern">
<div class="container clearfix">
<div class="element_fade_in" id="home-center">
<div class="div-align-center">
<h1 class="four-zero-four "><span class="text-color">404</span></h1>
<p class="below-four-zero-four">The page that you requested doesn't exist. You may want to return home and start again. Sorry :(</p>
<a class="newave-button medium outline white external" href="https://rj-capital.de">Back to home</a>
</div>
</div>
</div>
</div>
</div>
<!-- End Home Section -->
<!-- Footer -->
<footer class="footer-modern"> <div class="container no-padding">
<a id="back-top"><div id="menu_top"><div id="menu_top_inside"></div></div></a>
<p class="copyright">2016-2020 © RJ Capital Group. All rights reserved. | <a href="http://rj-capital.de/?page_id=599">Impressum</a> | <a href="http://rj-capital.de/?page_id=614">Disclaimer</a> | <a href="http://rj-capital.de/?page_id=618">Datenschutz</a></p>
</div>
</footer>
<!--/Footer -->
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/rj-capital.de\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://rj-capital.de/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="newave-like-js-extra" type="text/javascript">
/* <![CDATA[ */
var newaveLike = {"ajaxurl":"https:\/\/rj-capital.de\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script id="newave-like-js" src="https://rj-capital.de/wp-content/themes/newave-theme/components/post_like/js/newave-like.js?ver=1.0" type="text/javascript"></script>
<script id="jquery-sticky-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/jquery.sticky.js?ver=5.6" type="text/javascript"></script>
<script id="jquery-easing-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/jquery.easing-1.3.pack.js?ver=5.6" type="text/javascript"></script>
<script id="bootstrapjs-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/bootstrap.min.js?ver=5.6" type="text/javascript"></script>
<script id="parallax-jquery-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/jquery.parallax-1.1.3.js?ver=5.6" type="text/javascript"></script>
<script id="appearjs-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/appear.js?ver=5.6" type="text/javascript"></script>
<script id="modernizrjs-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/modernizr.js?ver=5.6" type="text/javascript"></script>
<script id="prettyphotojs-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/jquery.prettyPhoto.js?ver=5.6" type="text/javascript"></script>
<script id="isotopejs-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/isotope.js?ver=5.6" type="text/javascript"></script>
<script id="bxslider-jquery-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/jquery.bxslider.min.js?ver=5.6" type="text/javascript"></script>
<script id="cycle-all-jquery-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/jquery.cycle.all.js?ver=5.6" type="text/javascript"></script>
<script id="maximage-jquery-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/jquery.maximage.js?ver=5.6" type="text/javascript"></script>
<script id="skrollrjs-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/skrollr.js?ver=5.6" type="text/javascript"></script>
<script id="waitforimages-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/jquery.waitforimages.js?ver=5.6" type="text/javascript"></script>
<script id="flexsliderjs-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/jquery.flexslider.js?ver=5.6" type="text/javascript"></script>
<script id="caroufredseljs-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/jquery.carouFredSel-6.2.1-packed.js?ver=5.6" type="text/javascript"></script>
<script id="easingjs-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/jquery.easing.js?ver=5.6" type="text/javascript"></script>
<script id="mousewheeljs-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/jquery.mousewheel.js?ver=5.6" type="text/javascript"></script>
<script id="knobjs-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/jquery.knob.js?ver=5.6" type="text/javascript"></script>
<script id="newave_pluginsjs-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/plugins.js?ver=5.6" type="text/javascript"></script>
<script id="scriptsjs-js" src="https://rj-capital.de/wp-content/themes/newave-theme/js/scripts.js?ver=5.6" type="text/javascript"></script>
<script id="wp-embed-js" src="https://rj-capital.de/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-88835033-1', 'auto');
  ga('send', 'pageview');

</script></body>
</html>