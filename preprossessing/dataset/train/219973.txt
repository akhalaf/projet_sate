<!DOCTYPE html>
<html>
<head>
<title>Nothing found</title>
<meta charset="utf-8"/>
<style>
      @font-face {
        font-family: 'BrownStd-Bold';
        src: url('https://www.billboard.com/assets/1608590758/fonts/BrownStd/BrownStd-Bold.woff?55522f63a0c2cf148028');
        font-weight: normal;
        font-style: normal;
      }

      @font-face {
        font-family: 'BrownStd-Regular';
        src: url('https://www.billboard.com/assets/1608590758/fonts/BrownStd/BrownStd-Regular.woff?55522f63a0c2cf148028');
        font-weight: normal;
        font-style: normal;
      }
      * {
        margin: 0;
        padding: 0;
      }
      html, body{
            height: 100%;
            width: 100%;
          }
      body {
        font: 17px/24px 'BrownStd-Regular', sans-serif;
      }
      p {
        margin: 15px 0;
      }
      a {
        color: #3db1f4;
        text-decoration: none;
      }
        a:hover {
          text-decoration: underline;
        }
      #xouter{
        height:100%;
        width:100%;
        display: table;
        vertical-align: middle;
        padding: 0
      }
      #xcontainer {
        text-align: center;
        position: relative;
        vertical-align: middle;
        display: table-cell;
        *margin-top: 100px;
      }
      #xinner {
        padding: 15px 30px;
        text-align: left;
        margin: 0 auto 75px;
        position: relative;
        width: 650px;
        border: 1px solid #e6e6e6;
        text-align: center;
      }
      #logo {
        background: url("https://www.billboard.com/assets/1608590758/images/sprite-2013-01-25.png?55522f63a0c2cf148028") no-repeat scroll 0 -2400px transparent;
        width: 147px;
        height: 30px;
        margin: 0 auto;
        text-indent: -9999px;
      }
      #beta {
        background: url("https://www.billboard.com/assets/1608590758/images/sprite-2013-01-25.png?55522f63a0c2cf148028") no-repeat scroll -274px -67px transparent;
        width: 24px;
        height: 7px;
        margin: 0 auto;
        text-indent: -9999px;
        position: relative;
        right: -43px;
      }
      #copyright {
        text-align: center;
        color: #b2b2b2;
        font-size: 13px;
        line-height: 17px;
        font-family: sans-serif;
      }
      #hold_footer {
        border-top: 1px solid #e6e6e6;
        padding-top: 30px;
        margin-left: -30px;
        margin-right: -30px;
        margin-top: 30px;
      }
      h1 {
        font-size: 35px;
        line-height: 42px;
        margin-bottom: 15px;
        margin-top: 15px;
      }
    </style>
<script async="" src="/cdn-cgi/bm/cv/669835187/api.js"></script></head>
<body>
<div id="xouter">
<div id="xcontainer">
<div id="xinner">
<h1>Looking for something on<br/>Billboard?</h1>
<p>Sorry, whatever you were looking for is no longer here, or you typed in the wrong URL. Please double-check and try again.</p>
<p><a href="/">Homepage</a></p>
<div id="hold_footer">
<p id="logo">Billboard</p>
<p id="beta">beta</p>
<p id="copyright">© 2021 Billboard Media, LLC. All Rights Reserved.</p>
</div>
</div>
</div>
</div>
<script type="text/javascript">(function(){window['__CF$cv$params']={r:'611a427ecfc92254',m:'4d86faf40fb0a6aa165db0ec428a95dc89f792a8-1610657746-1800-Acsj4BpbF62WnLBjdkHMpi8HdTs/wwUq5IExr3a0W1ZS7fn+DbbFGG8IRbSYvG5jlRs5HH74BgHy/6qOaVv6263Ppu0mdfhRlvY97BXcZ6rz+1tJpXS+HWvDyJGnQr5DNHL4uzfeiIw+6h0sNqWAln6QB8uedKLASMldW/ORAO4fgtnO+jES8t82M9fLbMradA==',s:[0x6d9f600210,0xa273ecd53e],}})();</script></body>
</html>
