<!DOCTYPE html>
<html class="TF" id="html" lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1">
<base href="/TrackAndField/"/>
<script type="text/javascript">
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];

        var pbjs = pbjs || {};
        pbjs.que = pbjs.que || [];
    </script>
<script>
        var PREBID_TIMEOUT = 2000;
        var FAILSAFE_TIMEOUT = 3000;

        googletag.cmd.push(function () {
            googletag.pubads().disableInitialLoad();
        });

        var prebid_bidders_720 = [{ bidder: 'appnexus', params: { placementId: '12769519',reserve:0.90}, labelAny: ['phone'] },{ bidder: 'appnexus', params: { placementId: '12769520',reserve:0.90}, labelAny: ['display', 'tablet'] },{ bidder: 'conversant', params: { site_id: '116164',bidfloor:0.10, secure: 1 } },{ bidder: 'criteo', params: { zoneId: 1144130 }, labelAny: ['phone'] },{ bidder: 'criteo', params: { zoneId: 1143004 }, labelAny: ['display', 'tablet'] },{ bidder: 'lockerdome', params: { adUnitId: 'LD12644243462403686' }, labelAny: ['phone'] },{ bidder: 'lockerdome', params: { adUnitId: 'LD12644244469036646' }, labelAny: ['display', 'tablet'] },{ bidder: 'rubicon', params: { accountId: '18102', siteId: '188796', zoneId: '919942',floor:0.10} },{ bidder: 'gumgum', params: { inSlot: 7161 }, labelAny: ['phone'] },{ bidder: 'gumgum', params: { inSlot: 7162 }, labelAny: ['display', 'tablet'] },{ bidder: 'sovrn', params: { tagid: '539836',reserve:0.90}, labelAny: ['phone'] },{ bidder: 'sovrn', params: { tagid: '539833',reserve:0.90}, labelAny: ['display', 'tablet'] }];
        var prebid_bidders_320_300Box = [{ bidder: 'appnexus', params: { placementId: '12769519',reserve:0.90}, labelAny: ['phone'] },{ bidder: 'appnexus', params: { placementId: '12769518',reserve:0.90}, labelAny: ['display', 'tablet'] },{ bidder: 'conversant', params: { site_id: '116164',bidfloor:0.10, secure: 1 } },{ bidder: 'criteo', params: { zoneId: 1144130 }, labelAny: ['phone'] },{ bidder: 'criteo', params: { zoneId: 1143002 }, labelAny: ['display', 'tablet'] },{ bidder: 'lockerdome', params: { adUnitId: 'LD12644243462403686' }, labelAny: ['phone'] },{ bidder: 'lockerdome', params: { adUnitId: 'LD12644242455770726' }, labelAny: ['display', 'tablet'] },{ bidder: 'rubicon', params: { accountId: '18102', siteId: '188796', zoneId: '919942',floor:0.10} },{ bidder: 'gumgum', params: { inSlot: 7161 }, labelAny: ['phone'] },{ bidder: 'gumgum', params: { inSlot: 7160 }, labelAny: ['display', 'tablet'] },{ bidder: 'sovrn', params: { tagid: '539836',reserve:0.90}, labelAny: ['phone'] },{ bidder: 'sovrn', params: { tagid: '539834',reserve:0.90}, labelAny: ['display', 'tablet'] }];
        var prebid_bidders_300Box = [{ bidder: 'appnexus', params: { placementId: '12769518',reserve:0.90} },{ bidder: 'conversant', params: { site_id: '116164',bidfloor:0.10, secure: 1 } },{ bidder: 'criteo', params: { zoneId: 1143002 } },{ bidder: 'lockerdome', params: { adUnitId: 'LD12644242455770726' } },{ bidder: 'rubicon', params: { accountId: '18102', siteId: '188796', zoneId: '919942',floor:0.10} },{ bidder: 'gumgum', params: { inSlot: 7160 } },{ bidder: 'sovrn', params: { tagid: '539834',reserve:0.90} }];
        var prebid_bidders_outstream640 = [{ bidder: 'appnexus', params: { placementId: 13979005, allowSmallerSizes: true,reserve:1.00}, labelAny: ['display', 'tablet'] },{ bidder: 'teads', params: { placementId:100744, pageId:92990 } }];
        var prebid_bidders_outstream320 = [{ bidder: 'appnexus', params: { placementId: 14089865,reserve:1.00}, labelAny: ['phone'] }];
        var prebid_sizes_banner = [[970, 90], [728, 90], [320, 50], [300, 100], [300, 50]];
        var prebid_sizes_300Box = [[300, 250]];
        var prebid_sizes_colapsingBox = [[300, 250], [320, 50], [300, 100], [300, 50]];

        pbjs.que.push(function () {
            pbjs.setConfig({
                // enableSendAllBids: true,
                priceGranularity: { 'buckets': [{ 'min': 0, 'max': 3, 'increment': 0.10 }, { 'min': 3, 'max': 8, 'increment': 0.25 }, { 'min': 8, 'max': 20, 'increment': 1 }] },
                sizeConfig: [
                    { mediaQuery: '(min-width: 1200px)', sizesSupported: [[970, 90], [728, 90], [300, 250], [640, 480], [320, 240], [1, 1]], labels: ['display'] },
                    { mediaQuery: '(min-width: 768px) and (max-width: 1199px)', sizesSupported: [[728, 90], [468, 60], [300, 250], [320, 50], [300, 100], [300, 50], [640, 480], [320, 240], [1, 1]], labels: ['tablet'] },
                    { mediaQuery: '(min-width: 0px) and (max-width: 767px)', sizesSupported: [[480, 80], [468, 60], [300, 250], [320, 50], [300, 100], [300, 50], [120, 20], [168, 28], [640, 480], [320, 240], [1, 1]], labels: ['phone'] }
                ]
            });
            pbjs.addAdUnits([{code:'ad_ATF_Banner',mediaTypes:{banner:{sizes:prebid_sizes_banner}},bids:prebid_bidders_720},{code:'ad_BTF_Banner',mediaTypes:{banner:{sizes:prebid_sizes_banner}},bids:prebid_bidders_720},{code:'ad_video1x1',mediaTypes:{video:{context:'outstream',playerSize:[640, 480]}},bids:prebid_bidders_outstream640},{code:'ad_video1x1',mediaTypes:{video:{context:'outstream',playerSize:[320, 240]}},bids:prebid_bidders_outstream320}]);
            pbjs.requestBids({ bidsBackHandler: initAdserver, timeout: PREBID_TIMEOUT });
        });

        function initAdserver(data) {
            if (pbjs.initAdserverSet) return;
            pbjs.initAdserverSet = true;
            googletag.cmd.push(function () {
                pbjs.que.push(function () {
                    pbjs.setTargetingForGPTAsync();
                    googletag.pubads().refresh();
                    // console.log('sendAdserverRequest:', data);console.log('BidResponses',pbjs.getBidResponses());console.log('AdserverTargeting',pbjs.getAdserverTargeting());
                });
            });
        }

        setTimeout(function () { // in case PBJS doesn't load
            if (!pbjs.initAdserverSet) gtag('event', 'FAILSAFE_TIMEOUT', { event_category: 'Prebid.js FAILSAFE_TIMEOUT', event_label: 'TimeOut', value: 1, non_interaction: true });
            initAdserver('timeout');
        }, FAILSAFE_TIMEOUT);

    </script>
<script async="" src="https://stage.athletic.net/Shared/scripts/prebid/prebid3.25.0.js"></script>
<script type="text/javascript">
        googletag.cmd.push(function () {
            var bannerMapping = googletag.sizeMapping().addSize([100, 100], [88, 31]).addSize([320, 400], [[300, 100], [300, 50], [320, 50], [300, 50]]).addSize([768, 200], [[300, 100], [300, 50], [320, 50], [468, 60], [728, 90]]).addSize([1050, 200], [[300, 100], [300, 50], [320, 50], [468, 60], [728, 90], [970, 90], [1024, 120]]).build();
            var boxMapping = googletag.sizeMapping().addSize([100, 100], [88, 31]).addSize([320, 320], [[300, 100], [300, 50], [320, 50], [350, 400]]).addSize([900, 550], [[300, 250]]).build();
            var takeoverMapping = googletag.sizeMapping().addSize([0, 0], []).addSize([1200, 500], [1, 1]).build();
            var pixleMapping = googletag.sizeMapping().addSize([320, 320], [1, 1]).build();
             googletag.defineSlot('/1011615/Responsive//ATF_Banner', [728, 90], 'ad_ATF_Banner').defineSizeMapping(bannerMapping).addService(googletag.pubads());
googletag.defineSlot('/1011615/Responsive//BTF_Banner', [728, 90], 'ad_BTF_Banner').defineSizeMapping(bannerMapping).addService(googletag.pubads());
googletag.defineSlot('/1011615/bg_takeover', [1, 1], 'bgTakeover').defineSizeMapping(takeoverMapping).addService(googletag.pubads());
googletag.defineSlot('/1011615/video1x1', [1, 1], 'ad_video1x1').defineSizeMapping(pixleMapping).addService(googletag.pubads());
googletag.defineSlot('/1011615/bottom1x1', [1, 1], 'ad_bottom1x1').defineSizeMapping(pixleMapping).addService(googletag.pubads());
googletag.pubads().setTargeting('Level', 'HighSchool');
googletag.pubads().setTargeting('Page', 'TeamHome');
googletag.pubads().setTargeting('Region', 'CA');
googletag.pubads().setTargeting('Section', 'TF');
googletag.pubads().setTargeting('SSupport', '0');
googletag.pubads().setTargeting('UserRole', 'General');
googletag.pubads().setTargeting('Tag', 'notag');

            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>
<!-- Quantcast Tag, part 1 - part 2 in footer-->
<script type="text/javascript">
        var _qevents = _qevents || []; (function () { var a = document.createElement("script"); a.src = ("https:" == document.location.protocol ? "https://secure" : "http://edge") + ".quantserve.com/quant.js"; a.async = !0; a.type = "text/javascript"; var b = document.getElementsByTagName("script")[0]; b.parentNode.insertBefore(a, b) })();
    </script>
<title>
	Bishop O'Dowd High School Track &amp; Field Statistics
</title><meta content="Athletic.net" name="application-name"/><link href="/favicon.png" rel="icon" type="image/x-icon"/><meta content="width=device-width, initial-scale=1" name="viewport"/><meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/><meta content="169507956425670" property="fb:app_id"/><meta content="Athletic.net" property="og:site_name"/>
<meta content="https://www.athletic.net/TrackAndField/School.aspx?SchoolID=921" property="og:url"/>
<meta content="@AthleticNet" name="twitter:site"/><link href="../opensearch.xml" id="Link1" rel="search" title="Athletic.net Search" type="application/opensearchdescription+xml"/>
<meta content="https://www.athletic.net/images/Logo/AthleticNet-OG-Image_1200x630.jpg" id="ogImage" property="og:image"/>
<meta content="website" property="og:type"/>
<meta content="Track &amp; Field and Cross Country Statistics" property="og:title"/>
<meta content="Rankings for middle school, high school, and college athletes. Compare yourself against athletes in your district, your state, or the nation." property="og:description"/>
<meta content="summary" name="twitter:card"/>
<script type="text/javascript">var anetBSVersion = 'Bootstrap4';</script>
<link href="https://stage.athletic.net/css/Bootstrap4-v-xc39MVoAsEn5zR9uFnqmM5I2Nf1UECyY21h3KE5ibQM1" rel="stylesheet"/>
<link crossorigin="anonymous" href="https://pro.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-9ZfPnbegQSumzaE7mks2IYgHoayLtuto3AS6ieArECeaR8nCfliJVuLh/GaQ1gyM" rel="stylesheet"/>
<link href="https://stage.athletic.net/css/teampage-v-wt1jyZRo6O5KeXDyB6fRHZfK7SjCG8ByRQVC27Oz9Is1" rel="stylesheet"/>
<link href="https://stage.athletic.net/css/teamNav-v-53GbIv4DvFTxw_2JYJqqt8ZSDgIkwL1KAtulgg4zQ0c1" rel="stylesheet"/>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
        var anetSiteAppParams = {"footerRecentVideos":true,"teamHeader":921,"seasonId":2021,"footerShowSignup":true,"app":"net","isProduction":true,"siteVersion":"1.1.7684.16361","uid":0,"tree":[{"type":"sport","id":"tf","title":"Track & Field","url":"/TrackAndField/"},{"type":"div","id":116019,"title":"High School","url":"/TrackAndField/Division/Top.aspx?DivID=116019","HomePage":"./","BaseDivID":79},{"type":"div","id":116302,"title":"United States","url":"/TrackAndField/Division/Top.aspx?DivID=116302","HomePage":"","BaseDivID":2},{"type":"div","id":116431,"title":"California","url":"/TrackAndField/Division/Top.aspx?DivID=116431","HomePage":"California/","BaseDivID":278},{"type":"div","id":116497,"title":"North Coast","url":"/TrackAndField/Division/Top.aspx?DivID=116497","HomePage":"California/NorthCoast.aspx","BaseDivID":319},{"type":"div","id":116503,"title":"Bayshore","url":"/TrackAndField/Division/Top.aspx?DivID=116503","HomePage":"","BaseDivID":323},{"type":"div","id":116506,"title":"West Alameda County","url":"/TrackAndField/Division/Top.aspx?DivID=116506","HomePage":"","BaseDivID":326},{"type":"team","id":921,"title":"Bishop O'Dowd","url":"/TrackAndField/School.aspx?SchoolID=921"}]};
    </script>
<script type="application/ld+json">{"@context":"http://schema.org","@type":"BreadcrumbList","itemListElement":[{"@type":"ListItem","position":1,"name":"Track & Field","item":"https://www.athletic.net/TrackAndField/"},{"@type":"ListItem","position":2,"name":"High School","item":"https://www.athletic.net/TrackAndField/Division/Top.aspx?DivID=116019"},{"@type":"ListItem","position":3,"name":"United States","item":"https://www.athletic.net/TrackAndField/Division/Top.aspx?DivID=116302"},{"@type":"ListItem","position":4,"name":"California","item":"https://www.athletic.net/TrackAndField/Division/Top.aspx?DivID=116431"},{"@type":"ListItem","position":5,"name":"North Coast","item":"https://www.athletic.net/TrackAndField/Division/Top.aspx?DivID=116497"},{"@type":"ListItem","position":6,"name":"Bayshore","item":"https://www.athletic.net/TrackAndField/Division/Top.aspx?DivID=116503"},{"@type":"ListItem","position":7,"name":"West Alameda County","item":"https://www.athletic.net/TrackAndField/Division/Top.aspx?DivID=116506"}]}</script>
<script src="https://stage.athletic.net/js/moment-v-4uBI_Vj5PSeJoMBRlWCjQjI0slTmm0L9XXBzYZhzUaY1"></script>
<script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-database.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>
<script crossorigin="" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
<script>
        var anetURLS = {
            searchCompleteCSS: 'https://stage.athletic.net/css/SearchComplete-v-9Qh6OR6qtB5zAf8pQau74z_5WvwLklZur2Xn__8pxs1',
            searchCompleteJS: 'https://stage.athletic.net/js/SearchComplete-v-4ayVlTASYemYUfdwrP3x4qjTOprXqTMJhxtPmxAC281',
            fastClickJS: 'https://stage.athletic.net/js/FastClick-v-cTIvAJU9NF88N39T3pDSqxGmZdhyl7dppM6rJOXkAc1'
        }

        function $anetAJAX(a, b, c) { return $.ajax(a, { type: "POST", contentType: "application/json", data: b, dataType: "json", context: c }) };
    </script>
<script src="https://stage.athletic.net/js/MainJS_NewLo-v-NKpjZHaujLLRc1hM72xMHOCbXCDxSW3tKaz9dINLSA1"></script>
<script async="" src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<link href="https://www.athletic.net/TrackAndField/School.aspx?SchoolID=921" rel="canonical"/>
<meta content="TF-BOOD HS" name="favTitle"/>
<script>
        var anetBlogEdit = false;
        var CKEDITOR_BASEPATH = '/Shared/components/ckeditor/4.5.7-custom/';

        angular.module('angularPartialsBundles', []);
    </script>
<script src="https://stage.athletic.net/partials/TeamPage-v-9glTYo389v5FeAXYyYDA6VKQaBz1xFhzj4gW8CLw1"></script>
<script src="https://stage.athletic.net/js/TeamPage2-v-pg0wwooy9cs62m3XqE9aVG_USSO9EHCrnjzcEWqHPo1"></script>
<script src="https://stage.athletic.net/js/TeamPage3-v-guckV_4LXFoWC1DmNb51sN3MY9Qw8yBLOvQragwABM1"></script>
<script src="https://stage.athletic.net/js/TeamPage-v-SfN_nJMaH4Mb2R9NnavhVMEaVCaKsKD3x97zKeiw01"></script>
<script>
        (function () {
            angular.module('teamHome')
                .constant('sportPath', '/TrackAndField/TeamHome/')
                .constant("params", {"sport":"tf","SchoolID":921,"SeasonID":2021,"MembershipLevel":0,"TwitterUser":null,"addEventUrl":"us","UserID":0,"photoStyle":null,"isCoach":false,"isAuthenticatedWithGoodStanding":false,"editPermission":false,"admin":false,"isTeamAthlete":false,"publicToken":"931somji48wudjcssd0jaw","editToken":null,"uploadToken":null,"guid":"453dd0ce-975a-4554-9bd6-b9699fd87904","embedToken":"0adk4","coverPhoto":null,"athleteCoachToken":"","teamCode":null,"live":true})
                .constant("coachAlerts", null)
                .constant("initialData", {"team":{"Name":"Bishop O'Dowd","Level":4,"TeamRecords":1,"Address":"9500 Stearns Avenue","City":"Oakland ","State":"CA","ZipCode":"94605-4799","Phone":"510-577-9100","Fax":"510-638-3259","URL":"http://www.bishopodowd.org/","URLTeam":"http://bishopodowd.org/athletics/cross_country.php","PrefStore":1,"PrepSportID":117270,"AccountLock":1,"RegionID":4,"hasPhotos":false},"grades":[{"IDGrade":9,"GradeDesc":"9th Grade"},{"IDGrade":10,"GradeDesc":"10th Grade"},{"IDGrade":11,"GradeDesc":"11th Grade"},{"IDGrade":12,"GradeDesc":"12th Grade"},{"IDGrade":99,"GradeDesc":"?"}],"teamNav":{"team":{"Name":"Bishop O'Dowd","Level":4,"City":"Oakland ","State":"CA","Mascot":"Dragons","MascotUrl":"//lh3.googleusercontent.com/Egp3tBLjCbE3KhBWDLGhj5c56Vdo5-ma1_E3ugqCWnJM__dVVScqTDriTHdn7FCyMpg65pjEds9OpCIQQ3TnWQ","TeamRecords":1,"siteSupport":0,"colors":["Black","Gold",null],"coverPhoto":null,"teamFollowerCount":99},"userAthleteIdOnTeam":0,"hasCoachAccess":false,"mascotToken":null,"mascotToken2":null,"grades":[{"IDGrade":9,"GradeDesc":"9th Grade"},{"IDGrade":10,"GradeDesc":"10th Grade"},{"IDGrade":11,"GradeDesc":"11th Grade"},{"IDGrade":12,"GradeDesc":"12th Grade"},{"IDGrade":99,"GradeDesc":"?"}],"divisions":[{"id":115858,"b":75,"name":"2021 Outdoor","gender":"x"},{"id":116019,"b":79,"name":"High School","gender":"x"},{"id":116302,"b":2,"name":"United States","gender":"x"},{"id":116431,"b":278,"name":"California","gender":"x"},{"id":116497,"b":319,"name":"North Coast","gender":"x"},{"id":116503,"b":323,"name":"Bayshore","gender":"x"},{"id":116506,"b":326,"name":"West Alameda County","gender":"x"}],"customLists":{},"customDivisions":[]},"regions":[{"ID":19,"Name":"Alabama"},{"ID":6,"Name":"Alaska"},{"ID":209,"Name":"Alberta"},{"ID":17,"Name":"Arizona"},{"ID":20,"Name":"Arkansas"},{"ID":206,"Name":"British Columbia"},{"ID":9,"Name":"CA: Central"},{"ID":10,"Name":"CA: Central Coast"},{"ID":11,"Name":"CA: Los Angeles"},{"ID":4,"Name":"CA: North Coast"},{"ID":3,"Name":"CA: Northern"},{"ID":12,"Name":"CA: Oakland"},{"ID":13,"Name":"CA: Sac-Joaquin"},{"ID":14,"Name":"CA: San Diego"},{"ID":15,"Name":"CA: San Francisco"},{"ID":16,"Name":"CA: Southern"},{"ID":21,"Name":"Colorado"},{"ID":22,"Name":"Connecticut"},{"ID":23,"Name":"Delaware"},{"ID":60,"Name":"District of Columbia"},{"ID":301,"Name":"Eastern Caribbean States"},{"ID":24,"Name":"Florida"},{"ID":25,"Name":"Georgia"},{"ID":7,"Name":"Hawaii"},{"ID":5,"Name":"Idaho"},{"ID":26,"Name":"Illinois"},{"ID":27,"Name":"Indiana"},{"ID":28,"Name":"Iowa"},{"ID":29,"Name":"Kansas"},{"ID":30,"Name":"Kentucky"},{"ID":31,"Name":"Louisiana"},{"ID":32,"Name":"Maine"},{"ID":205,"Name":"Manitoba"},{"ID":33,"Name":"Maryland"},{"ID":34,"Name":"Massachusetts"},{"ID":35,"Name":"Michigan"},{"ID":36,"Name":"Minnesota"},{"ID":37,"Name":"Mississippi"},{"ID":38,"Name":"Missouri"},{"ID":39,"Name":"Montana"},{"ID":40,"Name":"Nebraska"},{"ID":18,"Name":"Nevada"},{"ID":204,"Name":"New Brunswick"},{"ID":41,"Name":"New Hampshire"},{"ID":42,"Name":"New Jersey"},{"ID":43,"Name":"New Mexico"},{"ID":44,"Name":"New York"},{"ID":210,"Name":"Newfoundland & Labrador"},{"ID":45,"Name":"North Carolina"},{"ID":46,"Name":"North Dakota"},{"ID":212,"Name":"Northwest Territories"},{"ID":203,"Name":"Nova Scotia"},{"ID":47,"Name":"Ohio"},{"ID":48,"Name":"Oklahoma"},{"ID":201,"Name":"Ontario"},{"ID":1,"Name":"Oregon"},{"ID":101,"Name":"Overseas"},{"ID":49,"Name":"Pennsylvania"},{"ID":207,"Name":"Prince Edward Island"},{"ID":202,"Name":"Quebec"},{"ID":50,"Name":"Rhode Island"},{"ID":208,"Name":"Saskatchewan"},{"ID":51,"Name":"South Carolina"},{"ID":52,"Name":"South Dakota"},{"ID":53,"Name":"Tennessee"},{"ID":54,"Name":"Texas"},{"ID":8,"Name":"Utah"},{"ID":55,"Name":"Vermont"},{"ID":56,"Name":"Virginia"},{"ID":2,"Name":"Washington"},{"ID":57,"Name":"West Virginia"},{"ID":58,"Name":"Wisconsin"},{"ID":59,"Name":"Wyoming"},{"ID":211,"Name":"Yukon"}],"seasons":{"1980":{"ID":1980},"1981":{"ID":1981},"1982":{"ID":1982},"1983":{"ID":1983},"1984":{"ID":1984},"1985":{"ID":1985},"1986":{"ID":1986},"1987":{"ID":1987},"1988":{"ID":1988},"1989":{"ID":1989},"1990":{"ID":1990},"1991":{"ID":1991},"1992":{"ID":1992},"1993":{"ID":1993},"1994":{"ID":1994},"1995":{"ID":1995},"1996":{"ID":1996},"1997":{"ID":1997},"1998":{"ID":1998},"1999":{"ID":1999},"2000":{"ID":2000},"2001":{"ID":2001},"2002":{"ID":2002},"2003":{"ID":2003},"2004":{"ID":2004},"2005":{"ID":2005},"2006":{"ID":2006},"2007":{"ID":2007},"2008":{"ID":2008},"2009":{"ID":2009},"2010":{"ID":2010},"2011":{"ID":2011},"2012":{"ID":2012},"2013":{"ID":2013},"2014":{"ID":2014},"2015":{"ID":2015},"2016":{"ID":2016},"2017":{"ID":2017},"2018":{"ID":2018},"2019":{"ID":2019},"2020":{"ID":2020},"2021":{"ID":2021},"12007":{"ID":12007},"12010":{"ID":12010},"12011":{"ID":12011},"12012":{"ID":12012},"12013":{"ID":12013},"12016":{"ID":12016},"12017":{"ID":12017},"12018":{"ID":12018},"12019":{"ID":12019},"12020":{"ID":12020}},"currentCal":[],"inviteRequests":null,"athletes":[{"ID":12287750,"Name":"Abigail Gonzalez","Gender":"F"},{"ID":13959406,"Name":"Abigale Richter","Gender":"F"},{"ID":16460999,"Name":"Acacia Sellers","Gender":"F"},{"ID":14506120,"Name":"Aidan Rocha","Gender":"M"},{"ID":1746505,"Name":"Alex White","Gender":"F"},{"ID":16460977,"Name":"Alijah Brown","Gender":"M"},{"ID":14564780,"Name":"Alon Evron","Gender":"M"},{"ID":16461070,"Name":"Amare Chatman","Gender":"M"},{"ID":16634227,"Name":"Amari Grey","Gender":"M"},{"ID":16459739,"Name":"Andrew Bell","Gender":"M"},{"ID":17235549,"Name":"Ayana Grant","Gender":"F"},{"ID":12199674,"Name":"Benjamin Eichel","Gender":"M"},{"ID":16460956,"Name":"Bernard Jones","Gender":"M"},{"ID":16460971,"Name":"Brandon Rockmore","Gender":"M"},{"ID":16460973,"Name":"Cameron Hankins","Gender":"M"},{"ID":12692292,"Name":"Cassidy Cisz","Gender":"F"},{"ID":12769792,"Name":"Chisomaga Nlemigbo","Gender":"M"},{"ID":13959271,"Name":"Christopher Costagliola","Gender":"M"},{"ID":14518530,"Name":"Conner Pate","Gender":"M"},{"ID":6194874,"Name":"Crystal Miles-Threat","Gender":"F"},{"ID":17533605,"Name":"Dan Steven","Gender":"M"},{"ID":16629185,"Name":"Danielle Kha","Gender":"F"},{"ID":10187541,"Name":"David Green","Gender":"M"},{"ID":15758264,"Name":"Dylan Feldman","Gender":"M"},{"ID":15451763,"Name":"Dylan Frith","Gender":"M"},{"ID":16460991,"Name":"Edward Hoffman","Gender":"M"},{"ID":16293834,"Name":"Elijah Brown","Gender":"M"},{"ID":16629413,"Name":"Elijah Warren","Gender":"M"},{"ID":15758453,"Name":"Elina Virmani","Gender":"F"},{"ID":13973333,"Name":"Ellie Sellman","Gender":"F"},{"ID":12694816,"Name":"Emily Dotson","Gender":"F"},{"ID":16462526,"Name":"Ethan Browne","Gender":"M"},{"ID":13007834,"Name":"Faith Odesanya","Gender":"F"},{"ID":11173944,"Name":"Giancarlo Joyner","Gender":"M"},{"ID":13959311,"Name":"Griffin Osser","Gender":"M"},{"ID":15758445,"Name":"Hannah Elenteny","Gender":"F"},{"ID":13959324,"Name":"Hannes Rodriguez","Gender":"M"},{"ID":12769793,"Name":"Henry Palmer III","Gender":"M"},{"ID":14518502,"Name":"Indigo Eatman","Gender":"F"},{"ID":13959398,"Name":"Isabella Whitten","Gender":"F"},{"ID":14687948,"Name":"Issaac Greene","Gender":"M"},{"ID":12199656,"Name":"Jackson Bigelow","Gender":"M"},{"ID":16460975,"Name":"Jacob Monroe","Gender":"M"},{"ID":16461067,"Name":"Jaden Yamamoto","Gender":"M"},{"ID":17216854,"Name":"Jamilla Churchill","Gender":"F"},{"ID":12199813,"Name":"Janelle Mondesir","Gender":"F"},{"ID":12694815,"Name":"Jazlynn Gibbs","Gender":"F"},{"ID":16634240,"Name":"Johnathan Brewer","Gender":"M"},{"ID":16496671,"Name":"Joliese Davis-Pinkney","Gender":"F"},{"ID":14457532,"Name":"Kamryn Beene","Gender":"F"},{"ID":12199772,"Name":"Kandace Blackshire","Gender":"F"},{"ID":17235550,"Name":"Kendra Johnson","Gender":"F"},{"ID":12992753,"Name":"Kennedy Johnson","Gender":"F"},{"ID":12992759,"Name":"Kennedy Lincoln","Gender":"F"},{"ID":13959394,"Name":"Lilian Yu","Gender":"F"},{"ID":12199750,"Name":"Lucas Chaney","Gender":"M"},{"ID":13959403,"Name":"Maika Schneider","Gender":"F"},{"ID":16462331,"Name":"Malik Graham","Gender":"M"},{"ID":13959290,"Name":"Marcus Fong","Gender":"M"},{"ID":16293833,"Name":"Mathew White","Gender":"M"},{"ID":16460963,"Name":"Max Iranmanesh","Gender":"M"},{"ID":12199775,"Name":"Maya DaSilva","Gender":"F"},{"ID":14457936,"Name":"Meilin Nelson","Gender":"F"},{"ID":16460036,"Name":"Michael Pham","Gender":"M"},{"ID":13959358,"Name":"Mickey Dunstan","Gender":"F"},{"ID":6197025,"Name":"Miesha Marzell","Gender":"F"},{"ID":14457534,"Name":"Milan Ford","Gender":"F"},{"ID":13959364,"Name":"Mollie King","Gender":"F"},{"ID":12992752,"Name":"Morgan Jenkins","Gender":"F"},{"ID":12992730,"Name":"Naomi Meyer","Gender":"F"},{"ID":15758446,"Name":"Natalie Elenteny","Gender":"F"},{"ID":6194876,"Name":"Nicky Saleta","Gender":"F"},{"ID":14518461,"Name":"Olivia Hoffman-Paul","Gender":"F"},{"ID":12199809,"Name":"Olwen Meritt","Gender":"F"},{"ID":12199711,"Name":"Peter Schmitz","Gender":"M"},{"ID":15758270,"Name":"Ryan O'Donnell","Gender":"M"},{"ID":12199730,"Name":"Ryan Wright","Gender":"M"},{"ID":12199774,"Name":"Samantha Daniels","Gender":"F"},{"ID":12199835,"Name":"Sariyah Shabazz","Gender":"F"},{"ID":16459742,"Name":"Sierra Camacho","Gender":"F"},{"ID":16397294,"Name":"Sophie Rodague","Gender":"F"},{"ID":14508312,"Name":"Sumayah Chatman","Gender":"F"},{"ID":15758452,"Name":"Sydney Seltzer","Gender":"F"},{"ID":17533606,"Name":"Tarrick Glenn","Gender":"M"},{"ID":16460954,"Name":"Tobias Yegian","Gender":"M"},{"ID":15758441,"Name":"Tonia Bauerlein Kelly","Gender":"F"},{"ID":16466857,"Name":"Trent Bigelow","Gender":"M"},{"ID":14040889,"Name":"Vince Moreno","Gender":"M"},{"ID":16460987,"Name":"Willie Rogers","Gender":"M"},{"ID":13959348,"Name":"Zorah Chappel","Gender":"F"}],"coaches":[{"ID":94430,"Name":"Jamal Cooks","Position":"Athletic Director","PhotoUrl":null,"DaysSinceActive":243,"PhotoToken":""},{"ID":94502,"Name":"Hillary Kigar","Position":"Coach","PhotoUrl":null,"DaysSinceActive":103,"PhotoToken":""},{"ID":1202345,"Name":"Carmen LaRoche","Position":"Coach","PhotoUrl":null,"DaysSinceActive":263,"PhotoToken":""},{"ID":254221,"Name":"Laron McCoy","Position":"Assistant Coach","PhotoUrl":null,"DaysSinceActive":312,"PhotoToken":""}],"adHTML":null,"tLog":null,"userInfo":null,"vendors":[],"cartAmounts":null,"cartFees":null,"subscriptions":null,"tipOfTheDayHistory":null});

            angular.module('anet.blog')
                .constant('blogInfo', {
                    "welcomeMessage": {"AuthorID":0,"LinkType":109,"LinkID":921,"AccessLevel":0,"UserRole":"Public","authToken":"none","Index":{},"FirstPost":null},
                    "teamBlog": {"AuthorID":0,"LinkType":120,"LinkID":921,"AccessLevel":0,"UserRole":"Public","authToken":"none","Index":{},"FirstPost":null}
                    });
        })();
        var anetAngularJsApp = 'teamHome';
    </script>
</head>
<body id="body">
<anet-athletic-header></anet-athletic-header>
<main class="d-flex flex-column" id="anetMain">
<div class="ad-takeover visible-lg hidden-md-down d-none d-lg-block">
<div class="container">
<div id="bgTakeoverLeft"></div>
<div id="bgTakeoverRight"></div>
</div>
</div>
<div id="printHeader" style="display: none;">
<img src="/images/Logo/Athletic_net_red.svg" style="margin-bottom: 4px;" width="175"/>
<span style="float: right; margin-top: 8px">Team Results Management</span>
</div>
<div aria-hidden="true" class="Ad ATF Banner" id="ad_ATF_Banner">
<script type="text/javascript">
                googletag.cmd.push(function () { googletag.display('ad_ATF_Banner'); });
            </script>
</div>
<div class="small text-center hidden-print d-print-none" id="ATF_Remove_Ads" style="margin: -5px 0 5px;">
<a href="/features/athletes" style="background: #ededed; border-radius: 2px; padding: 0 7px;">View Athletic.net Ad Free</a>
</div>
<div class="container">
<div class="ng-cloak" ng-controller="AppCtrl as appC">
<div ng-include="::'Alerts/alerts.tpl.html' | templatesURL"></div>
<div ng-include="::'Photos/photos.tpl.html' | templatesURL"></div>
<div class="mb-2 mt-1 alert alert-primary" ng-if="appC.editPermission &amp;&amp; appC.teamCode">
                Team Code: <a class="alert-link" href="/join-team/{{::appC.teamCode}}"><b>{{::appC.teamCode}}</b></a>
<button class="btn btn-light float-right" ng-click="appC.showVirtualInvite()" style="margin-top: -7px;" type="button">
<i class="far fa-share"></i><span class="d-none d-sm-inline ml-1">Share Team Code</span>
</button>
</div>
<div class="row">
<div class="col-md-6">
<div ng-include="::'Calendar/calendar.tpl.html' | templatesURL"></div>
<team-log></team-log>
</div>
<div class="col-md-6">
<div class="text-center mb-2" ng-if="!appC.params.MembershipLevel &amp;&amp; !appC.editPermission">
<a class="btn btn-outline-sport btn-lg" href="/Partner/SiteSupporter/Upgrade.aspx?SchoolID={{::appC.params.SchoolID}}&amp;utm_campaign=a_Upgrade&amp;utm_source=tf_TeamP&amp;Track">
<i class="fas fa-star yellowS"></i>
                            Upgrade Team
                        </a>
<a class="btn btn-link-sport" href="/features/coaches?SchoolID=921&amp;utm_campaign=a_Upgrade&amp;utm_source=tf_TeamC&amp;Track" target="_blank">Learn More</a>
</div>
<div ng-if="!appC.params.MembershipLevel">
<div ng-if="appC.team.PrepSportID" ng-include="::'PrepSportswear/prepSportswear.tpl.html' | templatesURL"></div>
<div class="mb-2 text-center" ng-bind-html="appC.adHTML" ng-if="!appC.team.PrepSportID"></div>
</div>
<div anet-blog="appC.welcomeMessage"></div>
<anet-lazy-module module="postsNoTLog"></anet-lazy-module>
<vendors></vendors>
<div ng-include="::'Athletes/athletes.tpl.html' | templatesURL"></div>
<div ng-include="::'TwitterFeed/twitterFeed.tpl.html' | templatesURL"></div>
<files></files>
</div>
</div>
<div class="card">
<h5 class="card-header pointer" ng-click="appC.$ui.hideBlog = !appC.$ui.hideBlog">
<i class="fas fa-comment" style="margin-right: 7px;"></i>Blog
                </h5>
<div anet-blog="appC.teamBlog" class="card-body slide-animation" ng-if="!appC.$ui.hideBlog"></div>
</div>
<div class="row">
<div class="col-md-6">
<div ng-include="::'Coaches/coaches.tpl.html' | templatesURL"></div>
</div>
<div class="col-md-6">
<div ng-include="::'Contact/contact.tpl.html' | templatesURL"></div>
</div>
</div>
<div ng-if="appC.params.MembershipLevel &amp;&amp; appC.team.PrefStore &amp;&amp; appC.team.PrepSportID" ng-include="::'PrepSportswear/prepSportswear.tpl.html' | templatesURL"></div>
</div>
</div>
<anet-site-app></anet-site-app>
<div id="ad_video1x1" style="text-align: center">
<script type="text/javascript">googletag.cmd.push(function () { googletag.display('ad_video1x1'); });</script>
</div>
<div id="ad_bottom1x1">
<script type="text/javascript">googletag.cmd.push(function () { googletag.display('ad_bottom1x1'); });</script>
</div>
<div aria-hidden="true" class="Ad BTF Banner" id="ad_BTF_Banner">
<script type="text/javascript">googletag.cmd.push(function () { googletag.display('ad_BTF_Banner'); });</script>
</div>
</main>
<anet-athletic-footer></anet-athletic-footer>
<div id="bgTakeover" style="display: none;">
<script type="text/javascript">googletag.cmd.push(function () { googletag.display('bgTakeover'); });</script>
</div>
<script type="text/javascript">
        function SetupTakeover(left, right) {
            document.getElementById('bgTakeoverLeft').innerHTML = left.innerHTML;
            document.getElementById('bgTakeoverRight').innerHTML = right.innerHTML;
            document.getElementById('bgTakeover').innerHTML = '';
        }
        function SetupANetCustomAd(dest, source) {
            document.getElementById(dest).appendChild(source);
            document.getElementById(dest).firstChild.setAttribute('style', 'display:none');
        }
    </script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-297644-1"></script>
<script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'UA-297644-1', {
            'dimension1':'General'
             
        });

      

        
        function gtagTOga(a, b, category, action, label, value, fieldsObject) { gtag('event', action, { event_category: category, event_label: label, value: value, non_interaction: true }); }
        pbjs.que.push(function () {
            pbjs.enableAnalytics([{ provider: 'ga', options: { enableDistribution: true, global: 'gtagTOga' } }]);
        });
    </script>
<!-- End Google Analytics Tag -->
<!-- Begin comScore Tag -->
<script>
        var _comscore = _comscore || [];
        _comscore.push({ c1: "2", c2: "22801758" });
        (function () {
            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
            el.parentNode.insertBefore(s, el);
        })();
    </script>
<noscript><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=22801758&amp;cv=2.0&amp;cj=1"/></noscript>
<!-- End comScore Tag -->
<!-- Begin Quantcast Tag, part 2 - part one in header -->
<script type="text/javascript">
        _qevents.push([{ qacct: "p-357yONfRnfd4g" }, { qacct: "p-56TeoNo_KrtZ6" }]);
    </script>
<noscript>
<div style="display: none;">
<img alt="Quantcast" border="0" height="1" src="//pixel.quantserve.com/pixel/p-357yONfRnfd4g.gif" width="1"/>
<img alt="Quantcast" border="0" height="1" src="//pixel.quantserve.com/pixel/p-56TeoNo_KrtZ6.gif" width="1"/>
</div>
</noscript>
<!-- End Quantcast Tag, part 2 -->
<link href="https://angular.athletic.net/app/site-app/styles.e9ce55c0627fa6d247ce.css" rel="stylesheet"/>
<script src="https://angular.athletic.net/app/site-app/runtime-es2015.49b4da2a62941a530766.js" type="module"></script>
<script defer="" nomodule="" src="https://angular.athletic.net/app/site-app/runtime-es5.49b4da2a62941a530766.js"></script>
<script defer="" nomodule="" src="https://angular.athletic.net/app/site-app/polyfills-es5.3d08feb9db76612b243d.js"></script>
<script src="https://angular.athletic.net/app/site-app/polyfills-es2015.10215f29da2a516e3170.js" type="module"></script>
<script src="https://angular.athletic.net/app/site-app/vendor-es2015.6b8ffd8845fd18485427.js" type="module"></script>
<script defer="" nomodule="" src="https://angular.athletic.net/app/site-app/vendor-es5.6b8ffd8845fd18485427.js"></script>
<script src="https://angular.athletic.net/app/site-app/main-es2015.5c8f4987b0d3f5ecb392.js" type="module"></script>
<script defer="" nomodule="" src="https://angular.athletic.net/app/site-app/main-es5.5c8f4987b0d3f5ecb392.js"></script>
</body>
</html>
