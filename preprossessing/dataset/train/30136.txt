<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "7265",
      cRay: "610b298aed4dd187",
      cHash: "dd7c3c2c7a5f100",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuNGlua2pldHMuY29tLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "gwj+VFo75WF5ACWdnG4xu806M7qkXbvZjyICpMsyiUYitdvPmVQXd9LpR2gKfwdRFvOeK+J9QT5t5VXebvq77xBu48x1XyFxPmoI0NTyuRgi01b3hUEbg6jpiTe0rOgmFHqWgLWFPiL7mxmoObLknJ7EgMTKVq8QgPj4Wi3Mb6T9stl6pfgAccrHM0aW3NMCFujy8A1EWwJUkyyHSCs/euWzW6esWWmQmERB2lWI1zHLdYUKyz8+h0MWqCeb2Zrbf/HRNkJjWjdru+5WUM5mVpQcdBqhTsU37kmPrWM1/H6F0NjvQdmBBaQ3fHE0gtJGvJE2L+VNoYpBUDSYhfIdEjClB6pdO9SrKPacSmi2YlpcsOw+zwVjn7ysV6Fo0nJUkysI+B1jvC98di5qs9VD6Y7PAq15zYbnCl3mGqkBblP+mFNt8JpwvLQic3oarNGjocVdvu9lDO7ftoCxgbaa6wTsH0Td7hQSwpV1pMZppbOIFN8/U28UqPthEiOIjc0hAfY5qYSrVNWEmjKR5DJ2gdp/i57SNRVDC2d658Mim1akNqykyEfOXduHlf+yELAfSSMy1BuaJfCVuDR0yY7vsc1IDrbEdFiJoqE0kYNLyDWTUeVv978LgcZhrXzsBqd8ekq9nI/a89UtomvG0iA3pFsxUdfgp7ckWiYdEHHVhGhhRBVE7t1hBSiw+v2mSwcws2ZcHRRxS81o1s2/MCoUnuZgIDsp1d+QFAxpd8DftjQD4FBAxVSshFJtCRnYvD7l6LLuILXGiyRE7GGktw7o6DY5pRjrXd4hMdoF7JQFRm4=",
        t: "MTYxMDQ5OTQzNy4yNjcwMDA=",
        m: "fREyp1ZGBkPlKm9NeqWVbEwx7DTIewVpjwZ1UhNyCR8=",
        i1: "WwdlcMQgKW/dM2o/DYjHyw==",
        i2: "IjXj6FuNLnONVUB56xlxaQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "SQNpAqOTjP0PQL5Mq9uioHyJb3y0ln35hbIHcvMJiaI=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610b298aed4dd187");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> 4inkjets.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=b8bed0e3c1327daa185adb8158234dff19d6f064-1610499437-0-AexbKsE_3TPLEKgEsCufDLoUB6ttA_FR1ysbBBIPDLlT-Mp4vrbNCA0LMX--Kae9kRVnelAtqH4myUFNHcs5kyaLIbxwPVEAaAQ-fuRSrhYnnsplR558-835CZgiNs_1VK0ZxJkdERh6cqeQYTfxhNJuD0oGBK2nRb8rmDRdjNL_tfa3-22jbJYlDOZOjnDtPFUzMVtVzwnGJ1M1aSSab21HME9-7_xBKkIItD4BJR104U2S2dln8tXhZg2Lqeu23aJOjN6QSTJbag0KD1FvBYN5G0kfoDHe2xP0oICZRBE1_6HJroGlAmydJwTKVZwnpmFz-F2Bx2AUEMp9Luk9uTJdSqv4uf77DaKhtBDJ-p4_pl23PkzoLu99QHA8wxkpOQ" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="8d5b2359589f54f0fe82939c78fb8337871e0346-1610499437-0-AZRCjqrRZXCFN9wScZSZdZUz3QZljgOPgwc1BY8fVpx4YtPOUn9w14ttLMyjBbIABHZrATa+Sj94IgmZ1Ecwgn/wipoA7F0K5utjBPLxlvHfJyZjlZbMurSeb1SLaL/UJXDjZA6vZQCBTqF/asYv4gGKNb9dCJCgldiOpMf6Dhv66k5McK+eSr169xtrJcH1jI2uDijdlSa7Jwu/t+CiW8jHeoGPnL6Jxj1j+dzJfnNcDJMQxPzoVRJN0UYlZNNs9absx47diNyq/kuk5Azp+pJo83OFiQEvBbC9wkn5F63XL+tmN5Ltie0CG0P5YK9g6S1OpdsUxESSBIxKEZf2CLeJMfdWhVtqYA29BBwQrC8cP3x20Bo0IOboqs8gcg8NnefPLCEPp7p61S/iG45DbrVCebARZ3UQmWhgfqroN5hCMYOaALB6VCYaiVVMoJLhxsseDZgmaPzD6uyIitDABNPKQNcgIXZp0pUMnh/hEOxm6L/zMAiaUEIy6BlsS3U1BiK42fdynLu+SuD2Vo/c2Tg++gT7SL11pjKHSd/DYMHNyE8bK7HAF+3hR5BzzxgQEzh9AR9wF42rFao4204D8uJVBW/gaNNSu3pZgXGll9Hv2xrRP6n8bEnUd9cnS8Xt3iXgGlCE2JQVzVN/UngWXL4GOkvKR1s0q6VuX73ekYpCREI8dFKbpdliPXF+8QWfel7YdwsbaePi/w9jyXgKpfBNx3eph7j9idmBSabimX5fbLFVV0PQ1axbkkJJmDJaHafHEcNAsMwziOSSAwavTwFU5EkfH/ACRH7kXfm6LuliAg2a97JyKRinNUBicMn72F96/qv32W54yFg8Mc2szFttGI9OJ+bIG7trG1NrKy14Ah3rFjJR2o4TlY6kYlEyqltynsoDs3u2bvJeiqcZTNq30NiKN7Dbv0kJTqFiUe7GGRCraPydtLECKBaTA+98kaQcUCNHokOy/yEQh1o909YYG329kUvoLvsdnOXs1LbCDd2TbTAiIevXheRIRfR19uFJKkq8GOJ9YqrDTIx8nYcFIt0u6sAu/3jP+eCFw41UHch+bkGrs17ohPXdAoyMpDDRsD1Xp5J1H+KFEi5Jy4egnp/5DpKaXJZSU7ZaHfKXeHNt8a+GXsJ3GD2xNlynapxOebcL1tHM2osn5cMc0JO0aw/wd6lC/8IgOL2zjFSLcz8n0m5cwUdY2GbrkvkZD+6eh6A5C0S+NCTF753QJneXjD+8mmzWnh+MwuLi/3DL4fpnz4sW2AoGVXZZkJewJJWk1EZSHjSj3hoFgrTFGECml2bpdPH3F8OeSgsjFPVMED406um01AiXBL5qyeGRsw=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="89a16fe7c985f1fd4135213a3bb1500b"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610499441.267-gPH5bD/vUc"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610b298aed4dd187')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610b298aed4dd187</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
