<!DOCTYPE html>
<!--
scripts and programs that download content transparent to the user are not allowed without permission
--><html lang="en"><head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><title>Unknown address c0de4189-8b3a-b56-b56-af9897b0433f – Please try to search below…</title><meta content="" name="description"/><meta content="" name="keywords"/><meta content="" property="og:image"/><meta content="1366" property="og:image:width"/><meta content="738" property="og:image:height"/><meta content="website" property="og:type"/><style>
@font-face{font-family:iconfont;src:url("/common/fonts/iconfont.woff2?v7") format("woff2"),url("/common/fonts/iconfont.woff?v7") format("woff"),url("/common/fonts/iconfont.ttf?v7") format("truetype"),url("/common/fonts/iconfont.svg?v7#iconfont") format("svg");font-weight:400;font-style:normal}
</style>
<link as="font" crossorigin="" href="/common/fonts/iconfont.woff2?v7" rel="preload" type="font/woff2"/>
<link crossorigin="" href="https://c.tadst.com" rel="preconnect"/>
<link as="script" href="/common/prebidtaddesk_9.js" rel="preload"/>
<script async="async" src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<script async="async" src="//timeanddate-d.openx.net/w/1.0/jstag?nc=1004254-timeanddate"></script>
<script>
var hbbids={},adUnits=[{code:'div-gpt-com-728x90',mediaTypes:{banner:{sizes:[[728,90]]}},bids:[{bidder:'aol',params:{dcn:'8a96958f01707087f9589c40e2820046',pos:'728x90_atf'}},{bidder:'ix',params:{siteId:'189917',size:[728,90]}},{bidder:'pubmatic',params:{publisherId:'157610',adSlot:'com728@728x90'}},{bidder:'sonobi',params:{ad_unit:'/1004254/com728'}},{bidder:'rubicon',params:{accountId:'16448',siteId:'127484',zoneId:'601634'}}]},{code:'div-gpt-com-160x600',mediaTypes:{banner:{sizes:[[160,600]]}},bids:[{bidder:'aol',params:{dcn:'8a96958f01707087f9589c40e2820046',pos:'160x600_atf'}},{bidder:'ix',params:{siteId:'281963',size:[160,600]}},{bidder:'pubmatic',params:{publisherId:'157610',adSlot:'com_160@160x600'}},{bidder:'sonobi',params:{ad_unit:'/1004254/com_160'}},{bidder:'rubicon',params:{accountId:'16448',siteId:'127484',zoneId:'601642'}}]}];
;
var pbjs=pbjs||{};pbjs.que=pbjs.que||[];
var googletag=googletag||{};
googletag.cmd=googletag.cmd||[];
googletag.cmd.push(function(){googletag.pubads().disableInitialLoad();});
pbjs.que.push(function(){
pbjs.setConfig({
debug:false,
priceGranularity:{"buckets":[{"min":0,"max":20,"increment":0.01},{"min":20,"max":30,"increment":0.10},{"min":30,"max":50,"increment":0.25},{"min":50,"max":100,"increment":1}]},
enableSendAllBids:true,
bidderTimeout:1000,
publisherDomain: "https://www.timeanddate.com"
});
pbjs.addAdUnits(adUnits);
pbjs.requestBids({bidsBackHandler:prebidDone});
});
function prebidDone(){
hbbids.prebid=1;
if(hbbids.ox){sendAdserverRequest()}
}
function oxDone(){hbbids.ox=1;if(hbbids.prebid){sendAdserverRequest()}};

function sendAdserverRequest(){
if(pbjs.adserverRequestSent)return;
var isLog=0;
function logAds(){
if(isLog){return;}
if(!window.jcb || !window.TADhba){
setTimeout(logAds,1000);
return;
}
isLog=1;
jcb("/scripts/logads.php?d=d"+"&"+TADhba(),function(){});
}
setTimeout(logAds,1000);
pbjs.adserverRequestSent=true;
googletag.cmd.push(function(){
pbjs.que.push(function(){
var c=0,a,b,r=pbjs.getBidResponses();
var ox=window.OX;if(ox&&ox.dfp_bidder){ox.dfp_bidder.setOxTargeting();try{var pm=ox.dfp_bidder.getPriceMap();for(i=0;i<pm.length;i++){if(pm[i].price){c++}}}catch(e){};}
pbjs.setTargetingForGPTAsync();
for(a in r){
if(r.hasOwnProperty(a)){
b=r[a].bids;for(var i=0;i<b.length;i++){if(b[i].cpm){c++;}}
}
}
if(window.pbv){googletag.pubads().setTargeting("pbv",window.pbv);}
googletag.pubads().setTargeting("tadbid",""+c).refresh();
});
});
}
setTimeout(function(){
sendAdserverRequest();
},3000);
var OX_dfp_options={prefetch:true};
var OX_dfp_ads=[["/1004254/com728",["728x90"],"div-gpt-com-728x90"],["/1004254/com_160",["160x600"],"div-gpt-com-160x600"]];

var googletag=googletag||{};
googletag.cmd=googletag.cmd||[];
googletag.cmd.push(function() {googletag.defineSlot('/1004254/com728',[728,90],'div-gpt-com-728x90').setTargeting('pf','053').addService(googletag.pubads());
googletag.defineSlot('/1004254/com_160',[160,600],'div-gpt-com-160x600').setTargeting('pf','053').addService(googletag.pubads());
googletag.pubads().setTargeting('ab','a').setTargeting('art','3712').setTargeting('hr','18').setTargeting('tadhb','12').setTargeting('pf','055').setTargeting('gdpr','na').enableSingleRequest();googletag.enableServices();});
</script>
<script async="" src="/common/prebidtaddesk_9.js"></script>
<link as="script" href="/common/prebidtaddesk_9.js" rel="preload"/>
<script async="async" src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<script async="async" src="//timeanddate-d.openx.net/w/1.0/jstag?nc=1004254-timeanddate"></script><link href="/site.webmanifest" rel="manifest"/><link href="/favicon-48x48.png" rel="icon" sizes="48x48" type="image/png"/><link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/><link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/><link href="/common/global_116.css" rel="stylesheet" type="text/css"/><link href="/common/tpl_banner_5.css" rel="stylesheet" type="text/css"/><link href="/common/citypages_48.css" rel="stylesheet" type="text/css"/><link href="/common/city_overview_22.css" rel="stylesheet" type="text/css"/><style>
@media only screen and (max-width: 870px) {
.nav-2__submenu, .nav-2__submenu-item {
display:none!important;
}
}
.alert.info {
margin-top: 12px;
}
</style></head><body class="tpl-banner timepage"><div class="header__wrapper" id="header__wrapper"><div class="header__inner" id="header__inner"><div class="banner" id="header"><div class="fixed"><div id="logo"><a href="/" rel="home"><img alt="timeanddate.com" src="//c.tadst.com/gfx/n/tad-logo-com3.png" title="Home page timeanddate.com"/></a></div></div></div><div id="ad-wrap"><div class="fixed" id="ad-wrap2"><div id="ad7"><div id="div-gpt-com-728x90" style="width:728px;height:90px;"><script>
googletag.cmd.push(function(){googletag.display('div-gpt-com-728x90'); });
</script>
</div></div></div></div><nav class="site-nav-bar" id="nav"><div class="site-nav-bar__inner fixed"><button aria-label="Menu" class="site-nav-bar__button site-nav-bar__button--menu" id="site-nav-menu" title="Menu"><i class="i-font i-menu"></i></button><div class="site-nav-bar__logo"><a href="/" rel="home" title="Home page timeanddate.com"><img alt="timeanddate.com" loading="lazy" src="//c.tadst.com/gfx/n/logo-mobile--com.svg"/></a></div><button aria-label="Share" class="site-nav-bar__button" onclick="modpop('/custom/shareframe.php', null, 'Share...')" title="Share"><i class="i-font i-share"></i></button><form action="/search/results.html" class="site-nav-bar__search-form"><input aria-label="Search" class="site-nav-bar__search" id="site-nav-search" name="query" placeholder="Search..." title="Search"/><button aria-label="Search" class="site-nav-bar__button site-nav-bar__button--search" id="site-nav-search-btn"><div class="site-nav-bar__search-inner"><i class="i-font i-search"></i></div></button></form><div class="site-nav-bar__menu-wrap"><div class="site-nav__header"><button class="site-nav__login" id="site-nav-login">Sign in</button><button class="site-nav__close" id="site-nav-close"><i class="i-font i-close"></i></button></div><ul class="site-nav" id="site-nav"><li class="site-nav__menu "><a class="site-nav__title" href="/">Home</a><ul class="site-nav__sub-menu"><li class="site-nav__item site-nav__item--divider site-nav__item--mob"><a class="site-nav__link" href="/">Home Page</a></li><li class="site-nav__item"><a class="site-nav__link" href="/newsletter/">Newsletter</a></li><li class="site-nav__item"><a class="site-nav__link" href="/information/">About Us</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/information/feedback.html">Contact Us</a></li><li class="site-nav__item"><a class="site-nav__link" href="/sitemap.html">Site Map</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/topics/">Our Articles</a></li><li class="site-nav__item"><a class="site-nav__link" href="/custom/">Account/Settings</a></li></ul></li><li class="site-nav__menu active"><a class="site-nav__title" href="/worldclock/">World Clock</a><ul class="site-nav__sub-menu"><li class="site-nav__item"><a class="site-nav__link" href="/worldclock/">Main World Clock</a></li><li class="site-nav__item"><a class="site-nav__link" href="/worldclock/full.html">Extended World Clock</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/worldclock/personal.html">Personal World Clock</a></li><li class="site-nav__item"><a class="site-nav__link" href="/worldclock/search.html">World Time Lookup </a></li><li class="site-nav__item"><a class="site-nav__link" href="/worldclock/timezone/utc">UTC Time</a></li></ul></li><li class="site-nav__menu "><a class="site-nav__title" href="/time/">Time Zones</a><ul class="site-nav__sub-menu"><li class="site-nav__item site-nav__item--divider site-nav__item--mob"><a class="site-nav__link" href="/time">Time Zones Home</a></li><li class="site-nav__item"><a class="site-nav__link" href="/worldclock/converter.html">Time Zone Converter</a></li><li class="site-nav__item"><a class="site-nav__link" href="/worldclock/meeting.html">International Meeting Planner</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/worldclock/fixedform.html">Event Time Announcer</a></li><li class="site-nav__item"><a class="site-nav__link" href="/time/map/">Time Zone Map</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/time/zones/">Time Zone Abbreviations</a></li><li class="site-nav__item"><a class="site-nav__link" href="/time/dst/">Daylight Saving Time</a></li><li class="site-nav__item"><a class="site-nav__link" href="/time/change/">Time Changes Worldwide</a></li><li class="site-nav__item"><a class="site-nav__link" href="/time/difference/">Time Difference</a></li><li class="site-nav__item"><a class="site-nav__link" href="/news/time/">Time Zone News</a></li></ul></li><li class="site-nav__menu "><a class="site-nav__title" href="/calendar/">Calendar</a><ul class="site-nav__sub-menu"><li class="site-nav__item site-nav__item--divider site-nav__item--mob"><a class="site-nav__link" href="/calendar/info.html">Calendars Home</a></li><li class="site-nav__item"><a class="site-nav__link" href="/calendar/">Calendar 2021</a></li><li class="site-nav__item"><a class="site-nav__link" href="/calendar/?year=2022">Calendar 2022</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/calendar/monthly.html">Monthly Calendar</a></li><li class="site-nav__item"><a class="site-nav__link" href="/calendar/create.html">Printable Calendar (PDF)</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/calendar/events/">Add Your Own Calendar Events</a></li><li class="site-nav__item"><a class="site-nav__link" href="/calendar/basic.html">Calendar Creator</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/calendar/custommenu.html">Advanced Calendar Creator</a></li><li class="site-nav__item"><a class="site-nav__link" href="/holidays/">Holidays Worldwide</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/on-this-day/">On This Day in History</a></li><li class="site-nav__item"><a class="site-nav__link" href="/calendar/months/">Months of the Year</a></li><li class="site-nav__item"><a class="site-nav__link" href="/calendar/days/">Days of the Week</a></li><li class="site-nav__item"><a class="site-nav__link" href="/date/leapyear.html">About Leap Years</a></li></ul></li><li class="site-nav__menu "><a class="site-nav__title" href="/weather/">Weather</a><ul class="site-nav__sub-menu"><li class="site-nav__item"><a class="site-nav__link" href="/weather/">Worldwide</a></li><li class="site-nav__item"><a class="site-nav__link" href="/scripts/go.php">Local Weather</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/scripts/go.php?type=hourly">Hour-by-Hour</a></li><li class="site-nav__item"><a class="site-nav__link" href="/scripts/goweather.php?type=ext">2-Week Forecast</a></li><li class="site-nav__item"><a class="site-nav__link" href="/scripts/go.php?type=historic">Past Week</a></li><li class="site-nav__item"><a class="site-nav__link" href="/scripts/go.php?type=climate">Climate</a></li></ul></li><li class="site-nav__menu "><a class="site-nav__title" href="/astronomy/">Sun &amp; Moon</a><ul class="site-nav__sub-menu"><li class="site-nav__item site-nav__item--divider site-nav__item--mob"><a class="site-nav__link" href="/astronomy">Sun &amp; Moon Home</a></li><li class="site-nav__item"><a class="site-nav__link" href="/sun/">Sun Calculator</a></li><li class="site-nav__item"><a class="site-nav__link" href="/moon/">Moon Calculator</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/moon/phases/">Moon Phases</a></li><li class="site-nav__item"><a class="site-nav__link" href="/astronomy/night/">Night Sky</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/astronomy/meteor-shower/">Meteor Showers</a></li><li class="site-nav__item"><a class="site-nav__link" href="/worldclock/sunearth.html">Day and Night Map</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/astronomy/moon/light.html">Moon Light World Map</a></li><li class="site-nav__item"><a class="site-nav__link" href="/eclipse/">Eclipses</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/live/">Live Streams</a></li><li class="site-nav__item"><a class="site-nav__link" href="/calendar/seasons.html">Seasons</a></li></ul></li><li class="site-nav__menu "><a class="site-nav__title" href="/counters/">Timers</a><ul class="site-nav__sub-menu"><li class="site-nav__item site-nav__item--mob"><a class="site-nav__link" href="/counters">Timers Home</a></li><li class="site-nav__item"><a class="site-nav__link" href="/stopwatch/">Stopwatch</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/timer/">Timer</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/countdown/create">Countdown to Any Date</a></li><li class="site-nav__item"><a class="site-nav__link" href="/countdown/newyear">New Year Countdown</a></li></ul></li><li class="site-nav__menu "><a class="site-nav__title" href="/date/">Calculators</a><ul class="site-nav__sub-menu"><li class="site-nav__item site-nav__item--divider site-nav__item--mob"><a class="site-nav__link" href="/date/">Calculators Home</a></li><li class="site-nav__item"><a class="site-nav__link" href="/date/duration.html">Date to Date Calculator (duration)</a></li><li class="site-nav__item"><a class="site-nav__link" href="/date/workdays.html">Business Date to Date (exclude holidays)</a></li><li class="site-nav__item"><a class="site-nav__link" href="/date/dateadd.html">Date Calculator (add / subtract)</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/date/weekdayadd.html">Business Date (exclude holidays)</a></li><li class="site-nav__item"><a class="site-nav__link" href="/date/weekday.html">Weekday Calculator</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/date/weeknumber.html">Week Number Calculator</a></li><li class="site-nav__item"><a class="site-nav__link" href="/worldclock/dialing.html">International Dialing Codes</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/time/travel.html">Travel Time Calculator</a></li><li class="site-nav__item"><a class="site-nav__link" href="/worldclock/distance.html">Distance Calculator</a></li><li class="site-nav__item"><a class="site-nav__link" href="/worldclock/distances.html">Distance Signpost</a></li></ul></li><li class="site-nav__menu "><a class="site-nav__title" href="/extra/">Apps &amp; API</a><ul class="site-nav__sub-menu"><li class="site-nav__item"><a class="site-nav__link" href="/ios/">iOS Apps</a></li><li class="site-nav__item"><a class="site-nav__link" href="/android/">Android Apps</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/windows/">Windows App</a></li><li class="site-nav__item"><a class="site-nav__link" href="/clocks/free.html">Free Clock</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/clocks/freecountdown.html">Free Countdown</a></li><li class="site-nav__item"><a class="site-nav__link" href="/services/api/">API for Developers</a></li></ul></li><li class="site-nav__menu "><a class="site-nav__title" href="/fun/">Free Fun</a><ul class="site-nav__sub-menu"><li class="site-nav__item site-nav__item--divider site-nav__item--mob"><a class="site-nav__link" href="/fun">Free &amp; Fun Home</a></li><li class="site-nav__item"><a class="site-nav__link" href="/clocks/free.html">Free Clock for Your Site</a></li><li class="site-nav__item"><a class="site-nav__link" href="/clocks/freecountdown.html">Free Countdown for Your Site</a></li><li class="site-nav__item"><a class="site-nav__link" href="/wordclock/">Word Clock</a></li><li class="site-nav__item"><a class="site-nav__link" href="/holidays/fun/">Fun Holidays</a></li><li class="site-nav__item"><a class="site-nav__link" href="/date/birthday.html">Alternative Age Calculator</a></li><li class="site-nav__item"><a class="site-nav__link" href="/date/pattern.html">Date Pattern Calculator</a></li><li class="site-nav__item"><a class="site-nav__link" href="/topics/fun">Fun Fact Articles</a></li></ul></li><li class="site-nav__menu site-nav__menu--my-account"><a class="site-nav__title" href="/custom/"><span class="site-nav__mob-title">My Account</span><i class="i-font i-account_circle site-nav__desktop-title"></i></a><ul class="site-nav__sub-menu"><li class="site-nav__item site-nav__item--divider site-nav__item--mob"><a class="site-nav__link" href="/custom">My Account</a></li><li class="site-nav__item"><a class="site-nav__link" href="/custom/location.html" id="popchi">My Location</a></li><li class="site-nav__item"><a class="site-nav__link" href="/custom/site.html">My Units</a></li><li class="site-nav__item"><a class="site-nav__link" href="/calendar/events/">My Events</a></li><li class="site-nav__item"><a class="site-nav__link" href="/worldclock/personal.html">My World Clock</a></li><li class="site-nav__item site-nav__item--divider"><a class="site-nav__link" href="/custom/privacy.html">My Privacy</a></li><li class="site-nav__item"><a class="site-nav__link" href="/services/">Paid Services</a></li><li class="site-nav__item"><a class="site-nav__link" href="/custom/login.html" id="poplogin">Sign in</a></li><li class="site-nav__item"><a class="site-nav__link" href="/custom/create.html" id="popreg">Register</a></li></ul></li></ul> </div><div class="site-nav-bar__blur" id="site-nav-blur"></div></div></nav></div></div><div class="po" id="po1"></div><div id="mpo"></div><div class="alert-notice__wrap" id="anw"></div><div class="dn" id="privacybanner"></div><div class="main-content-div"><script>
window.TAD=window.TAD||{};TAD.abtest='a';
</script><header class="bn-header bn-header--timepage"><div class="row socrow"><div class="fixed" id="bc"><div id="bct"><a class="fx" href="/" target="_top">Home</a>   <a class="fx" href="/time/" target="_top">Time Zones</a>   <a class="fx" href="/worldclock/" target="_top">World Clock</a>   Unknown address c0de4189-8b3a-b56-b56-af9897b0433f – Please try to...</div></div></div><div class="bn-header__wrap fixed"><div class="headline-banner "><section class="headline-banner__wrap">
<div class="headline-banner__content"><h1 class="headline-banner__title">Unknown address c0de4189-8b3a-b56-b56-af9897b0433f – Please try to search below…</h1></div></section><section class="headline-banner__extra"><form action="/worldclock/" class="bn-header__searchbox picker-city noprint"><input autocomplete="off" class="picker-city__input" name="query" onfocus="ifc(this,'ci',1,1,{&quot;pre&quot;:&quot;/worldclock/&quot;})" placeholder="Search for city or place…" type="search"/><button class="picker-city__button" title="Search" type="submit"><i class="i-font i-search"></i></button></form></section></div></div><section class="bn-header__extra"></section></header><main class="tpl-banner__main layout-grid layout-grid--sky"><article class="layout-grid__main tpl-banner__content"><section class="bk-wc bk-search" id="bk-focus"><div class="fixed"><div id="tri-focus">◢</div><div id="focus-fm"><div class="row"><div class="six columns" id="focus-fm1"><p class="h1">Search for a city's <a href="/time/dst/">current time</a>:</p><p class="fm1-p2">Find the current time, weather, sun, moon, and much more...</p></div><div class="six columns" id="focus-fm2"><form action="/worldclock/" method="get" name="f1"><div class="picker-city"><input aria-label="Place or country..." autocomplete="off" class="picker-city__input" id="wcquery" name="query" onfocus="ifc(this,'ci',1,1)" placeholder="Place or country..." type="text"/><button class="picker-city__button" title="Search" type="submit"><i class="i-font i-search"></i></button></div><p class="ct-near">Suggestions: <a href="/worldclock/china/shenzhen" title="Nearby Location">Shenzhen</a> | <a href="/worldclock/hong-kong/hong-kong" title="Nearby Location">Hong Kong</a> | <a href="/worldclock/china/kowloon" title="Nearby Location">Kowloon</a> | <a href="/worldclock/china/zhuhai" title="Nearby Location">Zhuhai</a> | <a href="/worldclock/macau/macau" title="Nearby Location">Macau</a></p></form></div></div></div></div></section><section class="fixed"></section></article><aside class="layout-grid__sky layout-grid__sky--sticky tpl-banner__sky"><fieldset id="ad1"><legend>Advertising</legend><div id="div-gpt-com-160x600" style="width:160px;height:600px;"><script>
googletag.cmd.push(function(){googletag.display('div-gpt-com-160x600'); });
</script>
</div></fieldset></aside></main><script src="/common/wcommon_140.js"></script><script src="/common/togglefullscreen_8.js"></script><script src="/common/classonhover.js"></script><script type="text/javascript">//Copyright timeanddate.com 2016, do not use without permission
(function(){it(gebtn("table"),function(a){hC(a,"tb-click")&&it(gebtn("tr",a),function(a){var b=gebtn("a",a);1<=b.length&&ael(a,"click",function(){window.location=b[0].href})})})})();
//Copyright timeanddate.com 2016, do not use without permission
(function e$$0(e,f,g){function h(b,k){if(!f[b]){if(!e[b]){var c="function"==typeof require&&require;if(!k&&c)return c(b,!0);if(a)return a(b,!0);c=Error("Cannot find module '"+b+"'");throw c.code="MODULE_NOT_FOUND",c;}c=f[b]={exports:{}};e[b][0].call(c.exports,function(a){var c=e[b][1][a];return h(c?c:a)},c,c.exports,e$$0,e,f,g)}return f[b].exports}for(var a="function"==typeof require&&require,b=0;b<g.length;b++)h(g[b]);return h})({1:[function(d,e,f){d=d(3);_T.control.add("ClassOnHover",d)},{3:3}],
2:[function(d,e,f){d(1);_T.control.applyBindingsOnLoad()},{1:1}],3:[function(d,e,f){d=function(d,e){var a=this;a._element=d;a._options=e;a._target=a._options.target||null;a._attachToTarget=a._options.attachToTarget||!1;a._classname=a._options.classname||"hover";a._target&&(a._targetElement=gf(a._target));ael(a._element,"mouseover",function(b){a._onmouseover(b)});ael(a._element,"mouseout",function(b){a._onmouseout(b)});a._attachToTarget&&(ael(a._targetElement,"mouseover",function(b){a._onmouseover(b)}),
ael(a._targetElement,"mouseout",function(b){a._onmouseout(b)}))};d.prototype={_onmouseover:function(){ac(this._element,this._classname,1);this._attachToTarget&&this._targetElement&&ac(this._targetElement,this._classname,1)},_onmouseout:function(){ac(this._element,this._classname,0);this._attachToTarget&&this._targetElement&&ac(this._targetElement,this._classname,0)}};e.exports=d},{}]},{},[2]);
aelc("chi", function() {
var text="Set Home Location for local times";
modpop('/scripts/tzq.php?type=homecity',1,text);
});
var fs=new Fullscreen("fs-clock");fs.bindTo("curclock","fs-close","full-clk", "fs-logo");aelc("fs-native",Fullscreen.toggleNativeFullscreen);
//Copyright timeanddate.com 2016, do not use without permission
aelc("fs-popup",function(){ofs(gf("fs-popup"))});aelc("extastro",function(){var a=gf("extastrorow"),b=!hC(a,"dn");ac(a,"dn",b);iH("extastro",b?"+ Show More Twilight and Moon Phase Information":"- Hide Extra Twilight and Moon Phase Information")});
</script> </div>
<footer class="footer">
<section class="feedback-bar">
<div class="fixed feedback-bar__wrap">
<section class="feedback-bar__instant-feedback" id="instant-feedback">
<span class="feedback-bar__msg">How was your experience?</span>
<span class="feedback-bar__thankyou">Thank you for your feedback!</span>
<a aria-label="Vote for having a good experience" class="feedback-bar__thumb feedback-bar__thumb--up" href="#" onclick="return blif(1)" title="Vote Good"><svg height="24" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M1 21h4V9H1v12zm22-11c0-1.1-.9-2-2-2h-6.31l.95-4.57.03-.32c0-.41-.17-.79-.44-1.06L14.17 1 7.59 7.59C7.22 7.95 7 8.45 7 9v10c0 1.1.9 2 2 2h9c.83 0 1.54-.5 1.84-1.22l3.02-7.05c.09-.23.14-.47.14-.73v-2z"></path></svg></a>
<a aria-label="Vote for having a bad experience" class="feedback-bar__thumb feedback-bar__thumb--down" href="#" onclick="return blif(0)" title="Vote Bad"><svg height="24" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h24v24H0z" fill="none"></path><path d="M15 3H6c-.83 0-1.54.5-1.84 1.22l-3.02 7.05c-.09.23-.14.47-.14.73v2c0 1.1.9 2 2 2h6.31l-.95 4.57-.03.32c0 .41.17.79.44 1.06L9.83 23l6.59-6.59c.36-.36.58-.86.58-1.41V5c0-1.1-.9-2-2-2zm4 0v12h4V3h-4z"></path></svg>
</a>
</section>
<section class="feedback-bar__contact"><a href="/information/feedback.html" id="bls1" onclick="return bls(this)">Contact Us 
<svg height="24" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z"></path><path d="M0 0h24v24H0z" fill="none"></path></svg></a></section>
</div>
</section>
<section class="fixed footer__wrap">
<section class="footer__block footer__block--promo">
<article class="footer-card">
<a class="footer-card__img-link" href="/services/supporter.html">
<img alt="Illustration of a pink shield with a white heart." class="footer-card__img" height="100" loading="lazy" src="//c.tadst.com/gfx/n/i/service__supporter.svg" width="100"/>
</a>
<section class="footer-card__content">
<h4 class="footer-card__title">
<a href="/services/supporter.html">
  Love Our Site? Become a Supporter
  </a>
</h4>
<ul class="footer-card__text">
<li>Browse our site <strong>advert free.</strong></li>
<li>Sun &amp; Moon times <strong>precise to the second.</strong></li>
<li><strong>Exclusive calendar templates</strong> for PDF Calendar.</li>
</ul>
</section>
</article>
<section class="footer__logo">
<a href="/" rel="home"><img alt="The timeanddate logo" height="88" src="//c.tadst.com/gfx/n/tad-logo-com3.png" width="170"/></a>
<p><a href="/information/copyright.html">© Time and Date AS 1995–2021</a></p>
</section>
</section>
<section class="footer__block footer__block--links-wrap">
<section class="footer__links">
<nav class="footer__links-block footer__links-block--company">
<h4>Company</h4>
<ul>
<li><a href="/company/">About us</a></li>
<li><a href="/company/jobs">Careers/Jobs</a></li>
<li><a href="/information/feedback.html" id="bls2" onclick="return bls(this)">Contact Us</a></li>
<li><a href="/information/contact.html">Contact Details</a></li>
<li><a href="/sitemap.html">Sitemap</a></li>
<li><a href="/newsletter/">Newsletter</a></li>
</ul>
</nav>
<nav class="footer__links-block footer__links-block--legal">
<h4>Legal</h4>
<ul>
<li><a href="/information/copyright.html">Link policy</a></li>
<li><a href="/information/advertising.html">Advertising</a></li>
<li><a href="/information/disclaimer.html">Disclaimer</a></li>
<li><a href="/information/terms-conditions.html">Terms &amp; Conditions</a></li>
<li><a href="/information/privacy.html">Privacy Policy</a></li>
<li><a href="/custom/privacy.html">My Privacy</a></li>
</ul>
</nav>
<nav class="footer__links-block footer__links-block--services">
<h4>Services</h4>
<ul>
<li><a href="/worldclock/">World Clock</a></li>
<li><a href="/time/">Time Zones</a></li>
<li><a href="/calendar/">Calendar</a></li>
<li><a href="/weather/">Weather</a></li>
<li><a href="/astronomy/">Sun &amp; Moon</a></li>
<li><a href="/counters/">Timers</a></li>
<li><a href="/date/">Calculators</a></li>
<li><a href="/services/api/">API</a></li>
</ul>
</nav>
<nav class="footer__links-block footer__links-block--sites">
<h4>Sites</h4>
<ul>
<li><a href="https://www.timeanddate.no">timeanddate.no</a></li>
<li><a href="https://www.timeanddate.de">timeanddate.de</a></li>
</ul>
</nav>
<section class="footer__social">
<h4>Follow Us</h4>
<div class="footer__social-icons">
<a href="https://www.facebook.com/timeanddate/"><i class="footer__social-icon footer__social-icon--facebook" title="timeanddate.com on Facebook"></i></a>
<a href="https://twitter.com/timeanddate"><i class="footer__social-icon footer__social-icon--twitter" title="timeanddate.com on Twitter"></i></a>
<a href="https://www.linkedin.com/company/time-and-date-as/about/"><i class="footer__social-icon footer__social-icon--linkedin" title="timeanddate.com on Linkedin"></i></a>
<a href="https://www.instagram.com/timeanddatecom/"><i class="footer__social-icon footer__social-icon--instagram" title="timeanddate.com on Instagram"></i></a>
<a href="https://www.youtube.com/c/timeanddate"><i class="footer__social-icon footer__social-icon--youtube" title="timeanddate.com on YouTube"></i></a>
</div>
</section>
</section>
<p class="footer__copyright">
 © Time and Date AS 1995–2020. 
 <a href="/information/copyright.html">Privacy &amp; Terms</a>
</p>
</section>
</section>
</footer>
<!-- FOOTER END -->
<script>
 
bli();main();
 
</script>
<div class="wfc" id="FBD" style="display:none"></div>
</body></html>