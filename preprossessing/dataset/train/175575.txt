<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Page not found</title>
<meta content="noindex, nofollow, noarchive, noodp, noydir" name="robots"/>
<link href="https://www.avalonvideoproductions.com/wp-content/themes/thesis_185/custom/layout.css" media="screen, projection" rel="stylesheet" type="text/css"/>
<!--[if lte IE 8]><link rel="stylesheet" href="https://www.avalonvideoproductions.com/wp-content/themes/thesis_185/lib/css/ie.css" type="text/css" media="screen, projection" /><![endif]-->
<link href="https://www.avalonvideoproductions.com/wp-content/themes/thesis_185/custom/custom.css" media="screen, projection" rel="stylesheet" type="text/css"/>
<link href="https://www.avalonvideoproductions.com/feed" rel="alternate" title="Avalon Video Productions RSS Feed" type="application/rss+xml"/>
<link href="https://www.avalonvideoproductions.com/xmlrpc.php" rel="pingback"/>
<link href="https://www.avalonvideoproductions.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.avalonvideoproductions.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.avalonvideoproductions.com/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.avalonvideoproductions.com/wp-json/" rel="https://api.w.org/"/>
<style type="text/css">#wpcf fieldset { padding: 10px; border: 1px solid #666666; width: 400px; margin: auto }
#wpcf legend { font-weight: bold: border: 1px solid #666666; padding: 3px }
#wpcf label { display: block; float: left; text-align: right; width: 140px; padding-right: 10px; font-size: 100% }
#wpcf p { margin: 0 0 7px 0 }
#wpcf .field { font-size: 100%; width: 240px; padding: 0; margin: 0 }
#wpcf p.button { text-align: right; padding: 0 5px 0 0; }
#wpcf textarea { font-size: 100%; width: 240px; height: 50px }
#wpcf .error { background-color: #FFFF00 }
#wpcf .challenge { font-size: 100%; display: inline-block; display: -moz-inline-stack; text-align: left; width: 240px }
#wpcf p.alert { color:#FF0000; font-weight: 700; text-align: center; padding: 5px 0 10px 0 }
</style></head>
<body class="custom" data-rsssl="1">
<div id="container">
<div id="page">
<div id="header">
<p id="logo"><a href="https://www.avalonvideoproductions.com">Avalon Video Productions</a></p>
<p id="tagline">Telling Your Story For Future Generations.</p>
</div>
<ul class="menu">
<li class="tab tab-home"><a href="https://www.avalonvideoproductions.com">Home</a></li>
<li class="tab tab-1"><a href="https://www.avalonvideoproductions.com/contact-us" title="Contact Us">Contact Us</a></li>
<li class="rss"><a href="https://www.avalonvideoproductions.com/feed" rel="nofollow" title="Avalon Video Productions RSS Feed">Subscribe</a></li>
</ul>
<div id="content_box">
<div id="content">
<div class="post_box top">
<div class="headline_area">
<h1>You 404’d it. Gnarly, dude.</h1>
</div>
<div class="format_text">
<p>Surfin’ ain’t easy, and right now, you’re lost at sea. But don’t worry; simply pick an option from the list below, and you’ll be back out riding the waves of the Internet in no time.</p>
<ul>
<li>Hit the “back” button on your browser. It’s perfect for situations like this!</li>
<li>Head on over to the <a href="https://www.avalonvideoproductions.com" rel="nofollow">home page</a>.</li>
<li>Punt.</li>
</ul>
</div>
</div>
</div>
<div id="sidebars">
<div class="video_box" id="multimedia_box">
<div id="video_box">
<p><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" height="315" id="FLVPlayer" width="382">
<param name="movie" value="FLVPlayer_Progressive.swf"/>
<param name="quality" value="high"/>
<param name="wmode" value="opaque"/>
<param name="scale" value="noscale"/>
<param name="salign" value="lt"/>
<param name="FlashVars" value="&amp;MM_ComponentVersion=1&amp;skinName=Halo_Skin_1&amp;streamName=AVPLogoTextMed&amp;autoPlay=false&amp;autoRewind=false"/>
<param name="swfversion" value="8,0,0,0"/>
<!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you dont want users to see the prompt. -->
<param name="expressinstall" value="../Scripts/expressInstall.swf"/>
<!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
<!--[if !IE]>-->
<object data="FLVPlayer_Progressive.swf" height="315" type="application/x-shockwave-flash" width="382">
<!--<![endif]-->
<param name="quality" value="high"/>
<param name="wmode" value="opaque"/>
<param name="scale" value="noscale"/>
<param name="salign" value="lt"/>
<param name="FlashVars" value="&amp;MM_ComponentVersion=1&amp;skinName=Halo_Skin_3&amp;streamName=AVPLogoTextMed&amp;autoPlay=true&amp;autoRewind=false"/>
<param name="swfversion" value="8,0,0,0"/>
<param name="expressinstall" value="../Scripts/expressInstall.swf"/>
<!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
<div>
<h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
<p><a href="http://www.adobe.com/go/getflashplayer"><img alt="Get Adobe Flash player" src="https://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"/></a></p>
</div>
<!--[if !IE]>-->
</object>
<!--<![endif]-->
</object></p>
</div>
</div>
<div class="sidebar" id="sidebar_1">
<ul class="sidebar_list">
<li class="widget widget_text" id="text-6"><h3>Telling Your Family Story …</h3> <div class="textwidget"><p>Ok, Ok –if you are like most folks, you have photos … LOTS of photos.  Your parents have BOXES of pictures and, maybe even film (super8, 16MM) and videos (VHS, VHS-C, Hi8).  Perhaps you also have video – taken over the years in all different formats.  What do you DO with all this?</p>
<p>We can help.  We can help you organize, digitize and create wonderfully entertaining and educational videos for your family to enjoy now and to pass along to generations to come.</p>
<p>What about current events?  Birthdays, Weddings, Bar Mitzvahs, Quincenteras, Anniversaries, Reunions – all wonderful excuses to not only capture the actual events in video but to SHOW a special video that captures all the history, emotion and wonder of the occasion.</p>
<p>Again, we can help – whether it is in advising that special Family Historian in your family to BEING your family historian we can help you to capture your story!</p>
<p>How about your child's school?  They spend more time here than at home - are you capturing their sports, their performances, or their projects?  If you are, we can help you create entertaining and educational DVDs and web pages to share; if you are not, we can capture them for you!  We work with your school to arrange to professionally record special events, programs, and performances - we can even arrange with teachers to add video to special class projects to enhance and preserve your child's educational experience!</p>
</div>
</li> </ul>
</div>
<div class="sidebar" id="sidebar_2">
<ul class="sidebar_list">
<li class="widget widget_text" id="text-5"><h3>Telling Your Business Story …</h3> <div class="textwidget"><p>What? Not sure you HAVE a business story?  Let me assure you, you do!  You have MANY stories:  stories about your Company, your Market, your Employees, your Products and mostly, about yourself.  Let your passion for what you do come through in light and sound for others to benefit and learn.</p>
<p>We can help you make videos that explain and market your product or service; that train your employees; that demonstrate your commitment to excellence and customer satisfaction and that demystify your specific value proposition to your clients.</p>
<p>These videos may be used in company meetings, the boardroom, a trade show.  They can be shown on the big Plasma or LCD screen in your reception area; shown over the internet to train remote staff or liven up your web-site (search engines just LOVE video).  They can even be used to spiff up your “Elevator Pitch” with images and music to show on your iPad, iPod, iPhone or other portable devices!</p>
<p>Need help animating your logo?  Do you want to add motion and sound to your marketing package?  We can help you use music, narration, and animation to reinforce your message and create branding that is sure to stay with your prospective customers.</p>
</div>
</li> </ul>
</div>
</div>
</div>
<div id="footer">
<p>Get smart with the <a href="http://diythemes.com/thesis/">Thesis WordPress Theme</a> from DIYthemes.</p>
<p><a href="https://www.avalonvideoproductions.com/wp-admin/">WordPress Admin</a></p>
<script src="https://www.avalonvideoproductions.com/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
</div>
</div>
</div>
<!--[if lte IE 8]>
<div id="ie_clear"></div>
<![endif]-->
</body>
</html>