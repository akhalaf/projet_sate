<!DOCTYPE html>
<html lang="en">
<head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Wazifa-e-sadat | Home :: w3layoutss</title>
<link href="css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/><!-- fontawesome -->
<link href="css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css"/><!-- Bootstrap stylesheet -->
<link href="css/style.css" media="all" rel="stylesheet" type="text/css"/><!-- stylesheet -->
<!-- meta tags -->
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="Solar Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" name="keywords"/>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //meta tags -->
<!--fonts-->
<link href="//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"/>
<!--//fonts-->
<script src="js/jquery-2.1.4.min.js" type="text/javascript"></script>
<!-- Required-js -->
<!-- main slider-banner -->
<script src="js/skdslider.min.js"></script>
<link href="css/skdslider.css" rel="stylesheet"/>
<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
			jQuery('#responsive').change(function(){
			  $('#responsive_wrapper').width(jQuery(this).val());
			});
			
		});
</script>
<!-- //main slider-banner -->
<!-- scriptfor smooth drop down-nav -->
<script>
$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');       
        }
    );
});
</script>
<!-- //script for smooth drop down-nav -->
</head>
<body>
<!-- header -->
<header>
<div class="w3layouts-top-strip">
<div class="container">
<div class="logo">
<h1><a href="index.php"><img alt="" src="images/logo56.png"/></a></h1>
</div>
<div class="w3ls-social-icons">
<a class="facebook" href="https://www.facebook.com/awswm/"><i class="fa fa-facebook"></i></a>
<a class="twitter" href="https://twitter.com/AWSWM2"><i class="fa fa-twitter"></i></a>
<a class="linkedin" href="https://www.linkedin.com/in/wazifae-sadat-b956a9142/"><i class="fa fa-linkedin"></i></a>
</div>
<div class="agileits-contact-info text-right">
<ul>
<li><span aria-hidden="true" class="glyphicon glyphicon-envelope"></span><a href="mailto:gensect@wazifaesadat.in">wazifaesadat@gmail.com</a></li>
<li><span aria-hidden="true" class="glyphicon glyphicon-earphone"></span>7505559595(IT),9997019772(HO), </li>
</ul>
</div>
<div class="clearfix"></div>
</div>
</div>
<!-- navigation -->
<nav class="navbar navbar-default">
<div class="container">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<button aria-expanded="false" class="navbar-toggle collapsed" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav">
<li><a class="active" href="index.php">HOME</a></li>
<li><a href="AboutUs.php">ABOUT ANJUMAN</a></li>
<li><a href="OFFICEBEARER.php">OFFICE BEARERS</a></li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">MEMBERS <span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="Umoomimembers.php">Umoomi Member</a></li>
<li><a href="Dawamimembers.php">Dawami Member</a></li>
<li><a href="Lifemember.php">Life Member</a></li>
</ul>
</li>
<li><a href="NAMZADFUND.php">NAMZAD FUND</a></li>
<li><a href="wazifadar.php">WAZIFADAR</a></li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">EXECUTIVE<span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="MANSABIMEMBER.php">Mansabi Member</a></li>
<li><a href="Electedmembers.php">Elected Member</a></li>
<li><a href="Mohsin-e-Anjuman.php">Mohsin-e-Anjuman</a></li>
<li><a href="Sarparst-e-Anjuman.php">Sarparst-e-Anjuman</a></li>
</ul>
</li>
<li><a href="contact.php">Contact Us</a></li>
</ul>
</div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>
<!-- //navigation -->
</header>
<!-- //header -->
<!-- top-header and slider -->
<div class="w3-slider">
<!-- main-slider -->
<ul id="demo1">
<li>
<img alt="" src="images/s4.jpg"/>
<!--Slider Description example-->
<div class="slide-desc">
</div>
</li>
<li>
<img alt="" src="images/jalal.jpg"/>
<div class="slide-desc">
</div>
</li>
<li>
<img alt="" src="images/Mohsinmeerza.jpg"/>
<div class="slide-desc">
</div>
</li>
<li>
<img alt="" src="images/yaumtasees.jpg"/>
<div class="slide-desc">
</div>
</li>
</ul>
</div>
<!-- //script for smooth drop down-nav -->
<!-- //main-slider -->
<!-- //header -->
<header>
<table width="auto"><tr><td width="50"><img height="40" src="images/bell-icon.png" width="40"/>
</td><td><marquee bgcolor="black" onmouseout="this.start();" onmouseover="this.stop();">
<font color="white" size="5" style="font-family:Calibri;">
 Important Notice :
</font><font color="white" size="5" style="font-family:Calibri;">
 The last date for submission of the form is 30 September 2020

</font><font color="white" size="5" style="font-family:Calibri;">  
.

<font color="white" size="5" style="font-family:Calibri;">
      we request to Respected Members, Fund Owners and Wazifadar if there is any change/correction/updation in your available record, Email us on wazifaesadat@gmail.com or contact to : 9997019772...
</font></font></marquee></td></tr></table></header>
<!-- //top-header and slider -->
<section class="w3-about text-center">
<div class="container">
<h2 class="w3ls_head">BECOME<span>-A-</span><class>PART</class></h2>
<p></p><h3>OF ANJUMAN WAZIFAE SADAT WA MOMINEEN</h3>
<section class="w3-about text-center">
<span> <a button="" class="btn btn-success btn-lg" href="Becomemember.php" type="button">Become Member</a>
<span> <a button="" class="btn btn-danger btn-lg" href="MakeaDonationorPayKhums.php" type="button">Make Donation or Pay Khums</a>
<span> <a button="" class="btn btn-warning btn-lg" href="createNamzadFund.php" type="button">Namzad Fund</a>
<span> <a button="" class="btn btn-primary btn-lg" href="returnofqarzehasna.php" type="button">Return of Qarz-e-Hasna</a>
<span> <a button="" class="btn btn-success btn-lg" href="http://gswportal.com/" type="button">Imamia Job Portal</a>
</span></span></span></span></span></section></div>
</section>
<!-- Specialize-section -->
<section class="w3-about text-center">
<div class="container">
<h1 class="w3ls_head">Wel<span>come</span></h1>
<div>
<div class="col-sm-12">
<div class="row">
<div class="col-sm-12">
<p><strong>Anjuman Wazifa-e-Sadat-wa-MomSneen was established in 1912 with a modest capital of Rs. 4.50 (72 Annas) by two eminent educationists Late Haji S. Jalaluddin Haider and Late Nawab S. Mohsin Meerza. The object was to award loan scholarship (Qarz-e-Hasna) to the needy, deserving and meritorious students of the community for pursuing their lower or higher studies both religious and temporal, to be returned without interest in easy instalments after the students get settled.</strong></p>
</div>
</div>
</div>
<div class="col-sm-12">
<div class="row">
<div class="col-sm-12">
<p><strong>The Anjuman has by this time benefited more than nine thousand students. Many of them have held or are holding important posts as Engineers, Doctors, Lawyers, Teachers, Administrators, Judges, Writers, Bankers, Philosophers, Alim-e-Deen and even Ministers. </strong>
</p>
</div>
</div>
</div>
<div class="col-sm-12">
<div class="row">
<div class="col-sm-12">
<p><strong>Anjuman has celebrated the centenary of the organization in the year 2012. The sources of income are membership fee from its members, nominated fund and recovery of loan from the ex-students. Anjuman is committed to reach out to each and every needy and desirous student in the form of scholarship.</strong>
</p>
</div>
</div>
</div>
<div class="col-sm-12">
<div class="row">
<div class="col-sm-12">
<p><strong>Anjuman humbly appeals to the esteemed members of the community to come forward and be part of the organization. One can start a Namzad fund; a nominated fund, in respect or memory of a loved one, or become a local secretary in your city or town or even scour needy students for the scholarship. 
We wish to receive your active support!</strong></p>
</div>
</div>
</div>
</div>
<!-- //Specialize-section -->
<!--clients-->
<!--//clients-->
<!--advantage-->
<div class="what-w3ls">
<div class="container">
<h3 class="w3ls_head">Our <span>Activities</span></h3>
<div class="what-grids">
<div class="col-md-6 what-grid">
<div class="list-img">
<img alt="" class="img-responsive" src="images/our activity.jpg"/>
<div class="textbox"></div>
</div>
</div>
<div class="col-md-6 what-grid1">
<div class="what-top">
<div class="what-left">
</div>
<div class="what-right">
<h4>Our Activities</h4>
<p><strong>The objects of the Anjuman Wazifa-e-sadat-wa-mommineen include spreading of education in the community and to provide vocational guidance to thestudents for acquiring purposeful and job oriented education. Beside this the idea behind this provision is that the scholarship holders should develop a sense of pride and self confidence, also develop a sense of respon sibility for helping their community to become self respecting. Anjuman Wazifa-e-sadat-wa-mommineen (Regd) has its own constitution and bye-laws framed under it. The highest body of the Anjuman is its Karkun Committee. Its office bearers are elected every year in its Annual Session. Anjuman Wazifa-e-sadat-wa-mommineen are 117 local secretaries of the Anjuman in various cities of India and also abroad. The local secretaries are the BACK -BONE of the Anjuman, who collect membership fees, recoverloans and enroll new members. They collect applications for scholarship and recommend them for sanction.</strong></p>
</div>
<div class="clearfix"></div>
</div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!--advantage-->
<!--stats-->
<div class="stats" id="starts">
<div class="container">
<div class="stats-info">
<div class="col-md-3 col-sm-3 stats-grid slideanim">
<div class="numscroller numscroller-big-bottom" data-delay=".5" data-increment="1" data-max="22000" data-min="21600" data-slno="1">22000</div>
<h4 class="stats-info">Umoomi</h4>
</div>
<div class="col-md-3 col-sm-3 stats-grid slideanim">
<div class="numscroller numscroller-big-bottom" data-delay=".5" data-increment="1" data-max="12000" data-min="11700" data-slno="1">12080</div>
<h4 class="stats-info">Life</h4>
</div>
<div class="col-md-3 col-sm-3 stats-grid slideanim">
<div class="numscroller numscroller-big-bottom" data-delay=".5" data-increment="10" data-max="5600" data-min="5000" data-slno="1">5600</div>
<h4 class="stats-info">Dawami</h4>
</div>
<div class="col-md-3 col-sm-3 stats-grid slideanim">
<div class="numscroller numscroller-big-bottom" data-delay=".5" data-increment="1" data-max="11586" data-min="11400" data-slno="1">11586</div>
<h4 class="stats-info">Wazifadar</h4>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!--//stats-->
<br/>
<section class="w3-about text-center">
<div class="container">
<h2 class="w3ls_head">DOWNLOAD<span>-RISALA-<span>WAZIFA</span></span></h2>
<p></p><h3>CLICK THE BUTTONS TO DOWNLOAD</h3>
<section class="w3-about text-center">
<div class="container">
<div class="col-sm-12"><a button="" class="btn btn-primary btn-lg" href="#" type="button">LATEST RISALA WAZIFA</a> <span> <div class="btn-group">
<button class="btn btn-success btn-lg" type="button">PASSED EDITIONS </button>
<button class="btn btn-success btn-lg dropdown-toggle" data-toggle="dropdown" type="button">
<span class="caret"></span>
</button>
<ul class="dropdown-menu">
<li><a href="#">2020</a></li>
<li><a href="https://drive.google.com/open?id=1PaLAAuAo8TfQEb51emEO7um8HgMZAZN_">2019</a></li>
<li><a href="https://drive.google.com/open?id=18at9tqnU0euK49iTKIhD8o6QD2H1yy6c">2018</a></li>
<ul class="dropdown-menu" role="menu">
<li><a href="https://drive.google.com/file/d/182Wm_kEnV8Sysn8oKdeQX7H_4UrVDiTe/view?usp=drivesdk">Jul -2019</a></li>
<li><a href="https://drive.google.com/open?id=1ascz0adGZmPnYP_MCxk9n2elHqubx2lw">Jun -2019</a></li>
<li><a href="https://drive.google.com/open?id=1De88rxNR3ovREG-4kgD3LN4TYSjJC0xG">May -2019</a></li>
<li><a href="https://drive.google.com/open?id=1t_oZ_fx5mldBDDpVqR0bIu8pe6FdP6i1">Apr -2019</a></li>
<li><a href="https://drive.google.com/open?id=1PdcZajHLBN9XzlweO1ft-rMQtjJE3oeJ">Mar -2019</a></li>
<li><a href="https://drive.google.com/open?id=1CSPC271BsHABGYZK6KvrUczCpmZklK4F">Feb -2019</a></li>
<li><a href="https://drive.google.com/open?id=0B_PCkD2zlDFlUG04dDI1dU1vN0p5MURhTV82cWVBWm9aS0xV">Jan -2019</a></li>
<li><a href="https://drive.google.com/open?id=19tXM_Hy48lI1CfZ82iXQKfYOYtNNIKpu">Dec -2018</a></li>
<li><a href="https://drive.google.com/open?id=1u70iXCxYR-M0ja5Q87mPDvqnB0m2e1AY">Nov -2018</a></li>
<li><a href="https://drive.google.com/open?id=1Tf-IYDiYilgG6gMJ0_ZRVGl8x2aPAc9Q">Sep&amp;Oct -2018</a></li>
<li><a href="https://drive.google.com/open?id=1qgkFbkrl5A54Jdz7b1J1Kf_GZOcOUQxx">Aug -2018</a></li>
<li><a href="https://drive.google.com/open?id=1CmEalYztKh5ENdumk7tm37SMdRD2mxcr">July -2018</a></li>
<li><a href="https://drive.google.com/open?id=15deUA1ATLyLU713VJq0ID-AVzu_4HoOe">June -2018</a></li>
<li><a href="https://drive.google.com/open?id=1XVqjM5c3CMQ3-ZEUkpe6RzLTsXk0NXSC">May -2018</a></li>
<li><a href="https://drive.google.com/open?id=15taFYnK0OcAIHK5-eadH5SLGGZ5_QZAi">Apr -2018</a></li>
<li><a href="https://drive.google.com/open?id=1QrAIAXXd3eGuSyNCD42iQx8gcGlZQlih">Mar -2018</a></li>
<li><a href="https://drive.google.com/open?id=1nz4zH8FOxr-JKwYQa8scgoysDDCb7-4L">Feb -2018</a></li>
<li><a href="https://drive.google.com/open?id=1Qft6juPi5L_Mds06RQJlZ3z0vp-kWqXM">Jan -2018</a></li>
</ul>
</ul></div> <span> <a button="" class="btn btn-danger btn-lg" href="https://drive.google.com/open?id=1ylXpbLmFEwY3r8zupCBxMi6hvSVWq-j7" type="button">WAZIFA SILVER JUBLI</a>
<span> <a button="" class="btn btn-warning btn-lg" href="https://drive.google.com/open?id=10dN8Gtw9Dgzclp7pbwpfvE86kjFcZLCZ" type="button">WAZIFA GOLDEN JUBLI</a>
<span> <a button="" class="btn btn-primary btn-lg" href="https://drive.google.com/open?id=1dQWzPbWpRf1eGiuvM55gopYiKZ74Pc1h" type="button">WAZIFA DIAMOND JUBLI</a>
</span></span></span></span></div>
</div>
</section>
<!-- footer -->
<div class="footer-top">
<div class="container">
<div class="col-md-3 w3ls-footer-top">
<h3>IMPORTANT <span>LINKS</span></h3>
<ul>
<li><a href="ANJUMAN.php">ANJUMAN AT GLANCE</a></li>
<li><a href="CORPUSMETER.php">CORPUS METER</a></li>
<li><a href="ByLaw.php">BY LAWS</a></li>
<li><a href="LOCALSECRETARIES.php">LOCAL SECRETARIES</a></li>
<li><a href="otherscholarship.php">OTHER SCHOLARSHIP SITES</a></li>
<li><a href="Membership.php">OUR MEMBERSHIP</a></li>
<li><a href="https://drive.google.com/open?id=1PTV4DlEJDJrNG8U1JLhxoMUwi28IRlN8">WAZIFA LIST 18-19</a></li>
</ul>
</div>
<div class="col-md-3 w3ls-footer-top">
<h3>PUBLI<span>CATIONS</span></h3>
<ul>
<li><a href="GUIDANCE.php">WAZIFA GUIDANCE</a></li>
<li><a href="https://drive.google.com/open?id=1H_fIJa7Yhiew78-yjk4zf35q4ikCh04B">TABLE CALANDER</a></li>
<li><a href="https://drive.google.com/open?id=0B_PCkD2zlDFlZHVhb3dDcDhaa0R6T292ZUN5Mk9BMGpHMjU4">WALL CALANDER </a></li>
<li><a href="https://qiblafinder.withgoogle.com/intl/en/desktop/finder">QIBLA FINDER</a></li>
<li><a href="photogallery.php">PHOTO GALLARY</a></li>
</ul>
</div>
<div class="col-md-3 w3ls-footer-top">
<h3>DOWN<span>LOADS</span></h3>
<ul>
<li><a href="https://drive.google.com/open?id=0B_PCkD2zlDFlUnJJOElaa0JhZ0NBRTV2bThEU2JtUXpYRldV">ELECTION RESULT</a></li>
<li><a href="https://drive.google.com/open?id=1QDEuarcIJaGQNuXubsuvoVLwC0V2qRaN">ELECTED MEMBERS</a></li>
<li><a href="https://drive.google.com/open?id=1MN9pp2V7uzraEbgcGSBdKrNNBf2tsLZA">CHANGE IN ACTs</a></li>
<li><a href="https://drive.google.com/open?id=16vL_HRuZDJ4mpvxg4VgxeXEoucaNTObY">WAZIFA APPLICATION FORM</a></li>
<li class="dropdown"><a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">FARD-E-HISAB<span class="caret"></span></a><ul class="dropdown-menu">
<li><a href="FardeHisabFormat.xlsx">DOWNLOAD IN EXCEL</a></li>
<li><a href="farde hisab formatt.pdf">DOWNLOAD IN PDF</a></li>
<li class="dropdown">
</li></ul>
</li></ul></div>
<div class="col-md-2 w3ls-footer-top">
<h3>ADD<span>RESS</span></h3>
<ul>
<li><a href="https://goo.gl/maps/T9YR4hmAZQs"> <h5>CLICK FOR DIRECTION</h5> Wazifa Manzil, AhmedNagar, Aligarh-202002(U.P.)India</a></li>
</ul>
</div>
<div class="clearfix"></div>
<div class="footer-w3layouts">
<div class="agile-copy">
<p>© 2017 Wazifa-e-sadat wa-Momineen All rights reserved | Design by <a href="https://goo.gl/maps/kjbKdhiqg4r">SMD Solutions</a></p>
</div>
</div>
</div>
</div>
<!-- //footer -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/bootstrap.js"></script>
<script src="js/numscroller-1.0.js" type="text/javascript"></script>
</div></section></div></section></body>
</html>