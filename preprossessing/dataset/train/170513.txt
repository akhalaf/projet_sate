<!DOCTYPE html>
<html lang="es">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>AulasCELCIT</title>
<link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" rel="stylesheet"/>
<script crossorigin="anonymous" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script defer="" src="https://use.fontawesome.com/releases/v5.2.0/js/all.js"></script>
<link href="/css/general.css" rel="stylesheet" type="text/css"/>
<link href="/css/contenidos-curso.css" rel="stylesheet" type="text/css"/>
<link href="/personalize/template.css" rel="stylesheet" type="text/css"/>
<link href="/personalize/favicon.ico" rel="shortcut icon"/>
<script src="/js/general.js" type="text/javascript"></script>
<script>
	getMailData();
	alertNotifications();
</script>
</head>
<body>
<nav class="navbar navbar-dark bg-dark fixed-top" id="navMain">
<div>
<!-- LOGO FOR DESKTOP-->
<a class="d-none d-sm-inline navbar-brand ml-2" href="/"><img alt="CELCIT" border="0" src="/personalize/logo.png"/></a>
<!-- LOFO FOR MOBILE -->
<a class="d-inline d-sm-none navbar-brand ml-2" href="/"><img alt="CELCIT" border="0" src="/personalize/logoMobile.png"/></a>
</div>
</nav>
<div id="main">
<div id="contenido"><div class="row">
<div class="col"></div>
<div class="col-md-4">
<h1>Acceso</h1>
<form action="" id="frmLogin" method="post" name="frmLogin">
<div class="form-group">
<div class="input-group">
<div class="input-group-prepend">
<div class="input-group-text"><i class="fas fa-at"></i></div>
</div>
<input class="form-control" id="user" maxlength="120" name="user" placeholder="Ingresá tu email" required="" type="email" value=""/>
</div>
</div>
<div class="form-group">
<div class="input-group">
<div class="input-group-prepend">
<div class="input-group-text"><i class="fas fa-key"></i></div>
</div>
<input class="form-control" id="pass" maxlength="20" name="pass" placeholder="Ingresá tu contraseña" required="" size="20" type="password"/>
</div>
</div>
<div class="row mb-1">
<div class="checkbox col-6"><label><input id="recordar" name="recordar" type="checkbox"/> Recordarme</label></div>
<div class="col-6 text-right"><a href="/recordar.php">¿Olvidaste tu contraseña?</a></div>
</div>
<button class="btn btn-primary btn-block" id="login" name="login" type="submit">Ingresar</button>
</form>
<div class="alert alert-warning mt-3">
<strong>¿Problemas para ingresar?</strong> escribinos: <a href="mailto:cursosadistancia@celcit.org.ar">cursosadistancia@celcit.org.ar</a>
</div>
</div>
<div class="col"></div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#user").focus();
	});
</script>
<div style="clear:both"></div>
<footer>
<div id="usersOnline"></div>
</footer>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
			});
</script>
</body>
</html>