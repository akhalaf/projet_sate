<html><body><div id="container">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<title>Volta al món | El blog i les fotos del nostre viatge</title>
<meta content="El blog, les fotos i tot el necessari per seguir el viatge de volta al món de l'Anna i en Jordi" name="description"/>
<meta content="volta al mon, RTW, blog, weblog, viatge, vuelta al mundo, arround the world, roundtheworld, round the world, viatge mon, viatges, viatge, volta al món, jordi i anna, fotos de viatges, fotos de viatge, fotos de viaje, fotografia viatges, fotografies viatge, fotografia viaje, any sabàtic, año sabatico, català" name="keywords"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="global" name="distribution"/>
<meta content="follow, all" name="robots"/>
<meta content="ca" name="language"/>
<meta content="IE=EmulateIE7" http-equiv="X-UA-Compatible"/>
<!-- <link rel="Shortcut Icon" href="http://www.voltaalmon.com/wp-content/themes/blue-zinfandel-3column/images/favicon.ico" type="image/x-icon" /> -->
<link href="https://www.voltaalmon.com/feed/" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="https://www.voltaalmon.com/feed/rss/" rel="alternate" title="RSS .92" type="text/xml"/>
<link href="https://www.voltaalmon.com/feed/atom/" rel="alternate" title="Atom 0.3" type="application/atom+xml"/>
<link href="https://www.voltaalmon.com/xmlrpc.php" rel="pingback"/>
<script language="JavaScript" type="text/javascript"> function setUrlValue(form) {form.url.value = document.location;} </script>
<link href="https://www.voltaalmon.com/2009/09/" rel="archives" title="setembre 2009"/>
<link href="https://www.voltaalmon.com/2009/08/" rel="archives" title="agost 2009"/>
<link href="https://www.voltaalmon.com/2009/07/" rel="archives" title="juliol 2009"/>
<link href="https://www.voltaalmon.com/2009/06/" rel="archives" title="juny 2009"/>
<link href="https://www.voltaalmon.com/2009/05/" rel="archives" title="maig 2009"/>
<link href="https://www.voltaalmon.com/2009/04/" rel="archives" title="abril 2009"/>
<link href="https://www.voltaalmon.com/2009/03/" rel="archives" title="març 2009"/>
<link href="https://www.voltaalmon.com/2009/02/" rel="archives" title="febrer 2009"/>
<link href="https://www.voltaalmon.com/2009/01/" rel="archives" title="gener 2009"/>
<link href="https://www.voltaalmon.com/2008/12/" rel="archives" title="desembre 2008"/>
<link href="https://www.voltaalmon.com/2008/11/" rel="archives" title="novembre 2008"/>
<link href="https://www.voltaalmon.com/2008/10/" rel="archives" title="octubre 2008"/>
<link href="https://www.voltaalmon.com/2008/09/" rel="archives" title="setembre 2008"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.voltaalmon.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.14"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/prototype/1.7.1.0/prototype.js?ver=1.7.1" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/scriptaculous/1.9.0/scriptaculous.js?ver=1.9.0" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/scriptaculous/1.9.0/effects.js?ver=1.9.0" type="text/javascript"></script>
<script src="https://www.voltaalmon.com/wp-content/plugins/lightbox-2/lightbox.js?ver=1.8" type="text/javascript"></script>
<link href="https://www.voltaalmon.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.voltaalmon.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.voltaalmon.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.14" name="generator"/>
<link charset="utf-8" href="https://www.voltaalmon.com/wp-content/plugins/ajax-calendar/calendar.css" media="screen" rel="stylesheet" title="no title" type="text/css"/>
<script src="https://www.voltaalmon.com/wp-content/plugins/ajax-calendar/js/microajax.js" type="text/javascript"></script>
<script charset="utf-8" type="text/javascript">
/*<![CDATA[ */
	function calendar (year,month,full)
	{
		microAjax ('https://www.voltaalmon.com/wp-content/plugins/ajax-calendar/ajax.php?full=' + full + '&month=' + month + '&year=' + year, function(response) { document.getElementById ('giraffe_calendar').innerHTML = response});
		return false;
	}
/*]]>*/
</script>
<!-- begin lightbox scripts -->
<script type="text/javascript">
    //<![CDATA[
    document.write('<link rel="stylesheet" href="https://www.voltaalmon.com/wp-content/plugins/lightbox-2/Themes/Black/lightbox.css" type="text/css" media="screen" />');
    //]]>
    </script>
<!-- end lightbox scripts -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5552656-1");
pageTracker._trackPageview();
</script><br/>
<b>Warning</b>:  ob_start(): non-static method sem_google_analytics::track_links() should not be called statically in <b>/home/voltaalm/public_html/wp-content/plugins/sem-google-analytics/sem-google-analytics.php</b> on line <b>159</b><br/>
<style media="screen" type="text/css">
	<!-- @import url( https://www.voltaalmon.com/wp-content/themes/vm/style.css ); -->
	</style>
<div id="container">
<div id="header">
<a class="home" href="http://www.voltaalmon.com" onclick="javascript:urchinTracker('/outbound/www.voltaalmon.com');"> <img alt="Volta al mon" border="0" src="https://www.voltaalmon.com/wp-content/themes/vm/images/portada.jpg"/></a>
</div> <!-- End header -->
<div id="navbar">
<ul id="navegacion">
<li id="home"> <a href="http://www.voltaalmon.com" onclick="javascript:urchinTracker('/outbound/www.voltaalmon.com');"><span>Home</span></a></li>
<li id="archive"><a href="http://www.voltaalmon.com/arxiu/" onclick="javascript:urchinTracker('/outbound/www.voltaalmon.com/arxiu/');"><span>Arxiu</span></a></li>
<li id="photo"><a href="http://www.voltaalmon.com/fotos/" onclick="javascript:urchinTracker('/outbound/www.voltaalmon.com/fotos/');"><span>Fotos</span></a></li>
<li id="maps"><a href="http://www.voltaalmon.com/mapes/" onclick="javascript:urchinTracker('/outbound/www.voltaalmon.com/mapes/');"><span>Mapes</span></a></li>
<li id="contact"><a href="http://www.voltaalmon.com/about/" onclick="javascript:urchinTracker('/outbound/www.voltaalmon.com/about/');"><span>Contacte</span></a></li>
</ul>
</div>
<div id="wrap">
<div id="content">
<!-- l_sidebar -->
<div id="l_sidebar">
<div id="Buscador">
<img alt="cercador" src="https://www.voltaalmon.com/wp-content/themes/vm/images/cercador.gif"/>
<form action="/index.php" id="searchform" method="get">
<div>
<input id="s" name="s" size="15" type="text"/>
</div>
</form>
</div>
<br/><br/>
<div id="Calendar">
<img alt="Calendari" src="https://www.voltaalmon.com/wp-content/themes/vm/images/calendari.gif"/>
<div id="giraffe_calendar">
<table id="wp-calendar">
<caption id="wp-calendar-caption">gener 2021</caption>
<thead>
<tr>
<th abbr="dilluns" scope="col" title="dilluns">dl.</th>
<th abbr="dimarts" scope="col" title="dimarts">dt.</th>
<th abbr="dimecres" scope="col" title="dimecres">dc.</th>
<th abbr="dijous" scope="col" title="dijous">dj.</th>
<th abbr="divendres" scope="col" title="divendres">dv.</th>
<th abbr="dissabte" scope="col" title="dissabte">ds.</th>
<th abbr="diumenge" scope="col" title="diumenge">dg.</th></tr></thead><tfoot><tr> <td abbr="" colspan="3" id="prev">
<a href="https://www.voltaalmon.com/2009/09/" onclick="return calendar(2009,9,0)" title="View posts for setembre 2009">
   		  « set.   		  </a></td>
<td id="showit"><a href="https://www.voltaalmon.com/2021/01/" onclick="return calendar (2021,1,1)"> «» </a></td>
<td class="pad" colspan="3" id="next"> </td>
</tr>
</tfoot>
<tbody>
<tr>
<td class="pad" colspan="4"> </td><td>1</td><td>2</td><td>3</td>
</tr>
<tr>
<td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td><td>10</td>
</tr>
<tr>
<td>11</td><td id="today">12</td><td>13</td><td>14</td><td>15</td><td>16</td><td>17</td>
</tr>
<tr>
<td>18</td><td>19</td><td>20</td><td>21</td><td>22</td><td>23</td><td>24</td>
</tr>
<tr>
<td>25</td><td>26</td><td>27</td><td>28</td><td>29</td><td>30</td><td>31</td>
</tr>
</tbody>
</table></div> </div>
<br/><br/>
<div id="Recent">
<img alt="Entrades recents" src="https://www.voltaalmon.com/wp-content/themes/vm/images/recents.gif"/>
<ul>
<li><a href="https://www.voltaalmon.com/des-de-casa/">Des de casa…</a></li>
<li><a href="https://www.voltaalmon.com/al-sostre-del-mon-everest-flotant-altura/">Flotant pel sostre del món</a></li>
<li><a href="https://www.voltaalmon.com/la-ciutat-medieval-de-bhaktapur/">La ciutat medieval de Bhaktapur</a></li>
<li><a href="https://www.voltaalmon.com/a-kathmandu/">A Kathmandú!</a></li>
<li><a href="https://www.voltaalmon.com/fins-aviat-pokhara-familia/">Fins aviat família!</a></li>
</ul>
</div>
<br/><br/>
<div id="Seguiment">
<img alt="Seguiment" src="https://www.voltaalmon.com/wp-content/themes/vm/images/seguiment.gif"/>
<br/>   <img alt="" src="https://www.voltaalmon.com/wp-content/themes/vm/images/rss.gif"/> <a href="http://feeds.feedburner.com/voltaalmon" onclick="javascript:urchinTracker('/outbound/feeds.feedburner.com/voltaalmon');" rel="alternate" target="_blank" type="application/rss+xml">Subscripció RSS</a>
<br/>   <img alt="" src="https://www.voltaalmon.com/wp-content/themes/vm/images/rss.gif"/> <a href="http://feeds2.feedburner.com/fotosvoltaalmon" onclick="javascript:urchinTracker('/outbound/feeds2.feedburner.com/fotosvoltaalmon');" rel="alternate" target="_blank" type="application/rss+xml">RSS de les fotos</a>
<br/>   <img alt="" src="https://www.voltaalmon.com/wp-content/themes/vm/images/mail.gif"/> <a href="http://www.feedburner.com/fb/a/emailverifySubmit?feedId=2430783&amp;loc=en_US" onclick="javascript:urchinTracker('/outbound/www.feedburner.com/fb/a/emailverifySubmit?feedId=2430783_amp_loc=en_US');" target="_blank">Notif. per e-mail</a>
<!--
			<br/><form name="translateUrlForm" method="POST" action="http://traductor.gencat.net/url.do" target="_blank"><input type="image" src="https://www.voltaalmon.com/wp-content/themes/vm/images/ca_es.gif" name="boto_tdium" value="" onClick="setUrlValue(this.form)"><INPUT TYPE="hidden" name="translationDirection" value="CATALAN-SPANISH"> <INPUT TYPE="hidden" value="GV" name="subjectArea"> <INPUT TYPE="hidden" NAME="MARK_ALTERNATIVES" VALUE="0"> <input type="hidden" name="url" value=""></form>
			<br/><form name="translateUrlForm" method="POST" action="http://traductor.gencat.net/url.do" target="_blank"><input type="image" src="https://www.voltaalmon.com/wp-content/themes/vm/images/ca_en.gif" name="boto_tdium" value="" onClick="setUrlValue(this.form)"><INPUT TYPE="hidden" name="translationDirection" value="CATALAN-ENGLISH"> <INPUT TYPE="hidden" value="GV" name="subjectArea"> <INPUT TYPE="hidden" NAME="MARK_ALTERNATIVES" VALUE="0"> <input type="hidden" name="url" value=""></form>
			-->
</div>
</div>
<!-- end l_sidebar -->
<div id="contentmiddle">
<div class="contenttitle">
<h1>Not Found, Error 404</h1><br/>
<p>The page you are looking for no longer exists.</p>
</div>
</div>
<!-- begin r_sidebar -->
<div id="r_sidebar">
<strong>Informació útil dels països</strong>
	Selecciona'n un!
	
	<div class="categoria" id="myanmar">
<a href="/myanmar"><img alt="Myanmar" src="https://www.voltaalmon.com/wp-content/themes/vm/images/mini_myanmar.gif"/></a>
</div>
<div class="categoria" id="tailandia">
<a href="/tailandia"><img alt="Tailàndia" src="https://www.voltaalmon.com/wp-content/themes/vm/images/mini_tailandia.gif"/></a>
</div>
<div class="categoria" id="cambodja">
<a href="/cambodja"><img alt="Cambodja" src="https://www.voltaalmon.com/wp-content/themes/vm/images/mini_cambodja.gif"/></a>
</div>
<div class="categoria" id="laos">
<a href="/laos"><img alt="Laos" src="https://www.voltaalmon.com/wp-content/themes/vm/images/mini_laos.gif"/></a>
</div>
<div class="categoria" id="vietnam">
<a href="/vietnam"><img alt="Vietnam" src="https://www.voltaalmon.com/wp-content/themes/vm/images/mini_vietnam.gif"/></a>
</div>
<div class="categoria" id="fiji">
<a href="/fiji"><img alt="Fiji" src="https://www.voltaalmon.com/wp-content/themes/vm/images/mini_fiji.gif"/></a>
</div>
<div class="categoria" id="novazelanda">
<a href="/nova-zelanda"><img alt="Nova Zelanda" src="https://www.voltaalmon.com/wp-content/themes/vm/images/mini_nz.gif"/></a>
</div>
<div class="categoria" id="xile">
<a href="/xile"><img alt="Xile" src="https://www.voltaalmon.com/wp-content/themes/vm/images/mini_xile.gif"/></a>
</div>
<div class="categoria" id="argentina">
<a href="/argentina"><img alt="Argentina" src="https://www.voltaalmon.com/wp-content/themes/vm/images/mini_argentina.gif"/></a>
</div>
<div class="categoria" id="bolivia">
<a href="/bolivia"><img alt="Bolívia" src="https://www.voltaalmon.com/wp-content/themes/vm/images/mini_bolivia.gif"/></a>
</div>
<div class="categoria" id="peru">
<a href="/peru"><img alt="Perú" src="https://www.voltaalmon.com/wp-content/themes/vm/images/mini_peru.gif"/></a>
</div>
<div class="categoria" id="brasil">
<a href="/brasil"><img alt="Brasil" src="https://www.voltaalmon.com/wp-content/themes/vm/images/mini_brasil.gif"/></a>
</div>
<div class="categoria" id="uruguay">
<a href="/uruguay"><img alt="Uruguay" src="https://www.voltaalmon.com/wp-content/themes/vm/images/mini_uruguay.gif"/></a>
</div>
<div class="categoria" id="preparatius">
<a href="/preparatius"><img alt="Preparatius" src="https://www.voltaalmon.com/wp-content/themes/vm/images/mini_preparatius.gif"/></a>
</div>
</div>
<!-- end l_sidebar -->
</div>
<!-- The main column ends  -->
<!-- begin footer -->
<div style="clear:both;"></div>
<div style="clear:both;"></div>
</div>
<div id="footer">
Copyright © 2008 • <a href="http://www.voltaalmon.com/" onclick="javascript:urchinTracker('/outbound/www.voltaalmon.com/');">Volta al món</a> • Fet amb <a href="http://www.wordpress.org" onclick="javascript:urchinTracker('/outbound/www.wordpress.org');">WordPress</a>, però sobretot amb amor i molta, molta il·lusió.
</div>
<script src="https://www.voltaalmon.com/wp-includes/js/wp-embed.min.js?ver=4.9.14" type="text/javascript"></script>
<script src="http://stats.wordpress.com/e-202102.js" type="text/javascript"></script>
<script type="text/javascript">
st_go({blog:'4628935',v:'ext',post:'0'});
var load_cmc = function(){linktracker_init(4628935,0,2);};
if ( typeof addLoadEvent != 'undefined' ) addLoadEvent(load_cmc);
else load_cmc();
</script>
</div> <!-- End Container -->
</div></body></html>