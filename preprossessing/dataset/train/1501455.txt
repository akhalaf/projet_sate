<!DOCTYPE html>
<html lang="en">
<!--rewind responsive theme-->
<head>
<meta charset="utf-8"/>
<meta content="unsafe-url" name="referrer"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="https://s3.amazonaws.com/autoprint/115/images/branding/689/logo.png" property="og:image"/>
<meta content="initial-scale=.75, width=device-width, user-scalable=yes, minimum-scale=.5, maximum-scale=2.0" name="viewport"/>
<meta content="noarchive" name="robots"/>
<title>ulinecolor.com | Page.php | Css</title>
<meta content=", ulinecolor.com" name="keywords"/>
<meta content="" name="description"/>
<link href="https://www.ulinecolor.com/css/page/	" rel="canonical"/>
<meta content="#ffffff" name="msapplication-TileColor"/>
<meta content="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" name="msapplication-TileImage"/>
<meta content="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" name="msapplication-square70x70logo"/>
<meta content="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" name="msapplication-square150x150logo"/>
<meta content="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" name="msapplication-wide310x150logo"/>
<meta content="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" name="msapplication-square310x310logo"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon" sizes="16x16" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon-precomposed" sizes="16x16" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon" sizes="32x32" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon-precomposed" sizes="32x32" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon" sizes="48x48" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon-precomposed" sizes="48x48" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon" sizes="60x60" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon-precomposed" sizes="60x60" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon" sizes="72x72" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon-precomposed" sizes="72x72" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon" sizes="96x96" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon-precomposed" sizes="96x96" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon" sizes="114x114" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon-precomposed" sizes="114x114" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon" sizes="120x120" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon-precomposed" sizes="120x120" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon" sizes="128x128" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon-precomposed" sizes="128x128" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon" sizes="144x144" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon-precomposed" sizes="144x144" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon" sizes="152x152" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="apple-touch-icon-precomposed" sizes="152x152" type="image/png"/>
<link href="https://s3.amazonaws.com/autoprint/115/images/branding/689/favicon.jpg" rel="shortcut icon" type="image/png"/>
<link href="https://autoprint-cdn.s3.amazonaws.com/themes/rewind-responsive/css/bootstrap-3.1.1.min.css?version=3.3.331" rel="stylesheet"/> <link href="https://autoprint-cdn.s3.amazonaws.com/themes/rewind-responsive/css/jquery-ui-1.10.3.css?version=3.3.331" rel="stylesheet"/> <link href="https://autoprint-cdn.s3.amazonaws.com/cart-includes/libraries/font-awesome-4.5.0/css/font-awesome.min.css?version=3.3.331" rel="stylesheet"/> <link href="https://autoprint-cdn.s3.amazonaws.com/public-cart/css/app.css?version=3.3.331" rel="stylesheet"/> <link href="https://autoprint-cdn.s3.amazonaws.com/themes/rewind-responsive/css/validation-min.css?version=3.3.331" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,regular,italic,600,600italic,700,700italic,800,800italic" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=PT+Sans:regular,italic,700,700italic" rel="stylesheet"/>
<style>
        #overDiv {
            -webkit-box-shadow: 0 0 15px 0 rgba(0, 0, 0, 0);
            -moz-box-shadow: 0 0 15px 0 rgba(0, 0, 0, 0);
            box-shadow: 0 0 15px 0 rgba(0, 0, 0, 0);
            border-radius: 6px 6px 6px 6px;
            -moz-border-radius: 6px 6px 6px 6px;
            -webkit-border-radius: 6px 6px 6px 6px;
            border: 1px solid #666;
            padding: 10px;
            background-color: #fff;
        }

        #overDiv table {
            background-color: #fff;
        }
    </style>
<script>
        var SERVER_NAME = 'https://www.ulinecolor.com/';
        var CART_SERVER_NAME = 'https://www.ulinecolor.com/';
        var CUSTOMER_SERVER_NAME = 'https://www.ulinecolor.com/';
        var SITE_CODE = '10722';
    </script>
<script>var isLoggedIn = false;</script>
<script src="https://autoprint-cdn.s3.amazonaws.com/cart-includes/libraries/jquery-3.3.1/jquery-3.3.1.min.js?version=3.3.331"></script> <script src="https://autoprint-cdn.s3.amazonaws.com/cart-includes/libraries/jquery-3.3.1/jquery-migrate-3.0.1.min.js?version=3.3.331"></script> <script src="https://autoprint-cdn.s3.amazonaws.com/cart-includes/js/jquery.1.x.support.js?version=3.3.331"></script> <script src="https://autoprint-cdn.s3.amazonaws.com/cart-includes/js/jquery-ui/jquery-ui-1.11.4.min.js?version=3.3.331"></script> <script src="https://autoprint-cdn.s3.amazonaws.com/themes/rewind-responsive/scripts/bootstrap.min.js?version=3.3.331"></script>
<!-- cms css include -->
<style>
                .sidebar {
            float: left;
            margin-right: 1%;
        }

            </style>
<link href="https://www.ulinecolor.com/theme/theme_custom.css?ajax=true&amp;portalId=&amp;version=3.3.331" media="screen" rel="stylesheet"/>
<!-- cms css include -->
<script>
        window.onload = function () {
            setTimeout('$.unblockUI();', 1000);
        };

            </script>
<style>a[href*="products/view-product-prices.html"] { display: none !important; } 
</style></head><body>
<!-- STANDARD HEADER START-->
<div class="header rounded-none">
<div class="container header-container">
<!-- HEADER LEFT START -->
<div class="header-top-group">
<div class="header-left">
<div class="logo">
<a href="https://www.ulinecolor.com/">
<img alt="ulinecolor.com" onerror="this.src='/themes/general/images/misc/no_image.gif'" src="https://s3.amazonaws.com/autoprint/115/images/branding/689/logo.png"/>
</a>
</div>
</div>
<!-- HEADER RIGHT END -->
<!-- HEADER CENTER START -->
<div class="header-center">
<div class="click-to-call" id="phone-number">
<span class="glyphicon glyphicon-earphone"></span> 1-888-900-0814                                    </div>
<!-- <div class="hours">24 hrs M-F | 8am - 5pm PT weekends</div>-->
<div class="social">
<a class="facebook" href="https://www.facebook.com/UlineColor/" rel="nofollow" target="_blank" title="Please click here to visit our facebook page"></a> <a class="instagram" href="https://www.instagram.com/ulinecolor/" rel="nofollow" target="_blank" title="Please click here to visit our Instagram page"></a> </div>
</div>
<!-- HEADER CENTER END -->
<!-- HEADER RIGHT START -->
<div class="header-right">
<div class="dropdown" id="login-nav">
<a href="https://www.ulinecolor.com/account/login.html"><i aria-hidden="true" class="fa fa-key"></i> Login/Create Profile</a>
<a class="login-last" href="https://www.ulinecolor.com/store/track-order.html">Order Status</a>
</div>
<div class="cart-links">
<span class="glyphicon glyphicon-shopping-cart"></span>
<span class="view-cart">Cart Empty!</span>
</div>
<div class="owes-money">
<a href="https://www.ulinecolor.com/orders/view-my-orders.html"></a>
</div>
</div>
<!-- HEADER RIGHT END -->
</div>
</div>
<div class="container navbar-container">
<nav class="navbar navbar-default main-menu light-accents light-borders rounded-none" role="navigation">
<div class="container-fluid">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<button class="navbar-toggle" data-target="#auto-print-nav-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand product-menu" href="#" id="product-menu">
                                                    Place Order ↓
                                            </a>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="auto-print-nav-collapse">
<ul class="nav navbar-nav">
<li><a href="https://www.ulinecolor.com/">Home</a></li>
<li><a href="https://www.ulinecolor.com/home/dashboard.html">My Profile</a></li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Services
                                            <span class="caret"></span></a>
<ul class="dropdown-menu">
<li>
<a href="https://www.ulinecolor.com/services/printing-services.html">Printing Services</a>
</li>
<li>
<a href="https://www.ulinecolor.com/services/design-services.html">Design Services</a>
</li>
<li class="divider"></li>
<li>
<a href="https://www.ulinecolor.com/services/mailing-services.html">Mailing Services</a>
</li>
<li>
<a href="https://www.ulinecolor.com/services/mailing-lists.html">Mailing Lists</a>
</li>
</ul>
</li>
<li><a href="https://www.ulinecolor.com/help/index.html">Customer Support</a></li>
</ul>
</div><!-- /.navbar-collapse -->
</div>
</nav></div><!--HEADER END-->
<script>
        // EXPANDS THE PRODUCT NAVIGATION MENU
        $(document).ready(function() {

            $(".product-menu").click(function() {
                //$(".expanded-nav").toggle();

                $("html, body").animate({scrollTop: 0}, "fast");

                $(".nav-expanded").slideToggle('fast', function() {
                    // Get an array of all element heights
                    var elementHeights = $(".nav-expanded").find($('.category-group-item')).map(function() {
                        return $(this).height();
                    }).get();

                    // Math.max takes a variable number of arguments
                    // `apply` is equivalent to passing each height as an argument
                    var maxHeight = Math.max.apply(null, elementHeights);

                    // Set each height to the max height
                    $('.category-group-item').height(maxHeight);

                    if($(this).is(":visible")){
                        $(this).css('margin-bottom', '6px');
                    } else {
                        $(this).css('margin-bottom', '0px');
                    }
                });

                $("#productCategorySearch").focus();

            });

            $('.unsetPortal').click(function() {
                swal({
                    title: "Exit Portal",
                    html: "Are you sure you would like to exit the portal?\n\n<span style='color: #CCCCCC;'>This action will be cancelled if no selection is made within <strong>30 seconds</strong>.</span>",
                    type: "question",
                    showCancelButton: true,
                    confirmButtonColor: "#5cb85c",
                    confirmButtonText: "Yes",
                    cancelButtonColor: "#d33",
                    cancelButtonText: "No",
                    timer: 30000
                }).then(function() {
                    $.ajax({
                        url: 'https://www.ulinecolor.com/canvasBase/ajaxLogoutPortal.html',
                        success: function() {
                            $(".unsetPortal").hide();
                            $.growlUI('', 'You have succesfully exited the portal!<br><br>You will now be redirected to the normal website.', 5000);

                            //remove get variables or else user may not be able to exit the portal
                            window.location = window.location.href.split("?")[0];

                            setTimeout(function() {
                                window.location.reload();
                            }, 3500);
                        }
                    });
                }, function(dismiss) {
                    // dismiss can be "cancel", "overlay", "close", "timer"
                    if (dismiss === "cancel") {
                        swal.clickCancel();
                        swal("Canceled!", "The request was cancelled!", "success");
                    } else if (dismiss === "timer") {
                        swal.clickCancel();
                        swal("Canceled!", "The request was automatically cancelled!", "success");
                    }
                });
            });
        });

    </script><!-- NAV MENU EXPANDED -->
<div class="container expanded-nav">
<div class="nav-expanded rounded-none light-accents" style="display:none;">
<div class="content-block">
<div class="input-group" id="productSearchTool">
<div class="search-tool-group" id="multiple-datasets">
<input class="form-control cat-product-search-input" id="productCategorySearch" name="productCategorySearch" placeholder="Type Here To Search Products" type="text"/>
<label for="header-search"><i aria-hidden="true" class="fa fa-search"></i><span class="sr-only">Search Products</span></label>
</div>
</div><!-- /input-group -->
<div class="alert alert-danger noCategories" role="alert">
<span class="glyphicon glyphicon-alert"></span> No products match this search
                </div>
<div id="navCatalogResult"></div>
<script>
                            $(document).ready(function() {
                                $("#navCatalogResult").load("https://www.ulinecolor.com/store/product_catalog.html?ajax=true", function() {
                                    //alert('navtest');
                                });
                            });
                        </script> </div>
</div>
</div><!-- NAV MENU EXPANDED --><!-- NO HEADER END -->
<!-- CONTAINER START -->
<div cartrestored="0" class="container" id="content">
<meta content="7; url=http://www.ulinecolor.com" http-equiv="refresh"/>
<div class="light-borders rounded-none light-accents" id="page-content">
<div id="page-title-bg"></div>
<h1 class=" light-accents light-borders rounded-none" id="page-title">404 ERROR | PAGE NOT FOUND</h1>
<div class="padded-content">
<p>We recently redesigned our website, replacing many of our pages, updating information and making it more convenient for you to find what you are looking for. It seems that the page you are looking for is one that has been replaced, moved or disabled.</p>
<p>Please visit our new
            <a href="http://www.ulinecolor.com">Home Page</a> or you will be automatically redirected.</p>
</div>
</div></div>
<!--FOOTER START-->
<div class="container footer-container">
<div class="footer-wrapper rounded-none" id="footer-wrapper">
<div class="content-block" id="footer">
<div class="category-groups-footer">
<h3 class="footer-heading">Services</h3>
<div class="products-footer">
<ul>
<li><a class="product-menu" href="#">Product Catalog</a></li>
<li>
<a href="https://www.ulinecolor.com/services/printing-services.html">Printing Services</a>
</li>
<li>
<a href="https://www.ulinecolor.com/store/request-sample.html">Request Samples</a>
</li>
<li><a class="product-menu" href="#">Place Order</a></li>
<li>
<a href="https://www.ulinecolor.com/services/design-services.html">Design Services</a>
</li>
<li>
<a href="https://www.ulinecolor.com/services/mailing-services.html">Mailing Services</a>
</li>
<li><a href="https://www.ulinecolor.com/services/mailing-lists.html">Mailing Lists</a>
</li>
</ul>
</div>
</div>
<div class="category-groups-footer">
<h3 class="footer-heading">Help</h3>
<div class="products-footer">
<ul>
<li><a href="https://www.ulinecolor.com/help/index.html">Customer Support</a></li>
<li><a href="https://www.ulinecolor.com/help/about-us.html">About Us</a></li>
<li><a href="https://www.ulinecolor.com/faq/categories.html">F.A.Q.</a></li>
<li><a href="https://www.ulinecolor.com/help/glossary.html">Glossary</a></li>
<li><a href="https://www.ulinecolor.com/help/print-specs.html">Print Specifications</a>
</li>
<li><a href="https://www.ulinecolor.com/help/testimonials.html">Testimonials</a></li>
<li><a href="https://www.ulinecolor.com/help/tutorials.html">Tutorials</a></li>
</ul>
</div>
</div>
<div class="category-groups-footer" id="quick-link-cart-footer">
<h3 class="footer-heading">Quick Links</h3>
<div class="products-footer">
<ul>
<li>
<a href="https://www.ulinecolor.com/account/login.html">Login/Create Profile</a>
</li>
<li>
<a href="https://www.ulinecolor.com/store/request-sample.html">Request Samples</a>
</li>
<li><a class="product-menu" href="#">Place Order</a></li>
<li>
<a href="https://www.ulinecolor.com/orders/custom_quote.html">Custom Quote</a>
</li>
<li>
<a href="https://www.ulinecolor.com/products/view-product-prices.html">Pricing</a>
</li> <li><a href="https://www.ulinecolor.com/home/dashboard.html">My Profile</a>
</li> </ul>
</div>
</div>
<div class="category-groups-footer legal-items">
<h3 class="footer-heading">Legal &amp; Other</h3>
<div class="products-footer">
<ul>
<li><a href="https://www.ulinecolor.com/site_map/site-map.html">Sitemap</a></li>
<li><a href="https://www.ulinecolor.com/feedback/feedback.html">Customer Feedback</a>
</li>
<li><a href="https://www.ulinecolor.com/employment/careers.html">Employment</a></li>
<li><a href="https://www.ulinecolor.com/legal/terms.html">Terms &amp; Conditions</a>
</li>
</ul>
</div>
</div>
<div class="social-icons-footer">
<span id="follow-us">Connect with Us:</span><br/>
<div class="social">
<a class="facebook" href="https://www.facebook.com/UlineColor/" rel="nofollow" target="_blank" title="Please click here to visit our facebook page"></a> <a class="instagram" href="https://www.instagram.com/ulinecolor/" rel="nofollow" target="_blank" title="Please click here to visit our Instagram page"></a> </div>
</div>
</div>
</div><!--FOOTER END-->
</div>
<div id="card-logos">
<span id="visa"></span> <span id="mastercard"></span> <span id="discover"></span> <span id="amex"></span>
<span id="paypal"></span>
</div>
<div id="sub-footer-footer">
    Copyright © 
    <script>var today = new Date();
        document.write(+today.getFullYear());</script>
            . All rights reserved.
    <a href="https://www.ulinecolor.com/legal/copyright.html">Copyright</a> &amp;
    <a href="https://www.ulinecolor.com/legal/privacy.html">Privacy &amp; Security Policy</a>
<p><br/>Powered by <a href="http://store.ulinecolor.com/" target="_blank">http://store.ulinecolor.com/</a></p></div><!-- NO FOOTER END -->
<!-- CONTINUE DESIGN IN PROGRESS Modal -->
<div aria-hidden="true" aria-labelledby="continue-modal" class="modal fade" id="continue-design-modal" role="dialog" tabindex="-1">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
<h4 class="modal-title"><strong>Shopping Cart / Design Item(s)</strong></h4>
</div>
<div class="modal-body design-in-progress"></div>
<div class="modal-footer">
<button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
</div>
</div>
</div>
</div><!-- CONTINUE DESIGN IN PROGRESS Modal -->
<!-- SELECT PORTAL Modal -->
<div aria-hidden="true" aria-labelledby="select-portal" class="modal fade" id="select-portal" role="dialog" tabindex="-1">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
<h4 class="modal-title portal-modal-title"><strong>Create / Edit Portal</strong></h4>
</div>
<div class="modal-body portal-selection-content">
<div class="canvasbaseLoader row" style="display: none">
<div class="col-md-12">
<div class="text-center">
<i class="fa fa-spinner fa-spin fa-4x"></i>
</div>
</div>
</div>
</div>
<div class="modal-footer">
<a class="btn btn-success" href="https://www.ulinecolor.com/organization/edit.html" id="create-organization-button" style="display: none;" type="button">Create New Organization</a>
<button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
</div>
</div>
</div>
</div>
<!-- SELECT PORTAL Modal -->
<script src="https://autoprint-cdn.s3.amazonaws.com/public-cart/js/app.js?version=3.3.331"></script>
<script src="https://autoprint-cdn.s3.amazonaws.com/cart-includes/libraries/sweetalert/dist/sweetalert2.min.js?version=3.3.331"></script>
<script src="https://autoprint-cdn.s3.amazonaws.com/cart-includes/js/jquery.blockUI.js?version=3.3.331"></script><script src="https://autoprint-cdn.s3.amazonaws.com/cart-includes/js/jquery.cookie.js?version=3.3.331"></script><script src="https://autoprint-cdn.s3.amazonaws.com/cart-includes/js/subdomains.js?version=3.3.331"></script>
<style>
    .tooltip {
        pointer-events: none;
        position: fixed;
    }
</style>
<script>
    var I18nNumberFormatter_locale = 'en_US';
    var I18nNumberFormatter_currency = 'USD';
</script>
<script src="https://autoprint-cdn.s3.amazonaws.com/cart-includes/libraries/I18nNumberFormatter/I18nNumberFormatter.js?version=3.3.331"></script>
<script>
    var shouldDisplayOverLib = true;
    var switchCartItemLoaded = 0;
    window.addEventListener('load', function () {
        
        $(document).ready(function () {

            
            $(function () {
                $('.tooltip-button').attr('data-toggle', 'tooltip');
                $('[data-toggle="tooltip"]').tooltip({html: true});
            });

            if ($('.organization-profile-title').text().length > 20) {
                $('.organization-profile-title').html($('.organization-profile-title').html().substring(0, 20) + '...');
            } else if ($('.organization-profile-title').text().length > 0) {
                $('.organization-profile-title').html($('.organization-profile-title').html().substring(0, 20));
            }

            var productInfoTimeout = null;
            $(document).on('mouseover', '.product-menu-link', function () {

                var categoryId = $(this).attr('categoryId');
                productInfoTimeout = setTimeout(function () {
                    shouldDisplayOverLib = true;
                    $.get('/store/product-info-box.html?ajax=true&categoryId=' + categoryId, function (data) {

                        if (shouldDisplayOverLib) {
                            overlib(data);
                        }

                    });
                }, 300);

            });
            $(document).on('mouseout', '.product-menu-link', function () {

                clearTimeout(productInfoTimeout);

            });

            $(document).on('mouseleave', '.product-menu-link', function () {

                shouldDisplayOverLib = false;
                return nd();

            });

            $(document).ready(function () {
                var tz = jstz.determine();
                var timezoneName = tz.name();

                setTimeout(function () {
                    $.cookie('userTimeZone', timezoneName, {expires: 7, path: '/'});
                }, 500);
            });
            // $('.product-menu-link').

            
            
            
            
            
            $(window).resize(function () {
                if (screen.width <= 782) {
                    $('.nav.navbar-nav').css('overflow', 'scroll');
                    $('.nav.navbar-nav').css('max-height', (screen.height - $('.navbar-header').height()));
                }
            }).resize();
        });
    });
</script>
<script>

        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        try {
            ga('create', 'UA-125133091-1', 'auto');
            ga('send', 'pageview');

        } catch (err) {
        }
    </script>
<script type="text/javascript">
    $(document).ajaxComplete(function (event, xhr, settings) {
        $('.tooltip-button').attr('data-toggle', 'tooltip');
        $('.tooltip-button').attr('data-placement', 'top');
        $('[data-toggle="tooltip"]').tooltip({html: true});
    });
</script>
</div></body>
</html>