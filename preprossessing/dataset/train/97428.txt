<!DOCTYPE html>
<html lang="en-US">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=G-BWJE0V5J2L"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-BWJE0V5J2L');
</script>
<meta charset="utf-8"/>
<title>
		    - Алимс	</title>
<link href="https://alims.gov.rs/favicon.ico" rel="shortcut icon"/>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/cupertino/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/style.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans&amp;subset=latin,cyrillic,cyrillic-ext,latin-ext" rel="stylesheet"/>
<!--[if IE]>
    <link href="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/css/ie.css" rel="stylesheet"  />
    <![endif]-->
<!--[if IE 7]>
    <link href="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/css/ie6.css" rel="stylesheet"  />
    <![endif]-->
<!--[if lt IE 7]>
    <link href="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/css/ie6.css" rel="stylesheet"  />
    <![endif]-->
<link href="https://www.alims.gov.rs/ciril/xmlrpc.php" rel="pingback"/>
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.alims.gov.rs\/ciril\/wp-includes\/js\/wp-emoji-release.min.js?ver=8b4720c183c6857ede422c3a951e6b5b"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.alims.gov.rs/ciril/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.alims.gov.rs/ciril/wp-content/plugins/jquery-collapse-o-matic/light_style.css?ver=1.6" id="collapseomatic-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.alims.gov.rs/ciril/wp-content/plugins/sitemap/css/page-list.css?ver=4.3" id="page-list-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.alims.gov.rs/ciril/wp-content/plugins/newsletter/style.css?ver=7.0.2" id="newsletter-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://www.alims.gov.rs/ciril/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://www.alims.gov.rs/ciril/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="fontResizerCookie-js" src="https://www.alims.gov.rs/ciril/wp-content/plugins/font-resizer/js/jquery.cookie.js?ver=8b4720c183c6857ede422c3a951e6b5b" type="text/javascript"></script>
<script id="fontResizer-js" src="https://www.alims.gov.rs/ciril/wp-content/plugins/font-resizer/js/jquery.fontsize.js?ver=8b4720c183c6857ede422c3a951e6b5b" type="text/javascript"></script>
<script id="fontResizerPlugin-js" src="https://www.alims.gov.rs/ciril/wp-content/plugins/font-resizer/js/main.js?ver=8b4720c183c6857ede422c3a951e6b5b" type="text/javascript"></script>
<link href="https://www.alims.gov.rs/ciril/wp-json/" rel="https://api.w.org/"/><link href="https://www.alims.gov.rs/ciril/wp-json/wp/v2/pages/2" rel="alternate" type="application/json"/><link href="https://www.alims.gov.rs/ciril/" rel="canonical"/>
<link href="https://www.alims.gov.rs/ciril/" rel="shortlink"/>
<link href="https://www.alims.gov.rs/ciril/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.alims.gov.rs%2Fciril%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://www.alims.gov.rs/ciril/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.alims.gov.rs%2Fciril%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<style id="zeno-font-resizer" type="text/css">
		p.zeno_font_resizer .screen-reader-text {
			border: 0;
			clip: rect(1px, 1px, 1px, 1px);
			clip-path: inset(50%);
			height: 1px;
			margin: -1px;
			overflow: hidden;
			padding: 0;
			position: absolute;
			width: 1px;
			word-wrap: normal !important;
		}
	</style>
<script src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/js/jquery.validate.js"></script>
<!--
<script src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/js/pixelmatrix-uniform/jquery.uniform.js" ></script>
-->
<script src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/js/supersleight.plugin.js"></script>
<link href="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/js/pixelmatrix-uniform/css/uniform.default.css" media="screen" rel="stylesheet"/>
<script src="https://www.google.com/recaptcha/api.js"></script>
</head>
<body class="home page-template-default page page-id-2">
<div id="page-wrap">
<div id="header">
<div id="top_submenu">
<span style="margin-left:215px; float:left;"><img alt="Мапа сајта" height="29" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/site_map.png" width="16"/></span>
<a href="https://www.alims.gov.rs/ciril/mapa-sajta/">Мапа сајта</a>
<span style="margin-left:13px; float:left;"><img alt="Контакт" height="29" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/contact.png" width="13"/></span>
<a href="https://www.alims.gov.rs/ciril/kontakt/">Контакт</a>
<span style="margin-left:13px; float:left;"><img alt="Помоћ" height="29" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/help.png" width="16"/></span>
<a href="https://www.alims.gov.rs/ciril/pomoc/">Помоћ</a>
<span style="margin-left:13px; float:left;"><img alt="Правила" height="29" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/disclaimer.png" width="16"/></span>
<a href="https://www.alims.gov.rs/ciril/pravila/">Правила</a>
<span style="margin: 6px 0 0 45px; font-size:10px; font-weight:500; display:inline-block; float:left;">Величина текста:</span>
<ul>
<ul class="ct-font-resizer"><li class="fontResizer ct-font-resizer-element" style="text-align: center; font-weight: bold;"><a class="fontResizer_minus ct-font-resizer-minus" href="#" style="font-size: 0.7em;" title="Decrease font size">A</a> <a class="fontResizer_reset ct-font-resizer-reset" href="#" title="Reset font size">A</a> <a class="fontResizer_add ct-font-resizer-plus" href="#" style="font-size: 1.2em;" title="Increase font size">A</a> <input id="fontResizer_value" type="hidden" value="ownelement"/><input id="fontResizer_ownid" type="hidden" value=""/><input id="fontResizer_ownelement" type="hidden" value="p"/><input id="fontResizer_resizeSteps" type="hidden" value="1.6"/><input id="fontResizer_cookieTime" type="hidden" value="31"/><input id="fontResizer_maxFontsize" type="hidden" value=""/><input id="fontResizer_minFontsize" type="hidden" value=""/></li></ul></ul>
<a href="https://www.alims.gov.rs/ciril" style="margin-left:40px; float:left; text-decoration:underline; color:#253a7d; font-weight:bold;">Ћирилица</a>
<a href="https://www.alims.gov.rs/latin" style="margin-left:18px; float:left;">Latinica</a>
<a href="https://www.alims.gov.rs/eng" style="margin-left:18px; float:left;">English</a>
</div> <!-- id="top_submenu" -->
<div id="header_wrapper">
<a href="https://www.alims.gov.rs/ciril" style="float:left;"><img alt="АЛИМС почетна" height="146" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/logo.png" width="120"/></a>
<a href="https://www.alims.gov.rs/ciril" style="float:left;"><img alt="АЛИМС почетна" height="146" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/alims_name.png" width="250"/></a>
<div id="header_search_box">
<div id="header_search_box_form">
<form action="https://www.alims.gov.rs/ciril" id="searchform" method="get">
<input class="header_form_search_text" id="s" name="s" type="text" value=""/>
<input class="header_form_search_button" id="searchsubmit" type="submit" value=""/>
</form>
</div><!-- id="header_search_box_form" -->
</div><!-- id="header_search_box" -->
<div id="link_srbija">
<a href="https://www.srbija.gov.rs/" style="float:left;" target="_blank"><img alt="Република Србија" height="146" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/serbia_logo.png" width="170"/></a>
</div>
</div>
</div>
<nav id="access" role="navigation">
<div class="menu-main_menu-container"><ul class="menu" id="menu-main_menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20361" id="menu-item-20361"><a href="https://www.alims.gov.rs/ciril/covid-19/">COVID-19</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67" id="menu-item-67"><a href="https://www.alims.gov.rs/ciril/o-agenciji/">О Aгенцији</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71" id="menu-item-71"><a href="https://www.alims.gov.rs/ciril/regulativa/">Регулатива</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11" id="menu-item-11"><a href="https://www.alims.gov.rs/ciril/lekovi/">Хумани лекови</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15" id="menu-item-15"><a href="https://www.alims.gov.rs/ciril/medicinska-sredstva/">Медицинска средства</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22" id="menu-item-22"><a href="https://www.alims.gov.rs/ciril/veterinarski-lekovi/">Ветеринарски лекови</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21" id="menu-item-21"><a href="https://www.alims.gov.rs/ciril/farmakovigilanca/">Фармаковигиланца</a></li>
</ul></div> </nav>
<!-- </header> -->
<div id="main">
<aside>
<div class="widget-area" id="secondary" role="complementary">
<div>
<img alt="informacije" border="0" height="323" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/widget_informacije.png" usemap="#Map1" width="220"/>
<map id="Map1" name="Map1">
<area alt="pacijenti" coords="1,38,219,99" href="https://www.alims.gov.rs/ciril/informacije-pacijenti-i-javnost/" shape="rect"/>
<area alt="zdravstveni" coords="1,101,219,169" href="https://www.alims.gov.rs/ciril/informacije-zdravstveni-radnici/" shape="rect"/>
<area alt="nosioci" coords="1,171,219,239" href="https://www.alims.gov.rs/ciril/informacije-nosioci-dozvola/" shape="rect"/>
<area alt="media" coords="1,241,219,307" href="https://www.alims.gov.rs/ciril/informacije-media-centar/" shape="rect"/>
</map>
</div>
<!--
    		<div style="margin-top:20px;">
           <a href="https://www.alims.gov.rs/ciril/eusluge-i-euprava/"><img src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/widget_euprava.png" width="220" height="39" alt="euprava" /></a>
           </div>
-->
<div style="margin-top:20px;">
<a href="https://www.alims.gov.rs/ciril/eusluge-i-euprava/"><img alt="euprava" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/widget_euprava-cloud-cir.png" width="220"/></a>
</div>
<!--
               		<div style="margin-top:20px;">
           <a href="https://www.alims.gov.rs/ciril/eusluge-i-euprava/"><img src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/eUprava-cir.jpg" width="220"  alt="euprava" /></a>
           </div>
-->
<div style="margin-top:10px;">
<a href="https://www.alims.gov.rs/ciril/otvoreni-podaci-alimsa/"><img alt="otvoreni_podaci" height="39" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/widget_otvoreni_podaci.png" width="220"/></a>
</div>
<div style="margin-top:20px;">
<a href="https://www.alims.gov.rs/latin/newsletter/"><img alt="newsletter" height="45" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/widget_newsletter.png" width="220"/></a>
</div>
<div style="margin-top:4px;">
<a href="https://www.alims.gov.rs/ciril/najava-radionica-i-desavanja/"><img alt="najava_radionica" height="45" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/widget_najava.png" width="220"/></a>
</div>
<div style="margin-top:20px;">
<img alt="prijava" height="214" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/widget_zelim.png" style="border: none;" usemap="#Map2" width="220"/>
<map id="Map2" name="Map2">
<area alt="humani" coords="1,42,219,79" href="https://www.alims.gov.rs/ciril/prijava-nezeljene-reakcije-na-humani-lek/" shape="rect"/>
<area alt="sredstvo" coords="1,82,219,124" href="https://www.alims.gov.rs/ciril/prijava-nezeljene-reakcije-na-medicinsko-sredstvo/" shape="rect"/>
<area alt="veterinarski" coords="1,126,219,168" href="https://www.alims.gov.rs/ciril/prijava-nezeljene-reakcije-na-veterinarski-lek/" shape="rect"/>
<area alt="status" coords="1,171,219,201" href="https://www.alims.gov.rs/ciril/status-zahteva/" shape="rect"/>
</map>
</div>
<div style="margin-top:20px;">
<a href="https://www.alims.gov.rs/ciril/najcesce-postavljana-pitanja/"><img alt="faq" height="89" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/widget_faq.png" width="220"/></a>
</div>
<div style="margin-top:20px;">
<a href="https://www.alims.gov.rs/ciril/informator-o-radu/"><img alt="informator" height="39" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/widget_informator.png" width="220"/></a>
</div>
<div style="margin-top:5px;">
<a href="https://www.alims.gov.rs/ciril/elektronska-oglasna-tabla/"><img alt="tabla" height="39" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/widget_tabla.png" width="220"/></a>
</div>
<div style="margin-top:5px;">
<a href="https://www.alims.gov.rs/ciril/anketa-o-zadovoljstvu-korisnika/"><img alt="anketa" height="56" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/widget_anketa.png" width="220"/></a>
</div>
<div style="margin-top:5px;">
<a href="https://www.alims.gov.rs/ciril/pristup-informacijama/"><img alt="pristup" height="55" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/widget_pristup.png" width="220"/></a>
</div>
<div style="margin-top:5px;">
<a href="https://www.alims.gov.rs/ciril/lista-fleksibilnih-aktivnosti"><img alt="lista" height="39" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/widget_lista.png" width="220"/></a>
</div>
</div><!-- #secondary .widget-area -->
</aside>
<div id="primary">
<div id="content" role="main">
<!-- breadcrumb -->
<!-- breadcrumb end -->
<!-- sub menu -->
<div id="sub_menu">
</div>
<!-- sub menu end -->
<article class="post-2 page type-page status-publish hentry" id="post-2">
<div class="entry-content">
<div id="sadrzaj_full">
<div id="naslovna_pretraga">
<div id="naslovna_pretraga_form">
<form>
<div id="naslovna_pretraga_form_1" style="position:relative; top:170px; left:90px;">
<select class="naslovna_form_search_select" id="selectsearch"><option value="#">Изаберите област претраживања</option><option value="https://www.alims.gov.rs/ciril/lekovi/pretrazivanje-humanih-lekova/">Хумани лекови</option><option value="https://www.alims.gov.rs/ciril/lekovi/pretrazivanje-certifikata/">Сертификати хуманих лекова</option><option value="https://www.alims.gov.rs/ciril/veterinarski-lekovi/pretrazivanje-veterinarskih-lekova/">Ветеринарски лекови</option><option value="https://www.alims.gov.rs/ciril/veterinarski-lekovi/pretrazivanje-certifikata/">Сертификати ветеринарских лекова</option><option value="https://www.alims.gov.rs/ciril/medicinska-sredstva/pretrazivanje-medicinskih-sredstava/">Медицинска средства</option><option value="https://www.alims.gov.rs/ciril/medicinska-sredstva/pretrazivanje-medicinskih-sredstava/%d1%80%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d0%b0%d1%80-%d0%bf%d1%80%d0%be%d0%b8%d0%b7%d0%b2%d0%be%d1%92%d0%b0%d1%87%d0%b0-%d0%bc%d0%b5%d0%b4%d0%b8%d1%86%d0%b8%d0%bd%d1%81%d0%ba%d0%b8%d1%85-%d1%81%d1%80/">Регистар произвођача медицинских средстава</option><option value="https://www.alims.gov.rs/ciril/medicinska-sredstva/pretrazivanje-medicinskih-sredstava/произвођачи-овлашћени-представници/">Регистар овлашћених представника произвођача медицинских средстава</option><option value="https://www.alims.gov.rs/ciril/medicinska-sredstva/pretrazivanje-veterinarskih-medicinskih-sredstava/">Ветеринарска медицинска средства</option><option value="https://www.alims.gov.rs/ciril/lekovi/pretrazivanje-odobrenih-klinickih-ispitivanja/">Одобрена клиничка испитивања</option><option value="https://www.alims.gov.rs/ciril/lekovi/pretrazivanje-uvoznih-dozvola/">Увозне дозволе</option></select>
</div>
</form>
</div>
<p><!-- id="naslovna_pretraga_form" --></p>
<div style="margin-top:2px; margin-bottom:5px; width:100%">
<img alt="" class="alignleft size-full wp-image-22477" height="100" loading="lazy" sizes="(max-width: 700px) 100vw, 700px" src="https://www.alims.gov.rs/ciril/files/2020/12/ng-700-100-cir-2021.jpg" srcset="https://www.alims.gov.rs/ciril/files/2020/12/ng-700-100-cir-2021.jpg 700w, https://www.alims.gov.rs/ciril/files/2020/12/ng-700-100-cir-2021-480x69.jpg 480w" width="700"/>
</div>
<p><!--


<div style="margin-top:20px; margin-bottom:20px; width:100%">
<a href="http://www.simpozijum-alims.rs/"><img loading="lazy" src="https://www.alims.gov.rs/ciril/files/2020/12/baner-alims.jpg" alt="" width="700" height="366" class="alignleft size-full wp-image-22347" srcset="https://www.alims.gov.rs/ciril/files/2020/12/baner-alims.jpg 700w, https://www.alims.gov.rs/ciril/files/2020/12/baner-alims-480x251.jpg 480w" sizes="(max-width: 700px) 100vw, 700px" /></a>
</div>





<div style="margin-top:20px; margin-bottom:20px; width:100%">
<a href="https://www.alims.gov.rs/ciril/farmakovigilanca/medsafetyweek/"><img loading="lazy" src="https://www.alims.gov.rs/ciril/files/2020/10/Social-media-card-2020-700x366.png" alt="" width="700" height="366" class="alignleft size-large wp-image-21986" srcset="https://www.alims.gov.rs/ciril/files/2020/10/Social-media-card-2020-700x366.png 700w, https://www.alims.gov.rs/ciril/files/2020/10/Social-media-card-2020-480x251.png 480w, https://www.alims.gov.rs/ciril/files/2020/10/Social-media-card-2020-768x402.png 768w, https://www.alims.gov.rs/ciril/files/2020/10/Social-media-card-2020-1536x804.png 1536w, https://www.alims.gov.rs/ciril/files/2020/10/Social-media-card-2020-2048x1072.png 2048w" sizes="(max-width: 700px) 100vw, 700px" /></a>
</div>


--></p>
<div style="float:left; margin-right:1px; width:170px;"> <a href="https://www.alims.gov.rs/ciril/o-agenciji/publikacije/" rel="noopener noreferrer" target="_blank"> <img alt="NRL2020" src="https://www.alims.gov.rs/ciril/files/2020/03/nrl-2020.jpg"/> </a></div>
<div style="float:left; margin-left:1px; width:170px;"><a href="https://www.alims.gov.rs/ciril/files/2019/08/NRLvet2019.pdf" rel="noopener noreferrer" target="_blank"><img alt="НРЛвет2019" src="https://www.alims.gov.rs/ciril/files/2019/08/nrl-vet-2019.jpg"/> </a></div>
<div style="float:left; margin-left:1px; width:170px;">
<div>
<a href="https://www.alims.gov.rs/latin/o-agenciji/publikacije/" rel="noopener noreferrer" target="_blank"><img alt="" class="alignleft size-full wp-image-21663" height="70" loading="lazy" src="https://www.alims.gov.rs/ciril/files/2020/09/ppl-2019-baner.jpg" width="170"/></a>
</div>
<div style="margin-top:-20px;"><a href="https://www.alims.gov.rs/ciril/files/2020/10/ppl-2019.pdf" rel="noopener noreferrer" target="_blank"><br/>
<img alt="" class="alignleft size-full wp-image-22179" height="30" loading="lazy" src="https://www.alims.gov.rs/ciril/files/2020/11/ppl-2019-pola-ms.jpg" width="170"/></a>
</div>
</div>
<div style="float:left; margin-left:1px; width:170px;">
<a href="https://www.alims.gov.rs/ciril/files/2017/02/FV6-promocija.pdf"><img alt="FV6" class="alignnone size-full wp-image-12516" height="48" loading="lazy" src="https://www.alims.gov.rs/ciril/files/2016/08/FV6-195-50-cir.jpg" width="185"/> </a><br/>
<a href="http://www.simpozijum-alims.rs/" rel="noopener noreferrer" target="_blank"> <img alt="" class="alignleft size-full wp-image-22348" height="48" loading="lazy" src="https://www.alims.gov.rs/ciril/files/2020/12/mali-baner.jpg" width="185"/></a>
</div>
<div style="min-height: 400px;">
<div class="responsive-tabs">
<h2 class="tabtitle">Вести</h2>
<div class="tabcontent responsive-tabs__panel--active">
<ul class="lcp_catlist" id="lcp_instance_0"><li><h5><a href="https://www.alims.gov.rs/ciril/2020/12/18/%d1%81%d0%b2%d0%b5%d1%87%d0%b0%d0%bd%d0%be-%d0%be%d1%82%d0%b2%d0%be%d1%80%d0%b5%d0%bd-%d0%b4%d0%b0%d1%82%d0%b0-%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80-%d1%83-%d0%ba%d1%80%d0%b0%d0%b3%d1%83%d1%98%d0%b5/" title="Свечано отворен Дата Центар у Крагујевцу и похрањени подаци АЛИМС">Свечано отворен Дата Центар у Крагујевцу и похрањени подаци АЛИМС</a></h5><span class="date_tekst">  18.12.2020.</span><div class="lcp_excerpt">Данас је председница Владе Републике Србије госпођа Ана Брнабић свечано отворила Дата Центра у Крагујевцу, у присуству највиших државних званичника, међу којима је посебно место имао директор Агенције за лекове и медицинска средства Србије (АЛИМС), Спец.др.мед. Саша Јаћовић, пошто је управо ова институција и први корисник Дата центра.
“Ово је тренутак када се покреће једна кључна ...</div></li><li><h5><a href="https://www.alims.gov.rs/ciril/2020/12/18/%d0%b2%d0%b0%d0%b6%d0%bd%d0%be-%d0%be%d0%b1%d0%b0%d0%b2%d0%b5%d1%88%d1%82%d0%b5%d1%9a%d0%b5-%d0%b2%d0%b5%d0%b7%d0%b0%d0%bd%d0%be-%d0%b7%d0%b0-%d0%b1%d1%80%d0%b7%d0%b5-%d1%82%d0%b5%d1%81%d1%82%d0%be/" title="ВАЖНО ОБАВЕШТЕЊЕ ВЕЗАНО ЗА БРЗЕ ТЕСТОВЕ НА „COVID-19“">ВАЖНО ОБАВЕШТЕЊЕ ВЕЗАНО ЗА БРЗЕ ТЕСТОВЕ НА „COVID-19“</a></h5><span class="date_tekst">  18.12.2020.</span><div class="lcp_excerpt"> Поводом примене брзих антиген тестова који се могу набавити у апотекама у Републици Србији, Агенција за лекове и медицинска средства Србије издаје следеће важно саопштење:
Сви тестови намењени за дијагностику болести „COVID-19“, па и брзи антиген тестови, које је регистровала Агенција за лекове и медицинска средства Србије, су производи са „CE“ знаком и намењени су ...</div></li></ul><a href="https://www.alims.gov.rs/ciril/category/vesti/">Прикажи још вести</a>
</div><h2 class="tabtitle">Занимљивости</h2>
<div class="tabcontent">
<ul class="lcp_catlist" id="lcp_instance_0"><li><h5><a href="https://www.alims.gov.rs/ciril/2019/07/30/%d1%81%d0%b2%d0%b5%d1%82%d1%81%d0%ba%d0%b0-%d0%b7%d0%b4%d1%80%d0%b0%d0%b2%d1%81%d1%82%d0%b2%d0%b5%d0%bd%d0%b0-%d0%be%d1%80%d0%b3%d0%b0%d0%bd%d0%b8%d0%b7%d0%b0%d1%86%d0%b8%d1%98%d0%b0-%d1%81%d0%b2/" title="Светска здравствена организација сврстала Србију међу 50 најразвијенијих земља у области регулативе лекова укључујући и вакцине">Светска здравствена организација сврстала Србију међу 50 најразвијенијих земља у области регулативе лекова укључујући и вакцине</a></h5><span class="date_tekst">  30.07.2019.</span><div class="lcp_excerpt">Национални регулаторни ауторитет Републике Србије кога чине Министарство здравља, Агенција за лекове и медицинска средства Србије и Институт за јавно здравље Србије „Др Милан Јовановић Батут”, је управо добио потврду од стране Светске здравствене организације да се може сврстати 50 најбољих на свету. Наиме, успешно је окончан дуготрајан процес детаљне провере свих параметара, чак 285 ...</div></li><li><h5><a href="https://www.alims.gov.rs/ciril/2018/09/28/%d1%82%d1%80%d0%b5%d1%9b%d0%b5-%d0%be%d0%b1%d0%b0%d0%b2%d0%b5%d1%88%d1%82%d0%b5%d1%9a%d0%b5-%d0%b8-%d0%bf%d0%be%d0%b7%d0%b8%d0%b2-%d0%bd%d0%b0-14-%d0%bc%d0%b5%d1%92%d1%83%d0%bd%d0%b0%d1%80%d0%be/" title="Треће обавештење и позив на 14. међународни годишњи симпозијум АЛИМС">Треће обавештење и позив на 14. међународни годишњи симпозијум АЛИМС</a></h5><span class="date_tekst">  28.09.2018.</span><div class="lcp_excerpt">Позивамо све заинтересоване да узму учешћа у 14. традиционалном међународном годишњем симпозијуму Агенције за лекове и медицинска средства Србије (АЛИМС) под називом “Од визије до праксе у регулативи лекова и медицинских средстава за хуману и ветеринарску примену”.
Симпозијум се одржава у периоду 5-6. октобра 2018. године у Хотелу Шумарице у Крагујевцу а АЛИМС, у сарадњи са ...</div></li></ul><a href="https://www.alims.gov.rs/ciril/category/zanimljivosti/">Прикажи још занимљивости</a>
</div><h2 class="tabtitle">Медији о нама</h2>
<div class="tabcontent">
<p><a href="https://www.alims.gov.rs/ciril/files/2020/12/Kurir-Tanjug-vakcinacija-24-12-2020.pdf"> Курир-Танјуг – ДИРЕКТОР АГЕНЦИЈЕ ЗА ЛЕКОВЕ: Фајзерова вакцина одобрена јуче, ради се на руској и кинеској, 24.12.2020</a> </p>
<hr/>
<p><a href="https://www.alims.gov.rs/mediji/24_12_2020_Tanjug_Tanjug_live_08_00_00.mp4" rel="noopener noreferrer" target="_blank"> Танјуг – Почела вакцинација у Србији, 24.12.2020</a> </p>
<hr/>
<p><a href="https://www.alims.gov.rs/mediji/22_12_2020_Happy_Dobro_jutro_Srbijo_07_38_00.mp4" rel="noopener noreferrer" target="_blank"> Happy – 22.12.2020 – Гости директор Агенције за лекове и медицинска средства Србије Спец.др.мед. Саша Јаћовић и Павле Зелић магистар фармације</a> </p>
<hr/>
<p><a href="https://www.alims.gov.rs/mediji/11_12_2020_Kurir_TV_Puls_Srbije___TV_Kurir_18_00_00.mp4" rel="noopener noreferrer" target="_blank"> Курир ТВ – 11.12.2020</a> </p>
<hr/>
<p><a href="https://www.alims.gov.rs/ciril/files/2020/12/20201217_Agencija_za_lekove_i_medicinska_sredstva_11-55-28_998_92.pdf"> Дневни лист “Курир”, 12.12.2020 – За седам дана Србија одобрава вакцину?</a> </p>
<hr/>
<p><a href="https://www.alims.gov.rs/ciril/files/2020/12/Intervju-Glasnik-LKS-novembar-2020.pdf" rel="noopener noreferrer" target="_blank">Велики интервју директора Агенције за лекове и медицинска средства Србије Спец.др.мед. Саше Јаћовића објављен у Гласнику Лекарске коморе Србије, новембар 2020</a></p>
<hr/>
<p><a href="https://www.alims.gov.rs/ciril/files/2020/11/Kurir13-11-2020.pdf" rel="noopener noreferrer" target="_blank"> Дневни лист “Курир”, 13.11.2020. – Ово је пут до безбедне вакцине против короне – Интервју Спец.др.мед. Саша Јаћовић, директор АЛИМС</a></p>
<hr/>
<p><a href="https://www.alims.gov.rs/ciril/files/2020/10/20201012_Agencija_za_lekove_i_medicinska_sredstva_12-02-05_962_35.pdf"> Дневни лист “Вечерње новости” 11.10.2020. – “Да се вакцина појави сутра, спремни смо за контролу” – Интервју Спец.др.мед. Саша Јаћовић, директор АЛИМС</a></p>
<hr/>
<p><a href="http://www.politika.rs/scc/clanak/371698/Skracene-procedure-za-pustanje-lekova-u-promet" rel="noopener noreferrer" target="_blank">Политика 10.1.2017 – Скраћене процедуре за пуштање лекова у промет – Интервју Спец.др.мед. Саша Јаћовић, директор АЛИМС</a></p>
<hr/>
<p><a href="https://www.alims.gov.rs/mediji/09_01_2017_TV_Studio_B_Beograde_dobro_jutro.avi" rel="noopener noreferrer" target="_blank"> Студио Б – Београде добро јутро од 09.01.2017.</a></p>
<hr/>
<p><a href="http://www.blic.rs/vesti/drustvo/agencija-za-lekove-donirala-novac-za-lecenje-dece-u-inistranstvu/jkxpztp" rel="noopener noreferrer" target="_blank">Блиц 4.1.2017 Агенција за лекове донирала новац за лечење деце у иностранству</a></p>
<hr/>
<p><a href="https://www.youtube.com/watch?v=iEdcGBuI0P8&amp;t=1099s" rel="noopener noreferrer" target="_blank">YouTube – Новогодишњи Информативни Програм 2017 – Еп.7.- (TV KCN) – од 0:35 – Изјава Спец.др.мед. Саша Јаћовић, директор АЛИМС</a></p>
<hr/>
<p><a href="http://www.blic.rs/vesti/drustvo/oprez-evo-koji-lekovi-izazivaju-najvise-nezeljenih-reakcija/yt8l0g4" rel="noopener noreferrer" target="_blank">Блиц 24.7.2016 ОПРЕЗ! Ево који лекови изазивају највише нежељених реакција</a></p>
<hr/>
<p><a href="http://www.rts.rs/page/tv/sr/story/20/rts-1/1508575/mira-adanja-polak-i-vi.html" rel="noopener noreferrer" target="_blank">РТС 1.2.2014 – Мира Адања Полак и Ви – Гостовање Спец.др.мед. Саша Јаћовић, директор АЛИМС</a> </p>
<hr/>
<p><a href="https://www.alims.gov.rs/ciril/files/2012/06/bilje_zdravlje.png" rel="noopener noreferrer" target="_blank">Биље и здравље – број 329 од 25.11.2016.</a></p>
<hr/>
<p><a href="https://www.alims.gov.rs/mediji/23_03_2016_RTS1_Tako_stoje_stvari.avi" rel="noopener noreferrer" target="_blank"> РТС1 Тако стоје ствари од 23.03.2016.</a></p>
<hr/>
<p><a href="https://www.alims.gov.rs/mediji/22_03_2016_RTS1_Jutarnji_program.avi" rel="noopener noreferrer" target="_blank"> РТС1 Јутарњи програм од 22.03.2016.</a></p>
<hr/>
<p><a href="https://www.alims.gov.rs/mediji/19.11.2015._RTS1_Jutarnji_program.avi" rel="noopener noreferrer" target="_blank"> РТС1 Јутарњи програм од 19.11.2015.</a></p>
<hr/>
<p><a href="https://www.alims.gov.rs/mediji/07_05_2015_Pink_Dobro_jutro.mp4" rel="noopener noreferrer" target="_blank"> ПИНК Добро јутро од 07.05.2015.</a></p>
<hr/>
<p><a href="https://www.alims.gov.rs/mediji/16.11.2011.RTS1_Beogradska_hronika.avi" rel="noopener noreferrer" target="_blank"> РТС1 Београдска хроника од 16.11.2011.</a></p>
<hr/>
</div></div></div>
</div>
<p><script type="text/javascript">
jQuery(document).ready(function () {
  jQuery("#selectsearch").change(function() {
    var $option = jQuery(this).find(':selected');
    var url = $option.val();
    if (url != "") {
      url += "?text=" + encodeURIComponent($option.text());
      window.location.href = url;
    }
  });
});
</script></p>
</div><!-- .entry-content -->
<footer class="entry-meta">
</footer><!-- .entry-meta -->
<!-- #post-2 -->
</div><!-- #content -->
</article></div><!-- #primary -->
<div id="footer">
<div id="footer_wrapper">
<hr/>
<img alt="АЛИМС" height="70" src="https://www.alims.gov.rs/ciril/wp-content/themes/Alims_cir/images/logo_footer.png" width="100"/>
<p class="footer_naslov">Агенција за лекове и медицинска средства Србије</p>
<p>
Војводе Степе 458, 11221 Београд, Србија<br/>
Факс +381 11 3951 131, +381 11 3951 147<br/>
Презентација: <a href="https://www.alims.gov.rs">www.alims.gov.rs</a>, Општи и-мејл: <a href="mailto:hygia@alims.gov.rs">hygia@alims.gov.rs</a><br/>Вебмастер: <a href="mailto:tatjana.stojadinovic@alims.gov.rs">Вебмастер</a></p>
<p>
© 2005 - 2021 Copyright АЛИМС
</p>
</div> <!-- id="footer_wrapper" -->
</div>
</div>
<script type="text/javascript">
var colomatduration = 'fast';
var colomatslideEffect = 'slideFade';
var colomatpauseInit = '';
var colomattouchstart = '';
</script><link href="https://www.alims.gov.rs/ciril/wp-content/plugins/tabby-responsive-tabs/css/tabby.css?ver=1.2.3" id="tabby-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.alims.gov.rs/ciril/wp-content/plugins/tabby-responsive-tabs/css/tabby-print.css?ver=1.2.3" id="tabby-print-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-ui-core-js" src="https://www.alims.gov.rs/ciril/wp-includes/js/jquery/ui/core.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.alims.gov.rs\/ciril\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://www.alims.gov.rs/ciril/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="collapseomatic-js-js" src="https://www.alims.gov.rs/ciril/wp-content/plugins/jquery-collapse-o-matic/js/collapse.js?ver=1.6.23" type="text/javascript"></script>
<script id="zeno_font_resizer_cookie-js" src="https://www.alims.gov.rs/ciril/wp-content/plugins/zeno-font-resizer/js/js.cookie.js?ver=1.7.6" type="text/javascript"></script>
<script id="zeno_font_resizer_fontsize-js" src="https://www.alims.gov.rs/ciril/wp-content/plugins/zeno-font-resizer/js/jquery.fontsize.js?ver=1.7.6" type="text/javascript"></script>
<script id="google-recaptcha-js" src="https://www.google.com/recaptcha/api.js?render=6LdCtr4ZAAAAAA00zrHa6ChzLF0uofPuCCljc-t3&amp;ver=3.0" type="text/javascript"></script>
<script id="wpcf7-recaptcha-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7_recaptcha = {"sitekey":"6LdCtr4ZAAAAAA00zrHa6ChzLF0uofPuCCljc-t3","actions":{"homepage":"homepage","contactform":"contactform"}};
/* ]]> */
</script>
<script id="wpcf7-recaptcha-js" src="https://www.alims.gov.rs/ciril/wp-content/plugins/contact-form-7/modules/recaptcha/script.js?ver=5.3.2" type="text/javascript"></script>
<script id="jquery-ui-tabs-js" src="https://www.alims.gov.rs/ciril/wp-includes/js/jquery/ui/tabs.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.alims.gov.rs/ciril/wp-includes/js/wp-embed.min.js?ver=8b4720c183c6857ede422c3a951e6b5b" type="text/javascript"></script>
<script id="tabby-js" src="https://www.alims.gov.rs/ciril/wp-content/plugins/tabby-responsive-tabs/js/tabby.js?ver=1.2.3" type="text/javascript"></script>
<script>jQuery(document).ready(function($) { RESPONSIVEUI.responsiveTabs(); })</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-3280354-1', 'auto');
  ga('send', 'pageview');

</script>
<script>
 jQuery(document).ready(function($) {
 jQuery('#tabs').tabs();
});
  </script>
</div> <!-- main -->
</div></body>
</html>
