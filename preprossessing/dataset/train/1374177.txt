<html><body><p>ï»¿<!DOCTYPE HTML>

</p>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="musicoterapia, estimulacion musical, musica terapia, Stefanie Fleddermann" name="keywords"/>
<meta content="Musicoterapia y EstimulaciÃ³n musical" name="description"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<title>Stefanie Fleddermann</title>
<link href="http://www.stefaniefleddermann.cl/imagenes/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700,400italic,700italic,300italic" rel="stylesheet" type="text/css"/>
<link href="estilos.css" rel="stylesheet" type="text/css"/>
<link href="fuentes.css" rel="stylesheet" type="text/css"/>
<link href="responsive.css" rel="stylesheet" type="text/css"/>
<link href="responsiveslides.css" rel="stylesheet" type="text/css"/>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-49940758-1', 'xtrweb.com');
  ga('send', 'pageview');

</script>
<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="responsiveslides.min.js"></script>
<script>
    // You can also use "$(window).load(function() {"
    $(function () {

      // Slideshow 1
      $(".rslides").responsiveSlides({
        
        speed: 800
      });

    });
  </script>
<div id="linea"></div>
<div id="cajaheader">
<div id="logo"><h1>Stefanie Fleddermann</h1></div>
</div>
<div id="menucontenedor">
<div id="menucaja">
<a class="menu" href="index.html"><div id="boton1">Inicio</div></a>
<a class="menu" href="quiensoy.html"><div id="boton2">Quien soy</div></a>
<a class="menu" href="equipo.html"><div id="boton6">Equipo</div></a>
<a class="menu" href="musicoterapia.html"><div id="boton3">Musicoterapia</div></a>
<a class="menu" href="estimulacion.html"><div id="boton4">EstimulaciÃ³n Musical Temprana</div></a>
<a class="menu" href="contacto.html"><div id="boton2">Contacto</div></a>
</div>
</div>
<a class="menu" href="index.html"><div class="fondo5"><div class="menumovil"> Inicio</div>
</div></a>
<a class="menu" href="quiensoy.html"><div class="fondo4"><div class="menumovil">Quien soy</div>
</div></a>
<a class="menu" href="equipo.html"><div class="fondo6"><div class="menumovil">Equipo</div>
</div></a>
<a class="menu" href="musicoterapia.html"><div class="fondo1"><div class="menumovil">Musicoterapia</div>
</div></a>
<a class="menu" href="estimulacion.html"><div class="fondo2"><div class="menumovil">EstimulaciÃ³n Musical Temprana</div>
</div></a>
<a class="menu" href="contacto.html"><div class="fondo6"><div class="menumovil">Contacto</div>
</div></a>
<div class="contenedor-ancho">
<ul class="rslides">
<li><a href="musicoterapia.html"><img alt="" src="imagenes/slider03.jpg"/></a></li>
<li><img alt="" src="imagenes/slider02.jpg"/></li>
<li><img alt="" src="imagenes/slider01.jpg"/></li>
</ul>
</div>
<div class="contenedor-ancho">
<div class="contenedor940">
<div class="caja-textocentrado texto-gris">
<h1 class="texto-gris">Bienvenidos</h1>
			Este es un espacio para la Musicoterapia, Psicoterapia y EducaciÃ³n Musical y otras actividades relacionadas, que buscan promover el bienestar &amp; la calidad de vida.
			Fragmentos sonoros de diversos colores e intensidades que se unen en una obra mayor, en la que cada tono, cada silencio y cada disonancia encuentran su lugar y su sentido.
		</div>
</div>
</div>
<footer>
<div id="cajafoot"><h3>Stefanie Fleddermann</h3>Musicoterapia - EstimulaciÃ³n musical | <a class="linkcorreo" href="mailto:estimulacionmusicalchile@gmail.com">estimulacionmusicalchile@gmail.com</a> Santiago de Chile, 2017 | DiseÃ±o web: <a href="http://mateobolson.cl/" target="_blank">MateoBolsÃ³n</a>
</div>
</footer>
<div id="linea"></div>
</body></html>