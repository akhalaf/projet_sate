<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "97250",
      cRay: "610934c2feeb19fd",
      cHash: "55c415d79aa9d9e",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cudHppbWFzbWVhdC5nci90bXAvYm9va21hcmsvaWkucGhwJTA5JTBB",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "TQIf997a40eU44y1oYzekc9PE2AqEH88yDXhFzH/u2kYhjLH3Zso8eRQTUa/FwDTCztcS8GGAv37gHRB1UcMKLPQZwLYzK3zr6ezm+396GJXxZJf5rYgGHhjv0SmnaiRIZHM9vC/qiHXYkKkFU281GYeA1XyZEl8zIVu5sJ2dh1ZAYNubonClXY1w5cGqsJVtrgaOmcwqI2IKVCYNt2eOP9eErmrBfRub9muKLzepvTJOt/fTrs1wW4bUQmZSpeUEBm1npriES3zT49/FQqhqxXyKcqT3X67Mt+qPA6zgun5yGb7F8TLVZYH7SGLZTtPtr9vx+MAzVVv2OE2IVSf8nkOIOpHq52D8vduw3y95RAiaBczvi1IGkHw/xOzOqUOZVhem+SDTb52iPZrfxuvsv5Hlyncg2xeF+ybNaF2IdUj0xJxrxLdqs7YM5MvePdxB3FqJIiQD2Au/kHCQ5FrAnyVo0aS/hCoA3XZhbaSLiQ1Tbis/a5PIngvrP6JVNQ5CTktEGnfkVUMWsW/ES2S++bYDK/ELVhEvgKh44uMDa4YgHGMhI7vMQ6+y2uwL5e4FzJJfUedfUP79zWwIhGnpe4BK2MjDsW4eUFTLYMeSuuweOfPOKiOZcr39TAhqVju2S9qhvAAoaiWI/IG5Pve8lueNauQj/HgOJ6mC3T0CcLYn4wAk44zD8hHMNtrADGfsMSGiziDX9vVv/4injKHi2nVS78iteb2ztZOts66NdVI/Yu7WCyyZbwJ1kzCqPya",
        t: "MTYxMDQ3ODkyNS4yNzkwMDA=",
        m: "ldXXdPq7RwQ6U2Oq5KxqRp7vNPvEafEUopCxrZIAOG4=",
        i1: "ZYyEFQt1UQO4HNOI6R9/Bg==",
        i2: "tDoORDjE4XOcxU+6tQ3pLQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "vjcu5rH3tzV9dWVndm+eLAP3Z0yeOeF3Efd/pcDB0SU=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.tzimasmeat.gr</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/tmp/bookmark/ii.php%09%0A?__cf_chl_captcha_tk__=c87f6be580f8ca73f3d38c6691494db18a4c8355-1610478925-0-AdD4CDBgwycTFRnPtGnjzYzgFzHhXzXv59NXz1g3hAopeq6iDslTe8h2uYWmZcwhHX02z6WzYgpF6qsOoQ6L-JLVu3r1rgJeg39pfQCszFTaP8rNyxTC23xIwtXggfFDUzhrKuVflHkddfUfwxLa5knVaLYRCP44sVST6R5Ju0-6t-Ng908Wb5YQeC3-wo9cxWOPeg2z_IiThAxptdPVKo3WP6qMm_Q0Bgb1bptk9Qa6aHjkRChO3SloAlG8Mh1pcJRGQkMd9uVhN2XodtgcJTNhSVOTqcbTxc5r__wUEqiuTLcZxdSANmCMvwQHt1RN7iU96rmTHyjIRO0Odlh88oKFaS23DRqdwi9MPB29e6HTdTQi995hcVm8MVhQBkyqBAq3p4OTkpykqPXq-n1c6EB6g8A54HRqsa38ssVsLpehyEuR2UNrCShGvL-Bs13wokGobaqX7TPfN-SLFUfVCqn1BQHcYiemN_Nwmd7D0gpgFuvpHMytrDrwGJF01OptmmQHOyk7fEKkF7ixbNzk9RPNStLC3g_tFSQdbDRN9fza9maXcKF8wIdDoXrFXNuIqg" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="0a58f518406f9b6a2cf33d5b9c27ac4fd72632e3-1610478925-0-AfU1iEMEhTsSRR1Bbmo6JHCsp2AhUS1043yxSIka5D4b9cMkM9FQ2Cjohlpe0j6+yvyQHLXUT5zOLuTk0MteG4KiKnUlSetHOZNkWVZjzwwHQ/8ZQBJq8ilvEF2w0JCuZTGB83Zieul7ehyZ/6cSW6fgUnb91NqeL0raTT4Jw9xS3PwtDWWJN/nvCIWZdQzlOmq/AqiOalOIGIcZfqQxrN4vWq/CQuj6FQybjFsKMsugNbvkQI57a0BQsOn8OQjjvpctfcSkwBL2BjijGMe04Yk86NodyKpqGES7EXYzatwPBnl28LMeGp6RPSilLDCjKf/0H0NML8DpHG1XsQmWkq+UAX3C1zBWm/lTh0tprxAgHleyRueDbS7pfRycRZj4JFw8QQuM08lfy00MhzULEAeC8YftzHJGHcfp7zCU+kUQFj5to0RFvdBLP63tR61VP9ekZJobnPmtMRaLG5YCn9IRK5J/aY6XNpPJbQy5/y144NbuS3mniR8gLzbvlglVXhixyX4XFl13fMi5nUSZ9Qi4BolxtCqnMw0WAKItk0wp9hW7ti78SgGmKU/4/QdHxz0fPRqYwIAz1yrRi+pLdF+6eW2cMTY6V2LqE/Kb/4v8noRLQgrgPf6O4LVtdktI7fNVulDPASbKGrCR/Ub8sL0gZ2QOjk/JdbirA8umxnkISDJZ9Hx/fnTjUWXOaZHep+9QCD7GcrCzIZwNA5hiM6OfZmAvNW/nLK+RmBZkSiSmkaHgCcJ9qj7mPLo+Lpqr9YUkUL1Ynn09436vSxovSwNr1U/rCng7mBGdZNFMg3/RSpyViBpkDemJuTjc6Q28QQicFbTARYhDiqdCNk/cmF+2T6jKVaR9PTPIpcnkgR4C8Tm11+jTXmr59hBrqIofcGyrytqUlizHlFoY5OzzawIAuYPNZtG5bKe8RdPAvpdMKe7TMnx6B/r8PPNnhBdR1XFQGJibUkPv4lQ6OCPBky9yLsJnc0o1NsZOEzu7MHy/XEaR5RUejf4nw315+bK3D6+tKleHThOk5TSetPlu4r1/lxhdDvP6dhofnCuH3qQ5W+iZv18Qruhajobzkd60aIaTLmMwYYOc2cZKKUrUXjkCzb4x0KVunawrCt0qKEYvEB2M0aq4IVEM+52vMX1wM6+o7OjT+MWrSmdam+axjNzv5OKGe3DHNe/xWejQ2fZbZxQHb6Rz7hrWxCoyjo1h8xDVXc2Iwc1EnrYg3Q4oysOSiS8n5+hyBUMJOpvN7WO5J8J0/bzglYzEJFyrQwpD30N9r/GX2m2kxr1gQr2QicnTXtxjgUVN/Cn7xx0eGVUlIohlSDEAhEwzJ801CogSRw=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="6e3ab0cb059a03df069683de51f04b99"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610934c2feeb19fd')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<a href="https://nhs-foi.com/aberrantgrump.php?more=5"></a>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610934c2feeb19fd</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
