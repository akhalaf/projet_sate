<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "32010",
      cRay: "610bd838fa24d1b7",
      cHash: "ad932256cedae60",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYWR5b3UubWUv",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "n8RsM/bR/klSsFB9g1sBKSzHn6V+3qnZvrgaaS0rBjY6Ds/OW7NspJBwM2owlCudvj6LNvJ9EMPwAIxTKKZc/bczyIyG0Osb46TOpNeU+yj/YpV6NGjJq++QjUC1xc7BOBb4TCSXw1dIyYyEbGhmaqYHldTErk51T983lGPq/aHreCgIoUQW3sCpgF9EwIEkz4tC9WnIMD+UBizg0y0HZ9wuKf6aIbM0cN08fwMEHsIt4eoIJfElFBNME84k68SNn+dERUTwSpPq2PIeqG6IKryctlKu9+8EQd+d60RsoOaXq5Y90PTzMAbUYgaaSf7o1JQRiVIGfk00cVgOCHBhn8rGL76KQ3mvcneUoVgwIeaVziKPY2VsZGXr+6ZT1juNQro871uFddGiomXJ8ul1FTydY8VhiGQxkECttxEcgNKs54Yr5UnN6ddn25eKWlIsS8B2iNIuoLxugbL0j6EIxNmbF0/wQ5UdojKHi/FXph8C1e1jwZOBjIe5jW6u2N0Y1tD9vdhdES9yj6zZV6eFwuRxXReyUm15iBpl6Es+1KVC07yLbPj0nqCitqFSOed8kqRjqrRG6N85RvmivM4SkpbUCBdyKCYC01Wb2d/CXHxYVCyL2hQATYjxkkto8t7+DnPvxhl9l+XbuUV8cbxR9SGl4K0vIQKW4qw4/Olihj7gr50ucs6+XwTRGbB9HDkTQTDThyLgo49xwmUD2HtAbE0T5qunSs2A6XpZZqkTw+oRzdsOHgz6DDcAF50Uw6w0",
        t: "MTYxMDUwNjU5Mi4xNzEwMDA=",
        m: "i3teDAzSNQgP59A4JMjqjQIVGV3qpky865Z4jN9RQTY=",
        i1: "KGYNBbSeY8CBx4lujr0C0g==",
        i2: "Wj6nKW13H7U3O4zH7BlBYw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "UvgOF0Qwq+LvioCfMK1OprYBq4d/DetLpLx7hUaSpoc=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.adyou.me</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=596f411274c6c8ffa90e61831916d9e504a7c18d-1610506592-0-AYB9CZBpngnKCjDC5_85agEQcI4QVy4g41PrDth8vNuwx5MKvT7jkdvwIZ-SWQ92KKHSIvkyMZxFH2YyLh74kbAh8yKMFkVqHdm2lzabQaC9WI0yoARcHcCL7k4fnMvfxZFpMMmKaHhj6p5xQOxjNcOHzdmq0Bb9VyeBJe640cmNG7D6o8dsHUQR1sshPCLQimo2AteH2GjsKjxT6AKeY_hwMyooAWoY5X-t_FhjoDAPZHZeqQGOLJ0iT1tztrsRZFgI25GfRzUMTcX9UC2tEgdNIw35dh--gagM9PHHk7HuRA24wi9ZfWxoDhdUVqqJROU35DB59DHNmOTbDgkffZznlRlOKBQFyOobmN7dhKUC5KxtlrZeFhKridRpt2loU3yJw8Bq1kFP7jdxcWKcalKZtVMKU4thl-YIEhhJkmHHjhkcGsNCxHcxjbhTiRKs6t1vrXiV0ywjvHUsNaHZxeASZIOkhtbu5dTV3LyuZCQSbm-M3cxPxRqXLytzPxF9OKwQ9nro1CnR-yq_zRAXB3Y" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="f27b835a99dea3f195713de19d8bcd63297627d0-1610506592-0-AROb60Jn+hqbLfHvQeW0kObvMHL1Ep/0JNgjnL9x+W4XffWYPh/lCt+8vk9j7CbY9AvPEuZgem5QAScVcK2kU2aI3zUeZhoAToMDoLcwxWsX6QPAsQ/iElVgntQBqGptAGyN/xP5OqDxdAbWnq91eD3BW6V4AW6m/O5H5DMrrGp8Yr9S/q3bCoEX/YfHKDZv6lPChOXzM5CzRDk4KhMiCVqMMw1vjLgFGWgU3Li5t1knC41CJRuLmxLQZFZag8RpSc+KDePgpck7/pFfkLtpZPwB7Akkd9/NOKEI0/BBl6rACkd/SvriAPOJ0xLxqlo6MJhqL2I1H+j1YuNHLWnZLIJGejVNzsWkPPNkb8sb1q247K7cIO984pGuEOyhQITqSosAv/2d1ApIT4zpNWnRlAef5RoZKK7lGUvfZ0ofNR21azBOQy8aG1MU3kQxzNtbou4ZJE6UOhL+apDAOhHSYXvLfIEKv/P3plgQRQd6/xr/fJ1UyzE8lbLLpDgS+fr9hMORAlDnr4KRwgF0BDbbQ7eKfU6qq54cwnrSTVMoL1wfExKV9MtjycIjhiJPwozYh+9M9aqYxUj4Pow4roN01gA2sqVIBp+afqTYMReADqcwg15309+yHmnq2MjxYLjqrPvet/83T/WNrQypXPAZ0LcDm+2Rx2MlRw52YkjJyiNrQMdj0kEDwxc07MS+mNUR/cAG1+dMb3Yv7pfXyyf9DHplgJ0M8vs4UNJfPIGmWd/9SkPYHR9Tx/w9sejYm/OiI89r3H/h20H36Uk8l5vWGZauvkkkJ3SrmbCkHFHAH3lcyWjPScplcUL75Wo43xa1emECvQIxwmONmEa+YIw/4itQo7VH+IO0SiN4YH6EZR5DfylcZmaRR6pqyBnHUAfRR82c/wR2y9LLTcDKu7UNoS6AZ1+KZKKQfHaj3lv9L+3mYgZmdJephYBQMX3TXJuIduy14psKUOdUEDq0XoS/2F53UqvLPHRUGPtl2qlZykJ0TB6mxdZE4vMTY2ewG89PjP9U32TwB7MA3KKUVP8ZXAqQBFctGxoj5YkJIswG8OQ343ItuFfIe8C7172jdsWkbGuXXGUEsER105AmYxFU3cPJjkVWI1VebJAvC+DRENxi8n4LiaebNffFmQatydUEGLmW1R8nUFzstgreTr0Zehem53wNAJaeUaeiObWbhKh88oObQ8baJY2Fo0DwqRb2utMvqWDFjhByfa/ILS61xReHR6EgbXbsx6mdtGgmMckPMfYQCmlCWq/SXY/7oETxW4Q0W8JXJ0oW8Jm5ikwlcYiTcDPYQ16M52l10gWkN6sS"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="06cc6b5da9ba87b34c50c513881e87db"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610bd838fa24d1b7')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<a href="https://chattard.com/grandfather.php?sid=6"><!-- table --></a>
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610bd838fa24d1b7</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
