<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<base href="https://wpxhosting.com/"/>
<title>WordPress Hosting: WPX Hosting</title>
<meta content="https://wpxhosting.com/" property="og:url"/>
<meta content="I Recommend WPX Hosting" property="og:title"/>
<meta content="Two thumbs up - I recently switched to WPX Hosting and recommend their speed, service and security - they do know what they are talking about when it comes to WordPress hosting." property="og:description"/>
<link href="https://cf.wpxhosting.com/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="https://cf.wpxhosting.com/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<style>
        body {
            color: #fff;
            background: linear-gradient(-45deg, #E14324, #23A6D5);
        }

        h1,
        h6 {
            font-family: 'Montserrat', sans-serif;
            font-weight: 400;
            font-size: 35px;
            text-align: center;
            position: absolute;
            top: 10%;
            right: 0;
            left: 0;
            padding-left: 50px;
            padding-right: 50px;
        }
        a {
            color: #fff;
            text-align: center;
        }

    </style>
</head>
<body>
<h1>
<img class="img-responsive" src="https://cf.wpxhosting.com/img/WPX_Logo_Icon_White.png" style="width: 200px;"/> <br/>
         
        <br/>

       You have reached the default host page of a server at WPX Hosting <br/>
         
        <br/>

            The most probable reason for seeing this message is some type of mistake in your request.
            Perhaps you are trying to access a domain that is not available on this server or an existing domain via https with no SSL enabled.
            Click <b><a href="https://wpxhosting.com/knowledgebase/">here</a></b> for more help, or send us a support ticket <b><a href="https://wpxhosting.com/tickets/new/">here</a></b> .
        <br/>
         
        <br/>
        WPX Hosting - Yes We Do Offer The World's <b><a href="https://wpxhosting.com">Best WordPress Hosting</a></b>
</h1>
<img class="img-responsive" src="https://stat.wpxhosting.com/stat.png" style="width: 0px;"/>
</body>
</html>
