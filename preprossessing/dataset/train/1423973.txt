<!DOCTYPE html>
<!-- BEGIN CURRENT html --><html lang="es" prefix="og: http://ogp.me/ns#">
<head>
<title>Página no encontrada - Tengo un trato</title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://www.tengountrato.com/feed/" rel="alternate" title="Tengo un trato RSS Feed" type="application/rss+xml"/>
<link href="https://www.tengountrato.com/xmlrpc.php" rel="pingback"/>
<!--[if !IE]><!--><script>if (/*@cc_on!@*/false) { document.documentElement.className+=' lt-ie10'; }</script><!--<![endif]-->
<!--[if lt IE 9]><!--><script>if (/*@cc_on!@*/false) { document.documentElement.className+=' lt-ie9'; }</script><!--<![endif]-->
<style>
.isotope-item img { -webkit-filter: saturate(150%); }.post .entry-content-media .post-thumb { background-color:#FF584C; } a.hover-off:hover img, .post .entry-content-media .post-thumb a:hover img { opacity: .2; }
body,#theme-wrapper{background-color:#ffffff;}p,body,a:hover,.theme-tagline p a:hover{color:#202125;}.bean-tabs .bean-tab,.bean-toggle .target{color:#202125!important;}a,.cats,blockquote,.author-tag,.entry-title a:hover,.comment-meta a:hover,.comment-author a:hover,.entry-meta a:hover{color:#FF584C;}.logo h1:hover,li.submit .button,.archives-list li,.bean-panel-title > a,#respond input[type="submit"],.widget_bean_tweets .follow-link:hover{color:#FF584C!important;}.btn,.button,.new-tag,.tagcloud a,button.button,.letter-logo a,div.jp-play-bar,.btn[type="submit"],input[type="button"],input[type="reset"],input[type="submit"],.button[type="submit"],div.jp-volume-bar-value,.isotope-item .hover-off{background-color:#FF584C;}.bean-quote,.featurearea_icon .icon{background-color:#FF584C!important;}#isotope-container li a div.overlay h4,#isotope-container li a div.overlay h4 span{color:#FFF;}#isotope-container li a div.overlay{background-color:#202125;}
</style>
<!-- This site is optimized with the Yoast SEO plugin v14.4.1 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Página no encontrada - Tengo un trato</title>
<meta content="noindex, follow" name="robots"/>
<meta content="es_ES" property="og:locale"/>
<meta content="Página no encontrada - Tengo un trato" property="og:title"/>
<meta content="Tengo un trato" property="og:site_name"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://www.tengountrato.com/#website","url":"https://www.tengountrato.com/","name":"Tengo un trato","description":"Tengountrato","potentialAction":[{"@type":"SearchAction","target":"https://www.tengountrato.com/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"es"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link crossorigin="" href="https://fonts.gstatic.com" rel="preconnect"/>
<link href="https://www.tengountrato.com/feed/" rel="alternate" title="Tengo un trato » Feed" type="application/rss+xml"/>
<link href="https://www.tengountrato.com/comments/feed/" rel="alternate" title="Tengo un trato » Feed de los comentarios" type="application/rss+xml"/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.10.4 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script data-cfasync="false" type="text/javascript">
	var mi_version         = '7.10.4';
	var mi_track_user      = true;
	var mi_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-64366683-1';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-64366683-1', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('send','pageview','/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer);
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Función desactivada __gaTracker(' + arguments[0] + " ....) porque no estás siendo rastreado. " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<!-- / Google Analytics by MonsterInsights -->
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.tengountrato.com/wp-includes/css/dist/block-library/style.min.css?ver=87655f933fb4d10471c037be37528bbf" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tengountrato.com/wp-content/plugins/asesor-cookies-para-la-ley-en-espana/html/front/estilos.css?ver=87655f933fb4d10471c037be37528bbf" id="front-estilos-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tengountrato.com/wp-content/plugins/bean-shortcodes/assets/bean-shortcodes.css?ver=1.0" id="bean-shortcodes-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tengountrato.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.9" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tengountrato.com/wp-content/themes/koi/style.css?ver=87655f933fb4d10471c037be37528bbf" id="parent-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tengountrato.com/wp-content/themes/koi/assets/css/mobile.css?ver=87655f933fb4d10471c037be37528bbf" id="parent-mobile-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tengountrato.com/wp-content/themes/koi-child/style.css?ver=1.0" id="main-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tengountrato.com/wp-content/themes/koi-child/assets/css/mobile.css?ver=1.0" id="mobile-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto%3A400%2C500%2C700&amp;ver=87655f933fb4d10471c037be37528bbf" id="roboto-css" media="all" rel="stylesheet" type="text/css"/>
<script>if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script><script type="text/javascript">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[]","home_url":"https:\/\/www.tengountrato.com","hash_tracking":"false"};
/* ]]> */
</script>
<script src="https://www.tengountrato.com/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.10.4" type="text/javascript"></script>
<script src="https://www.tengountrato.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.tengountrato.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var cdp_cookies_info = {"url_plugin":"https:\/\/www.tengountrato.com\/wp-content\/plugins\/asesor-cookies-para-la-ley-en-espana\/plugin.php","url_admin_ajax":"https:\/\/www.tengountrato.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script src="https://www.tengountrato.com/wp-content/plugins/asesor-cookies-para-la-ley-en-espana/html/front/principal.js?ver=87655f933fb4d10471c037be37528bbf" type="text/javascript"></script>
<link href="https://www.tengountrato.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.tengountrato.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.tengountrato.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<link href="https://www.tengountrato.com/wp-content/plugins/microkids-related-posts/microkids-related-posts-default.css" rel="stylesheet" type="text/css"/><script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		if (window.wfLogHumanRan) { return; }
		window.wfLogHumanRan = true;
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//www.tengountrato.com/?wordfence_lh=1&hid=672D1C8B08D14474A3C0DDCEC52A662E');
</script><link href="https://www.tengountrato.com/wp-content/uploads/2015/06/tengo-un-trato-logo.jpg" rel="icon" sizes="32x32"/>
<link href="https://www.tengountrato.com/wp-content/uploads/2015/06/tengo-un-trato-logo.jpg" rel="icon" sizes="192x192"/>
<link href="https://www.tengountrato.com/wp-content/uploads/2015/06/tengo-un-trato-logo.jpg" rel="apple-touch-icon"/>
<meta content="https://www.tengountrato.com/wp-content/uploads/2015/06/tengo-un-trato-logo.jpg" name="msapplication-TileImage"/>
</head>
<body class="error404 unknown" data-rsssl="1">
<!-- MARCA -->
<div id="theme-wrapper">
<header>
<nav class="show-for-small" id="mobile-nav">
<div class="menu"><ul>
<li class="page_item page-item-713"><a href="https://www.tengountrato.com/barcelona-seguimiento-semana-actual/">Barcelona – Seguimiento semana actual</a></li>
<li class="page_item page-item-6103"><a href="https://www.tengountrato.com/declaracion-de-privacidad/">Declaración de privacidad</a></li>
<li class="page_item page-item-492"><a href="https://www.tengountrato.com/">Home</a></li>
<li class="page_item page-item-719"><a href="https://www.tengountrato.com/seguimiento-en-barcelona/">Seguimiento en Barcelona</a></li>
<li class="page_item page-item-497 page_item_has_children"><a href="https://www.tengountrato.com/servicios/">Servicios</a>
<ul class="children">
<li class="page_item page-item-526 page_item_has_children"><a href="https://www.tengountrato.com/servicios/street-marketing/">Street Marketing</a>
<ul class="children">
<li class="page_item page-item-528"><a href="https://www.tengountrato.com/servicios/street-marketing/pegada-de-carteles/">Pegada de carteles</a></li>
<li class="page_item page-item-534"><a href="https://www.tengountrato.com/servicios/street-marketing/reparto-de-flyers/">Reparto de flyers, CDs, etc…</a></li>
<li class="page_item page-item-532"><a href="https://www.tengountrato.com/servicios/street-marketing/distribuciones/">Distribución Publicaciones</a></li>
</ul>
</li>
<li class="page_item page-item-696 page_item_has_children"><a href="https://www.tengountrato.com/servicios/guerrilla-marketing/">Guerrilla Marketing</a>
<ul class="children">
<li class="page_item page-item-530"><a href="https://www.tengountrato.com/servicios/guerrilla-marketing/vinilos-y-pegatinas/">Vinilos y pegatinas</a></li>
<li class="page_item page-item-698"><a href="https://www.tengountrato.com/servicios/guerrilla-marketing/stands/">Festivales y Stands</a></li>
<li class="page_item page-item-700"><a href="https://www.tengountrato.com/servicios/guerrilla-marketing/acciones-especiales/">Acciones especiales</a></li>
</ul>
</li>
<li class="page_item page-item-694"><a href="https://www.tengountrato.com/servicios/otros/">Otros</a></li>
</ul>
</li>
<li class="page_item page-item-501"><a href="https://www.tengountrato.com/clientes/">Clientes</a></li>
<li class="page_item page-item-1608 page_item_has_children"><a href="https://www.tengountrato.com/distribuciones/">Distribuciones</a>
<ul class="children">
<li class="page_item page-item-1593"><a href="https://www.tengountrato.com/distribuciones/distribuciones-en-barcelona/">Distribuciones en Barcelona</a></li>
<li class="page_item page-item-1598"><a href="https://www.tengountrato.com/distribuciones/distribuciones-en-madrid/">Distribuciones en Madrid</a></li>
</ul>
</li>
<li class="page_item page-item-503"><a href="https://www.tengountrato.com/quienes-somos/">Quiénes somos</a></li>
<li class="page_item page-item-21 current_page_parent"><a href="https://www.tengountrato.com/blog/">Blog</a></li>
<li class="page_item page-item-20"><a href="https://www.tengountrato.com/contacto/">Contacto</a></li>
<li class="page_item page-item-507"><a href="https://www.tengountrato.com/aviso-legal/">Aviso legal</a></li>
<li class="page_item page-item-509"><a href="https://www.tengountrato.com/cookies/">Cookies</a></li>
</ul></div>
</nav><!-- END #mobile-nav -->
<div class="branding-wrap uploaded-image header"> <!--hide-for-small-->
<div class="logo">
<a href="https://www.tengountrato.com" rel="home" title="Tengo un trato"><img alt="logo" class="logo-uploaded" src="https://www.tengountrato.com/wp-content/uploads/2015/06/tengo-un-trato-logo.jpg"/></a>
</div><!-- END .logo --> <div class="phrase-logo-container">
<img alt="Tengo un trato" class="phrase-logo" src="https://www.tengountrato.com/wp-content/uploads/2015/06/tengountrato.jpg"/>
</div><!-- END .site-description -->
</div><!-- END .branding-wrap.hide-for-small -->
<div class="hide-for-small" id="header-container">
<span class="menu-icon"></span>
</div><!-- END #header-container.hide-for-small -->
</header>
<div class="animated fadein" id="primary-container">
<div class="row">
<div class="twelve columns centered">
<div class="entry-content">
<h1 class="entry-title"></h1>
<p></p>
<p>← <b><a href="javascript:javascript:history.go(-1)">Go Back</a></b> or <b><a href="https://www.tengountrato.com">Go Home</a></b> →</p>
</div><!-- END .entry-content -->
</div><!-- END .twelve columns centered -->
</div><!-- END .row -->
</div><!-- END #primary-container -->
</div><!-- END #theme-wrapper -->
<div class="hidden-sidebar">
<div class="hidden-sidebar-inner">
<span class="close-btn show-for-small"></span>
<h6 class="menu_text">Tengo un Trato</h6>
<ul class="main-menu" id="primary-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-515" id="menu-item-515"><a href="https://www.tengountrato.com/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-514" id="menu-item-514"><a href="https://www.tengountrato.com/servicios/">Servicios</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-545" id="menu-item-545"><a href="https://www.tengountrato.com/servicios/street-marketing/">Street Marketing</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-544" id="menu-item-544"><a href="https://www.tengountrato.com/servicios/street-marketing/pegada-de-carteles/">Pegada de carteles</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-541" id="menu-item-541"><a href="https://www.tengountrato.com/servicios/street-marketing/reparto-de-flyers/">Reparto de flyers, CDs, etc…</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-542" id="menu-item-542"><a href="https://www.tengountrato.com/servicios/street-marketing/distribuciones/">Distribución Publicaciones</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-705" id="menu-item-705"><a href="https://www.tengountrato.com/servicios/guerrilla-marketing/">Guerrilla Marketing</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-543" id="menu-item-543"><a href="https://www.tengountrato.com/servicios/guerrilla-marketing/vinilos-y-pegatinas/">Vinilos y pegatinas</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-706" id="menu-item-706"><a href="https://www.tengountrato.com/servicios/guerrilla-marketing/stands/">Festivales y Stands</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-707" id="menu-item-707"><a href="https://www.tengountrato.com/servicios/guerrilla-marketing/acciones-especiales/">Acciones especiales</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-704" id="menu-item-704"><a href="https://www.tengountrato.com/servicios/otros/">Otros</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-512" id="menu-item-512"><a href="https://www.tengountrato.com/clientes/">Clientes</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7480" id="menu-item-7480"><a href="https://www.tengountrato.com/seguimiento-en-barcelona/">Seguimiento</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1611" id="menu-item-1611"><a href="https://www.tengountrato.com/distribuciones/">Distribuciones</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-511" id="menu-item-511"><a href="https://www.tengountrato.com/quienes-somos/">Quiénes somos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-516" id="menu-item-516"><a href="https://www.tengountrato.com/blog/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-517" id="menu-item-517"><a href="https://www.tengountrato.com/contacto/">Contacto</a></li>
</ul>
<div class="widget widget_search clearfix">
<form action="https://www.tengountrato.com/" class="searchform" id="searchform" method="get">
<input id="s" name="s" onblur="if(this.value=='')this.value='Search here...';" onfocus="if(this.value=='Search here...')this.value='';" type="text" value="Search here..."/>
</form><!-- END #searchform --></div><div class="widget widget_categories clearfix"><h6 class="widget-title">El Blog de Tengo un Trato</h6> <ul>
<li class="cat-item cat-item-22"><a href="https://www.tengountrato.com/category/actualidad/">Actualidad</a>
</li>
<li class="cat-item cat-item-38"><a href="https://www.tengountrato.com/category/el-mundo-del-cine-y-documental/">El mundo del Cine y Documental</a>
</li>
<li class="cat-item cat-item-1"><a href="https://www.tengountrato.com/category/sin-categoria/">Sin categoría</a>
</li>
<li class="cat-item cat-item-25"><a href="https://www.tengountrato.com/category/tut-en-la-calle/">TUT en la calle</a>
</li>
</ul>
</div>
<footer>
<p class="copyright">
				Tengountrato.com - <a href="/aviso-legal/">Aviso legal</a> - <a href="/cookies/">Cookies</a> </p>
</footer><!-- END footer -->
</div><!-- END .hidden-sidebar-inner -->
</div><!-- END .hidden-sidebar --><!-- HTML del pié de página -->
<div class="cdp-cookies-alerta cdp-solapa-ocultar cdp-cookies-textos-izq cdp-cookies-tema-gris">
<div class="cdp-cookies-texto">
<h4 style="font-size:15px !important;line-height:15px !important">Uso de cookies</h4><p style="font-size:12px !important;line-height:12px !important">Utilizamos cookies para mejorar nuestro sitio web y su experiencia al usarlo. Las cookies utilizadas para el funcionamiento esencial de este sitio ya se han establecido. Para obtener más información sobre las cookies que utilizamos y cómo eliminarlas, ver nuestra <a href="https://www.tengountrato.com/declaracion-de-privacidad/" style="font-size:12px !important;line-height:12px !important">Política de privacidad</a> y nuestra <a href="https://www.tengountrato.com/cookies/" style="font-size:12px !important;line-height:12px !important">Política de cookies</a></p>
<a class="cdp-cookies-boton-cerrar" href="javascript:;">ACEPTAR</a>
</div>
<a class="cdp-cookies-solapa">Aviso de cookies</a>
</div>
<script type="text/javascript">
				var _paq = _paq || [];
								_paq.push(['trackPageView']);
								(function () {
					var u = "https://stats1.wpmudev.com/";
					_paq.push(['setTrackerUrl', u + 'track/']);
					_paq.push(['setSiteId', '7386']);
					var d   = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
					g.type  = 'text/javascript';
					g.async = true;
					g.defer = true;
					g.src   = 'https://stats.wpmucdn.com/analytics.js';
					s.parentNode.insertBefore(g, s);
				})();
			</script>
<script src="https://www.tengountrato.com/wp-content/plugins/bean-shortcodes/assets/js/bean-shortcodes.min.js?ver=1.0" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.tengountrato.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"cached":"1"};
/* ]]> */
</script>
<script src="https://www.tengountrato.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.9" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6LdyarYUAAAAADMJ9cfOWyNc9MZ0-1DdlygItxHs&amp;ver=3.0" type="text/javascript"></script>
<script src="https://www.tengountrato.com/wp-content/themes/koi/assets/js/isotope.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.tengountrato.com/wp-content/themes/koi-child/assets/js/dist/smart-resize-min.js?ver%5B0%5D=jquery" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var my_ajax_object = {"ajax_url":"https:\/\/www.tengountrato.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script src="https://www.tengountrato.com/wp-content/themes/koi-child/assets/js/dist/month-loader-min.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://www.tengountrato.com/wp-content/themes/koi/assets/js/jquery.fitvids.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.tengountrato.com/wp-content/themes/koi/assets/js/modernizr.js?ver=1.0" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var bean = {"ajaxurl":"https:\/\/www.tengountrato.com\/wp-admin\/admin-ajax.php","nonce":"a05b956aa4"};
/* ]]> */
</script>
<script src="https://www.tengountrato.com/wp-content/themes/koi/assets/js/custom.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.tengountrato.com/wp-content/themes/koi/assets/js/custom-libraries.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.tengountrato.com/wp-content/themes/koi/assets/js/retina.js?ver=1.0" type="text/javascript"></script>
<script type="text/javascript">
( function( sitekey, actions ) {

	document.addEventListener( 'DOMContentLoaded', function( event ) {
		var wpcf7recaptcha = {

			execute: function( action ) {
				grecaptcha.execute(
					sitekey,
					{ action: action }
				).then( function( token ) {
					var event = new CustomEvent( 'wpcf7grecaptchaexecuted', {
						detail: {
							action: action,
							token: token,
						},
					} );

					document.dispatchEvent( event );
				} );
			},

			executeOnHomepage: function() {
				wpcf7recaptcha.execute( actions[ 'homepage' ] );
			},

			executeOnContactform: function() {
				wpcf7recaptcha.execute( actions[ 'contactform' ] );
			},

		};

		grecaptcha.ready(
			wpcf7recaptcha.executeOnHomepage
		);

		document.addEventListener( 'change',
			wpcf7recaptcha.executeOnContactform, false
		);

		document.addEventListener( 'wpcf7submit',
			wpcf7recaptcha.executeOnHomepage, false
		);

	} );

	document.addEventListener( 'wpcf7grecaptchaexecuted', function( event ) {
		var fields = document.querySelectorAll(
			"form.wpcf7-form input[name='g-recaptcha-response']"
		);

		for ( var i = 0; i < fields.length; i++ ) {
			var field = fields[ i ];
			field.setAttribute( 'value', event.detail.token );
		}
	} );

} )(
	'6LdyarYUAAAAADMJ9cfOWyNc9MZ0-1DdlygItxHs',
	{"homepage":"homepage","contactform":"contactform"}
);
</script>
</body>
</html>