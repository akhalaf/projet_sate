<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "88136",
      cRay: "61107523de5fd1df",
      cHash: "c5f143459f8b3bb",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmNkYi5jb20vY2FydG9vbnMvV2FsdF9EaXNuZXlfU3R1ZGlvcy9UZWxldmlzaW9uL2luZGV4Lmh0bWw=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "PEWQNG4b8owfdY+bBBLka/Ps1omtzegPlqVl1Ry2uD9iYRngMiHBrxvqF7oqoMlhnXzrHXhp01UFzckh1c2GeUGlTQfCJKMCeQ3pUr6bvQt70VEMvmQx1/MEzNledP7T+r79vzy2WUn4+Nh+ePTzgnStbMivbVd6fJd8/+s/PWWFdHHp4n91bFxmEBj7CHXHa0wKufqROyN3zvyQwlvkT8nvA4eSSuI2sgIcONyLrjeJ/VU9EFIA0uQIsvnIBkFR6Rfc83rKaEizAYudQK2U6XnMQR//RDnveEadF71i21UyblGraONPy/CTMetydUgUdiHJQU81nVwUNlh8J7ElCcu6ArGAKSx3cPXzLagbdCpWe3PSrEEavU1RcGXwWZy9OBXxodscDXma/pOVtxkEgNNisAYynySuJQckGArxadE/jCee1bii7gBsXd8/I+HNjeeEpxRzINU2a8SF/iEcIPJ+SnLd3Q9H0F/iOG6AdpvgDsC3oVYabbPioyxPCtf0v2q2ieoOPc/koJeYvIzrwg8s3vzawnGDcCXsLfHYrFhyqQkXL7OoSUzsdI2joJdJp7Gs5esPsYy1Sd4ff8cLI0323vifHiPjytIH2Wci+54rWmJasb4lxXE9eX9RmmRnN3LYspio35iQt6vGmZChTIbhhQwTSjynSH+gmkxiv3cKkRRgCnGgdT1CTHbABImKWhHvfWoCEwEHI0nN8eJxUgAtXgZMzQou4hU1ZS60ir9v3w/QMb5MXFeg7rTmJvng",
        t: "MTYxMDU1NDk2Mi41MzcwMDA=",
        m: "unrnrc3YtS/KDgv4k+l7Uduwyac5jzPEhnnHWeeTlfQ=",
        i1: "ftd6+GmZXi6Uq+g4zMTKwA==",
        i2: "yckWb49vYf/1x4024Ita5Q==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "cXDaOma3wiBrP6kg81PYP0GD5VWUQPtDIIDjSKLqBr4=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.bcdb.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/cartoons/Walt_Disney_Studios/Television/index.html?__cf_chl_captcha_tk__=22be1149653e8bafcda22cec513702df4587b8f7-1610554962-0-AS-p2navv19wlGGTjGm1iN2YwmN7DwWWEndNsB-dnOfdkTQZqU7kLbw9MImTBkUE21mvtcfUHNpFfPiVTUgo-d8IwLiM-R6BZB3GnDOnfkAIAomFz3JhObppW_lE8SrQOjb0nsQMLx1WX5vqaYVZ3_6bw7L5dO0N1oDYPldzOZ_Bc_abe_U1w6ucVNJjBl5AXfc_dsA2qTvdfBLEjtuF-C7ukDKs905ZTbAxPhyyGqg0os7Ny8-n6S683HKna8o-5E6DCf8-0VWYZ0o1JkXMFLXi0mrDBX4hPPG6i_DTffoEw35H7RMAGs6DStYR_tpS0Pk9FcPJuNrPRHVM3yFVm2-E5_BXAA54atOtpxYsqD5a8aa8avVpCkUPyVTzyYc_yREMnV0e7HVfr1k0V7xiemkamiw9us9D-ViEMhRNXjZJLqDkjGKslRcUEB6koYpuykgzZoO8eFKC3ehlUvGvzpwcSYedexVztyadKOLd06r2ti3KxMnp9l0ouILRjv_QlZhWSx8JKc2J6ZCH--mauC1FYcnAeFkkcmK-e0Abkslei-vI2bgDUx2Cq1T46M0qK6kHyid9JuikofPB_KKIDUg" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="a69dae57a6ba307f8ceb7b7bb74fec8041db04e2-1610554962-0-AQRpLD/tfYblA2assu5HP56Q4/FJKGC5k9hDww31msfktyW0P/1F/z8EP515Qt3OXU+IFM3egK9sDKeGMLzLD/floWpDqUNV/x7IGRWhrAulwMEJ5J+OV8jJkL10o0f6iKqrkK+BKpakS4G4do0zsoSB/3zP8F+MCC30StZvVRaqrVYTbrDyRoOQX1t+9qgHNCR8ZYnj7SBGO5DsKDH/o49FnLy2UtKH+W/ZPMB91+sAj4a2z9kf1jtjE/cxRy9qkNkBO8eooq2qFF7n7qGVbq/PIzPIfnMgT1/kEcW7BIKtDD5RMxuRBX97djO856Qcb518a0O4WZ3QoAG1MoPOz5+JIuXdRc8yPBll4eifW3jE+ckNJmSr9dUS0q4MrrCPMmyFIbHlS1/kiqkgWo9b54XnoHai3x3YVKdiH4qVcD/0xn3jv72QoJ3wcsM7n3pJhdrKcJxzanD7BW1p7LYNQq1e0bv3gCXtlYsfksYcw8MWm8/d/9hK2T3sAMiACkbEtOtkqn8TQbk8bhyTD1gtSpaJ/FMkqWjKklCGilcvTlqOe8ZrMv4tH+XNlSxpxcp4kxd9PGMxH76QPP2ldgSdrqZMg9Us8XHjfqf29IELFfNZAfwZsKfQJKO0Rs2v5lEEaLlagw4zb0lFuYVKKEg1nxEXfWE6qB8229J/gOV/ba/LjUsEm4dUnf26qgo2K2XLhontR5aKWbKzHM+qkR9KFPz1J2ONblC7HfbmvzK0zE21ViAvZXZQCCccjXHlKqNA85fQDmMIuppgYofS6pCgeP2pQLU/2aKpIgzuwNYwC+hffnBWqcqUo9evdw5KfQ/2ZURfKc8BeN7YwzKVpkn4xgdAXMRM6YS14Tg5TTpE51W5ssdKnxbwxOQfgG2TA0uH4opHONbIzu2l9oI+z4EmywJWGu51NzFBeC27RD//bDVFOti+7jV1/eknESZBqBL+jKWBHo6esR+LTrVM8hqH8t6ej+Fw42+AsU3FYv3/BaSPNqGV26GIpm9uG5MD3p/8qndBh/d4xEbNbY+bkxaHPwRMoLsuy68JeADNVnYHxHP9sxy99yPykB+C4jeYYibaSFVYhgPemCZnG1OsEJA5Zvcu+uB7kCA4wOLVV1XSH3jbwfFSL2NR4K59ibUiaeFuq1cXnex6iblolTG0ZXJlgGPWdQv2mmEnYZW01coVBF/Az7OsLoO7Oa9u2Ks34KjAL3zp8MzeVFgTCgSKB9vLF7Ktx4FmeBYoHD1f/f7ksEMdDQMQUSmIzab1ANyUpo6KTAZn6bfI6lLWBo1LUvBwMFs="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="055463783c689a66301a83678800a018"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=61107523de5fd1df')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<a href="https://tinwatch.net/inclusivecool.php?tag=9"><!-- table --></a>
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">61107523de5fd1df</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 210.75.253.169</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
