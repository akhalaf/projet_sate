<!DOCTYPE html>
<html><body><p>ang="en-ZA"&gt;
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <!--<![endif]-->
</p>
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<title>Avoca Developments</title>
<link href="https://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/css/normalize.css" rel="stylesheet"/>
<link href="https://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/css/foundation.css" rel="stylesheet"/>
<link href="https://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/css/animate.css" rel="stylesheet"/>
<link href="https://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/css/icomoon.css" rel="stylesheet"/>
<link href="https://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/css/style.css" rel="stylesheet"/>
<script src="https://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/vendor/custom.modernizr.js"></script>
<!-- Default WordPress jQuery lib (sorry, it just have to go this way because of template system limitations) -->
<script src="https://avocadevelopments.co.za/wp-includes/js/jquery/jquery.js"></script>
<link href="https://fonts.googleapis.com/css?family=Ubuntu&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/><link href="https://fonts.googleapis.com/css?family=Ubuntu&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/><link href="https://fonts.googleapis.com/css?family=Vollkorn&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/><link href="https://fonts.googleapis.com/css?family=Ubuntu&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/><style>body {background: !important;}.button {background:#dd3333 !important;}.button:hover {background:#9e0039 !important;}.nifty-title {font-family:'Ubuntu' !important;}.nifty-coming-soon-message {font-family:'Ubuntu' !important;}.timer-item {font-family:'Ubuntu' !important;}body p, .nifty-inform, .nifty-success, .nifty-error, input {font-family:'Vollkorn' !important;}</style>
<div class="nifty-main-wrapper">
<!-- Page Preloader -->
<div id="preloader"></div>
<div class="nifty-content-wrapper">
<header class="nifty-row ">
<div class="large-12 columns text-center">
<!-- Logo and navigation  -->
<div class="nifty-logo"><a href="https://avocadevelopments.co.za"><img alt="Avoca Developments" src="http://avocadevelopments.co.za/wp-content/uploads/2017/11/logoonwhite.png"/></a></div>
</div>
</header></div>
<div class="nifty-row">
<div class="large-10 small-centered columns text-center">
<div class="nifty-coming-soon-message">
<div class="tlt" id="animated_intro">
<ul class="texts" style="display: none">
<li>Our website is coming very soon</li>
<li>Feel free to drop-by any time soon</li>
</ul>
</div>
</div>
</div>
</div>
<!-- Timer Section -->
<div class="nifty-row" id="clock">
<div class="large-8 small-centered columns">
<div class="large-3 small-3 columns">
<div class="timer-item">
<div class="timer-top"><span id="days"></span></div>
<div class="timer-bottom">days</div>
</div>
</div>
<div class="large-3 small-3 columns">
<div class="timer-item">
<div class="timer-top"><span id="hours"></span></div>
<div class="timer-bottom">hours</div>
</div>
</div>
<div class="large-3 small-3 columns">
<div class="timer-item">
<div class="timer-top"><span id="minutes"></span></div>
<div class="timer-bottom">minutes</div>
</div>
</div>
<div class="large-3 small-3 columns">
<div class="timer-item">
<div class="timer-top"><span id="seconds"></span></div>
<div class="timer-bottom">seconds</div>
</div>
</div>
</div>
</div>
<!-- Content Section -->
<div class="nifty-content-full">
<div class="nifty-row">
<!-- Slider Section -->
<ul class="bxslider">
<!-- Slide One - Subscribe Here -->
<li>
<section class="large-12 columns"> <form id="contact" method="post">
<div class="large-4 small-centered columns">
<div class="nifty-inform">Sign up to find out when we launch </div> <div class="nifty-row collapse">
<div class="small-8 columns">
<input autocomplete="off" id="email" name="email" placeholder="socialmedia@avocadevelopments.co.za" type="text"/>
</div>
<div class="small-4 columns">
<input class="button prefix" name="submit" type="submit" value=" Sign Up"/>
</div>
<div class="nifty-success" style="display:none">You will be notified, thanks.</div>
<div class="nifty-error" style="display:none"> Please, enter valid email address.</div>
</div>
</div>
</form>
</section>
</li>
<!-- Slide Two - About Us -->
<li>
<section class="small-12 small-centered columns">
<div class="nifty-contact-details">
<p><strong>Avoca Developments
				</strong></p>
<p><span aria-hidden="true" class="icon-house"></span>
				230 New Found lane, 8900 New City</p>
<p><span aria-hidden="true" class="icon-phone"></span>
				+27 083 488 5489</p><p> <span aria-hidden="true" class="icon-mail"></span> <a href="mailto:socialmedia@avocadevelopments.co.za">socialmedia@avocadevelopments.co.za</a></p></div>
</section>
</li>
<!-- Slide Three - Social links -->
</ul>
</div>
</div>
</div>
<!-- jQuery Vegas Background Slider -->
<script>
	jQuery(document).ready(function($) {
	$.vegas('slideshow', {
		
		delay:6000,
    backgrounds:[
    { src:'http://avocadevelopments.co.za/wp-content/uploads/2017/11/holderrev.jpg', fade:1000 },
    { src:'http://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/admin//assets/slideshow/2.jpg', fade:1000 },
    { src:'http://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/admin//assets/slideshow/3.jpg', fade:1000 }
     ] 
    });
		
	
	$.vegas('overlay', {
		 src:'https://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/admin//assets/images/patterns/07.png', opacity:0.5});});</script>
<script>

// Timer Settings  //

jQuery(function($) { 
  $('div#clock').countdown("2017/11/30", function(event) { 
    var $this = $(this);
    switch(event.type) {
      case "seconds":
      case "minutes":
      case "hours":
      case "days":
      case "weeks":
      case "daysLeft":
        $this.find('span#'+event.type).html(event.value);
		
        break;
      case "finished":
        $this.hide();
        break;
    }
  });
});
</script>
<!-- Footer js scripts -->
<script src="https://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/scripts.js"></script>
<script src="https://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/jquery.countdown.js"></script>
<script src="https://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/jquery.bxslider.min.js"></script>
<script src="https://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/jquery.vegas.min.js"></script>
<script src="https://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/jquery.fittext.js"></script>
<script src="https://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/jquery.textillate.js"></script>
<script src="https://avocadevelopments.co.za/wp-content/plugins/nifty-coming-soon-and-under-construction-page/template/assets/js/jquery.lettering.js"></script>
<script>
	jQuery(document).ready(function($){
	 $('.tlt').textillate({
  // the default selector to use when detecting multiple texts to animate
  selector: '.texts',

  // enable looping
  loop: true,

  // sets the minimum display time for each text before it is replaced
  minDisplayTime: 2500,


  // set whether or not to automatically start animating
  autoStart: true,

  // custom set of 'out' effects
  outEffects: [ 'bounceOut' ],

  // in animation settings
  in: {
    // set the effect name
    effect: 'fadeIn',

    // set the delay factor applied to each consecutive character
    delayScale: 1.5,

    // set the delay between each character
    delay: 50,

    // set to true to animate all the characters at the same time
    sync: false,

    // randomize the character sequence 
    // (note that shuffle doesn't make sense with sync = true)
    shuffle: true
  },

  // out animation settings.
  out: {
    effect: 'bounceOut',
    delayScale: 1.5,
    delay: 150,
    sync: false,
    shuffle: true,
  }
});
	});
	</script>
<!-- Google Analytics Code -->
<!-- Additional CSS -->
<style></style>
</body></html>