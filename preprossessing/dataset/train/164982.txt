<!DOCTYPE html>
<html lang="en">
<head>
<!-- OneTrust Cookies Consent Notice start -->
<script charset="UTF-8" data-document-language="true" data-domain-script="c110b96d-f429-443c-b644-e6c66ea868a6" id="onetrustcdn" src="https://cdn.cookielaw.org/scripttemplates/otSDKStub.js" type="text/javascript"></script>
<script type="text/javascript">
function OptanonWrapper() {
	var cookiePolicyLink = 'https://www.wminewmedia.com/cookies-policy/';
	var allLinks = document.querySelectorAll('a');
	for(i = 0; i < allLinks.length; i++) {
		let href = allLinks[i].href;
		if (href.indexOf(cookiePolicyLink) > -1 && href.indexOf('?ot=') < 0) {
			href = href + '?ot=' + document.getElementById('onetrustcdn').getAttribute('data-domain-script') + '&url=' + window.location.hostname;
			allLinks[i].setAttribute("href", href);
			allLinks[i].setAttribute("target", "_blank");
		}
	}
	var eOT = new Event("OneTrustGroupsUpdated");
	document.dispatchEvent(eOT);
}
</script>
<link href="https://www.wminewmedia.com/cookies-policy/onetrust/ot.css" rel="stylesheet" type="text/css"/>
<!-- OneTrust Cookies Consent Notice end --> <meta charset="utf-8"/>
<meta content="initial-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<!-- Common Meta tags -->
<title>Against The Current - Official Site</title>
<meta content="" name="keywords"/>
<!-- Open Graph Tags -->
<meta content="The official website of the band Against The Current. Get news on music, videos, merch, sign up for the email list and more." name="description"/>
<!-- Open Graph Tags -->
<meta content="Against The Current - Official Site" property="og:title"/>
<meta content="https://www.atcofficial.com/" property="og:url"/>
<meta content="https://www.atcofficial.com//images/OG.jpg" property="og:image"/>
<meta content="The official website of the band Against The Current. Get news on music, videos, merch, sign up for the email list and more." property="og:description"/>
<meta content="Against The Current - Official Site" property="og:site_name"/>
<meta content="website" property="og:type"/>
<meta content="follow, index" name="robots"/>
<meta content="google030fc2f1e5487842.html" name="google-site-verification"/>
<!-- Icons -->
<link href="images/Fav.jpg" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<!-- JS Files -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.1/jquery.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-browser/0.1.0/jquery.browser.min.js" type="text/javascript"></script>
<script src="https://libraries.wmgartistservices.com/custom-js/mailing-list/staging/plainmailinglist.js"></script>
<script src="https://libraries.wmgartistservices.com/custom-js/mailing-list/validation.js"></script>
<script src="https://libraries.wmgartistservices.com/custom-js/mailing-list/staging/dtm.js"></script>
<link href="https://use.typekit.net/mor3bra.css" rel="stylesheet"/>
<link href="css/style.css?1610540182" rel="stylesheet" type="text/css"/>
<link href="css/video.css?1610540182" rel="stylesheet" type="text/css"/>
<link href="css/music.css?cache" rel="stylesheet" type="text/css"/>
<link href="css/tour.css?cache" rel="stylesheet" type="text/css"/>
<link href="css/onetrust.css?cache" rel="stylesheet" type="text/css"/>
<link href="css/mlist.css?1610540182" rel="stylesheet" type="text/css"/>
<link href="css/merch.css?1610540182" rel="stylesheet" type="text/css"/>
<link href="css/footer.css?1610540182" rel="stylesheet" type="text/css"/>
<link href="css/cursor.css?cache" rel="stylesheet" type="text/css"/>
<link href="css/owl.carousel.min.css" rel="stylesheet" type="text/css"/>
<link href="css/owl.theme.default.min.css" rel="stylesheet" type="text/css"/>
<link href="css/magnific-popup.css?xasdasxx" rel="stylesheet" type="text/css"/>
<link href="css/all.min.css?11" rel="stylesheet" type="text/css"/>
<link href="https://use.typekit.net/mva5ibg.css" rel="stylesheet"/>
<!-- <link rel="stylesheet" type="text/css" href="css/owlcarousel.css"/> -->
<script src="js/owl.carousel.js?adssssascas" type="text/javascript"></script>
<script src="js/magnific.js?sdfds" type="text/javascript"></script>
<script src="js/site.js?1610540182" type="text/javascript"></script>
<script src="js/animate.js?1603769305" type="text/javascript"></script>
<script type="text/javascript">								
var digitalData={								
	settings:{							
		reportSuites:"wmgagainstthecurrent,wmg"						
	},							
	page:{							
		pageInfo:{						
			pageName:"Against The Current:Homepage",					
			server:"Against The Current:Site",					
			platform:"MIS Custom Page",					
			devTeam:"WMAS"					
		},						
		category:{						
			primaryCategory:"Against The Current:Home",					
			pageType:"homepage"					
		}						
	},							
	content:{							
		artist:"Against The Current",						
		label:"Elektra Music Group",						
		sublabel:"Fueled By Ramen"						
	}							
}								
</script>
<script async="" src="//assets.adobedtm.com/launch-EN302b8a31b75a4dda8ff8df1d0cdb4762.min.js"></script>
</head>
<body class="homepage">
<div class="demo-3 cda-noimg cda-alignright">
<main><div class="content content--demo3">
<div class="circle-cursor circle-cursor--inner"></div>
<div class="circle-cursor circle-cursor--outer"></div>
</div>
</main></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/paper.js/0.12.0/paper-core.min.js"></script>
<script src="js/cursor.js" type="text/javascript"></script>
<div class="home-section">
<div class="header" id="headersec">
<div class="headerOurerWrapper" id="headersec">
<div class="menu-logo-wrapper">
<div class="overlays">
<div class="mobilemenu"><div class="nav-toggle">
<span></span><span></span><span></span>
<span class="menu-text">MENU</span>
</div></div>
<div class="menu-wrapper">
<div class="menu-content">
<ul class="menu">
<li class="home-link-wrapper">
<a class="home" href="./?frontpage=true">Home</a>
</li>
<li class="music-link-wrapper">
<a class="music" href="#musicSectionstart">Music</a>
<!--a class="music" href="#tourSectionstart">Tour</a-->
</li>
<li class="video-link-wrapper">
<a class="videos" href="#videoSectionstart">Videos</a>
</li>
<li class="merch-link-wrapper">
<a class="merch" data-track="enter-store" href="https://store.atcofficial.com/" target="_blank">Store</a>
</li>
</ul>
</div>
</div>
</div>
</div> <div class="headerlogo">
<div class="logoWrapper">
<h1><a href="./?frontpage=true">
<img alt="images/png" class="artistLogoDesk" src="images/logo.png?1"/></a></h1>
</div>
</div>
<div class="atc"><img src="images/atc.png"/></div>
</div>
</div> </div>
<div class="music" id="musicSectionstart">
<div class="music-inner"><div class="title">MUSIC</div>
<div class="album-wrapper">
<div class="right-album-section">
<div class="album-wrap">
<div class="album-content">
<div class="album-image">
<img src="images/album.jpg"/>
</div>
<div class="album-title">
            That won’t save us
            </div>
<div class="album-button">
<a href="https://againstthecurrent.lnk.to/twsu" target="_blank">
                GET IT NOW
                </a>
</div>
</div>
<div class="seeall">
<a href="https://againstthecurrent.lnk.to/music" target="_blank">SEE ALL MUSIC</a>
</div>
</div>
</div>
</div>
</div> </div>
<div class="mlist">
<div class="mlistwrapper">
<div class="mlist-inner-section">
<div class="mlist-inner-content">
<div class="plain-ml-wrapper dtmtoaster" data-custom-page-name="Homepage" id="firstMlistForm">
<div id="mlform">
<div class="message">
<!--p class="message-description">Be the first to know about special updates with Kehlani’s special, handwritten emails: </p-->
</div>
<form action="https://signup.wmg.com/register?" class="mlform twostep" id="mlistFormOne">
<div class="email fieldWrap">
<label class="" for="email">Email</label>
<input autocomplete="off" data-error-text="Please enter a valid email address" data-type="email" id="email" name="email" placeholder="Email" required=""/>
</div>
<div class="submit">
<input class="mlistSubmit" id="submit" type="submit" value="join"/>
</div>
<div class="primary-list-values">
<input id="Datasource" name="Datasource" type="hidden" value="AgainstTheCurrent_TWSUOutNow_Standalone_Website"/>
<input id="mainListId" name="newsletterId" type="hidden" value="14076391"/>
</div>
</form>
<div id="terms">
<a class="terms hoverbutton" href="javascript:void(0)">terms</a>
<p class="terms-message fadeOut">
                    By submitting my information, I agree to receive personalized updates and marketing messages about Against The Current based on my information, interests, activities, website visits and device data and in accordance with the <a class="external-link" href="http://www.atlanticrecords.com/privacy-policy" rel="nofollow" target="_blank" title="">Privacy Policy</a>. I understand that I can opt-out at any time by emailing <a href="mailto:privacypolicy@wmg.com">privacypolicy@wmg.com</a>.
                </p>
</div>
</div>
<div class="fadeOut" id="thankyou">
            Thank you!
        </div>
<div class="fadeOut singleform" id="secondform">
<div class="overlays"></div>
<!--div class="secondFormInnerWrapper"-->
<span id="secondFormClose">x</span>
<div class="welcome">
<p>Welcome <span id="name"></span> to Against The Current's mailing list!!</p>
</div>
<form action="https://signup.wmg.com/register?" class="mlform" id="mlistFormTwo">
<input id="secondFormEmail" name="email" type="hidden" value=""/>
<div class="postalcode fieldWrap">
<label class="" for="postalCode">postal code</label>
<input autocomplete="off" id="postalcode" name="postalcode" type="text"/>
</div>
<div class="postal_country">
<div class="country">
<select class="form-control" id="country" name="country">
<option value="">select a country</option>
<option value="Afghanistan">Afghanistan</option>
<option value="Åland Islands">Åland Islands</option>
<option value="Albania">Albania</option>
<option value="Algeria">Algeria</option>
<option value="American Samoa">American Samoa</option>
<option value="Andorra">Andorra</option>
<option value="Angola">Angola</option>
<option value="Anguilla">Anguilla</option>
<option value="Antarctica">Antarctica</option>
<option value="Antigua and Barbuda">Antigua and Barbuda</option>
<option value="Argentina">Argentina</option>
<option value="Armenia">Armenia</option>
<option value="Aruba">Aruba</option>
<option value="Australia">Australia</option>
<option value="Austria">Austria</option>
<option value="Azerbaijan">Azerbaijan</option>
<option value="Bahamas">Bahamas</option>
<option value="Bahrain">Bahrain</option>
<option value="Bangladesh">Bangladesh</option>
<option value="Barbados">Barbados</option>
<option value="Belarus">Belarus</option>
<option value="Belgium">Belgium</option>
<option value="Belize">Belize</option>
<option value="Benin">Benin</option>
<option value="Bermuda">Bermuda</option>
<option value="Bhutan">Bhutan</option>
<option value="Bolivia">Bolivia</option>
<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
<option value="Botswana">Botswana</option>
<option value="Bouvet Island">Bouvet Island</option>
<option value="Brazil">Brazil</option>
<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
<option value="Brunei Darussalam">Brunei Darussalam</option>
<option value="Bulgaria">Bulgaria</option>
<option value="Burkina Faso">Burkina Faso</option>
<option value="Burundi">Burundi</option>
<option value="Cambodia">Cambodia</option>
<option value="Cameroon">Cameroon</option>
<option value="Canada">Canada</option>
<option value="Cape Verde">Cape Verde</option>
<option value="Cayman Islands">Cayman Islands</option>
<option value="Central African Republic">Central African Republic</option>
<option value="Chad">Chad</option>
<option value="Chile">Chile</option>
<option value="China">China</option>
<option value="Christmas Island">Christmas Island</option>
<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
<option value="Colombia">Colombia</option>
<option value="Comoros">Comoros</option>
<option value="Congo">Congo</option>
<option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
<option value="Cook Islands">Cook Islands</option>
<option value="Costa Rica">Costa Rica</option>
<option value="Cote D'ivoire">Cote D'ivoire</option>
<option value="Croatia">Croatia</option>
<option value="Cuba">Cuba</option>
<option value="Cyprus">Cyprus</option>
<option value="Czech Republic">Czech Republic</option>
<option value="Denmark">Denmark</option>
<option value="Djibouti">Djibouti</option>
<option value="Dominica">Dominica</option>
<option value="Dominican Republic">Dominican Republic</option>
<option value="Ecuador">Ecuador</option>
<option value="Egypt">Egypt</option>
<option value="El Salvador">El Salvador</option>
<option value="Equatorial Guinea">Equatorial Guinea</option>
<option value="Eritrea">Eritrea</option>
<option value="Estonia">Estonia</option>
<option value="Ethiopia">Ethiopia</option>
<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
<option value="Faroe Islands">Faroe Islands</option>
<option value="Fiji">Fiji</option>
<option value="Finland">Finland</option>
<option value="France">France</option>
<option value="French Guiana">French Guiana</option>
<option value="French Polynesia">French Polynesia</option>
<option value="French Southern Territories">French Southern Territories</option>
<option value="Gabon">Gabon</option>
<option value="Gambia">Gambia</option>
<option value="Georgia">Georgia</option>
<option value="Germany">Germany</option>
<option value="Ghana">Ghana</option>
<option value="Gibraltar">Gibraltar</option>
<option value="Greece">Greece</option>
<option value="Greenland">Greenland</option>
<option value="Grenada">Grenada</option>
<option value="Guadeloupe">Guadeloupe</option>
<option value="Guam">Guam</option>
<option value="Guatemala">Guatemala</option>
<option value="Guernsey">Guernsey</option>
<option value="Guinea">Guinea</option>
<option value="Guinea-bissau">Guinea-bissau</option>
<option value="Guyana">Guyana</option>
<option value="Haiti">Haiti</option>
<option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
<option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
<option value="Honduras">Honduras</option>
<option value="Hong Kong">Hong Kong</option>
<option value="Hungary">Hungary</option>
<option value="Iceland">Iceland</option>
<option value="India">India</option>
<option value="Indonesia">Indonesia</option>
<option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
<option value="Iraq">Iraq</option>
<option value="Ireland">Ireland</option>
<option value="Isle of Man">Isle of Man</option>
<option value="Israel">Israel</option>
<option value="Italy">Italy</option>
<option value="Jamaica">Jamaica</option>
<option value="Japan">Japan</option>
<option value="Jersey">Jersey</option>
<option value="Jordan">Jordan</option>
<option value="Kazakhstan">Kazakhstan</option>
<option value="Kenya">Kenya</option>
<option value="Kiribati">Kiribati</option>
<option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
<option value="Korea, Republic of">Korea, Republic of</option>
<option value="Kuwait">Kuwait</option>
<option value="Kyrgyzstan">Kyrgyzstan</option>
<option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
<option value="Latvia">Latvia</option>
<option value="Lebanon">Lebanon</option>
<option value="Lesotho">Lesotho</option>
<option value="Liberia">Liberia</option>
<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
<option value="Liechtenstein">Liechtenstein</option>
<option value="Lithuania">Lithuania</option>
<option value="Luxembourg">Luxembourg</option>
<option value="Macao">Macao</option>
<option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of
                                </option>
<option value="Madagascar">Madagascar</option>
<option value="Malawi">Malawi</option>
<option value="Malaysia">Malaysia</option>
<option value="Maldives">Maldives</option>
<option value="Mali">Mali</option>
<option value="Malta">Malta</option>
<option value="Marshall Islands">Marshall Islands</option>
<option value="Martinique">Martinique</option>
<option value="Mauritania">Mauritania</option>
<option value="Mauritius">Mauritius</option>
<option value="Mayotte">Mayotte</option>
<option value="Mexico">Mexico</option>
<option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
<option value="Moldova, Republic of">Moldova, Republic of</option>
<option value="Monaco">Monaco</option>
<option value="Mongolia">Mongolia</option>
<option value="Montenegro">Montenegro</option>
<option value="Montserrat">Montserrat</option>
<option value="Morocco">Morocco</option>
<option value="Mozambique">Mozambique</option>
<option value="Myanmar">Myanmar</option>
<option value="Namibia">Namibia</option>
<option value="Nauru">Nauru</option>
<option value="Nepal">Nepal</option>
<option value="Netherlands">Netherlands</option>
<option value="Netherlands Antilles">Netherlands Antilles</option>
<option value="New Caledonia">New Caledonia</option>
<option value="New Zealand">New Zealand</option>
<option value="Nicaragua">Nicaragua</option>
<option value="Niger">Niger</option>
<option value="Nigeria">Nigeria</option>
<option value="Niue">Niue</option>
<option value="Norfolk Island">Norfolk Island</option>
<option value="Northern Mariana Islands">Northern Mariana Islands</option>
<option value="Norway">Norway</option>
<option value="Oman">Oman</option>
<option value="Pakistan">Pakistan</option>
<option value="Palau">Palau</option>
<option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
<option value="Panama">Panama</option>
<option value="Papua New Guinea">Papua New Guinea</option>
<option value="Paraguay">Paraguay</option>
<option value="Peru">Peru</option>
<option value="Philippines">Philippines</option>
<option value="Pitcairn">Pitcairn</option>
<option value="Poland">Poland</option>
<option value="Portugal">Portugal</option>
<option value="Puerto Rico">Puerto Rico</option>
<option value="Qatar">Qatar</option>
<option value="Reunion">Reunion</option>
<option value="Romania">Romania</option>
<option value="Russian Federation">Russian Federation</option>
<option value="Rwanda">Rwanda</option>
<option value="Saint Helena">Saint Helena</option>
<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
<option value="Saint Lucia">Saint Lucia</option>
<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
<option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
<option value="Samoa">Samoa</option>
<option value="San Marino">San Marino</option>
<option value="Sao Tome and Principe">Sao Tome and Principe</option>
<option value="Saudi Arabia">Saudi Arabia</option>
<option value="Senegal">Senegal</option>
<option value="Serbia">Serbia</option>
<option value="Seychelles">Seychelles</option>
<option value="Sierra Leone">Sierra Leone</option>
<option value="Singapore">Singapore</option>
<option value="Slovakia">Slovakia</option>
<option value="Slovenia">Slovenia</option>
<option value="Solomon Islands">Solomon Islands</option>
<option value="Somalia">Somalia</option>
<option value="South Africa">South Africa</option>
<option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands
                                </option>
<option value="Spain">Spain</option>
<option value="Sri Lanka">Sri Lanka</option>
<option value="Sudan">Sudan</option>
<option value="Suriname">Suriname</option>
<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
<option value="Swaziland">Swaziland</option>
<option value="Sweden">Sweden</option>
<option value="Switzerland">Switzerland</option>
<option value="Syrian Arab Republic">Syrian Arab Republic</option>
<option value="Taiwan, Province of China">Taiwan, Province of China</option>
<option value="Tajikistan">Tajikistan</option>
<option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
<option value="Thailand">Thailand</option>
<option value="Timor-leste">Timor-leste</option>
<option value="Togo">Togo</option>
<option value="Tokelau">Tokelau</option>
<option value="Tonga">Tonga</option>
<option value="Trinidad and Tobago">Trinidad and Tobago</option>
<option value="Tunisia">Tunisia</option>
<option value="Turkey">Turkey</option>
<option value="Turkmenistan">Turkmenistan</option>
<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
<option value="Tuvalu">Tuvalu</option>
<option value="Uganda">Uganda</option>
<option value="Ukraine">Ukraine</option>
<option value="United Arab Emirates">United Arab Emirates</option>
<option value="United Kingdom">United Kingdom</option>
<option value="United States">United States</option>
<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
<option value="Uruguay">Uruguay</option>
<option value="Uzbekistan">Uzbekistan</option>
<option value="Vanuatu">Vanuatu</option>
<option value="Venezuela">Venezuela</option>
<option value="Viet Nam">Viet Nam</option>
<option value="Virgin Islands, British">Virgin Islands, British</option>
<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
<option value="Wallis and Futuna">Wallis and Futuna</option>
<option value="Western Sahara">Western Sahara</option>
<option value="Yemen">Yemen</option>
<option value="Zambia">Zambia</option>
<option value="Zimbabwe">Zimbabwe</option>
</select>
</div>
</div>
<div class="name">
<div class="first fieldWrap">
<label class="" for="FirstName">First Name</label>
<input autocomplete="off" id="first" name="firstname" type="text"/>
</div>
</div>
<div class="birthday">
<label class="birthday" for="birthday">birthday</label>
<div class="dobMonth fieldWrap">
<select id="dobMonth" name="dobMonth">
<option value="">Month</option>
<option value="1">January</option>
<option value="2">February</option>
<option value="3">March</option>
<option value="4">April</option>
<option value="5">May</option>
<option value="6">June</option>
<option value="7">July</option>
<option value="8">August</option>
<option value="9">September</option>
<option value="10">October</option>
<option value="11">November</option>
<option value="12">December</option>
</select>
</div>
<div class="dobDay fieldWrap">
<select id="dobDay" name="dobDay">
<option value="">Day</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select>
</div>
</div>
<input name="newsletterId" type="hidden" value="14075551"/>
<input id="Datasource" name="Datasource" type="hidden" value="AgainstTheCurrent_TWSUOutNow_Standalone_Website"/>
<div class="globaloptin fieldWrap">
<input id="goptin_checkbox" name="goptin" type="checkbox" value="20369"/>
<label class="goptin_checkbox" for="goptin_checkbox">Sign me up to discover more artists like <span>Against The Current</span> and other offers.</label>
</div>
<div class="submit">
<input class="mlistSubmit" id="submit" type="submit" value="join"/>
</div>
<div id="terms">
<a class="terms hoverbutton" href="javascript:void(0)">terms</a>
<p class="terms-message-2 fadeOut">
                            By submitting my information, I agree to receive personalized updates and marketing messages about Against The Current based on my information, interests, activities, website visits and device data and in accordance with the <a class="external-link" href="http://www.atlanticrecords.com/privacy-policy" rel="nofollow" target="_blank" title="">Privacy Policy</a>. In addition, if I have checked the box above, I agree to receive such updates and messages about similar artists, products and offers. I understand that I can opt-out from messages at any time by emailing <a href="mailto:privacypolicy@wmg.com">privacypolicy@wmg.com</a>.
                        </p>
</div>
</form>
<!--/div-->
</div>
</div>
</div>
</div>
</div> </div>
<div class="video" id="videoSectionstart">
<video autoplay="" class="initial_video desktop" id="myVideo2" loop="" muted="" playsinline="">
<source id="video-src" src="https://d2cstorage-a.akamaihd.net/atl/atcofficial/twsu%20web%20clip.mp4" type="video/mp4">
</source></video>
<div class="video-inner">
<div class="headerVideo">
<div class="title">
        VIDEOS
    </div>
</div>
<div class="watch-wrapper">
<div class="watch-inner-content">
<a class="watch-now" data-isplaylist="" href="javascript:(0);" video-id="PD8q6R-AiO4">
<img src="images/video-play.svg"/>
</a>
</div>
</div>
<div class="ytEmbedAndClose mfp-hide" id="ytEmbedAndClose">
<div class="ytOuterWrapper">
<div class="youtubeLightBox"></div>
</div>
</div>
<div class="all-video">
<a href="https://www.youtube.com/channel/UCxMsgwldMZiuFTD6jjv32yQ" target="_blank">see all videos</a>
</div>
</div>
<script>
    function playCurrentVideo(event) {
        // if (Object.keys(ytPlayers).length > 0) {
        // 	for (let k = 0; k < Object.keys(ytPlayers).length; k++) {
        // 		ytPlayers[Object.keys(ytPlayers)[k]].pauseVideo()
        // 	}
        // }	
        event.target.playVideo();
    }

    function formYoutubePlaylist(playerID, youtubeID) {
        player = new YT.Player(playerID, {
            height: '360',
            width: '640',
            host: 'https://www.youtube-nocookie.com',
            playerVars: {
                listType: 'playlist',
                list: youtubeID
            },
            events: {
                'onReady': playCurrentVideo,
                'onStateChange': onPlayerStateChange
            }
        });
        //ytPlayers.push(playerID);
        //ytPlayers[playerID]=player;
    }

    function formYoutubePlayer(playerID, youtubeID) {
        player = new YT.Player(playerID, {
            height: '315',
            width: '560',
            host: 'https://www.youtube-nocookie.com',
            videoId: youtubeID,
            events: {
                'onReady': playCurrentVideo,
                'onStateChange': onPlayerStateChange
            }
        });
        //ytPlayers[playerID]=player;
    }

    jQuery(window).load(function() {
        jQuery(".headervideoSection .videoWrapper").click(function() {
            var videoID = jQuery(this).find(".videoPlayer").attr("video-id");

            var isPlaylist = jQuery(this).find(".videoPlayer").attr("data-isplaylist");

            var videoElementID = "ytplayer-" + videoID + "-inline";

            var videoElement = '<div id="' + videoElementID + '"></div>';

            //	videoElementID = 'ytplayer' + videoID + '-popup-';

            //	videoElement = '<div id="' + videoElementID + '"></div>';

            jQuery.magnificPopup.open({
                items: {
                    src: "#ytEmbedAndClose",

                    type: "inline",
                },

                closeOnContentClick: false,

                closeOnContentClick: false,

                closeBtnInside: true,

                mainClass: "mfp-fade", // class to remove default margin from left and right side

                callbacks: {
                    open: function() {
                        jQuery(".youtubeLightBox").append(videoElement);

                        if (isPlaylist.toLowerCase() == "true") {
                            formYoutubePlaylist(videoID, videoElementID);
                        } else {
                            formYoutubePlayer(videoElementID, videoID);
                        }
                    },

                    beforeClose: function() {
                        jQuery(".ytOuterWrapper iframe").remove();
                    },
                },
            });
        });


        jQuery(".watch-wrapper a").click(function() {
            var videoID = jQuery(this).attr("video-id");

            var isPlaylist = jQuery(this).attr("data-isplaylist");

            var videoElementID = "ytplayer-" + videoID + "-inline";

            var videoElement = '<div id="' + videoElementID + '"></div>';

            //	videoElementID = 'ytplayer' + videoID + '-popup-';

            //	videoElement = '<div id="' + videoElementID + '"></div>';

            jQuery.magnificPopup.open({
                items: {
                    src: "#ytEmbedAndClose",

                    type: "inline",
                },

                closeOnContentClick: false,

                closeOnContentClick: false,

                closeBtnInside: true,

                mainClass: "mfp-fade", // class to remove default margin from left and right side

                callbacks: {
                    open: function() {
                        jQuery(".youtubeLightBox").append(videoElement);

                        if (isPlaylist.toLowerCase() == "true") {
                            formYoutubePlaylist(videoID, videoElementID);
                        } else {
                            formYoutubePlayer(videoElementID, videoID);
                        }
                    },

                    beforeClose: function() {
                        jQuery(".ytOuterWrapper iframe").remove();
                    },
                },
            });
        });
    });

    function playVideo(event) {
        event.target.playVideo();
    }
</script> </div>
<div class="merch" id="merchSectionstart">
<div>
<div class="merch-inner">
<div class="title">
        MERCH
    </div>
<div class="merch-inner-wrapper">
<div class="merch-content-wrapper">
<div class="merch-content content-one">
<div class="merch-img-wrapper">
<a href="https://store.atcofficial.com/prayer-hands-t-shirt-e-5.html" target="_blank"> <img src="images/tee.png"/></a>
</div>
<div class="merch-name">
<a href="https://store.atcofficial.com/prayer-hands-t-shirt-e-5.html" target="_blank">
                        TEE
</a>
</div>
</div>
<div class="merch-content content-two">
<div class="merch-img-wrapper">
<a href="https://store.atcofficial.com/flames-hoodie-b-5.html" target="_blank"><img src="images/hoodie.png"/></a>
</div>
<div class="merch-name">
<a href="https://store.atcofficial.com/flames-hoodie-b-5.html" target="_blank"> HOODIE </a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="all-video">
<a data-track="enter-store" href="https://store.atcofficial.com" target="_blank">see all merch</a>
</div>
</div>
<div class="footer" id="footerSectionstart">
<div class="footer-wrapper">
<ul class="social-icons">
<li>
<a data-track="facebook-ftr" href="//www.facebook.com/againstthecurrentband" target="_blank"> <i class="fab fa-facebook-f" id="facebook-icon-footer"></i></a>
</li>
<li>
<a data-track="twitter-ftr" href="//twitter.com/ATC_BAND" target="_blank"> <i class="fab fa-twitter" id="twitter-icon-footer"></i></a>
</li>
<li>
<a data-track="instagram-ftr" href="//www.instagram.com/AgainstTheCurrent/?hl=en" target="_blank"><i class="fab fa-instagram" id="instagram-icon-footer"></i></a>
</li>
<li>
<a data-track="youtube-ftr" href="//www.youtube.com/channel/UCxMsgwldMZiuFTD6jjv32yQ" target="_blank"> <i class="fab fa-youtube" id="youtube-icon-footer"></i></a>
</li>
<li>
<a data-track="spotify-ftr" href="//open.spotify.com/artist/6yhD1KjhLxIETFF7vIRf8B" target="_blank"><i class="fab fa-spotify" id="spotify-icon-footer"></i></a>
</li>
<li>
<a data-track="apple-music-ftr" href="//itunes.apple.com/gb/artist/against-the-current/id545086911" target="_blank"><i class="fab fa-apple" id="apple-icon-footer"></i></a>
</li>
</ul>
<div class="desk-footer-links">
<div class="line-1">
<span>©</span> <span>copyright</span> <span id="desk-year">2020</span> | <a href="http://www.fueledbyramen.com/privacy-policy" target="_blank">privacy policy</a>
<span>|</span> <a href="http://www.fueledbyramen.com/terms-use" target="_blank">terms of use</a> | <a href="http://www.fueledbyramen.com/privacy-policy#adchoices" target="_blank">ad choices</a> | <a href="https://www.wminewmedia.com/cookies-policy/" target="_blank">Cookies Policy</a>
<span class="sep-pipe"> | </span>
<a class="ot-sdk-show-settings" style="cursor:pointer">Cookies Settings</a>
</div>
</div>
</div> </div>
</body></html>
