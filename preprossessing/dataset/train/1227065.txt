<!DOCTYPE html>
<html lang="fr-FR">
<head>
<meta charset="utf-8"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://repairedesamazones.fr/WP/xmlrpc.php" rel="pingback"/>
<title>Page non trouvée – Le Repaire des Amazones</title>
<style>
		#wpadminbar #wp-admin-bar-cp_plugins_top_button .ab-icon:before {
			content: "\f533";
			top: 3px;
		}
		#wpadminbar #wp-admin-bar-cp_plugins_top_button .ab-icon {
			transform: rotate(45deg);
		}
		</style>
<style>
#wpadminbar #wp-admin-bar-wccp_free_top_button .ab-icon:before {
	content: "\f160";
	color: #02CA02;
	top: 3px;
}
#wpadminbar #wp-admin-bar-wccp_free_top_button .ab-icon {
	transform: rotate(45deg);
}
</style>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://repairedesamazones.fr/feed/" rel="alternate" title="Le Repaire des Amazones » Flux" type="application/rss+xml"/>
<link href="https://repairedesamazones.fr/comments/feed/" rel="alternate" title="Le Repaire des Amazones » Flux des commentaires" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/repairedesamazones.fr\/WP\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://repairedesamazones.fr/WP/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://repairedesamazones.fr/WP/wp-content/themes/edge/style.css?ver=5.5.3" id="edge-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="edge-style-inline-css" type="text/css">
	/****************************************************************/
						/*.... Color Style ....*/
	/****************************************************************/
	/* Nav and links hover */
	a,
	ul li a:hover,
	ol li a:hover,
	.top-header .widget_contact ul li a:hover, /* Top Header Widget Contact */
	.main-navigation a:hover, /* Navigation */
	.main-navigation ul li.current-menu-item a,
	.main-navigation ul li.current_page_ancestor a,
	.main-navigation ul li.current-menu-ancestor a,
	.main-navigation ul li.current_page_item a,
	.main-navigation ul li:hover > a,
	.main-navigation li.current-menu-ancestor.menu-item-has-children > a:after,
	.main-navigation li.current-menu-item.menu-item-has-children > a:after,
	.main-navigation ul li:hover > a:after,
	.main-navigation li.menu-item-has-children > a:hover:after,
	.main-navigation li.page_item_has_children > a:hover:after,
	.main-navigation ul li ul li a:hover,
	.main-navigation ul li ul li:hover > a,
	.main-navigation ul li.current-menu-item ul li a:hover,
	.header-search:hover, .header-search-x:hover, /* Header Search Form */
	.entry-title a:hover, /* Post */
	.entry-title a:focus,
	.entry-title a:active,
	.entry-meta span:hover,
	.entry-meta a:hover,
	.cat-links,
	.cat-links a,
	.tag-links,
	.tag-links a,
	.entry-meta .entry-format a,
	.entry-format:before,
	.entry-meta .entry-format:before,
	.entry-header .entry-meta .entry-format:before,
	.widget ul li a:hover,/* Widgets */
	.widget-title a:hover,
	.widget_contact ul li a:hover,
	.site-info .copyright a:hover, /* Footer */
	#colophon .widget ul li a:hover,
	#footer-navigation a:hover {
		color: #ed2381;
	}

	.cat-links,
	.tag-links {
		border-bottom-color: #ed2381;
	}

	/* Webkit */
	::selection {
		background: #ed2381;
		color: #fff;
	}
	/* Gecko/Mozilla */
	::-moz-selection {
		background: #ed2381;
		color: #fff;
	}


	/* Accessibility
	================================================== */
	.screen-reader-text:hover,
	.screen-reader-text:active,
	.screen-reader-text:focus {
		background-color: #f1f1f1;
		color: #ed2381;
	}

	/* Buttons reset, button, submit */

	input[type="reset"],/* Forms  */
	input[type="button"],
	input[type="submit"],
	.go-to-top a:hover {
		background-color:#ed2381;
	}

	/* Default Buttons */
	.btn-default:hover,
	.vivid,
	.search-submit {
		background-color: #ed2381;
		border: 1px solid #ed2381;
	}
	.go-to-top a {
		border: 2px solid #ed2381;
		color: #ed2381;
	}

	#colophon .widget-title:after {
		background-color: #ed2381;
	}

	/* -_-_-_ Not for change _-_-_- */
	.light-color:hover,
	.vivid:hover {
		background-color: #fff;
		border: 1px solid #fff;
	}

	ul.default-wp-page li a {
		color: #ed2381;
	}

	#bbpress-forums .bbp-topics a:hover {
	color: #ed2381;
	}
	.bbp-submit-wrapper button.submit {
		background-color: #ed2381;
		border: 1px solid #ed2381;
	}

	/* Woocommerce
	================================================== */
	.woocommerce #respond input#submit, 
	.woocommerce a.button, 
	.woocommerce button.button, 
	.woocommerce input.button,
	.woocommerce #respond input#submit.alt, 
	.woocommerce a.button.alt, 
	.woocommerce button.button.alt, 
	.woocommerce input.button.alt,
	.woocommerce-demo-store p.demo_store {
		background-color: #ed2381;
	}
	.woocommerce .woocommerce-message:before {
		color: #ed2381;
	}

</style>
<link href="https://repairedesamazones.fr/WP/wp-content/themes/edge/assets/font-awesome/css/font-awesome.min.css?ver=5.5.3" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://repairedesamazones.fr/WP/wp-content/themes/edge/css/responsive.css?ver=5.5.3" id="edge-responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Lato%3A400%2C300%2C700%2C400italic%7CPlayfair+Display&amp;ver=5.5.3" id="edge_google_fonts-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://repairedesamazones.fr/WP/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="edge-main-js" src="https://repairedesamazones.fr/WP/wp-content/themes/edge/js/edge-main.js?ver=5.5.3" type="text/javascript"></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='https://repairedesamazones.fr/WP/wp-content/themes/edge/js/html5.js?ver=3.7.3' id='html5-js'></script>
<![endif]-->
<link href="https://repairedesamazones.fr/wp-json/" rel="https://api.w.org/"/><link href="https://repairedesamazones.fr/WP/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://repairedesamazones.fr/WP/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<script id="wpcp_disable_selection" type="text/javascript">
var image_save_msg='You are not allowed to save images!';
	var no_menu_msg='Context Menu disabled!';
	var smessage = "Contenu protégé";

function disableEnterKey(e)
{
	var elemtype = e.target.tagName;
	
	elemtype = elemtype.toUpperCase();
	
	if (elemtype == "TEXT" || elemtype == "TEXTAREA" || elemtype == "INPUT" || elemtype == "PASSWORD" || elemtype == "SELECT" || elemtype == "OPTION" || elemtype == "EMBED")
	{
		elemtype = 'TEXT';
	}
	
	if (e.ctrlKey){
     var key;
     if(window.event)
          key = window.event.keyCode;     //IE
     else
          key = e.which;     //firefox (97)
    //if (key != 17) alert(key);
     if (elemtype!= 'TEXT' && (key == 97 || key == 65 || key == 67 || key == 99 || key == 88 || key == 120 || key == 26 || key == 85  || key == 86 || key == 83 || key == 43 || key == 73))
     {
		if(wccp_free_iscontenteditable(e)) return true;
		show_wpcp_message('You are not allowed to copy content or view source');
		return false;
     }else
     	return true;
     }
}


/*For contenteditable tags*/
function wccp_free_iscontenteditable(e)
{
	var e = e || window.event; // also there is no e.target property in IE. instead IE uses window.event.srcElement
  	
	var target = e.target || e.srcElement;

	var elemtype = e.target.nodeName;
	
	elemtype = elemtype.toUpperCase();
	
	var iscontenteditable = "false";
		
	if(typeof target.getAttribute!="undefined" ) iscontenteditable = target.getAttribute("contenteditable"); // Return true or false as string
	
	var iscontenteditable2 = false;
	
	if(typeof target.isContentEditable!="undefined" ) iscontenteditable2 = target.isContentEditable; // Return true or false as boolean

	if(target.parentElement.isContentEditable) iscontenteditable2 = true;
	
	if (iscontenteditable == "true" || iscontenteditable2 == true)
	{
		if(typeof target.style!="undefined" ) target.style.cursor = "text";
		
		return true;
	}
}

////////////////////////////////////
function disable_copy(e)
{	
	var e = e || window.event; // also there is no e.target property in IE. instead IE uses window.event.srcElement
	
	var elemtype = e.target.tagName;
	
	elemtype = elemtype.toUpperCase();
	
	if (elemtype == "TEXT" || elemtype == "TEXTAREA" || elemtype == "INPUT" || elemtype == "PASSWORD" || elemtype == "SELECT" || elemtype == "OPTION" || elemtype == "EMBED")
	{
		elemtype = 'TEXT';
	}
	
	if(wccp_free_iscontenteditable(e)) return true;
	
	var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
	
	var checker_IMG = '';
	if (elemtype == "IMG" && checker_IMG == 'checked' && e.detail >= 2) {show_wpcp_message(alertMsg_IMG);return false;}
	if (elemtype != "TEXT")
	{
		if (smessage !== "" && e.detail == 2)
			show_wpcp_message(smessage);
		
		if (isSafari)
			return true;
		else
			return false;
	}	
}

//////////////////////////////////////////
function disable_copy_ie()
{
	var e = e || window.event;
	var elemtype = window.event.srcElement.nodeName;
	elemtype = elemtype.toUpperCase();
	if(wccp_free_iscontenteditable(e)) return true;
	if (elemtype == "IMG") {show_wpcp_message(alertMsg_IMG);return false;}
	if (elemtype != "TEXT" && elemtype != "TEXTAREA" && elemtype != "INPUT" && elemtype != "PASSWORD" && elemtype != "SELECT" && elemtype != "OPTION" && elemtype != "EMBED")
	{
		return false;
	}
}	
function reEnable()
{
	return true;
}
document.onkeydown = disableEnterKey;
document.onselectstart = disable_copy_ie;
if(navigator.userAgent.indexOf('MSIE')==-1)
{
	document.onmousedown = disable_copy;
	document.onclick = reEnable;
}
function disableSelection(target)
{
    //For IE This code will work
    if (typeof target.onselectstart!="undefined")
    target.onselectstart = disable_copy_ie;
    
    //For Firefox This code will work
    else if (typeof target.style.MozUserSelect!="undefined")
    {target.style.MozUserSelect="none";}
    
    //All other  (ie: Opera) This code will work
    else
    target.onmousedown=function(){return false}
    target.style.cursor = "default";
}
//Calling the JS function directly just after body load
window.onload = function(){disableSelection(document.body);};

//////////////////special for safari Start////////////////
var onlongtouch;
var timer;
var touchduration = 1000; //length of time we want the user to touch before we do something

var elemtype = "";
function touchstart(e) {
	var e = e || window.event;
  // also there is no e.target property in IE.
  // instead IE uses window.event.srcElement
  	var target = e.target || e.srcElement;
	
	elemtype = window.event.srcElement.nodeName;
	
	elemtype = elemtype.toUpperCase();
	
	if(!wccp_pro_is_passive()) e.preventDefault();
	if (!timer) {
		timer = setTimeout(onlongtouch, touchduration);
	}
}

function touchend() {
    //stops short touches from firing the event
    if (timer) {
        clearTimeout(timer);
        timer = null;
    }
	onlongtouch();
}

onlongtouch = function(e) { //this will clear the current selection if anything selected
	
	if (elemtype != "TEXT" && elemtype != "TEXTAREA" && elemtype != "INPUT" && elemtype != "PASSWORD" && elemtype != "SELECT" && elemtype != "EMBED" && elemtype != "OPTION")	
	{
		if (window.getSelection) {
			if (window.getSelection().empty) {  // Chrome
			window.getSelection().empty();
			} else if (window.getSelection().removeAllRanges) {  // Firefox
			window.getSelection().removeAllRanges();
			}
		} else if (document.selection) {  // IE?
			document.selection.empty();
		}
		return false;
	}
};

document.addEventListener("DOMContentLoaded", function(event) { 
    window.addEventListener("touchstart", touchstart, false);
    window.addEventListener("touchend", touchend, false);
});

function wccp_pro_is_passive() {

  var cold = false,
  hike = function() {};

  try {
	  const object1 = {};
  var aid = Object.defineProperty(object1, 'passive', {
  get() {cold = true}
  });
  window.addEventListener('test', hike, aid);
  window.removeEventListener('test', hike, aid);
  } catch (e) {}

  return cold;
}
/*special for safari End*/
</script>
<script id="wpcp_disable_Right_Click" type="text/javascript">
document.ondragstart = function() { return false;}
	function nocontext(e) {
	   return false;
	}
	document.oncontextmenu = nocontext;
</script>
<style>
.unselectable
{
-moz-user-select:none;
-webkit-user-select:none;
cursor: default;
}
html
{
-webkit-touch-callout: none;
-webkit-user-select: none;
-khtml-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
-webkit-tap-highlight-color: rgba(0,0,0,0);
}
</style>
<script id="wpcp_css_disable_selection" type="text/javascript">
var e = document.getElementsByTagName('body')[0];
if(e)
{
	e.setAttribute('unselectable',on);
}
</script>
<meta content="width=device-width" name="viewport"/>
<!-- Custom CSS -->
<style media="screen" type="text/css">
/*Slider Content With background color*/
									.slider-content {
										background: rgba(255, 255, 255, 0.5);
										border: 10px double rgba(255, 255, 255, 0.5);
										padding: 20px 30px 30px;
									}
/*Disabled First Big Letter */
									.post:first-child .entry-content p:first-child:first-letter {
									 border-right: none;
									 display: inherit;
									 float: inherit;
									 font-family: inherit;
									 font-size: inherit;
									 line-height: inherit;
									 margin-bottom: inherit;
									 margin-right: inherit;
									 margin-top: inherit;
									 padding: inherit;
									 text-align: inherit;
									}
</style>
</head>
<body class="error404 wp-embed-responsive unselectable has-header-image">
<div class="hfeed site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<!-- Masthead ============================================= -->
<header class="site-header" id="masthead" role="banner">
<a href="https://repairedesamazones.fr/"><img alt="Le Repaire des Amazones" class="header-image" height="542" src="https://repairedesamazones.fr/WP/wp-content/uploads/2019/10/cropped-image_310.jpeg" width="1570"/> </a>
<div class="top-header">
<div class="container clearfix">
<!-- Contact Us ============================================= --><aside class="widget widget_contact" id="edge_contact_widgets-3"> <h3 class="widget-title">Contact</h3> <!-- end .widget-title -->
<ul>
<li><a href="https://www.facebook.com/cleomene.muceignet.5" target="_blank" title="Facebook"><i class="fa fa-map-marker"> </i> Facebook</a></li>
<li><a href="mailto:repaireamazones@gmail.com" title="repaireamazones@gmail.com"><i class="fa fa-envelope-o"> </i> repaireamazones@gmail.com</a></li>
</ul>
</aside><!-- end .widget_contact --><div class="header-social-block"> <div class="social-links clearfix">
</div><!-- end .social-links -->
</div><!-- end .header-social-block --><div id="site-branding"> <h2 id="site-title"> <a href="https://repairedesamazones.fr/" rel="home" title="Le Repaire des Amazones"> Le Repaire des Amazones </a>
</h2> <!-- end .site-title --> <div id="site-description"> Dans la tanière des femme-guerrières </div> <!-- end #site-description -->
</div> </div> <!-- end .container -->
</div> <!-- end .top-header -->
<!-- Main Header============================================= -->
<div id="sticky_header">
<div class="container clearfix">
<h3 class="nav-site-title">
<a href="https://repairedesamazones.fr/" title="Le Repaire des Amazones">Le Repaire des Amazones</a>
</h3>
<!-- end .nav-site-title -->
<!-- Main Nav ============================================= -->
<nav aria-label="Main Menu" class="main-navigation clearfix" id="site-navigation" role="navigation">
<button aria-controls="primary-menu" aria-expanded="false" class="menu-toggle">
<span class="line-one"></span>
<span class="line-two"></span>
<span class="line-three"></span>
</button>
<!-- end .menu-toggle -->
<ul class="menu nav-menu" id="primary-menu"><li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-40" id="menu-item-40"><a href="https://repairedesamazones.fr/category/blog/">Journal intime</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-3038" id="menu-item-3038"><a href="https://repairedesamazones.fr/category/fiction/">Extraits</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-3031" id="menu-item-3031"><a href="https://repairedesamazones.fr/category/ebooks/">Ebooks</a></li>
</ul> </nav> <!-- end #site-navigation -->
<button class="header-search" id="search-toggle" type="button"></button>
<div class="clearfix" id="search-box">
<form action="https://repairedesamazones.fr/" class="search-form" method="get">
<input autocomplete="off" class="search-field" name="s" placeholder="Search …" type="search"/>
<button class="search-submit" type="submit"><i class="fa fa-search"></i></button>
</form> <!-- end .search-form --> </div> <!-- end #search-box -->
</div> <!-- end .container -->
</div> <!-- end #sticky_header --></header> <!-- end #masthead -->
<!-- Main Page Start ============================================= -->
<div id="content">
<div class="container clearfix">
<div class="page-header">
<h1 class="page-title">Page NOT Found</h1>
<!-- .page-title -->
<!-- .breadcrumb -->
</div>
<!-- .page-header -->
<div id="primary">
<main class="site-main clearfix" id="main" role="main">
<article class="post error404 not-found" id="post-0">
<section class="error-404 not-found">
<header class="page-header">
<h2 class="page-title"> Oops! That page can’t be found. </h2>
</header> <!-- .page-header -->
<div class="page-content">
<p> It looks like nothing was found at this location. Maybe try a search? </p>
<form action="https://repairedesamazones.fr/" class="search-form" method="get">
<input autocomplete="off" class="search-field" name="s" placeholder="Search …" type="search"/>
<button class="search-submit" type="submit"><i class="fa fa-search"></i></button>
</form> <!-- end .search-form --> </div> <!-- .page-content -->
</section> <!-- .error-404 -->
</article> <!-- #post-0 .post .error404 .not-found -->
</main> <!-- #main -->
</div> <!-- #primary -->
<aside id="secondary" role="complementary">
<aside class="widget widget_recent_entries" id="recent-posts-9">
<h2 class="widget-title">Nouveautés</h2>
<ul>
<li>
<a href="https://repairedesamazones.fr/lage-de-la-maturite/">L’âge de la maturité</a>
<span class="post-date">3 novembre 2020</span>
</li>
<li>
<a href="https://repairedesamazones.fr/la-boite-de-pandore-initiee-a-la-soumission/">La boîte de Pandore : Initiée à la soumission</a>
<span class="post-date">11 octobre 2020</span>
</li>
<li>
<a href="https://repairedesamazones.fr/docteur-follanus-therapie-sexuelle/">Docteur Follanus : thérapie sexuelle</a>
<span class="post-date">6 octobre 2020</span>
</li>
<li>
<a href="https://repairedesamazones.fr/aneries-recueil-des-femmes-et-des-equides/">Âneries [recueil] : des femmes et des équidés</a>
<span class="post-date">2 octobre 2020</span>
</li>
<li>
<a href="https://repairedesamazones.fr/le-petit-plaisir-de-julie/">Le petit plaisir de Julie</a>
<span class="post-date">30 septembre 2020</span>
</li>
</ul>
</aside><aside class="widget widget_archive" id="archives-9"><h2 class="widget-title">Archives</h2> <label class="screen-reader-text" for="archives-dropdown-9">Archives</label>
<select id="archives-dropdown-9" name="archive-dropdown">
<option value="">Sélectionner un mois</option>
<option value="https://repairedesamazones.fr/2020/11/"> novembre 2020  (1)</option>
<option value="https://repairedesamazones.fr/2020/10/"> octobre 2020  (3)</option>
<option value="https://repairedesamazones.fr/2020/09/"> septembre 2020  (6)</option>
<option value="https://repairedesamazones.fr/2020/08/"> août 2020  (7)</option>
<option value="https://repairedesamazones.fr/2020/07/"> juillet 2020  (1)</option>
<option value="https://repairedesamazones.fr/2020/06/"> juin 2020  (2)</option>
<option value="https://repairedesamazones.fr/2020/05/"> mai 2020  (2)</option>
<option value="https://repairedesamazones.fr/2020/04/"> avril 2020  (2)</option>
<option value="https://repairedesamazones.fr/2020/03/"> mars 2020  (2)</option>
<option value="https://repairedesamazones.fr/2020/02/"> février 2020  (1)</option>
<option value="https://repairedesamazones.fr/2019/12/"> décembre 2019  (2)</option>
<option value="https://repairedesamazones.fr/2019/11/"> novembre 2019  (2)</option>
<option value="https://repairedesamazones.fr/2019/10/"> octobre 2019  (4)</option>
<option value="https://repairedesamazones.fr/2019/04/"> avril 2019  (2)</option>
<option value="https://repairedesamazones.fr/2019/02/"> février 2019  (13)</option>
<option value="https://repairedesamazones.fr/2019/01/"> janvier 2019  (1)</option>
<option value="https://repairedesamazones.fr/2018/10/"> octobre 2018  (1)</option>
<option value="https://repairedesamazones.fr/2018/06/"> juin 2018  (1)</option>
<option value="https://repairedesamazones.fr/2018/05/"> mai 2018  (2)</option>
<option value="https://repairedesamazones.fr/2018/04/"> avril 2018  (1)</option>
<option value="https://repairedesamazones.fr/2018/03/"> mars 2018  (4)</option>
<option value="https://repairedesamazones.fr/2018/02/"> février 2018  (2)</option>
<option value="https://repairedesamazones.fr/2017/12/"> décembre 2017  (2)</option>
<option value="https://repairedesamazones.fr/2017/11/"> novembre 2017  (1)</option>
<option value="https://repairedesamazones.fr/2017/10/"> octobre 2017  (1)</option>
<option value="https://repairedesamazones.fr/2017/08/"> août 2017  (3)</option>
<option value="https://repairedesamazones.fr/2017/07/"> juillet 2017  (3)</option>
<option value="https://repairedesamazones.fr/2017/06/"> juin 2017  (5)</option>
<option value="https://repairedesamazones.fr/2017/05/"> mai 2017  (4)</option>
<option value="https://repairedesamazones.fr/2017/04/"> avril 2017  (1)</option>
<option value="https://repairedesamazones.fr/2017/03/"> mars 2017  (2)</option>
<option value="https://repairedesamazones.fr/2016/12/"> décembre 2016  (2)</option>
<option value="https://repairedesamazones.fr/2016/11/"> novembre 2016  (2)</option>
<option value="https://repairedesamazones.fr/2016/10/"> octobre 2016  (4)</option>
<option value="https://repairedesamazones.fr/2016/08/"> août 2016  (1)</option>
<option value="https://repairedesamazones.fr/2016/07/"> juillet 2016  (5)</option>
<option value="https://repairedesamazones.fr/2016/06/"> juin 2016  (6)</option>
<option value="https://repairedesamazones.fr/2016/05/"> mai 2016  (5)</option>
<option value="https://repairedesamazones.fr/2016/04/"> avril 2016  (4)</option>
<option value="https://repairedesamazones.fr/2016/03/"> mars 2016  (3)</option>
<option value="https://repairedesamazones.fr/2016/01/"> janvier 2016  (6)</option>
<option value="https://repairedesamazones.fr/2015/12/"> décembre 2015  (2)</option>
<option value="https://repairedesamazones.fr/2015/11/"> novembre 2015  (4)</option>
<option value="https://repairedesamazones.fr/2015/10/"> octobre 2015  (4)</option>
<option value="https://repairedesamazones.fr/2015/09/"> septembre 2015  (1)</option>
<option value="https://repairedesamazones.fr/2015/08/"> août 2015  (3)</option>
<option value="https://repairedesamazones.fr/2015/07/"> juillet 2015  (2)</option>
<option value="https://repairedesamazones.fr/2015/06/"> juin 2015  (2)</option>
<option value="https://repairedesamazones.fr/2015/05/"> mai 2015  (2)</option>
<option value="https://repairedesamazones.fr/2015/04/"> avril 2015  (1)</option>
<option value="https://repairedesamazones.fr/2014/10/"> octobre 2014  (1)</option>
<option value="https://repairedesamazones.fr/2014/08/"> août 2014  (1)</option>
<option value="https://repairedesamazones.fr/2014/07/"> juillet 2014  (1)</option>
<option value="https://repairedesamazones.fr/2013/12/"> décembre 2013  (2)</option>
<option value="https://repairedesamazones.fr/2013/10/"> octobre 2013  (1)</option>
<option value="https://repairedesamazones.fr/2013/08/"> août 2013  (1)</option>
<option value="https://repairedesamazones.fr/2013/06/"> juin 2013  (1)</option>
<option value="https://repairedesamazones.fr/2013/05/"> mai 2013  (1)</option>
<option value="https://repairedesamazones.fr/2013/01/"> janvier 2013  (2)</option>
<option value="https://repairedesamazones.fr/2012/08/"> août 2012  (2)</option>
<option value="https://repairedesamazones.fr/2012/07/"> juillet 2012  (4)</option>
<option value="https://repairedesamazones.fr/2012/06/"> juin 2012  (2)</option>
<option value="https://repairedesamazones.fr/2012/05/"> mai 2012  (4)</option>
<option value="https://repairedesamazones.fr/2012/04/"> avril 2012  (2)</option>
<option value="https://repairedesamazones.fr/2012/03/"> mars 2012  (1)</option>
<option value="https://repairedesamazones.fr/2012/02/"> février 2012  (1)</option>
<option value="https://repairedesamazones.fr/2012/01/"> janvier 2012  (1)</option>
<option value="https://repairedesamazones.fr/2011/12/"> décembre 2011  (1)</option>
<option value="https://repairedesamazones.fr/2011/10/"> octobre 2011  (1)</option>
</select>
<script type="text/javascript">
/* <![CDATA[ */
(function() {
	var dropdown = document.getElementById( "archives-dropdown-9" );
	function onSelectChange() {
		if ( dropdown.options[ dropdown.selectedIndex ].value !== '' ) {
			document.location.href = this.options[ this.selectedIndex ].value;
		}
	}
	dropdown.onchange = onSelectChange;
})();
/* ]]> */
</script>
</aside></aside> <!-- #secondary -->
</div> <!-- end .container -->
</div> <!-- end #content -->
<!-- Footer Start ============================================= -->
<footer class="site-footer clearfix" id="colophon" role="contentinfo">
<div class="site-info">
<div class="container">
<div class="social-links clearfix">
</div><!-- end .social-links -->
<nav aria-label="Footer Menu" id="footer-navigation" role="navigation"><ul><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3016" id="menu-item-3016"><a href="https://repairedesamazones.fr/mentions-legales/">Mentions légales</a></li>
</ul></nav><!-- end #footer-navigation --><div class="copyright">© 2021 			<a href="https://repairedesamazones.fr/" target="_blank" title="Le Repaire des Amazones">Le Repaire des Amazones</a> | 
							Designed by: <a href="https://themefreesia.com" target="_blank" title="Theme Freesia">Theme Freesia</a> | 
							Powered by: <a href="http://wordpress.org" target="_blank" title="WordPress">WordPress</a>
</div>
<div style="clear:both;"></div>
</div> <!-- end .container -->
</div> <!-- end .site-info -->
<button class="go-to-top"><a href="#masthead" title="Go to Top"><i class="fa fa-angle-double-up"></i></a></button> <!-- end .go-to-top -->
</footer> <!-- end #colophon -->
</div> <!-- end #page -->
<div class="msgmsg-box-wpcp hideme" id="wpcp-error-message"><span>error: </span>Contenu protégé</div>
<script>
	var timeout_result;
	function show_wpcp_message(smessage)
	{
		if (smessage !== "")
			{
			var smessage_text = '<span>Alert: </span>'+smessage;
			document.getElementById("wpcp-error-message").innerHTML = smessage_text;
			document.getElementById("wpcp-error-message").className = "msgmsg-box-wpcp warning-wpcp showme";
			clearTimeout(timeout_result);
			timeout_result = setTimeout(hide_message, 3000);
			}
	}
	function hide_message()
	{
		document.getElementById("wpcp-error-message").className = "msgmsg-box-wpcp warning-wpcp hideme";
	}
	</script>
<style>
	@media print {
	body * {display: none !important;}
		body:after {
		content: "Copie non autorisée, merci"; }
	}
	</style>
<style type="text/css">
	#wpcp-error-message {
	    direction: ltr;
	    text-align: center;
	    transition: opacity 900ms ease 0s;
	    z-index: 99999999;
	}
	.hideme {
    	opacity:0;
    	visibility: hidden;
	}
	.showme {
    	opacity:1;
    	visibility: visible;
	}
	.msgmsg-box-wpcp {
		border:1px solid #f5aca6;
		border-radius: 10px;
		color: #555;
		font-family: Tahoma;
		font-size: 11px;
		margin: 10px;
		padding: 10px 36px;
		position: fixed;
		width: 255px;
		top: 50%;
  		left: 50%;
  		margin-top: -10px;
  		margin-left: -130px;
  		-webkit-box-shadow: 0px 0px 34px 2px rgba(242,191,191,1);
		-moz-box-shadow: 0px 0px 34px 2px rgba(242,191,191,1);
		box-shadow: 0px 0px 34px 2px rgba(242,191,191,1);
	}
	.msgmsg-box-wpcp span {
		font-weight:bold;
		text-transform:uppercase;
	}
		.warning-wpcp {
		background:#ffecec url('https://repairedesamazones.fr/WP/wp-content/plugins/wp-content-copy-protector/images/warning.png') no-repeat 10px 50%;
	}
    </style>
<script id="jquery_cycle_all-js" src="https://repairedesamazones.fr/WP/wp-content/themes/edge/js/jquery.cycle.all.js?ver=5.5.3" type="text/javascript"></script>
<script id="edge_slider-js-extra" type="text/javascript">
/* <![CDATA[ */
var edge_slider_value = {"transition_effect":"fade","transition_delay":"4000","transition_duration":"1000"};
/* ]]> */
</script>
<script id="edge_slider-js" src="https://repairedesamazones.fr/WP/wp-content/themes/edge/js/edge-slider-setting.js?ver=5.5.3" type="text/javascript"></script>
<script id="edge-navigation-js" src="https://repairedesamazones.fr/WP/wp-content/themes/edge/js/navigation.js?ver=5.5.3" type="text/javascript"></script>
<script id="edge-skip-link-focus-fix-js" src="https://repairedesamazones.fr/WP/wp-content/themes/edge/js/skip-link-focus-fix.js?ver=5.5.3" type="text/javascript"></script>
<script id="wp-embed-js" src="https://repairedesamazones.fr/WP/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
</body>
</html>