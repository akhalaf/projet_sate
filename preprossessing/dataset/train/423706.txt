<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<title>Home | Divya Vaibhav Gaikwad</title>
<meta content="" name="description"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- Favicon -->
<!-- 
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico">
     -->
<!-- CSS
	============================================ -->
<!-- Bootstrap CSS -->
<link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Icon Font CSS -->
<link href="assets/css/icon-font.min.css" rel="stylesheet"/>
<!-- Plugins CSS -->
<link href="assets/css/plugins.css" rel="stylesheet"/>
<!-- VMap CSS -->
<link href="assets/css/jqvmap.min.css" rel="stylesheet"/>
<!-- Main Style CSS -->
<link href="assets/css/style.css" rel="stylesheet"/>
<!-- Modernizer JS -->
<script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>
<!-- Main Wrapper Start -->
<div class="main-wrapper">
<!-- Header Section Start -->
<div class="header-section section">
<!-- Header Top Start -->
<div class="header-top">
<div class="container">
<div class="row">
<div class="col">
<!-- Header Social Start -->
<div class="header-social">
<a href="https://www.facebook.com/divyavaibhavgaikwad" target="_blank"><i class="fa fa-facebook"></i></a>
<a href="https://twitter.com/divyavaibhav64" target="_blank"><i class="fa fa-twitter"></i></a>
<a href="https://plus.google.com/+Divyavaibhavgaikwad64" target="_blank"><i class="fa fa-google-plus"></i></a>
<a href="https://www.youtube.com/channel/UCOswBWnQjNRcuMbwlxyOvgw" target="_blank"><i class="fa fa-youtube"></i></a>
</div><!-- Header Social End -->
</div>
<div class="col">
<!-- Header Top Links Start -->
<div class="header-links">
<a href="mailto:96641 49999"><i class="fa fa-phone"></i> 96641 49999</a>
<a href="mailto:92229 66666"><i class="fa fa-phone"></i> 92229 66666</a>
</div><!-- Header Top Links End -->
</div>
</div>
</div>
</div><!-- Header Top End -->
<!-- Header Bottom Start -->
<div class="header-bottom header-sticky">
<div class="container">
<div class="row">
<!-- Logo Start -->
<div class="col">
<a class="logo" href="index.html"><img alt="Divya Vaibhav Gaikwad Logo" src="assets/images/dvg-logo.png"/></a>
</div><!-- Logo End -->
<!-- Main Menu Start -->
<div class="col d-none d-lg-block">
<div class="main-menu">
<nav>
<!-- 
                                <ul>
                                    <li class="active"><a href="index.html">home</a></li>
                                    <li><a href="about-us.html">about</a></li>
                                    <li class="menu-item-has-children"><a href="campaign.html">campaign</a>
                                        <ul class="sub-menu">
                                            <li><a href="campaign.html">campaign</a></li>
                                            <li><a href="campaign-details.html">campaign details</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children"><a href="gallery.html">gallery</a>
                                        <ul class="sub-menu">
                                            <li><a href="gallery-2-column.html">gallery 2 column</a></li>
                                            <li><a href="gallery-3-column.html">gallery 3 column</a></li>
                                            <li><a href="gallery.html">gallery 4 column</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children"><a href="news.html">news</a>
                                        <ul class="sub-menu">
                                            <li><a href="news.html">news</a></li>
                                            <li><a href="news-details.html">news details</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="contact.html">contact</a></li>
                                </ul>
                                -->
<ul>
<li class="active"><a href="index.html">home</a></li>
<li class="menu-item-has-children"><a href="#">About</a>
<ul class="sub-menu">
<li><a href="divyavaibhavgaikwad-biography.html">Biography</a></li>
<li><a href="divyavaibhavgaikwad-party.html">Party</a></li>
<li><a href="divyavaibhavgaikwad-vision-mission.html">Vision Mission</a></li>
</ul>
</li>
<li class="menu-item-has-children"><a href="#">Projects</a>
<ul class="sub-menu">
<li><a href="divyavaibhavgaikwad-ongoing-upcoming-projects.html">Ongoing &amp; Upcomng</a></li>
<!-- <li><a href="divyavaibhavgaikwad-upcoming-projects.html">Upcoming</a></li> -->
<li><a href="divyavaibhavgaikwad-past-projects.html">Past</a></li>
</ul>
</li>
<li class="menu-item-has-children"><a href="#">NGO's</a>
<ul class="sub-menu">
<li><a href="divyavaibhavgaikwad-healthcare-ngo.html">Healthcare</a></li>
<li><a href="divyavaibhavgaikwad-education-ngo.html">Education</a></li>
<li><a href="divyavaibhavgaikwad-social-ngo.html">Social</a></li>
</ul>
</li>
<li class="menu-item-has-children"><a href="#">Media</a>
<ul class="sub-menu">
<li><a href="divyavaibhavgaikwad-marathi-media.html">Marathi</a></li>
<li><a href="divyavaibhavgaikwad-english-media.html">English</a></li>
<li><a href="divyavaibhavgaikwad-interaction-media.html">Interaction</a></li>
</ul>
</li>
<li><a href="divyavaibhavgaikwad-contact-us.html">Get Enrolled</a></li>
</ul>
</nav>
</div>
</div><!-- Main Menu End -->
<!-- Header Button Start -->
<!-- 
                    <div class="col">
                        <div class="header-button">
                            <a href="index.html#donation" class="btn btn-red">donate now</a>
                        </div>
                    </div>
 -->
<!-- Header Button End -->
<!-- Mobile Menu Start -->
<div class="mobile-menu d-block d-lg-none col"></div><!-- Mobile Menu End -->
</div>
</div>
</div><!-- Header Bottom End -->
</div><!-- Header Section End -->
<!-- Hero Section Start -->
<div class="hero-section section">
<!-- Hero Slider Start -->
<div class="hero-slider">
<!-- Hero Item Start -->
<div class="hero-item" data-opacity="5" data-overlay="black" style="background-image: url('assets/images/bg/slider-bg.jpg')">
<div class="container">
<div class="row align-items-end">
<!-- Hero Content -->
<div class="hero-content col-md-7 col-12">
<div class="title"><h1>Divya</h1></div>
<div class="title-2"><h1>Vaibhav Gaikwad</h1></div>
<div class="sub-title"><h3>Corporator NMMC, Ward No 64 <br/>Vashi, Navi Mumbai</h3></div>
<div class="button"><a class="btn btn-lg btn-red" href="divyavaibhavgaikwad-contact-us.html">Become a Volunteer</a></div>
</div>
<!-- Hero Image -->
<div class="hero-image col-md-5 col-12 ">
<img alt="divyavaibhavgaikwad" src="assets/images/hero/divyavaibhavgaikwad.png"/>
</div>
</div>
</div>
</div><!-- Hero Item End -->
<!-- Hero Item Start -->
<!-- 
            <div class="hero-item" style="background-image: url('assets/images/bg/hero-bg-2.jpg')" data-overlay="black" data-opacity="5">
                <div class="container">
                    <div class="row align-items-end">
                       
                        <div class="hero-content col-md-7 col-12">

                           <div class="title"><h1>Steven</h1></div>
                           <div class="title-2"><h1>Henderson</h1></div>
                           <div class="sub-title"><h3>Bangladeshi Politician</h3></div>
                           <div class="button"><a href="contact.html" class="btn btn-lg btn-red">Become a Volunteer</a></div>

                        </div>
                        
                        
                        <div class="hero-image col-md-5 col-12">
                           <img src="assets/images/hero/2.png" alt="Hero Image">
                        </div>
                        
                    </div>
                </div>
            </div>
            -->
<!-- Hero Item End -->
</div><!-- Hero Slider End -->
</div><!-- Hero Section End -->
<!-- Feature Section Start -->
<div class="feature-section section pt-110 pb-90">
<div class="container">
<div class="row">
<!-- Section Title Start -->
<div class="section-title text-center col-12 mb-80">
<h1>Always at your service sincerely</h1>
<p>Corporator For 
Ward No. 64 
Sector 1A, 3,4, 5, 6, 7 &amp; 8, Vashi.</p>
</div><!-- Section Title End -->
<!-- Feature Start-->
<div class="feature text-center col-md-4 col-12 mb-30">
<!-- Icon -->
<div class="icon">
<i class="icon-anchor"></i>
<!-- Shape -->
<span class="shape"></span>
</div>
<!-- Content -->
<div class="content">
<h4>Mission</h4>
<p>To make a contribution to the community by making, sharing, &amp; understanding them for their advancement.</p>
</div>
</div><!-- Feature End-->
<!-- Feature Start-->
<div class="feature text-center col-md-4 col-12 mb-30">
<!-- Icon -->
<div class="icon">
<i class="icon-layers"></i>
<!-- Shape -->
<span class="shape"></span>
</div>
<!-- Content -->
<div class="content">
<h4>Vision</h4>
<p>Promote appreciation for &amp; understanding among communities. Engaging in &amp; providing leadership to foster social &amp; economic justice.</p>
</div>
</div><!-- Feature End-->
<!-- Feature Start-->
<div class="feature text-center col-md-4 col-12 mb-30">
<!-- Icon -->
<div class="icon">
<i class="icon-megaphone"></i>
<!-- Shape -->
<span class="shape"></span>
</div>
<!-- Content -->
<div class="content">
<h4>Campaigns</h4>
<p>Run more camapaigns for the need of the people, to undertstand their problems as much as possible.</p>
</div>
</div><!-- Feature End-->
</div>
</div>
</div><!-- Feature Section End -->
<!-- Donation Section Start -->
<div class="donation-section section pt-110 pb-90" data-opacity="6.5" data-overlay="black" id="donation" style="background-image: url('assets/images/bg/about-me-bg.jpg')">
<div class="container">
<div class="row">
<!-- Section Title Start -->
<div class="section-title title-white text-center col-12 mb-60">
<h1>Little About Me</h1>
<p>Myself Divya Vaibhav Gaikwad, am Corporator of newly formed Ward No. 64- consisting of Sector 1A, 3, 4, 5, 6, 7 &amp; 8, Vashi.</p>
</div><!-- Section Title End -->
<div class="col-12">
<!-- Donation Form Start -->
<!-- 
                    <form action="#">
                        <div class="donation-form row">
                            <div class="col-lg-3 col-md-6 col-12 mb-30"><input type="text" placeholder="Full Name"></div>
                            <div class="col-lg-3 col-md-6 col-12 mb-30"><input type="email" placeholder="E-Mail"></div>
                            <div class="col-lg-3 col-md-6 col-12 mb-30"><input type="text" placeholder="Donation, $"></div>
                            <div class="col-lg-3 col-md-6 col-12 mb-30"><button class="btn btn-red btn-hover-dark-red">donate now</button></div>
                        </div>
                    </form>
                     -->
<!-- Donation Form End -->
</div>
</div>
</div>
</div><!-- Donation Section End -->
<!-- Campaign Section Start -->
<div class="campaign-section section pt-110 pb-90">
<div class="container">
<div class="row">
<!-- Section Title Start -->
<div class="section-title text-center col-12 mb-60">
<h1>Ongoing Projects</h1>
<p>These works have been carried out in consultation with the Members of the Society.</p>
</div><!-- Section Title End -->
<!-- Campaign Start -->
<div class="col-lg-4 col-md-6 col-12 mb-30">
<div class="campaign">
<!-- Image -->
<div class="image"><img alt="Footpath Work" src="assets/images/ongoing-projects/garbage-bins-distribution.jpg"/></div>
<!-- Content -->
<div class="content">
<h3><a href="#">Garbage Bins Distribution </a></h3>
<ul>
<li><i class="icon-map-pin"></i> Multiple Soc. Sec - 4, Vashi</li>
<li><i class="icon-alarmclock"></i>01 Mar 2019</li>
</ul>
</div>
</div>
</div><!-- Campaign End -->
<!-- Campaign Start -->
<div class="col-lg-4 col-md-6 col-12 mb-30">
<div class="campaign">
<!-- Image -->
<div class="image"><img alt="Cabling" src="assets/images/ongoing-projects/save-water.jpg"/></div>
<!-- Content -->
<div class="content">
<h3><a href="#">Save Water</a></h3>
<ul>
<li><i class="icon-map-pin"></i>Vashi</li>
<li><i class="icon-alarmclock"></i>07 Feb 2016</li>
</ul>
</div>
</div>
</div><!-- Campaign End -->
<!-- Campaign Start -->
<div class="col-lg-4 col-md-6 col-12 mb-30">
<div class="campaign">
<!-- Image -->
<div class="image"><img alt="Naala Work" src="assets/images/ongoing-projects/farm-to-home.jpg"/></div>
<!-- Content -->
<div class="content">
<h3><a href="#">Farm to Home</a></h3>
<ul>
<li><i class="icon-map-pin"></i>Vashi</li>
<li><i class="icon-alarmclock"></i>18 Oct 2018</li>
</ul>
</div>
</div>
</div><!-- Campaign End -->
<div class="contiribution-content text-center col">
<a class="btn btn-lg btn-black btn-hover-dark-red" href="divyavaibhavgaikwad-ongoing-projects.html">View all Projects</a>
</div><!-- Contiribution Content End -->
</div>
</div>
</div><!-- Campaign Section End -->
<!-- Contiribution Section Start -->
<div class="contiribution-section section bg-red pt-110 pb-120" style="background-image: url('assets/images/bg/orange-bg.png')">
<div class="container">
<div class="row">
<!-- Contiribution Content Start -->
<div class="contiribution-content text-center col">
<h4>My husband Mr. Vaibhav Gaikwad, Ex Corporator of Ward No. 53 (now Ward No. 64), Vashi has been doing âSustainable Developmentâ works.</h4>
<h1 class="counter">200</h1>
<h5>Projects completed So Far</h5>
<a class="btn btn-lg btn-black btn-hover-dark-red" href="http://www.vaibhavgaikwad.com/">View all his Projects</a>
</div><!-- Contiribution Content End -->
</div>
</div>
</div><!-- Contiribution Section End -->
<!-- Testimonial Section Start -->
<div class="testimonial-section section">
<div class="container">
<div class="row">
<div class="col-lg-10 col-12 ml-auto mr-auto">
<!-- Testimonial Slider Start -->
<div class="testimonial-slider" style="background-image: url('assets/images/bg/testimonial-bg.png')">
<!-- Testimonial Item -->
<div class="testimonial">
<p>A big thank you to our corporator, Divya Vaibhav Gaikwad for giving us an opportunity to work with her on sustainable empowerment with newspaper bag making skill training workshop held at their senior citizen club, Vashi. </p>
<h5>Meena Bhatia</h5>
</div>
<!-- Testimonial Item -->
<div class="testimonial">
<p>Every morning I go for a walk with my parents and husband. We notice all the changes that have occurred in our area... right from cleaning of drains well before monsoon to the sweepers doing their job diligently then ever before. The trees and plants on the divider are much healthier with the regular water being provided to them. And last but not the least the walls... they make our morning walk so much more colourful!
We have been away from India for a while but after returning back to Vashi we are not disappointed, rather quite happy on how things are shaping up thanks to your leadership. Places like Bandra or Andheri seem like they are in shambles unfortunately and I breathe a sign of relief when I come back to Vashi!</p>
<h5>Hina Malik</h5>
</div>
<!-- Testimonial Item -->
<div class="testimonial">
<p>Fantastic work to beautify ward 64! Have never seen a better public representative than you Mam. You are a super woman who has just set the standard very high for anyone to even come close to match it.</p>
<h5>Anand Sreenivasan</h5>
</div>
</div><!-- Testimonial Slider End -->
</div>
</div>
</div>
</div>
<!-- Testimonial Section End -->
<!-- Mission Section Start -->
<!-- 
    <div class="mission-section section pt-110 pb-90">
        <div class="container">
            <div class="row align-items-center">
                
                
                <div class="col-lg-6 col-12 order-lg-2 mb-30">
                    
                    <div class="mission-map" id="world-vmap"></div>
                    
                </div>
                
                
                <div class="mission-content col-lg-6 col-12 mb-30">
                    
                    <h5>MISSION & VISION</h5>
                    <h2>Changes We Need</h2>
                    <p>If you use this site regularly and would like to help keep the site on the Internet, please consider donating a small sum to help pay for the hosting and bandwidth bill.</p>
                    <h3>Jacob <br>Cunninghm</h3>
                    
                </div>
                
                
            </div>
        </div>
    </div>
 -->
<!-- Mission Section End -->
<!-- Subscribe Section Start -->
<div class="subscribe-section section pt-140 " data-opacity="6" data-overlay="dark-blue" style="background-image: url('assets/images/bg/newsletter-bg.jpg')">
<div class="container">
<div class="row">
<div class="section-title title-white text-center col-12 mb-60">
<h1>
<a href="tel:+919664149999" style="text-decoration:none">+91 96641 49999</a> / 
                        <a href="tel:+919222966666" style="text-decoration:none">+91 92229 66666</a>
</h1>
<p>Reach Me</p>
</div>
<!-- 
                <div class="col-12 text-center ml-auto mr-auto">
                
                    
                    <form id="mc-form" class="mc-form subscribe-form" >
                        <input id="mc-email" type="email" autocomplete="off" placeholder="Enter your Email Here" />
                        <button id="mc-submit" class="btn btn-red btn-hover-dark-red">subscribe now<i class="zmdi zmdi-mail-send"></i></button>
                    </form>
                     
                    <div class="mailchimp-alerts text-centre">
                        <div class="mailchimp-submitting"></div>
                        <div class="mailchimp-success"></div>
                        <div class="mailchimp-error"></div>
                    </div>
                    
                </div>
                 -->
</div>
</div>
</div><!-- Subscribe Section End -->
<!-- News Section Start -->
<div class="news-section section pt-110 pb-70">
<div class="container">
<div class="row">
<!-- Section Title Start -->
<div class="section-title text-center col-12 mb-80">
<h1>Upcoming Projects</h1>
<p>Together with your support, blessing &amp; encouragement we can excel in all our future challenges.</p>
</div><!-- Section Title End -->
<!-- News Start -->
<div class="news col-lg-4 col-md-6 col-12 mb-50">
<!-- Image -->
<a class="image" href="divyavaibhavgaikwad-samaj-mandir.html"><img alt="Hall" src="assets/images/demo/samaj-mandir/new-hall.jpg"/></a>
<!-- Content -->
<div class="content">
<h3>
<a href="divyavaibhavgaikwad-samaj-mandir.html">
                                Community Centre &amp; Multipurpose Hall, <br/>at Sector-3
                            </a>
</h3>
<!-- Meta -->
<div class="meta">
<a class="meta-item" href="divyavaibhavgaikwad-samaj-mandir.html"><i class="icon-map-pin"></i>Vashi</a>
<span class="meta-item"><i class="fa fa-calendar"></i>12.03.2018</span>
</div>
<!-- 
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                         -->
<a class="btn btn-sm btn-red" href="divyavaibhavgaikwad-samaj-mandir.html">View more</a>
</div>
</div><!-- News End -->
<!-- News Start -->
<div class="news col-lg-4 col-md-6 col-12 mb-50">
<!-- Image -->
<a class="image" href="divyavaibhavgaikwad-daily-fish-vegetable-market"><img alt="Fish Market" src="assets/images/demo/daily-fish-vegetable-matket/new-fish-market.jpg"/></a>
<!-- Content -->
<div class="content">
<h3>
<a href="divyavaibhavgaikwad-daily-fish-vegetable-market.html">
                                Development of Fish &amp; Vegetable Market Area, at Sector 1A
                            </a>
</h3>
<!-- Meta -->
<div class="meta">
<a class="meta-item" href="divyavaibhavgaikwad-daily-fish-vegetable-market.html"><i class="icon-map-pin"></i>Vashi</a>
<span class="meta-item"><i class="fa fa-calendar"></i>12.03.2018</span>
</div>
<!-- 
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                         -->
<a class="btn btn-sm btn-red" href="divyavaibhavgaikwad-daily-fish-vegetable-market.html">View more</a>
</div>
</div><!-- News End -->
<!-- News Start -->
<div class="news col-lg-4 col-md-6 col-12 mb-50">
<a class="image" href="#"><img alt="buildings" src="assets/images/upcoming-projects/sunrise-sunset.jpg"/></a>
<div class="content">
<h3>
<a href="divyavaibhavgaikwad-sunrise-sunset.html">
                                Sunrise / Sunset Point at Veer Sawarkar Garden<br/>Sector-8
                            </a>
</h3>
<div class="meta">
<a class="meta-item" href="divyavaibhavgaikwad-sunrise-sunset.html"><i class="icon-map-pin"></i>Vashi</a>
<span class="meta-item"><i class="fa fa-calendar"></i>01.02.2019</span>
</div>
<a class="btn btn-sm btn-red" href="divyavaibhavgaikwad-sunrise-sunset.html">View more</a>
</div>
</div>
</div>
</div>
</div><!-- News Section End -->
<!-- Footer Top Section Start -->
<div class="footer-top-section section bg-cyan-blue pt-120 pb-80">
<div class="container">
<div class="row">
<!-- Footer Widget Start -->
<div class="footer-widget text-center col-lg-4 col-md-6 col-12 mb-40">
<div class="footer-about">
<!-- Logo -->
<img alt="Divya Vaibhav Gaikwad Logo" src="assets/images/dvg-logo-footer.png"/>
<p>Divya Vaibhav Gaikwad - Corporator, NMMC Ward No. 64 | Chairman Environment Advisory Committee.</p>
<!-- Social -->
<div class="footer-social">
<a href="https://www.facebook.com/divyavaibhavgaikwad" target="_blank"><i class="fa fa-facebook"></i></a>
<a href="https://twitter.com/divyavaibhav64" target="_blank"><i class="fa fa-twitter"></i></a>
<a href="https://plus.google.com/111642369709259270416" target="_blank"><i class="fa fa-google-plus"></i></a>
<a href="https://www.youtube.com/channel/UCOswBWnQjNRcuMbwlxyOvgw" target="_blank"><i class="fa fa-youtube"></i></a>
</div>
</div>
</div>
<!-- Footer Widget End -->
<!-- Footer Widget Start -->
<!-- 
                <div class="footer-widget text-right col-lg-4 col-md-6 col-12 mb-40">
                    
                    <h3 class="title">INSTAGRAM <span>photos</span></h3>
                    
                    <div class="footer-instagram">
                        <ul id="instagram-feed"></ul>
                    </div>
                    
                </div>
                 -->
<!-- Footer Widget End -->
<!-- Footer Widget Start -->
<div class="footer-widget text-left col-lg-4 col-md-6 col-12 mb-40">
<h3 class="title">Quick  <span>Links</span></h3>
<ul class="contact-list ">
<li><span>&gt;</span> <a href="index.html">Home</a></li>
<li><span>&gt;</span> <a href="divyavaibhavgaikwad-biography.html">Biography</a></li>
<li><span>&gt;</span> <a href="divyavaibhavgaikwad-ongoing-upcoming-projects.html">Ongoing Projects</a></li>
<li><span>&gt;</span> <a href="divyavaibhavgaikwad-ongoing-upcoming-projects.html">Upcoming Projects</a></li>
<li><span>&gt;</span> <a href="divyavaibhavgaikwad-past-projects.html">Past Projects</a></li>
<li><span>&gt;</span> <a href="divyavaibhavgaikwad-contact-us.html">Get Enrolled </a></li>
</ul>
</div>
<!-- Footer Widget End -->
<!-- Footer Widget Start -->
<div class="footer-widget text-left col-lg-4 col-md-6 col-12 mb-40">
<h3 class="title">Contact <span>Me</span></h3>
<ul class="contact-list">
<li><span>Phone:</span> <a href="tel:+919664149999">+91 96641 49999</a></li>
<li><span>Phone:</span> <a href="tel:+919222966666">+91 92229 66666</a></li>
<!-- <li><span>Email:</span> <a href="mailto:poltipro@email.com">poltipro@email.com</a></li> -->
<li><span>PRO:</span>  C2/06/0:1, Angarika Appts.,
Sector 4, Vashi, Navi Mumbai 400703, Maharashtra, India.</li>
</ul>
</div>
<!-- Footer Widget End -->
</div>
</div>
</div><!-- Footer Top Section End -->
<!-- Footer Bottom Section Start -->
<div class="footer-bottom-section section">
<div class="container">
<div class="row">
<!-- Footer Copyright -->
<div class="footer-copyright text-center col-12">
<p>CopyrightÂ© 2019, All Right Reserved. Designed by <a href="http://www.creativesaints.com/" target="_blank">Creative Saints</a></p>
</div>
</div>
</div>
</div><!-- Footer Bottom Section End -->
</div><!-- Main Wrapper End -->
<!-- JS
============================================ -->
<!-- jQuery JS -->
<script src="assets/js/vendor/jquery-1.12.0.min.js"></script>
<!-- Popper JS -->
<script src="assets/js/popper.min.js"></script>
<!-- Bootstrap JS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- Plugins JS -->
<script src="assets/js/plugins.js"></script>
<!-- Ajax Mail -->
<script src="assets/js/ajax-mail.js"></script>
<!-- Main JS -->
<script src="assets/js/main.js"></script>
<!-- VMap JS -->
<script src="assets/js/jquery.vmap.min.js"></script>
<script src="assets/js/maps/jquery.vmap.world.js"></script>
<!-- VMap Active JS -->
<script>

    function escapeXml(string) {
        return string.replace(/[<>]/g, function (c) {
            switch (c) {
                case '<': return '\u003c';
                case '>': return '\u003e';
            }
        });
    }

    jQuery(document).ready(function () {

        var pins = {
            cn: escapeXml('<div class="vmap-pin-text"><p>Floor. 4 House. 15, Block C, Banasree Main Rd, Dhaka</p></div><span class="vmap-pin"></span>'),
            ca: escapeXml('<div class="vmap-pin-text"><p>Floor. 4 House. 15, Block C, Banasree Main Rd, Dhaka</p></div><span class="vmap-pin"></span>'),
            au: escapeXml('<div class="vmap-pin-text"><p>Floor. 4 House. 15, Block C, Banasree Main Rd, Dhaka</p></div><span class="vmap-pin"></span>'),
            br: escapeXml('<div class="vmap-pin-text"><p>Floor. 4 House. 15, Block C, Banasree Main Rd, Dhaka</p></div><span class="vmap-pin"></span>'),
        };

        jQuery('#world-vmap').vectorMap({
            backgroundColor: 'transparent',
            borderColor: '#6f89a2',
            map: 'world_en',
            pins: pins,
            color: '#6f89a2',
            enableZoom: false,
            pinMode: 'content',
            selectedColor: '#6f89a2',
            hoverColor: '#ef3345',
            showTooltip: true,
            selectedRegions: ['CN', 'CA', 'AU', 'BR'],
            onRegionClick: function(element, code, region)
            {
                var message = '<h4>'
                    + region
                    + ' Office</h4> <p></p> '

                    var $this = $(this).find('div[for='+ code +']').find('.vmap-pin-text');
                    if ($this.hasClass('open')) {
                        $this.removeClass('open').find('h4').remove();
                    } else {
                        $this.addClass('open').prepend(message);
                    }
                
            }
        });

    });
</script>
</body>
</html>