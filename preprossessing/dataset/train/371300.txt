<!DOCTYPE html>
<html>
<head>
<title>Application Firewall Error</title>
<style media="screen" type="text/css">
    body { font-family: Arial, Garamond, sans-serif; padding: 40px; background-color: #333333; }
    #container {width: 65%; padding: 20px 40px; border: 1px solid #353535; margin: 0 auto; background-color: #ffffff;}
    table { margin-top: 45px; }
    table tr td { padding: 7px; }
  </style>
<script charset="utf-8" type="text/javascript">
    var d = new Date();
    
    
    var bodyVars = "";
    bodyVars += "Session ID: N/A%0D%0D";
    bodyVars += "Event ID: 6801510467719184606%0D%0D";
    bodyVars += "Host: www.credit-suisse.com%0D%0D";
    bodyVars += "Time: "+d+"%0D%0D";
    
    var mailLink = "<a href='mailto:web.services2@credit-suisse.com?subject=WAF Block Page: 6801510467719184606&body="+bodyVars+"'>web.services2@credit-suisse.com </a> for assistance.";
</script>
</head>
<body>
<div id="container">
<p><strong>Your request has been blocked.</strong><br/><br/>
    Please try again, consider using an alternative device or switching to another, more secured network.  <br/><br/>Thank you.
  </p>
<table>
<tr>
<td>Event ID:</td>
<td>6801510467719184606</td>
</tr>
<tr>
<td>Host:</td>
<td>www.credit-suisse.com</td>
</tr>
<tr>
<td>Timestamp:</td>
<td>
<script type="text/JavaScript">
          document.write(d);
        </script>
</td>
</tr>
<tr>
</tr>
</table>
</div>
<p> </p>
<p> </p>
<script charset="utf-8" type="text/javascript">
  if (document.referrer) {
    document.write('<p style="text-align:center;"><input type="button" value="Return to Previous Page" onclick="javascript: history.go(-1);return false;" /></p>');
  }
</script>
</body>
</html>