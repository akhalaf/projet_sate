<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Page not found - Famous Waffle</title>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<link href="https://famouswaffle.net/xmlrpc.php" rel="pingback"/>
<!-- This site is optimized with the Yoast SEO plugin v11.9 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex,follow" name="robots"/>
<meta content="en_US" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Page not found - Famous Waffle" property="og:title"/>
<meta content="Famous Waffle" property="og:site_name"/>
<meta content="summary" name="twitter:card"/>
<meta content="Page not found - Famous Waffle" name="twitter:title"/>
<script class="yoast-schema-graph yoast-schema-graph--main" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://famouswaffle.net/#website","url":"https://famouswaffle.net/","name":"Famous Waffle","potentialAction":{"@type":"SearchAction","target":"https://famouswaffle.net/?s={search_term_string}","query-input":"required name=search_term_string"}}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//s0.wp.com" rel="dns-prefetch"/>
<link href="//secure.gravatar.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://famouswaffle.net/feed/" rel="alternate" title="Famous Waffle » Feed" type="application/rss+xml"/>
<link href="https://famouswaffle.net/comments/feed/" rel="alternate" title="Famous Waffle » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/famouswaffle.net\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://famouswaffle.net/wp-includes/css/dist/block-library/style.min.css?ver=5.2.9" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://famouswaffle.net/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.4" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://famouswaffle.net/wp-content/themes/circumference-lite%202/css/circumferencelite-bootstrap.min.css?ver=3.0.0" id="circumferencelite-bootstrap-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://famouswaffle.net/wp-content/themes/circumference-lite%202/style.css?ver=5.2.9" id="circumferencelite-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://famouswaffle.net/wp-content/plugins/jetpack/css/jetpack.css?ver=7.6.1" id="jetpack_css-css" media="all" rel="stylesheet" type="text/css"/>
<script>if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script><script src="https://famouswaffle.net/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://famouswaffle.net/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://famouswaffle.net/wp-json/" rel="https://api.w.org/"/>
<link href="https://famouswaffle.net/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://famouswaffle.net/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.2.9" name="generator"/>
<meta content="DpYBnscQ2gL24N-jq8FTS9r8UVXuj7Q5Cqp7QJuVaEU" name="google-site-verification"/><script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		if (window.wfLogHumanRan) { return; }
		window.wfLogHumanRan = true;
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//famouswaffle.net/?wordfence_lh=1&hid=4C8EA62C45CC0ADF3B21249E4A613960');
</script>
<link href="//v0.wordpress.com" rel="dns-prefetch"/>
<link href="//i0.wp.com" rel="dns-prefetch"/>
<link href="//i1.wp.com" rel="dns-prefetch"/>
<link href="//i2.wp.com" rel="dns-prefetch"/>
<style type="text/css">img#wpstats{display:none}</style> <style type="text/css">
			h1,h2,h3,h4,h5,h6,h1 a, h2 a {color: #40494e;}
			#cir-bottom-wrapper h3 {color: #ffffff;}
			#cir-footer-wrapper h4 {color: #818181;}
			#cir-banner {color: #ffffff;}
			#cir-content-wrapper {color: #ffffff;}
			#cir-cta h1 {color: #fff2f2;}
			a {color: #2bafbb;}
			a:hover,a:focus,.menu.widget ul li:hover::before,.menu.widget ul li a:hover,h1 a:hover, h2 a:hover {color: #c6b274;}
			.menu.widget a {color:#656565;}
			#cir-bottom-wrapper .menu.widget a, #cir-bottom-wrapper .menu.widget a {color:#abb3b4;}
			
			#cir-bottom-wrapper a {color:#efefef;}
			#cir-bottom-wrapper a:hover {color:#abb3b4;}
			
			#cir-content-wrapper,#cir-bottom-wrapper {font-size: 0.813em;}
			#cir-footer-wrapper a {color: #c6b274;}			
			#cir-footer-wrapper a:hover {color: #818181;}
			.nav-menu li a, .nav-menu li.home a {color:#565656;}
			.nav-menu li a:hover {color:#c6b274;}
			ul.nav-menu ul a,.nav-menu ul ul a {color: #919191;}
			ul.nav-menu ul a:hover,	.nav-menu ul ul a:hover,.nav-menu .current_page_item > a,.nav-menu .current_page_ancestor > a,.nav-menu .current-menu-item > a,.nav-menu .current-menu-ancestor > a {color:#c6b274;}
			ul.nav-menu li:hover > ul,.nav-menu ul li:hover > ul {background-color: #ffffff;border-color:#c6b274;}
			ul.sub-menu .current_page_item > a,ul.sub-menu .current_page_ancestor > a, ul.sub-menu .current-menu-item > a, ul.sub-menu .current-menu-ancestor > a {background-color: #f3f3f3;}
			ul.nav-menu li:hover > ul li:hover {background-color: #f3f3f3;}	
			
			#social-icons a {color: #ffffff;}
			#social-icons a:hover {color: #000000;}
			#socialbar .icomoon {background-color: #dd3333;}
			#cir-breadcrumbs-wrapper a {color:#d79832;}
			#cir-breadcrumbs-wrapper a:hover {color:#2bafbb;}
			#cir-content-area .list-lines.widget ul li {border-color:#e2e5e7;}
			#cir-bottom-wrapper ul li {border-color:#5c646b;}
		</style>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<!--[if lt IE 9]>
		<script src="https://famouswaffle.net/wp-content/themes/circumference-lite%202/js/circumferencelite-respond.min.js" type="text/javascript"></script>
		<script src="https://famouswaffle.net/wp-content/themes/circumference-lite%202/js/circumferencelite-html5.min.js" type="text/javascript"></script>
	<![endif]-->
<link href="https://www.famouswaffle.net/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="https://www.famouswaffle.net/favicon.ico" rel="icon" type="image/vnd.microsoft.icon"/>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1680691722143512');
fbq('track', "PageView");</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=1680691722143512&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body class="error404" data-rsssl="1" style="font-size: 100%;">
<div id="cir-wrapper" style="border-color:#000000;">
<div id="cir-ann-social-wrapper" style="background-color: #e29b1f; border-color: #f3f3f3;">
<div class="container">
<div class="row">
<div class="col-md-12">
<div id="cir-social-wrapper">
<div id="socialbar">
<div id="social-icons"><a href="http://www.facebook.com/famouswaffle" target="_blank" title="Facebook"><div class="icomoon icon-facebook3" id="facebook"></div></a><a href="https://www.youtube.com/user/famouswafflemy" target="_blank" title="Youtube"><div class="icomoon icon-youtube" id="youtube"></div></a><a href="http://www.instagram.com/famouswaffle" target="_blank" title="Instagram"><div class="icomoon icon-instagram" id="instagram"></div></a></div>
</div>
</div>
</div>
</div>
</div>
</div>
<header id="cir-site-header" role="banner" style="background-color: #ffffff;">
<div class="container">
<div class="row">
<div class="col-md-5">
<div id="cir-logo-group-wrapper">
<div id="cir-logo-group">
<a href="https://famouswaffle.net/" rel="home" title="Famous Waffle">
<img alt="Famous Waffle " class="img-responsive" src="https://famouswaffle.net/wp-content/uploads/2015/02/12344.png"/>
</a>
</div>
<div id="cir-site-title-group" style="margin: 0 0 0 0;">
<h1 id="cir-site-title"><a href="https://famouswaffle.net/" rel="home" style="color: #a37025;" title="Famous Waffle">Famous Waffle</a></h1>
<h2 id="cir-site-tagline" style="color: #260a20;">waffle.crepe.ice cream.crefel</h2>
</div>
</div>
</div>
<div class="col-md-7">
<div class="navbar" id="navbar" style="margin: 14px 0 0 0;">
<nav class="navigation main-navigation" id="site-navigation" role="navigation">
<div class="menu-toggle-wrapper hidden-md hidden-lg">
<h3 class="menu-toggle">Menu</h3>
</div>
<a class="screen-reader-text skip-link" href="#content" title="Skip to content">
					Skip to content</a>
<ul class="nav-menu" id="menu-famous"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-35" id="menu-item-35"><a href="https://www.famouswaffle.net">HOME</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25" id="menu-item-25"><a href="https://famouswaffle.net/products/">PRODUK</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-24" id="menu-item-24"><a href="https://famouswaffle.net/jom-niaga/">JOM BISNES!</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32" id="menu-item-32"><a href="https://famouswaffle.net/bisnes-waffle/">PAKEJ ASAS WAFFLE</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-987" id="menu-item-987"><a href="https://famouswaffle.net/bisnes-waffle/pakej-advance-waffle/">PAKEJ ADVANCE WAFFLE</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31" id="menu-item-31"><a href="https://famouswaffle.net/bisnes-crefel/">BISNES CREFEL</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23" id="menu-item-23"><a href="https://famouswaffle.net/testimoni/">TESTIMONI</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26" id="menu-item-26"><a href="https://famouswaffle.net/hubungi/">HUBUNGI</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-430" id="menu-item-430"><a href="https://famouswaffle.net/category/cerita-kami/">CERITA KAMI</a></li>
</ul> </nav><!-- #site-navigation -->
</div><!-- #navbar -->
</div>
</div>
</div>
</header>
<aside id="cir-banner" style="background-color: #c6b274; background-image: url(https://famouswaffle.net/wp-content/themes/circumference-liteimages/banner-bg.jpg);">
<div class="container">
<div class="row">
<div class="col-md-12">
</div>
</div>
</div>
<img alt="Famous Waffle" src="https://famouswaffle.net/wp-content/themes/circumference-lite%202/images/demo-banner.jpg"/>
</aside>
<div id="cir-breadcrumbs-wrapper" style="background-color: #e2e5e7; color:#9ca4a9;">
<div class="container">
<div class="row">
<div class="col-md-12">
</div>
</div>
</div>
</div>
<aside id="cir-cta" role="complementary" style="background-color: #e05959; color: #ffffff;">
<div class="container">
<div class="row">
<div class="col-md-12">
<h1>JOM BISNES BERSAMA KAMI :)</h1> <div class="textwidget"><p>Kami menawarkan peluang dan pakej perniagaan menarik bagi usahawan yang ingin memulakan perniagaan waffle, crepe, ice cream dan crefel. Kami percaya adanya keberkatan jika kita ‘bisnes secara berjemaah’.</p>
<form action="https://famouswaffle.net/bisnes-waffle/"><input type="submit" value="  Pakej Asas Waffle  "/></form>
<form action="https://famouswaffle.net/bisnes-waffle/pakej-advance-waffle/"><input type="submit" value="Pakej Advance Waffle"/></form>
<form action="https://famouswaffle.net/bisnes-crefel/"><input type="submit" value="  Bisnes Crefel  "/></form>
<form action="https://famouswaffle.net/category/cerita-kami/"><input type="submit" value="  Cerita Kami  "/></form>
</div>
</div>
</div>
</div>
</aside>
<div id="cir-content-wrapper" style="background-color:#ffffff; color:#656565;">
<section id="cir-content-area" role="main">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="entry-content error-content">
<header class="page-header">
<h1 class="heading1">Page Not Found</h1>
<h2 class="heading2">Well this does not look good.<br/>It appears this page is missing or was removed.</h2>
</header>
<p>If what you were looking for is not found, you may want to try searching with keywords relevant to what you were looking for.</p>
<div class="input-group-box">
<form action="https://famouswaffle.net/" class="search-form" method="get" role="search">
<span class="screen-reader-text">Search for:</span>
<div class="input-group">
<input class="form-control" name="s" type="text" value=""/>
<span class="input-group-btn">
<button class="btn btn-default" type="submit" value="Search">Go!</button>
</span>
</div><!-- /input-group -->
</form> </div>
</div><!-- .page-content -->
</div>
</div><!-- #main -->
</div><!-- #primary -->
</section>
</div><!-- #cir-content-wrapper-->
<aside id="cir-bottom-wrapper" role="complementary" style="background-color: #384149; color:#abb3b4;">
<div class="container">
<div class="row" id="cir-bottom-group">
<div class="col-md-12" id="bottom1" role="complementary">
<div class="widget widget_text" id="text-11"><h3>Tentang Kami</h3> <div class="textwidget"><p><strong>SM FAMOUS WAFFLE PLT (LLP0011941-LGN)</strong></p>
<p>“Kami merupakan syarikat 100% muslim. Bahan-bahan yang kami gunakan kami pilih secara berhati-hati dari sumber yang halal dan cara pembuatan produk kami berlandaskan syarat-syarat yang ditetapkan oleh syarak. Semoga usaha kita dalam mengeluarkan produk yang halal dan toyibban mendapat keredhaan dan keberkatan dari Allah.”</p>
</div>
</div> </div><!-- #top1 -->
<div class="col-md-12" id="bottom2" role="complementary">
</div><!-- #top2 -->
<div class="col-md-12" id="bottom3" role="complementary">
</div><!-- #top3 -->
<div class="col-md-12" id="bottom4" role="complementary">
</div><!-- #top4 -->
</div>
</div>
</aside>
<footer id="cir-footer-wrapper" style="color: #818181;">
<div id="cir-footer-menu">
</div>
<div class="copyright">
    Copyright © 2021 Famous Waffle. All rights reserved.    </div>
</footer>
</div><!-- #cir-wrapper -->
<div style="display:none">
</div>
<script src="https://famouswaffle.net/wp-content/plugins/jetpack/_inc/build/photon/photon.min.js?ver=20190201" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/famouswaffle.net\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"cached":"1"};
/* ]]> */
</script>
<script src="https://famouswaffle.net/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.4" type="text/javascript"></script>
<script src="https://s0.wp.com/wp-content/js/devicepx-jetpack.js?ver=202102" type="text/javascript"></script>
<script src="https://secure.gravatar.com/js/gprofiles.js?ver=2021Janaa" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var WPGroHo = {"my_hash":""};
/* ]]> */
</script>
<script src="https://famouswaffle.net/wp-content/plugins/jetpack/modules/wpgroho.js?ver=5.2.9" type="text/javascript"></script>
<script src="https://famouswaffle.net/wp-content/themes/circumference-lite%202/js/circumferencelite-bootstrap.min.js?ver=3.0.0" type="text/javascript"></script>
<script src="https://famouswaffle.net/wp-content/themes/circumference-lite%202/js/circumferencelite-extras.js?ver=1.0" type="text/javascript"></script>
<script src="https://famouswaffle.net/wp-includes/js/wp-embed.min.js?ver=5.2.9" type="text/javascript"></script>
<script async="async" defer="defer" src="https://stats.wp.com/e-202102.js" type="text/javascript"></script>
<script type="text/javascript">
	_stq = window._stq || [];
	_stq.push([ 'view', {v:'ext',j:'1:7.6.1',blog:'91298676',post:'0',tz:'0',srv:'famouswaffle.net'} ]);
	_stq.push([ 'clickTrackerInit', '91298676', '0' ]);
</script>
</body>
</html>
<!-- Page generated by LiteSpeed Cache 3.6.1 on 2021-01-12 15:45:48 -->