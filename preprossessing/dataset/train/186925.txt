<!DOCTYPE html>
<html class="no-js">
<head>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta charset="utf-8"/>
<meta content="initial-scale=1.0,maximum-scale=1.0" name="viewport"/>
<!-- Stylesheet -->
<link href="/css?include=templates/balsta/" rel="stylesheet"/>
<link href="/css?include=jquery-ui-1.8.9.custom.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Lora:400,400italic,700,700italic" rel="stylesheet" type="text/css"/>
<!-- Scripts -->
<script src="/js/jquery-1.11.1.min.js"></script>
<script src="/js/jquery-ui-1.11.4.min.js"></script>
<script src="/js/modernizr.js"></script>
<script src="/js/templates/balsta/scripts.js"></script>
<!-- Fav- and App-icons -->
<link href="/images/favicon/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="/images/favicon/apple-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="/images/favicon/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/images/favicon/apple-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/images/favicon/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="/images/favicon/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/images/favicon/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="/images/favicon/apple-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/images/favicon/apple-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/images/favicon/android-icon-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="/images/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/images/favicon/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="/images/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<meta content="#ffffff" name="msapplication-TileColor"/>
<meta content="/images/favicon/ms-icon-144x144.png" name="msapplication-TileImage"/>
<meta content="#ffffff" name="theme-color"/>
<script>
		// Google analytics
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-36028440-1', 'auto');
		ga('send', 'pageview');
	</script>
<link href="/css?include=layouts/guestStartpage" rel="stylesheet"/>
<title>Välkommen</title>
<meta content="" name="keywords"/>
<meta content="" name="description"/>
<link href="/css?include=views/html/customLayout/show" id="viewCss" rel="stylesheet"/></head>
<body ontouchstart="">
<div id="cookieNotification">
<div id="infoContainer">
<div class="message">
<p><strong>Information om kakor</strong></p>
<p>För att du ska kunna använda vår sida på ett bra sätt så använder vi kakor (cookies). </p>
</div>
<form action="/" method="post">
<div class="hidden">
<input class="hidden" id="frmCookieAcceptAdd" name="frmCookieAcceptAdd" type="hidden" value="1"/>
</div>
<p class="buttons">
<button type="submit">Jag förstår</button>
</p>
</form>
</div>
</div>
<div id="wrapper">
<header>
<div class="container">
<!-- Search toggler -->
<input id="searchBox" type="checkbox"/>
<div id="mainSearchForm">
<label for="searchBox"><span class="icon"></span><span class="text">Sök</span></label>
<div id="searchBoxContainer">
<div class="view infoContent guestSearchForm">
<form action="/objektlista" class="marginal" method="get">
<div class="searchQuery field">
<label class="accessibility" for="searchQuery">Nyckelord</label>
<input class="text" id="searchQuery" maxlength="255" minlength="3" name="searchQuery" placeholder="Nyckelord" title="Nyckelord" type="text" value=""/>
</div>
<div class="searchType field">
<label for="searchType"></label>
<select id="searchType" name="searchType" title="">
<option selected="selected" value="active">Pågående</option>
<option value="archive">Arkiv</option>
</select>
</div>
<div class="hidden">
<input class="hidden" id="frmSearch" name="frmSearch" type="hidden" value="true"/>
</div>
<p class="buttons">
<button type="submit">Sök</button>
</p>
</form>
</div>
<script type="text/javascript">
		function makeSearch() {
			var searchQuery = $("#searchQuery").val();

			if( searchQuery.length > 2 ) {
				$(".view.guestSearchResults").load( "/objektlista #productList", $(".guestSearchForm form").serialize() );
			} else {
				$(".view.guestSearchResults").html( "" );
			}
		}

		$("#searchQuery").keyup( function() {
			makeSearch();
		} );
		$("#searchType").change( function() {
			makeSearch();
		} );

		$("#mainSearchForm label").click( function() {
			if( !$("#searchBoxContainer").is(":visible") ) {
				$("#searchQuery").focus();
			}
		} );

		$("input#searchBox").change( function() {
			if( $(this).prop("checked") ) {
				makeSearch();
			} else {
				$(".view.guestSearchResults").html( "" );
			}
		} );
  </script>
<div class="view product categoryList">
<ul class="treeList">
<li class="specialAuction"><a href="/objektlista">Alla objekt</a></li>
<li class="first specialAuction lastSpecialAuction"><a class="ajax" href="/objektlista/polisgods">Polismyndighetens Beslagsgods</a></li>
<li class="specialAuction lastSpecialAuction"><a class="ajax" href="/objektlista/månadens-utvalda---januari">Månadens Utvalda - Januari</a></li>
<li class="specialAuction lastSpecialAuction"><a class="ajax" href="/objektlista/vitvaror---24-januari">Vitvaror - 24 januari</a></li>
<li><a class="ajax" href="/objektlista/allmoge">Allmoge Kuriosa</a></li>
<li><a class="ajax" href="/objektlista/belysning/lampor">Belysning Lampor</a></li>
<li><a class="ajax" href="/objektlista/böcker-frimärken-vykort">Böcker</a></li>
<li><a class="ajax" href="/objektlista/cyklar-motorfordon-verktyg">Cyklar Motorfordon</a></li>
<li><a class="ajax" href="/objektlista/glas-porslin-keramik">Glas Keramik Porslin</a></li>
<li><a class="ajax" href="/objektlista/guld-silver--smycken">Guld Silver Ädelstenar</a></li>
<li><a class="ajax" href="/objektlista/trädgård">Hem, Inredning, Trädgård</a></li>
<li><a class="ajax" href="/objektlista/konst">Konst Fotografi Skulptur</a>
</li><li><a class="ajax" href="/objektlista/smycken-klockor--accessoarer">Klockor Smycken</a></li>
<li><a class="ajax" href="/objektlista/textil-kläder">Kläder Skor Accessoarer</a></li>
<li><a class="ajax" href="/objektlista/leksaker">Leksaker Sport Hobby</a></li>
<li><a class="ajax" href="/objektlista/mattor">Mattor Textil</a></li>
<li><a class="ajax" href="/objektlista/silver--metallföremål">Metall</a></li>
<li><a class="ajax" href="/objektlista/musikinstrument">Musik LP</a></li>
<li><a class="ajax" href="/objektlista/mynt/frimärken/vykort">Mynt Frimärken Vykort</a></li>
<li><a class="ajax" href="/objektlista/möbler-belysning-speglar">Möbler</a></li>
<li><a class="ajax" href="/objektlista/orientaliska-föremål-">Orientaliskt</a></li>
<li><a class="ajax" href="/objektlista/teknik--hemelektronik">Teknik Elektronik Foto</a></li>
<li><a class="ajax" href="/objektlista/verktyg-maskiner--handverktyg">Verktyg Maskiner</a></li>
<li><a class="ajax" href="/objektlista/nautica">Vapen Jakt Nautica Fiske</a></li>
<li class="last"><a class="ajax" href="/objektlista/övrigt">Övrigt</a></li>
</ul>
</div> <div class="view guestSearchResults products list">
<h1>Sökhjälp</h1>
<p>Här kan du söka bland aktuella objekt, efter kategori eller filtrera ut varor som klubbas på en specialauktion.</p>
<p>Antingen kan du söka i frisöksrutan på ord eller objektsnummer, eller välja någon av auktionerna eller kategorierna i listan till vänster.  </p> </div>
</div>
</div>
<a class="logo" href="/">
<img src="/images/templates/balsta/logo.png"/> </a>
<!-- Menu toggler -->
<input id="mainMenu" type="checkbox"/>
<nav id="mainNavigation">
<label for="mainMenu"><span class="text">Meny</span><span class="icon"></span></label>
<div class="navContainer">
<div class="leftSide">
</div>
<div class="rightSide">
<div class="view navigation list">
<ul class="navMain"><li class="subTree odd first"><a class="ajax" href="/om-oss" target="_self">OM OSS</a><ul><li class="subFirst odd"><a class="ajax" href="/kontakt" target="_self">Kontakt</a></li><li><a class="ajax" href="/hitta-hit" target="_self">Hitta hit</a></li><li class="odd"><a class="ajax" href="/medarbetare" target="_self">Medarbetare</a></li><li class="subLast"><a class="ajax" href="/lediga-tjänster" target="_self">Lediga tjänster</a></li></ul></li><li class="subTree"><a class="ajax" href="/köpa" target="_self">Köpa</a><ul><li class="subFirst odd"><a class="ajax" href="/information-in-english" target="_self">Information in English</a></li><li><a class="ajax" href="/support" target="_self">Vanliga frågor</a></li><li class="odd"><a class="ajax" href="/frakt--transport" target="_self">Frakt &amp; Transport</a></li><li class="subLast"><a class="ajax" href="/covid-19" target="_self">Med anledning av Covid-19</a></li></ul></li><li class="subTree odd last"><a class="ajax" href="/sälja" target="_self">Sälja</a><ul><li class="subFirst odd"><a class="ajax" href="/att-sälja-på-auktion" target="_self">Att sälja på auktion</a></li><li><a class="ajax" href="/för-konkursförvaltare-företag--myndighet" target="_self">För Konkursförvaltare, Företag &amp; Myndighet</a></li><li class="odd subLast"><a class="ajax" href="/hela-hem--dödsbon" target="_self">Hela hem &amp; dödsbon</a></li></ul></li>
</ul>
</div> </div>
</div>
</nav>
</div>
</header>
<div class="dontPrint" id="extraNavigation">
<nav class="view navigation listCrumbs" role="breadcrumb">
<ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<a href="/" itemprop="item">
<span itemprop="name">Hem</span>
</a>
<meta content="1" itemprop="position"/>
</li>
</ol>
</nav>
<div class="myPage"><a href="/logga-in">Logga in</a></div> </div>
<div class="navigation subNav" id="subMenu">
<ul class="navMain">
<li class="selected">
</li>
</ul>
</div>
<div class="layout customLayout">
<main id="main" role="main">
<div class="view customLayout show">
<div class="view customLayout sectionShow slideshowImage slideshow rows1">
<div class="container">
<ul class="cycle-slideshow" data-cycle-cycle-auto-height="4:3" data-cycle-log="false" data-cycle-pager=".cycle-pager" data-cycle-pager-active-class="cycle-pager-active" data-cycle-pager-template="&lt;span&gt;•&lt;/span&gt;" data-cycle-slides="li" data-cycle-swipe="true">
<li data-cycle-fx="fade" data-cycle-speed="500" data-cycle-timeout="4000"><div class="slideContent" style="background-image: url(/images/custom/SlideshowImage/313820.jpg);"><div class="description"><h1>Månadens Utvalda</h1></div></div></li><li data-cycle-fx="fade" data-cycle-speed="500" data-cycle-timeout="6000"><div class="slideContent" style="background-image: url(/images/custom/SlideshowImage/300434.jpg);"></div></li><li data-cycle-fx="fade" data-cycle-speed="500" data-cycle-timeout="6000"><div class="slideContent" style="background-image: url(/images/custom/SlideshowImage/236986.jpg);"><div class="description"><h1>Enkel värdering online</h1></div></div></li><li data-cycle-fx="fade" data-cycle-speed="500" data-cycle-timeout="3000"><div class="slideContent" style="background-image: url(/images/custom/SlideshowImage/234056.jpg);"><a class="containerLink" href="https://balstaauktionshall.nu/objektlista/m%C3%A5nadens-utvalda---mars"></a></div></li>
</ul>
</div>
<div class="cycle-pager"></div>
</div>
<script src="/js/jquery.cycle2.min.js"></script>
<script src="/js/jquery.cycle2.swipe.min.js"></script>
<script>
			$(document).ready(function() {
				// Preload all images in slideshow in this array
				var slideshowImages = [];
				slideshowImages.push( "/images/custom/SlideshowImage/313820.jpg" ); slideshowImages.push( "/images/custom/SlideshowImage/300434.jpg" ); slideshowImages.push( "/images/custom/SlideshowImage/236986.jpg" ); slideshowImages.push( "/images/custom/SlideshowImage/234056.jpg" );
				var imageSlideshowObjects = [];
				$.each( slideshowImages , function(index, value) {
					imageSlideshowObjects[index] = new Image();
					imageSlideshowObjects[index].src = value;
				} );
			});
		</script>
<div class="view customLayout sectionShow productCategorySection genericList col2">
<article class="productCategorySection" style="background-image: url('/images/custom/ProductCategory/291279.jpg');">
<a href="/objektlista/avdelning-1">
<img class="transparent transparent_1_1" src="/images/templates/balsta/transparent.png"/>
<img class="transparent transparent_2_1" src="/images/templates/balsta/transparent_2_1.png"/>
<img class="transparent transparent_3_2" src="/images/templates/balsta/transparent_3_2.png"/>
<header>
<h4>Interiör Livsstil</h4>
</header>
<div class="summary"><div class="summaryContent"><p>Design, antikviteter, glas, keramik, konst, vintage, guld och smycken - Allt som förgyller hemmet och livet i stort hittar du under 'Interiör &amp; Livsstil'.</p></div></div>
</a>
</article>
<article class="productCategorySection" style="background-image: url('/images/custom/ProductCategory/293533.jpg');">
<a href="/objektlista/avdelning-2">
<img class="transparent transparent_1_1" src="/images/templates/balsta/transparent.png"/>
<img class="transparent transparent_2_1" src="/images/templates/balsta/transparent_2_1.png"/>
<img class="transparent transparent_3_2" src="/images/templates/balsta/transparent_3_2.png"/>
<header>
<h4>Garage Maskiner</h4>
</header>
<div class="summary"><div class="summaryContent"><p>På jakt efter verktyg, maskiner, cyklar, fordon, byggmaterial eller teknik? Titta in i 'Garage &amp; Maskin'!</p></div></div>
</a>
</article>
</div>
<div class="view customLayout sectionShow product products list">
<h2>Objekt från <strong>Alla auktioner</strong> sorterad efter Kortast tid kvar</h2>
<ul class="productList normal rows1" data-last-updated="1610549663" id="productList">
<li data-last-updated="1610549663" data-product-id="170155" id="product-170155" itemscope="" itemtype="http://schema.org/Product">
<div class="image">
<a class="ajax" href="/objekt/lavoar-med-marmorskiva-1920-tal/118013"><img alt="" itemprop="image" src="/images/custom/ProductTemplate/tn/311704.jpg"/></a>
</div>
<div class="information">
<div class="productFavorite"></div>
<div class="productId" itemprop="productID">170155</div>
<h2>
<a class="ajax" href="/objekt/lavoar-med-marmorskiva-1920-tal/118013" itemprop="name">
							LAVOAR, med marmorskiva, 1920-tal
						</a>
</h2>
<div class="productBidCurrent" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
						Bud:
						<span class="currentBid" data-customer-id="" itemprop="price">
							100 kr
						</span>
</div>
<div class="productEndTime" data-product-id="170155">
<span class="productClock" data-time="1610560800">Slutar idag 19:00</span>
</div>
</div>
</li>
<li data-last-updated="1610549663" data-product-id="170156" id="product-170156" itemscope="" itemtype="http://schema.org/Product">
<div class="image">
<a class="ajax" href="/objekt/vitrinskåp-teak-1960-tal/118014"><img alt="" itemprop="image" src="/images/custom/ProductTemplate/tn/311711.jpg"/></a>
</div>
<div class="information">
<div class="productFavorite"></div>
<div class="productId" itemprop="productID">170156</div>
<h2>
<a class="ajax" href="/objekt/vitrinskåp-teak-1960-tal/118014" itemprop="name">
							VITRINSKÅP, teak, 1960-tal
						</a>
</h2>
<div class="productBidCurrent" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
						Bud:
						<span class="currentBid" data-customer-id="" itemprop="price">
							125 kr
						</span>
</div>
<div class="productEndTime" data-product-id="170156">
<span class="productClock" data-time="1610560920">Slutar idag 19:02</span>
</div>
</div>
</li>
<li data-last-updated="1610549663" data-product-id="170157" id="product-170157" itemscope="" itemtype="http://schema.org/Product">
<div class="image">
<a class="ajax" href="/objekt/slagbord-allmoge-1800-tal/118015"><img alt="" itemprop="image" src="/images/custom/ProductTemplate/tn/311716.jpg"/></a>
</div>
<div class="information">
<div class="productFavorite"></div>
<div class="productId" itemprop="productID">170157</div>
<h2>
<a class="ajax" href="/objekt/slagbord-allmoge-1800-tal/118015" itemprop="name">
							SLAGBORD, allmoge, 1800-tal
						</a>
</h2>
<div class="productBidCurrent" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
						Bud:
						<span class="currentBid" data-customer-id="" itemprop="price">
							550 kr
						</span>
</div>
<div class="productEndTime" data-product-id="170157">
<span class="productClock" data-time="1610561040">Slutar idag 19:04</span>
</div>
</div>
</li>
<li data-last-updated="1610549663" data-product-id="170158" id="product-170158" itemscope="" itemtype="http://schema.org/Product">
<div class="image">
<a class="ajax" href="/objekt/golvlampa-mae-87-2-mässing-1960-tal/118016"><img alt="" itemprop="image" src="/images/custom/ProductTemplate/tn/311722.jpg"/></a>
</div>
<div class="information">
<div class="productFavorite"></div>
<div class="productId" itemprop="productID">170158</div>
<h2>
<a class="ajax" href="/objekt/golvlampa-mae-87-2-mässing-1960-tal/118016" itemprop="name">
							GOLVLAMPA, MAE 87-2, mässing, 1960-tal
						</a>
</h2>
<div class="productBidCurrent" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
						Bud:
						<span class="currentBid" data-customer-id="" itemprop="price">
							100 kr
						</span>
</div>
<div class="productEndTime" data-product-id="170158">
<span class="productClock" data-time="1610561160">Slutar idag 19:06</span>
</div>
</div>
</li>
<li data-last-updated="1610549663" data-product-id="170159" id="product-170159" itemscope="" itemtype="http://schema.org/Product">
<div class="image">
<a class="ajax" href="/objekt/spegel-1950-tal/118017"><img alt="" itemprop="image" src="/images/custom/ProductTemplate/tn/311728.jpg"/></a>
</div>
<div class="information">
<div class="productFavorite"></div>
<div class="productId" itemprop="productID">170159</div>
<h2>
<a class="ajax" href="/objekt/spegel-1950-tal/118017" itemprop="name">
							SPEGEL, 1950-tal
						</a>
</h2>
<div class="productBidCurrent" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
						Bud:
						<span class="currentBid" data-customer-id="" itemprop="price">
							350 kr
						</span>
</div>
<div class="productEndTime" data-product-id="170159">
<span class="productClock" data-time="1610561280">Slutar idag 19:08</span>
</div>
</div>
</li>
<li data-last-updated="1610549663" data-product-id="170160" id="product-170160" itemscope="" itemtype="http://schema.org/Product">
<div class="image">
<a class="ajax" href="/objekt/bord-rullbart-rostfritt-stål/118018"><img alt="" itemprop="image" src="/images/custom/ProductTemplate/tn/311732.jpg"/></a>
</div>
<div class="information">
<div class="productFavorite"></div>
<div class="productId" itemprop="productID">170160</div>
<h2>
<a class="ajax" href="/objekt/bord-rullbart-rostfritt-stål/118018" itemprop="name">
							BORD, rullbart, rostfritt stål
						</a>
</h2>
<div class="productBidCurrent" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
						Bud:
						<span class="currentBid" data-customer-id="" itemprop="price">
							325 kr
						</span>
</div>
<div class="productEndTime" data-product-id="170160">
<span class="productClock" data-time="1610561400">Slutar idag 19:10</span>
</div>
</div>
</li>
</ul>
<div class="showMoreLink"><a href="/objektlista">Visa alla auktioner</a></div>
</div>
<div class="view customLayout sectionShow productCategorySpecial genericList col2">
<article class="productCategorySection" style="background-image: url('/images/custom/ProductCategory/131778.jpg');">
<a href="/objektlista/polisgods">
<img class="transparent transparent_1_1" src="/images/templates/balsta/transparent.png"/>
<img class="transparent transparent_2_1" src="/images/templates/balsta/transparent_2_1.png"/>
<img class="transparent transparent_3_2" src="/images/templates/balsta/transparent_3_2.png"/>
<header>
<h4>Polismyndighetens Beslagsgods</h4>
</header>
</a>
</article>
<article class="productCategorySection" style="background-image: url('/images/custom/ProductCategory/130818.jpg');">
<a href="/objektlista/smycken-klockor--accessoarer">
<img class="transparent transparent_1_1" src="/images/templates/balsta/transparent.png"/>
<img class="transparent transparent_2_1" src="/images/templates/balsta/transparent_2_1.png"/>
<img class="transparent transparent_3_2" src="/images/templates/balsta/transparent_3_2.png"/>
<header>
<h4>Klockor Smycken</h4>
</header>
</a>
</article>
<article class="productCategorySection">
<a href="/objektlista/månadens-utvalda---januari">
<img class="transparent transparent_1_1" src="/images/templates/balsta/transparent.png"/>
<img class="transparent transparent_2_1" src="/images/templates/balsta/transparent_2_1.png"/>
<img class="transparent transparent_3_2" src="/images/templates/balsta/transparent_3_2.png"/>
<header>
<h4>Månadens Utvalda - Januari</h4>
</header>
</a>
</article>
<article class="productCategorySection" style="background-image: url('/images/custom/ProductCategory/313821.jpg');">
<a href="/objektlista/vitvaror---24-januari">
<img class="transparent transparent_1_1" src="/images/templates/balsta/transparent.png"/>
<img class="transparent transparent_2_1" src="/images/templates/balsta/transparent_2_1.png"/>
<img class="transparent transparent_3_2" src="/images/templates/balsta/transparent_3_2.png"/>
<header>
<h4>Vitvaror - 24 januari</h4>
</header>
</a>
</article>
</div>
</div>
</main>
</div>
<footer>
<div class="view puff listNonDefault">
<article class="puff" id="puff-1" style="background-image: url([image]);" title="Öppettider">
<div class="content"><p style="text-align: center;"><strong> </strong><strong>ÖPPETTIDER</strong></p>
<p style="text-align: center;">Tisdagar 12.30-16.30</p>
<p style="text-align: center;">Onsdagar 12.30-16.30</p>
<p style="text-align: center;">Torsdagar 12.30-16.30</p>
<p style="text-align: center;">Fredagar 12.30-16.30</p>
<p style="text-align: center;">Lördagar Stängt</p>
<p style="text-align: center;">Söndagar Stängt</p>
<p style="text-align: center;">Måndagar Stängt</p></div>
</article>
<article class="puff" id="puff-2" style="background-image: url([image]);" title="Kontaktuppgifter">
<div class="content"><p><img alt="" height="224" src="/images/user/footer-logo.png" style="float: left;" width="600"/></p>
<p style="text-align: center;">Telefon 0171 53450</p>
<p style="text-align: center;"> <a href="mailto:info@balstaauktionshall.com">info@balstaauktionshall.com</a> </p>
<p style="text-align: center;"><a href="https://www.instagram.com/balstaauktionshall/" rel="noopener noreferrer" style="background: url('/images/user/Instagram_AppIcon.png') no-repeat left center; padding: 6px 56px; line-height: 32px;" target="_blank">Följ oss på Instagram</a></p>
<p style="text-align: center;">Besöksadress:<br/>Jättorpsvägen 3,</p>
<p style="text-align: center;">Ekolskrog/Bålsta 746 93</p></div>
</article>
</div> <div class="ucSigill">
<a href="https://www.uc.se/risksigill2/?showorg=556188-3884&amp;language=swe&amp;special=" target="_blank" title="Sigillet är utfärdat av UC AB. Klicka på bilden för information om UC:s Riskklasser."><img alt="" src="https://www.uc.se/ucsigill2/sigill?org=556188-3884&amp;language=swe&amp;product=lsa&amp;special=&amp;fontcolor=b&amp;type=svg" style="border:0;"/></a>
</div>
<div class="copyright argonova">
<a href="http://argonova.se/" target="_blank">Webbdesign</a> &amp; <a href="http://argonova.se/webbsystem/publiceringsverktyg/cms.php" target="_blank">Webbsystem</a> - <a href="http://argonova.se/blogg" target="_blank">Argonova Systems</a>
</div>
</footer>
</div>
<script src="/js/modules/product/clocks.php"></script>
<script src="/js/modules/product/auctionUpdate.php"></script>
<script src="/js/modules/product/favoriteHandler.php"></script></body>
</html>
