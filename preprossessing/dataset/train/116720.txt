<!DOCTYPE html>
<html lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/> <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="/favicon.ico" rel="icon" type="image/x-icon"/><link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/> <title>AmericanTowns.com: Online Local Community Network - Connecting The Community Is What We Do Best</title>
<meta content="AmericanTowns.com: Online Local Community Network - Connecting The Community Is What We Do Best" name="page_title"/>
<meta content="index, follow" name="robots"/>
<meta content="AmericanTowns.com: Online Local Community Network - Connecting The Community Is What We Do Best" name="description"/>
<meta content="AmericanTowns, Online, Local, Community, Network, Connecting, What, We, Do, Best" name="keywords"/>
<link href="https://www.americantowns.com/" rel="canonical"/>
<meta content="name" name="twitter:card"/>
<meta content="http://www.americantowns.com/" property="og:url"/>
<!-- Bootstrap -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Optional theme -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"/>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
<link href="" rel="shortcut icon"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700,800" rel="stylesheet" type="text/css"/>
<link href="https://p300live-americantownscom.netdna-ssl.com/css/animate.min.css" rel="stylesheet"/>
<link href="https://p300live-americantownscom.netdna-ssl.com/css/style.css" rel="stylesheet"/>
<link href="https://p300live-americantownscom.netdna-ssl.com/css/americantowns.css" rel="stylesheet"/>
<link href="https://p300live-americantownscom.netdna-ssl.com/css/atmedia.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat|PT+Sans|Raleway:400,900" rel="stylesheet"/>
<!--token : Header Script Module-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script-->
<script crossorigin="anonymous" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1662663580647713');
fbq('track', "PageView");</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=1662663580647713&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<!-- End Facebook Pixel Code -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-370662-1', 'americantowns.com');
  ga('send', 'pageview');
var dimensionValue = '1';
ga('set', 'dimension1', ', ');
</script>
<!-- START OF Onclick Google Analytics TAG -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-45868632-1']);
  _gaq.push(['_trackPageview']);
 _gaq.push(['_trackPageLoadTime']);
ga('1', ', ', 'Town');
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();


function _pageTracker (type) {
this.type = type;
this._trackEvent = function(a,b,c) {
_gaq.push(['_trackEvent', a, b, c]);
};
this._trackPageview = function(a) {
_gaq.push(['_trackPageview', a]);
};
}
var pageTracker = new _pageTracker();

</script>
<!-- END OF Onclick Google Analytics Tag -->
<script type="text/javascript">
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') + 
'//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})();
</script>
<script type="text/javascript">
 var p = /yp\/.*escort/g;
 var Restricted = p.test(window.location.href);
</script>
<script type="text/javascript">
if (!Restricted) {
googletag.cmd.push(function() {
googletag.defineSlot('/1005417/AmericanTowns_728_Top_Interior', [728, 90], 'div-gpt-ad-1365456931459-2').addService(googletag.pubads());
googletag.defineSlot('/1005417/AmericanTowns_728_Top_Interior_Left', [728, 90], 'div-gpt-ad-1367007795783-0').addService(googletag.pubads());
googletag.defineSlot('/1005417/AmericanTowns_300_Top_Side', [300, 250], 'div-gpt-ad-1365456931459-0').addService(googletag.pubads());
googletag.defineSlot('/1005417/AmericanTowns_728_Bottom_Interior', [728, 90], 'div-gpt-ad-1365456931459-1').addService(googletag.pubads());

googletag.pubads().setTargeting('State', '');
googletag.pubads().setTargeting('County', ' County');
googletag.pubads().setTargeting('City', '');

googletag.enableServices();
});
}
</script>
<script language="JavaScript">
/*GA_googleAddAttr("State", "");
GA_googleAddAttr("County", " County");
GA_googleAddAttr("City", "");*/
</script>
<script>
		var basecdn = 'https://p300live-americantownscom.netdna-ssl.com';
			</script>
<script async="" src="//forms.americantowns.com/js/formsApi.js" type="text/javascript"></script>
<script async="" src="//forms.americantowns.com/js/jspopupscript.js" type="text/javascript"></script>
<link href="//forms.americantowns.com/css/jspopupstyle.css" rel="stylesheet"/>
</head>
<body>
<!--
<div id="topline">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-right atm-top-menu">
                <div id="topdiv">
<ul id="topnav">
                        <li><a href="/newhome/">Home</a></li>
                        <li><a href="https://www.americantownsmedia.com/about-us">About</a></li>
                        <li><a href="https://www.americantownsmedia.com/connect-with-us">Contact</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
-->
<div id="medline">
<div class="container">
<div class="row">
<div class="col-md-6 col-sm-12 col-sx-12 atmedia-mid-logo">
<div id="toplogo">
<!--<img src="https://s3.amazonaws.com/americantowns/img/atownslogotext2.png"/>-->
<div class="attitle"><span class="attitle_a">AMERICAN</span><span class="attitleb">TOWNS</span></div>
</div>
</div>
<div class="col-md-6 col-sm-12 col-sx-12 atmedia-mid-nav">
<div id="middiv">
<ul id="minnav">
<li><a href="/">Home</a></li>
<li><a href="https://www.americantownsmedia.com/about-us">About</a></li>
<li><a href="https://support.americantowns.com/hc/en-us/requests/new">Contact</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="modulemodule" id="row-panel1"><div class="container">
<div class="row">
<div class="atmediaboxes" style="padding-right: 0px; padding-top: 0px;">
<div class="fullboxleft">
<div class="mainlinks">
<div class="maintitle">Your Town</div>
<div class="maindetails">Come in to see why people rely on AmericanTowns.com to find &amp; share everything local</div>
<div class="mainlink">
<div class="row">
<div class="col-md-8 col-md-offset-2">
<form action="#" id="guestform" method="get" name="form1">
<div class="input-group">
<input autocomplete="off" class="cls-input city-state form-control ui-autocomplete-input" id="choose_city" name="search_terms" placeholder="Enter your city or zipcode here" type="text"/>
<span class="input-group-btn">
<button alt="Submit button" class="submit btn btn-primary btn-block" id="changecitybutton" type="button">Go</button>
</span>
</div>
<a class="go-to-browser-location small" href="" onclick="return false" style="color: #b4b8b9;">Use Browser/Current Location <i class="fa fa-location-arrow"></i></a>
<br/>
<a class="small" href="/states/" style="color: #b4b8b9;">Choose by State <i class="fa fa-book"></i></a>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-12" style="margin: 0px 0px 20px 0px;"><hr style="border-top: 3px solid #00b0e0;"/></div>
</div>
</div>
</div>
<footer id="footer">
<div class="container">
<!--token : Footer Menu Module--><br/>
//= Modules::load('AT - Footer Section') ?&gt;
<div class="row copyright" style="display:flex; justify-content:space-around">
<div class="col-md-4">
<h4>Need Help?</h4>
<div><a href="https://www.americantownsmedia.com/about-us">About Us</a></div>
<div><a href="https://support.americantowns.com/hc/en-us/requests/new">Contact Us</a></div>
<div><a href="http://support.americantowns.com">Frequently Asked Questions</a></div>
<div><a href="/about/whyjoin.html">Why Join?</a></div>
<div><a href="http://support.americantowns.com">Report Incorrect Information</a></div>
</div>
<div class="col-md-4">
<h4>© 2019  All rights reserved.</h4>
<div><a href="https://www.americantownsmedia.com/terms">Terms and Conditions of Use</a></div>
<div><a href="https://www.americantownsmedia.com/privacy-policy">Privacy Policy</a></div>
<div><a href="https://www.americantownsmedia.com/dmca-policy">DMCA Policy</a></div>
<br/>
             Local Content Optimization powered by <a href="https://www.americantownsmedia.com/"> AmericanTowns Media</a>
</div>
</div>
<br/>
</div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7c0aYAl7_lT--ouR64Fx1lFOHVM4vMbQ"></script>
<script src="https://p300live-americantownscom.netdna-ssl.com/js/blazy.min.js"></script>
<script src="https://p300live-americantownscom.netdna-ssl.com/js/script.js"></script>
<script src="https://p300live-americantownscom.netdna-ssl.com/js/americantowns.js"></script>
<script src="https://p300live-americantownscom.netdna-ssl.com/js/modernizr.js"></script>
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
<!--token : Footer Script Module-->

//= Modules::load('ATMedia - Footer Script') ?&gt;

</body>
</html>
