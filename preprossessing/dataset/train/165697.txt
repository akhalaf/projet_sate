<!DOCTYPE html>
<!-- jsn_gruve_pro 3.1.0 --><html dir="ltr" lang="">
<head>
<base href="https://www.athensairportbus.com/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="Ελ. Βενιζέλος, λεωφορεία αεροδρόμιο, Χ93, Χ95, Χ96, Χ97, δρομολόγια, Σύνταγμα αεροδρόμιο), Πειραιάς αεροδρόμιο, ελληνικό - αεροδρόμιο, κηφισού αεροδρόμιο" name="keywords"/>
<meta content="Λεωφορεία Αεροδρομίου Ελ. Βενιζελος - Τα δρομολόγια Χ95 (Σύνταγμα - Αεροδρόμιο), Χ96 (Πειραιάς - Αεροδρόμιο), Χ97 (Μετρό Ελληνικό - Αεροδρόμιο) Χ93 (Σταθμός Λεωφ. Κηφισού - Αεροδρόμιο) λειτουργούν όλο το 24ωρο. Δείτε τα δρομολόγια λεωφορίων, ώρες αναχώρησης, χάρτες και στάσεις των διαδρόμων  Χ93, Χ95, Χ96, Χ97 από και προς το αεροδρόμιο Ελευθέριος Βενιζέλος." name="description"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>Λεωφορεία Αθήνα Αεροδρόμιο Ελ. Βενιζελος - Δρομολόγια Χ93, Χ95, Χ96, Χ97 για Αεροδρόμιο Αθήνας</title>
<link href="http://www.athensairportbus.com/" rel="canonical"/>
<link href="/templates/jsn_gruve_pro/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/cache/jsn_gruve_pro/b131a06d5bee6ca93dd90ecfcfd48c98.css?951cb152a0cf552fc3ab7c17e336e79f" rel="stylesheet" type="text/css"/>
<link href="/cache/jsn_gruve_pro/6b54d2d92485cf4c30ab2957278ce17e.css?951cb152a0cf552fc3ab7c17e336e79f" media="print" rel="stylesheet" type="text/css"/>
<link href="/cache/jsn_gruve_pro/537ffb85051c170ccc368b3aa4e09ef7.css?951cb152a0cf552fc3ab7c17e336e79f" rel="stylesheet" type="text/css"/>
<style type="text/css">
	div.jsn-modulecontainer ul.menu-mainmenu ul,
	div.jsn-modulecontainer ul.menu-mainmenu ul li {
		width: 200px;
	}
	div.jsn-modulecontainer ul.menu-mainmenu ul ul {
		margin-left: 199px;
	}
	#jsn-pos-toolbar div.jsn-modulecontainer ul.menu-mainmenu ul ul {
		margin-right: 199px;
		margin-left : auto
	}
	div.jsn-modulecontainer ul.menu-sidemenu ul,
	div.jsn-modulecontainer ul.menu-sidemenu ul li {
		width: 200px;
	}
	div.jsn-modulecontainer ul.menu-sidemenu li ul {
		right: -200px;
	}
	body.jsn-direction-rtl div.jsn-modulecontainer ul.menu-sidemenu li ul {
		left: -200px;
		right: auto;
	}
	div.jsn-modulecontainer ul.menu-sidemenu ul ul {
		margin-left: 199px;
	}
	</style>
<script class="joomla-script-options new" type="application/json">{"csrf.token":"0ddd5a4fd5d9daaf6426af9f1eb023a5","system.paths":{"root":"","base":""}}</script>
<script src="https://www.athensairportbus.com/cache/jsn_gruve_pro/cac62cbe4425277a97666a480cb5266d.js?951cb152a0cf552fc3ab7c17e336e79f" type="text/javascript"></script>
<script src="https://www.athensairportbus.com/cache/jsn_gruve_pro/6b0de9ee8eecd56f41d176468177cf03.js?951cb152a0cf552fc3ab7c17e336e79f" type="text/javascript"></script>
<script src="https://www.athensairportbus.com/cache/jsn_gruve_pro/db0c77fea39deb60407cce387d7c6290.js?951cb152a0cf552fc3ab7c17e336e79f" type="text/javascript"></script>
<script src="https://www.athensairportbus.com/cache/jsn_gruve_pro/4cec93b9855cd6741f6d70f1b8d2fbbd.js?951cb152a0cf552fc3ab7c17e336e79f" type="text/javascript"></script>
<script src="https://www.athensairportbus.com/cache/jsn_gruve_pro/30ca306c98a6a1ca9164f2c6ec4ac4b0.js?951cb152a0cf552fc3ab7c17e336e79f" type="text/javascript"></script>
<script src="/media/system/js/modal.js?951cb152a0cf552fc3ab7c17e336e79f" type="text/javascript"></script>
<script src="https://www.athensairportbus.com/cache/jsn_gruve_pro/1a0ce068e5c20279072bec316af4c7c7.js?951cb152a0cf552fc3ab7c17e336e79f" type="text/javascript"></script>
<script type="text/javascript">
jQuery(window).on('load',  function() {
				new JCaption('img.caption');
			});
				JSNTemplate.initTemplate({
					templatePrefix			: "jsn_gruve_pro_",
					templatePath			: "/templates/jsn_gruve_pro",
					enableRTL				: 0,
					enableGotopLink			: 1,
					enableMobile			: 1,
					enableMobileMenuSticky	: 1,
					enableDesktopMenuSticky	: 0,
					responsiveLayout		: ["mobile"],
					mobileMenuEffect		: "default"
				});
			
		jQuery(function($) {
			SqueezeBox.initialize({});
			initSqueezeBox();
			$(document).on('subform-row-add', initSqueezeBox);

			function initSqueezeBox(event, container)
			{
				SqueezeBox.assign($(container || document).find('a.modal').get(), {
					parse: 'rel'
				});
			}
		});

		window.jModalClose = function () {
			SqueezeBox.close();
		};

		// Add extra modal close functionality for tinyMCE-based editors
		document.onreadystatechange = function () {
			if (document.readyState == 'interactive' && typeof tinyMCE != 'undefined' && tinyMCE)
			{
				if (typeof window.jModalClose_no_tinyMCE === 'undefined')
				{
					window.jModalClose_no_tinyMCE = typeof(jModalClose) == 'function'  ?  jModalClose  :  false;

					jModalClose = function () {
						if (window.jModalClose_no_tinyMCE) window.jModalClose_no_tinyMCE.apply(this, arguments);
						tinyMCE.activeEditor.windowManager.close();
					};
				}

				if (typeof window.SqueezeBoxClose_no_tinyMCE === 'undefined')
				{
					if (typeof(SqueezeBox) == 'undefined')  SqueezeBox = {};
					window.SqueezeBoxClose_no_tinyMCE = typeof(SqueezeBox.close) == 'function'  ?  SqueezeBox.close  :  false;

					SqueezeBox.close = function () {
						if (window.SqueezeBoxClose_no_tinyMCE)  window.SqueezeBoxClose_no_tinyMCE.apply(this, arguments);
						tinyMCE.activeEditor.windowManager.close();
					};
				}
			}
		};
		window.cookieconsent_options = {"learnMore":"Read More - \u03a0\u03b5\u03c1\u03b9\u03c3\u03c3\u03cc\u03c4\u03b5\u03c1\u03b1","dismiss":"OK","message":"By using this website you accept the use of cookies. \u0397 \u03b9\u03c3\u03c4\u03bf\u03c3\u03b5\u03bb\u03af\u03b4\u03b1 \u03c7\u03c1\u03b7\u03c3\u03b9\u03bc\u03bf\u03c0\u03bf\u03b9\u03b5\u03af cookies. \u039f\u03b4\u03b7\u03b3\u03af\u03b5\u03c2 \u0391\u03c0\u03b5\u03bd\u03b5\u03c1\u03b3\u03bf\u03c0\u03bf\u03af\u03b7\u03c3\u03b7\u03c2 https:\/\/www.wikihow.com\/Disable-Cookies ","link":"https:\/\/athensairportbus.com\/cookies-policy","theme":"\/plugins\/system\/jsntplframework\/assets\/3rd-party\/cookieconsent\/styles\/light-bottom.css"};
	</script>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=2.0" name="viewport"/>
<!-- html5.js and respond.min.js for IE less than 9 -->
<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script src="/plugins/system/jsntplframework/assets/3rd-party/respond/respond.min.js"></script>
	<![endif]-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-139924665-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-139924665-2');
</script>
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-2954210190324642",
          enable_page_level_ads: true
     });
</script></head>
<body class="jsn-textstyle-business jsn-color-blue jsn-direction-ltr jsn-responsive jsn-mobile jsn-joomla-30 jsn-com-content jsn-view-article jsn-itemid-648 jsn-homepage" id="jsn-master">
<a id="top"></a>
<div class="container" id="jsn-page">
<div id="jsn-header">
<div class="pull-left" id="jsn-logo">
<div id="jsn-pos-logo">
<div class=" jsn-modulecontainer"><div class="jsn-modulecontainer_inner"><div class="jsn-modulecontent">
<div class="jsn-mod-custom">
<h1 style="font-size: 28px;"><a href="https://athensairportbus.com/" style="text-decoration: none; color: #ffffff;">Λεωφορεία Αεροδρόμιου</a></h1></div><div class="clearbreak"></div></div></div></div>
</div>
</div>
<div class="pull-right" id="jsn-headerright">
<div class="pull-left" id="jsn-pos-top">
<div class=" jsn-modulecontainer"><div class="jsn-modulecontainer_inner"><div class="jsn-modulecontent"><div class="mod-languages">
<ul class="lang-inline" dir="ltr">
<li class="lang-active">
<a href="https://www.athensairportbus.com/">
<img alt="Ελληνικά" src="/media/mod_languages/images/el.gif" title="Ελληνικά"/> </a>
</li>
<li>
<a href="/en/">
<img alt="English (UK)" src="/media/mod_languages/images/en.gif" title="English (UK)"/> </a>
</li>
</ul>
</div>
<div class="clearbreak"></div></div></div></div>
<div class="clearbreak"></div>
</div>
</div>
<div class="clearbreak"></div>
</div>
<div id="jsn-body">
<div id="jsn-menu">
<div id="jsn-pos-mainmenu">
<div class=" jsn-modulecontainer"><div class="jsn-modulecontainer_inner"><div class="jsn-modulecontent"><span class="jsn-menu-toggle" id="jsn-menu-toggle-parent">Menu</span>
<ul class="menu-mainmenu">
<li class="jsn-menu-mobile-control"><span class="close-menu"><i class="fa fa-times"></i></span></li>
<li class="active parent first"><a class="clearfix" href="javascript: void(0)">
<span>
<span class="jsn-menutitle">ΑΡΧΙΚΗ</span> </span>
</a>
<span class="jsn-menu-toggle right-10"></span><ul><li class="current active first"><a class="current clearfix" href="/" title="Αρχική Σελίδα - Λεωφορεία Αεροδρόμιου">
<span>
<span class="jsn-menutitle">Γενικά</span><span class="jsn-menudescription">Αρχική Σελίδα - Λεωφορεία Αεροδρόμιου</span> </span>
</a></li><li class=""><a class="clearfix" href="/leoforeia/eisithria-leoforiou-aerodromio.html" title="Εισιτήρια λεωφορίων για Ελ. Βενιζέλος">
<span>
<span class="jsn-menutitle">Εισιτήρια</span><span class="jsn-menudescription">Εισιτήρια λεωφορίων για Ελ. Βενιζέλος</span> </span>
</a></li><li class=""> <a class="clearfix" href="/λεωφορεια/χ95-αεροδρομιο-συνταγμα.html" title="Χ95 Συνταγμα Αεροδρομιο">
<span>
<span class="jsn-menutitle">X95 Σύνταγμα</span><span class="jsn-menudescription">Χ95 Συνταγμα Αεροδρομιο</span> </span>
</a>
</li><li class=""> <a class="clearfix" href="/λεωφορεια/χ96-αεροδρομιο-πειραιας.html" title="Χ96 Πειραιάς Αεροδρόμιο">
<span>
<span class="jsn-menutitle">Χ96 Πειραιάς</span><span class="jsn-menudescription">Χ96 Πειραιάς Αεροδρόμιο</span> </span>
</a>
</li><li class=""> <a class="clearfix" href="/λεωφορεια/χ93-αεροδρομιο-κτελ.html" title="Χ93 ΚΤΕΛ Κηφισού Αεροδρόμιο">
<span>
<span class="jsn-menutitle">Χ93 Σταθμός Κηφισού</span><span class="jsn-menudescription">Χ93 ΚΤΕΛ Κηφισού Αεροδρόμιο</span> </span>
</a>
</li><li class="last"> <a class="clearfix" href="/λεωφορεια/χ97-αεροδρομιο-ελληνικο.html" title="X97 Μετρό Ελληνικό - Αεροδρόμιο">
<span>
<span class="jsn-menutitle">X97 Ελληνικό</span><span class="jsn-menudescription">X97 Μετρό Ελληνικό - Αεροδρόμιο</span> </span>
</a>
</li></ul></li><li class="parent"><a class="clearfix" href="javascript: void(0)">
<span>
<span class="jsn-menutitle">ΔΡΟΜΟΛΟΓΙΑ ΑΕΡΟΔΡ.</span> </span>
</a>
<span class="jsn-menu-toggle right-10"></span><ul><li class="first"><a class="clearfix" href="/λεωφορεια/χ93-αεροδρομιο-κτελ.html" title="X93 από αεροδρομιο">
<span>
<span class="jsn-menutitle">X93: Προς ΚΤΕΛ Κηφισού</span><span class="jsn-menudescription">X93 από αεροδρομιο</span> </span>
</a></li><li class=""><a class="clearfix" href="/λεωφορεια/χ95-αεροδρομιο-συνταγμα.html" title="Χ95 από αεροδρομιο">
<span>
<span class="jsn-menutitle">Χ95: Προς Σύνταγμα</span><span class="jsn-menudescription">Χ95 από αεροδρομιο</span> </span>
</a></li><li class=""><a class="clearfix" href="/λεωφορεια/χ96-αεροδρομιο-πειραιας.html" title="Χ96 από αεροδρομιο">
<span>
<span class="jsn-menutitle">Χ96: Προς Πειραιά</span><span class="jsn-menudescription">Χ96 από αεροδρομιο</span> </span>
</a></li><li class=""><a class="clearfix" href="/λεωφορεια/χ97-αεροδρομιο-ελληνικο.html" title="χ97 από αεροδρομιο">
<span>
<span class="jsn-menutitle">X97: Προς Ελληνικό</span><span class="jsn-menudescription">χ97 από αεροδρομιο</span> </span>
</a></li><li class=""><a class="clearfix" href="/λεωφορεια/χ93_λεωφορείο_κτελ_αεροδρόμιο.html" title="Χ93 προς αεροδρόμιο">
<span>
<span class="jsn-menutitle">X93: Από ΚΤΕΛ Κηφισού</span><span class="jsn-menudescription">Χ93 προς αεροδρόμιο</span> </span>
</a></li><li class=""><a class="clearfix" href="/λεωφορεια/x95_leoforio_syntagma_aerodromio.html" title="Χ95 προς αεροδρόμιο">
<span>
<span class="jsn-menutitle">Χ95: Από Σύνταγμα</span><span class="jsn-menudescription">Χ95 προς αεροδρόμιο</span> </span>
</a></li><li class=""><a class="clearfix" href="/λεωφορεια/x96_leoforio_piraeus_aerodromio.html" title="Χ96 προς αεροδρόμιο">
<span>
<span class="jsn-menutitle">Χ96: Από Πειραιά</span><span class="jsn-menudescription">Χ96 προς αεροδρόμιο</span> </span>
</a></li><li class=""><a class="clearfix" href="/λεωφορεια/x97_λεωφορειο-μετρο-ελληνικο-προς-ελ-βενιζελος.html" title="Χ97 προς αεροδρόμιο">
<span>
<span class="jsn-menutitle">X97: Από Ελληνικό</span><span class="jsn-menudescription">Χ97 προς αεροδρόμιο</span> </span>
</a></li><li class=""><a class="clearfix" href="/λεωφορεια/ραφηνα-αεροδρομιο.html">
<span>
<span class="jsn-menutitle">Ραφήνα - Αεροδρόμιο</span> </span>
</a></li><li class="last"><a class="clearfix" href="/λεωφορεια/αεροδρόμιο-ραφήνα.html">
<span>
<span class="jsn-menutitle">Αεροδρόμιο - Ραφήνα </span> </span>
</a></li></ul></li><li class="parent"><a class="clearfix" href="javascript: void(0)">
<span>
<span class="jsn-menutitle">ΜΕΤΡΟ</span> </span>
</a>
<span class="jsn-menu-toggle right-10"></span><ul><li class="first"><a class="clearfix" href="/metro/metro-athina.html">
<span>
<span class="jsn-menutitle">Τιμές Εισιτηρίων</span> </span>
</a></li><li class=""><a class="clearfix" href="/metro/metro-pros-aerodromio.html">
<span>
<span class="jsn-menutitle">Δρομολόγια προς αεροδρόμιο</span> </span>
</a></li><li class="last"><a class="clearfix" href="/metro/dromologio-apo-aerodromio.html">
<span>
<span class="jsn-menutitle">Δρομολόγια από αεροδρόμιο</span> </span>
</a></li></ul></li><li class="parent"><a class="clearfix" href="javascript: void(0)">
<span>
<span class="jsn-menutitle">ΠΡΟΑΣΤΙΑΚΟΣ</span> </span>
</a>
<span class="jsn-menu-toggle right-10"></span><ul><li class="first"><a class="clearfix" href="/προαστιακος/εισιτήρια.html">
<span>
<span class="jsn-menutitle">Γενικά</span> </span>
</a></li><li class=""><a class="clearfix" href="/προαστιακος/απο-αεροδρομιο.html">
<span>
<span class="jsn-menutitle">Από Αεροδρόμιο</span> </span>
</a></li><li class="last"><a class="clearfix" href="/προαστιακος/προς-αεροδρομιο.html" title="Paphos Airport Shuttle - Bus 613: Airport - City Center">
<span>
<span class="jsn-menutitle">Προς Αεροδρόμιο</span><span class="jsn-menudescription">Paphos Airport Shuttle - Bus 613: Airport - City Center</span> </span>
</a></li></ul></li><li class="last"><a class="clearfix" href="/επικοινωνια.html">
<span>
<span class="jsn-menutitle">ΑΛΛΑ</span> </span>
</a></li></ul>
<div class="clearbreak"></div></div></div></div>
</div>
<div class="clearbreak"></div>
</div>
<div class="jsn-hasinnerleft " id="jsn-content">
<div class="row-fluid" id="jsn-content_inner">
<div class="span12 order1 row-fluid" id="jsn-maincontent">
<div id="jsn-maincontent_inner">
<div id="jsn-centercol"><div id="jsn-centercol_inner">
<div class=" jsn-hasmainbody" id="jsn-mainbody-content">
<div id="jsn-mainbody-content-inner1"><div id="jsn-mainbody-content-inner2"><div id="jsn-mainbody-content-inner3"><div class="row-fluid" id="jsn-mainbody-content-inner4">
<div class="span9 order2 offset3" id="jsn-mainbody-content-inner">
<div id="jsn-mainbody">
<div id="system-message-container">
</div>
<div class="item-page" itemscope="" itemtype="https://schema.org/Article">
<meta content="el" itemprop="inLanguage"/>
<div class="page-header">
<h2 itemprop="headline">
			Λεωφορεία από και προς το Αεροδρόμιο		</h2>
</div>
<div itemprop="articleBody">
<p>	Τα δρομολόγια από και προς το αεροδρόμιο Ελ. Βενιζέλος λειτουργούν καθημερινά όλο το εικοσιτετράωρο. Το λεωφορείο είναι η πιο οικονομική και γρήγορη λύση προς το αεροδρόμιο, ιδιαίτερα κατά τις βραδυνές ώρες που δε λειτουργεί το μετρό και τα ταξί δουλεύουν με διπλή χρέωση.</p>
<p><img alt="Λεωφορείο από Αεροδρόμιο Ελευθέριος προς κέντρο Αθήνας, δρομολόγια" src="/images/athens-airport-to-athens-city-center-by-bus.JPG" title="Λεωφορεία από Αεροδρόμιο Ελευθέριος προς Αθήνα, δρομολόγια"/></p>
<p>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Athens Airport Bus -->
<ins class="adsbygoogle" data-ad-client="ca-pub-2954210190324642" data-ad-format="auto" data-ad-slot="1837088138" data-full-width-responsive="true" style="display:block"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
</p>
<p>Υπάρχουν συνολικά τέσσερα δρομολόγια για το αεροδρόμιο. Παρακάτω είναι μια σύντομη περιγραφή των δρομολογίων. Για περισσότερες πληροφορίες, ώρες αναχώρησης, χάρτες των διαδρόμων, στάσεις κ.τ.λ. <strong> κάντε κλικ στον τίτλο του δρομολογίου.</strong></p>
<h2><a href="/λεωφορεια/x95_leoforio_syntagma_aerodromio.html" hreflang="el" title="Δρομολόγια Χ95 Αεροδρόμιο">- X95: Σύνταγμα - Αεροδρόμιο</a></h2>
<p><strong>Δρομολόγιο: </strong>Σύνταγμα - Βασιλίσσης Σοφίας - Μεσογείων - Λεωφόρος Μαραθώνος - Αττική οδός - Κτίριο Τελωνείου - Αεροδρόμιο. Είναι δυνατή σύνδεση με τις γραμμές 2 και 3 του μετρό. </p>
<h2 style="margin-top:25px"><a href="/λεωφορεια/x96_leoforio_piraeus_aerodromio.html" hreflang="el" title="Δρομολόγια Χ96 Αεροδρόμιο">- X96: Πειραιάς - Αεροδρόμιο</a></h2>
<p><strong>Δρομολόγιο: </strong>Ακτή Βασιλειάδη - Σταθμός ΗΣΑΠ - Τζαβέλα - Λεωφόρος Ποσειδώνος - Κων/νου Καραμανλή - Βάρης -Κορωπίου - Αεροδρόμιο</p>
<p>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Athens Airport Bus -->
<ins class="adsbygoogle" data-ad-client="ca-pub-2954210190324642" data-ad-format="auto" data-ad-slot="1837088138" data-full-width-responsive="true" style="display:block"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
</p>
<h2 style="margin-top:25px"><a href="/λεωφορεια/x97_λεωφορειο-μετρο-ελληνικο-προς-ελ-βενιζελος.html" hreflang="el" title="Δρομολόγια Χ97 Αεροδρόμιο">- X97: Μετρό Ελληνικό - Αεροδρόμιο</a></h2>
<p><strong>Δρομολόγιο: </strong>Σταθμός Μετρό Ελληνικό - Λεωφόρος Βουλιαγμένης - Λεωφόρος Βάρης Κορωπίου - Λεωφόρος Κορωπίου - Κτήριο Τελωνείου - Αεροδρόμιο</p>
<h2 style="margin-top:25px"><a href="/λεωφορεια/χ93_λεωφορείο_κτελ_αεροδρόμιο.html" hreflang="el" title="Δρομολόγια Χ93 Αεροδρόμιο">- X93: Σταθμός Υπερ. Λεωφ. Κηφισού - Αεροδρόμιο</a></h2>
<p><strong>Δρομολόγιο: </strong>Σταθμός υπεραστικών λεωφορείων - Λεωφόρος Κηφισού - Λιοσίων - Αττική Οδός - Κτήριο Τελωνείου - Αεροδρόμιο.</p> </div>
</div>
</div>
</div>
<div class="span3 order1 offset-12" id="jsn-pos-innerleft">
<div id="jsn-pos-innerleft_inner">
<div class="lightbox-2 jsn-modulecontainer"><div class="jsn-modulecontainer_inner"><h3 class="jsn-moduletitle"><span class="jsn-moduleicon">ΑΡΧΙΚΗ</span></h3><div class="jsn-modulecontent"><span class="jsn-menu-toggle" id="jsn-menu-toggle-parent">Menu</span>
<ul class="menu-treemenu">
<li class="jsn-menu-mobile-control"><span class="close-menu"><i class="fa fa-times"></i></span></li>
<li class="current active first"><a class="current clearfix" href="/" title="Αρχική Σελίδα - Λεωφορεία Αεροδρόμιου">
<span>
<span class="jsn-menutitle">Γενικά</span><span class="jsn-menudescription">Αρχική Σελίδα - Λεωφορεία Αεροδρόμιου</span> </span>
</a></li><li class=""><a class="clearfix" href="/leoforeia/eisithria-leoforiou-aerodromio.html" title="Εισιτήρια λεωφορίων για Ελ. Βενιζέλος">
<span>
<span class="jsn-menutitle">Εισιτήρια</span><span class="jsn-menudescription">Εισιτήρια λεωφορίων για Ελ. Βενιζέλος</span> </span>
</a></li><li class=""> <a class="clearfix" href="/λεωφορεια/χ95-αεροδρομιο-συνταγμα.html" title="Χ95 Συνταγμα Αεροδρομιο">
<span>
<span class="jsn-menutitle">X95 Σύνταγμα</span><span class="jsn-menudescription">Χ95 Συνταγμα Αεροδρομιο</span> </span>
</a>
</li><li class=""> <a class="clearfix" href="/λεωφορεια/χ96-αεροδρομιο-πειραιας.html" title="Χ96 Πειραιάς Αεροδρόμιο">
<span>
<span class="jsn-menutitle">Χ96 Πειραιάς</span><span class="jsn-menudescription">Χ96 Πειραιάς Αεροδρόμιο</span> </span>
</a>
</li><li class=""> <a class="clearfix" href="/λεωφορεια/χ93-αεροδρομιο-κτελ.html" title="Χ93 ΚΤΕΛ Κηφισού Αεροδρόμιο">
<span>
<span class="jsn-menutitle">Χ93 Σταθμός Κηφισού</span><span class="jsn-menudescription">Χ93 ΚΤΕΛ Κηφισού Αεροδρόμιο</span> </span>
</a>
</li><li class="last"> <a class="clearfix" href="/λεωφορεια/χ97-αεροδρομιο-ελληνικο.html" title="X97 Μετρό Ελληνικό - Αεροδρόμιο">
<span>
<span class="jsn-menutitle">X97 Ελληνικό</span><span class="jsn-menudescription">X97 Μετρό Ελληνικό - Αεροδρόμιο</span> </span>
</a>
</li></ul>
<div class="clearbreak"></div></div></div></div><div class="lightbox-2 jsn-modulecontainer"><div class="jsn-modulecontainer_inner"><h3 class="jsn-moduletitle"><span class="jsn-moduleicon">Like and Share :)</span></h3><div class="jsn-modulecontent">
<div class="jsn-mod-custom">
<!-- Facebook like -->
<div id="fb-root"> </div>
<script>// <![CDATA[
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3&appId=206017032936277";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
// ]]></script>
<div class="fb-like" data-action="like" data-href="http://athensairportbus.com/" data-layout="button_count" data-share="true" data-show-faces="true"> </div></div><div class="clearbreak"></div></div></div></div><div class="lightbox-2 jsn-modulecontainer"><div class="jsn-modulecontainer_inner"><div class="jsn-modulecontent">
<div class="jsn-mod-custom">
<p>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Athens Airport Bus -->
<ins class="adsbygoogle" data-ad-client="ca-pub-2954210190324642" data-ad-format="auto" data-ad-slot="1837088138" data-full-width-responsive="true" style="display:block"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
</p></div><div class="clearbreak"></div></div></div></div>
</div>
</div>
</div></div></div></div></div>
</div></div> <!-- end centercol -->
</div></div><!-- end jsn-maincontent -->
</div>
</div>
</div>
<div id="jsn-footer">
<div class="jsn-modulescontainer jsn-modulescontainer2 row-fluid" id="jsn-footermodules">
<div class="span6" id="jsn-pos-footer">
<div class=" jsn-modulecontainer"><div class="jsn-modulecontainer_inner"><div class="jsn-modulecontent"><span class="jsn-menu-toggle" id="jsn-menu-toggle-parent">Menu</span>
<ul class="menu-divmenu">
<li class="jsn-menu-mobile-control"><span class="close-menu"><i class="fa fa-times"></i></span></li>
<li class="first"> <a class="clearfix" href="/">
<span>
<span class="jsn-menutitle">Αρχική</span> </span>
</a>
</li><li class=""> <a class="clearfix" href="/λεωφορεια/χ95-αεροδρομιο-συνταγμα.html">
<span>
<span class="jsn-menutitle">Προς Κέντρο</span> </span>
</a>
</li><li class=""> <a class="clearfix" href="/λεωφορεια/χ96-αεροδρομιο-πειραιας.html">
<span>
<span class="jsn-menutitle">Προς Πειραιά</span> </span>
</a>
</li><li class=""> <a class="clearfix" href="/metro/metro-athina.html">
<span>
<span class="jsn-menutitle">Μετρό</span> </span>
</a>
</li><li class=""> <a class="clearfix" href="/προαστιακος/εισιτήρια.html">
<span>
<span class="jsn-menutitle">Προαστιακός</span> </span>
</a>
</li><li class="last"><a class="clearfix" href="/σχόλια.html">
<span>
<span class="jsn-menutitle">Cookies Policy</span> </span>
</a></li></ul>
<div class="clearbreak"></div></div></div></div><div class=" jsn-modulecontainer"><div class="jsn-modulecontainer_inner"><div class="jsn-modulecontent">
<div class="jsn-mod-custom">
<p>Τελευταία ενημέρωση του ιστότοπου για τα δρομολόγια αεροδρόμιου: May 2019, Ερωτήσεις/Συζήτηση: <a href="https://www.facebook.com/AthensAirportBus" rel="publisher">Facebook</a></p>
</div><div class="clearbreak"></div></div></div></div>
</div>
<div class="span6" id="jsn-pos-bottom">
<div class=" jsn-modulecontainer"><div class="jsn-modulecontainer_inner"><div class="jsn-modulecontent">
<div class="jsn-mod-custom">
<p>Ο ιστότοπος δεν είναι το επίσημο website του ΟΑΣΑ.<br/>Παρόλα αυτά προσπαθούμε να ανανεώνουμε το περιεχόμενο όσο πιο συχνά γίνεται.</p></div><div class="clearbreak"></div></div></div></div>
</div>
<div class="clearbreak"></div>
</div>
</div>
</div>
<a href="https://www.athensairportbus.com/#top" id="jsn-gotoplink">
<span>Κύλιση στην Αρχή</span>
</a>
</body>
</html>
