<!DOCTYPE html>
<html>
<head>
<title>Allegiance Engage Platform</title>
<script src="//static.allegiancetech.com/Shared/myfonts.js" type="text/javascript"></script>
<link href="//static.allegiancetech.com/Shared/Allegiance.Styles.Universal.css" rel="stylesheet" type="text/css"/>
<link href="//static.allegiancetech.com/Login/styles/engage8.login.css" rel="stylesheet" type="text/css"/>
<link href="//static.allegiancetech.com/Shared/Images/favicon.ico" rel="icon" type="image/vnd.microsoft.icon"/>
<link href="//static.allegiancetech.com/Shared/Images/favicon.ico" rel="SHORTCUT ICON"/>
<!--[if lt IE 9]>
    <script>
    //IE8 shim for semi-responsive design. Will only work on page-load, not window resize.
    if (document.documentElement.clientHeight < 630) { 
        document.write('<style>#newsPane{display:none;}#loginPane{height:340px;}#loginPaneWrapper{margin-top:-170px;}</style>');
    }
    </script>
    <![endif]-->
<script language="javascript" src="/v7/Includes/Js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script language="javascript" src="/v7/Includes/Js/jquery-migrate-1.4.1.min.js" type="text/javascript"></script>
<script language="javascript" src="/v7/Includes/Js/common.js" type="text/javascript"></script>
<script language="javascript" src="/v7/Includes/Js/warning.js" type="text/javascript"></script>
<script type="text/javascript">
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g, "");
    }
    $(document).ready(function () {
        var NUM_OF_IMAGES = 8;
        var bkgrnd = Math.floor((Math.random() * NUM_OF_IMAGES) + 1);
        $('body').css('background-image', 'url(//static.allegiancetech.com/Login/images/bg' + bkgrnd + '.jpg)');
    });

        function rotateBanners(elem) {
            var active = $(elem + " img.active");
            var next = $(elem + " img").not(".active");
            var nextImage = shuffle(next).first();
            if (nextImage.length == 0)
                nextImage = $(elem + " img:first");
            active.removeClass("active").fadeOut(200);
            nextImage.addClass("active").fadeIn(200);
        }

        function shuffle(imageArray) {
            for (var i = imageArray.length; i > 0; i--) {
                var j = Math.floor(Math.random() * i);
                var temp = imageArray[i - 1];
                imageArray[i - 1] = imageArray[j];
                imageArray[j] = temp;
            }
            return imageArray;
        }

        function prepareRotator(elem) {
            $(elem + " img").fadeOut(0);
            $(elem + " img:first").fadeIn(0).addClass("active");
        }

        function startRotator(elem) {
            prepareRotator(elem);
            setInterval("rotateBanners('" + elem + "')", 10000);
        }

        $(window).load(function () {
            startRotator("#rotator");
        });


        $(window).load(function () {
            $("a").click(function () {
                top.window.location.href = $(this).attr("href")
                return true;
            })
        });
    </script>
<style>
        #rotator img { position: absolute; }

        .whatsnew {
        background-color: transparent;
        margin: 0px;
        }

    </style>
</head>
<body>
<div id="loginPaneWrapper">
<div id="loginPane">
<div id="header">
</div>
<div id="formWrapper">
<form action="./login.aspx" id="frmLogin" method="post">
<input id="__VIEWSTATE" name="__VIEWSTATE" type="hidden" value="/wEPDwUKMjAzNTUxMTMxOA8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAgIBD2QWAgIBD2QWDmYPFgIeBFRleHQFB1NpZ24gSW5kAgEPD2QWAh4LcGxhY2Vob2xkZXIFCFVzZXJuYW1lZAICDw9kFgIfAgUIUGFzc3dvcmRkAgUPDxYCHwEFB1NpZ24gSW5kZAIKDw9kFgIfAgUIVXNlcm5hbWVkAgsPFgIeCWlubmVyaHRtbAUFUmVzZXRkAg0PDxYCHwEFEVJldHVybiB0byBTaWduIEluZGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFHmN0bDAwJGNwaENvbnRlbnQkY2hrUmVtZW1iZXJNZca7NpsrlAh88hEINEGeiCwjIxlO"/>
<input id="__VIEWSTATEGENERATOR" name="__VIEWSTATEGENERATOR" type="hidden" value="AB8345D7"/>
<input id="__EVENTVALIDATION" name="__EVENTVALIDATION" type="hidden" value="/wEdAAtCmm+QIOQsO1B32swXjoGzyLMKh8ND3VtXZbCnde7BZY0PdAtYVR++5HhigaQCBdU/+gXPX82vRv3MKitnqwpt+f+JR+tPcmfya6yR7MnVOTySqZXMt03aRAYBgr8Rn3vYTOA7tzceww0vkDc1kO+GRmwoFClKZkLSTMTNFhW8RGB/w1RyhaW0ZXnIVriC5r59uf+iTljxuztiLFTir83MT1NnPic7iSLCdHf9c/cXm5/ue7tIRiq87wda3ZWIaNgdeHV/"/>
<div id="SignIn">
<h2>Sign In</h2>
<input autocomplete="off" class="formField" id="cphContent_txtUserName" maxlength="100" name="ctl00$cphContent$txtUserName" placeholder="Username" type="text"/>
<input autocomplete="off" class="formField" id="cphContent_txtPassword" maxlength="256" name="ctl00$cphContent$txtPassword" placeholder="Password" type="password"/>
<label><input id="cphContent_chkRememberMe" name="ctl00$cphContent$chkRememberMe" type="checkbox"/><span id="cphContent_ltRememberMeText">Remember User</span></label>
<input class="primaryBtn" id="cphContent_cbtnSubmit" name="ctl00$cphContent$cbtnSubmit" onclick="if(!LoginValidate()) return false;" type="submit" value="Sign In"/>
<a href="#" id="cphContent_aForgotPassword" onclick="ForgotPassword(true)">Forgot Your Password?</a>
<a class="padding6" href="#" id="cphContent_aPrivacyPolicy" onclick="OpenInNewTab(); return false;">
             View Privacy</a>
</div>
<div id="ForgotPassword">
<h2>Forgot Password?</h2>
<input class="formField" id="cphContent_txtForgotPass" maxlength="100" name="ctl00$cphContent$txtForgotPass" onkeypress="return resetPasswordOnEnter(event);" placeholder="Username" type="text"/>
<button class="primaryBtn" id="cphContent_btnResetPassword" onclick="if(!resetThePassword())return false; __doPostBack('ctl00$cphContent$btnResetPassword','')" type="button">Reset</button>
<p>Or, <a class="login" href="https://maritzcx.atlassian.net/servicedesk/customer/portal/1" target="_blank">Contact us</a> for help resetting your password.</p>
<p><a href="javascript:void(0)" id="cphContent_lnkReturnToSign" onclick="SignIn(true)">Return to Sign In</a></p>
</div>
<script src="/v7/App/Libs/Zone/zone.min.js" type="text/javascript"></script>
<script type="text/javascript">
        function ForgotPassword(clear) {
            $('#ForgotPassword').show();
            $('#SignIn').hide();

            if (document.getElementById('cphContent_txtForgotPass') != null)
            {
                document.getElementById('cphContent_hdnUserName').value= '';
                document.getElementById('cphContent_txtForgotPass').value= '';
                document.getElementById('cphContent_txtForgotPass').focus();
             }

            if (clear) 
                ClearMessages();
        }
        function OpenInNewTab() {
            var url = 'https://static.allegiancetech.com/support/privacy.html';
            var win = window.open(url, '_blank');
            win.focus();
        }
        function SignIn(clear) {
            $('#ForgotPassword').hide();
            $('#SignIn').show();

            if (document.getElementById('cphContent_txtUserName') != null)
                document.getElementById('cphContent_txtUserName').focus();

            if (clear)
                ClearMessages();
        }

        function ClearMessages() {
            $('#resetMessage').hide();
            $('#invalidLogin').hide();
        }
        function LoginValidate() {

            var errors = '';
            if (document.getElementById('cphContent_txtUserName').value.trim() == '') {
                errors += document.getElementById('cphContent_hdnEnterUserNameLogin').value.trim() + '\r\n';
                document.getElementById('cphContent_txtUserName').focus();
                $('#cphContent_txtUserName').addClass('error');
            }
            if (document.getElementById('cphContent_txtPassword').value.trim() == '') {
                errors += document.getElementById('cphContent_hdnEnterPasswordLogin').value.trim() + '\r\n';
                document.getElementById('cphContent_txtPassword').focus();
                $('#cphContent_txtPassword').addClass('error');
            }

            if (errors) {
                alert(errors);
                return false;
            }            
            
            return true;
        }
		
		function getCustomLogin(company) {
			var url = window.location.origin.replace('www', 'opencanvasclientapi') + '/api/v1/Login/' + company + '/Login';
			$('body').css('background-size', '0 0');
			$.ajax({
				url: url,
				async: false,
				success: function(data) {
					if (data.script) {
						const scriptElement = document.createElement('script');
						scriptElement.id = 'custom-login';
						scriptElement.type = 'text/javascript';
						scriptElement.text = data.script;
                        document.body.appendChild(scriptElement);
                        createWidget(scriptElement.id);
					} else {
						$('#loginPaneWrapper').show();
						$('body').css('background-size', 'cover');
					}
				},
                error: function (err) {
					$('#loginPaneWrapper').show();
					$('body').css('background-size', 'cover');
				}
			});
		}
		
		function createWidget(elementID) {
			var widget = document.createElement(elementID);
			widget.setAttribute('remember', $('#cphContent_chkRememberMe').prop('checked'));
			widget.addEventListener('login', handleLogin);
			document.body.append(widget);
		}
		
		function handleLogin(event) {
            $('#cphContent_txtUserName').val(event.detail.username);
            $('#cphContent_txtPassword').val(event.detail.password);
			$('#cphContent_chkRememberMe').prop('checked', event.detail.remember);
            $('#cphContent_cbtnSubmit').click();
		}
		
		function getUrlParameter(name) {
			name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
			var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
			var results = regex.exec(location.search);
			return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
		};

        $(document).ready(function () {
			var company = getUrlParameter('Company') || localStorage.Company;
			if (company) {
				localStorage.Company = company;
				getCustomLogin(company);
			}
			else {
				$('#loginPaneWrapper').show();
			}
			
            $('input').blur(function () {
                if (this.value) {
                    $(this).removeClass('error');
                }
                else {
                    $(this).addClass('error');
                }
            });
                
            SignIn();
            
        });

        function resetThePassword() {
            if (document.getElementById('cphContent_txtForgotPass').value.trim() == '' || document.getElementById('cphContent_txtForgotPass').value.trim() == document.getElementById('cphContent_hdnUserName').value.trim()) {
                alert(document.getElementById('cphContent_hdnPleaseEnterUserName').value.trim());
                return false;
            }
            else {
                return true;
            }
        }

        function resetPasswordOnEnter(e) {
            if (e.keyCode == 13) {
                document.getElementById('cphContent_btnResetPassword').click();
                return false;
            }
            return true;
        }
		
		(function (arr) {
		  arr.forEach(function (item) {
			if (item.hasOwnProperty('append')) {
			  return;
			}
			Object.defineProperty(item, 'append', {
			  configurable: true,
			  enumerable: true,
			  writable: true,
			  value: function append() {
				var argArr = Array.prototype.slice.call(arguments),
				  docFrag = document.createDocumentFragment();
				
				argArr.forEach(function (argItem) {
				  var isNode = argItem instanceof Node;
				  docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
				});
				
				this.appendChild(docFrag);
			  }
			});
		  });
		})([Element.prototype, Document.prototype, DocumentFragment.prototype]);
		
    </script>
<style type="text/css">
            .padding6{
                display: block;
                padding-top: 6Px;
            }
			
			#loginPaneWrapper {
				display: none;
			}
     </style>
<input id="cphContent_hdnUserName" name="ctl00$cphContent$hdnUserName" type="hidden"/>
<input id="cphContent_hdnPleaseEnterUserName" name="ctl00$cphContent$hdnPleaseEnterUserName" type="hidden" value="Please enter Username"/>
<input id="cphContent_hdnEnterUserNameLogin" name="ctl00$cphContent$hdnEnterUserNameLogin" type="hidden" value="Please enter Username"/>
<input id="cphContent_hdnEnterPasswordLogin" name="ctl00$cphContent$hdnEnterPasswordLogin" type="hidden" value="Please enter a valid password."/>
<script type="text/javascript">
//<![CDATA[
appRootGlobal = '/v7/'; imageUrlGlobal = '/v7/Includes/Images/'; //]]>
</script>
</form>
</div>
<div id="newsPane">
<div class="whatsnew" style="width:333px;height:254px;margin-top:10px; text-align:left">
<div class="main" style="width:333px;height:0px">
<div id="rotator" style="position: relative; top: 8px; bottom: -8px; left: 8px; right: -8px; width: 333px; height: 0px">
<a href="https://www.maritzcx.com/cx-governance/" rel="noopener">
<img alt="" class="alignnone wp-image-2940 size-full" height="236" src="../../v7/App/Images/WhatsNew/Break_the_Insight_Barrior.png" style="display: none;" width="315"/>
</a>
<a href="https://www.maritzcx.com/what-we-do/text-analytics/" rel="noopener">
<img alt="" class="alignnone wp-image-2907 size-full" height="236" src="../../v7/App/Images/WhatsNew/Conquer_Data_Mountain.png" style="display: none;" width="315"/>
</a>
<a href="https://www.maritzcx.com/cxevolution/" rel="noopener">
<img alt="" class="alignnone wp-image-2943 size-full" height="236" src="../../v7/App/Images/WhatsNew/Rev_Up_Your_ROI.png" style="display: none;" width="315"/>
</a>
<a href="https://www.maritzcx.com/lp/unlock-the-value-of-cx/" rel="noopener">
<img alt="" class="alignnone wp-image-2907 size-full" height="236" src="../../v7/App/Images/WhatsNew/Value_of_CX.png" style="display: none;" width="315"/>
</a>
</div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
