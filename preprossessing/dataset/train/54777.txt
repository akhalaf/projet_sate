<!DOCTYPE html>
<!--[if lt IE 9]><html class="no-js lt-ie9" lang="en" dir="ltr"><![endif]--><!--[if gt IE 8]><!--><html class="no-js" dir="ltr" lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<!-- Web Experience Toolkit (WET) / Boîte à outils de l'expérience Web (BOEW)
		wet-boew.github.io/wet-boew/License-en.html / wet-boew.github.io/wet-boew/Licence-fr.html -->
<title>We couldn't find that Web page (Error 404) - Government of Canada Intranet theme / Nous ne pouvons trouver cette page Web (Erreur 404) - Thème du gouvernement du Canada pour les sites intranet</title>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<!-- Meta data -->
<meta content="noindex, nofollow, noarchive" name="robots"/>
<!-- Meta data-->
<!--[if gte IE 9 | !IE ]><!-->
<link href="/DAM/PresentationDAM/STAGING/WET/WET4.0.27-CANADA.CA/assets/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="/DAM/PresentationDAM/STAGING/WET/WET4.0.27-CANADA.CA/css/wet-boew.min.css" rel="stylesheet"/>
<!--<![endif]-->
<link href="/DAM/PresentationDAM/STAGING/WET/WET4.0.27-CANADA.CA/css/theme-srv.min.css" rel="stylesheet"/>
<!--[if lt IE 9]>
<link href="/DAM/PresentationDAM/STAGING/WET/WET4.0.27-CANADA.CA/assets/favicon.ico" rel="shortcut icon" />
<link rel="stylesheet" href="/DAM/PresentationDAM/STAGING/WET/WET4.0.27-CANADA.CA/css/ie8-wet-boew.min.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="/DAM/PresentationDAM/STAGING/WET/WET4.0.27-CANADA.CA/js/ie8-wet-boew.min.js"></script>
<![endif]-->
<noscript><link href="/DAM/PresentationDAM/STAGING/WET/WET4.0.27-CANADA.CA/css/noscript.min.css" rel="stylesheet"/></noscript>
</head>
<body typeof="WebPage" vocab="http://schema.org/">
<header id="wb-bnr" role="banner">
<div class="container">
<div class="row">
<div class="col-sm-6">
<object aria-label="Government of Canada" data="/DAM/PresentationDAM/STAGING/WET/WET4.0.27-CANADA.CA/assets/sig-blk-en.svg" id="gcwu-sig" role="img" tabindex="-1" type="image/svg+xml"></object>
</div>
<div class="col-sm-6">
<object aria-label="Symbol of the Government of Canada" data="/DAM/PresentationDAM/STAGING/WET/WET4.0.27-CANADA.CA/assets/wmms-intra.svg" id="wmms" role="img" tabindex="-1" type="image/svg+xml"></object>
</div>
</div>
</div>
</header>
<main class="container" property="mainContentOfPage" role="main">
<div class="row mrgn-tp-lg">
<h1 class="wb-inv">We couldn't find that Web page (Error 404) / <span lang="fr">Nous ne pouvons trouver cette page Web (Erreur 404)</span></h1>
<section class="col-md-6">
<h2><span class="glyphicon glyphicon-warning-sign mrgn-rght-md"></span> We couldn't find that Web page (Error 404)</h2>
<p>We're sorry you ended up here. Sometimes a page gets moved or deleted, but hopefully we can help you find what you're looking for.</p>
<ul>
<li>Return to the <a href="/eng/1100100010002/1100100010021">home page</a></li>
</ul>
</section>
<section class="col-md-6" lang="fr">
<h2><span class="glyphicon glyphicon-warning-sign mrgn-rght-md"></span> Nous ne pouvons trouver cette page Web (Erreur 404)</h2>
<p>Nous sommes désolés que vous ayez abouti ici. Il arrive parfois qu'une page ait été déplacée ou supprimée. Heureusement, nous pouvons vous aider à trouver ce que vous cherchez.</p>
<ul>
<li>Retournez à la <a href="/fra/1100100010002/1100100010021">page d'accueil</a></li>
</ul>
</section>
</div>
</main>
<!--[if gte IE 9 | !IE ]><!-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="/DAM/PresentationDAM/STAGING/WET/WET4.0.27-CANADA.CA/js/wet-boew.min.js"></script>
<!--<![endif]-->
<!--[if lt IE 9]>
<script src="/DAM/PresentationDAM/STAGING/WET/WET4.0.27-CANADA.CA/js/ie8-wet-boew2.min.js"></script>
<![endif]-->
<script src="/DAM/PresentationDAM/STAGING/WET/WET4.0.27-CANADA.CA/js/theme.min.js"></script>
</body>
</html>