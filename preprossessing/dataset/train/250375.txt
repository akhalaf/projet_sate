<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>BooksList.me</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="BooksList.me - отслеживание прод с СамИздата samlib.ru, упорядочивание прочитанного при помощи меток, рекомендаций и коллекций" name="description"/>
<meta content="https://bookslist.me/images/logo-withbg.png" property="og:image"/>
<link href="/images/favicon.ico" rel="shortcut icon" type="image/ico"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="/images/apple-touch-icon.png" rel="apple-touch-icon"/>
<link href="/images/apple-touch-icon-precomposed.png" rel="apple-touch-icon-precomposed"/>
<link href="/inform.css?35" rel="stylesheet" type="text/css"/>
<script src="https://yandex.st/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="https://yandex.st/jquery/form/2.83/jquery.form.min.js" type="text/javascript"></script>
<script src="/jquery_php/inform.js?35" type="text/javascript"></script>
<meta content="6d01a0abeaf9cb4d" name="yandex-verification"/>
</head>
<body>
<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter38061875 = new Ya.Metrika({ id:38061875, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img alt="" src="https://mc.yandex.ru/watch/38061875" style="position:absolute; left:-9999px;"/></div></noscript> <!-- /Yandex.Metrika counter -->
<div class="leftbar-wrap">
<div href="#0" id="scroll-back">
<span class="active-area">
<span class="bar-desc"> ↓</span>
</span>
</div>
<div class="left-controlbar" href="#">
<span class="active-area">
<span class="bar-desc"> ↑</span>
</span>
</div>
</div>
<div id="site-content">
<div id="site-content-left">
<div class="logo logo1">
<a href="/" title="Главная страница"><img alt="Главная страница" height="30" src="/images/logo.png" width="160"/></a>
</div>
<ul class="menu">
<li><a href="/news">Новости</a></li>
<li><a href="/recommends">Рекомендации</a></li>
<li><a href="/collections">Коллекции</a></li>
<li><a href="/putewoditel">Путеводитель</a></li>
<li><a href="/index.php?section=search&amp;subject=user">Авторы</a></li>
<li><a href="/index.php?section=find">Поиск</a></li>
<li><a href="/index.php?section=project">О проекте</a></li>
</ul>
</div>
<div id="site-content-center">
<div id="header">
<div id="sidebar-toggle" onclick="$('#site-content-left').toggleClass('show');">
<span class="bar"></span>
<span class="bar"></span>
<span class="bar"></span>
</div>
<div class="logo logo2">
<a href="/" title="Главная страница"><img alt="Главная страница" height="30" src="/images/logo.png" width="160"/></a>
</div>
<input class="header-search" id="header-search" placeholder="Поиск книг/авторов" type="text"/>
<div class="header-search-answ" id="header-search-answ">
<ul>
<li data-url="/index.php?section=search&amp;subject=fic"><a href="/index.php?section=search&amp;subject=fic"><div class="link-big">Искать произведение »</div></a></li>
<li data-url="/index.php?section=search&amp;subject=user"><a href="/index.php?section=search&amp;subject=user"><div class="link-big">Искать автора »</div></a></li>
</ul>
</div>
<div id="sidebar-toggle2" onclick="$('#site-content-right').toggleClass('show');"><img src="images/noavatar_128.png"/></div>
<div id="sidebar-toggle3" onclick="$('#header').toggleClass('show-search'); $('#header-search').toggleClass('show');"><img src="/images/search_32.png"/></div>
</div>
<div style="float:left; padding:3px;"><a href="#about">Зачем нужен этот сайт?</a></div><br/><br/>
<form id="FicToFile" onsubmit="pftake('/section_fictofile_post.php?action=load', 'FicToFile', 'FicToFileButton', '&lt;img class=inline3 src=/images/load_2.gif&gt; &lt;span class=small_light&gt;Пожалуйста, подождите&lt;/span&gt;'); return false;">
<div class="ContentTable">
<h1>Вставьте ссылку на страницу произведения на СИ</h1>
<div class="center"><input id="FicUrl" name="FicUrl" placeholder="Например: http://samlib.ru/a/avtor/kniga.shtml" type="text" value=""/></div>
<div class="center" id="FicToFileButton">
<input class="modern_button" type="submit" value="Хочу скачать это произведение"/>
</div>
</div>
</form>
<div class="ContentTable" id="FicToFileResult"></div>
<br/><br/>
<div class="ContentTable MainBlock">
<div class="MainBlockLeft"><script type="text/javascript"><!--
google_ad_client = "ca-pub-6359265769479039";
    google_ad_slot = "9368357791";
    google_ad_width = 336;
    google_ad_height = 280;
var width = window.innerWidth;
	if(!width) {
		if(document.documentElement) {
			width = document.documentElement.clientWidth;
		}
		width = Math.min(width, document.body.clientWidth);
	}
	if(!width) {
		width = screen.width;
	}

	if (width < 336) {
		google_ad_slot = "3362691447";
		google_ad_width = 300;
		google_ad_height = 250;
	}
//-->
</script>
<script src="//pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript">
</script></div>
<div class="MainBlockRight tleft on630hide">
<div class="InfoContainer">
<h2>Сделайте себе закладку в браузере</h2>
                Просто перетащите расположенную ниже ссылку на панель закладок в браузере. Теперь, находясь на странице произведения на СИ (samlib.ru), вам достаточно будет нажать на эту закладку, и вы попадёте на соответствующую страницу нашего сайта (чтобы подписаться и следить за продами или поставить метку, написать заметку/рекомендацию, добавить в коллекцию, скачать fb2/ePub).
                <div class="center">
<br/>
<a href="javascript:(function()%7Bvar%20ficurl=window.location.href;var%20ftfurl=%22http://bookslist.me/?url=%22+ficurl;window.open(ftfurl,%22_blank%22);%7D)()">Открыть в BooksList</a>
<br/><br/>
</div>
</div>
</div>
<div class="clear"></div>
</div><br/><br/><table class="PropertiesMenu">
<tr>
<td class="sp">
<div class="activ"><a>Жанры</a></div>
<div class="right"><a href="/index.php?section=find">Поиск по параметрам »</a></div>
</td>
</tr>
</table>
<br/>
<div class="ContentTable GenresMain">
<div><a href="/index.php?section=find&amp;genre=1">Фантастика</a> (12474)<br/><a href="/index.php?section=find&amp;genre=2">Детектив</a> (638)<br/><a href="/index.php?section=find&amp;genre=5">Проза</a> (2567)<br/><a href="/index.php?section=find&amp;genre=7">Эротика</a> (985)<br/><a href="/index.php?section=find&amp;genre=8">Юмор</a> (2090)<br/></div><div><a href="/index.php?section=find&amp;genre=10">Переводы</a> (886)<br/><a href="/index.php?section=find&amp;genre=12">Любовный роман</a> (3174)<br/><a href="/index.php?section=find&amp;genre=16">Сказки</a> (413)<br/><a href="/index.php?section=find&amp;genre=17">Мистика</a> (1003)<br/><a href="/index.php?section=find&amp;genre=22">Киберпанк</a> (744)<br/></div><div><a href="/index.php?section=find&amp;genre=23">Литобзор</a> (650)<br/><a href="/index.php?section=find&amp;genre=24">Фэнтези</a> (15402)<br/><a href="/index.php?section=find&amp;genre=25">Приключения</a> (4395)<br/><a href="/index.php?section=find&amp;genre=26">История</a> (1148)<br/><a href="/index.php?section=find&amp;genre=30">Хоррор</a> (388)<br/></div><div><a href="/index.php?section=find&amp;genre=31">Пародии</a> (274)<br/><a href="/index.php?section=find&amp;genre=35">Фанфик</a> (4001)<br/></div><div class="clear"></div></div><br/><br/><table class="PropertiesMenu">
<tr>
<td class="sp">
<div class="activ"><a href="/putewoditel">Путеводитель самиздата</a></div>
</td>
</tr>
</table>
<br/>
<div class="ContentTable"><div class="PW-title" data-list-id="1"><b>Иные расы и виды существ</b> 11 списков</div>
<div class="PW-content" id="PW-list1"><a href="/collection8578">Ангелы</a> <span class="small_light">(Произведений: 91)</span><br/><a href="/collection8579">Оборотни</a> <span class="small_light">(Произведений: 181)</span><br/><a href="/collection8580">Орки, гоблины, гномы, назгулы, тролли</a> <span class="small_light">(Произведений: 41)</span><br/><a href="/collection8585">Эльфы, эльфы-полукровки, дроу</a> <span class="small_light">(Произведений: 230)</span><br/><a href="/collection8586">Привидения, призраки, полтергейсты, духи</a> <span class="small_light">(Произведений: 74)</span><br/><a href="/collection8587">Боги, полубоги, божественные сущности</a> <span class="small_light">(Произведений: 165)</span><br/><a href="/collection8588">Вампиры</a> <span class="small_light">(Произведений: 241)</span><br/><a href="/collection8645">Демоны</a> <span class="small_light">(Произведений: 265)</span><br/><a href="/collection8646">Драконы</a> <span class="small_light">(Произведений: 164)</span><br/><a href="/collection8647">Особенная раса, вид (созданные автором)</a> <span class="small_light">(Произведений: 122)</span><br/><a href="/collection8648">Редкие расы (но не авторские)</a> <span class="small_light">(Произведений: 107)</span><br/></div><div class="PW-title" data-list-id="2"><b>Профессии, занятия, стили жизни</b> 8 списков</div>
<div class="PW-content" id="PW-list2"><a href="/collection8660">Маг, колдун, ведьма, волшебник</a> <span class="small_light">(Произведений: 564)</span><br/><a href="/collection8661">Некромант, чернокнижник, темный маг</a> <span class="small_light">(Произведений: 89)</span><br/><a href="/collection8662">Целитель, лекарь, врач, священник</a> <span class="small_light">(Произведений: 79)</span><br/><a href="/collection8663">Вор, мошенник, убийца, пират</a> <span class="small_light">(Произведений: 113)</span><br/><a href="/collection8664">Охранник, телохранитель, судья, сыщик, стражник, палач</a> <span class="small_light">(Произведений: 65)</span><br/><a href="/collection8665">Ученик, студент, подмастерье</a> <span class="small_light">(Произведений: 209)</span><br/><a href="/collection8666">Герой и его профессия</a> <span class="small_light">(Произведений: 206)</span><br/><a href="/collection8667">Творческая личность: музыкант, художник, скульптор</a> <span class="small_light">(Произведений: 50)</span><br/></div><div class="PW-title" data-list-id="3"><b>Внутренний мир человека. Мысли и жизнь</b> 4 списка</div>
<div class="PW-content" id="PW-list3"><a href="/collection8682">Жизнь и смерть. Ритуалы</a> <span class="small_light">(Произведений: 122)</span><br/><a href="/collection8683">Реализм. Психологическая проза. Повседневность</a> <span class="small_light">(Произведений: 143)</span><br/><a href="/collection8684">Сон и реальность</a> <span class="small_light">(Произведений: 93)</span><br/><a href="/collection8685">Философские и социальные произведения</a> <span class="small_light">(Произведений: 166)</span><br/></div><div class="PW-title" data-list-id="4"><b>Миры фэнтези и фантастики: каноны, апокрифы, смешение жанров</b> 7 списков</div>
<div class="PW-content" id="PW-list4"><a href="/collection8686">Артефакт, важная находка</a> <span class="small_light">(Произведений: 152)</span><br/><a href="/collection8687">Классические миры фэнтези</a> <span class="small_light">(Произведений: 342)</span><br/><a href="/collection8688">Люди и технология</a> <span class="small_light">(Произведений: 117)</span><br/><a href="/collection8689">Произведения по мотивам, фанфики и самостоятельные авторские пародии</a> <span class="small_light">(Произведений: 161)</span><br/><a href="/collection8690">Проклятья и пророчества</a> <span class="small_light">(Произведений: 187)</span><br/><a href="/collection8691">Фантастика - классическая, научная, современная</a> <span class="small_light">(Произведений: 252)</span><br/><a href="/collection8692">Фантастико-фэнтезийные истории &amp; Техномагия</a> <span class="small_light">(Произведений: 201)</span><br/></div><div class="PW-title" data-list-id="5"><b>О взаимоотношениях</b> 7 списков</div>
<div class="PW-content" id="PW-list5"><a href="/collection8725">Истории со свадьбой, женитьбой, помолвкой</a> <span class="small_light">(Произведений: 178)</span><br/><a href="/collection8726">Личные межрасовые отношения (фэнтези и фантастика)</a> <span class="small_light">(Произведений: 382)</span><br/><a href="/collection8727">Личные отношения людей (фэнтези и фантастика)</a> <span class="small_light">(Произведений: 418)</span><br/><a href="/collection8729">Любовный роман (реалистика)</a> <span class="small_light">(Произведений: 268)</span><br/><a href="/collection8730">Любовный роман в стиле фантастики и фэнтези</a> <span class="small_light">(Произведений: 517)</span><br/><a href="/collection8731">Откровенное чтиво 18+</a> <span class="small_light">(Произведений: 114)</span><br/><a href="/collection8732">Подростковая тематика. Поиск себя</a> <span class="small_light">(Произведений: 186)</span><br/></div><div class="PW-title" data-list-id="6"><b>Герои</b> 13 списков</div>
<div class="PW-content" id="PW-list6"><a href="/collection8863">Правитель или представитель аристократического сословия</a> <span class="small_light">(Произведений: 320)</span><br/><a href="/collection8864">Сильная, нахальная и боевая личность</a> <span class="small_light">(Произведений: 225)</span><br/><a href="/collection8865">Слабая, невезучая, нуждающаяся в защите личность</a> <span class="small_light">(Произведений: 67)</span><br/><a href="/collection8866">Учреждениях по работе с магией, нечистью, аномальными явлениями</a> <span class="small_light">(Произведений: 84)</span><br/><a href="/collection8867">Потеря памяти</a> <span class="small_light">(Произведений: 83)</span><br/><a href="/collection8868">Необыкновенная личность, наделенная сверхспособностями</a> <span class="small_light">(Произведений: 390)</span><br/><a href="/collection8869">Родственники</a> <span class="small_light">(Произведений: 87)</span><br/><a href="/collection8870">Избранник, мессия, спаситель</a> <span class="small_light">(Произведений: 123)</span><br/><a href="/collection8871">Команда персонажей</a> <span class="small_light">(Произведений: 438)</span><br/><a href="/collection8872">Обыкновенный герой</a> <span class="small_light">(Произведений: 270)</span><br/><a href="/collection8873">Перерождение, изменение тела</a> <span class="small_light">(Произведений: 270)</span><br/><a href="/collection3119">Подкидыш, найденыш, приемыш</a> <span class="small_light">(Произведений: 63)</span><br/><a href="/collection8874">Темный Властелин</a> <span class="small_light">(Произведений: 93)</span><br/></div><div class="PW-title" data-list-id="7"><b>Земля</b> 6 списков</div>
<div class="PW-content" id="PW-list7"><a href="/collection3129">Альтернативная история</a> <span class="small_light">(Произведений: 213)</span><br/><a href="/collection8875">Аномальные зоны</a> <span class="small_light">(Произведений: 73)</span><br/><a href="/collection8876">Городские истории</a> <span class="small_light">(Произведений: 306)</span><br/><a href="/collection8877">Исторические фантазии</a> <span class="small_light">(Произведений: 98)</span><br/><a href="/collection8878">Постапокалиптика</a> <span class="small_light">(Произведений: 104)</span><br/><a href="/collection8879">Стилизации и этнические мотивы</a> <span class="small_light">(Произведений: 130)</span><br/></div><div class="PW-title" data-list-id="8"><b>Попадалово</b> 5 списков</div>
<div class="PW-content" id="PW-list8"><a href="/collection8880">Путешественник по нескольким мирам</a> <span class="small_light">(Произведений: 112)</span><br/><a href="/collection8881">Из иного мира в наш, или какой-либо другой</a> <span class="small_light">(Произведений: 99)</span><br/><a href="/collection8882">Наши в другом мире</a> <span class="small_light">(Произведений: 564)</span><br/><a href="/collection8883">Наши в космосе/на другой планете</a> <span class="small_light">(Произведений: 79)</span><br/><a href="/collection8884">Путешествие во времени</a> <span class="small_light">(Произведений: 128)</span><br/></div><div class="PW-title" data-list-id="9"><b>Противостояние</b> 9 списков</div>
<div class="PW-content" id="PW-list9"><a href="/collection8930">Азартные игры, мошенничество</a> <span class="small_light">(Произведений: 30)</span><br/><a href="/collection8931">Боевая Фиф. Эпоха автоматов</a> <span class="small_light">(Произведений: 47)</span><br/><a href="/collection8932">Боевая Фиф. Эпоха космолётов</a> <span class="small_light">(Произведений: 91)</span><br/><a href="/collection8933">Боевая Фиф. Эпоха меча</a> <span class="small_light">(Произведений: 119)</span><br/><a href="/collection8934">Выживание и прогрессорство</a> <span class="small_light">(Произведений: 162)</span><br/><a href="/collection8935">Глобальные войны, революции, катастрофы</a> <span class="small_light">(Произведений: 178)</span><br/><a href="/collection8936">Детектив в стиле Фиф и реалистики</a> <span class="small_light">(Произведений: 115)</span><br/><a href="/collection8937">Криминальный роман, повесть, рассказ</a> <span class="small_light">(Произведений: 53)</span><br/><a href="/collection8939">Темные &amp; Светлые</a> <span class="small_light">(Произведений: 181)</span><br/></div><div class="PW-title" data-list-id="10"><b>О чувствах</b> 3 списка</div>
<div class="PW-content" id="PW-list10"><a href="/collection8940">Грустные истории (драма и трагедия)</a> <span class="small_light">(Произведений: 143)</span><br/><a href="/collection8941">Смешные истории (юмористические)</a> <span class="small_light">(Произведений: 288)</span><br/><a href="/collection8942">Страшные истории (хоррор, ужасы)</a> <span class="small_light">(Произведений: 104)</span><br/></div><div class="PW-title PW-selected" data-list-id="11"><b>Следующее поколение</b> 4 списка</div>
<div class="PW-content PW-selected" id="PW-list11"><a href="/collection8944">Детское фэнтези</a> <span class="small_light">(Произведений: 39)</span><br/><a href="/collection8945">Для самых маленьких</a> <span class="small_light">(Произведений: 34)</span><br/><a href="/collection8946">О животных</a> <span class="small_light">(Произведений: 48)</span><br/><a href="/collection8947">Поучительные сказки, притчи</a> <span class="small_light">(Произведений: 82)</span><br/></div></div><div class="ContentTable MainBlock"><div class="MainBlockLeft">
<h1>Новые рекомендации</h1><a href="/book161690">Хозяйка гор.   глава 15</a> автора <a href="/author3953">Каламацкая Елена</a>
<div class="rec_on_main"><a class="user" href="/user433509">rybnat24</a>:
            не пожалейте потраченного времени! позитива от прочтения хватило на целую неделю! Пока не начала читать следующую...</div>
<a class="small_link light" href="/recommends?id=68624">11 Января в 21:18
            <br/><br/><br/><div class="clear"></div></a><a href="/book451769">Син'Дорай</a> автора <a href="/author6">Седрик</a>
<div class="rec_on_main"><a class="user" href="/user516860">Krown the King</a>:
            <span id="short_recommend_68347">Люблю Ваши книги) Хочу поблагодарить и поздравить с Новым Годом ! Я б хотела поблагодарить чуть более существенно , но... Я банкрот ) И "подушка безопасности". исчерпала себя ещё в начале июля и с тех<span class="normal_link" onclick="$('#short_recommend_68347').remove(); $('#full_recommend_68347').show();" title="Показать текст рекомендации полностью">...&gt;&gt;</span></span><span id="full_recommend_68347" style="display:none;">Люблю Ваши книги) Хочу поблагодарить и поздравить с Новым Годом ! Я б хотела поблагодарить чуть более существенно , но... Я банкрот ) И "подушка безопасности". исчерпала себя ещё в начале июля и с тех пор ничего особо не поменялось ) Просто хотела пожелать здоровья и музы ) И печенек )</span></div>
<a class="small_link light" href="/recommends?id=68347">1 Января в 01:58
            <br/><br/><br/><div class="clear"></div></a><a href="/book70720">Хорошо замаскированный подручный</a> автора <a href="/author278">Актер Тысячи Масок</a>
<div class="rec_on_main"><a class="user" href="/user175664">Диадан</a>:
            С одной стороны ничего особого, но затягивает.</div>
<a class="small_link light" href="/recommends?id=68004">14 Декабря 2020
            <br/><br/><br/><div class="clear"></div></a><a href="/book9800">Угли "Embers" (завершено)</a> автора <a href="/author417">Vathara</a>
<div class="rec_on_main"><a class="user" href="/user175664">Диадан</a>:
            Очень хороший фик получился.<br/>
Стоит потраченного времени.</div>
<a class="small_link light" href="/recommends?id=67930">10 Декабря 2020
            <br/><br/><br/><div class="clear"></div></a><a href="/book206488">Душа крючкотвора</a> автора <a href="/author645">Бубела Олег Николаевич</a>
<div class="rec_on_main"><a class="user" href="/user653319">AlexGard</a>:
            <span id="short_recommend_67717">Для многих Бубела Олег Николаевич, это знак качества. Отличный фанфик! Талант есть талант. Жаль, что концовка оборвана хоть и понятно, что там дальше будет.  
Напомнило старый мультик:  
- Брейн, а чт<span class="normal_link" onclick="$('#short_recommend_67717').remove(); $('#full_recommend_67717').show();" title="Показать текст рекомендации полностью">...&gt;&gt;</span></span><span id="full_recommend_67717" style="display:none;">Для многих Бубела Олег Николаевич, это знак качества. Отличный фанфик! Талант есть талант. Жаль, что концовка оборвана хоть и понятно, что там дальше будет. <br/>
Напомнило старый мультик: <br/>
- Брейн, а что мы будем делать дальше?<br/>
- О, это просто, Пинки. Мы захватим ВЕСЬ МИР!!!</span></div>
<a class="small_link light" href="/recommends?id=67717">4 Декабря 2020
            <br/><br/><br/><div class="clear"></div></a><a class="dotted_button" href="/recommends" style="margin-top:-20px;">Еще рекомендации »</a>
</div><div class="MainBlockRight">
<h1>Последние обновления</h1>
<span class="green">сегодня в 21:05:</span> <a class="user" href="/author3514">Керн Максим Александрович</a> обновил <a href="/book633746">Восьмой лист</a> 122k<span class="red">-1k</span><br/><br/><span class="green">сегодня в 21:05:</span> <a class="user" href="/author10">Гончарова Галина Дмитриевна</a> обновил <a href="/book633665">Времена года - обновление</a> 46k<span class="green">+4k</span><br/><br/><span class="green">сегодня в 21:05:</span> <a class="user" href="/author10">Гончарова Галина Дмитриевна</a> обновил <a href="/book631404">Осень бедствий</a> 192k<span class="green">+46k</span><br/><br/><span class="green">сегодня в 20:05:</span> <a class="user" href="/author822">Симонов Сергей</a> обновил <a href="/book15352">Цвет сверхдержавы - красный 2 Место под Солнцем</a> 2655k<span class="green">+1k</span><br/><br/><span class="green">сегодня в 19:37:</span> <a class="user" href="/author54532">Кротов Сергей Владимирович</a> обновил <a href="/book516393">Чаганов: Война</a> 414k<span class="green">+3k</span><br/><br/><span class="green">сегодня в 19:05:</span> <a class="user" href="/author62261">Белоус Олег</a> обновил <a href="/book513982">Когда на Земле стало тесно</a> 326k<span class="green">+4k</span><br/><br/><span class="green">сегодня в 18:55:</span> <a class="user" href="/author63287">Харламов Игорь Борисович</a> обновил <a href="/book498208">Битва за Эллиоты. Часть 5</a> 434k<span class="green">+10k</span><br/><br/><span class="green">сегодня в 18:05:</span> <a class="user" href="/author568">Белогорский Евгений Александрович</a> обновил <a href="/book633667">Сталинградская метель</a> 84k<span class="green">+12k</span><br/><br/><span class="green">сегодня в 18:05:</span> <a class="user" href="/author165034">Бородин Евгений Валериевич</a> обновил <a href="/book480030">Честный</a> 677k<span class="green">+31k</span><br/><br/><span class="green">сегодня в 15:05:</span> <a class="user" href="/author2002">Котяра Леопольд</a> обновил <a href="/book638372">Можно было и так!</a> 35k<span class="green">+1k</span><br/><br/><span class="green">сегодня в 15:05:</span> <a class="user" href="/author3514">Керн Максим Александрович</a> обновил <a href="/book633746">Восьмой лист</a> 122k<span class="green">+5k</span><br/><br/><span class="green">сегодня в 15:05:</span> <a class="user" href="/author62261">Белоус Олег</a> обновил <a href="/book501685">Волчонок с Паллады</a> 331k<span class="green">+5k</span><br/><br/><span class="green">сегодня в 14:05:</span> <a class="user" href="/author271">Oxygen</a> обновил <a href="/book305165">Квинт Лициний 4</a> 154k<span class="green">+13k</span><br/><br/><span class="green">сегодня в 11:05:</span> <a class="user" href="/author169299">Могила Михаил Владимирович</a> обновил <a href="/book622928">Попытаться  поймать за хвост Жар-Птицу-4</a> 440k<span class="green">+10k</span><br/><br/><span class="green">сегодня в 10:05:</span> <a class="user" href="/author82634">Бингс Г.Г.</a> обновил <a href="/book542889">Красные камзолы, часть вторая</a> 227k<span class="green">+24k</span><br/><br/>
<a class="dotted_button" href="/news">Все последние обновления »</a>
</div><div class="clear"></div></div><div class="MainBlock Center"><div class="top-banner">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- bookslist_inform_adaptive -->
<ins class="adsbygoogle" data-ad-client="ca-pub-6359265769479039" data-ad-format="auto" data-ad-slot="6789411395" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div></div><div class="ContentTable MainBlock"><div class="MainBlockLeft">
<h1>Топ авторов</h1>
<a class="user" href="/author19">Бастет Бродячая Кошка</a><br/><span class="small_light">Произведений: 4   Подписчиков: 503</span><br/><br/><a class="user" href="/author6">Седрик</a><br/><span class="small_light">Произведений: 226   Подписчиков: 226</span><br/><br/><a class="user" href="/author65">Метельский Николай</a><br/><span class="small_light">Произведений: 7   Подписчиков: 177</span><br/><br/><a class="user" href="/author139">Сапегин Александр Павлович</a><br/><span class="small_light">Произведений: 4   Подписчиков: 129</span><br/><br/><a class="user" href="/author618">Балакин Андрей В.</a><br/><span class="small_light">Произведений: 6   Подписчиков: 119</span><br/><br/><a class="user" href="/author97">Kami</a><br/><span class="small_light">Произведений: 44   Подписчиков: 101</span><br/><br/><a class="user" href="/author1378">Арестович Артур Викторович</a><br/><span class="small_light">Произведений: 3   Подписчиков: 92</span><br/><br/><a class="user" href="/author35">Zang</a><br/><span class="small_light">Произведений: 2   Подписчиков: 90</span><br/><br/><a class="user" href="/author354">Мищенко Александр Владиславович</a><br/><span class="small_light">Произведений: 18   Подписчиков: 84</span><br/><br/><a class="user" href="/author10">Гончарова Галина Дмитриевна</a><br/><span class="small_light">Произведений: 29   Подписчиков: 81</span><br/><br/><a class="user" href="/author148">Захарова Наталья Анатольевна</a><br/><span class="small_light">Произведений: 54   Подписчиков: 73</span><br/><br/><a class="user" href="/author72">Небо В Глазах Ангела</a><br/><span class="small_light">Произведений: 7   Подписчиков: 72</span><br/><br/><a class="user" href="/author510">Кощиенко Андрей Геннадьевич</a><br/><span class="small_light">Произведений: 9   Подписчиков: 72</span><br/><br/><a class="user" href="/author63">Сыромятникова Ирина</a><br/><span class="small_light">Произведений: 8   Подписчиков: 67</span><br/><br/><a class="user" href="/author5485">Ясенева Ника</a><br/><span class="small_light">Произведений: 6   Подписчиков: 64</span><br/><br/><a class="user" href="/author145">Демченко А.В.</a><br/><span class="small_light">Произведений: 25   Подписчиков: 55</span><br/><br/><a class="user" href="/author5">Сейтимбетов Самат Айдосович</a><br/><span class="small_light">Произведений: 31   Подписчиков: 53</span><br/><br/><a class="user" href="/author624">Измайлова Кира</a><br/><span class="small_light">Произведений: 7   Подписчиков: 53</span><br/><br/><a class="user" href="/author93">Hitech Алекс</a><br/><span class="small_light">Произведений: 6   Подписчиков: 52</span><br/><br/><a class="user" href="/author124">Маленький Диванный Тигр</a><br/><span class="small_light">Произведений: 8   Подписчиков: 52</span><br/><br/>
<a class="dotted_button" href="/index.php?section=search&amp;subject=user">Все авторы »</a>
</div><div class="MainBlockRight">
<h1>Топ произведений</h1>
<a href="/book847">Фанфик по Гарри Поттеру: Мы, аристократы - 1</a> автора <a class="user" href="/author19">Бастет Бродячая Кошка</a><br/><span class="small_light">1038 читателей   Фэнтези, 351 kb</span><br/><br/><a href="/book35383">Бракованный мозг и Орден Феникса</a> автора <a class="user" href="/author19">Бастет Бродячая Кошка</a><br/><span class="small_light">1003 читателя   Фэнтези, 661 kb</span><br/><br/><a href="/book1307">Фанфик по Гарри Поттеру: Мы, аристократы - 5</a> автора <a class="user" href="/author19">Бастет Бродячая Кошка</a><br/><span class="small_light">989 читателей   Фэнтези, 1159 kb</span><br/><br/><a href="/book1775">Ворон. Подлинная история лорда Северуса Тобиаса Снейп-Принц.</a> автора <a class="user" href="/author74">Vizivul</a><br/><span class="small_light">855 читателей   Фэнтези, 1577 kb</span><br/><br/><a href="/book61743">Настоящий Слизеринец. Части 1 и 2</a> автора <a class="user" href="/author354">Мищенко Александр Владиславович</a><br/><span class="small_light">727 читателей   Фэнтези, 355 kb</span><br/><br/><a href="/book848">Фанфик по Гарри Поттеру: Мы, аристократы - 2</a> автора <a class="user" href="/author19">Бастет Бродячая Кошка</a><br/><span class="small_light">610 читателей   Фэнтези, 616 kb</span><br/><br/><a href="/book2129">Живёшь только трижды</a> автора <a class="user" href="/author93">Hitech Алекс</a><br/><span class="small_light">572 читателя   Фантастика/Фэнтези/Юмор, 2430 kb</span><br/><br/><a href="/book849">Фанфик по Гарри Поттеру: Мы, аристократы - 3</a> автора <a class="user" href="/author19">Бастет Бродячая Кошка</a><br/><span class="small_light">567 читателей   Фэнтези, 757 kb</span><br/><br/><a href="/book850">Фанфик по Гарри Поттеру: Мы, аристократы - 4</a> автора <a class="user" href="/author19">Бастет Бродячая Кошка</a><br/><span class="small_light">552 читателя   Фэнтези, 694 kb</span><br/><br/><a href="/book20135">Истинный целитель</a> автора <a class="user" href="/author1045">Няха</a><br/><span class="small_light">519 читателей   Приключения, 195 kb</span><br/><br/><a href="/book57185">Аромат лимонной мяты. Книга первая.</a> автора <a class="user" href="/author1694">Весенний Смерч</a><br/><span class="small_light">510 читателей   Фэнтези, 1086 kb</span><br/><br/><a href="/book25173">Бастард</a> автора <a class="user" href="/author957">Капелан</a><br/><span class="small_light">504 читателя   Фанфик, 1068 kb</span><br/><br/><a href="/book12750">Другой</a> автора <a class="user" href="/author19">Бастет Бродячая Кошка</a><br/><span class="small_light">497 читателей   Фэнтези, 551 kb</span><br/><br/><a href="/book8193">Жизнь на лезвии бритвы.</a> автора <a class="user" href="/author139">Сапегин Александр Павлович</a><br/><span class="small_light">441 читатель   Фэнтези, 985 kb</span><br/><br/><a href="/book385610">Фанфик по Гарри Поттеру: Мы, аристократы - 6</a> автора <a class="user" href="/author19">Бастет Бродячая Кошка</a><br/><span class="small_light">423 читателя   Фанфик, 282 kb</span><br/><br/>
<a class="dotted_button" href="/index.php?section=find">Все произведения »</a>
</div><div class="clear"></div></div><a name="about"></a>
<div class="ContentTable MainBlock" style="line-height:150%;">
<h1>О проекте</h1><p><b>BooksList.me</b> - это помощник читателю литературного сайта СамИздат (samlib.ru, budclub.ru). Наш сайт предоставляет дополнительные возможности, которых нет на СИ.</p>
<br/>
<p><b>Подписка на произведение</b> - добавьте на произведение метку "Подписка на новые главы", и вы будете получать уведомления о каждом изменении текста (в течение часа после внесения изменений автором на СИ). Добавлять произведения в подписку можно списком, а не по одному.</p>
<br/>
<p><b>Подписка на автора</b> - подпишитесь на автора, и вы будете получать уведомления о каждом новом его произведении.</p>
<br/>
<p><b>История изменений</b> - мы записываем каждое изменение текста автором и даём возможность читать произведение с любого места. Забыли о книге на пару месяцев, автор успел обновить её три раза, вы не знаете с какого места читать - у нас эту проблему легко решить.</p>
<br/>
<p><b>Статус "закончено"</b> - у нас есть возможность указать, что произведение дописано до конца. Для нескольких тысяч произведений статус уже указан, и это условие можно использовать при поиске.</p>
<br/>
<p><b>Коллекции</b> - создайте свои коллекции книг, чтобы ничего не потерять. Листайте чужие коллекции, чтобы найти интересные книги. Это похоже на "списки", которые ведут многие пользователи СамИздата, только удобнее - информация о добавленных в коллекцию произведениях обновляется автоматически, на коллекцию можно подписаться, и получать уведомления, когда составитель добавляет туда новое произведение. Добавлять произведения в коллекции можно списком, а не по одному.</p>
<br/>
<p><b>Рекомендации</b> - это лучше комментариев, рекомендации бывают только положительными, если читатель пишет рекомендацию, значит книга ему понравилась и он рекомендует её другим. Всегда интересно узнать, кто и какие книги рекомендует.</p>
<br/>
<p><b>Метки, заметки, закладки</b> - у нас удобно не только читать одно произведение, но и вести учёт всего прочитанного или того, что вы только планируете прочитать.</p><br/>
</div><div class="center"><a href="/index.php?section=reg"><input class="modern_button" type="button" value="Зарегистрироваться"/><br/><br/></a></div><br/><div class="clear"></div>
</div>
<div id="site-content-right">
<div class="hide_sidebar" onclick="$('#site-content-right').toggleClass('show');">&gt;&gt;</div>
<div class="logo">
<table class="PrivatMenu">
<tr>
<td width="40"></td>
<td><div><a class="black" href="/index.php?section=profile" id="private_menu_name"><b></b></a></div></td>
</tr>
</table>
</div>
<div id="Enter">
<a href="/autent.php"><input class="modern_button" type="button" value="Вход"/></a>
<br/>
<a href="/index.php?section=reg"><input class="modern_button" type="button" value="Регистрация"/></a>
<div style="margin-bottom:15px;font-size:11px; font-weight:bold;">Войти при помощи:</div>
<script src="//ulogin.ru/js/ulogin.js"></script>
<div data-uloginid="0d08db7a" id="uLogin"></div>
</div>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><br/><div class="center" id="inform_right_160x600">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- bookslist_inform_160 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-6359265769479039" data-ad-slot="3601025798" style="display:inline-block;width:160px;height:600px"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div></td></tr><tr><td><hr class="light"/></td></tr>
<tr><td class="small" style="padding:10px 0 5px 0;"><a href="https://litrpg.ru" target="_blank" title="Книги жанра ЛитРПГ"><img src="//img1.fanfics.me/images/litrpg-logo.png"/></a>Книги жанра ЛитРПГ<br/>Опубликуй свою книгу!</td></tr>
<tr><td><hr class="light"/></td></tr>
<tr><td class="small" style="padding:10px 0 5px 0;"><a href="https://fanfics.me" target="_blank" title="Фанфики"><img src="//img1.fanfics.me/images/pf_logo_char_7.png" width="160"/></a><br/></td></tr>
<tr><td><hr class="light"/></td></tr>
<tr><td class="small" style="padding:10px 0 5px 0;"><a href="https://ranobe.me" target="_blank" title="Ранобэ читать онлайн"><img src="/images/ranobe-logo.png"/></a>О-о-о-очень длинные истории о попаданцах и реинкарнаторах!</td></tr>
<tr><td><hr class="light"/></td></tr>
<tr><td><div class="center" id="inform_right_160x600_2">
<!-- Yandex.RTB R-A-189321-1 -->
<div id="yandex_rtb_R-A-189321-1"></div>
<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-189321-1",
                renderTo: "yandex_rtb_R-A-189321-1",
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script>
</div></td></tr><tr>
<td align="center" class="small">
<br/>
<!--noindex-->
<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t29.5;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet: показано количество просмотров и"+
" посетителей' "+
"border='0' width='88' height='120'><\/a>")
//--></script><!--/LiveInternet-->
<!--/noindex-->
<br/><br/>
</td>
</tr>
</table>
</div>
</div>
<div id="footer">
<div class="Footer_1">
<div class="Footer_2">
<ul>
<li><a href="/autent.php">Войти</a></li>
<li><a href="/index.php?section=reg">Зарегистрироваться</a></li> </ul>
</div>
<div class="Footer_2">
<ul>
<li><a href="/news">Новости</a></li>
<li><a href="/recommends">Рекомендации</a></li>
<li><a href="/collections">Коллекции</a></li>
<li><a href="/putewoditel">Путеводитель</a></li>
<li><a href="/index.php?section=search&amp;subject=user">Авторы</a></li>
<li><a href="/index.php?section=find">Поиск</a></li>
</ul>
</div>
<div class="clear"></div>
</div>
<div class="Footer_1">
<div class="Footer_2">
<ul>
<li><a href="https://fanfics.me">Fanfics.me</a></li>
<li><a href="https://litrpg.ru">LitRPG.ru</a></li>
<li><a href="https://multifandom.ru">Multifandom.ru</a></li>
<li><a href="https://ranobe.me">Ranobe.me</a></li>
<li><a href="https://lovehelp.ru">LoveHelp.ru</a></li>
</ul>
</div>
<div class="Footer_2">
<ul id="footer-menu">
<li><a href="/index.php?section=project">О проекте</a></li>
<li><a href="/index.php?section=project&amp;action=rules">Правила</a></li>
<br/>
<li>2016-2021 © <a href="/">BooksList.me</a></li>
</ul>
</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
</div>
<div style="display: none;">
<div class="box-modal" id="exampleModal">
<div class="box-modal_close arcticmodal-close">Закрыть</div>
<div id="modalBody"></div>
</div>
<div class="box-modal" id="likesModal">
<div class="box-modal_close arcticmodal-close">Закрыть</div>
<div id="likesmodalBody"></div>
</div>
</div>
<div id="body_error">
<div id="body_error_close"><a onclick="$('#body_error').hide();" title="Закрыть сообщение об ошибке">Закрыть</a></div>
<div id="body_error_text"></div>
</div>
</body>
</html>