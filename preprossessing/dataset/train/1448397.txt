<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-gb" xml:lang="en-gb" xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="https://www.threegreen.com.au/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="Three Green" name="description"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>HOME - Three Green</title>
<link href="/?format=feed&amp;type=rss" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="/?format=feed&amp;type=atom" rel="alternate" title="Atom 1.0" type="application/atom+xml"/>
<link href="/plugins/system/jce/css/content.css?a421c96175b774fb13a84684fc787319" rel="stylesheet" type="text/css"/>
<link href="/modules/mod_vertical_menu/cache/109/ae2d3640e415ebb939d9715dab88d21a.css" rel="stylesheet" type="text/css"/>
<link href="/modules/mod_universal_ajaxlivesearch/cache/121/91fa51e0bd1b7574fca6465024bd3e6b.css" rel="stylesheet" type="text/css"/>
<link href="/media/widgetkit/wk-styles-77f86ef0.css" id="wk-styles-css" rel="stylesheet" type="text/css"/>
<style type="text/css">

.noscript div#off-menu_109 dl.level1 dl{
	position: static;
}
.noscript div#off-menu_109 dl.level1 dd.parent{
	height: auto !important;
	display: block;
	visibility: visible;
}

	</style>
<script src="/plugins/system/offlajnparams/compat/greensock.js" type="text/javascript"></script>
<script src="/media/jui/js/jquery.min.js?a421c96175b774fb13a84684fc787319" type="text/javascript"></script>
<script src="/media/jui/js/jquery-noconflict.js?a421c96175b774fb13a84684fc787319" type="text/javascript"></script>
<script src="/media/jui/js/jquery-migrate.min.js?a421c96175b774fb13a84684fc787319" type="text/javascript"></script>
<script src="/media/system/js/caption.js?a421c96175b774fb13a84684fc787319" type="text/javascript"></script>
<script src="/modules/mod_vertical_menu/js/perfect-scrollbar.js?_=1610477739" type="text/javascript"></script>
<script src="/modules/mod_vertical_menu/js/mod_vertical_menu.js?_=1610477739" type="text/javascript"></script>
<script src="https://www.threegreen.com.au/modules/mod_universal_ajaxlivesearch/engine/dojo.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/dojo/1.6/dojo/dojo.xd.js" type="text/javascript"></script>
<script src="/modules/mod_universal_ajaxlivesearch/cache/121/a0a5b2c34937489442fe07263b7d8017.js" type="text/javascript"></script>
<script src="/media/widgetkit/uikit2-e0c7ce32.js" type="text/javascript"></script>
<script src="/media/widgetkit/wk-scripts-82238bc3.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(window).on('load',  function() {
				new JCaption('img.caption');
			});GOOGLE_MAPS_API_KEY = "AIzaSyDaKGxSd34DMt5XBj-HQDKRsxCbd3SmEqg";
	</script>
<svg height="0" style="position:absolute"><symbol id="sym-point-to-right" viewbox="0 0 451.846 451.847"><path d="M345.441,248.292L151.154,442.573c-12.359,12.365-32.397,12.365-44.75,0c-12.354-12.354-12.354-32.391,0-44.744L278.318,225.92L106.409,54.017c-12.354-12.359-12.354-32.394,0-44.748c12.354-12.359,32.391-12.359,44.75,0l194.287,194.284c6.177,6.18,9.262,14.271,9.262,22.366C354.708,234.018,351.617,242.115,345.441,248.292z" fill="currentColor"></path></symbol></svg>
<script data-cfasync="false">
document[(_el=document.addEventListener)?'addEventListener':'attachEvent'](_el?'DOMContentLoaded':'onreadystatechange',function(){
	if (!_el && document.readyState != 'complete') return;
	(window.jq183||jQuery)('.noscript').removeClass('noscript');
	window.sm109 = new VerticalSlideMenu({
		id: 109,
		visibility: ["1","1","1","1","0",["0","px"],["10000","px"]],
		parentHref: 0,
		theme: 'rounded',
		result: 'Search Results',
		noResult: 'No Results Found',
		backItem: '',
		filterDelay: 500,
		filterMinChar: 3,
		navtype: 'drop',
		sidebar: -1,
		popup: 0,
		overlay: 0,
		sidebarUnder: 768,
		width: 300,
		menuIconCorner: 1,
		menuIconX: 0,
		menuIconY: 0,
		hidePopupUnder: 1750,
		siteBg: '#444444',
		effect: 12,
    dur: 400/1000,
		perspective: 0,
		inEase: 'Quad.easeOut'.split('.').reverse().join(''),
		inOrigin: '50% 50% 0',
		inX: 100,
		inUnitX: '%',
    logoUrl: '',
		inCSS: {
			y: 0,
			opacity: 100/100,
			rotationX: 0,
			rotationY: 0,
			rotationZ: 0,
			skewX: 0,
			skewY: 0,
			scaleX: 100/100,
			scaleY: 100/100
		},
		outEase: 'Quad.easeOut'.split('.').reverse().join(''),
		outOrigin: '50% 50% 0',
		outX: -100,
		outUnitX: '%',
		outCSS: {
			y: 0,
			opacity: 100/100,
			rotationX: 0,
			rotationY: 0,
			rotationZ: 0,
			skewX: 0,
			skewY: 0,
			scaleX: 100/100,
			scaleY: 100/100
		},
		anim: {
			perspective: 1000,
			inDur: 300/1000,
			inEase: 'Quad.easeOut'.split('.').reverse().join(''),
			inOrigin: '50% 50% 0',
			inX: -30,
			inUnitX: 'px',
			inCSS: {
				y: 0,
				opacity: 0/100,
				rotationX: 0,
				rotationY: 0,
				rotationZ: 0,
				skewX: 0,
				skewY: 0,
				scaleX: 100/100,
				scaleY: 100/100
			},
			outDur: 300/1000,
			outEase: 'Quad.easeOut'.split('.').reverse().join(''),
			outOrigin: '50% 50% 0',
			outX: 20,
			outUnitX: 'px',
			outCSS: {
				y: 0,
				opacity: 0/100,
				rotationX: 0,
				rotationY: 0,
				rotationZ: 0,
				skewX: 0,
				skewY: 0,
				scaleX: 100/100,
				scaleY: 100/100
			}
		},
		miAnim: 0,
		miDur: 500/1000,
		miShift: 40/1000,
		miEase: 'Quad.easeOut'.split('.').reverse().join(''),
		miX: 40,
		miUnitX: '%',
		miCSS: {
			transformPerspective: 600,
			transformOrigin: '50% 50% 0',
			y: 0,
			opacity: 0/100,
			rotationX: 0,
			rotationY: 0,
			rotationZ: 0,
			skewX: 0,
			skewY: 0,
			scaleX: 100/100,
			scaleY: 100/100
		},
		iconAnim: 0 && 0,
		bgX: 0,
		dropwidth: 250,
		dropspace: 0,
		dropFullHeight: 0,
		dropEvent: 'mouseenter',
		opened: 1,
		autoOpen: 0,
		autoOpenAnim: 1,
		hideBurger: 0,
		scrollOffset: parseInt('0')
	});
});
</script>
<script type="text/javascript">
dojo.addOnLoad(function(){
    document.search = new AJAXSearchsimple({
      id : '121',
      node : dojo.byId('offlajn-ajax-search121'),
      searchForm : dojo.byId('search-form121'),
      textBox : dojo.byId('search-area121'),
      suggestBox : dojo.byId('suggestion-area121'),
      searchButton : dojo.byId('ajax-search-button121'),
      closeButton : dojo.byId('search-area-close121'),
      searchCategories : dojo.byId('search-categories121'),
      productsPerPlugin : 3,
      dynamicResult : '0',
      searchRsWidth : 250,
      searchImageWidth : '180',
      minChars : 2,
      searchBoxCaption : 'Search...',
      noResultsTitle : 'Results(0)',
      noResults : 'No results found for the keyword!',
      searchFormUrl : '/index.php',
      enableScroll : '1',
      showIntroText: '1',
      scount: '10',
      lang: '',
      stext: 'No results found. Did you mean?',
      moduleId : '121',
      resultAlign : '1',
      targetsearch: '1',
      linktarget: '0',
      keypressWait: '500',
      catChooser : 0,
      searchResult : 1,
      seemoreEnabled : 1,
      seemoreAfter : 30,
      keywordSuggestion : '1',
      seeMoreResults : 'See more results...',
      resultsPerPage : '4',
      resultsPadding : '10',
      controllerPrev : 'PREV',
      controllerNext : 'NEXT',
      fullWidth : '1',
      resultImageWidth : '180',
      resultImageHeight : '130',
      showCat : '1',
      voicesearch : '0'
    })
  });</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-25272822-55"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-25272822-55');
</script>
<link href="/templates/three_green_clients_2017/css/template.css" rel="stylesheet" type="text/css"/>
<link href="/templates/three_green_clients_2017/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Didact+Gothic|Questrial&amp;display=swap" rel="stylesheet"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
</head>
<body>
<div id="wrap_background"></div>
<div id="container">
<div id="wrap_top">
<div id="top_block">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 logo_space">
<div class="custom">
<div><a href="/index.php"><img alt="Three Green" id="top_logo" src="/images/2019/three_green_logo_2.png"/></a></div></div>
</div>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
</div>
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2" id="center_block">
</div>
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2" id="center_block">
</div>
</div>
<div class="noscript">
<nav class="off-menu_109 sm-menu " id="off-menu_109">
<h3 class="sm-head">
<svg class="sm-back sm-arrow" title="Prev" viewbox="0 0 492 492"><path d="M464.344,207.418l0.768,0.168H135.888l103.496-103.724c5.068-5.064,7.848-11.924,7.848-19.124c0-7.2-2.78-14.012-7.848-19.088L223.28,49.538c-5.064-5.064-11.812-7.864-19.008-7.864c-7.2,0-13.952,2.78-19.016,7.844L7.844,226.914C2.76,231.998-0.02,238.77,0,245.974c-0.02,7.244,2.76,14.02,7.844,19.096l177.412,177.412c5.064,5.06,11.812,7.844,19.016,7.844c7.196,0,13.944-2.788,19.008-7.844l16.104-16.112c5.068-5.056,7.848-11.808,7.848-19.008c0-7.196-2.78-13.592-7.848-18.652L134.72,284.406h329.992c14.828,0,27.288-12.78,27.288-27.6v-22.788C492,219.198,479.172,207.418,464.344,207.418z" fill="currentColor"></path></svg> <span class="sm-title">Three Green</span> </h3>
<div class="sm-levels">
<div class="sm-level level1"><dl class="level1">
<dt class="level1 off-nav-101 notparent opened active first">
<div class="inner">
<div class="link"><a data-text="HOME" href="https://www.threegreen.com.au/">HOME</a></div>
</div>
<div class="sm-arrow">
</div>
</dt>
<dd class="level1 off-nav-101 notparent opened active first">
</dd>
<dt class="level1 off-nav-193 notparent">
<div class="inner">
<div class="link"><a data-text="ABOUT US" href="/about-us">ABOUT US</a></div>
</div>
<div class="sm-arrow">
</div>
</dt>
<dd class="level1 off-nav-193 notparent">
</dd>
<dt class="level1 off-nav-195 notparent">
<div class="inner">
<div class="link"><a data-text="PRODUCTS SHOWCASE" href="/products-showcase">PRODUCTS SHOWCASE</a></div>
</div>
<div class="sm-arrow">
</div>
</dt>
<dd class="level1 off-nav-195 notparent">
</dd>
<dt class="level1 off-nav-378 notparent">
<div class="inner">
<div class="link"><a data-text="OUR PRODUCTS" href="/products">OUR PRODUCTS</a></div>
</div>
<div class="sm-arrow">
</div>
</dt>
<dd class="level1 off-nav-378 notparent">
</dd>
<dt class="level1 off-nav-310 notparent">
<div class="inner">
<div class="link"><a data-text="RESPONSIBLE SOURCING" href="/responsible-soucing">RESPONSIBLE SOURCING</a></div>
</div>
<div class="sm-arrow">
</div>
</dt>
<dd class="level1 off-nav-310 notparent">
</dd>
<dt class="level1 off-nav-309 notparent">
<div class="inner">
<div class="link"><a data-text="ENVIRONMENTAL MANAGEMENT" href="/environmental-management">ENVIRONMENTAL MANAGEMENT</a></div>
</div>
<div class="sm-arrow">
</div>
</dt>
<dd class="level1 off-nav-309 notparent">
</dd>
<dt class="level1 off-nav-366 notparent">
<div class="inner">
<div class="link"><a data-text="PROCUREMENT PROCESS" href="/procurement-process">PROCUREMENT PROCESS</a></div>
</div>
<div class="sm-arrow">
</div>
</dt>
<dd class="level1 off-nav-366 notparent">
</dd>
<dt class="level1 off-nav-308 notparent">
<div class="inner">
<div class="link"><a data-text="TERMS &amp; CONDITIONS" href="/terms-conditions">TERMS &amp; CONDITIONS</a></div>
</div>
<div class="sm-arrow">
</div>
</dt>
<dd class="level1 off-nav-308 notparent">
</dd>
<dt class="level1 off-nav-128 notparent">
<div class="inner">
<div class="link"><a data-text="CONTACT US" href="/contact-us">CONTACT US</a></div>
</div>
<div class="sm-arrow">
</div>
</dt>
<dd class="level1 off-nav-128 notparent">
</dd>
<dt class="level1 off-nav-339 notparent">
<div class="inner">
<div class="link"><a data-text="CLIENT LOGIN" href="/client-login">CLIENT LOGIN</a></div>
</div>
<div class="sm-arrow">
</div>
</dt>
<dd class="level1 off-nav-339 notparent">
</dd>
</dl></div>
</div>
</nav></div>
<div class="" id="offlajn-ajax-search121">
<div class="offlajn-ajax-search-container">
<form action="/shop1/results,1-200?search=true" id="search-form121" method="get" onsubmit="return false;">
<div class="offlajn-ajax-search-inner">
<input autocomplete="off" id="search-area121" name="keyword" type="text" value=""/>
<input autocomplete="off" id="suggestion-area121" name="searchwordsugg" tabindex="-1" type="text" value=""/>
<input name="option" type="hidden" value="com_virtuemart"/>
<input name="page" type="hidden" value="shop.browse"/>
<input name="view" type="hidden" value="category"/>
<input name="virtuemart_category_id" type="hidden" value="0"/>
<div id="search-area-close121"></div>
<div id="ajax-search-button121"><div class="magnifier"></div></div>
<div class="ajax-clear"></div>
</div>
</form>
<div class="ajax-clear"></div>
</div>
</div>
<div class="ajax-clear"></div>
<svg height="0" style="position:absolute" width="0"><filter id="searchblur"><fegaussianblur in="SourceGraphic" stddeviation="3"></fegaussianblur></filter></svg>
</div>
</div>
</div>
</div>
<div>
<div data-uk-slideshow="{}">
<div class="uk-position-relative">
<ul class="uk-slideshow">
<li style="min-height: 100px;">
<img alt="Home Banner" src="/images/Front_Page_-_Banner_-_V2.png"/>
</li>
</ul>
</div>
</div>
</div>
<div id="wrap_slogan">
<div class="custom">
<div class="white_text" id="block_cont">
<h1>Welcome</h1>
<p>Think of us as your personal procurement assistant. More than a business partner, we are a resource who works directly for you on procurement projects and tasks you determine, in formats you require and to timetables you establish. The only proviso we have is that the parameters are within what is humanly possible, safe, legal and both financially and ethically responsible.<br/> <br/>We operate differently; whether it be inside or outside or along-side of normal product procurement channels. There is no necessity nor any incentive for you to operate within and be restricted to a catalogue(s) and the catalogue(s) set product options nor for that matter, product formats that you find on the internet.<br/> <br/>Three Green does both the easy and hard, the small and large volume, left of field or standard items, old classics, new to market or improved formats, with any modifications you require, any decoration you desire, direct from the most relevant and advantageous factory (time permitting), at the lowest and best possible, respectively, cost and quality, with a money back guarantee.<br/> <br/>We are professional procurement officers’ assisting an array of business departments (marketing, sales, administration, specialised procurement) across a broad spectrum of manufacturing disciplines (textiles, technology, stamped, moulded etc), for government, small medium and medium to large businesses.<br/> <br/>We don’t operate in the areas you do well yourself. We compliment the available skill sets while not infringing on flexibility nor unreasonably restricting it, thus helping you realise creative plans. We solve problems.<br/> <br/>Our strong preference and where we can drive out the largest amount benefit is when we go direct to a selected factory under client approved specifications to Australian retail standards. This model allows us to cut out levels of margin, complexity, rigidness and also allowing greater scope for personalisation of products.<br/> <br/>Increasingly our business is becoming specialised in the sourcing of new, difficult to negotiate or find, comparable items to patented products and or outsourcing of procurement so as to drive down costs and or shifting of procurement risk to an external party. We have procured medical devices, organised hot embossing to soft low-density diary covers, were the first to bring in rice husk and stone paper products for non-retail purposes; to name a few.<br/> <br/>We are universally recognised as being different. We are a small team of experienced and motivated employees who are empowered to operate independently, allowing quick response times, greater flexibility and adaptability to client requirements, while also being able to harness the skills of all members of our business, our business partners and factories.<br/> <br/>We pride ourselves on ensuring that the highest standards of professional practice (ethical sourcing, best environmental outcomes, adherence to statutory obligations e.g. Modern Slavery Act). This discipline is reflected in all outcomes and products we source.<br/> <br/>Consistence builds trust. Producing value and quality for the respective spend, at no risk and with the minimum inconvenience and greatest customer satisfaction is our core and only focus.</p>
</div></div>
</div>
<div id="wrap_option">
</div>
<div id="wrap_content">
<div id="inner_content">
<div class="blog-featured" itemscope="" itemtype="https://schema.org/Blog">
</div>
</div>
</div>
<div id="wrap_lower_banner">
<div class="custom">
<div style="text-align: center;">
<h6>Our Commitment to You</h6>
</div>
<p><br/><br/></p>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1"> </div>
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"><img alt="icon2 responsible sourcing" src="/images/2019/icons/icon2_responsible_sourcing.png"/></div>
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"><img alt="icon2 low price" src="/images/2019/icons/icon2_low_price.png"/></div>
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"><img alt="icon2 customer service" src="/images/2019/icons/icon_customer_service.png"/></div>
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"><img alt="icon2 warehouse" src="/images/2019/icons/icon2_warehouse.png"/></div>
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"><img alt="icon2 zero risk" src="/images/2019/icons/icon2_zero_risk.png"/></div>
</div></div>
</div>
<div id="wrap_map">
</div>
<div id="wrap_footer">
<div id="block_cont">
<div class="custom">
<div class="row footer_text">
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="text-align: center;">47 Market St<br/>South Melbourne Vic 3205</div>
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="text-align: center;">Three Green | Product Solutions</div>
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="text-align: center;">Phone: <a href="tel:1800060305">1800 060 305</a><br/>Mobile +61 48 484 0 484<br/>Email: <a href="mailto:sales@threegreen.com.au">sales@threegreen.com.au</a></div>
</div></div>
</div>
</div>
</div>
</body>
</html>