<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>A Day In The Life Of...</title>
<link href="https://adayinthelifeof.nl/assets/css/styles_feeling_responsive.css" rel="stylesheet" type="text/css"/>
<script src="https://adayinthelifeof.nl/assets/js/modernizr.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js"></script>
<script>
    WebFont.load({
      google: {
        families: [ 'Lato:400,700,400italic:latin', 'Volkhov::latin' ] 
      }
    });
  </script>
<noscript>
<link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic|Volkhov" rel="stylesheet" type="text/css"/>
</noscript>
<meta content="" name="description"/>
<link href="/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="/apple-touch-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/android-chrome-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/manifest.json" rel="manifest"/>
<link color="#5bbad5" href="/safari-pinned-tab.svg" rel="mask-icon"/>
<meta content="#da532c" name="msapplication-TileColor"/>
<meta content="/mstile-144x144.png" name="msapplication-TileImage"/>
<meta content="#ffffff" name="theme-color"/>
<!-- Facebook Optimization -->
<meta content="en_EN" property="og:locale"/>
<meta content="A Day In The Life Of..." property="og:title"/>
<meta content="" property="og:description"/>
<meta content="https://adayinthelifeof.nl//index.html" property="og:url"/>
<meta content="A Day In The Life Of..." property="og:site_name"/>
<!-- Search Engine Optimization -->
<style type="text/css">
        .PageNavigation {
          font-size: 14px;
          display: block;
          width: auto;
          overflow: hidden;
        }

        .PageNavigation a {
          display: block;
          width: 50%;
          float: left;
          margin: 1em 0;
        }

        .PageNavigation .next {
          text-align: right;
        }

        table {
            width: 100%
        }
	</style>
</head>
<body class="" id="top-of-page">
<div class="sticky" id="navigation">
<nav class="top-bar" data-topbar="" role="navigation">
<ul class="title-area">
<li class="name">
</li>
<li class="toggle-topbar menu-icon"><a href="#"><span>Navigation</span></a></li>
</ul>
<section class="top-bar-section">
<ul class="right">
<li class="divider"></li>
<li><a href="https://adayinthelifeof.nl/gpg-key/">GPG Key</a></li>
<li class="divider"></li>
<li><a href="https://adayinthelifeof.nl/search/">Search</a></li>
<li class="divider"></li>
<li><a href="https://adayinthelifeof.nl/contact/">Contact</a></li>
</ul>
<ul class="left">
<li><a href="https://adayinthelifeof.nl/">Home</a></li>
<li class="divider"></li>
<li class="has-dropdown">
<a href="https://adayinthelifeof.nl/">Blog</a>
<ul class="dropdown">
<li><a href="https://adayinthelifeof.nl/archive/">Archive</a></li>
<li><a href="https://adayinthelifeof.nl/archive/tags/">By tags</a></li>
<li><a href="https://adayinthelifeof.nl/archive/date/">By date</a></li>
</ul>
</li>
<li class="divider"></li>
<li><a href="https://adayinthelifeof.nl/about-me/">About me</a></li>
<li class="divider"></li>
<li><a href="https://adayinthelifeof.nl/overview/">Talks &amp; Workshops</a></li>
<li class="divider"></li>
<li><a href="https://adayinthelifeof.nl/books/">Books</a></li>
<li class="divider"></li>
</ul>
</section>
</nav>
</div><!-- /#navigation -->
<div id="masthead-with-text" style=" ">
<div class="row">
<div class="small-12 columns">
<div class="masthead-title">A day in the life of,..</div>
</div><!-- /.small-12.columns -->
</div><!-- /.row -->
</div><!-- /#masthead -->
<div class="row t30">
<div class="medium-12 columns">
<article>
<header>
<h1></h1>
</header>
<h1>Latest blog posts</h1>
<dl>
<dd>
<h2><a href="https://adayinthelifeof.nl/2020/08/12/mac2win.html" title="Read From Mac to Windows">From Mac to Windows</a></h2>
<img align="right" src="//www.gravatar.com/avatar/1761ecd7fe763583553dde43e62c47bd?s=50"/>

Date: 12 Aug 2020<br/>
Tags:

    
    [ <a href="/archive/tags/mac">mac</a> ] 

    
    [ <a href="/archive/tags/win">win</a> ] 

<br/>
<br/>
<p>Iâve moved from a Macbook Pro to a Dell PS running Windows 10. I decided against MacBook after their annoucement to 
move to ARM. Even though the macbook 16â was the only system i actually was thinking of buying, iâve decided against it 
and give windows a new try. Iâve heared more and more good stories about windows, and more and more bad stored about mac 
over the last few years. Letâs switch!</p>
<div style="text-align: right">
<a class="button radius tiny" href="https://adayinthelifeof.nl/2020/08/12/mac2win.html" title="Read From Mac to Windows"><strong>Read more...</strong></a>
</div>
<hr/>
</dd>
<dd>
<h2><a href="https://adayinthelifeof.nl/2020/06/12/bitmaelum.html" title="Read Introducting BitMaelum - A new mail concept">Introducting BitMaelum - A new mail concept</a></h2>
<img align="right" src="//www.gravatar.com/avatar/1761ecd7fe763583553dde43e62c47bd?s=50"/>

Date: 12 Jun 2020<br/>
Tags:

    
    [ <a href="/archive/tags/crypto">crypto</a> ] 

    
    [ <a href="/archive/tags/email">email</a> ] 

    
    [ <a href="/archive/tags/bitmaelum">bitmaelum</a> ] 

<br/>
<br/>
<p>What if you can design an email system with a clean sheet. You donât need to care about existing email clients or servers or anything at all. Even the concept of an email address can be touched. What would such a system look like? This is my attempt,..</p>
<div style="text-align: right">
<a class="button radius tiny" href="https://adayinthelifeof.nl/2020/06/12/bitmaelum.html" title="Read Introducting BitMaelum - A new mail concept"><strong>Read more...</strong></a>
</div>
<hr/>
</dd>
<dd>
<h2><a href="https://adayinthelifeof.nl/2020/05/20/aws.html" title="Read Amazon Web Services">Amazon Web Services</a></h2>
<img align="right" src="//www.gravatar.com/avatar/1761ecd7fe763583553dde43e62c47bd?s=50"/>

Date: 20 May 2020<br/>
Tags:

    
    [ <a href="/archive/tags/aws">aws</a> ] 

    
    [ <a href="/archive/tags/amazon">amazon</a> ] 

<br/>
<br/>
<p>More often than not, Iâm using Amazon Web Services (AWS) as my âcloudâ. Not only for my own projects, but almost all customers Iâm working for use Amazon for hosting their applications. So over time you build up a lot of experience on AWS service: you know how to (correctly) setup VPCâs, know when to you ECS, EC2 or lambda to host code and even services like S3, SNS and SQS pose no challenges anymore.</p>
<p>But there are a lot of AWS services available. And I do mean: a LOT. Currently, there are 163 (!) different services that are available from the Amazon Dashboard, each with their own way of working, difficulties, catches and best practises.</p>
<div style="text-align: right">
<a class="button radius tiny" href="https://adayinthelifeof.nl/2020/05/20/aws.html" title="Read Amazon Web Services"><strong>Read more...</strong></a>
</div>
<hr/>
</dd>
<dd>
<h2><a href="https://adayinthelifeof.nl/2017/09/19/symfony-autowire.html" title="Read Symfony's autowiring">Symfony's autowiring</a></h2>
<img align="right" src="//www.gravatar.com/avatar/1761ecd7fe763583553dde43e62c47bd?s=50"/>

Date: 19 Sep 2017<br/>
Tags:

    
    [ <a href="/archive/tags/symfony">symfony</a> ] 

    
    [ <a href="/archive/tags/autowire">autowire</a> ] 

    
    [ <a href="/archive/tags/magic">magic</a> ] 

<br/>
<br/>
<p>When asking people if they use Symfonyâs autowiring functionality, an often heard excuse why people donât use it is because of all the magic that is happening during autowiring. But just like most impressive magic tricks, once explained, it all boils down to a few simple principles and Symfonyâs autowiring is nothing different in that perspective. In this blogpost I will explain the new autowiring and autoconfiguration features, and why you should love them.</p>
<div style="text-align: right">
<a class="button radius tiny" href="https://adayinthelifeof.nl/2017/09/19/symfony-autowire.html" title="Read Symfony's autowiring"><strong>Read more...</strong></a>
</div>
<hr/>
</dd>
<dd>
<h2><a href="https://adayinthelifeof.nl/2016/02/28/write-your-own-github-clone.html" title="Read Write your own GitHub clone">Write your own GitHub clone</a></h2>
<img align="right" src="//www.gravatar.com/avatar/1761ecd7fe763583553dde43e62c47bd?s=50"/>

Date: 28 Feb 2016<br/>
Tags:

    
    [ <a href="/archive/tags/github">github</a> ] 

    
    [ <a href="/archive/tags/git">git</a> ] 

<br/>
<br/>
<p>Shower thought: What would it take to write your own GitHub clone? Answer: not that much! Iâve spend a few hours on 
tinkering with some of the basic concepts, and it turns out itâs actually quite easy to set something up from scratch. 
And before you all go and write comments that it not feature-complete: yes, I know. But most of them are fairly 
trivial to implement though, and my goal was to actually see if we can get the foundations up and running. Implementing 
things like an issue-tracker and webhooks isnât part of that.</p>
<div style="text-align: right">
<a class="button radius tiny" href="https://adayinthelifeof.nl/2016/02/28/write-your-own-github-clone.html" title="Read Write your own GitHub clone"><strong>Read more...</strong></a>
</div>
<hr/>
</dd>
</dl>
<p><a class="button radius large" href="/archive/">View older posts</a></p>
</article>
</div><!-- /.medium-12.columns -->
</div><!-- /.row -->
<div class="row" id="up-to-top">
<div class="small-12 columns" style="text-align: right;">
<a class="iconfont" href="#top-of-page"></a>
</div><!-- /.small-12.columns -->
</div><!-- /.row -->
<footer class="bg-grau" id="footer-content">
<div id="footer">
<div class="row">
<div class="medium-6 large-5 columns">
<h5 class="shadow-black">About this site</h5>
<p class="shadow-black">
              This is the personal website of Joshua Thijssen, where I on occasion
              post useful posts, not-so-useful posts, rants, deep thoughts and
              everything in between.
            </p>
</div><!-- /.large-6.columns -->
<div class="small-6 medium-3 large-3 large-offset-1 columns">
<h5 class="shadow-black">Services</h5>
<ul class="no-bullet shadow-black">
<li>
<a href="" title=""></a>
</li>
<li>
<a href="/syndication-policy/" title="Syndication Policy">Syndication Policy</a>
</li>
<li>
<a href="/feed/" title="Subscribe to RSS Feed">RSS</a>
</li>
<li>
<a href="/atom.xml" title="Subscribe to Atom Feed">Atom</a>
</li>
<li>
<a href="/sitemap.xml" title="Sitemap for Google Webmaster Tools">sitemap.xml</a>
</li>
</ul>
</div><!-- /.large-4.columns -->
<div class="small-6 medium-3 large-3 columns">
<h5 class="shadow-black">More info: </h5>
<ul class="no-bullet shadow-black">
<a href="https://www.techanalyze.io">https://www.techanalyze.io</a>
<a href="https://www.techademy.nl">https://www.techademy.nl</a>
<a href="https://www.noxlogic.nl">https://www.noxlogic.nl</a>
<a href="https://www.dutchweballiance.nl">https://www.dutchweballiance.nl</a>
</ul>
</div><!-- /.large-3.columns -->
</div><!-- /.row -->
</div><!-- /#footer -->
<div id="subfooter">
<nav class="row">
<section class="b30 small-12 medium-6 columns credits" id="subfooter-left">
<p>
              Created with ♥
              by <a href="">Joshua Thijssen</a>
              with <a href="http://jekyllrb.com/" target="_blank">Jekyll</a>
              based on <a href="http://phlow.github.io/feeling-responsive/">Feeling Responsive</a>.
            </p>
</section>
<section class="small-12 medium-6 columns social-icons" id="subfooter-right">
<ul class="inline-list">
<li><a class="icon-twitter" href="http://twitter.com/JayTaph" target="_blank" title="@JayTaph"></a></li>
<li><a class="icon-github" href="http://github.com/JayTaph" target="_blank" title="Code..."></a></li>
<li><a class="icon-mail" href="mailto://jthijssen@noxlogic.nl" target="_blank" title="Mail"></a></li>
<li><a class="icon-linkedin" href="https://nl.linkedin.com/in/joshuathijssen" target="_blank" title="LinkedIn"></a></li>
<li><a class="icon-youtube" href="https://www.youtube.com/results?search_query=joshua+thijssen" target="_blank" title="YouTube"></a></li>
</ul>
</section>
</nav>
</div><!-- /#subfooter -->
</footer>
<script src="https://adayinthelifeof.nl/assets/js/javascript.min.js"></script>
<script>
    var num = Math.floor(Math.random() * (10 - 1)) + 1;
    var s = num + "";
    while (s.length < 3) s = "0" + s;

    var image_url = "https://adayinthelifeof.nl/images//headers/header-" + s + ".jpg";

    $("#masthead").backstretch(image_url, {fade: 700});
    $("#masthead-with-text").backstretch(image_url, {fade: 700});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-12154327-1', 'auto');
  ga('set', 'anonymizeIp', true);
  ga('send', 'pageview');

</script>
</body>
</html>
