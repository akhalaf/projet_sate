<!DOCTYPE html>
<html amp="" lang="en-US">
<head>
<meta charset="utf-8"/>
<title>OBDII Codes Engine Light Definitions Diagnostic Repair | AutoCodes</title>
<!-- ARTaTFwyRygP03CMaIrrQCmHNpY -->
<meta content="AutoCodes.com - The largest OBDII Codes Database - Description, Location and Repair Information" name="description"/>
<link href="https://www.autocodes.com/index.php" rel="canonical"/>
<meta content="eES7ltlGP80aNQcR13QsMbUblWCKWZPYlFemDqf27sc" name="google-site-verification"/>
<meta content="width=device-width,minimum-scale=1,initial-scale=1" name="viewport"/>
<link href="https://www.autocodes.com/images/favicon/apple-touch-icon.png" rel="apple-touch-icon"/>
<link href="https://www.autocodes.com/images/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="https://www.autocodes.com/images/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="https://www.autocodes.com/images/favicon/site.webmanifest" rel="manifest"/>
<link color="#5bbad5" href="https://www.autocodes.com/images/favicon/safari-pinned-tab.svg" rel="mask-icon"/>
<link href="https://www.autocodes.com/images/favicon/favicon.ico" rel="shortcut icon"/>
<meta content="#da532c" name="msapplication-TileColor"/>
<meta content="https://www.autocodes.com/images/favicon/browserconfig.xml" name="msapplication-config"/>
<meta content="#ffffff" name="theme-color"/>
<style amp-boilerplate="">body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate="">body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<link as="script" href="https://cdn.ampproject.org/v0.js" rel="preload"/>
<script async="" src="https://cdn.ampproject.org/v0.js"></script>
<script async="" custom-element="amp-ad" src="https://cdn.ampproject.org/v0/amp-ad-0.1.js"></script>
<script async="" custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
<script async="" custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
<script async="" custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
<style amp-custom="">html,body,div,ul,li,td{font-family:Verdana;font-variant:normal;font-size:16px;color:#161616}body{margin:0;background-color:#E9EBEE}body:before{content:'';opacity:0;transition:opacity .5s}body.overlay:before{position:fixed;top:0;bottom:0;left:0;right:0;background:#000;opacity:.4}a:visited,a:link,a:active{text-decoration:none;color:#024A93}a:hover{color:#024A93;text-decoration:underline}a img{border:none}a:focus{outline:0}#container{width:1000px;margin:auto;clear:both;margin-top:55px;padding:5px}h3.code{font-size:24px;font-weight:500;margin-top:10px;margin-bottom:0}.container{display:flex}.section{flex:1;width:96%;padding:10px;margin:8px;text-align:left;background:#fff;border-radius:8px;}.small-index{padding:10px;color:gray}.small-index-form{width:100%;text-align:center}#tagcloud{width:95%;padding:10px;text-align:center;margin:auto}#tagcloud a:link,#tagcloud a:visited{text-decoration:none}#tagcloud a:hover{text-decoration:underline}#tagcloud span{padding:10px}#tagcloud .smallest{font-size:70%}#tagcloud .small{font-size:80%}#tagcloud .medium{font-size:110%}#tagcloud .large{font-size:150%}#tagcloud .largest{font-size:170%}@media (max-width:768px){#container{width:auto;clear:both;height:auto;padding:5px;margin:55px 10px 5px 0}.container{flex-direction:column}}h1.code{font-size:34px;font-weight:500;text-align:center}h2.code{font-size:28px;font-weight:500;margin-top:20px;margin-bottom:0}ul.ndx-list{columns:4}ul.rght-list{columns:2}ul.causes{padding-left:10px;margin:0}#wrapper{margin:0 auto;padding:0;border:0;width:100%}#container{width:1140px;margin:auto;clear:both;margin-top:55px}#content{width:780px;float:left}#rightcolumn{width:320px;float:left;margin:0 0 20px 20px}.hamburger{display:inline;float:left;font-size:30px;text-decoration:none;margin:0 25px 0 10px;cursor:pointer;transform:scale(1.2,0.8)}.sidebar{padding:10px;margin:0}.sidebar > li{font-size:14px;list-style:none;margin:6px 0 6px 2px;padding:6px 0 6px 2px}.sidebar a{text-decoration:none}.close-sidebar{font-size:1.5em;padding-left:5px}.site-name{display:inline-block}.headerbar{width:100%;height:40px;background:#fff;position:fixed;top:0;left:0;z-index:2;padding:3px 0;text-align:left;text-overflow:clip;box-shadow:0 2px 10px 0 rgba(0,0,0,0.35)}.navlinks{display:inline-block;padding:0 4px 20px;vertical-align:middle}.navform{display:inline-block;float:right;margin-right:30px}.info{width:98%;padding:10px;margin:8px;background:#fff;border-radius:8px;text-align:left;line-height:1.5}.info li{list-style:disc;margin-left:15px;text-indent:-2px;position:relative}.ad{width:98%;padding:5px;margin:10px auto;text-align:center;clear:both}.search_box{border:1px #CCC solid;height:28px;width:220px;color:#999;padding:2px;vertical-align:top;margin:3px}.form_input{width:200px;height:25px;border:1px #0079D3 solid;padding:3px}.views{font-size:12px;color:#666;font-weight:400}.views a:link,.views a:visited,.views a:hover{font-size:12px;text-decoration:none}.search-descpt{font-size:12px;color:#000;font-weight:400}.details{height:36px;background-color:#0079D3;font:normal normal normal Arial;color:#fff;position:relative;text-align:center;padding:5px 8px;border:0;border-radius:4px;cursor:pointer}.details:hover{height:36px;background-color:#0079D3;opacity:0.5;font:normal normal normal Arial;color:#fff;position:relative;text-align:center;padding:5px 8px;border:0;border-radius:4px;cursor:pointer}.details a:link,.details a:visited,.details a:hover,.details a:active{height:36px;color:#FFF;text-decoration:none;font:normal normal normal Arial;cursor:pointer}.rating_small{display:none}.busca{display:none}.menus a{display:inline;float:left;font-size:30px;text-decoration:none;margin:0 25px 0 10px;cursor:pointer;transform:scale(1.2,0.8)}.footer-list{width:97%;padding:10px;margin:8px;background:#fff;border-radius:8px;text-align:left}.footer-list ul{columns:2}.footer-list li{padding:5px;margin:3px}@media only screen and (max-width: 768px){#container{width:auto;margin:55px 20px 5px 0}#content{width:auto;float:none}#rightcolumn{display:none}ul.ndx-list{columns:2}.info{width:96%}#scroller li{width:96%}.footer-list{width:96%}.busca{display:inline;float:right;margin:10px;opacity:.8}h1.code{font-size:32px;font-weight:500;text-align:center}h2.code{font-size:26px;font-weight:500;margin-top:10px}.rating_small{display:block;width:95%;margin:auto;padding:2px;margin:5px}.rating_small_1{font-size:125%}.navform{display:none}}</style></head>
<body>
<amp-analytics type="googleanalytics">
<script type="application/json">
{
 "vars": {
 "account": "UA-373412-35"
 },
 "triggers": {
 "trackPageview": {
 "on": "visible",
 "request": "pageview"
 }
 }
}
</script>
</amp-analytics>
<header class="headerbar">
<div class="hamburger" on="tap:sidebar1.toggle" role="button" tabindex="0">☰</div>
<div class="site-name" style="margin-top:7px"><a href="https://www.autocodes.com" title="OBDII Trouble Codes Definition, Description and Repair Information | AutoCodes.com">
<amp-img alt="AutoCodes.com | All About OBDII Engine Codes" height="25" src="https://www.autocodes.com/images/logo.webp" width="170"></amp-img></a></div>
<div class="navlinks" style="margin-top:8px">
<a href="https://www.autocodes.com/shops/index.php" title="Local Automotive Repair Shops | AutoCodes.com">Find a Shop</a>
</div>
<div class="navform">
<form action="https://www.autocodes.com/search_codes.php" method="GET" target="_top">
<input aria-label="Enter OBDII Code Number" class="form_input" name="q" style="width:150px;margin:0px 10px 10px 0px" type="text"/>
<input aria-label="Search" class="details" type="submit" value="Search"/>
</form>
</div>
</header>
<amp-sidebar id="sidebar1" layout="nodisplay" side="left">
<div aria-label="close sidebar" class="close-sidebar" on="tap:sidebar1.toggle" role="button" tabindex="0">✕</div>
<ul class="sidebar">
<li>
<form action="https://www.autocodes.com/search_codes.php" method="GET" target="_top">
<input aria-label="Enter OBDII Code Number" class="form_input" name="q" style="width:150px;margin:0px 10px 10px 0px" type="text"/><br/>
<input aria-label="Search" class="details" type="submit" value="Search"/>
</form>
</li>
<li><a href="https://www.autocodes.com/search.php" title="OBDII Codes | AutoCodes.com">Search Codes</a></li>
<li><a href="https://www.autocodes.com/shops/index.php" title="Find Local Automotive Repair Shops | AutoCodes.com">Find a Shop</a></li>
<li><a href="https://www.autocodes.com/challenge/index.php" title="Automotive Test Game | AutoCodes.com">Auto Test Game</a></li>
<li><a href="https://www.autocodes.com/glossary/index.php" title="Glossary of Automotive Terms and Acronyms | AutoCodes.com">Glossary</a></li>
<li><a href="https://www.autocodes.com/articles/index.php" title="OBDII Articles | AutoCodes.com">OBDII Components</a></li>
<li><a href="https://www.autocodes.com/qa/index.php" title="AutoCodes.com Questions and Answers">Questions</a></li>
<li><a href="https://www.autocodes.com/warning/index.php" title="Automotive Warning Light | AutoCodes.com Warning Lights">Warning Lights</a></li>
<li><a href="https://www.autocodes.com/biz/index.php" title="Find and Claim yout Repair Shop AutoCodes Page | AutoCodes.com">For Shop Owners</a></li>
</ul>
</amp-sidebar><div id="wrapper">
<div id="container">
<!-- Content Starts -->
<h1 class="code">OBDII Codes Engine Light Definitions, Diagnostic, Description &amp; Repair Information</h1>
<div class="section" style="background-color:#E9EBEE">
<amp-img alt="OBD2-OBDII Engine Light Trouble Codes Definitions, Description and Repair Information | Engine-Codes.com" height="438" layout="responsive" src="https://www.autocodes.com/images/background.jpg" width="970"></amp-img>
</div>
<div class="container">
<div class="section">
<h3 class="code">Search OBDII Codes</h3>
<div class="small-index">Get OBDII code definitions, description and repair information</div>
<div class="small-index-form">
<form action="https://www.autocodes.com/search_codes.php" method="GET" name="form" target="_top">
<span style="font-size:90%; color:#666666">e.g P0420 - P0455 - P1320</span><br/>
<input aria-label="Enter OBDII Code Number" class="form_input" name="q" style="font-size: 15px; margin-bottom:15px; width:80%" type="text"/>
<input aria-label="Search Codes" class="details" type="submit" value="Search Engine-Codes"/>
</form>
</div>
</div>
<div class="section">
<h3 class="code">Find a Shop</h3>
<div class="small-index">Having problems fixing the engine code? Find a repair shop near you</div>
<div class="small-index-form">
<form action="https://www.autocodes.com/shops/search_shops.php" method="GET" name="form1" target="_top">
<span style="font-size:90%; color:#666666">city, state, zip or name</span><br/>
<input aria-label="Enter Zip Code, City or Shop Name" class="form_input" name="q" style="font-size: 15px; margin-bottom:15px; width:80%" type="text"/>
<input aria-label="Search Repair Shops" class="details" type="submit" value="Search Local Shops"/>
</form>
</div>
</div>
<div class="section">
<h3 class="code">Ask a Question</h3>
<div class="small-index">Get free answers from our OBDII expert community</div>
<div class="small-index-form">
<form action-xhr="https://www.autocodes.com/qa/ask" method="POST" target="_top">
<textarea aria-label="Enter OBDII Question" name="title" rows="2" style="color: #999; font-size: 15px; margin-bottom:15px; width:80%"></textarea><br/>
<input aria-label="Ask A Question" class="details" name="doask1" type="submit" value="Ask Question"/>
</form>
</div>
</div>
</div>
<div class="info">
<h2 class="code">Popular Codes</h2>
<div id="tagcloud">
<span class="medium">
<a href="https://www.autocodes.com/p0420_nissan.html" title="OBDII Code P0420 NISSAN - EVAP Control System Gross Leak Detected | AutoCodes.com">P0420 NISSAN</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p0171.html" title="OBDII Code P0171 - EVAP Control System Gross Leak Detected | AutoCodes.com">P0171</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p0300_chevrolet.html" title="OBDII Code P0300 CHEVROLET - EVAP Control System Gross Leak Detected | AutoCodes.com">P0300 CHEVROLET</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p0420_honda.html" title="OBDII Code P0420 HONDA - EVAP Control System Gross Leak Detected | AutoCodes.com">P0420 HONDA</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p0171_chevrolet.html" title="OBDII Code P0171 CHEVROLET - EVAP Control System Gross Leak Detected | AutoCodes.com">P0171 CHEVROLET</a>
</span>
<span class="small">
<a href="https://www.autocodes.com/p0455_nissan.html" title="OBDII Code P0455 NISSAN - EVAP Control System Gross Leak Detected | AutoCodes.com">P0455 NISSAN</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p1320_nissan.html" title="OBDII Code P1320 NISSAN - EVAP Control System Gross Leak Detected | AutoCodes.com">P1320 NISSAN</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p0420_toyota.html" title="OBDII Code P0420 TOYOTA - EVAP Control System Gross Leak Detected | AutoCodes.com">P0420 TOYOTA</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/u1000_nissan.html" title="OBDII Code U1000 NISSAN - EVAP Control System Gross Leak Detected | AutoCodes.com">U1000 NISSAN</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p0340_nissan.html" title="OBDII Code P0340 NISSAN - EVAP Control System Gross Leak Detected | AutoCodes.com">P0340 NISSAN</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p0171_ford.html" title="OBDII Code P0171 FORD - EVAP Control System Gross Leak Detected | AutoCodes.com">P0171 FORD</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p0300_nissan.html" title="OBDII Code P0300 NISSAN - EVAP Control System Gross Leak Detected | AutoCodes.com">P0300 NISSAN</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p0420.html" title="OBDII Code P0420 - EVAP Control System Gross Leak Detected | AutoCodes.com">P0420</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p0340.html" title="OBDII Code P0340 - EVAP Control System Gross Leak Detected | AutoCodes.com">P0340</a>
</span>
<span class="largest">
<a href="https://www.autocodes.com/p1273_nissan.html" title="OBDII Code P1273 NISSAN - EVAP Control System Gross Leak Detected | AutoCodes.com">P1273 NISSAN</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p0171_toyota.html" title="OBDII Code P0171 TOYOTA - EVAP Control System Gross Leak Detected | AutoCodes.com">P0171 TOYOTA</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p0430.html" title="OBDII Code P0430 - EVAP Control System Gross Leak Detected | AutoCodes.com">P0430</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p0455_ford.html" title="OBDII Code P0455 FORD - EVAP Control System Gross Leak Detected | AutoCodes.com">P0455 FORD</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p0300.html" title="OBDII Code P0300 - EVAP Control System Gross Leak Detected | AutoCodes.com">P0300</a>
</span>
<span class="medium">
<a href="https://www.autocodes.com/p0011_nissan.html" title="OBDII Code P0011 NISSAN - EVAP Control System Gross Leak Detected | AutoCodes.com">P0011 NISSAN</a>
</span>
</div>
</div>
<div class="info"><h2 class="code">Find Codes By Make</h2><ul class="ndx-list"><li><a href="https://www.autocodes.com/search_make.php?make=acura" title="Acura OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Acura</a></li><li><a href="https://www.autocodes.com/search_make.php?make=audi" title="Audi OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Audi</a></li><li><a href="https://www.autocodes.com/search_make.php?make=bmw" title="BMW OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">BMW</a></li><li><a href="https://www.autocodes.com/search_make.php?make=buick" title="Buick OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Buick</a></li><li><a href="https://www.autocodes.com/search_make.php?make=cadillac" title="Cadillac OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Cadillac</a></li><li><a href="https://www.autocodes.com/search_make.php?make=chevrolet" title="Chevrolet OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Chevrolet</a></li><li><a href="https://www.autocodes.com/search_make.php?make=chrysler" title="Chrysler OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Chrysler</a></li><li><a href="https://www.autocodes.com/search_make.php?make=dodge" title="Dodge OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Dodge</a></li><li><a href="https://www.autocodes.com/search_make.php?make=eagle" title="Eagle OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Eagle</a></li><li><a href="https://www.autocodes.com/search_make.php?make=ford" title="Ford OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Ford</a></li><li><a href="https://www.autocodes.com/search_make.php?make=geo" title="GEO OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">GEO</a></li><li><a href="https://www.autocodes.com/search_make.php?make=gmc" title="GMC OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">GMC</a></li><li><a href="https://www.autocodes.com/search_make.php?make=honda" title="Honda OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Honda</a></li><li><a href="https://www.autocodes.com/search_make.php?make=hummer" title="Hummer OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Hummer</a></li><li><a href="https://www.autocodes.com/search_make.php?make=hyundai" title="Hyundai OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Hyundai</a></li><li><a href="https://www.autocodes.com/search_make.php?make=infiniti" title="Infiniti OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Infiniti</a></li><li><a href="https://www.autocodes.com/search_make.php?make=isuzu" title="Isuzu OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Isuzu</a></li><li><a href="https://www.autocodes.com/search_make.php?make=jaguar" title="Jaguar OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Jaguar</a></li><li><a href="https://www.autocodes.com/search_make.php?make=jeep" title="Jeep OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Jeep</a></li><li><a href="https://www.autocodes.com/search_make.php?make=kia" title="KIA OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">KIA</a></li><li><a href="https://www.autocodes.com/search_make.php?make=land-rover" title="Land Rover OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Land Rover</a></li><li><a href="https://www.autocodes.com/search_make.php?make=lexus" title="Lexus OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Lexus</a></li><li><a href="https://www.autocodes.com/search_make.php?make=lincoln" title="Lincoln OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Lincoln</a></li><li><a href="https://www.autocodes.com/search_make.php?make=mazda" title="Mazda OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Mazda</a></li><li><a href="https://www.autocodes.com/search_make.php?make=mercedes-benz" title="Mercedes-Benz OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Mercedes-Benz</a></li><li><a href="https://www.autocodes.com/search_make.php?make=mercury" title="Mercury OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Mercury</a></li><li><a href="https://www.autocodes.com/search_make.php?make=mini" title="MINI OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">MINI</a></li><li><a href="https://www.autocodes.com/search_make.php?make=mitsubishi" title="Mitsubishi OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Mitsubishi</a></li><li><a href="https://www.autocodes.com/search_make.php?make=nissan" title="Nissan OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Nissan</a></li><li><a href="https://www.autocodes.com/search_make.php?make=oldsmobile" title="Oldsmobile OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Oldsmobile</a></li><li><a href="https://www.autocodes.com/search_make.php?make=pontiac" title="Pontiac OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Pontiac</a></li><li><a href="https://www.autocodes.com/search_make.php?make=ram" title="RAM OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">RAM</a></li><li><a href="https://www.autocodes.com/search_make.php?make=saab" title="Saab OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Saab</a></li><li><a href="https://www.autocodes.com/search_make.php?make=saturn" title="Saturn OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Saturn</a></li><li><a href="https://www.autocodes.com/search_make.php?make=scion" title="Scion OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Scion</a></li><li><a href="https://www.autocodes.com/search_make.php?make=subaru" title="Subaru OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Subaru</a></li><li><a href="https://www.autocodes.com/search_make.php?make=suzuki" title="Suzuki OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Suzuki</a></li><li><a href="https://www.autocodes.com/search_make.php?make=toyota" title="Toyota OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Toyota</a></li><li><a href="https://www.autocodes.com/search_make.php?make=volkswagen" title="Volkswagen OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Volkswagen</a></li><li><a href="https://www.autocodes.com/search_make.php?make=volvo" title="Volvo OBD2-OBDII Codes Definition, Description and Repair Information | AutoCodes.com">Volvo</a></li></ul></div><div class="info"><h2 class="code">Find Repair Shops by State</h2><ul class="ndx-list"><li><a href="https://www.autocodes.com/shops/state.php?state=al" title="Alabama Local Repair Shops">Alabama</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=ak" title="Alaska Local Repair Shops">Alaska</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=ar" title="Arkansas Local Repair Shops">Arkansas</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=az" title="Arizona Local Repair Shops">Arizona</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=ca" title="California Local Repair Shops">California</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=co" title="Colorado Local Repair Shops">Colorado</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=ct" title="Connecticut Local Repair Shops">Connecticut</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=dc" title="Washington DC Local Repair Shops">Washington DC</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=de" title="Delaware Local Repair Shops">Delaware</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=fl" title="Florida Local Repair Shops">Florida</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=ga" title="Georgia Local Repair Shops">Georgia</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=hi" title="Hawaii Local Repair Shops">Hawaii</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=ia" title="Iowa Local Repair Shops">Iowa</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=id" title="Idaho Local Repair Shops">Idaho</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=il" title="Illinois Local Repair Shops">Illinois</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=in" title="Indiana Local Repair Shops">Indiana</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=ks" title="Kansas Local Repair Shops">Kansas</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=ky" title="Kentucky Local Repair Shops">Kentucky</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=la" title="Louisiana Local Repair Shops">Louisiana</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=ma" title="Massachusetts Local Repair Shops">Massachusetts</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=md" title="Maryland Local Repair Shops">Maryland</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=me" title="Maine Local Repair Shops">Maine</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=mi" title="Michigan Local Repair Shops">Michigan</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=mn" title="Minnesota Local Repair Shops">Minnesota</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=mo" title="Missouri Local Repair Shops">Missouri</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=ms" title="Mississippi Local Repair Shops">Mississippi</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=mt" title="Montana Local Repair Shops">Montana</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=nc" title="North Carolina Local Repair Shops">North Carolina</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=nd" title="North Dakota Local Repair Shops">North Dakota</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=ne" title="Nebraska Local Repair Shops">Nebraska</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=nh" title="New Hampshire Local Repair Shops">New Hampshire</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=nj" title="New Jersey Local Repair Shops">New Jersey</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=nm" title="New Mexico Local Repair Shops">New Mexico</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=nv" title="Nevada Local Repair Shops">Nevada</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=ny" title="New York Local Repair Shops">New York</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=oh" title="Ohio Local Repair Shops">Ohio</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=ok" title="Oklahoma Local Repair Shops">Oklahoma</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=on" title="Oklahoma Local Repair Shops">Oklahoma</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=or" title="Oregon Local Repair Shops">Oregon</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=pa" title="Pennsylvania Local Repair Shops">Pennsylvania</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=ri" title="Rhode Island Local Repair Shops">Rhode Island</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=sc" title="South Carolina Local Repair Shops">South Carolina</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=sd" title="South Dakota Local Repair Shops">South Dakota</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=tn" title="Tennessee Local Repair Shops">Tennessee</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=tx" title="Texas Local Repair Shops">Texas</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=ut" title="Utah Local Repair Shops">Utah</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=va" title="Virginia Local Repair Shops">Virginia</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=vt" title="Vermont Local Repair Shops">Vermont</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=wa" title="Washington Local Repair Shops">Washington</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=wi" title="Wisconsin Local Repair Shops">Wisconsin</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=wv" title="West Virginia Local Repair Shops">West Virginia</a></li><li><a href="https://www.autocodes.com/shops/state.php?state=wy" title="Wyoming Local Repair Shops">Wyoming</a></li></ul></div>
<div class="ad"><amp-ad data-ad-client="ca-pub-3579096038790910" data-ad-slot="1336275660" data-auto-format="rspv" data-full-width="" height="320" type="adsense" width="100vw">
<div overflow=""></div>
</amp-ad></div>
<div class="footer-list"><h2 class="code">Related Information</h2><ul><li><a href="https://www.autocodes.com/articles/1/what-is-obdii.html" title="What is OBDII? Information, Description, Diagnose | AutoCodes.com">What is OBDII?</a></li><li><a href="https://www.autocodes.com/articles/2/how-does-the-obdii-works.html" title="How does the OBDII works? Information, Description, Diagnose | AutoCodes.com">How does the OBDII works?</a></li><li><a href="https://www.autocodes.com/articles/4/why-is-the-engine-light-on.html" title="Why is the Engine Light ON? Information, Description, Diagnose | AutoCodes.com">Why is the Engine Light ON?</a></li><li><a href="https://www.autocodes.com/articles/31/how-to-reset-the-check-engine-light.html" title="How to Reset the Check Engine Light Information, Description, Diagnose | AutoCodes.com">How to Reset MIL</a></li><li><a href="https://www.autocodes.com/articles/34/5-most-common-reasons-for-a-check-engine-light.html" title="5 Most Common Reasons for a Check Engine Light | AutoCodes.com">5 Most Common Reasons for Check Engine Light</a></li><li><a href="https://www.autocodes.com/popular_codes.php" title="Top 25 Searched OBDII Engine Codes | AutoCodes.com">Top 25 Engine Codes</a></li><li><a href="https://www.autocodes.com/recently_updated.php" title="Latest 15 Codes Updated | AutoCodes.com">Latest 15 Codes Updated</a></li><li><a href="https://www.autocodes.com/all-codes.php" title="All OBD2-OBDII List Codes Definition, Description and Repair Information | AutoCodes.com">All OBDII Codes</a></li><li><a href="https://www.autocodes.com/obd-code-list/powertrain" title="Powertrain OBD2-OBDII Codes List Definition, Description and Repair Information | AutoCodes.com">Powertrain (P) Codes</a></li><li><a href="https://www.autocodes.com/obd-code-list/network" title="Network OBD2-OBDII Codes List Definition, Description and Repair Information | AutoCodes.com">Network (U) Codes</a></li><li><a href="https://www.autocodes.com/obd-code-list/body" title="Body OBD2-OBDII Codes List Definition, Description and Repair Information | AutoCodes.com">Body (B) Codes</a></li><li><a href="https://www.autocodes.com/obd-code-list/chassis" title="Chassis OBD2-OBDII List Codes Definition, Description and Repair Information | AutoCodes.com">Chassis (C) Codes</a></li></ul></div><div class="footer-list" style="background-color:#E9EBEE"><ul><li><a href="https://www.autocodes.com" title="OBDII Codes Engine Light Definitions Diagnostic Repair | AutoCodes">AutoCodes.com</a></li><li><a href="https://www.engine-codes.com" title="OBD2-OBDII Engine Light Trouble Codes Definitions Description Repair Information | Engine-Codes.com">Engine-Codes.com</a></li><li><a href="https://www.infinitihelp.com" title="Infinitihelp.com | All About Infiniti">Infinitihelp.com</a></li><li><a href="https://www.nissanhelp.com" title="Nissanhelp.com | All About Nissan">Nissanhelp.com</a></li><li><a href="https://www.autocodes.com/about/about.php" title="About AutoCodes.com | AutoCodes.com">About AutoCodes.com</a></li><li><a href="https://www.autocodes.com/about/privacy.php" title="Privacy Policy | AutoCodes.com">Privacy Policy</a></li><li><a href="https://www.autocodes.com/about/terms.php" title="Terms and Conditions | AutoCodes.com">Terms and Conditions</a></li><li>© 2010-2021 AutoCodes.com</li><li><span style="font-size:80%;">Posted by AutoCodes.com January 13, 2021</span></li></ul></div>
<!-- Content Ends -->
</div>
</div>
</body>
</html>