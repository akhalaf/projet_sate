<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "//www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html style="overflow-x: auto;" xmlns="//www.w3.org/1999/xhtml">
<head>
<title>人道至尊_人道至尊最新章节目录_笔趣阁</title>
<meta content="人道至尊,笔趣阁" name="keywords"/>
<meta content="笔趣阁，书友最值得收藏的网络小说阅读网" name="description"/>
<meta content="" name="copyright"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<link href="/favicon.ico" rel="shortcut icon"/>
<link href="//m.boquge.com/" media="handheld" rel="alternate" type="application/vnd.wap.xhtml+xml"/>
<meta content="format=html5; url=//m.boquge.com/" name="mobile-agent"/>
<script type="text/javascript">var murl="//m.boquge.com/";</script>
<meta content="no-siteapp" http-equiv="Cache-Control"/>
<meta content="no-transform" http-equiv="Cache-Control"/>
<link href="//www.boquge.com/css/b.css" rel="stylesheet"/>
<link href="//www.boquge.com/css/m.css" rel="stylesheet"/>
<script src="//apps.bdimg.com/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script src="//apps.bdimg.com/libs/jquery.cookie/1.4.1/jquery.cookie.min.js" type="text/javascript"></script>
<script src="//www.boquge.com/main.js?v=1" type="text/javascript"></script>
</head>
<body id="btop-info">
<div class="container">
<nav class="navbar navbar-default col-xs-12 pl20" role="navigation">
<div class="navbar-header"><a class="logo" href="//www.boquge.com" title="笔趣阁"><img src="http://www.boquge.com/images/logo.gif"/></a></div>
<div>
<ul class="nav navbar-nav">
<li><a href="//www.boquge.com/shuku.htm">分类</a></li>
<li><a href="//www.boquge.com/search.htm" rel="nofollow">搜索</a></li>
<li><a href="//www.boquge.com/zuji.htm" rel="nofollow">足迹</a></li>
<li><a href="//www.boquge.com/home/" rel="nofollow">我的书架</a></li>
<li id="login"><a href="//www.boquge.com/login.htm" rel="nofollow">登陆</a></li>
<li id="regist"><a href="//www.boquge.com/register.php" rel="nofollow">注册</a></li>
<li id="logout" style="display:none;"><a href="//www.boquge.com/home/logout.htm" rel="nofollow">退出登陆</a></li>
<li style="width:300px;margin-top:10px;"><form action="/search.htm" class="" onsubmit="return doFormSubmit();" role="form">
<div class="input-group"><input class="form-control" id="keyword" name="keyword" placeholder="请输入书名或作者" type="text"/><span class="input-group-addon" onclick="doFormSubmit();">搜索</span>
</div>
</form>
</li>
</ul>
</div>
</nav>
<section class="col-xs-12">
<div class="panel panel-success">
<div class="panel-heading"><h3 class="panel-title">站长推荐</h3></div>
<div class="panel-body">
<ul class="img_list "><li><a href="/book/4043/"><img alt="银狐" class="img-thumbnail" src="http://r.boquge.com/files/article/image/4/4043/4043s.jpg"/><span>银狐</span></a></li><li><a href="/book/1/"><img alt="人道至尊" class="img-thumbnail" src="http://r.boquge.com/files/article/image/0/1/1s.jpg"/><span>人道至尊</span></a></li><li><a href="/book/4049/"><img alt="玄界之门" class="img-thumbnail" src="http://r.boquge.com/files/article/image/4/4049/4049s.jpg"/><span>玄界之门</span></a></li><li><a href="/book/3390/"><img alt="永恒剑主" class="img-thumbnail" src="http://r.boquge.com/files/article/image/3/3390/3390s.jpg"/><span>永恒剑主</span></a></li><li><a href="/book/2000/"><img alt="步步惊唐" class="img-thumbnail" src="http://r.boquge.com/files/article/image/2/2000/2000s.jpg"/><span>步步惊唐</span></a></li><li><a href="/book/76671/"><img alt="伏天氏" class="img-thumbnail" src="http://r.boquge.com/cover/aHR0cDovL2Njc3RhdGljLTEyNTIzMTc4MjIuZmlsZS5teXFjbG91ZC5jb20vYm9va2NvdmVyaW1nLzIwMTgtMDEtMDUvNWE0ZjI3YWRhYWI1Mi5qcGc="/><span>伏天氏</span></a></li><li><a href="/book/1180/"><img alt="绝世天君" class="img-thumbnail" src="http://r.boquge.com/files/article/image/1/1180/1180s.jpg"/><span>绝世天君</span></a></li><li><a href="/book/3552/"><img alt="超级灵泉" class="img-thumbnail" src="http://r.boquge.com/files/article/image/3/3552/3552s.jpg"/><span>超级灵泉</span></a></li><li><a href="/book/25/"><img alt="异度" class="img-thumbnail" src="http://r.boquge.com/files/article/image/0/25/25s.jpg"/><span>异度</span></a></li></ul>
</div>
</div>
</section>
<section class="col-xs-12">
<div class="panel panel-success">
<div class="panel-heading"><h3 class="panel-title">书友推荐</h3></div>
<div class="panel-body">
<ul class="img_list"><li><a href="/book/1666/"><img alt="赘婿" class="img-thumbnail" src="http://r.boquge.com/files/article/image/1/1666/1666s.jpg"/><span>赘婿</span></a></li><li><a href="/book/1667/"><img alt="龙王戒" class="img-thumbnail" src="http://r.boquge.com/files/article/image/1/1667/1667s.jpg"/><span>龙王戒</span></a></li><li><a href="/book/247/"><img alt="完美世界" class="img-thumbnail" src="http://r.boquge.com/files/article/image/0/247/247s.jpg"/><span>完美世界</span></a></li><li><a href="/book/420/"><img alt="巨星夫妻" class="img-thumbnail" src="http://r.boquge.com/files/article/image/0/420/420s.jpg"/><span>巨星夫妻</span></a></li><li><a href="/book/944/"><img alt="永夜君王" class="img-thumbnail" src="http://r.boquge.com/files/article/image/0/944/944s.jpg"/><span>永夜君王</span></a></li><li><a href="/book/1180/"><img alt="绝世天君" class="img-thumbnail" src="http://r.boquge.com/files/article/image/1/1180/1180s.jpg"/><span>绝世天君</span></a></li><li><a href="/book/1058/"><img alt="真武世界" class="img-thumbnail" src="http://r.boquge.com/files/article/image/1/1058/1058s.jpg"/><span>真武世界</span></a></li><li><a href="/book/4351/"><img alt="新大明帝国" class="img-thumbnail" src="http://r.boquge.com/files/article/image/4/4351/4351s.jpg"/><span>新大明帝国</span></a></li><li><a href="/book/4526/"><img alt="我的时空之门" class="img-thumbnail" src="http://r.boquge.com/files/article/image/4/4526/4526s.jpg"/><span>我的时空之门</span></a></li></ul>
</div>
</div>
</section>
<section class="pr10 col-xs-4">
<div class="panel panel-info index-category-qk">
<div class="panel-heading"><h3 class="panel-title">玄幻</h3></div>
<div class="panel-body">
<dl>
<dt><a href="/xs/84409/index.html"><img alt="诡秘之主" src="http://r.boquge.com/cover/aHR0cDovL3FpZGlhbi5xcGljLmNuL3FkYmltZy8zNDk1NzMvMTAxMDg2ODI2NC8xODA="/></a></dt>
<dd>
<h3><a class="text-nowrap modal-open" href="/xs/84409/index.html">诡秘之主</a></h3>
<p>蒸汽与机械的浪潮中，谁能触及非凡？历史和黑暗的迷雾里，又是谁在耳语？我从诡秘中醒来，睁眼看见这个世界：枪械，大炮，巨舰，飞空艇，差分机；魔药，占卜，诅咒，倒吊...</p>
</dd>
</dl>
<ul class="book_textList">
<li><i class="tag-blue">玄幻</i><a href="/xs/1/index.html">人道至尊</a>　/　宅猪</li>
<li><i class="tag-blue">玄幻</i><a href="/xs/247/index.html">完美世界</a>　/　辰东</li>
<li><i class="tag-blue">玄幻</i><a href="/xs/46773/index.html">斗破苍穹</a>　/　天蚕土豆</li>
<li><i class="tag-blue">玄幻</i><a href="/xs/4622/index.html">万古神帝</a>　/　飞天鱼</li>
<li><i class="tag-blue">玄幻</i><a href="/xs/76671/index.html">伏天氏</a>　/　净无痕</li>
<li><i class="tag-blue">玄幻</i><a href="/xs/3339/index.html">全职法师</a>　/　乱(书坊)</li>
<li><i class="tag-blue">玄幻</i><a href="/xs/414/index.html">不灭龙帝</a>　/　妖夜</li>
</ul>
<div class="listMore"><a href="http://www.boquge.com/xuanhuanxiaoshuo/">更多玄幻小说精品作品&gt;&gt;</a></div>
</div>
</div>
</section>
<section class="col-xs-4 pr10">
<div class="panel panel-info index-category-qk">
<div class="panel-heading"><h3 class="panel-title">奇幻</h3></div>
<div class="panel-body">
<dl>
<dt><a href="/xs/26136/index.html"><img alt="放开那个女巫" src="http://r.boquge.com/cover/aHR0cDovL2ltYWdlLmNtZnUuY29tL2Jvb2tzLzEwMDMzMDY4MTEvMTAwMzMwNjgxMS5qcGc="/></a></dt>
<dd>
<h3><a class="text-nowrap modal-open" href="/xs/26136/index.html">放开那个女巫</a></h3>
<p>程岩原以为穿越到了欧洲中世纪，成为了一位光荣的王子。但这世界似乎跟自己想的不太一样？女巫真实存在，而且还真具有魔力？女巫种田文，将种田进行到底。</p>
</dd>
</dl>
<ul class="book_textList">
<li><i class="tag-blue">奇幻</i><a href="/xs/47406/index.html">神秘之旅</a>　/　滚开</li>
<li><i class="tag-blue">奇幻</i><a href="/xs/944/index.html">永夜君王</a>　/　烟雨江南</li>
<li><i class="tag-blue">奇幻</i><a href="/xs/43844/index.html">天王主播</a>　/　懵懂的大狗熊</li>
<li><i class="tag-blue">奇幻</i><a href="/xs/30003/index.html">暴风法神</a>　/　余云飞</li>
<li><i class="tag-blue">奇幻</i><a href="/xs/39298/index.html">穿越之武侠群芳谱</a>　/　金宇峯</li>
<li><i class="tag-blue">奇幻</i><a href="/xs/54126/index.html">重生之都市狂仙</a>　/　醒灯</li>
<li><i class="tag-blue">奇幻</i><a href="/xs/45269/index.html">异界大村长</a>　/　太平王</li>
</ul>
<div class="listMore"><a href="http://www.boquge.com/qihuanxiaoshuo/">更多奇幻小说精品作品&gt;&gt;</a></div>
</div>
</div>
</section>
<section class="col-xs-4">
<div class="panel panel-info index-category-qk">
<div class="panel-heading"><h3 class="panel-title">仙侠</h3></div>
<div class="panel-body">
<dl>
<dt><a href="/xs/4049/index.html"><img alt="玄界之门" src="http://r.boquge.com/files/article/image/4/4049/4049s.jpg"/></a></dt>
<dd>
<h3><a class="text-nowrap modal-open" href="/xs/4049/index.html">玄界之门</a></h3>
<p>天降神物！异血附体！群仙惊惧！万魔退避！一名从东洲大陆走出的少年。一具生死相依的红粉骷髅。一个立志成为至强者的故事。一段叱咤星河，大闹三界的传说。忘语新书，已...</p>
</dd>
</dl>
<ul class="book_textList">
<li><i class="tag-blue">仙侠</i><a href="/xs/121554/index.html">大奉打更人</a>　/　卖报小郎君</li>
<li><i class="tag-blue">仙侠</i><a href="/xs/68760/index.html">最强反套路系统</a>　/　太上布衣</li>
<li><i class="tag-blue">仙侠</i><a href="/xs/699/index.html">造化之门</a>　/　鹅是老五</li>
<li><i class="tag-blue">仙侠</i><a href="/xs/87013/index.html">三寸人间</a>　/　耳根</li>
<li><i class="tag-blue">仙侠</i><a href="/xs/29714/index.html">不朽凡人</a>　/　鹅是老五</li>
<li><i class="tag-blue">仙侠</i><a href="/xs/258/index.html">一念永恒</a>　/　耳根</li>
<li><i class="tag-blue">仙侠</i><a href="/xs/14078/index.html">万道独尊</a>　/　魂圣</li>
</ul>
<div class="listMore"><a href="http://www.boquge.com/xianxiaxiaoshuo/">更多仙侠小说精品作品&gt;&gt;</a></div>
</div>
</div>
</section>
<div class="clearfix"></div>
<section class="pr10 col-xs-4">
<div class="panel panel-info index-category-qk">
<div class="panel-heading"><h3 class="panel-title">都市</h3></div>
<div class="panel-body">
<dl>
<dt><a href="/xs/98617/index.html"><img alt="手术直播间" src="http://r.boquge.com/cover/aHR0cDovL2Jvb2tjb3Zlci55dWV3ZW4uY29tL3FkYmltZy8zNDk1NzMvMTAxMzQxNDkyOS8xODA="/></a></dt>
<dd>
<h3><a class="text-nowrap modal-open" href="/xs/98617/index.html">手术直播间</a></h3>
<p>一个外科的小医生，一不小心得到了系统加持，横扫医学界，妙手回春，活人无数。</p>
</dd>
</dl>
<ul class="book_textList">
<li><i class="tag-blue">都市</i><a href="/xs/87073/index.html">全球高武</a>　/　老鹰吃小鸡</li>
<li><i class="tag-blue">都市</i><a href="/xs/69085/index.html">大王饶命</a>　/　会说话的肘子</li>
<li><i class="tag-blue">都市</i><a href="/xs/31747/index.html">重生之都市修仙</a>　/　十里剑神</li>
<li><i class="tag-blue">都市</i><a href="/xs/28036/index.html">极品仙帝在花都</a>　/　女王娟姐</li>
<li><i class="tag-blue">都市</i><a href="/xs/11962/index.html">绝品强少</a>　/　步履无声</li>
<li><i class="tag-blue">都市</i><a href="/xs/66958/index.html">我的姐姐是大明星</a>　/　卖报小郎君</li>
<li><i class="tag-blue">都市</i><a href="/xs/120030/index.html">万族之劫</a>　/　老鹰吃小鸡</li>
</ul>
<div class="listMore"><a href="http://www.boquge.com/dushixiaoshuo/">更多都市小说精品作品&gt;&gt;</a></div>
</div>
</div>
</section>
<section class="col-xs-4 pr10">
<div class="panel panel-info index-category-qk">
<div class="panel-heading"><h3 class="panel-title">言情</h3></div>
<div class="panel-body">
<dl>
<dt><a href="/xs/7874/index.html"><img alt="魅王宠妻：鬼医纨绔妃" src="http://r.boquge.com/cover/aHR0cDovL2ltYWdlLmNtZnUuY29tL2Jvb2tzLzM1OTYzNDQvMzU5NjM0NC5qcGc="/></a></dt>
<dd>
<h3><a class="text-nowrap modal-open" href="/xs/7874/index.html">魅王宠妻：鬼医纨绔妃</a></h3>
<p>她本是实力强悍，医术超群的世家家主。一朝穿越成将军府的废柴嫡小姐，成为第一位被退婚的太子妃，人人嘲讽！选秀宴上，她被赐嫁给鼎鼎有名的残废王爷。众人笑：瞎子配残...</p>
</dd>
</dl>
<ul class="book_textList">
<li><i class="tag-blue">言情</i><a href="/xs/23494/index.html">豪门少奶奶：谢少的心尖宠妻</a>　/　凤元糖果</li>
<li><i class="tag-blue">言情</i><a href="/xs/36338/index.html">随身空间：末世女穿七零</a>　/　醉雪逍遥</li>
<li><i class="tag-blue">言情</i><a href="/xs/53930/index.html">影后来袭：黑帝强势夺爱</a>　/　千萝绿</li>
<li><i class="tag-blue">言情</i><a href="/xs/53551/index.html">穿越八零之军妻养成计划</a>　/　阿窝</li>
<li><i class="tag-blue">言情</i><a href="/xs/112122/index.html">许你万丈光芒好</a>　/　囧囧有妖</li>
<li><i class="tag-blue">言情</i><a href="/xs/23162/index.html">绝宠妖妃：邪王，太闷骚！</a>　/　卡特琳娜</li>
<li><i class="tag-blue">言情</i><a href="/xs/10807/index.html">丑女种田：山里汉宠妻无度</a>　/　巅峰小雨</li>
</ul>
<div class="listMore"><a href="http://www.boquge.com/yanqingxiaoshuo/">更多言情小说精品作品&gt;&gt;</a></div>
</div>
</div>
</section>
<section class="col-xs-4">
<div class="panel panel-info index-category-qk">
<div class="panel-heading"><h3 class="panel-title">历史</h3></div>
<div class="panel-body">
<dl>
<dt><a href="/xs/4043/index.html"><img alt="银狐" src="http://r.boquge.com/files/article/image/4/4043/4043s.jpg"/></a></dt>
<dd>
<h3><a class="text-nowrap modal-open" href="/xs/4043/index.html">银狐</a></h3>
<p>人的第一要求就是活着，第二要求还是活着，第三要求依旧是活着……在某些特定的环境下，活着就成了一种奢望。在地狱中我们仰望天堂，把手伸出去，虽然不能触碰到天堂，却...</p>
</dd>
</dl>
<ul class="book_textList">
<li><i class="tag-blue">历史</i><a href="/xs/1666/index.html">赘婿</a>　/　愤怒的香蕉</li>
<li><i class="tag-blue">历史</i><a href="/xs/44224/index.html">大唐贞观第一纨绔</a>　/　危险的世界</li>
<li><i class="tag-blue">历史</i><a href="/xs/39924/index.html">带着仓库到大明</a>　/　迪巴拉爵士</li>
<li><i class="tag-blue">历史</i><a href="/xs/86295/index.html">明朝败家子</a>　/　上山打老虎额</li>
<li><i class="tag-blue">历史</i><a href="/xs/86875/index.html">如意小郎君</a>　/　荣小荣</li>
<li><i class="tag-blue">历史</i><a href="/xs/44529/index.html">开着外挂闯三国</a>　/　一蓑烟雨dj</li>
<li><i class="tag-blue">历史</i><a href="/xs/37026/index.html">钢铁皇朝</a>　/　背着家的蜗牛</li>
</ul>
<div class="listMore"><a href="http://www.boquge.com/lishixiaoshuo/">更多历史小说精品作品&gt;&gt;</a></div>
</div>
</div>
</section>
<div class="clearfix"></div>
<section class="panel panel-default col-xs-9">
<div class="panel-heading"><h3 class="panel-title">最新更新</h3></div>
<div class="panel-body" id="novel-list">
<ul><li class="list-group-item clearfix">
<div class="col-xs-1">类型</div>
<div class="col-xs-3">小说名称</div>
<div class="col-xs-4">最新章节</div>
<div class="col-xs-2">作者</div>
<div class="col-xs-2">更新时间</div>
</li>
<li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">言情</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/116365/">大月谣</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/116365/174602621.html">第二百一十八章 解忧</a></div>
<div class="col-xs-2">林树叶</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">其他</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/131971/">斗罗之我的武魂是自己未婚妻</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/131971/174599545.html">第二百八十四章、自欺欺人？联合？</a></div>
<div class="col-xs-2">芽月儿</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">游戏</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/132231/">扑街游戏</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/132231/174603195.html">236.沉阳趣事</a></div>
<div class="col-xs-2">偷偷尴尬</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">其他</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/132189/">霍格沃茨的训练家</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/132189/174602832.html">第226章 那究竟是什么？</a></div>
<div class="col-xs-2">青花云落</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">都市</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/129347/">我对钱没有兴趣了</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/129347/174601027.html">第386章 大年前夕</a></div>
<div class="col-xs-2">听樘</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">历史</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/129618/">大唐第一杠精</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/129618/174602628.html">第252章 后为前师敢叫黄袍加身</a></div>
<div class="col-xs-2">水鱼要吃素</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">科幻</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/127450/">暗黑野蛮人降临美漫</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/127450/174604292.html">360 勇气和希望</a></div>
<div class="col-xs-2">阡南望</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">科幻</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/131883/">重选人生系统</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/131883/174602638.html">42:相亲 （第六个世界起航!）</a></div>
<div class="col-xs-2">你为什么忧伤</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">游戏</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/129381/">末日拼图游戏</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/129381/174604293.html">卷末章：新队长与新副团长</a></div>
<div class="col-xs-2">更从心</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">游戏</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/131052/">电竞经理从保级赛开始</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/131052/174602624.html">第242章-还敢吃我分不？</a></div>
<div class="col-xs-2">一织小虎牙</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">玄幻</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/122933/">从氪金开始砍翻世界</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/122933/174602837.html">第740章 属性暴涨（求订阅）</a></div>
<div class="col-xs-2">许你风华绝代</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">游戏</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/128417/">网游之我是大团长</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/128417/174602667.html">070 《排面》（加更5/7）</a></div>
<div class="col-xs-2">启封的秘典</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">其他</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/131552/">篮坛巨石</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/131552/174602812.html">165 “砍蛋”战术</a></div>
<div class="col-xs-2">篮下推土机</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">科幻</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/112247/">维度魔神的代行者</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/112247/174602850.html">第580章 我可是要成为正义的伙伴啊</a></div>
<div class="col-xs-2">苏夜十三</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">武侠</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/129708/">金刚不坏大寨主</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/129708/174604285.html">400~401：超越天阶的功法，打出天牢！（保底更）</a></div>
<div class="col-xs-2">徍男</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">其他</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/115877/">雏田的武神强踢</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/115877/174599585.html">第469章 纲手好算计啊！</a></div>
<div class="col-xs-2">孤晨i</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">科幻</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/127511/">心灵学者</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/127511/174604280.html">第一百八十四章 服务费</a></div>
<div class="col-xs-2">稀饭悬浮猫</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">历史</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/113285/">大明最后一个狠人</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/113285/174599428.html">第1187章 下诏狱</a></div>
<div class="col-xs-2">大明第一帅</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">都市</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/119849/">重生之北国科技</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/119849/174602877.html">第746章 机器战警</a></div>
<div class="col-xs-2">冰城之光</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li><li class="list-group-item clearfix">
<div class="col-xs-1"><i class="tag-blue">玄幻</i></div>
<div class="col-xs-3 text-nowrap modal-open"><a href="/book/131137/">待我重拾旧山河</a></div>
<div class="col-xs-4 text-nowrap modal-open"><a href="/book/131137/174602658.html">第二百零四章：夜谈</a></div>
<div class="col-xs-2">缺悦</div>
<div class="col-xs-2"><span class="time">2021-01-14</span></div>
</li>
<li class="list-group-item"><div class="listMore"><a href="//www.boquge.com/top/lastupdate_1.html">更多更新&gt;&gt;</a></div></li>
</ul>
</div>
</section>
<section class="panel panel-default col-xs-3">
<div class="panel-heading"><h3 class="panel-title">最新入库小说</h3></div>
<div class="panel-body" id="novel-list">
<ul><li class="list-group-item text-nowrap modal-open"><a href="/book/132999/">冷宫开局签到葵花宝典</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132998/">网游鸿蒙世界</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132997/">重回1986</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132996/">凤帝临朝</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132995/">没有人比我更懂修仙生活</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132994/">我被坑到了柯南世界</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132993/">三人一狗事务所</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132992/">乡村夜曲</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132991/">当个咸鱼不好吗</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132990/">诸天战争之王</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132989/">系统超能时代</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132988/">谍云重重</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132987/">逆流之剑</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132986/">赛博魔兽2077</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132985/">陌上小草向阳生</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132984/">诸天开局长生药</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132983/">我真的只是个网红</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132982/">乱世长生劫</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132981/">我，青云剑首，开局签到诛仙剑阵</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132980/">美漫悍刀行</a></li><li class="list-group-item text-nowrap modal-open"><a href="/book/132979/">绝煞帝尊</a></li>
<li class="list-group-item"><div class="listMore"><a href="//www.boquge.com/top/postdate_1.html">更多新书&gt;&gt;</a></div></li>
</ul>
</div>
</section>
<div class="clearfix"></div>
<section class="panel panel-default">
<div class="panel-heading"><h3 class="panel-title">友情链接</h3></div>
<div class="clearfix mb10">
<ul class="pl20 pt10 pr20">
<li class="col-xs-2"><a href="//www.jingcaiyuedu.com/" target="_blank">精彩小说网</a></li>
</ul>
</div>
</section>
<footer class="footer">
<p class="links"><a href="//m.boquge.com">笔趣阁手机版</a><i>　|　</i><a href="//mail.qq.com/cgi-bin/qm_share?t=qm_mailme&amp;email=" rel="nofollow">问题反馈</a><i>　|　</i><a class="go_top" href="#btop-info">返回顶部</a></p>
<p>请所有作者发布作品时务必遵守国家互联网信息管理办法规定，我们拒绝任何色情小说，一经发现，即作删除<br/>
本站所收录作品、社区话题、书库评论及本站所做之广告均属其个人行为，与本站立场无关<br/>
Copyright©2015-2016 <a href="//www.boquge.com">www.boquge.com</a> All   rights Reserved  版权所有 <br/>
</p>
</footer>
<div class="hidden">
<script type="text/javascript">showTongJi();</script>
</div>
</div>
</body>
</html>