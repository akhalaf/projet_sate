<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="logged-out question-page   lt-ie9 lt-ie8 lt-ie7" xmlns="//www.w3.org/1999/xhtml" xmlns:og="//ogp.me/ns#" xmlns:fb="//www.facebook.com/2008/fbml"> <![endif]--><!--[if IE 7]>         <html class="logged-out question-page   lt-ie9 lt-ie8" xmlns="//www.w3.org/1999/xhtml" xmlns:og="//ogp.me/ns#" xmlns:fb="//www.facebook.com/2008/fbml"> <![endif]--><!--[if IE 8]>         <html class="logged-out question-page   lt-ie9" xmlns="//www.w3.org/1999/xhtml" xmlns:og="//ogp.me/ns#" xmlns:fb="//www.facebook.com/2008/fbml"> <![endif]--><!--[if gt IE 8]><!--><html class="logged-out question-page " xmlns="//www.w3.org/1999/xhtml" xmlns:fb="//www.facebook.com/2008/fbml" xmlns:og="//ogp.me/ns#"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<title>Why Is My Labrador Retching? - Blurtit</title>
<meta content="XJi_ihJwPvmAnMmTbEIGqxw-Udj4zwOlB1aN3URwE_I" name="google-site-verification"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Answer (1 of 1): If your dog is retching a vet should be consulted as soon as possible. Labs are prone to eating and chewing all sorts of rubbish. If he vomits, pick some of the vomit up in a tissue and take it with you to the vet for analysis....it may be poisonous. My lab made himself very sick by eating some blood and bone fertilizer from the garden flower beds. Labradors have very acute sense of smell and are attracted to anything with a bone content as well as blood in various forms from various sources. They will also pick things up on their exercise walks. Be especially mindful of the urgency&amp;nbsp;&amp;nbsp;if the vomit contains what appears to be blood.&amp;nbsp;&amp;nbsp;I urge you not to be complacent as this could be very serious." name="description"/>
<meta content="110555434708" name="fb:app_id"/>
<meta content="Why Is My Labrador Retching?" name="og:title"/>
<meta content="article" name="og:type"/>
<meta content="//pets-animals.blurtit.com/3750508/why-is-my-labrador-retching" name="og:url"/>
<meta content="//www.blurtit.com/favicon.ico" name="og:image"/>
<meta content="Blurtit" name="og:site_name"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<script>if (window.top !== window.self) window.top.location.replace(window.self.location.href);</script>
<link href="https://pets-animals.blurtit.com/3750508/why-is-my-labrador-retching" rel="canonical"/>
<link href="/favicon.ico" rel="shortcut icon"/>
<!--[if lt IE 9]>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
<link href="//cf.blurtitcdn.com/css/blurtit-v1.82.css" media="screen" rel="stylesheet"/>
<link href="//cf.blurtitcdn.com/css/responsive-v1.82.css" media="screen and (max-width: 480px)" rel="stylesheet"/>
<!--[if lte IE 8]>
		<link rel="stylesheet" href="//cf.blurtitcdn.com/css/ie/lte8.css" />
		<![endif]-->
<!--[if lt IE 8]>
		<link rel="stylesheet" href="//cf.blurtitcdn.com/css/ie/lt8.css" />
		<![endif]-->
<!--[if IE 6]>
		<link rel="stylesheet" href="//cf.blurtitcdn.com/css/ie/ie6.css" />
		<![endif]-->
<!--[if IE 7]>
		<link rel="stylesheet" href="//cf.blurtitcdn.com/css/ie/font-awesome-ie7.min.css">
		<![endif]-->
<link href="//fonts.googleapis.com/css?family=Oswald" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
	var googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];
	(function() {
		var gads = document.createElement('script');
		gads.async = true;
		gads.type = 'text/javascript';
		var useSSL = 'https:' == document.location.protocol;
		gads.src = (useSSL ? 'https:' : 'http:') +
			'//www.googletagservices.com/tag/js/gpt.js';
		var node = document.getElementsByTagName('script')[0];
		node.parentNode.insertBefore(gads, node);
	})();
	googletag.cmd.push(function() {
			googletag.defineSlot('/1016611/blurtit_top_mpu', [300, 250], 'div-gpt-ad-1368461579297-4').addService(googletag.pubads());
		googletag.defineSlot('/1016611/blurtit_after_question', [468, 60], 'div-gpt-ad-1368461579297-0').addService(googletag.pubads());
		googletag.defineSlot('/1016611/blurtit_bottom_mpu', [300, 250], 'div-gpt-ad-1368461579297-1').addService(googletag.pubads());
	if( window.innerWidth<=480 ) {
		googletag.defineSlot('/1016611/blurtit_mobile_footer', [320, 50], 'div-gpt-ad-1368461579297-3').addService(googletag.pubads());
		googletag.defineSlot('/1016611/blurtit_mobile_after_question', [320, 50], 'div-gpt-ad-1368461579297-2').addService(googletag.pubads());
	}
		googletag.pubads().enableSingleRequest();
		googletag.pubads().collapseEmptyDivs();
		googletag.enableServices();
	});
</script>
<script type="text/javascript">
		window._taboola = window._taboola || [];
		_taboola.push({article:'auto'});
		!function (e, f, u, i) {
			if (!document.getElementById(i)){
				e.async = 1;
				e.src = u;
				e.id = i;
				f.parentNode.insertBefore(e, f);
			}
		}(document.createElement('script'),
			document.getElementsByTagName('script')[0],
			'//cdn.taboola.com/libtrc/blurtlt/loader.js',
			'tb_loader_script');
	</script>
<script src="//www.google.com/recaptcha/api.js"></script>
<!-- GPT (New Tims experiments)
<script type='text/javascript'>
  googletag.cmd.push(function() {
    googletag.defineSlot('/1016611/T_Blurtit_Top_Right_300_250', [300, 250], 'div-gpt-ad-1450682766942-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script>

<script type='text/javascript'>
  googletag.cmd.push(function() {
    googletag.defineSlot('/1016611/T_Blurtit_Second_Top_Right_300_250', [300, 250], 'div-gpt-ad-1450706941357-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script>
-->
</head>
<body>
<script>
	google_analytics_uacct = "UA-5849863-1";
	</script>
<div class="stripe"></div>
<div class="container">
<header class="clearfix" role="banner">
<!-- Begin with the logo -->
<div class="logo pull-left">
<a accesskey="1" href="//www.blurtit.com" title="Home">Blurtit<span>.</span></a>
</div>
<form action="//www.blurtit.com/search/" class="top-search pull-left" method="get" role="search">
<input autocomplete="off" class="search-input search-typeahead" name="search-query" placeholder="Search questions, topics and people" type="text" value=""/>
<input name="filter" type="hidden" value=""/>
</form>
<div class="pull-right" role="navigation">
<nav class="clearfix">
<ul>
<li class="hlink-ask"><a data-target="#ask_question" data-toggle="collapse" href="//www.blurtit.com/#ask_question" id="ask_button"><i class="icon-comment"></i>Ask</a></li>
<li class="hlink-topics"><a class="" href="//www.blurtit.com/topics"><i class="icon-tag"></i>Topics</a></li>
<li class="hlink-discover"><a class="" href="//www.blurtit.com/discover"><i class="icon-search"></i>Discover</a></li>
<li class="hlink-login"><a href="//www.blurtit.com/signin"><i class="icon-user"></i>Sign in</a></li>
</ul>
</nav>
</div>
</header>
<div class="collapse collapse-ask" id="ask_question"><div class="clearfix relative popdown" id="ask_popdown"></div></div>
</div>
<div class="container wrapper clearfix" itemprop="QAPage" itemscope="" itemtype="http://schema.org/QAPage">
<div class="main pull-left" itemprop="mainEntity" itemscope="" itemtype="http://schema.org/Question" role="main">
<div class="clearfix question main-sides">
<div class="article-complimentary">
<a class="topic-thumb" href="//www.blurtit.com/Dog-Health/"><img alt="Anonymous" src="//cf.blurtitcdn.com/var/topics/pets_animals/thumb_dog_health.svg"/></a>
</div>
<div class="article-main">
<h1 class="page-title editable-non-textarea" id="question3750508" itemprop="name">Why Is My Labrador Retching?</h1>
<span itemprop="answerCount" style="display:none;">1</span>
<script>
<!--
var advert = '';
var random_number = Math.random();
var split = Math.round( random_number );
var mobile_advert = '';

	mobile_advert += '<div class="adblock-first-question" style="margin-top:5px;margin-left: -3px;">\n';
	mobile_advert += '<script type="text/javascript"><!--\n';
	mobile_advert += 'google_ad_client = "ca-pub-1549962111268759"\n';
	mobile_advert += '/* BlurtIt_Cust_468x180 */\n';
	mobile_advert += 'google_override_format = true;\n';
	mobile_advert += 'google_ad_slot = "9097876704";\n';
	mobile_advert += 'google_ad_width = 468;\n';
	mobile_advert += 'google_ad_height = 60;\n';
	mobile_advert += 'google_ad_channel = "Additional00035,Additional00038"; /* Add Channel IDs*/\n';
	mobile_advert += 'google_ad_type = "text";\n';
	mobile_advert += 'google_color_link =  "#38A1EC";\n';
	mobile_advert += 'google_color_text =  "#7F7F7F";\n';
	mobile_advert += 'google_color_bg =  "#FFFFFF";\n';
	mobile_advert += 'google_color_url =  "#38A1EC";\n';
	mobile_advert += 'google_font_face =  "arial";\n';
	mobile_advert += 'google_adtest = "off";\n';
	mobile_advert += '//-->\n';
	mobile_advert += '<\/script>\n';
	mobile_advert += '<script type="text/javascript" src="//pagead2.googlesyndication.com/pagead/show_ads.js"><\/script>\n';
	mobile_advert += '<\/div>\n';
if( mobile_advert!=='') {
	document.write(mobile_advert);
}
//-->
</script>
</div>
</div>
<div class="" id="answers">
<div class="clearfix heading-bar">
<h2 class="pull-left">1 Answers</h2>
</div>
<article class="answer clearfix main-sides last" id="answerArticle2047432" itemprop="suggestedAnswer" itemscope="" itemtype="http://schema.org/Answer">
<div class="article-complimentary">
<div>
<a class="media-thumb" href="//www.blurtit.com/u/635241/"><img alt="John Faulkner Profile" height="56" src="//cf.blurtitcdn.com/var/avatar/avatar11.jpg" width="56"/></a>
</div>
<div class="rating clearfix">
<a class="modal-login-prompt-show rating-button positive " href='//www.blurtit.com/signin?vote="2047432,q1817119.html,635241,1"&amp;page=//pets-animals.blurtit.com/3750508/why-is-my-labrador-retching' title="Upvote"><i class="icon-caret-up" data-value="0" itemprop="upvoteCount"></i></a>
<a class="modal-login-prompt-show rating-button negative " href='//www.blurtit.com/signin?vote="2047432,q1817119.html,635241,-1"&amp;page=//pets-animals.blurtit.com/3750508/why-is-my-labrador-retching' title="Downvote"><i class="icon-caret-down"></i></a>
</div>
</div>
<div class="article-main">
<div class="meta "><strong><a href="/u/635241/">John Faulkner</a></strong>  answered </div>
<!--INFOLINKS_ON-->
<div class="user-content clearfix " id="answer2047432" itemprop="text">If your dog is retching a vet should be consulted as soon as possible. Labs are prone to eating and chewing all sorts of rubbish. If he vomits, pick some of the vomit up in a tissue and take it with you to the vet for analysis....it may be poisonous. My lab made himself very sick by eating some blood and bone fertilizer from the garden flower beds. Labradors have very acute sense of smell and are attracted to anything with a bone content as well as blood in various forms from various sources. They will also pick things up on their exercise walks. Be especially mindful of the urgency  if the vomit contains what appears to be blood.  I urge you not to be complacent as this could be very serious.							<!--INFOLINKS_OFF-->
<script>
									var advert = '';

									advert = '<div style="position: relative;left: -3px;top:6px;"">\n';
									advert += '<script type="text/javascript"><!--\n';
									advert += 'google_ad_client = "ca-pub-1549962111268759"\n';
									advert += 'google_ad_channel = "Additional00042,Additional00038"; /* Add Channel IDs*/\n';
									advert += 'google_override_format = true;\n';
									advert += 'google_ad_slot = "3256114700";\n';
									advert += 'google_ad_width = 468;\n';
									advert += 'google_ad_height = 60;\n';
									advert += 'google_ad_type = "text";\n';
									advert += 'google_color_link =  "#38A1EC";\n';
									advert += 'google_color_text =  "#7F7F7F";\n';
									advert += 'google_color_bg =  "#FFFFFF";\n';
									advert += 'google_color_url =  "#38A1EC";\n';
									advert += 'google_font_face =  "arial";\n';
									advert += 'google_adtest = "off";\n';
									advert += '//-->\n';
									advert += '<\/script>\n';
									advert += '<script type="text/javascript" src="//pagead2.googlesyndication.com/pagead/show_ads.js"><\/script>\n';
									advert += '<\/div>\n';

									if( advert!=='') {
										document.write(advert);
									}
									</script>
</div>
<footer>
<div class="actions clearfix">
<ul class="pull-right">
</ul>
<ul class="pull-left action-links">
<li class="separate"><a class="modal-login-prompt-show thank-link thank2047432" href='//www.blurtit.com/signin?thank="2047432,635241"&amp;page=//pets-animals.blurtit.com/3750508/why-is-my-labrador-retching'>Thank Writer</a></li>
<li class="separate"><a class="modal-login-prompt-show comment-add-link" href="//www.blurtit.com/signin?page=//pets-animals.blurtit.com/3750508/why-is-my-labrador-retching">Comment</a></li>
<li class="">
<a class="modal-login-prompt-show share-link shareA2047432" href='/signin?blurt="answer,2047432,635241"&amp;page=//pets-animals.blurtit.com/3750508/why-is-my-labrador-retching'>Blurt</a></li>
</ul>
</div>
<section class="additional-content up-arrow hide">
<div class="additional-summary thank-summary hide">
<i class="icon-thumbs-up"></i> thanked the writer.
		</div>
<div class="additional-summary share-summary hide">
<i class="icon-retweet"></i> blurted this.
		</div>
</section>
</footer>
</div>
</article>
<aside class="relative main-sides">
<div id="taboola-below-article-thumbnails"></div>
</aside>
<script type="text/javascript">
  window._taboola = window._taboola || [];
  _taboola.push({
    mode: 'thumbnails-a',
    container: 'taboola-below-article-thumbnails',
    placement: 'Below Article Thumbnails',
    target_type: 'mix'
  });
</script>
<aside class="relative main-sides">
<h3>You might also like...</h3>
<ul class="list list-bullet" id="live-related">
<li style="margin-bottom:3px;">
<script type="text/javascript"><!--
							google_ad_client = "ca-pub-1549962111268759";
							/* Also Asked As */
							google_ad_slot = "4488337108";
							google_ad_width = 468;
							google_ad_height = 60;
							google_ad_channel = "Additional00040,Additional00038"; /* Add Channel IDs*/
							google_override_format = true;
							google_ad_type = "text";
							google_color_link =  "#38A1EC";
							google_color_text =  "#7F7F7F";
							google_color_bg =  "#FFFFFF";
							google_color_url =  "#38A1EC";
							google_font_face =  "arial";
							google_adtest = "off";
							//-->
							</script>
<script src="//pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript">
</script>
</li>
<li class="clearfix live-related-li">
<p class="live-related-title"><a class="also-asked-title" href="//pets-animals.blurtit.com/100776/when-do-i-mate-my-female-labrador">When Do I Mate My Female Labrador?</a></p>
<p class="also-asked-cat"><img alt="Breeding Dogs" height="11" src="//cf.blurtitcdn.com/var/topics/pets_animals/thumb_breeding_dogs.svg" width="11"/>Breeding Dogs</p>
<p class="also-asked-summary" style="color:#7f7f7f;">You mate her when she's old enough to take care of puppies, and you have the proper equipment to raise...</p>
</li>
<li class="clearfix live-related-li">
<p class="live-related-title"><a class="also-asked-title" href="//pets-animals.blurtit.com/143956/my-13-yr-old-cat-has-been-swallowing-air-and-retching-for-2-days-what-can-be-the-cause-of">My 13 Yr Old Cat Has Been "Swallowing" Air And Retching For 2 Days. What Can Be The Cause Of My 13 Yr Old Cat Retching?</a></p>
<p class="also-asked-cat"><img alt="Cat Health" height="11" src="//cf.blurtitcdn.com/var/topics/pets_animals/thumb_cat_health.svg" width="11"/>Cat Health</p>
<p class="also-asked-summary" style="color:#7f7f7f;">You should observe silently that your cat is stressed or nervous for some reasons. These symptoms show...</p>
</li>
<li class="clearfix live-related-li">
<p class="live-related-title"><a class="also-asked-title" href="//pets-animals.blurtit.com/282841/my-female-chihuaha-has-rectal-bleeding-and-is-retching-and-moaning-can-you-help">My Female Chihuaha Has Rectal Bleeding And Is Retching And Moaning. Can You Help?</a></p>
<p class="also-asked-cat"><img alt="Dogs" height="11" src="//cf.blurtitcdn.com/var/topics/pets_animals/thumb_dogs.svg" width="11"/>Dogs</p>
<p class="also-asked-summary" style="color:#7f7f7f;">In my opinion your chihuaha needs a tampon and maybe sum midol...</p>
</li>
<li class="clearfix live-related-li">
<p class="live-related-title"><a class="also-asked-title" href="//pets-animals.blurtit.com/983669/my-labrador-puppy-is-acting-weird-shes-throwing-up-shaking-and-hiding-is-she-sick">My Labrador Puppy Is Acting Weird She's Throwing Up Shaking And Hiding Is She Sick?</a></p>
<p class="also-asked-cat"><img alt="Dog Health" height="11" src="//cf.blurtitcdn.com/var/topics/pets_animals/thumb_dog_health.svg" width="11"/>Dog Health</p>
<p class="also-asked-summary" style="color:#7f7f7f;">It sounds like she's sick. Anytime a dog's behavior changes in any way, it's time for a visit with the...</p>
</li>
<li class="clearfix live-related-li">
<p class="live-related-title"><a class="also-asked-title" href="//pets-animals.blurtit.com/1192125/why-does-my-labradors-pant-so-much">Why Does My Labradors Pant So Much?</a></p>
<p class="also-asked-cat"><img alt="Dogs" height="11" src="//cf.blurtitcdn.com/var/topics/pets_animals/thumb_dogs.svg" width="11"/>Dogs</p>
<p class="also-asked-summary" style="color:#7f7f7f;">If your dog is more than 50 years old in dog years then it can be a side of old age.
If the dog was...</p>
</li>
<li class="clearfix live-related-li">
<p class="live-related-title"><a class="also-asked-title" href="//pets-animals.blurtit.com/1240147/how-long-is-a-labrador-pregnant">How Long Is A Labrador Pregnant?</a></p>
<p class="also-asked-cat"><img alt="Dog Health" height="11" src="//cf.blurtitcdn.com/var/topics/pets_animals/thumb_dog_health.svg" width="11"/>Dog Health</p>
<p class="also-asked-summary" style="color:#7f7f7f;">All dogs are pregnant for 63 days. If its her first litter she may go anywhere from 60 days to 65 days,...</p>
</li>
<li class="clearfix live-related-li">
<p class="live-related-title"><a class="also-asked-title" href="//pets-animals.blurtit.com/1425514/when-do-i-know-that-my-female-labrador-is-in-heat">When Do I Know That My Female Labrador Is In Heat?</a></p>
<p class="also-asked-cat"><img alt="Breeding Dogs" height="11" src="//cf.blurtitcdn.com/var/topics/pets_animals/thumb_breeding_dogs.svg" width="11"/>Breeding Dogs</p>
<p class="also-asked-summary" style="color:#7f7f7f;">She will bleed (small drops) from her vulva which will be swollen.. She will also urinate more often...</p>
</li>
<li class="clearfix live-related-li">
<p class="live-related-title"><a class="also-asked-title" href="//pets-animals.blurtit.com/2091554/whats-causing-my-labrador-to-shed-so-much-hair">What's Causing My Labrador To Shed So Much Hair?</a></p>
<p class="also-asked-cat"><img alt="Dog Health" height="11" src="//cf.blurtitcdn.com/var/topics/pets_animals/thumb_dog_health.svg" width="11"/>Dog Health</p>
<p class="also-asked-summary" style="color:#7f7f7f;">Dry skin, the food you give him. I have a yellow female lab. She has dry skin and sheds a lot too. I...</p>
</li>
<li class="clearfix live-related-li">
<p class="live-related-title"><a class="also-asked-title" href="//pets-animals.blurtit.com/3478695/what-is-a-champagne-labrador">What is a Champagne Labrador?</a></p>
<p class="also-asked-cat"><img alt="Dog Behavior" height="11" src="//cf.blurtitcdn.com/var/topics/pets_animals/thumb_dog_behaviour.svg" width="11"/>Dog Behavior</p>
<p class="also-asked-summary" style="color:#7f7f7f;">It is actually a Yellow Lab and not a Champagne Lab. It is a dog....</p>
</li>
<li class="clearfix live-related-li">
<p class="live-related-title"><a class="also-asked-title" href="//pets-animals.blurtit.com/3752215/when-is-the-labrador-going-to-menstruate">When Is The Labrador Going To Menstruate?</a></p>
<p class="also-asked-cat"><img alt="Dog Behavior" height="11" src="//cf.blurtitcdn.com/var/topics/pets_animals/thumb_dog_behaviour.svg" width="11"/>Dog Behavior</p>
<p class="also-asked-summary" style="color:#7f7f7f;">Menstruation is feature of group of animals called primates which also includes humans. In dog menstruation...</p>
</li>
<li style="margin-bottom:3px;">
<script type="text/javascript"><!--
							google_ad_client = "ca-pub-1549962111268759";
							/* Also Asked As */
							google_ad_slot = "8253251906";
							google_ad_width = 468;
							google_ad_height = 60;
							google_ad_channel = "Additional00041,Additional00038"; /* Add Channel IDs*/
							google_override_format = true;
							google_ad_type = "text";
							google_color_link =  "#38A1EC";
							google_color_text =  "#7F7F7F";
							google_color_bg =  "#FFFFFF";
							google_color_url =  "#38A1EC";
							google_font_face =  "arial";
							google_adtest = "off";
							//-->
							</script>
<script src="//pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript">
</script>
</li>
</ul>
</aside>
<aside class="relative main-sides"><div id="taboola-feed-split-container"></div></aside>
</div>
<div id="answerForm">
<div class="clearfix heading-bar main-sides answer-form-heading">
<h2>Answer Question</h2>
</div>
<form action="/answer" class="clearfix main-sides standard-form answer-form" method="POST">
<input name="answer_page" type="hidden" value="q1817119.html"/>
<input name="answer_question_id" type="hidden" value="3750508"/>
<div class="article-complimentary">
<div>
<span class="media-thumb"><img alt="Anonymous" height="56" src="//cf.blurtitcdn.com/var/avatar/thumb_default_avatar.jpg" width="56"/></span>
</div>
</div>
<div class="article-main">
<div class="clearfix">
<textarea class="wysiwyg" cols="30" id="answer_text" name="answer_text" placeholder="Leave your answer" rows="10"></textarea>
</div>
<div class="actions pull-right">
<button class="button primary" name="answer_submit" type="submit" value="1">Answer</button>
</div>
</div>
</form>
</div> </div>
<div class="complimentary pull-right" role="complimentary">
<div class="adblock-top-left"><!-- blurtit_top_mpu --><div id="div-gpt-ad-1368461579297-4" style="width:300px; height:250px;"><script type="text/javascript">googletag.cmd.push(function() { googletag.display("div-gpt-ad-1368461579297-4"); });</script></div></div>
<nav class="stats">
<ul class="clearfix">
<li><span class="nolink"><span class="number">1</span> Answers</span></li>
<li><span class="nolink"><span class="number" id="pageviews">150</span> Views</span></li>
<li><span class="nolink"><span class="number">0</span> Followers</span></li>
<li><span class="nolink"><span class="number">0</span> Favorites</span></li>
</ul>
</nav><div class="adblock-top-left"><!-- blurtit_bottom_mpu --><div id="div-gpt-ad-1368461579297-1" style="width:300px; height:250px;"><script type="text/javascript">googletag.cmd.push(function() { googletag.display("div-gpt-ad-1368461579297-1"); });</script></div></div><div style="padding: 19px;border-bottom: 1px solid #cbcbcb;"><div id="taboola-right-rail-thumbnails"></div></div>
<script type="text/javascript">
  window._taboola = window._taboola || [];
  _taboola.push({
    mode: 'thumbnails-rr',
    container: 'taboola-right-rail-thumbnails',
    placement: 'Right Rail Thumbnails',
    target_type: 'mix'
  });
</script> <aside class="relative">
<h3>Related Reading</h3>
<ul class="list list-bullet">
<li class="clearfix">
<p class="title"><a href="//pets-animals.blurtit.com/282841/my-female-chihuaha-has-rectal-bleeding-and-is-retching-and-moaning-can-you-help">My Female Chihuaha Has Rectal Bleeding And Is Retching And Moaning. Can You Help?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//pets-animals.blurtit.com/143956/my-13-yr-old-cat-has-been-swallowing-air-and-retching-for-2-days-what-can-be-the-cause-of">My 13 Yr Old Cat Has Been "Swallowing" Air And Retching For 2 Days. What Can Be The Cause Of My 13 Yr Old Cat Retching?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//pets-animals.blurtit.com/2051039/what-does-labrador-eat">What Does Labrador Eat?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//pets-animals.blurtit.com/327110/chocolate-lab-is-coughing-really-bad-it-sounds-like-the-croup-and-shes-spitting-up-lots">Chocolate Lab Is Coughing Really Bad.  It Sounds Like The Croup And She's Spitting Up Lots Of Phlegm (sometimes Looks Bloody).  She's Still Wagging Tail, Walking Around &amp; Doesn't Seem To Be In Pain.  We Know Its Not Heart Worms.  What Could It Be?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//pets-animals.blurtit.com/155363/what-happens-if-your-dog-eats-plastic">What Happens If Your Dog Eats Plastic?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//pets-animals.blurtit.com/155345/my-dog-throws-up-a-yellow-bile-every-day-what-is-it-and-what-can-i-do-for-him">My Dog Throws Up A Yellow Bile Every Day.  What Is It? And What Can I Do For Him?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//pets-animals.blurtit.com/965943/why-is-my-dog-vomiting-white-foam">Why Is My Dog Vomiting White Foam?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//pets-animals.blurtit.com/132130/my-dog-is-throwing-up-blood-can-you-tell-me-what-may-be-wrong">My Dog Is Throwing Up Blood, Can You Tell Me What May Be Wrong?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//pets-animals.blurtit.com/132628/what-does-it-mean-if-my-dog-is-coughing-or-gagging">What Does It Mean If My Dog Is Coughing Or Gagging?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//business-finance.blurtit.com/252879/if-i-am-fired-for-missing-time-with-a-doctor-excuse-can-i-collect-unemployment">If I Am Fired For Missing Time With A Doctor Excuse Can I Collect Unemployment?</a></p>
</li>
</ul>
<div class="clearfix">
<a class="help-link" href="#help" title="Help">?</a>
<div class="help-hint fade hide">
<p>Here are some related questions which you might be interested in reading.</p>
</div>
</div>
</aside>
<aside class="relative main-sides">
<h3>Popular</h3>
<ul class="list list-bullet">
<li class="clearfix">
<p class="title"><a href="//www.blurtit.com/3844595/does-anybody-know-what-happened-to-smiley165">Does Anybody Know What Happened To Smiley165?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//philosophy-religion.blurtit.com/3837589/what-does-god-want">What Does God Want?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//travel.blurtit.com/3904636/197---------500">$1.97 + _______ = $5.00?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//pets-animals.blurtit.com/3831255/what-do-i-do-to-make-a-dog-not-jump-on-people">What Do I Do To Make A Dog Not Jump On People?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//food-drink.blurtit.com/3842799/why-do-people-wait-in-line-at-a-fast-food-drive-through-rather-than-go-inside-and-order">Why Do People Wait In Line At A Fast Food Drive Through Rather Than Go Inside And Order?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//food-drink.blurtit.com/3831757/what-is-the-average-food-type-liked-by-people-in-the-whole-world">What Is The Average Food Type Liked By People In The Whole World?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//education.blurtit.com/3765209/how-do-i-explain-a-failed-degree-in-an-interview-i-did-law-and-spanish-failed-the-spanish">How do I explain a failed degree in an interview? I did law and spanish, failed the spanish by one element. I repeated several times, but by the end, exhausted and defeated I couldnt face repeating it so its gone. ADVICE?</a></p>
</li>
<li class="clearfix">
<p class="title"><a href="//legal.blurtit.com/3852089/whats-the-difference-between-a-lawyer-and-a-solicitor">What's the difference between a lawyer and a solicitor?</a></p>
</li>
</ul>
</aside>
</div>
</div>
<!-- Modal -->
<div aria-hidden="true" aria-labelledby="genericModal" class="clearfix modal hide fade in" id="genericModal" role="dialog" tabindex="-1">
<div class="main pull-left">
</div>
<div class="complimentary pull-right">
</div>
<a aria-hidden="true" class="modal-close"><i class="icon-remove"></i></a>
</div><div aria-hidden="true" aria-labelledby="genericModal" class="clearfix modal hide fade in" id="loginPromptModal" role="dialog" tabindex="-1">
<div class="main-section">
<p id="login_prompt_text"></p>
<div class="social-sharing">
<form action="/signin" class="standard-form no-top-margin" method="GET">
<input id="login_prompt_actions" name="actions" type="hidden" value=""/>
<ul class="social-buttons clearfix">
<li><button class="button button-twitter" name="twitter_register" type="submit" value="1"><i class="icon-twitter"></i> Connect</button></li>
<li><button class="button button-google-plus" name="google_register" type="submit" value="1"><i class="icon-google-plus"></i> Connect</button></li>
<li><button class="button button-facebook" name="facebook_register" type="submit" value="1"><i class="icon-facebook"></i> Connect</button></li>
</ul>
</form>
</div>
</div>
<a aria-hidden="true" class="modal-login-prompt-close"><i class="icon-remove"></i></a>
</div> <div class="container">
<div class="breadcrumb" role="navigation">
<nav class="clearfix">
<ul itemscope="" itemtype="//data-vocabulary.org/Breadcrumb">
<li class="breadcrumb-item-first "><a href="//www.blurtit.com/topics" itemprop="url"><span itemprop="title">All Topics</span></a></li>
<li class=""><a href="//www.blurtit.com/Pets-Animals/" itemprop="url"><span itemprop="title">Pets &amp; Animals</span></a></li>
<li class=""><a href="//www.blurtit.com/Pets/" itemprop="url"><span itemprop="title">Pets</span></a></li>
<li class=""><a href="//www.blurtit.com/Dogs/" itemprop="url"><span itemprop="title">Dogs</span></a></li>
<li class="breadcrumb-item-last"><a href="//www.blurtit.com/Dog-Health/" itemprop="url"><span itemprop="title">Dog Health</span></a></li>
</ul>
</nav>
</div>
<footer class="page-footer clearfix">
<section class="pull-left">
<h2>Questions</h2>
<ul>
<li><a href="//www.blurtit.com/#ask">Ask</a></li>
<li><a href="//www.blurtit.com/topics">Topics</a></li>
<li><a href="//www.blurtit.com/discover">Discover</a></li>
</ul>
</section>
<section class="pull-left">
<h2>Company</h2>
<ul>
<li><a href="//www.blurtit.com/support/about">About</a></li>
<!-- <li><a href="/badges">Badges</a></li> -->
<li><a href="//www.blurtit.com/support/advertise">Advertise</a></li>
<!-- <li><a href="/blog">Blog</a></li> -->
<li><a href="//www.blurtit.com/support/contact">Contact</a></li>
<!-- <li><a href="/support/feedback">Feedback</a></li> -->
<li><a href="//www.blurtit.com/support/">Support</a></li>
</ul>
</section>
<section class="pull-left">
<h2>Everything Else</h2>
<ul>
<li><a href="//www.blurtit.com/terms_of_use">Terms Of Use</a></li>
<li><a href="//www.blurtit.com/privacy_policy">Privacy Policy</a></li>
<li><a href="//www.blurtit.com/cookie_policy">Cookie Policy</a></li>
</ul>
</section>
<div class="pull-right" style="width:314px">
<h2>Follow Us</h2>
<div class="social">
<h3>Social Networks</h3>
<ul>
<li><a class="on-facebook" href="https://www.facebook.com/blurtit"><i class="icon-facebook"></i><span>Blurtit on Facebook</span></a></li>
<li><a class="on-twitter" href="https://twitter.com/blurtit"><i class="icon-twitter"></i><span>Blurtit on Twitter</span></a></li>
<!-- <li><a href="https://linkedin.com"></a></li> -->
<li><a class="on-google-plus" href="https://plus.google.com/102183012434304818865" rel="publisher"><i class="icon-google-plus"></i><span>Blurtit on Google+</span></a></li>
</ul>
</div>
<div class="sign-off">
<div class="logo pull-left">
<a href="//www.blurtit.com">Blurtit<span>.</span></a>
</div>
<div>
<p><small>© Blurtit Ltd. All rights reserved.</small></p>
</div>
</div>
</div>
</footer>
</div>
<script>
			var blurtit                   = {};
			blurtit.user_id               = 0;
			blurtit.fullname              = '';
			blurtit.profile_pic           = '';
			blurtit.anonymous_name        = 'Anonymous';
			blurtit.anonymous_profile_pic = '/var/avatar/thumb_default_avatar.jpg';
			blurtit.loggedin              = false;
			blurtit.query                 = '';
			blurtit.local                 = false;
			blurtit.in_mixpanel           = '';
			blurtit.joined          	  = '';
			blurtit.base_url 			  = '//www.blurtit.com';
			blurtit.last_popular_item	  = '';
			blurtit.page_number			  = 0;
							blurtit.feed_page 		  = false;
										blurtit.question_id       = '3750508';
								</script>
<!-- scripts concatenated and minified via build script -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="//cf.blurtitcdn.com/js/qa-loggedout-v1.62.min.js"></script>
<!-- end scripts -->
<script type="text/javascript">
   var infolink_pid = 429299;
   var infolink_wsid = 0;
</script>
<script src="//resources.infolinks.com/js/infolinks_main.js" type="text/javascript"></script>
<script>
<!--
if( window.innerWidth<=480 ) {
	var mobile_advert = '<!-- blurtit_mobile_footer --><div id="div-gpt-ad-1368461579297-3" class="adblock-mobile-fixed-bottom" style="width:320px; height:50px;"><' + 'script type="text/javascript">googletag.cmd.push(function() { googletag.display("div-gpt-ad-1368461579297-3"); });<' + '/script></div>';
	document.write(mobile_advert);
}
//-->
</script>
<script>
$(document).ready(function(){

	$('#popin_ask').click(function(event){

		event.preventDefault();

		$("html, body").animate({
	 		scrollTop:0
	 	},"slow",function(){
	 		$( "#ask_button" ).trigger( "click" );
	 	});
	});
});
 </script>
<div class="plus-one-page" id="plusOnePage">
<p>Didn't find the answer you were looking for?
	<button class="button primary" id="popin_ask" style="margin-top:10px;">Ask a Question</button>
</p>
</div>
<script type="text/javascript">
			  window._taboola = window._taboola || [];
			  _taboola.push({flush: true});
			</script>
<script>
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-5849863-1']);
	_gaq.push(['_setDomainName', 'blurtit.com']);
	_gaq.push(['_trackPageview']);

	(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();

	(function (tos) {
		window.setInterval(function () {
			tos = (function (t) {
				return t[0] == 50 ? (parseInt(t[1], 10) + 1) + ':00' : (t[1] || '0') + ':' + (parseInt(t[0], 10) + 10);
			})(tos.split(':').reverse());
			try {
				_gaq.push(['_trackEvent', 'Time', 'Log',tos,1,true]);
			} catch(err){}
		}, 10000);
	})('00');
</script>
<script src="//cf.blurtitcdn.com/js/cookieControl-6.2.min.js" type="text/javascript"></script>
<script type="text/javascript">//<![CDATA[
      cookieControl({
          t: {
              title: '<p>This site uses cookies to store information on your computer.</p>',
              intro: '<p>Some of these cookies are essential to make our site work and others help us to improve by giving us some insight into how the site is being used.</p>',
              full:'<p>These cookies are set when you submit a form, login or interact with the site by doing something that goes beyond clicking some simple links.</p><p>We also use some non-essential cookies to anonymously track visitors or enhance your experience of this site. If you\'re not happy with this, we won\'t set these cookies but some nice features on the site may be unavailable.</p><p>To control third party cookies, you can also <a class="ccc-settings" href="https://www.civicuk.com/cookie-control/browser-settings" target="_blank">adjust your browser settings.</a></p><p>By using our site you accept the terms of our <a href="/privacy_policy">Privacy Policy</a>.</p>'
          },
                    position:CookieControl.POS_BOTTOM,
                    style:CookieControl.STYLE_BAR,
          theme:CookieControl.THEME_LIGHT, // light or dark
          startOpen:false,
          autoHide:7000,
          subdomains:false,
          domain:'.blurtit.com',
          cookieDomain:'.blurtit.com',
          cookieName:'blurtit_cookie_control',
          protectedCookies: [], //list the cookies you do not want deleted, for example ['analytics', 'twitter']
          apiKey: '4ba22a150ebb15b8f875f424f2026c3a0eccd0a8',
          product: CookieControl.PROD_PAID,
          consentModel: CookieControl.MODEL_INFO,
          onAccept:function(){},
          onReady:function(){},
          onCookiesAllowed:function(){},
          onCookiesNotAllowed:function(){},
          countries:'AL,AD,AM,AT,CH,CY,CZ,DE,DK,EE,ES,FO,FR,GB,GE,GI,GR,HU,HR,IE,IS,IT,LT,LU,LV,MC,MK,MT,NO,NL,PO,PT,RO,RU,SI,SK,SM,TR,UA,VA' // Or supply a list, like ['United Kingdom', 'Greece']
          });
       //]]>
    </script>
</body>
</html>
