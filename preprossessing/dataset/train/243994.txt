<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html class="no-js" lang="pl" xml:lang="pl" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>BMWstore.pl - Nowe oryginalne części do wszystkich modeli BMW - darmowa wysyłka od 1000 zł</title>
<meta content="" name="description"/>
<meta content="" name="keywords"/>
<script type="text/javascript">
	// <![CDATA[
	/** 
	 * <html class="no-js" ... >
	 */
	document.documentElement.className = document.documentElement.className.replace(/\bno-js\b/,'js');
	
	/**
	 * Konfiguracja
	 */
	var config = {
		url: {
			// ścieśki do zasobów, np.: css: 'http://domena.pl/static/css/'
      css: 'https://www.bmwstore.pl/static/pl/css/',
      js: 'https://www.bmwstore.pl/static/pl/js/',
      images: 'https://www.bmwstore.pl/static/pl/images/'
		}
	}	
	// ]]>
	</script>
<link href="https://www.bmwstore.pl/static/pl/css/main.css?v=2011040502" rel="stylesheet" type="text/css"/>
<link href="https://www.bmwstore.pl/static/pl/css/extra.css?v=2011040502" rel="stylesheet" type="text/css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js" type="text/javascript"></script>
<script src="https://www.bmwstore.pl/static/pl/js/jquery.tools.min.js" type="text/javascript"></script>
<script src="https://www.bmwstore.pl/static/pl/js/jquery.tools.min.js?v=2011040502" type="text/javascript"></script>
<script src="https://www.bmwstore.pl/static/pl/js/jquery.cookie.js?v=2011040502" type="text/javascript"></script>
<script src="https://www.bmwstore.pl/static/pl/js/common.js?v=2011040502" type="text/javascript"></script>
<link href="https://www.bmwstore.pl/static/pl/favicon.ico" rel="shortcut icon"/>
<script type="text/javascript">
  var clicked = false;

  $(document).ready(function() {
    $('body').click( function (event) {
      clicked = $(event.target);
    } );
  });

  function do_request(ajax, action, method, data)
  {
    $root = $('#' + ajax);
    if ($root.is(':empty')) {
      $root.prepend('<div class="box loading"><div class="loading-indicator"><i>Ładowanie</i></div></div>');
    } else {
      $root.find('div[class="box"]').prepend('<div class="loading-indicator"><i>Ładowanie</i></div>').toggleClass('loading');
    }
    if (method == "post") 
    { 
      $.ajax({
          type: 'POST',
          url: action,
          data: data,
          success: function (data) {
            do_response(ajax, data);
          },
          error: function (xmlHttpRequest, textStatus, errorThrown) {
            do_response_error(ajax, xmlHttpRequest, textStatus, errorThrown);
          },
          dataType: 'json',
          timeout: 30000,
      });
    } else {
      $.ajax({
          type: 'GET',
          url: action,
          success: function (data) {
            do_response(ajax, data);
          },
          error: function (xmlHttpRequest, textStatus, errorThrown) {
            do_response_error(ajax, xmlHttpRequest, textStatus, errorThrown);
          },
          dataType: 'json',
          timeout: 30000,
      });
    }
  }

  function do_response_error(ajax, xmlHttpRequest, textStatus, errorThrown)
  {
    $('#' + ajax).find('div.box').removeClass('loading', false).find('div.loading-indicator').remove();
    alert("Wystąpił bład podczas przesyłania zapytania. Spróbuj ponownie.");
  }


  function do_form_ajax(ajax, id)
  {
    $('#' + ajax + ' form#'+ id).submit(function(event) {
      var action = this.action;
      var target = get_param(action, 'ajax');
      if (!target) {
        target = ajax;
      }
      var method = this.method;      
      var data = $(this).serialize();
      var name = undefined;
      if (event.originalEvent != undefined && event.originalEvent.explicitOriginalTarget != undefined && event.originalEvent.explicitOriginalTarget.type == "submit") {
        name = event.originalEvent.explicitOriginalTarget.name;
      }
      if (name == undefined) {
        name = clicked.context.name;
      }
      if (name != undefined) {
        if (method == "post") {
          if (data) {
            data += "&" + name + "=1";
          } else {
            data = name + "=1";
          }
        } else {
          if (action.indexOf('?')) {
            action += "&" + name + "=1";
          } else {
            action += "?" + name + "=1";
          }
        }
      }
      do_request(target, action, method, data);
      return false;
    });
  }

  function do_form_ajax_safe(ajax, id)
  {
    $('#' + ajax + ' form#'+ id).submit(function(event) {
      var $nested_forms = $(this).find('div.formmarker');
      var $noblock_input = $(this).find('input[name=noblocking]').eq(0);
      var pass = (!$nested_forms.length || $noblock_input.val() === 'true') ? true : false;
      if (!pass) {
        alert("Zamknij najpierw wszystkie podformularze aby kontynuuować.");
        return false;
      }
      var action = this.action;
      var target = get_param(action, 'ajax');
      if (!target) {
        target = ajax;
      }
      var method = this.method;      
      var data = $(this).serialize();
      var name = undefined;
      if (event.originalEvent != undefined && event.originalEvent.explicitOriginalTarget != undefined && event.originalEvent.explicitOriginalTarget.type == "submit") {
        name = event.originalEvent.explicitOriginalTarget.name;
      }
      if (name == undefined) {
        name = clicked.context.name;
      }
      if (name != undefined) {
        if (method == "post") {
          if (data) {
            data += "&" + name + "=1";
          } else {
            data = name + "=1";
          }
        } else {
          if (action.indexOf('?')) {
            action += "&" + name + "=1";
          } else {
            action += "?" + name + "=1";
          }
        }
      }
      do_request(target, action, method, data);
      return false;
    });
  }

  function do_form_safe(id)
  {
    $form = $('#' + id);
    var $nested_forms = $(form).find('div.formmarker');
    var $noblock_input = $(form).find('input[name=noblocking]').eq(0);
    var pass = (!$nested_forms.length || $noblock_input.val() === 'true') ? true : false;
    if (!pass) {
      alert("Zamknij najpierw wszystkie podformularze aby kontynuuować.");
      return false;
    }
    return true;
  }


  function do_refresh(ajax)
  {
    $('#' + ajax + ' a.refresh').each(function() {  
      var ajax = get_param($(this).attr('href'), 'ajax');
      if (ajax) {
        do_request(ajax, $(this).attr('href'));
      }
    });
  }

  // Zastępowanie na wywołania AJAX wszystkich <a href="...">, które mają 
  // parametr ajax= w ramach elementu, którego wartość jest w zmiennej $ajax
  function do_a_ajax(ajax)
  {
    $('#' + ajax + ' a').each(function() {
      var target = get_param($(this).attr('href'), 'ajax');
      // Parametr noajax blokuje dodawanie ajaxowego click()
      if (target && !get_param($(this).attr('href'), 'noajax')) {
        $(this).click(function() {
          do_request(target, $(this).attr('href'), 'get');
          return false;
        });
      }
    });
  }

  function do_response(ajax, data)
  {
	  // TODO: obsługa messages
    if (data.html != undefined) 
    {      
      $('#' + ajax).html(data.html);
    } else if (data.redirect != undefined) {
      if (data.redirect.method == "post") {
        if (data.redirect.data != undefined) {
          $.post(
            data.redirect.action,
            data.redirect.data,
            function (data) {
              do_response(ajax, data);
            }
          );
        } else {
          $.post(
            data.redirect.action,
            function (data) {
              do_response(ajax, data);
            }
          );
        }
      } else if (data.redirect.method == "href") {
        document.location.href = data.redirect.action;
      } else {
        $.get(
          data.redirect.action,
          function (data) {
            do_response(ajax, data);
          }
        );
      }
    // TODO: usunąć START
    } else if (data.action != undefined) {
      if (data.method == "post") {
        if (data.data != undefined) {
          $.post(
            data.action,
            data.data,
            function (data) {
              do_response(ajax, data);
            }
          );
        } else {
          $.post(
            data.action,
            function (data) {
              do_response(ajax, data);
            }
          );
        }
      } else if (data.method == "href") {
        document.location.href = data.action;
      } else {
        $.get(
          data.action,
          function (data) {
            do_response(ajax, data);
          }
        );
      }
    // TODO: usunąc STOP
    }
    if (data.refresh != undefined) {
      if ($.isArray(data.refresh)) {
        $.each(
          data.refresh,
          function (intIndex, objValue) {
            do_refresh(objValue);
          }
        );
      } else {
        do_refresh(data.refresh);
      }
    }
  }

  function get_param(url, param) {
    var query = url.substring(url.indexOf('?') + 1);
    if (query) {
      var vars = query.split("&");
      for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if (pair[0] == param) {
          return pair[1];
        }
      } 
    }
    return false;
  }

  function do_datepicker_ajax(ajax) {
    Date.firstDayOfWeek = 0;
    Date.format = 'yyyy-mm-dd';
    ajax = ajax == undefined ? '' : '#' + ajax + ' ';
    $(ajax + '.datepicker').datePicker();
    $(ajax + '.datepicker-clickable').datePicker({clickInput:true});
    $(ajax + '.datepicker-clickable[id$=date_from]:text').change(function() {
	    var start_date = new Date($(this).val());
	    start_date.setDate(start_date.getDate() + 1);
	    var dd = start_date.getDate();
	    var mm = start_date.getMonth() + 1; // 0 is January, so we must add 1
	    var yyyy = start_date.getFullYear();
	    $(ajax + '.datepicker-clickable[id$=date_to]:text').datePicker({startDate: yyyy + '-' + mm + '-' + dd});
    });
  }

  $(document).delegate('a.ajax_close', 'click', function() {
    if ($(this).attr('rel')) {
      $data = $(document).find('#' + $(this).attr('rel'));
    } else {
      var $data = $(this).closest('div');
    }
    $data.remove();
    return false;
  });

  $(document).delegate('a.ajax_popup', 'click', function(){
    popup = window.open($(this).attr('href'),'','height=400,width=600');
    if (window.focus) { popup.focus() }
    return false;
  });

</script>
</head>
<body itemscope="" itemtype="http://schema.org/WebPage">
<!--<div id="cookies-info" style="padding: 10px; display: none; font-size: 0.8em; text-align: center; height: 15px;">
  <div style="position: relative; top: -0.25em; font-size: 1.5em; cursor: pointer; padding-left: 10px; float: right;" id="cookies-info-close">X</div>
-->
<div id="cookies-info" style="display: none; border: 1px solid black; background-color: #ffffff; padding: 10px; font-size: 0.9em; text-align: center; position: fixed; top: 5px; z-index: 10000000; right: 5px; width: 550px; border-radius: 5px; color: #000000;">
<div id="cookies-info-close" style="position: absolute; top: 0.25em; right: 0.25em; font-size: 1.5em; cursor: pointer;">X</div>
  Strona korzysta z plików cookies w celu realizacji usług i zgodnie z <a href="https://www.bmwstore.pl/pl/ciasteczka">Polityką Plików Cookies</a>. Możesz określić warunki przechowywania lub dostępu do plików cookies w Twojej przeglądarce.
</div>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    if ($.cookie('cookie-info') == undefined) {
      var $cookies_info = $(document).find('#cookies-info');
      $cookies_info.slideDown('slow');
      $cookies_info.find('#cookies-info-close').click(function (ev) {
        ev.preventDefault();
        $cookies_info.slideUp('fast');
        $.cookie('cookie-info', '1', { expires: 365, path: '/', domain: 'www.bmwstore.pl' });
      });
    }
  });
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pl_PL/all.js#xfbml=1";
  js.async = 1;
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="header">
<div class="wrap">
<h1 id="site-title">
<span class="structural">Oryginalne części do BMW</span>
</h1>
<p class="logo"></p>
<ul id="nav">
<li class="current"><a href="https://www.bmwstore.pl/pl/">Strona główna</a></li>
<li><a href="https://www.bmwstore.pl/pl/o-nas">O nas</a></li>
<li><a href="https://www.bmwstore.pl/pl/gwarancja">Gwarancja</a></li>
<li><a href="https://www.bmwstore.pl/pl/dlaczego-oryginalne-czesci">Dlaczego oryginalne częsci?</a></li>
<li><a href="https://www.bmwstore.pl/pl/praca">Praca</a></li>
<li><a href="https://www.bmwstore.pl/pl/kontakt">Kontakt</a></li>
<li><a href="https://www.bmwstore.pl/pl/zwrot-towaru">ZWROT TOWARU</a></li>
</ul>
</div>
</div><!-- / #header -->
<div id="teaser">
<p class="head">ORYGINALNE CZĘŚCI <span>do BMW</span></p>
<img alt="" height="280" src="https://www.bmwstore.pl/static/pl/images/baner_zima_1024x280_2.jpg" usemap="#teaser" width="1024"/>
<map name="teaser">
<area alt="" coords="17,230,150,264" href="mailto:sklep@bmwstore.pl?subject=Koła%20zimowe%20BMW" shape="rect" title="Kontakt"/>
<area alt="" coords="170,226,356,263" href="https://www.bmw.pl/pl/topics/offers-and-services/original-bmw-accessories/complete-wheel-configurator.html#/bookmark=aHR0cHM6Ly93d3cuYm13LWFjY2Vzc29yaWVzLWNvbmZpZ3VyYXRvci5jb20vSXBwcy9ibXdvYWM/cGFnZXF1ZXJ5PW9hYyZicmFuZD1ibXcmbWFya2V0PTI0NzU1LVBMJmxhbmc9UEwjdG9w" shape="rect" target="_blank" title="Konfigurator kół"/>
</map>
</div><!-- -->
<div id="main">
<div id="content">
<div class="message msg-warning">
<p>W związku z obecną sytuacją epidemiologiczną, czas realizacji zamówień może ulec wydłużeniu</p>
</div>
<div class="message msg-success">
<p>Darmowa wysyłka od kwoty zamówienia powyżej 1000 zł</p>
</div>
<div class="partsearch box">
<h2 class="color-b">Wpisz numer części</h2>
<form action="https://www.bmwstore.pl/pl/katalog/produkt?return=aHR0cHM6Ly93d3cuYm13c3RvcmUucGwvcGwv" class="simplesearch" method="get">
<fieldset>
<p class="fields">
<input class="text" name="part_id" type="text"/>
<input class="btn" name="part_search" type="submit" value="Szukaj"/>
</p>
<p>Podaj numer części BMW (wystarczy wpisać 7 ostatnich cyfr). Przykład: <a href="https://www.bmwstore.pl/pl/katalog/produkt?part_id=6855006" target="_blank">6855006</a></p>
</fieldset>
</form>
</div>
<div class="caridentification box box_b">
<div class="in">
<div class="lead">
<h2>Zidentyfikuj swoje BMW</h2>
<p>Podaj typ i model swojego BMW, aby łatwiej znaleźć pasujące do niego części.</p>
</div>
<div class="a">
<h3 class="color-b">Wyszukiwanie modelu BMW <span>Na podstawie nr VIN:</span></h3>
<form action="https://www.bmwstore.pl/pl/pojazd?return=aHR0cHM6Ly93d3cuYm13c3RvcmUucGwvcGwv" class="simplesearch" method="post">
<fieldset>
<p class="fields">
<input class="text" name="vin" type="text"/>
<input class="btn" name="vin_search" type="submit" value="Szukaj"/>
</p>
<p>Podaj numer VIN (wystarczy wpisać 7 ostatnich znaków)</p>
<!--
             <p><a href="">Co to jest VIN i jak go znaleźć?</a></p>
           -->
</fieldset>
</form>
</div>
<div class="b">
<h3 class="color-b">Wyszukiwanie modelu BMW i Mini <span>Na podstawie katalogu:</span></h3>
<div style="text-align: center;">
<ul>
<li style="margin-right: 5px;">
<img alt="" height="44" src="https://www.bmwstore.pl/static/pl/images/carid-icon-car.png" width="79"/>
<a class="btn" href="https://www.bmwstore.pl/pl/konfigurator/krok1/samochod">BMW</a>
</li>
<li style="margin-right: 5px;">
<img alt="" height="44" src="https://www.bmwstore.pl/static/pl/images/carid-icon-mini.png" width="79"/>
<a class="btn" href="https://www.bmwstore.pl/pl/konfigurator/krok2/samochod/MINI">Mini</a>
</li>
<li style="margin-right: 5px;">
<img alt="" height="44" src="https://www.bmwstore.pl/static/pl/images/carid-icon-moto.png" width="79"/>
<a class="btn" href="https://www.bmwstore.pl/pl/konfigurator/krok1/motocykl">Motocykl</a>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="box" style="position: relative; background-image: url(/static/pl/images/valueline.png); width: 688px; height: 168px;">
<div style="position: absolute; margin: 0px; padding: 0px; bottom: 10px; left: 120px; height: 24px;">
<form action="https://www.bmwstore.pl/pl/pojazd?valueline=1&amp;return=aHR0cHM6Ly93d3cuYm13c3RvcmUucGwvcGwv" class="simplesearch" method="post">
<fieldset>
<p class="fields">
<input class="text" name="vin" size="8" type="text"/>
<input class="btn" name="vin_search" type="submit" value="Szukaj"/>
</p>
</fieldset>
</form>
</div>
</div>
<div class="box">
<a href="https://www.bmw.pl/pl/topics/offers-and-services/original-bmw-accessories/przeglad.html" target="_blank">
<img src="/static/pl/M_PERFORMANCE_baner.jpg"/>
</a>
</div>
<div class="box box_c">
<div class="hd">
<h2>Części w wyprzedaży</h2>
</div>
<ul class="productlist v1">
<li>
<div class="in">
<div class="thumb" style="text-align: center;">
<a href="https://www.bmwstore.pl/pl/katalog/produkt/6862774-obr-bezszp-z-met-lekk-czarna-ii-mat"><img alt="" src="https://www.bmwstore.pl/pl/katalog/produkt/obrazek/0/M190xM240"/></a>
<div class="desc" style="height: 50px;">
<h3><a href="https://www.bmwstore.pl/pl/katalog/produkt/6862774-obr-bezszp-z-met-lekk-czarna-ii-mat">Obr. bezszp. z met. lekk. czarna II mat</a></h3>
</div>
<div class="price">
<div class="bonus" style="float: left;">
<span class="color-d" style="font-size: 2.0em; font-weight: bold;">33,51%</span>
</div>
<div class="prices" style="float: right;">
<span class="color-d" style="font-size: 1.2em; font-weight: bold;">2 999,00 zł</span><br/>
<span style="font-weight: bold;"><del>4 510,84 zł</del></span>
</div>
<br style="clear: both;"/><br/>
<form action="https://www.bmwstore.pl/pl/koszyk?return=aHR0cHM6Ly93d3cuYm13c3RvcmUucGwvcGwv" method="post">
<p class="tocart">
<button class="btn btn-tocart" name="add" value="6862774"><i></i> Do Koszyka</button>
</p>
</form>
</div>
</div>
</div>
</li><li>
<div class="in">
<div class="thumb" style="text-align: center;">
<a href="https://www.bmwstore.pl/pl/katalog/produkt/6787582-obr-bezsz-z-met-lekk-tocz-na-polysk"><img alt="" src="https://www.bmwstore.pl/pl/katalog/produkt/obrazek/0/M190xM240"/></a>
<div class="desc" style="height: 50px;">
<h3><a href="https://www.bmwstore.pl/pl/katalog/produkt/6787582-obr-bezsz-z-met-lekk-tocz-na-polysk">Obr. bezsz. z met. lekk. tocz. na połysk</a></h3>
</div>
<div class="price">
<div class="bonus" style="float: left;">
<span class="color-d" style="font-size: 2.0em; font-weight: bold;">34,64%</span>
</div>
<div class="prices" style="float: right;">
<span class="color-d" style="font-size: 1.2em; font-weight: bold;">2 699,00 zł</span><br/>
<span style="font-weight: bold;"><del>4 129,62 zł</del></span>
</div>
<br style="clear: both;"/><br/>
<form action="https://www.bmwstore.pl/pl/koszyk?return=aHR0cHM6Ly93d3cuYm13c3RvcmUucGwvcGwv" method="post">
<p class="tocart">
<button class="btn btn-tocart" name="add" value="6787582"><i></i> Do Koszyka</button>
</p>
</form>
</div>
</div>
</div>
</li><li>
<div class="in">
<div class="thumb" style="text-align: center;">
<a href="https://www.bmwstore.pl/pl/katalog/produkt/6768861-kolo-dojazdowe-stalowe-czarne"><img alt="" src="https://www.bmwstore.pl/pl/katalog/produkt/obrazek/0/M190xM240"/></a>
<div class="desc" style="height: 50px;">
<h3><a href="https://www.bmwstore.pl/pl/katalog/produkt/6768861-kolo-dojazdowe-stalowe-czarne">Koło dojazdowe stalowe czarne</a></h3>
</div>
<div class="price">
<div class="bonus" style="float: left;">
<span class="color-d" style="font-size: 2.0em; font-weight: bold;">47,04%</span>
</div>
<div class="prices" style="float: right;">
<span class="color-d" style="font-size: 1.2em; font-weight: bold;">199,00 zł</span><br/>
<span style="font-weight: bold;"><del>375,77 zł</del></span>
</div>
<br style="clear: both;"/><br/>
<form action="https://www.bmwstore.pl/pl/koszyk?return=aHR0cHM6Ly93d3cuYm13c3RvcmUucGwvcGwv" method="post">
<p class="tocart">
<button class="btn btn-tocart" name="add" value="6768861"><i></i> Do Koszyka</button>
</p>
</form>
</div>
</div>
</div>
</li><li>
<div class="in">
<div class="thumb" style="text-align: center;">
<a href="https://www.bmwstore.pl/pl/katalog/produkt/6774774-obrecz-kola-aluminiowa-polerowana"><img alt="" src="https://www.bmwstore.pl/pl/katalog/produkt/obrazek/0/M190xM240"/></a>
<div class="desc" style="height: 50px;">
<h3><a href="https://www.bmwstore.pl/pl/katalog/produkt/6774774-obrecz-kola-aluminiowa-polerowana">Obręcz koła aluminiowa, polerowana</a></h3>
</div>
<div class="price">
<div class="bonus" style="float: left;">
<span class="color-d" style="font-size: 2.0em; font-weight: bold;">41,84%</span>
</div>
<div class="prices" style="float: right;">
<span class="color-d" style="font-size: 1.2em; font-weight: bold;">399,00 zł</span><br/>
<span style="font-weight: bold;"><del>686,14 zł</del></span>
</div>
<br style="clear: both;"/><br/>
<form action="https://www.bmwstore.pl/pl/koszyk?return=aHR0cHM6Ly93d3cuYm13c3RvcmUucGwvcGwv" method="post">
<p class="tocart">
<button class="btn btn-tocart" name="add" value="6774774"><i></i> Do Koszyka</button>
</p>
</form>
</div>
</div>
</div>
</li><li>
<div class="in">
<div class="thumb" style="text-align: center;">
<a href="https://www.bmwstore.pl/pl/katalog/produkt/1936174-pokrywa-silnika"><img alt="" src="https://www.bmwstore.pl/pl/katalog/produkt/obrazek/0/M190xM240"/></a>
<div class="desc" style="height: 50px;">
<h3><a href="https://www.bmwstore.pl/pl/katalog/produkt/1936174-pokrywa-silnika">Pokrywa silnika</a></h3>
</div>
<div class="price">
<div class="bonus" style="float: left;">
<span class="color-d" style="font-size: 2.0em; font-weight: bold;">15,67%</span>
</div>
<div class="prices" style="float: right;">
<span class="color-d" style="font-size: 1.2em; font-weight: bold;">1 900,00 zł</span><br/>
<span style="font-weight: bold;"><del>2 253,32 zł</del></span>
</div>
<br style="clear: both;"/><br/>
<form action="https://www.bmwstore.pl/pl/koszyk?return=aHR0cHM6Ly93d3cuYm13c3RvcmUucGwvcGwv" method="post">
<p class="tocart">
<button class="btn btn-tocart" name="add" value="1936174"><i></i> Do Koszyka</button>
</p>
</form>
</div>
</div>
</div>
</li><li>
<div class="in">
<div class="thumb" style="text-align: center;">
<a href="https://www.bmwstore.pl/pl/katalog/produkt/7138522-schowek-tylny"><img alt="" src="https://www.bmwstore.pl/pl/katalog/produkt/obrazek/0/M190xM240"/></a>
<div class="desc" style="height: 50px;">
<h3><a href="https://www.bmwstore.pl/pl/katalog/produkt/7138522-schowek-tylny">Schowek tylny</a></h3>
</div>
<div class="price">
<div class="bonus" style="float: left;">
<span class="color-d" style="font-size: 2.0em; font-weight: bold;">32,18%</span>
</div>
<div class="prices" style="float: right;">
<span class="color-d" style="font-size: 1.2em; font-weight: bold;">60,00 zł</span><br/>
<span style="font-weight: bold;"><del>88,48 zł</del></span>
</div>
<br style="clear: both;"/><br/>
<form action="https://www.bmwstore.pl/pl/koszyk?return=aHR0cHM6Ly93d3cuYm13c3RvcmUucGwvcGwv" method="post">
<p class="tocart">
<button class="btn btn-tocart" name="add" value="7138522"><i></i> Do Koszyka</button>
</p>
</form>
</div>
</div>
</div>
</li> <p style="float: right; align: right;">
<a href="https://www.bmwstore.pl/pl/katalog/wyprzedaz">zobacz wszystkie części w wyprzedaży</a>
</p>
</ul>
</div>
<div class="box box_c">
<div class="hd">
<h2>Diagramy ostatnio przeglądane przez klientów</h2>
</div>
<ul class="productlist v1">
<li>
<div class="in">
<div class="thumb">
<a href="https://www.bmwstore.pl/pl/katalog/diagram/52_4146-torba-na-narty-i-snowboard/i59668/sN">
<img alt="" height="135" src="https://www.bmwstore.pl/pl/katalog/obrazek/210747/190x135" width="190"/>
</a>
</div>
<div class="desc">
<h3><a href="https://www.bmwstore.pl/pl/katalog/diagram/52_4146-torba-na-narty-i-snowboard/i59668/sN">Torba na narty i snowboard</a></h3>
<p>BMW 2' F46 Gran Tourer LCI 218dX B47B Gran Tourer ECE</p>
</div>
</div>
</li><li>
<div class="in">
<div class="thumb">
<a href="https://www.bmwstore.pl/pl/katalog/diagram/52_3574-fot-przedn-elektryka-i-napedy/i52547/sA">
<img alt="" height="135" src="https://www.bmwstore.pl/pl/katalog/obrazek/231502/190x135" width="190"/>
</a>
</div>
<div class="desc">
<h3><a href="https://www.bmwstore.pl/pl/katalog/diagram/52_3574-fot-przedn-elektryka-i-napedy/i52547/sA">Fot. przedn., elektryka i napędy</a></h3>
<p>BMW 5' F07 GT 535iX Gran Turismo USA</p>
</div>
</div>
</li><li>
<div class="in">
<div class="thumb">
<a href="https://www.bmwstore.pl/pl/katalog/diagram/03_1665-zest-pierw-pomocy-uniwersalny/i54644/sN">
<img alt="" height="135" src="https://www.bmwstore.pl/pl/katalog/obrazek/250404/190x135" width="190"/>
</a>
</div>
<div class="desc">
<h3><a href="https://www.bmwstore.pl/pl/katalog/diagram/03_1665-zest-pierw-pomocy-uniwersalny/i54644/sN">Zest. pierw. pomocy, uniwersalny</a></h3>
<p>BMW 6' F12 640dX Cabrio ECE</p>
</div>
</div>
</li><li>
<div class="in">
<div class="thumb">
<a href="https://www.bmwstore.pl/pl/katalog/diagram/33_1377-polos-tylnego-mostulozyskowanie-kola/i56433/kL/sN/r2015/m10">
<img alt="" height="135" src="https://www.bmwstore.pl/pl/katalog/obrazek/480438/190x135" width="190"/>
</a>
</div>
<div class="desc">
<h3><a href="https://www.bmwstore.pl/pl/katalog/diagram/33_1377-polos-tylnego-mostulozyskowanie-kola/i56433/kL/sN/r2015/m10">Półoś tylnego mostu/łożyskowanie koła</a></h3>
<p>BMW X4 F26 X4 28iX SAC USA</p>
</div>
</div>
</li><li>
<div class="in">
<div class="thumb">
<a href="https://www.bmwstore.pl/pl/katalog/diagram/63_0550-poj-czesci-reflektora-ksenonowego/i47676/kL/sN/r2001/m03">
<img alt="" height="135" src="https://www.bmwstore.pl/pl/katalog/obrazek/203972/190x135" width="190"/>
</a>
</div>
<div class="desc">
<h3><a href="https://www.bmwstore.pl/pl/katalog/diagram/63_0550-poj-czesci-reflektora-ksenonowego/i47676/kL/sN/r2001/m03">Poj. części reflektora ksenonowego</a></h3>
<p>BMW 3' E46 330xi Touring ECE</p>
</div>
</div>
</li><li>
<div class="in">
<div class="thumb">
<a href="https://www.bmwstore.pl/pl/katalog/diagram/52_3574-fot-przedn-elektryka-i-napedy/i52405/sA">
<img alt="" height="135" src="https://www.bmwstore.pl/pl/katalog/obrazek/231502/190x135" width="190"/>
</a>
</div>
<div class="desc">
<h3><a href="https://www.bmwstore.pl/pl/katalog/diagram/52_3574-fot-przedn-elektryka-i-napedy/i52405/sA">Fot. przedn., elektryka i napędy</a></h3>
<p>BMW 5' F07 GT 550iX Gran Turismo ECE</p>
</div>
</div>
</li><li>
<div class="in">
<div class="thumb">
<a href="https://www.bmwstore.pl/pl/katalog/diagram/41_1598-scianka-przednia-poj-czesci/i50161/kL/sN/r2009/m11">
<img alt="" height="135" src="https://www.bmwstore.pl/pl/katalog/obrazek/133378/190x135" width="190"/>
</a>
</div>
<div class="desc">
<h3><a href="https://www.bmwstore.pl/pl/katalog/diagram/41_1598-scianka-przednia-poj-czesci/i50161/kL/sN/r2009/m11">Ścianka przednia, poj. części</a></h3>
<p>BMW 3' E92 320d Coupé ECE</p>
</div>
</div>
</li><li>
<div class="in">
<div class="thumb">
<a href="https://www.bmwstore.pl/pl/katalog/diagram/03_3610-driver-utility-set/i47249/sM">
<img alt="" height="135" src="https://www.bmwstore.pl/pl/katalog/obrazek/190337/190x135" width="190"/>
</a>
</div>
<div class="desc">
<h3><a href="https://www.bmwstore.pl/pl/katalog/diagram/03_3610-driver-utility-set/i47249/sM">Driver utility set</a></h3>
<p>BMW 3' E30 316 2-drzwiowy ECE</p>
</div>
</div>
</li><li>
<div class="in">
<div class="thumb">
<a href="https://www.bmwstore.pl/pl/katalog/diagram/65_2304-kontroler/i57285/sN">
<img alt="" height="135" src="https://www.bmwstore.pl/pl/katalog/obrazek/471923/190x135" width="190"/>
</a>
</div>
<div class="desc">
<h3><a href="https://www.bmwstore.pl/pl/katalog/diagram/65_2304-kontroler/i57285/sN">Kontroler</a></h3>
<p>BMW 2' F22 218d B47 Coupé ECE</p>
</div>
</div>
</li> </ul>
</div>
</div><!-- / #content -->
<div id="side">
<div class="minicart box">
<h2><a href="https://www.bmwstore.pl/pl/koszyk">Koszyk</a> (0)</h2>
<p>wartość koszyka 0,00 zł</p>
</div>
<div class="box box_login">
<h2>Logowanie</h2>
<form action="https://www.bmwstore.pl/pl/logowanie?return=aHR0cHM6Ly93d3cuYm13c3RvcmUucGwvcGwv" method="post">
<fieldset>
<div>
<label>e-mail:</label>
<input class="text" name="u_id_or_name_or_email" type="text"/>
</div>
<div>
<label>hasło:</label>
<input class="text" name="u_password" type="password"/>
</div>
<div class="align-right">
<input class="btn" name="login_submit" type="submit" value="Logowanie"/>
</div>
<p>
        Nie masz konta <a class="super" href="https://www.bmwstore.pl/pl/rejestracja">Zarejestruj się</a>
</p>
<p>
        Nie pamiętasz hasła <a class="super" href="https://www.bmwstore.pl/pl/odzyskiwanie-hasla">Odzyskaj je</a>
</p>
</fieldset>
</form>
</div>
<div class="sep"></div>
<div class="box">
<ul class="bulleted-a">
<li><a href="https://www.bmwstore.pl/pl/o-nas">O nas</a></li>
<li><a href="https://www.bmwstore.pl/pl/gwarancja">Gwarancja BMW</a></li>
<li><a href="https://www.bmwstore.pl/pl/dlaczego-oryginalne-czesci">Dlaczego oryginalne części?</a></li>
<li><a href="https://www.bmwstore.pl/pl/dostawa-i-platnosci">Dostawa i płatności</a></li>
<li><a href="https://www.bmwstore.pl/pl/regulamin">Regulamin</a></li>
<li><a href="https://www.bmwstore.pl/pl/praca">Praca</a></li>
<li><a href="https://www.bmwstore.pl/pl/kontakt">Kontakt</a></li>
</ul>
</div>
<div class="sep"></div>
<div class="box">
<h2>BMW Lifestyle</h2>
<center>
<a href="https://www.bmwstore.pl/static/pl/BMW_Lifestyle_Main_16_18_POL.pdf" target="_blank"><img src="https://www.bmwstore.pl/static/pl/BMW_Lifestyle_Main_16_18_POL.png"/></a>
<a href="https://www.bmwstore.pl/static/pl/Lifestyle_Sport_POL.pdf" target="_blank"><img src="https://www.bmwstore.pl/static/pl/Lifestyle_Sport_POL.png"/></a>
</center>
<ul class="bulleted-a">
<li><a href="https://www.bmwstore.pl/static/pl/tabela_rozmiarow.jpg" target="_blank">Tabela rozmiarów</a></li>
</ul>
</div>
<div class="sep"></div>
<div class="box">
<h2>BMWstore.pl to:</h2>
<ul class="bulleted-a">
<li style="color: #003382;">autoryzowany dealer BMW</li>
<li style="color: #003382;">wyłącznie oryginalne części</li>
<li style="color: #003382;">gwarancja BMW</li>
<li style="color: #003382;">wysokie rabaty (nawet do 15%)</li>
</ul>
</div>
<div class="sep"></div>
<div class="box box_contact">
<h2>Kontakt</h2>
<ul class="bulleted-a">
<li>
      e-mail: <a href="mailto:sklep@bmwstore.pl"><strong class="big">sklep@BMWstore.pl</strong></a>
</li>
<li>
      pracujemy w godzinach:<br/> od 10.00 do 16.00<br/>od poniedziałku do piątku
    </li>
<!--
    <li>
    dział częsci samochodowych:<br/>tel:&nbsp;<strong class="big">33&nbsp;4996100</strong>
    </li>
    <li>
    dział częsci motocyklowych<br/>tel:&nbsp;<strong class="big">33&nbsp;4996101</strong>
    </li>
    -->
<li>
      dział częsci samochodowych<br/>
      dział częsci motocyklowych<br/><br/>
<strong class="big">691 336 242</strong>
</li>
<li>
    bardzo proszę o przygotowanie nr VIN lub nr zamówienia BMWstore
    </li>
</ul>
<p style="text-align: right;">
<a href="https://www.bmwstore.pl/pl/kontakt">Więcej</a>
</p>
</div>
<div class="box box_fb">
<div class="fb-like-box" data-header="true" data-height="290" data-href="http://www.facebook.com/BMWstorepl" data-show-faces="true" data-stream="false" data-width="229"></div>
</div>
</div><!-- / #side -->
</div><!-- / #main -->
<div id="footer">
<div class="wrap">
<ul>
<li><a href="https://www.bmwstore.pl/pl/regulamin">Regulamin</a></li>
<li><a href="https://www.bmwstore.pl/pl/ciasteczka">Polityka zarządzania plikami cookies</a></li>
<li><a href="https://www.bmwstore.pl/pl/kontakt">Kontakt</a></li>
</ul>
</div>
</div><!-- / #footer -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-4421870-13']);
  _gaq.push(['_trackPageview']);
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- Google Code for BMWstore - all Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1055622308;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "y9i2CMaRxAIQpImu9wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script src="//www.googleadservices.com/pagead/conversion.js" type="text/javascript">
</script>
<noscript>
<div style="display:inline;">
<img alt="" height="1" src="//www.googleadservices.com/pagead/conversion/1055622308/?label=y9i2CMaRxAIQpImu9wM&amp;guid=ON&amp;script=0" style="border-style:none;" width="1"/>
</div>
</noscript>
<script type="text/javascript">
  var goadservicesq = goadservicesq || [];
    goadservicesq.push(["_ENTRY"]);
    (function() {
    var goadservices = document.createElement('script');
    goadservices.type = 'text/javascript';
    goadservices.async = true;
    goadservices.src = '//t.goadservices.com/engine/7deb144b-b569-4f21-9a17-462315337330';

    var id_s = document.cookie.indexOf('__goadservices=');
    if (id_s != -1) {
      id_s += 15;
      var id_e = document.cookie.indexOf(';', id_s);
      if (id_e == -1) {
        id_e = document.cookie.length;
      }
      goadservices.src += '?id='+document.cookie.substring(id_s, id_e);
    }

    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(goadservices, s);
  })();
</script>
</body>
</html>
