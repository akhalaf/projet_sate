<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"https:\/\/www.veterinarydiscussion.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.2.29"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.veterinarydiscussion.com/wp-content/plugins/LayerSlider/static/css/layerslider.css?ver=5.1.1" id="layerslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900|Open+Sans:300|Indie+Flower:regular|Oswald:300,regular,700&amp;subset=latin,latin-ext" id="ls-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.veterinarydiscussion.com/wp-content/themes/veterinary/css/sumoselect.css?ver=4.2.29" id="sumoselectcss-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.veterinarydiscussion.com/wp-content/plugins/add-to-any/addtoany.min.css?ver=1.11" id="A2A_SHARE_SAVE-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.veterinarydiscussion.com/wp-includes/js/jquery/jquery.js?ver=1.11.2" type="text/javascript"></script>
<script src="https://www.veterinarydiscussion.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1" type="text/javascript"></script>
<script src="https://www.veterinarydiscussion.com/wp-content/plugins/LayerSlider/static/js/layerslider.kreaturamedia.jquery.js?ver=5.1.1" type="text/javascript"></script>
<script src="https://www.veterinarydiscussion.com/wp-content/plugins/LayerSlider/static/js/greensock.js?ver=1.11.2" type="text/javascript"></script>
<script src="https://www.veterinarydiscussion.com/wp-content/plugins/LayerSlider/static/js/layerslider.transitions.js?ver=5.1.1" type="text/javascript"></script>
<script src="https://www.veterinarydiscussion.com/wp-content/plugins/html5-responsive-faq/js/hrf-script.js?ver=4.2.29" type="text/javascript"></script>
<script src="https://www.veterinarydiscussion.com/wp-content/themes/veterinary/js/veterinary.js?ver=4.2.29" type="text/javascript"></script>
<script src="https://www.veterinarydiscussion.com/wp-content/themes/veterinary/js/jRate.js?ver=4.2.29" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var MyAjax = {"ajaxurl":"https:\/\/www.veterinarydiscussion.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script src="https://www.veterinarydiscussion.com/wp-content/themes/veterinary/js/wp-ajax.js?ver=4.2.29" type="text/javascript"></script>
<script src="https://www.veterinarydiscussion.com/wp-content/themes/veterinary/js/jquery-ui.js?ver=4.2.29" type="text/javascript"></script>
<script src="https://www.veterinarydiscussion.com/wp-content/themes/veterinary/js/jquery.sumoselect.js?ver=4.2.29" type="text/javascript"></script>
<link href="https://www.veterinarydiscussion.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.veterinarydiscussion.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.2.29" name="generator"/>
<script type="text/javascript"><!--
var a2a_config=a2a_config||{},wpa2a={done:false,html_done:false,script_ready:false,script_load:function(){var a=document.createElement('script'),s=document.getElementsByTagName('script')[0];a.type='text/javascript';a.async=true;a.src='https://static.addtoany.com/menu/page.js';s.parentNode.insertBefore(a,s);wpa2a.script_load=function(){};},script_onready:function(){wpa2a.script_ready=true;if(wpa2a.html_done)wpa2a.init();},init:function(){for(var i=0,el,target,targets=wpa2a.targets,length=targets.length;i<length;i++){el=document.getElementById('wpa2a_'+(i+1));target=targets[i];a2a_config.linkname=target.title;a2a_config.linkurl=target.url;if(el){a2a.init('page',{target:el});el.id='';}wpa2a.done=true;}wpa2a.targets=[];}};a2a_config.callbacks=a2a_config.callbacks||[];a2a_config.callbacks.push({ready:wpa2a.script_onready});
a2a_config.onclick=1;
//--></script>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<link href="https://www.veterinarydiscussion.com/wp-content/themes/veterinary/style.css" rel="stylesheet"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<link href="https://www.veterinarydiscussion.com/wp-content/themes/veterinary/favicon.jpg" rel="icon" type="image/x-icon"/>
<link href="https://www.veterinarydiscussion.com/wp-content/themes/veterinary/favicon.jpg" rel="shortcut icon" type="image/x-icon"/>
<script type="text/javascript">
			var base_url='https://www.veterinarydiscussion.com';
		</script>
<!-- 	<link rel="shortcut icon" href="https://www.veterinarydiscussion.com/wp-content/themes/veterinary/img/favicon.ico" /> -->
</head>
<body class="error404 off" id="page-id-">
<div class="page_loader">
<img src="https://www.veterinarydiscussion.com/wp-content/themes/veterinary/img/loader.gif"/>
</div>
<div class="wrap_header">
<div class="res_menu">
<div class="menu-mobile_view-menu-container"><ul class="nav" id="menu-mobile_view-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-195" id="menu-item-195"><a href="https://www.veterinarydiscussion.com/categories/">Categories</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-196" id="menu-item-196"><a href="https://www.veterinarydiscussion.com/categories/emergency-medicine/">Emergency Medicine</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-197" id="menu-item-197"><a href="https://www.veterinarydiscussion.com/categories/diagnostic-imaging/">Diagnostic Imaging</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-198" id="menu-item-198"><a href="https://www.veterinarydiscussion.com/categories/clinical-and-anatomic-pathology/">Clinical and Anatomic Pathology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-199" id="menu-item-199"><a href="https://www.veterinarydiscussion.com/categories/surgery/">Surgery</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-200" id="menu-item-200"><a href="https://www.veterinarydiscussion.com/categories/neurology/">Neurology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-201" id="menu-item-201"><a href="https://www.veterinarydiscussion.com/categories/internal-medicine/">Internal medicine</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-202" id="menu-item-202"><a href="https://www.veterinarydiscussion.com/categories/exotics/">Exotics</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-203" id="menu-item-203"><a href="https://www.veterinarydiscussion.com/categories/dentistry/">Dentistry</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-204" id="menu-item-204"><a href="https://www.veterinarydiscussion.com/categories/dermatology/">Dermatology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205" id="menu-item-205"><a href="https://www.veterinarydiscussion.com/categories/equine-specialty/">Equine specialty</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-206" id="menu-item-206"><a href="https://www.veterinarydiscussion.com/categories/ophthalmology/">Ophthalmology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-207" id="menu-item-207"><a href="https://www.veterinarydiscussion.com/categories/oncology/">Oncology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-208" id="menu-item-208"><a href="https://www.veterinarydiscussion.com/categories/practice-management-and-stress/">Practice management and stress</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209" id="menu-item-209"><a href="https://www.veterinarydiscussion.com/categories/production-animal-medicine/">Production animal medicine</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-210" id="menu-item-210"><a href="https://www.veterinarydiscussion.com/categories/general-practice/">General Practice</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-211" id="menu-item-211"><a href="https://www.veterinarydiscussion.com/categories/vet-tech-and-vet-students/">Vet Tech and Vet Students</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-212" id="menu-item-212"><a href="https://www.veterinarydiscussion.com/categories/pet-owners-hangout/">Pet Owners Hangout</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-213" id="menu-item-213"><a href="https://www.veterinarydiscussion.com/about-us/">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-214" id="menu-item-214"><a href="https://www.veterinarydiscussion.com/healthy-article/">Pet Health Article</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-240" id="menu-item-240"><a href="http://www.merckvetmanual.com/mvm/resources/list_of_tables/section/table.html">Veterinary Professionals</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-239" id="menu-item-239"><a href="http://www.merckvetmanual.com/pethealth/full-sections.html">Pet Owners</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-215" id="menu-item-215"><a href="https://www.veterinarydiscussion.com/blog/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-216" id="menu-item-216"><a href="https://www.veterinarydiscussion.com/search-post/">Search Post</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-217" id="menu-item-217"><a href="https://www.veterinarydiscussion.com/contact-us/">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-224" id="menu-item-224"><a href="https://www.veterinarydiscussion.com/faq/">FAQ</a></li>
</ul></div> <ul>
<li><a href="https://www.veterinarydiscussion.com/login">Login</a></li>
</ul>
</div>
<div class="header" id="header">
<div class="container">
<div class="header-menu">
<div class="header_logo">
<a href="https://www.veterinarydiscussion.com">
<img alt="veterinary" src="https://www.veterinarydiscussion.com/wp-content/themes/veterinary/img/logo.png" title="veterinary"/>
</a>
</div>
<div class="menu-login">
<nav class="site-navigation primary-navigation menu" id="primary-navigation" role="navigation">
<div class="menu-main-menu-container"><ul class="nav navbar-nav navbar-center" id="menu-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-13" id="menu-item-13"><a href="https://www.veterinarydiscussion.com/categories/">Categories</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22" id="menu-item-22"><a href="https://www.veterinarydiscussion.com/categories/emergency-medicine/">Emergency Medicine</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32" id="menu-item-32"><a href="https://www.veterinarydiscussion.com/categories/diagnostic-imaging/">Diagnostic Imaging</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31" id="menu-item-31"><a href="https://www.veterinarydiscussion.com/categories/clinical-and-anatomic-pathology/">Clinical and Anatomic Pathology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-118" id="menu-item-118"><a href="https://www.veterinarydiscussion.com/categories/surgery/">Surgery</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-117" id="menu-item-117"><a href="https://www.veterinarydiscussion.com/categories/neurology/">Neurology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-116" id="menu-item-116"><a href="https://www.veterinarydiscussion.com/categories/internal-medicine/">Internal medicine</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-115" id="menu-item-115"><a href="https://www.veterinarydiscussion.com/categories/exotics/">Exotics</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-114" id="menu-item-114"><a href="https://www.veterinarydiscussion.com/categories/dentistry/">Dentistry</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-113" id="menu-item-113"><a href="https://www.veterinarydiscussion.com/categories/dermatology/">Dermatology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-112" id="menu-item-112"><a href="https://www.veterinarydiscussion.com/categories/equine-specialty/">Equine specialty</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-111" id="menu-item-111"><a href="https://www.veterinarydiscussion.com/categories/ophthalmology/">Ophthalmology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-110" id="menu-item-110"><a href="https://www.veterinarydiscussion.com/categories/oncology/">Oncology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-109" id="menu-item-109"><a href="https://www.veterinarydiscussion.com/categories/practice-management-and-stress/">Practice management and stress</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-108" id="menu-item-108"><a href="https://www.veterinarydiscussion.com/categories/production-animal-medicine/">Production animal medicine</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-107" id="menu-item-107"><a href="https://www.veterinarydiscussion.com/categories/general-practice/">General Practice</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-106" id="menu-item-106"><a href="https://www.veterinarydiscussion.com/categories/vet-tech-and-vet-students/">Vet Tech and Vet Students</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30" id="menu-item-30"><a href="https://www.veterinarydiscussion.com/categories/pet-owners-hangout/">Pet Owners Hangout</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16" id="menu-item-16"><a href="https://www.veterinarydiscussion.com/about-us/">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-125" id="menu-item-125"><a href="https://www.veterinarydiscussion.com/healthy-article/">Pet Health Article</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-242" id="menu-item-242"><a href="http://www.merckvetmanual.com/mvm/resources/list_of_tables/section/table.html">Veterinary Professionals</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-241" id="menu-item-241"><a href="http://www.merckvetmanual.com/pethealth/full-sections.html">Pet Owners</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-141" id="menu-item-141"><a href="https://www.veterinarydiscussion.com/blog/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-182" id="menu-item-182"><a href="https://www.veterinarydiscussion.com/search-post/">Search Post</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19" id="menu-item-19"><a href="https://www.veterinarydiscussion.com/contact-us/">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-225" id="menu-item-225"><a href="https://www.veterinarydiscussion.com/faq/">FAQ</a></li>
</ul></div> </nav>
<div class="login_dwn_btn">
<div class="login">
<a href="https://www.veterinarydiscussion.com/login">Login</a>
</div>
<div class="dwn_app">
<a href="https://itunes.apple.com/in/app/veterinary-discussion/id1028977765?mt=8" target="blank"><img alt="App Store" class="pr_size" src="https://www.veterinarydiscussion.com/wp-content/themes/veterinary/img/app_store.png" title="App Store"/></a>
</div>
</div>
</div>
<div class="res_menu_dwn_btn">
<div class="responsive_menu">
</div>
<div class="dwn_app">
<a href="https://itunes.apple.com/in/app/veterinary-discussion/id1028977765?mt=8" target="blank"><img alt="App Store" class="pr_size" src="https://www.veterinarydiscussion.com/wp-content/themes/veterinary/img/app_store.png" title="App Store"/></a>
</div>
</div>
</div>
</div>
</div>
</div>
<div style="width: 80%;margin: 0 auto;text-align: center;color:rgb(8, 177, 244);">
<h1 style="font-size:70px;">404 </h1>
<h2>Something went wrong here!</h2>
</div>
<footer id="footer">
<div class="footer-bottom">
<div class="container">
<div class="bottom-wrap">
<div class="fotter-social">
<div class="copyright">
<p>© 2018 Veterinary discussion, All Rights Reserved</p>
<p class="design">Designed and Developed by <a class="tristate_link" href="http://www.tristatetechnology.com" target="_blank">TriState Technology.</a></p>
</div>
<div class="social-footer">
<a class="facebook" href="https://www.facebook.com/Veterinary-discussion-144305632596205/?ref=hl" target="_blank" title="Facebook"></a>
<a class="twitter" href="https://twitter.com/VetDiscussion" target="_blank" title="Twitter"></a>
<a class="gplus" href="https://plus.google.com/113692153852870784516" target="_blank" title="Google Plus"></a>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript"><!--
wpa2a.targets=[];
wpa2a.html_done=true;if(wpa2a.script_ready&&!wpa2a.done)wpa2a.init();wpa2a.script_load();
//--></script>
<style type="text/css">
            h2.frq-main-title{
               font-size: 18px;
            }
            .hrf-entry{
               border:1px solid lightgray !important;
               margin-bottom: 10px !important;
               padding-bottom: 0px !important;
            }
            .hrf-content{
               display:none;
               color: #444444;
               background: ffffff;
               font-size: 14px;
               padding: 10px;
               padding-left: 50px;
            }
            h3.hrf-title{
               font-size: 18px ;
               color: #444444;
               background: #ffffff;
               padding: 10px ;
               padding-left: 50px;
               margin: 0;
               -webkit-touch-callout: none;
               -webkit-user-select: none;
               -khtml-user-select: none;
               -moz-user-select: none;
               -ms-user-select: none;
               user-select: none;
               outline-style:none;
            }
            .hrf-title.close-faq{
               cursor: pointer;
            }
            .hrf-title.close-faq span{
               width: 30px;
               height: 30px;
               display: inline-block;
               position: relative;
               left: 0;
               top: 8px;
               margin-right: 12px;
               margin-left: -42px;
               background: #444444 url(https://www.veterinarydiscussion.com/wp-content/plugins/html5-responsive-faq/images/open.png) no-repeat center center;
            }
            }.hrf-title.open-faq{
            
            }
            .hrf-title.open-faq span{
               width: 30px;
               height: 30px;
               display: inline-block;
               position: relative;
               left: 0;
               top: 8px;
               margin-right: 12px;
               margin-left: -42px;
               background: #444444 url(https://www.veterinarydiscussion.com/wp-content/plugins/html5-responsive-faq/images/close.png) no-repeat center center;
            }
            .hrf-entry p{
            
            }
            .hrf-entry ul{
            
            }
            .hrf-entry ul li{
            
            }</style></footer>
</body>
</html>