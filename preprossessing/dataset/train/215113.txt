<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]--><!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]--><!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang=""> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<title>:: B4U</title>
<meta content="" name="description"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="favicon.png" rel="shortcut icon"/>
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<link href="css/bootstrap-theme.min.css" rel="stylesheet"/>
<link href="css/jquery.smallipop.css" rel="stylesheet"/>
<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet"/>
<link href="css/datepicker.css" rel="stylesheet"/>
<link href="css/main.css" rel="stylesheet"/>
<link href="css/media.css" rel="stylesheet"/>
<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<header class="header">
<div class="container">
<div class="row">
<div class="col-sm-9 pull-right">
<div class="main-meta">
<div class="top-bar">
<div class="hdr-offer">
<p>Online customers will GET attractive <strong>PRICE</strong> Guarantee..! <span>Upto <strong> 15%</strong></span>
</p>
</div>
<ul>
<li class="help">24 x 7 Helpline 9895 44 17 28</li>
</ul>
</div>
<div class="main-menu">
<ul class="menu-list">
<!--<li><a href="index.html">Home</a></li>
                            <li><a href="about-us.html">About Us</a></li>
                            <li><a href="overview.html">Overview</a></li>
                            <li><a href="food-menu.html">Food Menu</a></li>
                            <li><a href="packages.html">Packages</a></li>
                            <li><a href="gallery.html">Gallery</a></li>
                            <li><a href="contact-us.html">Contact Us</a></li>-->
<li class="active"><a href="/index.php">Home</a></li>
<li class=""><a href="/about-us.php">About-Us</a></li>
<li class=""><a href="/overview.php">Overview</a></li>
<li class=""><a href="/food-menu.php">Food Menu</a></li>
<li class=""><a href="#">Packages</a>
<ul>
<li><a href="deluxe-ac-houseboat.php">Deluxe A/C Houseboat</a></li>
<li><a href="premium-full-ac-houseboat.php">Premium Full A/C Houseboat</a> </li>
<li><a href="day-cruise.php">Day Cruise With Lunch</a> </li>
<li><a href="cruise-with-conference.php">Cruise With Conference</a></li>
<li><a href="honeymoon-packages.php">Honeymoon Package</a> </li>
</ul>
</li>
<li class=""><a href="/gallery.php">Gallery</a></li>
<li class=""><a href="/contact-us.php">Contact Us</a></li>
</ul>
<ul id="mainnav">
</ul>
<a class="menu-toggle" href="javascript:void(0)">
<span class="line a"></span>
<span class="line b"></span>
<span class="line c"></span>
</a>
</div>
</div>
</div>
<div class="col-sm-3">
<div class="logo"><a href="index.php"><img class="img-responsive" src="img/logo.png"/></a></div>
</div>
</div>
</div>
<div class="mobile-menu"></div>
</header>
<section class="banner-wrapper">
<!--<div class="slider-loader">
            <div class="sk-circle">
                <div class="sk-circle1 sk-child"></div>
                <div class="sk-circle2 sk-child"></div>
                <div class="sk-circle3 sk-child"></div>
                <div class="sk-circle4 sk-child"></div>
                <div class="sk-circle5 sk-child"></div>
                <div class="sk-circle6 sk-child"></div>
                <div class="sk-circle7 sk-child"></div>
                <div class="sk-circle8 sk-child"></div>
                <div class="sk-circle9 sk-child"></div>
                <div class="sk-circle10 sk-child"></div>
                <div class="sk-circle11 sk-child"></div>
                <div class="sk-circle12 sk-child"></div>
            </div>
        </div>--><!--slider-loader-->
<ul class="main-slider clearfix">
<li>
<div class="container">
<div class="banner-content">
<div class="row">
<div class="col-md-8 col-sm-8">
<div class="banner-text">
<h2>Enjoy your every moments at <br/><span>Kerala backwaters...</span></h2>
<p>You can book your own houseboat in an easyway without any formalities.</p>
<a class="cm-btn" href="overview.php">View Details</a>
</div>
</div>
<div class="col-md-4 col-sm-4">
</div>
</div>
</div> <!--banner-content -->
</div>
<span class="banner-bg"><img alt="#" src="img/banner-slider-1.jpg"/></span>
</li>
<li>
<div class="container">
<div class="banner-content">
<div class="row">
<div class="col-md-8 col-sm-8">
<div class="banner-text">
<h2>Enjoy your every moments at <br/><span>Kerala backwaters...</span></h2>
<p>You can book your own houseboat in an easyway without any formalities.</p>
<a class="cm-btn" href="#">View Details</a>
</div>
</div>
<div class="col-md-4 col-sm-4">
</div>
</div>
</div> <!--banner-content -->
</div>
<span class="banner-bg"><img alt="#" src="img/banner-slider-1.jpg"/></span>
</li>
</ul>
</section>
<section class="home-intro">
<div class="container">
<div class="row">
<div class="col-lg-7 col-lg-offset-5 col-md-8 col-md-offset-4">
<div class="ceo-msg">
<h3>CEO Message</h3>
<p>Welcome to our website; we appreciate your interest in our company. Our site is organized to make it easy for you to find the information you want. If you have any questions or suggestions, we gladly welcome.</p>
</div>
</div>
</div>
<div class="wrapper">
<div class="row">
<div class="col-sm-5">
<div class="intro-gallery cm-height">
<div class="pic-item pic-1 set-as-bg"><span class="make-bg"><img src="img/intro-gallery-1.jpg"/></span></div>
<div class="pic-item pic-2 set-as-bg"><span class="make-bg"><img src="img/intro-gallery-2.jpg"/></span></div>
<div class="pic-item pic-3 set-as-bg"><span class="make-bg"><img src="img/intro-gallery-3.jpg"/></span></div>
</div>
</div>
<div class="col-sm-7">
<div class="intro-para cm-height">
<h2 class="section-h2 left">About Alleppey</h2>
<p>Alappuzha district occupies a prominent place in the tourist map of Kerala interlocked with lakes and canals.The Vembanad Lake stretching up to Kochi, the rivers Achankovil, Manimala and Pamba and a net work of canals in the district are used for inland navigation.</p>
<p>The vast stretch of paddy fields of Kuttanad, which is popularly known as the 'rice bowl of Kerala,' is in this district. Coir and coir products form a thriving industry in this district. </p>
<a class="cm-btn" href="about-us.php">Read More</a>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="package-section cover-bg">
<div class="container">
<h2 class="section-h2 invert">HOUSE BOAT PACKAGE</h2>
<ul class="package-slider">
<li>
<div class="pack-item set-as-bg cover-bg">
<div class="head"><a href="honeymoon-packages.php">honeymoon Package<i class="fa fa-chevron-circle-right"></i></a></div>
<span class="make-bg"><img alt="" src="img/package-img-1.jpg"/></span>
</div>
</li>
<li>
<div class="pack-item half set-as-bg cover-bg">
<div class="head"><a href="deluxe-ac-houseboat.php">deluxe ac houseboat<i class="fa fa-chevron-circle-right"></i></a></div>
<span class="make-bg"><img alt="" src="img/package-img-2.jpg"/></span>
</div>
<div class="pack-item half set-as-bg cover-bg">
<div class="head"><a href="premium-full-ac-houseboat.php">premium full ac houseboat<i class="fa fa-chevron-circle-right"></i></a></div>
<span class="make-bg"><img alt="" src="img/package-img-3.jpg"/></span>
</div>
</li>
<li>
<div class="pack-item set-as-bg cover-bg filter">
<div class="head texting">
<a href="day-cruise.php">Day Cruise With Lunch<i class="fa fa-chevron-circle-right"></i></a>
<ul>
<li>TV and DVD / Evn: Cable Connection.</li>
<li>Separate Sitting Chairs</li>
<li>Mosquito Repellent.</li>
<li>Wall Cupboards</li>
<li>Wall Fans</li>
<li>Safety Equipment's</li>
</ul>
</div>
<span class="make-bg"><img alt="" src="img/package-img-4.jpg"/></span>
</div>
</li>
</ul>
</div>
</section>
<section class="about-us">
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="how-to-get cm-height">
<h3 class="section-h3 left">How to Reach Alleppey</h3>
<div class="acc-block">
<div class="acc-item">
<div class="acc-head"><h4>By Air</h4></div>
<div class="acc-content">
<p>The airport nearest to Alleppey is Kochi at a distance of 64 km north. The Trivandrum airport is located 165 km south of Alleppey. Cities like Cochin, Chennai, Howrah, Bangalore and Bokaro are well connected to Alleppey. </p>
</div>
</div>
<div class="acc-item">
<div class="acc-head"><h4>By Rail</h4></div>
<div class="acc-content">
<p>Alleppey is also well connected through waterways. Alleppey is linked by boat and ferry services through the scenic backwaters to Cochin, Kottayam, Kavalam, Changanassery and Chengannur.</p>
</div>
</div>
<div class="acc-item">
<div class="acc-head"><h4>By Road</h4></div>
<div class="acc-content">
<p>National Highway 47 passes through Alleppey connecting it with almost all the nearby places of South India.</p>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-6">
<div class="brief-descrp cm-height">
<div class="descrp-bg set-as-bg"><span class="make-bg"><img alt="" src="img/descrp-bg.jpg"/></span></div>
<div class="texting">
<h3 class="section-h3 left invert">KUMARAKOM</h3>
<p>Kumarakom- (16 kms from Kottayam) numbers on the backs of the famous vemabanad Lake which is  a part of Kuttanad . Another is a Kollam backwater previously known as Quilon  located  on the edge of Asthamudi Lake. </p>
<p>Traders of Arabia, Greece, Rome, China   visited Kollam in ancient times. also the Dutch, British and the Portuguese.  Cochin Backwater is another backwater with finest natural harbour. </p>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="photo-gallery">
<div class="container">
<h2 class="section-h2">Photo Gallery</h2>
<div class="gallery-wrapper">
<ul class="gallery-slider">
<li><img alt="" class="img-responsive" src="img/gallery-img-1.jpg"/></li>
<li><img alt="" class="img-responsive" src="img/gallery-img-2.jpg"/></li>
<li><img alt="" class="img-responsive" src="img/gallery-img-3.jpg"/></li>
<li><img alt="" class="img-responsive" src="img/gallery-img-4.jpg"/></li>
<li><img alt="" class="img-responsive" src="img/gallery-img-5.jpg"/></li>
<li><img alt="" class="img-responsive" src="img/gallery-img-6.jpg"/></li>
</ul>
</div>
</div>
</section>
<section class="punch-block">
<div class="container-fluid">
<div class="row">
<div class="col-sm-6">
<div class="video-wrapper set-as-bg cm-height">
<a class="play-btn video-pop" href="#video-base"><img alt="" class="img-responsive" src="img/play-btn.png"/></a>
<h3 class="section-h3 invert">Enjoy your every moments at Kerala backwaters...</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt <br/>ut labore et dolore magna aliqua. </p>
<a class="simple-btn video-pop" href="#video-base">Watch Our Videos </a>
<span class="make-bg"><img alt="" src="img/punch-bg.jpg"/></span>
<div class="video-base" id="video-base">
<video controls="">
<source src="video/B4U.mp4" type="video/mp4">
<source src="video/B4U.ogv" type="video/ogv">
                                Your browser does not support the video tag.
                                </source></source></video>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="staff-gallery cm-height">
<h3>Staff gallery</h3>
<div class="staff-slider">
<ul class="staff-carousal">
<li>
<a class="img-pop" href="img/staff/1.JPG" rel="img-pop"><img alt="" class="img-responsive" src="img/staff/1.JPG"/></a>
</li>
<li>
<a class="img-pop" href="img/staff/2.JPG" rel="img-pop"><img alt="" class="img-responsive" src="img/staff/2.JPG"/></a>
</li>
</ul>
<div class="nav-controls">
<a class="prev" href="javascript:void(0)"><i aria-hidden="true" class="fa fa-angle-left"></i></a>
<a class="next" href="javascript:void(0)"><i aria-hidden="true" class="fa fa-angle-right"></i></a>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="testimonials">
<div class="container">
<h2 class="section-h2">Client Testimonials</h2>
<ul class="testi-slider">
<li>
<div class="testi-item">
<div class="texting">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip .<span class="close-quote"></span></p>
</div>
<div class="testi-tag">
<h4>Deepak, <span>Kochi</span></h4>
</div>
</div>
</li>
<li>
<div class="testi-item">
<div class="texting">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip .<span class="close-quote"></span></p>
</div>
<div class="testi-tag">
<h4>Joshi, <span>Mumbai</span></h4>
</div>
</div>
</li>
<li>
<div class="testi-item">
<div class="texting">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip .<span class="close-quote"></span></p>
</div>
<div class="testi-tag">
<h4>Deepak, <span>Kochi</span></h4>
</div>
</div>
</li>
</ul>
</div>
</section>
<footer class="footer">
<div class="container">
<div class="ftr-top">
<div class="row">
<div class="col-sm-3">
<div class="ftr-block cm-height">
<h5>Quick Links</h5>
<ul>
<li><a href="index.php">Home</a></li>
<li><a href="about-us.php">About Us</a></li>
<li><a href="overview.php">Overview</a></li>
<li><a href="food-menu.php">Food Menu</a></li>
<li><a href="packages.php">Packages</a></li>
<li><a href="gallery.php">Gallery</a></li>
<li><a href="contact-us.php">Contact Us</a></li>
</ul>
</div>
</div>
<div class="col-sm-3">
<div class="ftr-block cm-height">
<h5>Overview</h5>
<ul>
<li><a href="overview.php">What is Backwater ?</a></li>
<li><a href="overview.php">Backwater Map</a></li>
<li><a href="overview.php">Houseboat</a></li>
<li><a href="overview.php">features</a></li>
<li><a href="overview.php">Classification</a></li>
</ul>
</div>
</div>
<div class="col-sm-3">
<div class="ftr-block cm-height">
<h5>Packages</h5>
<ul>
<li><a href="deluxe-ac-houseboat.php">Deluxe A/C Houseboat</a></li>
<li><a href="premium-full-ac-houseboat.php">Premium Full A/C Houseboat</a></li>
<li><a href="day-cruise.php">Day Cruise With Lunch</a></li>
<li><a href="cruise-with-conference.php">Cruise With Conference</a></li>
<li><a href="honeymoon-packages.php">Honeymoon Package</a></li>
</ul>
</div>
</div>
<div class="col-sm-3">
<div class="ftr-block cm-height">
<h5>Contact Us</h5>
<h6>Alleppey Office</h6>
<p>
                                    B 4 You Houseboats 
                                    Visrutha Ayurveda | Near Starting Point 
                                    Thathampally P.O | ALLEPPEY. 
                                    PIN-688013.
                                </p>
</div>
</div>
</div>
</div>
</div>
<div class="ftr-bottom">
<div class="container">
<div class="row">
<div class="col-md-9">
<div class="ftr-menu">
<ul>
<li><a href="seasons.php">Kerala Seasons &amp; Local Informations</a></li>
<li><a href="cruise-route.php">Cruise Route</a></li>
<li><a href="attraction.php">Attractions</a></li>
<li><a href="payment-terms.php">Payment Terms</a></li>
<li><a href="privacy-policy.php">Privacy Policy</a></li>
</ul>
</div>
</div>
<div class="col-md-3">
<div class="follow">
<ul>
<li><a href="#"><i aria-hidden="true" class="fa fa-facebook"></i></a></li>
<li><a href="#"><i aria-hidden="true" class="fa fa-twitter"></i></a></li>
<li><a href="#"><i aria-hidden="true" class="fa fa-linkedin"></i></a></li>
<li><a href="#"><i aria-hidden="true" class="fa fa-google-plus"></i></a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</footer>
<script src="js/vendor/jquery-1.11.2.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/jquery.smallipop.min.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/isotope-docs.min.js?3"></script>
<script src="js/datepicker.min.js"></script>
<script src="js/i18n/datepicker.en.js"></script>
<script src="js/main.js"></script>
</body>
</html>
