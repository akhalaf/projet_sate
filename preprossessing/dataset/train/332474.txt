<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>AMG Labs</title>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-145053627-1"></script>
<script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-145053627-1');
    </script>
<style type="text/css">
        .js-loader div#preloader {
            position: fixed;
            left: 0;
            top: 0;
            z-index: 999;
            width: 100%;
            height: 100%;
            overflow: visible;
            background: #fff url('images/loading.gif') no-repeat center center;
        }
    </style>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="css/fontawesome-all.min.css" rel="stylesheet" type="text/css"/>
<link href="css/slick.css" rel="stylesheet" type="text/css"/>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<link href="css/custom.css" rel="stylesheet" type="text/css"/>
</head>
<body class="js-loader">
<div id="preloader"></div>
<div id="header-holder">
<nav class="navbar navbar-full" id="nav">
<div class="container-fluid">
<div class="container container-nav">
<div class="row">
<div class="col-md-12">
<div class="navbar-header">
<button aria-expanded="false" class="navbar-toggle collapsed" data-target="#bs" data-toggle="collapse" type="button">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="/site/"><img alt="AMG Labs" src="images/logo.png"/></a>
</div>
<div aria-expanded="false" class="navbar-collapse collapse navbar-collapse-centered" id="bs" role="main" style="height: 1px;">
<ul class="nav navbar-nav navbar-nav-centered">
<li class="nav-item active"><a class="nav-link" href="/site/">Início</a></li>
<li class="nav-item">
<a class="nav-link" href="#servicos">Serviços</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#planos">Planos</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#clientes">Clientes</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#sobre">Sobre</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#contato">Contato</a>
</li>
</ul>
<ul class="nav navbar-nav navbar-right other-navbar">
<li class="nav-item">
<a class="nav-link btn-client-area" href="https://amglabs.net/sistema/" target="_blank"><img alt="" src="images/lock.svg"/>Área do Cliente</a>
<div class="lang-info row">
<div class="col-md-4 col-xs-3"></div>
<div class="col-md-2 col-xs-3 text-right">
<a href="/site/"><img alt="Português" src="images/br-flag.png" title="Português"/></a>
</div>
<div class="col-md-2 col-xs-3 text-left">
<a href="/site/?lang=en"><img alt="English" src="images/us-flag.png" title="English"/></a>
</div>
<div class="col-md-4 col-xs-3"></div>
</div>
</li>
<!--
                                <li class="nav-item">
                                    <a class="nav-link btn-chat" href="#"><i class="hstb hstb-chat"></i></a>
                                </li>
                                -->
</ul>
</div>
</div>
</div>
</div>
</div>
</nav>
<div class="container-fluid" id="top-content">
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="main-slider">
<div class="slide">
<div class="row rtl-row">
<div class="col-sm-5">
<div class="img-holder">
<img alt="" src="images/feature4.png"/>
</div>
</div>
<div class="col-sm-7">
<div class="b-text">Hospedagem<br/>Web</div>
<div class="m-text">Os melhores planos de hospedagem para o seu negócio.</div>
<a class="hbtn hbtn-primary hbtn-lg" href="#planos">Ver Planos</a>
</div>
</div>
</div>
<div class="slide">
<div class="row rtl-row">
<div class="col-sm-5">
<div class="img-holder">
<img alt="" src="images/slide-img2.png"/>
</div>
</div>
<div class="col-sm-7">
<div class="b-text">Desenvolvimento <br/>De Software</div>
<div class="m-text">Sites, sistemas, aplicativos e lojas virtuais.</div>
<a class="hbtn hbtn-primary hbtn-lg" href="#contato">Solicitar Informações</a>
</div>
</div>
</div>
<div class="slide">
<div class="row rtl-row">
<div class="col-sm-5">
<div class="img-holder">
<img alt="" src="images/wordpress-white.png"/>
</div>
</div>
<div class="col-sm-7">
<div class="b-text">Desenvolvimento<br/>WordPress</div>
<div class="m-text">Somos especialistas em desenvolvimento na plataforma WordPress.</div>
<a class="hbtn hbtn-primary hbtn-lg" href="#contato">Entre em Contato</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div><div class="features container-fluid" id="servicos">
<div class="container">
<div class="row">
<div class="col-sm-5">
<div class="img-holder">
<img alt="" src="images/feature6.png"/>
</div>
</div>
<div class="col-sm-7 def-aligned">
<div class="feature-info">
<div class="feature-title">Hospedagem Web</div>
<div class="feature-text">Disponibilizamos planos de hospedagem web rápidos e flexíveis. Com nossos planos seus dados estão sempre seguros na nuvem, com garantia de backup e disponibilidade.</div>
<div class="feature-link"><a class="hbtn hbtn-default" href="#planos">Contrate Agora</a></div>
</div>
</div>
</div>
<div class="row rtl-row">
<div class="col-sm-5">
<div class="img-holder">
<img alt="" src="images/office.png"/>
</div>
</div>
<div class="col-sm-7">
<div class="feature-info">
<div class="feature-title">Desenvolvimento de Software</div>
<div class="feature-text">Trabalhamos com desenvolvimento personalizado. Desenvolvemos softwares para gerenciamento de empresas, sites, lojas virtuais e aplicativos para dispositivos móveis.</div>
<div class="feature-link"><a class="hbtn hbtn-default" href="#contato">Mais Informações</a></div>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-5">
<div class="img-holder">
<img alt="" src="images/wapuu.png"/>
</div>
</div>
<div class="col-sm-7 def-aligned">
<div class="feature-info">
<div class="feature-title">WordPress</div>
<div class="feature-text">Somos especialista em desenvolvimento utilizando o WordPress. Prestamos consultoria, desenvolvemos temas e plugins para a plataforma.</div>
<div class="feature-link"><a class="hbtn hbtn-default" href="#contato">Entre em Contato</a></div>
</div>
</div>
</div>
<div class="row rtl-row">
<div class="col-sm-5">
<div class="img-holder">
<img alt="" src="images/feature5.png"/>
</div>
</div>
<div class="col-sm-7">
<div class="feature-info">
<div class="feature-title">Gerenciamento de Servidores</div>
<div class="feature-text">Oferecemos o serviço de gerenciamento de servidores. Implantamos e gerenciamos servidores com tecnologias open source como CentOS, Debian, FreeBSD, OpenBSD e Ubuntu Server.</div>
<div class="feature-link"><a class="hbtn hbtn-default" href="#contato">Solicite Informações</a></div>
</div>
</div>
</div>
</div>
</div><div class="pricing container-fluid" id="planos">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="row-title">Planos de Hospedagem Web</div>
</div>
</div>
<!-- Shared hosting -->
<div class="row custom-padding">
<div class="col-sm-6 col-md-4">
<div class="pricing-box">
<div class="pricing-title">Plano Prata</div>
<div class="pricing-icon">
<div class="icon"><img alt="Plano Prata" src="images/server1.svg"/></div>
</div>
<div class="pricing-details">
<ul>
<li>2GB de espaço</li>
<li>20GB de transfêrencia</li>
<li>Domínios e e-mails ilimitados</li>
<li>PHP/Python/Perl/MySQL</li>
<li>SSL Incluso</li>
</ul>
</div>
<div class="pricing-price">
<div class="price-info">A partir de</div>
<div class="price">
<span class="currency">R$</span><span class="num"> 60/mês</span>
</div>
</div>
<div class="pricing-button">
<a class="btn-order" data-description="Plano Prata - R$  60/mês" href="#planos">Contratar</a>
</div>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="pricing-box">
<div class="pricing-title">Plano Prata SSL</div>
<div class="pricing-icon">
<div class="icon"><img alt="Plano Prata SSL" src="images/server2.svg"/></div>
</div>
<div class="pricing-details">
<ul>
<li>5GB de espaço</li>
<li>50GB de transferência</li>
<li>Domínios e e-mails ilimitados</li>
<li>PHP/Python/Perl/MySQL</li>
<li>SSL Incluso</li>
</ul>
</div>
<div class="pricing-price">
<div class="price-info">A partir de</div>
<div class="price">
<span class="currency">R$</span><span class="num"> 90/mês</span>
</div>
</div>
<div class="pricing-button">
<a class="btn-order" data-description="Plano Prata SSL - R$  90/mês" href="#planos">Contratar</a>
</div>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="pricing-box">
<div class="pricing-title">Plano Diamante SSL</div>
<div class="pricing-icon">
<div class="icon"><img alt="Plano Diamante SSL" src="images/server3.svg"/></div>
</div>
<div class="pricing-details">
<ul>
<li>10GB de espaço</li>
<li>100GB de transferência</li>
<li>Domínios e e-mails ilimitados</li>
<li>PHP/Python/Perl/MySQL</li>
<li>SSL Incluso</li>
</ul>
</div>
<div class="pricing-price">
<div class="price-info">A partir de</div>
<div class="price">
<span class="currency">R$</span><span class="num"> 110/mês</span>
</div>
</div>
<div class="pricing-button">
<a class="btn-order" data-description="Plano Diamante SSL - R$  90/mês" href="#planos">Contratar</a>
</div>
</div>
</div>
</div>
<!-- End shared hosting -->
<!-- VPS and dedicated -->
<div class="row custom-padding">
<div class="col-sm-6 col-md-4">
<div class="pricing-box">
<div class="pricing-title">VPS Cloud Prata</div>
<div class="pricing-icon">
<div class="icon"><img alt="Plano Prata" src="images/server5.svg"/></div>
</div>
<div class="pricing-details">
<ul>
<li>4 cores de CPU</li>
<li>4GB de memória RAM</li>
<li>160GB de espaço em disco (SSD)</li>
<li>Linux ou Windows</li>
<li>Gerenciamento e atualizações incluídas</li>
</ul>
</div>
<div class="pricing-price">
<div class="price-info">A partir de</div>
<div class="price">
<span class="currency"></span><span class="num">Consulte-nos</span>
</div>
</div>
<div class="pricing-button">
<a href="#contato">Entre em Contato</a>
</div>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="pricing-box">
<div class="pricing-title">VPS Cloud Ouro</div>
<div class="pricing-icon">
<div class="icon"><img alt="VPS Cloud Ouro" src="images/server5.svg"/></div>
</div>
<div class="pricing-details">
<ul>
<li>5 cores de CPU</li>
<li>8GB de memória RAM</li>
<li>240GB de espaço em disco (SSD)</li>
<li>Linux ou Windows</li>
<li>Gerenciamento e atualizações incluídas</li>
</ul>
</div>
<div class="pricing-price">
<div class="price-info">A partir de</div>
<div class="price">
<span class="currency"></span><span class="num">Consulte-nos</span>
</div>
</div>
<div class="pricing-button">
<a href="#contato">Entre em Contato</a>
</div>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="pricing-box">
<div class="pricing-title">Servidor Dedicado</div>
<div class="pricing-icon">
<div class="icon"><img alt="Servidor Dedicado" src="images/server4.svg"/></div>
</div>
<div class="pricing-details">
<ul>
<li>A partir de 4 núcleos</li>
<li>A partir de 8GB de memória RAM</li>
<li>A partir de 240GB de espaço em disco (SSD)</li>
<li>Linux, Windows, BSD ou macOS</li>
<li>Gerenciamento e atualizações</li>
</ul>
</div>
<div class="pricing-price">
<div class="price-info">A partir de</div>
<div class="price">
<span class="currency"></span><span class="num">Consulte-nos</span>
</div>
</div>
<div class="pricing-button">
<a href="#contato">Entre em Contato</a>
</div>
</div>
</div>
</div>
<!-- End VPS and dedicated -->
</div>
</div><div class="container-fluid" id="clientes">
<div class="row">
<div class="col-md-12 text-holder">
<div class="text-box text-box-clients">
<h4 class="clients-title text-center row-title">Clientes</h4>
<p class="text-center clients-description">Algumas empresas que confiam na qualidade dos serviços prestados pela AMG Labs.</p>
<div class="row">
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.acisap.com.br" target="_blank">
<img alt="ACISAP" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/acisap.png" title="ACISAP" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.administradorasolar.com.br" target="_blank">
<img alt="Administradora Solar" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/administradora-solar.png" title="Administradora Solar" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.cristianidecken.com.br" target="_blank">
<img alt="Cristiani Decken" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/cristiani-decken.png" title="Cristiani Decken" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.dacolonia.com.br" target="_blank">
<img alt="Dacolônia" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/dacolonia.png" title="Dacolônia" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.dharmainformatica.com" target="_blank">
<img alt="Dharma Informática" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/dharma-informatica.png" title="Dharma Informática" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.ecofieldambiental.com.br" target="_blank">
<img alt="Ecofield" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/ecofield-ambiental.png" title="Ecofield" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.editerochaimoveis.com.br" target="_blank">
<img alt="Edite Rocha Imóveis" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/edite-rocha-imoveis.png" title="Edite Rocha Imóveis" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.expressocatanduva.com.br" target="_blank">
<img alt="Expresso Catanduva" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/expresso-catanduva.png" title="Expresso Catanduva" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="#" target="_blank">
<img alt="Farmácia Santa Rita" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/farmacia-santa-rita.png" title="Farmácia Santa Rita" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="https://www.gabrielecosta.com.br" target="_blank">
<img alt="Gabriele Costa" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/gabriele-costa.png" title="Gabriele Costa" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.glucosul.com.br" target="_blank">
<img alt="Glucosul" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/glucosul.png" title="Glucosul" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.graficagaucha.com" target="_blank">
<img alt="Gráfica Gaúcha" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/grafica-gaucha.png" title="Gráfica Gaúcha" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.jefersonmolina.com.br" target="_blank">
<img alt="Jeferson Molina" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/jeferson-molina.png" title="Jeferson Molina" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.isoarte.com.br" target="_blank">
<img alt="Isoarte" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/isoarte.png" title="Isoarte" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.lojauniverso.com.br" target="_blank">
<img alt="Loja Universo" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/loja-universo.png" title="Loja Universo" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.madeferroestruturas.com.br" target="_blank">
<img alt="Madeferro" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/madeferro.png" title="Madeferro" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.malibiabierfotografias.com.br" target="_blank">
<img alt="Malíbia Bier Fotografias" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/malibia-bier.png" title="Malíbia Bier Fotografias" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.mecanicaluzardo.com.br" target="_blank">
<img alt="Mecânica Luzardo" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/mecanica-luzardo.png" title="Mecânica Luzardo" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="https://www.mpiinformatica.com" target="_blank">
<img alt="MPI Informática" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/mpi-informatica.png" title="MPI Informática" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.naturalsolucoes.com.br" target="_blank">
<img alt="Natural Soluções" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/natural-solucoes.png" title="Natural Soluções" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.paynelessmedia.com.au" target="_blank">
<img alt="Payneless Media" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/payneless-media.png" title="Payneless Media" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.produtosmelo.com.br" target="_blank">
<img alt="Produtos Melo" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/produtos-melo.png" title="Produtos Melo" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.redemaisrs.com.br" target="_blank">
<img alt="Rede Mais RS" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/redemais-rs.png" title="Rede Mais RS" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.ricotransportes.com.br" target="_blank">
<img alt="Rico Transportes" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/rico-transportes.png" title="Rico Transportes" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.sanimaq.com.br" target="_blank">
<img alt="Sanimaq" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/sanimaq.png" title="Sanimaq" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.sementinhaeduca.com.br" target="_blank">
<img alt="Sementinha Educa" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/sementinha-educa.png" title="Sementinha Educa" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.cestobasico.com.br" target="_blank">
<img alt="Supermercado Cesto Básico" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/supermercado-cesto-basico.png" title="Supermercado Cesto Básico" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.supersorb-br.com.br" target="_blank">
<img alt="Supersorb" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/supersorb.png" title="Supersorb" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.gruposaojoaquim.com.br" target="_blank">
<img alt="Grupo São Joaquim" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/sao-joaquim.png" title="Grupo São Joaquim" width="120"/>
</a>
</div>
<div class="col-xs-4 col-md-2 clients-item">
<a href="http://www.zemaqnet.com.br" target="_blank">
<img alt="Zemaq" class="img-responsive client-logo" src="https://amglabs.net/site/images/clients/zemaq.png" title="Zemaq" width="120"/>
</a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="text-photo-sc container-fluid darkblue-bg" id="sobre">
<div class="row rtl-row">
<div class="col-md-6 photo-holder photo2"></div>
<div class="col-md-6 text-holder text2 opposite">
<div class="text-box">
<h4>Sobre a AMG Labs</h4>
<p>A AMG Labs é uma empresa fundada em 2014 que atua principalmente no desenvolvimento de softwares personalizados e hospedagem de aplicações web. </p>
<p>Desenvolve desde sistemas customizados para controle e gerenciamento de empresas até aplicativos para dispositivos móveis, sites e lojas virtuais. No quesito hospedagem, trabalha com hospedagem de sites, e-mails e aplicações web, contando com servidores modernos e constantemente atualizados. </p>
<p>Como principal objetivo, busca sempre atingir a satisfação do cliente. Baseado nesta premissa, oferece serviços de alta qualidade, proporcionando um ótimo atendimento, com agilidade na execução de seus projetos.</p>
<p>Se você precisa de um sistema  personalizado, tem um projeto desafiador ou procura uma hospedagem de qualidade para seus projetos, tenha certeza que a AMG Labs é <b>a escolha certa</b>.</p>
</div>
</div>
</div>
</div><div class="contact-us container-fluid" id="contato">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="row-title">Entre em Contato</div>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-md-8">
<div class="form-holder">
<form action="?lang=br" method="post">
<div class="form-row">
<div class="col-xs-12 col-sm-6">
<label for="inputName1">Nome</label>
<input class="form-control" id="inputName1" name="name" required="" type="text"/>
</div>
<div class="col-xs-12 col-sm-6">
<label for="inputEmail1">E-mail</label>
<input class="form-control" id="inputEmail1" name="email" required="" type="text"/>
</div>
</div>
<div class="form-row">
<div class="col-xs-12 col-sm-6">
<label for="inputPhone1">Telefone</label>
<input class="form-control" id="inputPhone1" name="phone" required="" type="tel"/>
</div>
<div class="col-xs-12 col-sm-6">
<label for="inputSubject1">Assunto</label>
<div class="select-holder">
<select class="form-control" id="inputSubject1" name="subject" required="">
<option disabled="" selected="" value="">Selecione um assunto</option>
<option>Desenvolvimento de Software</option>
<option>Hospedagem</option>
<option>Dúvidas</option>
<option>Outros</option>
</select>
</div>
</div>
</div>
<div class="form-row">
<div class="col-xs-12">
<label for="inputMessage1">Mensagem</label>
<textarea class="form-control" id="inputMessage1" name="message" required=""></textarea>
</div>
</div>
<div class="form-row">
<div class="col-md-8 col-xs-12">
<div class="g-recaptcha" data-sitekey="6LdrJqUUAAAAAI3KcxhlnLf-xhCmiqrtJJF32cLD"></div>
</div>
<div class="col-md-4 col-xs-12">
<div class="submit-holder">
<input name="action" type="hidden" value="contact"/>
<button class="hbtn hbtn-blue" type="submit">Enviar Mensagem</button>
</div>
</div>
</div>
</form>
</div>
</div>
<div class="col-md-4">
<div class="address-details">
<h4>Atendimento</h4>
<p>Está com alguma dúvida ou deseja contratar um plano/serviço? Preencha o formulário e entraremos em contato o mais rápido possível.</p>
<h4>Suporte</h4>
<p>Já é cliente e precisa de atendimento? Envie sua solicitação para <b>suporte@amglabs.net</b>. </p>
<h4>Contato</h4>
<p><i class="fa fa-envelope"></i> contato@amglabs.net<br/>
<i class="fab fa-whatsapp"></i> (51) 92000 7613<br/>
<i class="fa fa-map-pin"></i> Av. Cel Victor Villa Verde 126/301 - Centro<br/>
Santo Antônio da Patrulha - RS - Brasil</p>
</div>
</div>
</div>
</div>
</div><!-- Modal -->
<div class="modal fade" id="order" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header order-modal-header">
<button class="close close-order-modal" data-dismiss="modal" type="button">×</button>
<h4 class="modal-title">AMG Labs - Contratação</h4>
</div>
<div class="modal-body order-modal-body">
<div class="row">
<div class="col-md-12">
<div class="form-holder">
<form action="?lang=br" method="post">
<div class="form-row">
<div class="col-xs-12">
<label for="inputDomain">Domínio (ex. suaempresa.com.br)</label>
<input autocapitalize="none" autocomplete="off" autocorrect="off" class="form-control" id="inputDomain" name="domain" required="" type="text"/>
</div>
</div>
<div class="form-row">
<div class="col-xs-12">
<label for="inputPlan">Plano</label>
<input class="form-control" id="inputPlan" name="plan" readonly="readonly" required="" type="text"/>
</div>
</div>
<div class="form-row">
<div class="col-xs-12 col-sm-6">
<label for="inputName1">Nome/Empresa</label>
<input class="form-control" id="inputName1" name="name" required="" type="text"/>
</div>
<div class="col-xs-12 col-sm-6">
<label for="inputEmail1">E-mail</label>
<input autocapitalize="none" class="form-control" id="inputEmail1" name="email" required="" type="email"/>
</div>
</div>
<div class="form-row">
<div class="col-xs-12 col-sm-6">
<label for="inputPhone">Telefone</label>
<input class="form-control" id="inputPhone" name="phone" required="" type="tel"/>
</div>
<div class="col-xs-12 col-sm-6">
<label for="inputPaymentMethod">Método de Pagamento</label>
<div class="select-holder">
<select class="form-control" id="inputPaymentMethod" name="payment_method">
<option value="Boleto Bancário">Boleto Bancário</option>
<option value="Cartão de Crédito (PagSeguro)">Cartão de Crédito (PagSeguro)</option>
<option value="Transferência Bancária">Transferência Bancária</option>
</select>
</div>
</div>
</div>
<div class="form-row">
<div class="col-md-12 col-xs-12 text-center">
<div class="g-recaptcha g-recaptcha-order" data-sitekey="6LdrJqUUAAAAAI3KcxhlnLf-xhCmiqrtJJF32cLD"></div>
</div>
<div class="col-md-12 col-xs-12 text-left">
<div class="text-center">
<input name="action" type="hidden" value="order"/>
<button class="hbtn hbtn-blue" type="submit">Enviar Solicitação</button>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div><div class="footer container-fluid">
<!--<a class="btn-go-top" href="#"><i class="hstb hstb-down-arrow"></i></a>-->
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="footer-menu">
<h4>Acesso Rápido</h4>
<ul>
<li><a href="https://amglabs.net/sistema/" target="_blank">Área do Cliente</a></li>
<li><a href="#contato">Contato</a></li>
<li><a href="#">Início</a></li>
</ul>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="footer-menu">
<h4>Empresa</h4>
<ul>
<li><a href="#clientes">Clientes</a></li>
<li><a href="#planos">Planos</a></li>
<li><a href="#servicos">Serviços</a></li>
<li><a href="#sobre">Sobre</a></li>
</ul>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="footer-menu custom-footer-menu">
<h4>Contato</h4>
<ul class="social">
<li><a href="https://wa.me/5551920007613" target="_blank"><i class="fab fa-whatsapp"></i></a></li>
<li><a href="https://instagram.com/amglabs" target="_blank"><i class="fab fa-instagram"></i></a></li>
<li><a href="https://facebook.com/amglabs" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
<li><a href="https://twitter.com/amglabsnet" target="_blank"><i class="fab fa-twitter"></i></a></li>
</ul>
<ul>
<li>(51) 92000 7613</li>
<li>contato@amglabs.net</li>
<li>Av. Cel. Victor Villa Verde, 126/301 - Centro<br/>Santo Antônio da Patrulha - RS - Brasil</li>
</ul>
</div>
</div>
</div>
<div class="sub-footer">
<div class="row">
<div class="col-md-6">
<!--
                    <div class="sub-footer-menu">
                        <ul>
                            <li><a href="#">Sitemap</a></li>
                            <li><a href="#">Terms of Service</a></li>
                            <li><a href="privacy.html">Privacy Policy</a></li>
                        </ul>        
                    </div>
                    -->
</div>
<div class="col-md-6">
<div class="copyright">AMG Labs</div>
</div>
</div>
</div>
</div>
</div>
<a class="whatsapp-float" href="https://api.whatsapp.com/send?phone=5551920007613" target="_blank">
<i class="fab fa-whatsapp whatsapp-my-float"></i>
</a>
<style type="text/css">
.whatsapp-float{
    position:fixed;
    width:60px;
    height:60px;
    bottom:40px;
    right:40px;
    background-color:#25d366;
    color:#fff !important;
    border-radius:50px;
    text-align:center;
    font-size:30px;
    z-index:100;
}
.whatsapp-my-float{
    margin-top:16px;
}
</style>
<script src="js/jquery.min.js"></script>
<script>
    $(window).on('load', function(){
        $('#preloader').hide();
    });
    $(document).ready(function(){
        $(".btn-order").click(function(){
            $('#inputPlan').val($(this).attr('data-description'));
            $("#order").modal();
        });
    })
</script>
<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/main.js"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>
</body>
</html>