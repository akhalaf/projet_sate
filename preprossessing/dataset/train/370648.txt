<!DOCTYPE html>
<!--[if IE]><![endif]--><!--[if IE 8 ]><html dir="ltr" lang="en" class="ie8"><![endif]--><!--[if IE 9 ]><html dir="ltr" lang="en" class="ie9"><![endif]--><!--[if (gt IE 9)|!(IE)]><!--><html dir="ltr" lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Page not found!</title>
<base href="https://www.creativeballoons.com.sg/"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" media="screen" rel="stylesheet"/>
<link href="catalog/view/theme/default/stylesheet/bootstrap-text.css" media="screen" rel="stylesheet"/>
<link href="catalog/view/theme/default/stylesheet/bootstrap-pull.css" media="screen" rel="stylesheet"/>
<link href="catalog/view/theme/default/stylesheet/bootstrap-five-cols.css" media="screen" rel="stylesheet"/>
<link href="catalog/view/theme/default/stylesheet/bootstrap-equal-height.css" media="screen" rel="stylesheet"/>
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/bootstrap/js/bootstrap-dialog.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css"/>
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet"/>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
</head>
<body class="error-not_found">
<nav class="hide" id="top">
<div class="container">
<div class="nav pull-right" id="top-links">
<ul class="list-inline">
<li><a href="https://www.creativeballoons.com.sg/contact"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md">+65 90224193 </span></li>
<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="https://www.creativeballoons.com.sg/account" title="My Account"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md">My Account</span> <span class="caret"></span></a>
<ul class="dropdown-menu dropdown-menu-right">
<li><a href="https://www.creativeballoons.com.sg/register">Register</a></li>
<li><a href="https://www.creativeballoons.com.sg/login">Login</a></li>
</ul>
</li>
<li><a href="https://www.creativeballoons.com.sg/wishlist" id="wishlist-total" title="Wish List (0)"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md">Wish List (0)</span></a></li>
<li><a href="https://www.creativeballoons.com.sg/index.php?route=checkout/cart" title="Shopping Cart"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">Shopping Cart</span></a></li>
<li><a href="https://www.creativeballoons.com.sg/index.php?route=checkout/checkout" title="Checkout"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md">Checkout</span></a></li>
</ul>
</div>
</div>
</nav>
<header>
<div class="container" style="position: relative;">
<img class="balloon-top-left visible-lg hidden-sm visible-md hidden-xs" src="catalog/view/theme/default/image/balloon-top-left.png"/>
<div class="row">
<div class="col-sm-12">
<nav class="top-nav" id="top">
<ul class="list-inline">
<li><div style="margin-left: -14.4em;"><a href="https://www.creativeballoons.com.sg/register"><img class="img-responsive" src="catalog/view/theme/default/image/signup.png"/></a></div></li>
<li><div style="margin-left: -9.9em;"><a href="https://www.creativeballoons.com.sg/login"><img class="img-responsive" src="catalog/view/theme/default/image/login.png"/></a></div></li>
<li><div style="margin-left: -5.7em;"><a href="https://www.creativeballoons.com.sg/index.php?route=checkout/cart" title="Shopping Cart"><img class="img-responsive" src="catalog/view/theme/default/image/cart.png"/></a></div></li>
</ul>
</nav>
</div>
</div>
<div class="row">
<div class="col-sm-15 pull-right">
<ul class="list-inline social-button pull-right">
<li><a href="https://www.instagram.com/creativeballoons/" target="_blank"><img class="img-responsive" src="catalog/view/theme/default/image/ig.png"/></a></li>
</ul>
</div>
<div class="col-sm-offset-15 col-sm-7">
<div id="logo">
<a href="https://www.creativeballoons.com.sg/"><img alt="Creative Balloons" class="img-responsive" src="https://www.creativeballoons.com.sg/image/catalog/banners/logo-cballoon.png" title="Creative Balloons"/></a>
</div>
</div>
<div class="col-sm-5 hide"><div class="input-group" id="search">
<input class="form-control input-lg" name="search" placeholder="Search" type="text" value=""/>
<span class="input-group-btn">
<button class="btn btn-default btn-lg" type="button"><i class="fa fa-search"></i></button>
</span>
</div> </div>
<div class="col-sm-3 hide"><div class="btn-group btn-block" id="cart">
<button class="btn btn-inverse btn-block btn-lg dropdown-toggle" data-loading-text="Loading..." data-toggle="dropdown" type="button"><i class="fa fa-shopping-cart"></i> <span id="cart-total">0 item(s) - $0.00</span></button>
<ul class="dropdown-menu pull-right">
<li>
<p class="text-center">Your shopping cart is empty!</p>
</li>
</ul>
</div>
</div>
</div>
<img class="balloon-top-right visible-lg hidden-sm visible-md hidden-xs" src="catalog/view/theme/default/image/balloon-top-right.png"/>
</div>
<div class="container">
<nav class="navbar" id="menu">
<div class="navbar-header"><span class="visible-xs" id="category">Menu</span>
<button class="btn btn-navbar navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button"><i class="fa fa-bars"></i></button>
</div>
<div class="collapse navbar-collapse navbar-ex1-collapse">
<ul class="nav navbar-nav">
<li><a href="https://www.creativeballoons.com.sg/">Home</a></li>
<li class="dropdown">
<a class="dropdown-toggle" href="https://www.creativeballoons.com.sg/product/balloon">Balloons</a>
<div class="dropdown-menu hide">
<div class="dropdown-inner">
<ul class="list-unstyled">
<li><a href="https://www.creativeballoons.com.sg/product/balloon/alphabet-balloons">Alphanumeric Balloons</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/balloon/animal-air-walking-pet">Animal Air Walking Pet Balloon</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/balloon/balloon-accessories">Balloon Accessories</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/balloon/gas-tank-rental">Gas Tank Rental</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/balloon/helium-inflation-service">Helium Inflation Service</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/balloon/super-shape-balloons">Super Shape Balloons</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/balloon/product-2">Giant Airwalker</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/balloon/product-3">Latex Balloon</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/balloon/sub-category">Foil 18 inches Balloon</a></li>
</ul>
</div>
<a class="see-all" href="https://www.creativeballoons.com.sg/product/balloon">Show All Balloons</a> </div>
</li>
<li class="dropdown">
<a class="dropdown-toggle" href="https://www.creativeballoons.com.sg/product/events-decor">Events Decor</a>
<div class="dropdown-menu hide">
<div class="dropdown-inner">
<ul class="list-unstyled">
<li><a href="https://www.creativeballoons.com.sg/product/events-decor/balloon-drop">Balloon Drop</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/events-decor/balloon-pop">Balloon Pop</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/events-decor/other-balloon-decorations">Other Balloon Decorations</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/events-decor/event-2">Balloon Arches</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/events-decor/event-3">Balloon Column</a></li>
</ul>
</div>
<a class="see-all" href="https://www.creativeballoons.com.sg/product/events-decor">Show All Events Decor</a> </div>
</li>
<li class="dropdown">
<a class="dropdown-toggle" href="https://www.creativeballoons.com.sg/product/giant-inflatables">Giant Inflatables</a>
<div class="dropdown-menu hide">
<div class="dropdown-inner">
<ul class="list-unstyled">
<li><a href="https://www.creativeballoons.com.sg/product/giant-inflatables/customised-shapes">Customised Shapes</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/giant-inflatables/dancing-tube">Dancing Tube</a></li>
</ul>
<ul class="list-unstyled">
<li><a href="https://www.creativeballoons.com.sg/product/giant-inflatables/inflate-1">Giant Advertising Inflatable</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/giant-inflatables/inflate-2">Lighted Tripod Balloon</a></li>
</ul>
</div>
<a class="see-all" href="https://www.creativeballoons.com.sg/product/giant-inflatables">Show All Giant Inflatables</a> </div>
</li>
<li><a href="https://www.creativeballoons.com.sg/product/balloon-printing">Balloon Printing </a></li>
<li><a href="https://www.creativeballoons.com.sg/Balloon-Sculpturing">Services</a></li>
<li><a href="https://www.creativeballoons.com.sg/special">Promotion</a></li>
<li><a href="https://www.creativeballoons.com.sg/contact">Contact Us</a></li>
</ul>
</div>
</nav>
</div>
</header>
<div class="container">
<ul class="breadcrumb hide">
<li><a href="https://www.creativeballoons.com.sg/"><i class="fa fa-home"></i></a></li>
<li><a href="https://www.creativeballoons.com.sg/index.php?route=error/not_found">Page not found!</a></li>
</ul>
<h2 class="green-banner"><span>Page not found!</span></h2>
<div class="row"> <div class="col-sm-12" id="content">
<p>The page you requested cannot be found.</p>
<div class="buttons">
<div class="pull-right"><a class="btn btn-primary" href="https://www.creativeballoons.com.sg/">Continue</a></div>
</div>
</div>
</div>
</div>
<footer>
<div class="container">
<div class="row">
<div class="col-sm-12"></div>
<div class="col-sm-12">
<ul class="list-inline footer-bottom">
<li><a href="https://www.creativeballoons.com.sg/">Home</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/balloon">Balloons</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/events-decor">Events Decor</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/giant-inflatables">Giant Inflatables</a></li>
<li><a href="https://www.creativeballoons.com.sg/product/balloon-printing">Balloon Printing </a></li>
<li><a href="https://www.creativeballoons.com.sg/Balloon-Sculpturing">Services</a></li>
<li><a href="https://www.creativeballoons.com.sg/special">Promotions</a></li>
<li><a href="https://www.creativeballoons.com.sg/contact">Contact Us</a></li>
</ul>
</div>
</div>
<div class="clearfix" style="margin-bottom: 5px;">
<div class="pull-left">
<ul class="list-inline footer-left">
<li><a href="https://www.creativeballoons.com.sg/privacy">Privacy Policy</a></li>
<li><a href="https://www.creativeballoons.com.sg/terms">Terms &amp; Conditions</a></li>
<li><a href="https://www.creativeballoons.com.sg/giant-inflatables-743930790">Latex Balloon - Custom Balloon Printing Services</a></li>
</ul>
</div>
<div class="pull-right">
<div class="powered">© 2021 Creative Balloons. All Rights Reserved.</div>
<div class="firstcom"><img class="firstcom-img" src="catalog/view/theme/default/image/firstcom_icon.png"/><a href="https://www.firstcom.com.sg/" target="_blank"> Web Design by Firstcom Solutions.</a></div>
</div>
</div>
</div>
</footer>
</body></html>
