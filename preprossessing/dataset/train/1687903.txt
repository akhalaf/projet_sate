<!DOCTYPE html>
<html lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<!-- Mobile Metas -->
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- Site Metas -->
<!-- CSRF Token -->
<meta content="loSEokLSjRod8ANM4hTEAuyQu9T8pb0zJhmi5ZEV" name="csrf-token"/>
<title>XANH TUOI TROPICAL FISH CO ., LTD | Xanh Tuoi Aquarium VN | XANHTUOI.COM | XANHTUOITROPICALFISH.COM</title>
<meta content="xanh tuoi xanh tuoi aquarium, xanh tuoi tropicalfish, xanhtuoi.com, xanhtuoitropicalfish.com" name="description"/>
<meta content="XANHTUOI, XANHTUOITROPICALFISH, FISH, AQUARIUM, CÁ CẢNH" name="keywords"/>
<meta content="XANH TUOI TROPICAL FISH CO.,LTD - XANH TUOI AQUARIUM" name="google-site-verification"/>
<meta content="XANH TUOI TROPICAL FISH CO.,LTD - XANH TUOI AQUARIUM" name="msvalidate.01"/>
<meta content="XANH TUOI TROPICAL FISH CO.,LTD - XANH TUOI AQUARIUM" name="alexaVerifyID"/>
<meta content="XANH TUOI TROPICAL FISH CO.,LTD - XANH TUOI AQUARIUM" name="pinterest"/>
<meta content="XANH TUOI TROPICAL FISH CO.,LTD - XANH TUOI AQUARIUM" name="yandex-verification"/>
<meta content="XANH TUOI TROPICAL FISH CO ., LTD | Xanh Tuoi Aquarium VN | XANHTUOI.COM | XANHTUOITROPICALFISH.COM" property="og:title"/>
<meta content="xanh tuoi xanh tuoi aquarium, xanh tuoi tropicalfish, xanhtuoi.com, xanhtuoitropicalfish.com" property="og:description"/>
<meta content="https://xanhtuoitropicalfish.com" property="og:url"/>
<meta content="www.xanhtuoitropicalfish.com" property="og:site_name"/>
<meta content="@xanhtuoi" name="twitter:site"/>
<script type="application/ld+json">{"@context":"https:\/\/schema.org","@type":"WebPage","name":"XANH TUOI TROPICAL FISH CO ., LTD | Xanh Tuoi Aquarium VN | XANHTUOI.COM | XANHTUOITROPICALFISH.COM","description":"xanh tuoi xanh tuoi aquarium, xanh tuoi tropicalfish, xanhtuoi.com, xanhtuoitropicalfish.com","url":"https:\/\/xanhtuoitropicalfish.com"}</script>
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous"> -->
<!-- Site Icons -->
<link href="https://xanhtuoi.com/public/common/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://xanhtuoi.com/public/common/images/apple-touch-icon.png" rel="apple-touch-icon"/>
<!-- Site CSS -->
<link href="https://xanhtuoi.com/public/common/Font-Awesome-5.6.1/css/all.css?v=1544635560" rel="stylesheet"/>
<!-- Bootstrap CSS -->
<link href="https://xanhtuoi.com/public/common/css/bootstrap.min.css?v=1586838972" rel="stylesheet"/>
<!-- Site CSS -->
<link href="https://xanhtuoi.com/public/common/css/style.css?v=1588842257" rel="stylesheet"/>
<!-- Responsive CSS -->
<link href="https://xanhtuoi.com/public/common/css/responsive.css?v=1587021219" rel="stylesheet"/>
<!-- Custom CSS -->
<link href="https://xanhtuoi.com/public/common/css/custom.css?v=1586838974" rel="stylesheet"/>
<link href="https://xanhtuoi.com/public/common/css/paginate.css?v=1586838978" rel="stylesheet"/>
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="https://xanhtuoi.com/public/common/lightbox/css/lc_lightbox.css?v=1578220500" rel="stylesheet"/>
<link href="https://xanhtuoi.com/public/common/language/css/flags.css?v=1521776400" rel="stylesheet"/>
<script src="https://xanhtuoi.com/public/common/js/jquery-1.12.4.min.js"></script>
<script src="https://xanhtuoi.com/public/common/js/jquery.vide.js"></script>
</head>
<body>
<!-- Start Main Top -->
<div class="main-top">
<div class="container-fluid bg-container">
<div class="header-top row">
<div class="container-top">
<div class="block-inline">
<p>
<span class="block-inline-left">
<i class="fas fa-map-marker-alt"></i>
<a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d980.3069148405231!2d106.6396759996835!3d10.63941410799871!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752faa14aaadd3%3A0x6ef875c8623cf16e!2sXANH%20TUOI%20TROPICAL%20FISH%20CO.%2CLTD%20(%20BRANCH)!5e0!3m2!1sen!2s!4v1586876235504!5m2!1sen!2s&amp;lang=en" target="_blank"> Ho Chi Minh, Vietnam</a>
</span>
<span class="block-inline-right"><i class="fas fa-phone-square"></i> Hotline : <a href="tel:84913681086"><strong>(+84) 909 810 346</strong></a> / <a href="tel:84938505397"><strong>(+84) 938 505 397</strong></a></span>
<span class="block-inline-right"><i class="fas fa-envelope"></i> Email: <a href="mailto:tuoi@xanhtuoitropicalfish.com"><strong>tuoi@xanhtuoitropicalfish.com</strong></a></span>
</p>
</div>
<div class="block-right">
<div data-selected-country="GB" id="options"> </div>
</div>
</div>
</div>
</div>
</div>
<!-- End Main Top -->
<!-- Start Main Top -->
<header class="main-header">
<!-- Start Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav nav-logo">
<div class="container">
<!-- Start Header Navigation -->
<div class="navbar-header">
<button aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbar-menu" data-toggle="collapse" type="button">
<i class="fa fa-bars"></i>
</button>
<a class="navbar-brand navbar-custom" href="https://xanhtuoi.com"><img alt="" class="logo" height="100" src="https://xanhtuoi.com/public/common/images/logo-xt.png" width="130"/></a>
</div>
<!-- End Header Navigation -->
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="navbar-menu">
<ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
<li class="nav-item "><a class="nav-link" href="https://xanhtuoi.com">Home</a></li>
<li class="nav-item "><a class="nav-link" href="https://xanhtuoi.com/about">About Us</a></li>
<li class="nav-item "><a class="nav-link" href="https://xanhtuoi.com/products">Products</a></li>
<li class="nav-item "><a class="nav-link" href="https://xanhtuoi.com/videos">Videos</a></li>
<li class="nav-item "><a class="nav-link" href="https://xanhtuoi.com/facility">Facility</a></li>
<li class="nav-item "><a class="nav-link" href="https://xanhtuoi.com/contact">Contact Us</a></li>
</ul>
</div>
<!-- /.navbar-collapse -->
<!-- Start Atribute Navigation -->
<div class="attr-nav">
<ul>
<li class="search"><a href="javascript:void(0);"><i class="fa fa-search"></i></a></li>
</ul>
</div>
<!-- End Atribute Navigation -->
</div>
</nav>
<!-- End Navigation -->
</header>
<!-- End Main Top -->
<!-- Start Top Search -->
<div class="top-search">
<div class="container">
<form action="https://xanhtuoi.com/search" enctype="multipart/form-data" method="get">
<!--  <input type="hidden" name="_token" value="loSEokLSjRod8ANM4hTEAuyQu9T8pb0zJhmi5ZEV"> -->
<div class="input-group">
<input class="form-control" name="keyword" placeholder="Search here..." type="text" value=""/>
<button class="btn-search" type="submit"> <i class="fa fa-search"></i> </button>
</div>
</form>
</div>
</div>
<!-- End Top Search -->
<!-- Content -->
<!-- /Content -->
<!-- Start Footer  -->
<footer>
<div class="footer-main">
<div class="container">
<div class="row">
<div class="col-lg-4 col-md-12 col-sm-12">
<div class="footer-link">
<h4>TAGS</h4>
<ul>
<li>
<label class="tags" for="Xanh Tuoi">
<a href="https://xanhtuoi.com"> Xanh Tuoi </a></label>
<label class="tags" for="Aquarium">
<a href="https://xanhtuoi.com/products"> Aquarium </a></label>
<label class="tags" for="Zoanthid button polyps">
<a href="https://xanhtuoi.com/products/detail/7/24"> Zoanthid button polyps </a></label>
<label class="tags" for="Branching hammer coral">
<a href="https://xanhtuoi.com/products/detail/7/145"> Branching hammer coral </a></label>
<label class="tags" for="Torch Coral">
<a href="https://xanhtuoi.com/products/detail/7/90"> Torch Coral </a></label>
<label class="tags" for="Acans Coral">
<a href="https://xanhtuoi.com/products/detail/7/57"> Acans Coral </a></label>
<label class="tags" for="Anemones">
<a href="https://xanhtuoi.com/products/detail/7/8"> Anemones </a></label>
<label class="tags" for="Open brain">
<a href="https://xanhtuoi.com/products/detail/7/151"> Open brain </a></label>
<label class="tags" for="Discosoma">
<a href="https://xanhtuoi.com/products/detail/9/17"> Discosoma </a></label>
<label class="tags" for="Mushroom">
<a href="https://xanhtuoi.com/products/detail/9/45"> Mushroom </a></label>
<label class="tags" for="Bubble">
<a href="https://xanhtuoi.com/products/detail/8/3"> Bubble </a></label>
<label class="tags" for="vietnam coral supplier">
<a href="https://xanhtuoi.com"> vietnam coral supplier </a></label>
<label class="tags" for="vietnam reef supplier">
<a href="https://xanhtuoi.com"> vietnam reef supplier </a></label>
<label class="tags" for="Vietnam reefs">
<a href="https://xanhtuoi.com"> Vietnam reefs </a></label>
<label class="tags" for="Vietnam LPS SPS">
<a href="https://xanhtuoi.com"> Vietnam LPS SPS </a></label>
<label class="tags" for="LPS coral">
<a href="https://xanhtuoi.com"> LPS coral </a></label>
<label class="tags" for="sps coral">
<a href="https://xanhtuoi.com"> sps coral </a></label>
<label class="tags" for="anemone">
<a href="https://xanhtuoi.com"> anemone </a></label>
<label class="tags" for="live stock">
<a href="https://xanhtuoi.com"> live stock </a></label>
<label class="tags" for="Coral exporter">
<a href="https://xanhtuoi.com"> Coral exporter </a></label>
</li>
</ul>
</div>
<div class="footer-widget" style="margin-top:50px;">
<ul>
<li><a href="https://www.facebook.com/xanhtuoitopicalfish/" target="_blank"><i aria-hidden="true" class="fab fa-facebook"></i></a></li>
<li><a href="https://twitter.com/xanhtuoitrop" target="_blank"><i aria-hidden="true" class="fab fa-twitter"></i></a></li>
<li><a href="https://www.instagram.com/explore/locations/1660461597539903/xanh-tuoi-aquarium-vn/?hl=vi" target="_blank"><i aria-hidden="true" class="fab fa-instagram"></i></a></li>
<li><a href="https://www.youtube.com/channel/UCDrOCsRqRCkIyfjj6K4A9qQ" target="_blank"><i class="fab fa-youtube"></i></a></li>
</ul>
</div>
</div>
<div class="col-lg-4 col-md-12 col-sm-12">
<div class="footer-link">
<h4>Menu</h4>
<ul>
<li><a class="menu-footer" href="https://xanhtuoi.com">Home</a></li>
<li><a class="menu-footer" href="https://xanhtuoi.com/about">About Us</a></li>
<li><a class="menu-footer" href="https://xanhtuoi.com/products">Products</a></li>
<li><a class="menu-footer" href="https://xanhtuoi.com/videos">Videos</a></li>
<li><a class="menu-footer" href="https://xanhtuoi.com/facility">Facility</a></li>
<li><a class="menu-footer" href="https://xanhtuoi.com/contact">Contact Us</a></li>
<li>
</li></ul>
</div>
</div>
<div class="col-lg-4 col-md-12 col-sm-12">
<div class="footer-link-contact">
<h4>Contact Us</h4>
<ul>
<li>
<p><i class="fas fa-map-marker-alt"></i><strong>Head Office </strong>: 121/4 Me Coc Str, ward 15, District 8, HCMC, Vietnam</p>
<p><i class="fas fa-map-marker-alt"></i><strong>Branch </strong>: C5/1A, Group 5, Hamlet 3, Qui Duc Commune, Binh Chanh District, HCMC, Vietnam</p>
</li>
<li>
<p><i class="fas fa-phone-square"></i><strong>Phone </strong>: <a href="tel:842836201608">(+84) 283 620 1608</a></p>
<p><i class="fas fa-fax"></i><strong>Fax </strong>: <a href="tel:842836201609">(+84) 283 620 1609</a></p>
</li>
<li>
<p><i class="fas fa-envelope"></i><strong>Email </strong>: <a href="mailto:tuoi@xanhtuoitropicalfish.com">tuoi@xanhtuoitropicalfish.com</a></p>
</li>
<li>
<p style="displlay:block;vertical-align: middle;"><i class="fab fa-whatsapp" style="font-size:26px;"></i> <a href="https://api.whatsapp.com/send?phone=+84913681086"> WhatsApp <span>(+84) 913 681 086</span></a></p>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</footer>
<!-- End Footer  -->
<!-- Start copyright  -->
<div class="footer-copyright">
<p class="footer-company">© 2020 <a href="#"><strong>Xanh Tuoi</strong></a> Tropical Fish Co.,Ltd. All Rights Reserved.</p>
</div>
<!-- End copyright  -->
<a href="#" id="back-to-top" style="display: none;" title="Back to top">↑</a>
<script src="https://xanhtuoi.com/public/common/js/imagezoom.js"></script>
<script src="https://xanhtuoi.com/public/common/language/js/jquery.flagstrap.js"></script>
<script>
        $(document).ready(function(){
            
        });
    </script>
<script>
        $('#options').flagStrap({
            countries: {
                "GB": "English",
                "VN": "Tiếng Việt"
            },
            placeholder: false,
            buttonSize: "btn-sm",
            buttonType: "btn-primary",
            labelMargin: "10px",
            scrollable: false,
            scrollableHeight: "350px",
            onSelect: function (value, element) {
                var params = "";            
                if(value == 'VN'){
                    if(params != ''){
                        window.location.href = "https://xanhtuoi.com/setlocale/vi"+'?'+params;
                    }else{
                        window.location.href = "https://xanhtuoi.com/setlocale/vi";
                    }
                }else{
                    if(params != ''){
                        window.location.href = "https://xanhtuoi.com/setlocale/en"+'?'+params;
                    }else{
                        window.location.href = "https://xanhtuoi.com/setlocale/en";
                    }
                }
            }
        });
    </script>
<script src="https://xanhtuoi.com/public/common/Font-Awesome-5.6.1/js/all.js?v=1544635560"></script>
<!-- ALL JS FILES -->
<script src="https://xanhtuoi.com/public/common/js/popper.min.js?v=1587051334"></script>
<script src="https://xanhtuoi.com/public/common/js/bootstrap.min.js?v=1587051326"></script>
<script src="https://xanhtuoi.com/public/common/lightbox/js/lc_lightbox.lite.js?v=1576697580"></script>
<script src="https://xanhtuoi.com/public/common/lightbox/lib/AlloyFinger/alloy_finger.min.js?v=1576697580"></script>
<script src="https://xanhtuoi.com/public/common/lightbox/lib/AlloyFinger/alloy_finger.min.js?v=1576697580"></script>
<!-- ALL PLUGINS -->
<script src="https://xanhtuoi.com/public/common/js/jquery.superslides.min.js?v=1587051330"></script>
<script src="https://xanhtuoi.com/public/common/js/bootstrap-select.js?v=1587051326"></script>
<script src="https://xanhtuoi.com/public/common/js/inewsticker.js?v=1587051329"></script>
<script src="https://xanhtuoi.com/public/common/js/bootsnav.js?v=1587051326"></script>
<script src="https://xanhtuoi.com/public/common/js/images-loded.min.js?v=1587051328"></script>
<script src="https://xanhtuoi.com/public/common/js/isotope.min.js?v=1587051329"></script>
<script src="https://xanhtuoi.com/public/common/js/owl.carousel.min.js?v=1587051333"></script>
<script src="https://xanhtuoi.com/public/common/js/baguetteBox.min.js?v=1587051325"></script>
<script src="https://xanhtuoi.com/public/common/js/custom.js?v=1588603969"></script>
<script type="text/javascript">
    $(".click-go").click(function() {
        $('html, body').animate({
            scrollTop: $("#div-gallery").offset().top - 199
        }, 600);
        return false;
    });
    </script>
<!-- ALL JS FILES -->
</body>
</html>
