<!DOCTYPE html>
<html><head><meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">// Script snippet courtesy of user 'Igal' who posted this on StackOverflow at
// http://stackoverflow.com/questions/1173194/select-all-div-text-with-single-mouse-click
function selectText( containerid ) {var node = document.getElementById( containerid );
if ( document.selection ) {var range = document.body.createTextRange();
range.moveToElementText( node  );
range.select();} else if ( window.getSelection ) {var range = document.createRange();
range.selectNodeContents( node );
window.getSelection().removeAllRanges();
window.getSelection().addRange( range );}}
function copyShortUrlToClipboardAndNotifyUser() {selectText( 'shortenedUrl' );
document.execCommand( 'copy' );
document.getElementById( 'message' ).innerText = "Copied to clipboard!";}</script>
<style>h1 {font-weight: bolder;
font-size: 5em;}
div.header {margin-bottom: 50px;}
div.result {padding: 30px;
border-radius: 10px;
background-color: #ECECEC;}
.large {font-size: 2em;}
.truncate {width: 100%;
white-space: nowrap;
overflow: hidden;
text-overflow: ellipsis;}
hr {margin-bottom: 3px;}
a.clipboard:link, a.clipboard:hover {text-decoration: none;}</style>
<title>9m URL shortener</title>
</head>
<body onload="document.forms[0].url.focus();"><div class="container"><div class="col-md-offset-3 col-md-6 col-xs-12 header"><h1 class="text-center"><a href="/"><img alt="9m" src="/static/svg/logo.svg" width="80%"/>
</a>
</h1>
</div>
<div class="col-md-offset-3 col-md-6 col-xs-12"><div class="row"><div class="col-md-12"><form action="/create" method="post" role="form"><div class="input-group input-group-lg"><input class="form-control" name="url" placeholder="URL to shorten" type="text"/>
<span class="input-group-btn"><button class="btn btn-primary" type="submit">Shorten</button>
</span>
</div>
</form>
</div>
</div>
</div>
<div class="row"><div class="col-md-offset-3 col-md-6 col-xs-12"><hr/>
</div>
</div>
<div class="row"><div class="col-md-offset-3 col-md-6 col-xs-12 text-center"><a href="https://www.github.com/ehamberg/9m">Code</a>
◇
<a href="/about">About</a>
</div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-304186-3', '9m.no');
ga('send', 'pageview');</script>
</div>
</body>
</html>
