<!DOCTYPE html>
<html lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Member Login | APSense.com</title>
<meta content="" name="description"/>
<meta content="XCal1IBdROQl4YOzVp2IHthbuoJvwI2_5UjQKQdYF2Y" name="google-site-verification"/>
<link href="/v50_css/style.css?ver=5.3" media="screen" rel="stylesheet"/>
<link href="/favicon.ico" rel="shortcut icon"/>
<script src="/v50_js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/v50_js/global.js?ver=5.3" type="text/javascript"></script>
<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="/v50_css/ie.css?ver=5.3" />
<![endif]-->
</head>
<body> <div id="wrapper">
<div class="header">
<div class="container_6 clearfix">
<div class="__pt_logo" id="logo"><a href="/"><img alt="APSense Business Social Networking" src="/v50_images/logo.png"/></a></div>
<div id="header_nav" style="width:280px;float:left;margin-left:10px;">
<ul class="clearfix">
</ul>
</div>
<div id="login" style="width:530px">
</div>
</div>
</div>
<div id="page_body">
<div class="container_6 clearfix">
<div class="main-content grid_4" style="margin:30px auto;min-height:100px;float:none;display:block;">
<div class="section" id="workspace">
<h3>Member Login</h3>
<form action="/login" class="form" method="post">
<input name="url" type="hidden" value=""/>
<input name="action" type="hidden" value="login"/>
<fieldset style="min-height:200px">
<legend>The field with * is required.</legend>
<div class="space_20h"></div>
<label>Username <em>*</em><small>Letters and numbers only</small></label><input class="input_index" name="username" required="required" type="text" value=""/>
<label>Password <em>*</em><small></small></label><input class="input_index" name="password" required="required" type="password"/>
<label> </label>
<div class="custom" style="width:350px">
<input checked="" class="radiobox" id="remember" name="remember" type="checkbox" value="1"/> Remember me on this computer
					</div>
<div class="action">
<button class="button button-blue" type="submit"><span class="accept"></span>Sign In</button>
<br/><br/>
<a class="admin" href="/register" target="_parent"><b>Create a new account</b></a><br/>
<a href="/forgot.html">Forgot password? Click here.</a><br/>
<div style="margin-top:10px;margin-bottom:10px">
<span class="line">            </span>
<span class="or">  OR  </span>
<span class="line">            </span>
</div>
<a href="javascript:" onclick="fbActionConnect();"><img alt="Facebook Connect" src="/images/facebook_connect.gif"/></a>
<div style="margin-top:10px;margin-bottom:10px">
<span class="line">            </span>
<span class="or">  OR  </span>
<span class="line">            </span>
</div>
<button class="button button-gray" onclick="location.href='/qrlogin'" style="width:194px;font-size:12px;" type="button"><img height="16" src="/v50_images/qrcode.png" style="vertical-align:middle" width="16"/>  Login by QR Code</button>
<div class="clear"></div>
</div>
</fieldset>
</form>
</div>
</div>
</div>
<div id="push"></div>
</div>
</div>
<div id="footer_nav">
<div class="container_6 clearfix __pt_footer" id="footer-inner">
<div class="grid_6">
<a href="/">Home</a>  -  <a href="/about.html">About APSense</a>  -  <a href="/howitworks.html">How It Works</a>  -  <a href="/privacy.html">Privacy Policy</a>  -  <a href="/terms.html">Terms of Use</a>  -  <a href="/contact.html">Contact Us</a>  -  <a href="/tools.html">Tools</a>  -  <a href="/sitemap.html">Directory</a>  -  <a href="/upgrade.html"><b>Upgrade Your Account</b></a>
<span class="fr">©2020 APSense Ltd. All Rights Reserved.  </span>
</div>
</div>
</div>
<div id="fb-root"></div>
<script>
function logoutFacebookUser() {
	FB.logout(function(response) {
 window.location.reload(); 
 });}
function fbActionConnect() {
 FB.login(function(response) {
 if (response.authResponse) {
 window.location = '/fbconnect.php';
 } }, {scope:'email'}); }
window.fbAsyncInit = function() { 
 FB.init({appId: 135021456540158, status: true, cookie: true, xfbml: true, oauth: true});
};
(function() {
var e = document.createElement('script'); e.async = true;
e.src = document.location.protocol +
'//connect.facebook.net/en_US/all.js';
document.getElementById('fb-root').appendChild(e);
}());
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17991863-1']);
  _gaq.push(['_setDomainName', '.apsense.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html> 