<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="Rubrikk" name="author"/>
<meta content="Ananzi is a search engine for South Africa's classified ads as well as a South African businesses. Ananzi Ads is your one-stop shop for finding the best ads for property, jobs, cars etc." name="description"/>
<title>Properties for sale and rent, Jobs, Used Cars and more - Ananzi.co.za</title>
<link href="https://www.ananzi.co.za/css/extra.css" rel="stylesheet"/>
<link href="https://www.ananzi.co.za/css/pure-min.css" rel="stylesheet"/>
<!--  <link rel="stylesheet" href="https://www.ananzi.co.za/css/southafrica.css" /> -->
<link href="https://www.ananzi.co.za/css/tabs-css.css" rel="stylesheet" type="text/css"/>
<link href="https://www.ananzi.co.za/css/jquery.auto-complete.css" rel="stylesheet" type="text/css"/>
<!--<link rel="stylesheet" type="text/css" href="https://www.ananzi.co.za/css/template.css?1610516399" />-->
<!--[if lte IE 8]>
  <link rel="stylesheet" href="https://www.ananzi.co.za/css/grids-responsive-old-ie-min.css">
  <![endif]-->
<!--[if gt IE 8]><!-->
<link href="https://www.ananzi.co.za/css/grids-responsive-min.css" rel="stylesheet"/>
<!--<![endif]-->
<link href="https://www.ananzi.co.za/css/main.css" rel="stylesheet"/>
<link href="https://www.ananzi.co.za/css/font-awesome.min.css" rel="stylesheet"/>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body data-offset="200" data-spy="scroll" data-target=".pure-menu">
<script>
  var base_url = 'https://www.ananzi.co.za/';
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-2409506-6', 'www.ananzi.co.za');
  ga('send', 'pageview');

  setTimeout("ga('send', 'event', 'read', '15 seconds')",15000);

</script>
<navbar class="header pure-menu-fixed">
<div class="container">
<div class="home-menu pure-menu pure-menu-open pure-menu-horizontal">
<a class="navbar-brand pure-menu-heading pull-left" href="https://www.ananzi.co.za/"><img alt="ananzi.co.za" src="https://www.ananzi.co.za/images/logo-big.png"/></a>
<ul class="navbar-nav nav pull-right">
<li><a href="https://www.ananzi.co.za/ads/">Ananzi Ads</a></li>
<li><a href="https://www.ananzi.co.za/sadirectory">SA Directory</a></li>
<li><a href="http://www.ananzimail.co.za/" rel="nofollow" target="_blank">Ananzi Mail</a></li>
</ul>
</div>
</div>
</navbar>
<header class="relative" id="top-4">
<div class="container text-center">
<h1 class="color-white search-header">Finding South Africa</h1>
<div class="search-wrapper">
<div class="form-wrappers" id="ananzi-sadirectory-form-wrapper" style="display:none;">
<form action="https://www.ananzi.co.za/sadirectory/" id="ananzi-sadirectory-form" method="get">
<span class="color-white search-text">Find me a </span>
<input class="search-input" id="ananzi-sadirectory-q" name="q" placeholder="eg. plumbers" type="text" value=""/>
<a class="pure-button pure-button-white search-select right-auto dropdown" href="#">
                SA Directory <i class="icon-down-dir"></i>
<ul class="menu" style="visibility: hidden;">
<li onclick="toggleSearch('ananzi-ads');">Ananzi Ads</li>
</ul>
</a>
<button class="pure-button pure-button-white search-button right-auto" tyoe="submit"><i class="icon-search"></i></button>
</form>
</div>
<div class="form-wrappers" id="ananzi-ads-form-wrapper">
<form action="https://www.ananzi.co.za/ads/get-top-box-results/" id="ananzi-ads-form" method="post">
<input id="ananzi-ads-cat" name="cat" type="hidden" value=""/>
<input name="geo1name" type="hidden" value="south-africa"/>
<span class="color-white search-text">Find me a </span>
<input class="search-input" id="ananzi-ads-q" name="q" placeholder="eg. iphone" type="text"/>
<a class="pure-button pure-button-white search-select right-auto dropdown" href="#">
                Ananzi Ads <i class="icon-down-dir"></i>
<ul class="menu" style="visibility: hidden;">
<li onclick="toggleSearch('ananzi-sadirectory');">SA Directory</li>
</ul>
</a>
<button class="pure-button pure-button-white search-button right-auto" id="btn-submit-ads" type="submit"><i class="icon-search"></i></button>
</form>
</div>
</div>
<div class="popular-searches">
<nav>
<em>Popular searches: </em>
<h2>
<a href="https://www.ananzi.co.za/ads/cars/volkswagen-polo" onclick="return cookieLinkSave('https://www.ananzi.co.za/ads/cars/volkswagen-polo');" target="_blank" title="Volkswagen Polo cars for sale"> Volkswagen Polo cars for sale </a>
</h2>
<span>, </span>
<h2>
<a href="https://www.ananzi.co.za/ads/houses-flats-for-sale/house--johannesburg-city" onclick="return cookieLinkSave('https://www.ananzi.co.za/ads/houses-flats-for-sale/house--johannesburg-city');" target="_blank" title="Houses for sale in Johannesburg"> Houses for sale in Johannesburg </a>
</h2>
<span>, </span>
<h2>
<a href="https://www.ananzi.co.za/ads/houses-flats-for-sale/cape-town--house" onclick="return cookieLinkSave('https://www.ananzi.co.za/ads/houses-flats-for-sale/cape-town--house');" target="_blank" title="Houses for sale in Cape Town"> Houses for sale in Cape Town </a>
</h2>
<span class="dots">...</span>
<a href="javascript:void(0)" id="more-searches" rel="nofollow">more</a>
<div class="popular-searches-more">
<span>, </span>
<h3>
<a href="https://www.ananzi.co.za/ads/cars/volkswagen-golf" onclick="return cookieLinkSave('https://www.ananzi.co.za/ads/cars/volkswagen-golf');" target="_blank" title="Volkswagen Golf cars for sale"> Volkswagen Golf cars for sale </a>
</h3>
<span>, </span>
<h3>
<a href="https://www.ananzi.co.za/ads/cars/toyota-corolla" onclick="return cookieLinkSave('https://www.ananzi.co.za/ads/cars/toyota-corolla');" target="_blank" title="Toyota Corolla cars for sale"> Toyota Corolla cars for sale </a>
</h3>
<span>, </span>
<h3>
<a href="https://www.ananzi.co.za/ads/houses-flats-for-rent/apartment--johannesburg-city" onclick="return cookieLinkSave('https://www.ananzi.co.za/ads/houses-flats-for-rent/apartment--johannesburg-city');" target="_blank" title="Flats for rent in Johannesburg"> Flats for rent in Johannesburg </a>
</h3>
<span>, </span>
<h3>
<a href="https://www.ananzi.co.za/ads/houses-flats-for-sale/durban-city--house" onclick="return cookieLinkSave('https://www.ananzi.co.za/ads/houses-flats-for-sale/durban-city--house');" target="_blank" title="Houses for sale in Durban"> Houses for sale in Durban </a>
</h3>
<span>, </span>
<h3>
<a href="https://www.ananzi.co.za/ads/jobs/johannesburg-city" onclick="return cookieLinkSave('https://www.ananzi.co.za/ads/jobs/johannesburg-city');" target="_blank" title="Jobs available in Johannesburg"> Jobs available in Johannesburg </a>
</h3>
<span>, </span>
<h3>
<a href="https://www.ananzi.co.za/ads/houses-flats-for-rent/apartment--cape-town" onclick="return cookieLinkSave('https://www.ananzi.co.za/ads/houses-flats-for-rent/apartment--cape-town');" target="_blank" title="Flats for rent in Cape Town"> Flats for rent in Cape Town </a>
</h3>
<span>, </span>
<h3>
<a href="https://www.ananzi.co.za/ads/jobs/cape-town" onclick="return cookieLinkSave('https://www.ananzi.co.za/ads/jobs/cape-town');" target="_blank" title="Jobs available in Cape Town"> Jobs available in Cape Town </a>
</h3>
<span>, </span>
<h3>
<a href="https://www.ananzi.co.za/ads/cars/bmw-3-series" onclick="return cookieLinkSave('https://www.ananzi.co.za/ads/cars/bmw-3-series');" target="_blank" title="BMW 3-Series cars for sale"> BMW 3-Series cars for sale </a>
</h3>
<span>, </span>
</div>
</nav>
</div>
</div>
</header>
<section class="container content-wrapper pure-g">
<div class="pure-u-1-1 area-ads">
<div class="pure-g" id="box-wrapper">
<section class="content-wrapper pure-g area-top-banner-wrapper">
<div class="area-top-banner pure-u-1-1">
<div class="bannerAd-top margin-auto">
<script type="text/javascript"><!--//<![CDATA[
           var m3_u = (location.protocol=='https:'?'https://www.servads.co.za/open/www/delivery/ajs.php':'http://www.servads.co.za/open/www/delivery/ajs.php');
           var m3_r = Math.floor(Math.random()*99999999999);
           if (!document.MAX_used) document.MAX_used = ',';
           document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
           document.write ("?zoneid=73");
           document.write ('&amp;cb=' + m3_r);
           if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
           document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
           document.write ("&amp;loc=" + escape(window.location));
           if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
           if (document.context) document.write ("&context=" + escape(document.context));
           if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
           document.write ("'><\/scr"+"ipt>");
        //]]>-->
        </script>
<noscript><a href="http://www.servads.co.za/open/www/delivery/ck.php?n=a85b49be&amp;cb=INSERT_RANDOM_NUMBER_HERE" target="_blank"><img alt="" border="0" src="http://www.servads.co.za/open/www/delivery/avw.php?zoneid=73&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a85b49be"/></a></noscript>
</div>
</div>
</section>
<section>
<br clear="all"/>
<br clear="all"/>
<div class="pure-g fb-promo-inner">
<div class="fb-promo-cover">
<a href="http://www.brabysebooks.co.za/ebooks/futurebride/files/downloads/Future%20Bride%202020.pdf" target="_blank"><img src="https://www.brabys.com/images/futurebride-cover-212x300.png"/></a>
</div>
<div class="fb-promo-text">
<p><strong>Future Bride Magazine</strong> published by A C Braby is South Africa’s Premier Bridal Magazine supported by an active Social Media which boasts a large following. It is dedicated to providing inspiration, latest trends and colors, as well as creative trend setting ideas for Future Brides.</p>
<p>With social distancing becoming the norm and Bridal Fairs being prohibited, we do not want you to lose out on what is trend setting right now.</p><p>Please download your free E-Magazine now and stay on trend. Also don’t forget to follow us on <a href="https://web.facebook.com/futurebrideSa/" rel="nofollow" target="_blank">Facebook</a> where we will keep you up to date with what is happening in the Bridal Industry.</p>
<a class="btn btn-primary" href="http://www.brabysebooks.co.za/ebooks/futurebride/files/downloads/Future%20Bride%202020.pdf" target="_blank">Download your free copy</a>
</div>
</div>
</section>
<section class="pure-u-lg-1-3 pure-u-md-1-3 pure-u-sm-1-2 pure-u-1-1 p-block box">
<div class="item">
<a class="item-header item-header-property bg-1" href="https://www.ananzi.co.za/ads/property/" title="810,663">
<div class="item-logo item-logo-1"></div>
<h2 class="item-header-text">Property</h2>
</a>
<ul class="category-list">
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/houses-flats-for-rent/" title="156,081"><i class="icon-angle-right"></i>Houses &amp; Flats for Rent</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/houses-flats-for-sale/" title="470,520"><i class="icon-angle-right"></i>Houses &amp; Flats for Sale</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/office-space-commercial/" title="61,703"><i class="icon-angle-right"></i>Office Space &amp; Commercial</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/other-properties/" title="45,438"><i class="icon-angle-right"></i>Other Properties</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/vacant-land-for-sale/" title="76,921"><i class="icon-angle-right"></i>Vacant Land for Sale</a></h3></li>
</ul>
</div>
</section>
<section class="pure-u-lg-1-3 pure-u-md-1-3 pure-u-sm-1-2 pure-u-1-1 p-block box">
<div class="item">
<a class="item-header item-header-property bg-2" href="https://www.ananzi.co.za/ads/jobs/" title="245,742">
<div class="item-logo item-logo-2"></div>
<h2 class="item-header-text">Jobs</h2>
</a>
<ul class="category-list">
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/jobs/accounting" title="Accounting jobs"><i class="icon-angle-right"></i>Accounting jobs</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/jobs/engineering" title="Engineering jobs"><i class="icon-angle-right"></i>Engineering jobs</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/jobs/it" title="IT jobs"><i class="icon-angle-right"></i>IT jobs</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/jobs/management" title="Management jobs"><i class="icon-angle-right"></i>Management jobs</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/jobs/sales" title="Sales jobs"><i class="icon-angle-right"></i>Sales jobs</a></h3></li>
</ul>
</div>
</section>
<section class="pure-u-lg-1-3 pure-u-md-1-3 pure-u-sm-1-2 pure-u-1-1 p-block box">
<div class="item">
<a class="item-header item-header-property bg-3" href="https://www.ananzi.co.za/ads/motoring/" title="337,883">
<div class="item-logo item-logo-3"></div>
<h2 class="item-header-text">Motoring</h2>
</a>
<ul class="category-list">
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/boats-marine/" title="4,397"><i class="icon-angle-right"></i>Boats &amp; Marine</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/cars/" title="299,208"><i class="icon-angle-right"></i>Cars</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/farm-vehicles-equipment/" title="5,171"><i class="icon-angle-right"></i>Farm Vehicles &amp; Equipment</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/heavy-vehicles/" title="7,690"><i class="icon-angle-right"></i>Heavy Vehicles</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/motorcycles-bikes/" title="6,316"><i class="icon-angle-right"></i>Motorcycles &amp; Bikes</a></h3></li>
</ul>
</div>
</section>
<section class="pure-u-lg-1-3 pure-u-md-1-3 pure-u-sm-1-2 pure-u-1-1 p-block box">
<div class="item">
<a class="item-header item-header-property bg-4" href="https://www.ananzi.co.za/ads/stuff/" title="140,644">
<div class="item-logo item-logo-4"></div>
<h2 class="item-header-text">Stuff</h2>
</a>
<ul class="category-list">
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/art-antiques-collectibles/" title="8,954"><i class="icon-angle-right"></i>Art, Antiques &amp; Collectibles</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/baby-kids/" title="15,560"><i class="icon-angle-right"></i>Baby &amp; Kids</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/books-games/" title="1,837"><i class="icon-angle-right"></i>Books &amp; Games</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/business-inventory/" title="452"><i class="icon-angle-right"></i>Business &amp; Inventory</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/cameras-photo/" title="4,326"><i class="icon-angle-right"></i>Cameras &amp; Photo</a></h3></li>
</ul>
</div>
</section>
<section class="pure-u-lg-1-3 pure-u-md-1-3 pure-u-sm-1-2 pure-u-1-1 p-block box">
<div class="item">
<a class="item-header item-header-property bg-5" href="https://www.ananzi.co.za/ads/pets/" title="7,790">
<div class="item-logo item-logo-5"></div>
<h2 class="item-header-text">Pets &amp; Animals</h2>
</a>
<ul class="category-list">
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/cats-kittens/" title="254"><i class="icon-angle-right"></i>Cats &amp; Kittens</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/dogs-puppies/" title="2,462"><i class="icon-angle-right"></i>Dogs &amp; Puppies</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/fish-aquariums/" title="828"><i class="icon-angle-right"></i>Fish &amp; Aquariums</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/horses-ponies/" title="111"><i class="icon-angle-right"></i>Horses &amp; Ponies</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/livestock-poultry/" title="1,257"><i class="icon-angle-right"></i>Livestock &amp; Poultry</a></h3></li>
</ul>
</div>
</section>
<section class="pure-u-lg-1-3 pure-u-md-1-3 pure-u-sm-1-2 pure-u-1-1 p-block box">
<div class="item">
<a class="item-header item-header-property bg-6" href="https://www.ananzi.co.za/ads/professional-services/" title="11,413">
<div class="item-logo item-logo-6"></div>
<h2 class="item-header-text">Professional Services</h2>
</a>
<ul class="category-list">
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/cleaning-home-help/" title="675"><i class="icon-angle-right"></i>Cleaning &amp; Home Help Services</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/computer-web-design/" title="623"><i class="icon-angle-right"></i>Computer &amp; Web Services</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/entertainment/" title="244"><i class="icon-angle-right"></i>Entertainment Services</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/events-services/" title="29"><i class="icon-angle-right"></i>Events Services</a></h3></li>
<li class="item-category"><h3><a href="https://www.ananzi.co.za/ads/financial/" title="681"><i class="icon-angle-right"></i>Financial Services</a></h3></li>
</ul>
</div>
</section>
<style>

.fb-promo-cover {
float:left;
width: 25%;
}
.fb-promo-text {
float: left;
width: 70%;
}

.fb-promo-cover {
    margin-right: 20px;
}

.fb-promo-cover img {
    max-width: 212px;
}


.fb-promo-text a {
    text-decoration: underline;
    font-weight: bold;
}
.fb-promo-text strong {
    font-weight: bold;
}

.fb-promo-text {
    font-size: 16px;
}

.fb-promo-inner {
    background: rgba(255,182,193, 0.32);            
    padding: 20px;
    border-radius: 4px;
    -webkit-box-shadow: 2px 4px 16px -8px rgba(0,0,0,0.32);
    -moz-box-shadow: 2px 4px 16px -8px rgba(0,0,0,0.32);
    box-shadow: 2px 4px 16px -8px rgba(0,0,0,0.32);
    margin-bottom: 40px;
}

.fb-promo-inner .btn {
    background-color: #CD202C;
    border-color: #CD202C;
    text-transform: uppercase;
    font-size: 12px;
    font-weight: bold;
    text-decoration: none;
    color: #fff;
	padding: 10px;
	padding-top: 10px;

}

        @media screen and (max-width: 767px) {


		.fb-promo-text {
			width: 100%;
		}

            .fb-promo-cover {
                text-align: center;
                width: 100%;
            }

            .fb-promo-cover img {
		margin: 0 auto;
		display: block;	
		}

            .fb-promo-cover a {
                margin: 0 auto !important;
                display: block!important;
                padding-bottom: 20px;
            }

            .fb-promo-inner .btn {
                width: 100%;
            }

        }

</style>
</div>
</div>
</section>
<footer class="footer">
<div class="container">
<ul class="footer-menu">
<li><a href="https://www.ananzi.co.za/Add_site">Add Site</a></li>
<li><a href="https://www.ananzi.co.za/advertise">Advertise</a></li>
<li><a href="https://www.ananzi.co.za/Copyright">Copyright</a></li>
<li><a href="https://www.ananzi.co.za/Privacy_Policy">Privacy Policy</a></li>
<li><a href="https://www.ananzi.co.za/About_us">About Us</a></li>
<li><a href="https://www.ananzi.co.za/contact">Contact Us</a></li>
<li><a href="https://www.ananzi.co.za/disclaimer">Disclaimer</a></li>
</ul>
<div class="footer-bottom">
<p class="">Ananzi (Pty) Ltd and its associates disclaim all liability for any loss, damage, injury or expense however caused, arising from the use of or reliance upon, in any manner, the information provided through this service and does not warrant the truth, accuracy or completeness of the information provided. </p>
<p class="footer-buttons">
<a href="https://www.brabys.com" rel="nofollow" target="_blank"><img border="0" height="47" src="/images/brabys_holdings_group.gif" width="120"/></a>
<a href="https://evine.co/?utm_source=ananzi&amp;utm_medium=cse&amp;utm_campaign=site_footer" rel="nofollow" target="_blank" title="Digital solutions provider"><img border="0" src="/images/evine_logo.png"/></a>
</p>
</div>
</div>
<div class="main-footer">
<div class="container">
<p>Copyright © 2000 - 2021 Ananzi (Pty) Ltd South Africa</p>
</div>
</div>
</footer>
<!-- JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://www.ananzi.co.za/inc/js" type="text/javascript"></script>
<script src="https://www.ananzi.co.za/js/jquery.cookie.js" type="text/javascript"></script>
<script src="https://www.ananzi.co.za//js/jquery.auto-complete.js"></script>
<script src="https://www.ananzi.co.za/js/main.js" type="text/javascript"></script>
<script src="https://www.ananzi.co.za/js/general.js?2017080401" type="text/javascript"></script>
<script type="text/javascript">

  $(function() {

    // setup_ac('ananzi-ads', 'ananzi-ads-q', 'https://www.ananzi.co.za/proxy/search_results_suggestions');

    var xhr;
    $("#ananzi-ads-q").autoComplete({
        source: function(term, response){
            try { xhr.abort(); } catch(e){}
            xhr = $.getJSON('https://www.ananzi.co.za/proxy/search_results_suggestions', { term: term }, function(data){ response(data); });
        },
        renderItem: function (item, search){
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            return '<div class="autocomplete-suggestion" data-url="'+item[1]+'"  data-val="'+search+'">'+item[0].replace(re, "<b>$1</b>") + '</div>';
        },
        onSelect: function(e, term, item){
            var url = item.data('url');

            $('#ananzi-ads-q').val(item.text());

            if (typeof url != 'undefined' && url != '') {
                top.location.href=url;
            }

        }        
    });

    $('#ananzi-ads-q').keypress(function(e) {
        if (e.keyCode == '13') {
            //e.preventDefault();
            $('#ananzi-ads').submit();
        }
    });

    $('#ananzi-ads-q').on('change', function() {


    });

    $('#ananzi-ads-q').focus();

    var submitted = false;

    $('#ananzi-ads-form').on('submit', function(e) {

      if (!submitted) {
        var q = $('#ananzi-ads-q').val();

        e.preventDefault();
        $('#btn-submit-ads').attr('disabled', 'disabled');

        $.get("https://www.ananzi.co.za/proxy/search_results_suggestions?term=" + q, function(result) {
          
          if (typeof result != 'object') {
            result = jQuery.parseJSON(result);
          }
          
          var url = result[0][1];

          submitted = true;
          // $('#ananzi-ads-form').submit();
          top.location.href=url;
        });


      }
    });
  });

  function toggleSearch(activateSearch) {

    $('.form-wrappers').hide();
    $('#' + activateSearch +'-form-wrapper').show();

    if (activateSearch == 'ananzi-sadirectory') {
      currentSearchVal = $('#ananzi-ads-q').val();
      $('#ananzi-sadirectory-q').val(currentSearchVal);
    } else {
      currentSearchVal = $('#ananzi-sadirectory-q').val();
      $('#ananzi-ads-q').val(currentSearchVal);
    }


  }

  function cookieLinkSave(url){
      return $.cookie('rub_trending_pages', url, { expires: 1 });
  }

</script>
</body>
</html>
