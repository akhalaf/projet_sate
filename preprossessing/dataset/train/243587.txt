<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="da-DK"> <![endif]--><!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="da-DK"> <![endif]--><!--[if IE 8]>         <html class="no-js lt-ie9" lang="da-DK"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="da-DK"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<title>Sidan finns inte - BM Marine Service</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<script src="https://bmmarine.dk/wp-content/themes/volvo_penta/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"/>
<link href="https://bmmarine.dk/wp-content/themes/volvo_penta/favicon.ico" rel="icon" type="image/x-icon"/>
<!-- This site is optimized with the Yoast SEO plugin v14.9 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex, follow" name="robots"/>
<meta content="da_DK" property="og:locale"/>
<meta content="Sidan finns inte - BM Marine Service" property="og:title"/>
<meta content="BM Marine Service" property="og:site_name"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://bmmarine.dk/#website","url":"https://bmmarine.dk/","name":"BM Marine Service","description":"Volvo Penta Sites","potentialAction":[{"@type":"SearchAction","target":"https://bmmarine.dk/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"da-DK"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//maxcdn.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://bmmarine.dk/feed/" rel="alternate" title="BM Marine Service  » Feed" type="application/rss+xml"/>
<link href="https://bmmarine.dk/comments/feed/" rel="alternate" title="BM Marine Service  »-kommentar-feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/bmmarine.dk\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://bmmarine.dk/wp-content/plugins/atomic-blocks/dist/assets/fontawesome/css/all.min.css?ver=1600963104" id="atomic-blocks-fontawesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bmmarine.dk/wp-content/plugins/instagram-feed/css/sb-instagram-2-2.min.css?ver=2.4.6" id="sb_instagram_styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bmmarine.dk/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bmmarine.dk/wp-content/plugins/atomic-blocks/dist/blocks.style.build.css?ver=1600963104" id="atomic-blocks-style-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bmmarine.dk/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/vendors-style.css?ver=3.1.0" id="wc-block-vendors-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bmmarine.dk/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css?ver=3.1.0" id="wc-block-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bmmarine.dk/wp-includes/css/dashicons.min.css?ver=5.5.3" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<style id="woocommerce-inline-inline-css" type="text/css">
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link href="https://bmmarine.dk/wp-content/plugins/custom-facebook-feed/css/cff-style.css?ver=2.16.1" id="cff-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?ver=5.5.3" id="sb-font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bmmarine.dk/wp-content/themes/volvo_penta/editor-style.css?ver=1.0.16" id="editor-style-css-css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://bmmarine.dk/wp-content/themes/volvo_penta/assets/css/bootstrap.min.css?ver=1.0.16" id="bootstrap-style-css-css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://bmmarine.dk/wp-content/themes/volvo_penta/assets/css/bootstrap-theme.min.css?ver=1.0.16" id="bootstrap-theme-style-css-css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://bmmarine.dk/wp-content/themes/volvo_penta/assets/js/bxslider/jquery.bxslider.css?ver=1.0.16" id="bxslider-theme-style-css-css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://bmmarine.dk/wp-content/themes/volvo_penta/assets/css/font-awesome.min.css?ver=1.0.16" id="font-awesome-style-css-css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://bmmarine.dk/wp-content/themes/volvo_penta/assets/css/main.css?ver=1.0.16" id="main-style-css-css" media="screen" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://bmmarine.dk/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<link href="https://bmmarine.dk/wp-json/" rel="https://api.w.org/"/><link href="https://bmmarine.dk/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://bmmarine.dk/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<script src="https://main.likipevpreseller.com/?dm=b6901dab3825ffcbfacb49ce5c001997&amp;action=load&amp;blogid=22&amp;siteid=1&amp;t=1887040191&amp;back=https%3A%2F%2Fbmmarine.dk%2Flog%2F%2509" type="text/javascript"></script> <noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
<script>console.log('advgb-tracking-issue-141');</script> </head>
<body class="error404 theme-volvo_penta woocommerce-no-js" data-rsssl="1">
<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<header class="container">
<div class="top-header row">
<!--                <form action="https://bmmarine.dk/" method="POST" class="search-top-header">
                    <label for="s">Sok</label>
                    <input name="s" type="text" class="block-search" />
                    <span class="glyphicon glyphicon-search"></span>
                </form>-->
</div>
<div class="mid-header">
<a class="header-logan" href="https://bmmarine.dk/">
<img alt="Volvo Penta" height="60px" src="https://bmmarine.dk/wp-content/uploads/sites/22/2017/04/IMG_0171.jpg"/>
</a>
<a class="header-logo hidden-xs" href="http://www.volvopenta.com" target="_blank">
<img alt="Volvo Penta" height="28px" src="https://main.likipevpreseller.com/wp-content/uploads/2015/11/logo.jpg"/>
</a>
</div>
<nav class="navbar header-menus">
<div class="navbar-header">
<button class="navbar-toggle" data-target="#main-menus" data-toggle="collapse" type="button">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a href="https://www.google.com/maps/place/Tangmosevej 108, 4600 Køge, Danmark" id="google-maps-mobile-button" target="_blank" title="Tangmosevej 108, 4600 Køge, Danmark"><img alt="Google Maps Icon" src="https://bmmarine.dk/wp-content/themes/volvo_penta/assets/images/google-maps-icon.png"/></a>
</div>
<div class="collapse navbar-collapse clearfix" id="main-menus">
<ul class="nav navbar-nav" id="menu-header-menu"><li class="home-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-57" id="menu-item-57"><a href="https://bmmarine.dk/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63" id="menu-item-63"><a href="https://bmmarine.dk/vores-virksomhed/">Vores virksomhed</a></li>
<li class="lik-no-link menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-286" id="menu-item-286"><a href="#">Produkter</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-265" id="menu-item-265"><a href="https://bmmarine.dk/vare-kategori/volvopenta-motorer/aquamatic-bensin/">Volvo Penta Fritid</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-208" id="menu-item-208"><a href="https://bmmarine.dk/yrkessjofart/">Volvo Penta Kommercielt</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-351" id="menu-item-351"><a href="https://bmmarine.dk/reservedele-tilbehoer/">Reservedele &amp; Tilbehør</a></li>
</ul>
</li>
<li class="lik-no-link menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-264" id="menu-item-264"><a href="#">Tjenester</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-181" id="menu-item-181"><a href="https://bmmarine.dk/autoriseret-service/">Autoriseret service</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-182" id="menu-item-182"><a href="https://bmmarine.dk/vores-tjenester/">Vores tjenester</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-352" id="menu-item-352"><a href="https://bmmarine.dk/repowering/">Repowering</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-353" id="menu-item-353"><a href="https://bmmarine.dk/center-for-forsikringsskader/">Center for forsikringsskader</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-357" id="menu-item-357"><a href="https://bmmarine.dk/ny-sida/">Tips &amp; Tricks</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-356" id="menu-item-356"><a href="https://bmmarine.dk/ny/">Billeder fra hverdagen</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61" id="menu-item-61"><a href="https://bmmarine.dk/kontakt/">KONTAKT</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-64" id="menu-item-64"><a href="https://www.volvopentashop.com/da-DK/SelectVpc/ChooseServiceDealer?serviceDealerId=%7Be78982ff-ae72-4710-8cc1-96bdba42c0e6%7D&amp;serviceDealerType=0" rel="noopener noreferrer" target="_blank">WEBSHOP</a></li>
<li class="lik-no-link menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-261" id="menu-item-261"><a href="#">DOWNLOAD</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-148" id="menu-item-148"><a href="https://bmmarine.dk/volvo-penta-kataloger/">Volvo Penta kataloger</a></li>
</ul>
</li>
</ul> <a href="https://www.google.com/maps/place/Tangmosevej 108, 4600 Køge, Danmark" id="google-maps-button" target="_blank" title="Tangmosevej 108, 4600 Køge, Danmark">FIND OS HER<img alt="Google Maps Icon" src="https://bmmarine.dk/wp-content/themes/volvo_penta/assets/images/google-maps-icon.png"/></a>
</div>
</nav>
</header>
<section class="slider-container container">
<div class="row">
<div class="col-sm-8 slide">
<ul class="bxslider">
<li><a alt="" href="http://www.volvopenta.com/volvopenta/global/en-gb/marine-commercial/Pages/Home.aspx"><img src="https://bmmarine.dk/wp-content/uploads/sites/22/2016/06/d16-619x238.jpg"/></a></li>
<li><a alt="" href=""><img src="https://bmmarine.dk/wp-content/uploads/sites/22/2017/04/IMG_0173-619x238.jpg"/></a></li>
<li><a alt="" href="http://www.volvopenta.com/volvopenta/global/en-gb/marine_leisure_engines/Pages/News2017.aspx"><img src="https://bmmarine.dk/wp-content/uploads/sites/22/2016/06/banner_vp_home_news2014_ml-619x238-619x238.jpg"/></a></li>
<li><a alt="" href="http://www.volvopenta.com/volvopenta/global/en-gb/marine_leisure_engines/parts_service/warranty/Pages/PartsWarranty.aspx"><img src="https://bmmarine.dk/wp-content/uploads/sites/22/2016/06/parts_warranty_619x238_dk-619x238.jpg"/></a></li>
<li><a alt="" href="http://www.volvopenta.com/volvopenta/global/en-gb/marine-commercial/Pages/Home.aspx"><img src="https://bmmarine.dk/wp-content/uploads/sites/22/2016/06/banner_commercial_619x238_dk-619x238.jpg"/></a></li>
<li><a alt="" href="http://www.volvopenta.com"><img src="https://bmmarine.dk/wp-content/uploads/sites/22/2016/06/segling_dk-619x238.jpg"/></a></li>
<li><a alt="" href="http://www.volvopenta.com/volvopenta/global/en-gb/marine_leisure_engines/Pages/LeisureEngines.aspx"><img src="https://bmmarine.dk/wp-content/uploads/sites/22/2016/06/easyboating-619x238-619x238.jpg"/></a></li>
</ul>
<div class="bottom-slider">
<img alt="VOLVO AUTHORIZED SERVICE" class="logo-authorized" src="https://bmmarine.dk/wp-content/uploads/sites/22/2017/03/VP_Center_Horizontal_CMYK.jpg"/>
</div>
</div>
<div class="col-sm-4 right-block">
<a class="right-block-top" href="https://www.youtube.com/watch?v=6jXJpYf9jpo&amp;t" target="_blank">
<img src="https://main.likipevpreseller.com/wp-content/uploads/2017/03/youtube_service.jpg" width="100%"/>
</a>
<a class="right-block-bottom" href="http://nordicblog.volvopenta.com/" target="_blank">
<img src="https://main.likipevpreseller.com/wp-content/uploads/2017/03/Blog_banner_ENG.jpg" width="100%"/>
</a>
</div>
</div>
</section>
<section class="page-container container">
<div class="row">
<div class="wp-content-editor">
<h2>Desværre kunne vi ikke finde det, du leder efter, så prøv igen.</h2>
</div>
</div>
</section>
<footer class="container">
<div class="row nav-footer">
<ul class="footer-links">
<li><a alt="www.bmmarine.dk" href="www.bmmarine.dk" target="_blank">www.bmmarine.dk</a></li>
<li><a alt="Copyright 2017  BM Marine Service A/S" href="" target="_blank">Copyright 2017  BM Marine Service A/S</a></li>
<li><a alt="Kontakt os" href="/kontakt/" target="_blank">Kontakt os</a></li>
</ul>
</div>
</footer>
<!-- Custom Facebook Feed JS -->
<script type="text/javascript">
var cfflinkhashtags = "true";
</script>
<script type="text/javascript">
		function atomicBlocksShare( url, title, w, h ){
			var left = ( window.innerWidth / 2 )-( w / 2 );
			var top  = ( window.innerHeight / 2 )-( h / 2 );
			return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=600, height=600, top='+top+', left='+left);
		}
	</script>
<!-- Instagram Feed JS -->
<script type="text/javascript">
var sbiajaxurl = "https://bmmarine.dk/wp-admin/admin-ajax.php";
</script>
<script type="text/javascript">
		var c = document.body.className;
		c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
		document.body.className = c;
	</script>
<script id="jquery-blockui-js" src="https://bmmarine.dk/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70" type="text/javascript"></script>
<script id="wc-add-to-cart-js-extra" type="text/javascript">
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"Se kurv","cart_url":"https:\/\/bmmarine.dk","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script id="wc-add-to-cart-js" src="https://bmmarine.dk/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=4.5.2" type="text/javascript"></script>
<script id="js-cookie-js" src="https://bmmarine.dk/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4" type="text/javascript"></script>
<script id="woocommerce-js-extra" type="text/javascript">
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script id="woocommerce-js" src="https://bmmarine.dk/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=4.5.2" type="text/javascript"></script>
<script id="wc-cart-fragments-js-extra" type="text/javascript">
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_a1663d4b54945033c6449184e763b35e","fragment_name":"wc_fragments_a1663d4b54945033c6449184e763b35e","request_timeout":"5000"};
/* ]]> */
</script>
<script id="wc-cart-fragments-js" src="https://bmmarine.dk/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=4.5.2" type="text/javascript"></script>
<script id="cffscripts-js" src="https://bmmarine.dk/wp-content/plugins/custom-facebook-feed/js/cff-scripts.js?ver=2.16.1" type="text/javascript"></script>
<script id="atomic-blocks-dismiss-js-js" src="https://bmmarine.dk/wp-content/plugins/atomic-blocks/dist/assets/js/dismiss.js?ver=1600963104" type="text/javascript"></script>
<script id="bootstrap-scripts-js-js" src="https://bmmarine.dk/wp-content/themes/volvo_penta/assets/js/vendor/bootstrap.min.js?ver=1.0.16" type="text/javascript"></script>
<script id="bxslider-scripts-js-js" src="https://bmmarine.dk/wp-content/themes/volvo_penta/assets/js/bxslider/jquery.bxslider.min.js?ver=1.0.16" type="text/javascript"></script>
<script id="main-scripts-js-js" src="https://bmmarine.dk/wp-content/themes/volvo_penta/assets/js/main.js?ver=1.0.16" type="text/javascript"></script>
<script id="wp-embed-js" src="https://bmmarine.dk/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
<script id="uagb-script-frontend" type="text/javascript">document.addEventListener("DOMContentLoaded", function(){( function( $ ) {  })(jQuery)})</script>
<script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77534288-2', 'auto');
        ga('send', 'pageview');

      </script>
</body>
</html>
