<!DOCTYPE html>
<html lang="fr-FR">
<head>
<meta charset="utf-8"/>
<title>Page non trouvée | Association Jeanne d'Arc de Chartres</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://ajachartres.fr/wp-content/themes/twentyten/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ajachartres.fr/xmlrpc.php" rel="pingback"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://ajachartres.fr/feed/" rel="alternate" title="Association Jeanne d'Arc de Chartres » Flux" type="application/rss+xml"/>
<link href="https://ajachartres.fr/comments/feed/" rel="alternate" title="Association Jeanne d'Arc de Chartres » Flux des commentaires" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/ajachartres.fr\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.16"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://ajachartres.fr/wp-json/" rel="https://api.w.org/"/>
<link href="https://ajachartres.fr/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://ajachartres.fr/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.16" name="generator"/>
<script src="http://ajachartres.fr/wp-content/plugins/show125/js/mootoolsp.js" type="text/javascript"></script><script src="http://ajachartres.fr/wp-content/plugins/show125/js/blackbox.js" type="text/javascript"></script>
<style>
	#myform label, #myform input, #myform textarea, #myform select {display: block;float: left;margin-bottom: 5px;}
	#myform input,#myform textarea {border: 1px solid black;padding:1px;width:220px}
	#myform input[type=checkbox] {width:10px;}
	#myform select {padding: 0px;}
	#myform label {text-align: left;width: 130px;padding-right: 20px;font-size: 0.9em;font-weight: bold;}
	#myform br {clear: left;}
	#myform .dno{display: none;}
	</style>
<script src="https://ajachartres.fr/wp-content/plugins/simple-sidebar-navigation/suckerfish_ie.js" type="text/javascript"></script><style id="custom-background-css" type="text/css">
body.custom-background { background-color: #ede8e6; }
</style>
<link href="https://ajachartres.fr/wp-content/uploads/2018/09/cropped-Sans-titre6-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://ajachartres.fr/wp-content/uploads/2018/09/cropped-Sans-titre6-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://ajachartres.fr/wp-content/uploads/2018/09/cropped-Sans-titre6-180x180.png" rel="apple-touch-icon-precomposed"/>
<meta content="https://ajachartres.fr/wp-content/uploads/2018/09/cropped-Sans-titre6-270x270.png" name="msapplication-TileImage"/>
</head>
<body class="error404 custom-background">
<div class="hfeed" id="wrapper">
<div id="header">
<div id="masthead">
<div id="branding" role="banner">
<div id="site-title">
<span>
<a href="https://ajachartres.fr/" rel="home" title="Association Jeanne d'Arc de Chartres">Association Jeanne d'Arc de Chartres</a>
</span>
</div>
<div id="site-description">AJA Chartres</div>
<img alt="" height="198" src="https://ajachartres.fr/wp-content/uploads/2020/10/cropped-site-333.jpg" width="940"/>
</div><!-- #branding -->
<div id="access" role="navigation">
<div class="skip-link screen-reader-text"><a href="#content" title="Skip to content">Skip to content</a></div>
<div class="menu"><ul>
<li><a href="https://ajachartres.fr/">Accueil</a></li><li class="page_item page-item-91"><a href="https://ajachartres.fr/inscription/">Inscription</a></li>
<li class="page_item page-item-585 page_item_has_children"><a href="https://ajachartres.fr/autres-activites/">Activités</a>
<ul class="children">
<li class="page_item page-item-450"><a href="https://ajachartres.fr/autres-activites/atelier-broderie/">Broderie</a></li>
<li class="page_item page-item-10"><a href="https://ajachartres.fr/autres-activites/chorale/">Chorale</a></li>
<li class="page_item page-item-18"><a href="https://ajachartres.fr/autres-activites/dessin-peinture/">Dessin Peinture</a></li>
<li class="page_item page-item-12"><a href="https://ajachartres.fr/autres-activites/gym/">Pilates</a></li>
<li class="page_item page-item-437"><a href="https://ajachartres.fr/autres-activites/qi-gong/">Qi Gong</a></li>
<li class="page_item page-item-14"><a href="https://ajachartres.fr/autres-activites/sophrologie/">Sophrologie</a></li>
<li class="page_item page-item-1429"><a href="https://ajachartres.fr/autres-activites/stretching-postural/">Stretching Postural</a></li>
<li class="page_item page-item-16"><a href="https://ajachartres.fr/autres-activites/yoga/">Yoga</a></li>
<li class="page_item page-item-2715"><a href="https://ajachartres.fr/autres-activites/kaekkyon/">Taekkyon – Santé</a></li>
<li class="page_item page-item-2611"><a href="https://ajachartres.fr/autres-activites/yoga-du-rire-2/">Yoga du Rire</a></li>
<li class="page_item page-item-2617"><a href="https://ajachartres.fr/autres-activites/carnet-de-voyage-dessin/">Carnet de voyage – Dessin</a></li>
</ul>
</li>
<li class="page_item page-item-6"><a href="https://ajachartres.fr/basket/">Basket</a></li>
<li class="page_item page-item-24"><a href="https://ajachartres.fr/accueil/comite-directeur/">Comité Directeur</a></li>
</ul></div>
</div><!-- #access -->
</div><!-- #masthead -->
</div><!-- #header -->
<div id="main">
<div id="container">
<div id="content" role="main">
<div class="post error404 not-found" id="post-0">
<h1 class="entry-title">Not Found</h1>
<div class="entry-content">
<p>Apologies, but the page you requested could not be found. Perhaps searching will help.</p>
<form action="https://ajachartres.fr/" class="searchform" id="searchform" method="get" role="search">
<div>
<label class="screen-reader-text" for="s">Rechercher :</label>
<input id="s" name="s" type="text" value=""/>
<input id="searchsubmit" type="submit" value="Rechercher"/>
</div>
</form> </div><!-- .entry-content -->
</div><!-- #post-0 -->
</div><!-- #content -->
</div><!-- #container -->
<script type="text/javascript">
		// focus on search field after it has loaded
		document.getElementById('s') && document.getElementById('s').focus();
	</script>
</div><!-- #main -->
<div id="footer" role="contentinfo">
<div id="colophon">
<div id="site-info">
<a href="https://ajachartres.fr/" rel="home" title="Association Jeanne d'Arc de Chartres">
					Association Jeanne d'Arc de Chartres				</a>
</div><!-- #site-info -->
<div id="site-generator">
<a href="http://wordpress.org/" rel="generator" title="Semantic Personal Publishing Platform">
					Proudly powered by WordPress.				</a>
</div><!-- #site-generator -->
</div><!-- #colophon -->
</div><!-- #footer -->
</div><!-- #wrapper -->
<script src="https://ajachartres.fr/wp-includes/js/wp-embed.min.js?ver=4.9.16" type="text/javascript"></script>
</body>
</html>
