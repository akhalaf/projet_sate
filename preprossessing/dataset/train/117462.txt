<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "https://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="no-cache, no-store, must-revalidate" http-equiv="Cache-Control"/>
<meta content="no-cache" http-equiv="Pragma"/>
<meta content="0" http-equiv="Expires"/>
<title></title>
</head>
<body>
<script type="text/javascript">
	var milliseconds = new Date().getTime();
	if (window.location.hostname == "itportal.tccglobal.com")
		window.location.href = "/atpc/b2b/j/home";
	else if (window.location.hostname == "www.amicidiscuola.com" || window.location.hostname == "amicidiscuola.com")
	{
		window.location.href = "/atpc/amicidiscuola/j/home"+document.location.search;
	}
	else
		window.location.href = "/index2.html?ts=" + milliseconds;
    </script>
</body>
</html>