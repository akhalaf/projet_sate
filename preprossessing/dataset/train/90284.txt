<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>AJES - Faculdades do Vale do Juruena</title>
<meta content="musk" name="keywords"/>
<meta content="" name="description"/>
<meta content="MUSKITTO" name="Author"/>
<meta content="jXNkH52Pi-uf-pmbS-bvqxXd8gkDHTXx2ctsNF3NJCA" name="google-site-verification"/>
<meta content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" name="viewport"/>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="css/settings.css" rel="stylesheet" type="text/css"/>
<link href="css/estilos.css" rel="stylesheet" type="text/css"/>
</head>
<body class="smoothscroll enable-animation">
<div class="barra">
<div class="logo"></div><!--logo-->
<div class="unidade abrirSus">
<a href="#"><img border="0" src="img/un-juina.png"/></a>
<div class="conteudoAbrir">
<ul class="menuSus">
<li><a href="http://juina.ajes.edu.br/">FACULDADE DO VALE DO JURUENA</a></li>
<li><a href="http://noroeste.ajes.edu.br/">FACULDADE DO NOROESTE DO MATO GROSSO</a></li>
</ul><!--conteudoMenu-->
</div>
</div><!--unidade-->
<div class="unidade"><a href="http://guaranta.ajes.edu.br/"><img src="img/un-gta.png"/></a></div><!--unidade-->
<div class="unidade"><a href="http://juara.ajes.edu.br/"><img src="img/un-juara.png"/></a></div><!--unidade-->
<div class="unidade"><a href="http://ead.ajes.edu.br"><img src="img/ead_ajes.png"/></a></div><!--unidade-->
<div class="outros"><a href="http://pos.ajes.edu.br/"><img src="img/un-poss.png"/></a></div><!--outros-->
<div class="outros"><a href="http://online.universo.edu.br/polos-de-ensino/juina-mt/"><img src="img/ead_universo.png"/></a></div><!--outros-->
<div class="igc"><a href="http://webajes.com.br/igc/instituicoes-do-estado-do-mato-grosso/"><img src="img/igc-qr.jpg"/></a></div><!--igc-->
</div><!--barra-->
<section class="fullwidthbanner-container roundedcorners" id="slider">
<div class="fullscreenbanner" data-navigationstyle="preview4">
<ul class="hide">
<li data-masterspeed="1000" data-saveperformance="off" data-slotamount="1" data-thumb="img/1.jpg" data-title="AJES 02" data-transition="random">
<img alt="" data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat" data-lazyload="img/1.jpg" src="img/1.jpg"/>
<div class="overlay dark-4"></div>
</li>
<li data-masterspeed="1000" data-saveperformance="off" data-slotamount="1" data-thumb="img/2.jpg" data-title="AJES 01" data-transition="random">
<img alt="" data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat" data-lazyload="img/2.jpg" src="img/2.jpg"/>
<div class="overlay dark-4"></div>
</li>
<li data-masterspeed="1000" data-saveperformance="off" data-slotamount="1" data-thumb="img/3.jpg" data-title="AJES 02" data-transition="random">
<img alt="" data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat" data-lazyload="img/3.jpg" src="img/3.jpg"/>
<div class="overlay dark-4"></div>
</li>
</ul>
</div>
</section>
<!-- /REVOLUTION SLIDER -->
<!-- PRELOADER -->
<div id="preloader">
<div class="inner">
<span class="loader"></span>
</div>
</div>
<!-- /PRELOADER -->
<!-- JAVASCRIPT FILES -->
<script src="js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="js/scripts.js" type="text/javascript"></script>
<!-- REVOLUTION SLIDER -->
<script src="js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="js/demo.revolution_slider.js" type="text/javascript"></script>
</body>
</html>