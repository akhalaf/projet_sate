<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html itemscope="" itemtype="https://schema.org/WebPage" lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="chrome=1" http-equiv="X-UA-Compatible"/>
<link href="http://abacusmortgagetraining.com/" itemprop="url" rel="canonical"/>
<title itemprop="name headline">
		Abacus Mortgage Training and Education - 		</title>
<meta content="" itemprop="description" name="Description"/>
<meta content="" name="Keywords"/>
<meta content="Abacus Mortgage Training and Education" name="author"/>
<meta content="copyright 2021 abacusmortgagetraining.com" name="copyright"/>
<link href="//static.blazonco.com/templates/racheal/abacus/css/style.css" id="CustomCSS" rel="stylesheet" type="text/css"/>
<link href="https://static.blazonco.com/stylesheets/flexslider/flexslider.css" id="flexslider-style" rel="stylesheet" type="text/css"/>
<link href="https://static.blazonco.com/templates/custom/buildingblocks/bbstarter6/style/common.css" id="stylesheet-common" rel="stylesheet" type="text/css"/>
<link href="/custom-branding.css" rel="stylesheet" type="text/css"/>
<link href="/files/favicon.ico" rel="shortcut icon"/>
<link href="/icon.png" rel="icon" type="image/png"/>
<link href="https://static.blazonco.com/templates/custom/buildingblocks/bbstarter6/style/text.css" id="stylesheet-text" rel="stylesheet" type="text/css"/>
<link href="https://static.blazonco.com/templates/custom/buildingblocks/bbstarter6/style/color1.css" id="stylesheet-variant" rel="stylesheet" type="text/css"/>
<script src="https://static.blazonco.com/scripts/yui/2.8.0/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
<script src="https://static.blazonco.com/scripts/yui/2.8.0/selector/selector-min.js" type="text/javascript"></script>
<script src="https://static.blazonco.com/scripts/yui/2.8.0/json/json-min.js" type="text/javascript"></script>
<script src="https://type-backup.blazonco.com/scripts/typostreamnd.js" type="text/javascript"></script>
<script type="text/javascript">
			Typostream.account = 'abacusmortgage';
			Typostream.sheet = 'default';
		</script>
<script src="https://static.blazonco.com/templates/custom/buildingblocks/bbstarter6/style/type.js" type="text/javascript"></script>
<!--[if IE]>
		<script type="text/javascript">Typostream.ie = true;</script>
		<![endif]-->
<!--[if IE 7]>
		<script type="text/javascript">Typostream.ie7 = true;</script>
		<![endif]-->
<!--[if lt IE 7]> 
		<script type="text/javascript">Typostream.ie6 = true;</script>
		<![endif]-->
<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="https://static.blazonco.com/templates/custom/buildingblocks/bbstarter6/style/ie7.css" />
		<![endif]-->
<!--[if lte IE 8]>
				<?import namespace="v" implementation="#default#VML" ?>		<style type="text/css">
			v\:shape { behavior: url(#default#VML); }
			v\:fill { behavior: url(#default#VML); }
			v\:stroke { behavior: url(#default#VML); }
		</style>
		<![endif]-->
<!--[if lt IE 7]>
		<script type="text/javascript" src="https://static.blazonco.com/scripts/ie6/ie6.js"></script>
		<link rel="stylesheet" type="text/css" href="https://static.blazonco.com/templates/custom/buildingblocks/bbstarter6/style/ie6.css" />
		<style type="text/css">
			body { behavior: url(/csshover2.htc); }
			
		</style>
		<script type="text/javascript" src="https://static.blazonco.com/scripts/ie6/DD_belatedPNG_0.0.2a.js"></script>
		<![endif]-->
<script src="https://static.blazonco.com/scripts/responsive/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="https://static.blazonco.com/scripts/responsive/flexslider/jquery.flexslider.js" type="text/javascript"></script>
<script type="text/javascript">			
                                $(window).load(function() {
                                    $('#FlexSliderModule2 .flexslider').flexslider({
                                        animation: 'slide',
                                        slideDirection: 'horizontal',
                                        slideshowSpeed: '7000',
                                        animationDuration: '600',
                                        controlsContainer: '#FlexSliderModule2',
					pausePlay: false
                                    });
                                });
                        </script> <link href="http://abacusmortgagetraining.com/news/feed" rel="alternate" title="Abacus Mortgage Training and Education RSS Feed" type="application/rss+xml"/><script type="text/javascript"> 
$(document).ready(function() { 
if($("body .flexslider ul.slides").length > 0) {
	$("body .flexslider ul.slides li img[src='/images/slides/slide1.jpg']").wrap("<a href='/continuing-education'/>");
        $("body .flexslider ul.slides li img[src='/images/slides/slide2.jpg']").wrap("<a href='/test-prep'/>");
        $("body .flexslider ul.slides li img[src='/images/slides/slide3.jpg']").wrap("<a href='/prelicensing'/>");
}
});
</script>
<style type="text/css">

html .ac-container input:checked ~ article.ac-large {
    overflow-y:scroll;
}

</style><!-- meta tag needed for mobile devices -->
<meta content="width=device-width, initial-scale=1.0 maximum-scale=1" name="viewport"/>
<!-- responsive stylesheet pulled from the template folder -->
<link href="https://static.blazonco.com/templates/custom/buildingblocks/bbstarter6/style/responsive.css" id="stylesheet-responsive" rel="stylesheet" type="text/css"/>
<!-- tiny nav js -->
<script src="https://static.blazonco.com/scripts/responsive/tinynav.js"></script>
<script>$(function () { $(".primarynav").tinyNav(); }); </script>
</head>
<body class="home"> <div class="" id="Main">
<div class="void void-main">
<div id="Header" itemscope="" itemtype="https://schema.org/WPHeader">
<div class="void void-header">
<div class="extra" id="Content-Header">
<div class="component module text-module first of-three" id="Module7">
<div class="void">
<h3 class="title empty"><span></span></h3>
<div class="content">
<p style="text-align: right;"><a href="https://www.mortgage-education.com/ContentPage.aspx?PageID=495&amp;RefID=abacus" target="_blank"><img alt="" class="null" src="/images/2019LogInBox.JPG"/></a></p> </div>
</div>
</div><div class="component text-block-component second of-three" id="TextBlockComponent1">
<div class="content">
</div>
</div>
<div class="component text-block-component third of-three" id="TextBlockComponent2">
<div class="content">
<p><a href="https://www.mortgage-education.com/?refid=abacus" target="_blank"><img alt="" class="null" src="/images/MEPartnerfinalCardmedium.jpg" style="float: right;"/></a>  </p>
<p><a href="https://www.mortgage-education.com/?refid=abacus" target="_blank"><img alt="" class="null" src="/images/AbacusPhonenumber.jpg" style="float: right;"/></a></p> </div>
</div>
</div>
<h1 itemprop="name"><a href="/"><span><span class="word-0">Abacus</span> <span class="word-1">Mortgage</span> <span class="word-2">Training</span> <span class="word-3">and</span> <span class="word-4 last-word">Education</span></span></a></h1>
<h2 itemprop="headline"><a href="/"><span><span class="word-0 last-word"></span></span></a></h2>
<div id="PrimaryNavigation">
<div class="void void-primarynav">
<ul class="primarynav" itemscope="" itemtype="https://schema.org/SiteNavigationElement">
<li class="active first of-nine items-0" itemprop="name"><a href="/" itemprop="url"><span>Home</span></a>
</li>
<li class="second of-nine items-0" itemprop="name"><a href="/get-your-license" itemprop="url"><span>Get Your License</span></a>
</li>
<li class="third of-nine items-0" itemprop="name"><a href="/renew-your-license" itemprop="url"><span>Renew Your License</span></a>
</li>
<li class="fourth of-nine items-0" itemprop="name"><a href="/test-prep" itemprop="url"><span>Test Prep</span></a>
</li>
<li class="fifth of-nine items-0" itemprop="name"><a href="/compliance" itemprop="url"><span>Compliance</span></a>
</li>
<li class="sixth of-nine items-0" itemprop="name"><a href="/contact-us" itemprop="url"><span>Contact Us</span></a>
</li>
<li class="seventh of-nine items-0" itemprop="name"><a href="/about-us" itemprop="url"><span>About Us</span></a>
</li>
<li class="eighth of-nine items-0" itemprop="name"><a href="/testimonials" itemprop="url"><span>Testimonials</span></a>
</li>
<li class="end ninth of-nine items-0" itemprop="name"><a href="/careers" itemprop="url"><span>Careers </span></a>
</li>
</ul>
</div>
</div>
</div>
</div>
<div id="Body">
<div id="StockImage">
<div class="component-group module-row items-2 first of-" id="ModuleRow1">
<div class="component module flex-slider-module first of-two" id="FlexSliderModule2">
<div class="void">
<h3><span></span></h3>
<div class="flexslider">
<ul class="slides">
<li>
<img src="/images/slides/MESlide1.jpg"/>
</li></ul>
</div>
</div>
</div><div class="component module text-module second of-two" id="Module1">
<div class="void">
<h3 class="title"><span>Module Title</span></h3>
<div class="content">
<h2>Welcome to the brand new Abacus website!<span style="font-size: 10px;"> </span></h2>
<hr/>
<h6><span style="color: #c0c0c0;">Abacus Mortgage Training has been helping the mortgage industry since 1999 and actually started as a Mortgage Company in the late 1980âs.</span></h6>
<p> <span style="color: #c0c0c0; font-size: 0.75em;">At Abacus today, we are still committed to the same standard of excellence the company was built on. As such, we seek out only the best NMLS approved providers, that offer top quality content, at a specially negotiated prices. This is our commitment to you. That we will always find you the best courses, from the best providers, at the best price. That is why we are so proud to announce that we have partnered with Mortgage Education (Provider ID:  1400051) to provide you with top quality training.</span></p> </div>
</div>
</div> <div class="catchall"></div>
</div> </div>
<div class="" id="Content">
<div class="void void-content">
<div id="Outer-Left">
<div id="Content-Left" itemscope="" itemtype="https://schema.org/WPSideBar">
</div>
</div>
<div id="Outer-Text">
<div id="Content-Text" itemprop="mainContentOfPage" itemscope="" itemtype="https://schema.org/WebPageElement">
<div class="component-group module-row items-4 first of-" id="ModuleRow3">
<div class="component module text-module first of-four" id="Module5">
<div class="void">
<h3 class="title"><span>Get Your License</span></h3>
<div class="content">
<p style="text-align: center;"><a href="/get-your-license" target="_self"><img alt="" class="null" src="/images/arrowupbusinessman.jpg"/></a></p> </div>
</div>
</div><div class="component module text-module second of-four" id="Module2">
<div class="void">
<h3 class="title"><span>Renew Your License</span></h3>
<div class="content">
<p style="text-align: center;"><a href="/renew-your-license" target="_self"><img alt="" class="null" src="/images/ladywithlaptop.jpg"/></a></p> </div>
</div>
</div><div class="component module text-module third of-four" id="Module3">
<div class="void">
<h3 class="title"><span>Prepare for the Test</span></h3>
<div class="content">
<p style="text-align: center;"><a href="/test-prep" target="_self"><img alt="" class="null" src="/images/module3.jpg"/></a></p> </div>
</div>
</div><div class="component module text-module fourth of-four" id="Module4">
<div class="void">
<h3 class="title"><span>Compliance FHA Processing</span></h3>
<div class="content">
<p style="text-align: center;"><a href="/compliance" target="_self"><img alt="" class="null" src="/images/module4.jpg"/></a></p> </div>
</div>
</div> <div class="catchall"></div>
</div> </div>
</div>
<div id="Outer-Right">
<div id="Content-Right" itemscope="" itemtype="https://schema.org/WPSideBar">
</div>
</div>
<div class="catchall"></div>
</div>
</div>
</div>
<div id="Footer">
<div class="void void-footer" itemscope="" itemtype="https://schema.org/WPFooter">
<div class="extra" id="Content-Footer">
<div class="component-group module-row items-1 first of-" id="ModuleRow2">
<div class="component module text-module first of-one" id="Module6">
<div class="void">
<h3 class="title"><span>Module Title</span></h3>
<div class="content">
<h5><span style="color: #000000;">Abacus Mortgage Training and Education specializes in helping you find the mortgage education that fits your learning style.  </span></h5>
<h5><span><span style="color: #000000;">We work directly with NMLS education providers and can help you to find the best courses from the best providers in the mortgage industry and make sure you get your training at the lowest price possible</span>.  </span> </h5>
<pre><strong style="font-size: 1.5em;">An Alliance to Best Help the Mortgage Training Industry</strong></pre>
<h5><span><br/></span></h5>
<h5><span>The economy has gone through many changes in the last 20 years; in the last 5 years, the mortgage industry even more so. New rules and regulations that are in place have fundamentally transformed how loan officers get and maintain licensure, how mortgage applications are taken, how loans are closed and so much more. </span></h5>
<h5><span><br/> </span><span>Abacus Mortgage Training and Education actually started out as MoneyNet Mortgage Planning Services in the 80s and continued to originate mortgage loans into the late 2000s. Founder Paul Donohue's main focus and philosophy was building "customers for life" through honesty, integrity, and professionalism; helping make the dream of homeownership possible for tens of thousands of families. During this time, Paul and his team at Abacus were also committed to delivering ground breaking mortgage training and education across the country to tens of thousands of individuals and companies large and small, as an NMLS approved education provider. Francis Donohue has been the National Sales Director at Abacus for over 7 years and has continued to help mortgage companies and loan originators nationwide reach their goal of mortgage excellence. <br/> <br/> In the last 3 years Abacus has evolved as well. We continue to evolve how we help train the industry and Francis Donohue is now the new President of Abacus and is ready to help you navigate through your mortgage training goals and requirements. Francis has an extremely clear and unique hands-on upbeat approach to helping you understand the entire licensing process, preparing for the exams, and or getting set up with a class to renew your individual or entire company license.</span></h5>
<p><span><br/></span></p>
<h5><span>When you call or email, one of highly knowledgeable Abacus's team members will help you get started fast.. or Francis might just answer the phone, he may not always be available to speak immediately; however, he loves to provide personalized solutions for each person he talks to and would be happy to spend that time speaking with you. </span></h5>
<p><span><br/></span></p>
<h5><span style="font-size: 1em;">Abacus Mortgage Training and Education specializes in helping to find the mortgage education that is right for you.  While we are not an NMLS approved course provider and we donât have courses approved by NMLS we can help you to find course providers and courses that are. </span></h5>
<p><span style="font-size: 1em;"><br/></span></p>
<h5><span>We have also recently aligned with NMLS approved education provider Mortgage Education.com (nmls id 1400051) to help bring you the very best training possible and at an extremely affordable price.</span></h5>
<p><span><br/></span></p>
<h5><span>Mortgage Education was also founded on the same principles of honor and excellence and hence why we are a natural fit to assist you together in your education experience. </span></h5>
<h5><span>All NMLS approved PreLicensing Courses are fully licensed and approved by Mortgage Education (nmls id 1400051)</span></h5>
<p> </p>
<table bgcolor="white" border="1" bordercolor="#E9E9E9" cellpadding="9" cellspacing="0" style="width: 960px;">
<tbody>
<tr>
<td valign="top">
<p><strong><br/></strong></p>
<p><strong><br/></strong></p>
<p><strong>License to Originate</strong></p>
<p><span style="color: #000000;">A little mortgage food for thought, especially if you are a first time <strong>visitor </strong>to Abacus Mortgage Training and <strong>you intend</strong> to get your mortgage license.</span></p>
<p> </p>
<p><span style="color: #000000;">NMLS Test First-Time-Pass Success Requires Both Mortgage Broker Education and Loan Officer Training</span></p>
<p> </p>
<p><span style="color: #000000;">It is critical that the NMLS Test <strong>and the licensing process</strong> does not stand in the way of your staffing, if you are a company, or <strong>limit your employment</strong> <strong>opportunities</strong> if <strong>you</strong> are an individual. <strong>Passing</strong> the NMLS Test on the first try <strong>requires both mortgage training and education.</strong></span></p>
<p> </p>
<p><span style="color: #000000;">Why do we insist you need both "education' and "training?"</span></p>
<p><span style="color: #000000;"><strong>Because you must pass the NMLS Test to get licensed; and, you must be able to apply your knowledge to originate mortgage loans successfully.</strong></span></p>
<p><span style="color: #000000;"><strong> âLoan Officer Trainingâ is the mortgage industry catch phrase used to mean acquiring the fundamentals of origination mortgage loans. This includes understanding proper disclosure, protecting borrowerâs rights, mastery of the 1003 form, mortgage math and understanding issues of credit. Your mastery of this specific body of mortgage fundamentals are the basis for todayâs Mortgage Loan Originator (MLO). Loan officer training deals with a significant percentage of the content addressed on the NMLS Test required by state and federal law as a prerequisite for obtaining your MLO license.  In summary, this body of content is far more fundamental and ground-level than the material covered by âMortgage Broker Educationâ.</strong></span></p>
<p><span style="color: #000000;"><strong>âMortgage Broker Educationâ is the term used to mean acquiring the mandatory education necessary to meet all state and federal requirements for Pre-licensure Education (PE) and Continuing Education (CE) in order to obtain and maintain your MLO license.  While ongoing Loan Officer Training is vital for the new and experienced MLO, passing the NMLS Test requires a comprehensive mastery of mortgage law, the primary and secondary mortgage markets, mortgage processing, loan file underwriting and full understanding of mortgage ethics. The SAFE Act requires individual licensure of Mortgage Brokers, Mortgage Lenders and their loan officers.  While Continuing Education is required for licensure renewal, as long as the MLO properly maintains their Loan Officerâs License the MLO will not have to repeat the NMLS Test. </strong></span></p>
<p><span style="color: #000000;"><strong>We consider both Loan Officer Training and Mortgage Education imperative. You need to meet your basic requirements for licensure which includes sitting for your Pre-license Education, master the test-prep tools and pass the NMLS Test. You also need the fundamental mortgage training to succeed as a mortgage originator, and this requires you become a student of the mortgage business. The national SAFE Act Test will require you to focus on your âmortgage Educationâ. Success as a Mortgage Loan Officer will require an ongoing commitment to âLoan Officer Trainingâ.</strong></span></p>
<p><span style="color: #000000;"><strong>The SAFE Act Test is designed to find any gaps in your knowledge of the mortgage industry and ensure that you are competent to sit across from a borrower with reasonable skill and care.</strong></span></p>
<p> </p>
<p><span style="color: #000000;">You can probably see our punch line coming but with so much at stake we don't consider repetition obnoxious: in no uncertain terms you must obtain mastery over both Mortgage Broker Education and Loan Officer Training for success on the NMLS Test.</span></p>
<p><span style="color: #000000;"><br/></span></p>
<span style="font-family: arial; font-size: x-small;">Â© Copyright 2015 Abacus Mortgage Training &amp; Education. All rights reserved. Printed in the United States of America. No part of this website, its content, text, or graphics may be used or reproduced in any manner whatever without the express written consent of Abacus. <br/><br/>All information about the <a href="http://www.abacusmortgagetraining.com/">NMLS Test,</a> <a href="http://www.abacusmortgagetraining.com/">mortgage broker education</a>, <a href="http://www.abacusmortgagetraining.com/">loan officer training</a> and <a href="http://www.abacusmortgagetraining.com/">MLO</a> requirements is gathered from government agencies that regulate the mortgage industry. Please check with your legal advisor to confirm all actions regarding your licensure for final confidence.</span></td>
</tr>
</tbody>
</table>
<p> </p> </div>
</div>
</div> <div class="catchall"></div>
</div><div class="component module sub-nav-module second of-three" id="SubNavModule1">
<div class="void">
<h3 class="title"><span>Navigation</span></h3>
<div class="subnav">
<ul>
<li class="active first of-nine items-0"><a href="/"><span>Home</span></a>
</li>
<li class="second of-nine items-0"><a href="/get-your-license"><span>Get Your License</span></a>
</li>
<li class="third of-nine items-0"><a href="/renew-your-license"><span>Renew Your License</span></a>
</li>
<li class="fourth of-nine items-0"><a href="/test-prep"><span>Test Prep</span></a>
</li>
<li class="fifth of-nine items-0"><a href="/compliance"><span>Compliance</span></a>
</li>
<li class="sixth of-nine items-0"><a href="/contact-us"><span>Contact Us</span></a>
</li>
<li class="seventh of-nine items-0"><a href="/about-us"><span>About Us</span></a>
</li>
<li class="eighth of-nine items-0"><a href="/testimonials"><span>Testimonials</span></a>
</li>
<li class="end ninth of-nine items-0"><a href="/careers"><span>Careers </span></a>
</li>
</ul>
</div>
</div>
</div><div class="component text-block-component third of-three" id="TextBlockComponent3">
<div class="content">
<p>Double click here to add text to this component.</p> </div>
</div>
</div>
<div class="copyright-notice">
<p>© 2021 Abacus Mortgage Training and Education</p>
</div>
</div>
</div>
</div>
</div>
<!-- Blazonco Tracking Code -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u=(("https:" == document.location.protocol) ? "https" : "http") + "://tracker.blazonco.com/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 2884]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
    g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img alt="" src="http://tracker.blazonco.com/piwik.php?idsite=2884" style="border:0;"/></p></noscript>
<!-- End Blazonco Tracking Code -->
</body>
</html>
