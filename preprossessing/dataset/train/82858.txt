<html><body><div align="left">
<table border="0" bordercolor="#111111" cellpadding="0" cellspacing="0" id="AutoNumber1" style="border-collapse: collapse" width="100%">
<tr>
<td width="18%">
<img border="0" height="123" src="/graphics/afrigenelogo5-horn2.jpg" width="115"/></td>
<td width="82%">
<h1>CODE 403 - Forbidden Request</h1>
</td>
</tr>
</table>
</div>
<p>If you were trying to access the <a href="http://www.afrigeneas.com">AfriGeneas</a> News Archives, those newsletters have been taken offline while we correct old and dead links and change our directory structure. We apologize for the
inconvenience.

If you are looking for another page, please try the following:

</p>
<ul>
<li>Click the Refresh/Reload button, or try again later. </li>
<li>Open the home page using the navigation bar above, and then try the link to the page you want or use the site search engine.
  </li>
<li>If you arrived here through a link from another
  <a href="http://www.afrigeneas.com">AfriGeneas</a> page,</li>
</ul>
<p>Please note the URL of the referring page and then e-mail the
<a href="http://www.afrigeneas.com">AfriGeneas</a> website administrator at 
<a href="mailto:webmaster@afrigeneas.com">webmaster@afrigeneas.com</a> and let 
us know the URLs of the pages you came from and were going to so that we can 
correct the problem. </p>

(you can just cut and paste this screen and send it in an email)

<p>Just cut and paste the following and place in your email to the
<a href="mailto:webmaster@afrigeneas.com">webmaster</a>.</p>
<div align="left">
<table border="1" bordercolor="#111111" cellpadding="0" cellspacing="0" id="AutoNumber2" style="border-collapse: collapse" width="66%">
<tr>
<td width="34%"><b>Web Item</b></td>
<td width="66%"><b>Value</b></td>
</tr>
<tr>
<td width="34%">Referring URL:</td>
<td width="66%"> </td>
</tr>
<tr>
<td width="34%">Your IP: 210.75.253.169

      </td>
<td width="66%"> </td>
</tr>
<tr>
<td width="34%">Requested URL</td>
<td width="66%">/library/slave_schedule2.html </td>
</tr>
<tr>
<td width="34%">Server Name</td>
<td width="66%">www.afrigeneas.com </td>
</tr>
<tr>
<td width="34%">Your Browser Type</td>
<td width="66%">python-requests/2.22.0 </td>
</tr>
</table>
</div>
</body></html>