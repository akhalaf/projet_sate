<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "71129",
      cRay: "61115de9d916dcd6",
      cHash: "eb907d0887337e5",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmxvZ2Jvb2ttYXJrLmNvbS8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "anCxsQCAOdRCNc1lQ/WLjjrGm9iLrMsIIx1xwL3O1/clYwI54hCGuBufTqgaEgZOWYfS4khZ4uBVUpo+r9CJKbiDcRp+onFSOR9GuZMUgRmNFZGf87zUSs4D2QzqW8g0ZphMHtGd+4QxQmfzn/CIO1LWW2WtFUsa5yYHkkLIeLBPgHe5m3b+7q48/WCzxZ5yqhOUYNIfGE/dmCxvVJMxBDZT8Ezp59gYFtN8dgemjuODVlPFkBKovGlZ/HdZiVGkCAdU1LL+BKhyBlJC3VBj7AmZM7P3/Nt0rCuZRfBd66UeshzwWtOboTAQaNVn1iEYekbvmRW07KBPEl/D7xDvrrJRq6+tRIjDh1JoIMOkMabmnToV0wNGzifkPKqskEyVOmOxNqIhacvTw933xgK/ONfF0Kq8W/UnbkWrn6twaTQd9CeIEdAgN8lGYbzXHdS6/twip/zQteS0tCJgqm3sBFPHlLa3AtxqLaBH+cIwy6y0MTvoAS6qkncOHLbxJPmzIN0j+WIkE/zPvYFLxlvnMExaReHrMGJj0QWE86mMAvQJxCzs4/66GYj9OHVqvGCbLw/jVP+XIBmxWgFvaGTPwZcXIhlcys5Y5d2AX1MXJYW09CEvD3qLnqVOJD9ARmFenD8e/8XcG6DEVXIylsxIaBKaF0CgrmIOwTUym5eE0YXgzgoegwzDlSr0uFrKlYLXXHuVKNOoquWHg7U9y6gLcrxIqa8dTZ0teqaQOjWy9k4QaO5ezgtFOlD43Is6QwCx",
        t: "MTYxMDU2NDQ5Ni45NDAwMDA=",
        m: "5RIMZVaxYeixtexCz8gpr0INF+naMpbyiJtOAOZLdyc=",
        i1: "CHcSfMbmmBv7Mu2yj+O0og==",
        i2: "MnVMMfLUywLmmb+VvvTTcw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "0/jfNVpiQ78kDVwmbVAdM9t49xLjDA7btXQMS48OF08=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.blogbookmark.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=0c077e13d30a8a0954340d24fe531c2fcd0da966-1610564496-0-ARNYAgJ3Zl46m4UKauCA7oEvMPptmEzQbxMncl46vUQkPMKH4bo34GQkcON22uPifL9GnzsMt5pofNgc43jhNJwAhPBeEslVJ5gNNu4w7tGgObKQSBRSK4_TbByGy60V1AX7Nf5YIjvGONb7nbXa1EzianZQnv8Aca28UJUgeJQA4p_A_1up1tDfV_g8y_I48Jsc2nFXTA2lNlL_x-XCecDZ-bvfsgmPMuwnHfvd9b0VVXCRlqdrtXJ1ZyndTDdQQaIZYUw-Covi3KMiGP6mAYvyHWmYs5wpgxnQP-ukhI5hD9LQAmXBOdFVFc2V0dEXo-aTS6Qq6Pz9M7KhnD-VbPYXwpKJfFknw19DWt4PVfBsUEqxr6JWcK-3fbYJWdnqIinx6QzI59u_OyEuWdMu3_swUvroh0rRJQnYqbkIguKJNqBvkrpussyrDyhq6X067JqfOvj9ZHX0MkqAP4gZU3kXMOqrhO8kIRqNsyD_ZHW1VzUkSso4UK2H4pFPnwt6g6Jioe_T16cQLK0vOoVTqAI" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="05643a266c1d70853e524c79747abd8b65308a6a-1610564496-0-Afa2tD/jaHbPnKib0ci/MFg2IZYZKLX6NgNAKh7xuaO3sA53ykNDPbu0+rhYUWNw67pbmDGFbuadKdoSATMFckxvGpAwcjTe5qX7JyEk/u3y1fkvxLsbQv2fn6LfzhHVbnv/Eo86Kk8Se/PG9Ev61vvaw49f91iB18NalHzSe/R7HiSezntvHrLNfVCseZVcX6vaRaV5i9im5FSfqx/QQTZzQTOPtCIINeqCvc4UJT6WrG8d/YYuzG5XZcZP/eA6umVmMdT8qF55groKyz0YA9lRraBjfSDcwN3ZP2yXInGNEvBGS6csoxyKQ7cVhdK3iBUqGXE4UYC1czLPlkaHqXWTE6YwVb1jl5OD3Sor42QHdccvVLrsU8EhqdxjdSmp8R0LFpcMe1kRrNJ/qGDuD+GNn+G4Qa66Hoe3hJRvcKlWrrHvnwir4FxT6W92QBDzgAvhA4TxF9LoyKda8JvvUxPO7hakEZCPlrAE7NkU51U5FqYv5+whMcc0yqtR7jVjiELwlYiqBen6q+U3kieONo/XSVbRnJKPD2g4fLZurJDc/brFf+PLh3fzsfzuoC/VLhY5tMrgvsPT5P6B1TFC3OoC9LAWsXfp5+62Z1r0AH6vAlIGv7sBRX/gWP9xAYSsPNShy75IxTGCJ1UQRDNQsf+fELiaJgv45NTr9kN6vORGgqu9clS5ENBxF7Z/blDRWcphw6pg5xzGoOijLZWUmTquNi1g9G0XVw+s/IkUHJDTEAECVqAobFmCd2xKSwG0V1NpdX0zwqkTIrSRNTXOytgp5TwGh82ngQ7IKqo2uvdjOcV9ofiL9SzNQP/CK6w6MUOPY8992hDBgOfM0imYZ0X4lTzbWOgF51aIBSz5vOsEOr1l5r30t/1Kdu3ZNFoRPpVT3Mn0lhgkfab5L1PGCL5hKp/ffM0dV+wAJlQyH3fMHhTLjhtAfoFju5ib/iNFGPufb9EzojR6Cqf5J4mtMJd4LmM+52RUcJ3q/RGV8jCYG18Go1IChfb8wUo1PPDb9thV/60CIPwannzp7mqVdPb72iuv2wLop/M+edFjw7GEInjWYMzH3wT08V4+Z8iE4ls/2zPm/nyIwUvIowPkNm9H4Zcrrvp8ylAAW5N+8/7WeWNs3gN3RlIyE7eWM5jirDhVQvL2Jh+p2lKltIxG1fYerpB9sH4g0aFjCbPlMviGagWQ6tZ4lq/P0UpaYQb4rwxRb2BlxGMDrMfViqRTWMyEvQIC5YuyL6kW8Seqlrs28XERnXeAXfGstc3LeA7SliVZA3nCHiSbrJYzURcsJm14CSJf3i2dXFiLOnqV5t6Lqm3sKY2R3LJFo7lf79pEfA=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="2b8f3c9545dcc0f12990ca439ff34f17"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=61115de9d916dcd6')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<a href="https://chattard.com/grandfather.php?sid=6"><!-- table --></a>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">61115de9d916dcd6</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:8d2b:d30c:bf6f:658f</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
