<!DOCTYPE html>
<html lang="pt-PT">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://bsfotodesign.com/xmlrpc.php" rel="pingback"/>
<title>bsfotodesign – Desde 2003</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://bsfotodesign.com/feed/" rel="alternate" title="bsfotodesign » Feed" type="application/rss+xml"/>
<link href="https://bsfotodesign.com/comments/feed/" rel="alternate" title="bsfotodesign » Feed de comentários" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/bsfotodesign.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://bsfotodesign.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bsfotodesign.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open%2BSans%3A400%2C300%2C300italic%2C400italic%2C600%2C600italic%2C700%2C700italic%7CMontserrat%3A400%2C700&amp;subset=latin%2Clatin-ext" id="screenr-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bsfotodesign.com/wp-content/themes/screenr/assets/css/font-awesome.min.css?ver=4.0.0" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bsfotodesign.com/wp-content/themes/screenr/assets/css/bootstrap.min.css?ver=4.0.0" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bsfotodesign.com/wp-content/themes/screenr/style.css?ver=5.6" id="screenr-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="screenr-style-inline-css" type="text/css">
	.parallax-window.parallax-videolightbox .parallax-mirror::before{
		background-color: rgba(2,2,2,0.57);
	}
			#page-header-cover.swiper-slider.no-image .swiper-slide .overlay {
		background-color: #000000;
		opacity: 1;
	}
		.footer-widgets {
		background-color: #070707;
	}
	
	
	
	
		.footer-widgets a:hover, .footer-widgets .sidebar .widget a:hover{
	color: #bc0505;
	}
	
	
	
			input[type="reset"], input[type="submit"], input[type="submit"],
		.btn-theme-primary,
		.btn-theme-primary-outline:hover,
		.features-content .features__item,
		.nav-links a:hover,
		.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce button.button.alt
		{
			background-color: #bc0505;
		}
		textarea:focus,
		input[type="date"]:focus,
		input[type="datetime"]:focus,
		input[type="datetime-local"]:focus,
		input[type="email"]:focus,
		input[type="month"]:focus,
		input[type="number"]:focus,
		input[type="password"]:focus,
		input[type="search"]:focus,
		input[type="tel"]:focus,
		input[type="text"]:focus,
		input[type="time"]:focus,
		input[type="url"]:focus,
		input[type="week"]:focus {
			border-color: #bc0505;
		}

		a,
		.screen-reader-text:hover,
		.screen-reader-text:active,
		.screen-reader-text:focus,
		.header-social a,
		.nav-menu li.current-menu-item > a,
		.nav-menu a:hover,
		.nav-menu ul li a:hover,
		.nav-menu li.onepress-current-item > a,
		.nav-menu ul li.current-menu-item > a,
		.nav-menu > li a.menu-actived,
		.nav-menu.nav-menu-mobile li.nav-current-item > a,
		.site-footer a,
		.site-footer .btt a:hover,
		.highlight,
		.entry-meta a:hover,
		.entry-meta i,
		.sticky .entry-title:after,
		#comments .comment .comment-wrapper .comment-meta .comment-time:hover, #comments .comment .comment-wrapper .comment-meta .comment-reply-link:hover, #comments .comment .comment-wrapper .comment-meta .comment-edit-link:hover,
		.sidebar .widget a:hover,
		.services-content .service-card-icon i,
		.contact-details i,
		.contact-details a .contact-detail-value:hover, .contact-details .contact-detail-value:hover,
		.btn-theme-primary-outline
		{
			color: #bc0505;
		}

		.entry-content blockquote {
			border-left: 3px solid #bc0505;
		}

		.btn-theme-primary-outline, .btn-theme-primary-outline:hover {
			border-color: #bc0505;
		}
		.section-news .entry-grid-elements {
			border-top-color: #bc0505;
		}
			.gallery-carousel .g-item{
		padding: 0px 2px;
	}
	.gallery-carousel {
		margin-left: -2px;
		margin-right: -2px;
	}
	.gallery-grid .g-item, .gallery-masonry .g-item .inner {
		padding: 2px;
	}
	.gallery-grid, .gallery-masonry {
		margin: -2px;
	}
	
</style>
<link href="https://bsfotodesign.com/wp-content/themes/screenr/assets/css/lightgallery.css?ver=5.6" id="screenr-gallery-lightgallery-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://bsfotodesign.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://bsfotodesign.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<link href="https://bsfotodesign.com/wp-json/" rel="https://api.w.org/"/><link href="https://bsfotodesign.com/wp-json/wp/v2/pages/1498" rel="alternate" type="application/json"/><link href="https://bsfotodesign.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://bsfotodesign.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<link href="https://bsfotodesign.com/" rel="canonical"/>
<link href="https://bsfotodesign.com/" rel="shortlink"/>
<link href="https://bsfotodesign.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fbsfotodesign.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://bsfotodesign.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fbsfotodesign.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><link href="https://bsfotodesign.com/wp-content/uploads/2020/04/cropped-bs_logo_web-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://bsfotodesign.com/wp-content/uploads/2020/04/cropped-bs_logo_web-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://bsfotodesign.com/wp-content/uploads/2020/04/cropped-bs_logo_web-180x180.png" rel="apple-touch-icon"/>
<meta content="https://bsfotodesign.com/wp-content/uploads/2020/04/cropped-bs_logo_web-270x270.png" name="msapplication-TileImage"/>
</head>
<body class="home page-template page-template-template-frontpage page-template-template-frontpage-php page page-id-1498 group-blog has-site-title no-site-tagline header-layout-fixed">
<div class="site" id="page">
<a class="skip-link screen-reader-text" href="#content">Saltar para o conteúdo</a>
<header class="site-header sticky-header transparent" id="masthead" role="banner">
<div class="container">
<div class="site-branding">
<h1 class="site-title"><a href="https://bsfotodesign.com/" rel="home">bsfotodesign</a></h1>
</div><!-- .site-branding -->
<div class="header-right-wrapper">
<a href="#" id="nav-toggle">Menu<span></span></a>
<nav class="main-navigation" id="site-navigation" role="navigation">
<ul class="nav-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1192" id="menu-item-1192"><a aria-current="page" href="https://bsfotodesign.com/#about">Sobre</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home current-menu-ancestor current-menu-parent menu-item-has-children menu-item-1197" id="menu-item-1197"><a aria-current="page" href="https://bsfotodesign.com/#video">Portfolio</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1240" id="menu-item-1240"><a aria-current="page" href="https://bsfotodesign.com/#video">Video</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1241" id="menu-item-1241"><a aria-current="page" href="https://bsfotodesign.com/#gallery">Projectos</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1193" id="menu-item-1193"><a aria-current="page" href="https://bsfotodesign.com/#services">Servicos</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1194" id="menu-item-1194"><a aria-current="page" href="https://bsfotodesign.com/#clients">Clientes</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1195" id="menu-item-1195"><a aria-current="page" href="https://bsfotodesign.com/#news">Blog</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1196" id="menu-item-1196"><a aria-current="page" href="https://bsfotodesign.com/#contact">Contactos</a></li>
</ul>
</nav>
<!-- #site-navigation -->
</div>
</div>
</header><!-- #masthead -->
<div class="site-content" id="content">
<main class="site-main" id="main" role="main">
<section class="section-slider screenr-section swiper-slider full-screen fixed" id="hero">
<div class="swiper-container" data-autoplay="7000">
<div class="swiper-wrapper">
<div class="swiper-slide slide-align-center slide_content slide_content_layout_1" style="background-image: url('https://bsfotodesign.com/wp-content/uploads/2020/04/fundo-homepage-be.jpg')"><div class="swiper-slide-intro"><div class="swiper-intro-inner"><h1><strong>Dedicação que só um atelier pode dar à sua marca</strong></h1>
<p>Design é o primeiro estágio da paixão entre seu consumidor e sua Marca.</p>
<p><a class="btn btn-lg btn-theme-primary" href="#about">Comece aqui</a> <a class="btn btn-lg btn-secondary-outline" href="#contact">Contacte agora</a></p>
</div></div><div class="overlay"></div></div> </div>
<div class="btn-next-section"></div>
</div>
</section>
<section class="screenr-section section-about section-padding section-padding-larger" id="about">
<div class="container">
<div class="row">
<div class="col-md-5">
<div class="section-title-area">
<h2 class="section-title">Sobre nós</h2> <div class="section-desc"><p>Oferecemos soluções criativas, modernas e adaptadas à preferência dos vossos clientes.</p>
</div> </div>
</div>
<div class="col-md-7">
<div class="section-about-content">
<p class="p1"><span class="s1">Em 2003 começamos a dar os primeiros passos e ao longo do tempo assumimos este papel de atelier, pois gostamos de fazer grandes campanhas, mas a remodelação e modernização de pequenos negócios é algo que nos dá muito prazer, pelo facto de uma imagem pouco cuidada limitar o potencial desses negócios.</span></p>
<p class="p1"><span class="s1">O Design é a arte de resolver problemas, por vezes problemas que não vemos, assim é importante entender a influência positiva do Design de Comunicação na performance de uma empresa e as ferramentas que esta apresenta para o desenvolvimento do negócio e na notoriedade de uma marca.</span></p>
<p class="p1"><span class="s1">Desenvolvemos projectos personalizados com conceitos sólidos e adaptados à realidade e objectivos de uma marca, estabecendo uma relação, próxima genuína, duradoura e confiante com os nossos clientes, por eles trabalhamos todos os pormenores.</span></p>
</div>
</div>
</div>
</div>
</section>
<div class="parallax-videolightbox section-parallax">
<div class="parallax-bg"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/andreas-brucker-X87yB-jvYHw-unsplash-scaled-e1586270353349.jpg"/> </div>
<section class="section-videolightbox section-padding section-padding-larger section-inverse onepage-section" id="video">
<div class="container">
<h2 class="videolightbox__heading">VIDEOS</h2>
<div class="videolightbox__icon videolightbox-popup">
<a class="popup-video" data-scr="https://youtu.be/ipAhp5nlVIg" href="https://youtu.be/ipAhp5nlVIg">
<span class="video_icon"><i class="fa fa-play"></i></span>
</a>
</div>
</div>
</section>
</div>
<section class="section-gallery section-padding onepage-section" id="gallery">
<div class="g-layout-full-width container">
<div class="section-title-area">
<div class="section-subtitle">design de comunicação</div> <h2 class="section-title">CAMPANHAS E PROJECTOS</h2> </div>
<div class="section-content gallery-content">
<div class="gallery-grid g-zoom-in enable-lightbox g-col-4"><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/ribafreixo1.jpg" title="Vinhos Ribafreixo"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/ribafreixo1-538x280.jpg"/></span></span></a><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/TAP.jpg" title="TAP"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/TAP-538x280.jpg"/></span></span></a><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/lasenia.jpg" title="La Sénia"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/lasenia-538x280.jpg"/></span></span></a><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/onconutri2.jpg" title="Onconutri"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/onconutri2-538x280.jpg"/></span></span></a><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/onconutri2-poster.jpg" title="Onconutri poster"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/onconutri2-poster-538x280.jpg"/></span></span></a><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/Mito2.jpg" title="Mito2"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/Mito2-538x280.jpg"/></span></span></a><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/FIAT.jpg" title="Campanha Mito - FIAT"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/FIAT-538x280.jpg"/></span></span></a><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/James.jpg" title="James Rawes"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/James-538x280.jpg"/></span></span></a><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/SolPortugal.jpg" title="Azeite Sol Portugal"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/SolPortugal-538x280.jpg"/></span></span></a><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/STON.jpg" title="STON"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/STON-538x280.jpg"/></span></span></a><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/Cais.jpg" title="Mockups Design"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/Cais-538x280.jpg"/></span></span></a><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/topemagrecer2-Outdoor.jpg" title="Top Emagrecer-Outdoor"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/topemagrecer2-Outdoor-538x280.jpg"/></span></span></a><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/MM-SCENE-6.jpg" title="MM-SCENE-6"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/MM-SCENE-6-538x280.jpg"/></span></span></a><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/be.jpg" title="Mockups Design"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/be-538x280.jpg"/></span></span></a><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/eles.jpg" title="eles"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/eles-538x280.jpg"/></span></span></a><a class="g-item" href="https://bsfotodesign.com/wp-content/uploads/2020/04/barte-site.jpg" title="barte-site"><span class="inner"><span class="inner-content"><img alt="" src="https://bsfotodesign.com/wp-content/uploads/2020/04/barte-site-538x280.jpg"/></span></span></a></div> </div>
</div>
</section>
<section class="section-services section-padding-lg section-meta screenr-section" id="services">
<div class="container">
<div class="section-title-area">
<div class="section-subtitle">veja os nossos</div> <h2 class="section-title">Os nossos serviços</h2> </div>
<div class="section-content services-content">
<div class="row">
<div class="col-sm-6">
<div class="card card-block service__media-icon">
<div class="service-card-content">
<h3 class="card-title"><a href="https://bsfotodesign.com/branding/">Criação e Gestão de Marca</a></h3>
<div class="card-text"><p>A importância de uma marca sólida passa pela elaboração de um projecto de identidade gráfica com base num forte conceito que transmita aos seu clientes aquilo que a sua marca é e o que pode fazer por estes.</p>
</div>
</div>
<div class="service-card-icon">
<i aria-hidden="true" class="fa fa-gg fa-3x"></i>
</div>
<a class="service-button" href="https://bsfotodesign.com/branding/">Veja mais →</a>
</div>
</div>
<div class="col-sm-6">
<div class="card card-block service__media-icon">
<div class="service-card-content">
<h3 class="card-title"><a href="https://bsfotodesign.com/design-digital/">Design Digital</a></h3>
<div class="card-text"><p>No mundo empresarial a presença digital é extremamente importante pois é um veículo directo de promoção da sua marca e uma forma rápida de manter os seu clientes informados.</p>
</div>
</div>
<div class="service-card-icon">
<i aria-hidden="true" class="fa fa-sitemap fa-3x"></i>
</div>
<a class="service-button" href="https://bsfotodesign.com/design-digital/">Veja mais →</a>
</div>
</div>
</div><!-- /.row  -->
<div class="row">
<div class="col-sm-6">
<div class="card card-block service__media-icon">
<div class="service-card-content">
<h3 class="card-title"><a href="https://bsfotodesign.com/design-editorial/">Design Editorial</a></h3>
<div class="card-text"><p>O design editorial requer conhecimentos e competências específicas, dadas as suas regras e condicionantes.<br/>
A forma como é vista os seus suportes de divulgação escritas diz muito sobre a qualidade da sua mensagem tal como a sua estratégia de comunicação.</p>
</div>
</div>
<div class="service-card-icon">
<i aria-hidden="true" class="fa fa-align-left fa-3x"></i>
</div>
<a class="service-button" href="https://bsfotodesign.com/design-editorial/">Veja mais →</a>
</div>
</div>
<div class="col-sm-6">
<div class="card card-block service__media-icon">
<div class="service-card-content">
<h3 class="card-title"><a href="https://bsfotodesign.com/campanhas-audiovisual/">Campanhas Audiovisuais</a></h3>
<div class="card-text"><p>Um video dentro do contexto de uma campanha ou identidade gráfica bem planeada, pode destacar a sua marca, pois capta a atenção tanto numa TV, site ou redes sociais. É uma forma de promover a sua empresa ou um determinado produto, dando-lhe visibilidade.</p>
</div>
</div>
<div class="service-card-icon">
<i aria-hidden="true" class="fa fa-video-camera fa-3x"></i>
</div>
<a class="service-button" href="https://bsfotodesign.com/campanhas-audiovisual/">Veja mais →</a>
</div>
</div>
</div><!-- /.row  -->
<div class="row">
</div>
</div><!-- /.section-content -->
</div>
</section>
<section class="section-clients section-padding section-padding-lg screenr-section" id="clients">
<div class="container">
<div class="section-title-area">
<div class="section-subtitle">Deixamos a nossa marca na</div> </div>
<div class="section-client-content">
<div class="clients-wrapper client-4-cols">
<div class="client-col" title="IMMERSIS">
<a href="#" title="IMMERSIS"> <img alt="IMMERSIS" src="https://bsfotodesign.com/wp-content/uploads/2020/04/immersis-final.png"/>
</a> </div>
<div class="client-col" title="TAP PORTUGAL">
<a href="#" title="TAP PORTUGAL"> <img alt="TAP PORTUGAL" src="https://bsfotodesign.com/wp-content/uploads/2020/04/TAP-final.png"/>
</a> </div>
<div class="client-col" title="FIAT">
<a href="#" title="FIAT"> <img alt="FIAT" src="https://bsfotodesign.com/wp-content/uploads/2020/04/FIAT-final.png"/>
</a> </div>
<div class="client-col" title="RIBAFREIXO">
<a href="#" title="RIBAFREIXO"> <img alt="RIBAFREIXO" src="https://bsfotodesign.com/wp-content/uploads/2020/04/ribafreixo-final.png"/>
</a> </div>
</div><!-- /.clients-wrapper -->
</div>
</div>
</section>
<section class="screenr-section section-counter section-padding section-padding-lg section-meta" id="counter">
<div class="container">
<div class="section-title-area">
<div class="section-subtitle">Algumas curiosidades sobre o nosso atelier?</div> </div>
<div class="counter-contents" data-layout="4">
<div class="section-content">
<div class="row">
<div class="col-sm-12 col-md-6 col-lg-4">
<div class="counter-item counter-item-" style="background-color: #0099e5">
<span class="counter-title">Projectos completos</span>
<div class="counter__number">
<span class="n counter">178</span>
</div>
<i class="fa fa-cogs fa-3x"></i>
</div>
</div>
<div class="col-sm-12 col-md-6 col-lg-4">
<div class="counter-item counter-item-" style="background-color: #d31204">
<span class="counter-title">Feedback Positivos</span>
<div class="counter__number">
<span class="n counter">97</span>
<span class="after-number">%</span>
</div>
<i class="fa fa-comments fa-3x"></i>
</div>
</div>
<div class="col-sm-12 col-md-6 col-lg-4">
<div class="counter-item counter-item-" style="background-color: #7ac143">
<span class="counter-title">Países com a nossa marca</span>
<div class="counter__number">
<span class="n counter">26</span>
</div>
<i class="fa fa-paper-plane-o fa-3x"></i>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section-news section-padding section-padding-lg" id="news">
<div class="container">
<div class="section-title-area">
<div class="section-subtitle">blog bsfotodesign</div> <h2 class="section-title">A nossa opinião</h2> </div>
<div class="section-content section-news-content">
<div class="row">
<div class="content-grid" data-layout="3" id="section-news-posts">
<article class="col-md-6 col-lg-4 post-1566 post type-post status-publish format-standard has-post-thumbnail hentry category-business tag-design tag-design-de-comunicacao tag-design-grafico tag-gestao-de-marca tag-publicidade" id="post-1566">
<div class="entry-thumb">
<a href="https://bsfotodesign.com/design-custo-ou-ganho/">
<img alt="" class="attachment-screenr-blog-grid-small size-screenr-blog-grid-small wp-post-image" height="200" loading="lazy" src="https://bsfotodesign.com/wp-content/uploads/2020/04/blog-bs1-350x200.jpg" width="350"/> </a>
</div>
<div class="entry-grid-elements">
<div class="entry-grid-cate"><a href="https://bsfotodesign.com/category/business/">Design</a></div> <header class="entry-header">
<div class="entry-grid-title"><a href="https://bsfotodesign.com/design-custo-ou-ganho/" rel="bookmark">Design, custo ou ganho?</a></div> </header><!-- .entry-header -->
<div class="entry-excerpt">
<p>Em tempos difíceis, muitas empresas ponderam os seu investimento em comunicação como desnecessário….</p>
</div><!-- .entry-content -->
<div class="entry-grid-more">
<a href="https://bsfotodesign.com/design-custo-ou-ganho/" title="Design, custo ou ganho?">Ler mais <i aria-hidden="true" class="fa fa-arrow-circle-o-right"></i></a>
</div>
</div>
</article><!-- #post-## -->
<article class="col-md-6 col-lg-4 post-1 post type-post status-publish format-standard has-post-thumbnail hentry category-arte category-arte-urbana category-audiovisuais category-cinema category-business category-etccanal category-youtube" id="post-1">
<div class="entry-thumb">
<a href="https://bsfotodesign.com/hello-world/">
<img alt="" class="attachment-screenr-blog-grid-small size-screenr-blog-grid-small wp-post-image" height="200" loading="lazy" src="https://bsfotodesign.com/wp-content/uploads/2020/04/nikita-kachanovsky-g-YiX8ynmnY-unsplash-scaled-e1586268123459-350x200.jpg" width="350"/> </a>
</div>
<div class="entry-grid-elements">
<div class="entry-grid-cate"><a href="https://bsfotodesign.com/category/arte/">arte</a></div> <header class="entry-header">
<div class="entry-grid-title"><a href="https://bsfotodesign.com/hello-world/" rel="bookmark">Olá a todos.</a></div> </header><!-- .entry-header -->
<div class="entry-excerpt">
<p>Esta é uma forma de poderem ver algo mais do que um atelier,…</p>
</div><!-- .entry-content -->
<div class="entry-grid-more">
<a href="https://bsfotodesign.com/hello-world/" title="Olá a todos.">Ler mais <i aria-hidden="true" class="fa fa-arrow-circle-o-right"></i></a>
</div>
</div>
</article><!-- #post-## -->
<article class="col-md-6 col-lg-4 post-1177 post type-post status-publish format-standard has-post-thumbnail hentry category-business tag-design tag-design-de-equipamento tag-design-industrial tag-designer tag-reymond-loewy" id="post-1177">
<div class="entry-thumb">
<a href="https://bsfotodesign.com/reymond-loewy/">
<img alt="" class="attachment-screenr-blog-grid-small size-screenr-blog-grid-small wp-post-image" height="200" loading="lazy" src="https://bsfotodesign.com/wp-content/uploads/2016/04/Reymond-Loewy-350x200.jpg" width="350"/> </a>
</div>
<div class="entry-grid-elements">
<div class="entry-grid-cate"><a href="https://bsfotodesign.com/category/business/">Design</a></div> <header class="entry-header">
<div class="entry-grid-title"><a href="https://bsfotodesign.com/reymond-loewy/" rel="bookmark">Reymond Loewy</a></div> </header><!-- .entry-header -->
<div class="entry-excerpt">
<p>(1893 – 1986) A um grau dificilmente inigualável no mundo do design industrial…</p>
</div><!-- .entry-content -->
<div class="entry-grid-more">
<a href="https://bsfotodesign.com/reymond-loewy/" title="Reymond Loewy">Ler mais <i aria-hidden="true" class="fa fa-arrow-circle-o-right"></i></a>
</div>
</div>
</article><!-- #post-## -->
</div>
<div class="clear"></div>
<div class="content-grid-loadmore blt-ajax">
<a class="btn btn-theme-primary-outline" href="#">Carregar mais notícias<i aria-hidden="true" class="fa fa-angle-double-down"></i></a>
</div>
</div>
</div>
</div>
</section>
<section class="section-contact section-padding onepage-section section-meta" id="contact">
<div class="container">
<div class="section-title-area">
<div class="section-subtitle">Mantenha-se em contacto</div> <h2 class="section-title">Contacte-nos</h2> <div class="section-desc"><p>Preencha o formulário abaixo e entraremos em contacto consigo.</p>
</div> </div>
<div class="section-content">
<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
<ul class="contact-details">
<li class="contact-detail">
<span class="contact-icon"><i aria-hidden="true" class="fa fa-instagram fa-2x"></i></span> <a href="https://www.instagram.com/bsaldanha"> <span class="contact-detail-value">bsaldanha</span>
</a> </li>
<li class="contact-detail">
<span class="contact-icon"><i aria-hidden="true" class="fa fa-facebook-official fa-2x"></i></span> <a href="https://www.facebook.com/bsfotodesign.2003/"> <span class="contact-detail-value">bsfotodesign</span>
</a> </li>
<li class="contact-detail">
<span class="contact-icon"><i aria-hidden="true" class="fa fa-youtube-play fa-2x"></i></span> <a href="https://www.youtube.com/etccanal"> <span class="contact-detail-value">ETC Canal</span>
</a> </li>
<li class="contact-detail">
<span class="contact-icon"><i aria-hidden="true" class="fa fa-phone-square fa-2x"></i></span> <a href="tel:+351 962 471 213"> <span class="contact-detail-value">(+351) 962 471 213</span>
</a> </li>
</ul>
</div>
<div class="col-md-1"></div>
</div>
</div>
</div>
</section>
</main><!-- #main -->
</div><!-- #content -->
<footer class="site-footer" id="colophon" role="contentinfo">
<div class="footer-widgets section-padding ">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-12 footer-column widget-area sidebar" id="footer-1" role="complementary">
<aside class="widget_text widget widget_custom_html" id="custom_html-3"><h3 class="widget-title">Contactos Rápidos</h3><div class="textwidget custom-html-widget"><div class="contact-info-item">
<div class="contact-text"><i class="fa fa-phone"></i></div>
<div class="contact-value"><a href="tel:+351962471213">(+351) 962 471 213</a></div>
</div>
<div class="contact-info-item">
<div class="contact-text"><i class="fa fa-envelope"></i></div>
<div class="contact-value"><a href="“gera@bsfotodesign.com”">geral@bsfotodesign.com</a></div>
</div>
<div class="contact-info-item">
<div class="contact-text"><i class="fa fa-whatsapp"></i></div>
<div class="contact-value"><a href="https://www.whats.li/bsfotodesign" rel="noopener" target="_blank">Contacte via Whatsapp</a></div>
</div>
</div></aside> </div>
<div class="col-md-6 col-sm-12 footer-column widget-area sidebar" id="footer-2" role="complementary">
<aside class="widget widget_text" id="text-3"><h3 class="widget-title">Não se esqueça</h3> <div class="textwidget"><p class="p1"><span class="s1">Uma imagem cuidada, assertiva e correctamente adaptada à mensagem e função que a sua marca procura transmitir, demonstra que essa mesma marca está<span class="Apple-converted-space">  </span>emprenhada<span class="Apple-converted-space">  </span>na consolidação do presente e a sua evolução, tendo assim o futuro no horizonte. </span></p>
<p class="p1"><span class="s1">Tudo isso se reflete no cliente, dando-lhe a clara percepção do cuidado que a marca irá ter com o mesmo. </span></p>
<p class="p1"><span class="s1">Entenda então que o design não é<span class="Apple-converted-space">  </span>custo para que a sua empresa ganhe mais, mais notoriedade, visibilidade e consequentemente negócio.</span></p>
</div>
</aside> </div>
<div class="col-md-3 col-sm-12 footer-column widget-area sidebar" id="footer-3" role="complementary">
<aside class="widget widget_search" id="search-7"><h3 class="widget-title">Pesquise aqui</h3><form action="https://bsfotodesign.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Pesquisar por:</span>
<input class="search-field" name="s" placeholder="Pesquisar …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Pesquisar"/>
</form></aside><aside class="widget widget_media_image" id="media_image-3"><img alt="" class="image wp-image-1251 attachment-60x60 size-60x60" height="60" loading="lazy" sizes="(max-width: 60px) 100vw, 60px" src="https://bsfotodesign.com/wp-content/uploads/2020/04/bslogoRED-150x150.png" srcset="https://bsfotodesign.com/wp-content/uploads/2020/04/bslogoRED-150x150.png 150w, https://bsfotodesign.com/wp-content/uploads/2020/04/bslogoRED-298x300.png 298w, https://bsfotodesign.com/wp-content/uploads/2020/04/bslogoRED.png 587w" style="max-width: 100%; height: auto;" width="60"/></aside> </div>
</div>
</div>
</div>
<div class=" site-info">
<div class="container">
<div class="site-copyright">
				Copyright © 2021 bsfotodesign. Todos os direitos reservados.			</div><!-- .site-copyright -->
<div class="theme-info">
<a href="https://www.famethemes.com/themes/screenr">Screenr parallax theme</a> por FameThemes			</div>
</div>
</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/bsfotodesign.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://bsfotodesign.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="screenr-plugin-js" src="https://bsfotodesign.com/wp-content/themes/screenr/assets/js/plugins.js?ver=4.0.0" type="text/javascript"></script>
<script id="bootstrap-js" src="https://bsfotodesign.com/wp-content/themes/screenr/assets/js/bootstrap.min.js?ver=4.0.0" type="text/javascript"></script>
<script id="screenr-theme-js-extra" type="text/javascript">
/* <![CDATA[ */
var Screenr = {"ajax_url":"https:\/\/bsfotodesign.com\/wp-admin\/admin-ajax.php","full_screen_slider":"1","header_layout":"transparent","slider_parallax":"1","is_home_front_page":"1","autoplay":"7000","speed":"700","effect":"slide","gallery_enable":"1"};
/* ]]> */
</script>
<script id="screenr-theme-js" src="https://bsfotodesign.com/wp-content/themes/screenr/assets/js/theme.js?ver=20120206" type="text/javascript"></script>
<script id="wp-embed-js" src="https://bsfotodesign.com/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
</body>
</html>
