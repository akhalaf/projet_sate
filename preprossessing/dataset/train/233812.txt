<!DOCTYPE html>
<html lang="fr">
<head>
<link href="/favicon.png" rel="Shortcut Icon" type="image/x-icon"/>
<meta content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" name="viewport"/>
<meta charset="utf-8"/>
<title>Forum pour webmasters e-business - BizPowa.com</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700|Lato:400,700" rel="stylesheet" type="text/css"/>
<link href="https://static.bizpowa.com/disclaimer.css" rel="stylesheet"/>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-7242164-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</head>
<body>
<div class="container container-disclaimer">
<img alt="" class="disclaimer-logo-header" src="https://static.bizpowa.com/images/disclaimer/logo_disclaimer.png"/>
<div class="disclaimer-box">
<p>Bizpowa n’est pas un forum comme les autres, c’est une communauté pour laquelle le <strong>partage</strong> est une <strong>valeur essentielle</strong>.</p>
<p>Si tu veux <strong>apprendre, t’informer et donner</strong> pour faire du <strong>business</strong>... la chèvre t’attend les bras ouverts ;-)</p>
</div>
<a class="disclaimer-enter" href="/forum/forumdisplay.php?f=2"><img alt="" src="https://static.bizpowa.com/images/disclaimer/disclaimer-btn.png"/></a>
</div>
<div class="disclaimer-footer">
<p>Bizpowa existe depuis 2006 . <a href="mailto:webmaster@bizpowa.com">Contact</a> . <a href="bizpowa-mentions-legales.gif">Mentions légales</a></p>
</div>
</body>
</html>