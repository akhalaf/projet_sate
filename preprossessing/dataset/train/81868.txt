<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="user-scalable=yes, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height" name="viewport"/>
<title>Login</title>
<link href="https://cdn.jsdelivr.net/gh/affinityteam/www-assets/v1/favicon1.ico" rel="icon" type="image/x-icon"/>
<link href="https://cdn.jsdelivr.net/gh/affinityteam/www@54c56bb/lg/content/styles.css" rel="stylesheet"/>
<link href="https://cdn.jsdelivr.net/gh/affinityteam/www@54c56bb/lg/content/app.css" rel="stylesheet"/>
</head>
<body class="newui idserve">
<div id="app">
<router-view></router-view>
</div>
<script src="https://cdn.jsdelivr.net/gh/affinityteam/www@54c56bb/lg/scripts/lib.js"></script>
<script src="https://cdn.jsdelivr.net/gh/affinityteam/www@54c56bb/lg/scripts/app.js"></script>
</body>
</html>
