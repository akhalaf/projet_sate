<!DOCTYPE html>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-163576236-7"></script>
<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	  gtag('config', 'UA-163576236-7');
	</script>
<title>Página inicial .:. Editora BEĨ</title> <meta charset="utf-8"/> <meta content="IE=edge" http-equiv="X-UA-Compatible"/> <meta content="#000" name="theme-color"/> <link href="https://www.bei.com.br/assets/css/images/logo-theme.png" rel="icon" sizes="192x192"/> <meta content="width=device-width, initial-scale=1" name="viewport"/> <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/> <link href="https://www.bei.com.br/assets/css/images/logo-theme.png" rel="icon"/> <link href="http://www.bei.com.br/" rel="canonical"/> <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/> <meta content="Página inicial .:. Editora BEĨ" itemprop="name"/> <meta content="" itemprop="description"/> <meta content="" itemprop="image"/> <meta content="Página inicial .:. Editora BEĨ" property="og:title"/> <meta content="website" property="og:type"/> <meta content="Página inicial .:. Editora BEĨ" property="og:site_name"/> <meta content="http://www.bei.com.br/" property="og:url"/> <meta content="" property="og:image"/> <meta content="pt_BR" property="og:locale"/> <meta content="" property="og:description"/> <meta content="summary" name="twitter:card"/> <meta content="http://www.bei.com.br/" name="twitter:site"/> <meta content="Página inicial .:. Editora BEĨ" name="twitter:title"/> <meta content="" name="twitter:description"/> <meta content="" name="twitter:creator"/> <meta content="" name="twitter:image"/> <meta content="" lang="pt-BR" name="description" xml:lang="pt-BR"/> <meta content="" name="keywords"/> <meta content="" name="abstract"/> <meta content="Internet" name="classification"/> <meta content="ALL" name="robots"/> <meta content="" name="author"/> <meta content="pt-br" name="language"/> <meta content="" name="reply-to"/> <meta content="BR-RS" name="geo.region"/> <meta content="Página inicial .:. Editora BEĨ" name="DC.title"/> <meta content="Página inicial .:. Editora BEĨ" name="title"/> <meta content="http://www.bei.com.br/" name="url"/> <meta content="" name="company"/> <meta content="" name="application-name"/> <meta content="" name="DC.creator "/> <meta content="" name="DC.creator.address"/> <meta content="© Copyright 2021" name="copyright"/> <meta content="Sublime Text 3" name="generator"/> <meta content="general" name="rating"/> <style type="text/css">.loading{position:fixed;top:0;left:0;display:block;width:100%;height:100%;background:#FFF;z-index:9999}.loading.trans{background:transparent}.loading::after{content:"";border:2px solid #efefef;visibility:visible;opacity:1;border-top:2px solid #333;position:absolute;top:50%;right:50%;margin-top:-50px;margin-right:-50px;display:block;border-radius:100%!important;width:100px;height:100px;-webkit-animation:spin .3s linear infinite;-moz-animation:spin .3s linear infinite;-o-animation:spin .3s linear infinite;animation:spin .3s linear infinite}@-webkit-keyframes spin{0%{-webkit-transform:rotate(0deg)}100%{-webkit-transform:rotate(360deg)}}@keyframes spin{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}</style>
</head>
<body style="overflow:hidden">
<i class="loading"></i>
<header class="page-top">
<div class="container">
<div class="row">
<div class="col-md-12">
<a class="pull-left" href="https://www.bei.com.br/">
<img alt="Editora BEĨ" class="img-responsive logo-figure" src="https://www.bei.com.br/assets/images/logo_bei.svg"/>
</a>
<nav class="menu">
<button class="btn-hamb hidden visible-sm visible-xs pull-right">
<span></span>
<span></span>
<span></span>
</button>
<ul class="list-inline text-uppercase">
<li class=""><a href="https://www.bei.com.br/sobre">Sobre</a></li>
<li class=""><a href="https://www.bei.com.br/catalogo">Catálogo</a></li>
<li class=""><a href="https://www.bei.com.br/blog">Blog</a></li>
<li class=""><a href="https://www.bei.com.br/imprensa">Imprensa</a></li>
<li>
<form action="https://www.bei.com.br/catalogo/" class="header-search" method="get">
<input name="search" placeholder="Faça sua busca" type="text"/>
<button><i class="icon-magnifier"></i></button>
</form>
</li>
<li class=""><a href="https://www.bei.com.br/login/?return=d3d3LmJlaS5jb20uYnIv">Login/Cadastro</a></li>
<li><a class="carrinho" href="https://www.bei.com.br/carrinho"><i class="icon-basket"></i><i class="count-carrinho" style="display: none;">0</i></a></li>
</ul>
</nav> </div>
</div>
</div>
</header>
<style type="text/css">
	/*header.page-top { background-color: #0406aa!important; }*/
	header.page-top nav.menu .btn-hamb span::before, header.page-top nav.menu .btn-hamb span::after { background-color: #0406aa!important }
</style>
<section class="main-slider" style="margin-top: 80px;">
<div class="container-fluid">
<div class="row">
<style type="text/css">
				section.main-slider {}
				section.main-slider .item { height: 85vh; max-height: 360px; background-size: cover; background-position: center; }
				@media (min-width: 1600px){
					section.main-slider .item { max-height: 650px; }
				}
				#carouselMain .item .carousel-caption > div:nth-child(1) { padding: 0 20px 0 0; }
				#carouselMain .item .carousel-caption > div > * { float: left; width: 100%; margin: 0; text-align: left; text-shadow: none!important; }
				#carouselMain .item .carousel-caption > div h3 { font-family: 'PT Serif',serif; font-weight: 700; font-size: 30px; margin-bottom: 20px; }
				#carouselMain .item .carousel-caption > div h2 { font-family: 'PT Serif',serif; font-weight: 700; font-size: 52px; margin-bottom: 20px; }
				#carouselMain .item .carousel-caption > div h4 { font-family: 'Fira Sans', serif; font-weight: 700; font-size: 22px; margin-bottom: 30px; } 
				#carouselMain .item .carousel-caption > div p { font-family: 'Fira Sans', serif; font-size: 20px; margin-bottom: 30px; }
				#carouselMain .item .carousel-caption > div span { text-align: center; }
				#carouselMain .item .carousel-caption > div span button {  display: inline-block; font-family: 'Martel Sans'; color: #0406aa; background: #FFF; text-transform: uppercase; border-radius: 20px; border: none; padding: 8px 18px 4px 18px; font-size: 16px;  }
				#carouselMain .item .carousel-caption > div span button:hover { background: #0406aa; color: #FFF; }
				#carouselMain .item .carousel-caption > div img { box-shadow: 0 10px 10px rgba(0,0,0,.5); max-height: 554px; width: initial; }
				@media (max-width: 1599px){
					section.main-slider .item { max-height: 400px; }
					#carouselMain .carousel-caption { right: 10.5%; left: 10.5%; padding: 0 0 30px 0; }
					#carouselMain .item .carousel-caption > div h3 { font-size: 20px; margin-bottom: 15px; }
					#carouselMain .item .carousel-caption > div h2 { font-size: 38px; margin-bottom: 15px; }
					#carouselMain .item .carousel-caption > div h4 { font-size: 14px; margin-bottom: 20px; }
					#carouselMain .item .carousel-caption > div p { font-size: 12px; margin-bottom: 20px; }
					#carouselMain .item .carousel-caption > div img { max-width: 250px; max-height: 320px; float: initial; display: inline-block; }
					#carouselMain .item .carousel-caption > div:nth-child(2) { text-align: center; }
					#carouselMain .item .carousel-caption > div span button { font-size: 14px }
				}
				@media (max-width: 1400px){
					section.main-slider .item { max-height: 360px; }
					#carouselMain .carousel-caption { right: 8%; left: 8%; padding: 30px 0 0 0; }
				}
				@media (max-width: 992px){

					section.main-slider .item { height: 710px;  max-height: initial; }


					#carouselMain .item .carousel-caption { bottom: initial; top: 0px; }

					#carouselMain .item .carousel-caption > div img { max-width: 200px; margin: 20px 0; box-shadow: 0 5px 10px rgba(0,0,0,.25);} 

					#carouselMain .item .carousel-caption > div h3 { font-size: 18px; margin-bottom: 10px; }
					#carouselMain .item .carousel-caption > div h2 { font-size: 22px; margin-bottom: 10px; }
					#carouselMain .item .carousel-caption > div h4 { font-size: 12px; margin-bottom: 10px; }
					#carouselMain .item .carousel-caption > div p { font-size: 12px; margin-bottom: 10px; }

					#carouselMain .item .carousel-caption > div:nth-child(1) { padding: 0; }

				}
			</style>
<div class="carousel slide" data-interval="10000" data-pause="null" data-ride="carousel" id="carouselMain">
<div class="carousel-inner">
<div class="item active" style="background-image: url(https://www.bei.com.br/assets//tim.php?w=1920&amp;h=730&amp;src=https://www.bei.com.br/manager/uploads/banners/);background-color:#c87dff">
<div class="container">
<div class="carousel-caption">
<div class="col-md-7 col-xs-12">
<h3>Pré-venda</h3> <h2>Política comercial no brasil: <br/>causas e consequências do nosso isolamento</h2> <h4>Emanuel Ornelas, João Paulo Pessoa e Lucas Ferraz</h4> <p>O que faz com que produtos importados sejam tão caros no Brasil em relação ao resto do mundo? Quais as consequências disso para a sociedade brasileira? E o que pode ser feito para melhorar a situação?<br/>
Essas são algumas das questões respondidas por Política comercial do Brasil: causas e consequências do nosso isolamento, escrito pelos economistas e pesquisadores Emanuel Ornelas, João Paulo Pessoa e Lucas Ferraz.</p> <span><a href="https://bei.com.br/livro/pr-venda-politica-comercial-no-brasil-causas-e-consequencias-do-nosso-isolamento/262"><button>Saiba Mais</button></a></span> </div>
<div class="col-md-5">
<img src="https://www.bei.com.br/assets//tim.php?w=500&amp;src=https://www.bei.com.br/manager/uploads/banners/pol-tica-comercial-no-brasil-causas-e-consequ-ncias-do-nosso-isolamento-659.jpg"/>
</div>
</div>
</div>
</div>
<div class="item " style="background-image: url(https://www.bei.com.br/assets//tim.php?w=1920&amp;h=730&amp;src=https://www.bei.com.br/manager/uploads/banners/);background-color:#7cc5ff">
<div class="container">
<div class="carousel-caption">
<div class="col-md-7 col-xs-12">
<h3>Lançamento</h3> <h2>Decadência e reconstrução: <br/> Espírito Santo</h2> <h4>Carlos Melo, Malu Delgado e Milton Seligman</h4> <p>“O Brasil de hoje é o Espírito Santo de ontem? E o Espírito Santo de hoje é o Brasil de amanhã?“  <br/>
Essas duas questões formam a cabeça, o tronco e os membros deste livro – que soma reflexão, trabalho de campo e conclusões capazes de levar à ação.  Escrito por Carlos Melo, Milton Seligman e Malu Delgado, Decadência e Reconstrução está destinado a se tornar referência nos estudos de um país que, “perplexo e polarizado”, tenta ainda “encontrar caminhos para agir”. Felizmente, eles existem.  <br/>
</p> <span><a href="https://bei.com.br/livro/pre-venda-decadencia-e-reconstrucao-espirito-santo-licoes-da-sociedade-civil-para-um-caso-politico-no-brasil-contemporaneo/261"><button>Saiba mais</button></a></span> </div>
<div class="col-md-5">
<img src="https://www.bei.com.br/assets//tim.php?w=500&amp;src=https://www.bei.com.br/manager/uploads/banners/espirito-santo-431.jpg"/>
</div>
</div>
</div>
</div>
<div class="item " style="background-image: url(https://www.bei.com.br/assets//tim.php?w=1920&amp;h=730&amp;src=https://www.bei.com.br/manager/uploads/banners/);background-color:#bebbbb">
<div class="container">
<div class="carousel-caption">
<div class="col-md-7 col-xs-12">
<h3>Lançamento</h3> <h2>A nova economia mundial: <br/>um guia para iniciantes</h2> <h4>Randy Charles Epping</h4> <p>Diante das rápidas e profundas mudanças que vêm acontecendo em todo o mundo, é imprescindível que as pessoas compreendam as bases e a dinâmica da nova economia globalizada: esse conhecimento não apenas garante melhores decisões individuais, mas também é essencial para a compreensão do cenário político mundial e para o exercício pleno e consciente da cidadania no novo século. <br/>
</p> <span><a href="https://bei.com.br/livro/a-nova-economia-mundial-um-guia-para-iniciantes/259"><button>Saiba mais</button></a></span> </div>
<div class="col-md-5">
<img src="https://www.bei.com.br/assets//tim.php?w=500&amp;src=https://www.bei.com.br/manager/uploads/banners/randy-424.jpeg"/>
</div>
</div>
</div>
</div>
<div class="item " style="background-image: url(https://www.bei.com.br/assets//tim.php?w=1920&amp;h=730&amp;src=https://www.bei.com.br/manager/uploads/banners/);background-color:#b49800">
<div class="container">
<div class="carousel-caption">
<div class="col-md-7 col-xs-12">
<h3>Lançamento</h3> <h2>Cozinha Pantaneira: <br/>comitiva de sabores</h2> <h4>Paulo Machado</h4> <p>Cozinha pantaneira: Comitiva de sabores, do chef de cozinha sul-mato-grossense Paulo Machado, é mais que um livro de receitas. O leitor terá em mãos um importante registro de uma cozinha rica em sabores e múltipla nas suas origens. Encontramos nas páginas desse livro uma profusão de condimentos e temperos, legumes, doces de fruta e conservas, caldos, peixes e carnes – em receitas que aproveitam todas as partes do animal.</p> <span><a href="https://bei.com.br/livro/cozinha-pantaneira-comitiva-de-sabores/260"><button>Saiba Mais</button></a></span> </div>
<div class="col-md-5">
<img src="https://www.bei.com.br/assets//tim.php?w=500&amp;src=https://www.bei.com.br/manager/uploads/banners/cozinha-pantaneira-372.jpg"/>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section-page">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2 class="section-title text-uppercase">Destaques</h2>
</div>
</div>
<div class="row">
<div class="row"></div><div class="row"> <div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<a href="https://www.bei.com.br/livro/politica-comercial-no-brasil-causas-e-consequencias-do-nosso-isolamento/262"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/pr-venda-politica-comercial-no-brasil-causas-e-consequencias-do-nosso-isolamento-505.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="303" href="https://www.bei.com.br/livro/politica-comercial-no-brasil-causas-e-consequencias-do-nosso-isolamento/262">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/politica-comercial-no-brasil-causas-e-consequencias-do-nosso-isolamento/262"><h3>Política comercial no brasil: causas e consequências do nosso isolamento</h3></a>
<p>Emanuel Ornelas, João Paulo Pessoa e Lucas Ferraz</p>
<div class="price">R$ 56,00</div>
</div>
</div>
</div>
<div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<a href="https://www.bei.com.br/livro/decadencia-e-reconstrucao-espirito-santo-licoes-da-sociedade-civil-para-um-caso-politico-no-brasil-contemporaneo/261"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/decadencia-e-reconstrucao-espirito-santo-licoes-da-sociedade-civil-para-um-caso-politico-no-brasil-c-359.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="302" href="https://www.bei.com.br/livro/decadencia-e-reconstrucao-espirito-santo-licoes-da-sociedade-civil-para-um-caso-politico-no-brasil-contemporaneo/261">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/decadencia-e-reconstrucao-espirito-santo-licoes-da-sociedade-civil-para-um-caso-politico-no-brasil-contemporaneo/261"><h3>Decadência e reconstrução - Espírito Santo: lições da sociedade civil para um caso político no Brasil contemporâneo</h3></a>
<p>Carlos Melo, Malu Delgado e Milton Seligman</p>
<div class="price">R$ 49,00</div>
</div>
</div>
</div>
<div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<a href="https://www.bei.com.br/livro/predio-da-escola-do-jockey-club-de-sao-paulo/175"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/predio-do-colegio-105.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="131" href="https://www.bei.com.br/livro/predio-da-escola-do-jockey-club-de-sao-paulo/175">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/predio-da-escola-do-jockey-club-de-sao-paulo/175"><h3>Prédio da Escola do Jockey Club de São Paulo</h3></a>
<p>Fernando Serapião, Marina Frúgoli, Mauro Calliari e Silvio Oksman</p>
<div class="price">R$ 90,00</div>
</div>
</div>
</div>
<div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<a href="https://www.bei.com.br/livro/cozinha-pantaneira-comitiva-de-sabores/260"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/cozinha-pantaneira-comitiva-de-sabores-189.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="301" href="https://www.bei.com.br/livro/cozinha-pantaneira-comitiva-de-sabores/260">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/cozinha-pantaneira-comitiva-de-sabores/260"><h3>Cozinha Pantaneira: comitiva de sabores</h3></a>
<p>Paulo Machado</p>
<div class="price">R$ 80,00</div>
</div>
</div>
</div>
</div><div class="row"> <div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<a href="https://www.bei.com.br/livro/a-nova-economia-mundial-um-guia-para-iniciantes/259"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/a-nova-economia-mundial-um-guia-para-iniciantes-629.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="300" href="https://www.bei.com.br/livro/a-nova-economia-mundial-um-guia-para-iniciantes/259">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/a-nova-economia-mundial-um-guia-para-iniciantes/259"><h3>A nova economia mundial: um guia para iniciantes</h3></a>
<p>Randy Charles Epping</p>
<div class="price">R$ 56,00</div>
</div>
</div>
</div>
<div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<a href="https://www.bei.com.br/livro/mari-hirata-sensei-por-haydee-belda/156"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/mari-hirata-806.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="293" href="https://www.bei.com.br/livro/mari-hirata-sensei-por-haydee-belda/156">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/mari-hirata-sensei-por-haydee-belda/156"><h3>Mari Hirata Sensei por Haydée Belda</h3></a>
<p>Haydée Belda e Mari Hirata</p>
<div class="price">R$ 120,00</div>
</div>
</div>
</div>
<div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<div class="price_off">-10%</div> <a href="https://www.bei.com.br/livro/parques-urbanos-de-sao-paulo/172"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/parques-urbanos-de-sp-277.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="128" href="https://www.bei.com.br/livro/parques-urbanos-de-sao-paulo/172">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/parques-urbanos-de-sao-paulo/172"><h3>Parques urbanos de São Paulo</h3></a>
<p>André Bueno, Tatewaki Nio, Maria Teresa Diniz, Arq.Futuro e Equipe Bei</p>
<div class="price">
<span class="old-price">R$ 75,00</span>
									R$ 67,50								</div>
</div>
</div>
</div>
<div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<a href="https://www.bei.com.br/livro/chantal-akerman-tempo-expandido/173"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/chantal-akerman-237.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="129" href="https://www.bei.com.br/livro/chantal-akerman-tempo-expandido/173">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/chantal-akerman-tempo-expandido/173"><h3>Chantal Akerman: tempo expandido</h3></a>
<p>Evangelina Seiler, Chantal Akerman, Claire Atherton, Élisabeth Lebovici e Ivone Margulies</p>
<div class="price">R$ 70,00</div>
</div>
</div>
</div>
</div> </div>
</div>
</section>
<section class="section-page section-catalog">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2 class="section-title text-uppercase">Catálogo</h2>
</div>
</div>
<div class="row">
<div class="row"></div><div class="row"> <div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<a href="https://www.bei.com.br/livro/o-fuzzy-e-o-techie/166"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/o-fuzzy-e-o-techie-91.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="123" href="https://www.bei.com.br/livro/o-fuzzy-e-o-techie/166">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/o-fuzzy-e-o-techie/166"><h3>O fuzzy e o techie</h3></a>
<p>Scoth Hartley</p>
<div class="price">R$ 59,00</div>
</div>
</div>
</div>
<div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<a href="https://www.bei.com.br/livro/pedra-o-universo-escondido/169"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/denise-milan-244.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="126" href="https://www.bei.com.br/livro/pedra-o-universo-escondido/169">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/pedra-o-universo-escondido/169"><h3>Pedra: o universo escondido</h3></a>
<p>Denise Milan, Organização: Manuela Mena</p>
<div class="price">R$ 120,00</div>
</div>
</div>
</div>
<div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<div class="price_off">-10%</div> <a href="https://www.bei.com.br/livro/colecoes-de-artistas/162"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/cole-es-de-artistas-691.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="122" href="https://www.bei.com.br/livro/colecoes-de-artistas/162">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/colecoes-de-artistas/162"><h3>Coleções de artistas</h3></a>
<p>Nessia Leonzini</p>
<div class="price">
<span class="old-price">R$ 90,00</span>
									R$ 81,00								</div>
</div>
</div>
</div>
<div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<a href="https://www.bei.com.br/livro/confianca-como-criar-as-bases-para-o-empreendedorismo-nos-paises-em-desenvolvimento/170"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/confian-a-tarun-khanna-604.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="127" href="https://www.bei.com.br/livro/confianca-como-criar-as-bases-para-o-empreendedorismo-nos-paises-em-desenvolvimento/170">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/confianca-como-criar-as-bases-para-o-empreendedorismo-nos-paises-em-desenvolvimento/170"><h3>Confiança – Como criar as bases para o empreendedorismo nos países em desenvolvimento</h3></a>
<p>Tarun Khanna</p>
<div class="price">R$ 49,00</div>
</div>
</div>
</div>
</div><div class="row"> <div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<a href="https://www.bei.com.br/livro/urbanismo-e-seguranca-publica/174"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/urbanismo-e-seguran-a-p-blica-453.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="130" href="https://www.bei.com.br/livro/urbanismo-e-seguranca-publica/174">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/urbanismo-e-seguranca-publica/174"><h3>Urbanismo e segurança pública</h3></a>
<p>Organização: Arq.Futuro e Escola da Cidade</p>
<div class="price">R$ 20,00</div>
</div>
</div>
</div>
<div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<a href="https://www.bei.com.br/livro/grafite-labirintos-do-olhar/160"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/grafite-labirintos-do-olhar-440.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="120" href="https://www.bei.com.br/livro/grafite-labirintos-do-olhar/160">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/grafite-labirintos-do-olhar/160"><h3>Grafite – Labirintos do olhar</h3></a>
<p>Eduardo Longman e Gabriela Longman</p>
<div class="price">R$ 60,00</div>
</div>
</div>
</div>
<div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<a href="https://www.bei.com.br/livro/espaco-publico-e-urbanidade-em-sao-paulo/158"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/espa-o-publico-e-urbanidade-692.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="119" href="https://www.bei.com.br/livro/espaco-publico-e-urbanidade-em-sao-paulo/158">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/espaco-publico-e-urbanidade-em-sao-paulo/158"><h3>Espaço público e urbanidade em São Paulo</h3></a>
<p>Mauro Calliari</p>
<div class="price">R$ 75,00</div>
</div>
</div>
</div>
<div class="col-md-3 col-xs-12">
<div class="book-item">
<div class="book-image">
<div class="price_off">-10%</div> <a href="https://www.bei.com.br/livro/jayme-c-fonseca-rodrigues-arquiteto/153"><img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/products/jayme-c-fonseca-rodrigues-arq-514.jpg&amp;w=500&amp;h=500&amp;zc=2"/></a>
<div class="book-actions no-login">
<a class="add-to-cart" data-product="117" href="https://www.bei.com.br/livro/jayme-c-fonseca-rodrigues-arquiteto/153">
<i class="icon-basket-loaded"></i>
</a>
</div>
</div>
<div class="item-infos text-center">
<a href="https://www.bei.com.br/livro/jayme-c-fonseca-rodrigues-arquiteto/153"><h3>Jayme C. Fonseca Rodrigues – Arquiteto</h3></a>
<p>Hugo Segawa</p>
<div class="price">
<span class="old-price">R$ 120,00</span>
									R$ 108,00								</div>
</div>
</div>
</div>
</div> </div>
<div class="row">
<div class="col-md-2 col-md-offset-5 text-center">
<div class="load-more-wrap">
<a class="text-uppercase btn-theme" href="https://www.bei.com.br/catalogo"><i class="icon-plus"></i> Mais</a>
</div>
</div>
</div>
</div>
</section>
<section class="section-page">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2 class="section-title text-uppercase">Blog</h2>
</div>
</div>
<div class="row">
<div class="col-md-6">
<div class="post-item">
<a href="https://www.bei.com.br/post/em-2021/6">
<img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/posts/em-2021-413.jpg&amp;w=560&amp;h=460"/>
</a>
</div>
</div>
<div class="col-md-6">
<div class="post-item">
<a href="https://www.bei.com.br/post/assista-o-webinar-de-lancamento-do-livro/5">
<img alt="" class="img-responsive" src="https://www.bei.com.br/assets//tim.php?src=https://www.bei.com.br/manager/uploads/posts/assista-o-webinar-de-lancamento-do-livro-51.jpg&amp;w=560&amp;h=460"/>
</a>
</div>
</div>
</div>
</div>
</section> <style type="text/css">
	.newsletter { background: #FFF; border-top: solid 1px #0406AA; padding: 40px 0; text-align: center; }
	.newsletter h2 { float: left; width: 100%; margin: 0; font-family: 'Martel Sans'; font-weight: 900; font-size: 24px; text-transform: uppercase; }
	.newsletter h4 { float: left; width: 100%; margin: 15px 0; font-family: 'Fira Sans'; font-size: 16px; }
	.newsletter form { display: inline-block; margin: 0; }
	.newsletter form button { padding: 10px 30px 6px 30px; outline: none; }
	.newsletter form input { display: inline-block; padding: 6px 10px; font-family: 'Fira Sans'; font-size: 16px; border-radius: 50px; min-width: 300px; max-width: 100%; border: solid 1px #CCC; outline: none; }
	.newsletter form input:focus { border: solid 1px #0406AA; }
	@media (max-width: 992px){
		.newsletter form input { margin-bottom: 5px; min-width: initial; width: 100%; }
		.newsletter form button { width: 100%; }
		.newsletter form { float: left; width: 100%; }
	}
</style>
<section class="newsletter">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2>Não perca nenhum lançamento</h2>
<h4>Inscreva-se na nossa newsletter e saiba das últimas novidades e promoções especiais.</h4>
<form class="">
<input name="action" type="hidden" value="newsletter"/>
<input data-required="email" name="email" placeholder="Seu e-mail..." type="text"/>
<button class="btn-theme">INSCREVER</button>
</form>
</div>
</div>
</div>
</section>
<footer class="page-footer">
<div class="container">
<div class="row">
<div class="col-md-4">
<h3 style="font-size:18px;margin-top:0">BEĨ Editora</h3>
<nav class="menu-footer">
<ul class="list-unstyled">
<li><a href="https://www.bei.com.br/sobre/quem-somos">Quem Somos</a></li>
<li><a href="https://www.bei.com.br/sobre/apoie-nos">Apoie-nos</a></li>
<li><a href="https://www.bei.com.br/sobre/plataformas">Plataformas</a></li>
<li><a href="https://www.bei.com.br/imprensa">Imprensa</a></li>
<!--<li><a href="https://www.bei.com.br/">perguntas frequentes</a></li>-->
<!--<li><a href="https://www.bei.com.br/">frete e prazos</a></li>-->
<!--<li><a href="https://www.bei.com.br/">pagamentos</a></li>-->
<li><a href="https://www.bei.com.br/politica-de-vendas-e-trocas">Política de Vendas e Trocas</a></li>
<li><a href="https://www.bei.com.br/termos-de-uso-e-politica-de-privacidade">Termos de Uso e Política de Privacidade</a></li>
</ul>
</nav>
</div>
<div class="col-md-3">
<div class="infos">
<ul class="list-unstyled">
<li class="clearfix">
<i class="icon-call-end"></i>
<h4>+55 (11) 3089.8855</h4>
</li>
<li class="clearfix">
<i class="icon-envelope"></i>
<h4>comercial@bei.com.br</h4>
</li>
</ul>
<address class="page-address">
<p>R. Elvira Ferraz, 250 cj. 411<br/>CEP 04552-050<br/>Vila Olímpia, São Paulo<br/>SP</p>
</address>
</div>
</div>
<div class="col-md-5 text-center">
<nav class="redes-sociais">
<ul class="list-inline">
<li><a href="https://twitter.com/beieditora" target="_blank"><i class="icon-social-twitter"></i></a></li>
<li><a href="https://www.facebook.com/editorabei" target="_blank"><i class="icon-social-facebook"></i></a></li>
<li><a href="https://www.instagram.com/beieditora/?hl=pt-br" target="_blank"><i class="icon-social-instagram"></i></a></li>
<li><a href="https://www.youtube.com/user/Beieditora" target="_blank"><i class="icon-social-youtube"></i></a></li>
<li><a href="https://www.linkedin.com/company/11802756/" target="_blank"><i class="icon-social-linkedin"></i></a></li>
</ul>
</nav>
<img class="img-responsive" src="https://www.bei.com.br/assets//images/formas-de-pagamento.svg" style="max-width:370px;margin-top:20px;display:inline-block;"/>
<img class="img-responsive" src="https://www.bei.com.br/assets//images/ssl.svg" style="max-width:200px;margin-top:20px;display:inline-block;"/>
</div>
</div>
<div class="row">
<div class="col-md-12 text-right" style="padding:15px 10px 15px 0">
<a alt="Desenvolvido por Create Pensamentos Online" href="http://create.art.br/" target="_blank" title="Desenvolvido por Create Pensamentos Online"><img src="https://create.art.br/logo-create_branco.svg" style="width:54px;"/></a>
</div>
</div>
</div>
</footer>
<div class="modal fade" data-backdrop="false" data-keyboard="false" id="alert" role="dialog" tabindex="-1">
<div class="modal-dialog modal-md"><div class="loading"></div></div>
</div>
<div class="msg">
<i class="first icon"></i><p></p><small></small><i class="icon icon-close"></i>
</div>
<input data-assets="https://www.bei.com.br/assets/" data-link="https://www.bei.com.br/" data-logs="TRUE" data-page="" data-param1="" data-param2="" data-param3="" data-param4="" data-uploads="https://www.bei.com.br/manager/uploads/" id="app-infos" type="hidden"/> <script src="https://www.bei.com.br/assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="https://www.bei.com.br/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://www.bei.com.br/assets/js/plugin.app.js?6849" type="text/javascript"></script>
<script src="https://www.bei.com.br/assets/js/jquery.mask.js" type="text/javascript"></script>
<script src="https://www.bei.com.br/assets/js/jquery.plugin.min.js" type="text/javascript"></script>
<script src="https://www.bei.com.br/assets/js/jquery.realperson.min.js" type="text/javascript"></script>
<script src="https://www.bei.com.br/assets/js/select-customized.js"></script>
<script src="https://www.bei.com.br/assets/js/app.js?3128" type="text/javascript"></script>
</body>
<link href="https://www.bei.com.br/assets/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://www.bei.com.br/assets/css/font-awesome.min.css" rel="stylesheet"/>
<link href="https://www.bei.com.br/assets/css/simple-line-icons.css" rel="stylesheet"/>
<link href="https://www.bei.com.br/assets/css/components.css?9096" rel="stylesheet"/>
<link href="https://www.bei.com.br/assets/css/jquery.realperson.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700|Martel+Sans:900|Fira+Sans:400,600|PT+Serif:700" rel="stylesheet"/>
<link href="https://www.bei.com.br/assets/css/select-customized.css" rel="stylesheet" type="text/css"/>
<link href="https://www.bei.com.br/assets/css/app.css?5866" rel="stylesheet"/>
<link href="https://www.bei.com.br/assets/css/mobile.css?7809" rel="stylesheet"/>
<!--<link rel="stylesheet" href="https://www.bei.com.br/assets/css/easyzoom.css">--></html>