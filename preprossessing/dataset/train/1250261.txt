<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<title>Under Construction – RPM Egypt</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://rpmegypt.com/index.php/feed/" rel="alternate" title="RPM Egypt » Feed" type="application/rss+xml"/>
<link href="https://rpmegypt.com/index.php/comments/feed/" rel="alternate" title="RPM Egypt » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/rpmegypt.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://rpmegypt.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rpmegypt.com/wp-content/themes/hello-elementor/style.min.css?ver=2.2.0" id="hello-elementor-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rpmegypt.com/wp-content/themes/hello-elementor/theme.min.css?ver=2.2.0" id="hello-elementor-theme-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rpmegypt.com/index.php/wp-json/" rel="https://api.w.org/"/><link href="https://rpmegypt.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://rpmegypt.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<link href="https://rpmegypt.com/?elementor_library=under-construction" rel="canonical"/>
<link href="https://rpmegypt.com/?p=8" rel="shortlink"/>
<link href="https://rpmegypt.com/index.php/wp-json/oembed/1.0/embed?url=https%3A%2F%2Frpmegypt.com%2F%3Felementor_library%3Dunder-construction" rel="alternate" type="application/json+oembed"/>
<link href="https://rpmegypt.com/index.php/wp-json/oembed/1.0/embed?url=https%3A%2F%2Frpmegypt.com%2F%3Felementor_library%3Dunder-construction&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<style>#wp-admin-bar-elementor-maintenance-on > a { background-color: #dc3232; }
			#wp-admin-bar-elementor-maintenance-on > .ab-item:before { content: "\f160"; top: 2px; }</style>
<meta content="width=device-width, initial-scale=1.0, viewport-fit=cover" name="viewport"/></head>
<body class="elementor_library-template elementor_library-template-elementor_canvas single single-elementor_library postid-8 elementor-default elementor-template-canvas elementor-kit-7 elementor-page elementor-page-8 elementor-maintenance-mode" data-rsssl="1">
<div class="elementor elementor-8" data-elementor-id="8" data-elementor-settings="[]" data-elementor-type="page">
<div class="elementor-inner">
<div class="elementor-section-wrap">
<section class="elementor-section elementor-top-section elementor-element elementor-element-5d90307 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-element_type="section" data-id="5d90307" data-settings='{"background_background":"classic"}'>
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-row">
<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-0751a53" data-element_type="column" data-id="0751a53">
<div class="elementor-column-wrap elementor-element-populated">
<div class="elementor-widget-wrap">
<div class="elementor-element elementor-element-3449486 elementor-widget elementor-widget-heading" data-element_type="widget" data-id="3449486" data-widget_type="heading.default">
<div class="elementor-widget-container">
<h3 class="elementor-heading-title elementor-size-default">RPM</h3> </div>
</div>
<div class="elementor-element elementor-element-fb67370 elementor-widget elementor-widget-heading" data-element_type="widget" data-id="fb67370" data-widget_type="heading.default">
<div class="elementor-widget-container">
<h3 class="elementor-heading-title elementor-size-default">TRADING &amp; Distribution</h3> </div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-1f28148" data-element_type="column" data-id="1f28148">
<div class="elementor-column-wrap elementor-element-populated">
<div class="elementor-widget-wrap">
<div class="elementor-element elementor-element-68c0313 elementor-widget elementor-widget-heading" data-element_type="widget" data-id="68c0313" data-widget_type="heading.default">
<div class="elementor-widget-container">
<h1 class="elementor-heading-title elementor-size-default">WEBSITE</h1> </div>
</div>
<div class="elementor-element elementor-element-bbb0190 elementor-widget elementor-widget-heading" data-element_type="widget" data-id="bbb0190" data-widget_type="heading.default">
<div class="elementor-widget-container">
<h1 class="elementor-heading-title elementor-size-default">UNDER CONSTRUCTION</h1> </div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-top-section elementor-element elementor-element-8eafcc2 elementor-section-height-min-height elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-items-middle" data-element_type="section" data-id="8eafcc2" data-settings='{"background_background":"classic"}'>
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-row">
<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f12a666" data-element_type="column" data-id="f12a666" data-settings='{"background_background":"classic"}'>
<div class="elementor-column-wrap elementor-element-populated">
<div class="elementor-widget-wrap">
<div class="elementor-element elementor-element-eaa4c30 elementor-widget elementor-widget-image" data-element_type="widget" data-id="eaa4c30" data-widget_type="image.default">
<div class="elementor-widget-container">
<div class="elementor-image">
<img alt="" class="attachment-full size-full" height="506" loading="lazy" sizes="(max-width: 900px) 100vw, 900px" src="https://rpmegypt.com/wp-content/uploads/2020/12/UC.png" srcset="https://rpmegypt.com/wp-content/uploads/2020/12/UC.png 900w, https://rpmegypt.com/wp-content/uploads/2020/12/UC-300x169.png 300w, https://rpmegypt.com/wp-content/uploads/2020/12/UC-768x432.png 768w" width="900"/> </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
<link href="https://rpmegypt.com/wp-content/plugins/elementor/assets/css/frontend-legacy.min.css?ver=3.0.15" id="elementor-frontend-legacy-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rpmegypt.com/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.0.15" id="elementor-frontend-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rpmegypt.com/wp-content/uploads/elementor/css/post-8.css?ver=1609088893" id="elementor-post-8-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rpmegypt.com/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.9.1" id="elementor-icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rpmegypt.com/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.0.15" id="elementor-animations-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rpmegypt.com/wp-content/uploads/elementor/css/post-7.css?ver=1609073289" id="elementor-post-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rpmegypt.com/wp-content/uploads/elementor/css/global.css?ver=1609085543" id="elementor-global-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;ver=5.6" id="google-fonts-1-css" media="all" rel="stylesheet" type="text/css"/>
<script id="wp-embed-js" src="https://rpmegypt.com/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
<script id="jquery-core-js" src="https://rpmegypt.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://rpmegypt.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="elementor-frontend-modules-js" src="https://rpmegypt.com/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.0.15" type="text/javascript"></script>
<script id="jquery-ui-core-js" src="https://rpmegypt.com/wp-includes/js/jquery/ui/core.min.js?ver=1.12.1" type="text/javascript"></script>
<script id="elementor-dialog-js" src="https://rpmegypt.com/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.8.1" type="text/javascript"></script>
<script id="elementor-waypoints-js" src="https://rpmegypt.com/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2" type="text/javascript"></script>
<script id="swiper-js" src="https://rpmegypt.com/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6" type="text/javascript"></script>
<script id="share-link-js" src="https://rpmegypt.com/wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.0.15" type="text/javascript"></script>
<script id="elementor-frontend-js-before" type="text/javascript">
var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.0.15","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":"https:\/\/rpmegypt.com\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":8,"title":"Under%20Construction%20%E2%80%93%20RPM%20Egypt","excerpt":"","featuredImage":false}};
</script>
<script id="elementor-frontend-js" src="https://rpmegypt.com/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.0.15" type="text/javascript"></script>
</body>
</html>
