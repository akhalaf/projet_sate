<html>
<head>
<title>Atmospheric Optics</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Atmospheric optics,rays, shadows, green flash, crepuscular, anticrepuscular,rainbows, rainbow, supernumerary,glory,glories,corona, fogbow, fogbows, diffraction, heiligenschein,brocken,ice,halos,water droplets,raindrops,aurorae,nacreous,noctilucent,optics, parhelia, sun dogs, sundogs, Parry, Lowitz, circumzenithal, circumhorizontal, tangent arcs, ice crystals" http-equiv="Keywords"/>
<meta content="rainbow, rainbows,Atmospheric halos, halo, atmospheric optics, parhelia, sun dogs, sundogs, halos, Parry, Lowitz, circumzenithal, circumhorizontal, tangent arcs, ice crystals" name="Keywords"/>
<meta content="Atmospheric optics - Rainbows, halos, glories and many other visual spectacles produced by light playing on water drops, dust and ice crystals in the atmosphere with explanations, images and downloadable freeware to simulate them." name="Description"/>
<link href="http://www.atoptics.co.uk/feature.rss" rel="alternate" title="Atmospheric Optics: OpticsPOD" type="application/rss+xml"/>
<script language="JavaScript1.2">
<!--
function cook1() {
  pr2 = window.open('cook.htm', 'new2', 'width=545,height=460,left=0,top=390,scrollbars=yes')
  pr2.focus()
}
function priv2() {
  pr2 = window.open('priv.htm', 'new2', 'width=445,height=460,left=0,top=390,scrollbars=yes')
  pr2.focus()
}
function copy2() {
  pr4 = window.open('copyrt.htm', 'new2', 'width=445,height=260,left=0,top=390,scrollbars=yes')
  pr4.focus()
}
function screens() {
  pr3 = window.open('brow.htm', 'new3', 'width=395,height=200,left=0,top=390,scrollbars=no')
  pr3.focus()
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<link href="portsty_dev.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
<!--
#j2 {  text-decoration: underline; color: #FF0000}
#j2:hover {  text-decoration: underline; color: #CC0033}
.style3 {	font-family: Arial, Helvetica, sans-serif;	font-weight: normal;	font-size: 14pt;	color: #CC9B84;}
#s2 {
	text-decoration : none;
	color: #C9A287;
	font-size: 8pt;
} 
#s2:hover { text-decoration: none; color: #F55612} 
#s3 {	text-decoration : none;	color: #A28984;} 
#s3:hover {
	text-decoration: none;
	color: #F55612;
	font-size: 8pt;
} 
.style8 {	color: #B1928B;	font-weight: bold;}
#apDiv1 {
	position:absolute;
	width:200px;
	height:115px;
	z-index:1;
	left: 461px;
	top: 100px;
}
.style12 {
	color: #FFFFFF;
	font-size: 11pt;
}
-->
</style>
</head>
<body bgcolor="#31464f" onload="MM_preloadImages('imopod/opodfo.gif')" text="#000000">
<table align="center" border="0" cellpadding="0" cellspacing="0" height="478" width="800">
<tr>
<td align="left" height="65" valign="bottom" width="60"><img height="17" src="images1/spacer.gif" width="60"/></td>
<td align="left" class="title" height="65" valign="bottom" width="740">Atmospheric 
      Optics</td>
</tr>
<tr>
<td align="left" colspan="2" height="12" valign="bottom"><img height="22" src="images1/spacer.gif" width="30"/></td>
</tr>
<tr>
<td class="bar" colspan="2" height="1"><img height="1" src="images1/spacer.gif" width="10"/></td>
</tr>
<tr>
<td align="left" colspan="2" height="303" valign="bottom">
<table border="0" cellpadding="0" cellspacing="0" width="693">
<tr>
<td height="267" width="60"><img height="326" src="images1/spacer.gif" width="60"/></td>
<td align="left" height="267" valign="bottom" width="350"><a href="opod.htm"><img height="301" src="im1800/fy1867.jpg" width="350"/></a></td>
<td height="267" width="43"><img height="322" src="images1/spacer.gif" width="43"/></td>
<td class="text1" height="267" width="248"><br/>
<span class="style12">Light playing on water drops, 
            dust or ice crystals in the atmosphere produces a host of visual spectacles 
            - rainbows, halos, glories, coronas and many more. Some can be 
            seen almost every day or so, some are once in a lifetime sights. Find 
            out where to see them and how they are formed. Then seek and enjoy 
            them outdoors.</span> <br/>
<br/></td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="left" colspan="2" height="8" valign="bottom"><img height="22" src="images1/spacer.gif" width="30"/></td>
</tr>
<tr>
<td align="left" colspan="2" height="33" valign="bottom">
<table border="0" cellpadding="0" cellspacing="0" height="18" width="800">
<tr align="left" valign="bottom">
<td height="9" width="10"></td>
<td height="9" width="157"><img height="10" src="images1/spacer.gif" width="19"/></td>
<td height="9" width="171"><img height="10" src="images1/spacer.gif" width="23"/></td>
<td height="9" width="214"><img height="10" src="images1/spacer.gif" width="21"/></td>
<td height="9" width="248"><img height="10" src="images1/spacer.gif" width="14"/></td>
</tr>
<tr>
<td height="7" width="10"><img height="24" src="images1/spacer.gif" width="7"/></td>
<td align="left" height="7" valign="bottom" width="157"><a href="opod.htm"><img border="0" height="22" id="Image1" name="Image1" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images2/imopod/opodfo.gif',1)" src="images2/imopod/opodf.gif" width="129"/></a></td>
<td align="left" height="7" valign="bottom" width="171"><a class="menbig" href="rayshad.htm">Rays/Shadows</a></td>
<td align="left" height="7" valign="bottom" width="214"><a class="menbig" href="light.htm">Droplets/Diffraction</a></td>
<td align="left" height="7" valign="bottom" width="248"><a class="menbig" href="bows.htm">Rainbows</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td colspan="2" height="2"><img height="12" src="images1/spacer.gif" width="30"/></td>
</tr>
<tr>
<td class="bar" colspan="2" height="1"><img height="1" src="images1/spacer.gif" width="30"/></td>
</tr>
<tr>
</tr>
<tr>
<td colspan="2" height="36">
<table border="0" cellpadding="0" cellspacing="0" height="36" width="800">
<tr>
<td width="167"><img height="24" src="images1/spacer.gif" width="16"/><a class="menpink" href="wnew.htm">What's
          New</a></td>
<td align="left" valign="bottom" width="172"><a class="menbig" href="halosim.htm">Ice
               Halos</a></td>
<td align="left" valign="bottom" width="214"><a class="menbig" href="hat1.htm">High Atmosphere</a></td>
<td align="left" valign="bottom" width="175"><a class="menbig" href="links.htm">Links/Resources</a></td>
<td align="left" valign="bottom" width="72"><a class="menpink" href="../resource/indx.htm">Search</a></td>
</tr>
</table>
</td>
</tr>
<tr align="left" valign="bottom">
<td colspan="2" height="2"><img height="6" src="images1/spacer.gif" width="30"/></td>
</tr>
<tr align="left" valign="bottom">
<td colspan="2" height="10">
<table border="0" cellpadding="0" cellspacing="0" width="800">
<tr>
<td height="21" valign="bottom" width="168"><img height="10" src="images1/spacer.gif" width="18"/><span class="style3"><a href="tiltsun.htm" id="s2">TiltingSun</a></span></td>
<td height="21" valign="bottom" width="172"><a class="smlink" href="../resource/feedback.htm">Contact 
            - Les Cowley</a></td>
<td align="left" height="21" valign="bottom" width="212"><a class="smlink" href="javascript:copy2()">Copyright &amp; permission requests</a></td>
<td align="left" height="21" valign="bottom" width="176"><a class="smlink" href="javascript:cook1()">Cookies</a></td>
<td align="left" height="21" valign="bottom" width="72"><a class="smlink" href="javascript:priv2()">Privacy</a></td>
</tr>
</table>
</td>
</tr>
<tr align="left" valign="bottom">
<td colspan="2" height="6"><img height="6" src="images1/spacer.gif" width="30"/></td>
</tr>
<tr align="left" valign="bottom">
<td colspan="2" height="8">
<table border="0" cellpadding="0" cellspacing="0" width="800">
<tr align="left" valign="top">
<td width="346"><br/>
<img height="9" src="images1/spacer.gif" width="42"/></td>
<td class="txtsmall style8" width="197"><a href="wnew.htm" id="s3"></a></td>
<td class="txtsmall" width="257"> </td>
</tr>
</table>
</td>
</tr>
</table>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-9886400-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
