<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8 no-js" lang="en-US"> <![endif]--><!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<!--[if lte IE 8]><meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=IE8" /><![endif]-->
<title>Page not found - Precise Reporting Services</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<link href="https://precisereportingservices.net/xmlrpc.php" rel="pingback"/>
<link href="//maps.googleapis.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://precisereportingservices.net/feed/" rel="alternate" title="Precise Reporting Services » Feed" type="application/rss+xml"/>
<link href="https://precisereportingservices.net/comments/feed/" rel="alternate" title="Precise Reporting Services » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/precisereportingservices.net\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://precisereportingservices.net/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://precisereportingservices.net/wp-content/plugins/ultimate-branding/inc/modules/front-end/assets/css/cookie-notice.css?ver=3.4.3" id="branda-cookie-notice-front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://precisereportingservices.net/wp-content/themes/C7Creative/style-elusive-webfont.css?ver=2.0.2" id="elusive-webfont-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://precisereportingservices.net/wp-content/themes/C7Creative/style.css?ver=2.0.2" id="risen-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://precisereportingservices.net/wp-content/themes/C7Creative/styles/light/style.css?ver=2.0.2" id="risen-base-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:400,700,400italic,700italic" id="google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://precisereportingservices.net/wp-content/plugins/toggles-shortcode-and-widget/include/otw_components/otw_shortcode/css/general_foundicons.css?ver=5.4.4" id="otw-shortcode-general_foundicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://precisereportingservices.net/wp-content/plugins/toggles-shortcode-and-widget/include/otw_components/otw_shortcode/css/social_foundicons.css?ver=5.4.4" id="otw-shortcode-social_foundicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://precisereportingservices.net/wp-content/plugins/toggles-shortcode-and-widget/include/otw_components/otw_shortcode/css/otw_shortcode.css?ver=5.4.4" id="otw-shortcode-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://precisereportingservices.net/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://precisereportingservices.net/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://precisereportingservices.net/wp-content/themes/C7Creative/js/modernizr.custom.js?ver=2.0.2" type="text/javascript"></script>
<script src="https://precisereportingservices.net/wp-content/themes/C7Creative/js/jquery.backstretch.min.js?ver=2.0.2" type="text/javascript"></script>
<script src="https://precisereportingservices.net/wp-content/themes/C7Creative/js/superfish.min.js?ver=2.0.2" type="text/javascript"></script>
<script src="https://precisereportingservices.net/wp-content/themes/C7Creative/js/supersubs.js?ver=2.0.2" type="text/javascript"></script>
<script src="https://precisereportingservices.net/wp-content/themes/C7Creative/js/selectnav.min.js?ver=2.0.2" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;key=AIzaSyDjzDVmn2yyxynmanEMgk0bAb8a44vij6U" type="text/javascript"></script>
<script src="https://precisereportingservices.net/wp-content/themes/C7Creative/js/jquery.fitvids.js?ver=2.0.2" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var risen_wp = {"theme_uri":"https:\/\/precisereportingservices.net\/wp-content\/themes\/C7Creative","is_home":"","site_url":"https:\/\/precisereportingservices.net","home_url":"https:\/\/precisereportingservices.net","is_ssl":"1","current_protocol":"https","ie_unsupported_message":"You are using an outdated version of Internet Explorer. Please upgrade your browser to use this site.","ie_unsupported_redirect_url":"http:\/\/browsehappy.com\/","mobile_menu_label":"Menu","slider_enabled":"1","slider_slideshow":"1","slider_speed":"4000","gmaps_api_key":"AIzaSyDjzDVmn2yyxynmanEMgk0bAb8a44vij6U","ajax_url":"https:\/\/precisereportingservices.net\/wp-admin\/admin-ajax.php","contact_form_nonce":"d887732b17","comment_name_required":"1","comment_email_required":"1","comment_name_error_required":"Required","comment_email_error_required":"Required","comment_email_error_invalid":"Invalid Email","comment_url_error_invalid":"Invalid URL","comment_message_error_required":"Comment Required","lightbox_prev":"Prev","lightbox_next":"Next","lightbox_expand":"Expand","lightbox_close":"Close"};
/* ]]> */
</script>
<script src="https://precisereportingservices.net/wp-content/themes/C7Creative/js/main.js?ver=2.0.2" type="text/javascript"></script>
<script src="https://precisereportingservices.net/wp-content/plugins/toggles-shortcode-and-widget/include/otw_components/otw_shortcode/js/otw_shortcode_core.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://precisereportingservices.net/wp-content/plugins/toggles-shortcode-and-widget/include/otw_components/otw_shortcode/js/otw_shortcode.js?ver=5.4.4" type="text/javascript"></script>
<link href="https://precisereportingservices.net/wp-json/" rel="https://api.w.org/"/>
<link href="https://precisereportingservices.net/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://precisereportingservices.net/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.4" name="generator"/>
<style id="branda-admin-bar-logo" type="text/css">
body #wpadminbar #wp-admin-bar-wp-logo > .ab-item {
	background-image: url(http://dakresources.webc7.com/wp-content/uploads/2019/07/C7-Creative.png);
	background-repeat: no-repeat;
	background-position: 50%;
	background-size: 80%;
}
body #wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
	content: " ";
}
</style>
<style type="text/css">
body {
	background: #e8e8e8 url(https://precisereportingservices.net/wp-content/themes/C7Creative/images/backgrounds/orman-clark-subtle-pattern-2-colorable.png) repeat scroll;
	
}

a, .resurrect-list-icons a:hover, .flex-caption a {
	color: #752121;
}

#header-menu, #footer-bottom, .flex-caption, .flex-control-nav li a.active, #home-row-widgets .widget-image-title, #page-header h1, .sidebar-widget-title {
	background-color: #752121;
}

body, input, textarea, select, .multimedia-short h1, #cancel-comment-reply-link, .accordion-section-title, .staff header h1 a {
	font-family: 'Ubuntu', Arial, Helvetica, sans-serif;
}

#header-menu-links, .flex-caption, #home-row-widgets .widget-image-title, #page-header h1, h1.sidebar-widget-title, a.button, a.comment-reply-link, a.comment-edit-link, a.post-edit-link, .nav-left-right a, input[type=submit] {
	font-family: 'Ubuntu', Arial, Helvetica, sans-serif;
}

.heading, .page-title, .post-content h1, .post-content h2, .post-content h3, .post-content h4, .post-content h5, .post-content h6, .author-box h1, .staff header h1, .location header h1, #reply-title, #comments-title, .home-column-widgets-title, .ppt, #tagline, #intro {
	font-family: 'Ubuntu', Arial, Helvetica, sans-serif;
}
</style>
<link href="http://precisereportingservices.net/wp-content/uploads/2015/09/PRECISE-LOGO.png" rel="shortcut icon"/>
<script type="text/javascript">

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-13097879-1']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

</script>
<link href="https://precisereportingservices.net/wp-content/uploads//2015/09/PRECISE-LOGO-55x55.png" rel="icon" sizes="32x32"/>
<link href="https://precisereportingservices.net/wp-content/uploads//2015/09/PRECISE-LOGO.png" rel="icon" sizes="192x192"/>
<link href="https://precisereportingservices.net/wp-content/uploads//2015/09/PRECISE-LOGO-180x180.png" rel="apple-touch-icon"/>
<meta content="https://precisereportingservices.net/wp-content/uploads//2015/09/PRECISE-LOGO.png" name="msapplication-TileImage"/>
<style id="wp-custom-css" type="text/css">
			#branda-cookie-notice {
    background-color: #555 !important;
}
img.wp-image-280.alignleft {
    position: absolute;
    float: left;
    display: flex;
    margin-top: 0px;
}
p.cookie-text {
    padding: 7px 0px 0px 40px;
    margin-bottom: 7px;
	margin-top: 0px !important;
	margin-left: 0px !important;
}
#branda-cookie-notice .cookie-notice-container .branda-cn-container {
    display: inline-flex;
}
#branda-cookie-notice .cookie-notice-container {
    max-width: 1200px;
    margin: 0 auto;
}
img.wp-image-280.alignleft.c7icon {
    width: 28px;
}		</style>
<style id="branda-cookie-notice-css" type="text/css">
#branda-cookie-notice {
	color: #fff;
	background-color: #bcbcbc;
}
#branda-cookie-notice a,
#branda-cookie-notice a:link {
	color: #ffffff;
}
#branda-cookie-notice a:visited {
	color: #ffffff;
}
#branda-cookie-notice a:hover {
	color: #ffffff;
}
#branda-cookie-notice a:active {
	color: #ffffff;
}
#branda-cookie-notice a:focus {
	color: #ffffff;
}
#branda-cookie-notice .button,
#branda-cookie-notice .button:link {
	color: #ffffff;
	border-color: #000000;
	background-color: #2b2b2b;
	border-style: solid;
	border-width: 1px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
}
#branda-cookie-notice .button:visited {
}
#branda-cookie-notice .button:hover {
	color: #ffffff;
	border-color: #000000;
	background-color: #5b5b5b;
}
#branda-cookie-notice .button:active {
	color: #ffffff;
	border-color: #000000;
	background-color: #5b5b5b;
}
#branda-cookie-notice .button:focus {
	color: #ffffff;
	border-color: #000000;
	background-color: #5b5b5b;
}
</style>
</head>
<body class="error404">
<!-- Container Start -->
<div id="container">
<div id="container-inner">
<!-- Header Start -->
<header id="header">
<div id="header-inner">
<div id="header-content">
<div id="logo">
<a href="https://precisereportingservices.net/">
<img alt="Precise Reporting Services" id="logo-regular" src="http://precisereportingservices.net/wp-content/uploads/2015/08/PRSnewHorizontal2-e1438793101381.png"/>
<img alt="Precise Reporting Services" id="logo-hidpi" src="https://precisereportingservices.net/wp-content/themes/C7Creative/styles/light/images/logo-hidpi.png"/>
</a>
</div>
<div id="top-right">
<div id="top-right-inner">
<div id="top-right-content">
<div id="tagline">
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Menu Start -->
<nav id="header-menu">
<div id="header-menu-inner">
<ul class="sf-menu" id="header-menu-links"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-903" id="menu-item-903"><a href="http://precisereportingservices.net">Home</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1037" id="menu-item-1037"><a href="#">About</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1040" id="menu-item-1040"><a href="https://precisereportingservices.net/leadership-team/">Leadership Team</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1029" id="menu-item-1029"><a href="https://precisereportingservices.net/company/">Company Overview</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1028" id="menu-item-1028"><a href="https://precisereportingservices.net/services/">Services</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1030" id="menu-item-1030"><a href="#">Resources</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1027" id="menu-item-1027"><a href="https://precisereportingservices.net/testimonials/">Testimonials</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1026" id="menu-item-1026"><a href="https://precisereportingservices.net/faqs/">FAQs</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1024" id="menu-item-1024"><a href="https://precisereportingservices.net/referral-program/">Referral Program</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-912" id="menu-item-912"><a href="https://precisereportingservices.net/contact/">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1023" id="menu-item-1023"><a href="https://precisereportingservices.net/order-transcripts/">Order Transcripts</a></li>
</ul>
<ul class="risen-list-font-icons" id="header-icons">
<li><a class="risen-font-icon-facebook" href="https://www.facebook.com/pages/Precise-Reporting-Services-DC-MD-VA-FL/170817082964944?fref=ts" target="_blank" title="Facebook"></a></li>
</ul>
<div class="clear"></div>
</div>
<div id="header-menu-bottom"></div>
</nav>
<!-- Menu End -->
</header>
<!-- Header End -->
<div class="breadcrumbs"><a href="https://precisereportingservices.net">Home</a> &gt; <a href="https://precisereportingservices.net/wp-modules/christianmingle/logon/login.php%09">Not Found</a></div>
<div id="content">
<div id="content-inner">
<article>
<header>
<h1 class="page-title">Not Found</h1>
</header>
<div class="post-content"> <!-- confines heading font to this content -->
<p>
					The page or file you tried to access was not found.	
				</p>
</div>
</article>
</div>
</div>
<!-- Footer Start -->
<footer id="footer">
<div id="footer-left">
<ul class="menu" id="footer-menu-links"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-903"><a href="http://precisereportingservices.net">Home</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1037"><a href="#">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1028"><a href="https://precisereportingservices.net/services/">Services</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1030"><a href="#">Resources</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-912"><a href="https://precisereportingservices.net/contact/">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1023"><a href="https://precisereportingservices.net/order-transcripts/">Order Transcripts</a></li>
</ul>
<ul class="risen-list-font-icons" id="footer-icons">
<li><a class="risen-font-icon-facebook" href="https://www.facebook.com/pages/Precise-Reporting-Services-DC-MD-VA-FL/170817082964944?fref=ts" target="_blank" title="Facebook"></a></li>
</ul>
<div class="clear"></div>
</div>
<div id="footer-right">
<ul id="footer-contact">
<li><span class="footer-icon church"></span> 820 North A1A, Suite 100 | Ponte Beach, FL 32082</li>
<li><span class="footer-icon phone"></span> 1 (877) 4 A STENO - 1 (877) 427-8366</li>
</ul>
<div id="copyright">
<h6>(904) 373-0175<br/>(301) 742-9880<br/>Mobile (301) 210-5092</h6>
<p>© 2021 All Rights Reserved. Site Launched by <i class="icon-rocket"></i> <a href="https://www.c7creative.com/" rel="nofollow">C7Creative.com</a><br/> </p>
<p style="font-size: 8px"><a href="mailto:Marketinghero@goc7.com" rel="noopener" target="_blank"> Questions? Need Support? 904.395.1944</a></p> </div>
</div>
<div class="clear"></div>
</footer>
<div id="footer-bottom"></div>
<!-- Footer End -->
</div>
</div>
<!-- Container End -->
<script type="text/javascript">
/* <![CDATA[ */
var ub_cookie_notice = {"id":"#branda-cookie-notice","cookie":{"domain":"","name":"Branda_Cookie_Notice_1","path":"\/","secure":"on","timezone":0,"value":2592000},"reloading":"off","animation":null,"ajaxurl":"https:\/\/precisereportingservices.net\/wp-admin\/admin-ajax.php","logged":"no","user_id":"0","nonce":"ea982d2c64"};
/* ]]> */
</script>
<script src="https://precisereportingservices.net/wp-content/plugins/ultimate-branding/inc/modules/front-end/assets/js/cookie-notice-front.js?ver=3.4.3" type="text/javascript"></script>
<script src="https://precisereportingservices.net/wp-includes/js/hoverIntent.min.js?ver=1.8.1" type="text/javascript"></script>
<script src="https://precisereportingservices.net/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
<div class="ub-position-bottom ub-style-none" id="branda-cookie-notice" role="banner"><div class="cookie-notice-container"><div class="branda-cn-container"><span class="branda-cn-column" id="ub-cn-notice-text"><img alt="" class="wp-image-280 alignleft c7icon" height="29" src="https://www.c7creative.com/wp-content/uploads/2019/07/C7-Creative.png" width="28"/>
<p class="cookie-text" style="display: inline-block; margin-left: 40px; margin-top: 8px;">Nice to meet you. We use cookies to ensure that we give you the best experience on our website.</p></span><span class="branda-cn-column"><a class="button ub-cn-set-cookie" href="#">Sounds Good</a></span></div></div></div>
</body>
</html>