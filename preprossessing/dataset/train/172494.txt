<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
    //<![CDATA[
    (function(){
      window._cf_chl_opt={
        cvId: "1",
        cType: "interactive",
        cNounce: "23608",
        cRay: "6119c6fdfa460bc0",
        cHash: "bbef78d9d96a518",
        cFPWv: "b",
        cRq: {
          ru: "aHR0cHM6Ly93d3cuYXV0by1kb2MuaXQv",
          ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
          rm: "R0VU",
          d: "8LLJb6hHM3N8D8iZFTy8K/+FcJxbVWYqfdnruY3tRMw/2/uYDyQbCYFBcQf7FahF7bVdxYMTHH2RkaMWjfiIGfBEENGR4kKz1OgfP6zrnk05bvA1g17JNCYOScE/z04dH8VWhM+l7BUdvMcvCAe3Zholwz+yg8+NENsupWT8z6qxSuxYFBNYDKuP/KKAjqu5O+t+T84C0lsUoPP5dFB0T4tfC9HAiKc+2HxtxLNEF/idjbIcFXcPidVRoiLNLJlL+ghPQCTK7u4fnaZcdCOPgnNM8jfSB9gcl/WSyHQyCdd7Lkn92atNRDOdnzY9iv2gxlvIBvBNuKpyo5eJlEc8OY7P7EsZJgHYGokGWLbSqkgJMfCFbS7yLmYAjP111vxFYlPPaEZD38nkLn3RYlyiheD4B5jAjEmXY1e4y+YHj1+uam/BUEEiVezc+U0o34ZW8x93gDYuWz7vKHR3nzMCbGjwITh2ArESjHjRjvjRZJDp41hrZvvC4vWrt3tTqOOZIY37IS3W83/3pUpURCJBepjUSEVXZwhLStYynJ76d/UyPbitxyWz9yYomWvAcs/bq+6XpWwXjTnAi3s1uWeqTsRFoGKsftN0zDjbs7poOXE0NfythZT+e5uU7uZX2ooJGheiu4OLErH+T2tdG0wsWt+0ZxHa5G91geNSip6N0ndok99sNlDXTxJKZkTrlYNI5/NoNyOtIayLGWZOuzavqjJa306y3l1hPxC7StWhVMup5K9BOTAU6wC4vdKoohFBDPiZTYJdcfmWhG0hVIDVUQ==",
          t: "MTYxMDY1MjY4Ny4wNDIwMDA=",
          m: "UnM8SHOlo9bOhJvsf/BY5oeJJkduda+1scLfqCE19tw=",
          i1: "wPv7DR5vES1L6F7qvBrWvw==",
          i2: "fxtOW+6qVDvQhBmpQ3bdQw==",
          uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
          hh: "DSgSLZEPxG4VOLNgxQobCtOJ3eiPZNZfnlcHkmrKils=",
        }
      }
      window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
      var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
        var cpo = document.createElement('script');
        cpo.type = 'text/javascript';
        cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
        var done = false;
        cpo.onload = cpo.onreadystatechange = function() {
          if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
            done = true;
            cpo.onload = cpo.onreadystatechange = null;
            window._cf_chl_enter()
          }
        };
        document.getElementsByTagName('head')[0].appendChild(cpo);
      }, false);
    })();
    //]]>
  </script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.auto-doc.it</h2>
</div>
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=d51480c01ca66115b2fe9e42551f24905aee9e0c-1610652687-0-AYpr2D-Izdm948AX5tMLLik_Bi-2rPIH-87t6BHD3GhLa1zgfWl5LiMNoi6J5CY4FQ6NBLXiVVw4n52tLjwMqDZOt_7uz5f51lQichxmp4lhe1ENP_jjYQYKil9wZ30Uv1VXQktZ8R2zIovpabKUDM-22RzoDltvOjdfivQfrdhGx0ArInGf_eIFmJ1YZQza4ZAL2rfupHp_7NpqgDF38sZ1_YiK1cm5Gx_2_GwfN3_eC2Kzg03YlGDgpR9uvtsbnPIHW3mllPQTD5UmKVFFapOXr5Nq4Rxu2DxfQmKuPRnSLLSnfkmcYTSq2kso03iIMLemoBxroQMs52grGOgwlm6j8r0ovlU3ZqxUB9t6xBs5eRs4CaTT1Dh0VwewgkUSaVMFdR5OmOABnynQCWzDFs1cAKRyZlX4AyTkbEEhqj68X9_33CDE7FSpXikNblpL2n_Q62cecyMQYCr0FvDO8W4H93z4QBDTJYz7KkVZzJKg7avzflRk4jLrh7A7-OLvMRery61Oi7JpUGAUN_69b9yyLMXw2N3g4w0EY1aaCJt3l6I5cjcSMaObBrd_6kYOW61dgs5sfcxxpG4y6z68gX8" class="challenge-form interactive-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="ccae4f34f4786e450ca828156658697717dcadba-1610652687-0-AVgyueQz0s+Yk5UaSvzWLdxHmA3bahGqWajcP8Ok5rQbjPYQBAKUBL6KmiXvnZynHqSfMiHI5r/7KSq6YFxqksTE4mw+78Ok114LVsQ/62Dz6fHTxcfVq0QHq396KQZRO9f9dV+482R2i/YV61AM/ry9BgSi4BlrgMuoUqMM88MFMROWqDr5GUH01Ukr6nx2ythrqAOtr10RZH2tAmj8songHVjbx7A03Wju20k+V/2cUBZGgxmlCHRsDhxE6ed2S7OEL3litXFE3fEl4Ibx9CdTG3PMLNsWHuqp/lZc8aOr5CKacdQ5HLfydwKkmtk6ZCyuSSJw/NhvXfYQ1PSzh62GpO6Tsdswy45ikhTOWnAnzF/yWWce34L8BJqSHIieqZmaHsIGhtboQINeFGVFa6yA0iDyDwEs5VkkLRWVmRUc5z1skwav/ePP/tuH8C+ETtA8f+rl1QwY0CDUSF/pVryE5DtXXQV3D3TNGR5Sz/mf4gya/0K/mahVOYdxuPPWx/xI8mToDfHr26+JBxWw7e1EBhy2n2vlptiZsJYu/N/m6IphPoMwFc/+PRJKddXog1uXywKseiU9yx/YNIVLhssSlmmMvetvtTf4LJXCKkjJwymRWOPz/YGAG7dlc5H3VTm1AppAmoVDODyVKEPE0oS+4fyWAkL/nqgs+fcwOxJCLZXIGgYscrfPpoba3ns6yYTlU1lE2YcDXC6F6UvUlnHRf9Ls+1Ya7KJE68atR5XeIzH9Av9TUz8xTzrV6PPxjLtopD/wRuZe2xiHeEMndON+GibJL4qFIt0nivnWcfeyJVK+JXlTBgRdfB5ukDEdJ5CyPCxwpv5Fmc0Ih1xZZR9yruBG1e9etu7dNPIiPQXHEXybzqAli2qy8rJRdrHHJwa1ITb0ckrtbuKOkl7cgbSyL9/ErdVzp5hYDBdccBKh2AFS6yS2xogms4UipF6F7HsgHJqDrevIK7d/mym2/lycfBc0bLfYh+w3uHtx7PeF2U7Sc4Ehs0xh2wlgUdqZ6eOi4dXfgmXsdLr8qnaiy/zw/jBbvR/MH+IsQEze+2JsjmIFUVDjb9ITwxXcHF1scvnjzQX3OY3zZ4eWt1bcT05UVPhMAJaFH8JAQ8zyQJsxc6IwntEtDVwzMR2DYdM+CgGFL+pC6hF+DHbnh7U+SIaARJ1YxP3d520oJpajRDbsaa8jM9/O305J0gPqZBn1aIPC4U/YCrDsk1CGVejrfuT7Oito4t+51sjko1N3Y/XHQCg/iQqQ4pnbzYapo0YSbB5Ydm4l5QJVLjXtRR4Pa68H220gONunRciAPj0sBXaOUaVOxrzUg4iwf1VVgxg0+A=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="78e91ffd201bef66c42d41e1d597f92a"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=6119c6fdfa460bc0')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div>
</div>
</div>
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div>
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">6119c6fdfa460bc0</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:e444:4db8:6cea:7c56</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div>
</div>
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
