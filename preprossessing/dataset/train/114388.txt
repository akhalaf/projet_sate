<!DOCTYPE html>
<html class="a-no-js" data-19ax5a9jf="dingo">
<head><script>var aPageStart = (new Date()).getTime();</script><meta charset="utf-8"/>
<title>Amazon Unlock</title>
<link href="/resources/images/apple-touch-icon.png" rel="apple-touch-icon"/>
<meta content="noindex, nofollow, noarchive" name="robots"/>
<meta content="width=device-width, initial-scale=0.8" name="viewport"/>
<link href="https://images-na.ssl-images-amazon.com/images/I/01nG0rwV7BL.css?AUIClients/HorizonteVirtuaCopExternalAssets" rel="stylesheet"/>
<link href="https://images-na.ssl-images-amazon.com/images/I/11EIQ5IGqaL._RC|012LjolmrML.css,41-crZfIjzL.css,11cMnOipjJL.css,017DsKjNQJL.css,01Vctty9pOL.css,01zAp95w-aL.css,41EWOOlBJ9L.css,11rkuLsEFQL.css,01ElnPiDxWL.css,11QxHU4QYaL.css,01Sp8sB1HiL.css,01IdKcBuAdL.css,01y-XAlI+2L.css,01evdoiemkL.css,01K+Ps1DeEL.css,314djKvMsUL.css,01ZTetsDh7L.css,01pbA9Lg3yL.css,21LK7jaicML.css,11L58Qpo0GL.css,21kyTi1FabL.css,01ruG+gDPFL.css,01YhS3Cs-hL.css,21GwE3cR-yL.css,11KLBtpWIAL.css,11GQQ8NbhuL.css,11M4XwS6hxL.css,11WgRxUdJRL.css,01dU8+SPlFL.css,11ocrgKoE-L.css,11k89RclloL.css,111-D2qRjiL.css,01QrWuRrZ-L.css,21xBJi4P1GL.css,01M3ZzSySfL.css,01gAR5pB+IL.css,119dKrtBoVL.css,01piEq-AdwL.css,11Z1a0FxSIL.css,01cbS3UK11L.css,21B5OR-vv2L.css,01giMEP+djL.css_.css?AUIClients/AmazonUI#us.not-trident.282109-T1.278290-T2" rel="stylesheet"/>
<script>
(function(f,h,Q,E){function F(a){v&&v.tag&&v.tag(q(":","aui",a))}function w(a,b){v&&v.count&&v.count("aui:"+a,0===b?0:b||(v.count("aui:"+a)||0)+1)}function m(a){try{return a.test(navigator.userAgent)}catch(b){return!1}}function x(a,b,c){a.addEventListener?a.addEventListener(b,c,!1):a.attachEvent&&a.attachEvent("on"+b,c)}function q(a,b,c,e){b=b&&c?b+a+c:b||c;return e?q(a,b,e):b}function G(a,b,c){try{Object.defineProperty(a,b,{value:c,writable:!1})}catch(e){a[b]=c}return c}function va(a,b){var c=a.length,
e=c,g=function(){e--||(R.push(b),S||(setTimeout(ca,0),S=!0))};for(g();c--;)da[a[c]]?g():(A[a[c]]=A[a[c]]||[]).push(g)}function wa(a,b,c,e,g){var d=h.createElement(a?"script":"link");x(d,"error",e);g&&x(d,"load",g);a?(d.type="text/javascript",d.async=!0,c&&/AUIClients|images[/]I/.test(b)&&d.setAttribute("crossorigin","anonymous"),d.src=b):(d.rel="stylesheet",d.href=b);h.getElementsByTagName("head")[0].appendChild(d)}function ea(a,b){return function(c,e){function g(){wa(b,c,d,function(b){T?w("resource_unload"):
d?(d=!1,w("resource_retry"),g()):(w("resource_error"),a.log("Asset failed to load: "+c));b&&b.stopPropagation?b.stopPropagation():f.event&&(f.event.cancelBubble=!0)},e)}if(fa[c])return!1;fa[c]=!0;w("resource_count");var d=!0;return!g()}}function xa(a,b,c){for(var e={name:a,guard:function(c){return b.guardFatal(a,c)},logError:function(c,d,e){b.logError(c,d,e,a)}},g=[],d=0;d<c.length;d++)H.hasOwnProperty(c[d])&&(g[d]=U.hasOwnProperty(c[d])?U[c[d]](H[c[d]],e):H[c[d]]);return g}function B(a,b,c,e,g){return function(d,
h){function n(){var a=null;e?a=h:"function"===typeof h&&(p.start=C(),a=h.apply(f,xa(d,k,l)),p.end=C());if(b){H[d]=a;a=d;for(da[a]=!0;(A[a]||[]).length;)A[a].shift()();delete A[a]}p.done=!0}var k=g||this;"function"===typeof d&&(h=d,d=E);b&&(d=d?d.replace(ha,""):"__NONAME__",V.hasOwnProperty(d)&&k.error(q(", reregistered by ",q(" by ",d+" already registered",V[d]),k.attribution),d),V[d]=k.attribution);for(var l=[],m=0;m<a.length;m++)l[m]=a[m].replace(ha,"");var p=ia[d||"anon"+ ++ya]={depend:l,registered:C(),
namespace:k.namespace};c?n():va(l,k.guardFatal(d,n));return{decorate:function(a){U[d]=k.guardFatal(d,a)}}}}function ja(a){return function(){var b=Array.prototype.slice.call(arguments);return{execute:B(b,!1,a,!1,this),register:B(b,!0,a,!1,this)}}}function W(a,b){return function(c,e){e||(e=c,c=E);var g=this.attribution;return function(){y.push(b||{attribution:g,name:c,logLevel:a});var d=e.apply(this,arguments);y.pop();return d}}}function I(a,b){this.load={js:ea(this,!0),css:ea(this)};G(this,"namespace",
b);G(this,"attribution",a)}function ka(){h.body?t.trigger("a-bodyBegin"):setTimeout(ka,20)}function D(a,b){a.className=X(a,b)+" "+b}function X(a,b){return(" "+a.className+" ").split(" "+b+" ").join(" ").replace(/^ | $/g,"")}function la(a){try{return a()}catch(b){return!1}}function J(){if(K){var a={w:f.innerWidth||n.clientWidth,h:f.innerHeight||n.clientHeight};5<Math.abs(a.w-Y.w)||50<a.h-Y.h?(Y=a,L=4,(a=k.mobile||k.tablet?450<a.w&&a.w>a.h:1250<=a.w)?D(n,"a-ws"):n.className=X(n,"a-ws")):0<L&&(L--,ma=
setTimeout(J,16))}}function za(a){(K=a===E?!K:!!a)&&J()}function Aa(){return K}function u(a,b){return"sw:"+(b||"")+":"+a+":"}function na(){oa.forEach(function(a){F(a)})}function p(a){oa.push(a)}function pa(a,b,c,e){if(c){b=m(/Chrome/i)&&!m(/Edge/i)&&!m(/OPR/i)&&!a.capabilities.isAmazonApp&&!m(new RegExp(Z+"bwv"+Z+"b"));var g=u(e,"browser"),d=u(e,"prod_mshop"),f=u(e,"beta_mshop");!a.capabilities.isAmazonApp&&c.browser&&b&&(p(g+"supported"),c.browser.action(g,e));!b&&c.browser&&p(g+"unsupported");c.prodMshop&&
p(d+"unsupported");c.betaMshop&&p(f+"unsupported")}}"use strict";var M=Q.now=Q.now||function(){return+new Q},C=function(a){return a&&a.now?a.now.bind(a):M}(f.performance),N=C(),r=f.AmazonUIPageJS||f.P;if(r&&r.when&&r.register){N=[];for(var l=h.currentScript;l;l=l.parentElement)l.id&&N.push(l.id);return r.log("A copy of P has already been loaded on this page.","FATAL",N.join(" "))}var v=f.ue;F();F("aui_build_date:3.20.6-2020-08-22");var R=[],S=!1;var ca=function(){for(var a=setTimeout(ca,0),b=M();R.length;)if(R.shift()(),
50<M()-b)return;clearTimeout(a);S=!1};var da={},A={},fa={},T=!1;x(f,"beforeunload",function(){T=!0;setTimeout(function(){T=!1},1E4)});var ha=/^prv:/,V={},H={},U={},ia={},ya=0,Z=String.fromCharCode(92),y=[],qa=f.onerror;f.onerror=function(a,b,c,e,g){g&&"object"===typeof g||(g=Error(a,b,c),g.columnNumber=e,g.stack=b||c||e?q(Z,g.message,"at "+q(":",b,c,e)):E);var d=y.pop()||{};g.attribution=q(":",g.attribution||d.attribution,d.name);g.logLevel=d.logLevel;g.attribution&&console&&console.log&&console.log([g.logLevel||
"ERROR",a,"thrown by",g.attribution].join(" "));y=[];qa&&(d=[].slice.call(arguments),d[4]=g,qa.apply(f,d))};I.prototype={logError:function(a,b,c,e){b={message:b,logLevel:c||"ERROR",attribution:q(":",this.attribution,e)};if(f.ueLogError)return f.ueLogError(a||b,a?b:null),!0;console&&console.error&&(console.log(b),console.error(a));return!1},error:function(a,b,c,e){a=Error(q(":",e,a,c));a.attribution=q(":",this.attribution,b);throw a;},guardError:W(),guardFatal:W("FATAL"),guardCurrent:function(a){var b=
y[y.length-1];return b?W(b.logLevel,b).call(this,a):a},log:function(a,b,c){return this.logError(null,a,b,c)},declare:B([],!0,!0,!0),register:B([],!0),execute:B([]),AUI_BUILD_DATE:"3.20.6-2020-08-22",when:ja(),now:ja(!0),trigger:function(a,b,c){var e=M();this.declare(a,{data:b,pageElapsedTime:e-(f.aPageStart||NaN),triggerTime:e});c&&c.instrument&&O.when("prv:a-logTrigger").execute(function(b){b(a)})},handleTriggers:function(){this.log("handleTriggers deprecated")},attributeErrors:function(a){return new I(a)},
_namespace:function(a,b){return new I(a,b)}};var t=G(f,"AmazonUIPageJS",new I);var O=t._namespace("PageJS","AmazonUI");O.declare("prv:p-debug",ia);t.declare("p-recorder-events",[]);t.declare("p-recorder-stop",function(){});G(f,"P",t);ka();if(h.addEventListener){var ra;h.addEventListener("DOMContentLoaded",ra=function(){t.trigger("a-domready");h.removeEventListener("DOMContentLoaded",ra,!1)},!1)}var n=h.documentElement,aa=function(){var a=["O","ms","Moz","Webkit"],b=h.createElement("div");return{testGradients:function(){b.style.cssText=
"background-image:-webkit-gradient(linear,left top,right bottom,from(#1E4),to(white));background-image:-webkit-linear-gradient(left top,#1E4,white);background-image:linear-gradient(left top,#1E4,white);";return~b.style.backgroundImage.indexOf("gradient")},test:function(c){var e=c.charAt(0).toUpperCase()+c.substr(1);c=(a.join(e+" ")+e+" "+c).split(" ");for(e=c.length;e--;)if(""===b.style[c[e]])return!0;return!1},testTransform3d:function(){var a=!1;f.matchMedia&&(a=f.matchMedia("(-webkit-transform-3d)").matches);
return a}}}();r=n.className;var sa=/(^| )a-mobile( |$)/.test(r),ta=/(^| )a-tablet( |$)/.test(r),k={audio:function(){return!!h.createElement("audio").canPlayType},video:function(){return!!h.createElement("video").canPlayType},canvas:function(){return!!h.createElement("canvas").getContext},svg:function(){return!!h.createElementNS&&!!h.createElementNS("http://www.w3.org/2000/svg","svg").createSVGRect},offline:function(){return navigator.hasOwnProperty&&navigator.hasOwnProperty("onLine")&&navigator.onLine},
dragDrop:function(){return"draggable"in h.createElement("span")},geolocation:function(){return!!navigator.geolocation},history:function(){return!(!f.history||!f.history.pushState)},webworker:function(){return!!f.Worker},autofocus:function(){return"autofocus"in h.createElement("input")},inputPlaceholder:function(){return"placeholder"in h.createElement("input")},textareaPlaceholder:function(){return"placeholder"in h.createElement("textarea")},localStorage:function(){return"localStorage"in f&&null!==
f.localStorage},orientation:function(){return"orientation"in f},touch:function(){return"ontouchend"in h},gradients:function(){return aa.testGradients()},hires:function(){var a=f.devicePixelRatio&&1.5<=f.devicePixelRatio||f.matchMedia&&f.matchMedia("(min-resolution:144dpi)").matches;w("hiRes"+(sa?"Mobile":ta?"Tablet":"Desktop"),a?1:0);return a},transform3d:function(){return aa.testTransform3d()},touchScrolling:function(){return m(/Windowshop|android|OS ([5-9]|[1-9][0-9]+)(_[0-9]{1,2})+ like Mac OS X|Chrome|Silk|Firefox|Trident.+?; Touch/i)},
ios:function(){return m(/OS [1-9][0-9]*(_[0-9]*)+ like Mac OS X/i)&&!m(/trident|Edge/i)},android:function(){return m(/android.([1-9]|[L-Z])/i)&&!m(/trident|Edge/i)},mobile:function(){return sa},tablet:function(){return ta},rtl:function(){return"rtl"===n.dir}};for(l in k)k.hasOwnProperty(l)&&(k[l]=la(k[l]));for(var ba="textShadow textStroke boxShadow borderRadius borderImage opacity transform transition".split(" "),P=0;P<ba.length;P++)k[ba[P]]=la(function(){return aa.test(ba[P])});var K=!0,ma=0,Y=
{w:0,h:0},L=4;J();x(f,"resize",function(){clearTimeout(ma);L=4;J()});var ua={getItem:function(a){try{return f.localStorage.getItem(a)}catch(b){}},setItem:function(a,b){try{return f.localStorage.setItem(a,b)}catch(c){}}};n.className=X(n,"a-no-js");D(n,"a-js");!m(/OS [1-8](_[0-9]*)+ like Mac OS X/i)||f.navigator.standalone||m(/safari/i)||D(n,"a-ember");r=[];for(l in k)k.hasOwnProperty(l)&&k[l]&&r.push("a-"+l.replace(/([A-Z])/g,function(a){return"-"+a.toLowerCase()}));D(n,r.join(" "));n.setAttribute("data-aui-build-date",
"3.20.6-2020-08-22");t.register("p-detect",function(){return{capabilities:k,localStorage:k.localStorage&&ua,toggleResponsiveGrid:za,responsiveGridEnabled:Aa}});m(/UCBrowser/i)||k.localStorage&&D(n,ua.getItem("a-font-class"));t.declare("a-event-revised-handling",!1);try{var z=navigator.serviceWorker}catch(a){F("sw:nav_err")}z&&(x(z,"message",function(a){a&&a.data&&w(a.data.k,a.data.v)}),z.controller&&z.controller.postMessage("MSG-RDY"));var oa=[];(function(a){var b=a.reg,c=a.unreg;z&&z.getRegistrations?
(O.when("A","a-util").execute(function(a,b){pa(a,b,c,"unregister")}),x(f,"load",function(){O.when("A","a-util").execute(function(a,c){pa(a,c,b,"register");na()})})):(b&&(b.browser&&p(u("register","browser")+"unsupported"),b.prodMshop&&p(u("register","prod_mshop")+"unsupported"),b.betaMshop&&p(u("register","beta_mshop")+"unsupported")),c&&(c.browser&&p(u("unregister","browser")+"unsupported"),c.prodMshop&&p(u("unregister","prod_mshop")+"unsupported"),c.betaMshop&&p(u("unregister","beta_mshop")+"unsupported")),
na())})({reg:{},unreg:{}});t.declare("a-fix-event-off",!1);w("pagejs:pkgExecTime",C()-N)})(window,document,Date);
  (window.AmazonUIPageJS ? AmazonUIPageJS : P).load.js('https://images-na.ssl-images-amazon.com/images/I/21xPpfkQ+VL.js?AUIClients/HorizonteVirtuaCopExternalAssets');
  (window.AmazonUIPageJS ? AmazonUIPageJS : P).load.js('https://images-na.ssl-images-amazon.com/images/I/61-6nKPKyWL._RC|11Y+5x+kkTL.js,61bnsosVEYL.js,212PEt8u8bL.js,11KoZmq92cL.js,51TNaPzHULL.js,11AHlQhPRjL.js,01Gpt4sPPhL.js,11OREnu1epL.js,11p81T3qWFL.js,21r53SJg7LL.js,0190vxtlzcL.js,51xpo+OFSiL.js,3139553HcbL.js,015c-6CIP9L.js,01ezj5Rkz1L.js,11EemQQsS-L.js,31pOTH2ZMRL.js,01rpauTep4L.js,01iyxuSGj4L.js,01poskbWxPL.js_.js?AUIClients/AmazonUI');
</script>
<!-- Snowplow starts plowing -->
<!-- From: https://github.com/snowplow/snowplow/wiki/1-General-parameters-for-the-Javascript-tracker#loading -->
<script async="" type="text/javascript">
  ;(function(p,l,o,w,i,n,g){if(!p[i]){p.GlobalSnowplowNamespace=p.GlobalSnowplowNamespace||[];
  p.GlobalSnowplowNamespace.push(i);p[i]=function(){(p[i].q=p[i].q||[]).push(arguments)
  };p[i].q=p[i].q||[];n=l.createElement(o);g=l.getElementsByTagName(o)[0];n.async=1;
  n.src=w;g.parentNode.insertBefore(n,g)}}(window,document,"script","/resources/js/sp-2.10.0.js","snowplow"));
  </script>
</head>
<body class="a-m-us a-aui_72554-c a-aui_control_group_273125-t1 a-aui_dropdown_274033-c a-aui_link_rel_noreferrer_noopener_274172-c a-aui_pci_risk_banner_210084-c a-aui_perf_130093-c a-aui_preload_261698-c a-aui_tnr_v2_180836-c a-aui_ux_145937-c"><div id="a-page"><script data-a-state='{"key":"a-wlab-states"}' type="a-state">{"AUI_PRELOAD_261698":"C","AUI_CONTROL_GROUP_273125":"T1"}</script>
<div class="a-row">
<div class="a-column a-span4 unlock-box-margin1"></div>
<div class="a-column a-span3 a-spacing-top-medium unlock-box" id="unlockBox">
<div class="a-spacing-small">
</div>
<br/>
<h1 class="a-text-center">Unlock Your Account</h1>
<br/>
<div class="a-box"><div class="a-box-inner">
<form action="/" id="unlockForm" method="post">
<div class="a-row a-spacing-top-base a-grid-vertical-align a-grid-center">
<div class="a-column a-span12 a-text-center">
            Unlock your Amazon employee account with your security key.
        </div>
</div>
<div class="a-row a-spacing-top-base a-grid-vertical-align a-grid-center">
        Enter your username :
    </div>
<div class="a-row a-spacing-top-mini a-grid-vertical-align a-grid-center">
<input autocomplete="off" class="a-input-text a-width-large" id="userId" name="userId" type="text"/>
</div>
<div class="a-row a-grid-vertical-align a-grid-center">
<div class="unlock-form-error" id="userIdError" style="display: none;">
<div aria-live="assertive" class="a-box a-alert-inline a-alert-inline-error" role="alert"><div class="a-box-inner a-alert-container"><i class="a-icon a-icon-alert"></i><div class="a-alert-content">
                Please enter a value.
            </div></div></div>
</div>
<div class="unlock-form-error" id="userIdInvalidError" style="display: none;">
<div aria-live="assertive" class="a-box a-alert-inline a-alert-inline-error" role="alert"><div class="a-box-inner a-alert-container"><i class="a-icon a-icon-alert"></i><div class="a-alert-content">
                Must contain only alphabets.
            </div></div></div>
</div>
</div>
<div class="a-row a-spacing-top-base a-grid-vertical-align a-grid-center">
        How would you like to be authenticated?
    </div>
<div class="a-row a-spacing-top-mini">
<label class="a-radio" data-a-input-name="operationMode" id="operationMode">
<input checked="true" class="radioOperation" id="gemalto" name="gemaltoFlag" type="radio" value="true"/>
<span class="a-radio-label"> Security Key </span>
</label>
<label class="a-radio" data-a-input-name="operationMode" id="operationMode">
<input class="radioOperation" id="sms" name="gemaltoFlag" type="radio" value="false"/>
<span class="a-radio-label"> One-time password on Mobile </span>
<span class="a-button a-button-primary a-button-small smsBlock sms-button" id="smsButton"><span class="a-button-inner"><button class="a-button-text" id="nativeSMSButton" type="button">
                Get code
            </button></span></span>
<span class="a-declarative smsBlock sms-info" data-a-popover='{"position":"triggerRight","content":"We will send one-time password to the mobile number in your employee profile. It may take a few minutes."}' data-action="a-popover" style="display: none;">
<a class="a-popover-trigger a-declarative">
<span class="aui-info-image"></span>
</a>
</span>
</label>
</div>
<div class="a-row smsBlock">
<div id="smsMessage">
<span id="smsSendingMessage" style="display: none;">
<span class="ajax-loader-image"></span> Sending a one-time password. 
            </span>
<div id="smsSuccessMessage" style="display: none;">
<div class="a-box a-alert-inline a-alert-inline-success"><div class="a-box-inner a-alert-container"><i class="a-icon a-icon-alert"></i><div class="a-alert-content">
                    One-time password has been sent to <span id="phoneNumber"></span>
</div></div></div>
</div>
<div id="smsErrorMessage" style="display: none;">
<div aria-live="assertive" class="a-box a-alert-inline a-alert-inline-error" role="alert"><div class="a-box-inner a-alert-container"><i class="a-icon a-icon-alert"></i><div class="a-alert-content">
                    There was an issue sending the one-time password. Please check input and try again.
                </div></div></div>
</div>
</div>
</div>
<div class="a-row a-spacing-top-base a-grid-vertical-align a-grid-center">
        Enter the text from the image below :
    </div>
<div class="a-row a-spacing-base a-spacing-top-base a-grid-vertical-align a-grid-center" style="height:80;">
<div class="a-column a-span6 a-text-left" id="captchaBlock"></div>
<div class="a-column a-span6 a-text-left a-span-last">
<span class="a-declarative" data-action="refreshCaptcha" data-refreshcaptcha="{}">
<a>
<div alttext="Get another image" id="captchaButton"></div>
</a>
</span>
</div>
</div>
<div class="a-row a-spacing-top-mini">
<input autocomplete="off" class="a-input-text a-width-large" id="captchaSolution" name="captchaSolution" type="text"/>
</div>
<div class="a-row a-grid-vertical-align a-grid-center">
<div class="unlock-form-error" id="captchaSolutionError" style="display: none;">
<div aria-live="assertive" class="a-box a-alert-inline a-alert-inline-error" role="alert"><div class="a-box-inner a-alert-container"><i class="a-icon a-icon-alert"></i><div class="a-alert-content">
                Please enter a value.
            </div></div></div>
</div>
</div>
<div class="a-row a-grid-vertical-align a-grid-center">
<div class="unlock-form-error" id="captchaSolutionInvalid" style="display:none;">
<div aria-live="assertive" class="a-box a-alert-inline a-alert-inline-error" role="alert"><div class="a-box-inner a-alert-container"><i class="a-icon a-icon-alert"></i><div class="a-alert-content">
                Input text does not match image text.
            </div></div></div>
</div>
</div>
<div class="a-row a-spacing-top-base gemaltoBlock">
        Enter your security key pin and one-time password* :
    </div>
<div class="a-row a-spacing-top-mini gemaltoBlock">
<input autocomplete="off" class="a-input-text a-width-large" id="gemaltoToken" name="gemaltoToken" type="password"/>
</div>
<div class="a-row gemaltoBlock">
<div class="unlock-form-error" id="gemaltoTokenError" style="display: none;">
<div aria-live="assertive" class="a-box a-alert-inline a-alert-inline-error" role="alert"><div class="a-box-inner a-alert-container"><i class="a-icon a-icon-alert"></i><div class="a-alert-content">
                Please enter a value.
            </div></div></div>
</div>
<div class="unlock-form-error" id="gemaltoTokenErrorCaps" style="display: none;">
<div class="a-box a-alert-inline a-alert-inline-info"><div class="a-box-inner a-alert-container"><i class="a-icon a-icon-alert"></i><div class="a-alert-content">
                    Caps Lock is active.
                </div></div></div>
</div>
</div>
<div class="a-row a-spacing-top-base smsBlock">
        Enter the one-time password* :
    </div>
<div class="a-row a-spacing-top-mini smsBlock">
<input autocomplete="off" class="a-input-text a-width-large" id="smsToken" name="smsToken" type="password"/>
</div>
<div class="a-row smsBlock">
<div class="unlock-form-error" id="smsTokenError" style="display: none;">
<div aria-live="assertive" class="a-box a-alert-inline a-alert-inline-error" role="alert"><div class="a-box-inner a-alert-container"><i class="a-icon a-icon-alert"></i><div class="a-alert-content">
                Please enter a value.
            </div></div></div>
</div>
<div class="unlock-form-error" id="smsTokenErrorCaps" style="display: none;">
<div class="a-box a-alert-inline a-alert-inline-info"><div class="a-box-inner a-alert-container"><i class="a-icon a-icon-alert"></i><div class="a-alert-content">
                Caps Lock is active.
            </div></div></div>
</div>
</div>
<div class="a-row a-spacing-top-base gemaltoBlock">
        *To generate the one-time password, type your security key PIN and press your security key.
    </div>
<div class="a-row a-spacing-top-base smsBlock">
            *Please enter the one-time password you just received on your mobile device.
        </div>
<input id="captchaToken" name="captchaToken" type="hidden"/>
<input id="captchaValid" name="captchaValid" type="hidden" value="true"/>
<div class="a-form-actions a-spacing-top-base a-text-center">
<span class="a-button a-button-primary" id="unlockButton"><span class="a-button-inner"><input aria-labelledby="unlockButton-announce" class="a-button-input" id="nativeUnlockButton" type="submit"/><span aria-hidden="true" class="a-button-text" id="unlockButton-announce">
            Submit
        </span></span></span>
</div>
</form>
</div></div>
<br/>
<p class="a-text-center">Need Help? Contact the Global IT Helpdesk</p>
<br/>
</div>
<div class="a-column a-span5 unlock-box-margin2 a-span-last"></div>
</div>
<script type="text/javascript">
    window.snowplow("newTracker", "cf", "sdihol5j2j.execute-api.us-west-2.amazonaws.com", {
        // Initialise a tracker
        appId: "AmazonUnlock",
        contexts: {
            performanceTiming: false,
            // geolocation: true, // If enabled the browser will ask the user to allow us to access their location.
            webPage: false
        },
        discoverRootDomain: true, // We use both password.amazon.com and password-v2.corp.amazon.com
        forceSecureTracker: true,
        platform: "web",
        post: true,
        postPath: "/prod"
    });

    window.snowplow("trackPageView");
</script>
</div></body>
</html>
