<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://cheetoo.com/xmlrpc.php" rel="pingback"/>
<title>Page not found – Arthur Anab Shams</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://cheetoo.com/feed/" rel="alternate" title="Arthur Anab Shams » Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/cheetoo.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://cheetoo.com/wp-includes/css/dist/block-library/style.min.css?ver=5.3.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cheetoo.com/wp-includes/css/dist/block-library/theme.min.css?ver=5.3.6" id="wp-block-library-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cheetoo.com/wp-content/themes/biography/assets/others/bootstrap/css/bootstrap.css?ver=3.3.4" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Raleway%3A400%2C300%2C500%2C600%2C700%2C900&amp;ver=5.3.6" id="biography-googleapis-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cheetoo.com/wp-content/themes/biography/assets/others/Font-Awesome/css/font-awesome.min.css?ver=4.4.0" id="fontawesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cheetoo.com/wp-content/themes/biography/assets/css/blocks.min.css?ver=5.3.6" id="biography-blocks-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cheetoo.com/wp-content/themes/biography/style.css?ver=5.3.6" id="biography-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="biography-style-inline-css" type="text/css">

      /*site identity font family*/
    .site-title,
    .site-title a,
    .site-description,
    .site-description a {
        font-family: Raleway!important;
    }
    /*Title font family*/
    h1, h1 a,
    h2, h2 a,
    h3, h3 a,
    h4, h4 a,
    h5, h5 a,
    h6, h6 a {
        font-family: Raleway!important;
    }
    h1, h1 a,
    h2, h2 a,
    h3, h3 a,
    h4, h4 a,
    h5, h5 a,
    h6, h6 a{
      color: #212121 !important; /*#212121*/
    }
    a,
    a > p,
    .posted-on a,
    .cat-links a,
    .tags-links a,
    .author a,
    .comments-link a,
    .edit-link a,
    .nav-links .nav-previous a,
    .nav-links .nav-next a,
    .page-links a {
        color: #212121 !important; /*#212121*/
    }
    
    a:hover,
    a > p:hover,
    .posted-on a:hover,
    .cat-links a:hover,
    .tags-links a:hover,
    .author a:hover,
    .comments-link a:hover,
    .edit-link a:hover,
    .nav-links .nav-previous a:hover,
    .nav-links .nav-next a:hover,
    .page-links a:hover {
      color: #ff0000 !important; /*#212121*/
        }
    
    .site-title,
    .site-title a,
    .site-description,
    .site-description a, sachyya{
        color:#ffffff!important;
    }
</style>
<script src="https://cheetoo.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://cheetoo.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='https://cheetoo.com/wp-content/themes/biography/assets/others/html5shiv/html5shiv.min.js?ver=3.7.3'></script>
<![endif]-->
<!--[if lt IE 9]>
<script type='text/javascript' src='https://cheetoo.com/wp-content/themes/biography/assets/others/respond/respond.min.js?ver=1.4.2'></script>
<![endif]-->
<link href="https://cheetoo.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://cheetoo.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://cheetoo.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3.6" name="generator"/>
<link href="https://cheetoo.com/wp-content/uploads/2019/05/cropped-lft_pic-1-32x32.jpg" rel="icon" sizes="32x32"/>
<link href="https://cheetoo.com/wp-content/uploads/2019/05/cropped-lft_pic-1-192x192.jpg" rel="icon" sizes="192x192"/>
<link href="https://cheetoo.com/wp-content/uploads/2019/05/cropped-lft_pic-1-180x180.jpg" rel="apple-touch-icon-precomposed"/>
<meta content="https://cheetoo.com/wp-content/uploads/2019/05/cropped-lft_pic-1-270x270.jpg" name="msapplication-TileImage"/>
</head>
<body class="error404 wp-custom-logo biography-right-sidebar">
<div class="hfeed site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header block-section biography-nav-left" id="masthead" role="banner" style="background-image: url('https://cheetoo.com/wp-content/uploads/2019/05/cropped-CBS_0189-2.jpg');">
<div class="block-overlay-content">
<div class="head-top">
<div class="container">
<div class="col-md-6 col-sm-8 col-xs-12">
<nav class="main-navigation" id="site-navigation" role="navigation">
<button aria-controls="primary-menu" aria-expanded="false" class="menu-toggle">
<i class="fa fa-bars"></i>
</button>
<div class="menu-main-menu-container"><ul class="menu" id="primary-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48" id="menu-item-48"><a href="https://cheetoo.com/introduction/">Introduction</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-47" id="menu-item-47"><a href="https://cheetoo.com/experience/">Experience</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-49" id="menu-item-49"><a href="http://thinkingloudly.tirzee.com">Thinking Loudly</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-54" id="menu-item-54"><a href="https://cheetoo.com/church-service/">Church Service</a></li>
</ul></div> </nav><!-- #site-navigation -->
</div>
<div class="col-md-6 col-sm-4 col-xs-12">
<div class="social-icon-only">
<div class="biography-social-section"><ul class="menu" id="menu-social-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-38" id="menu-item-38"><a href="https://facebook.com/anabshams" rel="noopener noreferrer" target="_blank">Facebook</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-39" id="menu-item-39"><a href="https://youtube.com/c/anabshams">Youtube</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-40" id="menu-item-40"><a href="https://www.linkedin.com/in/anabshams/">LinkedIn</a></li>
</ul></div> </div>
</div>
</div>
</div>
<div class="banner-section">
<div class="container">
<div class="row">
<div class="col-md-12 col-lg-10 col-lg-offset-1">
<div class="banner-inner biography-animate fadeInDown overhidden">
<div class="author-content">
<div class="site-branding">
<p class="site-title"><a href="https://cheetoo.com/" rel="home">Arthur Anab Shams</a></p>
<p class="site-description">IT Project Manager | Photographer | Poet</p>
</div><!-- .site-branding -->
</div><!-- .author-content -->
<div class="photo-section">
<span>
<a class="custom-logo-link" href="https://cheetoo.com/" rel="home"><img alt="Arthur Anab Shams" class="custom-logo" height="1068" sizes="(max-width: 1068px) 100vw, 1068px" src="https://cheetoo.com/wp-content/uploads/2019/05/cropped-CBS1575a.jpg" srcset="https://cheetoo.com/wp-content/uploads/2019/05/cropped-CBS1575a.jpg 1068w, https://cheetoo.com/wp-content/uploads/2019/05/cropped-CBS1575a-150x150.jpg 150w, https://cheetoo.com/wp-content/uploads/2019/05/cropped-CBS1575a-300x300.jpg 300w, https://cheetoo.com/wp-content/uploads/2019/05/cropped-CBS1575a-768x768.jpg 768w, https://cheetoo.com/wp-content/uploads/2019/05/cropped-CBS1575a-1024x1024.jpg 1024w" width="1068"/></a>
</span>
</div><!-- .photo-section -->
</div>
</div>
</div>
</div>
</div>
</div>
</header><!-- #masthead -->
<div id="breadcrumb"><div class="container"><div id="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#"><span typeof="v:Breadcrumb"><a href="https://cheetoo.com/" property="v:title" rel="v:url">Arthur Anab Shams</a></span> &gt; <span class="current">Error 404</span></div></div><!-- .container --></div><!-- #breadcrumb --><div class="site-content" id="content">
<div class="content-area" id="primary">
<main class="site-main" id="main" role="main">
<div class="col-sm-8 col-sm-offset-2">
<section class="error-404 not-found">
<header class="page-header">
<h1 class="page-title">Oops! That page can’t be found.</h1>
</header><!-- .page-header -->
<div class="page-content">
<p>It looks like nothing was found at this location. Maybe try one of the links below or a search?</p>
<form action="https://cheetoo.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form>
</div><!-- .page-content -->
</section><!-- .error-404 -->
</div>
</main><!-- #main -->
</div><!-- #primary -->
</div> <!-- *****************************************
             Footer section starts
    ****************************************** -->
<div class="clearfix"></div>
<footer class="site-footer" id="colophon" role="contentinfo">
<div class="site-info">
<div class="copyright">Copyright ©2019 Arthur Anab Shams</div> <a href="https://wordpress.org/">Proudly powered by WordPress</a>
<span class="sep"> | </span>
                 Biography by <a href="http://www.yamchhetri.com/" rel="designer">Yam Chhetri</a>.            </div><!-- .site-info -->
</footer><!-- #colophon -->
<!-- *****************************************
                 Footer section ends
        ****************************************** -->
<a class="biography-back-to-top" href="#page"><i class="fa fa-angle-up"></i></a>
</div><!-- #page -->
<script src="https://cheetoo.com/wp-content/themes/biography/assets/others/jquery.easing/jquery.easing.js?ver=0.3.6" type="text/javascript"></script>
<script src="https://cheetoo.com/wp-content/themes/biography/assets/others/bootstrap/js/bootstrap.min.js?ver=3.3.5" type="text/javascript"></script>
<script src="https://cheetoo.com/wp-content/themes/biography/assets/js/biography-custom.js?ver=1.0.1" type="text/javascript"></script>
<script src="https://cheetoo.com/wp-content/themes/biography/assets/js/navigation.js?ver=20120206" type="text/javascript"></script>
<script src="https://cheetoo.com/wp-content/themes/biography/assets/js/skip-link-focus-fix.js?ver=20130115" type="text/javascript"></script>
<script src="https://cheetoo.com/wp-includes/js/wp-embed.min.js?ver=5.3.6" type="text/javascript"></script>
</body>
</html>