<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>PayPal vs Google Checkout   » Page not found - Compare online payment processing systems </title>
<link href="https://paypalvsgooglecheckout.com/feed/" rel="alternate" title="PayPal vs Google Checkout RSS Feed" type="application/rss+xml"/>
<link href="https://paypalvsgooglecheckout.com/xmlrpc.php" rel="pingback"/>
<script src="https://paypalvsgooglecheckout.com/wp-content/themes/simplex/includes/js/suckerfish.js" type="text/javascript"></script>
<!--[if lt IE 7]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.0(beta3)/IE7.js" type="text/javascript"></script>
<![endif]-->
<link href="https://paypalvsgooglecheckout.com/wp-content/themes/simplex/style.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://paypalvsgooglecheckout.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://paypalvsgooglecheckout.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 3.8.35" name="generator"/>
<link href="https://paypalvsgooglecheckout.com/wp-content/themes/simplex/css/wordpress.css" rel="stylesheet" type="text/css"/>
<!--[if lte IE 7]>
<link rel="stylesheet" type="text/css" href="https://paypalvsgooglecheckout.com/wp-content/themes/simplex/ie.css" />
<![endif]-->
</head>
<body>
<div id="page">
<div id="header1" style="background:none repeat scroll 0 0 #FFFFFF;">
<div id="headerimg1">
<h1><a href="http://paypalvsgooglecheckout.com/">PayPal vs Google Checkout</a></h1>
<span class="description">Compare online payment processing systems</span>
<div id="logos">
<!-- Begin PayPal Logo --><a href="https://www.paypal.com/us/mrb/pal=5URA32R9C6QF4" rel="nofollow" target="_blank"><img alt="Sign up for PayPal and start accepting credit card payments instantly." border="0" class="alignleft size-full wp-image-16" src="http://paypalvsgooglecheckout.com/wp-content/uploads/2011/07/paypal.gif"/></a><!-- End PayPal Logo --><img alt="Google Checkout" class="alignright size-full wp-image-16" height="60" src="http://paypalvsgooglecheckout.com/wp-content/uploads/2010/03/google_checkout.gif" style="padding-top:18px;" title="google_checkout" width="155"/><img src="http://paypalvsgooglecheckout.com/wp-content/uploads/2011/07/versus.gif" style="padding-top:27px;"/>
</div>
</div>
</div>
<div id="pagemenu">
<ul class="clearfix" id="page-list"><li class="page_item"><a href="https://paypalvsgooglecheckout.com" title="Home">Home</a></li><li class="page_item page-item-5"><a href="https://paypalvsgooglecheckout.com/countries/">Countries &amp; Currencies</a></li>
<li class="page_item page-item-3"><a href="https://paypalvsgooglecheckout.com/fees/">Fees</a></li>
<li class="page_item page-item-6"><a href="https://paypalvsgooglecheckout.com/reviews/">Reviews</a></li>
<li class="page_item page-item-2"><a href="https://paypalvsgooglecheckout.com/compare-paypal-google-checkout/">Side-by-Side</a></li>
<li class="page_item page-item-12"><a href="https://paypalvsgooglecheckout.com/types-of-paypal-accounts/">Types of Paypal Accounts</a></li>
</ul>
</div>
<hr/><div id="main">
<div class="narrowcolumn" id="content">
<h2 class="center">Error 404 - Not Found</h2>
</div>
<div id="sidebar">
<ul>
<!--<li id="rssfeeds">Subscribe: <a href="https://paypalvsgooglecheckout.com/feed/">Entries</a> | <a href="https://paypalvsgooglecheckout.com/comments/feed/">Comments</a></li>-->
<li class="widget widget_text" id="text-3"> <div class="textwidget"><p>Take me to...</p><a href="https://www.paypal.com/us/mrb/pal=5URA32R9C6QF4" rel="nofollow" style="text-decoration:none" target="_blank"><img alt="Sign up for PayPal and start accepting credit card payments instantly." border="0" src="http://paypalvsgooglecheckout.com/wp-content/uploads/2011/07/paypal.gif"/></a>
<a href="http://checkout.google.com/" target="_blank">
<img alt="Google Checkout" height="60" src="http://paypalvsgooglecheckout.com/wp-content/uploads/2010/03/google_checkout.gif" title="google_checkout" width="155"/></a>
<script charset="utf-8" type="text/javascript">
	addEventListener('load', function() {
		setTimeout(hideAddressBar, 0);
	}, false);
	function hideAddressBar() {
		window.scrollTo(0, 1);
	}
</script>
<script type="text/javascript">


var isMSIE = /*@cc_on!@*/false;

var mode = 'flag_united_states';

function rate_changed(new_mode) {
 if ((typeof new_mode == 'string') && (new_mode.substr(0,5) == 'flag_')) { mode = new_mode; }
 /*var cb = document.forms['ppcalc_form'].elements['crossborder_checkbox'].checked;
 if (mode == 'flag_united_states') { set_pp_rate((cb)?'3.9':'2.9','0.30','$','USD'); }
 if (mode == 'flag_canada') { set_pp_rate((cb)?'3.9':'2.9','0.30','$','CAD'); }
 if (mode == 'flag_united_kingdom') { set_pp_rate((cb)?'3.9':'3.4','0.20','&pound;','GBP'); }
 if (mode == 'flag_europe') { set_pp_rate((cb)?'3.9':'3.4','0.35','&euro;','EUR'); }
 if (mode == 'flag_france') { set_pp_rate((cb)?'3.4':'3.4','0.25','&euro;','EUR'); }
 if (mode == 'flag_germany') { set_pp_rate((cb)?'3.9':'1.9','0.35','&euro;','EUR'); }
 if (mode == 'flag_australia') { set_pp_rate((cb)?'3.4':'2.4','0.30','$','AUD'); }
 if (mode == 'flag_japan') { set_pp_rate((cb)?'3.9':'3.4','40','&yen;','JPY'); }
 if (mode == 'flag_czech_republic') { set_pp_rate((cb)?'3.9':'3.4','10','','CZK'); }
 if (mode == 'flag_hungary') { set_pp_rate((cb)?'3.9':'3.4','90','','HUF'); }
 if (mode == 'flag_poland') { set_pp_rate((cb)?'3.9':'3.4','1.35','','PLN'); }
 if (mode == 'flag_denmark') { set_pp_rate((cb)?'3.9':'3.9','2.60','','DKK'); }
 if (mode == 'flag_norway') { set_pp_rate((cb)?'3.9':'3.9','2.80','','NOK'); }
 if (mode == 'flag_sweden') { set_pp_rate((cb)?'3.9':'3.9','3.25','','SEK'); }
 if (mode == 'flag_switzerland') { set_pp_rate((cb)?'3.9':'3.4','0.55','','CHF'); }
 if (mode == 'flag_singapore') { set_pp_rate((cb)?'3.4':'3.4','0.50','$','SGD'); }
 if (mode == 'flag_hong_kong') { set_pp_rate((cb)?'3.4':'3.4','2.35','$','HKD'); }
 if (mode == 'flag_new_zealand') { set_pp_rate((cb)?'3.9':'3.4','0.45','$','NZD'); }
 */
RPPCalculate(1);
 PPCalculate();
}

function get_float(string) {
 string = string.replace(",", "").replace("$", "");
 var to_return = parseFloat(string);
 if (typeof to_return == 'undefined') { to_return = 0.00; }
 if (isNaN(to_return)) { to_return = 0.00; }
 to_return = (to_return/100)*100;
 return to_return;
}

function get_key_code(e) {
 if (typeof e == "undefined") { e = window.event; }
 if (e.keyCode) { return e.keyCode; }
 else if (e.which) { return e.which; }
 else { return e.charCode; }
}

function has_selection(ele) {
 var txt='',len=0,start=0,end=0;
 if (document.selection) { txt = document.selection.createRange().text; }
 else if (window.getSelection) { txt = window.getSelection(); }
 else if (document.getSelection) { txt = document.getSelection(); }
 else { return false; }
 if (ele.selectionStart) { start = ele.selectionStart; }
 if (ele.selectionEnd) { end = ele.selectionEnd; }
 len = end - start;
 if (len > 0) { return true; }
 else if (typeof txt == "undefined") { return false; }
 else if (isNaN(txt)) { return false; }
 else if (txt.length > 0) { return true; }
 else { return false; }
}

function textbox_onkeypress(e) {
 if (typeof e == "undefined") { e = window.event; }
 var keycode = get_key_code(e);
 var to_return = true;
 switch (keycode) {
  case 8: // backspace
   break;
  case 9: // tab
   break;
  case 13: // enter
   break;
  case 17: case 86: // CTRL-V
   break;
  case 37: case 39: break; // left arrow, right arrow
  case 38: // up arrow
   if (this.value != '') { this.value = (get_float(this.value) + 0.5).toFixed(2); }
   break;
  case 40: // down arrow
   if (get_float(this.value) >= 0.5) { this.value = (get_float(this.value) - 0.5).toFixed(2); }
   else { this.value = '0.00'; }
   if (this.id == 'to_receive') { RPPCalculate(); }
   else if (this.id == 'if_you_receive') { PPCalculate(); }
   break;
  case 46: // period (.) was pressed
   if (this.value.indexOf(".") != -1) { // there is already a (.) in there
    if (has_selection(this) == false) { to_return = false; } // no good
   }
   break;
  case 48: case 49: case 50: case 51: case 52: case 53: case 54: case 55: case 56: case 57:
   if ((this.id != 'paypal_fee_percent')) {
    if ( this.value.length >= 3) {
     if ( this.value.substr(this.value.length - 3, 1) == '.' ) {
      if (has_selection(this) == false) { to_return = false; } // no good
     }
    }
   }
   break;
  default:
   to_return = false;
 } // end switch
 if ((!e.which) && (keycode == 46)) { to_return = true; } // allow delete
 if ((typeof e.which == "undefined") && (keycode == 46) && (this.value.indexOf(".") != -1) && (has_selection(this) == false)) { to_return = false; }
 e.returnValue = to_return;
 return to_return;
}

function set_pp_rate(percent,fixed,sign,name){
 document.forms['ppcalc_form'].elements['paypal_fee_percent'].value = percent;
 document.forms['ppcalc_form'].elements['paypal_fee_fixed'].value = fixed;
 document.getElementById('currency_sign').innerHTML = sign;
 document.getElementById('currency_name').innerHTML = name;
 document.getElementById('currency_symbol_1').innerHTML = sign;
 document.getElementById('currency_symbol_2').innerHTML = sign;
 document.getElementById('currency_symbol_3').innerHTML = sign;
 document.getElementById('currency_symbol_4').innerHTML = sign;
 document.getElementById('currency_symbol_5').innerHTML = sign;
}

function figure_fees(item_price, fee_percentage, fixed_fee) {
 var the_fee_percentage = fee_percentage / 100;
 the_fee_percentage = the_fee_percentage.toFixed(4);
 the_fee_percentage = get_float(the_fee_percentage);
 var to_return = item_price * the_fee_percentage + fixed_fee;
 to_return = Math.round(to_return*100)/100;
 to_return = to_return.toFixed(2);
 to_return = get_float(to_return);
 if (to_return > item_price) { to_return = item_price; }
 return to_return;
}

function PPCalculate() {
 if (document.forms['ppcalc_form'].elements['if_you_receive'].value != '') {
  var amount_sent = get_float(document.forms['ppcalc_form'].elements['if_you_receive'].value);
  if (amount_sent == 0) { document.forms['ppcalc_form'].elements['paypal_fees'].value = document.forms['ppcalc_form'].elements['you_would_receive'].value = '0.00'; }
  else {
   var the_paypal_fees = figure_fees(amount_sent, get_float(document.forms['ppcalc_form'].elements['paypal_fee_percent'].value), get_float(document.forms['ppcalc_form'].elements['paypal_fee_fixed'].value));
   document.forms['ppcalc_form'].elements['paypal_fees'].value = the_paypal_fees.toFixed(2);
   var amount_after_fees = amount_sent - get_float(document.forms['ppcalc_form'].elements['paypal_fees'].value);
   if (amount_after_fees < 0) { amount_after_fees = 0; }
   document.forms['ppcalc_form'].elements['you_would_receive'].value = amount_after_fees.toFixed(2);
  }
 }
 else {
  document.forms['ppcalc_form'].elements['paypal_fees'].value = '';
  document.forms['ppcalc_form'].elements['you_would_receive'].value = '';
 }
}

function RPPCalculate(src) {
 if (document.forms['ppcalc_form'].elements['to_receive'].value != '') {
  var desired_amount = get_float(document.forms['ppcalc_form'].elements['to_receive'].value);
  if (desired_amount == 0) { document.forms['ppcalc_form'].elements['a_person_would'].value = '0.00'; }
  else {
   var a_person_must_send = (desired_amount + get_float(document.forms['ppcalc_form'].elements['paypal_fee_fixed'].value)) / (1 - (get_float(document.forms['ppcalc_form'].elements['paypal_fee_percent'].value)/100).toFixed(4));
   document.forms['ppcalc_form'].elements['a_person_would'].value = a_person_must_send.toFixed(2);
   document.forms['ppcalc_form'].elements['if_you_receive'].value = document.forms['ppcalc_form'].elements['a_person_would'].value;
   PPCalculate();
  }
 }
 else {
  if (src != 1) {
   document.forms['ppcalc_form'].elements['a_person_would'].value = '';
   document.forms['ppcalc_form'].elements['if_you_receive'].value = '';
  }
  PPCalculate();
 }
}

if (window.addEventListener) { window.addEventListener("load", window_onload, false); }
else if (window.attachEvent) { window.attachEvent("onload", window_onload); }

function window_onload() {
 document.forms['ppcalc_form'].elements['to_receive'].onkeypress = textbox_onkeypress;
 document.forms['ppcalc_form'].elements['if_you_receive'].onkeypress = textbox_onkeypress;
 document.forms['ppcalc_form'].elements['paypal_fee_percent'].onkeypress = textbox_onkeypress;
 document.forms['ppcalc_form'].elements['paypal_fee_fixed'].onkeypress = textbox_onkeypress;
 document.forms['ppcalc_form'].elements['to_receive'].onkeyup = RPPCalculate;
 document.forms['ppcalc_form'].elements['if_you_receive'].onkeyup = PPCalculate;
 document.forms['ppcalc_form'].elements['crossborder_checkbox'].onclick = rate_changed;
 for (var i=0;i<document.images.length;i++) {
  if (document.images[i].id.substring(0,5) == 'flag_') {
   document.images[i].onclick = function(){ rate_changed(this.id); };
   if (isMSIE) { document.images[i].src = 'ie_images/' + document.images[i].id + '.gif'; }
  }
  else if (document.images[i].id.substring(0,14) == 'question_mark_') {
   if (isMSIE) { document.images[i].src = 'ie_images/question_mark_icon.gif'; }
  }
 }
 document.forms['ppcalc_form'].elements['to_receive'].focus();
}

var mouse_is_over_other_fee_calcs=false;
var mouse_is_over_other_calcs=false;

function other_fee_calcs_onmouseover() {
 mouse_is_over_other_fee_calcs=true;
 mouse_is_over_other_calcs=false;
 document.getElementById('other_calcs').style.visibility='visible';
 document.getElementById('other_calcs').focus();
}

function other_fee_calcs_onmouseout() {
 mouse_is_over_other_fee_calcs=false;
 setTimeout('if((mouse_is_over_other_fee_calcs==false)&&(mouse_is_over_other_calcs==false)){document.getElementById(\'other_calcs\').style.visibility=\'hidden\';}',75);
}

function other_calcs_onmouseover() {
 mouse_is_over_other_calcs=true;
 mouse_is_over_other_fee_calcs=false;
}

function other_calcs_onmouseout() {
 mouse_is_over_other_calcs=false;
 setTimeout('if((mouse_is_over_other_fee_calcs==false)&&(mouse_is_over_other_calcs==false)){document.getElementById(\'other_calcs\').style.visibility=\'hidden\';}',75);
}

/* ]]> */
</script>
<form action="" id="ppcalc_form" method="post">
<!-- PayPal Fee Rate Box -->
<div class="bg" id="paypal_fee_rate_div">
<table align="center" border="0" cellpadding="0" cellspacing="0" class="bg" width="260">
<tr>
<td colspan="3"><div align="center"><h3>PayPal &amp; Google Checkout<br/>
      Fee Calculator</h3></div></td>
</tr>
<tr>
<td class="left top"><span class="style5">
<label for="if_you_receive">If a person sends you:</label>
</span></td>
<td class="mid top"><span class="style5" id="currency_symbol_3">$</span></td>
<td class="right top"><input class="text_box " id="if_you_receive" name="if_you_receive" size="6" type="text"/></td>
</tr>
<tr>
<td class="left"><span class="style5">
<label for="paypal_fees">Your PayPal fees would be:</label>
</span></td>
<td class="mid"><span class="style5" id="currency_symbol_4">$</span></td>
<td class="right"><input class="text_box " id="paypal_fees" name="paypal_fees" readonly="readonly" size="6" type="text"/></td>
</tr>
<tr>
<td class="left"><span class="style10">
<label class="style5" for="you_would_receive">And you would receive:</label>
</span></td>
<td class="mid"><span class="style5" id="currency_symbol_5">$</span></td>
<td class="right"><input class="text_box " id="you_would_receive" name="you_would_receive" readonly="readonly" size="6" type="text"/></td>
</tr>
<tr>
<td class="bottom" colspan="3"></td>
</tr>
</table>
<br/>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="bg" width="230">
<tr>
<td class="head style3 style8" colspan="3"><div align="center" class="style5"><strong>Reverse  Fee Calculator</strong></div></td>
</tr>
<tr>
<td class="left top"><span class="style10">
<label for="to_receive"><span class="style5">To receive this<br/>
      amount (after fees):</span></label>
</span></td>
<td class="mid top" valign="bottom"><span class="style5" id="currency_symbol_1">$</span></td>
<td class="right top" valign="bottom"><input class="text_box " id="to_receive" name="to_receive" size="6" type="text"/></td>
</tr>
<tr>
<td class="left"><span class="style10">
<label class="style5" for="a_person_would">A person would<br/>
      have to send you:</label>
</span></td>
<td class="mid" valign="bottom"><span class="style5" id="currency_symbol_2">$</span></td>
<td class="right" valign="bottom"><input class="text_box " id="a_person_would" name="a_person_would" readonly="readonly" size="6" type="text"/></td>
</tr>
<tr>
<td class="bottom" colspan="3"></td>
</tr>
</table>
<div align="center" class="style6">Enter your fee rate below:</div>
<table align="center" border="0" cellpadding="0" cellspacing="5">
<tr>
<td><input class="text_box text_box_percent" id="paypal_fee_percent" name="paypal_fee_percent" onkeyup="PPCalculate();RPPCalculate(1);" size="3" type="text" value="2.9"/></td>
<td><label for="paypal_fee_fixed"><span class="style5">%  +  <span id="currency_sign">$</span></span></label></td>
<td><input class="text_box text_box_percent" id="paypal_fee_fixed" name="paypal_fee_fixed" onkeyup="PPCalculate();RPPCalculate(1);" size="4" type="text" value="0.30"/></td>
<td><span class="style3"> <span class="style13">
<label class="style5" for="paypal_fee_fixed" id="currency_name">USD</label>
</span> </span></td>
<td style="padding-bottom:3px"></td>
</tr>
</table>
</div>
<!--
<table width="294" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>

 <td style="padding-right:5px"><div style="visibility: hidden;"><input type="checkbox" name="crossborder_checkbox" id="crossborder_checkbox" /></div></td>
 <td style="text-align:left"><label for="crossborder_checkbox" class="style5">

   <div align="center"><span class="style4">If you are receiving this payment from another<br />
     country, click the corresponding flag below</span></div>
 </label></td>
 <td style="text-align:left"><div style="visibility: hidden;"><input type="checkbox" name="even_checker" id="even_checker" /></div></td>
</tr>
</table>
<div style="margin-top:8px;">
  <div align="center"><strong>
  <img src="images/flags/USD.gif" alt="US" name="flag_united_states" width="22" height="14" id="flag_united_states" title="United States" />

  <img id="flag_canada" src="images/flags/CAD.gif" width="22" height="14" alt="Canada" title="Canada" />
  <img id="flag_united_kingdom" src="images/flags/GBP.gif" width="22" height="14" alt="UK" title="United Kingdom" />
  <img id="flag_europe" src="images/flags/EUR.gif" width="22" height="14" alt="Europe" title="Europe" />
  <img id="flag_france" src="images/flags/FRA.gif" width="22" height="14" alt="France" title="France" />
  <img id="flag_germany" src="images/flags/DEM.gif" width="22" height="14" alt="Germany" title="Germany" />
  <img id="flag_australia" src="images/flags/AUD.gif" width="22" height="14" alt="Australia" title="Australia" />
  <img id="flag_japan" src="images/flags/JPY.gif" width="22" height="14" alt="Japan" title="Japan" />
  <img id="flag_czech_republic" src="images/flags/CZK.gif" width="22" height="14" alt="Czech Republic" title="Czech Republic" /><br />
  <img id="flag_hungary" src="images/flags/HUF.gif" width="22" height="14" alt="Hungary" title="Hungary" />

  <img id="flag_poland" src="images/flags/PLN.gif" width="22" height="14" alt="Poland" title="Poland" />
  <img id="flag_denmark" src="images/flags/DKK.gif" width="22" height="14" alt="Denmark" title="Denmark" />
  <img id="flag_norway" src="images/flags/NOK.gif" width="22" height="14" alt="Norway" title="Norway" />
  <img id="flag_sweden" src="images/flags/SEK.gif" width="22" height="14" alt="Sweden" title="Sweden" />
  <img id="flag_switzerland" src="images/flags/CHF.gif" width="22" height="14" alt="Switzerland" title="Switzerland" />
  <img id="flag_singapore" src="images/flags/SGD.gif" width="22" height="14" alt="Singapore" title="Singapore" />
  <img id="flag_hong_kong" src="images/flags/HKD.gif" width="22" height="14" alt="Hong Kong" title="Hong Kong" />
  <img id="flag_new_zealand" src="images/flags/NZD.gif" width="22" height="14" alt="New Zealand" title="New Zealand" />
  <br />

  </strong></div>
</div>-->
</form>
<br/>
</div>
</li>
</ul>
</div>
</div>
<hr/>
<div id="footer">
<div id="footerarea">
<span id="footerleft">
		© 2021. PayPal vs Google Checkout. All rights reserved.
	</span>
<span id="footerright">
<a href="http://www.easywarmfloor.com">radiant floor heating</a> | Easy Heated <a href="http://www.usfloorheating">Driveways</a> | document shredding services by <a href="http://www.shredfly.com">Shredfly</a> Chicago <br/> locked out? call <a href="http://www.orlandolocksmithservice.net"> Orlando Locksmith Service</a></span>
</div>
</div>
<!-- Simple design by Chandra Maharzan - http://wpshoppe.com/ -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18403896-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</div></body>
</html>