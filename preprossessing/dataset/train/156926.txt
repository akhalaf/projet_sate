<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "57319",
      cRay: "610ec7a6ffeb018b",
      cHash: "d573cef7d31098a",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXJ5cXR2LnR2Lw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "+CGxTxKzXGrULDnv494VS+Auj7JwuuH7epNKMmFoANnsJ6sX5yemLlmWw6AN7n9ziThFlhQPOidbfl5XULEbwsx2+jBKB1zhF/e7HGt91iAR0KmS2bM11rU5KtXHC4kYc8P1m/YIcsdfeirniEb8BFFaHHV3/NbYcvDVL9i+XUjELnk1aYH6G6omYXFTAxBS6rbdkfAdiK6RwZX4EB8bmCtixF3eqAtInfhSdTjtbcSp7PKpJyzucrx5mko8rP2K8pwJxAN2AeFj8TlEgfp2Q7l6HFJBue/N1bNZAHIS0axyGEA8eo3uWpk8syI5N9VY0q+daBOPGQoOnHHCG9YGwLWfG/qq82R2qS5PUs8XopNSYlGvZxszuZFCcajWq/wGQhcBq8lWO2lS6KrSs3V38RzcRg6aXnVD/DJkCy16Hj1NIHEBETCLqjmGqkok76BL1i4Nq/1USIRm7BOuHMMCvh2wDsFg6rsU65nf2w6sWOAhtD5k54Ed50hPFchzq/NyMx4JaVo6QyRl06Zd0Nif82iVqlMpSsyt4xev8JqJjTWXzfStcxWmFDGGNMCkrdrz5aMAvNPLZmLqzS4RlJYdMge1Kex4eFiPQtHlRGbRcDxcX5/o6PPM0fXJ17O36TwdObj5PZMtymRGms+ukrQtIPT1WKdPeFqs7xctd3V1qBqKh2da63Ua7MJXf92GXLDS5Ne7iJippdWbzM2dRcmNwzeVuQ3FC0hL96x2pxJeDheamG/5WsMXBEDgxlRsqiOG",
        t: "MTYxMDUzNzM3MC43MjIwMDA=",
        m: "y8fTwlIjjoMdTFyjHuGo/kFMXt4E9rQ4Al8jP+Hgc3k=",
        i1: "R9ehmoUJuVk+Se9D3Yk/Rw==",
        i2: "kuvmgAmDUFwHh179ryY/dQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "1jlvLMtq856I2P3p9xg8rEHfmbOFnfhaR5x2xH9RE/k=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.aryqtv.tv</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<a href="https://bt50.net/concerned.php?entry_id=84"><!-- table --></a>
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=331c740c3bf5d2030605150a15c7912047173081-1610537370-0-Ad4Rses37BT3PePXQQgQ3r9gLxi7wZ_pGIHd65B7vnTWeryW-L8viO2GctzPY6U2aBqkrwHkds8fWIhX_Z8-vhFeJIVMasv5UmAzr7qEqCy5kqc9p6dsBC_m9aQrYEKID7tdE4INyoHShxx9gUF0cyEeH6lwqgEnDvlb9HTmWjGWlhypd3j5I7P-8KOiOYlV6X1Fa08d2oWsDrPmaD8eHngIThtAXc_N2hTZrD_7ETd3MibVAJIsoNiZmx-vJiS9YiA2v98L2Ht502VPfVIgIYmPavvA11b2KTH1qQijUylJpWrwfLw-a1-s29mx3NL7joz0-PruJu4-I8tkwokUAIfnOwsu00QSBtSQ5PpwzgLDDH-EOF0WuOoXTuYAtL961QAhtDtVLQ1ldZ1zlzGJRIqaE5p_gWQizqugCt3MRl-pzSDzX12VY5UUV7S2ODPQE3hT3CUKwESGDMdPCKLMWPpNBbnHLK98I2e56Upk8w2nFCT3C40Shs4WQgfReci1xNhQxXyE2Xpwm0Cbjm_4oBg" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="c796f514a0133688993ec38cc4a3f7a131b3935f-1610537370-0-AbupRR+Ipdp3CuLTIxcgF9DFkSIVaPYBAxtFTKT6/ebZ2KYK9Ax/3SkIdYfGkCS180f4ZiUYCTArDkE/lCc/RBNFy75m6kcve8T4VXdN/3nsXrDw4kpq/UlfKOm/SvV3CPnRc54OxNDo47FI44VCdtY1cE4EWDD7XZZkZ43cOf7ZUBS1s2kjHBSWZcuFmWbZt3z+OvBvmIPseBXJRW62nIZ7g1Q6/ElZfzQb9g2IJRUb8X4K4gy2lyOCkAQPlvCHTm5DftECi4CfdTGGtZLcyyOIMOcuwrWfucFXmWpZZ+MN/Obt91FqZBgyat52YqepdkVEub0fWb3lEBNC2Xo/jgq55FyuNjUXVKyVOneLdHlVs21le69KaMV7lUOCwVTWvjMMhCsv3ZvL3LsbJcxhVsy+BRpDjLdzsPHPG4PAzWOmVv1zDdTjzG4ZsVR5NNASei6J0weVXBKaePZ04sSLe4EO7fKICtlQ9sJaSrAhpXcJisRzxHwu7TMZER7d144rZuUkLc7ajSjWV8XUmcNBILRz+8+nh/Ps51O4/gvGzXJOkOJSh8Clrjfd5f5thBcrq0QQS5/+NoFawfiZ090t64t5HTiDqk/0mGlRTzu1NOjtI2umHGN9P+nsaksljuNV+Rc/eQFX1Xw38CWQKHsCJ0faeVQe66UA2uZHA7oRwWdaOxoB3huGDk9N0P70Q7P1kwDgRO7ylmj5TAuQdQKzw7FLPHwtciqXkPjajqdwip+pkX01yZ/wUaB3k6k3dEfzKXyI1+rmksqLfx12bLZaq4L9U5QLRB95lGswDa4QDgV5RCe4Mq/azfNXyS4vWIC2amb33GF1Y9K0Ym0ahAWz6SQNQNIFF7AaDFAy1ebNRyqMPKy9ha0GKNnDdDCV8J2t8qM6ZZWMRgSnvx4h/P0xfCl02W3JwJc9KPfsQTof8hkKey971BxQiZBoX+vzz9Ach8fPEahr3rmRwC4fUi1o42MSbsQgoGxa4F/ozLiXGGNe611DKhY9VF8FJGCcJTw41k2RdJ6pwvQP1NPqah09qqYhHnf3pJ6Duj0m4/3qgaqqJiYKYhtUgzbH1tFcQLtb2TvQN1G4uPmdkhHEdOyHC5tphF48XKLJzuL3dAjGddYhs2ll7bZ98e3cpHw1hvssLFdpWI1BdWRUWynQkRnCC4vK2WVDy0O5yrQuaAJGhttih5JqpgD2TMDQu84dw7JQuAoWqcBz7cPQTCowUmYcOcHWaeIViNhEwwrmvJ573PxYYk9PcJE/WXujJpg+mmVZ05noJ3PC5+PRg0zU6vujqF9kpHkrjN3IpPRjiyNqGsrw"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="4774dad774360ca4a0c5c53113ec03d8"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610ec7a6ffeb018b')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610ec7a6ffeb018b</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
