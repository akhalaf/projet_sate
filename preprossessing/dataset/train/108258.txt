<html><body><p>ï»¿<!DOCTYPE html>

<!--[if IE 7]>
<html class="ie ie7" lang="en-GB">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en-GB">
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<!--<![endif]-->
<!-- Mirrored from amazingacts.net/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 26 Jun 2017 22:08:45 GMT -->
<!-- Added by HTTrack --><meta content="text/html;charset=utf-8" http-equiv="content-type"/><!-- /Added by HTTrack -->
</p>
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<title>Amazing Acts | Enjoying the real life drama of Acts –  Gerard Chrispin</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
<script src="http://amazingacts.net/wp-content/themes/twentytwelve/js/html5.js" type="text/javascript"></script>
<![endif]-->
<link href="http://fonts.googleapis.com/" rel="dns-prefetch"/>
<link href="http://s.w.org/" rel="dns-prefetch"/>
<link href="feed" rel="alternate" title="Amazing Acts » Feed" type="application/rss+xml"/>
<link href="comments/feed" rel="alternate" title="Amazing Acts » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/amazingacts.net\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7.5"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="wp-content/plugins/compact-wp-audio-player/css/flashblock66f2.css?ver=4.7.5" id="scap.flashblock-css" media="all" rel="stylesheet" type="text/css"/>
<link href="wp-content/plugins/compact-wp-audio-player/css/player66f2.css?ver=4.7.5" id="scap.player-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&amp;subset=latin,latin-ext" id="twentytwelve-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="wp-content/themes/twentytwelve/style66f2.css?ver=4.7.5" id="twentytwelve-style-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='twentytwelve-ie-css'  href='http://amazingacts.net/wp-content/themes/twentytwelve/css/ie.css?ver=20121010' type='text/css' media='all' />
<![endif]-->
<link href="wp-content/plugins/easy-table/themes/default/stylef24c.css?ver=1.6" id="easy_table_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="wp-content/plugins/synved-shortcodes/synved-shortcode/jqueryUI/css/snvdshc/jquery-ui-1.9.2.custom.min2c00.css?ver=1.9.2" id="synved-shortcode-jquery-ui-css" media="all" rel="stylesheet" type="text/css"/>
<link href="wp-content/plugins/synved-shortcodes/synved-shortcode/style/layout5152.css?ver=1.0" id="synved-shortcode-layout-css" media="all" rel="stylesheet" type="text/css"/>
<link href="wp-content/plugins/synved-shortcodes/synved-shortcode/style/jquery-ui5152.css?ver=1.0" id="synved-shortcode-jquery-ui-custom-css" media="all" rel="stylesheet" type="text/css"/>
<script src="wp-content/plugins/compact-wp-audio-player/js/soundmanager2-nodebug-jsmin66f2.js?ver=4.7.5" type="text/javascript"></script>
<script src="wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4" type="text/javascript"></script>
<script src="wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1" type="text/javascript"></script>
<script src="wp-includes/js/jquery/ui/core.mine899.js?ver=1.11.4" type="text/javascript"></script>
<script src="wp-includes/js/jquery/ui/widget.mine899.js?ver=1.11.4" type="text/javascript"></script>
<script src="wp-includes/js/jquery/ui/tabs.mine899.js?ver=1.11.4" type="text/javascript"></script>
<script src="wp-includes/js/jquery/ui/accordion.mine899.js?ver=1.11.4" type="text/javascript"></script>
<script src="wp-content/plugins/fd-footnotes/fdfootnotes2951.js?ver=1.34" type="text/javascript"></script>
<script src="wp-content/plugins/synved-shortcodes/synved-shortcode/script/jquery.ba-bbq.min1576.js?ver=1.2.1" type="text/javascript"></script>
<script src="wp-content/plugins/synved-shortcodes/synved-shortcode/script/jquery.scrolltab5152.js?ver=1.0" type="text/javascript"></script>
<script src="wp-includes/js/jquery/ui/button.mine899.js?ver=1.11.4" type="text/javascript"></script>
<script src="wp-content/plugins/synved-shortcodes/synved-shortcode/script/jquery-unselectable8a54.js?ver=1.0.0" type="text/javascript"></script>
<script src="wp-includes/js/jquery/ui/mouse.mine899.js?ver=1.11.4" type="text/javascript"></script>
<script src="wp-includes/js/jquery/ui/slider.mine899.js?ver=1.11.4" type="text/javascript"></script>
<script src="wp-content/plugins/synved-shortcodes/synved-shortcode/script/base5152.js?ver=1.0" type="text/javascript"></script>
<script src="wp-content/plugins/synved-shortcodes/synved-shortcode/script/custom5152.js?ver=1.0" type="text/javascript"></script>
<link href="wp-json/index.html" rel="https://api.w.org/"/>
<link href="xmlrpc0db0.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.7.5" name="generator"/>
<link href="index.html" rel="canonical"/>
<link href="index.html" rel="shortlink"/>
<link href="wp-json/oembed/1.0/embed27fe.json?url=http%3A%2F%2Famazingacts.net%2F" rel="alternate" type="application/json+oembed"/>
<link href="wp-json/oembed/1.0/embed5ccc?url=http%3A%2F%2Famazingacts.net%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<style type="text/css">
			.comments-link {
				display: none;
			}
					</style>
<!-- Hide Comments plugin -->
<link href="wp-content/plugins/j-shortcodes/css/jay.css" rel="stylesheet" type="text/css"/>
<script src="wp-content/plugins/j-shortcodes/js/jay.js" type="text/javascript"></script><link href="wp-content/plugins/j-shortcodes/galleryview/css/jquery.galleryview-3.0.css" rel="stylesheet" type="text/css"/>
<script src="wp-content/plugins/j-shortcodes/galleryview/js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="wp-content/plugins/j-shortcodes/galleryview/js/jquery.timers-1.2.js" type="text/javascript"></script>
<script src="wp-content/plugins/j-shortcodes/galleryview/js/jquery.galleryview-3.0.min.js" type="text/javascript"></script><link href="wp-content/plugins/j-shortcodes/css/jquery/smoothness/jquery-ui-1.8.9.custom.css" rel="stylesheet" type="text/css"/><!-- <meta name="NextGEN" version="2.1.77" /> -->
<style type="text/css"></style>
<style id="twentytwelve-header-css" type="text/css">
			.site-header h1 a,
		.site-header h2 {
			color: #080060;
		}
		</style>
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #1c1254; }
</style>
<div class="hfeed site" id="page">
<header class="site-header" id="masthead" role="banner">
<hgroup>
<h1 class="site-title"><a href="index.html" rel="home" title="Amazing Acts">Amazing Acts</a></h1>
<h2 class="site-description">Enjoying the real life drama of Acts –  Gerard Chrispin</h2>
</hgroup>
<nav class="main-navigation" id="site-navigation" role="navigation">
<button class="menu-toggle">Menu</button>
<a class="assistive-text" href="#content" title="Skip to content">Skip to content</a>
<div class="menu-menu-1-container"><ul class="nav-menu" id="menu-menu-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-4 current_page_item menu-item-11" id="menu-item-11"><a href="index.html">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-76" id="menu-item-76"><a href="overview.html">Overview</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-72" id="menu-item-72"><a href="introductions.html">Introductions</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73" id="menu-item-73"><a href="foreword.html">Foreword â Act one</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-420" id="menu-item-420"><a href="aa2-foreword.html">Foreword â Act two</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-653" id="menu-item-653"><a href="aa3-foreword.html">Foreword – Act three</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-315" id="menu-item-315"><a href="appendix-1.html">Running a discussion group</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-316" id="menu-item-316"><a href="appendix-2.html">Correspondence Courses</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71" id="menu-item-71"><a href="acts-1.html">Act One â Listen and Read</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-455" id="menu-item-455"><a href="act-two-listen-and-read.html">Act Two â Listen and Read</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-355" id="menu-item-355"><a href="act-3.html">Act Three â Listen and read</a></li>
</ul></div> </nav><!-- #site-navigation -->
</header><!-- #masthead -->
<div class="wrapper" id="main">
<div class="site-content" id="primary">
<div id="content" role="main">
<article class="post-4 page type-page status-publish hentry" id="post-4">
<header class="entry-header">
<h1 class="entry-title"></h1>
</header>
<div class="entry-content">
<h1><a href="https://amazingacts.net/wp-content/uploads/2015/06/cropped-cropped-AA1-small-e1433311800225.jpg"><img alt="AA1-cover" class=" wp-image-133 alignleft" height="130" sizes="(max-width: 83px) 100vw, 83px" src="wp-content/uploads/2014/03/AA1-cover-192x300.jpg" srcset="https://amazingacts.net/wp-content/uploads/2015/06/cropped-cropped-AA1-small-e1433311800225.jpg 192w, http://amazingacts.net/wp-content/uploads/2014/03/AA1-cover.jpg 562w" width="83"/></a><strong>R</strong><strong>ead and listen to Gerard Chrispin’s devotional and discipleship comments on the Acts of the Apostles.</strong></h1>
<p>All three books in this trilogy are now available for <a href="purchase.html" target="_blank">purchasing</a>, but you can read and listen online here freely.</p>
<p style="text-align: center;">_____________________</p>
</div><!-- .entry-content -->
<footer class="entry-meta">
</footer><!-- .entry-meta -->
</article><!-- #post -->
</div><!-- #content -->
</div><!-- #primary -->
</div><!-- #main .wrapper -->
<footer id="colophon" role="contentinfo">
<div class="site-info">
<a href="https://wordpress.org/" title="Semantic Personal Publishing Platform">Proudly powered by WordPress</a>
</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->
<!-- ngg_resource_manager_marker --><script src="wp-content/themes/twentytwelve/js/navigation0150.js?ver=20140711" type="text/javascript"></script>
<script src="wp-includes/js/wp-embed.min66f2.js?ver=4.7.5" type="text/javascript"></script>
<!-- WP Audio player plugin v1.9.6 - https://www.tipsandtricks-hq.com/wordpress-audio-music-player-plugin-4556/ -->
<script type="text/javascript">
        soundManager.useFlashBlock = true; // optional - if used, required flashblock.css
        soundManager.url = 'wp-content/plugins/compact-wp-audio-player/swf/soundmanager2.swf';
        function play_mp3(flg, ids, mp3url, volume, loops)
        {
            //Check the file URL parameter value
            var pieces = mp3url.split("|");
            if (pieces.length > 1) {//We have got an .ogg file too
                mp3file = pieces[0];
                oggfile = pieces[1];
                //set the file URL to be an array with the mp3 and ogg file
                mp3url = new Array(mp3file, oggfile);
            }

            soundManager.createSound({
                id: 'btnplay_' + ids,
                volume: volume,
                url: mp3url
            });

            if (flg == 'play') {
                    soundManager.play('btnplay_' + ids, {
                    onfinish: function() {
                        if (loops == 'true') {
                            loopSound('btnplay_' + ids);
                        }
                        else {
                            document.getElementById('btnplay_' + ids).style.display = 'inline';
                            document.getElementById('btnstop_' + ids).style.display = 'none';
                        }
                    }
                });
            }
            else if (flg == 'stop') {
    //soundManager.stop('btnplay_'+ids);
                soundManager.pause('btnplay_' + ids);
            }
        }
        function show_hide(flag, ids)
        {
            if (flag == 'play') {
                document.getElementById('btnplay_' + ids).style.display = 'none';
                document.getElementById('btnstop_' + ids).style.display = 'inline';
            }
            else if (flag == 'stop') {
                document.getElementById('btnplay_' + ids).style.display = 'inline';
                document.getElementById('btnstop_' + ids).style.display = 'none';
            }
        }
        function loopSound(soundID)
        {
            window.setTimeout(function() {
                soundManager.play(soundID, {onfinish: function() {
                        loopSound(soundID);
                    }});
            }, 1);
        }
        function stop_all_tracks()
        {
            soundManager.stopAll();
            var inputs = document.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].id.indexOf("btnplay_") == 0) {
                    inputs[i].style.display = 'inline';//Toggle the play button
                }
                if (inputs[i].id.indexOf("btnstop_") == 0) {
                    inputs[i].style.display = 'none';//Hide the stop button
                }
            }
        }
    </script>
<!-- Mirrored from amazingacts.net/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 26 Jun 2017 22:09:15 GMT -->
</body></html>