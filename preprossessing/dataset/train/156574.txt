<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="375030057061240566375" property="qc:admins"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>博宝艺术网-中国艺术品电商领导品牌！艺术品在线拍卖+艺术品在线交易+艺术媒体服务。</title>
<meta content="艺术品电商，艺术品，收藏品，山水画，油画，国画，名人字画，名家字画，书法，艺术品投资，艺术展览，书画展览，收藏家，艺术家" name="keywords"/>
<meta content="博宝艺术网是国内最早、最为专业的艺术品交易、在线拍卖、艺术媒体推广服务平台。平台服务于国内外千万藏家及数十万艺术家，在行业内取得了斐然的成绩。" name="description"/>
<link href="//news.artxun.com/external/modules/shou/templates/index/css/art_index.css" rel="stylesheet" type="text/css"/>
<link href="//news.artxun.com/external/modules/shou/templates/index/css/comm.css" rel="stylesheet" type="text/css"/>
<script src="//news.artxun.com/external/modules/shou/templates/index/js/jquery.js" type="text/javascript"></script>
<script src="//news.artxun.com/external/modules/shou/templates/index/js/comm.js" type="text/javascript"></script>
<script src="//news.artxun.com/external/modules/shou/templates/index/js/flexslider.js" type="text/javascript"></script>
<script src="//news.artxun.com/external/modules/shou/templates/index/js/jquery.cxscroll.min.js" type="text/javascript"></script>
<script type="text/javascript">
        var mobileAgent = new Array("iphone", "ipod", "ipad", "android", "mobile", "blackberry", "webos", "incognito", "webmate", "bada", "nokia", "lg", "ucweb", "skyfire");
        var browser = navigator.userAgent.toLowerCase();
        var isMobile = false;
        for (var i=0; i<mobileAgent.length; i++){
            if (browser.indexOf(mobileAgent[i])!=-1){
                isMobile = true;
                location.href = 'http://m.artxun.com/';
                break;
            }
        }
    </script>
<script language="javascript">
        var t1 = new Date().getTime();
    </script>
</head>
<body>
<div class="head_w">
<div class="head">
<form action="http://user.artxun.com/servlet/Login" id="_userinfo" method="post" name="showLogin">
<ul class="fl">
<li>您好！欢迎光临博宝</li>
<!--            <li>您好！欢迎光临博宝</li>
            <li><a target="_blank" class="head_btn01" href="http://artist.artxun.com/user/userm.jsp?type=login">登陆</a><a target="_blank" class="head_btn02" href="http://user.artxun.com/register/index.jsp">注册</a></li>
            -->
</ul>
</form>
<ul class="fr">
<!--<li class="t"><a href="">我的博宝</a><em></em>
            	<div class="dropdown">
                    <a target="_blank" href="http://mall.artxun.com/user-order_list.html" class="link">我的订单</a>
                    <a target="_blank" href="http://user.artxun.com/index.jsp" class="link">用户中心</a>
                    <a target="_blank" href="http://user.artxun.com/finance" class="link">财务中心</a>
                </div>
            </li>-->
<li class="t"><i class="icon_mobile"><em></em></i><a href="">关注博宝</a>
<div class="dropdown codes"><b class="arrows"></b><span class="code"></span><p>扫一扫 关注我们</p></div>
</li>
<!--<li class="t2"><a target="_blank" href="http://paimai.artxun.com">在线竞拍</a></li>-->
<!--<li><a target="_blank" href="http://jianbao.artxun.com/">鉴宝</a></li>-->
<li class="t"><i class="icon_mobile"><em></em></i><a href="">拍卖</a>
<div class="dropdown codes"><b class="arrows"></b><span class="code2"></span><p>扫一扫 关注我们</p></div>
</li>
<!--<li><a target="_blank" href="http://mall.artxun.com/">宝珍</a></li>-->
<!--<li class="t2"><a target="_blank" href="http://help.artxun.com/">帮助中心</a></li>-->
<!--<li class="site01"><a href="">网站导航</a><i></i></li>-->
</ul>
<div class="clear"></div>
</div>
</div>
<div class="banner">
<div class="w"></div>
</div>
<div class="nav_wrap" id="nav_wrap">
<div class="nav_bg"></div>
<div class="w">
<div class="index_nav">
<ul>
<li class="curr"><a class="list0" href="//www.artxun.com"></a></li>
<li><a class="list0 list02" href="//news.artxun.com" target="_blank"></a></li>
<li><a class="list0 list03" href="http://ypwj.artxun.com" target="_blank"></a></li>
<li><a class="list0 list04" href="//artist.artist.artxun.com/" target="_blank"></a></li>
<li><a class="list0 list05" href="//www.artxun.com/about/about.htm" target="_blank"></a></li>
</ul>
</div>
</div>
</div>
<div class="nav_wrap_float">
<div class="nav_bg"></div>
<div class="w">
<div class="index_nav">
<ul>
<li class="curr"><a class="list0" href="//www.artxun.com"></a></li>
<li><a class="list0 list02" href="//news.artxun.com" target="_blank"></a></li>
<li><a class="list0 list03" href="http://ypwj.artxun.com" target="_blank"></a></li>
<li><a class="list0 list04" href="//artist.artistys.artxun.com/" target="_blank"></a></li>
<li><a class="list0 list05" href="//www.artxun.com/about/about.htm" target="_blank"></a></li>
</ul>
</div>
</div>
</div>
<div class="flexslider">
<ul class="slides">
<li style="background:url(http://user.art138.com/news/26a/26ae71/aec44.jpg) 50% 0 no-repeat;height: 140px ;width: 1920px"><a href="https://open.yishu168.cn/special/goods-list?zcid=14053&amp;adsid=0&amp;afrom=shouye" target="_blank" title="艺术生活从一幅免费的作品开始"></a></li>
<li style="background:url(http://user.art138.com/news/766/76638a/f31d8.jpg) 50% 0 no-repeat;height: 140px ;width: 1920px"><a href="https://open.yishu168.cn/special/area-list#zc2&amp;afrom=shouye" target="_blank" title="让艺术成为一种生活"></a></li>
</ul>
</div>
<div class="in_draw" id="element_id">
<div class="News_title">
<div class="img"><img src="//news.artxun.com/external/modules/shou/templates/index/images/news.png"/></div>
<div class="txt"><p>推荐资讯</p><span></span></div>
</div>
<div class="box">
<ul class="list">
<li>
<a href="//news.artxun.com/1612433.shtml" target="_blank">艺品万家名家精品——刘旦宅</a>
<a href="//news.artxun.com/1612425.shtml" target="_blank">你想要得名家精品都在这里丨艺品万家</a>
<a href="//news.artxun.com/1612424.shtml" target="_blank">大写意花鸟画中的佼佼者——崔子虎</a>
</li>
<li>
<a href="//news.artxun.com/1612338.shtml" target="_blank">艺术的本质——中国美协会员陈晓华</a>
<a href="//news.artxun.com/1612191.shtml" target="_blank">日销售超百幅的著名艺术家——吴欢</a>
<a href="//news.artxun.com/1612247.shtml" target="_blank">“皇阿玛”的书法来了</a>
</li>
<li>
<a href="//news.artxun.com/1612180.shtml" target="_blank">“五十岁以上人的回忆 ”北影三朵金花——张金玲</a>
<a href="//news.artxun.com/1612182.shtml" target="_blank">什么是真正的人民艺术家</a>
<a href="//news.artxun.com/1612046.shtml" target="_blank">经典回忆——北影三朵金花 张金玲</a>
</li>
<li>
<a href="//news.artxun.com/1612041.shtml" target="_blank">手磨墨块用心创作精品  大写意水墨继承发扬传统</a>
<a href="//news.artxun.com/1612078.shtml" target="_blank">艺品万家告诉艺术家们如何快速稳定的销售</a>
<a href="//news.artxun.com/1612103.shtml" target="_blank">第一回·致青春——当代优秀青年艺术家邀请展</a>
</li>
<li>
<a href="//news.artxun.com/1612112.shtml" target="_blank">江南才子赵钲笔下的气韵生动</a>
<a href="//news.artxun.com/1612162.shtml" target="_blank">收藏家必备：一枚专属您的收藏印记——名家印章</a>
<a href="//news.artxun.com/1612171.shtml" target="_blank">我们卖的是文化 不是书画</a>
</li>
<li>
<a href="//news.artxun.com/1612173.shtml" target="_blank">世上无二丨吴欢</a>
<a href="//news.artxun.com/1612176.shtml" target="_blank">文字表现得最高艺术形式—书法</a>
<a href="//news.artxun.com/1610619.shtml" target="_blank">博艺传承——“艺品万家”二零一八年度艺术成果展 | 12月1日14：00开幕</a>
</li>
<li>
<a href="//news.artxun.com/1611214.shtml" target="_blank">【展讯】她·艺术——中国当代女画家邀请展</a>
<a href="//news.artxun.com/1611155.shtml" target="_blank">博宝艺品万家走进德钦采风写生汇报展 | 2月22日下午15：00开幕【博宝·展讯】</a>
<a href="//news.artxun.com/1610752.shtml" target="_blank">李晓明 | 大写意花鸟十二家·当代中青年大写意花鸟画学术特别邀请展</a>
</li>
<li>
<a href="//news.artxun.com/1610632.shtml" target="_blank">2018·中国印象——陈一茗写实油画展 | 11月27日15:00开幕</a>
<a href="//news.artxun.com/1610634.shtml" target="_blank">艺术收藏定会带来惊喜！30年前一辆车换得书画，如今富可敌国！</a>
<a href="//news.artxun.com/1608700.shtml" target="_blank">博宝·资讯 | 刘阔国画花鸟作品赏析：生意浮动的艺术情味</a>
</li>
<li>
<a href="//news.artxun.com/1608101.shtml" target="_blank">2018.05.19 14:30 | “弘扬经典”—— 中国人民大学艺术学院刘阔博士中国花鸟画研究文献展暨学术研讨会</a>
<a href="//news.artxun.com/1608354.shtml" target="_blank">【博宝·展讯】2018.05.13 | 她·艺术——中瑞女艺术家及艺术家庭作品交流展 HER ART——Sino-Swiss Exchange Exhibition of The Fe male Artists and Artistic Family</a>
<a href="//news.artxun.com/1608151.shtml" target="_blank">艺术惠民进万家系列活动博宝艺品万家与中国邮政共同为艺术品消费护航</a>
</li>
<li>
<a href="//news.artxun.com/1608175.shtml" target="_blank">博宝艺品万家签约艺术家冯少华作品被扬州文联永久收藏</a>
<a href="//news.artxun.com/1608129.shtml" target="_blank">博宝·资讯 | 钟鸣国画人物作品赏析：以形写神，率真有趣</a>
<a href="//news.artxun.com/1608099.shtml" target="_blank">赵钲谈艺 | 书画中技术含金量最高的是点</a>
</li>
<li>
<a href="//news.artxun.com/1603435.shtml" target="_blank">墨法自然︱艺品万家全国书法作品评选及书法展启动征集工作</a>
<a href="//news.artxun.com/1604015.shtml" target="_blank">墨法自然︱艺品万家全国书法作品评选及书法展启动</a>
<a href="//news.artxun.com/1607849.shtml" target="_blank">【博宝展讯】墨法·自然——艺品万家当代中国书法作品评选展</a>
</li>
<li>
<a href="//news.artxun.com/1607805.shtml" target="_blank">梅香自名苑  时代谱华章 ——中国当代书画博士后全国精品巡展（扬州站）</a>
<a href="//news.artxun.com/1607843.shtml" target="_blank">博宝·资讯 | 钟鸣国画人物作品赏析：笔墨恣肆写意，形象洒脱不羁</a>
<a href="//news.artxun.com/1607572.shtml" target="_blank">博宝·资讯 | 孙童国画人物作品赏析：丹青妙写佛心禅韵</a>
</li>
<li>
<a href="//news.artxun.com/1607691.shtml" target="_blank">【博宝快讯】2018.03.24 | 博宝艺品万家签约艺术家吴立伟作品亮相博宝美术馆</a>
<a href="//news.artxun.com/1607629.shtml" target="_blank">情系高原丨艺品万家签约艺术家吴立伟作品展</a>
<a href="//news.artxun.com/1607452.shtml" target="_blank">博宝·资讯 | 赵钲国画花鸟作品赏析：视角独特的艺术，温馨烂漫的画卷</a>
</li>
<li>
<a href="//news.artxun.com/1607486.shtml" target="_blank">博宝·资讯 | 刘阔国画花鸟作品赏析：色彩艳丽，美妙无限</a>
<a href="//news.artxun.com/1607543.shtml" target="_blank">博宝·资讯 | 陈敬友国画花鸟作品赏析：优雅恬淡的诗化意境</a>
<a href="//news.artxun.com/1607573.shtml" target="_blank">博宝·资讯 | 李宽国画人物作品赏析：关注现实，直面人生</a>
</li>
<li>
<a href="//news.artxun.com/1607345.shtml" target="_blank">博艺宝春拍 | 北京博艺宝2018迎春书画拍卖会</a>
<a href="//news.artxun.com/1607483.shtml" target="_blank">作为中国邮政战略合作伙伴，博宝艺品万家参与“绿水青山 最美邮路”系列主题赛</a>
<a href="//news.artxun.com/1607135.shtml" target="_blank">【博宝展讯】绘事文心 ——南北美术学博士迎春邀请展</a>
</li>
<li>
<a href="//news.artxun.com/1607413.shtml" target="_blank">赵钲谈艺 | 中国花鸟画中群众喜爱的吉祥题材</a>
<a href="//news.artxun.com/1607199.shtml" target="_blank">赵钲——诗书画“三绝”饮誉艺林</a>
<a href="//news.artxun.com/1605496.shtml" target="_blank">博宝·资讯 | 王晓辉国画人物作品赏析：诗酒人生自逍遥</a>
</li>
<li>
<a href="//news.artxun.com/1605287.shtml" target="_blank">博宝·资讯 | 王晓辉国画人物作品赏析：把写生的人物意象化，赋予人物特定的生命方式</a>
<a href="//news.artxun.com/1605286.shtml" target="_blank">博宝·资讯 | 王晓辉国画作品赏析：精湛笔墨让艺术升华，空灵雄浑过目不忘</a>
<a href="//news.artxun.com/1605495.shtml" target="_blank">博宝·资讯 | 王晓辉古典人物画作品赏析：笔墨情趣浓郁，人物形象鲜明</a>
</li>
<li>
<a href="//news.artxun.com/1605521.shtml" target="_blank">【博宝展讯】李宽——博艺传承丨“艺品万家”2017年度艺术成果展</a>
<a href="//news.artxun.com/1605638.shtml" target="_blank">博宝·资讯 | 王晓辉水墨人物作品赏析：表现豪放而壮美的艺术风格</a>
<a href="//news.artxun.com/1607160.shtml" target="_blank">博宝·资讯 | 王晓辉国画人物作品赏析：古韵古香，古意悠远</a>
</li>
<li>
<a href="//news.artxun.com/1607379.shtml" target="_blank">“大凉山下的自在天性”——谈艺品万家签约画家李宽老师水墨人物创作</a>
<a href="//news.artxun.com/1606958.shtml" target="_blank">在水墨实验中谱写现实的华章——李宽</a>
<a href="//news.artxun.com/1606896.shtml" target="_blank">博宝·资讯 | 李宽国画人物作品赏析：温润质朴，散发出浓郁的乡土气息</a>
</li>
<li>
<a href="//news.artxun.com/1606214.shtml" target="_blank">李宽国画人物作品赏析：推崇人性真善美</a>
<a href="//news.artxun.com/1607300.shtml" target="_blank">一年时间缔造如此销量传奇，你相信吗？</a>
<a href="//news.artxun.com/1607270.shtml" target="_blank">中国书法作品销量“里程碑式”人物</a>
</li>
<li>
<a href="//news.artxun.com/1607151.shtml" target="_blank">【博宝展讯】她·时代——中国当代女画家提名展</a>
<a href="//news.artxun.com/1607291.shtml" target="_blank">博宝·资讯 | 孙培增国画人物作品赏析：形象高古，气质飘逸</a>
<a href="//news.artxun.com/1607289.shtml" target="_blank">博宝·资讯 | 席军国画花鸟作品赏析：造型简洁，色泽古朴，凸显清新洒脱的艺术韵味</a>
</li>
<li>
<a href="//news.artxun.com/1607184.shtml" target="_blank">艺品万家签约画家赵钲戊戌新春寄语：书画家最终要对得起自己</a>
<a href="//news.artxun.com/1607063.shtml" target="_blank">博宝·资讯 | 席军国画花鸟作品赏析：无限惊奇，三思而后</a>
<a href="//news.artxun.com/1606959.shtml" target="_blank">【博宝新闻】“文化惠民迎新春 万幅书画送万家”暨中国·博宝艺术网展览中心落成典礼</a>
</li>
<li>
<a href="//news.artxun.com/1607038.shtml" target="_blank">博宝·资讯 | 孙培增国画人物作品赏析：笔墨妍丽，造型准确</a>
<a href="//news.artxun.com/1606975.shtml" target="_blank">博宝·资讯 | 李秋喜国画山水画赏析：意境唯美，充满古韵</a>
<a href="//news.artxun.com/1606962.shtml" target="_blank">博宝·资讯 | 刘向明国画人物作品赏析：品格高雅， 情态传神</a>
</li>
<li>
<a href="//news.artxun.com/1606895.shtml" target="_blank">水墨人物画新锐画家刘结锋</a>
<a href="//news.artxun.com/1606953.shtml" target="_blank">博宝·资讯 | 孙童国画人物作品赏析：古朴典雅，凸显出向善向美的高贵仪态</a>
<a href="//news.artxun.com/1606898.shtml" target="_blank">重磅：中传媒教授、中美协会员李宽巨幅代表作品将以0元起拍！</a>
</li>
<li>
<a href="//news.artxun.com/1606901.shtml" target="_blank">博宝·资讯 | 席军国画花鸟画赏析：细腻清新，笔墨灵动</a>
<a href="//news.artxun.com/1606924.shtml" target="_blank">博宝·资讯 |  刘阔国画花鸟作品赏析 ：造物生动 ，蕴含真情</a>
<a href="//news.artxun.com/1606926.shtml" target="_blank">博宝·资讯 | 苏军国画山水作品赏析：危岩高耸，鬼斧神工</a>
</li>
<li>
<a href="//news.artxun.com/1606925.shtml" target="_blank">博宝·资讯 | 刘子展国画山水作品赏析：山色空濛，再现出一种乡野田园之风</a>
<a href="//news.artxun.com/1606758.shtml" target="_blank">博宝·资讯 | 江南花鸟画名家赵钲：在大写意中透出空明的境界</a>
<a href="//news.artxun.com/1606716.shtml" target="_blank">陈敬友：精彩绽放源于坚守</a>
</li>
<li>
<a href="//news.artxun.com/1606569.shtml" target="_blank">刘阔：把鸟藏在心里</a>
<a href="//news.artxun.com/1606818.shtml" target="_blank">将鼠画的如此可爱伶俐，席军是如何做到的？</a>
<a href="//news.artxun.com/1606499.shtml" target="_blank">【博宝展讯】翰墨传承 ——2018迎新年当代中国画名家邀请展</a>
</li>
<li>
<a href="//news.artxun.com/1606573.shtml" target="_blank">寒冬里的温暖 ---- 记博宝艺术网董事长王刚先生</a>
<a href="//news.artxun.com/1606520.shtml" target="_blank">清华“追牛教授”画牛</a>
<a href="//news.artxun.com/1605250.shtml" target="_blank">【博宝展讯】吾望浮云一一王晓辉大写意作品展</a>
</li>
<li>
<a href="//news.artxun.com/1606406.shtml" target="_blank">【博宝快讯】吾望浮云一一王晓辉大写意作品展于京举办,中国书画艺术界半壁江山齐聚博宝艺术网</a>
<a href="//news.artxun.com/1606279.shtml" target="_blank">【博宝展讯】墨•境——中央美术学院优秀青年艺术家推介展</a>
<a href="//news.artxun.com/1606457.shtml" target="_blank">【博宝展讯】雪域神韵——艺品万家签约画家王贵邱作品展</a>
</li>
<li>
<a href="//news.artxun.com/1606354.shtml" target="_blank">赏字品词！晏殊诗词书法精品百元购</a>
<a href="//news.artxun.com/1605922.shtml" target="_blank">【博宝展讯】山水墨境丨艺品万家签约艺术家联展</a>
<a href="//news.artxun.com/1604816.shtml" target="_blank">【博宝展讯】博艺传承丨“艺品万家”2017年度艺术成果展</a>
</li>
<li>
<a href="//news.artxun.com/1605251.shtml" target="_blank">一幅画为什么能值钱？</a>
<a href="//news.artxun.com/1604593.shtml" target="_blank">【博宝展讯】砚边传承·水墨皮影丨艺品万家签约艺术家孙永猛个人作品展</a>
<a href="//news.artxun.com/1604011.shtml" target="_blank">【博宝展讯】问道中艺|中国艺术研究院2016级艺术硕士邀请展</a>
</li>
<li>
<a href="//news.artxun.com/1602679.shtml" target="_blank">“博宝艺品万家”开启艺术新时代 ——江国生在《醉墨心相——中国画名家邀请展》艺术沙龙上的发言</a>
<a href="//news.artxun.com/1602805.shtml" target="_blank">翰逸鸢飞丹青扇舞——董伟荣 郭建华花鸟作品展在潍坊十笏园全国名家展示馆开幕</a>
<a href="//news.artxun.com/1602577.shtml" target="_blank">【博宝展讯】生•墨|艺品万家签约艺术家刘结锋个人作品展</a>
</li>
<li>
<a href="//news.artxun.com/1596895.shtml" target="_blank">【博宝快讯】醉墨心相|中国画名家邀请展在京隆重开幕</a>
<a href="//news.artxun.com/1595843.shtml" target="_blank">【博宝展讯】翰墨传承 |当代名家优秀作品邀请展</a>
<a href="//news.artxun.com/1596513.shtml" target="_blank">【博宝快讯】翰墨传承丨当代名家优秀作品邀请展在京隆重开幕</a>
</li>
</ul>
</div>
</div>
<div class="floors fnews">
<div class="w">
<div class="ftitle"></div>
<div class="infos">
<ul>
<li>
<a href="https://open.yishu168.cn/mini/broadcast-list?afrom=ggw1201030171" target="_blank"><div class="info_l matte">
<span class="arrow"></span>
<h3>艺品万家直播——不一样的艺术直播</h3>
<p>艺术品对于中国大众来说，不仅仅只是一张纸，它承载了中国几千年文化的传承，承载了中国大众的文化思想，体现出的文化进化与发展的背景、文化精神与文化立场，以及应有的文化情怀与追求等，这才是我们应该去关注的。</p>
</div></a>
<div class="info_r pics"><img src="http://user.art138.com/news/bad/badf27/b0f8e.jpg" style="width: 500px;height: 300px"/></div>
</li>
<li>
<a href="https://mp.weixin.qq.com/s/WNd5WFiMBfoEcUgELvq6iQ" target="_blank"><div class="info_r matte">
<span class="arrow"></span>
<h3>艺品万家.大家风范—中国水墨名家邀请展”隆重开幕</h3>
<p>2020年10月18日15：00，艺品万家·大家风范——中国水墨名家邀请展在博宝美术馆隆重开幕！

本次展览邀请了十四位中国当代优秀艺术家：曾宓、崔振宽、王培东  、姜宝林 、李乃宙、王玉良、戴顺智、何家英、杨晓阳、李晓军、徐里、洪潮、范存刚、臧家伟（按年龄大小排序）。</p>
</div></a>
<div class="info_l pics"><img src="http://user.art138.com/news/f35/f35ad0/33bb5.jpg" style="width: 500px;height: 300px"/></div>
</li>
<li>
<a href="https://mp.weixin.qq.com/s/Jc-2rUcJHa7TbmsuQhx-KA" target="_blank"><div class="info_l matte">
<span class="arrow"></span>
<h3>艺品万家推荐——值得收藏的优秀艺术家</h3>
<p>本期艺品万家从个人风格，艺术角度，市场角度，为大家挑选了十位具有收藏价值的艺术家，且具有一定影响力的艺术家，将他们的作品及个人风采展示给大众，让大众通过艺品万家平台能够快速近距离的接触了解这些艺术家的作品及艺术风格！

推荐艺术家

王千里  张金玲  马保林  赵钲  冯少华  吕一中  张灵  赵纯厚  王雪峰  苏金成（按年龄排序）</p>
</div></a>
<div class="info_r pics"><img src="http://user.art138.com/news/8ee/8eee94/152ea.jpg" style="width: 500px;height: 300px"/></div>
</li>
</ul>
<div class="clear"></div>
</div>
<div class="more_link"><a class="links" href="//news.artxun.com/" target="_blank">更多 ···</a></div>
</div>
</div>
<div class="floors fshow">
<span class=""></span>
<div class="w">
<div class="ftitle ftitle2"></div>
<div class="show_wrap">
<div class="fshade"><a href="http://ypwj.artxun.com" target="_blank"><img src="//user.art138.com/news/6d8/6d8282/ce18e.jpg@1200w_474h_1e_1c"/></a></div>
<div class="show_list">
<ul>
<li>
<div class="pic"><img src="//user.art138.com/news/13c/13cb27/f001b.jpg@380w_380h_1e_1c"/></div>
<div class="name">国画</div>
<a href="http://ypwj.artxun.com/index.php?module=yipin&amp;act=pc_list&amp;op=show&amp;kind=1" target="_blank"><div class="buynow">立即前往</div></a>
</li>
<li>
<div class="pic"><img src="//user.art138.com/news/a0e/a0ee6c/8131a.jpg@380w_380h_1e_1c"/></div>
<div class="name">油画</div>
<a href="http://ypwj.artxun.com/index.php?module=yipin&amp;act=pc_list&amp;op=show&amp;kind=2" target="_blank"><div class="buynow">立即前往</div></a>
</li>
<li>
<div class="pic"><img src="//user.art138.com/news/751/751cd4/c460a.jpg@380w_380h_1e_1c"/></div>
<div class="name">书法</div>
<a href="http://ypwj.artxun.com/index.php?module=yipin&amp;act=pc_list&amp;op=show&amp;kind=3" target="_blank"><div class="buynow">立即前往</div></a>
</li>
</ul>
<div class="clear"></div>
</div>
</div>
</div>
</div>
<div class="floors arts">
<div class="w">
<div class="ftitle ftitle3"></div>
<div class="art_list_l">
<div class="art_main" style="display:block">
<h3>刘阔</h3>
<div class="art_main_p"><p>刘阔 1988年生于北京，青年花鸟画家，艺品万家签约艺术家。中国人民大学哲学美学博士，获艺术学硕士，文学学士学位。专攻小写意花鸟画，兼善书法篆刻以及古典诗词。已出版多部个人专著，著有：《刘阔工笔花鸟画技法-花卉篇上》，《刘阔工笔花鸟画技法-花卉篇下》，《刘阔工笔花鸟画技法-禽鸟草虫篇上》，《刘阔工笔花鸟画技法-禽鸟草虫篇下》，《刘阔白描没骨花鸟画谱》，《中国历代线描人物画精选》、《刘阔画辑》、《刘阔画选》、《八大山人册页精选》，《人美画谱-恽寿平》《人美画谱-华新罗》《人美画谱-赵之谦》等十余部学术专著。科研成果作品刊登于《书画研究》、《东方艺术》、《艺术品》、《美术观察》、《美术报》、《中国水墨》、《中国书画》、《凤凰书画》等多种学术刊物，并有多篇学术论文发表于世。其花鸟画作品风格清新隽秀、含蓄古雅，收藏于海内外。</p>
</div>
<div class="art_main_img">
<h4><a class="more_zp" href="//artist.artxun.com/zuopin/" target="_blank">更多 ···</a>推荐作品：</h4>
<a class="tjzp" href="https://shuhua.yishu168.cn/xrvgg/yllhsn/yipin_wap_list_goods?id=998468" target="_blank" title="推荐作品"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552471486740225.jpeg"/></a>
<a class="tjzp" href="https://shuhua.yishu168.cn/xrvgg/yllhsn/yipin_wap_list_goods?id=902538" target="_blank" title="推荐作品"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552471490842697.jpeg"/></a>
</div>
</div>
<div class="art_main">
<h3>赵钲</h3>
<div class="art_main_p"><p>赵钲 1955年2月出生，江苏省兴化市昭阳镇人。现为中国美协家协会会员，艺品万家签约艺术家，江苏省词协会办公室主任，《江海诗词》美编，国家中级美术师，江南诗画院常务理事，金陵书画院一级画师、中国工艺美术家协会会员。江苏省美术家协会会员，省书法家协会会员，省直属机关书法家协会会员，中华诗词学会会员，全球汉诗总会理事。 简介 郑板桥的乡风孕育了他的艺术灵感，自幼即拜乡贤乔维良先生为师学习猿猴的画法。赵钲先生以“诗书画三绝”深得海内外爱好者赞誉，成为江南花鸟画名家。他笔下的龙虾、芦雁、蔬果、梅花都各具特色 作品相继载入《海峡两岸书画作品大观》、《国际书画名家精选》、《”99回归中国名家书画集》等大型画册。名录入《2000·中国风·杰出人物特集》、 《世界华人文学艺术界名人录》等20余部大型辞书。</p>
</div>
<div class="art_main_img">
<h4><a class="more_zp" href="//artist.artxun.com/zuopin/" target="_blank">更多 ···</a>推荐作品：</h4>
<a class="tjzp" href="https://shuhua.yishu168.cn/veogrs/vifyw/xjboih/yipin_wap_list_goods?id=459676" target="_blank" title="推荐作品"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552471550542042.jpeg"/></a>
<a class="tjzp" href="https://shuhua.yishu168.cn/veogrs/vifyw/xjboih/yipin_wap_list_goods?id=459675" target="_blank" title="推荐作品"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552471553670283.jpeg"/></a>
</div>
</div>
<div class="art_main">
<h3>王雪峰</h3>
<div class="art_main_p"><p>王雪峰  1972年11月出生于江苏扬州, 2003年获南京师范大学美术学院中国画人物画创作方向硕士学位，2007年获东南大学艺术学系艺术学博士学位, 2010年中央美术学院美术学博士后流动站出站。现为中国美术馆研究馆员、收藏部副主任、中国美术馆展览评审委员会委员、中国美术家协会会员、中国文艺评论家协会会员、博宝艺品万家签约艺术家。</p>
</div>
<div class="art_main_img">
<h4><a class="more_zp" href="//artist.artxun.com/zuopin/" target="_blank">更多 ···</a>推荐作品：</h4>
<a class="tjzp" href="https://shuhua.yishu168.cn/xrvgg/yllhsn/yipin_wap_list_goods?id=1070975" target="_blank" title="推荐作品"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552471606233442.jpeg"/></a>
<a class="tjzp" href="https://shuhua.yishu168.cn/veogrs/vifyw/xjboih/yipin_wap_list_goods?id=1033813" target="_blank" title="推荐作品"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552471609873032.jpeg"/></a>
</div>
</div>
<div class="art_main">
<h3>苏军</h3>
<div class="art_main_p"> <p>苏军，艺品万家签约艺术家，14年毕业于中央美术学院中国画山水专业，先后获得中央美术学院优秀作业三等奖，中央美术学院春季写生作品展一等奖，中央美术学院优秀毕业作品三等 奖，第一届新绎之星优秀作品三等奖。中海油奖学金，国家励志奖学金，衣恋奖学金。画展有：《为中国画》全国高等艺术院校山水专业师生优秀作品展（中央美术 学院），《千里之行》第七届中国九大美院优秀毕业生作品展（武汉），《同行同至》第一届新绎之星优秀作品展（今日美术馆），《炎黄之春》中国当代中国画家 邀请展（炎黄艺术馆），《古意新相》乙未冬月水墨邀请展（炎黄艺术馆），《自主空间》全国高等艺术院校山水专业教学写生展（西安）。毕业作品被中央美术学 院美术馆永久收藏。</p>
</div>
<div class="art_main_img">
<h4><a class="more_zp" href="//artist.artxun.com/zuopin/" target="_blank">更多 ···</a>推荐作品：</h4>
<a class="tjzp" href="https://shuhua.yishu168.cn/xrvgg/yllhsn/yipin_wap_list_goods?id=724121" target="_blank" title="推荐作品"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552471649229020.jpeg"/></a>
<a class="tjzp" href="https://shuhua.yishu168.cn/veogrs/vifyw/xjboih/yipin_wap_list_goods?id=724116" target="_blank" title="推荐作品"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552471654292545.jpeg"/></a>
</div>
</div>
<div class="art_main">
<h3>尹伊</h3>
<div class="art_main_p"><p>   尹伊，女，出生于江苏省南通市。南京师范大学美术学院艺术硕士。宿迁市美术家协会副主席。中国美术家协会会员，艺品万家签约艺术家。江苏省书法家协会会员。江苏省文联书画研究中心研究员。中国当代女子画会会员。中国女书画家联盟成员。多家艺术机构签约画家。发表的论文：《论工笔人物画色彩装饰性》、《中国画的节奏》、《我看现代主义、后现代主义与当代中国画》、《浅谈“六法”与中国画创作》、《工笔人物画教学改革研究》、《浅析中国工笔人物画实践教学》。</p>
</div>
<div class="art_main_img">
<h4><a class="more_zp" href="//artist.artxun.com/zuopin/" target="_blank">更多 ···</a>推荐作品：</h4>
<a class="tjzp" href="https://shuhua.yishu168.cn/xrvgg/yllhsn/yipin_wap_list_goods?id=748941" target="_blank" title="推荐作品"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552471710338252.jpeg"/></a>
<a class="tjzp" href="https://shuhua.yishu168.cn/xrvgg/yllhsn/yipin_wap_list_goods?id=745821" target="_blank" title="推荐作品"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552471713751230.jpeg"/></a>
</div>
</div>
<div class="art_main">
<h3>张亮</h3>
<div class="art_main_p"><p>张 亮 1983年生于湖北襄阳，现任职于北京工商大学嘉华学院，艺品万家签约艺术家。毕业于湖北美术学院中国画系和中国艺术研究院研究生院，分别获学士和硕士学位，师从陈孟昕教授。2015-2016年曾研修于工笔画研究院何家英首届人物画创作班。中国工笔画学会会员，湖北省美术家协会会员。多幅作品散见于《中国画观察》《国画经典》《艺术品鉴》《神州集粹》《国画大家》《台声》等专业艺术杂志，多幅作品被美术馆和收藏家收藏。</p>
</div>
<div class="art_main_img">
<h4><a class="more_zp" href="//artist.artxun.com/zuopin/" target="_blank">更多 ···</a>推荐作品：</h4>
<a class="tjzp" href="https://shuhua.yishu168.cn/xrvgg/yllhsn/yipin_wap_list_goods?id=1069204" target="_blank" title="推荐作品"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552471738889538.jpeg"/></a>
<a class="tjzp" href="https://shuhua.yishu168.cn/veogrs/vifyw/xjboih/yipin_wap_list_goods?id=619473" target="_blank" title="推荐作品"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552471742972168.jpeg"/></a>
</div>
</div>
</div>
<div class="art_list_r">
<div class="more"><a href="//artist.artxun.com/list/" target="_blank">更多艺术家 ···</a></div>
<ul>
<li>
<div class="img"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552468948870622.jpeg"/></div>
<div class="font"><h3>刘阔</h3><a class="seebtn" href="//m.artxun.com/artist/liukuo/" target="_blank"><i></i><span>查看详情</span></a></div>
</li>
<li class="cur">
<div class="img"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552470135216657.jpeg"/></div>
<div class="font"><h3>赵钲</h3><a class="seebtn" href="//m.artxun.com/artist/diaozheng/" target="_blank"><i></i><span>查看详情</span></a></div>
</li>
<li>
<div class="img"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552470184261392.jpeg"/></div>
<div class="font"><h3>王雪峰</h3><a class="seebtn" href="//m.artxun.com/artist/wangxuefeng/" target="_blank"><i></i><span>查看详情</span></a></div>
</li>
<li>
<div class="img"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552470224818649.jpeg"/></div>
<div class="font"><h3>苏军</h3><a class="seebtn" href="//m.artxun.com/artist/sujun/" target="_blank"><i></i><span>查看详情</span></a></div>
</li>
<li>
<div class="img"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552470259233366.jpeg"/></div>
<div class="font"><h3>尹伊</h3><a class="seebtn" href="//m.artxun.com/artist/yinyia/" target="_blank"><i></i><span>查看详情</span></a></div>
</li>
<li>
<div class="img"><img src="//img.art138.com/data/files/old_shop/gh_0/store_0/jianbao/20190313/1552470303858416.jpeg"/></div>
<div class="font"><h3>张亮</h3><a class="seebtn" href="//m.artxun.com/artist/zhangliangd/" target="_blank"><i></i><span>查看详情</span></a></div>
</li>
</ul>
</div>
<div class="clear"></div>
</div>
</div>
<div class="pasts">
<div class="pasts_hd"></div>
<div class="hl_list bd">
<ul>
<li>
<div class="img"><img src="https://user.art138.com/news/7fe/7fe3b7/cfd88.jpg@387w_252h_1e_1c"/></div>
<div class="hl_name"><p>清华大学中国工笔画高端创新人才交流培养平台展览</p><a class="seebtn" href="//news.artxun.com/1611485.shtml" target="_blank"><i></i><span>查看详情</span></a></div>
</li>
<li>
<div class="img"><img src="https://user.art138.com/news/fe5/fe5dea/a058f.jpg@387w_252h_1e_1c"/></div>
<div class="hl_name"><p>【展讯】艺逐春风—博宝艺品万家2019年签约艺术家联展</p><a class="seebtn" href="//news.artxun.com/1611480.shtml" target="_blank"><i></i><span>查看详情</span></a></div>
</li>
<li>
<div class="img"><img src="https://user.art138.com/news/5ad/5ad2be/ff73d.jpg@387w_252h_1e_1c"/></div>
<div class="hl_name"><p>“画本归真—山西省当代工笔画艺术研究院第三届小品展”在北京博宝美术馆隆重开幕</p><a class="seebtn" href="//news.artxun.com/1611468.shtml" target="_blank"><i></i><span>查看详情</span></a></div>
</li>
<li>
<div class="img"><img src="https://user.art138.com/news/e21/e21573/f9c59.jpg@387w_252h_1e_1c"/></div>
<div class="hl_name"><p>画本归真——山西省当代工笔画艺术研究院第三届小品展 | 4月13日15:30开幕</p><a class="seebtn" href="//news.artxun.com/1611424.shtml" target="_blank"><i></i><span>查看详情</span></a></div>
</li>
<li>
<div class="img"><img src="https://user.art138.com/news/ebe/ebebf9/436fe.jpg@387w_252h_1e_1c"/></div>
<div class="hl_name"><p>3月8日女王节，“她·艺术——中国当代女画家邀请展”于京温馨绽放</p><a class="seebtn" href="//news.artxun.com/1611257.shtml" target="_blank"><i></i><span>查看详情</span></a></div>
</li>
<li>
<div class="img"><img src="https://user.art138.com/news/066/066a5b/188c9.jpg@387w_252h_1e_1c"/></div>
<div class="hl_name"><p>【展讯】参展艺术家介绍·第七期 | 她·艺术——中国当代女画家邀请展</p><a class="seebtn" href="//news.artxun.com/1611235.shtml" target="_blank"><i></i><span>查看详情</span></a></div>
</li>
</ul>
<div class="clear"></div>
</div>
</div>
<div class="floors about_us">
<div class="w">
<div class="ftitle ftitle4"></div>
<div class="about">
<p>博宝艺术网于2006年创建，至今已经走过了14个年头，一直致力于把艺术品通过互联网这个时代的媒介传递给大众。特别是以“让艺术品走进千家万户”为宗旨的“艺品万家”项目开启之后，更是颠覆了传统的书画艺术品领域。
公司践行着“让艺术品走进千家万户”的创新性理念。
坚守着“绝不以假乱真  更不以次充好”的经营理念。
我们致力于通过互联网的方式，颠覆中国传统艺术市场，让艺术市场从小众变为大众，培育真正的艺术品市场。</p>
</div>
<div class="more_link"><a class="links" href="//www.artxun.com/about/about.htm" target="_blank">查看详情</a></div>
</div>
</div>
<div class="footer_warp">
<div class="footer">
<div class="footer_t" style="padding-top:0">
<!--<h3><a target="_blank" href="//www.artxun.com/about/friend.htm">更多…</a>友情链接 LINKS</h3>-->
<div class="footer_t_m">
<ul>
<li><a href="http://www.pmv.cn/" target="_blank">中华收藏网</a></li>
<li><a href="http://www.wenbo.cc/index.asp" target="_blank">中国文博网</a></li>
<li><a href="http://yipin.artxun.com/" target="_blank">艺品万家</a></li>
<li><a href="http://www.dahuajia.com/" target="_blank">大画家网</a></li>
<li><a href="http://lizhuo.artistys.artxun.com/" target="_blank">李卓</a></li>
<li><a href="http://mazhangcheng.artistys.artxun.com/" target="_blank">马章乘</a></li>
<li><a href="http://liudaweib.artistys.artxun.com/" target="_blank">刘大为</a></li>
</ul>
</div>
</div>
<div class="footer_b" style="padding-top:0">
<!--<div class="footer_b1"> <a href="http://www.artxun.com/about/about.htm">关于我们</a>|<a target="_blank" href="http://www.artxun.com/about/service.htm">博宝服务</a>|<a target="_blank" href="http://www.artxun.com/about/payment.htm">汇款方式</a>|<a target="_blank" href="http://www.artxun.com/about/tradeflow.htm">交易流程</a>|<a target="_blank" href="http://www.artxun.com/about/link.htm">联系方式</a>|<a target="_blank" href="http://www.artxun.com/about/sitead.htm">广告业务</a>|<a target="_blank" href="http://www.artxun.com/about/copyright.htm">版权说明</a>|<a target="_blank" href="http://www.artxun.com/about/mianze.htm">免责声明 </a>|<a target="_blank" href="http://www.artxun.com/about/privacy.htm">隐私声明</a>|<a target="_blank" href="http://www.artxun.com/about/help.htm">帮助中心</a>|<a target="_blank" href="http://www.artxun.com/about/sitemap.htm">网站导航</a>|<a target="_blank" href="http://mall.artxun.com/daili.html">合作加盟</a>|<a target="_blank" href="http://www.artxun.com/about/job.htm">博宝诚聘</a> </div>-->
<div class="footer_bm">
<p>服务热线：010-68703488 E-mail：webmaster@artxun.com</p>
<p>工作时间： 周一至周五：9:00-21:00；周六，日：9:00-18:00；如需来公司洽谈业务，请提前2天（48小时）电话预约。</p>
<p>Copyright 2006-2011 版权所有 ARTXUN.COM  <a href="http://beian.miit.gov.cn" style="color: #a0a0a0" target="_blank">京ICP备09001523号-41</a>  京ICP证070566号  京公网安备11010802020683号</p></div>
<div class="footer_b2"><a href="http://www.miibeian.gov.cn/" target="_blank"><img alt="" src="http://news.artxun.com/external/modules/shou/templates/index/images/foot_01.jpg"/></a><a href="http://net.china.cn/chinese/index.htm" target="_blank"><img alt="" src="http://news.artxun.com/external/modules/shou/templates/index/images/foot_02.jpg"/></a><a href="http://www.cyberpolice.cn/wfjb/html/index.shtml" target="_blank"><img alt="" src="http://news.artxun.com/external/modules/shou/templates/index/images/foot_03.jpg"/></a></div>
</div>
</div>
</div>
<input type="hidden" value="0.067565202713013"/>
<input class="TimeShow" style="color: #2a1010;width:100%;background-color: #311717" type="text" value=""/>
<div style="display: none">
<script language="JavaScript" src="https://s13.cnzz.com/z_stat.php?id=1262160681&amp;web_id=1262160681"></script>
</div>
<div class="kf2">
<a href="javascript:;">
<img class="img" id="top_img" src="https://img.art138.com/ypwj/admin/2019/11/18/bNtBgdvyjpB7ASVKXOZ89SXKYzNi.png"/>
<div class="let-img"><img height="135" src="https://img.art138.com/ypwj/admin/2020/11/27/3pYNRunMMstQ5ozXLqgnRH9AM2Fm.png" width="145"/></div>
</a>
</div>
<div class="nav_em"><img height="108" src="https://img.art138.com/ypwj/admin/2019/11/19/3mh6lPbY6F9H73eyg5Bw9nqrZDE1.png" width="108"/></div>
<div class="sid_toolbar">
<a class="top_link" href="javascript:;">
<i></i>
<p>返回顶部</p>
</a>
</div>
<style>
    .kf2{
        position: fixed;
        right: 9px;
        bottom: 434px;
        width: 108px;
        height: 90px;
        z-index: 100;
    }
    .kf2 .img{
        position: relative;
    }
    .kf2 a:hover .let-img{
        display: block
    }
    .kf2 .let-img{
        position: absolute;
        left: -145px;
        top: 0px;
        display: none;
    }
</style>
<!--<script id="xiaonengjs" src="//visitor.ntalker.com/visitor/js/xiaoneng.js?siteid=kf_20187"></script>-->
<script>
  $(document).ready(function() {
    //to-top
    $(".sid_toolbar").css("display","none");

    $(window).scroll(function(){

      var top = $(window).scrollTop();
      if(top > 400){
        $(".sid_toolbar").fadeIn();
      }else{
        $(".sid_toolbar").fadeOut();
      }

      $(".sid_toolbar .top_link").click(function(){
        $("html,body").stop().animate(
            {"scrollTop":0},400);
      });
    })
  })
</script>
</body>
<script type="text/javascript">
$(document).ready(function(){
    //head_w
	$(".head_w .fr > li").hover(function(){
		$(this).children(".dropdown").show();
	},function(){
		$(".dropdown").hide();
	});
	//show_list
	$(".show_list li").hover(function(){
		$(this).addClass("curr").siblings().removeClass("curr");
	},function(){
		$(this).removeClass("curr");
	});
	//art_list_r
	$(".art_list_r li").hover(function(){
		$(this).addClass("cur").siblings().removeClass("cur");
		$i=$(this).index();
		$(".art_main").hide().eq($i).show();
    });
    //文字省略...
    $(".fnews .matte p").each(function(){
        var maxwidths=170;
        if($(this).text().length>maxwidths){
            $(this).text($(this).text().substring(0,maxwidths));
            $(this).html($(this).html()+'…');
        }
    });
    //Scroll
    $('#element_id').cxScroll({
        direction: 'bottom',
        speed: 500,
        time: 1800,
        plus: false,
        minus: false
    });
	//pasts
	var a = $(".hl_list ul li").length;
	$(".hl_list ul").html($(".hl_list ul").html()+$(".hl_list ul").html());
	var mytimer = 0;
	var nowleft = 0;
	var long1 = $(".hl_list ul li").eq(a).offset().left;
	var long2 = $(".hl_list ul").offset().left;
	var long = long2 - long1;
	shebiao();
	function shebiao(){
		window.clearInterval(mytimer);
		mytimer = window.setInterval(function(){
			if(nowleft== long){
				$(".hl_list ul").css("left",0);
				nowleft = 0;
			}else{
				nowleft = nowleft - 1;
				$(".hl_list ul").css("left",nowleft);
			}
		},18);
	}
	$(".hl_list li").mouseenter(function(){
		$(this).children("div").fadeIn(200);
		window.clearInterval(mytimer);
	});
	$(".hl_list li").mouseleave(function(){
		$(this).children(".hl_name").fadeOut(10);
		shebiao();
	})
});
window.onscroll=function(){
    if($(document).scrollTop()>900){
        $(".nav_wrap_float").fadeIn(200);
    }else{
        $(".nav_wrap_float").fadeOut(100);
    }
}

$(function(){
    $('.flexslider').flexslider({
        directionNav: false,
        keyboardNav: true,
        pauseOnAction: false
    });
});
window.onload = function()
{
    //alert("加载本页耗时 "+ (new Date().getTime()-t1) +" 毫秒");
    //document.getElementById("TimeShow").innerHTML = "加载本页耗时 "+ (new Date().getTime()-t1) +" 毫秒";
    $('.TimeShow').val("加载本页耗时 "+ (new Date().getTime()-t1) +" 毫秒");

    //document.getElementById("TimeShow").innerHTML = "加载本页耗时 "+ (new Date().getTime()-t1) +" 毫秒";
}
</script>
</html>