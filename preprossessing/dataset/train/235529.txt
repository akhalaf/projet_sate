<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US"
	prefix="og: https://ogp.me/ns#" >
<![endif]--><!--[if IE 7]>
<html id="ie7" lang="en-US"
	prefix="og: https://ogp.me/ns#" >
<![endif]--><!--[if IE 8]>
<html id="ie8" lang="en-US"
	prefix="og: https://ogp.me/ns#" >
<![endif]--><!--[if !(IE 6) & !(IE 7) & !(IE 8)]><!--><html lang="en-US" prefix="og: https://ogp.me/ns#">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<title>Rufus Thomas: His R&amp;B Recordings, 1949-1956 | BLACK GROOVES</title>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<link href="https://blackgrooves.org/wp-content/themes/twentyeleven/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blackgrooves.org/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
<script src="https://blackgrooves.org/wp-content/themes/twentyeleven/js/html5.js" type="text/javascript"></script>
<![endif]-->
<!-- All in One SEO Pack 3.3.4 by Michael Torbert of Semper Fi Web Designob_start_detected [-1,-1] -->
<meta content="Title: Rufus Thomas: His R&amp;B Recordings, 1949-1956 Artist: Rufus Thomas Label: Bear Family Catalog No.: BCD 16695AH Date: 2008 Rufus Thomas is best known as the" name="description"/>
<meta content="black radio,blues,memphis,rhythm and blues,rufus thomas,sam phillips,soul,stax,sun records,wdia" name="keywords"/>
<script class="aioseop-schema" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://blackgrooves.org/#organization","url":"https://blackgrooves.org/","name":"BLACK GROOVES","sameAs":[]},{"@type":"WebSite","@id":"https://blackgrooves.org/#website","url":"https://blackgrooves.org/","name":"BLACK GROOVES","publisher":{"@id":"https://blackgrooves.org/#organization"},"potentialAction":{"@type":"SearchAction","target":"https://blackgrooves.org/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://blackgrooves.org/rufus-thomas-his-rb-recordings-1949-1956/#webpage","url":"https://blackgrooves.org/rufus-thomas-his-rb-recordings-1949-1956/","inLanguage":"en-US","name":"Rufus Thomas: His R&#038;B Recordings, 1949-1956","isPartOf":{"@id":"https://blackgrooves.org/#website"},"datePublished":"2008-09-05T19:06:26-04:00","dateModified":"2008-09-05T19:06:26-04:00"},{"@type":"Article","@id":"https://blackgrooves.org/rufus-thomas-his-rb-recordings-1949-1956/#article","isPartOf":{"@id":"https://blackgrooves.org/rufus-thomas-his-rb-recordings-1949-1956/#webpage"},"author":{"@id":"https://blackgrooves.org/author/aaamc/#author"},"headline":"Rufus Thomas: His R&#038;B Recordings, 1949-1956","datePublished":"2008-09-05T19:06:26-04:00","dateModified":"2008-09-05T19:06:26-04:00","commentCount":0,"mainEntityOfPage":{"@id":"https://blackgrooves.org/rufus-thomas-his-rb-recordings-1949-1956/#webpage"},"publisher":{"@id":"https://blackgrooves.org/#organization"},"articleSection":"Blues, Popular, Rock, and Misc., Rhythm &#038; Blues, Soul, Funk, Black radio, Blues, Memphis, rhythm and blues, Rufus Thomas, Sam Phillips, Soul, Stax, Sun Records, WDIA"},{"@type":"Person","@id":"https://blackgrooves.org/author/aaamc/#author","name":"aaamc","sameAs":[],"image":{"@type":"ImageObject","@id":"https://blackgrooves.org/#personlogo","url":"https://secure.gravatar.com/avatar/109b66fe348c3c01deab33d0de628655?s=96&d=mm&r=g","width":96,"height":96,"caption":"aaamc"}}]}</script>
<link href="https://blackgrooves.org/rufus-thomas-his-rb-recordings-1949-1956/" rel="canonical"/>
<meta content="article" property="og:type"/>
<meta content="Rufus Thomas: His R&amp;B Recordings, 1949-1956 | BLACK GROOVES" property="og:title"/>
<meta content="Title: Rufus Thomas: His R&amp;B Recordings, 1949-1956 Artist: Rufus Thomas Label: Bear Family Catalog No.: BCD 16695AH Date: 2008 Rufus Thomas is best known as the Memphis soul singer who, along with" property="og:description"/>
<meta content="https://blackgrooves.org/rufus-thomas-his-rb-recordings-1949-1956/" property="og:url"/>
<meta content="BLACK GROOVES" property="og:site_name"/>
<meta content="https://blackgrooves.org/wp-content/uploads/2020/01/aaamc-facebook-profile.jpg" property="og:image"/>
<meta content="2008-09-05T19:06:26Z" property="article:published_time"/>
<meta content="2008-09-05T19:06:26Z" property="article:modified_time"/>
<meta content="https://blackgrooves.org/wp-content/uploads/2020/01/aaamc-facebook-profile.jpg" property="og:image:secure_url"/>
<meta content="summary" name="twitter:card"/>
<meta content="@IU_AAAMC" name="twitter:site"/>
<meta content="Rufus Thomas: His R&amp;B Recordings, 1949-1956 | BLACK GROOVES" name="twitter:title"/>
<meta content="Title: Rufus Thomas: His R&amp;B Recordings, 1949-1956 Artist: Rufus Thomas Label: Bear Family Catalog No.: BCD 16695AH Date: 2008 Rufus Thomas is best known as the Memphis soul singer who, along with" name="twitter:description"/>
<meta content="https://blackgrooves.org/wp-content/uploads/2020/01/aaamc-facebook-profile.jpg" name="twitter:image"/>
<!-- All in One SEO Pack -->
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://blackgrooves.org/feed/" rel="alternate" title="BLACK GROOVES » Feed" type="application/rss+xml"/>
<link href="https://blackgrooves.org/comments/feed/" rel="alternate" title="BLACK GROOVES » Comments Feed" type="application/rss+xml"/>
<link href="https://blackgrooves.org/rufus-thomas-his-rb-recordings-1949-1956/feed/" rel="alternate" title="BLACK GROOVES » Rufus Thomas: His R&amp;B Recordings, 1949-1956 Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/blackgrooves.org\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://blackgrooves.org/wp-content/plugins/mailchimp//css/flick/flick.css?ver=5.3.6" id="flick-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blackgrooves.org/?mcsf_action=main_css&amp;ver=5.3.6" id="mailchimpSF_main_css-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if IE]>
<link rel='stylesheet' id='mailchimpSF_ie_css-css'  href='https://blackgrooves.org/wp-content/plugins/mailchimp/css/ie.css?ver=5.3.6' type='text/css' media='all' />
<![endif]-->
<link href="https://blackgrooves.org/wp-includes/css/dist/block-library/style.min.css?ver=5.3.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blackgrooves.org/wp-includes/css/dist/block-library/theme.min.css?ver=5.3.6" id="wp-block-library-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blackgrooves.org/wp-content/themes/twentyeleven/blocks.css?ver=20181230" id="twentyeleven-block-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blackgrooves.org/wp-content/plugins/add-to-any/addtoany.min.css?ver=1.15" id="addtoany-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://blackgrooves.org/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://blackgrooves.org/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://blackgrooves.org/wp-content/plugins/mailchimp//js/scrollTo.js?ver=1.5.7" type="text/javascript"></script>
<script src="https://blackgrooves.org/wp-includes/js/jquery/jquery.form.min.js?ver=4.2.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var mailchimpSF = {"ajax_url":"https:\/\/blackgrooves.org\/"};
/* ]]> */
</script>
<script src="https://blackgrooves.org/wp-content/plugins/mailchimp//js/mailchimp.js?ver=1.5.7" type="text/javascript"></script>
<script src="https://blackgrooves.org/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://blackgrooves.org/wp-content/plugins/mailchimp//js/datepicker.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://blackgrooves.org/wp-content/plugins/add-to-any/addtoany.min.js?ver=1.1" type="text/javascript"></script>
<link href="https://blackgrooves.org/wp-json/" rel="https://api.w.org/"/>
<link href="https://blackgrooves.org/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://blackgrooves.org/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<link href="https://blackgrooves.org/the-story-of-my-life/" rel="prev" title="The Story of My Life"/>
<link href="https://blackgrooves.org/singin-sepia/" rel="next" title="Singin’ Sepia"/>
<meta content="WordPress 5.3.6" name="generator"/>
<link href="https://blackgrooves.org/?p=770" rel="shortlink"/>
<link href="https://blackgrooves.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fblackgrooves.org%2Frufus-thomas-his-rb-recordings-1949-1956%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://blackgrooves.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fblackgrooves.org%2Frufus-thomas-his-rb-recordings-1949-1956%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<script data-cfasync="false">
window.a2a_config=window.a2a_config||{};a2a_config.callbacks=[];a2a_config.overlays=[];a2a_config.templates={};
(function(d,s,a,b){a=d.createElement(s);b=d.getElementsByTagName(s)[0];a.async=1;a.src="https://static.addtoany.com/menu/page.js";b.parentNode.insertBefore(a,b);})(document,"script");
</script>
<script type="text/javascript">
        jQuery(function($) {
            $('.date-pick').each(function() {
                var format = $(this).data('format') || 'mm/dd/yyyy';
                format = format.replace(/yyyy/i, 'yy');
                $(this).datepicker({
                    autoFocusNextInput: true,
                    constrainInput: false,
                    changeMonth: true,
                    changeYear: true,
                    beforeShow: function(input, inst) { $('#ui-datepicker-div').addClass('show'); },
                    dateFormat: format.toLowerCase(),
                });
            });
            d = new Date();
            $('.birthdate-pick').each(function() {
                var format = $(this).data('format') || 'mm/dd';
                format = format.replace(/yyyy/i, 'yy');
                $(this).datepicker({
                    autoFocusNextInput: true,
                    constrainInput: false,
                    changeMonth: true,
                    changeYear: false,
                    minDate: new Date(d.getFullYear(), 1-1, 1),
                    maxDate: new Date(d.getFullYear(), 12-1, 31),
                    beforeShow: function(input, inst) { $('#ui-datepicker-div').removeClass('show'); },
                    dateFormat: format.toLowerCase(),
                });

            });

        });
    </script>
<!-- This site uses the Open External Links in a New Window plugin v1.4 by WebFactory Ltd. Download it for free at https://wordpress.org/extend/plugins/open-external-links-in-a-new-window/ -->
<script type="text/javascript">//<![CDATA[
  function external_links_in_new_windows_loop() {
    if (!document.links) {
      document.links = document.getElementsByTagName('a');
    }
    var change_link = false;
    var force = '';
    var ignore = '';

    for (var t=0; t<document.links.length; t++) {
      var all_links = document.links[t];
      change_link = false;
      
      if(document.links[t].hasAttribute('onClick') == false) {
        // forced if the address starts with http (or also https), but does not link to the current domain
        if(all_links.href.search(/^http/) != -1 && all_links.href.search('blackgrooves.org') == -1 && all_links.href.search(/^#/) == -1) {
          // console.log('Changed ' + all_links.href);
          change_link = true;
        }
          
        if(force != '' && all_links.href.search(force) != -1) {
          // forced
          // console.log('force ' + all_links.href);
          change_link = true;
        }
        
        if(ignore != '' && all_links.href.search(ignore) != -1) {
          // console.log('ignore ' + all_links.href);
          // ignored
          change_link = false;
        }

        if(change_link == true) {
          // console.log('Changed ' + all_links.href);
          document.links[t].setAttribute('onClick', 'javascript:window.open(\''+all_links.href+'\'); return false;');
          document.links[t].removeAttribute('target');
        }
      }
    }
  }
  
  // Load
  function external_links_in_new_windows_load(func)
  {  
    var oldonload = window.onload;
    if (typeof window.onload != 'function'){
      window.onload = func;
    } else {
      window.onload = function(){
        oldonload();
        func();
      }
    }
  }

  external_links_in_new_windows_load(external_links_in_new_windows_loop);
  //]]></script>
<style data-context="foundation-flickity-css">/*! Flickity v2.0.2
http://flickity.metafizzy.co
---------------------------------------------- */.flickity-enabled{position:relative}.flickity-enabled:focus{outline:0}.flickity-viewport{overflow:hidden;position:relative;height:100%}.flickity-slider{position:absolute;width:100%;height:100%}.flickity-enabled.is-draggable{-webkit-tap-highlight-color:transparent;tap-highlight-color:transparent;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.flickity-enabled.is-draggable .flickity-viewport{cursor:move;cursor:-webkit-grab;cursor:grab}.flickity-enabled.is-draggable .flickity-viewport.is-pointer-down{cursor:-webkit-grabbing;cursor:grabbing}.flickity-prev-next-button{position:absolute;top:50%;width:44px;height:44px;border:none;border-radius:50%;background:#fff;background:hsla(0,0%,100%,.75);cursor:pointer;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.flickity-prev-next-button:hover{background:#fff}.flickity-prev-next-button:focus{outline:0;box-shadow:0 0 0 5px #09f}.flickity-prev-next-button:active{opacity:.6}.flickity-prev-next-button.previous{left:10px}.flickity-prev-next-button.next{right:10px}.flickity-rtl .flickity-prev-next-button.previous{left:auto;right:10px}.flickity-rtl .flickity-prev-next-button.next{right:auto;left:10px}.flickity-prev-next-button:disabled{opacity:.3;cursor:auto}.flickity-prev-next-button svg{position:absolute;left:20%;top:20%;width:60%;height:60%}.flickity-prev-next-button .arrow{fill:#333}.flickity-page-dots{position:absolute;width:100%;bottom:-25px;padding:0;margin:0;list-style:none;text-align:center;line-height:1}.flickity-rtl .flickity-page-dots{direction:rtl}.flickity-page-dots .dot{display:inline-block;width:10px;height:10px;margin:0 8px;background:#333;border-radius:50%;opacity:.25;cursor:pointer}.flickity-page-dots .dot.is-selected{opacity:1}</style><style data-context="foundation-slideout-css">.slideout-menu{position:fixed;left:0;top:0;bottom:0;right:auto;z-index:0;width:256px;overflow-y:auto;-webkit-overflow-scrolling:touch;display:none}.slideout-menu.pushit-right{left:auto;right:0}.slideout-panel{position:relative;z-index:1;will-change:transform}.slideout-open,.slideout-open .slideout-panel,.slideout-open body{overflow:hidden}.slideout-open .slideout-menu{display:block}.pushit{display:none}</style><!-- Vipers Video Quicktags v6.6.0 | http://www.viper007bond.com/wordpress-plugins/vipers-video-quicktags/ -->
<style type="text/css">
.vvqbox { display: block; max-width: 100%; visibility: visible !important; margin: 10px auto; } .vvqbox img { max-width: 100%; height: 100%; } .vvqbox object { max-width: 100%; } 
</style>
<script type="text/javascript">
// <![CDATA[
	var vvqflashvars = {};
	var vvqparams = { wmode: "opaque", allowfullscreen: "true", allowscriptaccess: "always" };
	var vvqattributes = {};
	var vvqexpressinstall = "https://blackgrooves.org/wp-content/plugins/vipers-video-quicktags/resources/expressinstall.swf";
// ]]>
</script>
<link href="https://blackgrooves.org/wp-content/uploads/2018/09/cropped-aaamc_square_logo-32x32.jpg" rel="icon" sizes="32x32"/>
<link href="https://blackgrooves.org/wp-content/uploads/2018/09/cropped-aaamc_square_logo-192x192.jpg" rel="icon" sizes="192x192"/>
<link href="https://blackgrooves.org/wp-content/uploads/2018/09/cropped-aaamc_square_logo-180x180.jpg" rel="apple-touch-icon-precomposed"/>
<meta content="https://blackgrooves.org/wp-content/uploads/2018/09/cropped-aaamc_square_logo-270x270.jpg" name="msapplication-TileImage"/>
<style>.ios7.web-app-mode.has-fixed header{ background-color: rgba(127,59,59,.88);}</style></head>
<body class="post-template-default single single-post postid-770 single-format-standard wp-embed-responsive single-author singular two-column right-sidebar" data-rsssl="1">
<div class="hfeed" id="page">
<header id="branding" role="banner">
<hgroup>
<!-- <h1 id="site-title"><span><a href="https://blackgrooves.org/" rel="home">BLACK GROOVES</a></span></h1> -->
<h1 id="site-title"><span><a href="https://blackgrooves.org/" rel="home"><img src="https://blackgrooves.org/wp-content/uploads/2020/01/Black-Grooves-Title-Image.png"/></a></span></h1>
<h2 id="site-description"></h2>
</hgroup>
<!-- edited header image code begins here (see shared drive for original code) -->
<a href="https://blackgrooves.org/">
<img alt="BLACK GROOVES" height="" src="https://blackgrooves.org/wp-content/uploads/2018/09/Black-Grooves-Header-Image.jpg" width=""/>
</a>
<!-- edited header image code ends here -->
<form action="https://blackgrooves.org/" id="searchform" method="get">
<label class="assistive-text" for="s">Search</label>
<input class="field" id="s" name="s" placeholder="Search" type="text"/>
<input class="submit" id="searchsubmit" name="submit" type="submit" value="Search"/>
</form>
<nav id="access" role="navigation">
<h3 class="assistive-text">Main menu</h3>
<div class="skip-link"><a class="assistive-text" href="#content">Skip to primary content</a></div>
<div class="menu-page-navigation-container"><ul class="menu" id="menu-page-navigation"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-19278" id="menu-item-19278"><a href="https://blackgrooves.org/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19275" id="menu-item-19275"><a href="https://blackgrooves.org/about/">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19276" id="menu-item-19276"><a href="https://blackgrooves.org/subscribe/">Subscribe</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19277" id="menu-item-19277"><a href="https://blackgrooves.org/contact/">Contact</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19274" id="menu-item-19274"><a href="http://www.indiana.edu/~aaamc/">AAAMC</a></li>
</ul></div> </nav><!-- #access -->
</header><!-- #branding -->
<div id="main">
<div id="primary">
<div id="content" role="main">
<nav id="nav-single">
<h3 class="assistive-text">Post navigation</h3>
<span class="nav-previous"><a href="https://blackgrooves.org/the-story-of-my-life/" rel="prev"><span class="meta-nav">←</span> Previous</a></span>
<span class="nav-next"><a href="https://blackgrooves.org/singin-sepia/" rel="next">Next <span class="meta-nav">→</span></a></span>
</nav><!-- #nav-single -->
<article class="post-770 post type-post status-publish format-standard hentry category-blues category-popular-music category-rhythm-blues-soul-funk tag-black-radio tag-blues tag-memphis tag-rhythm-and-blues tag-rufus-thomas tag-sam-phillips tag-soul tag-stax tag-sun-records tag-wdia" id="post-770">
<header class="entry-header">
<h1 class="entry-title">Rufus Thomas: His R&amp;B Recordings, 1949-1956</h1>
<div class="entry-meta">
<span class="sep">Posted on </span><a href="https://blackgrooves.org/rufus-thomas-his-rb-recordings-1949-1956/" rel="bookmark" title="7:06 pm"><time class="entry-date" datetime="2008-09-05T19:06:26-04:00">September 5, 2008</time></a><span class="by-author"> <span class="sep"> by </span> <span class="author vcard"><a class="url fn n" href="https://blackgrooves.org/author/aaamc/" rel="author" title="View all posts by aaamc">aaamc</a></span></span> </div><!-- .entry-meta -->
</header><!-- .entry-header -->
<div class="entry-content">
<p><a href="https://blackgrooves.org/wp-content/uploads/2008/09/rufus.jpg"><img alt="" class="alignleft alignnone size-thumbnail wp-image-771" height="150" sizes="(max-width: 150px) 100vw, 150px" src="https://blackgrooves.org/wp-content/uploads/2008/09/rufus-150x150.jpg" srcset="https://blackgrooves.org/wp-content/uploads/2008/09/rufus-150x150.jpg 150w, https://blackgrooves.org/wp-content/uploads/2008/09/rufus.jpg 240w" style="float: left;" title="rufus" width="150"/></a>Title: <a href="http://www.amazon.com/Sun-Years-Plus-Rufus-Thomas/dp/B001C0NN6K/ref=sr_1_19?ie=UTF8&amp;s=music&amp;qid=1220623673&amp;sr=1-19">Rufus Thomas: His R&amp;B Recordings, 1949-1956</a><br/>
Artist: Rufus Thomas<br/>
Label: <a href="http://www.bear-family.de/cd-dvd-series/sun-records/?lang=1">Bear Family</a><br/>
Catalog No.: BCD 16695AH<br/>
Date: 2008</p>
<p><a href="http://en.wikipedia.org/wiki/Rufus_Thomas"></a></p>
<p><a href="http://en.wikipedia.org/wiki/Rufus_Thomas">Rufus Thomas</a> is best known as the Memphis soul singer who, along with daughter Carla Thomas, helped the fledgling Stax label rise to fame in the ‘60s and ‘70s. His biggest hits-“<a href="http://www.youtube.com/watch?v=6e9uk0fMNow&amp;feature=related">Do the Funky Chicken</a>” and “<a href="http://www.youtube.com/watch?v=M6AZNywvF-s">Walking the Dog</a>“–not only became his signature songs, but established Thomas as a consummate entertainer. Not surprisingly, he first honed these skills as a vaudeville performer and emcee for shows down on Beale   Street. Thomas also had a long career at WDIA in Memphis, the nation’s first all-Black format radio station, where he spun rhythm and blues records that caught the attention of many a white teenager, including a young Elvis Presley. Since his fellow WDIA deejay was none other than B.B. King, it should come as no surprise that Thomas decided to take a stab at recording.  “I just wanted to be on record. I never thought of getting rich. I just wanted to be known, be a recording artist.”</p>
<p>From 1949 to 1956 Thomas recorded 28 sides for various labels, though a number were unissued and have since been lost (all extant recordings have been included in this compilation). His first sessions in Memphis were for the Star Talent label (based in Dallas) and featured several of his own songs, including the bluesy “I’m So Worried,” the somewhat derivative “I’ll Be Good Boy,” and the previously unreleased “Who’s That Chick” and “Double Trouble” (the latter in rather poor sound). These were followed by two sides for Bullet-the rockin’ party song “Beer Bottle Boogie” and another of Thomas’ own compositions, “Gonna Bring My Baby Back,” a swinging jazz number backed by members of Lionel Hampton’s band let by saxophonist Bobby Platter.</p>
<p>The following year Thomas stopped in at Memphis Recording Service–soon to be renamed Sun Studios–and convinced Sam Phillips to record several songs which were released on the Chess label, including “Night Workin’ Blues,” his own cryin’ blues tune “Why Did You Deegee,” the uptempo boogie woogie “Crazy About You Baby” featuring Billy Love on piano, and “No More Dogging Around.” Additional sessions followed in 1952 producing the notable song “Decorate the Counter”–this had originally been  recorded by Rosco Gordon, but only Thomas’ version was released by Chess (both versions are included on the CD for comparison). Two additional songs were recorded at the same session but were never released: “Married Woman” included here with two alternate takes; and “I’m Off Of That Stuff” which is a bit stiff, not to mention somewhat truncated.</p>
<p>Thomas’ big break came in 1953 when he recorded “Bear Cat” for the new Sun label. An answer song to Big Mama Thornton’s bluesy “Hound Dog” that had topped the charts a few weeks earlier (also included on the CD), “Bear Cat” was a huge hit, signaling the shift towards rock ‘n’ roll and no doubt making an impression on Elvis Presley, whose cover of “Hound Dog” catapulted both him and Sun Records to fame three years later. Thomas cut several more sides for Sun, including “Tiger Man (King of the Jungle)” complete with Tarzan yells, and the straight-ahead blues song “Save That Money.” His early recording career concluded at Meteor, a short-lived Memphis label, which released “The Easy Living Plan” and “I’m Steady Holdin’ On,” both penned by Rufus Thomas and Joe Bihar.</p>
<p><em>Rufus Thomas: His R&amp;B Recordings, 1949-1956</em> is a great tribute to this legendary artist who passed away in 2001.  Interestingly, two other compilations including much of the same material have also been released in 2008 by <a href="http://www.amazon.com/Tiger-Man-Earliest-Recordings-1950-1957/dp/B0017LGP14/ref=sr_1_18?ie=UTF8&amp;s=music&amp;qid=1220623548&amp;sr=1-18">Document Records</a> and <a href="http://www.amazon.com/Before-Stax-Complete-50s-Recordings/dp/B0013KS71K/ref=sr_1_11?ie=UTF8&amp;s=music&amp;qid=1220623427&amp;sr=1-11">Important Artists</a>.  However, the Bear Family set is far superior in terms of remastering and production.  The wonderfully illustrated 67 page booklet (bound into the package) features a complete 1950s discography and an overview of Thomas’ pre-Stax career by Martin Hawkins, including lengthy discussions about the role of WDIA and Black radio.</p>
<p>The other thing that really sets this CD apart are the bonus features and alternate takes previously mentioned, as well as airchecks from Thomas’s WDAI radio show and a ten minute interview from the Daddy Cool Show.  With a total of 29 tracks, this is indeed the definitive compilation of Thomas’s early recordings. Anyone interested in Memphis soul, the roots of rock ‘n’ roll, and the story of Black radio will want to purchase this set–it would also be perfect for classroom use. <em>Rufus Thomas: His R&amp;B Recordings, 1949-1956</em> is absolutely the best single CD historical reissue that I’ve come across in 2008.</p>
<p>Posted by Brenda Nelson-Strauss</p>
<div class="addtoany_share_save_container addtoany_content addtoany_content_bottom"><div class="a2a_kit a2a_kit_size_22 addtoany_list" data-a2a-title="Rufus Thomas: His R&amp;B Recordings, 1949-1956" data-a2a-url="https://blackgrooves.org/rufus-thomas-his-rb-recordings-1949-1956/"><a class="a2a_button_facebook_like addtoany_special_service" data-href="https://blackgrooves.org/rufus-thomas-his-rb-recordings-1949-1956/"></a><a class="a2a_button_twitter_tweet addtoany_special_service" data-text="Rufus Thomas: His R&amp;B Recordings, 1949-1956" data-url="https://blackgrooves.org/rufus-thomas-his-rb-recordings-1949-1956/"></a><a class="a2a_button_pinterest a2a_counter" href="https://www.addtoany.com/add_to/pinterest?linkurl=https%3A%2F%2Fblackgrooves.org%2Frufus-thomas-his-rb-recordings-1949-1956%2F&amp;linkname=Rufus%20Thomas%3A%20His%20R%26B%20Recordings%2C%201949-1956" rel="nofollow noopener" target="_blank" title="Pinterest"></a><a class="a2a_button_pocket" href="https://www.addtoany.com/add_to/pocket?linkurl=https%3A%2F%2Fblackgrooves.org%2Frufus-thomas-his-rb-recordings-1949-1956%2F&amp;linkname=Rufus%20Thomas%3A%20His%20R%26B%20Recordings%2C%201949-1956" rel="nofollow noopener" target="_blank" title="Pocket"></a><a class="a2a_button_email" href="https://www.addtoany.com/add_to/email?linkurl=https%3A%2F%2Fblackgrooves.org%2Frufus-thomas-his-rb-recordings-1949-1956%2F&amp;linkname=Rufus%20Thomas%3A%20His%20R%26B%20Recordings%2C%201949-1956" rel="nofollow noopener" target="_blank" title="Email"></a><a class="a2a_dd addtoany_share_save addtoany_share" href="https://www.addtoany.com/share"></a></div></div> </div><!-- .entry-content -->
<footer class="entry-meta">
		This entry was posted in <a href="https://blackgrooves.org/category/blues/" rel="category tag">Blues</a>, <a href="https://blackgrooves.org/category/popular-music/" rel="category tag">Popular, Rock, and Misc.</a>, <a href="https://blackgrooves.org/category/rhythm-blues-soul-funk/" rel="category tag">Rhythm &amp; Blues, Soul, Funk</a> and tagged <a href="https://blackgrooves.org/tag/black-radio/" rel="tag">Black radio</a>, <a href="https://blackgrooves.org/tag/blues/" rel="tag">Blues</a>, <a href="https://blackgrooves.org/tag/memphis/" rel="tag">Memphis</a>, <a href="https://blackgrooves.org/tag/rhythm-and-blues/" rel="tag">rhythm and blues</a>, <a href="https://blackgrooves.org/tag/rufus-thomas/" rel="tag">Rufus Thomas</a>, <a href="https://blackgrooves.org/tag/sam-phillips/" rel="tag">Sam Phillips</a>, <a href="https://blackgrooves.org/tag/soul/" rel="tag">Soul</a>, <a href="https://blackgrooves.org/tag/stax/" rel="tag">Stax</a>, <a href="https://blackgrooves.org/tag/sun-records/" rel="tag">Sun Records</a>, <a href="https://blackgrooves.org/tag/wdia/" rel="tag">WDIA</a> by <a href="https://blackgrooves.org/author/aaamc/">aaamc</a>. Bookmark the <a href="https://blackgrooves.org/rufus-thomas-his-rb-recordings-1949-1956/" rel="bookmark" title="Permalink to Rufus Thomas: His R&amp;B Recordings, 1949-1956">permalink</a>.		
			</footer><!-- .entry-meta -->
</article><!-- #post-770 -->
<div id="comments">
</div><!-- #comments -->
</div><!-- #content -->
</div><!-- #primary -->
</div><!-- #main -->
<footer id="colophon" role="contentinfo">
<div class="three" id="supplementary">
<div class="widget-area" id="first" role="complementary">
<aside class="widget_text widget widget_custom_html" id="custom_html-4"><div class="textwidget custom-html-widget"> <p align="center">All content ©2020 by the Archives of African American Music and Culture</p></div></aside> </div><!-- #first .widget-area -->
<div class="widget-area" id="second" role="complementary">
<aside class="widget widget_a2a_follow_widget" id="a2a_follow_widget-2"><h3 class="widget-title">Follow us:</h3><div class="a2a_kit a2a_kit_size_22 a2a_follow addtoany_list"><a class="a2a_button_facebook" href="https://www.facebook.com/Archives-of-African-American-Music-and-Culture-59396481882/" rel="noopener" target="_blank" title="Facebook"></a><a class="a2a_button_twitter" href="https://twitter.com/IU_AAAMC" rel="noopener" target="_blank" title="Twitter"></a><a class="a2a_button_instagram" href="https://www.instagram.com/iu_aaamc/" rel="noopener" target="_blank" title="Instagram"></a><a class="a2a_button_tumblr" href="http://aaamc.tumblr.com/" rel="noopener" target="_blank" title="Tumblr"></a><a class="a2a_button_youtube" href="https://www.youtube.com/user/iuaaamc" rel="noopener" target="_blank" title="YouTube"></a></div></aside> </div><!-- #second .widget-area -->
<div class="widget-area" id="third" role="complementary">
<aside class="widget_text widget widget_custom_html" id="custom_html-5"><div class="textwidget custom-html-widget"><p align="right">
<a href="https://blackgrooves.org/wp-admin/">Login</a>
</p></div></aside> </div><!-- #third .widget-area -->
</div><!-- #supplementary -->
<div id="site-generator">
<a class="imprint" href="https://wordpress.org/" title="Semantic Personal Publishing Platform">
					Proudly powered by WordPress				</a>
</div>
</footer><!-- #colophon -->
</div><!-- #page -->
<p class="footer"></p><!-- Powered by WPtouch: 4.3.37 --><script src="https://blackgrooves.org/wp-content/plugins/page-links-to/dist/new-tab.js?ver=3.2.2" type="text/javascript"></script>
<script src="https://blackgrooves.org/wp-includes/js/wp-embed.min.js?ver=5.3.6" type="text/javascript"></script>
</body>
</html>
<!--
Performance optimized by W3 Total Cache. Learn more: https://www.w3-edge.com/products/


Served from: blackgrooves.org @ 2021-01-13 13:52:10 by W3 Total Cache
-->