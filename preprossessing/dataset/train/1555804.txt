<!DOCTYPE html>
<html dir="ltr" lang="en-gb" xml:lang="en-gb" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<base href="https://www.vtfootballcamps.com/camps-clinics/specialty-clinics"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="Vermont All-Star Football Camps, Vermont All-Star Football, Vermont All-Star, All-Star Football Camps, All-Star Football, Football Camps, Vermont All-Star Football Camps, All-Star Football Camp, Football Camp, Stowe Vermont, Stowe VT, Stowe V.T., Stowe Football Camps, Stowe Football Camp, Stowe Football" name="keywords"/>
<meta content="Larissa Kepchar" name="author"/>
<meta content="Vermont All-Star Football Camps - The Premiere Football Camp for Vermont, Massachusetts, New Hampshire and all of New England" name="description"/>
<title>Specialty Clinics - Vermont Football Camps</title>
<link href="/templates/football/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/templates/football/css/template.css" rel="stylesheet" type="text/css"/>
<link href="/media/mod_responsivemenu/css/theme2.css.php?maxMobileWidth=900&amp;menuBG=%23ffffff&amp;textColor=%23339933&amp;textColor2=%23339933" rel="stylesheet" type="text/css"/>
<script src="/media/jui/js/jquery.min.js?776de6f99ee44ba86fb5bfd31e33595b" type="text/javascript"></script>
<script src="/media/jui/js/jquery-noconflict.js?776de6f99ee44ba86fb5bfd31e33595b" type="text/javascript"></script>
<script src="/media/jui/js/jquery-migrate.min.js?776de6f99ee44ba86fb5bfd31e33595b" type="text/javascript"></script>
<script src="/media/system/js/caption.js?776de6f99ee44ba86fb5bfd31e33595b" type="text/javascript"></script>
<script src="/media/jui/js/bootstrap.min.js?776de6f99ee44ba86fb5bfd31e33595b" type="text/javascript"></script>
<script src="/templates/football/js/template.js" type="text/javascript"></script>
<script src="/plugins/system/aikon_smooth_scroll/assets/js/SmoothScroll.js" type="text/javascript"></script>
<script defer="defer" src="/modules/mod_responsivemenu/js/jquery.easing.1.3.js" type="text/javascript"></script>
<script defer="defer" src="/modules/mod_responsivemenu/js/theme2.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(window).on('load',  function() {
				new JCaption('img.caption');
			});
	</script>
<link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
<style type="text/css">
			h1,h2,h3,h4,h5,h6,.site-title{
				font-family: 'Open Sans', sans-serif;
			}
		</style>
<style type="text/css">
		body.site
		{
			border-top: 3px solid #ffffff;
			background-color: #ffffff		}
		a
		{
			color: #ffffff;
		}
		.navbar-inner, .nav-list > .active > a, .nav-list > .active > a:hover, .dropdown-menu li > a:hover, .dropdown-menu .active > a, .dropdown-menu .active > a:hover, .nav-pills > .active > a, .nav-pills > .active > a:hover,
		.btn-primary
		{
			background: #ffffff;
		}
		.navbar-inner
		{
			-moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
			-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
			box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
		}
	</style>
<!--[if lt IE 9]>
		<script src="/media/jui/js/html5.js"></script>
	<![endif]-->
<link href="https://fonts.googleapis.com/css?family=Graduate" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css"/>
</head>
<body class="site com_content view-article no-layout no-task itemid-172 fluid">
<!-- Body -->
<div class="body">
<div class="container-fluid">
<!-- Header -->
<div id="top-wrapper">
<div id="top-inner">
<div id="top">
<div class="moduletable">
<div class="custom">
<p><img alt="" src="/images/modules/car.png" style="margin-right: 2px;"/> <a href="/contact/directions-to-camps">Get Directions!</a>  |  <img alt="" src="/images/modules/phone.png" style="margin-right: 2px;"/> <a href="tel:+18022299653">802-229-9653</a> <a href="https://www.facebook.com/Vermont-Football-Camps-183871248324974/" target="_blank"><img alt="" src="/images/modules/fb.png" style="margin-left: 10px;"/></a></p></div>
</div>
</div><!--end-top-->
</div>
<div style="clear:both"></div>
</div><!--end-top-wrapper-->
<header class="header">
<div class="header-inner clearfix">
<a class="brand pull-left" href="/">
<img alt="Vermont Football Camps" src="https://www.vtfootballcamps.com/images/logo.png"/> </a>
<div id="my-nav">
<a class="toggleMenu " href="#"><span>Menu</span></a>
<ul class="responsiveMenuTheme2 " data-maxmobilewidth="900" id="responsiveMenu87">
<li class="first deeper parent" id="item-133"><a href="/about"><span class="linker">About</span><span class="opener"> </span></a><ul><li id="item-143"><a href="/about/coaches"><span class="linker">Coaches</span></a></li><li id="item-145"><a href="/about/videos"><span class="linker">Video Gallery</span></a></li></ul></li><li id="item-138"><a class="separator "><span class="linker">|</span></a>
</li><li class="active deeper parent" id="item-134"><a href="/camps-clinics"><span class="linker">Camps &amp; Clinics</span><span class="opener"> </span></a><ul><li id="item-144"><a href="/camps-clinics/tuition-fees"><span class="linker">Tuition &amp; Fees</span></a></li><li id="item-151"><a href="/camps-clinics/schedule-equipment"><span class="linker">Schedule &amp; Equipment </span></a></li><li id="item-152"><a href="/camps-clinics/dates-of-events"><span class="linker">Dates of Events</span></a></li><li id="item-170"><a href="/camps-clinics/youth-players"><span class="linker">Youth Players</span></a></li><li id="item-171"><a href="/camps-clinics/teen-players"><span class="linker">Teen Players</span></a></li><li class="current active" id="item-172"><a href="/camps-clinics/specialty-clinics"><span class="linker">Specialty Clinics</span></a></li></ul></li><li id="item-140"><a class="separator "><span class="linker">|</span></a>
</li><li class="deeper parent" id="item-136"><a href="/contact"><span class="linker">Contact</span><span class="opener"> </span></a><ul><li id="item-153"><a href="/contact/directions-to-camps"><span class="linker">Directions to Camps</span></a></li></ul></li></ul>
</div>
<nav class="navigation">
</nav>
</div>
</header>
<div id="slideshow">
<div class="moduletable">
<div class="custom">
<p><img alt="header-1" height="300" src="/images/headers/header-1.jpg" width="1920"/></p></div>
</div>
</div><!--end-slideshow-->
<div style="clear:both"></div>
<div style="clear:both"></div>
</div><!--end-specials-wrapper-->
<div id="content-wrapper">
<div class="row-fluid">
<main class="span12" id="content" role="main">
<!-- Begin Content -->
<div id="system-message-container">
</div>
<div class="item-page" itemscope="" itemtype="https://schema.org/Article">
<meta content="en-GB" itemprop="inLanguage"/>
<div class="page-header">
<h2 itemprop="headline">
			Specialty Clinics		</h2>
</div>
<div itemprop="articleBody">
<p>Our Specialty clinics are designed to give each student-athlete an opportunity to receive concentrated work in the areas of skill and coordinated movement at their designated position. These clinics will help each athlete in developing his ability to perform at his highest potential. Our goal is to teach, and teach and teach during this clinic!</p>
<p> </p>
<p><img alt="Specialty Clinics" src="/images/stories/specialty_clinics/14.jpg" title="Specialty Clinics"/></p>
<h4>Quarterbacks &amp; Receivers Clinics - Sunday 9:30am-4:00pm </h4>
<p><strong>For players entering Grades 4 through 12</strong></p>
<p>QUARTERBACKS and RECEIVERS are two of the most skilled positions in the game and  demand a great deal of discipline and repetition to perfect the skills and techniques necessary to perform at these positions. In most cases, fall team practices can not devote the individualized attention and concentration in the two areas of passing and receiving.  EXCLUSIVE concentration on Quarterbacking and Receiving instructed by Skilled Coaches who have achieved measures of greatness in these two specific areas, both as players and coaches.  Each athlete will receive a total football experience, development of confidence and competence, direction for proper training and conditioning, the importance of academic grades for college recruiting. </p>
<p>Equipment: Helmets and Shoulder pads</p>
<p> </p>
<p> </p>
<p><img alt="Specialty Clinics" src="/images/stories/specialty_clinics/15.jpg" title="Specialty Clinics"/></p>
<h4>Pro-Style Combines - Thursday 2:00pm-4:00pm</h4>
<p><strong>For players entering grades 9 through 12.</strong></p>
<p>If you are a high school student and want to play college football this is the best way to let college coaches know your ability.  The combine will test and record each participant's abilities through a series of specific pro-style drills administrated by college coaches.  These events include a 10 yard sprint, 40 yard sprint, pro-style shuttle run, vertical jump, long jump and pro-style "L" drill.  You will receive ten official comprehensive personalized performance reports showcasing your talents.  In addition your results will be distributed to all camp coaches and offered to all colleges.</p>
<p> </p>
<p><span class="green"><a href="/registration">Register Now!</a></span></p> </div>
</div>
<!-- End Content -->
</main>
</div>
</div>
</div>
<!-- Footer -->
<footer class="footer" role="contentinfo">
<div class="container-fluid">
<div id="my-footer">
<p>
				© 2021 Vermont Football Camps |   13 Black Road Berlin, Vermont 05602<br/>
<a href="tel:+18022299653">802-229-9653</a>    |    <a href="mailto:info@vtfootballcamps.com">info@vtfootballcamps.com</a>
</p>
<p> </p>
<p><a href="https://www.vtwebmarketing.com" target="_blank" title="Web Site Maintained by Vermont Web Marketing">Web Site Maintained by Vermont Web Marketing</a></p>
</div>
</div>
</footer>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74190750-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
