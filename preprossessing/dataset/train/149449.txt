<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "45591",
      cRay: "610e9859fd73c345",
      cHash: "6d7b38764d821a9",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXJlbmF2aXNpb24udXMv",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "1JU15F6atUuUTbn11QH/kSaCWGdqVcDXNsJ8+n98Kx3OmG7s90kdwhk/uaPePSMA9eR8QjPXayPHKgMMgXntt6CuCcHobggiPjdiSukEwCGQyex7uJ1kJdlKbNamymFOaWXuIk2psKLmpRjWxS56CZ3zqbCH5WqzKZTKX0Y1eo9laJ3dNfHgX6/85c9Ybjmul0CJtQZiEjuUVeXqEiKvbfQW0UwyeGv3dWd2JNh/WXnuUXnZ2Ko953kVDxYIEKVjk4XPqEn/a5e9KhEq4slDVcs/cr3JPcnY9u42pQMvqomjihe1uZlgUp8ooU4jQWL8jdJb32RcyePkQX0fxfzlrCqbOEf9pdmxRbrvkqwaKbCKFMfFCjkewsZbXocsmuD//tAPnkyrSy2NfO/HwF04U+r1N1queGygjV6bbhFtIDHeht1H/EBuqATicOQkXPpRcvs67bFU+XGMUbch3BIovRu5UCxXXpPCKB09jSzeIn85v1rvcwJSBjqU8AG3kMbKTkLyiW1n1Dws4DywC6aEPNPPYWaRoWQ365PkeG2HzX7i71nhT6++QJ2gZo3I2c/gRpnXcS9q4vC14D6a7qDQiOVxDzqlcCbvdpaiClS9pIAF15ep+p2MLaYAy6fwwj2nRGsHHr2dOp8KuFpwY0kyba7AQPS3JZRMguDjsn1bXMI022BlUkRrEKlYqM/xLNsGeemBTdKzB7UXDiTugkMxyFbmJTZwlbIYIwOywQ1hCytFJdLNW+FjXBo3iAjKI+joNd0KeSUNYgjsZYy3plk+Hg==",
        t: "MTYxMDUzNTQzMy4yOTAwMDA=",
        m: "k8NRbZPL4QWIr3EeiiM+FZ7HFLSqAmgMAKIpmf5g4fE=",
        i1: "RgpjobcWCKZ/wmZpOZd/OQ==",
        i2: "65dE2jGmmCgjiz8BJ+VRfg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "NHp0C4nejn/zjw27RszzJKBy4ao+c3e+veeaetY70os=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.arenavision.us</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=8b9e08ddddf34df83c4a279748e987776de6c789-1610535433-0-ARZanOWWf1unXKICBX_7IA_rL4ws7kNRN00ua6x7qKDudNcmU9rk2jnx2AZgSkmxsPYIF6BWidGd-Bvd9QkVZehv7l5sFvgMG5ARWNP7g54uSkFBTO__iO6lJE6pQ43wZ-6vA5zWOtuD72l5PpoFUQil-UuRJ3tErlS9DXwtaY9vQTTjHluRAInY_50q8EK_1XbXkYC33EHoFOW18vBWVuKcZR7kSz_vksqNTq8JZXQR3kooi0bPKfhzSGZJD2m4hrkBogj4WcJ0sLC5dF1CnGbc7eCwRvsKeBPtMZGGn8RO6Nuco646nLrFBhVXnIsIH0EdtXsOdzx8d7Z_lHnS2iC08_OQzk0Wv9qSEIek5JSFG_obWxqvdvLYn99_ppVkc71_xUBviOUH16A0iFNY75Xpqq9qkAKyfK9Nx4jV-vwaOUpS5HJCo9uTC6c1uigul9wKjHW6HQMazVQjW1Hzko3GonKGBGEVHSEh1voEn87wgquMooUrrCSSEfKpd0MyfjK0nlHZE0yIc_Wf1B76TzfLNpNktzKVLpORhEeb7DPxeU-BMb5fzfnUr3_5VMFzv-H_52wQuuY4jzlmHKnFulw" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="0950cb92d4ad487bc1abf1152012fce0da1d4a7d-1610535433-0-AZCL7iHcCkmyuU00wm0bO93dBec6WNsOThf13q3sFFPfJg0L+UNQbk2W5Z3KK4sz5CTyoXzNDakBkl+tfb/jZfKaoRJ9iIDeFp5ej4G93HxD3Z1HUb8r/hv4ENMbRzhqWpvH8eEO8ey+t3/Rj/wuqHcIbbF7VAXXSvBaVLkJHgM0YKnxssJDSTRUg06w3DJckjHInjPJeEHRzuWWD71rTvU1/2/IPqu2tA4ZFKP73M2KrrBecDXiqORntBqF1i6buXJBiUcTIa3bXUUtu6SsobIN/sisVBYZaFjph17rN8WowwXIFRXEc1giENrvFU54eibC102dBCptyJ/NBH47Tcj/7Xwzf3sLU4GGoHr8MYQdsNqJZHZEwlwu1qVcU03b+IkunfsE0PfvzC/KLAEc0uojB5fv4vJcGMMaGTczzcaZzC+/PLhsyqPFKYZi7WnIOKyDSPmyesu/8SSQ+/DMECzRxxBDWFfAKoIARgbu6HxcYyquOKmpiWhKhGwqjpntMWDf49YJjmIujtbeX1gTVo8Bht1mVOlKvp0UUJCLPiUSOH6VwRvs6cbuHLIAgxKbAAdJdz2qweAtms1H7Shaie6IZNr/Dnq/6kofIijODLcWNVFOHCB3MkfQmCOSYEepBJymnCa5xCEtF/Q4AddFytpEfI3W+uaqR77m87iwGGtsgP0T6svu1+8mdGLbuetMa0keMkkLERLkDN0mPq6+hou4+GGIx2xr7vrKf7jzeNlB7aA/7S95x6FIj/uVhAg8C2IKS9Qc6+SU8PTzxLdyzt5103FGCgCmbTjWMwT8fAKsBIxoAmoaJcepZZVF+vYOuaVCN5GK2rK8T7RHAu5bz8LdzeImJ/MD7INFzc8xkJ/z3GOImo+0IL4FxoBCd5fOz2TDzAQes/XF4icMJV/RPrYcxzc6SCV+q92PghFenvuaJgBGr/gwPmd51PKUfhUlTPJTSxgroL3mic7mDO9ty0FvkHxItc4O81T7ZQDgwrh/fTgg1Q8QlAz2eR34Ex6hRXKhdf6fJDtqGi6R5xm4vz+DiHsV4n8kxLomRCDb8OtUMud0xCaF9qDqQkkLUr3k6CxsSven2DFznjzs2F79lu9MZ3cL3k3PmC2Lgri6QdX48nCqcL4vIrD7ve63tp1oMfMFJY2KjpxyAOtypYIRTpp/UiofJzjf+VJVaAwh6ilNLY8GMtikzFk3Zm9VDtYJzOsm553PqphaaQqwYAv8+Z+5HfTzJZVp1+gOUZk9AvoSN549Wqt/11xRsG1QNkdaZJe7SfY1Zn0595UesYELRWz7K+dQz7XqBsEewX3lbTzpfRfqMqV4mHGldMYx6MsX2g=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="7dc2d4559ca578509afb0b557111eed5"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610e9859fd73c345')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<div style="position: absolute; top: -250px; left: -250px;"><a href="https://darksoulz.us/corporeal.php?op=422">table</a></div>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610e9859fd73c345</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
