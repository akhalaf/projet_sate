<!DOCTYPE HTML>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Search for products and services in your area. Find business close by. Business owners, list your business with us and get valuable exposure." name="Description"/>
<meta content="yellowpages Jamaica, Barbados, Trinidad and Tobago, Cayman Islands, bahamas, guyana, Caribbean online directory" name="Keywords"/>
<meta content="&lt;!--robots--&gt;" name="robots"/>
<link href="/touch-icon-iphone.png" rel="apple-touch-icon"/>
<!--<link rel="stylesheet" href="https://www.bizexposed.com/js/jquery-ui.css" />
		<link href="https://www.bizexposed.com/style/generalt.css" rel="stylesheet" type="text/css" /> -->
<link href="https://www.bizexposed.com/style/jquery_ui_generaltt_fancybox.css" rel="stylesheet" type="text/css"/>
<link href="https://www.bizexposed.com/style/fontawesome.min.css" rel="stylesheet" type="text/css"/>
<!--<link href="https://www.bizexposed.com/style/jquery.fancybox.css" rel="stylesheet" type="text/css" /> -->
<!--	<link href="/js/rateit/rateit.css" rel="stylesheet" type="text/css" /> -->
<!--css_includes-->
<link href="https://www.bizexposed.com/images/wob.ico" rel="shortcut icon" type="image/x-icon"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<style type="text/css">
		    .large_rec { width: 300px; height: 250px; }
		    .large_hori { width: 320px; height: 100px; }
            @media (min-width:500px) { .large_rec { width: 336px; height: 280px; } .large_hori { width: 468px; height: 60px; } }
            @media (min-width:800px) { .large_rec { width: 336px; height: 280px; } .large_hori { width: 728px; height: 90px; } }  
		</style>
<title>
			Online business directory, where you find the world of business.
		</title>
<!--[if IE 7]>
			<style>
				.search-form {margin-top: 5px;}
				.search-category{height:27px}
				.main-button{height:27px}
			</style>
		<![endif]-->
</head>
<body>
<div class="banner">
<div class="banner-top">
<div class="links" style="font-weight:bold; padding-top:3px;">
<span class="not_for_mobile">   </span>
<a href="https://www.bizexposed.com" style="color:white">Home</a><span class="not_for_mobile">   </span>
<a class="not_for_mobile" href="https://www.bizexposed.com/BizAdmin/Listing_Requirement.php" style="color:white">Get Listed</a>  
				<a class="not_for_mobile" href="https://www.bizexposed.com/WhatsNew.php" style="color:white">Whats New</a><span class="not_for_mobile">   </span>
<a href="https://www.bizexposed.com/ContactUs.php" style="color:white">Contact Us</a>
<!--<a style="color:white" href="https://promo.bizexposed.com">Promotions</a>--> <!--<a href="http://jobs.bizexposed.com">Jobs and Career Links</a>-->
</div>
<div class="login" style="font-weight:bold; padding-top:3px;">
<a href="https://www.bizexposed.com/BizAdmin/Login_Register.php" style="color:white">Log In</a>  <a href="https://www.bizexposed.com/BizAdmin/Register.php" style="color:white">Register</a> 					
				</div>
</div>
<div class="banner-bottom">
<div id="BD-logo"><a href="//www.bizexposed.com">
<img height="45px" src="/images/bizexposed_logo_1.png"/>
</a>
<!--   
					<h2><--website_name-- ></h2>
					<h3><--website_slogan-- ></h3>	
					-->
</div>
<div id="BD-search">
<form action="https://www.bizexposed.com/biznizdetails.php" class="search-form" id="search" method="get" name="search" onsubmit="return formSelect()">
<input class="query" id="QUERY" name="QUERY" placeholder="Business or Product Name, City, Country" required="" size="30" title="Enter a product name or a business name along with a city/country" type="text" value="" x-webkit-speech=""/>
<input class="search_btn" type="submit" value="GO"/>
</form>
</div>
<div id="BD-review">
<!--priority_review-->
</div>
</div>
</div>
<!--
		<div class="banner-bottom">	
			<div class="banner-logo-container"> 
				<div class="banner-logo-pic"> <img src="images/people.jpg"/ height="150px" width="110px">	</div> 		
				<div class="banner-logo"> <a class="logo" href=""> World of Business </a> "<span> Your business, your world</span>"	</div> 
			</div>			 
		</div> 
		
-->
<!-- <div class="sidebar"> REPORT PROBLEM </div> 
		
		<div id="main-container">
			<header>			
				<script>		//	document.write("Total Width: " + screen.width);			</script>
				<div class="top">  		
				
				</div>								
			</header>  
		-->
<!-- Menu starts -->
<!-- Menu ends -->
<!-- Content container starts -->
<div id="content-container clearfix">
<!--  split here for header -->
<div class="content">
<!--heading-->
<!--userdef3_data-->
<div class="logo-pics"><div class="logo-heading"> Featured Businesses </div><div class="logo-slide"> <div class="logo-pic-ind" title="Vanguard Internet Marketing"><a href="/Texas-USA/B/Vanguard_Internet_Marketing-Houston.php"><img alt="Vanguard Internet Marketing" data-src="/BizImages/05/logo_16912512_-10805150838.png" height="130" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" style="vertical-align:top;" width="140"/></a></div><div class="logo-pic-ind" title="Critical Systems Inc"><a href="/Idaho-USA/B/Critical_Systems_Inc-Boise.php"><img alt="Critical Systems Inc" data-src="/BizImages/04/logo_16912513_-10804150222.jpg" height="130" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" style="vertical-align:top;" width="140"/></a></div><div class="logo-pic-ind" title="Mr. Plumber"><a href="/Georgia-USA/B/Mr_Plumber-Valdosta.php"><img alt="Mr. Plumber" data-src="/BizImages/04/logo_16912514_-10804150226.png" height="130" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" style="vertical-align:top;" width="140"/></a></div><div class="logo-pic-ind" title="UGI Heating, Cooling &amp; Plumbing"><a href="/Pennsylvania-USA/B/UGI_Heating_Cooling_and_Plumbing-Whitehall.php"><img alt="UGI Heating, Cooling &amp; Plumbing" data-src="/BizImages/04/logo_16912515_-10804150233.jpg" height="130" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" style="vertical-align:top;" width="140"/></a></div><div class="logo-pic-ind" title="Talbot Arms"><a href="/Maryland-USA/B/Talbot_Arms-Sherwood.php"><img alt="Talbot Arms" data-src="/BizImages/04/logo_16912516_-10804150235.jpg" height="130" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" style="vertical-align:top;" width="140"/></a></div><div class="logo-pic-ind" title="Walter's Masonry"><a href="/Illinois-USA/B/Walters_Masonry-Chicago.php"><img alt="Walter's Masonry" data-src="/BizImages/04/logo_16912517_-10804150238.jpg" height="130" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" style="vertical-align:top;" width="140"/></a></div></div></div>
<!--bread_crumbs-->
<div class="sales-pitch"><div style="float:left">
<h2>Not Listed: <a href="/BizAdmin/Listing_Requirement.php">Get Listed</a>. </h2>
<div class="sub-promo">Push your business to the world. <a class="toggle_benefits off" href="#">More</a></div>
</div>
<div style="float:left; margin-left:200px">
<h2>Already Listed: Enhance Your Listing.</h2>
<div class="sub-promo">Search for and own your presence <a class="toggle_benefits off" href="#">More</a></div>
</div>
<div id="benefits" style="width:100%; float:left; margin-top:20px; display:none;">
<p>Create a business profile on bizexposed.com for a review cost of US$9.99. Upload your logo and get promoted as one of the featured listings. 
	Your listing can include any or all of the following:</p>
<div style="position:relative; display:table">
<ul>
<li>Description of business</li>
<li>Include business in multiple categories</li>
<li>Business Logo</li>
<li>Business pictures</li>
<li>List of products/service</li>
<li>picture of each product</li>
<li>Profile page dedicated to promoting your business</li>
<li>Multiple location/Branches</li>
<li>Brands carried</li>
<li>Service areas</li>
<li>Hours of operation</li>
</ul>
</div>
<div style="position:relative; width:800px; margin-bottom:20px;">
		Customers can locate you business by browsing the directory or searching for your business by name, products/services offered or category listed in.
	</div>
</div>
</div><div class="clearfix"> </div><div class="section-wide"><div class="two-third"><div class="desc-cont"><p> </p><h2>Quick Links</h2><div id="usertopbiz_inline"></div><script type="text/javascript"> 
						var cid = 90040221;
						var iid = 5429724; 
					 </script><div class="half"><p><a href="/Jamaica/B/Nice_N_Easy_Tours_and_Taxi_Service-Portmore.php" style="font-weight:bold;">Nice N Easy Tours &amp; Taxi Service</a></p><p style="width:100%; border:0px #000 solid;">
<span style="float:left;">Portmore</span> 
			<span>Saint Catherine</span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/B/Registrar_Generals_Department-Spanish_Town.php" style="font-weight:bold;">Registrar General's Department</a></p><p style="width:100%; border:0px #000 solid;">
<span style="float:left;">Spanish Town</span> 
			<span>Saint Catherine</span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/B/Bashco_Trading_Co_Ltd-May_Pen.php" style="font-weight:bold;">Bashco Trading Co Ltd</a></p><p style="width:100%; border:0px #000 solid;">
<span style="float:left;">May Pen</span> 
			<span>Clarendon</span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/B/Lucky_Dollar_Furniture_and_Appliance_Store-Kingston_10.php" style="font-weight:bold;">Lucky Dollar Furniture &amp; Appliance Store</a></p><p style="width:100%; border:0px #000 solid;">
<span style="float:left;">Kingston 10</span> 
			<span>Kingston</span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/B/Stewarts_Hardware_Ltd-May_Pen.php" style="font-weight:bold;">Stewart's Hardware Ltd</a></p><p style="width:100%; border:0px #000 solid;">
<span style="float:left;">May Pen</span> 
			<span>Clarendon</span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/B/Discount_Lumber_and_Hardware-Montego_Bay.php" style="font-weight:bold;">Discount Lumber &amp; Hardware</a></p><p style="width:100%; border:0px #000 solid;">
<span style="float:left;">Montego Bay</span> 
			<span>Saint James</span> <span>Jamaica</span></p></div><div class="section-full"><div class="category-heading">Top Categories</div><div class="categories-content"> <div class="half"><p><a href="/Jamaica/C0/Beauty_Salons.php" style="font-weight:bold;">Beauty Salons</a>
<span></span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/C7/Boats.php" style="font-weight:bold;">Boats</a>
<span></span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/C8/Blood_Banks_And_Centres.php" style="font-weight:bold;">Blood Banks And Centres</a>
<span></span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/C4/Waste_Management.php" style="font-weight:bold;">Waste Management</a>
<span></span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/C1/Waste_Management_Companies.php" style="font-weight:bold;">Waste Management Companies</a>
<span></span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/C2/Taxi_Services.php" style="font-weight:bold;">Taxi Services</a>
<span></span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/C1/Tanks_all_Types.php" style="font-weight:bold;">Tanks-all Types</a>
<span></span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/C1/Taverns.php" style="font-weight:bold;">Taverns</a>
<span></span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/C1/Trucking_contract_Haulers.php" style="font-weight:bold;">Trucking-contract Haulers</a>
<span></span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/C6/Laboratories.php" style="font-weight:bold;">Laboratories</a>
<span></span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/C7/Investment.php" style="font-weight:bold;">Investment</a>
<span></span> <span>Jamaica</span></p></div><div class="half"><p><a href="/Jamaica/C7/Refrigerating_Equipment.php" style="font-weight:bold;">Refrigerating Equipment</a>
<span></span> <span>Jamaica</span></p></div></div></div></div></div><p> </p><div class="one-third"><div class="section-full"><div class="category-heading">Advertisements</div><div class="categories-content"> <script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- medi_rec -->
<ins class="adsbygoogle" data-ad-client="ca-pub-7996693934539034" data-ad-slot="8709577118" style="display:inline-block;width:300px;height:250px"></ins>
<script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script></div></div><div class="section-full"><div class="category-heading">Top Countries</div><div class="categories-content"> <div class="two-third"><a href="/Jamaica-directory.php">Jamaica</a></div><div class="two-third"><a href="/Trinidad_and_Tobago-directory.php">Trinidad and Tobago</a></div><div class="two-third"><a href="/Barbados-directory.php">Barbados</a></div><div class="two-third"><a href="/United_States-directory.php">United States</a></div><div class="two-third"><a href="/Guyana-directory.php">Guyana</a></div><div class="two-third"><a href="/United_Kingdom-directory.php">United Kingdom</a></div><div class="two-third"><a href="/Bahamas-directory.php">Bahamas</a></div><div class="two-third"><a href="/Australia-directory.php">Australia</a></div><div class="two-third"><a href="/Canada-directory.php">Canada</a></div><div class="two-third"><a href="/Cayman_Islands-directory.php">Cayman Islands</a></div><div class="two-third"><a href="/China-directory.php">China</a></div><div class="two-third"><a href="/Philippines-directory.php">Philippines</a></div><div class="two-third"><a href="/Countries.php">All Countries</a></div></div></div></div></div><div class="clearfix"> </div>
<!--description_data-->
<!--products_data-->
<!--links_data-->
<!--owners_data-->
<div class="section-wide"><div class="tab-title"><div class="tab-header">New Listings</div></div><div class="desc-cont"><div class="half"><a href="/Indiana-USA/B/Pillow_Logistics_Inc-Indianapolis.php" style="font-weight:bold;">Pillow Logistics, Inc</a><br/>Founded in 1988, Pillow Logistics provides courier, warehouse and mailroom services daily.  We take ...</div><div class="half"><a href="/Florida-USA/B/Stay_Healthy_Vending_Miami-Miami.php" style="font-weight:bold;">Stay Healthy Vending Miami</a><br/>We specialize in healthy vending machines. We provide them for free for any establishment, with healthy snacks inside...</div><div class="half"><a href="/Louisiana-USA/B/The_Johnson_Firm-Lake_Charles.php" style="font-weight:bold;">The Johnson Firm</a><br/>Lawyers at The Johnson Firm, Lake Charles, Louisiana are experienced in personal injury, family, and criminal defense...</div><div class="half"><a href="/Lanarkshire-GBR/B/Glasgow_Double_Glazing-Glasgow.php" style="font-weight:bold;">Glasgow Double Glazing</a><br/>Glasgow Double glazing are local experts in both double and triple glazing windows that serve as both stylish and functi...</div><div class="half"><a href="/Jamaica/B/Dawgen_Global-Mandeville.php" style="font-weight:bold;">Dawgen Global</a><br/>Dawgen Global is an integrated multidisciplinary professional service firm in the Caribbean Region. We are integrated...</div><div class="half"><a href="/Jamaica/B/Dawgen_Global-Montego_Bay.php" style="font-weight:bold;">Dawgen Global</a><br/>Dawgen Global is an integrated multidisciplinary professional service firm in the Caribbean Region. We are integrated...</div></div></div><div class="clearfix"> </div>
<div class="section-wide"><div class="tab-title"><div class="tab-header">Advertisement</div></div><div class="desc-cont"><div style="width:100%; text-align:center; margin-top:10px; margin-bottom:10px"><script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- responsive -->
<ins class="adsbygoogle large_hori" data-ad-client="ca-pub-7996693934539034" data-ad-format="auto" data-ad-slot="4018597422" style="display:block"></ins>
<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script></div></div></div>
</div><!--  split here for footer -->
</div>
<!-- Content container ends -->
<!-- footer starts -->
<div class="footer">
<div class="left-container footer-left">
<ul class="clearfix">
</ul>
</div>
<div class="footer-right">
<div class="padding-huge">
<div>
<a href="/Privacy_Policy.php">Privacy Policy</a> | 
					<a href="/AboutUs.php">About Us</a> | 
					<a href="https://www.bizexposed.com/ContactUs.php">Contact Us</a>
</div>
<div>
<!--&copy; 2007 - 2014 the biz niz dir .com.  All rights reserved.-->	
                     © Copyright 2007 - 2021 BizExposed.com. All rights reserved. 		
				</div>
</div>
</div>
<div id="footer-static">
<!--<form  action="/biznizdetails.php">
			bizexposed.com Search
			
			<input type="text"  id="fquery" name="fquery" placeholder="Product/Business, City, Country" title="Enter a product name or a business name along with a city/country" x-webkit-speech />	
			<input type="submit" value="go">
			</form>
			-->
<!--
			<div class="login">  	   
					<a href="">Login </a>		 		
			</div>
			-->
</div>
</div>
<!-- footer ends -->
<!--</div>-->
<div id="element_to_pop_up">
<span class="b-close"><span>X</span></span>
<div id="pcontent"></div>
</div>
<!--just_before_body-->
<script type="text/javascript">(function() {
                              function getScript(url,success){
                                var script=document.createElement('script');
                                script.src=url;
                                var head=document.getElementsByTagName('head')[0],
                                    done=false;
                                script.onload=script.onreadystatechange = function(){
                                  if ( !done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete') ) {
                                    done=true;
                                    success();
                                    script.onload = script.onreadystatechange = null;
                                    head.removeChild(script);
                                  }
                                };
                                head.appendChild(script);
                              }
                                getScript('https://www.bizexposed.com/js/jquery.min.js',function(){
                                        getScript('https://www.bizexposed.com/js/jquery-ui.js',function(){
                                              getScript('https://www.bizexposed.com/js/ajaxfields.js',function(){
                                                      getScript('https://www.bizexposed.com/js/jquery.fancybox.pack.js',function(){
                                                              getScript('https://www.bizexposed.com/js/tabss.js',function(){
                                                                 getScript('https://www.bizexposed.com/js/external.js',function(){
                                                                     getScript('https://www.bizexposed.com/js/fontawesome.min.js',function(){
                                                                     
                                                                          //your tab code here
                                                                          console.log('complete');
                                                                        
                                                                    })
                                                                    
                                                                })
                                                            })
                                                        })
                                                })
                                        })
                                });
                            })();
                            </script>
[
<!--late_script-->
]
<!--
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42145288-1', 'bizexposed.com');
  ga('send', 'pageview');

</script>

	<script async type="text/javascript"
      src="//maps.googleapis.com/maps/api/js?key=AIzaSyCCpEPHVyZ51FfyYQF-_DW0-dOdr7bH868&sensor=false">
    </script>
   -->
</body>
</html>
