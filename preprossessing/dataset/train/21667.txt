<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>财盾网</title>
<meta content="width=device-width,maximum-scale=1.0,user-scalable=no" name="viewport"/>
<link href="./css/main.css" rel="stylesheet"/>
</head>
<body>
<div class="container">
<div class="box">
<div class="header">
<img class="logo" src="images/logo.png"/>
<span>上海财盾信息技术有限公司</span>
</div>
<div class="slogan">
<div class="title">科技，让金融更简单</div>
<div class="subtitle">搭建共享、共惠、共赢的开放式平台</div>
<br/>
</div>
<ul class="contents">
<li>
<div class="li-bg"></div>
<img src="images/icon1.png"/>
<h1>工商信息</h1>
<p>
                        公司名称：上海财盾信息技术有限公司
                        <br/> 注册号：310141000156542
                        <br/> 组织机构代码：342037233
                        <br/> 所属行业：软件和信息技术服务业
                        <br/> 公司类型：有限责任公司（非自然人投资或控股的法人独资)
                        <br/> 公司地址：上海市徐汇区凯滨路206号20-21楼
                        <br/>
</p>
</li>
<li>
<div class="li-bg"></div>
<img src="images/icon2.png"/>
<h1>公司愿景</h1>
<p>成为最受尊敬的互联网企业是财盾的愿景目标。财盾一直积极参与公益事业、努力承担企业社会责任、推动网络文明。每一项产品与业务都拥抱公益，开放互联，并倡导所有企业一起行动，通过互联网领域的技术、传播优势，缔造“人人可公益，民众齐参与”的互联网公益新生态。
                    </p>
</li>
<li>
<div class="li-bg"></div>
<img src="images/icon3.png"/>
<h1>经营范围</h1>
<p>
                        从事计算机信息技术、软件科技领域内的技术开发、技术咨询、技术转让、技术服务，计算机系统集成，计算机数据处理，企业管理咨询、商务信息咨询、财务咨询，投资咨询，市场信息咨询与调查（不得从事社会调研、社会调查、民意调查、民意测验），企业形象策划，设计、制作、代理、利用自有媒体发布广告。 【依法须经批准的项目，经相关部门批准后方可开展经营活动】
                    </p>
</li>
</ul>
<div class="footer">
<div class="line"></div>
<div class="copyright">
                    copyright © shanghaicaidun. All Rights Reserved. 上海财盾信息技术有限公司 版权所有
                </div>
<div class="mail"><span>zhangyuxiang653@pingan.com.cn</span></div>
<div class="phone"><span>02120660437</span></div>
</div>
</div></div></body>
</html>