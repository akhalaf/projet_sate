<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta charset="utf-8"/>
<title>制作ico图标 | 在线ico图标转换工具 方便制作favicon.ico - 比特虫 - Bitbug.net</title>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="轻松制作ico图标,在线提供ico图标转换工具,可以将jpg、jpeg、gif、png等图像转换成ico图像,方便浏览器制作并生成favicon.ico图标,提供ico图标下载,png to ico,jpg to ico,gif to ico" name="description"/>
<meta content="ico图标,icon图标,图标ico,favicon,ico, 在线favicon.ico生成,favicon制作工具, 转换jpg, 转换png,转换gif" name="keywords"/>
<link href="http://www.bitbug.net/favicon.ico" rel="shortcut icon"/>
<script type="text/javascript">timeStat=[];</script>
<link href="/css/vbase.min.css?t=2013111551" rel="stylesheet" type="text/css"/>
<link href="/css/main.min.css?t=2013111551" rel="stylesheet" type="text/css"/>
<script async="" data-ad-client="ca-pub-7633002528668288" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<body>
<div id="container">
<div class="yui3-g" id="head">
<div class="yui3-u-7-24" id="logo_desc">
<h1 style="margin: 0.3em 0;font-size:40px;text-align:center;color:#1d7ad9;">比特虫</h1>
</div>
<div class="yui3-u-17-24" id="ad_top">
<h1>在线制作ico图标</h1>
<h2>ico图标转换工具</h2>
</div>
</div>
<div class="yui3-g" id="menu">
<div class="yui3-u selected"><a href="/">制作ico图标</a></div>
<div class="yui3-u"><a href="/pagerank">PageRank查询</a></div>
</div>
<div class="yui3-g" id="content_full">
<h2 class="yui3-u-1">favicon图标介绍</h2>
<div class="yui3-u-1">
<strong class="important">favicon.ico</strong>一般用于作为缩略的网站标志,它显示位于浏览器的地址栏或者在标签上,用于显示网站的logo,如图<span class="important">红圈</span>的位置， 目前主要的浏览器都支持<em class="important">favicon.ico</em>图标.
        </div>
<div class="yui3-u-1">
<div class="yui3-g">
<div class="yui3-u">
<img alt="google ico for introduce favicon" src="img/eg_favicon.png"/>
</div>
<div class="yui3-u">
<img alt="yahoo ico for introduce favicon" src="img/eg_favicon2.png"/>
</div>
</div>
</div>
<div class="yui3-g">
<div class="yui3-u-1-2">
<div class="yui3-g box">
<h3 class="yui3-u-1">如何安装Favicon</h3>
<div class="yui3-u-1">
                        当成功生成<em class="important">favicon.ico</em>图像文件后,浏览器会自动弹出一个zip的压缩文件<br/>
                        将压缩文件中的<em class="important">favicon.ico</em>图像放在根目录下(也可以是其他目录)<br/>



                 在页面源文件的&lt;head&gt;&lt;/head&gt;标签之间插入<br/><strong>&lt;link rel="shortcut icon" href="    /favicon.ico" /&gt; </strong>
<br/>
<hr/>
                        最后形成:<br/>
                        &lt;head&gt;<br/>
                        ...<br/>
                        &lt;link rel="shortcut icon" href="/favicon.ico" /&gt;<br/>
                        &lt;/head&gt;

                    </div>
</div>
<br/>
</div>
<div class="yui3-u-1-2 rightbox">
<div class="yui3-g">
<h2 class="yui3-u-1">生成图标ico</h2>
<div class="yui3-u-1" id="convert_ico">
<!-- 生成图标ico -->
<div class="yui3-g">
<form action="/favicon" enctype="multipart/form-data" id="form_convert_ico" method="post" name="form_convert_ico">
<div class="yui3-u-1">
<label for="NewFile">原始图像: </label>
<input class="imgupload" id="NewFile" name="NewFile" type="file"/>
</div>
<div class="yui3-u-1">
<label for="select_size">目标尺寸: </label>
<select class="imgupload" id="select_size" name="select_size">
<option value="1">16x16  -</option>
<option selected="selected" value="2">32x32  -</option>
<option value="3">48x48  -</option>
<option value="4">64x64  -</option>
<option value="5">128x128  -</option>
</select>
</div>
<div class="yui3-u-1">
<div class="yui3-g">
<div class="yui3-u">
<label for="randcode">附  加  码: </label>
<input class="imgupload" id="randcode" name="randcode" size="4" type="text"/>  
                                    </div>
<div class="yui3-u">
<a href="javascript:void(0)"><img alt="Random Image Code" id="captcha_code" src="/code"/></a>
</div>
</div>
</div>
<div class="yui3-u-1">
<div id="err_msg"></div>
</div>
<div class="yui3-u-1">
<input class="gen_ico" id="genico" name="genico" type="submit" value="生成ico图标"/>
<input id="randcode_x" name="randcode_x" size="4" type="hidden"/>
</div>
</form>
</div>
</div>
<div class="yui3-u-1">
<h2>使用说明</h2>
<p>
                        - <abbr>原始图像</abbr>可以接受: .jpg .jpeg .gif .png等图像格式<br/>
                        - 原始图像文件大小限制在小于<em class="important">500k</em><br/>
                        - 建议制作一张400x400的jpg图像, 然后等比缩小到你想转换的ico尺寸,最后用比特虫转换成<strong>ico图标</strong>格式.<br/>
                        - 当然你也可以直接把原始尺寸的图像通过比特虫直接转换，比特虫会自动将图片缩小到合适的<strong>ico图标</strong>尺寸，不过可能会在缩小的时候有所失真

                        </p>
</div>
</div>
</div>
</div>
</div>
<div class="yui3-g" id="footer">
<div class="yui3-u-1">
    © 2006-2013 <a href="javascript:void(0)">bitbug.net</a>
</div>
<div class="yui3-u-1">
<a href="javascript:void(0)"><img alt="My Google PageRank" src="/mypagerank.php?style=1"/></a>
</div>
</div>
</div>
<script src="/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="/js/fav.min.js?t=2013111551" type="text/javascript"></script>
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?27d272a4207b1db9991885745fbdecec";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
</body>
</html>
