<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>Page not found | Walker and Hall  (formally Ormskirk Physiotherapy Centre)</title>
<link href="https://www.walkerandhall.co.uk/wp-content/themes/catalyst/images/favicon.png" rel="Shortcut Icon" type="image/x-icon"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.walkerandhall.co.uk/feed/" rel="alternate" title="Walker and Hall  (formally Ormskirk Physiotherapy Centre) » Feed" type="application/rss+xml"/>
<link href="https://www.walkerandhall.co.uk/comments/feed/" rel="alternate" title="Walker and Hall  (formally Ormskirk Physiotherapy Centre) » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.walkerandhall.co.uk\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.8"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.walkerandhall.co.uk/wp-content/themes/dynamik/css/dynamik-min.css?ver=1597508907" id="catalyst_minified_stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.walkerandhall.co.uk/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.walkerandhall.co.uk/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.walkerandhall.co.uk/xmlrpc.php" rel="pingback"/>
<link href="https://www.walkerandhall.co.uk/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.walkerandhall.co.uk/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.walkerandhall.co.uk/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<style media="screen" type="text/css">
	#rotator {
		position: relative;
		width: 1000px;
		height: 400px;
		margin: 0; padding: 0;
		overflow: hidden;
	}
</style>
<!-- <meta name="NextGEN" version="3.3.0" /> -->
<style media="all" type="text/css">
/* <![CDATA[ */
@import url("https://www.walkerandhall.co.uk/wp-content/plugins/wp-table-reloaded/css/plugin.css?ver=1.9.4");
@import url("https://www.walkerandhall.co.uk/wp-content/plugins/wp-table-reloaded/css/datatables.css?ver=1.9.4");
/* ]]> */
</style><!--[if lt IE 9]>
<script src="https://www.walkerandhall.co.uk/wp-content/themes/catalyst/lib/js/html5.js" type="text/javascript"></script>
<![endif]-->
</head>
<body class="error404 unknown-os unknown-browser left-sidebar catalyst_default logo-image" data-rsssl="1">
<div class="clearfix" id="wrap">
<div id="header-wrap">
<header id="header" role="banner">
<div id="header-left">
<p id="title"><a href="https://www.walkerandhall.co.uk/" title="Walker and Hall  (formally Ormskirk Physiotherapy Centre)">Walker and Hall  (formally Ormskirk Physiotherapy Centre)</a></p><p id="tagline">High quality physiotherapy management tailored to fit your individual needs</p></div>
<div id="header-right">
<div id="icons">
<img src="https://www.walkerandhall.co.uk/wp-content/themes/dynamik/css/images/connect.png"/>
<a href="https://www.facebook.com/WalkerandHall"><img src="https://www.walkerandhall.co.uk/wp-content/themes/dynamik/css/images/button-facebook.png"/></a><a href="https://twitter.com/WalkerandHallCP"><img src="https://www.walkerandhall.co.uk/wp-content/themes/dynamik/css/images/button-twitter.png"/></a><a href="#"><img src="https://www.walkerandhall.co.uk/wp-content/themes/dynamik/css/images/button-linkedin.png"/></a><a href="https://www.walkerandhall.co.uk/about/contact-us/"><img src="https://www.walkerandhall.co.uk/wp-content/themes/dynamik/css/images/button-email.png"/></a>
</div></div>
</header>
</div>
<div id="menu_wrap"><img src="https://www.walkerandhall.co.uk/wp-content/themes/dynamik/css/images/menu-wrap.png"/></div><div id="navbar-1-wrap"><nav class="clearfix" id="navbar-1" role="navigation"><div id="navbar-1-left"><ul class="nav-1 superfish" id="nav-1"><li class="home"><a href="https://www.walkerandhall.co.uk/" title="Home">Home</a></li><li class="page_item page-item-581 page_item_has_children"><a href="https://www.walkerandhall.co.uk/about/">About Us</a><ul class="children"><li class="page_item page-item-1341"><a href="https://www.walkerandhall.co.uk/about/meet-the-staff/">Meet the staff</a></li><li class="page_item page-item-1339"><a href="https://www.walkerandhall.co.uk/about/testimonials/">Testimonials</a></li><li class="page_item page-item-1661"><a href="https://www.walkerandhall.co.uk/about/contact-us/">Contact Us</a></li></ul></li><li class="page_item page-item-1346"><a href="https://www.walkerandhall.co.uk/what-is-physiotherapy/">What is Physiotherapy</a></li><li class="page_item page-item-1349 page_item_has_children"><a href="https://www.walkerandhall.co.uk/our-services-and-who-we-work-for/">Our Services &amp; Who We Work For</a><ul class="children"><li class="page_item page-item-1361"><a href="https://www.walkerandhall.co.uk/our-services-and-who-we-work-for/individuals/">Individuals</a></li><li class="page_item page-item-1358"><a href="https://www.walkerandhall.co.uk/our-services-and-who-we-work-for/solicitors-medico-legal-agencies/">Solicitors &amp; Medico-Legal Agencies</a></li><li class="page_item page-item-1364"><a href="https://www.walkerandhall.co.uk/our-services-and-who-we-work-for/rehabilitation-services/">Rehabilitation Services</a></li><li class="page_item page-item-1366"><a href="https://www.walkerandhall.co.uk/our-services-and-who-we-work-for/occupational-health-service/">Occupational Health Service</a></li><li class="page_item page-item-1369"><a href="https://www.walkerandhall.co.uk/our-services-and-who-we-work-for/national-health-service/">National Health Service</a></li></ul></li><li class="page_item page-item-1371 page_item_has_children"><a href="https://www.walkerandhall.co.uk/treatments/">Treatments</a><ul class="children"><li class="page_item page-item-1505"><a href="https://www.walkerandhall.co.uk/treatments/manual-therapy/">Manual Therapy</a></li><li class="page_item page-item-1508"><a href="https://www.walkerandhall.co.uk/treatments/electrical-therapy/">Electrical Therapy</a></li><li class="page_item page-item-1513"><a href="https://www.walkerandhall.co.uk/treatments/home-exercise-programme/">Home Exercise Programme</a></li></ul></li><li class="page_item page-item-1576"><a href="https://www.walkerandhall.co.uk/wilf/">WILF</a></li><li class="page_item page-item-1084"><a href="https://www.walkerandhall.co.uk/find-us/">Where we are</a></li><li class="page_item page-item-1383"><a href="https://www.walkerandhall.co.uk/freedownloads/">Resources</a></li><li class="page_item page-item-1381"><a href="https://www.walkerandhall.co.uk/room-rental/">Room Rental</a></li></ul></div><div class="navbar-right-text" id="navbar-1-right">tel: 01695 570002</div></nav></div>
<div id="banner"><img src="https://www.walkerandhall.co.uk/wp-content/themes/dynamik/css/images/banner-inside.jpg"/></div>
<div id="container-wrap">
<div id="container">
<div id="content-sidebar-wrap">
<div id="content-wrap">
<div class="hfeed" id="content" role="main">
<article class="page">
<header class="entry-header">
<h1 class="entry-title">Page Not Found</h1>
</header>
<div class="entry-content">
<p>The page you are looking for is not here. You can try a Search, use the Site Navigation or			<a href="https://www.walkerandhall.co.uk">return to the homepage</a>
			and try again.</p>
</div>
</article>
</div><!-- end #content -->
<div style="clear:both;"></div>
</div><!-- end #content-wrap -->
<div id="sidebar-1-wrap">
<div id="sidebar-1" role="complementary">
<link href="https://www.walkerandhall.co.uk/wp-content/plugins/grunion-contact-form/css/grunion.css?ver=4.9.8" id="grunion.css-css" media="all" rel="stylesheet" type="text/css"/>
<aside class="widget widget_text" id="text-2"><div class="widget-wrap"><h4 class="widgettitle">Get in touch with us!</h4>
<div class="textwidget"><div id="contact-form-widget-1">
<form action="#contact-form-widget-1" class="contact-form commentsblock" method="post">
<div>
<label class="name" for="widget-1-name">Name<span>(required)</span></label>
<input class="name" id="widget-1-name" name="widget-1-name" type="text" value=""/>
</div>
<div>
<label class="grunion-field-label email" for="widget-1-email">Email<span>(required)</span></label>
<input class="email" id="widget-1-email" name="widget-1-email" type="text" value=""/>
</div>
<div>
<label class="text" for="widget-1-telephone-if-youd-like-a-call-back">Telephone (if you'd like a call back)</label>
<input class="text" id="widget-1-telephone-if-youd-like-a-call-back" name="widget-1-telephone-if-youd-like-a-call-back" type="text" value=""/>
</div>
<div>
<label class="select" for="widget-1-nature-of-your-enquiry">Nature of your enquiry</label>
<select class="select" id="widget-1-nature-of-your-enquiry" name="widget-1-nature-of-your-enquiry" value=""></select>
<option>Select from the list</option>
<option>General Enquiry</option>
<option>Request a call back</option>
<option>New Instruction</option>
<option>Feedback</option>
<option>Other</option>
</div>
<div>
<label class="textarea" for="widget-1-enquiry">Enquiry<span>(required)</span></label>
<textarea id="contact-form-comment-widget-1-enquiry" name="widget-1-enquiry" rows="20"></textarea>
</div>
<p class="contact-submit">
<input class="pushbutton-wide" type="submit" value="Submit »"/>
<input id="_wpnonce" name="_wpnonce" type="hidden" value="86132aa067"/><input name="_wp_http_referer" type="hidden" value="/7832ghd"/>
<input name="contact-form-id" type="hidden" value="widget-1"/>
</p>
</form>
</div>
</div>
</div></aside>
</div><!-- end #sidebar-1 -->
<div style="clear:both;"></div>
</div><!-- end #sidebar-1-wrap -->
</div><!-- end #content-sidebar-wrap -->
</div><!-- end #container -->
<div style="clear:both;"></div>
</div><!-- end #container-wrap -->
<div style="clear:both;"></div>
<div id="footer_design"><img src="https://www.walkerandhall.co.uk/wp-content/themes/dynamik/css/images/bottom-design.png"/></div></div><!-- end #wrap -->
<div id="footer-wrap">
<footer class="clearfix" id="footer" role="contentinfo">
<p class="footer-content footer-center"> tel: 01695 570002 · 75 Aughton Street Ormskirk - Lancashire L39 3BN 
<br/>COPYRIGHT © Walker and Hall / <a href="https://www.theseoman.co.uk/" target="_blank">Website by The SEO Man</a>
</p>
</footer>
</div>
<!-- ngg_resource_manager_marker --><script src="https://www.walkerandhall.co.uk/wp-content/themes/catalyst/lib/js/navbars/superfish.js?ver=1.5.4" type="text/javascript"></script>
<script src="https://www.walkerandhall.co.uk/wp-content/themes/catalyst/lib/js/catalyst-responsive.js?ver=1.5.4" type="text/javascript"></script>
<script src="http://www.walkerandhall.co.uk/wp-content/plugins/wp-cycle/jquery.cycle.all.min.js?ver=2.9999.5" type="text/javascript"></script>
<script src="https://www.walkerandhall.co.uk/wp-includes/js/wp-embed.min.js?ver=4.9.8" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$("#rotator").cycle({ 
	    fx: 'fade',
	    timeout: 5000,
	    speed: 1000,
	    pause: 1,
	    fit: 1
	});
});
</script>
</body>
</html>
<!-- Page generated by LiteSpeed Cache 2.9.9.2 on 2021-01-12 19:49:47 -->