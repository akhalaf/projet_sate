<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="eng" xml:lang="eng" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>IDI Web Accessibility Checker : Web Accessibility Checker</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="AChecker - Copyright 2009 by ATRC http://atrc.utoronto.ca/" name="Generator"/>
<meta content="achecker,free, open source, accessibility checker, accessibility reviewer, accessibility evaluator, accessibility evaluation, WCAG evaluation, 508 evaluation, BITV evaluation, evaluate accessibility, test accessibility, review accessibility, ATRC, WCAG 2, STANCA, BITV, Section 508." name="keywords"/>
<meta content="AChecker is a Web accessibility evalution tool designed to help Web content developers and Web application developers ensure their Web content is accessible to everyone regardless to the technology they may be using, or their abilities or disabilities." name="description"/>
<base href="https://achecker.ca/"/>
<link href="https://achecker.ca/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://achecker.ca/themes/default/forms.css" rel="stylesheet" type="text/css"/>
<link href="https://achecker.ca/themes/default/styles.css" rel="stylesheet" type="text/css"/>
<!--[if IE]>
	  <link rel="stylesheet" href="https://achecker.ca/themes/default/ie_styles.css" type="text/css" />
	<![endif]-->
<script src="https://achecker.ca/jscripts/lib/jquery.js" type="text/javascript"></script>
<script src="https://achecker.ca/jscripts/lib/jquery-URLEncode.js" type="text/javascript"></script>
<script src="https://achecker.ca/jscripts/AChecker.js" type="text/javascript"></script>
<script type="text/javascript">
	  var _paq = window._paq || [];
	  _paq.push(['trackPageView']);
	  _paq.push(['enableLinkTracking']);
	  (function() {
	    var u="https://analytics.inclusivedesign.ca/";
	    _paq.push(['setTrackerUrl', u+'matomo.php']);
	    _paq.push(['setSiteId', '6']);
	    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
	    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
	  })();
	</script>
<script language="javascript" type="text/javascript">
	//<!--

var AChecker = AChecker || {};
AChecker.lang = AChecker.lang || {};

(function () {

    // Define langauge translations that are used in checker.js
    // @ see checker/js/checker.js
    AChecker.lang.provide_uri = "Please provide a uri!";
    AChecker.lang.provide_html_file = "Please provide a html file!";
    AChecker.lang.provide_upload_file = "Please upload html (or htm) file only!";
    AChecker.lang.provide_html_input = "Please provide a html input!";
    AChecker.lang.wait = "Please wait ...";
    AChecker.lang.get_file = "Get File";
    AChecker.lang.error_occur = "An error occured:";
    AChecker.lang.pass_decision = "Pass Decision Made";
    AChecker.lang.warning = "Warning: Confirmation Required";
    AChecker.lang.manual_check = "Manual Check Required";
    AChecker.lang.get_seal = '&nbsp;&nbsp;<span style="color:#E41B17;;border:thin solid #E41B17;"><a href="checker/index.php#seals_div">Conformance seal now issued.</a></span>';
    AChecker.lang.congrats_likely = '<img src="https://achecker.ca/images/feedback.gif" alt="Feedback" />  Congratulations! No likely problems.';
    AChecker.lang.congrats_potential = '<img src="https://achecker.ca/images/feedback.gif" alt="Feedback" />  Congratulations! No potential problems.';

})();
	//-->
	</script>
<script src="https://achecker.ca/checker/js/checker.js" type="text/javascript"></script>
</head>
<body onload="AChecker.input.initialize('AC_by_uri', 'by_guideline');">
<div id="liquid-round"><div class="top"><span></span></div>
<div class="center-content" id="center-content">
<div id="logo">
<a href="http://www.atutor.ca/achecker/"><img alt="AChecker" src="https://achecker.ca/themes/default/images/checker_logo.png" style="border:none;"/></a>
</div>
<div id="banner">
<span id="logininfo">
<a href="https://achecker.ca/login.php">Login</a>
				  
				<a href="https://achecker.ca/register.php">Register</a>
</span>
</div>
<div class="topnavlistcontainer">
<!-- the main navigation. in our case, tabs -->
<ul class="navigation">
<li class="navigation"><a class="active" href="/checker/index.php" title="Web Accessibility Checker"><span class="nav">Web Accessibility Checker</span></a></li>
</ul>
</div>
<!-- the sub navigation and guide -->
<div id="sub-menu">
<!-- guide -->
<div>
<a href="/documentation/index.php?p=checker/index.php" id="guide" onclick="AChecker.popup('/documentation/index.php?p=checker/index.php'); return false;" target="_new" title="AChecker Handbook: Web Accessibility Checker"><em>Web Accessibility Checker</em></a>
</div>
<!-- the sub navigation -->
<div id="sub-navigation">
					 
				</div>
</div>
<a name="content" title="Start Content"></a>
<table style="width:100%">
<tr>
<td>
<div class="center-input-form">
<form action="/checker/index.php" enctype="multipart/form-data" method="post" name="input_form">
<div class="left-col" style="float:left;clear:left;"><br/>
<fieldset class="group_form"><legend class="group_form">Check Accessibility By:</legend>
<div class="topnavlistcontainer"><br/>
<ul class="navigation">
<li class="navigation"><a accesskey="a" class="active" href="javascript:void(0)" id="AC_menu_by_uri" onclick="return AChecker.input.onClickTab('AC_by_uri');" title="Web Page URL Alt+1"><span class="nav">Web Page URL</span></a></li>
<li class="navigation"><a accesskey="b" href="javascript:void(0)" id="AC_menu_by_upload" onclick="return AChecker.input.onClickTab('AC_by_upload');" title="HTML File Upload Alt+2"><span class="nav">HTML File Upload</span></a></li>
<li class="navigation"><a accesskey="c" href="javascript:void(0)" id="AC_menu_by_paste" onclick="return AChecker.input.onClickTab('AC_by_paste');" title="Paste HTML Markup Alt+3"><span class="nav">Paste HTML Markup</span></a></li>
</ul>
</div>
<div class="input_tab" id="AC_by_uri" style="display:block">
<div style="text-align:center;">
<label for="checkuri">Address:</label>
<input id="checkuri" name="uri" size="50" type="text" value=""/>
<div class="validation_submit_div">
<div class="spinner_div">
<img alt="Accessibility Validation in Progress" class="spinner_img" id="AC_spinner_by_uri" src="https://achecker.ca/themes/default/images/spinner.gif" style="display:none"/>
						 
					</div>
<input class="validation_button" id="validate_uri" name="validate_uri" onclick="return AChecker.input.validateURI();" size="100" type="submit" value="Check It"/>
</div>
</div>
</div>
<div class="input_tab" id="AC_by_upload" style="display:none">
<div style="text-align:center;">
<label for="checkfile">File:</label>
<input name="MAX_FILE_SIZE" type="hidden" value="52428800"/>
<input id="checkfile" name="uploadfile" size="47" type="file"/>
<div class="validation_submit_div">
<div class="spinner_div">
<img alt="Accessibility Validation in Progress" class="spinner_img" id="AC_spinner_by_upload" src="https://achecker.ca/themes/default/images/spinner.gif" style="display:none"/>
						 
					</div>
<input class="validation_button" id="validate_file" name="validate_file" onclick="return AChecker.input.validateUpload();" type="submit" value="Check It"/>
</div>
</div>
</div>
<div class="input_tab" id="AC_by_paste" style="display:none">
<label for="checkpaste">Paste from clipboard complete HTML source:</label>
<div style="text-align:center;">
<textarea cols="75" id="checkpaste" name="pastehtml" rows="20"></textarea>
<div class="validation_submit_div">
<div class="spinner_div">
<img alt="Accessibility Validation in Progress" class="spinner_img" id="AC_spinner_by_paste" src="https://achecker.ca/themes/default/images/spinner.gif" style="display:none"/>
						 
					</div>
<input class="validation_button" id="validate_paste" name="validate_paste" onclick="return AChecker.input.validatePaste();" type="submit" value="Check It"/>
</div>
</div>
</div>
<div>
<h2 align="left">
<img alt="Expand Guidelines" border="0" id="toggle_image" src="images/arrow-closed.png" title="Expand Guidelines"/>
<a href="javascript:AChecker.toggleDiv('div_options', 'toggle_image');">Options</a>
</h2>
</div>
<div id="div_options" style="display:none">
<table class="data static" style="background-colour:#eeeeee;">
<tr>
<td class="one_third_width">
<input id="enable_html_validation" name="enable_html_validation" type="checkbox" value="1"/>
<label for="enable_html_validation">Enable HTML Validator</label>
</td>
<td class="one_third_width">
<input id="enable_css_validation" name="enable_css_validation" type="checkbox" value="1"/>
<label for="enable_css_validation">Enable CSS Validator</label>
</td>
<td class="one_third_width">
<input id="show_source" name="show_source" type="checkbox" value="1"/>
<label for="show_source">Show Source</label>
</td>
</tr>
<tr>
<td colspan="3"><h3>Guidelines to Check Against</h3></td>
</tr>
<!-- 
			<tr>
				<td>
					<input type="checkbox" name="gid[]" id='gid_1' value='1'  />
					<label for='gid_1'>BITV 1.0 (Level 2)</label>
				</td>
				<td>
					<input type="checkbox" name="gid[]" id='gid_2' value='2'  />
					<label for='gid_2'>Section 508</label>
				</td>
				<td>
					<input type="checkbox" name="gid[]" id='gid_3' value='3'  />
					<label for='gid_3'>Stanca Act</label>
				</td>
			</tr>
			<tr>
				<td>
					<input type="checkbox" name="gid[]" id='gid_4' value='4'  />
					<label for='gid_4'>WCAG 1.0 (Level A)</label>
				</td>
				<td>
					<input type="checkbox" name="gid[]" id='gid_5' value='5'  />
					<label for='gid_5'>WCAG 1.0 (Level AA)</label>
				</td>
				<td>
					<input type="checkbox" name="gid[]" id='gid_6' value='6'  />
					<label for='gid_6'>WCAG 1.0 (Level AAA)</label>
				</td>
			</tr>
			<tr>
				<td>
					<input type="checkbox" name="gid[]" id='gid_7' value='7'  />
					<label for='gid_7'>WCAG 2.0 (Level A)</label>
				</td>
				<td>
					<input type="checkbox" name="gid[]" id='gid_8' value='8'  />
					<label for='gid_8'>WCAG 2.0 (Level AA)</label>
				</td>
				<td>
					<input type="checkbox" name="gid[]" id='gid_9' value='9'  />
					<label for='gid_9'>WCAG 2.0 (Level AAA)</label>
				</td>
			</tr>
 -->
<tr>
<td colspan="3">
<div id="guideline_in_radio">
<table width="100%">
<tr>
<td class="one_third_width">
<input id="radio_gid_1" name="radio_gid[]" type="radio" value="1"/>
<label for="radio_gid_1">BITV 1.0 (Level 2)</label>
</td>
<td class="one_third_width">
<input id="radio_gid_2" name="radio_gid[]" type="radio" value="2"/>
<label for="radio_gid_2">Section 508</label>
</td>
<td class="one_third_width">
<input id="radio_gid_3" name="radio_gid[]" type="radio" value="3"/>
<label for="radio_gid_3">Stanca Act</label>
</td>
</tr>
<tr>
<td class="one_third_width">
<input id="radio_gid_4" name="radio_gid[]" type="radio" value="4"/>
<label for="radio_gid_4">WCAG 1.0 (Level A)</label>
</td>
<td class="one_third_width">
<input id="radio_gid_5" name="radio_gid[]" type="radio" value="5"/>
<label for="radio_gid_5">WCAG 1.0 (Level AA)</label>
</td>
<td class="one_third_width">
<input id="radio_gid_6" name="radio_gid[]" type="radio" value="6"/>
<label for="radio_gid_6">WCAG 1.0 (Level AAA)</label>
</td>
</tr>
<tr>
<td class="one_third_width">
<input id="radio_gid_7" name="radio_gid[]" type="radio" value="7"/>
<label for="radio_gid_7">WCAG 2.0 (Level A)</label>
</td>
<td class="one_third_width">
<input checked="checked" id="radio_gid_8" name="radio_gid[]" type="radio" value="8"/>
<label for="radio_gid_8">WCAG 2.0 (Level AA)</label>
</td>
<td class="one_third_width">
<input id="radio_gid_9" name="radio_gid[]" type="radio" value="9"/>
<label for="radio_gid_9">WCAG 2.0 (Level AAA)</label>
</td>
</tr>
</table>
</div>
<div id="guideline_in_checkbox" style="display:none">
<table width="100%">
<tr>
<td class="one_third_width">
<input id="checkbox_gid_1" name="checkbox_gid[]" type="checkbox" value="1"/>
<label for="checkbox_gid_1">BITV 1.0 (Level 2)</label>
</td>
<td class="one_third_width">
<input id="checkbox_gid_2" name="checkbox_gid[]" type="checkbox" value="2"/>
<label for="checkbox_gid_2">Section 508</label>
</td>
<td class="one_third_width">
<input id="checkbox_gid_3" name="checkbox_gid[]" type="checkbox" value="3"/>
<label for="checkbox_gid_3">Stanca Act</label>
</td>
</tr>
<tr>
<td class="one_third_width">
<input id="checkbox_gid_4" name="checkbox_gid[]" type="checkbox" value="4"/>
<label for="checkbox_gid_4">WCAG 1.0 (Level A)</label>
</td>
<td class="one_third_width">
<input id="checkbox_gid_5" name="checkbox_gid[]" type="checkbox" value="5"/>
<label for="checkbox_gid_5">WCAG 1.0 (Level AA)</label>
</td>
<td class="one_third_width">
<input id="checkbox_gid_6" name="checkbox_gid[]" type="checkbox" value="6"/>
<label for="checkbox_gid_6">WCAG 1.0 (Level AAA)</label>
</td>
</tr>
<tr>
<td class="one_third_width">
<input id="checkbox_gid_7" name="checkbox_gid[]" type="checkbox" value="7"/>
<label for="checkbox_gid_7">WCAG 2.0 (Level A)</label>
</td>
<td class="one_third_width">
<input checked="checked" id="checkbox_gid_8" name="checkbox_gid[]" type="checkbox" value="8"/>
<label for="checkbox_gid_8">WCAG 2.0 (Level AA)</label>
</td>
<td class="one_third_width">
<input id="checkbox_gid_9" name="checkbox_gid[]" type="checkbox" value="9"/>
<label for="checkbox_gid_9">WCAG 2.0 (Level AAA)</label>
</td>
</tr>
</table>
</div>
</td>
</tr>
<tr>
<td colspan="3"><h3>Report Format</h3></td>
</tr>
<tr>
<td class="one_third_width"><input checked="checked" id="option_rpt_gdl" name="rpt_format" type="radio" value="1"/><label for="option_rpt_gdl">View by Guideline</label></td>
<td class="one_third_width"><input id="option_rpt_line" name="rpt_format" type="radio" value="2"/><label for="option_rpt_line">View by Line Number</label></td>
</tr>
</table>
</div>
</fieldset>
</div>
</form>
<div style="float:right;margin-right:2em;clear:right;width:250px;"><br/>
<a href="checker/index.php#skipads"><img alt="Jump past advertisements" border="0" src="images/clr.gif"/></a>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="pub-8538177464726172" data-ad-slot="0783349774" style="display:inline-block;width:250px;height:250px"></ins>
<script>
    (adsbygoogle = window.adsbygoogle || []).push({});
  </script>
<a name="skipads" title="passed ads"></a>
</div>
</div>
</td>
</tr>
</table>
<div class="validator-output-form" id="output_div"><p><strong>Welcome to AChecker</strong>. This tool checks single HTML pages for conformance with accessibility standards to ensure the content can be accessed by everyone. See the Handbook link to the upper right for more about the Web Accessibility Checker.</p></div>
<div align="center" id="lang" style="clear: both;"><br/>
<small><label for="lang">Translate to </label></small><strong>English</strong> | <a href="/checker/index.php?lang=deu">German</a>  | <a href="/checker/index.php?lang=ita">Italiano</a> </div><br/><br/>
<div style="margin-left:auto; margin-right:auto; width:20em;">
<small>Web site engine's code is copyright © 2011</small><br/>
</div>
</div> <!--  end center-content div -->
<div class="bottom"><span></span></div><!--  bottom for liquid-round theme -->
</div> <!-- end liquid-round div -->
<div style="margin-left:auto; margin-right:auto; width:56px;">
<a href="http://inclusivedesign.ca/"><img alt="Inclusive Design Institute" height="73" src="https://achecker.ca/themes/default/images/IDI.png" style="border:none;" title="Inclusive Design Institute" width="56"/></a>
</div>
<script language="javascript" type="text/javascript">
//<!--
var selected;
function rowselect(obj) {
	obj.className = 'selected';
	if (selected && selected != obj.id)
		document.getElementById(selected).className = '';
	selected = obj.id;
}
function rowselectbox(obj, checked, handler) {
	var functionDemo = new Function(handler + ";");
	functionDemo();

	if (checked)
		obj.className = 'selected';
	else
		obj.className = '';
}
//-->
</script>
<div align="center" style="clear:both;margin-left:auto; width:15em;margin-right:auto;">
<a href="documentation/web_service_api.php" target="_new" title="Web Service API">Web Service API</a>
</div>
</body>
</html>
