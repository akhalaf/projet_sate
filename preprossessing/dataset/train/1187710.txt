<!DOCTYPE html>
<html lang="en">
<head>
<meta content="IE=Edge" http-equiv="X-UA-Compatible"/>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>404</title>
<meta content="" name="description"/>
<meta content="404" property="og:title"/>
<meta content="" property="og:description"/>
<meta content="" property="og:url"/>
<meta content="" property="og:image"/>
<style type="text/css">.pos-fixed{position:fixed}.pos-relative{position:relative}.pos-absolute{position:absolute}@media (min-width:65em){.pos-relative--l{position:relative}}.js .js-pos-fixed{position:fixed}.js .js-pos-relative{position:relative}.z-1{z-index:1}.z-2{z-index:2}.z-3{z-index:3}.z-4{z-index:4}.top-0{top:0}.left-0{left:0}.flo-left{float:left}.vis-hidden{visibility:hidden}.d-none{display:none}.d-block{display:block}.d-i-block{display:inline-block}.d-flex{display:-webkit-box;display:-ms-flexbox;display:flex}.d-t-cell{display:table-cell}@media (min-width:37.5em){.d-block--m{display:block}.d-i-block--m{display:inline-block}.d-flex--m{display:-webkit-box;display:-ms-flexbox;display:flex}}@media (min-width:65em){.d-none--l{display:none}.d-block--l{display:block}.d-flex--l{display:-webkit-box;display:-ms-flexbox;display:flex}}.js .js-d-none{display:none}.flx-dir-row{-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row}.flx-dir-row,.flx-dir-row-r{-webkit-box-orient:horizontal}.flx-dir-row-r{-webkit-box-direction:reverse;-ms-flex-direction:row-reverse;flex-direction:row-reverse}@media (min-width:37.5em){.flx-dir-row-r--m{-webkit-box-orient:horizontal;-webkit-box-direction:reverse;-ms-flex-direction:row-reverse;flex-direction:row-reverse}}.flx-wp-wrap{-ms-flex-wrap:wrap;flex-wrap:wrap}@media (min-width:65em){.flx-wp-wrap--l{-ms-flex-wrap:wrap;flex-wrap:wrap}}.flx-jc-s-between{-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between}@media (min-width:65em){.flx-jc-end--l{-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end}}.flx-ai-center{-webkit-box-align:center;-ms-flex-align:center;align-items:center}.flx-ac-s-between{-ms-flex-line-pack:justify;align-content:space-between}.wp-m{max-width:48em}.wp-l{max-width:85.375em}.w-50{width:50%}.w-100{width:100%}.w-100vw{width:100vw}@media (min-width:37.5em){.w-100--m{width:100%}}.h-100{height:100%}.h-100vh{height:100vh}.h-100vh-min{min-height:100vh}.m-top-2{margin-top:.5em}.m-top-3{margin-top:1em}.m-top-4{margin-top:2em}.m-top-5{margin-top:4em}.m-right-1{margin-right:.25em}.m-right-4{margin-right:2em}.m-bottom-3{margin-bottom:1em}.m-left-4{margin-left:2em}@media (min-width:37.5em){.m-top-n3--m{margin-top:-1em}.m-top-0--m{margin-top:0}.m-top-2--m{margin-top:.5em}.m-bottom-0--m{margin-bottom:0}.m-left-4--m{margin-left:2em}}@media (min-width:65em){.m-top-n4--l{margin-top:-2em}.m-top-0--l{margin-top:0}.m-top-3--l{margin-top:1em}.m-left-n4--l{margin-left:-2em}}.p-ver-1{padding-top:.25em;padding-bottom:.25em}.p-ver-2{padding-top:.5em;padding-bottom:.5em}.p-ver-4{padding-top:2em;padding-bottom:2em}.p-hor-3{padding-right:1em;padding-left:1em}.p-top-3{padding-top:1em}.p-top-4{padding-top:2em}.p-right-2{padding-right:.5em}.p-right-3{padding-right:1em}.p-right-4{padding-right:2em}.p-right-5{padding-right:4em}.p-bottom-3{padding-bottom:1em}.p-bottom-4{padding-bottom:2em}.p-bottom-5{padding-bottom:4em}.p-left-2{padding-left:.5em}.p-left-3{padding-left:1em}.p-left-4{padding-left:2em}@media (min-width:37.5em){.p-ver-5--m{padding-top:4em;padding-bottom:4em}.p-hor-4--m{padding-right:2em;padding-left:2em}.p-top-4--m{padding-top:2em}.p-right-0--m{padding-right:0}.p-right-3--m{padding-right:1em}.p-right-4--m{padding-right:2em}.p-bottom-0--m{padding-bottom:0}.p-bottom-3--m{padding-bottom:1em}.p-bottom-5--m{padding-bottom:4em}.p-left-0--m{padding-left:0}.p-left-3--m{padding-left:1em}.p-left-4--m{padding-left:2em}}@media (min-width:65em){.p-hor-5--l{padding-right:4em;padding-left:4em}.p-top-0--l{padding-top:0}.p-top-3--l{padding-top:1em}.p-top-4--l{padding-top:2em}.p-right-0--l{padding-right:0}.p-right-4--l{padding-right:2em}.p-left-0--l{padding-left:0}.p-left-4--l{padding-left:2em}}.va-middle{vertical-align:middle}.ha-center{margin-right:auto;margin-left:auto}@media (min-width:37.5em){.ha-right--m{margin-left:auto;margin-right:0}}.g12-6{max-width:50%;-ms-flex-preferred-size:50%;flex-basis:50%}.g12-12{max-width:100%;-ms-flex-preferred-size:100%;flex-basis:100%}@media (min-width:37.5em){.g12-4--m{max-width:33.3333333333%;-ms-flex-preferred-size:33.3333333333%;flex-basis:33.3333333333%}.g12-8--m{max-width:66.6666666667%;-ms-flex-preferred-size:66.6666666667%;flex-basis:66.6666666667%}}@media (min-width:65em){.g12-3--l{max-width:25%;-ms-flex-preferred-size:25%;flex-basis:25%}.g12-6--l{max-width:50%;-ms-flex-preferred-size:50%;flex-basis:50%}.g12-9--l{max-width:75%;-ms-flex-preferred-size:75%;flex-basis:75%}.g12-12--l{max-width:100%;-ms-flex-preferred-size:100%;flex-basis:100%}}.of-hidden{overflow:hidden}.of-auto{overflow:auto}.js .js-of-hidden{overflow:hidden}.pe-none{pointer-events:none}.pe-auto{pointer-events:auto}.f-f-open-sans{font-family:open sans,helvetica,sans-serif}.f-s-4{font-size:1.15em}.f-s-6{font-size:.875em}.f-s-7{font-size:.75em}@media (min-width:37.5em){.f-s-3--m{font-size:1.3em}.f-s-5--m{font-size:1em}.f-s-6--m{font-size:.875em}}.f-w-7{font-weight:700}.f-r-smooth{-webkit-font-smoothing:antialiased}.l-m-base{max-width:30em}.l-m-narrow{max-width:20em}.l-m-wide{max-width:34em}.l-h-copy{line-height:1.35}.l-h-title{line-height:1.25}.l-h-solid{line-height:1}.l-t-base{letter-spacing:.05em}.t-a-right{text-align:right}@media (min-width:37.5em){.t-a-left--m{text-align:left}.t-a-right--m{text-align:right}}.t-t-upper{text-transform:uppercase}.t-d-none{text-decoration:none}.c-red{color:#c61821}.c-near-black{color:#222}.c-mid-grey{color:#666}.c-moon-grey{color:#ddd}.c-white{color:#fff}.bg-c-dark-red{background-color:#9e0e14}.bg-c-black{background-color:#000}.bg-c-near-white{background-color:#f7f7f7}.bg-c-white{background-color:#fff}.b-all{border:1px solid}.b-top{border-top-width:1px;border-top-style:solid;border-top:1px solid}.b-right{border-right-width:1px;border-right-style:solid;border-right:1px solid}.b-bottom{border-bottom-width:1px;border-bottom-style:solid;border-bottom:1px solid}@media (min-width:37.5em){.b-left--m{border-left-width:1px;border-left-style:solid;border-left:1px solid}}.b-w-1{border-width:1px}@media (min-width:37.5em){.b-w-1--m{border-width:1px}.b-w-right-0--m{border-right-width:0}.b-w-bottom-0--m{border-bottom-width:0}}@media (min-width:65em){.b-w-right-0--l{border-right-width:0}}.b-c-red{border-color:#c61821}.b-c-moon-grey{border-color:#ddd}.b-c-white{border-color:#fff}@media (min-width:37.5em){.b-c-red--m{border-color:#c61821}.b-c-moon-grey--m{border-color:#ddd}}.b-r-2{border-top-left-radius:.25em;border-top-right-radius:.25em;border-bottom-right-radius:.25em;border-bottom-left-radius:.25em;border-radius:.25em}.fil-near-black{fill:#222}.fil-white{fill:#fff}.tx-1{transition:.15s;-webkit-transition:.15s;-webkit-transition-duration:.15s;transition-duration:.15s}html{-webkit-box-sizing:border-box;box-sizing:border-box}*{margin:0;padding:0;border:0;outline:0;vertical-align:baseline;background-color:transparent;-webkit-box-sizing:inherit;box-sizing:inherit;background-position:initial initial;background-repeat:initial initial}*,body{font-size:100%}body{line-height:1}div,footer,header,main,nav,section{display:block}a{font-size:100%;vertical-align:baseline;background-color:transparent;background-position:initial initial;background-repeat:initial initial;margin:0;padding:0;background:transparent}button{cursor:pointer;font-family:inherit;font-size:inherit;font-style:inherit;font-variant:inherit;font-weight:inherit;line-height:inherit;-webkit-appearance:none;-moz-appearance:none;appearance:none;margin:0;font:inherit}.smooth-touch-scroll{-webkit-overflow-scrolling:touch}.icon{width:1.5em;height:1.5em}.whirligig{width:39px;height:39px}.whirligig__side{width:33.3333333333%;height:33.3333333333%;-webkit-animation:grid-scale-delay 1.3s infinite ease-in-out;animation:grid-scale-delay 1.3s infinite ease-in-out}.whirligig__side:nth-child(4),.whirligig__side:nth-child(8){-webkit-animation-delay:.1s;animation-delay:.1s}.whirligig__side:first-child,.whirligig__side:nth-child(5),.whirligig__side:nth-child(9){-webkit-animation-delay:.2s;animation-delay:.2s}.whirligig__side:nth-child(2),.whirligig__side:nth-child(6){-webkit-animation-delay:.3s;animation-delay:.3s}.whirligig__side:nth-child(3){-webkit-animation-delay:.4s;animation-delay:.4s}.list{list-style:none}.logo{padding-bottom:16.6666666667%}.text-area-edit p{margin-top:1em}.text-area-edit p:first-child{margin-top:0}.text-article-edit p:not(:last-child){margin-bottom:1em}.text-article-edit li{display:table;width:100%}.text-article-edit li:before{display:table-cell;vertical-align:baseline;width:2em;font-size:.875em}.text-article-edit li:not(:last-child){margin-bottom:.5em}.text-article-edit ol{counter-reset:a 0;counter-reset:a}.text-article-edit ol li{counter-increment:a 1;counter-increment:a}.text-article-edit ol li:before{content:counter(a,decimal),".";content:counter(a) "."}.text-article-edit a{transition:color .15s;-webkit-transition:color .15s;color:#c61821}.js .loader__wrap--pre .loader{display:table;visibility:visible}.nav__a{text-decoration:none}.nav-mobile-menu__engage{-ms-flex-negative:0;flex-shrink:0}.nav-mobile-menu__engage,.nav-mobile-menu__engage-desc{-webkit-box-flex:0;-ms-flex-positive:0;flex-grow:0}.header-head__push{padding-top:2.5em}@media (min-width:37.5em){.header-head__push{padding-top:5.5em}}@media (min-width:65em){.header-head__push{padding-top:0}}.header-head__branding,.header-head__menu-btn,.header-head__nav{-ms-flex-negative:0;flex-shrink:0;-webkit-box-flex:0;-ms-flex-positive:0;flex-grow:0}.header_head__logo{width:9em}@media (min-width:37.5em){.header_head__logo{width:15em}}.footer-foot__illus-facade{width:9.6875em;height:4.5em;background-image:url(../../images/build/facade-small-light-silver.svg),-webkit-gradient(linear,left top,left bottom,from(transparent),to(transparent));background-image:url(../../images/build/facade-small-light-silver.svg),linear-gradient(transparent,transparent);background-size:9.6875em 4.5em;background-image:url(../../images/build/facade-small-light-silver.png);background-repeat:no-repeat}@media (min-width:65em){.footer-foot__illus-facade{width:19.125em;height:8.75em;background-image:url(../../images/build/facade-large-light-silver.svg),-webkit-gradient(linear,left top,left bottom,from(transparent),to(transparent));background-image:url(../../images/build/facade-large-light-silver.svg),linear-gradient(transparent,transparent);background-size:19.125em 8.75em;background-image:url(../../images/build/facade-large-light-silver.png)}}.footer-foot__illus-burning-tree{width:3.6875em;height:3.6875em;background-image:url(../../images/build/burning-tree-small.svg),-webkit-gradient(linear,left top,left bottom,from(transparent),to(transparent));background-image:url(../../images/build/burning-tree-small.svg),linear-gradient(transparent,transparent);background-size:3.6875em;background-image:url(../../images/build/burning-tree-small.png);background-size:3.6875em 3.6875em;background-repeat:no-repeat}@media (min-width:65em){.footer-foot__illus-burning-tree{width:8.375em;height:8.375em;background-image:url(../../images/build/burning-tree-large.svg),-webkit-gradient(linear,left top,left bottom,from(transparent),to(transparent));background-image:url(../../images/build/burning-tree-large.svg),linear-gradient(transparent,transparent);background-size:8.375em;background-image:url(../../images/build/burning-tree-large.png);background-size:8.375em 8.375em}}</style> <noscript id="deferred-styles">
<link href="https://www.pspc.org.sg/assets/stylesheets/build/style.css" rel="stylesheet"/> </noscript>
<script>
      var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById('deferred-styles');
        var replacement = document.createElement('div');
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement);
        addStylesNode.parentElement.removeChild(addStylesNode);
      };
      var raf = requestAnimationFrame || mozRequestAnimationFrame ||
          webkitRequestAnimationFrame || msRequestAnimationFrame;
      if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
      else window.addEventListener('load', loadDeferredStyles);
    </script>
<script>
      // Add class hook so UI dependent on javascript can be presented accordingly.
      if (document.documentElement.classList.length) {
        document.documentElement.className += ' js';
      } else {
        document.documentElement.className = ' js';
      }
      // <picture> element shiv.
      document.createElement('picture');
    </script>
<link href="assets/favicons/build/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="assets/favicons/build/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="assets/favicons/build/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="assets/favicons/build/site.webmanifest" rel="manifest"/>
<link color="#c61821" href="assets/favicons/build/safari-pinned-tab.svg" rel="mask-icon"/>
<link href="assets/favicons/build/favicon.ico" rel="shortcut icon"/>
<meta content="#ffffff" name="msapplication-TileColor"/>
<meta content="assets/favicons/build/mstile-144x144.png" name="msapplication-TileImage"/>
<meta content="assets/favicons/build/browserconfig.xml" name="msapplication-config"/>
<meta content="#ffffff" name="theme-color"/> </head>
<body class=" js-pos-relative js-of-hidden f-f-open-sans l-h-copy c-near-black">
<div class=" pos-fixed z-2 w-100vw h-100vh pe-none loader__wrap--pre" id="pre-loader">
<div class=" pos-relative d-none w-100 h-100 vis-hidden pe-auto bg-c-white loader">
<div class="d-t-cell va-middle">
<div class=" ha-center of-hidden whirligig">
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
</div>
</div>
</div> </div>
<div class=" pos-relative z-1 d-flex flx-dir-row flx-wp-wrap flx-ac-s-between h-100vh-min bg-c-near-white">
<div class=" pos-relative z-2 g12-12">
<header class=" pos-fixed pos-relative--l z-4 top-0 left-0 w-100">
<div class="bg-c-dark-red">
<div class="wp-l ha-center">
<div class=" d-flex flx-ai-center flx-jc-s-between p-ver-2 p-top-4--m p-bottom-3--m p-hor-3 p-hor-4--m p-hor-5--l">
<div class=" header-head__branding p-right-5 p-right-4--l">
<a class=" t-d-none fil-white hov-fil-moon-grey tx-1" href="https://www.pspc.org.sg">
<div class=" logo header_head__logo pos-relative z-1">
<div class=" pos-absolute z-1 top-0 left-0 w-100 h-100">
<svg class="d-block w-100 h-100 pe-none">
<use xlink:href="/assets/images/build/logo.svg#logo"></use>
</svg>
</div>
</div>
</a>
</div>
<button aria-controls="menu" aria-expanded="false" class=" collapsible__button menu-button header-head__menu-btn d-none--l fil-white hov-fil-moon-grey tx-1">
<svg class=" icon d-block">
<use xlink:href="/assets/symbols/build/symbolsheet.svg#menu-bar"></use>
</svg>
</button>
<nav class=" header-head__nav d-none d-block--l p-left-4--l">
<div class=" d-flex flx-ai-center flx-jc-s-between m-bottom-3">
<div class="p-right-4">
<ul>
<li class=" d-i-block">
<a class=" nav__a nav-head__a c-white hov-c-moon-grey tx-1" data-controls-uid="home" href="https://www.pspc.org.sg">
<span class=" nav__a-label f-s-6 f-r-smooth t-t-upper l-t-base l-h-solid">
                Home              </span>
</a>
</li>
<li class=" m-left-4 d-i-block">
<a class=" nav__a nav-head__a c-white hov-c-moon-grey tx-1" data-controls-uid="about" href="https://www.pspc.org.sg/about">
<span class=" nav__a-label f-s-6 f-r-smooth t-t-upper l-t-base l-h-solid">
                About              </span>
</a>
</li>
<li class=" m-left-4 d-i-block">
<a class=" nav__a nav-head__a c-white hov-c-moon-grey tx-1" data-controls-uid="contact" href="https://www.pspc.org.sg/contact">
<span class=" nav__a-label f-s-6 f-r-smooth t-t-upper l-t-base l-h-solid">
                Contact              </span>
</a>
</li>
</ul>
</div>
<div class="p-left-4">
<a class=" nav__a nav-head__engage-a d-i-block p-ver-1 p-hor-3 b-all b-w-1 b-c-white hov-b-c-moon-grey b-r-2 c-white hov-c-moon-grey tx-1" data-controls-uid="new" href="https://www.pspc.org.sg/new">
<span class=" nav__a-label f-s-6 f-w-7 f-r-smooth t-t-upper l-t-base l-h-solid">
          I'm New        </span>
</a>
</div>
</div>
<div>
<ul>
<li class=" d-i-block">
<a class=" nav__a nav-head__a c-white hov-c-moon-grey tx-1" data-controls-uid="services" href="https://www.pspc.org.sg/services">
<span class=" nav__a-label f-s-6 f-w-7 f-r-smooth t-t-upper l-t-base l-h-solid">
              Services            </span>
</a>
</li>
<li class=" m-left-4 d-i-block">
<a class=" nav__a nav-head__a c-white hov-c-moon-grey tx-1" data-controls-uid="ministries" href="https://www.pspc.org.sg/ministries">
<span class=" nav__a-label f-s-6 f-w-7 f-r-smooth t-t-upper l-t-base l-h-solid">
              Ministries            </span>
</a>
</li>
<li class=" m-left-4 d-i-block">
<a class=" nav__a nav-head__a c-white hov-c-moon-grey tx-1" data-controls-uid="audio-sermons" href="https://www.pspc.org.sg/audio-sermons">
<span class=" nav__a-label f-s-6 f-w-7 f-r-smooth t-t-upper l-t-base l-h-solid">
              Audio Sermons            </span>
</a>
</li>
<li class=" m-left-4 d-i-block">
<a class=" nav__a nav-head__a c-white hov-c-moon-grey tx-1" data-controls-uid="events" href="https://www.pspc.org.sg/events">
<span class=" nav__a-label f-s-6 f-w-7 f-r-smooth t-t-upper l-t-base l-h-solid">
              Happenings            </span>
</a>
</li>
</ul>
</div>
</nav>
</div>
</div>
</div>
</header>
<div aria-hidden="true" class=" js-pos-fixed z-3 top-0 left-0 js-d-none w-100 h-100 of-hidden" id="menu">
<div class=" smooth-touch-scroll header-head__push pos-relative h-100vh of-auto bg-c-dark-red c-white-blue">
<div class=" h-100 p-hor-3 p-hor-4--m p-top-4 p-bottom-5 b-top b-w-1 b-c-red">
<div class="d-flex--m flx-dir-row-r--m h-100">
<div class=" g12-4--m m-bottom-3 m-bottom-0--m p-bottom-3 p-bottom-0--m p-left-3--m b-bottom b-w-bottom-0--m b-w-1 b-c-red">
<div class=" d-flex d-block--m flx-dir-row-r flx-ai-center flx-jc-s-between d-block--m h-100 p-left-3--m b-left--m b-w-1--m b-c-red--m">
<div class=" nav-mobile-menu__engage p-left-2 p-left-0--m t-a-right">
<a class=" nav__a menu-nav__a nav-mobile-menu__engage-a d-i-block p-ver-1 p-hor-3 b-all b-w-1 b-c-white hov-b-c-moon-grey b-r-2 c-white hov-c-moon-grey tx-1" data-controls-uid="new" href="https://www.pspc.org.sg/new">
<span class=" nav__a-label f-s-6 f-w-7 f-r-smooth t-t-upper l-t-base l-h-solid">
                  I'm New                </span>
</a>
</div>
</div>
</div>
<div class="g12-8--m p-right-3--m">
<ul class=" list t-a-right t-a-left--m">
<li class=" ">
<div>
<a class=" menu-nav__a nav__a nav-mobile-menu__a f-r-smooth t-d-none c-white hov-c-silver tx-1" data-controls-uid="services" href="https://www.pspc.org.sg/services">
<span class="f-w-7 l-h-solid">
                      Services                    </span>
</a>
</div>
<ul class=" list m-top-2">
<li class=" ">
<a class=" menu-nav__a nav__a nav-mobile-menu__a f-r-smooth t-d-none c-moon-grey hov-c-silver tx-1" data-controls-uid="english-service" href="https://www.pspc.org.sg/services/english-service">
<span class="f-s-6 f-s-5--m l-h-solid">
                            English Service                          </span>
</a>
</li>
<li class=" m-top-2">
<a class=" menu-nav__a nav__a nav-mobile-menu__a f-r-smooth t-d-none c-moon-grey hov-c-silver tx-1" data-controls-uid="mandarin-service" href="https://www.pspc.org.sg/services/mandarin-service">
<span class="f-s-6 f-s-5--m l-h-solid">
                            Mandarin Service<br/>中文崇拜                          </span>
</a>
</li>
<li class=" m-top-2">
<a class=" menu-nav__a nav__a nav-mobile-menu__a f-r-smooth t-d-none c-moon-grey hov-c-silver tx-1" data-controls-uid="filipino-service" href="https://www.pspc.org.sg/services/filipino-service">
<span class="f-s-6 f-s-5--m l-h-solid">
                            Filipino Service                          </span>
</a>
</li>
<li class=" m-top-2">
<a class=" menu-nav__a nav__a nav-mobile-menu__a f-r-smooth t-d-none c-moon-grey hov-c-silver tx-1" data-controls-uid="others" href="https://www.pspc.org.sg/services/others">
<span class="f-s-6 f-s-5--m l-h-solid">
                            Mizo Service                          </span>
</a>
</li>
</ul>
</li>
<li class=" m-top-3">
<div>
<a class=" menu-nav__a nav__a nav-mobile-menu__a f-r-smooth t-d-none c-white hov-c-silver tx-1" data-controls-uid="ministries" href="https://www.pspc.org.sg/ministries">
<span class="f-w-7 l-h-solid">
                      Ministries                    </span>
</a>
</div>
<ul class=" list m-top-2">
<li class=" ">
<a class=" menu-nav__a nav__a nav-mobile-menu__a f-r-smooth t-d-none c-moon-grey hov-c-silver tx-1" data-controls-uid="explore-christianity" href="https://www.pspc.org.sg/ministries/explore-christianity">
<span class="f-s-6 f-s-5--m l-h-solid">
                            Explore Christianity                          </span>
</a>
</li>
<li class=" m-top-2">
<a class=" menu-nav__a nav__a nav-mobile-menu__a f-r-smooth t-d-none c-moon-grey hov-c-silver tx-1" data-controls-uid="get-connected" href="https://www.pspc.org.sg/ministries/get-connected">
<span class="f-s-6 f-s-5--m l-h-solid">
                            Get Connected                          </span>
</a>
</li>
<li class=" m-top-2">
<a class=" menu-nav__a nav__a nav-mobile-menu__a f-r-smooth t-d-none c-moon-grey hov-c-silver tx-1" data-controls-uid="arts-and-music" href="https://www.pspc.org.sg/ministries/arts-and-music">
<span class="f-s-6 f-s-5--m l-h-solid">
                            Arts and Music                          </span>
</a>
</li>
<li class=" m-top-2">
<a class=" menu-nav__a nav__a nav-mobile-menu__a f-r-smooth t-d-none c-moon-grey hov-c-silver tx-1" data-controls-uid="community-engagement" href="https://www.pspc.org.sg/ministries/community-engagement">
<span class="f-s-6 f-s-5--m l-h-solid">
                            Community Engagement                          </span>
</a>
</li>
</ul>
</li>
<li class=" m-top-3">
<div>
<a class=" menu-nav__a nav__a nav-mobile-menu__a f-r-smooth t-d-none c-white hov-c-silver tx-1" data-controls-uid="audio-sermons" href="https://www.pspc.org.sg/audio-sermons">
<span class="f-w-7 l-h-solid">
                      Audio Sermons                    </span>
</a>
</div>
</li>
</ul>
<div class=" m-top-3 p-top-3 b-top b-w-1 b-c-red">
<ul class=" list t-a-right t-a-left--m">
<li class=" ">
<a class=" menu-nav__a nav__a nav-mobile-menu__a t-d-none f-r-smooth t-d-none c-white hov-c-silver tx-1" data-controls-uid="events" href="https://www.pspc.org.sg/events">
<span class="f-w-7 l-h-solid">
                      Happenings                    </span>
</a>
</li>
<li class=" m-top-3">
<a class=" menu-nav__a nav__a nav-mobile-menu__a t-d-none f-r-smooth t-d-none c-white hov-c-silver tx-1" data-controls-uid="contact" href="https://www.pspc.org.sg/contact">
<span class="f-w-7 l-h-solid">
                      Contact                    </span>
</a>
</li>
<li class=" m-top-3">
<a class=" menu-nav__a nav__a nav-mobile-menu__a t-d-none f-r-smooth t-d-none c-white hov-c-silver tx-1" data-controls-uid="about" href="https://www.pspc.org.sg/about">
<span class="f-w-7 l-h-solid">
                      About                    </span>
</a>
</li>
<li class=" m-top-3">
<a class=" menu-nav__a nav__a nav-mobile-menu__a t-d-none f-r-smooth t-d-none c-white hov-c-silver tx-1" data-controls-uid="home" href="https://www.pspc.org.sg">
<span class="f-w-7 l-h-solid">
                      Home                    </span>
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
<div class=" pos-fixed z-2 w-100vw h-100vh pe-none" id="view-loader">
<div class=" pos-relative d-none w-100 h-100 vis-hidden pe-auto bg-c-white loader">
<div class="d-t-cell va-middle">
<div class=" ha-center of-hidden whirligig">
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
<div class=" flo-left bg-c-black whirligig__side"></div>
</div>
</div>
</div> </div>
<div class=" header-head__push pos-relative z-1" id="mainframe-wp">
<main class="mainframe" data-template="error" data-uid='{"current":"error"}'>
<div class=" p-bottom-4 p-bottom-5--m bg-c-white">
<div class="wp-m ha-center">
<div class="p-ver-4 p-ver-5--m p-hor-3 p-hor-4--m p-hor-5--l">
<header class="p-bottom-4 p-bottom-5--m">
<h1 class=" l-h-title c-red">
  404</h1>
<div class="m-top-4">
<h2 class="f-s-4 f-s-3--m f-w-7 l-h-title l-m-base">
        This page cannot be found.      </h2>
<div class="m-top-3">
<div class=" text-area-edit f-s-4 l-m-wide">
<p>The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</p> </div>
</div>
</div>
</header>
<section class=" p-ver-4 p-ver-5--m b-top b-w-1 b-c-moon-grey">
<div class=" text-article-edit l-m-wide">
<p>Please try the following:</p>
<ol>
<li>If you typed the page address in the Address bar, make sure that it is spelled correctly.</li>
<li>Open the <a href="https://www.pspc.org.sg">home page</a>, and then look for links to the information you want.</li>
</ol> </div>
</section>
</div>
</div>
</div>
<hr class=" m-ver-0 b-top b-w-2 b-c-red"/>
</main>
</div>
</div>
<div class=" pos-relative z-1 g12-12">
<footer>
<div class="bg-c-near-white">
<div class="wp-l ha-center">
<div class="p-ver-4 p-ver-5--m p-hor-3 p-hor-4--m p-hor-5--l">
<nav>
<div class="d-flex--m">
<div class="g12-8--m g12-9--l p-right-4--m">
<div class="of-hidden">
<ul class=" list d-flex--l flx-wp-wrap--l m-top-n4--l m-left-n4--l">
<li class="g12-3--l d-flex d-block--l p-top-4--l p-left-4--l">
<div class=" g12-6 g12-12--l p-top-0--l p-right-3 p-right-0--l b-right b-w-right-0--l b-w-1 b-c-moon-grey">
<a class=" nav__a nav-foot__a t-d-none c-near-black hov-c-mid-grey fil-near-black hov-fil-mid-grey tx-1" data-controls-uid="new" href="https://www.pspc.org.sg/new">
<span class="d-i-block va-middle m-right-1">
<svg class=" icon d-block">
<use xlink:href="/assets/symbols/build/symbolsheet.svg#arrow-circle-right"></use>
</svg>
</span>
<span class=" nav__a-label va-middle f-s-7 f-s-6--m f-w-7 t-t-upper l-t-base l-h-solid l-m-base">
                    I'm New                  </span>
</a>
</div>
</li>
<li class="g12-3--l d-flex d-block--l p-top-4--l p-left-4--l">
<div class=" p-top-4 g12-6 g12-12--l p-top-0--l p-right-3 p-right-0--l b-right b-w-right-0--l b-w-1 b-c-moon-grey">
<a class=" nav__a nav-foot__a t-d-none c-near-black hov-c-mid-grey fil-near-black hov-fil-mid-grey tx-1" data-controls-uid="services" href="https://www.pspc.org.sg/services">
<span class="d-i-block va-middle m-right-1">
<svg class=" icon d-block">
<use xlink:href="/assets/symbols/build/symbolsheet.svg#arrow-circle-right"></use>
</svg>
</span>
<span class=" nav__a-label va-middle f-s-7 f-s-6--m f-w-7 t-t-upper l-t-base l-h-solid l-m-base">
                    Services                  </span>
</a>
</div>
<div class=" p-top-4 g12-6 g12-12--l p-top-3--l p-left-3 p-left-0--l">
<ul class="list">
<li class=" ">
<a class=" nav__a nav-foot__a t-d-none c-mid-grey hov-c-grey tx-1" data-controls-uid="english-service" href="https://www.pspc.org.sg/services/english-service">
<span class=" nav__a-label f-s-6 f-s-5--m l-h-solid">
                            English Service                          </span>
</a>
</li>
<li class=" m-top-2 m-top-3--l">
<a class=" nav__a nav-foot__a t-d-none c-mid-grey hov-c-grey tx-1" data-controls-uid="mandarin-service" href="https://www.pspc.org.sg/services/mandarin-service">
<span class=" nav__a-label f-s-6 f-s-5--m l-h-solid">
                            Mandarin Service<br/>中文崇拜                          </span>
</a>
</li>
<li class=" m-top-2 m-top-3--l">
<a class=" nav__a nav-foot__a t-d-none c-mid-grey hov-c-grey tx-1" data-controls-uid="filipino-service" href="https://www.pspc.org.sg/services/filipino-service">
<span class=" nav__a-label f-s-6 f-s-5--m l-h-solid">
                            Filipino Service                          </span>
</a>
</li>
<li class=" m-top-2 m-top-3--l">
<a class=" nav__a nav-foot__a t-d-none c-mid-grey hov-c-grey tx-1" data-controls-uid="others" href="https://www.pspc.org.sg/services/others">
<span class=" nav__a-label f-s-6 f-s-5--m l-h-solid">
                            Mizo Service                          </span>
</a>
</li>
</ul>
</div>
</li>
<li class="g12-3--l d-flex d-block--l p-top-4--l p-left-4--l">
<div class=" p-top-4 g12-6 g12-12--l p-top-0--l p-right-3 p-right-0--l b-right b-w-right-0--l b-w-1 b-c-moon-grey">
<a class=" nav__a nav-foot__a t-d-none c-near-black hov-c-mid-grey fil-near-black hov-fil-mid-grey tx-1" data-controls-uid="ministries" href="https://www.pspc.org.sg/ministries">
<span class="d-i-block va-middle m-right-1">
<svg class=" icon d-block">
<use xlink:href="/assets/symbols/build/symbolsheet.svg#arrow-circle-right"></use>
</svg>
</span>
<span class=" nav__a-label va-middle f-s-7 f-s-6--m f-w-7 t-t-upper l-t-base l-h-solid l-m-base">
                    Ministries                  </span>
</a>
</div>
<div class=" p-top-4 g12-6 g12-12--l p-top-3--l p-left-3 p-left-0--l">
<ul class="list">
<li class=" ">
<a class=" nav__a nav-foot__a t-d-none c-mid-grey hov-c-grey tx-1" data-controls-uid="explore-christianity" href="https://www.pspc.org.sg/ministries/explore-christianity">
<span class=" nav__a-label f-s-6 f-s-5--m l-h-solid">
                            Explore Christianity                          </span>
</a>
</li>
<li class=" m-top-2 m-top-3--l">
<a class=" nav__a nav-foot__a t-d-none c-mid-grey hov-c-grey tx-1" data-controls-uid="get-connected" href="https://www.pspc.org.sg/ministries/get-connected">
<span class=" nav__a-label f-s-6 f-s-5--m l-h-solid">
                            Get Connected                          </span>
</a>
</li>
<li class=" m-top-2 m-top-3--l">
<a class=" nav__a nav-foot__a t-d-none c-mid-grey hov-c-grey tx-1" data-controls-uid="arts-and-music" href="https://www.pspc.org.sg/ministries/arts-and-music">
<span class=" nav__a-label f-s-6 f-s-5--m l-h-solid">
                            Arts and Music                          </span>
</a>
</li>
<li class=" m-top-2 m-top-3--l">
<a class=" nav__a nav-foot__a t-d-none c-mid-grey hov-c-grey tx-1" data-controls-uid="community-engagement" href="https://www.pspc.org.sg/ministries/community-engagement">
<span class=" nav__a-label f-s-6 f-s-5--m l-h-solid">
                            Community Engagement                          </span>
</a>
</li>
</ul>
</div>
</li>
<li class="g12-3--l d-flex d-block--l p-top-4--l p-left-4--l">
<div class=" p-top-4 g12-6 g12-12--l p-top-0--l p-right-3 p-right-0--l b-right b-w-right-0--l b-w-1 b-c-moon-grey">
<a class=" nav__a nav-foot__a t-d-none c-near-black hov-c-mid-grey fil-near-black hov-fil-mid-grey tx-1" data-controls-uid="audio-sermons" href="https://www.pspc.org.sg/audio-sermons">
<span class="d-i-block va-middle m-right-1">
<svg class=" icon d-block">
<use xlink:href="/assets/symbols/build/symbolsheet.svg#arrow-circle-right"></use>
</svg>
</span>
<span class=" nav__a-label va-middle f-s-7 f-s-6--m f-w-7 t-t-upper l-t-base l-h-solid l-m-base">
                    Audio Sermons                  </span>
</a>
</div>
<div class=" p-top-4 g12-6 g12-12--l p-top-3--l p-left-3 p-left-0--l">
<ul class="list">
<li class=" ">
<a class=" nav__a nav-foot__a t-d-none c-mid-grey hov-c-grey tx-1" data-controls-uid="english" href="https://www.pspc.org.sg/audio-sermons/english">
<span class=" nav__a-label f-s-6 f-s-5--m l-h-solid">
                            English                          </span>
</a>
</li>
</ul>
</div>
</li>
</ul>
</div>
</div>
<div class="g12-4--m g12-3--l p-left-4--m">
<div class="of-hidden">
<ul class=" list m-top-n3--m m-top-n4--l">
<li class=" w-50 w-100--m p-top-3 p-top-4--l p-right-3 p-right-0--m b-right b-w-right-0--m b-w-1 b-c-moon-grey">
<a class=" nav__a nav-foot__a t-d-none c-near-black hov-c-mid-grey fil-near-black hov-fil-mid-grey tx-1" data-controls-uid="events" href="https://www.pspc.org.sg/events">
<span class="d-i-block va-middle m-right-1">
<svg class=" icon d-block">
<use xlink:href="/assets/symbols/build/symbolsheet.svg#arrow-circle-right"></use>
</svg>
</span>
<span class=" nav__a-label va-middle f-s-7 f-s-6--m f-w-7 t-t-upper l-t-base l-h-solid l-m-base">
                  Happenings                </span>
</a>
</li>
<li class=" w-50 w-100--m p-top-3 p-top-4--l p-right-3 p-right-0--m b-right b-w-right-0--m b-w-1 b-c-moon-grey">
<a class=" nav__a nav-foot__a t-d-none c-near-black hov-c-mid-grey fil-near-black hov-fil-mid-grey tx-1" data-controls-uid="about" href="https://www.pspc.org.sg/about">
<span class="d-i-block va-middle m-right-1">
<svg class=" icon d-block">
<use xlink:href="/assets/symbols/build/symbolsheet.svg#arrow-circle-right"></use>
</svg>
</span>
<span class=" nav__a-label va-middle f-s-7 f-s-6--m f-w-7 t-t-upper l-t-base l-h-solid l-m-base">
                  About                </span>
</a>
</li>
<li class=" w-50 w-100--m p-top-3 p-top-4--l p-right-3 p-right-0--m b-right b-w-right-0--m b-w-1 b-c-moon-grey">
<a class=" nav__a nav-foot__a t-d-none c-near-black hov-c-mid-grey fil-near-black hov-fil-mid-grey tx-1" data-controls-uid="contact" href="https://www.pspc.org.sg/contact">
<span class="d-i-block va-middle m-right-1">
<svg class=" icon d-block">
<use xlink:href="/assets/symbols/build/symbolsheet.svg#arrow-circle-right"></use>
</svg>
</span>
<span class=" nav__a-label va-middle f-s-7 f-s-6--m f-w-7 t-t-upper l-t-base l-h-solid l-m-base">
                  Contact                </span>
</a>
</li>
<li class=" w-50 w-100--m p-top-3 p-top-4--l p-right-3 p-right-0--m b-right b-w-right-0--m b-w-1 b-c-moon-grey">
<a class=" nav__a nav-foot__a t-d-none c-near-black hov-c-mid-grey fil-near-black hov-fil-mid-grey tx-1" data-controls-uid="home" href="https://www.pspc.org.sg">
<span class="d-i-block va-middle m-right-1">
<svg class=" icon d-block">
<use xlink:href="/assets/symbols/build/symbolsheet.svg#arrow-circle-right"></use>
</svg>
</span>
<span class=" nav__a-label va-middle f-s-7 f-s-6--m f-w-7 t-t-upper l-t-base l-h-solid l-m-base">
                  Home                </span>
</a>
</li>
</ul>
</div>
</div>
</div>
</nav>
<div class=" d-flex--l m-top-5 p-top-4 p-top-4--l b-top b-w-1 b-c-moon-grey">
<div class="g12-6--l">
<div class="d-i-block">
<div class=" f-s-6 c-mid-grey">
<p>© Prinsep Street Presbyterian Church 2021</p> </div>
</div>
<nav class=" d-i-block--m m-top-2 m-top-0--m m-left-4--m p-left-4--m b-left--m b-w-1--m b-c-moon-grey--m">
<ul class="list">
<li class=" d-i-block">
<a class=" nav__a nav-foot__a c-mid-grey hov-c-grey tx-1" data-controls-uid="privacy-policy" href="https://www.pspc.org.sg/legal">
<span class=" nav__a-label f-s-6">
            Privacy Policy          </span>
</a>
</li>
</ul>
</nav>
</div>
<div class="g12-6--l d-flex flx-ai-end flx-jc-end--l m-top-4 m-top-0--l">
<div class=" footer-foot__illus-facade m-right-4"></div>
<div class="footer-foot__illus-burning-tree"></div>
</div>
</div>
</div>
</div>
</div>
</footer>
</div>
</div>
<script async="" src="https://www.pspc.org.sg/assets/javascripts/build/global.js"></script> </body>
</html>