<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>AdF.ly - Suspended</title>
<link href="https://cdn.adf.ly/static/image/favicon.ico" rel="icon" type="image/ico"/>
<link href="https://cdn.adf.ly/static/image/favicon.ico" rel="shortcut icon"/>
<link href="https://cdn.adf.ly/static/css/static.css" media="screen" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="container">
<header id="header">
<div class="container">
<h1 class="ir" id="logo">
<a href="/">Adfly</a>
</h1>
</div>
</header>
<div class="container">
<p style="font-size: 14px; font-family: arial; color: white; font-weight:bold;">This AdF.ly account has been suspended.</p>
<p style="font-size: 14px; font-family: arial; color: white;">If you feel this has been in error, please see our <a href="https://adf.ly/terms">terms and conditions</a>.</p>
</div>
</div></body>
</html>
