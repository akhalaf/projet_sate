<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<title>Billy Performance Network</title>
<meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport"/>
<script src="/main.js"></script>
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet"/>
<link href="/app.css" rel="stylesheet"/>
<link href="/assets/favicon.ico" rel="icon" type="image/x-icon"/>
<!--Facebook OpenGraph-->
<meta content="http://www.billymob.com" property="og:url"/>
<meta content="Billy Performance Network" property="og:site_name"/>
<meta content="website" property="og:type"/>
<meta content="Billy Performance Network" property="og:title"/>
<meta content="Billy Mobile is a mobile advertising start up based in Barcelona, operating exclusively in the mobile arena of the ad tech industry" property="og:description"/>
<meta content="//billymob.com/assets/facebook-id-photo.svg" property="og:image"/>
<!--Twitter metas-->
<meta content="summary" name="twitter:card"/>
<meta content="http://www.billymob.com" name="twitter:site"/>
<meta content="Billy Performance Network" name="twitter:title"/>
<meta content="Billy Mobile is a mobile advertising start up based in Barcelona, operating exclusively in the mobile arena of the ad tech industry" name="twitter:description"/>
<meta content="//billymob.com/assets/facebook-id-photo.svg" name="twitter:image"/>
<!-- Facebook Pixel Code-->
<script>!function (f, b, e, v, n, t, s) {
            if (f.fbq)return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq)f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1261824240496816');
        fbq('track', "PageView");
    </script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=1261824240496816&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<!-- End Facebook Pixel Code-->
</head>
<body class="header-component">
<header class="navbar-default navbar navbar-fixed-top billy-navbar" id="top">
<div class="container">
<div class="navbar-header">
<button class="collapsed navbar-toggle menu-opener" type="button">
<img src="/assets/header_burguer.png"/>
</button>
<a class="navbar-brand" href="/">
<img src="/assets/logo.png"/>
</a>
</div>
<div class="navbar-collapse-close"></div>
<nav class="collapse navbar-collapse" id="bs-navbar">
<ul class="nav navbar-nav navbar-right">
<li class="active"><a href="/en/">HOME<br/></a></li><li><a href="/en/advertising-solutions">ADVERTISING SOLUTIONS</a></li><li><a href="/en/publishers">PUBLISHERS</a></li><li><a href="/en/active-bx">ACTIVE B<sup>x</sup></a></li><li><a href="/en/about">COMPANY</a><ul><li><a href="/en/careers">CAREERS</a></li><li><a href="/en/press">PRESS</a></li></ul></li> <li><a href="https://blog.billymob.com" target="_blank">Blog</a></li>
<li><a href="https://push.billymob.com/" target="_blank">Push Traffic Platform<sup> NEW</sup></a></li>
<li class="orange-text">
<a href="https://publishers.billymob.com" target="_blank">Login publishers</a>
</li>
<li class="orange-text">
<a href="https://advertisers.billymob.com" target="_blank">Login advertisers</a>
</li>
</ul>
</nav>
</div>
</header>
<div class="main-section">
<div class="ms-section">
<div class="ms-background home">
<video class="home-video" loop="" muted=""><source src="/assets/video.mp4"/></video> </div>
<h1 class="ms-text container">Welcome to <br/>the leading affiliate platform</h1>
</div>
<div class=" meet-us-section">
<div class="container">
<div class="col-md-4 col-sm-4">
<h3 class="meet-us-title">Meet us at:</h3>
</div>
<div class="col-md-2 col-sm-2">
<div class="meet-us-box">
<a href="https://www.eventbrite.es/e/clickbid-awe19-europush-registration-59070951807" target="_blank">
<img src="/assets/events/Screen Shot 2019-06-14 at 12.25.14 PM.png"/>
</a>
<span>07/07/2019</span>
</div>
</div> <div class="col-md-2 col-sm-2">
<div class="meet-us-box">
<a href="https://affiliateworldconferences.com/europe/" target="_blank">
<img src="/assets/events/download.png"/>
</a>
<span>07/08/2019</span>
</div>
</div> </div>
</div>
</div><div class="container home-numbers">
<div class="col-md-3 col-sm-6 col-xs-6">
<div class="number-box">
<h4>27B</h4>
<span>clicks /month</span>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-6">
<div class="number-box">
<h4>11M</h4>
<span>conversions /month</span>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-6">
<div class="number-box">
<h4>16K+</h4>
<span>publishers</span>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-6">
<div class="number-box">
<h4>900+</h4>
<span>advertisers</span>
</div>
</div>
</div>
<div class="container text-block ">
<div class="divisor"></div>
<p class="subtitle ">Operating exclusively in the mobile arena of the ad tech industry, Billy is successfully shrinking the divide between advertisers and publishers. With advanced proprietary technology, 24/7 specialist advice and global presence, maximum value for both sides of the transaction is ensured.</p> </div>
<div class="split-boxes home-box">
<div class="container">
<div class="col-md-6 split-box">
<h3 class="title">Advertising<br/>solutions</h3>
<div class="divider"></div>
<ul class="bullet-list">
<li>
<img src="/assets/home/splitBoxes_advertisers_1.svg"/>
<p><b>Beam mobile content</b>, app promotion and video ads to high quality users globally</p>
</li>
<li>
<img src="/assets/home/splitBoxes_advertisers_2.svg"/>
<p><b>Advanced proprietary technology</b> that gets your ad to the users that count</p>
</li>
<li>
<img src="/assets/home/splitBoxes_advertisers_3.svg"/>
<p><b>Dedicated account managers</b> to rocket your ROI and smash your KPI's</p>
</li>
</ul>
<div class="box-buttons">
<a class="btn btn-linear-white" href="/en/advertising-solutions">Learn more</a>
<a class="btn btn-filled-white light-blue scroll-footer">Contact us</a>
</div>
</div>
<div class="col-md-6 split-box">
<h3 class="title">Powering<br/>publishers</h3>
<div class="divider"></div>
<ul class="bullet-list">
<li>
<img src="/assets/home/splitBoxes_publishers_1.svg"/>
<p><b>Providing Media buyers and ad Networks</b> with a service tailored to individual needs</p>
</li>
<li>
<img src="/assets/home/splitBoxes_publishers_2.svg"/>
<p><b>Inventory of 6,000+</b> CPA, CPL and CPI offers in a self-service platform</p>
</li>
<li>
<img src="/assets/home/splitBoxes_publishers_3.svg"/>
<p><b>Advanced technology and Bx link</b> to get your results maximised<b></b></p>
</li>
</ul>
<div class="box-buttons">
<a class="btn btn-linear-white" href="/en/publishers">Learn more</a>
<a class="btn btn-filled-white " href="https://publishers.billymob.com/" target="_blank">Sign up</a>
</div>
</div>
</div>
</div>
<div class="image-box home-section">
<div class="container">
<div class="col-md-3">
<h3 class="title">Active B<sup>x</sup></h3>
<div class="divider"></div>
<p>Our proprietary learning machine. Advanced proprietary technology that gets the right ad, to the right user, at the right time. Fuel your campaigns with artificial intelligence.</p>
<a class="btn btn-linear-blue" href="/en/active-bx">Learn more</a>
</div>
</div>
</div><div class="column-box home-section">
<div class="container">
<div class="col-md-4 col-sm-6">
<h3 class="title">Company</h3>
<img src="/assets/home/columnBoxes_about.png"/>
<p>Billy is one of the faster growing startups with a unique team and values.</p>
<a class="btn btn-linear-blue" href="/en/about">Learn more</a>
</div>
<div class="col-md-4 col-sm-6">
<h3 class="title">Careers</h3>
<img src="/assets/home/columnBoxes_careers.png"/>
<p>Join a talented team to grow with us and shape the mobile ad-tech industry together.</p>
<a class="btn btn-linear-blue" href="/en/careers">Learn more</a>
</div>
<div class="col-md-4 col-sm-6">
<h3 class="title">Press</h3>
<img src="/assets/home/columnBoxes_press.png"/>
<p>The leading affiliate platform you can trust. Find recent articles, the latest press release and press kit.</p>
<a class="btn btn-linear-blue" href="/en/press">Learn more</a>
</div>
</div>
</div><footer class="footer-component">
<div class="container">
<div class="col-md-4">
<h4 class="title">Let's talk!</h4>
<form class="contact">
<input name="name" placeholder="Your name" required="required" type="text"/>
<input name="email" placeholder="E-mail" required="required" type="email"/>
<textarea name="message" placeholder="Message" required="required" rows="4"></textarea>
<button class="btn btn-linear-blue" type="submit">Send message</button>
</form>
</div>
<div class="col-md-8">
<div class="social-networks">
<a href="https://www.facebook.com/billyperformance/" target="_blank">
<img src="/assets/footer_fb.png"/>
</a>
<a href="https://twitter.com/Billymob_" target="_blank">
<img src="/assets/footer_twitter.png"/>
</a>
<a href="https://www.linkedin.com/company/billy" target="_blank">
<img src="/assets/footer_in.png"/>
</a>
</div>
<div class="map-section">
<img src="/assets/footer.svg"/>
<div class="map-information">
<div class="col-md-6">
<h5 class="title">Europe headquarter</h5>
<p>Balmes 76 - Pral 2ª<br/>08007 Barcelona,<br/>Spain <b>T. +34 93 200 58 58</b></p>
</div>
<div class="col-md-6">
<h5 class="title">Asia office</h5>
<p>113 Cecil Street, #08-02/02A, Keck<br/>Seng Tower, Singapore 069535,<br/>Singapore <b>T. +34 93 200 58 58﻿</b></p>
</div>
</div>
</div>
<div class="legal-section">
<span class="copy">© 2021 Billy. All rights reserved</span>
<div class="link-section">
<a href="http://www.billymob.com/privacy-policy.html" target="_blank">Privacy terms</a>
<span>|</span>
<a href="http://www.billymob.com/terms-and-conditions.html" target="_blank">Terms &amp; conditions</a>
<span class="legal-separator"></span>
<a href="http://www.billymob.com/advertisers-terms.html" target="_blank">Advertisers terms</a>
<span>|</span>
<a href="http://www.billymob.com/publishers-terms.html" target="_blank">Publishers terms</a>
<span>|</span>
<a href="http://www.billymob.com/antifraud-guide-lines.html" target="_blank">Quality and anti-fraud guidelines</a>
</div>
</div>
</div>
<!-- Google Analytics-->
<script>window.ga = window.ga || function () {
            (ga.q = ga.q || []).push(arguments)
        };
    ga.l = +new Date;
    ga('create', 'UA-76964151-1', 'auto');
    ga('send', 'pageview');</script>
<script async="" src="//www.google-analytics.com/analytics.js"></script><!-- End Google Analytics-->
<script>window.intercomSettings = {
        app_id: "uidz8r4x"
    };</script>
<script>(function () {
        var w = window;
        var ic = w.Intercom;
        if (typeof ic === "function") {
            ic('reattach_activator');
            ic('update', intercomSettings);
        }
        else {
            var d = document;
            var i = function () {
                i.c(arguments)
            };
            i.q = [];
            i.c = function (args) {
                i.q.push(args)
            };
            w.Intercom = i;
            function l() {
                var s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = 'https://widget.intercom.io/widget/uidz8r4x';
                var x = d.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            }

            if (w.attachEvent) {
                w.attachEvent('onload', l);
            }
            else {
                w.addEventListener('load', l, false);
            }
        }
    })();
</script>
</div></footer></body>
</html>