<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-gb" xml:lang="en-gb" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="IE=EmulateIE7" http-equiv="X-UA-Compatible"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="index, follow" name="robots"/>
<meta content="" name="keywords"/>
<meta content="" name="description"/>
<meta content="" name="generator"/>
<title>Fastrack Global Logistics</title>
<link href="/templates/cms_web_tpl/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/plugins/system/jcemediabox/css/jcemediabox.css?version=119" rel="stylesheet" type="text/css"/>
<link href="/plugins/system/jcemediabox/themes/standard/css/style.css?version=119" rel="stylesheet" type="text/css"/>
<script src="/includes/js/joomla.javascript.js" type="text/javascript"></script>
<script src="/media/system/js/mootools.js" type="text/javascript"></script>
<script src="/plugins/system/jcemediabox/js/jcemediabox.js?version=119" type="text/javascript"></script>
<script type="text/javascript">

			function changeCaptcha(modifyFocus) {
				var url = 'https://www.fastrackgl.com/index.php?option=com_aicontactsafe&sTask=captcha&task=getNewCode&id=0&pj_id=0&pf=1&lang=en&format=raw';
				new Ajax(url, {
					method: 'get',
					update: $('div_captcha_img'),
					onRequest: function(){ $('div_captcha_img').setHTML('Please wait ...'); }
				}).request();
				if (modifyFocus) {
					document.getElementById('captcha-code').focus();
				}
			}
			function setDate( newDate, idDate ) {
				if (document.getElementById('day_'+idDate)) {
					document.getElementById('day_'+idDate).value = newDate.substr(8,10);
				}
				if (document.getElementById('month_'+idDate)) {
					var selMonth = parseInt(newDate.substr(5,7)) - 1;
					document.getElementById('month_'+idDate).options[selMonth].selected = true;
				}
				if (document.getElementById('year_'+idDate)) {
					document.getElementById('year_'+idDate).value = newDate.substr(0,4);
				}
			}
			function daysInFebruary( year ){
				return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
			}
			function daysInMonth( month, year ) {
				var days = 31;
				switch( true ) {
					case month == 2 :
						days = daysInFebruary( year );
						break;
					case month == 4 || month == 6 || month == 9 || month == 11 :
						days = 30;
						break;
				}
			   return days;
			}
			function checkDate( idDate ) {
				var year = 0;
				var month = 0;
				var day = 0;
				if (document.getElementById('year_'+idDate)) {
					year = document.getElementById('year_'+idDate).value;
				}
				if (document.getElementById('month_'+idDate)) {
					month = document.getElementById('month_'+idDate).value;
				}
				if (document.getElementById('day_'+idDate)) {
					day = document.getElementById('day_'+idDate).value;
				}
				if (day > 0 && month > 0 && year > 0) {
					var days = daysInMonth( month, year );
					if (day > days) {
						day = days;
						document.getElementById('day_'+idDate).value = days;
						var error = 'Only %days% days in the selected month ! Please set the month first.';
						alert( error.replace( '%days%', days ) );
					}
				}
				if (document.getElementById(idDate)) {
					document.getElementById(idDate).value = year+'-'+month+'-'+day;
				}
			}JCEMediaBox.init({popup:{width:"",height:"",legacy:0,lightbox:0,shadowbox:0,resize:1,icons:1,overlay:1,overlayopacity:0.8,overlaycolor:"#000000",fadespeed:500,scalespeed:500,hideobjects:1,scrolling:"fixed",close:2,labels:{'close':'Close','next':'Next','previous':'Previous','cancel':'Cancel','numbers':'{$current} of {$total}'},cookie_expiry:"",google_viewer:0},tooltip:{className:"tooltip",opacity:0.8,speed:150,position:"br",offsets:{x: 16, y: 16}},base:"/",imgpath:"plugins/system/jcemediabox/img",theme:"standard",themecustom:"",themepath:"plugins/system/jcemediabox/themes"});
  </script>
<link href="/templates/system/css/system.css" rel="stylesheet" type="text/css"/>
<link href="/templates/system/css/general.css" rel="stylesheet" type="text/css"/>
<link href="/templates/cms_web_tpl/css/template.css" media="screen" rel="stylesheet" type="text/css"/>
<!--[if IE 6]><link rel="stylesheet" href="/templates/cms_web_tpl/css/template.ie6.css" type="text/css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="/templates/cms_web_tpl/css/template.ie7.css" type="text/css" media="screen" /><![endif]-->
<script src="/templates/cms_web_tpl/script.js" type="text/javascript"></script>
<style type="text/css">
.productPrice {font-weight:bold; white-space: nowrap; color:#AC23B1;}
.product-Old-Price {color:#C6000E; text-decoration:line-through;}
a, a:link, a:visited, a.visited {color: #0106E3;}
a:hover, a.hover {color: #C6000E;}
.art-postheader {font-weight: normal; font-size: 18px; color: #000000;}
.art-nav {background-color:#FF1919;}
.art-menu a .t {font-weight:bold;color:#000000;border:1px solid #DFDFDF;background-color:#EFEFEF;}
.art-menu a:hover .t, .art-menu li:hover a .t, .art-menu li:hover>a .t {color:#000000;border:1px solid #DFDFDF;background-color:#BFBFBF;}
.art-menu a.active .t {color:#FFFFFF;border:1px solid #343434;background-color:#787878;}
.art-menu ul a {width: 200px;font-weight:bold;}
.art-menu ul a, .art-menu ul a:link, .art-menu ul a:visited, .art-nav ul.art-menu ul span, .art-nav ul.art-menu ul span span
{color:#FDFBFB;background-color:#4C4848;}
.art-menu ul li a:hover, .art-menu ul li:hover>a, .art-nav .art-menu ul li a:hover span, .art-nav .art-menu ul li a:hover span span, .art-nav .art-menu ul li:hover>a span, .art-nav .art-menu ul li:hover>a span span {color: #FFFFFF;background-color:#000000;}
.art-menu ul a.active {color:#000000;background-color:#DFDFDF;}
.art-sheet {width:900px;border:1px solid #08004D;}
.art-sheet-cc, .vmCartChild {background-color:#FFFFFF;}
.art-content-layout .art-content {width:56%;}
.art-content-layout .art-sidebar1 {width:22%;background-color:#FFFFFF;}
.art-content-layout .art-sidebar2 {width:22%;background-color:#FFFFFF;}
.art-content-layout .art-content-sidebar1 {width:78%;}
.art-content-layout .art-content-sidebar2 {width:78%;}
.art-content-layout .art-content-wide {width: 100%;}
.art-blockheader .t {font-size:16px; color:#000000;font-weight:bold;background-color:#DFDFDF;border-top:1px solid #CFCFCF;border-left:1px solid #CFCFCF;border-right:1px solid #CFCFCF;}
.art-blockcontent-body {font-size:15px; color:#565656;background-color:#FFFFFF;border:1px solid #CFCFCF;}
.art-blockcontent-body a:link, .art-blockcontent-body a:visited, .art-blockcontent-body a.visited {color:#0106E3;}
.art-blockcontent-body a:hover, .art-blockcontent-body a.hover {color:#C6000E;}
.art-footer .art-footer-text {color:#FFFFFF;}
.art-footer .art-footer-background {background-color:#2E2E2E;}
</style>
</head>
<body style="font-size:15px;color:#000000;background: url( 'photo/images/ft_bg.jpg' ) repeat center top transparent;background-color:#BABABA;margin-top:5px;background-attachment:fixed;">
<div id="art-main">
<div class="art-sheet">
<div class="art-sheet-cc"></div>
<div class="art-sheet-body">
<div>
<table border="0" cellpadding="0" cellspacing="0" id="headertbl" style="padding-bottom:0px;background:#FFFFFF;" width="100%">
<tr>
<td id="headertext"><img alt="ft header" height="250" src="photo/images/ft_header.jpg" width="900"/></td>
</tr>
</table>
</div>
<div class="art-post">
<div class="art-post-body">
<div class="art-post-inner">
<div class="art-postcontent">
<!-- article-content -->
<div style="text-align: justify;"><strong>Fas track Global Logistics Sdn Bhd</strong> was incorporated in the year 2006 in Penang, Malaysia. We offer total logistics solutions through our well trained staffs and strong partnership with shipping agents, forwarding agents, sea freight companies and air freight companies from all over the world. Fastrack Global Logistics strives to expand further on its current pool of logistics and freight forwarding services to provide greater value to our customers by seeking out long term affiliations with key partners who complement our network and meet our high level of expectations.<br/><br/><span style="color: #0000ff;"><strong>Our comprehensive service offering includes:</strong></span>
<ul>
<li><strong><span style="color: #0000ff;">International Air Freight</span>:</strong> Domestic and Worldwide service connection with major Air freight agent partnership. We also handle all commodities including oversized, perishable, restricted articles and dangerous goods. We offer a complete range of supply chain solutions, with our global air freight team providing local support at point of origin and point of destination. We go to great lengths to ensure that your goods arrive according to your expectation and always strive to exceed your requirements.</li>
<li><span style="color: #0000ff;"><strong>International Ocean Freight:</strong></span> We offer market leading rates and space security with all the major shipping lines, supporting your access to the global market. Full or part container load together with support from mega carriers to all over the destinations.</li>
<li><span style="color: #0000ff;"><strong>Land Transportation:</strong></span> Extensive domestic and cross-border coverage services through our fleet of bonded truck services and haulage partnership. Our operations staff ensures that through constant communications with our clients, all deliveries are on time and allocated to your preference. With dedicated transport drivers at your service, we deliver an efficient and professional service that meets all individual transport needs. </li>
<li><strong><span style="color: #0000ff;">Custom Brokerage:</span> </strong> We are linked with the Customs, carriers, terminal operators and clients through our Electronic Data Interchange (EDI) and computer network communication. We continuously updating our system with all changes to customs regulatory procedures, ensuring our customers prompt compliance. In addition to our standard customs services, our customs brokerage department will analyze your shipments on HS Code classification and duties/taxes exemption to ensure timely delivery.</li>
<li><span style="color: #0000ff;"><strong>Others:</strong></span> Transshipment and Cross Trade, Warehousing and Distribution, Personal Effect shipment and Exhibition Cargo arrangement.</li>
</ul>
</div>
<!-- /article-content -->
</div>
<div class="cleared"></div>
</div>
<div class="cleared"></div>
</div>
</div>
<div class="art-content-layout">
<div class="art-content-layout-row">
<div class="art-layout-cell art-content-wide">
<div style="margin-top:5px;"></div>
<div class="art-post">
<div class="art-post-body">
<div class="art-post-inner">
<div class="art-postcontent">
<!-- article-content -->
<form action="index.php?option=com_aicontactsafe&amp;sTask=message&amp;pf=1" enctype="multipart/form-data" method="post" name="adminForm">
<table border="0" cellpadding="0" cellspacing="5" id="aiContactSafeForm">
<tr>
<td valign="top"><div style="width: 520px;"><strong>CONTACT US</strong><br/>We welcome enquiries &amp; feedback regarding our products &amp; services. <br/>You may contact us directly or submit the online form on the right. <br/><br/><strong>Fas track Global Logistics Sdn Bhd</strong> (738307-k)<br/>22-03, Lintang Maung 2,<br/>Batu Maung, 11960, Bayan Lepas,<br/>Penang, Malaysia<br/><br/>Tel: 604-611 9838<br/>Fax: 604-611 8893<br/>Email: <a href="mailto:psteoh@fastrackgl.com">psteoh@fastrackgl.com</a><br/>Website: www.fastrackgl.com</div></td>
<td valign="top">
<div class="aiContactSafe" id="aiContactSafe_contact_form">
<div class="aiContactSafe" id="aiContactSafe_info">Fields marked with * are required..</div>
<div class="aiContactSafe_row" id="aiContactSafe_row_name"><div class="aiContactSafe_contact_form_field_label_left"><span class="aiContactSafe_label" id="aiContactSafe_label_name"><label for="name">Name</label></span><label class="required_field"> * </label></div><div class="aiContactSafe_contact_form_field_right"><input class="inputbox" id="name" name="name" style="width:250px;" type="text" value=""/></div></div>
<div class="aiContactSafe_row" id="aiContactSafe_row_email"><div class="aiContactSafe_contact_form_field_label_left"><span class="aiContactSafe_label" id="aiContactSafe_label_email"><label for="email">Email</label></span><label class="required_field"> * </label></div><div class="aiContactSafe_contact_form_field_right"><input class="inputbox" id="email" name="email" style="width:250px;" type="text" value=""/></div></div>
<div class="aiContactSafe_row" id="aiContactSafe_row_phone"><div class="aiContactSafe_contact_form_field_label_left"><span class="aiContactSafe_label" id="aiContactSafe_label_phone"><label for="phone">Phone</label></span><label class="required_field"></label></div><div class="aiContactSafe_contact_form_field_right"><input class="inputbox" id="phone" name="phone" style="width:250px;" type="text" value=""/></div></div>
<div class="aiContactSafe_row" id="aiContactSafe_row_subject"><div class="aiContactSafe_contact_form_field_label_left"><span class="aiContactSafe_label" id="aiContactSafe_label_subject"><label for="subject">Subject</label></span><label class="required_field"> * </label></div><div class="aiContactSafe_contact_form_field_right"><input class="combobox" id="subject" name="subject" style="width:250px;" type="text" value=""/></div></div>
<div class="aiContactSafe_row" id="aiContactSafe_row_aics_message"><div class="aiContactSafe_contact_form_field_label_left"><span class="aiContactSafe_label" id="aiContactSafe_label_aics_message"><label for="aics_message">Message</label></span><label class="required_field"> * </label></div><div class="aiContactSafe_contact_form_field_right"><textarea cols="33" id="aics_message" name="aics_message" rows="7"></textarea></div></div>
</div>
<br/>
<div id="aiContactSafeBtns"><div id="aiContactSafeSend"><input type="submit" value="Send"/></div></div>
</td>
</tr>
</table>
<input id="option" name="option" type="hidden" value="com_aicontactsafe"/><input id="sTask" name="sTask" type="hidden" value="message"/><input id="task" name="task" type="hidden" value="display"/><input id="send_mail" name="send_mail" type="hidden" value="1"/><input id="pf" name="pf" type="hidden" value="1"/><input id="return_to" name="return_to" type="hidden" value="https://www.fastrackgl.com/"/><input id="back_button" name="back_button" type="hidden" value="0"/><input id="boxchecked" name="boxchecked" type="hidden" value="0"/><input name="e8922ea7c41ee3e5515483ae6e66ebe6" type="hidden" value="1"/></form>
<!-- /article-content -->
</div>
<div class="cleared"></div>
</div>
<div class="cleared"></div>
</div>
</div>
</div>
</div>
</div>
<div class="cleared"></div>
<div class="art-footer">
<div class="art-footer-inner">
<div class="art-footer-text">
<p>Copyright © 2013 <b>Fastrack Global Logistics Sdn Bhd</b> <span style="font-size:8pt">(738307-K)</span> . All Rights Reserved.</p>
</div>
</div>
<div class="art-footer-background"></div>
</div>
<div class="cleared"></div>
</div>
</div>
<div class="cleared"></div>
<p align="center" class="art-page-footer">
<a href="http://www.shoppingcart2u.com">Ecommerce Website Malaysia</a></p>
</div>
</body>
</html>