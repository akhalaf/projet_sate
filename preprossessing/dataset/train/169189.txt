<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet"/>
<meta content="" name="author"/>
<meta content="https://aucaller.com/assets/img/aucaller.png" property="og:image"/>
<meta content="image/png" property="og:image:type"/>
<meta content="1200" property="og:image:width"/>
<meta content="630" property="og:image:height"/>
<link href="/assets/img/favicon.ico" rel="icon" type="image/png"/>
<title>Australia Reverse Phone Lookup - AUCaller.com</title>
<link href="https://www.aucaller.com/" rel="canonical"/>
<script type="application/ld+json">
     {
			"@context": "http://schema.org",
			"@graph": [{
                "@type": "WebSite",
                "name": "AUCaller",
                "url": "https://www.aucaller.com/",
			    "potentialAction": {
					"@type": "SearchAction",
					"target": "https://www.aucaller.com/{search_term_string}",
					"query-input": "required name=search_term_string"
				}
			}, {
                "@type": "Organization",
                "name": "AUCaller",
                "url": "https://www.aucaller.com/"
          }]
     }
</script>
<!-- Bootstrap core CSS -->
<link href="/assets/css/bootstrap.css" rel="stylesheet"/>
<!-- Custom styles for this template -->
<link href="/assets/css/main.css" rel="stylesheet"/>
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="/assets/js/hover.zoom.js"></script>
<script src="/assets/js/hover.zoom.conf.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<link href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" rel="stylesheet" type="text/css"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script>
        window.addEventListener("load", function () {
            var cookie_countries = ['AT', 'BE', 'BG', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FN', 'FR', 'DE', 'GR', 'HU', 'IE', 'IT', 'LV', 'LT', 'LU', 'NT', 'NE', 'PL', 'PT', 'RO', 'SK', 'SI', 'ES', 'SE', 'UK'];
            var country = "CN";
            if (cookie_countries.indexOf(country) > -1)
            {
                window.cookieconsent.initialise({
                    "palette": {
                        "popup": {
                            "background": "#ffffff"
                        },
                        "button": {
                            "background": "#1abc9c",
                            "text": "#ffffff",
                            "border": "#1abc9c"
                        }
                    },
                    "theme": "classic",
                    "content": {
                        "message": "We use cookies for traffic analysis, ads measurement, and other purposes. By continuing to use this website, you agree to our",
                        "dismiss": "OK",
                        "link": "Privacy Policy",
                        "href": "https://www.aucaller.com/privacy"
                    }
                });
            }
        });
    </script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-73261445-1"></script>
<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-73261445-1', { 'anonymize_ip': true });
	</script>
</head>
<body>
<!-- Static navbar -->
<div class="navbar navbar-inverse navbar-static-top">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="https://www.aucaller.com/">AU CALLER</a>
</div>
<div class="navbar-collapse collapse">
<ul class="nav navbar-nav navbar-right">
<li><a href="/contact">Contact Us</a></li>
</ul>
</div><!--/.nav-collapse -->
</div>
</div>
<div id="ww">
<div class="container">
<div class="row">
<form id="search">
<div class="col-lg-10 col-lg-offset-1 centered">
<h1>Australia Reverse Phone Lookup</h1>
<p>With more than 10 million registered numbers, the Do Not Call Register is a popular service among Australians. However, registering your number with the Do Not Call Register will not prevent you from receiving unsolicited marketing calls and faxes or phone calls related to scams. Use our service to get general details about the unknown Australian phone numbers as well as other people experience with them.</p>
<div class="input-group input-group-lg">
<input class="form-control" name="name" placeholder="Enter a Phone Number (e.g. (02) 3300 0000)" type="text"/>
<span class="input-group-btn">
<input class="btn btn-danger" type="submit" value="Search!"/>
</span>
</div><!-- /input-group -->
<font color="red"><span id="klaidasearch"></span></font>
</div><!-- /col-lg-8 -->
</form>
</div><!-- /row -->
</div> <!-- /container -->
</div><!-- /ww --><!-- +++++ Projects Section +++++ -->
<div class="container pt">
<div class="row mt">
<div class="adsense">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- aucaller-front-1 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-2913233292297309" data-ad-format="auto" data-ad-slot="5825347724" style="display:block"></ins>
<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
</div>
<div class="col-lg-8">
<h2>Recent Comments</h2>
<div class="panel-group">
<div class="panel panel-default">
<div class="panel-heading"><a href="/0413767526">0413 767 526</a></div>
<div class="panel-footer"><small><span>John Denver </span><span> / </span><span>5 hours, 38 minutes ago</span></small></div>
<div class="panel-body"><em>Caller Type: Unknown</em><br/><br/>These automated calls claim their tax file number (TFN) has been suspended and that there is a legal case against their name.

“The call tells people they must contact the caller by pressing '1' or they will be referred to the court and arrested.”</div>
</div>
<div class="panel panel-default">
<div class="panel-heading"><a href="/0742434961">(07) 4243 4961</a></div>
<div class="panel-footer"><small><span>Eliza Bird </span><span> / </span><span>6 hours, 25 minutes ago</span></small></div>
<div class="panel-body"><em>Caller Type: Unknown</em><br/><br/>Griffith University  after course enquiries </div>
</div>
<div class="panel panel-default">
<div class="panel-heading"><a href="/0417670481">0417 670 481</a></div>
<div class="panel-footer"><small><span>.</span><span> / </span><span>6 hours, 36 minutes ago</span></small></div>
<div class="panel-body"><em>Caller Type: Scam</em><br/><br/>said she was calling from Telstra about a security issue on my internet. Even gave me a telephone number to call if I didn't believe her, and a 'Telstra Employee' number too!</div>
</div>
<div class="panel panel-default">
<div class="panel-heading"><a href="/0412893171">0412 893 171</a></div>
<div class="panel-footer"><small><span>Anon</span><span> / </span><span>6 hours, 58 minutes ago</span></small></div>
<div class="panel-body"><em>Caller Type: Scam</em><br/><br/>Automated scam called allegedly from Services Australia suggesting issue with tax number and imminent arrest.</div>
</div>
<div class="panel panel-default">
<div class="panel-heading"><a href="/0400463574">0400 463 574</a></div>
<div class="panel-footer"><small><span>Vanessa Rogers</span><span> / </span><span>7 hours, 6 minutes ago</span></small></div>
<div class="panel-body"><em>Caller Type: Unknown</em><br/><br/>They (a male) left a v/mail saying all my assets would be frozen if I didn't return call and that there would be legal/criminal ramifications?? Pretty sure it's a scam!  </div>
</div>
</div>
</div>
<div class="col-lg-4">
<h2>Last Checked Numbers</h2>
<div class="panel-group">
<div class="panel panel-default">
<div class="panel-heading"><a href="/0297630057">(02) 9763 0057</a></div>
<div class="panel-footer"><small>Liverpool Sydney
</small></div>
</div>
<div class="panel panel-default">
<div class="panel-heading"><a href="/0391122032">(03) 9112 2032</a></div>
<div class="panel-footer"><small>Melbourne City
</small></div>
</div>
<div class="panel panel-default">
<div class="panel-heading"><a href="/0404886632">0404 886 632</a></div>
<div class="panel-footer"><small>Australia</small></div>
</div>
<div class="panel panel-default">
<div class="panel-heading"><a href="/0403822871">0403 822 871</a></div>
<div class="panel-footer"><small>Australia</small></div>
</div>
<div class="panel panel-default">
<div class="panel-heading"><a href="/0428381041">0428 381 041</a></div>
<div class="panel-footer"><small>Australia</small></div>
</div>
<div class="panel panel-default">
<div class="panel-heading"><a href="/0429555010">0429 555 010</a></div>
<div class="panel-footer"><small>Australia</small></div>
</div>
<div class="panel panel-default">
<div class="panel-heading"><a href="/0423471098">0423 471 098</a></div>
<div class="panel-footer"><small>Australia</small></div>
</div>
<div class="panel panel-default">
<div class="panel-heading"><a href="/0414377000">0414 377 000</a></div>
<div class="panel-footer"><small>Australia</small></div>
</div>
<div class="panel panel-default">
<div class="panel-heading"><a href="/0413053601">0413 053 601</a></div>
<div class="panel-footer"><small>Australia</small></div>
</div>
<div class="panel panel-default">
<div class="panel-heading"><a href="/0297282529">(02) 9728 2529</a></div>
<div class="panel-footer"><small>West Sydney
</small></div>
</div>
</div>
</div>
<div class="col-lg-12">
<h2>Landline Phone Numbers</h2>
<a class="btn btn-primary" href="/02">(02)</a>
<a class="btn btn-primary" href="/03">(03)</a>
<a class="btn btn-primary" href="/07">(07)</a>
<a class="btn btn-primary" href="/08">(08)</a>
<h2>Mobile Phone Numbers</h2>
<a class="btn btn-primary" href="/0400">0400</a>
<a class="btn btn-primary" href="/0401">0401</a>
<a class="btn btn-primary" href="/0402">0402</a>
<a class="btn btn-primary" href="/0403">0403</a>
<a class="btn btn-primary" href="/0404">0404</a>
<a class="btn btn-primary" href="/0405">0405</a>
<a class="btn btn-primary" href="/0406">0406</a>
<a class="btn btn-primary" href="/0407">0407</a>
<a class="btn btn-primary" href="/0408">0408</a>
<a class="btn btn-primary" href="/0409">0409</a>
<a class="btn btn-primary" href="/0410">0410</a>
<a class="btn btn-primary" href="/0411">0411</a>
<a class="btn btn-primary" href="/0412">0412</a>
<a class="btn btn-primary" href="/0413">0413</a>
<a class="btn btn-primary" href="/0414">0414</a>
<a class="btn btn-primary" href="/0415">0415</a>
<a class="btn btn-primary" href="/0416">0416</a>
<a class="btn btn-primary" href="/0417">0417</a>
<a class="btn btn-primary" href="/0418">0418</a>
<a class="btn btn-primary" href="/0419">0419</a>
<a class="btn btn-primary" href="/0420">0420</a>
<a class="btn btn-primary" href="/0421">0421</a>
<a class="btn btn-primary" href="/0422">0422</a>
<a class="btn btn-primary" href="/0423">0423</a>
<a class="btn btn-primary" href="/0424">0424</a>
<a class="btn btn-primary" href="/0425">0425</a>
<a class="btn btn-primary" href="/0426">0426</a>
<a class="btn btn-primary" href="/0427">0427</a>
<a class="btn btn-primary" href="/0428">0428</a>
<a class="btn btn-primary" href="/0429">0429</a>
<a class="btn btn-primary" href="/0430">0430</a>
<a class="btn btn-primary" href="/0431">0431</a>
<a class="btn btn-primary" href="/0432">0432</a>
<a class="btn btn-primary" href="/0433">0433</a>
<a class="btn btn-primary" href="/0434">0434</a>
<a class="btn btn-primary" href="/0435">0435</a>
<a class="btn btn-primary" href="/0437">0437</a>
<a class="btn btn-primary" href="/0438">0438</a>
<a class="btn btn-primary" href="/0439">0439</a>
<a class="btn btn-primary" href="/0444">0444</a>
<a class="btn btn-primary" href="/0447">0447</a>
<a class="btn btn-primary" href="/0448">0448</a>
<a class="btn btn-primary" href="/0449">0449</a>
<a class="btn btn-primary" href="/0450">0450</a>
<a class="btn btn-primary" href="/0451">0451</a>
<a class="btn btn-primary" href="/0452">0452</a>
<a class="btn btn-primary" href="/0455">0455</a>
<a class="btn btn-primary" href="/0456">0456</a>
<a class="btn btn-primary" href="/0457">0457</a>
<a class="btn btn-primary" href="/0458">0458</a>
<a class="btn btn-primary" href="/0459">0459</a>
<a class="btn btn-primary" href="/0466">0466</a>
<a class="btn btn-primary" href="/0467">0467</a>
<a class="btn btn-primary" href="/0468">0468</a>
<a class="btn btn-primary" href="/0469">0469</a>
<a class="btn btn-primary" href="/0470">0470</a>
<a class="btn btn-primary" href="/0473">0473</a>
<a class="btn btn-primary" href="/0474">0474</a>
<a class="btn btn-primary" href="/0475">0475</a>
<a class="btn btn-primary" href="/0476">0476</a>
<a class="btn btn-primary" href="/0477">0477</a>
<a class="btn btn-primary" href="/0478">0478</a>
<a class="btn btn-primary" href="/0479">0479</a>
<a class="btn btn-primary" href="/0481">0481</a>
<a class="btn btn-primary" href="/0482">0482</a>
<a class="btn btn-primary" href="/0484">0484</a>
<a class="btn btn-primary" href="/0487">0487</a>
<a class="btn btn-primary" href="/0488">0488</a>
<a class="btn btn-primary" href="/0490">0490</a>
<a class="btn btn-primary" href="/0491">0491</a>
<a class="btn btn-primary" href="/0497">0497</a>
<a class="btn btn-primary" href="/0498">0498</a>
<a class="btn btn-primary" href="/0499">0499</a>
</div>
</div><!-- /row -->
<div class="adsense">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- aucaller-front-2 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-2913233292297309" data-ad-format="auto" data-ad-slot="1367227967" style="display:block"></ins>
<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
</div>
</div><!-- /container --><!-- +++++ Footer Section +++++ -->
<div id="footer">
<div class="container">
<div class="row">
<div class="col-lg-3">
<h4>Contact Us</h4>
<p>
                    If you have any questions, comments or suggestions please <a href="/cdn-cgi/l/email-protection#2a4b5f494b46464f58044945476a4d474b434604494547">Contact Us</a>. You can also find us on <a href="https://www.facebook.com/aucaller/">Facebook</a>
</p>
</div><!-- /col-lg-4 -->
<div class="col-lg-5">
<h4>Our Links</h4>
<p>
<a href="https://www.aucaller.com/privacy">Privacy Policy</a><br/>
<a href="https://www.aucaller.com/terms">Terms and Conditions of Service</a><br/>
<a href="/cdn-cgi/l/email-protection#a0c1d5c3c1ccccc5d28ec3cfcde0c7cdc1c9cc8ec3cfcd">Report a Violation of Our Terms and Conditions of Service</a><br/>
<a href="https://www.donotcall.gov.au/">Do Not Call Register</a><br/>
</p>
</div><!-- /col-lg-4 -->
<div class="col-lg-4">
<h4>About Us</h4>
<p><a href="https://www.aucaller.com/">AUCaller.com</a> helps you to find caller information by using our free reverse phone lookup database.</p>
</div><!-- /col-lg-4 -->
</div>
</div>
</div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/js/bootstrap.min.js"></script>
<script>
    $( "#search" ).submit(function( event ) {
        $.ajax({
            type: 'POST',
            url: '/include/search.php',
            data: {
                'name' : $( "input[name='name']" ).val()
            },
            success: function(data){
                if(data.charAt(0) == 'h')
                {
                    window.location.href = data;
                }
                else
                {
                    document.getElementById("klaidasearch").innerHTML = data;
                }
            }
        });
        event.preventDefault();
    });
</script>
</body>
</html>
