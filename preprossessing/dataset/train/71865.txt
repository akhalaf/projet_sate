<!DOCTYPE html>
<html lang="en">
<head>
<title>Active24: Domains and professional web hosting</title>
<meta content="Your professional partner for domains, hosting and emails." name="description"/>
<link href="node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="node_modules/bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet"/>
<link href="custom.css" rel="stylesheet"/>
<link href="node_modules/bootstrap/dist/js/bootstrap.js" rel="script"/>
<link href="//www.active24.cz/webs/active24/images/favicon.ico" rel="shortcut icon"/>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-3476561-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<!-- Background -->
<!-- contents-->
<div class="container">
<!-- head -->
<div class="headLogo col-md-12 col-sm-12">
<div class="logoActive">
<img class='logoImage"' src="images/activeLogo.png"/>
<p class="logoText">ACTIVE 24 - your professional partner for domains, hosting and emails</p>
</div>
<img class="mapEurope" src="images/mapa.png"/>
</div>
<!-- States -->
<div class="states col-md-12 col-sm-12">
<div class="logoState firstState col-md-3 col-sm-4">
<div class="upperState">
<a href="https://www.active24.cz"><img src="images/czLogo.png"/></a>
</div>
<div class="bottomState">
<a class="activeBtn" href="https://www.active24.cz">
<span class="innerA"><span class="innerB"><span class="inner">WWW.ACTIVE24.CZ</span></span></span></a><br/>
<a class="stateSupport" href="https://www.active24.cz/klientska-zona/">Support</a>
</div>
</div>
<div class="logoState col-md-3 col-sm-4">
<div class="upperState">
<a href="https://www.active24.co.uk"><img src="images/ukLogo.png"/></a>
</div>
<div class="bottomState">
<a class="activeBtn" href="https://www.active24.co.uk">
<span class="innerA"><span class="innerB"><span class="inner">WWW.ACTIVE24.CO.UK</span></span></span>
</a><br/>
<a class="stateSupport" href="https://www.active24.co.uk/support/">Support</a>
</div>
</div>
<div class="logoState col-md-3 col-sm-4">
<div class="upperState">
<a href="https://www.active24.nl"><img src="images/nlLogo.png"/></a>
</div>
<div class="bottomState">
<a class="activeBtn" href="https://www.active24.nl">
<span class="innerA"><span class="innerB"><span class="inner">WWW.ACTIVE24.NL</span></span></span>
</a><br/>
<a class="stateSupport" href="https://www.active24.nl/support/">Support</a>
</div>
</div>
<div class="logoState col-md-3 col-sm-4">
<div class="upperState">
<a href="https://www.active24.sk"><img src="images/skLogo.png"/></a>
</div>
<div class="bottomState">
<a class="activeBtn" href="https://www.active24.sk">
<span class="innerA"><span class="innerB"><span class="inner">WWW.ACTIVE24.SK</span></span></span>
</a><br/>
<a class="stateSupport" href="https://www.active24.sk/klientska-zona/">Support</a>
</div>
</div>
<div class="logoState col-md-3 col-sm-4">
<div class="upperState">
<a href="https://www.active24.de"><img src="images/deLogo.png"/></a>
</div>
<div class="bottomState">
<a class="activeBtn" href="https://www.active24.de">
<span class="innerA"><span class="innerB"><span class="inner">WWW.ACTIVE24.DE</span></span></span>
</a><br/>
<a class="stateSupport" href="https://www.active24.de/support/">Support</a>
</div>
</div>
<div class="logoState firstState col-md-3 col-sm-4">
<div class="upperState">
<a href="https://www.active24.es"><img src="images/esLogo.png"/></a>
</div>
<div class="bottomState">
<a class="activeBtn" href="https://www.active24.es">
<span class="innerA"><span class="innerB"><span class="inner">WWW.ACTIVE24.ES</span></span></span>
</a><br/>
<a class="stateSupport" href="https://www.active24.es/support/">Support</a>
</div>
</div>
<div class="logoState col-md-3 col-sm-4">
<div class="upperState">
<a href="https://www.active24.cat"><img src="images/catLogo.png"/></a>
</div>
<div class="bottomState">
<a class="activeBtn" href="https://www.active24.cat">
<span class="innerA"><span class="innerB"><span class="inner">WWW.ACTIVE24.CAT</span></span></span>
</a><br/>
<a class="stateSupport" href="https://www.active24.cat/support/">Support</a>
</div>
</div>
</div>
<!-- footer -->
<div class="footer">
<div class="footerCopyright">Copyright 2017 Active&amp;nbsp24&amp;nbsps.r.o.</div>
</div>
</div>
</body>
</html>
