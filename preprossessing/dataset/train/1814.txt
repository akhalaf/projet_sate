<!DOCTYPE html>
<html dir="ltr" lang="ja">
<head>
<meta charset="utf-8"/>
<meta content="index,follow" name="robots"/>
<meta content="width=device-width,initial-scale=1.0" name="viewport"/>
<meta content="#000000" name="theme-color"/>
<link href="https://www.1-firststep.com/wp-content/themes/firststep/image/favicon.png" rel="icon" type="image/png"/>
<link href="https://www.1-firststep.com/wp-content/themes/firststep/image/apple-touch-icon.png" rel="apple-touch-icon"/>
<link href="https://www.1-firststep.com/wp-content/themes/firststep/css/style-sp-minify.css?date=20210110" media="screen and ( max-width:1079px )" rel="stylesheet"/>
<link href="https://www.1-firststep.com/wp-content/themes/firststep/css/style-pc-minify.css?date=20210110" media="screen and ( min-width:1080px )" rel="stylesheet"/>
<link href="https://www.1-firststep.com/wp-content/themes/firststep/css/front-page-sp-minify.css?date=20210110" media="screen and ( max-width:1079px )" rel="stylesheet"/>
<link href="https://www.1-firststep.com/wp-content/themes/firststep/css/front-page-pc-minify.css?date=20210110" media="screen and ( min-width:1080px )" rel="stylesheet"/>
<title>ホームページ制作とSEO対策なら岡崎市のファーストステップ</title>
<meta content="クリックしたらすぐに表示される爆速なホームページ制作。すべてはアクセスを増やして集客し、お問い合わせや売上に繋げるためです。WordPressサイトの制作、SEO対策、プログラム開発。メールフォームやBBSのダウンロードなど。" name="description"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="ホームページ制作とSEO対策なら岡崎市のファーストステップ" name="twitter:title"/>
<meta content="クリックしたらすぐに表示される爆速なホームページ制作。すべてはアクセスを増やして集客し、お問い合わせや売上に繋げるためです。WordPressサイトの制作、SEO対策、プログラム開発。メールフォームやBBSのダウンロードなど。" name="twitter:description"/>
<meta content="https://www.1-firststep.com/wp-content/themes/firststep/image/slider-5-1080.jpg" name="twitter:image"/>
<link href="https://www.1-firststep.com/" rel="canonical"/>
</head>
<body class="home page-template-default page page-id-756 toppage">
<div id="header">
<div>
<h1><a href="https://www.1-firststep.com"><img alt="ホームページ制作とSEO対策なら岡崎市のファーストステップ" decoding="async" height="60" loading="lazy" src="https://www.1-firststep.com/wp-content/themes/firststep/image/logo.svg" width="260"/></a></h1>
<ul id="menu">
<li>
<p><a href="https://www.1-firststep.com/service">提供サービス</a></p>
<ol>
<li><a href="https://www.1-firststep.com/service/homepage">ホームページ制作</a></li>
<li><a href="https://www.1-firststep.com/service/smartphone">スマホ対応化</a></li>
<li><a href="https://www.1-firststep.com/service/maintenance">維持・管理</a></li>
<li><a href="https://www.1-firststep.com/service/seo">SEO対策</a></li>
<li><a href="https://www.1-firststep.com/service/pagespeed">表示速度高速化</a></li>
<li><a href="https://www.1-firststep.com/archives/operation">サイト運用事例</a></li>
<li><a href="https://www.1-firststep.com/archives/category/software-development/program-support">プログラムの利用サポート</a></li>
</ol>
</li>
<li>
<p><a href="https://www.1-firststep.com/archives/category/website-creation">ホームページ制作</a></p>
<ol>
<li><a href="https://www.1-firststep.com/archives/category/website-creation/howto">技術情報</a></li>
<li><a href="https://www.1-firststep.com/archives/category/website-creation/old-post">昔の情報</a></li>
</ol>
</li>
<li>
<p><a href="https://www.1-firststep.com/archives/category/software-development">プログラム開発</a></p>
<ol>
<li><a href="https://www.1-firststep.com/archives/category/software-development/responsive-mailform">メールフォーム</a></li>
<li><a href="https://www.1-firststep.com/archives/category/software-development/contents-maker">コンテンツ メーカー</a></li>
<li><a href="https://www.1-firststep.com/archives/category/software-development/responsive-bbs">レスポンシブBBS</a></li>
<li><a href="https://www.1-firststep.com/archives/category/software-development/thread-bbs">スレッド式BBS</a></li>
<li><a href="https://www.1-firststep.com/archives/category/software-development/business-calendar">ビジネスカレンダー</a></li>
<li><a href="https://www.1-firststep.com/archives/category/software-development/pagespeed-timelog">ページスピード タイムログ</a></li>
<li><a href="https://www.1-firststep.com/archives/category/software-development/small-script">小規模なスクリプト</a></li>
<li><a href="https://www.1-firststep.com/archives/category/software-development/other-program">その他のプログラム</a></li>
</ol>
</li>
<li>
<p><a href="https://www.1-firststep.com/archives/category/seo">SEO</a></p>
<ol>
<li><a href="https://www.1-firststep.com/archives/category/seo/marketing">集客</a></li>
<li><a href="https://www.1-firststep.com/archives/category/seo/analytics">アクセス解析</a></li>
</ol>
</li>
<li>
<p><a href="https://www.1-firststep.com/developer">制作者情報</a></p>
<ol>
<li><a href="https://www.1-firststep.com/developer/firstvisit">初めてのご訪問の方へ</a></li>
<li><a href="https://www.1-firststep.com/developer/summary">制作事務所の概要</a></li>
</ol>
</li>
<li>
<p><a href="https://www.1-firststep.com/contact">お問い合わせ</a></p>
<ol>
<li><a href="https://www.1-firststep.com/contact/quote">転載・引用について</a></li>
<li><a href="https://www.1-firststep.com/contact/inquiry">問い合わせフォーム</a></li>
<li><a href="https://www.1-firststep.com/samplephp/support-bbs/" target="_blank">サポートBBS</a></li>
</ol>
</li>
</ul>
<div class="sp-open" id="sp-icon"><span></span></div>
</div>
</div><!-- header -->
<div id="slider">
<span id="pagespeed-timelog"></span>
<p><img alt="" height="550" sizes="100vw" src="https://www.1-firststep.com/wp-content/themes/firststep/image/slider-1-1200.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/slider-1-640.jpg 640w, https://www.1-firststep.com/wp-content/themes/firststep/image/slider-1-750.jpg 750w, https://www.1-firststep.com/wp-content/themes/firststep/image/slider-1-828.jpg 828w, https://www.1-firststep.com/wp-content/themes/firststep/image/slider-1-945.jpg 945w, https://www.1-firststep.com/wp-content/themes/firststep/image/slider-1-1080.jpg 1080w, https://www.1-firststep.com/wp-content/themes/firststep/image/slider-1-1200.jpg 1200w, https://www.1-firststep.com/wp-content/themes/firststep/image/slider-1-1250.jpg 1250w, https://www.1-firststep.com/wp-content/themes/firststep/image/slider-1-1440.jpg 1440w, https://www.1-firststep.com/wp-content/themes/firststep/image/slider-1-1600.jpg 1600w, https://www.1-firststep.com/wp-content/themes/firststep/image/slider-1.jpg 2000w" width="1200"/></p>
</div><!-- slider -->
<div id="page-speed">
<div>
<h2>表示が遅いサイトは<span>見込客を逃しています</span></h2>
<div class="speed-image">
<p><img alt="Web制作のファーストステップ" decoding="async" height="228" loading="lazy" sizes="(max-width: 767px) 90vw, (max-width: 1079px) 38vw, 350px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/page-speed3-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/page-speed3-1000.jpg 1000w, https://www.1-firststep.com/wp-content/themes/firststep/image/page-speed3-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/page-speed3-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/page-speed3-700.jpg 700w, https://www.1-firststep.com/wp-content/themes/firststep/image/page-speed3-750.jpg 750w" width="350"/></p>
</div>
<div class="speed-text">
<p>芸能人は歯が命<span class="red">※1</span>であるように、Webサイトは表示速度が命である。<br/>表示が遅いサイトは多くの見込客を逃しています。</p>
<p>モバイルからのアクセスでは「ページが完全に表示されるまでに3秒以上かかると、53％の人はページを離れる」という<a href="https://developers-jp.googleblog.com/2017/03/new-industry-benchmarks-for-mobile-page-speed.html" rel="noopener noreferrer" target="_blank">Googleの2017年の調査結果</a>もあるなど、表示速度はWebサイトを人に見てもらうためのとても重要な要素なのです。</p>
<ol>
<li><span class="red">※1</span><a href="https://youtu.be/5GRqSWIe0iI" rel="noopener noreferrer" target="_blank">アパガードのCM</a>より引用</li>
</ol>
</div>
<h2>自分のサイトの遅さには<span>気付きにくいものです</span></h2>
<div class="speed-text">
<p>一度閲覧したページは「キャッシュ」と呼ばれる履歴のようなデータがブラウザに保存され、次回からの閲覧の際はキャッシュを使用して表示するため、その分のダウンロード時間が省略されて本来よりも速く表示されます。</p>
<p>しかし、初めて訪問する人はそのサイトのキャッシュを事前に保存していないので、本来の表示速度が問われることになります。<br/>つまり、最初の1ページ目の表示が最も時間がかかります。</p>
<p><span class="yellow">もし最初の1ページ目の表示が遅かったら、あなたのサイトは1ページすら見てもらうことができずに見込客を逃してしまうことになるのです。</span><br/>前述の調査結果に基づけば、53％の新規見込客を逃していることになります。(モバイルからのアクセスの場合)</p>
</div>
<h2>綺麗なデザインでも<span>表示が遅いと意味がない</span></h2>
<div class="speed-text">
<p>しかし現実には、表示の速いサイトを作ることができる業者はあまりありません。<br/>サイトのデザインが綺麗だとしても、表示される前に閉じられてしまい見てもらえなかったら意味がないのです。</p>
<p>以下の<span class="red">※2</span>～<span class="red">※4</span>などを参考にし、ぜひ色々なサイトの表示速度を計測・比較してみてください。<br/>今ご覧になっているこのサイト以上に速いサイトは滅多に存在しないでしょう。<br/>WordPressサイトとしては世界最速レベルと呼んでください。(笑)</p>
<ol>
<li><span class="red">※2</span><a href="https://www.1-firststep.com/archives/8305">キャッシュを無効にしてページ表示速度を計測する方法</a></li>
<li><span class="red">※3</span><a href="https://developers.google.com/speed/pagespeed/insights/?hl=ja" rel="noopener noreferrer" target="_blank">PageSpeed Insights</a></li>
<li><span class="red">※4</span><a href="https://tools.pingdom.com/" rel="noopener noreferrer" target="_blank">Pingdom Tools</a></li>
</ol>
</div>
</div>
</div><!-- page-speed -->
<div id="program-info">
<h2>当サイトで配布している<span>プログラム一覧</span></h2>
<p>当サイトでは人や世の中の役に立つプログラムを制作・公開しています。<br/>開発者本人である私から見ても、どれも使いやすくて良いプログラムばかりだと思っております。</p>
<p>あなたのサイトに役に立つものを見つけて、ぜひ使ってみてください。<br/>このサイトで実際に使用しているプログラムは<a href="https://www.1-firststep.com/archives/4633">こちらのページ</a>で紹介しています。</p>
</div><!-- program-info -->
<div id="program-list">
<ul>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/462">レスポンシブ<span>メールフォーム</span></a></h3>
<p><img alt="レスポンシブ メールフォーム" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu7-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu7-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu7-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu7-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu7-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu7-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu7-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu7-750.jpg 750w" width="340"/></p>
</div>
<p>「レスポンシブ メールフォーム」は設置の手順が少なく、とても使い勝手の良いメールフォームプログラムです。無料でダウンロードできます。</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/614">コンテンツ<span>メーカー</span></a></h3>
<p><img alt="コンテンツ メーカー" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu10-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu10-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu10-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu10-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu10-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu10-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu10-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu10-750.jpg 750w" width="340"/></p>
</div>
<p>「コンテンツ メーカー」はウェブサイトの新着情報欄の更新を手軽にするためのプログラムです。HTMLページにも組み込むことが可能です。</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/2934">レスポンシブ<span>BBS</span></a></h3>
<p><img alt="レスポンシブBBS" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu8-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu8-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu8-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu8-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu8-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu8-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu8-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu8-750.jpg 750w" width="340"/></p>
</div>
<p>「レスポンシブBBS」は設置がとても簡単で使い勝手の良いBBSです。画像アップロードや返信機能を後からいつでも追加することができます。</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/6609">スレッド式<span>BBS</span></a></h3>
<p><img alt="スレッド式BBS" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu9-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu9-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu9-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu9-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu9-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu9-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu9-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu9-750.jpg 750w" width="340"/></p>
</div>
<p>「スレッド式BBS」は当サイトのサポートBBSでも使用しているプログラムです。すべての機能が最初から組み込まれています。</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/3279">ビジネス<span>カレンダー</span></a></h3>
<p><img alt="ビジネス カレンダー" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu11-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu11-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu11-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu11-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu11-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu11-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu11-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu11-750.jpg 750w" width="340"/></p>
</div>
<p>「ビジネス カレンダー」は定休日や営業日を表示できるカレンダープログラムです。複数の月を同時に表示させることもできます。</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/8480">ページスピード<span>タイムログ</span></a></h3>
<p><img alt="ページスピード タイムログ" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu26-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu26-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu26-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu26-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu26-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu26-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu26-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu26-750.jpg 750w" width="340"/></p>
</div>
<p>「ページスピード タイムログ」はページ表示にかかった時間(秒数)をそのページ上に表示・記録するプログラムです。</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/6169">スマホメニュー<span>スクリプト</span></a></h3>
<p><img alt="スマホメニュー スクリプト" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu12-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu12-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu12-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu12-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu12-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu12-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu12-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu12-750.jpg 750w" width="340"/></p>
</div>
<p>「スマホメニュー スクリプト」はドロップダウン式(縦方向の折り畳み式)のスマホ用メニューを簡単に設置できるスクリプトです。</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/1226">ソーシャルボタン<span>スクリプト</span></a></h3>
<p><img alt="ソーシャルボタン スクリプト" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu23-350.png" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu23-300.png 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu23-350.png 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu23-400.png 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu23-500.png 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu23-550.png 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu23-680.png 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu23-750.png 750w" width="340"/></p>
</div>
<p>「ソーシャルボタン スクリプト」は主要SNSのシェアボタンをまとめて設置するためのスクリプトです。とても軽量で表示が速いです。</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/6760">タブスイッチ<span>スクリプト</span></a></h3>
<p><img alt="タブスイッチ スクリプト" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu16-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu16-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu16-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu16-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu16-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu16-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu16-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu16-750.jpg 750w" width="340"/></p>
</div>
<p>「タブスイッチ スクリプト」はタブをクリックすることで、ウェブサイト上のとある領域の表示を切り替えるためのスクリプトです。</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/8289">スライドダウン<span>スクリプト</span></a></h3>
<p><img alt="スライドダウン スクリプト" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu25-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu25-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu25-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu25-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu25-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu25-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu25-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu25-750.jpg 750w" width="340"/></p>
</div>
<p>「スライドダウン スクリプト」はページ内にある見出しをクリックした際に、本文をスライドダウンで表示させるためのスクリプトです。</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/6870">リンクスクロール<span>スクリプト</span></a></h3>
<p><img alt="リンクスクロール スクリプト" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu17-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu17-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu17-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu17-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu17-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu17-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu17-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu17-750.jpg 750w" width="340"/></p>
</div>
<p>「リンクスクロール スクリプト」はページ内リンクにスクロールのアニメーションを簡単に付加できるスクリプトです。</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/7011">ページトップ<span>スクリプト</span></a></h3>
<p><img alt="ページトップ スクリプト" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu18-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu18-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu18-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu18-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu18-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu18-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu18-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu18-750.jpg 750w" width="340"/></p>
</div>
<p>「ページトップ スクリプト」はクリックするとページ最上部にアニメーションで戻るボタンを設置するスクリプトです。</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/3363">ランダムイメージ<span>スクリプト</span></a></h3>
<p><img alt="ランダムイメージ スクリプト" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu14-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu14-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu14-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu14-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu14-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu14-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu14-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu14-750.jpg 750w" width="340"/></p>
</div>
<p>「ランダムイメージ スクリプト」はあらかじめ設定しておいた複数の画像の中から1枚をランダムで表示するためのスクリプトです。</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/7194">フェードイメージ<span>スクリプト</span></a></h3>
<p><img alt="フェードイメージ スクリプト" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu15-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu15-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu15-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu15-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu15-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu15-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu15-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu15-750.jpg 750w" width="340"/></p>
</div>
<p>「フェードイメージ スクリプト」はマウスカーソルを画像の上に乗せた際に、画像をふわっと(フェード演出)切り替えるスクリプトです。</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/266">ズームボックス<span>スクリプト</span></a></h3>
<p><img alt="ズームボックス スクリプト" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu13-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu13-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu13-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu13-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu13-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu13-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu13-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu13-750.jpg 750w" width="340"/></p>
</div>
<p>「ズームボックス スクリプト」は画像リンクをクリックした際に、ズーム演出で大きな画像を表示するスクリプトです。</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/3113">カスタマーズ<span>マネージ</span></a></h3>
<p><img alt="カスタマーズ マネージ" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu20-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu20-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu20-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu20-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu20-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu20-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu20-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu20-750.jpg 750w" width="340"/></p>
</div>
<p>「カスタマーズ マネージ」はサーバ上に顧客情報を保存することで、複数店舗間での一元管理ができるプログラムです。(現時点では配布終了)</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/3807">レスポンシブ<span>アンケート</span></a></h3>
<p><img alt="レスポンシブ アンケート" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu24-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu24-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu24-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu24-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu24-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu24-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu24-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu24-750.jpg 750w" width="340"/></p>
</div>
<p>「レスポンシブ アンケート」はスマホ表示にも対応したアンケートフォームです。集計結果をCSVファイルでダウンロードできます。(現時点では配布終了)</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/5380">メルマガ<span>独自配信システム</span></a></h3>
<p><img alt="メルマガ 独自配信システム" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu19-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu19-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu19-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu19-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu19-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu19-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu19-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu19-750.jpg 750w" width="340"/></p>
</div>
<p>「メルマガ 独自配信システム」は自分のサーバーからメルマガを配信するためのプログラムです。(現時点では未発売)</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/4622">診療予約<span>システム</span></a></h3>
<p><img alt="診療予約システム" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu21-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu21-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu21-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu21-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu21-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu21-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu21-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu21-750.jpg 750w" width="340"/></p>
</div>
<p>「診療予約システム」はネットから予約を入れてもらい、順番制で受付・管理するタイプのシステムとなります。(現時点では未発売)</p>
</li>
<li>
<div>
<h3><a href="https://www.1-firststep.com/archives/9749">QRコード<span>ジェネレーター</span></a></h3>
<p><img alt="QRコード ジェネレーター" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu27-350.jpg" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu27-300.jpg 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu27-350.jpg 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu27-400.jpg 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu27-500.jpg 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu27-550.jpg 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu27-680.jpg 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu27-750.jpg 750w" width="340"/></p>
</div>
<p>「QRコード ジェネレーター」は誰でも簡単にQRコードを作ることができるプログラムです。アカウント登録不要で利用できます。</p>
</li>
<li>
<div>
<h3><a href="https://unko.1-firststep.com/" rel="noopener noreferrer" target="_blank">うんこタッチ<span>ゲーム</span></a></h3>
<p><img alt="うんこタッチゲーム" decoding="async" height="215" loading="lazy" sizes="(max-width: 767px) 46vw, (max-width: 1079px) 30vw, 340px" src="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu22-350.png" srcset="https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu22-300.png 300w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu22-350.png 350w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu22-400.png 400w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu22-500.png 500w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu22-550.png 550w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu22-680.png 680w, https://www.1-firststep.com/wp-content/themes/firststep/image/indexmenu22-750.png 750w" width="340"/></p>
</div>
<p>「うんこタッチゲーム」は画面上に降り注ぐウンコをどれだけたくさん触ることができるかを競うブラウザゲームです。(現在メンテナンス中)</p>
</li>
</ul>
</div><!-- program-list -->
<div id="info-list">
<div>
<div id="blog-info">
<h3>ブログ新着情報</h3>
<ul>
</ul>
<p><a href="https://www.1-firststep.com/bloglist">ブログ一覧へ</a></p>
</div>
<div id="update-info">
<h3>アップデート情報</h3>
<div id="news"></div>
</div>
</div>
</div><!-- info-list -->
<div id="footer-logo">
<div>
<h4><a href="https://www.1-firststep.com"><img alt="ホームページ制作とSEO対策なら愛知県岡崎市のファーストステップ" decoding="async" height="60" loading="lazy" src="https://www.1-firststep.com/wp-content/themes/firststep/image/logo.svg" width="260"/></a></h4>
<p>ファーストステップでは人や世の中の役に立つプログラムを作っています。<br/>ぜひ色々使ってみて、改善点や要望などがあればお申し出ください。</p>
</div>
</div><!-- footer-logo -->
<div id="support">
<h5>提供サポートなど</h5>
<ul>
</ul>
</div><!-- support -->
<div id="footer-menu">
</div><!-- footer-menu -->
<div id="footer">
<div>
<p>Copyright © 2006-2020 <a href="https://www.1-firststep.com">FIRSTSTEP</a> <span>All Rights Reserved.</span></p>
</div>
</div><!-- footer -->
<div id="pagetop-scroll"><span></span></div>
<script async="async" id="firststep-js" src="https://www.1-firststep.com/wp-content/themes/firststep/js/firststep-minify.js?date=20201217"></script>
</body>
</html>