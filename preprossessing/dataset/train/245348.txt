<!DOCTYPE html>
<html lang="en">
<head>
<title>Free Online Crossword Puzzles</title>
<meta content="crossword puzzles,crossword,free crossword puzzles,free online crossword puzzles,free crosswords,crossword puzzle app,crossword puzzle software" name="keywords"/><meta content="index" name="robots"/>
<meta content="The world's largest supply of crossword puzzles, playable for free online.  Tablet and phone friendly." name="description"/>
<style>
a:link, a:hover{color:#000084;}
a:Visited{color:#800080;}
body{background-color:white;color:black;border:2px solid #000084;font-family:Verdana,Arial,sans-serif;font-size:12px;margin-bottom:10px;margin-left:auto;margin-right:auto;margin-top:10px;width:773px;}
form{margin:0px;padding:0px;}
h2{color:#000084;font-size:22px;}
h4{color:#000084;font-size:14px;}
img{border:0px;margin:0px;}
li{margin-bottom:3px;margin-top:3px;}
ol{margin-left:24px;padding-left:0px;}
p, li{line-height:120%}
ul{margin-left:20px;padding-left:0px;}
div.narrowcontent{margin-left:10px;margin-right:202px;}
div.widecontent{margin-left:10px;margin-right:10px;}
input.button{height:20px;}
p.error{color:red;}
td.editableobject{background-color:#EEEEFF;border:1px solid #DDDDFF;padding-bottom:2px;padding-left:2px;padding-right:2px;padding-top:2px;}
td.homeblock{border:1px solid #000084;padding-bottom:10px;padding-left:10px;padding-right:10px;padding-top:10px;}
p.homeblockfooter{clear:left;margin-bottom:3px;margin-top:0px;}
#logo{padding-bottom:10px;padding-left:50px;padding-top:10px;}
#navbar{height:20px;padding-bottom:4px;padding-top:10px;text-align:center;width:100%;background-color:#000084;}
#navbar a{color:white;font-weight:bold;margin-left:0px;margin-right:0px;padding-left:20px;padding-right:20px;padding-top:3px;padding-bottom:3px;text-decoration:none;}
#navbar a.current, #navbar a:hover{color:#8080C2;}
#footer{padding-bottom:8px;padding-top:8px;width:100%;}
#footer img{padding-left:10px;text-align:left;}
#footer a{padding-left:10px;}
#footerline{height:1px;margin-top:20px;width:100%;background-color:#000084;}
#copyright{padding-right:10px;text-align:right;}
</style>
<script type="text/javascript">
function popup(url, width, height) {
  options = 'width=' + width + ',height=' + height + ',resizable,scrollbars';
  win = window.open(url, "popupWindow", options);
  win.opener = self;
}
</script>
</head>
<body>
<img height="129" id="logo" src="https://www.boatloadpuzzles.com/img/logo.png" width="477"/>
<div id="navbar"><a class="first current" href="https://www.boatloadpuzzles.com/playcrossword">Play Online</a><a href="https://www.boatloadpuzzles.com/crosswords">Apps</a><a href="https://www.boatloadpuzzles.com/dailycrossword">Web Gadget</a><a href="https://www.boatloadpuzzles.com/support">Support</a></div>
<div class="widecontent"><p></p><div align="center">
<ins class="adsbygoogle" data-ad-client="ca-pub-8904987435081556" data-ad-slot="1917106919" style="display:block;width:728px;height:90px;"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
<h2>Solve Boatload Puzzles' Free Online Crossword Puzzles</h2></div>
<div class="narrowcontent">
</div><div class="widecontent">
<table cellpadding="0" cellspacing="0"><tr>
<td valign="top" width="575">
<p>Boatload Puzzles is the home of the world's largest supply
of crossword puzzles. Solve Boatload Puzzles'
40,000 free online crossword puzzles below.  No registration is required.</p>
<script type="text/javascript">
boatload_puzzles_grid_size=13;
boatload_puzzles_puz_num=17326;
boatload_puzzles_puz_data='4A8F1A37013A50CA9E9CF97AAC553A1D91E4DD1AF6B67261BAC3C7F325AB79A0'
  + '0AA64D9131C11E68D165A59E08817FDCF25425B6F46DE16568B8343DE99FB3F3'
  + '752111AC3FCC7F060B7DD458426ED29DD94EBB77AB0ED74805452EF1E02C8D13'
  + '9EA8EF624E71A7A0D746B7210F0A4F3E9B5C0A1497931D5612DB23CACB189295'
  + 'D847C533F41D93F56B77447401D4EE393A75A659A2737ED29FF66E329273466F'
  + 'C2B6186C66F2BA0495B725258758A7CF3B1CA2EDC5221A4D7BF0F7C424FC84B9'
  + '83208CBD44DD90DEB79286F7044352F7C940E16BABF253BAF5FE89614A970FCB'
  + 'A2A3829173150AB2DD083A4C997ADF86A67F34C0DBDE08FA3AA1D25FB76B5DE3'
  + 'A689BED7A3C258C3B43E75C6EE207752A445B4CF983ACB6AE9BF5ED486EF123B'
  + 'EFD56748A29D8F258250BDE3B9E1868A85885D06B6045F1E13708921159456F5'
  + 'C8C984065E18D3B830C9E10D48D1F4ED97945C00ECC5CF78291684A960867577'
  + '1547733F0F3F29E65590C29F425AC0BC4A1F0259DE08F7B38A5749177AECB7C9'
  + '35C8341C80EEF58332A45F07A6598D1B8CC52CA18F8A30BD6D9BC594FB5F8F98'
  + '922C10D3FCEFA84A84001EDF3EB467AA5709CA65072AD07F3B91355E3C8F83E6'
  + 'FF91AB01CAC1F818089E8ED479D6E1494D32B9CCB4A5D0ABB28BE6D23FE7135E'
  + 'A7CAEDD5F4DB5AB26D0E33D01B6ACEADA5CBD1C417672664B808EBA00A71A478'
  + '0545AB277A248972E23A36F5D088BED4270E9430225FA50E5DC8123B52CD91C5'
  + '61FE1E794C4AF68D936C853D7F8317A5445AACB80F74E1C7807FA1A26AE0C999'
  + '847F3046A0BCB79EB7CF84A44016A6DB6B692EBD61CDDE4E35F053548A0015F9'
  + '2B248156C976958DD481DEA5F0D6E7C91BB49CE43E6F23512CE12853851801D1'
  + 'B3B5B37416C5A66A91246BEA75CC2F199FA2D946E0718C4BCE0D9C263F0A5C86'
  + 'B638B81E3CE6A3FDE85D47452F69F84C844C39CDFBBDF4FCFFCA7EF38836516B'
  + '016788BD97A2F35F3E5B826F9C2281D251E7DF2FD896659CC034261325043E8E'
  + '9172B529D277E041001810ADD8AFC08010E3EAE267890F8F4A7354FF8E529B7D'
  + '99311EBBBD785820F49A0136EA1587DF7080CDA3B016F235836634BF13E6D93B'
  + '98BE45AD0078228DD7EC3852931C766A219F05A06AF0A3E1495AA2B6090409A4'
  + 'D9C5AF856D9C5413C46122E6EE250CA0FB711664BCE50A11F6504A57BD256F54'
  + '0BF4CDE1DBE6039B725CABFFDDE99FD363BDBA08ABEE3E348526DB72866E2779'
  + 'EEBC591818A29124B1E24A8D8BD718A6506DAD9FF17C63D0573733746CB575E2'
  + '2AC678641BD8B94213F691CFACD40951186B36B6D4BEF8B17B181588313E4586'
  + '8D582C7FB2411CC45EB84AE33359E99AED2BB1E334F6472F067763B4';
boatload_puzzles_key='859AB2582713EDED1B0C390018A45C55';
</script>
<p id="1712985284" style="min-height:442px;">Loading
<a href="https://www.boatloadpuzzles.com/playcrossword">crossword puzzle</a>. One moment please.</p>
<script src="https://www.boatloadpuzzles.com/Crossword.js?v=20201224" type="text/javascript"></script>
<p>You can put a
<a href="https://www.boatloadpuzzles.com/dailycrossword">daily crossword puzzle</a> on your web site for free!
A new Boatload Puzzles crossword puzzle will appear on your web site each day.</p>
</td><td width="18"> </td><td valign="top" width="160">
<table align="right" cellpadding="0" cellspacing="0" width="160"><tr><td>
<ins class="adsbygoogle" data-ad-client="ca-pub-8904987435081556" data-ad-slot="6824350850" style="display:block;width:160px;height:600px;"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</td></tr></table>
</td></tr></table>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</div>
<div id="footerline"></div>
<table cellpadding="0" cellspacing="0" id="footer"><tr valign="middle">
<td align="left"><a href="javascript:popup('https://www.boatloadpuzzles.com/termsofservice.html', 830, 540)">Terms of Service</a></td>
<td id="copyright">Copyright © Boatload Puzzles, LLC</td>
</tr></table>
</body>
</html>
