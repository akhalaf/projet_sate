<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0" name="viewport"/>
<meta content="rLACZ6p0WZl894OQ8iB_CyyeEfSF-3a2P09JMObYIA4" name="google-site-verification"/>
<title>1MILLION DANCE STUDIO</title>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<link href="/img/favicon.ico" rel="icon" type="image/x-icon"/>
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
<script>
        WebFont.load({
            google: {
                families: ['Noto Sans SC', 'Noto Sans JP', 'Noto Sans KR']
            }
        });
</script>
<link href="https://use.typekit.net/vud8sqc.css" rel="stylesheet"/>
<!-- 커스텀 CSS -->
<link href="/css/font.css" rel="stylesheet" type="text/css"/>
<link href="/css/all.css" rel="stylesheet" type="text/css"/>
<!-- 커스텀 JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/js/onemillion.js" type="text/javascript"></script>
</head>
<body class="translation">
<link crossorigin="anonymous" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/i18next/12.0.0/i18next.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/i18next-xhr-backend/1.5.1/i18nextXHRBackend.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/i18next-browser-languagedetector/2.2.4/i18nextBrowserLanguageDetector.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-i18next/1.2.1/jquery-i18next.js"></script>
<script src="https://cdn.rawgit.com/alexgibson/shake.js/master/shake.js" type="text/javascript"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-131275752-4"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-131275752-4');
</script>
<header style="z-index: 9;">
<script id="custom-select-item" type="text/x-underscore-template">
    <li class="select-item text-size-12 mdl-menu__item" [%= itemDatas %] >
    [%= itemName %]
</li>
</script>
<script id="qrcode-dialog" type="text/x-underscore-template">
    
<div class="qrcode-dialog-container">
    <div class="qrcode-dialog-wrapper">
        <div class="qrcode-dialog-content">
            <div class="name">[%= userName %]</div>
            <div id="dialog-qrcode" class="qrcode"></div>
            <div class="expired-time">[%= expiredString %]</div>
        </div>
    </div>
</div>

</script>
<nav class="font-grotSemi">
<div class="required-data hidden" data-ins-id="" data-user-name=""></div>
<div class="nav-logo">
<a class="flex-row" href="/" style="align-items: center;justify-content: center;padding-left: 20px;display: block;width: 100%;">
<div class="nav-logo-img"></div>
</a>
</div>
<div class="nav-menu-wrapper d-hidden">
<div class="nav-menu-icon" id="nav-menu-icon">
<span></span>
</div>
</div>
<div class="nav-menu m-hidden t-hidden">
<div class="nav-menu-item">
<a class="nav-menu-anchor menu-instructor" data-i18n="translation:header_instructor" href="/instructor">INSTRUCTOR</a>
</div>
<div class="nav-menu-item">
<a class="nav-menu-anchor menu-step-by-1m" data-i18n="translation:header_stepby" href="/stepby1m" id="menu-step-by-1m">STEP BY 1M</a>
</div>
<div class="nav-menu-item">
<a class="nav-menu-anchor menu-class" data-i18n="translation:header_schedule" href="/class">SCHEDULE</a>
</div>
<div class="nav-menu-item is-active">
<a class="nav-menu-anchor menu-pricing" data-i18n="translation:header_price" href="/pricing">PRICING</a>
</div>
<div class="nav-menu-item">
<a class="nav-menu-anchor menu-qna" data-i18n="translation:header_qna" href="/qna">Q&amp;A</a>
</div>
<div class="nav-menu-item">
<a class="nav-menu-anchor menu-contact" data-i18n="translation:header_contact" href="/contact">CONTACT</a>
</div>
<div class="nav-menu-item">
<a class="nav-menu-anchor menu-store" data-i18n="translation:header_store" href="http://1millionstore.com" target="_blank">STORE</a>
</div>
</div>
<div class="nav-right-side-menu m-hidden t-hidden">
<div class="nav-menu-item padding-0">
<a class="btn-signin" data-i18n="translation:header_login" href="/signin">LOGIN</a>
</div>
<span class="separate-signin">/</span>
<div class="nav-menu-item padding-0">
<a class="btn-signup" data-i18n="translation:header_join" href="/signup">JOIN</a>
</div>
<span class="separate-signup">|</span>
<div class="nav-menu-item btn-select-language">
<div class="dropdown">
<button class="dropbtn">
<label class="btn-label">EN</label>
<i class="material-icons btn-language-dropdown">arrow_drop_down</i>
</button>
<div class="dropdown-content" id="dropdown-list">
<a class="dropdown-content-item hover-item" data-language="KO" href="javascript:void(0);">한국어</a>
<a class="dropdown-content-item hover-item" data-language="EN" href="javascript:void(0);">English</a>
<a class="dropdown-content-item hover-item" data-language="CN" href="javascript:void(0);">中文</a>
<a class="dropdown-content-item hover-item" data-language="JP" href="javascript:void(0);">日本語</a>
</div>
</div>
</div>
</div>
</nav>
<div class="nav-menu-mypage-dropdown">
<div class="nav-menu-item mypage-menu-item hover-item">
<a href="/mypage/account"><div data-i18n="translation:header_account">ACCOUNT</div></a>
</div>
<div class="nav-menu-item mypage-menu-item hover-item">
<a href="/mypage/coupon"><div data-i18n="translation:header_ticket">TICKET</div></a>
</div>
<div class="nav-menu-item mypage-menu-item hover-item">
<a href="/mypage/reservation"><div data-i18n="translation:header_reservation">RESERVATION</div></a>
</div>
<div class="nav-menu-item mypage-menu-item hover-item">
<a href="/mypage/sale_coupon"><div data-i18n="translation:header_coupon">COUPON</div></a>
</div>
<div class="nav-menu-item mypage-menu-item hover-item">
<a href="/logout"><div data-i18n="translation:header_logout">LOGOUT</div></a>
</div>
</div>
</header>
<div class="nav-menu-dropdown d-hidden">
<div class="nav-menu-dropdown-wrap flex-column noselect">
<div class="nav-menu-dropdown-item flex-row">
<a class="nav-menu-mobile-anchor menu-home" data-i18n="translation:header_home" href="/">HOME</a>
</div>
<div class="nav-menu-dropdown-item flex-row">
<a class="nav-menu-mobile-anchor menu-instructor" data-i18n="translation:header_instructor" href="/instructor">INSTRUCTOR</a>
</div>
<div class="nav-menu-dropdown-item flex-row">
<a class="nav-menu-mobile-anchor menu-step-by-1m" data-i18n="translation:header_stepby" href="/stepby1m">STEP BY 1M</a>
</div>
<div class="nav-menu-dropdown-item flex-row">
<a class="nav-menu-mobile-anchor menu-class" data-i18n="translation:header_schedule" href="/class">SCHEDULE</a>
</div>
<div class="nav-menu-dropdown-item flex-row">
<a class="nav-menu-mobile-anchor menu-pricing" data-i18n="translation:header_price" href="/pricing">PRICING</a>
</div>
<div class="nav-menu-dropdown-item flex-row">
<a class="nav-menu-mobile-anchor menu-qna" data-i18n="translation:header_qna" href="/qna">Q&amp;A</a>
</div>
<div class="nav-menu-dropdown-item flex-row">
<a class="nav-menu-mobile-anchor menu-contact" data-i18n="translation:header_contact" href="/contact">CONTACT</a>
</div>
<div class="nav-menu-dropdown-item flex-row">
<a class="nav-menu-mobile-anchor menu-store" data-i18n="translation:header_store" href="http://1millionstore.com" target="_blank">STORE</a>
</div>
<div class="nav-menu-dropdown-item flex-row">
<div class="nav-menu-dropdown-item-sub margin-right-30 flex-row">
<a class="nav-menu-mobile-anchor menu-signin" data-i18n="translation:header_login" href="/signin">LOGIN</a>
</div>
<div class="nav-menu-dropdown-item-sub flex-row">
<a class="nav-menu-mobile-anchor menu-signup" data-i18n="translation:header_join" href="/signup">JOIN</a>
</div>
</div>
<div class="nav-menu-dropdown-item language-menu-wrapper flex-row">
<div class="nav-menu-dropdown-item-sub flex-row">
<a class="nav-menu-mobile-anchor language language-ko" data-language="KO" tabindex="-1">한국어</a>
</div>
<div class="nav-menu-dropdown-item-sub flex-row">
<a class="nav-menu-mobile-anchor language language-en" data-language="EN" tabindex="-1">English</a>
</div>
<div class="nav-menu-dropdown-item-sub flex-row">
<a class="nav-menu-mobile-anchor language language-ch" data-language="CN" tabindex="-1">中文</a>
</div>
<div class="nav-menu-dropdown-item-sub flex-row">
<a class="nav-menu-mobile-anchor language language-jp" data-language="JP" tabindex="-1">日本語</a>
</div>
</div>
</div>
</div>
<div class="common-indicator spinner-wrapper hidden">
<div class="loader"></div>
</div>
<div class="notice-container hidden">
<div class="notice-wrapper">
<div class="notice-header-wrapper only-mobile">
<button class="btn-close btn-close-notice">
<i class="material-icons">close</i>
</button>
</div>
<div class="notice-content-wrapper">
<div class="notice-content-inner-wrapper">
<div class="notice-title font-noto-b"></div>
<div class="notice-content font-noto"></div>
</div>
<div class="notice-bottom-wrapper">
<div class="no-show-a-week-wrapper">
<input id="no-show-a-week" name="no-show-a-week" type="checkbox"/>
<label for="no-show-a-week">
<span></span><label for="no-show-a-week">Don't show me this again today.</label>
</label>
</div>
<div class="notice-button-wrapper">
<button class="btn btn-confirm btn-close-notice font-noto-b" type="button">Close</button>
</div>
</div>
</div>
</div>
</div>
<div class="temp_popup_back"></div>
<div class="temp_popup_wrapper">
<div class="temp_popup_img_wrapper">
<a href="https://class101.app/e/1milliion">
<img alt="popup_img" class="show-web" src="https://one-m.s3.ap-northeast-2.amazonaws.com/etc/KakaoTalk_Photo_2020-11-20-22-00-06.png"/>
<img alt="popup_img" class="show-mobile" src="https://one-m.s3.ap-northeast-2.amazonaws.com/etc/KakaoTalk_Photo_2020-11-20-22-00-15.png"/>
</a>
</div>
<div class="temp_popup_btn_wrapper">
<div class="temp_popup_btn_wrapper_content" data-i18n="translation:never_see_again" id="never_see_again_btn"></div>
<div class="temp_popup_btn_wrapper_content" data-i18n="translation:close" id="temp_popup_close_btn"></div>
</div>
</div>
<div class="main auto-height" id="home" style="overflow: initial;">
<div class="main-section">
<div class="main-inner-section">
<div class="service-container">
<div class="service-item">
<img alt="main_image" class="service-main-web-img" src="/img/main/main_new.jpg"/>
<div class="main_mobile">
<img alt="main_image" class="service-main-mobile-img" src="/img/main/main_new_mobile.jpg"/>
</div>
</div>
</div>
</div>
</div>
</div>
<meta content="telephone=no" name="format-detection"/>
<footer>
<div class="footer-container">
<div class="left-side-container only-pc">
<div class="studio-title">
				1MILLION DANCE STUDIO
			</div>
<div class="studio-info">
				주식회사 원밀리언 | <!-- 서울특별시 강남구 봉은사로 43길 29, 3/4층 --> 서울특별시 성동구 뚝섬로 13길, 33
			</div>
<div class="studio-info">
				대표: 윤여욱, 김혜랑 | 사업자 등록 번호: 358-81-01080 | 통신판매신고번호: 제2019-서울성동-01944호 | <a class="use-agreement">이용 약관</a> | <a class="privacy-policy">개인정보 처리방침</a>
</div>
<div class="studio-info font-grotRegular">
				ⓒ 2019 1MILLION Inc. All Rights Reserved.
			</div>
</div>
<div class="right-side-container only-pc">
<div class="item-list-wrapper">
<div class="item-wrapper">
<div class="item">
<a class="right-side-item-link" href="tel:+82-2-512-6756">+82 2 512 6756</a>
</div>
<div class="item">
<a class="right-side-item-link" href="mailto:studio@1milliondance.com" style="text-transform: lowercase;">STUDIO@1MILLIONDANCE.COM</a><br/>
</div>
</div>
<div class="item-wrapper" style="min-width: 79px">
<div class="item">
<a class="right-side-item-link" data-i18n="translation:footer_agency" href="https://agency.1milliondance.com" target="_blank">AGENCY</a>
</div>
<div class="item">
<a class="right-side-item-link" data-i18n="translation:footer_recruitment" href="http://recruit.1milliondance.com" target="_blank">RECRUITMENT</a>
</div>
</div>
<div class="item-wrapper" style="justify-content: flex-start;">
<div class="sns-wrapper">
<a href="https://www.youtube.com/channel/UCw8ZhLPdQ0u_Y-TLKd61hGA" target="_blank">
<div class="sns-item youtube"></div>
</a>
<a href="https://www.instagram.com/1milliondance/" target="_blank">
<div class="sns-item instagram"></div>
</a>
<a href="https://www.facebook.com/1milliondancestudio/" target="_blank">
<div class="sns-item facebook"></div>
</a>
</div>
</div>
</div>
</div>
<div class="mobile-footer-wrapper d-hidden t-show m-show">
<div class="sns-wrapper">
<a href="https://www.youtube.com/channel/UCw8ZhLPdQ0u_Y-TLKd61hGA" target="_blank">
<div class="sns-item youtube" style="background-image: url('/img/ic_youtube.png')"></div>
</a>
<a href="https://www.instagram.com/1milliondance/" target="_blank">
<div class="sns-item instagram" style="background-image: url('/img/ic_instagram.png');margin:0 20px;"></div>
</a>
<a href="https://www.facebook.com/1milliondancestudio/" target="_blank">
<div class="sns-item facebook" style="background-image: url('/img/ic_facebook.png');"></div>
</a>
</div>
<div class="about">
<a class="text-color-white" data-i18n="translation:footer_agency" href="https://agency.1milliondance.com" target="_blank">AGENCY</a>
</div>
<div class="recruitment">
<a class="text-color-white" data-i18n="translation:footer_recruitment" href="http://recruit.1milliondance.com" target="_blank">RECRUITMENT</a>
</div>
<div class="dropdown-menu-wrapper" id="wrapper">
<p class="click-text" data-i18n="translation:footer_more">
					MORE INFO <span class="more-info-arrow"></span>
</p>
<ul>
<li><a class="text-color-white" href="tel:+82-2-512-6756">+82 2 512 6756</a></li>
<li><a class="text-color-white" href="mailto:studio@1milliondance.com">studio@1milliondance.com</a></li>
<li class="font-noto"> 서울특별시 성동구 뚝섬로 13길 33</li>
<li class="font-noto">대표: 윤여욱, 김혜랑</li>
<li class="font-noto">사업자 등록 번호: 358-81-01080</li>
<li class="font-noto">통신판매신고번호: 제2018-서울강남-02547호</li>
<li class="font-noto"><a class="use-agreement" style="color: white;text-decoration: underline;">이용 약관</a></li>
<li class="font-noto"><a class="privacy-policy" style="color: white;text-decoration: underline;">개인정보 처리방침</a></li>
</ul>
</div>
<div style="margin-top: 36px;font-size: 12px; font-family: 'GrotRegular'">ⓒ 2019 1MILLION Inc. All Rights Reserved.</div>
</div>
</div>
<div class="agreement-container user-agreement-container hidden">
<div class="close-wrapper">
<i class="material-icons btn-close">close</i>
</div>
<div class="agreement-content-wrapper">
<div class="title" data-i18n="translation:terms_title"></div>
<div class="content" data-i18n="[html]terms_desc"></div>
</div>
</div>
<div class="agreement-container privacy-policy-container hidden">
<div class="close-wrapper">
<i class="material-icons btn-close">close</i>
</div>
<div class="agreement-content-wrapper">
<div class="title" data-i18n="translation:privacy_title"></div>
<div class="content" data-i18n="[html]privacy_desc"></div>
</div>
</div>
</footer>
</body>
</html>