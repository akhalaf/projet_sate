<!DOCTYPE html>
<html dir="ltr" lang="es-es" xml:lang="es-es" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<base href="https://www.pintapinta.cl/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="ProBusiness is a multi-purpose responsive Joomla template with user-friendly, modern, highly customizable and easy to integrate solution to build your custom Joomla 3.x website." name="description"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>PintaPinta.cl | Inicio</title>
<link href="/templates/probusiness/images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="https://www.pintapinta.cl/index.php/component/search/?id=7&amp;Itemid=114&amp;format=opensearch" rel="search" title="Buscar PintaPinta.cl" type="application/opensearchdescription+xml"/>
<link href="/templates/probusiness/css/owl.carousel.css" rel="stylesheet" type="text/css"/>
<link href="/templates/probusiness/css/owl.theme.css" rel="stylesheet" type="text/css"/>
<link href="/templates/probusiness/css/owl.transitions.css" rel="stylesheet" type="text/css"/>
<link href="/templates/probusiness/css/slide-animate.css" rel="stylesheet" type="text/css"/>
<link href="/components/com_sppagebuilder/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/components/com_sppagebuilder/assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
<link href="/components/com_sppagebuilder/assets/css/sppagebuilder.css" rel="stylesheet" type="text/css"/>
<link href="https://www.pintapinta.cl/media/com_uniterevolution2/assets/rs-plugin/css/settings.css" rel="stylesheet" type="text/css"/>
<link href="https://www.pintapinta.cl/media/com_uniterevolution2/assets/rs-plugin/css/dynamic-captions.css" rel="stylesheet" type="text/css"/>
<link href="https://www.pintapinta.cl/media/com_uniterevolution2/assets/rs-plugin/css/static-captions.css" rel="stylesheet" type="text/css"/>
<link href="/components/com_spsimpleportfolio/assets/css/featherlight.min.css" rel="stylesheet" type="text/css"/>
<link href="/components/com_spsimpleportfolio/assets/css/spsimpleportfolio.css" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,800,600,regular&amp;latin" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Share:regular&amp;latin-ext" rel="stylesheet" type="text/css"/>
<link href="/templates/probusiness/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/templates/probusiness/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/templates/probusiness/css/legacy.css" rel="stylesheet" type="text/css"/>
<link href="/templates/probusiness/css/template.css" rel="stylesheet" type="text/css"/>
<link class="preset" href="/templates/probusiness/css/presets/preset4.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
body{font-family:Open Sans, sans-serif; font-size:20px; font-weight:300; }h1{font-family:Open Sans, sans-serif; font-weight:800; }h2{font-family:Open Sans, sans-serif; font-weight:600; }h3{font-family:Share, sans-serif; font-weight:normal; }h4{font-family:Open Sans, sans-serif; font-weight:normal; }h5{font-family:Open Sans, sans-serif; font-weight:600; }h6{font-family:Open Sans, sans-serif; font-weight:600; }.sp-megamenu-parent{font-family:Share, sans-serif; font-weight:normal; }#sp-bottom{ background-color:#191b1f;color:#93959d;padding:80px 0px; }#sp-footer{ background-color:#0d0e11;color:#ffffff; }
	</style>
<script src="/media/jui/js/jquery.min.js?b1233320012356fd7f56d0b9b7d25e97" type="text/javascript"></script>
<script src="/media/jui/js/jquery-noconflict.js?b1233320012356fd7f56d0b9b7d25e97" type="text/javascript"></script>
<script src="/media/jui/js/jquery-migrate.min.js?b1233320012356fd7f56d0b9b7d25e97" type="text/javascript"></script>
<script src="/templates/probusiness/js/owl.carousel.min.js" type="text/javascript"></script>
<script src="/templates/probusiness/js/addon.slider.js" type="text/javascript"></script>
<script src="https://www.pintapinta.cl/media/com_uniterevolution2/assets/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="https://www.pintapinta.cl/media/com_uniterevolution2/assets/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="/components/com_spsimpleportfolio/assets/js/jquery.shuffle.modernizr.min.js" type="text/javascript"></script>
<script src="/components/com_spsimpleportfolio/assets/js/featherlight.min.js" type="text/javascript"></script>
<script src="/components/com_spsimpleportfolio/assets/js/spsimpleportfolio.js" type="text/javascript"></script>
<script src="/components/com_sppagebuilder/assets/js/sppagebuilder.js" type="text/javascript"></script>
<script src="/templates/probusiness/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/templates/probusiness/js/jquery.sticky.js" type="text/javascript"></script>
<script src="/templates/probusiness/js/main.js" type="text/javascript"></script>
<meta content="Inicio" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="https://www.pintapinta.cl/" property="og:url"/>
</head>
<body class="site com-sppagebuilder view-page no-layout no-task itemid-114 es-es ltr sticky-header layout-fluid">
<div class="body-innerwrapper">
<header id="sp-header"><div class="container"><div class="row"><div class="col-xs-8 col-sm-3 col-md-3" id="sp-logo"><div class="sp-column "><a class="logo" href="/"><h1>PintaPinta.cl</h1></a></div></div><div class="col-xs-4 col-sm-9 col-md-9" id="sp-menu"><div class="sp-column "> <div class="sp-megamenu-wrapper">
<a href="#" id="offcanvas-toggler"><i class="fa fa-bars"></i></a>
<ul class="sp-megamenu-parent menu-fade hidden-xs"><li class="sp-menu-item current-item active"><a href="/index.php">Inicio</a></li><li class="sp-menu-item"><a href="/index.php/pricing">Precios</a></li><li class="sp-menu-item"><a href="/index.php/contact-5">Contáctanos</a></li></ul> </div>
</div></div></div></div></header><section id="sp-page-title"><div class="row"><div class="col-sm-12 col-md-12" id="sp-title"><div class="sp-column "></div></div></div></section><section id="sp-main-body"><div class="row"><div class="col-sm-12 col-md-12" id="sp-component"><div class="sp-column "><div id="system-message-container">
</div>
<div class="sp-page-builder page-7" id="sp-page-builder">
<div class="page-content">
<section class="sppb-section " style=""><div class="sppb-row"><div class="sppb-col-sm-12"><div class="sppb-addon-container" style=""><div class="sppb-addon sppb-addon-module "><div class="sppb-addon-content"><!-- START REVOLUTION SLIDER 4.6.8 fullwidth mode -->
<div class="rev_slider_wrapper fullwidthbanner-container" id="rev_slider_3_1_wrapper" style="margin:0px auto;background-color:#E9E9E9;padding:0px;margin-top:0px;margin-bottom:0px;max-height:550px;">
<div class="rev_slider fullwidthabanner" id="rev_slider_3_1" style="display:none;max-height:550px;height:550px;">
<ul> <!-- SLIDE  1-->
<li data-delay="5000" data-masterspeed="300" data-saveperformance="off" data-slotamount="7" data-transition="3dcurtain-vertical">
<!-- MAIN IMAGE -->
<img alt="Pinta-Pinta" data-bgfit="contain" data-bgposition="center center" data-bgrepeat="no-repeat" src="https://www.pintapinta.cl/images/Pinta-Pinta.png"/>
<!-- LAYERS -->
</li>
<!-- SLIDE  2-->
<li data-delay="5000" data-masterspeed="300" data-saveperformance="off" data-slotamount="7" data-transition="3dcurtain-vertical">
<!-- MAIN IMAGE -->
<img alt="IMG_9186" data-bgfit="100% 100%" data-bgposition="center top" data-bgrepeat="no-repeat" src="https://www.pintapinta.cl/images/IMG_9186.JPG"/>
<!-- LAYERS -->
</li>
<!-- SLIDE  3-->
<li data-delay="5000" data-masterspeed="300" data-saveperformance="off" data-slotamount="7" data-transition="3dcurtain-horizontal">
<!-- MAIN IMAGE -->
<img alt="IMG_9480" data-bgfit="100% 100%" data-bgposition="center top" data-bgrepeat="no-repeat" src="https://www.pintapinta.cl/images/IMG_9480.JPG"/>
<!-- LAYERS -->
</li>
<!-- SLIDE  4-->
<li data-delay="5000" data-masterspeed="300" data-saveperformance="off" data-slotamount="7" data-transition="3dcurtain-horizontal">
<!-- MAIN IMAGE -->
<img alt="IMG_4211" data-bgfit="100% 100%" data-bgposition="center top" data-bgrepeat="no-repeat" src="https://www.pintapinta.cl/images/IMG_4211.JPG"/>
<!-- LAYERS -->
</li>
<!-- SLIDE  5-->
<li data-masterspeed="300" data-saveperformance="off" data-slotamount="7" data-transition="3dcurtain-vertical">
<!-- MAIN IMAGE -->
<img alt="IMG_3419" data-bgfit="100% 100%" data-bgposition="center top" data-bgrepeat="no-repeat" src="https://www.pintapinta.cl/images/IMG_3419.JPG"/>
<!-- LAYERS -->
</li>
</ul>
<div class="tp-bannertimer"></div> </div>
<script type="text/javascript">

					
				/******************************************
					-	PREPARE PLACEHOLDER FOR SLIDER	-
				******************************************/
								
				 
						var setREVStartSize = function() {
							var	tpopt = new Object(); 
								tpopt.startwidth = 960;
								tpopt.startheight = 550;
								tpopt.container = jQuery('#rev_slider_3_1');
								tpopt.fullScreen = "off";
								tpopt.forceFullWidth="on";

							tpopt.container.closest(".rev_slider_wrapper").css({height:tpopt.container.height()});tpopt.width=parseInt(tpopt.container.width(),0);tpopt.height=parseInt(tpopt.container.height(),0);tpopt.bw=tpopt.width/tpopt.startwidth;tpopt.bh=tpopt.height/tpopt.startheight;if(tpopt.bh>tpopt.bw)tpopt.bh=tpopt.bw;if(tpopt.bh<tpopt.bw)tpopt.bw=tpopt.bh;if(tpopt.bw<tpopt.bh)tpopt.bh=tpopt.bw;if(tpopt.bh>1){tpopt.bw=1;tpopt.bh=1}if(tpopt.bw>1){tpopt.bw=1;tpopt.bh=1}tpopt.height=Math.round(tpopt.startheight*(tpopt.width/tpopt.startwidth));if(tpopt.height>tpopt.startheight&&tpopt.autoHeight!="on")tpopt.height=tpopt.startheight;if(tpopt.fullScreen=="on"){tpopt.height=tpopt.bw*tpopt.startheight;var cow=tpopt.container.parent().width();var coh=jQuery(window).height();if(tpopt.fullScreenOffsetContainer!=undefined){try{var offcontainers=tpopt.fullScreenOffsetContainer.split(",");jQuery.each(offcontainers,function(e,t){coh=coh-jQuery(t).outerHeight(true);if(coh<tpopt.minFullScreenHeight)coh=tpopt.minFullScreenHeight})}catch(e){}}tpopt.container.parent().height(coh);tpopt.container.height(coh);tpopt.container.closest(".rev_slider_wrapper").height(coh);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh);tpopt.container.css({height:"100%"});tpopt.height=coh;}else{tpopt.container.height(tpopt.height);tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height);}
						};
						
						/* CALL PLACEHOLDER */
						setREVStartSize();
								
				
				var tpj=jQuery;				
				tpj.noConflict();				
				var revapi3;
				
				
				
				tpj(document).ready(function() {
				
					
								
				if(tpj('#rev_slider_3_1').revolution == undefined){
					revslider_showDoubleJqueryError('#rev_slider_3_1');
				}else{
				   revapi3 = tpj('#rev_slider_3_1').show().revolution(
					{
											
						dottedOverlay:"none",
						delay:5000,
						startwidth:960,
						startheight:550,
						hideThumbs:200,
						
						thumbWidth:100,
						thumbHeight:50,
						thumbAmount:5,
													
						simplifyAll:"off",						
						navigationType:"bullet",
						navigationArrows:"solo",
						navigationStyle:"round",						
						touchenabled:"on",
						onHoverStop:"off",						
						nextSlideOnWindowFocus:"off",
						
						swipe_threshold: 75,
						swipe_min_touches: 1,
						drag_block_vertical: false,
																		
																		
						keyboardNavigation:"off",
						
						navigationHAlign:"center",
						navigationVAlign:"bottom",
						navigationHOffset:0,
						navigationVOffset:20,

						soloArrowLeftHalign:"left",
						soloArrowLeftValign:"center",
						soloArrowLeftHOffset:20,
						soloArrowLeftVOffset:0,

						soloArrowRightHalign:"right",
						soloArrowRightValign:"center",
						soloArrowRightHOffset:20,
						soloArrowRightVOffset:0,
								
						shadow:0,
						fullWidth:"on",
						fullScreen:"off",

												spinner:"spinner0",
																		
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,

						shuffle:"off",
						
						autoHeight:"off",						
						forceFullWidth:"on",						
												
												
												
						hideThumbsOnMobile:"off",
						hideNavDelayOnMobile:1500,
						hideBulletsOnMobile:"off",
						hideArrowsOnMobile:"off",
						hideThumbsUnderResolution:0,
						
												hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						startWithSlide:0,
						isJoomla: true
					});
					
					
					
									}					
				});	/*ready*/
									
			</script>
</div>
<!-- END REVOLUTION SLIDER --> </div></div></div></div></div></section><section class="sppb-section " style="padding:40px 0;background-image:url(/images/sppagebuilder/cta-bg.png);background-repeat:no-repeat;background-size:cover;background-attachment:inherit;background-position:0 0;"><div class="sppb-container"><div class="sppb-row"><div class="sppb-col-sm-12"><div class="sppb-addon-container" style=""><div class="sppb-addon sppb-addon-cta " style=""><div class="sppb-row"><div class="sppb-col-sm-8"><h3 class="sppb-cta-title" style="font-size:24px;line-height:24px;">Quieres celebrar el cumpleaños de tu hijo?</h3><p class="sppb-lead sppb-cta-subtitle" style="font-size:14px;line-height:14px;">Contactanos y reserva tu hora!</p></div><div class="sppb-col-sm-4 sppb-text-right"><a class="sppb-btn sppb-btn-warning sppb-btn-lg " href="mailto:info@pintapinta.cl" role="button" target="_blank"><i class="fa fa-cart-arrow-down"></i> Contáctanos</a></div></div></div></div></div></div></div></section><section class="sppb-section " style=""><div class="sppb-container"><div class="sppb-row"><div class="sppb-col-sm-4"><div class="sppb-addon-container sppb-wow fadeInDown" style=""></div></div><div class="sppb-col-sm-4 "><div class="sppb-addon-container sppb-wow fadeInDown" style=""><div class="sppb-empty-space clearfix" style="margin-bottom:20px;"></div><div class="sppb-addon sppb-addon-facebook-likebox "><div id="fb-root"></div><script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=889817231105533&version=v2.0";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, "script", "facebook-jssdk"));</script><div class="fb-like-box" data-colorscheme="light" data-header="false" data-height="300" data-href="https://www.facebook.com/PintaPinta.cl" data-show-border="false" data-show-faces="true" data-stream="true" data-width="800"></div></div><div class="sppb-empty-space clearfix" style="margin-bottom:20px;"></div></div></div><div class="sppb-col-sm-4"><div class="sppb-addon-container" style=""></div></div></div></div></section><section class="sppb-section " style=""><div class="sppb-container"><div class="sppb-row"><div class="sppb-col-sm-12"><div class="sppb-addon-container" style=""><div class="sppb-empty-space clearfix" style="margin-bottom:20px;"></div><div class="sppb-addon sppb-addon-module "><div class="sppb-addon-content">
<div class="sp-simpleportfolio sp-simpleportfolio-view-items layout-gallery-space " id="mod-sp-simpleportfolio">
<div class="sp-simpleportfolio-filter">
<ul>
<li class="active" data-group="all"><a href="#">Show All</a></li>
<li data-group="cumpleanos"><a href="#">Cumpleaños</a></li>
<li data-group="fiestafluor"><a href="#">Fiesta Fluor</a></li>
<li data-group="eventos-especiales"><a href="#">Eventos Especiales</a></li>
<li data-group="eventos-empresas"><a href="#">Eventos Empresas</a></li>
</ul>
</div>
<div class="sp-simpleportfolio-items sp-simpleportfolio-columns-4">
<div class="sp-simpleportfolio-item" data-groups='["cumpleanos","fiestafluor"]'>
<div class="sp-simpleportfolio-overlay-wrapper clearfix">
<img alt="Folio 01" class="sp-simpleportfolio-img" src="/images/spsimpleportfolio/folio-01/MG_6169_600x600.JPG"/>
<div class="sp-simpleportfolio-overlay">
<div class="sp-vertical-middle">
<div>
<div class="sp-simpleportfolio-btns">
<a class="btn-zoom" data-featherlight="image" href="/images/spsimpleportfolio/folio-01/MG_6169_600x400.JPG">Zoom</a>
<a class="btn-view" href="/index.php/component/spsimpleportfolio/item/1-folio-01">View</a>
</div>
<h3 class="sp-simpleportfolio-title">
<a href="/index.php/component/spsimpleportfolio/item/1-folio-01">
										Folio 01									</a>
</h3>
<div class="sp-simpleportfolio-tags">
									Cumpleaños, Fiesta Fluor								</div>
</div>
</div>
</div>
</div>
</div>
<div class="sp-simpleportfolio-item" data-groups='["fiestafluor","eventos-especiales"]'>
<div class="sp-simpleportfolio-overlay-wrapper clearfix">
<img alt="Folio 02" class="sp-simpleportfolio-img" src="/images/spsimpleportfolio/folio-02/MG_6182_600x600.JPG"/>
<div class="sp-simpleportfolio-overlay">
<div class="sp-vertical-middle">
<div>
<div class="sp-simpleportfolio-btns">
<a class="btn-zoom" data-featherlight="image" href="/images/spsimpleportfolio/folio-02/MG_6182_600x400.JPG">Zoom</a>
<a class="btn-view" href="/index.php/component/spsimpleportfolio/item/2-folio-02">View</a>
</div>
<h3 class="sp-simpleportfolio-title">
<a href="/index.php/component/spsimpleportfolio/item/2-folio-02">
										Folio 02									</a>
</h3>
<div class="sp-simpleportfolio-tags">
									Fiesta Fluor, Eventos Especiales								</div>
</div>
</div>
</div>
</div>
</div>
<div class="sp-simpleportfolio-item" data-groups='["eventos-especiales","eventos-empresas"]'>
<div class="sp-simpleportfolio-overlay-wrapper clearfix">
<img alt="Folio 03" class="sp-simpleportfolio-img" src="/images/spsimpleportfolio/folio-03/IMG_3165_600x600.JPG"/>
<div class="sp-simpleportfolio-overlay">
<div class="sp-vertical-middle">
<div>
<div class="sp-simpleportfolio-btns">
<a class="btn-zoom" data-featherlight="image" href="/images/spsimpleportfolio/folio-03/IMG_3165_600x400.JPG">Zoom</a>
<a class="btn-view" href="/index.php/component/spsimpleportfolio/item/3-folio-03">View</a>
</div>
<h3 class="sp-simpleportfolio-title">
<a href="/index.php/component/spsimpleportfolio/item/3-folio-03">
										Folio 03									</a>
</h3>
<div class="sp-simpleportfolio-tags">
									Eventos Especiales, Eventos Empresas								</div>
</div>
</div>
</div>
</div>
</div>
<div class="sp-simpleportfolio-item" data-groups='["cumpleanos","eventos-empresas"]'>
<div class="sp-simpleportfolio-overlay-wrapper clearfix">
<img alt="Folio 04" class="sp-simpleportfolio-img" src="/images/spsimpleportfolio/folio-04/IMG_9480_600x600.JPG"/>
<div class="sp-simpleportfolio-overlay">
<div class="sp-vertical-middle">
<div>
<div class="sp-simpleportfolio-btns">
<a class="btn-zoom" data-featherlight="image" href="/images/spsimpleportfolio/folio-04/IMG_9480_600x400.JPG">Zoom</a>
<a class="btn-view" href="/index.php/component/spsimpleportfolio/item/4-folio-04">View</a>
</div>
<h3 class="sp-simpleportfolio-title">
<a href="/index.php/component/spsimpleportfolio/item/4-folio-04">
										Folio 04									</a>
</h3>
<div class="sp-simpleportfolio-tags">
									Cumpleaños, Eventos Empresas								</div>
</div>
</div>
</div>
</div>
</div>
<div class="sp-simpleportfolio-item" data-groups='["cumpleanos","fiestafluor"]'>
<div class="sp-simpleportfolio-overlay-wrapper clearfix">
<img alt="Folio 05" class="sp-simpleportfolio-img" src="/images/spsimpleportfolio/folio-05/IMG_8941_600x600.JPG"/>
<div class="sp-simpleportfolio-overlay">
<div class="sp-vertical-middle">
<div>
<div class="sp-simpleportfolio-btns">
<a class="btn-zoom" data-featherlight="image" href="/images/spsimpleportfolio/folio-05/IMG_8941_600x400.JPG">Zoom</a>
<a class="btn-view" href="/index.php/component/spsimpleportfolio/item/5-folio-05">View</a>
</div>
<h3 class="sp-simpleportfolio-title">
<a href="/index.php/component/spsimpleportfolio/item/5-folio-05">
										Folio 05									</a>
</h3>
<div class="sp-simpleportfolio-tags">
									Cumpleaños, Fiesta Fluor								</div>
</div>
</div>
</div>
</div>
</div>
<div class="sp-simpleportfolio-item" data-groups='["fiestafluor","eventos-especiales"]'>
<div class="sp-simpleportfolio-overlay-wrapper clearfix">
<img alt="Folio 06" class="sp-simpleportfolio-img" src="/images/spsimpleportfolio/folio-06/IMG_4207_600x600.JPG"/>
<div class="sp-simpleportfolio-overlay">
<div class="sp-vertical-middle">
<div>
<div class="sp-simpleportfolio-btns">
<a class="btn-zoom" data-featherlight="image" href="/images/spsimpleportfolio/folio-06/IMG_4207_600x400.JPG">Zoom</a>
<a class="btn-view" href="/index.php/component/spsimpleportfolio/item/6-folio-06">View</a>
</div>
<h3 class="sp-simpleportfolio-title">
<a href="/index.php/component/spsimpleportfolio/item/6-folio-06">
										Folio 06									</a>
</h3>
<div class="sp-simpleportfolio-tags">
									Fiesta Fluor, Eventos Especiales								</div>
</div>
</div>
</div>
</div>
</div>
<div class="sp-simpleportfolio-item" data-groups='["eventos-especiales","eventos-empresas"]'>
<div class="sp-simpleportfolio-overlay-wrapper clearfix">
<img alt="Folio 07" class="sp-simpleportfolio-img" src="/images/spsimpleportfolio/folio-07/20150829_124121_600x600.jpg"/>
<div class="sp-simpleportfolio-overlay">
<div class="sp-vertical-middle">
<div>
<div class="sp-simpleportfolio-btns">
<a class="btn-zoom" data-featherlight="image" href="/images/spsimpleportfolio/folio-07/20150829_124121_600x400.jpg">Zoom</a>
<a class="btn-view" href="/index.php/component/spsimpleportfolio/item/7-folio-07">View</a>
</div>
<h3 class="sp-simpleportfolio-title">
<a href="/index.php/component/spsimpleportfolio/item/7-folio-07">
										Folio 07									</a>
</h3>
<div class="sp-simpleportfolio-tags">
									Eventos Especiales, Eventos Empresas								</div>
</div>
</div>
</div>
</div>
</div>
<div class="sp-simpleportfolio-item" data-groups='["cumpleanos","eventos-empresas"]'>
<div class="sp-simpleportfolio-overlay-wrapper clearfix">
<img alt="Folio 08" class="sp-simpleportfolio-img" src="/images/spsimpleportfolio/folio-08/IMG_7153_600x600.JPG"/>
<div class="sp-simpleportfolio-overlay">
<div class="sp-vertical-middle">
<div>
<div class="sp-simpleportfolio-btns">
<a class="btn-zoom" data-featherlight="image" href="/images/spsimpleportfolio/folio-08/IMG_7153_600x400.JPG">Zoom</a>
<a class="btn-view" href="/index.php/component/spsimpleportfolio/item/8-folio-08">View</a>
</div>
<h3 class="sp-simpleportfolio-title">
<a href="/index.php/component/spsimpleportfolio/item/8-folio-08">
										Folio 08									</a>
</h3>
<div class="sp-simpleportfolio-tags">
									Cumpleaños, Eventos Empresas								</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div></div><div class="sppb-empty-space clearfix" style="margin-bottom:20px;"></div></div></div></div></div></section> </div>
</div>
</div></div></div></section><section id="sp-bottom"><div class="container"><div class="row"><div class="col-sm-6 col-md-6" id="sp-bottom1"><div class="sp-column "><div class="sp-module "><h3 class="sp-module-title">Contáctanos!</h3><div class="sp-module-content">
<div class="custom">
<div class="sppb-addon-content"><p>Saucache</p>
<p>Arica, Chile</p>
<p><span class="adress">Telefono:</span> +56 9 56641313</p>
<p><span class="adress">Email:</span> <a href="mailto:rodrigo@pintapinta.cl">rodrigo@pintapinta.cl</a></p></div></div>
</div></div></div></div><div class="col-sm-6 col-md-6" id="sp-bottom2"><div class="sp-column "><div class="sp-module "><h3 class="sp-module-title">Sobre Nosotros</h3><div class="sp-module-content">
<div class="custom">
<p>
  Nos dedicamos a entretener niños y jovenes en eventos.
</p>
<p>
  Pinta Caras, Atriles, Juegos Inflables y mucho más!
</p></div>
</div></div></div></div></div></div></section><footer id="sp-footer"><div class="container"><div class="row"><div class="col-sm-12 col-md-12" id="sp-footer1"><div class="sp-column "><span class="sp-copyright"> © 2015 PintaPinta.cl. Todos los derechos reservados. Diseñado por ProAgency.co</span></div></div></div></div></footer>
<div class="offcanvas-menu">
<a class="close-offcanvas" href="#"><i class="fa fa-remove"></i></a>
<div class="offcanvas-inner">
<div class="sp-module "><div class="sp-module-content"><div class="search">
<form action="/index.php" method="post">
<input class="inputbox search-query" id="mod-search-searchword" maxlength="200" name="searchword" placeholder="Buscar..." size="0" type="text"/> <input name="task" type="hidden" value="search"/>
<input name="option" type="hidden" value="com_search"/>
<input name="Itemid" type="hidden" value="114"/>
</form>
</div>
</div></div><div class="sp-module _menu"><h3 class="sp-module-title">Mobile Menu</h3><div class="sp-module-content"><ul class="nav menu">
<li class="item-114 default current active"><a href="/index.php">Inicio</a></li><li class="item-179"><a href="/index.php/pricing">Precios</a></li><li class="item-138"><a href="/index.php/contact-5">Contáctanos</a></li></ul>
</div></div>
</div>
</div>
</div>
</body>
</html>