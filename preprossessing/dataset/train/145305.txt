<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html class="site-html" dir="rtl" lang="ar" xml:lang="ar" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Kifah.m.shalash" name="author"/>
<meta content="kifah.shalash@hotmail.com" name="email"/>
<meta content="Global" name="Distribution"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="http://arab-board.org/ar/rss.xml" rel="alternate" title="المجلس العربي للاختصاصات الصحية آر.إس.إس" type="application/rss+xml"/>
<link href="/sites/default/files/board_favicon.jpg" rel="shortcut icon" type="image/x-icon"/>
<title>المجلس العربي للاختصاصات الصحية</title><script src="/sites/all/modules/jquery_update/replace/jquery.min.js?U" type="text/javascript"></script>
<script src="/misc/drupal.js?U" type="text/javascript"></script>
<script src="/sites/default/files/languages/ar_64be9459998e4075720acf1e473a2fd8.js?U" type="text/javascript"></script>
<script src="/sites/all/modules/nice_menus/superfish/js/superfish.js?U" type="text/javascript"></script>
<script src="/sites/all/modules/nice_menus/superfish/js/jquery.bgiframe.min.js?U" type="text/javascript"></script>
<script src="/sites/all/modules/nice_menus/superfish/js/jquery.hoverIntent.minified.js?U" type="text/javascript"></script>
<script src="/sites/all/modules/nice_menus/nice_menus.js?U" type="text/javascript"></script>
<script src="/sites/all/modules/rounded_corners/jquery.corner.js?U" type="text/javascript"></script>
<script src="/sites/all/modules/views/js/base.js?U" type="text/javascript"></script>
<script src="/sites/all/modules/views/js/ajax_view.js?U" type="text/javascript"></script>
<script src="/sites/all/modules/jcarousel/jcarousel/lib/jquery.jcarousel.js?U" type="text/javascript"></script>
<script src="/sites/all/modules/jcarousel/jcarousel.js?U" type="text/javascript"></script>
<script src="/sites/all/themes/board/tabses.js?U" type="text/javascript"></script>
<script src="/sites/all/themes/board/files/pirobox.js?U" type="text/javascript"></script>
<script src="/sites/all/themes/board/files/slider.js?U" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, { "basePath": "/", "nice_menus_options": { "delay": "500", "speed": "normal" }, "views": { "ajax_path": [ "/ar/views/ajax", "/ar/views/ajax", "/ar/views/ajax" ], "ajaxViews": [ { "view_name": "news", "view_display_id": "block_1", "view_args": "", "view_path": "node", "view_base_path": "News", "view_dom_id": 1, "pager_element": 0 }, { "view_name": "news", "view_display_id": "block_2", "view_args": "", "view_path": "node", "view_base_path": "News", "view_dom_id": 2, "pager_element": 0 }, { "view_name": "events_block", "view_display_id": "block_1", "view_args": "", "view_path": "node", "view_base_path": null, "view_dom_id": 3, "pager_element": 0 } ] }, "jcarousel": { "#viewscarousel-events-block-block-1": { "vertical": 1, "scroll": 3, "animation": "fast", "auto": 8, "wrap": "circular", "skin": "tango" } } });
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
$(function(){ // shorthand for $(document).ready() 
$(document).ready(function(){$(".pager, #node-links ul.links li, .questions_call").corner();});
 });
//--><!]]>
</script>
<link href="/modules/node/node.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/modules/node/node-rtl.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/modules/system/defaults.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/modules/system/defaults-rtl.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/modules/system/system.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/modules/system/system-rtl.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/modules/system/system-menus.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/modules/system/system-menus-rtl.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/modules/user/user.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/modules/user/user-rtl.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/cck/theme/content-module.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/cck/theme/content-module-rtl.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/ckeditor/ckeditor.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/date/date.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/date/date-rtl.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/date/date_popup/themes/jquery.timeentry.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/filefield/filefield.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/filefield/filefield-rtl.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/nice_menus/nice_menus.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/nice_menus/nice_menus_default.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/nice_menus/nice_menus_default-rtl.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/views/css/views.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/views/css/views-rtl.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/jcarousel/jcarousel/lib/jquery.jcarousel.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/jcarousel/jcarousel.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/themes/board/style.css?U" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/themes/board/files/piro/style.css?U" media="all" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript"> jQuery(document).ready(function(){ $('.block h2').click(function(){ $(this).next().toggle('fast'); return false; }).next().hide();});</script>-->
<!--[if IE 6]><link rel="stylesheet" href="/sites/all/themes/board/css.ie.css" type="text/css" media="screen" /><![endif]--></head>
<body class="site-body">
<div id="main_container">
<div id="header">
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" height="200" width="960">
<param name="movie" value="/sites/all/themes/board/files/header.swf"/>
<param name="quality" value="high"/>
<param name="wmode" value="transparent"/>
<embed height="200" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" quality="high" src="/sites/all/themes/board/files/header.swf" type="application/x-shockwave-flash" width="960" wmode="transparent"/>
</object>
</div>
<div id="menu">
<div id="menu-links"><div class="block block-nice_menus" id="block-nice_menus-1">
<div class="content">
<ul class="nice-menu nice-menu-down" id="nice-menu-1"><li class="menu-195 menu-path-front active-trail first odd "><a class="active" href="/ar" title="المجلس العربي للاختصاصات الطبية">الرئيسية</a></li>
<li class="menu-890 menu-path-arab-boardorg-ar-content-%D8%A7%D9%84%D9%86%D8%B8%D8%A7%D9%85-%D9%88%D8%A7%D9%84%D9%84%D8%A7%D8%A6%D8%AD%D8%A9 even "><a href="http://arab-board.org/ar/content/%D8%A7%D9%84%D9%86%D8%B8%D8%A7%D9%85-%D9%88%D8%A7%D9%84%D9%84%D8%A7%D8%A6%D8%AD%D8%A9" title="">النظام واللائحة</a></li>
<li class="menu-877 menu-path-node-139223 odd "><a href="/ar/content/%D9%87%D9%8A%D8%A6%D8%A7%D8%AA-%D9%88%D9%85%D8%AC%D8%A7%D9%84%D8%B3-%D9%85%D8%AD%D9%84%D9%8A%D8%A9" title="هيئات ومجالس محلية">هيئات ومجالس محلية</a></li>
<li class="menu-412 menu-path-journal even "><a href="/ar/journal" title="تحميل أعداد مجلة المجلس">مجلة المجلس</a></li>
<li class="menu-888 menu-path-node-218266 odd "><a href="/ar/content/%D9%85%D8%AC%D9%84%D8%A9-%D8%A7%D9%84%D8%A7%D8%AE%D8%AA%D8%B5%D8%A7%D8%B5%D8%A7%D8%AA-%D8%A7%D9%84%D8%B5%D8%AD%D9%8A%D8%A9-%D8%A7%D9%84%D8%B9%D8%B1%D8%A8%D9%8A%D8%A9" title="مجلة الاختصاصات الصحية العربية">نشرة إخبارية</a></li>
<li class="menu-196 menu-path-node-18610 even last "><a href="/ar/content/%D9%84%D9%84%D8%A7%D8%AA%D8%B5%D8%A7%D9%84-%D8%A8%D9%86%D8%A7" title="الاتصال بإدارة المجلس">اتصل بنا</a></li>
</ul>
</div>
</div>
<div class="block block-locale" id="block-locale-0">
<div class="content">
<ul><li class="ar first active"><a class="language-link active" href="/ar">عربي</a></li>
<li class="en last"><a class="language-link" href="/en">English</a></li>
</ul> </div>
</div>
</div>
<div id="search_div"><form accept-charset="UTF-8" action="/ar/search/node" method="post">
<input alt="بحث" class="sbotton" name="op" src="/sites/all/themes/board/files/arsbutton.jpg" title="بحث" type="image"/>
<input class="sarea" dir="RTL" lang="ar" maxlength="64" name="keys" type="text" value=""/>
<input graduates="" id="edit-type-graduates" name="type[" type="hidden" value="graduates"/>
<input id="edit-type-resaults" name="type[" resaults="" type="hidden" value="resaults"/>
</form></div>
</div>
<div id="site_content">
<div id="sidebar">
<div class="block block-block" id="block-block-3">
<h2>زاوية المتدرب</h2>
<div class="content">
<ul>
<li>
<a href="http://arab-board.org/ar/node/48">قواعد عامة</a></li>
<li>
<a href="http://arab-board.org/ar/content/%D8%A3%D8%AE%D9%84%D8%A7%D9%82%D9%8A%D8%A7%D8%AA-%D9%88%D9%82%D9%88%D8%A7%D9%86%D9%8A%D9%86-%D8%A7%D9%84%D9%85%D9%87%D9%86%D8%A9-%D8%A3%D8%B3%D8%A6%D9%84%D8%A9-%D9%88%D8%A3%D8%AC%D9%88%D8%A8%D8%A9">أخلاقيات وقوانين المهنة (أسئلة وأجوبة)</a></li>
<li>
<a href="http://arab-board.org/ar/node/140118">الرسوم</a></li>
<li>
<a href="http://arab-board.org/ar/node/50">المواعيد</a></li>
<li>
<a href="http://arab-board.org/ar/announcements">الاعلانات</a></li>
<li>
<a href="http://arab-board.org/ar/resaults">نتائج الامتحان</a></li>
<li>
<a href="http://arab-board.org/ar/q-a">سؤال وجواب</a></li>
</ul>
</div>
</div>
<div class="block block-block" id="block-block-8">
<h2>النماذج الالكترونية</h2>
<div class="content">
<ul>
<li>
<p>
<span style="font-size: 14px;"><a href="http://arab-board.org/ar/content/%D8%A7%D9%84%D9%86%D9%85%D8%A7%D8%B0%D8%AC-%D8%A7%D9%84%D8%A7%D9%84%D9%83%D8%AA%D8%B1%D9%88%D9%86%D9%8A%D8%A9">كافة النماذج الالكترونية</a></span></p>
</li>
</ul>
</div>
</div>
<div class="block block-block" id="block-block-1">
<h2>مراكز التدريب</h2>
<div class="content">
<ul>
<li>
<a href="http://arab-board.org/ar/centers">مراكز التدريب</a></li>
<li>
<a href="http://arab-board.org/ar/node/10">آلية الإعتراف بالمركز</a></li>
<li>
<a href="http://arab-board.org/ar/node/11">الشروط المطلوبة للاعتراف</a></li>
<li>
<a href="http://arab-board.org/ar/node/143069">استمارات الاعتراف بمركز تدريبي لكافة الاختصاصات</a></li>
</ul>
</div>
</div>
<div class="block block-block" id="block-block-10">
<h2>أعضاء المجالس العلمية</h2>
<div class="content">
<p class="rtecenter">
<a href="http://arab-board.org/ar/content/%D8%A3%D8%B9%D8%B6%D8%A7%D8%A1-%D8%A7%D9%84%D9%85%D8%AC%D8%A7%D9%84%D8%B3-%D8%A7%D9%84%D8%B9%D9%84%D9%85%D9%8A%D8%A9"><span style="font-size: 16px;"><span style="color: rgb(4, 94, 141);"><span><strong>أعضاء المجالس العلمية لكافة الاختصاصات</strong></span></span></span></a></p>
</div>
</div>
<div class="specialities_home">
<a href="/ar/specialities">الاختصاصات</a> </div>
</div>
<div id="home-content">
<div id="home_date">الأربعاء 
 13-1-2021       آخر تحديث :  13-01-2021 الساعة 0:59</div>
<div id="home_content">
<div id="flashnews">
<h2><a href="/ar/node/18627">المجلس العربي للإختصاصات الصحية</a></h2><div><p>
	أسس المجلس العربي للاختصاصات الصحية في عام 1978 بموجب قرار مجلس وزراء الصحة العرب بجامعة الدول العربية ويهدف إلى تحسين الخدمات الصحية في الوطن العربي عن طريق رفع المستوى العلمي والعملي في مختلف الاختصاصات ولا يهدف لتحقيق الربح .</p>
</div> </div>
<div id="home-bottom">
<div id="home-rights">
<div class="event_slider-ar"><div class="block block-views" id="block-views-events_block-block_1">
<h2>أنشطة المجلس</h2>
<div class="content">
<div class="view view-events-block view-id-events_block view-display-id-block_1 view-dom-id-3">
<div class="view-content">
<ul class="js-hide jcarousel-skin-tango" id="viewscarousel-events-block-block-1"><li>
<div class="views-field-title">
<span class="field-content"><a href="/ar/content/%D8%A7%D9%84%D8%A7%D9%85%D8%AA%D8%AD%D8%A7%D9%86-%D8%A7%D9%84%D8%B4%D9%81%D9%88%D9%8A-%D9%84%D8%A7%D8%AE%D8%AA%D8%B5%D8%A7%D8%B5-%D8%AA%D8%B5%D9%88%D9%8A%D8%B1-%D9%88%D8%AA%D8%B4%D8%AE%D9%8A%D8%B5-%D8%A3%D9%85%D8%B1%D8%A7%D8%B6-%D8%A7%D9%84%D8%AB%D8%AF%D9%8A-%D8%A8%D8%AA%D8%A7%D8%B1%D9%8A%D8%AE-4-%D9%83%D8%A7%D9%86%D9%88%D9%86-%D8%A7%D9%84%D8%A3%D9%88%D9%84-%D8%AF%D9%8A%D8%B3%D9%85%D8%A8%D8%B1-2020-%D9%85">الامتحان الشفوي لاختصاص تصوير وتشخيص أمراض الثدي...</a></span>
</div>
<div class="views-field-field-date-value">
<span class="field-content"><span class="date-display-single">Fri, 12/04/2020</span></span>
</div>
</li><li>
<div class="views-field-title">
<span class="field-content"><a href="/ar/content/%D8%A7%D9%84%D8%A7%D9%85%D8%AA%D8%AD%D8%A7%D9%86-%D8%A7%D9%84%D8%B4%D9%81%D9%88%D9%8A-%D9%84%D8%A7%D8%AE%D8%AA%D8%B5%D8%A7%D8%B5-%D8%A3%D8%B4%D8%B9%D8%A9-%D8%A7%D9%84%D8%B9%D8%B8%D8%A7%D9%85-%D9%88%D8%A7%D9%84%D8%B9%D8%B6%D9%84%D8%A7%D8%AA-%D8%A8%D8%AA%D8%A7%D8%B1%D9%8A%D8%AE-17-%D9%83%D8%A7%D9%86%D9%88%D9%86-%D8%A7%D9%84%D8%A3%D9%88%D9%84-%D8%AF%D9%8A%D8%B3%D9%85%D8%A8%D8%B1-2020-0">الامتحان الشفوي لاختصاص أشعة العظام والعضلات بتاريخ 17...</a></span>
</div>
<div class="views-field-field-date-value">
<span class="field-content"><span class="date-display-single">Thu, 12/17/2020</span></span>
</div>
</li><li>
<div class="views-field-title">
<span class="field-content"><a href="/ar/content/%D8%A7%D9%84%D8%A7%D9%85%D8%AA%D8%AD%D8%A7%D9%86-%D8%A7%D9%84%D8%B4%D9%81%D9%88%D9%8A-%D9%84%D8%A7%D8%AE%D8%AA%D8%B5%D8%A7%D8%B5-%D8%A3%D8%B4%D8%B9%D8%A9-%D8%A7%D9%84%D8%AC%D9%85%D9%84%D8%A9-%D8%A7%D9%84%D8%B9%D8%B5%D8%A8%D9%8A%D8%A9-%D9%88%D8%A7%D9%84%D8%B1%D8%A3%D8%B3-%D9%88%D8%A7%D9%84%D8%B9%D9%86%D9%82-%D8%A8%D8%AA%D8%A7%D8%B1%D9%8A%D8%AE-18-%D9%83%D8%A7%D9%86%D9%88%D9%86-%D8%A7%D9%84%D8%A3%D9%88%D9%84-%D8%AF%D9%8A%D8%B3%D9%85%D8%A8%D8%B1-202-0">الامتحان الشفوي لاختصاص أشعة الجملة العصبية والرأس...</a></span>
</div>
<div class="views-field-field-date-value">
<span class="field-content"><span class="date-display-single">Fri, 12/18/2020</span></span>
</div>
</li><li>
<div class="views-field-title">
<span class="field-content"><a href="/ar/content/%D8%A7%D9%84%D8%A7%D9%85%D8%AA%D8%AD%D8%A7%D9%86-%D8%A7%D9%84%D9%83%D8%AA%D8%A7%D8%A8%D9%8A-%D8%A7%D9%84%D8%A3%D9%88%D9%84%D9%8A-%D9%84%D8%A7%D8%AE%D8%AA%D8%B5%D8%A7%D8%B5-%D8%B7%D8%A8-%D8%A7%D9%84%D9%85%D8%AC%D8%AA%D9%85%D8%B9-%D8%A8%D8%AA%D8%A7%D8%B1%D9%8A%D8%AE-18%D9%83%D8%A7%D9%86%D9%88%D9%86-%D8%A7%D9%84%D8%AB%D8%A7%D9%86%D9%8A-%D9%8A%D9%86%D8%A7%D9%8A%D8%B1-2021">الامتحان الكتابي الأولي لاختصاص طب المجتمع بتاريخ 18/...</a></span>
</div>
<div class="views-field-field-date-value">
<span class="field-content"><span class="date-display-single">Mon, 01/18/2021</span></span>
</div>
</li><li>
<div class="views-field-title">
<span class="field-content"><a href="/ar/content/%D8%A7%D9%84%D8%A7%D9%85%D8%AA%D8%AD%D8%A7%D9%86-%D8%A7%D9%84%D9%83%D8%AA%D8%A7%D8%A8%D9%8A-%D8%A7%D9%84%D8%A3%D9%88%D9%84%D9%8A-%D9%84%D8%A7%D8%AE%D8%AA%D8%B5%D8%A7%D8%B5-%D8%B7%D8%A8-%D8%A7%D9%84%D8%B7%D9%88%D8%A7%D8%B1%D8%A6-%D8%A8%D8%AA%D8%A7%D8%B1%D9%8A%D8%AE-21%D9%83%D8%A7%D9%86%D9%88%D9%86-%D8%A7%D9%84%D8%AB%D8%A7%D9%86%D9%8A-%D9%8A%D9%86%D8%A7%D9%8A%D8%B1-2021-0">الامتحان الكتابي الأولي لاختصاص طب الطوارئ بتاريخ 21/...</a></span>
</div>
<div class="views-field-field-date-value">
<span class="field-content"><span class="date-display-single">Thu, 01/21/2021</span></span>
</div>
</li><li>
<div class="views-field-title">
<span class="field-content"><a href="/ar/content/%D8%A7%D9%84%D8%A7%D9%85%D8%AA%D8%AD%D8%A7%D9%86-%D8%A7%D9%84%D9%83%D8%AA%D8%A7%D8%A8%D9%8A-%D8%A7%D9%84%D9%86%D9%87%D8%A7%D8%A6%D9%8A-%D9%84%D8%A7%D8%AE%D8%AA%D8%B5%D8%A7%D8%B5-%D8%B7%D8%A8-%D8%A7%D9%84%D8%B7%D9%88%D8%A7%D8%B1%D8%A6-%D8%A8%D8%AA%D8%A7%D8%B1%D9%8A%D8%AE-21%D9%83%D8%A7%D9%86%D9%88%D9%86-%D8%A7%D9%84%D8%AB%D8%A7%D9%86%D9%8A-%D9%8A%D9%86%D8%A7%D9%8A%D8%B1-2021-0">الامتحان الكتابي النهائي لاختصاص طب الطوارئ بتاريخ 21/...</a></span>
</div>
<div class="views-field-field-date-value">
<span class="field-content"><span class="date-display-single">Thu, 01/21/2021</span></span>
</div>
</li><li>
<div class="views-field-title">
<span class="field-content"><a href="/ar/content/%D8%A7%D9%84%D8%A7%D9%85%D8%AA%D8%AD%D8%A7%D9%86-%D8%A7%D9%84%D8%B4%D9%81%D9%88%D9%8A-%D8%A7%D9%84%D8%A7%D9%81%D8%AA%D8%B1%D8%A7%D8%B6%D9%8A-%D9%84%D8%A7%D8%AE%D8%AA%D8%B5%D8%A7%D8%B5-%D8%B7%D8%A8-%D8%A7%D9%84%D8%B9%D9%8A%D9%88%D9%86-%D9%88%D8%AC%D8%B1%D8%A7%D8%AD%D8%AA%D9%87%D8%A7-%D8%A8%D8%AA%D8%A7%D8%B1%D9%8A%D8%AE-28-%E2%80%93-31-%D9%83%D8%A7%D9%86%D9%88%D9%86-%D8%A7%D9%84%D8%AB%D8%A7%D9%86%D9%8A%E2%80%93%D9%8A%D9%86%D8%A7%D9%8A%D8%B1-202-0">الامتحان الشفوي الافتراضي لاختصاص طب العيون وجراحتها...</a></span>
</div>
<div class="views-field-field-date-value">
<span class="field-content"><span class="date-display-single">Thu, 01/28/2021</span></span>
</div>
</li><li>
<div class="views-field-title">
<span class="field-content"><a href="/ar/content/%D8%A7%D9%84%D8%A7%D9%85%D8%AA%D8%AD%D8%A7%D9%86-%D8%A7%D9%84%D8%B3%D8%B1%D9%8A%D8%B1%D9%8A-%D9%84%D8%A7%D8%AE%D8%AA%D8%B5%D8%A7%D8%B5-%D8%AC%D8%B1%D8%A7%D8%AD%D8%A9-%D8%A7%D9%84%D8%AA%D8%AC%D9%85%D9%8A%D9%84-%D9%81%D9%8A-%D8%B9%D9%85%D9%91%D8%A7%D9%86-%D8%A7%D9%84%D9%85%D9%85%D9%84%D9%83%D8%A9-%D8%A7%D9%84%D8%A3%D8%B1%D8%AF%D9%86%D9%8A%D8%A9-%D8%A7%D9%84%D9%87%D8%A7%D8%B4%D9%85%D9%8A%D8%A9-%D8%A8%D8%AA%D8%A7%D8%B1%D9%8A%D8%AE-19-20%D8%B4%D8%A8%D8%A7%D8%B7-%D9%81">الامتحان السريري لاختصاص جراحة التجميل في عمّان -...</a></span>
</div>
<div class="views-field-field-date-value">
<span class="field-content"><span class="date-display-single">Fri, 02/19/2021</span></span>
</div>
</li><li>
<div class="views-field-title">
<span class="field-content"><a href="/ar/content/%D8%A7%D9%84%D8%A7%D9%85%D8%AA%D8%AD%D8%A7%D9%86-%D8%A7%D9%84%D8%B4%D9%81%D9%88%D9%8A-%D9%84%D8%A7%D8%AE%D8%AA%D8%B5%D8%A7%D8%B5-%D8%A7%D9%84%D8%A3%D8%B4%D8%B9%D8%A9-%D9%88%D8%A7%D9%84%D8%AA%D8%B5%D9%88%D9%8A%D8%B1-%D8%A7%D9%84%D8%B7%D8%A8%D9%8A-%D8%A8%D8%AA%D8%A7%D8%B1%D9%8A%D8%AE-28-%D8%B4%D8%A8%D8%A7%D8%B7-%D9%81%D8%A8%D8%B1%D8%A7%D9%8A%D8%B12020-0">الامتحان الشفوي لاختصاص الأشعة والتصوير الطبي بتاريخ...</a></span>
</div>
<div class="views-field-field-date-value">
<span class="field-content"><span class="date-display-single">Sun, 02/28/2021</span></span>
</div>
</li><li>
<div class="views-field-title">
<span class="field-content"><a href="/ar/content/%D8%A7%D9%84%D8%A7%D9%85%D8%AA%D8%AD%D8%A7%D9%86-%D8%A7%D9%84%D8%B3%D8%B1%D9%8A%D8%B1%D9%8A-%D9%84%D8%A7%D8%AE%D8%AA%D8%B5%D8%A7%D8%B5-%D8%A7%D9%84%D8%AC%D8%B1%D8%A7%D8%AD%D8%A9-%D8%A7%D9%84%D8%B9%D8%B5%D8%A8%D9%8A%D8%A9-%D9%81%D9%8A-%D8%A3%D8%B1%D8%A8%D9%8A%D9%84-%D8%AC%D9%85%D9%87%D9%88%D8%B1%D9%8A%D8%A9-%D8%A7%D9%84%D8%B9%D8%B1%D8%A7%D9%82-%D8%A8%D8%AA%D8%A7%D8%B1%D9%8A%D8%AE-28-%E2%80%93-31%D9%85%D8%A7%D9%8A%D9%88-%D8%A3%D9%8A%D8%A7%D8%B12021">الامتحان السريري لاختصاص الجراحة العصبية في أربيل -...</a></span>
</div>
<div class="views-field-field-date-value">
<span class="field-content"><span class="date-display-single">Fri, 05/28/2021</span></span>
</div>
</li></ul> </div>
</div> </div>
</div>
<div class="uptocome-ar"><b>قريباً</b><p>2021-05-28 </p><a href="/ar/node/235461"><h4>الامتحان السريري لاختصاص الجراحة العصبية في أربيل - جمهورية العراق بتاريخ 28 – 31/مايو - أيار/2021</h4></a></div></div>
<div id="home-rights-b">
<div class="grad_div"><a href="/ar/graduates">الخريجون</a></div><div class="unknown_div"><a href="/ar/node/40894">التعليم الطبي المستمر</a></div> </div>
</div>
<div id="home_news"><div class="block block-views" id="block-views-news-block_1">
<h2>أخبار المجلس</h2>
<div class="content">
<div class="view view-news view-id-news view-display-id-block_1 view-dom-id-1">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first">
<div class="views-field-created">
<span class="field-content">01/13/2021 - 01:59</span>
</div>
<div class="views-field-field-image-fid">
<div class="field-content"></div>
</div>
<div class="views-field-title">
<span class="field-content"><a href="/ar/content/%D9%82%D9%88%D8%A7%D8%A6%D9%85-%D8%A7%D8%B3%D9%85%D8%A7%D8%A1-%D8%A7%D9%84%D9%85%D8%AA%D9%82%D8%AF%D9%85%D9%8A%D9%86-%D9%84%D9%84%D8%A7%D9%85%D8%AA%D8%AD%D8%A7%D9%86-%D8%A7%D9%84%D8%A3%D9%88%D9%84%D9%8A-%D9%88%D8%A7%D9%84%D9%86%D9%87%D8%A7%D8%A6%D9%8A-%D8%A7%D9%84%D9%83%D8%AA%D8%A7%D8%A8%D9%8A-%D9%84%D8%A7%D8%AE%D8%AA%D8%B5%D8%A7%D8%B5-%D8%A7%D9%84%D8%A3%D9%85%D8%B1%D8%A7%D8%B6-%D8%A7%D9%84%D8%AC%D9%84%D8%AF%D9%8A%D8%A9-%D9%88%D8%A7%D9%84%D8%AA%D9%86%D8%A7%D8%B3%D9%84%D9%8A%D8%A9-%D8%AF%D9%88">قوائم اسماء المتقدمين للامتحان الأولي والنهائي الكتابي لاختصاص الأمراض الجلدية والتناسلية دورة فبراير 2021</a></span>
</div>
<div class="views-field-field-teaser-value">
<div class="field-content"></div>
</div>
</div>
<div class="views-row views-row-2 views-row-even">
<div class="views-field-created">
<span class="field-content">01/13/2021 - 01:24</span>
</div>
<div class="views-field-field-image-fid">
<div class="field-content"></div>
</div>
<div class="views-field-title">
<span class="field-content"><a href="/ar/content/%D9%82%D8%A7%D8%A6%D9%85%D8%A9-%D8%A7%D8%B3%D9%85%D8%A7%D8%A1-%D8%A7%D9%84%D9%85%D8%AA%D9%82%D8%AF%D9%85%D9%8A%D9%86-%D9%84%D9%84%D8%A7%D9%85%D8%AA%D8%AD%D8%A7%D9%86-%D8%A7%D9%84%D9%83%D8%AA%D8%A7%D8%A8%D9%8A-%D8%A7%D9%84%D8%A3%D9%88%D9%84%D9%8A-%D9%84%D8%A7%D8%AE%D8%AA%D8%B5%D8%A7%D8%B5-%D8%B7%D8%A8-%D8%A7%D9%84%D9%85%D8%AC%D8%AA%D9%85%D8%B9-%D8%A8%D8%AA%D8%A7%D8%B1%D9%8A%D8%AE-18-%D9%8A%D9%86%D8%A7%D9%8A%D8%B1-2021">قائمة اسماء المتقدمين للامتحان الكتابي الأولي لاختصاص طب المجتمع بتاريخ 18 يناير 2021</a></span>
</div>
<div class="views-field-field-teaser-value">
<div class="field-content"></div>
</div>
</div>
<div class="views-row views-row-3 views-row-odd views-row-last">
<div class="views-field-created">
<span class="field-content">01/10/2021 - 14:39</span>
</div>
<div class="views-field-field-image-fid">
<div class="field-content"></div>
</div>
<div class="views-field-title">
<span class="field-content"><a href="/ar/content/%D9%88%D9%8A%D8%A8%D9%8A%D9%86%D8%A7%D8%B1-%D8%AD%D9%88%D9%84-%D8%A7%D8%B3%D8%AA%D8%AE%D8%AF%D8%A7%D9%85-%D8%A7%D9%84%D8%A3%D8%AF%D9%88%D8%A7%D8%AA-%D8%A7%D9%84%D8%AA%D8%B9%D9%84%D9%8A%D9%85%D9%8A%D8%A9-%D9%88%D8%A7%D9%84%D8%A7%D8%AE%D8%AA%D8%A8%D8%A7%D8%B1%D8%A7%D8%AA-%D8%A7%D9%84%D8%A7%D9%84%D9%83%D8%AA%D8%B1%D9%88%D9%86%D9%8A%D8%A9-%D9%84%D9%85%D9%86%D8%B5%D8%A9-%D8%A7%D9%84%D9%85%D8%AC%D9%84%D8%B3-%D8%A7%D9%84%D8%B9%D8%B1%D8%A8%D9%8A">ويبينار حول استخدام الأدوات التعليمية والاختبارات الالكترونية لمنصة المجلس العربي</a></span>
</div>
<div class="views-field-field-teaser-value">
<div class="field-content">يسر اللجنة التنفيذية للتحول الرقمي في الأمانة العامة دعوة سعادة الأساتذة الكرام الى...</div>
</div>
</div>
</div>
<div class="more-link">
<a href="/ar/News">
    عرض كافة الأخبار  </a>
</div>
</div> </div>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="bottom_sep"></div>
<div id="site-footer"><div class="footer_right"><img alt="المجلس العربي للاختصاصات الصحية" src="/sites/all/themes/board/files/arabf.png" style="float:right;margin:0;padding:0;"/><div id="footerfixs">المجلس العربي للاختصاصات الصحية<br/>جميع الحقوق محفوظة </div></div><div class="footer_left"><a alt="أوسكار لتصميم وتطوير المواقع الالكترونية" href="http://www.eoscars.net" id="e-logo" title="أوسكار لتصميم وتطوير المواقع الالكترونية">
<img alt="أوسكار لتصميم وتطوير المواقع الالكترونية" src="/sites/all/themes/board/files/eoscars.jpg" title="أوسكار لتصميم وتطوير المواقع الالكترونية"/>
</a></div></div>
</div>
</body>
</html>