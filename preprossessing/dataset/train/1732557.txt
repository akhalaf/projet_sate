<!DOCTYPE html>
<html lang="en"><head><script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-36544524-1"></script><script>window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());
         gtag('config', 'UA-36544524-1');</script><meta charset="utf-8"/><link href="/favicon.ico" rel="shortcut icon"/><meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport"/><meta content="#28ac4a" name="theme-color"/><meta content="The crowdfunding platform that makes it easy for creatives and innovators in the Arab world to get their work funded" name="description"/><meta content="zoomaal,arab crowdfunding,zumaal,zoomal,zumal,Crowd funding,arab world, crowdfunding arab world, mevp, wamda,n2v,lebanon,ksa,jordan,uae,lebanon crowdfunding, uae crowdfunding, jordan crowdfunding,saudi crowdfunding,egypt crowdfunding,kickstarter,indiegogo" name="keywords"/><link href="/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/><link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/><link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/><link href="/manifest.json" rel="manifest"/><meta content="#da532c" name="msapplication-TileColor"/><meta content="#28ac4a" name="theme-color"/><title>Zoomaal | Ø°ÙÙØ§Ù</title></head><body><noscript>You need to enable JavaScript to run this app.</noscript><div id="root"></div><script>// window.fbAsyncInit = function () {
        //     FB.init({
        //         appId: '1096150180590029',
        //         cookie: true,
        //         xfbml: true,
        //         version: 'v4.0'
        //     });
        //     FB.AppEvents.logPageView();
        //     // Broadcast an event when FB object is ready
        //     var fbInitEvent = new Event('FBObjectReady');
        //     document.dispatchEvent(fbInitEvent);
        // };

        // (function (d, s, id) {
        //     var js, fjs = d.getElementsByTagName(s)[0];
        //     if (d.getElementById(id)) {
        //         return;
        //     }
        //     js = d.createElement(s);
        //     js.id = id;
        //     js.src = "https://connect.facebook.net/en_US/sdk.js";
        //     fjs.parentNode.insertBefore(js, fjs);
        // }(document, 'script', 'facebook-jssdk'));</script><script src="/bundle.js?v=13.1"></script></body></html>