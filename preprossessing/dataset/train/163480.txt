<!DOCTYPE html>
<html class="no-js" lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<title>
  Asthmatic Kitty Records  </title>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<link href="https://asthmatickitty.com/wp-content/themes/asthmatic-kitty/style.css" rel="stylesheet" type="text/css"/>
<link href="https://asthmatickitty.com/xmlrpc.php" rel="pingback"/>
<!--  
  <script type="text/javascript" src="https://fast.fonts.com/jsapi/52652f81-bd9c-414a-886e-6c8eba06673c.js"></script>
  -->
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
_fbq.push(['addPixelId', '955163521167275']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img alt="" height="1" src="https://www.facebook.com/tr?id=955163521167275&amp;ev=PixelInitialized" style="display:none" width="1"/></noscript>
<!-- Fathom - simple website analytics -->
<script>
(function(f, a, t, h, o, m){
	a[h]=a[h]||function(){
		(a[h].q=a[h].q||[]).push(arguments)
	};
	o=f.createElement('script'),
	m=f.getElementsByTagName('script')[0];
	o.async=1; o.src=t; o.id='fathom-script';
	m.parentNode.insertBefore(o,m)
})(document, window, '//akr.usesfathom.com/tracker.js', 'fathom');
fathom('trackPageview');
</script>
<!-- / Fathom -->
<!-- Colorbox stylesheet
  <link rel="stylesheet" href="https://asthmatickitty.com/wp-content/themes/asthmatic-kitty/js/colorbox/colorbox.css"  type="text/css" media="screen" />
  -->
<!--[if lt IE 9]>
    <script src="https://asthmatickitty.com/wp-content/themes/asthmatic-kitty/js/html5.js" type="text/javascript"></script>
  <![endif]-->
<link href="//asthmatickitty.com" rel="dns-prefetch"/>
<link href="//secure.gravatar.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://asthmatickitty.com/asthmatic-kitty-records/feed/" rel="alternate" title="Asthmatic Kitty Records » Home Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/asthmatickitty.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.2"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://c0.wp.com/c/5.4.2/wp-includes/css/dist/block-library/style.min.css" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<style id="wp-block-library-inline-css" type="text/css">
.has-text-align-justify{text-align:justify;}
</style>
<link href="https://c0.wp.com/c/5.4.2/wp-includes/css/dist/components/style.min.css" id="wp-components-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Noto+Serif%3A400%2C400i%2C700%2C700i&amp;ver=5.4.2" id="wp-editor-font-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://c0.wp.com/c/5.4.2/wp-includes/css/dist/block-editor/style.min.css" id="wp-block-editor-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://c0.wp.com/c/5.4.2/wp-includes/css/dist/nux/style.min.css" id="wp-nux-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://c0.wp.com/c/5.4.2/wp-includes/css/dist/editor/style.min.css" id="wp-editor-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://c0.wp.com/c/5.4.2/wp-includes/css/dist/block-library/theme.min.css" id="wp-block-library-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://c0.wp.com/c/5.4.2/wp-includes/css/dist/block-library/editor.min.css" id="wp-edit-blocks-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://asthmatickitty.com/wp-content/plugins/embed-sendy/dist/blocks.style.build.css?ver=5.4.2" id="embed_sendy-cgb-style-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://asthmatickitty.com/wp-content/plugins/embed-sendy/assets/embed-sendy.css?ver=1.1" id="embed-sendy-css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://asthmatickitty.com/wp-content/plugins/search-autocomplete/css/redmond/jquery-ui-1.9.2.custom.min.css?ver=1.9.2" id="SearchAutocomplete-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://c0.wp.com/p/jetpack/8.6.1/css/jetpack.css" id="jetpack_css-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://c0.wp.com/c/5.4.2/wp-includes/js/jquery/jquery.js" type="text/javascript"></script>
<script src="https://c0.wp.com/c/5.4.2/wp-includes/js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
<link href="https://asthmatickitty.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://asthmatickitty.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://asthmatickitty.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.2" name="generator"/>
<link href="https://asthmatickitty.com/" rel="canonical"/>
<link href="https://asthmatickitty.com/" rel="shortlink"/>
<link href="https://asthmatickitty.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fasthmatickitty.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://asthmatickitty.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fasthmatickitty.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<link href="//i0.wp.com" rel="dns-prefetch"/>
<link href="//i1.wp.com" rel="dns-prefetch"/>
<link href="//i2.wp.com" rel="dns-prefetch"/>
<link href="//c0.wp.com" rel="dns-prefetch"/>
<!-- Jetpack Open Graph Tags -->
<meta content="website" property="og:type"/>
<meta content="Asthmatic Kitty Records" property="og:title"/>
<meta content="https://asthmatickitty.com/" property="og:url"/>
<meta content="Asthmatic Kitty Records" property="og:site_name"/>
<meta content="https://i2.wp.com/asthmatickitty.com/wp-content/uploads/2020/06/cropped-AKR-logo-transparent_512-1.png?fit=512%2C512&amp;ssl=1" property="og:image"/>
<meta content="512" property="og:image:width"/>
<meta content="512" property="og:image:height"/>
<meta content="en_US" property="og:locale"/>
<meta content="Home" name="twitter:text:title"/>
<meta content="https://i2.wp.com/asthmatickitty.com/wp-content/uploads/2020/06/cropped-AKR-logo-transparent_512-1.png?fit=240%2C240&amp;ssl=1" name="twitter:image"/>
<meta content="summary" name="twitter:card"/>
<meta content="Visit the post for more." name="twitter:description"/>
<!-- End Jetpack Open Graph Tags -->
<link href="https://i2.wp.com/asthmatickitty.com/wp-content/uploads/2020/06/cropped-AKR-logo-transparent_512-1.png?fit=32%2C32&amp;ssl=1" rel="icon" sizes="32x32"/>
<link href="https://i2.wp.com/asthmatickitty.com/wp-content/uploads/2020/06/cropped-AKR-logo-transparent_512-1.png?fit=192%2C192&amp;ssl=1" rel="icon" sizes="192x192"/>
<link href="https://i2.wp.com/asthmatickitty.com/wp-content/uploads/2020/06/cropped-AKR-logo-transparent_512-1.png?fit=180%2C180&amp;ssl=1" rel="apple-touch-icon"/>
<meta content="https://i2.wp.com/asthmatickitty.com/wp-content/uploads/2020/06/cropped-AKR-logo-transparent_512-1.png?fit=270%2C270&amp;ssl=1" name="msapplication-TileImage"/>
</head>
<body class="home page-template page-template-page-homepage page-template-page-homepage-php page page-id-5">
<div class="hfeed container" id="page">
<header>
<div class="header-logo"></div>
<h1 id="site-title"><span><a href="https://asthmatickitty.com/" rel="home" title="Asthmatic Kitty Records">Asthmatic Kitty Records</a></span></h1>
<nav id="access" role="navigation">
<div class="skip-link"><a class="assistive-text" href="#content" title="Skip to primary content">Skip to primary content</a></div>
<div class="skip-link"><a class="assistive-text" href="#secondary" title="Skip to secondary content">Skip to secondary content</a></div>
<div class="menu-main-menu-container"><ul class="menu" id="menu-main-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-home menu-item-7456" id="menu-item-7456"><a href="http://asthmatickitty.com">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7368" id="menu-item-7368"><a href="https://asthmatickitty.com/artists/">Artists</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7373" id="menu-item-7373"><a href="https://asthmatickitty.com/news/">News</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7369" id="menu-item-7369"><a href="https://asthmatickitty.com/info/">Info</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7370" id="menu-item-7370"><a href="https://asthmatickitty.com/tours/">Tours</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8034" id="menu-item-8034"><a href="http://store.asthmatickitty.com/">Store</a></li>
</ul></div> </nav>
</header>
<div class="sub-header">
<div class="widget social">
<a href="https://www.facebook.com/asthmatickitty">Facebook</a>
<a href="https://twitter.com/asthmatickitty">Twitter</a>
<a href="https://instagram.com/asthmatickittyrecords">Instagram</a>
</div>
<div class="widget newsletter">
<a href="https://asthmatickitty.com/subscribe/">Click to subscribe for news</a>
</div>
<div class="widget search">
<form action="https://asthmatickitty.com/" class="searchform" id="searchform" method="get" role="search">
<div>
<label class="screen-reader-text" for="s">Search for:</label>
<input id="s" name="s" type="text" value=""/>
<input id="searchsubmit" type="submit" value="Search"/>
</div>
</form></div>
<div class="widget cart">
<a class="cart" href="http://store.asthmatickitty.com/cart">View cart</a>
</div>
</div>
<div id="main"><div class="content primary">
<!-- showcase -->
<div class="page-showcase">
<div class="image">
<a href="https://asthmatickitty.com/denison-witmers-simplified-ep-out-now/" rel="bookmark" title="Permanent Link to Denison Witmer’s Simplified EP Out Now"><img alt="" src="https://asthmatickitty.com/wp-content/uploads/2021/01/Denison-Witmer-American-Foursquare-Simplified-EP-YOUTUBE.jpg"/></a>
</div>
<div style="clear: both;"></div>
<div class="post">
<h1><a href="https://asthmatickitty.com/denison-witmers-simplified-ep-out-now/" rel="bookmark" title="Permanent Link to Denison Witmer’s Simplified EP Out Now">
          Denison Witmer’s Simplified EP Out Now</a>
</h1>
<div class="post-content">
<a href="https://asthmatickitty.com/denison-witmers-simplified-ep-out-now/"><p>Features songs with Karen and Don Paris and Rosie Thomas</p>
</a>
<!-- <div class="more"><a href="https://asthmatickitty.com/denison-witmers-simplified-ep-out-now/">Read more...</a></div>  -->
</div>
</div>
</div>
<div style="clear: both;"></div>
</div>
<ul class="sub-showcase">
<li>
<a href="https://asthmatickitty.com/the-welcome-wagon-share-new-song-and-video-i-can-cross-the-sea/" rel="bookmark" title="Permanent Link to The Welcome Wagon Share New Song and Video, “I Can Cross the Sea”"><img alt="" src="https://asthmatickitty.com/wp-content/uploads/2020/10/Screen-Shot-2020-10-28-at-4.14.21-PM.jpg"/></a>
<h1><a href="https://asthmatickitty.com/the-welcome-wagon-share-new-song-and-video-i-can-cross-the-sea/" rel="bookmark" title="Permanent Link to The Welcome Wagon Share New Song and Video, “I Can Cross the Sea”">
     The Welcome Wagon Share New Song and Video, “I Can Cross the Sea”</a>
</h1>
<div class="post-content">
<!--- <a href="https://asthmatickitty.com/the-welcome-wagon-share-new-song-and-video-i-can-cross-the-sea/"><p>&#8220;This is a song we wrote for people who have to cross great seas and bear great burdens.&#8221;</p>
</a> --->
<!-- <div class="more"><a href="https://asthmatickitty.com/the-welcome-wagon-share-new-song-and-video-i-can-cross-the-sea/">Read more...</a></div>  -->
</div>
</li>
<li>
<a href="https://asthmatickitty.com/sufjans-the-ascension-available-now/" rel="bookmark" title="Permanent Link to Sufjan’s The Ascension Available Now"><img alt="" src="https://asthmatickitty.com/wp-content/uploads/2020/09/akr150_full.jpg"/></a>
<h1><a href="https://asthmatickitty.com/sufjans-the-ascension-available-now/" rel="bookmark" title="Permanent Link to Sufjan’s The Ascension Available Now">
     Sufjan’s The Ascension Available Now</a>
</h1>
<div class="post-content">
<!--- <a href="https://asthmatickitty.com/sufjans-the-ascension-available-now/"><p>Double LP, CD, and tape cassette available now</p>
</a> --->
<!-- <div class="more"><a href="https://asthmatickitty.com/sufjans-the-ascension-available-now/">Read more...</a></div>  -->
</div>
</li>
</ul>
<div style="clear: both;"></div>
</div>
<footer>
<div class="footer-left">
<div class="widget widget_text" id="text-3"> <div class="textwidget"><p><a href="http://asthmatickitty.com/contact/">Click here to contact us</a>.</p>
<p>Asthmatic Kitty Records Post Office Box 1282 Lander, WY 82520 USA</p>
<p><a href="http://asthmatickitty.com/info/privacy-policy/">Click to view our privacy policy</a>.</p>
</div>
</div> </div>
<div class="footer-right">
<img src="/images/kittyanimated.gif"/>
</div>
</footer><!-- #colophon -->
</div><!-- #page -->
<script src="https://c0.wp.com/p/jetpack/8.6.1/_inc/build/photon/photon.min.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var esdSettings = {"ajaxurl":"https:\/\/asthmatickitty.com\/wp-admin\/admin-ajax.php","url":"https:\/\/asthmatickitty.com\/sendy\/subscribe","successMessage":"","alreadySubscribed":""};
/* ]]> */
</script>
<script src="https://asthmatickitty.com/wp-content/plugins/embed-sendy/assets/embed-sendy.js?ver=1.1" type="text/javascript"></script>
<script src="https://c0.wp.com/c/5.4.2/wp-includes/js/jquery/ui/core.min.js" type="text/javascript"></script>
<script src="https://c0.wp.com/c/5.4.2/wp-includes/js/jquery/ui/widget.min.js" type="text/javascript"></script>
<script src="https://c0.wp.com/c/5.4.2/wp-includes/js/jquery/ui/position.min.js" type="text/javascript"></script>
<script src="https://c0.wp.com/c/5.4.2/wp-includes/js/jquery/ui/menu.min.js" type="text/javascript"></script>
<script src="https://c0.wp.com/c/5.4.2/wp-includes/js/dist/vendor/wp-polyfill.min.js" type="text/javascript"></script>
<script type="text/javascript">
( 'fetch' in window ) || document.write( '<script src="https://asthmatickitty.com/wp-includes/js/dist/vendor/wp-polyfill-fetch.min.js?ver=3.0.0"></scr' + 'ipt>' );( document.contains ) || document.write( '<script src="https://asthmatickitty.com/wp-includes/js/dist/vendor/wp-polyfill-node-contains.min.js?ver=3.42.0"></scr' + 'ipt>' );( window.DOMRect ) || document.write( '<script src="https://asthmatickitty.com/wp-includes/js/dist/vendor/wp-polyfill-dom-rect.min.js?ver=3.42.0"></scr' + 'ipt>' );( window.URL && window.URL.prototype && window.URLSearchParams ) || document.write( '<script src="https://asthmatickitty.com/wp-includes/js/dist/vendor/wp-polyfill-url.min.js?ver=3.6.4"></scr' + 'ipt>' );( window.FormData && window.FormData.prototype.keys ) || document.write( '<script src="https://asthmatickitty.com/wp-includes/js/dist/vendor/wp-polyfill-formdata.min.js?ver=3.0.12"></scr' + 'ipt>' );( Element.prototype.matches && Element.prototype.closest ) || document.write( '<script src="https://asthmatickitty.com/wp-includes/js/dist/vendor/wp-polyfill-element-closest.min.js?ver=2.0.2"></scr' + 'ipt>' );
</script>
<script src="https://c0.wp.com/c/5.4.2/wp-includes/js/dist/dom-ready.min.js" type="text/javascript"></script>
<script src="https://c0.wp.com/c/5.4.2/wp-includes/js/dist/a11y.min.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var uiAutocompleteL10n = {"noResults":"No results found.","oneResult":"1 result found. Use up and down arrow keys to navigate.","manyResults":"%d results found. Use up and down arrow keys to navigate.","itemSelected":"Item selected."};
/* ]]> */
</script>
<script src="https://c0.wp.com/c/5.4.2/wp-includes/js/jquery/ui/autocomplete.min.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var SearchAutocomplete = {"ajaxurl":"https:\/\/asthmatickitty.com\/wp-admin\/admin-ajax.php","fieldName":"#s","minLength":"3","delay":"500","autoFocus":"false"};
/* ]]> */
</script>
<script src="https://asthmatickitty.com/wp-content/plugins/search-autocomplete/js/search-autocomplete.min.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://c0.wp.com/c/5.4.2/wp-includes/js/wp-embed.min.js" type="text/javascript"></script>
<script async="async" defer="defer" src="https://stats.wp.com/e-202102.js" type="text/javascript"></script>
<script type="text/javascript">
	_stq = window._stq || [];
	_stq.push([ 'view', {v:'ext',j:'1:8.6.1',blog:'41257380',post:'5',tz:'-4',srv:'asthmatickitty.com'} ]);
	_stq.push([ 'clickTrackerInit', '41257380', '5' ]);
</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
	var $mcGoal = {'settings':{'uuid':'8b99b26102810d538744c3fa2','dc':'us1'}};
	(function() {
		 var sp = document.createElement('script'); sp.type = 'text/javascript'; sp.async = true; sp.defer = true;
		sp.src = ('https:' == document.location.protocol ? 'https://s3.amazonaws.com/downloads.mailchimp.com' : 'http://downloads.mailchimp.com') + '/js/goal.min.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sp, s);
	})(); 
</script>
<!-- <script src="https://asthmatickitty.com/wp-content/themes/asthmatic-kitty/js/jquery.tools.min.js"></script> -->
<script src="https://asthmatickitty.com/wp-content/themes/asthmatic-kitty/js/plugins.js"></script>
<script src="https://asthmatickitty.com/wp-content/themes/asthmatic-kitty/js/scripts.js"></script>
<script type="text/javascript">var _merchantSettings=_merchantSettings || [];_merchantSettings.push(['AT', '10lJLG']);(function(){var autolink=document.createElement('script');autolink.type='text/javascript';autolink.async=true; autolink.src= ('https:' == document.location.protocol) ? 'https://autolinkmaker.itunes.apple.com/js/itunes_autolinkmaker.js' : 'http://autolinkmaker.itunes.apple.com/js/itunes_autolinkmaker.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(autolink, s);})();</script>
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">
twttr.conversion.trackPid('l5syv', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
<noscript>
<img alt="" height="1" src="https://analytics.twitter.com/i/adsct?txn_id=l5syv&amp;p_id=Twitter&amp;tw_sale_amount=0&amp;tw_order_quantity=0" style="display:none;" width="1"/>
<img alt="" height="1" src="//t.co/i/adsct?txn_id=l5syv&amp;p_id=Twitter&amp;tw_sale_amount=0&amp;tw_order_quantity=0" style="display:none;" width="1"/></noscript>
</body>
</html>