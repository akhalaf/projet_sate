<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta content="noindex,nofollow,noarchive" name="robots"/><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="en" http-equiv="Content-Language"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>403 Forbidden</title>
<meta content="Page Forbidden" name="description"/>
<meta content="" name="keywords"/>
<meta content="C4C24015E0D4FE0056CABBA2B775D74E" name="msvalidate.01"/>
<link href="https://plus.google.com/u/0/b/101916043644470275583/101916043644470275583" rel="publisher"/>
<link href="/common/files/css/styleheader.css" rel="stylesheet" type="text/css"/>
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="/common/files/css/ie.css">
<![endif]-->
</head>
<body>
<div id="divloading" style="display:none;">
</div>
<div class="fixed">
<div class="social-media">
<div class="top-left">
</div>
</div>
<div class="clearfix header_round">
<div class="header_center">
<div class="header_center_2 clearfix">
<div class="tab_container">
<div class="tab_content" id="tab1">
<form action="/search" name="search" onsubmit="return checklocalsearch(this);">
<div class="logo"><a href="/"><img alt="b2bYellowpages.com - Business to Business Yellow Pages" border="0" src="/common/files/images/b2b-logo-new.png" title="b2bYellowpages.com - Business to Business Yellow Pages"/></a></div>
<div class="home-textbox">
<div class="hourglass"></div>
<ul class="inner-wrapper">
<li>
<ul class="list-me">
<li><a alt="List Your Site" href="/promote" rel="nofollow" title="List Your Site">List Your Site</a></li>
<li><a alt="Member Directory" href="/directory/" title="Member Directory">Member Directory</a></li>
<li><a alt="Local Services" href="/local-services.shtml" id="local-services" title="Local Services">Local Services</a></li>
<li><a alt="US Business Directory" href="/business-directory.shtml" title="US Business Directory">US Business Directory</a></li>
<li><a alt="Zip Code Directory" href="/zip-codes.shtml" title="Zip Code Directory">Zip Code Directory</a></li>
</ul>
</li>
<li>
<div class="what"></div>
<input class="menu-input" name="business" placeholder="What?" type="text" value=""/>
<div class="example">Ex: Accountants, Lawyers, Dentists</div>
</li>
<li>
<div class="where"></div>
<input class="menu-input" name="location" placeholder="Where? City, State, County or Zip" type="text" value=""/>
<div class="example">Location: Street, City, State, County or Zip Code</div>
</li>
<li>
<input class="search-btn" type="submit" value=""/>
</li>
</ul>
</div>
</form>
</div>
<div class="mobilenav">
<ul>
<li><a href="/" title="Home">Home</a></li>
<li><a href="/promote" rel="nofollow" title="List Your Site">List Your Site</a></li>
<li><a href="/directory/" title="Member Directory">Member Directory</a></li>
<li><a href="/local-services.shtml" title="Local Services">Local Services</a></li>
<li><a href="/business-directory.shtml" title="US Business Directory">US Business Directory</a></li>
<li><a href="/zip-codes.shtml" title="Zip Code Directory">Zip Code Directory</a></li>
</ul>
</div>
</div></div>
</div>
</div>
</div>
<div class="relative-all">
<div class="wrapper"><div class="clearfix content_center">
<div class="staticpages">
<h1>Blocked</h1><br/><br/>Sorry, but we are currently blocking access to our site from your IP <b>210.75.253.169</b>.<br/><br/>This could be the result of either:<br/><br/>1.) We have detected an unusual amount of improper or excessive behavior. This is most likely the reason if you were using our site to compile business lists, company information, accumulate sales leads, etc. This is a breach of our <a href="/terms.shtml">Terms Of Use.</a>. <br/><br/>2.) The IP is part of a virtual private network (VPN) to mask your actual IP, or your IP was masked and served a proxy IP. <br/><br/>3.) You are located outside of the United States. Our database is over 16 million businesses that are solely located in the United States. As such, our business model focuses on connecting and servicing buyers and sellers that are located only in the US.<br/><br/>If you believe our view of your activity is unwarranted, please feel free to email us at "blocked at b2byellowpages.com". <br/><i><small>Replace the word 'at' with @ to ensure proper email format. (The word 'at' was used here to prevent spam.)</small></i><br/><br/>Be sure to:<br/><br/>1. Include the IP shown on the top of this message page.<br/>2. Give an example of a web page you are trying to access. (You can copy and paste from the URL address bar.)<br/>3. Place the word 'Blocked' in the subject line.<br/><br/><br/>Thank you,<br/><br/>b2bYellowpages.com<br/>Engineering Team<br/>
</div>
</div>
</div>
<div class="footer-wrapper">
<div class="footer">
<div class="clearfix copyright">
<div class="bottom-list">
<ul>
<li><a href="/promote" rel="nofollow"><b>List Your Site</b></a></li>
<li><a href="/member" rel="nofollow"><b><b>Member Login</b></b></a></li>
<li><a href="/ourstory.shtml">About Us</a></li>
<li><a href="/help.shtml">Help &amp; FAQ's</a></li>
<li><a href="/contact.shtml">Contact Us</a></li>
<li><a href="/terms.shtml">Terms of Use</a></li>
<li><a href="/privacy.shtml">Privacy Policy</a></li>
<li><a href="/sitemap.shtml">Site Map</a></li>
</ul>
</div>
<br/>
<span>© 1999-<script type="text/javascript">document.write(new Date().getFullYear());</script><noscript>2017</noscript> b2bYellowpages.com® - <span>All rights reserved.</span></span><br/>
		b2bYellowpages.com logo and all related marks are trademarks of b2bBiz.com, Inc. All other marks are the property of their respective owners
	</div>
</div>
</div>
<script defer="" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script defer="" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" type="text/javascript"></script>
<script defer="" src="/common/files/js/scripts.js" type="text/javascript"></script>
</div></body>
</html>