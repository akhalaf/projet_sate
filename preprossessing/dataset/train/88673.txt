<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="max-age=0" http-equiv="Cache-control"/>
<title>Air Facts Journal</title>
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700|Roboto:400,500,300,300italic" rel="stylesheet"/>
<style type="text/css">
		html, body {
			margin: 0;
			padding: 0;
			min-width: 100%;
			width: 100%;
			max-width: 100%;
			min-height: 100%;
			height: 100%;
			max-height: 100%;
		}

		.wp-defender {
			height: 100%;
			display: flex;
			align-items: center;
			font-family: Roboto;
			color: #000;
			font-size: 13px;
			line-height: 18px;
		}

		.container {
			margin: 0 auto;
			text-align: center;
		}

		.image {
			width: 128px;
			height: 128px;
			background-color: #F2F2F2;
			margin: 0 auto;
			border-radius: 50%;
			background-image: url("https://airfactsjournal.com/wp-content/plugins/wp-defender/assets/img/def-stand.svg");
			background-repeat: no-repeat;
			background-size: contain;
			margin-bottom: 30px;
		}

		.powered {
			position: absolute;
			bottom: 20px;
			display: block;
			text-align: center;
			width: 100%;
			font-size: 10px;
			color: #C0C0C0;
		}

		.powered strong {
			color: #8A8A8A;
			font-weight: normal;
		}
	</style>
</head>
<body>
<div class="wp-defender">
<div class="container">
<div class="image"></div>
<p>The administrator has blocked your IP from accessing this website.</p>
</div>
<div class="powered">Powered by		<strong>Defender</strong>
</div>
</div>
</body>
</html>