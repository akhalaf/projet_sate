<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Vlada Macek Content Organizer, http://macek.sandbox.cz" name="generator"/>
<meta content="aTeo koláže fotokoláže collages photocollages collaging photocollaging humor humour" name="keywords"/>
<title>aTeo - fotokoláže a jiné úlety</title>
<link href="/favicon.ico" rel="shortcut icon"/>
<link href="/s/css/style.css" rel="stylesheet" type="text/css"/>
<link href="/s/css/style_head_small.css" rel="stylesheet" type="text/css"/>
<!--[if lte IE 6]>
	 <link rel="stylesheet" type="text/css" href="/s/css/style_ie6.css" />
	<![endif]-->
<!--[if gte IE 7]>
	 <link rel="stylesheet" type="text/css" href="/s/css/style_ie7.css" />
	<![endif]-->
<link href="/feed/rss/" rel="alternate" title="Zdroj RSS pro aTeo.cz" type="application/rss+xml"/>
</head>
<body>
<div id="container">
<div id="head">
<!--		<div id="logo">
			<h1><a href="/">aTeo.cz</a></h1>
			<p>fotokoláže a jiné úlety</p>
		</div> -->
<div id="thumb">
<a href="#" id="img_link" title="Klikni pro zvetšení obrázku"><img alt="" height="190" id="img_thumb" src="/s/img/no_image.gif" width="190"/></a>
</div>
</div>
<div id="menu">
<h2>Hlavní menu</h2>
<ul>
<li><a href="/pg/home/">Úvod</a></li>
<li><a href="/obrazky/">Obrázky
				<!--[if IE 7]><!--> </a> <!--<![endif]-->
<!--[if lte IE 6]><table><tr><td><![endif]-->
<ul>
<li><a href="/obrazky/">Všechny</a></li>
<li><a href="/favorite/">Favorité</a></li>
<li><a href="/celebrity/"><span style="margin-left: 0px">Celebrity</span></a></li>
<li><a href="/celebrity_politici/"><span style="margin-left: 15px">Poblitici</span></a></li>
<li><a href="/celebrity_ostatni/"><span style="margin-left: 15px">Ostatní</span></a></li>
<li><a href="/legracky/"><span style="margin-left: 0px">Legrácky</span></a></li>
<li><a href="/legracky_atentaty-na-umeni/"><span style="margin-left: 15px">ARTentáty</span></a></li>
<li><a href="/legracky_genetika/"><span style="margin-left: 15px">Genetika</span></a></li>
<li><a href="/legracky_politika/"><span style="margin-left: 15px">Poblitika</span></a></li>
<li><a href="/legracky_ostatni/"><span style="margin-left: 15px">Ostatní</span></a></li>
<li><a href="/pohlednice/"><span style="margin-left: 0px">Pohlednice</span></a></li>
<li><a href="/ostatni/"><span style="margin-left: 0px">Ostatní</span></a></li>
<li><a href="/hanbarny/"><span style="margin-left: 0px">Hanbárny</span></a></li>
<li><a href="/archiv/"><span style="margin-left: 0px">Archiv</span></a></li>
<li><a href="/soukrome/"><span style="margin-left: 0px">Soukromé</span></a></li>
<li><a href="/bublinada/"><span style="margin-left: 0px">Bublináda</span></a></li>
<li><a href="/klicova-slova/">Dle klíčových slov</a></li>
</ul>
<!--[if lte IE 6]></td></tr></table></a><![endif]-->
</li>
<li><a href="/texty/">Texty
				<!--[if IE 7]><!--> </a> <!--<![endif]-->
<!--[if lte IE 6]><table><tr><td><![endif]-->
<ul>
<li><a href="/texty/">Všechny</a></li>
<li><a href="/texty/mikropovidky"><span style="margin-left: 0px">Mikropovídky</span></a></li>
<li><a href="/texty/typologie"><span style="margin-left: 0px">Typologie</span></a></li>
<li><a href="/texty/pseudouvahy"><span style="margin-left: 0px">Pseudoúvahy</span></a></li>
<li><a href="/texty/pribehy"><span style="margin-left: 0px">Příběhy</span></a></li>
</ul>
<!--[if lte IE 6]></td></tr></table></a><![endif]-->
</li>
<li><a href="/pg/links/">Odkazy</a></li>
<li><a href="/pg/contacts/">Kontakty</a></li>
</ul>
<div class="clear"></div>
<h3>Hledat</h3>
<form action="/obrazky/" method="get" name="frm_search">
<h3>RSS Feed</h3>
<a class="rss-feed-link" href="/feed/rss/" target="_blank">RSS</a>
<input id="search" name="q" type="text" value=""/>
<input class="btn" type="submit" value="Hledej"/>
</form>
</div>
<div id="main">
<div id="main_top">
<p class="right">
			Nelze-li něco vysvětlit pomocí­ Murphyho zákonů, pak TO neexistuje.<br/>
			Pokud TO navzdory výše uvedenému tvrzení­ existuje, ignorujte TO!</p>
</div>
<h2>
<span class="hdr-subtitle">Kategorie:</span> Všechny obrázky
			
		
		</h2>
<div class="clear"></div>
<table border="0" cellspacing="0" class="thumbTab">
<tr>
<td title="Abbey Road"><a href="/i/7693/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/7693/', '/f/t/images/b/BeatlesEmpireState_jpg_190x190_q75.jpg', '190', '120');" src="/f/t/thumbnails/a/abro_p05JMEf_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
<td class="empty-cell">15.08.2017</td>
<td title="The Lord of Peace"><a href="/i/7062/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/7062/', '/f/t/images/p/putinlordofpeace_jpg_190x190_q75.jpg', '190', '138');" src="/f/t/thumbnails/n/nabojnice_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
<td title="Sado-Maso"><a href="/i/7064/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/7064/', '/f/t/images/s/sodomashow_jpg_190x190_q75.jpg', '190', '126');" src="/f/t/thumbnails/b/bohusoc_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
<td class="empty-cell">13.08.2017</td>
<td title="Lochneska"><a href="/i/6877/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/6877/', '/f/t/images/l/lochness_jpg_190x190_q75.jpg', '151', '190');" src="/f/t/thumbnails/c/clun_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
<td title="Někdo se dívá..."><a href="/i/6979/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/6979/', '/f/t/images/g/GiraffeView_jpg_190x190_q75.jpg', '142', '190');" src="/f/t/thumbnails/z/zirafka_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
<td title="Chovanec"><a href="/i/7692/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/7692/', '/f/t/images/s/SvejkChovanec_jpg_190x190_q75.jpg', '190', '121');" src="/f/t/thumbnails/1/170809_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
</tr><tr>
<td title="PapaGay"><a href="/i/6607/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/6607/', '/f/t/images/p/papa-gay_jpg_190x190_q75.jpg', '190', '135');" src="/f/t/thumbnails/m/macaw_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
<td title="Totální vyprázdněnost"><a href="/i/7547/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/7547/', '/f/t/images/v/vyprazdnenost_jpg_190x190_q75.jpg', '190', '123');" src="/f/t/thumbnails/n/nebe_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
<td class="empty-cell">07.08.2017</td>
<td title="Auditorium"><a href="/i/6994/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/6994/', '/f/t/images/a/auditorium_jpg_190x190_q75.jpg', '190', '130');" src="/f/t/thumbnails/o/opona_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
<td title="CD obal"><a href="/i/5758/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/5758/', '/f/t/images/i/icaros_jpg_190x190_q75.jpg', '190', '190');" src="/f/t/thumbnails/i/ik_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
<td class="empty-cell">03.08.2017</td>
<td title="Chucky"><a href="/i/5936/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/5936/', '/f/t/images/c/ChuckyBaby_jpg_190x190_q75.jpg', '139', '190');" src="/f/t/thumbnails/y/y_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
<td title="Nun Show"><a href="/i/5450/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/5450/', '/f/t/images/n/nunshow_jpg_190x190_q75.jpg', '124', '190');" src="/f/t/thumbnails/j/jept_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
</tr><tr>
<td title="Rudolapka"><a href="/i/5927/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/5927/', '/f/t/images/m/masozravka_jpg_190x190_q75.jpg', '129', '190');" src="/f/t/thumbnails/k/ksznak_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
<td title="Ano, bude jim líp"><a href="/i/7690/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/7690/', '/f/t/images/b/BabisovicTitanic_jpg_190x190_q75.jpg', '190', '125');" src="/f/t/thumbnails/t/titalogo_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
<td title="Geiša"><a href="/i/6448/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/6448/', '/f/t/images/g/Geisha_jpg_190x190_q75.jpg', '190', '134');" src="/f/t/thumbnails/i/ivazeman_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
<td class="empty-cell">28.07.2017</td>
<td title="Pogrom"><a href="/i/6784/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/6784/', '/f/t/images/p/PutinPogrom_jpg_190x190_q75.jpg', '190', '133');" src="/f/t/thumbnails/v/vovput_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
<td title="Christ the Redeemer "><a href="/i/5796/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/5796/', '/f/t/images/c/ChristRedeemer_jpg_190x190_q75.jpg', '190', '120');" src="/f/t/thumbnails/c/chrcorc_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
<td class="empty-cell">26.07.2017</td>
<td title="Světýlko na konci tunelu"><a href="/i/6193/" title=""><img alt="" height="100" onclick="slowlow()" onmouseout="slowlow(this)" onmouseover="slowhigh(this, '/i/6193/', '/f/t/images/t/tunnel-light_jpg_190x190_q75.jpg', '129', '190');" src="/f/t/thumbnails/c/ChurchWindow_jpg_100x100_crop_upscale_q85.jpg" width="100"/></a>
<p> </p></td>
</tr>
</table>
<div id="viewer-nav">
<a class="first"><span>←←První</span></a>
<a class="back"><span>←Předchozí</span></a>
<span class="info">1 / 208</span>
<a class="next" href="/obrazky/strana/2/"><span>Následující→</span></a>
<a class="last" href="/obrazky/strana/208/"><span>Poslední→</span></a>
</div>
</div>
<div>
<p id="credit">Software: <a href="http://macek.sandbox.cz" target="_blank">macek.sandbox.cz</a> </p>
</div>
</div>
<script src="/s/js/gradual.js" type="text/javascript"></script>
<script type="text/javascript">
	try {
		/* Opens all external links in new window. */
		(function () {
			var hostname = window.location.hostname.replace('www.', '').toLowerCase();
			var i = document.links.length;
			while (--i >= 0) {
				var a = document.links[i];
				var href = a.href.toLowerCase();
				if (href.indexOf('http://') == 0) {
					href = href.slice(7);
					if (href.indexOf('www.') == 0) {
						href = href.slice(4)
					}
					if (href.indexOf(hostname) != 0) {
						a.target = '_blank';
						a.className = 'external'
					}
				}
			}
		})()
	} catch (e) {};
	</script>
</body>
</html>
