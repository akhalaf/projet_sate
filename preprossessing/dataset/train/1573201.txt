<!DOCTYPE html>
<html><head>
<title>Создание сайтов визиток, корпоративных сайтов, каталогов, лэндингов</title>
<meta content="создание изготовление сайт визитка эффективный корпоративный лендинг лэндинг landing page интернет-магазин" name="keywords"/>
<meta content="Изготовление сайтов-визиток, корпоративных, каталогов, посадочных страниц (лендингов) и интернет-магазинов" name="description"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="/favicon.png" rel="icon" type="image/png"/>
<link href="css/app.css" rel="stylesheet"/>
</head>
<body id="page1">
<div class="main-bg">
<header>
<div class="row-top">
<div class="main-1">
<div class="box-adress overflow"><em>Телефон: <span>+7 (499) 990-03-90</span>   E-mail: <a class="mail-1" href="mailto:info@website-media.ru">info@website-media.ru</a></em></div>
</div>
</div>
<div class="row-nav">
<div class="main-1"> <a class="logo" href="/"></a>
<nav>
<ul class="sf-menu">
<li class="topHere">Главная</li>
<li><a href="https://www.website-media.ru/o-nas">О нас</a></li>
<li><a href="https://www.website-media.ru/uslugi-i-ceny">Услуги и цены</a></li>
<li><a href="https://www.website-media.ru/portfolio">Портфолио</a></li>
<li class="last"><a href="https://www.website-media.ru/contact">Контакты</a></li>
</ul>
</nav>
<div class="clear"></div>
<form id="main-search" method="get">
<fieldset>
<div class="rowElem">
<input name="search" type="text" value=""/>
<a class="search-submit"></a> </div>
</fieldset>
</form>
</div>
</div>
</header>
<div class="slider-holder">
<div id="slider">
<h2>Эффективные сайты</h2>
<h3>для вашего бизнеса</h3>
<p class="grayTxt">Качество - наш <span class="red">приоритет</span></p>
<p class="motto">Наши сайты продвигают ваш бизнес</p>
<a class="button redBtn" href="zakaz-saita">Заказать сайт</a>
</div>
<div class="homeBn">
<img alt="Наши сайты продвигают ваш бизнес" src="img/sitesPromote.png" width="460"/>
</div>
</div>
<section id="content">
<div class="container_12">
<div class="wrapper drops">
<article class="grid_4">
<div class="wrapper"> <a class="link" href="tehzadanie-na-sait">
<div class="dropcap_1">01</div>
<div class="overflow"> <span>Техзадание<em>на сайт</em></span></div></a> </div>
</article>
<article class="grid_4">
<div class="wrapper"> <a class="link" href="sozdanie-saitov">
<div class="dropcap_1">02</div>
<div class="overflow"> <span>Создание <em>сайта</em></span></div></a> </div>
</article>
<article class="grid_4">
<div class="wrapper"> <a class="link" href="podderjka-i-prodvojenie-saitov">
<div class="dropcap_1">03</div>
<div class="overflow"><span>Поддержка <em>и продвижение</em></span></div></a> </div>
</article>
</div>
</div>
<div class="block-main">
<div class="container_12">
<div class="wrapper">
<div class="main">
<div class="row-1">
<h1 class="border-bot">Что мы предлагаем</h1>
<p class="color-1">Мы представим Вам на выбор различные варианты дизайна сайта, согласуем требуемый функционал, определим необходимые страницы и разделы сайта и установим сроки для выполнения каждого этапа работ.</p>
<div class="wrapper">
<article class="grid_4 alpha">
<ul class="list-2">
<li><a href="predvaritelnaya-ocenka-rezultata">Вы можете оценить результат заранее</a></li>
<li><a href="tehnologii">Применение передовых технологий веб-разработки</a></li>
</ul>
</article>
<article class="grid_4">
<ul class="list-2">
<li><a href="design-saita">Привлекательный дизайн сайта</a></li>
<li><a href="sroki">Быстрые сроки выполнения работ</a></li>
</ul>
</article>
<article class="grid_4 omega">
<ul class="list-2">
<li><a href="hosting">Бесплатный домен и хостинг на 1 год</a></li>
<li><a href="cms-sistema-upravleniya-saitom">Использование современной системы управления (CMS)<!--Изготовление сайта под ключ--></a></li>
</ul>
</article>
</div>
</div>
</div>
</div>
<div class="row-2">
<div class="container_12">
<div class="wrapper">
<article class="grid_4">
<h1 class="border-bot">Добро пожаловать</h1>
<p class="color-1">Студия Website Media рада приветствовать Вас!<br/>Мы живем в удивительное время быстрых изменений, безграничных возможностей и удивительных открытий. Время, когда человек может сам творить свою судьбу и реализовывать свои мечты и замыслы.</p>
<p>Как сказал Лао-Цзы «Путь в 1000 миль начинается с одного шага». Возможно, одним из шагов на этом пути может оказаться создание вашего сайта.</p>
<div class="p5"><a class="button" href="dobro-pojalovat">Подробнее</a></div>
</article>
<article class="grid_4">
<h1 class="border-bot">О нас</h1>
<p class="color-2">Молодая, активно развивающаяся веб-студия. Мы находимся в Москве, но наших заказчиков можно найти по всей России и за ее пределами.</p>
<p>Наши клиенты - это, в основном, малый бизнес и индивидуальные предприниматели.</p>
<p>Мы любим свою работу.</p>
<div class="p5"><a class="button" href="o-nas">Подробнее</a></div>
</article>
<article class="grid_4">
<h1 class="border-bot">Виды сайтов</h1>
<ul class="list-1">
<li><a href="sait-vizitka">Сайт Визитка</a></li>
<li><a href="korporativniy-sait">Сайт Корпоративный</a></li>
<li><a href="sait-katalog">Сайт Каталог</a></li>
<li><a href="sait-psihologa">Сайт Психолога</a></li>
</ul>
<!--<div class="p4"><a href="uslugi-i-ceny" class="button">Сравнить</a></div>-->
</article>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
<footer>
<div class="main-1">
<ul class="menu-footer">
<li class="btmHere">Главная</li>
<li><a href="https://www.website-media.ru/o-nas">О нас</a></li>
<li><a href="https://www.website-media.ru/uslugi-i-ceny">Услуги и цены</a></li>
<li><a href="https://www.website-media.ru/portfolio">Портфолио</a></li>
<li><a href="https://www.website-media.ru/contact">Контакты</a></li>
</ul>
<div class="clear"></div>
<div class="privacy p13">Website-Media.ru  © 2014</div>
<div class="clear"></div>
</div>
</footer>
<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script src="js/script.js"></script>
<!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter34140385 = new Ya.Metrika({ id:34140385, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img alt="" src="https://mc.yandex.ru/watch/34140385" style="position:absolute; left:-9999px;"/></div></noscript><!-- /Yandex.Metrika counter -->
</body></html>