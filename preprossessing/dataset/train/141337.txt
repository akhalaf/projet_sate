<!DOCTYPE html>
<html lang="en">
<head>
<title>Test Prep for the SAT, ACT, GRE, SSAT and More | Applerouth</title><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><meta content="With Applerouth, you get the smarter approach to test prep. Our methods, materials, and tutor training are informed by evidence on what helps students succeed." name="description"/><meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="/apple-touch-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<meta content="Applerouth" name="apple-mobile-web-app-title"/>
<link href="/favicon-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="/favicon-160x160.png" rel="icon" sizes="160x160" type="image/png"/>
<link href="/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<meta content="#b91d47" name="msapplication-TileColor"/>
<meta content="/mstile-144x144.png" name="msapplication-TileImage"/>
<meta content="Applerouth" name="application-name"/>
<link href="/styles/applerouth.css?v=1608310327" rel="stylesheet" type="text/css"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<script src="/preminify/public_js.js?v=1608310327" type="text/javascript"></script>
<link href="https://plus.google.com/111371969964087261689/" rel="publisher"/>
<script type="text/javascript">
			
				(function($) {
					
					$(function () {
						
						$('[data-toggle="tooltip"]').tooltip();
						
					});
					
					if (/studentid=(\d+)/.test(window.location.href)) {
						
						$(document).on('click', 'a', function() {
							
							var href = $(this).attr('href');
							
							if (/studentid=(\d+)/.test(href) == false &&
								href.charAt(0) != '#') {
							
								var matches = window.location.href.match(/studentid=(\d+)/);
							
								href += (/\?/.test(href) ? '&' : '?') + matches[0];
							
								$(this).attr('href', href);
								
							}
							
						});
						
					}
				
					setTimeout(
						function() { $('#session-timeout').modal('show'); },
						2700000
					);
					
					$(document).on('click', '#continue-session', function() {
						
						$.get('/contact/', function(e) { $('#session-timeout').modal('hide'); });
						
						setTimeout(
							function() { $('#session-timeout').modal('show'); },
							2700000
						);
						
						return false;
						
					});
					
					$(document).on('click', '#show-search', function() {
						
						$('.navbar-form').toggleClass('show-navbar-form').find('input').focus();
						
						return false;
						
					});
					
				})(jQuery);
				
			</script><script type="application/ld+json">
				{
					"@context": "https://schema.org",
					"@type": "Organization",
					"name": "Applerouth",
					"legalName" : "Applerouth Tutoring Services Llc",
					"url": "https://www.applerouth.com/",
					"logo": "https://www.applerouth.com/media/images/logo.png",
					"founders": [
						{
						"@type": "Person",
						"name": "Jed Applerouth"
						}
					],
					"address": {
						"@type": "PostalAddress",
						"streetAddress": "PO Box 14161",
						"addressLocality": "Atlanta ",
						"addressRegion": "GA",
						"postalCode": "30324",
						"addressCountry": "USA"
					},
					"contactPoint": {
						"@type": "ContactPoint",
						"telephone": "866-789-7737",
						"email": "info@applerouth.com"
					},
					"sameAs": [ 
						"https://www.facebook.com/applerouth/",
						"https://twitter.com/applerouth",
						"https://www.instagram.com/applerouthtutoring/",
						"https://www.youtube.com/channel/UCFv4vYSyw-0Vt2l_mwPMghQ",
						"https://www.linkedin.com/company/appelrouth-tutoring-services"
					]
				}
		   	</script>
<script type="text/javascript">

				$(document).ready(function() {

					$('#featured').carousel({
						interval: 10000,
					});

				});

			</script><link as="image" href="/media/images/banner/home_academic.jpg" rel="preload"/><link as="image" href="/media/images/logo.svg" rel="preload"/>
<link as="font" crossorigin="" href="/fonts/raleway-v18-latin-500.woff2" rel="preload" type="font/woff2"/>
<link as="font" crossorigin="" href="/fonts/montserrat-v15-latin-regular.woff2" rel="preload" type="font/woff2"/>
<link as="font" crossorigin="" href="/fonts/montserrat-v15-latin-300.woff2" rel="preload" type="font/woff2"/>
<link as="font" crossorigin="" href="/fonts/raleway-v18-latin-600.woff2" rel="preload" type="font/woff2"/>
<link as="font" crossorigin="" href="/fonts/raleway-v18-latin-300.woff2" rel="preload" type="font/woff2"/>
<link as="font" crossorigin="" href="/fonts/montserrat-v15-latin-italic.woff2" rel="preload" type="font/woff2"/>
<link as="font" crossorigin="" href="/fonts/montserrat-v15-latin-700.woff2" rel="preload" type="font/woff2"/>
<link as="font" crossorigin="" href="/fonts/montserrat-v15-latin-300italic.woff2" rel="preload" type="font/woff2"/>
<link as="font" crossorigin="" href="/fonts/icons.woff2" rel="preload" type="font/woff2"/><script type="text/javascript">
			
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

				ga('create', 'UA-70195-1', 'auto');
				
								
				ga('send', 'pageview');
				
				// Process ecommerce events after page view. 
				var _egaq = _egaq || [];
				
				if (_egaq.length) {
					
					ga('require', 'ecommerce');
				
					$.each(_egaq, function(i, value) {
						
						ga(value[0], value[1]);
						
					});
				
					ga('ecommerce:send');
					
				}

			</script><!-- Facebook Pixel Code -->
<script>
			!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '593817520765840'); // Insert your pixel ID here.
			fbq('track', 'PageView');
			
			// Process ecommerce events after page view. 
			var _fbevents = _fbevents || [];
			
			if (_fbevents.length) {
							
				$.each(_fbevents, function(i, value) {
					
					fbq('track', value);
					
				});
				
			}
			
			</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=593817520765840&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code --></head>
<body class="home"><nav class="navbar navbar-default navbar-static-top noprint">
<div class="container">
<div class="navbar-header">
<button aria-expanded="false" class="navbar-toggle collapsed" data-target="#ats-nav" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="/">
<img alt="Applerouth" height="50" src="/media/images/logo.svg" title="Applerouth"/>
</a>
</div>
<div class="collapse navbar-collapse" id="ats-nav">
<ul class="nav navbar-nav">
<li class="dropdown">
<a href="/services/test-prep/">Test Prep</a>
<ul class="dropdown-menu spectrum-orange-background"><li class="category">College Admissions</li><li><a href="/services/test-prep/act/">ACT Prep</a></li><li><a href="/services/test-prep/sat/">SAT Prep</a></li><li><a href="/services/test-prep/ap-exams/">AP Exams Prep</a></li><li><a href="/services/test-prep/psat/">PSAT Prep</a></li><li><a href="/services/test-prep/sat-subjects/">SAT Subjects Prep</a></li><li class="category">Grad Admissions</li><li><a href="/services/test-prep/gre/">GRE Prep</a></li><li class="category">HS Admissions</li><li><a href="/services/test-prep/shsat/">SHSAT Prep</a></li><li><a href="/services/test-prep/isee-ssat/">SSAT/ISEE Prep</a></li></ul>
</li>
<li class="dropdown">
<a href="/services/academic/">Academic<span class="hidden-sm hidden-md"> Tutoring</span></a>
<ul class="dropdown-menu spectrum-green-background">
<li><a href="/services/academic/elementary-school/">Elementary School Tutoring</a></li>
<li><a href="/services/academic/middle-school/">Middle School Tutoring</a></li>
<li><a href="/services/academic/high-school/">AP, IB &amp; High School Tutoring</a></li>
<li><a href="/services/academic/college/">College Tutoring</a></li>
</ul>
</li>
<li>
<a href="/materials/">Books</a>
</li>
<li class="dropdown">
<a name="free-resources"><span class="hidden-sm">Free </span>Resources</a>
<ul class="dropdown-menu expand-mobile spectrum-mint-background"><li class="category">Free Practice + Events</li><li><a href="/calendar/?event-types[mock-test]=1">Free Practice Tests</a></li><li><a href="/calendar/?event-types[free-seminar]=1">Free Events</a></li><li class="category">Experts’ Corner</li><li><a href="/experts/">Applerouth’s Blog</a></li><li><a href="/backtoschool/">New for 2020-2021: Virtual Bookshelf</a></li><li><a href="/edconsultant/">Resources for Educational Consultants</a></li><li class="category">SAT &amp; ACT Free Resources</li><li><a href="/actchanges/?utm_source=website&amp;utm_medium=freeresources&amp;utm_campaign=act_changes_hub_2020">2020 ACT Changes</a></li><li><a href="https://www.applerouth.com/blog/about-the-psat/">About the PSAT</a></li><li><a href="https://www.applerouth.com/blog/about-the-sat-subject-tests/">About the SAT Subject Tests</a></li><li><a href="https://www.applerouth.com/blog/admissions-testing-policy-updates-in-response-to-covid-19/">Admissions Testing Policy Updates in response to COVID-19</a></li><li><a href="https://www.applerouth.com/blog/about-the-sat/">About the SAT</a></li><li><a href="https://www.applerouth.com/blog/about-the-act/">About the ACT</a></li><li><a href="https://www.applerouth.com/blog/what-is-a-good-sat-score/">What is a good SAT score?</a></li><li><a href="https://www.applerouth.com/blog/what-is-a-good-act-score/">What is a good ACT score?</a></li><li><a href="https://www.applerouth.com/blog/sat-test-dates-and-registration/">SAT Test Dates and Registration</a></li><li><a href="https://www.applerouth.com/blog/act-test-dates-and-registration/">ACT Test Dates and Registration</a></li><li><a href="https://www.applerouth.com/blog/sat-and-act-test-day-checklist/">SAT and ACT Test Day Checklist</a></li><li><a href="/testcomparison/">SAT vs ACT</a></li></ul>
</li>
<li class="dropdown">
<a href="/different/">About Us</a>
<ul class="dropdown-menu spectrum-indigo-background">
<li class="category">Our Difference</li>
<li><a href="/methodology/">Methodology</a></li>
<li><a href="/process/">Process</a></li>
<li><a href="/results/">Results</a></li>
<li><a href="/aboutus/">Our Team</a></li>
<li class="category">FAQ</li>
<li><a href="/faqs/#General">General</a></li>
<li><a href="/faqs/#PrivateTutoring">Private Tutoring</a></li>
<li><a href="/faqs/#Groups">Group Classes</a></li>
<li><a href="/faqs/#MockTests">Mock Tests</a></li>
<li><a href="/faqs/#Testing">Testing</a></li>
<li><a href="/faqs/#OnlineTutoring">Online Tutoring</a></li>
</ul>
</li>
<li>
<a href="/contact/">Contact</a>
</li>
</ul>
<ul class="nav navbar-nav navbar-right">
<li><a href="/login/">Login</a></li><li class="hidden-xs"><a href="#" id="show-search"><i class="ats ats-fw ats-search"></i></a></li>
</ul>
<form action="/search/" class="navbar-form">
<div class="input-group input-group-lg">
<input name="studentid" type="hidden" value=""/>
<input class="nav-search form-control" name="q" placeholder="search or event code" type="text"/>
<span class="input-group-btn">
<button class="btn" type="submit"><i class="ats ats-search"></i></button>
</span>
</div>
</form>
</div>
</div>
</nav><div class="carousel" id="featured">
<div class="carousel-inner"><div class="item active"><div class="item-background" style="background: url('/media/images/banner/home_academic.jpg') no-repeat scroll 0px 0px;
					background-size: cover;">
<div class="item-feature">
<h1>Conquer your semester<br/>with an Applerouth tutor</h1>
<h2 class="spacing-top">Get organized and start<br/>learning from home.</h2>
<div class="spacing-top">
<a class="btn btn-lg btn-primary" href="/services/academic">Learn More</a>
</div>
</div></div></div><div class="item "><div class="item-background" style="background: url('/media/images/banner/home_04.jpg') no-repeat scroll 0px 0px;
					background-size: cover;">
<div class="item-feature">
<h1>How would you score<br/>on the SAT or ACT?</h1>
<h2 class="spacing-top">Take a practice test without<br/>test day pressure</h2>
<div class="spacing-top">
<a class="btn btn-lg btn-primary" href="/calendar?event-types[mock-test]=on">Find a Test</a>
</div>
</div></div></div><div class="item "><div class="item-background" style="background: url('/media/images/banner/home_elementary.jpg') no-repeat scroll 0px 0px;
					background-size: cover;">
<div class="item-feature">
<h1>Structured Learning for<br/>Elementary and Middle School</h1>
<h2 class="spacing-top">Call today to be matched<br/>with an online tutor.</h2>
<div class="spacing-top">
<a class="btn btn-lg btn-primary" href="/services/academic/">Learn More</a>
</div>
</div></div></div><div class="item "><div class="item-background heavy" style="background: url('/media/images/banner/home_03.jpg') no-repeat scroll 0px 0px;
					background-size: cover; background-color:#000000;">
<div class="item-feature">
<h1 style="color:#FFFFFF">Work with the most talented<br/>and motivated tutors</h1>
<h2 class="spacing-top" style="color:#FFFFFF">Our tutors go beyond the content.</h2>
<div class="spacing-top">
<a class="btn btn-lg btn-alternate" href="/aboutus#the-tutors">Meet our Tutors</a>
</div>
</div></div></div><div class="item "><div class="item-background" style="background: url('/media/images/banner/home_05.jpg') no-repeat scroll 0px 0px;
					background-size: cover;">
<div class="item-feature">
<h1>A smarter approach to test prep</h1>
<h2 class="spacing-top">Using the latest research in cognition,<br/>memory and motivation to help students<br/>achieve higher scores</h2>
<div class="spacing-top">
<a class="btn btn-lg btn-primary" href="/methodology">The Applerouth Difference</a>
</div>
</div></div></div><div class="item "><div class="item-background heavy" style="background: url('/media/images/banner/home_02.jpg') no-repeat scroll 0px 0px;
					background-size: cover; background-color:#000000;">
<div class="item-feature">
<h1 style="color:#FFFFFF">Behind each student is a<br/>proud team of experts.</h1>
<h2 class="spacing-top" style="color:#FFFFFF">We hire the best and empower them<br/>to do the most.</h2>
<div class="spacing-top">
<a class="btn btn-lg btn-alternate" href="/aboutus">Meet our Team</a>
</div>
</div></div></div></div><a aria-label="Previous" class="left carousel-control" data-slide="prev" href="#featured">
<i class="ats ats-arrow-left"></i>
</a>
<a aria-label="Next" class="right carousel-control" data-slide="next" href="#featured">
<i class="ats ats-arrow-right"></i>
</a><div class="spectrum hidden-xs"><a class="spectrum-background spectrum-red-background-tint" href="/services/test-prep/act/">
<h2 class="spectrum-segment">ACT<i aria-hidden="true" class="ats ats-angle-right"></i>
</h2>
</a><a class="spectrum-background spectrum-orange-background-tint" href="/services/test-prep/sat/">
<h2 class="spectrum-segment">SAT<i aria-hidden="true" class="ats ats-angle-right"></i>
</h2>
</a><a class="spectrum-background spectrum-yellow-background-tint" href="/services/test-prep/gre/">
<h2 class="spectrum-segment">GRE<i aria-hidden="true" class="ats ats-angle-right"></i>
</h2>
</a><a class="spectrum-background spectrum-green-background-tint" href="/services/test-prep/ap-exams/">
<h2 class="spectrum-segment">AP Exams<i aria-hidden="true" class="ats ats-angle-right"></i>
</h2>
</a><a class="spectrum-background spectrum-mint-background-tint" href="/services/test-prep/psat/">
<h2 class="spectrum-segment">PSAT<i aria-hidden="true" class="ats ats-angle-right"></i>
</h2>
</a><a class="spectrum-background spectrum-teal-background-tint" href="/services/test-prep/shsat/">
<h2 class="spectrum-segment">SHSAT<i aria-hidden="true" class="ats ats-angle-right"></i>
</h2>
</a><a class="spectrum-background spectrum-blue-background-tint" href="/services/test-prep/sat-subjects/">
<h2 class="spectrum-segment">SAT Subjects<i aria-hidden="true" class="ats ats-angle-right"></i>
</h2>
</a><a class="spectrum-background spectrum-indigo-background-tint" href="/services/test-prep/isee-ssat/">
<h2 class="spectrum-segment">SSAT/ISEE<i aria-hidden="true" class="ats ats-angle-right"></i>
</h2>
</a><a class="spectrum-background spectrum-purple-background-tint" href="/services/test-prep//">
<h2 class="spectrum-segment">More Tests<i aria-hidden="true" class="ats ats-angle-right"></i>
</h2>
</a></div>
</div>
<div class="panel ui-helper-clearfix " data-url="/controller/HomePanel/index/"><div class="container container-band">
<div class="hidden-lg hidden-md hidden-sm spacing-bottom-2 text-center">
<form action="/services/test-prep/">
<h2 class="h6">Ready to Prep?</h2>
<div class="row">
<div class="col-xs-9 center-block">
<p>
<select class="form-control" name="test" title="Choose a test">
<option value="">Choose a test...</option><option value="act">ACT</option><option value="sat">SAT</option><option value="gre">GRE</option><option value="ap-exams">AP Exams</option><option value="psat">PSAT</option><option value="shsat">SHSAT</option><option value="sat-subjects">SAT Subjects</option><option value="isee-ssat">SSAT/ISEE</option><option value="">More Tests</option></select>
</p>
</div>
</div>
<button class="btn-link" type="submit">Learn More</button>
</form>
</div>
<div class="row">
<div class="col-md-3 col-sm-6 icon-description-block"><i aria-hidden="true" class="ats ats-refresh"></i>
<h2 class="h6">Our Process</h2>
<p>
				The tests are standardized; our approach is anything but. 
				Our tutoring process caters to each student’s specific 
				strengths, needs, and schedule.
			</p>
<a class="btn-link" href="/process/">Learn More</a></div>
<div class="col-md-3 col-sm-6 icon-description-block spacing-grid-top-2"><i aria-hidden="true" class="ats ats-maps-pin"></i>
<h2 class="h6">Tutoring Anywhere</h2>
<p>
				Wherever you are, our tutors are ready to help. 
				We've helped students just like you across the globe 
				from Atlanta to Singapore.
			</p>
<a class="btn-link" href="/locations/online/">Learn More</a></div>
<div class="col-md-3 col-sm-6 icon-description-block spacing-grid-top-2"><i aria-hidden="true" class="ats ats-user-circle"></i>
<h2 class="h6">Our Students</h2>
<p>
				Applerouth students earn higher tests scores. It's that 
				simple. See how students just like you improve 
				their grades and boost their confidence.
			</p>
<a class="btn-link" href="/ourstudents/">Learn More</a></div>
<div class="col-md-3 col-sm-6 icon-description-block spacing-grid-top-2">
<i aria-hidden="true" class="ats ats-edit"></i>
<h2 class="h6">Mock Tests</h2>
<p>
							Gain confidence through experience. With every package, 
							students take timed mock tests and receive comprehensive 
							performance reports.
						</p>
<a class="btn-link" href="/calendar?event-types[mock-test]=1">Learn More</a>
</div>
</div>
</div><div class="panel ui-helper-clearfix " data-url="/controller/CalendarWidgetPanel/index/"><div class="calendar widget">
<div class="container container-band">
<div class="row">
<div class="col-md-8 col-sm-6">
<h2 class="no-margin-top">Applerouth Events <small class="hidden-xs hidden-sm"> near San Francisco, CA</small></h2>
</div>
<div class="col-md-4 col-sm-6">
<div class="row"><div class="col-sm-7 pull-right" id="events-listing">
<form action="/calendar/" method="get">
<div class="input-group input-group-sm">
<span class="input-group-addon">
<i class="ats ats-maps-pin"></i>
</span>
<input class="form-control" id="postal" name="postal-code" placeholder="Postal Code" title="Postal Code" type="text" value="94107"/>
<span class="input-group-btn">
<button aria-label="Submit Postal Code" class="btn btn-primary" type="submit">
<i aria-hidden="true" class="ats ats-fw ats-angle-right"></i>
</button>
</span>
</div>
</form>
</div></div>
</div>
</div>
<div class="row">
<div class="col-lg-12 col-md-12">
<div class="row"><script type="application/ld+json">[{"@context":"http:\/\/schema.org","@type":"EducationEvent","name":"What Parents of Juniors Need to Know Now about the SAT and ACT","url":"https:\/\/www.applerouth.com\/signup\/?scheduleid=425382","startDate":"2021-01-14T20:00:00-05:00","description":"A lot has changed in the world of college admissions testing. Our panel of experts will provide the latest updates and answer your most pressing questions, including:\n\n- What are the differences between the ACT and SAT? \n- How do I decide which test is best for my student?\n- What does \"test-optional\" mean for my student?\n- When should my student take the test? And what's the prep timeline?","location":{"@type":"Place","name":"Online","address":"Online"},"endDate":"2021-01-14T21:00:00-05:00"}]</script><div class="col-lg-4 col-md-4 spacing-grid-top-2">
<div class="calendar-item">
<div class="calendar-date">
<div class="date">
<div class="day">14</div>
<div class="month">JAN</div>
</div>
</div>
<div class="calendar-info">
<div class="h3 spacing-top">Free Webinar</div>
<p class="small">Attend <em>What Parents of Juniors Need to Know Now about the SAT and ACT</em> <strong>online</strong>.</p>
<a class="btn-link" href="https://www.applerouth.com/signup/?scheduleid=425382">Sign Up</a>
</div>
</div>
</div><script type="application/ld+json">[{"@context":"http:\/\/schema.org","@type":"EducationEvent","name":"SAT vs ACT: The differences, the timelines, and the expectations of colleges","url":"https:\/\/www.applerouth.com\/signup\/?scheduleid=425450","startDate":"2021-01-27T20:00:00-05:00","description":"Join Applerouth Tutoring to learn about the SAT and ACT, as well as up-to-the-minute updates on the test administrations. Come learn about the main differences between the two tests, how to decide which one is better for your child, when to take them, how much to prepare for them, and more. Hear about how your child's strengths can favor one test or the other. Learn about what college admissions officers expect from students during the pandemic and what you need to plan for.","location":{"@type":"Place","name":"Online","address":"Online"},"endDate":"2021-01-27T21:00:00-05:00"}]</script><div class="col-lg-4 col-md-4 spacing-grid-top-2">
<div class="calendar-item">
<div class="calendar-date">
<div class="date">
<div class="day">27</div>
<div class="month">JAN</div>
</div>
</div>
<div class="calendar-info">
<div class="h3 spacing-top">Free Webinar</div>
<p class="small">Attend <em>SAT vs ACT: The differences, the timelines, and the expectations of colleges</em> <strong>online</strong>.</p>
<a class="btn-link" href="https://www.applerouth.com/signup/?scheduleid=425450">Sign Up</a>
</div>
</div>
</div><div class="col-lg-4 col-md-4 spacing-grid-top-2">
<div class="calendar-item">
<div class="calendar-info">
<div class="h4 no-margin-top">Have an Event Code?</div>
<form action="/signup/" method="get"><div class="input-group input-group-sm">
<input class="form-control" name="code" placeholder="Event Code (ACT1234)" title="Event Code" type="text"/>
<span class="input-group-btn">
<button aria-label="Submit Event Code" class="btn btn-primary" type="submit">
<i aria-hidden="true" class="ats ats-fw ats-angle-right"></i>
</button>
</span>
</div>
</form>
<hr class="dark spacing-top spacing-bottom"/>
<p class="small">
												For a complete listing of group classes, mock tests and 
												free seminars in your area click view all.
											</p>
<a class="btn-link" href="/calendar/">View All Upcoming Events</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div></div><div class="container container-band">
<div class="row">
<div class="col-lg-3 col-md-3 hidden-xs hidden-sm">
<ul class="sidebar">
<li>
<a class="btn-link" href="/services/academic/elementary-school/private-tutoring/">
									Elementary School Tutoring
								</a>
</li>
<li>
<a class="btn-link" href="/services/academic/middle-school/private-tutoring/">
									Middle School Tutoring
								</a>
</li>
<li>
<a class="btn-link" href="/services/academic/high-school/private-tutoring/">
									High School Tutoring
								</a>
</li>
<li>
<a class="btn-link" href="/services/academic/college/private-tutoring/">
									College Tutoring
								</a>
</li>
</ul>
</div>
<div class="col-lg-9 col-md-9 ">
<h2 class="h4 no-margin-top">Academic Tutoring</h2>
<p class="large">
							For many students, "back to school" means "back to the computer" this fall. 
							If you are worried that your student will get lost in the distance 
							between students and teachers this year, Applerouth can help 
							bridge the gap. From elementary school to college courses.
							Applerouth has you covered.
						</p>
<p class="hidden-md hidden-lg">
<a class="btn-link" href="/services/academic/">Learn More</a>
</p>
</div>
</div>
<div class="carousel slide news" data-interval="10000" data-ride="carousel" id="news-carousel">
<div aria-label="In The News" class="carousel-inner" role="listbox"><div class="item active">
<div class="row align-center spacing-bottom"><div aria-label="The New York Times" class="col-sm-3 col-sm-offset-0" role="option">
<div class="frame">
<a href="http://www.nytimes.com/2016/02/09/us/sat-test-changes.html" rel="noopener" target="_blank">
<img alt="The New York Times" class="media-logo img-responsive" loading="lazy" src="/media/images/press/the_new_york_times.png"/>
</a>
</div>
</div><div aria-label="U.S. News &amp; World Report" class="col-sm-3" role="option">
<div class="frame">
<a href="https://www.usnews.com/education/best-colleges/articles/how-the-coronavirus-is-pushing-colleges-to-go-test-optional" rel="noopener" target="_blank">
<img alt="U.S. News &amp; World Report" class="media-logo img-responsive" loading="lazy" src="/media/images/press/usn-logo-large.png"/>
</a>
</div>
</div><div aria-label="CBS" class="col-sm-3" role="option">
<div class="frame">
<a href="http://www.cbsnews.com/news/why-the-new-sat-is-spooking-high-school-students/" rel="noopener" target="_blank">
<img alt="CBS" class="media-logo img-responsive" loading="lazy" src="/media/images/press/cbs.png"/>
</a>
</div>
</div><div aria-label="Time Magazine" class="col-sm-3" role="option">
<div class="frame">
<a href="http://time.com/4057309/standardized-testing/" rel="noopener" target="_blank">
<img alt="Time Magazine" class="media-logo img-responsive" loading="lazy" src="/media/images/press/time_magazine.png"/>
</a>
</div>
</div></div>
</div><div class="item">
<div class="row align-center spacing-bottom"><div aria-label="Chicago Tribune" class="col-sm-3 col-sm-offset-0" role="option">
<div class="frame">
<a href="http://www.chicagotribune.com/news/ct-met-testing-accommodations-20120429-58-story.html" rel="noopener" target="_blank">
<img alt="Chicago Tribune" class="media-logo img-responsive" loading="lazy" src="/media/images/press/chicago_tribune.png"/>
</a>
</div>
</div><div aria-label="The Atlanta Journal Constitution" class="col-sm-3" role="option">
<div class="frame">
<a href="http://www.myajc.com/news/news/local-education/students-gear-up-for-revised-sat-test/nqPMf/" rel="noopener" target="_blank">
<img alt="The Atlanta Journal Constitution" class="media-logo img-responsive" loading="lazy" src="/media/images/press/the_atlanta_journal-constitution.png"/>
</a>
</div>
</div><div aria-label="The Huffington Post" class="col-sm-3" role="option">
<div class="frame">
<a href="http://www.huffingtonpost.com/jed-applerouth/deconstructing-the-new-sa_b_5206926.html" rel="noopener" target="_blank">
<img alt="The Huffington Post" class="media-logo img-responsive" loading="lazy" src="/media/images/press/the_huffington_post.png"/>
</a>
</div>
</div><div aria-label="Inside Higher Ed" class="col-sm-3" role="option">
<div class="frame">
<a href="https://www.insidehighered.com/news/2013/12/03/college-board-pushes-back-revised-sat-one-year" rel="noopener" target="_blank">
<img alt="Inside Higher Ed" class="media-logo img-responsive" loading="lazy" src="/media/images/press/inside_higher_ed.png"/>
</a>
</div>
</div></div>
</div><div class="item">
<div class="row align-center spacing-bottom"><div aria-label="Bloomberg" class="col-sm-3 col-sm-offset-3" role="option">
<div class="frame">
<a href="http://www.bloomberg.com/news/articles/2015-08-24/it-s-yale-vs-brown-in-ivy-league-split-over-optional-sat-essay" rel="noopener" target="_blank">
<img alt="Bloomberg" class="media-logo img-responsive" loading="lazy" src="/media/images/press/bloomberg.png"/>
</a>
</div>
</div><div aria-label="Seventeen" class="col-sm-3" role="option">
<div class="frame">
<a href="http://www.seventeen.com/life/school/news/a34850/heres-why-some-experts-recommend-you-take-the-act-instead-of-the-sat-but-only-if-youre-a-junior/" rel="noopener" target="_blank">
<img alt="Seventeen" class="media-logo img-responsive" loading="lazy" src="/media/images/press/seventeen.png"/>
</a>
</div>
</div><div aria-label="11 Alive" class="col-sm-3" role="option">
<div class="frame">
<a href="http://www.11alive.com/news/education/sat-prep-courses-during-the-school-day/129190885" rel="noopener" target="_blank">
<img alt="11 Alive" class="media-logo img-responsive" loading="lazy" src="/media/images/press/11_alive.png"/>
</a>
</div>
</div></div>
</div></div>
<a class="left carousel-control" data-slide="prev" href="#news-carousel" role="button">
<i class="ats ats-arrow-left"></i>
<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" data-slide="next" href="#news-carousel" role="button">
<i class="ats ats-arrow-right"></i>
<span class="sr-only">Next</span>
</a>
</div>
</div></div><div class="container" id="content"></div><div class="footer">
<div class="spectrum">
<div class="spectrum-segment spectrum-red-background"></div>
<div class="spectrum-segment spectrum-orange-background"></div>
<div class="spectrum-segment spectrum-yellow-background"></div>
<div class="spectrum-segment spectrum-green-background"></div>
<div class="spectrum-segment spectrum-mint-background"></div>
<div class="spectrum-segment spectrum-teal-background"></div>
<div class="spectrum-segment spectrum-blue-background"></div>
<div class="spectrum-segment spectrum-indigo-background"></div>
<div class="spectrum-segment spectrum-purple-background"></div>
</div>
<div class="container">
<div class="row footer-icons">
<div class="col-sm-9">
<form action="//applerouth.us10.list-manage.com/subscribe/post?u=4748abfb7bcea49090558ca68&amp;id=41a7cbffe6" method="post">
<input id="mce-MMERGE5" name="MMERGE5" type="hidden" value="Online Tutoring"/>
<a aria-label="Facebook" class="gray-link pull-left" href="http://www.facebook.com/applerouth" rel="noopener" target="_blank">
<i aria-hidden="true" class="ats ats-facebook-square ats-2x"></i>
</a>
<a aria-label="Twitter" class="gray-link pull-left" href="http://twitter.com/applerouth" rel="noopener" target="_blank">
<i aria-hidden="true" class="ats ats-twitter-square ats-2x"></i>
</a>
<a aria-label="LinkedIn" class="gray-link pull-left" href="https://www.linkedin.com/company/appelrouth-tutoring-services" rel="noopener" target="_blank">
<i aria-hidden="true" class="ats ats-linkedin-square ats-2x"></i>
</a>
<div class="input-group input-group-sm col-sm-3 pull-left">
<input class="form-control" id="mce-EMAIL" name="EMAIL" placeholder="newsletter signup" title="Newsletter Signup" type="email"/>
<span class="input-group-btn">
<button aria-label="Signup for Newsletter" class="btn btn-primary" type="submit"><i aria-hidden="true" class="ats ats-fw ats-angle-right"></i></button>
</span>
</div>
</form>
</div>
<div class="col-sm-3 hidden-xs">
<img alt="Applerouth" class="footer-logo pull-right" src="/media/images/logo.svg" title="Applerouth"/>
</div>
</div>
<div class="row">
<div class="col-md-8">
<p class="footer-links">
<a class="gray-link" href="/jobs/">jobs</a>
<a class="gray-link" href="/educators/">educators</a>
<a class="gray-link" href="/testimonials/">testimonials</a>
<a class="gray-link" href="/materials/">test materials</a>
<a class="gray-link" href="/calendar/">calendar</a>
<a class="gray-link" href="/locations/">locations</a>
<a class="gray-link" href="/blog/">blog</a>
<a class="gray-link" href="/policies/">policies</a>
<a class="gray-link" href="/contact/">contact us</a>
</p>
</div>
<div class="col-lg-4 col-md-4 hidden-xs">
<small class="muted pull-right copyright-line">© 2021 Applerouth. All rights reserved.</small>
</div>
</div></div>
</div>
<div class="mobile-footer hidden-lg hidden-md hidden-sm">
<div class="container">
<div class="row">
<div class="col-sm-12"><a class="btn btn-block btn-primary" href="tel:8667897737">
<i class="ats ats-phone"></i> Give Us a Call For More Info</a>
</div>
</div>
</div>
</div><!--Start of Tawk.to Script-->
<script type="text/javascript">
								var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
								(function(){
								var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
								s1.async=true;
								s1.src='https://embed.tawk.to/58580d535191003fde7b75b5/default';
								s1.charset='UTF-8';
								s1.setAttribute('crossorigin','*');
								s0.parentNode.insertBefore(s1,s0);
								})();
								</script>
<!--End of Tawk.to Script--></body>
</html>