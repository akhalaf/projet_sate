<!DOCTYPE html>
<html dir="ltr" lang="en-gb" xml:lang="en-gb" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="en-gb" name="language"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>404 - Error: 404</title>
<link href="/media/jui/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/media/jui/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
<link href="/templates/it_orchidshop/assets/css/icomoon.css" rel="stylesheet" type="text/css"/>
<link href="/templates/it_orchidshop/assets/less/template.css" rel="stylesheet" type="text/css"/>
<link href="/templates/it_orchidshop/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<link href="/templates/it_orchidshop/assets/less/template_responsive.css" rel="stylesheet" type="text/css"/>
<link href="/templates/it_orchidshop/assets/less/styles/custom_style.css" id="stylesheet" rel="stylesheet" type="text/css"/>
<!-- Google Fonts -->
<link href="http://fonts.googleapis.com/css?family=Crimson+Text|Droid+Serif|Open+Sans:400,300|Coming+Soon" rel="stylesheet" type="text/css"/>
<script src="/media/jui/js/jquery.min.js" type="text/javascript"></script>
</head>
<body class="error_page">
<div id="content_page">
<div id="logo_page">
<a href="https://www.braidingcenter.com/"><img alt="Braiding Center" class="logo" src="https://www.braidingcenter.com/images/New-Logo.png"/></a>
</div>
<h2>The requested page can't be found.</h2>
<p class="alert">Error: 404 - Category not found</p>
<p>Go to the Home Page</p>
<p><a class="btn icebtn" href="https://www.braidingcenter.com/"><i class="icon-home"></i> Home Page</a></p>
</div>
<link href="/modules/mod_icefullslide/assets/css/superslides.css" rel="stylesheet" type="text/css"/>
<script src="/modules/mod_icefullslide/assets/js/jquery.superslides.min.js" type="text/javascript"></script>
<div class="icefullslide icefullslide_preload" id="slides-127">
<ul class="slides-container">
<li><img alt="" src="https://www.braidingcenter.com/images/big-background-000.jpg"/><div class="icefullslide_caption icefullslide_with-pagination"><div class="container">Patricia</div></div></li><li><img alt="" src="https://www.braidingcenter.com/images/big-background-01.jpg"/></li><li><img alt="" src="https://www.braidingcenter.com/images/big-background-02.jpg"/></li><li><img alt="" src="https://www.braidingcenter.com/images/big-background-03.jpg"/></li><li><img alt="" src="https://www.braidingcenter.com/images/big-background_copy.jpg"/></li> </ul>
<nav class="slides-navigation">
<a class="next" href="#"><span>Next ›</span></a>
<a class="prev" href="#"><span>‹ Previous</span></a>
</nav>
<div class="icefullslide_preload_icon"></div>
</div>
<script type="text/javascript">
	
	<!-- Superslides Plugin for Full-screen IceSlideshow -->
	jQuery('#slides-127').superslides({
		animation: 'fade',
		pagination: true,
		play: 7000,
		animation_speed: 'normal',
		animation_easing: 'linear'
	});
	
	jQuery(window).load (function () { 
	  jQuery('#slides-127').removeClass('icefullslide_preload')
	});

</script>
</body>
</html>
