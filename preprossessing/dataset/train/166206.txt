<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<title>ATIZA - Música, noticias, bares y conciertos en Barcelona</title>
<meta content="ATIZA - Música, noticias, bares y conciertos en Barcelona" name="title"/>
<meta content="ATIZA - Música, noticias, bares y conciertos en Barcelona" name="DC.Title"/>
<meta content="ATIZA - Música, noticias, bares y conciertos en Barcelona" http-equiv="title"/>
<meta content="barcelona, conciertos, festivales, música, musicas, musicos, dj, djs, mp3, real audio, winamp, videos, bar, bares, guias, fotos, fotografias, videos musicales, videoclips, video clips, rock, pop, jazz, blues, r&amp;b, Rhythm and blues, funky, disco, soul, hip-hop, worl music, trance, techno, house, rumba, country, musica cubana, musica caribeńa, musica africana, discografía, discografias, sellos discograficos, discograficas, discos, cds, cddb, biografía, biografias, noticias, entrevistas, criticas, jam sessions, cantantes, vocalistas, guitarristas, guitarras, ruta 66, en espańol, gratis, smashing pumpkins, azita" name="Keywords"/>
<meta content="Atiza contiene la mas completa agenda de conciertos y bares, asi como la biografia y discografia de los principales grupos que tocan en Barcelona." name="Description"/>
<meta content="Entertainment Music - Music Barcelona - Concert Barcelona - All music in Barcelona - Information music Barcelona - Site music" name="document-classification"/>
<meta content="atiza.com" name="author"/>
<meta content="atiza.com" name="DC.Creator"/>
<link href="mailto:info@atiza.com" rev="made"/>
<meta content="barcelona, conciertos, festivales, música, musicas, musicos, dj, djs, mp3, real audio, winamp, videos, bar, bares, guias, fotos, fotografias, videos musicales, videoclips, video clips, rock, pop, jazz, blues, r&amp;b, Rhythm and blues, funky, disco, soul, hip-hop, worl music, trance, techno, house, rumba, country, musica cubana, musica caribeńa, musica africana, discografía, discografias, sellos discograficos, discograficas, discos, cds, cddb, biografía, biografias, noticias, entrevistas, criticas, jam sessions, cantantes, vocalistas, guitarristas, guitarras, ruta 66, en espańol, gratis, smashing pumpkins, azita" http-equiv="Keywords"/>
<meta content="Atiza contiene la mas completa agenda de conciertos y bares, asi como la biografia y discografia de los principales grupos que tocan en Barcelona." http-equiv="Description"/>
<meta content="Atiza contiene la mas completa agenda de conciertos y bares, asi como la biografia y discografia de los principales grupos que tocan en Barcelona." http-equiv="DC.Description"/>
<meta content="no-cache" http-equiv="Pragma"/>
<meta content="no-cache" http-equiv="Pragma-directive"/>
<meta content="15 days" name="Revisit"/>
<meta content="15 days" name="Revisit-after"/>
<meta content="all" name="robots"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="no-cache" http-equiv="Cache-Control"/>
<meta content="max-age=1" http-equiv="Cache-Control"/>
<meta content="s-maxage=1" http-equiv="Cache-Control"/>
<meta content="must-revalidate" forua="true" http-equiv="Cache-Control"/>
<meta content="no-cache" http-equiv="cache-directive"/>
<meta content="0" http-equiv="Expires"/>
<meta content="iFqJVLcZ/62nHKkz/HctucztnaWbXbkSi/fvEbBB+8E=" name="verify-v1"/>
<meta content="1538395536" property="fb:admins"/>
<!--// Atiza contiene la mas completa agenda de conciertos y bares, asi como la biografia y discografia de los principales grupos que tocan en Barcelona. //-->
<link href="/v3/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/v3/plugins/fontawesome-free-5.13.0-web/css/all.min.css" rel="stylesheet"/>
<link href="/v3/css/home.css" rel="stylesheet"/>
<link href="/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<!--<script> 
function foco(elemento) {
elemento.style.border = "3px solid #e3d100";
elemento.style.bgcolor = "#543005"; 
}
 
function no_foco(elemento) {
elemento.style.border = "1px solid #CCCCCC";
elemento.style.bgcolor = "#FFFFFF"; 
}
</script> -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1356433-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div class="container-fluid" id="outerBox">
<header id="headerBox">
<div class="row mb-0 mt-3"><!-- innerBoxHead -->
<div class="col-12 pt-0"><!-- headerBox -->
<img alt="atiza.com" class="ctz-img-responsive" src="/imagenes/logo-home.gif"/>
</div><!-- headerBox -->
</div><!-- innerBoxHead -->
<div class="row mt-0 pt-0 pb-0 pr-0" id="innerBoxHead"><!-- innerBoxHead -->
<div class="col-12 pt-0" id="navTop"><!-- navTop -->
<nav class="navbar navbar-expand-lg navbar-dark pt-0">
<button aria-controls="navbarSupportedContent" aria-expanded="false" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav mr-auto">
<li class="nav-item">
<a class="nav-link" href="/conciertos/">Conciertos</a>
</li>
<li class="nav-item">
<a class="nav-link" href="/grupos.php">Grupos</a>
</li>
<li class="nav-item">
<a class="nav-link" href="/musicos/tablonanuncios/">Tablón de anuncios</a>
</li>
<li class="nav-item">
<a class="nav-link" href="/criticas/conciertos/">Críticas</a>
</li>
<li class="nav-item">
<a class="nav-link" href="/discos/">Discos</a>
</li>
<li class="nav-item">
<a class="nav-link" href="/bares.php">Bares</a>
</li>
</ul>
</div><!-- navbarSupportedContent -->
<form action="concresult.php" class="form-inline navbar-form navbar-left my-2 mylg-0" method="post" style="font-size: 0.75em;">
<input name="BuscarTexto" size="24" type="search"/>
<input class="button" name="ir" style="background-color: #398EC1; color: #FFF; cursor: hand;" title="IR" type="Submit" value="IR"/>
</form>
</nav>
</div><!-- navTop -->
</div><!-- innerBoxHead -->
</header><!-- #header -->
<main>
<div class="row pt-2 pb-2" id="innerBox">
<!--<div class="row" id="contentBox">-->
<div class="col-12 col-lg-7" id="contenidoIzq">
<div class="row"><!-- contenidoIzqPrimerRow -->
<div class="col-12 col-sm-8">
<div class="w-100 h-100" id="conciertoDia">
<div id="conciertoDiaBg" style="background-image: url(/imgtmp/cdd/2020/09/alerta_roja.jpg); background-repeat: no-repeat; background-size: cover; background-position: center top;">
</div><!-- conciertoDiaBg -->
</div><!-- conciertoDia -->
</div><!-- col-sm-8 -->
<div class="col-12 col-sm-4 pl-sm-0">
<div id="talDia">
<br/>
<img alt="Tal día como hoy" class="ctz-img-responsive" src="/imagenes/talDia.gif"/>
<div id="talDiaFoto"><img alt="John Lees" height="162" src="/grupos/j/johnlees/img/johnlees_tdch.jpg" title="John Lees" width="122"/><br/>
</div>
<div id="talDiaTexto"><span class="grupoTalDia">John Lees</span> 
                                13/1/1947.
                                Fundador de <b>Barclay James Harvest</b> junto a Woolly Wolstenholme en 1966. Ha influenciado a gente como Eric Clapton, Bob Dylan, Joe Walsh.<br/>
<br/>
<strong>Disco destacado:</strong><br/>
            					"Barclay James Harvest And Other Short Stories" (1971).<br/>
<br/>
</div>
</div><!-- talDia -->
</div><!-- col-sm-4 -->
</div><!-- contenidoIzqPrimerRow -->
<div class="row mt-2"><!-- contenidoIzqSegundoRow -->
<div class="col-12 col-sm-4" id="contenidoInferiorIzq">
<div id="festivales">
<img alt="Festivales" class="ctz-img-responsive" src="/imagenes/festivales_v3.gif"/><br/>
<span class="festivales"><a href="/festivales/2020/ciutat-flamenco/" title="Agenda de conciertos del Festival Ciutat Flamenco 2020 de Barcelona">Ciutat Flamenco2020</a></span><br/><br/>
<a href="/festivales/2020/ciutat-flamenco/" title="Agenda de conciertos del Festival Ciutat Flamenco 2020 de Barcelona"><img alt="Agenda de conciertos del Festival Ciutat Flamenco 2020 de Barcelona" border="0" class="w-75" src="/festivales/2020/ciutat-flamenco/img/home.jpg"/></a> <br/>
<span class="fecha"><a href="/festivales/2020/ciutat-flamenco/" title="Agenda de conciertos del Festival Ciutat Flamenco 2020 de Barcelona">Del 24 al 31 de Otubre</a></span> <br/>
<img alt="" height="6" src="imagenes/barra_festivales.gif" style="width: 85% !important;"/><br/>
<span class="festivales"><a href="/festivales/2020/lem/" title="Agenda de conciertos del LEM - International Experimental Music Meeting Gràcia 2020">OFF LEM 2020</a></span><br/><br/>
<a href="/festivales/2020/lem/" title="Agenda de conciertos del LEM - International Experimental Music Meeting Gràcia 2020"><img alt="Agenda de conciertos del LEM - International Experimental Music Meeting Gràcia 2020" border="0" class="w-75" src="/festivales/2020/lem/img/home.jpg"/></a> <br/>
<span class="fecha"><a href="/festivales/2020/lem/" title="Agenda de conciertos del LEM - International Experimental Music Meeting Gràcia 2020">Del 9 al 30 de Otubre</a></span> <br/>
<img alt="" height="6" src="imagenes/barra_festivales.gif" style="width: 85% !important;"/><br/>
<span class="festivales"><a href="/festivales/2020/jazz-terrassa-2/" title="Agenda de conciertos del 39 Festival Jazz Terrassa 2020">39 Jazz Terrassa</a></span><br/><br/>
<a href="/festivales/2020/jazz-terrassa-2/" title="Agenda de conciertos del 39 Festival Jazz Terrassa 2020"><img alt="Agenda de conciertos del 39 Festival Jazz Terrassa 2020" border="0" class="w-75" src="/festivales/2020/jazz-terrassa-2/img/home.jpg"/></a> <br/>
<span class="fecha"><a href="/festivales/2020/jazz-terrassa-2/" title="Agenda de conciertos del 39 Festival Jazz Terrassa 2020">Del 8 al 31 de Otubre</a></span> <br/>
<img alt="" height="6" src="imagenes/barra_festivales.gif" style="width: 85% !important;"/><br/>
<span class="festivales"><a href="/festivales/2020/blues-cerdanyola/" title="Agenda de conciertos del XXIX'5 Festival Internacional de Blues de Cerdanyola 2020">Cerdanyola Blues</a></span><br/><br/>
<a href="/festivales/2020/blues-cerdanyola/" title="Agenda de conciertos del XXIX'5 Festival Internacional de Blues de Cerdanyola 2020"><img alt="Agenda de conciertos del XXIX'5 Festival Internacional de Blues de Cerdanyola 2020" border="0" class="w-75" src="/festivales/2020/blues-cerdanyola/img/home.jpg"/></a> <br/>
<span class="fecha"><a href="/festivales/2020/blues-cerdanyola/" title="Agenda de conciertos del XXIX'5 Festival Internacional de Blues de Cerdanyola 2020">Del 2 al 11 de Octubre</a></span> <br/>
<img alt="" height="6" src="imagenes/barra_festivales.gif" style="width: 85% !important;"/><br/>
<span class="festivales"><a href="/festivales/2020/merce-bam/" title="Agenda de conciertos de las Fiestas de la Mercè y BAM 2020 de Barcelona">Fiestas de la Mercè y BAM</a></span><br/><br/>
<a href="/festivales/2020/merce-bam/" title="Agenda de conciertos de las Fiestas de la Mercè y BAM 2020 de Barcelona"><img alt="Agenda de conciertos de las Fiestas de la Mercè y BAM 2020 de Barcelona" border="0" class="w-75" src="/festivales/2020/merce-bam/img/home.jpg"/></a> <br/>
<span class="fecha"><a href="/festivales/2020/merce-bam/" title="Agenda de conciertos de las Fiestas de la Mercè y BAM 2020 de Barcelona">Del 23 al 27 de Septiembre</a></span> <br/>
<br/>
</div><!-- festivales -->
<br/>
</div><!-- contenidoInferiorIzq -->
<div class="col-12 col-sm-8 pl-sm-0" id="contenidoInferiorCentro">
<div id="imagenQueEstaPasando">
<img alt="Qué está pasando" class="ctz-responsive" src="/imagenes/queEstaPasando.gif"/>
<div id="comentarioQueEstaPasando">
<div class="row mt-2">
<div class="col-12">
<a href="/usuarios/perfil/index.php?alias=carlosaura" title="carlosaura"><img align="left" alt="carlosaura" border="0" height="35" hspace="5" src="https://www.gravatar.com/avatar.php?gravatar_id=7ca95c18641c870d8cfebebffa15b71b&amp;default=https%3A%2F%2Fwww.atiza.com%2F%2Fimagenes%2Fhome_qepea_01.gif&amp;size=35" width="35"/></a><span class="gravatar1"><a href="/usuarios/perfil/index.php?alias=carlosaura" title="carlosaura">carlosaura <img border="0" height="12" src="/usuarios/img/miniperfil12.gif" width="12"/></a></span> <span class="txtQueEstaPasando"><a href="/musicos/tablonanuncios/forosrespuestas.php?MsgId=2387801">ha publicado un anuncio en el Tablón de Anuncios</a></span> <span class="fechaQueEstaPasando"><a href="/musicos/tablonanuncios/forosrespuestas.php?MsgId=2387801">(hoy a las 13:18h.)</a></span>
</div>
</div>
<div class="row mt-2">
<div class="col-12">
<img align="left" alt="salsa5" border="0" height="35" hspace="5" src="/imagenes/home_qepea_01.gif" width="35"/><span class="gravatar1">salsa5</span> <span class="txtQueEstaPasando"><a href="/musicos/tablonanuncios/forosrespuestas.php?MsgId=2387800">ha publicado un anuncio en el Tablón de Anuncios</a></span> <span class="fechaQueEstaPasando"><a href="/musicos/tablonanuncios/forosrespuestas.php?MsgId=2387800">(hoy a las 12:47h.)</a></span>
</div>
</div>
<div class="row mt-2">
<div class="col-12">
<a href="/usuarios/perfil/index.php?alias=angel casado" title="angel casado"><img align="left" alt="angel casado" border="0" height="35" hspace="5" src="https://www.gravatar.com/avatar.php?gravatar_id=c56e83ce3277d8b682e67180940c89f9&amp;default=https%3A%2F%2Fwww.atiza.com%2F%2Fimagenes%2Fhome_qepea_01.gif&amp;size=35" width="35"/></a><span class="gravatar1"><a href="/usuarios/perfil/index.php?alias=angel casado" title="angel casado">angel casado <img border="0" height="12" src="/usuarios/img/miniperfil12.gif" width="12"/></a></span> <span class="txtQueEstaPasando"><a href="/musicos/tablonanuncios/forosrespuestas.php?MsgId=2387799">ha publicado un anuncio en el Tablón de Anuncios</a></span> <span class="fechaQueEstaPasando"><a href="/musicos/tablonanuncios/forosrespuestas.php?MsgId=2387799">(hoy a las 12:39h.)</a></span>
</div>
</div>
<div class="row mt-2">
<div class="col-12">
<img align="left" alt="ferrecio" border="0" height="35" hspace="5" src="/imagenes/home_qepea_01.gif" width="35"/><span class="gravatar1">ferrecio</span> <span class="txtQueEstaPasando"><a href="/musicos/tablonanuncios/forosrespuestas.php?MsgId=2387798">ha publicado un anuncio en el Tablón de Anuncios</a></span> <span class="fechaQueEstaPasando"><a href="/musicos/tablonanuncios/forosrespuestas.php?MsgId=2387798">(hoy a las 11:30h.)</a></span>
</div>
</div>
<div class="row mt-2">
<div class="col-12">
<a href="/usuarios/perfil/index.php?alias=manuelmira" title="manuelmira"><img align="left" alt="manuelmira" border="0" height="35" hspace="5" src="/imagenes/home_qepea_01.gif" width="35"/></a><span class="gravatar1"><a href="/usuarios/perfil/index.php?alias=manuelmira" title="manuelmira">manuelmira <img border="0" height="12" src="/usuarios/img/miniperfil12.gif" width="12"/></a></span> <span class="txtQueEstaPasando"><a href="/musicos/tablonanuncios/forosrespuestas.php?MsgId=2387797">ha publicado un anuncio en el Tablón de Anuncios</a></span> <span class="fechaQueEstaPasando"><a href="/musicos/tablonanuncios/forosrespuestas.php?MsgId=2387797">(hoy a las 11:03h.)</a></span>
</div>
</div>
</div>
</div>
<div class="mt-2" id="imagenDiscomatico">
<a href="/discos/"><img alt="Criticas de discos" border="0" height="41" src="/imagenes/discomatico_20.gif" title="Criticas de discos" width="217"/></a><br/>
<div id="comentarioDiscomatico"><br/>
<div class="row">
<div class="col-12">
<a href="/discos/2020/clara-peya-estat-de-larva/" title="Critica del disco Estat de Larva de Clara Peya"><img align="left" alt="Critica del disco Estat de Larva de Clara Peya" border="0" height="130" hspace="5" src="/discos/2020/clara-peya-estat-de-larva/home.jpg" width="130"/></a><span class="grupoDisco">Clara Peya</span><br/>
                                En Estat de Larva hay magia que crepita entre los sonidos del instrumento, en el percutir de los dedos sobre las teclas, los crujidos del taburete, de los pedales.<br/>
<span class="leerCriticaDisco"><a href="/discos/2020/clara-peya-estat-de-larva/" title="Critica del disco Estat de Larva de Clara Peya">Leer la crítica &gt;&gt;</a></span><br/>
</div>
</div>
<p><img height="6" hspace="8" src="imagenes/barra_critica.gif" style="width: 95% !important;" vspace="6"/></p>
<div class="row">
<div class="col-12">
<a href="/discos/2019/anna-ferrer-kronia/" title="Critica del disco Krönia de Anna Ferrer"><img align="left" alt="Critica del disco Krönia de Anna Ferrer" border="0" height="130" hspace="5" src="/discos/2019/anna-ferrer-kronia/home.jpg" width="130"/></a><span class="grupoDisco">Anna Ferrer</span><br/>
                                Krönia da cabida a programaciones, sintetizadores, mandolinas, ukeleles,  guitarrón, guitarra acústica y banjos, y suena  completamente natural.<br/>
<span class="leerCriticaDisco"><a href="/discos/2019/anna-ferrer-kronia/" title="Critica del disco Krönia de Anna Ferrer">Leer la crítica &gt;&gt;</a></span><br/>
</div>
</div>
<p><img height="6" hspace="8" src="imagenes/barra_critica.gif" style="width: 95% !important;" vspace="6"/></p>
<div class="row">
<div class="col-12">
<a href="/discos/2020/johnny-b-zero-metonymy-of-sound/" title="Critica del disco Metonymy of Sound de Johnny B. Zero"><img align="left" alt="Critica del disco Metonymy of Sound de Johnny B. Zero" border="0" height="130" hspace="5" src="/discos/2020/johnny-b-zero-metonymy-of-sound/home.jpg" width="130"/></a><span class="grupoDisco">Johnny B. Zero</span><br/>
                                Metonymy of Sound aúna el sonido más rockero de los 90 con un toque de hiperbolismo épico que recuerda tiempos más remotos.<br/>
<span class="leerCriticaDisco"><a href="/discos/2020/johnny-b-zero-metonymy-of-sound/" title="Critica del disco Metonymy of Sound de Johnny B. Zero">Leer la crítica &gt;&gt;</a></span><br/>
</div>
</div>
</div><!-- comentarioDiscomatico -->
<div><a href="/discos/" title="Más discos"><img align="right" alt="Más discos" border="0" height="20" src="/imagenes/masdiscos.gif" width="90"/></a></div>
</div><!-- imagenDiscomatico -->
</div><!-- contenidoInferiorCentro -->
</div><!-- contenidoIzqSegundoRow -->
</div><!-- contenidoIzq -->
<div class="col-12 col-lg-5 pl-lg-0 mt-2 mt-lg-0" id="contenidoDerecha">
<div id="criticaConciertos">
<a href="/criticas/conciertos/" title="Críticas de conciertos"><img alt="Críticas de conciertos" border="0" class="ctz-img-responsive" height="30" src="/imagenes/criticaConciertos.gif"/></a>
<div id="textoCriticaConciertos">
<div class="row">
<div class="col-12">
<a href="/criticas/conciertos/2020/barbarascuderi-jorditriquell-xavilozano-marcvidal/" title="Crítica del concierto de Bàrbara Scuderi, Jordi Triquell, Xavi Lozano &amp; Marc Vidal en Plaça Reial - Drap Art (Barcelona) el 19 de Diciembre de 2020"><img align="left" alt="Crítica del concierto de Bàrbara Scuderi, Jordi Triquell, Xavi Lozano &amp; Marc Vidal en Plaça Reial - Drap Art (Barcelona) el 19 de Diciembre de 2020" border="0" height="166" hspace="4" src="/criticas/conciertos/2020/barbarascuderi-jorditriquell-xavilozano-marcvidal/img/home.jpg" width="130"/></a><br/>
<span class="grupoCritica">Bàrbara Scuderi, Jordi Triquell, Xavi Lozano &amp; Marc Vidal</span><br/>
                                19/12/2020 - <span class="bar">Plaça Reial - Drap Art</span><br/>
                                Música y poesía han creado el milagro de convertir  una plaza Real inusualmente desierta en la pista de despegue de un viaje sonoro y literario.
                                <br/>
<span class="leerCritica"><a href="/criticas/conciertos/2020/barbarascuderi-jorditriquell-xavilozano-marcvidal/" title="Crítica del concierto de Bàrbara Scuderi, Jordi Triquell, Xavi Lozano &amp; Marc Vidal en Plaça Reial - Drap Art (Barcelona) el 19 de Diciembre de 2020">Leer la crítica &gt;&gt;</a></span>
</div>
</div>
<p><img height="6" hspace="8" src="imagenes/barra_critica.gif" style="width: 93% !important;" vspace="6"/></p>
<div class="row">
<div class="col-12">
<a href="/criticas/conciertos/2020/morente-and-roach-in-memoriam-l-auditori/" title="Crítica del concierto de Morente &amp; Roach in memoriam en L'Auditori (Barcelona) el 24 de Octubre de 2020"><img align="left" alt="Crítica del concierto de Morente &amp; Roach in memoriam en L'Auditori (Barcelona) el 24 de Octubre de 2020" border="0" height="166" hspace="4" src="/criticas/conciertos/2020/morente-and-roach-in-memoriam-l-auditori/img/home.jpg" width="130"/></a><br/>
<span class="grupoCritica">Morente &amp; Roach in memoriam</span><br/>
                                24/10/2020 - <span class="bar">L'Auditori</span><br/>
                                El espectáculo Morente &amp; Roach nos confirma que, como decía Morente, el flamenco parte de la fusión, como también el jazz.
                                <br/>
<span class="leerCritica"><a href="/criticas/conciertos/2020/morente-and-roach-in-memoriam-l-auditori/" title="Crítica del concierto de Morente &amp; Roach in memoriam en L'Auditori (Barcelona) el 24 de Octubre de 2020">Leer la crítica &gt;&gt;</a></span>
</div>
</div>
<p><img height="6" hspace="8" src="imagenes/barra_critica.gif" style="width: 93% !important;" vspace="6"/></p>
<div class="row">
<div class="col-12">
<a href="/criticas/conciertos/2020/vetusta-morla-auditori-parc-del-forum/" title="Crítica del concierto de Vetusta Morla en Auditori del Fòrum (Barcelona) el 6 de Marzo de 2020"><img align="left" alt="Crítica del concierto de Vetusta Morla en Auditori del Fòrum (Barcelona) el 6 de Marzo de 2020" border="0" height="166" hspace="4" src="/criticas/conciertos/2020/vetusta-morla-auditori-parc-del-forum/img/home.jpg" width="130"/></a><br/>
<span class="grupoCritica">Vetusta Morla</span><br/>
                                06/03/2020 - <span class="bar">Auditori del Fòrum</span><br/>
                                Atrevidos, sin complejos, sabedores que su público fiel como pocos, les acompañará en este nuevo viaje.
                                <br/>
<span class="leerCritica"><a href="/criticas/conciertos/2020/vetusta-morla-auditori-parc-del-forum/" title="Crítica del concierto de Vetusta Morla en Auditori del Fòrum (Barcelona) el 6 de Marzo de 2020">Leer la crítica &gt;&gt;</a></span>
</div>
</div>
<p><img height="6" hspace="8" src="imagenes/barra_critica.gif" style="width: 93% !important;" vspace="6"/></p>
<div class="row">
<div class="col-12">
<a href="/criticas/conciertos/2020/091-en-apolo-guitar-festival-bcn/" title="Crítica del concierto de 091 en Apolo (Barcelona) el 15 de Febrero de 2020"><img align="left" alt="Crítica del concierto de 091 en Apolo (Barcelona) el 15 de Febrero de 2020" border="0" height="166" hspace="4" src="/criticas/conciertos/2020/091-en-apolo-guitar-festival-bcn/img/home.jpg" width="130"/></a><br/>
<span class="grupoCritica">091</span><br/>
                                15/02/2020 - <span class="bar">Apolo</span><br/>
                                Repaso  a una forma de entender la música que ha creado escuela en muchos de los tótems del indie patrio actual.
                                <br/>
<span class="leerCritica"><a href="/criticas/conciertos/2020/091-en-apolo-guitar-festival-bcn/" title="Crítica del concierto de 091 en Apolo (Barcelona) el 15 de Febrero de 2020">Leer la crítica &gt;&gt;</a></span>
</div>
</div>
<p><img height="6" hspace="8" src="imagenes/barra_critica.gif" style="width: 93% !important;" vspace="6"/></p>
<div class="row">
<div class="col-12">
<a href="/criticas/conciertos/2020/new-york-tableau-en-tallers-24/" title="Crítica del concierto New York Tableau en Tallers 24 (Barcelona) el 14 de Febrero de 2020"><img align="left" alt="Crítica del concierto New York Tableau en Tallers 24 (Barcelona) el 14 de Febrero de 2020" border="0" height="166" hspace="4" src="/criticas/conciertos/2020/new-york-tableau-en-tallers-24/img/home.jpg" width="130"/></a><br/>
<span class="grupoCritica">New York Tableau</span><br/>
                                14/02/2020 - <span class="bar">Tallers 24</span><br/>
                                Gran concierto, intenso y emocionante, en el que los cinco músicos dieron una clase magistral de talento, riesgo y entrega sin fisuras.
                                <br/>
<span class="leerCritica"><a href="/criticas/conciertos/2020/new-york-tableau-en-tallers-24/" title="Crítica del concierto New York Tableau en Tallers 24 (Barcelona) el 14 de Febrero de 2020">Leer la crítica &gt;&gt;</a></span>
</div>
</div>
<p><img height="6" hspace="8" src="imagenes/barra_critica.gif" style="width: 93% !important;" vspace="6"/></p>
<div class="row">
<div class="col-12">
<a href="/criticas/conciertos/2020/zahara-en-palau-de-la-musica-catalana/" title="Crítica del concierto de Zahara en Palau de la Música Catalana (Barcelona) el 17 de Enero de 2020"><img align="left" alt="Crítica del concierto de Zahara en Palau de la Música Catalana (Barcelona) el 17 de Enero de 2020" border="0" height="166" hspace="4" src="/criticas/conciertos/2020/zahara-en-palau-de-la-musica-catalana/img/home.jpg" width="130"/></a><br/>
<span class="grupoCritica">Zahara</span><br/>
                                17/01/2020 - <span class="bar">Palau de la Música Catalana</span><br/>
                                Concierto redondo, que los allí presentes recordaremos durante mucho tiempo.
                                <br/>
<span class="leerCritica"><a href="/criticas/conciertos/2020/zahara-en-palau-de-la-musica-catalana/" title="Crítica del concierto de Zahara en Palau de la Música Catalana (Barcelona) el 17 de Enero de 2020">Leer la crítica &gt;&gt;</a></span>
</div>
</div>
</div><!-- textoCriticaConciertos -->
<div><a href="/criticas/conciertos/" title="Más críticas"><img align="right" alt="Más críticas" border="0" height="20" src="/imagenes/mascriticas.gif" width="101"/></a></div>
</div><!-- criticaConciertos -->
</div><!-- contenidoDerecha -->
</div><!-- innerBox -->
</main>
<footer id="footer">
<div class="row mt-4">
<div class="col-12 text-center">
<a href="https://www.facebook.com/AtizaCom" target="_blank"><i class="fab fa-facebook-f fa-3x ctz-footer-social"></i></a>
<a href="https://twitter.com/AtizaCom" target="_blank"><i class="fab fa-twitter fa-3x ctz-footer-social"></i></a>
<a href="https://www.instagram.com/AtizaCom/" target="_blank"><i class="fab fa-instagram fa-3x ctz-footer-social"></i></a>
</div>
</div>
<div class="row mt-4 pt-3 ctz-footer-bigmenu-row">
<div class="col-12 d-flex justify-content-center">
<div class="row" style="width: 758px;">
<div class="col-12 col-sm-4 text-left">
<p class="text-uppercase mb-1 ctz-footer-bigmenu">Agenda</p>
<ul class="list-unstyled list-inline ctz-footer-bigmenu">
<li class="list-item">
<a class="ctz-footer-bigmenu" href="/conciertos/" target="_top">Conciertos</a>
</li>
<li class="list-item">
<a class="ctz-footer-bigmenu" href="/grupos.php" target="_top">Grupos</a>
</li>
<li class="list-item">
<a class="ctz-footer-bigmenu" href="/bares.php" target="_top">Bares</a>
</li>
<li class="list-item">
<a class="ctz-footer-bigmenu" href="/musicos/jamsessions.php" target="_top">Jam Sessions</a>
</li>
</ul>
</div>
<div class="col-12 col-sm-4 text-left">
<p class="text-uppercase mb-1 ctz-footer-bigmenu">Tablón de anuncios</p>
<ul class="list-unstyled list-inline ctz-footer-bigmenu">
<li class="list-item">
<a class="ctz-footer-bigmenu" href="/musicos/tablonanuncios/#name7" target="_top">Clases particulares</a>
</li>
<li class="list-item">
<a class="ctz-footer-bigmenu" href="/musicos/tablonanuncios/#name3" target="_top">Ofertas y demandas</a>
</li>
<li class="list-item">
<a class="ctz-footer-bigmenu" href="/musicos/tablonanuncios/#name10" target="_top">Servicios</a>
</li>
<li class="list-item">
<a class="ctz-footer-bigmenu" href="/musicos/tablonanuncios/#name1" target="_top">Buscando músicos</a>
</li>
<li class="list-item">
<a class="ctz-footer-bigmenu" href="/musicos/tablonanuncios/#name4" target="_top">Compra venta</a>
</li>
</ul>
</div>
<div class="col-12 col-sm-4 text-left pl-sm-5">
<p class="text-uppercase mb-1 ctz-footer-bigmenu">Redacción</p>
<ul class="list-unstyled list-inline ctz-footer-bigmenu">
<li class="list-item">
<a class="ctz-footer-bigmenu" href="/criticas/conciertos/" target="_top">Críticas de conciertos</a>
</li>
<li class="list-item">
<a class="ctz-footer-bigmenu" href="/discos/" target="_top">Críticas de discos</a>
</li>
<li class="list-item">
<a class="ctz-footer-bigmenu" href="/rockimpres/conciertos.htm" target="_top">Fotos de conciertos</a>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="row mt-3">
<div class="col-12">
<a href="/." target="_top"><img alt="Atiza contiene la mas completa agenda de conciertos y bares, asi como la biografia y discografia de los principales grupos que tocan en Barcelona" border="0" class="mx-auto d-block ctz-img-responsive" height="28" hspace="45" src="/imagenes/atiza02.gif" title="Atiza contiene la mas completa agenda de conciertos y bares, asi como la biografia y discografia de los principales grupos que tocan en Barcelona" width="120"/></a>
</div>
</div>
<div class="row mt-2">
<div class="col-12 ctz-footernav">
<ol class="list-unstyled ctz-footernav mx-auto">
<li class="ctz-footernav-brand">
						Atiza 1999-2021 · 
					</li>
<li class="ctz-footernav-item">
<a href="javascript:var suscr=window.open('/inc/avisolegal.htm','avisolegal','width=400,height=350,scrollbars=yes');">Aviso Legal</a> · 
					</li>
<li class="ctz-footernav-item">
<a href="javascript:var suscr=window.open('/inc/politicaprivacidad.htm','politprivac','width=400,height=540,scrollbars=yes');">Política de Privacidad</a> · 
					</li>
<li class="ctz-footernav-item">
<a href="javascript:var suscr=window.open('/inc/condicionesuso.htm','condicuso','width=400,height=540,scrollbars=yes');">Condiciones de Uso</a> · 
					</li>
<li class="ctz-footernav-item">
<a href="javascript:var suscr=window.open('/musicos/tablonanuncios/condicionesuso.htm','condicusot','width=400,height=540,scrollbars=yes');">Condiciones de Uso Tablón de Anuncios</a>
</li>
</ol>
</div>
</div>
</footer><!-- #footer -->
</div><!-- outerBox -->
<div id="cookie-box">
    Atiza.com usa cookies para mejorar tu navegación.<br/>
    Obtén más información o descubre cómo modificar tus cookies. <a href="javascript:var suscr=window.open('/inc/politica-de-cookies/','condicuso','width=400,height=540,scrollbars=yes');">Más sobre nuestras cookies.</a>
<div onclick="setCookie('cookies_ok', '1', '365'); document.getElementById('cookie-box').style.display='none';">Aceptar Cookies</div>
</div>
<script>
if (getCookie("cookies_ok")=="1") document.getElementById('cookie-box').style.display='none';
function getCookie(name)
{
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}
function setCookie(cname, cvalue, exdays) 
{
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
} 
</script>
<script src="/v3/js/jquery.min.js"></script>
<script src="/v3/js/bootstrap.min.js"></script>
</body>
</html>
