<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=1280" name="viewport"/>
<meta content="none" name="msapplication-config"/>
<title>Chemistry.com - Personality Matching for Serious Relationships</title>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="https://pmi.peoplemedia.com/pmicontent/169/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://pmi.peoplemedia.com/pmicontent/169/images/apple-touch-icon-57x57.png" rel="apple-touch-icon-precomposed" sizes="57x57"/>
<link href="https://pmi.peoplemedia.com/pmicontent/169/images/apple-touch-icon-72x72.png" rel="apple-touch-icon-precomposed" sizes="72x72"/>
<link href="https://pmi.peoplemedia.com/pmicontent/169/images/apple-touch-icon-114x114.png" rel="apple-touch-icon-precomposed" sizes="114x114"/>
<link href="https://pmi.peoplemedia.com/pmicontent/169/images/apple-touch-icon-144x144.png" rel="apple-touch-icon-precomposed" sizes="144x144"/>
<link href="https://fonts.googleapis.com/css?family=PT+Sans:400" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=PT+Sans:700" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=PT+Sans:400italic" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=PT+Sans:700italic" rel="stylesheet" type="text/css"/>
<link href="/css/redesign_fonts.css" rel="stylesheet" type="text/css"/>
<link href="https://pmi.peoplemedia.com/pmicontent/styles/base_external.css" rel="stylesheet" type="text/css"/>
<link href="https://pmi.peoplemedia.com/pmicontent/169/theme.css" rel="stylesheet" type="text/css"/>
<script src="https://pmi.peoplemedia.com/pmicontent/scripts/jquery/jquery-3.5.1.min.js" type="text/javascript"></script>
<script src="https://pmi.peoplemedia.com/pmicontent/scripts/jquery/jquery-migrate-3.3.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
        var $jq = jQuery.noConflict();
    </script>
<script src="https://pmi.peoplemedia.com/pmicontent/scripts/moment.min.js"></script>
<!--Adomik randomizer for ad call key value targeting-->
<script class="" type="text/javascript">
	window.Adomik = window.Adomik || {};
	Adomik.randomAdGroup = function () {
		var rand = Math.random();
		switch (false) {
		case !(rand < 0.09): return "ad_ex" + (Math.floor(100 * rand));
		case !(rand < 0.10): return "ad_bc";
		default: return "ad_opt";
		}
	};
</script>
<script>
        
        var PeopleMediaConfig = PeopleMediaConfig || {};
        PeopleMediaConfig.Configuration = (function() {
            "use strict";
            var config = {};
            try {
                config.siteId = 169;
                config.currentUserSiteId = 169;
                config.profile = "";
                config.username = "";
                config.domain = "Chemistry.com";
                config.baseDomain = "Chemistry";
                config.siteHostName = "www.chemistry.com";
                config.siteImagePath = "https://pmi.peoplemedia.com/pmicontent/169";
                config.commonImagePath = "https://pmi.peoplemedia.com/pmicontent/v6/Images/common";
                config.imagePath = "https://pmi.peoplemedia.com/pmicontent/v6";
                config.themeImagePath = "https://pmi.peoplemedia.com/pmicontent/v6/images/gray";
                config.baseImagePath = "https://pmi.peoplemedia.com/pmicontent";
                config.popupPath = "/html/Popup/";
                config.webSyncUrl = "https://rtn.services.peoplemedia.com/websync.ashx";
                config.isSecureConnection = true;
                config.isUpgradePage = false;
                config.isExternal = true;
                config.isLoggedIn = false;
                config.showRealTimeNotificationAlerts = false;
                config.showChatOnlyWithoutRtnAlert = false;
                config.persistentLoginEnabled = false;
                config.isMobile = false;
                config.autorefresh = false;
                config.chatEnabled = false;
                config.defaultSessionToken = "";
                config.channel = "/chat/169/";
                config.chatBasePath = "https://chat.services.peoplemedia.com/v1";
                config.chatApi = "https://chat.services.peoplemedia.com/v1/api";
                config.chatLoggingEnabled = false;
                config.showChatPane = false;
                config.pageDefinitionId = 103319;
                config.publicMemberId = "";
                config.combineFlirtsAndLikes = false;
                config.pixelsEnabledForSite = true;
                config.refreshAfterConsent = true;
            } catch (ex) {
            }
            return config;
        })();
</script>
<script language="JavaScript" src="https://pmi.peoplemedia.com/pmicontent/build/5f9ef92/scripts/desktop/polyfill.js" type="text/javascript"></script>
<script src="https://pmi.peoplemedia.com/pmicontent/build/5f9ef92/scripts/desktop/url-search-params-polyfill.js"></script>
<script language="JavaScript" src="https://pmi.peoplemedia.com/pmicontent/build/5f9ef92/scripts/desktop/peoplemedia.js" type="text/javascript"></script>
<script language="JavaScript" src="https://pmi.peoplemedia.com/pmicontent/build/5f9ef92/scripts/desktop/menu.js" type="text/javascript"></script>
<script language="JavaScript" src="https://pmi.peoplemedia.com/pmicontent/scripts/loggerv2.js" type="text/javascript"></script>
<script language="javascript" src="https://pmi.peoplemedia.com/pmicontent/scripts/stacktrace-min-0.3.js" type="text/javascript"></script>
<script src="https://pmi.peoplemedia.com/pmicontent/build/5f9ef92/scripts/desktop/login-form.js" type="text/javascript"></script>
<!--Adomik randomizer for ad call key value targeting-->
<script class="" type="text/javascript">
        window.Adomik = window.Adomik || {};
        Adomik.randomAdGroup = function () {
        var rand = Math.random();
        switch (false) {
        case !(rand < 0.09): return "ad_ex" + (Math.floor(100 * rand));
        case !(rand < 0.10): return "ad_bc";
        default: return "ad_opt";
        }
        };
    </script>
<script>
        
        var PeopleMediaConfig = PeopleMediaConfig || {};
        PeopleMediaConfig.Configuration = (function() {
            "use strict";
            var config = {};
            try {
                config.siteId = 169;
                config.currentUserSiteId = 169;
                config.profile = "";
                config.username = "";
                config.domain = "Chemistry.com";
                config.baseDomain = "Chemistry";
                config.siteHostName = "www.chemistry.com";
                config.siteImagePath = "https://pmi.peoplemedia.com/pmicontent/169";
                config.commonImagePath = "https://pmi.peoplemedia.com/pmicontent/v6/Images/common";
                config.imagePath = "https://pmi.peoplemedia.com/pmicontent/v6";
                config.themeImagePath = "https://pmi.peoplemedia.com/pmicontent/v6/images/gray";
                config.baseImagePath = "https://pmi.peoplemedia.com/pmicontent";
                config.popupPath = "/html/Popup/";
                config.webSyncUrl = "https://rtn.services.peoplemedia.com/websync.ashx";
                config.isSecureConnection = true;
                config.isUpgradePage = false;
                config.showRealTimeNotificationAlerts = false;
                config.showChatOnlyWithoutRtnAlert = false;
                config.persistentLoginEnabled = false;
                config.isMobile = false;
                config.autorefresh = false;
                config.chatEnabled = false;
                config.defaultSessionToken = "";
                config.channel = "/chat/169/)";
                config.chatBasePath = "https://chat.services.peoplemedia.com/v1";
                config.chatApi = "https://chat.services.peoplemedia.com/v1/api";
                config.chatLoggingEnabled = false;
                config.showChatPane = false;
                config.pageDefinitionId = 103319;
                config.publicMemberId = "";
                config.combineFlirtsAndLikes = false;
                config.pixelsEnabledForSite = true;
                config.refreshAfterConsent = true;
            } catch (ex) {
            }
            return config;
        })();
    </script>
<!-- Google Analytics -->
<script class="" type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-380157-1']);
        
            _gaq.push(['_setDomainName', 'chemistry.com']);
            _gaq.push(['_addIgnoredRef', 'chemistry.com']);
        
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
<!-- End Google Analytics -->
</head>
<body class="external-layout ">
<div id="header">
<div id="headercontent">
<div id="logo" title="Chemistry.com">
<!-- change logo to use .com only if on external help page and on our time -->
<a href="/?notrack"><img alt="" border="0" src="https://pmi.peoplemedia.com/pmicontent/169/images/logo.png"/></a>
</div>
<div id="externalheadernologin">
<p style="display: "><a href="http://www.peoplemedia.com" target="_blank">A People Media Site</a></p>
</div>
</div>
</div>
<div id="maincontent">
<div id="columnleftexternal">
<script src="https://www.google.com/recaptcha/api.js" type="text/javascript"></script>
<div id="externallogin">
<div class="externalcontainer bgcolor-main">
<div id="externalloginmiddleleft">
<h1>Login to Chemistry.com</h1>
<div class="field-validation-error hidden">
<div id="externalloginerror">
<p></p>
</div>
<div id="externalloginerrorarrow"><img src="https://pmi.peoplemedia.com/pmicontent/169/images/external/errorarrow.png"/></div>
</div>
<form action="https://www.chemistry.com/v3/login/process" id="frmLogin" method="post">
<input name="__RequestVerificationToken" type="hidden" value="1unwTnVgq2Qho4H4SeVSjxZP30nlGf4m-SOGo3WbnxYXGGzinRyT4kPm6ez04KNUKyiCwZmbFNtLq7kX4AESqY7aFPs1"/>
<input name="SkipCSSVerif" type="hidden" value="HTMLEditor"/>
<input name="FromLocation" type="hidden" value="/v3/login"/>
<input name="PageDefinitionId" type="hidden" value="103319"/>
<input name="PageDefinitionSubId" type="hidden" value=""/>
<div id="externalloginusername">
<p>Email</p>
<input id="username" name="username" tabindex="1" type="text" value=""/>
</div>
<div id="externalloginpassword">
<p>Password</p>
<input autocomplete="off" id="password" name="password" tabindex="2" type="password" value=""/>
</div>
<div id="externalloginrememberme">
<input id="chkRememberMe" name="RememberMe" type="checkbox" value="true"/>
<label for="chkRememberMe">
<p>Remember Me</p>
</label>
</div>
<div class="g-recaptcha login-recaptcha" data-sitekey="6LdxbFoUAAAAAP9j5vutRbZMgq1htdFZQtK9cxGo" id="gCaptcha"></div>
<div class="login-button" id="externalloginbtn"><a class="button_style" href="javascript:void(0);" title="Login">Login</a></div>
</form>
</div>
<div class="bgcolor-login" id="externalloginmiddleright">
<p>
                Not a member yet?<br/>
<a href="/v3/signup">Join Free</a>
</p>
<p>
                Forgot password?<br/>
<a href="/v3/forgotpassword">Click here</a>
</p>
</div>
<p class="clearfix"> </p>
</div>
</div>
<script type="text/javascript">
    $jq(function() {
        PeopleMedia.Login.initialize({
            hasError: false,
            promoCode: ""
        });
    });
</script>
</div>
<div id="columnrightexternal">
</div>
<p class="clearfix"> </p>
<p class="clearfix"> </p>
</div>
<div id="footer">
<div id="footercontent">
<div id="footercontentleft">
<p>
                Copyright © 2021 People Media. All rights reserved.  169x2006.  <a href="/v3/termsandconditions">Terms of Use</a> | <a href="/v3/privacypolicy?notrack">Privacy Policy</a> | <a href="/v3/cookiepolicy">Cookie Policy</a> | <a href="/v3/security">Security</a>
</p>
</div>
</div>
</div>
<div id="externallinks">
<p>
<a href="https://www.chemistry.com?notrack">home</a> |
            <a href="/v3/datingtips">safety tips</a> |
            <a href="/v3/guidelines">guidelines</a> |
            <a href="/v3/security">security</a> |
            <a href="/v3/help">contact us</a> |
            <a href="https://www.chemistry.com/v3/billing/">billing</a> |
            <a href="https://www.match.com/cp/careers/CurrentOpenings.html">careers</a> |
            <a href="/v3/aboutonlinedating">about</a> |
            <a href="https://www.matchmediagroup.com" target="_blank">advertise with us</a> |
            <a href="/v3/externalsearch">search</a> |
            <a href="/v3/signup">join now</a> |
            <a href="javascript:void(0);" onclick="window.external.AddFavorite('https://www.chemistry.com?notrack', 'Chemistry.com - Personality Matching for Serious Relationships') ">bookmark page</a> |
            <a href="/v3/sitemap">site map</a>
<br/>
<a href="https://www.match.com" target="_blank">Match.com</a> |
            <a href="https://www.chemistry.com" target="_blank">Chemistry.com</a> |
            <a href="https://www.ourtime.com/?notrack" target="_blank">Mature Dating</a> |
            <a href="https://www.blackpeoplemeet.com/?notrack" target="_blank">Black Singles</a> |
            <a href="https://www.bbpeoplemeet.com/?notrack" target="_blank">Big and Beautiful</a>
</p>
</div>
<form id="sitervt">
<input name="__RequestVerificationToken" type="hidden" value="PiFm8corQepz3bTVhKV8ZBwxGKEAcA5Sco2z6yrWO9LD8zjtBzgpDLbJk9aqsVcGFgJk5SndTibSBEaE1XODJq71FQk1"/>
</form>
<script class="" type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 850818608;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<div style="display:none;">
<script class="" src="//www.googleadservices.com/pagead/conversion.js" type="text/javascript">
</script>
</div>
<noscript>
<div style="display:inline;">
<img alt="" height="1" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/850818608/?guid=ON&amp;script=0" style="border-style:none;" width="1"/>
</div>
</noscript>
<!-- Facebook Pixel Code -->
<script class="" type="text/javascript">
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function()
{n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)}
;if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '621173494639828'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" src="https://www.facebook.com/tr?id=621173494639828&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/>
</noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
</body>
</html>
