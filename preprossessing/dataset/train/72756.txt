<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8"> <![endif]--><!--[if IE 9]> <html lang="es" class="ie9"> <![endif]--><!--[if !IE]><!--><html lang="es"> <!--<![endif]-->
<head>
<title>AcuCanarias. Centro de Formación Profesional (Tenerife - Canarias)</title>
<meta content="text/html;charset=utf-8" http-equiv="content-type"/>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="Cursos del Centro de Formación Profesional Acucanarias (Tenerife - Canarias)" name="description"/>
<meta content="redesinformaticas.net" name="author"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="https://www.acucanarias.com" property="og:url"/>
<meta content="website" property="og:type"/>
<meta content="ACU CANARIAS" property="og:title"/>
<meta content="Cursos del Centro de Formación Profesional Acucanarias (Tenerife - Canarias)" property="og:description"/>
<meta content="/images/logo.png" property="og:image"/>
<link href="https://www.acucanarias.com/images/ico/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57" type="image/png"/>
<link href="https://www.acucanarias.com/images/ico/apple-touch-icon-60x60.png" rel="apple-touch-icon" sizes="60x60" type="image/png"/>
<link href="https://www.acucanarias.com/images/ico/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72" type="image/png"/>
<link href="https://www.acucanarias.com/images/ico/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76" type="image/png"/>
<link href="https://www.acucanarias.com/images/ico/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="https://www.acucanarias.com/images/ico/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="https://www.acucanarias.com/images/ico/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="https://www.acucanarias.com/images/ico/manifest.json" rel="manifest" type="image/x-icon"/>
<link href="https://www.acucanarias.com/images/ico/favicon.ico" rel="shortcut icon" sizes="76x76" type="image/x-icon"/>
<!--[if IE]>
    <script src="https://www.acucanarias.com/js/html5shiv.min.js"></script>
    <script src="https://www.acucanarias.com/js/respond.min.js"></script>
    <![endif]-->
<link href="https://www.acucanarias.com/assets/plugins/bootstrap/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.acucanarias.com/assets/plugins/slider.revolution/css/extralayers.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.acucanarias.com/assets/plugins/slider.revolution/css/settings.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.acucanarias.com/css/toastr.min.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.acucanarias.com/assets/css/essentials.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.acucanarias.com/assets/css/layout.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.acucanarias.com/assets/css/header-1.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.acucanarias.com/css/toastr.min.css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.acucanarias.com/js/cookies.js"></script>
<link href="https://www.acucanarias.com/css/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.acucanarias.com/css/style-acu.css" media="all" rel="stylesheet" type="text/css"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
</head>
<body class="header-sticky enable-animation">
<div id="slidetop">
<div class="container">
<div class="row">
<div class="col-md-3">
<h6>¿Por qué ACU CANARIAS?</h6>
<p>ACU CANARIAS es un Centro de Formación polivalente y versátil dedicado a la formación técnica con una dilata experiencia de más de 25 años y dirigida tanto a profesionales como al público en general, gracias a nuestro esfuerzo por ofrecer la mejor formación posible, hemos incrementado nuestra oferta formativa adaptándola a las necesidades de la población actual.</p>
</div>
<div class="col-md-7">
<h6>Servicios principales</h6>
<ul>
<li><i class="fa fa-angle-right"></i> Tutorías presénciales y telefónicas.(Sin cargo adicional).</li>
<li><i class="fa fa-angle-right"></i> Biblioteca y Sala de estudio. (Sin cargo adicional).</li>
<li><i class="fa fa-angle-right"></i> Material didáctico básico y de prácticas para el seguimiento de cada formación. (Sin cargo adicional).</li>
<li><i class="fa fa-angle-right"></i> Carnet de alumno, con sus múltiples ventajas. (Sin cargo adicional).</li>
<li><i class="fa fa-angle-right"></i> Servicio de fotocopias.</li>
<li><i class="fa fa-angle-right"></i> Clínica propia o centros concertados para realizar las prácticas.</li>
<li><i class="fa fa-angle-right"></i> Charlas gratuitas, a lo largo del curso.</li>
<li><i class="fa fa-angle-right"></i> Formación básica gratuita o subvencionada con la colaboración de los laboratorios más prestigiosos del sector.</li>
<li><i class="fa fa-angle-right"></i> Formación dirigida a profesionales gratuita o subvencionada, con la colaboración de asociaciones profesionales y/o los laboratorios más prestigiosos del sector.</li>
<li><i class="fa fa-angle-right"></i> Red WIFI en todo el Centro. (Sin cargo adicional).</li>
</ul>
</div>
<div class="col-md-2">
<h6>Localización</h6>
<ul class="list-unstyled">
<li><b>Dirección:</b> Av. Leonardo Torriani nº 61 local<br/> La Laguna C.P. 38205</li>
<li><b>Teléfono 1:</b>  922 25 71 54</li>
<li><b>Teléfono 2:</b>  922 12 12 69</li>
<li><b>Email:</b>  administracion@acucanarias.com</li>
</ul>
</div>
</div>
</div>
<a class="slidetop-toggle" href="#"></a>
</div>
<div id="wrapper">
<div id="topBar">
<div class="container">
<div class="row">
<div class="col-11">
<ul class="top-links list-inline">
<li><a href="/">Inicio</a></li>
<li><a href="https://www.acucanarias.com/quienes_somos">¿Quiénes somos?</a></li>
<li><a href="https://www.acucanarias.com/metodologia">Metodología</a></li>
<li><a href="https://www.acucanarias.com/novedades">Novedades</a></li>
<li><a href="https://www.acucanarias.com/localizacion">Localización</a></li>
<li><a href="https://moodle.acucanarias.com/" target="_blank">Campus Virtual</a></li>
<li><a href="https://www.plataformateleformacion.com/" target="_blank"><span class="animate hover-animate-bounceIn">Cursos On-line</span></a></li>
<li><a href="https://www.acucanarias.com/applicant/offer#">Bolsa de Empleo</a></li>
<li><a href="https://www.acucanarias.com/contactar#">Contactar</a></li>
</ul>
</div>
<div class="col-1">
<ul class="text-access pull-right">
<li class="text-access">
<a href="https://www.acucanarias.com/update"><i class="fa fa-unlock-alt primary-color"></i></a>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="navbar-toggleable-md sticky clearfix mb-50" id="header">
<header id="topNav">
<div class="container-fluid">
<div class="row">
<div class="col-4">
<button class="btn btn-mobile" data-target=".nav-main-collapse" data-toggle="collapse">
<i class="fa fa-bars"></i>
</button>
<a class="logo float-left" href="https://www.acucanarias.com">
<img alt="Logo ACU CANARIAS" src="https://www.acucanarias.com/images/logo-04.png"/>
</a>
</div>
<div class="col-8 p-t-15">
<div class="navbar-collapse collapse float-right nav-main-collapse submenu-dark">
<nav class="nav-main">
<ul class="nav nav-pills nav-main" id="topMain">
<li class="dropdown">
<a class="dropdown-toggle" href="#">
                                Presenciales
                            </a>
<ul class="dropdown-menu">
<li><a href="https://www.acucanarias.com/courses/presenciales/sanitarios">Sanitarios</a></li>
<li><a href="https://www.acucanarias.com/courses/presenciales/Terapias-Manuales--Masaje--Osteopatia--Otros----">Terapias Manuales (Masaje, Osteopatía, Otros...)</a></li>
<li><a href="https://www.acucanarias.com/courses/presenciales/parasanitarios-terapias-naturales">Parasanitarios (Terapias Naturales)</a></li>
<li><a href="https://www.acucanarias.com/courses/presenciales/Dietetica-y-Nutricion">Dietética y Nutrición</a></li>
<li><a href="https://www.acucanarias.com/courses/presenciales/veterinaria-ganaderia-y-apicultura">Veterinaria, Ganadería y Apicultura</a></li>
<li><a href="https://www.acucanarias.com/courses/presenciales/formacion-continua">Formación Contínua</a></li>
<li><a href="https://www.acucanarias.com/courses/presenciales/Fitosanitarios">Fitosanitarios</a></li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" href="#">
                                Formación Profesional
                            </a>
<ul class="dropdown-menu">
<li><a href="https://www.acucanarias.com/courses/formacion-profesional/sanitarios">Sanitarios</a></li>
<li><a href="https://www.acucanarias.com/courses/formacion-profesional/administracion-y-gestion">Administración y gestión  </a></li>
<li><a href="https://www.acucanarias.com/courses/formacion-profesional/informatica-y-comunicaciones">Informática y comunicaciones</a></li>
<li><a href="https://www.acucanarias.com/courses/formacion-profesional/comercio-y-marketing">Comercio y marketing  </a></li>
<li><a href="https://www.acucanarias.com/courses/formacion-profesional/cocina-hosteleria-y-turismo">Cocina, hostelería y turísmo</a></li>
<li><a href="https://www.acucanarias.com/courses/formacion-profesional/servicios-socioculturales-y-a-la-comunidad">Servicios socioculturales y a la comunidad</a></li>
</ul>
</li>
<li class="dropdown mega-menu">
<a class="dropdown-toggle" href="#">
                                On Line
                            </a>
<ul class="dropdown-menu" style="left: 25%;">
<li>
<div class="row">
<div class="col-md-5th">
<ul class="list-unstyled">
<li><a href="https://www.acucanarias.com/courses/on-line/sanitarios">Sanitarios</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/administracion-y-gestion">Administración y gestión  </a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/informatica-y-comunicaciones">Informática y comunicaciones</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/comercio-y-marketing">Comercio y marketing  </a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/cocina-hosteleria-y-turismo">Cocina, hostelería y turísmo</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/servicios-socioculturales-y-a-la-comunidad">Servicios socioculturales y a la comunidad</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/actividades-fisicas-y-deportivas">Actividades físicas y deportivas</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/imagen-personal">Imagen personal</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/seguridad-y-medio-ambiente">Seguridad y medio ambiente</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/agraria">Agraria</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/artes-y-graficas">Artes y gráficas</a></li>
</ul>
</div>
<div class="col-md-5th">
<ul class="list-unstyled">
<li><a href="https://www.acucanarias.com/courses/on-line/artes-y-artesanias">Artes y artesanías</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/edificacion-y-obra-civil">Edificación y obra civil</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/electricidad-y-electronica">Electricidad y electrónica</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/energia-y-agua">Energía y agua</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/fabricacion-mecanica">Fabricación mecánica</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/imagen-y-sonido">Imagen y sonido</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/industrias-alimentarias">Industrias alimentarias</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/industrias-extractivas">Industrias extractivas</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/instalaciones-y-mantenimiento">Instalaciones y mantenimiento</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/madera-mueble-y-corcho">Madera, mueble y corcho</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/maritimo-pesquero">Marítimo pequero</a></li>
</ul>
</div>
<div class="col-md-5th">
<ul class="list-unstyled">
<li><a href="https://www.acucanarias.com/courses/on-line/quimica">Química</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/textil-confeccion-y-piel">Textil, confección y piel</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/transporte-y-mantenimiento-de-vehiculos">Transporte y mantenimiento de vehículos</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/vidrio-y-ceramica">Vidrio y cerámica</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/veterinaria-ganaderia-y-apicultura">Veterinaria, Ganadería y Apicultura</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/Fitosanitarios">Fitosanitarios</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/Idiomas">Idiomas</a></li>
<li><a href="https://www.acucanarias.com/courses/on-line/Prevencion--Seguridad-y-Primeros-Auxilios">Prevención, Seguridad y Primeros Auxilios</a></li>
</ul>
</div>
</div>
</li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" href="#">
                                Certificados de Profesionalidad
                            </a>
<ul class="dropdown-menu">
<li><a href="https://www.acucanarias.com/courses/certificados-de-profesionalidad/servicios-socioculturales-y-a-la-comunidad">Servicios socioculturales y a la comunidad</a></li>
</ul>
</li>
<li class="dropdown mega-menu">
<a class="dropdown-toggle" href="#">
                                Cualificaciones Profesionales
                            </a>
<ul class="dropdown-menu" style="left: 50%;">
<li>
<div class="row">
<div class="col-md-5th">
<ul class="list-unstyled">
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/sanitarios">Sanitarios</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/administracion-y-gestion">Administración y gestión  </a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/informatica-y-comunicaciones">Informática y comunicaciones</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/comercio-y-marketing">Comercio y marketing  </a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/cocina-hosteleria-y-turismo">Cocina, hostelería y turísmo</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/servicios-socioculturales-y-a-la-comunidad">Servicios socioculturales y a la comunidad</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/actividades-fisicas-y-deportivas">Actividades físicas y deportivas</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/imagen-personal">Imagen personal</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/seguridad-y-medio-ambiente">Seguridad y medio ambiente</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/agraria">Agraria</a></li>
</ul>
</div>
<div class="col-md-5th">
<ul class="list-unstyled">
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/artes-y-graficas">Artes y gráficas</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/artes-y-artesanias">Artes y artesanías</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/edificacion-y-obra-civil">Edificación y obra civil</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/electricidad-y-electronica">Electricidad y electrónica</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/energia-y-agua">Energía y agua</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/fabricacion-mecanica">Fabricación mecánica</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/imagen-y-sonido">Imagen y sonido</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/industrias-alimentarias">Industrias alimentarias</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/industrias-extractivas">Industrias extractivas</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/instalaciones-y-mantenimiento">Instalaciones y mantenimiento</a></li>
</ul>
</div>
<div class="col-md-5th">
<ul class="list-unstyled">
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/madera-mueble-y-corcho">Madera, mueble y corcho</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/maritimo-pesquero">Marítimo pequero</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/quimica">Química</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/textil-confeccion-y-piel">Textil, confección y piel</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/transporte-y-mantenimiento-de-vehiculos">Transporte y mantenimiento de vehículos</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/vidrio-y-ceramica">Vidrio y cerámica</a></li>
<li><a href="https://www.acucanarias.com/courses/cualificaciones-profesionales/veterinaria-ganaderia-y-apicultura">Veterinaria, Ganadería y Apicultura</a></li>
</ul>
</div>
</div>
</li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" href="#">
                                Otros
                            </a>
<ul class="dropdown-menu">
<li><a href="https://www.acucanarias.com/courses/otros/oposiciones">Oposiciones</a></li>
</ul>
</li>
<li class="search">
<a href="#">
<i class="fa fa-search"></i>
</a>
<div class="search-box">
<form accept-charset="UTF-8" action="https://www.acucanarias.com/buscar" method="POST"><input name="_token" type="hidden"/>
<div class="input-group">
<input class="form-control form-control-lg" name="text" placeholder="busca tu curso..." type="text"/>
<span class="input-group-btn">
<button class="btn button-01" type="submit">Buscar</button>
</span>
</div>
</form>
</div>
</li>
</ul>
</nav>
</div>
</div>
</div>
</div>
</header>
</div>
<section>
<div class="container-fluid">
<div class="row min-vh-50">
<div class="col-12 mt-4">
<h3 class="fs-20">La página no ha sido encontrada <small>(error 404)</small></h3>
<p>La página no ha sido encontrada. Al presionar el botón "Ir al Inicio", se le redireccionará a la página principal de ACU CANARIAS.</p>
<div class="white-text">
<a class="btn btn-success" href="https://www.acucanarias.com">Ir al Inicio</a>
</div>
</div>
</div>
</div>
</section>
<section id="family">
<div class="container-fluid">
<div class="col-12">
<h4 class="letter-spacing-1">¿Qué deseas estudiar?</h4>
</div>
<div class="row">
<div class="col-md-3">
<ul class="list-unstyled list-icons family-list">
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/sanitarios">Sanitarios</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/administracion-y-gestion">Administración y gestión  </a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/informatica-y-comunicaciones">Informática y comunicaciones</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/comercio-y-marketing">Comercio y marketing  </a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/cocina-hosteleria-y-turismo">Cocina, hostelería y turísmo</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/servicios-socioculturales-y-a-la-comunidad">Servicios socioculturales y a la comunidad</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/actividades-fisicas-y-deportivas">Actividades físicas y deportivas</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/imagen-personal">Imagen personal</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/seguridad-y-medio-ambiente">Seguridad y medio ambiente</a></li>
</ul>
</div>
<div class="col-md-3">
<ul class="list-unstyled list-icons family-list">
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/agraria">Agraria</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/artes-y-graficas">Artes y gráficas</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/artes-y-artesanias">Artes y artesanías</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/edificacion-y-obra-civil">Edificación y obra civil</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/electricidad-y-electronica">Electricidad y electrónica</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/energia-y-agua">Energía y agua</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/fabricacion-mecanica">Fabricación mecánica</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/imagen-y-sonido">Imagen y sonido</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/industrias-alimentarias">Industrias alimentarias</a></li>
</ul>
</div>
<div class="col-md-3">
<ul class="list-unstyled list-icons family-list">
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/industrias-extractivas">Industrias extractivas</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/instalaciones-y-mantenimiento">Instalaciones y mantenimiento</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/madera-mueble-y-corcho">Madera, mueble y corcho</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/maritimo-pesquero">Marítimo pequero</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/quimica">Química</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/textil-confeccion-y-piel">Textil, confección y piel</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/transporte-y-mantenimiento-de-vehiculos">Transporte y mantenimiento de vehículos</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/vidrio-y-ceramica">Vidrio y cerámica</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/oposiciones">Oposiciones</a></li>
</ul>
</div>
<div class="col-md-3">
<ul class="list-unstyled list-icons family-list">
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/formacion-continua">Formación Contínua</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/Fitosanitarios">Fitosanitarios</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/parasanitarios-terapias-naturales">Parasanitarios (Terapias Naturales)</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/Terapias-Manuales--Masaje--Osteopatia--Otros----">Terapias Manuales (Masaje, Osteopatía, Otros...)</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/veterinaria-ganaderia-y-apicultura">Veterinaria, Ganadería y Apicultura</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/Dietetica-y-Nutricion">Dietética y Nutrición</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/Idiomas">Idiomas</a></li>
<li><i class="fa fa-check"></i> <a href="https://www.acucanarias.com/courses/family/Prevencion--Seguridad-y-Primeros-Auxilios">Prevención, Seguridad y Primeros Auxilios</a></li>
</ul>
</div>
</div>
</div>
</section>
<section class="alternate" id="information_form">
<div class="container-fluid">
<h4 class="letter-spacing-1 pb-10">Solicita más infomación</h4>
<form accept-charset="UTF-8" action="https://www.acucanarias.com/information" method="POST"><input name="_token" type="hidden"/>
<div class="row">
<div class="col-md-3">
<input class="form-control" name="name" placeholder="Nombre *" type="text"/>
</div>
<div class="col-md-3">
<input class="form-control" name="email" placeholder="Email *" type="text"/>
</div>
<div class="col-md-3">
<input class="form-control" name="phone" placeholder="Móvil *" type="text"/>
</div>
<div class="col-md-3">
<select class="select2 form-control" name="family_id"><option value="1">Sanitarios</option><option value="2">Administración y gestión  </option><option value="3">Informática y comunicaciones</option><option value="4">Comercio y marketing  </option><option value="5">Cocina, hostelería y turísmo</option><option value="6">Servicios socioculturales y a la comunidad</option><option value="7">Actividades físicas y deportivas</option><option value="8">Imagen personal</option><option value="9">Seguridad y medio ambiente</option><option value="10">Agraria</option><option value="11">Artes y gráficas</option><option value="12">Artes y artesanías</option><option value="13">Edificación y obra civil</option><option value="14">Electricidad y electrónica</option><option value="15">Energía y agua</option><option value="16">Fabricación mecánica</option><option value="17">Imagen y sonido</option><option value="18">Industrias alimentarias</option><option value="19">Industrias extractivas</option><option value="20">Instalaciones y mantenimiento</option><option value="21">Madera, mueble y corcho</option><option value="22">Marítimo pequero</option><option value="23">Química</option><option value="24">Textil, confección y piel</option><option value="25">Transporte y mantenimiento de vehículos</option><option value="26">Vidrio y cerámica</option><option value="27">Oposiciones</option><option value="28">Atención sociosanitaria a personas dependientes en instituciones sociales</option><option value="29">Dinamización, programación y desarrollo de acciones culturales</option><option value="30">Docencia de la formación profesional para el empleo</option><option value="31">Mediación comunitaria</option><option value="32">Varios</option><option value="36">Terapias Manuales (Masaje, Osteopatía, Otros...)</option><option value="35">Parasanitarios (Terapias Naturales)</option><option value="38">Dietética y Nutrición</option><option value="37">Veterinaria, Ganadería y Apicultura</option><option value="33">Formación Contínua</option><option value="34">Fitosanitarios</option><option value="39">Idiomas</option><option value="40">Prevención, Seguridad y Primeros Auxilios</option></select>
</div>
</div>
<div class="row">
<div class="col-md-12">
<textarea class="form-control" cols="50" name="information" placeholder="Indícanos tus dudas *" rows="3"></textarea>
</div>
</div>
<div class="row">
<div class="col-md-12 ml-3">
<label class="checkbox">
<input class="form-control" name="aceptLegal" type="checkbox" value="1"/>
<i></i>  He leído y acepto expresamente el contenido del apartado <a class="primary-color" href="https://www.acucanarias.com/legal" target="_blank">Aviso Legal</a> de esta página web.
                    </label>
<br/>
<label class="checkbox">
<input class="form-control" name="advertising" type="checkbox" value="1"/>
<i></i>  Consiento el uso de mis datos personales para recibir publicidad de su entidad.
                    </label>
</div>
</div>
<div class="row">
<div class="col-md-5 col-sm-6">
<p>Para validar el envío del formulario de contacto, por favor, indique el número que observa en la imagen.</p>
</div>
<div class="col-md-2 col-sm-3">
<input class="form-control" name="imagenContact" type="text"/>
</div>
<div class="col-md-1 col-sm-3">
<img alt="número de seguridad" src="https://www.acucanarias.com/images/numbers/num8.png"/>
<input name="numimagen" type="hidden" value="images/numbers/num8.png"/>
</div>
</div>
<button class="btn btn-success" type="submit">Solicitar información</button>
</form>
</div>
</section>
<footer class="footer-ligth" id="footer">
<div class="container-fluid">
<div class="row mt-60 mb-40 fs-13">
<div class="col-md-3 col-xs-6">
<div class="row">
<a class="logo float-left" href="https://www.acucanarias.com">
<img alt="ACU CANARIAS Secretaria de cursos" class="img-responsive" src="https://www.acucanarias.com/images/logo-alfa.png"/>
</a>
</div>
<div class="row mt-4">
<p class="letter-spacing-1">ACU CANARIAS. Un Centro de Formación por excelencia de Canarias, con los  cursos ideales para tu futuro.</p>
<span class="h2">922 25 71 54</span>
</div>
<div class="row">
<div class="clearfix">
<a class="social-icon social-icon-border social-facebook float-left" data-placement="top" data-toggle="tooltip" href="https://www.facebook.com/ACUCANARIA?fref=ts" target="_blank" title="Facebook">
<i class="icon-facebook"></i>
<i class="icon-facebook"></i>
</a>
<a class="social-icon social-icon-border social-twitter float-left" data-placement="top" data-toggle="tooltip" href="https://twitter.com/acucanarias" target="_blank" title="Twitter">
<i class="icon-twitter"></i>
<i class="icon-twitter"></i>
</a>
<a class="social-icon social-icon-border social-instagram float-left" data-placement="top" data-toggle="tooltip" href="https://www.instagram.com/explore/locations/212693052101405/acucanarias/" target="_blank" title="Instagram">
<i class="icon-instagram"></i>
<i class="icon-instagram"></i>
</a>
</div>
</div>
</div>
<div class="col-md-4 col-xs-6 mt-20 fs-13">
<h4 class="letter-spacing-1">Novedades</h4>
<ul class="list-unstyled footer-list half-paddings">
<li>
<a class="block" href="https://www.acucanarias.com/novedad/curso_de_drenaje_linfatico_la_laguna_60_horas">Curso de drenaje Linfático. ( La Laguna) (60 horas)</a>
<small><span class="font-lato">25</span> <span>Abril</span> 2020</small>
</li>
</ul>
<ul class="list-unstyled footer-list half-paddings">
<li>
<a class="block" href="https://www.acucanarias.com/novedad/curso_de_kinesiologia_la_laguna_60_horas">CURSO DE KINESIOLOGIA (La Laguna)(60 horas)</a>
<small><span class="font-lato">25</span> <span>Abril</span> 2020</small>
</li>
</ul>
</div>
<div class="col-md-2 col-xs-6 mt-20 fs-13 center">
<h4 class="letter-spacing-1">Web</h4>
<ul class="list-unstyled footer-list half-paddings b-0">
<li><a class="block" href="https://www.acucanarias.com/quienes_somos"><i class="fa fa-angle-right"></i> ¿Quiénes somos?</a></li>
<li><a class="block" href="https://www.acucanarias.com/metodologia"><i class="fa fa-angle-right"></i> Metodología</a></li>
<li><a class="block" href="https://www.acucanarias.com/novedades"><i class="fa fa-angle-right"></i> Novedades</a></li>
<li><a class="block" href="https://www.acucanarias.com/localizacion"><i class="fa fa-angle-right"></i> Localización</a></li>
<li><a class="block" href="https://www.plataformateleformacion.com/" target="_blank"><i class="fa fa-angle-right"></i> Campus Virtual</a></li>
<li><a href="https://moodle.acucanarias.com/" target="_blank"><i class="fa fa-angle-right"></i> Cursos On-line</a></li>
<li><a class="block" href="#"><i class="fa fa-angle-right"></i> Contactar</a></li>
</ul>
</div>
<div class="col-md-3 col-xs-6 mt-20 fs-13">
<h4 class="letter-spacing-1">Sedes</h4>
<div class="street-address">Central ( La Laguna)</div>
<div class="region">Av. Leonardo Torriani nº 61 local. La Laguna</div>
<div class="postal-code">C.P. 38205 España. Santa Cruz de Tenerife</div>
<span class="fa fa-phone"></span> 922 25 71 54 <br/>
<span class="fa fa-phone"></span> 902 12 12 69 <br/>
<hr class="style1"/>
<div class="street-address">Delegación (Los Cristianos) </div>
<div class="region">Santa Cruz de Tenerife</div>
<span class="fa fa-phone"></span> 922 25 71 54<br/>
<hr class="style1"/>
<div class="street-address">Delegación (Pto. de la Cruz) </div>
<div class="region">Santa Cruz de Tenerife</div>
<span class="fa fa-phone"></span> 922 26 61 12 <br/>
<span class="fa fa-phone"></span> 902 12 12 69
            </div>
</div>
</div>
<div class="copyright">
<div class="container-fluid">
<div class="row">
<div class="col-4">
                    © 2020 - AcuCanarias. Centro de Formación Profesional
                </div>
<div class="col-4 text-center">
<ul class="float-right m-0 list-inline mobile-block">
<li><a href="/legal">Aviso Legal</a></li>
<li>•</li>
<li><a href="/privacy">Política de privacidad</a></li>
<li><a href="/cookies">Política de cookies</a></li>
</ul>
</div>
<div class="col-4 text-right">
                    Web desarrolada por: <a href="https://www.redesinformaticas.net/" target="_blank">Redes Informaticas</a>
</div>
</div>
</div>
</div>
</footer>
</div>
<a href="#" id="toTop"></a>
<div id="preloader">
<div class="inner">
<span class="loader"></span>
</div>
</div>
<script type="text/javascript"> var plugin_path = '/assets/plugins/';</script>
<script src="https://www.acucanarias.com/assets/js/toastr.min.js"></script>
<script src="https://www.acucanarias.com/assets/js/jquery-3.4.1.min.js"></script>
<script src="https://www.acucanarias.com/assets/js/scripts.js"></script>
<script src="https://www.acucanarias.com/assets/js/toastr.min.js"></script>
<script src="https://www.acucanarias.com/assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="https://www.acucanarias.com/assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="https://www.acucanarias.com/assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="https://www.acucanarias.com/assets/js/demo.revolution_slider.js"></script>
<script src="https://www.acucanarias.com/js/app.messages.js"></script>
<div class="modal fade" id="mStandardWindows" role="dialog" tabindex="999999">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h3><span>.title</span></h3>
<button aria-hidden="true" class="close pull-right" data-dismiss="modal" type="button">×</button>
</div>
<div class="modal-body">
<div class="modal-footer">
<button class="btn btn-default" data-dismiss="modal" type="button"><i class="fa fa-times-circle"></i> messages.button_close</button>
</div>
</div>
</div>
</div>
</div>
<div class="panel-cookies" id="accepCookie">
    Este sitio web utiliza cookies, tanto propias como de terceros, para recopilar información estadística sobre su navegación<br/>
    y mostrarle publicidad relacionada con sus preferencias, generada a partir de sus pautas de navegación.<br/>
    Si continúa navegando, consideramos que acepta su uso.
    <button class="button-03" onclick="assignCookie();" type="button">Aceptar</button>
<a class="text-white" href="https://www.acucanarias.com/cookies"><u>Leer mas</u>.</a>
</div>
<script>
    if (getCookie('acu') === "1") {
        document.getElementById("accepCookie").style.display="none";
    }
</script>
<script>
        $(document).ready(function() {
                                        });
        window.onload = function () {
                    };
    </script>
</body>
</html>
