<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "35100",
      cRay: "610c7abe5ec901c0",
      cHash: "3443cc52bdcf4bf",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYWxsZXRhcGV0eS5wbC8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "JxTu9+0yMifYXPyMoooJW8iPrkro8QMkpidksfDkXR5/jL10BNF7jEYrTQBXPtn0u/DYgpJE3oeL2h8KmN2q1xODHhSriDlKZoRJzfhAzwUDs2ZTRGc3ZysjqRI1WDbRaff2ydNMBYTOVwiay77IuqpwkhQl2fPBsjTl8q8kDi1P7Y7g8ZLUjgWmGsMOKhx8BaNPziIHVY2EuRc/Zcg3zbmKXefmuei2Hl1QhSKa2Wdv8z8uSePwtbpvf3DLq5lWPVPN9rjImxTUUib3zoylAaxjN/IPSafIW477SgmAFegboc8M0aK6yat7qpKmGpzIyFG28Oax8HR3gj2yj4BUuvLwg+jbrOWdcp/WrUlizmY2WpqAiUh2mj1Ts/tkMcrFrcRX3RV5hVplaelFocg0Tvi5Cl3T7+Szlt/66c0QKmuOTwNRn22nlOgUgfmSUNN+JzfBYdR6bJ3Qh9DKEV/zzBWQY+bM066Tfhfm23aqGRx57PDehxy1sNS3ant5mblNltAwBLnnwaIrHpN0fhspxvSEw3BK+IYTJh5npMpwM1loNfzrCWnXpp8WpIm3ZmPEDFryUHWTH2EFPU16B9UyDZ5tIhNRvm0/eKjZJzgi+ZRxxpUrdEVnsvsRfqxV7PryrCtcUpo4MvofZNvLLE3gptWafHIU6haPQdF3MeG+FV8ZYnmCMxTqf3u7WalJua38xHEmcE4ZqqZsJuCU/v8TfD7rx5czkKyOZEFNy7VT1oMdF51Xxzaxhu9QlayRBc0X3by99ttsEmaNDzfLJ1AuISnRnkGTCoc1XoU7wRnebEM=",
        t: "MTYxMDUxMzI0OS4wMTYwMDA=",
        m: "sUCVO3GvBAWo2nt35HWB+3U5bnX79irbigd0PCYRC54=",
        i1: "BZwfpSh2rv/5a60GlytPXg==",
        i2: "qicvdzqvfd+Ors6oAfwJ/Q==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "NuRzy8at0FYWu3nrT97+m88Cf26vr/bgbLQovWbwOp8=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610c7abe5ec901c0");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> alletapety.pl.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<!-- <a href="https://beatlemail.net/picture.php?blogid=0">table</a> -->
<form action="/?__cf_chl_jschl_tk__=323689c8c3816eafdfd4066c47d2a774116ac801-1610513249-0-AW4i5Ypx46BNDhaABf0ZI0sjIW9w4CtJ49lxUqPX_Uxhf6lI4rmu3oLzt1gRZ50fnlkREBQ8YIaxwrsDZvWfNfZhGsmj6hUONe2F7DkvU5uTFcxSnmeK2i7pTS4SsgVFp0lu4wzUo3QUSW8py6F4xkP4b53OSNTKKopxURjMsTtA_-VUH8MhyJ9uScJXp7lz4Y-k7Vzb1kNPlCisL2fjVDT610hvpg-ItvveVd6sCqZuKZRjORapp7Tx928FmyY5MKtwpi-irQl0Kq6AakiXhX7CoW0dU-wEhb_gHKLPJXY-S-ZQScQkKX5tTuFp5ffNzr1P7ALKtXHEFxoM6J-ANI0IkaMDelbHpvC-HCPPt7fTN7rm_23EO6lmmyv4rMWk2g" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="e766dff12846fd206e2599866f77a638192f1dd5-1610513249-0-AeoDDyr9mq9iSK7pVDWP6wofEZ4CVn9np8pjUhxrCQ1mKxIEhU7pRhdapuX78Tr7yokzWD/0McqURW/PRxU3fN7e6dOmpEao7LuS3Dw8g8hfev72LEXzY9Kwqd/kQqM1bTkw5leWInHJ8GJowBB62Teyve6jA+j1IdVvwE5JuubQx4yeHyYD/nm8STBABbZWocuPrUloOojh3//W1ONm+VmOCpLlGbxI5o0kOa6VSkmERv0vS0kEvbuR1nowhZhzGdElhhfsekVqbuVSk4SzhrREBFuu3smceIH1oasfJzvQ9aovs30ry6DlNFin3CG7cKiwADRKTfhRGz6CKORSMgmrePNvGMfFTwPaku6fBmWxPmJZ6Kj8fG2G6F2MLz2m4gggftqNJNeW3ssLg54N9ONKlgW765qIdfeak/eUyT+ROwtZtjwQQiJYjEONlPE935PXNBYEAiott77uvmZHxXFwYawpIR7LLQwe6+cM4UtqGSFy6WFkZhhyR8eqCgOXW4ATo3PVWaK0CtqCFW+OMv2Wqlzv9ePL+wb140Z4i9bp5KxBLc4x/NZTcrHQtEM4HgnsEDuJyRVkK0d7fzkMB/uHVI++ML4/hT5ttCMZfuufrkmq9+JW0UhSTcJog2UEhfWZ47TTAKZwB61lZwsM/TTNwDjFhhxXGsQ2GvHAVwFXObQHxpfJP0Ecw4/buqEGVhm98WEP7xxycbLhKpoewoyqyy3z3O9S7iu3SG3lwrLIqdkn9kdbPgeLSAZdJuMHulJhUygvLEn47dsATGByRx68sEEyKzdP/SKvI7v63psFjwQ/DXJkBSBfob+Evr4uyu1sjCA/syd9tpJKZI6Qyty3x85gnEwYxY7OOGiB5o/Q+YBjgNpdS+tXHZeZAVzoCKvmuedz40XXtEgYytWzThGEVgcdL2disJtNbTFmL8gTtQwVMcfneNwE1g51edducWw8/qI6uRN4Fu8E8t8Xw94utSMt+nlBwpeccl2SWBkOQbaM/NOZJyY/kIbF6qhA3uUhPgju/FJN2kMkcJ+owUWRWs4CIDfqPkejehaPhf7hA1xuZnp89rKSxyqKkpQ5Ss7Ul0CYfSEX6SHHgs0X1Mu+91k97/LEvLQvbqfMXvP4EHdve/eWnp0zlGjF9MO07CDmgmB/RO5x0uu5VoHLUNO2ddHlzDScDZTJ52QfzzAqfmomfzuoA4I0EUChs+K3vNlWXHFE8DXZ47urwFps318Vqu0xs2WXEdMKI5EREJathvbOPX32dU+x48Xyo0qrS5X45u7OR6IakvcD2+VP6rQ1bTUN0OPzQdNOhjSpglYT"/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="0d3e3ed70a7313d54904d00c435f074c"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610513253.016-nkg/LJ9vue"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610c7abe5ec901c0')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610c7abe5ec901c0</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
