<!DOCTYPE html>
<html lang="en-US" xmlns:og="http://ogp.me/ns#">
<head>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-9533848-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-9533848-1');
</script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width" name="viewport"/>
<meta content="Son of the Mask (2005 Movie) - Behind The Voice Actors" name="title"/>
<meta content="2 images of the Son of the Mask cast of characters. Photos of the Son of the Mask (Movie) voice actors." name="description"/>
<meta content="Son of the Mask, Son of the Mask voice actors, Son of the Mask cast, Son of the Mask voice cast, Son of the Mask characters, Son of the Mask voices, " name="keywords"/>
<meta content="Son of the Mask" property="og:title"/>
<meta content="movie" property="og:type"/>
<meta content="https://www.behindthevoiceactors.com/movies/Son-of-the-Mask/" property="og:url"/>
<meta content="https://www.behindthevoiceactors.com/_img/movies/thumbs/movie_1254_thumb.jpg" property="og:image"/>
<meta content="Behind The Voice Actors" property="og:site_name"/>
<meta content="2 images of the Son of the Mask cast of characters. Photos of the Son of the Mask (Movie) voice actors." property="og:description"/>
<meta content="bzyEawXDJqy0LACQhtEg4xex7pChYa18XBI2GAg6kJg" name="google-site-verification"/>
<title>Son of the Mask (2005 Movie) - Behind The Voice Actors</title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<link href="https://www.behindthevoiceactors.com/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://www.behindthevoiceactors.com/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="https://www.behindthevoiceactors.com/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="https://www.behindthevoiceactors.com/site.webmanifest" rel="manifest"/>
<link href="https://www.behindthevoiceactors.com/favicon.ico?v=3" rel="shortcut icon" type="image/x-icon"/>
<link href="https://www.behindthevoiceactors.com/_css/master1v18.css" rel="stylesheet" type="text/css"/>
<link href="https://www.behindthevoiceactors.com/_css/mobile1v18.css" rel="stylesheet" type="text/css"/><link href="https://www.behindthevoiceactors.com/movies/Son-of-the-Mask/" rel="canonical"/> <!--AdThrive Head Tag -->
<script>
	(function(w, d) {
	w.adthrive = w.adthrive || {};
	w.adthrive.cmd = w.adthrive.cmd || [];
	w.adthrive.plugin = 'adthrive-ads-1.0.41';
	w.adthrive.host = 'ads.adthrive.com';
	var s = d.createElement('script');
	s.async = true;
	s.referrerpolicy='no-referrer-when-downgrade';
	s.src = 'https://' + w.adthrive.host + '/sites/5b914efa377d57533c0af714/ads.min.js?referrer=' + w.encodeURIComponent(w.location.href);
	var n = d.getElementsByTagName('script')[0];
	n.parentNode.insertBefore(s, n);
	})(window, document);
	</script>
<!--End AdThrive Head Tag -->
</head>
<body class="layout_title">
<div class="vs-context-menu"></div>
<div class="popup popup_login">
<div class="bg_bucket_title" style="padding-top: 6px; height: 24px;">LOGIN</div>
<div class="login_content">
<form action="https://www.behindthevoiceactors.com/forums/login.php" method="post" onsubmit="md5hash(vb_login_password,vb_login_md5password,vb_login_md5password_utf)">
<script src="https://staticf.behindthevoiceactors.com/behindthevoiceactors/forums/clientscript/vbulletin_md5.js" type="text/javascript"></script>
<b>USERNAME</b>:<br/>
<input class="login_field" id="navbar_username" name="vb_login_username" onfocus="if (this.value == 'User Name') this.value = '';" style="margin-bottom: 10px;" type="text"/>
<b>PASSWORD</b>:<br/>
<input class="login_field" name="vb_login_password" type="password"/>
<a href="https://www.behindthevoiceactors.com/forums/login.php?do=lostpw" style="font-size: 10px;">Forgot password?</a>
<div style="text-align: center;">
<label for="cb_cookieuser_navbar"><input checked="checked" id="cb_cookieuser_navbar" name="cookieuser" type="checkbox" value="1"/><span style="font-size: 10px;">Remember Me?<br/></span></label><input class="button" style="margin: 5px 0 10px 0;" type="submit" value=" LOGIN "/>
<br/>
<span style="color: #777;">Don't have an account?</span> <a href="https://www.behindthevoiceactors.com/forums/register.php?s=">Join BTVA</a>
</div>
<input name="s" type="hidden" value="/"/>
<input name="do" type="hidden" value="login"/>
<input name="vb_login_md5password" type="hidden"/>
<input name="vb_login_md5password_utf" type="hidden"/>
</form> </div>
</div>
<div class="popup" id="popup_comments"></div>
<div class="popup" id="popup_shoutout_who">
<div class="bg_bucket_title bg_bucket_title_html_text">Members Who Shout This Out!<a class="jqmClose popup_close" href="javascript:;">[X]</a></div>
<div id="shoutout_who_container" style="overflow-y: scroll;"></div>
</div>
<div class="popup" id="popup_confirmed_credit">
<div class="bg_bucket_title bg_bucket_title_html_text">Confirmed Credits<a class="jqmClose popup_close" href="javascript:;">[X]</a></div>
<div style="height: 360px; overflow-y: scroll;"><img alt="" src=""/></div>
</div>
<div id="header">
<a href="javascript:void(0);" id="nav_mobile_menu_burger" onclick="menu_burger_expand('navmob')"><span>MENU</span><div></div><div></div><div></div></a>
<a href="https://www.behindthevoiceactors.com/" id="logo" title="Behind The Voice Actors"></a>
<div id="login_logout">
<div class="top_links">
<a href="javascript:;" id="search_button"></a>
<div>
<a class="login_link" href="javascript:;">LOGIN</a> | 
				<a href="https://www.behindthevoiceactors.com/forums/register.php?s=">JOIN BTVA</a> <div class="accounts_alert"></div>
</div>
</div>
<div class="clear"></div>
</div>
</div>
<a href="#top" id="back_to_top" title="Back to Top"><svg height="50" id="back_to_top_arrow" width="50">
<circle cx="25" cy="24" fill="#666666" r="20" stroke="white" stroke-width="1"></circle>
<text dominant-baseline="middle" fill="white" text-anchor="middle" x="50%" y="50%">➜</text>
</svg></a>
<div id="search_container">
<form action="https://www.behindthevoiceactors.com/search/" class="search_form" method="GET">
<select id="search_type" name="search_type">
<option value="all">All</option><option value="va">Voice Actors</option><option value="med">Titles</option><option value="cg">Characters (Reprisals)</option><option value="char">Characters (Single)</option><option value="vc">Voice Compares</option> </select>
<input id="search_keyword" maxlength="150" name="search_keyword" placeholder="Search ..." type="text" value=""/>
<input class="orange" id="search_submit" name="search_submit" type="submit" value=""/>
<!--<input id="search_sort" name="search_sort" type="hidden" value="" />-->
	 
	<div class="clear"></div>
</form></div>
<div id="container">
<div id="navmob">
<a class="navmob_top" href="https://www.behindthevoiceactors.com/voice-actors/">▪  Voice Actors</a>
<a class="navmob_top" href="https://www.behindthevoiceactors.com/characters/">▪  Characters</a>
<div class="clear"></div>
<a href="https://www.behindthevoiceactors.com/tv-shows/">▪  TV Shows</a>
<a href="https://www.behindthevoiceactors.com/movies/">▪  Movies</a>
<a href="https://www.behindthevoiceactors.com/video-games/">▪  Video Games</a>
<a href="https://www.behindthevoiceactors.com/shorts/">▪  Shorts</a>
<a href="https://www.behindthevoiceactors.com/rides-attractions/">▪  Attractions</a>
<a href="https://www.behindthevoiceactors.com/commercials/">▪  Commercials</a>
<div class="divider navmob_divider"></div>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/team-ups/">•  Team Ups</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/voice-compare/">•  Voice Compare</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/voice-directors/">•  Voice Directors</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/franchises/">•  Franchises</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/news/">•  News</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/top-listings/?type=pop_va_weekly">•  Top Listings</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/coming-soon/">•  Coming Soon</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/quotes/voice-actors/">•  VA Quotes</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/casting-call/">•  Casting Call</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/forums/">•  Forums</a>
</div>
<div class="navbar" id="nav">
<a href="javascript:void(0);" id="nav_menu_burger" onclick="menu_burger_expand('nav')"><div></div><div></div><div></div></a>
<a href="https://www.behindthevoiceactors.com/voice-actors/">Voice Actors</a>
<a href="https://www.behindthevoiceactors.com/characters/">Characters</a>
<a href="https://www.behindthevoiceactors.com/tv-shows/">TV Shows</a>
<a href="https://www.behindthevoiceactors.com/movies/">Movies</a>
<a href="https://www.behindthevoiceactors.com/video-games/">Video Games</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/shorts/">Shorts</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/rides-attractions/">Attractions</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/commercials/">Commercials</a>
</div>
<div class="navbar" id="subnav">
<a href="javascript:void(0);" id="subnav_menu_burger" onclick="menu_burger_expand('subnav')"><div></div><div></div><div></div></a>
<a href="https://www.behindthevoiceactors.com/team-ups/">Team Ups</a>
<a href="https://www.behindthevoiceactors.com/voice-compare/">Voice Compare</a>
<a href="https://www.behindthevoiceactors.com/voice-directors/">Voice Directors</a>
<a href="https://www.behindthevoiceactors.com/franchises/">Franchises</a>
<a href="https://www.behindthevoiceactors.com/news/">News</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/top-listings/?type=pop_va_weekly">Top Listings</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/coming-soon/">Coming Soon</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/quotes/voice-actors/">VA Quotes</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/casting-call/">Casting Call</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/forums/">Forums</a>
</div>
<script type="text/javascript">
	/*$(function() {
		if (document.body.scrollWidth > 660) {
			document.cookie = 'my_browser_width = desktop';
		}
		else {
			document.cookie = 'my_browser_width = mobile';
		}
	});*/
	</script>
<div id="content_full">
<div id="feature_banner_small_info" itemscope="" itemtype="https://schema.org/Movie" style="height: auto;">
<ol class="breadcrumbs_padding" id="breadcrumbs" itemscope="" itemtype="https://schema.org/BreadcrumbList"><li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem"><a href="https://www.behindthevoiceactors.com/" itemprop="item"><span itemprop="name">Home</span></a><meta content="1" itemprop="position"/>
</li><b>›</b><b class="breadcrumb_back">‹</b><li class="breadcrumb_back" itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem"><a href="https://www.behindthevoiceactors.com/movies/" itemprop="item"><span itemprop="name">Movies</span></a><meta content="2" itemprop="position"/>
</li><b>›</b><li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem"><span itemprop="name">Son of the Mask</span><meta content="3" itemprop="position"/>
</li></ol><h1 id="medium_heading" itemprop="name">Son of the Mask</h1> <div class="polaroid_img" style="float: left; margin-bottom: 3px;">
<img alt="Son of the Mask" class="img-ondemand" itemprop="image" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/movies/movie_1254.jpg" style="width: 115px;"/>
</div>
<div class="img_left_text_right" id="tmg_info_text">
<b>US Release:</b> Feb 18, 2005<br/><p>
<b>Trending:</b>
<b class="orange">1,797th</b> <a href="https://www.behindthevoiceactors.com/top-listings/?type=pop_movie_weekly">This Week</a></p>
<b>Franchise: <i><a href="https://www.behindthevoiceactors.com/franchises/Mask/">Mask</a></i></b><br/><b>Genres:</b> Comedy, Fantasy<p><b>Credit Verification:</b> <a class="confirmed_credit_image_link" href="javascript:;" rel="https://www.behindthevoiceactors.com/_img/movies/credits_1254.jpg" title="">Official Credits</a></p> </div>
</div>
<div class="tmg_banner" id="feature_banner_small">
<!--<img id="banner_image" src="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/movies/banner_1254.jpg" alt="Son of the Mask" />-->
<img alt="Son of the Mask" class="img-ondemand" id="feature_banner2_small" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/movies/banner_1254.jpg"/>
<img alt="Son of the Mask Cast" class="img-ondemand" id="feature_banner1_small" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/movies/banner_1254.jpg"/>
<img alt="Movie" class="img-ondemand" id="banner_title" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/banner_title_movie.png"/>
</div>
<div id="feature_banner_tmg_label">
<img alt="Movie" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/banner_title_movie.png"/>
</div>
<div class="clear"></div>
<ul id="content_subnav" style="margin-bottom: 10px;">
<li class="msn_on"><a href="https://www.behindthevoiceactors.com/movies/Son-of-the-Mask/">CREDITS</a></li>
<li><a href="https://www.behindthevoiceactors.com/movies/Son-of-the-Mask/#poll">POLL</a></li>
<li><a href="https://www.behindthevoiceactors.com/movies/Son-of-the-Mask/#discussion">DISCUSSION</a></li>
</ul>
</div><div id="content_wide_left">
<div class="main" id="medium_title_container_wide" style="margin-top: -10px;">
<p class="big_bold"><b><span class="orange">Characters On BTVA:</span> 2</b>
</p>
<div class="divider" style="margin: -10px 0 -8px 0;"></div>
<div id="medium_chars_view_by">
			VIEW BY:  
			<a href="https://www.behindthevoiceactors.com/movies/Son-of-the-Mask/">Voice Cast</a>  
						|  
			<a href="https://www.behindthevoiceactors.com/movies/Son-of-the-Mask/characters/" style="">Characters</a>  |  
						<a href="https://www.behindthevoiceactors.com/movies/Son-of-the-Mask/voice-cast/" style="">Voice Actors</a>  |  
			<a href="https://www.behindthevoiceactors.com/movies/Son-of-the-Mask/voice-credits/" style="">Credits By Actor</a>
</div>
<div class="divider" style="margin-top: -8px;"></div>
<div class="credit_pics_wide" id="medium_credits_container"><h1 class="medium_title">Son of the Mask Cast</h1><div class="credit_pics_container"><div class="credit_pic_float credit_pic_float_sidebyside">
<a class="credit_pic_float" href="https://www.behindthevoiceactors.com/movies/Son-of-the-Mask/Baby-Alvey-Avery/" title="Baby Alvey Avery"><object><div class="confirmed_credit" title="Confirmed Credit - Opening / Ending Credits"><a class="confirmed_credit_image" href="javascript:;" rel="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/movies/credits_1254.jpg"> </a></div></object><img alt="Baby Alvey Avery voice" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/chars/thumbs/baby-alvey-avery-son-of-the-mask-2.7_thumb.jpg"/>Baby Alvey Avery<br/> <i>voiced by</i> </a><a class="credit_pic_float" href="https://www.behindthevoiceactors.com/Mona-Marshall/" title="Mona Marshall"><img alt="Mona Marshall" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/actors/thumbs/mona-marshall-13.6_thumb.jpg"/>Mona Marshall</a><a class="other_vas orange sidebyside_more_text" href="https://www.behindthevoiceactors.com/movies/Son-of-the-Mask/Baby-Alvey-Avery/" title="Baby Alvey Avery">and 3 others</a></div><div class="credit_pic_float credit_pic_float_sidebyside">
<a class="credit_pic_float" href="https://www.behindthevoiceactors.com/movies/Son-of-the-Mask/Mask-Otis/" title="Mask Otis"><object><div class="confirmed_credit" title="Confirmed Credit - Opening / Ending Credits"><a class="confirmed_credit_image" href="javascript:;" rel="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/movies/credits_1254.jpg"> </a></div></object><img alt="Mask Otis voice" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/chars/thumbs/mask-otis-son-of-the-mask-23.6_thumb.jpg"/>Mask Otis<br/> <i>voiced by</i> </a><a class="credit_pic_float" href="https://www.behindthevoiceactors.com/Bill-Farmer/" title="Bill Farmer"><img alt="Bill Farmer" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/actors/thumbs/bill-farmer-8.43_thumb.jpg"/>Bill Farmer</a><a class="other_vas orange sidebyside_more_text" href="https://www.behindthevoiceactors.com/movies/Son-of-the-Mask/Mask-Otis/" title="Mask Otis">and 1 other</a></div></div></div> <div class="divider"></div>
<h2 class="main_content_heading">YOU MIGHT ALSO LIKE</h2><div class="bucket_flex"><div class="franchise_image_block"><a class="franchise_image" href="https://www.behindthevoiceactors.com/movies/Shrek-the-Third/" title="Shrek the Third"><img alt="Shrek the Third" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/movies/thumbs/movie_382_thumb.jpg"/></a><br/><a class="franchise_link" href="https://www.behindthevoiceactors.com/movies/Shrek-the-Third/" title="Shrek the Third">Shrek the Third</a></div><div class="franchise_image_block"><a class="franchise_image" href="https://www.behindthevoiceactors.com/movies/Fate-stay-night-Heavens-Feel-III-spring-song/" title="Fate/stay night: Heaven's Feel III. spring song"><img alt="Fate/stay night: Heaven's Feel III. spring song" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/movies/thumbs/movie_fate-stay-night-heavens-feel-iii-spring-song-97.7_thumb.jpg"/></a><br/><a class="franchise_link" href="https://www.behindthevoiceactors.com/movies/Fate-stay-night-Heavens-Feel-III-spring-song/" title="Fate/stay night: Heaven's Feel III. spring song">Fate/stay night: Heaven's Feel III. spring song</a></div><div class="franchise_image_block"><a class="franchise_image" href="https://www.behindthevoiceactors.com/movies/Yellow-Submarine/" title="Yellow Submarine"><img alt="Yellow Submarine" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/movies/thumbs/movie_414_thumb.jpg"/></a><br/><a class="franchise_link" href="https://www.behindthevoiceactors.com/movies/Yellow-Submarine/" title="Yellow Submarine">Yellow Submarine</a></div><div class="franchise_image_block"><a class="franchise_image" href="https://www.behindthevoiceactors.com/movies/The-Fairly-OddParents-Abra-Catastrophe/" title="The Fairly OddParents: Abra Catastrophe!"><img alt="The Fairly OddParents: Abra Catastrophe!" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/movies/thumbs/movie_1172_thumb.jpg"/></a><br/><a class="franchise_link" href="https://www.behindthevoiceactors.com/movies/The-Fairly-OddParents-Abra-Catastrophe/" title="The Fairly OddParents: Abra Catastrophe!">The Fairly OddParents: Abra Catastrophe!</a></div><div class="franchise_image_block"><a class="franchise_image" href="https://www.behindthevoiceactors.com/movies/Hotel-Transylvania-3-Summer-Vacation/" title="Hotel Transylvania 3: Summer Vacation"><img alt="Hotel Transylvania 3: Summer Vacation" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/movies/thumbs/movie_2425_thumb.jpg"/></a><br/><a class="franchise_link" href="https://www.behindthevoiceactors.com/movies/Hotel-Transylvania-3-Summer-Vacation/" title="Hotel Transylvania 3: Summer Vacation">Hotel Transylvania 3: Summer Vacation</a></div><div class="franchise_image_block"><a class="franchise_image" href="https://www.behindthevoiceactors.com/movies/Munchie/" title="Munchie"><img alt="Munchie" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/movies/thumbs/movie_1507_thumb.jpg"/></a><br/><a class="franchise_link" href="https://www.behindthevoiceactors.com/movies/Munchie/" title="Munchie">Munchie</a></div><div class="divider" style="margin-bottom: -4px;"></div></div><div class="divider"></div><a name="discussion"></a><script src="https://statica.behindthevoiceactors.com/behindthevoiceactors/_js/jquery.js" type="text/javascript"></script><script type="text/javascript">
var hash = '';

$(document).ready(function() {
	var comment_note = $('#comment_note').val();
	var default_comment_position = 0;
	
	if (window.location.href.indexOf("#comment") > -1) {
		default_comment_position = -1;
		hash = window.location.hash.substr(1);
	}
	$('#add_new_reply').hide();
	
	$('#comments_load').click(function() {
		$(this).remove();
		$('#add_new_reply').show();
		render_the_comments(0);
		//render_the_comments(default_comment_position);
		render_reply_box($('#reply_to'), 0, 0, '','first_load',comment_note);
	});
    
     $('#add_new_reply').click(function() {
		var comment_note = $('#comment_note').val();
        $('.comments_reply_container').html('').hide();
        render_reply_box($('#reply_to'), 0, 0, '', '',comment_note);
    });
});
function render_reply_box(comments_reply_container, parent_id, reply_id, username, first_load, comment_note) {
    $.post('https://www.behindthevoiceactors.com/comments_reply_box.php', {
        parent_id : parent_id, reply_id : reply_id, username : username, comment_note : comment_note, id_sub : 'submit_comment', id_comment : 'comment', id_parent_comment_id : 'parent_comment_id', id_reply_user_id : 'reply_user_id'
    }, function(response) {
        var returnVal = jQuery.trim(response);
        comments_reply_container.html(returnVal).show();
		
		if (first_load != '') {
			$('.post_comment_login_link').click(function() {
				$('.popup_login').jqm({overlay:70,toTop:true}).jqmShow();
			});
			$('.post_comment_validate_link').click(function() {
				$('.popup_validate').jqm({overlay:70,toTop:true}).jqmShow();
			});
		}			
		
		$('#submit_comment').unbind('click').bind('click', function() {
			var button = $(this);
			button.hide().next().show();
			var comment = $('#comment').val();
			var parent_comment_id = $('#parent_comment_id').val();
			var reply_user_id = $('#reply_user_id').val();
			var medium = $('#medium').val();
			var medium_id = $('#medium_id').val();
			
			if (comment != '') {
				if (comment.length < 2000) {
					$.post('https://www.behindthevoiceactors.com/add_comment.php', {
						comment : comment
						, parent_comment_id : parent_comment_id
						, reply_user_id : reply_user_id
						, medium : medium
						, medium_id : medium_id
					}, function(response){
						var returnVal = jQuery.trim(unescape(response));
						if (returnVal != '') {
							var member = $('#my_username').html();
							var member_friendly = $('#my_username').attr('href');
							var avatar = $('#my_avatar img').attr('src');
							vals = returnVal.split('|',2);
							var new_id = vals[0];
							var date = vals[1];
							var css = (parent_comment_id == 0) ? 'comments_container' : 'comments_child_container';
							var comment_formatted = nl2br(comment,true);
							
							var html = $('<div class="'+css+'"><a name="comment'+new_id+'" href="https://www.behindthevoiceactors.com/members/'+member_friendly+'/" class="avatar_thumb"><img src="'+avatar+'" alt="'+member+'"></a><b class="orange"><a href="https://www.behindthevoiceactors.com/members/'+member_friendly+'/">'+member+'</a></b><span class="comments_date"><br />said at '+date+'</span><div class="comments_description comments_description_child">'+comment_formatted+'</div><a rel="'+new_id+'|'+parent_comment_id+'|'+member+'" href="javascript:;" class="reply reply_link reply_child">Reply</a></div></div>').fadeIn('slow');
							$('#reply_to_'+parent_id).html('').hide();
							if (parent_comment_id == 0)
								$('#reply_to').after(html);
							else
								$('#reply_to_'+parent_id).parent().append(html);
							render_reply_box($('#reply_to'), 0, '', '','');
						}
						else
							window.location.reload();
					});
				}
				else {
					alert('Please keep your comments less than 2000 characters.');
					button.show().next().hide();
				}
			}
			else {
				$('#error_fill_in').show();
				button.show().next().hide();
			}
		});
		$('.cancel_reply_button').unbind('click').bind('click', function() {
			var comment_note = $('#comment_note').val();
			$('.comments_reply_container').html('').hide();
			render_reply_box($('#reply_to'), 0, '', '',comment_note);
		});
    });
}
function render_the_comments(current_position) {
    $.post('https://www.behindthevoiceactors.com/comments_get.php', {
        comments_medium : 'mediums', comments_medium_id_label : 'MediumID', comments_medium_id : '5576', current_position : current_position
    }, function(response) {
        var returnVal = jQuery.trim(response);
        $('#comments_render_container').append(returnVal).show();
		
		$('.reply_link').unbind('click').bind('click', function() {
			var rel = $(this).attr('rel');
			rel = rel.split('|',4);
			var comment_id = rel[0];
			var parent_id = rel[1];
			var reply_id = rel[2];
			var username = '@'+rel[3]+' ';
			$('.comments_reply_container').html('').hide();
			render_reply_box($('#reply_to_'+parent_id), parent_id, reply_id, username, '', '');
		});
		
		$('.read_full_comment').unbind('click').bind('click', function() {
			$(this).hide().next().slideToggle();
		});
		
		$('.shoutout,.unshoutout').unbind('click').bind('click', function() {
			var link = $(this);
			link.hide();
			var rel = link.attr('rel');
			rel = rel.split('|',4);
			action = rel[3];
			
			$.post('https://www.behindthevoiceactors.com/_widgets/do_shoutout.php', {
				post1 : rel[0], post2 : rel[1], post3 : rel[2], post4 : action
			}, function(response) {
				var return_val = $.trim(response);
				if (return_val == 'SUCCESS') {
					if (action == 'shoutout') {
						link.after('<span style="font-size: 10px;">You Shouted This Out!</span>').remove();
					}
					else if (action = 'unshoutout') {
						link.remove();
					}
					// update total number of ShoutOuts
				}
			});
		});
		
		
		$('.shoutout_who').unbind('click').bind('click', function() {
			var link = $(this);
			var rel = link.attr('rel');
			rel = rel.split('|',3);
			if (get_shoutout_who != null)
				get_shoutout_who.abort();
			
			get_shoutout_who = $.post('https://www.behindthevoiceactors.com/_widgets/shoutout_who.php', {
				post1 : rel[0], post2 : rel[1], post3 : rel[2]
			}, function(response) {
				var return_val = $.trim(response);
				if (return_val != 'fail') {
					$('#shoutout_who_container').html(return_val);
					$('#popup_shoutout_who').jqm({overlay:70,toTop:true}).jqmShow();
				}
			});
		});
		
		// jump to hash
		setTimeout(function() {
			if (hash != '') {
				window.location.hash = '#'+hash;
				location.href = window.location.href;
			}
		}, 1000);
		
		$('#comments_load_more').click(function() {
			$(this).remove();
			render_the_comments(11);
		});
	});
}
function nl2br(str,is_xhtml) {   
	var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
	return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}
</script>
<br/>
<a name="comments"></a><h2 id="comments_heading">Comments</h2>
<input id="comment_note" type="hidden" value="show"/>
<input id="medium" name="medium" type="hidden" value="mediums"/>
<input id="medium_id" name="medium_id" type="hidden" value="5576"/>
<h3><a href="javascript:;" id="add_new_reply" name="add_comment">Add a Comment</a></h3>
<div class="comments_reply_container" id="reply_to"></div>
<div id="comments_render_container">
<a href="javascript:;" id="comments_load" style="display: block; text-align: center; font-size: 16px; font-weight: bold; background: #1d8abd; color: #FFF; padding: 8px 5px; border: 2px solid #666; margin: 20px 0 10px 0;">SHOW COMMENTS (1)</a>
</div> </div>
</div>
<div id="content_home_right">
<div style="padding: 10px 0 22px 10px;"><div class="popup popup_reason">
<div class="bg_bucket_title" style="padding-top: 3px; height: 22px; font-size: 13px; background-position: 0 -5px;">Why Is This One Of Your Favorites?</div>
<div style="padding: 5px;">
<div style="color: #777; font-size: 10px; margin-bottom: 8px; line-height: 11px;">
			- Share your reason with the rest of the community.<br/>
</div>
<b>My Reason:</b>
<textarea cols="40" id="fave_reason" rows="2"></textarea><br/>
<span id="fave_reason_counter">Characters left: 140</span>
<input id="just_faved_id" type="hidden" value=""/>
<input id="faved_section" type="hidden" value="mediums"/>
<input id="faved_section_id" type="hidden" value="5576"/>
<center><input class="button" id="share_reason" style="height: 25px; font-size: 15px; margin: 8px 0 3px 0; padding: 0 5px 3px;" type="button" value="SHARE"/><br/>
<a class="jqmClose" href="javascript:;" style="font-size: 10px;">I Don't Have A Reason / I'll Add One Later</a></center>
</div>
</div>
<div class="orange" id="fave_container"> <a class="login_link" href="javascript:;" title="Add To Favorites"><img alt="Add To Favorites" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/icons/add_fave.png"/></a>Faved by <a href="javascript:;" id="all_fave_members_both"><b>3 BTVA Members</b></a> <div class="popup" id="popup_members_both_favorited">
<div class="bg_bucket_title" style="padding-top: 3px; height: 22px; font-size: 13px; background-position: 0 -5px;">Members Who Favorited This<a class="jqmClose popup_close" href="javascript:;">[X]</a></div>
<div class="members_favorited_list">
<div class="clear"></div>
</div>
</div>
</div><a class="a2a_dd" href="https://www.addtoany.com/share_save?linkname=&amp;linkurl=https%3A%2F%2Fwww.behindthevoiceactors.com%2Fmovies%2FSon-of-the-Mask%2F/"><img alt="Share/Save/Bookmark" src="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/share_button.png"/></a>
<script async="async" type="text/javascript">
	a2a_linkname=document.title;
	a2a_linkurl="https://www.behindthevoiceactors.com/movies/Son-of-the-Mask//";
</script>
<script async="async" src="https://static.addtoany.com/menu/page.js" type="text/javascript"></script></div><a href="https://www.voicereact.com/?ref=bant" onclick="gtag('event', 'click', {'event_category' : 'BannerClick3', 'event_label' : 'Voice-React'});" target="_blank" title="Play VoiceReact - a voice acting and improv game"><img alt="Play VoiceReact - a voice acting and improv game" height="210" src="https://www.behindthevoiceactors.com/_img/voicereact_banner3.jpg" width="340"/></a>
<a name="poll"></a><script type="text/javascript">
var vote_table_td_width = 0;
$(function() {
	$('#vote_now').click(function() {
		if ($('#fave_choice').val() != '') {
			$(this).hide();
			var type_id = $('#type_id').val();
			var type = $('#type').val();
			var fave_choice = $('#fave_choice').val();
			var has_voted = $('#has_voted').val();
			$.post('https://www.behindthevoiceactors.com/poll_fave_vote.php', {
				type_id : type_id, type : type, voted : fave_choice, voted_already_id : has_voted
			}, function(response){
				var returnVal = jQuery.trim(unescape(response));
				if (returnVal == 'SUCCESS') {
					window.location = 'https://www.behindthevoiceactors.com/movies/Son-of-the-Mask/';
				}
			});			
		}
	});
	$('#show_all_vote_results').click(function(){
		if (vote_table_td_width == 0) {
			vote_table_td_width = $('#top10_vote_results td:first-child').width();
			var vote_table_td_width2 = $('#top10_vote_results td:nth-child(2)').width();
			$('#all_vote_results td:first-child').width(vote_table_td_width);
			$('#all_vote_results td:nth-child(2)').width(vote_table_td_width2);
		}
		$('#all_vote_results').slideToggle();
		if ($('#show_all_vote_results').text() == 'Show all results')
			$('#show_all_vote_results').text('Hide results');
		else
			$('#show_all_vote_results').text('Show all results');
	});
});
</script>
<div class="bucket poll_fave_wide_container" id="poll_fave_thin_container">
<div class="bg_bucket_title">Favorite Character</div>
<div>
<span style="font-size: 11px; line-height: 13px;">Who's your favorite character?</span><form method="post" name="poll"><select class="fave_choice_thin" id="fave_choice" name="fave_choice"><option value=""> - Choose a Character - </option><option value="Baby Alvey Avery">Baby Alvey Avery</option><option value="Mask Otis">Mask Otis</option></select><div style="text-align: center; padding-top: 5px; padding-bottom: 2px;"><input class="button login_link" type="button" value="Vote Now"/></div><input id="type_id" type="hidden" value="5576"/><input id="type" type="hidden" value="m"/><input id="has_voted" type="hidden" value=""/></form><div class="poll_result_block"><table align="center" cellpadding="0" cellspacing="0" class="poll_results" id="top10_vote_results"><tr>
<td class="poll_label poll_label_name">Baby Alvey Avery</td>
<td class="poll_label" style="width: 80px;"><div class="poll_bar poll_bar_winner" style="width: 80px;"></div></td>
<td class="poll_percent">50.0% <span class="poll_votes">(1 vote)</span></td>
</tr><tr>
<td class="poll_label poll_label_name">Mask Otis</td>
<td class="poll_label" style="width: 80px;"><div class="poll_bar poll_bar_winner" style="width: 80px;"></div></td>
<td class="poll_percent">50.0% <span class="poll_votes">(1 vote)</span></td>
</tr></table><div style="text-align: center; padding-top: 5px;"><b>2</b> Total Votes</div></div> </div>
<div class="clear"></div>
</div><div class="bucket franchise_bucket"><div class="bg_bucket_title">FRANCHISE RELATED</div><div class="franchise_bucket_content"><span class="bucket_subtitle">Shows: </span><a class="franchise_link" href="https://www.behindthevoiceactors.com/tv-shows/Mask-The-Animated-Series/" title="The Mask: The Animated Series">The Mask: The Animated Series</a>.<div class="divider" style="margin: -4px 0 -6px 0;"></div></div></div><script type="text/javascript">
$(function() {
	if ($('.franchise_bucket_content').html() == '')
		$('.franchise_bucket').hide();
});
</script><div class="bucket">
<div class="bg_bucket_title">TRENDING THIS WEEK</div>
<div style="padding: 7px 0 5px 10px;">
<div class="name_list orange" style="margin-top: -5px;">
<div class="most_popular_week"><a class="most_popular_no1 polaroid_img" href="https://www.behindthevoiceactors.com/movies/Venom/" style="margin-right: 0;"><img alt="Venom" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/movies/thumbs/movie_2477_thumb.jpg"/></a><div class="most_popular_list_container"><ol class="most_popular_list"><li><a href="https://www.behindthevoiceactors.com/movies/Venom/">Venom</a></li><li><a href="https://www.behindthevoiceactors.com/movies/Encanto/">Encanto</a></li><li><a href="https://www.behindthevoiceactors.com/movies/Soul/">Soul</a></li><li><a href="https://www.behindthevoiceactors.com/movies/Justice-Society-World-War-II/">Justice Society: World War II</a></li><li><a href="https://www.behindthevoiceactors.com/movies/The-Clockwork-Girl/">The Clockwork Girl</a></li></ol><ol class="most_popular_list" start="6"><li><a href="https://www.behindthevoiceactors.com/movies/Konosuba-Gods-Blessing-on-this-Wonderful-World-Legend-of-Crimson/">Konosuba: God's Blessing on this Wonderful World! Legend of Crimson</a></li><li><a href="https://www.behindthevoiceactors.com/movies/The-Night-is-Short-Walk-on-Girl/">The Night is Short, Walk on Girl</a></li><li><a href="https://www.behindthevoiceactors.com/movies/Batman-Soul-of-the-Dragon/">Batman: Soul of the Dragon</a></li><li><a href="https://www.behindthevoiceactors.com/movies/My-Hero-Academia-Heroes-Rising/">My Hero Academia: Heroes Rising</a></li><li><a href="https://www.behindthevoiceactors.com/movies/A-Whisker-Away/">A Whisker Away</a></li></ol></div><div class="divider"></div><a class="orange check_out_more" href="https://www.behindthevoiceactors.com/top-listings/?type=pop_movie_weekly">CHECK OUT MORE »</a><div class="clear"></div></div> </div>
</div>
</div></div>
<div class="clear"></div>
<div id="footer">
<a href="https://www.behindthevoiceactors.com/about/">About</a>
		 | 
		<a href="https://www.behindthevoiceactors.com/contact/">Contact</a>
		 | 
		<a href="https://www.behindthevoiceactors.com/forums/">Forums</a>
		 | 
		<a href="https://www.behindthevoiceactors.com/faq/">FAQ</a>
		 | 
		<a href="https://www.behindthevoiceactors.com/terms-of-use/">Terms Of Use</a>
		 | 
		<a href="https://www.behindthevoiceactors.com/privacy-policy/">Privacy Policy</a>
<br/>
<br/>
<span>This is an unofficial site. All logos, images, video and audio clips pertaining to actors, characters and related indicia belong to their respective © and ™ owners.<br/>All original content © 2009-2021 Inyxception Enterprises, Inc. DBA Behind The Voice Actors. All Rights Reserved.</span>
</div>
</div>
<script src="https://statica.behindthevoiceactors.com/behindthevoiceactors/_js/jquery.jqModal2.js" type="text/javascript"></script>
<script src="https://statica.behindthevoiceactors.com/behindthevoiceactors/_js/jquery.dimensions.js" type="text/javascript"></script>
<script src="https://statica.behindthevoiceactors.com/behindthevoiceactors/_js/header_script27.js" type="text/javascript"></script>
<script defer="" src="https://dstik9906m659.cloudfront.net/eVIfRVUUblIfRlskRCEyUu.js"></script></body>
</html>