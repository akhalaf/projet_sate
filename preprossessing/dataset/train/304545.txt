<!DOCTYPE html>
<html class="no-js" dir="ltr" lang="en-gb">
<head>
<base href="https://www.cerviacasa.it/cache/ajaoi/mail.htm/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title></title>
<link href="/templates/tx_zenith/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/components/com_estateagent/templates/shadowbox/shadowbox.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/components/com_estateagent/assets/estateagent.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/libraries/expose/interface/css/joomla.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/templates/tx_zenith/css/typography.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/templates/tx_zenith/css/template.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/templates/tx_zenith/css/responsive.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/templates/tx_zenith/css/k2.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/templates/tx_zenith/css/styles/blue.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.cerviacasa.it/modules/mod_ea_search/mod_ea_search.css" rel="stylesheet" type="text/css"/>
<link href="/modules/mod_cookiesaccept/screen.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
</style>
<script src="/libraries/expose/interface/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="/libraries/expose/interface/js/equalheight.js" type="text/javascript"></script>
<script src="/libraries/expose/interface/js/jquery.lazyload.js" type="text/javascript"></script>
<script src="/libraries/expose/interface/js/breakpoints.js" type="text/javascript"></script>
<script src="/libraries/expose/interface/js/jquery.lettering.js" type="text/javascript"></script>
<script src="/libraries/expose/interface/js/offcanvas.js" type="text/javascript"></script>
<script src="/templates/tx_zenith/js/template.js" type="text/javascript"></script>
<script type="text/javascript">

		jQuery.noConflict();

		jQuery(document).ready(function($){
			jQuery('#roof .column').equalHeight('.block');jQuery('#header .column').equalHeight('.block');jQuery('#top .column').equalHeight('.block');jQuery('#utility .column').equalHeight('.block');jQuery('#feature .column').equalHeight('.block');jQuery('#main-top .column').equalHeight('.block');jQuery('#content-top .column').equalHeight('.block');jQuery('#content-bottom .column').equalHeight('.block');jQuery('#main-bottom .column').equalHeight('.block');jQuery('#bottom .column').equalHeight('.block');jQuery('#footer .column').equalHeight('.block');jQuery('#mainbody, #sidebar-a, #sidebar-b').equalHeight();
			jQuery('img').lazyload({effect: "fadeIn",threshold : 100});
			jQuery('header h2 a').lettering('words');

		});
  </script>
<link href="/en/rss" rel="alternate" title="EA Improved RSS Feed" type="application/rss+xml"/>
<link href="/templates/tx_zenith/images/apple_touch_icon.png" rel="apple-touch-icon-precomposed"/>
<!--[if (gte IE 6) & (lte IE 8)]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <script src="/libraries/expose/interface/js/respond.js"></script>
            <script src="/libraries/expose/interface/js/selectivizr.js"></script>
        <![endif]-->
</head>
<body class="blue align-ltr page-id-101 homepage unknown home">
<div id="warning-message"><br/><br/>
</div>
<div class="container">
<header id="header-wrap">
<!--Start Top Modules-->
<section class="row" id="top">
<div class="grid12 column first last ex-odd top-1"><div class="block widget widget-logo no-title clearfix "><div class="content"><a class="menu-toggle visible-phone" data-uk-offcanvas="" href="#offcanvas"></a><p class="brand text" id="logo"> <a href="/">CerviaCasa <span class="logo-tagline">Affitti turistici da privato a privato</span></a> </p></div></div></div> </section>
<!--End Top Modules-->
<!--Start Header Modules-->
<section class="row" id="header">
<div class="grid12 column first last ex-odd header-1"><div class="block widget widget-menu no-title clearfix "><div class="content">
<nav class="ex-menu hidden-phone" dropdown-animation="fade" dropdown-sub-animation="slide-right">
</nav> <!-- menu end -->
</div></div></div> </section>
<!--End Header Modules-->
</header>
<!--Start Main Body-->
<section class="row" id="main">
<div id="system-message-container">
</div>
<section class="grid9 offset3 clearfix" id="mainbody" role="main">
<section class="clearfix" id="component" role="article">
<div class="block">
<!-- ======================================================= -->
<!-- Estate Agent Improved - Version 2.5.4 -->
<!-- EA Improved team   -->
<!-- for more information visit http://www.www.eaimproved.eu -->
<!--===============Released under GPL======================= -->
<div class="contentheading">Categories</div><div class="ea_categories_border" id="equalize"><div class="ea_category_box" style="width:98%"><div class="ea_category_box_title"><img alt="Arrow" src="/media/com_estateagent/gui/arrow.png"/> Giugno (0)</div><div class="ea_category_box_content"><div class="ea_category_img"><a href="/en/category/5/giugno"><img alt="" src="/media/com_estateagent/categories/no_image.png"/></a></div><div class="ea_categories_description"></div><div class="ea_clearboth"></div></div></div><div class="ea_category_box" style="width:98%"><div class="ea_category_box_title"><img alt="Arrow" src="/media/com_estateagent/gui/arrow.png"/> <a href="/en/category/6/maggio">Maggio</a> (12)</div><div class="ea_category_box_content"><div class="ea_category_img"><a href="/en/category/6/maggio"><img alt="" src="/media/com_estateagent/categories/no_image.png"/></a></div><div class="ea_categories_description"></div><div class="ea_clearboth"></div></div></div><div class="ea_category_box" style="width:98%"><div class="ea_category_box_title"><img alt="Arrow" src="/media/com_estateagent/gui/arrow.png"/> Luglio (0)</div><div class="ea_category_box_content"><div class="ea_category_img"><a href="/en/category/4/luglio"><img alt="" src="/media/com_estateagent/categories/no_image.png"/></a></div><div class="ea_categories_description"></div><div class="ea_clearboth"></div></div></div><div class="ea_category_box" style="width:98%"><div class="ea_category_box_title"><img alt="Arrow" src="/media/com_estateagent/gui/arrow.png"/> Agosto (0)</div><div class="ea_category_box_content"><div class="ea_category_img"><a href="/en/category/3/agosto"><img alt="" src="/media/com_estateagent/categories/no_image.png"/></a></div><div class="ea_categories_description"></div><div class="ea_clearboth"></div></div></div><div class="ea_category_box" style="width:98%"><div class="ea_category_box_title"><img alt="Arrow" src="/media/com_estateagent/gui/arrow.png"/> <a href="/en/category/2/settembre">Settembre</a> (11)</div><div class="ea_category_box_content"><div class="ea_category_img"><a href="/en/category/2/settembre"><img alt="" src="/media/com_estateagent/categories/no_image.png"/></a></div><div class="ea_categories_description"></div><div class="ea_clearboth"></div></div></div><div class="ea_category_box" style="width:98%"><div class="ea_category_box_title"><img alt="Arrow" src="/media/com_estateagent/gui/arrow.png"/> Stagionale (0)</div><div class="ea_category_box_content"><div class="ea_category_img"><a href="/en/category/1/stagionale"><img alt="" src="/media/com_estateagent/categories/no_image.png"/></a></div><div class="ea_categories_description"></div><div class="ea_clearboth"></div></div></div></div><div class="ea_clearboth "></div><div class="ea_rss"><a href="/en/rss" target="_blank"><img alt="RSS Feed" border="0" src="/media/com_estateagent/gui/rss.png" title="RSS Feed"/></a></div>
</div>
</section>
<!--Start Content Bottom Modules-->
<section class="clearfix" id="content-bottom">
<div class="grid9 column first last ex-odd contentbottom-1"><div class="block module mod-97 no-title clearfix">
<div class="header">
<h2 class="title"><span>percorso</span></h2>
</div>
<div class="content">
<ul class="breadcrumb">
<li><span>Home</span></li></ul>
</div>
</div></div> </section>
<!--End Content Bottom Modules-->
</section>
<!--Start Sidebar-A Modules-->
<aside class="grid3 inset12 clearfix" id="sidebar-a" role="complementary">
<div class="grid3 column first last ex-odd multi-module-column sidebar-a"><div class="block module box alert title1 mod-89 clearfix">
<div class="header">
<h2 class="title"><span>Cerca</span> veloce</h2>
</div>
<div class="content">
<fieldset><form action="/en/result" method="post" name="easearch_mod_89"><div class="ea_vert_border"><label class="easm_label" for="src_country_89">Country</label><select class="inputbox" id="src_country_89" name="src_country" style="width:180px ">
<option value="no">::Not Selected::</option>
<option value="IT">Cervia - Italia</option>
</select>
</div><div class="ea_vert_border"><label class="easm_label" for="src_district_89">District</label><select class="inputbox" id="src_district_89" name="src_district" style="width:180px ">
<option value="no">::Not Selected::</option>
<option value="Ravenna">Ravenna</option>
</select>
</div><div class="ea_vert_border"><label class="easm_label" for="src_cat_89">Select Category</label><select class="inputbox" id="src_cat_89" name="src_cat" style="width:180px ">
<option value="">::Any Category::</option>
<option value="5">Giugno</option>
<option value="6">Maggio</option>
<option value="4">Luglio</option>
<option value="3">Agosto</option>
<option value="2">Settembre</option>
<option value="1">Stagionale</option>
</select>
</div><div class="ea_vert_border"><label class="easm_label" for="type_89"> Select Type</label><select class="inputbox" id="type_89" name="type" style="width:180px ">
<option selected="selected" value="0">Choose Type</option>
<option value="1">Bilocale 3-4 posti letto</option>
<option value="2">Bilocale 5-6 posti letto</option>
<option value="4">Tilocale 6-7 posti letto</option>
<option value="5">Tilocale 8+ posti letto</option>
</select>
</div><div class="ea_vert_border"><label class="easm_label">Price (Min-Max)</label><br/><select class="inputbox ea_minmax_input" id="minvalue_89" name="minvalue">
<option selected="selected" value="">::Any::</option>
<option value="0"> € 0</option>
<option value="50000"> € 50.000</option>
<option value="100000"> € 100.000</option>
<option value="150000"> € 150.000</option>
<option value="200000"> € 200.000</option>
<option value="250000"> € 250.000</option>
<option value="300000"> € 300.000</option>
<option value="350000"> € 350.000</option>
<option value="400000"> € 400.000</option>
<option value="450000"> € 450.000</option>
<option value="500000"> € 500.000</option>
</select>
 - <select class="inputbox ea_minmax_input" id="maxvalue_89" name="maxvalue">
<option selected="selected" value="">::Any::</option>
<option value="0"> € 0</option>
<option value="50000"> € 50.000</option>
<option value="100000"> € 100.000</option>
<option value="150000"> € 150.000</option>
<option value="200000"> € 200.000</option>
<option value="250000"> € 250.000</option>
<option value="300000"> € 300.000</option>
<option value="350000"> € 350.000</option>
<option value="400000"> € 400.000</option>
<option value="450000"> € 450.000</option>
<option value="500000"> € 500.000</option>
</select>
</div><div class="ea_vert_border"><label class="easm_label">Enter keyword or ID</label><input class="inputbox ea_textboxwidth" name="searchstring" type="text"/></div><div class="ea_vert_border_button"><input class="button" type="submit" value="Start Search"/><br/><a href="/en/search">Advance Search</a></div><input name="isnewsearch" type="hidden" value="1"/></form></fieldset>
</div>
</div><div class="block module mod-100 no-title clearfix">
<div class="header">
<h2 class="title"><span>CookiesAccept</span></h2>
</div>
<div class="content">
<!--googleoff: all-->
<div id="ca_banner" style="bottom:0px;
            ">
<h2 style="
		">NOTE! This site uses cookies and similar technologies.</h2>
<p style="
				">If you not change browser settings, you agree to it.        							<span class="infoplus" style=""><a href="/">Learn more</a></span>
</p>
<div class="accept" style="">I understand</div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () { 
	
	function setCookie(c_name,value,exdays)
	{
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString()) + "; path=/";
		document.cookie=c_name + "=" + c_value;
	}
	
	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
			}
		return null;
	}
    
	var $ca_banner = jQuery('#ca_banner');
    var $ca_infoplus = jQuery('.infoplus.info_modal');
    var $ca_info = jQuery('#ca_info');
    var $ca_info_close = jQuery('.ca_info_close');
    var $ca_infoaccept = jQuery('.accept');
    
	var cookieaccept = readCookie('cookieaccept');
	if(!(cookieaccept == "yes")){
	
		$ca_banner.delay(1000).slideDown('fast'); 
        $ca_infoplus.click(function(){
            $ca_info.fadeIn("fast");
        });
        $ca_info_close.click(function(){
            $ca_info.fadeOut("slow");
        });
        $ca_infoaccept.click(function(){
			setCookie("cookieaccept","yes",365);
            jQuery.post('https://www.cerviacasa.it/cache/ajaoi/mail.htm/', 'set_cookie=1', function(){});
            $ca_banner.slideUp('slow');
            $ca_info.fadeOut("slow");
        });
       } 
    });
</script>
<!--googleon: all-->
</div>
</div></div> </aside>
<!--End Sidebar-A Modules-->
</section>
<!--End Main Body Modules-->
<footer id="footer-wrap">
<!--Start Bottom Modules-->
<section class="row" id="bottom">
<div class="grid12 column first last ex-odd bottom-6"><div class="block module box1 title1 mod-94 no-title clearfix">
<div class="header">
<h2 class="title"><span>BOTTOM</span> 6</h2>
</div>
<div class="content">
<div class="custom">
<center><img border="0" src="/templates/tx_zenith/images/logo.png"/></center></div>
</div>
</div></div> </section>
<!--End Bottom Modules-->
<section class="row" id="copyright">
<div class="grid12 column first last ex-odd copyright-1"><div class="block widget widget-designedby no-title clearfix "><div class="content">
</div></div></div> </section>
</footer>
</div>
<div class="uk-offcanvas" id="offcanvas">
<div class="uk-offcanvas-bar">
<div class="grid12 column first last ex-odd offcanvas"><div class="block widget widget-offcanvas no-title clearfix "><div class="content"> <nav class="ex-menu">
</nav> <!-- menu end -->
</div></div></div> </div>
</div>
</body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-7345936-12', 'auto');
  ga('send', 'pageview');

</script>
</html>
