<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html><head><style type="text/css">

.cookieInfoPanel {
	margin: 0px;
  	padding: 0px !important;
  	border: 0px;
  	background: #FFFFFF;
  	color: #000 !important;
  	font-family:Arial;
  	display:block;
  	float: none;
	background-position:center;
  	height: 55px;
  	margin-left:0px;
  	margin-right: 6px;
  	padding-right: 15px !important;
  	text-decoration: none;
  	overflow: hidden;
  	font-size: 12px;
  	outline: none !important;
  	font-weight: normal;
}
</style>
<title>
	Agnew Trade Centre
</title><link href="style/reset.css" rel="stylesheet" type="text/css"/><link href="style/landing.css" rel="stylesheet" type="text/css"/>
<!--[if IE]>
        <meta http-equiv="Page-Exit" content="Alpha(opacity=100)" />
    <![endif]-->
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/Museo_600.font.js" type="text/javascript"></script>
<script type="text/javascript">
        Cufon.replace('h1');
    </script>
</head><body id="mainForm" style="background-image:url(images/landing/backgroundHome.jpg);">
<div id="container">
<div class="cookieInfoPanel" id="cookie">
<table style="width:100%">
<tr>
<td>
<div id="pnlCookiePolicy"><font color="Black">
<div>
<table width="1000px">
<tr>
<td style="text-align:justify; padding-left:50px; padding-top:10px; padding-right:10px">
                                        We use cookies to ensure you have the best experience of AgnewTradeCentre.com. If you continue without changing the cookie settings in your browser, we will assume you are happy to receive cookies. However if you do not want to receive cookies, read our <a href="manageCookies.aspx" style="TEXT-DECORATION: underline"><font style="COLOR:darkblue">cookie guide.</font></a>
</td>
<td style="width:100px; padding-top:10px;padding-right:100px;text-align:center" valign="top">
<input id="btnAcceptCookies" name="ctl00$btnAcceptCookies" onclick="__doPostBack('ctl00','btnAcceptCookies')" type="button" value="Accept"/>
<br/>
<a href="cookiePolicy.aspx" id="lnkCookiePolicy" style="color:DarkBlue">Cookie Policy</a>
</td>
</tr>
</table>
</div>
</font></div>
</td>
</tr>
</table>
</div>
<div id="header">
<div id="logo">
<a href="index.aspx"></a>
</div>
<div id="search">
<form name="search">
<input name="search" type="text" value=""/>
<input onclick="alert('Please Sign-Up to be able search our Auctions'); return false;" src="images/landing/search-btn.png" type="image"/>
</form>
</div>
<div id="loginpanel">
<p>Welcome GUEST | <a href="logon.aspx" style="text-decoration:none; color:White">Log-in</a> | <a href="register.aspx" style="text-decoration:none; color:White">Sign-up</a></p>
</div>
<ul id="nav">
<li><a href="index.aspx">Home</a></li>
<li><a href="aboutUs.aspx">About Us</a></li>
<li><a href="contactUs.aspx">Contact Us</a></li>
<li><a href="findUs.aspx">Find Us</a></li>
</ul>
</div>
<div id="body">
<form action="./index.aspx" id="Form1" method="post">
<input id="__VIEWSTATE" name="__VIEWSTATE" type="hidden" value="/wEPDwUKLTY2NzUzNzU1MQ9kFgJmD2QWAgIDDxYCHgVzdHlsZQU4YmFja2dyb3VuZC1pbWFnZTp1cmwoaW1hZ2VzL2xhbmRpbmcvYmFja2dyb3VuZEhvbWUuanBnKTsWAgIBD2QWAgIBD2QWBAIBDxYCHgRUZXh0BfMCV2UgdXNlIGNvb2tpZXMgdG8gZW5zdXJlIHlvdSBoYXZlIHRoZSBiZXN0IGV4cGVyaWVuY2Ugb2YgQWduZXdUcmFkZUNlbnRyZS5jb20uIElmIHlvdSBjb250aW51ZSB3aXRob3V0IGNoYW5naW5nIHRoZSBjb29raWUgc2V0dGluZ3MgaW4geW91ciBicm93c2VyLCB3ZSB3aWxsIGFzc3VtZSB5b3UgYXJlIGhhcHB5IHRvIHJlY2VpdmUgY29va2llcy4gSG93ZXZlciBpZiB5b3UgZG8gbm90IHdhbnQgdG8gcmVjZWl2ZSBjb29raWVzLCByZWFkIG91ciA8YSBzdHlsZT0iVEVYVC1ERUNPUkFUSU9OOiB1bmRlcmxpbmUiIGhyZWY9Im1hbmFnZUNvb2tpZXMuYXNweCI+PGZvbnQgc3R5bGU9IkNPTE9SOmRhcmtibHVlIj5jb29raWUgZ3VpZGUuPC9mb250PjwvYT5kAgMPFgIeB29uY2xpY2sFKF9fZG9Qb3N0QmFjaygnY3RsMDAnLCdidG5BY2NlcHRDb29raWVzJylkGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYBBS5jdGwwMCRDb250ZW50UGxhY2VIb2xkZXIxJGxvZ2luUGFuZWwxJGJ0bkxvZ2luW7PzYbcPzlmnDiPoW0LgogxSwfzkVaQpbUGDLqh/j80="/>
<input id="__VIEWSTATEGENERATOR" name="__VIEWSTATEGENERATOR" type="hidden" value="90059987"/>
<input id="__EVENTVALIDATION" name="__EVENTVALIDATION" type="hidden" value="/wEdAAUjBn4V5Pop3A6rP3wY3/nlAr4OFmaO0w4QBR7s9EvhgEBSB+oK82EeaGfYE62NAczX792PAqcPuXECur2jDoQT9whUYY0cvJ515oCRZ7RNjK8rnYMYongoBM/mZuDDLqgc7pBGy+ivWQAuYAwD4SZ3"/>
<div style="padding-top: 270px">
<div id="preview">
<p>
                Sign-up and access all of the Agnew Group trade-ins and ex fleet cars</p>
<a href="register.aspx">
<img alt="Preview Cars" height="38" src="images/landing/register.png" width="206"/></a>
</div>
<h1>
            Welcome to AgnewTradeCentre.com<br/>
<div style="padding-top:16px">
            You're only seconds away from signing up to our
            exclusive database of quality cars at trade prices
            </div>
</h1>
<div id="signup">
<div id="ContentPlaceHolder1_loginPanel1_pnlLogin">
<table border="0" cellpadding="0" cellspacing="0" style="font-size: 9pt" width="210px">
<tr>
<td colspan="2">
<h2>
                    Login
                 </h2>
</td>
</tr>
<tr>
<td style="text-align: left">
<p style="color: Black">
                    Username:</p>
</td>
<td style="text-align: left; width: 130px">
<input class="textbox-block" id="ContentPlaceHolder1_loginPanel1_txtUsername" name="ctl00$ContentPlaceHolder1$loginPanel1$txtUsername" type="text"/>
</td>
</tr>
<tr>
<td style="text-align: left">
<p style="color: Black">
                    Password:</p>
</td>
<td style="text-align: left; width: 130px">
<input class="textbox-block" id="ContentPlaceHolder1_loginPanel1_txtPassword" name="ctl00$ContentPlaceHolder1$loginPanel1$txtPassword" type="password"/>
</td>
</tr>
<tr>
<td style="text-align: left; width: 70px">
</td>
<td style="text-align: left; width: 120px; padding-top:5px">
<input id="ContentPlaceHolder1_loginPanel1_btnLogin" name="ctl00$ContentPlaceHolder1$loginPanel1$btnLogin" src="images/landing/login.png" type="image"/>
</td>
</tr>
<tr>
<td colspan="2">
</td>
</tr>
</table>
<div align="right" style="padding-top:5px">
<a href="forgotPassword.aspx" style="color: #333; font-size: 8pt;">Forgot Username or
            Password?</a>
</div>
<div align="center" style="padding-top: 3px">
</div>
</div>
</div>
</div>
<hr/>
<div style="float:left">
<h2>
        Why Sign-up?
    </h2>
<ul>
<li>Online auction format</li>
<li>Free to use</li>
<li>Trade prices</li>
<li>Quality cars</li>
</ul>
</div>
<div style="float:right">
<img src="images/SytnerLogo.png" title="Sytner"/>
</div>
<div style="float:right; width:100%;padding-top:10px; text-align:right">
<a href="https://www.nextgearcapital.co.uk" target="_window">
<img height="50px" src="images/NextGearCapitalLeaderboard.gif" title="NextGear"/>
</a>
</div>
<div style="clear:all"></div>
</form>
<hr/>
<p class="footnote"><span>Isaac Agnew Ltd ©</span> 2013 | <a href="termsConditions.aspx">Terms &amp; Conditions</a> | <a href="privacyPolicy.aspx">Privacy policy</a> | Agnew Trade Centre, 18 Boucher Way, Belfast BT12 6RE</p>
<hr/>
</div>
</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18154826-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body></html>