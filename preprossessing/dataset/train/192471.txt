<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>BA Pilon Website</title>
<meta content="OpenOffice.org 1.1.4  (Win32)" name="generator"/>
<meta content="20051027;13254700" name="created"/>
<meta content="20051110;11564358" name="changed"/>
</head>
<body bgcolor="#ffffff" dir="ltr" lang="en-us">
<p align="center" style="margin-top: 0.17in; page-break-after: avoid"><font color="#4700b8"><font face="Thorndale"><font size="6"><b>Welcome to BAPilon.com</b></font></font></font></p>
<p align="center" style="margin-top: 0.17in; page-break-after: avoid"><font color="#4700b8"><img align="bottom" alt="Pilon Coat of Arms" border="0" height="222" id="Graphic1" src="pilonarms.gif" width="188"/></font></p>
<p align="center"><font color="#b3b300"><font face="thorndale"><font size="6"><b>This
is the Official Site of the Bernard and Mary Pilon Family</b></font></font></font></p>
<p align="center"><font color="#b3b300"><font face="Thorndale"><i>All
areas beyond this page are password protected</i></font></font></p>
<p align="center"><a href="http://siblings.bapilon.com/"><font size="4" style="font-size: 15pt"><font face="Thorndale"><font color="#4700b8">Dad, Siblings, Spouses and (Grand)Kids</font></font></font></a></p>
<p align="center"><a href="http://family.bapilon.com/"><font size="4" style="font-size: 15pt"><font face="Thorndale"><font color="#4700b8">Extended
Family</font></font></font></a></p>
<p align="center"><a href="http://friends.bapilon.com/"><font size="4" style="font-size: 15pt"><font face="Thorndale"><font color="#4700b8">Friends</font></font></font></a></p>
<p align="center"><a href="http://public.bapilon.com/"><font size="4" style="font-size: 15pt"><font face="Thorndale"><font color="#4700b8">Public Pages</font></font></font></a></p>
<br/>
<a href="http://validator.w3.org/check?uri=referer"><img alt="Valid HTML 4.0 Transitional" height="31" src="http://www.w3.org/Icons/valid-html40" width="88"/></a>
</body>
</html>