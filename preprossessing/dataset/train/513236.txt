<!DOCTYPE html>
<html lang="sr-RS">
<head>
<meta charset="utf-8"/>
<title>Euro Antik | kvalitetan, polovan, stilski nameštaj iz uvoza</title>
<meta content="Polovan nameštaj u salonu Euro Antik - vrhunski kvalitet po povoljnim cenama. Izaberite iz našeg bogatog asortimana komade najkvalitetnijeg stilskog nameštaja za Vaš prostor." name="description"/>
<link href="http://euroantikbg.com/" rel="canonical"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro%3A400,700%3Alatin%7CMontserrat%3A700%3Alatin" rel="stylesheet"/>
<link href="css/style.css" rel="stylesheet"/>
<link href="css/prettyPhoto.css" rel="stylesheet"/>
<link href="css/font-awesome.css" rel="stylesheet"/>
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/isotope.js" type="text/javascript"></script>
<script src="js/imagesloaded.js" type="text/javascript"></script>
<script src="js/modernizr.custom.24530.js" type="text/javascript"></script>
<link href="/favicon.ico" rel="icon" type="image/x-icon"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-119417276-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-119417276-1');
</script>
</head>
<body class="fixed-navigation">
<!-- JSON-LD ознаке које је генерисао Помоћник за означавање структурираних података. --> <script type="application/ld+json"> [ { "@context" : "http://schema.org", "@type" : "Product", "name" : "Polovan nameštaj", "description" : "Najbolji polovan nameštaj iz Holandije" }, { "@context" : "http://schema.org", "@type" : "Product", "name" : "Polovne garniture", "image" : "http://euroantikbg.com/images/polovne-garniture.jpg", "description" : "U ponudi su polovne garniture, kožne garniture, stilske garniture od drveta, garniture presvučenog plišom, vunenim meblom ili brokatom.", "url" : "http://euroantikbg.com/garniture.php" }, { "@context" : "http://schema.org", "@type" : "Product", "name" : "Polovne fotelje", "image" : "http://euroantikbg.com/images/polovne-fotelje.jpg", "description" : "Pronađite kvalitetnu fotelju od drveta ili kože koja će se savršeno uklopiti u enterijer Vašeg stana. Nudimo Vam veliki izbor fotelja", "url" : "http://euroantikbg.com/fotelje.php" }, { "@context" : "http://schema.org", "@type" : "Product", "name" : "Polovne komode", "image" : "http://euroantikbg.com/images/polovne-komode.jpg", "description" : "Izaberite neku od jedinstvenih primeraka komoda iz naše ponude. Komode obogaćene rezbarijama, intarzijama ili čipkastim okovima", "url" : "http://euroantikbg.com/komode.php" }, { "@context" : "http://schema.org", "@type" : "Product", "name" : "Polovne vitrine", "image" : "http://euroantikbg.com/images/polovne-vitrine.jpg", "description" : "Upotpunite Vaš dnevni boravak, trpezariju ili neku drugu prostoriju sa nekom od kvalitetnih vitrina od punog drveta iz naše ponude.", "url" : "http://euroantikbg.com/vitrine.php" }, { "@context" : "http://schema.org", "@type" : "Product", "name" : "Polovni regali/Ormani", "image" : "http://euroantikbg.com/images/polovni-regali-ormani.jpg", "description" : "Oplemenite prostor sa teškim i masivnim komadima nameštaja. Pogledajte našu ponudu ormana i regala rusticnog stila od punog drveta.", "url" : "http://euroantikbg.com/regali-ormani.php" }, { "@context" : "http://schema.org", "@type" : "Product", "name" : "Polovni stolovi i stolice", "image" : "http://euroantikbg.com/images/polovni-stolovi-stolice.jpg", "description" : "Udahnite Vašoj trpezariji novi život sa stolovima i stolicama iz naše ponude. Stolice od punog drveta u kombinaciji sa meblom ili kožom su pravi izbor", "url" : "http://euroantikbg.com/stolovi-i-stolice.php" }, { "@context" : "http://schema.org", "@type" : "Product", "name" : "Polovni klub stolovi", "image" : "http://euroantikbg.com/images/polovni-klub-stolovi.jpg", "description" : "Izaberite neki od klub stolova od punog drveta iz naše bogate ponude. Oplemenite prostor i svaki predah pretvorite u vrhunsko uživanje", "url" : "http://euroantikbg.com/klub-stolovi.php" }, { "@context" : "http://schema.org", "@type" : "Product", "name" : "Polovne lampe i lusteri", "image" : "http://euroantikbg.com/images/polovne-lampe-lusteri.jpg", "description" : "Nudimo Vam veliki izbor rustičnih i stilskih podnih i stonih lampi i lustera koji će svakoj prostoriji dati poseban starinski šarm", "url" : "http://euroantikbg.com/lampe-i-lusteri.php" }, { "@context" : "http://schema.org", "@type" : "Product", "name" : "Ostali polovni nameštaj", "image" : "http://euroantikbg.com/images/polovni-namestaj-ostalo.jpg", "description" : "Iz naše široke ponude izaberite natkasnu, ogledalo, škrinju, klupicu, stalak ili policu i dekorišite Vaš životni prostor sa stilom.", "url" : "http://euroantikbg.com/ostali-namestaj.php" } ] </script>
<div class="boxed-container">
<div class="top">
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-6">
<div class="top__left">Euro Antik - kvalitetan, polovan, stilski nameštaj iz uvoza</div>
</div>
<div class="col-xs-12 col-md-6">
<div class="top__right">
<ul class="navigation--top" id="menu-top-menu">
<li><a href="polovan-namestaj.php" target="_blank">Polovan nameštaj</a></li>
<li><a href="kontakt.php" target="_blank">Kontakt</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<header class="header">
<div class="container">
<div class="logo">
<a href="index.php">
<img alt="Euro Antik logo" class="img-responsive" src="images/logo.png"/>
</a>
</div>
<div class="header-widgets header-widgets-desktop">
<div class="widget widget-icon-box">
<div class="icon-box">
<i class="fa fa-phone fa-3x"></i>
<div class="icon-box__text">
<h4 class="icon-box__title">065 85 55 584</h4>
<span class="icon-box__subtitle">euroantik.bg@gmail.com</span>
</div>
</div>
</div>
<div class="widget widget-icon-box">
<div class="icon-box">
<i class="fa fa-home fa-3x"></i>
<div class="icon-box__text">
<h4 class="icon-box__title">Zrenjaninski put 5a</h4>
<span class="icon-box__subtitle">Beograd</span>
</div>
</div>
</div>
<div class="widget widget-icon-box">
<div class="icon-box">
<i class="fa fa-clock-o fa-3x"></i>
<div class="icon-box__text">
<h4 class="icon-box__title">Pon - Pet 9.00 - 18.00</h4>
<span class="icon-box__subtitle">Subotom 9.00 - 15.00</span>
</div>
</div>
</div>
<div class="widget widget-social-icons">
<a class="social-icons__link" href="https://www.facebook.com/ProdajaPolovnogNamestajaBeograd/" target="_blank"><i class="fa fa-facebook"></i></a>
<a class="social-icons__link" href="https://www.youtube.com/channel/UC1ol_UDaLrEd-sc9COl5BUA/" target="_blank"><i class="fa fa-youtube"></i></a>
<a class="social-icons__link" href="https://www.instagram.com/euroantik_stilski_namestaj/" target="_blank"><i class="fa fa-instagram"></i></a>
</div>
</div>
<!-- Toggle Button for Mobile Navigation -->
<button class="navbar-toggle" data-target="#buildpress-navbar-collapse" data-toggle="collapse" type="button">
<span class="navbar-toggle__text">NAVIGACIJA</span>
<span class="navbar-toggle__icon-bar">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</span>
</button>
</div>
<div class="sticky-offset js-sticky-offset"></div>
<div class="container">
<div class="navigation">
<div class="collapse navbar-collapse" id="buildpress-navbar-collapse">
<ul class="navigation--main" id="menu-main-menu">
<li class="current-menu-item"><a href="index.php">POČETNA</a></li>
<li><a href="o-nama.php">O NAMA</a></li>
<li class="menu-item-has-children">
<a href="polovan-namestaj.php">POLOVAN NAMEŠTAJ</a>
<ul class="sub-menu">
<li><a href="garniture.php">Garniture</a></li>
<li><a href="fotelje.php">Fotelje</a></li>
<li><a href="komode.php">Komode</a></li>
<li><a href="vitrine.php">Vitrine</a></li>
<li><a href="regali-ormani.php">Regali/Ormani</a></li>
<li><a href="stolovi-i-stolice.php">Stolovi i stolice</a></li>
<li><a href="klub-stolovi.php">Klub stolovi</a></li>
<li><a href="lampe-i-lusteri.php">Lampe i lusteri</a></li>
<li><a href="ostali-namestaj.php">Ostalo</a></li>
</ul>
</li>
<li><a href="novosti.php">NOVOSTI</a></li>
<li><a href="blog.php">BLOG</a></li>
<li><a href="kontakt.php">KONTAKT</a></li>
</ul>
</div>
</div>
</div>
<div class="container">
<div class="header-widgets hidden-md hidden-lg">
<div class="widget widget-icon-box">
<div class="icon-box">
<i class="fa fa-phone fa-3x"></i>
<div class="icon-box__text">
<h4 class="icon-box__title">065 85 55 584</h4>
<span class="icon-box__subtitle">euroantik.bg@gmail.com</span>
</div>
</div>
</div>
<div class="widget widget-icon-box">
<div class="icon-box">
<i class="fa fa-home fa-3x"></i>
<div class="icon-box__text">
<h4 class="icon-box__title">Zrenjaninski put 5a</h4>
<span class="icon-box__subtitle">Beograd</span>
</div>
</div>
</div>
<div class="widget widget-icon-box">
<div class="icon-box">
<i class="fa fa-clock-o fa-3x"></i>
<div class="icon-box__text">
<h4 class="icon-box__title">Pon - Pet 9.00 - 18.00</h4>
<span class="icon-box__subtitle">Subotom 9.00 - 15.00</span>
</div>
</div>
</div>
<div class="widget widget-social-icons">
<a class="social-icons__link" href="https://www.facebook.com/ProdajaPolovnogNamestajaBeograd/" target="_blank"><i class="fa fa-facebook"></i></a>
<a class="social-icons__link" href="https://www.youtube.com/channel/UC1ol_UDaLrEd-sc9COl5BUA/" target="_blank"><i class="fa fa-youtube"></i></a>
<a class="social-icons__link" href="https://www.instagram.com/euroantik_stilski_namestaj/" target="_blank"><i class="fa fa-instagram"></i></a>
</div>
</div>
</div>
</header>
<div class="jumbotron jumbotron--with-captions">
<div class="carousel slide js-jumbotron-slider" data-interval="5000" id="headerCarousel">
<!-- Wrapper for slides -->
<div class="carousel-inner">
<div class="item active">
<img alt="Euroantik Juzni bulevar" src="images/slide-6.jpg"/>
<div class="container">
<div class="carousel-content">
<div class="jumbotron__category">
<h6>EUROANTIK NOVA RADNJA U JUŽNOM BULEVARU</h6>
</div>
<div class="jumbotron__title">
<h1>Euro Antik Vračar</h1>
</div>
<div class="jumbotron__content">
<p><strong>Euro Antik</strong> je otvorio još jednu radnju na novoj lokaciji, na Vračaru, Južni bulevar 97...</p>
<a class="btn btn-primary" href="euro-antik-juzni-bulevar.php" target="_self">DETALJNIJE</a>
</div>
</div>
</div>
</div>
<div class="item">
<img alt="Polovne kozne garniture" src="images/slide-1.jpg"/>
<div class="container">
<div class="carousel-content">
<div class="jumbotron__category">
<h6>EUROANTIK GARNITURE</h6>
</div>
<div class="jumbotron__title">
<h1>Garniture i sofe</h1>
</div>
<div class="jumbotron__content">
<p><strong>Euro Antik</strong> Vam nudi veliki izbor salonskih garnitura, sofa, troseda, dvoseda i fotelja od kože, mebla, brokata...</p>
<a class="btn btn-primary" href="garniture.php" target="_self">DETALJNIJE</a>
</div>
</div>
</div>
</div>
<div class="item">
<img alt="Polovni stolovi i stolice" src="images/slide-2.jpg"/>
<div class="container">
<div class="carousel-content">
<div class="jumbotron__category">
<h6>EUROANTIK TRPEZARIJE</h6>
</div>
<div class="jumbotron__title">
<h1>Stolovi i stolice</h1>
</div>
<div class="jumbotron__content">
<p>Euro Antik Vam nudi veliki izbor trpezarijskih stolova i stolica od punog drveta, različitih oblika i dezena... </p>
<a class="btn btn-primary" href="stolovi-i-stolice.php" target="_self">DETALJNIJE</a>
</div>
</div>
</div>
</div>
<div class="item ">
<img alt="Polovni masivni namestaj" src="images/slide-3.jpg"/>
<div class="container">
<div class="carousel-content">
<div class="jumbotron__category">
<h6>EUROANTIK MASIVNI NAMEŠTAJ</h6>
</div>
<div class="jumbotron__title">
<h1>Ormani i regali</h1>
</div>
<div class="jumbotron__content">
<p>Nudimo Vam kvalitatne komade masivnog nameštaja. Pogledajte našu kompletnu ponudu ormana i regala od punog drveta...</p>
<a class="btn btn-primary" href="regali-ormani.php" target="_self">DETALJNIJE</a>
</div>
</div>
</div>
</div>
<div class="item">
<img alt="Polovne stilske komode" src="images/slide-4.jpg"/>
<div class="container">
<div class="carousel-content">
<div class="jumbotron__category">
<h6>EUROANTIK KOMODE</h6>
</div>
<div class="jumbotron__title">
<h1>Komode i vitrine</h1>
</div>
<div class="jumbotron__content">
<p>U ponudi su unikatni komadi nameštaja specifičnog stila i vrhunskih politura...</p>
<a class="btn btn-primary" href="komode.php" target="_self">DETALJNIJE</a>
</div>
</div>
</div>
</div>
<div class="item">
<img alt="Polovne natkasne, stocici, ogledala" src="images/slide-5.jpg"/>
<div class="container">
<div class="carousel-content">
<div class="jumbotron__category">
<h6>EUROANTIK stilski nameštaj</h6>
</div>
<div class="jumbotron__title">
<h1>Stočići, natkasne, ogledala</h1>
</div>
<div class="jumbotron__content">
<p>Oplemenite prostor rustičnim  natkasnama, stočićima, ogledalima, lampama, lusterima...</p>
<a class="btn btn-primary" href="ostali-namestaj.php" target="_self">DETALJNIJE</a>
</div>
</div>
</div>
</div>
</div>
<!-- Controls -->
<a class="left carousel-control" data-slide="prev" href="#headerCarousel" role="button">
<i class="fa fa-angle-left"></i>
</a>
<a class="right carousel-control" data-slide="next" href="#headerCarousel" role="button">
<i class="fa fa-angle-right"></i>
</a>
</div>
</div>
<div class="master-container">
<div class="promo">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="siteorigin-panels-stretch panel-row-style" style="background-color:#eeeeee">
<div class="panel-grid-cell">
<div class="panel widget widget_pt_banner panel-first-child panel-last-child">
<div class="banner__text">
										Potreban Vam je kvalitetan, stilski, polovan nameštaj od punog drveta?	
									</div>
<div class="banner__buttons">
<a class="btn btn-primary" href="polovan-namestaj.php" target="_self">Pogledajte ponudu</a> 
										<a class="btn btn-default" href="kontakt.php" target="_self">Kontaktirajte nas</a>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="spacer"></div>
<div class="container" role="main">
<div class="row">
<div class="col-md-12">
<h1>Polovan nameštaj Euro Antik</h1>
<h2>Najbolji polovan nameštaj iz Holandije</h2>
<p>Polovan nameštaj u salonu Euro Antik - vrhunski kvalitet po povoljnim cenama. <br/>
                Izaberite iz našeg bogatog asortimana komade najkvalitetnijeg stilskog nameštaja za Vaš prostor.</p>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<div class="container" role="main">
<div class="row">
<div class="col-md-4">
<div class="panel widget widget_pt_featured_page panel-first-child panel-last-child" id="panel-7-1-0-0">
<div class="has-post-thumbnail page-box page-box--block">
<a class="page-box__picture" href="garniture.php">
<img alt="Polovne garniture, kozne garniture" height="240" src="images/polovne-garniture.jpg" width="360"/>
</a>
<div class="page-box__content">
<h2 class="page-box__title text-uppercase">
<a href="garniture.php">Polovne garniture</a>
</h2>
								U ponudi su polovne garniture, kožne garniture, stilske garniture od drveta, garniture presvučenog plišom, vunenim meblom ili brokatom.
								<p>
<a class="read-more read-more--page-box" href="garniture.php">Detaljnije</a>
</p>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="panel widget widget_pt_featured_page panel-first-child panel-last-child" id="panel-7-1-0-0">
<div class="has-post-thumbnail page-box page-box--block">
<a class="page-box__picture" href="fotelje.php">
<img alt="Polovne fotelje" height="240" src="images/polovne-fotelje.jpg" width="360"/>
</a>
<div class="page-box__content">
<h2 class="page-box__title text-uppercase">
<a href="fotelje.php">Polovne fotelje</a>
</h2>
								Pronađite kvalitetnu fotelju od drveta ili kože koja će se savršeno uklopiti u enterijer Vašeg stana. Nudimo Vam veliki izbor fotelja. 
								<p>
<a class="read-more read-more--page-box" href="fotelje.php">Detaljnije</a>
</p>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="panel widget widget_pt_featured_page panel-first-child panel-last-child" id="panel-7-1-0-0">
<div class="has-post-thumbnail page-box page-box--block">
<a class="page-box__picture" href="komode.php">
<img alt="Polovne komode od punog drveta" height="240" src="images/polovne-komode.jpg" width="360"/>
</a>
<div class="page-box__content">
<h2 class="page-box__title text-uppercase">
<a href="komode.php">Polovne komode</a>
</h2>
								Izaberite neku od jedinstvenih primeraka komoda iz naše ponude. Komode obogaćene rezbarijama, intarzijama ili čipkastim okovima.
								<p>
<a class="read-more read-more--page-box" href="komode.php">Detaljnije</a>
</p>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="panel widget widget_pt_featured_page panel-first-child panel-last-child" id="panel-7-1-0-0">
<div class="has-post-thumbnail page-box page-box--block">
<a class="page-box__picture" href="vitrine.php">
<img alt="Polovne vitrine" height="240" src="images/polovne-vitrine.jpg" width="360"/>
</a>
<div class="page-box__content">
<h2 class="page-box__title text-uppercase">
<a href="vitrine.php">Polovne vitrine</a>
</h2>
								Upotpunite Vaš dnevni boravak, trpezariju ili neku drugu prostoriju sa nekom od kvalitetnih vitrina od punog drveta iz naše ponude. 
								<p>
<a class="read-more read-more--page-box" href="vitrine.php">Detaljnije</a>
</p>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="panel widget widget_pt_featured_page panel-first-child panel-last-child" id="panel-7-1-0-0">
<div class="has-post-thumbnail page-box page-box--block">
<a class="page-box__picture" href="regali-ormani.php">
<img alt="Polovni regali i ormani" height="240" src="images/polovni-regali-ormani.jpg" width="360"/>
</a>
<div class="page-box__content">
<h2 class="page-box__title text-uppercase">
<a href="regali-ormani.php">Polovni regali/Ormani</a>
</h2>
								Oplemenite prostor sa teškim i masivnim komadima nameštaja. Pogledajte našu ponudu ormana i regala rusticnog stila od punog drveta.	
								<p>
<a class="read-more read-more--page-box" href="regali-ormani.php">Detaljnije</a>
</p>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="panel widget widget_pt_featured_page panel-first-child panel-last-child" id="panel-7-1-0-0">
<div class="has-post-thumbnail page-box page-box--block">
<a class="page-box__picture" href="stolovi-i-stolice.php">
<img alt="Polovni stolovi i stolice" height="240" src="images/polovni-stolovi-stolice.jpg" width="360"/>
</a>
<div class="page-box__content">
<h2 class="page-box__title text-uppercase">
<a href="stolovi-i-stolice.php">Polovni stolovi i stolice</a>
</h2>
								Udahnite Vašoj trpezariji novi život sa stolovima i stolicama iz naše ponude. Stolice od punog drveta u kombinaciji sa meblom ili kožom su pravi izbor.
								<p>
<a class="read-more read-more--page-box" href="stolovi-i-stolice.php">Detaljnije</a>
</p>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="panel widget widget_pt_featured_page panel-first-child panel-last-child" id="panel-7-1-0-0">
<div class="has-post-thumbnail page-box page-box--block">
<a class="page-box__picture" href="klub-stolovi.php">
<img alt="Polovni klub stolovi" height="240" src="images/polovni-klub-stolovi.jpg" width="360"/>
</a>
<div class="page-box__content">
<h2 class="page-box__title text-uppercase">
<a href="klub-stolovi.php">Polovni klub stolovi</a>
</h2>
								Izaberite neki od klub stolova od punog drveta iz naše bogate ponude. Oplemenite prostor i svaki predah pretvorite u vrhunsko uživanje.
								<p>
<a class="read-more read-more--page-box" href="klub-stolovi.php">Detaljnije</a>
</p>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="panel widget widget_pt_featured_page panel-first-child panel-last-child" id="panel-7-1-0-0">
<div class="has-post-thumbnail page-box page-box--block">
<a class="page-box__picture" href="lampe-i-lusteri.php">
<img alt="Polovne lampe i lusteri" height="240" src="images/polovne-lampe-lusteri.jpg" width="360"/>
</a>
<div class="page-box__content">
<h2 class="page-box__title text-uppercase">
<a href="lampe-i-lusteri.php">Polovne lampe i lusteri</a>
</h2>
								Nudimo Vam veliki izbor rustičnih i stilskih podnih i stonih lampi i lustera koji će svakoj prostoriji dati poseban starinski šarm.
								<p>
<a class="read-more read-more--page-box" href="lampe-i-lusteri.php">Detaljnije</a>
</p>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="panel widget widget_pt_featured_page panel-first-child panel-last-child" id="panel-7-1-0-0">
<div class="has-post-thumbnail page-box page-box--block">
<a class="page-box__picture" href="ostali-namestaj.php">
<img alt="Polovan namestaj" height="240" src="images/polovni-namestaj-ostalo.jpg" width="360"/>
</a>
<div class="page-box__content">
<h2 class="page-box__title text-uppercase">
<a href="ostali-namestaj.php">Ostali polovni nameštaj</a>
</h2>
								Iz naše široke ponude izaberite natkasnu, ogledalo, škrinju, klupicu, stalak ili policu i dekorišite Vaš životni prostor sa stilom.
								<p>
<a class="read-more read-more--page-box" href="ostali-namestaj.php">Detaljnije</a>
</p>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="spacer-big"></div>
<div class="panel-grid" id="pg-7-5">
<div class="promobg">
<div class="container">
<div class="panel widget row">
<div class="col-md-12">
<div class="motivational-text">
								Euro Antik Vam nudi najbolji polovan nameštaj iz Holandije. Pogledajte našu široku ponudu najkvalitetnijih komada stilskog nameštaja, pozovite nas ili nas posetite. 
							</div>
</div>
</div>
</div>
</div>
</div>
<div class="spacer-big"></div>
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="panel widget widget_text panel-first-child" id="panel-7-3-0-0">
<div class="textwidget"></div>
</div>
<div class="panel panel-grid widget widget_black-studio-tinymce panel-last-child" id="panel-7-3-0-1">
<h3 class="widget-title">Zašto nas izabrati?</h3>
<div class="textwidget">
<h5>
<span style="color: #fcc71f"><br/><span class="icon-container"><span class="fa fa-check"></span></span></span>
<span style="color: #333333">KVALITET</span>
</h5>
<p>
								Uvozimo isključivo kvalitetan, antikvarni, stilski, rustičan polovni nameštaj od punog drveta iz Holandije.
							</p>
<h5>
<span style="color: #fcc71f"><br/><span class="icon-container"><span class="fa fa-check"></span></span></span> 
								ISKUSTVO
							</h5>
<p>
								Karakteriše nas dugogodišnje iskustvo praćeno velikom upornošću, odricanjima i ulaganjima. Danas oko nas imamo izgrađenu mrežu ozbiljnih i lojalnih poslovnih partnera od poverenja koji ispunjavaju sve potrebe našeg tržišta.
							</p>
<h5>
<span style="color: #fcc71f"><br/><span class="icon-container"><span class="fa fa-check"></span></span></span>
<span style="color: #333333">POSVEĆENOST</span>
</h5>
<p>
								Cene nameštaja koji uvozimo su izuzetno povoljne. Našim kupcima smo omogućili razne načine plaćanja, kao i o mogućnosti da iz Holandije uvezemo nameštaj prema posebnoj želji bez prethodnog plaćanja narudžbine.
							</p>
</div>
</div>
</div>
<div class="col-md-6">
<div class="panel widget widget_text panel-first-child" id="panel-7-3-1-0">
<div class="textwidget"></div>
</div>
<div class="panel panel-grid widget widget_black-studio-tinymce panel-last-child" id="panel-7-3-1-1">
<h3 class="widget-title">Ko smo mi?</h3>
<div class="textwidget">
<p>
<a data-rel="prettyPhoto" href="images/euro-antik-polovan-namestaj.jpg">
<img alt="Opis slike" class="alignleft wp-image-115 size-medium" height="168" src="images/euro-antik-polovan-namestaj.jpg" width="300"/>
</a>
</p>
<p>
								Euro Antik je prvi u Srbiju doneo polovan nameštaj iz Holandije. Već dug niz godina u vaše domove unosimo šarmantnu rustiku, strogu klasiku i modernu. Sledimo vanvremene estetske kodekse kojima se čovek današnjice uveliko vraća zamoren hladnim linijama unificiranog savremenog nameštaja. Naš nameštaj udovoljava visokim zahtevima izgleda i očuvanosti.</p>
<h5><strong><a href="o-nama.php">Više o nama</a></strong></h5>
</div>
</div>
</div>
</div>
</div>
<div class="spacer-big"></div>
</div>
<footer>
<div class="footer">
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-4">
<div class="widget widget_text push-down-30">
<h6 class="footer__headings">O NAMA</h6>
<div class="textwidget">
<img alt="Euro Antik namestaj" height="45" src="images/logo_footer.png" width="218"/>
<br/><br/>
                                Euro Antik<br/>
                                Zrenjaninski put 5a, Beograd<br/>
                                065 85 55 584<br/>
                                011 73 17 499<br/>
<strong><a href="mailto:euroantik.bg@gmail.com">euroantik.bg@gmail.com</a></strong>
<br/><br/>
</div>
</div>
</div>
<div class="col-xs-12 col-md-4">
<div class="widget widget_nav_menu push-down-30">
<h6 class="footer__headings">POLOVAN NAMEŠTAJ</h6>
<div class="menu-top-menu-container">
<ul class="menu" id="menu-top-menu-1">
<li><a href="garniture.php">Garniture</a></li>
<li><a href="fotelje.php">Fotelje</a></li>
<li><a href="komode.php">Komode</a></li>
<li><a href="vitrine.php">Vitrine</a></li>
<li><a href="regali-ormani.php">Regali/Ormani</a></li>
<li><a href="stolovi-i-stolice.php">Stolovi i stolice</a></li>
<li><a href="klub-stolovi.php">Klub stolovi</a></li>
<li><a href="lampe-i-lusteri.php">Lampe i lusteri</a></li>
<li><a href="ostali-namestaj.php">Ostalo</a></li>
</ul>
</div>
</div>
</div>
<div class="col-xs-12 col-md-4">
<div class="widget widget_text push-down-30">
<h6 class="footer__headings">O nama</h6>
<div class="textwidget">
								Euro Antik Vam nudi najbolji polovan nameštaj iz Holandije. Pogledajte našu široku ponudu najkvalitetnijih komada stilskog nameštaja, pozovite nas ili nas posetite.
								<br/><br/>
<a class="btn btn-default" href="o-nama.php">Detaljnije</a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="footer-bottom">
<div class="container">
<div class="footer-bottom__right">
<strong>Euro Antik</strong> 2018.	
				</div>
<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
</div>
</div>
</footer>
<!-- end of .boxed-container -->
<script src="js/almond.js" type="text/javascript"></script>
<script src="js/underscore.js" type="text/javascript"></script>
<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="js/header_carousel.js" type="text/javascript"></script>
<script src="js/sticky_navbar.js" type="text/javascript"></script>
<script src="js/simplemap.js" type="text/javascript"></script>
<script src="js/main.min.js" type="text/javascript"></script>
<script src="js/main.js" type="text/javascript"></script>
<script src="js/require.js" type="text/javascript"></script>
</body>
</html>