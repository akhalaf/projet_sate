<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>ecoprint Impresores</title>
<meta content="width = 1000, minimum-scale = 0.25, maximum-scale = 1.60" name="viewport"/>
<meta content="Freeway 5 Pro 5.4.2" name="GENERATOR"/>
<meta content="Ecoprint Impresores, Servicio de Imprenta y Diseño con la calidad que usted necesita. Avda Cuevas 1291 Santiago, Chile" name="Description"/>
<meta content="imprenta, ecoprint, impresores, diseño grafico, offset, catalogos, revistas, libros, flyers, manuales, folletos, calidad, impresión, brochures, papelerias, imagen corporativa" name="Keywords"/>
<style type="text/css">
<!-- 
body { margin:0px; background-color:#fff; height:100% }
html { height:100% }
img { margin:0px; border-style:none }
button { margin:0px; border-style:none; padding:0px; background-color:transparent; vertical-align:top }
p:first-child { margin-top:0px }
table { empty-cells:hide }
.f-sp { font-size:1px; visibility:hidden }
.f-lp { margin-bottom:0px }
.f-fp { margin-top:0px }
.f-x1 {  }
.f-x2 {  }
.f-x3 {  }
em { font-style:italic }
h1 { font-size:18px }
h1:first-child { margin-top:0px }
strong { font-weight:bold }
-->
</style>
<!--[if lt IE 7]>
<link rel=stylesheet type="text/css" href="css/ie6.css">
<![endif]-->
</head>
<body>
<div id="PageDiv" style="position:relative; min-height:100%">
<table border="0" cellpadding="0" cellspacing="0" width="952">
<colgroup>
<col width="49"/>
<col width="1"/>
<col width="900"/>
<col width="1"/>
<col width="1"/>
</colgroup>
<tr valign="top">
<td colspan="4" height="20"></td>
<td height="20"></td>
</tr>
<tr valign="top">
<td height="136"></td>
<td colspan="3" height="136"><img alt="banner03" border="0" height="136" src="Resources/banner03.jpeg" style="float:left" width="902"/></td>
<td height="136"></td>
</tr>
<tr valign="top">
<td colspan="2" height="62"></td>
<td height="62"><img alt="botomera" border="0" height="62" src="Resources/botomera.jpeg" style="float:left" usemap="#map1" width="900"/></td>
<td height="62"></td>
<td height="62"></td>
</tr>
<tr class="f-sp">
<td><img alt="" border="0" height="1" src="Resources/_clear.gif" style="float:left" width="49"/></td>
<td><img alt="" border="0" height="1" src="Resources/_clear.gif" style="float:left" width="1"/></td>
<td><img alt="" border="0" height="1" src="Resources/_clear.gif" style="float:left" width="900"/></td>
<td><img alt="" border="0" height="1" src="Resources/_clear.gif" style="float:left" width="1"/></td>
<td height="32"><img alt="" border="0" height="1" src="Resources/_clear.gif" style="float:left" width="1"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="951">
<colgroup>
<col width="49"/>
<col width="480"/>
<col width="421"/>
<col width="1"/>
</colgroup>
<tr valign="top">
<td height="386"></td>
<td height="386"><img alt="Ecoprint Impresores Ltda. fue fundada" border="0" height="267" src="Resources/index1.gif" style="position:relative; top:1px; float:left" width="478"/></td>
<td height="386"><img alt="DSC01582" border="0" height="386" src="Resources/dsc01582.jpeg" style="float:left" width="421"/></td>
<td height="386"></td>
</tr>
<tr class="f-sp">
<td><img alt="" border="0" height="1" src="Resources/_clear.gif" style="float:left" width="49"/></td>
<td><img alt="" border="0" height="1" src="Resources/_clear.gif" style="float:left" width="480"/></td>
<td><img alt="" border="0" height="1" src="Resources/_clear.gif" style="float:left" width="421"/></td>
<td height="11"><img alt="" border="0" height="1" src="Resources/_clear.gif" style="float:left" width="1"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="951">
<colgroup>
<col width="49"/>
<col width="901"/>
<col width="1"/>
</colgroup>
<tr valign="top">
<td height="6"></td>
<td height="6"><img alt="" border="0" height="6" src="Resources/filete01.jpeg" style="float:left" width="901"/></td>
<td height="6"></td>
</tr>
<tr class="f-sp">
<td><img alt="" border="0" height="1" src="Resources/_clear.gif" style="float:left" width="49"/></td>
<td><img alt="" border="0" height="1" src="Resources/_clear.gif" style="float:left" width="901"/></td>
<td height="10"><img alt="" border="0" height="1" src="Resources/_clear.gif" style="float:left" width="1"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="951">
<colgroup>
<col width="49"/>
<col width="901"/>
<col width="1"/>
</colgroup>
<tr valign="top">
<td height="26"></td>
<td height="26"><img alt="Ecoprint Impresores Ltda. • Cuevas" border="0" height="14" src="Resources/index3.gif" style="position:relative; left:88px; top:1px; float:left" width="725"/></td>
<td height="26"></td>
</tr>
<tr class="f-sp">
<td><img alt="" border="0" height="1" src="Resources/_clear.gif" style="float:left" width="49"/></td>
<td><img alt="" border="0" height="1" src="Resources/_clear.gif" style="float:left" width="901"/></td>
<td height="1"><img alt="" border="0" height="1" src="Resources/_clear.gif" style="float:left" width="1"/></td>
</tr>
</table>
<map name="map1">
<area alt="CONTACTO02" coords="757,20,862,59" href="contacto.html" target="_self" title="contacto"/>
<area alt="TRABAJOS02" coords="577,18,682,57" href="trabajos.html" target="_self" title="trabajos"/>
<area alt="CLIENTES02" coords="399,18,504,57" href="clientes.html" target="_self" title="clientes"/>
<area alt="SERVICIO02" coords="220,18,325,57" href="servicios.html"/>
</map>
</div>
</body>
</html>
