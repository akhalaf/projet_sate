<!DOCTYPE html>
<html lang="en-US">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.appserv.org/xmlrpc.php" rel="pingback"/>
<title> AppServ : Apache + PHP + MYSQL – AppServ, AppServHosting, AppServNetwork, AppServ Download</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.appserv.org/en/feed/" rel="alternate" title=" AppServ : Apache + PHP + MYSQL » Feed" type="application/rss+xml"/>
<link href="https://www.appserv.org/en/comments/feed/" rel="alternate" title=" AppServ : Apache + PHP + MYSQL » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.appserv.org\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.4"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.appserv.org/wp-includes/css/dist/block-library/style.min.css?ver=5.2.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.appserv.org/wp-content/plugins/accesspress-social-share/css/font-awesome/font-awesome.min.css?ver=4.4.8" id="apss-font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans&amp;ver=5.2.4" id="apss-font-opensans-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.appserv.org/wp-content/plugins/accesspress-social-share/css/frontend.css?ver=4.4.8" id="apss-frontend-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.appserv.org/wp-content/plugins/share-this/css/style.css?ver=5.2.4" id="st-widget-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.appserv.org/wp-content/themes/mantra/style.css?ver=2.6.0" id="mantras-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.appserv.org/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.4.0" id="elementor-icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.appserv.org/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=2.7.5" id="elementor-animations-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.appserv.org/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=2.7.5" id="elementor-frontend-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.appserv.org/wp-content/plugins/elementor-pro/assets/css/frontend.min.css?ver=2.7.3" id="elementor-pro-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.appserv.org/wp-content/uploads/elementor/css/global.css?ver=1572540737" id="elementor-global-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;ver=5.2.4" id="google-fonts-1-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.appserv.org/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.appserv.org/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.appserv.org/wp-content/themes/mantra/js/frontend.js?ver=2.6.0" type="text/javascript"></script>
<link href="https://www.appserv.org/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.appserv.org/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.appserv.org/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.2.4" name="generator"/>
<link href="https://www.appserv.org/en/" rel="canonical"/>
<link href="https://www.appserv.org/" rel="shortlink"/>
<link href="https://www.appserv.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.appserv.org%2Fen%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://www.appserv.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.appserv.org%2Fen%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<script charset="utf-8" type="text/javascript">var switchTo5x=true;</script>
<script charset="utf-8" src="https://ws.sharethis.com/button/buttons.js" type="text/javascript"></script>
<script charset="utf-8" type="text/javascript">stLight.options({"publisher":"5edd87a4-cfe6-4c1c-bafb-def3216ecb81","doNotCopy":true,"hashAddressBar":false,"doNotHash":true});var st_type="wordpress4.4.1";</script>
<link href="https://www.appserv.org/en/" hreflang="en" rel="alternate"/>
<link href="https://www.appserv.org/th/" hreflang="th" rel="alternate"/>
<link href="https://www.appserv.org/" hreflang="x-default" rel="alternate"/>
<style type="text/css"> #wrapper, #access, #colophon, #branding, #main { width:1100px ;} #content { width:790px;} #primary,#secondary {width:250px;}#content, #content p, #content ul, #content ol, #content input, #content select, #content textarea{ font-size:14px; } body, .widget-title {font-family:Segoe UI, Arial, sans-serif ; } #content h1.entry-title a, #content h2.entry-title a, #content h1.entry-title , #content h2.entry-title {font-family:Georgia, Times New Roman, Times, serif ; } .widget-area * {font-family:Helvetica, sans-serif ; } .entry-content h1, .entry-content h2, .entry-content h3, .entry-content h4, .entry-content h5, .entry-content h6 {font-family:Georgia, Times New Roman, Times, serif ; } .nocomments, .nocomments2 {display:none;} #header-container > div { margin-top:20px;} #header-container > div { margin-left:40px;} body { background-color:#444444 !important ;} #header { background-color:#333333 ;} #footer { background-color:#222222 ;} #footer2 { background-color:#171717 ;} #site-title span a { color:#0D85CC ;} #site-description { color:#999999 ;} #content, #content p, #content ul, #content ol { color:#333333 ;} .widget-area a:link, .widget-area a:visited, a:link, a:visited ,#searchform #s:hover , #container #s:hover, #access a:hover, #wp-calendar tbody td a , #site-info a ,#site-copyright a, #access li:hover > a, #access ul ul :hover > a { color:#0D85CC;} a:hover, .entry-meta a:hover, .entry-utility a:hover , .widget-area a:hover { color:#12a7ff ;} #content .entry-title a, #content .entry-title, #content h1, #content h2, #content h3, #content h4, #content h5, #content h6{ color:#444444 ;} #content .entry-title a:hover { color:#000000 ;} .widget-title,#footer-widget-area .widget-title { background-color:#444444 ;} .widget-title { color:#2EA5FD ;} #footer-widget-area .widget-title { color:#0C85CD ; ;} #footer-widget-area a { color:#666666 ;} #footer-widget-area a:hover { color:#888888 ;} #content p, .entry-content ul, .entry-summary ul , .entry-content ol, .entry-summary ol { margin-bottom:1.5em;} .entry-meta .entry-time {display:none;} #branding { height:75px ;} </style>
<meta content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" name="viewport"/><style>/* Mantra Custom CSS */ </style>
<!--[if lte IE 8]>
<style type="text/css" media="screen">
 #access ul  li,
.edit-link a ,
 #footer-widget-area .widget-title, .entry-meta,.entry-meta .comments-link,
.short-button-light, .short-button-dark ,.short-button-color ,blockquote  {
     position:relative;
     behavior: url(https://www.appserv.org/wp-content/themes/mantra/js/PIE/PIE.php);
   }

#access ul ul {
-pie-box-shadow:0px 5px 5px #999;
}
   
#access  ul  li.current_page_item,  #access ul li.current-menu-item ,
#access ul  li ,#access ul ul ,#access ul ul li, .commentlist li.comment	,.commentlist .avatar,
 .nivo-caption, .theme-default .nivoSlider {
     behavior: url(https://www.appserv.org/wp-content/themes/mantra/js/PIE/PIE.php);
   }
</style>
<![endif]-->
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #ffffff; }
</style>
<!--[if lt IE 9]>
<script>
document.createElement('header');
document.createElement('nav');
document.createElement('section');
document.createElement('article');
document.createElement('aside');
document.createElement('footer');
document.createElement('hgroup');
</script>
<![endif]-->
<script type="text/javascript">
function makeDoubleDelegate(function1, function2) {
// concatenate functions
    return function() { if (function1) function1(); if (function2) function2(); }
}

function mantra_onload() {


     // Add responsive videos
     if (jQuery(window).width() < 800) jQuery(".entry-content").fitVids();
}; // mantra_onload


jQuery(document).ready(function(){
     // Add custom borders to images
     jQuery("img.alignnone, img.alignleft, img.aligncenter,  img.alignright").addClass("imageSeven");

	// Add select navigation to small screens
     jQuery("#access > .menu > ul").tinyNav({
          	header: ' = Menu = '
			});
});

// make sure not to lose previous onload events
window.onload = makeDoubleDelegate(window.onload, mantra_onload );
</script>
<meta data-pso-pt="front" data-pso-pv="1.2.1" data-pso-th="71076bdc7c2d88f9e9480c92a7e5f21e"/></head>
<body class="home page-template page-template-template-onecolumn page-template-template-onecolumn-php page page-id-171 custom-background elementor-default elementor-page elementor-page-171">
<div id="toTop"> </div>
<div class="hfeed" id="wrapper">
<nav class="topmenu"><ul class="menu" id="menu-language"><li class="lang-item lang-item-3 lang-item-en lang-item-first current-lang menu-item menu-item-type-custom menu-item-object-custom current_page_item menu-item-home menu-item-25-en" id="menu-item-25-en"><a href="https://www.appserv.org/en/" hreflang="en-US" lang="en-US"><img alt="English" height="11" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHzSURBVHjaYkxOP8IAB//+Mfz7w8Dwi4HhP5CcJb/n/7evb16/APL/gRFQDiAAw3JuAgAIBEDQ/iswEERjGzBQLEru97ll0g0+3HvqMn1SpqlqGsZMsZsIe0SICA5gt5a/AGIEarCPtFh+6N/ffwxA9OvP/7//QYwff/6fZahmePeB4dNHhi+fGb59Y4zyvHHmCEAAAW3YDzQYaJJ93a+vX79aVf58//69fvEPlpIfnz59+vDhw7t37968efP3b/SXL59OnjwIEEAsDP+YgY53b2b89++/awvLn98MDi2cVxl+/vl6mituCtBghi9f/v/48e/XL86krj9XzwEEEENy8g6gu22rfn78+NGs5Ofr16+ZC58+fvyYwX8rxOxXr169fPny+fPn1//93bJlBUAAsQADZMEBxj9/GBxb2P/9+S/R8u3vzxuyaX8ZHv3j8/YGms3w8ycQARmi2eE37t4ACCDGR4/uSkrKAS35B3TT////wADOgLOBIaXIyjBlwxKAAGKRXjCB0SOEaeu+/y9fMnz4AHQxCP348R/o+l+//sMZQBNLEvif3AcIIMZbty7Ly6t9ZmXl+fXj/38GoHH/UcGfP79//BBiYHjy9+8/oUkNAAHEwt1V/vI/KBY/QSISFqM/GBg+MzB8A6PfYC5EFiDAABqgW776MP0rAAAAAElFTkSuQmCC" title="English" width="16"/></a></li>
<li class="lang-item lang-item-6 lang-item-th menu-item menu-item-type-custom menu-item-object-custom menu-item-25-th" id="menu-item-25-th"><a href="https://www.appserv.org/th/" hreflang="th" lang="th"><img alt="ไทย" height="11" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFWSURBVHjaYvzPgAD/UNlYEUAAisQgBwAQhGGi/pzP8EBvG+BImqbL7pzuUlda9SJ7DMs85NYEBgX5Ir4AYvz/H2QHhIQz/mMDjIyMnz59AgggRkfXjTmZOu/e/fz7D2jH/7///v398+8PkPEHCEHsv3///fn978+/f8JCnGWlWwACiGX/7jOmhiKPHn3+8wck8fvPv9+//wLRr1//wORfOCkvz8fAsAUggIB++AdxJ8iRQNf++f/rF8TZ/4B6fgEZQPIXRAEoLAACCKjhx9+/f/78+f0LaC/YbIjxyGaDSaCFvxgYvgAEEAs3r5qKqhAPLzs4GP4CnQR2G9CMf2A2iPEH7BNJSe5Tp8wAAojx58+fzMzM//79wxU4EACUBYbS27dvAQKI5R87O1NJCQPEjX//MvwGkn8Yf/8GRggCAY0DSgFt2bsXIIAYv6JGJJ44hgCAAAMA8pZimQIezaoAAAAASUVORK5CYII=" title="ไทย" width="16"/></a></li>
</ul></nav>
<header id="header">
<div id="masthead">
<div id="branding" role="banner">
<img alt="" id="bg_image" src="https://www.appserv.org/wp-content/uploads/2016/01/logobanner.png" title=""/>
<div id="header-container">
<div><div id="site-title"><span> <a href="https://www.appserv.org/en/" rel="home" title=" AppServ : Apache + PHP + MYSQL"> AppServ : Apache + PHP + MYSQL</a> </span></div><div id="site-description">AppServ, AppServHosting, AppServNetwork, AppServ Download</div></div><div class="socials" id="sheader">
<a class="socialicons social-Facebook" href="#" rel="nofollow" target="_blank" title="Facebook"><img alt="Facebook" src="https://www.appserv.org/wp-content/themes/mantra/images/socials/Facebook.png"/></a>
<a class="socialicons social-Twitter" href="#" rel="nofollow" target="_blank" title="Twitter"><img alt="Twitter" src="https://www.appserv.org/wp-content/themes/mantra/images/socials/Twitter.png"/></a>
<a class="socialicons social-RSS" href="#" rel="nofollow" target="_blank" title="RSS"><img alt="RSS" src="https://www.appserv.org/wp-content/themes/mantra/images/socials/RSS.png"/></a></div></div> <div style="clear:both;"></div>
</div><!-- #branding -->
<nav class="jssafe" id="access" role="navigation">
<div class="skip-link screen-reader-text"><a href="#content" title="Skip to content">Skip to content</a></div>
<div class="menu"><ul class="menu" id="prime_nav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-171 current_page_item menu-item-176" id="menu-item-176"><a aria-current="page" href="https://www.appserv.org/en/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158" id="menu-item-158"><a href="https://www.appserv.org/en/download/">Download</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-678" id="menu-item-678"><a href="https://www.appserv.org/en/faq/">FAQ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-180" id="menu-item-180"><a href="https://www.appserv.org/en/howto/">Howto Install</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-181" id="menu-item-181"><a href="https://www.appserv.org/en/howto-use/">Howto Use</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-184" id="menu-item-184"><a href="https://www.appserv.org/en/version-history/">Version History</a></li>
<li class="ppr-new-window menu-item menu-item-type-post_type menu-item-object-page menu-item-182" id="menu-item-182"><a href="https://www.appserv.org/en/appservhosting/" rel="noopener noreferrer" target="_blank">Hosting</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-185" id="menu-item-185"><a href="https://www.appserv.org/en/about/">About</a></li>
</ul></div>
</nav><!-- #access -->
</div><!-- #masthead -->
<div style="clear:both;"> </div>
</header><!-- #header -->
<div id="main">
<div id="forbottom">
<div style="clear:both;"> </div>
<section class="one-column" id="container">
<div id="content" role="main">
<div class="post-171 page type-page status-publish hentry" id="post-171">
<h1 class="entry-title">Home</h1>
<div class="entry-content">
<p class="no-break"><span class="st_fblike_hcount" st_title="Home" st_url="https://www.appserv.org/en/"></span><span class="st_facebook_hcount" st_title="Home" st_url="https://www.appserv.org/en/"></span><span class="st_twitter_hcount" st_title="Home" st_url="https://www.appserv.org/en/"></span><span class="st_sharethis_hcount" st_title="Home" st_url="https://www.appserv.org/en/"></span><span class="st_linkedin_hcount" st_title="Home" st_url="https://www.appserv.org/en/"></span><span class="st_plusone_hcount" st_title="Home" st_url="https://www.appserv.org/en/"></span></p> <div class="elementor elementor-171" data-elementor-id="171" data-elementor-settings="[]" data-elementor-type="wp-page">
<div class="elementor-inner">
<div class="elementor-section-wrap">
<section class="elementor-element elementor-element-42832348 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section" data-id="42832348">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-row">
<div class="elementor-element elementor-element-117a3ea0 elementor-column elementor-col-100 elementor-top-column" data-element_type="column" data-id="117a3ea0">
<div class="elementor-column-wrap elementor-element-populated">
<div class="elementor-widget-wrap">
<div class="elementor-element elementor-element-1e939384 elementor-widget elementor-widget-text-editor" data-element_type="widget" data-id="1e939384" data-widget_type="text-editor.default">
<div class="elementor-widget-container">
<div class="elementor-text-editor elementor-clearfix"><table align="center" border="0" width="1054">
<tbody>
<tr>
<td height="23" valign="top" width="607">
<h1><strong>AppServ : Apache + PHP + MySQL</strong></h1>
<h2><strong>Simple package for programming</strong></h2>
<ul>
<li><strong>Quickly and easy to install Apache, PHP, MySQL.</strong></li>
<li><strong>Don’t need any skill for setting up step by step.</strong></li>
<li><strong>Can turn your PC to Web Server and Database Server.</strong></li>
</ul>
<h3><strong>AppServ is <span style="color: #0000ff;">FREE</span> for everyone in this world.</strong></h3>
<h2><img alt="setup01" height="161" src="https://www.appserv.org/wp-content/uploads/2016/01/setup02.jpg" width="210"/>  <img alt="setup02" height="161" src="https://www.appserv.org/wp-content/uploads/2016/01/setup03.jpg" width="210"/>
<img alt="setup03" height="161" src="https://www.appserv.org/wp-content/uploads/2016/01/setup04.jpg" width="210"/>  <img alt="setup04" height="161" src="https://www.appserv.org/wp-content/uploads/2016/01/setup05.jpg" width="210"/></h2>
<h3></h3>
</td>
<td valign="top" width="283">
<h1><img alt="topicappserv" height="80" src="https://www.appserv.org/wp-content/uploads/2016/01/topicappserv.gif" style="font-family: inherit; font-size: 16px;" width="88"/> <span style="color: #d10d0d;"><strong>AppServ 9.3.0 </strong></span></h1>
<ul>
<li>
<h2><span style="color: #000000;">Apache 2.4.41</span></h2>
</li>
<li>
<h2><span style="color: #000000;">PHP 7.3.10</span></h2>
</li>
<li>
<h2><span style="color: #000000;">MySQL 8.0.17</span></h2>
</li>
<li>
<h2><span style="color: #000000;">phpMyAdmin 4.9.1
</span></h2>
</li>
<li>
<h2><span style="color: #000000;">Support TLS,SSL or https
</span></h2>
</li>
<li>
<h2><span style="color: #000000;">For 64bit only</span></h2>
</li>
</ul>
<h3><strong><span style="color: #000000;">Release Date :</span> <span style="color: #0000ff;">2019-09-29</span></strong></h3>
<a href="https://www.appserv.org/download/"><img alt="downloadbutton" height="130" src="https://www.appserv.org/wp-content/uploads/2016/01/downloadbutton.png" width="333"/></a>
<h2></h2>
</td>
</tr>
</tbody>
</table></div>
</div>
</div>
<div class="elementor-element elementor-element-48a47a7 elementor-widget elementor-widget-text-editor" data-element_type="widget" data-id="48a47a7" data-widget_type="text-editor.default">
<div class="elementor-widget-container">
<div class="elementor-text-editor elementor-clearfix"><h1 style="text-align: center;"><span style="font-size: 160%;"><span style="color: #ff0000;"><strong>Support AppServ Join Our Course Online</strong></span></span></h1></div>
</div>
</div>
<div class="elementor-element elementor-element-6e92c57 elementor-widget elementor-widget-text-editor" data-element_type="widget" data-id="6e92c57" data-widget_type="text-editor.default">
<div class="elementor-widget-container">
<div class="elementor-text-editor elementor-clearfix"><h3 class="entry-title" style="background-color: #ffffff;"><a href="https://course.appserv.org/courses/raspberrypi-4-workshop/"><span style="text-decoration-line: underline;">Raspberry Pi 4 Workshop by AppServ Course Online</span></a></h3></div>
</div>
</div>
<div class="elementor-element elementor-element-8361113 elementor-widget elementor-widget-text-editor" data-element_type="widget" data-id="8361113" data-widget_type="text-editor.default">
<div class="elementor-widget-container">
<div class="elementor-text-editor elementor-clearfix"><p><img alt="" class="wp-image-910 alignleft" height="356" sizes="(max-width: 475px) 100vw, 475px" src="https://www.appserv.org/wp-content/uploads/2019/10/S__3760135-1024x768.jpg" srcset="https://www.appserv.org/wp-content/uploads/2019/10/S__3760135-1024x768.jpg 1024w, https://www.appserv.org/wp-content/uploads/2019/10/S__3760135-1024x768-300x225.jpg 300w, https://www.appserv.org/wp-content/uploads/2019/10/S__3760135-1024x768-768x576.jpg 768w, https://www.appserv.org/wp-content/uploads/2019/10/S__3760135-1024x768-200x150.jpg 200w, https://www.appserv.org/wp-content/uploads/2019/10/S__3760135-1024x768-150x113.jpg 150w" width="475"/></p><h3 class="entry-title"><span style="background-color: #ffffff; color: #3a3a3a; font-family: Lato, sans-serif; font-size: 20px; font-weight: bold;">Course Description</span></h3><p style="margin-bottom: 1.6em; outline: 0px; color: #3a3a3a; font-family: Lato, sans-serif; background-color: #ffffff;"><span style="font-size: 16px;">Raspberry Pi 4 Workshop for you IoT with PHP CLI mode you can create your own Server and IoT at home easy to control your IoT device with the simple script from PHP.<br/></span><span style="font-style: inherit; font-weight: bold; font-size: 1.25rem;">Key concepts covered include:<br/></span><span style="font-style: inherit; font-weight: inherit; font-size: 16px;">– Raspberry Pi 4<br/></span><span style="font-style: inherit; font-weight: inherit; font-size: 16px;">– OS Install and Update<br/></span><span style="font-style: inherit; font-weight: inherit; font-size: 16px;">– Shell command<br/></span><span style="font-style: inherit; font-weight: inherit; font-size: 16px;">– Remote by VNC<br/></span><span style="font-style: inherit; font-weight: inherit; font-size: 16px;">– VNC Remote without monitor setting<br/></span><span style="font-style: inherit; font-weight: inherit; font-size: 16px;">– Create your own Server at home<br/></span><span style="font-style: inherit; font-weight: inherit; font-size: 16px;">– Improve security for your Raspberry Pi with SSL<br/></span><span style="font-style: inherit; font-weight: inherit; font-size: 16px;">– GPIO control by PHP CLI mode<br/></span><span style="font-style: inherit; font-weight: inherit; font-size: 16px;">– Make your home automation with PHP CLI mode</span></p><h3 style="font-size: 16px; margin-bottom: 1.6em; outline: 0px; color: #3a3a3a; font-family: Lato, sans-serif; background-color: #ffffff;"><span style="font-size: 16px;">If you don’t know about Raspberry Pi 4 or IoT device. This course can help you to know it. Raspberry Pi 4 is powerful hardware with low power. You can make your home automation with Raspberry Pi 4.</span></h3><p><span style="font-style: inherit; font-weight: bold; font-size: 1.25rem;">Requirements <br/></span><span style="font-style: inherit; font-weight: inherit;">– </span><span style="font-size: 16px;">Raspberry Pi 4 or 3 or 2 Model B</span><br/><span style="font-size: 16px;">– Windows, Mac or Linux computer</span><br/><span style="font-size: 16px;">– Card Reader for Micro SD Card</span></p><h3 class="entry-title"><span style="background-color: #ffffff; color: #3a3a3a; font-family: Lato, sans-serif; font-size: 20px; font-weight: bold;">Course Description</span></h3><p><span style="color: #3a3a3a; font-family: Lato, sans-serif; font-size: 20px; font-weight: bold;">Introduction to Raspberry Pi 4 Workshop<br/></span><span style="font-style: inherit; font-weight: inherit;">    – Raspberry Pi 4<br/>    – Prepare for Install Raspbian OS on Raspberry Pi<br/>    – Download Raspbian OS<br/>    – Flash Raspbian OS to MicroSD Card<br/>    – Boot up Raspberry Pi<br/>    – Setup Raspbian OS</span></p><p><span style="color: #3a3a3a; font-family: Lato, sans-serif; font-size: 20px; font-weight: bold;">Control Raspberry Pi 4 from shell and desktop<br/></span><span style="font-style: inherit; font-weight: inherit;">    – Check IP Address and config static IP<br/>    – Configure WIFI password<br/>    – Control Raspberry Pi with SSH<br/>    – Control Raspberry Pi with VNC<br/>    – Upgrade Firmware for Raspberry Pi 4<br/>    – Upgrade Raspbian OS<br/>    – Basic command line for Raspberry Pi<br/>    – Upload &amp; Download File via SSH from remote PC<br/>    – Download file with wget<br/>    – Synchronize file with Rsync</span></p><p><span style="color: #3a3a3a; font-family: Lato, sans-serif; font-size: 20px; font-weight: bold;">Control Raspberry Pi 4 from shell and desktop<br/></span><span style="font-style: inherit; font-weight: inherit;">    – Check IP Address and config static IP<br/>    – Configure WIFI password<br/>    – Control Raspberry Pi with SSH<br/>    – Control Raspberry Pi with VNC<br/>    – Upgrade Firmware for Raspberry Pi 4<br/>    – Upgrade Raspbian OS<br/>    – Basic command line for Raspberry Pi<br/>    – Upload &amp; Download File via SSH from remote PC<br/>    – Download file with wget<br/>    – Synchronize file with Rsync</span></p><p><span style="color: #3a3a3a; font-family: Lato, sans-serif; font-size: 20px; font-weight: bold;">Setting up your private server by Raspberry Pi 4<br/></span><span style="font-style: inherit; font-weight: inherit;">    – Install Web Server, Database Server with (Apache,PHP,MariaDB)<br/>    – Install Web File Manager<br/>    – Install SQLite Database<br/>    – File Sharing with Samba Server<br/>    – Build you own DNS Server<br/>    – Raspberry Pi Port Forward with SSH, HTTP, HTTPS, VNC (Port 22, 80, 443, 5900)<br/>    – Raspberry Pi with Free Dynamic DNS<br/>    – Raspberry Pi with your Own Domain<br/>    – Automatic Update IP to Dynamic DNS with Crontab<br/>    – Install Let’s Encrypt SSL on Raspberry Pi<br/>    – Apache Virtual Host For Multiple Domain and SSL<br/>    – Automatic Renew Let’s Encrypt SSL<br/>    – Install UniFi Network Controller on Raspberry Pi<br/>    – UniFi Network Controller with Domain and Let’s Encrypt SSL</span></p><p><span style="color: #3a3a3a; font-family: Lato, sans-serif; font-size: 20px; font-weight: bold;">Programming with PHP CLI mode and Web Server mode<br/></span>    – Upgrade WiringPi for Raspberry Pi 4<br/>    – GPIO on Raspberry Pi 4<br/>    – PHP CLI Accessing GPIO pins<br/>    – PHP CLI with Text File and SQLite Database<br/>    – PHP CLI &amp; Web Server Control relay switch<br/>    – PHP CLI &amp; Web Server Create your Web IP Camera monitoring<br/>    – Raspberry Pi with Free Dynamic DNS<br/>    – Raspberry Pi with your Own Domain<br/>    – Crontab for PHP CLI to Control your GPIO pins</p></div>
</div>
</div>
<div class="elementor-element elementor-element-e89969c elementor-widget elementor-widget-text-editor" data-element_type="widget" data-id="e89969c" data-widget_type="text-editor.default">
<div class="elementor-widget-container">
<div class="elementor-text-editor elementor-clearfix"><h3 class="entry-title" style="background-color: #ffffff; text-align: center;"><a href="https://course.appserv.org/courses/raspberrypi-4-workshop/"><span style="background-color: #ffffff; color: #3a3a3a; font-family: Lato, sans-serif; font-size: 40px; font-weight: bold;">Take This Course</span></a></h3><div><span style="background-color: #ffffff; color: #3a3a3a; font-family: Lato, sans-serif; font-size: 20px; font-weight: bold;"> </span></div></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
<div class="apss-social-share apss-theme-3 clearfix">
<div class="apss-facebook apss-single-icon">
<a href="https://www.facebook.com/sharer/sharer.php?u=https://www.appserv.org/en/" rel="nofollow" target="_blank" title="Share on Facebook">
<div class="apss-icon-block clearfix">
<i class="fa fa-facebook"></i>
<span class="apss-social-text">Share on Facebook</span>
<span class="apss-share">Share</span>
</div>
</a>
</div>
<div class="apss-twitter apss-single-icon">
<a href="https://twitter.com/intent/tweet?text=Home&amp;url=https%3A%2F%2Fwww.appserv.org%2Fen%2F&amp;" rel="nofollow" target="_blank" title="Share on Twitter">
<div class="apss-icon-block clearfix">
<i class="fa fa-twitter"></i>
<span class="apss-social-text">Share on Twitter</span><span class="apss-share">Tweet</span>
</div>
</a>
</div>
<div class="apss-pinterest apss-single-icon">
<a href="javascript:pinIt();" rel="nofollow" title="Share on Pinterest">
<div class="apss-icon-block clearfix">
<i class="fa fa-pinterest"></i>
<span class="apss-social-text">Share on Pinterest</span>
<span class="apss-share">Share</span>
</div>
</a>
</div>
<div class="apss-linkedin apss-single-icon">
<a href="http://www.linkedin.com/shareArticle?mini=true&amp;title=Home&amp;url=https://www.appserv.org/en/&amp;summary=AppServ+%3A+Apache+%2B+PHP+%2B+MySQL%0ASimple+package+for+programming%0A%0A+%09Quickly+and+easy+to+install+Apache%2C..." rel="nofollow" target="_blank" title="Share on LinkedIn">
<div class="apss-icon-block clearfix"><i class="fa fa-linkedin"></i>
<span class="apss-social-text">Share on LinkedIn</span>
<span class="apss-share">Share</span>
</div>
</a>
</div>
<div class="apss-digg apss-single-icon">
<a href="http://digg.com/submit?phase=2%20&amp;url=https://www.appserv.org/en/&amp;title=Home" rel="nofollow" target="_blank" title="Share on Digg">
<div class="apss-icon-block clearfix">
<i class="fa fa-digg"></i>
<span class="apss-social-text">Share on Digg</span>
<span class="apss-share">Share</span>
</div>
</a>
</div>
<div class="apss-print apss-single-icon">
<a href="javascript:void(0);" onclick="window.print(); return false;" rel="nofollow" title="Print">
<div class="apss-icon-block clearfix"><i class="fa fa-print"></i>
<span class="apss-social-text">Print</span>
<span class="apss-share">Print</span>
</div>
</a>
</div>
</div> </div><!-- .entry-content -->
</div><!-- #post-## -->
<div id="comments">
<p class="nocomments">Comments are closed.</p>
</div><!-- #comments -->
</div><!-- #content -->
</section><!-- #container -->
<div style="clear:both;"></div>
</div> <!-- #forbottom -->
</div><!-- #main -->
<footer id="footer" role="contentinfo">
<div id="colophon">
</div><!-- #colophon -->
<div id="footer2">
<div style="text-align:center;clear:both;padding-top:4px;">
<a href="https://www.appserv.org/en/" rel="home" title=" AppServ : Apache + PHP + MYSQL">
			 AppServ : Apache + PHP + MYSQL</a> | Powered by <a href="http://www.cryoutcreations.eu" target="_blank" title="Mantra Theme by Cryout Creations">Mantra</a> &amp; <a href="http://wordpress.org/" target="_blank" title="Semantic Personal Publishing Platform">  WordPress.		</a>
</div><!-- #site-info -->
<div class="socials" id="sfooter">
<a class="socialicons social-Facebook" href="#" rel="nofollow" target="_blank" title="Facebook"><img alt="Facebook" src="https://www.appserv.org/wp-content/themes/mantra/images/socials/Facebook.png"/></a>
<a class="socialicons social-Twitter" href="#" rel="nofollow" target="_blank" title="Twitter"><img alt="Twitter" src="https://www.appserv.org/wp-content/themes/mantra/images/socials/Twitter.png"/></a>
<a class="socialicons social-RSS" href="#" rel="nofollow" target="_blank" title="RSS"><img alt="RSS" src="https://www.appserv.org/wp-content/themes/mantra/images/socials/RSS.png"/></a></div>
</div><!-- #footer2 -->
</footer><!-- #footer -->
</div><!-- #wrapper -->
<link href="https://www.appserv.org/wp-content/themes/mantra/style-mobile.css?ver=2.6.0" id="mantra-mobile-css" media="all" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
/* <![CDATA[ */
var frontend_ajax_object = {"ajax_url":"https:\/\/www.appserv.org\/wp-admin\/admin-ajax.php","ajax_nonce":"6e1f6b7233"};
/* ]]> */
</script>
<script src="https://www.appserv.org/wp-content/plugins/accesspress-social-share/js/frontend.js?ver=4.4.8" type="text/javascript"></script>
<script src="https://www.appserv.org/wp-includes/js/comment-reply.min.js?ver=5.2.4" type="text/javascript"></script>
<script src="https://www.appserv.org/wp-includes/js/wp-embed.min.js?ver=5.2.4" type="text/javascript"></script>
<script src="https://www.appserv.org/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=2.7.5" type="text/javascript"></script>
<script src="https://www.appserv.org/wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js?ver=2.7.3" type="text/javascript"></script>
<script type="text/javascript">
var ElementorProFrontendConfig = {"ajaxurl":"https:\/\/www.appserv.org\/wp-admin\/admin-ajax.php","nonce":"8a7f394e91","shareButtonsNetworks":{"facebook":{"title":"Facebook","has_counter":true},"twitter":{"title":"Twitter"},"google":{"title":"Google+","has_counter":true},"linkedin":{"title":"LinkedIn","has_counter":true},"pinterest":{"title":"Pinterest","has_counter":true},"reddit":{"title":"Reddit","has_counter":true},"vk":{"title":"VK","has_counter":true},"odnoklassniki":{"title":"OK","has_counter":true},"tumblr":{"title":"Tumblr"},"delicious":{"title":"Delicious"},"digg":{"title":"Digg"},"skype":{"title":"Skype"},"stumbleupon":{"title":"StumbleUpon","has_counter":true},"telegram":{"title":"Telegram"},"pocket":{"title":"Pocket","has_counter":true},"xing":{"title":"XING","has_counter":true},"whatsapp":{"title":"WhatsApp"},"email":{"title":"Email"},"print":{"title":"Print"}},"facebook_sdk":{"lang":"en_US","app_id":""}};
</script>
<script src="https://www.appserv.org/wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=2.7.3" type="text/javascript"></script>
<script src="https://www.appserv.org/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://www.appserv.org/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.7.3" type="text/javascript"></script>
<script src="https://www.appserv.org/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2" type="text/javascript"></script>
<script src="https://www.appserv.org/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=4.4.6" type="text/javascript"></script>
<script type="text/javascript">
var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"2.7.5","urls":{"assets":"https:\/\/www.appserv.org\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"general":{"elementor_global_image_lightbox":"yes","elementor_enable_lightbox_in_editor":"yes"}},"post":{"id":171,"title":"Home","excerpt":""}};
</script>
<script src="https://www.appserv.org/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=2.7.5" type="text/javascript"></script>
<script type="text/javascript">(function() {
				var expirationDate = new Date();
				expirationDate.setTime( expirationDate.getTime() + 31536000 * 1000 );
				document.cookie = "pll_language=en; expires=" + expirationDate.toUTCString() + "; path=/";
			}());</script>
</body>
</html><!-- WP Fastest Cache file was created in 0.3077700138092 seconds, on 01-11-19 12:10:30 -->