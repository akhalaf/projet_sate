<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--[if IE 8]>            <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>            <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
<head>
<!-- Basic -->
<meta charset="utf-8"/>
<title>Durbieland</title>
<meta content="" name="description"/>
<meta content="" name="keywords"/>
<meta content="index,follow" name="robots"/>
<!-- Mobile Metas -->
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"/>
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/css/durbieland.css" rel="stylesheet" type="text/css"/>
<link href="/css/justifiedGallery.min.css" rel="stylesheet" type="text/css"/>
<link href="/admin/editor/css/editor.css" rel="stylesheet" type="text/css"/>
<link href="/css/swipebox.css" rel="stylesheet"/>
<!--[if IE]>
    <link rel="stylesheet" href="/css/ie.css">
<![endif]-->
<script charset="utf-8" src="/admin/js/jquery.min.js" type="text/javascript"></script>
<!-- jeditable -->
<script charset="utf-8" src="/admin/editor/jeditable/jquery.jeditable.js" type="text/javascript"></script>
<!-- FancyBox main JS and CSS files -->
<script src="/admin/editor/js/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
<link href="/admin/editor/js/fancybox/jquery.fancybox.css" media="screen" rel="stylesheet" type="text/css"/>
<script charset="utf-8" src="/admin/editor/js/editor.js" type="text/javascript"></script>
<script charset="utf-8" src="/admin/js/jquery.validate.min.js" type="text/javascript"></script>
<script charset="utf-8" src="/admin/editor/includes/activate-editor.js" type="text/javascript"></script>
<script src="/admin/js/nprogress.js" type="text/javascript"></script>
<script src="/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/jquery.justifiedGallery.min.js" type="text/javascript"></script>
<script src="/js/jquery.swipebox.js"></script>
<script src="/vendor/respond.js"></script>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript">
            var pageid = '2';
            
            $(document).ready(function() {
                jQuery("#gallery").justifiedGallery({
                    rowHeight : 300,
                    fixedHeight : false, 
                    captions : false, 
                    margins : 7,
                    sizeRangeSuffixes: {
                        'lt100':'_t', 
                        'lt240':'_m', 
                        'lt320':'_n', 
                        'lt500':'', 
                        'lt640':'_z', 
                        'lt1024':'_b'
                    }
                });
                
                $(".d6_wrapper").width('95%'); 
                $(".d6_calendartitle").hide();
            });
            
            function initialize() {
                var latlng = new google.maps.LatLng(-33.836464, 18.648171);
                var myLatlng = new google.maps.LatLng(-33.836464, 18.648171);
                
                var myOptions = {
                    zoom: 16,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    disableDefaultUI: true,
                    scrollwheel: false
                    //styles: styles
                };
                
                var map = new google.maps.Map(document.getElementById('map-canvas'), myOptions);
                
                //var myLatlng = new google.maps.LatLng(43.04476,-76.124331);
                
                var image = '/images/marker.png';
                var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                      title: 'Durbanville Primary'
                      //icon: image
                  });
                  
                // To add the marker to the map, call setMap();
                marker.setMap(map);
              }
            
        </script>
</head>
<body role="document" style="width:100%;">
<div class="container-fluid border" role="main">
<nav class="navbar navbar-inverse navbar-fixed-top">
<div class="container">
<div class="navbar-header">
<button aria-controls="navbar" aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<div class="logo"><a href="/">DURBANVILLE - ONS EIE / OUR OWN</a></div>
</div>
<div class="navbar-collapse collapse" id="navbar">
<ul class="nav navbar-nav">
<li><a href="/about-us">About</a></li><li><a href="/general">General</a></li><li><a href="/kurrikuler">Kurrikulêr</a></li><li><a href="/co-curricular">Co-Curricular</a></li><li><a href="/circulars">Circulars</a></li><li><a href="/gallery">Gallery</a></li><li><a href="/donateurs-en-borge">Donateurs &amp; Borge</a></li><li><a href="/kontak-ons">Kontak</a></li> </ul>
</div><!--/.nav-collapse -->
</div>
</nav>
<!-- Header Image -->
<!--<div class="banner " id="index_header" name="index_header"><img src="/upload/banner_top.jpg" alt="banner_top" />-->
<div class="hidden-xs"><img class="banner hidden-xs" src="/upload/Banners/Banner vir Home Page -Dec 19.jpg"/>
<div class="logo-banner hidden-xs"><img alt="Durbanville" src="/images/logo.png"/></div>
</div>
<div class="hidden-sm hidden-md hidden-lg" style="background-image:url('/upload/Banners/Banner vir Home Page -Dec 19.jpg');background-size:cover;">
<div class="hidden-sm hidden-md hidden-lg"><img alt="Durbanville" src="/images/logo.png" style="margin-top:50px;"/></div>
</div>
<div class="blue-back" style="padding:20px;">
<div class="row" id="home_cols">
<div class="col-md-2"></div>
<div class="col-md-2 center"><img src="/upload/Icon_Akademie.png"/></div>
<div class="col-md-2"><a href="/akademie/">AKADEMIE</a><br/><p>Ons is trots op ons kurrikulum, aangesien dit kindgerig, dinamies en aktueel is.</p><a href="/akademie/">lees meer...</a></div>
<div class="col-md-2 center"><img src="/upload/Icon_Durbies.png"/></div>
<div class="col-md-2"><a href="/durbies-gee-om/">DURBIES GEE OM!</a><br/><p>Durbieland raak graag betrokke by aktiwiteite en projekte waardeur ander se lewens verbeter kan word.</p><a href="/durbies-gee-om/">lees meer...</a></div>
<div class="col-md-2"></div>
</div>
<br/>
<div class="row" id="home_cols">
<div class="col-md-2"></div>
<div class="col-md-2 center"><img src="/upload/Icon_Kultuur.png"/></div>
<div class="col-md-2"><a href="/kultuur/">KULTUUR</a><br/><p>Music, Choir, Eisteddfod, Zoem-Zoem Club, Art Club, Sport</p><a href="/kultuur/">lees meer...</a></div>
<div class="col-md-2 center"><img src="/upload/Icon_Sport.png"/></div>
<div class="col-md-2"><a href="/sport/">SPORT</a><br/><p>Ons glo steeds in “’n gesonde liggaam huisves ‘n gesonde gees”.</p><a href="/sport/">lees meer...</a></div>
<div class="col-md-2"></div>
</div>
</div>
<div class="row">
</div>
<div class="grey-back">
<div class="row">
<div class="col-md-12">
<h1 class="" id="index_events_heading" name="index_events_heading">Kalender   |   Calendar</h1>
</div>
</div>
<div class="row">
<div class="col-md-12">
<script src="https://www.d6communicator.com/school/listings.js.php?school=84&amp;type=calendar&amp;max=15&amp;color=d0d0d0"></script>
</div>
</div>
</div>
<div class="white-back">
<div class="row">
<div class="col-md-12">
<h1 class="" id="index_gallery_heading" name="index_gallery_heading"></h1>
</div>
</div>
<section>
<div class="content">
<div class="justified-gallery" id="gallery"><a href="/gallery/" title=""><img src=""/></a></div> </div>
</section>
</div>
<footer class="footer" id="footer">
<!--<div class="container">
        <div class="row">
            <div class="col-md-3" style="float:right;">
                <h5 class="short">Contact Us</h5>
                <span class="phone">0219768115</span>
                <p class="short">Fax: 0219751706</p>
                <ul class="list icons list-unstyled push-top">
                    <li><i class="icon icon-map-marker"></i> <strong>Address:</strong> Weyers Street
Durbanville
7550</li>
                    <li><i class="icon icon-envelope"></i> <strong>Email:</strong> <a href="mailto:office@durbieland.com">office@durbieland.com</a></li>
                </ul>
                <div class="social-icons push-top" style="display:none;">
                    <ul class="social-icons">
                        <li class="facebook"><a href="http://www.facebook.com/" target="_blank" data-placement="bottom" rel="tooltip" title="Facebook">Facebook</a></li>
                        <li class="twitter"><a href="http://www.twitter.com/" target="_blank" data-placement="bottom" rel="tooltip" title="Twitter">Twitter</a></li>
                        <li class="linkedin"><a href="http://www.linkedin.com/" target="_blank" data-placement="bottom" rel="tooltip" title="Linkedin">Linkedin</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>-->
<div class="footer-copyright">
<div class="container">
<div class="row">
<div class="col-md-12 footer-banner">
                    Durbieland                    <p><span class="copyright">copyright © 2021. All rights reserved. </span></p>
</div>
</div>
<!--<div class="row">
                <div class="col-md-12">
                    <div style="float:right;margin-right:10px;margin-bottom:10px;">
                        <a href="http://www.ezcms.co.za" target=_blank><img src="/admin/images/powered-by-logo-white.png" alt="Powered by ezCMS" pagespeed_url_hash=1754467230 /></a>
                    </div>
                </div>
            </div>-->
</div>
</div>
</footer>
<script type="text/javascript">
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73671344-1', 'auto');
  ga('send', 'pageview');

</script>
</div>
<script>
        
           // google.maps.event.addDomListener(window, 'load', initialize);
        
        </script>
</body>
</html>
