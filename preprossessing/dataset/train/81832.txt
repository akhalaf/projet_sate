<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="Start your own affiliate program with Affiliatly, control, track and manage affiliates with ease" name="description"/>
<title>Affiliate tracking software for your store - Affiliatly</title>
<link href="css/bootstrap.css" rel="stylesheet"/>
<link href="https://static.affiliatly.com/css/affiliate/main.css" rel="stylesheet"/>
<link href="//fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet" type="text/css"/>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"/>
<!--[if lt IE 9]>
	  <script src="js/html5shiv.js"></script>
	  <script src="js/respond.min.js"></script>
	<![endif]-->
<link href="https://www.affiliatly.com/favicon.ico" rel="shortcut icon"/>
<!--<script src="js/pace.js"></script>-->
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,600" rel="stylesheet" type="text/css"/>
</head>
<body>
<!--<div class="preloader"></div>-->
<main class="masthead" id="top" role="main">
<div class="container">
<div class="logo">
<a href="https://www.affiliatly.com/">
					Affiliatly
				</a>
</div>
<h1>Affiliate tracking software for your e-commerce store</h1>
<div class="row">
<div class="col-md-6 col-sm-12 col-md-offset-3 subscribe">
<div class="col-md-5 col-sm-4 element-centered" style="width:100%">
<a class="btn btn-success btn-lg" href="https://www.affiliatly.com/subscribe-choose-framework.html">START NOW</a>
</div>
<span class="alertMsg" id="result"></span> </div>
</div>
<a class="scrollto" href="#explore">
<p>SCROLL DOWN TO EXPLORE</p>
<p class="scrollto--arrow"><img alt="scroll down arrow" src="https://www.affiliatly.com/images/affiliate/scroll_down.png"/></p>
</a>
</div>
</main>
<ul class="header_menu">
<li><a href="#explore">About</a><b>/</b></li>
<li><a href="https://www.affiliatly.com/subscribe-choose-framework.html">Subscribe</a><b>/</b></li>
<li><a href="https://www.affiliatly.com/contact.html">Contact</a><b>/</b></li>
<li><a href="https://www.affiliatly.com/faq.html">FAQ</a><b>/</b></li>
<li><a href="https://www.affiliatly.com/blog">Blog</a><b>/</b></li>
<li><a href="https://www.affiliatly.com/login.html">Login</a></li>
</ul>
<div class="container" id="explore">
<div class="section-title">
<h2>Affiliatly is easy to use affiliate tracking software</h2>
<h4>Control and track the whole program from your browser</h4>
</div>
<section class="row heroimg breath carousel-section">
<div class="col-md-12 text-center">
<div id="carousel-background">
<div class="carousel slide" data-ride="carousel" id="carousel-example-generic">
<!-- Wrapper for slides -->
<div class="carousel-inner">
<div class="item active">
<a href="https://static.affiliatly.com/images/affiliate/demo/dashboard.png" rel="prettyphoto[]">
<img alt="On your dashboard, you will see the main info about your affiliate program" height="468" src="https://static.affiliatly.com/images/affiliate/demo/dashboard_thumbnail.png" width="822"/>
<div class="carousel-caption">On your dashboard, you will see the main info about your affiliate program</div>
</a>
</div><div class="item ">
<a href="https://static.affiliatly.com/images/affiliate/demo/1.png" rel="prettyphoto[]">
<img alt="Your affiliate can see detailed stats for generated visits and their earnings" height="468" src="https://static.affiliatly.com/images/affiliate/demo/1_thumbnail.png" width="822"/>
<div class="carousel-caption">Your affiliate can see detailed stats for generated visits and their earnings</div>
</a>
</div><div class="item ">
<a href="https://static.affiliatly.com/images/affiliate/demo/settings.png" rel="prettyphoto[]">
<img alt="This is your settings page, from where you can control your affiliate panel" height="468" src="https://static.affiliatly.com/images/affiliate/demo/settings_thumbnail.png" width="822"/>
<div class="carousel-caption">This is your settings page, from where you can control your affiliate panel</div>
</a>
</div><div class="item ">
<a href="https://static.affiliatly.com/images/affiliate/demo/3.png" rel="prettyphoto[]">
<img alt="The Integration of the Affiliatly is just few lines of code, JS or PHP" height="468" src="https://static.affiliatly.com/images/affiliate/demo/3_thumbnail.png" width="822"/>
<div class="carousel-caption">The Integration of the Affiliatly is just few lines of code, JS or PHP</div>
</a>
</div><div class="item ">
<a href="https://static.affiliatly.com/images/affiliate/demo/aff_details.png" rel="prettyphoto[]">
<img alt="You can view and make changes to your affiliate's details " height="468" src="https://static.affiliatly.com/images/affiliate/demo/aff_details_thumbnail.png" width="822"/>
<div class="carousel-caption">You can view and make changes to your affiliate's details </div>
</a>
</div><div class="item ">
<a href="https://static.affiliatly.com/images/affiliate/demo/4.png" rel="prettyphoto[]">
<img alt="Your affiliate can choose minimum payment sum and input info for payments" height="468" src="https://static.affiliatly.com/images/affiliate/demo/4_thumbnail.png" width="822"/>
<div class="carousel-caption">Your affiliate can choose minimum payment sum and input info for payments</div>
</a>
</div><div class="item ">
<a href="https://static.affiliatly.com/images/affiliate/demo/5.png" rel="prettyphoto[]">
<img alt="You can pay the affiliate directly from your panel, after reviewing affiliate's stats" height="468" src="https://static.affiliatly.com/images/affiliate/demo/5_thumbnail.png" width="822"/>
<div class="carousel-caption">You can pay the affiliate directly from your panel, after reviewing affiliate's stats</div>
</a>
</div><div class="item ">
<a href="https://static.affiliatly.com/images/affiliate/demo/6.png" rel="prettyphoto[]">
<img alt="Viewing affilaite's stats and earnings for chosen period of time" height="468" src="https://static.affiliatly.com/images/affiliate/demo/6_thumbnail.png" width="822"/>
<div class="carousel-caption">Viewing affilaite's stats and earnings for chosen period of time</div>
</a>
</div><div class="item ">
<a href="https://static.affiliatly.com/images/affiliate/demo/7.png" rel="prettyphoto[]">
<img alt="You can upload banners for your affilaites to use" height="468" src="https://static.affiliatly.com/images/affiliate/demo/7_thumbnail.png" width="822"/>
<div class="carousel-caption">You can upload banners for your affilaites to use</div>
</a>
</div><div class="item ">
<a href="https://static.affiliatly.com/images/affiliate/demo/8.png" rel="prettyphoto[]">
<img alt="The affiliate can choose banner and the app will generate an HTML code for him" height="468" src="https://static.affiliatly.com/images/affiliate/demo/8_thumbnail.png" width="822"/>
<div class="carousel-caption">The affiliate can choose banner and the app will generate an HTML code for him</div>
</a>
</div><div class="item ">
<a href="https://static.affiliatly.com/images/affiliate/demo/9.png" rel="prettyphoto[]">
<img alt="The window wih generated HTML code with affiliate id" height="468" src="https://static.affiliatly.com/images/affiliate/demo/9_thumbnail.png" width="822"/>
<div class="carousel-caption">The window wih generated HTML code with affiliate id</div>
</a>
</div>
</div>
<!-- Controls -->
<a class="left carousel-control" data-slide="prev" href="#carousel-example-generic" role="button">
<i class="fa fa-angle-left fa-4x"></i>
</a>
<a class="right carousel-control" data-slide="next" href="#carousel-example-generic" role="button">
<i class="fa fa-angle-right fa-4x"></i>
</a>
</div>
</div>
</div>
</section>
<div class="section-title">
<h2>Boost your sales and get insights about your affiliates</h2>
<h4></h4>
</div>
<section class="row features">
<div class="col-sm-6 col-md-3">
<div class="thumbnail">
<i class="fa fa-cog"></i>
<div class="caption">
<h3>Extensive Settings</h3>
<p>Log as admin and control every aspect of the affiliate programme</p>
</div>
</div>
</div>
<div class="col-sm-6 col-md-3">
<div class="thumbnail">
<i class="fa fa-upload"></i>
<div class="caption">
<h3>Upload Banners</h3>
<p>Upload image banners for your affiliates to use in their promotions</p>
</div>
</div>
</div>
<div class="col-sm-6 col-md-3">
<div class="thumbnail">
<i class="fa fa-user"></i>
<div class="caption">
<h3>Manage Affiliates individually</h3>
<p>You can see the stats for each affiliate and manage their account</p>
</div>
</div>
</div>
<div class="col-sm-6 col-md-3">
<div class="thumbnail">
<i class="fa fa-smile-o"></i>
<div class="caption">
<h3>Easy to use</h3>
<p>Affiliatly can generate links for your affiliates, so they are immediately ready to go</p>
</div>
</div>
</div>
</section>
<section class="row features">
<div class="col-sm-6 col-md-3">
<div class="thumbnail">
<i class="fa fa-gift"></i>
<div class="caption">
<h3>Gift cards</h3>
<p>With Affiliatly you can pay your affiliates with gift cards for your store</p>
</div>
</div>
</div>
<div class="col-sm-6 col-md-3">
<div class="thumbnail">
<i class="fa fa-area-chart"></i>
<div class="caption">
<h3>Track activity</h3>
<p>Track the activity and performance of your affiliates</p>
</div>
</div>
</div>
<div class="col-sm-6 col-md-3">
<div class="thumbnail">
<i class="fa fa-line-chart"></i>
<div class="caption">
<h3>Up your sales</h3>
<p>Boost your traffic and generate more sales</p>
</div>
</div>
</div>
<div class="col-sm-6 col-md-3">
<div class="thumbnail">
<i class="fa fa-child"></i>
<div class="caption">
<h3>Do what you love</h3>
<p>Leave marketing to others and just do the things you love</p>
</div>
</div>
</div>
</section>
<div class="container">
<div class="section-title">
<h2>We integrate with</h2>
</div>
<section class="row features">
<div class="col-sm-6 col-md-4">
<div class="thumbnail">
<h5>Shopify</h5>
<img alt="Shopify" src="https://static.affiliatly.com/images/affiliate/frameworks/shopify.jpg" width="85"/>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="thumbnail">
<h5>BigCommerce</h5>
<img alt="BigCommerce" src="https://static.affiliatly.com/images/affiliate/frameworks/bigcommerce.png" width="85"/>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="thumbnail">
<h5>WooCommerce</h5>
<img alt="WooCommerce" src="https://static.affiliatly.com/images/affiliate/frameworks/woocommerce.png" width="100"/>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="thumbnail">
<h5>Storeden</h5>
<img alt="Storeden" src="https://static.affiliatly.com/images/affiliate/frameworks/storeden.png" width="72"/>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="thumbnail">
<h5>Jumpseller</h5>
<img alt="Jumpseller" src="https://static.affiliatly.com/images/affiliate/frameworks/jumpseller.png" width="64"/>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="thumbnail">
<h5>Magento</h5>
<img alt="Magento" src="https://static.affiliatly.com/images/affiliate/frameworks/magento.png" width="120"/>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="thumbnail">
<h5>3dcart</h5>
<img alt="3d Cart" src="https://static.affiliatly.com/images/affiliate/frameworks/3dcart_new.png" width="85"/>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="thumbnail">
<h5>CrateJoy</h5>
<img alt="CrateJoy" src="https://static.affiliatly.com/images/affiliate/frameworks/cratejoy.png" width="100"/>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="thumbnail">
<h5>SquareSpace</h5>
<img alt="SquareSpace" src="https://static.affiliatly.com/images/affiliate/frameworks/squarespace.png" width="90"/>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="thumbnail">
<h5>HikaShop</h5>
<img alt="HikaShop" src="https://static.affiliatly.com/images/affiliate/frameworks/hikashop.png" width="81"/>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="thumbnail">
<h5>Ecwid</h5>
<img alt="Ecwid" src="https://static.affiliatly.com/images/affiliate/frameworks/ecwid.png" width="74"/>
</div>
</div>
<div class="col-sm-6 col-md-4">
<div class="thumbnail">
<h5>Custom stores</h5>
<i class="fa fa-cart-plus"></i>
</div>
</div>
</section>
<p class="col-md-12 text-center h4">
				We do not require any kind of payment info from your side in order to start your free trial!
			</p>
<div class="col-md-12 col-xs-12 text-center" style="margin-bottom:40px;">
<a class="btn btn-primary btn-lg" href="https://www.affiliatly.com/subscribe-choose-framework.html?blbn=1">Create your affiliate program now</a>
</div>
<div class="section-title">
<h2>Our plans</h2>
<h4></h4>
</div>
<section class="row breath planpricing">
<div class="col-md-4">
<div class="pricing color1 plan-box">
<div class="planname">Starter</div>
<div class="price"> <span class="curr">$</span>16<span class="per">/MO</span></div>
<div class="billing"><strong>Up to 50 Affiliates</strong></div>
<div class="billing">Billed Monthly</div>
</div>
</div>
<div class="col-md-4">
<div class="pricing color2 plan-box">
<div class="planname">Advanced</div>
<div class="price"> <span class="curr">$</span>24<span class="per">/MO</span></div>
<div class="billing"><strong>Up to 200 Affiliates</strong></div>
<div class="billing">Billed Monthly</div>
</div>
</div>
<div class="col-md-4">
<div class="pricing color3 plan-box">
<div class="planname">Professional</div>
<div class="price"> <span class="curr">$</span>39<span class="per">/MO</span></div>
<div class="billing"><strong>Up to 500 Affiliates</strong></div>
<div class="billing">Billed Monthly</div>
</div>
</div>
<div class="col-md-4">
<div class="pricing color4 plan-box">
<div class="planname">Pro 1000</div>
<div class="price"> <span class="curr">$</span>59<span class="per">/MO</span></div>
<div class="billing"><strong>Up to 1000 Affiliates</strong></div>
<div class="billing">Billed Monthly</div>
</div>
</div>
<div class="col-md-4">
<div class="pricing color5 plan-box">
<div class="planname">Pro 2500</div>
<div class="price"> <span class="curr">$</span>79<span class="per">/MO</span></div>
<div class="billing"><strong>Up to 2500 Affiliates</strong></div>
<div class="billing">Billed Monthly</div>
</div>
</div>
<div class="col-md-4">
<div class="pricing color7 plan-box">
<div class="planname">Pro Unlimited</div>
<div class="price"> <span class="curr">$</span>129<span class="per">/MO</span></div>
<div class="billing"><strong>Unlimited Affiliates</strong></div>
<div class="billing">Billed Monthly</div>
</div>
</div>
</section><!-- /section planpricing -->
<p style="margin-top:-45px; margin-left:12px;">* prices are without tax</p>
<h4 class="text-center text-success">This is the total and final price of the plan, there are no other costs or charges.</h4>
<div class="section-title">
<h5>Are you an affiliate?</h5>
<h4>Register to the affiliate programs of our advertisers!</h4>
</div>
<div class="col-md-12 col-xs-12 text-center" style="margin-bottom:60px;">
<a class="btn btn-success btn-lg" href="https://www.affiliatly.com/affiliate-programs.html">Check some of our affiliate programs</a>
</div>
<div class="section-title">
<h5>Frequently Asked Questions</h5>
</div>
<section class="row faq breath">
<div class="col-md-6">
<h6>What is Affiliatly?</h6>
<p>Affiliatly is an affiliate tracking app i.e fully working affiliate program.</p>
<h6>How does the free trial work?</h6>
<p>Our 90 days trial is 100% free. We do not require your credit card credentials for the free trial. </p>
<h6>Can I switch plans later?</h6>
<p>Absolutely. You can switch between our paid plans, or cancel your account altogether, whenever you like. We will adjust any payments accordingly.</p>
</div>
<div class="col-md-6">
<h6>How is the integration with my site made?</h6>
<p>Affiliatly is officially supporting: <strong>Shopify, Bigcommerce, Magento, CrateJoy, SquareSpace, Ecwid, Storeden, Jumpseller, Hikashop and WooCommerce.</strong></p>
<p>Even if you have a website which is not using some of these frameworks, you can still integrate it with Affiliatly. We provide JavaScript and PHP integration codes for custom build e-commerce sites.</p>
<h6>I am not sure if my site is compatible?</h6>
<p>No problem, <a href="https://www.affiliatly.com/check_website.html">click here</a> to contact us and we will check your website</p>
</div>
<a class="col-xs-12 text-center h3" href="https://www.affiliatly.com/faq.html">click here for more questions and answers</a>
</section>
</div>
<main class="footercta" role="main">
<div class="container">
<h2>Try our <strong> 90 days free trial</strong> and be convinced<br/> about the quality of <strong>Affiliatly</strong></h2>
<div class="row">
<div class="col-md-12 breath text-center">
<a class="btn btn-success btn-lg gototop" href="https://www.affiliatly.com/subscribe-choose-framework.html">FREE TRIAL</a>
</div>
</div>
</div>
</main>
<div class="section-title">
<h2>Upcoming changes</h2>
<section class="row faq breath">
<div class="col-md-12 text-center">
<p>More changes are here to come, if you want to be notified when they come, please leave your e-mail.<br/>We won't send you spam</p>
<form action="" class="col-md-6 text-centered" id="newsletter_form" method="post">
<div class="form-group">
<input class="form-control" name="email" placeholder="Enter Email" type="text"/>
<span class="btn btn-primary" id="notify_button">Notify me</span>
</div>
</form>
</div>
</section>
</div>
<div class="section-title">
<h2>Socializing</h2>
</div>
<section class="row faq breath">
<div class="col-md-12 col-xs-12 text-center">
<p class="text-center">If you want to become our friend or just ask a question</p>
<p class="col-md-6 col-xs-6 text-right">
<a href="https://www.facebook.com/affiliatly"><i class="fa fa-facebook-square fa-5x" style="color:#4A6EA9"></i><br/></a>
</p>
<p class="col-md-6 col-xs-6 text-left">
<a href="https://twitter.com/affiliatly"><i class="fa fa-twitter-square fa-5x" style="color:#62C8F8"></i><br/></a>
</p>
</div>
</section>
<div class="container">
<section class="row breath">
<div class="col-md-12 footerlinks">
<ul class="footer_menu">
<li><a href="#">About</a><b>/</b></li>
<li><a href="https://www.affiliatly.com/subscribe-choose-framework.html">Subscribe</a><b>/</b></li>
<li><a href="https://www.affiliatly.com/contact.html">Contact</a><b>/</b></li>
<li><a href="https://www.affiliatly.com/af-101/affiliate.panel">Our Affiliate program</a><b>/</b></li>
<li><a href="https://www.affiliatly.com/login.html">Login</a><b>/</b></li>
<li><a href="https://www.affiliatly.com/affiliate-programs.html">Some of our client's programs</a></li>
</ul>
<ul class="footer_menu">
<li><a href="/legal-tos">Terms of Service</a><b>/</b></li>
<li><a href="/legal-privacy">Privacy Policy</a><b>/</b></li>
<li><a href="/legal-cookie-policy">Cookie Policy</a><b>/</b></li>
<li><a href="/legal-dpa">DPA</a></li>
</ul>
<p>© 2014 - 2021 Developed by Overcode.bg. All Rights Reserved</p>
</div>
</section>
</div>
</div>
<script src="https://static.affiliatly.com/js/jquery.js"></script>
<script src="https://static.affiliatly.com/js/bootstrap.min.js"></script>
<script src="https://static.affiliatly.com/js/easing.js"></script>
<script src="https://static.affiliatly.com/js/nicescroll.js"></script>
<script src="https://static.affiliatly.com/js/jquery.prettyPhoto.js" type="text/javascript"></script>
<link href="https://static.affiliatly.com/css/prettyPhoto.css" rel="stylesheet" type="text/css"/>
<script src="https://www.affiliatly.com/easy_affiliate.js" type="text/javascript"></script>
<script type="text/javascript">startTracking('AF-101');</script>
<img alt="footer image" height="1" src="https://www.affiliatly.com/footer_image.png?ac=af-101" width="100"/>
<script>
	$(function(){
		$("a[rel*=prettyphoto]").prettyPhoto();
		checkLegalCookie();
	})
</script>
<script src="https://www.affiliatly.com/functions.js?caller=homepage" type="text/javascript"></script>
<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			  ga('create', 'UA-25783041-8', 'auto');
			  ga('set', 'anonymizeIp', true);
			  ga('send', 'pageview');

			</script>
</body>
</html>