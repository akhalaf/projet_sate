<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html dir="ltr" version="XHTML+RDFa 1.0" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://www.w3.org/1999/xhtml/vocab">
<meta charset="utf-8"/>
<link href="https://www.ausy.fr/sites/all/themes/ausy/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" name="viewport"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<meta content="Drupal 7 (http://drupal.org)" name="generator"/>
<link href="https://www.ausy.fr/fr" rel="canonical"/>
<link href="https://www.ausy.fr/fr" rel="shortlink"/>
<title>ausy.fr</title>
<link href="https://www.ausy.fr/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.ausy.fr/sites/default/files/css/css_CsbbwPRiFRYX1cw6WnBAswtrwflNagNtgsJRBl8TxvA.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.ausy.fr/sites/default/files/css/css_sPthZu20sBsqEHRMIEoiHgnaaUNkaIqeqRlT6t5kq3A.css" media="all" rel="stylesheet" type="text/css"/>
<style media="all" type="text/css">
<!--/*--><![CDATA[/*><!--*/
#sliding-popup.sliding-popup-bottom,#sliding-popup.sliding-popup-bottom .eu-cookie-withdraw-banner,.eu-cookie-withdraw-tab{background:#0779bf;}#sliding-popup.sliding-popup-bottom.eu-cookie-withdraw-wrapper{background:transparent}#sliding-popup .popup-content #popup-text h1,#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text h3,#sliding-popup .popup-content #popup-text p,#sliding-popup label,#sliding-popup div,.eu-cookie-compliance-secondary-button,.eu-cookie-withdraw-tab{color:#fff !important;}.eu-cookie-withdraw-tab{border-color:#fff;}.eu-cookie-compliance-more-button{color:#fff !important;}

/*]]>*/-->
</style>
<link href="https://www.ausy.fr/sites/default/files/css/css_056qi9xIOHXYkx0imYnZ78fgehzqDAGUzH5P0cuIDFA.css" media="print" rel="stylesheet" type="text/css"/>
<link href="https://www.ausy.fr/sites/default/files/css/css_-v_-xAQSjVuH666B848gEc1xyRfNc3eoFY22W2f5m1E.css" media="all" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
document.cookie = 'adaptive_image=' + Math.max(screen.width, screen.height) + '; path=/';
//--><!]]>
</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
window.jQuery || document.write("<script src='/sites/all/modules/contrib/jquery_update/replace/jquery/3.5/jquery.min.js'>\x3C/script>")
//--><!]]>
</script>
<script src="https://www.ausy.fr/sites/default/files/js/js_kikHFzYtlaRgkHHweIMIXUw3pO-m5xkO9dP44jm1BrU.js" type="text/javascript"></script>
<script src="https://www.ausy.fr/sites/default/files/js/js_Sv1kdg-n6MVUxAZaBTlUzeDpyL8q-59DmazIsLxR2S8.js" type="text/javascript"></script>
<script src="https://www.ausy.fr/sites/default/files/js/js_4hLyxnNR9Cxvs5vuBwHXXCM6w1O7B3aYopKpeBqch3o.js" type="text/javascript"></script>
<script src="https://www.ausy.fr/sites/default/files/js/js_SIzS6wHEv1-F_SW3yZnbOGgeP73YL8NyoGYx4qdijJI.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-43322156-4", {"cookieDomain":"auto","cookieExpires":33696000});ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script src="https://www.ausy.fr/sites/default/files/js/js_n-MjzCiEhHsxKBLZdS0lXUDlNiuP5a84qsUKRGiyRbI.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"fr\/","ajaxPageState":{"theme":"ausy","theme_token":"3g5drj3NMXLe8L7-P_fA_KrQ28Tu3HddbEp_irUDksw","js":{"sites\/all\/modules\/features\/ausy_chatbot\/assets\/js\/facebook_chat_configuration.js":1,"0":1,"sites\/all\/modules\/contrib\/eu_cookie_compliance\/js\/eu_cookie_compliance.js":1,"1":1,"\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/3.5.1\/jquery.min.js":1,"2":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery-html-prefilter-3.5.0-backport.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/libraries\/balupton-history.js\/scripts\/bundled\/html4+html5\/jquery.history.js":1,"sites\/all\/modules\/contrib\/eu_cookie_compliance\/js\/jquery.cookie-1.4.1.min.js":1,"sites\/all\/modules\/features\/ausy_faciliti\/assets\/js\/faciliti.js":1,"sites\/all\/modules\/features\/ausy_financial_documents\/js\/ausy_financial_documents.feature.js":1,"sites\/all\/modules\/features\/ausy_implantation\/js\/ausy_implantation.js":1,"sites\/all\/modules\/contrib\/contentanalysis\/contentanalysis.js":1,"sites\/all\/modules\/contrib\/contentoptimizer\/contentoptimizer.js":1,"sites\/all\/modules\/custom\/ausy_config\/js\/cookie-consent.js":1,"public:\/\/languages\/fr_4uhLuL5LzIwgW_8s3y0LIny7JKDtqp8T8KetVf1VhQg.js":1,"sites\/all\/modules\/contrib\/google_analytics\/googleanalytics.js":1,"3":1,"sites\/all\/themes\/ausy\/js\/stacktable.js":1,"sites\/all\/themes\/ausy\/js\/enquire.js":1,"sites\/all\/themes\/ausy\/js\/stickyfill.min.js":1,"sites\/all\/themes\/ausy\/js\/base.min.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/contrib\/adaptive_image\/css\/adaptive-image.css":1,"sites\/all\/modules\/features\/ausy_admin\/css\/styles.css":1,"sites\/all\/modules\/contrib\/date\/date_api\/date.css":1,"sites\/all\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/modules\/contrib\/domain\/domain_nav\/domain_nav.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/contrib\/i18n_menu_overview\/css\/i18n_menu_overview.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/modules\/contrib\/media\/modules\/media_wysiwyg\/css\/media_wysiwyg.base.css":1,"sites\/all\/modules\/contrib\/ctools\/css\/ctools.css":1,"modules\/locale\/locale.css":1,"sites\/all\/modules\/contrib\/eu_cookie_compliance\/css\/eu_cookie_compliance.css":1,"0":1,"sites\/all\/themes\/ausy\/css\/print.css":1,"sites\/all\/themes\/ausy\/css\/theme.css":1,"sites\/all\/themes\/ausy\/css\/contrib\/stacktable.css":1}},"facetapi":{"view_args":{"news:latest":[]},"exposed_input":{"news:latest":[]},"view_path":{"news:latest":"news"},"view_dom_id":{"news:latest":"79cd9ab1327271556642e5fb031bd23b"}},"better_exposed_filters":{"views":{"news":{"displays":{"latest":{"filters":[]}}}}},"urlIsAjaxTrusted":{"\/fr\/careers\/jobs":true},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":0,"popup_clicking_confirmation":false,"popup_scrolling_confirmation":false,"popup_html_info":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003EPrivacy settings\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--categories\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EVeuillez accepter l\u2019utilisation des cookies afin que nous puissions am\u00e9liorer votre exp\u00e9rience utilisateur.\n\u003C\/h2\u003E              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003EJe souhaite en savoir plus\u003C\/button\u003E\n          \u003C\/div\u003E\n          \u003Cdiv id=\u0022eu-cookie-compliance-categories\u0022 class=\u0022eu-cookie-compliance-categories\u0022\u003E\n                  \u003Cdiv class=\u0022eu-cookie-compliance-category\u0022\u003E\n            \u003Cdiv\u003E\n              \u003Cinput type=\u0022checkbox\u0022 name=\u0022cookie-categories\u0022 id=\u0022cookie-category-required\u0022\n                     value=\u0022required\u0022 checked disabled\u003E\n              \u003Clabel for=\u0022cookie-category-required\u0022\u003EMinimum\u003C\/label\u003E\n            \u003C\/div\u003E\n                      \u003Cdiv class=\u0022eu-cookie-compliance-category-description\u0022\u003E\u003C\/div\u003E\n                  \u003C\/div\u003E\n                  \u003Cdiv class=\u0022eu-cookie-compliance-category\u0022\u003E\n            \u003Cdiv\u003E\n              \u003Cinput type=\u0022checkbox\u0022 name=\u0022cookie-categories\u0022 id=\u0022cookie-category-statistics\u0022\n                     value=\u0022statistics\u0022 \u003E\n              \u003Clabel for=\u0022cookie-category-statistics\u0022\u003EStatistiques\u003C\/label\u003E\n            \u003C\/div\u003E\n                      \u003Cdiv class=\u0022eu-cookie-compliance-category-description\u0022\u003E\u003C\/div\u003E\n                  \u003C\/div\u003E\n                          \u003Cdiv class=\u0022eu-cookie-compliance-categories-buttons\u0022\u003E\n            \u003Cbutton type=\u0022button\u0022\n                    class=\u0022eu-cookie-compliance-save-preferences-button\u0022\u003EEnregistrer les pr\u00e9f\u00e9rences\u003C\/button\u003E\n          \u003C\/div\u003E\n              \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022eu-cookie-compliance-has-categories\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-default-button\u0022\u003EAccepter tous les cookies\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button eu-cookie-compliance-hidden\u0022 \u003EWithdraw consent\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","use_mobile_message":false,"mobile_popup_html_info":"  \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003EPrivacy settings\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--categories\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n                    \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003EJe souhaite en savoir plus\u003C\/button\u003E\n          \u003C\/div\u003E\n          \u003Cdiv id=\u0022eu-cookie-compliance-categories\u0022 class=\u0022eu-cookie-compliance-categories\u0022\u003E\n                  \u003Cdiv class=\u0022eu-cookie-compliance-category\u0022\u003E\n            \u003Cdiv\u003E\n              \u003Cinput type=\u0022checkbox\u0022 name=\u0022cookie-categories\u0022 id=\u0022cookie-category-required\u0022\n                     value=\u0022required\u0022 checked disabled\u003E\n              \u003Clabel for=\u0022cookie-category-required\u0022\u003EMinimum\u003C\/label\u003E\n            \u003C\/div\u003E\n                      \u003Cdiv class=\u0022eu-cookie-compliance-category-description\u0022\u003E\u003C\/div\u003E\n                  \u003C\/div\u003E\n                  \u003Cdiv class=\u0022eu-cookie-compliance-category\u0022\u003E\n            \u003Cdiv\u003E\n              \u003Cinput type=\u0022checkbox\u0022 name=\u0022cookie-categories\u0022 id=\u0022cookie-category-statistics\u0022\n                     value=\u0022statistics\u0022 \u003E\n              \u003Clabel for=\u0022cookie-category-statistics\u0022\u003EStatistiques\u003C\/label\u003E\n            \u003C\/div\u003E\n                      \u003Cdiv class=\u0022eu-cookie-compliance-category-description\u0022\u003E\u003C\/div\u003E\n                  \u003C\/div\u003E\n                          \u003Cdiv class=\u0022eu-cookie-compliance-categories-buttons\u0022\u003E\n            \u003Cbutton type=\u0022button\u0022\n                    class=\u0022eu-cookie-compliance-save-preferences-button\u0022\u003EEnregistrer les pr\u00e9f\u00e9rences\u003C\/button\u003E\n          \u003C\/div\u003E\n              \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022eu-cookie-compliance-has-categories\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-default-button\u0022\u003EAccepter tous les cookies\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button eu-cookie-compliance-hidden\u0022 \u003EWithdraw consent\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","mobile_breakpoint":"768","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class=\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EThank you for accepting cookies\u003C\/h2\u003E\n\u003Cp\u003EYou can now hide this message or find out more about cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button eu-cookie-compliance-hide-button\u0022\u003EHide\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button-thank-you\u0022 \u003EMore info\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_use_bare_css":false,"popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"\/fr\/information-sur-les-cookies","popup_link_new_window":1,"popup_position":null,"fixed_top_position":1,"popup_language":"fr","store_consent":false,"better_support_for_screen_readers":0,"reload_page":0,"domain":"","domain_all_sites":0,"popup_eu_only_js":0,"cookie_lifetime":"100","cookie_session":false,"disagree_do_not_show_popup":0,"method":"categories","whitelisted_cookies":"required:has_js\r\nrequired:adaptive_image\r\nstatistics:1P_JAR\r\nstatistics:CONSENT\r\nstatistics:_ga\r\nstatistics:_gat\r\nstatistics:_gid\r\nstatistics:submitted_page_visit_log","withdraw_markup":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003EPrivacy settings\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-withdraw-banner\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EYou have given your consent for us to set cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button\u0022\u003EWithdraw consent\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","withdraw_enabled":1,"withdraw_button_on_info_popup":1,"cookie_categories":["required","statistics"],"enable_save_preferences_button":1,"fix_first_cookie_category":1,"select_all_categories_by_default":0,"cookie_name":""},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls|xml|z|zip"},"ausy_chatbot":{"facebook_app_id":"478691319134000"},"ausy":{"gaProperty":"UA-43322156-4"},"ausy_faciliti":{"userId":"e8849af0-01ff-11e9-bfb9-000c298ed446"}});
//--><!]]>
</script>
</head>
<body class="html front not-logged-in no-sidebars page-ausy page-ausy-front domain-ausy-fr i18n-fr">
<div id="skip-link">
<a class="element-invisible element-focusable" href="#main-content">Aller au contenu principal</a>
</div>
<div id="page">
<div id="header">
<div class="section clearfix">
<div class="left">
<a href="/fr" id="logo" rel="home" title="Accueil">
<img alt="Accueil" src="https://www.ausy.fr/sites/all/themes/ausy/logo.png"/>
</a>
</div>
<div class="right">
<div class="region region-header">
<div class="block block-ausy-faciliti" id="block-ausy-faciliti-faciliti-logo">
<div class="content">
<a href="https://ws.facil-iti.com" target="_blank"><img alt="Faciliti" src="sites/all/modules/features/ausy_faciliti/assets/media/logo.svg"/>
</a> </div>
</div>
<div class="block block-locale" id="block-locale-language">
<div class="content">
<ul class="language-switcher-locale-url"><li class="en first"><a class="language-link" href="/en" xml:lang="en">English</a></li>
<li class="fr last active"><a class="language-link active" href="/fr" xml:lang="fr">Français</a></li>
</ul> </div>
</div>
<div class="block block-menu-block" id="block-menu-block-1">
<div class="content">
<div class="menu-block-wrapper menu-block-1 menu-name-262e39d408dbbdbf15176076afe6cd68 parent-mlid-0 menu-level-1">
<ul class="menu"><li class="first expanded menu-mlid-4344"><a href="/fr/le-groupe/presentation" title="">Le Groupe</a><ul class="menu"><li class="first leaf menu-mlid-4517"><a href="/fr/le-groupe/historique" title="">Historique</a></li>
<li class="leaf menu-mlid-4518"><a href="/fr/le-groupe/strategie" title="">Stratégie</a></li>
<li class="leaf menu-mlid-4348"><a href="/fr/le-groupe/gouvernance" title="">Gouvernance</a></li>
<li class="leaf menu-mlid-4519"><a href="/fr/le-groupe/valeurs" title="">Valeurs</a></li>
<li class="leaf menu-mlid-4520"><a href="/fr/le-groupe/nos-preoccupations-rse" title="">Nos engagements RSE</a></li>
<li class="leaf menu-mlid-4935"><a href="/fr/nos-certifications" title="">Certifications</a></li>
<li class="last leaf menu-mlid-4351"><a href="/fr/le-groupe/implantations" title="">Implantations</a></li>
</ul></li>
<li class="expanded menu-mlid-4352"><a href="/fr/carriere/offre" title="">Carrière</a><ul class="menu"><li class="first leaf menu-mlid-4353"><a href="/fr/carriere/offre" title="">Offres d'emploi</a></li>
<li class="leaf menu-mlid-5330"><a href="/fr/alternances-et-stages" title="">Alternances et stages</a></li>
<li class="leaf menu-mlid-4551"><a href="/fr/carriere/candidature-spontanee" title="">Candidature spontanée</a></li>
<li class="leaf has-children menu-mlid-4355"><a href="/fr/carriere/rejoindre-ausy" title="">Rejoindre AUSY</a></li>
<li class="leaf has-children menu-mlid-4454"><a href="/fr/carriere/evolution-de-carriere" title="">Evolution de carrière</a></li>
<li class="last leaf menu-mlid-4363"><a href="/fr/carriere/temoignages" title="">Témoignages</a></li>
</ul></li>
<li class="expanded menu-mlid-4364"><a href="/fr/expertise/notre-approche" title="">Expertise</a><ul class="menu"><li class="first leaf menu-mlid-4365"><a href="/fr/expertise/notre-approche" title="">Notre approche</a></li>
<li class="leaf menu-mlid-4366"><a href="/fr/expertise/domaines-expertise" title="">Domaines d'expertise</a></li>
<li class="leaf menu-mlid-4367"><a href="/fr/expertise/secteurs-activite" title="">Secteurs d'activité</a></li>
<li class="leaf menu-mlid-4368"><a href="/fr/expertise/references-projets" title="">Références projets</a></li>
<li class="last leaf menu-mlid-4369"><a href="/fr/expertise/temoignages-experts" title="">Témoignages d'experts</a></li>
</ul></li>
<li class="last expanded menu-mlid-4377"><a href="/fr/actualites" title="">Actu'</a><ul class="menu"><li class="first leaf menu-mlid-4378"><a href="/fr/actualites" title="">Actualités</a></li>
<li class="leaf menu-mlid-5315"><a href="/fr/actualites-techniques" title="">Actualités techniques</a></li>
<li class="leaf menu-mlid-4379"><a href="/fr/actualit%C3%A9/%C3%A9v%C3%A9nements" title="">événements</a></li>
<li class="last leaf menu-mlid-4521"><a href="/fr/actualit%C3%A9/communiqu%C3%A9s-de-presse" title="">Communiqués de presse</a></li>
</ul></li>
</ul></div>
</div>
</div>
</div>
<div class="mobile-burger"></div>
</div>
</div>
<!-- /.section -->
</div>
<!-- /#header -->
<div class="sidebar-right column sidebar" id="mobile-sidebar-right">
<div class="section">
<div class="region region-mobile-sidebar-right">
<div class="block block-menu-block" id="block-menu-block-5">
<div class="content">
<div class="menu-block-wrapper menu-block-5 menu-name-262e39d408dbbdbf15176076afe6cd68 parent-mlid-0 menu-level-1">
<ul class="menu"><li class="first expanded menu-mlid-4344"><a href="/fr/le-groupe/presentation" title="">Le Groupe</a><ul class="menu"><li class="first leaf menu-mlid-4517"><a href="/fr/le-groupe/historique" title="">Historique</a></li>
<li class="leaf menu-mlid-4518"><a href="/fr/le-groupe/strategie" title="">Stratégie</a></li>
<li class="leaf menu-mlid-4348"><a href="/fr/le-groupe/gouvernance" title="">Gouvernance</a></li>
<li class="leaf menu-mlid-4519"><a href="/fr/le-groupe/valeurs" title="">Valeurs</a></li>
<li class="leaf menu-mlid-4520"><a href="/fr/le-groupe/nos-preoccupations-rse" title="">Nos engagements RSE</a></li>
<li class="leaf menu-mlid-4935"><a href="/fr/nos-certifications" title="">Certifications</a></li>
<li class="last leaf menu-mlid-4351"><a href="/fr/le-groupe/implantations" title="">Implantations</a></li>
</ul></li>
<li class="expanded menu-mlid-4352"><a href="/fr/carriere/offre" title="">Carrière</a><ul class="menu"><li class="first leaf menu-mlid-4353"><a href="/fr/carriere/offre" title="">Offres d'emploi</a></li>
<li class="leaf menu-mlid-5330"><a href="/fr/alternances-et-stages" title="">Alternances et stages</a></li>
<li class="leaf menu-mlid-4551"><a href="/fr/carriere/candidature-spontanee" title="">Candidature spontanée</a></li>
<li class="expanded menu-mlid-4355"><a href="/fr/carriere/rejoindre-ausy" title="">Rejoindre AUSY</a><ul class="menu"><li class="first leaf menu-mlid-4356"><a href="/fr/carriere/rejoindre-ausy/philosophie" title="">Philosophie</a></li>
<li class="leaf menu-mlid-4358"><a href="/fr/carriere/rejoindre-ausy/integration" title="">Intégration</a></li>
<li class="leaf menu-mlid-4359"><a href="/fr/carriere/rejoindre-ausy/processus-de-recrutement" title="">Processus de recrutement</a></li>
<li class="last leaf menu-mlid-4357"><a href="/fr/carriere/rejoindre-ausy/relations-ecoles" title="">Relations écoles</a></li>
</ul></li>
<li class="expanded menu-mlid-4454"><a href="/fr/carriere/evolution-de-carriere" title="">Evolution de carrière</a><ul class="menu"><li class="first leaf menu-mlid-4361"><a href="/fr/carriere/evolution-de-carriere/universite-ausy" title="">Université AUSY</a></li>
<li class="leaf menu-mlid-4362"><a href="/fr/carriere/evolution-de-carriere/plan-de-carriere" title="">Plan de carrière</a></li>
<li class="last leaf menu-mlid-4473"><a href="/fr/carriere/evolution-de-carriere/plan-de-carriere/mobilite" title="">Mobilité</a></li>
</ul></li>
<li class="last leaf menu-mlid-4363"><a href="/fr/carriere/temoignages" title="">Témoignages</a></li>
</ul></li>
<li class="expanded menu-mlid-4364"><a href="/fr/expertise/notre-approche" title="">Expertise</a><ul class="menu"><li class="first leaf menu-mlid-4365"><a href="/fr/expertise/notre-approche" title="">Notre approche</a></li>
<li class="leaf menu-mlid-4366"><a href="/fr/expertise/domaines-expertise" title="">Domaines d'expertise</a></li>
<li class="leaf menu-mlid-4367"><a href="/fr/expertise/secteurs-activite" title="">Secteurs d'activité</a></li>
<li class="leaf menu-mlid-4368"><a href="/fr/expertise/references-projets" title="">Références projets</a></li>
<li class="last leaf menu-mlid-4369"><a href="/fr/expertise/temoignages-experts" title="">Témoignages d'experts</a></li>
</ul></li>
<li class="last expanded menu-mlid-4377"><a href="/fr/actualites" title="">Actu'</a><ul class="menu"><li class="first leaf menu-mlid-4378"><a href="/fr/actualites" title="">Actualités</a></li>
<li class="leaf menu-mlid-5315"><a href="/fr/actualites-techniques" title="">Actualités techniques</a></li>
<li class="leaf menu-mlid-4379"><a href="/fr/actualit%C3%A9/%C3%A9v%C3%A9nements" title="">événements</a></li>
<li class="last leaf menu-mlid-4521"><a href="/fr/actualit%C3%A9/communiqu%C3%A9s-de-presse" title="">Communiqués de presse</a></li>
</ul></li>
</ul></div>
</div>
</div>
</div>
</div>
<!-- /.section -->
</div> <!-- #mobile_sidebar_right-first -->
<div class="sidebar-right" id="filters">
<div class="section filters">
<div class="close">
<a href="#close">Apply filters</a>
</div>
<div class="region region-filters">
<div class="block block-views" id="block-views-exp-jobs-overview">
<h2>Par mot clé</h2>
<div class="content">
<form accept-charset="UTF-8" action="/fr/careers/jobs" id="views-exposed-form-jobs-overview" method="get"><div><div class="views-exposed-form">
<div class="views-exposed-widgets clearfix">
<div class="views-exposed-widget views-widget-filter-search_api_views_fulltext" id="edit-query-wrapper">
<div class="views-widget">
<div class="form-item form-type-textfield form-item-query">
<input class="form-text" id="edit-query" maxlength="128" name="query" placeholder="Mot-clé" size="30" type="text" value=""/>
</div>
</div>
</div>
<div class="views-exposed-widget views-submit-button">
<input class="form-submit" id="edit-submit-jobs" name="" type="submit" value="Postuler"/> </div>
</div>
</div>
</div></form> </div>
</div>
</div>
</div> <!-- /.section -->
</div> <!-- #filters -->
<div id="main-wrapper">
<div class="clearfix" id="main">
<div class="column" id="content">
<div class="region region-homepage-banner">
<div class="block block-ausy-homepage-banner" id="block-ausy-homepage-banner-ausy-homepage-banner">
<div class="content">
<div class="homepage-banner">
<div class="homepage-banner-image-wrapper">
<img alt="" class="adaptive-image" onload="hompageBannerImageLoaded()" src="https://www.ausy.fr/sites/default/files/styles/homepage_banner/adaptive-image/public/homepage_banner_image/digital.jpg?itok=drWWvGXg"/> <div class="homepage-banner-tagline section">
<div class="homepage-banner-title">
</div>
</div>
</div>
<div class="homepage-banner-subtitle">
    Conseil et ingénierie en hautes technologies. Accélérez vos projets. Boostez votre carrière.  </div>
<div class="homepage-banner-strong-points section">
<ul>
<li class="strong-point">
<a href="https://www.ausy.fr/fr/expertise/domaines-expertise" title="Découvrez nos offres">
<div class="left">
<img alt="" height="50" src="https://www.ausy.fr/sites/default/files/styles/homepage_banner_strong_point/public/homepage_banner_image/icons/expertise.png?itok=XYSjTzBG" title="Expertises Métiers" width="50"/> </div>
<div class="right">
<div class="title">
                  Expertises Métiers                </div>
<div class="link-wrapper">
<div class="link">
                    Découvrez nos offres                  </div>
</div>
</div>
</a>
</li>
<li class="strong-point">
<a href="https://www.ausy.fr/fr/groupe/implantations" title="Proximité et accompagnement">
<div class="left">
<img alt="" height="50" src="https://www.ausy.fr/sites/default/files/styles/homepage_banner_strong_point/public/homepage_banner_image/icons/international.png?itok=grju7Hwm" title="International" width="50"/> </div>
<div class="right">
<div class="title">
                  International                </div>
<div class="link-wrapper">
<div class="link">
                    Proximité et accompagnement                  </div>
</div>
</div>
</a>
</li>
<li class="strong-point">
<a href="https://www.ausy.fr/fr/expertise/notre-approche" title="Gestion de projets d'envergure">
<div class="left">
<img alt="" height="50" src="https://www.ausy.fr/sites/default/files/styles/homepage_banner_strong_point/public/homepage_banner_image/icons/cl%C3%A9-en-main_0.png?itok=rGQM-roa" title="Projets Clé en main" width="50"/> </div>
<div class="right">
<div class="title">
                  Projets Clé en main                </div>
<div class="link-wrapper">
<div class="link">
                    Gestion de projets d'envergure                  </div>
</div>
</div>
</a>
</li>
<li class="strong-point">
<a href="https://www.ausy.fr/fr/evolution-de-carri%C3%A8re" title="AUSY accélérateur de carrière">
<div class="left">
<img alt="" height="50" src="https://www.ausy.fr/sites/default/files/styles/homepage_banner_strong_point/public/homepage_banner_image/icons/r%C3%A9v%C3%A9ler-les-talents_V2.png?itok=LedSTsaz" title="Révéler les talents" width="50"/> </div>
<div class="right">
<div class="title">
                  Révéler les talents                </div>
<div class="link-wrapper">
<div class="link">
                    AUSY accélérateur de carrière                  </div>
</div>
</div>
</a>
</li>
<li class="strong-point">
<a href="https://www.ausy.fr/fr/universit%C3%A9-ausy" title="Centre de formation agréé">
<div class="left">
<img alt="" height="50" src="https://www.ausy.fr/sites/default/files/styles/homepage_banner_strong_point/public/homepage_banner_image/icons/universit%C3%A9.png?itok=zbBDG2qo" title="Université AUSY" width="50"/> </div>
<div class="right">
<div class="title">
                  Université AUSY                </div>
<div class="link-wrapper">
<div class="link">
                    Centre de formation agréé                  </div>
</div>
</div>
</a>
</li>
<li class="strong-point">
<a href="https://www.ausy.fr/fr/carriere/emplois" title="Consultez nos offres d'emplois">
<div class="left">
<img alt="" height="50" src="https://www.ausy.fr/sites/default/files/styles/homepage_banner_strong_point/public/homepage_banner_image/icons/rejoignez-nous_0.png?itok=U4pbfjFc" title="Rejoignez-nous" width="50"/> </div>
<div class="right">
<div class="title">
                  Rejoignez-nous                </div>
<div class="link-wrapper">
<div class="link">
                    Consultez nos offres d'emplois                  </div>
</div>
</div>
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<a id="main-content"></a>
<div class="section">
<div class="tabs"></div>
<div class="region region-content">
<div class="block block-views" id="block-views-news-latest">
<h2>Dernières actualités</h2>
<div class="content">
<div class="view view-news view-id-news view-display-id-latest list-overview view-dom-id-79cd9ab1327271556642e5fb031bd23b">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first">
<div class="views-field views-field-field-news-image"> <div class="field-content"><a href="/fr/actualites-techniques/data-breach"><img alt="data-breach-ausy" class="adaptive-image" src="https://www.ausy.fr/sites/default/files/styles/news_latest_first/adaptive-image/public/news/images/photo_en-tete.png?itok=Jw0KS6T7" title="data-breach-ausy"/></a></div> </div>
<div class="views-field views-field-field-news-date"> <div class="field-content"><span class="date-display-single">21 Octobre 2020</span></div> </div>
<div class="views-field views-field-title"> <h2 class="field-content"><a href="/fr/actualites-techniques/data-breach">Télétravail, Bring Your Own Device, shadow IT, retour au bureau, êtes-vous prêts à gérer un Data Breach ?</a></h2> </div>
<div class="views-field views-field-field-news-summary"> <div class="field-content">Avec la pandémie en cours, une vague de cybercriminalité sans précédent est en train de submerger le monde entier, avec en particulier un risque majeur de vol, de corruption ou de détournement de données critiques, ce qu’on appelle un Data Breach… Pour faire le point sur cette situation inédite, OakLand Group, un acteur spécialisé expert de la Data Privacy et de la Data Protection et le pôle d’expertise en cybersécurité du groupe AUSY exposent leur vision.</div> </div>
<div class="views-field views-field-path"> <span class="field-content"><a href="https://www.ausy.fr/fr/actualites-techniques/data-breach">En savoir plus</a></span> </div> </div>
<div class="views-row views-row-2 views-row-even">
<div class="views-field views-field-field-news-image"> <div class="field-content"><a href="/fr/actualites-techniques/le-d%C3%A9roulement-du-pentest"><img alt="image_en_tete" class="adaptive-image" src="https://www.ausy.fr/sites/default/files/styles/news_latest/adaptive-image/public/news/images/image_en_tete_0.jpg?itok=v4zlUdBa" title="Pentest"/></a></div> </div>
<div class="views-field views-field-field-news-date"> <div class="field-content"><span class="date-display-single">24 Septembre 2020</span></div> </div>
<div class="views-field views-field-title"> <h2 class="field-content"><a href="/fr/actualites-techniques/le-d%C3%A9roulement-du-pentest">LE DÉROULEMENT DU PENTEST</a></h2> </div>
<div class="views-field views-field-field-news-summary"> <div class="field-content"></div> </div>
<div class="views-field views-field-path"> <span class="field-content"><a href="https://www.ausy.fr/fr/actualites-techniques/le-d%C3%A9roulement-du-pentest">En savoir plus</a></span> </div> </div>
<div class="views-row views-row-3 views-row-odd">
<div class="views-field views-field-field-news-image"> <div class="field-content"><a href="/fr/actualites/ausy-publie-sa-1ere-etude-sur-les-ingenieurs-et-leurs-aspirations"><img alt="Engineer Employer Brand AUSY" class="adaptive-image" src="https://www.ausy.fr/sites/default/files/styles/news_latest/adaptive-image/public/news/images/istock-586393098.jpg?itok=vt8P3F69" title="Engineer Employer Brand AUSY"/></a></div> </div>
<div class="views-field views-field-field-news-date"> <div class="field-content"><span class="date-display-single">18 Septembre 2020</span></div> </div>
<div class="views-field views-field-title"> <h2 class="field-content"><a href="/fr/actualites/ausy-publie-sa-1ere-etude-sur-les-ingenieurs-et-leurs-aspirations">AUSY publie sa 1ère étude sur les ingénieurs et leurs aspirations</a></h2> </div>
<div class="views-field views-field-field-news-summary"> <div class="field-content"></div> </div>
<div class="views-field views-field-path"> <span class="field-content"><a href="https://www.ausy.fr/fr/actualites/ausy-publie-sa-1ere-etude-sur-les-ingenieurs-et-leurs-aspirations">En savoir plus</a></span> </div> </div>
<div class="views-row views-row-4 views-row-even views-row-last">
<div class="views-field views-field-field-news-image"> <div class="field-content"><a href="/fr/actualites-techniques/ausy-sera-present-la-journee-francaise-des-tests-logiciels-en-2020"><img alt="JFTL-2020" class="adaptive-image" src="https://www.ausy.fr/sites/default/files/styles/news_latest/adaptive-image/public/news/images/resize_pubjftl-2020.jpg?itok=8Oi8GY9H" title="JFTL-2020"/></a></div> </div>
<div class="views-field views-field-field-news-date"> <div class="field-content"><span class="date-display-single">24 Août 2020</span></div> </div>
<div class="views-field views-field-title"> <h2 class="field-content"><a href="/fr/actualites-techniques/ausy-sera-present-la-journee-francaise-des-tests-logiciels-en-2020">AUSY sera présent à la Journée Française des Tests Logiciels en 2020 !</a></h2> </div>
<div class="views-field views-field-field-news-summary"> <div class="field-content"></div> </div>
<div class="views-field views-field-path"> <span class="field-content"><a href="https://www.ausy.fr/fr/actualites-techniques/ausy-sera-present-la-journee-francaise-des-tests-logiciels-en-2020">En savoir plus</a></span> </div> </div>
</div>
</div> </div>
</div>
</div>
</div>
<!-- /.section -->
</div>
<!-- /#content -->
<div class="content-bottom">
</div>
</div>
<!-- /#main -->
</div>
<!-- /#main-wrapper -->
<div class="tracking">
</div>
</div>
<div id="footer">
<div class="section">
<div class="region region-footer">
<div class="block block-menu" id="block-menu-menu-footer-menu-for-ausy-fr">
<div class="content">
<ul class="menu"><li class="first leaf"><a href="/fr/mentions-legales" title="">Mentions légales</a></li>
<li class="leaf"><a href="/fr/conditions-generales-dutilisation" title="">CGU</a></li>
<li class="leaf"><a href="/fr/politique-de-confidentialite-des-donnees" title="">Confidentialité</a></li>
<li class="leaf"><a href="/fr/actualit%C3%A9/communiqu%C3%A9s-de-presse" title="">Presse</a></li>
<li class="last leaf"><a href="/fr/formulaire-de-contact" title="">Contact</a></li>
</ul> </div>
</div>
<div class="block block-ausy-chatbot" id="block-ausy-chatbot-ausy-chatbot-script">
<div class="content">
<div class="fb-customerchat" logged_in_greeting="Candidatez sans CV avec Randy!" logged_out_greeting="Candidatez sans CV avec Randy!" page_id="470516270408436" ref="utm_source:website.utm_medium:fb-customerchat.utm_content:ausy" theme_color="#2275d9"></div> </div>
</div>
<div class="block block-ausy-config" id="block-ausy-config-social-media">
<h2><none></none></h2>
<div class="content">
<div class="item-list"><ul class="social-media" id="social-media"><li class="social-media-item social-media-item-facebook first" id="social-media-facebook"><a href="https://www.facebook.com/Groupe.AUSY" target="_blank" title="Visit Ausy on Facebook">Facebook</a></li>
<li class="social-media-item social-media-item-linkedin" id="social-media-linkedin"><a href="http://fr.linkedin.com/company/ausy" target="_blank" title="Visit Ausy on LinkedIn">LinkedIn</a></li>
<li class="social-media-item social-media-item-youtube last" id="social-media-youtube"><a href="https://www.youtube.com/user/GROUPEAUSY" target="_blank" title="Visit Ausy on YouTube">YouTube</a></li>
</ul></div> </div>
</div>
</div>
</div>
<!-- /.section -->
</div>
<!-- /#footer -->
<script src="https://www.ausy.fr/sites/default/files/js/js_PriE11FvOv8NLqzbaqaKrAHsYFatrhC-MNLxYtUpvYc.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var eu_cookie_compliance_cookie_name = "";
//--><!]]>
</script>
<script src="https://www.ausy.fr/sites/default/files/js/js_lftW2kcsGIBavVFjiwiKjGYxB9Ck-v-03eFVKrh4K9M.js" type="text/javascript"></script>
</body>
</html>
