<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>AHGconnect</title>
<link href="/assets/favicon.png" rel="Shortcut Icon"/>
<meta content="authenticity_token" name="csrf-param"/>
<meta content="m6elDcNFC5z1IbXivAQGjLGSJa0Rk+Y+O1rKXe4l/U2rJOvzWHG7nOu69Evj9wciOA95ctZbMTX4EmPhiakJ2A==" name="csrf-token"/>
<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js"></script>
<![endif]-->
<script data-turbolinks-track="reload" src="https://d2asywj2uy1cdp.cloudfront.net/assets/ahg-02aa5de0af833b8f8b086d75e314f1046e313a079dc035ce2c2cf9c8238cca3d.js"></script>
<link data-turbolinks-track="reload" href="https://d2asywj2uy1cdp.cloudfront.net/assets/ahg-connect-456e01c0ed961bcfbdc1bb4a9cc7a57d5fad6f2cae19a6e6e99427cfa6a5aae4.css" media="all" rel="stylesheet"/>
</head>
<body class="ahg-connect new" id="user_account_sessions_page">
<div id="container">
<div class="navbar navbar-fixed-top hidden-phone">
<div class="navbar-inner">
<div class="container">
<a class="brand" href="/"><img src="https://d2asywj2uy1cdp.cloudfront.net/assets/ahg-white-horizontal-fae0bc67c030944fa6923b4b3c970c4ab48f3a25d0b32ffafab7c2626bc4db1d.png"/>
<span id="troop-number">
 
</span>
</a></div>
</div>
</div>
<div class="navbar navbar-fixed-top visible-phone">
<div class="navbar-inner">
<div class="container">
<a class="brand" href="/"><img src="https://d2asywj2uy1cdp.cloudfront.net/assets/ahg-white-horizontal-fae0bc67c030944fa6923b4b3c970c4ab48f3a25d0b32ffafab7c2626bc4db1d.png"/>
<span id="troop-number"></span>
</a></div>
</div>
</div>
<div class="container" id="main">
<div class="modal hide fade" id="the_modal"></div>
<noscript>
<div class="alert alert-warning">
This site has features that require JavaScript. Follow these simple
instructions to
<a href="http://www.activatejavascript.org" target="_blank">enable JavaScript in your web browser.
</a></div>
</noscript>
<div>
</div>
<div class="span4 offset4">
<br/>
<br/>
<br/>
<div class="well text-center">
<h1>Log In</h1>
<br/>
<form accept-charset="UTF-8" action="/user_account_session" class="formtastic user_account_session" id="new_user_account_session" method="post" novalidate="novalidate"><input name="utf8" type="hidden" value="✓"/><input name="_method" type="hidden" value="put"/><input name="authenticity_token" type="hidden" value="5qf58aj/+buCAxSIBuUZO89GJgrKkqiSuIh1LhIBaAvWJLcPM8tJu5yYVSFZFhiVRtt61Q1af5l7wNySdY2cng=="/><fieldset class="inputs"><div class="string input required autofocus stringish form-group" id="user_account_session_login_input"><span class="form-wrapper"><input autofocus="autofocus" class="form-control" id="user_account_session_login" name="user_account_session[login]" placeholder="Email Address or Username" type="text"/></span></div>
<div class="password input required stringish form-group" id="user_account_session_password_input"><span class="form-wrapper"><input class="form-control" id="user_account_session_password" name="user_account_session[password]" placeholder="Password" type="password"/></span></div>
</fieldset><fieldset class="form-actions"><input class="btn btn-primary btn-block btn-large" data-disable-with="Log In" disable_with="Please wait..." id="user_account_session_submit_action" name="commit" type="submit" value="Log In"/>
</fieldset></form><a href="/password_resets/new">I forgot my password</a>
|
<a href="/forgot_user_names/new">I forgot my username</a>
</div>
</div>
</div>
</div>
<div class="navbar navbar-fixed-bottom">
<div class="centered">
AHGconnect is proudly powered by TroopTrack.com.
<a href="http://trooptrack.com/american_heritage_girls" target="_blank">Learn More</a>
</div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-10707450-2', 'ahgconnect.org');
  ga('send', 'pageview');

</script>
</body>
</html>
