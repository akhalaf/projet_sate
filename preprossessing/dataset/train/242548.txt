<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Page not found - Blue Ridge Crossing</title>
<!-- stylesheet & favicon -->
<link href="https://blueridgecrossing.com/wp-content/themes/cleaner/style.css" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Arimo" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Default" rel="stylesheet" type="text/css"/>
<style type="text/css">
body { font-family: Arial !important; }
h1,h2,h3,h4,h5,h6 {font-family: Arimo !important; }
#primary-nav li { font-family: Arimo  !important; }
</style><style type="text/css">
#primary-nav{ background: #013761 !important; }
</style>
<!-- wp_head -->
<!-- This site is optimized with the Yoast SEO plugin v15.2.1 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex, follow" name="robots"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"http://blueridgecrossing.com/#website","url":"http://blueridgecrossing.com/","name":"Blue Ridge Crossing","description":"Just another WordPress site","potentialAction":[{"@type":"SearchAction","target":"http://blueridgecrossing.com/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-US"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//ajax.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"wpemoji":"https:\/\/blueridgecrossing.com\/wp-includes\/js\/wp-emoji.js?ver=5.5.3","twemoji":"https:\/\/blueridgecrossing.com\/wp-includes\/js\/twemoji.js?ver=5.5.3"}};
			/**
 * @output wp-includes/js/wp-emoji-loader.js
 */

( function( window, document, settings ) {
	var src, ready, ii, tests;

	// Create a canvas element for testing native browser support of emoji.
	var canvas = document.createElement( 'canvas' );
	var context = canvas.getContext && canvas.getContext( '2d' );

	/**
	 * Checks if two sets of Emoji characters render the same visually.
	 *
	 * @since 4.9.0
	 *
	 * @private
	 *
	 * @param {number[]} set1 Set of Emoji character codes.
	 * @param {number[]} set2 Set of Emoji character codes.
	 *
	 * @return {boolean} True if the two sets render the same.
	 */
	function emojiSetsRenderIdentically( set1, set2 ) {
		var stringFromCharCode = String.fromCharCode;

		// Cleanup from previous test.
		context.clearRect( 0, 0, canvas.width, canvas.height );
		context.fillText( stringFromCharCode.apply( this, set1 ), 0, 0 );
		var rendered1 = canvas.toDataURL();

		// Cleanup from previous test.
		context.clearRect( 0, 0, canvas.width, canvas.height );
		context.fillText( stringFromCharCode.apply( this, set2 ), 0, 0 );
		var rendered2 = canvas.toDataURL();

		return rendered1 === rendered2;
	}

	/**
	 * Detects if the browser supports rendering emoji or flag emoji.
	 *
	 * Flag emoji are a single glyph made of two characters, so some browsers
	 * (notably, Firefox OS X) don't support them.
	 *
	 * @since 4.2.0
	 *
	 * @private
	 *
	 * @param {string} type Whether to test for support of "flag" or "emoji".
	 *
	 * @return {boolean} True if the browser can render emoji, false if it cannot.
	 */
	function browserSupportsEmoji( type ) {
		var isIdentical;

		if ( ! context || ! context.fillText ) {
			return false;
		}

		/*
		 * Chrome on OS X added native emoji rendering in M41. Unfortunately,
		 * it doesn't work when the font is bolder than 500 weight. So, we
		 * check for bold rendering support to avoid invisible emoji in Chrome.
		 */
		context.textBaseline = 'top';
		context.font = '600 32px Arial';

		switch ( type ) {
			case 'flag':
				/*
				 * Test for Transgender flag compatibility. This flag is shortlisted for the Emoji 13 spec,
				 * but has landed in Twemoji early, so we can add support for it, too.
				 *
				 * To test for support, we try to render it, and compare the rendering to how it would look if
				 * the browser doesn't render it correctly (white flag emoji + transgender symbol).
				 */
				isIdentical = emojiSetsRenderIdentically(
					[ 0x1F3F3, 0xFE0F, 0x200D, 0x26A7, 0xFE0F ],
					[ 0x1F3F3, 0xFE0F, 0x200B, 0x26A7, 0xFE0F ]
				);

				if ( isIdentical ) {
					return false;
				}

				/*
				 * Test for UN flag compatibility. This is the least supported of the letter locale flags,
				 * so gives us an easy test for full support.
				 *
				 * To test for support, we try to render it, and compare the rendering to how it would look if
				 * the browser doesn't render it correctly ([U] + [N]).
				 */
				isIdentical = emojiSetsRenderIdentically(
					[ 0xD83C, 0xDDFA, 0xD83C, 0xDDF3 ],
					[ 0xD83C, 0xDDFA, 0x200B, 0xD83C, 0xDDF3 ]
				);

				if ( isIdentical ) {
					return false;
				}

				/*
				 * Test for English flag compatibility. England is a country in the United Kingdom, it
				 * does not have a two letter locale code but rather an five letter sub-division code.
				 *
				 * To test for support, we try to render it, and compare the rendering to how it would look if
				 * the browser doesn't render it correctly (black flag emoji + [G] + [B] + [E] + [N] + [G]).
				 */
				isIdentical = emojiSetsRenderIdentically(
					[ 0xD83C, 0xDFF4, 0xDB40, 0xDC67, 0xDB40, 0xDC62, 0xDB40, 0xDC65, 0xDB40, 0xDC6E, 0xDB40, 0xDC67, 0xDB40, 0xDC7F ],
					[ 0xD83C, 0xDFF4, 0x200B, 0xDB40, 0xDC67, 0x200B, 0xDB40, 0xDC62, 0x200B, 0xDB40, 0xDC65, 0x200B, 0xDB40, 0xDC6E, 0x200B, 0xDB40, 0xDC67, 0x200B, 0xDB40, 0xDC7F ]
				);

				return ! isIdentical;
			case 'emoji':
				/*
				 * So easy, even a baby could do it!
				 *
				 *  To test for Emoji 13 support, try to render a new emoji: Man Feeding Baby.
				 *
				 * The Man Feeding Baby emoji is a ZWJ sequence combining 👨 Man, a Zero Width Joiner and 🍼 Baby Bottle.
				 *
				 * 0xD83D, 0xDC68 == Man emoji.
				 * 0x200D == Zero-Width Joiner (ZWJ) that links the two code points for the new emoji or
				 * 0x200B == Zero-Width Space (ZWS) that is rendered for clients not supporting the new emoji.
				 * 0xD83C, 0xDF7C == Baby Bottle.
				 *
				 * When updating this test for future Emoji releases, ensure that individual emoji that make up the
				 * sequence come from older emoji standards.
				 */
				isIdentical = emojiSetsRenderIdentically(
					[0xD83D, 0xDC68, 0x200D, 0xD83C, 0xDF7C],
					[0xD83D, 0xDC68, 0x200B, 0xD83C, 0xDF7C]
				);

				return ! isIdentical;
		}

		return false;
	}

	/**
	 * Adds a script to the head of the document.
	 *
	 * @ignore
	 *
	 * @since 4.2.0
	 *
	 * @param {Object} src The url where the script is located.
	 * @return {void}
	 */
	function addScript( src ) {
		var script = document.createElement( 'script' );

		script.src = src;
		script.defer = script.type = 'text/javascript';
		document.getElementsByTagName( 'head' )[0].appendChild( script );
	}

	tests = Array( 'flag', 'emoji' );

	settings.supports = {
		everything: true,
		everythingExceptFlag: true
	};

	/*
	 * Tests the browser support for flag emojis and other emojis, and adjusts the
	 * support settings accordingly.
	 */
	for( ii = 0; ii < tests.length; ii++ ) {
		settings.supports[ tests[ ii ] ] = browserSupportsEmoji( tests[ ii ] );

		settings.supports.everything = settings.supports.everything && settings.supports[ tests[ ii ] ];

		if ( 'flag' !== tests[ ii ] ) {
			settings.supports.everythingExceptFlag = settings.supports.everythingExceptFlag && settings.supports[ tests[ ii ] ];
		}
	}

	settings.supports.everythingExceptFlag = settings.supports.everythingExceptFlag && ! settings.supports.flag;

	// Sets DOMReady to false and assigns a ready function to settings.
	settings.DOMReady = false;
	settings.readyCallback = function() {
		settings.DOMReady = true;
	};

	// When the browser can not render everything we need to load a polyfill.
	if ( ! settings.supports.everything ) {
		ready = function() {
			settings.readyCallback();
		};

		/*
		 * Cross-browser version of adding a dom ready event.
		 */
		if ( document.addEventListener ) {
			document.addEventListener( 'DOMContentLoaded', ready, false );
			window.addEventListener( 'load', ready, false );
		} else {
			window.attachEvent( 'onload', ready );
			document.attachEvent( 'onreadystatechange', function() {
				if ( 'complete' === document.readyState ) {
					settings.readyCallback();
				}
			} );
		}

		src = settings.source || {};

		if ( src.concatemoji ) {
			addScript( src.concatemoji );
		} else if ( src.wpemoji && src.twemoji ) {
			addScript( src.twemoji );
			addScript( src.wpemoji );
		}
	}

} )( window, document, window._wpemojiSettings );
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://blueridgecrossing.com/wp-includes/css/dist/block-library/style.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blueridgecrossing.com/wp-content/plugins/social-media-widget/social_widget.css?ver=5.5.3" id="social-widget-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-js" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js?ver=1.4.2" type="text/javascript"></script>
<script id="custom-js" src="https://blueridgecrossing.com/wp-content/themes/cleaner/js/custom.js?ver=5.5.3" type="text/javascript"></script>
<link href="https://blueridgecrossing.com/wp-json/" rel="https://api.w.org/"/><style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><script>
(function() {
	(function (i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', 'https://google-analytics.com/analytics.js', 'ga');

	ga('create', 'UA-29600811-8', 'auto');
			ga('send', 'pageview');
	})();
</script>
</head>
<body class="error404">
<div id="wrap">
<div id="header">
<div id="header-logo">
<a href="https://blueridgecrossing.com/" rel="home" title="Blue Ridge Crossing">
<img alt="Blue Ridge Crossing" src="http://blueridgecrossing.com/wp-content/uploads/2012/02/bnr.logoTagline.jpg"/>
</a>
</div><!-- /header-logo -->
<div id="search-wrap">
<form action="https://blueridgecrossing.com/" id="searchbar" method="get">
<input id="search" name="s" onblur="if(this.value=='')this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue)this.value='';" size="16" type="text" value="search this site..."/>
<input id="searchsubmit" type="submit" value=""/>
</form>
</div><!-- /search-wrap -->
</div><!-- /header -->
<div class="clearfix" id="primary-nav">
<div class="menu-primary-container"><ul class="sf-menu" id="menu-primary"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-21" id="menu-item-21"><a href="http://blueridgecrossing.com/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17" id="menu-item-17"><a href="https://blueridgecrossing.com/our-stores/">Our Stores</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19" id="menu-item-19"><a href="https://blueridgecrossing.com/map/">Directory Map</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16" id="menu-item-16"><a href="https://blueridgecrossing.com/leasing/">Leasing</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15" id="menu-item-15"><a href="https://blueridgecrossing.com/history/">History</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14" id="menu-item-14"><a href="https://blueridgecrossing.com/contact/">Contact</a></li>
</ul></div> </div><!-- /primary-nav -->
<div id="full-page-wrap">
<h1 class="page-title" id="archive-title">404 Error Page!</h1>
<div class="post">
<p>Sorry the page you were looking for could not be retrieved. Try browsing the archives below</p>
<ul>
<li><a href="https://blueridgecrossing.com/2012/04/">April 2012</a></li>
</ul>
</div>
<!-- end post -->
</div><!-- /full-page-wrap -->
<div class="clear"></div>
<div id="footer-wrap">
<div class="sliding-effect" id="footer">
<div id="footer-widget-left">
<div class="footer-box"><h4>Contact Info</h4> <div class="textwidget"><strong> Address: </strong> <br/>
4240 Blue Ridge Boulevard <br/>
Suite 900<br/>
Kansas City, Missouri 64133<br/> <br/>
<strong>Phone: 816-454-1200</strong><br/>
<strong>Fax: 816-356-7605</strong>
</div>
</div> </div><!--/footer-widget-left -->
<div id="footer-widget-middle-left">
<div class="footer-box"><h4>Follow Us!</h4><div class="socialmedia-buttons smw_left"><a href="https://www.facebook.com/pages/Blue-Ridge-Crossing/283212908410404?ref=tn_tnmn" rel="nofollow" target="_blank"><img alt="Follow Us on Facebook" class="fade" height="32" src="https://blueridgecrossing.com/wp-content/plugins/social-media-widget/images/default/32/facebook.png" style="opacity: 0.8; -moz-opacity: 0.8;" title="Follow Us on Facebook" width="32"/></a><a href="http://blueridgecrossing.com/feed" rel="nofollow" target="_blank"><img alt="Follow Us on RSS" class="fade" height="32" src="https://blueridgecrossing.com/wp-content/plugins/social-media-widget/images/default/32/rss.png" style="opacity: 0.8; -moz-opacity: 0.8;" title="Follow Us on RSS" width="32"/></a></div></div> </div><!--/footer-widget-middle-left -->
<div id="footer-widget-middle-right">
<div class="footer-box"><h4>Stores</h4> <div class="textwidget"><ul class="tm-latest-updates">
<li><a href="http://blueridgecrossing.com/our-stores/#chipotle" tilte="Chipotle">Chipotle</a></li>
<li><a href="http://blueridgecrossing.com/our-stores/#starbucks" tilte="Starbucks">Starbucks</a></li>
<li><a href="http://blueridgecrossing.com/our-stores/#vintage" tilte="">Vintage Stock</a></li>
<li><a href="http://blueridgecrossing.com/our-stores/#lowes" tilte="Lowes">Lowes</a></li>
<li><a href="http://blueridgecrossing.com/our-stores/" tilte="Stores">See more</a></li>
</ul></div>
</div> </div><!--/footer-widget-middle-right -->
<div id="footer-widget-right">
<div class="footer-box"><h4>Interested in Leasing?</h4><link href="https://blueridgecrossing.com/wp-content/plugins/gravityforms/css/formreset.css?ver=1.9.6" id="gforms_reset_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blueridgecrossing.com/wp-content/plugins/gravityforms/css/formsmain.css?ver=1.9.6" id="gforms_formsmain_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blueridgecrossing.com/wp-content/plugins/gravityforms/css/readyclass.css?ver=1.9.6" id="gforms_ready_class_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blueridgecrossing.com/wp-content/plugins/gravityforms/css/browsers.css?ver=1.9.6" id="gforms_browsers_css-css" media="all" rel="stylesheet" type="text/css"/>
<div class="gf_browser_unknown gform_wrapper" id="gform_wrapper_1"><form action="/history.html" enctype="multipart/form-data" id="gform_1" method="post">
<div class="gform_body"><ul class="gform_fields top_label form_sublabel_below description_below" id="gform_fields_1"><li class="gfield field_sublabel_below field_description_below" id="field_1_1"><label class="gfield_label" for="input_1_1">Name</label><div class="ginput_container"><input class="medium" id="input_1_1" name="input_1" tabindex="1" type="text" value=""/></div></li><li class="gfield field_sublabel_below field_description_below" id="field_1_2"><label class="gfield_label" for="input_1_2">Email</label><div class="ginput_container">
<input class="medium" id="input_1_2" name="input_2" tabindex="2" type="text" value=""/>
</div></li>
</ul></div>
<div class="gform_footer top_label"> <input class="gform_button button" id="gform_submit_button_1" onclick='if(window["gf_submitting_1"]){return false;}  window["gf_submitting_1"]=true;  ' tabindex="3" type="submit" value="Submit"/>
<input class="gform_hidden" name="is_submit_1" type="hidden" value="1"/>
<input class="gform_hidden" name="gform_submit" type="hidden" value="1"/>
<input class="gform_hidden" name="gform_unique_id" type="hidden" value=""/>
<input class="gform_hidden" name="state_1" type="hidden" value="WyJbXSIsIjA5NzgzM2M5NWQxYTU3OTdjNjA0NWQ4ZWJjYTcyNWE0Il0="/>
<input class="gform_hidden" id="gform_target_page_number_1" name="gform_target_page_number_1" type="hidden" value="0"/>
<input class="gform_hidden" id="gform_source_page_number_1" name="gform_source_page_number_1" type="hidden" value="1"/>
<input name="gform_field_values" type="hidden" value=""/>
</div>
</form>
</div><script type="text/javascript"> jQuery(document).bind('gform_post_render', function(event, formId, currentPage){if(formId == 1) {} } );jQuery(document).bind('gform_post_conditional_logic', function(event, formId, fields, isInit){} );</script><script type="text/javascript"> jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [1, 1]) } ); </script></div> </div><!--/footer-right -->
<div class="clear"></div>
</div><!--/footer -->
</div><!-- /footer-wrap -->
<div id="footer-bottom">
<div id="footer-social">
<ul>
</ul>
<!-- END footer-social -->
</div><!-- /footer-social -->
<div id="back-to-top"><a href="#up">Back to top ↑</a></div><!-- /back-to-top -->
</div><!-- /footer-bottom -->
</div><!-- /wrap -->
<script id="wp-embed-js" src="https://blueridgecrossing.com/wp-includes/js/wp-embed.js?ver=5.5.3" type="text/javascript"></script>
<script id="my-ajax-request-js-extra" type="text/javascript">
/* <![CDATA[ */
var MyAjax = {"ajaxurl":"https:\/\/blueridgecrossing.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script id="my-ajax-request-js" src="http://173.247.255.108/~blueridg/wp-content/themes/cleaner/js/tilcustom.js?ver=5.5.3" type="text/javascript"></script>
</body>
</html>