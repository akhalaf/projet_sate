<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "42455",
      cRay: "6108516b69f81a6a",
      cHash: "bd876634b641419",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cucHJhY3RpY2FsY2xlYW4uY28udWsvYWEvZHJvcGJveDIwMTYvSG9tZSUwOSUwQQ==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "OtlrPylHk0fytAWfQGD0emJC7gnepj67/6XoDwRcNUvu9IG6oSrYuda5wxrG5Qa9TMdE3UJJPxT/OAY1Rm8xpxrXCOvJGJFw0rtII9I6KO0m/5CydB2s8mrYfynnRRlUbX+xtYHW1vhzKNc4G5MbR23D/wYiPBzunqa52ZH2Ctt7Qg6tOrdHfLwzFF0uwSsda1CHkgJxJxG8E3Ro5A2msB1INbekTot7CHy5UvotbwtGHFHZrImt2uAEhAw/lrNNFKHBlOXNn4RdCcKkG2gIF/f723PWAWf4TFCP/pD4EV7i/usupnVe+AMl1x8jdJtbYZWEFf5Na4WZA3YhjvsHkNd2tgfRLr6klJWewf8jQ++IE0W1BS6fuU/GpzFFzv/9/7hGb4RGJx4mItBPIxtos7rXgPDY9e/oK/H3fDuaBhOAymje5ZnP5a+Aw4gvRle6S8M5KiT5fLJxzdFgMKPIRUrbI4p3sBl6K7whsNcMtb2COFyGiBbk9uZhGx9TNsXr+3ywPerugWft6q/HLq4K4ZYLT8N+vtGgKAd3Ag+5X4BmlqOsJF3qW7AKR8ORXg8Md1+WyulhBa5DBgjwWct84NP/6Mir9YBbl/vg6UPmY8bwdJsjGRgtPbfMbilLA1q6YNWihXLFiYC2LptX/LDf5pPrNXEkwU0IYjwNfk5MF7nBNmuCPplLLzhuUOERLK1SSP4zUwUXrKN/zuBtHwY6FRNTC/ijaHkeQXB7UnYhqrB9e3sKk6xCIlhDJ87X1LSC",
        t: "MTYxMDQ2OTYxMy4zNTEwMDA=",
        m: "JJ6lpXSOWfEhXLS8R8J16OGzqWHdt73tvwE0dm8uAMY=",
        i1: "6UgWXuyXEWQwTCAeEbS16Q==",
        i2: "Vvvo3f/i5p42mwqc3vTXlg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "Zf3BDDUsVKsAu4u0Sn6VrXWVcyC6XMYqvHXl6yP8LE0=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.practicalclean.co.uk</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/aa/dropbox2016/Home%09%0A?__cf_chl_captcha_tk__=ced4a53ef83cac56b028856cb658cfdf3a42fe52-1610469613-0-ASy1OcwWGrXICtO0VWlchOJh-tzXbmpUiIgu9ojxxTGXIx0lzw7zUSJlZFiM9t86iNgdEz0fvdsMACDi-rRRtyCLZO-91eLAOgElWl1W76i2U36Kwbeol4G1DW2nxzDhDZe0PvRf3ppvS5lfCvSpIuHfqilmtW4hyF_F0Ft54mjz6rtdj8VSJxJlq0SjPhrDXiq2RnAyq2T3SHYRkYDDp2NPgh0i8tHKPSunxYt46p451zFGqHFgG3C6DXwhKmot8A1QazqBRO1Ga7b6jJxG2PeGgtzUvwgMUfCnMQJFDaSOKqnZh857D9iImYXR6hqSaFqSm9frg2TrVhYcAIa2bz75NTIoe6oU2yuhtq2HVWLl-4sbykqOCa9RtNDI32LM6WA4xfPDjgC_jCtgGyWs6eDc0aVeZq8gLoETM4OLDxzCaeRfU3NvB1F_itCc-yxWHXgQANRmKTaqeR9sbNNZwFz98A4cVbeYG-fUN9jfYwrRrNNYrc5IBuhGPEpoJjnUpqBAQNcJ9kIADZ0TlkGzykU_WfF-LD0TqftCrX9iQHPvVpzRwYfdR6ClGZ0BtySw8g" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="18bef26ed2508da8a1bca3c2bdbb9f64c898f6d9-1610469613-0-AfnTcqsXoCO8qmeUPEwv0Hwram548TOA3VGaiemLJl2IjX/5sUeUIdCV7G3NUU7kk4hsJykSGh8RMs1tNHBFwkaQ7GmrHRO4Wm5fwhJ8grCw62+UqqFQKjBfciflVP8O2CGCyA+/QEJ8Ud3s8BFucJzOnpqX5N3iHdOoYE5n8LE9kePKgAsSLxoz6bZS457uY1TTxrQ5jvVg4vFqVjcyf+PYguFOLiOmT7N7MzbI2035lAcPTfgnbqB0sCP/rpTmOz4yLdn2n986vqXnPEndRVwbRxOL8bJbneCPPfSUh27d1hRKIk6uXgXZn2zKQyuUuUirl7bPwqKDFY8zKdN1WgFkQe1+di4pSz/zk5f9jFuQlMw4rAnBJyk6WYtKayHv6lLoErD2X9IWW793GNBpXxSUeLJUuYj90ZHsx+qTCtb2I9zd5wX5KzKWIMXoP252+bdT722l6asuUH7MihNM3tWZYj49BEv4JnHiB3UIeGu6wfDHy5Ds0/8yur6dDpZ1xpttSRVgWNRLVRi8aTy+L4GHEeWtb582+XjAxqhJEoacqIuO+A9oY+tpP67s8Gf6qJTmgNSsWlmmAj3jGB1yLmjB61dfW9QsKV2boIZ+wABFUjs8bv7kDrvSQWk4OSdMbPCPc83akR4acP8H3obI7dcG+F6SZpYaHbkkCGW2mN2Em3ycz31synlrX4rBhdNsrYjgWKbPLVKCG3ODZLtGlrHx4xtALgrHcWh7tv+AgwnPAQxQXVOWu8zhLTl/OdVlh74dYob1GApmPVtRx4IeTWfRQ2g5fWcqjZFEBHaV8WalxdhXEvPTX2C6ZNEeuMNaiJANDl0lfY0adnLOCHzfuqOMZjV/aCxTg3KkNqjcrwB2M7AqYqiZm43OQsOCzNuyn/eYbxgWdvZIKoEdmouwt8zH9P4fvnl4gQCFroBbK9pyXihl4Vd3sZChcksiZSxpA43jOf/s5GDTRZjvTdaqURyCEWawRv+8WzYaRTou8XIS+Dd9W/dZpK3oGm9DgVFj3MVAcWakZcKCGurmGmnc+uzKC83GPK7bKqgq4w7Qk3Qm6xJEDctv1q7jRE5rEz/1rrpAp5fsAHDjsqRQCywPGEhPFZ4MrVKT8aNy2BoaRiRwcuukisNrkfEqqL4zZdxtORP9VNjzaZXvkR5Qh+vXZygS3lzbjXAhEemJb0hb3j1oh74D7x1bw8t1QZLAxWMTwbhkn6Gx9FrVQbTiwiiHGJLdvSKv/rycLRjgKPeSb3XBsdZ2U6jbVzJqoU+DGiGsFL/8WqmsnC6E1YqWZP1aNW4AFOwCmgZhrMt7FlEb0i1FSIxu2AuL7w1AT7Nm2ThMMA=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="faf07afc54bef17a2c6f20e6d4406a6a"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=6108516b69f81a6a')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<!-- <a href="https://thepaulrobinson.com/printing.php?doc_id=4">table</a> -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">6108516b69f81a6a</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
