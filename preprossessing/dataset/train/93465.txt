<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<title>I18324: Marie-Georgienne  BELANGER  (____ - ____)
</title>
<meta content="Marie-Georgienne  BELANGER  (____ - ____)" name="Description"/>
<meta content="Marie-Georgienne  BELANGER , GED2HTML, genealogy" name="Keywords"/>
<meta content="GED2HTML v3.5e-WIN95 (Sep 26 1998)" name="Generator"/>
<meta content="01/02/00 05:15:40 " name="Date"/>
</head>
<body>
<h2><a name="I18324"></a>Marie-Georgienne  BELANGER </h2>
<h3>____ - ____</h3>
<b>Family 1</b>
: <a href="../d0000/g0000064.html#I18321">Joseph-Alphonse  PINEAU </a>
<ul>
<li><em>MARRIAGE</em>: 14 Jul 1914, St-Anaclet, Rimouski, Quebec
</li></ul>
<br/>
<hr/>
<pre>
    __
 __|
|  |__
|
|--<a href="../d0003/g0000064.html#I18324">Marie-Georgienne BELANGER </a>
|
|   __
|__|
   |__
</pre>
<hr/>
<p><a href="../persons.html">INDEX</a></p>
<p></p>
<hr/>
<em>HTML created by <a href="http://www.gendex.com/ged2html/">GED2HTML v3.5e-WIN95 (Sep 26 1998)</a> on 01/02/00 05:15:40 </em>.  
<hr/>
<h2><a name="I12807"></a>Gaspard  CLOUTIER </h2>
<h3>____ - ____</h3>
<b>Father: </b> <a href="../d0001/g0000064.html#I12805">Francois  CLOUTIER </a><br/>
<b>Mother: </b> <a href="../d0002/g0000064.html#I12806">Olympe  CHOUINARD </a><br/>
<br/>
<b>Family 1</b>
: <a href="../d0004/g0000064.html#I12808">Maria  CARRIER </a>
<ul>
<li><em>MARRIAGE</em>: 5 Nov 1901, St-Jean-Port-Joli, Quebec, Canada
</li></ul>
<ol>
<li> <tt> </tt><a href="../d0019/g0000001.html#I12811">Germaine  CLOUTIER </a>
</li></ol>
<hr/>
<pre>
                      <a href="../d0027/g0000089.html#I12796">_Issac CLOUTIER ______</a>+
 <a href="../d0001/g0000064.html#I12805">_Francois CLOUTIER _</a>|
|                    |<a href="../d0028/g0000089.html#I12797">_Olivette ST. PIERRE _</a>+
|
|--<a href="../d0003/g0000064.html#I12807">Gaspard CLOUTIER </a>
|
|                     ______________________
|<a href="../d0002/g0000064.html#I12806">_Olympe CHOUINARD __</a>|
                     |______________________
</pre>
<hr/>
<p><a href="../persons.html">INDEX</a></p>
<p></p>
<hr/>
<em>HTML created by <a href="http://www.gendex.com/ged2html/">GED2HTML v3.5e-WIN95 (Sep 26 1998)</a> on 01/02/00 05:15:40 </em>.  
<hr/>
<h2><a name="I19389"></a>Victoire  GAGNON </h2>
<h3>____ - ____</h3>
<b>Family 1</b>
: <a href="../d0002/g0000064.html#I19388">Augustin  CARRIER </a>
<ul>
<li><em>MARRIAGE</em>: 22 Jan 1794
</li></ul>
<ol>
<li> <tt> </tt><a href="../d0001/g0000064.html#I19387">Victoire  CARRIER </a>
</li></ol>
<hr/>
<pre>
    __
 __|
|  |__
|
|--<a href="../d0003/g0000064.html#I19389">Victoire GAGNON </a>
|
|   __
|__|
   |__
</pre>
<hr/>
<p><a href="../persons.html">INDEX</a></p>
<p></p>
<hr/>
<em>HTML created by <a href="http://www.gendex.com/ged2html/">GED2HTML v3.5e-WIN95 (Sep 26 1998)</a> on 01/02/00 05:15:40 </em>.  
<hr/>
<h2><a name="I07676"></a>Jacques  LANGLOIS </h2>
<h3>4 Apr 1707 - ____</h3>
<ul>
<li><em>BIRTH</em>: 4 Apr 1707, Chateau-Richer, Quebec, Canada
</li></ul>
<b>Father: </b> <a href="../d0034/g0000054.html#I07596">Clement  LANGLOIS </a><br/>
<b>Mother: </b> <a href="../d0035/g0000054.html#I07597">Marie-Anne  PREVOST </a><br/>
<br/>
<hr/>
<pre>
                       <a href="../d0037/g0000078.html#I06530">_Jean LANGLOIS ________________</a>+
 <a href="../d0034/g0000054.html#I07596">_Clement LANGLOIS ___</a>|
|                     |<a href="../d0020/g0000017.html#I06529">_Charlotte-Francoise BELANGER _</a>+
|
|--<a href="../d0003/g0000064.html#I07676">Jacques LANGLOIS </a>
|
|                      _______________________________
|<a href="../d0035/g0000054.html#I07597">_Marie-Anne PREVOST _</a>|
                      |_______________________________
</pre>
<hr/>
<p><a href="../persons.html">INDEX</a></p>
<p></p>
<hr/>
<em>HTML created by <a href="http://www.gendex.com/ged2html/">GED2HTML v3.5e-WIN95 (Sep 26 1998)</a> on 01/02/00 05:15:40 </em>.  
<hr/>
<h2><a name="I36544"></a>Louis-Joseph  LEPAGE </h2>
<h3>____ - ____</h3>
<b>Father: </b> <a href="../d0016/g0000002.html#I17522">Joseph  LEPAGE </a><br/>
<b>Mother: </b> <a href="../d0036/g0000040.html#I17515">Claire  RACINE </a><br/>
<br/>
<b>Family 1</b>
: <a href="../d0004/g0000064.html#I36545">Elisabeth  JOLIN </a>
<ul>
<li><em>MARRIAGE</em>: 23 Apr 1743, St-Francois, Ile d'Orleans
</li></ul>
<ol>
<li> <tt>+</tt><a href="../d0038/g0000064.html#I36521">Pierre  LEPAGE </a>
</li></ol>
<hr/>
<pre>
                  <a href="../d0038/g0000008.html#I07486">_Louis LEPAGE ________</a>+
 <a href="../d0016/g0000002.html#I17522">_Joseph LEPAGE _</a>|
|                |<a href="../d0037/g0000008.html#I07485">_Sebastienne LOIGNON _</a>+
|
|--<a href="../d0003/g0000064.html#I36544">Louis-Joseph LEPAGE </a>
|
|                 <a href="../d0014/g0000003.html#I17505">_Francois RACINE _____</a>+
|<a href="../d0036/g0000040.html#I17515">_Claire RACINE _</a>|
                 |<a href="../d0015/g0000003.html#I17506">_Marie BAUCHER _______</a>
</pre>
<hr/>
<p><a href="../persons.html">INDEX</a></p>
<p></p>
<hr/>
<em>HTML created by <a href="http://www.gendex.com/ged2html/">GED2HTML v3.5e-WIN95 (Sep 26 1998)</a> on 01/02/00 05:15:40 </em>.  
<hr/>
<h2><a name="I24831"></a>Sylvain  LEVASSEUR </h2>
<h3>____ - ____</h3>
<b>Father: </b> <a href="../d0015/g0000064.html#I17859">Hubert  LEVASSEUR </a><br/>
<b>Mother: </b> <a href="../d0032/g0000025.html#I17860">Marie  PARENT </a><br/>
<br/>
<b>Family 1</b>
: <a href="../d0011/g0000064.html#I24839">Elisabeth  LAVOIE </a>
<ul>
<li><em>MARRIAGE</em>: 1 Mar 1859, Bic, Rimouski, Quebec
</li></ul>
<br/>
<hr/>
<pre>
                     <a href="../d0003/g0000038.html#I17570">_Magloire LEVASSEUR _</a>+
 <a href="../d0015/g0000064.html#I17859">_Hubert LEVASSEUR _</a>|
|                   |<a href="../d0042/g0000038.html#I17551">_Marguerite LEPAGE __</a>+
|
|--<a href="../d0003/g0000064.html#I24831">Sylvain LEVASSEUR </a>
|
|                    <a href="../d0033/g0000097.html#I32059">_Francois PARENT ____</a>+
|<a href="../d0032/g0000025.html#I17860">_Marie PARENT _____</a>|
                    |<a href="../d0007/g0000059.html#I32060">_Genevieve REHEL ____</a>
</pre>
<hr/>
<p><a href="../persons.html">INDEX</a></p>
<p></p>
<hr/>
<em>HTML created by <a href="http://www.gendex.com/ged2html/">GED2HTML v3.5e-WIN95 (Sep 26 1998)</a> on 01/02/00 05:15:40 </em>.  
<hr/>
<h2><a name="I25896"></a>Louise  MICHAUD </h2>
<h3>____ - ____</h3>
<b>Father: </b> <a href="../d0006/g0000064.html#I25899">Maxime  MICHAUD </a><br/>
<b>Mother: </b> <a href="../d0011/g0000014.html#I25900">Louise  MICHAUD </a><br/>
<br/>
<b>Family 1</b>
: <a href="../d0002/g0000064.html#I25895">Amable  DIONNE </a>
<ul>
<li><em>MARRIAGE</em>: 14 Jan 1856, St Louis de Kamouraska
</li></ul>
<ol>
<li> <tt> </tt><a href="../d0001/g0000064.html#I25894">Pierre  DIONNE </a>
</li></ol>
<hr/>
<pre>
                   __
 <a href="../d0006/g0000064.html#I25899">_Maxime MICHAUD _</a>|
|                 |__
|
|--<a href="../d0003/g0000064.html#I25896">Louise MICHAUD </a>
|
|                  __
|<a href="../d0011/g0000014.html#I25900">_Louise MICHAUD _</a>|
                  |__
</pre>
<hr/>
<p><a href="../persons.html">INDEX</a></p>
<p></p>
<hr/>
<em>HTML created by <a href="http://www.gendex.com/ged2html/">GED2HTML v3.5e-WIN95 (Sep 26 1998)</a> on 01/02/00 05:15:40 </em>.  
<hr/>
<h2><a name="I29560"></a>Marie  OUELLET </h2>
<h3>____ - ____</h3>
<b>Father: </b> <a href="../d0026/g0000046.html#I29389">Jeremie  OUELLET </a><br/>
<b>Mother: </b> <a href="../d0020/g0000046.html#I29383">Azelle  L'ITALIEN </a><br/>
<br/>
<b>Family 1</b>
: <a href="../d0004/g0000064.html#I29561">Bruno  LIZOTTE </a>
<ul>
<li><em>MARRIAGE</em>: 2 Mar 1886, Ste-Anne-de-La-Pocatiere, Kamouraska, Quebec
</li></ul>
<ol>
<li> <tt> </tt><a href="../d0005/g0000016.html#I40259">Amanda  LIZOTTE </a>
</li><li> <tt>+</tt><a href="../d0029/g0000035.html#I32943">Reine  LIZOTTE </a>
</li><li> <tt> </tt><a href="../d0039/g0000058.html#I40736">Leda  LIZOTTE </a>
</li></ol>
<hr/>
<pre>
                     <a href="../d0032/g0000053.html#I01504">_Zacharie OUELLET __</a>+
 <a href="../d0026/g0000046.html#I29389">_Jeremie OUELLET __</a>|
|                   |<a href="../d0030/g0000096.html#I01350">_Josette OUELLET ___</a>+
|
|--<a href="../d0003/g0000064.html#I29560">Marie OUELLET </a>
|
|                    <a href="../d0008/g0000053.html#I28462">_Prosper L'ITALIEN _</a>+
|<a href="../d0020/g0000046.html#I29383">_Azelle L'ITALIEN _</a>|
                    |<a href="../d0009/g0000053.html#I28463">_Arzelie PICARD ____</a>+
</pre>
<hr/>
<p><a href="../persons.html">INDEX</a></p>
<p></p>
<hr/>
<em>HTML created by <a href="http://www.gendex.com/ged2html/">GED2HTML v3.5e-WIN95 (Sep 26 1998)</a> on 01/02/00 05:15:40 </em>.  
<hr/>
<h2><a name="I25308"></a>Pamela  ST. PIERRE </h2>
<h3>____ - ____</h3>
<b>Father: </b> <a href="../d0026/g0000013.html#I25292">Benoni  ST. PIERRE </a><br/>
<b>Mother: </b> <a href="../d0028/g0000013.html#I25294">Elisabeth  SIROIS </a><br/>
<br/>
<b>Family 1</b>
: <a href="../d0022/g0000025.html#I25311">Joseph  GIBEAU </a>
<ul>
<li><em>MARRIAGE</em>: 18 Aug 1896, St-Epiphane
</li></ul>
<br/>
<hr/>
<pre>
                      <a href="../d0024/g0000013.html#I25290">_Marcel ST. PIERRE _</a>
 <a href="../d0026/g0000013.html#I25292">_Benoni ST. PIERRE _</a>|
|                    |<a href="../d0025/g0000013.html#I25291">_Rosalie MARQUIS ___</a>
|
|--<a href="../d0003/g0000064.html#I25308">Pamela ST. PIERRE </a>
|
|                     <a href="../d0034/g0000043.html#I27001">_Francois SIROIS ___</a>
|<a href="../d0028/g0000013.html#I25294">_Elisabeth SIROIS __</a>|
                     |<a href="../d0035/g0000043.html#I27002">_Elisabeth HUDON ___</a>
</pre>
<hr/>
<p><a href="../persons.html">INDEX</a></p>
<p></p>
<hr/>
<em>HTML created by <a href="http://www.gendex.com/ged2html/">GED2HTML v3.5e-WIN95 (Sep 26 1998)</a> on 01/02/00 05:15:40 </em>.  
<hr/>
</body>
</html>
