<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="csrf-token"/>
<meta content="dream-soft UG" name="generator"/>
<meta content="dream-soft UG" name="author"/>
<title>SocialPhishing</title>
<link rel="icon" src="https://www.accountprotection.org/img/socialshishing_landscape_new.png" type="image/png"/>
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css"/>
<link href="https://www.accountprotection.org/css/app.css" rel="stylesheet"/>
<link href="https://www.accountprotection.org/css/main.css" rel="stylesheet"/>
<link href="https://www.accountprotection.org/css/fontawesome.css" rel="stylesheet"/>
<link href="https://www.accountprotection.org/d3-geomap/d3-geomap.css" rel="stylesheet"/>
</head>
<body>
<div id="app">
<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
<div class="container-fluid">
<a class="navbar-brand" href="https://www.accountprotection.org">
<img alt="Logo" src="https://www.accountprotection.org/img/socialshishing_landscape_new.png" style="max-width:140px"/>
</a>
<button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
<!-- Left Side Of Navbar -->
<ul class="navbar-nav mr-auto">
</ul>
<!-- Right Side Of Navbar -->
<ul class="navbar-nav ml-auto" style="margin-right: 20px">
<!-- Authentication Links -->
<li class="nav-item">
<a class="nav-link" href="https://www.accountprotection.org/login">Anmelden</a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://www.accountprotection.org/register">Registrieren</a>
</li>
</ul>
</div>
</div>
</nav>
<main class="py-4">
<div class="modal fade" id="info_modal_success" role="dialog" tabindex="-1">
<div class="modal-dialog modal-dialog-centered " role="document">
<div class="modal-content" style="display: table">
<div class="modal-body info-box row">
<div class="col icon-feld badge-success">
<i class="fas fa-5x fa-check"></i>
</div>
<div class="col info-text-box" id="info_modal_success_content">
</div>
<div class="col info-close-feld">
<button aria-label="Close" class="close info-close-feld" data-dismiss="modal" type="button">
<span aria-hidden="true" class="info-close-feld">×</span>
</button>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="info_modal_danger" role="dialog" tabindex="-1">
<div class="modal-dialog modal-dialog-centered " role="document">
<div class="modal-content" style="display: table">
<div class="modal-body info-box row">
<div class="col icon-feld badge-danger">
<i class="fas fa-5x fa-times"></i>
</div>
<div class="col info-text-box" id="info_modal_danger_content">
</div>
<div class="col info-close-feld">
<button aria-label="Close" class="close info-close-feld" data-dismiss="modal" type="button">
<span aria-hidden="true" class="info-close-feld">×</span>
</button>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="info_modal_warning" role="dialog" tabindex="-1">
<div class="modal-dialog modal-dialog-centered " role="document">
<div class="modal-content" style="display: table">
<div class="modal-body info-box row">
<div class="col icon-feld badge-warning">
<i class="fas fa-5x fa-exclamation-circle"></i>
</div>
<div class="col info-text-box" id="info_modal_warning_content">
</div>
<div class="col info-close-feld">
<button aria-label="Close" class="close info-close-feld" data-dismiss="modal" type="button">
<span aria-hidden="true" class="info-close-feld">×</span>
</button>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="info_modal_info" role="dialog" tabindex="-1">
<div class="modal-dialog modal-dialog-centered " role="document">
<div class="modal-content" style="display: table">
<div class="modal-body info-box row">
<div class="col icon-feld badge-info">
<i class="fas fa-5x fa-info-circle"></i>
</div>
<div class="col info-text-box" id="info_modal_info_content">
</div>
<div class="col info-close-feld">
<button aria-label="Close" class="close info-close-feld" data-dismiss="modal" type="button">
<span aria-hidden="true" class="info-close-feld">×</span>
</button>
</div>
</div>
</div>
</div>
</div>
<!--info-modal-end -->
<div class="container">
<div class="row justify-content-center">
<div class="col-md-8">
<div class="card">
<div class="card-header">Ein Fehler ist aufgetreten</div>
<div class="card-body">
                  Die angeforderte Ressource konnte nicht gefunden werden. - HTTP Exception 404
                </div>
</div>
</div>
</div>
</div>
</main>
<div class="footer" id="footer">
<a href="https://www.socialphishing.de" target="_blank">

                Social Phishing Toolkit v4.Beta © 2021</a>
            (<a href="https://www.accountprotection.org/report" style="font-weight: bold">Feature Request/Bug Report Form</a>)
        </div>
</div>
<!--info-modals -->
<main class="py-4">
<div class="modal fade" id="info_modal_success" role="dialog" tabindex="-1">
<div class="modal-dialog modal-dialog-centered " role="document">
<div class="modal-content" style="display: table">
<div class="modal-body info-box row">
<div class="col icon-feld badge-success">
<i class="fas fa-5x fa-check"></i>
</div>
<div class="col info-text-box" id="info_modal_success_content">
</div>
<div class="col info-close-feld">
<button aria-label="Close" class="close info-close-feld" data-dismiss="modal" type="button">
<span aria-hidden="true" class="info-close-feld">×</span>
</button>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="info_modal_danger" role="dialog" tabindex="-1">
<div class="modal-dialog modal-dialog-centered " role="document">
<div class="modal-content" style="display: table">
<div class="modal-body info-box row">
<div class="col icon-feld badge-danger">
<i class="fas fa-5x fa-times"></i>
</div>
<div class="col info-text-box" id="info_modal_danger_content">
</div>
<div class="col info-close-feld">
<button aria-label="Close" class="close info-close-feld" data-dismiss="modal" type="button">
<span aria-hidden="true" class="info-close-feld">×</span>
</button>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="info_modal_warning" role="dialog" tabindex="-1">
<div class="modal-dialog modal-dialog-centered " role="document">
<div class="modal-content" style="display: table">
<div class="modal-body info-box row">
<div class="col icon-feld badge-warning">
<i class="fas fa-5x fa-exclamation-circle"></i>
</div>
<div class="col info-text-box" id="info_modal_warning_content">
</div>
<div class="col info-close-feld">
<button aria-label="Close" class="close info-close-feld" data-dismiss="modal" type="button">
<span aria-hidden="true" class="info-close-feld">×</span>
</button>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="info_modal_info" role="dialog" tabindex="-1">
<div class="modal-dialog modal-dialog-centered " role="document">
<div class="modal-content" style="display: table">
<div class="modal-body info-box row">
<div class="col icon-feld badge-info">
<i class="fas fa-5x fa-info-circle"></i>
</div>
<div class="col info-text-box" id="info_modal_info_content">
</div>
<div class="col info-close-feld">
<button aria-label="Close" class="close info-close-feld" data-dismiss="modal" type="button">
<span aria-hidden="true" class="info-close-feld">×</span>
</button>
</div>
</div>
</div>
</div>
</div><!--spinner-modal -->
<div class="modal fade" id="spinner-modal" role="dialog" tabindex="-1">
<div class="modal-dialog modal-dialog-centered" role="document">
<div class="modal-content">
<div class="modal-body" style="text-align: center;">
<i class="fa fa-spinner fa-spin fa-5x"></i>
</div>
</div>
</div>
</div>
<script src="https://www.accountprotection.org/js/app.js"></script>
<script src="https://www.accountprotection.org/js/swal.js"></script>
<script src="https://www.accountprotection.org/js/main.js"></script>
<script src="https://www.accountprotection.org/js/check_submit.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        var session = '';

        if (session) {
            $('#info_modal_success').modal('show');
        }
        var session = '';
        if (session) {
            $('#info_modal_danger').modal('show');
        }
        var session = '';
        if (session) {
            $('#info_modal_warning').modal('show');
        }
        var session = '';
        if (session) {
            $('#info_modal_info').modal('show');
        }


    });
    var lang_swal = {
        title: "Achtung!",
        text: "Möchten sie den Inhalt wirklich löschen?",
        no: "nein",
        yes: "ja",
    }
    var tabSelect = function (input) {
        var href = input.attr('href')
        href = href.replace('#', '');
        $('#tabselect').val(href);
    }
</script>
</main></body>
</html>
