<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "92806",
      cRay: "6110b0fc19be1a42",
      cHash: "2db95f0e8874ad1",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmVxYWRpdmVycy5jb20v",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "ueJA9aAIzWJcas2RZtOjwIQ+0S6EKL2jJ2So0i05xTA43DIB/unSc2n2OUugZIphCU2OiOPL8RQqbzXneJOhZktkGsA5DL3gU65FheWDbYlKWuLKSHJzfhIKfWf6TXhP+BQnuW0Wdt14eKKq6qOXBLHWc3KcD4AOQ7h/Jqjo8hD2xupjKEJZPhihng5hEzRh13YcyF/3qP06lnBQxotA1oXacEX0EOXXg+/ZP22qj1v8zS6oDfAhwt9krGerwrrFDZvOiSwglNLvOv9bzq3PelnldCdTFAHuAXWSppHljNmuZyyz/KtI4JPiMlFSsftZDzljFueY1W3aYXezlcGk2bRgyj63Jg8FP8AyAJygH54hB+hhz50s5da2vpXRY0BQzN1iRhUBVDDyMaCI3EhauShWPqYolFHZPKE/+GdbK8FfsazJpC+NA23V5oXKRVg97Y352vpyQvTAngI6j1Yh/h0qw7leu6QX7Sifhezw+lMDDFpq6nonHPSymOZBhkX9gPMpF1RyruZZYMIlP+Lg9JUHNXcYfs53mapCmyjKusdm+qqDrFB1OqQSodDWajpXxVwoUNuEq5uMdhTjPtXPO2am6alD1pnLcrofDbMYUHJffSuH3Te4gDBHTNuL4HUV7ohEzvv2uvp4NFzyhAAZGyxzQt0T7QiFfusuDXgJfWmjhMW+8P3/EEKZO+EHpVz8ZwIxwf03YZy98AdagXPWNv6EL9ollyrZeU6vpS2/hisk0mFuoW6YR+HQ1PEzBITA",
        t: "MTYxMDU1NzQxMy43ODAwMDA=",
        m: "3NnJQLKzLP7mxo5PiR0HsCfNXU99It24zH8e0Jfwo6A=",
        i1: "w6VdDI7Y5l6vOXRnj2y47Q==",
        i2: "5fp5z+D/T5BFShqfbcHBNQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "SLVK7CSrrpU1aouOA/34NctGh+yn0pEDY4j/mCnKwQY=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.beqadivers.com</h2>
</div><!-- /.header -->
<a href="https://abusesurvey.org/shareholder.php?cfm=877"><span style="display: none;">table</span></a>
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=73d8441801da2a0b1eb6a2c7c77bb784af71d06a-1610557413-0-ATYlVChzGBtZVj3Ww0zoVIG1stiMbSnCUKdtTUdBQQg7escL9BNbZlSuX_kYsHdxU_yrTKZCyVSq5ZpWe112AbYV5v7rngqJ0-g9R-dAfRr6fx3QzcNSR4AqKpULHm9fvcDPZXH4V6f3M0ofSleYzfxZvnmI85p1pVkcJsZhydPoauGaIvqbv2DjMpSAgK1Xu3syxJNr_2nSLRy1ZQzjhZKFD_gR1IRedOCt62COgHbdE8axwMO7blffB6IZQttRTd54VVN6DR0zMMon56u5IYLf5zkc8yin996-FU6S-vclxXOlqBpqMU0Jf5U6YDYYG00351TmAhp-9mJN-AmFCQs9UPKqh1qlR_SC1YX_Unv703K1tKtVJjUJafqlxO1kdtYSBAUmRaviNSJUqD26W98rcKvv3CQlTndidBg5szwKl_IJW6Tz7ns__5e149TW1kKJoYNXYN_ZouhfxMNfaLhOVqha4JHSEuOI8Q249vWK7S-0eopdmJT-9yNLjUsPf19FDmy0RH8XI9dqHMsLcsU" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="ae6fe4e60542f75d973a0c579cf5cdf093c4fbd3-1610557413-0-Aah6sSEBjF1RvSPFry387QSjsA+ZWciuP5KQ+2vxDzl9Xl6umXQOHlWXG/xcdn9SwuG2yN03wtIXo6s32axr4bub2D1HS5yxUH3IcGRn4j5UMb0k1f6J7v5tG9sQjCw2Nhabi2JCKmUcvN4YWTIMbHLbWUjhs8ph27HN7K4SMIjxvoB5YySSFb2jwc4WP9H9VChY+cD3r6FO4o8touWuX9fzIfVhUBKaLfd0cTY6ggzSUDQ4Q1IrQdzfgf+AliFZthoyaUG6FKhKjtvdBbzdwYyQVwJ5BoX8xWJ/7/Mn98XOHjo0PeQ3Ab1968t/Hdstpj7tr03fFy2Xv0SA6yjH2z/qc/x8GptqVd6mEZCJONJMxtGn/Lz6dOz+9BXdqxOli4F+p91U/1raVow6jEWJLhGFBjnistlpONO7x13B3ZfxgUk/eBkOqO6OzkHwI6FGsLaCIAmtDcsz1C4DjSOt5Wap8uMkDw9rvborZNaJXS1PjP87sSjObBb+iEhQo7qwLyrRfz6tZ6vmpNioQHW3z+eW2Lo1Jc9Se4UaJaFnQqJeUVXOF63ST5vJobezlwhQ/4Nb7mzvL+rfwtz7ESIOfxeyyzwNnefGrBExbhuCqZQtTlbgPvJqqsnJB5N7Mg+9XEA4c6YpSfWcAnkjgkucDHz8nqT1u70QJtc9/aP4TcG7q82cl80vZoPH+Q0p727QRWjbNVAsZLVcXAfRvcO6n4UXkDDxITsQ13fJx/Iv3L2ADBUe4oHE0KSIRObL2qWPXpLCkAlXxbyau/7rYnzkb7yOCxd3HQFH0rSGjwDJt5AeH9nYzbLaClz0hPCSDQ+DX8Wz+ZefOjPX+ilaecjsLHAOWOvyFeE2VCzdK9VXrGqEAbL+Tbi4iUTHnWe0++uo8h8oAh+AsFSTPUI0SXwdLBUtdYyBkN7kwEsoAxCCwB5gk8BSRVxtN1/WNs4EpJUeno1czJFwFkDpTvY1MnRZ4V09EjHaSGjP6j2RDTTUk1jqa59fMm6+hX8pTHAKH0eKfixkyNQY8LQQkj2eYwFNYvgN0DUu9uwIo7RPbO3+f0VKv7hPyfa5UZFiZ+h8tp26cvTukVJ6iny0aJz+TQNBbgYMxn+V3wPQkGYTuw/fCKZWIM+g7o6FYxnrKu9r5fBqatH9OESt7+SWt7kVTRrfSx2DEGq1B3xIJuRKo3L2S5EyT/tsO+ZTkpqUhkMg6x9k2V+Y61w5wMyEl+ND5FXCoORd0pE1th0v4AXULDzSuirmsiMY22fVRczORu9DmHwHYn0QUII0Xjs0JSLwfAkOHsr/pWBzDG4iZe0TlvEThImRgmhCnTc4qASz6OtYkF2hIA=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="180de6f453872a4d82fff4dcc2c8046d"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=6110b0fc19be1a42')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">6110b0fc19be1a42</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:8d2b:d30c:bf6f:658f</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
