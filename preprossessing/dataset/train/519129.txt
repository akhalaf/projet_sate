<!DOCTYPE html>
<html class="no-js" lang="en-CA">
<head>
<meta charset="utf-8"/>
<meta content="initial-scale=1, maximum-scale=1, width=device-width" name="viewport"/>
<script>(function(){document.documentElement.className='js'})();</script>
<title>Excel Pacific Insurance Agency Inc. of Canada – Excel Pacific Insurance Agency Inc. of Canada</title>
<meta content="noindex,nofollow" name="robots"/>
<script type="text/javascript">
var ajaxurl = "https://excelpacificinsurance.com/wp-admin/admin-ajax.php";
var ff_template_url = "https://excelpacificinsurance.com/wp-content/themes/casanova";
</script>
<link href="//maps.google.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://excelpacificinsurance.com/feed/" rel="alternate" title="Excel Pacific Insurance Agency Inc. of Canada » Feed" type="application/rss+xml"/>
<link href="https://excelpacificinsurance.com/comments/feed/" rel="alternate" title="Excel Pacific Insurance Agency Inc. of Canada » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/excelpacificinsurance.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=56f17c735de36bc90ad2cd5d8f43a014"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://excelpacificinsurance.com/wp-content/uploads/freshframework/assetsmin/28656bc03a23faff352d03108743b52b.css" id="ff-minified-style-0-css" media="all" rel="stylesheet" type="text/css"/>
<style id="ff-minified-style-0-inline-css" type="text/css">
#rs-demo-id {}
html, body{font-family: 'Open Sans', Helvetica, Arial, sans-serif; }
h1, h2, h3, h4, h5, h6{font-family: 'Open Sans', Helvetica, Arial, sans-serif; }
button, input, select, textarea{font-family: 'Open Sans', Helvetica, Arial, sans-serif; }
code, kbd, pre, samp{font-family: 'Courier New', Courier, monospace, monospace; }

a,
#site-nav > ul > li:not(.over) > a:hover,
.light .entry-date,
.dark  .entry-date,
.light .entry-title a:hover,
.dark  .entry-title a:hover,
.light .entry-meta a:hover,
.dark  .entry-meta a:hover,
.light .project-filter .active a,
.dark  .project-filter .active a,
.light .project-filter a:hover,
.dark  .project-filter a:hover,
.project .project-thumb figcaption .icon:hover,
.pricing .plan-price{
    color: #3498DB;
}

.content-header.v3:after,
.breadcrumbs,
.pagenavi a:hover,
.pagenavi span.current,
.project .project-thumb figcaption,
.progress-bar .bar div,
.tabs.vertical .tabnav .active a{
    background-color: #3498DB;
}

input[type="text"]:focus,
input[type="text"]:focus,
input[type="password"]:focus,
input[type="search"]:focus,
input[type="url"]:focus,
input[type="email"]:focus,
input[type="number"]:focus,
select:focus,
textarea:focus,
.pagenavi a:hover,
.pagenavi span.current,
.tabs.vertical .tabnav .active a,
.pricing .plan.recommended{
    border-color: #3498DB;
}

.tabs.vertical .tabnav .active a:after{
    border-left-color: #3498DB;
}
a:active, a:hover,
.light .entry-meta a:hover,
.dark  .entry-meta a:hover,
.light .masonry-entries .entry .entry-title a:hover,
.dark  .masonry-entries .entry .entry-title a:hover,
.light .post-list .details .title a:hover,
.dark  .post-list .details .title a:hover,
.light .entry-meta a:hover,
.dark  .entry-meta a:hover,
.light .project .project-title a:hover,
.dark  .project .project-title a:hover{
    color: #3486BC;
}

/* Black icon */
.icon.primary{
    color: #3498DB;
}
.icon.circle.primary,
.icon.square.primary{
    background-color: #3498DB;
    border-color: #3498DB;
    color: #FFFFFF;
}
.icon.circle.primary:hover,
.icon.square.primary:hover{
    background-color: #3486BC;
    border-color: #3486BC;
    color: #FFFFFF;
}

a.custom-color-class-primary,
.icon.custom-color-class-primary{
    color:#3498DB;
}
.icon.circle.custom-color-class-primary,
.icon.square.custom-color-class-primary,
.button.custom-color-class-primary{
    background-color:#3498DB;
    border-color:#3498DB;
    color:#FFFFFF;
}
.icon.circle.custom-color-class-primary:hover,
.icon.square.custom-color-class-primary:hover,
.button.custom-color-class-primary:hover,
.button.custom-color-class-primary:focus,
.button.custom-color-class-primary:active,
.button.custom-color-class-primary.disabled,
.button.custom-color-class-primary[disabled]{
    background-color:#3486BC;
    border-color:#3486BC;
    color:#FFFFFF;
}

</style>
<link href="//fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C300italic%2C400italic%2C600italic%2C700italic&amp;subset=latin%2Clatin-ext&amp;ver=56f17c735de36bc90ad2cd5d8f43a014" id="google-font-open-sans-css" media="all" rel="stylesheet" type="text/css"/>
<script id="ff-minified-script-0-js" src="https://excelpacificinsurance.com/wp-content/uploads/freshframework/assetsmin/ba8a99b45105d022c38bb58e9e263413.js" type="text/javascript"></script>
<script id="tp-tools-js" src="https://excelpacificinsurance.com/wp-content/plugins/revslider/public/assets/js/rbtools.min.js?ver=6.3.4" type="text/javascript"></script>
<script id="revmin-js" src="https://excelpacificinsurance.com/wp-content/plugins/revslider/public/assets/js/rs6.min.js?ver=6.3.4" type="text/javascript"></script>
<script id="ff-minified-script-1-js" src="https://excelpacificinsurance.com/wp-content/uploads/freshframework/assetsmin/cfb2db07e5436992045c6dc080180262.js" type="text/javascript"></script>
<link href="https://excelpacificinsurance.com/wp-json/" rel="https://api.w.org/"/><link href="https://excelpacificinsurance.com/wp-json/wp/v2/pages/34" rel="alternate" type="application/json"/><link href="https://excelpacificinsurance.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://excelpacificinsurance.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<link href="https://excelpacificinsurance.com/" rel="canonical"/>
<link href="https://excelpacificinsurance.com/" rel="shortlink"/>
<link href="https://excelpacificinsurance.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fexcelpacificinsurance.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://excelpacificinsurance.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fexcelpacificinsurance.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><meta content="Powered by Visual Composer - drag and drop page builder for WordPress." name="generator"/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://excelpacificinsurance.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="https://excelpacificinsurance.com/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><meta content="Powered by Slider Revolution 6.3.4 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." name="generator"/>
<script type="text/javascript">function setREVStartSize(e){
			//window.requestAnimationFrame(function() {				 
				window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;	
				window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;	
				try {								
					var pw = document.getElementById(e.c).parentNode.offsetWidth,
						newh;
					pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
					e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
					e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
					e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
					e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
					e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
					e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
					e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);		
					if(e.layout==="fullscreen" || e.l==="fullscreen") 						
						newh = Math.max(e.mh,window.RSIH);					
					else{					
						e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
						for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];					
						e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
						e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
						for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];
											
						var nl = new Array(e.rl.length),
							ix = 0,						
							sl;					
						e.tabw = e.tabhide>=pw ? 0 : e.tabw;
						e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
						e.tabh = e.tabhide>=pw ? 0 : e.tabh;
						e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;					
						for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
						sl = nl[0];									
						for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}															
						var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);					
						newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
					}				
					if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));					
					document.getElementById(e.c).height = newh+"px";
					window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";				
				} catch(e){
					console.log("Failure at Presize of Slider:" + e)
				}					   
			//});
		  };</script>
<!-- Favicon -->
<link href="https://excelpacificinsurance.com/wp-content/uploads/freshframework/ff_fresh_favicon/favicon_57x57--2018_01_27__09_08_54.png" rel="apple-touch-icon-precomposed" sizes="57x57"/> <!-- iPhone iOS ≤ 6 favicon -->
<link href="https://excelpacificinsurance.com/wp-content/uploads/freshframework/ff_fresh_favicon/favicon_114x114--2018_01_27__09_08_54.png" rel="apple-touch-icon-precomposed" sizes="114x114"/> <!-- iPhone iOS ≤ 6 Retina favicon -->
<link href="https://excelpacificinsurance.com/wp-content/uploads/freshframework/ff_fresh_favicon/favicon_72x72--2018_01_27__09_08_54.png" rel="apple-touch-icon-precomposed" sizes="72x72"/> <!-- iPad iOS ≤ 6 favicon -->
<link href="https://excelpacificinsurance.com/wp-content/uploads/freshframework/ff_fresh_favicon/favicon_144x144--2018_01_27__09_08_54.png" rel="apple-touch-icon-precomposed" sizes="144x144"/> <!-- iPad iOS ≤ 6 Retina favicon -->
<link href="https://excelpacificinsurance.com/wp-content/uploads/freshframework/ff_fresh_favicon/favicon_60x60--2018_01_27__09_08_54.png" rel="apple-touch-icon-precomposed" sizes="60x60"/> <!-- iPhone iOS ≥ 7 favicon -->
<link href="https://excelpacificinsurance.com/wp-content/uploads/freshframework/ff_fresh_favicon/favicon_120x120--2018_01_27__09_08_54.png" rel="apple-touch-icon-precomposed" sizes="120x120"/> <!-- iPhone iOS ≥ 7 Retina favicon -->
<link href="https://excelpacificinsurance.com/wp-content/uploads/freshframework/ff_fresh_favicon/favicon_76x76--2018_01_27__09_08_54.png" rel="apple-touch-icon-precomposed" sizes="76x76"/> <!-- iPad iOS ≥ 7 favicon -->
<link href="https://excelpacificinsurance.com/wp-content/uploads/freshframework/ff_fresh_favicon/favicon_152x152--2018_01_27__09_08_54.png" rel="apple-touch-icon-precomposed" sizes="152x152"/> <!-- iPad iOS ≥ 7 Retina favicon -->
<link href="https://excelpacificinsurance.com/wp-content/uploads/freshframework/ff_fresh_favicon/favicon_196x196--2018_01_27__09_08_54.png" rel="icon" sizes="196x196" type="image/png"/> <!-- Android Chrome M31+ favicon -->
<link href="https://excelpacificinsurance.com/wp-content/uploads/freshframework/ff_fresh_favicon/favicon_160x160--2018_01_27__09_08_54.png" rel="icon" sizes="160x160" type="image/png"/> <!-- Opera Speed Dial ≤ 12 favicon -->
<link href="https://excelpacificinsurance.com/wp-content/uploads/freshframework/ff_fresh_favicon/favicon_96x96--2018_01_27__09_08_54.png" rel="icon" sizes="96x96" type="image/png"/> <!-- Google TV favicon -->
<link href="https://excelpacificinsurance.com/wp-content/uploads/freshframework/ff_fresh_favicon/favicon_32x32--2018_01_27__09_08_54.png" rel="icon" sizes="32x32" type="image/png"/> <!-- Default medium favicon -->
<link href="https://excelpacificinsurance.com/wp-content/uploads/freshframework/ff_fresh_favicon/favicon_16x16--2018_01_27__09_08_54.png" rel="icon" sizes="16x16" type="image/png"/> <!-- Default small favicon -->
<meta content="#FFFFFF" name="msapplication-TileColor"/> <!-- IE10 Windows 8.0 favicon -->
<link href="https://excelpacificinsurance.com/wp-content/uploads/freshframework/ff_fresh_favicon/icon2018_01_27__09_08_54.ico" rel="shortcut icon"/> <!-- Default favicons (16, 32, 48) in .ico format -->
<!--/Favicon -->
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript> <script src="https://www.google.com/recaptcha/api.js?hl=en"></script>
</head>
<body class="home page-template page-template-page-onepage page-template-page-onepage-php page page-id-34 wpb-js-composer js-comp-ver-4.11.2.1 vc_responsive">
<a name="servicesmain" style="position: absolute;top:550px;"></a>
<header class="section header section-navigation light" data-section="navigation">
<div class="section-background-block background-color" style="background-color:#f9f9f9;"></div><div class="section-background-block background-image" style="background-image:url('');"></div><div class="section-background-block background-pattern" style="background-image:url('');"></div><div class="section-background-block background-youtube-video background-youtube-video-id-1" style=""><div class="player" data-property="{videoURL:'',containment:'.background-youtube-video-id-1',autoplay:1,loop:true,showControls:false,showYTLogo:false,mute:true,startAt:0,opacity:1}"></div></div><div class="section-background-block background-url-video" style=""><video autoplay="true" id="video_background" loop="loop" muted="muted" preload="auto" volume="0"></video></div> <div class="container">
<div class="row">
<div class="col-29" id="logo">
<h1 class="site-title">
<a href="https://excelpacificinsurance.com/" rel="home">
<img alt="Excel Pacific Insurance Agency Inc. of Canada" src="https://excelpacificinsurance.com/wp-content/uploads/2018/01/epc-logo-big-1.png"/>
</a>
</h1>
</div>
<div class="col-7" id="header-widgets">
<nav class="text-right" id="header-social">
</nav>
<nav class="nav horizontal text-right" id="site-nav"><ul class="dropdown nav navbar-nav navbar-left" id="menu-menu-1" role="menu"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-4118 active" id="menu-item-4118"><a aria-haspopup="true" class="dropdown-toggle" href="/#servicesmain" title="Our Services">Our Services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32" id="menu-item-32"><a aria-haspopup="true" class="dropdown-toggle" href="https://excelpacificinsurance.com/our-partners/" title="Our Partners">Our Partners</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31" id="menu-item-31"><a aria-haspopup="true" class="dropdown-toggle" href="https://excelpacificinsurance.com/about-us/" title="About Us">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30" id="menu-item-30"><a aria-haspopup="true" class="dropdown-toggle" href="https://excelpacificinsurance.com/join-us/" title="Join Us">Join Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29" id="menu-item-29"><a aria-haspopup="true" class="dropdown-toggle" href="https://excelpacificinsurance.com/contact-us/" title="Contact Us">Contact Us</a></li>
</ul></nav> </div>
</div>
</div>
</header>
<section class="section section-revslider tp-banner-container">
<!-- START Revslider Casanova REVOLUTION SLIDER 6.3.4 --><p class="rs-p-wp-fix"></p>
<rs-module-wrap data-source="gallery" id="rev_slider_1_1_wrapper" style="background:#E9E9E9;padding:0;margin:0px auto;margin-top:0;margin-bottom:0;max-width:;">
<rs-module data-version="6.3.4" id="rev_slider_1_1" style="">
<rs-slides>
<rs-slide data-anim="ei:d;eo:d;s:d;t:boxfade;sl:d;" data-key="rs-1" data-title="Slide">
<img class="rev-slidebg" data-bg="p:center top;" data-no-retina="" src="//excelpacificinsurance.com/wp-content/uploads/revslider/revslider_casanova/slide-bg-1.jpg" title="Home"/>
<!--
--><rs-layer class="large-black" data-color="rgba(41,41,41,1)" data-frame_0="tp:600;" data-frame_1="tp:600;st:500;" data-frame_999="st:w;auto:true;" data-rsp_ch="on" data-text="s:28;l:22;" data-type="text" data-xy="x:c;y:172px;" id="slider-1-slide-1-layer-1" style="z-index:5;">Welcome to  
							</rs-layer><!--
--><rs-layer class="big-medium-black" data-color="rgba(41,41,41,1)" data-frame_0="tp:600;" data-frame_1="tp:600;st:1000;" data-frame_999="st:w;auto:true;" data-rsp_ch="on" data-text="s:40;l:22;" data-type="text" data-xy="x:c;y:215px;" id="slider-1-slide-1-layer-2" style="z-index:6;">Excel Pacific Insurance Agency 
							</rs-layer><!--
--><rs-layer class="medium-black" data-color="rgba(41,41,41,1)" data-dim="w:656px;h:75px;" data-frame_0="tp:600;" data-frame_1="tp:600;st:1500;" data-frame_999="st:w;auto:true;" data-rsp_ch="on" data-text="w:normal;s:18;l:22;a:center;" data-type="text" data-xy="x:c;xo:-1px;y:284px;" id="slider-1-slide-1-layer-3" style="z-index:7;">We pride our partnership with GMS
 in delivering you premium-quality medical  insurance packages for Canadian Residents and Visitors to Canada! 
							</rs-layer><!--
--> </rs-slide>
</rs-slides>
</rs-module>
<script type="text/javascript">
					setREVStartSize({c: 'rev_slider_1_1',rl:[1240,1024,778,480],el:[],gw:[1050],gh:[500],type:'standard',justify:'',layout:'fullwidth',mh:"0"});
					var	revapi1,
						tpj;
					function revinit_revslider11() {
					jQuery(function() {
						tpj = jQuery;
						revapi1 = tpj("#rev_slider_1_1");
						tpj.noConflict();
						if(revapi1==undefined || revapi1.revolution == undefined){
							revslider_showDoubleJqueryError("rev_slider_1_1");
						}else{
							revapi1.revolution({
								duration:"",
								visibilityLevels:"1240,1024,778,480",
								gridwidth:1050,
								gridheight:500,
								spinner:"spinner4",
								perspectiveType:"local",
								responsiveLevels:"1240,1024,778,480",
								progressBar:{disableProgressBar:true},
								navigation: {
									onHoverStop:false
								},
								fallbacks: {
									allowHTML5AutoPlayOnAndroid:true
								},
							});
						}
						
					});
					} // End of RevInitScript
				var once_revslider11 = false;
				if (document.readyState === "loading") {document.addEventListener('readystatechange',function() { if((document.readyState === "interactive" || document.readyState === "complete") && !once_revslider11 ) { once_revslider11 = true; revinit_revslider11();}});} else {once_revslider11 = true; revinit_revslider11();}
				</script>
<script>
					var htmlDivCss = '	#rev_slider_1_1_wrapper rs-loader.spinner4 div { background-color: #FFFFFF !important; } ';
					var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
					if(htmlDiv) {
						htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
					}else{
						var htmlDiv = document.createElement('div');
						htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
						document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
					}
				</script>
</rs-module-wrap>
<!-- END REVOLUTION SLIDER -->
</section><section class="section section-text-left-icon light" data-section="text-left-icon">
<div class="container">
<div class="section-content row">
<header class="content-header section-header text-center">
<h2>Life and Medical Insurance in Canada</h2>
<p class="lead">If you are a Canadian Resident or just visiting this magnificent country, we have got you covered with top-notch life and medical insurance products from the most reputable Canadian insurance companies including Life Insurance, Critical Illness Insurance, Disability Insurance, as well as Travel Insurance for Canadians and Visitors to Canada / Super Visa applicants!</p>
</header>
<div class="col-24">
<aside class="iconbox text-left">
<div class="icon circle blue ">
<i class="ff-font-awesome4 icon-road"></i>
</div>
<div class="iconbox-content">
<h4 class="title">Travel Insurance for Canadians</h4>
<p>Being Canadian has its perks for travelling around the world and one of them is travel insurance!
<br/>

We offer multiple coverage options from the best Canadian companies such as medical emergency insurance, all-inclusive packages as well as non-medical insurance policies to take care of the unexpected costs for during your trip!</p>
<a class="button blue rounded" href="/travel-insurance-for-canadians/" target="_blank">Learn More</a>
</div> </aside>
</div>
<div class="col-24">
<aside class="iconbox text-left">
<div class="icon circle blue ">
<i class="ff-font-awesome4 icon-street-view"></i>
</div>
<div class="iconbox-content">
<h4 class="title">Life Insurance</h4>
<p>Making smart life choices requires us to see the whole picture, where our loved ones may depend on us more than we think. 
<br/>

With the right Life, Critical Illness or Disability insurance policy you gain control of your investment in a stable financial future and well-being of your family!</p>
<a class="button blue rounded" href="/life-insurance-canada/" target="_blank">Learn More</a>
</div> </aside>
</div>
<div class="col-24">
<aside class="iconbox text-left">
<div class="icon circle blue ">
<i class="ff-font-awesome4 icon-clipboard"></i>
</div>
<div class="iconbox-content">
<h4 class="title">Visitors to Canada &amp; Super Visa Insurance</h4>
<p>Whether you are exploring Canada as a tourist, settling in as a New Immigrant, Temporary Foreign Worker or IEC / Working Holiday participant, or you are applying for a Super Visa to reunite with your family, Visitors to Canada Insurance will protect your finances against the costs for medical emergency care, while you can truly enjoy having a time of your life!</p>
<a class="button blue rounded" href="/visitors-to-canada-and-super-visa-insurance/" target="_blank">Learn More</a>
</div> </aside>
</div>
<div class="clear"></div>
</div>
</div>
</section>
<section class="section section-text-left-icon light" data-section="text-left-icon" id="service2">
<div class="section-background-block background-color" style="background-color:#0ab1ff;opacity:0.60;"></div><div class="section-background-block background-image" style="background-image:url('');"></div><div class="section-background-block background-pattern" style="background-image:url('');"></div><div class="section-background-block background-youtube-video background-youtube-video-id-2" style=""><div class="player" data-property="{videoURL:'',containment:'.background-youtube-video-id-2',autoplay:1,loop:true,showControls:false,showYTLogo:false,mute:true,startAt:0,opacity:1}"></div></div><div class="section-background-block background-url-video" style=""><video autoplay="true" id="video_background" loop="loop" muted="muted" preload="auto" volume="0"></video></div> <div class="container">
<div class="section-content row">
<header class="content-header section-header text-center">
<h2>Investment Services</h2>
<p class="lead">Thinking about making a sound investment with a favorable return with a segregated fund or want to save for your children’s post-secondary education with RESP, but not sure if it’s worth it? Worry no more! Let our investment product choice of Segregated Funds and Registered Education Savings Plans (RESP) make your money work for you and not the other way round!</p>
</header>
<div class="col-18">
<aside class="iconbox text-left">
<div class="icon circle ">
<i class="ff-font-awesome4 icon-credit-card"></i>
</div>
<div class="iconbox-content">
<h4 class="title">Segregated Funds</h4>
<p>What makes Segregated Fund a worry-free product is a certainty of receiving a financial benefit from your investment! As it combines the benefits from mutual funds and life insurance coverage, therefore, allowing your investment to mature, while you have guaranteed financial security, Segregated Fund is a great deal, if you are looking for a safe long-term bet!</p>
<a class="button rounded" href="/canadian-segregated-funds/" target="_blank">Learn More</a>
</div> </aside>
</div>
<div class="col-18">
<aside class="iconbox text-left">
<div class="icon circle ">
<i class="ff-font-awesome4 icon-list"></i>
</div>
<div class="iconbox-content">
<h4 class="title">RESP (Registered Education Savings Plan)</h4>
<p>With a growing global economy, post-secondary education is among key assets that Canadian youth can offer at the job market. A government-supported Registered Education Savings Plan (RESP) could significantly reduce the financial burden of caring parents paying for post-secondary education to ensure a future full of career possibilities for their kids!</p>
<a class="button rounded" href="/resp-registered-education-savings-plan/" target="_blank">Learn More</a>
</div> </aside>
</div>
<div class="clear"></div>
</div>
</div>
</section>
<section class="section section-clients-1 light" data-section="clients-1">
<div class="section-background-block background-color" style="background-color:#f7f7f7;"></div><div class="section-background-block background-image" style="background-image:url('');"></div><div class="section-background-block background-pattern" style="background-image:url('');"></div><div class="section-background-block background-youtube-video background-youtube-video-id-3" style=""><div class="player" data-property="{videoURL:'',containment:'.background-youtube-video-id-3',autoplay:1,loop:true,showControls:false,showYTLogo:false,mute:true,startAt:0,opacity:1}"></div></div><div class="section-background-block background-url-video" style=""><video autoplay="true" id="video_background" loop="loop" muted="muted" preload="auto" volume="0"></video></div> <div class="container">
<div class="section-content">
<header class="content-header section-header text-center">
<h2>Our Partners</h2>
<p class="lead">We value the relationship with our national partners in delivering you the best services from the top providers of medical insurance and financial products across Canada!</p>
</header>
<div class="clients row">
<div class="col-24">
<div class="client">
<a class="tooltip" href="http://www.manulife.ca/" target="_blank" title="Click to visit - Manulife Financial">
<img alt="client-1" src="https://excelpacificinsurance.com/wp-content/uploads/2016/09/manulife_financial.png"/>
</a>
</div>
</div>
<div class="col-24">
<div class="client">
<a class="tooltip" href="http://www.excellence.qc.ca/" target="_blank" title="Click to visit - Industrial Alliance Excellence">
<img alt="client-1" src="https://excelpacificinsurance.com/wp-content/uploads/2016/09/industrial-alliance-excellence-logo-en.png"/>
</a>
</div>
</div>
<div class="col-24">
<div class="client">
<a class="tooltip" href="https://www.desjardins.com/ca/" target="_blank" title="Click to visit - Desjardins">
<img alt="client-1" src="https://excelpacificinsurance.com/wp-content/uploads/2016/09/logo_desjardins_cl3.gif"/>
</a>
</div>
</div>
<div class="col-24">
<div class="client">
<a class="tooltip" href="http://www.canadalife.com/003/Home/index.htm" target="_blank" title="Click to visit - Canada Life">
<img alt="client-1" src="https://excelpacificinsurance.com/wp-content/uploads/2016/09/alu_2-6_canada-life_481x190.png"/>
</a>
</div>
</div>
<div class="col-24">
<div class="client">
<a class="tooltip" href="http://www.empire.ca/consumer/en/index.html" target="_blank" title="Click to visit - Empire Life">
<img alt="client-1" src="https://excelpacificinsurance.com/wp-content/uploads/2016/09/Empire-Life-logo-2011.png"/>
</a>
</div>
</div>
<div class="col-24">
<div class="client">
<a class="tooltip" href="https://ssq.ca/" target="_blank" title="Click to visit - SSQ Assurance">
<img alt="client-1" src="https://excelpacificinsurance.com/wp-content/uploads/2016/09/ssq.png"/>
</a>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section-map light" data-overlay="#FFFFFF" data-overlay-opacity="0.82" data-section="map">
<div class="container">
<div class="section-content row">
<div class="col-1">
<header class="content-header section-header text-center">
<h2>Contact Us</h2>
<p class="lead">Have a question about the services or products we offer? Leave us a message using the contact form below and we will get in touch with you shortly!</p>
</header>
<form method="post">
<div class="row">
<p class="col-24">
<label for="name">Name:  <span class="required">*</span></label><br/>
<input class="ff-input-name" id="name" name="name" size="100" type="text"/>
</p>
<p class="col-24">
<label for="email">E-mail:  <span class="required">*</span></label><br/>
<input class="ff-input-email" id="email" name="email" size="100" type="email"/>
</p>
<p class="col-24">
<label for="url">Website: </label><br/>
<input id="url" name="url" size="150" type="text"/>
</p>
<p class="col-1">
<label for="subject">Subject: </label><br/>
<input id="subject" name="subject" size="150" type="text"/>
</p>
<p class="col-1">
<label for="msg">Message:  <span class="required">*</span></label><br/>
<textarea class="ff-input-message" cols="100" id="msg" name="message" rows="11"></textarea>
</p>
<div class="col-1 g-recaptcha" data-sitekey="6LdLhAkUAAAAALIcAzzZAPt7VeFAP75zU4dSwEwm"></div>
<p class="col-1" style="margin-bottom: 0; margin-top: 20px;">
<input class="button primary" name="submit" type="submit" value="Send Message"/>
</p>
</div>
<div class="ff-contact-info" style="display:none;">5240,5151,5218,5226,5214,5222,5225,5151,5175,5151,5226,5222,5216,5221,5214,5218,5225,5163,5214,5231,5215,5218,5233,5228,5235,5181,5220,5226,5214,5222,5225,5163,5216,5228,5226,5151,5161,5151,5232,5234,5215,5223,5218,5216,5233,5151,5175,5151,5189,5228,5226,5218,5151,5242</div><br/><div class="ff-contact-info-message-good notification success" style="display:none;"><p>Your message has been sent successfully</p></div><div class="ff-contact-info-message-bad notification danger" style="display:none;"><p>There was a problem, please send your message later</p></div><div class="ff-contact-info-message-bad notification danger recaptcha" style="display:none;"><p>The recaptcha is wrong! Please re-enter recaptcha!</p></div> </form>
</div>
</div>
</div>
</section>
<section class="section section-footer-widgets dark" data-section="footer-widgets"> <div class="container">
<div class="section-content row">
<div class="col-27"><aside class="widget widget_text" id="text-3"><div class="content-header widget-header v3"><h4>Excel Pacific Insurance Agency Inc.</h4></div> <div class="textwidget"></div>
</aside></div><div class="col-27"><aside class="widget widget_text" id="text-4"> <div class="textwidget">Richmond Office: 670-4400 Hazelbridge Way, Richmond, BC, V6X 3R8, Canada
<br/>

Head Office: 201-640 West Broadway, Vancouver, BC, V5Z 1G4, Canada</div>
</aside></div><div class="col-27"><aside class="widget widget_text" id="text-5"> <div class="textwidget"><p>1 (604)-603-9030 - Richmond<br/>
1 (604)-726-2705 - Head Office<br/>
michael@arbetovinsurance.com</p>
</div>
</aside></div><div class="col-27"><aside class="widget widget_text" id="text-6"> <div class="textwidget"><p>Excel Pacific Insurance Agency Inc. is a Canadian insurance agency operating as a part of <a href="http://www.excelfinancialgroup.ca/" rel="noopener" target="newwin">Excel Financial Group</a> that has been providing life insurance, medical insurance and financial services for Canadians for over twenty years!</p>
<p><a href="/privacy-policy">Privacy  and terms of use</a></p>
</div>
</aside></div> </div>
</div>
</section> <script type="text/javascript">
		if(typeof revslider_showDoubleJqueryError === "undefined") {
			function revslider_showDoubleJqueryError(sliderID) {
				var err = "<div class='rs_error_message_box'>";
				err += "<div class='rs_error_message_oops'>Oops...</div>";
				err += "<div class='rs_error_message_content'>";
				err += "You have some jquery.js library include that comes after the Slider Revolution files js inclusion.<br>";
				err += "To fix this, you can:<br>&nbsp;&nbsp;&nbsp; 1. Set 'Module General Options' -> 'Advanced' -> 'jQuery & OutPut Filters' -> 'Put JS to Body' to on";
				err += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jQuery.js inclusion and remove it";
				err += "</div>";
			err += "</div>";
				var slider = document.getElementById(sliderID); slider.innerHTML = err; slider.style.display = "block";
			}
		}
		</script>
<script id="wp-embed-js" src="https://excelpacificinsurance.com/wp-includes/js/wp-embed.min.js?ver=56f17c735de36bc90ad2cd5d8f43a014" type="text/javascript"></script>
<script id="ff-minified-script-2-js" src="https://excelpacificinsurance.com/wp-content/uploads/freshframework/assetsmin/766bbaa709739b3bcfb1d4cf5b4d8e5b.js" type="text/javascript"></script>
<script id="ff-google-maps-js" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en&amp;key=AIzaSyAaqvNiI9NTTpLakjinSQe6Sd6Hc0MSFeo&amp;ver=56f17c735de36bc90ad2cd5d8f43a014" type="text/javascript"></script>
<script id="ff-minified-script-3-js" src="https://excelpacificinsurance.com/wp-content/uploads/freshframework/assetsmin/359af1575a07ec35e6e30dd323b15c37.js" type="text/javascript"></script>
</body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-779838-22', 'auto');
  ga('send', 'pageview');

</script>
</html><!-- WP Fastest Cache file was created in 0.43664813041687 seconds, on 12-01-21 18:00:23 -->