<!DOCTYPE html>
<html lang="nl">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://abkoo.nl/xmlrpc.php" rel="pingback"/>
<title>Pagina niet gevonden – Abkoo Mestkelders, Mestputten, Mestbassins, Mestsilo's en Mestzakken reiniging en inspectie</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//maxcdn.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://abkoo.nl/feed/" rel="alternate" title="Abkoo Mestkelders, Mestputten, Mestbassins, Mestsilo's en Mestzakken reiniging en inspectie » Feed" type="application/rss+xml"/>
<link href="https://abkoo.nl/comments/feed/" rel="alternate" title="Abkoo Mestkelders, Mestputten, Mestbassins, Mestsilo's en Mestzakken reiniging en inspectie » Reactiesfeed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/abkoo.nl\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://abkoo.nl/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.0.1" id="js_composer_front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://abkoo.nl/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://abkoo.nl/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://abkoo.nl/wp-content/plugins/essential-grid/public/assets/css/settings.css?ver=2.0.8" id="essential-grid-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&amp;ver=5.6" id="tp-open-sans-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&amp;ver=5.6" id="tp-raleway-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&amp;ver=5.6" id="tp-droid-serif-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://abkoo.nl/wp-content/themes/construction/style.css?ver=5.6" id="wpcharming-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css?ver=4.2.0" id="wpcharming-fontawesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=PT+Sans%3A400%2C700%2C400italic%2C700italic%7CMontserrat%3A400%2C700&amp;subset=latin&amp;ver=1492002514" id="redux-google-fonts-wpc_options-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js-extra" type="text/javascript">
/* <![CDATA[ */
var header_fixed_setting = {"fixed_header":"1"};
/* ]]> */
</script>
<script id="jquery-core-js" src="https://abkoo.nl/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://abkoo.nl/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="themepunchboxext-js" src="https://abkoo.nl/wp-content/plugins/essential-grid/public/assets/js/lightbox.js?ver=2.0.8" type="text/javascript"></script>
<script id="tp-tools-js" src="https://abkoo.nl/wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.tools.min.js?ver=2.0.8" type="text/javascript"></script>
<script id="essential-grid-essential-grid-script-js" src="https://abkoo.nl/wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.essential.min.js?ver=2.0.8" type="text/javascript"></script>
<script id="wpcharming-modernizr-js" src="https://abkoo.nl/wp-content/themes/construction/assets/js/modernizr.min.js?ver=2.6.2" type="text/javascript"></script>
<script id="wpcharming-libs-js" src="https://abkoo.nl/wp-content/themes/construction/assets/js/libs.js?ver=5.6" type="text/javascript"></script>
<link href="https://abkoo.nl/wp-json/" rel="https://api.w.org/"/><link href="https://abkoo.nl/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://abkoo.nl/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		if (window.wfLogHumanRan) { return; }
		window.wfLogHumanRan = true;
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//abkoo.nl/?wordfence_lh=1&hid=8628B39535C56EF5FD4F3D683F5585A7');
</script><!--[if lt IE 9]><script src="https://abkoo.nl/wp-content/themes/construction/assets/js/html5.min.js"></script><![endif]-->
<style id="theme_option_custom_css" type="text/css">
</style>
<meta content="Powered by Visual Composer - drag and drop page builder for WordPress." name="generator"/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://abkoo.nl/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><style class="options-output" title="dynamic-css" type="text/css">.site-header .site-branding{margin-top:5px;margin-right:20px;margin-bottom:0;margin-left:0;}.page-title-wrap{background-color:#f8f9f9;}a, .primary-color, .wpc-menu a:hover, .wpc-menu > li.current-menu-item > a, .wpc-menu > li.current-menu-ancestor > a,
                                                       .entry-footer .post-categories li a:hover, .entry-footer .post-tags li a:hover,
                                                       .heading-404, .grid-item .grid-title a:hover, .widget a:hover, .widget #calendar_wrap a, .widget_recent_comments a,
                                                       #secondary .widget.widget_nav_menu ul li a:hover, #secondary .widget.widget_nav_menu ul li li a:hover, #secondary .widget.widget_nav_menu ul li li li a:hover,
                                                       #secondary .widget.widget_nav_menu ul li.current-menu-item a, .woocommerce ul.products li.product .price, .woocommerce .star-rating,
                                                       .iconbox-wrapper .iconbox-icon .primary, .iconbox-wrapper .iconbox-image .primary, .iconbox-wrapper a:hover,
                                                       .breadcrumbs a:hover, #comments .comment .comment-wrapper .comment-meta .comment-time:hover, #comments .comment .comment-wrapper .comment-meta .comment-reply-link:hover, #comments .comment .comment-wrapper .comment-meta .comment-edit-link:hover,
                                                       .nav-toggle-active i, .header-transparent .header-right-wrap .extract-element .phone-text, .site-header .header-right-wrap .extract-element .phone-text,
                                                       .wpb_wrapper .wpc-projects-light .esg-navigationbutton:hover, .wpb_wrapper .wpc-projects-light .esg-filterbutton:hover,.wpb_wrapper .wpc-projects-light .esg-sortbutton:hover,.wpb_wrapper .wpc-projects-light .esg-sortbutton-order:hover,.wpb_wrapper .wpc-projects-light .esg-cartbutton-order:hover,.wpb_wrapper .wpc-projects-light .esg-filterbutton.selected,
                                                       .wpb_wrapper .wpc-projects-dark .esg-navigationbutton:hover, .wpb_wrapper .wpc-projects-dark .esg-filterbutton:hover, .wpb_wrapper .wpc-projects-dark .esg-sortbutton:hover,.wpb_wrapper .wpc-projects-dark .esg-sortbutton-order:hover,.wpb_wrapper .wpc-projects-dark .esg-cartbutton-order:hover, .wpb_wrapper .wpc-projects-dark .esg-filterbutton.selected{color:#ff0000;}input[type="reset"], input[type="submit"], input[type="submit"], .wpc-menu ul li a:hover,
                                                       .wpc-menu ul li.current-menu-item > a, .loop-pagination a:hover, .loop-pagination span:hover,
                                                       .loop-pagination a.current, .loop-pagination span.current, .footer-social, .tagcloud a:hover, woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,
                                                       .woocommerce #respond input#submit.alt:hover, .woocommerce #respond input#submit.alt:focus, .woocommerce #respond input#submit.alt:active, .woocommerce a.button.alt:hover, .woocommerce a.button.alt:focus, .woocommerce a.button.alt:active, .woocommerce button.button.alt:hover, .woocommerce button.button.alt:focus, .woocommerce button.button.alt:active, .woocommerce input.button.alt:hover, .woocommerce input.button.alt:focus, .woocommerce input.button.alt:active,
                                                       .woocommerce span.onsale, .entry-content .wpb_content_element .wpb_tour_tabs_wrapper .wpb_tabs_nav li.ui-tabs-active a, .entry-content .wpb_content_element .wpb_accordion_header li.ui-tabs-active a,
                                                       .entry-content .wpb_content_element .wpb_accordion_wrapper .wpb_accordion_header.ui-state-active a,
                                                       .btn, .btn:hover, .btn-primary, .custom-heading .heading-line, .custom-heading .heading-line.primary,
                                                       .wpb_wrapper .eg-wpc_projects-element-1{background-color:#ff0000;}textarea:focus, input[type="date"]:focus, input[type="datetime"]:focus, input[type="datetime-local"]:focus, input[type="email"]:focus, input[type="month"]:focus, input[type="number"]:focus, input[type="password"]:focus, input[type="search"]:focus, input[type="tel"]:focus, input[type="text"]:focus, input[type="time"]:focus, input[type="url"]:focus, input[type="week"]:focus,
                                                       .entry-content blockquote, .woocommerce ul.products li.product a img:hover, .woocommerce div.product div.images img:hover{border-color:#ff0000;}#secondary .widget.widget_nav_menu ul li.current-menu-item a:before{border-left-color:#ff0000;}.secondary-color, .iconbox-wrapper .iconbox-icon .secondary, .iconbox-wrapper .iconbox-image .secondary{color:#00aeef;}.btn-secondary, .custom-heading .heading-line.secondary{background-color:#00aeef;}.hentry.sticky, .entry-content blockquote, .entry-meta .sticky-label,
                                .entry-author, #comments .comment .comment-wrapper, .page-title-wrap, .widget_wpc_posts ul li,
                                .inverted-column > .wpb_wrapper, .inverted-row, div.wpcf7-response-output{background-color:#f8f9f9;}hr, abbr, acronym, dfn, table, table > thead > tr > th, table > tbody > tr > th, table > tfoot > tr > th, table > thead > tr > td, table > tbody > tr > td, table > tfoot > tr > td,
                                fieldset, select, textarea, input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="email"], input[type="month"], input[type="number"], input[type="password"], input[type="search"], input[type="tel"], input[type="text"], input[type="time"], input[type="url"], input[type="week"],
                                .left-sidebar .content-area, .left-sidebar .sidebar, .right-sidebar .content-area, .right-sidebar .sidebar,
                                .site-header, .wpc-menu.wpc-menu-mobile, .wpc-menu.wpc-menu-mobile li, .blog .hentry, .archive .hentry, .search .hentry,
                                .page-header .page-title, .archive-title, .client-logo img, #comments .comment-list .pingback, .page-title-wrap, .page-header-wrap,
                                .portfolio-prev i, .portfolio-next i, #secondary .widget.widget_nav_menu ul li.current-menu-item a, .icon-button,
                                .woocommerce nav.woocommerce-pagination ul, .woocommerce nav.woocommerce-pagination ul li,woocommerce div.product .woocommerce-tabs ul.tabs:before, .woocommerce #content div.product .woocommerce-tabs ul.tabs:before, .woocommerce-page div.product .woocommerce-tabs ul.tabs:before, .woocommerce-page #content div.product .woocommerce-tabs ul.tabs:before,
                                .woocommerce div.product .woocommerce-tabs ul.tabs li:after, .woocommerce div.product .woocommerce-tabs ul.tabs li:before,
                                .woocommerce table.cart td.actions .coupon .input-text, .woocommerce #content table.cart td.actions .coupon .input-text, .woocommerce-page table.cart td.actions .coupon .input-text, .woocommerce-page #content table.cart td.actions .coupon .input-text,
                                .woocommerce form.checkout_coupon, .woocommerce form.login, .woocommerce form.register,.shop-elements i, .testimonial .testimonial-content, .breadcrumbs,
                                .woocommerce-cart .cart-collaterals .cart_totals table td, .woocommerce-cart .cart-collaterals .cart_totals table th,.carousel-prev, .carousel-next,.recent-news-meta,
                                .woocommerce ul.products li.product a img, .woocommerce div.product div.images img{border-color:#e9e9e9;}.site{background-color:#ffffff;}.layout-boxed{background-color:#333333;}body{font-family:"PT Sans";color:#777777;font-size:14px;}h1,h2,h3,h4,h5,h6, .font-heading{font-family:Montserrat;font-weight:normal;color:#333333;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>
<body class="error404 page-fullwidth header-topbar header-fixed-on header-normal unknown wpb-js-composer js-comp-ver-5.0.1 vc_responsive elementor-default elementor-kit-1047">
<div class="hfeed site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<div class="site-topbar" id="topbar">
<div class="container">
<div class="topbar-inner clearfix">
<div class="topbar-left topbar widget-area clearfix">
<aside class="topbar-widget widget widget_text"><h2 class="widgettitle">TopBar Social</h2>
<div class="textwidget"><div class="topbar-social">
<a href="mailto:info@abkoo.nl" rel="noopener" target="_blank" title="Email"><i class="fa fa-envelope"></i></a>
</div></div>
</aside><aside class="topbar-widget widget widget_text"><h2 class="widgettitle">TopBar Text</h2>
<div class="textwidget">Veilig werken met mestopslag.</div>
</aside> </div>
<div class="topbar-right topbar widget-area clearfix">
<aside class="topbar-widget widget widget_text"><h2 class="widgettitle">TopBar Phone Contact</h2>
<div class="textwidget"><div class="extract-element">
<span class="topbar-text">Mob: </span>
<span class="topbar-highlight primary-color">06 15960741</span>
</div></div>
</aside><aside class="topbar-widget widget widget_text"><h2 class="widgettitle">TopBar Email Contact</h2>
<div class="textwidget"><div class="extract-element">
<span class="topbar-text"> Email: </span>
<span class="topbar-highlight primary-color"><a href="mailto:info@abkoo.nl">info@abkoo.nl</a></span>
</div></div>
</aside> </div>
</div>
</div>
</div> <!-- /#topbar -->
<header class="site-header fixed-on" id="masthead" role="banner">
<div class="header-wrap">
<div class="container">
<div class="site-branding">
<a href="https://abkoo.nl/" rel="home" title="Abkoo Mestkelders, Mestputten, Mestbassins, Mestsilo's en Mestzakken reiniging en inspectie">
<img alt="" src="http://abkoo.nl/wp-content/uploads/site-logo-300-100-001.png"/>
</a>
</div><!-- /.site-branding -->
<div class="header-right-wrap clearfix">
<nav class="main-navigation" id="site-navigation" role="navigation">
<div id="nav-toggle"><i class="fa fa-bars"></i></div>
<ul class="wpc-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-846" id="menu-item-846"><a href="http://abkoo.nl">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-961" id="menu-item-961"><a href="https://abkoo.nl/projecten/">Projecten</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-853" id="menu-item-853"><a href="https://abkoo.nl/contact/">Contact</a></li>
</ul>
</nav><!-- #site-navigation -->
</div>
</div>
</div>
</header><!-- #masthead -->
<div class="site-content" id="content">
<div class="error-page-wrapper">
<div class="error-box-wrap">
<div class="text-center">
<h1 class="heading-404"><i class="fa fa-frown-o"></i>404</h1>
<div class="error-box">
<h3>Page Not Found</h3>
<p>The page you are looking for does not appear to exist. Please go back or head on over our homepage to choose a new direction.</p>
<div class="error-action clearfix">
<a class="btn btn-light error-previous" href="javascript: history.go(-1)">Go to previous page</a>
<a class="btn btn-light error-home" href="https://abkoo.nl">Go back to homepage</a>
</div>
</div>
</div>
</div>
</div>
</div><!-- #content -->
<div class="clear"></div>
<footer class="site-footer" id="colophon" role="contentinfo">
<div class="footer-connect">
<div class="container">
</div>
</div>
<div class="container">
<div class="footer-widgets-area">
<div class="sidebar-footer footer-columns footer-3-columns clearfix">
<div class="footer-1 footer-column widget-area" id="footer-1" role="complementary">
<aside class="widget widget_text" id="text-4"><h3 class="widget-title">Abkoo</h3> <div class="textwidget"><div class="contact-info-box">
<div class="contact-info-item">
<div class="contact-text"><i class="fa fa-map-marker"></i></div>
<div class="contact-value">De Joodyk 9c<br/>
      8614 JK Oudega SWF
	</div>
</div>
<div class="contact-info-item">
<div class="contact-text"><i class="fa fa-phone"></i></div>
<div class="contact-value">06 15960741</div>
</div>
<div class="contact-info-item">
<div class="contact-text"><i class="fa fa-envelope"></i></div>
<div class="contact-value"><a href="mailto:info@abkoo.nl">info@abkoo.nl</a></div>
</div>
</div></div>
</aside> </div>
<div class="footer-2 footer-column widget-area" id="footer-2" role="complementary">
</div>
<div class="footer-3 footer-column widget-area" id="footer-3" role="complementary">
</div>
</div>
</div>
</div>
<div class="site-info-wrapper">
<div class="container">
<div class="site-info clearfix">
<div class="copy_text">
						Copyright © 2017 Abkoo Mestkelders, Mestputten, Mestbassins, Mestsilo's en Mestzakken reiniging en inspectie - Een pagina van <a href="http://www.idb-reclame.nl/" rel="designer" target="_blank">IDB</a> </div>
<div class="footer-menu">
</div>
</div>
</div>
</div>
</footer><!-- #colophon -->
</div><!-- #page -->
<div id="btt"><i class="fa fa-angle-double-up"></i></div>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/abkoo.nl\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://abkoo.nl/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="wpcharming-theme-js" src="https://abkoo.nl/wp-content/themes/construction/assets/js/theme.js?ver=5.6" type="text/javascript"></script>
<script id="wp-embed-js" src="https://abkoo.nl/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
</body>
</html>
