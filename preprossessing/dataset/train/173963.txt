<!DOCTYPE html>
<html lang="pl">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Sprawdzenie numeru VIN. Historia pojazdu. Dekoder VIN.</title>
<meta content="Darmowe sprawdzenie numeru VIN pojazdu! Kupujesz auto? Poznaj jego historię, przebieg, usterki, akcje serwisowe. Dekodowanie numerów VIN: kody wyposażenia, dane techniczne, zdjęcia pojazdu." name="description"/>
<meta content="Automo24 - Automotive Solutions" name="author"/>
<meta content="cbqMhlEjQS8nvo-djhkS_k4v2yQNH62rkse47x-LCAA" name="google-site-verification"/>
<meta content="article" property="og:type"/>
<meta content="https://www.automo.pl/?utm_source=dark&amp;utm_medium=social&amp;utm_campaign=open-graph" property="og:url"/>
<meta content="Sprawdzenie numeru VIN. Historia pojazdu. Dekoder VIN." property="og:title"/>
<meta content="Darmowe sprawdzenie numeru VIN pojazdu! Kupujesz auto? Poznaj jego historię, przebieg, usterki, akcje serwisowe. Dekodowanie numerów VIN: kody wyposażenia, dane techniczne, zdjęcia pojazdu." property="og:description"/>
<meta content="https://www.automo.pl/theme/automo/img/assetic/x6ec241d.jpg,q1592572041.pagespeed.ic.MnqIfdav7l.webp" property="og:image"/>
<link href="/theme/automo/img/bundles/unit01automoplgui/img/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="/theme/automo/img/bundles/unit01automoplgui/img/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700,800&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
<!--[if gte IE 9]><style type="text/css">
        .gradient {	filter: none; }
    </style><![endif]-->
<link href="/css/extern.style.css?1610445147" rel="stylesheet" type="text/css"/> <link href="/theme/automo/css/theme.css?1610445147" rel="stylesheet" type="text/css"/> <link href="/css/bootstrap.min.css?1610445147" rel="stylesheet" type="text/css"/> <link href="/css/gdpr/gdpr-cookie-bs3.css?1610445147" rel="stylesheet" type="text/css"/>
<!--[if lt IE 12]>
	<link rel="stylesheet" type="text/css" href="/css/base_ie.css?1610445147"/>    <![endif]-->
<script src="/js/jquery-3.4.1.min.js?1610445147"></script><!-- <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-725369-16"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-725369-16')
</script>
</head>
<body>
<div class="container">
<a href="/" title=""><img alt="Automo logo - sprawdzenie VIN" class="logo" id="mainlogo" src="/theme/automo/img/bundles/unit01automoplgui/img/automo_logo.png?1610445147"/></a>
<div class="hidden-xs" style="float:right;margin-left:4px;margin-top: -10px;">
<div class="fb-like" data-action="like" data-href="https://www.facebook.com/automopl.historia.pojazdu" data-layout="box_count" data-share="false" data-show-faces="false"></div>
</div>
</div>
<div class="container">
<nav class="navbar navbar-default" role="navigation">
<div class="container-fluid"><!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<button class="navbar-toggle" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
<span class="sr-only">Przełącz nawigację</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="/"><i class="fa fa-home"></i></a>
</div><!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav yamm">
<li class=""><a href="/">Historia pojazdu</a></li>
<li class="dropdown yamm-fw "><a class="dropdown-toggle" data-toggle="dropdown" href="#">Przykładowe raporty<span class="caret"></span></a>
<ul class="dropdown-menu">
<li><div class="yamm-content">
<div class="row">
<div class="col-sm-4 navdec">
<a href="/historia-pojazdu-automo/5fa159d4-fbe0-44db-9f95-13b2ac1e1e2a" title="historia pojazdu Automo"><span class="nav-head xs-margin-bottom-0"><i class="fa fa-dot-circle-o"></i> Historia pojazdu Automo</span></a>
</div>
<div class="col-sm-4 navdec">
<a href="/raport-z-usa/5f36b67c-b47c-4e66-a615-516fac1e1e2a" title="historia pojazdu z USA"><span class="nav-head xs-margin-bottom-0"><i class="fa fa-dot-circle-o"></i> Historia pojazdu z USA</span></a>
</div>
<div class="col-sm-4 navdec">
<a href="/historia-pojazdu-z-holandii/5f115371-66e0-4511-a1cc-40e4ac1e1e2a/pol" title="historia pojazdu z Holandii"><span class="nav-head margin-bottom-0"><i class="fa fa-dot-circle-o"></i> Historia pojazdu z Holandii</span></a>
</div>
<hr class="clear hidden-xs"/>
<div class="col-sm-4 navdec">
<a href="/historia-pojazdu-z-belgii/5f107bc7-9e24-4515-b366-40e3ac1e1e2a" title="Historia pojazdu z Belgii"><span class="nav-head margin-bottom-0"><i class="fa fa-dot-circle-o"></i> Historia pojazdu z Belgii</span></a>
</div>
<div class="col-sm-4 navdec">
<a href="/wycena-wartosci-pojazdu/" title="Raport wyceny Info Ekspert"><span class="nav-head margin-bottom-0"><i class="fa fa-dot-circle-o"></i> Wycena Info Ekspert</span></a>
</div>
<div class="col-sm-4 navdec">
<a href="/lista-procedur-serwisowych/5f9fd719-c0e8-4422-a23b-3a0bac1e1e2a" title="Raport czynności serwisowych"><span class="nav-head margin-bottom-0"><i class="fa fa-dot-circle-o"></i> Raport Czynności Serwisowych</span></a>
</div>
<br class="hidden-xs"/>
<hr class="clear hidden-xs"/>
<div class="col-sm-4 navdec">
<a href="/przewodnik-po-raporcie-inspekcji-pojazdu" title="Raport Archiwalny Autotesto"><span class="nav-head margin-bottom-0"><i class="fa fa-dot-circle-o"></i> Raport Archiwalny Autotesto</span></a>
</div>
</div>
</div>
</li>
</ul>
</li>
<li>
<a href="/dekoder-vin">Dekoder VIN</a>
</li>
<li class="dropdown yamm-fw">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Narzędzia<span class="caret"></span></a>
<ul class="dropdown-menu">
<li>
<div class="yamm-content">
<div class="row">
<div class="col-sm-4 navdec">
<span class="nav-head"><i class="fa fa-dot-circle-o"></i> Kody wyposażenia</span>
<ul class="nav-deco">
<li><a href="/kody-wyposazenia-bmw">Kody wyposażenia BMW</a></li>
<li><a href="/kody-wyposazenia-mercedes">Kody wyposażenia Mercedes</a></li>
<li><a href="/kody-wyposazenia-gm-rpo">Kody wyposażenia GM (RPO)</a></li>
<li><a href="/kody-wyposazenia-opel">Kody wyposażenia Opel</a></li>
</ul>
</div>
<div class="col-sm-4 navdec">
<span class="nav-head"><i class="fa fa-dot-circle-o"></i> Grupa VAG - <small>Audi, Seat, Skoda, Volkswagen</small></span>
<ul class="nav-deco">
<li><a href="/vw-audi-seat-skoda-kody-opcje-wyposazenie">VAG opcje wyposażenia</a></li>
<li><a href="/vas-50510kody-bledow">VAS 5051 kody błędów</a></li>
<li><a href="/audi-kody-silnikow">Audi kody silników</a></li>
<li><a href="/seat-kody-silnikow">Seat kody silników</a></li>
<li><a href="/skoda-kody-silnikow">Skoda kody silników</a></li>
<li><a href="/vw-kody-silnikow">Volkswagen kody silników</a></li>
</ul>
</div>
<div class="col-sm-4 navdec">
<span class="nav-head"><i class="fa fa-dot-circle-o"></i> Pozostałe kody</span>
<ul class="nav-deco">
<li><a href="/kody-kolorow-nadwozia">Kody kolorów nadwozia</a></li>
<li><a href="/kody-bledow-obd2">Kody błędów OBD2</a></li>
<li><a href="/numer-vin">Numery VIN</a>
</li>
</ul>
</div>
</div>
</div>
</li>
</ul>
</li>
</ul>
<ul class="nav navbar-nav navbar-right">
<li><a href="/login"><i class="fa fa-lock"></i><span class="hidden-sm"> Zaloguj</span></a></li>
</ul>
</div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>
<div class="clearfix"></div>
</div>
<div class="container">
<script src="/js/frontpage.js?1610445147"></script>
<div class="row ">
<div class="col-sm-12 col-md-4 col-lg-4">
<div class="gradient" id="vincheck">
<div class="row ">
<div class="col-sm-6 col-md-12">
<form accept-charset="utf-8" action="/vinchecker" data-form="vinchecker" data-toggle="validator" id="vin-form" method="post" name="raportvin">
<div class="form-group" style="margin-bottom: 5px;">
<label for="vin">Sprawdzenie VIN</label>
<input class="form-control" data-digits="Cztery ostatnie znaki muszą być cyframi" data-length="Numer VIN musi mieć dokładnie 17 znaków. Dozwolone znaki to litery i cyfry" data-minlength="17" data-placement="bottom" data-vin="vin" id="vin" maxlength="17" name="data[Order][vin]" placeholder="Wprowadź numer VIN" required="" type="text"/>
</div>
<div style="display: block; font-size:12px;margin-bottom: 6px;">
<a href="/co-to-jest-vin" style="color:#eee;">Co to jest VIN?</a>
<a href="/jak-sprawdzic-numer-vin" style="color:#eee;margin-left:15px;">Jak sprawdzić nr VIN?</a>
</div>
<a class="btn btn-warning" data-submit-vin="vinchecker" id="vin-button" title="Sprawdź auto">Sprawdź numer VIN</a>
</form>
</div>
<div class="col-sm-6 col-md-12">
<img alt="Szukaj numeru VIN" src="/theme/automo/img/assetic/8f4a953.png?1610445147"/> </div>
</div>
</div>
</div>
<div class="col-sm-12 col-md-8 col-lg-8 hidden-xs"><!-- Carousel -->
<div class="carousel slide" data-ride="carousel" id="mainCarousel">
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#mainCarousel"></li>
<li data-slide-to="1" data-target="#mainCarousel"></li>
<li data-slide-to="2" data-target="#mainCarousel"></li>
<li data-slide-to="3" data-target="#mainCarousel"></li>
</ol>
<div class="carousel-inner">
<div class="item active">
<img alt="Kupujesz auto? Sprawdź VIN bo warto." src="/theme/automo/img/assetic/6ec241d.jpg?1610445147"/> </div>
<div class="item">
<img alt="Historia pojazdu i jego pochodzenie." src="/theme/automo/img/assetic/226962d.jpg?1610445147"/> </div>
<div class="item">
<img alt="Sprawdź VIN i dowiedz się czy auto nie pochodzi z kradzieży." src="/theme/automo/img/assetic/16ca0dc.jpg?1610445147"/> </div>
<div class="item">
<img alt="Dekoder VIN Decoder - dane techniczne pojazdu" src="/theme/automo/img/assetic/5cf030c.jpg?1610445147"/> </div>
</div>
<a class="left carousel-control" data-slide="prev" href="#mainCarousel"><span class="glyphicon glyphicon-chevron-left"></span></a>
<a class="right carousel-control" data-slide="next" href="#mainCarousel"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div><!-- /.carousel -->
</div>
</div>
<hr class="hidden-xs"/>
<div class="well features gradient hidden-xs">
<h3>Co zawierają nasze Raporty VIN:</h3><!-- Carousel -->
<div class="carousel slide" data-ride="carousel" id="featuresCarousel">
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#featuresCarousel"></li>
<li data-slide-to="1" data-target="#featuresCarousel"></li>
</ol>
<div class="carousel-inner">
<div class="item active">
<div class="row">
<div class="col-sm-3">
<div class="head-report">
<img alt="Informacja kradzieżowa" src="/theme/automo/img/assetic/b61b627.png?1610445147"/><strong>Informacja kradzieżowa</strong><span>Sprawdzenie rejestrów i baz pojazdów skradzionych.</span>
</div>
</div>
<div class="col-sm-3">
<div class="head-report">
<img alt="Historia pojazdu po VIN" src="/theme/automo/img/assetic/c12c6a9.png?1610445147"/><strong>Historia pojazdu</strong><span>Zawiera informacje o wypadkowości pojazdu, dane z aukcji i ogłoszeń oraz źródeł niezależnych.</span>
</div>
</div>
<div class="col-sm-3">
<div class="head-report">
<img alt="Wezwania i akcje serwisowe" src="/theme/automo/img/assetic/fbfca63.png?1610445147"/><strong>Wezwania i akcje serwisowe</strong><span>Lista wezwań i akcji serwisowych prowadzonych przez producenta pojazdu.</span>
</div>
</div>
<div class="col-sm-3">
<div class="head-report">
<img alt="Zdjęcia pojazdu po nr VIN" src="/theme/automo/img/assetic/7798b7a.png?1610445147"/><strong>Zdjęcia pojazdu</strong><span>Chronologiczna lista pogrupowanych wystąpień zdjęć sprawdzanego pojazdu.</span>
</div>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-4">
<div class="head-report">
<img alt="Analiza numeru VIN" src="/theme/automo/img/assetic/ba51b5f.png?1610445147"/><strong>Analiza numeru VIN</strong><span>Sprawdzenie poprawności numeru VIN oraz dane producenta pojazdu.</span>
</div>
</div>
<div class="col-sm-4">
<div class="head-report">
<img alt="Dane techniczne dekoder VIN decoder" src="/theme/automo/img/assetic/ee4c103.png?1610445147"/><strong>Dane techniczne</strong><span>Rozkodowanie numeru VIN, które zawiera szereg informacji techczninych pojazdu.</span>
</div>
</div>
<div class="col-sm-4">
<div class="head-report">
<img alt="Opcje wyposażenia po numerze VIN" src="/theme/automo/img/assetic/e91c2b6.png?1610445147"/><strong>Opcje wyposażenia</strong><span>Lista opcji wyposażenia standardowego i opcjonalnego pojazdu.</span>
</div>
</div>
</div>
</div>
</div>
</div><!-- /.carousel -->
</div>
<hr class="hidden-xs"/>
<div class="well hidden-xs" style="margin-bottom:0;">
<div class="row">
<div class="col-sm-4 col-md-4 col-lg-2 center-block">
<img alt="Historia pojazdu dla pojazdów z USA" src="/theme/automo/img/assetic/nmvtis-min.png?1610445147"/> </div>
<div class="col-sm-4 col-md-4 col-lg-2 center-block">
<img alt="Historia pojazdu dla pojazdów z Holandii" src="/theme/automo/img/assetic/852dc9f.png?1610445147"/> </div>
<div class="col-sm-4 col-md-4 col-lg-2 center-block">
<img alt="Wezwania i akcje serwisowe dla pojazdów z USA" src="/theme/automo/img/assetic/4809df3.png?1610445147"/> </div>
<div class="col-sm-4 col-md-4 col-lg-2 center-block">
<img alt="Historia przeglądów technicznych po numerze vin" src="/theme/automo/img/assetic/fd2f1d3.png?1610445147"/> </div>
<div class="col-sm-4 col-md-4 col-lg-2 center-block">
<img alt="Wezwania i akcje serwisowe dla pojazdów Europejskich" src="/theme/automo/img/assetic/2862169.png?1610445147"/> </div>
<div class="col-sm-4 col-md-4 col-lg-2 center-block">
<img alt="Dane techniczne z dekodera VIN decoder" src="/theme/automo/img/assetic/c3574bf.png?1610445147"/> </div>
</div>
</div>
</div>
<div class="container"><!-- Site footer -->
<div class="clearfix"></div>
<hr/>
<div class="footerMy">
<div class="text-center">
<ul class="list-inline list-inline-space">
<li><small><a href="/regulamin">» Regulamin serwisu</a></small></li>
<li><small><a href="/rejestracja-programu-partnerskiego">» Dołącz do programu partnerskiego</a></small></li>
<li><small><a href="/polityka-prywatnosci">» Polityka prywatności i Cookies</a></small></li>
<li><small><a href="javascript:$.gdprcookie.display()">» Ustawienia Cookies</a></small></li>
<li><small><a href="/faq">» F.A.Q.</a></small></li>
<li><small><a href="/numer-vin">» Numer VIN</a></small></li>
<li><small><a href="/kontakt">» Kontakt</a></small></li>
</ul>
<hr class="visible-xs"/>
<ul class="list-inline list-inline-space">
<li><small><a href="https://www.facebook.com/automopl.historia.pojazdu" rel="noopener" target="_blank"><i class="fa fa-2x fa-facebook-square" style="color:#3b5998;"></i> Nasz Facebook</a></small></li>
<li><small><a href="https://twitter.com/Automopl" rel="noopener" target="_blank"><i class="fa fa-2x fa-twitter-square" style="color:#0084B4;"></i> Nasz Twitter</a></small></li>
</ul>
</div>
<div class="text-center">
<p><small>© 2010-2021 Automo.pl - Historia pojazdu. Wszelkie prawa zastrzeżone. </small></p>
</div>
</div>
</div>
<script src="/js/extern.script.js?1610445147"></script> <script src="/js/base.js?1610445147"></script> <script src="/theme/automo/js/theme.js?1610445147"></script> <script src="/js/gdpr/gdpr-cookie-bs3.js?1610445147"></script> <script type="text/javascript">
//<![CDATA[
window.app = {"company":{"companyName":"Autoiso","companyCity":"Katowice 40-142","companyStreet":"Gnie\u017anie\u0144ska 12","companyEmailEng":"support@autoiso.pl","companyEmail":"pomoc@autoiso.pl"},"canaryPassArg":{"canaryText":null}};
//]]>
</script>
<script src="/theme/automo/js/gdpr/gdpr-cookie-init.js?1610445147"></script> <script src="/js/canary.js?1610445147"></script> <script>
    $(document.body)
        .on('gdpr:show', function() {
            $('.overlay').addClass('gdpr');
            $('body').css('overflow','hidden');
        })
        .on('gdpr:accept', function() {
            $('.overlay').removeClass('gdpr');
            $('body').css('overflow','auto');
        })
        .on('gdpr:advanced', function() {

        });

</script> <script src="https://www.google.com/recaptcha/api.js?render=6LfcTvMZAAAAAHm8O1nOViZpfPXD9a9tIQfddABm"></script><script>
grecaptcha.ready(function() {
            grecaptcha.execute('6LfcTvMZAAAAAHm8O1nOViZpfPXD9a9tIQfddABm', {action: 'vinchecker'}).then(function(token) {

                $.ajax({
                    type: "GET",
                    url: "/recaptcha_verify/"+token,
                    dataType: 'json',
                    cache: false,
                    crossDomain: true,
                    contentType: false,
                    processData: false
                })
                .always(function(data, textStatus, jqXHR){
                                    });
            });
        });  
</script> <script>

    $(document).ready(function () {

        $('#UserLoginForm #UserEmail').append('<input type="hidden" id="reCaptchaPassed" name="reCaptchaPassed" value="1">');
        $('#UserLoginForm').submit(function (e) {
//            var userType = '';
            var userEmail = $('#UserLoginForm #UserEmail').val();
        });

        LoginFailLogging();
    });

    function LoginFailLogging(){

        var LoginFailMessage = 'Nieprawidłowe dane logowania';


            if ($('.alert').html() == LoginFailMessage){
                gtag('event', 'Fail', {
                    'event_category': 'Login'
                });
            }
    }
    </script>
<div id="fb-root"></div>
<script type="text/javascript">

      	$('#featuresCarousel').carousel({
        	  interval: 6000
      	});


      	$(document).on('click', '.yamm .dropdown-menu', function(e) {
        	  e.stopPropagation();
        });

	    </script>
<link href="/manifest.json" rel="manifest"/>
</body>
</html>
