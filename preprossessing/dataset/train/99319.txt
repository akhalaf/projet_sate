<!DOCTYPE html>
<html lang="zh" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="no-cache" http-equiv="pragma"/>
<meta content="无差别同人站拥有国内各类同人展会、同人制品资讯，致力于为广大的同人文化爱好者提供一个信息交流服务平台。" name="description"/>
<meta content="同人,同人展,同人本,漫展,制品,only展,cp,comicup,魔都同人祭,东方,国产,原创,衍生品,粉丝经济" name="keywords"/>
<title> 无差别同人站</title>
<link href="https://cppstatic.allcpp.cn/cn/static/img/allcpp/favicon.ico" media="screen" rel="shortcut icon" type="image/x-icon"/>
<script src="https://cppstatic.allcpp.cn/cn/static/js/jquery-1.11.3/jquery.min.js" type="text/javascript"></script>
<script src="https://cppstatic.allcpp.cn/cn/static/js/jquery-1.11.3/jquery.cookie.js" type="text/javascript"></script>
<script src="https://cppstatic.allcpp.cn/cn/static/js/jquery-1.11.3/moment.min.js" type="text/javascript"></script>
<script src="https://cppstatic.allcpp.cn/cn/static/js/jquery-1.11.3/socket.io-2.0.3.js" type="text/javascript"></script>
<script src="https://cppstatic.allcpp.cn/cn/static/js/allcpp/common.js?v20210114091659" type="text/javascript"></script>
<script src="https://cppstatic.allcpp.cn/cn/static/service.js?v20210114091659" type="text/javascript"></script>
<script>
function isIE(){ 
  var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
    var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1; //判断是否IE<11浏览器
    var isEdge = userAgent.indexOf("Edge") > -1 && !isIE; //判断是否IE的Edge浏览器
    var isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf("rv:11.0") > -1;
    return isIE|| isIE11;
}
if(isIE()){
  window.location.href = "https://store.awsl.wang/browser.html"
}
</script>
<link href="https://cppstatic.allcpp.cn/cn/static/css/allcpp/common.css?v20210114091660" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
var staticRoot= "https://cppstatic.allcpp.cn/cn/static/";
var webRoot = "https://www.allcpp.cn/";
var picUrlRoot = "https://imagecdn.allcpp.cn/upload";
var isWebUrl = 1;
var userEnabled=3;
</script> <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" name="viewport"/>
<link href="https://cppstatic.allcpp.cn/cn/static/css/allcpp/login/login.css?v20210114092" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="bgCover" style="display: none;"></div>
<div id="main" style="background-image:url(https://imagecdn.allcpp.cn/upload/2020/12/cbd80b52-91ea-4973-bd02-ab10450bde7d.png)">
<div class="container">
<div class="login-status" id="loginbg">
<div class="cpp-home-links" style="display:none">
<a href="https://www.allcpp.cn/w/m.do" target="_blank"><i class="cpp-i-work"></i><span>后花园　/　图文</span></a>
<a href="https://www.allcpp.cn/allcpp/djs/list.do" target="_blank"><i class="cpp-i-djs"></i><span>扫本　/　制品</span></a>
<a href="https://www.allcpp.cn/allcpp/event/eventlist.do" target="_blank"><i class="cpp-i-event"></i><span>聚会　/　活动</span></a>
<a href="//store.allcpp.cn" target="_blank"><i class="cpp-i-lolita"></i><span>服装　/　商城</span></a>
</div>
<div id="logo"></div>
<!--登录框-->
<div id="login">
<input id="l-user-id" placeholder="手机号/邮箱/id" type="text"/>
<input id="l-user-pd" placeholder="密码" type="password"/>
<div class="row error" style="display:none;">哎呀出错了,请再试一次噢~</div>
<button id="l-user-login">登录</button>
<!-- </form> -->
<div class="row info">
<a href="javascript:showPasswordBack()">忘记密码?</a>
<a href="javascript:showRegister()" style="float: right">注册新账号</a>
</div>
<div class="row links">
<a href="//www.allcpp.cn/s/3432.do" target="_blank">什么是同人？</a>
<a href="//www.allcpp.cn/w/141382.do?redirect=false" style="display:none" target="_blank">CPP无差别同人站 社区规则</a>
</div>
</div>
<!--手机注册框-->
<div id="register" style="display: none;">
<div class="tel" style="font-size:0">
<select id="r-user-region">
<option value="1">美国</option>
<option value="32">比利时</option>
<option value="33">法国</option>
<option value="39">意大利</option>
<option value="60">马来西亚</option>
<option value="61">澳大利亚</option>
<option value="65">新加坡</option>
<option value="81">日本</option>
<option value="82">韩国</option>
<option selected="selected" value="86">中国大陆</option>
<option value="852">中国香港</option>
<option value="853">中国澳门</option>
<option value="886">中国台湾</option>
</select>
<input id="r-user-id" placeholder="请输入手机号" type="tel"/>
</div>
<div class="verify">
<button id="r-user-vfg">获取验证码
                        </button><input id="r-user-vf" placeholder="请输入验证码" type="text"/>
</div>
<input id="r-user-pd" placeholder="请输入密码" type="password"/>
<input id="r-user-pds" placeholder="请再次输入密码" type="password"/>
<div class="row check">
<input name="agree-user" type="checkbox"/>
<span>我已同意</span>
<a href="//www.allcpp.cn/w/156176.do" target="_blank">《用户协议》</a>
</div>
<!-- </form> -->
<div class="row error" style="display:none;">哎呀出错了,请再试一次噢~</div>
<button id="r-user-register">注册</button>
<div class="row info">
<a href="javascript:showOkMsg('邮箱注册正在维护升级中')">邮箱注册</a>
<a href="javascript:showLogin()" style="float: right;">已有账号（可使用ComiCUP官网账号）</a>
</div>
</div>
<!-- 邮箱注册 -->
<div id="register-mail" style="display:none">
<input id="r-mail" placeholder="请输入邮箱" type="text"/>
<input id="r-user-mail-pd" placeholder="请输入密码" type="password"/>
<input id="r-user-mail-pds" placeholder="请再次输入密码" type="password"/>
<div class="row check">
<input name="agree-user" type="checkbox"/>
<span>我已同意</span>
<a href="//www.allcpp.cn/w/156176.do" target="_blank">《用户协议》</a>
</div>
<!-- </form> -->
<div class="row error" style="display:none;">哎呀出错了,请再试一次噢~</div>
<button id="r-user-mail-register">注册</button>
<div class="row info">
<a href="javascript:showRegister()">手机注册</a>
<a href="javascript:showLogin()" style="float: right;">已有账号（可使用ComiCUP官网账号）</a>
</div>
</div>
<div id="mail-p" style="font-size: 25px;padding-top: 50px; display:none">
<p id="mail-p" style="color: #fff;margin-bottom:30px;">邮件已发送，请注意查收。</p>
<button onclick="window.location.reload();">返回</button>
</div>
<!--找回密码框-->
<div id="passwordBack" style="display:none;">
<div class="tel">
<select id="p-user-region">
<option value="1">美国</option>
<option value="32">比利时</option>
<option value="33">法国</option>
<option value="39">意大利</option>
<option value="60">马来西亚</option>
<option value="61">澳大利亚</option>
<option value="65">新加坡</option>
<option value="81">日本</option>
<option value="82">韩国</option>
<option selected="selected" value="86">中国大陆</option>
<option value="852">中国香港</option>
<option value="853">中国澳门</option>
<option value="886">中国台湾</option>
</select><input id="p-user-id" placeholder="请输入绑定的手机号码" type="tel"/>
</div>
<div class="verify">
<button id="p-user-vfg">获取验证码
                        </button><input id="p-user-vf" placeholder="请输入验证码" type="text"/>
</div>
<input id="p-user-pd" placeholder="请输入新密码" type="password"/>
<input id="p-user-pds" placeholder="请再次输入新密码" type="password"/>
<div class="row error" style="display:none;">哎呀出错了,请再试一次噢~</div>
<button id="p-user-back">重置密码</button>
<div class="row info">
<a href="javascript:showEmailPwdBack()">邮箱忘记密码</a>
<a href="javascript:showLogin()" style="float: right">已有账号（可使用ComiCUP官网账号）</a>
</div>
</div>
<!-- 邮箱找回密码 -->
<div id="password-mail-back" style="display:none;">
<input id="p-user-mail" placeholder="请输入邮箱" type="text"/>
<div class="row error" style="display:none;">哎呀出错了,请再试一次噢~</div>
<button id="p-user-mail-back">申请密码重置</button>
<div class="row info">
<a href="javascript:showPasswordBack()">手机忘记密码</a>
<a href="javascript:showLogin()" style="float: right">已有账号（可使用ComiCUP官网账号）</a>
</div>
</div>
<!-- 信息填写 -->
<div id="user-info" style="display:none">
<div class="u-title">
<p>欢迎加入CPP</p>
<label>完善个人信息，让囧猫子更了解你~</label>
</div>
<div class="u-txt">昵称</div>
<input class="u-nickname" placeholder="请输入您的昵称" type="text"/>
<div class="u-txt">性别</div>
<div class="u-sex">
<div class="u-female"></div>
<div class="u-male"></div>
</div>
<div class="u-error">
<div class="row error" style="display:none;">请输入您的昵称</div>
</div>
<button class="cpp-btn" id="go-true">答题成为正式会员</button>
<a class="go-cpp" id="go-cpp">进入CPP</a>
</div>
<div id="successInfo" style="display:none;"></div>
</div>
</div>
<div class="floatWin" id="login-p" style="display:none;">
<div class="del"><b>×</b></div>
<div class="content">
<input id="l-p-user-id" placeholder="手机号/id" type="text"/>
<input id="l-p-user-pd" placeholder="密码" type="password"/>
<div class="row error" style="display:none;">哎呀出错了,请再试一次噢~</div>
<button id="l-p-user-login">立即登录</button>
<div class="row info">
<a id="login-p-forget" target="_blank">忘记密码?</a>
<a id="login-p-register" style="float: right">注册新账号</a>
</div>
</div>
</div>
<div class="infoMsg" id="infoMsg" style="display:none;"></div>
<div class="warning" id="warning" style="display:none;">xxx错误</div> <div id="footer">
<img height="32" src="https://cppstatic.allcpp.cn/cn/static/img/allcpp/icon/foot-logo.png"/>
<div class="copy">
<a href="http://www.beian.miit.gov.cn" target="_blank">沪ICP备15000050号-2</a>
                      
                    沪公网安备 31011002004615号   
                    沪网文（2020）1689-127号<br/>
                Copyright (C) www.allcpp.cn All rights reserved.
            </div>
</div>
<script src="https://cppstatic.allcpp.cn/cn/static/js/allcpp/login/login.js?v20210114092"></script>
</div></body>
</html>
