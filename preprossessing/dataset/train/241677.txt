<!DOCTYPE html>
<html>
<head>
<title>Www.bloxawards.com - Earn Robux by doing simple tasks!</title>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="Earn Robux by completing quizzes, downloading games on your mobile device and watching videos!" name="description"/>
<meta content="Baheeg" name="author"/>
<meta content="Www.bloxawards.com - Earn Robux by doing simple tasks!" property="og:title"/>
<meta content="Earn Robux by completing quizzes, downloading games on your mobile device and watching videos!" property="og:description"/>
<meta content="https://www.bloxawards.com/new-logo-sm.png" property="og:image"/>
<meta content="https://www.bloxawards.com.com" property="og:url"/>
<meta content="emid9NDatbzSVS8tGWzZD5JWpRgOccnmUuOuscGu" name="csrf-token"/>
<!-- Favicon icon -->
<link href="/img/new-logo-sm.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/css/bootstrap.css" rel="stylesheet"/>
<link href="/css/toastify.min.css" rel="stylesheet"/>
<link href="/css/custom.css?cb=28" rel="stylesheet"/>
<link href="https://cdn.jsdelivr.net/npm/@sweetalert2/theme-default/default.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500&amp;display=swap" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Nunito:400,600|Open+Sans:400,600,700" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js"></script>
<script src="/js/jquery.js"></script>
</head> <body>
<section class="content mt-0 mb-5 " id="content">
<!-- Header -->
<nav class="main-navbar navbar navbar-expand-lg m-auto p-0 w-100">
<button aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler collapsed" data-target="#navbarNav" data-toggle="collapse" type="button">
<section class="d-none">
<span class="navbar-toggler-icon" style="color: white">
<svg focusable="false" height="30" role="img" viewbox="0 0 30 30" width="30" xmlns="http://www.w3.org/2000/svg">
<title>Menu</title>
<path d="M4 7h22M4 15h22M4 23h22" stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"></path>
</svg>
</span>
</section>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand d-block pr-3 pl-3 m-auto" href="https://bloxawards.com">
<img src="/img/logo.png" style="height: 36px;"/>
</a>
<div class="navbar-collapse collapse" id="navbarNav">
<hr class="d-xs-block d-lg-none mt-0 mb-0 d-none" style="border-top-width: 1px; border-top: 1px solid rgba(0, 0, 0, 0.2);"/>
<ul class="navbar-nav navbar-mobile-padding ml-auto">
<li class="nav-item d-md-block d-lg-none">
<a class="nav-link" data-target="#register-modal" data-toggle="modal" href="#" id="mobile_nav_register">
<p class="mb-0"><span aria-hidden="true" class="fas fa-user-plus mr-2"></span> Register</p>
</a>
</li>
<li class="nav-item d-md-block d-lg-none">
<a class="nav-link" data-target="#login-modal" data-toggle="modal" href="#" id="mobile_nav_login">
<p class="mb-0"><span aria-hidden="true" class="fas fa-sign-in-alt mr-2"></span> Login</p>
</a>
</li>
<li class="nav-item d-none d-lg-inline-block mr-0">
<a class="nav-link" data-target="#register-modal" data-toggle="modal" href="#">
<div class="btn signup-btn btn-success"><span class="fas fa-user-plus mr-2"></span>REGISTER</div>
</a>
</li>
<li class="nav-item d-none d-lg-inline-block">
<a class="nav-link" data-target="#login-modal" data-toggle="modal">
<div class="btn login-btn btn-light"><span class="fas fa-sign-in-alt mr-2"></span>LOGIN</div>
</a>
</li>
</ul>
</div>
</nav>
<div class="modal fade" id="login-modal">
<div class="modal-dialog">
<div class="modal-content">
<!-- Modal Header -->
<div class="modal-header" style="padding: 0.9rem 0.9rem;">
<h4 class="modal-title" style="font-weight: 400">Login</h4>
<button class="close" data-dismiss="modal" type="button">×</button>
</div>
<!-- Modal body -->
<div class="modal-body py-2 mb-2">
<div class="row mt-3">
<div class="col-12">
<button class="btn btn-block text-light btn-danger" data-target="#roblox-auth-modal" data-toggle="modal" onclick="$('#login-modal').modal('hide');" type="submit"><span class="float-left" style="padding: 5px"> <img src="/img/roblox-icon.png" style="height: 28px;"/></span><span style="display: flex;justify-content: center;align-items: center;margin-top: 5px;font-size:18px">Enter with Username</span></button>
</div>
<div class="col-12" style="margin-top: 1rem">
<button class="btn btn-block text-light auth-google-btn" onclick="location.href='/auth/google'" type="submit"><span class="float-left" style="background-color: white;padding: 5px"><img src="/img/google-icon.png" style="height: 28px;"/></span><span style="display: flex;justify-content: center;align-items: center;margin-top: 5px;font-size:18px">Sign in with Google</span></button>
</div>
<div class="col-12 d-none" style="margin-top: 1rem">
<button class="btn btn-block text-light auth-discord-btn" onclick="location.href='/login/discord'" type="submit"><span class="float-left" style="background-color: white;padding: 5px"><img src="/img/discord-icon.svg" style="height: 28px;"/></span><span style="display: flex;justify-content: center;align-items: center;margin-top: 5px;font-size:18px">Sign in with Discord</span></button>
</div>
</div>
<h5 class="centered-line-words font-weight-normal" style="color: black; margin-top: 2rem; margin-bottom: 2rem; font-size: 20px;"><span style="background-color: #eef5f9;">or</span></h5>
<p style="margin-bottom: 0.5rem"><small></small></p>
<p>
<input autocomplete="off" class="form-control" id="email_login_email" name="email_login_email" placeholder="Email address" type="email"/>
</p>
<p>
<input autocomplete="off" class="form-control" id="email_login_password" name="email_login_password" placeholder="Password" type="password"/>
</p>
<div class="row mb-2">
<div class="col-12 col-md-6" style="margin-top:12.5px;">
<button class="btn btn-primary-bright text-light btn-block fw-500" id="emailLoginBtn" style="font-size:22px;min-height:40px" type="submit"><span aria-hidden="true" class="fas fa-sign-in-alt mr-2"></span>Login</button>
</div>
<div class="col-12 col-md-6" style="margin-top:12.5px;">
<button class="btn btn-secondary text-light btn-block fw-500" data-target="#register-modal" data-toggle="modal" onclick="$('#login-modal').modal('hide');" style="font-size:22px;min-height:40px" type="submit"><span aria-hidden="true" class="fas fa-user-plus mr-2"></span>Register</button>
</div>
<div class="col-12 mx-auto mt-4 text-center">
<a class="text-primary" href="/password/reset" style="font-size:18px">Forgot your Password?</a>
</div>
</div>
<!-- <hr> -->
</div>
</div>
</div>
</div>
<div class="modal fade" id="register-modal">
<div class="modal-dialog">
<div class="modal-content">
<!-- Modal Header -->
<div class="modal-header" style="padding: 0.9rem 0.9rem;">
<h4 class="modal-title" style="font-weight: 400">Register</h4>
<button class="close" data-dismiss="modal" type="button">×</button>
</div>
<!-- Modal body -->
<div class="modal-body py-2 mb-2" style="background-color: #eef5f9;">
<div class="row mt-3">
<div class="col-12">
<button class="btn btn-block text-light btn-danger" data-target="#roblox-auth-modal" data-toggle="modal" onclick="$('#register-modal').modal('hide');" type="submit"><span class="float-left" style="padding: 5px"> <img src="/img/roblox-icon.png" style="height: 28px;"/></span><span style="display: flex;justify-content: center;align-items: center;margin-top: 5px;font-size:18px">Enter with Username</span></button>
</div>
<div class="col-12" style="margin-top: 1rem">
<button class="btn btn-block text-light auth-google-btn" onclick="location.href='/auth/google'" type="submit"><span class="float-left" style="background-color: white;padding: 5px"><img src="/img/google-icon.png" style="height: 28px;"/></span><span style="display: flex;justify-content: center;align-items: center;margin-top: 5px;font-size:18px">Sign up with Google</span></button>
</div>
<div class="col-12 d-none" style="margin-top: 1rem">
<button class="btn btn-block text-light auth-discord-btn" onclick="location.href='/login/discord'" type="submit"><span class="float-left" style="background-color: white;padding: 5px"><img src="/img/discord-icon.svg" style="height: 28px;"/></span><span style="display: flex;justify-content: center;align-items: center;margin-top: 5px;font-size:18px">Sign up with Discord</span></button>
</div>
</div>
<h5 class="centered-line-words font-weight-normal" style="color: black; margin-top: 2rem; margin-bottom: 1rem; font-size: 20px;"><span style="background-color: #eef5f9;">or</span></h5>
<!-- <p>
                 <input autocomplete="off" type="username" id="email_register_name" name="email_register_name" class="form-control" placeholder="Nickname">
               </p>
               <p>
               <input autocomplete="off" type="email" id="email_register_email" name="email_register_email" class="form-control" placeholder="Email address">
               </p>
               <p>
               <input autocomplete="off" type="password" id="email_register_password" name="email_register_password" class="form-control" placeholder="Password">
               </p>
               <p>
               <input autocomplete="off" type="password" id="email_register_password_confirmation" name="email_register_password_confirmation" class="form-control" placeholder="Confirm Password">
               </p> -->
<div class="row mb-2">
<!-- <div class="col-12 col-md-6" style="margin-top:12.5px;">
                  <button id="emailRegisterBtn" type="submit" class="btn btn-primary-bright text-light btn-block fw-500" style="font-size:22px;min-height:40px"><span class="fas fa-user-plus mr-2" aria-hidden="true"></span>Register</button>
               </div> -->
<div class="col-12 text-center" style="margin-top:12.5px;">
<p data-target="#login-modal" data-toggle="modal" onclick="$('#register-modal').modal('hide');" role="button" style="font-size: 20px; outline: none !important; color: #0868d0; cursor: pointer" tabindex="0">Have an account? Click here to login!</p>
</div>
</div>
<!-- <hr> -->
</div>
</div>
</div>
</div>
<div class="modal fade" id="roblox-auth-modal">
<div class="modal-dialog">
<div class="modal-content">
<!-- Modal Header -->
<div class="modal-header">
<h4 class="modal-title" style="font-weight: 400">Enter with ROBLOX</h4>
<button class="close" data-dismiss="modal" type="button">×</button>
</div>
<!-- Modal body -->
<div class="modal-body py-2 mb-2 mt-3" style="background-color: #eef5f9;">
<div class="banner alert bg-secondary border-0 fade show mb-3" id="roblox_password_message" role="alert" style="display:none;">
<p class="mb-0">This account is verified, please type your Bloxawards password. <a class="font-weight-bold" href="/password/reset" style="color: white; text-decoration:underline;">Forgot your password?</a></p>
</div>
<p class="text-center w-100 mb-0">
<img class="avatar-circle mb-3" id="roblox_form_avatar" src="https://www.roblox.com/headshot-thumbnail/image?userId=1&amp;width=420&amp;height=420&amp;format=png" style="display:none"/>
</p>
<p class="mb-1">
<input autocomplete="off" class="form-control" id="roblox_username" name="roblox_username" placeholder="ROBLOX Username" type="text"/>
</p>
<p class="text-danger" id="invalid_roblox_username_message" style="display:none;font-size: 16px;font-weight: 500;">This ROBLOX username does not exist!</p>
<p>
<input autocomplete="off" class="form-control mt-2" id="roblox_password" name="roblox_password" placeholder="Password" style="display:none" type="password"/>
</p>
<p class="text-danger" id="invalid_password_message" style="display:none;font-size: 16px;font-weight: 500;">Invalid password!</p>
<div class="row mb-2">
<div class="col-12 col-md-6" style="margin-top:12.5px;">
<button class="btn btn-primary-bright text-light btn-block fw-500" id="enterWithRobloxBtn" style="font-size:22px;min-height:40px" type="submit"><span aria-hidden="true" class="fas fa-user-plus mr-2"></span>Enter</button>
</div>
<div class="col-12 col-md-6" style="margin-top:12.5px;">
<button class="btn btn-secondary text-light btn-block fw-500" data-target="#login-modal" data-toggle="modal" onclick="$('#roblox-auth-modal').modal('hide');" style="font-size:22px;min-height:40px" type="submit"><span aria-hidden="true" class="fas fa-sign-in-alt mr-2"></span>Back</button>
</div>
</div>
<!-- <hr> -->
</div>
</div>
</div>
</div>
<!-- Banner -->
<!-- Page content -->
<div class="p-3 pt-0 text-center text-light landing-hero">
<h1 class="display-4 font-weight-bold text-uppercase">Free <span class="text-success-hero">R$</span> from... <span id="earning-method"></span></h1>
<div class="btn btn-xlg btn-success shadow" data-target="#register-modal" data-toggle="modal" style="font-weight: 700"><span aria-hidden="true" class="fas fa-user-plus mr-3"></span>START NOW</div>
</div>
<div class="row px-md-5 landing-steps" style="margin: unset !important;background-image: url('https://i.imgur.com/ae6Sd9U.png');
    background-repeat: repeat;
    background-position-y: center;
    background-position-x: center;
    background-size: 850px;
   ">
<style>
          @media  screen and (min-width: 768px) {
            .landing-steps {
              background-position-y: bottom !important;
              background-repeat: repeat-x !important;
            }
          }
    </style>
<div class="col-12 col-md-4 text-dark text-center pt-4">
<div class="d-flex h-100" style="flex-direction: column;">
<h2 class="display-5">Create Account</h2>
<p class="mb-5" style="font-size: 1.2rem">Sign up with your ROBLOX username or Google Account</p>
<img class="img-fluid shadow mx-auto mt-auto w-100" src="https://i.imgur.com/v2fGoNd.png" style="max-width: 349px !important; border-top-right-radius: 21px; border-top-left-radius: 21px"/>
</div>
</div>
<div class="col-12 col-md-4 text-lightn text-center pt-4">
<div class="d-flex h-100" style="flex-direction: column;">
<h2 class="display-5">Complete Tasks</h2>
<p class="mb-5" style="font-size: 1.2rem">Complete tasks for Robux</p>
<img class="img-fluid shadow mx-auto mt-auto w-100" src="https://i.imgur.com/ayLEbYu.png" style="max-width: 354px !important; border-top-right-radius: 21px; border-top-left-radius: 21px"/>
</div>
</div>
<div class="col-12 col-md-4 text-dark text-center pt-4">
<div class="d-flex h-100" style="flex-direction: column;">
<h2 class="display-5">Withdraw Robux</h2>
<p class="mb-5" style="font-size: 1.2rem">Transfer your Robux to your ROBLOX account</p>
<img class="img-fluid shadow mx-auto mt-auto w-100" src="https://i.imgur.com/UF7x38h.png" style="max-width: 354px !important; border-top-right-radius: 21px; border-top-left-radius: 21px"/>
</div>
</div>
</div>
<div class="container landing-second d-none">
<h4 class="text-uppercase text-center mt-4" style="color: rgb(33, 37, 41);">💰 Earn R$ by doing fun tasks 💰</h4>
<hr/>
<div class="row">
<div class="col-12 col-md-6 col-lg-3 text-center text-light p-3">
<div class="landing-method landing-method-games">
<span class="fas fa-gamepad" style="font-size: 4rem;"></span>
<h2>Games</h2>
</div>
</div>
<div class="col-12 col-md-6 col-lg-3 text-center text-light p-3">
<div class="landing-method landing-method-videos">
<span class="fas fa-video" style="font-size: 4rem;"></span>
<h2>Videos</h2>
</div>
</div>
<div class="col-12 col-md-6 col-lg-3 text-center text-light p-3">
<div class="landing-method landing-method-surveys">
<span class="fas fa-poll-h" style="font-size: 4rem;"></span>
<h2>Surveys</h2>
</div>
</div>
<div class="col-12 col-md-6 col-lg-3 text-center text-light p-3">
<div class="landing-method landing-method-contests">
<span class="fas fa-trophy" style="font-size: 4rem;"></span>
<h2>Contests</h2>
</div>
</div>
</div>
</div>
<script>
   window.addEventListener('load', function() {
       const instance = new TypeIt('#earning-method', {
           strings: ['doing quizzes', 'watching videos', 'playing games', 'winning contests'],
           speed: 100,
           loopDelay: [500, 4000],
           nextStringDelay: [4000, 500],
           loop: true,
           breakLines: false,
           waitUntilVisible: true,
       }).go();
   });
</script>
</section>
<!-- Footer -->
<footer class="page-footer font-small text-light" style="background-color: #0a5cb5">
<div class="footer-container container">
<div class="row">
<div class="col-6 col-md-4 mb-2">
<h3>About us</h3>
<ul>
<li><a class="text-light" href="/help/getting-started">About Us</a></li>
<li><a class="text-light" href="/terms-of-service">Terms of Service</a></li>
<li><a class="text-light" href="/terms-of-service#privacypolicy">Privacy Policy</a></li>
<li><a class="text-light" href="/contact-us">Business Contact</a></li>
</ul>
</div>
<div class="col-6 col-md-4 mb-2">
<h3>Support</h3>
<ul>
<li><a class="text-light" href="/contact-us">Contact Us</a></li>
<li><a class="text-light" href="/help">Help Centre</a></li>
<li><a class="text-light" href="/help/referrals-and-sponsorships">Sponsorships</a></li>
<li><a class="text-light" href="/help/missing-points">Where is my R$?</a></li>
</ul>
</div>
<div class="col-6 col-md-4 mb-2">
<h3>Socials</h3>
<ul>
<li><a class="text-light" href="https://discord.gg/TrSZjcs"><i aria-hidden="true" class="fab fa-discord"></i>Discord</a></li>
<li><a class="text-light" href="https://www.youtube.com/channel/UC8vte6A2PF_4DQ0A1BWroKg"><i aria-hidden="true" class="fab fa-youtube"></i>Youtube</a></li>
<li><a class="text-light" href="https://twitter.com/blox_awards"><i aria-hidden="true" class="fab fa-twitter"></i>Twitter</a></li>
<li><a class="text-light" href="https://instagram.com/bloxawards"><i aria-hidden="true" class="fab fa-instagram"></i>Instagram</a></li>
</ul>
</div>
</div>
<div class="row">
<div class="col-12">
<h6 class="text-center">© 2021 | Bloxawards</h6>
<h6 class="text-center">We are not affiliated with ROBLOX Corporation.</h6>
</div>
</div>
</div>
</footer>
<!-- Core JS dependencies -->
<script src="/js/popper.js"></script>
<script src="/js/bootstrap.js"></script>
<script src="/js/toastify.min.js"></script>
<script src="/js/promise.js"></script>
<script src="/js/custom.js?cb=13"></script>
<script src="/js/login.js?cb=2"></script>
<script src="/js/preferences.js?cb=3"></script>
<script src="/js/swal2.js"></script>
<script async="" src="https://cdn.onesignal.com/sdks/OneSignalSDK.js"></script>
<script src="/js/shepherd.js"></script>
<!-- Page Specific JS dependencies -->
<script src="/js/typeit.min.js"></script>
</body>
</html>