<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-US">
<![endif]--><!--[if IE 8]>
<html class="ie ie8" lang="en-US">
<![endif]--><!--[if !(IE 7) | !(IE 8) ]><!--><html lang="en-US">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Atria NYC |   Page not found</title>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<link href="https://atrianyc.com/xmlrpc.php" rel="pingback"/>
<link href="https://atrianyc.com/wp-content/themes/atria/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600" rel="stylesheet" type="text/css"/>
<link href="https://atrianyc.com/wp-content/themes/atria/images/favicon.png" rel="icon" sizes="32x32"/>
<link href="https://atrianyc.com/wp-content/themes/atria/images/favicon.png" rel="icon" sizes="192x192"/>
<link href="https://atrianyc.com/wp-content/themes/atria/images/favicon.png" rel="apple-touch-icon-precomposed"/>
<meta content="https://atrianyc.com/wp-content/themes/atria/images/favicon.png" name="msapplication-TileImage"/>
<!--[if lt IE 9]>
    <script src="https://atrianyc.com/wp-content/themes/atria/js/html5.js" type="text/javascript"></script>
    <![endif]-->
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/atrianyc.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.0.11"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://atrianyc.com/wp-includes/css/dist/block-library/style.min.css?ver=5.0.11" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://atrianyc.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.1" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://atrianyc.com/wp-content/themes/atria/js/slick/slick.css?ver=1.0.0" id="jquery_slick_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://atrianyc.com/wp-content/themes/atria/js/shadowbox/shadowbox.css?ver=1.0.0" id="jquery_shadowbox_style-css" media="all" rel="stylesheet" type="text/css"/>
<script>if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script><script src="https://atrianyc.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://atrianyc.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://atrianyc.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://atrianyc.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://atrianyc.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.0.11" name="generator"/>
</head>
<!-- Body starts here -->
<body class="error404" data-rsssl="1" id="top" style="background-position:fixed; background:url(https://atrianyc.com/wp-content/themes/atria/images/background-darker.jpg) fixed center top no-repeat transparent; background-size:cover; ">
<div class="hfeed site" id="ultra-wrapper"><!-- This Container Ends Before the Closing Body Tag -->
<div id="wrap">
<div id="page">
<header id="main-header">
<div class="header-w">
<div class="row">
<div class="header-i">
<div class="row-logo">
<a href="https://atrianyc.com"><img alt="ATRIA Logo" src="https://atrianyc.com/wp-content/themes/atria/images/atria-logo-2.png"/></a>
</div>
<div class="row-menu">
<div class="row-menu-i">
<div class="menu-holder">
<a href="#" id="menu-toggle"></a>
<div class="menu-container">
<div class="menu-main-navigation-container"><ul class="menu" id="main-navigation"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18" id="menu-item-18"><a href="https://atrianyc.com/about/">about</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19" id="menu-item-19"><a href="https://atrianyc.com/featured-properties/">featured properties</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17" id="menu-item-17"><a href="https://atrianyc.com/what-weve-been-up-to/">what we’ve been up to</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20" id="menu-item-20"><a href="https://atrianyc.com/contact-us/">contact us</a></li>
</ul><div class="cl-fl"></div></div>
</div>
</div>
</div>
</div>
<div class="cl-fl"></div>
</div>
</div>
</div>
</header>
<div class="main-area-w home-page-template page-bg">
<div class="main-area-i">
</div><!-- main-area-i -->
</div><!-- main-area-w -->
<footer>
<div class="footer-w">
<div class="row">
<div class="footer-i">
<div class="footer-left">
<div class="frow">
<div class="menu-main-navigation-container"><ul class="fooer-menu" id="footer-navigation"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18"><a href="https://atrianyc.com/about/">about</a> | </li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19"><a href="https://atrianyc.com/featured-properties/">featured properties</a> | </li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17"><a href="https://atrianyc.com/what-weve-been-up-to/">what we’ve been up to</a> | </li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20"><a href="https://atrianyc.com/contact-us/">contact us</a> | </li>
</ul><div class="cl-fl"></div></div>
</div>
<div class="frow">
                ©2017 ATRIANYC 
                <!--
                <ul>
                  <li><a href="#"><img src="https://atrianyc.com/wp-content/themes/atria/images/facebook.png" alt="f"></a></li>
                  <li><a href="#"><img src="https://atrianyc.com/wp-content/themes/atria/images/twitter.png" alt="t"></a></li>
                  <li><a href="#"><img src="https://atrianyc.com/wp-content/themes/atria/images/instagram.png" alt="i"></a></li>
                </ul>
                -->
<div class="cl-fl"></div>
</div>
<div class="cl-fl"></div>
</div>
<div class="footer-right">
<div class="subscribe-form">
<!--
                  <form method="post" action="/">
                    <p>receive updates from atria</p>
                    <div class="email-subscribe">
                      <input type="text" name="name" placeholder="Your Email Address" >
                    </div>
                    <div class="submit-subscribe">
                      <input type="text" name="name" value="SIGN UP" >
                    </div>
                    <div class="cl-fl"></div>                    
                  </form>
                  -->
<section class="widget"><h2 style="display:none;">Subscribe2</h2><div class="search"><form method="post" name="s2formwidget"><input name="ip" type="hidden" value="210.75.253.169"/><span style="display:none !important"><label for="firstname">Leave This Blank:</label><input id="firstname" name="firstname" type="text"/><label for="lastname">Leave This Blank Too:</label><input id="lastname" name="lastname" type="text"/><label for="uri">Do Not Change This:</label><input id="uri" name="uri" type="text" value="http://"/></span><p><label for="s2email">Your email:</label><br/><input id="s2email" name="email" onblur="if (this.value === '') {this.value = 'Enter email address...';}" onfocus="if (this.value === 'Enter email address...') {this.value = '';}" size="20" type="email" value="Enter email address..."/></p><p><input name="subscribe" type="submit" value="Subscribe"/> <input name="unsubscribe" type="submit" value="Unsubscribe"/></p></form>
</div></section> <div class="cl-fl"></div>
</div>
</div>
<div class="cl-fl"></div>
</div>
</div>
</div>
</footer>
</div>
</div><!-- #wrap -->
</div><!-- #ultra-wrapper -->
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/atrianyc.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://atrianyc.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.1" type="text/javascript"></script>
<script src="https://atrianyc.com/wp-admin/admin-ajax.php?action=extje_request_ajax&amp;ver=1.0.202063547" type="text/javascript"></script>
<script src="https://atrianyc.com/wp-content/themes/atria/js/jquery.extends.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://atrianyc.com/wp-content/themes/atria/js/jquery.transit.min.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://atrianyc.com/wp-content/themes/atria/js/jquery.backgroundSize.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://atrianyc.com/wp-content/themes/atria/js/slick/slick.min.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://atrianyc.com/wp-content/themes/atria/js/shadowbox/shadowbox.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://atrianyc.com/wp-content/themes/atria/js/imagesloaded.pkgd.min.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://atrianyc.com/wp-content/themes/atria/js/imagesloaded.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://atrianyc.com/wp-content/themes/atria/js/masonry.pkgd.min.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://atrianyc.com/wp-content/themes/atria/js/custom-functions.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://atrianyc.com/wp-includes/js/wp-embed.min.js?ver=5.0.11" type="text/javascript"></script>
</body>
</html>