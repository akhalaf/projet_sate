<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "82706",
      cRay: "610ff512bc0ad1b3",
      cHash: "434f467a1324cf0",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmFsdGltb3JlY2l0eS5nb3Yv",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "rmAOQWU4k1+8m6hEWBk48WdfqxS8L1wAJc5B1MvLVeaD3/nNcpNZiknr/vew03J1A7Gm0TASi6j1CifSbalGSrj4JycqKb5xdEQB45OQjv38RW9zeqnHncL+37lcGJ+IiSKTAMmmkFN9Dvtf43OCaGGOm3+o/rtA206FNqpUn1M+2KR2g/1gZJXuac6nbh/m4VkA3OfmnIQ6s/IbsWYaCBK5SV5hmnO2xjrhLZwJzbuuu9GNYvT78fFxsPVBHCwx8qLLW0AVve+Zt8YurvIQf+AiCcyJzjP+iJXN5Kc1GfwSxv1TymfSrKfUMw0RX5tVcgGTEYwTtq2t0P83TNmsjH8knU2efyNKtJHGaIkgYhZjydQceGzGEmMbG7biV4m8tzxGsI9UYgJ9RaeH+epfTMZTXYshVfKjPCiHDfJODzSzPUvh3L/EG71Hcq3fevMR4rPIzxBVAvH74fSxrAfzRLyrxecNaxTB7tlMkDsk1j3KoXzwUO0RI0rfpQz9mZxdNRPuO39R3c0Kj6YLXWKtjhrKE1SjqC1fXz8FDBXDyf9VrtzFGh63B3O9QQrf8qo4+AnpjIyA1AmIwDbhNzFJcoOCjBf/H7yK0F7Fm49weKW02f6VfGdgWF6tV2iCsTmSfcbc8LkF5p078xqRXwwqYRNs3V2YIkE/QYkPYQjgDQVa73yysQG9SVpfDcO4rroPnnt6bYAAYOXwk59oBWVis9W1zWguLKhOJs+OT1jM8GhaFKIHCYp2VUQUnpjpvtEI",
        t: "MTYxMDU0OTcxNi45MzkwMDA=",
        m: "jw/WhDwjNosnZC4pVi3AWFWf5opMd2yXqd3JUxHk5N0=",
        i1: "02VUMXd7uFVdjDI6HaXw1A==",
        i2: "iTChCq+QVmqafb6EGx6KeA==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "B9TdQlThbk/Og3UIiqMkTLiEZ+dCifyGrSrcyj8dkhQ=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.baltimorecity.gov</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=b6745b345e244324c9cdde441237c93dcf734a78-1610549716-0-AVuGjfOdL8rVFTaa6e_36o4i4p0liFxA8_bV53Ir-jIfXcEZKlo1QL9UeGcPzGogroOtDhGH0xmV1YeBVBdHCSUY5Azeus11JvlANo9M8-dencehivoJhn5aLz_SDpeFttuglQu6ZkCCR7BFPty4tneS8P3nvXasfWSaWNndKdLxNr5aQSXiSzoBtwBT1jn8r86l6M_yn_A0zK8U_AKO-9Nik7ADSLJ7l9rg3DHns4UMtdmqFYOXwbE8Xhh0W4kvLEPqrK7GHt29qphSndvnBVNAFW-5rlDWtFa6vdax8pNSVDJ5Y1IELqRuirZchpmEJLc1-XevbwBguSJQDG1c_jU2Os_JP5DxcGIpFxD9KmzXmwI4zUw9ClCUc6uPzFCUDNhoRLbw_eekp3-0RzlY7due7OcqtPN4g-CkCDQh9NXVzwFx3i2SXQAFip3a3Fi5zaP_9LDaswk8Rz2Ad4NBIFmPnWI_Zl9bV-144Ba8R1VgaYYQM5dxTNQWbHuV53e1dxGy3g8kfyXOwK-v7CEa6kA" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="e3e66222cd5d49b75b984c5f1a30470b18b9faa7-1610549716-0-AZ9/RVWMv15ciCQjnmvmwBf9aJzd+wAF3NvQZZoAocgAqyPC5x8EQ2il6CoNwVo+83Nmo/oeRKP25DZukwUFq6ujJgqnm3gT+0Ce0dXqsEVTsZ1le2NZMjC81jClXQJMLlb6Ywpt/2MWALpAPC1/YD+WpWsMrr3tASTxVXNK+nLtFyf1EGNcT0qO0744rp0BXU5qsMYet/RaQXDNWFq4RRTlxHqieyLm6TSYC1TbyFMueY8b1YClHA8IBGumAwuyaUOyEaPt7xTXdS5w7cmOVdHA1AvX27T0uPk7VTpysNgsQchUu7Uzf3GJ6nybquUyP/T8vIUGBWoBFA48zQAwyqnId9PQsQeG+i3zdRlKocj8TahXoS4Zjr678JRllF8Ikn7RpWhSsRWhNmHDCyoB+0Nb4cgqZRwHqFJ1uG4NUhYhM6RwB5Z5Efb1nfZW+G/2WNmVVCfzpVdQ5toABsvvpluqelDbKZ9jfjY22tLmfOY6ntXA15F7H2oyAkm63UgmPgv4yCQd9EbMSSBg/7q8fAdQ4ygkatrn2xLo2UubDb8gznh8qPRJHk2cxerFUvyiyy7Srzh/vdzJkBh5dthll9e25h/3XLYA+UiDZYV7vJX+Q5OM2p6+Dqc1fmgkDuLth8UTZtRQ/pxhv+i4meJj8UIn78xlRSBTUsGs8k2lInA0nQCzbqUr/fF5G85uIwFX8Kl2409rp5rh4091RdER9XB9tUMF6CFCeYvWxZf3mRc1h7J51A0Yv8VDBsN97rEQcpc01B389NUN12L1AH63uajpTlUoMBPfAG2ONMd63zWFGh0dhutI1fdPW2BgmWNBHoBpAxuesdC/ppRyL0Tpt49H74j89HzlkEgZCzuM5ioLBwYlLYDVBG8KQkYzkq291v6INgbfx4Iw9oUqJJtbFXB8bpa1PrWueKWpDr2SppGbiPloSwcUkDMX4HAbntvpTQ5B9SbWSJNgnR0HHEeIKIBn9Y9ISHZmGEtIbghtSu1WOtCZj+vbccjtv6BRwSDOLY3mE/F3QLk0QQPYfMinjube7Cikd86G+ZJjBFvPjVyXq6/BOf0YMU8O0OCs6ZcV2jRMNlcflwVjdFW/GXowuNqYnoSA4kcuGi1FMJptcjIDOmln7a41jWiqwC6s76tXW6v/wcenO5if7Y5d+3VraDP1PHojJxIaIeYHvhcHy94+2/nIShHXPIhJZCZYhUPv1eavQ7EwQxqE2Y45g5/2eFP/Ly6oDwtvLDT1numrAJQAN7RUS+9fJNG33klvLFBbuLr1MYJXZAO6Omxxq7KjHil2NbrXPEBoMiX3+gpWzZ+PmElOnakaP4gU358KMhdcVQ=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="bdc5299fac0aad2a5c9183baca134398"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610ff512bc0ad1b3')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610ff512bc0ad1b3</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
