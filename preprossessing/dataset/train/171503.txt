<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]--><!--[if IE 7 ]>    <html class="ie7"> <![endif]--><!--[if IE 8 ]>    <html class="ie8"> <![endif]--><!--[if IE 9 ]>    <html class="ie9"> <![endif]--><!--[if (gt IE 9)|!(IE)]><!--><html class=""> <!--<![endif]-->
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Roy Agnew : Represented Artist Profile : Australian Music Centre" name="title"/>
<meta content="Roy Agnew,Agnew,Roy,composer,sound artist,music,composition,australian music,australian composer,australian sound artist,australia" name="keywords"/>
<meta content="Roy Agnew is a represented artist with the Australian Music Centre. Read a biography, discover works, products, events, influences and more." name="description"/>
<meta content="index, follow" name="robots"/>
<meta content="en" name="language"/>
<title>Roy Agnew : Represented Artist Profile : Australian Music Centre</title>
<script type="text/javascript">

            //<![CDATA[

            amcfe = window.amcfe ? window.amcfe : {};
            amcfe.geoIP = window.amcfe.geoIP ? amcfe.geoIP : {};
            amcfe.geoIP.continentCode = 'AS';
            amcfe.geoIP.countryCode = 'CN';
            amcfe.geoIP.regionCode = null;
            amcfe.geoIP.city = null;

            // ]]>
        </script>
<link href="/favicon.ico" rel="shortcut icon"/>
<link href="/touch-icon-iphone.png" rel="apple-touch-icon"/>
<link href="/touch-icon-ipad.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/touch-icon-iphone-retina.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/touch-icon-ipad-retina.png" rel="apple-touch-icon" sizes="167x167"/>
<script src="//awscdn.australianmusiccentre.com.au/lib/jquery/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="//awscdn.australianmusiccentre.com.au/lib/jquery/hoverIntent-r7.js" type="text/javascript"></script>
<script src="//awscdn.australianmusiccentre.com.au/lib/jquery/superfish-1.7.4.min.js" type="text/javascript"></script>
<script src="//awscdn.australianmusiccentre.com.au/lib/simple-lightbox/simple-lightbox.min.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js" type="text/javascript"></script>
<script src="/js/common110825.js" type="text/javascript"></script>
<script src="/js/countries.js" type="text/javascript"></script>
<link href="/css/main200416.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://awscdn.australianmusiccentre.com.au/lib/simple-lightbox/simplelightbox.css" media="screen" rel="stylesheet" type="text/css"/>
<!--<link rel="stylesheet" type="text/css" href="/css/print.css" media="print" />-->
<!--[if IE]>

        <script type="text/javascript" src="/lib/jquery/jquery.corner.js"></script>

        <script type="text/javascript">

            $(document).ready(function()
            {

                // Rounded Corners for IE
                $(".corner").corner('5px');

            });

        </script>

        <link rel="stylesheet" href="/css/ie090519.css" media="screen, projection" type="text/css" />

    <![endif]-->
<!--[if IE 6]>

        <script type="text/javascript" src="/js/DD_belatedPNG_0.0.7a-min.js"></script>

        <script type="text/javascript">
        //<![CDATA[

            DD_belatedPNG.fix('.png_bg', 'img', '#content');

        //]]>
        </script>

        <link rel="stylesheet" href="/css/ielte6091113.css" media="screen, projection" />

        
    <![endif]-->
<script type="text/javascript">

        $(document).ready(function() {

            $("#quick_search_submit").mouseover(function() {

                $(this).attr({ src: '/images/bg-search-submit-sml-hover.gif' });

            }).mouseout(function() {

                $(this).attr({ src: '/images/bg-search-submit-sml.gif' });

            });

        });

    </script>
</head>
<body>
<ul id="access_options">
<li><a href="#nav">Skip to navigation</a></li>
<li><a href="#content">Skip to content</a></li>
</ul>
<div id="wrapper">
<div id="login_cart">
<div class="inner">
<div class="col_6 col_first">
<h3>Login</h3>
<p>Enter your username and password</p>
<form action="https://www.australianmusiccentre.com.au/myaccount/login" method="post">
<p>
<input id="login_username" name="login[username]" type="text"/>
</p>
<p>
<input id="login_password" name="login[password]" type="password"/>
</p>
<p>
<input class="submit" type="submit" value="Login"/>
</p>
<p>
<a href="/myaccount/loginreminder" title="Forgotten your username or password?">Forgotten your username or password?</a>
</p>
</form>
</div>
<div class="col_6 col_last">
<h3>Your Shopping Cart</h3>
<p>There are no items in your shopping cart.</p>
</div>
<p class="user_settings"><a class="login_trigger icon_account" href="#">Login</a> <a class="icon_email" href="/myaccount/signup">Sign up to receive our e-Newsletter</a> <a class="login_trigger icon_cart" href="#">View Cart</a></p>
</div>
</div>
<div id="header">
<div class="inner">
<h1><a href="/" title="Australian Music Centre | Breaking Sound Barriers">Australian Music Centre | Breaking sound barriers</a></h1>
</div>
</div>
<div id="nav">
<ul class="inner sf-menu">
<li><a href="/search" title="Find music">Find music</a>
<ul>
<li><a class="first" href="/introductions" title="Introductions to Australian music">Introductions to Australian music</a></li>
<li><a class="first" href="/artists" title="Represented artists">Represented artists</a></li>
<li><a href="/repertoirenavigator" title="Instrumentation navigator">Repertoire navigator</a></li>
<li><a href="/subjectnavigator" title="Subject navigator">Subject navigator</a></li>
<li><a href="/prizes" title="Subject navigator">Prize &amp; award winners</a></li>
</ul>
</li>
<li><a href="/resonate" title="Resonate magazine">Resonate magazine</a>
<ul>
<li><a class="first" href="/resonate/news" title="News">News</a></li>
<li><a href="/resonate/reviews" title="Reviews">Reviews</a></li>
<li><a href="/resonate/blogs" title="Blogs">Blogs</a></li>
<li><a href="/resonate/features" title="Features">Features</a></li>
<li><a href="/resonate/opportunities" title="Opportunities">Opportunities</a></li>
<li><a href="/resonate/journal" title="Bi-annual journal">Bi-annual journal</a></li>
<li><a href="/resonate/about" title="About">About</a></li>
</ul>
</li>
<li><a href="/guides/" title="Pathways">Pathways</a>
<ul>
<li><a class="first" href="/guides/students" title="for Students">for Students</a></li>
<li><a href="/guides/teachers" title="for Teachers">for Teachers</a></li>
<li><a href="/guides/performers" title="for Performers">for Performers</a></li>
<li><a href="/guides/composers-soundartists-improvisers" title="for Composers, improvisers &amp; sound artists">for Composers, improvisers &amp; sound artists</a></li>
<li><a href="/introductions" title="Introductions to Australian music">Introductions to Australian music</a></li>
</ul>
</li>
<li><a href="/calendar" title="Calendar">Calendar</a>
<ul>
<li><a class="first" href="/calendar" title="View calendar">View calendar</a></li>
<li><a href="/contribute/newEvent" title="Add an event">Add an event</a></li>
</ul>
</li>
<li><a href="/shop" title="Shop">Shop</a>
<ul>
<li><a class="first" href="/shop/cds" title="CDs">CDs</a></li>
<li><a href="/shop/mp3s" title="Mp3s">MP3s</a></li>
<li><a href="/shop/digitalsheetmusic" title="Digital sheet music">Digital sheet music</a></li>
<li><a href="/shop/sheetmusic" title="Sheet music">Sheet music</a></li>
<li><a href="/shop/books" title="Books">Books</a></li>
<li><a href="/shop/education" title="Educational resources">Education resources</a></li>
<li><a href="/cart/" title="View cart">View cart</a></li>
</ul>
</li>
<li><a href="/about" title="About">About</a>
<ul>
<li><a class="first" href="/about/membership" title="Membership">Membership</a></li>
<li><a href="/myaccount/donation" title="Donate to the AMC">Donate to the AMC</a></li>
<li><a href="/about/projects" title="Projects, Awards">Projects, Awards</a></li>
<li><a href="/about/representation" title="Artist representation">Artist representation</a></li>
<li><a href="/about/library" title="Library">Library</a></li>
<li><a href="/about/sponsors" title="Funding, sponsors &amp; Donors">Funding, sponsors &amp; donors</a></li>
<li><a href="/about" title="About us">About us</a></li>
<li><a href="/about/websitedevelopment" title="About theis website">About this website</a></li>
</ul>
</li>
<li><a href="/contact" title="Contact">Contact</a></li>
</ul>
</div>
<hr class="hidden"/>
<div id="page_options">
<div class="inner">
<ul class="breadcrumbs">
<li><a href="/" title="">Home</a></li>
<li><a href="/artists">Represented Artists</a></li>
<li class="breadcrumb_current">Roy Agnew</li>
</ul>
<form action="/search" id="frm_quick_search" method="get">
<p>
<input id="quick_search_text" name="q" type="text" value="Search"/>
<input id="quick_search_submit" src="/images/bg-search-submit-sml.gif" type="image" value="Go"/>
</p>
</form>
</div>
</div>
<div id="content">
<div class="inner">
<h2>
                    Roy Agnew                    (1891-1944)                     : Represented Artist                </h2>
<div style="margin-bottom:20px;">
<table class="nostyle"><tr><td style="width:25px;">
<!-- Start audio sample -->
<div class="audioSampleContainer" id="audioSampleContainer_24781">
<audio id="audioTag_24781" src="https://micnet-cdn-fm.s3-ap-southeast-2.amazonaws.com/aod/samples/ds_67982_24781.m4a"></audio>
<div id="audioControl_24781"></div>
</div>
<script type="text/javascript">
			var a = document.getElementById('audioTag_24781');
			if (typeof a.canPlayType === 'function' && a.canPlayType('audio/mp4; codecs="mp4a.40.5"') != '') {
				var controller = (controller) ? controller : [];
				var audio = (audio) ? audio : [];
				controller[24781] = $('#audioControl_24781');
				audio[24781] = $(a);

				controller[24781].addClass('audioPlay');
				controller[24781].click(function(){
					var pauseRequest = controller[24781].hasClass('audioPause');

					var method = pauseRequest ? 'pause' : 'play';
					audio[24781].get(0)[method]();
					controller[24781].toggleClass('audioPlay',pauseRequest);
					controller[24781].toggleClass('audioPause',!pauseRequest);
				});
			}
			else {
				$('#audioTag_24781').html('');
				$('#audioControl_24781').html('');

				swfobject.embedSWF(
					'https://awscdn.australianmusiccentre.com.au/flash/miniplayer-aws.swf',
					'audioSampleContainer_24781',
					'23',
					'23',
					'9',
					null,
					{ song: 'ds_67982_24781.m4a' },
					{ allowFullScreen: false },
					{},
					function(e){
						if(!e.success && $.browser.msie){
							return false;
						}
					}
				);
			}
		</script>
<!-- End audio sample -->
</td><td>
<p>Random Audio Sample: <a href="/workversion/agnew-roy-sonata-1929/8732">Sonata (1929) (solo piano)</a> by Roy Agnew, from the CD <a href="/product/piano-music-7">Piano Music</a></p>
</td></tr></table>
</div>
<ul>
<li><a href="/search?type=work&amp;sort=alphaTitleSort&amp;wfc[]=Roy+Agnew">Browse <strong>works</strong> by Roy Agnew</a></li><li><a href='/search?q="Roy+Agnew"&amp;type=product&amp;mf[format]=CD&amp;mf[comAvailId]=1&amp;sort=titleSort'>Browse <strong>recordings available to purchase</strong> featuring music by Roy Agnew</a></li><li><a href="/search?type=event&amp;ef[parties]=Roy+Agnew&amp;sort=startDate+desc">Browse <strong>events</strong> featuring music by Roy Agnew</a></li>
</ul>
<hr/>
<div class="col_9 col_first">
<div class="artist_image">
<img alt="Photo of Roy Agnew" src="https://awscdn.australianmusiccentre.com.au/images/par_298_135w.jpg"/>
</div>
<p>
<strong>Roy Agnew</strong> was born in Sydney in 1891 and taught
  himself to play the piano at an early age. His first formal music
  tuition was with Emanuel de Beaupuis, an Italian pianist living
  in Sydney, and he later studied composition with Alfred Hill at
  the NSW State Conservatorium of Music.
</p>
<p>
  Although Agnew's first publication (<span class="work">Australian
  Forest Pieces for Piano</span>) appeared in 1913, it was not
  until Benno Moiseiwitsch performed <span class="work">Deirdre's
  Lament</span> and <span class="work">Dance of the Wild Men</span>
  at the Sydney Town Hall in 1920 that his music became known. In
  1923 Agnew travelled to London, where he studied with Gerard
  Williams and Cyril Scott at the Royal College of Music and gave
  recitals of music by contemporary composers such as Debussy and
  Stravinsky. His own compositions were performed by William
  Murdoch (to whom Agnew dedicated <span class="work">Sonata
  Fantasie</span>), and were also published in London by Augener
  Ltd, and in New York by the Arthur P. Schmidt Co.
</p>
<p>
  Returning to Sydney in 1928, Agnew's music was beginning to be
  well-received by Australian audiences. He gave recitals, and his
  poem for orchestra and voice, <span class="work">The Breaking of
  the Drought</span>, was encored after a performance conducted by
  Alfred Hill. However, he did not remain in Australia for long. He
  married in 1930 and shortly afterwards returned to Britain where
  he performed his compositions in London and Glasgow and for BBC
  broadcasts. He remained in Britain for three years, and finally
  returned to Australia in late 1934 to undertake a recital tour
  for the Australian Broadcasting Commission.
</p>
<p>
  In 1938 he accepted a position with the ABC arranging and
  presenting a weekly programme on contemporary music, "Modern and
  Contemporary Composers' Session". He broadcast the music of
  composers such as Webern, Berg, Busoni, Szymanowski, Debussy,
  Stravinsky and Scriabin, sometimes performing the pieces himself.
  He also prepared and presented another programme for the ABC,
  "Music Through the Ages: The Piano and its Composers" for which
  he performed the music of composers such as Giles Farnaby,
  Scarlatti, Mozart, John Field, Chopin and Debussy. He remained
  with the ABC for five years. Agnew joined the staff of the NSW
  State Conservatorium of Music at the beginning of 1944, but died
  in November of that year from septicaemia following tonsillitis.
</p>
<p>
  Apart from his career as a piano recitalist and his compositional
  activity, Agnew was also a piano teacher, a teacher of 'Practical
  Composition' and 'General Interpretation and the Art of
  Pedalling' (his pupils including Dulcie Holland and Frank
  Hutchens), and an examiner for the Australian Music Examinations
  Board. He recorded his <span class="work">Sonata Ballade</span>
  (a prize-winning composition in the NSW Music Association's
  Sesqui-Centenary Competition) for the Columbia Phonograph Co.,
  and in 1943 the ABC recorded him playing many of his own
  compositions - forty-four piano works and five songs. These were
  broadcast nationally and in Papua New Guinea on many occasions.
</p>
<hr/>
<p>
</p>
</div>
<div class="col_3 col_last">
</div>
<div class="clear"></div>
<div style="margin-top:30px;"><h3>Analysis &amp; Media</h3><p>Book: <a href="/product/one-hand-on-the-manuscript-music-in-australian-cultural-history-1930-1960">One hand on the manuscript : music in Australian cultural history, 1930-1960 / edited by Nicholas Brown, Peter Campbell, Robyn Holmes, Peter Read &amp; Larry Sitsky.</a></p><p>Book: <a href="/product/an-analytical-study-of-the-piano-works-of-roy-agnew-margaret-sutherland-and-dulcie-holland-including-biographical-material">An analytical study of the piano works of Roy Agnew, Margaret Sutherland and Dulcie Holland, including biographical material / by Rita Sandra Crews.</a></p><p>Book: <a href="/product/the-career-of-roy-agnew-and-his-impact-on-australian-musical-life">The career of Roy Agnew and his impact on Australian musical life / Fiona McGregor.</a></p></div> </div>
</div>
<hr class="hidden"/>
<div id="footer">
<div class="inner">
<dl class="first">
<dt><a href="/search" title="Find music">Find music</a></dt>
<dd><a class="first" href="/introductions" title="Introductions to Australian music">Introductions</a></dd>
<dd><a href="/artists" title="Represented artists">Artists</a></dd>
<dd><a href="/subjectnavigator" title="Subject navigator">Subjects</a></dd>
<dd><a href="/repertoirenavigator" title="Instrumentation navigator">Instrumentations</a></dd>
</dl>
<dl>
<dt><a href="/resonate" title="Resonate magazine">Magazine</a></dt>
<dd><a class="first" href="/resonate/news" title="News">News</a></dd>
<dd><a href="/resonate/reviews" title="Reviews">Reviews</a></dd>
<dd><a href="/resonate/blogs" title="Blogs">Blogs</a></dd>
<dd><a href="/resonate/features" title="Features">Features</a></dd>
<dd><a href="/resonate/opportunities" title="Opportunities">Opportunities</a></dd>
<dd><a href="/resonate/journal" title="Bi-annual journal">Journal</a></dd>
<dd><a href="/resonate/about" title="About">About</a></dd>
</dl>
<dl>
<dt><a href="/guides/" title="Pathways">Pathways</a></dt>
<dd><a class="first" href="/guides/students" title="for Students">Students</a></dd>
<dd><a href="/guides/teachers" title="for Teachers">Teachers</a></dd>
<dd><a href="/guides/performers" title="for Performers">Performers</a></dd>
<dd><a href="/guides/composers-soundartists-improvisers" title="for Composers, improvisers &amp; sound artists">Composers</a></dd>
<dd><a href="/guides/composers-soundartists-improvisers" title="for Composers, improvisers &amp; sound artists">Improvisers</a></dd>
<dd><a href="/guides/composers-soundartists-improvisers" title="for Composers, improvisers &amp; sound artists">Sound artists</a></dd>
<dd><a href="/guides/" title="Introductions to Australian music">Introductions</a></dd>
</dl>
<dl>
<dt><a href="/calendar/" title="Calendar">Calendar</a></dt>
<dd><a class="first" href="/calendar/" title="View calendar">View calendar</a></dd>
<dd><a href="/contribute/newEvent" title="Add an event">Add event</a></dd>
</dl>
<dl>
<dt><a href="/shop" title="Shop">Shop</a></dt>
<dd><a class="first" href="/shop/cds" title="CDs">CDs</a></dd>
<dd><a href="/shop/mp3s" title="Mp3s">Mp3s</a></dd>
<dd><a href="/shop/digitalsheetmusic" title="Digital sheet music">Digital sheet music</a></dd>
<dd><a href="/shop/sheetmusic" title="Sheet music">Sheet music</a></dd>
<dd><a href="/shop/books" title="Books">Books</a></dd>
<dd><a href="/shop/education" title="Educational resources">Education resources</a></dd>
<dd><a href="/cart/" title="View cart">View cart</a></dd>
</dl>
<dl class="last">
<dt><a href="/about" title="About">About</a></dt>
<dd><a class="first" href="/about" title="About the AMC">About the AMC</a></dd>
<dd><a href="/about/projects" title="Projects &amp; awards">Projects</a></dd>
<dd><a href="/about/projects" title="Projects &amp; awards">Awards</a></dd>
<dd><a href="/about/publishing" title="Publishing">Publishing</a></dd>
<dd><a href="/about/representation" title="Artist representation">Representation</a></dd>
<dd><a href="/about/library" title="Library">Library</a></dd>
<dd><a href="/about/membership" title="Membership">Membership</a></dd>
<dd><a href="/about/funding" title="Funding">Funding</a></dd>
<dd><a href="/about/sponsors" title="Sponsors &amp; Donors">Sponsors</a></dd>
<dd><a href="/about/sponsors" title="Sponsors &amp; Donors">Donors</a></dd>
</dl>
<div class="overflow clear">
<p class="left">
                        All content © 2021 Australian Music Centre Ltd | ABN 52 001 250 595<br/>
                        All prices are in Australian dollars and are inclusive of GST<br/>
                        Ph +61 2 9935 7805 or Toll free 1300 651 834 | <a href="/about/terms">Terms &amp; conditions</a> | <a href="/about/privacy">Privacy policy</a> </p>
<p class="right" style="margin-top:10px;">
<a href="http://www.iamic.net" id="logo-iamic" title="IAMIC">IAMIC</a>
<a href="http://www.iscm.org" id="logo-iscm" title="ISCM">ISCM</a>
</p>
</div>
</div>
</div>
</div>
<span class="png_bg" id="page_bg"> </span>
<script type="text/javascript">
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-8167875-1', 'auto');
ga('send', 'pageview');

</script>
</body>
</html>