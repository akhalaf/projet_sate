<!DOCTYPE html>
<!--[if gt IE 8]><!--><html class="no-js" id="ng-app" lang="en" xmlns:ng="http://angularjs.org">
<!--<![endif]-->
<head>
<script> window.default_user = 1; </script>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="origin" name="referrer"/>
<title class="page_title" ng-bind="ms.page.title"></title>
<link href="{{ms.page.canonical_url}}" ng-if="ms.page.canonical_url" rel="canonical"/>
<link ng-attr-href="{{link.href}}" ng-attr-hreflang="{{link.hreflang}}" ng-attr-rel="{{link.rel}}" ng-repeat="link in ms.page.links"/>
<!-- Meta Data ================ -->
<meta charset="utf-8"/>
<meta content="initial-scale=1, minimum-scale=1,maximum-scale=1, height=device-height, width=device-width" name="viewport"/>
<meta content="{{metas.content}}" name="{{metas.name}}" ng-if="(metas.content &amp;&amp; metas.content != 'undefined')" ng-repeat="metas in ms.settings.meta_tags"/>
<meta content="{{meta.content}}" name="{{meta.name}}" ng-if="(meta.content &amp;&amp; meta.content != 'undefined')" ng-repeat="meta in ms.page.meta_tags"/>
<link href="{{ms.settings.favicon | image:'240x240'}}" rel="shortcut icon" type="image/x-icon"/>
<meta content="{{og.content}}" ng-if="(og.content &amp;&amp; og.content != 'undefined')" ng-repeat="og in ms.page.og_tags" property="{{og.property}}"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<script>
        MYSTORE_MODULES = ['header_slider', 'myPriceApp', 'mystore-filters2'];
    </script>
<!--[if lt IE 9]>
    <script src="//cdn.storehippo.com/global/assets/html5shiv.js?_v=2019042904"></script>
    <![endif]-->
<!--[if lte IE 8]>
    <script>
        document.createElement('ng-include');
        document.createElement('ng-pluralize');
        document.createElement('ng-view');
        document.createElement('ms-widget');
        // Optionally these for CSS
        document.createElement('ng:include');
        document.createElement('ng:pluralize');
        document.createElement('ng:view');
        document.createElement('widget');
    </script>
    <![endif]-->
<!-- CSS -->
<link as="font" crossorigin="" href="https://cdn.storehippo.com/global/assets/fontawesome-webfont.woff2?v=4.5.0" rel="preload" type="font/woff2"/>
<!--<link rel="preload" as="font" type="font/woff2" crossorigin href="https://cdn.storehippo.com/global/assets/fontawesome-webfont.woff2?v=4.2.0"/>-->
<link href="//cdn.storehippo.com/global/assets/font-awesome-4.5.1.min.css?_v=2019042904" media="all" rel="stylesheet"/>
<link href="//cdn.storehippo.com/s/546764e5e1e5990d3ba3c075/ms.local_themes/54ae268e7ac7e23a39abca76/theme.css?_v=ms17339_1609823737481" media="all" rel="stylesheet"/>
<link href="//cdn.storehippo.com/global/assets/bootstrap-3.0.2.min.css?_v=2019042904" media="all" rel="stylesheet"/>
<!--icons8 css-->
<link href="//cdn.storehippo.com/global/assets/themeicons2/css/styles.min.css?_v=2019042904" media="all" rel="stylesheet"/>
<!-- <link rel="stylesheet" href="//cdn.storehippo.com/global/assets/thm_v0.0.2/css/styles.min.css?_v=2019042904">-->
<script>
       window.lazySizesConfig = window.lazySizesConfig || {};
       window.lazySizesConfig.srcAttr = 'ms-data-src';
       window.lazySizesConfig.srcsetAttr = 'ms-data-srcset';
       window.lazySizesConfig.sizesAttr = 'ms-data-sizes';
    </script>
<style>
      img.lazyload:not([src]) {
        visibility: hidden;
        } 
    </style>
<base href="/"/>
<link href="https://cdn.storehippo.com" rel="dns-prefetch"/>
<link href="https://cdn1.storehippo.com" rel="dns-prefetch"/>
<link href="https://cdn2.storehippo.com" rel="dns-prefetch"/><script src="//cdn.storehippo.com/global/assets/mystore-assets-1.1.3.js?_v=2019042904"></script><script src="/user/info.js?_v=1610625536588"></script>
<script src="//cdn.storehippo.com/origin/betahippo/ms/store/beadsnfashion/EN/storeinfo-betahippo_ms17339_1610620649019.js"></script>
<script src="//cdn.storehippo.com/origin/betahippo/ms/store/beadsnfashion/themeinfo-betahippo_5bff92725663f026461138a9_ms17339_1609823737481.js"></script>
<script src="//cdn.storehippo.com/global/assets/mystore/js/mystore-ec36c8c.js"></script>
<script async="" defer="" src="//cdn.storehippo.com/origin/betahippo/ms/store/beadsnfashion/EN/translations-betahippo_1557476681115.js"></script>
<script>serverTimestamp = "1610625536588"</script>
<script>token = "6000320052ad2c0278177ff3"</script>
<script>url ="//cdn.storehippo.com/origin/betahippo"</script>
<script>widget_version ="1234"</script>
<script src="//cdn.storehippo.com/origin/betahippo/ms/store/beadsnfashion/cacheEntities/EN/storedata-betahippo_ms17339_1610082463352.js"></script>
<!--<link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">

    <script src="https://use.fonticons.com/ffe176a3.js"></script>-->
<!--[if IE 8]>
    <link rel="stylesheet" href="//cdn.storehippo.com/s/546764e5e1e5990d3ba3c075/ms.local_themes/54ae268e7ac7e23a39abca76/ie8.css?_v=ms17339_1609823737481" type="text/css"/>
    <![endif]-->
<!--[if IE ]>
    <script type="text/javascript" src="//cdn.storehippo.com/s/546764e5e1e5990d3ba3c075/ms.local_themes/54ae268e7ac7e23a39abca76/placeholder.js?_v=ms17339_1609823737481"></script>
    <![endif]-->
<script src="//cdn.storehippo.com/global/assets/cloudZoom1.js?_v=2019042904" type="text/javascript"></script>
<!--<script type="text/javascript" src="//cdn.storehippo.com/global/assets/fonticons.js?_v=2019042904"></script>-->
<script src="//cdn.storehippo.com/s/546764e5e1e5990d3ba3c075/ms.local_themes/54ae268e7ac7e23a39abca76/combine.js?_v=ms17339_1609823737481" type="text/javascript"></script>
<!--<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false">
    </script>-->
<script>
        angular.module('mystore-filters2', [])
            .filter('msRemoveUnderscore', ['$http', '$filter', '$translate', function ($http, $filter, $translate) {
                return function (key) {//_brand	abc _ xyz
                    if (!key)return;
                    var str = key;

                    var res = str.split("_");

                    if (res[0] == '') {
                        return res[1];
                    }
                    else {
                        var str1 = res.join(' ')
                        return str1;
                    }

                }
            }])
            .filter('removeNum', function () {
                return function (x) {
                    var i, txt = "";
                    x = x.split("")
                    for (i = 0; i < x.length; i++) {
                        if (x[i] >= 0 || x[i] <= 9) {
                            x[i] = " ";
                        }
                        txt += x[i];
                    }
                    return txt;
                };
            })
            .filter('removeSpacetoUnderscore', function () {
                return function (key) {//_brand	abc _ xyz
                    if (!key)return;
                    var str = key;

                    var res = str.split(" ");

                    if (res[0] == ' ') {
                        return res[1];
                    }
                    else {
                        var str1 = res.join('_')
                        return str1;
                    }

                }
            })
            .filter('removeDash', function () {
                return function (x) {
                    var i, txt = "";
                    x = x.split("")
                    for (i = 0; i < x.length; i++) {
                        if (x[i] == '-') {
                            x[i] = " ";
                        }
                        txt += x[i];
                    }
                    return txt;
                };
            })
            .filter('removeAttribute', function () {
                return function (x) {
                    var i, txt = "";
                    x = x.split(".")
                    for (i = 0; i < x.length; i++) {
                        if (x[i] == 'attributes') {
                            x[i] = " ";
                        }
                        txt += x[i];
                    }
                    return txt;
                };
            })
            .filter('roundup', function () {
                return function (value) {
                    return Math.ceil(value);
                };
            })
            .filter('startsWith', function () {
                return function (input, value) {
                    if (input.startsWith(value) || input.startsWith(value.toLowerCase()) || input.startsWith(value.toUpperCase())) {
                        return input;
                    }
                };
            })
            .filter('msSort', function () {
                return function (input) {
                    if (input) {
                        input = input.sort(function (b, c) {
                            if (b.title && c.title) {
                                if (b.title < c.title) return -1;
                                if (b.title > c.title) return 1;
                            }
                        });
                        return input;
                    }
                    else
                        return input;
                }
            })
            .filter('unique', function() {
                // we will return a function which will take in a collection
                // and a keyname
                return function(collection, keyname) {
                    // we define our output and keys array;
                    var output = [],
                        keys = [];
                    // we utilize angular's foreach function
                    // this takes in our original collection and an iterator function
                    angular.forEach(collection, function(item) {
                        // we check to see whether our string exists
                        var key = item[keyname];
                        // if it's not already part of our keys array
                        if (keys.indexOf(key) === -1) {
                            // add it to our keys array
                            keys.push(key);
                            // push this item to our final output array
                            output.push(item);
                        }
                    });
                    // return our array which should be devoid of
                    // any duplicates
                    return output;
                };
            })

    </script>
<script src="//cdn.storehippo.com/global/assets/bootstrap-3.0.2.min.js?_v=2019042904"></script>
<link href="//cdn.storehippo.com/s/5bf5074adb08ee03812b17be/ms.local_themes/5bff92725663f026461138a9/ms17339_1592383140574/53734f1410ee11cd79000002.less.css?_v=ms17339_1609823737481&amp;storename=beadsnfashion" id="msVariantFile" rel="stylesheet" title="lessCss"/></head>
<body class="{{ms.user.language}}" ng-cloak="" ng-controller="MobileAppController">
<!--<div ms-widget="all_categories"></div>-->
<!--<div ms-widget="ms.form.return_order"></div>-->
<!--<div ms-widget="ms.th3.return_modal"></div>-->
<!-- <div ms-widget="ms.orderStatus_new"></div>-->
<!--<div ms-widget="ms.th3.carousel"> </div>-->
<!--<div ms-widget="ms.categories"> </div>-->
<!--<div ms-widget="ms.pay_balance_amount"> </div>-->
<div ng-if="ms.screen.xs || ms.screen.s">
<!--<div id="mobile-nav" ng-class="{hide1:!showmenu,slide:showmenu}">
        <div ms-widget="ms.navigation">
            <div ms-widget="mobile_navigation"></div>
        </div>
    </div>-->
</div>
<div class="body-bg-image body-bg-color " id="body" ng-class="{hide1:showmenu,slide:!showmenu}">
<div class="overlay" ng-class="{show:showmenu}" ng-click="showmenu=false" ng-swipe-left="showmenu=false"></div>
<div data-ng-if="ms.variables.show_feedback =='yes'">
<div ms-widget="feedback"></div>
</div>
<div ms-widget="header"></div>
<div ms-widget="microdata"></div>
<div class="text-center" id="th2messages" ms-widget="messages"></div>
<div id="th2uiview" ui-view=""></div>
<div id="footer" ms-widget="footer"></div>
<div id="compare_list" ms-widget="comparelist"></div>
</div>
<div ng-if="ms.device.os == 'iphone'">
<style>
        .ms-ve-2-14 {
            white-space: inherit !important;
        }
    </style>
</div>
<style>
    #th2uiview {
        min-height: 650px;
    }

    .AR {
        direction: rtl;
    }

    @media screen and (min-width: 768px) and (max-width: 1024px) {
        #th2uiview {
            min-height: 835px;
        }
    }

    #th2messages {
        position: fixed;
        z-index: 999999;
        top: 0%;
        left: 35%;
        right: 0;
        width: 100%
    }
</style>
<!--[if lte IE 8]>
<script src="/js/respond.js"></script>
<link href="//cdn.storehippo.com/global/assets/respond-proxy.html?_v=2019042904" id="respond-proxy" rel="respond-proxy"/>
<link href="/img/respond.proxy.gif" id="respond-redirect" rel="respond-redirect"/>
<script src="/js/respond.proxy.js"></script>
<![endif]-->
<script>
    var prc = angular.module('myPriceApp', ['rzModule']);
    prc.controller('priceRangeController', ['$scope', function ($scope) {
        $scope.values = {
            min: 100,
            max: 5000,
            ceil: 150000,
            floor: 100
        }
    }]);
</script>
</body>
</html>