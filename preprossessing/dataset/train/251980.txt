<!DOCTYPE html>
<html>
<head>
<meta content="width=device-width,initial-scale=1.0,shrink-to-fit=no" name="viewport"/>
<title>
    Locked
  </title>
<style>
    body, html {
      height: 100%;
    }
    body {
      font-family: Helvetica Neue, Helvetica;
      text-align: center;
      display: flex;
      align-items: center;
      justify-content: center;
    }
    .admin {
      color: #788188;
    }
    .admin_link {
      color: #385148;
    }
    .error {
      margin-bottom: 0;
      font-style: italic;
      font-size: 16px;
      font-weight: 700;
      color: #F44336;
    }
    .host {
      font-weight: 300;
      color: #788188
    }
    .soon {
      font-weight: 500;
      color: #788188;
      font-size: 16px;
    }
    .provider {
      margin-top: 24px;
      color: #687178;
      font-size: 18px;
    }
    .berdu {
      color: rgb(30,136,229);
      font-size: 120%;
      text-decoration: none;
      font-style: italic;
      font-weight: 700;
    }
    .berdu:hover {
      text-decoration: underline;
    }
  </style>
</head>
<body>
<div>
<h1 class="host">www.bosqueclothing.com </h1>
<h2 class="soon">
      PAKET SUDAH BERAKHIR
    </h2>
<div>
<div>
<p class="admin">
          Apakah kamu pemilik toko?
          <a class="admin_link" href="/admin/login">
            Login di sini
          </a>
</p>
<p class="provider">
          Provided by
          <a class="berdu" href="https://berdu.id">
            Berdu
          </a>
</p>
</div>
</div>
</div></body>
</html>
