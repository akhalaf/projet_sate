<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-gb" xml:lang="en-gb" xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="https://www.birdday.org/2013materials/correlation/Apple-YOYO/9d8f13895478aec77edbe583ee8fafa9	
"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="joomla, Joomla" name="keywords"/>
<meta content="Joomla! - the dynamic portal engine and content management system" name="description"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>home</title>
<link href="/templates/birdday/css/birdday.css" rel="stylesheet" type="text/css"/>
<style>
.main_b1 {//filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/templates/birdday/images/main_bg/shadow_lr.png', sizingMethod='scale');}
.main_b3 {//filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/templates/birdday/images/main_bg/shadow_b.png', sizingMethod='scale');}
.main_b3_1 {//filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/templates/birdday/images/main_bg/shadow_lb.png', sizingMethod='scale');}
.main_b3_2 {//filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/templates/birdday/images/main_bg/shadow_rb.png', sizingMethod='scale');}
</style>
</head>
<body><a id="up" name="up"></a>
<map name="logo1"><area alt="" coords="0,0,140,61" href="/" shape="rect"/></map>
<map name="logo2"><area alt="" coords="0,0,140,95" href="/" shape="rect"/></map>
<div class="main">
<div class="main_b1">
<div class="main_b2">
<div class="main_top">
<img height="61" src="/templates/birdday/images/top_banner/main.jpg" usemap="#logo1" width="880"/>
<img height="95" src="/templates/birdday/images/top_banner/b8.jpg" usemap="#logo2" width="880"/>
<div class="menu">
<ul class="menu_l0">
<li class="item-418"><a href="https://www.coloradogives.org/BirdDay/overview">Support Us!</a></li><li class="item-3 parent"><a href="/birdday" target="_blank">Bird Day</a></li><li class="item-4 parent"><a href="/educate">Educate</a></li><li class="item-5 parent"><a href="/connectcultures">Connect Cultures</a></li><li class="item-208"><a href="/science">Science</a></li><li class="item-2 parent"><a href="http://www.environmentamericas.org/shop/" target="_blank">Shop</a></li><li class="item-7 parent"><a href="/about">About</a></li><li class="item-297 current active"><a href="/">home</a></li><li class="item-121"><a href="/connectnew">ConnectNew</a></li><li class="item-163"><a href="/art-and-artist">Art and Artist</a></li></ul>
</div>
<div class="clear"></div>
<div class="search">
<form action="index.php?option=com_search" id="searchForm" method="post" name="searchForm">
<input name="search" type="hidden" value="view"/>
<input name="task" type="hidden" value="search"/>
<input class="inputbox" maxlength="20" name="searchword" size="30" type="text" value=""/>
<input class="submit" type="submit" value="search"/>
</form>
</div>
<div class="secondary_menu">
<ul class="menu_sl">
<li class="item-16"><a href="http://www.environmentamericas.org/shop/">Shopping Cart</a></li><li class="item-21"><a href="/educate/flyway-news">Newsletter</a></li><li class="item-17"><a href="/about/contact-ua">Contact Us</a></li><li class="item-145"><a href="http://www.facebook.com/pages/Environment-for-the-Americas/27572262531?ref=ts">Facebook</a></li><li class="item-179"><a href="http://birdday.org/about">About Us</a></li><li class="item-203"><a href="https://www.coloradogives.org/BirdDay/overview">Donate</a></li></ul>
</div>
</div>
<div class="main_content">
<link href="https://www.birdday.org/components/com_sitepages/css/main.css" rel="stylesheet" type="text/css"/>
<div class="site_pages_top">
<div class="left_center_pan">
<div class="left_pan">
<div class="b0">
</div>
</div>
<div class="center_pan">
<div class="b0">
<div class="breadcrumb" style="background:white;">
<div class="breadcrumbs">
<span>Home</span></div>
</div>
</div>
</div>
</div>
<div class="right_pan">
<div class="b0">
</div>
</div>
<div class="clear"></div>
</div>
</div>
<div class="main_footer">
<div class="left">
						Copyright © for the Americas. All rights reserved.
					</div>
<div class="right">
<b>Environment for the Americas</b><br/>
						5171 Eldorado Springs Drive, Suite N, Boulder, CO 80303<br/>
						303.499.1950<br/>
						info@environmentamericas.org
					</div>
<div class="clear"></div>
</div>
</div>
</div>
<div class="main_b3">
<div class="main_b3_1"></div>
<div class="main_b3_2"></div>
</div>
</div>
</body>
</html>
