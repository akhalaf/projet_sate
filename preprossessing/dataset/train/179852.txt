<!DOCTYPE HTML>
<html class="no-js" lang="en">
<!-- BC_OBNW -->
<head>
<title>The Ayurvedic Institute | Leading Ayurveda School | Home Page</title>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width,initial-scale=1,minimum-scale=1, maximum-scale=1" name="viewport"/>
<link href="/stylesheets/screen.css" media="all" rel="stylesheet" type="text/css"/>
<link href="/favicon.ico" rel="shortcut icon"/>
<script src="/libs/jquery/2.2.4/jquery.min.js"></script>
<!--Begin Google Analytics-->
<!--End Google Analytics-->
<!--Begin Company Data-->
<!--End Company Data-->
<!--Begin Page Control-->
<!--End Page Control-->
<!--Begin Print App-->
<!--End Print App--><script src="/js/plugins/jquery.royalslider.min.js"></script>
<meta content="Leading Ayurveda school &amp; spa outside India since 1984 | Ayurvedic Studies Program &amp; seminars with Vasant Lad, Ayurvedic Physician | Authentic Panchakarma Therapies " name="description"/>
</head>
<body>
<div class="root">
<!--Begin Header-->
<header class="header">
<div class="header-container"> <a class="logo" href="/"><img alt="The Ayurvedic Institute" height="89" src="/images/template/the-ayurvedic-institute.png" width="253"/></a>
<button class="drawer-toggle reset"><span></span></button>
<div class="drawer">
<!--Begin Utility Links-->
<div class="utility-links"> <ul>
<li><a href="/education/overview"><span class="util-ico util-ico--ico-campus"></span> Campus Learning</a></li>
<li><a href="/education/contact-admissions"><span class="util-ico util-ico--ico-adm"></span> Admissions Team</a></li>
<li><a href="/about/donate"><span class="util-ico util-ico--ico-donate"></span> Donations</a></li>
<li><a href="/videostream/"><span class="util-ico util-ico--ico-videos"></span> Videos</a></li>
<li><a href="/calendar?view=gallery"><span class="util-ico util-ico--ico-cal"></span> Calendar</a></li>
</ul> </div>
<!--End Utility Links-->
<!-- Begin Main Menu -->
<nav class="menu" role="navigation menu"> <ul>
<li><a href="/about/the-ayurvedic-institute">About</a> <ul>
<li><a href="/about/the-ayurvedic-institute">The Ayurvedic Institute</a> </li>
<li><a href="/about/about-vasant-lad">About Vasant Lad</a> </li>
<li><a href="/about/membership">Membership</a> </li>
<li><a href="/about/donate">Donate</a> </li>
<li><a href="/about/ayurvedic-press">Ayurvedic Press</a> </li>
<li><a href="/about/vioa">Vasanta Institute of Ayurveda Pune, India</a> </li>
<li><a href="/about/documentary">Documentary</a> </li>
<li><a href="/about/ayurvedic-philosophy">Ayurvedic Philosophy</a> </li>
<li><a href="/about/employment-opportunities">Employment Opportunities</a> </li>
<li><a href="/about/contact">Contact Us</a> </li>
<li><a href="/about/directions">Directions</a> </li>
<li><a href="/about/affiliations">Affiliations</a> </li>
<li><a href="/about/newsletter">Email Newsletter</a> </li>
</ul>
</li>
<li><a href="/education/overview">Education</a> <ul>
<li><a href="/education/overview">Education Overview</a> </li>
<li><a href="/education/ayurvedic-studies-programs">Ayurvedic Studies Programs</a> </li>
<li><a href="/seminars/overview">Online Webinars</a> </li>
<li><a href="/education/yoga-teacher-training">Yoga Teacher Training</a> </li>
<li><a href="/education/continuing-education">Continuing Education</a> </li>
<li><a href="/education/pune-training-programs">Training Programs in India</a> </li>
<li><a href="/education/tuition-fees">ASP Tuition &amp; Fees</a> </li>
<li><a href="/education/admissions-application">Admissions &amp; Application</a> </li>
<li><a href="/education/faculty">Faculty &amp; Administration</a> </li>
<li><a href="/education/faqs">Education FAQs</a> </li>
<li><a href="/education/photo-gallery">Photo Gallery</a> </li>
</ul>
</li>
<li><a href="/seminars/overview">Webinars</a> <ul>
<li><a href="/seminars/overview">Webinars Overview</a> </li>
<li><a href="/seminars/online-webinars">Online Webinars</a> </li>
<li><a href="/calendar?view=gallery">Event Calendar</a> </li>
</ul>
</li>
<li><a href="/consultations">Clinic</a> </li>
<li><a href="/panchakarma/overview">Panchakarma</a> <ul>
<li><a href="/panchakarma/overview">Panchakarma Overview</a> </li>
<li><a href="/panchakarma/at-the-ayurvedic-institute">At the Ayurvedic Institute</a> </li>
<li><a href="/panchakarma/your-panchakarma-program">Panchakarma Program FAQs</a> </li>
<li><a href="/panchakarma/stress-management-treatments">Stress Management Treatments</a> </li>
<li><a href="/panchakarma/fees-optional-services">Fees &amp; Optional Services</a> </li>
<li><a href="/panchakarma/covid-protocols-pk-department">COVID-19 Safety Protocols for Panchakarma Program</a> </li>
</ul>
</li>
<li class="drop-right"><a href="/resources/overview">Resources</a> <ul>
<li><a href="/resources/overview">Resources Overview</a> </li>
<li><a href="/resources/general-information">General Information</a> </li>
<li><a href="/resources/food-and-nutrition">Food and Nutrition</a> </li>
<li><a href="/resources/recipes/all">Recipes</a> </li>
<li><a href="/resources/ayurvedic-cleansing-procedures">Ayurvedic Cleansing Procedures</a> </li>
</ul>
</li>
<li><a href="/shop">Shop</a> </li>
</ul> </nav>
<!-- End Main Menu -->
<!--Begin Top Bar-->
<div class="top-bar">
<!-- Begin Top Bar Right -->
<div class="top-bar__right">
<!--Begin Search-->
<form action="/Default.aspx?SiteSearchID=4763&amp;ID=/search-results" class="search" method="post">
<div class="search-row">
<input name="CAT_Search" placeholder="Search..." type="text"/>
<button class="reset" type="submit"><span class="icon-search"></span></button>
</div>
<span class="js-search-close search-close"></span>
</form>
<!--End Search-->
<!-- Begin Social Links -->
<div class="social-links">
<ul>
<li><a class="js-search icon-search" href="#"><span>Search</span></a></li>
<li><a class="icon-printer" href="/_System/Apps/solid-urlbox/public/index.html?url=https://www.ayurveda.com/" rel="nofollow" target="_blank"><span>Print</span></a>
</li><li><a class=" icon-facebook" href="https://www.facebook.com/TheAyurvedicInstitute/" target="_blank"><span>Facebook</span></a></li>
<li><a class=" icon-instagram" href="https://www.instagram.com/theayurvedicinstitute/" target="_blank"><span>Instagram</span></a></li>
<li><a class=" icon-twitter" href="https://twitter.com/ayurvedicinst" target="_blank"><span>Twitter</span></a></li>
<li><a class=" icon-youtube" href="https://www.youtube.com/user/AyurvedicInstitute" target="_blank"><span>YouTube</span></a></li>
<li><a class=" icon-mail" href="/about/newsletter"><span>Newsletter</span></a></li>
</ul>
</div>
<!-- End Social Links -->
</div>
<!-- End Top Bar Right -->
<!-- Begin Top Bar Left -->
<div class="top-bar__left"> <a class="top-bar__addr" href="https://goo.gl/maps/VSZrjSmx62iQRWTs7" target="_blank">11311 Menaul Blvd NE, <span>Albuquerque, NM 87112</span></a> <a class="top-bar__phone" href="tel:(505) 291-9698">(505) 291-9698</a> <a class="top-bar__contact" href="/about/contact">contact</a> </div>
<!-- End Top Bar Left -->
</div>
<!--End Top Bar-->
</div>
<!--Begin Mobile Menu Body Overlay-->
<div class="content-overlay"><span class="icon-arrow-left7"></span></div>
<!--End Mobile Menu Body Overlay-->
</div>
</header>
<!--End Header-->
<!--Begin Main Body-->
<div class="wrapper">
<!--Begin Slider-->
<div class="slider-container">
<div class="slider royalSlider">
<!--Begin Slide-->
<section class="rsContent"> <img alt="01 - 2021 PK Special Offer" class="rsImg" height="645" src="/images/content/hero/01.jpg" width="1920"/>
<!--Begin Slide Layout B-->
<div class="slider-caption-b rsABlock" data-fade-effect="true" data-move-effect="none" data-speed="300">
<div class="row column">
<h2 class="slider-caption-b__headline">Recognized as one of the leading Ayurveda Schools and Ayurvedic Health Spas outside of India.</h2> <div class="slider-caption-b__box">
<div class="slider-caption-b__img" style="background-image:url(/images/content/hero/pk-special-january-2021-slider.jpg);"></div>
<div class="slider-caption-b__box-body">
<div class="slider-caption-b__box-body-inner"> <h3 class="slider-caption-b__title">Panchakarma Special Prices!</h3> <span class="slider-caption-b__secondary-headline">Between Now and Feb 26, 2021</span> <p>Book a 5-Day PK Therapy in January or February and receive $450 off your PK with a $100 credit towards additional treatments. Call Extension 114 to book today.</p> <a class="button" href="/panchakarma/overview">More PK Info</a>
</div>
</div>
</div>
</div>
</div>
<!--End Slide Layout B-->
</section>
<!--End Slide-->
<!--Begin Slide-->
<section class="rsContent"> <img alt="02 - 2021 Spring Webinar Series" class="rsImg" height="645" src="/images/content/hero/01.jpg" width="1920"/>
<!--Begin Slide Layout B-->
<div class="slider-caption-b rsABlock" data-fade-effect="true" data-move-effect="none" data-speed="300">
<div class="row column">
<h2 class="slider-caption-b__headline">Recognized as one of the leading Ayurveda Schools and Ayurvedic Health Spas outside of India.</h2> <div class="slider-caption-b__box">
<div class="slider-caption-b__img" style="background-image:url(/images/content/hero/vasant-lad-namaste.jpg);"></div>
<div class="slider-caption-b__box-body">
<div class="slider-caption-b__box-body-inner"> <h3 class="slider-caption-b__title">Expand Your Ayurvedic Skill Set with the Spring Webinar Series</h3> <p>Learn the essential skills of Ayurveda with Vasant Lad, MASc. Series includes the systems and channels of the body and how to read and evaluate the body by its outward signs.</p> <a class="button" href="https://liveayurprana.com/collections/spring-classes-2021">Find Out More Here</a>
</div>
</div>
</div>
</div>
</div>
<!--End Slide Layout B-->
</section>
<!--End Slide-->
<!--Begin Slide-->
<section class="rsContent"> <img alt="03 - ASP 1 and 2 Enrollment" class="rsImg" height="645" src="/images/content/hero/01.jpg" width="1920"/>
<!--Begin Slide Layout B-->
<div class="slider-caption-b rsABlock" data-fade-effect="true" data-move-effect="none" data-speed="300">
<div class="row column">
<h2 class="slider-caption-b__headline">Recognized as one of the leading Ayurveda Schools and Ayurvedic Health Spas outside of India.</h2> <div class="slider-caption-b__box">
<div class="slider-caption-b__img" style="background-image:url(/images/content/hero/VLad-come-study-with-us.jpg);"></div>
<div class="slider-caption-b__box-body">
<div class="slider-caption-b__box-body-inner"> <h3 class="slider-caption-b__title">Ayurvedic Studies Programs 1 &amp; 2 Enrollment Begins Soon</h3> <p>Our hands-on programs and clinical training prepare you for life as an Ayurvedic Health Counselor and Practitioner. See Vasant Lad's<a href="/newsletter/videos/the-world-needs-healers-come-study-with-us/" target="_blank"> message.</a></p> <span class="slider-caption-b__tertiary-headline">Begin Your Profound Education in Healing</span> <a class="button" href="https://www.ayurveda.com/education/ayurvedic-studies-programs">Learn More</a>
</div>
</div>
</div>
</div>
</div>
<!--End Slide Layout B-->
</section>
<!--End Slide-->
<!--Begin Slide-->
<section class="rsContent"> <img alt="03 - Gratitude to AyurPrana" class="rsImg" height="645" src="/images/content/hero/01.jpg" width="1920"/>
<!--Begin Slide Layout B-->
<div class="slider-caption-b rsABlock" data-fade-effect="true" data-move-effect="none" data-speed="300">
<div class="row column">
<h2 class="slider-caption-b__headline">Recognized as one of the leading Ayurveda Schools and Ayurvedic Health Spas outside of India.</h2> <div class="slider-caption-b__box">
<div class="slider-caption-b__img" style="background-image:url(/images/content/hero/ayurprana-gold.png);"></div>
<div class="slider-caption-b__box-body">
<div class="slider-caption-b__box-body-inner"> <h3 class="slider-caption-b__title">Our Thanks to AyurPrana</h3> <p>For their efforts to make Dr. Lad’s vision of <b>Healing All Beings through Ayurveda</b> to come to fruition. AyurPrana works to make Ayurveda accessible in the modern world.</p> <a class="button" href="https://liveayurprana.com/">Visit AyurPrana</a>
</div>
</div>
</div>
</div>
</div>
<!--End Slide Layout B-->
</section>
<!--End Slide-->
<!--Begin Slide-->
<section class="rsContent"> <img alt="04 - Coronavirus COVID-19 Update" class="rsImg" height="645" src="/images/content/hero/01.jpg" width="1920"/>
<!--Begin Slide Layout B-->
<div class="slider-caption-b rsABlock" data-fade-effect="true" data-move-effect="none" data-speed="300">
<div class="row column">
<h2 class="slider-caption-b__headline">Recognized as one of the leading Ayurveda Schools and Ayurvedic Health Spas outside of India.</h2> <div class="slider-caption-b__box">
<div class="slider-caption-b__img" style="background-image:url(/images/content/events/covid-19.jpg);"></div>
<div class="slider-caption-b__box-body">
<div class="slider-caption-b__box-body-inner"> <h3 class="slider-caption-b__title">Coronavirus COVID-19 Update</h3> <span class="slider-caption-b__secondary-headline">Visit our update page for details.</span> <p>The Ayurvedic Institute remains closed to the public. We are implementing safety protocols to protect our students, clients and staff. See also a<a href="/ayurvedic-perspective-on-covid-19" target="_blank"> message from Vasant Lad.</a></p> <a class="button" href="/coronavirus-covid-19" target="_blank">Updates Here</a>
</div>
</div>
</div>
</div>
</div>
<!--End Slide Layout B-->
</section>
<!--End Slide-->
</div>
</div>
<!--End Slider-->
<!--Begin Subscribe Bar-->
<div class="subscribe-bar">
<div class="row column">
<div class="newsletter-subscribe noise">
<div class="newsletter-subscribe__container"> <span class="newsletter-subscribe__headline">Newsletter Signup</span>
<script id="f_mc_f" src="/seminar-waitlist/load/form/mc/" type="text/javascript"></script>
</div>
</div>
</div>
</div>
<!--End Subscribe Bar-->
<!-- Begin Page Body -->
<main class="main main--home">
<div class="row column">
<div class="alert alert-info">
<p><strong>The Ayurvedic Institute will remain closed to the general public through January 24, 2021 and reopen Monday, January 25.</strong> Our
                Office and Store are open for phone orders and questions! To order products, call 800-863-7721 or order online. We offer <strong>CURBSIDE PICKUP</strong>                for local orders. Our Panchakarma Department is open and receiving clients. Call 505-291-9698 for questions. Press “ 1 ” to talk to our
                Office and “114” to talk our Panchakarma staff. <strong>We are New Mexico Safe Certified!</strong> </p>
</div>
<div class="alert alert-success">
<p>We are in an active adjustment period for COVID-19 to help ensure the safety of all individuals: our students, staff, and faculty. We continuously
                adjust to the needs around COVID-19 in all aspects of our operations and school. This means we are assessing our protocols with our panchakarma
                department, our store, and our school classes on a recurring basis. At this time we have limited accessibility via online courses and webinars.
                Check our <a href="/coronavirus-covid-19" target="_blank" title="Coronavirus COVID-19 Updates">update page</a> for the most current information.
                Please reach out to our <a href="/education/contact-admissions" target="_blank" title="Contact Admissions">admissions department</a> for
                details on the status of our school programs. We thank you for your patience and flexibility.</p>
</div> <section class="row xsmall-up-1 small-up-2 medium-up-4 featured-blocks mb1">
<!--Begin Featured Item-->
<div class="column">
<div class="featured-box img-zoom"> <a class="featured-box__img img-zoom__container" href="/seminars/overview"><img alt="2 Online Series that Enhance Your Ayurvedic Skills!" class="img-zoom__img img-zoom__img--slowest" src="/images/content/featured/Lad-in-class-2016.jpg"/> </a>
<div class="featured-box__body" data-mh="featured-box">
<h2><a href="/seminars/overview">2 Online Series that Enhance Your Ayurvedic Skills!</a></h2>
<p>Introduction to the Channels of the Body Series + Body Reading / Lakshana Series </p>
<a class="button tertiary" href="/seminars/overview">Learn More</a> </div>
</div>
</div>
<!--End Featured Item-->
<!--Begin Featured Item-->
<div class="column">
<div class="featured-box img-zoom"> <a class="featured-box__img img-zoom__container" href="/education/ayurvedic-studies-programs"><img alt="Ayurvedic Studies&lt;br /&gt; Programs" class="img-zoom__img img-zoom__img--slowest" src="/images/content/education/vedic-stories-homa.jpg"/> </a>
<div class="featured-box__body" data-mh="featured-box">
<h2><a href="/education/ayurvedic-studies-programs">Ayurvedic Studies<br/> Programs</a></h2>
<p>Full-time Certificate Programs:<br/> Clinical, Spiritual, Practical, Authentic Ayurveda<br/><br/>Applications Available Now!<br/></p>
<a class="button secondary" href="/education/ayurvedic-studies-programs">Learn More</a> </div>
</div>
</div>
<!--End Featured Item-->
<!--Begin Featured Item-->
<div class="column">
<div class="featured-box img-zoom"> <a class="featured-box__img img-zoom__container" href="/newsletter/videos/the-world-needs-healers-come-study-with-us/"><img alt="Vasant Lad&lt;br /&gt;Video" class="img-zoom__img img-zoom__img--slowest" src="/images/content/featured/Vasant-lad-come-study-with-us.jpg"/> </a>
<div class="featured-box__body" data-mh="featured-box">
<h2><a href="/newsletter/videos/the-world-needs-healers-come-study-with-us/">Vasant Lad<br/>Video</a></h2>
<p>Vasant Lad talks about how the world needs healers.</p>
<a class="button " href="/newsletter/videos/the-world-needs-healers-come-study-with-us/">Watch Video</a> </div>
</div>
</div>
<!--End Featured Item-->
<!--Begin Featured Item-->
<div class="column">
<div class="featured-box img-zoom"> <a class="featured-box__img img-zoom__container" href="/about/donate"><img alt="Donate to the Ayurvedic Institute" class="img-zoom__img img-zoom__img--slowest" src="/images/content/hero/donation-link.jpg"/> </a>
<div class="featured-box__body" data-mh="featured-box">
<h2><a href="/about/donate">Donate to the Ayurvedic Institute</a></h2>
<p>Be a part of the growth of Ayurveda in the West. We invite your support of our mission and value your contribution.</p>
<a class="button quaternary" href="/about/donate">Learn More</a> </div>
</div>
</div>
<!--End Featured Item-->
</section>
<!--End Featured Blocks-->
<!--Begin Main Featured-->
<section class="main-featured">
<div class="main-featured__body">
<div class="main-featured__body-inner">
<h1 class="h4 headings mb6">Education</h1>
<h2 class="subheader caps light">Let Ayurveda Change Your Whole Life</h2>
<p class="t2">Each of our programs is designed to provide opportunities to deepen your awareness of the healing traditions of Ayurveda that have
                        been practiced for thousands of years and to understand how these ancient teachings are relevant today in your personal life and
                        clinical practice.</p>
<div class="main-featured__cta">
<a href="/pdf-viewer/?pdf=%2Fpdf%2Facademic-catalog.pdf&amp;title=2020%20-%202021%20Academic%20Catalog" target="_blank"><img alt="Academic Catalog" class="main-featured__cta-img" src="/images/content/featured/catalog.jpg"/>
<br/> <span class="main-featured__cta-title">2020-2021<br/> Academic Catalog</span> </a> <a class="button" href="/pdf-viewer/?pdf=%2Fpdf%2Facademic-catalog.pdf&amp;title=2020%20-%202021%20Academic%20Catalog" target="_blank">Download Now</a> </div>
</div>
</div>
<div class="main-featured__img" style="background-image:url(/images/content/featured/main.jpg)"> </div>
</section>
<!--End Main Featured-->
<br/>
<br/>
</div>
</main>
<!-- End Page Body -->
<!--Begin Featured Navigation-->
<div class="featured-nav mt4 mb4">
<div class="row column">
<!--Begin Two Circles-->
<div class="row xsmall-up-1 small-up-2">
<!--Begin Featured Nav Block-->
<div class="column">
<div class="featured-nav__block">
<div class="featured-nav__block-img"><a href="/shop"><img alt="Online Store" height="225" src="/images/content/featured/featured-nav-01.jpg" width="225"/></a></div>
<div class="featured-nav__block-body"> <span class="featured-nav__block-title">Shop</span> <span class="featured-nav__block-name">Online Store</span>
<p>We offer a complete range of Ayurveda lifestyle products. You can order by phone or online.</p>
<a class="button" href="/shop">Shop Now</a>
</div>
</div>
</div>
<!--End Featured Nav Block-->
<!--Begin Featured Nav Block-->
<div class="column">
<div class="featured-nav__block">
<div class="featured-nav__block-img"><a href="/panchakarma/overview"><img alt="Panchakarma" height="225" src="/images/content/featured/featured-nav-02.jpg" width="225"/></a></div>
<div class="featured-nav__block-body"> <span class="featured-nav__block-title">Restore</span> <span class="featured-nav__block-name">Panchakarma</span>
<p>A cleansing and rejuvenating program for the body, mind and consciousness.</p>
<a class="button" href="/panchakarma/overview">Learn More</a>
</div>
</div>
</div>
<!--End Featured Nav Block-->
</div>
<!--End Two Circles-->
</div>
</div>
<!--End Featured Navigation-->
<!-- Begin Footer -->
<footer class="footer">
<div class="footer__top">
<div class="row column">
<div class="footer-col footer-col--a"> <a href="/"><img alt="The Ayurvedic Institute" height="89" src="/images/template/logo-footer.png" width="80"/></a> </div>
<div class="footer-col footer-col--b">
<h3>Quick Links</h3>
<div class="footer-links footer-links--quick"> <ul>
<li><a href="/about/the-ayurvedic-institute">About Us</a></li>
<li><a href="/consultations">Consultations</a></li>
<li><a href="/resources/overview">Resources</a></li>
<li><a href="/education/overview">Education</a></li>
<li><a href="/panchakarma/overview">Panchakarma</a></li>
<li><a href="/about/employment-opportunities">Careers</a></li>
<li><a href="/seminars/overview">Seminars</a></li>
<li><a href="/products">Products</a></li>
<li><a href="/about/contact">Contact</a></li>
</ul> </div>
</div>
<div class="footer-col footer-col--c">
<h3>Social</h3>
<div class="footer-links footer-links--social"><ul><li><a class=" icon-facebook" href="https://www.facebook.com/TheAyurvedicInstitute/" target="_blank"><span>Facebook</span></a></li>
<li><a class=" icon-instagram" href="https://www.instagram.com/theayurvedicinstitute/" target="_blank"><span>Instagram</span></a></li>
<li><a class=" icon-twitter" href="https://twitter.com/ayurvedicinst" target="_blank"><span>Twitter</span></a></li>
<li><a class=" icon-youtube" href="https://www.youtube.com/user/AyurvedicInstitute" target="_blank"><span>YouTube</span></a></li>
<li><a class=" icon-mail" href="/about/newsletter"><span>Newsletter</span></a></li>
</ul></div>
</div>
<div class="footer-col footer-col--d">
<h3>Contact</h3>
<p class="footer-addr"> <a class="footer-addr__addr" href="https://goo.gl/maps/VSZrjSmx62iQRWTs7" target="_blank">11311 Menaul Blvd NE,<br/>
          Albuquerque, NM 87112</a> <a class="footer-addr__phone" href="tel:(505) 291-9698">(505) 291-9698</a> </p>
</div>
</div>
</div>
<div class="footer__bottom">
<div class="row column">
<div class="copyright">© 2021 The Ayurvedic Institute.  All Rights Reserved. <span><a href="/fair-use-policy">Fair Use Policy</a>. <a href="/privacy-policy">Privacy Policy</a>. Site by <a href="http://www.namasteinteractive.com/" target="_blank">Namaste Interactive</a>.</span></div>
</div>
</div>
</footer>
<!-- End Footer -->
</div>
<!--End Main Body-->
</div>
<!--Begin Body Close Tags and Scripts-->
<!--Begin Scripts -->
<script src="/js/extras.js" type="text/javascript"></script>
<script src="/js/scripts.js" type="text/javascript"></script>
<!--End Scripts -->
<!--Begin Schema JSON-->
<script type="application/ld+json">
{
	"@context": "http://schema.org",
	"@type": "Organization",
	"name": "The Ayurvedic Institute",
			"telephone": "(505) 291-9698",	"faxNumber": "(505) 294-7572",		"logo": "/images/template/the-ayurvedic-institute.png",	"address":
	[
	{
		"@type": "PostalAddress",
		"streetAddress": "11311 Menaul Blvd NE ",
		"addressLocality": "Albuquerque",
		"addressRegion": "NM",
		"postalCode": "87112",
		"addressCountry": "United States",
		"telephone": "(505) 291-9698",
		"faxNumber": "(505) 294-7572"
	},
	{
		"@type": "PostalAddress",
		"streetAddress": "P.O. Box 23445 ",
		"addressLocality": "Albuquerque",
		"addressRegion": "NM",
		"postalCode": "87192-1445",
		"addressCountry": "United States",
		"telephone": "",
		"faxNumber": ""
	}
		]
}
</script><!--End Schema JSON-->
<!-- AI_WEBAN --> <script type="text/javascript"> var _paq = _paq || []; _paq.push(['trackPageView']); _paq.push(['enableLinkTracking']); (function() { var u=(("https:" == document.location.protocol) ? "https" : "http") + "://ayurveda.com/ai_weban/"; _paq.push(['setTrackerUrl', u+'piwik.php']); _paq.push(['setSiteId', 1]); var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript'; g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s); })();  </script> <noscript><p><img alt="" src="http://ayurveda.com/ai_weban/piwik.php?idsite=1" style="border:0;"/></p></noscript> <!-- AI_WEBAN -->
<!--End Body Close Tags and Scripts-->
</body>
</html>