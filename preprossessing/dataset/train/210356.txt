<?xml version="1.0" encoding="iso-8859-1"?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="de" xml:lang="de" xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="https://fonts.googleapis.com/css?family=Oswald:400,300" rel="stylesheet"/>
<title>
Gedenkstätte Berliner Mauer</title>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="de" http-equiv="content-language"/>
<meta content="text/javascript" http-equiv="Content-Script-Type"/>
<meta content="noodp" name="robots"/>
<meta content="noodp" name="msnbot"/>
<meta content="noodp" name="GoogleBot"/>
<meta content="" name="author"/>
<meta content="" name="publisher"/>
<meta content="Die Gedenkstätte Berliner Mauer ist der zentrale Erinnerungsort an die deutsche Teilung." name="description"/>
<meta content="Gedenkstätte Berliner Mauer, Dokumentationszentrum Berliner Mauer, Berliner Mauer, Mauer, Mauerbau, 13. August 1961, Bernauer Straße, Kapelle der Versöhnung, West-Berlin, Ost-Berlin, geteilte Stadt, Geschichte der Teilung, Fenster des Gedenkens" name="keywords"/>
<meta content="index,follow" name="robots"/>
<meta content="15 days" name="revisit-after"/>
<meta content="PRO-Base, www.pro-base.de" name="generator"/>
<meta content="de" name="content-language"/>
<meta content="no-cache" http-equiv="pragma"/>
<link href="favicon.ico" rel="shortcut icon"/>
<link href="favicon.ico" rel="icon" type="image/ico"/>
<!-- <script language="JavaScript" src="includes/jquery-1.11.0.min.js" type="text/javascript"></script> -->
<script language="JavaScript" src="includes/jquery-3.4.1.min.js" type="text/javascript"></script>
<script language="JavaScript" src="https://vjs.zencdn.net/7.8.2/video.js" type="text/javascript"></script>
<!-- <script language="JavaScript" src="includes/jquery.magnific-popup.min.js" type="text/javascript"></script> -->
<script language="JavaScript" src="includes/magnific-popup.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/jquery-ui.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/jquery.colorbox-min.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/jquery.event.move.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/jquery.twentytwenty.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/audiojs/audio.min.js" type="text/javascript"></script>
<!-- <script language="JavaScript" src="includes/videojs/video.min.js" type="text/javascript"></script> -->
<!-- <script language="JavaScript" src="includes/videojs/videojs-playlist.js" type="text/javascript"></script> -->
<!-- <script src="https://unpkg.com/video.js/dist/video.js"></script>
<script src="https://unpkg.com/@videojs/http-streaming/dist/videojs-http-streaming.js"></script> -->
<script language="JavaScript" src="includes/functions.js" type="text/javascript"></script>
<script>
	// videojs.options.flash.swf = "includes/videojs/video-js.swf"
</script>
<!--[if IE 8]>
	<script src="includes/videojs/videojs-ie8.min.js"></script>
	<![endif]-->
<link href="css/screen.css?1610557753" media="screen,print" rel="stylesheet" type="text/css"/>
<link href="css/magnific-popup.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="css/jquery-ui.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="css/colorbox.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="css/twentytwenty.css" media="screen" rel="stylesheet" type="text/css"/>
<!-- <link rel="stylesheet" type="text/css" media="screen" href="https://unpkg.com/video.js/dist/video-js.css"> -->
<!-- <link rel="stylesheet" type="text/css" media="screen" href="https://vjs.zencdn.net/7.8.2/video-js.css" /> -->
<link href="css/video-js.css" media="screen" rel="stylesheet" type="text/css"/>
</head><body>
<div id="outer_main">
<div id="inner_main">
<div id="head">
<div style="float:left;">
<form action="select_lang.php" method="get" name="langselect" target="_blank">
<a href="../de/" style="padding-left:0px;">Deutsch</a>
<a href="leichte-sprache-1714.html">Leichte Sprache</a>
<a href="../en/">English</a>
<a href="../fr/">Français</a>
<select id="pdfSelector" name="lang" onchange="window.open(this.value);this.selectedIndex=0;" style="font-weight:normal;">
<option value="">others (PDF)</option>
<option value="download/italian.pdf">Italian</option>
<option value="download/japanese.pdf">Japanese</option>
<option value="download/russian.pdf">Russian</option>
<option value="download/spanish.pdf">Spanish</option>
<option value="download/english.pdf">English</option>
<option value="download/french.pdf">French</option>
</select>
</form>
</div>
<div style="float:right;width:55%;text-align:right;">
<a href="https://www.facebook.com/pages/Stiftung-Berliner-Mauer/119701714673" id="fb_head" target="_blank" title="Folgen Sie der Stiftung Berliner Mauer auf facebook."><span class="invisible">facebook</span></a>
<form action="suche-7.html" method="get">
<a href="https://www.stiftung-berliner-mauer.de" target="_blank">Stiftung</a>
<a href="sitemap-4.html">Sitemap</a>
<label for="suche" style="border-left:1px solid #ffffff;padding:10px 0px 5px 10px;">Suche</label>
<input id="suche" name="suchwort" style="font-weight:normal;" type="text"/>
<input class="submit" type="submit" value="SUCHEN"/>
</form>
</div>
<a href="index.html"><img alt="Hier geht es zur Startseite!" src="pix/logo.gif"/></a> <!-- <div id="kes-logo"><img src="uploads/logos/kes_logo.png" alt="Logo Europ&auml;isches Kulturerbe-Siegel" title="Europ&auml;isches Kulturerbe-Siegel" /></div> -->
</div> <div class="head_start">
<div id="navig"><div class="level1"><a class="" href="die-berliner-mauer-10.html">Die Berliner Mauer</a><a class="" href="der-historische-ort-11.html">Der historische Ort</a><a class="" href="die-gedenkstaette-berliner-mauer-12.html">Die Gedenkstätte</a><a class="" href="forschung-244.html">Forschung</a><a class="" href="zeitzeugen-13.html">Zeitzeugen</a><a class="" href="sammlungen-1051.html">Sammlungen</a><a class="" href="bildung-15.html">Bildung</a><a class="" href="veranstaltungen-14.html">Veranstaltungen</a><a class="" href="besucherservice-16.html">Besucherservice</a><a class="" href="presse-17.html">Presse</a><a class="" href="kontakt-29.html">Kontakt</a></div></div><div id="headpic"><img alt="" src="pix/bild_startseite.jpg"/></div> <div style="clear:both;"><!-- --></div>
</div>
<div id="aligner_start">
<div id="content_start">
<p style="margin-top:5px;margin-bottom:20px;">Die Gedenkstätte Berliner Mauer ist der zentrale Erinnerungsort an die deutsche Teilung, 
gelegen im Zentrum der Hauptstadt. Am historischen Ort in der Bernauer Straße erstreckt sie sich auf 1,4 km Länge über den 
ehemaligen Grenzstreifen. Auf dem Areal der Gedenkstätte befindet sich das letzte Stück der Berliner Mauer, das in seiner 
Tiefenstaffelung erhalten geblieben ist und einen Eindruck vom Aufbau der Grenzanlagen zum Ende der 1980er Jahre vermittelt. Anhand der 
weiteren Reste und Spuren der Grenzsperren sowie der dramatischen Ereignisse an diesem Ort wird exemplarisch die Geschichte der Teilung 
nachvollziehbar. Die Gedenkstätte ist Teil der <a class="extern" href="https://www.stiftung-berliner-mauer.de">Stiftung Berliner Mauer</a>, 
zu der auch die <a class="extern" href="https://www.notaufnahmelager-berlin.de">Erinnerungsstätte Notaufnahmelager Marienfelde</a>, die 
<a class="extern" href="https://www.gedenkstaette-guenter-litfin.de">Gedenkstätte Günter Litfin</a> und die 
<a class="extern" href="https://www.eastsidegalleryberlin.de">East Side Gallery</a> gehören.</p> <!-- Anfang linke Spalte -->
<div id="left_column">
<!-- Video zum 9. November 2020 -->
<div style="width:400px; padding-bottom:10px;">
<h2>31 Jahre nach dem Mauerfall</h2>
<video class="video-js" height="225" id="single-player" poster="uploads/2020_11_09_video/poster_videobotschaften_fhd.png" width="400">
<source src="uploads/2020_11_09_video/2020_11_09_videobotschaften_1024.mp4">
</source></video>
<p style="padding:10px 0;"><strong>Videobotschaften zum 9. November 1989 <a href="https://www.berliner-mauer-gedenkstaette.de/de/aktuelles-620.html">mehr</a></strong></p>
</div>
<div class="teaser">
<h2>TODESOPFER</h2>
<p><img alt="Das Fenster des Gedenkens" src="uploads/todesopfer_bilder/fdg_home.jpg" style="float:left;padding-right:10px;padding-bottom:5px;"/> <br/>Die Todesopfer an der Berliner Mauer 1961 - 1989 <a href="todesopfer-240.html" target="_self">mehr</a> </p>
</div><!-- Ende div.teaser -->
<div class="teaser">
<h2>GENERATION 1975</h2>
<p><img alt="Ausstellungsflyer Generation 75" src="uploads/zeitzeugen_bilder/generation75_ausstellung_home.jpg" style="float:left;padding-right:10px;padding-bottom:5px;"/> Videoinstallation in der Erinnerungsstätte Notaufnahmelager Marienfelde <a href="generation75-1758.html" target="_self">mehr</a> </p>
</div><!-- Ende div.teaser -->
<div class="teaser">
<h2>EUROPAS GRENZ-WERTE</h2>
<p><img alt="Europas Grenz-Werte" src="uploads/startseite/europas_gren_werte_home.jpg" style="float:left;padding-right:10px;padding-bottom:5px;"/> <br/>Vom Mauerfall bis Corona. <a href="europas-grenz-werte-1902.html" target="_self">mehr</a> </p>
</div><!-- Ende div.teaser -->
<div class="teaser">
<h2>GOODBYE CHARLIE</h2>
<p><img alt="Goodbye Checkpoint Charlie" src="uploads/goodbye_cpc/goodbye_cpc_home.jpg" style="float:left;padding-right:10px;padding-bottom:5px;"/> <br/>Die internationalen 2+4 Verhand-lungen zur deutschen Einheit 1990 <a href="goodbye-checkpoint-charlie-1897.html" target="_self">mehr</a> </p>
</div><!-- Ende div.teaser -->
<div class="teaser">
<h2>ONLINE AUF SPURENSUCHE</h2>
<p><img alt="Mobiler Tourguide" src="uploads/allgemeine_bilder/tourguide_home.jpg" style="float:left;padding-right:10px;padding-bottom:5px;"/> <br/>Mit der mobilen Website der Gedenkstätte Berliner Mauer auf Entdeckungstour gehen <a href="mobiler-tourguide-623.html" target="_self">mehr</a> </p>
</div><!-- Ende div.teaser -->
<div class="teaser">
<h2>GEHEN ODER BLEIBEN?</h2>
<p><img alt="Kooperation Theater Strahl" src="uploads/bildung_bilder/strahl_berlinberlin_home.jpg" style="float:left;padding-right:10px;padding-bottom:5px;"/> <br/>Unser Begleitprogramm zum Theaterstück #BerlinBerlin des Theaters Strahl <a href="kooperation-theater-strahl-berlin-1637.html" target="_self">mehr</a> </p>
</div><!-- Ende div.teaser -->
</div> <!-- Ende #left_column -->
<div id="right_column">
<div><!-- &Ouml;ffnungszeiten, height normal: 225px -->
<h2>Corona-Pandemie</h2>
<h3>
   	Aufgrund der Corona-Pandemie bleiben das Dokumentations- und das Besucherzentrum der Gedenkstätte Berliner Mauer bis auf Weiteres geschlossen. 
   	Derzeit finden keine Führungen und Veranstaltungen vor Ort statt.
   </h3>
<br/>
<h3>Ausstellung im Gedenkstättenareal</h3>
<p>
		Die Außenausstellung im Gedenkstättenareal an der Bernauer Straße ist unter Einhaltung der geltenden Abstands- und Hygieneregelungen uneingeschränkt zugänglich (Montag - Sonntag, 08.00 - 22.00 Uhr).   		
   </p>
<h3>Ausstellung Geisterbahnhöfe</h3>
<p>
		Die  Ausstellung <a href="geisterbahnhoefe-558.html" target="_self">"Grenz- und 
		Geisterbahnhöfe im geteilten Berlin"</a> kann während der Öffnungszeiten des 
		S-Bahnhofs Nordbahnhof besucht werden.
	</p>
<h3>Andachten in der Kapelle der Versöhnung</h3>
<p>
		Die Andachten in der Kapelle der Versöhnung zum Gedenken an die Todesopfer an der Berliner Mauer finden regulär statt, kurzfristige Änderungen sind 
		jedoch vorbehalten.
	</p>
<p class="location">
<!-- <a class="popup-gmaps" href="https://maps.google.de/maps?hl=de&client=firefox-a&ie=UTF8&q=gedenkst%C3%A4tte+berliner+mauer&fb=1&gl=de&hq=gedenkst%C3%A4tte+berliner+mauer&hnear=Brandenburg,+Teltow&cid=0,0,16885119300824838572&ei=lOvOS-vtG8qbOP2CxcgP&ved=0CAcQnwIwAA&ll=52.536143,13.390017&spn=0.008875,0.01929&z=16&iwloc=A">Open Google Map</a> -->
<!-- Der Link, der das Popup mit dem Lageplan &ouml;ffnet, wird per jquery eingef&uuml;gt (s. index.php) -->
</p>
<p>
<a class="download" href="uploads/flyer/gedenkstaette_berliner_mauer.pdf" style="background-image:url(pix/icon_pdf.gif);" target="_blank" title="Gedenkstätte Berliner Mauer"> Gedenkstätte Berliner Mauer</a>
</p>
<noscript>
<p><a href="verkehrsanbindung-164.html">Lageplan und Verkehrsanbindung</a></p>
</noscript>
</div>
<!-- <div>
	<h2>Besucherzentrum</h2>
	<p>Informationen &uuml;ber die Angebote der Gedenkst&auml;tte Berliner Mauer sowie Orientierung &uuml;ber das Gedenkst&auml;ttenareal <a href="besucherzentrum-560.html">mehr</a></p>
</div> -->
<div><!-- FÃ¼hrungen -->
<h2>FÜHRUNGEN UND BILDUNGSANGEBOTE</h2>
<p>Die Gedenkstätte Berliner Mauer bietet unterschiedliche Führungen und Seminare an.<br/>
<a href="fuehrungen-496.html" style="margin-right: 25px;">Führungen</a><a href="bildung-15.html">Seminare</a></p>
<p>
<a class="download" href="uploads/flyer/angebote_fuer_kinder.pdf" style="background-image:url(pix/icon_pdf.gif);" target="_blank" title="Angebote für Kinder">Angebote für Kinder</a> <br/>
<a class="download" href="uploads/flyer/mobiler_tourguide.pdf" style="background-image:url(pix/icon_pdf.gif);" target="_blank" title="Digitaler Tourguide">Online auf Spurensuche</a>
</p>
</div>
<div><!-- Dokumentationszentrum -->
<h2>1961 | 1989. DIE BERLINER MAUER</h2>
<p>Multimediale Dauerausstellung zur Geschichte der Teilung Berlins im Dokumentationszentrum der Gedenkstätte Berliner Mauer. Wie kam es zum Mauerbau? Wieso stand die Mauer so lange? 
	Warum fiel sie 1989? <a href="dokumentationszentrum-213.html">mehr</a></p>
</div>
<div><!-- Andachten -->
<h2>GEDENKEN</h2>
<p>Zum Gedenken an die Todesopfer an der Berliner Mauer finden in der Kapelle der Versöhnung regelmäßig Andachten statt  
	<a href="andachten-582.html">mehr</a></p>
<p>
<a class="download" href="uploads/flyer/gedenkandachten-sep-dez-2020.pdf" style="background-image:url(pix/icon_pdf.gif);" target="_blank" title="Gedenkandachten September - Dezember 2020">Gedenkandachten September - Dezember 2020</a><br/>
</p>
</div>
<div>
<h2>FILM ÜBER DIE GEDENKSTÄTTE</h2>
<p>Der Film informiert über die Gedenkstätte Berliner Mauer als Ort der Erinnerung an die deutsche 
Teilung und die Opfer der Mauer  (ca. 10 Min.)<br/><a href="film-ueber-die-gedenkstaette-779.html">Film ansehen</a></p>
</div>
</div><!-- Ende #right_column -->
</div> <!-- Ende #content_start -->
</div> <!-- Ende #aligner_start -->
<div style="margin-top:25px;width:190px;float:right;">
<div id="marginal_start" style="width:190px;margin:0;padding:0px;">
<div class="content" style="margin:0 0 46px 0;padding:10px 0px 0px 0px;">
<img class="ls-homelogo" src="uploads/leichte_sprache/logo_etr.gif" title="Â© EuropÃ¤isches Logo fÃ¼r einfaches Lesen: Inclusion Europe. Weitere Informationen unter www.leicht-lesbar.eu "/>
<p><strong>Informationen in Leichter Sprache <br/><br/><a href="leichte-sprache-1714.html">hier klicken</a></strong></p>
</div>
<div class="content" style="margin:0;padding:0px;">
<h1>AKTUELLES</h1>
<h3>Corona-Pandemie - Schließung der Häuser und Ausstellungen</h3>
<p>
<a class="mehr" href="besucherservice-aktuelles-1023.html#1989">mehr</a>
</p><h3>Die Stiftung Berliner Mauer unterstützt "Die Vielen"</h3>
<p>
<a class="mehr" href="berliner-erklaerung-der-vielen-1872.html#1873">mehr</a>
</p> <div style="height:10px;"><!-- --></div>
</div>
<div class="content">
<div style="height:10px;"><!-- --></div>
</div>
<!-- Inhalt der Marginalie OHNE Aktuelles und Termine -->
<div id="marginal"></div>
<!-- Marginalie Ende -->
<div class="breaker"><!-- --></div>
</div>
</div>
<div id="contact_service">
<div class="teaser" style="margin-left:15px;">
<h2>KONTAKT</h2>
<p>Gedenkstätte Berliner Mauer<br/>
	Bernauer Straße 111<br/>
	13355 Berlin<br/>
	Fon: +49 (0)30 213085-123<br/>
<a href="kontaktformular-30.html?ELM=YVc1bWIwQnpkR2xtZEhWdVp5MWlaWEpzYVc1bGNpMXRZWFZsY2k1a1pYd3hZekJpTnpabVkyVTNOemxtTnpobU5URmlaVE16T1dNME9UUTBOV00wT1h3PQ%3D%3D">E-Mail schreiben</a>
</p>
</div>
<div class="teaser" style="margin-left:20px;">
<h2>SPENDEN</h2>
<p>Unterstützen Sie die Arbeit der Gedenkstätte Berliner Mauer mit einer Spende 
	<a href="spenden-800.html">mehr</a></p>
</div>
<div class="teaser" style="width: 300px; margin-left:25px;">
<h2>ÜBER UNS</h2>
<p>Die Gedenkstätte Berliner Mauer ist Teil der <a class="extern" href="https://www.stiftung-berliner-mauer.de">Stiftung Berliner Mauer</a>.<br/>
	Weitere Standorte: <br/>
<a class="extern" href="https://www.notaufnahmelager-berlin.de">Erinnerungsstätte 
	Notaufnahmelager Marienfelde</a> <br/>
<a class="extern" href="https://www.gedenkstaette-guenter-litfin.de">Gedenkstätte Günter Litfin</a><br/>
<a class="extern" href="https://www.eastsidegalleryberlin.de">East Side Gallery</a></p>
</div>
<div class="teaser" style="margin-left:20px;">
<h2>Infos per E-Mail</h2>
<p>Möchten Sie regelmäßig über Veranstaltungen der Stiftung Berliner Mauer 
	informiert werden? Dann abonnieren Sie unsere Veranstaltungshinweise<br/>
<a href="veranstaltungseinladungen-1253.html">mehr</a></p>
</div> </div>
<div id="sponsors">
<a class="extern" href="http://www.kulturstaatsministerin.de" target="_blank" title="Gefördert von: Die Beauftragte der Bundesregierung für Kultur und Medien"><img alt="Die Beauftragte der Bundesregierung für Kultur und Medien" src="uploads/sponsoren/logo_bkm.gif" style="margin-right:10px;"/></a>
<a class="extern" href="https://www.bundesregierung.de/breg-de/bundesregierung/staatsministerin-fuer-kultur-und-medien/neustart-kultur-startet-1775272" target="_blank" title="Sonderprogramm ''Neustart Kultur'' der Beauftragten der Bundesregierung für Kultur und Medien"><img alt="Neustart Kultur" src="uploads/sponsoren/bkm_neustart_kultur.png " style="margin-right:10px;"/></a>
<img alt="Europäische Union: Europäischer Fonds für regionale Entwicklung" src="uploads/sponsoren/logo_eu_efre.jpg" style="margin-right:6px; margin-bottom:2px;" title="Europäische Union: Europäischer Fonds für regionale Entwicklung"/>
<img alt="LOTTO-Stiftung Berlin" src="uploads/sponsoren/logo_lottostiftung_berlin.jpg" style="margin-right:8px; margin-bottom:1px;" title="LOTTO-Stiftung Berlin"/>
<img alt="be Berlin - Die Hauptstadtkampagne" src="uploads/sponsoren/logo_beberlin.jpg" style="margin-right:0; margin-bottom:-1px;" title="be Berlin - Die Hauptstadtkampagne"/>
<!-- <img src="uploads/sponsoren/logo_guetesiegel.jpg" alt="G&uuml;tesiegel Berliner Mauer" title="G&uuml;tesiegel Berliner Mauer"> -->
</div>
<div style="clear:both;height:1px;"><!-- --></div>
<div id="footer">
<div style="padding:8px;padding-top:3px;padding-bottom:3px;">© GEDENKSTÄTTE BERLINER MAUER 2021<a href="index.html">Home</a><a href="kontaktformular-30.html">Kontakt</a><a href="datenschutz-3.html">Datenschutzerklärung</a><a href="impressum-6.html">Impressum</a></div>
</div>
</div>
</div>
<script language="JavaScript" src="includes/jquery-config.js" type="text/javascript"></script>
</body>
</html>
