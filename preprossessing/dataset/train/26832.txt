<!-- BEGIN: preheader --><!DOCTYPE html>
<html dir="ltr" id="htmlTag" lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="https://www.3djobs.com/"/>
<link href="favicon.ico" rel="Shortcut Icon" type="image/x-icon"/>
<link href="css.php?styleid=38&amp;td=ltr&amp;sheet=css_reset.css,css_unreset.css,css_utilities.css&amp;ts=1604643582 " rel="stylesheet" type="text/css"/>
<link href="css.php?styleid=38&amp;td=ltr&amp;sheet=css_imgareaselect-animated.css,css_jquery-ui-1_12_1_custom.css,css_jquery_qtip.css,css_jquery_selectBox.css,css_jquery_autogrow.css,css_global.css,css_fonts.css,css_b_link.css,css_b_layout.css,css_b_button.css,css_b_button_group.css,css_b_icon.css,css_b_tabbed_pane.css,css_b_form_control.css,css_b_form_input.css,css_b_form_select.css,css_b_form_textarea.css,css_b_media.css,css_b_divider.css,css_b_avatar.css,css_b_ajax_loading_indicator.css,css_responsive.css,css_b_vbscroller.css,css_b_theme_selector.css,css_b_top_background.css,css_b_module.css,css_b_comp_menu_horizontal.css,css_b_comp_menu_dropdown.css,css_b_comp_menu_vert.css,css_b_top_menu.css&amp;ts=1604643582 " rel="stylesheet" type="text/css"/>
<script src="js/header-rollup-564.js" type="text/javascript"></script>
<!-- END: preheader --><!-- BEGIN: error_page --><!-- BEGIN: header -->
<meta content="3DjOBS" property="og:site_name"/>
<meta content="" property="og:description"/>
<meta content="https://www.3djobs.com" property="og:url"/>
<meta content="website" property="og:type"/>
<link href="xmlsitemap.php" rel="sitemap" type="application/xml"/>
<link href="https://www.3djobs.com" rel="canonical"/>
<meta content="width=device-width, initial-scale=1, viewport-fit=cover" name="viewport"/>
<title>
		
		3DjOBS
	</title>
<meta content="" name="description"/>
<meta content="vBulletin 5.6.4" name="generator"/>
<link href="css.php?styleid=38&amp;td=ltr&amp;sheet=css_login.css&amp;ts=1604643582 " rel="stylesheet" type="text/css"/>
<link href="css.php?styleid=38&amp;td=ltr&amp;sheet=css_b_modal_banner.css&amp;ts=1604643582 " rel="stylesheet" type="text/css"/>
<link class="js-additional-css" href="css.php?styleid=38&amp;td=ltr&amp;sheet=css_additional.css&amp;ts=1604643582 " rel="stylesheet" type="text/css"/>
<script type="text/javascript">
		if (location.hash) {
			document.write('<style type="text/css"> a.anchor { display:none;} <\/style>');
		}

		var uploadUrlTarget = "https://www.3djobs.com/uploader/url";
		var pageData = {
			"baseurl": "https://www.3djobs.com",
			"baseurl_path": "/",
			"baseurl_core": "https://www.3djobs.com/core",
			"baseurl_pmchat": "https://www.3djobs.com/pmchat/chat",
			"pageid": "",
			"pagetemplateid": "",
			"channelid": "",
			"nodeid": "0",
			"userid": "0",
			"username": "Guest",
			"musername": "Guest",
			"user_startofweek": "1",
			
			"user_lang_pickerdateformatoverride": "",
			"user_editorstate": "",
			"textDirLeft": "left",
			"textDirRight": "right",
			"textdirection": "ltr",
			"can_use_sitebuilder": "",
			"cookie_prefix": "3djobs_",
			"cookie_path": "/",
			"cookie_domain": "",
			"inlinemod_cookie_name": "inlinemod_nodes",
			
				"pagenum": "1",
			
			"languageid": "1",
			"threadmarking": "2",
			"lastvisit": "1610498564",
			"phrasedate": "",
			"optionsdate": "",
			
			"current_server_datetime": "1610498564",
			"simpleversion": "v=564",
			
			"showhv_post": "1",
			"nextcron": "1610476200",
			"securitytoken": "guest",
			"privacystatus": "1",
			"flash_message": "",
			"registerurl": "https://www.3djobs.com/register",
			"activationurl": "https://www.3djobs.com/activateuser",
			"helpurl": "https://www.3djobs.com/help",
			"contacturl": "https://www.3djobs.com/contact",
			"datenow": "01-12-2021"
		};
		
		

	</script>
<!-- BEGIN: head_include --><script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-4176449910220140",
          enable_page_level_ads: true
     });
</script><!-- END: head_include -->
</head>
<body class="l-desktop page vb-page view-mode logged-out" data-styleid="38" data-usergroupid="1" id="vb-page-body" itemscope="">
<script type="text/javascript">
	vBulletin.Responsive.Debounce.checkBrowserSize();
</script>
<div class="b-top-menu__background b-top-menu__background--sitebuilder js-top-menu-sitebuilder h-hide-on-small h-hide">
<div class="b-top-menu__container">
<ul class="b-top-menu b-top-menu--sitebuilder js-top-menu-sitebuilder--list js-shrink-event-parent">
<!-- BEGIN: top_menu_sitebuilder --><!-- END: top_menu_sitebuilder -->
</ul>
</div>
</div>
<nav class="b-top-menu__background js-top-menu-user">
<div class="b-top-menu__container">
<ul class="b-top-menu b-top-menu--user js-top-menu-user--list js-shrink-event-parent">
<!-- BEGIN: top_menu_user -->
<li class="b-top-menu__item b-top-menu__item--no-left-divider b-top-menu__item--no-right-divider username-container js-shrink-event-child">
<div class="b-comp-menu-dropdown b-comp-menu-dropdown--headerbar js-comp-menu-dropdown b-comp-menu-dropdown--open-on-hover js-login-menu">
<div class="b-comp-menu-dropdown__trigger js-comp-menu-dropdown__trigger b-comp-menu-dropdown__trigger--arrow b-comp-menu-dropdown__trigger--headerbar js-button" id="lnkLoginSignupMenu" tabindex="0">
				Login or Sign Up
			</div>
<ul class="b-comp-menu-dropdown__content js-comp-menu-dropdown__content b-comp-menu-dropdown__content--right">
<li class="b-comp-menu-dropdown__content-item js-comp-menu-dropdown__content-maintain-menu b-comp-menu-dropdown__content-item--no-hover">
<!-- BEGIN: login_main -->
<div class="js-login-form-main-container login-form-main-container">
<div class="js-error-box error h-margin-bottom-l h-hide"></div>
<div class="js-login-message-box login-message-box h-hide">
<div class="h-center-container"><div class="h-center">Logging in...</div></div>
</div>
<form action="" class="h-clearfix js-login-form-main " method="post">
<div class="login-fieldset table">
<div class="tr">
<span class="td">
<input class="js-login-username b-form-input__input b-form-input__input--full" name="username" placeholder="User/Email" type="text" value=""/>
</span>
</div>
<div class="tr">
<span class="td">
<input autocomplete="off" class="js-login-password b-form-input__input b-form-input__input--full" name="password" placeholder="Password" type="password"/>
</span>
</div>
</div>
<div class="h-warning h-text-size--11">By logging into your account, you agree to our <a href="privacy" target="_blank">Privacy Policy</a>, personal data processing and storage practices as described therein.</div>
<input name="privacyconsent" type="hidden" value="1"/>
<div class="secondary-controls h-left h-clear-left h-margin-top-s">
<label><input name="rememberme" type="checkbox"/>Remember me</label>
</div>
<div class="primary-controls h-right h-clear-right h-margin-top-s">
<button class="js-login-button b-button b-button--primary" type="button">Log in</button>
</div>
<div class="secondary-controls h-left h-clear-left">
<a class="b-link lost-password-link" href="https://www.3djobs.com/lostpw">Forgot password or user name?</a>
</div>
<div class="primary-controls h-right h-clear-right">
		
		or <a href="https://www.3djobs.com/register" id="idLoginSignup" target="_top">Sign Up</a>
</div>
</form>
</div><!-- END: login_main -->
</li>
<li class="b-comp-menu-dropdown__content-item js-comp-menu-dropdown__content-maintain-menu b-comp-menu-dropdown__content-item--no-hover js-external-login-providers h-clearfix h-hide-imp" id="externalLoginProviders">
<span class="h-left h-margin-left-s h-margin-top-s">Log in with</span>
<!-- BEGIN: twitterlogin_loginbutton -->
<!-- END: twitterlogin_loginbutton -->
<!-- BEGIN: googlelogin_loginbutton -->
<!-- END: googlelogin_loginbutton -->
</li>
</ul>
</div>
</li>
<!-- END: top_menu_user -->
</ul>
</div>
</nav>
<div class="noselect h-clearfix h-hide-on-small" id="sitebuilder-wrapper">
</div>
<div class="main-navbar-bottom-line"></div>
<div id="outer-wrapper">
<div id="wrapper">
<div class="header-edit-box axdnum_1 h-clearfix" id="header-axd">
<div class="axd-container axd-container_header">
<div class="admin-only">
</div>
<div class="axd axd_header">
<!-- BEGIN: ad_header --><!-- END: ad_header -->
</div>
</div>
<div class="axd-container axd-container_header2">
<div class="admin-only">
</div>
<div class="axd axd_header2">
<!-- BEGIN: ad_header2 --><!-- END: ad_header2 -->
</div>
</div>
</div>
<header class="b-top-background__header-mainnav-subnav">
<div class="b-top-background__header-mainnav">
<div class="noselect" id="header">
<div class="header-cell">
<div class="site-logo header-edit-box">
<a alt="3DjOBS" class="logo-3djobs" href="https://www.3djobs.com/" title="3DjOBS">3DjOBS</a>
</div>
</div>
<div class="toolbar">
<ul class="h-right">
<li class="search-container">
<form action="https://www.3djobs.com/search" class="h-left" id="searchForm" method="GET">
<div class="h-left">
<div class="search-box h-clearfix">
<div class="search-term-container">
<input autocomplete="off" class="search-term b-form-input__input b-form-input__input--shadow" id="q" name="q" placeholder="Search" type="text"/>
<div class="search-menu-container h-clearfix">
<div class="vertical-divider-left"></div>
<div class="b-comp-menu-dropdown js-comp-menu-dropdown b-comp-menu-dropdown--open-on-hover b-comp-menu-dropdown--header-search">
<div class="b-comp-menu-dropdown__trigger js-comp-menu-dropdown__trigger b-comp-menu-dropdown__trigger--arrow js-button" tabindex="0"></div>
<!-- BEGIN: search_popup --><ul class="b-comp-menu-dropdown__content js-comp-menu-dropdown__content">
<li class="b-comp-menu-dropdown__content-item b-comp-menu-dropdown__content-item--first b-comp-menu-dropdown__content-item--no-hover js-comp-menu-dropdown__content-maintain-menu">
<div class="searchPopupBody">
<label><input class="searchFields_title_only" name="searchFields[title_only]" type="checkbox" value="1"/>Search in titles only</label>
<div class="b-button-group h-margin-top-xs">
<button class="b-button b-button--primary" id="btnSearch-popup" type="submit">Search</button>
</div>
</div>
<input name="searchJSON" type="hidden" value=""/>
</li>
<li class="b-comp-menu-dropdown__content-item js-comp-menu-dropdown__content-maintain-menu">
<a class="adv-search-btn" href="#" id="btnAdvSearch">Advanced Search</a>
</li>
</ul><!-- END: search_popup -->
</div>
</div>
</div>
<button class="search-btn" id="btnSearch" title="Search" type="submit"><span class="b-icon b-icon__search"></span></button>
</div>
</div>
</form>
</li>
</ul>
</div>
</div>
<nav class="h-clearfix noselect" id="channel-tabbar">
<ul class="channel-tabbar-list h-left b-comp-menu-horizontal js-comp-menu-horizontal js-comp-menu--dropdown-on-small b-comp-menu-dropdown--inactive js-comp-menu-dropdown__content--main-menu h-hide-on-small js-shrink-event-parent">
<li class=" section-item js-shrink-event-child">
<a class="h-left navbar_home" href="">3DjOBS</a>
<span class="channel-tabbar-divider"></span>
<span class="mobile dropdown-icon"><span class="icon h-right"></span></span>
</li>
<li class=" section-item js-shrink-event-child">
<a class="h-left navbar_articles" href="articles">Articles</a>
<span class="channel-tabbar-divider"></span>
<span class="mobile dropdown-icon"><span class="icon h-right"></span></span>
</li>
<li class=" section-item js-shrink-event-child">
<a class="h-left navbar_forums" href="forums">Forums</a>
<span class="channel-tabbar-divider"></span>
<span class="mobile dropdown-icon"><span class="icon h-right"></span></span>
</li>
<li class="current section-item js-shrink-event-child">
<a class="h-left navbar_jobs" href="https://www.3djobs.com/jobs">Jobs</a>
<span class="channel-tabbar-divider"></span>
<span class="mobile dropdown-icon"><span class="icon h-right"></span></span>
</li>
<li class=" section-item js-shrink-event-child">
<a class="h-left navbar_marketplace" href="https://www.3djobs.com/buy-and-sell">Marketplace</a>
<span class="channel-tabbar-divider"></span>
<span class="mobile dropdown-icon"><span class="icon h-right"></span></span>
</li>
<li class=" section-item js-shrink-event-child">
<a class="h-left navbar_news" href="news">News</a>
<span class="channel-tabbar-divider"></span>
<span class="mobile dropdown-icon"><span class="icon h-right"></span></span>
</li>
<li class=" section-item js-shrink-event-child">
<a class="h-left navbar_reviews" href="https://www.3djobs.com/reviews">Reviews</a>
<span class="mobile dropdown-icon"><span class="icon h-right"></span></span>
</li>
</ul>
</nav>
</div>
<nav class="h-clearfix h-hide h-hide-on-small h-block js-channel-subtabbar" id="channel-subtabbar">
<ul class="h-left channel-subtabbar-list js-channel-subtabbar-list js-shrink-event-parent">
</ul>
</nav>
</header>
<nav class="breadcrumbs-wrapper">
<div id="breadcrumbs"></div>
</nav>
<!-- BEGIN: notices -->
<!-- END: notices -->
<main id="content">
<div class="canvas-layout-container js-canvas-layout-container">
<!-- END: header -->
<div class="canvas-layout" data-layout-id="" id="canvas-layout-full">
<div class="canvas-widget-list section-0">
		
			
				
				Invalid Page URL. If this is an error and the page should exist, please contact the system administrator and tell them how you got this message.
			
		
		
	</div>
</div>
<!-- BEGIN: footer --></div>
<div class="h-clear"></div>
</main>
<footer id="footer">
<nav class="h-clearfix js-shrink-event-parent" id="footer-tabbar">
<ul class="h-left js-footer-chooser-list h-margin-left-xxl js-shrink-event-child">
</ul>
<ul class="nav-list h-right js-footer-nav-list h-margin-right-xxl b-comp-menu-horizontal js-comp-menu-horizontal js-comp-menu--dropdown-on-xsmall b-comp-menu-dropdown--inactive js-shrink-event-child" data-dropdown-menu-classes="h-margin-vert-l b-comp-menu-dropdown--direction-up" data-dropdown-trigger-classes="b-comp-menu-dropdown__trigger--arrow" data-dropdown-trigger-phrase="go_to_ellipsis">
<li class="">
<a href="https://www.3djobs.com/help" rel="nofollow">Help</a>
</li>
<li class="">
<a href="https://www.3djobs.com/contact-us" rel="nofollow">Contact Us</a>
</li>
<li class="">
<a href="https://www.3djobs.com/privacy">Privacy</a>
</li>
<li class="">
<a href="https://www.3djobs.com/about">About</a>
</li>
<li><a class="js-footer-go-to-top" href="#">Go to top</a></li>
</ul>
</nav>
</footer>
</div>
<div id="footer-copyright">
<div id="footer-user-copyright">3DjOBS</div>
<div id="footer-vb-copyright">Powered by <a href="https://www.vbulletin.com" id="vbulletinlink">vBulletin®</a> Version 5.6.4 <br/>Copyright © 2021 MH Sub I, LLC dba vBulletin. All rights reserved. </div>
<div id="footer-current-datetime">All times are GMT-5. This page was generated at 07:42 PM.</div>
</div>
</div>
<div class="js-loading-indicator b-ajax-loading-indicator h-hide" id="loading-indicator">
<span>Working...</span>
</div>
<div class="js-flash-message b-ajax-loading-indicator b-ajax-loading-indicator--medium h-hide">
<span class="js-flash-message-content"></span>
</div>
<div class="sb-dialog" id="confirm-dialog">
<div class="dialog-content h-clearfix">
<div class="icon h-left"></div>
<div class="message"></div>
</div>
<div class="b-button-group">
<button class="b-button b-button--primary js-button" id="btnConfirmDialogYes" type="button">Yes</button>
<button class="b-button b-button--secondary js-button" id="btnConfirmDialogNo" type="button">No</button>
</div>
</div>
<div class="sb-dialog" id="alert-dialog">
<div class="dialog-content h-clearfix">
<div class="icon h-left"></div>
<div class="message"></div>
</div>
<div class="b-button-group">
<button class="b-button b-button--primary js-button" id="btnAlertDialogOK" type="button">OK</button>
</div>
</div>
<div class="sb-dialog" id="prompt-dialog">
<div class="dialog-content table h-clearfix">
<div class="message td"></div>
<div class="input-box-container td">
<input class="input-box textbox" type="text"/>
<textarea class="input-box textbox h-hide" rows="3"></textarea>
</div>
</div>
<div class="js-prompt-error h-hide"></div>
<div class="b-button-group">
<button class="b-button b-button--primary js-button" id="btnPromptDialogOK" type="button">OK</button>
<button class="b-button b-button--secondary js-button" id="btnPromptDialogCancel" type="button">Cancel</button>
</div>
</div>
<div class="sb-dialog slideshow" id="slideshow-dialog">
<div class="b-icon b-icon__x-square--gray close-btn">X</div>
<div class="slideshow-wrapper"></div>
<div class="caption"></div>
<div class="thumbnails-wrapper">
<div class="thumbnails"></div>
</div>
</div>
<div class="unsubscribe-overlay-container"></div>
<div data-facebook-language="en_US" id="fb-root"></div>
<script src="js/jquery/jquery-3.5.1.min.js" type="text/javascript"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-1786220-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-1786220-3');
</script>
<script type="text/javascript">window.jQuery || document.write('<script type="text/javascript" src="js/jquery/jquery-3.5.1.min.js"><\/script>');</script>
<script src="js/footer-rollup-564.js" type="text/javascript"></script>
<script type="text/javascript">
	
		window.vBulletin = window.vBulletin || {};
	
		vBulletin.version = '5.6.4';
</script>
<!-- BEGIN: privacy_consent_banner --><div class="b-modal-banner__banner js-privacy-consent-banner__banner h-hide-imp b-modal-banner__banner--center">
<p class="b-modal-banner__paragraph">We process personal data about users of our site, through the use of cookies and other technologies, to deliver our services, personalize advertising, and to analyze site activity. We may share certain information about our users with our advertising and analytics partners. For additional details, refer to our <a class="b-modal-banner__clickable" href="privacy" target="_blank">Privacy Policy</a>.</p>
<p class="b-modal-banner__paragraph">By clicking "<b>I AGREE</b>" below, you agree to our <a class="b-modal-banner__clickable" href="privacy" target="_blank">Privacy Policy</a> and our personal data processing and cookie practices as described therein. You also acknowledge that this forum may be hosted outside your country and you consent to the collection, storage, and processing of your data in the country where this forum is hosted.</p>
<p class="b-modal-banner__paragraph">
<a class="b-button b-button--primary b-modal-banner__clickable js-privacy-consent-banner__button" href="#">I Agree</a>
</p>
</div>
<div class="b-modal-banner__overlay js-privacy-consent-banner__overlay h-hide-imp"></div><!-- END: privacy_consent_banner -->
<script src="js/login.js?v=564" type="text/javascript"></script>
<script src="js/privacy-consent-banner.js?v=564" type="text/javascript"></script>
</body>
</html><!-- END: footer --><!-- END: error_page -->