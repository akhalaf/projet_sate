<!DOCTYPE html>
<html lang="en"> <head> <link href="/App_Themes/Aramco/img/favicons/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/> <link href="/App_Themes/Aramco/img/favicons/apple-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/> <link href="/App_Themes/Aramco/img/favicons/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/> <link href="/App_Themes/Aramco/img/favicons/apple-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/> <link href="/App_Themes/Aramco/img/favicons/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/> <link href="/App_Themes/Aramco/img/favicons/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/> <link href="/App_Themes/Aramco/img/favicons/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/> <link href="/App_Themes/Aramco/img/favicons/apple-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/> <link href="/App_Themes/Aramco/img/favicons/apple-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/> <link href="/App_Themes/Aramco/img/favicons/android-icon-192x192.png" rel="icon" sizes="192x192" type="image/png"/> <link href="/App_Themes/Aramco/img/favicons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/> <link href="/App_Themes/Aramco/img/favicons/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/> <link href="/App_Themes/Aramco/img/favicons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/> <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/> <meta charset="utf-8"/> <title>
	Home
</title> <link href="/ResourcePackages/Bootstrap/assets/dist/css/main.min.css" rel="stylesheet" type="text/css"/> <link crossorigin="anonymous" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" rel="stylesheet"/> <link href="/App_Themes/Aramco/css/flowup.css" rel="stylesheet"/> <link href="/App_Themes/Aramco/css/slick.css" rel="stylesheet"/> <link href="/App_Themes/Aramco/css/slidetheme.css" rel="stylesheet"/> <link href="/App_Themes/Aramco/css/unite-gallery.css" rel="stylesheet"/> <link href="/App_Themes/Aramco/css/slidetheme.css" rel="stylesheet"/> <link href="/App_Themes/Aramco/css/layerslider.css" rel="stylesheet"/> <link href="/App_Themes/Aramco/skins/fullwidth/skin.css" rel="stylesheet"/> <link href="/App_Themes/Aramco/css/styles-mapbox.css" rel="stylesheet"/> <link href="https://api.tiles.mapbox.com/mapbox-gl-js/v0.31.0/mapbox-gl.css" rel="stylesheet"/> <link href="/App_Themes/Aramco/css/styles.css" rel="stylesheet"/> <link href="/App_Themes/AramcoJobs/css/job-styles.css" rel="stylesheet"/> <script type="text/javascript">var sf_appPath='/';</script><meta content="Sitefinity 12.2.7225.0 PU" name="Generator"/><link href="https://www.aramco.jobs" rel="canonical"/><meta content="width=device-width, initial-scale=1.0" name="viewport"/><script type="text/javascript">
	(function() {var _rdDeviceWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;var _rdDeviceHeight = (window.innerHeight > 0) ? window.innerHeight : screen.height;var _rdOrientation = (window.width > window.height) ? 'landscape' : 'portrait';})();
</script></head> <body> <script src="/ScriptResource.axd?d=4bMApOb58R6igmkUEZ0eXsrBXEd_VZiKObb1CzrzZwo3jThWRGw7b6tYW8fMTGeBEi4-KOsy4KO21tC0218XDn5OmbrFifuL66b7IXPIRhBpXQAm8Bn1No0JVbzv5hF387GdzTM7v3ItfcGWEYM203VEd0VDZM3xOWKNxTOZlZ7zrz7eIvHMiVHDLeQk5NwH0&amp;t=ffffffffdfd9cd05" type="text/javascript"></script><script src="/ScriptResource.axd?d=EydukmxBmDstn7gSYzQESOHBbVDXUKUfdNBjat30rLYdofqCkQsy7AXMDiLeubY6UIGW4lOWj8G-f9KKNFn4h3FsSRKQbGdfJOaE4lXYNAKtPMKBOAb8hgf-YGmG_Cki6AmoQlJy3Ro7P3fHTxciWoY4gqSvxIx7GIb5n86CoRGQ6VLnOPp8CRdv6EDLWmyw0&amp;t=ffffffffdfd9cd05" type="text/javascript"></script><script src="/ScriptResource.axd?d=VKaJmfFWDpQxp1_HxsR1qKJxhiwA2_VM0FlVNWAVc6_eeHbXytLOY9n7o_jIh_-QLfPIGWaA403eCoN19fd_B58VQTnG_B2n0wZjj_EqERagsQVZ7NXHqgihiqgLKWaLh7iAhN-J4-0vr1LbPJOptTGcQhDxAAIfwwqhS2wSYQguph2O8fmXi1NRW3s3LZAnp94NRRc6oyl32heXSsY5ZA2&amp;t=ffffffffdfd9cd05" type="text/javascript"></script><script src="/Frontend-Assembly/Telerik.Sitefinity.Frontend/Mvc/Scripts/Bootstrap/js/bootstrap.min.js?package=Bootstrap&amp;v=MTIuMi43MjI1LjA%3d" type="text/javascript"></script> <script src="https://api.tiles.mapbox.com/mapbox-gl-js/v0.31.0/mapbox-gl.js"></script> <script src="/App_Themes/Aramco/js/greensock.js"></script> <script src="/App_Themes/Aramco/js/layerslider.kreaturamedia.jquery.js"></script> <script src="/App_Themes/Aramco/js/layerslider.transitions.js"></script> <div class="sfPublicWrapper" id="PublicWrapper">
<header>
<div class="container">
<div class="aux-nav-container">
<div class="sf_colsIn aux-nav search" data-placeholder-label="Search Placeholder" data-sf-element="Search Placeholder" id="Contentplaceholder1_T9A97D582001_Col00">
<div class="form-inline">
<form class="searchbox">
<input class="searchbox-input" id="9dc8921f-9457-43a3-96b0-66dd26ec58e4" onkeyup="buttonUp();" placeholder="Search" title="Search input" type="search" value=""/>
<input class="searchbox-submit" type="submit" value="GO"/>
<button class="searchbox-icon search-btn" id="4b80eebd-5cb5-479e-821a-17a44722a86b" type="button"><img src="/App_Themes/Aramco/img/search.png"/></button>
<span class="searchbox-icon search-trigger"><img src="/App_Themes/Aramco/img/search.png"/></span>
<span class="search-close"></span>
</form>
<input data-sf-role="resultsUrl" type="hidden" value="/search-results"/>
<input data-sf-role="indexCatalogue" type="hidden" value="jobs-search"/>
<input data-sf-role="wordsMode" type="hidden" value="AllWords"/>
<input data-sf-role="disableSuggestions" type="hidden" value="false"/>
<input data-sf-role="minSuggestionLength" type="hidden" value="3"/>
<input data-sf-role="suggestionFields" type="hidden" value="Title,Content"/>
<input data-sf-role="language" type="hidden" value=""/>
<input data-sf-role="suggestionsRoute" type="hidden" value="/restapi/search/suggestions"/>
<input data-sf-role="searchTextBoxId" type="hidden" value="#9dc8921f-9457-43a3-96b0-66dd26ec58e4"/>
<input data-sf-role="searchButtonId" type="hidden" value="#4b80eebd-5cb5-479e-821a-17a44722a86b"/>
</div>
</div>
<div class="sf_colsIn aux-nav site-selector" data-placeholder-label="Site Selector Placeholder" data-sf-element="Site Selector Placeholder" id="Contentplaceholder1_T9A97D582001_Col01">
<ul id="site-selector">
<li>
<a href="#" id="selector-dropdown">Other sites</a>
<ul class="site-selector-sub">
<li><a href="/other-sites/aramco-china" target="_blank">Aramco China</a></li>
<li><a href="/other-sites/aramco-japan" target="_blank">Aramco Japan</a></li>
<li><a href="/other-sites/aramco-korea" target="_blank">Aramco Korea</a></li>
<li><a href="/other-sites/aramco-singapore" target="_blank">Aramco Singapore</a></li>
<li><a href="/other-sites/aramco-services-company-(usa)" target="_blank">Aramco Americas</a></li>
<li><a href="/other-sites/aramco-overseas-company-(europe)" target="_blank">Aramco Europe</a></li>
</ul>
</li>
</ul>
<script>
    //$(function(){
    //  // bind change event to select
    //    $('#site-selector').click {
    //      var url = $(this).val(); // get selected value
    //      //if (url) { // require a URL
    //      //    window.open(url); // redirect
    //      //    $(this).find('option:first').attr('selected', 'selected');
    //      //}
    //        //return false;
    //      e.preventDefault();
    //      $.ajax({
    //          url: url,
    //          async: false,
    //          dataType: "json",
    //          success: function () {
    //              window.open(url);
    //          }
    //      });
    //  });
    //});
    $('#selector-dropdown').click(function(e) {
        e.preventDefault();
        $('.site-selector-sub').toggle();
        
        return false;
    });
    $('.site-selector-sub li a').click(function () {
        $('.site-selector-sub').toggle();

    });
</script>
</div>
<div class="sf_colsIn aux-nav aux-contact" data-placeholder-label="Contact Placeholder" data-sf-element="Contact Placeholder" id="Contentplaceholder1_T9A97D582001_Col02">
<div>
<div><a href="/contact-us">Contact</a></div>
</div>
</div>
<div class="sf_colsIn aux-nav aux-social" data-placeholder-label="Social Navigation" data-sf-element="Social Navigation" id="Contentplaceholder1_T9A97D582001_Col03">
<div>
<div><ul><li><a href="https://www.linkedin.com/company/aramco" target="_blank"><img alt="" src="/App_Themes/Aramco/img/btn-linkedin-nav.png"/></a></li><li><a href="https://www.youtube.com/user/AramcoVideo" target="_blank"><img alt="" src="/App_Themes/Aramco/img/btn-youtube-nav.png"/></a></li><li><a href="https://www.facebook.com/aramco" target="_blank"><img alt="" src="/App_Themes/Aramco/img/btn-facebook-nav.png"/></a></li></ul></div>
</div>
</div>
<div class="sf_colsIn aux-nav-right aux-logo" data-placeholder-label="Logo-Desktop Placeholder" data-sf-element="Logo-Desktop Placeholder" id="Contentplaceholder1_T9A97D582001_Col04">
<a href="/home">
<img alt="Aramco Logo" src="/images/librariesprovider2/default-album/logo.png?sfvrsn=74934ab7_4" title="logo"/>
</a>
</div>
<div style="clear:both;"></div>
</div>
</div>
<div class="main-nav">
<div class="sf_colsIn container mobile-container" data-placeholder-label="Navigation Placeholder" data-sf-element="Navigation Placeholder" id="Contentplaceholder1_T9A97D582001_Col05">
<nav class="navbar" role="navigation">
<div class="navbar-header">
<button aria-expanded="false" class="navbar-toggle collapsed" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<div class="mobile-nav-right aux-logo">
<a href="\"><img src="/images/librariesprovider2/default-album/logo.png?Status=Temp&amp;sfvrsn=74934ab7_2"/></a>
</div>
<div style="clear:both;"></div>
</div>
<div aria-expanded="false" class="navbar-collapse collapse" id="bs-example-navbar-collapse-1" style="height: 1px;">
<ul class="nav navbar-nav">
<li class="dropdown active">
<a class="" href="\">
<span class="home-icon"></span>
<span class="home-text">Home</span>
</a>
</li>
<li class="primaryNavigationItem back js-back">Back</li>
<li class="primaryNavigationItem">
<a data-brand="/about-saudi-aramco" data-top-level="/about-saudi-aramco" href="/about-saudi-aramco" target="_self">About Saudi Aramco</a>
<div class="secondaryNavigation" style="display: none;">
<div class="bss-container">
<ul class="megaNavigation cols threeCols patternListWithImage">
<li class="col col1">
<a class="megaNavigationHeaderLink" data-subpage="/about-saudi-aramco/our-story" href="/about-saudi-aramco/our-story">Our story
                    

            </a>
<div style="clear:both;"></div>
</li><!-- /.nav-dropdown-col -->
<li class="col col1">
<a class="megaNavigationHeaderLink" data-subpage="/about-saudi-aramco/our-people" href="/about-saudi-aramco/our-people">Our people
                    

            </a>
<ul class="megaNavigationList">
<li class="megaNavigationItem">
<a class=" megaNavigationItemLink" href="/about-saudi-aramco/our-people/julio-reliability-engineer">Julio, Reliability Engineer</a>
</li>
<li class="megaNavigationItem">
<a class=" megaNavigationItemLink" href="/about-saudi-aramco/our-people/john-engineering-specialist">John, Engineering Specialist</a>
</li>
<li class="megaNavigationItem">
<a class=" megaNavigationItemLink" href="/about-saudi-aramco/our-people/jim-head-of-helicopter-pilot-training-and-standards">Jim, Head of Helicopter Pilot Training and Standards</a>
</li>
<li class="megaNavigationItem">
<a class=" megaNavigationItemLink" href="/about-saudi-aramco/our-people/mel-crisis-communications-coordinator">Mel, Crisis Communications Coordinator</a>
</li>
</ul>
<div style="clear:both;"></div>
</li><!-- /.nav-dropdown-col -->
<li class="col col1">
<a class="megaNavigationHeaderLink" data-subpage="/about-saudi-aramco/mega-projects" href="/about-saudi-aramco/mega-projects">Projects &amp; Technology
                    

            </a>
<ul class="megaNavigationList">
<li class="megaNavigationItem">
<a class=" megaNavigationItemLink" href="/about-saudi-aramco/mega-projects/khurais-field-development">Fadhili Gas Plant</a>
</li>
<li class="megaNavigationItem">
<a class=" megaNavigationItemLink" href="/about-saudi-aramco/mega-projects/shaybah-field-expansion">4 IR Center</a>
</li>
<li class="megaNavigationItem">
<a class=" megaNavigationItemLink" href="/about-saudi-aramco/mega-projects/saudi-aramco-total-refining-petrochemical-company-(satorp)">Smart Helmets supporting operations during COVID-19</a>
</li>
</ul>
<div style="clear:both;"></div>
</li><!-- /.nav-dropdown-col -->
</ul><!-- /.nav-dropdown-container -->
<div style="clear:both;"></div>
</div>
</div><!-- /.nav-dropdown -->
</li>
<li class="primaryNavigationItem">
<a data-brand="/living-overseas" data-top-level="/living-overseas" href="/living-overseas" target="_self">Living Overseas</a>
<div class="secondaryNavigation" style="display: none;">
<div class="bss-container">
<ul class="megaNavigation cols threeCols patternListWithImage">
<li class="col col1">
<a class="megaNavigationHeaderLink" data-subpage="/living-overseas/about-saudi-arabia" href="/living-overseas/about-saudi-arabia">About Saudi Arabia
                    

            </a>
<div style="clear:both;"></div>
</li><!-- /.nav-dropdown-col -->
<li class="col col1">
<a class="megaNavigationHeaderLink" data-subpage="/living-overseas/housing" href="/living-overseas/housing">Housing
                    

            </a>
<div style="clear:both;"></div>
</li><!-- /.nav-dropdown-col -->
<li class="col col1">
<a class="megaNavigationHeaderLink" data-subpage="/living-overseas/schools" href="/living-overseas/schools">Schools
                    

            </a>
<div style="clear:both;"></div>
</li><!-- /.nav-dropdown-col -->
<li class="col col1">
<a class="megaNavigationHeaderLink" data-subpage="/living-overseas/travel" href="/living-overseas/travel">Travel
                    

            </a>
<div style="clear:both;"></div>
</li><!-- /.nav-dropdown-col -->
</ul><!-- /.nav-dropdown-container -->
<div style="clear:both;"></div>
</div>
</div><!-- /.nav-dropdown -->
</li>
<li><a data-top-level="/comp-and-benefits" href="/comp-and-benefits">Rewards &amp; Benefits</a></li>
<li class="primaryNavigationItem">
<a data-brand="/recruitment-process" data-top-level="/recruitment-process" href="/recruitment-process" target="_self">Recruitment Process</a>
<div class="secondaryNavigation" style="display: none;">
<div class="bss-container">
<ul class="megaNavigation cols threeCols patternListWithImage">
<li class="col col1">
<a class="megaNavigationHeaderLink" data-subpage="/recruitment-process/overview" href="/recruitment-process/overview">Overview
                    

            </a>
<div style="clear:both;"></div>
</li><!-- /.nav-dropdown-col -->
<li class="col col1">
<a class="megaNavigationHeaderLink" data-subpage="/recruitment-process/career-development" href="/recruitment-process/career-development">Career development
                    

            </a>
<div style="clear:both;"></div>
</li><!-- /.nav-dropdown-col -->
<li class="col col1">
<a class="megaNavigationHeaderLink" data-subpage="/recruitment-process/recruitment-disclaimer" href="/recruitment-process/recruitment-disclaimer">Recruitment Disclaimer 
                    

            </a>
<div style="clear:both;"></div>
</li><!-- /.nav-dropdown-col -->
</ul><!-- /.nav-dropdown-container -->
<div style="clear:both;"></div>
</div>
</div><!-- /.nav-dropdown -->
</li>
<li class="primaryNavigationItem">
<a data-brand="/connect" data-top-level="/connect" href="/connect" target="_self">Connect</a>
<div class="secondaryNavigation" style="display: none;">
<div class="bss-container">
<ul class="megaNavigation cols threeCols patternListWithImage">
<li class="col col1">
<a class="megaNavigationHeaderLink" data-subpage="/connect/events" href="/connect/events">Events
                    

            </a>
<div style="clear:both;"></div>
</li><!-- /.nav-dropdown-col -->
<li class="col col1">
<a class="megaNavigationHeaderLink" data-subpage="/connect/publications" href="/connect/publications">Publications
                    

            </a>
<div style="clear:both;"></div>
</li><!-- /.nav-dropdown-col -->
</ul><!-- /.nav-dropdown-container -->
<div style="clear:both;"></div>
</div>
</div><!-- /.nav-dropdown -->
</li>
<li class="primaryNavigationItem mobile-nav">
<a data-brand="/other-sites" data-top-level="/other-sites" href="#" target="_self">Other Sites</a>
<div class="secondaryNavigation" style="display: none;">
<div class="bss-container">
<ul class="megaNavigation cols threeCols patternListWithImage othersite">
<li class="col col1">
<a class="megaNavigationHeaderLink" href="http://china.aramco.com/en/home.html" target="_blank">
                                            Aramco China
                                        </a>
<div style="clear:both;"></div>
</li><!-- /.nav-dropdown-col -->
<li class="col col1">
<a class="megaNavigationHeaderLink" href="http://japan.aramco.com/en/home.html" target="_blank">
                                            Aramco Japan
                                        </a>
<div style="clear:both;"></div>
</li><!-- /.nav-dropdown-col -->
<li class="col col1">
<a class="megaNavigationHeaderLink" href="http://korea.aramco.com/en/home.html" target="_blank">
                                            Aramco Korea
                                        </a>
<div style="clear:both;"></div>
</li><!-- /.nav-dropdown-col -->
<li class="col col1">
<a class="megaNavigationHeaderLink" href="http://singapore.aramco.com/en/home.html" target="_blank">
                                            Aramco Singapore
                                        </a>
<div style="clear:both;"></div>
</li>
<li class="col col1">
<a class="megaNavigationHeaderLink" href="http://www.aramcoservices.com/Home.aspx" target="_blank">
                                            Aramco Services Company (USA)
                                        </a>
<div style="clear:both;"></div>
</li>
<li class="col col1">
<a class="megaNavigationHeaderLink" href="https://aramcooverseas.com/" target="_blank">
                                            Aramco Overseas Company (Europe)
                                        </a>
<div style="clear:both;"></div>
</li>
</ul>
<div style="clear:both;"></div>
</div>
</div><!-- /.nav-dropdown -->
</li>
</ul>
<div class="social-mobile mobile-nav">
<ul>
<div class="social-mobile-title">Follow us</div>
<li><a href="https://twitter.com/Saudi_Aramco" target="_blank"><img alt="" src="/App_Themes/Aramco/img/btn-twitter-article.png"/></a></li>
<li><a href="https://www.linkedin.com/company/saudi-aramco" target="_blank"><img alt="" src="/App_Themes/Aramco/img/btn-linkedin-article.png"/></a></li>
<li><a href="https://www.youtube.com/user/AramcoVideo" target="_blank"><img alt="" src="/App_Themes/Aramco/img/btn-youtube-nav.png"/></a></li>
<li><a href="https://www.facebook.com/Saudi-Aramco-102895883124925" target="_blank"><img alt="" src="/App_Themes/Aramco/img/btn-facebook-article.png"/></a></li>
</ul>
</div>
</div>
</nav><!-- /.nav -->
<div class="nav-overlay"></div><!-- /.nav-overlay -->
</div>
</div>
</header><div class="slider-wrapper">
<div class="fullsize" id="layerslider" style="width:1200px;height:100vh;">
<div class="ls-slide" data-ls="bgsize:cover;bgposition:50% 50%;overflow:true;kenburnsscale:1.2;parallaxevent:scroll;parallaxdistance:15;duration:8000;" style="background-image:url('https://www.aramco.jobs/images/librariesprovider2/slider-and-hero-images/slider-home2.png?sfvrsn=43d24bb7_6')">
<img alt="landing-intro-bg" class="ls-bg" height="1371" src="https://www.aramco.jobs/images/librariesprovider2/slider-and-hero-images/slider-home2.png?sfvrsn=43d24bb7_6" style="background-image:url('https://www.aramco.jobs/images/librariesprovider2/slider-and-hero-images/slider-home2.png?sfvrsn=43d24bb7_6')" width="1920"/>
<div class="container">
<div class="slide-content">
<h1 class="ls-l" data-ls="offsetyin:60;durationin:1500;delayin:900;easingin:easeOutExpo;offsetyout:80;durationout:1500;" style="color: #fff;font-size: 32px;line-height: 35px;text-transform: capitalize;height:auto;margin:0 0 33px;font-weight: 300;margin-bottom: 33px;white-space: normal;">Real people, real stories</h1>
<p class="ls-l" data-ls="offsetyin:60;durationin:1500;delayin:1100;easingin:easeOutExpo;offsetyout:80;durationout:1500;" style="color: #fff;font-size: 18px;margin-bottom: 35px;line-height: 28px;height:initial !important;margin-bottom: 35px;white-space: normal; width:50%;">
                            We provide opportunities professional and personal experiences that last a lifetime. Learn what our employees think about life and work in Saudi Arabia.
                        </p>
<div class="btn-container">
<div class="ls-l" data-ls="offsetyin:60;durationin:1200;delayin:1500;easingin:easeOutExpo;offsetyout:80;durationout:1500;" style="line-height: 28px;font-size: 18px;margin-bottom: 32px; height:auto;width:50%;">
<a class="btn-blue" href="/about-saudi-aramco/our-people">Learn more</a> </div>
</div>
</div>
</div>
</div>
<div class="ls-slide" data-ls="bgsize:cover;bgposition:50% 50%;overflow:true;kenburnsscale:1.2;parallaxevent:scroll;parallaxdistance:15;duration:8000;" style="background-image:url('https://www.aramco.jobs/images/librariesprovider2/slider-and-hero-images/slider-home3.png?sfvrsn=bbd24bb7_6')">
<img alt="landing-intro-bg" class="ls-bg" height="1371" src="https://www.aramco.jobs/images/librariesprovider2/slider-and-hero-images/slider-home3.png?sfvrsn=bbd24bb7_6" style="background-image:url('https://www.aramco.jobs/images/librariesprovider2/slider-and-hero-images/slider-home3.png?sfvrsn=bbd24bb7_6')" width="1920"/>
<div class="container">
<div class="slide-content">
<h1 class="ls-l" data-ls="offsetyin:60;durationin:1500;delayin:900;easingin:easeOutExpo;offsetyout:80;durationout:1500;" style="color: #fff;font-size: 32px;line-height: 35px;text-transform: capitalize;height:auto;margin:0 0 33px;font-weight: 300;margin-bottom: 33px;white-space: normal;">Stay connected, stay informed</h1>
<p class="ls-l" data-ls="offsetyin:60;durationin:1500;delayin:1100;easingin:easeOutExpo;offsetyout:80;durationout:1500;" style="color: #fff;font-size: 18px;margin-bottom: 35px;line-height: 28px;height:initial !important;margin-bottom: 35px;white-space: normal; width:50%;">
                            Get the latest information on opportunities and advancements at Saudi Aramco and subscribe to our news updates now.
                        </p>
<div class="btn-container">
<div class="ls-l" data-ls="offsetyin:60;durationin:1200;delayin:1500;easingin:easeOutExpo;offsetyout:80;durationout:1500;" style="line-height: 28px;font-size: 18px;margin-bottom: 32px; height:auto;width:50%;">
<a class="btn-blue" href="/connect">Learn more</a> </div>
</div>
</div>
</div>
</div>
<div class="ls-slide" data-ls="bgsize:cover;bgposition:50% 50%;overflow:true;kenburnsscale:1.2;parallaxevent:scroll;parallaxdistance:15;duration:8000;" style="background-image:url('https://www.aramco.jobs/images/librariesprovider2/slider-and-hero-images/slider-home1.png?sfvrsn=60ba4ab7_6')">
<img alt="landing-intro-bg" class="ls-bg" height="1371" src="https://www.aramco.jobs/images/librariesprovider2/slider-and-hero-images/slider-home1.png?sfvrsn=60ba4ab7_6" style="background-image:url('https://www.aramco.jobs/images/librariesprovider2/slider-and-hero-images/slider-home1.png?sfvrsn=60ba4ab7_6')" width="1920"/>
<div class="container">
<div class="slide-content">
<h1 class="ls-l" data-ls="offsetyin:60;durationin:1500;delayin:900;easingin:easeOutExpo;offsetyout:80;durationout:1500;" style="color: #fff;font-size: 32px;line-height: 35px;text-transform: capitalize;height:auto;margin:0 0 33px;font-weight: 300;margin-bottom: 33px;white-space: normal;">Your energy, your opportunity</h1>
<p class="ls-l" data-ls="offsetyin:60;durationin:1500;delayin:1100;easingin:easeOutExpo;offsetyout:80;durationout:1500;" style="color: #fff;font-size: 18px;margin-bottom: 35px;line-height: 28px;height:initial !important;margin-bottom: 35px;white-space: normal; width:50%;">
                            You've been looking for a new way forward 
Now you have the freedom to explore it.
                        </p>
<div class="btn-container">
<div class="ls-l" data-ls="offsetyin:60;durationin:1200;delayin:1500;easingin:easeOutExpo;offsetyout:80;durationout:1500;" style="line-height: 28px;font-size: 18px;margin-bottom: 32px; height:auto;width:50%;">
<a class="btn-blue" href="https://www.aramco.jobs/about-saudi-aramco/our-people">Learn more</a> </div>
</div>
</div>
</div>
</div>
</div>
</div>
<script>
    $("#layerslider").layerSlider({
        type: 'fullsize',
        autoStart: true,
        height: 700,
        skinsPath: '/App_Themes/Aramco/skins/',
        maxRatio: 1,
        navStartStop: false,
        parallaxScrollReverse: true,
        controls: true,
        showCircleTimer: false
    });
</script>
<div class="nav-position"><div id="supage-subnav"></div></div>
<div class="bss-container">
<div class="job-btn-container">
<div class="sf_colsIn job-search-button home-search" data-placeholder-label="Find a Job Button Placeholder" data-sf-element="Find a Job Button Placeholder" id="Contentplaceholder1_C009_Col00">
<div>
<div><a class="btn-green" data-sf-ec-immutable="" href="https://apply.aramco.jobs/us/jobs?page=1" target="_blank">Find a Job</a></div>
</div>
</div>
</div>
</div><div class="quote-break flowUp">
<div class="quote-block">
<div class="sf_colsIn quote-container" data-placeholder-label="Quote Placeholder" data-sf-element="Quote Placeholder" id="Contentplaceholder1_C007_Col00">
<div>
<div>Your life’s best work awaits. Enjoy a rewarding career where personal and professional enrichment are top priorities – and openness, knowledge sharing, and innovation are at the core of everything we do.</div>
</div>
</div>
</div>
</div>
<div>
<div><a class="cta-block-link" href="https://aramco.avature.net/talent" target="_blank"><div class="cta-block" style="background-image:url('/images/librariesprovider2/content-images/home-page/talent_notext.png?Status=Temp&amp;sfvrsn=894947b7_0');"><span></span><h2>join our talent community</h2></div></a></div>
</div><div class="cta-container-jobs">
<div class="sf_colsIn cta-block flowUp" data-placeholder-label="CTA Block Placeholder" data-sf-element="CTA Block Placeholder" id="Contentplaceholder1_C012_Col00"> <a class="cta-block-link" href="https://apply.aramco.jobs/jobs?page=1">
<div class="cta-block" style="background-image: url('/images/librariesprovider2/content-images/home-page/apply-now-cta.png?sfvrsn=42bb4ab7_16')">
<span></span>
<h2>find a job</h2>
</div>
</a>
</div>
</div>
<div class="subpage-wrapper">
<div class="sf_colsIn bss-container" data-placeholder-label="Subpage Wrapper" data-sf-element="Subpage Wrapper" id="Contentplaceholder1_C020_Col00"><div class="featured-jobs">
<div class="sf_colsIn" data-placeholder-label="Title Placeholder" data-sf-element="Title Placeholder" id="Contentplaceholder1_C038_Col00">
<div>
<div><p><span style="font-size:x-large;">Featured Careers</span></p></div>
</div></div>
<div class="sf_colsIn" data-placeholder-label="Featured Events Widget" data-sf-element="Featured Events Widget" id="Contentplaceholder1_C038_Col01">
<div>
<div><ul style="display:flex;flex-wrap:wrap;list-style:none;padding-left:0;"><li style="text-align:center;list-style-position:inside;"> <a data-sf-ec-immutable="" href="https://apply.aramco.jobs/us/jobs?page=1&amp;categories=Expl.%20%26%20p.e.%20advanced%20research%20center%7CUnconventionals"><span style="font-size:x-large;">Exploration</span></a> 
 </li><li style="text-align:center;list-style-position:inside;"><a data-sf-ec-immutable="" href="https://apply.aramco.jobs/us/jobs?page=1&amp;categories=Materials%2Fcorrosion%2Finspection%20engineering%7CPetrochemical%7CRefinery%20engineering"><span style="font-size:x-large;">Downstream</span></a> <br/></li><li style="text-align:center;list-style-position:inside;"><a data-sf-ec-immutable="" href="https://saudiaramco.jibeapply.com/us/jobs?page=1&amp;Codes=ASC-W-AAPG&amp;categories=Public%20relations"><span style="font-size:x-large;">Public Relations</span></a></li><li style="text-align:center;list-style-position:inside;"><a data-sf-ec-immutable="" href="https://apply.aramco.jobs/us/jobs?page=1&amp;categories=Research%20%26%20lab%20sciences"><span style="font-size:x-large;">Research &amp; Development</span></a></li><li style="text-align:center;list-style-position:inside;"><a data-sf-ec-immutable="" href="https://apply.aramco.jobs/us/jobs?page=1&amp;categories=Finance%7CManagement%2Forganizational%20consultants"><span style="font-size:x-large;">Finance &amp; Marketing Strategy</span></a></li></ul></div>
</div>
</div>
<div class="sf_colsIn col-lg-offset-8 col-md-offset-4 col-sm-offset-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 featured-link" data-placeholder-label="Link Placeholder" data-sf-element="Link Placeholder" id="Contentplaceholder1_C038_Col02">
<div>
<div><a class="btn-blue" data-sf-ec-immutable="" href="https://saudiaramco.jibeapply.com/us/jobs">Find more opportunities</a></div>
</div>
</div>
<div style="clear: both"></div>
</div>
<div class="main-news-list featured-events">
<div class="sf_colsIn" data-placeholder-label="Title Placeholder" data-sf-element="Title Placeholder" id="Contentplaceholder1_C023_Col00">
<div>
<div><p>Recent Events<span class="rss-icon"></span></p></div>
</div></div>
<div class="sf_colsIn" data-placeholder-label="Featured Events Widget" data-sf-element="Featured Events Widget" id="Contentplaceholder1_C023_Col01">
<ul class="list-unstyled news-list">
<li class="news-item col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="news-list-img"><a href="https://seg.org/AM/2020/" target="_blank"><img src="https://www.aramco.jobs/images/librariesprovider2/events/seg-logo.png?sfvrsn=425f5cb7_9"/></a></div>
<h5>Oct 11                                             - 
16, 2020 / SEG, Virtual Event</h5>
<h4>
<a href="https://seg.org/AM/2020/" target="_blank">Society of Exploration Geophysicists Interna... </a>
</h4>
</li>
<li class="news-item col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="news-list-img"><a href="http://www.atce.org/" target="_blank"><img src="https://www.aramco.jobs/images/librariesprovider2/events/spe-logo.jpg?sfvrsn=465f5cb7_4"/></a></div>
<h5>Oct 27                                             - 
29, 2020 / SPE, Virtual Event</h5>
<h4>
<a href="http://www.atce.org/" target="_blank">Society of Petroleum Engineers Annual Techni... </a>
</h4>
</li>
</ul>
</div>
<div class="sf_colsIn col-lg-offset-4 col-md-offset-4 col-sm-offset-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 featured-link" data-placeholder-label="Link Placeholder" data-sf-element="Link Placeholder" id="Contentplaceholder1_C023_Col02">
<div>
<div><a class="btn-blue" href="/connect/events">See more events</a></div>
</div>
</div>
<div style="clear: both"></div>
</div>
</div>
</div>
<div class="sf_colsIn fullwidth-img-wrapper flowUp" data-placeholder-label="Full Width Image Placeholder" data-sf-element="Full Width Image Placeholder" id="Contentplaceholder1_C014_Col00"> <div class="slider-wrapper subpage-hero" style="background-image:url('https://www.aramco.jobs/images/librariesprovider2/default-album/home-super-foot.png?sfvrsn=e23e46b7_0');">
<div class="container">
<div class="slide-content">
<h1></h1>
</div>
</div>
</div>
</div>
<footer>
<div class="container">
<div class="foot-top mobile-hidden">
<div class="sf_colsIn" data-placeholder-label="Left Column" data-sf-element="Left Column" id="Contentplaceholder1_T140A0376017_Col00">
</div>
<div class="sf_colsIn col-lg-9 col-md-9 col-sm-9 center-foot" data-placeholder-label="Center Column" data-sf-element="Center Column" id="Contentplaceholder1_T140A0376017_Col01">
<div>
<div><p>Popular pages</p>
<ul>
<li><a href="https://apply.aramco.jobs">Find a Job</a></li>
<li><a href="https://www.aramco.jobs/about-saudi-aramco">About Saudi Aramco</a></li>
<li><a href="https://www.aramco.jobs/living-in-saudi-arabia">Living Overseas</a></li>
<li><a href="https://www.aramco.jobs/comp-and-benefits">Rewards &amp; Benefits</a></li>
<li><a href="https://www.aramco.jobs/recruitment-process">Recruitment Process</a></li>
<li><a href="https://www.aramco.jobs/connect">Connect</a></li>
</ul></div>
</div>
</div>
<div class="sf_colsIn col-lg-3 col-md-3 col-sm-3 right-foot" data-placeholder-label="Right Column" data-sf-element="Right Column" id="Contentplaceholder1_T140A0376017_Col02">
<div>
<div><p>Follow us</p><ul><li><a href="https://www.linkedin.com/company/aramco" target="_blank"><img alt="" src="/App_Themes/Aramco/img/btn-linkedin-footer.png"/></a></li><li><a href="https://www.youtube.com/user/AramcoVideo" target="_blank"><img alt="" src="/App_Themes/Aramco/img/btn-youtube-footer.png"/></a></li><li><a href="https://www.facebook.com/aramco" target="_blank"><img alt="" src="/App_Themes/Aramco/img/btn-facebook-footer.png"/></a></li></ul></div>
</div>
</div>
<div style="clear:both;"></div>
<div class="col-lg-3 col-md-3 col-sm-3 left-foot">
</div>
<div class="col-lg-6 col-md-6 col-sm-6 center-foot">
</div>
<div class="sf_colsIn col-lg-3 col-md-3 col-sm-3 right-foot app-row" data-placeholder-label="Right Column" data-sf-element="Right Column" id="Contentplaceholder1_T140A0376017_Col03">
<div>
<div></div>
</div>
</div>
<div style="clear:both;"></div>
</div>
<div class="foot-bottom">
<div class="sf_colsIn col-lg-9 col-md-9 col-sm-9 foot-nav" data-placeholder-label="Sub Navigation" data-sf-element="Sub Navigation" id="Contentplaceholder1_T140A0376017_Col04">
<div>
<div><ul><li><a href="/contact-us">Contact us</a></li><li><a href="/privacy-policy">Privacy Policy</a></li></ul></div>
</div>
</div>
<div class="sf_colsIn col-lg-3 col-md-3 col-sm-3 copyright" data-placeholder-label="Copyright" data-sf-element="Copyright" id="Contentplaceholder1_T140A0376017_Col05">
<div>
<div>Copyright © <span id="currentyear"></span> Aramco Services Company</div>
</div>
</div>
<div style="clear:both;"></div>
</div>
</div>
</footer>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-8678286-8"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-8678286-8');
</script>
</div> <script src="/Frontend-Assembly/Telerik.Sitefinity.Frontend.Search/Mvc/Scripts/SearchBox/Search-box.js?package=Bootstrap&amp;v=MTIuMi43MjI1LjA%3d" type="text/javascript"></script><script src="/Frontend-Assembly/Telerik.Sitefinity.Frontend.Navigation/Mvc/Scripts/Navigation.js?package=Bootstrap&amp;v=MTIuMi43MjI1LjA%3d" type="text/javascript"></script><script src="/WebResource.axd?d=yAZznlIeaGQJz8qTi4uqDfTwXqrjC6pCO-v1Zo5GsijtKPwGNwSSosRBgNqNFs0KOcowrkJyWSPhFMS48H4sja0x-8FO7L4GCa07ohgwFawd_2LsjxlkwE3TSBSR8GwUHYedLnLO8klJM8qpG8rTZ6oZo1EYcocmahH_w2y3img1&amp;t=637163395900000000" type="text/javascript">
</script><script type="text/javascript">
	StatsClient.LogVisit('6e04265d-39b9-6cfe-9b7c-ff0000605314', '82e29a5b-e4df-4944-98e7-7cc486318876');
</script> <script src="https://use.typekit.net/yoa8aku.js"></script> <script>try{Typekit.load({ async: true });}catch(e){}</script> <script src="/App_Themes/Aramco/js/slick.min.js"></script> <script src="/App_Themes/Aramco/js/video-slider.js"></script> <script src="/App_Themes/Aramco/js/ug-theme-compact.js"></script> <script src="/App_Themes/Aramco/js/flowup.js"></script> <script src="/App_Themes/Aramco/js/functions.js"></script> <script src="/App_Themes/AramcoJobs/js/functions-jobs.js"></script> </body> </html>