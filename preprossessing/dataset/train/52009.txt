<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="/res/front/sub-site/gehua/css/reset.css" rel="stylesheet" type="text/css"/>
<link href="/res/front/sub-site/gehua/css/common.css" rel="stylesheet" type="text/css"/>
<link href="/res/front/sub-site/gehua/css/home.css" rel="stylesheet" type="text/css"/>
<link href="/res/front/sub-site/gehua/css/form.css" rel="stylesheet" type="text/css"/>
<link href="/res/front/sub-site/gehua/css/select.css" rel="stylesheet" type="text/css"/>
<script src="/res/front/sub-site/gehua/js/jquery.min.js" type="text/javascript"></script>
<script src="/res/front/sub-site/gehua/js/jquery.SuperSlide.2.1.1.js" type="text/javascript"></script>
<script src="/res/front/sub-site/gehua/js/jquery-hover.js" type="text/javascript"></script>
<script src="/res/front/sub-site/gehua/js/jquery-form.js" type="text/javascript"></script>
<script src="/res/front/sub-site/gehua/js/jquery.select.js" type="text/javascript"></script>
<script src="/res/front/sub-site/gehua/js/jquery-cookie.js" type="text/javascript"></script>
<script src="/res/front/sub-site/gehua/js/cms.js" type="text/javascript"></script>
<script src="/res/front/sub-site/gehua/js/smsCode.js" type="text/javascript"></script>
<!--[if IE 6]>   
<script type="text/javascript" src="js/DD_belatedPNG_0.0.8a-min.js"></script> 
<script>  
    DD_belatedPNG.fix('*');   
</script>  
<![endif]-->
<script type="text/javascript">
function browserRedirect() { 
var url = location.href;
var sUserAgent= navigator.userAgent.toLowerCase(); 
var bIsIpad= sUserAgent.match(/ipad/i) == "ipad"; 
var bIsIphoneOs= sUserAgent.match(/iphone os/i) == "iphone os"; 
var bIsMidp= sUserAgent.match(/midp/i) == "midp"; 
var bIsUc7= sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4"; 
var bIsUc= sUserAgent.match(/ucweb/i) == "ucweb"; 
var bIsAndroid= sUserAgent.match(/android/i) == "android"; 
var bIsCE= sUserAgent.match(/windows ce/i) == "windows ce"; 
var bIsWM= sUserAgent.match(/windows mobile/i) == "windows mobile"; 
 
if ((bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM )&&url.indexOf('.tv/wap')==-1) { 
   
  //window.location.href= 'http://http://172.24.64.211:8080/wap';
 window.location.href= 'http://www.96196.tv/wap'; 
  
} 
} 

browserRedirect(); 
	$.get("/visit/count.action",{location:'WS'},function (data,textStatus){});
	$.get("/visit/countVisit.action",{location:'WS'},function (data,textStatus){});
	$.get("/visit/countEvery.action",{location:'WS'},function (data,textStatus){});
</script>
<script src="/res/front/sub-site/gehua/js/rotateRolling.js" type="text/javascript"></script>
<script src="/res/front/sub-site/gehua/js/cms.js" type="text/javascript"></script>
<script src="/res/front/sub-site/gehua/js/DD_belatedPNG_0.0.8a-min.js" type="text/javascript"></script>
<title>歌华网上营业厅</title>
</head>
<body>
<!-- 首页头部 -->
<div class="top-header-nav">
<div class="top-bg"><img src="/res/front/sub-site/gehua/images/top-header-bg.png"/></div>
<div class="top-inner">
<div class="top-notice">
    			欢迎来到歌华有线网上营业厅，请不要向陌生人透露您的账号信息。
    	</div>
<div class="top-tools">
<div class="top-fun function-1"><a href="https://bgctv.tmall.com/" target="_blank">天猫旗舰店</a></div>
<!-- <div class="top-fun function-3"><a href="/jf/detailList.action">积分商城</a></div> --><!--隐藏的话 可以注释这段 -->
<!--             <div class="top-fun function-2">
            	<div class="phone-box">
                	<a class="icon-phone">掌上歌华</a>
                	<ul class="phone-box-ul">
	                    	<li class="phone-box-ul-li">
	                        	<div class="phone-code code-1"><img src="/u/cms/sub-site/201607/25175314kopr.png" width="120" height="120" /></div>
	                            <span class="phone-box-ul-li-span">掌上营业厅</span>  	
	                        </li>
	                    	<li class="phone-box-ul-li">
	                        	<div class="phone-code code-1"><img src="/u/cms/sub-site/201608/10135908rbga.png" width="120" height="120" /></div>
	                            <span class="phone-box-ul-li-span">云飞视</span>  	
	                        </li>
	                    	<li class="phone-box-ul-li">
	                        	<div class="phone-code code-1"><img src="/u/cms/sub-site/201604/qr-code.png" width="120" height="120" /></div>
	                            <span class="phone-box-ul-li-span">官方旗舰店</span>  	
	                        </li>
                    </ul>
                </div>
            </div> -->
<div class="top-fun function-3"><a href="/xsbz2/index.htm?me=6">新手帮助</a></div>
<div class="top-fun function-4"><a href="/ywjsyxds.htm?mainChannelId=&amp;me=6">业务介绍</a></div>
</div>
</div>
</div>
<div class="mainnav">
<div class="mainnav-inner">
<!-- 顶部logo -->
<div class="logo"><a href="/" style="margin-top:0px"><img height="48" src="/res/front/sub-site/gehua/images/home_gehua_logo.png" width="215"/></a></div> <!-- 顶部菜单 -->
<!--
            <div class="nav-name"><a class="nav-name-a" href="/product/findListOfTV.action">电视业务</a><span class="cur-line"></span></div>
            <div class="nav-name"><a class="nav-name-a" href="/product/findListOfBroadBand.action">宽带业务</a><span class="cur-line"></span></div>
            <div class="nav-name"><a class="nav-name-a" href="/product/findListOfHardWare.action">硬件业务</a><span class="cur-line"></span></div>
            <div class="nav-name"><a class="nav-name-a" href="/product/findListOfCorp.action">集团业务</a><span class="cur-line"></span></div>
            <div class="nav-name"><a class="nav-name-a" href="/product/findListOfService.action">服务中心</a><span class="cur-line"></span></div>
        -->
<div class="nav">
<div class="nav-name">
<a class="nav-name-a nav-cur" hidefocus="true" href="/?me=1&amp;mainChannelId=0">首页</a>
<span class="cur-line" style="display:block;"></span>
</div>
<div class="nav-name">
<a class="nav-name-a" hidefocus="true" href="/tv/index.action?me=2&amp;mainChannelId=301">电视业务</a>
<span class="cur-line"></span>
</div>
<div class="nav-name">
<a class="nav-name-a" hidefocus="true" href="/cm/index.action?me=3&amp;mainChannelId=598">宽带业务</a>
<span class="cur-line"></span>
</div>
<div class="nav-name">
<a class="nav-name-a" hidefocus="true" href="/hw/index.action?me=4&amp;mainChannelId=599">硬件业务</a>
<span class="cur-line"></span>
</div>
<div class="nav-name">
<a class="nav-name-a" hidefocus="true" href="/corp/index.action?me=5&amp;mainChannelId=600">集团业务</a>
<span class="cur-line"></span>
</div>
<div class="nav-name">
<a class="nav-name-a" hidefocus="true" href="/cs/index.action?me=6&amp;mainChannelId=601">客服中心</a>
<span class="cur-line"></span>
</div>
<!-- <div class="nav-name">
        		<a hidefocus="true" class="nav-name-a" href="/jf/detailList.action?me=7">积分商城</a>
        		<span class="cur-line" ></span>
        	</div> --><!--隐藏的话 可以注释这段 -->
</div> <!-- 顶部用户中心 -->
<div class="user">
<div class="user-button button-1"><a onclick="javascript:location.href='/register/o_userRegist01.action'" style="cursor: pointer;">立即注册</a></div>
<div class="user-button button-2" style="margin-left:8px;"><a href="/login.jspx">登录</a></div>
</div> </div>
</div>
<!-- 上部图片轮询和快捷服务 -->
<div class="focus-box">
<div class="focus">
<div class="focus-inner">
<a href="javascript:" id="prev"></a>
<a href="javascript:" id="next"></a>
<ul>
<!-- <li>
	                    <img src="/res/front/sub-site/gehua/images/henyear.jpg" border="0" width="1280" height="400" /> 
	                    <a  href="/aa/actact.action" target="_blank">
	                    	<img src="/res/front/sub-site/gehua/images/henyear.jpg" border="0" width="1280" height="400" />
	                    </a>
	                </li> -->
<li>
<a href="/shouygdgg/755.htm">
<img border="0" height="400" src="/u/cms/sub-site/202101/07100759qqyv.jpg" width="1280"/>
</a>
</li>
<!-- <li>
	                    <img src="/res/front/sub-site/gehua/images/henyear.jpg" border="0" width="1280" height="400" /> 
	                    <a  href="/aa/actact.action" target="_blank">
	                    	<img src="/res/front/sub-site/gehua/images/henyear.jpg" border="0" width="1280" height="400" />
	                    </a>
	                </li> -->
<li>
<a href="/shouygdgg/760.htm">
<img border="0" height="400" src="/u/cms/sub-site/202101/07100938qq0q.jpg" width="1280"/>
</a>
</li>
<!-- <li>
	                    <img src="/res/front/sub-site/gehua/images/henyear.jpg" border="0" width="1280" height="400" /> 
	                    <a  href="/aa/actact.action" target="_blank">
	                    	<img src="/res/front/sub-site/gehua/images/henyear.jpg" border="0" width="1280" height="400" />
	                    </a>
	                </li> -->
<li>
<a href="/shouygdgg/751.htm">
<img border="0" height="400" src="/u/cms/sub-site/202012/311024402fs3.jpg" width="1280"/>
</a>
</li>
<!-- <li>
	                    <img src="/res/front/sub-site/gehua/images/henyear.jpg" border="0" width="1280" height="400" /> 
	                    <a  href="/aa/actact.action" target="_blank">
	                    	<img src="/res/front/sub-site/gehua/images/henyear.jpg" border="0" width="1280" height="400" />
	                    </a>
	                </li> -->
<li>
<a href="/shouygdgg/759.htm">
<img border="0" height="400" src="/u/cms/sub-site/202012/31102727krj7.jpg" width="1280"/>
</a>
</li>
</ul>
<ol></ol>
</div>
<div class="channel">
<div class="channel-tit c-t-bg-2">快捷服务</div>
<div class="channel-con">
<ul>
<li class="channel-li-1"><a class="channel-a channel-1" href="/person/perInfo.action">信息查询</a></li>
<li class="channel-li-2"><a class="channel-a channel-2" href="/order/initInstall.action">业务报装</a></li>
<li class="channel-li-3"><a class="channel-a channel-3" href="/tv/v_settlementOfArrears.action">费用清缴</a></li>
<li class="channel-li-4"><a class="channel-a channel-4" href="/cm/initCmNewalsSelectProds.action?me=1&amp;isLine=Y">宽带续费</a></li>
<li class="channel-li-5"><a class="channel-a channel-5" href="/tv/initRecharge.action?me=1">账户充值</a></li>
<li class="channel-li-6"><a class="channel-a channel-6" href="/order/index_fault.action?me=1">故障报修</a></li>
</ul>
</div>
</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
     	try{
	        $(".focus").slide({
	            titCell:".hd ul",
	            mainCell:".bd ul",
	            effect:"fold",
	            interTime:5000,
	            delayTime:1000,
	            autoPlay:true,
	            autoPage:true, 
	            trigger:"click" 
	        });
        }catch(e){}
    });

    </script>
<div class="content">
<div class="content-inner">
<div class="layer height-352">
<div class="layer-top">
<div class="layer-top-tit layer-h-1"><h1>明星产品</h1></div>
<span class="layer-point point-1">最热门，最值得购买的都在这里！</span>
</div>
<div class="layer-con height-300">
<ul class="protect-ul">
<li class="protect" style=" margin:0;">
<div class="protect-div">
<span class="cur-a" style=" width:226px; height:298px;"></span>
<div class="protect-img"><a href="/tv/detailProduct.action?prodId=800500005713&amp;policyId=400011142&amp;kind=TV"><img height="226" src="/u/cms/sub-site/201605/dsyxnk.jpg" width="226"/></a></div>
<div class="protect-name">
<dl>
<dt><a href="/tv/detailProduct.action?prodId=800500005713&amp;policyId=400011142&amp;kind=TV">电视院线会员产品包</a></dt>
<dd><span class="protect-price">238</span>元/年</dd>
</dl>
</div>
</div>
</li>
<li class="protect" style=" width:226px; height:298px;">
<div class="protect-div">
<span class="cur-a" style=" width:226px; height:298px;"></span>
<div class="protect-img"><a href="/tv/detailProduct.action?prodId=800400001142&amp;policyId=400982466&amp;kind=BB"><img height="226" src="/u/cms/sub-site/202009/171651365fse.jpg" width="226"/></a></div>
<div class="protect-name">
<dl>
<dt><a href="/tv/detailProduct.action?prodId=800400001142&amp;policyId=400982466&amp;kind=BB">110M宽带（2020版）</a></dt>
<dd><span class="protect-price">780</span>元/年</dd>
</dl>
</div>
</div>
</li>
<li class="protect" style=" width:226px; height:298px;">
<div class="protect-div">
<span class="cur-a" style=" width:226px; height:298px;"></span>
<div class="protect-img"><a href="/tv/detailProduct.action?prodId=800600009812&amp;policyId=&amp;kind=HW"><img height="226" src="/u/cms/sub-site/201907/10171201369l.png" width="226"/></a></div>
<div class="protect-name">
<dl>
<dt><a href="/tv/detailProduct.action?prodId=800600009812&amp;policyId=&amp;kind=HW">歌华小果</a></dt>
<dd> <span class="protect-price">598</span> - <span class="protect-price">1680</span>元</dd>
</dl>
</div>
</div>
</li>
</ul>
</div>
</div>
<div class="layer">
<div class="layer-top">
<div class="layer-top-tit"><h1 class="layer-h-2">宽带产品</h1></div>
<span class="layer-point point-2">超值特惠，买送升级最给力！</span>
<a class="layer-more" href="/cm/index.action?me=3">更多</a>
</div>
<div class="layer-con height-470">
<ul class="net-ul">
<li class="net-li-1 net-1">
<span class="cur-a" style="width:228px; height:470px;"></span>
<a href="/tv/detailProduct.action?prodId=800500009895&amp;policyId=400012853&amp;kind=BB"><img height="470" src="/u/cms/sub-site/201912/311419329y4k.jpg" width="228"/></a>
</li>
<li class="net-li-2 net-2">
<span class="cur-a" style="width:227px; height:227px;"></span>
<a href="/tv/detailProduct.action?prodId=800400001140&amp;policyId=400973311&amp;kind=BB&amp;me=3"><img height="227" src="/u/cms/sub-site/202009/171649023tqm.jpg" width="227"/></a>
</li>
<li class="net-li-3 net-2">
<span class="cur-a" style="width:227px; height:227px;"></span>
<a href="/tv/detailProduct.action?prodId=800400001138&amp;policyId=400973309&amp;kind=BB&amp;me=3"><img height="227" src="/u/cms/sub-site/202009/17164802j3tw.jpg" width="227"/></a>
</li>
<li class="net-li-4 net-2">
<span class="cur-a" style="width:227px; height:227px;"></span>
<a href="/tv/detailProduct.action?prodId=800400001137&amp;policyId=400973308&amp;kind=BB&amp;me=3"><img height="227" src="/u/cms/sub-site/202009/17164651ccyf.jpg" width="227"/></a>
</li>
<li class="net-li-5 net-2">
<span class="cur-a" style="width:227px; height:227px;"></span>
<a href="/tv/detailProduct.action?prodId=800400001130&amp;policyId=400973221&amp;kind=BB&amp;me=3"><img height="227" src="/u/cms/sub-site/202009/17164849a1ux.jpg" width="227"/></a>
</li>
<li class="net-li-6 net-2">
<span class="cur-a" style="width:227px; height:227px;"></span>
<a href="/tv/detailProduct.action?prodId=800400001128&amp;policyId=400973222&amp;kind=BB&amp;me=3"><img height="227" src="/u/cms/sub-site/202009/17164817t61l.jpg" width="227"/></a>
</li>
<li class="net-li-7 net-2">
<span class="cur-a" style="width:227px; height:227px;"></span>
<a href="/tv/detailProduct.action?prodId=800400001127&amp;policyId=400973081&amp;kind=BB&amp;me=3"><img height="227" src="/u/cms/sub-site/202009/17164709mnnq.jpg" width="227"/></a>
</li>
</ul>
</div>
</div>
<div class="layer">
<div class="layer-top">
<div class="layer-top-tit"><h1 class="layer-h-3">高清节目</h1></div>
<span class="layer-point point-3">更多精彩节目，尽在歌华有线！</span>
<a class="layer-more" href="/tv/detailList.action?type=HD&amp;me=2">更多</a>
</div>
<div id="d_tab29">
<ul class="d_img">
<li class="d_pos1">
<a href="/tv/detailProduct.action?prodId=800500005713&amp;policyId=400011142&amp;kind=TV&amp;me=2"><img src="/u/cms/sub-site/201605/dsyxnk.jpg"/></a>
<div class="hdtv-name-3">
<dl>
<dt class="hdtv-dt"><a href="/tv/detailProduct.action?prodId=800500005713&amp;policyId=400011142&amp;kind=TV&amp;me=2">电视院线会员产品包</a></dt>
<dd></dd>
</dl>
</div>
</li>
<li class="d_pos2">
<a href="/tv/detailProduct.action?prodId=800500004420&amp;policyId=400001554&amp;kind=TV&amp;me=2"><img src="/u/cms/sub-site/201608/29101657sr75.jpg"/></a>
<div class="hdtv-name-3">
<dl>
<dt class="hdtv-dt"><a href="/tv/detailProduct.action?prodId=800500004420&amp;policyId=400001554&amp;kind=TV&amp;me=2">巧虎来啦</a></dt>
<dd></dd>
</dl>
</div>
</li>
<li class="d_pos3">
<a href="/tv/detailProduct.action?prodId=800500005305&amp;policyId=400010493&amp;kind=TV&amp;me=2"><img src="/u/cms/sub-site/201605/guoguo.jpg"/></a>
<div class="hdtv-name-3">
<dl>
<dt class="hdtv-dt"><a href="/tv/detailProduct.action?prodId=800500005305&amp;policyId=400010493&amp;kind=TV&amp;me=2">果果乐园</a></dt>
<dd></dd>
</dl>
</div>
</li>
<li class="d_pos4">
<a href="/tv/detailProduct.action?prodId=800500005236&amp;policyId=400010423&amp;kind=TV&amp;me=2"><img src="/u/cms/sub-site/201605/kalaok.jpg"/></a>
<div class="hdtv-name-3">
<dl>
<dt class="hdtv-dt"><a href="/tv/detailProduct.action?prodId=800500005236&amp;policyId=400010423&amp;kind=TV&amp;me=2">卡拉OK</a></dt>
<dd></dd>
</dl>
</div>
</li>
<li class="d_pos5">
<a href="/tv/detailProduct.action?prodId=800500900360&amp;policyId=400901360&amp;kind=TV&amp;me=2"><img src="/u/cms/sub-site/201912/30160430y0hk.jpg"/></a>
<div class="hdtv-name-3">
<dl>
<dt class="hdtv-dt"><a href="/tv/detailProduct.action?prodId=800500900360&amp;policyId=400901360&amp;kind=TV&amp;me=2">芒果互联网电视</a></dt>
<dd></dd>
</dl>
</div>
</li>
</ul>
<p class="d_prev"></p>
<p class="d_next"></p>
</div>
<script>
                $('#d_tab29').DB_rotateRollingBanner({
                    key:"c37080",            
                    moveSpeed:200,           
                    autoRollingTime:3000      
                })
            </script>
</div>
<!--         	<div class="layer">
        	<div class="layer-top">
       	  	  <div class="layer-top-tit"><h1 class="layer-h-3">高清节目</h1></div>
                <span class="layer-point point-3">更多精彩节目，尽在歌华有线！</span>
              	<a class="layer-more" href="/tv/detailList.action?type=HD&me=2">更多</a>
       	    </div>
            <div class="layer-con height-364">
				<ul class="hdtv-ul">
            		<li class="hdtv-li-1">
                    	<a href="/tv/detailProduct.action?prodId=800500005713&policyId=400011142&kind=TV&me=2"><img src="/u/cms/sub-site/201605/dsyxnk.jpg" /></a>
                        <div class="hdtv-name-1">
                        	<dl>
                            	<dt><a href="/tv/detailProduct.action?prodId=800500005713&policyId=400011142&kind=TV&me=2">电视院线会员产品包</a></dt>
                                <dd></dd>
                            </dl>
                        </div>
                    </li>
            		<li class="hdtv-li-2">
                    	<a href="/tv/detailProduct.action?prodId=800500004420&policyId=400001554&kind=TV&me=2"><img src="/u/cms/sub-site/201608/29101657sr75.jpg" /></a>
                        <div class="hdtv-name-1">
                        	<dl>
                            	<dt><a href="/tv/detailProduct.action?prodId=800500004420&policyId=400001554&kind=TV&me=2">巧虎来啦</a></dt>
                                <dd></dd>
                            </dl>
                        </div>
                    </li>
            		<li class="hdtv-li-3">
                    	<a href="/tv/detailProduct.action?prodId=800500005305&policyId=400010493&kind=TV&me=2"><img src="/u/cms/sub-site/201605/guoguo.jpg" /></a>
                        <div class="hdtv-name-1">
                        	<dl>
                            	<dt><a href="/tv/detailProduct.action?prodId=800500005305&policyId=400010493&kind=TV&me=2">果果乐园</a></dt>
                                <dd></dd>
                            </dl>
                        </div>
                    </li>
            		<li class="hdtv-li-4">
                    	<a href="/tv/detailProduct.action?prodId=800500005236&policyId=400010423&kind=TV&me=2"><img src="/u/cms/sub-site/201605/kalaok.jpg" /></a>
                        <div class="hdtv-name-1">
                        	<dl>
                            	<dt><a href="/tv/detailProduct.action?prodId=800500005236&policyId=400010423&kind=TV&me=2">卡拉OK</a></dt>
                                <dd></dd>
                            </dl>
                        </div>
                    </li>
            		<li class="hdtv-li-5">
                    	<a href="/tv/detailProduct.action?prodId=800500900360&policyId=400901360&kind=TV&me=2"><img src="/u/cms/sub-site/201912/30160430y0hk.jpg" /></a>
                        <div class="hdtv-name-1">
                        	<dl>
                            	<dt><a href="/tv/detailProduct.action?prodId=800500900360&policyId=400901360&kind=TV&me=2">芒果互联网电视</a></dt>
                                <dd></dd>
                            </dl>
                        </div>
                    </li>
                </ul>
            </div>
        </div>   -->
<!--     	<div class="layer height-352">
        	<div class="layer-top">
            	<div class="layer-top-tit layer-h-4"><h1>游戏专区</h1></div>
                <span class="layer-point point-4">最热门，最值得购买的都在这里！</span>
                <a class="layer-more" href="/tv/detailList.action?type=GAME&me=2">更多</a>
            </div>
            <div class="layer-con height-280">
            	<ul class="game-ul">
                </ul>
            </div>
        </div> -->
<div class="layer height-352">
<div class="layer-top">
<div class="layer-top-tit layer-h-5"><h1>硬件产品</h1></div>
<span class="layer-point point-5">最热门，最值得购买的都在这里！</span>
<!-- <a class="layer-more" href="/hw/detailList.action?me=4">更多</a> -->
<a class="layer-more" href="/hw/index.action?me=4&amp;mainChannelId=599">更多</a>
</div>
<div class="layer-con height-300">
<ul class="protect-ul">
<!--  <a  href="/smp/smpt.action">SMP接口测试模块</a> -->
<li class="protect" style=" margin:0;">
<div class="protect-div">
<span class="cur-a" style=" width:226px; height:298px;"></span>
<div class="protect-img"><a href="/tv/detailProduct.action?prodId=800600009812&amp;&amp;kind=HW&amp;me=4"><img height="226" src="/u/cms/sub-site/201907/10171735k0pz.png" width="226"/></a></div>
<div class="protect-name">
<dl>
<dt><a href="/tv/detailProduct.action?prodId=800600009812&amp;&amp;kind=HW&amp;me=4">歌华小果</a></dt>
<dd> <span class="protect-price">598</span> - <span class="protect-price">1180</span>元</dd>
</dl>
</div>
</div>
</li>
<!--  <a  href="/smp/smpt.action">SMP接口测试模块</a> -->
<li class="protect">
<div class="protect-div">
<span class="cur-a" style=" width:226px; height:298px;"></span>
<div class="protect-img"><a href="/tv/detailProduct.action?prodId=800600006111&amp;&amp;kind=HW&amp;me=4"><img height="226" src="/u/cms/sub-site/201605/cqh-jbx.jpg" width="226"/></a></div>
<div class="protect-name">
<dl>
<dt><a href="/tv/detailProduct.action?prodId=800600006111&amp;&amp;kind=HW&amp;me=4">歌华超清智能机顶盒（基本型）</a></dt>
<dd> <span class="protect-price">780</span> - <span class="protect-price">780</span>元</dd>
</dl>
</div>
</div>
</li>
<!--  <a  href="/smp/smpt.action">SMP接口测试模块</a> -->
<li class="protect">
<div class="protect-div">
<span class="cur-a" style=" width:226px; height:298px;"></span>
<div class="protect-img"><a href="/tv/detailProduct.action?prodId=800600006103&amp;&amp;kind=HW&amp;me=4"><img height="226" src="/u/cms/sub-site/201605/cqh-zqx.jpg" width="226"/></a></div>
<div class="protect-name">
<dl>
<dt><a href="/tv/detailProduct.action?prodId=800600006103&amp;&amp;kind=HW&amp;me=4">歌华超清智能机顶盒（增强型）</a></dt>
<dd> <span class="protect-price">880</span> - <span class="protect-price">880</span>元</dd>
</dl>
</div>
</div>
</li>
<!--  <a  href="/smp/smpt.action">SMP接口测试模块</a> -->
<li class="protect">
<div class="protect-div">
<span class="cur-a" style=" width:226px; height:298px;"></span>
<div class="protect-img"><a href="/tv/detailProduct.action?prodId=800600009808&amp;&amp;kind=HW&amp;me=4"><img height="226" src="/u/cms/sub-site/202007/06095333aee8.png" width="226"/></a></div>
<div class="protect-name">
<dl>
<dt><a href="/tv/detailProduct.action?prodId=800600009808&amp;&amp;kind=HW&amp;me=4">小度智能音箱系列套餐</a></dt>
<dd> <span class="protect-price">249</span> - <span class="protect-price">780</span>元</dd>
</dl>
</div>
</div>
</li>
</ul>
</div>
</div>
</div>
</div>
<!--页面脚部 -->
<div class="footer">
<div class="footer-inner floor-1">
<div class="footer-left" style="padding-left:20px">
<ul class="footer-link">
<li><a href="http://www.bgctv.com.cn/" target="_blank">关于我们</a></li>
<li><a href="/yytlb/index.htm?me=6">营业网点</a></li>
<li><a href="/yxdsyw/index.htm?me=6">常见问题</a></li>
<li style="border:none;"><a href="http://www.gehua.net/" target="_blank">友情链接</a></li>
</ul>
<div class="footer-tel"><span class="tel-text">客服热线 ：</span><span class="tel-num">96196</span><!-- <span class="tel-text">周一至周六  9 : 00 - 16 : 00</span> --></div>
</div>
<div class="footer-right">
<div class="footer-rightss">
<div class="footer-code" style="float:left;"><img height="100" src="/u/cms/sub-site/201604/wx2wm.jpg" width="100"/></div>
<span>歌华有线<br/>微信营业厅</span>
</div>
<div class="footer-rightss">
<div class="footer-code"><img height="100" src="/u/cms/sub-site/201607/25175314kopr.png" width="100"/></div>
<span>歌华有线<br/>掌上营业厅</span>
</div>
<div class="footer-rightss">
<div class="footer-code" style="float:left;"><img height="100" src="/u/cms/sub-site/201608/10135908rbga.png" width="100"/></div>
<span>歌华<br/>云飞视</span>
</div>
</div>
<!--             <div class="footer-right">
      </div> -->
</div>
<div class="footer-inner floor-2"> 京ICP备 05060937号
北京市东城区青龙胡同1号歌华大厦7层 北京歌华有线电视网络股份有限公司
北京市公安局京公网安备 110402120003号</div>
</div></body>
</html>