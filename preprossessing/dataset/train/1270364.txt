<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" lang="ja" xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:mixi="http://mixi-platform.com/ns#" xmlns:og="http://ogp.me/ns#">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<title>インテリア SHOP　「さるるの部屋」</title>
<meta content="ラグカーペット,玄関マット,クッション,い草敷物のインテリア商品通信販売" name="Keywords"/>
<meta content="インテリア商品の通信販売。主にカーペット、ラグ、クッション、マットの格安通販。" name="Description"/>
<meta content="Sato Seiji" name="Author"/>
<meta content="Copyright 2004 SATOMENGYO CO.,LTD." name="Copyright"/>
<meta content="text/css" http-equiv="content-style-type"/>
<meta content="text/javascript" http-equiv="content-script-type"/>
<link href="https://satomen.com/css/framework/colormekit.css" rel="stylesheet" type="text/css"/>
<link href="https://satomen.com/css/framework/colormekit-responsive.css" rel="stylesheet" type="text/css"/>
<link href="https://img08.shop-pro.jp/PA01036/372/css/20/index.css?cmsp_timestamp=20190204113107" rel="stylesheet" type="text/css"/>
<meta content="aIG3c-87UmQElak0RtEo-UKQLHl5qUAr9HUI2D7Kjq8" name="google-site-verification"/>
<link href="https://satomen.com/?mode=rss" rel="alternate" title="rss" type="application/rss+xml"/>
<link href="https://satomen.com/" media="handheld" rel="alternate" type="text/html"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<meta content="インテリア SHOP　「さるるの部屋」" property="og:title"/>
<meta content="インテリア商品の通信販売。主にカーペット、ラグ、クッション、マットの格安通販。" property="og:description"/>
<meta content="https://satomen.com" property="og:url"/>
<meta content="インテリア SHOP　「さるるの部屋」" property="og:site_name"/>
<script>
  var Colorme = {"page":"top","shop":{"account_id":"PA01036372"},"basket":{"total_price":0,"items":[]},"customer":{"id":null}};
</script>
<script>
  (function() {
    function insertScriptTags() {
      var scriptTagDetails = [];
      var entry = document.getElementsByTagName('script')[0];

      scriptTagDetails.forEach(function(tagDetail) {
        var script = document.createElement('script');

        script.type = 'text/javascript';
        script.src = tagDetail.src;
        script.async = true;

        if( tagDetail.integrity ) {
          script.integrity = tagDetail.integrity;
          script.setAttribute('crossorigin', 'anonymous');
        }

        entry.parentNode.insertBefore(script, entry);
      })
    }

    window.addEventListener('load', insertScriptTags, false);
  })();
</script>
</head>
<body>
<img alt="" height="1" src="https://acclog001.shop-pro.jp/li.php?st=1&amp;pt=10001&amp;ut=0&amp;at=PA01036372&amp;v=20210113024433&amp;re=&amp;cn=f6f346c72c86144224e47a189b28de45" style="margin:0px;padding:0px;border:none;position:absolute;top:0px;left:0px;" width="1"/><img alt="" height="1" src="https://acclog002.shop-pro.jp/li.php?st=1&amp;pt=10001&amp;ut=0&amp;at=PA01036372&amp;v=20210113024433&amp;re=&amp;cn=f6f346c72c86144224e47a189b28de45" style="margin:0px;padding:0px;border:none;position:absolute;top:0px;left:0px;" width="1"/><script src="https://img.shop-pro.jp/tmpl_js/73/jquery.tile.js"></script>
<script src="https://img.shop-pro.jp/tmpl_js/73/jquery.skOuterClick.js"></script>
<div class="container mar_auto pad_t_20" id="wrapper">
<div class="mar_b_30" id="header">
<p class="txt_10">インテリア SHOP　「さるるの部屋」</p>
<ul class="inline pull-right col-sm-12 hidden-phone txt_r">
<li>
<a class="txt_c_333" href="https://satomen.com/?mode=myaccount"><i class="icon-b icon-user va-10 mar_r_5"></i>マイアカウント</a>
</li>
<li>
<a class="txt_c_333" href="https://members.shop-pro.jp/?mode=members_regi&amp;shop_id=PA01036372"><i class="icon-b icon-adduser va-10 mar_r_5"></i>会員登録</a>
</li>
<li>
<a class="txt_c_333" href="https://satomen.com/?mode=members"><i class="icon-b icon-login va-10 mar_r_5"></i>ログイン</a>
</li>
</ul>
<br clear="all"/>
<div align="center" class="txt_24" id="headborder">
<a href="./"><img alt="インテリア SHOP　「さるるの部屋」" src="https://img08.shop-pro.jp/PA01036/372/PA01036372.png?cmsp_timestamp=20201228101521"/></a></div>
<ul class="inline mar_t_30 bor_t_1 bor_b_1">
<li class="pad_v_10 mar_r_20"><a class="txt_c_333" href="./"><i class="icon-lg-b icon-home va-30 mar_r_5"></i>ホーム</a></li>
<li class="pad_v_10 mar_r_20"><a class="txt_c_333" href="https://satomen.com/?mode=sk"><i class="icon-lg-b icon-help va-30 mar_r_5"></i>支払・配送について</a></li>
<li class="pad_v_10 mar_r_20"><a class="txt_c_333" href="http://sarurunoheya.jugem.jp/"><i class="icon-lg-b icon-pencil va-30 mar_r_5"></i>ショップブログ</a></li> <li class="pad_v_10 mar_r_20"><a class="txt_c_333" href="https://satomen.shop-pro.jp/secure/?mode=inq&amp;shop_id=PA01036372"><i class="icon-lg-b icon-mail va-30 mar_r_5"></i>お問い合わせ</a></li>
<li class="pad_v_10 mar_r_20"><a class="txt_c_333" href="https://satomen.shop-pro.jp/cart/proxy/basket?shop_id=PA01036372&amp;shop_domain=satomen.com"><i class="icon-lg-b icon-cart va-30 mar_r_5"></i>カートを見る</a></li>
<li class="pad_v_10 mar_r_20"><a class="txt_c_333" href="http://amzn.to/2hZhiOc" target="_blank"><img alt="Amazon店へ" src="https://img08.shop-pro.jp/PA01036/372/etc/amazon.gif?cmsp_timestamp=20170106172023"/></a></li>
</ul>
<li class="pad_10 bor_t_1 visible-phone"><a class="txt_c_333" href="https://satomen.com/?mode=myaccount">マイアカウント</a></li>
<li class="pad_10 bor_t_1 visible-phone"><a class="txt_c_333" href="https://members.shop-pro.jp/?mode=members_regi&amp;shop_id=PA01036372">会員登録</a></li>
<li class="pad_10 bor_t_1 visible-phone"><a class="txt_c_333" href="https://satomen.com/?mode=members">ログイン</a></li>
</div>
<div class="slider">
<link href="./js/jquery.bxslider/jquery.bxslider.css" rel="stylesheet" type="text/css"/>
<script src="./js/jquery.bxslider/jquery.bxslider.min.js"></script>
<div id="slider">
<div><img alt="" src="https://img08.shop-pro.jp/PA01036/372/slideshow/slideshow_img_3928aa.jpg?cmsp_timestamp=20170111103755" title=""/></div>
<div><img alt="" src="https://img08.shop-pro.jp/PA01036/372/slideshow/slideshow_img_23cc22.jpg?cmsp_timestamp=20170119101634" title=""/></div>
<div><img alt="" src="https://img08.shop-pro.jp/PA01036/372/slideshow/slideshow_img_64d242.jpg?cmsp_timestamp=20170111103758" title=""/></div>
</div>
<style>
  .bx-wrapper .bx-pager {
    padding-top: 10px;
    bottom: -20px;
  }
  .bx-wrapper {
    margin-bottom: 30px;
  }
</style>
</div>
<script type="text/javascript">
//<![CDATA[
$(function(){
  $('#slider').bxSlider({
    auto: true,
    pause: 5000,
    speed: 3000,
    controls: false,
    captions: true,
    mode: 'fade'
  });
});
//]]>
</script>
<div class="row">
<div align="center" class="col col-md-12 col-lg-6"><a href="http://satomen.com/?mode=f9">
<img alt="折りたたみカーペット特集" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/etc/carpet_link.gif?cmsp_timestamp=20170111175616"/>
</a></div>
<div align="center" class="col col-md-12 col-lg-6">
<a href="http://satomen.com/?mode=f13">
<img alt="巻きカーペット特集" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/etc/makicarpet_link.gif?cmsp_timestamp=20170111175720"/>
</a>
</div>
</div>
<div class="row">
<ul class="row">
<li class="col col-xs-12 col-lg-4 pad_20 txt_c"><a href="http://satomen.com/?mode=srh&amp;cid=&amp;keyword=&amp;x=13&amp;y=13&amp;sort=n"><img alt="新入荷" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/etc/sbanner_newarrival.gif?cmsp_timestamp=20170116180401"/></a>
</li>
<li class="col col-xs-12 col-lg-4 pad_20 txt_c"><a href="http://satomen.com/?mode=grp&amp;gid=34412"><img alt="訳ありアウトレット" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/etc/sbanner_outlet.gif?cmsp_timestamp=20170116180436"/></a>
</li>
<li class="col col-xs-12 col-lg-4 pad_20 txt_c"><a href="http://satomen.com/?tid=15&amp;mode=f6"><img alt="もれなくプレゼント" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/etc/sbanner_bamboo.gif?cmsp_timestamp=20170116180541"/></a>
</li>
</ul>
</div>
<div class="row">
<div class="col col-lg-9 col-sm-12 mar_b_50" id="contents">
<div class="row mar_b_50">
<h2 class="pad_l_10 mar_b_20 txt_28 txt_fwn bor_b_1">お知らせ</h2>
<div class="col col-lg-12">
<img alt="休業日について" src="https://img08.shop-pro.jp/PA01036/372/etc/info_kyujitsu.gif?cmsp_timestamp=2100"/><br/>
１２月２９日から１月４日までは、お正月休みです。<br/>
１２月２９日以降のご注文は、１月５日以降の発送となります。<br/>
土曜日、日曜日、祝日は、休業しております。<br/>
金曜日のご注文は、時間帯によりましては、発送が週明けとなる場合がございますのでご了承くださいませ。<br/>
<img alt="新着情報" src="https://img08.shop-pro.jp/PA01036/372/etc/info_news.gif?cmsp_timestamp=208205"/><br/>
<div id="news">

2020.10.1　  折りたためるユニット畳、<a href="https://satomen.com/?pid=132039314">80x160cm</a>、<a href="https://satomen.com/?pid=152295318">100x200cm</a>、が入荷しました。<br/>
2019.3.1　  国産カーペット「ローリー」、<a href="http://satomen.com/?pid=140977582">ベージュ色</a>、<a href="http://satomen.com/?pid=140979736">ブラウン色</a>、<a href="http://satomen.com/?pid=140979757">グレー色</a>、が入荷しました。<br/>
2018.3.7　  洗えるキルトラグ<a href="http://satomen.com/?pid=129734644">「ボタニカルフラワー」</a><a href="http://satomen.com/?pid=129734306">「ビンテージフラワー」</a>が入荷しました。<br/>
2017.9.1　  ふっくらボリュームラグ<a href="http://satomen.com/?pid=126721537">「フレンチブル」</a>と<a href="http://satomen.com/?pid=126727437">「シャムキャット」</a>が入荷しました。<br/>
2017.3.1　  ＰＰレジャーラグ<a href="http://satomen.com/?pid=114693789">「うさぎＢＬ」</a><a href="http://satomen.com/?pid=114694639">「うさぎＰＫ」</a><a href="http://satomen.com/?pid=114694799">「くまＲＯ」</a><a href="http://satomen.com/?pid=114695065">「イルカＢＬ」</a>が入荷しました。<br/>
2016.12.1　 <a href="http://satomen.com/?pid=114840185">「ふわもちクッション　（ブラウン、ベージュ、ネイビー）」</a>が入荷しました。<br/>
2016.6.1　  デニムとい草のコンビネーションラグ<a href="http://satomen.com/?pid=103107973">「インディゴ」</a>が入荷しました。<br/>
2016.5.1　  ボリューム竹コンパクトラグ<a href="http://satomen.com/?pid=103109727">「チェック柄」</a>と<a href="http://satomen.com/?pid=103122815">「ボーダー柄」</a>が入荷しました。<br/>
2015.3.21　  <a href="http://satomen.com/?mode=cate&amp;cbid=1922728&amp;csid=0"> さらさら肌触りのペーパーラグ</a>が入荷しました。<br/>
</div>
<img alt="お買い得情報" src="https://img08.shop-pro.jp/PA01036/372/etc/info_sale.gif?cmsp_timestamp=2128"/><br/>
季節にあった<a href="http://satomen.com/?mode=srh&amp;cid=&amp;keyword=&amp;x=13&amp;y=13&amp;sort=n">　&lt;&lt; 新着商品 ｎｅｗ　ａｒｒｉｖａｌｓ &gt;&gt;</a>もご覧ください。

    </div>
</div>
<div class="mar_b_50">
<h2 class="pad_l_10 mar_b_20 txt_28 txt_fwn bor_b_1">おすすめ商品</h2>
<ul class="row unstyled">
<li class="col col-xs-12 col-lg-4 recommend-unit pad_20 mar_b_30 txt_c">
<a href="?pid=114762247">
<img alt="ふっくらボリューム、デニム調キルトラグ　「インディゴ」&lt;img class='new_mark_img2' src='https://img.shop-pro.jp/img/new/icons51.gif' style='border:none;display:inline;margin:0px;padding:0px;width:auto;' /&gt;" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/product/114762247_th.jpg?cmsp_timestamp=20180606223232"/>
</a>
<a href="?pid=114762247">ふっくらボリューム、デニム調キルトラグ　「インディゴ」<img class="new_mark_img2" src="https://img.shop-pro.jp/img/new/icons51.gif" style="border:none;display:inline;margin:0px;padding:0px;width:auto;"/></a>
                      ふっくらボリューム、デニム調キルトラグ　「インディゴ」
                                SOLD OUT
                  </li>
<li class="col col-xs-12 col-lg-4 recommend-unit pad_20 mar_b_30 txt_c">
<a href="?pid=114763240">
<img alt="やさしい肌触りのコットンキルティングラグ「デニム＆フレンチブル」&lt;img class='new_mark_img2' src='https://img.shop-pro.jp/img/new/icons51.gif' style='border:none;display:inline;margin:0px;padding:0px;width:auto;' /&gt;" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/product/114763240_th.jpg?cmsp_timestamp=20180606223437"/>
</a>
<a href="?pid=114763240">やさしい肌触りのコットンキルティングラグ「デニム＆フレンチブル」<img class="new_mark_img2" src="https://img.shop-pro.jp/img/new/icons51.gif" style="border:none;display:inline;margin:0px;padding:0px;width:auto;"/></a>
                      やさしい肌触りのコットンキルティングラグ「デニム＆フレンチブル」
                                SOLD OUT
                  </li>
<li class="col col-xs-12 col-lg-4 recommend-unit pad_20 mar_b_30 txt_c">
<a href="?pid=114763786">
<img alt="やさしい肌触りのコットンキルティングラグ「ダンガリー＆キャット」&lt;img class='new_mark_img2' src='https://img.shop-pro.jp/img/new/icons51.gif' style='border:none;display:inline;margin:0px;padding:0px;width:auto;' /&gt;" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/product/114763786_th.jpg?cmsp_timestamp=20180606224147"/>
</a>
<a href="?pid=114763786">やさしい肌触りのコットンキルティングラグ「ダンガリー＆キャット」<img class="new_mark_img2" src="https://img.shop-pro.jp/img/new/icons51.gif" style="border:none;display:inline;margin:0px;padding:0px;width:auto;"/></a>
                      やさしい肌触りのコットンキルティングラグ「ダンガリー＆キャット」
                                SOLD OUT
                  </li>
<li class="col col-xs-12 col-lg-4 recommend-unit pad_20 mar_b_30 txt_c">
<a href="?pid=129734306">
<img alt="&lt;img class='new_mark_img1' src='https://img.shop-pro.jp/img/new/icons15.gif' style='border:none;display:inline;margin:0px;padding:0px;width:auto;' /&gt;丸洗いＯＫ！花柄キルトラグ　ビンテージ−Flower−" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/product/129734306_th.jpg?cmsp_timestamp=20180325222849"/>
</a>
<a href="?pid=129734306"><img class="new_mark_img1" src="https://img.shop-pro.jp/img/new/icons15.gif" style="border:none;display:inline;margin:0px;padding:0px;width:auto;"/>丸洗いＯＫ！花柄キルトラグ　ビンテージ−Flower−</a>
                      丸洗いＯＫ！花柄キルトラグ　ビンテージ−Flower−　
                                SOLD OUT
                  </li>
<li class="col col-xs-12 col-lg-4 recommend-unit pad_20 mar_b_30 txt_c">
<a href="?pid=129734644">
<img alt="&lt;img class='new_mark_img1' src='https://img.shop-pro.jp/img/new/icons15.gif' style='border:none;display:inline;margin:0px;padding:0px;width:auto;' /&gt;丸洗いＯＫ！花柄キルトラグ　ボタニカル−Flower−" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/product/129734644_th.jpg?cmsp_timestamp=20180325224301"/>
</a>
<a href="?pid=129734644"><img class="new_mark_img1" src="https://img.shop-pro.jp/img/new/icons15.gif" style="border:none;display:inline;margin:0px;padding:0px;width:auto;"/>丸洗いＯＫ！花柄キルトラグ　ボタニカル−Flower−</a>
                      丸洗いＯＫ！花柄キルトラグ　ボタニカル−Flower−　
                                SOLD OUT
                  </li>
<li class="col col-xs-12 col-lg-4 recommend-unit pad_20 mar_b_30 txt_c">
<a href="?pid=103107973">
<img alt="&lt;img class='new_mark_img1' src='https://img.shop-pro.jp/img/new/icons15.gif' style='border:none;display:inline;margin:0px;padding:0px;width:auto;' /&gt;い草ラグ　インディゴ（デニム＆い草）　【コンパクト収納タイプ】" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/product/103107973_th.jpg?cmsp_timestamp=20180606224631"/>
</a>
<a href="?pid=103107973"><img class="new_mark_img1" src="https://img.shop-pro.jp/img/new/icons15.gif" style="border:none;display:inline;margin:0px;padding:0px;width:auto;"/>い草ラグ　インディゴ（デニム＆い草）　【コンパクト収納タイプ】</a>
                      デニムとい草のコンビネーションラグです。コンパクト収納タイプ。１８０ｘ１８０ｃｍ、１８０ｘ２４０ｃｍ。
                                SOLD OUT
                  </li>
</ul>
</div>
<div class="mar_b_50">
<h2 class="pad_l_10 mar_b_20 txt_28 txt_fwn bor_b_1">売れ筋商品</h2>
<ul class="row unstyled">
<li class="col col-xs-12 col-lg-4 seller-unit pad_20 mar_b_30 txt_c">
<a href="?pid=136285463">
<img alt="&lt;img class='new_mark_img1' src='https://img.shop-pro.jp/img/new/icons15.gif' style='border:none;display:inline;margin:0px;padding:0px;width:auto;' /&gt;ユニット畳８２ｘ８２ｃｍ「葵」【スベリ止め加工、フェルトボード使用】" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/product/136285463_th.jpg?cmsp_timestamp=20181022231139"/>
</a>
<a href="?pid=136285463"><img class="new_mark_img1" src="https://img.shop-pro.jp/img/new/icons15.gif" style="border:none;display:inline;margin:0px;padding:0px;width:auto;"/>ユニット畳８２ｘ８２ｃｍ「葵」【スベリ止め加工、フェルトボード使用】</a>
                      フローリングに敷くだけの簡単畳です。組み合わせは、自由自在。
                                            1,620円(内税)
                              </li>
<li class="col col-xs-12 col-lg-4 seller-unit pad_20 mar_b_30 txt_c">
<a href="?pid=136285861">
<img alt="&lt;img class='new_mark_img1' src='https://img.shop-pro.jp/img/new/icons15.gif' style='border:none;display:inline;margin:0px;padding:0px;width:auto;' /&gt;ユニット畳８２ｘ１６４ｃｍ「葵」【スベリ止め加工、フェルトボード使用】" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/product/136285861_th.jpg?cmsp_timestamp=20181022232933"/>
</a>
<a href="?pid=136285861"><img class="new_mark_img1" src="https://img.shop-pro.jp/img/new/icons15.gif" style="border:none;display:inline;margin:0px;padding:0px;width:auto;"/>ユニット畳８２ｘ１６４ｃｍ「葵」【スベリ止め加工、フェルトボード使用】</a>
                      フローリングに敷くだけの簡単畳です。組み合わせは、自由自在。
                                            3,240円(内税)
                              </li>
<li class="col col-xs-12 col-lg-4 seller-unit pad_20 mar_b_30 txt_c">
<a href="?pid=136285930">
<img alt="&lt;img class='new_mark_img1' src='https://img.shop-pro.jp/img/new/icons15.gif' style='border:none;display:inline;margin:0px;padding:0px;width:auto;' /&gt;軽量厚床ふっくらユニット畳約８２ｘ８２ｃｍ「極」【スベリ止め加工、発泡エチレンボード】" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/product/136285930_th.jpg?cmsp_timestamp=20181022234135"/>
</a>
<a href="?pid=136285930"><img class="new_mark_img1" src="https://img.shop-pro.jp/img/new/icons15.gif" style="border:none;display:inline;margin:0px;padding:0px;width:auto;"/>軽量厚床ふっくらユニット畳約８２ｘ８２ｃｍ「極」【スベリ止め加工、発泡エチレンボード】</a>
                      フローリングに敷くだけの簡単畳です。組み合わせは、自由自在。
                                            2,600円(内税)
                              </li>
<li class="col col-xs-12 col-lg-4 seller-unit pad_20 mar_b_30 txt_c">
<a href="?pid=136285981">
<img alt="&lt;img class='new_mark_img1' src='https://img.shop-pro.jp/img/new/icons15.gif' style='border:none;display:inline;margin:0px;padding:0px;width:auto;' /&gt;軽量厚床ふっくらユニット畳約８２ｘ１６４ｃｍ「極」【スベリ止め加工、発泡エチレンボード】" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/product/136285981_th.jpg?cmsp_timestamp=20181022235141"/>
</a>
<a href="?pid=136285981"><img class="new_mark_img1" src="https://img.shop-pro.jp/img/new/icons15.gif" style="border:none;display:inline;margin:0px;padding:0px;width:auto;"/>軽量厚床ふっくらユニット畳約８２ｘ１６４ｃｍ「極」【スベリ止め加工、発泡エチレンボード】</a>
                      フローリングに敷くだけの簡単畳です。組み合わせは、自由自在。
                                            3,900円(内税)
                              </li>
<li class="col col-xs-12 col-lg-4 seller-unit pad_20 mar_b_30 txt_c">
<a href="?pid=23240039">
<img alt="&lt;img class='new_mark_img1' src='https://img.shop-pro.jp/img/new/icons15.gif' style='border:none;display:inline;margin:0px;padding:0px;width:auto;' /&gt;ダイニングラグ（クッションフロアーラグ）：木目調（ナチュラル色）　【日本製】" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/product/23240039_th.jpg?cmsp_timestamp=20180606225406"/>
</a>
<a href="?pid=23240039"><img class="new_mark_img1" src="https://img.shop-pro.jp/img/new/icons15.gif" style="border:none;display:inline;margin:0px;padding:0px;width:auto;"/>ダイニングラグ（クッションフロアーラグ）：木目調（ナチュラル色）　【日本製】</a>
                      汚れがさっとふけるダイニングラグです。<br/>１畳用：１００ｘ１５０ｃｍ　<br/>２畳用：１８２ｘ１８０ｃｍ　<br/>２畳用：２００ｘ２００ｃｍ　<br/>３畳用大：２００ｘ２５０ｃｍ
                                            4,280円(内税)
                              </li>
<li class="col col-xs-12 col-lg-4 seller-unit pad_20 mar_b_30 txt_c">
<a href="?pid=23243248">
<img alt="&lt;img class='new_mark_img1' src='https://img.shop-pro.jp/img/new/icons15.gif' style='border:none;display:inline;margin:0px;padding:0px;width:auto;' /&gt;ダイニングラグ（クッションフロアーラグ）：木目調（ブラウン色）　【日本製】" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/product/23243248_th.jpg?cmsp_timestamp=20180606225647"/>
</a>
<a href="?pid=23243248"><img class="new_mark_img1" src="https://img.shop-pro.jp/img/new/icons15.gif" style="border:none;display:inline;margin:0px;padding:0px;width:auto;"/>ダイニングラグ（クッションフロアーラグ）：木目調（ブラウン色）　【日本製】</a>
                      ふけるダイニングラグです。<br/>２畳用：１８２ｘ１８０ｃｍ　<br/>３畳用：１８２ｘ２３０ｃｍ　<br/>３畳用：２００ｘ２５０ｃｍ
                                            4,280円(内税)
                              </li>
<li class="col col-xs-12 col-lg-4 seller-unit pad_20 mar_b_30 txt_c">
<a href="?pid=12374315">
<img alt="&lt;img class='new_mark_img1' src='https://img.shop-pro.jp/img/new/icons15.gif' style='border:none;display:inline;margin:0px;padding:0px;width:auto;' /&gt;い草上敷　エコノミータイプ＃１　江戸間１畳(87x174cm)　【防ダニ・防カビ加工】" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/product/12374315_th.jpg?cmsp_timestamp=20180606231132"/>
</a>
<a href="?pid=12374315"><img class="new_mark_img1" src="https://img.shop-pro.jp/img/new/icons15.gif" style="border:none;display:inline;margin:0px;padding:0px;width:auto;"/>い草上敷　エコノミータイプ＃１　江戸間１畳(87x174cm)　【防ダニ・防カビ加工】</a>
                      エコノミータイプ。中国製双目織り上敷です。江戸間１畳〜１０畳まであります。
                                            980円(内税)
                              </li>
<li class="col col-xs-12 col-lg-4 seller-unit pad_20 mar_b_30 txt_c">
<a href="?pid=7147682">
<img alt="&lt;img class='new_mark_img1' src='https://img.shop-pro.jp/img/new/icons24.gif' style='border:none;display:inline;margin:0px;padding:0px;width:auto;' /&gt;ユニット畳　８２ｘ８２ｃｍ：半畳（１．２ｃｍ厚）" class="show mar_auto mar_b_10" src="https://img08.shop-pro.jp/PA01036/372/product/7147682_th.jpg?cmsp_timestamp=20170225110704"/>
</a>
<a href="?pid=7147682"><img class="new_mark_img1" src="https://img.shop-pro.jp/img/new/icons24.gif" style="border:none;display:inline;margin:0px;padding:0px;width:auto;"/>ユニット畳　８２ｘ８２ｃｍ：半畳（１．２ｃｍ厚）</a>
                      フローリングに敷くだけの簡単畳です。組み合わせは、自由自在。
                                            1,380円(内税)
                              </li>
</ul>
</div>
<div class="row mar_b_50">
<div class="col col-lg-12">
      ■ご確認メールについて■ ──────────<br/>
ご注文いただきますと、必ず自動確認メールをお送りさせていただきます。<br/>
ご注文後、メールが届かない場合は、メールアドレス入力の入力が間違っていると思われます。<br/>
その場合、お問い合わせメールを至急お送りください。<br/>
■商品のお色について■ ──────────<br/>
実際の商品の色目に出来るだけ近く表示しておりますが、モニター等によって多少色合いが異なって見える場合がございますので、ご了承ください。<br/>
■在庫数について■ ────────────<br/>
在庫数は、少し前の状況を参考に表示しているため、ご注文時には、表示と異なる場合もございますが、ご了承ください。<br/>
小売店への卸販売も行っておりますので、在庫数に余裕がある商品でもすぐに売り切れとなる場合もございますが、ご理解の上、お早めのご購入をお願いいたします。<br/>
■お支払いについて■ ────────────<br/>
カード決済(VISA,MASTER,DINERS)、銀行振込、イーバンク決済、代引決済とあります。代引決済『代金引換』にしていただきますと、佐川急便のｅコレクトサービスがご利用いただき、商品を配達する際に複数のクレジットカードや電子マネー、デビットカードでの決済が可能です。<br/>
■配達について■ ────────────<br/>
基本的には、翌日の発送とさせて頂きます。ただ、お急ぎの場合や早めのお時間でのご注文であれば当日の発送も可能です。休日の前後につきましては、出荷が遅れる可能性があります。当社営業日のカレンダーをご参考にしください。<br/>
■不良品について■ ────────────<br/>
当社では、出荷前に自社で検品、検針作業を行っておりますが、もしもこれは不良品？と思われた場合はすぐにご連絡ください。<br/>
■お問い合わせ、お便り■ ────────────<br/>
みなさまのご希望に出来るだけお答えさせていただいております。あたらしお客さまの為に、いままでのお問い合わせ、お便りをご紹介させていただいております。ご参考にしてください。なお、お問い合わせ、お便りは当社にて抜粋して匿名にて掲載させていただく場合もございます事をご理解をお願いいたします。<br/>
</div>
</div>
<script>
  $(window).load(function () {
    $('.recommend-unit').tile();
    $('.seller-unit').tile();
  });
  $(window).resize(function () {
    $('.recommend-unit').tile();
    $('.seller-unit').tile();
  });
</script>
</div>
<div class="col col-lg-3 col-sm-12 mar_t_20 mar_b_50" id="side">
<form action="https://satomen.com/" class="mar_b_50" method="GET">
<input name="mode" type="hidden" value="srh"/>
<select class="mar_b_10" name="cid">
<option value="">カテゴリーを選択</option>
<option value="245980,0">折りたたみｶｰﾍﾟｯﾄ</option>
<option value="1236471,0">丸巻カーペット</option>
<option value="841450,0">中敷ラグｶｰﾍﾟｯﾄ</option>
<option value="1385504,0">ダイニングラグ</option>
<option value="251189,0">クッション</option>
<option value="251190,0">マット各種</option>
<option value="1380545,0">ジョイントマット</option>
<option value="454328,0">マルチカバー</option>
<option value="1630138,0">座椅子</option>
<option value="501151,0">い草 - 上敷 -</option>
<option value="472667,0">い草 - カーペット -</option>
<option value="472668,0">い草　-　ラグ　-</option>
<option value="472670,0">い草 - クッション -</option>
<option value="472676,0">い草　-枕、寝ござ-</option>
<option value="472675,0">い草　-　雑貨　-</option>
<option value="2265312,0">ＰＰ- ﾗｸﾞ･ｶｰﾍﾟｯﾄ -</option>
<option value="423955,0">竹 -  bamboo  -</option>
<option value="307859,0">籐 - rattan ラタン -</option>
<option value="1922728,0">紙−Paper−</option>
<option value="307933,0">あったら便利物</option>
</select>
<input name="keyword" type="text"/>
<button class="btn btn-xs"><i class="icon-lg-b icon-search va-35"></i><span class="visible-phone mar_l_5 pad_r_5 txt_14">SEARCH</span></button>
</form>
<div>
<h3 class="pad_l_10">カテゴリーから探す</h3>
<ul class="unstyled bor_b_1 mar_b_50">
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=245980&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/245980_0.jpg?cmsp_timestamp=20111102072414"/>
                                    折りたたみｶｰﾍﾟｯﾄ
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=1236471&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/1236471_0.jpg?cmsp_timestamp=20120712225951"/>
                                    丸巻カーペット
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=841450&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/841450_0.jpg?cmsp_timestamp=20120330182621"/>
                                    中敷ラグｶｰﾍﾟｯﾄ
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=1385504&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/1385504_0.jpg?cmsp_timestamp=20121013161414"/>
                                    ダイニングラグ
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=251189&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/251189_0.jpg?cmsp_timestamp=20121013161209"/>
                                    クッション
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=251190&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/251190_0.jpg?cmsp_timestamp=20121013161209"/>
                                    マット各種
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=1380545&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/1380545_0.jpg?cmsp_timestamp=20121013161209"/>
                                    ジョイントマット
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=454328&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/454328_0.jpg?cmsp_timestamp=20121013161209"/>
                                    マルチカバー
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=1630138&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/1630138_0.jpg?cmsp_timestamp=20131005185652"/>
                                    座椅子
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=501151&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/501151_0.jpg?cmsp_timestamp=20131005185456"/>
                                    い草 - 上敷 -
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=472667&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/472667_0.jpg?cmsp_timestamp=20131005185456"/>
                                    い草 - カーペット -
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=472668&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/472668_0.jpg?cmsp_timestamp=20131005185456"/>
                                    い草　-　ラグ　-
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=472670&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/472670_0.jpg?cmsp_timestamp=20131005185456"/>
                                    い草 - クッション -
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=472676&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/472676_0.jpg?cmsp_timestamp=20131005185456"/>
                                    い草　-枕、寝ござ-
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=472675&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/472675_0.jpg?cmsp_timestamp=20131005185456"/>
                                    い草　-　雑貨　-
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=2265312&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/2265312_0.jpg?cmsp_timestamp=20170306104216"/>
                                    ＰＰ- ﾗｸﾞ･ｶｰﾍﾟｯﾄ -
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=423955&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/423955_0.jpg?cmsp_timestamp=20170306104020"/>
                                    竹 -  bamboo  -
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=307859&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/307859_0.jpg?cmsp_timestamp=20170306104020"/>
                                    籐 - rattan ラタン -
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=1922728&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/1922728_0.jpg?cmsp_timestamp=20170306104020"/>
                                    紙−Paper−
                </a>
</li>
<li class="pad_10 bor_t_1">
<a class="show txt_c_333" href="https://satomen.com/?mode=cate&amp;cbid=307933&amp;csid=0">
<img class="show hidden-phone mar_b_5" src="https://img08.shop-pro.jp/PA01036/372/category/307933_0.jpg?cmsp_timestamp=20170306104020"/>
                                    あったら便利物
                </a>
</li>
</ul>
</div>
<!-- グループリスト
                        <div>
            <h3 class="pad_l_10">グループから探す</h3>
            <ul class="unstyled bor_b_1 mar_b_50">
                    <li class="pad_10 bor_t_1">
              <a href="https://satomen.com/?mode=grp&gid=18769" class="show txt_c_333">
                                  <img src="https://img08.shop-pro.jp/PA01036/372/category/g_18769.jpg?cmsp_timestamp=20120202142738" class="show hidden-phone mar_b_5" />
                                ベルギー製
              </a>
            </li>
                                  <li class="pad_10 bor_t_1">
              <a href="https://satomen.com/?mode=grp&gid=19105" class="show txt_c_333">
                                  <img src="https://img08.shop-pro.jp/PA01036/372/category/g_19105.jpg?cmsp_timestamp=20120202142738" class="show hidden-phone mar_b_5" />
                                エジプト製
              </a>
            </li>
                                  <li class="pad_10 bor_t_1">
              <a href="https://satomen.com/?mode=grp&gid=34412" class="show txt_c_333">
                                アウトレット激安処分
              </a>
            </li>
                    </ul>
          </div>
                 グループリスト -->
<div>
<h3 class="pad_l_10">コンテンツ</h3>
<ul class="unstyled bor_b_1 mar_b_50">
<a href="http://satomen.com/?mode=f7"><img alt="" src="https://img08.shop-pro.jp/PA01036/372/etc/igusa_rink.jpg?cmsp_timestamp=20170111175107"/></a>
<br/>
<a href="http://satomen.com/?mode=f12"><img alt="花ござ特集" src="https://img08.shop-pro.jp/PA01036/372/etc/hanagoza_link_s.gif?cmsp_timestamp=20170111175213"/> </a>
<li class="pad_10 bor_t_1"><a class="show txt_c_333" href="https://satomen.com/?mode=f1">商品のお手入れについて</a></li>
<li class="pad_10 bor_t_1"><a class="show txt_c_333" href="https://satomen.com/?mode=f2">商品の特色説明</a></li>
<li class="pad_10 bor_t_1"><a class="show txt_c_333" href="https://satomen.com/?mode=f3">お客さまからのご感想のお便り</a></li>
<li class="pad_10 bor_t_1"><a class="show txt_c_333" href="https://satomen.com/?mode=f4">アフェリエイトリンクの説明</a></li>
<li class="pad_10 bor_t_1"><a class="show txt_c_333" href="https://satomen.com/?mode=f6">今月のプレゼントのご案内</a></li>
<li class="pad_10 bor_t_1"><a class="show txt_c_333" href="https://satomen.com/?mode=f7">さるるの部屋〜イグサ特集〜</a></li>
<li class="pad_10 bor_t_1"><a class="show txt_c_333" href="https://satomen.com/?mode=f8">お客様からのお問い合わせのお便り紹介</a></li>
<li class="pad_10 bor_t_1"><a class="show txt_c_333" href="https://satomen.com/?mode=f9">カーペット特集</a></li>
<li class="pad_10 bor_t_1"><a class="show txt_c_333" href="https://satomen.com/?mode=f10">会社概要</a></li>
<li class="pad_10 bor_t_1"><a class="show txt_c_333" href="https://satomen.com/?mode=f11">さるるのこだわり</a></li>
<li class="pad_10 bor_t_1"><a class="show txt_c_333" href="https://satomen.com/?mode=f15">ホームページ更新履歴</a></li>
<li class="pad_10 bor_t_1"><a class="show txt_c_333" href="http://sarurunoheya.jugem.jp/">ショップブログ</a></li> <li class="pad_10 bor_t_1"><a class="show txt_c_333" href="https://satomen.com/?mode=sk#payment">お支払い方法について</a></li>
<li class="pad_10 bor_t_1"><a class="show txt_c_333" href="https://satomen.com/?mode=sk">配送方法・送料について</a></li>
</ul>
</div>
<div class="hidden-phone mar_b_50">
<h3 class="pad_l_10">モバイルショップ</h3>
<img class="show mar_auto" src="https://img08.shop-pro.jp/PA01036/372/qrcode.jpg?cmsp_timestamp=20201228101521"/>
</div>
<div class="hidden-phone mar_b_50">
<h3 class="pad_l_10">ショップについて</h3>
<img class="show mar_auto" src="https://img08.shop-pro.jp/PA01036/372/PA01036372_m.gif?cmsp_timestamp=20201228101521"/>
<p class="txt_fwb txt_c mar_t_10 mar_b_10">佐藤　誠治</p>
<p>
            お客さまの、満足して、よろこんだ、笑顔のために！！
<br/>
<a href="http://sarurunoheya.jugem.jp/" target="_blank">店長日記</a>
<br/><br/>
<a href="http://twitter.com/#!/bananaroom">【@bananaroom(saruru's room)】</a>
でもつぶやいてます。
          </p>
</div>
</div>
</div>
<div class="pad_v_30 bor_t_1 txt_c" id="footer">
<ul class="inline">
<li><a class="txt_c_333" href="./">ホーム</a></li>
<li><a class="txt_c_333" href="https://satomen.com/?mode=sk">支払・配送について</a></li>
<li><a class="txt_c_333" href="https://satomen.com/?mode=sk#info">特定商取引法に基づく表記</a></li>
<li><a class="txt_c_333" href="https://satomen.com/?mode=privacy">プライバシーポリシー</a></li>
<li><a class="txt_c_333" href="https://satomen.shop-pro.jp/secure/?mode=inq&amp;shop_id=PA01036372">お問い合わせ</a></li>
</ul>
<p></p><address>Copyright(c) SATO MENGYO CO.,LTD. All Right Reserved</address>
</div>
</div>
<script>
  $(function () {
    // viewport
    var viewport = document.createElement('meta');
    viewport.setAttribute('name', 'viewport');
    viewport.setAttribute('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0');
    document.getElementsByTagName('head')[0].appendChild(viewport);

    function window_size_switch_func() {
      if($(window).width() >= 768) {
        // product option switch -> table
        $('#prd-opt-table').html($('.prd-opt-table').html());
        $('#prd-opt-table table').addClass('table table-bordered');
        $('#prd-opt-select').empty();
      } else {
        // product option switch -> select
        $('#prd-opt-table').empty();
        if($('#prd-opt-select > *').size() == 0) {
          $('#prd-opt-select').append($('.prd-opt-select').html());
        }
      }
    }
    window_size_switch_func();
    $(window).load(function () {
      $('.history-unit').tile();
    });
    $(window).resize(function () {
      window_size_switch_func();
      $('.history-unit').tile();
    });
  });
</script> <script src="https://satomen.com/js/cart.js" type="text/javascript"></script>
<script src="https://satomen.com/js/async_cart_in.js" type="text/javascript"></script>
<script src="https://satomen.com/js/product_stock.js" type="text/javascript"></script>
<script src="https://satomen.com/js/js.cookie.js" type="text/javascript"></script>
<script src="https://satomen.com/js/favorite_button.js" type="text/javascript"></script>
</body></html>