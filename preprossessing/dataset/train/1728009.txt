<!DOCTYPE html>
<html dir="ltr" lang="uk-ua">
<head>
<meta charset="utf-8"/>
<title>Помилка: 404 Категорію не знайдено</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"/>
<style>
			h1, h2, h3, h4, h5, h6, .site-title {
				font-family: 'Open Sans', sans-serif;
			}
		</style>
<link href="/templates/protostar2/css/template.css" rel="stylesheet"/>
<link href="/templates/protostar2/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<style>
			body.site {
				border-top: 3px solid #9d6745;
				background-color: #f0e1d8			}
			a {
				color: #9d6745;
			}
			.navbar-inner, .nav-list > .active > a, .nav-list > .active > a:hover, .dropdown-menu li > a:hover, .dropdown-menu .active > a, .dropdown-menu .active > a:hover, .nav-pills > .active > a, .nav-pills > .active > a:hover {
				background: #9d6745;
			}
			.navbar-inner {
				-moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
				-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
				box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
			}
		</style>
<!--[if lt IE 9]><script src="/media/jui/js/html5.js"></script><![endif]-->
</head>
<body class="site com_content view-category no-layout no-task itemid-101 fluid">
<!-- Body -->
<div class="body">
<div class="container-fluid">
<!-- Header -->
<header class="header" role="banner">
<div class="header-inner clearfix">
<a class="brand pull-left" href="/">
<img alt="Туристичний інформаційний центр. Жовква. Львів. Кравець Любомир" src="https://zhovkva-tour.info/images/logo.-.jpg"/> </a>
<div class="header-search pull-right">
</div>
</div>
</header>
<div class="navigation">
</div>
<!-- Banner -->
<div class="banner">
</div>
<div class="row-fluid">
<div class="span12" id="content">
<!-- Begin Content -->
<h1 class="page-header">Запитану сторінку не знайдено.</h1>
<div class="well">
<div class="row-fluid">
<div class="span6">
<p><strong>Виникла помилка при обробці вашого запиту.</strong></p>
<p>У Вас відсутні <strong>права</strong> для перегляду цієї сторінки, оскільки:</p>
<ul>
<li><strong>застарілих закладок/вибраного</strong></li>
<li><strong>невірно введена адреса</strong></li>
<li>пошукова система виявила <strong>застарілі записи на цьому сайті</strong></li>
<li>у Вас відсутній <strong>доступ</strong> до цієї сторінки</li>
</ul>
</div>
<div class="span6">
<p><strong>Ви можете здійснити пошук на сайті або відвідати головну сторінку.</strong></p>
<p>Пошук по сайту</p>
<div class="search">
<form action="/" class="form-inline" method="post" role="search">
<label class="element-invisible" for="mod-search-searchword0">Пошук...</label> <input class="inputbox search-query input-medium" id="mod-search-searchword0" maxlength="200" name="searchword" placeholder="Пошук..." type="search"/> <input name="task" type="hidden" value="search"/>
<input name="option" type="hidden" value="com_search"/>
<input name="Itemid" type="hidden" value="101"/>
</form>
</div>
<p>Перейти на головну сторінку</p>
<p><a class="btn" href="/index.php"><span aria-hidden="true" class="icon-home"></span> Головна сторінка</a></p>
</div>
</div>
<hr/>
<p>Якщо у Вас виникли труднощі, будь ласка, зв'яжіться з адміністратором сайту</p>
<blockquote>
<span class="label label-inverse">404</span> Категорію не знайдено													</blockquote>
</div>
<!-- End Content -->
</div>
</div>
</div>
</div>
<!-- Footer -->
<div class="footer">
<div class="container-fluid">
<hr/>
<p class="pull-right">
<a href="#top" id="back-top">
					Back to Top				</a>
</p>
<p>
				© 2021 Туристичний інформаційний центр. Жовква. Львів. Кравець Любомир			</p>
</div>
</div>
</body>
</html>
