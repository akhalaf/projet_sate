<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8 no-js" lang="en-US" prefix="og: http://ogp.me/ns#"> <![endif]--><!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en-US" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<!--[if lte IE 8]><meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=IE8" /><![endif]-->
<title>The Original Bach Flower Remedies - www.BachFlower.com</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<link href="https://www.bachflower.com/wordpress/xmlrpc.php" rel="pingback"/>
<!-- This site is optimized with the Yoast SEO plugin v9.1 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="INFORMATION &amp; SALE of the Original Bach Flower Remedies, Rescue Remedy, Rescue Sleep, Rescue Remedy Pets. Wholesale Customer Service 1-800-214-2850" name="description"/>
<link href="https://www.bachflower.com/" rel="canonical"/>
<link href="http://www.bachflower.com/page/2/" rel="next"/>
<meta content="en_US" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="The Original Bach Flower Remedies - www.BachFlower.com" property="og:title"/>
<meta content="INFORMATION &amp; SALE of the Original Bach Flower Remedies, Rescue Remedy, Rescue Sleep, Rescue Remedy Pets. Wholesale Customer Service 1-800-214-2850" property="og:description"/>
<meta content="https://www.bachflower.com/" property="og:url"/>
<meta content="Information &amp; Sales - The Original Bach Flower Remedies " property="og:site_name"/>
<script type="application/ld+json">{"@context":"https:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/www.bachflower.com\/","name":"Information &amp; Sales - The Original Bach Flower Remedies ","potentialAction":{"@type":"SearchAction","target":"https:\/\/www.bachflower.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<!-- / Yoast SEO plugin. -->
<link href="//maps.googleapis.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.bachflower.com/feed/" rel="alternate" title="Information &amp; Sales - The Original Bach Flower Remedies  » Feed" type="application/rss+xml"/>
<link href="https://www.bachflower.com/comments/feed/" rel="alternate" title="Information &amp; Sales - The Original Bach Flower Remedies  » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.bachflower.com\/wordpress\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.bachflower.com/wordpress/wp-includes/css/dist/block-library/style.min.css?ver=5.2.9" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bachflower.com/wordpress/wp-content/plugins/column-shortcodes//assets/css/shortcodes.css?ver=1.0" id="cpsh-shortcodes-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bachflower.com/wordpress/wp-content/plugins/document-gallery/assets/css/style.min.css?ver=4.4.2" id="document-gallery-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bachflower.com/wordpress/wp-content/plugins/hupso-share-buttons-for-twitter-facebook-google/style.css?ver=5.2.9" id="hupso_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bachflower.com/wordpress/wp-content/plugins/social-media-widget/social_widget.css?ver=5.2.9" id="social-widget-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bachflower.com/wordpress/wp-content/themes/risen/style.css?ver=1.1.1" id="risen-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bachflower.com/wordpress/wp-content/themes/risen/styles/light/style.css?ver=1.1.1" id="risen-base-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700" id="google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
			ajaxurl = typeof(ajaxurl) !== 'string' ? 'https://www.bachflower.com/wordpress/wp-admin/admin-ajax.php' : ajaxurl;
		</script>
<script src="https://www.bachflower.com/wordpress/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.bachflower.com/wordpress/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.bachflower.com/wordpress/wp-content/themes/risen/js/css3-mediaqueries.js?ver=1.1.1" type="text/javascript"></script>
<script src="https://www.bachflower.com/wordpress/wp-content/themes/risen/js/modernizr.custom.js?ver=1.1.1" type="text/javascript"></script>
<script src="https://www.bachflower.com/wordpress/wp-content/themes/risen/js/jquery.backstretch.min.js?ver=1.1.1" type="text/javascript"></script>
<script src="https://www.bachflower.com/wordpress/wp-content/themes/risen/js/hoverIntent.js?ver=1.1.1" type="text/javascript"></script>
<script src="https://www.bachflower.com/wordpress/wp-content/themes/risen/js/superfish.js?ver=1.1.1" type="text/javascript"></script>
<script src="https://www.bachflower.com/wordpress/wp-content/themes/risen/js/supersubs.js?ver=1.1.1" type="text/javascript"></script>
<script src="https://www.bachflower.com/wordpress/wp-content/themes/risen/js/selectnav.min.js?ver=1.1.1" type="text/javascript"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=&amp;sensor=false" type="text/javascript"></script>
<script src="https://www.bachflower.com/wordpress/wp-content/themes/risen/js/jquery.flexslider-min.js?ver=1.1.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var risen_wp = {"theme_uri":"https:\/\/www.bachflower.com\/wordpress\/wp-content\/themes\/risen","is_home":"1","site_url":"https:\/\/www.bachflower.com\/wordpress","home_url":"https:\/\/www.bachflower.com","mobile_menu_label":"Menu","slider_enabled":"1","slider_slideshow":"1","slider_speed":"6000","gmaps_api_key":"","ajax_url":"https:\/\/www.bachflower.com\/wordpress\/wp-admin\/admin-ajax.php","contact_form_nonce":"637e4cd410","comment_name_required":"1","comment_email_required":"1","comment_name_error_required":"Required","comment_email_error_required":"Required","comment_email_error_invalid":"Invalid Email","comment_url_error_invalid":"Invalid URL","comment_message_error_required":"Comment Required","lightbox_prev":"Prev","lightbox_next":"Next","lightbox_expand":"Expand","lightbox_close":"Close"};
/* ]]> */
</script>
<script src="https://www.bachflower.com/wordpress/wp-content/themes/risen/js/main.js?ver=1.1.1" type="text/javascript"></script>
<link href="https://www.bachflower.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.bachflower.com/wordpress/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.bachflower.com/wordpress/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.2.9" name="generator"/>
<!-- <meta name="NextGEN" version="3.0.16" /> -->
<style type="text/css">
body {
	background: #f0eaa8;
	
}

a, .flex-caption a {
	color: #5c2907;
}

#header-menu, #footer-bottom, .flex-caption, .flex-control-nav li a.active, #home-row-widgets .widget-image-title, #page-header h1, .sidebar-widget-title {
	background-color: #8bdb77;
}

body, input, textarea, select, .multimedia-short h1, #cancel-comment-reply-link, .accordion-section-title {
	font-family: 'Open Sans', Arial, Helvetica, sans-serif;
}

#header-menu-links, .flex-caption, #home-row-widgets .widget-image-title, #page-header h1, h1.sidebar-widget-title, a.button, a.comment-reply-link, a.comment-edit-link, a.post-edit-link, .nav-left-right a, input[type=submit] {
	font-family: 'Open Sans', Arial, Helvetica, sans-serif;
}

.heading, .page-title, .post-content h1, .post-content h2, .post-content h3, .post-content h4, .post-content h5, .post-content h6, .author-box h1, .staff header h1, .location header h1, #reply-title, #comments-title, .home-column-widgets-title, .ppt, #tagline, #intro {
	font-family: 'Open Sans', Arial, Helvetica, sans-serif;
}
</style>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<style media="all" type="text/css">
/* <![CDATA[ */
@import url("https://www.bachflower.com/wordpress/wp-content/plugins/wp-table-reloaded/css/plugin.css?ver=1.9.4");
@import url("https://www.bachflower.com/wordpress/wp-content/plugins/wp-table-reloaded/css/datatables.css?ver=1.9.4");
.wp-table-reloaded-id-3 td {
  font-size: 12px;
}

.wp-table-reloaded-id-4 td {
  font-size: 12px;
}
/* ]]> */
</style><link href="https://www.bachflower.com/wordpress/wp-content/uploads/2018/04/Bach-Signature-B-54x55.gif" rel="icon" sizes="32x32"/>
<link href="https://www.bachflower.com/wordpress/wp-content/uploads/2018/04/Bach-Signature-B.gif" rel="icon" sizes="192x192"/>
<link href="https://www.bachflower.com/wordpress/wp-content/uploads/2018/04/Bach-Signature-B.gif" rel="apple-touch-icon-precomposed"/>
<meta content="https://www.bachflower.com/wordpress/wp-content/uploads/2018/04/Bach-Signature-B.gif" name="msapplication-TileImage"/>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37427814-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body class="home blog">
<!-- Container Start -->
<div id="container">
<div id="container-inner">
<!-- Header Start -->
<header id="header">
<div id="header-content">
<div id="logo">
<a href="https://www.bachflower.com"><img alt="Information &amp; Sales - The Original Bach Flower Remedies " src="http://www.bachflower.com/wordpress/wp-content/uploads/2013/04/BFLOGOnotrade-copy.png"/></a>
</div>
<div id="top-right">
<div id="top-right-inner">
<div id="top-right-content">
<div id="tagline">
									Bach Flower is a Trusted Brand for nearly 90 Years								</div>
</div>
</div>
</div>
</div>
<!-- Menu Start -->
<nav id="header-menu">
<div id="header-menu-inner">
<ul class="sf-menu" id="header-menu-links"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-56" id="menu-item-56"><a href="http://bachflower.com" title="Bach Flower Information, Sale, Wholesale">HOME</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-212" id="menu-item-212"><a title="Bach Flower Remedies">BACH PRODUCTS</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-304" id="menu-item-304"><a href="https://www.bachflower.com/original-bach-flower-remedies/" title="THE 38 BACH REMEDIES">THE 38 BACH REMEDIES</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-558" id="menu-item-558"><a href="#" title="RESCUE REMEDIES PRODUCTS">RESCUE REMEDY PRODUCTS</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-299" id="menu-item-299"><a href="https://www.bachflower.com/alcohol-free-bach-flower-remedies/" title="Alcohol Free Bach Flower Remedies">ALCOHOL FREE</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-301" id="menu-item-301"><a href="https://www.bachflower.com/rescue-remedy-information/" title="Rescue Remedy">Rescue Remedy</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-300" id="menu-item-300"><a href="https://www.bachflower.com/rescue-remedy-pet/" title="Misc. Rescue Products">Rescue Remedy Pets</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-290" id="menu-item-290"><a href="https://www.bachflower.com/bach-rescue-sleep/" title="Rescue Sleep &amp; Melt">Rescue Sleep &amp; Melt</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-291" id="menu-item-291"><a href="https://www.bachflower.com/rescue-cream/" title="Rescue Cream, Gel &amp; Balm">Rescue Cream</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-292" id="menu-item-292"><a href="https://www.bachflower.com/bach-rescue-pastilles/" title="Rescue Pastilles">Rescue Pastilles</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1653" id="menu-item-1653"><a href="https://www.bachflower.com/rescue-pearls/">Rescue Pearls</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1976" id="menu-item-1976"><a href="https://www.bachflower.com/rescue-plus/">Rescue PLUS</a></li>
</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-557" id="menu-item-557"><a title="Bach Flower Information">MORE INFO</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-272" id="menu-item-272"><a href="https://www.bachflower.com/faq-bach-flower-remedies-rescue/" title="FAQ about Bach Flower Remedies">FAQ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1654" id="menu-item-1654"><a href="https://www.bachflower.com/how-to-mix-bach-flower-remedies/">How to take the Remedies</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-524" id="menu-item-524"><a href="https://www.bachflower.com/rescue-remedy-pets-bach-flower/" title="Bach for Animals">Bach Animals</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-525" id="menu-item-525"><a href="https://www.bachflower.com/bach-flower-rescue-remedy-children-kids/" title="Bach For Children">Bach For Kids</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-529" id="menu-item-529"><a href="https://www.bachflower.com/the-7-bach-groups/" title="The 7 Bach Flower Groups">The 7 Bach Flower Groups</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-527" id="menu-item-527"><a href="https://www.bachflower.com/cure-negative-emotions-bach-flower-remedies/" title="Cure Negative Emotions with Bach">Cure Negative Emotions</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-528" id="menu-item-528"><a href="https://www.bachflower.com/bach-flower-education-literature/" title="Bach Educational Material">Educational Bach Material</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1651" id="menu-item-1651"><a href="https://www.bachflower.com/compare-remedies/">Compare Remedies</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-526" id="menu-item-526"><a href="https://www.bachflower.com/bach-flower-consultation/" title="Bach Flower Consultation">Bach Flower Consultation</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-541" id="menu-item-541"><a href="https://www.bachflower.com/las-flores-de-bach/" title="Flores de Bach">Flores de Bach</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52" id="menu-item-52"><a href="https://www.bachflower.com/bach-flower-rescue-remedy-success-stories/" title="BACH SUCCESS STORIES">SUCCESS STORIES</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55" id="menu-item-55"><a href="https://www.bachflower.com/bach-blog/" title="BACH FLOWER BLOG">BLOG</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-26" id="menu-item-26"><a href="https://www.bachflower.com/contact-bach-flower/" title="Contact Bach Flower ">CONTACT US</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-427" id="menu-item-427"><a href="https://www.bachflower.com/customer-service-bach-flower/" title="Bach Customer Service">Customer Service</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-636" id="menu-item-636"><a href="https://www.bachflower.com/contact-bach-flower/bach-flower-wholesale-contact/" title="Bach Flower Wholesale ">Wholesale Support and Products</a></li>
</ul>
</li>
</ul>
<ul class="risen-icon-list light" id="header-icons">
<li><a class="single-icon facebook-icon light-icon" href="https://www.facebook.com/OriginalBachFlower/" target="_blank">Facebook</a></li>
</ul>
<div class="clear"></div>
</div>
<div id="header-menu-bottom"></div>
</nav>
<!-- Menu End -->
</header>
<!-- Header End -->
<div class="home-column-widgets-both">
<div id="slider">
<div class="flexslider">
<ul class="slides">
<li>
<div class="flex-image-container">
<img alt="" class=" wp-post-image" height="350" sizes="(max-width: 950px) 100vw, 950px" src="https://www.bachflower.com/wordpress/wp-content/uploads/2012/09/Winter-A-950x350.jpg" srcset="https://www.bachflower.com/wordpress/wp-content/uploads/2012/09/Winter-A.jpg 950w, https://www.bachflower.com/wordpress/wp-content/uploads/2012/09/Winter-A-150x55.jpg 150w, https://www.bachflower.com/wordpress/wp-content/uploads/2012/09/Winter-A-768x283.jpg 768w, https://www.bachflower.com/wordpress/wp-content/uploads/2012/09/Winter-A-200x74.jpg 200w" title="" width="950"/>
</div>
</li>
<li>
<div class="flex-image-container">
<img alt="" class=" wp-post-image" height="350" sizes="(max-width: 950px) 100vw, 950px" src="https://www.bachflower.com/wordpress/wp-content/uploads/2012/09/Winter-B-950x350.jpg" srcset="https://www.bachflower.com/wordpress/wp-content/uploads/2012/09/Winter-B.jpg 950w, https://www.bachflower.com/wordpress/wp-content/uploads/2012/09/Winter-B-150x55.jpg 150w, https://www.bachflower.com/wordpress/wp-content/uploads/2012/09/Winter-B-768x283.jpg 768w, https://www.bachflower.com/wordpress/wp-content/uploads/2012/09/Winter-B-200x74.jpg 200w" title="" width="950"/>
</div>
</li>
</ul>
</div>
</div>
<div id="intro">
		Dr. Bach's Original Flower Remedies, a system of 38 Flower Remedies to help mankind achieve joy and happiness	</div>
<div id="home-row-widgets">
<div class="thumb-grid">
<div class="widget thumb-grid-item image-frame">
<a href="http://bachflower.com/wordpress/?page_id=293">
<div class="thumb-grid-image-container">
<img class="thumb-grid-item-placeholder" src="https://www.bachflower.com/wordpress/wp-content/themes/risen/images/thumb-placeholder.png"/>
<img alt="" class="thumb-grid-image wp-post-image" height="400" sizes="(max-width: 600px) 100vw, 600px" src="https://www.bachflower.com/wordpress/wp-content/uploads/2012/09/HOMEBOX-1a-600-400-1.jpg" srcset="https://www.bachflower.com/wordpress/wp-content/uploads/2012/09/HOMEBOX-1a-600-400-1.jpg 600w, https://www.bachflower.com/wordpress/wp-content/uploads/2012/09/HOMEBOX-1a-600-400-1-150x100.jpg 150w, https://www.bachflower.com/wordpress/wp-content/uploads/2012/09/HOMEBOX-1a-600-400-1-200x133.jpg 200w" title="" width="600"/> </div>
<div class="widget-image-title">
						About Rescue Remedies					</div>
</a>
</div>
<div class="widget thumb-grid-item image-frame">
<a href="http://bachflower.com/wordpress/?page_id=302">
<div class="thumb-grid-image-container">
<img class="thumb-grid-item-placeholder" src="https://www.bachflower.com/wordpress/wp-content/themes/risen/images/thumb-placeholder.png"/>
<img alt="" class="thumb-grid-image wp-post-image" height="400" src="https://www.bachflower.com/wordpress/wp-content/uploads/2019/06/BFR-7Colors-600x400.jpg" title="" width="600"/> </div>
<div class="widget-image-title">
						About Bach Flower Remedies					</div>
</a>
</div>
<div class="widget thumb-grid-item image-frame">
<a href="http://bachflower.com/wordpress/?page_id=47">
<div class="thumb-grid-image-container">
<img class="thumb-grid-item-placeholder" src="https://www.bachflower.com/wordpress/wp-content/themes/risen/images/thumb-placeholder.png"/>
<img alt="" class="thumb-grid-image wp-post-image" height="400" src="https://www.bachflower.com/wordpress/wp-content/uploads/2012/09/Homebox-3-600x400.jpg" title="" width="600"/> </div>
<div class="widget-image-title">
						About Dr. Edward Bach					</div>
</a>
</div>
<div class="clear"></div>
</div>
</div>
<div id="home-column-widgets">
<div id="home-column-widgets-left">
<section class="widget content-widget widget_sp_image" id="widget_sp_image-4"><header><h1 class="home-column-widgets-title">Order NOW:</h1></header><a class="widget_sp_image-image-link" href="http://www.directlyfromnature.com/" target="_blank" title="Order NOW:"><img alt="Order NOW:" class="attachment-full aligncenter" height="128" sizes="(max-width: 216px) 100vw, 216px" src="https://www.bachflower.com/wordpress/wp-content/uploads/2015/05/DFN-Logo-OrderNow-sm.jpg" srcset="https://www.bachflower.com/wordpress/wp-content/uploads/2015/05/DFN-Logo-OrderNow-sm.jpg 216w, https://www.bachflower.com/wordpress/wp-content/uploads/2015/05/DFN-Logo-OrderNow-sm-150x89.jpg 150w, https://www.bachflower.com/wordpress/wp-content/uploads/2015/05/DFN-Logo-OrderNow-sm-200x119.jpg 200w" style="max-width: 100%;" width="216"/></a><div class="widget_sp_image-description"><p><b><a href="http://www.DirectlyFromNature.com" target="_blank" title="Check out our Daily Specials">TO PLACE ORDER CLICK HERE&gt;</a></b></p>
<p>- Save 10% on all Individual Bach Flower Remedies with Coupon Code STRESSFREE2X<br/>
- Save 7% on your entire order with Coupon Code WINTER21</p>
</div></section><section class="widget content-widget Social_Widget" id="social-widget-9"><header><h1 class="home-column-widgets-title">Follow Us!</h1></header><div class="socialmedia-buttons smw_left"><div class="socialmedia-text">@OriginalBachFlower</div><a href="https://www.facebook.com/OriginalBachFlower/" rel="nofollow" target="_blank"><img alt="Follow Us on Facebook" class="fade" height="32" src="https://www.bachflower.com/wordpress/wp-content/plugins/social-media-widget/images/default/32/facebook.png" style="opacity: 0.8; -moz-opacity: 0.8;" title="Follow Us on Facebook" width="32"/></a><a href="https://twitter.com/bachflowerdfn" rel="nofollow" target="_blank"><img alt="Follow Us on Twitter" class="fade" height="32" src="https://www.bachflower.com/wordpress/wp-content/plugins/social-media-widget/images/default/32/twitter.png" style="opacity: 0.8; -moz-opacity: 0.8;" title="Follow Us on Twitter" width="32"/></a><a href="http://instagram.com/originalbachflower" rel="nofollow" target="_blank"><img alt="Follow Us on Instagram" class="fade" height="32" src="https://www.bachflower.com/wordpress/wp-content/plugins/social-media-widget/images/default/32/instagram.png" style="opacity: 0.8; -moz-opacity: 0.8;" title="Follow Us on Instagram" width="32"/></a><a href="mailto:info@bachflower.com" rel="nofollow" target="_blank"><img alt="Follow Us on E-mail" class="fade" height="32" src="https://www.bachflower.com/wordpress/wp-content/plugins/social-media-widget/images/default/32/email.png" style="opacity: 0.8; -moz-opacity: 0.8;" title="Follow Us on E-mail" width="32"/></a><a href="http://Pinterest.com/originalbachflower" rel="nofollow" target="_blank"><img alt="Follow Us on Pinterest" class="fade" height="32" src="https://www.bachflower.com/wordpress/wp-content/plugins/social-media-widget/images/default/32/pinterest.png" style="opacity: 0.8; -moz-opacity: 0.8;" title="Follow Us on Pinterest" width="32"/></a><a href="https://www.youtube.com/user/originalbachflower" rel="nofollow" target="_blank"><img alt="Follow Us on YouTube" class="fade" height="32" src="https://www.bachflower.com/wordpress/wp-content/plugins/social-media-widget/images/default/32/youtube.png" style="opacity: 0.8; -moz-opacity: 0.8;" title="Follow Us on YouTube" width="32"/></a></div></section><section class="widget_text widget content-widget widget_custom_html" id="custom_html-3"><header><h1 class="home-column-widgets-title">Join our Mailing List</h1></header><div class="textwidget custom-html-widget"><!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="https://directlyfromnature.us18.list-manage.com/subscribe/post?u=45f552f9fee209bf10c162794&amp;id=33c6a650d2" class="validate" id="mc-embedded-subscribe-form" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
<div id="mc_embed_signup_scroll">
<label for="mce-EMAIL">Subscribe to our mailing list</label>
<input class="email" id="mce-EMAIL" name="EMAIL" placeholder="email address" required="" type="email" value=""/>
<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
<div aria-hidden="true" style="position: absolute; left: -5000px;"><input name="b_45f552f9fee209bf10c162794_33c6a650d2" tabindex="-1" type="text" value=""/></div>
<div class="show"><input class="button" id="mc-embedded-subscribe" name="subscribe" type="submit" value="Subscribe"/></div>
</div>
</form>
</div>
<!--End mc_embed_signup--></div></section><section class="widget content-widget widget_text" id="text-10"> <div class="textwidget"><div class="wp-caption aligncenter" id="attachment_2099" style="width: 200px"><a href="http://www.bachflower.com/bach-flower-remedy-questionnaire/"><img alt="" class="size-large wp-image-2099" height="188" src="http://www.bachflower.com/wordpress/wp-content/uploads/2018/04/Image-Questionnaire-041018-200x188.jpg" width="200"/></a>
<p class="wp-caption-text"><strong>This Quiz can help you select the correct Bach Flower Remedies for how you feel right now</strong></p>
</div>
</div>
</section><section class="widget content-widget widget_sp_image" id="widget_sp_image-5"><header><h1 class="home-column-widgets-title">Used by Celebrities</h1></header><img alt="Used by Celebrities" class="attachment-large aligncenter" height="200" sizes="(max-width: 200px) 100vw, 200px" src="https://www.bachflower.com/wordpress/wp-content/uploads/2013/05/photo-7-200x200.jpg" srcset="https://www.bachflower.com/wordpress/wp-content/uploads/2013/05/photo-7-200x200.jpg 200w, https://www.bachflower.com/wordpress/wp-content/uploads/2013/05/photo-7-150x150.jpg 150w, https://www.bachflower.com/wordpress/wp-content/uploads/2013/05/photo-7-960x960.jpg 960w, https://www.bachflower.com/wordpress/wp-content/uploads/2013/05/photo-7-180x180.jpg 180w, https://www.bachflower.com/wordpress/wp-content/uploads/2013/05/photo-7-55x55.jpg 55w, https://www.bachflower.com/wordpress/wp-content/uploads/2013/05/photo-7.jpg 1024w" style="max-width: 100%;" width="200"/><div class="widget_sp_image-description"><p>Jennifer Anniston says it keeps her cool under pressure.<br/>
Cate Blanchett swears by it, and Salma Hayek has been a fan for years.</p>
<p>Martha Stewart uses Rescue Remedy and Rescue Sleep.</p>
<p>Jennifer Meyer Maguire recommends Rescue Remedy by Bach: "a homeopathic spray that instantly calms you down when you're stressed out."</p>
<p><b><a href="https://directlyfromnature.com" target="_blank" title="ORDER NOW">ORDER NOW&gt;</a></b></p>
</div></section>
</div>
<div id="home-column-widgets-right">
<section class="widget content-widget widget_text" id="text-2"><header><h1 class="home-column-widgets-title">Welcome to BachFlower.com</h1></header> <div class="textwidget"><p><strong>Our Bach Foundation Trained Staff can help answer questions or take orders.</strong><br/>
<strong>1-800-214-2850 or email info@bachflower.com</strong></p>
<p>Dr. Edward Bach discovered that flowers in nature have the ability to affect our emotions positively. The energies from different flowers can remove our emotional pains and suffering, which over time harm our health and impair healing. He made sure that when he died in 1936 that his original Bach Flower system would be simple and easy for everyone to understand and use.</p>
<p><em><strong>The Bach Flower Remedies work in harmony with herbs, homeopathy and medications and are safe for everyone, including children, pregnant women, pets, the elderly and even plants.</strong></em></p>
<p>Visit these Bach Flower Web-Sites<br/>
<strong><a href="http://DirectlyFromNature.com">DirectlyFromNature.com – For Orders </a></strong><br/>
<a href="http://BachFlower.com">BachFlower.com</a><br/>
<a href="http://BachFlower4Kids.com">BachFlower4Kids.com</a><br/>
<a href="http://BachFlowerPets.com">BachFlowerPets.com</a></p>
<p><i>‘Disease of the body itself is nothing but the result of the disharmony between soul and mind. Remove the disharmony, and we regain harmony between soul and mind, and the body is once more perfect in all its parts.’<br/>
– Dr. Edward Bach </i></p>
</div>
</section><section class="widget content-widget widget_text" id="text-4"><header><h1 class="home-column-widgets-title">Did You Know?</h1></header> <div class="textwidget"><p>Dr. Bach was born more than 130 years ago and he left this amazing system in our hands at his death in 1936.</p>
<p>Shannon Beador from Housewives of Orange County uses and recommend Rescue Remedy and the Bach Flower Remedies.<br/>
<b><a href="https://youtu.be/09wkkqOwbOU" rel="noopener noreferrer" target="_blank" title="Watch the Show">Watch the Show&gt; </a></b></p>
<p>Dr. Phil and Dr. Oz recommend Rescue Remedy. </p>
<p>Selma Hayek mentioned in InStyle magazine that she uses Rescue Pastilles to help with crazy busy days, to keep her calm. </p>
<p>Emma Watson says about Rescue Remedy, "A few drops under my tongue before I go out calms me down. It's in my makeup bag all the time."</p>
<p>Jay McCarrol, the winner of the reality TV show Project Runway, used Rescue Remedy on the show's grand finale.</p>
<p>"....and to calm nerves I take a few drops of Bach Flower Rescue Remedy, made with relaxing flower essences.” –Andrea Gentl</p>
<p><b><a href="https://directlyfromnature.com" rel="noopener noreferrer" target="_blank" title="ORDER NOW">ORDER NOW&gt;</a></b></p>
</div>
</section>
</div>
<div class="clear"></div>
</div>
</div>
<!-- Footer Start -->
<footer id="footer">
<div id="footer-left">
<div class="clear"></div>
</div>
<div id="footer-right">
<ul id="footer-contact">
<li><span class="footer-icon generic"></span> BachFlower.com - California - USA</li>
<li><span class="footer-icon phone"></span> 1-800-214-2850 | 1-805-241-3257 | Wholesale 1-800-919-2515</li>
</ul>
<div id="copyright">
						BachFlower.com, Directly from Nature, LLC Copyright © 1996-2019. All rights reserved.

“Directly from Nature, LLC and BachFlower.com are independent retailers/wholesalers and are not affiliated with Bach Flower Remedies Limited or Nelsons. BACH, BACH FLOWER ESSENCES, RESCUE, RESCUE Sleep Liquid Melts, RESCUE Pearls, RESCUE SLEEP, Rescue PLUS, RESCUE REMEDY, RESCUE CREAM and RESCUE PASTILLES are trademark or registered trademarks of Bach Flower Remedies Limited.”

*The claims for this (these) product(s) is based on traditional homeopathic practice.  They have not been reviewed by the Food and Drug Administration.*					</div>
</div>
<div class="clear"></div>
</footer>
<div id="footer-bottom"></div>
<!-- Footer End -->
</div>
</div>
<!-- Container End -->
<!-- ngg_resource_manager_marker --> <script type="text/javascript">
			ajaxurl = typeof(ajaxurl) !== 'string' ? 'https://www.bachflower.com/wordpress/wp-admin/admin-ajax.php' : ajaxurl;
		</script>
<script src="https://www.bachflower.com/wordpress/wp-content/plugins/document-gallery/assets/js/gallery.min.js?ver=4.4.2" type="text/javascript"></script>
<script src="https://www.bachflower.com/wordpress/wp-includes/js/wp-embed.min.js?ver=5.2.9" type="text/javascript"></script>
</body>
</html>