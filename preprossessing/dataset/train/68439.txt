<!DOCTYPE html>
<html class="no-js" lang="">
<head>
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<title>Mira Sorvino Pictures, Latest News, Videos.</title>
<meta content="Mira Sorvino picture, Mira Sorvino album, Mira Sorvino movie, Mira Sorvino video" name="keywords"/>
<meta content="About Mira Sorvino including Mira Sorvino photos, news, gossip and videos." name="description"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="no-cache" http-equiv="Pragma"/>
<meta content="no-cache" http-equiv="Cache-Control"/>
<meta content="Wed, 17 Aug 2016 05:00:00 GMT" http-equiv="Expires"/>
<meta content="en-us, en-gb, en-au, en-ca" http-equiv="content-language"/>
<meta content="320088948016615" property="fb:app_id"/>
<meta content="Mira Sorvino Pictures, Latest News, Videos." property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="https://www.aceshowbiz.com/celebrity/mira_sorvino/" property="og:url"/>
<meta content="/images/photo/mira_sorvino.jpg" property="og:image"/>
<meta content="AceShowbiz" property="og:site_name"/>
<meta content="About Mira Sorvino including Mira Sorvino photos, news, gossip and videos." property="og:description"/>
<meta content="Mira Sorvino Pictures, Latest News, Videos." property="og:image:alt"/>
<meta content="Mira Sorvino Pictures, Latest News, Videos." name="twitter:title"/>
<meta content="About Mira Sorvino including Mira Sorvino photos, news, gossip and videos." name="twitter:description"/>
<meta content="/images/photo/mira_sorvino.jpg" name="twitter:image"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@aceshowbiz" name="twitter:site"/>
<meta content="@aceshowbiz" name="twitter:creator"/>
<meta content="aceshowbiz.com" name="twitter:domain"/>
<meta content="index, follow" name="robots"/>
<link href="https://m.aceshowbiz.com/celebrity/mira_sorvino/" media="only screen and (max-width: 600px)" rel="alternate"/>
<link href="https://www.aceshowbiz.com/amp/celebrity/mira_sorvino/" rel="amphtml"/>
<link href="/assets/css/normalize.css" rel="stylesheet"/>
<link href="/assets/css/main.css" rel="stylesheet"/>
<link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/assets/css/font-awesome.min.css" rel="stylesheet"/>
<link href="/assets/css/meanmenu.min.css" rel="stylesheet"/>
<link href="/assets/css/style.css?v=1.1" rel="stylesheet"/>
<link href="/assets/css/ie-only.css" rel="stylesheet" type="text/css"/>
<script src="/assets/js/modernizr-2.8.3.min.js"></script>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="black" name="apple-mobile-web-app-status-bar-style"/>
<meta content="#5F256F" name="theme-color"/>
<link href="/manifest.json" rel="manifest"/>
<link href="/assets/img/gif/favicon.gif" rel="shortcut icon" type="image/x-icon"/>
<link href="/assets/img/png/icon192.png" rel="icon" sizes="192x192"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon167.png" rel="apple-touch-icon" sizes="167x167"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon192.png" rel="icon" sizes="192x192"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon128.png" rel="icon" sizes="128x128"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon128.png" rel="icon" sizes="128x128"/>
<link href="//whizzco.com" rel="dns-prefetch"/>
<link href="//facebook.net" rel="dns-prefetch"/>
<link href="//sharethis.com" rel="dns-prefetch"/>
<link href="//mgid.com" rel="dns-prefetch"/>
<link href="//googlesyndication.com" rel="dns-prefetch"/>
<link href="//google-analytics.com" rel="dns-prefetch"/>
<link href="https://certify-js.alexametrics.com" rel="dns-prefetch"/>
<link href="https://tpc.googlesyndication.com" rel="dns-prefetch"/>
<link href="https://pagead2.googlesyndication.com" rel="dns-prefetch"/>
<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-4375969-1', 'aceshowbiz.com');
		  ga('require', 'displayfeatures');
		  ga('send', 'pageview');

		</script>
<script src="/assets/js/modernizr-2.8.3.min.js"></script>
<script src="/assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script async="" src="/assets/js/plugins.js " type="text/javascript"></script>
<script async="" src="/assets/js/bootstrap.min.js " type="text/javascript"></script>
<script src="/assets/js/jquery.meanmenu.min.js " type="text/javascript"></script>
<script src="/assets/js/jquery.scrollUp.min.js " type="text/javascript"></script>
<script async="" src="/assets/js/jquery.magnific-popup.min.js"></script>
<script defer="" src="/assets/js/main.js?v=10" type="text/javascript"></script>
<script src="//platform-api.sharethis.com/js/sharethis.js#property=5989b2b57c544f0011665e85&amp;product=inline-share-buttons" type="text/javascript"></script>
<style>
		/* begin: CSS Accordion Modified */
		label {
		  position: relative;
		  display: block;
		  padding: 0 0 0 1em;
		  background: #ffffff;
		  color: black;
		  font-weight: bold;
		  line-height: 3;
		  cursor: pointer;
		  border-bottom: 1px solid #d8dada;
		}
		
		/* :checked */
		input:checked ~ .tab-content {
		  max-height: 5000em;
		}					

		.profile ul {
			list-style: none;
			margin-left: 30px;
			font-size:14px;
			line-height:2.5;
		}
		.profile li::before {
			content: "\2022"; 
			display: inline-block; width: 1em;
			margin-left: -1em
		}
		/* end: CSS Accordion Modified */
		</style>
<script async="" src="/cdn-cgi/bm/cv/669835187/api.js"></script></head>
<body>
<!--[if lt IE 8]>
    <p class="browserupgrade">You are using an 
        <strong>outdated</strong> browser. Please 
        <a href="https://www.google.com/chrome/">upgrade your browser</a> to improve your experience.
    </p>
    <![endif]-->
<div class="wrapper" id="wrapper">
<header>
<div class="header-style1" id="header-layout1">
<div class="main-menu-area bg-primarytextcolor header-menu-fixed" id="sticker">
<div class="container">
<div class="row no-gutters d-flex align-items-center">
<div class="col-lg-2 d-none d-lg-block">
<div class="logo-area">
<a href="/">
<img alt="AceShowbiz logo" class="/assets/img-fluid" src="/assets/img/logo.png"/>
</a>
</div>
</div>
<div class="col-xl-8 col-lg-7 position-static min-height-none">
<div class="ne-main-menu">
<nav id="dropdown">
<ul>
<li>
<a href="/" title="Home">
<strong>Home</strong>
</a>
</li>
<li>
<a href="/news/" title="News">
<strong>News</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/news/celebrity/" title="Celebrity News">
Celebrity News
</a>
</li>
<li>
<a href="/news/movie/" title="Movie News">
Movie News
</a>
</li>
<li>
<a href="/news/tv/" title="TV News">
TV News
</a>
</li>
<li>
<a href="/news/music/" title="Music News">
Music News
</a>
</li>
</ul>
</li>
<li>
<a href="/celebrity/" title="Celebrity">
<strong>Celebrity</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/celebrity/buzz/" title="The Buzz">
The Buzz
</a>
</li>
<li>
<a href="/celebrity/legends/" title="The Legend">
The Legends
</a>
</li>
<li>
<a href="/celebrity/teenage/" title="Young Celeb">
Young Celeb
</a>
</li>
</ul>
</li>
<li>
<a href="/movie/" title="Movie">
<strong>Movie</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/movie/chart/" title="U.S. Box Office">
U.S. Box Office
</a>
</li>
<li>
<a href="/movie/now_playing/" title="Now Playing">
Now Playing
</a>
</li>
<li>
<a href="/movie/coming_soon/" title="Coming Soon">
Coming Soon
</a>
</li>
<li>
<a href="/movie/trailer/" title="Trailers">
Trailers
</a>
</li>
<li>
<a href="/movie/stills/" title="Pictures">
Pictures
</a>
</li>
<li>
<a href="/movie/review/" title="Reviews">
Reviews
</a>
</li>
<li>
<a href="/movie/soundtrack/" title="Soundtrack">
Soundtrack
</a>
</li>
</ul>
</li>
<li>
<a href="/tv/" title="TV">
<strong>TV</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/tv/trailer/" title="TV Clip / Previews">
TV Clip / Previews
</a>
</li>
<li>
<a href="/tv/dvd/" title="TV on DVD">
TV on DVD
</a>
</li>
<li>
<a href="/tv/soundtrack/" title="Soundtrack">
Soundtrack
</a>
</li>
</ul>
</li>
<li>
<a href="/music/" title="Music">
<strong>Music</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/music/artist/" title="Artist of The Week">
Artist of The Week
</a>
</li>
<li>
<a href="/music/chart/" title="ASB Music Chart">
ASB Music Chart
</a>
</li>
<li>
<a href="/music/new_release/" title="New Release">
New Release
</a>
</li>
<li>
<a href="/music/video/" title="Music Video">
Music Video
</a>
</li>
</ul>
</li>
<li>
<a href="/gallery/" title="Photo">
<strong>Photo</strong>
</a>
</li>
<li>
<a href="/video/" title="Video">
<strong>Video</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/music/video/" title="Music Video">
Music Video
</a>
</li>
<li>
<a href="/movie/trailer/" title="Movie Trailer">
Movie Trailer
</a>
</li>
<li>
<a href="/tv/trailer/" title="TV Clip">
TV Clip
</a>
</li>
</ul>
</li>
<li>
<a href="#" title="Other">
<strong>Other</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/interviews/" title="Interviews">
<strong>Interviews</strong>
</a>
</li>
<li>
<a href="/movie/dvd/" title="DVD">
<strong>DVD</strong>
</a>
</li>
<li>
<a href="/contest/" title="Contest">
<strong>Contest</strong>
</a>
</li>
<li>
<a href="/index_old.php" title="Old Homepage">
<strong>Old Homepage</strong>
</a>
</li>
</ul>
</li>
</ul>
</nav>
</div>
</div>
<div class="col-xl-2 col-lg-3 col-md-12 text-right position-static">
<div class="header-action-item">
<ul>
<li>
<form class="header-search-light" id="top-search-form">
<input class="search-input" placeholder="Search...." required="" style="display: none;" type="text"/>
<button class="search-button">
<i aria-hidden="true" class="fa fa-search"></i>
</button>
  
</form>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</header>
<div class="container"> </div>
<div class="mt-30"> </div>
<style>
			#RightFloatAds
			{
			right: 0px;
			position: fixed;
			top: 84px;
			z-index:2;
			}
		</style>
<section class="breadcrumbs-area" style="background-image: url('/assets/img/banner/breadcrumbs-banner.jpg');">
<div class="container">
<div class="breadcrumbs-content">
<h1>Mira Sorvino</h1>
<ul>
<li><a href="/">Home</a></li>
- <li><a href="/celebrity/mira_sorvino/">Mira Sorvino</a></li>
- <li><a href="/celebrity/mira_sorvino/news.html">News</a></li>
- <li><a href="/celebrity/mira_sorvino/picture.html">Picture Gallery</a></li>
- <li><a href="/celebrity/mira_sorvino/profile.html">Profile</a></li>
- <li><a href="/celebrity/mira_sorvino/filmography.html">Filmography</a></li>
- <li><a href="/celebrity/mira_sorvino/awards.html">Awards</a></li>
- <li><a href="/celebrity/mira_sorvino/video.html">Video</a></li>
</ul>
</div>
</div>
</section>
<section class="bg-accent section-space-less30">
<div class="container">
<div class="row">
<div class="col-12">
<div class="ne-banner-layout1 mb-30 text-center">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-5315453046799966" data-ad-slot="3899962133" style="display:inline-block;width:728px;height:90px"></ins>
<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row mb-50-r">
<div class="col-lg-8 col-md-12">
<div class="row mb-30">
<div class="col-12">
<div class="topic-border color-cinnabar mb-30 width-100">
<div class="topic-box-lg color-cinnabar">Mira Sorvino Profile</div>
</div>
</div>
<div class="col-lg-6 col-md-12">
<div class="img-overlay-70">
<img alt="Mira Sorvino Profile Photo" class="img-fluid width-100" data-qazy="true" src="/images/photo/mira_sorvino.jpg"/>
</div>
</div>
<div class="col-lg-6 col-md-12">
<div class="bg-body item-shadow-gray box-padding20">
<h3 class="title-medium-dark size-lg mb-15 mb5-md text-center">
<a href="#">Mira Sorvino</a>
</h3>
<div class="mt-10">
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Famous As</div>
<div class="profile_item">Actress</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Birth Name</div>
<div class="profile_item">Mira Katherine Sorvino</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Birth Date</div>
<div class="profile_item">Sep 28, 1967</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Birth Place</div>
<div class="profile_item">Tenafly, New Jersey, USA</div>
</div>
</div>
</div>
</div>
</div>
<div class="row mb-30">
<div class="col-md-12">
<div class="tab">
<input id="tab-1" name="tabs" type="checkbox"/>
<label for="tab-1">Claim to Fame</label>
<div class="tab-content" style="background:white; color:black;">
<div style="padding: 20px;">
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Famous As</div>
<div class="profile_item">Actress</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Popular for</div>
<div class="profile_item">As Linda Ash in "Mighty Aphrodite" (1995)</div>
</div>
</div>
</div>
</div>
<div class="tab">
<input id="tab-2" name="tabs" type="checkbox"/>
<label for="tab-2">Personal Fact</label>
<div class="tab-content" style="background:white; color:black;">
<div style="padding: 20px;">
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Birth Name</div>
<div class="profile_item">Mira Katherine Sorvino</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Birth Date</div>
<div class="profile_item">Sep 28, 1967</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Birth Place</div>
<div class="profile_item">Tenafly, New Jersey, USA</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Height</div>
<div class="profile_item">5' 10"</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Nationality</div>
<div class="profile_item">American</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Hair Color</div>
<div class="profile_item">Blonde</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Eye Color</div>
<div class="profile_item">Brown</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Education</div>
<ul class="profile">
<li>Graduated from Dwight Englewood High School in Englewood, New Jersey in 1986</li>
<li>Graduated magna cum laude from Harvard University in 1989, with a B.A. in East Asian Studies</li>
</ul>
</div>
</div>
</div>
</div>
<div class="tab">
<input id="tab-3" name="tabs" type="checkbox"/>
<label for="tab-3">Family and Relationship</label>
<div class="tab-content" style="background:white; color:black;">
<div style="padding: 20px;">
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Father</div>
<div class="profile_item">Paul Sorvino</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Mother</div>
<div class="profile_item">Lorraine Davis</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Brother</div>
<div class="profile_item">Michael Sorvino</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Sister</div>
<div class="profile_item">Amanda Sorvino (playwright)</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Spouse</div>
<div class="profile_item">Christopher Backus (actor, since 11-Jun-04)</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Relation</div>
<div class="profile_item"><a href="/celebrity/quentin_tarantino/" title="Quentin Tarantino">Quentin Tarantino</a> (film director, 1996 - 1998), <a href="/celebrity/olivier_martinez/" title="Olivier Martinez">Olivier Martinez</a> (actor, 1999 - 2002)</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Son</div>
<div class="profile_item">Johnny Christopher King (b. 29-May-06), Holden Paul Terry Backus (b. 22-Jun-09)</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Daughter</div>
<div class="profile_item">Mattea Angel Backus (b. 3-Nov-04), Lucia (b. 3-May-12)</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-12">
<div class="ne-banner-layout1 mb-30 text-center">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-5315453046799966" data-ad-slot="2423228935" style="display:inline-block;width:300px;height:250px"></ins>
<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
</div>
</div>
<div class="container">
<div class="row mb-20">
<div class="col-12">
<div class="topic-border color-cinnabar mb-30 width-100">
<div class="topic-box-lg color-cinnabar">Mira Sorvino News and Articles</div>
</div>
</div>
<div class="col-xl-4 col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mb-25 position-relative">
<a class="img-opacity-hover" href="/news/view/00150540.html">
<img alt="Rosanna Arquette, Mira Sorvino Applaud Harvey Weinstein's 23-Year Prison Sentence" class="img-fluid width-100 mb-15" data-qazy="true" src="/display/images/300x188/2020/03/12/00150540.jpg"/>
</a>
<div class="post-date-dark">
<ul>
<li>
<span>
<i aria-hidden="true" class="fa fa-calendar"></i>
</span>Mar 12</li>
</ul>
</div>
<h3 class="title-medium-dark size-md">
<a href="/news/view/00150540.html">Rosanna Arquette, Mira Sorvino Applaud Harvey Weinstein's 23-Year Prison Sentence</a>
</h3>
</div>
</div>
<div class="col-xl-4 col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mb-25 position-relative">
<a class="img-opacity-hover" href="/news/view/00137581.html">
<img alt="Mira Sorvino Goes Public With Date Rape Confession to Help Other Victims" class="img-fluid width-100 mb-15" data-qazy="true" src="/display/images/300x188/2019/06/13/00137581.jpg"/>
</a>
<div class="post-date-dark">
<ul>
<li>
<span>
<i aria-hidden="true" class="fa fa-calendar"></i>
</span>Jun 13</li>
</ul>
</div>
<h3 class="title-medium-dark size-md">
<a href="/news/view/00137581.html">Mira Sorvino Goes Public With Date Rape Confession to Help Other Victims</a>
</h3>
</div>
</div>
<div class="col-xl-4 col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mb-25 position-relative">
<a class="img-opacity-hover" href="/news/view/00125102.html">
<img alt="Mira Sorvino Is 'Heartsick' Over Asia Argento Sexual Assault Scandal" class="img-fluid width-100 mb-15" data-qazy="true" src="/display/images/300x188/2018/08/22/00125102.jpg"/>
</a>
<div class="post-date-dark">
<ul>
<li>
<span>
<i aria-hidden="true" class="fa fa-calendar"></i>
</span>Aug 22</li>
</ul>
</div>
<h3 class="title-medium-dark size-md">
<a href="/news/view/00125102.html">Mira Sorvino Is 'Heartsick' Over Asia Argento Sexual Assault Scandal</a>
</h3>
</div>
</div>
<div class="col-xl-4 col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mb-25 position-relative">
<a class="img-opacity-hover" href="/news/view/00121209.html">
<img alt="Mira Sorvino Says Harvey Weinstein's Arrest Is a 'Great First Step'" class="img-fluid width-100 mb-15" data-qazy="true" src="/display/images/300x188/2018/06/07/00121209.jpg"/>
</a>
<div class="post-date-dark">
<ul>
<li>
<span>
<i aria-hidden="true" class="fa fa-calendar"></i>
</span>Jun 07</li>
</ul>
</div>
<h3 class="title-medium-dark size-md">
<a href="/news/view/00121209.html">Mira Sorvino Says Harvey Weinstein's Arrest Is a 'Great First Step'</a>
</h3>
</div>
</div>
</div>
<div class="row mb-50">
<div class="col-12">
<div class="text-center">
<a class="btn-gtf-dtp-50" href="/celebrity/mira_sorvino/news.html">More News</a>
</div>
</div>
</div>
</div>
<div class="mb-30">
<section class="bg-body">
<div class="container">
<div class="row">
<div class="col-12">
<div class="ne-banner-layout1 mt-20 text-center">
<div id="ads_container_104124" website_id="104" widget_id="124"></div>
<script type="text/javascript">var uniquekey = "104124"; </script>
<script src="https://cdn.whizzco.com/scripts/widget/widget_t.js" type="text/javascript"></script>
</div>
</div>
</div>
</div>
</section>
</div>
<style>
				.border-bottom:before {
					background-color : #f8f8f8;
				}
			</style>
<section>
<div class="container mb-50">
<div class="row zoom-gallery">
<div class="col-12">
<div class="topic-border color-cinnabar mb-30 width-100">
<div class="topic-box-lg color-cinnabar">Mira Sorvino Photo Collections</div>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-6">
<div class="gallery-layout-1 mb-40 border-bottom pb-10">
<div class="popup-icon-hover img-overlay-hover mb-30">
<a href="/events/Mira Sorvino/mira-sorvino-14th-rome-film-festival-01.html">
<img alt="14th Rome Film Festival - Drowning - Photocall" class="width-100 img-fluid" data-qazy="true" src="/images/wennpic/preview/mira-sorvino-14th-rome-film-festival-01.jpg"/>
</a>
<a class="ne-zoom img-popup-icon" href="/images/wennpic/mira-sorvino-14th-rome-film-festival-01.jpg" title="14th Rome Film Festival - Drowning - Photocall">
<i aria-hidden="true" class="fa fa-plus"></i>
</a>
</div>
<div class="post-date-dark">
<ul>
<li>
<span>
<i aria-hidden="true" class="fa fa-calendar"></i>
</span>Feb 26, 2020</li>
</ul>
</div>
<h3 class="title-semibold-dark size-c22 mb-5">
<a href="/events/Mira Sorvino/mira-sorvino-14th-rome-film-festival-01.html">14th Rome Film Festival - Drowning - Photocall</a>
</h3>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-6">
<div class="gallery-layout-1 mb-40 border-bottom pb-10">
<div class="popup-icon-hover img-overlay-hover mb-30">
<a href="/events/Mira Sorvino/mira-sorvino-14th-rome-film-festival-02.html">
<img alt="14th Rome Film Festival - Drowning - Photocall" class="width-100 img-fluid" data-qazy="true" src="/images/wennpic/preview/mira-sorvino-14th-rome-film-festival-02.jpg"/>
</a>
<a class="ne-zoom img-popup-icon" href="/images/wennpic/mira-sorvino-14th-rome-film-festival-02.jpg" title="14th Rome Film Festival - Drowning - Photocall">
<i aria-hidden="true" class="fa fa-plus"></i>
</a>
</div>
<div class="post-date-dark">
<ul>
<li>
<span>
<i aria-hidden="true" class="fa fa-calendar"></i>
</span>Feb 26, 2020</li>
</ul>
</div>
<h3 class="title-semibold-dark size-c22 mb-5">
<a href="/events/Mira Sorvino/mira-sorvino-14th-rome-film-festival-02.html">14th Rome Film Festival - Drowning - Photocall</a>
</h3>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-6">
<div class="gallery-layout-1 mb-40 border-bottom pb-10">
<div class="popup-icon-hover img-overlay-hover mb-30">
<a href="/events/Mira Sorvino/mira-sorvino-2019-catalina-film-festival-01.html">
<img alt="2019 Catalina Film Festival" class="width-100 img-fluid" data-qazy="true" src="/images/wennpic/preview/mira-sorvino-2019-catalina-film-festival-01.jpg"/>
</a>
<a class="ne-zoom img-popup-icon" href="/images/wennpic/mira-sorvino-2019-catalina-film-festival-01.jpg" title="2019 Catalina Film Festival">
<i aria-hidden="true" class="fa fa-plus"></i>
</a>
</div>
<div class="post-date-dark">
<ul>
<li>
<span>
<i aria-hidden="true" class="fa fa-calendar"></i>
</span>Mar 12, 2020</li>
</ul>
</div>
<h3 class="title-semibold-dark size-c22 mb-5">
<a href="/events/Mira Sorvino/mira-sorvino-2019-catalina-film-festival-01.html">2019 Catalina Film Festival</a>
</h3>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-6">
<div class="gallery-layout-1 mb-40 border-bottom pb-10">
<div class="popup-icon-hover img-overlay-hover mb-30">
<a href="/events/Mira Sorvino/mira-sorvino-2019-catalina-film-festival-02.html">
<img alt="2019 Catalina Film Festival" class="width-100 img-fluid" data-qazy="true" src="/images/wennpic/preview/mira-sorvino-2019-catalina-film-festival-02.jpg"/>
</a>
<a class="ne-zoom img-popup-icon" href="/images/wennpic/mira-sorvino-2019-catalina-film-festival-02.jpg" title="2019 Catalina Film Festival">
<i aria-hidden="true" class="fa fa-plus"></i>
</a>
</div>
<div class="post-date-dark">
<ul>
<li>
<span>
<i aria-hidden="true" class="fa fa-calendar"></i>
</span>Mar 12, 2020</li>
</ul>
</div>
<h3 class="title-semibold-dark size-c22 mb-5">
<a href="/events/Mira Sorvino/mira-sorvino-2019-catalina-film-festival-02.html">2019 Catalina Film Festival</a>
</h3>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-6">
<div class="gallery-layout-1 mb-40 border-bottom pb-10">
<div class="popup-icon-hover img-overlay-hover mb-30">
<a href="/events/Mira Sorvino/mira-sorvino-2019-vanity-fair-oscar-party-01.html">
<img alt="2019 Vanity Fair Oscar Party - Arrivals" class="width-100 img-fluid" data-qazy="true" src="/images/wennpic/preview/mira-sorvino-2019-vanity-fair-oscar-party-01.jpg"/>
</a>
<a class="ne-zoom img-popup-icon" href="/images/wennpic/mira-sorvino-2019-vanity-fair-oscar-party-01.jpg" title="2019 Vanity Fair Oscar Party - Arrivals">
<i aria-hidden="true" class="fa fa-plus"></i>
</a>
</div>
<div class="post-date-dark">
<ul>
<li>
<span>
<i aria-hidden="true" class="fa fa-calendar"></i>
</span>Jun 14, 2019</li>
</ul>
</div>
<h3 class="title-semibold-dark size-c22 mb-5">
<a href="/events/Mira Sorvino/mira-sorvino-2019-vanity-fair-oscar-party-01.html">2019 Vanity Fair Oscar Party - Arrivals</a>
</h3>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-6">
<div class="gallery-layout-1 mb-40 border-bottom pb-10">
<div class="popup-icon-hover img-overlay-hover mb-30">
<a href="/events/Mira Sorvino/mira-sorvino-2019-vanity-fair-oscar-party-02.html">
<img alt="2019 Vanity Fair Oscar Party - Arrivals" class="width-100 img-fluid" data-qazy="true" src="/images/wennpic/preview/mira-sorvino-2019-vanity-fair-oscar-party-02.jpg"/>
</a>
<a class="ne-zoom img-popup-icon" href="/images/wennpic/mira-sorvino-2019-vanity-fair-oscar-party-02.jpg" title="2019 Vanity Fair Oscar Party - Arrivals">
<i aria-hidden="true" class="fa fa-plus"></i>
</a>
</div>
<div class="post-date-dark">
<ul>
<li>
<span>
<i aria-hidden="true" class="fa fa-calendar"></i>
</span>Jun 14, 2019</li>
</ul>
</div>
<h3 class="title-semibold-dark size-c22 mb-5">
<a href="/events/Mira Sorvino/mira-sorvino-2019-vanity-fair-oscar-party-02.html">2019 Vanity Fair Oscar Party - Arrivals</a>
</h3>
</div>
</div>
</div>
<div class="row mb-50">
<div class="col-12">
<div class="text-center">
<a class="btn-gtf-dtp-50" href="/celebrity/mira_sorvino/picture.html">More Photos</a>
</div>
</div>
</div>
</div>
</section>
</div>
<div class="ne-sidebar sidebar-break-md col-lg-4 col-md-12">
<div class="sidebar-box">
<div class="ne-banner-layout1 text-center">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-5315453046799966" data-ad-slot="6493156130" style="display:inline-block;width:300px;height:250px"></ins>
<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
</div>
</div>
<div class="sidebar-box">
<ul class="stay-connected overflow-hidden">
<li class="facebook">
<a href="https://www.facebook.com/aceshowbizdotcom/">
<i aria-hidden="true" class="fa fa-facebook"></i>
</a>
</li>
<li class="twitter">
<a href="https://twitter.com/aceshowbiz/">
<i aria-hidden="true" class="fa fa-twitter"></i>
</a>
</li>
<li class="google_plus">
<a href="https://plus.google.com/117276967966258729454">
<i aria-hidden="true" class="fa fa-google-plus"></i>
</a>
</li>
<li class="rss">
<a href="/site/rss.php">
<i aria-hidden="true" class="fa fa-rss"></i>
</a>
</li>
</ul>
</div>
<div class="sidebar-box">
<div class="ne-banner-layout1 text-center">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-5315453046799966" data-ad-slot="7830288536" style="display:inline-block;width:300px;height:250px"></ins>
<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
</div>
</div>
<div class="sidebar-box">
<div class="topic-border color-scampi mb-5">
<div class="topic-box-lg color-scampi">Most Read</div>
</div>
<div class="row">
<div class="col-12">
<div class="img-overlay-70 img-scale-animate mb-30">
<a href="/news/view/00164994.html" target="_blank"><img alt="LeToya Luckett's Husband Rumored Having Secret Family After She Takes Him Back" class="img-fluid width-100" data-qazy="true" src="/display/images/300x400/2021/01/11/00164994.jpg"/></a>
<div class="topic-box-top-lg">
<div class="topic-box-sm color-cod-gray mb-20">Celebrity</div>
</div>
<div class="mask-content-lg">
<div class="post-date-light">
<ul>
<li>
<span>
<i aria-hidden="true" class="fa fa-calendar"></i>
</span></li>
</ul>
</div>
<h2 class="title-medium-light size-lg">
<a href="/news/view/00164994.html" target="_blank">LeToya Luckett's Husband Rumored Having Secret Family After She Takes Him Back</a>
</h2>
</div>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164988.html" target="_blank">
<img alt="Radio Host Alex Jones Denounces QAnon After Capitol Hill Riot" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/11/00164988.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164988.html" target="_blank">Radio Host Alex Jones Denounces QAnon After Capitol Hill Riot</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164954.html" target="_blank">
<img alt="Keri Hilson Defends Herself After Criticizing Twitter for Banning Trump Following D.C. Riot" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/11/00164954.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164954.html" target="_blank">Keri Hilson Defends Herself After Criticizing Twitter for Banning Trump Following D.C. Riot</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164981.html" target="_blank">
<img alt="Proud Boys Leader Arrested for Taking Part in Capitol Siege After Ridiculous 'Journalist' Claim" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/11/00164981.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164981.html" target="_blank">Proud Boys Leader Arrested for Taking Part in Capitol Siege After Ridiculous 'Journalist' Claim</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165016.html" target="_blank">
<img alt="LeToya Luckett Goes Public With Divorce From Husband Months After Welcoming Second Child" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165016.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165016.html" target="_blank">LeToya Luckett Goes Public With Divorce From Husband Months After Welcoming Second Child</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165031.html" target="_blank">
<img alt="Yolanda Hadid Accidentally Reveals Face of Gigi Hadid's Daughter for the First Time" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165031.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165031.html" target="_blank">Yolanda Hadid Accidentally Reveals Face of Gigi Hadid's Daughter for the First Time</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164969.html" target="_blank">
<img alt="Pedro Pascal Encourages Fans to Give Ted Cruz Their Thoughts by Sharing His Office Phone Number " class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/11/00164969.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164969.html" target="_blank">Pedro Pascal Encourages Fans to Give Ted Cruz Their Thoughts by Sharing His Office Phone Number </a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164968.html" target="_blank">
<img alt="Waka Flocka Flame Dragged for Defending Keri Hilson's Donald Trump Remarks" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/11/00164968.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164968.html" target="_blank">Waka Flocka Flame Dragged for Defending Keri Hilson's Donald Trump Remarks</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165047.html" target="_blank">
<img alt="Mary J. Blige Applauded for Shutting Down Handsy Tyrese Gibson" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165047.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165047.html" target="_blank">Mary J. Blige Applauded for Shutting Down Handsy Tyrese Gibson</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164984.html" target="_blank">
<img alt="Prince Harry and Meghan Markle to Attend Queen's Birthday Celebration After Megxit" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/11/00164984.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164984.html" target="_blank">Prince Harry and Meghan Markle to Attend Queen's Birthday Celebration After Megxit</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165018.html" target="_blank">
<img alt="Armie Hammer's Rumored Ex-Flame Appears to Accuse Him of Abuse Amid Baffling Leaked DMs" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165018.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165018.html" target="_blank">Armie Hammer's Rumored Ex-Flame Appears to Accuse Him of Abuse Amid Baffling Leaked DMs</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165015.html" target="_blank">
<img alt="Report: Siegfried Fischbacher of Siegfried and Roy Fighting Terminal Pancreatic Cancer" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165015.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165015.html" target="_blank">Report: Siegfried Fischbacher of Siegfried and Roy Fighting Terminal Pancreatic Cancer</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165035.html" target="_blank">
<img alt="Drake Expertly Turns Down 'Black Widow' Celina Powell in Viral Text" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165035.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165035.html" target="_blank">Drake Expertly Turns Down 'Black Widow' Celina Powell in Viral Text</a>
</h3>
</div>
</div>
</div>
</div>
<div class="sidebar-box">
<div class="ne-banner-layout1 text-center">
</div>
</div>
</div>
</div>
</div>
</section>
<footer>
<div class="footer-area-bottom">
<div class="container">
<div class="row">
<div class="col-12 text-center">
<a class="footer-logo img-fluid" href="/">
<img alt="AceShowbiz logo" class="/assets/img-fluid" src="/assets/img/logo.png"/>
</a>
<h2 class="title-bold-light">
<a href="/site/about.php" title="About">About</a> |
<a href="/site/faq.php" title="Authors">FAQ</a> |
<a href="/site/term.php" title="Term of Use">Term of Use</a> |
<a href="/site/sitemap.php" title="Sitemap">Sitemap</a> |
<a href="/site/contact.php" title="Contact Us">Contact Us</a>
</h2>
<h3 class="title-medium-light size-md mb-10">© 2005-2021 <a href="https://www.aceshowbiz.com" target="_blank" title="AceShowbiz">AceShowbiz</a>. All Rights Reserved.</h3>
</div>
</div>
</div>
</div>
</footer>
</div>
<script>
			if ('serviceWorker' in navigator) {
				navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
					// Registration was successful
					console.log('ServiceWorker registration successful with scope: ', registration.scope);
				}).catch(function(err) {
					// registration failed :(
					console.log('ServiceWorker registration failed: ', err);
				});
			}
			
			self.addEventListener('fetch', function(event) {
			console.log(event.request.url);
			event.respondWith(
			caches.match(event.request).then(function(response) {
			return response || fetch(event.request);
			})
			);
			});			
		</script>
<script type="text/javascript">(function(){window['__CF$cv$params']={r:'610b0ec9dc57d9b0',m:'9f0e5a2d9c50769efdf9c788fbae5e0346e93aa7-1610498342-1800-ASoSIdRPODvtONU0TlIaa43iIP9AwQ1nljDCVNpDui48RviO8NyhdbgszrLIP0sTKy7dXA71gJ6UUPbdqEdnht2HzKC5JWWk3s8B0FQw/sRA1jR+NlNBsPRQQzTl5NUW4vvF75mGHCasGzaaNY79iFw=',s:[0x2d0011f590,0x6534058fd7],}})();</script></body>
</html>