<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "71206",
      cRay: "610a26c2680d01cd",
      cHash: "88be28d0ae04374",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuY2FmaW5hbHNyb2Rlby5jb20v",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "KHJVNyfcKb46wG1A1QK8cfaiB0J263iUq7tBOkvWxArQDnBgqtXUSWO1QKdq3JzooZewBF/8sRz/EMs8Z3OumCqaiK2f9rgTq3Zg7bAnSAJhbqxgJn/Io1MQvHi/6ujtJe3EHUxek7xJOhIroZIyUm21SwzgMpjbUq/gysMEr3JW9Xktg82LEv0rSPn4Dq/yhEz6BFGqXjFiBndeZE0oq5horFRcIBcVAXG7FOJkLFt7Q+GLKBnefmXzP+kI2E4CYYrir2zaCGipfNICZYUSbADGN7JWq6czJsDfxx2C3gNQikow2PBu8uVM8r9k39DB7AxCv5RhbAtknBwtJTy98kaO0cXVC2ip2mVMKd2HB2ZTRwvHtYLhBvP0r99lvqrR/4UkBJb2ry/Wd0XdQKDU+06mc1enJwZo+dlh4LJNvg+jKQPoNFmY8CTBctJ1+MXXLV5jeW4azk+UtV9va/VhMYGIviHs9lTxQz9ZfQsGu2lQGXdih8P8n+AWp3ziUUiWGp4cA/rzz73jirFFSeed4HC4vW//Oz/82CHogqpBPASMJ5uucnBY3TbNlXM7s9d9zH3jyFMk8mBTGMVYsadhrAWtQgsPmPvGy2pQqgeun12ze43yhTNFJZH28Oz1Tk6+jzEnIJoJa/z1HfgJ+HhU0w/RySIxd1JqFB0rYzHUaXPS8YNKW7gfiCc58GtWNoowQymzGI0am3yYLaZIRylHU6XVmpbhrTvKW3Ey2wsN0AbqiB5AXSRNdkp1YPoIeQbi",
        t: "MTYxMDQ4ODgzNy41MTAwMDA=",
        m: "MXC3cfj4hFO5nMDkjs1dO/gDiPi3OzmBs+PgStecxFY=",
        i1: "0pVtL4+eXHUNGzxRpQLjJg==",
        i2: "LGFXt8KdZ7eR+jmfdL9KlQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "QA+MV1v6CQFpOs1pc5tOS7ImHjiMasmwrjNQve3ncjE=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610a26c2680d01cd");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> cafinalsrodeo.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=1d61604c1330c52d5ac6cdbed77da694cb9bda22-1610488837-0-AUSiqlEwUknOjs1UKGHtQ0F-kxZaCU_m8j4R-YszJd5QmSePhn1S_VQijiBOwti82MyDLRglQt-1mT8AsZQGlNro0oUIqf00wx269aEsk_PcFAmMU2yNX3lFKg-Gv6Jm_Vpb7rLTbjwHEURTdscTDWX_jyqxMgaToQuNmeFImbWkJejoisdmDcpDPtEv-X5qqOqW3XZXrRl00dII21je8i8TmD66eFZ-toPE8CrtmsojSdJnN50AgFx09JE0wNGJ25mxMq6eeoBlgd_CaIy-9KgFo_kP5L22ZAyiQirJI2IDFi_H25T3LAqiwPixLV7I5A" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="71baf17583c475417a220327299485d4666484e2-1610488837-0-AYwnZOJvTMXrKg/y5xeBT51ujZcG/RNgKxR0X/wLBbvcAY4uFUL8l9eW+NngW0d3NcRLnwFKXJwJXPug064obS2CJtzcc+QGKIyYQhXWLfoW/sVbUJmaxykwENk5TX+HSto+Fsv18Rt/leHQhgyk2oWeulXPE6cJ2PdYz5OcFyZik8DIwKH4pQy1GvBT6LlzU94EUYhqlfyfV+q5rHUm2D2OlCf6OjYjQqx49dmrvAASuaDw6cR1tP222VVajbOlabc8F/2M8uMjZHq+1ZBRYz/NANvPz9SVCeb5P+fFv1BZZLPlS6HogipgbrHCOVUpAftfQjaL+zAXOsZtnDbt3NklMxUmpVnPDV0bLI/Wtg6NTTtyGeP5XvQhtPfmWUmmaNbOqD3oQkHZrAWqMCgR4hgT1uozZuj5cwvSiSO3e52wtuNqxET+kHhgyBalEALvr5TSzzPAFNQWJR9CuKZU0svCtX/WhK0oHdvndJ1X/C7ghNnfrGrMPBuDG4jvf/UqEoBALJwfuE/T5xFrJez/3mFW+gQH3Jim86wkgN0hINMkjpX8pbTWmE0BY7fvXtWNtInyZZngqDd4hoTSlOy7zenNAtwwY8zfG10bV7ZfnrPwBpZh50I/ultBQvP+iWYUcV47qewFtEdAwGMzFuTBlqlD1JTIWGT3vQPq3e/H00bIDCR2tiAqiJwOL865HpyeS3X7R3RESbKIqy5qQBtgr6vTbRcJ5Q5/t0opW2F9ueIE3NSs6T5Qofm/xp+Vl/p25PtKSt1tJ7vur3QWftQYr00FmoQI+1vQ8iRyMMfrXmWqQxHnXqTbSAVamxiR4kDgR1XHXJjBcXyggDhQkVJUrqE3SNbix74Nn4qarAEcZ8YUhT6aM6RVEILrRWw4VkbbL6LT8J2h5C4nAjhlj4o9eDQ/EElywRFOFcoXP8tqZTXAV1rWUJ/h4pmYovTZKXKKQNritjX2zhJ0/pFh/SAtS5v/UWIWLxypkSPmx+jf9JvOjLkq9O0lX55ppN4v8zn9IxJzOTilULFD4w4P6wKWUBA09LJOry97a4Rm8QkYHS7/gp7tkDOts5sp3SO+LnTVsj2LKBMMGRtH8eE3QJF6cDX92ZL13zL14+E3oGqPanuINsrEFx7wLHuWZDKlg8p7PV9wajP57ArF4VGBYk+xVUInNEX5Ot+VECwnZofbaICq88nAafz4TxI0I0aIgzLbnXhLwO60T4tEVdgHUdXyTR0WwucCcwMlTaLJUX3o1TWU1zZGycTXHA/IoMyxf/FB9L2gNrmRzAtjq5KrtiXWR+OdMjH5vQcYaEjN4KdZ93hoagykUN27ZKnQv6vAphfxbA=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="ecface4e15ce4b7036fa2e20e51989f1"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610488841.51-IiKlmf9Abk"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610a26c2680d01cd')"> </div>
</div>
<a href="https://robinsonsdrlg.com/direct.php?faqid=941"><span style="display: none;">table</span></a>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610a26c2680d01cd</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
