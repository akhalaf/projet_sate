<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "85506",
      cRay: "610ef604e9bc32d7",
      cHash: "7ca9d65b06b669d",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXNzb2hvbGljcy5jYy8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "1dKm+JmEq0c9Gko/8NZtPkTeowhRLbLgOmTJsLpmsVe/U+N9gbRErgjApnsjKtx/qHpsz4Xd5Bz+JcZqy8iFoGHAotadJmaACiD7akmONZP5SkG+W/Clw7pzfdt5Nu28/l+nfLGo+ADOCC+t5vXxZsTn6WyGBHFBJb8Eo0AB/nOAPSYV/8HTMkmwOT+utgAV79P2u4K84/CYcxtw96YL5bfTdg3yRIINihbejPqujUgVEn77suNGhwY8/41kHJGDgO7UoDlqpCgeWIQkA+r8ub0stYF0CnjF8qPmUmg+iCNK9fjnblDxGnvXlU67fflGfzdWtizIRmh031limhTv2sjQWpuHeB74o9LENLdVqDjJ+/LflJspUdXXNfTH49g7NY7J1c4lO6NTZvjJiqShfeXdnlHXQk/4hQEhrCSZJowUgfS7kUYK2PrFrBf7Tlu2bDaVRUrtB8jE8E9ntn7Zur0yYNSfi10r20ZULRZYhThOPQReZ7DS4b1HxPP3EaAS5XRx6nL8WSFWFyVctCC36q2eoo58uFOxz11LnaAYVg+z17MqRE7IIbM4YDmtr7geKCmv9T6DNSrnprLKRAg37cvKw60Di6tEDFxNIT+nvFHSCc3ZF+DKL9P+Lc3DlbUozztw4uIb1iCe6sqpMl+17vSCb3VHxVDNHpYPiwgugBcXoxM95r5SApDM7ZfcfTVYCuVu3ux95ANrf0OG6ZV9ANwDZtQhT/AWq1YumpqJ+gZXPnOb6SVWPb2RPi1ZSnq+",
        t: "MTYxMDUzOTI2OS45MTEwMDA=",
        m: "5ETi9f+USbwvBQvCp/1ftsVMHZkOEvh3EN7lPqW/uiM=",
        i1: "WfpokmsrfJiMaFKSMAS7sg==",
        i2: "l/+XtjpAr+JshIBjoHWf1Q==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "P0/K9FjfBo8HrCX7cgd0QWmx/2q/rvIzIpD4Nm2JmX4=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.assoholics.cc</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=815a5b64df001cc67080cd0f50b1a11031b2dc92-1610539269-0-AVCXD_ijyifxMbxSP-MXXGO84G_aJjCwR9_OqFpLzZFM5T13Cyt4KDQWiz7JP4miHwyZyGHdflEArxIOvYIrKl9RCtGfrpgM8PoKCuO4Yx9YoD5hErsiVeYKe_tIZNtaU8TseTKwqqHaedzqoGHgEwiGANkAmXd_5fk_FZ_AGJ-Iwl4K5L_A8rnx2uEMHe611BV2KJe45VcgrlwwPEjr128wevtN7lmQwS5TxRsse5vu83mpkWb_lRzqjawN0z0buAHaBaKQYbyvHenW0Xv8M4n9tIJmzJQ3joK-ePAmWxyutV2dAfcOgCsDl1LA9rAHxxhwlsUL58xtHkGTJF8s_PRI9MeRAMENB9yEFiP7M_ziQcgIVFkE9x0ZmthmS4Gj0KQOxGuAR0efOHna-z41B3wW9QeAz-c5pfdsLvfb9WUcc_NXwKANQIOOvdCl7r2Hx_fpQSHJuhXxHhtHX7J7_h-qdKRO_UKsQ3MOl9bRtKlwmr2cEZ_teSoKt9NHjKyLnTlobxtrTt6O8Sp20UJbh_M" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="c9db457fae25c720a43e8eff60ffb33e19ac5433-1610539269-0-Abd516DXPBTlqdFwZzPFm121NSpoFJtxHbM4SNLnrSYBf+lUfe6yKD8vV0A6sTvdg7UM54kRdVAq/Y0+zYFgcXbqYoI0LD1Qxo5wzKNEM0mc7NvZUMoAvIZLJYGDTmIHaQYLT3HLh7azO4Xfn1lna2o14yo6X/yot2a8zOoAS1uzTxVoGLlUR+9PAo+dyD77kQyOieP66dd0tfGlTl+sb/4rRL+FuoJpf+kateGn7654KG2B4cB32hB+iW7/Dv4nWQ+3Thr+Jvx2mcMfnAVifDd2Dv8hCUkQjM1j5y5dLgMqQY/D4ffg1wTxeayd8QsJOAO0H9k3upVunabcBBRma6Cv4wbHhGJb2tYwCo5vB8adyRPdYs7bL8z0g8ySVnjoXKEO1OU1GIluAf98RhQPKeGSKKrRGmJ8GbDjv045Xl3UOKnXfyUhZiiW2nK9Kj2bF+i0YlTloBNhbzLYTpQjK5oFkTyqlFefdRfFwgtSwn1/ISsb3dO7zbB3+TCQUKWPYqSXhI5eeLLMXjOwMzZfIDSQSkgQ7Z0paSDIgtmykMfeQ7qqkJPljnHs++WU7GCt8LWP8uqHTKsom6E8v43pH2QADRNBvAY/0uHgyzXIIrp1dUBhWUzuo+lwCIfhJHReMRdjXWQJwyxiXIfknzhCrO044wMRDugAMpCg6hlQfQGMV3tsvxLZmzcZLc84OMsyiALBVZZFvYr9+JIOqfk+CRGlKoxleTKL0pFMD584y2IU7IafvKo+XqjIZ7Y6ADzDTGkJxp3VTNOSlWkxbgdiyuFqmxqEkI52tVLOZAfgqqZKzeSTDEJVuPWKxyEKwXcI9De+q5NyQNdUFIDSJl/rTqhEjg/lC6iPAaMJ1QalznFcKXNoqmmcn516AUX/TegEHMJE+8U0uL+1AUcG2aKqZy9UxWN5NH+IObWw0Qi54B7E/JWp/rgixRLS7cDTsiDdMVYGDsoOcW0V8dXDEf1D8OWge/FtrtUBKoZMz18V2r4iIv71zThQz1FHhem/m/JYIRGQNxtkRAVchAAXW+zzxk0wfbVlDhVJkwb/2hrKKhl0XXJVhlZcS4J/UCFHUD9JSLJj96bAH7YTsLssx4GfyrzgOjmynW28w4ibQjhtcyHhvivHaAIycXNcf5HOk1LEqLpw7MWxi81PW18iI7S/qErbiJsoB5S7KpsNcdENXuGlDhZEDRWP0jkZwn/1IT6T/wwhejXC4SYQSsCsVQGsA/uAPh/jghIUJFYI1UgmZXE8uECIv27yFbujA7hc32OQHNJMF+yOne2Ex7ZpEdUepAdV0abUKEGVduHH7KAF0OLn"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="e9868c11749f711af3846ce4320a1442"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610ef604e9bc32d7')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610ef604e9bc32d7</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
