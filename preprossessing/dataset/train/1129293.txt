<!-- 
	Theme        : ange - Agency &  Startup HTML Template.
	Category     : ange.
	Author       : @Unifytheme.
	Designed By  : @Unifytheme.
	Developed By : @Unifytheme.
 --><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<!-- For IE -->
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<!-- For Resposive Device -->
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>Electrician,Electrical Contractors, Industrial Electrician,Residential Wiring,Commercial Electrician L &amp; M Pedulla</title>
<meta content=" L &amp; M PEDULLA offers complete, Industrial Electrician, residential Wiring, and Commercial Electrician work. Get the best Electricians and best deal soon!." name="description"/>
<meta content=" Electrical Contractors, Industrial Electrician, Residential Wiring, Commercial Electrician, Kitchener ,Waterloo,Cambridge,Guelph,Burlington,Stoney creek Hamiltion,Oakville,Milton,Brampton,Mississauga,Toronto ,Georgetown,Ancaster,Acton
		" name="keywords"/>
<!-- Main style sheet -->
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<!-- responsive style sheet -->
<link href="css/responsive.css" rel="stylesheet" type="text/css"/>
<!-- Fix Internet Explorer ______________________________________-->
<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="vendor/html5shiv.js"></script>
			<script src="vendor/respond.js"></script>
		<![endif]-->
<!--[if lte IE 8]>
		  <div style="color:#fff;background:#f00;padding:20px;text-align:center;">
		    Our template no longer actively supports this version of Internet Explorer. We suggest that you <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie" style="color:#fff;text-decoration:underline;">upgrade to a newer version</a> or try a different browser</a>.
		  </div>
		<![endif]-->
</head>
<body class="agency-theme">
<div class="main-page-wrapper">
<!-- ===================================================
				Loading Transition
			==================================================== -->
<div id="loader-wrapper">
<div id="loader"></div>
</div>
<!-- 
			=============================================
				Theme Header
			============================================== 
			-->
<header class="agency-header">
<!-- ============================ Theme Menu ========================= -->
<div class="theme-main-menu">
<div class="container">
<div class="main-container clearfix">
<div class="logo float-left"><a href="index.html"><img alt="Logo" src="images/logo/logo2.png"/></a></div>
<!-- ============== Menu Warpper ================ -->
<div class="menu-wrapper float-right">
<nav class="clearfix" id="mega-menu-holder">
<ul class="clearfix menu-content-holder">
<li class="active"><a href="index.html">Home</a>
</li>
<li><a href="about-us.html">About Us</a>
</li>
<li><a href="service.html">Services</a>
</li>
<li><a href="contact-us.html">contact</a></li>
</ul>
</nav> <!-- /#mega-menu-holder -->
</div> <!-- /.menu-wrapper -->
</div> <!-- /.main-container -->
</div> <!-- /.container -->
</div> <!-- /.theme-main-menu -->
</header> <!-- /.agency-header -->
<!-- 
			=============================================
				Theme Main Banner
			============================================== 
			-->
<div id="banner">
<div class="rev_slider_wrapper">
<!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
<div class="rev_slider" data-version="5.0.7" id="agency-main-banner">
<ul>
<!-- SLIDE1  -->
<li data-description="" data-easein="default" data-easeout="default" data-index="rs-378" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-title="Curtain from Right" data-transition="fade-in">
<!-- MAIN IMAGE -->
<img alt="" class="rev-slidebg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" src="images/home/slide-1.jpg"/>
<!-- LAYERS -->
<!-- LAYER NR. 1 -->
<div class="tp-caption text-center" data-height="none" data-hoffset="['10','10','5','0']" data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="1000" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-voffset="['-60','-60','-60','-40']" data-whitespace="normal" data-width="full" data-x="['left','left','left','center']" data-y="['middle','middle','middle','middle']" style="z-index: 6; white-space: nowrap;">
<h1><a href="index.html"><img alt="Logo" src="images/logo/logo-banner.png"/></a></h1>
</div>
</li>
</ul>
</div><!-- END REVOLUTION SLIDER -->
</div> <!--  /#banner -->
<!-- 
			=============================================
				About us
			============================================== 
			-->
<div class="agency-about-us">
<div class="container">
<div class="row">
<div class="col-md-7 col-xs-12 left-side">
<div class="image-box">
<img alt="" src="images/home/22.jpg"/>
<h2>Welcome To Pedulla Electric</h2>
</div> <!-- /.image-box -->
</div> <!-- /.left-side -->
<div class="col-md-5 col-xs-12 text">
<br/>
<p>Our mission at L &amp; M Pedulla Electric is to serve the needs of our customers in the industrial, commercial, and construction fields of electrical Offering complete systems, which include, high voltage in plant distribution, telecommunication and networking, fiber optics, and power from 24v to 4160v.L &amp; M's goal is to provide a safe product that will last.</p>
<p>Our company, is family owned and operated, we have been in business for over 25 years.We are working well into are second generation, with the same mindset.We are ready to take on any job no matter how big our small it may be, we try to service are customers in a timely fashion we value all our customers the same.</p>
<a class="button-seven agency-p-bg-color hvr-ripple-out" href="about-us.html">Learn More</a>
</div> <!-- /.text -->
</div> <!-- /.row -->
</div> <!-- /.container -->
</div> <!-- /.agency-about-us -->
<!-- 
			=============================================
				Agency Concept
			============================================== 
			-->
<div class="agency-concept">
<div class="container">
<div class="row">
<div class="col-md-6 col-xs-12 wow fadeInUp">
<div class="agency-single-concept tran3s">
<div class="box agency-p-bg-color tran3s"><i class="fa fa-clock-o"></i></div>
<h5><a class="tran3s" href="#">24 Hour Emergency</a></h5>
<p>L &amp; M Pedulla Electric has a fleet of service vehicles available for quick response to any emergency, anytime day or night.</p>
</div> <!-- /.agency-single-concept -->
</div> <!-- /.col- -->
<div class="col-md-6 col-xs-12 wow fadeInUp">
<div class="agency-single-concept tran3s">
<div class="box agency-p-bg-color tran3s"><i class="fa fa-industry"></i></div>
<h5><a class="tran3s" href="#">Industrial Insurance Services</a></h5>
<p>L &amp; M Pedulla Electric understands the importance of minimizing downtime.We have been specializing in the manufacturing packaging industry for 25 years. </p>
</div> <!-- /.agency-single-concept -->
</div> <!-- /.col- -->
</div> <!-- /.row -->
</div> <!-- /.container -->
</div> <!-- /.agency-concept -->
<!-- 
			=============================================
				Years Of Experience
			============================================== 
			-->
<div class="years-of-experience">
<div class="container">
<div class="row">
<div class="col-md-6 col-xs-12 text">
<h3>We have 30+ years of experiences</h3>
<p>Over the passed 25 years we have taken pride in the fact that we have had no serious injuries, and tend to keep are safety and yours as our number one priority, while maintaining a high level of work efficiency.We hold a certificate of participation in the Workers Safety and Insurance Board, and remain in good standing with them.</p> <br/>
<a class="button-seven agency-p-bg-color hvr-ripple-out" href="about-us.html">Read More</a>
</div> <!-- /.text -->
<div class="col-md-6 col-xs-12">
<img alt="" src="images/home/24.jpg"/>
</div>
</div> <!-- /.row -->
</div> <!-- /.container -->
</div> <!-- /.years-of-experience -->
<!-- 
			=============================================
				Our Work
			============================================== 
			-->
<div class="our-work">
<div class="container">
<div class="theme-title text-center">
<br/>
<h3>Our Work</h3>
</div> <!-- /.theme-title -->
<div class="row">
<div class="col-sm-6 col-xs-6">
<div class="single-work">
<img alt="" src="images/project/69.jpg"/>
<div class="opacity tran3s">
<h5><a href="work-details.html">Test Bench</a></h5>
<p>This is a test bench with its on breaker panle on board,  the test bench is testing the quality of coffee and needs 8 to 10 industail grade coffee marks rumming at the same time </p>
<a class="fancybox tran3s" href="images/project/69.jpg"></a>
</div> <!-- /.opacity -->
</div> <!-- /.single-work -->
</div> <!-- /.col- -->
<div class="col-sm-6 col-xs-6">
<div class="single-work">
<img alt="" src="images/project/70.jpg"/>
<div class="opacity tran3s">
<h5><a href="work-details.html">Rigid Pipe</a></h5>
<p>This picture shows the rigid pipe from a 120/240 panel, also a stand which houses a transformer.  This pipeing will meet the needs of the customer now and in the future, it was more cost effective to do all the pipeing at once, and made for a much neater job.</p>
<a class="fancybox tran3s" href="images/project/70.jpg"></a>
</div> <!-- /.opacity -->
</div> <!-- /.single-work -->
</div> <!-- /.col- -->
<div class="col-sm-4 col-xs-6">
<div class="single-work">
<img alt="" src="images/project/71.jpg"/>
<div class="opacity tran3s">
<h5><a href="work-details.html">Wire Way</a></h5>
<p>A combination of wire way,  and rigid conduit,  very strong and build to last.  </p>
<a class="fancybox tran3s" href="images/project/71.jpg"></a>
</div> <!-- /.opacity -->
</div> <!-- /.single-work -->
</div> <!-- /.col- -->
<div class="col-sm-4 col-xs-6">
<div class="single-work">
<img alt="" src="images/project/72.jpg"/>
<div class="opacity tran3s">
<h5><a href="work-details.html">Transformer</a></h5>
<p>This picture shows a 150 K.V.A. transformer  600 volts  to 480 volts,  as it was a work in progress the line side was feed,  and the load side still needed to be completed.   Once again a custom stand was made by us, the design helped to fit the transformer in the tight space restrictions </p>
<a class="fancybox tran3s" href="images/project/72.jpg"></a>
</div> <!-- /.opacity -->
</div> <!-- /.single-work -->
</div> <!-- /.col- -->
<div class="col-sm-4 col-xs-6">
<div class="single-work">
<img alt="" src="images/project/73.jpg"/>
<div class="opacity tran3s">
<h5><a href="work-details.html">M.C.C to Cable Tray</a></h5>
<p>Cable tray system have been very popular in Europe for a number of years,  this system seems to work well, and make for a cleaner installations. Future installations look like they have always been apart of the system </p>
<a class="fancybox tran3s" href="images/project/73.jpg"></a>
</div> <!-- /.opacity -->
</div> <!-- /.single-work -->
</div> <!-- /.col- -->
</div> <!-- /.row -->
</div> <!-- /.container -->
</div> <!-- /.our-work -->
<!-- 
			=============================================
				Footer
			============================================== 
			-->
<footer class="agency-footer">
<div class="container">
<div class="row">
<div class="col-sm-6 col-xs-12 float-right footer-contact">
<form action="/inc/sendemail.php" method="post">
<input placeholder="Name" type="text"/>
<input placeholder="Email" type="email"/>
<input placeholder="Subject" type="text"/>
<textarea placeholder="Text Message"></textarea>
<button class="button-seven agency-p-bg-color hvr-ripple-out">Submit Message</button>
</form>
</div> <!-- /.footer-contact -->
<div class="col-sm-6 col-xs-12 footer-address">
<ul class="address-list">
<li>
<i class="flaticon-telephone"></i>
<span>Call Number</span>
<p>905 854 5306</p>
</li>
<li>
<i class="flaticon-map-pin-marked"></i>
<span>Location</span>
<p>9455 Frist Line, Moffat Ontario, Canada L0P1J0</p>
</li>
<li>
<i class="flaticon-close-envelope"></i>
<span>Email</span>
<p>info@pedullaelectric.com</p>
</li>
</ul> <!-- /.address-list -->
<p>All Rights Reserved  <a class="tran3s agency-p-color" href="#">Pedulla Eletric</a> © 2017</p>
</div> <!-- /.footer-address -->
</div> <!-- /.row -->
</div> <!-- /.container -->
</footer>
<!-- Scroll Top Button -->
<button class="scroll-top tran3s hvr-shutter-out-horizontal">
<i aria-hidden="true" class="fa fa-angle-up"></i>
</button>
<!-- Js File_________________________________ -->
<!-- j Query -->
<script src="vendor/jquery.2.2.3.min.js" type="text/javascript"></script>
<!-- Bootstrap JS -->
<script src="vendor/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<!-- Vendor js _________ -->
<!-- revolution -->
<script src="vendor/revolution/jquery.themepunch.tools.min.js"></script>
<script src="vendor/revolution/jquery.themepunch.revolution.min.js"></script>
<script src="vendor/revolution/revolution.extension.slideanims.min.js" type="text/javascript"></script>
<script src="vendor/revolution/revolution.extension.layeranimation.min.js" type="text/javascript"></script>
<script src="vendor/revolution/revolution.extension.navigation.min.js" type="text/javascript"></script>
<script src="vendor/revolution/revolution.extension.kenburn.min.js" type="text/javascript"></script>
<!-- menu  -->
<script src="vendor/menu/src/js/jquery.slimmenu.js" type="text/javascript"></script>
<script src="vendor/jquery.easing.1.3.js" type="text/javascript"></script>
<!-- Bootstrap Select JS -->
<script src="vendor/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
<!-- fancy box -->
<script src="vendor/fancy-box/jquery.fancybox.pack.js" type="text/javascript"></script>
<!-- WOW js -->
<script src="vendor/WOW-master/dist/wow.min.js" type="text/javascript"></script>
<!-- owl.carousel -->
<script src="vendor/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<!-- Theme js -->
<script src="js/theme.js" type="text/javascript"></script>
</div> <!-- /.main-page-wrapper -->
</div></body>
</html>