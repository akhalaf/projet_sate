<!DOCTYPE html>
<html lang="sl-SI">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&amp;display=swap" rel="stylesheet"/>
<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxcxh74V-jH2lF-1zt6GGdnxJHkUsPUF4" type="text/javascript"></script>
<script crossorigin="anonymous" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<link href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="https://kit.fontawesome.com/4699d6e3cc.js"></script>
<script src="wp-content/themes/kocar/js/custom.js"></script>
<script>var disableStr = 'ga-disable-UA-133749934-1'; if (document.cookie.indexOf(disableStr + '=true') > -1) { window[disableStr] = true; }</script>
<!-- This site is optimized with the Yoast SEO plugin v14.3 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Page not found - Gostilna s prenočišči Olimpija Dolga Vas</title>
<meta content="noindex, follow" name="robots"/>
<meta content="sl_SI" property="og:locale"/>
<meta content="Page not found - Gostilna s prenočišči Olimpija Dolga Vas" property="og:title"/>
<meta content="Gostilna s prenočišči Olimpija Dolga Vas" property="og:site_name"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://prenocisca-kocar.si/#website","url":"https://prenocisca-kocar.si/","name":"Gostilna s preno\u010di\u0161\u010di Olimpija Dolga Vas","description":"Preno\u010di\u0161\u010da v Lendavi","potentialAction":[{"@type":"SearchAction","target":"https://prenocisca-kocar.si/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"sl-SI"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="https://prenocisca-kocar.si/feed/" rel="alternate" title="Gostilna s prenočišči Olimpija Dolga Vas » Vir" type="application/rss+xml"/>
<link href="https://prenocisca-kocar.si/comments/feed/" rel="alternate" title="Gostilna s prenočišči Olimpija Dolga Vas » Vir komentarjev" type="application/rss+xml"/>
<!-- This site uses the Google Analytics by ExactMetrics plugin v6.0.2 - Using Analytics tracking - https://www.exactmetrics.com/ -->
<script data-cfasync="false" type="text/javascript">
	var em_version         = '6.0.2';
	var em_track_user      = true;
	var em_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-133749934-1';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}

	if ( 'undefined' === typeof gaOptout ) {
		function gaOptout() {
			__gaTrackerOptout();
		}
	}
	
	if ( em_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

window.ga = __gaTracker;		__gaTracker('create', 'UA-133749934-1', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('send','pageview','/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer);
		__gaTracker( function() { window.ga = __gaTracker; } );
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + em_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
			window.ga = __gaTracker;		})();
		}
</script>
<!-- / Google Analytics by ExactMetrics -->
<link href="https://prenocisca-kocar.si/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://prenocisca-kocar.si/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.9" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://prenocisca-kocar.si/wp-content/plugins/cookie-notice/css/front.min.css?ver=5.4.4" id="cookie-notice-front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://prenocisca-kocar.si/wp-content/themes/kocar/style.css?ver=5.4.4" id="kocar-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://prenocisca-kocar.si/wp-content/plugins/wp-gdpr-compliance/assets/css/front.min.css?ver=1592388362" id="wpgdprc.css-css" media="all" rel="stylesheet" type="text/css"/>
<style id="wpgdprc.css-inline-css" type="text/css">

            div.wpgdprc .wpgdprc-switch .wpgdprc-switch-inner:before { content: 'Yes'; }
            div.wpgdprc .wpgdprc-switch .wpgdprc-switch-inner:after { content: 'No'; }
        
</style>
<script type="text/javascript">
/* <![CDATA[ */
var exactmetrics_frontend = {"js_events_tracking":"true","download_extensions":"zip,mp3,mpeg,pdf,docx,pptx,xlsx,rar","inbound_paths":"[{\"path\":\"\\\/go\\\/\",\"label\":\"affiliate\"},{\"path\":\"\\\/recommend\\\/\",\"label\":\"affiliate\"}]","home_url":"https:\/\/prenocisca-kocar.si","hash_tracking":"false"};
/* ]]> */
</script>
<script src="https://prenocisca-kocar.si/wp-content/plugins/google-analytics-dashboard-for-wp/assets/js/frontend.min.js?ver=6.0.2" type="text/javascript"></script>
<script src="https://prenocisca-kocar.si/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://prenocisca-kocar.si/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var cnArgs = {"ajaxUrl":"https:\/\/prenocisca-kocar.si\/wp-admin\/admin-ajax.php","nonce":"353700aa6e","hideEffect":"slide","position":"bottom","onScroll":"0","onScrollOffset":"100","onClick":"0","cookieName":"cookie_notice_accepted","cookieTime":"2592000","cookieTimeRejected":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"0","cache":"0","refuse":"0","revokeCookies":"0","revokeCookiesOpt":"automatic","secure":"1","coronabarActive":"0"};
/* ]]> */
</script>
<script src="https://prenocisca-kocar.si/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.3.2" type="text/javascript"></script>
<link href="https://prenocisca-kocar.si/wp-json/" rel="https://api.w.org/"/>
<link href="https://prenocisca-kocar.si/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://prenocisca-kocar.si/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.4" name="generator"/>
</head>
<body class="error404 cookies-not-set hfeed no-sidebar" data-rsssl="1">
<div class="site" id="page">
<div id="subhead"> <i class="fab fa-facebook-f subhead-facebook"></i> <i class="fab fa-tripadvisor subhead-tripadvisor"></i> <i class="far fa-map subhead-map"></i>Dolga vas, Glavna ulica 57, 9220 Lendava, Slovenia <i class="fas fa-map-marker-alt subhead-loc"></i>46.580922, 16.446546 </div>
<header class="site-header" id="masthead">
<div id="logo">
<div id="logo-inner">
<span>Gostilna s prenočišči</span><br/>Olimpija
			</div>
</div>
<ul id="main-navigation">
<li id="b-home"><a href="index.php">Domov</a></li>
<li id="b-onas">O nas</li>
<!-- <li>Cenik</li> -->
<li id="b-galerija">Galerija</li>
<li id="b-kjesmo">Kje smo</li>
<li id="b-kontakt">Kontakt</li>
</ul>
<div id="rezervirajte">
<button id="b-rezervirajte">REZERVIRAJTE</button>
</div>
</header><!-- #masthead -->
<div class="site-content" id="content">
<div class="content-area" id="primary">
<main class="site-main" id="main">
<section class="error-404 not-found">
<header class="page-header">
<h1 class="page-title">Oops! That page can’t be found.</h1>
</header><!-- .page-header -->
<div class="page-content">
<p>It looks like nothing was found at this location. Maybe try one of the links below or a search?</p>
<form action="https://prenocisca-kocar.si/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Išči:</span>
<input class="search-field" name="s" placeholder="Išči …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Išči"/>
</form>
<div class="widget widget_categories">
<h2 class="widget-title">Most Used Categories</h2>
<ul>
<li class="cat-item cat-item-1"><a href="https://prenocisca-kocar.si/category/uncategorized/">Uncategorized</a> (1)
</li>
</ul>
</div><!-- .widget -->
<div class="widget widget_archive"><h2 class="widgettitle">Arhiv</h2><p>Try looking in the monthly archives. 🙂</p> <label class="screen-reader-text" for="archives-dropdown--1">Arhiv</label>
<select id="archives-dropdown--1" name="archive-dropdown">
<option value="">Izberite mesec</option>
</select>
<script type="text/javascript">
/* <![CDATA[ */
(function() {
	var dropdown = document.getElementById( "archives-dropdown--1" );
	function onSelectChange() {
		if ( dropdown.options[ dropdown.selectedIndex ].value !== '' ) {
			document.location.href = this.options[ this.selectedIndex ].value;
		}
	}
	dropdown.onchange = onSelectChange;
})();
/* ]]> */
</script>
</div>
</div><!-- .page-content -->
</section><!-- .error-404 -->
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- #content -->
<footer class="site-footer" id="colophon">
<a href="/piskotki">Piškotki</a>, <a href="politika-zasebnosti">Politika zasebnosti</a>
</footer><!-- #colophon -->
</div><!-- #page -->
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/prenocisca-kocar.si\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://prenocisca-kocar.si/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.9" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6Ld-0KUZAAAAAAqAWLpSEIRZs2zRHMWHuc9CPDuA&amp;ver=3.0" type="text/javascript"></script>
<script src="https://prenocisca-kocar.si/wp-content/themes/kocar/js/navigation.js?ver=20151215" type="text/javascript"></script>
<script src="https://prenocisca-kocar.si/wp-content/themes/kocar/js/skip-link-focus-fix.js?ver=20151215" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpgdprcData = {"ajaxURL":"https:\/\/prenocisca-kocar.si\/wp-admin\/admin-ajax.php","ajaxSecurity":"c045bbea45","isMultisite":"","path":"\/","blogId":""};
/* ]]> */
</script>
<script src="https://prenocisca-kocar.si/wp-content/plugins/wp-gdpr-compliance/assets/js/front.min.js?ver=1592388362" type="text/javascript"></script>
<script src="https://prenocisca-kocar.si/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
<script type="text/javascript">(function() {
				var expirationDate = new Date();
				expirationDate.setTime( expirationDate.getTime() + 31536000 * 1000 );
				document.cookie = "pll_language=sl; expires=" + expirationDate.toUTCString() + "; path=/";
			}());</script><script type="text/javascript">
( function( sitekey, actions ) {

	document.addEventListener( 'DOMContentLoaded', function( event ) {
		var wpcf7recaptcha = {

			execute: function( action ) {
				grecaptcha.execute(
					sitekey,
					{ action: action }
				).then( function( token ) {
					var event = new CustomEvent( 'wpcf7grecaptchaexecuted', {
						detail: {
							action: action,
							token: token,
						},
					} );

					document.dispatchEvent( event );
				} );
			},

			executeOnHomepage: function() {
				wpcf7recaptcha.execute( actions[ 'homepage' ] );
			},

			executeOnContactform: function() {
				wpcf7recaptcha.execute( actions[ 'contactform' ] );
			},

		};

		grecaptcha.ready(
			wpcf7recaptcha.executeOnHomepage
		);

		document.addEventListener( 'change',
			wpcf7recaptcha.executeOnContactform, false
		);

		document.addEventListener( 'wpcf7submit',
			wpcf7recaptcha.executeOnHomepage, false
		);

	} );

	document.addEventListener( 'wpcf7grecaptchaexecuted', function( event ) {
		var fields = document.querySelectorAll(
			"form.wpcf7-form input[name='g-recaptcha-response']"
		);

		for ( var i = 0; i < fields.length; i++ ) {
			var field = fields[ i ];
			field.setAttribute( 'value', event.detail.token );
		}
	} );

} )(
	'6Ld-0KUZAAAAAAqAWLpSEIRZs2zRHMWHuc9CPDuA',
	{"homepage":"homepage","contactform":"contactform"}
);
</script>
<!-- Cookie Notice plugin v1.3.2 by Digital Factory https://dfactory.eu/ -->
<div aria-label="Cookie Notice" class="cookie-notice-hidden cookie-revoke-hidden cn-position-bottom" id="cookie-notice" role="banner" style="background-color: rgba(118,118,118,1);"><div class="cookie-notice-container" style="color: #fff;"><span class="cn-text-container" id="cn-notice-text">Ta spletna stran uporablja piškotke z namenom zagotavljanja spletne storitve, analizo uporabe, oglasnih sistemov in funkcionalnosti, ki jih brez piškotkov ne bi mogli nuditi. Z nadaljnjo uporabo spletne strani soglašate s piškotki. </span><span class="cn-buttons-container" id="cn-notice-buttons"><a aria-label="Se strinjam" class="cn-set-cookie cn-button wp-default button" data-cookie-set="accept" href="#" id="cn-accept-cookie">Se strinjam</a><a aria-label="Več o možnih nastavitvah piškotkov" class="cn-more-info cn-button wp-default button" href="" id="cn-more-info" target="_blank">Več o možnih nastavitvah piškotkov</a></span><a aria-label="Se strinjam" class="cn-close-icon" data-cookie-set="accept" href="javascript:void(0);" id="cn-close-notice"></a></div>
</div>
<!-- / Cookie Notice plugin -->
</body>
</html>
