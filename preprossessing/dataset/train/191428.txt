<!DOCTYPE html>
<html lang="en">
<head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="General" name="rating"/>
<meta content="Global" name="distribution"/>
<meta content="1 days" name="revisit-after"/>
<meta content="follow, index, all" name="robots"/>
<meta content="follow, index, all" name="googlebot"/>
<meta content="follow, index, all" name="Scooter"/>
<meta content="follow, index, all" name="msnbot"/>
<meta content="follow, index, all" name="alexabot"/>
<meta content="follow, index, all" name="Slurp"/>
<meta content="follow, index, all" name="ZyBorg"/>
<meta content="ALL" name="SPIDERS"/>
<meta content="ALL" name="WEBCRAWLERS"/>
<meta content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" name="viewport"/>
<link href="favicon.png" rel="shortcut icon" type="image/png"/>
<link href="https://banks.az/favicon.png" rel="shortcut icon" type="image/png"/>
<link href="https://banks.az/" rel="canonical"/>
<meta content="telephone=no" name="format-detection"/>
<meta content="yes" name="mobile-web-app-capable"/>
<meta content="#262262" name="theme-color"/>
<meta content="Banks.az" name="application-name"/>
<meta content="#262262" name="msapplication-TileColor"/>
<meta content="ie=edge" http-equiv="X-UA-Compatible"/>
<title>banks.az — финансовый супермаркет. Вклады, кредиты, ипотека, страховые и инвестиционные продукты</title>
<meta content="проценты по вкладам,рейтинг банков,список банков,срочный вклад,ставки по вкладам,депозитные вклады,рейтинг банков Баку,вклады сбербанка,страхование вкладов,вклады,банковские вклады,сбербанк,внешторгбанк,банки Баку,центральный банк,рейтинг банков,банки" name="keywords"/>
<meta content="Крупнейший независимый финансовый портал. Подбор и сравнение банковских, страховых и инвестиционных продуктов онлайн. Народный рейтинг банков. Все новости финансовой сферы." name="description"/>
<meta content="" property="fb:app_id"/>
<meta content="Banks.az" property="og:site_name"/>
<meta content="Подбор и сравнение банковских, страховых и инвестиционных продуктов онлайн" property="og:description"/>
<meta content="image/jpeg" property="og:image:type"/>
<meta content="http://www.banks.az/fb.jpg" property="og:image"/>
<meta content="1200" property="og:image:width"/>
<meta content="628" property="og:image:height"/>
<meta content="Подбор и сравнение банковских, страховых и инвестиционных продуктов онлайн" property="og:image:alt"/>
<meta content="Финансовый супермаркет Banks.az" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="http://banks.az/" property="og:url"/>
<meta content="khalilza.de" property="author"/>
<meta content="banks.az — финансовый супермаркет. Вклады, кредиты, ипотека, страховые и инвестиционные продукты" name="twitter:card"/>
<meta content="Подбор и сравнение банковских, страховых и инвестиционных продуктов онлайн" name="twitter:image:alt"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-151887959-1"></script>
<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-151887959-1');
	</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(61130578, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
   });
</script>
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/61130578" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="fonts/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="vendor/animate/animate.css" rel="stylesheet" type="text/css"/>
<link href="vendor/select2/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="css/util.css" rel="stylesheet" type="text/css"/>
<link href="css/main.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="simpleslide100">
<div class="simpleslide100-item bg-img1" style="background-image: url('images/bg01.jpg');"></div>
<div class="simpleslide100-item bg-img1" style="background-image: url('images/bg02.jpg');"></div>
</div>
<div class="flex-col-c-sb size1 overlay1">
<div class="flex-w flex-c-m p-b-35">
<div class="wrappic1 m-r-30 m-t-10 m-b-10">
<img alt="banks.az" src="images/icons/logo.png" style="max-height:150px;margin-top: 20%;"/>
</div>
</div>
<!--<div class="flex-col-c-m p-l-15 p-r-15 p-t-15 p-b-120">
				<div class="flex-w flex-c-m cd100">
				<div class="flex-col-c wsize1 m-b-30">
				<span class="l1-txt2 p-b-9 days"></span>
				<span class="s1-txt1 where1 p-l-35">Days</span>
				</div>
				<div class="flex-col-c wsize1 m-b-30">
				<span class="l1-txt2 p-b-9 hours"></span>
				<span class="s1-txt1 where1 p-l-35">Hours</span>
				</div>
				<div class="flex-col-c wsize1 m-b-30">
				<span class="l1-txt2 p-b-9 minutes"></span>
				<span class="s1-txt1 where1 p-l-35">Minutes</span>
				</div>
				<div class="flex-col-c wsize1 m-b-30">
				<span class="l1-txt2 p-b-9 seconds"></span>
				<span class="s1-txt1 where1 p-l-35">Seconds</span>
				</div>
				</div>
			</div>-->
<div class="flex-w flex-c-m p-b-35">
<a class="size3 flex-c-m how-social trans-04 m-r-3 m-l-3 m-b-5" href="https://www.facebook.com/Banks.az/" target="_blank">
<i class="fa fa-facebook"></i>
</a>
<a class="size3 flex-c-m how-social trans-04 m-r-3 m-l-3 m-b-5" href="https://www.instagram.com/banks.az/" target="_blank">
<i class="fa fa-instagram"></i>
</a>
<a class="size3 flex-c-m how-social trans-04 m-r-3 m-l-3 m-b-5" href="https://www.linkedin.com/company/34932463/" target="_blank">
<i class="fa fa-linkedin"></i>
</a>
</div>
</div>
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/select2/select2.min.js"></script>
<script src="vendor/countdowntime/moment.min.js"></script>
<script src="vendor/countdowntime/moment-timezone.min.js"></script>
<script src="vendor/countdowntime/moment-timezone-with-data.min.js"></script>
<script src="vendor/countdowntime/countdowntime.js"></script>
<script>
			$('.cd100').countdown100({
				/*Set Endtime here*/
				/*Endtime must be > current time*/
				endtimeYear: 2020,
				endtimeMonth: 5,
				endtimeDate: 1,
				endtimeHours: 09,
				endtimeMinutes: 0,
				endtimeSeconds: 0,
				timeZone: "Asia/Baku" 
				//ex:  timeZone: "Asia/Baku"
				//go to " http://momentjs.com/timezone/ " to get timezone
			});
		</script>
<script src="vendor/tilt/tilt.jquery.min.js"></script>
<script>
			$('.js-tilt').tilt({
				scale: 1.1
			})
		</script>
<script src="js/main.js"></script>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script data-cf-settings="47128ff3d9c0a098b61aa9b6-|49" defer="" src="../../../../ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js"></script>
</body>
</html>
