<!DOCTYPE html>
<html class="is-must2018 no-js" lang="en">
<head>
<!-- Google Tag Manager -->
<script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-M52ZZ4J');</script>
<!-- End Google Tag Manager -->
<meta charset="utf-8"/>
<title>The page doesn’t exist or has moved, please use the navigation above to navigate further</title>
<meta content="The page doesn’t exist or has moved, please use the navigation above to navigate further" name="description"/>
<meta content="noindex,follow" name="robots"/>
<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0" name="viewport"/>
<link href="/favicon-APL-3.png" rel="shortcut icon" type="image/png"/>
<style>
        .big15 a {
            margin-top: 2rem;
            height: 2.75rem;
            padding: 0 1.875rem;
            -webkit-border-radius: 1.375rem;
            border-radius: 1.375rem;
            line-height: 2.625rem;
            font-size: 1rem;
            border-color: #fb0000;
            color: #fff;
            background-color: #fb0000;
            font-family: opensans__semi, Open Sans, sans-serif;
            font-weight: 600;
            display: inline-block;
            border: 1px solid;
            overflow: hidden;
            text-align: center;
            white-space: nowrap;
            -o-text-overflow: ellipsis;
            text-overflow: ellipsis;
            vertical-align: middle;
            cursor: pointer;
            opacity: 1;
            outline: 0 none !important;
            -webkit-transition: background-color 300ms, border-color 300ms, color 300ms, opacity 300ms, -webkit-box-shadow 300ms, -webkit-transform 300ms;
            transition: background-color 300ms, border-color 300ms, color 300ms, opacity 300ms, -webkit-box-shadow 300ms, -webkit-transform 300ms;
            -o-transition: background-color 300ms, border-color 300ms, color 300ms, opacity 300ms, box-shadow 300ms, transform 300ms;
            transition: background-color 300ms, border-color 300ms, color 300ms, opacity 300ms, box-shadow 300ms, transform 300ms;
            transition: background-color 300ms, border-color 300ms, color 300ms, opacity 300ms, box-shadow 300ms, transform 300ms, -webkit-box-shadow 300ms, -webkit-transform 300ms;
            overflow: hidden !important;
            text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.05);
        }

            .big15 a:hover {
                border-color: #E20000;
                background-color: #E20000;
                text-decoration: none;
            }
    </style>
<link href="/Content/basecss?v=tvLS98bLWBWU6zCFP1GRKXD5W5YfH5Z2EaV2pBJW8YE1" rel="stylesheet"/>
<link href="/Content/libcss?v=lXx2MqZP9PZ7HQOx0J7a8KqCfGEtBpMyTO-bfJ47pwM1" rel="stylesheet"/>
<link href="/Content/APL-css?v=72ZtOOiuup95rYXr59QmhgsWTOvq5X7eXhTzdV8zAB41" rel="stylesheet"/>
<script>
        (function(l,f){function m(){var a=e.elements;return"string"==typeof a?a.split(" "):a}function i(a){var b=n[a[o]];b||(b={},h++,a[o]=h,n[h]=b);return b}function p(a,b,c){b||(b=f);if(g)return b.createElement(a);c||(c=i(b));b=c.cache[a]?c.cache[a].cloneNode():r.test(a)?(c.cache[a]=c.createElem(a)).cloneNode():c.createElem(a);return b.canHaveChildren&&!s.test(a)?c.frag.appendChild(b):b}function t(a,b){if(!b.cache)b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag();
			a.createElement=function(c){return!e.shivMethods?b.createElem(c):p(c,a,b)};a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+m().join().replace(/\w+/g,function(a){b.createElem(a);b.frag.createElement(a);return'c("'+a+'")'})+");return n}")(e,b.frag)}function q(a){a||(a=f);var b=i(a);if(e.shivCSS&&!j&&!b.hasCSS){var c,d=a;c=d.createElement("p");d=d.getElementsByTagName("head")[0]||d.documentElement;c.innerHTML="x<style>article,aside,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}</style>";
			c=d.insertBefore(c.lastChild,d.firstChild);b.hasCSS=!!c}g||t(a,b);return a}var k=l.html5||{},s=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,r=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,j,o="_html5shiv",h=0,n={},g;(function(){try{var a=f.createElement("a");a.innerHTML="<xyz></xyz>";j="hidden"in a;var b;if(!(b=1==a.childNodes.length)){f.createElement("a");var c=f.createDocumentFragment();b="undefined"==typeof c.cloneNode||
			"undefined"==typeof c.createDocumentFragment||"undefined"==typeof c.createElement}g=b}catch(d){g=j=!0}})();var e={elements:k.elements||"abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup main mark meter nav output progress section summary time video",version:"3.6.2",shivCSS:!1!==k.shivCSS,supportsUnknownElements:g,shivMethods:!1!==k.shivMethods,type:"default",shivDocument:q,createElement:p,createDocumentFragment:function(a,b){a||(a=f);if(g)return a.createDocumentFragment();
			for(var b=b||i(a),c=b.frag.cloneNode(),d=0,e=m(),h=e.length;d<h;d++)c.createElement(e[d]);return c}};l.html5=e;q(f)})(this,document);
        document.documentElement.className = document.documentElement.className.replace(/(^|\s)no-js(\s|$)/, '$1js$2');
    </script>
</head>
<body id="body">
<header class="l-zone__header" role="banner">
<div class="c-header">
<div class="c-header--wrapper" id="header">
<div class="c-header--trigger__nav">
<button class="js-menutrigger" data-classopen="has-menu__nav" data-target="c-header--section__nav">
<i aria-hidden="true" class="o-icon__menu"></i>
<span class="u-hiddentext">Show navigation menu</span>
</button>
</div>
<div class="c-header--section__logo">
<a href="/"><img alt="Logo APL" height="52" src="/Images/2018/logo/logo-apl.svg" width="85"/></a>
</div>
<div class="c-header--trigger__user">
<a href="https://auth.cma-cgm.com/idp/prp.wsf?wa=wsignin1.0&amp;wtrealm=https%3A%2F%2Fwww.apl.com&amp;wctx=rm%3d0%26id%3dpassive%26ru%3d%26Language%3den-US%26Site%3dapl" rel="nofollow">
<i aria-hidden="true" class="o-icon__user"></i>
<span>Sign In</span>
</a>
</div>
<div class="c-header--section__nav">
</div>
</div>
</div>
</header>
<div class="l-zone__main">
<div class="w680p center small-w100 mb4 mt4">
<div class="line txtcenter">
<h1 class="gray7">The page doesn’t exist or has moved, please use the navigation above to navigate further</h1>
<img alt="Error 404" class="mt5 pb3 xs-mt2 xs-mb2 blue1 biggest" src="/Content/img/404.jpg"/>
<div class="big15 lh-midi mt2">
<p>If you entered this address by mistake, please check it contains no typo/misspelling.<br/>If you landed on this page following a previously working link, it may have been move or deleted.<br/><a href="/">You may get back to this website's home page by clicking here.</a></p>
</div>
</div>
</div>
</div>
<footer class="l-zone__footer">
<div class="c-footer">
<div class="c-footer--wrapper">
<p class="c-footer--copyright">© 2021 APL</p>
<p class="u-color__transparent">
                    Version 17.0.3.895aac17 |
                    1/14/2021 7:35:21 PM - 0681
                </p>
<ul class="c-footer--links">
<li>
<a href="/help">Help</a>
</li>
<li>
<a href="/contact">Contact</a>
</li>
<li>
<a href="/sitemap">Site Map</a>
</li>
<li>
<a href="/legal-terms">Legal Terms</a>
</li>
<li><a href="/legal-terms/privacy-policy">Privacy Notice</a></li>
<li><a href="https://www.cmacgm-group.com">CMA CGM Group</a></li>
<li><a href="javascript:void(0)" id="customizeFooter">Customize Cookies</a></li>
</ul>
<ul class="c-footer--socials">
<li>
<a href="https://www.facebook.com/APLShipping/">
<i aria-hidden="true" class="o-icon__facebook"></i>
<span class="u-hiddentext">Like APL on Facebook</span>
</a>
</li>
<li>
<a href="https://twitter.com/APLShipping">
<i aria-hidden="true" class="o-icon__twitter"></i>
<span class="u-hiddentext">Follow APL on Twitter</span>
</a>
</li>
<li>
<a href="https://www.instagram.com/APLShipping/">
<i aria-hidden="true" class="o-icon__instagram"></i>
<span class="u-hiddentext">Follow APL on Instagram</span>
</a>
</li>
<li>
<a href="https://www.youtube.com/user/NOLGroup">
<i aria-hidden="true" class="o-icon__youtube"></i>
<span class="u-hiddentext">Watch APL videos on Youtube</span>
</a>
</li>
<li>
<a href="https://www.linkedin.com/company/apl">
<i aria-hidden="true" class="o-icon__linkedin-square"></i>
<span class="u-hiddentext">Follow APL on Linkedin</span>
</a>
</li>
</ul> </div>
</div>
</footer>
<script src="/bundles/library?v=0EleLwbPxL_vYQk6xDBNVYkc_felNkOPQ7hJXQ9mwtw1"></script>
<script src="/bundles/must-en?v=Pb95ZNGCsVK-GOPtalCmGJYmR9_lZxM0_iT7Q8lbqws1"></script>
<script src="/bundles/tealeaf?v=IS0BiClZl5CWXCDNoDTnQmiZbv2f-pNeHrjx2eJFpFc1"></script>
<script>
</script>
<script async="" src="//radar.cedexis.com/1/14542/radar.js"></script>
</body>
</html>