<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<title>UBS Squashclub Zürich</title>
<meta content="" name="description"/>
<meta content="web*olution web&amp;admin http://webadmin.webolution.ch/" name="generator"/>
<link href="https://ubs-squashclub-zh.ch/designs/default/favicons/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://ubs-squashclub-zh.ch/designs/default/favicons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="https://ubs-squashclub-zh.ch/designs/default/favicons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="https://ubs-squashclub-zh.ch/designs/default/favicons/site.webmanifest" rel="manifest"/>
<link color="#333333" href="https://ubs-squashclub-zh.ch/designs/default/favicons/safari-pinned-tab.svg" rel="mask-icon"/>
<meta content="#ffffff" name="msapplication-TileColor"/>
<meta content="#ffffff" name="theme-color"/>
<meta content="https://ubs-squashclub-zh.ch/designs/default/favicons/browserconfig.xml" name="msapplication-config"/>
<meta content="UBS Squashclub Zürich" name="application-name"/>
<script type="text/javascript">var SITE_URL='https://ubs-squashclub-zh.ch/';var DESIGN_URL='https://ubs-squashclub-zh.ch/designs/default/';var language_id=1;</script>
<script src="https://ubs-squashclub-zh.ch/js/jquery.min.js" type="text/javascript"></script>
<script src="https://ubs-squashclub-zh.ch/js/jQuery.print-master/jQuery.print.js" type="text/javascript"></script>
<script src="https://ubs-squashclub-zh.ch/js/functions.js" type="text/javascript"></script>
<link href="https://ubs-squashclub-zh.ch/css/styles.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">var SPECIALS_URL='https://ubs-squashclub-zh.ch/designs/default/specials/';var loaded_page_data={language_id:'1',lang_iso:'de',area_id:'0',page_id:'0',area_type:'',area_group:'',area_spec:'',page_type:'',page_link:'',page_spec:'',page_shp:'',page_ahp:'',member_id:'0',loggedIn:false,inWebAdmin:false,standAlone:false};</script>
<script src="https://ubs-squashclub-zh.ch/designs/default/js/PicGallery.js" type="text/javascript"></script>
<link href="https://ubs-squashclub-zh.ch/designs/default/css/pic-gallery.css" rel="stylesheet" type="text/css"/>
<link href="https://ubs-squashclub-zh.ch/designs/default/fonts/icomoon/style.css" rel="stylesheet" type="text/css"/>
<link href="https://ubs-squashclub-zh.ch/designs/default/css/styles.css?v=2.1" rel="stylesheet" type="text/css"/>
<link href="https://ubs-squashclub-zh.ch/designs/default/css/tabs.css" rel="stylesheet" type="text/css"/>
<script src="https://ubs-squashclub-zh.ch/designs/default/js/functions.js?v=2" type="text/javascript"></script>
<script src="https://ubs-squashclub-zh.ch/designs/default/js/popups.js" type="text/javascript"></script>
<script src="https://ubs-squashclub-zh.ch/designs/default/js/ms-Dropdown-master/js/msdropdown/jquery.dd.min.js"></script>
<script src="https://ubs-squashclub-zh.ch/designs/default/js/MemberLogin.js?v=1"></script>
<script src="https://ubs-squashclub-zh.ch/designs/default/js/tabs.js"></script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet"/>
<link href="https://ubs-squashclub-zh.ch/designs/default/js/ms-Dropdown-master/css/msdropdown/dd.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<header>
<div class="logo"><a alt="home" href="https://ubs-squashclub-zh.ch/" title="home"><img src="https://ubs-squashclub-zh.ch/designs/default/images/logo.svg"/></a></div>
<div id="nav-button"><a alt="Menu öffnen" href="javascript:void(0)" title="Menu öffnen"><img class="nav-button-img" src="https://ubs-squashclub-zh.ch/designs/default/images/nav-button.svg"/></a></div>
<div class="nav-container">
<nav class="global">
<div class="area-container" data-id="1" id="areacont-1"><div class="area" data-id="1" id="area-1"><a class="area" data-id="1" href="https://ubs-squashclub-zh.ch/home-aktuell/" id="a-lnk-1">Home / Aktuell</a></div></div><!-- .area-container --><div class="area-container" data-id="2" id="areacont-2"><div class="area" data-id="2" id="area-2"><a class="area" data-id="2" href="https://ubs-squashclub-zh.ch/kontakt/" id="a-lnk-2">Kontakt</a></div></div><!-- .area-container --><div class="area-container" data-id="3" id="areacont-3"><div class="area" data-id="3" id="area-3"><a class="area" data-id="3" href="https://ubs-squashclub-zh.ch/squash-links/" id="a-lnk-3">Squash Links</a></div></div><!-- .area-container --><div class="area-container" data-id="11" id="areacont-11"><div class="area" data-id="11" id="area-11"><a class="area subs" data-id="11" href="https://ubs-squashclub-zh.ch/archiv-aktuelles/" id="a-lnk-11">Archiv Aktuelles</a><a class="nav-button" href="javascript:void(0)" onclick="pageStuffHandler.handleNavi(11)"></a></div><div class="items-container" id="items-11"><div class="item" id="item-55"><a class="item" data="" href="https://ubs-squashclub-zh.ch/archiv-aktuelles/archyear-2020">2020</a></div><div class="item" id="item-43"><a class="item" data="" href="https://ubs-squashclub-zh.ch/archiv-aktuelles/archyear-2019">2019</a></div><div class="item" id="item-42"><a class="item" data="" href="https://ubs-squashclub-zh.ch/archiv-aktuelles/archyear-2018">2018</a></div><div class="item" id="item-41"><a class="item" data="" href="https://ubs-squashclub-zh.ch/archiv-aktuelles/archyear-2017">2017</a></div><div class="item" id="item-40"><a class="item" data="" href="https://ubs-squashclub-zh.ch/archiv-aktuelles/archyear-2016">2016</a></div><div class="item" id="item-39"><a class="item" data="" href="https://ubs-squashclub-zh.ch/archiv-aktuelles/archyear-2015">2015</a></div><div class="item" id="item-38"><a class="item" data="" href="https://ubs-squashclub-zh.ch/archiv-aktuelles/archyear-2014">2014</a></div><div class="item" id="item-36"><a class="item" data="" href="https://ubs-squashclub-zh.ch/archiv-aktuelles/archyear-2013">2013</a></div><div class="item" id="item-34"><a class="item" data="" href="https://ubs-squashclub-zh.ch/archiv-aktuelles/archyear-2012">2012</a></div><div class="item" id="item-33"><a class="item" data="" href="https://ubs-squashclub-zh.ch/archiv-aktuelles/archyear-2011">2011</a></div><div class="item" id="item-28"><a class="item" data="" href="https://ubs-squashclub-zh.ch/archiv-aktuelles/archyear-2010">2010</a></div><div class="item" id="item-32"><a class="item" data="" href="https://ubs-squashclub-zh.ch/archiv-aktuelles/archyear-2009">2009</a></div><div class="item" id="item-31"><a class="item" data="" href="https://ubs-squashclub-zh.ch/archiv-aktuelles/archyear-2008">2008</a></div><div class="item" id="item-30"><a class="item" data="" href="https://ubs-squashclub-zh.ch/archiv-aktuelles/archyear-2007">2007</a></div></div></div><!-- .area-container -->
</nav>
<nav class="local">
<div class="area-container" data-id="6" id="areacont-6"><div class="area" data-id="6" id="area-6"><a class="area" data-id="6" href="javascript:void(0)" id="a-lnk-6" onclick="memberLogin.openLoginFormInLightbox(6, 'area-members', 0, '')">Mitgliederbereich</a></div></div><!-- .area-container --><div class="area-container" data-id="7" id="areacont-7"><div class="area" data-id="7" id="area-7"><a class="area subs" data-id="7" href="https://ubs-squashclub-zh.ch/verein/" id="a-lnk-7">Verein</a><a class="nav-button" href="javascript:void(0)" onclick="pageStuffHandler.handleNavi(7)"></a></div><div class="items-container" id="items-7"><div class="item" id="item-6"><a class="item" data="" href="https://ubs-squashclub-zh.ch/verein/verein-mitglieder">Verein / Mitglieder</a></div><div class="item" id="item-18"><a class="item" data="" href="https://ubs-squashclub-zh.ch/verein/ehrenmitglieder">Ehrenmitglieder</a></div><div class="item" id="item-8"><a class="item" data="" href="https://ubs-squashclub-zh.ch/verein/vorstand">Vorstand</a></div><div class="item" id="item-19"><a class="item" data="member-registration" href="https://ubs-squashclub-zh.ch/verein/beitritt-in-verein-ubs-squashclub-zuerich">Beitrittsformular</a></div><div class="item" id="item-49"><a class="item" data="" href="https://ubs-squashclub-zh.ch/kontakt/kontakt">Kontakt</a></div></div></div><!-- .area-container --><div class="area-container" data-id="9" id="areacont-9"><div class="area" data-id="9" id="area-9"><a class="area subs" data-id="9" href="https://ubs-squashclub-zh.ch/aktivitaeten/" id="a-lnk-9">Aktivitäten</a><a class="nav-button" href="javascript:void(0)" onclick="pageStuffHandler.handleNavi(9)"></a></div><div class="items-container" id="items-9"><div class="item" id="item-21"><a class="item" data="acts-public" href="https://ubs-squashclub-zh.ch/aktivitaeten/jahresprogramm">Jahresprogramm</a></div><div class="item" id="item-25"><a class="item" data="trn-pubinfo" href="https://ubs-squashclub-zh.ch/aktivitaeten/training-lektionen">Training / Lektionen</a></div><div class="item" id="item-22"><a class="item" data="pl-pubinfo" href="https://ubs-squashclub-zh.ch/aktivitaeten/plauschliga">Plauschliga</a></div><div class="item" id="item-23"><a class="item" data="ic-pubinfo" href="https://ubs-squashclub-zh.ch/aktivitaeten/interclub">Interclub</a></div><div class="item" id="item-24"><a class="item" data="fs-pubinfo" href="https://ubs-squashclub-zh.ch/aktivitaeten/firmensport">Firmensport</a></div><div class="item" id="item-26"><a class="item" data="" href="https://ubs-squashclub-zh.ch/aktivitaeten/email-verteiler-fuer-squash-anlaesse">Email-Verteiler Squash-Anlässe</a></div></div></div><!-- .area-container --><div class="area-container" data-id="10" id="areacont-10"><div class="area" data-id="10" id="area-10"><a class="area" data-id="10" href="https://ubs-squashclub-zh.ch/spielorte/" id="a-lnk-10">Spielorte</a></div></div><!-- .area-container --><div class="area-container" data-id="12" id="areacont-12"><div class="area" data-id="12" id="area-12"><a class="area subs" data-id="12" href="https://ubs-squashclub-zh.ch/spielplaene/" id="a-lnk-12">Spielpläne</a><a class="nav-button" href="javascript:void(0)" onclick="pageStuffHandler.handleNavi(12)"></a></div><div class="items-container" id="items-12"><div class="item" id="item-51"><a class="item" data="trn-public" href="https://ubs-squashclub-zh.ch/spielplaene/trainingsplan">Training</a></div><div class="item" id="item-46"><a class="item" data="pl-public" href="https://ubs-squashclub-zh.ch/spielplaene/plauschliga-public">Plauschliga</a></div><div class="item" id="item-52"><a class="item" data="" href="https://ubs-squashclub-zh.ch/spielplaene/interclub-public?gnd=m#players">Interclub Herren</a></div><div class="item" id="item-54"><a class="item" data="" href="https://ubs-squashclub-zh.ch/spielplaene/interclub-public?gnd=f#players">Interclub Damen</a></div><div class="item" id="item-48"><a class="item" data="fs-public" href="https://ubs-squashclub-zh.ch/spielplaene/firmensport-public">Firmensport</a></div></div></div><!-- .area-container -->
</nav>
</div><!-- .nav-container -->
</header>
<!-- </div> --><!-- .header -->
<div id="content-container">
<main>
<section>
<!-- <div class="clear">&nbsp;</div> -->
<div class="wow-message info">Oh, kein Inhalt gefunden. Surf' mittels Navigation weiter ...</div>
</section>
</main>
</div><!-- content-container -->
<footer>
<div class="footer">
<div>© UBS Squashclub Zürich – <a href="https://ubs-squashclub-zh.ch/kontakt">Kontakt</a> – <a href="https://ubs-squashclub-zh.ch/impressum">Impressum</a> – <a href="https://ubs-squashclub-zh.ch/datenschutzbestimmungen">Datenschutz</a></div>
<div>Wir sind Mitglied von: <a href="http://www.squash.ch/" target="_blank">SWISS SQUASH</a> | <a href="https://www.firmensport.ch/" target="_blank">SFFS Schweiz. Firmen- und Freizeitsport</a> | <a href="http://www.zuerichsquash.ch/" target="_blank">Zürich Squash</a></div>
<div class="font-small"><span class="font-inact">powered by <a href="http://webadmin.webolution.ch/" target="_blank"><span class="webolution">web*olution</span> <span class="font-inact">web&amp;admin</span></a></span></div>
</div>
</footer>
<script type="text/javascript">var pageStuffHandler=new PageStuffHandler();pageStuffHandler.docLoadQuery();if(loaded_page_data.page_spec.indexOf('acts-')==0&&$('main section div.paragraf[data-info=activities] div.event-container.activity').length > 0){activityHandler=new ActivityHandler;activityHandler.setHashChange('on');activityHandler.getHash('load');activityHandler.setLinkLines();}</script>
<script src="https://ubs-squashclub-zh.ch/designs/default/js/page-top-button.js" type="text/javascript"></script>
</body>
</html>
