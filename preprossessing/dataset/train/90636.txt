<!DOCTYPE html>
<html lang="fi">
<head>
<title>Suomen Akatemia - Suomen Akatemia</title>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="fi" http-equiv="content-language"/>
<meta content="Fri, 27 Nov 2020 12:12:07 GMT" http-equiv="last-modified"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@SuomenAkatemia" name="twitter:site"/>
<meta content="@SuomenAkatemia" name="twitter:creator"/>
<meta content="Suomen Akatemia" name="twitter:title"/>
<meta content="https://www.aka.fi/Static/Themes/aka_default_kuva_sininen.jpg" name="twitter:image"/>
<meta content="" name="twitter:description"/>
<meta content="52267" name="pageID"/>
<link href="/Static/academy/img/favicon.ico" rel="shortcut icon"/>
<link href="/Static/bootstrap-4.4.1-dist/bootstrap-4.4.1-dist/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,700&amp;display=swap" rel="stylesheet"/>
<link href="/Static/academy/css/styles.css" rel="stylesheet"/>
<script src="/Static/js/jquery-3.4.1.min.js"></script>
<script src="/Static/bootstrap-4.4.1-dist/bootstrap-4.4.1-dist/js/bootstrap.bundle.min.js"></script>
<script src="/Static/js/script.js"></script>
<script src="/Static/js/modernizr.min.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-3020439-1']);
  _gaq.push(['_setDomainName', 'auto']);
  _gaq.push(['_trackPageview']);

  (function () {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div>
<header class="main-header">
<nav aria-label="Päänavigaatio" class="menu-container" id="topmenu">
<a class="skip-main" href="#main-content">Siirry pääsisältöön</a>
<div class="mobile-header">
<div class="mobile-logo"><a class="current" href="https://www.aka.fi/"><img alt="Suomen Akatemia" class="aka-logo" src="/globalassets/vanhat/y_kuvat/aka_logo_fi.svg"/></a></div>
<a aria-haspopup="true" aria-label="Avaa mobiilimenu" class="menu-mobile" href="#"></a>
</div>
<div class="menu">
<ul class="left-content" id="header-top-row-mobile-main">
<li class="topnav-home-container"><a class="current" href="https://www.aka.fi/"><img alt="Suomen Akatemia" class="aka-logo" src="/globalassets/vanhat/y_kuvat/aka_logo_fi.svg"/></a></li>
<li class="desktopboxmenu children">
<a class=" list-link" href="#">Tutkimusrahoitus</a>
<ul class="has-children">
<li class="">
<span class="mobileopen">Hae rahoitusta</span>
<ul>
<li class="">
<a href="/tutkimusrahoitus/hae-rahoitusta/haut/">Haut</a> </li>
<li class="">
<a href="/tutkimusrahoitus/hae-rahoitusta/nain-haet-rahoitusta/">Näin haet rahoitusta</a> <ul>
<li class="">
<a href="/tutkimusrahoitus/hae-rahoitusta/nain-haet-rahoitusta/ohjehakemisto/">Ohjehakemisto A-Ö</a>
</li>
<li class="">
<a href="/tutkimusrahoitus/hae-rahoitusta/nain-haet-rahoitusta/ask-apply/">Ask &amp; Apply</a>
</li>
</ul>
</li>
<li class="">
<a href="/tutkimusrahoitus/hae-rahoitusta/nain-rahoitusta-kaytetaan/">Näin rahoitusta käytetään</a> </li>
<li class="">
<a href="/tutkimusrahoitus/hae-rahoitusta/nain-raportoit/">Näin raportoit</a> </li>
</ul>
</li>
<li class="">
<span class="mobileopen">Arviointi ja rahoituspäätökset</span>
<ul>
<li class="">
<a href="/tutkimusrahoitus/arviointi-ja-rahoituspaatokset/hakemusten-arviointi-ja-paatoksenteko/">Hakemusten arviointi ja päätöksenteko</a> </li>
<li class="">
<a href="/tutkimusrahoitus/arviointi-ja-rahoituspaatokset/nain-hakemukset-arvioidaan/">Näin hakemukset arvioidaan</a> </li>
<li class="">
<a href="/tutkimusrahoitus/arviointi-ja-rahoituspaatokset/arviointiohjeet-ja-lomakkeet/">Arviointiohjeet ja -lomakkeet</a> </li>
<li class="">
<a href="/tutkimusrahoitus/arviointi-ja-rahoituspaatokset/nain-paatokset-tehdaan/">Näin päätökset tehdään</a> </li>
<li class="">
<a href="/tutkimusrahoitus/arviointi-ja-rahoituspaatokset/rahoituspaatokset/">Rahoituspäätökset</a> <ul>
<li class="">
<a href="/tutkimusrahoitus/arviointi-ja-rahoituspaatokset/rahoituspaatokset/rahoituspaatosten-haku/">Hae rahoituspäätöksiä</a>
</li>
<li class="">
<a href="/tutkimusrahoitus/arviointi-ja-rahoituspaatokset/rahoituspaatokset/rahoituspaatostiedotteet/">Rahoituspäätöstiedotteet</a>
</li>
<li class="">
<a href="/tutkimusrahoitus/arviointi-ja-rahoituspaatokset/rahoituspaatokset/hakemusten-ja-rahoituspaatosten-tilastoja/">Hakemusten ja rahoituspäätösten tilastoja</a>
</li>
</ul>
</li>
</ul>
</li>
<li class="">
<span class="mobileopen">Tutustu rahoitukseemme</span>
<ul>
<li class="">
<a href="/tutkimusrahoitus/tutustu-rahoitukseemme/tutkijalle/">Tutkijalle</a> </li>
<li class="">
<a href="/tutkimusrahoitus/tutustu-rahoitukseemme/tutkimusryhmalle/">Tutkimusryhmälle</a> </li>
<li class="">
<a href="/tutkimusrahoitus/tutustu-rahoitukseemme/tutkimusorganisaatiolle/">Tutkimusorganisaatiolle</a> </li>
<li class="">
<a href="/tutkimusrahoitus/tutustu-rahoitukseemme/strateginen-tutkimus/">Strategisen tutkimuksen rahoitus</a> </li>
<li class="">
<a href="/tutkimusrahoitus/tutustu-rahoitukseemme/kansainvalinen-rahoitusyhteistyo/">Kansainvälinen rahoitusyhteistyö</a> </li>
</ul>
</li>
<li class="">
<span class="mobileopen">Ohjelmat ja muut rahoitusmuodot</span>
<ul>
<li class="">
<a href="/tutkimusrahoitus/ohjelmat-ja-muut-rahoitusmuodot/akatemiaohjelmat/">Akatemiaohjelmat</a> </li>
<li class="">
<a href="/strateginen-tutkimus/" target="_blank">Strateginen tutkimus</a> </li>
<li class="">
<a href="/tutkimusrahoitus/ohjelmat-ja-muut-rahoitusmuodot/huippuyksikkoohjelmat/">Huippuyksikköohjelmat</a> </li>
<li class="">
<a href="/tutkimusrahoitus/ohjelmat-ja-muut-rahoitusmuodot/lippulaivaohjelma/">Lippulaivaohjelma</a> </li>
<li class="">
<a href="/tutkimusrahoitus/ohjelmat-ja-muut-rahoitusmuodot/tutkimusinfrastruktuurit/">Tutkimusinfrastruktuurit</a> </li>
<li class="">
<a href="/tutkimusrahoitus/ohjelmat-ja-muut-rahoitusmuodot/yliopistojen-profiloituminen/">Yliopistojen profiloituminen</a> </li>
<li class="">
<a href="/tutkimusrahoitus/ohjelmat-ja-muut-rahoitusmuodot/ict-2023/">ICT 2023</a> </li>
<li class="">
<a href="/tutkimusrahoitus/ohjelmat-ja-muut-rahoitusmuodot/etelamanner-tutkimus/">Etelämanner-tutkimus</a> </li>
</ul>
</li>
<li class="">
<span class="mobileopen">Vastuullinen tiede</span>
<ul>
<li class="">
<a href="/tutkimusrahoitus/vastuullinen-tiede/tutkimusetiikka/">Tutkimusetiikka</a> </li>
<li class="">
<a href="/tutkimusrahoitus/vastuullinen-tiede/tasa-arvo-ja-yhdenvertaisuus/">Tasa-arvo ja yhdenvertaisuus</a> </li>
<li class="">
<a href="/tutkimusrahoitus/vastuullinen-tiede/avoin-tiede/">Avoin tiede</a> </li>
<li class="">
<a href="/tutkimusrahoitus/vastuullinen-tiede/kestava-kehitys/">Kestävä kehitys</a> </li>
</ul>
</li>
</ul>
</li>
<li class="desktopboxmenu children">
<a class=" list-link" href="#">Suomen Akatemian toiminta</a>
<ul class="has-children">
<li class="">
<span class="mobileopen">Mitä teemme</span>
<ul>
<li class="">
<a href="/suomen-akatemian-toiminta/mita-teemme/mika-on-suomen-akatemia/">Mikä on Suomen Akatemia</a> <ul>
<li class="">
<a href="/suomen-akatemian-toiminta/mita-teemme/mika-on-suomen-akatemia/nain-toimimme/">Näin toimimme</a>
</li>
<li class="">
<a href="/suomen-akatemian-toiminta/mita-teemme/mika-on-suomen-akatemia/kenelle-rahoitus-myonnetaan/">Kenelle rahoitus myönnetään</a>
</li>
<li class="">
<a href="/suomen-akatemian-toiminta/mita-teemme/mika-on-suomen-akatemia/strategia-ja-arvot/">Strategia ja arvot</a>
</li>
<li class="">
<a href="/suomen-akatemian-toiminta/mita-teemme/mika-on-suomen-akatemia/organisaatio/">Organisaatio</a>
</li>
</ul>
</li>
<li class="">
<a href="/suomen-akatemian-toiminta/mita-teemme/toiminnan-raportointi/">Toiminnan raportointi</a> </li>
<li class="">
<a href="/suomen-akatemian-toiminta/mita-teemme/kansainvalinen-toiminta/">Kansainvälinen toiminta</a> </li>
</ul>
</li>
<li class="">
<span class="mobileopen">Toimielimet</span>
<ul>
<li class="">
<a href="/suomen-akatemian-toiminta/toimielimet/hallitus/">Hallitus</a> </li>
<li class="">
<a href="/suomen-akatemian-toiminta/toimielimet/jaostot/">Jaostot</a> </li>
<li class="">
<a href="/suomen-akatemian-toiminta/toimielimet/toimikunnat/">Toimikunnat</a> </li>
<li class="">
<a href="/suomen-akatemian-toiminta/toimielimet/infrastruktuurikomitea/">Tutkimusinfrastruktuurikomitea</a> </li>
<li class="">
<a href="/suomen-akatemian-toiminta/toimielimet/strategisen-tutkimuksen-neuvosto/">Strategisen tutkimuksen neuvosto</a> </li>
</ul>
</li>
<li class="">
<span class="mobileopen">Ajankohtaista</span>
<ul>
<li class="">
<a href="/suomen-akatemian-toiminta/ajankohtaista/tiedotteet-ja-uutiset/">Tiedotteet ja uutiset</a> </li>
<li class="">
<a href="/suomen-akatemian-toiminta/ajankohtaista/Tilaa_uutiskirje/">Tilaa uutiskirje</a> </li>
<li class="">
<a href="/suomen-akatemian-toiminta/ajankohtaista/blogi/">Blogipalsta</a> </li>
<li class="">
<a href="/suomen-akatemian-toiminta/ajankohtaista/medialle/">Medialle</a> </li>
<li class="">
<a href="/suomen-akatemian-toiminta/ajankohtaista/tapahtumat/">Tapahtumat</a> </li>
</ul>
</li>
<li class="">
<span class="mobileopen">Tutkimuksen tekijöitä</span>
<ul>
<li class="">
<a href="/suomen-akatemian-toiminta/tutkimuksen-tekijoita/tieteen-akateemikot/">Tieteen akateemikot</a> </li>
<li class="">
<a href="/suomen-akatemian-toiminta/tutkimuksen-tekijoita/akatemiapalkinnot/">Akatemiapalkinnot</a> </li>
</ul>
</li>
<li class="">
<span class="mobileopen">Tietoaineistot</span>
<ul>
<li class="">
<a href="/suomen-akatemian-toiminta/tietoaineistot/julkaisut/">Julkaisut</a> </li>
<li class="">
<a href="/suomen-akatemian-toiminta/tietoaineistot/tieteen-tila/">Tieteen tila</a> </li>
<li class="">
<a href="/suomen-akatemian-toiminta/tietoaineistot/hakemusten-ja-rahoituspaatosten-tilastoja/">Hakemusten ja rahoituspäätösten tilastoja</a> </li>
</ul>
</li>
<li class="">
<span class="mobileopen">Töihin Akatemiaan</span>
<ul>
<li class="">
<a href="/suomen-akatemian-toiminta/toihin-akatemiaan/akatemia-tyopaikkana/">Akatemia työpaikkana</a> </li>
</ul>
</li>
</ul>
</li>
<li class="mobile-bottom children desktopboxmenu">
<a class="btn btn2" href="/verkkoasiointi/">Verkkoasiointi</a>
<ul>
<li>
<div class="languages">
<div class="selection">
<a aria-label="På svenska" href="/sv/">SV</a>
<a aria-label="In English" href="/en/">EN</a>
</div>
</div></li>
<li><div id="header-search-mobile"><form action="/episerver-find-haku/" id="header-search-form-mobile" method="get"> <div class="input-group">
<input aria-label="Hakukenttä" class="form-control quick-search-query" id="quick-search-query-mobile" name="q" placeholder="Haku" type="text"/>
<span class="input-group-btn">
<button aria-label="Haku" class="btn btn-default transparent searchButton" id="quicksearch-search-submit-large-mobile" type="submit"></button>
</span>
</div>
</form></div></li>
</ul>
</li>
</ul>
<ul class="right-content">
<li class="right">
<div class="languages">
<div class="selection">
<a aria-label="På svenska" href="/sv/">SV</a>
<a aria-label="In English" href="/en/">EN</a>
</div>
</div>
</li>
<li class="right">
<div id="header-search">
<form action="/episerver-find-haku/" id="header-search-form" method="get"> <div class="input-group">
<input aria-label="Hakukenttä" class="form-control quick-search-query" id="quick-search-query" name="q" placeholder="Haku" type="text"/>
<span class="input-group-btn">
<button aria-label="Haku" class="btn btn-default transparent searchButton" id="quicksearch-search-submit-large" type="submit"></button>
</span>
</div>
</form>
</div>
</li>
<li class="right">
<a class="btn btn2" href="/verkkoasiointi/">Verkkoasiointi</a>
</li>
</ul>
</div>
</nav>
</header>
<main>
<!--Hero -->
<div class="container-fluid hero"><div>
<div class="hero-content" id="main-content">
<div class="row">
<h1>Suomen Akatemia</h1>
<p class="ingress">Suomen Akatemia rahoittaa korkealaatuista tieteellistä tutkimusta, toimii tieteen ja tiedepolitiikan asiantuntijana sekä vahvistaa tieteen ja tutkimustyön asemaa</p>
</div>
<div class="row buttons">
<div class="col-lg-4">
<p>
<a class="btn btn1" href="/tutkimusrahoitus/hae-rahoitusta/haut/">Hae rahoitusta</a>
</p>
</div>
<div class="col-lg-4">
<p>
<a class="btn btn1" href="/tutkimusrahoitus/arviointi-ja-rahoituspaatokset/rahoituspaatokset/">Tutustu rahoituspäätöksiin</a>
</p>
</div>
<div class="col-lg-4">
<p>
<a class="btn btn1" href="/suomen-akatemian-toiminta/mita-teemme/mika-on-suomen-akatemia/">Tutustu Akatemiaan</a>
</p>
</div>
</div>
</div></div></div>
<!-- / Hero -->
<div id="disturbanceMessage"></div>
<!-- Top content template -->
<div class="container-fluid top-content">
<div class="container">
<div class="row">
<h2 class="h3">
                    Tieteen akateemikot
                </h2>
<p class="ingress">
</p><p><strong>Kari Alitalo</strong> ja <strong>Kaisa Häkkinen</strong> ovat uusia tieteen akateemikoita. Tasavallan presidentti <strong>Sauli Niinistö</strong> myönsi heille akateemikon arvonimen presidentin esittelyssä 27.11.2020.</p>
</div>
<div class="row">
<p><a class="lue-lisaa" href="/suomen-akatemian-toiminta/tutkimuksen-tekijoita/tieteen-akateemikot/">Lue lisää</a></p>
</div>
</div>
</div>
<!-- / Top content template -->
<!-- Application boxes -->
<div class="container-fluid application">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2 class="application-title">

            Avoimet ja tulevat haut 

            </h2>
</div>
</div>
<div class="row">
<div class="col-md-6">
<div class="application-box">
<h3><a href="/tutkimusrahoitus/hae-rahoitusta/haut/kv-haut/jpi-amr--ohjelman-yhteisrahoitushaku-mikrobilaakeresistenssin-kehittymisen-ja-leviamisen-ehkaisemiseen-seka-vahentamiseen/">JPI AMR -ohjelman yhteisrahoitushaku mikrobilääkeresistenssin kehittymisen ja leviämisen ehkäisemiseen sekä vähentämiseen</a></h3>
<div class="app-start">
<strong>Haku alkaa</strong>
                                Alkuvuosi 2021
                            </div>
<div class="app-end">
<strong>Haku päättyy</strong>
                                    Alkuvuosi 2021
                                </div>
</div>
</div>
<div class="col-md-6">
<div class="application-box">
<h3><a href="/tutkimusrahoitus/hae-rahoitusta/haut/strategisen-tutkimuksen-haut/kutsuhaku-strategisen-tutkimuksen-ohjelmajohtajahaku-ohjelmalle-ilmastonmuutos-ja-ihminen-climate/">Kutsuhaku: ohjelmajohtajahaku STN-ohjelmalle Ilmastonmuutos ja ihminen (CLIMATE)</a></h3>
<div class="app-start">
<strong>Haku alkaa</strong>
                                1.12.2020
                            </div>
<div class="app-end">
<strong>Haku päättyy</strong>
                                    11.1.2021
                                </div>
</div>
</div>
<div class="col-md-6">
<div class="application-box">
<h3><a href="/tutkimusrahoitus/hae-rahoitusta/haut/strategisen-tutkimuksen-haut/kutsuhaku-strategisen-tutkimuksen-ohjelmajohtajahaku-ohjelmalle-tiedon-lukutaito-ja-tietoon-perustuva-paatoksenteko-literacy/">Kutsuhaku: ohjelmajohtajahaku STN-ohjelmalle Tiedon lukutaito ja tietoon perustuva päätöksenteko (LITERACY)</a></h3>
<div class="app-start">
<strong>Haku alkaa</strong>
                                1.12.2020
                            </div>
<div class="app-end">
<strong>Haku päättyy</strong>
                                    11.1.20221
                                </div>
</div>
</div>
<div class="col-md-6">
<div class="application-box">
<h3><a href="/tutkimusrahoitus/hae-rahoitusta/haut/strategisen-tutkimuksen-haut/strategisen-tutkimuksen-ohjelma--vaestorakenteen-muutokset--syyt-seuraukset-ja-ratkaisut-demography/">Strategisen tutkimuksen ohjelma – Väestörakenteen muutokset – syyt, seuraukset ja ratkaisut (DEMOGRAPHY)</a></h3>
<div class="app-start">
<strong>Haku alkaa</strong>
                                1.12.2020
                            </div>
<div class="app-end">
<strong>Haku päättyy</strong>
                                    13.1.2021 klo 16.15 Suomen aikaa (Aiehaku)
                                </div>
</div>
</div>
<div class="col-md-6">
<div class="application-box">
<h3><a href="/tutkimusrahoitus/hae-rahoitusta/haut/strategisen-tutkimuksen-haut/strategisen-tutkimuksen-ohjelma--elonkirjon-koyhtymisen-ymparistolliset-ja-yhteiskunnalliset-yhteydet-biod/">Strategisen tutkimuksen ohjelma – Elonkirjon köyhtymisen ympäristölliset ja yhteiskunnalliset yhteydet (BIOD)</a></h3>
<div class="app-start">
<strong>Haku alkaa</strong>
                                1.12.2020
                            </div>
<div class="app-end">
<strong>Haku päättyy</strong>
                                    13.1.2021 klo 16.15 Suomen aikaa (Aiehaku)
                                </div>
</div>
</div>
<div class="col-md-6">
<div class="application-box">
<h3><a href="/tutkimusrahoitus/hae-rahoitusta/haut/strategisen-tutkimuksen-haut/strategisen-tutkimuksen-ohjelma--pandemiat-yhteiskunnallisena-haasteena-pandemics/">Strategisen tutkimuksen ohjelma – Pandemiat yhteiskunnallisena haasteena (PANDEMICS)</a></h3>
<div class="app-start">
<strong>Haku alkaa</strong>
                                1.12.2020
                            </div>
<div class="app-end">
<strong>Haku päättyy</strong>
                                    13.1.2021 klo 16.15 Suomen aikaa (Aiehaku)
                                </div>
</div>
</div>
<div class="col-md-6">
<div class="application-box">
<h3><a href="/tutkimusrahoitus/hae-rahoitusta/haut/kv-haut/european-university-instituten-tohtoriohjelmahaku/">European University Instituten tohtoriohjelmahaku</a></h3>
<div class="app-start">
<strong>Haku alkaa</strong>
                                1.11.2020
                            </div>
<div class="app-end noicon">
<strong>Haku päättyy</strong>
                                    31.1.2021
                                </div>
</div>
</div>
<div class="col-md-6">
<div class="application-box">
<h3><a href="/tutkimusrahoitus/hae-rahoitusta/haut/kv-haut/suomen-akatemian-ja-nihn-yhteishakupilotti-lippulaivojen-edustamilla-aihealueilla/">Suomen Akatemian ja NIH:n yhteishakupilotti lippulaivojen edustamilla aihealueilla</a></h3>
<div class="app-start">
<strong>Haku alkaa</strong>
                                1.12.2020
                            </div>
<div class="app-end noicon">
<strong>Haku päättyy</strong>
                                    9.4.2021 klo 16.15 Suomen aikaa
                                </div>
</div>
</div>
</div>
<a class="btn btn1" href="/tutkimusrahoitus/hae-rahoitusta/haut/">Katso kaikki haut</a>
</div>
<!--<img src="~/Static/academy/img/haut_alhaalla.svg" class="triangle-bottom" />-->
</div>
<!-- / Application boxes -->
<!-- Startpage two column lists-->
<div class="container-fluid two-column news">
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="StartNewsList">
<div class="row news">
<div class="col-md-12">
<h2 class="h4">Ajankohtaista</h2>
<span class="rss"><a href="/suomen-akatemian-toiminta/ajankohtaista/rss-syotteet/tiedotteet/">Tilaa RSS</a></span>
</div>
</div>
<div class="newsframe row has-hr">
<div class="newsframe-content col-sm-9">
<div class="created">Tiedote 8.1.2021</div>
<div class="title"><a href="/suomen-akatemian-toiminta/ajankohtaista/tiedotteet-ja-uutiset/2021/suomessa-vietetaan-tutkitun-tiedon-teemavuotta/">Suomessa vietetään Tutkitun tiedon teemavuotta </a></div>
</div>
<hr aria-hidden="true"/>
</div>
<div class="newsframe row has-hr">
<div class="newsframe-content col-sm-9">
<div class="created">Tiedote 7.1.2021</div>
<div class="title"><a href="/suomen-akatemian-toiminta/ajankohtaista/tiedotteet-ja-uutiset/2021/avoimia-tehtavia-euroopan-yliopistoinstituutissa-eui/">Avoimia tehtäviä Euroopan yliopistoinstituutissa (EUI)</a></div>
</div>
<hr aria-hidden="true"/>
</div>
<div class="newsframe row has-hr">
<div class="newsframe-content col-sm-9">
<div class="created">Tiedote 4.1.2021</div>
<div class="title"><a href="/suomen-akatemian-toiminta/ajankohtaista/tiedotteet-ja-uutiset/2021/akateemikko-olli-lehto-on-kuollut/">Akateemikko Olli Lehto on kuollut</a></div>
</div>
<hr aria-hidden="true"/>
</div>
<div class="newsframe row has-hr">
<div class="newsframe-content col-sm-9">
<div class="created">Uutinen 28.12.2020</div>
<div class="title"><a href="/suomen-akatemian-toiminta/ajankohtaista/tiedotteet-ja-uutiset/20202/avoimia-tehtavia-euroopan-etelaisessa-observatoriossa2/">Avoimia tehtäviä Euroopan eteläisessä observatoriossa</a></div>
</div>
<hr aria-hidden="true"/>
</div>
<div class="newsframe row has-hr">
<div class="newsframe-content col-sm-9">
<div class="created">Tiedote 16.12.2020</div>
<div class="title"><a href="/suomen-akatemian-toiminta/ajankohtaista/tiedotteet-ja-uutiset/20202/suomen-akatemia-valitsi-kymmenen-uutta-akatemiaprofessoria/">Suomen Akatemia valitsi kymmenen uutta akatemiaprofessoria</a></div>
</div>
<hr aria-hidden="true"/>
</div>
<a href="/suomen-akatemian-toiminta/ajankohtaista/tiedotteet-ja-uutiset/">Lisää ajankohtaisia tiedotteita ja uutisia</a>
</div>
</div>
<div class="col-md-6">
<div class="StartNewsList">
<div class="row news">
<div class="col-md-12">
<h2 class="h4">Blogi</h2>
</div>
</div>
<div class="newsframe row has-hr">
<div class="liftimage col-sm-3">
<img alt='""' src="/globalassets/2-suomen-akatemian-toiminta/5-ajankohtaista/3-blogit/paivi_lindfors_aka_nostokuva.jpg"/>
</div>
<div class="newsframe-content col-sm-9">
<div class="created"> 11.1.2021</div>
<div class="title"><a href="/suomen-akatemian-toiminta/ajankohtaista/blogi/2021/afrikka-kiinnostaa/">Päivi Lindfors: Afrikka kiinnostaa</a></div>
</div>
<hr aria-hidden="true"/>
</div>
<div class="newsframe row has-hr">
<div class="liftimage col-sm-3">
<img alt='""' src="/globalassets/2-suomen-akatemian-toiminta/5-ajankohtaista/3-blogit/img_9964akah_nostokuva.jpg"/>
</div>
<div class="newsframe-content col-sm-9">
<div class="created"> 3.12.2020</div>
<div class="title"><a href="/suomen-akatemian-toiminta/ajankohtaista/blogi/20202/metallien-kierratys-vaatii-tutkimusta/">Saila Seppo: Metallien kierrätys vaatii tutkimusta</a></div>
</div>
<hr aria-hidden="true"/>
</div>
<div class="newsframe row has-hr">
<div class="liftimage col-sm-3">
<img alt='""' src="/globalassets/2-suomen-akatemian-toiminta/5-ajankohtaista/3-blogit/tiina_jokela_nostokuva.png"/>
</div>
<div class="newsframe-content col-sm-9">
<div class="created"> 21.10.2020</div>
<div class="title"><a href="/suomen-akatemian-toiminta/ajankohtaista/blogi/20202/miksi-tieto/">Tiina Jokela: Miksi tieto?</a></div>
</div>
<hr aria-hidden="true"/>
</div>
<div class="newsframe row has-hr">
<div class="liftimage col-sm-3">
<img alt='""' src="/globalassets/2-suomen-akatemian-toiminta/5-ajankohtaista/3-blogit/jyrkihakapaa_nostokuva.jpg"/>
</div>
<div class="newsframe-content col-sm-9">
<div class="created"> 15.10.2020</div>
<div class="title"><a href="/suomen-akatemian-toiminta/ajankohtaista/blogi/20202/uusia-toimia-tieteellisten-julkaisujen-avoimen-saatavuuden-edistamiseksi/">Jyrki Hakapää: Uusia toimia tieteellisten julkaisujen avoimen saatavuuden edistämiseksi</a></div>
</div>
<hr aria-hidden="true"/>
</div>
<div class="newsframe row has-hr">
<div class="liftimage col-sm-3">
<img alt='""' src="/globalassets/2-suomen-akatemian-toiminta/5-ajankohtaista/3-blogit/risto-vilkko_96px.jpg"/>
</div>
<div class="newsframe-content col-sm-9">
<div class="created"> 1.9.2020</div>
<div class="title"><a href="/suomen-akatemian-toiminta/ajankohtaista/blogi/20202/aikakoneella-euroopan-historiaan/">Risto Vilkko: Aikakoneella Euroopan historiaan</a></div>
</div>
<hr aria-hidden="true"/>
</div>
<a href="/suomen-akatemian-toiminta/ajankohtaista/blogi/">Lisää blogikirjoituksia</a>
</div>
</div>
</div>
</div>
</div>
<!-- /Startpage two column lists-->
<!-- Eventlift-->
<div class="container-fluid top-content second-top-content">
<div class="container">
<div class="row">
<h2 class="h3">
                    Tapahtumat
                </h2>
<p class="ingress">
</p><p>Tutustu Akatemian ja sen yhteistyökumppaneiden järjestämiin tilaisuuksiin.</p>
</div>
<div class="row">
<p><a class="btn btn1" href="/suomen-akatemian-toiminta/ajankohtaista/tapahtumat/">Tutustu tapahtumiin</a></p>
</div>
</div>
</div>
<!--/ Eventlift-->
<!-- Articleblocks-->
<div class="container-fluid article-content">
<div class="container">
<div>
<div class="row article-block-row">
<div class="col-md-6 article-block-img">
<img alt="Kuvituskuva strategisen tutkimuksen logo" src="/globalassets/3-stn/1-strateginen-tutkimus/tiedon-kayttajalle/stn_nostokuva_perus.jpg"/>
</div>
<div class="col-md-6">
<div class="article-block-text">
<h2>Strateginen tutkimus</h2>
<p>Strategisen tutkimuksen neuvosto (STN) rahoittaa yhteiskunnallisesti merkittävää ja vaikuttavaa korkeatasoista tiedettä. Tutkimuksen avulla etsitään konkreettisia ratkaisuja suuriin ja monitieteistä otetta vaativiin haasteisiin. Yhteistyö tiedontuottajien ja -hyödyntäjien välillä koko hankkeen ajan on tärkeää.</p>
<p><a class="read-more" href="/strateginen-tutkimus/">Tutustu tarkemmin</a></p>
</div>
</div>
</div>
</div><div>
<div class="row article-block-row flex-row-reverse">
<div class="col-md-6 article-block-img">
<img alt="Tietysti.fi kuvituskuva silmä" src="/globalassets/etusivujen-kuvat/kuvatietysti.jpg"/>
</div>
<div class="col-md-6">
<div class="article-block-text">
<h2>Tietysti.fi</h2>
<p>Tietysti.fi on sivusto, joka kertoo yleistajuisesti Akatemian rahoittamasta tutkimuksesta sekä tieteestä ja tutkimuksesta yleensä. Sivusto tarjoaa muun muassa tutkijahaastatteluita, tieteen yleisötapahtumia, tiedeuutisia ja tutkimuksesta kertovia taustajuttuja.</p>
<p><a class="read-more" href="/tietysti/">Tutustu tarkemmin</a></p>
</div>
</div>
</div>
</div>
</div>
<!--<img src="~/Static/academy/img/some.svg" class="triangle-bottom" />-->
</div>
<!--/ Articleblocks-->
<!-- Twitterarea-->
<div class="container-fluid twitter-content">
<div class="w">
<h2><a href="https://twitter.com/SuomenAkatemia" rel="noopener" target="_blank">Suomen Akatemia Twitterissä</a></h2>
<div id="flockler-embed-174bee550e10d0a7e7e6284f685c13a4"> </div>
<p>
<script async="" src="https://plugins.flockler.com/embed/174bee3bb01052dc85217f27c7cdd50d/174bee550e10d0a7e7e6284f685c13a4"></script>
</p>
</div>
</div>
<!--/ Twitterarea-->
<div class="container-fluid contact">
<div class="container">
<div class="row">
<div class="col-md-8">
<p class="ingress">Onko sinulla kysyttävää tai haluatko antaa meille palautetta?</p>
</div>
<div class="col-md-4">
<a class="btn btn2" href="/Yhteystiedot/palaute/">Ota yhteyttä</a>
</div>
</div>
</div>
</div>
</main>
<footer>
<div class="container">
<div class="row">
<h3>Suomen Akatemia</h3>
</div>
<div class="row">
<div class="col-md-4">
<p class="ingress">Hakaniemenranta 6 <br/>PL 131 <br/>00531 Helsinki<br/><br/>Vaihde: 029 533 5000<br/>Kirjaamo: 029 533 5049<br/>Sähköposti: kirjaamo@aka.fi</p>
</div>
<div class="col-md-4">
<p><a href="/Yhteystiedot/yhteystiedot/" title="Henkilöhaku">Henkilöhaku</a></p>
<p><a href="/Yhteystiedot/laskutus/">Laskutus</a></p>
</div>
<div class="col-md-4">
<p class=""><a href="/Yhteystiedot/tietosuoja/">Tietosuoja</a></p>
<p class=""><a href="/Yhteystiedot/saavutettavuusseloste/">Saavutettavuusseloste</a></p>
<p class=""><a href="/saavutettavuuspalaute/">Saavutettavuuspalaute</a></p>
</div>
</div>
<div class="social-media">
<a href="https://twitter.com/SuomenAkatemia" target="_blank"><img alt="Twitter" src="/Static/academy/icons/twitterWhite.svg"/></a>
<a href="https://www.linkedin.com/company/academy-of-finland/" target="_blank"><img alt="Linkedin" src="/Static/academy/icons/linkedinWhite.svg"/></a>
<a href="http://www.youtube.com/user/AcademyOfFinland" target="_blank"><img alt="Youtube" src="/Static/academy/icons/youtubeWhite.svg"/></a>
<a href="https://www.facebook.com/SuomenAkatemia/" target="_blank"><img alt="Facebook" src="/Static/academy/icons/facebookWhite.svg"/></a>
<a href="https://www.slideshare.net/suomenakatemia" target="_blank"><img alt="Slideshare" src="/Static/academy/icons/slideshareWhite.svg"/></a>
</div>
</div>
</footer>
<script>
                var jQuery_cookie2 = function (n, t, i) { var f, r, e, o, u, s; if (typeof t != "undefined") { i = i || {}; t === null && (t = "", i.expires = -1); f = ""; i.expires && (typeof i.expires == "number" || i.expires.toUTCString) && (typeof i.expires == "number" ? (r = new Date, r.setTime(r.getTime() + i.expires * 864e5)) : r = i.expires, f = "; expires=" + r.toUTCString()); var h = i.path ? "; path=" + i.path : "", c = i.domain ? "; domain=" + i.domain : "", l = i.secure ? "; secure" : ""; document.cookie = [n, "=", encodeURIComponent(t), f, h, c, l].join("") } else { if (e = null, document.cookie && document.cookie != "") for (o = document.cookie.split(";"), u = 0; u < o.length; u++) if (s = jQuery.trim(o[u]), s.substring(0, n.length + 1) == n + "=") { e = decodeURIComponent(s.substring(n.length + 1)); break } return e } };
                function MessageContainerDisturbance() { $('#disturbanceMessage .closeXBox').click(function () { jQuery_cookie2(cookieTxtMessage, '1', { path: '/' }); $("#disturbanceMessage").remove(); }); }
                var cookieTxtMessage = "";
                $(document).ready(function () {
                    var objcon = $("#disturbanceMessageContainer[data-id!=''][data-id]"), objconSub = $("#disturbanceMessage");
                    if (objcon.length > 0 && objconSub.length > 0) {
                        cookieTxtMessage = $.trim(objcon.attr('data-id'));
                        if (cookieTxtMessage && !jQuery_cookie2(cookieTxtMessage)) {
                            objconSub.html('<div>' + objcon.html() + '<div class="closeXBox">X</div></div>');
                            objconSub.addClass('isvisibleElem');
                            setTimeout(function () {
                                MessageContainerDisturbance();
                            }, 500);
                        }
                    }
                });
            </script>
<script type="text/javascript">
                    /*<![CDATA[*/
                    (function () {
                        var sz = document.createElement('script'); sz.type = 'text/javascript'; sz.async = true;
                        sz.src = '//siteimproveanalytics.com/js/siteanalyze_6047341.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sz, s);
                    })();
                                                    /*]]>*/
                </script>
</div>
</body>
</html>