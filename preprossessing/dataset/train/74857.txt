<!DOCTYPE html>
<html dir="ltr" lang="en" xmlns:fb="http://ogp.me/ns/fb#" xmlns:og="http://opengraphprotocol.org/schema/">
<head>
<link href="http://www.w3.org/1999/xhtml/vocab" rel="profile"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Drupal 7 (http://drupal.org)" name="Generator"/>
<link href="/rituals/roman/roman-holidays3.html" rel="canonical"/>
<link href="/node/4334" rel="shortlink"/>
<meta content="This article is the sixth of a series of articles outlining the basics of a Roman focus of worship and practice, and the third in a series about the Roman festival calendar, the first two covering the" property="og:description"/>
<meta content="https://www.adf.org/sites/all/themes/bootstrap/images/default-share.png" property="og:image"/>
<meta content="https://www.adf.org/rituals/roman/roman-holidays3.html" property="og:url"/>
<meta content="ADF: Ár nDraíocht Féin: A Druid Fellowship" property="og:site_name"/>
<link href="https://www.adf.org/system/files/logo.png" rel="shortcut icon" type="image/png"/>
<meta content="Drupal 7 (https://www.drupal.org)" name="generator"/>
<link href="https://www.adf.org/rituals/roman/roman-holidays3.html" rel="canonical"/>
<link href="https://www.adf.org/node/4334" rel="shortlink"/>
<meta content="summary" name="twitter:card"/>
<meta content="https://www.adf.org/rituals/roman/roman-holidays3.html" name="twitter:url"/>
<title>Major Holidays of Rome October (Mensis October) | ADF: Ár nDraíocht Féin: A Druid Fellowship</title>
<style>
@import url("https://www.adf.org/modules/system/system.base.css?qmuo81");
</style>
<style>
@import url("https://www.adf.org/sites/all/modules/date/date_api/date.css?qmuo81");
@import url("https://www.adf.org/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?qmuo81");
@import url("https://www.adf.org/modules/field/theme/field.css?qmuo81");
@import url("https://www.adf.org/sites/all/modules/filetree/filetree.css?qmuo81");
@import url("https://www.adf.org/modules/node/node.css?qmuo81");
@import url("https://www.adf.org/sites/all/modules/views/css/views.css?qmuo81");
@import url("https://www.adf.org/sites/all/modules/autofloat/css/autofloat.css?qmuo81");
</style>
<style>
@import url("https://www.adf.org/sites/all/libraries/bootstrap/css/bootstrap.min.css?qmuo81");
@import url("https://www.adf.org/sites/all/libraries/bootstrap/css/bootstrap-theme.min.css?qmuo81");
@import url("https://www.adf.org/sites/all/modules/colorbox/styles/default/colorbox_style.css?qmuo81");
@import url("https://www.adf.org/sites/all/modules/ctools/css/ctools.css?qmuo81");
@import url("https://www.adf.org/sites/all/modules/custompage/custompage.css?qmuo81");
@import url("https://www.adf.org/files/css/menu_icons.css?qmuo81");
@import url("https://www.adf.org/sites/all/modules/panels/css/panels.css?qmuo81");
@import url("https://www.adf.org/sites/all/modules/print/print_ui/css/print_ui.theme.css?qmuo81");
@import url("https://www.adf.org/sites/all/modules/social_media_links/social_media_links.css?qmuo81");
</style>
<style>
@import url("https://www.adf.org/sites/all/themes/bootstrap/css/style.css?qmuo81");
</style>
<style media="screen and (max-width: 575px)">
@import url("https://www.adf.org/sites/all/themes/bootstrap/css/575.css?qmuo81");
</style>
<style media="screen and (min-width: 576px) and (max-width: 603px)">
@import url("https://www.adf.org/sites/all/themes/bootstrap/css/603.css?qmuo81");
</style>
<style media="screen and (min-width: 604px) and (max-width: 768px)">
@import url("https://www.adf.org/sites/all/themes/bootstrap/css/768.css?qmuo81");
</style>
<style media="screen and (min-width: 769px)">
@import url("https://www.adf.org/sites/all/themes/bootstrap/css/769.css?qmuo81");
</style>
<style media="screen and (min-width: 991px)">
@import url("https://www.adf.org/sites/all/themes/bootstrap/css/991.css?qmuo81");
</style>
<link href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" media="all" rel="stylesheet" type="text/css"/>
<!-- HTML5 element support for IE6-8 -->
<!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write("<script src='/sites/all/modules/jquery_update/replace/jquery/1.9/jquery.min.js'>\x3C/script>")</script>
<script src="https://www.adf.org/misc/jquery-extend-3.4.0.js?v=1.9.1"></script>
<script src="https://www.adf.org/misc/jquery-html-prefilter-3.5.0-backport.js?v=1.9.1"></script>
<script src="https://www.adf.org/misc/jquery.once.js?v=1.2"></script>
<script src="https://www.adf.org/misc/drupal.js?qmuo81"></script>
<script src="https://www.adf.org/sites/all/modules/filetree/filetree.js?qmuo81"></script>
<script src="https://www.adf.org/sites/all/libraries/bootstrap/js/bootstrap.min.js?qmuo81"></script>
<script src="https://www.adf.org/sites/all/libraries/colorbox/jquery.colorbox-min.js?qmuo81"></script>
<script src="https://www.adf.org/sites/all/modules/colorbox/js/colorbox.js?qmuo81"></script>
<script src="https://www.adf.org/sites/all/modules/colorbox/styles/default/colorbox_style.js?qmuo81"></script>
<script src="https://www.adf.org/sites/all/modules/colorbox/js/colorbox_load.js?qmuo81"></script>
<script src="https://www.adf.org/sites/all/modules/colorbox/js/colorbox_inline.js?qmuo81"></script>
<script src="https://www.adf.org/sites/all/themes/bootstrap/js/custom-navbar.js?qmuo81"></script>
<script src="https://www.adf.org/sites/all/themes/bootstrap/../../libraries/simplemde-markdown-editor/dist/simplemde.min.js?qmuo81"></script>
<script src="https://www.adf.org/sites/all/themes/bootstrap/js/custom-print.js?qmuo81"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"bootstrap","theme_token":"rQJBkCXSf-GGcMICB4oSslXTrKY8bCXNbJSG4PfPM5U","js":{"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/1.9.1\/jquery.min.js":1,"0":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery-html-prefilter-3.5.0-backport.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/filetree\/filetree.js":1,"sites\/all\/libraries\/bootstrap\/js\/bootstrap.min.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_load.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_inline.js":1,"sites\/all\/themes\/bootstrap\/js\/custom-navbar.js":1,"sites\/all\/themes\/bootstrap\/..\/..\/libraries\/simplemde-markdown-editor\/dist\/simplemde.min.js":1,"sites\/all\/themes\/bootstrap\/js\/custom-print.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/filetree\/filetree.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/autofloat\/css\/autofloat.css":1,"sites\/all\/libraries\/bootstrap\/css\/bootstrap.min.css":1,"sites\/all\/libraries\/bootstrap\/css\/bootstrap-theme.min.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/custompage\/custompage.css":1,"public:\/\/css\/menu_icons.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"sites\/all\/modules\/print\/print_ui\/css\/print_ui.theme.css":1,"sites\/all\/modules\/social_media_links\/social_media_links.css":1,"sites\/all\/themes\/bootstrap\/css\/style.css":1,"sites\/all\/themes\/bootstrap\/css\/575.css":1,"sites\/all\/themes\/bootstrap\/css\/603.css":1,"sites\/all\/themes\/bootstrap\/css\/768.css":1,"sites\/all\/themes\/bootstrap\/css\/769.css":1,"sites\/all\/themes\/bootstrap\/css\/991.css":1,"https:\/\/use.fontawesome.com\/releases\/v5.7.2\/css\/all.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px","specificPagesDefaultValue":"admin*\nimagebrowser*\nimg_assist*\nimce*\nnode\/add\/*\nnode\/*\/edit\nprint\/*\nprintpdf\/*\nsystem\/ajax\nsystem\/ajax\/*"},"urlIsAjaxTrusted":{"\/rituals\/roman\/roman-holidays3.html?destination=node\/4334":true},"bootstrap":{"anchorsFix":"0","anchorsSmoothScrolling":"0","formHasError":1,"popoverEnabled":1,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"click hover focus","delay":0,"container":"body"}}});</script>
</head>
<body class="navbar-is-fixed-top html not-front not-logged-in one-sidebar sidebar-second page-node page-node- page-node-4334 node-type-ritual i18n-en">
<div id="skip-link">
<a class="element-invisible element-focusable" href="#main-content">Skip to main content</a>
</div>
<header class="navbar navbar-fixed-top navbar-default" id="navbar" role="banner">
<div class="container">
<div class="navbar-header col-md-12 text-center">
<a class="logo navbar-btn pull-left" href="/" title="Home">
<img alt="Home" src="https://www.adf.org/system/files/druid-sigil-sideways.png"/>
</a>
<button class="navbar-toggle" data-target="#navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="navbar-collapse collapse" id="navbar-collapse">
<nav role="navigation">
<ul class="menu nav col-md-10 navbar-nav navbar-left"><li class="first collapsed"><a href="/about">About</a></li>
<li class="collapsed"><a href="/people">Community</a></li>
<li class="collapsed"><a href="/training-practice">Training &amp; Practice</a></li>
<li class="leaf"><a href="/membership">Membership</a></li>
<li class="last leaf"><a href="/contact-us">Contact Us</a></li>
</ul> <ul class="usermenu menu nav navbar-nav col-md-2"><li class="first last leaf"><a href="/user/login?destination=members/index.html" title="login">Login</a></li>
</ul> </nav>
</div>
</div>
<a class="logo2 navbar-btn center-block" href="/core/index.html" title="Home">
<img alt="Home" class="center-block" src="https://www.adf.org/system/files/druid-sigil-sideways.png"/>
</a>
</header>
<div class="view view-hero-banner view-id-hero_banner view-display-id-panel_pane_1 view-dom-id-ed31b2d258bab2867f6ea177c1b3017c">
<div class="view-empty">
<div class="view view-hero-banner view-id-hero_banner view-display-id-panel_pane_2 view-dom-id-3768ca48364cef93ae408799f6e16f15">
<div class="view-content">
<div class="carousel slide" data-ride="carousel" id="carousel-hero-banner">
<!-- Indicators -->
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#carousel-hero-banner"></li>
<li class="" data-slide-to="1" data-target="#carousel-hero-banner"></li>
<li class="" data-slide-to="2" data-target="#carousel-hero-banner"></li>
<li class="" data-slide-to="3" data-target="#carousel-hero-banner"></li>
</ol>
<!-- Wrapper for slides -->
<div class="carousel-inner" role="listbox">
<div class="item active views-row views-row-1 views-row-odd views-row-first">
             <div class="bgslide-short" style="background-image: url(https://www.adf.org/files/59839722_2276826299049662_5848294621494378496_o_1.jpg);"> </div>
</div>
<div class="item views-row views-row-2 views-row-even">
             <div class="bgslide-short" style="background-image: url(https://www.adf.org/files/img_3799_1.jpg);"> </div>
</div>
<div class="item views-row views-row-3 views-row-odd">
             <div class="bgslide-short" style="background-image: url(https://www.adf.org/files/sebastien-gabriel-imlv9jlb24-unsplash.jpg);"> </div>
</div>
<div class="item views-row views-row-4 views-row-even views-row-last">
             <div class="bgslide-short" style="background-image: url(https://www.adf.org/files/tara.jpg);"> </div>
</div>
</div>
<!-- Controls -->
<a class="left carousel-control" data-slide="prev" href="#carousel-hero-banner" role="button">
<span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>
<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" data-slide="next" href="#carousel-hero-banner" role="button">
<span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>
<span class="sr-only">Next</span>
</a>
</div>
</div>
</div> <!-- /.view -->
</div>
</div> <!-- /.view -->
<div class="main-container container">
<header id="page-header" role="banner">
</header> <!-- /#page-header -->
<div class="row">
<section class="col-sm-9">
<a id="main-content"></a>
<h1 class="page-header">Major Holidays of Rome October (Mensis October)</h1>
<div class="region region-content">
<section class="block block-system clearfix" id="block-system-main">
<div class="bootstrap-threecol-stacked">
<div class="row">
<div class="panel-pane pane-entity-view pane-node">
<h1 class="pane-title">
      Major Holidays of Rome October (Mensis October)    </h1>
<div class="pane-content">
<div class="node node-ritual node-promoted view-mode-full">
<div class="row">
<div class="col-sm-12 ">
<div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><p><i>This article is the sixth of a series of articles outlining the basics of a Roman focus of worship and practice, and the third in a series about the Roman festival calendar, the first two covering the months of April through September. Future topics will probably include the remainder of the Roman festival calendar, early Roman Gods and Goddesses, and Roman divination (and any other topics that may be inspired or requested). In this article, I will discuss the holidays and festivals of October, along with a bit of the lore attached to each.</i></p><p>October marks the end of the campaign season, and so there are related festivals in honor of Mars , the October Horse, and the purification of the army. In the early days when campaigning occurred only during the summer months, it was important that the warriors be purified before leaving the city to go on campaign, but also upon returning so that they could then be fit inhabitants of the city again. Once the soldiers were allowed to enter the pomerium (the sacred boundary around the city), they would join the farmers in cleaning, sharpening, and purifying their tools and weapons, to be put away for the winter.</p><p>The agricultural season in northern Mediterranean climates comes to a close in October – at least for the most important of crop in Rome: grapes. The College of Pontiffs of ancient Rome usually set the vintage for the wine harvest in October, although the Codex Theodosianus allows the time to be set anytime from August 23 to October 15.</p><h4>Kalendris Octobris (Kalends of October - October 1) Fidei in Capitolio, Tigillo Sororio ad Compitum Acili, Juno Sororio, Janus Curiatius dies nefastus (no legal or political business could take place).</h4><p>Fides is honored on this date in commemoration of the temple dedication on the Capitoline Hill to Her by Aulus Atilius Calatinus during his consulship in the mid-3rd century. Occasionally, the temple was used as a meeting place for the Senate or for the making of significant oaths, contracts or treaties, and copies of Rome's international treaties hung on its walls.</p><p>Fides is the personification of good faith, and therefore verbal contracts and treaties. Worship of Fides was established by King Numa in the regal era by instituting its flamens, or priests, in the state religion. While idea of <i>fides</i> was probably first seen as an attitude of the gods toward men, rather than human loyalty to any deities, it really has more to do with the reliability of the gods and reinforces the concept of <i>Do ut Des</i>, the Roman religious contractual formula, "I give so that you may give."</p><p>Fides is portrayed as a goddess with Her right hand bound or gloved by white cloth. From the times of King Numa, on the first of October, Her <i>flamens</i> were conveyed to her shrine in a covered, two-horse carriage, their hands wrapped in white as a symbol of good faith, "as a sign that faith must be kept, and that even in men's clasped hands her seat is sacred" (Livy, <i>Ab Urbe Condita</i>, 1.21.4). Sacrifices made to Fides must be offered with the right hand covered by a white cloth or wearing a white glove.</p><p>Also on the Kalends of October, sacrifices were made to Tigillo Sororio, Juno Sororio, and Janus Curiatius, all of which seem to be somewhat related, at least in lore. <i>Tigillo Sororio</i> literally means the "sister beam." Originally, it was a horizontal beam placed on two uprights under which one could pass beneath for purification. Livy accounts a heroic tale in which a young man, Horatius was acquitted of his sister's murder, but in order to purge himself of the blood guilt, he was made to pass with covered head under a <i>tigillum</i> (beam) across the street erected by his father, which accounts for the etymology of the "sister's beam." Dionysius of Halicarnassus (<i>Roman Antiquities</i>, 3.22) added that before Horatius passed under this yoke, King Tullus Hostilius ordered the Pontifices to erect two altars and sacrifice upon them, one to Iuno Sororia and another to Ianus Curiatius. Juno Sororio is associated with the puberty of girls, while Janus Curiatius has to do with the passage of boys into manhood, each to be honored in this legend with respect to the murdered sister and Horatius, and each being associated with adolescent rites of passage.</p><p>As the existence of the <i>tigellum</i> actually predates the time of Horatius, it is likely that it at one time marked an ancient gateway into the city, under which soldiers returning to the city from their campaigning must first pass in order to release their warlike passions and resume their roles as <i>Quirites</i> (people of Rome).</p><h4>Ante Diem IV Nonas Octobras (four days before the Nones of October - October 4) Ieiumium Cereris (Fast of Ceres) dies comitalis (citizens may vote on political or criminal matters).</h4><p>In 191 BCE, a series of prodigies, or odd events thought to be worthy of attention, occurred. In Carinae, someone noted a couple of oxen climbing up the stairs on to the flat roof of a building, odd behavior indeed. The unfortunate bovines were ordered by the consulting haruspices to be burnt alive and their ashes thrown into the Tiber, but there were other strange portents that apparently occurred simultaneously throughout the lands, including several showers of stones reported from Terracina and Ameriternum, the temple of Jupiter at Menturnae being struck by lightning, and two ships in the mouth of the river at Volturnus being similarly struck and burned.</p><p>As a result of this accumulation of strange portents, the Senate ordered the <i>Decemviri</i> to consult the Sibylline books. The <i>Decemviri</i> then ordained that every five years a fast day in honor of Ceres would be observed. By the time of Augustus the fast had come to be celebrated annually and its date fixed on October 4th.</p><h4>Ante Diem III Nonas Octobras (three days before the Nones of October - October 5) Mundus Patet (The Mundus is opened) dies comitalis (but with a caveat).</h4><p>This is one of the three times a year (the other two dates being August 24 and November 8) when the <i>mundus</i>, the gate to the underworld, is opened so that the dead may communicate with the living, when, according to Varro, "it is as if the door of the grim, infernal deities were open." Although the calendars indicates that the day technically is a <i>dies comitalis</i>, no public business could be performed, no battles could be fought, no ships could set sail, and no marriages could take on days when the <i>mundus</i> was opened; all the underworld spirits could roam the land, and therefore any actions undertaken on such a day would be inauspicious.</p><h4>Nonas Octobras (the Nones of October - October 7) Iovi Fulgarii (Jove/Jupiter Fulgar), Iononi Quiriti (Juno Curriti or Quiriti) dies fastus (legal action is permitted).</h4><p>Iove Fulagar is the appellation of Jupiter who hurls the thunderbolts during the daytime (as opposed to Summanus, who hurls thunderbolts during the night and is honored on June 20). On this date, a shrine – probably open to the sky, as logic would dictate – was dedicated to Iovi Fulgar in the Campus Martius.</p><p>Also on this date, Juno Curriti was brought from the town of Falerii in 241 BCE (or possible before even then) by the rite of <i>evocatio</i>. One of the early Romans' more creative means of bringing their Italian neighbors under Roman rule was to lure away their gods by tempting them with promises of temples and continuing worship far exceeding that which they were accustomed in the towns of their patronage. Sometimes also referred to as "Juno, Protectress of Spearmen" Juno Curriti is the only deity whose cult is known to have been universal throughout all the <i>curiae</i> (tribes) of Rome. To her are offered first fruits of the harvest, spelt and barley cakes, and wine.</p><h4>Ante Diem VI Idus Octobres (six days before the Ides of October - October 10) Iunoni Monetae (Juno the Warner) dies comitalis.</h4><p>This date marks the rededication of the temple to Juno Moneta on the Arx of the Capitoline Hill, originally dedicated on June 1. The temple was erected in gratitude for that appellation of Juno who caused the geese to give warning by their hissing of attacking Gauls in 390 BCE. Unlike the city's dogs, who were reviled thereafter as the for not giving any such warning, a flock of sacred geese was kept by the temple priests for their part in preventing a total rout that day.</p><h4>Ante Diem V Idus Octobres (five days before the Ides of October -October 11) Meditrinalia nefastus publicus (public festival day).</h4><p>Though Varro theorized that the Meditrinalia comes from <i>mederi</i> ("to heal"), in fact it more likely derives from a non-Indo-European word for "wine press," which would explain etymologically why this date marks a wine tasting festival. The grape harvest is complete, and so an offering of wines of new and old vintages is made in order to be healed of all manner of illness. It is an old festival, important to early agricultural Rome, but not well understood, as it was more of a pastoral festival, rather than urban.</p><h4>Ante Diem III Idus Octobres (three days before the Ides of October - October 13) FONTINALIA nefastus publicus.</h4><p><i>Fons</i> is the God of springs, the son-in-law of Volturnus, husband of Juturna. On this day garlands are hung at the springs and wells throughout Rome. The custom of dropping coins into wells and fountains had been well established in even ancient Roman times, as all sources of water are especially venerated by those who rely upon them.</p><h4>Idus Octobres (Ides of October - October 15) Feria Iuvi (Feast of Jupiter), Equus October (October Horse), Ludi Capitolini (Capitoline Games) nefastus publicus.</h4><p>The Ides of each month are sacred to Jupiter, but the ides of October are something special, indeed, and the events dedicated to Mars on this day seem to have superceded any attention to Jupiter. On this day, chariot races in honor of Mars were held on the Campus Martius. The right horse of the winning pair was sacrificed by the <i>flamen Martialis</i> on an altar to Mars in the Campus Martius. Before being sacrificed, the horse's head was adorned with loaves of bread, meant to acknowledge and thank Mars for protecting the harvest, recalling that Mars was first and foremost an agricultural protector god, rather than merely a warrior god. After the horse was sacrificed, its head was cut off and decorated with cakes, while residents of the Via Sacra and the Subura, two neighborhoods of Rome, in a friendly rivalry vied for possession of the head; if the folks from the Via Sacra got it, they nailed it to the wall of the Regia; if the folks of the Subura got it, they nailed it to the Turris Mamilia, a tradition that unfortunately died out by the first century BCE. Meanwhile priests collected the tail, (and possibly genitals) of the sacrificed horse, while it was still dripping blood, and rushed it to the Regia (the king's house) to bleed on the sacred hearth. The Vestal Virgins then collected and retained the congealed blood and ashes for distribution at Parilia, on April 21.</p><p>Also on this date, the Capitoline games are celebrated. These games are probably quite ancient, since their institution was attributed to either Romulus for saving the Capitoline Hill from being captured by the Gauls or Camillus for the conquest of the Veii – it's very unclear which or both, and most Romans could probably care less about the origins of games, as long as they continued each year. It was a time to take off work and enjoy the pageantry and colorfulness of the festival time, much as we Buckeye fans enjoy the pageantry and excitement of football Saturdays here in Columbus.</p><h4>October 19 ARMILUSTRIUM (Cleansing of the implements of war) - See also March 19 nefastus publicus.</h4><p>A bunch of blood-thirsty, gritty warriors with nothing to do during the winter months would be a very bad thing. In order to prevent the city and its people from becoming infected by contact with bloodshed and foreigners, the Sailian priests (priests of Mars) dance and sing in the streets hymns to Mars. The <i>Armilustrium</i> is actually the tail end of the Salii's dance. In a great <i>lustratio</i>, or purification rite, on the Aventine Hill, the <i>tubae</i>, the sacred war trumpets, are sounded one last time, as the <i>arma</i> and <i>ancilla</i> (other sacred implements of war) are purified and put away until next year, all accompanied by sacrifices, songs, and dancing in honor of Mars.</p><h5>SELECT BIBLIOGRAPHY:</h5><p>Adkins, Lesley and Roy A. Adkins. <i>Dictionary of Roman Religion</i>. New York: Facts on File, 1996.</p><p>Grant, Michael. <i>Roman Myths</i>. New York: Charles Scribner's Sons, 1971.</p><p>Livy (Titius Livius). <i>History of Rome: Books IIIIV</i>. B.O. Foster, trans. Cambridge, MA: Harvard University Press, 1922.</p><p>Macrobius (Macrobius Ambrosius Theodosius). <i>Saturnalia</i>. Percival Vaughn Davies, trans. New York: Columbia University Press, 1969.</p><p>Scullard, H.H. <i>A History of the Roman World 753 to 146 BC</i>. London: Routledge, 1980.</p><p>Turcan, Robert. <i>The Gods of Ancient Rome: Religion in Everyday Life from Archaic to Imperial Times</i>. Antonia Nevell, trans. New York: Routledge, 2000.</p></div></div></div> </div>
</div>
</div>
<!-- Needed to activate display suite support on forms -->
</div>
</div>
<div class="panel-separator"></div><div class="panel-pane pane-custom pane-1">
<div class="pane-content">
<p> </p>
<p><strong>Page Information:</strong><br/>"Major Holidays of Rome October (Mensis October)." submitted by <a href="https://www.adf.org/users/JenniHunt">JenniHunt</a> on 15 May, 2019. Last modified on 12 January, 2021.<br/>Page URL: <a href="https://www.adf.org/rituals/roman/roman-holidays3.html">https://www.adf.org/rituals/roman/roman-holidays3.html</a></p>
<p><strong>Related Pages: </strong> <a href="/ritual-categories/reference">Reference</a> <a href="/grove-hearth-cultures/roman">Roman</a> </p> </div>
</div>
</div>
</div>
</section>
<section class="block block-block clearfix" id="block-block-10">
<hr width="25%"/><p style="text-align: center;"><strong><a href="https://docs.google.com/forms/d/e/1FAIpQLSdwhO-a-HR45I4_9oS2n3RBvkU8x3UIiARrNTpFyIlctlZbsg/viewform?usp=pp_url&amp;entry.1378865123=Describe+the+problem+with+this+page&amp;entry.1432994978=https://www.adf.org/rituals/roman/roman-holidays3.html&amp;entry.245970529=Summary+of+the+page+issues" rel="noopener" target="_blank">Request Update or Rate this page</a> ~ <a href="https://docs.google.com/forms/d/e/1FAIpQLSdwhO-a-HR45I4_9oS2n3RBvkU8x3UIiARrNTpFyIlctlZbsg/viewform?usp=pp_url&amp;entry.1378865123=Please+Archive+this+page&amp;entry.1391149249=Check+to+flag+for+review+to+archive+this+page&amp;entry.1972437392=1&amp;entry.1432994978=https://www.adf.org/rituals/roman/roman-holidays3.html" rel="noopener" target="_blank">Flag for Archive</a> ~ </strong><a href="https://docs.google.com/forms/d/e/1FAIpQLSdwhO-a-HR45I4_9oS2n3RBvkU8x3UIiARrNTpFyIlctlZbsg/viewform?usp=pp_url&amp;entry.1378865123=Highlight+this+article+to+be+featured&amp;entry.1972437392=5&amp;entry.1432994978=https://www.adf.org/rituals/roman/roman-holidays3.html" rel="noopener" target="_blank"><strong>Highlight for Featuring</strong></a><br/><strong> ~ <a href="https://forms.gle/Hg7ws7EuWPLiSTACA">Submit an article or ritual for the website</a> ~</strong></p>
</section>
</div>
</section>
<aside class="col-sm-3" role="complementary">
<div class="region region-sidebar-second">
<section class="block block-social-media-links clearfix" id="block-social-media-links-social-media-links">
<ul class="social-media-links platforms inline horizontal"><li class="facebook first"><a href="https://www.facebook.com/adfdruidry/" rel="nofollow" target="_blank" title="Facebook"><img alt="Facebook icon" src="https://www.adf.org/sites/all/modules/social_media_links/libraries/elegantthemes/PNG/facebook.png"/></a></li><li class="twitter"><a href="http://www.twitter.com/adfdruidry/" rel="nofollow" target="_blank" title="Twitter"><img alt="Twitter icon" src="https://www.adf.org/sites/all/modules/social_media_links/libraries/elegantthemes/PNG/twitter.png"/></a></li><li class="youtube"><a href="http://www.youtube.com/user/adfutube" rel="nofollow" target="_blank" title="YouTube"><img alt="YouTube icon" src="https://www.adf.org/sites/all/modules/social_media_links/libraries/elegantthemes/PNG/youtube.png"/></a></li><li class="contact last"><a href="https://www.adf.org/contact.html" rel="nofollow" target="_blank" title="Contact"><img alt="Contact icon" src="https://www.adf.org/sites/all/modules/social_media_links/libraries/elegantthemes/PNG/email.png"/></a></li></ul>
</section>
<section class="block block-user clearfix" id="block-user-login">
<h2 class="block-title">User login</h2>
<form accept-charset="UTF-8" action="/rituals/roman/roman-holidays3.html?destination=node/4334" id="user-login-form" method="post"><div><div class="form-item form-item-name form-type-textfield form-group"> <label class="control-label" for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
<input class="form-control form-text required" id="edit-name" maxlength="60" name="name" size="15" type="text" value=""/></div><div class="form-item form-item-pass form-type-password form-group"> <label class="control-label" for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
<input class="form-control form-text required" id="edit-pass" maxlength="128" name="pass" size="15" type="password"/></div><ul><li><a href="/user/register" title="Create a new user account.">Create new account</a></li>
<li><a href="/user/password" title="Request new password via e-mail.">Request new password</a></li>
</ul><input name="form_build_id" type="hidden" value="form-RcfmZVYKEDHACXIYdD7T5Msoz3QNbGpIugWzVtn0vDQ"/>
<input name="form_id" type="hidden" value="user_login_block"/>
<div class="form-actions form-wrapper form-group" id="edit-actions"><button class="btn btn-primary form-submit" id="edit-submit" name="op" type="submit" value="Log in">Log in</button>
</div></div></form>
</section>
</div>
</aside> <!-- /#sidebar-second -->
</div>
</div>
<bottom class="footer" style="background-image: url(https://www.adf.org/files/59839722_2276826299049662_5848294621494378496_o_1.jpg);">
<section class="footer">
<footer class="footer container">
<div class="first col-xs-12 col-sm-5 text-center">
<h3>Ár nDraíocht Féin: A Druid Fellowship, Inc.</h3><h4>1147 Brook Forest Ave #355, Shorewood, IL 60404</h4><form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"><p><input name="cmd" type="hidden" value="_s-xclick"/> <input name="hosted_button_id" type="hidden" value="WE9EGAF7228TU"/> <span style="font-size: 8pt;">Donate to ADF:<br/><input alt="Donate with PayPal button" name="submit" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif" title="PayPal - The safer, easier way to pay online!" type="image"/> <br/></span><a href="https://www.adf.org/donations/donate.html"><span style="font-size: 8pt;">Donate to a specific fund</span></a></p></form> </div>
<div class="second col-xs-12 col-sm-3">
<!-- <h2>Useful Links</h2> --><ul class="wrap2"><!-- keep the wrap2 class on there or it displays strange with a long list, with a short list, take wrap2 off --></ul><p> </p><ul><li><a href="/hearth-keeper-way.html">Hearthkeepers Way</a></li><li><a href="/rituals/index.html">Rituals</a></li><li><a href="/training/dedicant/index.html">Dedicants Program</a></li><li><a href="/adf-code-conduct.html">Code of Conduct</a></li><li><a href="/user/register">Subscribe to Oak Leaves</a></li><li><a href="/groups/groves/index.html">Find a Grove</a></li><li><a href="/donations/donate.html">Support Us</a></li></ul><p style="text-align: center;">Not a Member? <a href="/user/register">Join us now.</a></p><p> </p> </div>
<div>
<div class="third col-xs-12 col-sm-4">
<h2>Problems?</h2>
<h4><a href="mailto:webmasters@adf.org">webmasters@adf.org</a></h4>
<h2>Suggestions</h2>
<h4><a href="mailto:adf-suggestion-box@adf.org">adf-suggestion-box@adf.org</a></h4>
</div>
<div class="language col-xs-12 col-sm-4">
<h3>Language</h3>
<ul class="menu nav"><li class="first leaf"><a class="menu_icon menu-11322" href="http://www.adf.org/de/" title="">DE</a></li>
<li class="leaf"><a class="menu_icon menu-11321" href="http://www.adf.org/" title="English">EN</a></li>
<li class="last leaf"><a class="menu_icon menu-11323" href="https://www.adf.org/pt-br" title="">PT-BR</a></li>
</ul> </div>
</div>
</footer>
</section>
</bottom>
<script src="https://www.adf.org/sites/all/themes/bootstrap/js/bootstrap.js?qmuo81"></script>
</body>
</html>
