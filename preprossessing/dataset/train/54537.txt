<html>
<head>
<meta content="AAA Math, recíproco, las habilidades de matemáticas, la adición, la sustracción, la multiplicación, la división, las matemáticas de ratios,measuring,money,exponents,decimals,equations,geometry,algebra,practical , las matemáticas mentales, las matemáticas de consumidor, los números que denominan, el valor del lugar, por ciento, la estimación, las fracciones, los gráficos, el valor del lugar, la estadística, los modelos" name="keywords"/>
<meta content="AAA Math presenta una amplia variedad de lecciones interactivas de aritmética. La práctica ilimitada en cada tema facilita alcanzar  un amplio dominio de los conceptos." name="description"/>
<title>AAA Math</title>
<script language="JavaScript">
<!--
function SymError(){return true;}
window.onerror = SymError;
//-->
</script>
<script language="JavaScript">
<!--
if (document.images) {
  image1on = new Image();
  image1on.src = "dark_goarrow.gif";
  image2on = new Image();
  image2on.src = "dark_goarrow.gif";	
  image3on = new Image();
  image3on.src = "dark_goarrow.gif";
  image4on = new Image();
  image4on.src = "cd_on_100.jpg";
  image1off = new Image();
  image1off.src = "dark_stoparrow.gif";
  image2off = new Image();
  image2off.src = "dark_stoparrow.gif";
  image3off = new Image();
  image3off.src = "dark_stoparrow.gif";
  image4off = new Image();
  image4off.src = "cd_off_100.jpg"
}
function changeImages() {
  if (document.images) {
    for (var i=0; i<changeImages.arguments.length; i+=2) {
      document[changeImages.arguments[i]].src = eval(changeImages.arguments[i+1] + ".src");
    }
  }
}
function wrtLmenu(){
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='alg.htm' class='rollsmallbold'>Álgebra</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='men.htm' class='rollsmallbold'>Cálculos mentales</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='cmp.htm' class='rollsmallbold'>Comparación</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='cnt.htm' class='rollsmallbold'>Contar</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='dec.htm' class='rollsmallbold'>Decimales</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='mny.htm' class='rollsmallbold'>Dinero</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='div.htm' class='rollsmallbold'>División</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='equ.htm' class='rollsmallbold'>Ecuaciones</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='sta.htm' class='rollsmallbold'>Estadística</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='est.htm' class='rollsmallbold'>Estimación</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='exp.htm' class='rollsmallbold'>Exponentes</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='fra.htm' class='rollsmallbold'>Fracciones</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='geo.htm' class='rollsmallbold'>Geometría</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='gra.htm' class='rollsmallbold'>Gráficos</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='pra.htm' class='rollsmallbold'>Matemática Práctica</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='mea.htm' class='rollsmallbold'>Medición</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='mul.htm' class='rollsmallbold'>Multiplicación</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='nam.htm' class='rollsmallbold'>Nombres de números</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='pat.htm' class='rollsmallbold'>Patrones</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='pct.htm' class='rollsmallbold'>Porcentaje</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='pro.htm' class='rollsmallbold'>Propiedades</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='rat.htm' class='rollsmallbold'>Proporciones</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='sub.htm' class='rollsmallbold'>Resta</A><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<a href='add.htm' class='rollsmallbold'>Suma</a><br>&nbsp;");
document.write("<br>&nbsp;&nbsp;&nbsp;<A HREF='plc.htm' class='rollsmallbold'>Valor posicional</A><br>&nbsp;");
}
// -->
</script>
<link href="aaa_style.css" rel="stylesheet" type="text/css"/>
</head>
<body bgcolor="#333333" bgproperties="FIXED" leftmargin="0" marginheight="0" marginwidth="0" onload="" topmargin="0">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td width="783"><table border="0" cellpadding="0" cellspacing="0" height="150" width="790"><tr><td height="95" width="275"><img alt="AAAMatematicas.com" border="0" height="95" name="aaa_logo0" src="aaa_logo_1x1.gif" width="275"/></td><td bgcolor="#333333"><table bgcolor="#333333" border="0" cellpadding="0" cellspacing="0" width="100%"><tr valign="top"><td bgcolor="#333333" height="94" valign="bottom" width="200"><span class="greenbold">Clasificado por Grado</span></td><td bgcolor="#333333" height="79"><div align="right"><span class="smallwhitebold">Estudia habilidades matemáticas básicas  <br/>Práctica Interactiva Ilimitada  <br/>Explicaciones y ejemplos  <br/>Juegos desafiantes  <br/>Cientos de páginas  </span></div></td></tr></table></td></tr><tr><td height="29" width="275"><img border="0" height="29" name="aaa_logo1" src="aaa_logo_2x1.gif" width="275"/></td> <td bgcolor="#ffce00" class="pagelinks" halign="left" width="100%"><a class="rollsmallbold" href="kinder.htm">Jardín de infantes </a>|<a class="rollsmallbold" href="grade1.htm"> Primero </a>|<a class="rollsmallbold" href="grade2.htm"> Segundo </a>|<a class="rollsmallbold" href="grade3.htm"> Tercero </a>|<a class="rollsmallbold" href="grade4.htm"> Cuarto </a>|<a class="rollsmallbold" href="grade5.htm"> Quinto </a>|<a class="rollsmallbold" href="grade6.htm"> Sexto </a>|<a class="rollsmallbold" href="grade7.htm"> Séptimo </a>|<a class="rollsmallbold" href="grade8.htm"> Octavo</a></td></tr><tr valign="middle"><td height="26" width="275"><img border="0" height="26" name="aaa_logo2" src="aaa_logo_3x1.gif" width="275"/></td><td bgcolor="#333333"><a href="mailto:spanish@aaamath.com"><img align="absbottom" alt="Contacto AAAMatematicas" border="0" height="15" name="image1" src="dark_stoparrow.gif" width="9"/></a> <span class="smallwhite"><a class="lightrollsmall" href="mailto:spanish@aaamath.com">Contacto AAAMatematicas</a> <a href="https://www.aaamatematicas.com/cd.htm"><img align="absbottom" alt="Contacto AAAMath" border="0" height="15" name="image2" src="dark_stoparrow.gif" width="9"/></a> <a class="lightrollsmall" href="https://www.aaamatematicas.com/cd.htm">Compre el CD De AAAMatematicas</a></span></td></tr></table></td></tr><tr><td bgcolor="333333"><table cellpadding="0" cellspacing="0" width="750"><tr valign="top"><td rowspan="2" width="133"><img height="2" src="shim.gif" width="133"/><br/><span class="greenbold">Clasificado por tema</span><table><tr><td bgcolor="#ffce00" cellpadding="5" class="pagelinks" halign="left"><br/> <a class="rollsmallbold" href="alg.htm">Álgebra</a><br/> <br/> <a class="rollsmallbold" href="men.htm">Cálculos mentales</a><br/> <br/> <a class="rollsmallbold" href="cmp.htm">Comparación</a><br/> <br/> <a class="rollsmallbold" href="cnt.htm">Contar</a><br/> <br/> <a class="rollsmallbold" href="dec.htm">Decimales</a><br/> <br/> <a class="rollsmallbold" href="mny.htm">Dinero</a><br/> <br/> <a class="rollsmallbold" href="div.htm">División</a><br/> <br/> <a class="rollsmallbold" href="equ.htm">Ecuaciones</a><br/> <br/> <a class="rollsmallbold" href="sta.htm">Estadística</a><br/> <br/> <a class="rollsmallbold" href="est.htm">Estimación</a><br/> <br/> <a class="rollsmallbold" href="exp.htm">Exponentes</a><br/> <br/> <a class="rollsmallbold" href="fra.htm">Fracciones</a><br/> <br/> <a class="rollsmallbold" href="geo.htm">Geometría</a><br/> <br/> <a class="rollsmallbold" href="gra.htm">Gráficos</a><br/> <br/> <a class="rollsmallbold" href="pra.htm">Matemática Práctica</a><br/> <br/> <a class="rollsmallbold" href="mea.htm">Medición</a><br/> <br/> <a class="rollsmallbold" href="mul.htm">Multiplicación</a><br/> <br/> <a class="rollsmallbold" href="nam.htm">Nombres de números</a><br/> <br/> <a class="rollsmallbold" href="pat.htm">Patrones</a><br/> <br/> <a class="rollsmallbold" href="pct.htm">Porcentaje</a><br/> <br/> <a class="rollsmallbold" href="pro.htm">Propiedades</a><br/> <br/> <a class="rollsmallbold" href="rat.htm">Proporciones</a><br/> <br/> <a class="rollsmallbold" href="sub.htm">Resta</a><br/> <br/> <a class="rollsmallbold" href="add.htm">Suma</a><br/> <br/> <a class="rollsmallbold" href="plc.htm">Valor posicional</a><br/> </td></tr></table>
</td>
<td bgcolor="#ffffff" width="10"><img rowspan="3" src="shim.gif" width="10"/>
</td>
<td bgcolor="#ffffff">
<img height="2" src="shim.gif" width="630"/><br/>
<br/><br/>
<table align="left" cellpadding="5" cellspacing="0" width="630"><tr valign="top"><td width="100%">
<p>AAA Math presenta una amplia variedad de lecciones interactivas de aritmética. 
La práctica ilimitada en cada tema facilita alcanzar  un amplio dominio de los conceptos.  
La variada gama de lecciones (de jardín de infantes a octavo grado)  posibilita el 
aprendizaje o la revisión para cualquier alumno en su propio nivel.</p>
<p>Respuestas inmediatas previenen la práctica y el aprendizaje de métodos incorrectos, 
algo que sucede frecuentemente con las tradicionales hojas de ejercicios o deberes para la casa.</p>
</td></tr>
<tr valign="top"><td width="100%">
<p> </p>
<table bgcolor="#ffffaa" border="5" cellpadding="3" summary="">
<caption><font color="#000000"><big>Esto es la Versión Estados unidos<br/>Escoja por favor la Versión Apropiada</big></font></caption>
<tr>
<td><b>La versión</b></td>
<td><a href="https://www.aaamatematicas.com"><b>Estados unidos</b></a></td>
<td><a href="https://www.aaamatematicas.com/ca/index.htm"><b>América central y Perú</b></a></td>
<td><a href="https://www.aaamatematicas.com/sa/index.htm"><b>Sudamérica y España</b></a></td>
</tr>
<tr>
<td valign="top"><b>Los países</b></td>
<td valign="top">Estados Unidos</td>
<td valign="top">El Salvador<br/>Guatemala<br/>Honduras<br/>México<br/>Nicaragua<br/>Panamá<br/>Perú<br/>Puerto Rico</td>
<td>Argentina<br/>Bolivia<br/>Chile<br/>Colombia<br/>Costa Rica<br/>Ecuador<br/>Paraguay<br/>España<br/>Uruguay<br/>Venezuela</td>
</tr>
<tr>
<td><b>Decimales</b></td>
<td>3.1416</td>
<td>3.1416</td>
<td>3,1416</td>
</tr>
<tr>
<td valign="top"><b>Las Unidades de la medida</b></td>
<td>El sistema métrico<br/>y  Sistema de medición usual en Estados Unidos </td>
<td valign="top">El sistema métrico</td>
<td valign="top">El sistema métrico</td>
</tr>
</table>
<h3><a href="https://www.aaaknow.com"><b><font color="#ff0000"></font></b><center>AAAMatematicas in English</center></a></h3>
</td></tr>
</table>
<br/>
</td>
</tr>
<tr valign="top">
<td width="10"><img src="shim.gif" width="10"/></td>
<td align="center"><div align="center">
<span class="smallwhite">© 2005 J. Banfill.   Todos los derechos reservados.<br/><a class="rollsmalllight" href="copyright.htm" target="_blank">Aviso legal</a><br/> </span></div></td>
</tr>
</table>
</td>
</tr>
</table>
<br/>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42367988-1', 'aaamatematicas.com');
  ga('send', 'pageview');

</script>
</body>
</html>
