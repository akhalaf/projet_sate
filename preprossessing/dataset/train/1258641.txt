<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>SAFA SORGUN THERMAL OTEL  WELLNESS SPA  SORGUN YOZGAT</title>
<meta content="safatermal,safasorgunotel,safaotel,sorguntermal,termalotel,safasorguntermalotel,otel,hotel,motel,yüzmehavuzu,kaplıca,termal,sorgun,yozgat,türkiye,turkey" name="keywords"/>
<meta content="Safa Sorgun Büyük Termal Hotel Welness and Spa Sizi Bekliyoruz Sudan Sebepler ile. Sorgun / Yozgat" name="”description”"/>
<meta content="SAFA SORGUN THERMAL OTEL &amp; WELLNESS SPA  " name="Title"/>
<meta content="SAFA SORGUN THERMAL OTEL  WELLNESS SPA  SORGUN YOZGAT" name="page-topic"/>
<meta content="Sorgun Büyük Termal Turizm Enerji İnşaat San. ve Tic. A.Ş." name="”author”"/>
<meta content="Sorgun Büyük Termal Turizm Enerji İnşaat San. ve Tic. A.Ş." name="”owner”"/>
<meta content="20.03.2019" name="”copyright”"/>
<link href="uploads/logo/safatermallogo.jpg" rel="shortcut icon"/>
<!-- Website's title ( it will be shown in browser's tab ), Change the website's title based on your Hotel information -->
<meta content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=no" name="viewport"/>
<link href="https://fonts.googleapis.com/css?family=Lobster%7cRaleway:400,300,100,600,700,800" rel="stylesheet" type="text/css"/><!-- Attach Google fonts -->
<link href="assets/css/styles.css?v=1" rel="stylesheet" type="text/css"/><!-- Attach the main stylesheet file -->
<link href="/assets/css/opening-modal.min.css?v=3" rel="stylesheet"/>
<script crossorigin="anonymous" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-127035041-2"></script>
<script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-127035041-2');
    </script>
</head>
<body class="homepage trans-header sticky white-datepicker">
<!-- Top Header -->
<div id="top-header">
<div class="inner-container container">
<!-- Contact Info -->
<ul class="contact-info list-inline">
<li><i class="fa fa-phone"></i>0354 414 00 00</li>
<li><i class="fa fa-fax"></i>0354 415 60 77</li>
<li><i class="fa fa-envelope-o"></i>info@safasorgunotel.com</li>
<li><a href="https://www.facebook.com/safasorguntermalhotel/"><span><i class="fa fa-facebook"></i></span></a></li>
<li><a href="https://twitter.com/SorgunBykTermal"><span><i class="fa fa-twitter"></i></span></a></li>
<li><a href="https://www.instagram.com/safasorguntermal"><span><i class="fa fa-instagram"></i></span></a>
</li>
</ul>
<!-- Language Switcher -->
<script language="javascript" src="https://safasorgunotel.com/sitediliceviri1.js"></script>
<!-- End of Language Switcher -->
</div>
</div>
<!-- End of Top Header -->
<!-- Main Header -->
<header id="main-header">
<div class="inner-container container">
<div class="left-sec col-sm-4 col-md-2 clearfix">
<!-- Top Logo -->
<div id="top-logo">
<a href="anasayfa.html"> <img src="uploads/logo/safatermallogo.jpg"/></a>
</div>
</div>
<div class="right-sec col-sm-8 col-md-10 clearfix">
<!-- Book Now -->
<a class="book-now-btn btn btn-default btn-sm" href="/rezervation">REZERVASYON</a>
<!-- Main Menu -->
<nav id="main-menu">
<ul class="list-inline">
<li class="active"><a href="index.html">Anasayfa</a>
</li>
<li><a href="#">Kurumsal</a>
<ul>
<li>
<a href="sayfa-detay-Hakkimizda.html">Hakkımızda</a>
</li>
<li>
<a href="sayfa-detay-Vizyonumuz.html">Vizyonumuz</a>
</li>
<li>
<a href="sayfa-detay-Misyonumuz.html">Misyonumuz</a>
</li>
<li><a href="/rezervation/insan-kaynaklari-is-basvuru-formu">İnsan Kaynakları</a>
</li></ul>
</li>
<li><a href="odalar.html">Odalar</a></li>
<li><a href="#">Hizmetlerimiz</a>
<ul>
<li><a href="safasayfa-detay-Cafe-Eglence.html">Cafe &amp; Eğlence</a>
</li><li>
<a href="sayfa-detay2-Dugun-Davet-Org-.html">Düğün &amp; Davet Org.</a>
</li>
<li>
<a href="sayfa-detay2-Aktiviteler-ve-Etkinlikler.html">Aktiviteler ve Etkinlikler</a>
</li>
<li>
<a href="sayfa-detay2-Kongre-Toplanti-ve-Organizasyon.html">Kongre Toplantı ve Organizasyon</a>
</li>
<li>
<a href="sayfa-detay2-Spor-Salonu.html">Spor Salonu</a>
</li>
<li>
<a href="sayfa-detay2-Restaurant.html">Restaurant</a>
</li>
<li>
<a href="sayfa-detay2-Devre-Tatil.html">Devre Tatil</a>
</li>
</ul>
</li><li><a href="#">Spa</a>
<ul>
<li><a href="safasayfa-detay-Safa-Sorgun-Termal-Spa.html">Safa Sorgun Termal Spa</a>
</li><li><a href="safasayfa-detay-Kaplicalar-ve-Yuzme-Havuzlari.html">Kaplıcalar ve Yüzme
                                    Havuzları</a>
</li><li><a href="safasayfa-detay-Tuz--Buhar---Sauna-Odalari.html">Tuz - Buhar - Sauna
                                    Odaları</a>
</li><li><a href="safasayfa-detay-Masaj.html">Masaj ve Çeşitleri</a>
</li></ul>
</li><li><a href="#">Sorgun</a>
<ul>
<li>
<a href="sayfa-detay5-Otelimize-Ulasim.html">Otelimize Ulaşım</a>
</li>
<li>
<a href="sayfa-detay5-Sorgun-Hakkinda.html">Sorgun Hakkında</a>
</li>
<li>
<a href="sayfa-detay5-Gezilecek-Gorulecek-Yerler.html">Gezilecek Görülecek Yerler</a>
</li>
<li>
<a href="sayfa-detay5-Yemek-Kulturu.html">Yemek Kültürü</a>
</li>
</ul>
</li><li><a href="haberler.html">Haberler</a>
</li>
<li><a href="#">Toplantı ve Org.</a>
<ul>
<li>
<a href="toplanti-detay-Bozok-Salonu.html">Bozok Salonu</a>
</li>
<li>
<a href="toplanti-detay-Kerkenes-Salonu.html">Kerkenes Salonu</a>
</li>
<li>
<a href="toplanti-detay-Surmeli-Salonu.html">Sürmeli Salonu</a>
</li>
<li>
<a href="toplanti-detay-Sinema-Salonu.html">Sinema Salonu</a>
</li>
<li>
<a href="toplanti-detay-Vip-Salonu.html">Vip Salonu</a>
</li>
</ul>
</li><li><a href="#">Galeri</a>
<ul>
<li><a href="foto-galeri.html">Foto Galeri</a>
</li><li><a href="sayfa-detay4-Tanitim-Videolarimiz.html">Video Galeri</a>
</li></ul>
</li>
<li><a href="iletisim.html">İletişim</a>
</li>
</ul>
</nav>
<!-- Menu Handel -->
<div class="hidden-md hidden-lg" id="main-menu-handle"><i class="fa fa-bars"></i></div>
</div>
</div>
<div class="hidden-md hidden-lg" id="mobile-menu-container"></div>
</header>
<!-- End of Main Header -->
<div aria-hidden="true" class="modal fade" id="opening-modal" role="dialog">
<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
<div class="modal-content">
<!--            <div class="modal-header">-->
<!--                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>-->
<!--            </div>-->
<div class="modal-body">
<h2><span>GÜVENLİ TURİZM SERTİFİKASI </span> </h2>
<br/>
 Otelimiz Kültür ve Turizm Bakanlığına Bağlı Türkiye Turizm ve Geliştirme Ajansı
tarafından önerilen "Güvenli Turizm Sertifikasına" sahiptir.
			<img alt="" src="/guvenliturizm.jpg"/>
</div>
<div class="modal-footer">
<button class="btn btn-secondary" data-dismiss="modal" type="button">Kapat</button>
</div>
</div>
</div>
</div>
<div id="side_card">
<a id="card_trigger"><span>&gt;&gt;</span></a>
<b style="display: block;margin-top:10px">BİLGİ VE REZERVASYON HATTI</b>
<span class="tel" style="display: inline;"><h3 style="margin-top:5px"><a href="tel:0 354 414 00 00">0 354 414 00 00</a></h3></span>
<span> <a class="btn" href="/sayfa-detay2-Devre-Tatil.html"><i aria-hidden="true" class="fa fa-angle-double-right"></i> DEVRE  TATİL  BİLGİ </a></span>
<span> <a class="btn" href="/rezervation/bilgi-talep-iletisim-formu"><i aria-hidden="true" class="fa fa-bookmark"></i> Bilgi ve Talep Formu </a> </span>
<span> <a class="btn" href="/rezervation/insan-kaynaklari-is-basvuru-formu/"><i aria-hidden="true" class="fa fa-bookmark"></i> İnsan  Kaynakları       </a> </span>
<span> <a class="btn" href="/iletisim.html"><i aria-hidden="true" class="fa fa-bookmark"></i>               İletişim           </a> </span>
</div>
<script>

    $('#card_trigger').click(function () {
        $('#side_card').toggleClass('active');
    });
    ;

</script>
<!-- Top Slider and Booking form -->
<div id="home-top-section">
<br/>
<br/>
<br/>
<br/>
<br/>
<!-- Main Slider -->
<div id="main-slider"> <a href="">
<img alt="3" class="slider-resim" src="uploads/slider/safa_giris_bina.jpg"/>
<!-- Change the URL section based on your image\'s name -->
</a><a href="">
<img alt="3" class="slider-resim" src="uploads/slider/IMG_2517.jpg"/>
<!-- Change the URL section based on your image\'s name -->
</a><a href="">
<img alt="3" class="slider-resim" src="uploads/slider/4.jpg"/>
<!-- Change the URL section based on your image\'s name -->
</a><a href="">
<img alt="3" class="slider-resim" src="uploads/slider/safaotell_2017_12_20_at_101420.jpeg"/>
<!-- Change the URL section based on your image\'s name -->
</a><a href="">
<img alt="3" class="slider-resim" src="uploads/slider/5.jpg"/>
<!-- Change the URL section based on your image\'s name -->
</a></div>
</div>
<!-- Luxury Rooms -->
<div id="luxury-rooms">
<!-- Heading box -->
<div class="heading-box">
<h2><span>Odalarımız</span></h2><!-- Title -->
<div class="subtitle"></div><!-- Subtitle -->
</div>
<!-- Room Box Container -->
<div class="room-container container">
<!-- Room box -->
<div class="room-boxes">
<img alt="" class="room-img" src="uploads/projeler/medium/5.jpg"/>
<!-- Room Image -->
<div class="room-details col-xs-6 col-md-4">
<div class="title">Standart Oda</div><!-- Room title -->
<div class="description"><!-- Room Description -->
</div>
<a class="btn btn-default" href="oda-detay-Standart-Oda.html">Detay</a>
<!-- Detail link -->
</div>
</div>
<!-- Room box -->
<div class="room-boxes right">
<img alt="" class="room-img" src="uploads/projeler/medium/IMG_5166.jpg"/>
<!-- Room Image -->
<div class="room-details col-xs-6 col-md-4">
<div class="title">Superior Oda</div><!-- Room title -->
<div class="description"><!-- Room Description -->
</div>
<a class="btn btn-default" href="oda-detay-Superior-Oda.html">Detay</a>
<!-- Detail link -->
</div>
</div>
<div class="room-boxes">
<img alt="" class="room-img" src="uploads/projeler/medium/site3.jpg"/>
<!-- Room Image -->
<div class="room-details col-xs-6 col-md-4">
<div class="title">Suit Oda</div><!-- Room title -->
<div class="description"><!-- Room Description -->
</div>
<a class="btn btn-default" href="oda-detay-Suit-Oda.html">Detay</a>
<!-- Detail link -->
</div>
</div>
<!-- Room box -->
<div class="room-boxes right">
<img alt="" class="room-img" src="uploads/projeler/medium/IMG_2503.jpg"/>
<!-- Room Image -->
<div class="room-details col-xs-6 col-md-4">
<div class="title">Apart Daire</div><!-- Room title -->
<div class="description"><!-- Room Description -->
</div>
<a class="btn btn-default" href="oda-detay-Apart-Daire.html">Detay</a>
<!-- Detail link -->
</div>
</div>
</div>
</div>
<!-- End of Luxury Rooms -->
<!-- Gallery -->
<div id="gallery">
<!-- Heading box -->
<div class="heading-box">
<h2>Fotoğraf <span>Galeri</span></h2><!-- Title -->
</div>
<!-- Gallery Container -->
<div class="gallery-container">
<div class="sort-section">
<div class="sort-section-container">
<div class="sort-handle">Filters</div>
<ul class="list-inline">
<li><a class="active" data-filter="*" href="#">Tümü</a></li>
<li><a data-filter=".restaurant-53" href="#">APARTLAR</a>
</li>
<li><a data-filter=".restaurant-52" href="#">EĞLENCE VE KAFETERYA</a>
</li>
<li><a data-filter=".restaurant-51" href="#">RESTAURANT</a>
</li>
<li><a data-filter=".restaurant-50" href="#">TOPLANTI - SEMİNER - ORGANİZASYON</a>
</li>
<li><a data-filter=".restaurant-49" href="#">SPA MERKEZİ</a>
</li>
<li><a data-filter=".restaurant-48" href="#">Lobi</a>
</li>
</ul>
</div>
</div>
<ul class="image-main-box clearfix">
<li class="item col-xs-6 col-md-3 restaurant-48">
<figure>
<img alt="11" src="uploads/galeriler/large/BW1A2299_Copy.jpg"/>
<a class="more-details" data-title="" href="uploads/galeriler/large/BW1A2299_Copy.jpg"></a>
<figcaption>
<h4><span>Resepsiyon</span></h4>
</figcaption>
</figure>
</li>
<li class="item col-xs-6 col-md-3 restaurant-48">
<figure>
<img alt="11" src="uploads/galeriler/large/IMG_4864.jpg"/>
<a class="more-details" data-title="" href="uploads/galeriler/large/IMG_4864.jpg"></a>
<figcaption>
<h4><span>Lobi</span></h4>
</figcaption>
</figure>
</li>
<li class="item col-xs-6 col-md-3 restaurant-48">
<figure>
<img alt="11" src="uploads/galeriler/large/IMG_4917.jpg"/>
<a class="more-details" data-title="" href="uploads/galeriler/large/IMG_4917.jpg"></a>
<figcaption>
<h4><span>Lobi</span></h4>
</figcaption>
</figure>
</li>
<li class="item col-xs-6 col-md-3 restaurant-48">
<figure>
<img alt="11" src="uploads/galeriler/large/IMG_5027.jpg"/>
<a class="more-details" data-title="" href="uploads/galeriler/large/IMG_5027.jpg"></a>
<figcaption>
<h4><span>Lobi</span></h4>
</figcaption>
</figure>
</li>
<li class="item col-xs-6 col-md-3 restaurant-48">
<figure>
<img alt="11" src="uploads/galeriler/large/IMG_5034.jpg"/>
<a class="more-details" data-title="" href="uploads/galeriler/large/IMG_5034.jpg"></a>
<figcaption>
<h4><span>Lobi</span></h4>
</figcaption>
</figure>
</li>
<li class="item col-xs-6 col-md-3 restaurant-48">
<figure>
<img alt="11" src="uploads/galeriler/large/LOB_3.jpg"/>
<a class="more-details" data-title="" href="uploads/galeriler/large/LOB_3.jpg"></a>
<figcaption>
<h4><span>Lobi</span></h4>
</figcaption>
</figure>
</li>
<li class="item col-xs-6 col-md-3 restaurant-48">
<figure>
<img alt="11" src="uploads/galeriler/large/LOB_5.jpg"/>
<a class="more-details" data-title="" href="uploads/galeriler/large/LOB_5.jpg"></a>
<figcaption>
<h4><span>Lobi</span></h4>
</figcaption>
</figure>
</li>
<li class="item col-xs-6 col-md-3 restaurant-51">
<figure>
<img alt="11" src="uploads/galeriler/large/RESTAURANT_1_1.jpg"/>
<a class="more-details" data-title="" href="uploads/galeriler/large/RESTAURANT_1_1.jpg"></a>
<figcaption>
<h4><span>Restaurant</span></h4>
</figcaption>
</figure>
</li>
<li class="item col-xs-6 col-md-3 restaurant-51">
<figure>
<img alt="11" src="uploads/galeriler/large/RESTAURANT_2_1.jpg"/>
<a class="more-details" data-title="" href="uploads/galeriler/large/RESTAURANT_2_1.jpg"></a>
<figcaption>
<h4><span>Restaurant</span></h4>
</figcaption>
</figure>
</li>
<li class="item col-xs-6 col-md-3 restaurant-51">
<figure>
<img alt="11" src="uploads/galeriler/large/BW1A2594yen2.jpg"/>
<a class="more-details" data-title="" href="uploads/galeriler/large/BW1A2594yen2.jpg"></a>
<figcaption>
<h4><span>Restaurant</span></h4>
</figcaption>
</figure>
</li>
<li class="item col-xs-6 col-md-3 restaurant-51">
<figure>
<img alt="11" src="uploads/galeriler/large/yen4.jpg"/>
<a class="more-details" data-title="" href="uploads/galeriler/large/yen4.jpg"></a>
<figcaption>
<h4><span>Restaurant</span></h4>
</figcaption>
</figure>
</li>
<li class="item col-xs-6 col-md-3 restaurant-51">
<figure>
<img alt="11" src="uploads/galeriler/large/yen5.jpg"/>
<a class="more-details" data-title="" href="uploads/galeriler/large/yen5.jpg"></a>
<figcaption>
<h4><span>Restaurant</span></h4>
</figcaption>
</figure>
</li>
</ul>
<a class="btn btn-default btn-sm" href="foto-galeri.html">Daha Fazla ...</a>
</div>
</div>
<!-- End of Gallery -->
<!-- Buy Theme -->
<!-- End of Buy Theme -->
<!-- Top Footer -->
<div id="top-footer">
<div id="go-up-box"><i class="fa fa-chevron-up"></i></div>
<div class="inner-container container">
<div class="widget col-xs-6 col-md-4">
<h4>SAFA SORGUN THERMAL OTEL &amp; WELLNESS SPA  </h4>
<div class="widget-content">
					Sizi Bekliyoruz Sudan Sebepler İle.				</div>
</div>
<div class="widget col-xs-6 col-md-4">
<h4>Ebülten Kayıt</h4>
<div class="widget-content">
<div class="desc">
</div>
<form action="" class="news-letter-form" method="post">
<div class="news_letter clearfix">
<input name="email" placeholder="EMAİL ADRESİNİZ" type="text"/>
<input class="btn btn-default" name="mesajgonder" type="submit" value="KAYDET"/>
</div>
</form></div>
</div>
<div class="widget col-md-4 get-in-touch">
<h4>Adres Bilgileri</h4>
<div class="widget-content">
<ul>
<li><i class="fa fa-map-marker "></i>Yeni Mahalle Sivas Bulvarı No :112<br/>  Sorgun / Yozgat / TÜRKİYE<br/></li>
<li><i class="fa fa-phone"></i>0354 414 00 00</li>
<li><i class="fa fa-envelope-o"></i>info@safasorgunotel.com</li>
</ul>
<ul class="list-inline social-icons">
<li><a href="https://www.facebook.com/safasorguntermalhotel/"><i class="fa fa-facebook "></i></a></li>
<li><a href="https://twitter.com/sorgunbyktermal"><i class="fa fa-twitter"></i></a></li>
<li><a href="https://www.instagram.com/safasorguntermal/"><i class="fa fa-instagram"></i></a></li>
<li><a href="#"><i class="fa fa-skype"></i></a></li>
</ul>
</div>
</div>
</div>
</div>
<!-- End of Top Footer -->
<!-- Footer -->
<footer id="footer">
<nav>
<ul class="list-inline">
<li><a href="anasayfa.html">Anasayfa</a></li>
<li><a href="sayfa-detay-hakkimizda.html">Kurumsal</a></li>
<li><a href="odalar.html">Odalar</a></li>
<li><a href="safasayfa-detay-Safa-Sorgun-Termal-Spa.html">Spa</a></li>
<li><a href="safasayfa-detay-Kaplicalar-ve-Yuzme-Havuzlari.html">Kaplıcalar Ve Yüzme Havuzları</a></li>
<li><a href="safasayfa-detay-Tuz--Buhar---Sauna-Odalari.html">Tuz - Buhar - Sauna Odaları</a></li>
<li><a href="sayfa-detay5-Gezilecek-Gorulecek-Yerler.html">Gezilecek Görülecek Yerler</a></li>
<li><a href="haberler.html">Duyurular</a></li>
<li><a href="foto-galeri.html">Foto Galeri</a></li>
<li><a href="iletisim.html">İletişim</a></li>
</ul>
</nav>
<div class="copy-right">
			Copyright 2019 © WeDoDizayn 		</div>
</footer>
<!-- End of Footer -->
<script src="assets/js/jquery.js" type="text/javascript"></script>
<!-- Include the js files  -->
<script src="assets/js/jquery.js" type="text/javascript"></script>
<script src="assets/js/owl.carousel.min.js" type="text/javascript"></script>
<script src="assets/js/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="assets/js/imagesloaded.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/helper.js" type="text/javascript"></script>
<script src="assets/js/template.js" type="text/javascript"></script>
<script src="assets/js/opening-modal.min.js?v=1" type="text/javascript"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		"use strict";
		// Init the Wow plugin
    	new WOW({mobile: false}).init();
	});
	</script>
<!-- End of js files and codes -->
</body>
<center><a href="http://www.sorguntermal.com" target="_blank"> <img border="0" height="150" src="termal.png" style="margin-top:10px; float:left;"/></a></center>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
</html>
