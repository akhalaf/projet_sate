<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" lang="fr-fr" xml:lang="fr-fr" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>404-Erreur: 404</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=2.0" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="http://fonts.googleapis.com/css?family=Rosario" rel="stylesheet" type="text/css"/>
<link href="https://www.sante-habitat.org/templates/gk_university/css/system/error.style1.css" rel="stylesheet" type="text/css"/>
<link href="https://www.sante-habitat.org/templates/gk_university/css/override.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="gkPage">
<div id="gkPageWrap">
<a href="./" id="gkLogo"> <img alt="Fédération Santé &amp; Habitat" src="https://www.sante-habitat.org/images/FederationSanteHabitat.png"/> </a>
<div id="frame">
<div id="errorDescription">
<h1>404</h1>
<h2>Oops, you've encountered an error </h2>
<h3>It appears the page you were looking for doesn't exist. Sorry about that.</h3>
</div>
</div>
<div id="errorboxbody"> <a href="https://www.sante-habitat.org/" title="Aller à la page d'accueil">Page d'accueil</a> |
                              <a href="mailto:contact@web-horn.com">Contact Webmaster</a> </div>
</div>
</div>
</body>
</html>