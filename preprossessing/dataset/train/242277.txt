<!DOCTYPE html>
<html dir="ltr" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://www.w3.org/1999/xhtml/vocab">
<!--[if IE]><![endif]-->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Drupal 7 (http://drupal.org)" name="Generator"/>
<link href="/home" rel="canonical"/>
<link href="/node/173" rel="shortlink"/>
<link href="/sites/default/files/favicons/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="/sites/default/files/favicons/apple-touch-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="/sites/default/files/favicons/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/sites/default/files/favicons/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/sites/default/files/favicons/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="/sites/default/files/favicons/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/sites/default/files/favicons/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="/sites/default/files/favicons/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/sites/default/files/favicons/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/sites/default/files/favicons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/sites/default/files/favicons/android-chrome-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="/sites/default/files/favicons/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="/sites/default/files/favicons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/sites/default/files/favicons/manifest.json" rel="manifest"/>
<meta content="Blue Hill Farm" name="application-name"/>
<meta content="#ffffff" name="msapplication-TileColor"/>
<meta content="#ffffff" name="theme-color"/>
<meta content="/sites/default/files/favicons/mstile-144x144.png" name="msapplication-TileImage"/>
<meta content="/mstile-70x70.png" name="msapplication-square70x70logo"/>
<meta content="/mstile-150x150.png" name="msapplication-square150x150logo"/>
<meta content="/mstile-310x150.png" name="msapplication-wide310x150logo"/>
<meta content="/mstile-310x310.png" name="msapplication-square310x310logo"/>
<title>Home | Blue Hill Farm</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<style media="all" type="text/css">
@import url("/modules/system/system.base.css?qmme3e");
@import url("/modules/system/system.menus.css?qmme3e");
@import url("/modules/system/system.messages.css?qmme3e");
@import url("/modules/system/system.theme.css?qmme3e");
</style>
<style media="all" type="text/css">
@import url("/sites/all/modules/custom/jqueryui_custom/css-ny/jquery-ui.min.css?qmme3e");
@import url("/sites/all/modules/custom/jqueryui_custom/css-ny/jquery-ui.theme.min.css?qmme3e");
</style>
<style media="all" type="text/css">
@import url("/sites/all/modules/simplenews/simplenews.css?qmme3e");
@import url("/modules/field/theme/field.css?qmme3e");
@import url("/modules/node/node.css?qmme3e");
@import url("/modules/user/user.css?qmme3e");
@import url("/sites/all/modules/views/css/views.css?qmme3e");
@import url("/sites/all/modules/back_to_top/css/back_to_top.css?qmme3e");
@import url("/sites/all/modules/ckeditor/css/ckeditor.css?qmme3e");
</style>
<style media="all" type="text/css">
@import url("/sites/all/modules/colorbox/styles/default/colorbox_style.css?qmme3e");
@import url("/sites/all/modules/ctools/css/ctools.css?qmme3e");
@import url("/sites/all/modules/lightbox2/css/lightbox.css?qmme3e");
@import url("/sites/all/modules/flexslider/assets/css/flexslider_img.css?qmme3e");
@import url("/sites/all/libraries/flexslider/flexslider.css?qmme3e");
@import url("/sites/all/modules/nice_menus/css/nice_menus.css?qmme3e");
@import url("/sites/all/themes/unsemantic/css/menu.css?qmme3e");
</style>
<style media="all" type="text/css">
/* <![CDATA[ */
div.a1796_field{display:none;visibility:hidden;}
div.aaf62_field{display:none;visibility:hidden;}
div.aaad7_field{display:none;visibility:hidden;}

/* ]]> */
</style>
<style media="all" type="text/css">
@import url("/sites/all/modules/text_resize/text_resize.css?qmme3e");
</style>
<style media="all" type="text/css">
@import url("/sites/all/themes/unsemantic/css/unsemantic-grid-responsive.css?qmme3e");
@import url("/sites/all/themes/unsemantic/css/custom-grid-responsive.css?qmme3e");
@import url("/sites/all/themes/unsemantic/css/fonts.css?qmme3e");
@import url("/sites/all/themes/unsemantic/css/styles.css?qmme3e");
@import url("/sites/all/themes/unsemantic/css/media.css?qmme3e");
@import url("/sites/all/themes/unsemantic/plugins/qtip/jquery.qtip.min.css?qmme3e");
@import url("/sites/all/themes/unsemantic/plugins/mmenu/jquery.mmenu.all.css?qmme3e");
</style>
<script src="/sites/all/modules/jquery_update/replace/jquery/1.7/jquery.min.js?v=1.7.2" type="text/javascript"></script>
<script src="/misc/jquery-extend-3.4.0.js?v=1.7.2" type="text/javascript"></script>
<script src="/misc/jquery-html-prefilter-3.5.0-backport.js?v=1.7.2" type="text/javascript"></script>
<script src="/misc/jquery.once.js?v=1.2" type="text/javascript"></script>
<script src="/misc/drupal.js?qmme3e" type="text/javascript"></script>
<script src="/sites/all/modules/custom/jqueryui_custom/js/jquery-ui.min.js?v=1.10.2" type="text/javascript"></script>
<script src="/sites/all/modules/nice_menus/js/jquery.bgiframe.js?v=2.1" type="text/javascript"></script>
<script src="/sites/all/libraries/jquery.hoverIntent/jquery.hoverIntent.js?v=r7" type="text/javascript"></script>
<script src="/sites/all/libraries/superfish/superfish.js?v=v1.4.8" type="text/javascript"></script>
<script src="/sites/all/modules/nice_menus/js/nice_menus.js?v=1.0" type="text/javascript"></script>
<script src="/sites/all/modules/jquery_update/replace/ui/external/jquery.cookie.js?v=67fb34f6a866c40d0570" type="text/javascript"></script>
<script src="/sites/all/modules/back_to_top/js/back_to_top.js?qmme3e" type="text/javascript"></script>
<script src="/sites/all/libraries/colorbox/jquery.colorbox-min.js?qmme3e" type="text/javascript"></script>
<script src="/sites/all/modules/colorbox/js/colorbox.js?qmme3e" type="text/javascript"></script>
<script src="/sites/all/modules/colorbox/styles/default/colorbox_style.js?qmme3e" type="text/javascript"></script>
<script src="/sites/all/modules/lightbox2/js/lightbox.js?qmme3e" type="text/javascript"></script>
<script src="/sites/all/libraries/easing/jquery.easing.js?qmme3e" type="text/javascript"></script>
<script src="/sites/all/libraries/flexslider/jquery.flexslider-min.js?qmme3e" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function ($) {
  Drupal.behaviors.a9338fc8c8ed2fff8af = {
    attach: function (context, settings) {
      $("input.a1796_field").each(function() {
        f=$(this)[0];
        if (f.value.indexOf("f8afad2ff")==0){f.value="61"+f.value.substring(5)+"a3702367b9e9e4de_form";}
      });
    }
  };
})(jQuery);
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function ($) {
  Drupal.behaviors.a9338fc8c8eadf0b8db = {
    attach: function (context, settings) {
      $("input.aaf62_field").each(function() {
        f=$(this)[0];
        tok2 = f.style.fontFamily;
        if(tok2.charAt(0) == "'" || tok2.charAt(0) == '"') tok2=tok2.substring(1, tok2.length-1);
        tok2=tok2.substring(1, tok2.length);
        if (f.value.indexOf("b8dadf0b8d")==0){f.value="25"+f.value.substring(3)+tok2;}
      });
    }
  };
}(jQuery));
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function ($) {
  Drupal.behaviors.a9338fc8c8ee8f5955a = {
    attach: function (context, settings) {
      $("input.aaad7_field").each(function() {
        f=$(this)[0];
        if (f.value.indexOf("955adce8f")==0){
          v=f.value.substring(6);
          $("#simplenews-block-form-1").get(0).setAttribute('action', '/?aad7_name=7c'+v+'adcf17f5e7bafad7c_form');
        }
      });
    }
  };
}(jQuery));
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var text_resize_scope = "body";
          var text_resize_minimum = "11";
          var text_resize_maximum = "20";
          var text_resize_line_height_allow = 1;
          var text_resize_line_height_min = "14";
          var text_resize_line_height_max = "28";
//--><!]]>
</script>
<script src="/sites/all/modules/text_resize/text_resize.js?qmme3e" type="text/javascript"></script>
<script src="/sites/all/modules/google_analytics/googleanalytics.js?qmme3e" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-3954979-13", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script src="/sites/all/themes/unsemantic/plugins/qtip/jquery.qtip.min.js?qmme3e" type="text/javascript"></script>
<script src="/sites/all/themes/unsemantic/plugins/dateFormat/jquery-dateFormat.min.js?qmme3e" type="text/javascript"></script>
<script src="/sites/all/themes/unsemantic/plugins/arctext/jquery.arctext.js?qmme3e" type="text/javascript"></script>
<script src="/sites/all/themes/unsemantic/scripts/jquery.mmenu.all.js?qmme3e" type="text/javascript"></script>
<script src="/sites/all/themes/unsemantic/scripts/custom.js?qmme3e" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"unsemantic","theme_token":"ldfFrxzj-mOJLiGLpRKCg2h5Q5AFoNCP48_e9cbEIKA","css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"misc\/ui\/jquery.ui.core.css":1,"misc\/ui\/jquery.ui.theme.css":1,"sites\/all\/modules\/simplenews\/simplenews.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/back_to_top\/css\/back_to_top.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/lightbox2\/css\/lightbox.css":1,"sites\/all\/modules\/flexslider\/assets\/css\/flexslider_img.css":1,"sites\/all\/libraries\/flexslider\/flexslider.css":1,"sites\/all\/modules\/nice_menus\/css\/nice_menus.css":1,"sites\/all\/themes\/unsemantic\/css\/menu.css":1,"sites\/all\/modules\/text_resize\/text_resize.css":1,"sites\/all\/themes\/unsemantic\/css\/unsemantic-grid-responsive.css":1,"sites\/all\/themes\/unsemantic\/css\/custom-grid-responsive.css":1,"sites\/all\/themes\/unsemantic\/css\/fonts.css":1,"sites\/all\/themes\/unsemantic\/css\/styles.css":1,"sites\/all\/themes\/unsemantic\/css\/media.css":1,"sites\/all\/themes\/unsemantic\/plugins\/qtip\/jquery.qtip.min.css":1,"sites\/all\/themes\/unsemantic\/plugins\/mmenu\/jquery.mmenu.all.css":1},"js":{"sites\/all\/modules\/flexslider\/assets\/js\/flexslider.load.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.7\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery-html-prefilter-3.5.0-backport.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.core.min.js":1,"sites\/all\/modules\/nice_menus\/js\/jquery.bgiframe.js":1,"sites\/all\/libraries\/jquery.hoverIntent\/jquery.hoverIntent.js":1,"sites\/all\/libraries\/superfish\/superfish.js":1,"sites\/all\/modules\/nice_menus\/js\/nice_menus.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/external\/jquery.cookie.js":1,"sites\/all\/modules\/back_to_top\/js\/back_to_top.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/lightbox2\/js\/lightbox.js":1,"sites\/all\/libraries\/easing\/jquery.easing.js":1,"sites\/all\/libraries\/flexslider\/jquery.flexslider-min.js":1,"sites\/all\/modules\/text_resize\/text_resize.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"sites\/all\/themes\/unsemantic\/plugins\/qtip\/jquery.qtip.min.js":1,"sites\/all\/themes\/unsemantic\/plugins\/dateFormat\/jquery-dateFormat.min.js":1,"sites\/all\/themes\/unsemantic\/plugins\/arctext\/jquery.arctext.js":1,"sites\/all\/themes\/unsemantic\/scripts\/jquery.mmenu.all.js":1,"sites\/all\/themes\/unsemantic\/scripts\/custom.js":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px","specificPagesDefaultValue":"admin*\nimagebrowser*\nimg_assist*\nimce*\nnode\/add\/*\nnode\/*\/edit\nprint\/*\nprintpdf\/*\nsystem\/ajax\nsystem\/ajax\/*"},"lightbox2":{"rtl":0,"file_path":"\/(\\w\\w\/)public:\/","default_image":"\/sites\/all\/modules\/lightbox2\/images\/brokenimage.jpg","border_size":16,"font_color":"fff","box_color":"000","top_position":"","overlay_opacity":"0.8","overlay_color":"000","disable_close_click":1,"resize_sequence":0,"resize_speed":400,"fade_in_speed":400,"slide_down_speed":600,"use_alt_layout":0,"disable_resize":1,"disable_zoom":0,"force_show_nav":0,"show_caption":0,"loop_items":0,"node_link_text":"View Image Details","node_link_target":0,"image_count":"Image !current of !total","video_count":"Video !current of !total","page_count":"Page !current of !total","lite_press_x_close":"press \u003Ca href=\u0022#\u0022 onclick=\u0022hideLightbox(); return FALSE;\u0022\u003E\u003Ckbd\u003Ex\u003C\/kbd\u003E\u003C\/a\u003E to close","download_link_text":"","enable_login":false,"enable_contact":false,"keys_close":"c x 27","keys_previous":"p 37","keys_next":"n 39","keys_zoom":"z","keys_play_pause":"32","display_image_size":"original","image_node_sizes":"()","trigger_lightbox_classes":"","trigger_lightbox_group_classes":"","trigger_slideshow_classes":"","trigger_lightframe_classes":"","trigger_lightframe_group_classes":"","custom_class_handler":0,"custom_trigger_classes":"","disable_for_gallery_lists":true,"disable_for_acidfree_gallery_lists":true,"enable_acidfree_videos":true,"slideshow_interval":5000,"slideshow_automatic_start":true,"slideshow_automatic_exit":true,"show_play_pause":true,"pause_on_next_click":false,"pause_on_previous_click":true,"loop_slides":false,"iframe_width":600,"iframe_height":400,"iframe_border":0,"enable_video":0,"useragent":"python-requests\/2.22.0"},"flexslider":{"optionsets":{"default":{"namespace":"flex-","selector":".slides \u003E li","easing":"swing","direction":"horizontal","reverse":false,"smoothHeight":false,"startAt":0,"animationSpeed":800,"initDelay":0,"useCSS":true,"touch":true,"video":false,"keyboard":true,"multipleKeyboard":false,"mousewheel":0,"controlsContainer":".flex-control-nav-container","sync":"","asNavFor":"","itemWidth":0,"itemMargin":0,"minItems":0,"maxItems":0,"move":0,"animation":"fade","slideshow":true,"slideshowSpeed":"5000","directionNav":false,"controlNav":false,"prevText":"Previous","nextText":"Next","pausePlay":false,"pauseText":"Pause","playText":"Play","randomize":false,"thumbCaptions":false,"thumbCaptionsBoth":false,"animationLoop":true,"pauseOnAction":false,"pauseOnHover":false,"manualControls":""}},"instances":{"flexslider-1":"default"}},"nice_menus_options":{"delay":"800","speed":"slow"},"urlIsAjaxTrusted":{"\/":true},"back_to_top":{"back_to_top_button_trigger":"100","back_to_top_button_text":"Back to top","#attached":{"library":[["system","ui"]]}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1}});
//--><!]]>
</script>
<script>
!function(t,o,c,k){if(!t.tock){var e=t.tock=function(){e.callMethod?
e.callMethod.apply(e,arguments):e.queue.push(arguments)};t._tock||(t._tock=e),
e.push=e,e.loaded=!0,e.version='1.0',e.queue=[];var f=o.createElement(c);f.async=!0,
f.src=k;var g=o.getElementsByTagName(c)[0];g.parentNode.insertBefore(f,g)}}(
window,document,'script','https://www.exploretock.com/tock.js');

tock('init', 'bluehillatstonebarns');
</script>
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-173 node-type-page">
<div id="page-container">
<div id="page-wrapper">
<div class="" id="page">
<a class="skip-main" href="#main">Skip to main content</a>
<div class="mhide" id="pre-header">
<div class="section">
<div class="region region-pre-header">
<div class="block block-system top-bar-container block-menu" id="block-system-user-menu">
<div class="content">
<ul class="menu"><li class="first leaf giftcerts"><a href="https://www.bluehillmarket.com/gifts/gift-certs" target="_blank">Gift Certificates</a></li>
<li class="leaf date"><a class="date nolink" href="#" tabindex="0">Date</a></li>
<li class="leaf"><a href="/press">Press</a></li>
<li class="leaf"><a class="wasted" href="http://www.resourcedny.com" target="_blank">resourcED</a></li>
<li class="last leaf"><a href="/search">Search</a></li>
</ul> </div>
</div>
<div class="block block-block text-center margin-top-20 mhide" id="block-block-24">
<div class="content">
<div style="background-color:#e6d5c5;padding-top:1px;padding-bottom:1px;">
<p>Introducing: Chef in Residence at Stone Barns. Reservations available now for limited indoor bookings. <a href="https://exploretock.com/stonebarnscenter" style="text-decoration:underline" target="_blank">Click here</a> to reserve. We also offer to-go boxes from soup to sundires. <a href="https://www.exploretock.com/bluehillatstonebarns" style="text-decoration:underline" target="_blank">Click here</a> to reserve your box.</p>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="clearfix" id="header">
<div class="grid-parent grid-100">
<a href="/" id="logo" rel="home" title="Home">
<img alt="Home" src="https://www.bluehillfarm.com/sites/all/themes/unsemantic/images/blue-hill-logo-big.png"/>
</a>
<div class="">
<div class="region region-header">
<div class="block block-nice-menus grid-container mhide" id="block-nice-menus-1">
<div class="content">
<ul class="nice-menu nice-menu-down nice-menu-main-menu" id="nice-menu-1"><li class="menu-1165 menu-path-sexploretockcom-bluehillatstonebarns first odd "><a href="https://www.exploretock.com/bluehillatstonebarns" target="_blank">ResourcED Boxes</a></li>
<li class="menu-424 menuparent menu-path-nolink even "><a class="nolink" href="#" tabindex="0">Reserve</a><ul><li class="menu-1166 menu-path-node-262 first odd "><a href="/chef-residence-stone-barns">Chef in Residence at Stone Barns</a></li>
<li class="menu-1152 menu-path-node-261 even last"><a href="/resourced">reserve a resourcED box</a></li>
</ul></li>
<li class="menu-218 menu-path-front odd "><a class="active" href="/" title="Blue Hill Farm Logo - Home">Home</a></li>
<li class="menu-428 menuparent menu-path-node-18 even "><a href="/events">Events</a><ul><li class="menu-636 menu-path-node-18 first odd "><a href="/events#country">Country</a></li>
<li class="menu-637 menu-path-node-18 even "><a href="/events#city">City</a></li>
<li class="menu-638 menu-path-node-18 odd "><a href="/events#on-the-road">On the Road</a></li>
<li class="menu-649 menu-path-node-203 even "><a href="/events/favors">Favors</a></li>
<li class="menu-487 menu-path-team odd last"><a href="/team">Team</a></li>
</ul></li>
<li class="menu-442 menu-path-sbluehillmarketcom odd last"><a href="https://www.bluehillmarket.com" target="_blank">Market</a></li>
</ul>
</div>
</div>
<div class="block block-block mobile-logo mshow" id="block-block-23">
<div class="content">
<div><a href="/"><img alt="Blue Hill Farm" src="/sites/all/themes/unsemantic/images/blue-hill-logo-big.png"/></a></div>
</div>
</div>
<div class="block block-system block-menu" id="block-system-main-menu">
<div class="content" id="block-main-menu">
<ul class="menu"><li class="first leaf"><a href="https://www.exploretock.com/bluehillatstonebarns" target="_blank">ResourcED Boxes</a></li>
<li class="expanded"><a class="nolink" href="#" tabindex="0">Reserve</a><ul class="menu"><li class="first leaf"><a href="/chef-residence-stone-barns">Chef in Residence at Stone Barns</a></li>
<li class="last leaf"><a href="/resourced">reserve a resourcED box</a></li>
</ul></li>
<li class="leaf"><a class="active" href="/" title="Blue Hill Farm Logo - Home">Home</a></li>
<li class="expanded"><a href="/events">Events</a><ul class="menu"><li class="first leaf"><a href="/events#country">Country</a></li>
<li class="leaf"><a href="/events#city">City</a></li>
<li class="leaf"><a href="/events#on-the-road">On the Road</a></li>
<li class="leaf"><a href="/events/favors">Favors</a></li>
<li class="last leaf"><a href="/team">Team</a></li>
</ul></li>
<li class="last leaf"><a href="https://www.bluehillmarket.com" target="_blank">Market</a></li>
</ul> </div>
</div>
</div>
<a class="mshow" href="#block-main-menu" id="mmenu_open"><div class="mmenu-bar"></div><div class="mmenu-bar"></div><div class="mmenu-bar last"></div></a>
</div>
</div>
</div>
<div class="" id="page-slideshow">
<div class="">
<div class="region region-slideshow">
<div class="block block-block text-center margin-bottom-20 mshow" id="block-block-25">
<div class="content">
<div style="background-color:#e6d5c5;padding-top:1px;padding-bottom:1px;">
<p>Introducing: Chef in Residence at Stone Barns. Reservations available now for limited indoor bookings. <a href="https://exploretock.com/stonebarnscenter" style="text-decoration:underline" target="_blank">Click here</a> to reserve. We also offer to-go boxes from soup to sundires. <a href="https://www.exploretock.com/bluehillatstonebarns" style="text-decoration:underline" target="_blank">Click here</a> to reserve your box.</p>
</div>
</div>
</div>
<div class="block block-views" id="block-views-page-slideshow-block-1">
<div class="content">
<div class="view view-page-slideshow view-id-page_slideshow view-display-id-block_1 slideshow view-dom-id-f2c03ccaa08df5fbbd6aa54d944d6b97">
<div class="view-content">
<div class="flexslider optionset-default" id="flexslider-1">
<ul class="slides"><li>
<div class="views-field views-field-field-slideshow"> <div class="field-content"><img alt="Chef in Residence at Stone Barns" height="500" src="https://www.bluehillfarm.com/sites/default/files/images/slideshow/banner_chef-in-residence-stone-barns.jpg" width="1900"/></div> </div></li>
<li>
<div class="views-field views-field-field-slideshow"> <div class="field-content"><img alt="resourcED Boxes" height="500" src="https://www.bluehillfarm.com/sites/default/files/images/slideshow/banner_resourced-boxes.jpg" width="1900"/></div> </div></li>
<li>
<div class="views-field views-field-field-slideshow"> <div class="field-content"><img alt="Private Events at Blue Hill" height="500" src="https://www.bluehillfarm.com/sites/default/files/images/slideshow/banner_private-events-bhsb_0.jpg" width="1900"/></div> </div></li>
<li>
<div class="views-field views-field-field-slideshow"> <div class="field-content"><img alt="Blue Hill Stone Barns Aerial" height="500" src="https://www.bluehillfarm.com/sites/default/files/images/slideshow/blue-hill-aerial.jpg" width="1900"/></div> </div></li>
</ul></div>
</div>
</div> </div>
</div>
<div class="block block-block" id="block-block-1">
<div class="content">
<div class="slideshow">
<div class="view-content">
<p><img alt="Know Thy Farmer" src="/sites/default/files/images/pages/ktf-mainbanner.jpg"/></p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="clearfix" id="main-wrapper">
<div class="grid-parent" id="main">
<div class="column grid-100 grid-parent" id="content">
<div class="section">
<a id="main-content"></a>
<div class="tabs grid-container">
</div>
<div class="region region-content">
<div class="block block-system" id="block-system-main">
<div class="content">
<div>
<div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="clearfix" id="footer">
<div class="grid-parent">
<div class="region region-footer">
<div class="block block-block" id="block-block-2">
<div class="content">
<div class="grid-container"><ul class="grid-100 grid-parent" id="logos">
<li class="" style="display:none"><a href="http://bluehillyogurt.com/" title="Blue Hill Yogurt"><img alt="Blue Hill Yogurt" src="/sites/all/themes/unsemantic/images/logos/bluehillyogurt-big.png" style="width:71px"/></a></li>
<li class=""><a href="/dine/new-york" title="Blue Hill New York City"><img alt="Blue Hilll New York" src="/sites/all/themes/unsemantic/images/logos/bluehillny-big.png" style="width:68px"/></a></li>
<li class=""><a href="/history" title="Blue Hill Farm"><img alt="Blue Hill Barn" src="/sites/all/themes/unsemantic/images/logos/bluehillbarn-big.png" style="width:52px"/></a></li>
<li class=""><a href="/dine/stone-barns" title="Blue Hill at Stone Barns"><img alt="Blue Hill Stone Barns" src="/sites/all/themes/unsemantic/images/logos/bluehillsb-big.png" style="width:65px;padding-top:3px"/></a></li>
<li class=""><a href="/market" title="Blue Hill Market"><img alt="Blue Hill Market" src="/sites/all/themes/unsemantic/images/logos/bluehillmarket-big.png" style="width:67px"/></a></li>
</ul></div> </div>
</div>
<div class="block block-block grid-container" id="block-block-4">
<div class="content">
<div class="center">
<a href="/know-thy-farmer"><img alt="Know thy farmer" src="/sites/default/files/images/pages/ktf-banner-big.png" style="width:268px"/></a>
</div> </div>
</div>
<div class="block block-simplenews grid-container" id="block-simplenews-1">
<div class="content">
<p>JOIN OUR MAILING LIST</p>
<form accept-charset="UTF-8" action="/" class="simplenews-subscribe" id="simplenews-block-form-1" method="post"><div><div class="a1796_field"><span class="description"> (If you're a human, don't change the following field)</span><div class="form-item form-type-textfield form-item-317-name">
<label for="edit-317-name">Enter your name </label>
<input autocomplete="off" class="a1796_field form-text" id="edit-317-name" maxlength="128" name="317_name" size="60" type="text" value="f8afad2fff8af"/>
<div class="description">Your first name.</div>
</div>
</div><noscript>Please enable Javascript to use this form.</noscript><div class="aaf62_field"><span class="description"> (If you're a human, don't change the following field)</span><div class="form-item form-type-textfield form-item-6af-name">
<label for="edit-6af-name">Enter your name </label>
<input autocomplete="off" class="aaf62_field form-text" id="edit-6af-name" maxlength="128" name="6af_name" size="60" style='font-family: "a58c85a1218645b21_form"' type="text" value="b8dadf0b8db"/>
<div class="description">Your first name.</div>
</div>
</div><noscript>Please enable Javascript to use this form.</noscript><div class="aaad7_field"><span class="description"> (If you're a human, don't change the following field)</span><div class="form-item form-type-textfield form-item-botcha">
<label for="edit-botcha">Enter your name </label>
<input autocomplete="off" class="aaad7_field form-text" id="edit-botcha" maxlength="128" name="botcha" size="60" type="text" value="955adce8f5955"/>
<div class="description">Your first name.</div>
</div>
</div><noscript>Please enable Javascript to use this form.</noscript><div class="form-item form-type-textfield form-item-mail">
<input class="form-text required" id="edit-mail" maxlength="128" name="mail" placeholder="Email address" size="20" type="text" value=""/>
</div>
<input class="btn form-submit" id="edit-submit" name="op" type="submit" value="Send"/><input name="form_build_id" type="hidden" value="form-9UuKz0KowVqxXbJiA6Y-2YLLd9nPcGp0G_wK7p8v7p4"/>
<input name="form_id" type="hidden" value="simplenews_block_form_1"/>
<input name="timegate" type="hidden" value="1610565021"/>
<input class="aaad7_field" name="4aa_name" type="hidden" value="955e8f5955"/>
</div></form>
</div>
</div>
<div class="block block-block grid-container" id="block-block-5">
<div class="content">
<p><a href="http://twitter.com/#!/bluehillfarm" rel="nofollow" target="_blank"><img alt="Twitter" src="/sites/default/files/images/pages/twitter.png"/></a><a href="http://www.facebook.com/pages/Blue-Hill-Farm/144591172271894" rel="nofollow" target="_blank"><img alt="Facebook" src="/sites/default/files/images/pages/facebook.png"/></a><a href="http://instagram.com/bluehillfarm" rel="nofollow" target="_blank"><img alt="Instagram" src="/sites/default/files/images/pages/instagram.png"/></a><a href="https://www.pinterest.com/bluehillevents/" rel="nofollow" target="_blank"><img alt="Pinterest" src="/sites/default/files/images/pages/pinterest.png"/></a></p>
</div>
</div>
</div>
</div>
</div>
<div class="grid-container" id="footer-bottom">
<div class="grid-parent">
<div class="region region-footer-bottom">
<div class="block block-menu grid-30" id="block-menu-menu-footer-menu">
<div class="content">
<ul class="menu"><li class="first leaf"><a href="/dine/stone-barns">Blue Hill at Stone Barns</a></li>
<li class="leaf"><a href="https://stonebarnscenter.org/residency/" target="_blank">CHEF IN RESIDENCE AT STONE BARNS</a></li>
<li class="last leaf"><a href="https://resourcedny.com/" target="_blank">ResourcED Boxes</a></li>
</ul> </div>
</div>
<div class="block block-menu grid-25" id="block-menu-menu-footer-menu-2">
<div class="content">
<ul class="menu"><li class="first leaf"><a href="/faq">FAQ</a></li>
<li class="leaf"><a href="/directions">Directions</a></li>
<li class="last leaf"><a href="/opportunities">Opportunities</a></li>
</ul> </div>
</div>
<div class="block block-menu grid-20" id="block-menu-menu-footer-menu-3">
<div class="content">
<ul class="menu"><li class="first leaf"><a href="/team">Team</a></li>
<li class="leaf"><a href="/history">History</a></li>
<li class="last leaf"><a href="/press">Press</a></li>
</ul> </div>
</div>
<div class="block block-menu grid-25" id="block-menu-menu-footer-menu-4">
<div class="content">
<ul class="menu"><li class="first leaf"><a class="wasted" href="/wastED">wastED</a></li>
<li class="leaf"><a href="/contact">Contact</a></li>
<li class="last leaf"><a href="/know-thy-farmer">Know Thy Farmer</a></li>
</ul> </div>
</div>
<div class="block block-block grid-100" id="block-block-3">
<div class="content">
<p>© 2021 Blue Hill Farm</p>
</div>
</div>
</div>
</div>
</div>
<div id="fixed-bottom">
<div class="region region-fixed-bottom">
<div class="block block-text-resize" id="block-text-resize-0">
<h2>Text Resize</h2>
<div class="content">
<a class="changer" href="javascript:;" id="text_resize_decrease"><sup>-</sup>A</a> <a class="changer" href="javascript:;" id="text_resize_reset">A</a> <a class="changer" href="javascript:;" id="text_resize_increase"><sup>+</sup>A</a><div id="text_resize_clear"></div> </div>
</div>
</div>
</div>
</div>
</div> <script src="/sites/all/modules/flexslider/assets/js/flexslider.load.js?qmme3e" type="text/javascript"></script>
</div>
</body>
</html>
