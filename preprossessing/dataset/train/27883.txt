<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8"/>
<!---ZONE META & INFO MOBILE-->
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<!---ZONE META & INFO MOBILE-->
<script type="text/javascript">/* <![CDATA[ */
var box_settings = {tt_img:true,sel_g:"#documents_portfolio a[type='image/jpeg'],#documents_portfolio a[type='image/png'],#documents_portfolio a[type='image/gif']",sel_c:".mediabox",trans:"elastic",speed:"200",ssSpeed:"2500",maxW:"90%",maxH:"90%",minW:"400px",minH:"",opa:"0.9",str_ssStart:"Diaporama",str_ssStop:"Arrêter",str_cur:"{current}/{total}",str_prev:"Précédent",str_next:"Suivant",str_close:"Fermer",splash_url:""};
var box_settings_splash_width = "600px";
var box_settings_splash_height = "90%";
var box_settings_iframe = true;
/* ]]> */</script>
<!-- insert_head_css --><link href="plugins-dist/mediabox/colorbox/black-striped/colorbox.css" media="all" rel="stylesheet" type="text/css"/><link href="plugins-dist/porte_plume/css/barre_outils.css?1555054697" media="all" rel="stylesheet" type="text/css"/>
<link href="local/cache-css/cssdyn-css_barre_outils_icones_css-3a2dcb60.css?1586391000" media="all" rel="stylesheet" type="text/css"/>
<link href="plugins/auto/gis/v4.43.3/lib/leaflet/dist/leaflet.css" rel="stylesheet"/>
<link href="plugins/auto/gis/v4.43.3/lib/leaflet/plugins/leaflet-plugins.css" rel="stylesheet"/>
<link href="plugins/auto/gis/v4.43.3/lib/leaflet/plugins/leaflet.markercluster.css" rel="stylesheet"/>
<link href="plugins/auto/gis/v4.43.3/css/leaflet_nodirection.css" rel="stylesheet"/>
<!--Vos feuilles de style pour l'habillage du site-->
<link href="squelettes/css/bootstrap.css" media="all" rel="stylesheet" type="text/css"/>
<link href="squelettes/css/font-face.css" media="all" rel="stylesheet" type="text/css"/>
<link href="squelettes/css/mystyle.css" media="all" rel="stylesheet" type="text/css"/>
<!-- -->
<script src="prive/javascript/jquery.js?1555054754" type="text/javascript"></script>
<script src="prive/javascript/jquery-migrate-3.0.1.js?1555054754" type="text/javascript"></script>
<script src="prive/javascript/jquery.form.js?1555054754" type="text/javascript"></script>
<script src="prive/javascript/jquery.autosave.js?1555054754" type="text/javascript"></script>
<script src="prive/javascript/jquery.placeholder-label.js?1555054754" type="text/javascript"></script>
<script src="prive/javascript/ajaxCallback.js?1555054754" type="text/javascript"></script>
<script src="prive/javascript/js.cookie.js?1555054754" type="text/javascript"></script>
<script src="prive/javascript/jquery.cookie.js?1555054754" type="text/javascript"></script>
<!-- insert_head -->
<script src="plugins-dist/mediabox/javascript/jquery.colorbox.js?1555054641" type="text/javascript"></script>
<script src="plugins-dist/mediabox/javascript/spip.mediabox.js?1555054641" type="text/javascript"></script><script src="plugins-dist/porte_plume/javascript/jquery.markitup_pour_spip.js?1555054698" type="text/javascript"></script>
<script src="plugins-dist/porte_plume/javascript/jquery.previsu_spip.js?1555054699" type="text/javascript"></script>
<script src="local/cache-js/jsdyn-javascript_porte_plume_start_js-2999aefe.js?1586426300" type="text/javascript"></script>
<link href="spip.php?page=forms_styles.css" media="all" rel="stylesheet" type="text/css"/>
<link href="plugins/auto/intertitrestdm/v0.9.10/css/intertitres_publics.css" rel="stylesheet" type="text/css"/>
<script src="http://www.utc.fr/lib/js/modernizr.js" type="text/javascript"></script>
<script src="squelettes/js/jquery.min.js" type="text/javascript"></script>
<script src="squelettes/js/bootstrap.min.js" type="text/javascript"></script>
<!--Personnalisation pour terminaux apple-->
<link href="img/startup.png" rel="apple-touch-startup-image"/>
<link href="img/touch-icon-iphone.png" rel="apple-touch-icon"/>
<link href="" rel="apple-touch-icon" sizes="76x76"/>
<link href="" rel="apple-touch-icon" sizes="120x120"/>
<link href="" rel="apple-touch-icon" sizes="152x152"/>
<!--Personnalisation pour terminaux apple--></head>
<body id="ut-homepage">
<!--MODAL--->
<div aria-hidden="true" aria-labelledby="myModalLabel" class="modal fade" id="myModal" role="dialog" tabindex="-1">
<div class="modal-dialog modal-lg">
<div class="modal-content"></div>
</div>
</div>
<!--MODAL--->
<!--Entete Grand ecran-->
<header class="hidden-xs hidden-sm NavFix">
<div class="container ZoneNavi">
<!--Comment candidater ?-->
<div class="fr dropdown">
<a aria-expanded="true" data-toggle="dropdown" href="#" id="HowToCandidate" type="button">Comment candidater ?</a>
<!--Articles comment candidater ?-->
<ul aria-expanded="true" aria-labelledby="dropdownMenu1" class="dropdown-menu" id="ArtHowTo" role="menu" toggle="dropdown">
<li role="presentation">
<a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=7&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" role="menuitem" tabindex="-1" title="Le calendrier">Le calendrier</a></li>
<li role="presentation">
<a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=8&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" role="menuitem" tabindex="-1" title="Dépôt de candidature">Dépôt de candidature</a></li>
<li role="presentation">
<a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=9&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" role="menuitem" tabindex="-1" title="Frais de candidatures">Frais de candidatures</a></li>
<li role="presentation">
<a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=10&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" role="menuitem" tabindex="-1" title="L’entretien">L’entretien</a></li>
<li role="presentation">
<a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=11&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" role="menuitem" tabindex="-1" title="Résultats - inscription">Résultats - inscription</a></li>
<li><a href="IMG/pdf/grille-diplomes-recevables-2020.pdf">Grille 2020 des diplômes recevables</a></li>
</ul>
<!--Articles comment candidater ?-->
</div>
<!--Comment candidater ?-->
<nav class="clearfix">
<a class="RootLink" href="https://www.3ut-admissions.fr" title="retour a l'accueil du site 3-ut Admissions">Accueil</a>
<a class="RootLink" href="presentation-des-ut/" id="Art4" title="Présentation">Présentation</a>
<a class="RootLink" href="informations-pratiques/" id="Art5" title="Informations pratiques">Informations pratiques</a>
<a class="RootLink" href="l-apprentissage/" id="Art3" title="L’apprentissage">L’apprentissage</a>
<a class="RootLink" href="domaines-d-excellences/" id="Art6" title="Domaines d’excellences ">Domaines d’excellences </a>
<a class="RootLink" href="f-a-q/" id="Art7" title="F.A.Q.">F.A.Q.</a>
</nav>
</div>
</header>
<!--Entete Grand ecran-->
<!--Entete Tablette-->
<header class="visible-sm" id="MenuTablette">
<!--Comment candidater ?-->
<div class="dropdown">
<a aria-expanded="true" data-toggle="dropdown" href="#" id="TabHowToCandidate" type="button">
<span>Comment candidater ?</span>
</a>
<!--Articles comment candidater ?-->
<ul aria-expanded="true" aria-labelledby="dropdownMenu1" class="dropdown-menu" id="TabArtHowTo" role="menu" toggle="dropdown">
<li role="presentation"> <a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=7&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" role="menuitem" tabindex="-1" title="Le calendrier">Le calendrier</a></li>
<li role="presentation"> <a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=8&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" role="menuitem" tabindex="-1" title="Dépôt de candidature">Dépôt de candidature</a></li>
<li role="presentation"> <a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=9&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" role="menuitem" tabindex="-1" title="Frais de candidatures">Frais de candidatures</a></li>
<li role="presentation"> <a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=10&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" role="menuitem" tabindex="-1" title="L’entretien">L’entretien</a></li>
<li role="presentation"> <a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=11&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" role="menuitem" tabindex="-1" title="Résultats - inscription">Résultats - inscription</a></li>
<li><a href="IMG/pdf/grille-diplomes-recevables-2020.pdf">Grille 2020 des diplômes recevables</a></li>
</ul>
<!--Articles comment candidater ?-->
</div>
<!--Comment candidater ?-->
<nav>
<a class="TabLink" href="https://www.3ut-admissions.fr" title="retour a l'accueil du site 3-ut Admissions">Accueil</a>
<a class="TabLink" href="presentation-des-ut/">Présentation</a>
<a class="TabLink" href="informations-pratiques/">Informations pratiques</a>
<a class="TabLink" href="l-apprentissage/">L’apprentissage</a>
<a class="TabLink" href="domaines-d-excellences/">Domaines d’excellences </a>
<a class="TabLink" href="f-a-q/">F.A.Q.</a>
</nav>
</header>
<_sm-root>
<!--Entete Tablette-->
<!--Entete Smartphone-->
<header class="visible-xs" id="MenuSmartphone">
<!--Comment candidater ?-->
<div class="SmartDrop dropdown">
<a aria-expanded="true" data-toggle="dropdown" href="#" id="SmartHowToCandidate" type="button">Comment candidater ?</a>
<!--Articles comment candidater ?-->
<ul aria-expanded="true" aria-labelledby="dropdownMenu1" class="dropdown-menu" id="ArtHowTo" role="menu" toggle="dropdown">
<li role="presentation"> <a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=7&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" role="menuitem" tabindex="-1" title="Le calendrier">Le calendrier</a></li>
<li role="presentation"> <a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=8&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" role="menuitem" tabindex="-1" title="Dépôt de candidature">Dépôt de candidature</a></li>
<li role="presentation"> <a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=9&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" role="menuitem" tabindex="-1" title="Frais de candidatures">Frais de candidatures</a></li>
<li role="presentation"> <a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=10&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" role="menuitem" tabindex="-1" title="L’entretien">L’entretien</a></li>
<li role="presentation"> <a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=11&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" role="menuitem" tabindex="-1" title="Résultats - inscription">Résultats - inscription</a></li>
<li><a href="IMG/pdf/grille-diplomes-recevables-2020.pdf">Grille 2020 des diplômes recevables</a></li>
</ul>
<!--Articles comment candidater ?-->
</div>
<!--Comment candidater ?-->
<a aria-controls="SmartMenu" aria-expanded="false" class="SmartLink" data-target="#SmartMenu" data-toggle="collapse" href="#" id="MenuPanel">Menu</a>
<nav aria-expanded="false" class="panel-body collapse" id="SmartMenu">
<a class="SmartLink" href="https://www.3ut-admissions.fr" title="retour a l'accueil du site 3-ut Admissions">Accueil</a>
<a class="SmartLink" href="presentation-des-ut/">Présentation</a>
<a class="SmartLink" href="informations-pratiques/">Informations pratiques</a>
<a class="SmartLink" href="l-apprentissage/">L’apprentissage</a>
<a class="SmartLink" href="domaines-d-excellences/">Domaines d’excellences </a>
<a class="SmartLink" href="f-a-q/">F.A.Q.</a>
</nav>
</header>
<!--Entete Smartphone--><section class="container" id="content">
<a href="https://www.3ut-admissions.fr/candidater" id="JeCandidate" title="Je candidate">Je candidate</a>
<section id="LogoZone"><img alt="3-ut Admissions" src="local/cache-vignettes/L184xH80/siteon0-62ad9.png?1536931227"/></section>
</section>
<section>
<!--Selecteurs d'onglets-->
<section class="container" id="UT-Tabs">
<ul class="row" id="TabUTs" role="tablist">
<!--Boucle Active-->
<li class="col-sm-4 active" id="Onglet-1" role="presentation">
<a aria-controls="background-1" data-toggle="tab" href="#background-1" role="tab" title="Découvrir l'UTC">UTC</a>
</li>
<!--Boucle Active-->
<li class="col-sm-4" id="Onglet-2" role="presentation">
<a aria-controls="background-2" data-toggle="tab" href="#background-2" role="tab" title="Découvrir l'UTBM">UTBM</a>
</li>
<li class="col-sm-4" id="Onglet-8" role="presentation">
<a aria-controls="background-8" data-toggle="tab" href="#background-8" role="tab" title="Découvrir l'UTT">UTT</a>
</li>
</ul>
</section>
<!--Selecteurs d'onglets-->
<div class="tab-content">
<!--Boucle Active-->
<section class="tab-pane fade in active" id="background-1" role="tabpanel" style="background: url(IMG/moton1.jpg?1417180421) no-repeat center; background-size:cover;">
<article class="PanelUT container" id="Panel-1">
<h2 class="ut-titre">Université de Technologie de Compiègne</h2>
<section class="ut-desc col-sm-offset-6"><p>Construite sur une pédagogie de l’autonomie et une recherche technologique interdisciplinaire orientée vers l’innovation, l’UTC forme des ingénieurs, masters et docteurs aptes à appréhender les interactions de la technologie avec l’homme et la société, et à évoluer dans un environnement concurrentiel mondial, dans un souci de développement durable.</p></section>
<!--Liens vers Page UT -->
<a class="Voir-UT Voir-UT-1 col-sm-offset-8" href="universite-de-technologie-de-compiegne" title="UTC">En savoir plus</a>
<!--Liens vers Page UT -->
</article>
</section>
<!--Boucle Active-->
<!--Panneaux d'onglets-->
<section class="tab-pane fade" id="background-2" role="tabpanel" style="background: url(IMG/moton2.jpg?1417180464) no-repeat center; background-size:cover;">
<article class="PanelUT container" id="Panel-2">
<h2 class="ut-titre">Université de Technologie de Belfort-Montbéliard</h2>
<section class="ut-desc col-sm-offset-6"><p>L’UTBM forme des ingénieurs rapidement opérationnels, particulièrement adaptables aux évolutions de la technologie et aux mutations de la société. Ses formations s’appuient sur les activités de recherche et sur la valorisation.</p>
<p>Créée en 1999, l’UTBM est un établissement public à caractère scientifique, culturel et professionnel. Membre du réseau des universités de technologie, elle est née du regroupement de deux établissements d’enseignement supérieur : l’Ecole Nationale d’Ingénieurs de Belfort (1962) et l’Institut Polytechnique de Sevenans (antenne de l’UTC implantée à Sevenans en 1985).</p></section>
<!--Liens vers Page UT 2-->
<a class="Voir-UT Voir-UT-2 col-sm-offset-8" href="universite-de-technologie-de-belfort-montbeliard" title="UTBM">En savoir plus</a>
<!--Liens vers Page UT 2-->
</article>
</section>
<!--Panneaux d'onglets-->
<!--Panneaux d'onglets-->
<section class="tab-pane fade" id="background-8" role="tabpanel" style="background: url(IMG/moton8.jpg?1417180490) no-repeat center; background-size:cover;">
<article class="PanelUT container" id="Panel-8">
<h2 class="ut-titre">Université de Technologie de Troyes</h2>
<section class="ut-desc col-sm-offset-6"><p>La recherche, la formation et le transfert de technologie sont les trois missions de l’UTT. Etablissement public créé à Troyes en 1994, l’UTT est aujourd’hui parmi les 10 écoles d’ingénieurs les plus importantes en France.</p>
<p>L’UTT entretient des liens forts avec le monde économique, notamment à travers la recherche partenariale avec les entreprises et les stages longs de ses étudiants.</p></section>
<!--Liens vers Page UT 2-->
<a class="Voir-UT Voir-UT-8 col-sm-offset-8" href="universite-de-technologie-de-troyes" title="UTT">En savoir plus</a>
<!--Liens vers Page UT 2-->
</article>
</section>
<!--Panneaux d'onglets-->
</div></section>
<section class="container" id="DescSite">
<p>L’UTBM à Belfort-Montbéliard, l’UTC à Compiègne et l’UTT à Troyes sont regroupées dans le réseau des « universités de technologie » : elles réunissent la renommée des grandes écoles d’ingénieurs et les atouts des universités.</p>
<p>Établissements publics, ils délivrent des diplômes d’ingénieur habilités par le ministère de l’éducation nationale, validés par la Commission des Titres d’Ingénieurs (CTI) et reconnus par le monde professionnel.  Ils confèrent le grade de master.</p>
<section class="NewsZone clearfix">
</section>
<!--Bloc Agenda Salon-->
<dl class="BlocHome col-sm-12 col-md-4 Bloc-8" id="BlocSalon">
<dt><a class="LiensRub" href="spip.php?page=event">Où nous rencontrer</a></dt>
<dd class="h-event-off">
<p>Retrouvez nos dates de salons très prochainement</p>
</dd>
</dl>
<!--Bloc Agenda Salon-->
<!--Bloc Apprentissage-->
<dl class="BlocHome col-sm-6 col-md-4 Bloc-3" id="Blocapp">
<dt><a class="LiensRub" href="l-apprentissage/">L’apprentissage</a></dt>
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=1" data-target="#myModal" data-toggle="modal" href="#" title="L’apprentissage à UTBM">L’apprentissage à UTBM</a></dd>
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=2" data-target="#myModal" data-toggle="modal" href="#" title="L’apprentissage à l’UTT">L’apprentissage à l’UTT</a></dd>
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=3" data-target="#myModal" data-toggle="modal" href="#" title="l’apprentissage à l’UTC">l’apprentissage à l’UTC</a></dd>
<!--ARTICLES BLOCS-->
<!--ARTICLES BLOCS-->
</dl>
<!--Bloc Apprentissage-->
<!--Bloc Domaines d'excellences-->
<dl class="BlocHome col-sm-6 col-md-4" id="BlocExcellence">
<dt><a class="LiensRub" href="domaines-d-excellences/">Domaines d’excellences </a></dt>
<!--Article Domaines excellences-->
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=24&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" title="Sport élite et musique">Sport élite et musique</a></dd>
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=25&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" title="Sport études et musique">Sport études et musique</a></dd>
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=26&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" title="Sport, musique et théâtre">Sport, musique et théâtre</a></dd>
<!--Article Domaines excellences-->
</dl>
<!--Bloc Domaines d'excellences-->
<!--Infos Pratiques-->
<dl class="BlocHome col-sm-6" id="BlocInfos">
<dt><a class="LiensRub" href="informations-pratiques/">Informations pratiques</a></dt>
<!--Article Infos Pratiques-->
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=7&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" title="Le calendrier">Le calendrier</a></dd>
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=8&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" title="Dépôt de candidature">Dépôt de candidature</a></dd>
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=9&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" title="Frais de candidatures">Frais de candidatures</a></dd>
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=10&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" title="L’entretien">L’entretien</a></dd>
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=11&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" title="Résultats - inscription">Résultats - inscription</a></dd>
<!--Article Infos Pratiques-->
</dl>
<!--Infos Pratiques-->
<!--Infos FAQ-->
<dl class="BlocHome col-sm-6" id="BlocFAQ">
<dt><a class="LiensRub" href="f-a-q/">F.A.Q.</a></dt>
<!--Article FAQ-->
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=16&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" title="Devons-nous envoyer un dossier papier pour les branches ?">Devons-nous envoyer un dossier papier pour les branches ?</a></dd>
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=17&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" title="A quel moment commence les 15 jours ?">A quel moment commence les 15 jours ?</a></dd>
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=18&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" title="Comment savons nous que nos documents ont été validés ?">Comment savons nous que nos documents ont été validés ?</a></dd>
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=19&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" title="Si je ne règle pas les frais de candidature, mon dossier est-il traité ?">Si je ne règle pas les frais de candidature, mon dossier est-il traité ?</a></dd>
<dd><a data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=20&amp;var_mode=recalcul" data-target="#myModal" data-toggle="modal" href="#" title="Dois-je payer les frais de candidature pour chaque UTs ?">Dois-je payer les frais de candidature pour chaque UTs ?</a></dd>
<!--Article FAQ-->
</dl>
<!--Infos FAQ-->
</section><footer>
<h2 class="container-fluid" id="BienvenueUt">
    	Bienvenue dans le réseau des Universités de Technologies
    </h2>
<div id="InfosPlus">
<section class="container-fluid">
<section class="FooterTxt col-md-6">
<section><p>L’UTBM à Belfort-Montbéliard, l’UTC à Compiègne et l’UTT à Troyes sont regroupées dans le réseau des « universités de technologie » : elles réunissent la renommée des grandes écoles d’ingénieurs et les atouts des universités.</p>
<p>Établissements publics, ils délivrent des diplômes d’ingénieur habilités par le ministère de l’éducation nationale, validés par la Commission des Titres d’Ingénieurs (CTI) et reconnus par le monde professionnel.  Ils confèrent le grade de master.</p></section>
</section>
<section class="col-sm-6 col-md-4">
<ul class="SitesReseaux">
<li><a class="FooterLinks" href="http://www.utc.fr">Université de Technologie de Compiègne</a></li>
<li><a class="FooterLinks" href="http://www.utt.fr">Université de technologie de Troyes</a></li>
<li><a class="FooterLinks" href="http://www.utbm.fr">Université de technologie Belfort-Montbéliard</a></li>
</ul>
</section>
<section class="col-sm-6 col-md-2">
<ul class="ZoneComplement">
<li><a class="credits" data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=32" data-target="#myModal" data-toggle="modal" role="menuitem" tabindex="-1" title="Crédits">Crédits</a></li>
<li><a class="legales" data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=33" data-target="#myModal" data-toggle="modal" role="menuitem" tabindex="-1" title="Mentions légales">Mentions légales</a></li>
<li><a class="contacts" data-backdrop="false" data-remote="https://www.3ut-admissions.fr/spip.php?page=modal&amp;id_article=34" data-target="#myModal" data-toggle="modal" role="menuitem" tabindex="-1" title="Nous Contacter">Nous Contacter</a></li>
</ul>
</section>
</section>
</div>
</footer>
<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//piwik.utc.fr/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 34]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img alt="" src="//piwik.utc.fr/piwik.php?idsite=34" style="border:0;"/></p></noscript>
<!-- End Piwik Code -->
</_sm-root></body>
</html>
