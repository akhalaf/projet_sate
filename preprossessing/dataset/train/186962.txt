<html>
<head>
<title>BalticMaps</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans&amp;display=swap" rel="stylesheet"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/png"/>
<script src="/bj.js" type="text/javascript"></script>
<meta content="width=device-width, initial-scale=0.9, user-scalable=no" name="viewport"/>
<meta content="Karšu pārlūks BalticMaps internetā un mobilajās ierīcēs. Adrešu un vietu meklēšana, maršrutu veidošana, ceļu karte, sastrēgumu informācija, interešu punktu interaktīvais slānos" name="description"/>
<meta content="Latvijas karte, Rīgas karte, Lietuvas, Igaunijas, Baltijas un Eiropas kartes, adrešu un vietu meklēšana, ceļu karte, sastrēgumi, sastrēgumu informācija, sastrēgumu kartes, sastrēgumi Rīgā, interešu punkti, sastrēgumu karte, sastregumi, Jāņa sētas kartes, jāņa sēta, jana seta, adrešu un vietu meklēšana, ceļu remontdarbu kartes, Lietuvas karte, Igaunijas karte, Tallinas karte, Viļņas karte, maršrutēšana, GPS sekošanas sistēmas, karšu pārlūks" name="keywords"/>
<meta content="telephone=no" name="format-detection"/> <!-- so that Edge doesn't style text as phone numbers -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<link href="/mapbox-gl.css" rel="stylesheet"/>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-143421113-1"></script>
<link href="//kartes.lv" rel="dns-prefetch"/>
<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('config', 'UA-143421113-1');
	</script>
</head>
<body>
<style>
		@keyframes slideInFromLeft {
			0% {
				width: 0px;
			}

			100% {
				width: 130px;
			}
		}

		body {
			font-family: 'Open Sans', sans-serif;
			width: 100%;
			overflow: hidden;
			margin: 0;
		}

		.ol-scale-line {
			background-color: #fff;
			border-radius: 0px;
			box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12);
			left: 62px
		}

		.ol-scale-line-inner {
			color: #00264A;
			border: 1px solid #00264A;
			border-top: none;
			font-weight: 600;
		}

		.ReactTable .rt-thead.-header {
			box-shadow: none;
			border-bottom: 1px solid #676767;
		}

		.ReactTable .rt-tbody .rt-tr-group {
			border-bottom: none;
		}

		.ReactTable .rt-tbody .rt-td {
			border-right: none;
		}

		.ReactTable .rt-thead .rt-th,
		.ReactTable .rt-thead .rt-td {
			border-right: none;
		}

		.ReactTable .rt-table {
			padding: 14px;
			font-size: 0.8rem;
		}

		.ReactTable .rt-thead .rt-resizable-header-content {
			text-align: left;
		}

		.ReactTable .-pagination .-btn {
			border-radius: 0;
		}

		.ReactTable .pagination-bottom {
			font-size: 0.8rem;
		}

		.tooltip {
			position: relative;
			background: rgba(0, 0, 0, 0.5);
			border-radius: 4px;
			color: white;
			padding: 6px 10px;
			opacity: 1;
			white-space: nowrap;
		}

		.tooltip-measure {
			opacity: 0.7;
			padding: 4px 8px;
		}

		.tooltip-measure-total {
			font-weight: bold;
			background: rgba(70, 70, 70, 1);
		}

		.tooltip-static {
			background-color: #ffcc33;
			color: black;
			border: 1px solid white;
		}

		.tooltip-measure:before,
		.tooltip-static:before {
			border-top: 6px solid rgba(0, 0, 0, 0.5);
			border-right: 6px solid transparent;
			border-left: 6px solid transparent;
			content: "";
			position: absolute;
			bottom: -6px;
			margin-left: -7px;
			left: 50%;
		}

		.tooltip-static:before {
			border-top-color: #ffcc33;
		}

		#wrong-browser {
			width: 100%;
			text-align: center;
			margin-top: 200px;
		}
		.ol-overlay-container, 
		.tooltip {
			pointer-events: none;
		}
	</style>
<script>
		window.kijsKey = 'BMQEJd2PO';
		window.activeLang = 'lv';
		window.translations = {
			"BM_MOBILE_NOTIFICATION":"BalticMaps.eu ir ērtāk lietot mobilajai ierīcei piemērotā lietotnē!","SEARCH_NOT_SUCCESSFULL":"Notika kļūda!","ERROR_IN_LOADING":"Notika kļūda!","RADARS_AND_TRAFFIC":"Radari un sastrēgumi","DRAW_TOGGLE":"Zīmēšana un attālumu mērīšana","ABOUT_APP":"Par BalticMaps","SOCIAL_MEDIA":"Sociālie tīkli","ROUTING_TOGGLE":"Maršruta izveide","MORE_ACTIONS":"Citas darbības","DECIMAL_SEPARATOR":",","ZOOM_IN":"Pietuvināt","ZOOM_OUT":"Attālināt","GPS_BUTTON":"Mana atrašanās vieta","DRAW_POINT":"Zīmēt punktu","DRAW_LINE":"Zīmēt līniju un mērīt attālumu","DRAW_SHAPE":"Zīmēt figūru un mērīt laukumu","CLEAR_DRAWINGS":"Dzēst visas figūras","RIX_VIEW":"Rīgas skats","LVA_VIEW":"Latvijas skats","MAP_TOGGLE_BUTTON":"Kartes slāņu izvēle","FILTER_TRAFFICJAMS":"Meklēt","MORE_ENTRIES_AVAILABLE_SIN":"vēl pieejams <> rezultāts","MORE_ENTRIES_AVAILABLE_ENDS_WITH_1":"vēl pieejams <> rezultāts","MORE_ENTRIES_AVAILABLE_ENDS_WITH_234":"vēl pieejami <> rezultāti","MORE_ENTRIES_AVAILABLE_PLU":"vēl pieejami <> rezultāti","MANAKARTE":"manakarte.kartes.lv","GO_TO_OLD_BM":"Vecā versija","RADARS":"Fotoradari","TRAFFIC_JAM_BTT":"Sastrēgumi","TRAFFIC":"Sastrēgumi","TRAFFIC_TABLE_PREVIOUS":"Iepriekšējie","TRAFFIC_TABLE_NEXT":"Nākamie","TRAFFIC_TABLE_NAME":"Posms","TRAFFIC_TABLE_CURRENT":"Šobrīd","TRAFFIC_TABLE_USUALLY":"Parasti","TRAFFIC_TABLE_TIME":"Laiks","TRAFFIC_DRIVE_TIME":"Caurbraukšanas laiks","TRAFFIC_NORMAL_DRIVE_TIME":"Normālais caurbraukšanas laiks","TRAFFIC_DATE":"Datums","TRAFFIC_LEVEL":"Ticamība","ROUTING_FROM":"Norādes no šejienes","ROUTING_TO":"Norādes uz šejieni","SEARCH_POP_PLACE":"Apdzīvota vieta","SEARCH_ADDRESS":"Adrese","SEARCH_ADMIN_UNIT":"Administratīva vienība","SEARCH_TERIT_UNIT":"Teritoriāla vienība","SEARCH_STREET":"Iela, ceļš","SEARCH_RIVER":"Upe","SEARCH_LAKE":"Ezers","SEARCH_MOUNTAIN":"Kalns","SEARCH_HOTEL":"Naktsmītne","SEARCH_PUBLIC_TRANSPORT":"Transports","SEARCH_DUS":"Degvielas uzpildes stacija","SEARCH_EDUCATION":"Izglītības iestāde","SEARCH_AIRPORTS":"Lidosta","SEARCH_RELIGION":"Dievnams","SEARCH_MUSEUM":"Muzejs","SEARCH_CULTURE":"Kultūras objekts","SEARCH_SHOPPING":"Iepirkšanās","SEARCH_SKIING":"Slēpošanas trase","SEARCH_NATURE":"Dabas objekts","SEARCH_HEALTH":"Veselības iestāde","SEARCH_POST":"Pasta nodaļa","SEARCH_PUBLIC":"Sabiedriska iestāde","SEARCH_FINANCE":"Finanšu iestāde","SEARCH_CATERING":"Ēdināšanas iestāde","SEARCH_SPORTS":"Sporta iestāde","SEARCH_TOURISM":"Tūrisms","SEARCH_SWAMP":"Purvs","SEARCH_PARK_FOREST":"Parks un mežs","SEARCH_ISLAND":"Sala","MIN_SIN":"min","MIN_PLUR":"min","HOUR_SIN":"h","HOUR_PLUR":"h","ROUTE":"Maršruts:","ROUTE_NR":"maršruts:","SHOW_MORE_RESULTS":"Visi rezultāti","SPEED_CAMERAS_TITLE":"Fotoradari","CAMERAS_LEGEND":"Apzīmējumi:","STATIC_CAMERAS":"Stacionārie radari","MOVE_CAMERAS":"Pārvietojamie radari","SEARCH_ADDRESS_OR_COORDS":"Meklēt vietu vai koordinātas","ROUTING_POINT":"<>. punkts","MAP_MISTAKES":"Ziņot par labojumiem","NAME":"Vārds","EMAIL":"E-pasts","MAP_MISTAKE_DESCRIPTION":"Apraksts","MAP_MISTAKES_SUBMIT_SUCCESS":"Paldies! Jūsu ziņojums tika nosūtīts!","MAP_MISTAKES_SUBMIT_FAILED":"Notika kļūda! Ziņojums netika nosūtīts!","NEW_MESSAGE":"Jauns ziņojums","NEW_MESSAGE_SHARE":"Dalīties vēlreiz","SUBMIT":"Iesniegt","MAP_LINK":"Kartes adrese","SHARE_MAP":"Dalīties ar karti","SHARE_TO_EMAIL":"Nosūtīt uz e-pastu","SHARE_COPY_LINK":"Nokopēt saiti","SHARE_MAP_NAME":"Sūtītāja vārds","SHARE_MAP_EMAIL":"Saņēmēja e-pasts","SHARE_MAP_TITLE":"Tēma","SHARE_MAP_DESCRIPTION":"Komentārs","MAP_SHARE_SUBMIT_SUCCESS":"Karte tika nosūtīta!","MAP_SHARE_SUBMIT_FAILED":"Notika kļūda! Karte netika nosūtīta!","SHARE_SEND_MAP":"Sūtīt","SHARE_COPY_MAX_TEXT":"Nokopējiet kartes adresi, lai dalītos ar karti","ROUTING_NEW_ROUTE":"DZĒST MARŠRUTU","ROUTING_ADDRESS_INPUT":"Meklēt adresi","ROUTING":"Maršruta izveide","ROUTING_FOOT_TOOLTIP":"Gājēju maršrutu izveide ir izstrādes stadijā.","FORM_MANDATORY_FIELD":"Lūdzu aizpildiet šo lauku!","CAMERAS_TITLE":"Fotoradari","SEARCH_NO_RESULTS":"Nekas netika atrasts","DRAW_GEOMETRY_LIMIT_REACHED":"Sasniegts maksimālais ģeometriju skaits","MAP_TOGGLE_WMSBWLAYER":"Melnbaltā karte","MAP_TOGGLE_PHOTOLAYER":"Ortofoto karte","MAP_TOGGLE_LABELLAYER":"ar nosaukumiem","MAP_TOGGLE_TOPOLAYER":"Topogrāfiskā karte","MAP_TOGGLE_WMSLAYER":"Tradicionālā karte","MAP_TOGGLE_LIDARLAYER":"ar reljefu","MAP_TOGGLE_VECTORLAYER":"Vektoru karte","MAP_TOGGLE_POILAYER":"Interešu punkti","MAP_TOGGLE_TOPO75LAYER":"Topogrāfiskā (1921-1940)","MAP_TOGGLE_ADMDIVISIONLAYER":"Pašvaldības (01.07.2021)","MAP_TOGGLE_POI_RECREATION":"atpūta","MAP_TOGGLE_POI_TRANSPORTATION":"transports","MAP_TOGGLE_POI_CULTURE":"kultūra","MAP_TOGGLE_POI_SERVICES":"pakalpojumi","MAP_TOGGLE_POI_INSTITUTIONS":"iestādes","INVALID_EMAIL":"Ievadīts nepareizs e-pasts","ROUTING_MAX_POINTS":"Sasniegts maksimāli pieļautais punktu skaits","GPS_ERROR":"Atrašanās vietas noteikšanas kļūda","GPS_ERROR_USER_DENIED":"Jūs esat aizliedzis piekļūt atrašanās vietas informācijai","TRAFFIC_TRUST_LEVEL_0":"Nav datu","TRAFFIC_TRUST_LEVEL_1":"Zema","TRAFFIC_TRUST_LEVEL_2":"Vidēja","TRAFFIC_TRUST_LEVEL_3":"Virs vidējā","TRAFFIC_TRUST_LEVEL_4":"Laba","TRAFFIC_TRUST_LEVEL_5":"Ļoti laba","TRAFFIC_TRUST_LEVEL_6":"Teicama","TRAFFIC_TRUST_LEVEL_7":"Teicama","SEARCH_HELP":"Palīdzība","PAGE_OF":"lapa no","ABOUT_APP_TITLE":"Par BalticMaps","COOKIE_POLICY_TITLE":"Sīkdatņu politika","PRIVACY_POLICY_TITLE":"Privātuma atruna","DELETE_FEATURE":"Dzēst ģeometriju","ABOUT_APP_TEXT":"Karšu pārlūks BalticMaps ir Baltijā lielākā kartogrāfijas un ģeotelpisko datu pakalpojumu sniedzēja „Karšu izdevniecība Jāņa sēta” vietne, kas satur ar kartes izmantošanu saistītus bezmaksas pakalpojumus. Karšu pārlūka lietotāji var veikt adrešu vai vietu meklēšanu, plānot maršrutu, iegūt informāciju par satiksmi un sastrēgumiem. Karšu pārlūks nodrošina iespēju kartē mērīt attālumus, atzīmēt punktus vai nosūtīt uz e-pastu izvēlētu kartes saiti.<br />Vietnes pakalpojumi paredzēti personiskai un nekomerciālai izmantošanai, ja vien nav norādīts citādi. <b>Aizliegts modificēt, kopēt, izplatīt, pārraidīt, demonstrēt, izpildīt, reproducēt, publicēt, licencēt, radīt atvasinātu darbu vai pārdot jebkādu informāciju, programmatūru, produktus vai pakalpojumus, kas iegūti no šīs vietnes.</b> Tikai personiskos un nekomerciālos nolūkos var izmantot vietnes funkcionalitāti, kas veic kartes nosūtīšanu uz e-pastu. Ja lietotājam rodas šaubas par pakalpojumu izmantošanas atbilstību šiem Noteikumiem, lietotājam jāsazinās ar uzņēmumu “Karšu izdevniecība Jāņa sēta”. Ja vēlaties izmantot tos vietnes pakalpojumus, kurus šie noteikumi ierobežo, aicinām rakstīt mums uz e-pastu: <a href='mailto:kartes@kartes.lv'>kartes@kartes.lv</a>.<br />Visas intelektuālā īpašuma tiesības attiecībā uz vietni pieder tikai un vienīgi uzņēmumam “Karšu izdevniecība Jāņa sēta”. Šo tiesību pārkāpuma gadījumā vainīgā persona tiek saukta pie normatīvajos aktos noteiktās atbildības, kā arī ir pilnībā atbildīga par visiem zaudējumiem, kas ir vai varētu tikt nodarīti Vietnei un trešajām personām.<br />© Karšu izdevniecība Jāņa sēta<br />Produkta sagatavošanā izmantoti Valsts adrešu reģistra dati (© Valsts zemes dienests, 1999-2019), Latvijas Ģeotelpiskās informācijas aģentūras dati (2016-2018), VAS Latvijas Valsts meži dati (2018-2019), OpenStreetMap dati (© OpenStreetMap contributors, <a href='https://www.openstreetmap.org/copyright'>https://www.openstreetmap.org/copyright</a>), ForestRadar pamatkartes serviss, iekļaujot visu Latviju un Igauniju (2019; <a href='https://www.forestradar.com'>https://www.forestradar.com</a>), Igaunijas Zemes dienesta dati (© Maa-amet)","COOKIE_POLICY_TEXT":"<b>Šī tīmekļa vietne izmanto sīkdatnes</b><br />Mēs izmantojam sīkdatnes, lai personalizētu saturu un reklāmas, nodrošinātu sociālo saziņas līdzekļu funkcijas un analizētu mūsu datplūsmu. Informāciju par to, kā jūs izmantojat mūsu vietni, mēs arī varam kopīgot ar saviem sociālās saziņas līdzekļu, reklamēšanas un analīzes partneriem, kuri to var apvienot ar citu informāciju, ko viņiem sniedzat vai ko viņi apkopo, kad lietojat viņu pakalpojumus. Turpinot lietot mūsu tīmekļa vietni, Jūs apstiprināt mūsu sīkdatnes.","PRIVACY_POLICY_TEXT":"<b>Pārziņa identitāte un kontaktinformācija</b>: SIA “Karšu izdevniecība Jāņa sēta” (turpmāk tekstā – Jāņa sēta), reģ.Nr. 40003426448; Krasta ielā 105a, Rīgā, LV-1019, tālrunis: 67317540, elektroniskā pasta adrese: <a href='mailto:kartes@kartes.lv' target='_blank'>kartes@kartes.lv</a>.<br /><b>Apstrādes pamats</b>: Jāņa sēta nevāc nekādus Jūsu personas datus šajā lapā, neskaitot to informāciju, kuru Jūs brīvprātīgi nododat (piemēram, nosūtot mums e-pastu par kartes labojumiem). Jebkura informācija, kuru Jūs sniegsiet šādā veidā, nav pieejama trešajām personām, un Jāņa sēta to izmanto tikai tiem mērķiem, kādiem Jūs šo informāciju sniedzāt.<br /><b>Apstrādes nolūki, kam paredzēti personas dati</b>: Vārds, e-pasts – lai sazinātos ar labojumu iesniedzēju gadījumos, kad Jāņa sētai nepieciešams precizēt Jūsu iesūtīto informāciju.<br /><b>Personas datu apstrāde, aizsardzība un glabāšanas termiņi, personas datu saņēmēji vai saņēmēju kategorijas, tiesības iesniegt sūdzību uzraudzības iestādei un citi datu aizsardzības jautājumi ir norādīti</b>: <a href='https://www.kartes.lv/lv/privatuma-politika/' target='_blank'>Jāņa sētas Privātuma politikā</a>.","COORDINATES_HELP":"<p>Meklētājs meklē šādos informācijas slāņos:</p><ul class='search_help_items'><li>Adreses</li><li>Apdzīvotas vietas</li><li>Administratīvās un teritoriālās vienības</li><li>Upes</li><li>Ezeri</li><li>Kalni, pilskalni</li><li>Ielas, ceļi</li><li>Naktsmītnes</li><li>Koordinātas šādā formātā:<br />Ģeogrāfiskās koordinātas<br /><span class='helpbox_coordinates_item'>57° 23′ 57″ N, 25° 29′ 39″ E</span><br /><span class='helpbox_coordinates_item'>57°23′57″N, 25°29′39″E</span><br /><span class='helpbox_coordinates_item'>57° 23′ 57″ Z, 25° 29′ 39″ A</span><br /><span class='helpbox_coordinates_item'>57,399167;25,494167</span><br /><span class='helpbox_coordinates_item'>57,399167 25,494167</span><br /><span class='helpbox_coordinates_item'>56.95752 24.14990</span><br />Baltijas koordinātu sistēma<br /><span class='helpbox_coordinates_item'>395225 6305210</span><br />LKS-92<br /><span class='helpbox_coordinates_item'>395225 305210</span></li></ul>","COOKIES_TEXT":"Šī vietne izmanto sīkdatnes, lai uzlabotu lietošanas pieredzi un optimizētu tās darbību. Turpinot lietot šo vietni, Jūs piekrītat sīkdatņu lietošanai balticmaps.eu","RIGHTS_COOKIES":"Jūs nedrīkstat modificēt, kopēt, izplatīt, pārraidīt, demonstrēt, izpildīt, reproducēt, publicēt, licencēt, radīt atvasinātu darbu vai pārdot jebkādu informāciju, programmatūru, produktus vai pakalpojumus, kas iegūti no vietnes. Jautājumus par vietnes izmantošanu lūdzam sūtīt uz <a href='mailto:kartes@kartes.lv' class='cookiesLink'>kartes@kartes.lv</a>","SPEEDCAMERAS_DOC_TITLE":"Fotoradaru karte","SPEEDCAMERAS_DOC_DESC":"Fotoradaru atrašanās vietas BalticMaps kartē. Stacionārie un pārvietojamie fotoradari. Baltijas valstu karšu apskatīšana tiešsaistē","SPEEDCAMERAS_DOC_KEYWORDS":"Fotoradari, stacionārie fotoradari, pārvietojamie fotoradari, fotoradars, foto radari, fotoradari kartē","ROUTING_DOC_TITLE":"Maršruta izveide","ROUTING_DOC_KEYWORDS":"Maršrutu veidošana, maršrutēšana, GPS sekošanas sistēmas, karšu pārlūks, adrešu un vietu meklēšana, adrese, maršruts, laiks, attālums, Latvijas karte, Rīgas karte, Lietuvas, Igaunijas, Baltijas un Eiropas kartes","ROUTING_DOC_DESC":"Ātra un ērta maršrutu veidošana karšu pārlūkā BalticMaps. Maršrutēšana starp vairākiem punktiem uz kartes, plānotā maršruta saglabāšana, kā arī maršruta ceļā pavadāmā laika un attāluma aprēķināšana","TRAFFIC_DOC_TITLE":"Sastrēgumu karte","TRAFFIC_DOC_KEYWORDS":"satiksme Rīgas ielās, satiksme, sastrēgumi, sastrēgumu informācija, sastrēgumu kartes, sastrēgumi Rīgā, sastrēgumu karte, sastregumi","TRAFFIC_DOC_DESC":"Sastrēgumu un satiksmes informācijas attēlošana Rīgā. Baltijas valstu karšu apskatīšana tiešsaistē BalticMaps karšu pārlūkā","BALTICMAPS_TITLE":"BalticMaps","BALTICMAPS_KEYWORDS":"Latvijas karte, Rīgas karte, Lietuvas, Igaunijas, Baltijas un Eiropas kartes, adrešu un vietu meklēšana, ceļu karte, sastrēgumi, sastrēgumu informācija, sastrēgumu kartes, sastrēgumi Rīgā, interešu punkti, sastrēgumu karte, sastregumi, Jāņa sētas kartes, jāņa sēta, jana seta, adrešu un vietu meklēšana, ceļu remontdarbu kartes, Lietuvas karte, Igaunijas karte, Tallinas karte, Viļņas karte, maršrutēšana, GPS sekošanas sistēmas, karšu pārlūks","BALTICMAPS_DESC":"Karšu pārlūks BalticMaps internetā un mobilajās ierīcēs. Adrešu un vietu meklēšana, maršrutu veidošana, ceļu karte, sastrēgumu informācija, interešu punktu interaktīvais slānos","BALTICMAPS_DOC_DESC":"Baltijas karte","TRAFFIC_JAM_TOOLTIP":"Izplatot sastrēgumu informāciju plašsaziņas līdzekļos, atsaukšanās uz vietni Balticmaps.eu ir obligāta. Citiem mērķiem šīs informācijas izmantošana ir jāsaskaņo.","TRAFFIC_TABLE_DIFF":"Kavējums","TRAFFIC_TABLE_DIFF_TIME":"Kavējuma laiks","DRAW_GEOMETRY_MAX_TEXT":"Maksimālais virsotņu skaits:","DRAW_CURRENT_POINTS":"Pašreizējais virsotņu skaits:","ROUTING_LESS_THAN_MINUTE":"Mazāk kā minūte","MY_BM_BTT":"Pasūtīt kartes izdruku","MY_BM_BTT_TOOLTIP":"MyBalticMaps karte",
			/* "TRAFFIC_JAM_BTT": "Sastrēgumi",
			 "ROUTING_FROM": "Norādes no šejienes",
			 "ROUTING_TO": "Norādes uz šejieni",
			 "SHOW_MORE_RESULTS": "Rādīt pārējos rezultātus",*/
		};

		window.displayText = function(TXTKEY) {

			if (typeof window.translations[TXTKEY] === 'undefined') {

				return TXTKEY;
			}

			return window.translations[TXTKEY];
		}
	</script>
<div id="app"></div>
<script>
		// Get IE or Edge browser version
		var version = detectIE();

		if ((version && version < 10) || typeof Object.assign == 'undefined') {
			document.getElementById('app').innerHTML = '<div id="wrong-browser"><h2>Neatbilstošs vai novecojis WEB pārlūks.</h2><p>Atbalstītie interneta pārlūki: Google Chrome, Firefox, Edge.</p></div>';
		}
	</script>
<script src="/dist/vendors~main.b1cfd8d0a70f41b74334.js" type="text/javascript"></script><script src="/dist/main.2ef08e1f887599f6d998.js" type="text/javascript"></script></body>
</html>