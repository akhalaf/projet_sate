<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>bezdeals</title>
<link href="favicon.png" rel="icon"/>
<link href="bezdeals.css" rel="stylesheet" type="text/css"/>
</head>
<body onload="handleSearch()">
<div id="logo">bezdeals</div>
<center>
<form>
<input aria-label="search box" id="searchBox" name="search" placeholder="search..." type="text"/>
</form>
</center>
<p id="results"></p>
<div id="tag">the best of the best deals, searchable</div>
<div id="copyright">copyright 2020 marcella tanzil and ward childress</div>
<div id="version">version alpha.201015</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="bezdeals.js"></script>
</body>
</html>
