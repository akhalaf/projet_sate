<!DOCTYPE html>
<html lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=1200" name="viewport"/>
<meta content="Arclab Software GbR is a developer of Email Software Solutions, Database and Website Software for Windows PC." name="description"/>
<meta content="Arclab Software" name="author"/>
<meta content="Arclab Software" name="copyright"/>
<meta content="Arclab Software" name="publisher"/>
<title>Arclab Software | Email Solutions, Database and Website Software</title>
<link href="res/arclab.css" rel="stylesheet" type="text/css"/>
<link href="https://www.arclab.com/en/" rel="canonical"/>
<link href="https://www.arclab.com/de/" hreflang="de" rel="alternate"/>
</head>
<body>
<!-- ARCLAB.com Start Header -->
<div id="header">
<div id="header-in">
<div id="header-menu">
<ul>
<li class="logo"><a href="/en/"> </a></li>
<li><a href="/en/products.html"><i class="material-icons">apps</i>  Products</a>
<ul>
<li><a href="/en/amlc/">MailList Controller</a></li>
<li><a href="/en/webformbuilder/">Web Form Builder</a></li>
<li><a href="/en/inbox2db/">Inbox2DB</a></li>
<li><a href="/en/websitelinkanalyzer/">Website Link Analyzer</a></li>
<li><a href="/en/dir2html/">Dir2HTML</a></li>
<li><a href="/en/watermarkstudio/">Watermark Studio</a></li>
</ul>
</li>
<li><a href="/en/download.html"><i class="material-icons">file_download</i></a></li>
<li><a href="/en/store.html"><i class="material-icons">shopping_cart</i></a></li>
<li><a href="/en/support.html"><i class="material-icons">help_outline</i></a></li>
<li><a href="/en/about.html"><i class="material-icons">business</i></a>
<ul>
<li><a href="/en/about.html">Imprint &amp; About</a></li>
<li><a href="/en/contact.html">Contact us</a></li>
<li><a href="/en/cookies-privacy.html">Cookie and Privacy Policy</a></li>
<li><a href="https://www.facebook.com/arclab" target="_blank">Arclab on Facebook</a></li>
<li><a href="https://twitter.com/arclabsoftware" target="_blank">Arclab on Twitter</a></li>
<li><a href="/en/kb/">Knowledge Base</a></li>
<li><a href="/de/">German Website</a></li>
</ul>
</li>
<li><a href="#"><i class="material-icons">share</i></a>
<ul>
<li><a href="https://www.facebook.com/sharer/sharer.php?u=https%3A//www.arclab.com" target="_blank">Share on Facebook</a></li>
<li><a href="https://twitter.com/home?status=https%3A//www.arclab.com" target="_blank">Share on Twitter</a></li>
<li><a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=http%3A//www.arclab.com&amp;title=&amp;summary=&amp;source=" target="_blank">Share on LinkedIn</a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
<!-- ARCLAB.com End Header -->
<div id="content">
<!-- Content Start -->
<div class="c-h" style="height: 18em;">
<div class="c-def">
<h1>Arclab Software</h1>
<h2>Email Solutions, Database and Website Software</h2>
<p> </p>
<p><a class="butt-header" href="download.html">Download</a>   <a class="butt-header" href="store.html">Purchase</a></p>
</div>
</div>
<div class="c-def">
<div class="b-t b-l b-r" style="width: 34%; float: left; padding: 2em;">
<h4><a href="amlc/">ArclabÂ® MailList Controller</a></h4>
<p>Email Newsletter Software and<br/>Mailing List Management for Windows</p>
<p><a href="amlc/">Homepage <i class="material-icons">arrow_forward</i></a></p>
</div>
<div class="b-t b-r" style="width: 33%; float: left; padding: 2em;">
<h4><a href="webformbuilder/">ArclabÂ® Web Form Builder</a></h4>
<p>Online Form Generator Software and<br/>Web Form Editor for Windows PC</p>
<p><a href="webformbuilder/">Homepage <i class="material-icons">arrow_forward</i></a></p>
</div>
<div class="b-t b-r" style="width: 33%; float: left; padding: 2em;">
<h4><a href="inbox2db/">ArclabÂ® Inbox2DB</a></h4>
<p>Email/Form to Database Parser<br/>Software for Windows PC and Server</p>
<p><a href="inbox2db/">Homepage <i class="material-icons">arrow_forward</i></a></p>
</div>
<div class="b-t b-b b-l b-r" style="width: 34%; float: left; padding: 2em;">
<h4><a href="websitelinkanalyzer/">ArclabÂ® Website Link Analyzer</a></h4>
<p>Page and Link Checker<br/>Website Audit Software for Windows</p>
<p><a href="websitelinkanalyzer/">Homepage <i class="material-icons">arrow_forward</i></a></p>
</div>
<div class="b-t b-b b-r" style="width: 33%; float: left; padding: 2em;">
<h4><a href="dir2html/">ArclabÂ® Dir2HTML</a></h4>
<p>Directory to HTML Converter and<br/>HTML Index Generator for Windows</p>
<p><a href="dir2html/">Homepage <i class="material-icons">arrow_forward</i></a></p>
</div>
<div class="b-t b-b b-r" style="width: 33%; float: left; padding: 2em;">
<h4><a href="watermarkstudio/">ArclabÂ® Watermark Studio</a></h4>
<p>Batch Watermark Software and Digital Photo Copy Protection for Windows 
		PC</p>
<p><a href="watermarkstudio/">Homepage <i class="material-icons">arrow_forward</i></a></p>
</div>
</div>
<div class="footer-push"></div>
<!-- Content End -->
</div>
<!-- ARCLAB.com Start Footer -->
<div id="footer">
<div id="footer-menu">
<ul>
<li style="width:76%"><p>©1997-2021 Arclab®. All other trademarks and brand names are the property of their respective owners.</p></li>
<li style="width:6%"><a href="/en/about.html"><i class="material-icons">info_outline</i></a></li>
<li style="width:18%"><a href="/en/cookies-privacy.html"><i class="material-icons">fingerprint</i>  Cookies &amp; Privacy</a></li>
</ul>
</div>
</div>
<!-- ARCLAB Start Analytics - IP Address anonymized - Cookies disabled -->
<script>
function arclabSession() 
{
	function getUuid(){return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g,function(c){var r=Math.random()*16|0,v=c=='x'?r:(r&0x3|0x8);return v.toString(16);});}
	var id=getUuid();if(window.sessionStorage)sessionStorage.setItem('SESSID',id);
    return id;
}
var arclabSessionID=(window.sessionStorage&&sessionStorage.getItem('SESSID'))||arclabSession();

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},
i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})
(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-1537044-1', {'storage' : 'none', 'storeGac' : false, 'clientId' : arclabSessionID});
ga('set', 'anonymizeIp', true);
ga('send', 'pageview');
</script>
<!-- End Analytics -->
<!-- ARCLAB.com End Footer -->
</body>
</html>
