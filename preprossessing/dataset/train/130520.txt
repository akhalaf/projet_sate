<!-- HTML BEGINS --><html lang="en">
<head>
<title>Antony Johnston | Sample Scripts and Templates</title>
<meta content="Sample comic and graphic novel scripts written by Antony Johnston, creator of Atomic Blonde" name="description"/>
<link href="https://www.antonyjohnston.com/css/ajstyle.css" rel="stylesheet" type="text/css"/>
<link href="/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<!-- META INFORMATION -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<!-- GOOGLE ANALYTICS -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-609300-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- TWITTER -->
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@AntonyJohnston" name="twitter:creator"/>
<!-- OPENGRAPH -->
<meta content="Sample Scripts and Templates" property="og:title"/>
<meta content="article" property="og:type"/>
<meta content="https://antonyjohnston.com/images/AJ-social-site-default.jpg" property="og:image"/>
<meta content="https://antonyjohnston.com/forwriters/samplescripts.php" property="og:url"/>
<meta content="Sample comic and graphic novel scripts written by Antony Johnston, creator of ATOMIC BLONDE." property="og:description"/>
</head>
<!-- BODY BEGINS -->
<body>
<!-- PAGE HEADER & NAVBAR -->
<!-- PAGE TITLE -->
<div id="siteheaderbox">
<div id="siteheader">
<a href="https://antonyjohnston.com/"><img class="siteheaderpic" src="/images/aj-siteheader-pic-2019.jpg"/></a>
<a href="https://antonyjohnston.com/"><img class="siteheadername" src="/images/aj-siteheader-name-2019.png"/></a>
<div id="siteheadertext">
<p class="siteheaderbody">
			New York Times bestseller   &amp;bullet;   <span style="text-transform:none;"><span class="siteheaderbodylink"><a href="https://ajwriter.substack.com/" target="_blank">SIGN UP</a></span> for Antony's newsletter</span>
</p>
</div>
</div>
</div>
<!-- NAVBAR -->
<div id="navbar">
<nav role="navigation">
<ul>
<li>
<a href="/work/">
		Work
	</a>
<ul>
<li><a href="/work/fiction/index.php">Fiction</a></li>
<br/>
<li><a href="/work/nonfiction/index.php">Non-Fiction</a></li>
<br/>
<li><a href="/work/graphicnovels/index.php">Graphic Novels</a></li>
<br/>
<li><a href="/work/videogames/index.php">Games</a></li>
<br/>
<li><a href="/work/film/index.php">Film</a></li>
<br/>
<li><a href="/work/podcasts/index.php">Podcasts</a></li>
<br/>
<li><a href="/work/music/index.php">Music</a></li>
</ul>
</li>
<li>
<a href="/about/">
		About
	</a>
</li>
<li>
<a href="/forwriters/">
		For Writers
	</a>
</li>
<li>
<a href="/contact/">
		Contact
	</a>
</li>
<li>
<div class="navbartwitter" id="navbarsocial">
<a href="https://twitter.com/AntonyJohnston/" target="_blank"><img alt="Antony on Twitter" height="50" src="https://antonyjohnston.com/images/spacer.gif" width="50"/></a>
</div>
</li>
<li>
<div class="navbarfacebook" id="navbarsocial">
<a href="https://www.facebook.com/AntonyJohnston" target="_blank"><img alt="Antony on Facebook" height="50" src="https://antonyjohnston.com/images/spacer.gif" width="50"/></a>
</div>
</li>
<li>
<div class="navbarinstagram" id="navbarsocial">
<a href="https://instagram.com/AntonyJohnston/" target="_blank"><img alt="Antony on Instagram" height="50" src="https://antonyjohnston.com/images/spacer.gif" width="50"/></a>
</div>
</li>
</ul>
</nav>
</div>
<br clear="all"/>
<!-- MAIN CONTENT BEGINS -->
<div id="main">
<!-- MAIN CONTENT BEGINS -->
<div class="pagetitle">
	Sample comic scripts
</div>
<p class="indent">
<em>A selection of scripts from Antony's graphic novels and comics, which you may find useful. Dates refer to when the script was written; publication year may differ.</em>
</p>
<p class="indent">
<em><strong>Artists:</strong> <a href="https://antonyjohnston.com/forwriters/samplescripts.php#forartists">click here</a> for a sample script you can work from.</em>
</p>
<!-- SAMPLE SCRIPTS -->
<p class="subhead">
<a href="https://antonyjohnston.com/forwriters/images/ajcomicscripts/AJ-julius-acti-script.pdf" target="_blank">
<img class="coverleftsmall" src="https://antonyjohnston.com/work/julius/images/julius-cover.jpg"/>
		JULIUS (2003)
	</a>
</p>
<p>
	Act I of the script for the Shakespearean graphic novel <a href="https://antonyjohnston.com/work/julius/">JULIUS</a>. This script shows Antony's original script format (JULIUS was written entirely in Final Draft, before he switched to Scrivener), and is a good example of how to direct an artist you've never worked with before.
</p>
<p>
<a href="https://antonyjohnston.com/forwriters/images/ajcomicscripts/AJ-julius-acti-script.pdf" target="_blank"><img alt="download the free PDF" src="https://antonyjohnston.com/images/downloadpdf.svg" width="200"/></a>
</p>
<br clear="all"/>
<p class="subhead">
<a href="https://antonyjohnston.com/forwriters/images/ajcomicscripts/AJ-wasteland-9-script.pdf" target="_blank">
<img class="coverleftsmall" src="https://antonyjohnston.com/work/wasteland/images/wasteland9-cover.jpg"/>
		WASTELAND #9 (2006)
	</a>
</p>
<p>
	This script for issue #9 of <a href="https://antonyjohnston.com/work/wasteland/">WASTELAND</a> shows a later format, with some tweaks. The style is also looser and more casual — by the time this was written for artist Christopher Mitten, he and Antony had been working together for almost a year.
</p>
<p>
<a href="https://antonyjohnston.com/forwriters/images/ajcomicscripts/AJ-wasteland-9-script.pdf" target="_blank"><img alt="download the free PDF" src="https://antonyjohnston.com/images/downloadpdf.svg" width="200"/></a>
</p>
<br clear="all"/>
<p class="subhead">
<a href="https://antonyjohnston.com/forwriters/images/ajcomicscripts/AJ-shadowlandbots1-script.pdf" target="_blank">
<img class="coverleftsmall" src="https://antonyjohnston.com/work/shadowlandbloodonthestreets/images/slbots1-cover.jpg"/>
		SHADOWLAND: BLOOD ON THE STREETS #1 (2010)
	</a>
</p>
<p>
<a href="https://antonyjohnston.com/work/shadowlandbloodonthestreets/">SHADOWLAND: BLOOD ON THE STREETS</a> uses the same essential format, despite Antony switching from FD to Scrivener in 2007. This script was another case of working with a new artist, so some of the direction is quite formal and detailed.
</p>
<p>
<a href="https://antonyjohnston.com/forwriters/images/ajcomicscripts/AJ-shadowlandbots1-script.pdf" target="_blank"><img alt="download the free PDF" src="https://antonyjohnston.com/images/downloadpdf.svg" width="200"/></a>
</p>
<br clear="all"/>
<p class="subhead">
<a href="https://antonyjohnston.com/forwriters/images/ajcomicscripts/AJ-umbral-1-script.pdf" target="_blank">
<img class="coverleftsmall" src="https://antonyjohnston.com/work/umbral/images/umbral1-cover.jpg"/>
		UMBRAL #1 (2013)
	</a>
</p>
<p>
	The debut issue of <a href="https://antonyjohnston.com/work/umbral/">UMBRAL</a>. Another format tweak, though still easily recognisable. Stylistically, though, this script is extremely informal — by this time, Antony and Christopher Mitten had produced more than 700 pages of comics together. <em>See the <a href="https://antonyjohnston.tumblr.com/post/67173963592/writers-notes-umbral-1-hrrrm-never-done" target="_blank">writer's notes</a> for this issue on tumblr.</em>
</p>
<p>
<a href="https://antonyjohnston.com/forwriters/images/ajcomicscripts/AJ-umbral-1-script.pdf" target="_blank"><img alt="download the free PDF" src="https://antonyjohnston.com/images/downloadpdf.svg" width="200"/></a>
</p>
<br clear="all"/>
<!-- SAMPLE FOR ARTISTS -->
<a name="forartists"></a>
<p class="subtitle">
	For Artists
</p>
<p class="subhead">
<a href="https://antonyjohnston.com/forwriters/images/ajcomicscripts/AJ-Horror-Sample-Comic-Script.pdf" target="_blank">
<img class="coverleftsmall" src="https://antonyjohnston.com/forwriters/images/ajcomicscripts/horrorsamplescript.jpg"/>
		Sample ‘Horror’ Comic Script
	</a>
</p>
<p>
	In 2005 Antony wrote a five-page sample script for use by artists entering the Oni Press talent search at Comic-Con. This re-formatted version is now freely available for any artist who wants to practice, or draw submission pages, by working from a ‘real’ script.
</p>
<p>
<a href="https://antonyjohnston.com/forwriters/images/ajcomicscripts/AJ-Horror-Sample-Comic-Script.pdf" target="_blank"><img alt="download the free PDF" src="https://antonyjohnston.com/images/downloadpdf.svg" width="200"/></a>
</p>
<br clear="all"/>
<!-- TEMPLATES -->
<p class="subtitle">
	Comic script templates
</p>
<p class="subhead">
<a href="https://literatureandlatte.com/" target="_blank">
		Scrivener
	</a>
</p>
<p>
	Scrivener features a built-in Comic Script template which Antony designed in conjunction with the developer, based on his screenplay-like format. <a href="/forwriters/">Click here</a> for articles showing how to use it.
</p>
<p class="indent">
	&amp;bullet; <a href="https://literatureandlatte.com/" target="_blank">Visit the Scrivener website</a>
</p><p class="subhead">
<a href="https://antonyjohnston.com/forwriters/images/ajcomicscripts/AJ-comicscript-fd8.zip">
		Final Draft
	</a>
</p>
<p>
	Final Draft now includes some graphic novel templates with the application, but they are rather perfunctory and old-fashioned. Antony continues to prefer his own.

</p><p class="indent">
	&amp;bullet; <a href="https://antonyjohnston.com/forwriters/images/ajcomicscripts/AJ-comicscript-fd8.zip" target="_blank">Download the Final Draft template</a>
</p><p class="copy">
<strong>NB</strong> This template was originally designed for Final Draft v8 on Mac OS X. It <i>should</i> run on later platforms and versions, but we can't guarantee that, and we don't support it in any way. The template is supplied as-is, with no warranty or indemnity given or implied. If Final Draft breaks or your computer blows up, it's not our fault. By downloading the template, you agree to this disclaimer.
</p>
<p class="subhead">
<a href="https://antonyjohnston.com/forwriters/images/fountain/AJ-fountain-comics-template.fountain" target="_blank">
		Fountain/Highland/Slugline
	</a>
</p>
<p>
	The <a href="http://fountain.io/" target="_blank">Fountain</a> plaintext markup language was created by <a href="http://johnaugust.com" target="_blank">John August</a> and <a href="https://prolost.com" target="_blank">Stu Maschwitz</a> for use in screenwriting. Antony uses it to this end for screenplays, but the format can also be used for comics, as this template shows.
</p>
<p class="indent">
	&amp;bullet; <a href="https://antonyjohnston.com/forwriters/images/fountain/AJ-fountain-comics-template.fountain" target="_blank">Download</a> the Fountain template.

</p><p class="indent">
	&amp;bullet; <a href="https://antonyjohnston.com/forwriters/images/fountain/AJ-shadowlandbots1-script.fountain" target="_blank">Download and read</a> the SHADOWLAND: BLOOD ON THE STREETS #1 script, re-formatted in Fountain. It wasn't originally written this way, but it's a good example of how a full comic script looks in this format.

</p><p>
	Software designed to use Fountain:
</p>
<p class="indent">
	&amp;bullet; <a href="https://quoteunquoteapps.com/highland-2/" target="_blank">Highland</a>, a screenwriting app from John August. As of v2, Highland contains a built-in Graphic Novel format similar to Antony's template.
</p>
<p class="indent">
	&amp;bullet; <a href="https://slugline.co/" target="_blank">Slugline</a>, a screenwriting app from Stu Maschwitz.
</p>
<br clear="all"/>
<p class="subhead">
<a href="index.php">
		« Go back to the ‘For Writers’ index
	</a>
</p>
<!-- FOOTER -->
<br clear="all"/>
<div id="footer">
<span class="copy">
<a href="https://antonyjohnston.com/">Home</a>  |  <a href="#top">Top</a>  |  
All contents © 1996-2021 Antony Johnston unless otherwise stated.
</span>
<br clear="all"/>
</div>
<!-- MAIN CONTENT ENDS -->
</div>
</body>
</html>
