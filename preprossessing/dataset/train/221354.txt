<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<meta content="index, follow" name="robots"/>
<meta content="windows firewall control, wfc, firewall notifications, firewall learning mode, firewall, binisoft" name="keywords"/>
<meta content="BiniSoft.org" name="title"/>
<meta content="Windows Firewall Control is a powerful tool which extends the functionality of Windows Firewall by adding outbound notifications and many other features." name="description"/>
<link href="images/logo.ico" rel="shortcut icon"/>
<title>BiniSoft.org</title>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="container">
<div id="top">
<img alt="Website logo" src="images/logo.png"/>
</div>
<div id="topnavigation">
<div id="topleft">
<div class="navbar">
<a href="/">Home</a>
<div class="dropdown">
<button class="dropbtn">Products<img alt="Down arrow" src="images/down.png"/>
</button>
<div class="dropdown-content">
<a href="wfc">Windows Firewall Control</a>
<a href="usbc">USB Flash Drives Control</a>
</div>
</div>
<a href="faq">Frequently asked questions</a>
<a href="contact">Contact</a>
</div>
</div>
<div id="topright">
<div class="navbar">
<a href="login">My account</a>
</div>
</div>
</div>
<div id="content">
<div id="content-index">
<img alt="" src="images/MB_LOGO_BLUE.png" width="60%"/>
<h1>Welcome, Binisoft fans</h1>
<p>
			We at Malwarebytes are big fans too. That’s why we’ve asked Binisoft founder Alexandru Dicu to join our team. 
			And not to worry—we will maintain, support, and keep Binisoft products free for everyone in the short term.
			</p>
<ul class="columns">
<li>
<img alt="WFC icon" src="images/icon_w.png"/>
<h1><a href="wfc">Windows Firewall Control</a></h1>
<p>Windows Firewall Control is a powerful tool which
					extends the functionality of Windows Firewall and provides
					quick access to the most frequent options of the Windows
					Firewall. It runs in the system tray and allows user to control
					the native firewall easily without having to waste time by
					navigating to the specific part of the firewall.
					</p>
</li>
<li>
<img alt="USBC icon" src="images/icon_u.png"/>
<h1><a href="usbc">USB Flash Drives Control</a></h1>
<p>USB Flash Drives Control is a small and easy to use utility
					that runs in system tray, next to the system clock, and provides quick
					access to control the way in which USB flash drives are used on 
					your computer. This tool offers access to few, but 
					powerful, options that help you control how your USB removable
					drives are used.
					</p>
</li>
</ul>
</div>
</div>
<div id="footer">
		Copyright © 2010-2020 BiniSoft.org. All rights reserved.
	</div>
</div>
</body>
</html>