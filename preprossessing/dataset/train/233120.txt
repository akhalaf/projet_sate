<!DOCTYPE html>
<html lang="en" ng-app="BKWEB" ng-cloak="">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport"/>
<meta content="Find best deals at Biyaheko for Flight Tickets, Hotels, Holiday Packages and Bus Reservations for Philippines &amp; International travel. Book cheap air tickets online for Domestic &amp; International airlines, customized holiday packages and special deals on Hotel Bookings." name="description"/>
<meta content="Philippines,Philippines travel,B2B, Cebu Pacific, ZestAir, Philippines Airlines, Airphil Express,cheap air tickets, cheap flights, flight, hotels, hotel, holidays, bus tickets, air travel, air tickets, holiday packages, travel packages,hnI-5OkMOvjiZwsaxf2-dsQ_lOg" name="keywords"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:600" rel="stylesheet"/>
<link href="/Content/Style0_1?v=rxOBK5VsWvqUCk_tbJIwlsPc5dcZSlOlMh62iOgJSgQ1" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.13/moment-timezone.min.js" type="text/javascript"></script>
<link href="/Content/Css/md-data-table.css" rel="stylesheet"/>
<style>
       md-menu-bar
        {
        padding:0px;
        }
    </style>
</head>
<body ng-controller="LayoutCtrl">
<title ng-bind-template="{{titlecontent}}"></title>
<link href="{{favicon}}" rel="shortcut icon" type="image/x-icon"/>
<div class="layout" data-loading="">
<div>
<md-progress-linear class="md-hue-2" md-diameter="40" md-mode="indeterminate" style="z-index: 10001; position: fixed;"></md-progress-linear>
<md-backdrop aria-hidden="true" class="md-dialog-backdrop md-opaque ng-scope" style="height: 100%; position: fixed;z-index: 10000; opacity: 0.12;"></md-backdrop>
</div>
</div>
<div layout="row" ng-if="IsLoggedIn">
<div class="flex-5" hide-xs=""> </div>
<div class="flex-90" flex-xs="100">
<div class="layout-row flex" layout-align="start end" style="background-color:#ffffff">
<div class="flex-gt-xs-60" ng-if="Logoimage"><img class="flex-xs-90 flex-gt-xs-60" ng-click="Gotomenu('home');" ng-src="{{Logoimage}}" style="cursor:pointer;position:absolute;top:50px"/></div>
<div class="flex-gt-xs-70 clsLogo" ng-if="!IsLoggedIn  &amp;&amp;  Issmart"><img class="flex-xs-60 flex-gt-xs-40 " ng-click="Gotomenu('home');" style="cursor:pointer"/></div>
<div ng-class="!IsLoggedIn ? ' ' : 'flex-gt-xs-60' "></div>
<div class="flex-gt-xs-100 layout-padding hide-xs" ng-if="IsLoggedIn" style="color:#0f2b4a; font-size: 14px;text-align:right">
<div layout="column">
<b style="text-transform: uppercase;vertical-align:super">{{Time | date:'dd MMM yyyy HH:mm:ss a'}}</b><br/>
<b style="vertical-align:super">{{AgentDetails.COMPANYNAME|uppercase}}</b> <br/>
<b style="vertical-align:super">TERMINAL ID : {{AgentDetails.TERMINALID |uppercase}}</b>
</div>
<div flex="" layout="row" layout-align="end center" style="font-size:11px">
<div>
<div class="flex" layout="row" layout-align="center center" style="font-size: 14px; border-radius: 5px; background-color: #0f2b4a; color: #fff; padding: 4px 0">
<div layout="">
<div flex="15">
<ng-md-icon class="link" icon="refresh" ng-click="GetBalance();" size="20" style="color:rgb(255, 255, 255)"></ng-md-icon>
</div>
<div flex="" ng-if="BalanceView" style="text-align: right;">
<lable style="color:rgb(255, 255, 255)">   PHP   {{BalanceAmt | number:2}} </lable>
</div>
<div flex="" ng-if="!BalanceView" style="text-align: left;font-size:12px">
                                        BALANCE AMOUNT
                                    </div>
</div>
</div>
</div>
<div>
<md-menu md-position-mode="target-right target">
<ng-md-icon class="hide-xs" icon="account_circle" ng-click="$mdOpenMenu()" size="32" style="cursor: pointer; top: 0px; vertical-align: sub; margin-right: 10px;"></ng-md-icon>
<md-menu-content>
<md-menu-item aria-label="">
<md-button ng-click="Gotomenu('accountstatement')"><b style="color: #0f2b4a;">Account Statement</b></md-button>
</md-menu-item>
<md-menu-item aria-label="">
<md-button ng-click="Gotomenu('profile')"><b style="color: #0f2b4a;">My Profile</b></md-button>
</md-menu-item>
<md-menu-item aria-label="">
<md-button ng-click="Gotomenu('changepassword')"><b style="color: #0f2b4a;">Change Password</b></md-button>
</md-menu-item>
<md-menu-item aria-label="">
<md-button ng-click="GotoLogout();"><b style="color: #0f2b4a;">Logout</b></md-button>
</md-menu-item>
</md-menu-content>
</md-menu>
</div>
</div>
</div>
</div>
</div>
<div class="flex-5" hide-xs=""> </div>
</div>
<div class="layout-column">
<div class="hide-sm hide-md hide-lg hide-gt-xs" ng-if="IsLoggedIn">
<md-sidenav class="md-sidenav-left" md-component-id="left" md-disable-close-events="" md-whiteframe="4">
<md-content class="layout-padding bgprimary">
<div ng-repeat="Menu in Menus">
<div class="white" ng-click="ShowSubMenu = (ShowSubMenu =='Myhide' ? 'Myshow' : 'Myhide')" style="cursor:pointer;">
<div class="layout-row layout-align-space-between-center">
<div class="md-padding"> {{Menu.Main | uppercase}}</div>
<div ng-init="ShowSubMenu='Myhide'">
<md-button aria-label="test" ng-click="ShowSubMenu = (ShowSubMenu =='Myhide' ? 'Myshow' : 'Myhide')">
<ng-md-icon icon="{{ShowSubMenu =='Myshow' ? 'expand_less':'expand_more'}}" style="color:#fff;"></ng-md-icon>
</md-button>
</div>
</div>
<div ng-class="ShowSubMenu">
<div class="white" ng-repeat="item in Menu.SubMenu">
<md-button ng-click="Gotomenu(item.Url)"><b>{{item.Name | uppercase}}</b></md-button>
<md-divider></md-divider>
</div>
</div>
</div>
<md-divider></md-divider>
</div>
<div class="layout-row layout-align-space-between-center">
<div class="md-padding white">
<md-button ng-click="GotoLogout();">
<b>Logout</b>
</md-button>
</div>
</div>
</md-content>
</md-sidenav>
<md-button aria-label="Comment" class="md-fab bgprimary" ng-click="openSideMenu()">
<ng-md-icon icon="menu"></ng-md-icon>
</md-button>
</div>
<div class="layout flex" id="menu" ng-if="IsLoggedIn">
<div class="flex-5" hide-xs=""> </div>
<div class="flex-90" flex-xs="100">
<div class="layout-row hide-xs bg-Color-2 " style="background-color: #0f2b4a; color: rgb(255, 255, 255); ">
<div class="flex-5 layout layout-align-center-center" style="border-right: 1px solid rgb(221, 221, 221);">
<ng-md-icon aria-label="" icon="home" ng-click="Gotomenu('home');"></ng-md-icon>
</div>
<div ng-if="Menu.ID != '10'" ng-repeat="Menu in Menus" style="border-right: 1px solid rgb(221, 221, 221);">
<md-menu-bar>
<md-menu ng-if="Menu.SubMenu">
<md-button class="menuBtn" ng-click="$mdOpenMenu()">
<ng-md-icon icon="{{Menu.Icon}}"></ng-md-icon>
<b>{{Menu.Main}}</b>
</md-button>
<md-menu-content width="3">
<md-menu-item aria-label="" ng-repeat="item in Menu.SubMenu">
<md-button aria-label="" ng-click="Gotomenu(item.Url)"><b style="color: #0f2b4a;">{{item.Name | uppercase}}</b></md-button>
</md-menu-item>
</md-menu-content>
</md-menu>
<md-menu ng-if="!Menu.SubMenu">
<md-button ng-click="Gotomenu(Menu.Url)">
<ng-md-icon icon="{{Menu.Icon}}"></ng-md-icon>
<b>{{Menu.Main}}</b>
</md-button>
</md-menu>
</md-menu-bar>
</div>
<div style="border-right: 1px solid #fff;">
<md-menu>
<md-button ng-click="Gotomenu('Offer')">
<ng-md-icon icon="local_offer"></ng-md-icon>
<b>Promo</b>
</md-button>
</md-menu>
</div>
<div>
<md-menu>
<md-button ng-click="Gotomenu('Topup')">
<ng-md-icon icon="local_offer"></ng-md-icon>
<b>Topup</b>
</md-button>
</md-menu>
</div>
</div>
</div>
<div class="flex-5" hide-xs=""> </div>
</div>
<div class="layout flex BodyContent">
<div class="flex-5" hide-xs="" ng-if="IsLoggedIn"> </div>
<div flex-xs="100" ng-class="(IsLoggedIn ? 'flex-90' : 'flex-100' )">
<div ng-cloak="" ng-view=""></div>
</div>
<div class="flex-5" hide-xs="" ng-if="IsLoggedIn"> </div>
</div>
</div>
<div ng-if="!IsLoggedIn" style="height:10px"></div>
<div id="footer">
<div style="background-color: #e3e3e3;text-align:center;padding:10px 0px">
<img alt="FooterImg" class="flex-100" src="/Content/Images/our-partners.png" style="width:100%"/>
</div>
<div layout="row" layout-align="space-around center" style="background-color: rgb(58,58,58); margin-top: -3px; color: rgb(102,102,102); display:none;">
<div flex="100" layout="row">
<div flex="25"></div>
<div flex="10"><a href="#" style="color:#808080; text-decoration:none;">HOME</a></div>
<div flex="10"><a href="#/aboutus" style="color:#808080;text-decoration:none;">ABOUT US</a></div>
<div flex="10"><a href="#/contactus" style="color:#808080;text-decoration:none;">CONTACT US</a></div>
<div flex="10"><a href="#/privacypolicy" style="color:#808080;text-decoration:none;">PRIVACY POLICY</a></div>
<div flex="10"><a href="#/contactus" style="color:#808080;text-decoration:none;">UTILITIES</a></div>
<div flex="25"></div>
</div>
</div>
<div class="layout-row layout-padding layout-align-space-around-center" style="background-color: rgb(58,58,58); margin-top: -3px; color: rgb(102,102,102); ">
<div>
<b>
                    COPYRIGHT <ng-md-icon icon="copyright" size="18"></ng-md-icon> 2018
                    <div ng-if="BkURL" style="display:inline-block;position:absolute;padding-left:5px">
<a href="http://www.gicorpphil.com" target="_blank"></a> <img alt="GICORP" src="/images/GI.jpg"/>
</div>
</b>
</div>
<div> <b>       ALL RIGHTS RESERVED.</b></div>
</div>
</div>
<div class="loader-Back-cover hide" id="loading-bar-container" style="display:none;"></div>
<script src="/bundles/library0_1?v=A3O3cSBC_F-nd5HcD3ri4UhjntpDEm4MM_WuUKEy2_E1"></script>
<script src="/bundles/script0_1?v=ArTjpnr37vCtez9qqEXkcHybt2z5k-E3uxXCKixwn4M1"></script>
<script src="/Content/App/Layout/Layout.js?ver=13.5" type="text/javascript"></script>
<script src="/Content/App/Ticketing/Ticekting.js?v=13.5"></script>
<script src="/Content/Lib/angular-bootstrap-calendar.min.js" type="text/javascript"></script>
<script src="/Content/Lib/md-data-table.min.js"></script>
</body>
</html>