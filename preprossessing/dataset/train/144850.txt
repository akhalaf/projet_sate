<!DOCTYPE html>

<html>
<head>
<title>Hébergement Web - Gestion de Cloud Privé et Public - Aqua Ray</title>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport"/>
<base href="/"/>
<meta content="Aqua Ray : infrastructure d'hébergement Web sur mesure, sites Web et applications SaaS, services de colocation et location de serveurs VPS et physiques en infogérance sur systèmes Linux et Mac OS X." name="description"/>
<meta content="hebergement Web, baie serveur, infogérance serveur dédié linux, vps ssd, serveur dédié infogéré, maintenance serveur, centre de données, data-center france, datacenter france, mac os x server, machine virtuelle linux" name="keywords"/>
<meta content="index,follow" name="robots"/>
<link href="/media/img/favicon.ico?v=3" rel="shortcut icon" type="image/x-icon"/>
<link href="/media/img/favicon.ico" rel="icon" type="image/x-icon"/>
<!-- Boostrap CSS -->
<link crossorigin="anonymous" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" rel="stylesheet"/>
<!-- Bootstrap optional theme CSS -->
<link crossorigin="anonymous" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" rel="stylesheet"/>
<!-- Gridlex -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/gridlex/2.6.1/gridlex.min.css" rel="stylesheet"/>
<!-- Google Font Raleway -->
<link href="https://fonts.googleapis.com/css?family=Raleway:200,300,400,500,600,700" rel="stylesheet"/>
<!-- Font awesome -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<!-- Slick carousel -->
<link href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css" rel="stylesheet" type="text/css"/>
<!-- Selectric -->
<link href="/libs/selectric/selectric.css" rel="stylesheet" type="text/css"/>
<!-- Sweet Alert -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css" rel="stylesheet"/>
<link href="/media/css/style.css?20181205002" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" rel="stylesheet" type="text/css"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-11752919-14"></script>
<script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-11752919-14');
    </script>
</head>
<body class="fade-in " data-lang="fr">
<nav class="display-none" id="menuMobile">
<div class="menuMobileWrapper">
<div class="menuMobileItems languages grid-2">
<a class="col menuMobileItem language" href="/">
<img alt="En français" src="/media/img/layout/fr.png"/>
                Français
            </a>
<a class="col menuMobileItem language" href="/en/">
<img alt="In english" src="/media/img/layout/en.png"/>
                English
            </a>
</div>
<a class="logo" href="/">
<img alt="Logo" src="/media/img/layout/logo.png"/>
</a>
<div class="menuMobileItems">
<a class="menuMobileItem active" href="/">
<i class="fa fa-fw fa-chevron-left"></i>
            Accueil
        </a>
<a class="menuMobileItem " href="/hosting-ready/">
<i class="fa fa-fw fa-chevron-left"></i>
            Hosting Ready
        </a>
<a class="menuMobileItem orange " href="/enterprise-services/">
<i class="fa fa-fw fa-chevron-left"></i>
            Enterprise Services
        </a>
<a class="menuMobileItem " href="/le-lab/">
<i class="fa fa-fw fa-chevron-left"></i>
            Le Lab'
        </a>
<a class="menuMobileItem " href="/blog">
<i class="fa fa-fw fa-chevron-left"></i>
            Aquablog
        </a>
<a class="menuMobileItem " href="/a-propos-hebergeur-web/">
<i class="fa fa-fw fa-chevron-left"></i>
            A propos
        </a>
</div>
<a class="phone" href="tel:+33184040405">
<div class="flex flex-align-items-center">
<img alt="Phone" src="/media/img/layout/phone1.png"/>
<span class="number">01 84 04 04 05</span>
</div>
<span class="info">Assistance commerciale<br/>(Appel non surtaxé)</span>
</a>
<div class="menuMobileItems">
<a class="menuMobileItem" href="https://extranet.aquaray.com" target="_blank">
<img alt="Espace Client" class="picto" src="/media/img/layout/espace-client.png"/>
            Espace Client
        </a>
<a class="menuMobileItem" href="https://webmail.aquaray.com" target="_blank">
<img alt="Webmail" class="picto" src="/media/img/layout/webmail.png"/>
            Webmail
        </a>
</div>
</div> </nav>
<main data-slideout-ignore="" id="panel">
<section class="preHeaderWrapper mobile hideOnDesktop">
<div class="grid-container">
<a class="headerLogo" href="/">
<img alt="Logo" src="/media/img/layout/logo.png"/>
</a>
<div class="flex-1"></div>
<button class="toggleMenu">
<span></span>
<span></span>
<span></span>
</button>
</div>
</section>
<section class="preHeaderWrapper hideOnNotDesktop">
<div class="grid-container height-100p">
<a class="preHeaderLink" href="https://extranet.aquaray.com" target="_blank">
<img alt="Espace Client" class="picto" src="/media/img/layout/espace-client.png"/>
            Espace Client
        </a>
<a class="preHeaderLink" href="https://webmail.aquaray.com" target="_blank">
<img alt="Webmail" class="picto" src="/media/img/layout/webmail.png"/>
            Webmail
        </a>
<div class=" flex-1"></div>
<a class="preHeaderLink" href="/blog">Aquablog</a>
<a class="preHeaderLink language" href="/">
<img alt="En français" src="/media/img/layout/fr.png"/>
</a>
<a class="preHeaderLink language" href="/en/">
<img alt="In english" src="/media/img/layout/en.png"/>
</a>
</div>
</section> <section class="headerWrapper hideOnNotDesktop">
<div class="grid-container grid-spaceBetween height-100p">
<a class="headerLogo" href="/">
<img alt="Logo" src="/media/img/layout/logo.png"/>
</a>
<a class="headerPhone" href="tel:+33184040405">
<img alt="Phone" src="/media/img/layout/phone1.png"/>
<span class="flex-column flex-center">
<span class="number">01 84 04 04 05</span>
<span class="info">Assistance commerciale (Appel non surtaxé)</span>
</span>
</a>
<div class="menuWrapper height-100p">
<div class="menuItem bleu">
<a href="/hosting-ready/">
<span>Hosting Ready</span>
</a>
<div class="triangle"></div>
<div class="megaMenu">
<div class="grid-container">
<div class="grid width-100p">
<div class="col megaMenuItem">
<a class="megaMenuTitle" href="/hosting-ready/#nos-vps">
                                Nos VPS
                            </a>
<div class="catchline">A partir de <strong>7.95 €</strong> HT / mois</div>
<div class="produitsWrapper">
<div class="produit">
<a href="/hosting-ready/commandes/configuration/serveurs-dedies/VPS-Cloud-Public-Aqua-Ray">VPS Cloud Public Aqua Ray</a>
</div>
</div>
</div>
<div class="col megaMenuItem">
<a class="megaMenuTitle" href="/hosting-ready/#serveurs-dedies">
                                Serveurs dédiés
                            </a>
<div class="catchline">A partir de <strong>69 €</strong> HT / mois</div>
<div class="produitsWrapper">
<div class="titreProduit">Gamme Business Box</div>
<div class="produit">
<a href="/hosting-ready/commandes/configuration/serveurs-dedies/Business-Box-XL-SSD">Business Box XL SSD</a>
</div>
<div class="produit">
<a href="/hosting-ready/commandes/configuration/serveurs-dedies/Business-Box-Karnak-SSD">Business Box Karnak SSD</a>
</div>
<div class="produit">
<a href="/hosting-ready/commandes/configuration/serveurs-dedies/Business-Box-Druss-SSD">Business Box Druss SSD</a>
</div>
</div>
<div class="produitsWrapper">
<div class="titreProduit">Gamme Apple</div>
<div class="produit">
<a href="/hosting-ready/commandes/configuration/serveurs-dedies/Mac-Mini-Sieben">Mac Mini Sieben</a>
</div>
<div class="produit">
<a href="/hosting-ready/commandes/configuration/serveurs-dedies/Mac-Mini-Skil">Mac Mini Skil</a>
</div>
</div>
</div>
<div class="col megaMenuItem">
<a class="megaMenuTitle" href="/hosting-ready/#hebergement-web">
                                Hébergement Web
                            </a>
<div class="catchline">A partir de <strong>12 €</strong> HT / an</div>
<div class="produitsWrapper">
<div class="produit">
<a href="/hosting-ready/commandes/configuration/hebergement/Aqua-One">Aqua One</a>
</div>
<div class="produit">
<a href="/hosting-ready/commandes/configuration/hebergement/Aqua-Pro">Aqua Pro</a>
</div>
</div>
<div class="produitsWrapper">
<div class="produit">
<a href="/hosting-ready/commandes/configuration/hebergement/Aqua-Mail">Aqua Mail</a>
</div>
</div>
</div>
<div class="col megaMenuItem">
<a class="megaMenuTitle" href="/hosting-ready/#noms-de-domaine">
                                Noms de domaine
                            </a>
<div class="catchline">A partir de <strong>6 €</strong> HT / an</div>
<div class="produitsWrapper">
<div class="produit">
<a href="/hosting-ready/commandes/configuration/noms-de-domaine?extension=fr">.fr</a>
</div>
<div class="produit">
<a href="/hosting-ready/commandes/configuration/noms-de-domaine?extension=net">.net</a>
</div>
<div class="produit">
<a href="/hosting-ready/commandes/configuration/noms-de-domaine?extension=com">.com</a>
</div>
<div class="produit">
<a href="/hosting-ready/commandes/configuration/noms-de-domaine?extension=org">.org</a>
</div>
<div class="produit">
<a href="/hosting-ready/commandes/configuration/noms-de-domaine?extension=info">.info</a>
</div>
<div class="produit">
<a href="/hosting-ready/commandes/configuration/noms-de-domaine?extension=eu">.eu</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="menuItem orange">
<a href="/enterprise-services/">
<span>Enterprise Services</span>
</a>
<div class="triangle"></div>
<div class="megaMenu">
<div class="grid-container">
<div class="grid width-100p">
<a class="col megaMenuItem" href="/enterprise-services/#cloud-prive">
<div class="megaMenuTitle">
                                Cloud privé
                            </div>
<div class="catchline catchline_hover_colored">Intégration de Cloud Privé sur mesure</div>
</a>
<a class="col megaMenuItem" href="/enterprise-services/#cloud-public">
<div class="megaMenuTitle">
                                Cloud public
                            </div>
<div class="catchline catchline_hover_colored">VPS Aqua Ray et stockage objet</div>
</a>
<a class="col megaMenuItem" href="/enterprise-services/#cloud-aws">
<div class="megaMenuTitle">
                                Cloud AWS
                            </div>
<div class="catchline catchline_hover_colored">Gestion d'infrastructure par Aqua Ray</div>
</a>
<a class="col megaMenuItem" href="/enterprise-services/#infogerance">
<div class="megaMenuTitle">
                                Infogérance
                            </div>
<div class="catchline catchline_hover_colored">Service d'infogérance LAMP et étendue</div>
</a>
<a class="col megaMenuItem" href="/enterprise-services/#colocation">
<div class="megaMenuTitle">
                                Colocation
                            </div>
<div class="catchline catchline_hover_colored">De la location d'1U à la baie entière</div>
</a>
</div>
</div>
</div>
</div>
<div class="menuItem bleu">
<a href="/le-lab/">
<span>Le Lab'</span>
</a>
</div>
<div class="menuItem bleu">
<a href="/a-propos-hebergeur-web/">
<span>A propos</span>
</a>
<div class="triangle"></div>
<div class="megaMenu">
<div class="grid-container">
<div class="grid width-100p">
<div class="grid width-100p">
<a class="col megaMenuItem" href="/a-propos-hebergeur-web/#aqua-ray">
<div class="megaMenuTitle">
                                    Aqua Ray
                                </div>
<div class="catchline catchline_hover_colored">Expert Linux et Hébergeur Web français depuis 2003</div>
</a>
<a class="col megaMenuItem" href="/a-propos-hebergeur-web/#infrastructure">
<div class="megaMenuTitle">
                                    Infrastructure
                                </div>
<div class="catchline catchline_hover_colored">Centres de données en Ile de France</div>
</a>
<a class="col megaMenuItem" href="/a-propos-hebergeur-web/#references">
<div class="megaMenuTitle">
                                    Références
                                </div>
<div class="catchline catchline_hover_colored">Start-ups, collectivités, banques...</div>
</a>
<a class="col megaMenuItem" href="/a-propos-hebergeur-web/#partenaires">
<div class="megaMenuTitle">
                                    Partenaires
                                </div>
<div class="catchline catchline_hover_colored">Offre apporteurs d'affaires</div>
</a>
<a class="col megaMenuItem" href="/a-propos-hebergeur-web/#revendeurs">
<div class="megaMenuTitle">
                                    Revendeurs
                                </div>
<div class="catchline catchline_hover_colored">Revente de nos services d'hébergement Web</div>
</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<div class="sliderWrapper" style="background-color: #2b75b2">
<h1>Hébergeur Web spécialiste en gestion d'infrastructures sous Linux et logiciels libres</h1>
<div class="carousel-home">
<div class="sliderItemWrapper">
<div class="sliderItem">
<div class="contentWrapper">
<h2>
<span class="title"></span>
<span class="subtitle">SERVICE DE COLOCATION</span>
</h2>
<div class="description">De la location d'1U à la baie entière, hébergez votre infrastructure dans un centre de données Tier 4.</div>
<div class="boutonWrapper">
<a class="btn btn-bleu" href="/enterprise-services/#colocation" target="_self">Découvrir</a>
</div>
</div>
<div class="imageWrapper">
<img alt="" src="/content/img/home/slide1.png"/>
</div>
</div>
</div>
<div class="sliderItemWrapper">
<div class="sliderItem">
<div class="contentWrapper">
<h2>
<span class="title"></span>
<span class="subtitle">INFOGERANCE</span>
</h2>
<div class="description">Faites confiance à Aqua Ray pour la gestion de vos serveurs Linux et logiciels libres.</div>
<div class="bandeau">À partir de 39,95€</div>
<div class="boutonWrapper">
<a class="btn btn-blanc" href="/enterprise-services" target="_self">Voir nos offres</a>
</div>
</div>
<div class="imageWrapper">
<img alt="" src="/content/img/home/slide5.png"/>
</div>
</div>
</div>
<div class="sliderItemWrapper">
<div class="sliderItem">
<div class="contentWrapper">
<h2>
<span class="title"></span>
<span class="subtitle">HOSTING READY</span>
</h2>
<div class="description">Découvrez nos offres de VPS, serveurs dédiés, hébergement Web et noms de domaine.</div>
<div class="bandeau">Hébergement 100% français</div>
<div class="boutonWrapper">
<a class="btn btn-bleu" href="/hosting-ready" target="_self">Découvrir</a>
<div class="aPartir">
<div>Hébergement Web à partir de</div>
<div>
                                        12 € HT<span>/an</span>
</div>
</div>
</div>
</div>
<div class="imageWrapper">
<img alt="" src="/content/img/home/slide0.png"/>
</div>
</div>
</div>
<div class="sliderItemWrapper">
<div class="sliderItem">
<div class="contentWrapper">
<h2>
<span class="title"></span>
<span class="subtitle">ENTERPRISE SERVICES</span>
</h2>
<div class="description">Faites appel à Aqua Ray pour la gestion et la maintenance de votre Cloud sur mesure.</div>
<div class="boutonWrapper">
<a class="btn btn-blanc" href="/enterprise-services" target="_self">Voir nos offres</a>
</div>
</div>
<div class="imageWrapper">
<img alt="" src="/content/img/home/slide2.png"/>
</div>
</div>
</div>
<div class="sliderItemWrapper">
<div class="sliderItem">
<div class="contentWrapper">
<h2>
<span class="title"></span>
<span class="subtitle">LE LAB'</span>
</h2>
<div class="description">Suivez nos derniers projets et innovations.</div>
<div class="boutonWrapper">
<a class="btn btn-bleu" href="/le-lab" target="_self">En savoir plus</a>
</div>
</div>
<div class="imageWrapper">
<img alt="" src="/content/img/home/slide3.png"/>
</div>
</div>
</div>
</div>
<div class="controlsWrapper">
<li class="control active" data-color="#2b75b2" data-slide-to="0"></li>
<li class="control " data-color="#2b75b2" data-slide-to="1"></li>
<li class="control " data-color="#2b75b2" data-slide-to="2"></li>
<li class="control " data-color="#2b75b2" data-slide-to="3"></li>
<li class="control " data-color="#2b75b2" data-slide-to="4"></li>
</div>
<div class="subSlider"></div>
</div>
<script type="template">
<div class="sliderWrapper carousel-home" style="background-color: #2b75b2">
    <div class="grid-container">
                    <div class="item col-1 grid-spaceAround-noGutter no-padding">
                <div class="contentWrapper flex-column flex-justify-space-between flex-align-start">
                    <div>
                        <div class="title"></div>
                        <div class="subtitle">SERVICE DE COLOCATION</div>
                    </div>
                    <div class="description">De la location d'1U à la baie entière, hébergez votre infrastructure dans un centre de données Tier 4.</div>
                    <a href="/enterprise-services/#colocation" class="btn btn-blanc">Découvrir</a>
                </div>
                <div class="imageWrapper flex-center">
                    <img src="/content/img/home/slide1.png" alt="">
                </div>
            </div>
                    <div class="item col-1 grid-spaceAround-noGutter no-padding">
                <div class="contentWrapper flex-column flex-justify-space-between flex-align-start">
                    <div>
                        <div class="title"></div>
                        <div class="subtitle">INFOGERANCE</div>
                    </div>
                    <div class="description">Faites confiance à Aqua Ray pour la gestion de vos serveurs Linux et logiciels libres.</div>
                    <a href="/enterprise-services" class="btn btn-blanc">Voir nos offres</a>
                </div>
                <div class="imageWrapper flex-center">
                    <img src="/content/img/home/slide5.png" alt="">
                </div>
            </div>
                    <div class="item col-1 grid-spaceAround-noGutter no-padding">
                <div class="contentWrapper flex-column flex-justify-space-between flex-align-start">
                    <div>
                        <div class="title"></div>
                        <div class="subtitle">HOSTING READY</div>
                    </div>
                    <div class="description">Découvrez nos offres de VPS, serveurs dédiés, hébergement Web et noms de domaine.</div>
                    <a href="/hosting-ready" class="btn btn-blanc">Découvrir</a>
                </div>
                <div class="imageWrapper flex-center">
                    <img src="/content/img/home/slide0.png" alt="">
                </div>
            </div>
                    <div class="item col-1 grid-spaceAround-noGutter no-padding">
                <div class="contentWrapper flex-column flex-justify-space-between flex-align-start">
                    <div>
                        <div class="title"></div>
                        <div class="subtitle">ENTERPRISE SERVICES</div>
                    </div>
                    <div class="description">Faites appel à Aqua Ray pour la gestion et la maintenance de votre Cloud sur mesure.</div>
                    <a href="/enterprise-services" class="btn btn-blanc">Voir nos offres</a>
                </div>
                <div class="imageWrapper flex-center">
                    <img src="/content/img/home/slide2.png" alt="">
                </div>
            </div>
                    <div class="item col-1 grid-spaceAround-noGutter no-padding">
                <div class="contentWrapper flex-column flex-justify-space-between flex-align-start">
                    <div>
                        <div class="title"></div>
                        <div class="subtitle">LE LAB&#039;</div>
                    </div>
                    <div class="description">Suivez nos derniers projets et innovations.</div>
                    <a href="/le-lab" class="btn btn-blanc">En savoir plus</a>
                </div>
                <div class="imageWrapper flex-center">
                    <img src="/content/img/home/slide3.png" alt="">
                </div>
            </div>
            </div>
    <div class="grid-container grid-center-middle controlsWrapper">
                    <li data-slide-to="0" data-color="#2b75b2" class="control active"></li>
                    <li data-slide-to="1" data-color="#2b75b2" class="control "></li>
                    <li data-slide-to="2" data-color="#2b75b2" class="control "></li>
                    <li data-slide-to="3" data-color="#2b75b2" class="control "></li>
                    <li data-slide-to="4" data-color="#2b75b2" class="control "></li>
                                            </div>
    <div class="grid-container grid-center-middle subSlider"></div>
</div>
</script>
<div class="presentationWrapper">
<div class="grid-container">
<div class="grid-noGutter-equalHeight">
<div class="col-4_xs-12_sm-12 centerItem">
<div class="presentation child-1">
<div class="imageWrapper">
<img alt="Hosting Ready" src="/content/img/home/presentation1.png"/>
</div>
<div class="title bleu">Hosting Ready</div>
<div class="description">
                            Gamme de serveurs et services d'hébergement Web en libre accès. Aqua Ray vous propose un large choix de serveurs VPS, de serveurs dédiés, de domaines, de services d'e-mails et d'hébergement Web mutualisé.
                        </div>
<a class="btn btn-bleu" href="/hosting-ready" target="_self">Voir nos offres</a>
</div>
</div>
<div class="col-4_xs-12_sm-12 ">
<div class="presentation child-2">
<div class="imageWrapper">
<img alt="Enterprise Services" src="/content/img/home/presentation2.png"/>
</div>
<div class="title orange">Enterprise Services</div>
<div class="description">
                            Maintenance et gestion de Cloud Privé, Cloud Public et Cloud AWS. Aqua Ray délivre un service d'infogérance spécialisé sur les OS Linux.
                        </div>
<a class="btn btn-orange" href="/enterprise-services" target="_self">Voir nos offres</a>
</div>
</div>
<div class="col-4_xs-12_sm-12 centerItem">
<div class="presentation child-3">
<div class="imageWrapper">
<img alt="Le Lab'" src="/content/img/home/presentation3.png"/>
</div>
<div class="title bleu">Le Lab'</div>
<div class="description">
                            Présentation de nos derniers projets en cours de développement. Découvrez nos derniers chantiers et les projets que nous soutenons.
                        </div>
<a class="btn btn-bleu" href="/le-lab" target="_self">À Découvrir</a>
</div>
</div>
</div>
</div>
</div> <div class="useCasesWrapper">
<div class="useCasesTitle grid-container grid-center-middle">
        Cas d'usages
    </div>
<div class="grid-container grid-3_xs-1_sm-1_md-2">
<div class="col useCase flex-column flex-justify-center flex-align-center">
<div class="title title_bleu">Hébergement mutualisé</div>
<div class="imageWrapper">
<img alt="Hébergement mutualisé" src="/content/img/home/logo_Virginie_Schaeffer.jpeg"/>
</div>
<div class="description">
                    Virginie Schaeffer fait appel à Aqua Ray pour l'enregistrement de son nom de domaine, la gestion de ses emails professionnels et l'hébergement de son site Internet. La cliente bénéficie de services FTP, MySQL et PHP adaptés à ses besoins.
                </div>
<a class="btn btn-bleu" href="/blog/articles/hebergement-mutualise-virginie-schaeffer" target="_blank">En savoir plus</a>
</div>
<div class="col useCase flex-column flex-justify-center flex-align-center">
<div class="title title_bleu">Cloud privé infogéré</div>
<div class="imageWrapper">
<img alt="Cloud privé infogéré" src="/content/img/home/logo_recia.png"/>
</div>
<div class="description">
                    Depuis 2009 Aqua Ray maintient l'infrastructure ENT du GIP RECIA. Elle est basée sur de nombreuses ressources dédiées et bénéficie de GTR pouvant descendre à 2h en 24h/7j.
                </div>
<a class="btn btn-bleu" href="/blog/articles/cloud-prive-infogere-region-centre-recia" target="_blank">En savoir plus</a>
</div>
<div class="col useCase flex-column flex-justify-center flex-align-center">
<div class="title title_bleu">Gestion de Cloud Public</div>
<div class="imageWrapper">
<img alt="Gestion de Cloud Public" src="/content/img/home/logo_indochine.png"/>
</div>
<div class="description">
                    Le groupe de musique a adopté le Cloud basé sur nos offres de VPS Linux avec SSD et maintenu par notre équipe d'ingénieurs basée à Ivry-sur-Seine (94). Le service d'infogérance d'Aqua Ray leur permet d'avoir accès à notre ligne d'urgence et nos GTR 4h en 24h/7j.
                </div>
<a class="btn btn-bleu" href="/blog/articles/cloud-public-infogere-indochine" target="_blank">En savoir plus</a>
</div>
</div>
</div>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement":
        [
            {
                "@type": "ListItem",
                "position": 1,
                "item":
                {
                    "@id": "https://www.aquaray.com/",
                    "url": "https://www.aquaray.com/",
                    "name": "Accueil"
                }
            }
        ]
    }
    </script>
<div class="helpWrapper">
<div class="grid-container grid-column-center-middle height-100p">
<div class="description">
            Besoin d'aide ? Appelez-nous maintenant !
        </div>
<div class="number">
<div class="flex-center hideOnNotDesktop">
<img alt="Appelez-nous maintenant&amp;nbsp;!" src="/media/img/layout/phone2.png"/>
<span>01 84 04 04 05</span>
</div>
<a class="flex-center hideOnDesktop" href="tel:+33184040405">
<img alt="Appelez-nous maintenant&amp;nbsp;!" src="/media/img/layout/phone2.png"/>
<span>01 84 04 04 05</span>
</a>
</div>
</div>
</div>
<div class="socialNetworksWrapper">
<div class="grid-container grid-2_xs-1_sm-1_md-2-noBottom">
<div class="col flex-column flex-center">
<a class="title twitter" href="https://twitter.com/aquaray_fr" target="_blank"><i class="fa fa-twitter"></i> aquaray_fr</a>
<div class="twitterFeedWrapper flex-1 flex flex-justify-center">
<div class="flex flex-justify-center" id="twitter-container"></div>
</div>
</div>
<div class="col flex-column flex-center">
<a class="title instagram" href="https://www.instagram.com/aquaray_france" target="_blank"><i class="fa fa-instagram"></i> aquaray_france</a>
<div class="instagramFeedWrapper flex-1">
<div class="grid-3-noGutter">
<a class="col padding-10" href="https://www.instagram.com/aquaray_france" target="_blank">
<img alt="Instagram picture" src="/media/img/layout/instagram_placeholder.jpg"/>
</a>
<a class="col padding-10" href="https://www.instagram.com/aquaray_france" target="_blank">
<img alt="Instagram picture" src="/media/img/layout/instagram_placeholder.jpg"/>
</a>
<a class="col padding-10" href="https://www.instagram.com/aquaray_france" target="_blank">
<img alt="Instagram picture" src="/media/img/layout/instagram_placeholder.jpg"/>
</a>
</div>
</div>
</div>
</div>
</div>
<div class="subFooterWrapper hideOnNotDesktop">
<div class="grid-container height-100p">
<div class="footerAws">
<img alt="AWS Certified" class="aws" src="/media/img/layout/aws.png"/>
</div>
<div class="footerFW500">
<img alt="FW 500" class="fw500" src="/media/img/layout/fw500.png"/>
</div>
<div class="footerSocial">
<a class="socialItem linkedin" href="https://www.linkedin.com/company/aqua-ray-sarl" target="_blank">
<i class="fa fa-fw fa-linkedin"></i>
</a>
<a class="socialItem facebook" href="https://www.facebook.com/aquarayfrance/" target="_blank">
<i class="fa fa-fw fa-facebook"></i>
</a>
<a class="socialItem twitter" href="https://twitter.com/aquaray_fr" target="_blank">
<i class="fa fa-fw fa-twitter"></i>
</a>
<a class="socialItem instagram" href="https://www.instagram.com/aquaray_france/" target="_blank">
<i class="fa fa-fw fa-instagram"></i>
</a>
</div>
<div class="flex-1"></div>
<a class="subFooterLink" href="/pages/conditions-de-vente">Conditions de vente</a>
<a class="subFooterLink" href="/pages/plan-du-site">Plan du site</a>
<a class="subFooterLink" href="/pages/mentions-legales">Mentions légales</a>
<a class="subFooterLink" href="/pages/donnees-personnelles">Données personnelles</a>
<a class="subFooterLink" href="/contactez-nous/">Contact</a>
</div>
</div>
<div class="subFooterWrapper hideOnDesktop">
<div class="footerAws">
<img alt="AWS Certified" class="aws" src="/media/img/layout/aws.png"/>
</div>
<div class="footerFW500">
<img alt="FW 500" class="fw500" src="/media/img/layout/fw500.png"/>
</div>
<div class="footerSocial">
<a class="socialItem linkedin" href="https://www.linkedin.com/company/aqua-ray-sarl" target="_blank">
<i class="fa fa-fw fa-linkedin"></i>
</a>
<a class="socialItem facebook" href="https://www.facebook.com/aquarayfrance/" target="_blank">
<i class="fa fa-fw fa-facebook"></i>
</a>
<a class="socialItem twitter" href="https://twitter.com/aquaray_fr" target="_blank">
<i class="fa fa-fw fa-twitter"></i>
</a>
<a class="socialItem instagram" href="https://www.instagram.com/aquaray_france/" target="_blank">
<i class="fa fa-fw fa-instagram"></i>
</a>
</div>
<div class="footerLiens">
<a class="subFooterLink" href="/pages/conditions-de-vente">Conditions de vente</a>
<a class="subFooterLink" href="/pages/plan-du-site">Plan du site</a>
<a class="subFooterLink" href="/pages/mentions-legales">Mentions légales</a>
<a class="subFooterLink" href="/pages/donnees-personnelles">Données personnelles</a>
<a class="subFooterLink" href="/contactez-nous/">Contact</a>
</div>
</div>
</main>
<!-- jQuery -->
<script crossorigin="anonymous" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<!-- Fos JS Routing -->
<script src="/bundles/fosjsrouting/js/router.js"></script>
<script src="/js/routing?callback=fos.Router.setData"></script>
<!-- Latest Bootstrap JS -->
<script crossorigin="anonymous" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Slick carousel -->
<script src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js" type="text/javascript"></script>
<!-- Sweet Alert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
<!-- Selectric -->
<script src="/libs/selectric/selectric.min.js" type="text/javascript"></script>
<!-- Twitter widget -->
<script charset="utf-8" src="//platform.twitter.com/widgets.js"></script>
<!-- jQuery Dot Dot Dot -->
<script src="/libs/jquery.dotdotdot.js"></script>
<!-- Slideout (menu mobile) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/slideout/1.0.1/slideout.min.js"></script>
<!-- Mustache -->
<script src="/libs/mustache.min.js" type="text/javascript"></script>
<script type="text/javascript">locale = 'fr';</script>
<script src="/media/js/layout.js?20181205002" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js" type="text/javascript"></script>
<script type="text/javascript">
        $(document).ready(function() {
            window.cookieconsent.initialise({
                container: document.getElementById("content"),
                palette:{
                    popup: {background: "#2396cc"},
                    button: {background: "#fff", text: "#111"},
                    highlight: {background: "transparent", text: "#111"},
                },
                revokable: false,
                law: {
                    regionalLaw: false
                },
                location: true,
                position: "bottom",
                content: {
                    message: "En poursuivant votre navigation sur ce site, vous acceptez l&#039;utilisation de cookies.",
                    dismiss: "OK",
                    link: "En savoir plus",
                    url: "https://cookiesandyou.com/"
                }
            });
        });
    </script>
<script type="text/javascript"> _linkedin_partner_id = "613505"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img alt="" height="1" src="https://dc.ads.linkedin.com/collect/?pid=613505&amp;fmt=gif" style="display:none;" width="1"/> </noscript>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Organization",
        "name": "Aqua Ray",
        "url": "https://www.aquaray.com",
        "logo": "https://www.aquaray.com/media/img/layout/logo.png",
        "contactPoint": [
            {
                "@type": "ContactPoint",
                "telephone": "+33-01-84-04-04-05",
                "contactType": "customer support"
            }
        ]
    }
    </script>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "url": "https://www.aquaray.com/",
        "potentialAction": {
        "@type": "SearchAction",
            "target": "https://www.aquaray.com/blog?q={search_term_string}",
            "query-input": "required name=search_term_string"
        }
    }
    </script>
<script type="text/javascript">
        setTimeout(function() {
            $('.sliderWrapper').css({opacity: '1'});
        }, 0);
        $(document).ready(function(){
            $('.carousel-home').slick({
                autoplay: true,
                // cssEase: 'ease-in-out',
                autoPlaySpeed: 5000,
                arrows: false,
                // centerMode: true,
                // variableWidth: true,
                // fade: true,
                dots: false,
                draggable: true,
                infinite: true,
                speed: 800,
                slidesToShow: 1,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 769,
                        settings: {
                            adaptiveHeight: true,
                            speed: 400
                        }
                    }
                ]
            });

            $(document).on('click', '.sliderWrapper .control', function() {
                var index = $(this).data('slide-to');
                $('.carousel-home').slick('slickGoTo', index);
            });

            $('.carousel-home').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                var currentControl = $('.sliderWrapper .control[data-slide-to="' + currentSlide + '"]');
                var newControl = $('.sliderWrapper .control[data-slide-to="' + nextSlide + '"]');

                currentControl.removeClass('active');
                newControl.addClass('active');

                var backgroundColor = newControl.data('color');
                $('.sliderWrapper').css({backgroundColor: backgroundColor});
            });
        });
    </script>
<!-- PureChat -->
<script data-cfasync="false" type="text/javascript">window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: 'b06c5906-4587-44f6-8d35-4c870294c81c', f: true }); done = true; } }; })();</script>
</body>
</html>