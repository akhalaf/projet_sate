<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0" name="viewport"/>
<meta content="This URL shortener is private and internal-use only." name="description"/>
<meta content="shortly" name="keywords"/>
<!-- Open Graph Tags -->
<meta content="website" property="og:type"/>
<meta content="https://shortly.im" property="og:url"/>
<meta content="Private URL Shortener - Shortly - A Premium URL shortener" property="og:title"/>
<meta content="This URL shortener is private and internal-use only." property="og:description"/>
<meta content="http://s.wordpress.com/mshots/v1/https://shortly.im?w=800" property="og:image"/>
<title>Private URL Shortener - Shortly - A Premium URL shortener</title>
<!-- Bootstrap core CSS -->
<link href="https://shortly.im/static/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Component CSS -->
<link href="https://shortly.im/themes/default/style.css" rel="stylesheet" type="text/css"/>
<link href="https://shortly.im/static/css/components.min.css" rel="stylesheet" type="text/css"/>
<!-- Required Javascript Files -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js?v=2.0.3" type="text/javascript"></script>
<script src="https://shortly.im/static/bootstrap.min.js" type="text/javascript"></script>
<script src="https://shortly.im/static/js/zclip.js" type="text/javascript"></script>
<script src="https://shortly.im/static/application.fn.js?v=1.0" type="text/javascript"></script>
<script src="https://shortly.im/static/application.js?v=1.0" type="text/javascript"></script>
<script type="text/javascript">
      var appurl="https://shortly.im";
      var token="385d4f0be1dc5c683277c103f96e0dc6";
    </script>
<script src="//cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js?v=1.1.0" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.1/icheck.min.js?v=1.0.1" type="text/javascript"></script>
<script type="text/javascript">(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');if(!ga.getByName) ga.create = undefined;ga('create', 'UA-67309207-1','auto');ga('send', 'pageview');</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pace/0.4.17/pace.js?v=0.4.17" type="text/javascript"></script>
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="dark" id="body">
<a href="#body" id="back-to-top"><i class="glyphicon glyphicon-arrow-up"></i></a>
<header>
<div class="navbar" role="navigation">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
<span class="glyphicon glyphicon-align-justify"></span>
</button>
<a class="navbar-brand" href="https://shortly.im">
<img alt="Shortly - A Premium URL shortener" src="https://shortly.im/content/auto_site_logo.png"/>
</a>
</div>
<div class="navbar-collapse collapse">
<div class="navbar-collapse collapse"><ul class="nav navbar-nav navbar-right"><li><a href="https://shortly.im/user/login">Login</a></li></ul></div> </div>
</div>
</div>
</header>
<section>
<div class="container">
<div class="centered is404">
<h1>Hello</h1>
<h3>This service is meant to be private.</h3><h4>Do you want to buy this website? Please contact support@shortly.im</h4> </div>
</div>
</section> <footer class="main">
<div class="container">
<div class="row">
<div class="col-md-5">
              2021 © Shortly - A Premium URL shortener.
            </div>
<div class="col-md-7 text-right">
<a href="https://shortly.im/page/terms" title="Terms of Service">Terms of Service</a>
<a href="https://shortly.im/page/privacy" title="Privacy Policy">Privacy Policy</a>
<a href="https://shortly.im/contact" title="Contact">Contact</a>
<div class="languages">
<a class="active" href="#lang" id="show-language"><i class="glyphicon glyphicon-globe"></i> Language</a>
<div class="langs">
<a href="?lang=en">English</a><a href="?lang=ru">PÑÑÑÐºÐ¸Ð¹</a><a href="?lang=de">Deutsch</a><a href="?lang=es">EspaÃ±ol</a><a href="?lang=po">PortuguÃªs</a><a href="?lang=th">Thai</a><a href="?lang=fr">Francais</a> </div>
</div>
</div>
</div>
</div>
</footer>
<script type="text/javascript">
        var lang={"del":"Delete","url":"URL","count":"Country","copied":"Copied","geo":"Geotargeting data for","error":"Please enter a valid URL.","success":"URL has been successfully shortened. Click Copy or CRTL+C to Copy it.","stats":"You can access the statistic page via this link","copy":"Copied to clipboard.","from":"Redirect from","to":"Redirect to","share":"Share this on","congrats":"Congratulation! Your URL has been successfully shortened. You can share it on Facebook or Twitter by clicking the links below.","qr":"Copy QR Code"};
  </script>
<script src="https://shortly.im/static/server.js?v=1.0" type="text/javascript"></script>
</body>
</html>