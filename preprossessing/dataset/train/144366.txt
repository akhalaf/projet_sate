<!DOCTYPE html>
<html>
<head>
<title></title>
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<meta content="no-cache, no-store, must-revalidate" http-equiv="Cache-Control"/>
<meta content="no-cache" http-equiv="Pragma"/>
<meta content="0" http-equiv="Expires"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="yes" name="mobile-web-app-capable"/>
<script type="text/javascript">
			var builder = '';
			var path = "external/script/roth-lib-js-cache.js";
			builder += "<script ";
			builder += "src" + "=\"" +path + "?key" + "=" + "k" + (new Date().getTime()) + "\" ";
			builder += "><\/script>";
			document.write(builder);
		</script>
<meta content="black" name="apple-mobile-web-app-status-bar-style"/>
<meta content="image/mstile-144x144.png" name="msapplication-TileImage"/>
<meta content="#42a0b5" name="msapplication-TileColor"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,100,700" rel="stylesheet" type="text/css"/>
<script>loadScript("external/script/roth-lib-js-env.js");</script>
<script>loadScript("script/env.js");</script>
<script>loadDependencies("index");</script>
<script>checkSecure();</script>
<script>web.init();</script>
<script>readMobileParams();</script>
<script>
			document.addEventListener("DOMContentLoaded", function()
			{
				FastClick.attach(document.body);
				var host = window.location.host.toLowerCase();
				var hash = window.location.hash.toLowerCase();
				var showEnv = true;
				var env = getEnvironment();
				switch(env)
				{
					case Environment.PROD:
					{
						showEnv = false;
						break;
					}
				}
				if(isTrue(showEnv))
				{
					$("#env").show().html(env.toUpperCase());
				}
				if(host.match(/propaydocs.aptx.cm/) || host.match(/docs.epay.cm/))
				{
					$("title").text("Propay");
					$("<link href=\"image/favicon-propay.ico\" rel=\"shortcut icon\">").prependTo($("head"));
				}
				else if(host.match(/vantivisv.com/))
				{
					$("title").text("Vantiv");
					$("<link href=\"image/favicon-vantiv.ico\" rel=\"shortcut icon\">").prependTo($("head"));
				}
				else if(host.match(/aptx.cm/) || isDev())
				{
					$("title").text("Aptexx");
					$("<link href=\"image/favicon.ico\" rel=\"shortcut icon\"><link href=\"image/favicon-16x16.png\" rel=\"icon\" sizes=\"16x16\"><link href=\"image/favicon-32x32.png\" rel=\"icon\" sizes=\"32x32\"><link href=\"image/favicon-96x96.png\" rel=\"icon\" sizes=\"96x96\">").prependTo($("head"));
				}
				else
				{
					$("title").text("Payments");
					$("<link href=\"image/favicon-pay.ico\" rel=\"shortcut icon\">").prependTo($("head"));
				}
				if(host.match(/lenderpayments.com/))
				{
					$("<link rel=\"apple-touch-icon\" href=\"image/apple-touch-icon.png?default\"><meta name=\"apple-mobile-web-app-title\" content=\"Payments\">").prependTo($("head"));
				}
				else if(hash.match(/new-package/))
				{
					$("<link rel=\"apple-touch-icon\" href=\"image/apple-touch-icon-packages.png?package\"><meta name=\"apple-mobile-web-app-title\" content=\"New Package\">").prependTo($("head"));
				}
				else if(hash.match(/dashboard/))
				{
					$("<link rel=\"apple-touch-icon\" href=\"image/apple-touch-icon.png?dashboard\"><meta name=\"apple-mobile-web-app-title\" content=\"Dashboard\">").prependTo($("head"));
				}
				else if(hash.match(/portal/))
				{
					$("<link rel=\"apple-touch-icon\" href=\"image/apple-touch-icon.png?portal\"><meta name=\"apple-mobile-web-app-title\" content=\"My Portal\">").prependTo($("head"));
				}
				else if(hash.match(/application\/terms/))
				{
					$("<link rel=\"apple-touch-icon\" href=\"image/apple-touch-icon-application.png?application\"><meta name=\"apple-mobile-web-app-title\" content=\"New Application\">").prependTo($("head"));
				}
				else
				{
					$("<link rel=\"apple-touch-icon\" href=\"image/apple-touch-icon.png?default\"><meta name=\"apple-mobile-web-app-title\" content=\"Aptexx\">").prependTo($("head"));
				}
			},
			false);
		</script>
<!-- Start of aptexx Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=4479d201-c69c-4adb-ba3f-902fd0be09ea"></script>
<!-- End aptexx Zendesk Widget script -->
</head>
<body>
<div class="hgroup--env" id="env" style="display:none;"></div>
<div class="wrapper" id="layout"></div>
</body>
</html>