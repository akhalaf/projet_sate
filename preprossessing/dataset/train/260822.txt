<!DOCTYPE HTML>
<html xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://ogp.me/ns#">
<head>
<title>User Domain Auction:brokendreamsthemovie.com | Dynadot</title><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><meta content="IE=edge" http-equiv="X-UA-Compatible"/><meta content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0" name="viewport"/><link href="/tr/1610433498303bootstrap.min.css" media="all" rel="stylesheet" type="text/css"/><link href="/tr/1610433498301main.css" media="all" rel="stylesheet" type="text/css"/><link href="/tr/1610433498302responsive.css" media="all" rel="stylesheet" type="text/css"/><link href="/fontawesome/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/><link href="/open-sans/css/fonts.css" media="all" rel="stylesheet" type="text/css"/><link href="https://use.typekit.net/eyi5ire.css" media="all" rel="stylesheet" type="text/css"/><link href="/tr/1610433498308cropper.css" media="all" rel="stylesheet" type="text/css"/><style type="text/css"><!--
HTML {height: 100%;}body {background-color: #f9f9fb; direction: ltr; padding: 0 !important; margin: 0 !important; height: 100%; font-family: 'Open Sans', "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 13px; font-style: normal; font-weight: normal; line-height: 1.5;}p {line-height: 1.8; font-size: 16px; font-weight: 300;}button,input,textarea {outline:none}.left {float: left;}.right {float: right;}.hidden {display: none !important;}i.fa-1 {font-size: 16px;}i.fa-2 {font-size: 22px;}a.navbar-logo {display: block; height: 0; width: 110px; padding: 8px 0; float: left;}button.btn {outline: none;}.navbar-toggle-menu {width: 58px; height: 50px; margin-left: -15px; color: #fff; padding: 15px 19px; float: left; z-index: 1032;}.navbar-avatar {width: 32px; height: 32px; border-radius: 3px;}.navbar-block i.fa {border-radius: 3px; width: 32px; height: 32px; color: #FFF; text-align: center; padding-top: 7px;}.navbar-msg-unread {background-color: #F95750;}.navbar-msg-default {background-color: #515b6f;}.footer-container a {color: #ccc;}.footer-container a:hover {color: #ff3364;}.page-content {background-color: #fff; height: 100%; padding: 0; margin-top: 50px;}.page-bottom ul.page-bottom-menu-dropdown {margin: 0; padding: 0; list-style-type: none; overflow: hidden; height: auto;}.btn.btn-4bbeec {background-color: #4BBEEC; border-color: #4BBEEC; color: #fff;outline: none;}.btn.btn-4bbeec:hover {background-color: #00AEF9; border-color: #00AEF9; color: #fff;outline: none;}.btn.btn-18c485 {background-color: #18c485;border: 1px solid #18c485;color: #fff;outline: none;}.btn.btn-18c485:hover {background-color: #00ce89;border: 1px solid #00ce89;}--></style>
<script src="/tr/1610433498283jquery.min.js" type="text/javascript"></script><script src="/tr/1610433498289bootstrap.min.js" type="text/javascript"></script><script type="text/javascript">

 function googleAnalytics(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o);a.type = "text/javascript";a.async=1;a.src=g;document.body.appendChild(a);
  };
 $(function(){ googleAnalytics(window,document,'script','/analytics.js','ga'); ga('create', 'UA-5755187-1', 'auto'); ga('send', 'pageview');});   

</script><script type="text/javascript">var _paq=window._paq||[];_paq.push(["setCustomDimension",1,"visitor"]);_paq.push(["trackPageView"]);_paq.push(["enableLinkTracking"]);$(function(){var u="/";_paq.push(["setTrackerUrl",u+"matomo.php"]);_paq.push(["setSiteId","1"]);var d=document,g=d.createElement("script");g.type="text/javascript";g.async=true;g.defer=true;g.src=u+"matomo.js";document.body.appendChild(g);});</script><style media="all" type="text/css">@import "/sr/1610433486705popup.css";</style>
</head><body><style media="all" type="text/css">@import "/forsale-common.css";</style>
<style media="all" type="text/css">@import "/forsale-auction.css";</style>
<div id="div-body" style='background-image: url("/for-sale-background.png");overflow: auto;'><nav class="navbar-default" role="navigation" style="background: #FFFFFF;"><div class="container-fluid " style="background: #FFFFFF;"><div class="navbar-header pull-left "><a class="navbar-brand" href="/"><img alt="" border="0" src="/logo-dark-blue273x90.png"/></a></div><div class="navbar-right pull-right " style="margin-right: 10px;"><a href="/community/help/" target="_blank"><button class="btn navbar-button"><div id="reqSup">Request Support</div></button>
</a></div></div></nav><div><h1 class="home-title">brokendreamsthemovie.com</h1><div class="home-subtitle">This domain is available on auction!</div><div class="container "><div class="row " style="text-align: center"><div class="col-md-2 "></div><div class="col-xs-12 col-md-8 "><div class="auction-continer"><div class="content-title title">Auction Ends:</div><div class="row "><div class="col-xs-1 col-md-2 "></div><div class="col-xs-10 col-md-8 "><div class="row "><div class="col-xs-3 col-md-3 " style="padding: 0px 0px;"><span id="remaining" style="display:none;">157221737</span><span class="time-num" id="day0">0</span><span class="time-num" id="day1">1</span><div class="time-text">DAYS</div></div><div class="col-xs-3 col-md-3 " style="padding: 0px 0px;"><span class="time-num" id="hour0">1</span><span class="time-num" id="hour1">9</span><div class="time-text">HOURS</div></div><div class="col-xs-3 col-md-3 " style="padding: 0px 0px;"><span class="time-num" id="minute0">4</span><span class="time-num" id="minute1">0</span><div class="time-text">MINUTES</div></div><div class="col-xs-3 col-md-3 " style="padding: 0px 0px;"><span class="time-num" id="second0">2</span><span class="time-num" id="second1">1</span><div class="time-text">SECONDS</div></div></div></div><div class="col-xs-1 col-md-2 "></div></div><div style="margin-top: 40px; margin-bottom: 40px;"><a href="/market/user-auction/brokendreamsthemovie.com"><button class="view-button">View Auction</button>
</a></div></div></div><div class="col-md-2 "></div></div></div><div class="bottom-logo"><img alt="" border="0" src="/for-sale-recommended.png"/></div></div></div><script type="text/javascript"><!--
setInterval(function(){var second = 1000;
    var minute = 60 * second;
    var hour = 60 * minute;
    var day = 24 * hour;
    var remaining = parseInt($("#remaining").text());
    remaining = remaining - second;
    if (remaining <= 0) {
      $("#day0").text(0);
      $("#day1").text(0);
      $("#hour0").text(0);
      $("#hour1").text(0);
      $("#minute0").text(0);
      $("#minute1").text(0);
      $("#second0").text(0);
      $("#second1").text(0);
      return;
    }    $("#remaining").text(remaining);
    var days = parseInt(remaining/day);
    remaining -= days * day;
    var hours = parseInt(remaining/hour);
    remaining -= hours * hour;
    var minutes = parseInt(remaining/minute);
    remaining -= minutes * minute;
    var seconds = parseInt(remaining/second);
    
    if (days >= 10) {
      $("#day0").text(parseInt(days/10));
      $("#day1").text(parseInt(days%10));
    } else {
      $("#day0").text(0);
      $("#day1").text(parseInt(days));
    }
    
    if (hours >= 10) {
      $("#hour0").text(parseInt(hours/10));
      $("#hour1").text(parseInt(hours%10));
    } else {
      $("#hour0").text(0);
      $("#hour1").text(parseInt(hours));
    }
    
    if (minutes >= 10) {
      $("#minute0").text(parseInt(minutes/10));
      $("#minute1").text(parseInt(minutes%10));
    } else {
      $("#minute0").text(0);
      $("#minute1").text(parseInt(minutes));
    }
    
    if (seconds >= 10) {
      $("#second0").text(parseInt(seconds/10));
      $("#second1").text(parseInt(seconds%10));
    } else {
      $("#second0").text(0);
      $("#second1").text(parseInt(seconds));
    }}, 1000);
--></script><script src="/hp_script.js" type="text/javascript"></script></body>
</html>
