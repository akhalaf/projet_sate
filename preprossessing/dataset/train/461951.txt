<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<title>Ed Treatment for you – L’évaluation de l’état des victimes</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.edtreatmentforyou.com/feed/" rel="alternate" title="Ed Treatment for you » Feed" type="application/rss+xml"/>
<link href="https://www.edtreatmentforyou.com/comments/feed/" rel="alternate" title="Ed Treatment for you » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.edtreatmentforyou.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.16"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.edtreatmentforyou.com/wp-content/themes/savona/style.css?ver=4.9.16" id="savona-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.edtreatmentforyou.com/wp-content/themes/savona/assets/css/font-awesome.css?ver=4.9.16" id="fontawesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.edtreatmentforyou.com/wp-content/themes/savona/assets/css/fontello.css?ver=4.9.16" id="fontello-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.edtreatmentforyou.com/wp-content/themes/savona/assets/css/slick.css?ver=4.9.16" id="slick-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.edtreatmentforyou.com/wp-content/themes/savona/assets/css/perfect-scrollbar.css?ver=4.9.16" id="scrollbar-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.edtreatmentforyou.com/wp-content/themes/savona/assets/css/woocommerce.css?ver=4.9.16" id="savona-woocommerce-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.edtreatmentforyou.com/wp-content/themes/savona/assets/css/responsive.css?ver=4.9.16" id="savona-responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Playfair+Display%3A400%2C700&amp;ver=1.0.0" id="savona-playfair-font-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans%3A400italic%2C400%2C600italic%2C600%2C700italic%2C700&amp;ver=1.0.0" id="savona-opensans-font-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.edtreatmentforyou.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.edtreatmentforyou.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.edtreatmentforyou.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.edtreatmentforyou.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.edtreatmentforyou.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.16" name="generator"/>
<link href="https://www.edtreatmentforyou.com/" rel="canonical"/>
<link href="https://www.edtreatmentforyou.com/" rel="shortlink"/>
<link href="https://www.edtreatmentforyou.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.edtreatmentforyou.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://www.edtreatmentforyou.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.edtreatmentforyou.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<style id="savona_dynamic_css">body {background-color: #ffffff;}#top-bar {background-color: #000000;}#top-bar a {color: #ffffff;}#top-bar a:hover,#top-bar li.current-menu-item > a,#top-bar li.current-menu-ancestor > a,#top-bar .sub-menu li.current-menu-item > a,#top-bar .sub-menu li.current-menu-ancestor> a {color: #dddddd;}#top-menu .sub-menu,#top-menu .sub-menu a {background-color: #000000;border-color: rgba(255,255,255, 0.05);}.header-logo a,.site-description {color: #111;}.entry-header {background-color: #ffffff;}#featured-links h4 {background-color: rgba(255,255,255, 0.85);color: #000000;}#main-nav a,#main-nav i,#main-nav #s {color: #000000;}.main-nav-sidebar span,.sidebar-alt-close-btn span {background-color: #000000;}#main-nav a:hover,#main-nav i:hover,#main-nav li.current-menu-item > a,#main-nav li.current-menu-ancestor > a,#main-nav .sub-menu li.current-menu-item > a,#main-nav .sub-menu li.current-menu-ancestor> a {color: #999999;}.main-nav-sidebar:hover span {background-color: #999999;}#main-menu .sub-menu,#main-menu .sub-menu a {background-color: #ffffff;border-color: rgba(0,0,0, 0.05);}#main-nav #s {background-color: #ffffff;}#main-nav #s::-webkit-input-placeholder { /* Chrome/Opera/Safari */color: rgba(0,0,0, 0.7);}#main-nav #s::-moz-placeholder { /* Firefox 19+ */color: rgba(0,0,0, 0.7);}#main-nav #s:-ms-input-placeholder { /* IE 10+ */color: rgba(0,0,0, 0.7);}#main-nav #s:-moz-placeholder { /* Firefox 18- */color: rgba(0,0,0, 0.7);}/* Background */.sidebar-alt,#featured-links,.main-content,#featured-slider,#page-content select,#page-content input,#page-content textarea {background-color: #ffffff;}/* Text */#page-content,#page-content select,#page-content input,#page-content textarea,#page-content .post-author a,#page-content .savona-widget a,#page-content .comment-author {color: #464646;}/* Title */#page-content h1 a,#page-content h1,#page-content h2,#page-content h3,#page-content h4,#page-content h5,#page-content h6,.post-content > p:first-child:first-letter,#page-content .author-description h4 a,#page-content .related-posts h4 a,#page-content .blog-pagination .previous-page a,#page-content .blog-pagination .next-page a,blockquote,#page-content .post-share a {color: #030303;}#page-content h1 a:hover {color: rgba(3,3,3, 0.75);}/* Meta */#page-content .post-date,#page-content .post-comments,#page-content .post-author,#page-content .related-post-date,#page-content .comment-meta a,#page-content .author-share a,#page-content .post-tags a,#page-content .tagcloud a,.widget_categories li,.widget_archive li,.ahse-subscribe-box p,.rpwwt-post-author,.rpwwt-post-categories,.rpwwt-post-date,.rpwwt-post-comments-number {color: #a1a1a1;}#page-content input::-webkit-input-placeholder { /* Chrome/Opera/Safari */color: #a1a1a1;}#page-content input::-moz-placeholder { /* Firefox 19+ */color: #a1a1a1;}#page-content input:-ms-input-placeholder { /* IE 10+ */color: #a1a1a1;}#page-content input:-moz-placeholder { /* Firefox 18- */color: #a1a1a1;}/* Accent */#page-content a,.post-categories {color: #999999;}.ps-container > .ps-scrollbar-y-rail > .ps-scrollbar-y {background: #999999;}#page-content a:hover {color: rgba(153,153,153, 0.8);}blockquote {border-color: #999999;}/* Selection */::-moz-selection {color: #ffffff;background: #999999;}::selection {color: #ffffff;background: #999999;}/* Border */#page-content .post-footer,#page-content .author-description,#page-content .related-posts,#page-content .entry-comments,#page-content .savona-widget li,#page-content #wp-calendar,#page-content #wp-calendar caption,#page-content #wp-calendar tbody td,#page-content .widget_nav_menu li a,#page-content .tagcloud a,#page-content select,#page-content input,#page-content textarea,.widget-title h2:before,.widget-title h2:after,.post-tags a,.gallery-caption,.wp-caption-text,table tr,table th,table td,pre {border-color: #e8e8e8;}hr {background-color: #e8e8e8;}/* Buttons */.widget_search i,.widget_search #searchsubmit,.single-navigation i,#page-content .submit,#page-content .blog-pagination.numeric a,#page-content .blog-pagination.load-more a,#page-content .savona-subscribe-box input[type="submit"],#page-content .widget_wysija input[type="submit"],#page-content .post-password-form input[type="submit"],#page-content .wpcf7 [type="submit"] {color: #ffffff;background-color: #333333;}.single-navigation i:hover,#page-content .submit:hover,#page-content .blog-pagination.numeric a:hover,#page-content .blog-pagination.numeric span,#page-content .blog-pagination.load-more a:hover,#page-content .savona-subscribe-box input[type="submit"]:hover,#page-content .widget_wysija input[type="submit"]:hover,#page-content .post-password-form input[type="submit"]:hover,#page-content .wpcf7 [type="submit"]:hover {color: #ffffff;background-color: #999999;}/* Image Overlay */.image-overlay,#infscr-loading,#page-content h4.image-overlay {color: #ffffff;background-color: rgba(73,73,73, 0.3);}.image-overlay a,.post-slider .prev-arrow,.post-slider .next-arrow,#page-content .image-overlay a,#featured-slider .slick-arrow,#featured-slider .slider-dots {color: #ffffff;}.slide-caption {background: rgba(255,255,255, 0.95);}#featured-slider .slick-active {background: #ffffff;}#page-footer,#page-footer select,#page-footer input,#page-footer textarea {background-color: #f6f6f6;color: #333333;}#page-footer,#page-footer a,#page-footer select,#page-footer input,#page-footer textarea {color: #333333;}#page-footer #s::-webkit-input-placeholder { /* Chrome/Opera/Safari */color: #333333;}#page-footer #s::-moz-placeholder { /* Firefox 19+ */color: #333333;}#page-footer #s:-ms-input-placeholder { /* IE 10+ */color: #333333;}#page-footer #s:-moz-placeholder { /* Firefox 18- */color: #333333;}/* Title */#page-footer h1,#page-footer h2,#page-footer h3,#page-footer h4,#page-footer h5,#page-footer h6 {color: #111111;}#page-footer a:hover {color: #999999;}/* Border */#page-footer a,#page-footer .savona-widget li,#page-footer #wp-calendar,#page-footer #wp-calendar caption,#page-footer #wp-calendar tbody td,#page-footer .widget_nav_menu li a,#page-footer select,#page-footer input,#page-footer textarea,#page-footer .widget-title h2:before,#page-footer .widget-title h2:after,.footer-widgets {border-color: #e0dbdb;}#page-footer hr {background-color: #e0dbdb;}.savona-preloader-wrap {background-color: #333333;}.boxed-wrapper {max-width: 1160px;}.sidebar-alt {width: 340px;left: -340px; padding: 85px 35px 0px;}.sidebar-left,.sidebar-right {width: 307px;}[data-layout*="rsidebar"] .main-container,[data-layout*="lsidebar"] .main-container {width: calc(100% - 307px);width: -webkit-calc(100% - 307px);}[data-layout*="lrsidebar"] .main-container {width: calc(100% - 614px);width: -webkit-calc(100% - 614px);}[data-layout*="fullwidth"] .main-container {width: 100%;}#top-bar > div,#main-nav > div,#featured-slider.boxed-wrapper,#featured-links,.main-content,.page-footer-inner {padding-left: 40px;padding-right: 40px;}#top-menu {float: left;}.top-bar-socials {float: right;}.entry-header {background-image:url();}.logo-img {max-width: 500px;}#main-nav {text-align: center;}.main-nav-sidebar {position: absolute;top: 0px;left: 40px;z-index: 1;}.main-nav-icons {position: absolute;top: 0px;right: 40px;z-index: 2;}#featured-slider.boxed-wrapper {padding-top: 41px;}#featured-links .featured-link {margin-right: 30px;}#featured-links .featured-link:last-of-type {margin-right: 0;}#featured-links .featured-link {width: calc( (100% - -30px) / 0);width: -webkit-calc( (100% - -30px) / 0);}.blog-grid > li {margin-bottom: 30px;}[data-layout*="col2"] .blog-grid > li,[data-layout*="col3"] .blog-grid > li,[data-layout*="col4"] .blog-grid > li {display: inline-block;vertical-align: top;margin-right: 37px;}[data-layout*="col2"] .blog-grid > li:nth-of-type(2n+2),[data-layout*="col3"] .blog-grid > li:nth-of-type(3n+3),[data-layout*="col4"] .blog-grid > li:nth-of-type(4n+4) {margin-right: 0;}[data-layout*="col1"] .blog-grid > li {width: 100%;}[data-layout*="col2"] .blog-grid > li {width: calc((100% - 37px ) /2);width: -webkit-calc((100% - 37px ) /2);}[data-layout*="col3"] .blog-grid > li {width: calc((100% - 2 * 37px ) /3);width: -webkit-calc((100% - 2 * 37px ) /3);}[data-layout*="col4"] .blog-grid > li {width: calc((100% - 3 * 37px ) /4);width: -webkit-calc((100% - 3 * 37px ) /4);}[data-layout*="rsidebar"] .sidebar-right {padding-left: 37px;}[data-layout*="lsidebar"] .sidebar-left {padding-right: 37px;}[data-layout*="lrsidebar"] .sidebar-right {padding-left: 37px;}[data-layout*="lrsidebar"] .sidebar-left {padding-right: 37px;}.footer-widgets > .savona-widget {width: 30%;margin-right: 5%;}.footer-widgets > .savona-widget:nth-child(3n+3) {margin-right: 0;}.footer-widgets > .savona-widget:nth-child(3n+4) {clear: both;}.copyright-info {float: right;}.footer-socials {float: left;}.woocommerce div.product .stock,.woocommerce div.product p.price,.woocommerce div.product span.price,.woocommerce ul.products li.product .price,.woocommerce-Reviews .woocommerce-review__author,.woocommerce form .form-row .required,.woocommerce form .form-row.woocommerce-invalid label,.woocommerce #page-content div.product .woocommerce-tabs ul.tabs li a {color: #464646;}.woocommerce a.remove:hover {color: #464646 !important;}.woocommerce a.remove,.woocommerce .product_meta,#page-content .woocommerce-breadcrumb,#page-content .woocommerce-review-link,#page-content .woocommerce-breadcrumb a,#page-content .woocommerce-MyAccount-navigation-link a,.woocommerce .woocommerce-info:before,.woocommerce #page-content .woocommerce-result-count,.woocommerce-page #page-content .woocommerce-result-count,.woocommerce-Reviews .woocommerce-review__published-date,.woocommerce .product_list_widget .quantity,.woocommerce .widget_products .amount,.woocommerce .widget_price_filter .price_slider_amount,.woocommerce .widget_recently_viewed_products .amount,.woocommerce .widget_top_rated_products .amount,.woocommerce .widget_recent_reviews .reviewer {color: #a1a1a1;}.woocommerce a.remove {color: #a1a1a1 !important;}p.demo_store,.woocommerce-store-notice,.woocommerce span.onsale { background-color: #999999;}.woocommerce .star-rating::before,.woocommerce .star-rating span::before,.woocommerce #page-content ul.products li.product .button,#page-content .woocommerce ul.products li.product .button,#page-content .woocommerce-MyAccount-navigation-link.is-active a,#page-content .woocommerce-MyAccount-navigation-link a:hover { color: #999999;}.woocommerce form.login,.woocommerce form.register,.woocommerce-account fieldset,.woocommerce form.checkout_coupon,.woocommerce .woocommerce-info,.woocommerce .woocommerce-error,.woocommerce .woocommerce-message,.woocommerce .widget_shopping_cart .total,.woocommerce.widget_shopping_cart .total,.woocommerce-Reviews .comment_container,.woocommerce-cart #payment ul.payment_methods,#add_payment_method #payment ul.payment_methods,.woocommerce-checkout #payment ul.payment_methods,.woocommerce div.product .woocommerce-tabs ul.tabs::before,.woocommerce div.product .woocommerce-tabs ul.tabs::after,.woocommerce div.product .woocommerce-tabs ul.tabs li,.woocommerce .woocommerce-MyAccount-navigation-link,.select2-container--default .select2-selection--single {border-color: #e8e8e8;}.woocommerce-cart #payment,#add_payment_method #payment,.woocommerce-checkout #payment,.woocommerce .woocommerce-info,.woocommerce .woocommerce-error,.woocommerce .woocommerce-message,.woocommerce div.product .woocommerce-tabs ul.tabs li {background-color: rgba(232,232,232, 0.3);}.woocommerce-cart #payment div.payment_box::before,#add_payment_method #payment div.payment_box::before,.woocommerce-checkout #payment div.payment_box::before {border-color: rgba(232,232,232, 0.5);}.woocommerce-cart #payment div.payment_box,#add_payment_method #payment div.payment_box,.woocommerce-checkout #payment div.payment_box {background-color: rgba(232,232,232, 0.5);}#page-content .woocommerce input.button,#page-content .woocommerce a.button,#page-content .woocommerce a.button.alt,#page-content .woocommerce button.button.alt,#page-content .woocommerce input.button.alt,#page-content .woocommerce #respond input#submit.alt,.woocommerce #page-content .widget_product_search input[type="submit"],.woocommerce #page-content .woocommerce-message .button,.woocommerce #page-content a.button.alt,.woocommerce #page-content button.button.alt,.woocommerce #page-content #respond input#submit,.woocommerce #page-content .widget_price_filter .button,.woocommerce #page-content .woocommerce-message .button,.woocommerce-page #page-content .woocommerce-message .button,.woocommerce #page-content nav.woocommerce-pagination ul li a,.woocommerce #page-content nav.woocommerce-pagination ul li span {color: #ffffff;background-color: #333333;}#page-content .woocommerce input.button:hover,#page-content .woocommerce a.button:hover,#page-content .woocommerce a.button.alt:hover,#page-content .woocommerce button.button.alt:hover,#page-content .woocommerce input.button.alt:hover,#page-content .woocommerce #respond input#submit.alt:hover,.woocommerce #page-content .woocommerce-message .button:hover,.woocommerce #page-content a.button.alt:hover,.woocommerce #page-content button.button.alt:hover,.woocommerce #page-content #respond input#submit:hover,.woocommerce #page-content .widget_price_filter .button:hover,.woocommerce #page-content .woocommerce-message .button:hover,.woocommerce-page #page-content .woocommerce-message .button:hover,.woocommerce #page-content nav.woocommerce-pagination ul li a:hover,.woocommerce #page-content nav.woocommerce-pagination ul li span.current {color: #ffffff;background-color: #999999;}.woocommerce #page-content nav.woocommerce-pagination ul li a.prev,.woocommerce #page-content nav.woocommerce-pagination ul li a.next {color: #333333;}.woocommerce #page-content nav.woocommerce-pagination ul li a.prev:hover,.woocommerce #page-content nav.woocommerce-pagination ul li a.next:hover {color: #999999;}.woocommerce #page-content nav.woocommerce-pagination ul li a.prev:after,.woocommerce #page-content nav.woocommerce-pagination ul li a.next:after {color: #ffffff;}.woocommerce #page-content nav.woocommerce-pagination ul li a.prev:hover:after,.woocommerce #page-content nav.woocommerce-pagination ul li a.next:hover:after {color: #ffffff;}.cssload-cube{background-color:#ffffff;width:9px;height:9px;position:absolute;margin:auto;animation:cssload-cubemove 2s infinite ease-in-out;-o-animation:cssload-cubemove 2s infinite ease-in-out;-ms-animation:cssload-cubemove 2s infinite ease-in-out;-webkit-animation:cssload-cubemove 2s infinite ease-in-out;-moz-animation:cssload-cubemove 2s infinite ease-in-out}.cssload-cube1{left:13px;top:0;animation-delay:.1s;-o-animation-delay:.1s;-ms-animation-delay:.1s;-webkit-animation-delay:.1s;-moz-animation-delay:.1s}.cssload-cube2{left:25px;top:0;animation-delay:.2s;-o-animation-delay:.2s;-ms-animation-delay:.2s;-webkit-animation-delay:.2s;-moz-animation-delay:.2s}.cssload-cube3{left:38px;top:0;animation-delay:.3s;-o-animation-delay:.3s;-ms-animation-delay:.3s;-webkit-animation-delay:.3s;-moz-animation-delay:.3s}.cssload-cube4{left:0;top:13px;animation-delay:.1s;-o-animation-delay:.1s;-ms-animation-delay:.1s;-webkit-animation-delay:.1s;-moz-animation-delay:.1s}.cssload-cube5{left:13px;top:13px;animation-delay:.2s;-o-animation-delay:.2s;-ms-animation-delay:.2s;-webkit-animation-delay:.2s;-moz-animation-delay:.2s}.cssload-cube6{left:25px;top:13px;animation-delay:.3s;-o-animation-delay:.3s;-ms-animation-delay:.3s;-webkit-animation-delay:.3s;-moz-animation-delay:.3s}.cssload-cube7{left:38px;top:13px;animation-delay:.4s;-o-animation-delay:.4s;-ms-animation-delay:.4s;-webkit-animation-delay:.4s;-moz-animation-delay:.4s}.cssload-cube8{left:0;top:25px;animation-delay:.2s;-o-animation-delay:.2s;-ms-animation-delay:.2s;-webkit-animation-delay:.2s;-moz-animation-delay:.2s}.cssload-cube9{left:13px;top:25px;animation-delay:.3s;-o-animation-delay:.3s;-ms-animation-delay:.3s;-webkit-animation-delay:.3s;-moz-animation-delay:.3s}.cssload-cube10{left:25px;top:25px;animation-delay:.4s;-o-animation-delay:.4s;-ms-animation-delay:.4s;-webkit-animation-delay:.4s;-moz-animation-delay:.4s}.cssload-cube11{left:38px;top:25px;animation-delay:.5s;-o-animation-delay:.5s;-ms-animation-delay:.5s;-webkit-animation-delay:.5s;-moz-animation-delay:.5s}.cssload-cube12{left:0;top:38px;animation-delay:.3s;-o-animation-delay:.3s;-ms-animation-delay:.3s;-webkit-animation-delay:.3s;-moz-animation-delay:.3s}.cssload-cube13{left:13px;top:38px;animation-delay:.4s;-o-animation-delay:.4s;-ms-animation-delay:.4s;-webkit-animation-delay:.4s;-moz-animation-delay:.4s}.cssload-cube14{left:25px;top:38px;animation-delay:.5s;-o-animation-delay:.5s;-ms-animation-delay:.5s;-webkit-animation-delay:.5s;-moz-animation-delay:.5s}.cssload-cube15{left:38px;top:38px;animation-delay:.6s;-o-animation-delay:.6s;-ms-animation-delay:.6s;-webkit-animation-delay:.6s;-moz-animation-delay:.6s}.cssload-spinner{margin:auto;width:49px;height:49px;position:relative}@keyframes cssload-cubemove{35%{transform:scale(0.005)}50%{transform:scale(1.7)}65%{transform:scale(0.005)}}@-o-keyframes cssload-cubemove{35%{-o-transform:scale(0.005)}50%{-o-transform:scale(1.7)}65%{-o-transform:scale(0.005)}}@-ms-keyframes cssload-cubemove{35%{-ms-transform:scale(0.005)}50%{-ms-transform:scale(1.7)}65%{-ms-transform:scale(0.005)}}@-webkit-keyframes cssload-cubemove{35%{-webkit-transform:scale(0.005)}50%{-webkit-transform:scale(1.7)}65%{-webkit-transform:scale(0.005)}}@-moz-keyframes cssload-cubemove{35%{-moz-transform:scale(0.005)}50%{-moz-transform:scale(1.7)}65%{-moz-transform:scale(0.005)}}</style></head>
<body class="home page-template-default page page-id-5">
<!-- Preloader -->
<!-- Page Wrapper -->
<div id="page-wrap">
<!-- Boxed Wrapper -->
<div id="page-header">
<div class="clear-fix" id="top-bar">
<div class="boxed-wrapper">
<nav class="top-menu-container"><ul class="" id="top-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-13" id="menu-item-13"><a href="https://www.edtreatmentforyou.com/">Accueil</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14" id="menu-item-14"><a href="https://www.edtreatmentforyou.com/informations-didentification-personnelle/">Informations d’identification personnelle</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15" id="menu-item-15"><a href="https://www.edtreatmentforyou.com/la-protection-de-la-zone-accidentee/">La protection de la zone accidentée.</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17" id="menu-item-17"><a href="https://www.edtreatmentforyou.com/secourir-les-blesses/">Secourir les blessés</a></li>
</ul></nav>
<div class="top-bar-socials">
</div>
</div>
</div><!-- #top-bar -->
<div class="entry-header">
<div class="cv-outer">
<div class="cv-inner">
<div class="header-logo">
<a href="https://www.edtreatmentforyou.com/">Ed Treatment for you</a>
<br/>
<p class="site-description">L’évaluation de l’état des victimes</p>
</div>
</div>
</div>
</div><div class="clear-fix" data-fixed="1" id="main-nav">
<div class="boxed-wrapper">
<!-- Alt Sidebar Icon -->
<!-- Icons -->
<div class="main-nav-icons">
<div class="main-nav-search">
<i class="fa fa-search"></i>
<i class="fa fa-times"></i>
<form action="https://www.edtreatmentforyou.com/" class="clear-fix" id="searchform" method="get" role="search"><input data-placeholder="Type &amp; hit enter..." id="s" name="s" placeholder="Search..." type="search" value=""/><i class="fa fa-search"></i><input id="searchsubmit" type="submit" value="st"/></form> </div>
</div>
<!-- Menu -->
<span class="mobile-menu-btn">
<i class="fa fa-chevron-down"></i>
</span>
<nav class="main-menu-container"><ul class="" id="main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-13"><a href="https://www.edtreatmentforyou.com/">Accueil</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14"><a href="https://www.edtreatmentforyou.com/informations-didentification-personnelle/">Informations d’identification personnelle</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15"><a href="https://www.edtreatmentforyou.com/la-protection-de-la-zone-accidentee/">La protection de la zone accidentée.</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17"><a href="https://www.edtreatmentforyou.com/secourir-les-blesses/">Secourir les blessés</a></li>
</ul></nav><nav class="mobile-menu-container"><ul class="" id="mobile-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-13"><a href="https://www.edtreatmentforyou.com/">Accueil</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14"><a href="https://www.edtreatmentforyou.com/informations-didentification-personnelle/">Informations d’identification personnelle</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15"><a href="https://www.edtreatmentforyou.com/la-protection-de-la-zone-accidentee/">La protection de la zone accidentée.</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17"><a href="https://www.edtreatmentforyou.com/secourir-les-blesses/">Secourir les blessés</a></li>
</ul></nav>
</div>
</div><!-- #main-nav -->
</div><!-- .boxed-wrapper -->
<!-- Page Content -->
<div id="page-content">
<div class="main-content clear-fix boxed-wrapper" data-layout="col1-rsidebar" data-sidebar-sticky="1">
<!-- Main Container -->
<div class="main-container">
<article class="post-5 page type-page status-publish hentry" id="page-5">
<header class="post-header"><h1 class="page-title">Accueil</h1></header><div class="post-content"><p>Ce site va devoir vous expliquer à propos des premiers soins dont vous devez savoir pour vous sauver au moment des cas d’accident, maladie, blessure…Au départ il vous sera important de savoir que les premiers secours ou soins représentent l’ensemble des méthodes qui pourront apporter de l’aide aux personnes victimes d’un accident, d’une catastrophe, ou encore d’un problème de santé. Une fois au courant de ces techniques, vous serez en mesure d’apporter de l’aide efficaces face à un danger vital et surtout l’objectif est de pouvoir minimiser les conséquences tout en assurant la survie des personnes tout en leur offrant des premiers soins. Ces premiers secours peuvent être donnés par n’importe qui, car ils servent juste pour de mesure de protection jusqu’à l’arrivée de corps médicaux.</p>
<p><img alt="8077018-homme-avec-deux-femmes-jouant-à-la-roulette-au-casino-la-dépendance-à-l-jeu-" class="size-full wp-image-21 aligncenter" height="400" sizes="(max-width: 500px) 100vw, 500px" src="https://www.edtreatmentforyou.com/wp-content/uploads/2018/07/18077018-homme-avec-deux-femmes-jouant-à-la-roulette-au-casino-la-dépendance-à-l-jeu-.jpg" srcset="https://www.edtreatmentforyou.com/wp-content/uploads/2018/07/18077018-homme-avec-deux-femmes-jouant-à-la-roulette-au-casino-la-dépendance-à-l-jeu-.jpg 500w, https://www.edtreatmentforyou.com/wp-content/uploads/2018/07/18077018-homme-avec-deux-femmes-jouant-à-la-roulette-au-casino-la-dépendance-à-l-jeu--300x240.jpg 300w" width="500"/>Ces techniques peuvent s’appliquer dans différents domaines car ils se conçoivent comme méthodes de secourisme et d’aide médicale qu’un initié peut donner à un malade ou un accidenté sans qu’il soit nécessairement un corps médical. Ces techniques peuvent aussi être considérées comme techniques de sauvetage ou de soustraction des personnes en danger.</p>
<p>En passant, nous vous proposons de jetter un coup d’oeil sur <a href="https://www.casinolariviera.net/">la riviera</a> pour vous acquérir des informations sur comment se taper de sous en ligne. Aujourd’hui, les jeux casinos vous remporte plus que vous pouvez vous imaginer et surtout dans un laps de temps. Alors n’hésitez pas et tentez votre chance.</p>
<p>Vous devez aussi savoir que la notion des premiers secours dépend plus des infrastructures d’un pays à un autre. Dans le cas contraire, la seule aide que vous pouvez obtenir n’est sans doute celle de l’intervention des forces de l’ordre ou celle d’un médecin. Mais dans un pays ou les secours publics sont très développés, la victime peut bénéficier d’un traitement auprès d’un expert qui sera proche au moment d’accident ou de l’inconvénient. Il est aussi déconseillé aux personnes non professionnelles de transporter la victime d’un accident à l’hôpital faute de pouvoir aggraver la situation voire même provoquer sa mort.</p>
<p><img alt="casino-innovation" class="alignnone size-full wp-image-22" height="450" sizes="(max-width: 600px) 100vw, 600px" src="https://www.edtreatmentforyou.com/wp-content/uploads/2018/07/casino-innovation.jpg" srcset="https://www.edtreatmentforyou.com/wp-content/uploads/2018/07/casino-innovation.jpg 600w, https://www.edtreatmentforyou.com/wp-content/uploads/2018/07/casino-innovation-300x225.jpg 300w" width="600"/>Un pays bien organisé doit avoir un service de prise en charge des personnes victimes d’accident concernant les premiers soins de base. L’Etat doit pouvoir mettre en place une organisation des soins et de secours permettant cette prise en charge. C’est alors qu’il faudrait que quelqu’un puisse prévenir ce service.Vous ne vous êtes pas trompé de tomber sur ce site, et voila la surprise, accédez sur <a href="https://www.lecasinoenligne.co/">https://www.lecasinoenligne.co/</a> pour vous faire plus d’argent sur la machine à sous, l’accès et l’enregistrement est gratuit.</p>
<p>C’est de là qu’on parlera alors de la chaine des secours qui commence bien par le témoin de l’incident. Pour ce faire, les pages qui suivent expliqueront en détails les gestes à faire dans les différents cas d’incident.</p>
</div>
</article>
</div><!-- .main-container -->
<div class="sidebar-right-wrap">
<aside class="sidebar-right">
<div class="savona-widget widget_search" id="search-2"><form action="https://www.edtreatmentforyou.com/" class="clear-fix" id="searchform" method="get" role="search"><input data-placeholder="Type &amp; hit enter..." id="s" name="s" placeholder="Search..." type="search" value=""/><i class="fa fa-search"></i><input id="searchsubmit" type="submit" value="st"/></form></div><div class="savona-widget widget_calendar" id="calendar-3"><div class="calendar_wrap" id="calendar_wrap"><table id="wp-calendar">
<caption>January 2021</caption>
<thead>
<tr>
<th scope="col" title="Monday">M</th>
<th scope="col" title="Tuesday">T</th>
<th scope="col" title="Wednesday">W</th>
<th scope="col" title="Thursday">T</th>
<th scope="col" title="Friday">F</th>
<th scope="col" title="Saturday">S</th>
<th scope="col" title="Sunday">S</th>
</tr>
</thead>
<tfoot>
<tr>
<td colspan="3" id="prev"><a href="https://www.edtreatmentforyou.com/2018/06/">« Jun</a></td>
<td class="pad"> </td>
<td class="pad" colspan="3" id="next"> </td>
</tr>
</tfoot>
<tbody>
<tr>
<td class="pad" colspan="4"> </td><td>1</td><td>2</td><td>3</td>
</tr>
<tr>
<td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td><td>10</td>
</tr>
<tr>
<td>11</td><td id="today">12</td><td>13</td><td>14</td><td>15</td><td>16</td><td>17</td>
</tr>
<tr>
<td>18</td><td>19</td><td>20</td><td>21</td><td>22</td><td>23</td><td>24</td>
</tr>
<tr>
<td>25</td><td>26</td><td>27</td><td>28</td><td>29</td><td>30</td><td>31</td>
</tr>
</tbody>
</table></div></div><div class="savona-widget widget_meta" id="meta-2"><div class="widget-title"><h2>Meta</h2></div> <ul>
<li><a href="https://www.edtreatmentforyou.com/wp-login.php">Log in</a></li>
<li><a href="https://www.edtreatmentforyou.com/feed/">Entries <abbr title="Really Simple Syndication">RSS</abbr></a></li>
<li><a href="https://www.edtreatmentforyou.com/comments/feed/">Comments <abbr title="Really Simple Syndication">RSS</abbr></a></li>
<li><a href="https://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress.org</a></li> </ul>
</div> </aside>
</div>
</div><!-- #page-content -->
</div><!-- #page-content -->
<!-- Page Footer -->
<footer class="clear-fix" id="page-footer">
<!-- Scroll Top Button -->
<span class="scrolltop">
<i class="fa fa fa-angle-up"></i>
</span>
<div class="page-footer-inner boxed-wrapper">
<!-- Footer Widgets -->
<div class="footer-copyright">
<div class="copyright-info">
</div>
<div class="credit">
					Savona Theme by 					<a href="http://optimathemes.com/">
					Optima Themes					</a>
</div>
</div>
</div><!-- .boxed-wrapper -->
</footer><!-- #page-footer -->
</div><!-- #page-wrap -->
<script src="https://www.edtreatmentforyou.com/wp-content/themes/savona/assets/js/custom-plugins.js?ver=4.9.16" type="text/javascript"></script>
<script src="https://www.edtreatmentforyou.com/wp-content/themes/savona/assets/js/custom-scripts.js?ver=4.9.16" type="text/javascript"></script>
<script src="https://www.edtreatmentforyou.com/wp-includes/js/wp-embed.min.js?ver=4.9.16" type="text/javascript"></script>
</body>
</html>