<html lang="en">
<head>
<title>403 | Forbidden</title>
<meta charset="utf-8"/>
<style>
        body { text-align: center; padding: 150px; }
        h1 { font-size: 50px; }
        body { font: 20px Helvetica, sans-serif; color: #333; }
        article { display: block; text-align: left; width: 650px; margin: 0 auto; }
        p { line-height: 1.5; }
        a { color: #dc8100; text-decoration: none; }
        a:hover { color: #333; text-decoration: none; }
    </style>
</head>
<body>
<article>
<h1>403. Forbidden.</h1>
<div>
<p>Request has been forbidden by the server.</p>
<p>
            If you believe you are getting this message by mistake, please
                <a href="mailto:feedback@allacronyms.com?subject='Forbidden' message by mistake&amp;body=IP: 210.75.253.169">email us</a>
            with your IP address (210.75.253.169).
        </p>
<p>— The All Acronyms Team</p>
</div>
</article>
</body>
</html>
