<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="https://www.altomisimali.com/wp-content/themes/cvall-altomisimali/style.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.altomisimali.com/feed" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="https://www.altomisimali.com/xmlrpc.php" rel="pingback"/>
<link href="https://fonts.googleapis.com/css?family=ABeeZee" rel="stylesheet" type="text/css"/>
<!-- This site is optimized with the Yoast SEO plugin v5.4 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Pagina no encontrada - Cabañas Alto Misimali Pucón</title>
<meta content="noindex,follow" name="robots"/>
<meta content="es_ES" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Pagina no encontrada - Cabañas Alto Misimali Pucón" property="og:title"/>
<meta content="Cabañas Alto Misimali Pucón" property="og:site_name"/>
<script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/www.altomisimali.com\/","name":"Caba\u00f1as Alto Misimali Puc\u00f3n","potentialAction":{"@type":"SearchAction","target":"https:\/\/www.altomisimali.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<!-- / Yoast SEO plugin. -->
<link href="//ajax.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.altomisimali.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.altomisimali.com/wp-includes/css/dist/block-library/style.min.css?ver=5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.altomisimali.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css?ver=1.11.4" id="jquery-ui-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.altomisimali.com/wp-content/plugins/contact-form-7-datepicker/js/jquery-ui-timepicker/jquery-ui-timepicker-addon.min.css?ver=5.3" id="jquery-ui-timepicker-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.altomisimali.com/wp-content/plugins/revslider/public/assets/css/rs6.css?ver=6.1.5" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
#rs-demo-id {}
</style>
<link href="https://www.altomisimali.com/wp-content/plugins/table-sorter/wp-style.css?ver=5.3" id="table-sorter-custom-css-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='vc_lte_ie9-css'  href='https://www.altomisimali.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css?ver=6.0.5' type='text/css' media='screen' />
<![endif]-->
<script src="https://www.altomisimali.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-content/plugins/table-sorter/jquery.tablesorter.min.js?ver=5.3" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-content/plugins/table-sorter/jquery.metadata.js?ver=2.2" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-content/plugins/table-sorter/wp-script.js?ver=2.2" type="text/javascript"></script>
<link href="https://www.altomisimali.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.altomisimali.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.altomisimali.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3" name="generator"/>
<meta content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." name="generator"/>
<meta content="Powered by Slider Revolution 6.1.5 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." name="generator"/>
<script type="text/javascript">function setREVStartSize(t){try{var h,e=document.getElementById(t.c).parentNode.offsetWidth;if(e=0===e||isNaN(e)?window.innerWidth:e,t.tabw=void 0===t.tabw?0:parseInt(t.tabw),t.thumbw=void 0===t.thumbw?0:parseInt(t.thumbw),t.tabh=void 0===t.tabh?0:parseInt(t.tabh),t.thumbh=void 0===t.thumbh?0:parseInt(t.thumbh),t.tabhide=void 0===t.tabhide?0:parseInt(t.tabhide),t.thumbhide=void 0===t.thumbhide?0:parseInt(t.thumbhide),t.mh=void 0===t.mh||""==t.mh||"auto"===t.mh?0:parseInt(t.mh,0),"fullscreen"===t.layout||"fullscreen"===t.l)h=Math.max(t.mh,window.innerHeight);else{for(var i in t.gw=Array.isArray(t.gw)?t.gw:[t.gw],t.rl)void 0!==t.gw[i]&&0!==t.gw[i]||(t.gw[i]=t.gw[i-1]);for(var i in t.gh=void 0===t.el||""===t.el||Array.isArray(t.el)&&0==t.el.length?t.gh:t.el,t.gh=Array.isArray(t.gh)?t.gh:[t.gh],t.rl)void 0!==t.gh[i]&&0!==t.gh[i]||(t.gh[i]=t.gh[i-1]);var r,a=new Array(t.rl.length),n=0;for(var i in t.tabw=t.tabhide>=e?0:t.tabw,t.thumbw=t.thumbhide>=e?0:t.thumbw,t.tabh=t.tabhide>=e?0:t.tabh,t.thumbh=t.thumbhide>=e?0:t.thumbh,t.rl)a[i]=t.rl[i]<window.innerWidth?0:t.rl[i];for(var i in r=a[0],a)r>a[i]&&0<a[i]&&(r=a[i],n=i);var d=e>t.gw[n]+t.tabw+t.thumbw?1:(e-(t.tabw+t.thumbw))/t.gw[n];h=t.gh[n]*d+(t.tabh+t.thumbh)}void 0===window.rs_init_css&&(window.rs_init_css=document.head.appendChild(document.createElement("style"))),document.getElementById(t.c).height=h,window.rs_init_css.innerHTML+="#"+t.c+"_wrapper { height: "+h+"px }"}catch(t){console.log("Failure at Presize of Slider:"+t)}};</script>
<noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>
<body data-rsssl="1">
<div id="marco">
<div id="menu2">
<img src="https://www.altomisimali.com/wp-content/themes/cvall-altomisimali/images/logo.png"/>
<div class="opcion2"><ul class="op2" id="menu-m2"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-16" id="menu-item-16"><a href="https://www.altomisimali.com/">Portada</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19" id="menu-item-19"><a href="https://www.altomisimali.com/casas.html">Cabañas</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23" id="menu-item-23"><a href="https://www.altomisimali.com/descripcion.html">Cabañas</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26" id="menu-item-26"><a href="https://www.altomisimali.com/servicios.html">Servicios</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-39" id="menu-item-39"><a href="https://www.altomisimali.com/ubica.html">Ubicacion</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1034" id="menu-item-1034"><a href="https://www.altomisimali.com/llegar.html">Pucón – Como LLegar</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1043" id="menu-item-1043"><a href="https://www.altomisimali.com/actividades.html">Actividades en Pucón</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-38" id="menu-item-38"><a href="https://www.altomisimali.com/fcasa.html">Galeria</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-42" id="menu-item-42"><a href="https://www.altomisimali.com/reservas.html">Contacto</a></li>
</ul></div> </div>
<div class="fondocolor">
<div id="container">
<br clear="all"/>
<div id="content">
<div class="navigation">
</div>
</div>
</div>
<br clear="all"/>
<div id="footer">
<br/>
<div class="textwidget">INFORMACIONES: <a href="https://www.altomisimali.com/reservas.html" title="info@altomisimali.com">altomisimali@hotmail.com</a> / FonoFax(45) 244 1049 / <a href="https://www.altomisimali.com" rel="noopener noreferrer" target="_blank" title="Cabañas Alto Misimali">www.altomisimali.com</a></div>
</div>
</div>
<br clear="all"/>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.altomisimali.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://www.altomisimali.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.2" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.11.4" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Cerrar","currentText":"Hoy","monthNames":["enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"],"monthNamesShort":["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],"nextText":"Siguiente","prevText":"Anterior","dayNames":["domingo","lunes","martes","mi\u00e9rcoles","jueves","viernes","s\u00e1bado"],"dayNamesShort":["Dom","Lun","Mar","Mi\u00e9","Jue","Vie","S\u00e1b"],"dayNamesMin":["D","L","M","X","J","V","S"],"dateFormat":"dd\/mm\/yy","firstDay":1,"isRTL":false});});
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/i18n/datepicker-es.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-content/plugins/contact-form-7-datepicker/js/jquery-ui-timepicker/jquery-ui-timepicker-addon.min.js?ver=5.3" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-content/plugins/contact-form-7-datepicker/js/jquery-ui-timepicker/i18n/jquery-ui-timepicker-es.js?ver=5.3" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-includes/js/jquery/ui/slider.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-includes/js/jquery/ui/button.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-content/plugins/contact-form-7-datepicker/js/jquery-ui-sliderAccess.js?ver=5.3" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-content/plugins/revslider/public/assets/js/revolution.tools.min.js?ver=6.0" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-content/plugins/revslider/public/assets/js/rs6.min.js?ver=6.1.5" type="text/javascript"></script>
<script src="https://www.altomisimali.com/wp-includes/js/wp-embed.min.js?ver=5.3" type="text/javascript"></script>
</div></body>
</html>