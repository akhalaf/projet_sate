<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8"/>
<meta content="mEhXfPVpl_B3lgrOQkluy0GDTmhbtJxp7wssUMi_hcg" name="google-site-verification"/>
<script src="/javascripts/application.js?1448339454" type="text/javascript"></script>
<script src="/javascripts/jquery.min.js"></script>
<script src="/javascripts/jquery-ui.custom.min.js"></script>
<script src="/javascripts/jquery.tablehover.js"></script>
<script src="/javascripts/jquery.datepicker.js"></script>
<script src="/javascripts/menu.js"></script>
<script src="/javascripts/application.js"></script>
<meta content="authenticity_token" name="csrf-param"/>
<meta content="Sp7HxcNPaGklhXW7e7x8QpeptU1YzdZFzpQVKacGm4I=" name="csrf-token"/>
<meta content="初心者でも簡単登録2PMのアフィリエイト広告" name="description"/>
<meta content="2pm,アフィリエイト,アフィリエイトプログラム,広告" name="keywords"/>
<link href="" rel="icon"/>
<link href="" rel="Shortcut Icon"/>
<title>2PM - Premier Media Pertners</title>
<link href="/stylesheets/common/import.css" media="screen" rel="stylesheet"/>
<!--[if lt IE 9 ]>
<link href="/stylesheets/common/forIE.css" media="screen" rel="stylesheet">
<script src="/javascripts/html5.js"></script>
<script src="/javascripts/jquery.mediaqueries.js"></script>
<![endif]-->
</head>
<body class="home work">
<!-- header -->
<header class="top" id="header">
<div class="top">
<h1><a href="/"><img alt="2PM Premier Media Pertners" height="42" src="/images/common/logo.gif?1448339449" width="130"/></a></h1>
<p class="logo_bg"><img alt="信頼で繋がるアフィリエイト" height="38" src="images/common/chatch.gif" width="450"/></p>
<ul id="newLogin">
<li id="toP"><a href="/partner/login"><img height="12" src="images/common/toP.gif" width="12"/>パートナーログイン</a></li>
<li id="toC"><a href="/client/login"><img height="12" src="images/common/toC.gif" width="12"/>広告主ログイン</a></li>
</ul>
</div>
</header>
<!-- /header -->
<!-- nav -->
<nav id="globalNavigationTop">
<div class="top">
<ul id="navigationMenu">
<li id="top"><a href="/" id="navi1"><img src="/images/common/home.gif"/> HOME</a></li>
<li id="faq"><a href="http://2pm.jp/faq/p_b/cat22/" id="navi1" target="_blank">FAQ</a></li>
</ul>
</div>
</nav>
<!-- /nav -->
<!-- flash -->
<div id="flash">
</div>
<!-- /flash -->
<!-- contentsLayout -->
<section id="contentsLayout">
<div id="container">
<div id="wrapper">
<div><img alt="2PMはアフィリエイトを始めたい方や、既にアフィリエイトを始めていて、もっと効果を高めたい方にお勧めのサービスです。" height="261" src="/images/common/main_bg.jpg" width="1010"/></div>
<div class="partnerNew">
<h2>2pmを選ぶ3つのメリット</h2>
<span class="about"><a href="http://2pm.jp/partner/index.html">・パートナー詳細 （アフィリエイトとは？）</a></span>
<div class="promo">
<h3>信頼のおけるプロモーション</h3>
<p>2PMに掲載されているプロモーションは、全てある一定の基準を満たした広告主様のもののみを取り揃えております。パートナー様のニーズに最適な、信頼性の高い広告を提供できるのが2PMの強みです。</p>
</div>
<div class="detail">
<h3>詳細に渡る管理・確認</h3>
<p>2PMの管理画面は、パートナー様の登録されたサイト単位はもちろん定型している広告主様や媒体単位、また注文に対する請求など、詳細に渡って管理・確認が可能となります。ケースに講じて管理スタイルを使い分けられるのも2PMの強みです。</p>
</div>
<div class="smooth">
<h3>スムーズな操作</h3>
<p>2PMの操作はとても簡単です。広告を掲載したいサイトを登録したら、後は流れに沿って掲載するだけ。プロモーションを検索→統計で分析。とスムーズな操作と管理が可能です。詳細な管理が行えるからこそ、スムーズな操作が可能な設計。これも2PMの強みです。</p>
</div>
<div class="bt">
<span class="btPartnerNew"><a class="partnerNew" href="/partner/account/agreement" title="新規パートナー登録"></a></span>
</div>
</div>
<div id="recommended">
<div class="topicsNews">
<dl>
<dt>・2015年1月16日におけるシステムメンテナンスは完了いたしました。</dt>
<dt><a href="https://2pm.jp/news/news20150110.html">・2015年1月16日におけるシステムメンテナンスのお知らせ。</a></dt>
</dl>
</div>
<div id="recommendedCont">
<span id="pmark"><a href="http://privacymark.jp/" target="_blank"><img alt="プライバシーマーク制度" height="85" src="/images/common/10822731_85_JP.gif" width="85"/></a></span>
</div>
</div>
</div>
</div>
</section>
<!-- /contentsLayout -->
<p class="toTop"><a href="#header">PageTop</a></p>
<!-- footer -->
<footer id="footer">
<div class="top">
<ul class="pageLink">
<li class="newWindow"><a href="http://www.itokuro.jp/documents/pp.html" target="_blank">個人情報保護方針</a></li>
<li><a href="http://www.itokuro.jp/corporate/" target="_blank">会社概要</a></li>
<li><a href="http://2pm.jp/faq/p_b/recoenv.html" target="_blank">推奨環境</a></li>
<li><a href="mailto:info@2pm.jp">お問い合わせ</a></li>
</ul>
<ul class="copy">
<li>Copyright© 2PM All Rights Reserved</li>
</ul>
</div>
</footer>
<!--
   _____  _________________
  |A__  ||___  | ___ |     |
  / \_\ /|  ___|  ___| | | |
  |____V||_____|__|  |_|_|_|
  The Premier media Partners
  
  produced by 
  ############################################################
  ##########    ####  ############         ###             ###
  ########   #######  ##########  #######  ###  #########  ###
  ##        ########      ###############  ###  #########  ###
  ########  ########  ###    ############  ###  #########  ###
  ########  ########  ##################  ####  #########  ###
  ########  ########  #################  #####  #########  ###
  ########  ########  ###############  #######             ###
  ############################################################
-->
<!-- /footer -->
<script src="/javascripts/scrollsmoothly.js"></script>
</body>
</html>
