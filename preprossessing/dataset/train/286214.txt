<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html dir="ltr" version="XHTML+RDFa 1.0" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://www.w3.org/1999/xhtml/vocab">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={licenseKey:"e30bb3ed95",applicationID:"57026217"};window.NREUM||(NREUM={}),__nr_require=function(e,t,n){function r(n){if(!t[n]){var i=t[n]={exports:{}};e[n][0].call(i.exports,function(t){var i=e[n][1][t];return r(i||t)},i,i.exports)}return t[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var i=0;i<n.length;i++)r(n[i]);return r}({1:[function(e,t,n){function r(){}function i(e,t,n){return function(){return o(e,[u.now()].concat(c(arguments)),t?null:this,n),t?void 0:this}}var o=e("handle"),a=e(6),c=e(7),f=e("ee").get("tracer"),u=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var d=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],p="api-",l=p+"ixn-";a(d,function(e,t){s[t]=i(p+t,!0,"api")}),s.addPageAction=i(p+"addPageAction",!0),s.setCurrentRouteName=i(p+"routeName",!0),t.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,t){var n={},r=this,i="function"==typeof t;return o(l+"tracer",[u.now(),e,n],r),function(){if(f.emit((i?"":"no-")+"fn-start",[u.now(),r,i],n),i)try{return t.apply(this,arguments)}catch(e){throw f.emit("fn-err",[arguments,this,e],n),e}finally{f.emit("fn-end",[u.now()],n)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,t){m[t]=i(l+t)}),newrelic.noticeError=function(e,t){"string"==typeof e&&(e=new Error(e)),o("err",[e,u.now(),!1,t])}},{}],2:[function(e,t,n){function r(){return c.exists&&performance.now?Math.round(performance.now()):(o=Math.max((new Date).getTime(),o))-a}function i(){return o}var o=(new Date).getTime(),a=o,c=e(8);t.exports=r,t.exports.offset=a,t.exports.getLastTimestamp=i},{}],3:[function(e,t,n){function r(e,t){var n=e.getEntries();n.forEach(function(e){"first-paint"===e.name?d("timing",["fp",Math.floor(e.startTime)]):"first-contentful-paint"===e.name&&d("timing",["fcp",Math.floor(e.startTime)])})}function i(e,t){var n=e.getEntries();n.length>0&&d("lcp",[n[n.length-1]])}function o(e){e.getEntries().forEach(function(e){e.hadRecentInput||d("cls",[e])})}function a(e){if(e instanceof m&&!g){var t=Math.round(e.timeStamp),n={type:e.type};t<=p.now()?n.fid=p.now()-t:t>p.offset&&t<=Date.now()?(t-=p.offset,n.fid=p.now()-t):t=p.now(),g=!0,d("timing",["fi",t,n])}}function c(e){d("pageHide",[p.now(),e])}if(!("init"in NREUM&&"page_view_timing"in NREUM.init&&"enabled"in NREUM.init.page_view_timing&&NREUM.init.page_view_timing.enabled===!1)){var f,u,s,d=e("handle"),p=e("loader"),l=e(5),m=NREUM.o.EV;if("PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){f=new PerformanceObserver(r);try{f.observe({entryTypes:["paint"]})}catch(v){}u=new PerformanceObserver(i);try{u.observe({entryTypes:["largest-contentful-paint"]})}catch(v){}s=new PerformanceObserver(o);try{s.observe({type:"layout-shift",buffered:!0})}catch(v){}}if("addEventListener"in document){var g=!1,w=["click","keydown","mousedown","pointerdown","touchstart"];w.forEach(function(e){document.addEventListener(e,a,!1)})}l(c)}},{}],4:[function(e,t,n){function r(e,t){if(!i)return!1;if(e!==i)return!1;if(!t)return!0;if(!o)return!1;for(var n=o.split("."),r=t.split("."),a=0;a<r.length;a++)if(r[a]!==n[a])return!1;return!0}var i=null,o=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var c=navigator.userAgent,f=c.match(a);f&&c.indexOf("Chrome")===-1&&c.indexOf("Chromium")===-1&&(i="Safari",o=f[1])}t.exports={agent:i,version:o,match:r}},{}],5:[function(e,t,n){function r(e){function t(){e(a&&document[a]?document[a]:document[i]?"hidden":"visible")}"addEventListener"in document&&o&&document.addEventListener(o,t,!1)}t.exports=r;var i,o,a;"undefined"!=typeof document.hidden?(i="hidden",o="visibilitychange",a="visibilityState"):"undefined"!=typeof document.msHidden?(i="msHidden",o="msvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(i="webkitHidden",o="webkitvisibilitychange",a="webkitVisibilityState")},{}],6:[function(e,t,n){function r(e,t){var n=[],r="",o=0;for(r in e)i.call(e,r)&&(n[o]=t(r,e[r]),o+=1);return n}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],7:[function(e,t,n){function r(e,t,n){t||(t=0),"undefined"==typeof n&&(n=e?e.length:0);for(var r=-1,i=n-t||0,o=Array(i<0?0:i);++r<i;)o[r]=e[t+r];return o}t.exports=r},{}],8:[function(e,t,n){t.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,t,n){function r(){}function i(e){function t(e){return e&&e instanceof r?e:e?u(e,f,a):a()}function n(n,r,i,o,a){if(a!==!1&&(a=!0),!l.aborted||o){e&&a&&e(n,r,i);for(var c=t(i),f=v(n),u=f.length,s=0;s<u;s++)f[s].apply(c,r);var p=d[h[n]];return p&&p.push([b,n,r,c]),c}}function o(e,t){y[e]=v(e).concat(t)}function m(e,t){var n=y[e];if(n)for(var r=0;r<n.length;r++)n[r]===t&&n.splice(r,1)}function v(e){return y[e]||[]}function g(e){return p[e]=p[e]||i(n)}function w(e,t){s(e,function(e,n){t=t||"feature",h[n]=t,t in d||(d[t]=[])})}var y={},h={},b={on:o,addEventListener:o,removeEventListener:m,emit:n,get:g,listeners:v,context:t,buffer:w,abort:c,aborted:!1};return b}function o(e){return u(e,f,a)}function a(){return new r}function c(){(d.api||d.feature)&&(l.aborted=!0,d=l.backlog={})}var f="nr@context",u=e("gos"),s=e(6),d={},p={},l=t.exports=i();t.exports.getOrSetContext=o,l.backlog=d},{}],gos:[function(e,t,n){function r(e,t,n){if(i.call(e,t))return e[t];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:r,writable:!0,enumerable:!1}),r}catch(o){}return e[t]=r,r}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],handle:[function(e,t,n){function r(e,t,n,r){i.buffer([e],r),i.emit(e,t,n)}var i=e("ee").get("handle");t.exports=r,r.ee=i},{}],id:[function(e,t,n){function r(e){var t=typeof e;return!e||"object"!==t&&"function"!==t?-1:e===window?0:a(e,o,function(){return i++})}var i=1,o="nr@id",a=e("gos");t.exports=r},{}],loader:[function(e,t,n){function r(){if(!x++){var e=b.info=NREUM.info,t=p.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&t))return u.abort();f(y,function(t,n){e[t]||(e[t]=n)});var n=a();c("mark",["onload",n+b.offset],null,"api"),c("timing",["load",n]);var r=p.createElement("script");r.src="https://"+e.agent,t.parentNode.insertBefore(r,t)}}function i(){"complete"===p.readyState&&o()}function o(){c("mark",["domContent",a()+b.offset],null,"api")}var a=e(2),c=e("handle"),f=e(6),u=e("ee"),s=e(4),d=window,p=d.document,l="addEventListener",m="attachEvent",v=d.XMLHttpRequest,g=v&&v.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:v,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var w=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1194.min.js"},h=v&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),b=t.exports={offset:a.getLastTimestamp(),now:a,origin:w,features:{},xhrWrappable:h,userAgent:s};e(1),e(3),p[l]?(p[l]("DOMContentLoaded",o,!1),d[l]("load",r,!1)):(p[m]("onreadystatechange",i),d[m]("onload",r)),c("mark",["firstbyte",a.getLastTimestamp()],null,"api");var x=0},{}],"wrap-function":[function(e,t,n){function r(e,t){function n(t,n,r,f,u){function nrWrapper(){var o,a,s,p;try{a=this,o=d(arguments),s="function"==typeof r?r(o,a):r||{}}catch(l){i([l,"",[o,a,f],s],e)}c(n+"start",[o,a,f],s,u);try{return p=t.apply(a,o)}catch(m){throw c(n+"err",[o,a,m],s,u),m}finally{c(n+"end",[o,a,p],s,u)}}return a(t)?t:(n||(n=""),nrWrapper[p]=t,o(t,nrWrapper,e),nrWrapper)}function r(e,t,r,i,o){r||(r="");var c,f,u,s="-"===r.charAt(0);for(u=0;u<t.length;u++)f=t[u],c=e[f],a(c)||(e[f]=n(c,s?f+r:r,i,f,o))}function c(n,r,o,a){if(!m||t){var c=m;m=!0;try{e.emit(n,r,o,t,a)}catch(f){i([f,n,r,o],e)}m=c}}return e||(e=s),n.inPlace=r,n.flag=p,n}function i(e,t){t||(t=s);try{t.emit("internal-error",e)}catch(n){}}function o(e,t,n){if(Object.defineProperty&&Object.keys)try{var r=Object.keys(e);return r.forEach(function(n){Object.defineProperty(t,n,{get:function(){return e[n]},set:function(t){return e[n]=t,t}})}),t}catch(o){i([o],n)}for(var a in e)l.call(e,a)&&(t[a]=e[a]);return t}function a(e){return!(e&&e instanceof Function&&e.apply&&!e[p])}function c(e,t){var n=t(e);return n[p]=e,o(e,n,s),n}function f(e,t,n){var r=e[t];e[t]=c(r,n)}function u(){for(var e=arguments.length,t=new Array(e),n=0;n<e;++n)t[n]=arguments[n];return t}var s=e("ee"),d=e(7),p="nr@original",l=Object.prototype.hasOwnProperty,m=!1;t.exports=r,t.exports.wrapFunction=c,t.exports.wrapInPlace=f,t.exports.argsToArray=u},{}]},{},["loader"]);</script>
<link href="https://carcarenewsservice.org/sites/default/files/favicon.png" rel="shortcut icon" type="image/png"/>
<meta content="Drupal 7 (https://www.drupal.org)" name="generator"/>
<link href="https://carcarenewsservice.org/" rel="canonical"/>
<link href="https://carcarenewsservice.org/" rel="shortlink"/>
<title>Page Not Found | Car Care News Service</title>
<link href="https://carcarenewsservice.org/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://carcarenewsservice.org/sites/default/files/css/css_EPsgvacVXxxhwhLr4DGj8P1D4es4L_QnnRJPHn_TJHE.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://carcarenewsservice.org/sites/default/files/css/css_2gkrWMKqE9oNShrXVfTT7PeftecQok3UMUxADYD0Pos.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://carcarenewsservice.org/sites/default/files/css/css_OR_ccKKiEKQNcarKPYaFUFOcVvzwYMpqnxyQ6I3C-Gs.css" media="all" rel="stylesheet" type="text/css"/>
<!--[if IE 8]>
<link type="text/css" rel="stylesheet" href="https://carcarenewsservice.org/sites/all/themes/custom/mayo/css/ie8.css?przgpx" media="all" />
<![endif]-->
<!--[if  IE 7]>
<link type="text/css" rel="stylesheet" href="https://carcarenewsservice.org/sites/all/themes/custom/mayo/css/ie.css?przgpx" media="all" />
<![endif]-->
<!--[if IE 6]>
<link type="text/css" rel="stylesheet" href="https://carcarenewsservice.org/sites/all/themes/custom/mayo/css/ie6.css?przgpx" media="all" />
<![endif]-->
<link href="https://carcarenewsservice.org/sites/default/files/css/css_HKhOYu5JbzI0qj5bc7GfNwpmD0incd5VdnPC7b1Hw7I.css" media="all" rel="stylesheet" type="text/css"/>
<style media="all" type="text/css">
<!--/*--><![CDATA[/*><!--*/
body{font-size:87.5%;font-family:Helvetica,Arial,sans-serif;}
h1,h2,h3,h4,h5{font-size:87.5%;font-family:Helvetica,Arial,sans-serif;font-family:Helvetica,Arial,sans-serif;}

/*]]>*/-->
</style>
<script src="https://carcarenewsservice.org/sites/default/files/js/js_EebRuRXFlkaf356V0T2K_8cnUVfCKesNTxdvvPSEhCM.js" type="text/javascript"></script>
<script src="https://carcarenewsservice.org/sites/default/files/js/js_rrpaie7hRzVHhiJLdwWzQRDVEmKqpr6xEo_u_VTa9zw.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-12992120-2", {"cookieDomain":"auto"});ga("set", "page", "/404.html?page=" + document.location.pathname + document.location.search + "&from=" + document.referrer);ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(w,d,t,u,n,a,m){w["MauticTrackingObject"]=n;w[n]=w[n]||function(){(w[n].q=w[n].q||[]).push(arguments)},a=d.createElement(t),m=d.getElementsByTagName(t)[0];a.async=1;a.src=u;m.parentNode.insertBefore(a,m)})(window,document,"script","https://obd.carcare.news/mtc.js","mt");mt("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"mayo","theme_token":"3pw8loLwQxWsTXMvSiGSUgHcRKIW4bwDRwOGu8Vc-WA","js":{"0":1,"\/\/tags.clickagy.com\/data.js?rnd=59035ec924b9d":1,"sites\/all\/modules\/contrib\/mautic\/js\/mauticform-prefill.js":1,"sites\/all\/modules\/contrib\/addthis\/addthis.js":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/contrib\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/contrib\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/contrib\/colorbox\/js\/colorbox_load.js":1,"sites\/all\/modules\/contrib\/google_analytics\/googleanalytics.js":1,"1":1,"2":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/contrib\/date\/date_api\/date.css":1,"sites\/all\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/modules\/contrib\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/contrib\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/contrib\/panels\/css\/panels.css":1,"sites\/all\/themes\/custom\/mayo\/css\/layout.css":1,"sites\/all\/themes\/custom\/mayo\/css\/style.css":1,"sites\/all\/themes\/custom\/mayo\/css\/colors.css":1,"sites\/all\/themes\/custom\/mayo\/css\/ccns.css":1,"sites\/all\/themes\/custom\/mayo\/css\/ie8.css":1,"sites\/all\/themes\/custom\/mayo\/css\/ie.css":1,"sites\/all\/themes\/custom\/mayo\/css\/ie6.css":1,"sites\/all\/themes\/custom\/mayo\/css\/round-sidebar.css":1,"sites\/all\/themes\/custom\/mayo\/css\/round-node.css":1,"0":1,"1":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1},"urlIsAjaxTrusted":{"\/Mpootryjidrt\/customer_center\/customer-IDPP00C345\/%09%0A":true}});
//--><!]]>
</script>
</head>
<body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-32 node-type-page">
<div id="skip-link">
<a class="element-invisible element-focusable" href="#main-content">Skip to main content</a>
</div>
<div id="page-wrapper" style="width: 975px; margin-top: 14px; margin-bottom: 14px;">
<div id="page" style="padding: 20px;">
<div id="header" style="height: 220px;border-width: 0px;">
<div id="header-watermark" style="">
<div class="section clearfix">
<div id="logo" style="padding-left: 10px; padding-top: 30px;">
<a href="/" rel="home" title="Home">
<img alt="Home" src="https://carcarenewsservice.org/sites/default/files/xcarcare_logo.png.pagespeed.ic_._YlJSUeVzW.png"/>
</a>
</div>
<div id="name-and-slogan" style="padding-left: 40px; padding-top: 120px;">
<div id="site-slogan"><b>Free and Credible Content about Vehicle Maintenance,<br/> Repair, and Enhancement Since 1964</b></div>
</div>
<p align="right">
<a href="?q=user">Sponsor Login</a>
</p><p align="right">
</p>
<div id="header-searchbox" style="padding-right: 0px; padding-top: 0px;">
<form accept-charset="UTF-8" action="/Mpootryjidrt/customer_center/customer-IDPP00C345/%09%0A" id="search-block-form" method="post"><div><div class="container-inline">
<h2 class="element-invisible">Search form</h2>
<div class="form-item form-type-textfield form-item-search-block-form">
<label class="element-invisible" for="edit-search-block-form--2">Search </label>
<input class="form-text" id="edit-search-block-form--2" maxlength="128" name="search_block_form" onblur="if (this.value == '') { this.value = 'search this site'; }" onfocus="if (this.value == 'search this site') { this.value = ''; }" size="25" type="text" value="search this site"/>
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input class="form-submit" src="/sites/all/themes/custom/mayo/images/search-submit.png" type="image"/></div><input name="form_build_id" type="hidden" value="form-0CZeDsD7c6lPYV-EYXuMJQxlfmPqS-5BWvcOMrA7ALo"/>
<input name="form_id" type="hidden" value="search_block_form"/>
</div>
</div></form> </div>
<div class="clearfix cfie"></div>
</div>
</div>
</div>
<div id="navigation"><div class="section">
<ul class="links inline clearfix" id="main-menu"><li class="menu-537 first"><a href="/articles" title="">Articles</a></li>
<li class="menu-585"><a href="/press-releases">Press Releases</a></li>
<li class="menu-933"><a href="/promotions">Promotions</a></li>
<li class="menu-931"><a href="https://carcarenews.wordpress.com/" target="_blank">Blog</a></li>
<li class="menu-541"><a href="/video">Video</a></li>
<li class="menu-584"><a href="/audio" title="">Audio</a></li>
<li class="menu-543"><a href="/sponsors">Sponsors</a></li>
<li class="menu-544"><a href="/sponsorship">Sponsorship</a></li>
<li class="menu-542"><a href="/about">About Us</a></li>
<li class="menu-724 last"><a href="/contact-us">Contact Us</a></li>
</ul> </div></div>
<div class="clearfix cfie"></div>
<div class="spacer clearfix cfie"></div>
<div id="main-wrapper">
<div class="clearfix" id="main" style="">
<div class="clearfix cfie"></div>
<div class="column" id="content" style="width: 100%;"><div class="section" style="margin-left: 0px; margin-right: 0px;">
<a id="main-content"></a>
<h1 class="title" id="page-title">Page Not Found</h1> <div class="tabs"></div> <div class="region region-content">
<div class="block block-system clearfix" id="block-system-main">
<div class="content">
<div class="node node-page clearfix" id="node-32">
<div class="content">
<div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><h2><img alt="" src="/sites/default/files/Traffic%20095-0143.gif" style="height:300px; width:400px"/></h2>
<h3><strong>Woah! Hold it there!</strong></h3>
<p>You seem to have tried to access a page that we can't find. We're really sorry about that! Please try again, or use the search bar up top to look for the correct page. If you're still having problems, <a href="/contact-us">contact us</a> and we'll help you out <strong>ASAP</strong>. </p>
<p> </p>
<p>Thanks!</p>
<p>- The CCNS Web Team</p>
</div></div></div> </div>
</div>
</div>
</div>
</div>
</div></div>
<div class="clearfix cfie"></div>
<div class="clearfix cfie"></div>
</div>
</div>
<div class="clearfix cfie" id="spacer"></div>
<div id="footer-wrapper">
<div id="footer"><div class="section">
<div class="region region-footer">
<div class="block block-block clearfix" id="block-block-9">
<div class="content">
<p><a href="https://twitter.com/carcarenews" style="line-height: 1.6em;" target="_blank"><img alt="Twitter" longdesc="" src="/sites/default/files/twitter.png" style="height:30px; width:30px" title="CCNS Twitter"/></a> <a href="https://carcarenews.wordpress.com/" style="line-height: 1.6em;" target="_blank"><img alt="CCNS Blog" src="/sites/default/files/Screen%20Shot%202013-04-22%20at%207.01.15%20PM.png" style="height:30px; width:30px" title="CCNS Blog"/></a> <a href="http://www.carcarenewsservice.org/articles/all/rss" style="line-height: 1.6em;" target="_blank"><img alt="RSS Feed" src="/sites/default/files/rss.png" style="height:30px; width:30px" title="CCNS RSS Feed"/></a> <a href="https://www.facebook.com/pages/Car-Care-News-Service/59347947906?ref=search&amp;sid=715802368.1480386970..1&amp;v=wall" style="line-height: 1.6em;" target="_blank"><img alt="Facebook" src="/sites/default/files/facebook.png" style="height:30px; width:30px" title="CCNS Facebook"/></a> <a href="https://www.youtube.com/user/CarCareNews" style="line-height: 1.6em;" target="_blank"><img alt="YouTube" src="/sites/default/files/youtube.png" style="height:30px; width:30px" title="CCNS YouTube"/></a><br/><a href="/subscriber/profile"><strong>Sign up for Email Updates!</strong></a></p>
<p><strong>© 2017 Car Care News Service LLC. All rights reserved. </strong><br/><br/>
If you have any questions, <strong><a href="/contact-us" title="Contact Us">contact us</a></strong> and we will be happy to assist you.</p>
</div>
</div>
<div class="block block-block clearfix" id="block-block-17">
<div class="content">
<p style="text-align: center;"><script async="async" charset="utf8" src="https://obd.carcare.news/focus/1.js"></script></p>
</div>
</div>
</div>
</div></div>
</div>
</div>
</div>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
_clickagyData={"aid":"154c3t0qu4hsyh","list":"wi98vcr4c8bkv","ci":true}
//--><!]]>
</script>
<script src="//tags.clickagy.com/data.js?rnd=59035ec924b9d" type="text/javascript"></script>
<script src="https://carcarenewsservice.org/sites/default/files/js/js_3HFNPKBtZrBjLwmoDlPPkx5vJpIXl7EHzemcZ_f3hhU.js" type="text/javascript"></script>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"e30bb3ed95","applicationID":"57026217","transactionName":"Y1MEYBEDW0BZBkwKXFoZJ1cXC1pdFwtXB1ZrRgdTBj1DWl0S","queueTime":0,"applicationTime":92,"atts":"TxQHFlkZSE4=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>
</html>
