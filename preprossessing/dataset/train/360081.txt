<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="https://cooperativasocialpenedes.org/xmlrpc.php" rel="pingback"/> <link href="" rel="shortcut icon"/><title>Página no encontrada | Cooperativa Social Penedes</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://cooperativasocialpenedes.org/feed/" rel="alternate" title="Cooperativa Social Penedes » Feed" type="application/rss+xml"/>
<link href="https://cooperativasocialpenedes.org/comments/feed/" rel="alternate" title="Cooperativa Social Penedes » Feed de los comentarios" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/cooperativasocialpenedes.org\/wp-includes\/js\/wp-emoji-release.min.js?ver=8a2b763776facea1c6eab3e2e03b5a35"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://cooperativasocialpenedes.org/wp-includes/css/dist/block-library/style.min.css?ver=8a2b763776facea1c6eab3e2e03b5a35" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cooperativasocialpenedes.org/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
#rs-demo-id {}
</style>
<link href="https://cooperativasocialpenedes.org/wp-content/themes/lambda/assets/css/bootstrap.min.css?ver=8a2b763776facea1c6eab3e2e03b5a35" id="lambda-bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cooperativasocialpenedes.org/wp-includes/js/mediaelement/mediaelementplayer-legacy.min.css?ver=4.2.16" id="mediaelement-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cooperativasocialpenedes.org/wp-includes/js/mediaelement/wp-mediaelement.min.css?ver=8a2b763776facea1c6eab3e2e03b5a35" id="wp-mediaelement-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cooperativasocialpenedes.org/wp-content/themes/lambda/assets/css/theme.min.css?ver=8a2b763776facea1c6eab3e2e03b5a35" id="lambda-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cooperativasocialpenedes.org/wp-content/themes/lambda/inc/assets/stylesheets/visual-composer/vc-frontend.css?ver=8a2b763776facea1c6eab3e2e03b5a35" id="lambda-vc-frontend-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cooperativasocialpenedes.org/wp-content/uploads/lambda/stack-113.css?ver=8a2b763776facea1c6eab3e2e03b5a35" id="lambda-theme-stack-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://cooperativasocialpenedes.org/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://cooperativasocialpenedes.org/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="tp-tools-js" src="https://cooperativasocialpenedes.org/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.8" type="text/javascript"></script>
<script id="revmin-js" src="https://cooperativasocialpenedes.org/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8" type="text/javascript"></script>
<link href="https://cooperativasocialpenedes.org/wp-json/" rel="https://api.w.org/"/><link href="https://cooperativasocialpenedes.org/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://cooperativasocialpenedes.org/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><meta content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." name="generator"/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://cooperativasocialpenedes.org/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." name="generator"/>
<script type="text/javascript">function setREVStartSize(e){									
						try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
							if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
						}catch(d){console.log("Failure at Presize of Slider:"+d)}						
					};</script>
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic%7CMontserrat:400,700&amp;subset=latin,latin" rel="stylesheet" type="text/css"/><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
</head>
<body class="error404 pace-on pace-dot wpb-js-composer js-comp-ver-5.5.4 vc_responsive" data-rsssl="1">
<div class="pace-overlay"></div>
<div class="menu navbar navbar-static-top header-logo-left-menu-right oxy-mega-menu navbar-sticky navbar-not-mobile-stuck text-caps" id="masthead" role="banner">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle collapsed" data-target=".main-navbar" data-toggle="collapse" type="button">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="https://cooperativasocialpenedes.org">
<img alt="Cooperativa Social Penedes" src="https://cooperativasocialpenedes.org/wp-content/uploads/2018/11/COOP-SOCIAL-PENEDES-LOGO-NEGREimp.png"/>
</a>
</div>
<div class="nav-container">
<nav class="collapse navbar-collapse main-navbar logo-navbar navbar-right" role="navigation">
<div class="menu-container"><ul class="nav navbar-nav" id="menu-main"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-114" id="menu-item-114"><a href="https://cooperativasocialpenedes.org/">La cooperativa social</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-115" id="menu-item-115"><a href="https://cooperativasocialpenedes.org/who-we-are/">Qui Som</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209" id="menu-item-209"><a href="https://cooperativasocialpenedes.org/serveis/">Serveis</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-118" id="menu-item-118"><a href="https://cooperativasocialpenedes.org/contact-us/">Contacte</a></li>
</ul></div><div class="menu-sidebar">
</div>
</nav>
</div>
</div>
</div>
<div id="content" role="main"><section class="section text-normal section-text-no-shadow section-inner-no-shadow section-normal section-opaque">
<div class="background-overlay grid-overlay-0 " style="background-color: rgba(0,0,0,0);"></div>
<div class="container container-vertical-default">
<div class="row vertical-default">
<header class="small-screen-center text-normal col-md-12">
<h1 class="text-left element-top-20 element-bottom-20 text-normal normal default" data-os-animation="none" data-os-animation-delay="0s">
    Blog</h1>
</header>
</div>
</div>
</section>
<article class="post-66 page type-page status-publish hentry" id="post-66">
<span class="hide"><span class="author vcard"><span class="fn"></span></span><span class="entry-title">Blog</span><time class="entry-date updated" datetime="2015-08-04T12:06:35+00:00">08.04.2015</time></span></article>
<footer id="footer" role="contentinfo">
<section class="section">
<div class="container">
<div class="row element-top-20 element-bottom-20 footer-columns-3">
<div class="col-sm-4">
<div class="sidebar-widget widget_text" id="text-1"><h3 class="sidebar-header">CONTACTE</h3> <div class="textwidget"><p>m. 605 933 559 m. 609 317 911</p>
<p><a href="mailto:hola@cooperativasocialpenedes.org?subject=Contacte">hola@cooperativasocialpenedes.org</a></p>
<p><a href="https://www.cooperativasocialpenedes.org/" rel="noopener" target="_blank">www.cooperativasocialpenedes.org</a></p>
</div>
</div> </div>
<div class="col-sm-4">
<div class="sidebar-widget widget_social" id="oxywidgetsocial-2"><h3 class="sidebar-header">SEGUEIX-NOS</h3><ul class="unstyled inline social-icons social-background social-lg"><li><a data-iconcolor="#3b9999" href="https://www.facebook.com/cooperativasocialpenedes/"><i class="fa fa-facebook-official"></i></a></li><li><a data-iconcolor="#00acee" href=""><i class="fa fa-twitter"></i></a></li><li><a data-iconcolor="#E45135" href=""><i class="fa fa-google-plus"></i></a></li><li><a data-iconcolor="#3b9999" href=""><i class="fa fa-pinterest-p"></i></a></li></ul></div> </div>
<div class="col-sm-4">
<div class="sidebar-widget widget_text" id="text-2"> <div class="textwidget"><p><img alt="" class="alignnone size-medium wp-image-121" height="100" loading="lazy" sizes="(max-width: 300px) 100vw, 300px" src="https://cooperativasocialpenedes.org/wp-content/uploads/2018/11/COOP-SOCIAL-PENEDES-LOGO-NEGREimp-300x100.png" srcset="https://cooperativasocialpenedes.org/wp-content/uploads/2018/11/COOP-SOCIAL-PENEDES-LOGO-NEGREimp-300x100.png 300w, https://cooperativasocialpenedes.org/wp-content/uploads/2018/11/COOP-SOCIAL-PENEDES-LOGO-NEGREimp.png 412w" width="300"/></p>
</div>
</div> </div>
</div>
</div>
</section>
<section class="section subfooter">
<div class="container">
<div class="row element-top-10 element-bottom-10 footer-columns-2">
<div class="col-sm-6">
<div class="sidebar-widget widget_text" id="text-3"> <div class="textwidget"><p>© 2018 Dv-Alpha Solutions. Todos los derechos reservados</p>
</div>
</div> </div>
<div class="col-sm-6">
</div>
</div>
</div>
</section>
</footer>
</div>
<!-- Fixing the Back to top button -->
<a class="go-top go-top-circle " href="javascript:void(0)">
<i class="fa fa-angle-up"></i>
</a>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/cooperativasocialpenedes.org\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://cooperativasocialpenedes.org/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="flexslider-js" src="https://cooperativasocialpenedes.org/wp-content/plugins/js_composer/assets/lib/bower/flexslider/jquery.flexslider-min.js?ver=5.5.4" type="text/javascript"></script>
<script id="mediaelement-core-js-before" type="text/javascript">
var mejsL10n = {"language":"es","strings":{"mejs.download-file":"Descargar archivo","mejs.install-flash":"Est\u00e1s usando un navegador que no tiene Flash activo o instalado. Por favor, activa el componente del reproductor Flash o descarga la \u00faltima versi\u00f3n desde https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen":"Pantalla completa","mejs.play":"Reproducir","mejs.pause":"Pausa","mejs.time-slider":"Control de tiempo","mejs.time-help-text":"Usa las teclas de direcci\u00f3n izquierda\/derecha para avanzar un segundo y las flechas arriba\/abajo para avanzar diez segundos.","mejs.live-broadcast":"Transmisi\u00f3n en vivo","mejs.volume-help-text":"Utiliza las teclas de flecha arriba\/abajo para aumentar o disminuir el volumen.","mejs.unmute":"Activar el sonido","mejs.mute":"Silenciar","mejs.volume-slider":"Control de volumen","mejs.video-player":"Reproductor de v\u00eddeo","mejs.audio-player":"Reproductor de audio","mejs.captions-subtitles":"Pies de foto \/ Subt\u00edtulos","mejs.captions-chapters":"Cap\u00edtulos","mejs.none":"Ninguna","mejs.afrikaans":"Afrik\u00e1ans","mejs.albanian":"Albano","mejs.arabic":"\u00c1rabe","mejs.belarusian":"Bielorruso","mejs.bulgarian":"B\u00falgaro","mejs.catalan":"Catal\u00e1n","mejs.chinese":"Chino","mejs.chinese-simplified":"Chino (Simplificado)","mejs.chinese-traditional":"Chino (Tradicional)","mejs.croatian":"Croata","mejs.czech":"Checo","mejs.danish":"Dan\u00e9s","mejs.dutch":"Holand\u00e9s","mejs.english":"Ingl\u00e9s","mejs.estonian":"Estonio","mejs.filipino":"Filipino","mejs.finnish":"Fin\u00e9s","mejs.french":"Franc\u00e9s","mejs.galician":"Gallego","mejs.german":"Alem\u00e1n","mejs.greek":"Griego","mejs.haitian-creole":"Creole haitiano","mejs.hebrew":"Hebreo","mejs.hindi":"Indio","mejs.hungarian":"H\u00fangaro","mejs.icelandic":"Island\u00e9s","mejs.indonesian":"Indonesio","mejs.irish":"Irland\u00e9s","mejs.italian":"Italiano","mejs.japanese":"Japon\u00e9s","mejs.korean":"Coreano","mejs.latvian":"Let\u00f3n","mejs.lithuanian":"Lituano","mejs.macedonian":"Macedonio","mejs.malay":"Malayo","mejs.maltese":"Malt\u00e9s","mejs.norwegian":"Noruego","mejs.persian":"Persa","mejs.polish":"Polaco","mejs.portuguese":"Portugu\u00e9s","mejs.romanian":"Rumano","mejs.russian":"Ruso","mejs.serbian":"Serbio","mejs.slovak":"Eslovaco","mejs.slovenian":"Esloveno","mejs.spanish":"Espa\u00f1ol","mejs.swahili":"Swahili","mejs.swedish":"Sueco","mejs.tagalog":"Tagalo","mejs.thai":"Tailand\u00e9s","mejs.turkish":"Turco","mejs.ukrainian":"Ukraniano","mejs.vietnamese":"Vietnamita","mejs.welsh":"Gal\u00e9s","mejs.yiddish":"Yiddish"}};
</script>
<script id="mediaelement-core-js" src="https://cooperativasocialpenedes.org/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.16" type="text/javascript"></script>
<script id="mediaelement-migrate-js" src="https://cooperativasocialpenedes.org/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=8a2b763776facea1c6eab3e2e03b5a35" type="text/javascript"></script>
<script id="mediaelement-js-extra" type="text/javascript">
/* <![CDATA[ */
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
/* ]]> */
</script>
<script id="wp-mediaelement-js" src="https://cooperativasocialpenedes.org/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=8a2b763776facea1c6eab3e2e03b5a35" type="text/javascript"></script>
<script id="lambda-theme-js-extra" type="text/javascript">
/* <![CDATA[ */
var oxyThemeData = {"navbarScrolledPoint":"30","navbarHeight":"","navbarScrolled":"","siteLoader":"on","menuClose":"off","scrollFinishedMessage":"No more items to load.","hoverMenu":{"hoverActive":false,"hoverDelay":200,"hoverFadeDelay":200}};
/* ]]> */
</script>
<script id="lambda-theme-js" src="https://cooperativasocialpenedes.org/wp-content/themes/lambda/assets/js/theme.min.js?ver=1.0" type="text/javascript"></script>
<script id="wp-embed-js" src="https://cooperativasocialpenedes.org/wp-includes/js/wp-embed.min.js?ver=8a2b763776facea1c6eab3e2e03b5a35" type="text/javascript"></script>
</body>
</html>
