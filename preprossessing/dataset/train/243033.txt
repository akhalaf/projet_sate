<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="de" xml:lang="de" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<!-- 
	This website is powered by TYPO3 - inspiring people to share!
	TYPO3 is a free open source Content Management Framework initially created by Kasper Skaarhoj and licensed under GNU/GPL.
	TYPO3 is copyright 1998-2020 of Kasper Skaarhoj. Extensions are copyright of their respective owners.
	Information and contribution at http://typo3.org/
-->
<base href="https://www.blumau.com/"/>
<meta content="TYPO3 CMS" name="generator"/>
<meta content="index,follow" name="robots"/>
<meta content="IE=edge" name="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="https://www.blumau.com/" name="og:url"/>
<meta content="website" name="og:type"/>
<meta content="Rogner Bad Blumau - Hundertwasser Therme und Wellness-Hotel in der Steiermark, Österreich" name="og:title"/>
<meta content="0xgXB0l7f4wP2bfVmuGgAtaK_8nahoU-5199lmUOqRI" name="google-site-verification"/>
<meta content="Thermen,Therme,Wellness,Hotel,Wellness Hotel,Wellnesshotel,Österreich,Steiermark,Hundertwasser,Bad Blumau,Wellnessurlaub,Urlaub,Rogner,Blumau,Angebote" name="KEYWORDS"/>
<meta content="4 Sterne Wellnesshotel mit Thermen und Gesundheitszentrum in Österreich gestaltet von Friedensreich Hundertwasser mit begrünten Dächern, bunten Fassaden und goldenen Kuppeln." name="DESCRIPTION"/>
<meta content="https://www.blumau.com/fileadmin/root_blumau/Facebook/rognerBadBlumau.jpg" name="og:image"/>
<link href="/typo3temp/compressor/merged-825bde79fcd380771ee5819584f43892-7af58d8ea6559b17420f21817b29f965.css.gzip?1537513724" media="all" rel="stylesheet" type="text/css"/>
<link href="/typo3temp/compressor/jquery.fancybox-bbe723e335c52f0e5a6c6a1c98fcc5f6.css.gzip?1469224123" media="screen" rel="stylesheet" type="text/css"/>
<link href="/typo3temp/compressor/print-dc6a38b80026d8be206311a27586017c.css.gzip?1469224123" media="print" rel="stylesheet" type="text/css"/>
<link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" media="all" rel="stylesheet" type="text/css"/>
<link href="/fileadmin/scripts/msgBox/msgBoxLight.css?1450433759" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.googleoptimize.com/optimize.js?id=OPT-N4PCQ22" type="text/javascript"></script>
<script src="/typo3temp/compressor/merged-3fd3cba5e4f090d5ff6f7b8ca629406f-0eabfb8421584603fe3e130b16d5fa86.js.gzip?1595787925" type="text/javascript"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-2013284-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-2013284-2',{ 'optimize_id': 'OPT-N4PCQ22'});
</script> <title>Rogner Bad Blumau - Hundertwasser Therme und Wellness-Hotel in der Steiermark, Österreich</title><link href="/fileadmin/templates/grafiken/faviconsblumau/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="/fileadmin/templates/grafiken/faviconsblumau/apple-touch-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="/fileadmin/templates/grafiken/faviconsblumau/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/fileadmin/templates/grafiken/faviconsblumau/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/fileadmin/templates/grafiken/faviconsblumau/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="/fileadmin/templates/grafiken/faviconsblumau/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/fileadmin/templates/grafiken/faviconsblumau/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="/fileadmin/templates/grafiken/faviconsblumau/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/fileadmin/templates/grafiken/faviconsblumau/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/fileadmin/templates/grafiken/faviconsblumau/favicon.ico" rel="shortcut icon" type="image/ico"/>
<link href="/fileadmin/templates/grafiken/faviconsblumau/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/fileadmin/templates/grafiken/faviconsblumau/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="/fileadmin/templates/grafiken/faviconsblumau/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/fileadmin/templates/grafiken/faviconsblumau/manifest.json" rel="manifest"/>
<link color="#5bbad5" href="/fileadmin/templates/grafiken/faviconsblumau/safari-pinned-tab.svg" rel="mask-icon"/>
<link href="/fileadmin/templates/grafiken/faviconsblumau/favicon.ico" rel="shortcut icon"/>
<meta content="#da532c" name="msapplication-TileColor"/>
<meta content="/fileadmin/templates/grafiken/faviconsblumau/mstile-144x144.png" name="msapplication-TileImage"/>
<meta content="/fileadmin/templates/grafiken/faviconsblumau/browserconfig.xml" name="msapplication-config"/>
<meta content="#ffffff" name="theme-color"/>
</head>
<body>
<div class="container">
<div class="mp-pusher" id="mp-pusher">
<div data-set="menu" id="mobilemenu"> </div>
<div id="center">
<div id="overall-container">
<div id="logo-menu-lang-container">
<div class="menu-wrapper">
<div class="headmenu-wrapper">
<div class="headmenu headmenu-left">
<a class="btn" href="tel:+43338351000"> <span class="fa fa-phone"></span> <span class="hidden-xs hidden-md">+43(0)3383 5100-0 </span> </a><a class="btn" href="https://online-res.com/onlineres.php?lang=de&amp;orv=12&amp;flagfromsystem=1&amp;hid=775&amp;apid=BadBlumau" target="fancybox"><span class="icon-buchen"></span> <span class="hidden-xs">Buchen </span></a><a class="btn" href="/de/gleich-direkt/zimmeranfrage.html"><span class="icon-anfrage"></span> <span class="hidden-xs">Anfragen </span></a><a class="btn" href="/de/gleich-direkt/gutscheine-shop.html"><span class="fa fa-gift"></span> <span class="hidden-xs">Gutscheine &amp; Shop </span></a>
<a class="headmenu" href="/no_cache/de/wunschliste.html" id="merkliste"><span id="anzahl-merkliste">0</span><span class="hidden-xs hidden-sm hidden-md">WUNSCHLISTE</span></a>
</div>
<div class="headmenu mobile-menu"> <a class="btn" href="/de.html#" id="trigger"> <span class="fa fa-bars"></span> </a> </div>
<div class="headmenu suche">
<a class="btn" href="/de/suche.html"><span class="fa fa-search"></span></a>
</div>
<div data-set="menumeta" id="desktopmenu-meta">
<div class="appending">
<div class="headmenu tx-srlanguagemenu tx-srlanguagemenu-links">
<div class="CUR">
<a class="linked-language" href="/de.html">DE</a>
	

                                 | 
							
					</div>
<div class="NO SPC">
<a class="linked-language" href="/en.html">EN</a>
	

                             | 
						</div>
<div class="NO SPC">
<a class="linked-language" href="/it.html">IT</a>
</div>
</div>
<div class="headmenu headmenu-social">
<a class="btn" href="https://www.facebook.com/rognerbadblumau/" target="_blank">
<span class="fa fa-facebook-official"></span>
</a>
<a class="btn" href="https://www.instagram.com/rognerbadblumau/" target="_blank">
<span class="fa fa-instagram"></span>
</a>
<a class="btn" href="mailto:urlaubsschneiderei@rogner.com?subject=Post%20von%20der%20Webseite&amp;body=">
<span class="fa fa-envelope"></span>
</a>
</div>
</div>
</div>
</div>
<a href="/de.html" id="logo"><img alt="" border="0" height="85" src="/fileadmin/templates/grafiken/logo_blumau.png" width="65"/></a>
<ul class="hidden-xs hidden-sm" id="mainmenu-ebene1"><li><a href="/de/ankommen.html" title="ankommen">ankommen</a></li><li><a href="/de/schlafen.html" title="schlafen">schlafen</a></li><li><a href="/de/baden.html" title="baden">baden</a></li><li><a href="/de/spueren.html" title="spüren">spüren</a></li><li><a href="/de/essen.html" title="essen">essen</a></li><li><a href="/de/seminar-feiern.html" title="Seminar &amp; Feiern">Seminar &amp; Feiern</a></li><li><a href="/de/gleich-direkt.html" title="gleich direkt">gleich direkt</a></li><li><a href="/de/menschen.html" title="Menschen">Menschen</a></li></ul>
</div>
</div>
<!-- <img class="print-logo" src="/fileadmin/templates/grafiken/logo_auf_weiss.jpg" /> -->
<div id="main-container">
<a href="https://www.thermencheck.com/award/#t-237" target="_blank"><img alt="" border="0" class="book-now-button" data-pin-nopin="true" height="127" src="/fileadmin/root_blumau/Startseite/Button/127x127px_beliebteste_Therme_Oesterreichs_2018.png" width="127"/></a>
<!--TYPO3SEARCH_begin-->
<!--  CONTENT ELEMENT, uid:912/list [begin] -->
<div class="csc-default" id="c912">
<!--  Header: [begin] -->
<div class="csc-header csc-header-n1"></div>
<!--  Header: [end] -->
<!--  Plugin inserted: [begin] -->
<div class="tx-rogner-content">
<!-- <h1>Single View for Topic</h1> -->
<span id="#375"></span>
<div class="headline headline-teasertext">
<div class="headline-left">
<h1>Sehnsucht nach</h1>
</div>
<div class="headline-right">
<p>Ruhe · Wärme · Geborgenheit</p>
</div>
</div>
<div class="content-interactive">
<div class="nivoGallery" id="nivo-gallery-uid90">
<ul>
<li>
<img alt="" height="525" src="/typo3temp/_processed_/csm_940x525px_slider_winter2020_bca68b0448.jpg" width="940"/>
</li>
<li>
<img alt="" height="525" src="/typo3temp/_processed_/csm_940x525px_slider_winter2020_Goldenekuppel_30be224acd.jpg" width="940"/>
</li>
<li>
<img alt="" height="525" src="/typo3temp/_processed_/csm_940x525px_slider_winter2020_innentherme_785c6dd946.jpg" width="940"/>
</li>
<li>
<img alt="" height="525" src="/typo3temp/_processed_/csm_940x525px_slider_winter2020_Kunsthaus_eae4c5b349.jpg" width="940"/>
</li>
<li>
<img alt="" height="525" src="/typo3temp/_processed_/csm_940x525px_slider_winter2020_schnee_herz_64a6e6d6be.jpg" width="940"/>
</li>
</ul>
</div>
<script type="text/javascript">
	  flowplayer("a.myPlayer-uid90", "/fileadmin/scripts/flowplayer/flowplayer-3.2.16.swf", {
		  clip:  {
			  autoPlay: false,
			  autoBuffering: true
		  }
	  });
	</script>
</div>
<div class="content-1-text">
<div class="content-1-text-left">
<h2>Wichtige Information an unsere Gäste</h2>
<p class="bodytext"><b>Liebe Gäste,</b> </p>
<p class="bodytext"><br/>aufgrund der aktuellen behördlichen Bestimmungen der Bundesregierung zur Eindämmung des Covid-19 Virus wird unsere gesamte Anlage (Hotel, Restaurants, Therme und Spa)<b> geschlossen</b>.</p>
<p class="bodytext"><br/><b>Wir sehen diese Schließung als wichtigen Beitrag zum Schutze der Gesundheit unserer Gäste, MitarbeiterInnen sowie unserer Lieferanten und Partner.</b> </p>
<p class="bodytext"><br/>Wir werden die Zeit nutzen und freuen uns, Sie bald wieder in der mit Abstand beliebtesten Therme Österreichs begrüßen zu dürfen. Bunt, vielseitig und voller Lebensfreude.<br/><br/><b>Wir freuen uns auf ein Wiedersehen voraussichtlich ab 25. Jänner 2021.<br/></b> </p>
<p class="bodytext"><br/>Gerne sind wir in der Zwischenzeit auch für Sie da und freuen uns über Ihre Reservierung. Bei direkter Buchung über unsere UrlaubsberaterInnen bieten wir Ihnen ein Maximum an Flexibilität. Umbuchungen und Stornierungen aufgrund der behördlichen Maßnahmen sind jederzeit kostenfrei möglich.</p>
<p class="bodytext"><b>Wir sind für Sie da</b><br/>Telefon: +43 (0) 3383 / 5100 9449<br/>E-Mail: <a class="mail" href="javascript:linkTo_UnCryptMailto('ocknvq,wtncwduuejpgkfgtgkBtqipgt0eqo');">urlaubsschneiderei(at)rogner.com</a> </p>
<p class="bodytext"><b>TIPP: Bleiben Sie informiert und abonnieren Sie unseren <a class="external-link-new-window" href="https://newsletter.rogner.com/subscription.aspx" target="_blank" title="Website öffnet sich in einem neuen Fenster">Newsletter</a>.</b></p>
</div>
</div>
<div class="content-1-text">
<div class="content-1-text-left">
<h2>Märchenhafte Wasserwelten</h2>
<p class="bodytext">Ruhe und Wärme im größten bewohnbaren Gesamtkunstwerk, gestaltet von Friedensreich Hundertwasser finden. Farbenfroh und fröhlich. Eintauchen in die märchenhafte Welt des Wassers. Sanft getragen von der einzigartigen Vulkania® Heilquelle. Refugien für zwei. Vertrautes neu entdecken. Höchste Qualität. Im Rogner Bad Blumau entsteht ein neues Urlaubsgefühl, voller Lebensfreude. </p>
<p class="bodytext">Unendlich Freiraum im Steirischen Hügelwiesenland. Ausreichend Platz im Hotel und Spa. Abwechslungsreiche und großzügige Restaurants. Regionale Halbpension mit Langschläferfrühstück im Bademantel. Essen wo, wann und wie Sie möchten. Überraschend vielseitig und einfach schön.</p>
<p class="bodytext"><b>Gesamtfläche 420.000m</b><b><b>²</b> · 8.500m² Wellnessbereich · Über 3.000m² Wasserfläche · Ruhe, Rückzug und ganz viel Freiraum sind garantiert</b> </p>
<p class="bodytext">Gerne stehen wir Ihnen für Fragen und Reservierungen jederzeit zur Verfügung.<br/>Telefon: +43 (0) 3383 / 5100 9449<br/>E-Mail: <a class="mail" href="javascript:linkTo_UnCryptMailto('ocknvq,wtncwduuejpgkfgtgkBtqipgt0eqo');">urlaubsschneiderei(at)rogner.com</a></p>
</div>
</div>
<div class="content-2-1-pictext">
<img alt="" class="first" height="335" src="/uploads/tx_rognercontent/470x335px_Nachtbild_Rahmen_gold_Thermencheck.jpg" width="470"/>
<div class="content-2-1-pictext-right">
<h2>Beliebteste Therme Österreichs 2020</h2>
<p class="bodytext"><b>Rogner Bad Blumau die unschlagbare Nummer 1 in Österreich.</b><br/><br/>Das Rogner Bad Blumau wurde bereits zum 6. Mal auf Platz 1 gewählt und sicherte sich in allen 8 Jahren der Wahl, einen Platz unter den Top 3.</p>
</div>
</div>
<div class="content-2-1-pictext">
<img alt="" class="first" height="335" src="/uploads/tx_rognercontent/470x335_Sektbar.jpg" width="470"/>
<div class="content-2-1-pictext-right">
<h2>Lebensfreude feiern 2021</h2>
<p class="bodytext"><span lang="DE-AT" style='font-size:11.0pt; font-family: "Calibri","sans-serif"'>An jedem 24. und an jedem 31. des Monats begrüßen wir Sie mit Rogners Sektbar exklusiv. Wir feiern Sie und mit Ihnen zu jeder Jahreszeit.</span></p>
</div>
</div>
<div class="content-1-1-1-pic">
<div class="pic-1-3 pic-1-3-first">
<a href="https://www.blumau.com/de/baden/zeitzuzweit.html">
<img alt="" class="first" height="335" src="/typo3temp/_processed_/csm_300x335px_Kuschelliege_01_593a8a575b.jpg" width="300"/>
</a>
</div>
<div class="pic-1-3 pic-1-3-middle">
<a href="https://www.blumau.com/de/schlafen/aktuelle-informationen.html">
<img alt="" class="middle" height="335" src="/typo3temp/_processed_/csm_300x335pxaktuelles_neu_02_0b49c2c0e2.jpg" width="300"/>
</a>
</div>
<div class="pic-1-3 pic-1-3-last">
<a href="https://www.blumau.com/de/gleich-direkt/gutscheine-shop.html">
<img alt="" class="last" height="335" src="/typo3temp/_processed_/csm_300x335px_gutscheine_2020_01_c59449ed6d.jpg" width="300"/>
</a>
</div>
</div>
<div class="content-1-1-1-pic">
<div class="pic-1-3 pic-1-3-first">
<a href="https://www.blumau.com/de/baden.html">
<img alt="" class="first" height="335" src="/typo3temp/_processed_/csm_300x335px_Baden___Sauna_4c2ac202ec.jpg" width="300"/>
</a>
</div>
<div class="pic-1-3 pic-1-3-middle">
<a href="https://www.thermencheck.com/award/#t-237" target="_blank">
<img alt="" class="middle" height="335" src="/typo3temp/_processed_/csm_300x335_GIF_899269fc58.gif" width="300"/>
</a>
</div>
<div class="pic-1-3 pic-1-3-last">
<a href="https://www.blumau.com/de/spueren.html">
<img alt="" class="last" height="335" src="/typo3temp/_processed_/csm_300x335px_Gesundheit___Sch%C3%B6nheit_eab1fcd702.jpg" width="300"/>
</a>
</div>
</div>
<div class="content-1-1-1-pic">
<div class="pic-1-3 pic-1-3-first">
<a href="https://www.blumau.com/de/ankommen.html">
<img alt="" class="first" height="335" src="/typo3temp/_processed_/csm_300x335px_ankommen_2020_01_316e26628e.jpg" width="300"/>
</a>
</div>
<div class="pic-1-3 pic-1-3-middle">
<a href="/de/schlafen/angebote/qualitaet.html" target="_blank">
<img alt="" class="middle" height="335" src="/typo3temp/_processed_/csm_300x335px_qualitaet2020_84a3faccac.jpg" width="300"/>
</a>
</div>
<div class="pic-1-3 pic-1-3-last">
<a href="https://www.blumau.com/de/ankommen/besondere-plaetze.html">
<img alt="" class="last" height="335" src="/typo3temp/_processed_/csm_300x335px_entdecken_2020_01_1e848c2e6d.jpg" width="300"/>
</a>
</div>
</div>
<div class="content-2-1-pictext">
<img alt="" class="first" height="335" src="/uploads/tx_rognercontent/470x335px_BNS_2017_05.jpg" width="470"/>
<div class="content-2-1-pictext-right">
<h2>Bewusster Neustart</h2>
<p class="bodytext">Wo Lebensfreude erwacht.<b><b></b></b> </p>
<p class="bodytext"><b><b>Ab € 112,- p.P./Nacht inkl. HP</b></b> </p>
<p class="bodytext">Gültig bis 01. Mai 2021</p>
<a class="btn-default" href="/de/schlafen/angebote/bewussterneustart.html">weiter</a>
</div>
</div>
<div class="content-2-1-pictext">
<img alt="" class="first" height="335" src="/uploads/tx_rognercontent/470x335px_feuerkorb.jpg" width="470"/>
<div class="content-2-1-pictext-right">
<h2>ValentinsZeit</h2>
<p class="bodytext">Feuer der Liebe entfachen.<br/>Eine Liebeserklärung zum Valentinstag. Zauberhafte Augenblicke.</p>
<a class="btn-default" href="/de/schlafen/angebote/valentinszeit.html" target="valentinszeit">weiter</a>
</div>
</div>
<div class="content-2-1-pictext">
<img alt="" class="first" height="525" src="/uploads/tx_rognercontent/940x525px_Augenschlitzhaus_06.jpg" width="940"/>
<div class="content-2-1-pictext-right">
<h2>Augenschlitzhäuser</h2>
<p class="bodytext">Die 13 Appartements in den 3 Augenschlitzhäusern bilden eine kleine Einheit mitten in der Natur. Zwei der Häuser bieten Raum für Gäste mit Hund. Das Frühstück kommt auf Wunsch direkt an die Tür.</p>
<a class="btn-default" href="/de/schlafen/appartements.html">WEITER</a>
</div>
</div>
<div class="content-2-1-pictext">
<img alt="" class="first" height="335" src="/uploads/tx_rognercontent/470x335px_whh_neu2018_2_05.jpg" width="470"/>
<div class="content-2-1-pictext-right">
<h2>Waldhofhaus Rückzugssuiten</h2>
<p class="bodytext">Wohn- und Schlafraum getrennt · Doppelbadewanne · Sonnendurchfluteter Innenhof · Liebevolle Details · Flasche Sekt &amp; Pralinen · Besondere Zeit für Dich &amp; mich.</p>
<a class="btn-default" href="/de/schlafen/angebote/waldhof.html">WEITER</a>
</div>
</div>
<div class="content-2-1-pictext">
<img alt="" class="first" height="525" src="/uploads/tx_rognercontent/940x525px_slider_winter2020_Kunsthaus.jpg" width="940"/>
<div class="content-2-1-pictext-right">
<h2>Ehrlich. Bewusst. Mit Freude.</h2>
<p class="bodytext">Wir vertrauen und leben unsere Werte.</p>
<a class="btn-default" href="/de/menschen/leitbild-vision.html" target="_blank">weiter</a>
</div>
</div>
<div class="content-1-1-1-pic">
<div class="pic-1-3 pic-1-3-first">
<a href="/de/schlafen/angebote/restaurantgenussreich.html" target="_blank">
<img alt="" class="first" height="335" src="/typo3temp/_processed_/csm_300x335px_steak_08_e397f03797.jpg" width="300"/>
</a>
</div>
<div class="pic-1-3 pic-1-3-middle">
<a href="/de/schlafen/angebote.html" target="_blank">
<img alt="" class="middle" height="335" src="/typo3temp/_processed_/csm_300x335px_mehr_angebote_12_7f046498f9.jpg" width="300"/>
</a>
</div>
<div class="pic-1-3 pic-1-3-last">
<a href="/de/schlafen/angebote/lebenswerk.html" target="_blank">
<img alt="" class="last" height="335" src="/typo3temp/_processed_/csm_300x335px_lebenswerk_2018_2_7c726ba981.jpg" width="300"/>
</a>
</div>
</div>
</div>
<!--  Plugin inserted: [end] -->
</div>
<!--  CONTENT ELEMENT, uid:912/list [end] -->
<!--TYPO3SEARCH_end-->
</div>
<div id="social-share-container"></div>
<div id="newsletter-form-container">
<!--  CONTENT ELEMENT, uid:1680/html [begin] -->
<div class="csc-default" id="c1680">
<!--  Raw HTML content: [begin] -->
<div id="newsletter-form">
<h3>Newsletter</h3>
<p style="display:inline-block;color:white;margin:0 10px 0 0;">Melden Sie sich jetzt für unseren Newsletter an.</p>
<a href="https://newsletter.rogner.com" id="request-form-anmelden" style="color:#FFF;text-decoration:none" target="_blank">Anmelden</a>
</div>
<!--  Raw HTML content: [end] -->
</div>
<!--  CONTENT ELEMENT, uid:1680/html [end] -->
</div>
<div id="request-form-container">
<!--  CONTENT ELEMENT, uid:10/html [begin] -->
<div class="csc-default" id="c10">
<!--  Raw HTML content: [begin] -->
<div id="request-form">
<h3>Online Buchung</h3>
<form action="https://online-res.com/onlineres.php?lang=de&amp;orv=12&amp;flagfromsystem=1&amp;hid=775&amp;apid=BadBlumau" id="footer-request" method="get" name="footer-request">
<div class="request-input">
<input id="von" name="von" placeholder="VON" type="text" value=""/>
<br/>
<label class="sr-only" for="von">VON</label>
</div>
<div class="request-input">
<input id="bis" name="bis" placeholder="BIS" type="text" value=""/>
<br/>
<label class="sr-only" for="bis">BIS</label>
</div>
<div class="request-input">
<select id="zimmer" name="zimmer" size="1">
<option value="1">1 Zimmer</option>
<option value="2">2 Zimmer</option>
<option value="3">3 Zimmer</option>
<option value="4">4 Zimmer</option>
</select>
<label class="sr-only" for="zimmer">Zimmer</label>
</div>
<div class="request-input">
<input id="request-form-buchen" name="submit" title="Absenden" type="submit" value="Buchen"/>
</div>
<div class="request-input"><a href="https://www.blumau.com/index.php?id=52" id="request-form-anfragen" onclick="return false;">Anfragen</a></div>
</form>
</div>
<!--  Raw HTML content: [end] -->
</div>
<!--  CONTENT ELEMENT, uid:10/html [end] -->
</div>
<div data-set="menu" id="fullmenu"><div class="appending"><nav id="mp-menu"><div class="mp-level"><a class="mp-back" href="/de.html#"></a><ul class="fullmenu-ebene1"><li><a href="/de/ankommen.html" title="ankommen">ankommen</a><div class="mp-level"><a class="mp-back" href="/de.html#"><span class="sr-only">Eine Ebene höher</span></a><ul class="fullmenu-ebene2"><li><a href="/de/ankommen/entdecken.html" title="Entdecken">Entdecken</a></li><li><a href="/de/ankommen/architektur.html" title="Architektur">Architektur</a></li><li><a href="/de/ankommen/philosophie.html" title="Philosophie">Philosophie</a><div class="mp-level"> <a class="mp-back" href="/de.html#"><span class="sr-only">Eine Ebene höher</span></a><ul class="fullmenu-ebene3"><li><a href="/de/ankommen/philosophie/hundertwasser.html" target="hundertwasser" title="Umsetzung der Philosophie Friedensreich Hundertwassers">Umsetzung der Philosophie Friedensreich Hundertwassers</a></li></ul></div></li><li><a href="/de/ankommen/geothermie.html" title="Geothermie">Geothermie</a></li><li><a href="/de/ankommen/fuehrungen.html" title="Führungen">Führungen</a></li><li><a href="/de/ankommen/besondere-plaetze.html" title="besondere Plätze">besondere Plätze</a></li><li><a href="/de/ankommen/anreise.html" title="Anreise">Anreise</a></li></ul></div></li><li><a href="/de/schlafen.html" title="schlafen">schlafen</a><div class="mp-level"><a class="mp-back" href="/de.html#"><span class="sr-only">Eine Ebene höher</span></a><ul class="fullmenu-ebene2"><li><a href="/de/schlafen/preisuebersicht.html" title="Preisübersicht">Preisübersicht</a></li><li><a href="/de/schlafen/angebote.html" title="Angebote">Angebote</a></li><li><a href="/de/schlafen/doppelzimmer.html" title="Doppelzimmer">Doppelzimmer</a></li><li><a href="/de/schlafen/studios-suiten.html" title="Studios &amp; Suiten">Studios &amp; Suiten</a></li><li><a href="/de/schlafen/appartements.html" title="Appartements">Appartements</a></li><li><a href="/de/schlafen/anfragen.html" title="Anfragen">Anfragen</a></li></ul></div></li><li><a href="/de/baden.html" title="baden">baden</a><div class="mp-level"><a class="mp-back" href="/de.html#"><span class="sr-only">Eine Ebene höher</span></a><ul class="fullmenu-ebene2"><li><a href="/de/baden/preisuebersicht.html" title="Preisübersicht">Preisübersicht</a></li><li><a href="/de/baden/zeitzuzweit.html" target="zeitzuzweit" title="Zeit zu zweit">Zeit zu zweit</a></li><li><a href="/de/baden/ruhe.html" title="Ruhe">Ruhe</a></li><li><a href="/de/baden/wasser.html" title="Wasser">Wasser</a></li><li><a href="/de/baden/vulkania.html" title="Vulkania">Vulkania</a></li><li><a href="/de/baden/sauna.html" title="Sauna">Sauna</a></li><li><a href="/de/baden/alphasphere.html" title="AlphaSphere">AlphaSphere</a></li><li><a href="/de/baden/salzgrotte.html" title="Salzgrotte">Salzgrotte</a></li><li><a href="/de/baden/meeresklima.html" title="Meeresklima">Meeresklima</a></li></ul></div></li><li><a href="/de/spueren.html" title="spüren">spüren</a><div class="mp-level"><a class="mp-back" href="/de.html#"><span class="sr-only">Eine Ebene höher</span></a><ul class="fullmenu-ebene2"><li><a href="/de/spueren/packages.html" title="Packages">Packages</a></li><li><a href="/de/spueren/detox.html" title="Detox">Detox</a></li><li><a href="/de/spueren/massagen.html" title="Massagen">Massagen</a></li><li><a href="/de/spueren/hamam.html" title="Hamam">Hamam</a></li><li><a href="/de/spueren/ayurveda.html" title="Ayurveda">Ayurveda</a></li><li><a href="/de/spueren/undopathie.html" title="Undopathie">Undopathie</a></li><li><a href="/de/spueren/koerper.html" title="Körper">Körper</a></li><li><a href="/de/spueren/gesicht.html" title="Gesicht">Gesicht</a></li><li><a href="/de/spueren/pharmosnatur.html" target="pharmosnatur" title="Pharmos Natur">Pharmos Natur</a></li><li><a href="/de/spueren/hand-fuss.html" title="Hand &amp; Fuß">Hand &amp; Fuß</a></li><li><a href="/de/spueren/rogners-seifenwerkstatt.html" title="Rogners Seifenwerkstatt">Rogners Seifenwerkstatt</a></li><li><a href="/de/spueren/bewegung.html" title="Bewegung">Bewegung</a></li></ul></div></li><li><a href="/de/essen.html" title="essen">essen</a><div class="mp-level"><a class="mp-back" href="/de.html#"><span class="sr-only">Eine Ebene höher</span></a><ul class="fullmenu-ebene2"><li><a href="/de/essen/herbstkulinarik.html" target="herbstkulinarik" title="Winterkulinarik">Winterkulinarik</a></li><li><a href="/de/essen/wo.html" title="Wo">Wo</a></li><li><a href="/de/essen/woher.html" title="Woher">Woher</a></li></ul></div></li><li><a href="/de/seminar-feiern.html" title="Seminar &amp; Feiern">Seminar &amp; Feiern</a><div class="mp-level"><a class="mp-back" href="/de.html#"><span class="sr-only">Eine Ebene höher</span></a><ul class="fullmenu-ebene2"><li><a href="/de/seminar-feiern/seminar.html" title="Seminar">Seminar</a></li><li><a href="/de/seminar-feiern/incentive.html" title="Incentive">Incentive</a></li><li><a href="/de/seminar-feiern/hochzeiten.html" title="Hochzeiten">Hochzeiten</a></li><li><a href="/de/seminar-feiern/feiern.html" title="Feiern">Feiern</a></li></ul></div></li><li><a href="/de/gleich-direkt.html" title="gleich direkt">gleich direkt</a><div class="mp-level"><a class="mp-back" href="/de.html#"><span class="sr-only">Eine Ebene höher</span></a><ul class="fullmenu-ebene2"><li><a href="/de/gleich-direkt/zimmeranfrage.html" title="Zimmeranfrage">Zimmeranfrage</a></li><li><a href="/de/gleich-direkt/gutscheine-shop.html" title="Gutscheine &amp; Shop">Gutscheine &amp; Shop</a></li><li><a href="/de/gleich-direkt/webshop.html" target="_blank" title="Webshop">Webshop</a></li><li><a href="/de/gleich-direkt/my-rognerr.html" target="_blank" title="my rogner®">my rogner®</a></li><li><a href="/de/gleich-direkt/presse.html" title="Presse">Presse</a></li><li><a href="/de/gleich-direkt/downloads.html" title="Downloads">Downloads</a></li></ul></div></li><li><a href="/de/menschen.html" title="Menschen">Menschen</a><div class="mp-level"><a class="mp-back" href="/de.html#"><span class="sr-only">Eine Ebene höher</span></a><ul class="fullmenu-ebene2"><li><a href="/de/menschen/leitbild-vision.html" title="Leitbild &amp; Vision">Leitbild &amp; Vision</a></li><li><a href="/de/menschen/benefits.html" title="Benefits">Benefits</a></li><li><a href="/de/menschen/lehre-praktikum.html" title="Lehre &amp; Praktikum">Lehre &amp; Praktikum</a></li><li><a href="/de/menschen/team.html" title="Team">Team</a></li><li><a href="/de/menschen/offene-stellen.html" title="Offene Stellen">Offene Stellen</a></li></ul></div></li></ul><div data-set="menumeta" id="mobilemenu-meta"> </div></div></nav></div></div>
<div id="legal-meta-social-container">
<p id="legal-information">Copyright © 2013 Rogner Inc. Alle Rechte vorbehalten.</p>
<ul id="metamenu"><li><a href="/de/zimmeranfrage.html">Zimmeranfrage</a></li><li><a href="/de/jobboerse.html" target="fancybox">Jobbörse</a></li><li><a href="/de/kontakt.html">Kontakt</a></li><li><a href="/de/myrogner.html">myrogner</a></li><li><a href="/de/impressum.html">Impressum</a></li><li><a href="/de/datenschutz.html" target="datenschutz">Datenschutz</a></li></ul>
<div id="socialmedia-footer"> <a class="btn" href="https://www.facebook.com/rognerbadblumau/" target="_blank">
<span class="fa fa-facebook-official"></span>
</a>
<a class="btn" href="https://www.instagram.com/rognerbadblumau/" target="_blank">
<span class="fa fa-instagram"></span>
</a>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Load Facebook SDK for JavaScript --> <div id="fb-root"></div> <script>(function(d, s, id) {    var js, fjs = d.getElementsByTagName(s)[0];    if (d.getElementById(id)) return;    js = d.createElement(s); js.id = id;    js.src = "//connect.facebook.net/de_DE/all.js#xfbml=1";    fjs.parentNode.insertBefore(js, fjs);  }(document, 'script', 'facebook-jssdk'));</script><!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(["trackPageView"]);
  _paq.push(["enableLinkTracking"]);

  (function() {
    var u=(("https:" == document.location.protocol) ? "https" : "http") + "://piwik.mittwald.gugler.at/";
    _paq.push(["setTrackerUrl", u+"piwik.php"]);
    _paq.push(["setSiteId", "3"]);
    var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
    g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Piwik Code -->
<div id="tx_cookies">
<div data-hide="0" id="tx_cookies_inner">
<h3>Diese Webseite nutzt Cookies</h3>
<p id="tx_cookies_hint">Diese Webseite nutzt Cookies zur Verbesserung des Erlebnisses unserer Besucher. Indem Sie weiterhin auf dieser Webseite navigieren, erklären Sie sich mit unserer Verwendung von Cookies einverstanden und nehmen die <a href="/?id=datenschutz" style="text-decoration:underline" target="_blank" title="Zu den Datenschutzerklärungen">Datenschutzerklärungen</a> zur Kenntnis.</p>
<form action="/de.html?tx_cookies_main%5Baction%5D=submit&amp;cHash=1054f75cb80bb64b03302c5a777b87f2" id="tx_cookies_hide" method="post">
<div>
<input name="tx_cookies_main[__referrer][@extension]" type="hidden" value="Cookies"/>
<input name="tx_cookies_main[__referrer][@vendor]" type="hidden" value="SBTheke"/>
<input name="tx_cookies_main[__referrer][@controller]" type="hidden" value="Main"/>
<input name="tx_cookies_main[__referrer][@action]" type="hidden" value="cookie"/>
<input name="tx_cookies_main[__referrer][arguments]" type="hidden" value="YTowOnt9d15f7b1381032c7a31699b14a774895a7f2267a1"/>
<input name="tx_cookies_main[__referrer][@request]" type="hidden" value='a:4:{s:10:"@extension";s:7:"Cookies";s:11:"@controller";s:4:"Main";s:7:"@action";s:6:"cookie";s:7:"@vendor";s:7:"SBTheke";}0b57a19883e9d68688f14d3c3539098891cb3652'/>
<input name="tx_cookies_main[__trustedProperties]" type="hidden" value='a:2:{s:4:"hide";i:1;s:6:"submit";i:1;}dfb941c8b005cf22ba8faa45b024e4017349e933'/>
</div>
<input name="tx_cookies_main[hide]" type="hidden" value="1"/>
<input name="tx_cookies_main[submit]" type="submit" value="Ich bin damit einverstanden"/>
</form>
<button id="tx_cookies_close" title="Schließen">Schließen</button>
</div>
</div>
<script src="/typo3temp/compressor/merged-9af2ab8f7d89cc3eefd790f55e370752-b280e0a54001683ec3d92238d20f7a7b.js.gzip?1594697855" type="text/javascript"></script>
</body>
</html>
<!-- Cached page generated 13.01.21 10:29. Expires 14.01.21 10:29 -->
