<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-gb" prefix="og: http://ogp.me/ns#" xml:lang="en-gb" xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
var siteurl='/';
var tmplurl='/templates/ja_ores/';
var isRTL = false;
</script>
<base href="https://www.arcano-ie.com/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="Arcano Pumps, Valves &amp; Fittings Importation" name="description"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>Arcano Pumps, Valves &amp; Fittings Importation</title>
<link href="/index.php?format=feed&amp;type=rss" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="/index.php?format=feed&amp;type=atom" rel="alternate" title="Atom 1.0" type="application/atom+xml"/>
<link href="https://www.arcano-ie.com/index.php/component/search/?Itemid=101&amp;format=opensearch" rel="search" title="Search Arcano Pumps" type="application/opensearchdescription+xml"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="/t3-assets/css_d275f.css" rel="stylesheet" type="text/css"/>
<link href="/t3-assets/css_070c6.css" rel="stylesheet" type="text/css"/>
<link href="/t3-assets/css_c1992.css?v=4" rel="stylesheet" type="text/css"/>
<script class="joomla-script-options new" type="application/json">{"csrf.token":"940b6f3ac77d74ea9da1dbac09fba298","system.paths":{"root":"","base":""}}</script>
<script src="/t3-assets/js_cf5e7.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(window).on('load',  function() {
				new JCaption('img.caption');
			});
	</script>
<!--[if ie]><link href="/plugins/system/jat3/jat3/base-themes/default/css/template-ie.css" type="text/css" rel="stylesheet" /><![endif]-->
<!--[if ie]><link href="/templates/ja_ores/css/template-ie.css" type="text/css" rel="stylesheet" /><![endif]-->
<!--[if ie 7]><link href="/plugins/system/jat3/jat3/base-themes/default/css/template-ie7.css" type="text/css" rel="stylesheet" /><![endif]-->
<!--[if ie 7]><link href="/templates/ja_ores/css/template-ie7.css" type="text/css" rel="stylesheet" /><![endif]-->
<link href="/plugins/system/jat3/jat3/base-themes/default/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<style type="text/css">
/*dynamic css*/

    body.bd .main {width: 980px;}
    body.bd #ja-wrapper {min-width: 980px;}
</style></head>
<body class="bd fs3 com_content" id="bd">
<a id="Top" name="Top"></a>
<div id="ja-wrapper">
<div class="wrap " id="ja-hot-news">
<div class="main clearfix">
<div class="ja-moduletable moduletable clearfix" id="Mod99">
<h3><span>Hot news</span></h3>
<div class="ja-box-ct clearfix">
<div class="ja-headelines-buttons">
<a class="ja-headelines-pre" href="/" onclick="return false;"><span>Previous</span></a>
<a class="ja-headelines-next" href="/" onclick="return false;"><span>Next</span></a>
</div>
<div class="ja-headlines " id="jalh-modid99">
<div id="jahl-wapper-items-jalh-modid99" style="white-space:nowrap; ">
<!-- HEADLINE CONTENT -->
<div class="ja-headlines-item jahl-horizontal" style="visibility:visible">
<a href="/index.php/products/water" title="
Pumps, valves and systems for water applications

As a r"><span>Water...</span>
</a>
</div>
<div class="ja-headlines-item jahl-horizontal" style="visibility:hidden">
<a href="/index.php/products/waste-water" title="
Environmentally aware and efficient – KSB, your strong p"><span>Waste water...</span>
</a>
</div>
<div class="ja-headlines-item jahl-horizontal" style="visibility:hidden">
<a href="/index.php/services/service-spare-parts" title="


Comprehensive services and original spare parts

Bes"><span>Service and Spare Parts...</span>
</a>
</div>
<div class="ja-headlines-item jahl-horizontal" style="visibility:hidden">
<a href="/index.php/84-slideshow/104-our-showroom-in-tripoli" title="..."><span>Our Showroom in Tripoli...</span>
</a>
</div>
<div class="ja-headlines-item jahl-horizontal" style="visibility:hidden">
<a href="/index.php/84-slideshow/103-ksb-pumps-2" title="..."><span>KSB Pumps...</span>
</a>
</div>
<!-- //HEADLINE CONTENT -->
</div>
</div>
<script type="text/javascript">
	//<![CDATA[
	var options = { 
		box:$('jalh-modid99'),
		items: $$('#jalh-modid99 .ja-headlines-item'),
		mode: 'horizontal',
		wrapper:$('jahl-wapper-items-jalh-modid99'),
		buttons:{next: $$('.ja-headelines-next'), previous: $$('.ja-headelines-pre')},
		interval:5000,
		fxOptions : { 
			duration: 500,
			transition: Fx.Transitions.linear ,
			wait: false,
			link: 'cancel' 
		}	
	};

	var jahl = new JANewSticker( options );
	//]]>
</script> </div>
</div>
</div>
</div>
<div class="wrap " id="ja-header">
<div class="main">
<div class="main-inner1 clearfix">
<h1 class="logo">
<a href="/index.php" title="Arcano Pumps"><span>Arcano Pumps</span></a>
</h1>
<div id="ja-search">
<form action="/index.php" method="post">
<div class="search">
<label for="mod-search-searchword">Search ...</label><input class="inputbox" id="mod-search-searchword" maxlength="200" name="searchword" onblur="if (this.value=='') this.value='Search ...';" onfocus="if (this.value=='Search ...') this.value='';" size="20" type="text" value="Search ..."/><input class="button" onclick="this.form.searchword.focus();" type="submit" value="Search"/> <input name="task" type="hidden" value="search"/>
<input name="option" type="hidden" value="com_search"/>
<input name="Itemid" type="hidden" value="101"/>
</div>
</form>
</div>
</div>
</div>
</div><div class="wrap " id="ja-mainnav">
<div class="main">
<div class="main-inner1 clearfix">
<div class="ja-megamenu clearfix" id="ja-megamenu">
<ul class="megamenu level0"><li class="mega first active"><a class="mega first active" href="https://www.arcano-ie.com/" id="menu101" title="Home"><span class="menu-title">Home</span></a></li><li class="mega"><a class="mega" href="/index.php/about-us" id="menu602" title="About us"><span class="menu-title">About us</span></a></li><li class="mega haschild"><a class="mega haschild" href="#" id="menu603" title="Products"><span class="menu-title">Products</span></a><div class="childcontent cols1 ">
<div class="childcontent-inner-wrap">
<div class="childcontent-inner clearfix"><div class="megacol column1 first" style="width: 200px;"><ul class="megamenu level1"><li class="mega first"><a class="mega first" href="/index.php/products/water" id="menu604" title="Water"><span class="menu-title">Water</span></a></li><li class="mega"><a class="mega" href="/index.php/products/energy" id="menu605" title="Energy"><span class="menu-title">Energy</span></a></li><li class="mega"><a class="mega" href="/index.php/products/industry" id="menu606" title="Industry"><span class="menu-title">Industry</span></a></li><li class="mega"><a class="mega" href="/index.php/products/building-services" id="menu607" title="Building Services"><span class="menu-title">Building Services</span></a></li><li class="mega last"><a class="mega last" href="/index.php/products/waste-water" id="menu608" title="waste-water"><span class="menu-title">waste-water</span></a></li></ul></div></div>
</div></div></li><li class="mega haschild"><a class="mega haschild" href="#" id="menu609" title="services"><span class="menu-title">services</span></a><div class="childcontent cols1 ">
<div class="childcontent-inner-wrap">
<div class="childcontent-inner clearfix"><div class="megacol column1 first" style="width: 200px;"><ul class="megamenu level1"><li class="mega first"><a class="mega first" href="/index.php/services/service-spare-parts" id="menu610" title="Service &amp; Spare Parts"><span class="menu-title">Service &amp; Spare Parts</span></a></li></ul></div></div>
</div></div></li><li class="mega last"><a class="mega last" href="/index.php/contact-us" id="menu611" title="Contact us"><span class="menu-title">Contact us</span></a></li></ul>
</div> <script type="text/javascript">
                var megamenu = new jaMegaMenuMoo ('ja-megamenu', {
                    'bgopacity': 0,
                    'delayHide': 300,
                    'slide'    : 0,
                    'fading'   : 0,
                    'direction': 'down',
                    'action'   : 'mouseover',
                    'tips'     : false,
                    'duration' : 300,
                    'hidestyle': 'fastwhenshow'
                });
            </script>
</div>
</div>
</div>
<ul class="no-display">
<li><a href="#ja-content" title="Skip to content">Skip to content</a></li>
</ul> <div class="wrap " id="ja-slideshow">
<div class="wrap-inner1">
<div class="wrap-inner2">
<div class="main clearfix">
<div class="ja-moduletable moduletable clearfix" id="Mod121">
<div class="ja-box-ct clearfix">
<div class="ja-slidewrap_vintase ja-articles" id="ja-slide-articles-121" style="visibility:hidden">
<div class="ja-slide-main-wrap">
<div class="ja-slide-mask">
</div>
<div class="ja-slide-main">
<div class="ja-slide-item">
<img alt="Our Showroom in Tripoli" src="/images/h1.jpg" title="Our Showroom in Tripoli"/> </div>
<div class="ja-slide-item">
<img alt="KSB Pumps" src="/images/h4.jpg" title="KSB Pumps"/> </div>
<div class="ja-slide-item">
<img alt="KSB Pumps" src="/images/h2.jpg" title="KSB Pumps"/> </div>
</div>
<div class="ja-slide-progress"></div>
<div class="ja-slide-loader"></div>
<!-- JA SLIDESHOW 3 MARK -->
<div class="maskDesc">
<div class="inner">
</div>
</div>
</div>
<!-- END JA SLIDESHOW 3 MARK -->
<!-- JA SLIDESHOW 3 NAVIAGATION -->
<div class="ja-slide-thumbs-wrap ja-horizontal">
<div class="ja-slide-thumbs">
<div class="ja-slide-thumb">
<div class="ja-slide-thumb-inner">
<h3>Our Showroom in Tripoli</h3>
</div>
</div>
<div class="ja-slide-thumb">
<div class="ja-slide-thumb-inner">
<h3>KSB Pumps</h3>
</div>
</div>
<div class="ja-slide-thumb">
<div class="ja-slide-thumb-inner">
<h3>KSB Pumps</h3>
</div>
</div>
</div>
<div class="ja-slide-thumbs-mask" style=" display:none ">
<div class="ja-slide-thumbs-mask-left"> </div>
<div class="ja-slide-thumbs-mask-center"> </div>
<div class="ja-slide-thumbs-mask-right"> </div>
</div>
<p class="ja-slide-thumbs-handles">
<span> </span>
<span> </span>
<span> </span>
</p>
<!-- JA SLIDESHOW 3 NAVIAGATION -->
</div>
<!-- JA SLIDESHOW 3 BUTTONS CONTROL -->
</div>
<script type="text/javascript">
	var Ja_direction = '';
	var cookie_path = '/';
	var cur_template_name = 'ja_ores';
	window.jasliderInst = window.jasliderInst || [];
	
	window.addEvent('domready', function(){
		var Ja_maskAlign_121= 'bottom';
		if(typeof(tmpl_name) =='undefined'){
			cookie_path = "ja_ores_direction";
		} else{
			cookie_path = tmpl_name+"_direction";
		}
		
		Ja_direction = Cookie.read(cookie_path);
		if( Ja_direction == '' || Ja_direction == null){
			Ja_direction = 'ltr';
		}
		var style_l_value = 'auto';
		if(cur_template_name == 'ja_norite'){
			style_l_value = '0';
		}
		if(Ja_direction == 'rtl'){
			setStyleLinkWithRTLDirection();
			$('ja-slide-articles-121').getElement(".ja-slide-main").setStyle('left',style_l_value);
			$('ja-slide-articles-121').getElement(".ja-slide-main").setStyle('right','auto');
			if(Ja_maskAlign_121 == 'right'){
				Ja_maskAlign_121 = 'left';
			}
			else if(Ja_maskAlign_121 == 'left'){
				Ja_maskAlign_121 = 'right';
			}
		}

		window.jasliderInst.push(new JASlider('ja-slide-articles-121', {
			slices: 8,
			boxCols: 8,
			boxRows: 4,
			
			animation: 'fade',
			fbanim: 'move',
			direction: 'horizontal',
			
			interval: 5000,
			duration: 400,
			transition: Fx.Transitions.linear,
			
			repeat: 'true',
			autoPlay: 1,
			
			mainWidth: 960,
			mainHeight: 240,
			
			rtl:( typeof Ja_direction == 'string') ? Ja_direction : '',
			
			startItem: 0,
			
			thumbItems: 3,
			thumbType: 'thumbs',
			thumbWidth: 0,
			thumbHeight: 0,
			thumbSpaces: [0,0],
			thumbOpacity: 0.8,
			thumbTrigger: 'click',
			thumbOrientation: 'horizontal',
			
			
			maskStyle: 1,
			maskWidth: 180,
			maskHeigth:300,
			maskOpacity: 1,
			maskAlign: Ja_maskAlign_121,
			maskTransitionStyle: 'opacity',
			maskTransition: Fx.Transitions.linear,
			
			showDesc: '',
			descTrigger: 'always',
			edgemargin:0,
			showControl: 0,
			
			showNavBtn: true,
			navBtnOpacity: 0.4,
			navBtnTrigger: 'click',
			
			showProgress: 0,
			
			urls:['/index.php/84-slideshow/104-our-showroom-in-tripoli','/index.php/84-slideshow/103-ksb-pumps-2','/index.php/84-slideshow/102-ksb-pumps'],
			targets:['target="_parent"','target="_parent"','target="_parent"']
		}));
	});
</script>
<script type="text/javascript">
	function setStyleLinkWithRTLDirection()
	{
		var links = document.getElementsByTagName ('link');
				
		var script = document.createElement('link');
		script.setAttribute('type', 'text/css');
		script.setAttribute('rel', 'stylesheet');
		script.setAttribute('href', 'https://www.arcano-ie.com/modules/mod_jaslideshow/assets/themes/default/mod_jaslideshow_rtl.css');
		document.getElementsByTagName("head")[0].appendChild(script);
	}
</script> </div>
</div>
</div>
</div>
</div>
</div>
<div class="wrap " id="ja-topsl">
<div class="main">
<div class="main-inner1 clearfix">
<!-- SPOTLIGHT -->
<div class="ja-box column ja-box-left" style="width: 25%;">
<div class="ja-moduletable moduletable clearfix" id="Mod125">
<div class="ja-box-ct clearfix">
<div class="custom">
<p><img align="left" alt="planning" border="0" class="caption" src="/images/stories/demo/sam-1.jpg"/></p>
<h3 style="font-size: 22px;">Water &amp; Waste water</h3>
<p> </p>
<p>KSB provides innovative technology for water extraction and groundwater management. Our products move water reliably and efficiently.</p></div>
</div>
</div>
</div>
<div class="ja-box column ja-box-center" style="width: 25%;">
<div class="ja-moduletable moduletable clearfix" id="Mod122">
<div class="ja-box-ct clearfix">
<div class="custom">
<p><img align="left" alt="planning" border="0" class="caption" src="/images/stories/demo/sam-2.jpg"/></p>
<h3 style="font-size: 22px;">Energy</h3>
<p> </p>
<p>Reliable power station pumps and high-pressure valves make KSB market leader the world over</p></div>
</div>
</div>
</div>
<div class="ja-box column ja-box-center" style="width: 25%;">
<div class="ja-moduletable moduletable clearfix" id="Mod123">
<div class="ja-box-ct clearfix">
<div class="custom">
<p><img align="left" alt="planning" border="0" class="caption" src="/images/stories/demo/sam-3.jpg"/></p>
<h3 style="font-size: 22px;">Building Services</h3>
<p> </p>
<p>KSB offers comprehensive know-how for economical all-in solutions including pumps, valves and systems for building services.</p></div>
</div>
</div>
</div>
<div class="ja-box column ja-box-right" style="width: 25%;">
<div class="ja-moduletable moduletable clearfix" id="Mod124">
<div class="ja-box-ct clearfix">
<div class="custom">
<p><img align="left" alt="planning" border="0" class="caption" src="/images/stories/demo/sam-4.jpg"/></p>
<h3 style="font-size: 22px;">industry</h3>
<p> </p>
<p>For industrial applications KSB combines innovative technology with outstanding service. KSB’s industrial pumps and valves cover almost any application</p></div>
</div>
</div>
</div>
<!-- SPOTLIGHT -->
<script type="text/javascript">
    window.addEvent('load', function (){ equalHeight ('#ja-topsl .ja-box') });
</script> </div>
</div>
</div>
<!-- MAIN CONTAINER -->
<div class="wrap ja-r1" id="ja-container">
<div class="main clearfix">
<div id="ja-mainbody" style="width:75%">
<!-- CONTENT -->
<div id="ja-main" style="width:100%">
<div class="inner clearfix">
<div id="system-message-container">
</div>
<div class="clearfix " id="ja-contentwrap">
<div class="column" id="ja-content" style="width:100%">
<div class="column" id="ja-current-content" style="width:100%">
<div class="ja-content-main clearfix" id="ja-content-main">
<div class="blog-featured">
<div class="items-leading">
<div class="leading leading-0 clearfix">
<div class="contentpaneopen">
<h2 class="contentheading">
					Arcano Company			</h2>
<div class="article-tools clearfix">
</div>
<h2><em>Better Life for a Brighter Tomorrow ..</em></h2>
<p>Arcano Pumps, Valves &amp; Fittings Importation is the Sole agent of KSB pumps in Libya</p>
<p>Our company represents innovative products with high quality, safety and reliability.</p>
<p>We achieve maximum customer satisfaction through a customer-oriented sales and after-sales service with highly specialised staff.</p>
<p class="readmore">
<a href="/index.php/about-us">
					Read more: Arcano Company</a>
</p>
</div>
<div class="item-separator"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- //CONTENT -->
</div>
<!-- RIGHT COLUMN-->
<div class="column sidebar" id="ja-right" style="width:25%">
<div class="ja-colswrap clearfix ja-r1">
<div class="ja-col column" id="ja-right2" style="width:100%">
<div class="ja-moduletable moduletable clearfix" id="Mod126">
<h3><span>Our Clients</span></h3>
<div class="ja-box-ct clearfix">
<div class="custom">
<div class="ja-gallery clearfix">
<div class="ja-thumb"><a href="#"><img alt="Sample image" border="0" src="/images/stories/demo/gallery/thumb-1.jpg"/></a></div>
<div class="ja-thumb last"><a href="#"><img alt="Sample image" border="0" class="last-image" src="/images/stories/demo/gallery/thumb-2.jpg"/></a></div>
<div class="ja-thumb"><a href="#"><img alt="Sample image" border="0" src="/images/stories/demo/gallery/thumb-3.jpg"/></a></div>
<div class="ja-thumb last"><a href="#"><img alt="Sample image" border="0" class="last-image" src="/images/stories/demo/gallery/thumb-4.jpg"/></a></div>
</div></div>
</div>
</div>
</div>
</div>
</div>
<!-- //RIGHT COLUMN-->
</div>
</div>
<!-- //MAIN CONTAINER -->
<div class="wrap " id="ja-navhelper">
<div class="main">
<div class="main-inner1 clearfix">
<div class="ja-breadcrums">
<span class="breadcrumbs pathway">
<strong>You are here: </strong>Home</span>
</div>
<ul class="ja-links">
<li class="top"><a href="#Top" title="Back to top">Top</a></li>
</ul>
<ul class="no-display">
<li><a href="#ja-content" title="Skip to content">Skip to content</a></li>
</ul> </div>
</div>
</div>
<div class="wrap " id="ja-footer">
<div class="main clearfix">
<div class="main">
<div class="inner clearfix">
<div class="ja-copyright">
<div class="custom">
<p style="text-align: center;"><small>Copyright © 2018 Arcano Pumps. Valevs &amp; Fittings Impotation . All Rights Reserved. Designed by <a href="http://www.3rdhub.com/" rel="noopener noreferrer" target="_blank" title="Visit 3rdHub.com!">3rd Hub</a>.</small></p></div>
</div>
<div class="ja-footnav">
</div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>