<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="tr" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Atatürk Araştırma Merkezi</title>
<!--Start Meta-->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=8" http-equiv="X-UA-Compatible"/>
<meta content="Atatürk Araştırma Merkezi Başkanlığı" name="description"/>
<meta content="ATAM, Atatürk Araştırmaları Merkezi" name="keywords"/>
<meta content="width=device-width, user-scalable=yes" name="viewport"/>
<!--End Meta-->
<!--Start Stylesheet-->
<link href="https://www.atam.gov.tr/wp-content/themes/v1/style.css?ver=1.0.4" media="screen" rel="stylesheet" type="text/css"/>
<!--End Stylesheet-->
<!--Start Miscellaneous-->
<link href="https://www.atam.gov.tr/feed" rel="alternate" title="Atatürk Araştırma Merkezi RSS Feed" type="application/rss+xml"/>
<link href="https://www.atam.gov.tr/feed/rss" rel="alternate" title="RSS .92" type="text/xml"/>
<link href="https://www.atam.gov.tr/feed/atom" rel="alternate" title="Atom 0.3" type="application/atom+xml"/>
<link href="https://www.atam.gov.tr/xmlrpc.php" rel="pingback"/>
<link href="https://www.atam.gov.tr/wp-content/themes/v1/images/favicon.png" rel="Shortcut icon" type="image/png"/>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,700&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
<!--End Miscellaneous-->
<!--Start Javascript-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script>
<script src="https://www.atam.gov.tr/wp-content/themes/v1/js/slider.js" type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<!--	<script>
		$(function() {
			$( "#home" ).tabs();
			$("#home li.un a").unbind('click').each(function() {
  this.href = 'http://www.atam.gov.tr/faaliyetler/raporlar';
});
		});
	</script>
-->
<script type="text/javascript">
		$(document).ready(function() {
			$('ul#filter a').click(function() {
				$(this).css('outline','none');
				$('ul#filter .current').removeClass('current');
				$(this).parent().addClass('current');
				
				var filterVal = $(this).text().toLowerCase().replace(' ','-');
						
				if(filterVal == 'tümü') {
					$('ul#post li.hidden').fadeIn('slow').removeClass('hidden');
				} else {
					$('ul#post li').each(function() {
						if(!$(this).hasClass(filterVal)) {
							$(this).fadeOut('normal').addClass('hidden');
						} else {
							$(this).fadeIn('slow').removeClass('hidden');
						}
					});
				}
				
				return false;
			});
		});
	</script>
<!--[if lt IE 8]>
	<script>
		$(function() {
			$("#picker ul li:last-child").addClass('last-child');
			$("#picker ul li:first-child").addClass('first-child');
			$("#menu ul li:last-child").addClass('last-child');
		});
	</script>
	<![endif]-->
<!--[if IE 8]>
	<script>
		$(function() {
			$("#picker ul li:last-child").addClass('last-child');
			$("#picker ul li:first-child").addClass('first-child');
			$("#menu ul li:last-child").addClass('last-child');
		});
	</script>
	<![endif]-->
<!--[if IE 9]>
	<script>
		$(function() {
			$("#picker ul li:last-child").addClass('last-child');
			$("#picker ul li:first-child").addClass('first-child');
			$("#menu ul li:last-child").addClass('last-child');
		});
	</script>
	<![endif]-->
<!--[if gt IE 9]>
	<script>
		$(function() {
			$("#picker ul li:last-child").addClass('last-child');
			$("#picker ul li:first-child").addClass('first-child');
			$("#menu ul li:last-child").addClass('last-child');
		});
	</script>
	<![endif]-->
<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-1358117-1', 'atam.gov.tr');
	  ga('send', 'pageview');

	</script>
<!--End Javascript-->
<link href="//s.w.org" rel="dns-prefetch"/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.10.4 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script data-cfasync="false" type="text/javascript">
	var mi_version         = '7.10.4';
	var mi_track_user      = true;
	var mi_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-48908759-7';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-48908759-7', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('require', 'displayfeatures');
		__gaTracker('send','pageview');
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<!-- / Google Analytics by MonsterInsights -->
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.atam.gov.tr\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.atam.gov.tr/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.atam.gov.tr/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.9" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.atam.gov.tr/wp-content/plugins/wp-pagenavi/pagenavi-css.css?ver=2.70" id="wp-pagenavi-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://www.atam.gov.tr/wp-content/plugins/media-element-html5-video-and-audio-player/mediaelement/v4/mediaelementplayer.min.css?ver=5.4.4" id="mediaelementjs-styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://www.atam.gov.tr/wp-content/plugins/media-element-html5-video-and-audio-player/mediaelement/v4/mediaelementplayer-legacy.min.css?ver=5.4.4" id="mediaelementjs-styles-legacy-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.atam.gov.tr/wp-content/plugins/lightbox-plus/css/shadowed/colorbox.css?ver=2.6" id="lightboxStyle-css" media="screen" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[]","home_url":"https:\/\/www.atam.gov.tr","hash_tracking":"false"};
/* ]]> */
</script>
<script src="https://www.atam.gov.tr/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.10.4" type="text/javascript"></script>
<script src="https://www.atam.gov.tr/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.atam.gov.tr/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="http://www.atam.gov.tr/wp-content/plugins/media-element-html5-video-and-audio-player/mediaelement/v4/mediaelement-and-player.min.js?ver=4.2.8" type="text/javascript"></script>
<link href="https://www.atam.gov.tr/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.atam.gov.tr/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.atam.gov.tr/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.4" name="generator"/>
</head>
<body>
<!-- Start Header -->
<div id="header">
<div class="wrap">
<div id="logo">
<a href="https://www.atam.gov.tr"><img src="https://www.atam.gov.tr/wp-content/themes/v1/images/ataturk-arastirma-merkezi.png"/></a>
</div>
<div id="ayk">
<a href="http://www.ayk.gov.tr" target="_blank"><img src="/wp-content/uploads/ayk_head2.png"/></a>
</div>
<div id="menu">
<ul>
<li class="home"><a href="https://www.atam.gov.tr"><img src="https://www.atam.gov.tr/wp-content/themes/v1/images/home-button.png"/></a></li>
<li><a href="https://www.atam.gov.tr/kurumsal">Kurumsal</a>
<ul class="submenu">
<li><a href="https://www.atam.gov.tr/kurumsal/tarihce-2">Tarihçe</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal/misyon-vizyon">Misyon, Vizyon</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal/teskilat-semasi">Teşkilat Şeması</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal/yonetim">Yönetim</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal/bilim-kurulu">Bilim Kurulu</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal/bilim-uygulama-kollari">Bilim Uygulama Kolları</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal/komisyonlar">Komisyonlar</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal/eski-baskanlar">Eski Başkanlar</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal/uyeler">Üyeler</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal/kamu-hizmetleri-envanteri">Kamu Hizmetleri Envanteri</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal/mevzuat">Mevzuat</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal/iletisim">İletişim</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal/kam-gor-etik-sozlesmesi">Kam. Gör. Etik Sözleşmesi</a></li>
</ul>
</li>
<li><a href="https://www.atam.gov.tr/faaliyetler">Faaliyetler</a>
<ul class="submenu">
<li><a href="https://www.atam.gov.tr/faaliyetler/bilimsel-ve-kulturel-etkinlikler">Bilimsel ve Kültürel Etkinlikler</a></li>
<li><a href="https://www.atam.gov.tr/faaliyetler/yayinlar">Yayınlar</a></li>
<li><a href="https://www.atam.gov.tr/faaliyetler/projeler">Projeler</a></li>
<li><a href="https://www.atam.gov.tr/faaliyetler/oduller">Ödüller</a></li>
<li><a href="https://www.atam.gov.tr/faaliyetler/burslar">Burslar</a></li>
<li><a href="https://www.atam.gov.tr/faaliyetler/raporlar">Stratejik Plan ve Raporlar</a></li>
</ul>
</li>
<li><a href="https://www.atam.gov.tr/kutuphane">Kütüphane</a>
<ul class="submenu">
<li><a href="https://www.atam.gov.tr/kutuphane">Tanıtım</a></li>
<li><a href="http://katalog.ayk.gov.tr" target="_blank">AYK Toplu Katalog</a></li>
<li><a href="http://kutuphane.atam.gov.tr/opac_atam" target="_blank">Katalog Tarama</a></li>
</ul>
</li>
<li><a href="https://www.atam.gov.tr">Multimedya Galeri</a>
<ul class="submenu">
<li><a href="https://www.atam.gov.tr/fotograflar">Fotoğraflar</a></li>
</ul>
</li>
<li><a href="#">Dergi</a>
<ul class="submenu">
<li><a href="/faaliyetler/yayinlar/dergi">ATAM Dergisi</a></li>
<li><a href="/atam-dergisi">Sayılar</a></li>
</ul>
</li>
<li><a href="https://www.atam.gov.tr/kurumsal/iletisim">İletişim</a>
<ul class="submenu">
<li><a href="https://www.atam.gov.tr/kurumsal/iletisim">İletişim</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal/iletisim/telefon-rehberi">Telefon Rehberi</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal/iletisim/bilgi-edinme">Bilgi Edinme</a></li>
</ul>
</li>
<li class="store"><a href="http://e-magaza.atam.gov.tr/" target="_blank">E-mağaza</a></li>
</ul>
</div>
</div>
</div>
<!-- End Header -->
<!-- Start Slider -->
<div id="slider">
<div class="wrap">
<div id="slide">
<div class="content">
<a href="https://www.atam.gov.tr/duyurular/lisansustu-burs-sonuc-duyurusu" style="background: url(https://www.atam.gov.tr/wp-content/uploads/WhatsApp-Image-2021-01-11-at-14.34.42-3.jpeg); background-size: 100% auto;">
<div class="title">
<h2>Lisansüstü Burs Sonuç Duyurusu</h2>
<p>detaylar</p>
</div>
</a>
</div>
<div class="content">
<a href="https://www.atam.gov.tr/duyurular/2021-yili-burs-mulakati-duyurusu" style="background: url(https://www.atam.gov.tr/wp-content/uploads/Adsız-27.png); background-size: 100% auto;">
<div class="title">
<h2>2021 Yılı Eğitim Bursu Mülakat Duyurusu</h2>
<p>detaylar</p>
</div>
</a>
</div>
<div class="content">
<a href="https://www.atam.gov.tr/duyurular/2021-yili-burs-duyurusu" style="background: url(https://www.atam.gov.tr/wp-content/uploads/Adsız-24-e1606224440230.png); background-size: 100% auto;">
<div class="title">
<h2>2021 Yılı Burs Duyurusu</h2>
<p>detaylar</p>
</div>
</a>
</div>
<div class="content">
<a href="https://www.atam.gov.tr/duyurular/24-kasim-ogretmenler-gunu-kutlu-olsun" style="background: url(https://www.atam.gov.tr/wp-content/uploads/24-Kasım-Site.png); background-size: 100% auto;">
<div class="title">
<h2>24 Kasım Öğretmenler Günü Kutlu Olsun!</h2>
<p>detaylar</p>
</div>
</a>
</div>
<div class="content">
<a href="https://www.atam.gov.tr/duyurular/gazi-mustafa-kemal-ataturku-vefatinin-82-yil-donumunde-saygi-ve-minnetle-aniyoruz" style="background: url(https://www.atam.gov.tr/wp-content/uploads/Site10-kasımm.png); background-size: 100% auto;">
<div class="title">
<h2>Gazi Mustafa Kemal Atatürk’ü Vefatının 82. Yıl Dönümünde Saygı ve Minnetle Anıyoruz</h2>
<p>detaylar</p>
</div>
</a>
</div>
<div class="content">
<a href="https://www.atam.gov.tr/duyurular/ataturk-ansiklopedisi-dijital-ortamda-erisime-acildi" style="background: url(https://www.atam.gov.tr/wp-content/uploads/Site-ansiklopedi-2.png); background-size: 100% auto;">
<div class="title">
<h2>Atatürk Ansiklopedisi Dijital Ortamda Erişime Açıldı</h2>
<p>detaylar</p>
</div>
</a>
</div>
</div>
<div id="picker">
<ul>
<li class="menuItem"><a href="https://www.atam.gov.tr/duyurular/lisansustu-burs-sonuc-duyurusu"><img alt="" class="attachment-full size-full wp-post-image" height="365" sizes="(max-width: 1024px) 100vw, 1024px" src="https://www.atam.gov.tr/wp-content/uploads/WhatsApp-Image-2021-01-11-at-14.34.42-3.jpeg" srcset="https://www.atam.gov.tr/wp-content/uploads/WhatsApp-Image-2021-01-11-at-14.34.42-3.jpeg 1024w, https://www.atam.gov.tr/wp-content/uploads/WhatsApp-Image-2021-01-11-at-14.34.42-3-300x107.jpeg 300w, https://www.atam.gov.tr/wp-content/uploads/WhatsApp-Image-2021-01-11-at-14.34.42-3-768x274.jpeg 768w" width="1024"/></a></li>
<li class="menuItem"><a href="https://www.atam.gov.tr/duyurular/2021-yili-burs-mulakati-duyurusu"><img alt="" class="attachment-full size-full wp-post-image" height="3552" sizes="(max-width: 7008px) 100vw, 7008px" src="https://www.atam.gov.tr/wp-content/uploads/Adsız-27.png" srcset="https://www.atam.gov.tr/wp-content/uploads/Adsız-27.png 7008w, https://www.atam.gov.tr/wp-content/uploads/Adsız-27-300x152.png 300w, https://www.atam.gov.tr/wp-content/uploads/Adsız-27-1024x519.png 1024w, https://www.atam.gov.tr/wp-content/uploads/Adsız-27-768x389.png 768w, https://www.atam.gov.tr/wp-content/uploads/Adsız-27-1536x779.png 1536w, https://www.atam.gov.tr/wp-content/uploads/Adsız-27-2048x1038.png 2048w" width="7008"/></a></li>
<li class="menuItem"><a href="https://www.atam.gov.tr/duyurular/2021-yili-burs-duyurusu"><img alt="" class="attachment-full size-full wp-post-image" height="3556" sizes="(max-width: 6850px) 100vw, 6850px" src="https://www.atam.gov.tr/wp-content/uploads/Adsız-24-e1606224440230.png" srcset="https://www.atam.gov.tr/wp-content/uploads/Adsız-24-e1606224440230.png 6850w, https://www.atam.gov.tr/wp-content/uploads/Adsız-24-e1606224440230-300x156.png 300w, https://www.atam.gov.tr/wp-content/uploads/Adsız-24-e1606224440230-1024x532.png 1024w, https://www.atam.gov.tr/wp-content/uploads/Adsız-24-e1606224440230-768x399.png 768w, https://www.atam.gov.tr/wp-content/uploads/Adsız-24-e1606224440230-1536x797.png 1536w, https://www.atam.gov.tr/wp-content/uploads/Adsız-24-e1606224440230-2048x1063.png 2048w" width="6850"/></a></li>
<li class="menuItem"><a href="https://www.atam.gov.tr/duyurular/24-kasim-ogretmenler-gunu-kutlu-olsun"><img alt="" class="attachment-full size-full wp-post-image" height="365" sizes="(max-width: 1024px) 100vw, 1024px" src="https://www.atam.gov.tr/wp-content/uploads/24-Kasım-Site.png" srcset="https://www.atam.gov.tr/wp-content/uploads/24-Kasım-Site.png 1024w, https://www.atam.gov.tr/wp-content/uploads/24-Kasım-Site-300x107.png 300w, https://www.atam.gov.tr/wp-content/uploads/24-Kasım-Site-768x274.png 768w" width="1024"/></a></li>
<li class="menuItem"><a href="https://www.atam.gov.tr/duyurular/gazi-mustafa-kemal-ataturku-vefatinin-82-yil-donumunde-saygi-ve-minnetle-aniyoruz"><img alt="" class="attachment-full size-full wp-post-image" height="365" sizes="(max-width: 1024px) 100vw, 1024px" src="https://www.atam.gov.tr/wp-content/uploads/Site10-kasımm.png" srcset="https://www.atam.gov.tr/wp-content/uploads/Site10-kasımm.png 1024w, https://www.atam.gov.tr/wp-content/uploads/Site10-kasımm-300x107.png 300w, https://www.atam.gov.tr/wp-content/uploads/Site10-kasımm-768x274.png 768w" width="1024"/></a></li>
<li class="menuItem"><a href="https://www.atam.gov.tr/duyurular/ataturk-ansiklopedisi-dijital-ortamda-erisime-acildi"><img alt="" class="attachment-full size-full wp-post-image" height="824" sizes="(max-width: 1024px) 100vw, 1024px" src="https://www.atam.gov.tr/wp-content/uploads/Site-ansiklopedi-2.png" srcset="https://www.atam.gov.tr/wp-content/uploads/Site-ansiklopedi-2.png 1024w, https://www.atam.gov.tr/wp-content/uploads/Site-ansiklopedi-2-300x241.png 300w, https://www.atam.gov.tr/wp-content/uploads/Site-ansiklopedi-2-768x618.png 768w" width="1024"/></a></li>
</ul>
</div>
</div>
</div>
<!-- End Slider -->
<!-- Start Content -->
<div id="home">
<div class="wrap">
<div class="left-column">
<div id="left-nav">
<ul>
<li><a class="ournews" href="https://ataturkansiklopedisi.gov.tr" target="_blank">Atatürk Ansiklopedisi</a></li>
<li><a class="ournews" href="/haberler">Kurumdan Haberler</a></li>
<li><a class="books" href="/kitaplar">Kitaplar</a></li>
<li><a class="books" href="/turkiye-cumhuriyeti-tarihi-veri-tabani">E-Yayınlarımız</a></li>
<li><a class="magazines" href="/atam-dergisi">Dergi</a></li>
<li><a class="news" href="/duyurular">Duyurular</a></li>
<li><a class="photos" href="/fotograflar">Fotoğraflar</a></li>
<li class="un"><a class="books" href="https://www.atam.gov.tr/faaliyetler/raporlar">Raporlar</a></li>
<li><a class="books" href="https://yaysis.ayk.gov.tr/">Eser/Makale Başvurusu</a></li>
</ul>
</div>
<form action="https://www.atam.gov.tr/" id="searchform" method="get">
<div id="search">
<input id="s" name="s" onblur="if (this.value == '') {this.value = 'arama yap...';}" onfocus="if (this.value == 'arama yap...') {this.value = '';}" type="text" value="arama yap..."/>
<input id="searchsubmit" style="display: none;" type="submit" value=""/>
</div>
</form>
<div id="portrait">
<ul>
<li><a href="https://www.atam.gov.tr/mustafa-kemal-ataturk-veri-tabani">Mustafa Kemal Atatürk</a></li>
<li><a href="https://www.atam.gov.tr/turkiye-cumhuriyeti-tarihi-veri-tabani">Türkiye Cumhuriyeti Tarihi Veri Tabanı(E-yayınlarımız)</a></li>
</ul>
</div>
<div id="other-com">
<ul>
<li><a href="http://ayk.gov.tr/" target="_blank"><img src="/wp-content/uploads/ayk-logo-50.png"/>Atatürk Kültür, Dil ve Tarih</a></li>
<li style="margin:-30px 0 -20px 0;"><a href="http://ayk.gov.tr/" target="_blank"><img src="/wp-content/uploads/tr-50.png"/>Yüksek Kurumu</a></li>
<li><a href="http://akmb.gov.tr/" target="_blank"><img src="/wp-content/uploads/akm.png"/>Atatürk Kültür Merkezi</a></li>
<li><a href="http://tdk.gov.tr/" target="_blank"><img src="/wp-content/uploads/tdk.png"/>Türk Dil Kurumu</a></li>
<li><a href="http://ttk.gov.tr/" target="_blank"><img src="/wp-content/uploads/ttk.png"/>Türk Tarih Kurumu</a></li>
</ul>
</div>
</div>
<div class="right-column">
<div id="events">
<div class="item">
<div class="thumb">
<a href="https://www.atam.gov.tr/haberler/yeni-yayin-demokrat-parti-trabzon-milletvekili-selahaddin-karayavuzun-uc-devre-isik-tutan-anilari"><img alt="" class="attachment-full size-full wp-post-image" height="365" sizes="(max-width: 1024px) 100vw, 1024px" src="https://www.atam.gov.tr/wp-content/uploads/DERgi-yeni-site-1.png" srcset="https://www.atam.gov.tr/wp-content/uploads/DERgi-yeni-site-1.png 1024w, https://www.atam.gov.tr/wp-content/uploads/DERgi-yeni-site-1-300x107.png 300w, https://www.atam.gov.tr/wp-content/uploads/DERgi-yeni-site-1-768x274.png 768w" width="1024"/></a>
</div>
<div class="title"><h2>Yeni Yayın: Demokrat Parti Trabzon Milletvekili Selahaddin Karayavuz’un Üç Devre Işık Tutan Anıları</h2></div>
<div class="content">
<p>Atatürk Kültür, Dil ve Tarih Yüksek Kurumu bünyesindeki Atatürk Araştırma Merkezi, yayınları arasına yeni bir eser daha ekledi.  Demokrat Parti [...]</p>
</div>
<div class="read-more">
<a href="https://www.atam.gov.tr/haberler/yeni-yayin-demokrat-parti-trabzon-milletvekili-selahaddin-karayavuzun-uc-devre-isik-tutan-anilari">detaylar</a>
</div>
</div>
<div class="item">
<div class="thumb">
<a href="https://www.atam.gov.tr/haberler/ataturk-arastirma-merkezi-dergisinin-102-sayisi-yayimlandi"><img alt="" class="attachment-full size-full wp-post-image" height="365" sizes="(max-width: 1024px) 100vw, 1024px" src="https://www.atam.gov.tr/wp-content/uploads/DERgi-yeni-site.png" srcset="https://www.atam.gov.tr/wp-content/uploads/DERgi-yeni-site.png 1024w, https://www.atam.gov.tr/wp-content/uploads/DERgi-yeni-site-300x107.png 300w, https://www.atam.gov.tr/wp-content/uploads/DERgi-yeni-site-768x274.png 768w" width="1024"/></a>
</div>
<div class="title"><h2>Atatürk Araştırma Merkezi Dergisi’nin 102. Sayısı Yayımlandı</h2></div>
<div class="content">
<p>Atatürk Kültür, Dil ve Tarih Yüksek Kurumu bünyesindeki Atatürk Araştırma Merkezi Başkanlığının 1984 yılından beri yayımlanan süreli yayını Atatürk Araştırma [...]</p>
</div>
<div class="read-more">
<a href="https://www.atam.gov.tr/haberler/ataturk-arastirma-merkezi-dergisinin-102-sayisi-yayimlandi">detaylar</a>
</div>
</div>
<div class="item">
<div class="thumb">
<a href="https://www.atam.gov.tr/haberler/ataturk-arastirma-merkezi-yayinlarina-3-yeni-eser-daha-eklendi-2"><img alt="" class="attachment-full size-full wp-post-image" height="1344" sizes="(max-width: 2560px) 100vw, 2560px" src="https://www.atam.gov.tr/wp-content/uploads/3-KİTAP-scaled-e1606292090628.jpg" srcset="https://www.atam.gov.tr/wp-content/uploads/3-KİTAP-scaled-e1606292090628.jpg 2560w, https://www.atam.gov.tr/wp-content/uploads/3-KİTAP-scaled-e1606292090628-300x158.jpg 300w, https://www.atam.gov.tr/wp-content/uploads/3-KİTAP-scaled-e1606292090628-1024x538.jpg 1024w, https://www.atam.gov.tr/wp-content/uploads/3-KİTAP-scaled-e1606292090628-768x403.jpg 768w, https://www.atam.gov.tr/wp-content/uploads/3-KİTAP-scaled-e1606292090628-1536x806.jpg 1536w, https://www.atam.gov.tr/wp-content/uploads/3-KİTAP-scaled-e1606292090628-2048x1075.jpg 2048w" width="2560"/></a>
</div>
<div class="title"><h2>Atatürk Araştırma Merkezi Yayınlarına 3 Yeni Eser Daha Eklendi.</h2></div>
<div class="content">
<p>2020 yılının son günlerine yaklaşırken Atatürk Kültür, Dil ve Tarih Yüksek Kurumu bünyesindeki Atatürk Araştırma Merkezi Başkanlığı okuyucuların beğenisine 3 [...]</p>
</div>
<div class="read-more">
<a href="https://www.atam.gov.tr/haberler/ataturk-arastirma-merkezi-yayinlarina-3-yeni-eser-daha-eklendi-2">detaylar</a>
</div>
</div>
<div class="item">
<div class="thumb">
<a href="https://www.atam.gov.tr/haberler/ataturk-arastirma-merkezi-baskanligi-arsivimizden-fotograflarla-ataturk-sergisi"><img alt="" class="attachment-full size-full wp-post-image" height="365" sizes="(max-width: 1024px) 100vw, 1024px" src="https://www.atam.gov.tr/wp-content/uploads/Site10-kasımm-1.png" srcset="https://www.atam.gov.tr/wp-content/uploads/Site10-kasımm-1.png 1024w, https://www.atam.gov.tr/wp-content/uploads/Site10-kasımm-1-300x107.png 300w, https://www.atam.gov.tr/wp-content/uploads/Site10-kasımm-1-768x274.png 768w" width="1024"/></a>
</div>
<div class="title"><h2>Atatürk Araştırma Merkezi Başkanlığı “Arşivimizden Fotoğraflarla Atatürk” Sergisi</h2></div>
<div class="content">
<p>Atatürk Kültür, Dil ve Tarih Yüksek Kurumunun kuruluşuna dâhil kurumlardan biri olan Atatürk Araştırma Merkezi Başkanlığı, Gazi Mustafa Kemal Atatürk’ün [...]</p>
</div>
<div class="read-more">
<a href="https://www.atam.gov.tr/haberler/ataturk-arastirma-merkezi-baskanligi-arsivimizden-fotograflarla-ataturk-sergisi">detaylar</a>
</div>
</div>
<div class="item">
<div class="thumb">
<a href="https://www.atam.gov.tr/haberler/ataturk-arastirma-merkezi-baskanligindan-yeni-eser-milletin-sesi-mehmet-akif-ersoy"><img alt="" class="attachment-full size-full wp-post-image" height="412" sizes="(max-width: 1032px) 100vw, 1032px" src="https://www.atam.gov.tr/wp-content/uploads/Milletin-SESi-site-1.png" srcset="https://www.atam.gov.tr/wp-content/uploads/Milletin-SESi-site-1.png 1032w, https://www.atam.gov.tr/wp-content/uploads/Milletin-SESi-site-1-300x120.png 300w, https://www.atam.gov.tr/wp-content/uploads/Milletin-SESi-site-1-1024x409.png 1024w, https://www.atam.gov.tr/wp-content/uploads/Milletin-SESi-site-1-768x307.png 768w" width="1032"/></a>
</div>
<div class="title"><h2>Atatürk Araştırma Merkezi Başkanlığından Yeni Eser: “Milletin Sesi Mehmet Akif ERSOY”</h2></div>
<div class="content">
<p>Atatürk Kültür, Dil ve Tarih Yüksek Kurumu bünyesinde yer alan Atatürk Araştırma Merkezi Başkanlığı raflarına, Adnan YILMAZ tarafından kaleme alınan [...]</p>
</div>
<div class="read-more">
<a href="https://www.atam.gov.tr/haberler/ataturk-arastirma-merkezi-baskanligindan-yeni-eser-milletin-sesi-mehmet-akif-ersoy">detaylar</a>
</div>
</div>
<div class="item">
<div class="thumb">
<a href="https://www.atam.gov.tr/haberler/ataturk-arastirma-merkezi-baskanligi-4-eseri-okuyucuyla-bulusturdu"><img alt="" class="attachment-full size-full wp-post-image" height="512" sizes="(max-width: 1024px) 100vw, 1024px" src="https://www.atam.gov.tr/wp-content/uploads/23698.png" srcset="https://www.atam.gov.tr/wp-content/uploads/23698.png 1024w, https://www.atam.gov.tr/wp-content/uploads/23698-300x150.png 300w, https://www.atam.gov.tr/wp-content/uploads/23698-768x384.png 768w" width="1024"/></a>
</div>
<div class="title"><h2>Atatürk Araştırma Merkezi Başkanlığı 4 Eseri Okuyucuyla Buluşturdu</h2></div>
<div class="content">
<p>Atatürk Kültür, Dil ve Tarih Yüksek Kurumu bünyesinde yer alan Atatürk Araştırma Merkezi Başkanlığı, yayınlarını okuyucularla buluşturmaya devam ediyor. Dr. [...]</p>
</div>
<div class="read-more">
<a href="https://www.atam.gov.tr/haberler/ataturk-arastirma-merkezi-baskanligi-4-eseri-okuyucuyla-bulusturdu">detaylar</a>
</div>
</div>
<div class="item">
<div class="thumb">
<a href="https://www.atam.gov.tr/haberler/ataturk-arastirma-merkezi-baskanligindan-3-yeni-eser"><img alt="" class="attachment-full size-full wp-post-image" height="1331" sizes="(max-width: 2560px) 100vw, 2560px" src="https://www.atam.gov.tr/wp-content/uploads/3d-kapaklar-scaled-e1594891518582.jpg" srcset="https://www.atam.gov.tr/wp-content/uploads/3d-kapaklar-scaled-e1594891518582.jpg 2560w, https://www.atam.gov.tr/wp-content/uploads/3d-kapaklar-scaled-e1594891518582-300x156.jpg 300w, https://www.atam.gov.tr/wp-content/uploads/3d-kapaklar-scaled-e1594891518582-1024x532.jpg 1024w, https://www.atam.gov.tr/wp-content/uploads/3d-kapaklar-scaled-e1594891518582-768x399.jpg 768w, https://www.atam.gov.tr/wp-content/uploads/3d-kapaklar-scaled-e1594891518582-1536x799.jpg 1536w, https://www.atam.gov.tr/wp-content/uploads/3d-kapaklar-scaled-e1594891518582-2048x1065.jpg 2048w" width="2560"/></a>
</div>
<div class="title"><h2>Atatürk Araştırma Merkezi Başkanlığından 3 yeni eser</h2></div>
<div class="content">
<p>Atatürk Kültür, Dil ve Tarih Yüksek Kurumu bünyesinde yer alan Atatürk Araştırma Merkezi, Türkiye tarihine ışık tutmaya devam ediyor. 2020 [...]</p>
</div>
<div class="read-more">
<a href="https://www.atam.gov.tr/haberler/ataturk-arastirma-merkezi-baskanligindan-3-yeni-eser">detaylar</a>
</div>
</div>
<div class="item">
<div class="thumb">
<a href="https://www.atam.gov.tr/haberler/ataturk-arastirma-merkezinden-yeni-bir-biyografik-eser"><img alt="" class="attachment-full size-full wp-post-image" height="304" sizes="(max-width: 768px) 100vw, 768px" src="https://www.atam.gov.tr/wp-content/uploads/Kamran-İnan-1.jpg" srcset="https://www.atam.gov.tr/wp-content/uploads/Kamran-İnan-1.jpg 768w, https://www.atam.gov.tr/wp-content/uploads/Kamran-İnan-1-300x119.jpg 300w" width="768"/></a>
</div>
<div class="title"><h2>Atatürk Araştırma Merkezinden Yeni Bir Biyografik Eser</h2></div>
<div class="content">
<p>Atatürk Kültür, Dil ve Tarih Yüksek Kurumu bünyesinde yer alan Atatürk Araştırma Merkezi yayımlamış olduğu biyografi eserlerine bir yenisini daha [...]</p>
</div>
<div class="read-more">
<a href="https://www.atam.gov.tr/haberler/ataturk-arastirma-merkezinden-yeni-bir-biyografik-eser">detaylar</a>
</div>
</div>
<!--<a class="all" href="https://www.atam.gov.tr/etkinlikler">tümü</a>-->
</div>
</div>
</div>
</div>
<!-- End Content -->
<!-- Start Footer -->
<div id="footer">
<div class="wrap">
<div id="social">
<ul>
<li><a href="https://www.facebook.com/Atam.Baskanlik"><img src="https://www.atam.gov.tr/wp-content/themes/v1/images/facebook.png"/></a></li>
<li><a href="https://twitter.com/AtamBaskanlik"><img src="https://www.atam.gov.tr/wp-content/themes/v1/images/twitter.png"/></a></li>
<li><a href="https://www.youtube.com/user/atambaskanlik"><img src="https://www.atam.gov.tr/wp-content/themes/v1/images/youtube.png"/></a></li>
</ul>
</div>
<div id="copyright">
<p>Copyrights © 2013</p>
<p>T.C. Başbakanlık Atatürk Kültür, Dil ve Tarih Yüksek Kurumu</p>
<p>Atatürk Araştırma Merkezi Başkanlığı</p>
<p>Tüm Hakları Saklıdır.</p>
</div>
<div id="footer-nav">
<ul>
<li><a href="https://www.atam.gov.tr">Ana Sayfa</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal">Kurumsal</a></li>
<li><a href="https://www.atam.gov.tr/faaliyetler">Faaliyetler</a></li>
<li><a href="https://www.atam.gov.tr/online-islemler">Online İşlemler</a></li>
<li><a href="https://www.atam.gov.tr/kutuphane">Kütüphane</a></li>
<li><a href="https://www.atam.gov.tr/kurumsal/iletisim">İletişim</a></li>
<li><a href="https://emagaza-atam.ayk.gov.tr">E-mağaza</a></li>
</ul>
</div>
</div>
</div>
<!-- End Footer -->
<!-- Lightbox Plus ColorBox v2.6/1.3.32 - 2013.01.24 - Message: 0-->
<script type="text/javascript">
jQuery(document).ready(function($){
  $("a[rel*=lightbox]").colorbox({initialWidth:"30%",initialHeight:"30%",maxWidth:"90%",maxHeight:"90%",opacity:0.8,current:"Görsel {current} / {total}",previous:"< Önceki",next:"Sonraki >     ",close:"X Kapat"});
});
</script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.atam.gov.tr\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://www.atam.gov.tr/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.9" type="text/javascript"></script>
<script src="https://www.atam.gov.tr/wp-content/plugins/lightbox-plus/js/jquery.colorbox.1.3.32.js?ver=1.3.32" type="text/javascript"></script>
<script src="https://www.atam.gov.tr/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
</body>
</html>
