<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://d16l25os8aa57a.cloudfront.net/assets/favicon-96af1416e568d84cfa13e37d8dbf75c7f066d8c3a680a835de0b6e2c7afb22a8.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://d16l25os8aa57a.cloudfront.net/assets/apple-touch-icon-76x76-a551b3cbb79a472e36bcbe33737576619566bd92b13cdffba381bdd1d36e834a.png" rel="apple-touch-icon" sizes="76x76" type="image/png"/>
<link href="https://d16l25os8aa57a.cloudfront.net/assets/apple-touch-icon-120x120-47352355ae1dfbbc33ec23341edd2cdb4969879c1c493e6ed07da543a27f2df9.png" rel="apple-touch-icon" sizes="120x120" type="image/png"/>
<link href="https://d16l25os8aa57a.cloudfront.net/assets/apple-touch-icon-152x152-56204e6647a9e56804a95048f33d39883b572a2de5fef3dfb4721f2b8a7cc7b4.png" rel="apple-touch-icon" sizes="152x152" type="image/png"/>
<link href="https://d16l25os8aa57a.cloudfront.net/assets/apple-touch-icon-180x180-318b093235b45b4a5fd78f46416ab813804dc4fc326852909fb2774d3481a7a4.png" rel="apple-touch-icon" sizes="180x180" type="image/png"/>
<title>
      Softball – Bath High School – Lima, Ohio
    </title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="Learn more about Softball at Bath High School in Lima, Ohio on BeRecruited." name="description"/>
<meta content="authenticity_token" name="csrf-param"/>
<meta content="yUtoE3u3qmG1+4ki58+syNkqzWzthxaqaUrrr8IhkfwnOF1RDFSBESlMjzF/ij62T9ChVxvBDN63ho96zM2Nww==" name="csrf-token"/>
<meta content="null" name="current-user"/>
<meta content="null" name="current-athlete"/>
<meta content="null" name="current-coach"/>
<meta content="null" name="current-high-school-coach"/>
<link href="https://new.berecruited.com/high-schools/ohio/lima/bath-high-school/softball?page=1&amp;view_type=athletes" rel="canonical"/>
<!-- global window.br initialization -->
<script type="text/javascript">
    var br = window.br || {};

    (function() {
        "use strict";
        var pageRunQueue = [];

        window.br.q = function(f) {
            pageRunQueue.push(f);
        };

        window.br.runQueuedFuncs = function() {
            $.each(pageRunQueue, function(i, f) {
                $.apply(this, [f]);
            });
        };
    }());
</script>
<link href="https://d16l25os8aa57a.cloudfront.net/assets/lib_v2-a18bed0fc4bd1b262eb1d6ab0c377c72b75f9b77edbce132d70d34ca74949eda.css" media="screen" rel="stylesheet"/>
<link href="https://d16l25os8aa57a.cloudfront.net/assets/components-8f1c0c39e5dc1a995127bba2c51e6a897cc849ef79d9fe79059a49e6d99c2814.css" media="screen" rel="stylesheet"/>
<link href="https://d16l25os8aa57a.cloudfront.net/assets/app-de5a69e5f0cc43e61e785611f5b586b6d9b99ace99458d3540173ca3aacb5844.css" media="screen" rel="stylesheet"/>
</head>
<body app-action="school-team" app-controller="teams" class="teams-page" id="teams-school-team">
<script type="text/javascript">
    window.br = window.br || {};
    window.br.userInfo = {"athleteData":{},"coachData":{}};
</script>
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '86567173233',
      xfbml      : true,
      version    : 'v2.3'
    });
  };

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = '//connect.facebook.net/en_US/sdk.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
<div class="page-wrapper">
<header class="signout-header">
<div class="container">
<nav class="navbar" role="navigation">
<div class="navbar-header">
<button class="navbar-toggle" data-target="#navbar-content" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigator</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="logo-inverse" href="/" id="logo" title="BeRecruited">
</a>
</div>
<div class="collapse navbar-collapse" id="navbar-content">
<ul class="nav navbar-nav navbar-right">
<li>
<span>
<a class="btn btn-cta" href="/login">Athletes Click Here</a>
</span>
</li>
<li>
<span>
<a class="btn btn-cta" href="/login">Parents Click Here</a>
</span>
</li>
<li>
<span>
<a class="btn btn-cta" href="/login">Coaches Click Here</a>
</span>
</li>
<li>
<span>
<a class="btn btn-link-cta" href="/login">ATHLETE SIGN IN</a>
</span>
</li>
</ul>
</div>
</nav>
</div>
</header>
<div id="sub-header">
<section class="container">
<span class="title-text"> Ohio High School Sports </span>
<ul class="breadcrumb">
<li> <a href="/high-schools">High Schools</a> <span class="divider"> &gt; </span></li>
<li> <a href="/high-schools/ohio">Ohio</a> <span class="divider"> &gt; </span></li>
<li> <a href="/high-schools/ohio/lima">Lima</a> <span class="divider"> &gt; </span></li>
<li> <a href="/high-schools/ohio/lima/bath-high-school">Bath High School</a> <span class="divider"> &gt; </span></li>
<li class="active">
<h1>Softball</h1>
</li>
</ul>
</section>
</div>
<div class="flash-messages">
</div>
<section id="main">
<div class="container">
<h2>Sports</h2>
<div class="row">
<div class="sidebar-header col-md-4">
<form accept-charset="UTF-8" action="/teams/45345011/choose" id="team-select-form" method="post"><input name="utf8" type="hidden" value="✓"/><input name="authenticity_token" type="hidden" value="P6L4EhNLj8n+usEAQAe5OdwqvHiOA9uj4aYeOEqNKNrR0c1QZKikuWINxxPYQitHStDQQ3hFwdc/anrtRGE05Q=="/>
<input id="state" name="state" type="hidden" value="ohio"/>
<input id="city" name="city" type="hidden" value="lima"/>
<input id="school" name="school" type="hidden" value="bath-high-school"/>
<input id="view_type" name="view_type" type="hidden" value="athletes"/>
<select class="form-control" id="team_select" name="team_select"><option value="">All Sports</option><option value="baseball">Baseball</option>
<option value="boys-baseball">-- Boys Baseball</option></select>
</form> </div>
<div class="tabs-header col-md-8">
<ul class="nav nav-pills">
<li class="active ">
<a href="#">Athletes</a>
</li>
<li class=" disabled">
<a href="#" title="This team has no photos.">Photos</a>
</li>
<li class=" disabled">
<a href="#" title="This team has no game film.">Game Film</a>
</li>
</ul>
</div>
</div>
<div class="content-row row">
<div class="col-md-4">
<div class="sidebar">
<div id="sidebar-content">
<h2> Statistics </h2>
<table class="table table-bordered">
<tr>
<td>Total Athletes <span class="pull-right">0</span></td>
</tr>
<tr>
<td>Total Unique Views<span class="pull-right">0</span></td>
</tr>
<tr>
<td>Total Search Appearances <span class="pull-right">0</span></td>
</tr>
<tr>
<td>Total Commits <span class="pull-right">0</span></td>
</tr>
</table>
</div>
</div>
<div class="teams-promos">
<div class="login-banner">
<div class="top">
<p class="text-center">
<img alt="Br coat of arms" src="https://d16l25os8aa57a.cloudfront.net/assets/logos/br_coat_of_arms-db6fd699727f80643092dd40cb1822414c0ae3f1f3ea14bdf6ca9d5f90a4310d.png"/>
</p>
</div>
<p class="main-text text-center">
      Register now and connect with 2 million high school athletes and 25,000 high school coaches.
    </p>
<div class="bottom text-center">
<p class="text-center">
<a class="btn btn-large btn-primary" href="/login">Start Now for Free</a>
</p>
</div>
</div>
</div>
</div>
<div class="col-md-8">
<div class="main-content">
<div class="col-xs-12">
<div class="athlete-list">
</div>
</div>
<script id="tpl-athlete-actions" type="text/html">
</script>
<script type="text/javascript">
  (function() {
    'use strict';

    br.q(function() {
      var listStatus, viewedAthleteIds, tplActions;

    });
  }());
</script>
</div>
<div class="text-center">
</div>
</div>
</div>
</div>
</section>
</div>
<div class="footer-wrapper">
<footer class="footer">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="row">
<div class="col-md-3 col-sm-6 col-xs-6 footer-col">
<h4>Explore</h4>
<ul class="footer-menu">
<li>
<a href="/search/athletes">Athletes</a>
</li>
<li>
<a href="/high-schools">High Schools</a>
</li>
<li>
<a href="/search/colleges">Colleges</a>
</li>
<li>
<a href="/resources">Resources</a>
</li>
</ul>
<h4>Service</h4>
<ul class="footer-menu">
<li>
<a href="/subscriptions">Pricing</a>
</li>
<li>
<a href="/resources/terms-of-use">Terms of Use</a>
</li>
<li>
<a href="/resources/terms-of-sale">Terms of Sale</a>
</li>
<li>
<a href="/resources/privacy">Privacy</a>
</li>
</ul>
</div>
<div class="col-md-3 col-sm-6 col-xs-6 footer-col">
<h4>About Us</h4>
<ul class="footer-menu">
<li>
<a href="/resources/about">About</a>
</li>
<li>
<a href="/resources/faq">FAQ</a>
</li>
<li>
<a href="/resources/contact">Contact us</a>
</li>
</ul>
<h4>Divisions</h4>
<ul class="footer-menu">
<li>
<a href="/division-1-colleges-softball">Division 1 Colleges</a>
</li>
<li>
<a href="/division-2-colleges-softball">Division 2 Colleges</a>
</li>
<li>
<a href="/division-3-colleges-softball">Division 3 Colleges</a>
</li>
</ul>
</div>
<div class="col-md-3 col-sm-6 col-xs-6 footer-col">
<h4>High School Sports</h4>
<ul class="footer-menu">
<li><a href="/high-school-baseball">Baseball</a></li>
<li><a href="/high-school-basketball">Basketball</a></li>
<li><a href="/high-school-diving">Diving</a></li>
<li><a href="/high-school-esports">Esports</a></li>
<li><a href="/high-school-field-hockey">Field Hockey</a></li>
<li><a href="/high-school-football">Football</a></li>
<li><a href="/high-school-golf">Golf</a></li>
<li><a href="/high-school-ice-hockey">Ice Hockey</a></li>
<li><a href="/high-school-lacrosse">Lacrosse</a></li>
<li><a href="/high-school-rowing">Rowing</a></li>
<li><a href="/high-school-soccer">Soccer</a></li>
<li><a href="/high-school-softball">Softball</a></li>
<li><a href="/high-school-swimming">Swimming</a></li>
<li><a href="/high-school-tennis">Tennis</a></li>
<li><a href="/high-school-running">Track and Field, XC</a></li>
<li><a href="/high-school-volleyball">Volleyball</a></li>
<li><a href="/high-school-water-polo">Water Polo</a></li>
<li><a href="/high-school-wrestling">Wrestling</a></li>
</ul>
</div>
<div class="col-md-3 col-sm-6 col-xs-6 footer-col">
<h4>Sport Scholarships</h4>
<ul class="footer-menu">
<li><a href="/baseball">Baseball</a></li>
<li><a href="/basketball">Basketball</a></li>
<li><a href="/diving">Diving</a></li>
<li><a href="/esports">Esports</a></li>
<li><a href="/field-hockey">Field Hockey</a></li>
<li><a href="/football">Football</a></li>
<li><a href="/golf">Golf</a></li>
<li><a href="/ice-hockey">Ice Hockey</a></li>
<li><a href="/lacrosse">Lacrosse</a></li>
<li><a href="/rowing">Rowing</a></li>
<li><a href="/soccer">Soccer</a></li>
<li><a href="/softball">Softball</a></li>
<li><a href="/swimming">Swimming</a></li>
<li><a href="/tennis">Tennis</a></li>
<li><a href="/running">Track and Field, XC</a></li>
<li><a href="/volleyball">Volleyball</a></li>
<li><a href="/water-polo">Water Polo</a></li>
<li><a href="/wrestling">Wrestling</a></li>
</ul>
</div>
</div>
</div>
<div class="col-xs-12">
<div class="footer-copy">
<p>© 2021 BeRecruited.com</p>
<p class="social-links">
<a href="https://twitter.com/beRecruited" target="_blank">
<span class="fa fa-twitter-square"></span>
</a> <a href="https://www.facebook.com/BeRecruited" target="_blank">
<span class="fa fa-facebook-square"></span>
</a> </p>
</div>
</div>
</div>
</div>
</footer>
</div>
<script>
//<![CDATA[
window.br=window.br||{};br.searchInfo=br.searchInfo||{};br.searchInfo.counts={"current":null,"total":null}
//]]>
</script>
<script>
    window.inspectlet = {
        tagSession: function(message) {
            if (!!window.__insp) {
                __insp.push(['tagSession', message]);
            }
        }
    };
</script>
<script>
  (function(y,g,r,i,t,e,q) {y.ygritteq=t,y[t]=y[t]||function()
  {(y[t].q=y[t].q||[]).push(arguments)}
  ,y[t].l=1*new Date,e=g.createElement(r),q=g.getElementsByTagName(r)[0],e.async=1,e.src=i,e.id=t,q.parentNode.insertBefore(e, q)})(window,document,'script','https://s.edkay.com/s/ygritte.js','ygritte');
  ygritte("init",
  {token: "c12900b97bf8abcc3de6002462e859e8", category: "Teams"}
  );
</script>
<script src="https://d16l25os8aa57a.cloudfront.net/assets/vendor-6a74374e538ad1eadbb1fa865a767a45f99c80a011cf3120ddb451b2e8b2b7b5.js"></script>
<script src="https://d16l25os8aa57a.cloudfront.net/assets/application_v2-36a4218054c3fbd79a002263ad9333c28f0c26df323d362f350635af2abe483d.js"></script>
<script>
//<![CDATA[
br.runQueuedFuncs();
//]]>
</script>
<script type="text/javascript">!function(e,t,n){function a(){var e=t.getElementsByTagName("script")[0],n=t.createElement("script");n.type="text/javascript",n.async=!0,n.src="https://beacon-v2.helpscout.net",e.parentNode.insertBefore(n,e)}if(e.Beacon=n=function(t,n,a){e.Beacon.readyQueue.push({method:t,options:n,data:a})},n.readyQueue=[],"complete"===t.readyState)return a();e.attachEvent?e.attachEvent("onload",a):e.addEventListener("load",a,!1)}(window,document,window.Beacon||function(){});</script>
<script type="text/javascript">window.Beacon('init', '9b77d78a-d01c-46f7-96f1-7af45efd6404')</script>
<!--[if lt IE 9]>
      <script src="https://d16l25os8aa57a.cloudfront.net/assets/ie8_fixes-591a0adf08d89106a4b397a864dae5e387056b3673382f380c8d53fa1b18a5a3.js"></script>
      <![endif]-->
</body>
<!-- host: app2.berecruited.internal, now: 1610557525, env: production, revision:  -->
</html>
