<!DOCTYPE html>
<html>
<head>
<title>剑知创业网 | (原美国剑知商务网)</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/css/main.css" rel="stylesheet" type="text/css"/>
<link href="/css/custom.advancedj.css" rel="stylesheet" type="text/css"/>
<link href="/css/search-suggestion.css" rel="stylesheet" type="text/css"/>
<script src="/js/jquery-1.11.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/xmlhttprequest.js"></script>
<script src="/js/search-suggestion.js"></script>
<!-- favicon files -->
<link href="/images/favicon.advancedj.ico" rel="icon" type="image/x-icon"/>
<!-- favicon files --><script type="text/javascript">
function createChkBox(chkPId) {
	var chkP = document.getElementById(chkPId);
	var cb = document.createElement("input");
	var cbText = document.createTextNode("请点击确认您不是spammer");
	cb.type = "checkbox";
	cb.name = "cl_check_9dd";
	cb.style.width = "25px";
	chkP.appendChild(cb);
	chkP.appendChild(cbText);
}
function validateChkBox(theForm) {
	if(theForm.cl_check_9dd.checked != true) {
		alert("请点击确认您不是spammer");
		return false;
	}
	return true;
}
function deleteStaticPage(pageId)
{
	if (!confirm("Are you sure to delete?")) return;
	document.location = "/do/manageStaticPage?doIt=delete&pageId=" + pageId;
}
</script>
</head>
<body id="home">
<div class="topnav" id="myTopnav"> 
	<a href="/newsletter">订阅简报</a>
<!--<a href="/list">博客</a>-->
<a href="/courses">教程</a>
<a href="/vip/listCats">VIP区</a>
<a href="/joinvip">订购VIP</a>
<a href="/do/login">登录</a>
<a class="icon" href="javascript:void(0);" onclick="toggleTopNav()" style="font-size:15px;">☰ Menu</a>
</div>
<script>
function toggleTopNav() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>
<div class="container">
<div class="row">
<div class="col-sm-2"></div>
<div class="col-sm-2">
<a href="/"><span class="logo">剑知创业网</span></a>
</div>
<div class="col-sm-7">
<form action="/search" id="searchForm" method="get" name="searchForm">
<input autocomplete="off" id="searchBox" maxlength="256" name="q" type="text" value=""/>
<input name="label" type="hidden" value=""/>
<input name="newSearch" type="hidden" value="y"/>
<input class="submitBtn" style="height:30px;line-height:30px;font-weight:bold;" type="submit" value=" Search "/>
</form>
</div>
</div>
</div>
<div class="container fnt" style="margin-top:15px; margin-bottom:10px;">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
<h1 style="display:inline-block;">
			美国创业学习园地
		</h1>
<div class="lnku" style="margin-top:20px;">
		对个人创业感兴趣？<br/>
<br/>
您来对啦 :-) 您将在这里学习到最实用的美国个人创业技能，比如: online marketing，英语business写作以及如何利用写作来推广你自己，网站推广技巧等等。<br/>
<br/>
请订阅我写的剑知简报，我会经常跟您分享我学习到的美国business和online marketing知识。<br/>
<br/>
祝好，<br/>
<br/>
JC<br/>
<br/>
<script type="text/javascript">
function submitForm(formId) {
var theForm = document.getElementById(formId);
if(!validateChkBox(theForm)) {
return false;
}
theForm.submit();
}
</script>
<form action="/subscribe" id="subscribeForm" method="post">
<input name="doIt" type="hidden" value="submit"/>
<input name="offer" type="hidden" value="newsletter"/>
<!--
<b>First Name</b><br>
<input class="subscribeFirstName" type="text" name="firstName" value="" maxlength="100"><br>
-->
<b>Email</b><br/>
<input class="subscribeEmail" maxlength="100" name="email" type="text" value=""/>
<p id="chkP" style="clear:both;font-size:14px;"></p>
<input class="submitBtn" onclick="submitForm('subscribeForm')" type="button" value=" 订阅简报 "/>
</form>
<script type="text/javascript">
createChkBox("chkP");
</script>
<!--
已经订阅了简报？<a href="/list">去看看博客文章</a>
-->
<br/>
<b>网友好评:</b><br/>
<br/>
美乐地爸爸: 看了你转行的文章，经历厉害，多谢你这种愿意分亨帮助别人的人，能做到这一点相当不容易，这得花多少时间码字呀。学了不少东西，谢谢了。<br/>
<br/>
amandama: 谢谢您的分享和博文推送！有您满满的正能量鼓励和成功经验分享，我会提升信心，坚持做下去！谢谢！<br/>
<br/>
丹华: 谢谢你发来的剑知简报！你看问题犀利深入，分享的经验很实用！<br/>
<br/>
<b>最新博客文章:</b>
<div class="lnku"><a href="/topic/497/what-i-learned-from-dave-ramsey">剑知简报(2020/10/11): 从Dave Ramsey视频学到的</a></div>
<div class="lnku"><a href="/topic/496/service-business-ideas">美国服务类个人创业idea汇总(附案例分析)</a></div>
<div class="lnku"><a href="/topic/235/steps-to-register-a-company-in-usa">我研究的如何在美国开公司的步骤和报税指南</a></div>
<br/>
</div>
</div>
</div>
</div>
<div style="text-align:center; margin-top: 10px;">
<a href="/aboutus">关于本站 |</a>
<a href="/list">博客 |</a>
<a href="/contact">Contact |</a>
<a href="https://www.jiansnet.com">美国剑知生活网 |</a>
<a href="https://www.jchen.blog">JC的英文站 |</a>
<a href="/privacy">Privacy Policy</a>
<br/><span class="copyright">Copyright © 2021, All Rights Reserved.</span>
</div>
<br/>
<script>
if ($("#searchForm input[name=q]").length > 0)
{
	$("#searchForm input[name=q]").focus();
}
</script>
<script src="//s.skimresources.com/js/27881X1526699.skimlinks.js" type="text/javascript"></script>
<!-- clicky tracking start -->
<script type="text/javascript">
var clicky_site_ids = clicky_site_ids || [];
clicky_site_ids.push(101079768);
(function() {
  var s = document.createElement('script');
  s.type = 'text/javascript';
  s.async = true;
  s.src = '//static.getclicky.com/js';
  ( document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0] ).appendChild( s );
})();
</script>
<noscript><p><img alt="Clicky" height="1" src="//in.getclicky.com/101079768ns.gif" width="1"/></p></noscript>
<!-- clicky tracking end --></body>
</html>