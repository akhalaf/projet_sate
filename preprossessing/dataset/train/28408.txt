<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "35475",
      cRay: "610b2197bdfd24bf",
      cHash: "3cd6bf126d96116",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuNDExbWFuaWEuY29tL3dyZXN0bGluZy92aWRlb19yZXZpZXdzLzUxMDMyL1Nob290aW5nLUZyb20tVGhlLUhpcDotVGhlLVJpY2stTWFydGVsLVJGLVNob290LUludGVydmlldy5odG0=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "WUpHXOlV5cJAdbV/iueFNbH9xfI6YQT7MR5x9FqYD6o+12+9jSymyih0edCpr75bq1nmcSZD0c9cdLA/UEbVdrw61ITI3rCdVX39sUNibArZsHG+qm0WDykFm7zYqtYHbt7Ji6qat+YnFMJSe0WXAAluxYB4jfGeMyRPrGiJzUeFRQbxYbZoA0UEpNzsr7o5QAcf+9P+yFFbURgpvpBfYVDqc2jm574n4q2jqznWFyi6ZXPoMbUo2joMQI03JiBn3u7nHK1B3SjjTlkg0YujsxDSovU1orZLaJETXXw/QTAMKK1bU6KpUHVLlcdHImTX3pgpUUGeMhEscU0U2pSCS1r2K85lrcDln/eUS8rDwMxxVMit3MY8F1iju7ga8CRXB9bRnJayqbxN0ua/8C7s0hlVKyPd9QtVPkA2JsoRyIy5eHysKpFf+2+5wgA6lYHRLNB0nMixEtei0gbuF8/Uc+hQRN136oZ8gxPJ29f1V/l91wflMfYuuWQXmiDXkiprh/xKRM4DMweLwh7qqZ2CYqZ95G7IwWnpRWSUrIcZnRnlrDi5o1kt8qj1NpOygIyXXPjVa6qvr7e4jqfahGCGcbsSJ1DBdpNrwo1JEjuI+SfY9+zmgT5U1ooZX8LmL6TidBz6uROAclPSr3Tcz49kDvYkHlwL/6nDlmIfQHRPk5aX3FVZEB/3O+P2vgbc4GRY7WyT2J/YP8Ciqr1t5uKaPmWY0p+QHo09nkzMHb2Md177oyMPYetFu3tL8DKxsb7v",
        t: "MTYxMDQ5OTExMS42MzMwMDA=",
        m: "E3hl97YAA1a6Ze+hClhJYbbiu2W/RIaDUe1RtOaGHEM=",
        i1: "sh+ajcUMTYIYnzfuIYDWEQ==",
        i2: "OZTO9HSsyPwC+1x9BkBDbw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "8Th5lpHJJvTylEZagdvhV5TXjOofKOGaVDxny/SJPZU=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610b2197bdfd24bf");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> 411mania.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/wrestling/video_reviews/51032/Shooting-From-The-Hip:-The-Rick-Martel-RF-Shoot-Interview.htm?__cf_chl_jschl_tk__=adba73aff350f8f50c03715b025cc857090bdeff-1610499111-0-Ac0F-aQaDwsBlYfabTJUwO0XrgHu_hvPEFvWwh-0Pd8r-YaP5rb0Tc1z-qDt-97fAIhhB-cZ56n_BwFZ-imqlTpRCFten-2-81XQtWxyiKnh7s2bYT5ec-aIwI2jmt7Yl2SPcfVxNRd0Eu3c_Ke6JQLzK5Jse5JaTpBC-N1VBtiv51v-soPA_IHwQlJCDxvx9XQmc7K1d_9MFcWZCnwCYlTeDZAUhbe8LXJXwavtW305igV7HnIN9T2iD3Xe39EsVG0Kd1U521v_Dgk4hD6KEf2-O8l5RX1O6S6Rf5HBTTfoUVAsueAUz01grUunE7c5EE-KeRdrjineGoYEqNQtIOcPqGXM7fwpmMzulNnoXwJFUIMQ50CjebZnRschSIKjhviQZ0ocRB2A2ktpryHYvrdM18xCcHFa1KyyUy9ofOMm" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="d29824a99beb1864cac48f89d3cb0215e3f91d87-1610499111-0-ARwDUAS4ERE+FfefLVVIZpmXTxaDTZGXDwHf/kVJJNG8wgMGw7UaevZ5W5Dx7S6sDYwxNzlxYLbNVC1HzBi9xgq8LKHVKim0YtHclEZWLW3Cb3tfRuEI0CNkB76SeaoIfM9h5G5K8rOEUXIDzBPgqmmv4Jz2xFQW3uzsSe1Z8OFzj24cSQ5SQglm6N5qThoZ2WWW6rkSjuVedrpX3s0a4IIAKQuxTrjynrgbIAT1buScsG526Ih3KDqdCoWmnAaTCkUGDwwWKIv1jWkvDyAkvPHADpWqcYeScqLidqdG8jFyW3gGJmCysQKfueSktsJw7+0qeHZmeYGxmoJZL9AIqBjxeYVLDpbd35e3Hg8gDHr0txnhEYIlSwZaBWXaDVY8D3qJN9jRlhoeI5haHe14F2gOZ2SDBjuj82zJ8C1SpDU4gDNAYX767ARJ2AeJ8VrBLa0pVbXS860JhkqsH/lX8pfLfywAA1WdOa3BeSm+/mStzk4WVgZQQ9qCXLLIc0TKF822UhQdKBolNmEQByVp4CacPLunp2RqyP0iq8jtZDOuOYzg2vz0TuFrHM2EvM8aniHVpuLq2jBRXHAb5mTbUN8DuLAxWgmo4Xin4UQn6jV8dDKpLRehK2izlwK8j94ep43cDU5feM0bDa2/6eT5OtUrqvSRJe5H22xND1N2xUxAUPaWxJoyNe1DpvQL0LvnanZDNiZ1VIKZcCIgSpJZ9r62QB5XgpV3hX56oG9r+KK3nVex2HiVuJjI3Au58VK8AmW4JShUnKoc/SvqBtiM58QxfSjkn9ZDVKjbEhVzo1CWb0B8h83aK1JNb/g+rGeb7z3dgGU80OLeHos4ZKV6X/H7qrVn1MQD75OZTG6ZQf3WRldxUHPLCCJLh5skT3XloqlAIA+8dvZgtu+HyKuRd5ucRIdmM9eOftl+O1jZvr+47AVGIO59kG4XuvNWSbMitzg+DAr/Yc35xqxOkAz9quYaKiHghmwXzN9FKNPY39LK8AJdwGahgWOl+EoMx01aFgZJpFBW3/n8wLL8anu896M8bhk95EhzGntVrN8ReGxPAhLyq9dSBBscpPmPDAPT6dLAiiMSLk4/MvnH9DMKG5M2OPwYkDqkwRGgHa+doNYmgmT6T/N9dybBUKQo8m1hAs5lhQ64Igx83KIDqiipfcaKVQwPSQ29l4TUuu0TB6Ox8wrQtXfbhUJi7d7CV2v/F9m/bPsWUi7zD3ei3+aV5ACZiZcJHPxRpouhtSDjvZHJxKiulhUhvJUhWmE1MgwChgTq3uDatK+ZiUBeLPGe4egf4bfWyvRS/lIxtQnpocUQO4TLnSqxKNuk+PeRxaoxrA=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="af652ba578b8628f069f20f2b7429fe2"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610499115.633-njrMaZKXlF"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610b2197bdfd24bf')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610b2197bdfd24bf</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
