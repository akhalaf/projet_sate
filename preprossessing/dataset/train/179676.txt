<!DOCTYPE html>
<html lang="tr-TR">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Ayrıntı Yayınları</title>
<meta content="Ayrıntı Yayınları" name="Description"/>
<meta content="Ayrıntı Yayınları" name="keywords"/>
<meta content="index, follow" name="robots"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=2" name="viewport"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<link href="https://www.ayrintiyayinlari.com.tr/" rel="canonical"/>
<meta content="https://www.ayrintiyayinlari.com.tr/" property="og:url"/>
<link href="//cdn1.dokuzsoft.com" rel="dns-prefetch"/>
<link href="//stats.g.doubleclick.net" rel="dns-prefetch"/>
<link href="//www.google-analytics.com" rel="dns-prefetch"/>
<link href="//www.googleadservices.com" rel="dns-prefetch"/>
<link href="/u/ayrintiyayinlari/favicon.ico?v=307" rel="shortcut icon" type="image/x-icon"/>
<link href="/u/ayrintiyayinlari/combine.css?v=307" rel="stylesheet" type="text/css"/>
<style>
.button_feedback,
.contact_form_table{display: none}
.ord_shipping_note{display: none}
</style>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '352918119100806'); 
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" src="https://www.facebook.com/tr?id=352918119100806&amp;ev=PageView
&amp;noscript=1" width="1"/>
</noscript>
<!-- End Facebook Pixel Code --></head>
<body>
<div class="page_0 page_home ">
<div class="container container_responsive ">
<div class="header_full_bg">
<div class="content_full_bg">
<div class="content_wrapper">
<div class="banner_top"></div>
<header class="header">
<div class="bar_top">
<div class="cw">
<div class="menu_social"> <ul>
<li><a href="https://www.facebook.com/pages/Ayr%C4%B1nt%C4%B1-Yayay%C4%B1nlar%C4%B1/231200173598676" target="_blank"><img alt="Facebook" src="/u/ayrintiyayinlari/menu/a/f/a/facebook-1580293352.png"/></a>
</li>
<li><a href="https://twitter.com/AYRINTIYAYINEVI" target="_blank"><img alt="Twitter" src="/u/ayrintiyayinlari/menu/a/t/w/twitter-1580293370.png"/></a>
</li>
<li><a href="https://instagram.com/ayrintiyayinlari/" target="_blank"><img alt="Instagram" src="/u/ayrintiyayinlari/menu/a/i/n/instagram-1580293389.png"/></a>
</li>
<li><a href="/iletisim.html"><img alt="Email" src="/u/ayrintiyayinlari/menu/a/m/a/mail-1580293404.png"/></a>
</li>
</ul>
</div>
<a class="cart_box " href="https://www.ayrintiyayinlari.com.tr/cart">
<span class="dy_cart_prd_count">0</span>
</a>
<div class="user_menu">
<ul class="user_menu_out">
<li class="login1"><a href="https://www.ayrintiyayinlari.com.tr/login" rel="nofollow"><span>Üye Girişi</span></a></li>
<li class="login2"><a href="https://www.ayrintiyayinlari.com.tr/index.php?p=Login&amp;seller=1" rel="nofollow"><span>Bayi Girişi</span></a>
</li>
</ul>
</div>
<div class="clear"></div>
</div>
</div>
<div class="cw header_content">
<span class="main_menu_opener fa fa-bars"></span>
<div class="col1">
<div class="logo">
<a href="https://www.ayrintiyayinlari.com.tr/"><img alt="www.ayrintiyayinlari.com.tr" src="/u/ayrintiyayinlari/ayrinti-logo-135-1561531123.png"/></a>
</div>
</div>
<div class="col2">
<nav class="main_menu">
<div class="cw">
<span class="fa fa-bars"></span>
<ul>
<li><a href="/"><span>Anasayfa</span></a>
</li>
<li><a href="/kitaplar-mg9.html"><span>Yeni Kitaplar</span></a>
</li>
<li><a href="/yazarlar.html"><span>Yazarlar</span></a>
</li>
<li><a href="/dizi/ayin-indirimli-kitaplari/113"><span>İndirimdekiler</span></a>
</li>
<li><a href="http://ayrintidergi.com.tr/" target="_blank"><span>Ayrıntı Dergi</span></a>
</li>
<li><a href="/iletisim.html"><span>İletişim</span></a>
</li>
<li><a href="/about-us"><span>About Us</span></a>
</li>
<li><a href="/duyurular"><span>Duyurular</span></a>
</li>
</ul>
<div class="cart_box_fixed"><a href="https://www.ayrintiyayinlari.com.tr/cart"><span class="fa fa-shopping-cart"></span></a></div>
<div class="clear"></div>
</div>
</nav>
</div>
<div class="col3">
<div class="banner_header">
<a href="/dizi/dinozor-cocuk/41">
<img alt="Dinozor Çocuk Header" class="banner_img lazy" data-src="/u/ayrintiyayinlari/banners/a/d/i/dinazor-cocuk-1580291148.png" height="86" src="/i/1x1.png" width="75"/>
</a>
<a href="/dizi/dinozor-genc/116">
<img alt="Dinozor Çocuk Header2" class="banner_img lazy" data-src="/u/ayrintiyayinlari/banners/a/d/i/dinozor-genc-1-1580735830.png" height="86" src="/i/1x1.png" width="75"/>
</a>
</div>
</div>
<div class="clear"></div>
<div class="search_box" data-error-text="Arama için en az 3 karakter girmelisiniz.">
<form action="https://www.ayrintiyayinlari.com.tr/index.php?" onsubmit="return checkSearchForm(this);">
<input name="p" type="hidden" value="Products"/>
<input id="q_field_active" name="q_field_active" type="hidden" value="0"/>
<div class="search_area">
<select class="inp_select" name="ctg_id">
<option value="">Tüm kategorilerde</option>
<option value="113">Ayın İndirimli Kitapları</option>
<option value="4">Biyografi-Otobiyografi</option>
<option value="6">Dünya Sorunları</option>
<option value="36">Editörün Seçtikleri</option>
<option value="10">İdea</option>
<option value="112">İndirimli Kitap Setleri</option>
<option value="50">Kara Kitaplar</option>
<option value="12">Lacivert Kitaplar</option>
<option value="39">Pedagoji</option>
<option value="13">Sanat ve Kuram</option>
<option value="14">Schola</option>
<option value="15">Seçme Yazılar</option>
<option value="54">Şiir</option>
<option value="9">Tarih</option>
<option value="17">Türkçe Edebiyat</option>
<option value="99">Türkçe Klasik</option>
<option value="18">Yakın Tarih</option>
<option value="3">Ağır Kitaplar</option>
<option value="42">Ayrıntı Dergi</option>
<option value="73">Beyaz Kitaplar</option>
<option value="93">Bilim</option>
<option value="104">Bilimkurgu</option>
<option value="41">Dinozor Çocuk</option>
<option value="7">Edebiyat</option>
<option value="8">Felsefe</option>
<option value="11">İnceleme</option>
<option value="111">Kadın</option>
<option value="67">Klasik</option>
<option value="51">Sarı Kitaplar</option>
<option value="16">Sinema</option>
<option value="19">Yeraltı Edebiyatı</option>
<option value="116">Dinozor Genç</option>
</select>
<input class="inp_text inp_search" data-container="search_box" id="qsearch" name="q" placeholder="Arama..." type="text" value=""/>
<button class="button button_search" name="search" type="submit">Ara</button>
</div>
<a class="adv_search" href="https://www.ayrintiyayinlari.com.tr/index.php?p=Products&amp;search_form=1" rel="nofollow">Detaylı Arama</a>
<input id="q_field" name="q_field" type="hidden" value=""/>
</form>
<div class="clear"></div>
</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
</header>
<main class="content_body">
<div class="clear"></div>
<div class="cw layout_010 layout_vit">
<div>
<div class="mod_container " id="mod_container_132">
<style>
@media only screen and (min-width: 768px) {
#mod_container_132 .tabs_view_splitter.tabs_view_splitter_2 .tabs_view_splitter_nav>li.tabs_view_splitter_col1{
	width: 40%;
}
#mod_container_132 .tabs_view_splitter.tabs_view_splitter_2 .tabs_view_splitter_nav>li.tabs_view_splitter_col2{
	width: 60%;
}

}
</style>
<div class="box tabs_view_splitter tabs_view_splitter_2">
<ul class="tabs_view_splitter_nav">
<li class="tabs_view_splitter_col1">
<div class="">
<div class="banner_wrapper"><div style="background: #f6e253; padding: 0.3em 1em; max-width: 400px">75 TL ve üzeri siparişlerde ücretsiz kargo ve hediye kitap!</div></div>
<div class="clear"></div>
</div>
</li>
<li class="tabs_view_splitter_col2">
<div class="">
<div class="banner_wrapper"><div style="background: #FF0033;color:#fff; padding: 0.3em 1em; ">Koronavirüs tehdidi nedeniyle siparişleriniz Pazartesi, Çarşamba ve Cuma günleri kargoya verilmektedir.
</div></div>
<div class="clear"></div>
</div>
</li>
</ul>
<div class="clear"></div>
</div>
</div>
<div class="mod_container " id="mod_container_17"><div class="banner_wrapper">
<div class="slide_banner_wrapper">
<div class="cycle-slideshow-wrapper">
<div class="cycle-slideshow-prev" id="cycle-slideshow-prev31"><span class="fa fa-angle-left"></span></div>
<div class="cycle-slideshow-next" id="cycle-slideshow-next31"><span class="fa fa-angle-right"></span></div>
<div class="cycle-slideshow" data-cycle-auto-height="calc" data-cycle-fx="scrollHorz" data-cycle-log="false" data-cycle-next="#cycle-slideshow-next31" data-cycle-pager="#cycle-slideshow-pager31" data-cycle-pager-template="&lt;a href='#'&gt;&lt;span&gt;{{slideNum}}&lt;/span&gt;&lt;/a&gt;" data-cycle-pause-on-hover="true" data-cycle-prev="#cycle-slideshow-prev31" data-cycle-slides="&gt; a" data-cycle-speed="500" data-cycle-swipe="true" data-cycle-swipe-fx="scrollHorz" data-cycle-timeout="5000" id="cycle-slideshow31">
<a href="https://www.ayrintiyayinlari.com.tr/dizi/indirimli-kitap-setleri/112">
<img alt="İndirimli Kitap Setleri" class="lazy" data-src="/u/ayrintiyayinlari/banners/a/y/e/yeniyil-indirim-1609268436.png" height="403" src="/i/1x1.png" width="1180"/>
</a>
<a href="https://www.ayrintiyayinlari.com.tr/kitap/her-yer-seri-direnis/1595">
<img alt="Her Yer Seri Direnis" class="lazy" data-src="/u/ayrintiyayinlari/banners/a/h/e/her-yer-seri-direnis-banner-1609268031.png" height="403" src="/i/1x1.png" width="1180"/>
</a>
<a href="https://www.ayrintiyayinlari.com.tr/kitap/iscilerin-hazirani/1528">
<img alt="İscilerin Hazirani" class="lazy" data-src="/u/ayrintiyayinlari/banners/a/i/s/iscilerin-hazirani-banner-1609267981.png" height="403" src="/i/1x1.png" width="1180"/>
</a>
<a href="https://www.ayrintiyayinlari.com.tr/kitap/pandemi-ve-covid-19/1596">
<img alt="Pandemi ve Covid 19" class="lazy" data-src="/u/ayrintiyayinlari/banners/a/p/a/pandemi-ce-covid-banner-1608195085.png" height="403" src="/i/1x1.png" width="1180"/>
</a>
</div>
<div class="cycle-slideshow-nav">
<span class="cycle-slideshow-pager" id="cycle-slideshow-pager31"></span>
</div>
</div>
</div>
</div>
</div>
<div class="mod_container " id="mod_container_117">
<div class="box box_prd box_prd_slide box_prd_slide_col6">
<div class="box_header"><a href="https://www.ayrintiyayinlari.com.tr/dinozor-cocuk">Dinozor Çocuk</a></div>
<div class="box_content cycle-slideshow-wrapper">
<div class="cycle-slideshow-prev cycle-slideshow-prev117"><span class="fa fa-angle-left"></span></div>
<div class="cycle-slideshow-next cycle-slideshow-next117"><span class="fa fa-angle-right"></span></div>
<ul class="cycle-slideshow" data-allow-wrap="true" data-carousel-fluid="true" data-cycle-carousel-initial-visible="6" data-cycle-carousel-visible="6" data-cycle-fx="carousel" data-cycle-log="false" data-cycle-next=".cycle-slideshow-next117" data-cycle-pause-on-hover="true" data-cycle-prev=".cycle-slideshow-prev117" data-cycle-slides="&gt; li" data-cycle-speed="500" data-cycle-swipe="true" data-cycle-swipe-fx="carousel" data-cycle-timeout="3000">
<li class="">
<div class="home_item_prd home_item_prd_a home_item_prd_1530" data-prd-box-no="" data-prd-id="1530">
<div class="image_container">
<div class="image image_a">
<div class="discount" data-discount="25"><sub>%</sub><span>25</span></div>
<a class="tooltip-ajax" href="https://www.ayrintiyayinlari.com.tr/kitap/happa-ninenin-masallari/1530" title="Happa Nine'nin Masalları">
<img alt="Happa Nine'nin Masalları" class="prd_img prd_img_117_0_1530 lazy" data-src="https://www.ayrintiyayinlari.com.tr/u/ayrintiyayinlari/img/a/h/a/happa-ninenin-masallari-1592473549.jpg" height="100" src="/i/1x1.png" title="Happa Nine'nin Masalları" width="100"/>
</a>
</div>
</div>
<div class="prd_info">
<div class="name"><a href="https://www.ayrintiyayinlari.com.tr/kitap/happa-ninenin-masallari/1530">Happa Nine'nin Masalları</a></div>
<div class="writer"><a href="https://www.ayrintiyayinlari.com.tr/kisi/sabahat-akkiraz/10927">Sabahat Akkiraz</a></div>
<div class="price_box">
<span class="price price_list convert_cur" data-cur-code="TL" data-price="16.00">16<sup>,00</sup>TL</span>
<span class="price price_sale convert_cur" data-cur-code="TL" data-price="12.00">12<sup>,00</sup>TL</span>
</div>
<div class="actions">
<a class="button button_add_to_cart" data-prd-id="1530"><span class="button-text">Sepete Ekle</span></a>
</div>
</div>
</div>
</li>
<li class="">
<div class="home_item_prd home_item_prd_a home_item_prd_1524" data-prd-box-no="" data-prd-id="1524">
<div class="image_container">
<div class="image image_a">
<div class="discount" data-discount="25"><sub>%</sub><span>25</span></div>
<a class="tooltip-ajax" href="https://www.ayrintiyayinlari.com.tr/kitap/peki-ama-kim-bu-marie-curie/1524" title="Peki Ama Kim Bu Marie Curie?">
<img alt="Peki Ama Kim Bu Marie Curie?" class="prd_img prd_img_117_1_1524 lazy" data-src="https://www.ayrintiyayinlari.com.tr/u/ayrintiyayinlari/img/a/m/a/marie-curie-kapak-1590662687.jpg" height="100" src="/i/1x1.png" title="Peki Ama Kim Bu Marie Curie?" width="100"/>
</a>
</div>
</div>
<div class="prd_info">
<div class="name"><a href="https://www.ayrintiyayinlari.com.tr/kitap/peki-ama-kim-bu-marie-curie/1524">Peki Ama Kim Bu Marie Curie?</a></div>
<div class="writer"><a href="https://www.ayrintiyayinlari.com.tr/kisi/giulia-calandra-buonaura/10912">Giulia Calandra Buonaura</a></div>
<div class="price_box">
<span class="price price_list convert_cur" data-cur-code="TL" data-price="29.00">29<sup>,00</sup>TL</span>
<span class="price price_sale convert_cur" data-cur-code="TL" data-price="21.75">21<sup>,75</sup>TL</span>
</div>
<div class="actions">
<a class="button button_add_to_cart" data-prd-id="1524"><span class="button-text">Sepete Ekle</span></a>
</div>
</div>
</div>
</li>
<li class="">
<div class="home_item_prd home_item_prd_a home_item_prd_1518" data-prd-box-no="" data-prd-id="1518">
<div class="image_container">
<div class="image image_a">
<div class="discount" data-discount="25"><sub>%</sub><span>25</span></div>
<a class="tooltip-ajax" href="https://www.ayrintiyayinlari.com.tr/kitap/kizlar-ve-oglanlar-esit-midir/1518" title="Kızlar ve Oğlanlar Eşit midir?">
<img alt="Kızlar ve Oğlanlar Eşit midir?" class="prd_img prd_img_117_2_1518 lazy" data-src="https://www.ayrintiyayinlari.com.tr/u/ayrintiyayinlari/img/a/1/2/123-basla-serisi-kizlar-ve-oglanlar-1588577102.jpg" height="100" src="/i/1x1.png" title="Kızlar ve Oğlanlar Eşit midir?" width="100"/>
</a>
</div>
</div>
<div class="prd_info">
<div class="name"><a href="https://www.ayrintiyayinlari.com.tr/kitap/kizlar-ve-oglanlar-esit-midir/1518">Kızlar ve Oğlanlar Eşit midir?</a></div>
<div class="writer"><a href="https://www.ayrintiyayinlari.com.tr/kisi/stephanie-duval/10918">Stéphanie Duval</a></div>
<div class="price_box">
<span class="price price_list convert_cur" data-cur-code="TL" data-price="12.00">12<sup>,00</sup>TL</span>
<span class="price price_sale convert_cur" data-cur-code="TL" data-price="9.00">9<sup>,00</sup>TL</span>
</div>
<div class="actions">
<a class="button button_add_to_cart" data-prd-id="1518"><span class="button-text">Sepete Ekle</span></a>
</div>
</div>
</div>
</li>
<li class="">
<div class="home_item_prd home_item_prd_a home_item_prd_1526" data-prd-box-no="" data-prd-id="1526">
<div class="image_container">
<div class="image image_a">
<div class="discount" data-discount="25"><sub>%</sub><span>25</span></div>
<a class="tooltip-ajax" href="https://www.ayrintiyayinlari.com.tr/kitap/black-canary/1526" title="Black Canary">
<img alt="Black Canary" class="prd_img prd_img_117_3_1526 lazy" data-src="https://www.ayrintiyayinlari.com.tr/u/ayrintiyayinlari/img/a/b/l/blackcanary-1590750533.jpg" height="100" src="/i/1x1.png" title="Black Canary" width="100"/>
</a>
</div>
</div>
<div class="prd_info">
<div class="name"><a href="https://www.ayrintiyayinlari.com.tr/kitap/black-canary/1526">Black Canary</a></div>
<div class="writer"><a href="https://www.ayrintiyayinlari.com.tr/kisi/meg-cabot/10923">Meg Cabot</a></div>
<div class="price_box">
<span class="price price_list convert_cur" data-cur-code="TL" data-price="33.00">33<sup>,00</sup>TL</span>
<span class="price price_sale convert_cur" data-cur-code="TL" data-price="24.75">24<sup>,75</sup>TL</span>
</div>
<div class="actions">
<a class="button button_add_to_cart" data-prd-id="1526"><span class="button-text">Sepete Ekle</span></a>
</div>
</div>
</div>
</li>
<li class="">
<div class="home_item_prd home_item_prd_a home_item_prd_1525" data-prd-box-no="" data-prd-id="1525">
<div class="image_container">
<div class="image image_a">
<div class="discount" data-discount="25"><sub>%</sub><span>25</span></div>
<a class="tooltip-ajax" href="https://www.ayrintiyayinlari.com.tr/kitap/steam-sehirde/1525" title="STEAM Şehirde">
<img alt="STEAM Şehirde" class="prd_img prd_img_117_4_1525 lazy" data-src="https://www.ayrintiyayinlari.com.tr/u/ayrintiyayinlari/img/a/s/t/steam-sehirde-on-kapak-1590663745.jpg" height="100" src="/i/1x1.png" title="STEAM Şehirde" width="100"/>
</a>
</div>
</div>
<div class="prd_info">
<div class="name"><a href="https://www.ayrintiyayinlari.com.tr/kitap/steam-sehirde/1525">STEAM Şehirde</a></div>
<div class="writer"><a href="https://www.ayrintiyayinlari.com.tr/kisi/vivet-pitelon-sparkes/1603">Vivet Pitelon Sparkes</a></div>
<div class="price_box">
<span class="price price_list convert_cur" data-cur-code="TL" data-price="25.00">25<sup>,00</sup>TL</span>
<span class="price price_sale convert_cur" data-cur-code="TL" data-price="18.75">18<sup>,75</sup>TL</span>
</div>
<div class="actions">
<a class="button button_add_to_cart" data-prd-id="1525"><span class="button-text">Sepete Ekle</span></a>
</div>
</div>
</div>
</li>
<li class="">
<div class="home_item_prd home_item_prd_a home_item_prd_1506" data-prd-box-no="" data-prd-id="1506">
<div class="image_container">
<div class="image image_a">
<div class="discount" data-discount="25"><sub>%</sub><span>25</span></div>
<a class="tooltip-ajax" href="https://www.ayrintiyayinlari.com.tr/kitap/mikroplar-zararli-midir/1506" title="Mikroplar Zararlı mıdır?">
<img alt="Mikroplar Zararlı mıdır?" class="prd_img prd_img_117_5_1506 lazy" data-src="https://www.ayrintiyayinlari.com.tr/u/ayrintiyayinlari/img/a/m/i/mikroplarzararlimidir-1584102569.jpg" height="100" src="/i/1x1.png" title="Mikroplar Zararlı mıdır?" width="100"/>
</a>
</div>
</div>
<div class="prd_info">
<div class="name"><a href="https://www.ayrintiyayinlari.com.tr/kitap/mikroplar-zararli-midir/1506">Mikroplar Zararlı mıdır?</a></div>
<div class="writer"><a href="https://www.ayrintiyayinlari.com.tr/kisi/anne-olliver/10911">Anne Olliver</a></div>
<div class="price_box">
<span class="price price_list convert_cur" data-cur-code="TL" data-price="12.00">12<sup>,00</sup>TL</span>
<span class="price price_sale convert_cur" data-cur-code="TL" data-price="9.00">9<sup>,00</sup>TL</span>
</div>
<div class="actions">
<a class="button button_add_to_cart" data-prd-id="1506"><span class="button-text">Sepete Ekle</span></a>
</div>
</div>
</div>
</li>
<li class="">
<div class="home_item_prd home_item_prd_a home_item_prd_1503" data-prd-box-no="" data-prd-id="1503">
<div class="image_container">
<div class="image image_a">
<div class="discount" data-discount="25"><sub>%</sub><span>25</span></div>
<a class="tooltip-ajax" href="https://www.ayrintiyayinlari.com.tr/kitap/darwin/1503" title="Darwin">
<img alt="Darwin" class="prd_img prd_img_117_6_1503 lazy" data-src="https://www.ayrintiyayinlari.com.tr/u/ayrintiyayinlari/img/a/d/a/darwin-yeni-1584100490.jpg" height="100" src="/i/1x1.png" title="Darwin" width="100"/>
</a>
</div>
</div>
<div class="prd_info">
<div class="name"><a href="https://www.ayrintiyayinlari.com.tr/kitap/darwin/1503">Darwin</a></div>
<div class="writer"><a href="https://www.ayrintiyayinlari.com.tr/kisi/edity-pauly/10908">Édity Pauly</a></div>
<div class="price_box">
<span class="price price_list convert_cur" data-cur-code="TL" data-price="24.00">24<sup>,00</sup>TL</span>
<span class="price price_sale convert_cur" data-cur-code="TL" data-price="18.00">18<sup>,00</sup>TL</span>
</div>
<div class="actions">
<a class="button button_add_to_cart" data-prd-id="1503"><span class="button-text">Sepete Ekle</span></a>
</div>
</div>
</div>
</li>
</ul>
<div class="clear"></div>
<div>
<a class="b_more_link" href="https://www.ayrintiyayinlari.com.tr/dinozor-cocuk">Tümünü göster</a>
</div>
<div class="clear"></div>
</div>
<div class="box_footer"></div>
</div>
</div>
<div class="mod_container " id="mod_container_118">
<div class="box writers_box">
<div class="box_header"><a href="/yazarlar.html">Yazarlar</a></div>
<div class="box_content">
<div class="wrt_item">
<div class="wrt_photo_box"><a href="https://www.ayrintiyayinlari.com.tr/kisi/samuel-beckett/2" title="Samuel Beckett"><img alt="Samuel Beckett" height="110" src="/u/ayrintiyayinlari/wrt/b/beckett-site-1582293504.jpg" width="110"/></a></div>
<div class="wrt_name"><a href="https://www.ayrintiyayinlari.com.tr/kisi/samuel-beckett/2" title="Samuel Beckett">Samuel Beckett</a></div>
</div>
<div class="wrt_item">
<div class="wrt_photo_box"><a href="https://www.ayrintiyayinlari.com.tr/kisi/michel-foucault/15" title="Michel Foucault"><img alt="Michel Foucault" height="110" src="/u/ayrintiyayinlari/wrt/b/michel-foucault303916710.jpg" width="110"/></a></div>
<div class="wrt_name"><a href="https://www.ayrintiyayinlari.com.tr/kisi/michel-foucault/15" title="Michel Foucault">Michel Foucault</a></div>
</div>
<div class="wrt_item">
<div class="wrt_photo_box"><a href="https://www.ayrintiyayinlari.com.tr/kisi/ursula-kroeber-le-guin/252" title="Ursula Kroeber Le Guin"><img alt="Ursula Kroeber Le Guin" height="110" src="/u/ayrintiyayinlari/wrt/b/ursula-site-1582293673.jpg" width="110"/></a></div>
<div class="wrt_name"><a href="https://www.ayrintiyayinlari.com.tr/kisi/ursula-kroeber-le-guin/252" title="Ursula Kroeber Le Guin">Ursula Kroeber Le Guin</a></div>
</div>
<div class="wrt_item">
<div class="wrt_photo_box"><a href="https://www.ayrintiyayinlari.com.tr/kisi/richard-sennett/92" title="Richard Sennett"><img alt="Richard Sennett" height="110" src="/u/ayrintiyayinlari/wrt/b/sennett-site-1582725954.jpg" width="110"/></a></div>
<div class="wrt_name"><a href="https://www.ayrintiyayinlari.com.tr/kisi/richard-sennett/92" title="Richard Sennett">Richard Sennett</a></div>
</div>
<div class="wrt_item">
<div class="wrt_photo_box"><a href="https://www.ayrintiyayinlari.com.tr/kisi/iris-murdoch/33" title="Iris Murdoch"><img alt="Iris Murdoch" height="110" src="/u/ayrintiyayinlari/wrt/b/irismurdoch-site-1582726001.jpg" width="110"/></a></div>
<div class="wrt_name"><a href="https://www.ayrintiyayinlari.com.tr/kisi/iris-murdoch/33" title="Iris Murdoch">Iris Murdoch</a></div>
</div>
<div class="clear"></div>
</div>
<div class="box_footer"></div>
</div>
</div>
<div class="mod_container " id="mod_container_15">
<div class="box news_box">
<div class="box_header"><a href="https://www.ayrintiyayinlari.com.tr/index.php?p=News&amp;ctg_id=1">Haberler</a></div>
<div class="box_content">
<div class="nws_item">
<div class="nws_image"><a href="https://www.ayrintiyayinlari.com.tr/gercek-uzmanlarin-pandemi-yorumlari"><img alt="Gerçek Uzmanların Pandemi Yorumları" src="/u/ayrintiyayinlari/news/a/covid-haberler-1608727752.jpg"/></a></div>
<div class="nws_info">
<a class="nws_title" href="https://www.ayrintiyayinlari.com.tr/gercek-uzmanlarin-pandemi-yorumlari">Gerçek Uzmanların Pandemi Yorumları</a>
<div class="nws_spot"><p>Farklı disiplinlerden uzmanların yazılarının yer aldığı <em>Pandemi ve COVID-19</em>’da, içinde sıkışıp kaldığımız krizi ve bunun nedenlerini, çözüme yönelik kimi açılımları, krizin ekonomik, psikolojik, felsefi, sosyal ve kültürel boyutlarını ortaya koyan makaleler yer alıyor.</p></div>
</div>
<div class="clear"></div>
</div>
<div class="nws_item">
<div class="nws_image"><a href="https://www.ayrintiyayinlari.com.tr/huseyin-gibi-olmak"><img alt='"Hüseyin gibi olmak."' src="/u/ayrintiyayinlari/news/a/1ejzqtssihcgzztx9iiwgoawjiary7nthhuhj5yi-1603791559.jpg"/></a></div>
<div class="nws_info">
<a class="nws_title" href="https://www.ayrintiyayinlari.com.tr/huseyin-gibi-olmak">"Hüseyin gibi olmak."</a>
<div class="nws_spot"><p><em>“Mutluluk insan eli değmemiş bir kayadır köyceğizde ya da güzel kadınların öldüresiye çiğnedikleri bir kaldırım taşıdır kentin alabildiğine kalabalık, üstüne kirli yağmurlar yağan bir bulvarında. Oysa biz insanız. Taş olma olanağını çoktan yitirdik.”</em></p></div>
</div>
<div class="clear"></div>
</div>
<div class="nws_item">
<div class="nws_image"><a href="https://www.ayrintiyayinlari.com.tr/fotograflanmis-devrim"><img alt="Fotoğraflanmış Devrim" src="/u/ayrintiyayinlari/news/a/devrimler-haberler-1582727144.jpg"/></a></div>
<div class="nws_info">
<a class="nws_title" href="https://www.ayrintiyayinlari.com.tr/fotograflanmis-devrim">Fotoğraflanmış Devrim</a>
<div class="nws_spot"><p>Devrimlerin fotoğrafları kuvvetli bir ütopik yüke sahiptir. Onları her daim güncel, her daim yıkıcı kılan sihirli yahut peygamberimsî bir niteliği gözlemcinin dikkatli bakışına sunar. Bizlere hem geçmişten hem de mümkün bir gelecekten bahsederler.</p></div>
</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
</div>
<div class="box_footer"></div>
</div>
</div>
</div>
</div>
<div class="content_body_cw cw layout_011" id="layout_style">
<div class="main_content">
<div class="mod_container " id="mod_container_27">
</div>
<div class="mod_container " id="mod_container_16">
</div>
</div>
<div class="side_column right_column">
<div class="mod_container " id="mod_container_56">
</div>
<div class="mod_container " id="mod_container_48">
</div>
</div>
<div class="clear"></div>
</div>
</main>
<div class="clear"></div>
<footer class="footer">
<div id="dump"></div>
<div class="cw">
<div>
<div class="footer_content">
<div class="col1">
<div class="logo_footer"><img alt="www.ayrintiyayinlari.com.tr" src="/u/ayrintiyayinlari/ayrinti-logo-135-1561531123.png"/></div>
</div>
<div class="col2">
<div class="footer_menu">
<ul>
<li><a href="/gizlilik-bildirimi"><span>Gizlilik Bildirimi</span></a>
</li>
<li><a href="/ayrintiya-dair"><span>Ayrıntı'ya Dair</span></a>
</li>
<li><a href="/about-us"><span>About Us</span></a>
</li>
<li><a href="/satis-sozlesmesi"><span>Satış Sözleşmesi</span></a>
</li>
<li><a href="/iletisim.html"><span>İletişim</span></a>
</li>
</ul>
</div>
<div class="clear"></div>
</div>
<div class="col3">
<div class="eml_form_container">
<div id="email_list_container">
<div class="box eml_box">
<div class="box_header eml_header"><a href="javascript:void(0);">Email Listesi</a></div>
<div class="box_content">
<div class="email_list_desc">E-Mail listemize katılın.</div>
<form action="https://www.ayrintiyayinlari.com.tr/index.php?p=EmailList&amp;popup=1&amp;no_common=1" class="edit_form" data-container="#mod_container_eml_form_container" method="post" onsubmit="$(this).loadPageSubmit();return false;">
<div class="error"></div>
<div class="inp_container">
<input class="inp_text" id="eml_email" maxlength="255" name="eml_email" placeholder="Email" size="15" type="text" value=""/>
<input class="button" id="save" name="save" type="submit" value="Kaydet"/>
</div>
</form>
</div>
<div class="box_footer"></div>
</div>
</div>
</div>
</div>
<div class="clear"></div>
<div class="banner_footer">
<img alt="eticaret visa master" src="/i/visa_master.png"/>
</div>
<div class="copyright">© 2021 www.ayrintiyayinlari.com.tr Tüm hakları saklıdır.</div>
<div class="clear"></div>
</div>
</div>
</div>
<div class="toTop"><span class="fa fa-arrow-up"></span></div>
<div class="dy_logo"><a href="https://www.dokuzyazilim.com" target="_blank" title="E-Ticaret">E-ticaret</a> <a href="http://www.dokuzyazilim.com" target="_blank" title="E-Ticaret"><img alt="E-Ticaret" height="26" src="/i/dokuzyazilim_logo.png" width="94"/></a></div>
</footer>
<div style="display:none;">
<div class="splash_banner"></div>
<div id="dummy_elm"></div><div id="label_close">Kapat</div><div class="button_adding_to_cart"></div><div class="button_added_to_cart"></div></div>
</div>
</div>
</div>
</div>
</div>
<form action="https://www.ayrintiyayinlari.com.tr/">
<input id="http_url" type="hidden" value="https://www.ayrintiyayinlari.com.tr/"/>
<input id="https_url" type="hidden" value="https://www.ayrintiyayinlari.com.tr/"/>
<input id="label_add_to_cart" type="hidden" value="Sepete Ekle"/>
<input id="label_adding" type="hidden" value="Ekleniyor"/>
<input id="label_added" type="hidden" value="Eklendi"/>
<input id="user_discount" type="hidden" value="0"/>
<input id="user_discount_type" type="hidden" value=""/>
<input id="prd_id_discount_url" type="hidden" value="https://www.ayrintiyayinlari.com.tr/index.php?p=Products&amp;get_special_price=1"/>
<input id="csrf_token" type="hidden" value="81537c8a1d2758dd121c8fb408b53199"/>
</form>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/><link href="https://fonts.googleapis.com/css?family=Muli&amp;display=swap" rel="stylesheet"/>
<script src="/u/ayrintiyayinlari/combine.js?v=307"></script>
<script src="/js/facebook_event_codes.js"></script>
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<!--  Bu site Dokuz Yazılım Eticaret Paketi kullanılarak yapılmıştır  -->
<!-- -->
<!--             @ Copyright 2021-->
<!--       Programın tüm hakları Dokuz Yazılım adına kayıtlıdır.      -->
<!--               İrtibat İçin: www.dokuzyazilim.com	              -->
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
</body>
</html>
