<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8"/>
<script>
        var langs = '{"en":{"code":"en","id":"1","native_name":"English","major":"1","active":0,"default_locale":"en_US","encode_url":"0","tag":"en","translated_name":"Inglese","url":"https:\/\/www.altran.com\/it\/en\/","country_flag_url":"https:\/\/www.altran.com\/as-content\/plugins\/sitepress-multilingual-cms\/res\/flags\/en.png","language_code":"en"},"it":{"code":"it","id":"27","native_name":"Italiano","major":"1","active":"1","default_locale":"it_IT","encode_url":"0","tag":"it","translated_name":"Italiano","url":"https:\/\/www.altran.com\/it\/it\/","country_flag_url":"https:\/\/www.altran.com\/as-content\/plugins\/sitepress-multilingual-cms\/res\/flags\/it.png","language_code":"it"}}';
        var default_lang = 'it';

        function getLanguage(language) {
            if (language.indexOf('-') > -1) {
                language = language.substring(0, language.indexOf('-'));
            }

            // cas particulier NORWAY
            if (('nb' === language) || ('nn' === language)) {
                language = 'no';
            }

            return language;
        }

        //Si InternetExplorer v. < 11
        if (navigator.browserLanguage) {
            var x = navigator.browserLanguage;
        } else {
            var x = navigator.language;
        }

        var current_lang = getLanguage(x);
        var found = false;

        var obj = JSON.parse(langs);

        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                if (key == current_lang) {
                    found = true;
                    document.location.href = obj[key]['url'];
                }
            }
        }

        if (!found) {
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    if (key == default_lang) {
                        found = true;
                        document.location.href = obj[key]['url'];
                    }
                }
            }
        }
    </script>
</head>
<body>
</body>
</html>
<!-- This website is like a Rocket, isn't it? Performance optimized by WP Rocket. Learn more: https://wp-rocket.me - Debug: cached@1610033566 -->