<!DOCTYPE html>
<html lang="en-US" prefix="og: https://ogp.me/ns#">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<title>Page not found – Bohol Tours Including Bohol Travel and Vacation Packages</title>
<!-- All in One SEO 4.0.12 -->
<meta content="noindex" name="robots"/>
<meta content="rJDZxk8RmWiKeEBQyomIuhakhGyA9CYZ43I-vCjMfds" name="google-site-verification"/>
<link href="https://www.traveltourbohol.com/hjdtyrfdges/t-online.de/%09/" rel="canonical"/>
<meta content="nositelinkssearchbox" name="google"/>
<script class="aioseo-schema" type="application/ld+json">
			{"@context":"https:\/\/schema.org","@graph":[{"@type":"WebSite","@id":"https:\/\/www.traveltourbohol.com\/#website","url":"https:\/\/www.traveltourbohol.com\/","name":"Bohol Tours Including Bohol Travel and Vacation Packages","description":"We offer great Bohol tour destinations around the island of Bohol, Philippines. Tour with the most knowledgeable and oldest travel and tours agency in Bohol.","publisher":{"@id":"https:\/\/www.traveltourbohol.com\/#organization"}},{"@type":"Organization","@id":"https:\/\/www.traveltourbohol.com\/#organization","name":"Bohol Tours Including Bohol Travel and Vacation Packages","url":"https:\/\/www.traveltourbohol.com\/","logo":{"@type":"ImageObject","@id":"https:\/\/www.traveltourbohol.com\/#organizationLogo","url":"https:\/\/www.traveltourbohol.com\/wp-content\/uploads\/2016\/05\/cropped-header-new.png","width":916,"height":280},"image":{"@id":"https:\/\/www.traveltourbohol.com\/#organizationLogo"}},{"@type":"BreadcrumbList","@id":"https:\/\/www.traveltourbohol.com\/hjdtyrfdges\/t-online.de\/%09%0A\/#breadcrumblist","itemListElement":[{"@type":"ListItem","@id":"https:\/\/www.traveltourbohol.com\/#listItem","position":1,"item":{"@type":"WebPage","@id":"https:\/\/www.traveltourbohol.com\/#item","name":"Home","description":"We offer great Bohol tours for 2019! New and exciting tour destinations around the island of Bohol, Philippines. Tour with the most knowledgeable and oldest travel and tours agency in Bohol.","url":"https:\/\/www.traveltourbohol.com\/"},"nextItem":"https:\/\/www.traveltourbohol.com\/hjdtyrfdges\/t-online.de\/%09%0A\/#listItem"},{"@type":"ListItem","@id":"https:\/\/www.traveltourbohol.com\/hjdtyrfdges\/t-online.de\/%09%0A\/#listItem","position":2,"item":{"@id":"https:\/\/www.traveltourbohol.com\/hjdtyrfdges\/t-online.de\/%09%0A\/#item","url":"https:\/\/www.traveltourbohol.com\/hjdtyrfdges\/t-online.de\/%09%0A\/"},"previousItem":"https:\/\/www.traveltourbohol.com\/#listItem"}]}]}
		</script>
<!-- All in One SEO -->
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.traveltourbohol.com/feed/" rel="alternate" title="Bohol Tours Including Bohol Travel and Vacation Packages » Feed" type="application/rss+xml"/>
<link href="https://www.traveltourbohol.com/comments/feed/" rel="alternate" title="Bohol Tours Including Bohol Travel and Vacation Packages » Comments Feed" type="application/rss+xml"/>
<script>
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.traveltourbohol.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style>
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.traveltourbohol.com/wp-content/plugins/content-views-query-and-display-post-page/public/assets/css/cv.css?ver=2.3.4" id="pt-cv-public-style-css" media="all" rel="stylesheet"/>
<link href="https://www.traveltourbohol.com/wp-content/themes/astra/assets/css/minified/style.min.css?ver=2.6.2" id="astra-theme-css-css" media="all" rel="stylesheet"/>
<style id="astra-theme-css-inline-css">
html{font-size:100%;}a,.page-title{color:#0274be;}a:hover,a:focus{color:#3a3a3a;}body,button,input,select,textarea,.ast-button,.ast-custom-button{font-family:-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;font-weight:inherit;font-size:16px;font-size:1rem;}blockquote{color:#000000;}.site-title{font-size:19px;font-size:1.1875rem;}.ast-archive-description .ast-archive-title{font-size:40px;font-size:2.5rem;}.site-header .site-description{font-size:15px;font-size:0.9375rem;}.entry-title{font-size:40px;font-size:2.5rem;}.comment-reply-title{font-size:26px;font-size:1.625rem;}.ast-comment-list #cancel-comment-reply-link{font-size:16px;font-size:1rem;}h1,.entry-content h1{font-size:35px;font-size:2.1875rem;}h2,.entry-content h2{font-size:30px;font-size:1.875rem;}h3,.entry-content h3{font-size:25px;font-size:1.5625rem;}h4,.entry-content h4{font-size:20px;font-size:1.25rem;}h5,.entry-content h5{font-size:18px;font-size:1.125rem;}h6,.entry-content h6{font-size:15px;font-size:0.9375rem;}.ast-single-post .entry-title,.page-title{font-size:30px;font-size:1.875rem;}#secondary,#secondary button,#secondary input,#secondary select,#secondary textarea{font-size:16px;font-size:1rem;}::selection{background-color:#0274be;color:#ffffff;}body,h1,.entry-title a,.entry-content h1,h2,.entry-content h2,h3,.entry-content h3,h4,.entry-content h4,h5,.entry-content h5,h6,.entry-content h6,.wc-block-grid__product-title{color:#000000;}.tagcloud a:hover,.tagcloud a:focus,.tagcloud a.current-item{color:#ffffff;border-color:#0274be;background-color:#0274be;}.main-header-menu .menu-link,.ast-header-custom-item a{color:#000000;}.main-header-menu .menu-item:hover > .menu-link,.main-header-menu .menu-item:hover > .ast-menu-toggle,.main-header-menu .ast-masthead-custom-menu-items a:hover,.main-header-menu .menu-item.focus > .menu-link,.main-header-menu .menu-item.focus > .ast-menu-toggle,.main-header-menu .current-menu-item > .menu-link,.main-header-menu .current-menu-ancestor > .menu-link,.main-header-menu .current-menu-item > .ast-menu-toggle,.main-header-menu .current-menu-ancestor > .ast-menu-toggle{color:#0274be;}input:focus,input[type="text"]:focus,input[type="email"]:focus,input[type="url"]:focus,input[type="password"]:focus,input[type="reset"]:focus,input[type="search"]:focus,textarea:focus{border-color:#0274be;}input[type="radio"]:checked,input[type=reset],input[type="checkbox"]:checked,input[type="checkbox"]:hover:checked,input[type="checkbox"]:focus:checked,input[type=range]::-webkit-slider-thumb{border-color:#0274be;background-color:#0274be;box-shadow:none;}.site-footer a:hover + .post-count,.site-footer a:focus + .post-count{background:#0274be;border-color:#0274be;}.footer-adv .footer-adv-overlay{border-top-style:solid;border-top-color:#7a7a7a;}.ast-comment-meta{line-height:1.666666667;font-size:13px;font-size:0.8125rem;}.single .nav-links .nav-previous,.single .nav-links .nav-next,.single .ast-author-details .author-title,.ast-comment-meta{color:#0274be;}.entry-meta,.entry-meta *{line-height:1.45;color:#0274be;}.entry-meta a:hover,.entry-meta a:hover *,.entry-meta a:focus,.entry-meta a:focus *{color:#3a3a3a;}.ast-404-layout-1 .ast-404-text{font-size:200px;font-size:12.5rem;}.widget-title{font-size:22px;font-size:1.375rem;color:#000000;}#cat option,.secondary .calendar_wrap thead a,.secondary .calendar_wrap thead a:visited{color:#0274be;}.secondary .calendar_wrap #today,.ast-progress-val span{background:#0274be;}.secondary a:hover + .post-count,.secondary a:focus + .post-count{background:#0274be;border-color:#0274be;}.calendar_wrap #today > a{color:#ffffff;}.ast-pagination a,.page-links .page-link,.single .post-navigation a{color:#0274be;}.ast-pagination a:hover,.ast-pagination a:focus,.ast-pagination > span:hover:not(.dots),.ast-pagination > span.current,.page-links > .page-link,.page-links .page-link:hover,.post-navigation a:hover{color:#3a3a3a;}.ast-header-break-point .ast-mobile-menu-buttons-minimal.menu-toggle{background:transparent;color:#0274be;}.ast-header-break-point .ast-mobile-menu-buttons-outline.menu-toggle{background:transparent;border:1px solid #0274be;color:#0274be;}.ast-header-break-point .ast-mobile-menu-buttons-fill.menu-toggle{background:#0274be;}.wp-block-buttons.aligncenter{justify-content:center;}@media (max-width:782px){.entry-content .wp-block-columns .wp-block-column{margin-left:0px;}}@media (max-width:768px){#secondary.secondary{padding-top:0;}.ast-separate-container .ast-article-post,.ast-separate-container .ast-article-single{padding:1.5em 2.14em;}.ast-separate-container #primary,.ast-separate-container #secondary{padding:1.5em 0;}.ast-separate-container.ast-right-sidebar #secondary{padding-left:1em;padding-right:1em;}.ast-separate-container.ast-two-container #secondary{padding-left:0;padding-right:0;}.ast-page-builder-template .entry-header #secondary{margin-top:1.5em;}.ast-page-builder-template #secondary{margin-top:1.5em;}#primary,#secondary{padding:1.5em 0;margin:0;}.ast-left-sidebar #content > .ast-container{display:flex;flex-direction:column-reverse;width:100%;}.ast-author-box img.avatar{margin:20px 0 0 0;}.ast-pagination{padding-top:1.5em;text-align:center;}.ast-pagination .next.page-numbers{display:inherit;float:none;}}@media (max-width:768px){.ast-page-builder-template.ast-left-sidebar #secondary{padding-right:20px;}.ast-page-builder-template.ast-right-sidebar #secondary{padding-left:20px;}.ast-right-sidebar #primary{padding-right:0;}.ast-right-sidebar #secondary{padding-left:0;}.ast-left-sidebar #primary{padding-left:0;}.ast-left-sidebar #secondary{padding-right:0;}.ast-pagination .prev.page-numbers{padding-left:.5em;}.ast-pagination .next.page-numbers{padding-right:.5em;}}@media (min-width:769px){.ast-separate-container.ast-right-sidebar #primary,.ast-separate-container.ast-left-sidebar #primary{border:0;}.ast-separate-container.ast-right-sidebar #secondary,.ast-separate-container.ast-left-sidebar #secondary{border:0;margin-left:auto;margin-right:auto;}.ast-separate-container.ast-two-container #secondary .widget:last-child{margin-bottom:0;}.ast-separate-container .ast-comment-list li .comment-respond{padding-left:2.66666em;padding-right:2.66666em;}.ast-author-box{-js-display:flex;display:flex;}.ast-author-bio{flex:1;}.error404.ast-separate-container #primary,.search-no-results.ast-separate-container #primary{margin-bottom:4em;}}@media (min-width:769px){.ast-right-sidebar #primary{border-right:1px solid #eee;}.ast-right-sidebar #secondary{border-left:1px solid #eee;margin-left:-1px;}.ast-left-sidebar #primary{border-left:1px solid #eee;}.ast-left-sidebar #secondary{border-right:1px solid #eee;margin-right:-1px;}.ast-separate-container.ast-two-container.ast-right-sidebar #secondary{padding-left:30px;padding-right:0;}.ast-separate-container.ast-two-container.ast-left-sidebar #secondary{padding-right:30px;padding-left:0;}}.menu-toggle,button,.ast-button,.ast-custom-button,.button,input#submit,input[type="button"],input[type="submit"],input[type="reset"]{color:#ffffff;border-color:#0274be;background-color:#0274be;border-radius:2px;padding-top:10px;padding-right:40px;padding-bottom:10px;padding-left:40px;font-family:inherit;font-weight:inherit;}button:focus,.menu-toggle:hover,button:hover,.ast-button:hover,.button:hover,input[type=reset]:hover,input[type=reset]:focus,input#submit:hover,input#submit:focus,input[type="button"]:hover,input[type="button"]:focus,input[type="submit"]:hover,input[type="submit"]:focus{color:#ffffff;background-color:#3a3a3a;border-color:#3a3a3a;}@media (min-width:768px){.ast-container{max-width:100%;}}@media (min-width:544px){.ast-container{max-width:100%;}}@media (max-width:544px){.ast-separate-container .ast-article-post,.ast-separate-container .ast-article-single{padding:1.5em 1em;}.ast-separate-container #content .ast-container{padding-left:0.54em;padding-right:0.54em;}.ast-separate-container #secondary{padding-top:0;}.ast-separate-container.ast-two-container #secondary .widget{margin-bottom:1.5em;padding-left:1em;padding-right:1em;}.ast-separate-container .comments-count-wrapper{padding:1.5em 1em;}.ast-separate-container .ast-comment-list li.depth-1{padding:1.5em 1em;margin-bottom:1.5em;}.ast-separate-container .ast-comment-list .bypostauthor{padding:.5em;}.ast-separate-container .ast-archive-description{padding:1.5em 1em;}.ast-search-menu-icon.ast-dropdown-active .search-field{width:170px;}.ast-separate-container .comment-respond{padding:1.5em 1em;}}@media (max-width:544px){.ast-comment-list .children{margin-left:0.66666em;}.ast-separate-container .ast-comment-list .bypostauthor li{padding:0 0 0 .5em;}}@media (max-width:768px){.ast-mobile-header-stack .main-header-bar .ast-search-menu-icon{display:inline-block;}.ast-header-break-point.ast-header-custom-item-outside .ast-mobile-header-stack .main-header-bar .ast-search-icon{margin:0;}.ast-comment-avatar-wrap img{max-width:2.5em;}.comments-area{margin-top:1.5em;}.ast-separate-container .comments-count-wrapper{padding:2em 2.14em;}.ast-separate-container .ast-comment-list li.depth-1{padding:1.5em 2.14em;}.ast-separate-container .comment-respond{padding:2em 2.14em;}}@media (max-width:768px){.ast-header-break-point .main-header-bar .ast-search-menu-icon.slide-search .search-form{right:0;}.ast-header-break-point .ast-mobile-header-stack .main-header-bar .ast-search-menu-icon.slide-search .search-form{right:-1em;}.ast-comment-avatar-wrap{margin-right:0.5em;}}@media (min-width:545px){.ast-page-builder-template .comments-area,.single.ast-page-builder-template .entry-header,.single.ast-page-builder-template .post-navigation{max-width:1240px;margin-left:auto;margin-right:auto;}}@media (max-width:768px){.ast-archive-description .ast-archive-title{font-size:40px;}.entry-title{font-size:30px;}h1,.entry-content h1{font-size:30px;}h2,.entry-content h2{font-size:25px;}h3,.entry-content h3{font-size:20px;}.ast-single-post .entry-title,.page-title{font-size:30px;}}@media (max-width:544px){.ast-archive-description .ast-archive-title{font-size:40px;}.entry-title{font-size:30px;}h1,.entry-content h1{font-size:30px;}h2,.entry-content h2{font-size:25px;}h3,.entry-content h3{font-size:20px;}.ast-single-post .entry-title,.page-title{font-size:30px;}}@media (max-width:768px){html{font-size:91.2%;}}@media (max-width:544px){html{font-size:91.2%;}}@media (min-width:769px){.ast-container{max-width:1240px;}}@font-face {font-family: "Astra";src: url(https://www.traveltourbohol.com/wp-content/themes/astra/assets/fonts/astra.woff) format("woff"),url(https://www.traveltourbohol.com/wp-content/themes/astra/assets/fonts/astra.ttf) format("truetype"),url(https://www.traveltourbohol.com/wp-content/themes/astra/assets/fonts/astra.svg#astra) format("svg");font-weight: normal;font-style: normal;font-display: fallback;}@media (max-width:921px) {.main-header-bar .main-header-bar-navigation{display:none;}}.ast-desktop .main-header-menu.submenu-with-border .sub-menu,.ast-desktop .main-header-menu.submenu-with-border .astra-full-megamenu-wrapper{border-color:#000000;}.ast-desktop .main-header-menu.submenu-with-border .sub-menu{border-top-width:2px;border-right-width:0px;border-left-width:0px;border-bottom-width:0px;border-style:solid;}.ast-desktop .main-header-menu.submenu-with-border .sub-menu .sub-menu{top:-2px;}.ast-desktop .main-header-menu.submenu-with-border .sub-menu .menu-link,.ast-desktop .main-header-menu.submenu-with-border .children .menu-link{border-bottom-width:0px;border-style:solid;border-color:#eaeaea;}@media (min-width:769px){.main-header-menu .sub-menu .menu-item.ast-left-align-sub-menu:hover > .sub-menu,.main-header-menu .sub-menu .menu-item.ast-left-align-sub-menu.focus > .sub-menu{margin-left:-0px;}}.ast-small-footer{border-top-style:solid;border-top-width:1px;border-top-color:#7a7a7a;}.ast-small-footer-wrap{text-align:center;}@media (max-width:920px){.ast-404-layout-1 .ast-404-text{font-size:100px;font-size:6.25rem;}}.ast-breadcrumbs .trail-browse,.ast-breadcrumbs .trail-items,.ast-breadcrumbs .trail-items li{display:inline-block;margin:0;padding:0;border:none;background:inherit;text-indent:0;}.ast-breadcrumbs .trail-browse{font-size:inherit;font-style:inherit;font-weight:inherit;color:inherit;}.ast-breadcrumbs .trail-items{list-style:none;}.trail-items li::after{padding:0 0.3em;content:"\00bb";}.trail-items li:last-of-type::after{display:none;}.ast-header-break-point .main-header-bar{border-bottom-width:1px;border-bottom-color:#000000;}@media (min-width:769px){.main-header-bar{border-bottom-width:1px;border-bottom-color:#000000;}}.ast-safari-browser-less-than-11 .main-header-menu .menu-item, .ast-safari-browser-less-than-11 .main-header-bar .ast-masthead-custom-menu-items{display:block;}.main-header-menu .menu-item, .main-header-bar .ast-masthead-custom-menu-items{-js-display:flex;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-moz-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-moz-box-orient:vertical;-moz-box-direction:normal;-ms-flex-direction:column;flex-direction:column;}.main-header-menu > .menu-item > .menu-link{height:100%;-webkit-box-align:center;-webkit-align-items:center;-moz-box-align:center;-ms-flex-align:center;align-items:center;-js-display:flex;display:flex;}.ast-primary-menu-disabled .main-header-bar .ast-masthead-custom-menu-items{flex:unset;}.header-main-layout-1 .ast-flex.main-header-container, .header-main-layout-3 .ast-flex.main-header-container{-webkit-align-content:center;-ms-flex-line-pack:center;align-content:center;-webkit-box-align:center;-webkit-align-items:center;-moz-box-align:center;-ms-flex-align:center;align-items:center;}
</style>
<link href="https://www.traveltourbohol.com/wp-content/themes/astra/assets/css/minified/menu-animation.min.css?ver=2.6.2" id="astra-menu-animation-css" media="all" rel="stylesheet"/>
<link href="https://www.traveltourbohol.com/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet"/>
<link href="https://www.traveltourbohol.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet"/>
<link href="https://www.traveltourbohol.com/wp-content/themes/astra/assets/css/minified/compatibility/contact-form-7.min.css?ver=2.6.2" id="astra-contact-form-7-css" media="all" rel="stylesheet"/>
<link href="https://www.traveltourbohol.com/wp-content/plugins/related-posts-thumbnails/assets/css/front.css?ver=1.6.2" id="rpt_front_style-css" media="all" rel="stylesheet"/>
<link href="https://www.traveltourbohol.com/wp-content/plugins/easy-fancybox/css/jquery.fancybox.min.css?ver=1.3.24" id="fancybox-css" media="screen" rel="stylesheet"/>
<link href="https://www.traveltourbohol.com/wp-content/plugins/add-to-any/addtoany.min.css?ver=1.15" id="addtoany-css" media="all" rel="stylesheet"/>
<link href="https://www.traveltourbohol.com?display_custom_css=css&amp;ver=5.5.3" id="wp-add-custom-css-css" media="all" rel="stylesheet"/>
<!--[if IE]>
<script src='https://www.traveltourbohol.com/wp-content/themes/astra/assets/js/minified/flexibility.min.js?ver=2.6.2' id='astra-flexibility-js'></script>
<script id='astra-flexibility-js-after'>
flexibility(document.documentElement);
</script>
<![endif]-->
<script id="jquery-core-js" src="https://www.traveltourbohol.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"></script>
<script id="addtoany-js" src="https://www.traveltourbohol.com/wp-content/plugins/add-to-any/addtoany.min.js?ver=1.1"></script>
<link href="https://www.traveltourbohol.com/wp-json/" rel="https://api.w.org/"/><link href="https://www.traveltourbohol.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.traveltourbohol.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<script data-cfasync="false">
window.a2a_config=window.a2a_config||{};a2a_config.callbacks=[];a2a_config.overlays=[];a2a_config.templates={};
(function(d,s,a,b){a=d.createElement(s);b=d.getElementsByTagName(s)[0];a.async=1;a.src="https://static.addtoany.com/menu/page.js";b.parentNode.insertBefore(a,b);})(document,"script");
</script>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-7593199406516189",
          enable_page_level_ads: true
     });
</script>
<link href="/wp-content/uploads/fbrfg/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/wp-content/uploads/fbrfg/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/wp-content/uploads/fbrfg/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/wp-content/uploads/fbrfg/manifest.json" rel="manifest"/>
<link color="#5bbad5" href="/wp-content/uploads/fbrfg/safari-pinned-tab.svg" rel="mask-icon"/>
<link href="/wp-content/uploads/fbrfg/favicon.ico" rel="shortcut icon"/>
<meta content="/wp-content/uploads/fbrfg/browserconfig.xml" name="msapplication-config"/>
<meta content="#ffffff" name="theme-color"/> <style>
      #related_posts_thumbnails li{
          border-right: 1px solid #DDDDDD;
          background-color: #FFFFFF      }
      #related_posts_thumbnails li:hover{
          background-color: #EEEEEF;
      }
      .relpost_content{
          font-size: 12px;
          color: #333333;
      }
      .relpost-block-single{
          background-color: #FFFFFF;
          border-right: 1px solid  #DDDDDD;
          border-left: 1px solid  #DDDDDD;
          margin-right: -1px;
      }
      .relpost-block-single:hover{
          background-color: #EEEEEF;
      }
      </style>
<link href="https://www.traveltourbohol.com/wp-content/uploads/2019/03/cropped-tours-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://www.traveltourbohol.com/wp-content/uploads/2019/03/cropped-tours-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://www.traveltourbohol.com/wp-content/uploads/2019/03/cropped-tours-180x180.png" rel="apple-touch-icon"/>
<meta content="https://www.traveltourbohol.com/wp-content/uploads/2019/03/cropped-tours-270x270.png" name="msapplication-TileImage"/>
</head>
<body class="error404 wp-custom-logo ast-desktop ast-separate-container ast-no-sidebar astra-2.6.2 ast-header-custom-item-inside ast-inherit-site-logo-transparent" data-rsssl="1" itemscope="itemscope" itemtype="https://schema.org/WebPage">
<div class="hfeed site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header ast-primary-submenu-animation-slide-down header-main-layout-2 ast-primary-menu-enabled ast-logo-title-inline ast-hide-custom-menu-mobile ast-menu-toggle-icon ast-mobile-header-stack" id="masthead" itemid="#masthead" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
<div class="main-header-bar-wrap">
<div class="main-header-bar">
<div class="ast-container">
<div class="ast-flex main-header-container">
<div class="site-branding">
<div class="ast-site-identity" itemscope="itemscope" itemtype="https://schema.org/Organization">
<span class="site-logo-img"><a class="custom-logo-link" href="https://www.traveltourbohol.com/" rel="home"><img alt="Cropped header new png" class="custom-logo" height="280" sizes="(max-width: 916px) 100vw, 916px" src="https://www.traveltourbohol.com/wp-content/uploads/2016/05/cropped-header-new.png" srcset="https://www.traveltourbohol.com/wp-content/uploads/2016/05/cropped-header-new.png 916w, https://www.traveltourbohol.com/wp-content/uploads/2016/05/cropped-header-new-300x92.png 300w, https://www.traveltourbohol.com/wp-content/uploads/2016/05/cropped-header-new-768x235.png 768w" width="916"/></a></span><div class="ast-site-title-wrap">
<span class="site-title" itemprop="name">
<a href="https://www.traveltourbohol.com/" itemprop="url" rel="home">
					Bohol Tours Including Bohol Travel and Vacation Packages
				</a>
</span>
</div> </div>
</div>
<!-- .site-branding -->
<div class="ast-mobile-menu-buttons">
<div class="ast-button-wrap">
<button aria-controls="primary-menu" aria-expanded="false" class="menu-toggle main-header-menu-toggle ast-mobile-menu-buttons-minimal " type="button">
<span class="screen-reader-text">Main Menu</span>
<span class="menu-toggle-icon"></span>
</button>
</div>
</div>
<div class="ast-main-header-bar-alignment"><div class="main-header-bar-navigation"><nav aria-label="Site Navigation" class="ast-flex-grow-1 navigation-accessibility" id="site-navigation" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement"><div class="main-navigation"><ul class="main-header-menu ast-nav-menu ast-flex ast-justify-content-flex-end submenu-with-border astra-menu-animation-slide-down " id="primary-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-14" id="menu-item-14"><a class="menu-link" href="https://www.traveltourbohol.com/" title="Travel Tours in Bohol Philippines">Home</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-259" id="menu-item-259"><a class="menu-link" href="https://www.traveltourbohol.com/resorts/">Resorts</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-75" id="menu-item-75"><a class="menu-link" href="https://www.traveltourbohol.com/tour-category/" title="Bohol Tour Category">Tour Category</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-13" id="menu-item-13"><a class="menu-link" href="https://www.traveltourbohol.com/bohol-tours-packages/" title="Bohol Tour Destinations">Bohol Tour Destinations</a><button aria-expanded="false" class="ast-menu-toggle"><span class="screen-reader-text">Menu Toggle</span></button>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-27" id="menu-item-27"><a class="menu-link" href="https://www.traveltourbohol.com/chocolate-hills-tour/">Chocolate Hills</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-59" id="menu-item-59"><a class="menu-link" href="https://www.traveltourbohol.com/dolphin-watching-tour/">Dolphin Watching</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-35" id="menu-item-35"><a class="menu-link" href="https://www.traveltourbohol.com/bohol-countryside-tour/">Bohol Countryside</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-39" id="menu-item-39"><a class="menu-link" href="https://www.traveltourbohol.com/loboc-river-cruise-tour/">Loboc River Cruise</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-40" id="menu-item-40"><a class="menu-link" href="https://www.traveltourbohol.com/philippine-tarsier-and-wildlife-sanctuary-tour/">Tarsier and Wildlife Sanctuary</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-47" id="menu-item-47"><a class="menu-link" href="https://www.traveltourbohol.com/tour-packages/">Tour Packages</a><button aria-expanded="false" class="ast-menu-toggle"><span class="screen-reader-text">Menu Toggle</span></button>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-48" id="menu-item-48"><a class="menu-link" href="https://www.traveltourbohol.com/loboc-eco-tourism-adventure-park-tour-package/">Loboc Eco-tourism Tour</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-49" id="menu-item-49"><a class="menu-link" href="https://www.traveltourbohol.com/danao-adventure-park-tour-package/">Danao Adventure Park Tour</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-50" id="menu-item-50"><a class="menu-link" href="https://www.traveltourbohol.com/panglao-island-tour-package/">Panglao Island Tour</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-52" id="menu-item-52"><a class="menu-link" href="https://www.traveltourbohol.com/countryside-tour-package/">Countryside Tour Package</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-51" id="menu-item-51"><a class="menu-link" href="https://www.traveltourbohol.com/countryside-tour-with-sagbayan-peak/">Countryside Tour with Sagbayan Peak</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-20" id="menu-item-20"><a class="menu-link" href="https://www.traveltourbohol.com/booking-reservations/" title="Booking And Reservations">Reservations</a><button aria-expanded="false" class="ast-menu-toggle"><span class="screen-reader-text">Menu Toggle</span></button>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-85" id="menu-item-85"><a class="menu-link" href="https://www.traveltourbohol.com/how-to-get-to-bohol-from-manila-or-cebu/" title="How to get to Bohol From Manila or Cebu">Getting To Bohol</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55" id="menu-item-55"><a class="menu-link" href="https://www.traveltourbohol.com/contact-us/">Contact Us</a></li>
</ul></div></nav></div></div> </div><!-- Main Header Container -->
</div><!-- ast-row -->
</div> <!-- Main Header Bar -->
</div> <!-- Main Header Bar Wrap -->
</header><!-- #masthead -->
<div class="site-content" id="content">
<div class="ast-container">
<div class="content-area primary" id="primary">
<section class="error-404 not-found">
<div class="ast-404-layout-1">
<header class="page-header"><h1 class="page-title">This page doesn't seem to exist.</h1></header><!-- .page-header -->
<div class="page-content">
<div class="page-sub-title">
			It looks like the link pointing here was faulty. Maybe try searching?		</div>
<div class="ast-404-search">
<div class="widget widget_search"><form action="https://www.traveltourbohol.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form></div> </div>
</div><!-- .page-content -->
</div>
</section><!-- .error-404 -->
</div><!-- #primary -->
</div> <!-- ast-container -->
</div><!-- #content -->
<footer class="site-footer" id="colophon" itemid="#colophon" itemscope="itemscope" itemtype="https://schema.org/WPFooter">
<div class="ast-small-footer footer-sml-layout-1">
<div class="ast-footer-overlay">
<div class="ast-container">
<div class="ast-small-footer-wrap">
<div class="ast-small-footer-section ast-small-footer-section-1">
						Copyright © 2021 <span class="ast-footer-site-title">Bohol Tours Including Bohol Travel and Vacation Packages</span> | Powered by <a href="https://wpastra.com/">Astra WordPress Theme</a> </div>
</div><!-- .ast-row .ast-small-footer-wrap -->
</div><!-- .ast-container -->
</div><!-- .ast-footer-overlay -->
</div><!-- .ast-small-footer-->
</footer><!-- #colophon -->
</div><!-- #page -->
<div id="aioseo-admin"></div><script id="astra-theme-js-js-extra">
var astra = {"break_point":"921","isRtl":""};
</script>
<script id="astra-theme-js-js" src="https://www.traveltourbohol.com/wp-content/themes/astra/assets/js/minified/style.min.js?ver=2.6.2"></script>
<script id="contact-form-7-js-extra">
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.traveltourbohol.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"cached":"1"};
</script>
<script id="contact-form-7-js" src="https://www.traveltourbohol.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2"></script>
<script id="pt-cv-content-views-script-js-extra">
var PT_CV_PUBLIC = {"_prefix":"pt-cv-","page_to_show":"5","_nonce":"7b1ae09b33","is_admin":"","is_mobile":"","ajaxurl":"https:\/\/www.traveltourbohol.com\/wp-admin\/admin-ajax.php","lang":"","loading_image_src":"data:image\/gif;base64,R0lGODlhDwAPALMPAMrKygwMDJOTkz09PZWVla+vr3p6euTk5M7OzuXl5TMzMwAAAJmZmWZmZszMzP\/\/\/yH\/C05FVFNDQVBFMi4wAwEAAAAh+QQFCgAPACwAAAAADwAPAAAEQvDJaZaZOIcV8iQK8VRX4iTYoAwZ4iCYoAjZ4RxejhVNoT+mRGP4cyF4Pp0N98sBGIBMEMOotl6YZ3S61Bmbkm4mAgAh+QQFCgAPACwAAAAADQANAAAENPDJSRSZeA418itN8QiK8BiLITVsFiyBBIoYqnoewAD4xPw9iY4XLGYSjkQR4UAUD45DLwIAIfkEBQoADwAsAAAAAA8ACQAABC\/wyVlamTi3nSdgwFNdhEJgTJoNyoB9ISYoQmdjiZPcj7EYCAeCF1gEDo4Dz2eIAAAh+QQFCgAPACwCAAAADQANAAAEM\/DJBxiYeLKdX3IJZT1FU0iIg2RNKx3OkZVnZ98ToRD4MyiDnkAh6BkNC0MvsAj0kMpHBAAh+QQFCgAPACwGAAAACQAPAAAEMDC59KpFDll73HkAA2wVY5KgiK5b0RRoI6MuzG6EQqCDMlSGheEhUAgqgUUAFRySIgAh+QQFCgAPACwCAAIADQANAAAEM\/DJKZNLND\/kkKaHc3xk+QAMYDKsiaqmZCxGVjSFFCxB1vwy2oOgIDxuucxAMTAJFAJNBAAh+QQFCgAPACwAAAYADwAJAAAEMNAs86q1yaWwwv2Ig0jUZx3OYa4XoRAfwADXoAwfo1+CIjyFRuEho60aSNYlOPxEAAAh+QQFCgAPACwAAAIADQANAAAENPA9s4y8+IUVcqaWJ4qEQozSoAzoIyhCK2NFU2SJk0hNnyEOhKR2AzAAj4Pj4GE4W0bkJQIAOw=="};
var PT_CV_PAGINATION = {"first":"\u00ab","prev":"\u2039","next":"\u203a","last":"\u00bb","goto_first":"Go to first page","goto_prev":"Go to previous page","goto_next":"Go to next page","goto_last":"Go to last page","current_page":"Current page is","goto_page":"Go to page"};
</script>
<script id="pt-cv-content-views-script-js" src="https://www.traveltourbohol.com/wp-content/plugins/content-views-query-and-display-post-page/public/assets/js/cv.js?ver=2.3.4"></script>
<script id="jquery-fancybox-js" src="https://www.traveltourbohol.com/wp-content/plugins/easy-fancybox/js/jquery.fancybox.min.js?ver=1.3.24"></script>
<script id="jquery-fancybox-js-after">
var fb_timeout, fb_opts={'overlayShow':true,'hideOnOverlayClick':true,'showCloseButton':true,'margin':20,'centerOnScroll':false,'enableEscapeButton':true,'autoScale':true };
if(typeof easy_fancybox_handler==='undefined'){
var easy_fancybox_handler=function(){
jQuery('.nofancybox,a.wp-block-file__button,a.pin-it-button,a[href*="pinterest.com/pin/create"],a[href*="facebook.com/share"],a[href*="twitter.com/share"]').addClass('nolightbox');
/* IMG */
var fb_IMG_select='a[href*=".jpg"]:not(.nolightbox,li.nolightbox>a),area[href*=".jpg"]:not(.nolightbox),a[href*=".jpeg"]:not(.nolightbox,li.nolightbox>a),area[href*=".jpeg"]:not(.nolightbox),a[href*=".png"]:not(.nolightbox,li.nolightbox>a),area[href*=".png"]:not(.nolightbox),a[href*=".webp"]:not(.nolightbox,li.nolightbox>a),area[href*=".webp"]:not(.nolightbox)';
jQuery(fb_IMG_select).addClass('fancybox image');
var fb_IMG_sections=jQuery('.gallery,.wp-block-gallery,.tiled-gallery,.wp-block-jetpack-tiled-gallery');
fb_IMG_sections.each(function(){jQuery(this).find(fb_IMG_select).attr('rel','gallery-'+fb_IMG_sections.index(this));});
jQuery('a.fancybox,area.fancybox,li.fancybox a').each(function(){jQuery(this).fancybox(jQuery.extend({},fb_opts,{'transitionIn':'elastic','easingIn':'easeOutBack','transitionOut':'elastic','easingOut':'easeInBack','opacity':false,'hideOnContentClick':false,'titleShow':true,'titlePosition':'over','titleFromAlt':true,'showNavArrows':true,'enableKeyboardNav':true,'cyclic':false}))});};
jQuery('a.fancybox-close').on('click',function(e){e.preventDefault();jQuery.fancybox.close()});
};
var easy_fancybox_auto=function(){setTimeout(function(){jQuery('#fancybox-auto').trigger('click')},1000);};
jQuery(easy_fancybox_handler);jQuery(document).on('post-load',easy_fancybox_handler);
jQuery(easy_fancybox_auto);
</script>
<script id="jquery-easing-js" src="https://www.traveltourbohol.com/wp-content/plugins/easy-fancybox/js/jquery.easing.min.js?ver=1.4.1"></script>
<script id="jquery-mousewheel-js" src="https://www.traveltourbohol.com/wp-content/plugins/easy-fancybox/js/jquery.mousewheel.min.js?ver=3.1.13"></script>
<script id="wp-embed-js" src="https://www.traveltourbohol.com/wp-includes/js/wp-embed.min.js?ver=5.5.3"></script>
<script>
			/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
			</script>
</body>
</html>
