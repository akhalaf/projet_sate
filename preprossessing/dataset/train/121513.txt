<html><body><p>ï»¿<!DOCTYPE html>

</p>
<meta charset="utf-8"/>
<title>æå ±ãµã¤ã Andraste</title>
<meta content="æ§ããªæå ±ããå±ããããµã¤ãã§ã" name="description"/>
<meta content="ã²ã¼ã , æ»ç¥" name="keywords"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@andraste_info" name="twitter:site"/>
<meta content="https://andraste.info/" property="og:url"/>
<meta content="æå ±ãµã¤ã Andraste" property="og:title"/>
<meta content="æ§ããªæå ±ããå±ããããµã¤ãã§ã" property="og:description"/>
<link as="style" href="/styles.css" rel="preload"/>
<link as="style" href="/styles-phone.css" rel="preload"/>
<link href="/styles-phone.css" media="only screen and (max-device-width:959px)" rel="stylesheet" type="text/css"/>
<link href="/styles.css" media="screen and (min-device-width:960px)" rel="stylesheet" type="text/css"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link as="script" href="/components/scripts/jquery-3.5.1.min.js" rel="preload"/>
<link href="/components/favicon/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/components/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/components/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/components/favicon/site.webmanifest" rel="manifest"/>
<link color="#5bbad5" href="/components/favicon/safari-pinned-tab.svg" rel="mask-icon"/>
<meta content="#da532c" name="msapplication-TileColor"/>
<meta content="#ffffff" name="theme-color"/>
<div id="wrap">
<header>
<div id="navigation-drawer">
<input class="display-none" id="navigation-input" type="checkbox"/>
<label for="navigation-input" id="navigation-open"><span></span></label>
<label class="display-none" for="navigation-input" id="navigation-close"></label>
<div id="navigation-menu">
<div id="navigation-mobile"><script src="/components/scripts/navigation-mobile.js"></script></div>
<div id="menu-mobile"><script src="menu-mobile.js"></script></div>
</div>
</div>
<h1 class="page-title">Andraste</h1>
<nav><div id="navigation"></div></nav>
</header>
<main>
<div id="main-1">
<h2>About Us</h2>
<p class="main-1">Andraste is an unofficial fansite dedicated to sharing helpful information about various subjects. We have been enjoying it since 2013, and now have over 10,000 pages on this website, thanks to your support. </p>
</div>
<div id="main-2">
<h2>ãç¥ãã</h2>
<p class="main-2">2020å¹´10æãã¬ã¤ã¢ã¦ãå¤æ´ãå®æ½ãããã¾ãããè¡¨ç¤ºãä¹±ããå ´åã¯ããã¼ã¸ã¨CSSã®ã­ã£ãã·ã¥æ´æ°(Ctrl+F5)ããè©¦ããã ããããçè§£ããååã®ã»ã©ä½åãããããé¡ãè´ãã¾ããã¬ã¤ã¢ã¦ãå¤æ´ä»¥åã«ãå©ç¨ã«ãªã£ã¦ããæå ±ãè¦ã¤ãããªãå ´åã¯ããã¡ãã®<a href="/sitemap.html">ãµã¤ãããã</a>ãããæ¢ãä¸ããã</p>
<br/>
<h2>ãé¡ã</h2>
<p class="main-2">å½ãµã¤ãã¯éå¬å¼ãªãã¡ã³ãµã¤ãã§ããæ­£ç¢ºãªæå ±ãå±æããããã«å¿æãã¦ããã¾ãããæ­£ç¢ºããä¿è¨¼ã§ãããã®ã§ã¯ããã¾ãããå¯è½ãªéãå¬å¼æå ±ã¨ç§åãã¦ãã ãããã¾ããå½ãµã¤ãããå©ç¨ã«ãªãåã«å©ç¨è¦ç´ã«ãåæãããã ãã¾ãããããããããé¡ãè´ãã¾ãã</p>
</div></main>
<footer><div id="footer-content"></div></footer>
<p id="page-top"><a href="#">â²<br/>â²</a></p>
<script defer="" src="/components/scripts/navigation.js"></script>
<script defer="" src="/components/scripts/footer.js"></script>
<script type="text/javascript">
(function(window, document) {
  function main() {

    var ad = document.createElement('script');
    ad.type = 'text/javascript';
    ad.async = true;

    ad.src = 'https://www.googletagmanager.com/gtag/js?id=UA-38618332-1';
    var sc = document.getElementsByTagName('script')[0];
    sc.parentNode.insertBefore(ad, sc);
  }
 

  var lazyLoad = false;
  function onLazyLoad() {
    if (lazyLoad === false) {

      lazyLoad = true;
      window.removeEventListener('scroll', onLazyLoad);
      window.removeEventListener('mousemove', onLazyLoad);
      window.removeEventListener('mousedown', onLazyLoad);
      window.removeEventListener('touchstart', onLazyLoad);
      window.removeEventListener('keydown', onLazyLoad);
      main();
    }
  }
  window.addEventListener('scroll', onLazyLoad);
  window.addEventListener('mousemove', onLazyLoad);
  window.addEventListener('mousedown', onLazyLoad);
  window.addEventListener('touchstart', onLazyLoad);
  window.addEventListener('keydown', onLazyLoad);
  window.addEventListener('load', function() {

    if (window.pageYOffset) {
      onLazyLoad();
    }

  window.setTimeout(onLazyLoad,3000)
  });
})(window, document);
</script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-38618332-1');
</script>
<script type="text/javascript">
(function(window, document) {
  function main() {

    var ad = document.createElement('script');
    ad.type = 'text/javascript';
    ad.async = true;

    ad.src = '/components/scripts/jquery-3.5.1.min.js';
    var sc = document.getElementsByTagName('script')[0];
    sc.parentNode.insertBefore(ad, sc);
  }
 

  var lazyLoad = false;
  function onLazyLoad() {
    if (lazyLoad === false) {

      lazyLoad = true;
      window.removeEventListener('scroll', onLazyLoad);
      window.removeEventListener('mousemove', onLazyLoad);
      window.removeEventListener('mousedown', onLazyLoad);
      window.removeEventListener('touchstart', onLazyLoad);
      window.removeEventListener('keydown', onLazyLoad);
      main();
    }
  }
  window.addEventListener('scroll', onLazyLoad);
  window.addEventListener('mousemove', onLazyLoad);
  window.addEventListener('mousedown', onLazyLoad);
  window.addEventListener('touchstart', onLazyLoad);
  window.addEventListener('keydown', onLazyLoad);
  window.addEventListener('load', function() {

    if (window.pageYOffset) {
      onLazyLoad();
    }

  window.setTimeout(onLazyLoad,3000)
  });
})(window, document);
</script>
<script type="text/javascript">
(function(window, document) {
  function main() {

    var ad = document.createElement('script');
    ad.type = 'text/javascript';
    ad.async = true;

    ad.src = '/components/scripts/page-top-button.js';
    var sc = document.getElementsByTagName('script')[0];
    sc.parentNode.insertBefore(ad, sc);
  }
 

  var lazyLoad = false;
  function onLazyLoad() {
    if (lazyLoad === false) {

      lazyLoad = true;
      window.removeEventListener('scroll', onLazyLoad);
      window.removeEventListener('mousemove', onLazyLoad);
      window.removeEventListener('mousedown', onLazyLoad);
      window.removeEventListener('touchstart', onLazyLoad);
      window.removeEventListener('keydown', onLazyLoad);
      main();
    }
  }
  window.addEventListener('scroll', onLazyLoad);
  window.addEventListener('mousemove', onLazyLoad);
  window.addEventListener('mousedown', onLazyLoad);
  window.addEventListener('touchstart', onLazyLoad);
  window.addEventListener('keydown', onLazyLoad);
  window.addEventListener('load', function() {

    if (window.pageYOffset) {
      onLazyLoad();
    }

  window.setTimeout(onLazyLoad,3000)
  });
})(window, document);
</script>
</div>
</body></html>