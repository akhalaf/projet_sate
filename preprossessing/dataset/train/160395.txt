<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "67879",
      cRay: "610ee0972bc1d18f",
      cHash: "80777a764836397",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXNrZ2FtYmxlcnMuY29tLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "i3Kj1CRYq1wOCEjCQAzxjyPnWG8rF4rkgyire41iFkskU4oieU/qwEMw/Pq+Pwas+batxpNiv46CVwZwDWsWcqSr7vG3GTPL+rNFTGgwGOybFMSTLvTv9rVGLy9ldtHPZgjaeuivAUSIj/tRjhK6zYCMey3EUcK57zwNUJCVXREgmUHvIHLlNIkltdmkKnvO4Jr7f6VsEjzbJNKneoy3tPgxy9rcBiue5gNowHOS70uDLZJ20F9bsao6wcrRkbMzEBYP+wRoW2e29FLxZZw4pwzZnpgotK4CTGMX2P8DT4wPHdUeUrXYtoSlsHvvppz+xZdYJu1hKS96Z4+JVRXgTgzP+ubwFVPoNq2LPg1T5cwob+8ardlQGRyqSVYGh/X7ZiKCsuadjvbIgtKCfcqRPeOUgVLlp2SebQ6lwC+abARPysx3HqzPCfjNHquUiOvMRBXBhJeqxmrWe53gODF3vcFNL77GCHzCV6XvV+tcct/z7RZeoG0vEWDEuU/5JIrTjB7X90fLLP3QOHs1bs2F6Vgcp+X4WASgFtCxd1ZLaSkdbpORjvLXi70I865Vp7Eo98VAMT1Ew8HSlI1zqu493Ow+5fmD4TI3l4J47UwYgm9LJKoRCysA+FylQWXPQ5hkZUAv/oF0sMcPU1rF/+tdY84zVO1zRFxTGDlG+L2uJvaX2Ijz/j8DEmAw+vvOa/VNLAHeIq2hxs5d1xfWMHu9WTD6dAxGQj8ff9GPpeI5Ke8eD/ERDJRiEZ17XxV82fNv8p6IgQJEoW1PPVEqMlx1Bg==",
        t: "MTYxMDUzODM5Mi4yMDkwMDA=",
        m: "46es7hU7SBWlwP/6my/B48CUL4o7rcPIMd0WLXurLvs=",
        i1: "z20j+9Mg3+xYVuKqyeDWZg==",
        i2: "bD5xjfRIQipRwVp3UbLoTw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "ukzICfqo72oj03x/Hg76/KIQMMOSOmtWRhv9Aoqin6E=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.askgamblers.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=e74710c208174acf559fa88a590129f3c16f8286-1610538392-0-AX8kKn-cJNNh41Bu7EA50TmwDi0KPHjurLl8sE29iv0k5pCabmdkRmLt87Cy_31N3BSYwQ-qAWOeKCo_Vig6SndQhrqR21Hji9LA3iu5iJAlXXJ607509ilF4l-93Lb2qTz6wR1n7A366u3fazk91LiFJ2M0t6glPWiLZUki0Kpi6gDY4bCJlor-asOGzX-Y_FynXR5s-tj0QC6GB-MmPmX0bLbdOE0H4CLY1XNPl6Fl0rHAULaeRh1NlOt1noz42PmBLbiPIDRdkzhcnXgeK6ohGdVfGl_syXPhwF-bffO9qGK6gcTsbuMah__ROlz_UVfuFsH2XXa2f_d3sJA0rh6rCF7F11qJFRSBwoGmVINVb8Xt36hGodbAz8lzfWMd2kRjaea9YrTaoezVcc-jwxwM51cECrUHvo24jMFIV-4_mnUuI3AAmE9hHgnshGmfulNicyCTS0zOS1Tz28e6YUFWOZSMTJKLZuXZajkbCyAZteARHrxXe-6lqUqsvedOVxldkgvfp8ov1YVW2dHO6Adg8cg9U_NBCPsZqAbRt1c7fsp1y6KSVbzNKjFJbErM5ok444dTnb2_LYNkCq5ZtnE" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="a423ba867e449c83ed42753bde5f1e2795ec8530-1610538392-0-AYnw4ngZZuLZ04EqqYlQ1lruY2UpD4exbvOcq6QorkrXmKd2E2HbyKV5xLUiOrgrBHYAvb58ue04WeA8j5xOHkx3OPCbWMk6vq3rXTIV/PrtWij5E2TrYjOG4925xmJnWsQbQReOq6c9qAaqdG5tHdyMco54aRjik3DJaei7e9oHsyQm5CqX3W64LcVQxNi1tnp9kEPZW7GoOJ91XqgrlMIgnAktMPuBh4el4bHC5lhHczT2V5yDsdgXfR0p3rQfuwnNqJbJGa4/NacVC5s4iICimRjHXmvyFVepgByAco2IXoG0VMDaDOCDILpD+LBrvZuCfm9FSlzZuvGosUZXylgSlvv+ANMFks5hufoTaNHp6h2iI8AwqabKMozwmh82FBPH7+xmbYOKxuf6rMBjUlgJZ7vbRr/d2uDYOlWvdGJbmE7ICHDg5gdvgJCP5Zr+caA7n8dY2TEfzriHRVpHGL5IZDHKkTESaAj5SiSuGd/D8CiV7iQIHJph7yNsjb9y5LRUOYT4b0xf5qxfYxXw4WnzIieXfoUm0rBW46+RdWDD6825XHivAWSuH+8QqY8C7EULS8mgEtqoFRTRdQHiz8qbAvsRCO5ljYnj/hr73ZHxid+I5mhNAvzfjfafvOoriPCb6BYBekC1biuN2KFLlrkqXW6T6uU828UrWQLv01O4taiYvkq1SY569pyl++n2maGbgPVDjfjtTyvunOo0tR1hWZfIJkmCwtutBsceQyK2WhbWW4RJO6tQmcBJ0yoJN2ZwUJ6SY1XfIR3DrMD0INYXrPX+UH28gvt+v3x1NJgwNAC3/PVGYBzIwUAKT+GttDETERRSxoLtZBVWNrrzNe086myRnpvfaCnvahKeUDkq4TfgqjdXGe72Pq1PyR/nHdHSZypafl28bdG1kfcaZy0MrapiML9RsxwLL7FcRoHr7DxrHdeolIzkyi0sk6PMh7F/Dmnh84pDALgI5w6BpnUH3DJC2/8TeHwkz6f9ZQ8Xt3T4+1pd3MKLwtZjghbbx6XGVJXL98fFaqPOmDP8vxJgC+PNvLJ6A1kqqiM26Sc005WJw5wyIbWHteQByoDt2ugeT/fd3+/f83HNQlSdIP5Lno20uP1ilsPVM9pAWjJaFodFsDs2ApCwZRuAsgOiX8AZF5DeYCa7lDWSE75LPYYNlwRaeHVLc+FW6ltujHpEUyurKjGJnfG0x9dNyi55iHZhIamSDPAe9OCqPu6lknZrjL+nUVF2VJzgIl1cUZ/GtQSmRCD5tVE+FpBwIlfILjP4Sz3VeMm44tnPrrfb4KBXhUblMNJIeVdvJIm3oR5U0C3Q46Higmok44sQDt/90A=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="add9d352b0cb4ce6007d6bae906c3aa4"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610ee0972bc1d18f')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610ee0972bc1d18f</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
