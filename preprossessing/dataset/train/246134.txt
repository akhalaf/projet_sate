<!DOCTYPE html>
<html dir="ltr" lang="tr">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>Bodrum Yapı</title>
<base href="http://bodrumyapi.com.tr/"/>
<meta content="bodrum yapı pvc aluminyum ve cam balkon" name="description"/>
<meta content="bodrum yapı , bodrum pvc , bodrum inşaat , bodrum yapı firmaları , bodrum cam balkon" name="keywords"/>
<link href="http://bodrumyapi.com.tr/image/data/logo_ust.png" rel="icon"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600italic,600,700,700italic,800,800italic&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,900,700italic,700,500italic,500,400italic,300italic,300,100italic,100,900italic&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet" type="text/css"/>
<link href="tema/premium/css/bootstrap.css" rel="stylesheet"/>
<link href="tema/premium/css/style.css" rel="stylesheet"/>
<link href="tema/premium/css/responsive.css" rel="stylesheet"/>
<link href="tema/premium/css/layout-wide.css" rel="stylesheet"/>
<link href="tema/premium/css/skin-blue.css" rel="stylesheet"/>
<!-- IE -->
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="tema/premium/js/html5shiv.js"></script>
      <script src="tema/premium/js/respond.min.js"></script>	   
    <![endif]-->
<!--[if lte IE 8]>
	<link href="tema/premium/css/ie8.css" rel="stylesheet">
	 <![endif]-->
</head>
<body class="off">
<div class="wrapbox">
<section class="toparea">
<div class="container">
<div class="row">
<div class="col-md-7 top-text pull-left animated fadeInLeft">
<i class="icon-phone"></i>   0252 358 55 03<span class="separator"></span>
<i class="icon-envelope"></i>
<a href="mailto:bodrumyapi@yahoo.com">bodrumyapi@yahoo.com</a>
</div>
<div class="col-md-5 text-right animated fadeInRight">
</div>
</div>
</div>
</section>
<nav class="navbar navbar-fixed-top wowmenu" role="navigation">
<div class="container">
<div class="navbar-header">
<a class="navbar-brand logo-nav" href="http://bodrumyapi.com.tr/"><img alt="Erk Reklam Ajans Bodrum" src="http://bodrumyapi.com.tr/image/data/logo.png"/></a>
</div>
<ul class="nav navbar-nav pull-right" id="nav">
<li><a href="index.php">Anasayfa</a></li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Kurumsal<i class="icon-angle-down"></i></a>
<ul class="dropdown-menu" style="display: none;">
<li><a href="http://bodrumyapi.com.tr/hakkimizda" title="Hakkımızda">Hakkımızda</a></li>
</ul>
</li>
<li><a href="http://bodrumyapi.com.tr/fotograf-galerisi">Galeri</a></li>
<li><a href="http://bodrumyapi.com.tr/iletisim">İletişim</a></li>
</ul>
</div>
</nav>
<section class="carousel carousel-fade slide home-slider" data-interval="4500" data-pause="false" data-ride="carousel" id="c-slide">
<ol class="carousel-indicators">
<li data-slide-to="0" data-target="#c-slide"></li>
</ol>
<div class="carousel-inner">
<div class="item">
<img class="img-responsive" src="http://bodrumyapi.com.tr/image/data/slider/slider-01-10.jpg"/>
</div>
</div>
<!-- /.carousel-inner -->
<a class="left carousel-control animated fadeInLeft" data-slide="prev" href="#c-slide"><i class="icon-angle-left"></i></a>
<a class="right carousel-control animated fadeInRight" data-slide="next" href="#c-slide"><i class="icon-angle-right"></i></a>
</section>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript">

$( ".carousel-inner .item" ).first().addClass( "active" );
</script><section class="service-box topspace30">
<div class="container">
<div class="row">
</div>
</div>
</section> <section class="grayarea recent-projects-home topspace30 animated fadeInUpNow notransition" style="border-top:none">
<div class="container">
<div class="row">
<h1 class="small text-center topspace0" style="text-transform:uppercase">Fotoğraf Galerisi</h1>
<div class="text-center smalltitle">
</div>
<div class="col-md-12">
<div class="list_carousel text-center">
<div class="carousel_nav">
<a class="prev" href="#" id="har_prev"><span>prev</span></a>
<a class="next" href="#" id="har_next"><span>next</span></a>
</div>
<div class="clearfix">
</div>
<ul id="carousel-galeri">
<!--featured-projects 1-->
<li>
<div class="boxcontainer">
<img alt="" src="http://bodrumyapi.com.tr/image/cache/data/galeri/kapak/motorlupergole_kapak-270x135.jpg"/>
<div class="roll" style="opacity: 0;">
<div class="wrapcaption">
<a href="http://bodrumyapi.com.tr/fotograf-galerisi?album_id=47"><i class="icon-link captionicons"></i></a>
<a data-gal="prettyPhoto[gallery1]" href="http://bodrumyapi.com.tr/image/cache/data/galeri/kapak/motorlupergole_kapak-270x135.jpg" title="Motorlu Pergole ve Tente Sistemleri"><i class="icon-zoom-in captionicons"></i></a>
</div>
</div>
<h1><a href="&lt;http://bodrumyapi.com.tr/fotograf-galerisi?album_id=47">Motorlu Pergole ve Tente Sistemleri</a></h1>
</div>
</li>
<li>
<div class="boxcontainer">
<img alt="" src="http://bodrumyapi.com.tr/image/cache/data/galeri/kapak/TENTE_KAPAK-270x135.jpg"/>
<div class="roll" style="opacity: 0;">
<div class="wrapcaption">
<a href="http://bodrumyapi.com.tr/fotograf-galerisi?album_id=45"><i class="icon-link captionicons"></i></a>
<a data-gal="prettyPhoto[gallery1]" href="http://bodrumyapi.com.tr/image/cache/data/galeri/kapak/TENTE_KAPAK-270x135.jpg" title="Tente Sistemleri"><i class="icon-zoom-in captionicons"></i></a>
</div>
</div>
<h1><a href="&lt;http://bodrumyapi.com.tr/fotograf-galerisi?album_id=45">Tente Sistemleri</a></h1>
</div>
</li>
<li>
<div class="boxcontainer">
<img alt="" src="http://bodrumyapi.com.tr/image/cache/data/galeri/kapak/PANJURKEPENK_kapak-270x135.jpg"/>
<div class="roll" style="opacity: 0;">
<div class="wrapcaption">
<a href="http://bodrumyapi.com.tr/fotograf-galerisi?album_id=44"><i class="icon-link captionicons"></i></a>
<a data-gal="prettyPhoto[gallery1]" href="http://bodrumyapi.com.tr/image/cache/data/galeri/kapak/PANJURKEPENK_kapak-270x135.jpg" title="Motorlu ve Mekanik Panjur Sistemleri"><i class="icon-zoom-in captionicons"></i></a>
</div>
</div>
<h1><a href="&lt;http://bodrumyapi.com.tr/fotograf-galerisi?album_id=44">Motorlu ve Mekanik Panjur Sistemleri</a></h1>
</div>
</li>
<li>
<div class="boxcontainer">
<img alt="" src="http://bodrumyapi.com.tr/image/cache/data/galeri/kapak/korkuluk_kapak-270x135.jpg"/>
<div class="roll" style="opacity: 0;">
<div class="wrapcaption">
<a href="http://bodrumyapi.com.tr/fotograf-galerisi?album_id=43"><i class="icon-link captionicons"></i></a>
<a data-gal="prettyPhoto[gallery1]" href="http://bodrumyapi.com.tr/image/cache/data/galeri/kapak/korkuluk_kapak-270x135.jpg" title="Aluminyum Korkuluk Sistemleri"><i class="icon-zoom-in captionicons"></i></a>
</div>
</div>
<h1><a href="&lt;http://bodrumyapi.com.tr/fotograf-galerisi?album_id=43">Aluminyum Korkuluk Sistemleri</a></h1>
</div>
</li>
<li>
<div class="boxcontainer">
<img alt="" src="http://bodrumyapi.com.tr/image/cache/data/galeri/kapak/firatpenpvc_kapak-270x135.jpg"/>
<div class="roll" style="opacity: 0;">
<div class="wrapcaption">
<a href="http://bodrumyapi.com.tr/fotograf-galerisi?album_id=42"><i class="icon-link captionicons"></i></a>
<a data-gal="prettyPhoto[gallery1]" href="http://bodrumyapi.com.tr/image/cache/data/galeri/kapak/firatpenpvc_kapak-270x135.jpg" title="Fıratpen PVC Kapı ve Pencere Sistemleri"><i class="icon-zoom-in captionicons"></i></a>
</div>
</div>
<h1><a href="&lt;http://bodrumyapi.com.tr/fotograf-galerisi?album_id=42">Fıratpen PVC Kapı ve Pencere Sistemleri</a></h1>
</div>
</li>
<li>
<div class="boxcontainer">
<img alt="" src="http://bodrumyapi.com.tr/image/cache/data/galeri/kapak/aluminyon kapak-270x135.jpg"/>
<div class="roll" style="opacity: 0;">
<div class="wrapcaption">
<a href="http://bodrumyapi.com.tr/fotograf-galerisi?album_id=41"><i class="icon-link captionicons"></i></a>
<a data-gal="prettyPhoto[gallery1]" href="http://bodrumyapi.com.tr/image/cache/data/galeri/kapak/aluminyon kapak-270x135.jpg" title="Aluminyum Kapı ve Pencere Sistemleri"><i class="icon-zoom-in captionicons"></i></a>
</div>
</div>
<h1><a href="&lt;http://bodrumyapi.com.tr/fotograf-galerisi?album_id=41">Aluminyum Kapı ve Pencere Sistemleri</a></h1>
</div>
</li>
<li>
<div class="boxcontainer">
<img alt="" src="http://bodrumyapi.com.tr/image/cache/data/galeri/kapak/cam_balkon_kapak-270x135.jpg"/>
<div class="roll" style="opacity: 0;">
<div class="wrapcaption">
<a href="http://bodrumyapi.com.tr/fotograf-galerisi?album_id=40"><i class="icon-link captionicons"></i></a>
<a data-gal="prettyPhoto[gallery1]" href="http://bodrumyapi.com.tr/image/cache/data/galeri/kapak/cam_balkon_kapak-270x135.jpg" title="Cam Balkon Sistemleri"><i class="icon-zoom-in captionicons"></i></a>
</div>
</div>
<h1><a href="&lt;http://bodrumyapi.com.tr/fotograf-galerisi?album_id=40">Cam Balkon Sistemleri</a></h1>
</div>
</li>
</ul>
</div>
</div>
</div>
</div>
</section>
<script>
	/* ---------------------------------------------------------------------- */
	/*	Carousel
	/* ---------------------------------------------------------------------- */
	$(window).load(function(){			
		$('#carousel-galeri').carouFredSel({
		responsive: true,
		items       : {
        width       : 200,
        height      : 295,
        visible     : {
            min         : 1,
            max         : 4
        }
    },
		width: '200px',
		height: '295px',
		auto: true,
		circular	: true,
		infinite	: false,
		prev : {
			button		: "#har_prev",
			key			: "left",
				},
		next : {
			button		: "#har_next",
			key			: "right",
					},
		swipe: {
			onMouse: true,
			onTouch: true
			},
		scroll: {
        easing: "",
        duration: 1200
    }
	});
		});
</script>
<section>
<div class="footer">
<div class="container animated fadeInUpNow notransition">
<div class="row">
<div class="col-md-3">
<h1 class="footerbrand">Bodrum Yapı</h1>
<p>
						Bodrum Yapı, Fıratpen PVC Doğrama, Aluminyum Doğrama, Balkon Camlama Sistemleri, Motorlu ve Mekanik Panjur, Motorlu Pergole ve Aluminyum Korkuluk Alanlarında Hizmet Vermektedir.					</p>
</div>
<div class="col-md-3">
<h1 class="title"><span class="colortext">N</span>eredeyiz</h1>
<div class="footermap">
<p>
<strong>Adres: </strong> Ortakent Yahşi Mahallesi Cumhurriyet Caddesi No: 171/A  Bodrum						</p>
<p>
<strong>Telefon: </strong>   0252 358 55 03						</p>
<p>
<strong>Fax: </strong> 0252 358 55 04						</p>
<p>
<strong>E-posta: </strong> bodrumyapi@yahoo.com</p>
</div>
</div> <div class="col-md-3"> <h1 class="title"> </h1>
</div>
</div>
</div>
<div class="copyright">
<div class="container">
<div class="row">
<div class="col-md-4">
<p class="pull-left">
						Bodrum Yapı © 2021					</p>
</div>
<div class="col-md-8">
<ul class="footermenu pull-right">
<li>Powered By <a href="http://www.oykuozen.com" title="Bodrum Web Tasarim ve Internet Hizmetleri">Oyku Ozen</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</section>
<script src="tema/premium/js/jquery.js"></script>
<script src="tema/premium/js/bootstrap.js"></script>
<script src="tema/premium/js/plugins.js"></script>
<script src="tema/premium/js/common.js"></script>
<script>
	/* ---------------------------------------------------------------------- */
	/*	Carousel
	/* ---------------------------------------------------------------------- */
	$(window).load(function(){			
		$('#carousel-projects').carouFredSel({
		responsive: true,
		items       : {
        width       : 200,
        height      : 295,
        visible     : {
            min         : 1,
            max         : 4
        }
    },
		width: '200px',
		height: '295px',
		auto: true,
		circular	: true,
		infinite	: false,
		prev : {
			button		: "#car_prev",
			key			: "left",
				},
		next : {
			button		: "#car_next",
			key			: "right",
					},
		swipe: {
			onMouse: true,
			onTouch: true
			},
		scroll: {
        easing: "",
        duration: 1200
    }
	});
		});
</script>
<script>
	//CALL TESTIMONIAL ROTATOR
	$( function() {
		/*
		- how to call the plugin:
		$( selector ).cbpQTRotator( [options] );
		- options:
		{
			// default transition speed (ms)
			speed : 700,
			// default transition easing
			easing : 'ease',
			// rotator interval (ms)
			interval : 8000
		}
		- destroy:
		$( selector ).cbpQTRotator( 'destroy' );
		*/
		$( '#cbp-qtrotator' ).cbpQTRotator();
	} );
</script>
<script>
	//CALL PRETTY PHOTO
	$(document).ready(function(){
		$("a[data-gal^='prettyPhoto']").prettyPhoto({social_tools:'', animation_speed: 'normal' , theme: 'dark_rounded'});
	});
</script>
<script>
	//MASONRY
	$(document).ready(function(){
	var $container = $('#content');
	  $container.imagesLoaded( function(){
		$container.isotope({
		filter: '*',	
		animationOptions: {
		 duration: 750,
		 easing: 'linear',
		 queue: false,	 
	   }
	});
	});
	$('#filter a').click(function (event) {
		$('a.selected').removeClass('selected');
		var $this = $(this);
		$this.addClass('selected');
		var selector = $this.attr('data-filter');
		$container.isotope({
			 filter: selector
		});
		return false;
	});
	});
</script>
<script>
//ROLL ON HOVER
	$(function() {
	$(".roll").css("opacity","0");
	$(".roll").hover(function () {
	$(this).stop().animate({
	opacity: .8
	}, "slow");
	},
	function () {
	$(this).stop().animate({
	opacity: 0
	}, "slow");
	});
	});
</script>
<script>
$('#myTab a').click(function (e) {
	e.preventDefault()
	$(this).tab('show')
	})
</script>
</div>
</body></html>