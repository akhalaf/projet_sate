<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Drink Recipes, Drinking Games and More at Bar None Drinks</title>
<meta content="With thousands of drink recipes, you'll find the drink you're looking for here.You'll also find a large list of drinking games, user forums, and bartending tips." name="Description"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- Bootstrap CSS-->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Google fonts - Vidaloka and Poppins-->
<link href="https://fonts.googleapis.com/css?family=Vidaloka|Poppins:300,400,500" rel="stylesheet"/>
<!-- Bootstrap Select-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css" rel="stylesheet"/>
<!-- Fancy Box-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.1/jquery.fancybox.min.css" rel="stylesheet"/>
<!-- Font Awesome CSS-->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<!-- Custom Font Icons CSS-->
<!-- <link rel="stylesheet" href="/theme/css/custom-fonticons.css"> -->
<!-- BN stylesheet - for your changes-->
<link href="/theme/css/barnone.css" rel="stylesheet"/>
<!-- Favicon-->
<link href="https://www.barnonedrinks.com/favicon.ico" rel="icon"/>
<!-- Google Ads = DFP -->
<script async="async" src="https://www.googletagservices.com/tag/js/gpt.js"></script>
<script>
      var googletag = googletag || {};
      googletag.cmd = googletag.cmd || [];
    </script>
<script>
      googletag.cmd.push(function() {
        googletag.defineSlot('/1017476/SSLLeaderboard', [728, 90], 'div-gpt-ad-1514679710045-0').addService(googletag.pubads());
        googletag.defineSlot('/1017476/SSLSkyscraper', [160, 600], 'div-gpt-ad-1514679710045-1').addService(googletag.pubads());
        googletag.defineSlot('/1017476/barnone_product_feature_160x223', [160, 223], 'div-gpt-ad-1514679710045-2').addService(googletag.pubads());
        googletag.defineSlot('/1017476/SSLCube', [336, 280], 'div-gpt-ad-1514679710045-3').addService(googletag.pubads());
        googletag.defineSlot('/1017476/SSLLeaderboardFooter', [728, 90], 'div-gpt-ad-1514679710045-4').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
      });
    </script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-147538-1"></script>
<script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-147538-1');
    </script>
</head>
<body>
<!-- Main Navbar-->
<header class="header">
<nav class="navbar navbar-expand-lg">
<div class="container">
<!-- Navbar Brand --><a class="navbar-brand" href="/"><img alt="Bar None Drinks" src="/theme/img/logo.png"/></a>
<!-- Toggle Button-->
<button aria-controls="navbarcollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler d-print-none" data-target="#navbarcollapse" data-toggle="collapse" type="button"><span>Menu</span><i class="fa fa-bars"></i></button>
<!-- Navbar Menu -->
<div class="collapse navbar-collapse d-print-none" id="navbarcollapse">
<ul class="navbar-nav ml-auto">
<li class="nav-item dropdown"><a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="/drinks/" id="navbarDrinks">Drinks</a>
<div aria-labelledby="navbarDrinks" class="dropdown-menu">
<a class="dropdown-item" href="/drinks/by_category/">By Category</a>
<a class="dropdown-item" href="/drinks/by_ingredient/">By Ingredient</a>
<a class="dropdown-item" href="/drinks/top_ingredients/">Top Ingredients</a>
<a class="dropdown-item" href="/drinks/random/">Random</a>
<a class="dropdown-item" href="/drinks/recent/">Newly Added</a>
</div>
</li>
<li class="nav-item dropdown"><a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="/games/" id="navbarGames">Games</a>
<div aria-labelledby="navbarGames" class="dropdown-menu">
<a class="dropdown-item" href="/games/by_category/">By Category</a>
<a class="dropdown-item" href="/games/random/">Random</a>
</div>
</li>
<li class="nav-item dropdown"><a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="/tips/" id="navbarTips">Tips</a>
<div aria-labelledby="navbarTips" class="dropdown-menu">
<a class="dropdown-item" href="/tips/dictionary/">Ingredient Dictionary</a>
<a class="dropdown-item" href="/tips/techniques/">Techniques</a>
<a class="dropdown-item" href="/tips/equipping/">Equipping</a>
<a class="dropdown-item" href="/tips/reference/">Reference</a>
</div>
</li>
<li class="nav-item dropdown"><a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="/tips/articles/" id="navbarArticles">Articles</a>
<div aria-labelledby="navbarArticles" class="dropdown-menu">
<a class="dropdown-item" href="/tips/articles/columns.html">Columns</a>
<a class="dropdown-item" href="/tips/reviews/">Reviews and Interviews</a>
<a class="dropdown-item" href="/forums/">Forum Posts</a>
<a class="dropdown-item" href="/press/">Industry News</a>
</div>
</li>
</ul>
</div>
<script>
            (function() {
              var cx = '015727763658184789878:n5loqqstfnm';
              var gcse = document.createElement('script');
              gcse.type = 'text/javascript';
              gcse.async = true;
              gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
              var s = document.getElementsByTagName('script')[0];
              s.parentNode.insertBefore(gcse, s);
            })();
          </script>
<div class="bnsearch d-print-none">
<gcse:searchbox-only></gcse:searchbox-only>
</div>
</div>
</nav>
</header>
<div class="bnbanner-outer bngray d-print-none">
<!-- /1017476/SSLLeaderboard -->
<div class="bnbanner-inner" id="div-gpt-ad-1514679710045-0">
<script>
      googletag.cmd.push(function() { googletag.display('div-gpt-ad-1514679710045-0'); });
      </script>
</div>
</div>
<div class="container">
<div class="row">
<main class="col-lg-12">
<section class="content">
<!-- Header Section-->
<section class="site-intro">
<div class="container">
<div class="row">
<div class="col-md-7">
<header>
<h1 class="has-lines">Bar None Drinks<small>Welcome to our world</small></h1>
</header>
<p class="lead">Bar None is one of the oldest mixed drink sites on the 'net. If you're looking for a cocktail or shooter, we probably have the drink recipe here. Other features that we're proud of include drink recipe tips, a home bar section, and a wide assortment of drinking games.  Servin' the web since 1995</p>
</div>
<div class="col-md-5 bnrcube-outer">
<!-- /1017476/SSLCube -->
<div class="bnrcube-inner" id="div-gpt-ad-1514679710045-3">
<script>
              googletag.cmd.push(function() { googletag.display('div-gpt-ad-1514679710045-3'); });
              </script>
</div>
</div>
</div>
</div>
</section>
<section>
<div class="container">
<header>
<h2 class="has-lines">Site Navigation<small>Learning to get around</small></h2>
</header>
<dl>
<dt><a href="/drinks/">Drink Recipes</a></dt>
<dd>If you know the name of the cocktail, shooter, punch, hot drink or non-alcoholic recipe you need, use the index to surf via category and name.  All of our recipes are listed here.</dd>
</dl>
<dl>
<dt><a href="/games/">Games</a></dt>
<dd>Here are all of our drinking games from the latest creations to the old classics.  Browse by category, search by name or allow us to suggest a random game to try.</dd>
</dl>
<dl>
<dt><a href="/tips/">Tips</a></dt>
<dd>Everything from articles, reviews, equipping your own bar, techniques for mixing and other references as well as our <a href="/tips/dictionary/">Ingredient Dictionary</a>.  The Dictionary contains definitions or descriptions of almost all the ingredients in the recipes database.</dd>
</dl>
</div>
</section>
<!-- Features Section-->
<section class="blog bngray">
<div class="container">
<header>
<h2 class="has-lines">Features From Within<small>Recent picks by the Bar None staff</small></h2>
</header>
<div class="row">
<!-- Post-->
<div class="col-lg-6">
<div class="post">
<div class="image"><img alt="Image of Paradise Daiquiri" src="/drinks/images/14986.jpg"/></div>
<div class="info d-flex align-items-end">
<div class="content">
<a href="/drinks/p/paradise-daiquiri-14986.html"><h3>Paradise Daiquiri</h3></a>
<p>This cocktails is made with Icing Sugar, Lime Juice, St-Germain Elderflower Liqueur, Bacardi White Rum, Fernet Branca. Add the sugar and lime in a tin and stir it together.  Pour in all of the other ingredients.  Fil the shaker half full with ice cubes and the other half with crushed ice.
Hard shake.  Double strain.  Garnish with a lime peel.  Antonio Oliveira is a finalist in the Bacardi Legacy Competition.</p>
<a class="has-lines read-more" href="/drinks/p/paradise-daiquiri-14986.html">Read More</a>
</div>
</div><a href="/drinks/p/paradise-daiquiri-14986.html">
<div class="badge badge-rounded text-uppercase">Featured Drink</div></a>
</div>
</div>
<!-- Post-->
<div class="col-lg-6">
<div class="post">
<div class="image"><img alt="Basil Hayden Bourbon" src="/tips/dictionary/images/2857.jpg"/></div>
<div class="info d-flex align-items-end">
<div class="content">
<a href="/tips/dictionary/b/basil-hayden-bourbon-2857.html"><h3>Basil Hayden Bourbon</h3></a>
<p>The eponymous Basil Hayden's Bourbon dates back to 1796, when Master Distiller Basil Hayden Sr. created a recipe unlike any other. He used a traditional corn base, but mixed in small grains in his mash to capture the spicy flavor of rye and complement the sweet smoothness of corn. More than 200 years later, Basil Hayden's is a singular bourbon that bridges the flavor of rye whiskies and small batch bourbons together.</p>
<a class="has-lines read-more" href="/tips/dictionary/b/basil-hayden-bourbon-2857.html">Read More</a>
</div>
</div><a href="/tips/dictionary/b/basil-hayden-bourbon-2857.html">
<div class="badge badge-rounded text-uppercase">Featured Ingredient</div></a>
</div>
</div>
</div>
</div>
</section>
</section>
</main>
</div>
</div>
<div class="bnbanner-outer bngray d-print-none">
<!-- /1017476/SSLLeaderboard -->
<div class="bnbanner-inner" id="div-gpt-ad-1514679710045-4">
<script>
      googletag.cmd.push(function() { googletag.display('div-gpt-ad-1514679710045-4'); });
      </script>
</div>
</div>
<!-- Page Footer-->
<footer class="main-footer">
<div class="container">
<div class="row">
<div class="contact col-md-6">
<h3>Contact us</h3>
<div class="info">
<p>329 Nottingham Drive 
              </p><p>Nanaimo BC Canada V9T 4T1 
            </p></div>
</div>
<div class="site-links col-md-3">
<h3>Useful links</h3>
<div class="menus d-flex">
<ul class="list-unstyled">
<li><a href="/info/advertising/">Advertising</a></li>
<li><a href="/info/">Site Info</a></li>
<li><a href="/submit/">Email Us</a></li>
</ul>
</div>
</div>
<div class="site-links col-md-3">
<h3>Legal stuff</h3>
<div class="menus d-flex">
<ul class="list-unstyled">
<li><a href="/info/legal/copyright.html">Copyright</a></li>
<li><a href="/info/legal/disclaimer.html">Disclaimer</a></li>
<li><a href="/info/legal/privacy.html">Privacy Policy</a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="copyrights text-center">
<p>© <span class="text-primary">Bar None Drinks</span> - All Rights Reserved.</p>
</div>
</footer>
<!-- Javascript files-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"> </script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.1/jquery.fancybox.min.js"></script>
<script src="/theme/js/front.js"></script>
</body>
</html>