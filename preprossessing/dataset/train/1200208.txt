<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Quickbiz</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<link href="css/bootstrap-responsive.css" rel="stylesheet"/>
<link href="css/style.css" rel="stylesheet"/>
<link href="color/default.css" rel="stylesheet"/>
<link href="css/flexslider.min.css" rel="stylesheet"/>
</head>
<body>
<!-- navbar -->
<div class="navbar-wrapper">
<div class="navbar navbar-inverse navbar-fixed-top">
<div class="navbar-inner">
<div class="container">
<!-- Responsive navbar -->
<a class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
</a>
<h1 class="brand"><a href="index.html"><img src="img/icons/Quik-biz.png"/></a></h1>
<!-- navigation -->
<nav class="pull-right nav-collapse collapse">
<ul class="nav" id="menu-main">
<li class="dropdown">
<a href="#" title="Company Registration">Company Registration</a>
<ul class="menus unstyled sub_navigation">
<li><a href="business-setup.html">Business Setup in Asia</a></li>
<li><a href="types-of-companies.html">Types of Companies</a></li>
<li><a href="company-registration.html">Company Registration</a></li>
</ul>
</li>
<li class="dropdown">
<a href="#" title="Immigration Malaysia">Immigration Services</a>
<ul class="menus unstyled sub_navigation">
<li><a href="immigration-laws-in-asia.html">Immigration Laws in Asia</a></li>
<li><a href="#">Malaysia Visa Process</a></li>
<li><a href="#">Singapore Visa Process</a></li>
</ul>
</li>
<li class="dropdown">
<a href="company-secretarial-services.html" title="Work Permit">Company Secretarial Services</a>
<!-- <ul class="menus unstyled">
							<li><a href="#">Working Permit Malaysia</a></li>
							<li><a href="#">Work Permit Malaysia FAQ</a></li>
							<li><a href="#">Employment Passes in Malaysia</a></li>
							<li><a href="#">Working Permit Spouse of Malaysian</a></li>						
						</ul>	 -->
</li>
<li class="dropdown">
<a href="#" title="Accounting">Accounting</a>
<ul class="menus unstyled sub_navigation">
<li><a href="accounting.html">What is Accounting?</a></li>
<li><a href="accounting-services.html">Outsourcing Accounting Services</a></li>
<li><a href="advantages-outsourcing.html">Advantages of outsourcing accounting</a></li>
</ul>
</li>
<li class="dropdown">
<a href="#" title="Payroll">Payroll</a>
<ul class="menus unstyled">
<li><a href="payroll-service.html">Payroll Services Asia Pacific</a></li>
</ul>
</li>
<li><a href="contact-us.html" title="Contact Us">Contact Us</a></li>
</ul>
</nav>
</div>
</div>
</div>
</div>
<!-- Header area -->
<div class="header-slider" id="header-wrapper">
<header class="clearfix flexslider">
<ul class="slides">
<li><img src="img/bg/bg-1.jpg"/></li>
<li><img src="img/bg/bg-2.jpg"/></li>
<!-- <li><img src="img/bg/bg-3.jpg" /></li>
		  <li><img src="img/bg/bg-5.jpg"/></li>	-->
</ul>
</header>
</div>
<!-- section: team -->
<section class="section" id="about">
<div class="container">
<h4>Who We Are</h4>
<div class="row">
<div style="text-align: center;">
<div class="we-are">
<p>
					Quickbiz Asia is a professional Accounting, Payroll and secretarial Services Hub, with excellent service and on time deliverable, and provides a broad spectrum of corporate secretarial services, among which business consultancy and company incorporation.
				</p>
</div>
</div>
</div>
</div>
<!-- /.container -->
</section>
<!-- end section: team -->
<!-- section: services -->
<section class="section orange" id="services">
<div class="container">
<h4>Services</h4>
<!-- Four columns -->
<div class="row services">
<div class="span3 animated-fast flyIn">
<div class="service-box">
<img alt="" src="img/icons/payroll-service.png"/>
<h2>Payroll Services</h2>
<p>
					Quickbiz Asia is a professional Payroll Services, with excellent service and on time deliverable, We take all your stress in payroll computation and deliver custom based payroll support as required by our customers. Our payroll business services take all the hassle out of payroll and benefits administration, for employers and employees alike.
				</p>
</div>
</div>
<div class="span3 animated flyIn">
<div class="service-box">
<img alt="" src="img/icons/immigration.png"/>
<h2>Immigration Malaysia</h2>
<p>
					 Quickbiz provides an quick advice and support on Immigration services with a core team which has more than 12 years of exceptional experience in Immigration services
				</p>
</div>
</div>
<div class="span3 animated-fast flyIn">
<div class="service-box">
<img alt="" src="img/icons/accounting.png"/>
<h2>Accounting Services</h2>
<p>
					Quickbiz provides our customer with low cost and quality way of maintaining their accounts, we have a exceptional team with more than 13 years of hands on experience in accounting. We ensure that our customers can take quick business decision through our services as we not only completes the accounts but we ensure that our customers receive timely monthly updates with financial summary. 
				</p>
</div>
</div>
</div>
</div>
</section>
<!-- end section: services -->
<!-- section: contact -->
<!-- <section id="contact" class="section green">
<div class="container">
	<h4>Contact Us</h4>
	
	<div class="col-md-4">
		<div>
			<h6>Singapore</h6>
			<p>3, Raffles Place, #09-02,Bharat Building Singapore, 048617<br/>
				Tel: +65 6817 3577 Fax: +65 6817 3588<br/>
				www.ensoft.com.sg 
			</p>
		</div>
		<div>
			<h6>Australia</h6>
			<p>Level 7, 91, Phillip Street, Parramatta Sydney New South Wales 2150<br/>
			Tel: +61-2-9891 0000 Fax: +61-2-9891 1771
			</p>
		</div>
		<div>
			<h6>Thailand</h6>
			<p>152, Chartered Square Building, Floor 30, Room 30-01, Unit 30-42,
				North Sathorn Road, Silom, Bangkok 10500 Thailand<br/>
				Tel: +66 (0)2021 0400 Fax: +66 (0)2021 0401	
			</p>
		</div>
	</div>
	<div class="col-md-4">		
		<div>
			<h6>India</h6>
			<p>Unit No: 206, 2nd Floor, Building No 1, CenterPoint, Residency Road, Bangalore 560025 india,<br/>Tel & Fax: +91-80-4204 6163</p>
			<p>2nd Floor, 1-4-210/135, Netaji Nagar, Kapra, Hyderabad 500 062</p>
			<p>Mezzanine Floor, New Delhi Airport Express Line Station, Konnectus 
			Building, Bhavbhuti Marg, Connaught Place, New Delhi 110 001<br/>
			Te1: +91-1123660137 Fax: +91-1123660101</p>
		</div>
		<div>
			<h6>Hong Kong</h6>
			<p>22F, No.3, Lockhart Road, Wan Chai, Hong Kong,<br/>
			Tel: +852-3180 7888 Fax: +852-3180 7899</p>
		</div>
	</div>
	<div class="col-md-4">		
		<div>
			<h6>Malaysia</h6>
			<p>C-2-20, Co Place 1, 2270, Jalan Usahawan 2, Cyberjaya, Selangor Darul Ehsan 63000, Malaysia.<br/> Tele & Fax: +603-8322 2679</p>
		</div>
		<div>
			<h6>Philippines</h6>
			<p>Level 20-1, One Global Place, 25th Street & 5th Avenue, Bonifacio Global City, Taguig 1632 Philippines.<br/>Tel: +63-2224 2000</p>
		</div>
		<div>
			<h6>Indonesia</h6>
			<p>Level 38, Tower A, Kota Kasablanka JI. Casablanca Raya Kav. 88 Jakarta 12870.<br/> Tel: +62 21 2963 6755 Fax: +62 21 2963 8088</p>
		</div>
	</div>
	
	
	
</div>
</section> -->
<footer>
<div class="container">
<div class="row">
<div class="span6 offset3">
<ul class="social-networks">
<li><a href="#"><i class="icon-circled icon-bgdark icon-instagram icon-2x"></i></a></li>
<li><a href="#"><i class="icon-circled icon-bgdark icon-twitter icon-2x"></i></a></li>
<li><a href="#"><i class="icon-circled icon-bgdark icon-dribbble icon-2x"></i></a></li>
<li><a href="#"><i class="icon-circled icon-bgdark icon-pinterest icon-2x"></i></a></li>
</ul>
<p class="copyright">
				Copyright © 2017 QuickBiz.				
                </p><div class="credits">
<!--
                        All the links in the footer should remain intact.
                        You can delete the links only if you purchased the pro version.
                        Licensing information: https://bootstrapmade.com/license/
                        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Maxim
                    -->
</div>
</div>
</div>
</div>
<!-- ./container -->
</footer>
<a class="scrollup" href="#"><i class="icon-angle-up icon-square icon-bgdark icon-2x"></i></a>
<script src="js/jquery.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/jquery.localscroll-1.2.7-min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/isotope.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/inview.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script>
</body>
</html>
