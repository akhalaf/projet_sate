<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<link href="favicon.ico" rel="icon" sizes="16x16" type="image/gif"/>
<link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" rel="stylesheet"/>
<link href="blogads.css" rel="stylesheet"/>
<title>Blogads</title>
</head>
<body>
<div class="logo d-block mx-auto p-4" style="max-width: 600px;"><img alt="Blogads logo" class="w-100" src="img/blogads-hires-logo.png"/></div>
<div class="container">
<div class="row">
<div class="col">
<p>
                    As you may have heard, ad service Blogads.com is closed for business.
                </p>
<p>
                    Blogads had a long and exhilarating ride. When our ad service for bloggers launched here in the
                    spring of 2002, Mark Zuckerberg was a high school senior and Barack Obama was in the Illinois State
                    Senate. Since then, Blogads has seen many peers in the social media advertising ecosystem arrive
                    (Facebook, LinkedIn, Twitter, Instagram, TIkTok et al) and depart (Xanga, Friendster, AdBrite,
                    Federated Media, Gawker, Pajamas Media, and Glam, to name a few.)
                </p>
<p class="mb-0"> <img alt="" class="mx-auto d-block p-0" src="img/image03.jpg" style="max-width: 100%;"/></p>
<figcaption class="mb-2 p-3">Blogads tees were a huge hit, seen everywhere from the Taj Mahal to
                    Yosemite to swanky parties in Manhattan.</figcaption>
<p>
                    For better or worse, Blogads helped fund many of the people whose ideas became the intellectual
                    bedrock of 21st century America. Through our ups and downs since 2002, it was a privilege to see
                    sites weâve helped fundâPerezHilton, DailyKos, Feministing, TalkingPointsMemo, PoliticalWire,
                    Dlisted, Wonkette, Instapundit, LargeHeartedBoy, SportsBlogNation, Methodshop, ObscureStore,
                    GalaDarling, The Millions, Metafilter, StereoGum, ConcreteLoop, Jack &amp; Jill Politics,
                    Electoral-vote, Dooce, TechCrunch, BusBlog, ComicsCurmudgeon, Pinkdome, Scienceblog, Kuro5hin,
                    Aquarium Drunkard, CuteOverload, Virginia Postrel, DaveWiner, LitKicks, TomandLorenzo, Gothamist,
                    Betches, GayPatriot, Drudgeretort, AmericaBlog, Joho, SusieBright, Regretsy, AndrewSullivan, Blue
                    Virginia, DemocraticUnderground, Volokh Conspiracy, Matt Welch to name a fewâshape the content,
                    format and trajectory of ideas (and fun) in the US and around the world.
                </p>
<p class="mb-0"><img alt="" class="mx-auto d-block p-0 mb-0" src="img/image08.jpg" style="width: 600px;max-width:100%;"/></p>
<figcaption class="mb-2">From the spring of 2002, the original spec for Blogads, the grandmother of all
                    platforms for self-serve content advertising in social media. (Iâd totally forgotten Blogads was
                    originally called Blogadz, until Nick Denton counselled me over coffee in NYC that the âzâ felt very
                    1999.)</figcaption>
<p>
                    On a personal note, thanks to Blogads, I got to know three amazing guys, Brian Clarke, Jake Brewer
                    and David Carr, who each died in 2015. Each guy was wonderful and wonder-filled. In their
                    fieldsârespectively advertising, advocacy, and journalismâeach was a diligent practitioner and a
                    joyful philosopher. Most years I got to talk with Brian, Jake and David just a few times, by phone
                    or in person, but what a difference those talks made for me. Every conversation fanned a fire.
                    Afterwards, words cut sharper, possibilities glimmered, goals clarified, the world got bigger. I
                    miss their wisdom and grins, particularly in this busted-to-hell 2017.
                </p>
<p>
</p><hr/>
<p>
                    Done right, a business is a life raft rowing away from lifeâs inexorably sinking ship. A good
                    business always keeps you busy, often keeps you dry and sometimes keeps you sane. By definition,
                    every shuttered business is a failure. But, though Blogads.com ultimately lost its own little war,
                    everyone who participated is proud that we helped other causes win their own important battles and
                    wars.
                </p>
<ul class="list">
<li>
                        Long before there was a clear ad model for social media, we helped political blogs become
                        viable. Weâre particularly proud that Blogads underwrote the grassroots political blogs that
                        helped set the stage for Barack Obamaâs victory in 2004. (More recently, we rue the ads we sold
                        for bloggers who later cheered Donald J. Trumpâs know-nothing nihilism.)
                    </li>
<li>
                        We helped underwrite the seedlings that ultimately grew into publishing empires like VOX,
                        TechCrunch, BlogHer and PopSugar. (In 2005, <a href="https://problogger.com/how-the-most-highly-visited-blogs-earn-money/" target="_blank">23 of the 30 biggest blogs</a> relied on our service.)
                    </li>
<li>
                        After much evolution, the reigning ad format of the twenty-teens (and onward?) is not much
                        different from the original 2002 âblogad,â our eponymous proprietary ad unit that combined an
                        image and link-filled text to deliver readable, site-synchronized information. This ad format
                        has become the standard for successful advertising and is today called ânativeâ advertising.
                        Itâs with zero chagrin and some laughs that I recall how intensely annoyed conventional
                        advertisers (including my buddy Michael Bassik) were when we introduced the blogad. âItâs a lot
                        easier to massproduce a 300Ã250 gif that can run anywhere,â the critics said. To which we
                        replied: exactly!
                    </li>
<li>
                        Self-service advertising, which Blogads helped pioneer, is now standard too â with DIY ads
                        driving revenues for Facebook, Twitter, LinkedIn and Google. (Hereâs a 2011 video of <a href="https://youtu.be/ybuhqCfPZQU" target='_blank"'>building a blogad</a>.)
                    </li>
<li>
                        My <a href="http://www.pressflex.com/news/fullstory.php/aid/54/Blogonomics:_making_a_living_from_blogging.html" target="_blank">2002 prediction</a> that the social media (then basically the blogosphere)
                        would eventually âpower knowledge-sharing far more profound than anything offered by current
                        mediaâ has been proven true 1000-times over. Driven by Darwinian pressures and abetted by
                        zero-cost CMSes, self-publishing has morphed and mutated to invade every existing media niche
                        and create many new niches. Social media has speciated into hundreds of different forms, forums
                        and idiomsâ¦ <a href="http://www.twitter.com/" target="_blank">snippets of text</a>, <a href="http://www.instagram.com/" target="_blank">photos</a>, <a href="http://purple.ai/" target="_blank">wifi-as-community</a>, <a href="http://www.snapchat.com/" target="_blank">vanishing photos</a>, <a href="https://gimletmedia.com/" target="_blank">audio</a>, videos, <a href="http://www.giphy.com/" target="_blank">animated
                            gifs</a>.
                    </li>
<li>
                        Traditional news and entertainment businesses have become, as I predicted in 2002, like
                        ârefrigerator salesmen trudging into the next ice age.â The volume and pace of people-created
                        content has diluted many publishersâ mindshare to the point that theyâre irrelevant. The social
                        media sideshow of 2002 has become 2017âs big top, with publishingâs once-mighty elephants now
                        beleaguered and begging for peanuts from the former audience (who are too busy selecting an
                        Instagram filter to notice.)
                    </li>
</ul>
<p>
                    Sadly, the Utopian dream that ad-supported, autonomous, righteous bloggers would flood the world
                    with torrents of cleansing information has proven to be false.
                </p>
<p>
                    First, the money isnât there. The supply of what we in the ad industry call âpage impressionsâ
                    (articles, posts, comments, videos, photos, animated gifs, comments on comments, smiley faces on
                    comments on posts about videos about memesâ¦ etc etc etcâ¦ content!) has multiplied, far oustripping
                    advertiserâs demand. Compounding the problem, programmatic ads, technology which allows advertisers
                    to bypass name brand publishers to target readers based on demographically specified cookies, has
                    proven immensely cost effective. (For example, Cisco doesnât need to pay $50k to reach a few
                    thousand (hundred?) CTOs on the New York Timesâ site or even TechCrunch because, for just a few
                    nickels, it can use cookies to locate and target the same people elsewhere on the web.
                </p>
<p>
                    Second, transparency. Some blogs are selling posts to advertisers without disclosing the impetus for
                    the post. Many others are creating information and spin to earn ideological or social brownie
                    points.
                </p>
<p>
                    Third, noise. Many blogs are now irrelevant amid a surplus of cat videos and bogus cancer cures. In
                    politics, a few blogs like PoliticalWire, DailyKos and TalkingPointsMemo battle on, often relying on
                    subscriptions. Their efforts notwithstanding, Facebook, with all its secret nuclear-powered
                    link-algorithms, now propels factoids and opinions far faster and further than human hands could
                    ever manage. As dana boyd notes, <a href="https://points.datasociety.net/hacking-the-attention-economy-9fa1daca7a37#.h1m9rd3zk" target="_blank">social media has proven to be both an R&amp;D lab and ICBM of falsehood.</a> Even
                    highly evolved state-of-the-art platforms like Medium are <a href="http://www.niemanlab.org/2017/01/medium-lays-off-dozens-as-it-tries-to-find-a-publishing-business-model-that-may-not-actually-exist-yet/" target="_blank">failing in their mission to power great bloggers.</a>
</p>
<p>
                    In short, most blogs are bust. Sadly, ad-driven fact-focused journalism is begging for bailouts too.
                    Maybe weâre all lost.
                </p>
<p>
</p><hr/>
<p>In addition to sitting in the front row as history was made and <a href="https://www.technewsworld.com/story/38588.html" http:="" target="_blank"></a>saving some marriages, we met a lot of great people.
                </p>
<p>
                    Many colleagues helped build and power Blogads, including Angie Riley, Anthony Perry, Balazs Kolbay,
                    Balint Erdi, Bevin Tighe, Bryan Rahija, Csaba Garay, Delphine Andrews, Devin Kelly, Donald Hughes,
                    Gabor Veres, Hano Grimm, Ivana Vidovic, Jessica Siracusa, Joe Stanton, John Faranda, Kate Studwell,
                    Katie Brauer, Kaley Credle, Kristof Strobl, Lanae Ball, Mark Wasserman, Marybeth Grossman, Megan
                    Mitzel, Miklos Gaspar, Nick Faber, Nicole Bogas, Orsolya Kerner, Paige Wilcox, Peter Klein, Piroska
                    Salamon, Rachel Hirsh, Rachel McGorman, Robert Mooney, Suzanne Despres, Tamas Decsi, Tina Merrill,
                    Viktor Bodrogi, Will Elliot, Zach Strom, and (alphabetically last but definitely not least!) Zsolt
                    Remenyi.
                </p>
<p>
                    Working at Blogads gave us the chance to collaborate with smart people like Aaron Dinin, Aaron Gell,
                    Aaron Peckham, Adam Cohen, Adam Connor, Adam Mordecai, Alaina Brown, Alan Rosenblatt, Amy Alkon, Amy
                    Schatz, Andrea Rosen, Andrew Bleeker, Andrew Golis, Andrew Meyer, Andrew S. Tanenbaum, Andru
                    Edwards, Andy Carvin, Anil Dash, Amanda Marcotte, Amy Langfield, Anamarie Cox, Andrew Krucoff,
                    Andrew Slack, Anil Dash, Anne Althouse, Anne Handley, Ari Rabin Havt, Azeem Azhar, Barby Lava, Ben
                    Clark, Ben Fisher, Ben Rahm, Ben Smith, Ben Sullivan, Bill Hartnett, Bill Quick, Billy Dennis, Biz
                    Stone, BL Ochman, Bob Fertik, Brad DeLong, Brad Friedman, Brian Dell, Brian Morrissey, Brian Reich,
                    Brian Stelter, Byron Crawford, Cameron Winklevoss, Carol Darr, Caroline McCarthy, Charley Territo,
                    Charlotte Selles, Chris Casey, Chris Heivly, Chris Jones, Chris Kenngott, Chris Locke, Chris
                    Pirillo, Chris Rabb, Christian Borges, Christian Crumlish, Christopher Batty, Cheryl Contee, Choire
                    Sicha, Chuck DeFeo, Clay Johnson, Clayton Ryan (Tovarich!), Clive Thompson, Clotilde Dusoulier,
                    Colby Cosh, Cyrus Krohn, Damien LaManna, Dan Gilmore, Darla Mack, Dave Karpf, Dave Neal, Dave
                    Taylor, Dave Winer, David Almancy, David Berkowitz, David Corn, David Hertog, David Kaplan, David
                    Lat, David Pinto, David Weinberger, Deborah Schultz, Digby Parton, Doc Searls, Don Steele, Doug
                    Gordon, Drew Curtis, Duncan Black, Ed Cohn, Elan Genius, Eli Pariser, Elizabeth Spiers, Emma Battle,
                    Erik Damato, Eric Muller, Eric Schoenborn, Erik Martin, Esther Dyson, Ethan Zuckerman, Eve Fox, Ezra
                    Klein, Farah Miller, Farra Trompeter, Felix Salmon, Frank Radice, Garance Franke-Ruta, Garrett
                    Graff, Gene Smith, Glenn Greenwald, Glenn Reynolds, Gokul Rajaram, Greg Galant, Greg Greene, Greg
                    Palmer, Gregory Ng, Hank Dearden, Hal Malchow, Heather Armstrong, Heather Cocks, Heather Holdridge,
                    Hollis Thomas, Howard Owens, Hylton Jolliffe, Hugh Forrest, Hugh MacLeod, Ian Schafer, Ilan Zechory,
                    Ilyse Hogue, Jackie Danicki, Jackie Huba, Jackie Schechner, Jake Dobkin, Jake Orlowitz, James Avery,
                    James Joyner, Jason Calacanis, Jason Rosenberg, Jay Rosen, JB Hopkins, JC Christian, JD Ashcroft, JD
                    Lasica, Jeannine Sessum, Jeannine Harvey, Jeff Cohen, Jeff Jarvis, Jeff Mascott, Jen Nedeau, Jeralyn
                    Merritt, Jess Amason, Jesse Taylor, Jessica Morgan, Jessica Valenti, Jill Fehrenbacher, Jim
                    Romenesko, Jim Treacher, Joe Fuld, Joe Gandelman, Joel Bartlett, John Amato, John Aravosis, John
                    Armstrong, John Byrne, John Dick, John Henke, John Hlinko, John Simpson, John Rees, Jon Accarrino,
                    Jonah Seiger, Josh Fruhlinger, Josh Glasstetter, Josh Marshall, Juan Cole, Juan Melli, Judd Legum,
                    Juhee Kim, Julie Germany, Justin Miller, Justin Olberman, Justin Safie, Karen Curry, Kate Kaye,
                    Katie Harbath, Kari Chisholm, Karl Frisch, Keegan Goudiss, Ken Deutsch, Ken Layne, Kenyatta Cheese,
                    Kevin Dando, Kevin Drum, Kevin Reid, Kirk Ross, Kombiz Lavasany, Laura Packard, Lenore Skenazi,
                    Leslie Bradshaw, Levi Asher, Libby Pigg, Lindsay Beyerstein, Lisa Cequeira, Lizz Hazeltine, Lola
                    Elfman, Loren Baker, Lynn Siprelle, Marc Cenedella, Marc Eliot Stein, Mark Blumenthal, Mark Drapeau,
                    Mark Glaser, Mark Johnson, Mark Nickolas, Mark OâBrien, Mark Rabinowitz, Mark Skidmore, Mark
                    Traphagen, Markos Moulitsas, Mario Lavandiera, Marshall Kirkpatrick, MaryKatherine Brewer, Matt
                    Britton, Matt Diffey, Matt Haughey, Matt Welch, Matthew Yglesias, Matthew Zablud, Max Sawicky, Meg
                    Frost, Megan McArdle, Mica Sifry, Micah Baldwin, Michael Allen, Michael Bassik, Michael Goff,
                    Michael K, Michael Miraflor, Michael Shaw, Michael Silberman, Michael Turk, Michelle Coyle, Mike
                    Butcher, Mike Krempasky, Mike Monello, Mike Panetta, Mike Street, Mindy Finn, Morra Aarons-Mele,
                    Moxie, Murshed Zaheed, Nathan Wilcox, Neil Sroka, Nicco Mele, Nick Denton, Nick Gillespie, Noel
                    Hidalgo, Oliver Willis, Olivier Travers, Ori Elraviv, PJ Rodriguez, Pam Spaulding, Patrick Nielsen
                    Hayden, Patrick Ruffini, Paul Chaney, Paul Jones, Peter Daou, Philip Kaplan, Phil Noble, Rachel
                    Hruska MacPherson, Rafat Ali, Raven Brooks, Rebecca Leib, Rebecca Schoenkopf, Rex Hammock, Richard
                    Luckett, Richard Schlackman, Richard Turner, Rick Bruner, Rick Calvert, Rick Klau, Ricky Engelberg,
                    Rob Neppell, Robert Bluey, Robert Gorell, Rogers Cadenhead, Roman Godzich, Ron Gunzberger, Rosalyn
                    Lemieux, Roxanne Cooper, Rusty Foster, Sandy Hussein, Sandy Marks, Sanford Dickert, Sara Holoubek,
                    Scott Goodstein, Sean Paul Kelley, Seth Miller, Shana Glickfield, Shankar Gupta-Harrison, Shannon
                    Okey, Shira Lazar, Simon Rosenberg, Soren Dayton, Stacy Kramer, Stephen Fraser, Steve Hall, Steve
                    Outing, Steve Rubel, Steven Waldman, Susie Bright, Taegan Goddard, Taylor Marsh, Teresa Nielsen
                    Hayden, Tig Tillinghast, Tim Ferris, Tim Tagaris, Todd Rubenstein, Todd Sawicki, Tom Limongello, Tom
                    Matzzie, Tom Watson, Tomer Treves, Tony Pierce, Tracy Viselli, Tristan Roy, Tucker Maxx, Twanna
                    Hines, Ty Montagu, Upendra Shardanand, Virginia Postrel, Waldo Tibbetts, Wayne Sutton, Will
                    Rockafellow, William Buetler, Zach Rogers, Zadi Diaz, Zeynep Tufekci and many others.
                </p>
<p>
                    Again, last and definitely not least, my wife Zsofiâs unique blend of idealism, skepticism and
                    encouragement was invaluable.
                </p>
<p>
                    Thank you all. Lifejackets on. If youâre feeling nostalgic, you can browse our old blog archives (<a href="http://weblog.blogads.com/" target="_blank">1</a> or <a href="http://web.blogads.com/blog/#axzz6URhGpcLc" target="_blank">2</a>) or browse our <a href="https://www.facebook.com/Blogads/photos/" target="_blank">Facebook photo galleries</a>.
                    Next up, anyone want to go for a <a href="http://racery.com/create-virtual-races/" target="_blank">virtual run in Paris</a>? :)
                </p>
<hr class="pb-4"/>
<div class="d-flex"><img alt="Henry Copeland" class="rounded-circle img-thumbnail float-left" src="img/hc.jpeg"/>
<div class="ml-4">Henry Copeland<br/>
                        March 1, 2017<br/>
                        Chapel Hill, North Carolina<br/></div>
</div>
<hr class="pt-4 "/>
<h2 class="header p-5 text-center">Front pages</h2>
<div class="homepage-gallery d-flex flex-wrap justify-content-center">
<div class="homepage-gallery-item w-100" style="max-width: 500px !important">
<img alt="blogads.com frontpage from 2003" class="w-100 border" src="img/image05.png"/>
<figcaption class="mb-2 p-3">2003</figcaption>
</div>
<div class="homepage-gallery-item w-100" style="max-width: 500px !important">
<img alt="blogads.com frontpage from 2005" class="w-100 border" src="img/image06.png"/>
<figcaption class="mb-2 p-3">2005</figcaption>
</div>
<div class="homepage-gallery-item w-100" style="max-width: 500px !important">
<img alt="blogads.com frontpage from 2006 " class="w-100 border" src="img/image02.png"/>
<figcaption class="mb-2 p-3">2006</figcaption>
</div>
<div class="homepage-gallery-item w-100" style="max-width: 500px !important">
<img alt="blogads.com frontpage from 2008-2011" class="w-100 border" src="img/image07.png"/>
<figcaption class="mb-2 p-3">2008-2011</figcaption>
</div>
<div class="homepage-gallery-item w-100" style="max-width: 500px !important">
<img alt="blogads.com frontpage from 2011-2016" class="w-100 border" src="img/image04.png"/>
<figcaption class="mb-2 p-3">2011-2016</figcaption>
</div>
</div>
<h2 class="header p-5 text-center">Office Shots</h2>
<div class="homepage-gallery d-flex flex-wrap justify-content-center">
<div class="homepage-gallery-item w-100" style="max-width: 500px !important">
<img alt="blogads.com frontpage" class="w-100 border" src="img/image10.png"/>
<figcaption class="mb-2 p-3">Budapest (Hungary office #2)</figcaption>
</div>
<div class="homepage-gallery-item w-100" style="max-width: 500px !important">
<img alt="blogads.com frontpage" class="w-100 border" src="img/image09.png"/>
<figcaption class="mb-2 p-3">Carrboro (US office #2)</figcaption>
</div>
<div class="homepage-gallery-item w-100" style="max-width: 500px !important">
<img alt="blogads.com frontpage" class="w-100 border" src="img/image11.png"/>
<figcaption class="mb-2 p-3">Durham (US office #3) </figcaption>
</div>
<div class="homepage-gallery-item w-100" style="max-width: 500px !important">
<img alt="blogads.com frontpage" class="w-100 border" src="img/image12.jpg"/>
<figcaption class="mb-2 p-3">Durham (US office #4)</figcaption>
</div>
<div class="homepage-gallery-item w-100" style="max-width: 500px !important">
<img alt="blogads.com frontpage" class="w-100 border" src="img/image01.png"/>
<figcaption class="mb-2 p-3">Keep on rolling Blogads! <br/>(inside front cover SXSW
                                    program, 2008) </figcaption>
</div>
</div>
</div>
</div>
</div>
<footer class="mx-auto w-50 text-center pt-2">© 2017-2020</footer>
<script crossorigin="anonymous" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>