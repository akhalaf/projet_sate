<!DOCTYPE html>

<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Komponenttisivut | uwanjas</title>
<meta content="text/html; charset=utf-8" http-equiv="content-type" xmlns="http://www.w3.org/1999/xhtml"/>
<meta content="" name="description"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!--uwanjas.kotisivukone.com/223472-->
<!--[if lt IE 9]><script src="https://cdn.kotisivukone.fi/libs/html5shiv/html5shiv.min.js"> </script><script>document.attachEvent("onreadystatechange", function () {
      if (document.readyState === "complete") {
        document.body.className += ' ksk_ie8';
      }
    }); </script>
    <![endif]--><link href="https://cdn.kotisivukone.fi/r201/b2980/clients/css/common.css" rel="stylesheet" type="text/css"/>
<link href="https://cdn.kotisivukone.fi/libs/jquery/ui/css/jquery-ui-1.8.20.custom.min.css" rel="stylesheet" type="text/css"/>
<link href="https://cdn.kotisivukone.fi/r201/b2980/clients/css/responsive/common_responsive.css" rel="stylesheet" type="text/css"/>
<link href="https://cdn.kotisivukone.fi/r201/b2980/clients/css/responsive/tablet_responsive.css" id="ksk_resp_tablet_css" media="only screen and (max-width: 998px)" rel="stylesheet"/>
<link href="https://cdn.kotisivukone.fi/r201/b2980/clients/css/responsive/mobile_responsive.css" id="ksk_resp_mobile_css" media="only screen and (max-width: 509px)" rel="stylesheet"/>
<style type="text/css">
        @media (max-width: 1198px) {
          div#ksk_page_wrapper { margin: 0; }
        }

        @media (max-width: 998px) {
        
         #navi { display:none; }

          body .ksk_middle_area_container_users_should_not_change {
            width: calc(100% - 1px);
          }
          
            body.twocol #right-bar {
              width: calc(100% - 1px);
              margin-left: 0;
            }
          
        }

        </style>
<noscript>
<style type="text/css">
          #navi { display:block; }  
        </style>
</noscript>
<link href="https://cdn.kotisivukone.fi/r201/b2980/clients/css/responsive/narrow_responsive.css" id="ksk_resp_narrow_css" media="only screen and (max-width: 998px)" rel="stylesheet"/>
<script type="text/javascript"> 
      var kskMiddleAreaOffset = 1;
      var kskTabletBreakpoint = 998;
      var kskMobileBreakpoint = 509;
      var kskRightBarOffset = 1;
      
        var kskHeaderKeepRatio = false;
        var kskMainImageKeepRatio = true;
      </script>
<!--[if lt IE 9]><script src="https://static.kotisivukone.fi/libs/respond.js"> </script>
    <![endif]--><link href="https://asiakas.kotisivukone.com/files/uwanjas.kotisivukone.com/.css/stylesheet-7.css" id="page_stylesheet" rel="stylesheet" type="text/css"/>
<style type="text/css">#left-bar { display: none; } #right-bar { display: none; }</style>
<script src="//ajax.googleapis.com/ajax/libs/prototype/1.7.2.0/prototype.js" type="text/javascript"> </script><script defer="" src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.7.0/underscore-min.js" type="text/javascript"> </script><script type="text/javascript">
      (function () {
        var kskInsightCustomerIdentifier = 'ksk_223472';
        var kskInsightURL = 'https://insight.fonecta.fi/fi.js';
        var _fiq = _fiq || [];
        _fiq.unshift(['trackPageview']);
        _fiq.unshift(['account', kskInsightCustomerIdentifier]);
        (function() {
          var _fi = document.createElement('script');
          _fi.type = 'text/javascript';
          _fi.async = true;
          _fi.src = kskInsightURL;
          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(_fi, s);
        })();
      })();
    </script>
<link href="https://fonts.googleapis.com/css?family=Arvo|IM+Fell+English|Droid+Serif" id="google-fonts" rel="stylesheet" type="text/css"/>
</head>
<body class="singlepage ksk_responsive_page normal requested_page_not_found madpages nocol using_kskpages ">
<div class="ksk_page_movement ksk_page_main_container" id="ksk_page_wrapper">
<div class="kskpages-backdrop" style="display: none;"></div>
<nav class="ksk-menu ksk_menu_wrapper ksk_menu_responsive_hidden ksk_menu_right_false">
<div class="ksk_inline_trick_users_should_not_change">
<div class="ksk_container ksk_menu_block" id="menu">
<div class="ksk_container_wrapper_menu">
<ul class="top" id="navi">
<li class="ksk_menu_responser ksk_all_pointer" onclick="KSK.showMenu(this, true);">
<a class="ksk_main_menu" href="#" style="float: left;">
<!-- -->
<!-- -->
<!-- -->
<!-- -->
<!-- -->
<!-- -->
<!-- -->
<!-- -->
<!-- --></a>
<div id="responsive_menu_button">
<div class="responsive_menu_button_line"></div>
<div class="responsive_menu_button_line"></div>
<div class="responsive_menu_button_line"></div>
</div>
</li>
<li class="mli first_link" id="menu_li1" title="">
<a class="ksk_main_menu" href="/" id="menulink1"><span>Etusivu</span></a>
</li>
<li class="mli" id="menu_li17" title="">
<a class="ksk_main_menu" href="/news" id="menulink17"><span>News</span></a>
</li>
<li class="mli ksk_has_sublink " id="menu_li8" title="">
<a class="ksk_main_menu" href="/our-dogs" id="menulink8"><span>Our Dogs</span></a>
<ul class="ksk_sub_menu_item">
<li class="mli first_link" id="menu_li9" title="">
<a class="ksk_sub_menu" href="/nala" id="menulink9"><span>Nala</span></a>
</li>
<li class="mli" id="menu_li10" title="">
<a class="ksk_sub_menu" href="/kiara" id="menulink10"><span>Kiara</span></a>
</li>
<li class="mli" id="menu_li11" title="">
<a class="ksk_sub_menu" href="/afia" id="menulink11"><span>Afia</span></a>
</li>
<li class="mli" id="menu_li34" title="">
<a class="ksk_sub_menu" href="/winda" id="menulink34"><span>Winda</span></a>
</li>
<li class="clearli">
<!-- -->
</li>
</ul>
</li>
<li class="mli ksk_has_sublink " id="menu_li18" title="">
<a class="ksk_main_menu" href="/litters" id="menulink18"><span>Litters</span></a>
<ul class="ksk_sub_menu_item">
<li class="mli first_link" id="menu_li25" title="">
<a class="ksk_sub_menu" href="/litter-2010-rod-x-nala" id="menulink25"><span>Litter 2010 (Rod x Nala)</span></a>
</li>
<li class="mli" id="menu_li20" title="">
<a class="ksk_sub_menu" href="/litter-2012-denga-x-athena" id="menulink20"><span>Litter 2012 (Denga x Athena)</span></a>
</li>
<li class="mli" id="menu_li21" title="">
<a class="ksk_sub_menu" href="/litter-2013-klever-x-kiara" id="menulink21"><span>Litter 2013 (Klever x Kiara)</span></a>
</li>
<li class="mli" id="menu_li22" title="">
<a class="ksk_sub_menu" href="/litter-2014-lynnard-x-kiara" id="menulink22"><span>Litter 2014 (Lynnard x Kiara)</span></a>
</li>
<li class="mli" id="menu_li23" title="">
<a class="ksk_sub_menu" href="/litter-2017-boy-x-afia" id="menulink23"><span>Litter 2017 (Boy x Afia)</span></a>
</li>
<li class="mli" id="menu_li36" title="">
<a class="ksk_sub_menu" href="/litter-2018-kila-x-boy" id="menulink36"><span>Litter 2018 (Kila x Boy)</span></a>
</li>
<li class="mli" id="menu_li38" title="">
<a class="ksk_sub_menu" href="/litter-2021-arlo-x-winda" id="menulink38"><span>Litter 2021 (Arlo x Winda)</span></a>
</li>
<li class="clearli">
<!-- -->
</li>
</ul>
</li>
<li class="mli ksk_has_sublink " id="menu_li12" title="">
<a class="ksk_main_menu" href="/import" id="menulink12"><span>Import</span></a>
<ul class="ksk_sub_menu_item">
<li class="mli first_link" id="menu_li28" title="">
<a class="ksk_sub_menu" href="/kabo" id="menulink28"><span>Kabo</span></a>
</li>
<li class="clearli">
<!-- -->
</li>
</ul>
</li>
<li class="mli ksk_has_sublink " id="menu_li13" title="">
<a class="ksk_main_menu" href="/expport" id="menulink13"><span>Export</span></a>
<ul class="ksk_sub_menu_item">
<li class="mli first_link" id="menu_li30" title="">
<a class="ksk_sub_menu" href="/raatum" id="menulink30"><span>RaAtum</span></a>
</li>
<li class="mli" id="menu_li29" title="">
<a class="ksk_sub_menu" href="/mila" id="menulink29"><span>Mila</span></a>
</li>
<li class="mli" id="menu_li32" title="">
<a class="ksk_sub_menu" href="/ava" id="menulink32"><span>Ava</span></a>
</li>
<li class="mli" id="menu_li37" title="">
<a class="ksk_sub_menu" href="/hekla" id="menulink37"><span>Hekla</span></a>
</li>
<li class="clearli">
<!-- -->
</li>
</ul>
</li>
<li class="mli ksk_has_sublink " id="menu_li14" title="">
<a class="ksk_main_menu" href="/breed" id="menulink14"><span>Breed</span></a>
<ul class="ksk_sub_menu_item">
<li class="mli first_link" id="menu_li26" title="">
<a class="ksk_sub_menu" href="/rotumaaritelma" id="menulink26"><span>Rotumääritelmä</span></a>
</li>
<li class="mli" id="menu_li27" title="">
<a class="ksk_sub_menu" href="/rotutietoa" id="menulink27"><span>Rotutietoa</span></a>
</li>
<li class="mli" id="menu_li31" title="">
<a class="ksk_sub_menu" href="/rodun-historia" id="menulink31"><span>Rodun historia</span></a>
</li>
<li class="mli" id="menu_li33" title="">
<a class="ksk_sub_menu" href="/rhodesiankoirat-suomessa" id="menulink33"><span>Rhodesiankoirat Suomessa</span></a>
</li>
<li class="clearli">
<!-- -->
</li>
</ul>
</li>
<li class="mli" id="menu_li16" title="">
<a class="ksk_main_menu" href="/contact-info" id="menulink16"><span>Contact info</span></a>
</li>
<!-- -->
</ul>
<div class="clear">
<!-- -->
</div>
</div>
</div>
</div>
</nav>
<div id="ksk_page_wrapper_inner">
<header class="ksk_top_wrapper">
<div class="ksk_container" id="top-bar">
<div class="ksk_container_wrapper" style="position: relative;">
<div id="logo_and_text_div" style="visibility:hidden;
        ">
<div class="ksk_logo_img" id="logo" style="float:left; margin:0px; ">
<a href="/" id="logo_href" title="Takaisin etusivulle"><img alt="" id="logo_holder" src="/www/pics/empty.gif"/></a>
<br/>
<!-- -->
</div>
<div class="ksk_logo_text" id="logo_text" style="
            font-family:IM Fell English;
            color:#FFCC24;
            font-size:60px;
           margin:0px; float:left; line-height: 1em;text-align:center;
        ">Uwanja's Rhodesian Ridgebacks</div>
</div>
<div id="extra1">
<span>
<!-- --></span>
</div>
<div id="extra2">
<span>
<!-- --></span>
</div>
</div>
</div>
</header>
<section class="ksk_main_image_wrapper">
<div class="ksk_container" id="ksk_main_image">
<div class="ksk_container_wrapper" style="position: relative;">
<div id="mainimage_logo_and_text_div" style="visibility:hidden;
         position: absolute; width: 100%;">
<div class="ksk_logo_img" id="mainimage_logo" style="float:left; margin:0px; ">
<a href="/" id="mainimage_logo_href" title="Takaisin etusivulle"><img alt="" id="mainimage_logo_holder" src="/www/pics/empty.gif"/></a>
<br/>
<!-- -->
</div>
<div class="ksk_logo_text" id="mainimage_logo_text" style="
            font-family:;
            color:#000000;
            font-size:30px;
           margin:0px; float:left; line-height: 1em;text-align:left;
        "></div>
</div>
</div>
</div>
</section>
<nav class="ksk-menu ksk_menu_wrapper">
<div class="ksk_inline_trick_users_should_not_change">
<div class="ksk_container ksk_menu_block" id="menu">
<div class="ksk_container_wrapper_menu">
<ul class="top" id="navi">
<li class="ksk_menu_responser ksk_all_pointer" onclick="KSK.showMenu(this, true);">
<a class="ksk_main_menu" href="#" style="float: left;">
<!-- -->
<!-- -->
<!-- -->
<!-- -->
<!-- -->
<!-- -->
<!-- -->
<!-- -->
<!-- --></a>
<div id="responsive_menu_button">
<div class="responsive_menu_button_line"></div>
<div class="responsive_menu_button_line"></div>
<div class="responsive_menu_button_line"></div>
</div>
</li>
<li class="mli first_link" id="menu_li1" title="">
<a class="ksk_main_menu" href="/" id="menulink1"><span>Etusivu</span></a>
</li>
<li class="mli" id="menu_li17" title="">
<a class="ksk_main_menu" href="/news" id="menulink17"><span>News</span></a>
</li>
<li class="mli ksk_has_sublink " id="menu_li8" title="">
<a class="ksk_main_menu" href="/our-dogs" id="menulink8"><span>Our Dogs</span></a>
<ul class="ksk_sub_menu_item">
<li class="mli first_link" id="menu_li9" title="">
<a class="ksk_sub_menu" href="/nala" id="menulink9"><span>Nala</span></a>
</li>
<li class="mli" id="menu_li10" title="">
<a class="ksk_sub_menu" href="/kiara" id="menulink10"><span>Kiara</span></a>
</li>
<li class="mli" id="menu_li11" title="">
<a class="ksk_sub_menu" href="/afia" id="menulink11"><span>Afia</span></a>
</li>
<li class="mli" id="menu_li34" title="">
<a class="ksk_sub_menu" href="/winda" id="menulink34"><span>Winda</span></a>
</li>
<li class="clearli">
<!-- -->
</li>
</ul>
</li>
<li class="mli ksk_has_sublink " id="menu_li18" title="">
<a class="ksk_main_menu" href="/litters" id="menulink18"><span>Litters</span></a>
<ul class="ksk_sub_menu_item">
<li class="mli first_link" id="menu_li25" title="">
<a class="ksk_sub_menu" href="/litter-2010-rod-x-nala" id="menulink25"><span>Litter 2010 (Rod x Nala)</span></a>
</li>
<li class="mli" id="menu_li20" title="">
<a class="ksk_sub_menu" href="/litter-2012-denga-x-athena" id="menulink20"><span>Litter 2012 (Denga x Athena)</span></a>
</li>
<li class="mli" id="menu_li21" title="">
<a class="ksk_sub_menu" href="/litter-2013-klever-x-kiara" id="menulink21"><span>Litter 2013 (Klever x Kiara)</span></a>
</li>
<li class="mli" id="menu_li22" title="">
<a class="ksk_sub_menu" href="/litter-2014-lynnard-x-kiara" id="menulink22"><span>Litter 2014 (Lynnard x Kiara)</span></a>
</li>
<li class="mli" id="menu_li23" title="">
<a class="ksk_sub_menu" href="/litter-2017-boy-x-afia" id="menulink23"><span>Litter 2017 (Boy x Afia)</span></a>
</li>
<li class="mli" id="menu_li36" title="">
<a class="ksk_sub_menu" href="/litter-2018-kila-x-boy" id="menulink36"><span>Litter 2018 (Kila x Boy)</span></a>
</li>
<li class="mli" id="menu_li38" title="">
<a class="ksk_sub_menu" href="/litter-2021-arlo-x-winda" id="menulink38"><span>Litter 2021 (Arlo x Winda)</span></a>
</li>
<li class="clearli">
<!-- -->
</li>
</ul>
</li>
<li class="mli ksk_has_sublink " id="menu_li12" title="">
<a class="ksk_main_menu" href="/import" id="menulink12"><span>Import</span></a>
<ul class="ksk_sub_menu_item">
<li class="mli first_link" id="menu_li28" title="">
<a class="ksk_sub_menu" href="/kabo" id="menulink28"><span>Kabo</span></a>
</li>
<li class="clearli">
<!-- -->
</li>
</ul>
</li>
<li class="mli ksk_has_sublink " id="menu_li13" title="">
<a class="ksk_main_menu" href="/expport" id="menulink13"><span>Export</span></a>
<ul class="ksk_sub_menu_item">
<li class="mli first_link" id="menu_li30" title="">
<a class="ksk_sub_menu" href="/raatum" id="menulink30"><span>RaAtum</span></a>
</li>
<li class="mli" id="menu_li29" title="">
<a class="ksk_sub_menu" href="/mila" id="menulink29"><span>Mila</span></a>
</li>
<li class="mli" id="menu_li32" title="">
<a class="ksk_sub_menu" href="/ava" id="menulink32"><span>Ava</span></a>
</li>
<li class="mli" id="menu_li37" title="">
<a class="ksk_sub_menu" href="/hekla" id="menulink37"><span>Hekla</span></a>
</li>
<li class="clearli">
<!-- -->
</li>
</ul>
</li>
<li class="mli ksk_has_sublink " id="menu_li14" title="">
<a class="ksk_main_menu" href="/breed" id="menulink14"><span>Breed</span></a>
<ul class="ksk_sub_menu_item">
<li class="mli first_link" id="menu_li26" title="">
<a class="ksk_sub_menu" href="/rotumaaritelma" id="menulink26"><span>Rotumääritelmä</span></a>
</li>
<li class="mli" id="menu_li27" title="">
<a class="ksk_sub_menu" href="/rotutietoa" id="menulink27"><span>Rotutietoa</span></a>
</li>
<li class="mli" id="menu_li31" title="">
<a class="ksk_sub_menu" href="/rodun-historia" id="menulink31"><span>Rodun historia</span></a>
</li>
<li class="mli" id="menu_li33" title="">
<a class="ksk_sub_menu" href="/rhodesiankoirat-suomessa" id="menulink33"><span>Rhodesiankoirat Suomessa</span></a>
</li>
<li class="clearli">
<!-- -->
</li>
</ul>
</li>
<li class="mli" id="menu_li16" title="">
<a class="ksk_main_menu" href="/contact-info" id="menulink16"><span>Contact info</span></a>
</li>
<!-- -->
</ul>
<div class="clear">
<!-- -->
</div>
</div>
</div>
</div>
</nav>
<section class="ksk_columns_wrapper" id="ksk_columns_wrapper">
<div class="ksk_container" data-kskpage-draft="false" data-kskpage-id="-1" id="columns">
<div class="ksk_container_wrapper" id="ksk_middle_block">
<div class="ksk_middle_area_container_users_should_not_change">
<div class="ksk_middle_area" id="content">
<div id="content_top">
<!-- -->
</div>
<p>Hakemaasi sivua ei löytynyt!</p>
<!-- -->
<div id="content_bottom">
<!-- -->
</div>
</div>
</div>
<aside class="ksk_sidebars" id="left-bar">
<!-- -->
</aside>
<aside class="ksk_sidebars" id="right-bar">
<div id="common_rightbar_sideboxes"></div>
<!-- -->
</aside>
<div class="clear">
<!-- -->
</div>
</div>
</div>
</section>
</div>
<footer class="ksk_footer_wrapper">
<div class="ksk_container" id="footer">
<div class="ksk_container_wrapper">
<div id="footer_content">
<div class="kskpages_container" data-kskpage-draft="" data-kskpage-id="">
<div class="madpages_component ksk_no_margins" data-kskpage-component-id="">
<div class="kskpage_text_comp" style="margin-bottom :  15px; 
      margin-top :  15px; 
      margin-right :  15px; 
      text-align :  center; 
      margin-left :  15px; 
      ">
<div class="madpages_element madpages_paragraph " data-ksk-el-id="1_footer_1" data-ksk-type="paragraph" id="madpages_element_paragraph_1_footer_1" style="display: block;"><div>© Copyright 2010 kennel Uwanja's
</div></div>
</div>
</div>
</div>
</div>
</div>
</div>
</footer>
</div>
<div id="extra3">
<span>
<!-- --></span>
</div>
<div id="extra4">
<span>
<!-- --></span>
</div>
<script src="//code.jquery.com/jquery-3.5.1.min.js"> </script><script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript"> </script><script src="https://cdn.kotisivukone.fi/r201/b2980/clients/js/kotisivukone_responsive.js" type="text/javascript"> </script><script src="https://cdn.kotisivukone.fi/r201/b2980/clients/js/kotisivukone.js" type="text/javascript"> </script><script type="text/javascript">
      var ajax_public_url = "/public_ajax/";
      var page_id = '223472';

      
          var logo_position_x =  0;
          var logo_position_y = 0;
          var logo_text_position_x = 0;
          var logo_text_position_y = 16;
        
          var mainimage_logo_position_x =  0;
          var mainimage_logo_position_y = 0;
          var mainimage_logo_text_position_x = 0;
          var mainimage_logo_text_position_y = 0;
        </script><script src="https://cdn.kotisivukone.fi/libs/cookieconsent/cookieconsent.js"> </script>
<div id="dialog-cookie-consent" style="display:none;" title="Evästeiden käyttö">
<img src="//cdn.kotisivukone.fi/www/pics/loader2.gif"/></div>
<link href="https://cdn.kotisivukone.fi/libs/cookieconsent/cookieconsent.min.css" rel="stylesheet" type="text/css"/>
<script>
      var cookie_text_url = "/app/public_ajax/?resource=dialog&action=get_popup_content&file_name=cookies_fi";
      function showModal() {
      jQuery( "#dialog-cookie-consent" ).load(cookie_text_url);
      jQuery( "#dialog-cookie-consent" ).dialog({
      dialogClass: "cookie-consent",
      resizable: false,
      draggable: false,
      height: "auto",
      width: jQuery(window).width() > 700 ? 700 : 'auto',
      maxWidth: 680,
      modal: true,
      fluid: true,
      position: {my: "top-600%"},
      buttons: {
      "OK": function() {
      jQuery( this ).dialog( "close" );
      },
      }

      });
      jQuery( ".ui-widget-overlay" ).addClass("cookie-consent");
      }

    </script><script>
      window.addEventListener("load", function(){
      window.cookieconsent.initialise({
      "palette": {
      "popup": {
      "background": "#ececec",
      "text": "#575757",
      "link": "#575757"
      },
      "button": {
      "background": "#959595",
      "text": "#ffffff"
      }
      },
      "position": "bottom-left",
      
      "content": {
      
          "message": "Jotta sivuston käyttö olisi sinulle sujuvaa ja mainokset kiinnostavia, käytämme kumppaniemme kanssa sivustolla evästeitä. Jos et hyväksy evästeitä, muuta selaimesi asetuksia. Hyväksyn evästeiden käytön jatkamalla sivuston käyttöä.",
          "dismiss": "Hyväksy",
          "link": "Lisätietoja"
        
      }
      })});
    </script>
<!--[if lt IE 9]>
    <script src="https://static.kotisivukone.fi/libs/respond/respond.min.js"> </script>
      <![endif]-->
</body>
</html>
