<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]--><!--[if IE 9]> <html lang="en" class="ie9"> <![endif]--><!--[if !IE]><!--><html lang="en"> <!--<![endif]-->
<head>
<title>Home | Sultanpur Fruits &amp; Vegitables Trading L.L.C. | Vegetable Suppliers Dubai | Fruit Suppliers Dubai, UAE</title>
<!-- Meta -->
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- Favicon -->
<link href="favicon.ico" rel="shortcut icon"/>
<!-- CSS Global Compulsory -->
<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<link href="assets/css/style.css" rel="stylesheet"/>
<!-- CSS Implementing Plugins -->
<link href="assets/plugins/line-icons/line-icons.css" rel="stylesheet"/>
<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<link href="assets/plugins/flexslider/flexslider.css" rel="stylesheet"/>
<link href="assets/plugins/layer-slider/layerslider/css/layerslider.css" rel="stylesheet"/>
<!-- CSS Theme -->
<link href="assets/css/theme-colors/default.css" rel="stylesheet"/>
<!-- CSS Customization -->
<link href="assets/css/custom.css" rel="stylesheet"/>
</head>
<body class="header-fixeds boxed-layout container" style="background: transparent  url('assets/img/bg.png') repeat 0% 0%;">
<div class="wrapper">
<div style="background:#FFFFFF;">
<!--=== Header ===-->
<div class="header header-sticky">
<!-- End Topbar -->
<!-- Navbar -->
<div class="navbar navbar-default mega-menu" role="navigation">
<div class="container">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-responsive-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="fa fa-bars"></span>
</button>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse navbar-responsive-collapse" style="margin-left:-15px;">
<ul class="nav navbar-nav">
<!-- Home -->
<li class=""><a href="index.php">Home</a></li>
<li><a href="about.php">About Us</a></li>
<li class=""><a href="services.php">Our Services</a></li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Product</a>
<ul class="dropdown-menu">
<li><a href="product.php?page=Fruits&amp;pid=1">Fruits</a></li>
<li><a href="product.php?page=Vegitables&amp;pid=2">Vegitables</a></li>
</ul>
</li>
<li class=""><a href="order.php">Order Request</a></li>
<li class=""><a href="contact.php">Contact Us</a></li>
</ul>
</div><!--/navbar-collapse-->
<div class="lang">
<div id="google_translate_element"></div>
<script type="text/javascript">
                    function googleTranslateElementInit() {
                      new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'ar,bn,da,de,en,es,fr,hi,ja,ko,ml,sv,th,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
                    }
                    </script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>
</div>
</div>
</div>
<!-- End Navbar -->
<div class="topbar">
<div class="container">
<div class="row logo-wrapper">
<div class="col-md-3">
<a class="navbar-brand logo" href="index.php">
<img alt="Logo" id="logo-header" src="assets/img/logo.png"/>
</a>
</div>
<div class="col-md-9">
<ul class="top-info">
<li>
<div class="info-box"><span class="info-icon"><i class="fa fa-envelope"> </i></span>
<div class="info-box-content">
<p class="info-box-title">Order By Mail</p>
<p class="info-box-subtitle"> khorshid@sulthanpurtrading.com </p>
</div>
</div>
</li>
<li>
<div class="info-box"><span class="info-icon"><i class="fa fa-phone"> </i></span>
<div class="info-box-content">
<p class="info-box-title">Fell Free Call Us</p>
<p class="info-box-subtitle"> +971 4 3202792/93</p>
</div>
</div>
</li>
</ul>
</div>
<!-- End Topbar Navigation -->
</div>
</div>
</div>
</div> <!--=== End Header ===-->
<div class="slide-bg">
<!--=== Slider ===-->
<div id="layerslider" style="width: 100%; height: 400px; margin: 0px auto;">
<div class="ls-slide" style="slidedirection: left; transition2d: 92,93,105; ">
<img alt="Slide background" class="ls-bg" src="assets/img/sliders/layer/bg9.jpg"/>
</div>
<!-- First slide -->
<div class="ls-slide" style="slidedirection: left; transition3d: 92,93,105; ">
<img alt="Slide background" class="ls-bg" src="assets/img/sliders/layer/bg1.jpg"/>
</div>
<!--Second Slide-->
<div class="ls-slide" style="slidedirection: right; transition3d: 92,93,105; ">
<img alt="Slide background" class="ls-bg" src="assets/img/sliders/layer/bg2.jpg"/>
</div>
<div class="ls-slide" style="slidedirection: right; transition2d: 92,93,105; ">
<img alt="Slide background" class="ls-bg" src="assets/img/sliders/layer/bg3.jpg"/>
</div>
<div class="ls-slide" style="slidedirection: right; transition3d: 92,93,105; ">
<img alt="Slide background" class="ls-bg" src="assets/img/sliders/layer/bg4.jpg"/>
</div>
<div class="ls-slide" style="slidedirection: right; transition2d: 92,93,105; ">
<img alt="Slide background" class="ls-bg" src="assets/img/sliders/layer/bg5.jpg"/>
</div>
<div class="ls-slide" style="slidedirection: right; transition3d: 92,93,105; ">
<img alt="Slide background" class="ls-bg" src="assets/img/sliders/layer/bg6.jpg"/>
</div>
<div class="ls-slide" style="slidedirection: right; transition2d: 92,93,105; ">
<img alt="Slide background" class="ls-bg" src="assets/img/sliders/layer/bg7.jpg"/>
</div>
<div class="ls-slide" style="slidedirection: right; transition3d: 92,93,105; ">
<img alt="Slide background" class="ls-bg" src="assets/img/sliders/layer/bg8.jpg"/>
</div>
</div><!--/layer_slider-->
<!--=== End Slider ===-->
</div>
</div>
<!--=== Content Part ===-->
<div class="container content gray">
<!-- Recent Works -->
<div class="row ">
<div class="col-md-12 margin-bottom-20 box">
<div class="text-center border-bottom"><h2>Welcome To Sultanpur Fruits &amp; Vegitables Trading L.L.C. </h2></div>
<p> Welcome to the fruit &amp; vegetable city website. Here you will be able to check out our latest news, find mouth watering recipes, Locate your nearest store, view our special offerings and so much more. So go ahead, browse around, and enjoy everything That his fresh site has to offer. Take a look at our different store formats and our amazing products. </p>
<p>
                Sultanpur Fruits &amp; Vegitables Trading L.L.C. is a business established by the United Arab Emirates market authority (U.A.E) in 2012. The uae is responsible for managing the dubai markets,the state of victoria's central wholesale market for fruit,vegetables.it is one of six central wholesale markets in dubai.wholesale markets are where growers send or take their produce. Wholesale markets are where gruowers send or take their produce for sale to greengrocers and other fresh retailers, restaurants, wholesalers and providers.most central markets operate during the early hours of the morning so that produce can be delivered out fresh each day to meet consumers needs for market fresh produce. Sutlhanpur Fruits &amp; Vegetable Trading L.L.C is committed to promoting the benefits of market fresh produce and providing members of the public with information about where they Can buy market fresh produce in their locality.
                </p>
</div>
<div class="col-md-3 col-sm-6">
<div class=" thumbnails thumbnail-style thumbnail-kenburn">
<div class="prod-img">
<div class="overflow-hidden">
<img alt="" class="img-responsive" src="uploads/products/1469000881Best-Vegetables-Tomato.jpg"/>
</div>
</div>
<!-- <a class="btn-more hover-effect" href="#">read more +</a>  -->
<div class="caption">
<h3><a class="hover-effect text-center" href="#">Tomato </a></h3>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class=" thumbnails thumbnail-style thumbnail-kenburn">
<div class="prod-img">
<div class="overflow-hidden">
<img alt="" class="img-responsive" src="uploads/products/1469012772sweetpotatoes.jpg"/>
</div>
</div>
<!-- <a class="btn-more hover-effect" href="#">read more +</a>  -->
<div class="caption">
<h3><a class="hover-effect text-center" href="#">Sweet Potato </a></h3>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class=" thumbnails thumbnail-style thumbnail-kenburn">
<div class="prod-img">
<div class="overflow-hidden">
<img alt="" class="img-responsive" src="uploads/products/1469012852616171-green-guava.jpg"/>
</div>
</div>
<!-- <a class="btn-more hover-effect" href="#">read more +</a>  -->
<div class="caption">
<h3><a class="hover-effect text-center" href="#">Guava </a></h3>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class=" thumbnails thumbnail-style thumbnail-kenburn">
<div class="prod-img">
<div class="overflow-hidden">
<img alt="" class="img-responsive" src="uploads/products/1469355772naga.jpg"/>
</div>
</div>
<!-- <a class="btn-more hover-effect" href="#">read more +</a>  -->
<div class="caption">
<h3><a class="hover-effect text-center" href="#">Capsicum </a></h3>
</div>
</div>
</div>
</div>
<!-- End Recent Works -->
</div><!--/container-->
<!-- End Content Part -->
<!--=== Footer Version 1 ===-->
<div class="footer-v1">
<div class="footer">
<div class="container">
<div class="row">
<!-- About -->
<div class="col-md-4 ">
<div class="headline"><h2>Webmail Login</h2></div>
<!-- <a href="index.php">
                            <img id="logo-footer" class="footer-logo" src="assets/img/logo.png" alt=""></a>-->
<ul class="social-icon">
<li><a href=""><img src="assets/img/facebook.png"/></a></li>
<li><a href=""><img src="assets/img/twi_icon.png"/></a></li>
<li><a href=""><img src="assets/img/google-plus-icon.png"/></a></li>
<li><a href=""><img src="assets/img/linkedin.png"/></a></li>
</ul>
<div class="clearfix"></div>
<div class="login-btn">
<a class="btn-u rounded btn-block text-center" href="http://sulthanpurtrading.com:2095/" target="_blank"><i class="fa fa-lock"></i> Webmail Login</a>
</div>
</div><!--/col-md-3-->
<!-- End About -->
<!-- Link List -->
<div class="col-md-4 md-margin-bottom-40">
<div class="headline"><h2>Useful Links</h2></div>
<ul class="list-unstyled link-list">
<li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
<li><a href="about.php">About us</a><i class="fa fa-angle-right"></i></li>
<li><a href="services.php">Services</a><i class="fa fa-angle-right"></i></li>
<li><a href="order.php">Order</a><i class="fa fa-angle-right"></i></li>
<li><a href="contact.php">Contact us</a><i class="fa fa-angle-right"></i></li>
</ul>
</div><!--/col-md-3-->
<!-- End Link List -->
<!-- Address -->
<div class="col-md-4 map-img md-margin-bottom-40">
<div class="headline"><h2>Contact Us</h2></div>
<address class="md-margin-bottom-40">
                            Sutlhanpur Fruits &amp; Vegetable Trading L.L.C Dubai, U.A.E. <br/>
                            
                            PO Box:  294801 <br/>
                            Phone: +971 4 3202792/93 <br/>
                            Fax: +971 4 3202791<br/>
                            Mobile: +971 50 5523270,+971 56 1337799  <br/>
                           
                            Email: <a class="" href="mailto:khorshid@sulthanpurtrading.com">khorshid@sulthanpurtrading.com</a>
</address>
</div><!--/col-md-3-->
<!-- End Address -->
</div>
</div>
</div><!--/footer-->
<div class="copyright">
<div class="container">
<div class="row">
<div class="col-md-6">
<p>
                            2016 © Sultanpur Trading. All Rights Reserved.
                           
                        </p>
</div>
<!-- Social Links -->
<div class="col-md-6">
<!--<ul class="footer-socials list-inline">
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Skype">
                                    <i class="fa fa-skype"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google Plus">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Linkedin">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                           
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                           
                        </ul>-->
</div>
<!-- End Social Links -->
</div>
</div>
</div><!--/copyright-->
</div>
<!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->
<!-- JS Global Compulsory -->
<script src="assets/plugins/jquery/jquery.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-migrate.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- JS Implementing Plugins -->
<script src="assets/plugins/back-to-top.js" type="text/javascript"></script>
<script src="assets/plugins/flexslider/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="assets/plugins/layer-slider/layerslider/js/greensock.js" type="text/javascript"></script>
<script src="assets/plugins/layer-slider/layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
<script src="assets/plugins/layer-slider/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
<!-- JS Customization 
<script type="text/javascript" src="assets/js/custom.js"></script>-->
<!-- JS Page Level -->
<script src="assets/js/app.js" type="text/javascript"></script>
<script src="assets/js/plugins/layer-slider.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        App.initSliders();
        LayerSlider.initLayerSlider();      
    });
</script>
<!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
    <script src="assets/plugins/html5shiv.js"></script>
    <script src="assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->
<!-- For Background Image -->
<script src="assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $.backstretch([
      "assets/img/bg/13..jpg"
      ])
</script>
<!-- End For Background Image -->
</body>
</html> 