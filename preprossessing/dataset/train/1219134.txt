<!DOCTYPE HTML>
<html>
<head>
<title>RedDragonsTeam Hobbi TenyÃ©szet</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="hÃ¼llÅ, kisÃ¡llat, tenyÃ©szet, tenyÃ©sztÅ, hegyvidÃ©ki, sisakos, kamÃ©leon, szakÃ¡llas, agÃ¡ma" name="keywords"/>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<script src="js/jquery.min.js"> </script>
<!--web-fonts-->
<link href="//fonts.googleapis.com/css?family=Lobster|Raleway:500,400,300" rel="stylesheet" type="text/css"/>
<!--//we-bfonts-->
<script src="js/move-top.js" type="text/javascript"></script>
<script src="js/easing.js" type="text/javascript"></script>
<!--/script-->
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>
</head>
<body>
<!--start-header-->
<div class="header" id="home">
<div class="top-header">
<div class="container">
<div class="logo">
<a href="index.html"><img src="images/logo.jpg"/></a>
</div>
<div class="top-menu">
<span class="menu"> </span>
<ul class="cl-effect-16">
<li><a class="active" data-hover="fooldal" href="index.html">FÅoldal</a></li>
<li><a data-hover="TerraplÃ¡za" href="https://terraplaza.org/hu/" target="_blank">TerraplÃ¡za</a></li>
<li><a data-hover="galÃ©ria" href="galeria.html">GalÃ©ria</a></li>
<li><a data-hover="kapcsolat" href="kapcsolat.html">Kapcsolat</a></li>
<div class="clearfix"></div>
</ul>
</div>
<!-- script-for-menu -->
<script>
									$("span.menu").click(function(){
										$(".top-menu ul").slideToggle("slow" , function(){
										});
									});
								</script>
<!-- script-for-menu -->
<div class="clearfix"> </div>
</div>
</div>
<div class="banner">
<div class="container">
<div class="callbacks_container" id="top">
<ul class="rslides callbacks callbacks1" id="slider4">
<li class="callbacks1_on" id="callbacks1_s0" style="display: block; float: left; position: relative; opacity: 1; transition: opacity 500ms ease-in-out;">
<div class="banner-info">
<h3>KÃ¶szÃ¶ntÃ¼nk</h3>
<h4>agÃ¡ma Ã©s kamÃ©leon tenyÃ©szetÃ¼nk oldalÃ¡n!</h4>
</div>
</li>
<li class="" id="callbacks1_s1" style="display: block; float: none; position: absolute; opacity: 0; transition: opacity 500ms ease-in-out;">
<div class="banner-info">
<h3>NÃ©zz be</h3>
<h4>galÃ©riÃ¡nkba is, hogy lÃ¡thasd tenyÃ©szetÃ¼nk bÃ¼szkesÃ©geit!</h4>
</div>
</li>
<li class="" id="callbacks1_s2" style="display: block; float: none; position: absolute; opacity: 0; transition: opacity 500ms ease-in-out;">
<div class="banner-info">
<h3>Terraplaza</h3>
<h4> bÃ¶rzÃ©in aktÃ­van rÃ©szt veszÃ¼nk, talÃ¡lkozzunk szemÃ©lyesen!</h4>
</div>
</li>
</ul>
</div>
<!--banner-Slider-->
<script src="js/responsiveslides.min.js"></script>
<script>
						// You can also use "$(window).load(function() {"
						$(function () {
						  // Slideshow 4
						  $("#slider4").responsiveSlides({
						auto: true,
						pager: true,
						nav:false,
						speed: 500,
						namespace: "callbacks",
						before: function () {
						  $('.events').append("<li>before event fired.</li>");
						},
						after: function () {
						  $('.events').append("<li>after event fired.</li>");
						}
						  });

						});
						  </script>
</div>
<div class="banner-bottom">
<div class="container">
<div class="banner-bot-grids">
<div class="col-md-4 banner-grid one">
<div class="icon">
<i class="glyphicon glyphicon-time"> </i>
</div>
<div class="icon-text">
<h4>SzakÃ©rtelem</h4>
<p>TÃ¶bb Ã©ves tapasztalattal a hÃ¡tunk mÃ¶gÃ¶tt elmondhatjuk, hogy valÃ³ban tudjuk, mit csinÃ¡lunk!</p>
</div>
<div class="clearfix"> </div>
</div>
<div class="col-md-4 banner-grid two">
<div class="icon">
<i class="glyphicon glyphicon-wrench"> </i>
</div>
<div class=" icon-text">
<h4>ElhivatottsÃ¡g</h4>
<p>SzÃ¡munkra ezt tÃ¶bb, mint egy egyszerÅ± hobbi, ami kisÃ¡llatainkon is lÃ¡tszik!</p>
</div>
<div class="clearfix"> </div>
</div>
<div class="col-md-4 banner-grid three">
<div class="icon">
<i class="glyphicon glyphicon-briefcase"> </i>
</div>
<div class="icon-text">
<h4>BiztonsÃ¡g</h4>
<p>Nagy figyelmet fordÃ­tunk kisÃ¡llataink igÃ©nyeinek, Ã­gy a lehetÅ legjobb Ã¡llapotban kerÃ¼lnek Ãºj gazdÃ¡ikhoz.</p>
</div>
<div class="clearfix"> </div>
</div>
</div>
<div class="clearfix"> </div>
</div>
</div>
</div>
</div>
<!--start-about-->
<div class="about">
<div class="container">
<h3 class="tittle wel">TenyÃ©szetÃ¼nkrÅl</h3>
<div class="about-top">
<div class="col-md-7 about-top-right">
<h4>A RedDragonsTeam kisÃ¡llat-tenyÃ©szet kÃ©t kÃ­vÃ¡lÃ³ tenyÃ©sztÅ szaporulataibÃ³l tevÅdik Ã¶ssze.</h4>
<p>TÃ¶bb Ã©ve foglalkozunk kifejezetten hÃ¼llÅk tenyÃ©sztÃ©sÃ©vel, azon belÃ¼l is inkÃ¡bb szakÃ¡llas agÃ¡mÃ¡k Ã©s tÃ¶bbfÃ©le kamÃ©leon (hegyvidÃ©ki, sisakos, pÃ¡rduc Ã©s ezeknek tovÃ¡bbi fajtÃ¡i) szÃ­nesiti populÃ¡ciÃ³nkat.</p>
<p>SzÃ¡munkra ez nem csupÃ¡n egy egyszerÅ± hobbi, szeretjÃ¼k Ã©s Ã©lvezzÃ¼k is a kisÃ¡llatainkkal valÃ³ foglalkozÃ¡st, Ã©s bÃ­zunk benne, hogy a szakszerÅ± tartÃ¡s, Ã¡polÃ¡s Ã©s foglalkozÃ¡s rajtuk is meglÃ¡tszik! </p>
<p>Amennyiben bÃ¡rmilyen terrarisztikÃ¡val Ã©s hÃ¼llÅkkel kapcsolatos kÃ©rdÃ©se van, legyen az beszerzÃ©s, tartÃ¡s, gondozÃ¡s, betegsÃ©g megelÅzÃ©se illetve kezelÃ©se, nyugodtan hÃ­vjon minket, vagy Ã­rjon egy emailt! </p>
</div>
<div class="col-md-5 about-top-left">
<img alt="" class="img-responsive" src="images/chameleon.jpg"/>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<svg height="100" id="bigTriangleShadow" preserveaspectratio="none" version="1.1" viewbox="0 0 100 100" width="100%" xmlns="http://www.w3.org/2000/svg">
<path d="M0 0 L50 100 L100 0 Z" id="trianglePath5"></path>
<path d="M50 100 L100 40 L100 0 Z" id="trianglePath6"></path>
</svg>
<!--start-breed-section-->
<div class="breed-section">
<div class="container">
<h3 class="tittle two">KisÃ¡llatainkrÃ³l</h3>
<div class="serve-grids">
<div class="col-md-5 service-img">
<img alt="" class="img-responsive" src="images/agama.jpg"/>
</div>
<div class="col-md-7 serve-text">
<h4>SzakÃ¡llas agÃ¡ma Ã©s tÃ¶bbfÃ©le kamÃ©leon</h4>
<p>PopulÃ¡ciÃ³nkban tÃ¶bbfÃ©le kamÃ©leon (Jacksonii Willegensis, Kinyongia Multituberculata, Sambava, Tamatave, stb) illetve szakÃ¡llas agÃ¡mÃ¡k is megtalÃ¡lhatÃ³ak!</p>
<div class="col-md-6 service-grid">
<ul>
<li><a href="#"><i class="glyphicon glyphicon-forward"></i></a>SzakÃ¡llas agÃ¡mÃ¡k</li>
<li><a href="#"><i class="glyphicon glyphicon-forward"></i></a>HegyvidÃ©ki kamÃ©leonok</li>
<li><a href="#"><i class="glyphicon glyphicon-forward"></i></a>PÃ¡rduc kamÃ©leonok</li>
<li><a href="#"><i class="glyphicon glyphicon-forward"></i></a>Sisakos kamÃ©leonok</li>
</ul>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<svg height="100" id="bigTriangleShadow" preserveaspectratio="none" version="1.1" viewbox="0 0 100 100" width="100%" xmlns="http://www.w3.org/2000/svg">
<path d="M0 0 L50 100 L100 0 Z" id="trianglePath1"></path>
<path d="M50 100 L100 40 L100 0 Z" id="trianglePath2"></path>
</svg>
<!--//services-->
<div class="health-care">
<div class="container">
<h3 class="tittle">Terrarisztikai bÃ¶rze</h3>
<div class="health">
<div class="col-md-7 health-text">
<h4>AktÃ­van rÃ©szt veszÃ¼nk a TerraPlaza szakmai bÃ¶rzÃ©in!</h4>
<p>Ãvek Ã³ta jÃ¡runk belfÃ¶ldÃ¶n Ã©s kÃ¼lfÃ¶ldÃ¶n egyarÃ¡nt terrarisztikai szakmai kiÃ¡llÃ­tÃ¡sokra Ã©s bÃ¶rzÃ©kre, amikre mindig a legjobb tudÃ¡sunk szerint kÃ©szÃ¼lÃ¼nk Ã©s tenyÃ©szetÃ¼nk Ã¡llatait is felkÃ©szÃ­tjÃ¼k, hogy a legjobb formÃ¡jukat hozhassÃ¡k a kÃ­vÃ¡ncsi szemek elÅtt!</p>
<p>Gyere el Te is a budapesti terrarisztikai bÃ¶rzÃ©kre Ã©s keresd a RedDragonsTeam standjÃ¡t (lÃ³gÃ³rÃ³l felismerni), hogy megismerj minket Ã©s kisÃ¡llatainkat!
						    TovÃ¡bbi informÃ¡ciÃ³k a Terraplaza honlapjÃ¡n (<a href="https://terraplaza.org/hu">https://terraplaza.org/hu</a>) Ã©s <a href="https://www.facebook.com/Reddragonsteamcom-174339502776420/">Facebook oldalunkon!</a></p>
</div>
<div class="col-md-5 health-img">
<img alt="" class="img-responsive" src="images/terra.jpg"/>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<svg height="100" id="bigTriangleShadow" preserveaspectratio="none" version="1.1" viewbox="0 0 100 100" width="100%" xmlns="http://www.w3.org/2000/svg">
<path d="M0 0 L50 100 L100 0 Z" id="trianglePath3"></path>
<path d="M50 100 L100 40 L100 0 Z" id="trianglePath4"></path>
</svg>
<!--//end-health-->
<!--footer-->
<div class="footer text-center">
<div class="container">
<ul class="social-icons">
<p style="font-size: 20px; color: #fff;">KÃ¶vess minket Facebook-on is!</p>
<a href="https://www.facebook.com/Reddragonsteamcom-174339502776420/">
<img src="/images/fb.png"/>
</a>
</ul>
<div class="copy">
<p>Copyright © 2019 RedDragonsTeam. All Rights Reserved.</p>
</div>
</div>
</div>
<!--start-smoth-scrolling-->
<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										$().UItoTop({ easingType: 'easeOutQuart' });
										
									});
								</script>
<a class="scroll" href="#home" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
</body>
</html>
