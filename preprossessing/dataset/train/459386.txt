<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-gb" xml:lang="en-gb" xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="https://www.ecosad.org/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="Consorcio, Salud, Ambiente, Desarrollo " name="keywords"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title></title>
<link href="/templates/mega_extramy/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="https://www.ecosad.org/index.php/en/en/en/en/component/search/?format=opensearch" rel="search" title="Search www.ecosad.org" type="application/opensearchdescription+xml"/>
<link href="/media/mod_vvisit_counter/digit_counter/default.css" rel="stylesheet" type="text/css"/>
<link href="https://www.ecosad.org/modules/mod_megabannerslider/css/layout.css" rel="stylesheet" type="text/css"/>
<script src="/media/system/js/mootools-core.js" type="text/javascript"></script>
<script src="/media/system/js/core.js" type="text/javascript"></script>
<script src="/media/system/js/caption.js" type="text/javascript"></script>
<script src="/media/system/js/mootools-more.js" type="text/javascript"></script>
<script src="https://www.ecosad.org//templates/mega_extramy/js/mega.script.js" type="text/javascript"></script>
<script src="https://www.ecosad.org//templates/mega_extramy/js/mega_menudropdown.js" type="text/javascript"></script>
<script src="https://www.ecosad.org/modules/mod_megabannerslider/js/jquery.js" type="text/javascript"></script>
<script src="https://www.ecosad.org/modules/mod_megabannerslider/js/jquerycycle.js" type="text/javascript"></script>
<script type="text/javascript">
window.addEvent('load', function() {
				new JCaption('img.caption');
			});
  </script>
<link href="/templates/mega_extramy/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="/templates/mega_extramy/css/template.css" rel="stylesheet" type="text/css"/>
<link href="/templates/mega_extramy/css/typography.css" rel="stylesheet" type="text/css"/>
<link href="/templates/mega_extramy/css/customs.css" rel="stylesheet" type="text/css"/>
<link href="/templates/mega_extramy/css/menu.css" rel="stylesheet" type="text/css"/>
<!--[if IE 6]>
	<link href="/templates/mega_extramy/css/ie6.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 7]>
	<link href="/templates/mega_extramy/css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
	<link href="/templates/mega_extramy/css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body id="page">
<div class="mega_wrapper">
<div class="mega_wrapper_i">
<div class="mega_header">
<div class="mega_header_i">
<div class="logo_follow_slide">
<div class="logo_follow_slide_i">
<div class="logobox">
<div class="logobox_r">
<div class="logobox_l">
<div class="logobox_p">
<a class="logo" href="https://www.ecosad.org/"></a>
</div>
</div>
</div>
</div>
<div class="mega_login_follow">
<div class="mega_login_follow_i">
<div class="mega_follow">
<div class="follow_i">
<div class="custom">
<p><strong>{date} </strong></p>
<p> </p></div>
</div>
</div>
</div>
</div>
<div class="mega_slide">
<div class="slide_i">
<div class="megawrapscroller" style="width: 960px">
<div class="megascrollable" style="width:960px; height: 300px">
<div class="items" style="width:960px; height: 300px">
<div class="img" style="width:960px; height: 300px">
<img alt="Sin título_ecosad-3.jpg" src="/images/cabecera/Sin título_ecosad-3.jpg" width="960"/> </div>
<div class="img" style="width:960px; height: 300px">
<img alt="Sin título_ecosad-5.jpg" src="/images/cabecera/Sin título_ecosad-5.jpg" width="960"/> </div>
<div class="img" style="width:960px; height: 300px">
<img alt="Sin título_ecosad-4.jpg" src="/images/cabecera/Sin título_ecosad-4.jpg" width="960"/> </div>
<div class="img" style="width:960px; height: 300px">
<img alt="Sin título_ecosad-2.jpg" src="/images/cabecera/Sin título_ecosad-2.jpg" width="960"/> </div>
<div class="img" style="width:960px; height: 300px">
<img alt="Sin título_ecosad-6.jpg" src="/images/cabecera/Sin título_ecosad-6.jpg" width="960"/> </div>
</div>
</div>
</div>
<script charset="utf-8" type="text/javascript">
<!--
	var j = jQuery.noConflict();
	j(function(){
		 var slideshow = j('.items')
		 .after('<div id="megaslidenav">')
		 .cycle({ 
		     fx:     'fade', 
		     speed:  '300', 
		     timeout: 4000,
		     pager:  '#megaslidenav' 
		 });
		 j('#megaslidenav').prepend('<span id="pauseButton" class="pause">&nbsp;</span>');
		 j('#pauseButton').click(function() {
		     var obj = j(this);
		     if (obj.hasClass('pause')) {
		         obj.removeClass('pause').addClass('play');
		         slideshow.cycle('pause'); 
		     } else if (obj.hasClass('play')) {
		         obj.removeClass('play').addClass('pause');
		         slideshow.cycle('resume');
		     }
		 });
		// add style
		j('#megaslidenav').css({
			'position': 'absolute',
			'top': 280		});
	});
-->
</script>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="menu_search">
<div class="mega_menu">
<div class="mainmenu_left"></div>
<div class="mainmenu_mid">
<div id="mega_menu">
</div>
</div>
<div class="mainmenu_right"></div>
</div>
<div class="mega_search">
<div class="search_i">
<form action="/index.php/en/en/en/en/" method="post">
<div class="search">
<div class="search_i">
<input class="inputbox" id="mod-search-searchword" maxlength="20" name="searchword" onblur="if (this.value=='') this.value='Buscar...';" onfocus="if (this.value=='Buscar...') this.value='';" size="20" type="text" value="Buscar..."/><input class="button" onclick="this.form.searchword.focus();" src="/templates/mega_extramy/images/searchButton.gif" type="image" value="Search"/> <input name="task" type="hidden" value="search"/>
<input name="option" type="hidden" value="com_search"/>
<input name="Itemid" type="hidden" value="491"/>
</div>
</div>
</form>
</div>
</div>
</div>
<div class="mega_main_body">
<div class="mega_main_body_i">
<div class="mega_breadcrumb">
<div class="mega_breadcrumb_i">
<div class="custom">
<table style="width: 487px; height: 25px;">
<tbody>
<tr>
<td><a href="http://ecosad.org:2095/" title="Click"> Correo Institucional</a><strong><a href="laboratorio-virtual/" target="_blank" title="Abrir"></a><br/></strong></td>
</tr>
</tbody>
</table></div>
</div>
</div>
<!-- TOP MODULES START HERE -->
<!-- END -->
<div class="main_content">
<div class="main_content_i">
<div class="message">
<div id="system-message-container">
<dl id="system-message">
<dt class="error">Error</dt>
<dd class="error message">
<ul>
<li>You are not authorised to view this resource.</li>
</ul>
</dd>
</dl>
</div>
</div>
<div class="main_frontpage">
<div class="main_frontpage_i">
<div class="mega_left">
<div class="mega_left_i">
<div class="megaclass_1 module">
<div class="megaclass_2">
<div class="megaclass_3">
<div class="megaclass_4">
<div class="megaclass_i">
<h3><span class="title">Menú principal</span></h3>
<div class="megamodules_i clearfix">
</div>
</div>
</div>
</div>
</div>
</div>
<div class="megaclass_1 module">
<div class="megaclass_2">
<div class="megaclass_3">
<div class="megaclass_4">
<div class="megaclass_i">
<h3><span class="title">Biblioteca Virtual</span></h3>
<div class="megamodules_i clearfix">
</div>
</div>
</div>
</div>
</div>
</div>
<div class="megaclass_1 module">
<div class="megaclass_2">
<div class="megaclass_3">
<div class="megaclass_4">
<div class="megaclass_i">
<h3><span class="title">ENLACES DE INTERÉS</span></h3>
<div class="megamodules_i clearfix">
<div class="custom">
<p style="padding-top: 5px; padding-bottom: 5px; line-height: 1.5em; color: #666666; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-align: center;"><span style="line-height: 1.5em;"><br/><a href="https://www.idrc.ca/en" target="_blank" title="Click"><img alt="" border="0" height="53" src="images/IDRC-canda.jpg" width="198"/></a></span></p></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="megaclass_1 module">
<div class="megaclass_2">
<div class="megaclass_3">
<div class="megaclass_4">
<div class="megaclass_i">
<h3><span class="title">Redes Sociales</span></h3>
<div class="megamodules_i clearfix">
<div class="custom">
<p><a href="https://www.facebook.com/EcosadPeru" target="_blank" title="Click"><img alt="" border="0" height="34" src="images/siguenos-en-facebook.jpg" width="106"/></a></p></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="megaclass_1 module">
<div class="megaclass_2">
<div class="megaclass_3">
<div class="megaclass_4">
<div class="megaclass_i">
<h3><span class="title">Visitas</span></h3>
<div class="megamodules_i clearfix">
<!-- Vinaora Visitors Counter >> http://vinaora.com/ -->
<style type="text/css">
	.vfleft{float:left;}.vfright{float:right;}.vfclear{clear:both;}.valeft{text-align:left;}.varight{text-align:right;}.vacenter{text-align:center;}
	#vvisit_counter108 .vstats_counter{margin-top: 5px;}
	#vvisit_counter108 .vrow{height:24px;}
	#vvisit_counter108 .vstats_icon{margin-right:5px;}
	#vvisit_counter108{padding:5px;}</style>
<div class="vvisit_counter vacenter" id="vvisit_counter108">
<div class="vdigit_counter"><span class="vdigit-3" title="Vinaora Visitors Counter">3</span><span class="vdigit-2" title="Vinaora Visitors Counter">2</span><span class="vdigit-6" title="Vinaora Visitors Counter">6</span><span class="vdigit-0" title="Vinaora Visitors Counter">0</span><span class="vdigit-7" title="Vinaora Visitors Counter">7</span><span class="vdigit-1" title="Vinaora Visitors Counter">1</span></div>
<div style="margin-bottom: 5px;">Your IP: 210.75.253.169</div>
<div>Server Time: 2021-01-12 15:00:58</div>
<div style="margin-top:5px;"><a href="http://vinaora.com/" target="_blank" title="Vinaora Visitors Counter">Visitors Counter</a></div></div>
<!-- Vinaora Visitors Counter >> http://vinaora.com/ --> </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="front_page">
<div class="megaclass_1">
<div class="megaclass_2">
<div class="megaclass_3">
<div class="megaclass_4">
<div class="fp_megaclass_i">
</div>
</div>
</div>
</div>
</div>
</div>
<div class="mega_right">
<div class="mega_right_i">
<div class="megaclass_1 modulemi_fondo">
<div class="megaclass_2">
<div class="megaclass_3">
<div class="megaclass_4">
<div class="megaclass_i">
<h3><span class="title">NOTICIAS RECIENTES</span></h3>
<div class="megamodules_i clearfix">
<div class="custommi_fondo">
<p align="center" class="Pa36" style="text-align: center;"><img border="0" height="120" src="images/afiche%20de%20CComunes.jpg" width="134"/></p>
<p align="center" class="Pa36" style="text-align: justify;"><span style="text-align: justify;">E</span><span style="text-align: justify;">ntre el 14 y 18 de enero de 2019, CComunes organizó en la Universidad Nacional Mayor de San Marcos, la Primera Escuela de Verano en Psicología Social Comunitaria. </span></p>
<p align="center" class="Pa36" style="text-align: justify;"><a href="index.php/the-joomla-project/146-historia-y-memoria-de-la-investigacion-accion-participativa" title="Click">Leer Mas.</a></p></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="megaclass_1 module">
<div class="megaclass_2">
<div class="megaclass_3">
<div class="megaclass_4">
<div class="megaclass_i">
<h3><span class="title">Nuevas Publicaciones</span></h3>
<div class="megamodules_i clearfix">
<div class="custom">
<p style="text-align: center;"><img border="0" height="121" src="images/Tpa%20libro%20metodologas%20participativas%20CLACSO.jpg" style="margin-left: 4px; margin-right: 4px;" width="83"/></p>
<p style="text-align: justify;"> <span style="text-align: justify; font-size: 12.16px;">El Grupo de Trabajo de CLACSO sobre Procesos y Metodología Participativas, del que ECOSAD forma parte, publicó en enero de 2019 el libro </span><strong style="text-align: justify; font-size: 12.16px;"><em>Procesos y metodologías participativas. Reflexiones y experiencias para la transformación social</em></strong><span style="text-align: justify; font-size: 12.16px;">. El Capítulo 2, escrito por Alain Santandreu, lleva por nombre </span><em style="text-align: justify; font-size: 12.16px;">Entre la subversión, la subvención y la tentación de Procusto.        </em><a href="http://biblioteca.clacso.edu.ar/clacso/gt/20190318060039/Procesos_y_metodologias.pdf" style="text-align: center;" target="_blank" title="Click">Descargar</a><span style="text-align: justify; font-size: 12.16px;"> </span></p></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="megaclass_1 modulemi_fondo">
<div class="megaclass_2">
<div class="megaclass_3">
<div class="megaclass_4">
<div class="megaclass_i">
<h3><span class="title">VIDEOS DESTACADOS</span></h3>
<div class="megamodules_i clearfix">
<div class="custommi_fondo">
<p><strong>Sistemas Alimentarios en Tiempos de Crisis</strong></p>
<p><strong>Protegernos de Nosotros Mismos</strong></p>
<p><span>{youtube}EqbdCaSGbXU|200|150|0{/youtube}</span></p>
<p>{youtube}eiJfj3e69Ew|200|150|0{/youtube}</p>
<p>{youtube}zNRqCVTMARw|200|150|0{/youtube}</p>
<p><span> </span></p></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="mega_footer">
<div class="footer_i">
<div class="footer_w">
<div class="footer_p">
<!-- BOTTOM MODULES START HERE -->
<div class="mega_bots" id="mega_bots">
<div class="mega_bots_i" id="mega_bots_i">
<div class="botbox botbox1 firstbox" style="width: 33%;">
<div class="megaclassbox_1 module">
<div class="megaclassbox_2">
<div class="megaclassbox_3">
<div class="megaclassbox_4">
<div class="megaclassbox_i">
<div class="megamodulesbox_i clearfix">
<div class="custom">
<p><a href="http://secasintermitentes.ecosad.org/" target="_blank" title="Click"><img alt="" border="0" height="80" src="images/logosecas.jpg" style="display: block; margin-left: auto; margin-right: auto;" width="213"/></a></p></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="botbox botbox4 midbox" style="width: 33%;">
<div class="megaclassbox_1 module">
<div class="megaclassbox_2">
<div class="megaclassbox_3">
<div class="megaclassbox_4">
<div class="megaclassbox_i">
<div class="megamodulesbox_i clearfix">
<div class="custom">
<p style="text-align: center;"><span style="font-size: 12.16px;"><img alt="" border="0" height="74" src="images/campus.png" width="138"/></span></p></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="botbox botbox5 lastbox" style="width: 33%;">
<div class="megaclassbox_1 module">
<div class="megaclassbox_2">
<div class="megaclassbox_3">
<div class="megaclassbox_4">
<div class="megaclassbox_i">
<div class="megamodulesbox_i clearfix">
<div class="custom">
<p><a href="laboratorio-virtual/" target="_blank" title="Click"><img alt="" border="0" height="65" src="images/logo_GC1.jpg" style="display: block; margin-left: auto; margin-right: auto;" width="197"/></a></p></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- END -->
</div>
</div>
</div>
</div>
<div class="bottom_menu_copyright">
<div class="bottom_menu_copyright_i">
<div class="bottom_menu">
<div class="bottom_menu_i">
</div>
</div>
<div class="copyright">
<div class="copyright_i">
<div class="custom">
<p><span style="line-height: 1.3em;">Consorcio por la Salud Ambiente y Desarrollo ECOSAD | Jr. Saco Oliveros Nro. 295, Oficina 501, Santa Beatriz, Lima, Perú. Telefono: 3011437 </span></p></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div style="clear: both;"></div>
</body>
</html>