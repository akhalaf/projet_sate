<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<link href="https://safetticr.com/wp-content/uploads/2020/03/cropped-Safetti-logo-32x32.png" rel="icon"/>
<link href="https://safetticr.com/wp-content/maintenance/assets/styles.css" rel="stylesheet"/>
<script src="https://safetticr.com/wp-content/maintenance/assets/timer.js"></script>
<title>Mantenimiento programado</title>
</head>
<body>
<div class="container">
<header class="header">
<h1>El sitio web se encuentra en mantenimiento.</h1>
<h2>Lo sentimos por los problemas causados. ¡En breve volveremos a estar operativos!</h2>
</header>
<!--START_TIMER_BLOCK-->
<!--END_TIMER_BLOCK-->
<!--START_SOCIAL_LINKS_BLOCK-->
<section class="social-links">
<a class="social-links__link" href="https://www.facebook.com/Plesk" target="_blank" title="Facebook">
<span class="icon"><img alt="Facebook" src="https://safetticr.com/wp-content/maintenance/assets/images/facebook.svg"/></span>
</a>
<a class="social-links__link" href="https://twitter.com/Plesk" target="_blank" title="Twitter">
<span class="icon"><img alt="Twitter" src="https://safetticr.com/wp-content/maintenance/assets/images/twitter.svg"/></span>
</a>
</section>
<!--END_SOCIAL_LINKS_BLOCK-->
</div>
<footer class="footer">
<div class="footer__content">
        Con la tecnología del paquete de herramientas de WordPress <a href="https://www.plesk.com/" target="_blank"><img alt="Plesk" class="logo" src="https://safetticr.com/wp-content/maintenance/assets/images/plesk-logo.png"/></a>
</div>
</footer>
</body>
</html>
