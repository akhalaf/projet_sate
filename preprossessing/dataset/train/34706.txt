<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="noarchive" name="robots"/>
<title>编程网_菜鸟学编程_编程培训_PHP培训_PHP程序员教程</title>
<meta content="菜鸟教程,学编程,编程培训,PHP教程,PHP培训,PHP程序员,PHP工程师" name="keywords"/><meta content="与其他教程网站不同，5ibc.net只服务于广大PHP程序员，从编程菜鸟开始、从最简单的HTML入门，一步步学习PHP网站设计的全部课程，后续本站会推出更高级的经验教程。" name="description"/> <link href="https://cdn.5ibc.net" rel="dns-prefetch"/>
<link href="https://cdn.5ibc.net/public/img/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://cdn.5ibc.net/www/css/style.css?v=2016052101" rel="stylesheet" type="text/css"/>
</head>
<body>
<!--顶部-->
<div id="header">
<div class="top">
<a class="logo" href="https://www.5ibc.net">www.5ibc.net</a>
<div class="menu">
<a class="menu-index menu-active" href="https://www.5ibc.net">首页</a>
<a href="https://www.5ibc.net/course.html">教程</a>
<a href="https://you.5ibc.net" target="_blank">优惠券</a>
<a href="https://www.5ibc.net/book.html" target="_blank">图书</a>
<a href="https://man.5ibc.net" target="_blank">命令大全</a>
</div>
</div>
</div>
<!--主体-->
<div id="wrap">
<div class="container">
<div class="wrap-lf">
<h3 class="wrap-tit">
<span class="icon icon-top title-icon"></span>
<span>教程推荐</span>
</h3>
<h4 class="course-tit"><a href="https://www.5ibc.net/item/php-base-tutorial.html">PHP入门教程</a></h4><div class="course-ls"><a href="https://www.5ibc.net/view/php-intro.html">PHP简介</a><a href="https://www.5ibc.net/view/php-environment.html">PHP环境搭建</a><a href="https://www.5ibc.net/view/php-windows-install.html">Windows PHP环境搭建</a><a href="https://www.5ibc.net/view/php-linux-install.html">Linux PHP环境搭建</a><a href="https://www.5ibc.net/view/php-syntax.html">PHP语法</a><a href="https://www.5ibc.net/view/php-constants.html">PHP常量</a><a href="https://www.5ibc.net/view/php-variables.html">PHP变量</a><a href="https://www.5ibc.net/view/php-global-variable.html">PHP超全局变量</a><a href="https://www.5ibc.net/view/php-magic-variable.html">PHP魔术变量</a><a href="https://www.5ibc.net/view/php-echo-print.html">PHP输出</a><a href="https://www.5ibc.net/view/php-datatype.html">PHP数据类型</a><a href="https://www.5ibc.net/view/php-string.html">PHP字符串</a><a href="https://www.5ibc.net/view/php-array.html">PHP数组</a><a href="https://www.5ibc.net/view/php-function.html">PHP函数</a><a href="https://www.5ibc.net/view/php-operators.html">PHP运算符</a><a href="https://www.5ibc.net/view/php-if-else.html">PHP if/else</a><a href="https://www.5ibc.net/view/php-switch.html">PHP switch</a><a href="https://www.5ibc.net/view/php-while-break-continue.html">PHP while循环</a><a href="https://www.5ibc.net/view/php-for.html">PHP for循环</a><a href="https://www.5ibc.net/view/php-foreach.html">PHP foreach循环</a></div><h4 class="course-tit"><a href="https://www.5ibc.net/item/php-advanced-tutorial.html">PHP高级教程</a></h4><div class="course-ls"><a href="https://www.5ibc.net/view/php-arrays.html">PHP多维数组</a><a href="https://www.5ibc.net/view/php-time-date.html">PHP时间日期</a><a href="https://www.5ibc.net/view/php-include-require.html">PHP包含</a><a href="https://www.5ibc.net/view/php-file.html">PHP文件操作</a><a href="https://www.5ibc.net/view/php-file-upload.html">PHP文件上传</a><a href="https://www.5ibc.net/view/php-cookie.html">PHP cookie</a><a href="https://www.5ibc.net/view/php-session.html">PHP session</a><a href="https://www.5ibc.net/view/php-mail.html">PHP email</a><a href="https://www.5ibc.net/view/php-secure-mail.html">PHP安全email</a><a href="https://www.5ibc.net/view/php-error-log.html">PHP错误日志</a><a href="https://www.5ibc.net/view/php-exception.html">PHP异常处理</a><a href="https://www.5ibc.net/view/php-namespace.html">PHP命名空间</a></div> </div>
<div class="wrap-rt">
<div class="rt-tit">
<h5>教程列表<span><i class="tip-outer"></i><i class="tip-inner"></i></span></h5>
</div>
<div class="course-box">
<dl><dd><a href="https://www.5ibc.net/list/php-tutorial.html"><img alt="PHP" src="https://cdn.5ibc.net/www/img/course/php.png"/><span class="box-tit">PHP</span><span>开源高效跨平台脚本语言</span></a></dd><dd><a href="https://www.5ibc.net/list/html5.html"><img alt="HTML5" src="https://cdn.5ibc.net/www/img/course/h5.png"/><span class="box-tit">HTML5</span><span>HTML5是下一代HTML标准</span></a></dd><dd class="edge-dd"><a href="https://www.5ibc.net/list/css3.html"><img alt="CSS3" src="https://cdn.5ibc.net/www/img/course/css3.png"/><span class="box-tit">CSS3</span><span>升级版CSS层叠样式表</span></a></dd><dd><a href="https://www.5ibc.net/list/bootstrap.html"><img alt="Bootstrap" src="https://cdn.5ibc.net/www/img/course/bootstrap.png"/><span class="box-tit">Bootstrap</span><span>目前最受欢迎的前端框架</span></a></dd><dd><a href="https://www.5ibc.net/list/ajax.html"><img alt="Ajax" src="https://cdn.5ibc.net/www/img/course/ajax.jpg"/><span class="box-tit">Ajax</span><span>实现前后端异步数据交换</span></a></dd><dd class="edge-dd"><a href="https://www.5ibc.net/list/json.html"><img alt="Json" src="https://cdn.5ibc.net/www/img/course/json.png"/><span class="box-tit">Json</span><span>轻量级数据交换格式</span></a></dd><dd><a href="https://www.5ibc.net/list/mysql.html"><img alt="MySQL" src="https://cdn.5ibc.net/www/img/course/mysql.png"/><span class="box-tit">MySQL</span><span>目前最好最流行的RDBMS</span></a></dd><dd><a href="https://www.5ibc.net/list/redis.html"><img alt="Redis" src="https://cdn.5ibc.net/www/img/course/redis.png"/><span class="box-tit">Redis</span><span>高性能NoSQL数据存储</span></a></dd><dd class="edge-dd"><a href="https://www.5ibc.net/list/memcached.html"><img alt="Memcached" src="https://cdn.5ibc.net/www/img/course/memcached.png"/><span class="box-tit">Memcached</span><span>高性能分布式内存对象缓存</span></a></dd><dd><a href="https://www.5ibc.net/list/linux.html"><img alt="Linux" src="https://cdn.5ibc.net/www/img/course/linux.png"/><span class="box-tit">Linux</span><span>性能稳定多用户操作系统</span></a></dd><dd><a href="https://man.5ibc.net" target="_blank"><img alt="Linux命令大全" src="https://cdn.5ibc.net/www/img/course/linux.png"/><span class="box-tit">Linux命令大全</span><span>最专业的Linux命令大全</span></a></dd></dl> </div>
<div class="rt-tit">
<h5>学习路线<span><i class="tip-outer"></i><i class="tip-inner"></i></span></h5>
</div>
<div class="learn-direct">
                HTML5
                <span class="icon"></span>CSS3
                <span class="icon"></span>JavaScript
                <span class="icon"></span>jQuery
                <span class="icon"></span>PHP
                <span class="icon"></span>MySQL
                <span class="icon"></span>Nginx
            </div>
<div class="rt-tit">
<h5>实用工具<span><i class="tip-outer"></i><i class="tip-inner"></i></span></h5>
</div>
<div class="course-box">
<dl><dd><a href="http://md5.5ibc.net"><img alt="MD5在线加密" src="https://cdn.5ibc.net/www/md5/md5.png"/><span class="box-tit">MD5在线加密</span><span>在线MD5加密工具</span></a></dd><dd><a href="http://json.5ibc.net"><img alt="Json在线解析" src="https://cdn.5ibc.net/www/json/json.png"/><span class="box-tit">Json在线解析</span><span>简单易用的Json解析工具</span></a></dd><dd class="edge-dd"><a href="http://urlencode.5ibc.net"><img alt="URL解码/编码" src="https://cdn.5ibc.net/www/urlencode/url.png"/><span class="box-tit">URL解码/编码</span><span>在线中文编码转换工具</span></a></dd><dd><a href="http://unixtime.5ibc.net"><img alt="Unix时间戳" src="https://cdn.5ibc.net/www/unixtime/time.png"/><span class="box-tit">Unix时间戳</span><span>在线时间戳转换工具</span></a></dd><dd><a href="http://rgbcolor.5ibc.net"><img alt="RGB颜色代码" src="https://cdn.5ibc.net/www/rgbcolor/ui.png"/><span class="box-tit">RGB颜色代码</span><span>在线RGB颜色选择器</span></a></dd><dd class="edge-dd"><a href="http://jscompress.5ibc.net"><img alt="JS格式化/压缩" src="https://cdn.5ibc.net/www/json/json.png"/><span class="box-tit">JS格式化/压缩</span><span>在线JS格式化与压缩工具</span></a></dd><dd><a href="http://wordcounter.5ibc.net"><img alt="字数统计" src="https://cdn.5ibc.net/www/wordcounter/word.png"/><span class="box-tit">字数统计</span><span>在线字数统计工具</span></a></dd></dl> </div>
<div class="rt-tit">
<h5>网站愿景<span><i class="tip-outer"></i><i class="tip-inner"></i></span></h5>
</div>
<div class="rt-txt">
<p>5ibc.net旨在服务于广大PHP爱好者，按照本站的学习路线，快速便捷、高效、稳步学习PHP。</p>
<p>5ibc.net站长8年PHP实战开发经验，走过许多弯路，也积累了不少经验，在本站都将分享给大家。</p>
<p>网站前期会整理一些互联网基础教程，后期会分享站长的项目框架、服务器架构、性能优化等方面经验。</p>
<p>学习路线里的教程会陆陆续续跟大家见面，可先收藏本站时常光顾。</p>
</div>
<div class="rt-tit">
<h5>Linux命令大全<span><i class="tip-outer"></i><i class="tip-inner"></i></span></h5>
</div>
<div class="rt-txt">
<ul><li><a href="https://man.5ibc.net/vmstat" target="_blank" title="vmstat命令：显示虚拟内存状态">vmstat命令：显示虚拟内存状态</a></li><li><a href="https://man.5ibc.net/volname" target="_blank" title="volname命令：显示指定的ISO-9660格式的设备的卷名称">volname命令：显示指定的ISO-9660格式</a></li><li><a href="https://man.5ibc.net/w" target="_blank" title="w命令：显示目前登入系统的用户信息">w命令：显示目前登入系统的用户信息</a></li><li><a href="https://man.5ibc.net/wait" target="_blank" title="wait命令：等待进程执行完后返回">wait命令：等待进程执行完后返回</a></li><li><a href="https://man.5ibc.net/wall" target="_blank" title="wall命令：向系统当前所有打开的终端上输出信息">wall命令：向系统当前所有打开的终端上输出信息</a></li><li><a href="https://man.5ibc.net/watch" target="_blank" title="watch命令：周期性的方式执行给定的指令">watch命令：周期性的方式执行给定的指令</a></li><li><a href="https://man.5ibc.net/wc" target="_blank" title="wc命令：统计文件的字节数、字数、行数">wc命令：统计文件的字节数、字数、行数</a></li><li><a href="https://man.5ibc.net/wget" target="_blank" title="wget命令：Linux系统下载文件工具">wget命令：Linux系统下载文件工具</a></li><li><a href="https://man.5ibc.net/whatis" target="_blank" title="whatis命令：查询一个命令执行什么功能">whatis命令：查询一个命令执行什么功能</a></li><li><a href="https://man.5ibc.net/whereis" target="_blank" title="whereis命令：查找二进制程序、代码等相关文件路径">whereis命令：查找二进制程序、代码等相关文件</a></li><li><a href="https://man.5ibc.net/which" target="_blank" title="which命令：查找并显示给定命令的绝对路径">which命令：查找并显示给定命令的绝对路径</a></li><li><a href="https://man.5ibc.net/who" target="_blank" title="who命令：显示目前登录系统的用户信息">who命令：显示目前登录系统的用户信息</a></li><li><a href="https://man.5ibc.net/whoami" target="_blank" title="whoami命令：打印当前有效的用户名称">whoami命令：打印当前有效的用户名称</a></li><li><a href="https://man.5ibc.net/write" target="_blank" title="write命令：向指定登录用户终端上发送信息">write命令：向指定登录用户终端上发送信息</a></li><li><a href="https://man.5ibc.net/xargs" target="_blank" title="xargs命令：给其他命令传递参数的一个过滤器">xargs命令：给其他命令传递参数的一个过滤器</a></li><li><a href="https://man.5ibc.net/xauth" target="_blank" title="xauth命令：显示和编辑被用于连接X服务器的认证信息">xauth命令：显示和编辑被用于连接X服务器的认证</a></li><li><a href="https://man.5ibc.net/xclip" target="_blank" title="xclip命令：管理 X 粘贴板">xclip命令：管理 X 粘贴板</a></li><li><a href="https://man.5ibc.net/xhost" target="_blank" title="xhost命令：制哪些X客户端能够在X服务器上显示">xhost命令：制哪些X客户端能够在X服务器上显示</a></li><li><a href="https://man.5ibc.net/xinit" target="_blank" title="xinit命令：是Linux下X-Window系统的初始化程序">xinit命令：是Linux下X-Window系统</a></li><li><a href="https://man.5ibc.net/xlsatoms" target="_blank" title="xlsatoms命令：列出X服务器内部所有定义的原子成分">xlsatoms命令：列出X服务器内部所有定义的原</a></li></ul> </div>
<div class="rt-tit">
<h5>CentOS专题<span><i class="tip-outer"></i><i class="tip-inner"></i></span></h5>
</div>
<div class="rt-txt">
<ul><li><a href="https://www.5ibc.net/centos/3244.html" target="_blank" title="CentOS6.x下安装谷歌浏览器">CentOS6.x下安装谷歌浏览器</a></li><li><a href="https://www.5ibc.net/centos/3245.html" target="_blank" title="centos6.x下安装eclipse">centos6.x下安装eclipse</a></li><li><a href="https://www.5ibc.net/centos/3246.html" target="_blank" title="centos6.x下安装maven">centos6.x下安装maven</a></li><li><a href="https://www.5ibc.net/centos/3247.html" target="_blank" title="CentOS6.5下安装php5.3">CentOS6.5下安装php5.3</a></li><li><a href="https://www.5ibc.net/centos/3248.html" target="_blank" title="CentOS下安装mysql5.5">CentOS下安装mysql5.5</a></li><li><a href="https://www.5ibc.net/centos/3249.html" target="_blank" title="CentOS6.5下安装apache2.2">CentOS6.5下安装apache2.2</a></li><li><a href="https://www.5ibc.net/centos/3250.html" target="_blank" title="CentOS6.5安装VNC">CentOS6.5安装VNC</a></li><li><a href="https://www.5ibc.net/centos/3251.html" target="_blank" title="CentOS挂载U盘">CentOS挂载U盘</a></li><li><a href="https://www.5ibc.net/centos/3252.html" target="_blank" title="CentOS下升级最新版Nginx">CentOS下升级最新版Nginx</a></li><li><a href="https://www.5ibc.net/centos/3253.html" target="_blank" title=" Centos 自动备份mysql"> Centos 自动备份mysql</a></li><li><a href="https://www.5ibc.net/centos/3254.html" target="_blank" title="CentOS6.5安装Chromium谷歌浏览器">CentOS6.5安装Chromium谷歌浏览器</a></li><li><a href="https://www.5ibc.net/centos/3255.html" target="_blank" title="CentOS下为php环境添加mysql扩展">CentOS下为php环境添加mysql扩展</a></li><li><a href="https://www.5ibc.net/centos/3256.html" target="_blank" title="CentOS下卸载MySQL的二种方法">CentOS下卸载MySQL的二种方法</a></li><li><a href="https://www.5ibc.net/centos/3257.html" target="_blank" title="CentOS下使用rdesktop访问Windows远程桌面">CentOS下使用rdesktop访问Window</a></li><li><a href="https://www.5ibc.net/centos/3258.html" target="_blank" title="CentOS6.5下安装字体">CentOS6.5下安装字体</a></li><li><a href="https://www.5ibc.net/centos/3259.html" target="_blank" title="U盘安装CentOS">U盘安装CentOS</a></li><li><a href="https://www.5ibc.net/centos/3260.html" target="_blank" title="CentOS下对httpd四之CGI、HTTPS、压缩配置">CentOS下对httpd四之CGI、HTTPS、</a></li><li><a href="https://www.5ibc.net/centos/3261.html" target="_blank" title="CentOS下改变ftp匿名用户的下载目录">CentOS下改变ftp匿名用户的下载目录</a></li><li><a href="https://www.5ibc.net/centos/3262.html" target="_blank" title="Win7安装CentOS 6.4双系统过程">Win7安装CentOS 6.4双系统过程</a></li><li><a href="https://www.5ibc.net/centos/3263.html" target="_blank" title="前Centos6.5安装Nvidia显卡驱动教程">前Centos6.5安装Nvidia显卡驱动教程</a></li></ul> </div>
<div class="rt-tit">
<h5>推荐书单<span><i class="tip-outer"></i><i class="tip-inner"></i></span></h5>
</div>
<div class="rt-txt">
<ul><li><a href="https://www.shubei.net/isbn/9787508536323" target="_blank" title="最美古诗词系列手账本-杜甫：落花时节又逢君">最美古诗词系列手账本-杜甫：落花时节又逢君</a></li><li><a href="https://www.shubei.net/isbn/9787312040832" target="_blank" title="大学生数学竞赛题选解">大学生数学竞赛题选解</a></li><li><a href="https://www.shubei.net/isbn/9787117226912" target="_blank" title="临床药物治疗学·儿科疾病(培训教材)">临床药物治疗学·儿科疾病(培训教材)</a></li><li><a href="https://www.shubei.net/isbn/9787302249078" target="_blank" title="工程材料（第5版）">工程材料（第5版）</a></li><li><a href="https://www.shubei.net/isbn/9787515511252" target="_blank" title="卡耐基写给女人的魅力口才与人际关系">卡耐基写给女人的魅力口才与人际关系</a></li><li><a href="https://www.shubei.net/isbn/12471740" target="_blank" title="（四级英语乱序词汇书+真题试卷）2018年12月星火大学四级全真">（四级英语乱序词汇书+真题试卷）2018年1</a></li><li><a href="https://www.shubei.net/isbn/9787115331410" target="_blank" title="从零开始：Photoshop CS6中文版基础培训教程">从零开始：Photoshop CS6中文版基</a></li><li><a href="https://www.shubei.net/isbn/9787532775859" target="_blank" title="文学名著·译文经典：西西弗神话：散论荒诞（精装）">文学名著·译文经典：西西弗神话：散论荒诞（精</a></li><li><a href="https://www.shubei.net/isbn/9787511505262" target="_blank" title="中公教育2020湖南省公务员录用考试教材：全真模拟预测试卷行政职">中公教育2020湖南省公务员录用考试教材：全</a></li><li><a href="https://www.shubei.net/isbn/9787543229136" target="_blank" title="中国银行业改革与发展：回顾、总结与展望">中国银行业改革与发展：回顾、总结与展望</a></li><li><a href="https://www.shubei.net/isbn/9787541144721" target="_blank" title="鲁滨孙漂流记（全译本）">鲁滨孙漂流记（全译本）</a></li><li><a href="https://www.shubei.net/isbn/9787518046485" target="_blank" title="凡尔纳科幻经典（格兰特船长的儿女 蓓根的五亿法郎 机器岛 冰雪冬">凡尔纳科幻经典（格兰特船长的儿女 蓓根的五亿</a></li><li><a href="https://www.shubei.net/isbn/9787565431951" target="_blank" title="中级财务会计（第6版）/东北财经大学会计学系列教材，“十二五”普">中级财务会计（第6版）/东北财经大学会计学系</a></li><li><a href="https://www.shubei.net/isbn/9787205085810" target="_blank" title="此生若能牵手，谁愿颠沛流离">此生若能牵手，谁愿颠沛流离</a></li><li><a href="https://www.shubei.net/isbn/9787115183392" target="_blank" title="HTML+CSS网页设计与布局从入门到精通（附CD光盘1张）">HTML+CSS网页设计与布局从入门到精通（</a></li><li><a href="https://www.shubei.net/isbn/9787550028296" target="_blank" title="帝凰.2">帝凰.2</a></li><li><a href="https://www.shubei.net/isbn/9787540472313" target="_blank" title="股票作手操盘术：全译版">股票作手操盘术：全译版</a></li><li><a href="https://www.shubei.net/isbn/9787521300383" target="_blank" title="国际人才英语教程(中级)">国际人才英语教程(中级)</a></li><li><a href="https://www.shubei.net/isbn/9787508695051" target="_blank" title="相信是成功的一半：战胜疑虑、释放潜能的自我精进法则">相信是成功的一半：战胜疑虑、释放潜能的自我精</a></li><li><a href="https://www.shubei.net/isbn/9787511139047" target="_blank" title="环评工程师考试教材2019 环境影响评价技术导则与标准（2019">环评工程师考试教材2019 环境影响评价技术</a></li><li><a href="https://www.shubei.net/isbn/9787020150304" target="_blank" title="银河边缘·天象祭司">银河边缘·天象祭司</a></li><li><a href="https://www.shubei.net/isbn/9787504682208" target="_blank" title="主管护师2019原军医版 护师资格证考试 卫生资格考试 中科小红">主管护师2019原军医版 护师资格证考试 卫</a></li><li><a href="https://www.shubei.net/isbn/12510349" target="_blank" title="一级建造师2019教材一建教材市政配套真题试卷市政公用工程管理与">一级建造师2019教材一建教材市政配套真题试</a></li><li><a href="https://www.shubei.net/isbn/11721534" target="_blank" title="人生掌控力系列：戒了吧，拖延症+人生赢在零逃避+别让情绪毁了你（">人生掌控力系列：戒了吧，拖延症+人生赢在零逃</a></li><li><a href="https://www.shubei.net/isbn/9787513326216" target="_blank" title="住在我心里的小怪兽">住在我心里的小怪兽</a></li><li><a href="https://www.shubei.net/isbn/9787115375124" target="_blank" title="Java从入门到精通（第2版）">Java从入门到精通（第2版）</a></li><li><a href="https://www.shubei.net/isbn/9787205082796" target="_blank" title="最经典英语文库：方法论（英文版）">最经典英语文库：方法论（英文版）</a></li><li><a href="https://www.shubei.net/isbn/9787534049354" target="_blank" title="爱德少儿幼小衔接天天练 数学 3">爱德少儿幼小衔接天天练 数学 3</a></li></ul> </div>
<div class="rt-tit">
<h5>友情链接<span><i class="tip-outer"></i><i class="tip-inner"></i></span></h5>
</div>
<div class="rt-txt rt-flink">
<a href="https://www.shubei.net" target="_blank">书贝网</a>
<a href="https://you.5ibc.net" target="_blank">淘宝优惠券</a>
<a href="http://www.zixuephp.net" target="_blank">PHP自学网</a>
<a href="https://man.5ibc.net" target="_blank">Linux命令大全</a>
<a href="https://www.51dev.com" target="_blank">IT技术网</a>
<a href="http://www.cjjjs.com" target="_blank">C++技术网</a>
<a href="http://www.365mini.com" target="_blank">CodePlayer</a>
<a href="http://www.jsons.cn" target="_blank">JSON解析</a>
<a href="http://www.v5pc.com" target="_blank">免费实用绿色软件</a>
<a href="https://www.appcgn.com" target="_blank">软件缘</a>
<a href="http://laravelacademy.org" target="_blank">Laravel学院</a>
<a href="http://bestcbooks.com" target="_blank">计算机书籍控</a>
<a href="http://www.lanecn.com" target="_blank">PHP博客</a>
<a href="https://www.leixuesong.com" target="_blank">PHP程序员技术博客</a>
<a href="http://bubuko.com" target="_blank">布布扣</a>
<a href="https://www.daokao.net" target="_blank">导考网</a>
</div>
</div>
</div>
</div>
<!--底部-->
<div id="footer">
    Copyright © 2021  <strong><a href="https://www.5ibc.net">菜鸟教程</a></strong>  5ibc.net  <a href="http://www.beian.miit.gov.cn" rel="external nofollow" target="_blank">湘ICP备19003306号-1</a>  广告合作：<span class="my-email">ten.cbi5@da</span>  联系客服：<span class="my-email">ten.cbi5@ufek</span>
</div>
<a href="javascript:scroll(0,0)" id="back-top" rel="nofollow" title="返回顶部"></a>
<!-- 微信公众号 Start -->
<style type="text/css">
    .wechat{position:fixed;left:10px;top:140px;padding:16px;font-size:12px;font-weight:bold;background-color:#ec3323;border-radius:6px;box-shadow:0px 2px 13px 0px rgba(0, 0, 0, 0.49), inset -2.828px 2.828px 0px 0px rgba(174, 21, 8, 0.38)}
    .wechat img{width:102px;height:102px}
    .wechat p{text-align:center;margin:5px 0 0;color:#fff}
    .wechat a{font-size:14px;color:#fff;text-decoration:underline}
</style>
<div class="wechat">
<img src="https://cdn.5ibc.net/public/img/yhq.jpg"/>
<p>关注公众号</p>
<p>领淘宝内部优惠券</p>
<p>
<a href="https://you.5ibc.net" target="_blank">精选好券推荐</a>
</p>
</div>
<!-- 微信公众号 End -->
<!--底部JS-->
<div style="display:none;">
<script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "//hm.baidu.com/hm.js?a11c9a8949d61f9dde2b12b8aa3340c3";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
</div>
</body>
</html>
<!-- Generated by 5ibc.net on 2021-01-14 -->