<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<script type="text/javascript">
        var const_country = "--";
    </script>
<link href="/themes/form/css/960_12_10_10.css" rel="stylesheet" type="text/css"/>
<link href="/themes/form/css/jquery-ui-1.9.0.dialog.min.css" rel="stylesheet" type="text/css"/>
<link href="/themes/form/css/main.css" rel="stylesheet" type="text/css"/>
<script src="/assets/ef2d33f0/md5.js" type="text/javascript"></script>
<script src="/assets/8cfd06ca/swfobject.js" type="text/javascript"></script>
<script src="/assets/495a3844/swfstore.js" type="text/javascript"></script>
<script src="/assets/288d5984/storage-origin.js" type="text/javascript"></script>
<script src="/assets/ef2d33f0/fingerprint.js" type="text/javascript"></script>
<script src="/assets/8cfd06ca/plugin_detect.js" type="text/javascript"></script>
<script src="/themes/form/js/jquery.min.js" type="text/javascript"></script>
<script src="/themes/form/js/jquery-ui-1.9.0.dialog.min.js" type="text/javascript"></script>
<script src="/themes/form/js/main.js" type="text/javascript"></script>
<script src="/themes/form/js/bp3.js" type="text/javascript"></script>
</head>
<body><script type="text/javascript">
/*<![CDATA[*/
var def_country_code="CN";

                var ANTIFROD_PATH='https://www.work-form.com/fingerprint/callback/index';
                Store = new Storage({
                    debug: false,
                    namespace: 'dp',
                    swf_url: 'https://www.work-form.com/storage.swf',
                    sl_url: 'https://www.work-form.com/storage.xap',
                    onready:function(){
                         if (typeof doLoad == 'function') { doLoad(); }  if(Store.get('form_filled')==1) { $('#reg input, #reg select').attr('disabled', 'disabled'); }
                    },
                    onerror:function(){
                         if (typeof doLoad == 'function') { doLoad(); } 
                    }
                });
            
/*]]>*/
</script>
<form action="/" id="reg" method="POST" name="reg"> <table border="0" cellpadding="5" cellspacing="0" width="290">
<tr>
<td>
<label for="LeadForm_fname">First name</label> <input class="formColor" id="LeadForm_fname" name="LeadForm[fname]" style="width: 264px" type="text"/> </td>
</tr>
<tr>
<td>
<label for="LeadForm_lname">Last name</label> <input class="formColor" id="LeadForm_lname" name="LeadForm[lname]" style="width: 264px" type="text"/> </td>
</tr>
<tr>
<td>
<label for="LeadForm_age">Age</label> <input class="formColor" id="LeadForm_age" name="LeadForm[age]" style="width: 264px" type="text"/> </td>
</tr>
<tr>
<td>
<label for="LeadForm_country">Country</label> <select class="drop" id="LeadForm_country" name="LeadForm[country]" style="width: 270px">
</select> </td>
</tr>
<tr id="state">
<td>
<label for="LeadForm_state">State</label> <select class="drop" id="LeadForm_state" name="LeadForm[state]" style="width: 270px">
</select> </td>
</tr>
<tr>
<td>
<label for="LeadForm_city">City</label> <input class="formColor" id="LeadForm_city" name="LeadForm[city]" style="width: 264px" type="text"/> </td>
</tr>
<tr>
<td>
<label for="LeadForm_phone">Phone number</label> <input class="formColor" id="LeadForm_phone" name="LeadForm[phone]" style="width: 264px" type="text"/> </td>
</tr>
<tr>
<td>
<label for="LeadForm_calltime">Best time to be reached</label> <select class="formColor" id="LeadForm_calltime" name="LeadForm[calltime]" style="width: 270px">
<option value="0">Any</option>
<option value="1">Morning (9 a.m.- 12 a.m.)</option>
<option value="2">Day (12 a.m. - 5 p.m)</option>
<option value="3">Evening (after 5 p.m.)</option>
</select> </td>
</tr>
<tr>
<td>
<label for="LeadForm_email">E-mail address</label> <input class="formColor" id="LeadForm_email" name="LeadForm[email]" style="width: 264px" type="text"/> </td>
</tr>
<tr>
<td>
<label for="LeadForm_emailconfirm">Confirm E-mail</label> <input class="formColor" id="LeadForm_emailconfirm" name="LeadForm[emailconfirm]" style="width: 264px" type="text"/> </td>
</tr>
<tr>
<td colspan="2"><div id="right2">
<div id="signup2" style="text-align: center;"> <br/>
<input class="signupfree" id="button" name="yt0" onclick="submitClick();" style="height:20px;" type="button" value="SEND"/> </div>
</div></td>
</tr>
</table>
</form>
<div id="dialog-message" title="Registration">
<p id="alert-msg"></p>
</div>
</body>
</html>
