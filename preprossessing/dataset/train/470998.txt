<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<title>Page not found - Eliminacion Biopolímeros</title>
<!-- This site is optimized with the Yoast SEO plugin v12.7.1 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex,follow" name="robots"/>
<meta content="en_US" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Page not found - Eliminacion Biopolímeros" property="og:title"/>
<meta content="Eliminacion Biopolímeros" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Page not found - Eliminacion Biopolímeros" name="twitter:title"/>
<script class="yoast-schema-graph yoast-schema-graph--main" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://eliminacionbiopolimeros.com/#organization","name":"Eliminacion Biopolimeros","url":"https://eliminacionbiopolimeros.com/","sameAs":["https://www.facebook.com/hdesthetique/","https://www.instagram.com/eliminabiopolimeros/","https://www.youtube.com/user/Prosumeur"],"logo":{"@type":"ImageObject","@id":"https://eliminacionbiopolimeros.com/#logo","url":"https://eliminacionbiopolimeros.com/wp-content/uploads/2019/12/eliminacion-biopolimeros-logo.jpg","width":180,"height":180,"caption":"Eliminacion Biopolimeros"},"image":{"@id":"https://eliminacionbiopolimeros.com/#logo"}},{"@type":"WebSite","@id":"https://eliminacionbiopolimeros.com/#website","url":"https://eliminacionbiopolimeros.com/","name":"Eliminacion Biopol\u00edmeros","description":"HDEsthetique","publisher":{"@id":"https://eliminacionbiopolimeros.com/#organization"},"potentialAction":{"@type":"SearchAction","target":"https://eliminacionbiopolimeros.com/?s={search_term_string}","query-input":"required name=search_term_string"}}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link crossorigin="" href="https://fonts.gstatic.com" rel="preconnect"/>
<link href="https://eliminacionbiopolimeros.com/feed/" rel="alternate" title="Eliminacion Biopolímeros » Feed" type="application/rss+xml"/>
<link href="https://eliminacionbiopolimeros.com/comments/feed/" rel="alternate" title="Eliminacion Biopolímeros » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/eliminacionbiopolimeros.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="//fonts.googleapis.com/css?family=System+Stack" id="generate-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eliminacionbiopolimeros.com/wp-content/plugins/instagram-feed/css/sb-instagram-2-1.min.css?ver=2.1.4" id="sb_instagram_styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eliminacionbiopolimeros.com/wp-includes/css/dist/block-library/style.min.css?ver=5.2.9" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eliminacionbiopolimeros.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.6" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eliminacionbiopolimeros.com/wp-content/plugins/wp-google-places-review-slider/public/css/wprev-public_combine.css?ver=6.4" id="wp-review-slider-pro-public_combine-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eliminacionbiopolimeros.com/wp-content/themes/generatepress/css/unsemantic-grid.min.css?ver=2.2.2" id="generate-style-grid-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eliminacionbiopolimeros.com/wp-content/themes/generatepress/style.min.css?ver=2.2.2" id="generate-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="generate-style-inline-css" type="text/css">
body{background-color:#efefef;color:#3a3a3a;}a, a:visited{color:#1e73be;}a:hover, a:focus, a:active{color:#000000;}body .grid-container{max-width:1100px;}body, button, input, select, textarea{font-family:"System Stack", ;}.entry-content > [class*="wp-block-"]:not(:last-child){margin-bottom:1.5em;}.main-navigation .main-nav ul ul li a{font-size:14px;}@media (max-width:768px){.main-title{font-size:30px;}h1{font-size:30px;}h2{font-size:25px;}}.top-bar{background-color:#636363;color:#ffffff;}.top-bar a,.top-bar a:visited{color:#ffffff;}.top-bar a:hover{color:#303030;}.site-header{background-color:#ffffff;color:#3a3a3a;}.site-header a,.site-header a:visited{color:#3a3a3a;}.main-title a,.main-title a:hover,.main-title a:visited{color:#222222;}.site-description{color:#757575;}.main-navigation,.main-navigation ul ul{background-color:#222222;}.main-navigation .main-nav ul li a,.menu-toggle{color:#ffffff;}.main-navigation .main-nav ul li:hover > a,.main-navigation .main-nav ul li:focus > a, .main-navigation .main-nav ul li.sfHover > a{color:#ffffff;background-color:#3f3f3f;}button.menu-toggle:hover,button.menu-toggle:focus,.main-navigation .mobile-bar-items a,.main-navigation .mobile-bar-items a:hover,.main-navigation .mobile-bar-items a:focus{color:#ffffff;}.main-navigation .main-nav ul li[class*="current-menu-"] > a{color:#ffffff;background-color:#3f3f3f;}.main-navigation .main-nav ul li[class*="current-menu-"] > a:hover,.main-navigation .main-nav ul li[class*="current-menu-"].sfHover > a{color:#ffffff;background-color:#3f3f3f;}.navigation-search input[type="search"],.navigation-search input[type="search"]:active{color:#3f3f3f;background-color:#3f3f3f;}.navigation-search input[type="search"]:focus{color:#ffffff;background-color:#3f3f3f;}.main-navigation ul ul{background-color:#3f3f3f;}.main-navigation .main-nav ul ul li a{color:#ffffff;}.main-navigation .main-nav ul ul li:hover > a,.main-navigation .main-nav ul ul li:focus > a,.main-navigation .main-nav ul ul li.sfHover > a{color:#ffffff;background-color:#4f4f4f;}.main-navigation .main-nav ul ul li[class*="current-menu-"] > a{color:#ffffff;background-color:#4f4f4f;}.main-navigation .main-nav ul ul li[class*="current-menu-"] > a:hover,.main-navigation .main-nav ul ul li[class*="current-menu-"].sfHover > a{color:#ffffff;background-color:#4f4f4f;}.separate-containers .inside-article, .separate-containers .comments-area, .separate-containers .page-header, .one-container .container, .separate-containers .paging-navigation, .inside-page-header{background-color:#ffffff;}.entry-meta{color:#595959;}.entry-meta a,.entry-meta a:visited{color:#595959;}.entry-meta a:hover{color:#1e73be;}.sidebar .widget{background-color:#ffffff;}.sidebar .widget .widget-title{color:#000000;}.footer-widgets{background-color:#ffffff;}.footer-widgets .widget-title{color:#000000;}.site-info{color:#ffffff;background-color:#222222;}.site-info a,.site-info a:visited{color:#ffffff;}.site-info a:hover{color:#606060;}.footer-bar .widget_nav_menu .current-menu-item a{color:#606060;}input[type="text"],input[type="email"],input[type="url"],input[type="password"],input[type="search"],input[type="tel"],input[type="number"],textarea,select{color:#666666;background-color:#fafafa;border-color:#cccccc;}input[type="text"]:focus,input[type="email"]:focus,input[type="url"]:focus,input[type="password"]:focus,input[type="search"]:focus,input[type="tel"]:focus,input[type="number"]:focus,textarea:focus,select:focus{color:#666666;background-color:#ffffff;border-color:#bfbfbf;}button,html input[type="button"],input[type="reset"],input[type="submit"],a.button,a.button:visited,a.wp-block-button__link:not(.has-background){color:#ffffff;background-color:#666666;}button:hover,html input[type="button"]:hover,input[type="reset"]:hover,input[type="submit"]:hover,a.button:hover,button:focus,html input[type="button"]:focus,input[type="reset"]:focus,input[type="submit"]:focus,a.button:focus,a.wp-block-button__link:not(.has-background):active,a.wp-block-button__link:not(.has-background):focus,a.wp-block-button__link:not(.has-background):hover{color:#ffffff;background-color:#3f3f3f;}.generate-back-to-top,.generate-back-to-top:visited{background-color:rgba( 0,0,0,0.4 );color:#ffffff;}.generate-back-to-top:hover,.generate-back-to-top:focus{background-color:rgba( 0,0,0,0.6 );color:#ffffff;}.entry-content .alignwide, body:not(.no-sidebar) .entry-content .alignfull{margin-left:-40px;width:calc(100% + 80px);max-width:calc(100% + 80px);}@media (max-width:768px){.separate-containers .inside-article, .separate-containers .comments-area, .separate-containers .page-header, .separate-containers .paging-navigation, .one-container .site-content, .inside-page-header{padding:30px;}.entry-content .alignwide, body:not(.no-sidebar) .entry-content .alignfull{margin-left:-30px;width:calc(100% + 60px);max-width:calc(100% + 60px);}}.rtl .menu-item-has-children .dropdown-menu-toggle{padding-left:20px;}.rtl .main-navigation .main-nav ul li.menu-item-has-children > a{padding-right:20px;}.one-container .sidebar .widget{padding:0px;}/* End cached CSS */
</style>
<link href="https://eliminacionbiopolimeros.com/wp-content/themes/generatepress/css/mobile.min.css?ver=2.2.2" id="generate-mobile-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eliminacionbiopolimeros.com/wp-content/themes/generatepress-child/style.css?ver=2.2.2.1553549517" id="generate-child-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eliminacionbiopolimeros.com/wp-content/plugins/widget-google-reviews/static/css/google-review.css?ver=1.8.2" id="grw_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eliminacionbiopolimeros.com/wp-content/plugins/gp-premium/blog/functions/css/style-min.css?ver=1.2.96" id="blog-styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://eliminacionbiopolimeros.com/wp-content/plugins/gp-premium/secondary-nav/functions/css/style.min.css?ver=1.2.96" id="generate-secondary-nav-css" media="all" rel="stylesheet" type="text/css"/>
<style id="generate-secondary-nav-inline-css" type="text/css">
.secondary-navigation{background-color:#636363;}.secondary-navigation .main-nav ul li a,.secondary-navigation .menu-toggle{color:#ffffff;}button.secondary-menu-toggle:hover,button.secondary-menu-toggle:focus{color:#ffffff;}.widget-area .secondary-navigation{margin-bottom:20px;}.secondary-navigation ul ul{background-color:#303030;top:auto;}.secondary-navigation .main-nav ul ul li a{color:#ffffff;}.secondary-navigation .main-nav ul li > a:hover,.secondary-navigation .main-nav ul li > a:focus,.secondary-navigation .main-nav ul li.sfHover > a{color:#ffffff;background-color:#303030;}.secondary-navigation .main-nav ul ul li > a:hover,.secondary-navigation .main-nav ul ul li > a:focus,.secondary-navigation .main-nav ul ul li.sfHover > a{color:#ffffff;background-color:#474747;}.secondary-navigation .main-nav ul li[class*="current-menu-"] > a, .secondary-navigation .main-nav ul li[class*="current-menu-"] > a:hover,.secondary-navigation .main-nav ul li[class*="current-menu-"].sfHover > a{color:#222222;background-color:#ffffff;}.secondary-navigation .main-nav ul ul li[class*="current-menu-"] > a,.secondary-navigation .main-nav ul ul li[class*="current-menu-"] > a:hover,.secondary-navigation .main-nav ul ul li[class*="current-menu-"].sfHover > a{color:#ffffff;background-color:#474747;}
</style>
<link href="https://eliminacionbiopolimeros.com/wp-content/plugins/gp-premium/secondary-nav/functions/css/mobile.min.css?ver=1.2.96" id="generate-secondary-nav-mobile-css" media="all" rel="stylesheet" type="text/css"/>
<script>if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script><script src="https://eliminacionbiopolimeros.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://eliminacionbiopolimeros.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://eliminacionbiopolimeros.com/wp-content/plugins/wp-google-places-review-slider/public/js/wprev-public-com-min.js?ver=6.4" type="text/javascript"></script>
<script src="https://eliminacionbiopolimeros.com/wp-content/plugins/widget-google-reviews/static/js/wpac-time.js?ver=1.8.2" type="text/javascript"></script>
<link href="https://eliminacionbiopolimeros.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://eliminacionbiopolimeros.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://eliminacionbiopolimeros.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.2.9" name="generator"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/> <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<noscript><style id="rocket-lazyload-nojs-css">.rll-youtube-player, [data-lazy-src]{display:none !important;}</style></noscript>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-15022402-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-15022402-3');
</script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '2490111957691756');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=2490111957691756&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body class="error404 wp-embed-responsive post-image-below-header post-image-aligned-center secondary-nav-above-header secondary-nav-aligned-right sticky-menu-fade right-sidebar nav-below-header fluid-header separate-containers active-footer-widgets-3 nav-aligned-left header-aligned-left dropdown-hover elementor-default" data-rsssl="1" itemscope="" itemtype="https://schema.org/WebPage">
<a class="screen-reader-text skip-link" href="#content" title="Skip to content">Skip to content</a> <header class="site-header" id="masthead" itemscope="" itemtype="https://schema.org/WPHeader">
<div class="inside-header grid-container grid-parent">
<div class="site-branding">
<p class="main-title" itemprop="headline">
<a href="https://eliminacionbiopolimeros.com/" rel="home">
					Eliminacion Biopolímeros
				</a>
</p>
<p class="site-description" itemprop="description">
				HDEsthetique
			</p>
</div> </div><!-- .inside-header -->
</header><!-- #masthead -->
<nav class="main-navigation" id="site-navigation" itemscope="" itemtype="https://schema.org/SiteNavigationElement">
<div class="inside-navigation grid-container grid-parent">
<button aria-controls="primary-menu" aria-expanded="false" class="menu-toggle">
<span class="mobile-menu">Menu</span>
</button>
<div class="main-nav" id="primary-menu"><ul class=" menu sf-menu" id="menu-english-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-997" id="menu-item-997"><a href="https://eliminacionbiopolimeros.com/">Español</a></li>
</ul></div> </div><!-- .inside-navigation -->
</nav><!-- #site-navigation -->
<div class="hfeed site grid-container container grid-parent" id="page">
<div class="site-content" id="content">
<div class="content-area grid-parent mobile-grid-100 grid-75 tablet-grid-75" id="primary">
<main class="site-main" id="main">
<div class="inside-article">
<header class="entry-header">
<h1 class="entry-title" itemprop="headline">Oops! That page can’t be found.</h1>
</header><!-- .entry-header -->
<div class="entry-content" itemprop="text">
<p>It looks like nothing was found at this location. Maybe try searching?</p><form action="https://eliminacionbiopolimeros.com/" class="search-form" method="get">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" title="Search for:" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form>
</div><!-- .entry-content -->
</div><!-- .inside-article -->
</main><!-- #main -->
</div><!-- #primary -->
<div class="widget-area grid-25 tablet-grid-25 grid-parent sidebar" id="right-sidebar" itemscope="" itemtype="https://schema.org/WPSideBar">
<div class="inside-right-sidebar">
<aside class="widget inner-padding widget_search" id="search-2"><form action="https://eliminacionbiopolimeros.com/" class="search-form" method="get">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" title="Search for:" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form>
</aside> <aside class="widget inner-padding widget_recent_entries" id="recent-posts-2"> <h2 class="widget-title">Recent Posts</h2> <ul>
<li>
<a href="https://eliminacionbiopolimeros.com/informacion-sobre-retiro-de-inyecciones-silicona-biopolimeros/">Informacion Sobre Retiro De Inyecciones de Biopolimeros y Silicona</a>
</li>
<li>
<a href="https://eliminacionbiopolimeros.com/sintomas-biopolimeros-en-gluteos-labios-cuerpo-senos/">Sintomas De Biopolimeros en Gluteos, Labios, Cuerpo Y Senos</a>
</li>
<li>
<a href="https://eliminacionbiopolimeros.com/mujer-es-arrestada-por-inyectar-y-deformarle-los-gluteos-a-otra-mujer-en-hialeah/">Mujer es arrestada por inyectar biopolimeros y deformarle los glúteos a otra mujer en Hialeah</a>
</li>
<li>
<a href="https://eliminacionbiopolimeros.com/informacion-consequencias-inyecciones-silicona-biopolimeros/">Informacion Y Consequencias Sobre Inyecciones De Biopolimeros/Silicona En Gluteos, Cara, Labios y Cuerpo</a>
</li>
<li>
<a href="https://eliminacionbiopolimeros.com/fda-advierte-contra-uso-inyecciones-silicona-para-aumentar-gluteos-labios-rostro-senos-y-cuerpo/">La FDA advierte contra el uso de inyecciones de silicona para aumentar gluteos, labios, rostro, senos y cuerpo</a>
</li>
</ul>
</aside><aside class="widget inner-padding widget_recent_comments" id="recent-comments-2"><h2 class="widget-title">Recent Comments</h2><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link"><a class="url" href="https://eliminacionbiopolimeros.com" rel="external nofollow">a. lucre</a></span> on <a href="https://eliminacionbiopolimeros.com/areas-donde-se-inyectan-biopolimeros/#comment-4">Areas Donde Se Inyectan Los Biopolimeros: Cara, Senos, Labios Y Gluteos</a></li></ul></aside><aside class="widget inner-padding widget_archive" id="archives-2"><h2 class="widget-title">Archives</h2> <ul>
<li><a href="https://eliminacionbiopolimeros.com/2020/04/">April 2020</a></li>
<li><a href="https://eliminacionbiopolimeros.com/2020/02/">February 2020</a></li>
<li><a href="https://eliminacionbiopolimeros.com/2020/01/">January 2020</a></li>
</ul>
</aside><aside class="widget inner-padding widget_categories" id="categories-2"><h2 class="widget-title">Categories</h2> <ul>
<li class="cat-item cat-item-1"><a href="https://eliminacionbiopolimeros.com/category/uncategorized/">Uncategorized</a>
</li>
</ul>
</aside><aside class="widget inner-padding widget_meta" id="meta-2"><h2 class="widget-title">Meta</h2> <ul>
<li><a href="https://eliminacionbiopolimeros.com/wp-login.php" rel="nofollow">Log in</a></li>
<li><a href="https://eliminacionbiopolimeros.com/feed/">Entries <abbr title="Really Simple Syndication">RSS</abbr></a></li>
<li><a href="https://eliminacionbiopolimeros.com/comments/feed/">Comments <abbr title="Really Simple Syndication">RSS</abbr></a></li>
<li><a href="https://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress.org</a></li> </ul>
</aside> </div><!-- .inside-right-sidebar -->
</div><!-- #secondary -->
<!--WPFC_FOOTER_START-->
</div><!-- #content -->
</div><!-- #page -->
<div class="site-footer">
<div class="site footer-widgets" id="footer-widgets">
<div class="footer-widgets-container grid-container grid-parent">
<div class="inside-footer-widgets">
<div class="footer-widget-1 grid-parent grid-33 tablet-grid-50 mobile-grid-100">
<aside class="widget inner-padding widget_text">
<h4 class="widget-title">Footer Widget</h4>
<div class="textwidget">
<p>
						Replace this widget content by going to <a href="https://eliminacionbiopolimeros.com/wp-admin/widgets.php"><strong>Appearance / Widgets</strong></a> and dragging widgets into this widget area.					</p>
<p>
						To remove or choose the number of footer widgets, go to <a href="https://eliminacionbiopolimeros.com/wp-admin/customize.php"><strong>Appearance / Customize / Layout / Footer Widgets</strong></a>.					</p>
</div>
</aside>
</div>
<div class="footer-widget-2 grid-parent grid-33 tablet-grid-50 mobile-grid-100">
<aside class="widget inner-padding widget_text">
<h4 class="widget-title">Footer Widget</h4>
<div class="textwidget">
<p>
						Replace this widget content by going to <a href="https://eliminacionbiopolimeros.com/wp-admin/widgets.php"><strong>Appearance / Widgets</strong></a> and dragging widgets into this widget area.					</p>
<p>
						To remove or choose the number of footer widgets, go to <a href="https://eliminacionbiopolimeros.com/wp-admin/customize.php"><strong>Appearance / Customize / Layout / Footer Widgets</strong></a>.					</p>
</div>
</aside>
</div>
<div class="footer-widget-3 grid-parent grid-33 tablet-grid-50 mobile-grid-100">
<aside class="widget inner-padding widget_text">
<h4 class="widget-title">Footer Widget</h4>
<div class="textwidget">
<p>
						Replace this widget content by going to <a href="https://eliminacionbiopolimeros.com/wp-admin/widgets.php"><strong>Appearance / Widgets</strong></a> and dragging widgets into this widget area.					</p>
<p>
						To remove or choose the number of footer widgets, go to <a href="https://eliminacionbiopolimeros.com/wp-admin/customize.php"><strong>Appearance / Customize / Layout / Footer Widgets</strong></a>.					</p>
</div>
</aside>
</div>
</div>
</div>
</div>
<footer class="site-info" itemscope="" itemtype="https://schema.org/WPFooter">
<div class="inside-site-info grid-container grid-parent">
<div class="copyright-bar">
<span class="copyright">© 2021 Eliminacion Biopolímeros</span> • Powered by <a href="https://generatepress.com" itemprop="url">GeneratePress</a> </div>
</div>
</footer><!-- .site-info -->
</div><!-- .site-footer -->
<!-- Instagram Feed JS -->
<script type="text/javascript">
var sbiajaxurl = "https://eliminacionbiopolimeros.com/wp-admin/admin-ajax.php";
</script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/eliminacionbiopolimeros.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://eliminacionbiopolimeros.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.6" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6LdJy6wUAAAAAFQsZLhvl7KC9O40IdK2KB3WkYQM&amp;ver=3.0" type="text/javascript"></script>
<!--[if lte IE 11]>
<script type='text/javascript' src='https://eliminacionbiopolimeros.com/wp-content/themes/generatepress/js/classList.min.js?ver=2.2.2'></script>
<![endif]-->
<script src="https://eliminacionbiopolimeros.com/wp-content/themes/generatepress/js/menu.min.js?ver=2.2.2" type="text/javascript"></script>
<script src="https://eliminacionbiopolimeros.com/wp-content/themes/generatepress/js/a11y.min.js?ver=2.2.2" type="text/javascript"></script>
<script src="https://eliminacionbiopolimeros.com/wp-content/plugins/gp-premium/secondary-nav/functions/js/navigation.min.js?ver=1.2.96" type="text/javascript"></script>
<script src="https://eliminacionbiopolimeros.com/wp-includes/js/wp-embed.min.js?ver=5.2.9" type="text/javascript"></script>
<script async="async" charset="utf-8" src="https://my.hellobar.com/6c42ec275a3e667163c6e13cf89739efd9808ccd.js" type="text/javascript"></script><script type="text/javascript">
( function( grecaptcha, sitekey, actions ) {

	var wpcf7recaptcha = {

		execute: function( action ) {
			grecaptcha.execute(
				sitekey,
				{ action: action }
			).then( function( token ) {
				var forms = document.getElementsByTagName( 'form' );

				for ( var i = 0; i < forms.length; i++ ) {
					var fields = forms[ i ].getElementsByTagName( 'input' );

					for ( var j = 0; j < fields.length; j++ ) {
						var field = fields[ j ];

						if ( 'g-recaptcha-response' === field.getAttribute( 'name' ) ) {
							field.setAttribute( 'value', token );
							break;
						}
					}
				}
			} );
		},

		executeOnHomepage: function() {
			wpcf7recaptcha.execute( actions[ 'homepage' ] );
		},

		executeOnContactform: function() {
			wpcf7recaptcha.execute( actions[ 'contactform' ] );
		},

	};

	grecaptcha.ready(
		wpcf7recaptcha.executeOnHomepage
	);

	document.addEventListener( 'change',
		wpcf7recaptcha.executeOnContactform, false
	);

	document.addEventListener( 'wpcf7submit',
		wpcf7recaptcha.executeOnHomepage, false
	);

} )(
	grecaptcha,
	'6LdJy6wUAAAAAFQsZLhvl7KC9O40IdK2KB3WkYQM',
	{"homepage":"homepage","contactform":"contactform"}
);
</script>
<script>window.lazyLoadOptions = {
                elements_selector: "[loading=lazy],.rocket-lazyload",
                data_src: "lazy-src",
                data_srcset: "lazy-srcset",
                data_sizes: "lazy-sizes",
                class_loading: "lazyloading",
                class_loaded: "lazyloaded",
                threshold: 300,
                callback_loaded: function(element) {
                    if ( element.tagName === "IFRAME" && element.dataset.rocketLazyload == "fitvidscompatible" ) {
                        if (element.classList.contains("lazyloaded") ) {
                            if (typeof window.jQuery != "undefined") {
                                if (jQuery.fn.fitVids) {
                                    jQuery(element).parent().fitVids();
                                }
                            }
                        }
                    }
                },
use_native: true};
        window.addEventListener('LazyLoad::Initialized', function (e) {
            var lazyLoadInstance = e.detail.instance;
        
            if (window.MutationObserver) {
                var observer = new MutationObserver(function(mutations) {
                    var image_count = 0;
                    var iframe_count = 0;
                    var rocketlazy_count = 0;

                    mutations.forEach(function(mutation) {
                        for (i = 0; i < mutation.addedNodes.length; i++) {
                            if (typeof mutation.addedNodes[i].getElementsByTagName !== 'function') {
                                return;
                            }

                           if (typeof mutation.addedNodes[i].getElementsByClassName !== 'function') {
                                return;
                            }

                            images = mutation.addedNodes[i].getElementsByTagName('img');
                            is_image = mutation.addedNodes[i].tagName == "IMG";
                            iframes = mutation.addedNodes[i].getElementsByTagName('iframe');
                            is_iframe = mutation.addedNodes[i].tagName == "IFRAME";
                            rocket_lazy = mutation.addedNodes[i].getElementsByClassName('rocket-lazyload');

                            image_count += images.length;
			                iframe_count += iframes.length;
			                rocketlazy_count += rocket_lazy.length;
                            
                            if(is_image){
                                image_count += 1;
                            }

                            if(is_iframe){
                                iframe_count += 1;
                            }
                        }
                    } );

                    if(image_count > 0 || iframe_count > 0 || rocketlazy_count > 0){
                        lazyLoadInstance.update();
                    }
                } );
                
                var b      = document.getElementsByTagName("body")[0];
                var config = { childList: true, subtree: true };
                
                observer.observe(b, config);
            }
        }, false);</script><script async="" data-no-minify="1" src="https://eliminacionbiopolimeros.com/wp-content/plugins/rocket-lazy-load/assets/js/12.0/lazyload.min.js"></script>
</body>
</html>
