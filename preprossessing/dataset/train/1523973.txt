<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Bolja budućnost | Udruga žena Romkinja Hrvatske | Službena web stranica</title>
<meta content="Udruga žena Romkinja Hrvatske, Bolja Budućnost, humanitarna udruga za probleme Roma i žena Romkinja." name="description"/>
<meta content="Bolja, budućnost, udruga, žena, roma, romkinja, hrvatske, romi, romkinje, humanitarna, organizacija, Hrvatska, zaštita, savjetovanje, pomoć, suradnja, sigurnost, Europa, romski." name="keywords"/>
<meta content="" name="keywords"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="css/stil.css" rel="stylesheet" type="text/css"/>
<link href="javascript/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css"/>
<script src="javascript/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="javascript/jscript.js" type="text/javascript"></script>
<script src="javascript/fancybox/jquery.easing-1.3.pack.js" type="text/javascript"></script>
<script src="javascript/fancybox/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script>
<script src="javascript/fancybox/jquery.mousewheel-3.0.4.pack.js" type="text/javascript"></script>
<meta content="v6v9conZL9JzMKL6d6ntJNG_D15Fgc3f9msUW5ATEsM" name="google-site-verification"/> </head>
<body>
<div id="container">
<div id="header">
<h2 style="color:white; font-size:16px; font-weight:normal; position:absolute; top:20px; right:610px; z-index:20; text-shadow: 1px 1px 1px #000000;">Udruga žena Romkinja Hrvatske</h2>
<h2 style="color:#FFBFFF; font-size:16px; font-weight:normal; font-style:italic; position:absolute; right:15px; z-index:20; text-shadow: 1px 1px 1px #000000;">Bolja Budućnost | Udruga žena Romkinja Hrvatske | <span style="color:white; font-size:20px;">Službena web stranica</span></h2>
<h2 style="color:#FFBFFF; font-size:16px; font-weight:normal; font-style:italic; position:absolute; right:15px; top:25px; z-index:20; text-shadow: 1px 1px 1px #000000;">Samo znanje može pomoći ženi za uspjeh u životu</h2>
<div id="banner"></div>
<div id="linkovi">
<a href="index.php?jezik=" id="link1" style="float:left;">Početna</a>
<a href="vijesti.php?jezik=" id="link2" style="float:left;">Vijesti</a>
<a href="javascript: void();" id="link3" onmouseout="hoverout3()" onmouseover="hoverin3()" style="float:left;">O nama</a>
<a href="javascript: void();" id="link4" onmouseout="hoverout4()" onmouseover="hoverin4()" style="float:left;">Projekti</a>
<a href="javascript: void();" id="link5" onmouseout="hoverout5()" onmouseover="hoverin5()" style="float:left;">Izdavaštvo</a>
<a href="kontakt.php?jezik=" id="link6" style="float:left;">Kontakt</a>
<a href="javascript: void();" id="link7" onmouseout="hoverout7()" onmouseover="hoverin7()" style="float:left;">Galerija</a>
<a href="arhiva.php?jezik=" id="link8" onmouseout="hoverout8()" onmouseover="hoverin8()" style="float:left;">Arhiva</a>
<div style="clear:both;"></div>
</div>
<a class="hrv" href="index.php?jezik=hr"></a>
<a class="eng" href="index.php?jezik=en"></a>
<div id="kat3" onmouseout="hoverout3()" onmouseover="hoverin3()">
<div class="kat_top"></div>
<div class="kat_mid">
<a class="box" href="misija.php?jezik=">Misija</a>
<a class="box" href="vizija.php?jezik=" style="border:none;">Vizija</a>
</div>
<div class="kat_bot"></div>
</div>
<div id="kat4" onmouseout="hoverout4()" onmouseover="hoverin4()">
<div class="kat_top"></div>
<div class="kat_mid">
<a class="box" href="projekt.php?id=27&amp;jezik=">Život nam je komedija, a cijeli svijet pozorište</a>
<a class="box" href="projekt.php?id=26&amp;jezik=">SEMINAR U TERME JEŽERČICA</a>
<a class="box" href="projekt.php?id=25&amp;jezik=">škola za djecu i mladež</a>
<a class="box" href="projekt.php?id=24&amp;jezik=">Intervju sa g. Paun</a>
<a class="box" href="projekt.php?id=23&amp;jezik=">Mis Romkinja </a>
<a class="box" href="projekt.php?id=22&amp;jezik=">„Bolja perspektiva žena Romkinja na tržištu rada“</a>
<a class="box" href="projekt.php?id=18&amp;jezik=">Afirmacija mladih i obuka edukatora</a>
<a class="box" href="projekt.php?id=17&amp;jezik=">Ljetna škola </a>
<a class="box" href="projekt.php?id=16&amp;jezik=">I mi imamo snove, ne uskraćujte nam budućnost </a>
<a class="box" href="projekt.php?id=15&amp;jezik=">Prevencija ženskog zdravlja</a>
</div>
<div class="kat_bot"></div>
</div>
<div id="kat5" onmouseout="hoverout5()" onmouseover="hoverin5()">
<div class="kat_top"></div>
<div class="kat_mid">
<a class="box" href="izdavastvo.php?id=30&amp;jezik=">UŽRH" BOLJA BUDUĆNOST"</a>
<a class="box" href="izdavastvo.php?id=27&amp;jezik=">Šarin veliki rječnik</a>
<a class="box" href="izdavastvo.php?id=26&amp;jezik=">Istraživanje položaja Roma u RH</a>
<a class="box" href="izdavastvo.php?id=25&amp;jezik=">Časopis Budućnost</a>
<a class="box" href="izdavastvo.php?id=23&amp;jezik=">Upoznajmo svoja prava</a>
</div>
<div class="kat_bot"></div>
</div>
<div id="kat7" onmouseout="hoverout7()" onmouseover="hoverin7()">
<div class="kat_top"></div>
<div class="kat_mid">
<a class="box" href="galerija.php?jezik=&amp;galerija=35">konferencija u Beogradu 2018</a>
<a class="box" href="galerija.php?jezik=&amp;galerija=34">teaterR</a>
<a class="box" href="galerija.php?jezik=&amp;galerija=33">TEATARR</a>
<a class="box" href="galerija.php?jezik=&amp;galerija=31">prekinimo šutnju</a>
<a class="box" href="galerija.php?jezik=&amp;galerija=28">Informativni sastanka za stausna pitanja u Kozari boku 10. mj. 2</a>
<a class="box" href="galerija.php?jezik=&amp;galerija=26">godišnji izvještaj</a>
<a class="box" href="galerija.php?jezik=&amp;galerija=25">Ljetna Škola 2012</a>
<a class="box" href="galerija.php?jezik=&amp;galerija=24">aktivnost</a>
<a class="box" href="galerija.php?jezik=&amp;galerija=20">SASTANAK DEKADE U MAKEDONIJI</a>
<a class="box" href="galerija.php?jezik=&amp;galerija=19">SASTANAK U ZAGREBU UPRAVNOG ODBORA DEKADE</a>
<a class="box" href="galerija.php?jezik=&amp;galerija=18">Sudjelovanje članova udruge na međunarodnima seminarima</a>
<a class="box" href="galerija.php?jezik=&amp;galerija=17">8. mart dan žena 2012</a>
<a class="box" href="galerija.php?jezik=&amp;galerija=14">Promocija Šarinog rječnika</a>
<a class="box" href="galerija.php?jezik=&amp;galerija=13">Kongres Roma u RH</a>
<a class="box" href="galerija.php?jezik=&amp;galerija=12">Dani Udruge</a>
</div>
<div class="kat_bot"></div>
</div>
</div>
<div id="content">
<div class="left">
<div class="slike">
<a href="galerija.php?jezik=&amp;galerija=16"><img alt="" class="okvir" src="galerije/Casopis Buducnost/thumbs/casopis_buducnost-1.jpg" title="16"/></a>
<img alt="" class="sjena" src="grafika/image_shadow.png"/>
</div>
<div class="slike">
<a href="galerija.php?jezik=&amp;galerija=31"><img alt="" class="okvir" src="galerije/prekinimo sutnju/thumbs/prekinimo_sutnju-14183841463.jpg" title="31"/></a>
<img alt="" class="sjena" src="grafika/image_shadow.png"/>
</div>
<div class="slike">
<a href="galerija.php?jezik=&amp;galerija=25"><img alt="" class="okvir" src="galerije/Ljetna Skola 2012/thumbs/ljetna_skola_2012-9.jpg" title="25"/></a>
<img alt="" class="sjena" src="grafika/image_shadow.png"/>
</div>
<div class="slike">
<a href="galerija.php?jezik=&amp;galerija=18"><img alt="" class="okvir" src="galerije/Sudjelovanje clanova udruge na medunarodnima seminarima/thumbs/sudjelovanje_clanova_udruge_na_medunarodnima_seminarima-13494396201.jpg" title="18"/></a>
<img alt="" class="sjena" src="grafika/image_shadow.png"/>
</div>
<div class="slike">
<a href="galerija.php?jezik=&amp;galerija=13"><img alt="" class="okvir" src="galerije/Kongres Roma u RH/thumbs/kongres_roma_u_rh-5.jpg" title="13"/></a>
<img alt="" class="sjena" src="grafika/image_shadow.png"/>
</div>
<div class="donatori">
<h2>DONATORI</h2>
<div class="don2">
<a class="reklama" href="http://www.nacionalne-manjine.info/"><img alt="" height="48" src="grafika/logocard.gif" width="141"/></a>
</div>
<div class="don2">
<a class="reklama" href="http://www.care.org/"><img alt="" height="175" src="grafika/care.gif" width="141"/></a>
</div>
</div>
<div style="clear:both;"></div>
</div>
<div id="mid">
<div id="mid_top"></div>
<div id="mid_mid">
<div class="text">
<br/>
<p style="margin:0; letter-spacing:0px;">
<img alt="" src="grafika/slika.jpg" style="float:left;"/>
								Kroz dugogodišnje djelovan je i upoznavanje s problematikom Roma, uočili smo da usprkos brojnim problemima Roma, kao što su diskriminacija i nizak stupanj obrazovanja, problemi Romskih žena su daleko veći. Romske su žene dvostruko diskriminirane, spolno i naconalno. Kako romski narod običava i živi tradicionalnim patrijarhalnim životom, Romkinje su diskriminirane i unutar svoje vlastite populacije. Nemaju prava, a ne usuđuju se niti spomenuti ravnopravnost sa suprugom, odnosno muškim članom obitelji.<br/>
<br/>
Najveći problem svakako je vezan uz djevojčice romske nacionalnosti. Žensko dijete u romskoj obitelji ne očekuje se na isti način kao muško dijete, obitelj je u pravilu ne šalje u školu, oduzima joj se pravo na izbor bračnog partnera, uništava joj se djetinjstvo. Zapravo, djevojčica Romkinja i nema djetinjstvo, jer joj se već u najranijoj dobi izabire budući bračni partner, a taj se nepravedni čin ponavlja stoljećima, prenoseći se iz generacije u generaciju.<br/>
<br/>
Zbog svih ovih problema koje smo uvidjeli, 1998. godine osnovana je Udruga Žena Romkinja - “Bolja budućnost”. Udruga je registrirana u siječnju 1999. godine. Od svog početka radimo na promociji prava Romkinja, odnosno poboljšanju njihovog ekonomskog, socijalnog, obrazovnog, zdravstvenog i pravnog statusa na području Republike Hrvatske. Vezano uz ovu problematiku, pružamo besplatne pravne <a href="https://www.ptwatches.com/rolex-day-date/">swiss rolex day-date copy watches</a> savjete u uredu udruge, ali i u kontaktu s Romkinjama na terenu, jer je evidentno kako Romkinje, odnosno veliki dio romske populacije nije upoznat sa svojim pravima u RH.<br/>
<br/>
Borimo se protiv spolne diskriminacije, koja je vrlo izražena u romskoj populaciji. Posebno se zalažemo za obrazovanje djece, s naglaskom na djevojčice, jer samo znanje može pomoći ženi da postigne uspjeh u životu! Također, pružamo i besplatne pravne savjete za reguliranje statusa u Republici Hrvatskoj, s obzirom da ima osoba koje još uvijek nisu stekle pravo na državljanstvo ili trajno <a href="https://www.watcheslive.com/panerai-replica/">panerai fake watches for sale</a> ishodovanje boravišta u RH.<br/>
<br/>
<b>Predsjednica Udruge<br/>
Ramiza Memedi </b></p><div style="position:absolute; left: -99999px;">Kasyno online (kasyno, które znajduje się w Internecie) - strona lub program, który pozwala na grę na automatach i inne gry hazardowe na prawdziwe pieniądze lub w trybie demo poprzez połączenie z Internetem. Kasyna online składają się zazwyczaj z 3 elementów:<br/>
<br/>
<p>Strona <a href="https://forzamilan.pl/2020/04/przedstawienie-dostawcy-gier-internetowych-quickspin/">kasyna online</a> . Prosta strona z danymi o kasynach online. Co do zasady, zawiera strony: o firmie właściciela, licencji, rodzajach rozrywki, programach bonusowych, zasadach, sposobach wpłacania i wypłacania gotówki, z danymi o serwisie wsparcia i rozwiązaniach dla często zadawanych pytań. Niektóre kasyna oferują na stronie głównej wejście na indywidualną stronę gracza, gdzie można śledzić historię jego działań, wyniki poprzednich gier i osobiste statystyki.</p><br/>
<p>Oprogramowanie do gier (oprogramowanie). Rzeczywiste oprogramowanie kasyna online i daje możliwość uruchamiania swoich ulubionych gier. Programy stron hazardowych można podzielić na 2 imponujące grupy: nie pobierające (Instant play) i pobierające (Download). Wiele kasyn korzysta z obu wersji. Jak można się było domyślać, pobrane programy należy pobrać z Internetu i rozpakować na własnym komputerze, są one zwykłym oprogramowaniem Windows (pod innymi systemami operacyjnymi prawdopodobnie nie znajdziesz wersji instalatora). Oprogramowanie, którego nie można pobierać, pozwala na grę z wykorzystaniem wyłącznie Internetu i przeglądarki. Istnieje kilka dużych producentów oprogramowania, którzy wdrażają swoje własne programy do setek różnych klubów gier hazardowych.</p><br/>
<p>Rachunki w kasynach online. Korzystając z kart płatniczych lub portfeli elektronicznych, możesz doładować swoje osobiste konto w kasynie i wypłacić swoje wygrane - te operacje nazywane są transakcjami. Konto osobiste może być częścią Twojego zainstalowanego oprogramowania lub częścią strony internetowej kasyna. Transakcje mogą być dokonywane z pomocą firmy należącej do właścicieli domu gier, ale procedura ta jest prawdopodobnie przeprowadzana przez osoby trzecie. Firmy te mogą być partnerami kasyna lub dostawcami oprogramowania do gier używanego przez klub.</p><br/>
</div><br/>
</div>
<div class="vijesti">
<h1 style="color:#8D0490; margin:0; position:absolute; left:10px; top:0; font-size:20px; font-weight:normal;">Vijesti</h1>
<div style="width:610px; margin:50px auto; padding-top:40px;">
<div class="vijesti_left">
<div class="vijesti_mid" style="border: 0">
<img alt="" src="vijesti/136/vijest_136_thumb.jpg" style="float:left; margin-right:10px; width:80px; height:80px; border:black solid 1px;"/>
<div class="vijesti_text">
<h1 style="color:#8C0490; margin:0; font-size:16px; margin-bottom:2px;">MOTIVIRANOST ROMKINJA RADIONI</h1>
											Grad Zagreb provodi dva europsak projekat pod
nazivom “Postani aktivan Rom/Romkinja i zaposli
se - PARIZ”, i „Socijalno se uključi i zaposli ...
											<a href="clanak.php?id=136&amp;jezik=" style="bottom:20px;">pročitaj više</a>
</div>
</div>
<div class="vijesti_mid" style="border: 0">
<img alt="" src="vijesti/135/vijest_135_thumb.jpg" style="float:left; margin-right:10px; width:80px; height:80px; border:black solid 1px;"/>
<div class="vijesti_text">
<h1 style="color:#8C0490; margin:0; font-size:16px; margin-bottom:2px;">Nasilje nad ženama</h1>
											Zadnjih nekoliko godina postoje više organiziranih aktivnosti za podizanje svijesti građana borbi protiv nasilja a posebno nad ženama. 
Nažalost,...
											<a href="clanak.php?id=135&amp;jezik=" style="bottom:20px;">pročitaj više</a>
</div>
</div>
</div>
<div class="vijesti_right">
<div class="vijesti_mid" style="border: 0">
<img alt="" src="vijesti/134/vijest_134_thumb.jpg" style="float:left; margin-right:10px; width:80px; height:80px; border:black solid 1px;"/>
<div class="vijesti_text">
<h1 style="color:#8C0490; margin:0; font-size:16px; margin-bottom:2px;">Romskoj djeci treba otvoriti </h1>
											
Ramiza Memedi: Romskoj djeci treba otvoriti veće mogućnosti za umjetničko stvaralaštvo
August 18, 2019
103
Podijelite s drugima na Facebooku!...
											<a href="clanak.php?id=134&amp;jezik=" style="bottom:20px;">pročitaj više</a>
</div>
</div>
<div class="vijesti_mid" style="border: 0">
<img alt="" src="vijesti/132/vijest_132_thumb.jpg" style="float:left; margin-right:10px; width:80px; height:80px; border:black solid 1px;"/>
<div class="vijesti_text">
<h1 style="color:#8C0490; margin:0; font-size:16px; margin-bottom:2px;">ROMSKE OBITELJI SU IPAK PRESE</h1>
											Bez obzira na žestoke i  verbalne prosvjedie i fizičkih blokada zgrade kojoj su pribjegli prošloga tjedna nisu pomogli stanovnicima Novog Petrušev...
											<a href="clanak.php?id=132&amp;jezik=" style="bottom:20px;">pročitaj više</a>
</div>
</div>
</div>
<div style="clear:both;"></div>
</div>
</div>
</div>
<div id="mid_bot"></div>
</div>
<div class="right">
<div class="slike">
<a href="galerija.php?jezik=&amp;galerija=17"><img alt="" class="okvir" src="galerije/8. mart dan zena 2012/thumbs/8._mart_dan_zena_2012-1.jpg" title="17"/></a>
<img alt="" class="sjena" src="grafika/image_shadow.png"/>
</div>
<div class="slike">
<a href="galerija.php?jezik=&amp;galerija=33"><img alt="" class="okvir" src="galerije/TEATARR/thumbs/teatarr-5.jpg" title="33"/></a>
<img alt="" class="sjena" src="grafika/image_shadow.png"/>
</div>
<div class="slike">
<a href="galerija.php?jezik=&amp;galerija=33"><img alt="" class="okvir" src="galerije/TEATARR/thumbs/teatarr-4.jpg" title="33"/></a>
<img alt="" class="sjena" src="grafika/image_shadow.png"/>
</div>
<div class="slike">
<a href="galerija.php?jezik=&amp;galerija="><img alt="" class="okvir" src="galerije/nasilje nad zenama/thumbs/" title=""/></a>
<img alt="" class="sjena" src="grafika/image_shadow.png"/>
</div>
<div class="slike">
<a href="galerija.php?jezik=&amp;galerija=13"><img alt="" class="okvir" src="galerije/Kongres Roma u RH/thumbs/kongres_roma_u_rh-1.jpg" title="13"/></a>
<img alt="" class="sjena" src="grafika/image_shadow.png"/>
</div>
</div>
<div style="clear:both;"></div>
</div>
<div id="footer">
<h2 style="color:white; font-size:16px; font-weight:normal; position:absolute; top:20px; right:675px; z-index:20; text-shadow: 1px 1px 1px #000000;">Udruga žena Romkinja Hrvatske</h2>
<div id="info">
					Avenija Marina Držića 4<br/>
					Zagreb<br/>
					10000<br/>
					Hrvatska<br/><br/>

					+385 1 6110 311<br/>
<a href="http://www.uzrh-bb.hr">http://www.uzrh-bb.hr</a><br/><br/>
<a href="mailto:uzrh.boljabuducnost@yahoo.com?">uzrh.boljabuducnost@yahoo.com</a>
<a href="mailto:ramiza.mehmedi@zg.t-com.hr?">ramiza.memedi@zg.t-com.hr</a>
</div>
<div id="info_bot">
<p>Udruga Bolja Budućnost | Copyright © 2011 | Sva prava pridržana | <a href="http://www.croative.org">izrada internet stranica <span style="font-size: 20px;"><span style="color:red">Cro</span>ative</span></a></p>
</div>
</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6668685-13']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</div>
</body>
</html>
