<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<html>
<head>
<meta content="Ethernet Topologies Gigabit LAN Standards Gigabit Ethernet Leads the Way Over Fibe" http-equiv="keywords"/>
<meta content="Ethernet Topologies Gigabit LAN Standards Gigabit Ethernet Leads the Way Over Fibe" http-equiv="description"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Microsoft FrontPage 6.0" name="GENERATOR"/>
<meta content="FrontPage.Editor.Document" name="ProgId"/>
<title>Ethernet Topologies Gigabit LAN Standards Gigabit Ethernet Leads the Way 
Over Fiber</title>
</head>
<body background="Pink_and_WhiteB0A6.gif" bgcolor="#FFFFFF">
<div align="center">
<center>
<table background="WB00516_.gif" border="6" bordercolor="#008080" bordercolordark="#008080" bordercolorlight="#008080" width="71%">
<tr>
<td width="100%">
<p align="center"><font color="#FF0000" size="7"><strong>Ethernet</strong> <b>Overview</b></font></p>
</td>
</tr>
</table>
</center>
</div>
<p align="center">
<font size="4">
Gigabit LAN Standards Gigabit Ethernet Leads the Way Over Fiber</font></p>
<p align="center"><a href="#Gigabit LANs at a Glance">Gigabit LAN
Standards</a></p>
<hr color="#008080" size="3"/>
<p align="center"><font color="#FF0000" face="Arial" size="6">Specifications <br/>
</font><font size="5"><br/>
Bus-Standard Ethernet (Coax)</font></p>
<p align="center"><b><font size="4">10BASE5</font></b></p>
<ol>
<li><font face="Arial" size="3">The maximum length of a segment is 500 m (1640 feet).A
        maximum of 2 IRL (Inter-Repeater Links) is allowed
        between devices</font></li>
<li><font face="Arial" size="3">The maximum length of cable is 2.5 km (1.5 miles).</font></li>
<li><font face="Arial" size="3">Devices attach to the backbone via transceivers.</font></li>
<li><font face="Arial" size="3">The minimum distance between transceivers is 2.5 m (8.2
        feet).</font></li>
<li><font face="Arial" size="3">The maximum length of a transceiver cable is 50 m (164
        feet),</font></li>
<li><font face="Arial" size="3">Up to 100 transceiver connections can be attached to a
        single segment.</font></li>
<li><font face="Arial" size="3">Only transceivers without SOE ("heartbeat")
        test enabled should be used with repeaters.</font></li>
<li><font face="Arial" size="3">Both ends of each segment should be terminated with a
        50-ohm resistor.</font></li>
<li><font face="Arial" size="3">One end of each segment should be grounded to earth.</font>
<hr color="#008080" size="3"/>
</li>
</ol>
<p align="center"><font color="#FF0000" size="5">Twisted-Pair Ethernet
(Unshielded Twisted Pair):</font></p>
<p align="center"><font size="4">10BASE=T, UTP</font></p>
<ol>
<li><font face="Arial" size="3">There are two versions of Ethernet over unshielded
        twisted pair-. 10BASE-T (the standard) and its
        predecessor UTP.</font> </li>
<li><font face="Arial" size="3">10BASE-T and UTP segments can coexist on the same network
        when each hub is attached to a common segment, via a
        transceiver and transceiver cable, or converter.</font></li>
<li><font face="Arial" size="3">The cable used is 22 to 26 AWG unshielded twisted pair
        (standard telephone wire), at least Category 2 with two
        twists per foot. Category 3 or 4 is preferred. Category 5
        supports 100 BASE-T (Fast Ethernet).</font></li>
<li><font face="Arial" size="3">Workstations are connected to a central concentrator
        ("hub") in a star configuration. Concentrators
        can be attached to a fiber optic or coax network, and can
        be daisy chain to form larger networks.</font></li>
<li><font face="Arial" size="3">A hub can have AUI port, BNC ThinNet, 10baseT, Fiberoptic
        for up steam standard Ethernet connections and 10baseT
        down steam.</font></li>
<li><font face="Arial" size="3">The maximum distance of a segment (from concentrator to
        node) is 100 m (328 feet).</font></li>
<li><font face="Arial" size="3">The maximum number
        of devices per segment is 2. One device is the hub port;
        the other is the 10BASE-T or UTP device.</font></li>
<li><font face="Arial" size="3">Ethernet network
        interface cards (NICS) are available with built-in
        10BASE-T transceivers. Devices with standard AUI ports
        may be attached with a twisted-pair transceiver. Twisted
        pair is the most economical cable type, especially since
        it may already be installed, and it is the easiest to
        work with. But it is not recommended for installations
        with much EMI/RFI interference (for example, in
        industrial environments).</font>
<hr color="#008080" size="3"/>
</li>
</ol>
<p align="center"><font color="#FF0000" size="5">Bus-Thin Net Ethernet (Coax):<br/>
</font><font size="4"><b>
10BASE2</b></font></p>
<ol>
<li><font face="Arial" size="3">The maximum length of a segment is 185 m (607 feet).</font></li>
<li><font face="Arial" size="3">A maximum of 2 IRL (Inter-Repeater Links) is allowed
        between devices, the maximum length of cable is 925 m
        (3035 feet).</font></li>
<li><font face="Arial" size="3">Typically, devices use Ethernet network interface cards
        (NICs') with built in BNC transceivers.
        This eliminates the need for separate transceivers, as
        connections can be made directly to the Thin Net cable.</font></li>
<li><font face="Arial" size="3">Devices are
        connected to the cable with T-connectors, which must be
        plugged directly into the card. Each dead end must be
        terminated with a BNC 50-ohm terminator (typical is at
        the server end and at the ends of the daisy chain) No
        cable is allowed between the T and the card. Workstations
        are daisy chained, with an "In-and-out" (unless
        it is terminated) cabling system.</font></li>
<li><font face="Arial" size="3">The minimum distance between T-connectors is 0.5 m (1.6
        feet).</font></li>
<li><font face="Arial" size="3">If the interface card does not have its own built-in BNC
        transceiver, a BNC transceiver and transceiver cable are
        required. The maximum length of a transceiver cable is 50
        m (164 feet).</font></li>
<li><font face="Arial" size="3">Up to 30 connections can be attached to a single segment.</font></li>
<li><font face="Arial" size="3">Both ends of each segment should be terminated with a
        50-ohm resistor.</font></li>
<li><font face="Arial" size="3">One end of each segment should be grounded to earth.</font>
<hr color="#008080" size="3"/>
</li>
</ol>
<p align="center"><font color="#FF0000" size="5"><b>Star-Fiberoptic Ethernet<br/>
</b></font><font size="4"><b>
FOIRL or 10BASEN-FL</b></font></p>
<p><font size="4">There are two versions of Ethernet over
fiberoptic cable, meeting the older FOIRL (Fiberoptic
Inter-Repeater Link) and the more recent 10BASE-FL standards.</font></p>
<ol>
<li><font face="Arial" size="3">FOIRL and 10BASE-FL fiberoptic Ethernet differ only in
        how far each will transmit (the maximum length of a
        segment). For FOIRL it is 1 km (0.6 miles); for 10BASE-FL
        it is 2 km (1.2 miles).</font></li>
<li><font face="Arial" size="3">The maximum number of devices per segment is 2. One
        device is the hub port, the other device is the 10BASE-FL
        device.</font></li>
<li><font face="Arial" size="3">Fiber optic cable provides the best signal quality as
        well as the greatest point-to-point distance typical
        cable is 62um core 125um cladding.</font></li>
<li><font face="Arial" size="3">Fiber optic cable is completely free of EMI/RFI
        interference.</font></li>
<li><font face="Arial" size="3">Fiber optic cable runs point to point only; it cannot be
        tapped or daisy chained which adds to the security of the
        network. A fiber optic hub or multiport repeater is
        required to carry the signal to multiple devices (for
        FOIRL, a FOIRL multiport repeater and transceivers).</font></li>
<li><font face="Arial" size="3">Since fiber optic cable does not carry electrical
        charges, all electrical cable problems disappear. When
        fiber optic cable (outdoor quality) is used to link
        buildings, grounding problems, ground loops, and voltage
        spikes are eliminated and fiberoptic cable is all so
        immune to electronic eavesdropping.</font></li>
</ol>
<hr color="#008080" size="3"/>
<p><font size="4"><strong>Ethernet</strong></font><font face="Arial" size="3">
is the most widely used network topology. You can choose between
bus and</font><font size="4"> </font><font face="Arial" size="3"> star topologies, and coaxial, twisted-pair, or fiber
optic cabling. But with the right connective equipment, multiple
Ethernet-based LANs (local area networks) can be linked together
no matter which topology and/or cabling system they use. In fact,
with the right equipment and software, even Token Ring, Apple
Talk, and wireless LANs can be connected to Ethernet.</font></p>
<p><font face="Arial" size="3">The access method Ethernet uses is CSMA/CD (Carrier
Sense Multiple Access with Collision Detection). In this method,
multiple workstation access a transmission medium (Multiple
Access) by listening until no signals are detected (Carrier
Sense). Then they transmit and check to see if more than one
signal is present (Collision Detection). Each station attempts to
transmit when it "believes" the network is free. If
there is a collision, each station attempts to retransmit after a
preset delay, which is different for each workstation.</font></p>
<p><font face="Arial" size="3">Collision detection is an essential part of the
CSMA/CD access method. Each transmitting workstation needs to be
able to detect that simultaneous (and therefore data-corrupting)
transmission has taken place. If a collision is detected, a
"jam" signal is propagated to all nodes. Each station
that detects the Collision will wait some period of time and then
try again.</font></p>
<p><font face="Arial" size="3">The two Possible topologies for Ethernet are bus
and star. The bus is the simplest (and the
traditional) topology. Standard Ethernet (10BASE5) and Thin
Ethernet (1OBASE2), both based on coaxial cable systems, use the
bus.</font></p>
<p><font face="Arial" size="3">In this one-cable LAN, all workstations are
connected in succession (a "bus" arrangement) on a
single cable. All transmissions go to all the connected
workstations. Each workstation then selects those transmissions
it should receive, based on the address information contained in
the transmission.</font></p>
<p><font face="Arial" size="3">In a star topology, all attached workstations
are wired directly to a central hub, which establishes,
maintains, and breaks connections between them (in the event of
an error). The advantage of a star topology is that it is easy to
isolate a problem node. The disadvantage is that if the hub
fails, the entire system is compromised.</font></p>
<p><font face="Arial" size="3">Twisted-Pair Ethernet (10BASE-T),
based on unshielded twisted pair, and Fiberoptic Ethernet (FOIRL
and 10BASE-FL), based on fiberoptic cable, use the star.</font></p>
<hr color="#008080" size="3"/>
<p align="center"><b><font color="#FF0000" size="5">Ethernet Topologies</font></b></p>
<ol>
<li><font size="4">Fast, reliable throughput speed of
        10&amp;100Mbps.</font></li>
<li><font size="4">Accurate transmission-CSMA/CD access
        method.</font></li>
<li><font size="4">More LAN components match Ethernet
        standards than any other.</font></li>
<li><font size="4">Maximum flexibility-two topologies (bus or
        star) and five kinds of cable (Standard</font></li>
<li><font size="4">or Thin coax; unshielded twisted pair;
        FOIRL or 10BASE-FL fiberoptic).</font></li>
</ol>
<hr color="#008080" size="3"/>
<p align="center"><b><font color="#FF0000" size="5">Switched Ethernet</font></b></p>
<p><font face="Arial" size="3">Switched Ethernet relies on centralized multiport Switches to
provide physical link between multiple LAN segments. Inside each
intelligent S" high-speed circuitry supports wire-speed
virtual connections between all the segments, for maximum
bandwidth allocation on demand. Adding new segments to a switch
increases the aggregate network speed while reducing overall
congestion, so Switched Ethernet provides superior configuration
flexibility. It also gives you an excellent migration path from
10- to 1 00-Mbps Ethernet, since both segments can often operate
via the same Switch.</font></p>
<hr color="#008080" size="3"/>
<p align="center"><b><font color="#FF0000" size="5">Benefits of Switched Ethernet</font></b></p>
<p><font face="Arial" size="3">It is a cost-effective technique for increasing the
overall network throughput and reducing congestion on a 10-Mbps
network. Other than the addition of the switching hub, the
Ethernet network remains the same the same network interface
cards, the same client software, the same LAN cabling.</font></p>
<hr color="#008080" size="3"/>
<p align="center"><b><font color="#FF0000" size="5">100BASE-T (IEEE 802.3u)</font></b></p>
<p><font face="Arial" size="3">100BASE-T retains the familiar CSMA/CD media access technique
used in 1 0-Mbps Ethernet networks. It also supports a broad
range of cabling options: two standards for twisted pair, one for
fiber. 100BASE-TX supports 2-pair Category 5 UTP or Type 1 STP
cable. 100BASE-T4 uses 4-pair Category 3 or 4 cable. And
100BASE-FX allows fiber optic links via duplex multimode fiber
cable.</font></p>
<hr color="#008080" size="3"/>
<p align="center"><b><font color="#FF0000" size="5">Benefits of 100BASE-T</font></b></p>
<p><font face="Arial" size="3">It retains CSMA/CD so existing network
management systems don't need to be rewritten. It can easily be
integrated into existing 10-Mbps Ethernet LANs so your previous
investment is saved.</font></p>
<hr color="#008080" size="3"/>
<p align="center"><b><font color="#FF0000" size="5">100VG (IEEE 802.12)</font></b></p>
<p><font face="Arial" size="3">100VG uses an encoding scheme called Quartet Signaling to
transmit data simultaneously over all four pairs in the network
cable, so it achieves a full tenfold increase in transmission
speeds over 1 OBASE-T. It also replaces the CSMA/CD media access
control protocol with Demand Priority to optimize network
operation and eliminate the overhead of packet collisions and
recovery. Demand Priority works like this: The hub directs all
transmissions, acknowledging higher-priority packet requests
before normal-priority requests. This effectively guarantees
bandwidth to time-sensitive applications like voice, video, and
multimedia applications.</font></p>
<hr color="#008080" size="3"/>
<p align="center"><b><font color="#FF0000" size="5">Benefits of 100VG</font></b></p>
<p><font face="Arial" size="3">It uses a transmission frequency very similar to traditional
Ethernet, and works on any conventional cabling system (Category
3, 4, or 5 UTP, Type 1 STP, and fiber optics) and uses the same
connectors. In addition, 100VG may soon support Token-Ring
networks-a potential advantage over its rival standard 100BASE-T.</font></p>
<hr color="#008080" size="3"/>
<p align="center"><b><font color="#FF0000" size="5">ATM</font></b></p>
<p><font face="Arial" size="3">Asynchronous Transfer Mode (ATM) is a cell-based fast-packet
communication technique that supports data-transfer rates ranging
from sub-Tl speeds (less than 1.544 Mbps) up to 10 Gbps. Like
other packet-switching services (Frame Relay, SMDS), ATM achieves
its high speeds in part by transmitting data in fixed-size cells,
and dispensing with error-correction protocols. Instead, it
relies on the inherent integrity of digital lines to ensure data
integrity.</font></p>
<hr color="#008080" size="3"/>
<p align="center"><font color="#FF0000" size="5"><b>Benefits of ATM</b></font></p>
<p><font face="Arial" size="3">Networks are extremely versatile. An ATM network can
        be treated as a single network, whether it connects
        points in a building or across the country. Its
        fixed-length cell-relay operation, the signaling
        technology of the future, offers more predictable
        performance than variable-length frames. And it can be
        integrated into an existing network as needed, without
        having to upgrade the entire LAN.</font></p>
<hr color="#008080" size="3"/>
<p align="center"><b><font color="#FF0000" size="5">The Advantages of
        100BASE-T</font></b></p>
<ul>
<li><b><font color="#000000" size="4">Carries data, voice, and video at up to 1
        00 Mbps.</font></b></li>
<li><b><font color="#000000" size="4">Supports CSMA/CD protocol so you can use
        your existing networking software.</font></b></li>
<li><b><font color="#000000" size="4">Operates over inexpensive Unshielded
        Twisted-Pair (UTP) cabling.</font></b></li>
<li><b><font color="#000000" size="4">Can be implemented gradually as 10/100Mbps
        adapters and hubs where 1 0- or 1 00-Mbps bandwidth is
        software-selectable.</font></b></li>
<li><b><font color="#000000" size="4">Serves as a stepping stone to even faster
        technologies like ATM.</font></b></li>
</ul>
<hr color="#008080" size="3"/>
<table border="3" bordercolor="#008080" bordercolordark="#008080" bordercolorlight="#008080" cellpadding="7" cellspacing="1" width="697">
<tr>
<td valign="top" width="28%"> </td>
<td valign="top" width="72%"><p align="center"><font size="4"><b>100Base-T (IEEE 802.3u)</b></font></p>
</td>
</tr>
<tr>
<td height="34" valign="top" width="28%"><font size="4">Variations of
        This Standard</font></td>
<td height="34" valign="top" width="72%"><p align="center"><font size="4">100BASE-TX<br/>
        100BASE-T4<br/>
        100BASE-FX</font></p>
</td>
</tr>
<tr>
<td height="37" valign="top" width="28%"><font size="4">Supported Cable
        Type</font></td>
<td height="37" valign="top" width="72%"><font size="4"><b>100BASE-TX:</b>
        5 (2-Pair)</font><p><font size="4">IBM Category Type 1 (2-Pair)</font></p>
<p><font size="4"><b>100BASE-T4:</b> Category 3 or 4 (4-Pair)</font></p>
<p><font size="4"><b>100BASE-FX:</b> Duplex Multimode or Single-Mode
        Fiber</font></p>
</td>
</tr>
<tr>
<td height="34" valign="top" width="28%"><font size="4">Maximum Cable
        Segments</font><p><font size="4">(HUB-TO-NODE)</font></p>
</td>
<td height="34" valign="top" width="72%"><font size="4"><b>100BASE-TX or
        T4: </b>Category 3, 4, or 5-1 00 m</font><p><font size="4">IBM Type 1-100 m</font></p>
<p><font size="4"><b>100BASE-FX: </b>MultimodeFiber-2km,Single-Mode-l0km</font></p>
<p><font size="4">(HUB-TO-NODE)</font></p>
<p><font size="4"><b>100BASE-TX or T4: </b>Category 3, 4, or 5-100 m</font> </p>
<p><font size="4">IBM Type 1-100 m</font></p>
<p><font size="4"><b>100BASE-FX: </b>MultimodeFiber-2km,Single-Mode-l0km</font></p>
</td>
</tr>
<tr>
<td height="43" valign="top" width="28%"><font size="4">Best
        Applications</font></td>
<td height="43" valign="top" width="72%"><font size="4">Backbone
        utilizing Ethernet switches to provide increased
        throughput</font><p><font size="4">Small to medium workgroups using
        applications (i.e.-. CAD, CAM)</font></p>
<p><font size="4">which output huge data files.</font></p>
</td>
</tr>
</table>
<hr color="#008080" size="3"/>
<p>By Erica Roberts <br/>
<br/>
<a name="Gigabit LANs at a Glance"><font color="#FF0000" size="5"><b>Gigabit LANs at
a Glance</b></font></a></p>
<p><br/>
<font size="4">
Gigabit LAN Standards, It Takes Two to Tangle</font> </p>
<p><font face="Arial" size="3">Do 1-Gbit/s versions of 100Base-T and 100VG mean double
trouble for net managers?<br/>
<br/>
Chalk it up to dèjà vu all over again. In 1993, when the IEEE
couldn't make the call on a single 100-Mbit/s LAN transport, it
"decided" on two: 100Base-T and 100VG-AnyLAN. Now the
standards body seems to think that playing doubles is a smart
bet: It's getting ready to define 1-Gbit/s versions of both
high-speed LAN specs.<br/>
</font><font size="4">
<br/>
</font><font face="Arial" size="3">
At first glance, two new gigabit standards look like a good deal.
Both standards have the horsepower needed to turbo-charge
overtired LANs. Both run over copper and fiber (naturally enough,
since they're both based on parts of the ANSI Fibre Channel
spec). Both make it possible to build so-called scalable
Ethernets. And both are relatively cheap. Vendors are already
hinting at prices of $1,500 per node, even though products aren't
slated to ship until next year.<br/>
<br/>
Too bad it all could add up to double trouble for net managers.
Two standards are bound to lead to compatibility questions.
What's more, neither spec guarantees delivery of time-sensitive
voice or video. And for all the big talk about big bandwidth, it
may not be possible to run either standard over copper without
severely cutting speed or limiting the distance between nodes.</font><font size="4"><br/>
<br/>
</font><font face="Arial" size="3">
Given these shortcomings, "it's unlikely that gigabit
Ethernet will ever become a dominant desktop technology,"
says Paul Sherer, vice president of technology development at
3Com Corp. (Santa Clara, Calif.). Nevertheless, it could make its
mark on the backbone--as a fat fire hose for async data.<br/>
<br/>
But does the world really need two 1-Gbit/s Ethernet standards?
(Did it really need two 100-Mbit/s LAN specs?) At this point,
100Base-T and 100VG vendors have too much invested in their
technologies to let an opportunity like this slip by.<br/>
<br/>
They better move fast. ATM is an obvious--and formidable--rival
for the backbone. Prices for ATM (asynchronous transfer mode) hardware are coming down even as we speak, and gigabit ATM gear
is on the way. So even if the IEEE manages to push through both
very-high-speed specs in record time, ATM could still leave them
in the dust.</font><font size="4"><br/>
</font>
<br/>
<font size="4">DIVIDE AND STANDARDIZE</font><br/>
<br/>
<font face="Arial" size="3">
The 802.12 working group has already received an IEEE Project
Authorization Request (PAR) to define a gigabit version of 100VG.
Not surprisingly, the push is headed up by Hewlett-Packard Co.
(HP, Palo Alto, Calif.), the driving force behind the original
AnyLAN. Joining HP are Compaq Computer Corp. (Houston), Texas
Instruments Inc. (Dallas), and the Semiconductor Division of
Motorola Inc. (Austin, Texas). The spec should receive "full
and final approval" by the summer of 1997, according to
Patricia Thaler, chair of the 802.12 working group and principal
engineer for LAN architecture and standards with Hewlett-Packard
Co. Roseville Networks Division (Roseville, Calif.).<br/>
<br/>
Like its 100-Mbit/s predecessor, the gigabit version of 100VG
will handle both Ethernet and token ring frames. It also will
boast several new features, including burst mode and redundant
links between repeaters. The plan now is to define transmissions
of 500 Mbit/s and 1 Gbit/s, with 4 Gbit/s possibly coming in the
future. The group also is working on a full-duplex version of the
spec.<br/>
<br/>
The 802.3 working group is still waiting to receive its PAR for
the gigabit version of 100Base-T. A task force that includes Bay
Networks Inc. (Santa Clara, Calif.), Cisco Systems Inc. (San
Jose, Calif.), Packet Engines Inc. (Union City, Calif.), and 3Com
Corp. (Santa Clara, Calif.) is working on a proposal that will be
presented at the plenary meeting of the 802 LAN/MAN Standards
Committee this month.</font><font size="4"> <br/>
<br/>
</font><font face="Arial" size="3">
"Assuming we get through the politics of the plenary
meeting, we could have a standard out as early as 1998,"
says Brian MacLeod, director of business development for Packet
Engines.<br/>
<br/>
The gigabit 100Base-T group is looking to keep things simple. It
will retain the minimum and maximum frame sizes defined by the
802.3 spec and accommodate full-duplex communications for
point-to-point switched connections. It also may incorporate new
developments like flow control as they are added to 100Base-T.</font><font size="4"><br/>
</font>
<br/>
<font size="4">SHARE TACTICS</font><br/>
<br/>
<font face="Arial" size="3">
Both specs will retain the MAC protocols employed by their
100-Mbit/s forerunners. Thus, gigabit 100Base-T will use CSMA/CD
(carrier-sense multiple access with collision detection), while
gigabit 100VG will go with the more recently developed
demand-priority mechanism.<br/>
<br/>
CSMA/CD and demand priority are both shared-media schemes, which
means sending stations contend for bandwidth. CSMA/CD uses a
back-off algorithm to prevent more than one device from sending
information at a time. Demand priority relies on a round-robin
polling sequence to give each network node the opportunity to
transmit.<br/>
<br/>
Shared-media schemes suffer from a simple shortcoming: As the
number of nodes on the network rises, the bandwidth available to
each drops. To make sure that the new gigabit networks don't wind
up suffering from bandwidth starvation, vendors say they'll
deliver switched versions of their technologies. <br/>
</font>
<br/>
<font size="4">RECYCLED FIBRE</font><br/>
<br/>
<font face="Arial" size="3">
For all the apparent differences between the two gigabit specs,
there's one underlying similarity: Both rely on Fibre Channel's
physical (PHY) layer as their transmission technology. For
gigabit 100Base-T, that means mapping the 802.3 MAC layer to the
PHY, so Ethernet packets can be carried via Fibre Channel
encoding. For gigabit 100VG, both the 802.3 and the 802.5 (token
ring) MAC layers must be mapped.<br/>
</font><font face="Arial" size="4">
<br/>
</font><font face="Arial" size="3">
"The Fibre Channel PHY is a good piece of engineering,"
comments Thaler. "Why reinvent it?" Recycling Fibre
Channel in this way reduces time to market. What's more, it
should help bring down prices. Gigabit gear will use Fibre
Channel transceivers; as silicon shipments climb, chip prices
should plummet. It's still very early in the game, but gigabit
over copper could cost between $1,500 and $2,000 per switched
connection (which includes the price of the adapter and the
switch port). Fiber is expected to cost $3,000 to $4,000.<br/>
</font><font face="Arial" size="4">
<br/>
</font><font face="Arial" size="3">
Fibre Channel aficionados view the gigabit vendors' recycling
plans as a tacit acknowledgment of the merits of their
technology. They also argue that if a little bit of Fibre Channel
is good, all of it would be even better. Scot Ruple, director of
product marketing for Emulex Corp. (Costa Mesa, Calif.), expects
to see Fibre Channel take off for storage and backbone
applications: "Gigabit Ethernet is our chance to slip
through the back door and onto the LAN." Thaler disagrees,
saying Fibre Channel's MAC layer would add too much overhead to
LAN applications.<br/>
</font>
<br/>
<font size="4">THE CABLING CATCH</font><br/>
<br/>
<font face="Arial" size="3">Fiber</font><font face="Arial" size="3"> Channel's PHY layer defines transmissions over fiber and
copper. Fiber is more expensive, but it shouldn't have any
trouble handling gigabit data. Multimode fiber should be able to
carry gigabit transmissions to 500 meters; with single-mode
fiber, that distance should reach 2 kilometers.<br/>
<br/>
But net managers who expect to see those sorts of speeds and
distances over copper are going to be disappointed. It's a
question of physics. Electromagnetic radiation increases in
proportion to the speed at which data is carried and the length
of the cable. In order to stay within the EMI limits set by the
FCC for UTP (unshielded twisted pair), net managers must reduce
data rates or shorten cabling runs (or both).<br/>
<br/>
That leads to two immediate questions: How slow? How short?</font><br/>
<br/>
<font face="Arial" size="3">
Proponents of gigabit 100Base-T say they can hit 1 Gbit/s over
UTP by locating the switch within 50 meters of the end-station.
And that's an optimistic estimate. Some vendors think things
could get even tighter. Remember, gigabit 100Base-T is a CSMA/CD
scheme. As the speed of a CSMA/CD network increases, so does the
likelihood of collisions. In effect, this means that the length
of the cabling runs are inversely proportional to the bit rate.
That's why some gigabit 100Base-T backers are saying link
distances will likely be held to 25 meters on Category 5 UTP.<br/>
<br/>
Even with these reduced runs, getting 1 Gbit/s over UTP is going
to be a good trick. The Fibre Channel spec is silent when it
comes to unshielded cabling. It does, however, call for a top
speed of 100 Mbit/s over shielded twisted pair.</font><br/>
<br/>
<font face="Arial" size="3">
Packet Engine's MacLeod has an answer: "Technology has moved
ahead since the Fibre Channel specs were ratified." He's
hopeful that new connectors and transceivers will make it
possible to push Ethernet frames at higher speeds over longer
distances on Category 5.<br/>
<br/>
Where does all this leave gigabit 100VG? Its backers say they can
hit 500 Mbit/s over 100-meter UTP runs. But to do so, they're
developing a new physical layer and transmission scheme expressly
for four-pair UTP. They'll use the Fibre Channel PHY strictly for
fiber. HP recently submitted a new physical layer to the 802.12
working group.<br/>
</font>
<br/>
<font size="4">BACKBONE BONUS</font><br/>
<br/>
<font face="Arial" size="3">
Questions about copper are one of the reasons that vendors are
talking up gigabit 100Base-T and 100VG as high-speed fiber links
for point-to-point applications. On the backbone, the gigabit
standards are being pitched as an alternative to FDDI or ATM.
"As fast Ethernet picks up momentum over the next few years,
we're going to need bigger pipes to servers and for interswitch
connections," says Peter Tarrant, vice president of product
management at Bay Networks.<br/>
<br/>
What's more, since both gigabit specs field conventional Ethernet
frames, they give net managers a way to build
"scalable" networks. These configurations run at
different speeds in different spots but use the same frame format
end to end. For instance, a company could run 10-Mbit/s Ethernet
to the desktop, 100-Mbit/s 100Base-T between departments, and
gigabit 100Base-T on the backbone--all without having to install
pricey, performance-impairing equipment that converts between one
format and another. This one-frame-fits-all approach isn't
possible with FDDI or Fibre Channel.<br/>
</font>
<br/>
<font size="4">THE ATM ARGUMENT</font><br/>
<br/>
<font face="Arial" size="3">
Backers of the new gigabit LANs also argue that their standards
are out ahead of ATM when it comes to price and performance. They
point out that a 1-Gbit/s fiber connection costs $3,000 to $4,000
per switch port (including adapter). A 622-Mbit/s ATM connection,
in contrast, today costs $15,000 per port (not counting the
adapter).<br/>
<br/>
But this is a good case of the figures not telling the entire
story. Only one LAN vendor offers 622-Mbit/s ATM. Prices are sure
to fall as competition works its market magic.<br/>
<br/>
"By the time gigabit Ethernet arrives many of the ATM issues
will be resolved," says Esmeralda Silva, LAN analyst with
International Data Corp. (Framingham, Mass.). "ATM prices
are already down. I see them dropping even more over the next few
quarters, with bigger savings in 1997."<br/>
</font>
<br/>
<font face="Arial" size="3">
And ATM has a very strong argument in its favor--advanced
multimedia capabilities. Gigabit 100Base-T does nothing to
guarantee the delivery of time-sensitive traffic. Gigabit 100VG
lets net managers assign two priority levels to traffic. But if
the network gets busy there's still a good chance that voice and
video won't arrive on time.<br/>
</font>
<br/>
<font size="4">HOLD THE LINE</font><br/>
<br/>
<font face="Arial" size="3">
Given all of the outstanding issues, it's not surprising that
some net managers are taking a wait-and-see attitude. "With
gigabit 100Base-T, it's going to be very interesting to see what
the actual performance is," comments John Scoggin,
supervisor of network operation with Delmarva Power and Light
(Newark, Del.). He's been testing gigabit Fiber Channel but
notes, "We're not getting anywhere close to a gigabit over
it."<br/>
<br/>
Scoggin knows that it's easy to play a waiting game when he
doesn't need the throughput. But net managers won't be able to
put off their decisions forever. "Right now I can't imagine
what I'd need gigabit Ethernet for. But 10 years ago I couldn't imagine networks running faster than T1."</font><br/>
<br/>
<a href="#ARC Electronics">TOP OF PAGE</a></p>
<hr color="#008080" size="3"/>
<h1 align="center"><a name="ARC Electronics"><font color="#FF0000" face="Cooper Md BT" size="7"><b>ARC Electronics</b></font></a></h1>
<p align="center"><font face="Helvetica" size="2"><b>301-924-7400 EXT 25
/ 301-924-7400 EXT 25 / Fax 281-392-3657</b></font></p>
<p align="center"><font face="Helvetica" size="2"><b>E-Mail us at
- </b></font><a href="mailto:arc@arcelect.com"><font face="Helvetica" size="2"><b>arc@arcelect.com </b></font></a><font face="Helvetica" size="2"><b>... </b></font><a href="http://arcelect.com/"><font face="Helvetica" size="2"><b>Home Page</b></font></a></p>
</body>
</html>