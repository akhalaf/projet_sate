<!DOCTYPE html>
<html>
<head>
<!--Aiur header-->
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport"/>
<meta content="Create a more open world. Aiursoft is focusing on open platform and open communication. Free training, tools, and community to help you grow your skills, career, or business." name="description"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<link href="https://ui.aiursoft.com/favicon.ico" rel="icon" type="image/x-icon"/>
<!--Project header-->
<title>Aiursoft</title>
<link href="https://ui.aiursoft.com/dist/AiurMarket.min.css" rel="stylesheet"/>
<link href="/open-search" rel="search" title="Aiursoft" type="application/opensearchdescription+xml"/>
<link href="/css/site.css" rel="stylesheet"/>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-light bg-white fixed-top">
<div class="container">
<a class="navbar-brand" href="/">
                    Aiursoft
                </a>
<button aria-controls="aiursoft-navbar" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#aiursoft-navbar" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="aiursoft-navbar">
<ul class="navbar-nav mr-auto">
<li class="nav-item">
<a class="nav-link" href="https://www.kahla.app">Kahla</a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://drive.aiursoft.com">AiurDrive</a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://wrap.aiursoft.com">Wrap</a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://tracer.aiursoft.com">Tracer</a>
</li>
<li class="nav-item dropdown">
<a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" id="dropdown106">
                    Support
                </a>
<div aria-labelledby="106" class="dropdown-menu">
<a class="dropdown-item" href="https://wiki.aiursoft.com">Wiki</a>
<a class="dropdown-item" href="/docs/terms">Terms</a>
<a class="dropdown-item" href="mailto:officials@aiursoft.com">Contact</a>
</div>
</li>
</ul>
<div class="form-inline">
<ul class="navbar-nav mr-auto">
<li class="nav-item active">
<a class="nav-link" href="/Auth/GoAuth">
                                        Sign in
                                    </a>
</li>
<li class="nav-item active">
<a class="nav-link" href="/Auth/GoRegister">
                                        Sign up
                                    </a>
</li>
</ul>
</div>
</div>
</div>
</nav>
<header class="masthead text-white text-center www-back" style="background: url('/img/back.svg') #184685 no-repeat center center; background-size: cover;">
<div class="overlay"></div>
<div class="container">
<div class="row">
<div class="col-xl-9 mx-auto">
<h1 class="mb-5 www-shadow">Aiursoft Search</h1>
</div>
<div class="col-lg-9 col-md-10 col-xl-11 mx-auto">
<form action="/search" class="form-row" method="get">
<div class="col-12 col-md-9 mb-2 mb-md-0">
<input autofocus="" autosave="www-search-input" class="form-control form-control-lg" list="auto-complete" name="q" pattern=".*\S+.*" placeholder="Search the web..." required="" results="7" title="This field is required" type="search"/>
<datalist id="auto-complete"></datalist>
</div>
<div class="col-12 col-md-3">
<button class="btn btn-block btn-lg btn-primary" type="submit">
<i class="fa fa-search"></i>
</button>
</div>
</form>
</div>
</div>
</div>
</header>
<footer class="footer bg-light">
<div class="container">
<div class="row">
<div class="col-lg-8 h-100 text-center text-lg-left my-auto">
<ul class="list-inline mb-2">
<li class="list-inline-item"><a href="https://www.aiursoft.com">Home</a></li>
<li class="list-inline-item"><a href="https://wiki.aiursoft.com">Wiki</a></li>
<li class="list-inline-item"><a href="https://status.aiursoft.com">Status</a></li>
<li class="list-inline-item"><a href="https://developer.aiursoft.com">Developer</a></li>
<li class="list-inline-item"><a href="https://github.com/AiursoftWeb">Company</a></li>
<li class="list-inline-item"><a href="https://github.com/AiursoftWeb">GitHub</a></li>
<li class="list-inline-item"><a href="https://www.aiursoft.com/docs/terms">Terms</a></li>
<li class="list-inline-item"><a data-language-change-link="" href="https://gateway.aiursoft.com/Api/SetLang">Language</a></li>
</ul>
<p class="text-muted small mb-4 mb-lg-0">
                        © 2021 - Aiursoft
                        
                    </p>
</div>
<div class="col-lg-4 h-100 text-center text-lg-right my-auto">
<ul class="list-inline mb-0">
<li class="list-inline-item mr-3">
<a href="https://github.com/AiursoftWeb/Infrastructures">
<i class="fab fa-github fa-2x "></i>
</a>
</li>
</ul>
</div>
</div>
</div>
</footer>
<!--Aiur scroll to top-->
<a class="aiur-scroll-to-top rounded" href="#page-top"><i class="fa fa-angle-up"></i></a>
<script>
    window.addEventListener("load", function () { $(document).scroll(function () { var scrollDistance = $(this).scrollTop(); if (scrollDistance > 100) { $(".aiur-scroll-to-top").fadeIn() } else { $(".aiur-scroll-to-top").fadeOut() } }); $("a.aiur-scroll-to-top").off("click"); $("a.aiur-scroll-to-top").on("click", function (event) { $("html, body").animate({ scrollTop: 0 }, "easeInOutExpo"); event.preventDefault() }) });
</script>
<!--End Aiur scroll to top-->
<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="exampleModal" role="dialog" tabindex="-1">
<div class="modal-dialog" role="document">
<form action="/Home/Logoff" class="modal-content" method="post">
<div class="modal-header">
<h5 class="modal-title">Ready to Leave?</h5>
<button aria-label="Close" class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">×</span>
</button>
</div>
<div class="modal-body">Select 'Logout' below if you are ready to end your current session.</div>
<div class="modal-footer">
<button class="btn btn-secondary" data-dismiss="modal" type="button">Cancel</button>
<input class="btn btn-primary" data-disable-with="Logging out..." type="submit" value="Logout"/>
</div>
</form>
</div>
</div>
<script src="https://ui.aiursoft.com/dist/AiurMarket.min.js"></script>
<script src="/js/site.js"></script>
</body>
</html>