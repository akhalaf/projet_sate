<!DOCTYPE html>
<!-- tag: 9.0.0 | ref: 41ed95b1 --><html>
<head>
<meta charset="utf-8"/>
<title>Warlicks Baptist Church | MAIN</title>
<meta content="This is a page on the Warlicks Baptist Church website. Check it out!" name="description"/>
<meta content="Warlicks Baptist Church | MAIN" property="og:title"/>
<meta content="This is a page on the Warlicks Baptist Church website. Check it out!" property="og:description"/>
<meta content="website" property="og:type"/>
<meta content="https://warlicksbc.org/main" property="og:url"/>
<meta content="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/b42ccb85-9bfc-4b88-964d-2db4a9bfdcbe.jpg" property="og:image"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Warlicks Baptist Church | MAIN" name="twitter:title"/>
<meta content="This is a page on the Warlicks Baptist Church website. Check it out!" name="twitter:description"/>
<meta content="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/b42ccb85-9bfc-4b88-964d-2db4a9bfdcbe.jpg" name="twitter:image"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<link href="/zero.ico" rel="shortcut icon" type="image/x-icon"/>
<link data_turbolinks_track="true" debug="false" href="https://cdn.cloversites.com/production/websites/application-d0025587cf10b3a6315a3e7068e76a2742d5b05c161d7dab6aa5fcd98922716a.css" media="all" rel="stylesheet"/>
<link debug="false" href="https://cdn.cloversites.com/production/websites/designs/boulevard/base-608652b4a2c507e531d776c263fdaa9712b771835def7bb37f052499ac7675f9.css" media="all" rel="stylesheet"/>
<script class="clover" data_turbolinks_track="true" debug="false" src="https://cdn.cloversites.com/production/websites/application-26b3b57df4f121c064cadfa3c741ca0322c53ca345ec99fffde6beb349bd1168.js"></script>
<script class="clover" data-turbolinks-track="true" debug="false" src="https://cdn.cloversites.com/production/websites/designs/boulevard/base-c86f5b193302a78c6ea6d52aba98cb9b4c87096497c69cc31e81235a605ca436.js"></script>
<script>
//<![CDATA[

      var __REACT_ON_RAILS_EVENT_HANDLERS_RAN_ONCE__ = true

//]]>
</script>
<link class="clover" href="https://fonts.googleapis.com/css?family=Oswald:400,700&amp;subset=latin,latin-ext" media="screen" rel="stylesheet"/>
<link class="clover" href="https://assets.cloversites.com/fonts/picker/clearsans/clearsansregular.css" media="screen" rel="stylesheet"/>
<link class="clover" href="https://fonts.googleapis.com/css?family=Oswald:400,300&amp;subset=latin,latin-ext" media="screen" rel="stylesheet"/>
<link href="https://cdn.cloversites.com/_user_generated_stylesheets/published_colors_d0c26f6c-fa5c-4e38-87ee-01fdb73e4849_89368d439878d4485edca5af10df5c25.css" id="color-css" media="all" rel="stylesheet"/>
<link href="https://cdn.cloversites.com/_user_generated_stylesheets/published_fonts_d0c26f6c-fa5c-4e38-87ee-01fdb73e4849_0c4fb8ad3301295af68831a73263113f.css" id="font-css" media="all" rel="stylesheet"/>
<link href="https://cdn.cloversites.com/_user_generated_stylesheets/published_tweaks_d0c26f6c-fa5c-4e38-87ee-01fdb73e4849_dfd1b10a1fcdab398817d337d9985e47.css" id="tweak-css" media="all" rel="stylesheet"/>
<meta content="authenticity_token" name="csrf-param"/>
<meta content="Z4m1Ap0uIzQAlkm9wOdoLHKqozS53VDmngai3vgh7vUpP7PYss+z1X5CdSVVzoqjJ9C9Ah35WpGEaRyI6wx+JQ==" name="csrf-token"/>
<script data-turbolinks-eval="false">
//<![CDATA[


      window.$ = window.$c;

      $(window).load(function() {
        window.initialLoad = true;
      });

      $(document).ready(function() {
        $('body').addClass('browser-' + bowser.name.toLowerCase().replace(' ', '-'))
        if (bowser.ios) $('body').addClass('os-ios')
        if (navigator.platform === 'Win32') $('body').addClass('os-windows')

        Sites.start({
          preview: false,
          greenhouse_preview: false,
          environment: "production",
          site_name: "warlicksbaptistchurch",
          site_uuid: "d0c26f6c-fa5c-4e38-87ee-01fdb73e4849",
          content_prefix: "d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849",
          ssl: true,
          main_navigation: "",
          mobile_navigation: "side",
          top_gallery: "",
          navigation_settings: {"mobile":"side"},
          gallery_settings: {},
          events_settings: {},
          forms_settings: {},
          s3_bucket_name: "media.cloversites.com",
          base_domain: "cloversites.com",
          forms_domain: "https://forms.ministryforms.net",
          form_builder_embed_url: "https://forms.ministryforms.net/scripts/mb.formbuilder.embed.js",
          packs_graph: {"runtime":["https://cdn.cloversites.com/builds/packs/js/runtime-64630796b8e08f0f1f1d.js",[]],"1":["https://cdn.cloversites.com/builds/packs/js/1-20b5d0cbcf243d6f07ba.chunk.js",[]],"2":["https://cdn.cloversites.com/builds/packs/js/2-73e4acda74be08ab4575.chunk.js",[]],"7":["https://cdn.cloversites.com/builds/packs/js/7-e3524d72d0b51c0ba8f7.chunk.js",[]],"media":["https://cdn.cloversites.com/builds/packs/js/media-4cfe0711a4a82d49ea8c.chunk.js",["runtime","1","2"]],"prayer":["https://cdn.cloversites.com/builds/packs/js/prayer-71b83e2d8de76628928d.chunk.js",["runtime","1","7"]],"small-groups":["https://cdn.cloversites.com/builds/packs/js/small-groups-1a35a953ae0dfc789458.chunk.js",["runtime","1","2"]]}
        })

        $(document).trigger('page:load');
      });

      $(document).on('page:load', function(e) {
        e.objectId = '785622'
        e.objectType = 'page'
        e.title = 'MAIN'

        if(window.loaded) {
          Sites.vent.trigger("pageunload", e)
        }

        Sites.vent.trigger("pageload", e);
        // window.loaded is also used to ensure greenhouse loaded
        // and to retry if it failed.
        window.loaded = true;
      });

      $(document).on("page:before-change", function(e) {
        Sites.vent.trigger("pagechange", e);
      })

      $(document).on("page:before-unload", function(e) {
        window.loaded = false
        Sites.vent.trigger("pageunload", e);
      })


//]]>
</script>
<!-- Begin Custom Head Code -->
</head><body><div align="center" class="custom-head-code clover">
<a class="cloverlinks" data-category="link" data-detail="855296" data-location="existing" href="/covid-19-updates" target="_self">
<marquee behavior="alternate" bgcolor="#a53744 " direction="left" height:="30" loop="2000" scrollamount="5" scrolldelay="2" width="100%">
<a class="cloverlinks" data-category="link" data-detail="855296" data-location="existing" href="/covid-19-updates" target="_self">
<span style="font-size: 30px;color:#FFFFFF">
 COVID-19 UPDATES </span></a></marquee>
</a></div>
<!-- End Custom Head Code -->
<script data-turbolinks-eval="false">
//<![CDATA[

      window.$ = window.$c;

//]]>
</script>
<!-- site content -->
<div id="wrapper">
<a class="skip-link" href="#sections">Skip to main content</a>
<!-- mobile nav -->
<div id="mobile-nav-button-container">
<button class="nav-menu-button sites-button" id="mobile-nav-button">
<span class="text">Menu</span>
<span class="mobile-nav-icon">
<span class="first"></span>
<span class="middle"></span>
<span class="last"></span>
</span>
</button>
</div>
<div id="mobile-navigation">
<nav class="main-navigation">
<button class="nav-menu-button">
<span class="text">Menu</span>
<span class="mobile-nav-icon">
<span class="first"></span>
<span class="middle"></span>
<span class="last"></span>
</span>
</button>
<ul>
<li class="selected first landing current">
<a href="/main">
<span>MAIN</span>
</a>
</li>
<li class=" has-sub">
<a href="/about-us/what-to-expect">
<span>About Us</span>
<i></i>
</a>
<ul class="sub-navigation">
<li class="">
<a href="/about-us/what-to-expect"><span>What to Expect</span></a>
</li>
<li class="">
<a href="/about-us/what-we-believe"><span>What We Believe</span></a>
</li>
<li class="">
<a href="/about-us/staff"><span>Staff</span></a>
</li>
</ul>
</li>
<li class=" landing">
<a href="/ministries">
<span>MINISTRIES</span>
</a>
</li>
<li class=" landing">
<a href="/church-photos">
<span>Church Photos</span>
</a>
</li>
<li class=" landing">
<a href="/classes-events">
<span>Classes &amp; Events</span>
</a>
</li>
<li class=" last has-sub">
<a href="/sermons-tithing/sermons">
<span>SERMONS &amp; TITHING</span>
<i></i>
</a>
<ul class="sub-navigation">
<li class="">
<a href="/sermons-tithing/sermons"><span>Sermons</span></a>
</li>
<li class="">
<a href="/sermons-tithing/sermon-notes"><span>Sermon Notes</span></a>
</li>
<li class="">
<a href="/sermons-tithing/donate"><span>Donate</span></a>
</li>
</ul>
</li>
</ul>
</nav>
</div>
<!-- page content -->
<section class="clearfix" id="main-content">
<header class="header site-section clearfix first-section-subpalette1" data-category="header" data-id="3267214">
<div class="content-wrapper">
<div class="branding media-content">
<div class="photo-content editable photo-0 " data-category="photo" data-height="256" data-id="13647246" data-width="498">
<div class="aspect-helper" style="padding-top:51.40562248995983%"></div>
<div class="photo-container">
<a data-category="link" data-detail="785622" data-location="existing" data-url="/main" href="/main">
<img border="0" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/site-images/949ce132-3463-4011-8bfd-1b5188ac8769.png, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/site-images/949ce132-3463-4011-8bfd-1b5188ac8769@2x.png 2x"/>
</a></div>
</div>
</div>
<nav class="main-navigation clearfix" id="main-navigation">
<ul>
<li class="selected landing first current">
<a href="/main"><span>MAIN</span></a>
</li>
<li class=" has-sub">
<a href="/about-us/what-to-expect"><span>About Us</span></a>
<ul class="sub-navigation">
<li>
<a href="/about-us/what-to-expect"><span>What to Expect</span></a>
</li>
<li>
<a href="/about-us/what-we-believe"><span>What We Believe</span></a>
</li>
<li>
<a href="/about-us/staff"><span>Staff</span></a>
</li>
</ul>
<i></i>
</li>
<li class="landing">
<a href="/ministries"><span>MINISTRIES</span></a>
</li>
<li class="landing">
<a href="/church-photos"><span>Church Photos</span></a>
</li>
<li class="landing">
<a href="/classes-events"><span>Classes &amp; Events</span></a>
</li>
<li class=" last has-sub">
<a href="/sermons-tithing/sermons"><span>SERMONS &amp; TITHING</span></a>
<ul class="sub-navigation">
<li>
<a href="/sermons-tithing/sermons"><span>Sermons</span></a>
</li>
<li>
<a href="/sermons-tithing/sermon-notes"><span>Sermon Notes</span></a>
</li>
<li>
<a href="/sermons-tithing/donate"><span>Donate</span></a>
</li>
</ul>
<i></i>
</li>
</ul>
</nav>
</div></header>
<main id="sections" role="main">
<article class="site-section subpalette1 gallery editable gallery-layout first last" data-category="gallery" data-id="3267215" id="gallery-3267215">
<div class="bg-helper">
<div class="bg-opacity" style="opacity: 0.0"></div>
</div>
<div class="content-wrapper clearfix">
<div class="slides-container">
<div class="slick-container" data-category="gallery" data-id="3267215" data-settings='{"gallery":{"max_width":2350,"max_height":1198},"background":{"opacity":1,"fadeMode":"none","blendMode":"none","photoOption":"fill"}}' data-slick-settings='{"autoplay":false,"autoplaySpeed":4500.0,"dots":true,"arrows":true,"infinite":true,"fade":false,"speed":500.0}'>
<ul class="slick">
<li>
<a data-category="link" data-detail="https://www.rightnowmedia.org/Content/Series/265337" data-location="external" data-url="https://www.rightnowmedia.org/Content/Series/265337" href="https://www.rightnowmedia.org/Content/Series/265337" target="_blank">
<picture class="slide-fit" data-id="17244073" data-image='{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/b42ccb85-9bfc-4b88-964d-2db4a9bfdcbe.jpg","thumb":{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/thumb_b42ccb85-9bfc-4b88-964d-2db4a9bfdcbe.jpg"}}'>
<source media="(min-width: 769px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/b42ccb85-9bfc-4b88-964d-2db4a9bfdcbe.jpg, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/b42ccb85-9bfc-4b88-964d-2db4a9bfdcbe@2x.jpg 2x"/>
<source media="(min-width: 600px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_b42ccb85-9bfc-4b88-964d-2db4a9bfdcbe.jpg, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_b42ccb85-9bfc-4b88-964d-2db4a9bfdcbe@2x.jpg 2x"/>
<source srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_b42ccb85-9bfc-4b88-964d-2db4a9bfdcbe.jpg"/>
<img class="image"/>
</picture></a></li>
<li>
<a data-category="link" data-detail="https://drive.google.com/file/d/1LNOz3Bt11jHuBEEBkMm-X-4oHv9ddJvp/view?usp=sharing" data-location="external" data-url="https://drive.google.com/file/d/1LNOz3Bt11jHuBEEBkMm-X-4oHv9ddJvp/view?usp=sharing" href="https://drive.google.com/file/d/1LNOz3Bt11jHuBEEBkMm-X-4oHv9ddJvp/view?usp=sharing" target="_blank">
<picture class="slide-fit" data-id="16049178" data-image='{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/512ec2e6-f2df-4f1c-a1b1-2e8698ce09a5.png","thumb":{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/thumb_512ec2e6-f2df-4f1c-a1b1-2e8698ce09a5.png"}}'>
<source media="(min-width: 769px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/512ec2e6-f2df-4f1c-a1b1-2e8698ce09a5.png, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/512ec2e6-f2df-4f1c-a1b1-2e8698ce09a5@2x.png 2x"/>
<source media="(min-width: 600px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_512ec2e6-f2df-4f1c-a1b1-2e8698ce09a5.png, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_512ec2e6-f2df-4f1c-a1b1-2e8698ce09a5@2x.png 2x"/>
<source srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_512ec2e6-f2df-4f1c-a1b1-2e8698ce09a5.png"/>
<img class="image"/>
</picture></a></li>
<li>
<a data-category="link" data-detail="https://www.youtube.com/watch?v=3QCkBL2DfVg" data-location="external" data-url="https://www.youtube.com/watch?v=3QCkBL2DfVg" href="https://www.youtube.com/watch?v=3QCkBL2DfVg" target="_blank">
<picture class="slide-fit" data-id="15751918" data-image='{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/1b96fbfc-e3e1-4712-afeb-094bfcf51a0b.png","thumb":{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/thumb_1b96fbfc-e3e1-4712-afeb-094bfcf51a0b.png"}}'>
<source media="(min-width: 769px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/1b96fbfc-e3e1-4712-afeb-094bfcf51a0b.png, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/1b96fbfc-e3e1-4712-afeb-094bfcf51a0b@2x.png 2x"/>
<source media="(min-width: 600px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_1b96fbfc-e3e1-4712-afeb-094bfcf51a0b.png, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_1b96fbfc-e3e1-4712-afeb-094bfcf51a0b@2x.png 2x"/>
<source srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_1b96fbfc-e3e1-4712-afeb-094bfcf51a0b.png"/>
<img class="image"/>
</picture></a></li>
<li>
<picture data-id="14143435" data-image='{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/c4195bf1-090d-4281-b6b5-56ffe4c32811.png","thumb":{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/thumb_c4195bf1-090d-4281-b6b5-56ffe4c32811.png"}}'>
<source media="(min-width: 769px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/c4195bf1-090d-4281-b6b5-56ffe4c32811.png, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/c4195bf1-090d-4281-b6b5-56ffe4c32811@2x.png 2x"/>
<source media="(min-width: 600px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_c4195bf1-090d-4281-b6b5-56ffe4c32811.png, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_c4195bf1-090d-4281-b6b5-56ffe4c32811@2x.png 2x"/>
<source srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_c4195bf1-090d-4281-b6b5-56ffe4c32811.png"/>
<img class="image"/>
</picture></li>
<li>
<a data-category="link" data-detail="809680" data-location="existing" data-url="/sermons-tithing/donate" href="/sermons-tithing/donate">
<picture data-id="14750436" data-image='{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/955afd8c-2c0c-494e-a36e-70f639343a9f.jpg","thumb":{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/thumb_955afd8c-2c0c-494e-a36e-70f639343a9f.jpg"}}'>
<source media="(min-width: 769px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/955afd8c-2c0c-494e-a36e-70f639343a9f.jpg, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/955afd8c-2c0c-494e-a36e-70f639343a9f@2x.jpg 2x"/>
<source media="(min-width: 600px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_955afd8c-2c0c-494e-a36e-70f639343a9f.jpg, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_955afd8c-2c0c-494e-a36e-70f639343a9f@2x.jpg 2x"/>
<source srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_955afd8c-2c0c-494e-a36e-70f639343a9f.jpg"/>
<img class="image"/>
</picture></a></li>
<li>
<picture class="slide-fit" data-id="14994935" data-image='{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/8ed9edd9-8159-4689-9a1d-8878c64f2995.jpg","thumb":{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/thumb_8ed9edd9-8159-4689-9a1d-8878c64f2995.jpg"}}'>
<source media="(min-width: 769px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/8ed9edd9-8159-4689-9a1d-8878c64f2995.jpg, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/8ed9edd9-8159-4689-9a1d-8878c64f2995@2x.jpg 2x"/>
<source media="(min-width: 600px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_8ed9edd9-8159-4689-9a1d-8878c64f2995.jpg, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_8ed9edd9-8159-4689-9a1d-8878c64f2995@2x.jpg 2x"/>
<source srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_8ed9edd9-8159-4689-9a1d-8878c64f2995.jpg"/>
<img class="image"/>
</picture></li>
<li>
<picture class="slide-fit" data-id="14994906" data-image='{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/691f0351-55bd-4464-8c37-2c94e9d56ca1.jpg","thumb":{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/thumb_691f0351-55bd-4464-8c37-2c94e9d56ca1.jpg"}}'>
<source media="(min-width: 769px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/691f0351-55bd-4464-8c37-2c94e9d56ca1.jpg, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/691f0351-55bd-4464-8c37-2c94e9d56ca1@2x.jpg 2x"/>
<source media="(min-width: 600px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_691f0351-55bd-4464-8c37-2c94e9d56ca1.jpg, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_691f0351-55bd-4464-8c37-2c94e9d56ca1@2x.jpg 2x"/>
<source srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_691f0351-55bd-4464-8c37-2c94e9d56ca1.jpg"/>
<img class="image"/>
</picture></li>
<li>
<picture class="slide-fit" data-id="14994907" data-image='{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/687473c5-fbb9-43c6-afa4-1b480cb0d9e4.jpg","thumb":{"url":"https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/thumb_687473c5-fbb9-43c6-afa4-1b480cb0d9e4.jpg"}}'>
<source media="(min-width: 769px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/687473c5-fbb9-43c6-afa4-1b480cb0d9e4.jpg, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/687473c5-fbb9-43c6-afa4-1b480cb0d9e4@2x.jpg 2x"/>
<source media="(min-width: 600px)" srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_687473c5-fbb9-43c6-afa4-1b480cb0d9e4.jpg, https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_687473c5-fbb9-43c6-afa4-1b480cb0d9e4@2x.jpg 2x"/>
<source srcset="https://s3.amazonaws.com/media.cloversites.com/d0/d0c26f6c-fa5c-4e38-87ee-01fdb73e4849/gallery/slides/mobile_687473c5-fbb9-43c6-afa4-1b480cb0d9e4.jpg"/>
<img class="image"/>
</picture></li>
</ul></div>
</div>
</div>
</article></main>
<footer class="site-section subpalette1 footer editable " data-category="footer" data-id="3267213" id="footer-3267213">
<div class="content-wrapper">
<div class="text-content text-0 editable" data-category="text" data-id="13647245"><div><p>Warlicks Baptist Church</p><p style="font-size: 0.7813em;">2684 Warlicks Church Road</p><p style="font-size: 0.7813em;">Connelly Springs, NC 28612</p></div></div>
</div>
</footer>
<div class="clearfix" id="made-by-clover">
<a href="https://www.cloversites.com" target="_blank">
<span id="clover-tagline"><span>church websites by clover</span></span>
<svg height="18" id="clover-icon" version="1.1" width="18" xmlns="http://www.w3.org/2000/svg">
<path d="M8.084,7.325c-4.01-6.091-7.848-1.756-5.221,0.683L8.084,7.325z M2.863,8.014c-2.627,2.438,1.211,6.774,5.221,0.683 L2.863,8.014z M11.904,2.942L11.904,2.942l-0.01,0.024l0.03-0.044C11.081-0.524,5.326,0.562,8.719,7L11.904,2.942z M9.912,7.674 c7.405-0.349,5.491-5.763,2.013-4.752l-0.087,0.176L9.912,7.674z M11.904,13.057L8.719,8.999 c-3.393,6.438,2.362,7.525,3.206,4.078l-0.021-0.031V13.057z M9.913,8.325l1.991,4.681v0.028l0.021,0.043 C15.402,14.086,17.316,8.674,9.913,8.325z"></path>
</svg>
</a>
</div>
</section>
</div>
<script>
//<![CDATA[
window.gon={};gon.parent_slug="main";gon.page_slug=null;gon.current_page_id=785622;gon.design_name="boulevard";
//]]>
</script>
<script>
//<![CDATA[

      // initialize all slick slideshows on ready
      $(document).ready(function() {
        SlickInterface.reinitializeSlideshows()
      })

      $(document).on('page:before-unload', function() {
        // Prevent slideshow state sometimes transferring to another page in Bloom.
        SlickInterface.pauseSlideshows()
      })

//]]>
</script>
</body></html>
