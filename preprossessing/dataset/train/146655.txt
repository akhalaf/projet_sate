<!DOCTYPE html>
<html lang="en-gb" xml:lang="en-gb">
<!--Begin head-->
<head>
<meta charset="utf-8"/>
<base href="https://www.archaeologica.org/"/>
<meta content="Super User" name="author"/>
<meta content="Your source on the web for daily 
archaeological news and information." name="description"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>Archaeologica - Home</title>
<link href="/templates/ArchaeologicaTemplate/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/media/jui/css/bootstrap.min.css?5e3d4e90b127c6c2276a2276793b7d7c" rel="stylesheet"/>
<link href="/media/jui/css/bootstrap-responsive.min.css?5e3d4e90b127c6c2276a2276793b7d7c" rel="stylesheet"/>
<link href="/media/jui/css/bootstrap-extended.css?5e3d4e90b127c6c2276a2276793b7d7c" rel="stylesheet"/>
<link href="/templates/ArchaeologicaTemplate/css/styles.css" rel="stylesheet"/>
<link href="/templates/ArchaeologicaTemplate/css/home-styles.css" rel="stylesheet"/>
<style>
</style>
<script src="/media/jui/js/jquery.min.js?5e3d4e90b127c6c2276a2276793b7d7c"></script>
<script src="/media/jui/js/jquery-noconflict.js?5e3d4e90b127c6c2276a2276793b7d7c"></script>
<script src="/media/jui/js/jquery-migrate.min.js?5e3d4e90b127c6c2276a2276793b7d7c"></script>
<script src="/media/system/js/caption.js?5e3d4e90b127c6c2276a2276793b7d7c"></script>
<script src="/media/jui/js/bootstrap.min.js?5e3d4e90b127c6c2276a2276793b7d7c"></script>
<script src="/templates/ArchaeologicaTemplate/js/navtoggle.js"></script>
<script>
jQuery(window).on('load',  function() {
				new JCaption('img.caption');
			});
	</script>
<!-- Adding this to help @media -->
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-5715469-3', 'auto');
  ga('send', 'pageview');

</script>
<!-- Universal Google Analytics Plugin by PB Web Development -->
</head>
<!--End head-->
<body>
<!-- Begin header -->
<section id="header">
<div class="wrapper">
<header>
<div class="logo_banner">
<div class="empty"></div>
<img alt="Archaeologica" class="logo_img" src="/templates/ArchaeologicaTemplate/images/logo.png"/>
<div class="logo_text">
<h1 class="title">Archaeologica</h1>
<h2 class="sub_title">Archaeological News &amp; Fun Information</h2>
</div>
<div class="empty"></div>
</div>
<!--Begin Nav-->
<nav class="navigation" id="siteNav">
<button class="show-on-small" id="menuToggler"><span class="icon icon-menu">☰</span></button>
<ul class="nav menu mod-list">
<li class="item-101 default current active"><a href="/">Home</a></li><li class="item-102"><a href="/news">News</a></li><li class="item-118"><a href="/resource-links">Resource Links</a></li><li class="item-104"><a href="/about">About</a></li><li class="item-105"><a href="https://www.archaeologica.org/forum/" rel="noopener noreferrer" target="_blank">Forum</a></li></ul>
</nav>
<!--End Nav-->
</header>
</div> <!-- End Wrapper -->
</section>
<!-- End header -->
<!-- Begin banner -->
<div id="underwriting">
<div id="banner">
<div class="bannergroup">
<div class="banneritem">
<a href="https://www.archaeologychannel.org/strata" rel="noopener noreferrer" target="_blank" title="Strata: Portraits of Humanity">
<img alt="Strata: Portraits of Humanity banner image" src="/images/banners/Strata-Banner.jpg"/>
</a> <div class="clr"></div>
</div>
</div>
</div>
</div>
<!-- End banner -->
<!--Begin Layout-->
<section id="content">
<div class="wrapper">
<content id="welcome">
<div id="article">
<div id="backdrop">
<div id="system-message-container">
</div>
<div class="item-page" itemscope="" itemtype="https://schema.org/Article">
<meta content="en-GB" itemprop="inLanguage"/>
<div class="page-header">
<h2 itemprop="headline">
			Welcome!		</h2>
</div>
<div itemprop="articleBody">
<section id="content">
<div class="wrapper">
<p style="text-align: center;"><span style="font-size: 18pt;"><strong>Your source on the web for daily archaeological news and information.</strong></span></p>
<p style="text-align: center;"><span style="font-size: 18pt;"><strong>Listen to our favorite news from each week on podcast!</strong></span></p>
</div>
</section> </div>
</div>
</div>
</div>
</content>
<content id="featured">
<div class="newsflash">
<h4 class="newsflash-title">
			January 13th, 2021 Edition		</h4>
<p><span data-sheets-hyperlink="https://www.livescience.com/pig-oldest-cave-animal-drawing.html" data-sheets-userformat='{"2":1325827,"3":{"1":4,"2":"\"$\"#,##0.00"},"4":{"1":2,"2":14275305},"11":0,"12":0,"14":{"1":2,"2":1136076},"15":"Arial","16":12,"21":1,"23":1}' data-sheets-value='{"1":2,"2":"Warty pig is oldest animal cave art on record"}'><a class="in-cell-link" href="https://www.livescience.com/pig-oldest-cave-animal-drawing.html" rel="noopener noreferrer" target="_blank">Warty pig is oldest animal cave art on record</a> Live Science</span></p>
<p><span data-sheets-hyperlink="https://www.livescience.com/pig-oldest-cave-animal-drawing.html" data-sheets-userformat='{"2":1325827,"3":{"1":4,"2":"\"$\"#,##0.00"},"4":{"1":2,"2":14275305},"11":0,"12":0,"14":{"1":2,"2":1136076},"15":"Arial","16":12,"21":1,"23":1}' data-sheets-value='{"1":2,"2":"Warty pig is oldest animal cave art on record"}'><a class="in-cell-link" href="https://www.shine.cn/news/nation/2101133184/" rel="noopener noreferrer" target="_blank">Palace system discovered in 5,300-year-old ancient capital</a> Shanghai Daily</span></p>
<p><span data-sheets-hyperlink="https://www.livescience.com/pig-oldest-cave-animal-drawing.html" data-sheets-userformat='{"2":1325827,"3":{"1":4,"2":"\"$\"#,##0.00"},"4":{"1":2,"2":14275305},"11":0,"12":0,"14":{"1":2,"2":1136076},"15":"Arial","16":12,"21":1,"23":1}' data-sheets-value='{"1":2,"2":"Warty pig is oldest animal cave art on record"}'><a class="in-cell-link" href="https://www.aa.com.tr/en/culture/2nd-anatolian-seljuk-sultans-grave-found-in-se-turkey/2107545" rel="noopener noreferrer" target="_blank">2nd Anatolian Seljuk sultan's grave found in SE Turkey</a> Anadolu Agency</span></p>
<p><span data-sheets-hyperlink="https://www.livescience.com/pig-oldest-cave-animal-drawing.html" data-sheets-userformat='{"2":1325827,"3":{"1":4,"2":"\"$\"#,##0.00"},"4":{"1":2,"2":14275305},"11":0,"12":0,"14":{"1":2,"2":1136076},"15":"Arial","16":12,"21":1,"23":1}' data-sheets-value='{"1":2,"2":"Warty pig is oldest animal cave art on record"}'><a class="in-cell-link" href="https://phys.org/news/2021-01-insights-domesday-survey-revealed.html" rel="noopener noreferrer" target="_blank">New insights from original Domesday survey revealed</a> Phys</span></p>
<p> </p>
</div>
</content>
</div>
</section><!--End Layout-->
<!--Begin footer-->
<section id="footer">
<div class="wrapper">
<footer>
<p id="copyright">© 2021 Archaeologica</p>
<div id="footer-nav">
<div class="custom">
<p><a href="https://archaeologica.org">Home </a><a href="/news" rel="alternate" title="News">News</a> <a href="/resources" id="footer-resource" rel="alternate">Resources</a><a href="/about" rel="alternate" title="About"> About</a> <a href="/forum" id="footer-forum" rel="noopener noreferrer" target="_blank">Forum</a></p></div>
</div>
<div id="socialicons">
<div class="custom">
<p><a href="http://feeds.feedburner.com/TheArchaeologyChannel-AudioNewsFromArchaeologica" id="icon-rss" rel="noopener noreferrer" target="_blank" title="Subscribe to our Feed!">FeedBurner Feed</a> <a href="https://www.facebook.com/TheArchaeologyChannel/" id="icon-fb" rel="noopener noreferrer" target="_blank" title="Visit us on Facebook!">Facebook</a> <a href="https://twitter.com/archchannel" id="icon-twitter" rel="noopener noreferrer" target="_blank" title="Visit us on Twitter!">Twitter</a></p></div>
</div>
</footer>
</div>
</section>
<!-- End Footer -->
</body>
</html>