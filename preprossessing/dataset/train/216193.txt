<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>BHP SERWIS</title>
<meta content="BHP SERWIS świadczy usługi w zakresie..." name="description"/>
<meta content="bhp serwis, mielec" name="keywords"/>
<link href="bootstrap/dist/css/bootstrap.css" rel="stylesheet"/>
<!--<script src="/js/jquery.min.js"></script>-->
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family='Open Sans'|Lato|Oswald|Roboto" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i|Open+Sans+Condensed:300,300i,700|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container">
<img alt="BHP SERWIS" class="img-fluid" src="/img/header-kask.png"/>
<!--<img src="img/header-mini.png" alt="BHP SERWIS" class="img-mobile" />-->
</div><div class="container">
<br/>
<nav class="navbar navbar-toggleable-md">
<button aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right" data-target="#navbarsExampleDefault" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<a class="navbar-brand" href="/">BHP SERWIS</a>
<div class="collapse navbar-collapse" id="navbarsExampleDefault">
<ul class="navbar-nav mr-auto">
<li class="nav-item active">
<a class="nav-link" href="o-nas">O nas</a>
</li>
<li class="nav-item">
<a class="nav-link" href="uslugi-i-obsluga-bhp">Usługi BHP</a>
</li>
<li class="nav-item">
<a class="nav-link" href="uslugi-ppoz">Usługi PPOŻ.</a>
</li>
<li class="nav-item">
<a class="nav-link" href="szkolenia-bhp">Szkolenia BHP</a>
</li>
<li class="nav-item">
<a class="nav-link" href="szkolenia-ppoz">Szkolenia PPOŻ.</a>
</li>
<li class="nav-item">
<a class="nav-link" href="klienci">Kilenci</a>
</li>
<li class="nav-item">
<a class="nav-link" href="kontakt">Kontakt</a>
</li>
<!--          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>
-->
</ul>
</div>
</nav>
</div><div class="container">
<br/><div class="container">
<div class="row">
<div class="col">
<h2>Usługi BHP</h2>
<p>
            Nasza oferta obejmuje pełny outsourcing BHP i PPOŻ. Jest to bardzo wygodne rozwiązanie bo to my przejmiemy troskę o wykonanie wszelkich spraw związanych z bhp w Państwa firmie (instytucji), zadbamy o bezpieczeństwo pracowników w pełnym zakresie. W tym zakresie opracowujemy instrukcje bhp i oceny...

            </p>
</div>
<div class="col">
<h2>Usługi PPOŻ.</h2>
<p>
W ramach tej usługi specjaliści PPOŻ. wykonają dla Państwa: Instrukcje Bezpieczeństwa Pożarowego dla budynków i obiektów budowlanych zgodnie z §6 Rozporządzenia MSWiA o ochronie przeciwpożarowej budynków ( Dz.U. 109 poz. 719 z 2010r.) Przeglądy PPOŻ. ...            

            </p>
</div>
<div class="col">
<h2>Audyt BHP - przeglądy</h2>
<p>
            Zgodnie z art. 2 §1 rozporządzenia Rady Ministrów z dnia 2 września 1997r. w sprawie służby bezpieczeństwa i higieny pracy (Dz. U. Nr 109, poz. 704 z 1997r z późn. zm.) do zakresu działań służby BHP należy...
            </p>
</div>
</div>
<div class="row">
<div class="col">
<div style="clear: both; text-align: center;">
<a class="btn" href="/uslugi-i-obsluga-bhp" role="button">więcej</a>
</div>
</div>
<div class="col">
<div style="clear: both; text-align: center;">
<a class="btn" href="/uslugi-ppoz" role="button">więcej</a>
</div>
</div>
<div class="col">
<div style="clear: both; text-align: center;">
<a class="btn" href="/audyt-bhp-przeglady" role="button">więcej</a>
</div>
</div>
</div>
<div class="row" style="margin-top: 30px;">
<div class="col">
<h2>Szkolenia BHP</h2>
<p>
            Prowadzimy szkolenia BHP dla różnych grup pracowniczych: dla pracowników administracyjno-biurowych dla pracowników na stanowiskach robotniczych dla pracowników na stanowiskach inżynieryjno-technicznych dla pracodawców oraz innych osób kierujących pracownikami Ponadto prowadzimy szkolenia wstępne...
            </p>
</div>
<div class="col">
<h2>Szkolenia PPOŻ.</h2>
<p>
            Od początku istnienia firmy zajmujemy się organizacją profesjonalnych i rzetelnych szkoleń PPOŻ. Można z nich skorzystać zarówno w siedzibie firmy, jak i w innych miejscach wskazanych przez naszych kontrahentów. Prowadzimy szkolenia PPOŻ. wstępne oraz okresowe, a szczegółowy program szkolenia ...
            </p>
</div>
<div class="col">
<center><img alt="BHP SERWIS KONTAKT" class="img-fluid" src="img/info-kontakt.png"/>
</center>
</div>
</div>
<div class="row">
<div class="col">
<div style="clear: both; text-align: center;">
<a class="btn" href="/szkolenia-bhp" role="button">więcej</a>
</div>
</div>
<div class="col">
<div style="clear: both; text-align: center;">
<a class="btn" href="/szkolenia-ppoz" role="button">więcej</a>
</div>
</div>
<div class="col">
</div>
</div>
</div>
</div>
<div style="clear: both;">
<div class="container"><br/>
<p style="text-align: center;">
<a href="/uslugi-bhp">Usługi BHP</a> |
<a href="/uslugi-ppoz">Usługi PPOŻ.</a> |
<a href="/szkolenia-bhp">Szkolenia BHP</a> |
<a href="/szkolenia-ppoz">Szkolenia PPOŻ.</a> |
<a href="/kontakt">Kontakt</a> |
                
    
</p>
</div>
</div> <script crossorigin="anonymous" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
<script>window.jQuery || document.write('<script src="bootstrap/docs/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script crossorigin="anonymous" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="bootstrap/docs/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>