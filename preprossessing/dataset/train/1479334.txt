<!DOCTYPE html PUBLIC "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Bob Basil" name="Author"/>
<meta content="trouble waits, troublewaits, troublewaits.com, robin plan, suicide notes, goodbye radio, bob basil, mental health, mental disease, Ellavon" name="KEYWORDS"/>
<meta content="Mozilla/4.74 [en] (Win98; U) [Netscape]" name="GENERATOR"/>
<meta content="trouble waits, troublewaits, troublewaits.com, robin plan, suicide notes, goodbye radio, bob basil, mental health, mental disease, Ellavon" name="KeyWords"/>
<title>troublewaits.com homepage</title>
</head>
<body alink="#FF0000" background="background.jpg" bgcolor="#FFFFFF" link="#FF0000" text="#000000" vlink="#FF0000">
 
<center>
<p><b><font size="+2">8888888888888888888888888888888888888888888888888888888888</font></b>
<br/><b><font size="+2">We Stay Together</font></b>
</p><p><b><font size="+3"> <a href="../../lili.html">Lili</a></font></b>
</p><p>
</p><hr width="100%"/>
<p><b><font size="+4">trouble<font color="#CC0000">waits</font>.com</font></b>
</p><p><img border="1" height="300" src="tub1man.jpg" width="197"/>
</p><p><b><font color="#CC0000"><font size="+1">This is not a mental health website</font></font></b>
<br/><b><font color="#CC0000"><font size="+1">It is a mental illness website</font></font></b>
<br/><b><font color="#CC0000"><font size="+1">A place for people to come after
no one else can stand to look at you, self included.</font></font></b>
<br/><b><font color="#CC0000"><font size="+1">ItÂs for people who wished they
wanted to get better but somethin keeps standin in the way.</font></font></b>
<br/><b><font color="#CC0000"><font size="+1">I want to learn why there are
so many of us.</font></font></b>
<br/><b><font color="#CC0000"><font size="+1">ÂTil then the little monster
inside you is welcome here.</font></font></b>
</p><p><b><font color="#CC0000"><font size="+1">Troublewaits is dedicated to
loser throwaway Stacey Anne Rainey who never saw the light at the end of
the tunnel, and loser throwaway Harriet Tubman who showed other loser throwaways
the way the fuck out.</font></font></b>
</p><p><b><font color="#CC0000"><font size="+1">To wit:</font></font></b>
<br/><b><font color="#CC0000"><font size="+1">ÂWhen you think the night has
seen your mind</font></font></b>
<br/><b><font color="#CC0000"><font size="+1">That inside youÂre twisted and
unkind</font></font></b>
<br/><b><font color="#CC0000"><font size="+1">Let me stand to show that you
are blind</font></font></b>
<br/><b><font color="#CC0000"><font size="+1">Please put down your hands</font></font></b>
<br/><b><font color="#CC0000"><font size="+1">ÂCause I see you</font></font></b>
<br/><b><font color="#CC0000"><font size="+1">IÂll be your mirror.Â</font></font></b>
<br/><b><font color="#CC0000"><font size="+1">-- Lou Reed</font></font></b>
</p><p>
</p><hr width="100%"/>
<p><font size="+2">trouble<font color="#FF0000">waits</font> is a thematic
non-fiction narrative, a personal take on sensibility in all its internal
and external permutations as experienced by the author.</font>
</p><p><font size="+2"><font color="#3333FF">Make it easy on yourself</font><font color="#000000">,</font></font>
<br/><b><font size="+2"><a href="../../castofcharacters.html">get to know
our</a></font></b>
<br/><b><font size="+2"><a href="../../castofcharacters.html"><font color="#FF0000">CAST
OF CHARACTERS</font> first.</a></font></b>
</p><p><font size="+2">Then come back and start at the top if you want.</font>
</p><p>
</p><hr width="100%"/>
<p><b><font size="+2"> <a href="../../legaliseme.htm">troublewaits theme
song</a></font></b>
</p><p><b><font size="+2"><a href="../../../decompensation.html">decompensation</a></font></b>
</p><p><font color="#FF0000"><font size="+2"> <b><a href="../../Weird.html">weirdly
wonderful ...</a></b></font></font>
</p><p><font size="+2"> <b><a href="../../Where.htm">whereÂs <strike>Stacey
Anne Rainey</strike> when you need her?</a></b></font>
</p><p><b><font size="+2"><a href="../../plebian.html">too plebian</a></font></b>
</p><p><font size="+2"> <b><a href="../../complaint.html">my complaint letter</a></b></font>
</p><p><b><font size="+2"> <a href="../../hereshowyouhelpme.htm">here's
how you help me</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/SheMayWalkLikeBoDiddlyBut.htm">she
may walk like bo diddly but</a></font></b>
</p><p><b><font size="+2"><a href="../../hihoImhome.htm">hi ho, i'm home</a></font></b>
</p><p><b><font size="+2"><a href="../../WORSHIPPERS.htm">rule worshippers</a></font></b>
</p><p><font size="+2"> <b><a href="../../thisishowithappens.htm">this is
how it happens</a></b></font>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/ROBINPLANFORDUMMIES.htm">ROBIN
PLAN FOR DUMMIES</a></font></b>
</p><p><font size="+2"> <b><a href="../../whatwedohereisthisithink.htm">what
we do here is this, i think</a></b></font>
</p><p><b><font size="+2"><a href="../../obvious.htm">listing</a></font></b>
</p><p><b><font size="+2"> <a href="../../howtotalktoyourpsychiatrist.htm">how
to talk to your psychiatrist</a></font></b>
</p><p><b><font size="+2"><a href="../../nickcaveteacheslottieallsheneedstoknowaboutlottie.htm">nick
cave teaches lottie all she needs to know about lottie</a></font></b>
</p><p><b><font size="+2"> <a href="../../youmakemelaughor.htm">you make
me laugh or I will not be held accountable for my insane acts of love</a></font></b>
</p><p><b><font size="+2"><a href="../../Youseengodlately.htm">you seen God lately?</a></font></b>
</p><p><b><font size="+2"><a href="../../ethicshere.htm">ethics here!</a></font></b>
</p><p><b><font size="+2"> <a href="../../fuckyou.htm">fuck you</a></font></b>
</p><p><b><font size="+2"><a href="../../newrules.htm">new rules breakin me</a></font></b>
</p><p><b><font size="+2"><a href="../../babyplease.htm">baby won't you please
come home? thanks, you can go now</a></font></b>
</p><p><font size="+2"> <b><a href="../../markeitzellyricshodgepodge.htm">mark
eitzel lyrics hodgepodge</a></b></font>
</p><p><font size="+2"> <b><a href="../../thesethings.htm">these things
don't happen</a></b></font>
</p><p><b><font size="+2"> <a href="../../lets.htm">let's all go the hospital,
the hospital will wake us well</a></font></b>
</p><p><b><font size="+2"><a href="../../bobdylan.htm">bob dylan is where I need
him</a></font></b>
</p><p><b><font size="+2"><a href="../../unthere.htm">unthere you un-ago again</a></font></b>
</p><p><b><font size="+2"><a href="../../listening.htm">listening to p.j. harvey,
brooding on hitchcock</a></font></b>
</p><p><b><font size="+2"> <a href="../../slattern.htm">a flower is a lovesome
thing</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/medicalupdate.htm">oh
christ it started again yesterday</a></font></b>
</p><p><b><font size="+2"> <a href="../../shebelongstome.htm">she belongs
to me</a></font></b>
</p><p><b><font size="+2"> <a href="../../fakemen.htm">fake men real! sorry,
fake men real! sorry, fake men more less after abortion!!!</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/lovesafunnything.htm">love's
a funny thing</a></font></b>
</p><p><b><font size="+2"> <a href="http://www.troublewaits.com/thiswillfucking.htm">this
will fuckin wreck ya</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/readers.htm">what?
do i have any fuckin readers or what?</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/WEBASTER.htm">webaster,
can we end up in jail, please</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/stackingcorpses.htm">stacking
corpses, feeling sensitive</a></font></b>
</p><p><font size="+2"> <b><a href="http://www.troublewaits.com/youcancallmeghandi.html">you
can call me gandhi</a></b></font>
</p><p><font size="+2"> <b><i><a href="http://www.troublewaits.com/conveyor.htm">not
another <font color="#000000">conveyor belt </font>of open caskets!</a></i></b></font>
</p><p><b><font size="+2"> <a href="http://www.troublewaits.com/hiworld.htm">
hi world, you can fuck off now</a></font></b>
</p><p><b><font size="+2"> <a href="http://www.troublewaits.com/shesthebostonpops.htm">she's
the boston pops</a></font></b>
</p><p><font size="+2"> <b><a href="http://www.troublewaits.com/yoloretta.htm">yo
loretta, cotton mather's home!</a></b></font>
</p><p><b><i><font size="+2"><a href="../../thougoesttowimmenforgetnotthywhip.htm">thou
goest to wimmen? forget not thy whip!</a></font></i></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/nothingexistsforme.htm">nothing
exists for me</a></font></b>
</p><p><b><font size="+2"><a href="../../intelligencequotidian.htm">intelligence
quotidian</a></font></b>
</p><p><b><font size="+2"> <a href="http://www.troublewaits.com/army.htm">army</a></font></b>
</p><p><b><font size="+2"> <a href="http://www.troublewaits.com/giveemback.htm">give
'em back god, i know what to do with them</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/part1.htm">you
wouldn't believe the day i had</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/ifithappened.htm">if
it happened once it could happen again</a></font></b>
</p><p><b><font size="+2"> <a href="../../part2revised.htm">you wouldn't
believe the day i had part 2</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/part3.htm">you
wouldn't believe the day i had, part 3</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/part4.htm">you
would not fuckin beleeeeeeve the etc.</a></font></b>
</p><p><b><font size="+2"> <a href="http://www.troublewaits.com/part5.htm">you
wouldn't believe the day i had, part 5</a></font></b>
</p><p><b><font size="+2"> <a href="http://www.troublewaits.com/part6.htm">you
would not believe the girls around here i had today</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/patriarchy.htm">patriarchy</a></font></b>
</p><p><b><font size="+2"> <a href="http://www.troublewaits.com/365.htm">365
new words of the day, all right here</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/helloho.htm">hi
ho, is hemingway a homo</a></font></b>
</p><p><b><font size="+2"><a href="../../Spectcular.htm">spectacular beating
number 1: stanford university</a></font></b>
</p><p><b><font size="+2"> <a href="http://www.troublewaits.com/willyoube.htm">will
you be my psychologist?</a></font></b>
</p><p><b><font size="+2"><a href="../../outofthecradleendlesslymocking.htm">out
of the cradle endlessly mocking</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/spectacular2.htm">spectacular
public beating number 2: rock critics</a></font></b>
</p><p><b><font size="+2"> <a href="http://www.troublewaits.com/stillwaiting.htm">still
waiting for my man</a></font></b>
</p><p><font size="+2"> <b><a href="http://www.troublewaits.com/medicationstation.htm">medication
station around the bend</a></b></font>
</p><p><b><font size="+2"> <a href="http://www.troublewaits.com/hey.htm">hey
shoshanna can we be friends or do you need another beating?</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/pullupchair.htm">pull
up a chair, this one's pain free, honest</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/peaches.htm">if
you don't like my peaches why do you shake my tree?</a></font></b>
</p><p><b><font size="+2"><a href="../../whostartedthis.htm">who started this?</a></font></b>
</p><p><b><font size="+2"> <a href="../../troublesstandupfalldowndiagnosisexplained.htm">trouble's
stand-up/fall down diagnosis explained</a></font></b>
</p><p><font size="+2"> <b><a href="http://www.troublewaits.com/anchors.htm">anchors
aweigh</a></b></font>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/mydoctordisplayshisrichfontofignorance.htm">dr.
little-bit displays his rich font of ignorance</a></font></b>
</p><p><font size="+2"> <b><a href="http://www.troublewaits.com/doctorsdothistoo.htm">doctors
do this too</a></b></font>
</p><p><b><font size="+2"> <a href="http://www.troublewaits.com/heyaustinheresanotheropenlovelettertoaustin.htm">hey
austin, this is another open love letter to austin</a></font></b>
</p><p><font size="+2"> <b><a href="http://www.troublewaits.com/rolemirrors.htm">role
mirrors</a></b></font>
</p><p><b><font size="+2"> <a href="http://www.troublewaits.com/thisisodd.htm">this
is odd</a></font></b>
</p><p><b><font size="+2"> <a href="../../fuckinghomoscomingoutofthewordwork.htm">fucking
homos coming out of the woodwork</a></font></b>
</p><p><font size="+2"> <b><a href="../../awomanisanopenbook.htm">a woman
is an open book</a></b></font>
</p><p><b><font size="+2"> <a href="http://www.troublewaits.com/twocanplayatthatgame.htm">two
can play at this game</a></font></b>
</p><p><b><font size="+2"><a href="../../whypaulwesterbergisfuckinshakespeare2.htm">why
paul westerberg is fuckin shakespeare, <i>bitch</i></a></font></b>
</p><p><b><font size="+2"><a href="../../psychobabbleapology3.htm">psycho-babble
apology</a></font></b>
</p><p><b><font size="+2"> <a href="../../postscript4.htm">postscript to
psb family i know i'm missin</a></font></b>
</p><p><b><font size="+2"><a href="../../bobdylansherekickingagainstthesoftware5.htm">bob
dylan's here kicking against the software</a></font></b>
</p><p><b><font size="+2"><a href="../../thisisthehousethatcapitalismbuilt.htm">this
is the house that capitalism built</a></font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/heaven.htm">heavenly
father fruit of thy blessings shakedown</a></font></b>
</p><p><b><font size="+2"> <a href="../../answermenodont.htm">answer me.
no don't.</a></font></b>
</p><p><b><font size="+2"><a href="../../doctrumpsmynightmarenurses.htm">dr.
disobey trumps my nightmare nurses</a></font></b>
</p><p><b><font size="+2"><a href="../../thewaking.htm">just in case</a></font></b>
</p><p><b><font size="+2"> <a href="../../bastarddaughterofpsychosocialbabble.htm">bastard
child of psycho-social-babble</a></font></b>
</p><p><b><font size="+2"><a href="../../webasterputshisstamponme.htm">webaster
put his stamp on me</a></font></b>
</p><p><b><font size="+2"><a href="../../youalreadyknow.htm">you already know</a></font></b>
</p><p><b><font size="+2"><a href="../../getthisandmoveonthanks.htm">get this
and move on thanks</a></font></b>
</p><p><b><font size="+2"> <a href="../../parentcultureuhcaniaskaqueston.htm">parent
culture? um, can i ask a question?</a></font></b>
</p><p><b><font size="+2"><a href="../../lookinsideyourinfantbreastandtakesomeresponsibility.htm">look
inside your infant breast and take some responsibility</a></font></b>
</p><p><b><font size="+2"><a href="../../iwallowyouswallowthiswaynoonediessee.htm">i
wallow, you swallow, this way no one dies, see</a></font></b>
</p><p><b><font size="+2"><a href="../../thisisyourbrainondoctors.htm">this is
your brain on doctors</a></font></b>
</p><p><b><font size="+2"> <a href="../../thisisyourbrainonmonomen.htm">this
is your brain on the monomen</a></font></b>
</p><p><b><font size="+2"> <a href="../../troublespmslovelettertoshirkinfuckinboyfriend.htm">trouble's
pms love letter to shirkin fuckin boyfriend</a></font></b>
</p><p><b><font size="+2"> <a href="../../stoplyingorelse.htm">stop lying
or else</a></font></b>
</p><p><b><font size="+2"> <a href="../../iguessimoldfashionedwhenitcomestoinsanity.htm">i
guess i'm old fashioned when it comes to insanity</a></font></b>
</p><p><b><font size="+2"> <a href="../../suzyisablackheartedbitch.htm">suzy
is a blackhearted bitch</a></font></b>
</p><p><b><font size="+2"> <a href="../../Haveyoucomehereforforgiveness.htm">have
you come here for forgiveness?</a></font></b>
</p><p><b><font size="+2"><a href="../../crippled.htm">crippled</a></font></b>
</p><p><b><font color="#FF0000"><font size="+2"> <a href="../../ienjoybeingagirl.htm">I
enjoy being a girl</a></font></font></b>
</p><p><b><font size="+2"> <a href="../../troubleswebasteprophecy.htm">trouble's
webaster prophecy</a></font></b>
</p><p><b><font color="#FF0000"><font size="+2"><a href="../../maidofconstantsorrow.htm">maid
of constant sorrow</a></font></font></b>
</p><p><b><font color="#FF0000"><font size="+2"><a href="../../heshere.htm">he's
here</a></font></font></b>
</p><p><b><font size="+2"><a href="../../hesgone.htm">he's gone</a></font></b>
</p><p><b><font size="+2"> <a href="../../whatsitgoingtobe.htm">what's it
going to be?</a></font></b>
</p><p><b><font size="+2"> <a href="../../doesthissoundright.htm">does this
sound right?</a></font></b>
</p><p><b><font size="+2"> <a href="../../whichsexismoreinsanehun.htm">which
sex is more insane, huh?</a></font></b>
</p><p><b><font size="+2"><a href="../../SERENITY.htm">serenity</a></font></b>
</p><p><b><font size="+2"> <a href="../../youmusthavethistoloseyourmind.htm">you
must have this to lose your mind</a></font></b>
</p><p><font size="+2"> <b><a href="../../youresogoddamnyoung.htm">you're
so goddamn young</a></b></font>
</p><p><b><font size="+2"> <a href="../../tohercoywebmaster.htm">to her
coy webmaster</a></font></b>
</p><p><b><font size="+2"> <a href="../../letshaveapicnic.htm">let's have
a picnic</a></font></b>
</p><p><b><font size="+2"> <a href="../../imustbebesidemyself.htm">i must
be beside myself</a></font></b>
</p><p><b><font size="+2"><a href="../../flyingbuttress.htm">flying buttress</a></font></b>
</p><p><b><font size="+2"> <a href="../../releasethebats.html">release the
bats</a></font></b>
</p><p><font size="+2"><b> </b> <b><a href="../../austinmakesmoney.htm">austin
makes money!!!</a></b></font>
</p><p><b><font size="+2"> <a href="../../maybebabyyoucandrivemycar.htm">maybe
baby you can drive me car</a></font></b>
</p><p><b><font size="+2"><font color="#CC33CC"> </font> <a href="../../ifatreefallsonaledzeppelinfan.htm">if
a tree falls on a led zeppelin fan</a></font></b>
</p><p><font size="+2"> <b><a href="../../countdowntofucking.htm">countdown
to fucking</a></b></font>
</p><p><b><font size="+2"> <a href="../../deargodtheblankpeople.htm">dear
god, the blank people</a></font></b>
</p><p><b><font size="+2"> <a href="../../friendshipisall.htm">friendship
is all</a></font></b>
</p><p><b><font size="+2"> <a href="../../imsupposedtostopkillingpeopledoesthissoundright.htm">I'm
supposed to stop <i>criticizing</i> people, does this sound right?</a></font></b>
</p><p><b><font size="+2"><a href="youkilledjonbenet.htm">you killed jonbonet</a></font></b>
</p><p><b><font size="+2"><a href="../../originalmomandpopredux.htm">original
mom and pop redux</a></font></b>
</p><p><font size="+2"> <b> <a href="../../itmaynotbechildbirthbut.htm">it
may not be childbirth but schizophrenics count for nothing too</a></b></font>
</p><p><font size="+2"> <b> <a href="../../ragstounriches.htm">rags to unriches</a></b></font>
</p><p><font size="+2"> <b> <a href="../../howmuchforyoursoul.htm">how much
for your soul?</a></b></font>
</p><p><font size="+2"> <b> <a href="../../theresalittlebitofwhoreineverynancyreagan.htm">there's
a little bit of whore in every nancy reagan</a></b></font>
</p><p><b><font size="+2"><a href="../../whatsonyermind.htm">what's on yer mind?</a></font></b>
</p><p><b><font size="+2"><a href="../../wedigrepetition.htm">we dig repetition</a></font></b>
</p><p><font size="+2"> <b><a href="../../isshedrunkiseveryone.htm">is she
drunk, is everyone?</a></b></font>
</p><p><b><font size="+2"><a href="../../luckbealady.htm">luck be a lady</a></font></b>
</p><p><b><font size="+2"><a href="../../caughtinmyeye.htm">caught in my eye</a></font></b>
</p><p><b><font size="+2"> <a href="../../thisisnotgossiptothosewhoarenotbrainless.htm">this
is not gossip to those who are not brainless</a></font></b>
</p><p><font size="+2"> <b> <a href="../../tinybubbles.htm">tiny bubbles</a></b></font>
</p><p><b><font size="+2"><a href="../../lame.htm">Lame-name-no-blame-banana-fanna-insane-a-fi-fie-foe-fum-</a></font></b>
<br/><b><font size="+2"><a href="../../lame.htm">I-smell-the-remains-of-a-mad-womon-la!</a></font></b>
</p><p><b><font size="+2"><a href="../../ghostinthemachinebusted.htm">ghost in
the machine, busted</a></font></b>
</p><p><b><font size="+2"><a href="../../apoetswork.htm">a poet's work</a></font></b>
</p><p><b><font size="+2"><a href="../../itsalwaysdarkestbeforeitgoespitchblack.htm">it's
always darkest before it goes pitch black</a></font></b>
</p><p><b><font size="+2">  <a href="../../anddownwego.htm">and down we
go</a></font></b>
</p><p><b><font size="+2"> <a href="../../memoriesaremadeofthis.htm">memories
are made of this</a></font></b>
</p><p><b><font size="+2"><a href="../../yoheifers.htm">Yo- heifers, sows, irritants,
recite this poem and get a boyfriend</a></font></b>
</p><p><b><font size="+2"><a href="../../everwonderwhichsexispureevil.htm">ever
wonder which sex is pure evil?</a></font></b>
</p><p><b><font size="+2"><a href="../../heycosmogirltakethisquiz.htm">hey cosmo
girl, take this quiz</a></font></b>
</p><p><b><font size="+2"><a href="../../happydaysarehereagain.htm">happy days
are here again</a></font></b>
</p><p><b><font size="+2"><a href="../../homicidelifeonthestreetsofdharmaandgreg.htm">homicide:
life on the streets of dharma and greg</a></font></b>
</p><p><b><font size="+2"><a href="../../asusualtheblackpeople.htm">as usual
the black people</a></font></b>
</p><p><b><font size="+2"><a href="../../perfectharmony.htm">perfect harmony</a></font></b>
</p><p><b><font size="+2"><a href="../../whatwedoissecretmyass.htm">what we do
is secret, my ass</a></font></b>
</p><p><b><font size="+2"><a href="../../thisisnotbrainlesstothosewhoarenotgossip.htm">this
is not brainless to those who are not gossip</a></font></b>
</p><p><b><font size="+2"><a href="../../weneedtotalk.htm">we need to talk</a></font></b>
</p><p><b><font size="+2"> <a href="../../kissmekissmeagainthenrekissme.htm">kiss
me, kiss me again, then re-kiss me</a></font></b>
</p><p><b><font size="+2"> <a href="../../myfirstactonenteringthisworld.htm">my
first act on entering this world</a></font></b>
</p><p><b><font size="+2"> <a href="../../mencannotfeel.htm">men cannot
feel</a></font></b>
</p><p><b><font size="+2"> <a href="../../fucksocialsecurityihaveabetteridea.htm">fuck
social security, i have a better idea</a></font></b>
</p><p><b><font size="+2"><a href="../../kickback.htm">kickback</a></font></b>
</p><p><b><font size="+2"><a href="../../lookitthisshit.htm">lookit this shit</a></font></b>
</p><p><b><font size="+2"><a href="../../bangbangcheckmate.htm">bang bang: check
mate</a></font></b>
</p><p><b><font size="+2"><a href="pointtakennowaboutthosesandals.htm">point
taken, now about those sandals</a></font></b>
</p><p><b><font size="+2"><a href="../../idontwannabeapinheadnomore.htm">i don't
wanna be a pinhead no more</a></font></b>
</p><p><b><font size="+2"><a href="../../justbecauseidontcaredoesntmeanidontunderstand.htm">just
because i don't care doesn't mean i don't understand</a></font></b>
</p><p><b><font size="+2"> 
<a href="../../bitchstolemyanorexia.htm">bitch
stole my anorexia</a></font></b>
</p><p><b><font size="+2"><a href="../../lovingherwaseasier.htm">loving her was
easier</a></font></b>
</p><p><b><font size="+2"><a href="../../eroticinterlude.htm">erotic interlude</a></font></b>
</p><p><b><font size="+2"><a href="../../imbackeh.html">i'm back, eh</a></font></b>
</p><p><b><font size="+2"> <a href="../../readyforyourcloseuphellhoundboulevard.htm">ready
for your close-up, hellhound boulevard?</a></font></b>
</p><p><b><font size="+2"><a href="../../troubleiswhereiwasborne.htm">trouble
is where i was borne</a></font></b>
</p><p><b><font size="+2"> <a href="roundtrip.htm">round trip</a></font></b>
</p><p><b><font size="+2"><a href="../../knock.htm">knock-knock, where's my unlimited
NEA Grant so i can finish my fucking art, pricks?</a></font></b>
</p><p><b><font size="+2"><a href="../../whymelord.htm">why me, lord?</a></font></b>
</p><p><b><font size="+2"><a href="../../allwehavetofearisdisposablespermbanks.htm">all
we have to fear is disposable sperm banks</a></font></b>
</p><p><b><font size="+2"><a href="proofthattroublesafictionalcharacter.htm">proof
that trouble's a <i>fictional</i> character, <i>lottie, </i>shit-fer-brains</a></font></b>
</p><p><b><font size="+2"><a href="enoughmother.htm">enough, mother</a></font></b>
</p><p><b><font size="+2"><a href="../../troublesbeatquotesbeatyours.htm">trouble's
beat</a></font></b>
</p><p><b><font color="#CC33CC"><font size="+2"><a href="../../youvebeenwaitingsopatientlyforthis.htm">you've
been waiting so patiently for this</a></font></font></b>
</p><p>
</p><hr width="100%"/>
<p><b><font size="+2">say hi to <a href="mailto:dariussmith55@hotmail.com">jeff</a>,
you made him what he is today --</font></b>
</p><p><b><font size="+2"><a href="http://www.troublewaits.com/diss.html">jeff</a></font></b>
<br/><b><font size="+2"><a href="http://www.troublewaits.com/sportyspice.html">jeff</a></font></b>
<br/><b><font size="+2"><a href="http://www.troublewaits.com/ode.html">jeff</a></font></b>
<br/><b><font size="+2"><a href="http://www.troublewaits.com/jeff3.html">jeff</a></font></b>
</p><p>
</p><hr width="100%"/>
<p><b><font size="+2">More material is on its way.  While waiting, you
might visit</font></b>
<br/><b><font size="+2"> <a href="http://www.ellavon.com/evn_sn.html"><strike>Suicide</strike>
Survivor Notes</a> and <a href="http://www.ellavon.com/evn_gr.html">Goodbye
Radio,</a></font></b>
<br/><b><font size="+2">brought to you by</font> <font size="+2"><a href="http://www.ellavon.com">Ellavon:
An Ezine of Basic Culture.</a></font></b>
</p><p>
</p><hr width="100%"/>
<p><font size="+2"> <b><a href="../../links.html">links i like</a></b></font>
</p><p>
</p><hr width="100%"/>
<p><b><a href="mailto:useme@troublewaits.com">Write me.</a></b>
<br/> 
</p><p><b><font color="#CC0000">Site updated May 16, 2003.</font></b>
<br/> 
<br/> 
<br/> 
<br/> 
<br/> 
<br/> 
<br/> 
<br/> 
</p><p><b><font size="-1">Copyright 2002 - 2003 © Robin Plan and troublewaits.com. 
All rights reserved.</font></b></p></center>
<p><br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
</p></body>
</html>
