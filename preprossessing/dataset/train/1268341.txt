<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]--><!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]--><!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="pt-br"> <!--<![endif]-->
<head>
<title>404        - Redes de Proteção Ideal Sp, Abc, Ipiranga e Mais Regiões</title>
<meta charset="utf-8"/>
<link href="https://www.saopauloredesdeprotecao.com.br/imagens/favicon.ico" rel="shortcut icon"/>
<base href="https://www.saopauloredesdeprotecao.com.br/"/>
<meta content="5upya1CqEZ0U1obyCgMS9nRa5yJ9LQz8DiuUhtLZ5kc" name="google-site-verification"/>
<meta content="050781796ADDABE87AFBE34327C5C36C" name="msvalidate.01"/>
<meta content="404" name="description"/>
<meta content="404" name="keywords"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content=";" name="geo.position"/>
<meta content="" name="geo.region"/>
<meta content="" name="geo.placename"/>
<meta content="," name="ICBM"/>
<meta content="index,follow" name="robots"/>
<meta content="General" name="rating"/>
<meta content="7 days" name="revisit-after"/>
<meta content="Redes de Proteção Ideal" name="author"/>
<meta content="Brasil" property="og:region"/>
<meta content="404 - Redes de Proteção Ideal Sp, Abc, Ipiranga e Mais Regiões" property="og:title"/>
<meta content="article" property="og:type"/>
<meta content="https://www.saopauloredesdeprotecao.com.br/imagens/logo.png" property="og:image"/>
<meta content="https://www.saopauloredesdeprotecao.com.br/home" property="og:url"/>
<meta content="404" property="og:description"/>
<meta content="Redes de Proteção Ideal" property="og:site_name"/>
<link href="https://www.saopauloredesdeprotecao.com.br/home" rel="canonical"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
<link href="https://www.saopauloredesdeprotecao.com.br/css/style.css" rel="stylesheet"/>
<link href="https://www.saopauloredesdeprotecao.com.br/css/normalize.css" rel="stylesheet"/>
<!-- Desenvolvido por BUSCA CLIENTES - www.buscaclientes.com.br -->
</head>
<body data-pagina="e3b42000e327e5f1aa45c58130c42f57">
<header>
<div class="wrapper">
<div class="logo">
<a class="pagina-logo" data-area="paginaLogo" href="https://www.saopauloredesdeprotecao.com.br/" rel="nofollow" title="Redes de Proteção Ideal - Sp, Abc, Ipiranga e mais regiões">
<img alt="404 - Redes de Proteção Ideal" src="https://www.saopauloredesdeprotecao.com.br/imagens/logo.png" title="Logo - Redes de Proteção Ideal"/>
</a>
</div>
<nav id="menu">
<ul>
<li><a class="btn-home" data-area="paginaHome" href="https://www.saopauloredesdeprotecao.com.br/" title="Home">Home</a></li> <li class="dropdown" id="open-side-menu" onclick="OpenMenuSide()"><a href="https://www.saopauloredesdeprotecao.com.br/servicos" title="Serviços">Serviços</a></li> <li><a href="https://www.saopauloredesdeprotecao.com.br/contato" title="Contato">Contato</a></li>
</ul>
</nav>
<div class="right" id="btnContatoHeader" onclick="hideContato()">
<i class="material-icons" id="menu_icon">menu</i>
<div id="popup-contato">
<p class="number"> (11) <strong><a class="tel follow-click" data-origem="cabecalho-telefone-" href="tel:+551127614260" rel="nofollow" title="Telefone  - Redes de Proteção Ideal">2761-4260</a>
</strong></p> <p class="number"> (11) <strong><a class="tel follow-click" data-origem="cabecalho-telefone-2" href="tel:+551127534464" rel="nofollow" title="Telefone 2 - Redes de Proteção Ideal">2753-4464</a>
</strong></p> <p class="number"> (11) <strong><a class="tel follow-click" data-origem="cabecalho-telefone-3" href="tel:+5511990175444" rel="nofollow" title="Telefone 3 - Redes de Proteção Ideal">99017-5444</a>
</strong></p></div></div>
<div class="popup-contato">
<p class="number"> (11) <strong><a class="tel follow-click" data-origem="cabecalho-telefone-" href="tel:+551127614260" rel="nofollow" title="Telefone  - Redes de Proteção Ideal">2761-4260</a>
</strong></p> <p class="number"> (11) <strong><a class="tel follow-click" data-origem="cabecalho-telefone-2" href="tel:+551127534464" rel="nofollow" title="Telefone 2 - Redes de Proteção Ideal">2753-4464</a>
</strong></p> <p class="number"> (11) <strong><a class="tel follow-click" data-origem="cabecalho-telefone-3" href="tel:+5511990175444" rel="nofollow" title="Telefone 3 - Redes de Proteção Ideal">99017-5444</a>
</strong></p></div>
</div>
</header>
<div class="wrapper">
<main>
<div class="content">
<section>
<div id="breadcrumb">
<ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<a href="https://www.saopauloredesdeprotecao.com.br/" itemprop="item">
<span itemprop="name">Home</span></a>
<meta content="1" itemprop="position"/>
</li>
<li><span>404</span></li>
</ol>
</div>
<h1>Página 404</h1>
<article class="full">
<p class="msg-404"><br/>Ops! Algo deu errado...<br/>
                        Escolha abaixo a página que deseja visualizar.
                    </p>
<ul class="sitemap">
<li><a class="btn-home" data-area="paginaHome" href="https://www.saopauloredesdeprotecao.com.br/" title="Home">Home</a></li> <li class="dropdown" id="open-side-menu" onclick="OpenMenuSide()"><a href="https://www.saopauloredesdeprotecao.com.br/servicos" title="Serviços">Serviços</a></li> <li><a href="https://www.saopauloredesdeprotecao.com.br/contato" title="Contato">Contato</a></li>
</ul>
</article>
<br class="clear"/>
</section>
</div>
</main>
</div>
<div class="clear"></div>
<footer>
<article id="pgEndereco">
<h2>Endereço</h2>
<section class="end_adicionais">
<article class="endereco">
<p class="end">
</p>
<p> (11)  2761-4260</p>
<p> (11)  2753-4464</p>
<p> (11)  99017-5444</p>
</article>
</section>
</article>
<section id="main-footer">
<h5>Redes de Proteção Ideal</h5>
<nav>
<ul>
<li>
<a href="https://www.saopauloredesdeprotecao.com.br/" title="Home">Home</a>
</li>
<li>
<a href="https://www.saopauloredesdeprotecao.com.br/servicos" title="Serviços">Serviços</a>
</li>
<li>
<a href="https://www.saopauloredesdeprotecao.com.br/contato" title="Contato">Contato</a>
</li>
<li><a href="https://www.saopauloredesdeprotecao.com.br/mapa-do-site" title="Mapa do site Redes de Proteção Ideal">Mapa do site</a></li>
</ul>
</nav>
</section>
<section id="copyrigth">
<p>O inteiro teor deste site está sujeito à proteção de direitos autorais. Copyright© Redes de Proteção Ideal (Lei 9610 de 19/02/1998)</p>
<img alt="Plataforma criada por BuscaCliente.com.br" src="https://www.saopauloredesdeprotecao.com.br/imagens/max.png"/>
</section>
</footer>
<script src="https://www.saopauloredesdeprotecao.com.br/js/jquery-1.7.2.min.js"></script>
<!-- MENU  MOBILE -->
<script src="https://www.saopauloredesdeprotecao.com.br/js/jquery.slicknav.js"></script>
<!-- /MENU  MOBILE -->
<script>
    $(document).ready(function () {

                var cotacaoImagem = 1;

        var estruturaCotacao = '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div id="title-empresa"><h2></h2></div><div id="title-keyword"><h3></h3></div><div class="fancybox-inner"></div></div></div></div>';

        if (cotacaoImagem === 1) {
            estruturaCotacao = '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div id="title-empresa"><h2></h2></div><div id="title-keyword"><h3></h3></div><div class="fancybox-inner"></div><p><button type="button" class="btn-laranja btn-cotacao" data-area="imagem">Fazer cotação</button></p></div></div></div>';
        }
        $(".lightbox").fancybox({
            wrapCSS: 'fancybox-custom',
            closeClick: true,
            tpl: {
                wrap: estruturaCotacao,
            },
            helpers: {
                title: {
                    type: 'inside'
                },
                overlay: {
                    css: {'background': 'rgba(0, 0, 0, 0.5)'}
                }
            },
            beforeShow: function () {
                var palavra = this.title;
                this.title = "Imagem ilustrativa de " + this.title;
                $("#title-empresa").html("<h2>404</h2>");
                $("#title-keyword").html("<h3>" + palavra + "</h3>");
                $("#fancyalter").attr({"alt": palavra, "title": palavra});

            },
            afterShow: function () {
                var element_id = $(this.element.context).data('id');

                var id = element_id.replace("imagem-", "")
                var referencia = $("#imagem-ref-" + parseInt(id)).html();

                $(".fancybox-inner").append('<span class="lightbox-fixed-ref">Cod.:' + referencia + '</span>');
            }
        });
    });

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-106890722-29', 'auto');
    ga('send', 'pageview');
    </script>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script defer="" src="https://apis.google.com/js/plusone.js"></script>
<script src="https://www.saopauloredesdeprotecao.com.br/js/vendor/modernizr-2.6.2.min.js"></script>
<script src="https://www.saopauloredesdeprotecao.com.br/js/jquery.fancybox.js"></script>
<script src="https://www.saopauloredesdeprotecao.com.br/js/jquery.mask.js"></script>
<script src="https://www.saopauloredesdeprotecao.com.br/js/newScript.js"></script>
<script src="https://www.saopauloredesdeprotecao.com.br/js/organictabs.jquery.js"></script>
<script src="https://www.saopauloredesdeprotecao.com.br/js/scriptbreaker-multiple-accordion-1.js"></script>
<script src="https://www.saopauloredesdeprotecao.com.br/js/geral.js"></script>
<script src="https://www.saopauloredesdeprotecao.com.br/js/carrossel.js"></script>
<!-- Desenvolvido por BUSCA CLIENTES - www.buscaclientes.com.br -->
<!-- #!Version3 - BuscaMax -->
</body>
</html>