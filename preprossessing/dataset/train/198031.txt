<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="4ac2e5c41ca3c4af" name="yandex-verification"/>
<title>© Baurum - клуб и форум строителей - Строительный портал | Справочник строителя | Сайт для строителей, застройщиков, заказчиков, проектировщиков, архитекторов и др.</title>
<meta content="строительство домов технологии строительства материалы работы оборудование строительные смр СМР жилищное монолитное подряд этапы малоэтажное разрешение комплекс строитель капитальное клуб справочник каталог компаний фирм инженерное производство новости высотное профессиональных строителей цены стоимость услуги форум цена" name="Keywords"/>
<meta content="Интернет клуб строителей baurum это: справочники по основным строительным материалам, работам, оборудованию, инженерным работам и оборудованию; возможность узнать цены на строительные материалы и услуги; разместить объявление;... Кроме этого - всегда свежие новости строительной индустрии, форум строителей и многое другое. Строительство и все, что с ним связано на сайте baurum." name="description"/>
<link href="/_system/repository/styles.inc.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/go/style3ssl.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css"/>
<script language="javascript" src="/_system/repository/scripts.inc.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link href="/favicon.ico" rel="shortcut icon"/>
<link href="/runtime_style.php?location=module.49.7" rel="stylesheet" type="text/css"/>
</head>
<body>
<style>
html, body {background:#eee;}
.wrp li, .wrp ul {padding:0;}
h5.title {font-size:14px;}
.content .content {padding:0;}
.content .content td {border:1px solid #eee;padding:3px 7px;}
.content .content .content-footer td {border:0 none;}
</style>
<div id="wrap">
<div id="header">
<div class="rrlogo">
<a href="https://baurum.ru">
<img alt="© Baurum - клуб строителей - Строительный портал | Справочник строителя | Сайт для строителей, застройщиков, заказчиков, проектировщиков, архитекторов и др." class="logo" src="/go/logo.png"/></a>
</div> <!-- rrlogo -->
<div class="rrsearch">
<div id="search" style="float:right;margin-top:5px;">
<form accept-charset="utf-8" action="https://forum.baurum.ru/search/" method="get">
<meta content="https://forum.baurum.ru/search/?q={q}" itemprop="target"/>
<input data-role="searchFilter" name="type" type="hidden" value="all"/>
<fieldset>
<label class="hide" for="main_search">Поиск</label>
<div class="right" id="search_wrap">
<input class="" id="main_search" itemprop="query-input" name="q" placeholder="Поиск по форуму..." size="17" tabindex="6" type="search"/>
<input class="submit_input clickable" type="submit" value=""/>
</div>
</fieldset>
</form>
</div>
<div class="ya-site-form ya-site-form_inited_no" onclick="return {'action':'https://baurum.ru/go/yasearch/','arrow':false,'bg':'transparent','fontsize':12,'fg':'#000000','language':'ru','logo':'rb','publicname':'Поиск по baurum.ru','suggest':true,'target':'_self','tld':'ru','type':2,'usebigdictionary':true,'searchid':2312007,'input_fg':'#000000','input_bg':'#ffffff','input_fontStyle':'normal','input_fontWeight':'normal','input_placeholder':'Поиск по всему сайту...','input_placeholderColor':'#000000','input_borderColor':'#7f9db9'}"><form accept-charset="utf-8" action="https://yandex.ru/search/site/" method="get" target="_self"><input name="searchid" type="hidden" value="2312007"/><input name="l10n" type="hidden" value="ru"/><input name="reqenc" type="hidden" value=""/><input name="text" type="search" value=""/><input type="submit" value="Найти"/></form></div><style type="text/css">.ya-page_js_yes .ya-site-form_inited_no { display: none; }</style><script type="text/javascript">(function(w,d,c){var s=d.createElement('script'),h=d.getElementsByTagName('script')[0],e=d.documentElement;if((' '+e.className+' ').indexOf(' ya-page_js_yes ')===-1){e.className+=' ya-page_js_yes';}s.type='text/javascript';s.async=true;s.charset='utf-8';s.src=(d.location.protocol==='https:'?'https:':'http:')+'//site.yandex.net/v2.0/js/all.js';h.parentNode.insertBefore(s,h);(w[c]||(w[c]=[])).push(function(){Ya.Site.Form.init()})})(window,document,'yandex_site_callbacks');</script>
</div> <!-- rrsearch -->
<div class="rrauth">
<ul class="ipsList_inline right">
<li><a href="https://forum.baurum.ru/login/" rel="nofollow">Вход на форум</a></li>
<li><a href="https://forum.baurum.ru/register/" id="register_link" rel="nofollow">Регистрация</a></li>
</ul>
</div>
</div>

﻿
	<script type="text/javascript">
		$(function() {
			var pull 		= $('#pull');
				menu 		= $('nav ul#mob');
				menuHeight	= menu.height();

			$(pull).on('click', function(e) {
				e.preventDefault();
				menu.slideToggle();
			});

			$(window).resize(function(){
        		var w = $(window).width();
        		if(w > 1000 && menu.is(':hidden')) {
        			menu.removeAttr('style');
        		}
    		});
		});
		</script>
<nav class="header_nav posrel">
<ul id="mob">
<li>
<a href="https://forum.baurum.ru/">Строительный форум</a>
</li>
<li>
<a href="https://www.baurum.ru/go/">Статьи</a>
<ul>
<li>
<a href="https://www.baurum.ru/go/category/vnutrennyaya-otdelka-i-dizajn/">Внутренняя отделка и дизайн</a>
</li>
<li>
<a href="https://www.baurum.ru/go/category/stroitelstvo-doma-dachi/">Строительство дома, дачи</a>
</li>
<li>
<a href="https://www.baurum.ru/go/category/stroitelnye-i-otdelochnye-materialy/">Строительные и отделочные материалы</a>
</li>
<li>
<a href="https://www.baurum.ru/go/category/inzhenernye-sistemy/">Инженерные системы</a>
</li>
<li>
<a href="https://www.baurum.ru/go/category/remont-kvartiry/">Ремонты</a>
</li>
<li>
<a href="https://www.baurum.ru/go/category/domashnie-zhivotnye/">Домашние животные</a>
</li>
<li>
<a href="https://www.baurum.ru/go/category/instrumenty-i-oborudovanie/">Инструменты, оборудование, техника</a>
</li>
<li>
<a href="https://www.baurum.ru/go/category/landshaftnyj-dizajn/">Ландшафтный дизайн</a>
</li>
<li>
<a href="https://www.baurum.ru/go/category/okna/">Окна</a>
</li>
<li>
<a href="https://www.baurum.ru/go/category/proekty-domov/">Проекты домов</a>
</li>
<li>
<a href="https://www.baurum.ru/go/category/sad-i-ogorod/">Сад и огород</a>
</li>
<li>
<a href="https://www.baurum.ru/go/category/fundamenty/">Фундаменты</a>
</li>
<li>
<a href="https://www.baurum.ru/go/category/svoimi-rukami/">Своими руками</a>
</li>
<li>
<a href="https://www.baurum.ru/go/category/ekonomiya-pri-stroitelstve-i-polzovanii-resursami/">Экономия</a>
</li>
</ul>
</li>
<li>
<a href="https://www.baurum.ru/_library/">Справочник строителя</a>
<ul>
<li>
<a href="https://www.baurum.ru/.stroymaterials/">Строительные материалы</a>
</li>
<li>
<a href="https://www.baurum.ru/stroyworks/">Строительные работы</a>
</li>
<li>
<a href="https://www.baurum.ru/stroymachines/">Строительное оборудование</a>
</li>
<li>
<a href="https://www.baurum.ru/enginworks/">Инженерные работы</a>
</li>
<li>
<a href="https://www.baurum.ru/enginequipment/">Инженерное оборудование</a>
</li>
<li>
<a href="https://www.baurum.ru/.expense_of_materials/">Расход материалов</a>
</li>
<li>
<a href="https://www.baurum.ru/articles/">Строительная документация</a>
</li>
<!-- -->
<li>
<a href="https://www.baurum.ru/reference_dictionary/">Справочник-словарь</a>
</li>
<li>
<a href="https://www.baurum.ru/units/">Единицы измерения</a>
</li>
<li>
<a href="https://www.baurum.ru/alphabet-figures/">Алфавит и цифры</a>
</li>
<li>
<a href="https://www.baurum.ru/mathematics/">Строительная математика</a>
</li>
<li>
<a href="https://www.baurum.ru/to-designer/">Справочник проектировщика</a>
</li>
</ul>
</li>
<li>
<a href="https://catalog.baurum.ru/">Товары и услуги</a>
</li>
<li>
<a href="https://www.baurum.ru/adv/">Реклама, сотрудничество</a>
</li>
<li class="hhide">
<a href="#">Объявления</a>
<ul>
<li>
<a href="https://www.baurum.ru/other/">Разное</a>
</li>
<li>
<a href="https://www.baurum.ru/turnrent/">Сдадим в аренду</a>
</li>
<li>
<a href="https://www.baurum.ru/rent/">Возьмём в аренду</a>
</li>
<li>
<a href="https://www.baurum.ru/sell/">Продадим</a>
</li>
<li>
<a href="https://www.baurum.ru/buy/">Купим</a>
</li>
</ul>
</li>
<li class="hhide">
<a href="https://www.baurum.ru/companies/">Компании</a>
</li>
</ul>
<a href="#" id="pull"></a>
</nav>
<!--img class="size-full wp-image-3463" title="Популярный строительный форум!" src="https://www.baurum.ru/go/wp-content/uploads/2015/07/forum4.gif" alt="Популярный строительный форум!" width="1000" height="44" /-->
<div class="wrapContent">

﻿<!-- Google Adsens pageLevel-->
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-5261755542783699",
    enable_page_level_ads: true
  });
</script>
<!-- Yandex.RTB R-A-258138-6 -->
<div id="yandex_rtb_R-A-258138-6"></div>
<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-258138-6",
                renderTo: "yandex_rtb_R-A-258138-6",
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script>

 

<div class="content">


				
					



					﻿<div class="mlayout-title std-content">
<link href="/ipb.css" media="screen" rel="stylesheet" type="text/css"/>
<style>a.stati {font-size:18px;display:block;}
#sidebar {
margin-left: -247px;
}
html, body {background:#eee;}</style>
<style>.cell-1 img, .cell-2 img {border-radius: 3px;
    display: block;height:160px;}
a.stati {font-size:18px;display:block;margin-top:6px;}
.press {clear: left;float: left;margin: 20px 0;width:100%;}
td.cell-1 {border-right: 1px solid #E8EFF7;padding-right: 20px;}
.cell-splitter td {vertical-align:top;width: 50%;}
td.cell-2 {padding-left: 20px;}
</style>
<p>Сайт baurum.ru (Клуб профессиональных строителей) посещают люди с самыми широкими интересами и запросами в области строительства. Это могут быть как профессиональные снабженцы, представители компаний заказчиков, частные застройщики, инженерно-технические работники строительной отрасли, проектировщики, так и посетители интересующиеся ремонтом собственной квартиры или дачи.</p>
<br/>
<p>Подборка статей связанных со строительством и ремонтом, которая будет полезна каждому.</p>
<table cellspacing="0" class="cell-splitter press">
<tbody>
<tr><td class="cell-1"><img alt="Модульные дома: преимущества и недостатки" class="attachment-240x240 wp-post-image" src="https://www.baurum.ru/go/wp-content/uploads/2020/12/modular_house0.jpg" title="Модульные дома: преимущества и недостатки" width="240"/> <a class="stati" href="/go/modulnye-doma-preimushhestva-i-nedostatki/">Модульные дома: преимущества и недостатки</a><br/><p>




В наше время всё чаще строятся загородные дома. Люди стремятся из душных и грязных городов на природу. Составление проекта – это всегда то, с чег...</p></td><td class="cell-2"><img alt="Выбираем проект дома с мансардой" class="attachment-240x240 wp-post-image" src="https://www.baurum.ru/go/wp-content/uploads/2020/12/mansarda1.jpg" title="Выбираем проект дома с мансардой" width="240"/> <a class="stati" href="/go/vybiraem-proekt-doma-s-mansardoj/">Выбираем проект дома с мансардой</a><br/><p>




Выбрать проект собственного дома всегда непросто, тем более, учесть при этом мнения всех членов семьи. Но есть несколько пунктов, которые помогут...</p></td></tr><tr><td class="cell-1"><img alt="Дома в стиле барнхаус" class="attachment-240x240 wp-post-image" src="https://www.baurum.ru/go/wp-content/uploads/2020/11/barnhouse1.jpg" title="Дома в стиле барнхаус" width="240"/> <a class="stati" href="/go/doma-v-stile-barnhaus/">Дома в стиле барнхаус</a><br/><p>




Амбарный вид домостроения в стиле барнхаус появился не так давно и уже стал популярным в США и странах Европы из-за своей эффективности, экологич...</p></td><td class="cell-2"><img alt="Раздвижные перегородки типа гармошка" class="attachment-240x240 wp-post-image" src="https://www.baurum.ru/go/wp-content/uploads/2020/11/sliding_partitions_accordion1.jpg" title="Раздвижные перегородки типа гармошка" width="240"/> <a class="stati" href="/go/razdvizhnye-peregorodki-tipa-garmoshka/">Раздвижные перегородки типа гармошка</a><br/><p>




Спроектированное пространство в помещении не всегда может соответствовать ожиданиям пользователей. Однако различные пространства можно перепланир...</p></td></tr><tr><td class="cell-1"><img alt="Вредная пыль, токсины, канцерогены и другие неожиданные свойства минеральной ваты" class="attachment-240x240 wp-post-image" src="https://www.baurum.ru/go/wp-content/uploads/2020/11/mineral_wool_toxic1.jpg" title="Вредная пыль, токсины, канцерогены и другие неожиданные свойства минеральной ваты" width="240"/> <a class="stati" href="/go/vrednaya-pyl-toksiny-kancerogeny-i-drugie-neozhidannye-svojstva-mineralnoj-vaty/">Вредная пыль, токсины, канцерогены и другие неожиданные свойства минеральной ваты</a><br/><p>
Часто
для теплоизоляции или звукозащиты помещений выбирают утеплители из минеральной
ваты. И каждый, кто хоть раз видел, как утепляют помещение минва...</p></td><td class="cell-2"><img alt="Как правильно выбрать стальную дверь" class="attachment-240x240 wp-post-image" src="https://www.baurum.ru/go/wp-content/uploads/2020/11/steel_door1.jpg" title="Как правильно выбрать стальную дверь" width="240"/> <a class="stati" href="/go/kak-pravilno-vybrat-stalnuyu-dver/">Как правильно выбрать стальную дверь</a><br/><p>




Раньше многие люди не испытывали необходимости в установке двери из стали в квартиру.  Обычно устанавливались обычные двери из дерева или обычног...</p></td></tr><tr><td class="cell-1"><img alt="Огнестойкость газобетонных домов" class="attachment-240x240 wp-post-image" src="https://www.baurum.ru/go/wp-content/uploads/2020/10/fire_resistance_of_aerated-concrete1.jpg" title="Огнестойкость газобетонных домов" width="240"/> <a class="stati" href="/go/ognestojkost-gazobetonnyh-domov/">Огнестойкость газобетонных домов</a><br/><p>




Из всех строительных материалов газобетон – один из самых устойчивых к огню. Эта огнестойкость дает домам с бетонными стенами определенные преиму...</p></td><td class="cell-2"><img alt="Автоматические выключатели: особенности выбора" class="attachment-240x240 wp-post-image" src="https://www.baurum.ru/go/wp-content/uploads/2020/10/circuit_breaker1.jpg" title="Автоматические выключатели: особенности выбора" width="240"/> <a class="stati" href="/go/avtomaticheskie-vyklyuchateli-osobennosti-vybora/">Автоматические выключатели: особенности выбора</a><br/><p>




Многие электрики считают, что лишний раз лезть в электропроводку частного дома или квартиры лучше не стоит, ведь это может привести к плачевным п...</p></td></tr><tr><td class="cell-1"><img alt="Как выбрать плиткорез?" class="attachment-240x240 wp-post-image" src="https://www.baurum.ru/go/wp-content/uploads/2020/10/tile_cutter1.jpg" title="Как выбрать плиткорез?" width="240"/> <a class="stati" href="/go/kak-vybrat-plitkorez/">Как выбрать плиткорез?</a><br/><p>




Правильный выбор оборудования для резки кафеля или тротуарной плитки важен для того, кто намерен его приобрести в собственность. Правила те же са...</p></td><td class="cell-2"><img alt="Печь для русской бани с закрытой каменкой" class="attachment-240x240 wp-post-image" src="https://www.baurum.ru/go/wp-content/uploads/2020/09/pech_zakritaya_kamenka_title.jpg" title="Печь для русской бани с закрытой каменкой" width="240"/> <a class="stati" href="/go/pech-dlya-russkoj-bani-s-zakrytoj-kamenkoj/">Печь для русской бани с закрытой каменкой</a><br/><p>




Русская баня всегда была востребована по причине положительного воздействия на организм человека. Из-за высокой влажности и температуры внутри не...</p></td></tr>
</tbody></table>
<table cellspacing="0" class="cell-splitter quick-links">
<tr>
<td class="cell-1">
<div class="bl .stroymaterials">
<ul class="bl-title">
<li class="crn-l"></li>
<li class="txt" style=" margin-top:-3px;"><a href="/.stroymaterials/">Строительные материалы</a></li>
<li class="crn-r">
<div style="background: url(/resources/images/blob_cache/10.0x0.png) no-repeat center;"></div>
</li>
</ul>
<div class="bl-content">
<div class="bl-content-wrp">
<a href="/.stroymaterials/?cat=buildingmaterials">Общие сведения о строительных материалах</a><span> | </span><a href="/.stroymaterials/?cat=.armature">Арматура, металлопрокат</a><span> | </span><a href="/.stroymaterials/?cat=concretes">Бетоны</a><span> | </span><a href="/.stroymaterials/?cat=.constructionmortars">Строительные растворы</a><span> | </span><a href="/.stroymaterials/?cat=dopant-concrete">Добавки к бетонам и растворам</a><span class="details">...</span>
</div>
</div>
</div>
</td>
<td class="cell-2">
<div class="bl stroyworks">
<ul class="bl-title">
<li class="crn-l"></li>
<li class="txt" style=" margin-top:-3px;"><a href="/stroyworks/">Строительные работы</a></li>
<li class="crn-r">
<div style="background: url(/resources/images/blob_cache/12.0x0.png) no-repeat center;"></div>
</li>
</ul>
<div class="bl-content">
<div class="bl-content-wrp">
<a href="/stroyworks/?cat=build_activity">Строительная деятельность</a><span> | </span><a href="/stroyworks/?cat=spadework">Подготовительные работы</a><span> | </span><a href="/stroyworks/?cat=geodesic_works">Геодезические работы</a><span> | </span><a href="/stroyworks/?cat=earthworks">Земляные работы</a><span> | </span><a href="/stroyworks/?cat=foundations">Устройство фундаментов</a><span class="details">...</span>
</div>
</div>
</div>
</td>
</tr>
<tr>
<td class="cell-1">
<div class="bl stroymachines">
<ul class="bl-title">
<li class="crn-l"></li>
<li class="txt" style="max-width: 168px; margin-top: -12px;"><a href="/stroymachines/">Строительная техника и оборудование</a></li>
<li class="crn-r">
<div style="background: url(/resources/images/blob_cache/11.0x0.png) no-repeat center;"></div>
</li>
</ul>
<div class="bl-content">
<div class="bl-content-wrp">
<a href="/stroymachines/?cat=faucets_build">Краны</a><span> | </span><a href="/stroymachines/?cat=earthworks_machines">Машины для земляных работ</a><span> | </span><a href="/stroymachines/?cat=roofings_mechanisms">Машины для кровельных работ</a><span> | </span><a href="/stroymachines/?cat=painter_machines">Машины для малярных работ</a><span> | </span><a href="/stroymachines/?cat=concrete_works_machines">Машины для производства бетонных работ</a><span class="details">...</span>
</div>
</div>
</div>
</td>
<td class="cell-2">
<div class="bl enginworks">
<ul class="bl-title">
<li class="crn-l"></li>
<li class="txt" style=" margin-top:-3px;"><a href="/enginworks/">Инженерные работы</a></li>
<li class="crn-r">
<div style="background: url(/resources/images/blob_cache/119.0x0.png) no-repeat center;"></div>
</li>
</ul>
<div class="bl-content">
<div class="bl-content-wrp">
<a href="/enginworks/?cat=ventilation">Вентиляция</a><span> | </span><a href="/enginworks/?cat=water_supply">Водоснабжение</a><span> | </span><a href="/enginworks/?cat=sewage_system">Канализация</a><span> | </span><a href="/enginworks/?cat=supply_warm">Теплоснабжение</a><span> | </span><a href="/enginworks/?cat=electrosupply">Электроснабжение</a><span class="details">...</span>
</div>
</div>
</div>
</td>
</tr>
<tr>
<td class="cell-1">
<div class="bl enginequipment">
<ul class="bl-title">
<li class="crn-l"></li>
<li class="txt" style=" margin-top:-3px;"><a href="/enginequipment/">Инженерное оборудование</a></li>
<li class="crn-r">
<div style="background: url(/resources/images/blob_cache/126.0x0.png) no-repeat center;"></div>
</li>
</ul>
<div class="bl-content">
<div class="bl-content-wrp">
<a href="/enginequipment/?cat=electrical-engineering">Электротехническое оборудование</a><span> | </span><a href="/enginequipment/?cat=pipes-plastic">Трубы и фитинги</a><span> | </span><a href="/enginequipment/?cat=safety-systems">Системы безопасности</a><span> | </span><a href="/enginequipment/?cat=sanitary-engineering">Санитарная техника</a><span> | </span><a href="/enginequipment/?cat=geodetic-equipment">Геодезическое оборудование</a><span class="details">...</span>
</div>
</div>
</div>
</td>
<td class="cell-2">
<div class="bl .expense_of_materials">
<ul class="bl-title">
<li class="crn-l"></li>
<li class="txt" style=" margin-top:-3px;"><a href="/.expense_of_materials/">Расход материалов</a></li>
<li class="crn-r">
<div style="background: url(/resources/images/blob_cache/118.0x0.png) no-repeat center;"></div>
</li>
</ul>
<div class="bl-content">
<div class="bl-content-wrp">
<a href="/.expense_of_materials/?cat=charge_development_ground">Разработка грунта</a><span> | </span><a href="/.expense_of_materials/?cat=expense_gidro_isolation">Гидроизоляция</a><span> | </span><a href="/.expense_of_materials/?cat=charge-stone-designs">Каменные конструкции</a><span> | </span><a href="/.expense_of_materials/?cat=charge_roofing_materials">Устройство кровель</a><span> | </span><a href="/.expense_of_materials/?cat=charge-painting-decorating">Отделочные работы</a><span class="details">...</span>
</div>
</div>
</div>
</td>
</tr>
</table>
<div class="links">
<div class="wrp">
<!--<a href="/companies/" class="item companies">
				<span>Визитница</span>
				Каталог фирм по разделам.
			</a>
			<span class="line"></span>
			<a href="/board/" class="item tenders">
				<span>Объявления</span>
                                Купим, Продадим, Возьмём в аренду, Сдадим в аренду, Разное ...
			</a>-->
<a class="item companies" href="/_library/" style="width: 136px; padding: 15px 14px;">
<span>Справочник строителя</span>
				Статьи на строительную тему.
			</a>
<span class="line"></span>
<a class="item tenders" href="/alldays/" style="width: 136px; padding: 15px 14px;">
<span>На каждый день</span>
                Полезные статьи
			</a>
<span class="line"></span>
<a class="item career" href="https://forum.baurum.ru" style="width: 136px; padding: 15px 14px;">
<span>Форум</span>
                                Строительный форум
			</a>
<span class="line"></span>
<a class="item events" href="/go/" style="width: 136px; padding: 15px 14px;">
<span>Строительный Журнал</span>
                                Статьи про строительство и ремонт
			</a>
</div>
</div>
</div>
</div>
<div id="sidebar">
﻿<img alt="баннер для сайдбара Вы это искали" class="aligncenter size-full wp-image-4475" height="40" src="/go/wp-content/uploads/2016/06/баннер-для-сайдбара-Вы-это-искали.png" width="240"/>
<!-- Yandex.RTB R-A-258138-1 -->
<div id="yandex_rtb_R-A-258138-1"></div>
<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-258138-1",
                renderTo: "yandex_rtb_R-A-258138-1",
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script>

 
    <div class="popTopics">
<div class="popHeading">
            Топ-5 тем форума
        </div>
<div class="popContent">
<div class="popItem">
<div class="popNumber">1</div>
<div class="popTitle"><a href="https://forum.baurum.ru/topic/107-как-выбрать-окно-пвх/" rel="nofollow">Как выбрать окно пвх?</a><span class="popViews">5980 просмотров</span></div>
</div>
<div class="popItem">
<div class="popNumber">2</div>
<div class="popTitle"><a href="https://forum.baurum.ru/topic/2232-отопление-в-загородный-дом/" rel="nofollow">Отопление в загородный дом</a><span class="popViews">3491 просмотр</span></div>
</div>
<div class="popItem">
<div class="popNumber">3</div>
<div class="popTitle"><a href="https://forum.baurum.ru/topic/74-какая-баня-лучше/" rel="nofollow">Какая баня лучше</a><span class="popViews">3395 просмотров</span></div>
</div>
<div class="popItem">
<div class="popNumber">4</div>
<div class="popTitle"><a href="https://forum.baurum.ru/topic/1121-газ-против-электричества/" rel="nofollow">Газ против электричества</a><span class="popViews">1942 просмотра</span></div>
</div>
<div class="popItem">
<div class="popNumber">5</div>
<div class="popTitle"><a href="https://forum.baurum.ru/topic/2041-новинка-в-проектировании-опс-и-видеонаблюдения/" rel="nofollow">Новинка в проектировании ОПС и видеонаблюдения</a><span class="popViews">1531 просмотр</span></div>
</div>
</div><!-- popContent -->
</div><!-- popTopics -->
<br/>
<div id="fixblock" style="width:240px">
<!-- разместите на месте показа блока -->
<!-- Yandex.RTB R-A-258138-2 -->
<div id="yandex_rtb_R-A-258138-2"></div>
<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-258138-2",
                renderTo: "yandex_rtb_R-A-258138-2",
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script>
 
</div>
<br/>
<center>
<p>
</p>
</center>
</div>
</div><!--wrapContent-->


﻿<div id="footer">
<div class="copyright">
<img alt="© Baurum - клуб строителей - Строительный портал | Справочник строителя | Сайт для строителей, застройщиков, заказчиков, проектировщиков, архитекторов и др." src="/go/logo.png" width="200"/>
<p>2007-2020 © baurum.ru<br/>All rights reserved.</p>
<p><strong>Строительство и ремонт</strong></p>
</div>
<div class="info">
<p><strong>О строительстве - для строителей, застройщиков,<br/>заказчиков, проектировщиков, архитекторов</strong></p>
<p><a href="/_library/">Справочник строителя</a><br/><a href="https://catalog.baurum.ru/" rel="nofollow">Товары и услуги</a><br/><a href="https://shop.baurum.ru/" rel="nofollow">Магазин</a><br/><a href="/alldays/">Справочник на каждый день</a><br/><a href="https://forum.baurum.ru/" rel="nofollow">Стройка и ремонт форум</a><br/><a href="https://forum.baurum.ru/contact/" rel="nofollow">Обратная связь</a></p>
</div>
<div class="footer-coright">При полном или частичном использовании материалов, обратная индексируемая ссылка на www.baurum.ru обязательна
	<br/>
<br/>
<noindex>
<!--LI--><script type="text/javascript"><!--
document.write("<a href='https://www.liveinternet.ru/click;Andrew' "+
"target=_blank><img src='//counter.yadro.ru/hit;Andrew?t14.10;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+";"+Math.random()+"' alt='' title='LiveInternet: показано число просмотров за 24"+
" часа, посетителей за 24 часа и за сегодня' border='0' width='88' height='31'><\/a>")
//--></script><!--/LiveInternet-->
<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=38689830&amp;from=informer" rel="nofollow" target="_blank"><img alt="Яндекс.Метрика" onclick="try{Ya.Metrika.informer({i:this,id:38689830,lang:'ru'});return false}catch(e){}" src="https://informer.yandex.ru/informer/38689830/3_1_FFFFFFFF_EFEFEFFF_0_pageviews" style="width:88px; height:31px; border:0;" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"/></a>
<!-- /Yandex.Metrika informer -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter38689830 = new Ya.Metrika({
                    id:38689830,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/38689830" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<div class="myblockbottom" style="display: none">
<div class="myblockbottom__close"></div>
<!-- Yandex.RTB R-A-258138-13 -->
<div id="yandex_rtb_R-A-258138-13"></div>
<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-258138-13",
                renderTo: "yandex_rtb_R-A-258138-13",
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script></div>
<script type="text/javascript">
(function() {
  
  document.addEventListener("scroll", checkIfNearPosition)

	document.querySelector('.myblockbottom__close').addEventListener('click', function(e) {
  	e.target.parentNode.style.display = 'none', document.removeEventListener("scroll", checkIfNearPosition)
  })
  
  function checkIfNearPosition(e) {
    var bv = document.getElementsByTagName("body")[0].offsetWidth;
    if (bv <= 1340) return;
    var x = document.querySelector('.myblockbottom');
  	window.pageYOffset > 100 && (x.style.display = 'block') && (window.location.pathname != '/') && (window.location.pathname != '/go/yasearch/') || (x.style.display = 'none')
  }
  
})()
</script>
<script type="text/javascript">
function getTopOffset(e) { 
	var y = 0;
	do { y += e.offsetTop; } while (e = e.offsetParent);
	return y;
}
var block = document.getElementById('fixblock'); /* fixblock - значение атрибута id блока */
if ( null != block ) {
	var topPos = getTopOffset( block );

	window.onscroll = function() {
		if ( $(window).width() >= 1024 ) {  // проверка ширины экрана
			var scrollHeight = Math.max( document.documentElement.scrollHeight, document.documentElement.clientHeight),

				// определяем высоту рекламного блока
				blockHeight = block.offsetHeight,

				// вычисляем высоту подвала, footer заменить на значение атрибута id подвала
				footerHeight = document.getElementById('footer').offsetHeight, 	    

				// считаем позицию, до которой блок будет зафиксирован 
				stopPos = scrollHeight - blockHeight - footerHeight; 

			var newcss = (topPos < window.pageYOffset) ? 
				'top:20px; position: fixed;' : 'position:static;';

			if ( window.pageYOffset > stopPos ) 
				newcss = 'position:static;';

			block.setAttribute( 'style', newcss );
		}
	}
}
</script>
<br/>
</noindex></div>
</div>
</div>
</body>
</html>