<!DOCTYPE html>
<html dir="ltr" lang="en" xml:lang="en">
<head>
<title>Education Connect Online Campus</title>
<link href="//www.education-connect.org/pluginfile.php/1/theme_adaptable/favicon/1508158200/edconn%20logo.jpg" rel="icon"/>
<link href="https://www.education-connect.org/theme/adaptable/style/font-awesome.min.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Audiowide:400" rel="stylesheet" type="text/css"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="moodle, Education Connect Online Campus" name="keywords"/>
<script type="text/javascript">
//<![CDATA[
var M = {}; M.yui = {};
M.pageloadstarttime = new Date();
M.cfg = {"wwwroot":"https:\/\/www.education-connect.org","sesskey":"m0xgChREc3","loadingicon":"https:\/\/www.education-connect.org\/theme\/image.php\/adaptable\/core\/1508158200\/i\/loading_small","themerev":"1508158200","slasharguments":1,"theme":"adaptable","jsrev":"1508158200","admin":"admin","svgicons":true};var yui1ConfigFn = function(me) {if(/-skin|reset|fonts|grids|base/.test(me.name)){me.type='css';me.path=me.path.replace(/\.js/,'.css');me.path=me.path.replace(/\/yui2-skin/,'/assets/skins/sam/yui2-skin')}};
var yui2ConfigFn = function(me) {var parts=me.name.replace(/^moodle-/,'').split('-'),component=parts.shift(),module=parts[0],min='-min';if(/-(skin|core)$/.test(me.name)){parts.pop();me.type='css';min=''};if(module){var filename=parts.join('-');me.path=component+'/'+module+'/'+filename+min+'.'+me.type}else me.path=component+'/'+component+'.'+me.type};
YUI_config = {"debug":false,"base":"https:\/\/www.education-connect.org\/lib\/yuilib\/3.17.2\/","comboBase":"https:\/\/www.education-connect.org\/theme\/yui_combo.php?","combine":true,"filter":null,"insertBefore":"firstthemesheet","groups":{"yui2":{"base":"https:\/\/www.education-connect.org\/lib\/yuilib\/2in3\/2.9.0\/build\/","comboBase":"https:\/\/www.education-connect.org\/theme\/yui_combo.php?","combine":true,"ext":false,"root":"2in3\/2.9.0\/build\/","patterns":{"yui2-":{"group":"yui2","configFn":yui1ConfigFn}}},"moodle":{"name":"moodle","base":"https:\/\/www.education-connect.org\/theme\/yui_combo.php?m\/1508158200\/","combine":true,"comboBase":"https:\/\/www.education-connect.org\/theme\/yui_combo.php?","ext":false,"root":"m\/1508158200\/","patterns":{"moodle-":{"group":"moodle","configFn":yui2ConfigFn}},"filter":null,"modules":{"moodle-core-formchangechecker":{"requires":["base","event-focus"]},"moodle-core-event":{"requires":["event-custom"]},"moodle-core-dock":{"requires":["base","node","event-custom","event-mouseenter","event-resize","escape","moodle-core-dock-loader"]},"moodle-core-dock-loader":{"requires":["escape"]},"moodle-core-checknet":{"requires":["base-base","moodle-core-notification-alert","io-base"]},"moodle-core-actionmenu":{"requires":["base","event","node-event-simulate"]},"moodle-core-blocks":{"requires":["base","node","io","dom","dd","dd-scroll","moodle-core-dragdrop","moodle-core-notification"]},"moodle-core-maintenancemodetimer":{"requires":["base","node"]},"moodle-core-dragdrop":{"requires":["base","node","io","dom","dd","event-key","event-focus","moodle-core-notification"]},"moodle-core-popuphelp":{"requires":["moodle-core-tooltip"]},"moodle-core-chooserdialogue":{"requires":["base","panel","moodle-core-notification"]},"moodle-core-formautosubmit":{"requires":["base","event-key"]},"moodle-core-lockscroll":{"requires":["plugin","base-build"]},"moodle-core-notification":{"requires":["moodle-core-notification-dialogue","moodle-core-notification-alert","moodle-core-notification-confirm","moodle-core-notification-exception","moodle-core-notification-ajaxexception"]},"moodle-core-notification-dialogue":{"requires":["base","node","panel","escape","event-key","dd-plugin","moodle-core-widget-focusafterclose","moodle-core-lockscroll"]},"moodle-core-notification-alert":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-notification-confirm":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-notification-exception":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-notification-ajaxexception":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-tooltip":{"requires":["base","node","io-base","moodle-core-notification-dialogue","json-parse","widget-position","widget-position-align","event-outside","cache-base"]},"moodle-core-handlebars":{"condition":{"trigger":"handlebars","when":"after"}},"moodle-core_availability-form":{"requires":["base","node","event","panel","moodle-core-notification-dialogue","json"]},"moodle-backup-confirmcancel":{"requires":["node","node-event-simulate","moodle-core-notification-confirm"]},"moodle-backup-backupselectall":{"requires":["node","event","node-event-simulate","anim"]},"moodle-calendar-info":{"requires":["base","node","event-mouseenter","event-key","overlay","moodle-calendar-info-skin"]},"moodle-course-management":{"requires":["base","node","io-base","moodle-core-notification-exception","json-parse","dd-constrain","dd-proxy","dd-drop","dd-delegate","node-event-delegate"]},"moodle-course-categoryexpander":{"requires":["node","event-key"]},"moodle-course-dragdrop":{"requires":["base","node","io","dom","dd","dd-scroll","moodle-core-dragdrop","moodle-core-notification","moodle-course-coursebase","moodle-course-util"]},"moodle-course-util":{"requires":["node"],"use":["moodle-course-util-base"],"submodules":{"moodle-course-util-base":{},"moodle-course-util-section":{"requires":["node","moodle-course-util-base"]},"moodle-course-util-cm":{"requires":["node","moodle-course-util-base"]}}},"moodle-course-modchooser":{"requires":["moodle-core-chooserdialogue","moodle-course-coursebase"]},"moodle-course-toolboxes":{"requires":["node","base","event-key","node","io","moodle-course-coursebase","moodle-course-util"]},"moodle-course-formatchooser":{"requires":["base","node","node-event-simulate"]},"moodle-form-shortforms":{"requires":["node","base","selector-css3","moodle-core-event"]},"moodle-form-dateselector":{"requires":["base","node","overlay","calendar"]},"moodle-form-showadvanced":{"requires":["node","base","selector-css3"]},"moodle-form-passwordunmask":{"requires":["node","base"]},"moodle-core_message-messenger":{"requires":["escape","handlebars","io-base","moodle-core-notification-ajaxexception","moodle-core-notification-alert","moodle-core-notification-dialogue","moodle-core-notification-exception"]},"moodle-question-qbankmanager":{"requires":["node","selector-css3"]},"moodle-question-preview":{"requires":["base","dom","event-delegate","event-key","core_question_engine"]},"moodle-question-chooser":{"requires":["moodle-core-chooserdialogue"]},"moodle-question-searchform":{"requires":["base","node"]},"moodle-availability_completion-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_date-form":{"requires":["base","node","event","io","moodle-core_availability-form"]},"moodle-availability_grade-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_group-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_grouping-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_profile-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-mod_assign-history":{"requires":["node","transition"]},"moodle-mod_forum-subscriptiontoggle":{"requires":["base-base","io-base"]},"moodle-mod_oublog-savecheck":{"requires":["base","node","io","panel","moodle-core-notification-alert"]},"moodle-mod_oublog-tagselector":{"requires":["base","node","autocomplete","autocomplete-filters","autocomplete-highlighters"]},"moodle-mod_quiz-autosave":{"requires":["base","node","event","event-valuechange","node-event-delegate","io-form"]},"moodle-mod_quiz-randomquestion":{"requires":["base","event","node","io","moodle-core-notification-dialogue"]},"moodle-mod_quiz-repaginate":{"requires":["base","event","node","io","moodle-core-notification-dialogue"]},"moodle-mod_quiz-quizquestionbank":{"requires":["base","event","node","io","io-form","yui-later","moodle-question-qbankmanager","moodle-core-notification-dialogue"]},"moodle-mod_quiz-questionchooser":{"requires":["moodle-core-chooserdialogue","moodle-mod_quiz-util","querystring-parse"]},"moodle-mod_quiz-dragdrop":{"requires":["base","node","io","dom","dd","dd-scroll","moodle-core-dragdrop","moodle-core-notification","moodle-mod_quiz-quizbase","moodle-mod_quiz-util-base","moodle-mod_quiz-util-page","moodle-mod_quiz-util-slot","moodle-course-util"]},"moodle-mod_quiz-util":{"requires":["node"],"use":["moodle-mod_quiz-util-base"],"submodules":{"moodle-mod_quiz-util-base":{},"moodle-mod_quiz-util-slot":{"requires":["node","moodle-mod_quiz-util-base"]},"moodle-mod_quiz-util-page":{"requires":["node","moodle-mod_quiz-util-base"]}}},"moodle-mod_quiz-modform":{"requires":["base","node","event"]},"moodle-mod_quiz-quizbase":{"requires":["base","node"]},"moodle-mod_quiz-toolboxes":{"requires":["base","node","event","event-key","io","moodle-mod_quiz-quizbase","moodle-mod_quiz-util-slot","moodle-core-notification-ajaxexception"]},"moodle-message_airnotifier-toolboxes":{"requires":["base","node","io"]},"moodle-block_navigation-navigation":{"requires":["base","io-base","node","event-synthetic","event-delegate","json-parse"]},"moodle-filter_glossary-autolinker":{"requires":["base","node","io-base","json-parse","event-delegate","overlay","moodle-core-event","moodle-core-notification-alert","moodle-core-notification-exception","moodle-core-notification-ajaxexception"]},"moodle-filter_mathjaxloader-loader":{"requires":["moodle-core-event"]},"moodle-editor_atto-editor":{"requires":["node","transition","io","overlay","escape","event","event-simulate","event-custom","yui-throttle","moodle-core-notification-dialogue","moodle-core-notification-confirm","moodle-editor_atto-rangy","handlebars","timers"]},"moodle-editor_atto-plugin":{"requires":["node","base","escape","event","event-outside","handlebars","event-custom","timers"]},"moodle-editor_atto-menu":{"requires":["moodle-core-notification-dialogue","node","event","event-custom"]},"moodle-editor_atto-rangy":{"requires":[]},"moodle-format_grid-gridkeys":{"requires":["event-nav-keys"]},"moodle-report_eventlist-eventfilter":{"requires":["base","event","node","node-event-delegate","datatable","autocomplete","autocomplete-filters"]},"moodle-report_loglive-fetchlogs":{"requires":["base","event","node","io","node-event-delegate"]},"moodle-gradereport_grader-gradereporttable":{"requires":["base","node","event","handlebars","overlay","event-hover"]},"moodle-gradereport_history-userselector":{"requires":["escape","event-delegate","event-key","handlebars","io-base","json-parse","moodle-core-notification-dialogue"]},"moodle-tool_capability-search":{"requires":["base","node"]},"moodle-tool_monitor-dropdown":{"requires":["base","event","node"]},"moodle-assignfeedback_editpdf-editor":{"requires":["base","event","node","io","graphics","json","event-move","event-resize","querystring-stringify-simple","moodle-core-notification-dialog","moodle-core-notification-exception","moodle-core-notification-ajaxexception"]},"moodle-atto_accessibilitychecker-button":{"requires":["color-base","moodle-editor_atto-plugin"]},"moodle-atto_accessibilityhelper-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_align-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_bold-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_charmap-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_clear-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_collapse-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_emoticon-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_equation-button":{"requires":["moodle-editor_atto-plugin","moodle-core-event","io","event-valuechange","tabview","array-extras"]},"moodle-atto_html-button":{"requires":["moodle-editor_atto-plugin","event-valuechange"]},"moodle-atto_image-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_indent-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_italic-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_link-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_managefiles-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_managefiles-usedfiles":{"requires":["node","escape"]},"moodle-atto_media-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_noautolink-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_orderedlist-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_rtl-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_strike-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_subscript-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_superscript-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_table-button":{"requires":["moodle-editor_atto-plugin","moodle-editor_atto-menu","event","event-valuechange"]},"moodle-atto_title-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_underline-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_undo-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_unorderedlist-button":{"requires":["moodle-editor_atto-plugin"]}}},"gallery":{"name":"gallery","base":"https:\/\/www.education-connect.org\/lib\/yuilib\/gallery\/","combine":true,"comboBase":"https:\/\/www.education-connect.org\/theme\/yui_combo.php?","ext":false,"root":"gallery\/1508158200\/","patterns":{"gallery-":{"group":"gallery"}}}},"modules":{"core_filepicker":{"name":"core_filepicker","fullpath":"https:\/\/www.education-connect.org\/lib\/javascript.php\/1508158200\/repository\/filepicker.js","requires":["base","node","node-event-simulate","json","async-queue","io-base","io-upload-iframe","io-form","yui2-treeview","panel","cookie","datatable","datatable-sort","resize-plugin","dd-plugin","escape","moodle-core_filepicker"]},"mathjax":{"name":"mathjax","fullpath":"https:\/\/cdn.mathjax.org\/mathjax\/2.5-latest\/MathJax.js?delayStartupUntil=configured"}}};
M.yui.loader = {modules: {}};

//]]>
</script>
<link href="https://www.education-connect.org/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.css" rel="stylesheet" type="text/css"/><script src="https://www.education-connect.org/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.js&amp;rollup/1508158200/mcore-min.js" type="text/javascript"></script><script src="https://www.education-connect.org/theme/jquery.php/core/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="https://www.education-connect.org/theme/jquery.php/theme_adaptable/pace-min.js" type="text/javascript"></script>
<script src="https://www.education-connect.org/theme/jquery.php/theme_adaptable/jquery-flexslider-min.js" type="text/javascript"></script>
<script src="https://www.education-connect.org/theme/jquery.php/theme_adaptable/tickerme.js" type="text/javascript"></script>
<script src="https://www.education-connect.org/theme/jquery.php/theme_adaptable/jquery-easing-min.js" type="text/javascript"></script>
<script src="https://www.education-connect.org/theme/jquery.php/theme_adaptable/adaptable.js" type="text/javascript"></script>
<script id="firstthemesheet" type="text/css">/** Required in order to fix style inclusion problems in IE with YUI **/</script><link href="https://www.education-connect.org/theme/styles.php/adaptable/1508158200/all" rel="stylesheet" type="text/css"/>
<script src="https://www.education-connect.org/lib/javascript.php/1508158200/lib/javascript-static.js" type="text/javascript"></script>
<meta content="71pqkg_7tNgjqNLTWLMZ6phnOW7VpItcIv8k4D2d53U" name="google-site-verification"/> <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
</head>
<body class="format-site course path-site dir-ltr lang-en yui-skin-sam yui3-skin-sam www-education-connect-org pagelayout-frontpage course-1 context-2 notloggedin two-column has-region-side-post used-region-side-post has-region-middle empty-region-middle has-region-frnt-footer empty-region-frnt-footer has-region-frnt-market-a empty-region-frnt-market-a has-region-frnt-market-b empty-region-frnt-market-b has-region-frnt-market-c empty-region-frnt-market-c has-region-frnt-market-d empty-region-frnt-market-d has-region-frnt-market-e empty-region-frnt-market-e has-region-frnt-market-f empty-region-frnt-market-f has-region-frnt-market-g empty-region-frnt-market-g has-region-frnt-market-h empty-region-frnt-market-h has-region-frnt-market-i empty-region-frnt-market-i has-region-frnt-market-j empty-region-frnt-market-j has-region-frnt-market-k empty-region-frnt-market-k has-region-frnt-market-l empty-region-frnt-market-l has-region-frnt-market-m empty-region-frnt-market-m has-region-frnt-market-n empty-region-frnt-market-n has-region-frnt-market-o empty-region-frnt-market-o has-region-frnt-market-p empty-region-frnt-market-p has-region-frnt-market-q empty-region-frnt-market-q has-region-frnt-market-r empty-region-frnt-market-r has-region-frnt-market-s empty-region-frnt-market-s has-region-frnt-market-t empty-region-frnt-market-t side-post-only layout-option-nonavbar" id="page-site-index">
<div class="skiplinks"><a class="skip" href="#maincontent">Skip to main content</a></div>
<script type="text/javascript">
//<![CDATA[
document.body.className += ' jsenabled';
//]]>
</script>
<div class="container-fluid fullin showblockicons" id="page">
<header id="page-header-wrapper">
<div id="above-header">
<div class="clearfix container userhead">
<div class="pull-left">
<ul class="usermenu2 nav navbar-nav navbar-right"></ul> </div>
<div class="headermenu row">
<form action="https://www.education-connect.org/login/index.php" method="post">
<input name="username" placeholder="Username" size="10" style="height: 12px; padding-bottom: 4px;" type="text"/>
<input name="password" placeholder="Password" size="10" style="height: 12px; padding-bottom: 4px;" type="password"/>
<button class="btn-login" type="submit">Log In</button>
</form>
</div>
<div style="float: right; position: relative; display: inline; margin-left: 15px; height:20px;">
</div>
</div>
</div>
<div class="clearfix container" id="page-header">
<div class="pull-left" id="titlecontainer"><div id="logocontainer"><a href="https://www.education-connect.org"><img alt="logo" id="logo" src="//www.education-connect.org/pluginfile.php/1/theme_adaptable/logo/1508158200/edconn%20logo.jpg"/></a></div></div>
<div class="socialbox pull-right"></div>
<div id="course-header">
</div>
</div>
</header>
<div class="slidewrap">
<div class="flexslider" id="main-slider">
<ul class="slides"><li><img alt="p1" src="//www.education-connect.org/pluginfile.php/1/theme_adaptable/p1/1508158200/Education%20Connect%20Banner.png"/></li><li><img alt="p2" src="//www.education-connect.org/pluginfile.php/1/theme_adaptable/p2/1508158200/Monet.png"/><div class="flex-caption">"the more I live, the more I regret how little i know" <p>                                                                         Claude Monet</p><p>                                                                      </p><p></p></div></li><li><img alt="p3" src="//www.education-connect.org/pluginfile.php/1/theme_adaptable/p3/1508158200/banner%20quote.png"/></li></ul></div></div>
<div class="container outercont">
<div class="row-fluid" id="page-content">
<div class="span12" id="page-navbar">
<nav class="breadcrumb-button"></nav>
<ul class="breadcrumb"><a href="https://www.education-connect.org/"><i class="fa fa-home fa-lg"></i></a></ul> </div>
<section id="region-main">
<div role="main"><span id="maincontent"></span><div class="box generalbox sitetopic"><ul class="section img-text"></ul></div><a class="skip-block" href="#skipavailablecourses">Skip available courses</a><div id="frontpage-course-list"><h2>Available courses</h2><div class="courses frontpage-course-list-all"><div class=" span4 panel panel-default coursebox hover"><a href="https://www.education-connect.org/course/view.php?id=23"><div class="panel-heading"></div></a><div class="panel-body clearfix"><div class="cimbox" style="background: #FFF url() no-repeat center center;
                                                          background-size: contain;"></div><div class="coursebox-content"><h3><a class="" href="https://www.education-connect.org/course/view.php?id=23">Instructional Design Presentation</a></h3><div class="summary"><p><strong>Instructional Design Presentation</strong></p></div></div><div class="boxfooter"></div><a class=" coursebtn submit btn btn-info btn-sm pull-right" href="https://www.education-connect.org/course/view.php?id=23"><span class="coursequicklink">Course <span class="fa fa-chevron-right"></span></span></a></div></div><div class=" span4 panel panel-default coursebox hover"><a href="https://www.education-connect.org/course/view.php?id=20"><div class="panel-heading"></div></a><div class="panel-body clearfix"><div class="cimbox" style="background: #FFF url(https://www.education-connect.org/pluginfile.php/879/course/overviewfiles/google%20calendar%20icon.jpg) no-repeat center center;
                                                                  background-size: contain;"></div><div class="coursebox-content"><h3><a class="" href="https://www.education-connect.org/course/view.php?id=20">Google Calendar</a></h3><div class="summary"><p><strong>Google Calendar</strong></p><span title="Google Calendar excels as an anytime, anywhere web and mobile app.">Google Calendar excels as an anytime, anywhere web and mobile app.</span><ul class="teachers"><li>Teacher: <a href="https://www.education-connect.org/user/view.php?id=2&amp;course=1">Carol Ashton</a></li></ul></div></div><div class="boxfooter"></div><a class=" coursebtn submit btn btn-info btn-sm pull-right" href="https://www.education-connect.org/course/view.php?id=20"><span class="coursequicklink">Course <span class="fa fa-chevron-right"></span></span></a></div></div><div class=" span4 panel panel-default coursebox hover"><a href="https://www.education-connect.org/course/view.php?id=18"><div class="panel-heading"></div></a><div class="panel-body clearfix"><div class="cimbox" style="background: #FFF url(https://www.education-connect.org/pluginfile.php/676/course/overviewfiles/Cyber%20Security.jpg) no-repeat center center;
                                                                  background-size: contain;"></div><div class="coursebox-content"><h3><a class="" href="https://www.education-connect.org/course/view.php?id=18">Cyber Security</a></h3><div class="summary"><p><strong>Cyber Security</strong></p><span title="Learn strategies to assess and address security risks">Learn strategies to assess and address security risks</span></div></div><div class="boxfooter"></div><a class=" coursebtn submit btn btn-info btn-sm pull-right" href="https://www.education-connect.org/course/view.php?id=18"><span class="coursequicklink">Course <span class="fa fa-chevron-right"></span></span></a></div></div><div class=" span4 panel panel-default coursebox hover"><a href="https://www.education-connect.org/course/view.php?id=17"><div class="panel-heading"></div></a><div class="panel-body clearfix"><div class="cimbox" style="background: #FFF url(https://www.education-connect.org/pluginfile.php/659/course/overviewfiles/Google%20Drive.png) no-repeat center center;
                                                                  background-size: contain;"></div><div class="coursebox-content"><h3><a class="" href="https://www.education-connect.org/course/view.php?id=17">Google Drive and Add-Ons</a></h3><div class="summary"><p><strong>Google Drive and Add-Ons</strong></p><span title="Learn how to enhance the functionality of Drive with Add-Ons">Learn how to enhance the functionality of Drive with Add-Ons</span></div></div><div class="boxfooter"></div><a class=" coursebtn submit btn btn-info btn-sm pull-right" href="https://www.education-connect.org/course/view.php?id=17"><span class="coursequicklink">Course <span class="fa fa-chevron-right"></span></span></a></div></div><div class=" span4 panel panel-default coursebox hover"><a href="https://www.education-connect.org/course/view.php?id=16"><div class="panel-heading"></div></a><div class="panel-body clearfix"><div class="cimbox" style="background: #FFF url(https://www.education-connect.org/pluginfile.php/639/course/overviewfiles/Managing%20Google%20Drive.png) no-repeat center center;
                                                                  background-size: contain;"></div><div class="coursebox-content"><h3><a class="" href="https://www.education-connect.org/course/view.php?id=16">Managing and Organizing Google Drive</a></h3><div class="summary"><p><strong>Managing and Organizing Google Drive</strong></p><span title="Learn techniques to manage and organize the Google Drive interface.">Learn techniques to manage and organize the Google Drive interface.</span></div></div><div class="boxfooter"></div><a class=" coursebtn submit btn btn-info btn-sm pull-right" href="https://www.education-connect.org/course/view.php?id=16"><span class="coursequicklink">Course <span class="fa fa-chevron-right"></span></span></a></div></div></div></div><span class="skip-block-to" id="skipavailablecourses"></span><br/><div class="box mdl-align"><form action="https://www.education-connect.org/course/search.php" class="form-inline" id="coursesearch" method="get" role="form"><div class="form-group"><label class="sr-only" for="coursesearchbox">Search courses</label><input class="form-control" id="coursesearchbox" name="search" placeholder="Search courses" size="30" type="text" value=""/></div><button class="btn btn-default" type="submit">Go</button></form></div><br/></div></section>
<aside class="span3 block-region" data-blockregion="side-post" data-droptarget="1" id="block-region-side-post"><a class="skip-block" href="#sb-1">Skip Navigation</a><div aria-labelledby="instance-4-header" class="block_navigation block" data-block="navigation" data-dockable="1" data-instanceid="4" id="inst4" role="navigation"><div class="header"><div class="title"><div class="block_action"></div><h2 id="instance-4-header">Navigation</h2></div></div><div class="content"><ul class="block_tree list"><li aria-expanded="true" class="type_unknown depth_1 contains_branch current_branch"><p class="tree_item branch active_tree_node canexpand navigation_node"><a href="https://www.education-connect.org/">Home</a></p><ul><li class="type_unknown depth_2 item_with_icon"><p class="tree_item leaf hasicon"><a href="https://www.education-connect.org/blog/index.php"><img alt="" class="smallicon navicon" src="https://www.education-connect.org/theme/image.php/adaptable/core/1508158200/i/navigationitem" title=""/><span class="item-content-wrap">Site blogs</span></a></p></li><li class="type_activity depth_2 item_with_icon"><p class="tree_item leaf hasicon"><a href="https://www.education-connect.org/mod/forum/view.php?id=270" title="Forum"><img alt="Forum" class="smallicon navicon" src="https://www.education-connect.org/theme/image.php/adaptable/forum/1508158200/icon" title="Forum"/><span class="item-content-wrap">Site news</span></a></p></li><li aria-expanded="false" class="type_system depth_2 collapsed contains_branch"><p class="tree_item branch" id="expandable_branch_0_courses"><a href="https://www.education-connect.org/course/index.php">Courses</a></p></li></ul></li></ul></div></div><span class="skip-block-to" id="sb-1"></span></aside></div>
</div>
<footer id="page-footer">
<div id="course-footer"></div>
<div class="container blockplace1"><div class="row-fluid"><div class="left-col span3"><h3 title=""></h3> </div><div class="left-col span3"><h3 title=""></h3> </div><div class="left-col span3"><h3 title=""></h3> </div><div class="left-col span3"><h3 title=""></h3> </div></div></div> <div class="info container2 clearfix">
<div class="container">
<div class="row-fluid">
<div class="span4">
</div>
<div class="span4 helplink">
</div>
<div class="span4">
</div>
</div>
</div>
</div>
</footer>
<a class="back-to-top" href="#top"><i class="fa fa-angle-up "></i></a>
<script type="text/javascript">
//<![CDATA[
var require = {
    baseUrl : 'https://www.education-connect.org/lib/requirejs.php/1508158200/',
    // We only support AMD modules with an explicit define() statement.
    enforceDefine: true,
    skipDataMain: true,

    paths: {
        jquery: 'https://www.education-connect.org/lib/javascript.php/1508158200/lib/jquery/jquery-1.11.2.min',
        jqueryui: 'https://www.education-connect.org/lib/javascript.php/1508158200/lib/jquery/ui-1.11.4/jquery-ui.min',
        jqueryprivate: 'https://www.education-connect.org/lib/javascript.php/1508158200/lib/requirejs/jquery-private'
    },

    // Custom jquery config map.
    map: {
      // '*' means all modules will get 'jqueryprivate'
      // for their 'jquery' dependency.
      '*': { jquery: 'jqueryprivate' },

      // 'jquery-private' wants the real jQuery module
      // though. If this line was not here, there would
      // be an unresolvable cyclic dependency.
      jqueryprivate: { jquery: 'jquery' }
    }
};

//]]>
</script>
<script src="https://www.education-connect.org/lib/javascript.php/1508158200/lib/requirejs/require.min.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
require(['core/first'], function() {
;
require(["core/log"], function(amd) { amd.setConfig({"level":"warn"}); });
});
//]]>
</script>
<script src="https://www.education-connect.org/lib/javascript.php/1508158200/filter/tabs/js/bootstrap-2.3.2-tab.min.js" type="text/javascript"></script>
<script src="https://www.education-connect.org/theme/javascript.php/adaptable/1508158200/footer" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
M.str = {"moodle":{"lastmodified":"Last modified","name":"Name","error":"Error","info":"Information","ok":"OK","viewallcourses":"View all courses","morehelp":"More help","loadinghelp":"Loading...","cancel":"Cancel","yes":"Yes","confirm":"Confirm","no":"No","areyousure":"Are you sure?","closebuttontitle":"Close","unknownerror":"Unknown error"},"repository":{"type":"Type","size":"Size","invalidjson":"Invalid JSON string","nofilesattached":"No files attached","filepicker":"File picker","logout":"Logout","nofilesavailable":"No files available","norepositoriesavailable":"Sorry, none of your current repositories can return files in the required format.","fileexistsdialogheader":"File exists","fileexistsdialog_editor":"A file with that name has already been attached to the text you are editing.","fileexistsdialog_filemanager":"A file with that name has already been attached","renameto":"Rename to \"{$a}\"","referencesexist":"There are {$a} alias\/shortcut files that use this file as their source","select":"Select"},"block":{"addtodock":"Move this to the dock","undockitem":"Undock this item","dockblock":"Dock {$a} block","undockblock":"Undock {$a} block","undockall":"Undock all","hidedockpanel":"Hide the dock panel","hidepanel":"Hide panel"},"langconfig":{"thisdirectionvertical":"btt"},"admin":{"confirmation":"Confirmation"}};
//]]>
</script>
<script type="text/javascript">
//<![CDATA[
var navtreeexpansions4 = [{"id":"expandable_branch_0_courses","key":"courses","type":0}];
//]]>
</script>
<script type="text/javascript">
//<![CDATA[
YUI().use('node', function(Y) {
M.util.load_flowplayer();
setTimeout("fix_column_widths()", 20);
Y.use("moodle-filter_mathjaxloader-loader",function() {M.filter_mathjaxloader.configure({"mathjaxconfig":"MathJax.Hub.Config({\r\n    config: [\"Accessible.js\", \"Safe.js\"],\r\n    errorSettings: { message: [\"!\"] },\r\n    skipStartupTypeset: true,\r\n    messageStyle: \"none\"\r\n});\r\n","lang":"en"});
});
Y.use("moodle-filter_glossary-autolinker",function() {M.filter_glossary.init_filter_autolinking({"courseid":0});
});
Y.use("moodle-core-dock-loader",function() {M.core.dock.loader.initLoader();
});
Y.use("moodle-theme_adaptable-zoom",function() {M.theme_adaptable.zoom.init();
});
Y.use("moodle-theme_adaptable-full",function() {M.theme_adaptable.full.init();
});
Y.use("moodle-block_navigation-navigation",function() {M.block_navigation.init_add_tree({"id":"4","instance":"4","candock":true,"courselimit":"20","expansionlimit":0});
});
Y.use("moodle-block_navigation-navigation",function() {M.block_navigation.init_add_tree({"id":"5","instance":"5","candock":true});
});
M.util.help_popups.setup(Y);
Y.use("moodle-core-popuphelp",function() {M.core.init_popuphelp();
});
M.util.init_block_hider(Y, {"id":"inst4","title":"Navigation","preference":"block4hidden","tooltipVisible":"Hide Navigation block","tooltipHidden":"Show Navigation block"});
 M.util.js_pending('random5ffdbb09471b43'); Y.on('domready', function() { M.util.js_complete("init");  M.util.js_complete('random5ffdbb09471b43'); });

});
//]]>
</script>
</div>
</body>
</html>
