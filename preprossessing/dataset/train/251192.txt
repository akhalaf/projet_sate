<!DOCTYPE html>
<html lang="en"><head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><base href="https://www.borbet.de/"/><title>BORBET - alloy wheels in perfection</title><meta content="Alloy wheels in perfection. BORBET is one of the world's leading manufacturers of alloy wheels." name="description"/><meta content="Alloy Wheels, Wheel Configurator, Wheel Program, Wheel Tire Combination, Manufacturer" name="keywords"/><meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet"/>
<!--  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic&amp;subset=latin">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">
  -->
<link href="assets/bootstrap-material-design-font/css/material.css" rel="stylesheet"/>
<link href="assets/et-line-font-plugin/style.css" rel="stylesheet"/>
<link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<link href="assets/linecons/style.css" rel="stylesheet"/>
<link href="assets/icon54/style.css" rel="stylesheet"/>
<link href="assets/icon54-v2/style.css" rel="stylesheet"/>
<link href="assets/tether/tether.min.css" rel="stylesheet"/>
<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<link href="assets/animate.css/animate.min.css" rel="stylesheet"/>
<link href="assets/dropdown/css/style.css" rel="stylesheet"/>
<link href="assets/socicon/css/styles.css" rel="stylesheet"/>
<link href="assets/asPieProgress/css/progress.min.css" rel="stylesheet"/>
<link href="assets/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet"/>
<link href="assets/theme/css/style.css" rel="stylesheet"/>
<link href="assets/mobirise3-blocks-plugin/css/style.css" rel="stylesheet"/>
<link href="assets/mobirise-gallery/style.css" rel="stylesheet"/>
<link href="assets/mobirise/css/mbr-additional.css" rel="stylesheet" type="text/css"/>
<link href="css/customize.css" rel="stylesheet" type="text/css"/>
<link href="css/chatslider.css" rel="stylesheet" type="text/css"/>
<link href="css/leaflet.css" rel="stylesheet"/>
<link href="css/lightbox.min.css" rel="stylesheet"/>
<link href="css/menu.css" rel="stylesheet" type="text/css"/>
<link href="css/chat.css" rel="stylesheet" type="text/css"/>
<link href="plugins/BorbetTimeLine/template/style.css" rel="stylesheet" type="text/css"/>
<script src="js/leaflet/leaflet.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<!--  
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic&amp;subset=latin">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">
<link rel="stylesheet" href="../assets/bootstrap-material-design-font/css/material.css">
<link rel="stylesheet" href="../assets/et-line-font-plugin/style.css">
<link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../assets/linecons/style.css">
<link rel="stylesheet" href="../assets/icon54/style.css">
<link rel="stylesheet" href="../assets/icon54-v2/style.css">
<link rel="stylesheet" href="../assets/tether/tether.min.css">
<link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/animate.css/animate.min.css">
<link rel="stylesheet" href="../assets/dropdown/css/style.css">
<link rel="stylesheet" href="../assets/socicon/css/styles.css">
<link rel="stylesheet" href="../assets/asPieProgress/css/progress.min.css">
<link rel="stylesheet" href="../assets/dataTables/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/theme/css/style.css">
<link rel="stylesheet" href="../assets/mobirise3-blocks-plugin/css/style.css">
<link rel="stylesheet" href="../assets/mobirise-gallery/style.css">
<link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">  
<link rel="stylesheet" href="../css/customize.css" type="text/css">
<link rel="stylesheet" href="../css/chatslider.css" type="text/css">
<link rel="stylesheet" type="../text/css" href="css/leaflet.css" />
<link rel="stylesheet" href="../css/lightbox.min.css">
<link rel="stylesheet" href="../css/menu.css" type="text/css">
-->
<link href="css/customize.css" rel="stylesheet" type="text/css"/>
</head><body><section class="extMenu1" id="extMenu1-2">
<div class="topbar" style="font-size: 25px;">
<a class="no-underline" href="en/products/watchlist.php" style="color: #fff;"><i aria-hidden="true" class="fa fa-star"></i> <span class="badge" style="color: #165096; background-color: #fff;">0</span></a>  <a class="no-underline" href="en/products/shopping-cart.php" style="color: #fff;"><i aria-hidden="true" class="fa fa-shopping-cart"></i> <span class="badge" style="color: #165096; background-color: #fff;">0</span></a>  <a class="" href="de/" style=""><img src="images/icon-german.png" style="height: 25px; margin-top: -4px;"/></a>
<!--  
<a href="index.php?page=merkliste" class="icon54-bookmark-3" style="color: #fff;"><span class="badge" style="color: #165096; background-color: #fff;">0</span></a>&nbsp;&nbsp;
<a href="#" class="icon54-cart" style="color: #fff;"><span class="badge" style="color: #165096; background-color: #fff;">0</span></a>&nbsp;&nbsp;

<a href="#" class="icon54-phone-intalk" style="color: #fff;"></a>&nbsp;&nbsp;&nbsp;
<a href="#" class="icon54-cart" style="color: #fff;"><span class="badge">0</span></a>&nbsp;&nbsp;&nbsp;
<a href="#" class="" style="color: #fff;">en</a>
-->
</div>
</section>
<div class="borbetnav" style="">
<nav class="navbar navbar-default">
<div class="">
<div class="navbar-header">
<button class="navbar-toggle collapsed" data-target="#navbar3" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<div class="hidden-md-down">
<a class="navbar-brand" href="https://www.borbet.de/en/"><img src="images/logo-borbet.png" style="max-height: 30px; max-width: 250px; margin-top: 17px;"/></a>
</div>
<div class="hidden-lg-up">
<a class="navbar-brand" href="https://www.borbet.de/en/"><img src="images/logo-borbet.png" style="max-height: 28px; max-width: 220px; margin-top: 25px;"/></a>
</div>
</div>
<div class="navbar-collapse collapse" id="navbar3">
<ul class="nav navbar-nav navbar-right">
<!--  <li><a href="#">Start</a></li> -->
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="index.php"><img src="images/logo-small.jpg" style="height: 35px; margin-top: -5px;"/> PRODUCTS</a>
<ul class="dropdown-menu navleft">
<li><a href="https://www.borbet.de/en/products/program/">After Market Program</a></li>
<li><a href="http://shop.borbet.de" target="_blank">B2B Shop</a></li>
<li><a href="https://www.borbet.de/en/configurator/">Configurator</a></li>
<li><a href="https://www.borbet.de/en/products/retailersearch/">Retailer search</a></li>
<li><a href="https://www.borbet.de/en/shop/">Shop</a></li>
<li><a href="https://www.borbet.de/en/products/radcarsten/">RadCarsten</a></li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="index.php">COMPANY</a>
<ul class="dropdown-menu navleft">
<li><a href="https://www.borbet.de/en/company/values-history/">Values &amp; history</a></li>
<li><a href="https://www.borbet.de/en/company/sustainability/">Sustainability</a></li>
<li><a href="https://www.borbet.de/en/company/locations-facts-figures/">Locations, facts &amp; figures</a></li>
<li><a href="https://www.borbet.de/en/company/management-politics/">Management politics</a></li>
<li><a href="https://www.borbet.de/en/company/virtual-production.php">Virtual production</a></li>
<li><a href="https://www.borbet.de/en/company/partnerships-awards/">Parnerships &amp; awards</a></li>
<li><a href="https://www.borbet.de/en/company/sponsoring/">Sponsoring</a></li>
<li><a href="https://www.borbet.de/en/company/contact/">Contact</a></li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="index.php">CAREER</a>
<ul class="dropdown-menu navleft">
<li><a href="https://www.borbet.de/en/jobs/">Job offers</a></li>
<li><a href="https://www.borbet.de/en/career/work-at-borbet/">Working at BORBET</a></li>
<li><a href="https://www.borbet.de/en/career/examples/">Examples</a></li>
<li><a href="https://www.borbet.de/en/career/candidature/">Candidature</a></li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="index.php">CURRENT</a>
<ul class="dropdown-menu navleft">
<li><a href="https://www.borbet.de/en/news/">News</a></li>
<li><a href="https://www.borbet.de/en/current/events/">Events</a></li>
</ul>
</li>
<li><a href="https://www.borbet.de/en/partner/partner-suppliers/">PARTNER</a></li>
<!-- 
          <li class="dropdown">
          <a href="index.php" class="dropdown-toggle" data-toggle="dropdown">PARTNER</a>
          <ul class="dropdown-menu navleft">
            <li><a href="index.php">Suppliers</a></li>
            <li><a href="index.php">Customer</a></li>
          </ul>
          </li>
          -->
</ul>
</div>
<!--/.nav-collapse -->
</div>
<!--/.container-fluid -->
</nav>
</div>
<section class="mbr-section mbr-section-hero mbr-section-full mbr-section-with-arrow extHeader3" data-vide-bg="https://www.borbet.de/assets/videos/This-is-BORBET.mp4" id="header4-1a">
<video autoplay="" id="myVideo" loop="" muted="" style="position: absolute; right: 0; bottom: 0; min-width: 100%; min-height: 100%;">
<source src="assets/videos/This-is-BORBET.mp4" type="video/mp4">
</source></video>
<div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(22, 80, 150);"></div>
<div class="mbr-table-cell">
<div class="container" style="padding-top: 120px;">
<div class="mbr-section">
<div class="col-xs-12">
<h3 class="hidden-sm-down mbr-section-title display-2 text-center"><span style="font-size: 6vh; font-weight: bold; font-family: 'Nunito', sans-serif;">Alloy wheels in perfection.</span></h3>
<h3 class="hidden-md-up mbr-section-title display-2 text-center"><span style="font-size: 5vh; font-weight: bold; font-family: 'Nunito', sans-serif;">Alloy wheels in perfection.</span></h3>
<div class="hidden-sm-down mbr-section-text lead text-center">
<p style="font-weight: normal; font-family: 'Nunito', sans-serif;">Discover the world of BORBET.<br/></p><br/><br/>
</div>
<div class="hidden-md-up mbr-section-text lead text-center">
<p style="font-size: 20px; font-weight: normal; font-family: 'Nunito', sans-serif;">Discover the world of BORBET.<br/></p><br/><br/>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6 col-md-6 col-lg-2 col-lg-offset-1"><a href="en/products/navigation/"><img class="img-responsive center-block wl-transparent wl-menubutton" src="../images/icons/produkte-en.png" style=""/></a></div>
<div class="col-sm-6 col-md-6 col-lg-2"><a href="en/company/navigation/"><img class="img-responsive center-block wl-transparent wl-menubutton" src="../images/icons/unternehmen-en.png" style=""/></a></div>
<div class="col-sm-6 col-md-6 col-lg-2"><a href="en/career/navigation/"><img class="img-responsive center-block wl-transparent wl-menubutton" src="../images/icons/karriere-en.png" style=""/></a></div>
<div class="col-sm-6 col-md-6 col-lg-2"><a href="en/current/navigation/"><img class="img-responsive center-block wl-transparent wl-menubutton" src="../images/icons/aktuell-en.png" style=""/></a></div>
<div class="col-sm-6 col-md-6 col-lg-2"><a href="en/partner/partner-suppliers/"><img class="img-responsive center-block wl-transparent wl-menubutton" src="../images/icons/partner-en.png" style=""/></a></div>
</div>
</div>
</div>
</section>
<div class="container">
<div class="col-lg-12" style="margin-top: 30px;">
<!--  
 	 <div class="col-xs-12 col-lg-6" style="background-color: #fff; padding: 15px;">
	  <a href="index.php?page=produkte/programm"><img class="img-responsive" src="images/start-programm.jpg"></a>
       <div class="startimage_overlay">
    	<div class="startimage_text">PROGRAMM</div>
  	   </div>
     </div>
 	 -->
<div class="col-xs-12 col-lg-4" style="background-color: #fff; padding: 15px;">
<div class="div-img-hover" style="overflow: hidden;">
<a href="https://www.borbet.de/en/products/program/"><img class="img-responsive" src="../images/start-programm.jpg"/></a>
<div class="startimage_overlay" onclick="window.open('https://www.borbet.de/en/products/program/','_self','');">
<div class="startimage_text">PROGRAM</div>
</div>
</div>
</div>
<div class="col-xs-12 col-lg-4" style="background-color: #fff; padding: 15px;">
<div class="div-img-hover" style="overflow: hidden;">
<a href="https://www.borbet.de/en/career/navigation/"><img class="img-responsive startimage" src="../images/start-karriere.jpg"/></a>
<div class="startimage_overlay" onclick="window.open('https://www.borbet.de/en/career/navigation/','_self','');">
<div class="startimage_text">CAREER</div>
</div>
</div>
</div>
<div class="col-xs-12 col-lg-4" style="background-color: #fff; padding: 15px;">
<div class="div-img-hover" style="overflow: hidden;">
<a href="https://www.borbet.de/en/current/news/"><img class="img-responsive" src="../images/start-news.jpg"/></a>
<div class="startimage_overlay" onclick="window.open('https://www.borbet.de/en/current/news/','_self','');">
<div class="startimage_text">NEWS</div>
</div>
</div>
</div>
<div class="col-xs-12 col-lg-4" style="background-color: #fff; padding: 15px;">
<div class="div-img-hover" style="overflow: hidden;">
<a href="https://www.borbet.de/en/current/events/"><img class="img-responsive" src="../images/start-termine.jpg"/></a>
<div class="startimage_overlay" onclick="window.open('https://www.borbet.de/en/current/events/','_self','');">
<div class="startimage_text">EVENTS</div>
</div>
</div>
</div>
<div class="col-xs-12 col-lg-4" style="background-color: #fff; padding: 15px;">
<div class="div-img-hover" style="overflow: hidden;">
<a href="https://www.borbet.de/en/company/virtual-production/"><img class="img-responsive" src="../images/start-3dwelt.jpg"/></a>
<div class="startimage_overlay" onclick="window.open('https://www.borbet.de/en/company/virtual-production/','_self','');">
<div class="startimage_text text-center">VIRTUAL<br/>PRODUCTION</div>
</div>
</div>
</div>
<div class="col-xs-12 col-lg-4" style="background-color: #fff; padding: 15px;">
<div class="div-img-hover" style="overflow: hidden;">
<a href="https://www.borbet.de/en/products/webshop/"><img class="img-responsive" src="../images/start-shop.jpg"/></a>
<div class="startimage_overlay" onclick="window.open('https://www.borbet.de/en/products/webshop/','_self','');">
<div class="startimage_text">SHOP</div>
</div>
</div>
</div>
</div>
</div>
<!--  
<script async type="text/javascript" src="//userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/0ba6f43008bb521c301cd370fcf487d5c124ca7c56c5003b81412641dde20952.js"></script>
-->
<section class="mbr-section mbr-section-md-padding" id="social-buttons4-w" style="background-color: #fff; padding-top: 50px; padding-bottom: 30px;">
<div class="container">
<div class="col-xs-12">
<div class="row">
<div class="col-xs-12 text-center" style="">
<div style="border-top: 1px #165096 solid; padding-bottom: 30px;"></div>
<h3 class="mbr-section-title display-2" style="color: #165096; font-family: 'Nunito', sans-serif; font-weight: bold; font-size: 30px !important;">FOLLOW US</h3>
<div>
<a class="btn btn-social" href="https://www.facebook.com/BORBETGmbH/" style="padding-top: 0px;" target="_blank" title="Facebook"><i class="socicon socicon-facebook"></i></a>
<a class="btn btn-social" href="https://www.youtube.com/channel/UC3R7jfCXc7PSujfzbDRBbyg" style="padding-top: 0px;" target="_blank" title="YouTube"><i class="socicon socicon-youtube"></i></a>
<a class="btn btn-social" href="http://www.instagram.com/borbet_wheels/" style="padding-top: 0px;" target="_blank" title="Instagram"><i class="socicon socicon-instagram"></i></a>
</div>
</div>
</div>
</div>
<section class="mbr-section mbr-section-md-padding" id="social-buttons4-w" style="background-color: #fff; padding-top: 40px; padding-bottom: 90px;"><div class="container"><div class="col-xs-12 col-md-6 col-md-offset-3 text-center"><h3 class="mbr-section-title display-2" style="color: #165096; font-family: 'Nunito', sans-serif; font-weight: bold; font-size: 30px !important;">BORBET NEWSLETTER</h3>Be always up-to-date with the BORBET newsletter - new products, news about the company or exclusive specials.<br/><br/><a class="btn btn-primary" href="https://www.borbet.de/en/company/newsletter/">Sign up</a></div></div></section><footer class="mbr-small-footer mbr-section mbr-section-nopadding csl-footer" id="footer1-z" style="background-color: #333; padding-top: 50px; padding-bottom: 100px;">
<div class="container">
<div class="col-xs-12 col-md-6 col-lg-3" style="font-size: 25px; padding-bottom: 30px;">
<h2>DISCOVER THE WORLD OF <b>BORBET</b></h2>
</div>
<div class="col-xs-12 col-md-6 col-lg-3" style="">
<h2 style="font-size: 30px;">PRODUCTS</h2>
<a class="link_footer" href="https://www.borbet.de/en/products/program/">Program</a><br/>
<a class="link_footer" href="http:/shop.borbet.de" target="_blank">B2B-Shop</a><br/>
<a class="link_footer" href="https://www.borbet.de/en/products/retailersearch/">Retailer search</a><br/>
<a class="link_footer" href="https://www.borbet.de/en/products/webshop/">Shop</a><br/>
<a class="link_footer" href="https://www.borbet.de/en/configurator/">Configurator</a>
<br/>
<a class="link_footer" href="https://www.borbet.de/en/products/radcarsten/">RadCarsten</a><br/>
<br/>
</div>
<div class="col-xs-12 col-md-6 col-lg-3" style="">
<h2 style="font-size: 30px;">CAREER</h2>
<a class="link_footer" href="https://www.borbet.de/en/career/job-offers/">Job offers</a><br/>
<a class="link_footer" href="https://www.borbet.de/en/career/work-at-borbet/">Work at BORBET</a><br/>
<a class="link_footer" href="https://www.borbet.de/en/career/examples/">Examples</a><br/>
<a class="link_footer" href="https://www.borbet.de/en/career/candidature/">Candidature</a><br/>
<br/>
</div>
<div class="col-xs-12 col-md-6 col-lg-3" style="">
<h2 style="font-size: 30px;">COMPANY</h2>
<a class="link_footer" href="https://www.borbet.de/en/current/news/">News</a><br/>
<a class="link_footer" href="https://www.borbet.de/en/current/events/">Events</a><br/>
<a class="link_footer" href="https://www.borbet.de/en/company/values-history/">Values &amp; history</a><br/>
<a class="link_footer" href="https://www.borbet.de/en/company/locations-facts-figures/">Locations, facts &amp; figures</a><br/>
<a class="link_footer" href="https://www.borbet.de/en/company/contact/">Contact</a><br/>
<br/>
</div>
<div class="col-xs-12 hidden-sm-down" style="margin-top: 30px;">
<a class="link_footer" href="https://www.borbet.de/en/company/about-us/">about us</a>
<a class="link_footer" href="https://www.borbet.de/en/company/data-protection/">data protection</a>
<a class="link_footer" href="https://www.borbet.de/en/company/terms-and-conditions/">terms and conditions</a>
<br/><br/>© 2020 BORBET GmbH 
        </div>
<div class="col-xs-12 hidden-md-up" style="margin-top: 30px;">
<a class="link_footer" href="https://www.borbet.de/en/company/about-us/">about us</a><br/>
<a class="link_footer" href="https://www.borbet.de/en/company/data-protection/">data protection</a><br/>
<a class="link_footer" href="https://www.borbet.de/en/company/terms-and-conditions/">terms and conditions</a><br/>
        © 2020 BORBET GmbH 
        </div>
</div>
</footer>
<script>
function closeCockieMessage() {

	var x = document.getElementById("div_cookiemessage");
	x.style.display = "none";
	$.post("https://www.borbet.de/closecookiemessage/");
}
</script>
<div class="cookiemessage" id="div_cookiemessage"><div class="container"><div class="col-xs-8 col-md-10">This website uses cookies. By using the website you agree to the use of cookies.<br/><a href="https://www.borbet.de/en/company/about-us/">about us</a> | <a href="https://www.borbet.de/en/company/data-protection/">data privacy</a></div><div class="col-xs-4 col-md-2 text-right"><a class="btn btn-sm btn-primary" href="javascript: closeCockieMessage();" style="color: #fff; font-family: 'Nunito', sans-serif;">X</a></div></div></div><!--  
<div id="konfiguratorModal" class="modal fade" role="dialog" style="z-index:10000000;">
  <div class="modal-dialog modal-lg" style="width: 1070px;">


    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Configurator</h4>
      </div>
      <div class="modal-body" style="height: 800px; width: 1070px;">
       <iframe src="http://konfigurator.borbet.de/" style="zoom:0.60" width="99.6%" height="100%" frameborder="0"></iframe>
      </div>
    </div>

  </div>
</div>
-->
</div></section></body> <script src="../assets/web/assets/jquery/jquery.min.js"></script>
<!-- 
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/smooth-scroll/smooth-scroll.js"></script> 
  <script src="assets/viewport-checker/jquery.viewportchecker.js"></script>
  <script src="assets/jarallax/jarallax.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script> 					// Paralax Effekt
 -->
<script src="js/lightbox-plus-jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/jquery-mb-ytplayer/jquery.mb.ytplayer.min.js"></script>
<script src="assets/touch-swipe/jquery.touch-swipe.min.js"></script>
<script src="assets/masonry/masonry.pkgd.min.js"></script>
<script src="assets/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
<script src="assets/asPieProgress/jquery-asPieProgress.min.js"></script>
<script src="assets/dataTables/jquery.dataTables.min.js"></script>
<script src="assets/dataTables/dataTables.bootstrap4.min.js"></script>
<script src="assets/theme/js/script.js"></script>
<script src="assets/mobirise3-blocks-plugin/js/script.js"></script>
<script src="assets/formoid/formoid.min.js"></script>
<script src="js/webdesign-lange.js"></script>
<script src="assets/mobirise-gallery/player.min.js"></script>
<script src="assets/mobirise-gallery/script.js"></script>
<script src="assets/mobirise-slider-video/script.js"></script>
<script type="text/javascript">
 function changeIcon() {

	 $('#nav-icon4').toggleClass('open');

}
 </script>
<script language="JavaScript" type="text/javascript">


		function showChatBox(checkbox) {
			//alert("jo geht");
	        document.getElementById('chatbox').style.display="block";
 
   		} // end function

		function closeChatBox(checkbox) {
			//alert("jo geht");
	        document.getElementById('chatbox').style.display="none";
 
   		} // end function


   		//function showloading() {
			//alert("jo geht");
	        //document.getElementById('divloading').style.display="block";
   			//$(".se-pre-con").fadeIn("fast");;
	        //document.getElementById('dseprecon').style.visible="initial";
   		 //} // end function
   		
</script>
</html>
