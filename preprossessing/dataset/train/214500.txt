<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "87242",
      cRay: "6110da747e462290",
      cHash: "4a3b2d9d60540e3",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmV4YXIub3JnLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "Frm7BMZeHU3LP6GJ/JBfDi+xy8lypB+l4StnGQWnHcqk3pAWJGY+GR/2CMQm5ERbLXIefAGZbLgDajCe/IbLxwllEIQo601zMFv80SjQstFukMdpjzhUJjAhsBvC0xALl1eHtE1LvFC+i3ySBvQtzg8Btx8Wl109CyW6KHMFWFGMLP+os1j0dkY2LNS7mU8jRBPZKQK5vKWkiOGHmNNKS0TWGq9N1mGSVwrfieGwiF2oC5/qrvN9nWdjCXEUYk0D6SMYJ5YImIPEwnyN1XWhKGUAWWZzUfQleOu+mLNXmBZ56wql32ul+3S8IJsvvheLiMS8Zuh3g996aBmDEVkxVQ+dRCZnz2LJISBQ7qR2hcMLn4tXS0V0WJbaobntycKtc9YHuRZEAOezezK7t/5fzKgctpZDkBvLj47rNntO5D9Q59NXxKYPAQIsKX3dy77wW3Eo2IhuyrL0nyxzSqf5se6n2B+V+LDlZAsDn8yL3j1LfzB7eLYpceiP0KnJBsZBxyIrdChkREp1tqjPLY56cgXMQOPftvJQx/nZt3SVn6rV4NJjmsX5DYUx5trrMOgA9iUdCbRoD7lakD6u/8aOe4gRZZaQJKDBKxHMfgrZkRwWg4wyNlHLQNP9wmNUucM7v51004EvctofJD58JLxFMvxU5TVtAZRsVRZkp2pD/rZSoZ7ZFQNT4+82rQ2nKqE9mxFWYpVr/hPsZ2k04wZ7PMAESD+1HAh7RFPlq+XjcYIyryyOG8EWWoiVvTM58Ypp",
        t: "MTYxMDU1OTExMi40MTEwMDA=",
        m: "PZDPiZqHOqnjo/M6I4fV0omtWbf24PA6eKdY3U52WqQ=",
        i1: "f/RxSJSpvWLg6pTput0gBw==",
        i2: "Teg8LFrxRmYv01bpx8KDaQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "79ZhP9XPyUFsxVEz6u8iXXMaghUPSqQzvZGKQiBKaUE=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.bexar.org</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=9b2de4f67cc05dd36826da3dd45b715d6f804fbb-1610559112-0-ARx53CWckdhqZHbnb7JdPzc3lFd-r_Cuz8tf3GM2Y_h6HBF_aJII_9MRlrUrgpKrX_FJOR_TPhMhSsl29oGZbFXq4liuwchN5GwGso-uTb0fQUrOX9J3TVrlfGetRpq_Eh4MCzt_2auTToITrfp9Q9hWrTBXjx6r3C842PrEzWZiFMH_Q0IeygJmPs4dzSJkJKmVG90CgRFKRMHYTgJIcZgvlL15dg2SfFLcxolEvj5HlkqFstIZgZFU2FBGHE3RHZLzrhCOtyIuKPeuZd-_R8RdQBWC9tHc9Lrh9NeUTKhEv2ZaXfCGKjXP6j9x6DsJL-MVlpsmZRtQAda6o_FGBiBPuZiO3h75lz35dPTsyCrIaRbwBv34GFsun6PjsJJyKFjE57YlRXGgHypeoNddRBJTY-PFkQyQcBy-Jugz_5eBrwUr1DYzhdzcaGEgy61QQ23usBxsiA-Bpdlzpqo5N8BynHwKKs5jSW3Yh_Q_rsxQZ6NyBoR9Yf3gdEx5miyhHVzJz6i5M-IRiFmC5CU_epU" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="83b4bc9b375df4cebc975f96b20a6c5b48fb69b4-1610559112-0-AXLoTtbbxj/Er49wxnBEEOOEh0c+3U2KmfNP1l6LTtSQHvae+OJl0ohyFd34cx7VBP1E1itSyElMqa46GgO31Pj9slDNwLO7N7P5JC9mNwGCt9HWfecxRSttFyJJ/gJXYmkrzj9l6x7jjSrVR/Dpk/4B6Kgv4QADoL76rmcPhQXcnzB7Hy/BSuMuN78CyrU47jzYWzBAzp3rLk+lx3yRDYe+Lquc+elGnkmorLQG8BW7bVDSgtZnZybxJ5fcqYHTBc/s0W4HdZmgCsQRqQAgh4muGu7dps3fcBZk16lJqvrYHwgVHOTsvxgxC6DWFCYO961lLdQOS/Ler8OP3FoLdTrajGQcqtTvH8udnoeUDhmEhl9gS4Ha54OSSUcZqLmVllTRIjXiKzrYTIiDcSIO5Es9SYZ9RZcM/0PjXf7R66bEYQ838guBo4GXxTDtx2J+6yDIr7hO3aJMXOTNiKYRxxqpUDaI8OMTtaoK0IaejkRhE1z0565vi93eLDKFnPNveXfi6zi5vXqJD71x3mpOP/qW+6oNRfS1d5fTXCPOj6gK0ilCgzDADS/LdcXDGh2vuSF0oJKJyZW+814sq3BR6rQ0iBeX2aDlOK9hEv8W1qo7IUndgT1P2h4jRG/KqP0lPjtAznWLsMenlxcA+PfILKwVWaR9YVls7hsF8Xdc51pLUbcsSLUoHC1aWUYaWthW4DyFixUzsH4jXjq+QI0Xl6kA3qDmeXYoQtud4d6S8NNrnAgMud6Ku+cWebPuOlk4im4+WBtoK/PLQ2iTOo6csWXmh18CgFzOgGzigJsnWOU+Iy8s6XtHuYVKKwwGjcmS8OYmGNMRtFIeq5mOMft4LSVoo7/BjKJ9qNN4zcRAapQVIu/I4RH3QZgMmvTTLeo8qn73v1ueyK7oibhzjmvkqlcGXPAozJs+v2ZYDKf4V9SMVItX7ftqbCb7vmKefcyrOIsnjRLUsd38pMJdHnPVr48uRbDT9z6ivOshGsEzJTe/mfX515j/NY2u5mUAIMC8ZbxXeosTnDX1kIxqAt53i0D0VQrhYtge3SK34EHt2X03nq9QB7N373JmTi8d3VXydmdwSqFzXJs2uiVlsn5XHOHbD46D7mDdctl/O/MOnVwR2D//GEivdl7FmIpM3Af8Zrg3e95UFtQpiIxkDKixuLZERLuY1CLDYfzGlcNF9WS5oLELYZEmhdW8EI2acKVViiTMd9o6GKt2zsFF4MKLc/F+mrUftEMirPrQDwgsM1PAgOWDb8YS/TQDyE6EfQioEA=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="dc1597782dcf052c660cbbe1b01cf00a"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=6110da747e462290')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">6110da747e462290</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 210.75.253.169</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
