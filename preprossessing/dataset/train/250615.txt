<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.boomerater.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/senior-living/senior-housing-in-Chesnee_South-Carolina?__cf_chl_captcha_tk__=e80ed70c60e52502a0b821fb5157234785aea26f-1610567122-0-Aaez-QdiinwAbGKUhKhwpYeK7YmbqkdqYnO9SJpIPPvVq2AJUp0XA7V_Aj5f9d8hbo6bK6Y-D18Sr2cWqxQdS36t8zMsN-DU-YzbGOzumQoNM-09Im95bYKot2WDO_z1e1QwwriQLTKIO-rCkTWVJDTDvKuiC5Zj-_Qqd9hmStqxSS2iNeQYS_h6uUlS6_yzgKJmGjyztQByPL1JzpBRRxptYmxOjMpnjklLD4nP59_W2IDAV188jBu5FZYj1keVb78FBE2is44KgMuBDcTIrEIRO6K7BDEWvf4CLIHeJcSLTb1tJVgD349Cuj-dyl2JGyU-kIQY6T6WzwIJlvcSZxWCZr9aXANnpbxYGP59mFCLuDQVA4HU7HkP3pP-qVAIs2hn877OMn_qcq4RAje_m47EB3bS7LcE5USJ3yPu-0s_jkVKTsIFz_tSbT3wYgF_6wV1qmQ0PdsToprV9reBIY7HLZIO80mtEPy40ejT9oL77gI47biZY7QAR2L9flBzCX8xk6fItcv8-1sqhcK7oJltlXuISPAytdjdmZfwaW2-g1NayWUE2tjpl-slaoNy9ovWFMpzj-L1NU45LgL0B6w" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="68aee3cf67e80ac52fa5cfa4ea017e3410a667d8-1610567122-0-AX6A4adxcn3FiyTSp0bC9bSCOk5o/7Rp+p6wEE4teDYJC8x6UDRBOHueDbFvk5e3+/R3RHV49k01AYT03N8ARSYOel1sWYSTQdHZwyqobjSxpv+WfvoTav/OG1I5VP8WyNGVfOvmAOJ3kED12XTiVJCf9E6SgqpTrpJL6T6vlKxZHRaLcimWL9Js97XgXouIqFbRZBY4kMDvQuauF6OhGvotdtvKvoorhvmtDOUIp7XhnCsSHOlpWVJFNO/MQCN8xhWQ9PQmMFZ0CurlhaDo3dFNL4CecPw0vwhYwDiib6mXTSMFoKG/rgSHLsobcH99l5fM9qAbfbocV91MkwPTxNfEUeD5FLu2kgSnU+7kc0Y8K1RD7f33ZTp0Bz12vPqzlofn2V7TfyXb4RVaXa61PXTG6AlA+DpmTkMNPTF16G7DzSHqh75vMkf1duTK3enZLvMoXchn29aSwejCyaDAunMlt77sz8Y+A5vLsRnNhPiZsIO/sfrolhr+CCfcLn7pgrluL3DduWI8R3Mt52vuSnOIqmGeU25T5xzMl0/CBViZD3kV6Kj24cs9yRlJC81EdXrP7/kGbap6HixznhIfArJAggFhRwNotaE92xRw0pBcJ9BzPvbV+XUJTxDeY8Ywnq2vKSglAkchJTMkyrlowJI7pC+P7kZAQVdp7QbmZEGuVL2vS/rDjYCMALzIr3oYVS2/iq+OwmftKBbkG/R3wJPzcXUQoShrCb3p+l9KSnFTwwOsaQPZ/yxgQ0EVREf6/l/btlmkmNlHQtNiJWMeLDpY1hnLMpa9HW5lA+qzj+Ek7SKYQNKGWDGARvkdGYk7Fq0pKpcZf9B5wS4+cTYtYJD6p3M+SFme72BuM3PO0JAsL3ZHwaRlTXSz1Cr9YCNbNSWDuGIPHT2wD41DSzB4vmrLocSk+oCZap3Gtqif7WICt3fqPSNQIqSZOYfZSQMqRRp0ZevCm8wHCJZEV9WbyIB3GNeG7y3uxb7z2jM+n2qEVgXJ5JdawTCV26oXwYG3vA2B0P8M1ovcm21YYuivuiziRcwAR3MXoR9MzSGN4QOX68MiPeLXgfi7ZPbPHZ/kj5NfyvejtHUI237BhFxFeKolxMHawYIEPPlQ5R9X88aes5S7H2dCgMou9KQsl+/4Sd3dK8TkGOwNyammpkFTyxIc9Iz9ChMIrOcayWnILZZ1rgLbd0t/+joMNm1LUkfjT9/4gsErf6pynrilL6mfQ+wt8w/dPjuey/450LjR5O3SaR96YGlb8zqFrb0ojm15cUlCXwz0sIS3Ty+KUhI3U2wT9Oy5EfIi2dzdQscFZovteZ8kjxaohxePjnxD9lp0Yg=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value=""/>
<script async="" data-ray="61119e048d23dcc6" data-sitekey="f9630567-8bfa-4fc9-8ee5-9c91c6276dff" data-type="normal" src="/cdn-cgi/scripts/hcaptcha.challenge.js" type="text/javascript"></script>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=61119e048d23dcc6')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<a href="https://funny-video-clip.info/eldest.php?v=334" style="display: none;">table</a>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">61119e048d23dcc6</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:8d2b:d30c:bf6f:658f</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
