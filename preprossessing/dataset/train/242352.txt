<!DOCTYPE html>
<html>
<head>
<title>Bluelight Sports</title>
<link href="assets/bls.json" rel="manifest"/>
<script src="https://cdn.rawgit.com/GoogleChrome/pwacompat/v1.0.3/pwacompat.min.js"></script>
<link href="assets/images/bls.png" rel="icon" type="image/png"/>
<link href="assets/images/apple-icon-32x32.png" rel="apple-touch-icon" sizes="32x32"/>
<link href="assets/images/apple-icon-48x48.png" rel="apple-touch-icon" sizes="48x48"/>
<link href="assets/images/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="assets/images/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="assets/images/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="assets/images/apple-icon-128x128.png" rel="apple-touch-icon" sizes="128x128"/>
<link href="assets/images/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="assets/images/apple-icon-256x256.png" rel="apple-touch-icon" sizes="256x256"/>
<!-- Latest compiled and minified CSS -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Optional theme -->
<link crossorigin="anonymous" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" rel="stylesheet"/>
<!-- Jquery CSS -->
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
<!-- Fontawesome -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<link href="assets/main.css" rel="stylesheet" type="text/css"/>
<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
<meta content="#007efd" name="theme-color"/>
<meta charset="utf-8"/>
<meta content="yes" name="mobile-web-app-capable"/>
</head>
<body>
<div class="bls-container">
<div class="bls-container" id="content"></div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="assets/home.js"></script>
<script src="assets/main.js"></script>
</body>
</html>