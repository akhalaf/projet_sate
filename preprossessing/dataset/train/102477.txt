<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Allstays | Campgrounds | RV Parks</title>
<meta content="Contact us today for full details and contact info in the Allstays.com complete guide to campgrounds, RV parks and parking lots for boondocking, hotels, motels, and much, much more." name="description"/>
<meta content="AllStays LLC" name="author"/>
<!-- Mobile Specific Metas
  ================================================== -->
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!-- CSS
  ================================================== -->
<!-- Loading Bootstrap -->
<link href="//www.allstays.com/css/vendor/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<!-- Loading Flat UI -->
<link href="//www.allstays.com/css/flat-ui.css" rel="stylesheet" type="text/css"/>
<!-- Loading Custom UI -->
<link href="//www.allstays.com/css/main.css" rel="stylesheet" type="text/css"/>
<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
<!-- Favicons
	================================================== -->
<link href="//www.allstays.com/images/favicon.ico" rel="shortcut icon"/>
<link href="//www.allstays.com/images/apple-touch-icon.png" rel="apple-touch-icon"/>
<link href="//www.allstays.com/images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="//www.allstays.com/images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
</button>
<a class="navbar-brand" href="//www.allstays.com">ALLSTAYS</a>
</div>
<div class="navbar-collapse collapse">
<ul class="nav navbar-nav">
<li><a href="//www.allstays.com/Campgrounds/">Camping</a></li>
<li><a href="//www.allstays.com/us-hotels.htm">Rooms</a></li>
<li><a href="//www.allstays.com/c/road-guides.htm">Drivers</a></li>
<li><a href="//www.allstays.com/apps/">Apps</a></li>
<li><a href="//www.allstays.com/Features/">Tips</a></li>
<li><a href="//www.allstays.com/proamember/member">Pro Sign In</a></li>
<li><a href="//www.allstays.com/DL/">Sign Up</a></li>
</ul><form action="//google.com/cse" class="navbar-form navbar-right" id="searchbox_008064745474647833724:_x7ksj0tpra" role="search">
<div class="form-group">
<div class="input-group">
<input name="cx" type="hidden" value="008064745474647833724:_x7ksj0tpra"/>
<input name="cof" type="hidden" value="FORID:1"/>
<input name="ie" type="hidden" value="UTF-8"/>
<input class="form-control" id="navbarInput-01" name="q" placeholder="Search" type="text" value="search"/>
<span class="input-group-btn">
<button class="btn" type="submit"><span class="fui-search"></span></button>
</span>
</div>
</div>
</form>
<!--<li class="dropdown">
              <a href="//www.allstays.com/#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="//www.allstays.com/#">Action</a></li>
                <li><a href="//www.allstays.com/#">Another action</a></li>
                <li><a href="//www.allstays.com/#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="//www.allstays.com/#">Separated link</a></li>
                <li><a href="//www.allstays.com/#">One more separated link</a></li>
              </ul>
            </li>-->
<!--original right<ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="//www.allstays.com/Members/">Pro Sign In</a></li>
            <li><a href="//www.allstays.com/DL/">Sign Up</a></li>-->
</div><!--/.nav-collapse -->
</div>
</div>
<div class="jumbotron top">
<div class="eventPhotos">
<picture class="eventImage" data-credit-location="//www.allstays.com" data-credit-name="AllStays LLC" data-credit-title="Campgrounds">
<!--[if IE 9]><video style="display: none;"><![endif]-->
<source media="(min-width: 600px)" srcset="images/main-hidden-lake-large_1188x360.jpg 1x, images/main-hidden-lake-large_2x_2376x720.jpg 2x">
<source srcset="images/main-hidden-lake-small_600x300.jpg 1x, images/main-hidden-lake-small_2x_1188x360.jpg 2x">
<!--[if IE 9]></video><![endif]-->
<img alt="Campgrounds" src="images/main-hidden-lake-large_1188x360.jpg"/>
</source></source></picture>
<h2>Enjoy The Journey</h2>
</div>
</div>
<div class="container about">
<div class="row">
<h4 class="text-center">COVID-19: Most places are open but call ahead</h4>
<h2 class="text-center">Free Guides Online...</h2>
<div class="col-md-4">
<p class="text-center"><a href="//www.allstays.com/Campgrounds/"><img border="0" class="jcamp" height="1" src="images/img_trans.gif" width="1"/><br/><u>Camping</u></a></p>
<p><a href="Campgrounds/Arizona-campgrounds.htm">Arizona Campgrounds</a><br/>
<a href="Campgrounds/California-campgrounds.htm">California Campgrounds</a><br/>
<a href="Campgrounds/Colorado-campgrounds.htm">Colorado Campgrounds</a><br/>
<a href="Campgrounds/Florida-campgrounds.htm">Florida Campgrounds</a><br/>
<a href="Campgrounds/Texas-campgrounds.htm">Texas Campgrounds</a><br/>
<a href="Campgrounds/">more states campgrounds</a><br/>
<br/>
			  Over 37,000 Campgrounds: Independent, KOA, National/State Forest, State Parks, Public Lands, Army Corps, National Park, Military, County and City Parks, casinos and more. No one else has more. <br/>
<br/>

Amenities noted include open season, rate range, sites, hookups, amps, big rigs access, tents or no tents, club discounts (Good Sam, Passport America, Escapees, KOA, 1,000 Trails, Resorts of Distinction, AOR, Coast to Coast), water, toilets/restrooms, tables, pool, playground, laundry, propane, pet-friendly, RV dump station, age restricted, big rigs, boat launch, trails, internet access and more.<br/><br/></p>
</div>
<div class="col-md-4">
<p class="text-center"><a href="//www.allstays.com/us-hotels.htm"><img border="0" class="jroom" height="1" src="images/img_trans.gif" width="1"/><br/><u>Rooms</u></a></p>
<p><a href="/us-arizona-hotels/">Arizona Hotels </a><br/>
<a href="/us-california-hotels/">California Hotels </a><br/>
<a href="/us-florida-hotels/">Florida Hotels </a><br/>
<a href="/us-new-york-hotels/">New York Hotels</a> <br/>
<a href="/us-texas-hotels/">Texas Hotels </a><br/>
<a href="/us-hotels.htm">more states hotels</a><br/>
<br/>
Do you have favorite hotel chains or do you like independents? Most of us have our preferences. This is a independent lodging guide that is like no other. Easily find favorite chains with map filters or a manual lookup by chain. No one else lets you find Ritz-Carltons or Motel 6 together. The choice is yours.</p>
<p>We provide REAL hotel phone numbers and REAL website links directly to each property. You are not throttled by a third party. You get the number to the front desk of the hotel and not a customer service queue. No ads or corporate deals to throw certain brands at you and hide others.</p>
</div>
<div class="col-md-4">
<p class="text-center"><a href="//www.allstays.com/c/road-guides.htm"><img border="0" class="jtruck" height="1" src="images/img_trans.gif" width="1"/><br/><u>Drivers</u></a></p>
<p><a href="c/truck-stop-locations.htm">Truck Stops</a><br/>
<a href="c/rest-stop-locations.htm">Rest Areas</a><br/>
<a href="c/wal-mart-locations.htm">Walmart stores</a><br/>
<a href="hotel-maps/truck-hotels.htm">Motels with truck parking</a><br/>
<a href="c/truck-wash-locations.htm">Truck wash</a><br/>
<a href="c/road-guides.htm">more road guides</a><br/>
<br/>
              Independent Truck Stops, Flying Js, Pilot Travel Centers, Loves Travel Stops, Petro Centers, TA Travel Centers, Petro Canada. Includes info on fuel lanes, dump stations, propane, restaurants, internet, laundry, showers, tire care, travel stores, ATMs, Western Union, UPS, FedEx and more. Are they RV friendly.</p>
<p>Walmarts. What are the store amenities? Thousands of RV parking reports from users help you find a possible parking spot.</p>
<p>Rest Areas, Turnouts, Welcome Centers, Scenic Vistas: These are shown by direction and amenities such as wifi, RV dump, handicap, pet friendly, tables, security, vending and restrooms.</p>
</div>
</div>
</div>
<div class="container about">
<div class="row">
<h2 class="text-center">... On The Go</h2>
<div class="col-md-3">
<p class="text-center"><a href="//www.allstays.com/DL/">  <img border="0" class="aspin" height="1" src="images/img_trans.gif" width="1"/><br/><u>AllStays Pro  (PC/Mac)</u></a></p>
<p>Pure information, faster, no ads, privacy, full screen maps, offline use with Google Earth, more filters, layers for  traffic, biking, transit, over half a million search combinations.<br/>
<br/>
<br/>
<a href="//www.allstays.com/DL/"><img border="0" height="105" src="apps/ASProBadge210105.png" width="210"/></a><br/>
</p><hr/>
</div>
<div class="col-md-3">
<p class="text-center"><a href="//www.allstays.com/apps/camprv.htm"><img border="0" class="appcrv" height="1" src="images/img_trans.gif" width="1"/><br/><u>Camp &amp; RV (App)</u></a></p>
<p>The #1 camping app for iPhone and iPads. From resorts to hike-in spots. Amenities, truck stops, rest areas, Wal-mart and casino parking, RV services, sporting goods stores and much more.</p>
<p class="text-center"><a href="//www.allstays.com/go/apps.php?goto=Apple/camprv"><img border="0" src="apps/AppStoreBadge210105.png"/></a></p><hr/>
</div>
<div class="col-md-3">
<p class="text-center"><a href="//www.allstays.com/apps/truckstops.htm"><img border="0" class="apptat" height="1" src="images/img_trans.gif" width="1"/><br/><u>Truck &amp; Travel (App)</u></a></p>
<p>The #1 paid truck app for iPhone and iPads. If you drive a big rig, you need this app. If you just drive on road trips in a car and prefer making your stops count, you'll love this app.<br/>
 </p>
<p class="text-center"><a href="//www.allstays.com/go/apps.php?goto=Apple/truckstops"><img border="0" src="apps/AppStoreBadge210105.png"/></a></p><hr/>
</div>
<div class="col-md-3">
<p class="text-center"><a href="//www.allstays.com/apps/allstaysbychain.htm"><img border="0" class="apphbc" height="1" src="images/img_trans.gif" width="1"/><br/><u>AllStays Hotels By Chain (App)</u></a></p>
<p>Find all kinds of beds near you. All the chains. The only iOS app that puts you one button from the front desk, NOT a phone bank on the other side of the world.</p>
<p class="text-center"><a href="//www.allstays.com/go/apps.php?goto=Apple/allstayshotelsbychain"><img border="0" src="apps/AppStoreBadge210105.png"/></a></p><hr/>
</div>
</div>
</div>
<div class="container about">
<div class="row">
<h2 class="text-center">As Seen In...</h2>
<div class="col-md-6">
<p class="text-center">The New York Times<br/>
Philadelphia Inquirer<br/>
Kiplinger Finance<br/>
Forbes<br/>
Starbucks<br/>
Wired Magazine<br/>
USA Today<br/>
Boston Globe<br/>
Tamp Bay Times<br/>
The Street<br/>
ESPN Game Day<br/></p>
</div>
<div class="col-md-6">
<p class="text-center">Featured in Apple iTunes<br/>
Harvard Business Review<br/>
Womans Day<br/>
Miami Herald<br/>
Washington Times<br/>
Padgadget<br/>
Appadvice<br/>
Techlicious<br/>
Appolicious<br/>
</p>
</div>
</div>
</div>
<div class="jumbotron foot">
<div class="row">
<div class="col-md-3">
<p>
<b><a href="//www.allstays.com/#">More about AllStays</a></b><br/>
<a href="//www.allstays.com">Home</a><br/>
<a href="//www.allstays.com/Services/affiliate.htm" rel="nofollow">Affiliate</a><br/>
<a href="//www.allstays.com/Services/contactus.htm" rel="nofollow">Contact Us</a><br/>
<a href="//www.allstays.com/Services/legend.htm" rel="nofollow">Support</a><br/>
<a href="//www.allstays.com/sitemap.htm">Site Map</a><br/>
<a href="//www.allstays.com/Services/privacypolicy.htm" rel="nofollow">Privacy Policy</a><br/>
<a href="//www.allstays.com/Services/termsservice.htm" rel="nofollow">Terms</a><br/>
<a href="//www.allstays.com/Services/jobs.htm" rel="nofollow">Jobs</a><br/>
</p>
</div>
<div class="col-md-3">
<p>
<b><a href="//www.allstays.com/Services/memberservices.htm" rel="nofollow">Connect</a></b><br/>
<a href="//www.allstays.com/https://www.facebook.com/allstays" rel="nofollow">Facebook</a><br/>
<a href="//www.allstays.com/http://twitter.com/allstays" rel="nofollow">Twitter</a><br/>
<a href="//www.allstays.com/Services/advertising.htm" rel="nofollow">Advertising</a><br/>
<a href="//www.allstays.com/Services/add-place.htm" rel="nofollow">Add A Campground</a><br/>
<a href="//www.allstays.com/Services/change-hotel.php" rel="nofollow">Add A Room</a><br/>
<a href="//www.allstays.com/Services/altstf.htm" rel="nofollow">Add A Truck Stop</a><br/>
<a href="//www.allstays.com/Services/memberservices.htm" rel="nofollow">Add Other Places</a><br/>
</p>
</div>
<div class="col-md-3">
<p>
<b><a href="//www.allstays.com/#">AllStays Classic Guides</a></b><br/>
<a href="//www.allstays.com/hotels-by-chain/" title="Hotels By Chain">Hotels By Chain</a><br/>
<a href="//www.allstays.com/Special/luxury.htm" title="Luxury Hotels">Luxury Hotels</a><br/>
<a href="//www.allstays.com/Special/spa.htm" title="Spa Resorts Guide">Spa Resorts</a><br/>
<a href="//www.allstays.com/green-hotels/green-hotels.htm" title="Green Lodging">Eco &amp; Green</a> <br/>
<a href="//www.allstays.com/BandB/bandb.htm" title="Bed &amp; Breakfasts">B &amp; Bs</a><br/>
<a href="//www.allstays.com/Special/haunted.htm" title="Haunted Hotels">Haunted Hotels</a><br/>
</p>
</div>
<div class="col-md-3">
<p>
<br/>
<a href="//www.allstays.com/Extended-Stays/extendedstays.htm" title="Extended Stays Hotels">Extended Stays</a><br/>
<a href="//www.allstays.com/real-suites/" title="Real Suites">Real Suites</a><br/>
<a href="//www.allstays.com/Special/petfriendly.htm" title="Pet Friendly Hotels">Pet Friendly</a><br/>
<a href="//www.allstays.com/yoga/yoga-retreats.htm" title="Yoga Retreats">Yoga Retreats</a><br/>
<a href="//www.allstays.com/Special/skiing.htm" title="Ski Resorts">Ski Resorts</a><br/>
<a href="//www.allstays.com/c/road-guides.htm" title="Road Guides Store Locators">Road Guides</a><br/>
</p>
</div>
</div>
</div>
<div class="jumbotron footer">
<div class="container">
<p>
<span class="text-left top">Copyright © 2000-2021 AllStays LLC.</span>
<a class="icon" href="//twitter.com/allstays"><img height="40" src="img/soc-twitter.png" width="48"/></a>
<a class="icon" href="//www.facebook.com/allstays"><img height="40" src="img/soc-fb.png" width="21"/></a>
</p>
</div>
</div><!-- /container -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/vendor/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/flat-ui.min.js"></script>
<script src="js/application.js"></script>
<script type="text/javascript">
var clicky_site_ids = clicky_site_ids || [];
clicky_site_ids.push(101112378);
(function() {
  var s = document.createElement('script');
  s.type = 'text/javascript';
  s.async = true;
  s.src = '//static.getclicky.com/js';
  ( document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0] ).appendChild( s );
})();
</script>
<noscript><p><img alt="Clicky" height="1" src="//in.getclicky.com/101112378ns.gif" width="1"/></p></noscript>
</body>
</html>