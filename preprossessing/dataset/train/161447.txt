<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<meta content="en-ca" http-equiv="Content-Language"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Asphaltplanet.ca &gt; QuÃ©bec &gt; Autoroute 15</title>
<style type="text/css">
.style3 {
	text-align: left;
}
.style7 {
	margin-top: 0;
	margin-bottom: 0;
}
.style1 {
	font-family: "Lucida Sans";
	font-size: 9pt;
}
.style25 {
	font-family: "Lucida Sans";
}
.style26 {
	font-family: "Lucida Sans";
	color: #000099;
}
#table2 {
	border-width: 1px;
}
.style28 {
	border: 1px solid #CCCCCC;
	text-align:center
}
.style32 {
	border-style: solid;
	border-width: 1px;
}
.style5 {
	font-size: 16pt;
}
.style4 {
	font-weight: bold;
}
</style>
</head>
<body alink="#333333" bgcolor="#CCCCCC" link="#000099" style="background-color: #CCCCCC" vlink="#000066">
<p align="right" style="margin-bottom: 3px; text-align: right; font-size: xx-small; font-style: italic; font-family: Tahoma;">
<a href="../../../index.html">AsphaltPlanet.ca</a> &gt; 
<a href="../../index.html">QuÃ©bec</a> &gt; Autoroute 15  </p>
<p align="center" class="style7"><font face="Lucida Sans" style="font-size: 9pt">
<img height="381" src="A15_index_front.jpg" width="1022"/></font></p>
<table align="center" class="style28" style="width: 1024px; text-align: center">
<tr>
<td style="height: 184px">
<p class="style3"><font face="Lucida Sans" style="font-size: 9pt"><b>
		Southern Terminus:</b> 
        The Canada/USA border where the highway continues into New York as 
		Interstate 87.</font></p>
<p class="style3"><font face="Lucida Sans" style="font-size: 9pt"><b>
		Northern Terminus:</b> R-117, 89kms north of the Metropolitane Autoroute 
		in Sainte-Agathe-des-Monts.</font></p>
<p class="style3"><font face="Lucida Sans" style="font-size: 9pt"><b>
		Regions:</b> 
        MRC Des Jardins, MRC de Roussillon, Agglomeration de Longueuil, 
		Agglomeration de Montreal, MRC de Laval, MRC de Therese-de-Blainville, 
		MRC de Mirabel, MRC De La Riviere Du Nord, MRC Des Pays-D'en Haut, MRC 
		Des Laurentides.</font></p>
<p class="style3">
<font face="Lucida Sans" style="font-size: 9pt"><b>Routing:</b> 
		A-15 begins its trek running north from the New York border, heading 
		almost due north into Montreal.  At Montreal, the highway swings 
		westerly, by-passing Downtown and crossing the Champlain Bridge.  
		In Northern Montreal, the freeway joins with A-40 for a short distance 
		before continuing Northwesterly into the Suburbs of Montreal eventually 
		leading into the Laurentides.</font></p>
</td>
</tr>
</table>
<p> </p>
<table align="center" cellpadding="3" cellspacing="1" class="style32" id="table6" style="width: 1024px">
<tr>
<td style="width: 90%" width="49%">
<p align="center"><span style="font-weight: 700">
<font face="Lucida Sans" style="font-size: 16pt">Virtual Tour of </font></span>
<font face="Lucida Sans" style="font-size: 9pt"><span class="style5">
<span class="style4">A-15:</span></span></font></p>
<p align="center"><font face="Lucida Sans" style="font-size: 9pt">Pages show both 
        eastbound and westbound photos, arranged in order from south to north along the entire 
        length of the highway.  Pages include both center-line and sign 
        photos.   </font></p>
<p align="center">
<font face="Lucida Sans" style="font-size: 9pt">
<a href="Page1.html">Page 1 - USA. to MontrÃ©al</a></font></p>
<p align="center">
<font face="Lucida Sans" style="font-size: 9pt">
<a href="Page2.html">Page 2 - MontrÃ©al</a></font></p>
<p align="center">
<font face="Lucida Sans" style="font-size: 9pt">
<a href="Page3.html">Page 3 - MontrÃ©al to Saint JÃ©rÃ´me</a></font></p>
<p align="center">
<font face="Lucida Sans" style="font-size: 9pt">
<a href="Page4.html">Page 4 - Saint JÃ©rÃ´me to Sainte-Agathe-des-Monts</a></font></p>
</td>
</tr>
</table>
<p> </p>
<table align="center" style="width: 1024px">
<tr>
<td>
<p class="style3">
<font face="Lucida Sans" style="font-size: 9pt; font-weight: 700">
		Links:</font></p>
<ul>
<li class="style3"><font face="Lucida Sans" style="font-size: 9pt"><i>
<a href="http://www.montrealroads.com/roads/laurentian/" target="_blank">The 
	Laurentian Autoroute @ MontrealRoads.com</a></i> -- </font>
<span class="style1">Steve Anderson</span></li>
<li class="style3"><font face="Lucida Sans" style="font-size: 9pt"><i>
<a href="http://www.montrealroads.com/roads/decarie/" target="_blank">Autoroute 
	Decarie @ MontrealRoads.com</a></i> -- </font><span class="style1">Steve Anderson</span></li>
<li class="style3"><font face="Lucida Sans" style="font-size: 9pt"><i>
<a href="http://www.alpsroads.net/roads/qc/a-15/" target="_blank">A-15 @ 
	AlpsRoads.net</a></i> -- Steve Alpert</font></li>
<li class="style3"><font face="Lucida Sans" style="font-size: 9pt"><i>
<a href="http://webfil_92.tripod.com/autoroutes_en/id6.html" target="_blank">A-15 @ Quebec Freeways</a></i> -- 
	</font><span class="style1"><font face="Arial" size="2">FÃ©lix Mathieu-BÃ©gin</font></span></li>
</ul>
</td>
</tr>
</table>
<p align="right" style="font-size: xx-small; font-style: italic; font-family: Tahoma; margin-bottom: 3px">
	 </p>
<hr/>
<p align="center"><a href="../13/"><span class="style25">&lt;- A</span><span class="style26">-13</span></a><span class="style25">         
	<a href="../../index.html">PQ Highways</a>         
	<a href="../19/index.html">A-19 -&gt;</a></span><br/>
<span class="style25"><a href="../../../index.html">AsphaltPlanet.ca</a></span></p>
<hr/>
<p align="left" style="font-size: 1pt; text-align: center">
<font face="Tahoma" size="1">Â© 2006-2014 Scott Steeves. 
	<a href="mailto:AsphaltPlanet@asphaltplanet.ca">AsphaltPlanet@asphaltplanet.ca</a>  
	Special thanks to Carl Tessier for assistance with French translations.<br/>
<i><br/>
    Page Created: December 13, 2008.<br/>
    Last Updated: June 28, 2014.</i></font></p>
</body>
</html>
