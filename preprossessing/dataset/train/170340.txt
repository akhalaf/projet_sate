<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="pt-PT">
<![endif]--><!--[if IE 8]>
<html class="ie ie8" lang="pt-PT">
<![endif]--><!--[if !(IE 7) & !(IE 8)]><!--><html lang="pt-PT">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://augustogemelli.com/wordpress/xmlrpc.php" rel="pingback"/>
<title>Página não encontrada – Chef Augusto Gemelli</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://augustogemelli.com/wordpress/pt/feed/" rel="alternate" title="Chef Augusto Gemelli » Feed" type="application/rss+xml"/>
<link href="https://augustogemelli.com/wordpress/pt/comments/feed/" rel="alternate" title="Chef Augusto Gemelli » Feed de comentários" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/augustogemelli.com\/wordpress\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.16"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://augustogemelli.com/wordpress/wp-content/themes/spacious/style.css?ver=4.9.16" id="spacious_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://augustogemelli.com/wordpress/wp-content/themes/spacious/genericons/genericons.css?ver=3.3.1" id="spacious-genericons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Lato&amp;ver=4.9.16" id="google_fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://augustogemelli.com/wordpress/wp-content/plugins/newsletter/subscription/style.css?ver=5.1.6" id="newsletter-subscription-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://augustogemelli.com/wordpress/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://augustogemelli.com/wordpress/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://augustogemelli.com/wordpress/wp-content/themes/spacious/js/spacious-custom.js?ver=4.9.16" type="text/javascript"></script>
<!--[if lte IE 8]>
<script type='text/javascript' src='https://augustogemelli.com/wordpress/wp-content/themes/spacious/js/html5shiv.min.js?ver=4.9.16'></script>
<![endif]-->
<link href="https://augustogemelli.com/wordpress/wp-json/" rel="https://api.w.org/"/>
<link href="https://augustogemelli.com/wordpress/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://augustogemelli.com/wordpress/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.16" name="generator"/>
<style type="text/css">
			#site-title a {
			color: #dd3333;
		}
		#site-description {
			color: #dd3333;
		}
		</style>
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #000000; }
</style>
<style type="text/css"> blockquote { border-left: 3px solid #11b70e; }
			.spacious-button, input[type="reset"], input[type="button"], input[type="submit"], button { background-color: #11b70e; }
			.previous a:hover, .next a:hover { 	color: #11b70e; }
			a { color: #11b70e; }
			#site-title a:hover { color: #11b70e; }
			.main-navigation ul li.current_page_item a, .main-navigation ul li:hover > a { color: #11b70e; }
			.main-navigation ul li ul { border-top: 1px solid #11b70e; }
			.main-navigation ul li ul li a:hover, .main-navigation ul li ul li:hover > a, .main-navigation ul li.current-menu-item ul li a:hover { color: #11b70e; }
			.site-header .menu-toggle:hover.entry-meta a.read-more:hover,#featured-slider .slider-read-more-button:hover,.call-to-action-button:hover,.entry-meta .read-more-link:hover,.spacious-button:hover, input[type="reset"]:hover, input[type="button"]:hover, input[type="submit"]:hover, button:hover { background: #008500; }
			.main-small-navigation li:hover { background: #11b70e; }
			.main-small-navigation ul > .current_page_item, .main-small-navigation ul > .current-menu-item { background: #11b70e; }
			.main-navigation a:hover, .main-navigation ul li.current-menu-item a, .main-navigation ul li.current_page_ancestor a, .main-navigation ul li.current-menu-ancestor a, .main-navigation ul li.current_page_item a, .main-navigation ul li:hover > a  { color: #11b70e; }
			.small-menu a:hover, .small-menu ul li.current-menu-item a, .small-menu ul li.current_page_ancestor a, .small-menu ul li.current-menu-ancestor a, .small-menu ul li.current_page_item a, .small-menu ul li:hover > a { color: #11b70e; }
			#featured-slider .slider-read-more-button { background-color: #11b70e; }
			#controllers a:hover, #controllers a.active { background-color: #11b70e; color: #11b70e; }
			.widget_service_block a.more-link:hover, .widget_featured_single_post a.read-more:hover,#secondary a:hover,logged-in-as:hover  a,.single-page p a:hover{ color: #008500; }
			.breadcrumb a:hover { color: #11b70e; }
			.tg-one-half .widget-title a:hover, .tg-one-third .widget-title a:hover, .tg-one-fourth .widget-title a:hover { color: #11b70e; }
			.pagination span ,.site-header .menu-toggle:hover{ background-color: #11b70e; }
			.pagination a span:hover { color: #11b70e; border-color: .#11b70e; }
			.widget_testimonial .testimonial-post { border-color: #11b70e #EAEAEA #EAEAEA #EAEAEA; }
			.call-to-action-content-wrapper { border-color: #EAEAEA #EAEAEA #EAEAEA #11b70e; }
			.call-to-action-button { background-color: #11b70e; }
			#content .comments-area a.comment-permalink:hover { color: #11b70e; }
			.comments-area .comment-author-link a:hover { color: #11b70e; }
			.comments-area .comment-author-link span { background-color: #11b70e; }
			.comment .comment-reply-link:hover { color: #11b70e; }
			.nav-previous a:hover, .nav-next a:hover { color: #11b70e; }
			#wp-calendar #today { color: #11b70e; }
			.widget-title span { border-bottom: 2px solid #11b70e; }
			.footer-widgets-area a:hover { color: #11b70e !important; }
			.footer-socket-wrapper .copyright a:hover { color: #11b70e; }
			a#back-top:before { background-color: #11b70e; }
			.read-more, .more-link { color: #11b70e; }
			.post .entry-title a:hover, .page .entry-title a:hover { color: #11b70e; }
			.post .entry-meta .read-more-link { background-color: #11b70e; }
			.post .entry-meta a:hover, .type-page .entry-meta a:hover { color: #11b70e; }
			.single #content .tags a:hover { color: #11b70e; }
			.widget_testimonial .testimonial-icon:before { color: #11b70e; }
			a#scroll-up { background-color: #11b70e; }
			.search-form span { background-color: #11b70e; }</style>
</head>
<body class="error404 custom-background no-sidebar ">
<div class="hfeed site" id="page">
<header class="site-header clearfix" id="masthead">
<div id="header-text-nav-container">
<div class="inner-wrap">
<div class="clearfix" id="header-text-nav-wrap">
<div id="header-left-section">
<div class="" id="header-text">
<h3 id="site-title">
<a href="https://augustogemelli.com/wordpress/principal/" rel="home" title="Chef Augusto Gemelli">Chef Augusto Gemelli</a>
</h3>
<p id="site-description">Cozinha de Autor</p>
<!-- #site-description -->
</div><!-- #header-text -->
</div><!-- #header-left-section -->
<div id="header-right-section">
<nav class="main-navigation" id="site-navigation" role="navigation">
<h3 class="menu-toggle">Menu</h3>
<div class="menu-principal-container"><ul class="menu" id="menu-principal"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-110" id="menu-item-110"><a href="https://augustogemelli.com/wordpress/o-chefe/">O Chefe</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-100" id="menu-item-100"><a href="https://augustogemelli.com/wordpress/aulas/">Aulas</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-167" id="menu-item-167"><a href="https://augustogemelli.com/wordpress/newsletter/">Newsletter</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-166" id="menu-item-166"><a href="https://augustogemelli.com/wordpress/newsletter/">Newsletter</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-121" id="menu-item-121"><a href="https://augustogemelli.com/wordpress/catering/">Catering</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-134" id="menu-item-134"><a href="https://augustogemelli.com/wordpress/consulting/">Consulting</a></li>
</ul></div> </nav>
</div><!-- #header-right-section -->
</div><!-- #header-text-nav-wrap -->
</div><!-- .inner-wrap -->
</div><!-- #header-text-nav-container -->
<div class="wp-custom-header" id="wp-custom-header"><img alt="Chef Augusto Gemelli" class="header-image" height="400" src="https://augustogemelli.com/wordpress/wp-content/uploads/2015/01/headGemelliDoubleG.jpg.png" width="1399"/></div>
<div class="header-post-title-container clearfix">
<div class="inner-wrap">
<div class="post-title-wrapper">
<h1 class="header-post-title-class">Page NOT Found</h1>
</div>
</div>
</div>
</header>
<div class="clearfix" id="main">
<div class="inner-wrap">
<div id="primary">
<div class="clearfix" id="content">
<section class="error-404 not-found">
<div class="page-content">
<header class="page-header">
<h2 class="page-title">Oops! That page can’t be found.</h2>
</header>
<p>It looks like nothing was found at this location. Try the search below.</p>
<form action="https://augustogemelli.com/wordpress/pt/" class="search-form searchform clearfix" method="get">
<div class="search-wrap">
<input class="s field" name="s" placeholder="Search" type="text"/>
<button class="search-icon" type="submit"></button>
</div>
</form><!-- .searchform -->
</div><!-- .page-content -->
</section><!-- .error-404 -->
</div><!-- #content -->
</div><!-- #primary -->
</div><!-- .inner-wrap -->
</div><!-- #main -->
<footer class="clearfix" id="colophon">
<div class="footer-widgets-wrapper">
<div class="inner-wrap">
<div class="footer-widgets-area clearfix">
<div class="tg-one-fourth tg-column-1">
<aside class="widget widget_text" id="text-7"><h3 class="widget-title"><span>Contactos</span></h3> <div class="textwidget"><p><b>Augusto Gemelli</b><br/>
tlm: 968283649<br/>
Info <a href="mailto:geral@augustogemelli.com">geral@augustogemelli.com</a><br/>
Aulas de Cozinha <a href="mailto:cursosdecozinha@augustogemelli.com">cursosdecozinha@augustogemelli.com</a><br/>
Consulting <a href="mailto:consultant@augustogemelli.com">consultant@augustogemelli.com</a></p>
</div>
</aside> </div>
<div class="tg-one-fourth tg-column-2">
</div>
<div class="tg-one-fourth tg-after-two-blocks-clearfix tg-column-3">
</div>
<div class="tg-one-fourth tg-one-fourth-last tg-column-4">
<aside class="widget widget_newsletterwidget" id="newsletterwidget-2"><h3 class="widget-title"><span>Newsletter</span></h3>Quer receber a nossa Newsletter?
<div class="tnp tnp-widget"><form action="https://augustogemelli.com/wordpress/?na=s" method="post" onsubmit="return newsletter_check(this)">
<input name="nr" type="hidden" value="widget"/>
<input name="nl[]" type="hidden" value="0"/>
<div class="tnp-field tnp-field-email"><label>Email</label><input class="tnp-email" name="ne" required="" type="email"/></div>
<div class="tnp-field tnp-field-button"><input class="tnp-submit" type="submit" value="Subscribe"/>
</div>
</form>
</div>
</aside> </div>
</div>
</div>
</div>
<div class="footer-socket-wrapper clearfix">
<div class="inner-wrap">
<div class="footer-socket-area">
<div class="copyright">Copyright © 2021 <a href="https://augustogemelli.com/wordpress/principal/" title="Chef Augusto Gemelli"><span>Chef Augusto Gemelli</span></a>. Powered by <a href="https://wordpress.org" target="_blank" title="WordPress"><span>WordPress</span></a>. Theme: Spacious by <a href="https://themegrill.com/themes/spacious" rel="designer" target="_blank" title="ThemeGrill"><span>ThemeGrill</span></a>.</div> <nav class="small-menu clearfix">
<div class="menu-principal-container"><ul class="menu" id="menu-principal-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-110"><a href="https://augustogemelli.com/wordpress/o-chefe/">O Chefe</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-100"><a href="https://augustogemelli.com/wordpress/aulas/">Aulas</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-167"><a href="https://augustogemelli.com/wordpress/newsletter/">Newsletter</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-166"><a href="https://augustogemelli.com/wordpress/newsletter/">Newsletter</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-121"><a href="https://augustogemelli.com/wordpress/catering/">Catering</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-134"><a href="https://augustogemelli.com/wordpress/consulting/">Consulting</a></li>
</ul></div> </nav>
</div>
</div>
</div>
</footer>
<a href="#masthead" id="scroll-up"></a>
</div><!-- #page -->
<script src="https://augustogemelli.com/wordpress/wp-content/themes/spacious/js/navigation.js?ver=4.9.16" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var newsletter = {"messages":{"email_error":"The email is not correct","name_error":"The name is not correct","surname_error":"The last name is not correct","privacy_error":"You must accept the privacy statement"},"profile_max":"20"};
/* ]]> */
</script>
<script src="https://augustogemelli.com/wordpress/wp-content/plugins/newsletter/subscription/validate.js?ver=5.1.6" type="text/javascript"></script>
<script src="https://augustogemelli.com/wordpress/wp-includes/js/wp-embed.min.js?ver=4.9.16" type="text/javascript"></script>
</body>
</html>
