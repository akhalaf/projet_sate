<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.walltec.com.br/xmlrpc.php" rel="pingback"/>
<title>Página não encontrada – Walltec</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.walltec.com.br/feed/" rel="alternate" title="Feed para Walltec »" type="application/rss+xml"/>
<link href="https://www.walltec.com.br/comments/feed/" rel="alternate" title="Feed de comentários para Walltec »" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.walltec.com.br\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7.19"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.walltec.com.br/wp-content/themes/b2xsolucoes/style.css?ver=4.7.19" id="accelerate_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Roboto%3A400%2C300%2C100%7CRoboto+Slab%3A700%2C400&amp;ver=4.7.19" id="accelerate_googlefonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.walltec.com.br/wp-content/themes/b2xsolucoes/fontawesome/css/font-awesome.css?ver=4.7.0" id="accelerate-fontawesome-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.walltec.com.br/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.walltec.com.br/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.walltec.com.br/wp-content/themes/b2xsolucoes/js/accelerate-custom.js?ver=4.7.19" type="text/javascript"></script>
<!--[if lte IE 8]>
<script type='text/javascript' src='https://www.walltec.com.br/wp-content/themes/b2xsolucoes/js/html5shiv.js?ver=3.7.3'></script>
<![endif]-->
<link href="https://www.walltec.com.br/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.walltec.com.br/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.walltec.com.br/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.7.19" name="generator"/>
<style type="text/css"> .accelerate-button,blockquote,button,input[type=button],input[type=reset],input[type=submit]{background-color:#ed3237}a{color:#ed3237}#page{border-top:3px solid #ed3237}#site-title a:hover{color:#ed3237}#search-form span,.main-navigation a:hover,.main-navigation ul li ul li a:hover,.main-navigation ul li ul li:hover>a,.main-navigation ul li.current-menu-ancestor a,.main-navigation ul li.current-menu-item a,.main-navigation ul li.current-menu-item ul li a:hover,.main-navigation ul li.current_page_ancestor a,.main-navigation ul li.current_page_item a,.main-navigation ul li:hover>a,.main-small-navigation li:hover > a{background-color:#ed3237}.site-header .menu-toggle:before{color:#ed3237}.main-small-navigation li:hover{background-color:#ed3237}.main-small-navigation ul>.current-menu-item,.main-small-navigation ul>.current_page_item{background:#ed3237}.footer-menu a:hover,.footer-menu ul li.current-menu-ancestor a,.footer-menu ul li.current-menu-item a,.footer-menu ul li.current_page_ancestor a,.footer-menu ul li.current_page_item a,.footer-menu ul li:hover>a{color:#ed3237}#featured-slider .slider-read-more-button,.slider-title-head .entry-title a{background-color:#ed3237}a.slide-prev,a.slide-next,.slider-title-head .entry-title a{background-color:#ed3237}#controllers a.active,#controllers a:hover{background-color:#ed3237;color:#ed3237}.format-link .entry-content a{background-color:#ed3237}#secondary .widget_featured_single_post h3.widget-title a:hover,.widget_image_service_block .entry-title a:hover{color:#ed3237}.pagination span{background-color:#ed3237}.pagination a span:hover{color:#ed3237;border-color:#ed3237}#content .comments-area a.comment-edit-link:hover,#content .comments-area a.comment-permalink:hover,#content .comments-area article header cite a:hover,.comments-area .comment-author-link a:hover{color:#ed3237}.comments-area .comment-author-link span{background-color:#ed3237}#wp-calendar #today,.comment .comment-reply-link:hover,.nav-next a,.nav-previous a{color:#ed3237}.widget-title span{border-bottom:2px solid #ed3237}#secondary h3 span:before,.footer-widgets-area h3 span:before{color:#ed3237}#secondary .accelerate_tagcloud_widget a:hover,.footer-widgets-area .accelerate_tagcloud_widget a:hover{background-color:#ed3237}.footer-widgets-area a:hover{color:#ed3237}.footer-socket-wrapper{border-top:3px solid #ed3237}.footer-socket-wrapper .copyright a:hover{color:#ed3237}a#scroll-up{background-color:#ed3237}.entry-meta .byline i,.entry-meta .cat-links i,.entry-meta a,.post .entry-title a:hover{color:#ed3237}.entry-meta .post-format i{background-color:#ed3237}.entry-meta .comments-link a:hover,.entry-meta .edit-link a:hover,.entry-meta .posted-on a:hover,.main-navigation li.menu-item-has-children:hover,.entry-meta .tag-links a:hover{color:#ed3237}.more-link span,.read-more{background-color:#ed3237}@media (max-width: 768px){.better-responsive-menu .sub-toggle{background:#bb0005}}.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,.woocommerce span.onsale {background-color: #ed3237;}.woocommerce ul.products li.product .price .amount,.entry-summary .price .amount,.woocommerce .woocommerce-message::before{color: #ed3237;},.woocommerce .woocommerce-message { border-top-color: #ed3237;}</style>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<link href="https://www.walltec.com.br/wp-content/uploads/2017/03/cropped-logo-walltec-teste-site-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://www.walltec.com.br/wp-content/uploads/2017/03/cropped-logo-walltec-teste-site-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://www.walltec.com.br/wp-content/uploads/2017/03/cropped-logo-walltec-teste-site-180x180.png" rel="apple-touch-icon-precomposed"/>
<meta content="https://www.walltec.com.br/wp-content/uploads/2017/03/cropped-logo-walltec-teste-site-270x270.png" name="msapplication-TileImage"/>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-98834838-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body class="error404 wp-custom-logo better-responsive-menu wide">
<div class="hfeed site" id="page">
<header class="site-header clearfix" id="masthead">
<div class="clearfix" id="header-text-nav-container">
<div class="inner-wrap">
<div class="clearfix" id="header-text-nav-wrap">
<div id="header-left-section">
<div id="header-logo-image">
<a class="custom-logo-link" href="https://www.walltec.com.br/" itemprop="url" rel="home"><img alt="" class="custom-logo" height="121" itemprop="logo" src="https://www.walltec.com.br/wp-content/uploads/2017/03/LOGOWALLTEC3.png" width="257"/></a>
</div><!-- #header-logo-image -->
<div class="screen-reader-text" id="header-text">
<h3 id="site-title">
<a href="https://www.walltec.com.br/" rel="home" title="Walltec">Walltec</a>
</h3>
<p id="site-description">Tudo para: Drywall, Forros e Divisórias</p>
</div><!-- #header-text -->
<div class="aten">
Ligue agora:
</div>
<a href="tel:1234243733"><img id="telefone" src="http://www.walltec.com.br/wp-content/uploads/2017/03/tel-img.png"/></a>
<a href="tel:12988499608"><img id="celular" src="http://www.walltec.com.br/wp-content/uploads/2017/03/cel-img.png"/></a>
<!--<div class="tel">
<div id="telefone"><img id="icone-fone" src="/wp-content/uploads/2017/03/fone.png" /><a href="tel:1234243733">(12) 3424-3733</a></div>
<div id="celular"><img id="icone-fone2" src="/wp-content/uploads/2017/03/whatsup-20.png" /><a href="tel:12988499608"> (12) 98849-9608</a></div>
</div>-->
</div><!-- #header-left-section -->
<div id="header-right-section">
<div class="containersocial">
<div class="siganos">
	Siga-nos:
</div>
<div class="iconesocial">
<ul>
<li id="hover1"><a href="https://www.facebook.com/pages/Walltec-Comercial-LTDA/1592252734335001?ref=hl" target="_blank"><div class="caixasocialface"></div></a></li>
<li id="hover2"><a href="https://twitter.com/Walltec1" target="_blank"><div class="caixasocialtwitter"></div></a></li>
<!--<li id="hover3"><a href="#" target="_blank"><img src="/imagens/icone-youtube.png" /></a></li>-->
</ul>
</div>
</div>
</div><!-- #header-right-section -->
</div><!-- #header-text-nav-wrap -->
</div><!-- .inner-wrap -->
<nav class="main-navigation inner-wrap clearfix" id="site-navigation" role="navigation">
<h3 class="menu-toggle">Menu</h3>
<div class="menu-menu-principal-container"><ul class="menu" id="menu-menu-principal"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-75" id="menu-item-75"><a href="http://www.walltec.com.br/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25" id="menu-item-25"><a href="https://www.walltec.com.br/empresa/">Empresa</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-42" id="menu-item-42"><a href="#">Produtos</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24" id="menu-item-24"><a href="https://www.walltec.com.br/drywall/">Drywall</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40" id="menu-item-40"><a href="https://www.walltec.com.br/forro-modular/">Forro Modular</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-41" id="menu-item-41"><a href="https://www.walltec.com.br/divisoria-eucatex/">Divisória Eucatex</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-181" id="menu-item-181"><a href="https://www.walltec.com.br/kit-porta-pronta/">Kit Porta Pronta</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22" id="menu-item-22"><a href="https://www.walltec.com.br/clientes/">Clientes</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21" id="menu-item-21"><a href="https://www.walltec.com.br/parceiros/">Parceiros</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20" id="menu-item-20"><a href="https://www.walltec.com.br/contato/">Contato</a></li>
</ul></div> </nav>
</div><!-- #header-text-nav-container -->
</header>
<div class="clearfix" id="main">
<div class="inner-wrap clearfix">
<div id="primary">
<div class="clearfix" id="content">
<section class="error-404 not-found">
<div class="page-content">
<header class="page-header">
<h1 class="page-title">Ops! Essa página não pode ser encontrada.</h1>
</header>
<p>Parece que nada foi encontrado aqui. Tente a busca.</p>
<form action="https://www.walltec.com.br/" class="searchform clearfix" id="search-form" method="get">
<input class="s field" name="s" placeholder="Pesquisar" type="text"/>
<input class="submit" id="search-submit" name="submit" type="submit" value="Pesquisar"/>
</form><!-- .searchform -->
</div><!-- .page-content -->
</section><!-- .error-404 -->
</div><!-- #content -->
</div><!-- #primary -->
<div id="secondary">
<aside class="widget widget_search" id="search-2"><form action="https://www.walltec.com.br/" class="searchform clearfix" id="search-form" method="get">
<input class="s field" name="s" placeholder="Pesquisar" type="text"/>
<input class="submit" id="search-submit" name="submit" type="submit" value="Pesquisar"/>
</form><!-- .searchform --></aside> <aside class="widget widget_recent_entries" id="recent-posts-2"> <h3 class="widget-title"><span>Tópicos recentes</span></h3> <ul>
<li>
<a href="https://www.walltec.com.br/ola-mundo/">Olá, mundo!</a>
</li>
</ul>
</aside> <aside class="widget widget_recent_comments" id="recent-comments-2"><h3 class="widget-title"><span>Comentários</span></h3><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link"><a class="url" href="https://wordpress.org/" rel="external nofollow">Um comentarista do WordPress</a></span> em <a href="https://www.walltec.com.br/ola-mundo/#comment-1">Olá, mundo!</a></li></ul></aside><aside class="widget widget_archive" id="archives-2"><h3 class="widget-title"><span>Arquivos</span></h3> <ul>
<li><a href="https://www.walltec.com.br/2017/03/">março 2017</a></li>
</ul>
</aside><aside class="widget widget_categories" id="categories-2"><h3 class="widget-title"><span>Categorias</span></h3> <ul>
<li class="cat-item cat-item-1"><a href="https://www.walltec.com.br/category/sem-categoria/">Sem categoria</a>
</li>
</ul>
</aside><aside class="widget widget_meta" id="meta-2"><h3 class="widget-title"><span>Meta</span></h3> <ul>
<li><a href="https://www.walltec.com.br/wp-login.php">Fazer login</a></li>
<li><a href="https://www.walltec.com.br/feed/">Posts <abbr title="Really Simple Syndication">RSS</abbr></a></li>
<li><a href="https://www.walltec.com.br/comments/feed/"><abbr title="em inglês: Really Simple Syndication">RSS</abbr> dos comentários</a></li>
<li><a href="https://br.wordpress.org/" title="Powered by WordPress, uma plataforma semântica de vanguarda para publicação pessoal.">WordPress.org</a></li> </ul>
</aside> </div>
</div><!-- .inner-wrap -->
</div><!-- #main -->
<footer class="clearfix" id="colophon">
<div class="footer-widgets-wrapper">
<div class="inner-wrap">
<div class="footer-widgets-area clearfix">
<div class="tg-one-third">
</div>
<div class="tg-one-third">
</div>
<div class="tg-one-third tg-one-third-last">
</div>
</div>
</div>
</div>
<div class="footer-socket-wrapper clearfix">
<div class="inner-wrap">
<div class="footer-socket-area">
						Copyright © 2017 Walltec - Todos os direitos reservados.
						<div class="footer-menu">
<div class="b2x">
<ul>
<li> Desenvolvido por:</li>
<li> <a href="http://www.b2xsolucoes.com.br" target="_blank" title="Ligue e solicite um orçamento (12)3624-3367 ou 99716-4299 - B2X Soluções"> <div class="logob2xfooter"></div></a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</footer>
<a href="#masthead" id="scroll-up"><i class="fa fa-long-arrow-up"></i></a>
</div><!-- #page -->
<script src="https://www.walltec.com.br/wp-content/themes/b2xsolucoes/js/navigation.js?ver=4.7.19" type="text/javascript"></script>
<script src="https://www.walltec.com.br/wp-includes/js/wp-embed.min.js?ver=4.7.19" type="text/javascript"></script>
</body>
</html>