<!DOCTYPE html>
<!--[if IEMobile 7]><html class="iem7 sticky-footer external-links ua-brand-icons overlay-menu-scroll-hide" lang="en" dir="ltr"><![endif]--><!--[if lte IE 6]><html class="lt-ie9 lt-ie8 lt-ie7 sticky-footer external-links ua-brand-icons overlay-menu-scroll-hide" lang="en" dir="ltr"><![endif]--><!--[if (IE 7)&(!IEMobile)]><html class="lt-ie9 lt-ie8 sticky-footer external-links ua-brand-icons overlay-menu-scroll-hide" lang="en" dir="ltr"><![endif]--><!--[if IE 8]><html class="lt-ie9 sticky-footer external-links ua-brand-icons overlay-menu-scroll-hide" lang="en" dir="ltr"><![endif]--><!--[if lte IE 9]><html class="lte-ie9 sticky-footer external-links ua-brand-icons overlay-menu-scroll-hide" lang="en" dir="ltr"><![endif]--><!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html class="sticky-footer external-links ua-brand-icons overlay-menu-scroll-hide" dir="ltr" lang="en" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="Drupal 7 (http://drupal.org)" name="Generator"/>
<link href="https://security.arizona.edu/profiles/ua_quickstart/themes/custom/ua_zen/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<title>Page not found | UA Security </title>
<meta content="width" name="MobileOptimized"/>
<meta content="true" name="HandheldFriendly"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=5, user-scalable=yes" name="viewport"/>
<link href="https://cdn.digital.arizona.edu/lib/ua-brand-icons/v2.0.1/ua-brand-icons.min.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdn.uadigital.arizona.edu/lib/ua-bootstrap/v1.0.0-beta.29/ua-bootstrap.min.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://security.arizona.edu/sites/default/files/css/css_zKlU3e7HADJ9NVD0dV5DnLmO-Kg92mBHS4wJTPGYOYk.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://security.arizona.edu/sites/default/files/css/css_L_zxc_wQ8YYa5H1rZ7xybNXnsAaXQWSv7ssgzwjTPRs.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://security.arizona.edu/sites/default/files/css/css_91eP5w694xjkCvtdRIYyiGF8KU3muzGnK_8l1lYV-go.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://security.arizona.edu/sites/default/files/css/css_B-jN1-Q8Zq11-dEm7KOBIM3q7bE6yWak-8ipMkiUDI0.css" media="all" rel="stylesheet" type="text/css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>window.jQuery || document.write("<script src='/profiles/ua_quickstart/modules/contrib/jquery_update/replace/jquery/3.1/jquery.min.js'>\x3C/script>")</script>
<script>jQuery.migrateMute=true;jQuery.migrateTrace=false;</script>
<script src="//code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
<script>window.jQuery && window.jQuery.migrateWarnings || document.write("<script src='/profiles/ua_quickstart/modules/contrib/jquery_update/replace/jquery-migrate/3/jquery-migrate.min.js'>\x3C/script>")</script>
<script src="https://security.arizona.edu/sites/default/files/js/js_38VWQ3jjQx0wRFj7gkntZr077GgJoGn5nv3v05IeLLo.js"></script>
<script>document.createElement( "picture" );</script>
<script src="https://security.arizona.edu/sites/default/files/js/js_-oEDdwe4XpUqUj4W0KJs96ENaXIrGvjBvIP-WYYBi54.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"ua_zen","theme_token":"p4afYt3i2uc6GV1ocF5uwHaPLsbuaTj2E0OA8ntYG4o","js":{"profiles\/ua_quickstart\/modules\/contrib\/picture\/picturefill2\/picturefill.min.js":1,"profiles\/ua_quickstart\/modules\/contrib\/picture\/picture.min.js":1,"profiles\/ua_quickstart\/themes\/custom\/ua_zen\/js\/ua_zen_back_to_top.js":1,"https:\/\/cdn.uadigital.arizona.edu\/lib\/ua-bootstrap\/v1.0.0-beta.29\/ua-bootstrap.min.js":1,"\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/3.1.1\/jquery.min.js":1,"0":1,"1":1,"\/\/code.jquery.com\/jquery-migrate-3.0.0.min.js":1,"2":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery-html-prefilter-3.5.0-backport.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"3":1,"profiles\/ua_quickstart\/modules\/contrib\/extlink\/js\/extlink.js":1},"css":{"https:\/\/cdn.digital.arizona.edu\/lib\/ua-brand-icons\/v2.0.1\/ua-brand-icons.min.css":1,"https:\/\/cdn.uadigital.arizona.edu\/lib\/ua-bootstrap\/v1.0.0-beta.29\/ua-bootstrap.min.css":1,"profiles\/ua_quickstart\/modules\/contrib\/editor\/css\/components\/align.module.css":1,"profiles\/ua_quickstart\/modules\/contrib\/editor\/css\/components\/resize.module.css":1,"profiles\/ua_quickstart\/modules\/contrib\/editor\/css\/filter\/filter.caption.css":1,"profiles\/ua_quickstart\/modules\/contrib\/editor\/modules\/editor_ckeditor\/css\/plugins\/drupalimagecaption\/editor_ckeditor.drupalimagecaption.css":1,"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.theme.css":1,"profiles\/ua_quickstart\/modules\/contrib\/date\/date_repeat_field\/date_repeat_field.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"profiles\/ua_quickstart\/modules\/contrib\/picture\/picture_wysiwyg.css":1,"modules\/search\/search.css":1,"profiles\/ua_quickstart\/modules\/custom\/uaqs_alphabetical_list\/css\/uaqs_alphabetical_list.css":1,"profiles\/ua_quickstart\/modules\/custom\/uaqs_event\/css\/uaqs_event.css":1,"profiles\/ua_quickstart\/modules\/custom\/uaqs_fields\/uaqs_fields_media\/css\/uaqs_fields_media.css":1,"profiles\/ua_quickstart\/modules\/custom\/uaqs_navigation\/uaqs_navigation_global_footer\/css\/uaqs_navigation_global_footer.css":1,"profiles\/ua_quickstart\/modules\/custom\/uaqs_navigation\/uaqs_navigation_resources\/css\/uaqs_navigation_resources.css":1,"profiles\/ua_quickstart\/modules\/custom\/uaqs_news\/css\/uaqs_news.css":1,"profiles\/ua_quickstart\/modules\/custom\/uaqs_person\/css\/uaqs_person.css":1,"profiles\/ua_quickstart\/modules\/custom\/uaqs_social_media\/css\/uaqs_social_media.css":1,"modules\/user\/user.css":1,"profiles\/ua_quickstart\/modules\/contrib\/extlink\/css\/extlink.css":1,"profiles\/ua_quickstart\/modules\/contrib\/views\/css\/views.css":1,"profiles\/ua_quickstart\/modules\/contrib\/ctools\/css\/ctools.css":1,"profiles\/ua_quickstart\/themes\/custom\/ua_zen\/css\/ua_zen_back_to_top.css":1,"profiles\/ua_quickstart\/themes\/custom\/ua_zen\/system.menus.css":1,"profiles\/ua_quickstart\/themes\/custom\/ua_zen\/system.messages.css":1,"profiles\/ua_quickstart\/themes\/custom\/ua_zen\/system.theme.css":1,"profiles\/ua_quickstart\/themes\/custom\/ua_zen\/css\/styles.css":1}},"currentPath":"phishing-alerts\/82316-academic-dishonesty-chinese-u-hong-kong-professor-and-dean-hiding-dissertation\t","currentPathIsAdmin":false,"urlIsAjaxTrusted":{"\/phishing-alerts\/82316-academic-dishonesty-chinese-u-hong-kong-professor-and-dean-hiding-dissertation%09%0A":true},"extlink":{"extTarget":"_blank","extClass":0,"extLabel":"(link is external)","extImgClass":0,"extIconPlacement":"append","extSubdomains":0,"extExclude":"","extInclude":"","extCssExclude":"","extCssExplicit":"","extAlert":0,"extAlertText":"This link will take you to an external web site.","mailtoClass":"mailto","mailtoLabel":"(link sends e-mail)","extUseFontAwesome":false}});</script>
<!--[if lt IE 9]>
    <script src="/profiles/ua_quickstart/themes/custom/ua_zen/js/html5-respond.js"></script>
    <![endif]-->
</head>
<body class="html not-front not-logged-in no-sidebars page-phishing-alerts page-phishing-alerts-82316-academic-dishonesty-chinese-u-hong-kong-professor-and-dean-hiding-dissertation section-phishing-alerts">
<p id="skip-link">
<a class="sr-only sr-only-focusable" href="#main_nav">Jump to navigation</a>
</p>
<header class="page-row l-arizona-header bg-red bg-cochineal-red" id="header_ua">
<section class="container l-container">
<div class="row">
<div class="col-xs-10 col-sm-6">
<a class="arizona-logo remove-external-link-icon" href="https://www.arizona.edu" title="The University of Arizona homepage">
<img alt="The University of Arizona Wordmark Line Logo White" class="arizona-line-logo" src="https://cdn.digital.arizona.edu/logos/v1.0.0/ua_wordmark_line_logo_white_rgb.min.svg"/>
</a>
</div>
<div class="col-xs-2 col-sm-6">
<div class="row">
<div class="region region-header-ua-utilities">
<div class="col-sm-7 col-md-5 col-lg-5 col-lg-offset-4 no-gutter col-md-offset-3"><div class="search-form text-size-h6 visible-sm visible-md visible-lg"><div class="block block-bean first last odd" id="block-bean-search-bean-block" role="search">
<form accept-charset="UTF-8" action="/phishing-alerts/82316-academic-dishonesty-chinese-u-hong-kong-professor-and-dean-hiding-dissertation%09%0A" id="search-block-form" method="post"><div><div class="container-inline">
<h2 class="element-invisible">Search form</h2>
<div class="form-item form-item-search-block-form form-type-textfield form-group"><input class="input-search form-control form-control form-text" id="edit-search-block-form--2" maxlength="128" name="search_block_form" onblur="this.placeholder = 'Search Site...'" onfocus="this.placeholder = ''" placeholder="Search Site..." size="15" tabindex="0" title="Enter the terms you wish to search for." type="text" value=""/> <label class="control-label element-invisible" for="edit-search-block-form--2">Search</label>
</div><div class="form-actions form-wrapper" id="edit-actions"><input class="btn btn-search radius form-submit" id="edit-submit" name="op" type="submit" value="Search"/></div><input name="form_build_id" type="hidden" value="form-3HcK3RsQysJM1NWl7hx0X6IlltZBQTJQEpk1jwriNx4"/>
<input name="form_id" type="hidden" value="search_block_form"/>
</div>
</div></form>
</div>
</div><a class="search-input-link visible-xs pull-right" href="/search/node"></a></div> </div>
</div>
</div>
</div>
</section>
</header>
<header class="header page-row" id="header_site" role="banner">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-6 col-lg-4">
<a class="header__logo" href="/" id="logo" rel="home" title="UA Security  | Home"><img alt="UA Security  | Home" class="header__logo-image" src="https://security.arizona.edu/sites/default/files/UA_UAInfoSec_Webheader-01.png"/></a> </div>
<div class="col-xs-12 col-sm-6 col-lg-8">
<div class="row">
<div class="col-xs-12">
<div class="header__region region region-header">
<div class="col-lg-3 right-buffer-lg-1 left-buffer-lg-min col-md-6 col-sm-12 col-xs-12 mb-4 my-6 py-2"><div aria-label="call to action link" class="block block-bean first odd" id="block-bean-phishing-alerts" role="complementary">
<div about="/block/phishing-alerts" class="entity entity-bean bean-uaqs-call-to-action clearfix">
<div class="content">
<a class="gta-cta-header-buttons btn btn-default btn-block" href="https://security.arizona.edu/phishing_alerts">Phishing Alerts</a> </div>
</div>
</div>
</div><div class="col-lg-3 right-buffer-lg-1 left-buffer-lg-min bottom-buffer-lg-min col-md-6 col-sm-12 col-xs-12 mb-4"><div aria-label="call to action link" class="block block-bean even" id="block-bean-report-a-phish" role="complementary">
<div about="/block/report-a-phish" class="entity entity-bean bean-uaqs-call-to-action clearfix">
<div class="content">
<a class="gta-cta-header-buttons btn btn-hollow-default btn-block" href="https://security.arizona.edu/content/report-phish">Report Phish</a> </div>
</div>
</div>
</div><div class="col-lg-3 right-buffer-lg-1 left-buffer-lg-min bottom-buffer-lg-min col-md-6 col-sm-12 col-xs-12 mb-4"><div aria-label="call to action link" class="block block-bean odd" id="block-bean-report-an-incident" role="complementary">
<div about="/block/report-an-incident" class="entity entity-bean bean-uaqs-call-to-action clearfix">
<div class="content">
<a class="gta-cta-header-buttons btn btn-hollow-default btn-block" href="https://security.arizona.edu/content/report-incident">Report Incident</a> </div>
</div>
</div>
</div><div class="col-lg-3 right-buffer-lg-1 left-buffer-lg-min bottom-buffer-lg-min col-md-6 col-sm-12 col-xs-12 mb-4"><div aria-label="call to action link" class="block block-bean last even" id="block-bean-contact-us" role="complementary">
<div about="/block/contact-us" class="entity entity-bean bean-uaqs-call-to-action clearfix">
<div class="content">
<a class="gta-cta-header-buttons btn btn-hollow-default btn-block" href="https://security.arizona.edu/content/contact-us">Contact Us</a> </div>
</div>
</div>
</div> </div>
</div>
</div>
</div>
</div> <!-- /.row -->
</div> <!-- /.container -->
</header>
<div class="page-row page-row-expanded page-row-padding-bottom page" id="main" role="main">
<div class="container">
<div class="row">
<div aria-label="breadcrumb" class="col-xs-12">
<div class="item-list"></div> </div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-sm-12" id="content">
<a id="main-content"></a>
<h1 class="page__title title mt-4" id="page-title">Page not found</h1>
                                        


The requested page "/phishing-alerts/82316-academic-dishonesty-chinese-u-hong-kong-professor-and-dean-hiding-dissertation%09%0A" could not be found.                </div>
</div>
</div>
</div> <!-- /.main -->
<!-- back to top button --->
<button aria-label="Return to the top of this page." class="ua-zen-back-to-top btn btn-primary" id="js-ua-zen-back-to-top" role="button"><span class="sr-only">Return to the top of this page.</span>
<i class="ua-brand-up-arrow"></i>
</button>
<footer class="page page-row" id="footer_site" role="contentinfo">
<div class="region region-footer">
<div class="container">
<div class="row">
<div class="page-row-padding-top page-row-padding-bottom"></div>
<div class="page-row-padding-top page-row-padding-bottom"></div>
<div class="col-xs-12 col-sm-5 col-md-4 col-lg-4 text-center-xs text-left-not-xs">
<div class="row bottom-buffer-30">
<div class="col-xs-12">
<a class="remove-external-link-icon" href="/" title="Home"><img alt="Home" src="https://security.arizona.edu/sites/default/files/UA_UAInfoSec_Webheader-01.png"/></a> </div>
</div>
</div>
<div class="col-xs-12">
<hr/>
</div>
</div>
</div>
</div>
<div class="region region-footer-sub" id="footer_sub">
<div class="container">
<div class="row">
<div class="block block-uaqs-social-media first last odd col-xs-6 col-sm-4 col-md-2" id="block-uaqs-social-media-uaqs-social-media-links" role="complementary">
<h5><strong class="text-uppercase">Connect with Us</strong></h5>
<div class="content">
<ul id="social-media-links"><li class="uaqs-social-media"><a href="https://www.facebook.com/uarizonainfotech" target="_blank" title="Click here to visit our Facebook page"><i class="ua-brand-facebook"></i>Facebook</a></li><li class="uaqs-social-media"><a href="https://twitter.com/UaInfotech" target="_blank" title="Click here to visit our Twitter page"><i class="ua-brand-twitter"></i>Twitter</a></li><li class="uaqs-social-media"><a href="https://www.instagram.com/uainfotech" target="_blank" title=""><i class="ua-brand-instagram"></i>Instagram</a></li></ul> </div>
</div>
</div>
<div class="row">
<div class="col-xs-12 text-center">
<hr/>
<p class="small"><a href="https://www.arizona.edu/information-security-privacy" target="_blank">University Information Security and Privacy</a></p> <p class="copyright small">© 2021 The Arizona Board of Regents on behalf of <a href="https://www.arizona.edu" target="_blank">The University of Arizona</a>.</p> </div>
</div>
</div>
</div>
</footer>
<script src="https://security.arizona.edu/sites/default/files/js/js_7Ukqb3ierdBEL0eowfOKzTkNu-Le97OPm-UqTS5NENU.js"></script>
<script src="https://security.arizona.edu/sites/default/files/js/js_8GVuv2Hl6fBw_L58vpbkv8DCxAOU-DQ5BXilXJJJOa8.js"></script>
<script src="https://cdn.uadigital.arizona.edu/lib/ua-bootstrap/v1.0.0-beta.29/ua-bootstrap.min.js"></script>
</body>
</html>
