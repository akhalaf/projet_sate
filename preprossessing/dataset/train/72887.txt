<!DOCTYPE html>
<html><!-- InstanceBegin template="/Templates/acute.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Acute Systems Home Page</title><!-- InstanceEndEditable -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><!-- InstanceBeginEditable name="head" -->
<meta content="Acute Systems software. TransMac - Open Mac APFS/HFS disks and dmg files from a Windows PC. CrossFont - Convert OpenType, TrueType and PostScript type-1 pfb fonts between Mac and Windows PC." name="description"/><!-- InstanceEndEditable -->
<link href="styles.css" rel="stylesheet" type="text/css"/>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
    (adsbygoogle = window.adsbygoogle || []).push({
      google_ad_client: "ca-pub-4661185817270698",
      enable_page_level_ads: true
    });
  </script>
</head>
<body>
<div class="header">
<img alt="Acute Systems" border="0" src="images/logo.gif"/>
<ul id="top-ul">
<li class="main">
<a href="index.htm">Home</a>
</li>
<li class="main">
<a href="index.htm">Software</a>
<ul class="sub">
<li>
<a href="scrtm.htm">TransMac</a>
</li>
<li>
<a href="scrcf.htm">CrossFont</a>
</li>
<li>
<a href="free.htm">Free Software</a>
</li>
</ul>
</li>
<li class="main">
<a href="support.htm">Support</a>
<ul class="sub">
<li>
<a href="support.htm">Support</a>
</li>
<li>
<a href="forms/feedback.htm">Contact</a>
</li>
<li>
<a href="about.htm">About</a>
</li>
<li>
<a href="privacy.htm">Privacy Policy</a>
</li>
</ul>
</li>
</ul>
<br/><br/>
</div>
<table border="0" cellpadding="15" width="100%">
<tr>
<td valign="top">
<!-- InstanceBeginEditable name="EditRegion" -->
<h1>Welcome to the Acute Systems Software web site!</h1>
<b class="hd1">Welcome to the Acute Systems Software web site!</b>
<hr/>

        New Updates:<br/>
<b>
		Dec 26, 2020: TransMac v14.1<br/>
        Dec 2, 2020: CrossFont v7.9<br/>
</b><br/>
<table border="2" cellpadding="8">
<tr>
<td valign="top">
<p><a href="scrtm.htm"><img alt="TransMac Screen" border="0" height="140" hspace="10" src="images/tmscrs.gif" style="float:right" vspace="5" width="200"/></a> <span class="hd1 c18"><a href="scrtm.htm">TransMac</a></span><br/>
<br/>
              Open Mac APFS/HFS+/HFS format disk drives, flash drives, CD/DVD/Blu-ray media, HD floppies, dmg, dmgpart, sparsebundle and sparseimage files.<br/>
              Runs under all versions of Windows 10, 8, 7, Vista and XP.</p>
<p>Single user license fee: <strong>US $59.00</strong>. <a href="regtm.htm">Site and multiple license discounts available</a>.</p>
<p><a href="scrtm.htm"><img alt="Details" border="0" src="images/learn_more.gif"/></a>    <a href="http://www.acutesystems.com/tmac/tmsetup.zip"><img alt="Download" border="0" src="images/download.gif"/></a>     <a href="https://sites.fastspring.com/acutesystems/instant/transmac" target="_blank"><img alt="Buy Now" border="0" src="images/buy_now.gif"/></a></p>
</td>
</tr>
<tr>
<td valign="top">
<p><a href="scrcf.htm"><img alt="CrossFont Screen" border="0" height="141" hspace="10" src="images/cfscrs.gif" style="float:right" vspace="5" width="155"/></a> <span class="hd1 c18"><a href="scrcf.htm">CrossFont</a></span>     <br/>
<br/>
              Convert PostScript Type 1 fonts to OpenType. Convert TrueType and PostScript Type1 fonts between Macintosh and PC platforms.<br/>
              Create WOFF and EOT web fonts from TrueType/OpenType fonts.<br/>
              Runs under all versions of Windows 10, 8, 7, Vista and XP.</p>
<p>Single user license fee: <strong>US $79.00</strong>. <a href="regcf.htm">Site and multiple license discounts available</a>.</p>
<p><a href="scrcf.htm"><img alt="Details" border="0" src="images/learn_more.gif"/></a>    <a href="http://www.acutesystems.com/cfnt/cfsetup.zip"><img alt="Download" border="0" src="images/download.gif"/></a>     <a href="https://sites.fastspring.com/acutesystems/instant/crossfont" target="_blank"><img alt="Buy Now" border="0" src="images/buy_now.gif"/></a></p>
</td>
</tr>
<tr>
<td valign="top">
<p><span class="hd1 c18"><a href="free.htm">Free Software</a></span>
</p>
<ul class="glist">
<li><strong>Acute Photo EXIF Viewer</strong> - View photo and image metadata (EXIF, IPTC/NAA, etc.).</li>
<li><strong>Acute Font Report</strong>- Preview OpenType, TrueType and PostScript Type 1 fonts. View font properties including which OpenType features are supported.</li>
<li><strong>Acute Batch Image Processor</strong>- Batch convert and resize image files.</li>
</ul>
</td>
</tr>
</table>
<p> </p>
<p> <!-- InstanceEndEditable -->
</p></td>
</tr>
</table>
<div class="footer">
    Copyright Â© 2019 Acute Systems. All rights reserved. The contents of these pages are provided "as is" and without warranty of any kind. Product and company names mentioned herein may be the trademarks of their respective owners.<br/>
<br/>
<table class="fmenu">
<tr>
<th>
<a href="index.htm">Software</a>
</th>
<th>
<a href="support.htm">Support</a>
</th>
<th>Information Articles</th>
</tr>
<tr>
<td>
<a href="scrtm.htm">TransMac</a>
</td>
<td>
<a href="about.htm">About</a>
</td>
<td>
<a href="files.htm">Mac and Windows PC Files</a>
</td>
</tr>
<tr>
<td>
<a href="scrcf.htm">CrossFont</a>
</td>
<td>
<a href="forms/feedback.htm">Contact</a>
</td>
<td>
<a href="dmg.htm">DMG Files in Windows</a>
</td>
</tr>
<tr>
<td>
<a href="free.htm">Free Software</a>
</td>
<td>
<a href="privacy.htm">Privacy Policy</a>
</td>
<td>
<a href="fonts.htm">Mac and Windows PC Fonts</a>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
</tr>
</table>
</div>
</body>
<!-- InstanceEnd --></html>
