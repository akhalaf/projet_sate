<!DOCTYPE html>
<!--[if lt IE 8]><html class="lt-ie9 lt-ie8 landing" lang="en"><![endif]--><!--[if IE 8]><html class="lt-ie9 landing" lang="en"><![endif]--><!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8"/>
<title>404 - Pemberton Valley Dyking District</title>
<meta content="width=device-width,initial-scale=1,maximum-scale=2.0,maximum-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="keywords"/>
<link href="/theme/v2/css/style.css?=v1" rel="stylesheet"/>
<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
<script src="/theme/v2/js/modernizr.custom.js"></script>
<link href="/images/icons/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/images/icons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/images/icons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/images/icons/site.webmanifest" rel="manifest"/>
<link color="#5bbad5" href="/images/icons/safari-pinned-tab.svg" rel="mask-icon"/>
<link href="/images/icons/favicon.ico" rel="shortcut icon"/>
<meta content="#da532c" name="msapplication-TileColor"/>
<meta content="/images/icons/browserconfig.xml" name="msapplication-config"/>
<meta content="#ffffff" name="theme-color"/>
<!-- typekit font rendering -->
<script>
  (function(d) {
    var config = {
      kitId: 'zra5vyr',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>
<!-- fouc prevention for typekit, but make sure those without js aren't left in the dark -->
<noscript>
<style>
header,
.subHeader,
main,
footer {
 opacity: 1 !important;
 visibility: visible !important;
}
</style>
</noscript>
</head>
<body class="drawer drawer--left">
<div class="siteWrapper">
<header class="drawer-navbar drawer-navbar--fixed shortHeader" role="banner">
<div class="drawer-container">
<div class="drawer-navbar-header---OFF">
<div class="logo__header">
<a class="logo" href="//www.pvdd.ca/">
<span class="logo_graphic"><img alt="" src="/theme/v2/gfx/pvdd_logo.svg"/></span>
</a>
</div><!-- logo -->
<button class="drawer-toggle drawer-hamburger" type="button">
<span class="sr-only">Site Menu</span>
<span class="drawer-hamburger-icon"></span>
</button>
</div>
<div class="globalNav">
<div class="globalNavGrid">
<div class="">
<ul class="utilityNav">
<li><a href="/">Home</a></li>
<li><a href="/secure/">Staff</a></li>
<li><a href="/contact/">Contact</a></li>
</ul>
</div>
<div class="">
<div class="secondaryNavItems">
<div class="miniSearchFormWrapper">
<form action="https://www.pvdd.ca/search/">
<input name="q" placeholder="Site Search" type="search"/>
</form>
</div>
</div> <!-- secondaryNavItems-->
</div>
</div>
<nav class="drawer-nav primaryNav" role="navigation">
<ul class="drawer-menu">
<li class="drawer-dropdown">
<a aria-expanded="false" class="drawer-menu-item" data-target="#" data-toggle="dropdown" href="#" role="button">Environment <span class="drawer-caret"></span></a>
<ul class="drawer-dropdown-menu">
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/pemberton/"><strong>Pemberton Valley</strong></a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/pemberton/geography/">Geography</a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/pemberton/geology/">Geology</a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/pemberton/history/">History</a></li>
</ul>
</li>
<li class="drawer-dropdown">
<a aria-expanded="false" class="drawer-menu-item" data-target="#" data-toggle="dropdown" href="#" role="button">Dyking Infrastructure <span class="drawer-caret"></span></a>
<ul class="drawer-dropdown-menu">
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/dyking/"><strong>Dyking Infrastructure</strong></a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/dyking/flood_protection/">Flood Protection</a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/dyking/engineering/">Operations &amp; Maintenance</a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/dyking/dyke_maintenance/">Dyke Maintenance and Monitoring</a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/dyking/rip-rap-bank-protection/">Rip Rap Bank Protection</a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/dyking/ditches-and-culverts/">Ditches and Culverts</a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/dyking/gravel-management/">Gravel Management</a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/dyking/high_water_events/">High Water Events</a></li>
</ul>
</li>
<li class="drawer-dropdown">
<a aria-expanded="false" class="drawer-menu-item" data-target="#" data-toggle="dropdown" href="#" role="button">PVDD <span class="drawer-caret"></span></a>
<ul class="drawer-dropdown-menu">
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/pvdd/"><strong>PVDD</strong></a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/pvdd/administration/">Administration</a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/pvdd/board/">Board</a></li>
</ul>
</li>
<li class="drawer-dropdown">
<a aria-expanded="false" class="drawer-menu-item" data-target="#" data-toggle="dropdown" href="#" role="button">News <span class="drawer-caret"></span></a>
<ul class="drawer-dropdown-menu">
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/news/"><strong>News</strong></a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/news/request-for-proposals/">Request For Proposals</a></li>
</ul>
</li>
<li class="drawer-dropdown">
<a aria-expanded="false" class="drawer-menu-item" data-target="#" data-toggle="dropdown" href="#" role="button">FAQs <span class="drawer-caret"></span></a>
<ul class="drawer-dropdown-menu">
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/faqs/"><strong>FAQs</strong></a></li>
</ul>
</li>
<li class="drawer-dropdown">
<a aria-expanded="false" class="drawer-menu-item" data-target="#" data-toggle="dropdown" href="#" role="button">Resources <span class="drawer-caret"></span></a>
<ul class="drawer-dropdown-menu">
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/resources/"><strong>Resources</strong></a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/resources/pvdd-letters-patent/">PVDD Letters Patent</a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/resources/meeting-minutes/">Meeting Minutes</a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/resources/links/">Links</a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/resources/studies_reports/">Studies and Reports</a></li>
<li><a class="drawer-dropdown-menu-item" href="//www.pvdd.ca/resources/pvdd-boundary/">PVDD Boundary</a></li>
</ul>
</li>
</ul>
</nav>
</div><!-- globalNav -->
</div>
</header>
<main>
<h1>Page not found</h1>
<p>What’s worse, a hilarious 404 page can’t be found either.</p>
<section class="bg-white">
<div class="container pad--top pad--bottom">
<span class="slug">At a glance</span>
<h2>PVDD Quick Links</h2>
<div class="ctaBoxes grid-wrap">
<div class="grid-col bp1-col-size1of2 bp2-col-size1of4 mvxl">
<div class="ctaBox">
<a href="//www.pvdd.ca/dyking/">
<img alt="Flood Protection" src="/assets/images/thumb-2.jpg"/>
<span>Flood Protection</span>
</a>
</div>
</div>
<div class="grid-col bp1-col-size1of2 bp2-col-size1of4 mvxl">
<div class="ctaBox">
<a href="//www.pvdd.ca/pvdd/">
<img alt="PVDD Roles" src="/assets/images/thumb-1.jpg"/>
<span>PVDD Roles</span>
</a>
</div>
</div>
<div class="grid-col bp1-col-size1of2 bp2-col-size1of4 mvxl">
<div class="ctaBox">
<a href="//www.pvdd.ca/faqs/">
<img alt="PVDD FAQs" src="/assets/images/DSC00185_2_thumb.JPG"/>
<span>PVDD FAQs</span>
</a>
</div>
</div>
<div class="grid-col bp1-col-size1of2 bp2-col-size1of4 mvxl">
<div class="ctaBox">
<a href="//www.pvdd.ca/resources/">
<img alt="Web Resources" src="/assets/images/thumb-4.jpg"/>
<span>Web Resources</span>
</a>
</div>
</div>
</div><!-- calloutboxes -->
</div><!-- container -->
</section>
</main>
<footer>
<div class="constrain-width-wide container bg-forest">
<div class="grid">
<div class="col col-md col-4 col-middle">
<div class="addressInfo bp2-text-left">
<strong>Communicate Via:</strong><br/>
<b>e:</b> trustees@pvdd.ca<br/>
<b>p:</b> (604) 894-6632<br/>
<b>f:</b> (604) 894-5271
						</div>
</div>
<div class="col col-md col-4 col-middle">
<a class="logo" href="//www.pvdd.ca/">
<span class="logo__logoicon"><img alt="" src="/theme/v2/gfx/tree.png"/></span>
</a>
</div>
<div class="col col-md col-4 col-middle">
<div class="addressInfo bp2-text-right">
<strong>Pemberton Valley Dyking District</strong><br/>
							1381 Aster St, Box 235<br/>
							Pemberton, BC V0N 2L0
						</div>
</div>
</div><!-- grid-wrap -->
</div><!-- container -->
<div class="bg-dark">
<div class="constrain-width-wide footerContainer">
<div class="grid">
<div class="col col-md col-middle">
<ul class="footerNav">
<li><a href="//www.pvdd.ca/sitemap/">Site Map</a></li>
<li><a href="//www.pvdd.ca/secure/">Board Login</a></li>
<li><a href="//www.pvdd.ca/privacy/">Privacy Policy</a></li>
<li><a href="//www.pvdd.ca/contact/">Contact Us</a></li>
</ul>
</div>
<div class="col col-md col-middle">
<p class="footer--copyright bp2-text-right">Copyright © 2021. All Rights Reserved. <span>Site by <a href="https://vibe9.design/" target="_blank">Vibe9</a></span></p>
</div>
</div>
</div>
</div>
</footer>
</div>
<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/theme/v2/js/jquery.magnific.popup.0.9.4.min.js" type="text/javascript"></script>
<script src="/theme/v2/js/jq.plugin.libs.js" type="text/javascript"></script>
<script src="/theme/v2/js/jq.plugins.js" type="text/javascript"></script>
<script type="text/javascript">
		
	$(document).ready(function() {
		 
		 $('.drawer').drawer();
	});
				
		$(function() {
		    var header = $(".tallHeader");
		    $(window).scroll(function() {    
		        var scroll = $(window).scrollTop();
		    
		        if (scroll >= 300) {
		            header.removeClass('tallHeader').addClass("shortHeader");
		        } else {
		            header.removeClass("shortHeader").addClass('tallHeader');
		        }
		    });
		});	
		
		

	</script>
</body>
</html>
