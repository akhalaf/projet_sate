<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "38581",
      cRay: "610b7e145ef9a354",
      cHash: "e7123ce96e1e3ba",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYWJ3LmJ5Lw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "yOgYM1ex+el+ZCeQfEHBSYkfabeEVd5OYQBGCDwWnalV+zSh1uY8GfYHEy4wZqnWVuZJYtVW5Fu6rOoGZlNJP6URQ7Tvddbs/uNvcg0kFhS87Sxg99gcY44qIIyjBgioHsA4YMs/amKWUN2g+HeHzD/WV7h16I+iATSnOh0lejRz7/gStxtGnvcdsM11tVtYetqYeFyTLLbnzfGQM30KO/6pIFlTb+jPY8gnwIwIgWTdZmZY0AlAr+qXSdUOlcwYUuJSLzHjAuV5Gxmnynv1SiukmObZF9y5NJzrjghtA6LIEBbYK4nvOTiY6IJnKsKS0zf2JfrqQD1CZpmpiwHq4oeE21Sdj5aZisqMte5sCX9M/cBUdc/p3VeET2c08kn7DJmc3X5gRIqkJikyfXGu71LcVdf6KTkuPJgvbNxLXieB7a4r6uiumWKqMCe8yGV5njZudAdGNGvuSyKsnvVICax7GR5/j0olmEMyzhdwB5BEJqNk7l5gKv5guquvZaFrANhRtHWj7vWCj4xT9tB7Zx2M5/NQ43gdtdxXfXf/Kupi44Rv1qaAPE4qWJjCGR0aARQcC93cuqKrdYKvFrMJ/Y/Wfe3FgaE3bvWVlnrhWSpRFG0rqxgO0w7O7Bow6fd0SzJuLsPHdkic7iy+DQPrubkS6BHbDRM3o/1CACKl16zP87TM9jvMDyK2yVhjIrfnrQL8hqpQLObSTCyMI9YFLa0g7y+OiswhVSi3FPa9dKNqqOp66TUzoTDs9m02RaXI",
        t: "MTYxMDUwMjg5OS44OTgwMDA=",
        m: "lFEZsLch3QXY0hFltjfnwtOqn+AoLYsH0c3TD1VqrW8=",
        i1: "anCh8u8QdD4BTAkBs3+oAw==",
        i2: "FTt4btyAnq5czlP6QmlYug==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "5/V3AmEyttotIkavoyZaPD2u00V7ZrEvuTJYwFxq7Yg=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610b7e145ef9a354");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> abw.by.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=647571f71eff517895309b396c6302e6350b1d49-1610502899-0-AXTmZ7AaUhCvdOi9gVp-WNRnjkxNr6MWHDqa6_RhSWl0IDuU0CfgJQCdb2-9-ISDSBkOB4jz4vVziOa8w8UmR4UYfxxV0hkVFFMrWObAq7ZXfPKpg25ziPbHcDrzN9_WI-Ja6VyCzm6QVh2EYK9Ifc5j6dMZgtEmMf0q2Gq_0yWiSCdjJSJx5MFtn3e_m_XK7L3DqBMFw1olSUnFGQ5hV-smCYIfKd38gyCuydlgbkMpcu_JQ6vKXD1rDMxYkRq6k4ZMKCXjkn8Uxa0McEkQxBdw6F7_v_eRjWJNA8UGwGhie_VdPqJTJU1qKPQMDTI5AQ" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="2c92e04dbf79609ddec866555bce481aedb91326-1610502899-0-AcwDIVYuS9uOnE7HxutH97yA6M+KDbPD8CUKeo4JeZOQMWExOfH1uU5nAilg5/eALTAKpILcs6/sZIuAQw8/w81nRw/vg0pkuTcWJVmnONesRVxZ9c+1DsebOFSZs+1LBug2bpCvtDw8sc9WFB1+YzyW552JDlazdui9f/R8DJG1tBeWmWNeYCNPdfK5gg+LGKoUlmzJXnK1Gl3BftaX4IgkI0CyVCnWzovgA4Jz96LGqn383KmMeWDYUwn+B2rr+DQbfmGSH68dR2Jl42Jucec5JGDcX6nB8vPww+lstegtLELKSM5JU8J/DThrw2kC3hQMhh9LmQ7iwEL2mo/TzeiYPk+pYKKZXNWk9EZ5TGtWUDBLLLIXYD2iDbp2dUBZ0x21dTVC6d+07mZep4N9ggwNcntt9G5d2WjV7si0ypY85XTxyvdQ+xXPpTj7lQQmDAbSQ9Sqf+8RenIF4VJtMx4N4E/HeCUe8zI7IsBBbBRq29dD8LW+kEykQ7f8TCGT8gvTUWr9BgQVSg8Rlq35CadXeZQQBzBl47xGC7EytYhP+EY78zAmQFYuG/PFb21T7+KS+gtcN9qqgC2TBfd7SC8QeLtT0ExJasnU3C6zRfl5tIkc4+gob2mtcWoZD4tYZnYepGahy4Pbh/8q8zyaxaohno6TRsf+DjEalzRHL/4EgIKg1nlg+cmF5/IRviyYn4g9qjHHtf9a0MbgMdKFKPmC6pTUCUjFmKYkRT1roWddEhlRS4JV+2YZdX/VWIMo3UlMXA8vAVvxsn0CbQxKjG0u3gVYpQBrmop2Q1hyqy3vUfqUht1n1Ue48fljUk/eWnePN/rBAKAFRZ/yuR1r59i2owGfDvsdUiq/r8d32Av9jTFtGNjhuRIQB847GtFJTFGZm3cheuIkf5rsZ064DzyMc0TuOMsc9cVauvUZUED3qvCggykTSDfGx1g5lnM1+tozkwBdojEryq4EQS0/xWb6uCcaEBaNw+tIazbK6/Oc2kDwFqyGDe84w/wfB9vVKjVPWQkt8/R3GBHbpBcL7L08faZdLeD9lr0Q6qXFqPSUUvgbBYYgk/L72243DhWIVo7tfEo/MWCkpD3ZDN1eSFNk+iuGL2QGLu+ozCazExhweNeaWUQTqlDwzmAazZ+RYeoiVDRdE//uD9Y0lV2PCN04n9ncFAQRsM/PCKGaSFX5m671OCL3RimZvwdxUWddZ7Ho7MygGK7D0GiRNMEqelHtd6VIP/viTjb6WlMH7pzywRtX4TX52dkENhJ9xu+ZlJDJrwmQjXajmtCpW//9nyE="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="2e8e77c97516e0c33b8d5b71c2f228c0"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610502903.898-xe+SvQTN+D"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610b7e145ef9a354')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610b7e145ef9a354</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
