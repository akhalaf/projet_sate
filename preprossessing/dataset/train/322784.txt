<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" lang="it-it" xml:lang="it-it" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>404 - Errore: 404</title>
<link href="/templates/system/css/error.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="error">
<div id="outline">
<div id="errorboxoutline">
<div id="errorboxheader">404 - URL invalid</div>
<div id="errorboxbody">
<p><strong>Non è possibile visualizzare questa pagina, potrebbe essere a causa di:</strong></p>
<ol>
<li>un <strong>bookmark/preferiti scaduto</strong></li>
<li>una ricerca attraverso il motore di ricerca che ha una <strong>indicizzazione non aggiornata di questo sito</strong></li>
<li>un <strong>indirizzo compilato male</strong></li>
<li><strong>non hai accesso</strong> a questa pagina</li>
<li>la risorsa richiesta non esiste.</li>
<li>si è verificato un errore durante l'esecuzione della richiesta.</li>
</ol>
<p><strong>Prova a visitare una delle seguenti pagine</strong></p>
<ul>
<li><a href="/index.php" title="Vai alla Home Page">Home Page</a></li>
</ul>
<p>Se persistono delle difficoltà, contatta l'Amministratore di questo sito e riporta l'errore.</p>
<div id="techinfo">
<p>URL invalid</p>
<p>
</p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
