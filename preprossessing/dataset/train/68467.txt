<!DOCTYPE html>
<html class="no-js" lang="">
<head>
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<title>Sorority Row (2009) Soundtrack from the Motion Picture</title>
<meta content="Sorority Row (2009) soundtrack, track listing" name="keywords"/>
<meta content="Sorority Row (2009) Soundtrack from the Motion Picture" name="description"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="no-cache" http-equiv="Pragma"/>
<meta content="no-cache" http-equiv="Cache-Control"/>
<meta content="Wed, 17 Aug 2016 05:00:00 GMT" http-equiv="Expires"/>
<meta content="en-us, en-gb, en-au, en-ca" http-equiv="content-language"/>
<meta content="320088948016615" property="fb:app_id"/>
<meta content="Sorority Row (2009) Soundtrack from the Motion Picture" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="https://www.aceshowbiz.com/soundtrack/sorority_row/" property="og:url"/>
<meta content="" property="og:image"/>
<meta content="AceShowbiz" property="og:site_name"/>
<meta content="Sorority Row (2009) Soundtrack from the Motion Picture" property="og:description"/>
<meta content="Sorority Row (2009) Soundtrack from the Motion Picture" property="og:image:alt"/>
<meta content="Sorority Row (2009) Soundtrack from the Motion Picture" name="twitter:title"/>
<meta content="Sorority Row (2009) Soundtrack from the Motion Picture" name="twitter:description"/>
<meta content="" name="twitter:image"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@aceshowbiz" name="twitter:site"/>
<meta content="@aceshowbiz" name="twitter:creator"/>
<meta content="aceshowbiz.com" name="twitter:domain"/>
<meta content="index, follow" name="robots"/>
<link href="https://m.aceshowbiz.com/soundtrack/sorority_row/" media="only screen and (max-width: 600px)" rel="alternate"/>
<link href="https://www.aceshowbiz.com/amp/soundtrack/sorority_row/" rel="amphtml"/>
<link href="/assets/css/normalize.css" rel="stylesheet"/>
<link href="/assets/css/main.css" rel="stylesheet"/>
<link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/assets/css/font-awesome.min.css" rel="stylesheet"/>
<link href="/assets/css/meanmenu.min.css" rel="stylesheet"/>
<link href="/assets/css/style.css?v=1.1" rel="stylesheet"/>
<link href="/assets/css/ie-only.css" rel="stylesheet" type="text/css"/>
<script src="/assets/js/modernizr-2.8.3.min.js"></script>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="black" name="apple-mobile-web-app-status-bar-style"/>
<meta content="#5F256F" name="theme-color"/>
<link href="/manifest.json" rel="manifest"/>
<link href="/assets/img/gif/favicon.gif" rel="shortcut icon" type="image/x-icon"/>
<link href="/assets/img/png/icon192.png" rel="icon" sizes="192x192"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon167.png" rel="apple-touch-icon" sizes="167x167"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon192.png" rel="icon" sizes="192x192"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon128.png" rel="icon" sizes="128x128"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon128.png" rel="icon" sizes="128x128"/>
<link href="//whizzco.com" rel="dns-prefetch"/>
<link href="//facebook.net" rel="dns-prefetch"/>
<link href="//sharethis.com" rel="dns-prefetch"/>
<link href="//mgid.com" rel="dns-prefetch"/>
<link href="//googlesyndication.com" rel="dns-prefetch"/>
<link href="//google-analytics.com" rel="dns-prefetch"/>
<link href="https://certify-js.alexametrics.com" rel="dns-prefetch"/>
<link href="https://tpc.googlesyndication.com" rel="dns-prefetch"/>
<link href="https://pagead2.googlesyndication.com" rel="dns-prefetch"/>
<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-4375969-1', 'aceshowbiz.com');
		  ga('require', 'displayfeatures');
		  ga('send', 'pageview');

		</script>
<script src="/assets/js/modernizr-2.8.3.min.js"></script>
<script src="/assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script async="" src="/assets/js/plugins.js " type="text/javascript"></script>
<script async="" src="/assets/js/bootstrap.min.js " type="text/javascript"></script>
<script src="/assets/js/jquery.meanmenu.min.js " type="text/javascript"></script>
<script src="/assets/js/jquery.scrollUp.min.js " type="text/javascript"></script>
<script async="" src="/assets/js/jquery.magnific-popup.min.js"></script>
<script defer="" src="/assets/js/main.js?v=10" type="text/javascript"></script>
<script src="//platform-api.sharethis.com/js/sharethis.js#property=5989b2b57c544f0011665e85&amp;product=inline-share-buttons" type="text/javascript"></script>
<style>
		/* begin: CSS Accordion Modified */
		label {
		  position: relative;
		  display: block;
		  padding: 0 0 0 1em;
		  background: #ffffff;
		  color: black;
		  font-weight: bold;
		  line-height: 3;
		  cursor: pointer;
		  border-bottom: 1px solid #d8dada;
		}
		
		/* :checked */
		input:checked ~ .tab-content {
		  max-height: 5000em;
		}					

		.profile ul {
			list-style: none;
			margin-left: 30px;
			font-size:14px;
			line-height:2.5;
		}
		.profile li::before {
			content: "\2022"; 
			display: inline-block; width: 1em;
			margin-left: -1em
		}
		/* end: CSS Accordion Modified */
		</style>
<script async="" src="/cdn-cgi/bm/cv/669835187/api.js"></script></head>
<body>
<!--[if lt IE 8]>
    <p class="browserupgrade">You are using an 
        <strong>outdated</strong> browser. Please 
        <a href="https://www.google.com/chrome/">upgrade your browser</a> to improve your experience.
    </p>
    <![endif]-->
<div class="wrapper" id="wrapper">
<header>
<div class="header-style1" id="header-layout1">
<div class="main-menu-area bg-primarytextcolor header-menu-fixed" id="sticker">
<div class="container">
<div class="row no-gutters d-flex align-items-center">
<div class="col-lg-2 d-none d-lg-block">
<div class="logo-area">
<a href="/">
<img alt="AceShowbiz logo" class="/assets/img-fluid" src="/assets/img/logo.png"/>
</a>
</div>
</div>
<div class="col-xl-8 col-lg-7 position-static min-height-none">
<div class="ne-main-menu">
<nav id="dropdown">
<ul>
<li>
<a href="/" title="Home">
<strong>Home</strong>
</a>
</li>
<li>
<a href="/news/" title="News">
<strong>News</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/news/celebrity/" title="Celebrity News">
Celebrity News
</a>
</li>
<li>
<a href="/news/movie/" title="Movie News">
Movie News
</a>
</li>
<li>
<a href="/news/tv/" title="TV News">
TV News
</a>
</li>
<li>
<a href="/news/music/" title="Music News">
Music News
</a>
</li>
</ul>
</li>
<li>
<a href="/celebrity/" title="Celebrity">
<strong>Celebrity</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/celebrity/buzz/" title="The Buzz">
The Buzz
</a>
</li>
<li>
<a href="/celebrity/legends/" title="The Legend">
The Legends
</a>
</li>
<li>
<a href="/celebrity/teenage/" title="Young Celeb">
Young Celeb
</a>
</li>
</ul>
</li>
<li>
<a href="/movie/" title="Movie">
<strong>Movie</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/movie/chart/" title="U.S. Box Office">
U.S. Box Office
</a>
</li>
<li>
<a href="/movie/now_playing/" title="Now Playing">
Now Playing
</a>
</li>
<li>
<a href="/movie/coming_soon/" title="Coming Soon">
Coming Soon
</a>
</li>
<li>
<a href="/movie/trailer/" title="Trailers">
Trailers
</a>
</li>
<li>
<a href="/movie/stills/" title="Pictures">
Pictures
</a>
</li>
<li>
<a href="/movie/review/" title="Reviews">
Reviews
</a>
</li>
<li>
<a href="/movie/soundtrack/" title="Soundtrack">
Soundtrack
</a>
</li>
</ul>
</li>
<li>
<a href="/tv/" title="TV">
<strong>TV</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/tv/trailer/" title="TV Clip / Previews">
TV Clip / Previews
</a>
</li>
<li>
<a href="/tv/dvd/" title="TV on DVD">
TV on DVD
</a>
</li>
<li>
<a href="/tv/soundtrack/" title="Soundtrack">
Soundtrack
</a>
</li>
</ul>
</li>
<li>
<a href="/music/" title="Music">
<strong>Music</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/music/artist/" title="Artist of The Week">
Artist of The Week
</a>
</li>
<li>
<a href="/music/chart/" title="ASB Music Chart">
ASB Music Chart
</a>
</li>
<li>
<a href="/music/new_release/" title="New Release">
New Release
</a>
</li>
<li>
<a href="/music/video/" title="Music Video">
Music Video
</a>
</li>
</ul>
</li>
<li>
<a href="/gallery/" title="Photo">
<strong>Photo</strong>
</a>
</li>
<li>
<a href="/video/" title="Video">
<strong>Video</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/music/video/" title="Music Video">
Music Video
</a>
</li>
<li>
<a href="/movie/trailer/" title="Movie Trailer">
Movie Trailer
</a>
</li>
<li>
<a href="/tv/trailer/" title="TV Clip">
TV Clip
</a>
</li>
</ul>
</li>
<li>
<a href="#" title="Other">
<strong>Other</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/interviews/" title="Interviews">
<strong>Interviews</strong>
</a>
</li>
<li>
<a href="/movie/dvd/" title="DVD">
<strong>DVD</strong>
</a>
</li>
<li>
<a href="/contest/" title="Contest">
<strong>Contest</strong>
</a>
</li>
<li>
<a href="/index_old.php" title="Old Homepage">
<strong>Old Homepage</strong>
</a>
</li>
</ul>
</li>
</ul>
</nav>
</div>
</div>
<div class="col-xl-2 col-lg-3 col-md-12 text-right position-static">
<div class="header-action-item">
<ul>
<li>
<form class="header-search-light" id="top-search-form">
<input class="search-input" placeholder="Search...." required="" style="display: none;" type="text"/>
<button class="search-button">
<i aria-hidden="true" class="fa fa-search"></i>
</button>
  
</form>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</header>
<div class="container"> </div>
<div class="mt-30"> </div>
<style>
			#RightFloatAds
			{
			right: 0px;
			position: fixed;
			top: 84px;
			z-index:2;
			}
		</style>
<section class="breadcrumbs-area" style="background-image: url('/assets/img/banner/breadcrumbs-banner.jpg');">
<div class="container">
<div class="breadcrumbs-content">
<h1>Sorority Row (2009) Soundtrack from the Motion Picture</h1>
<ul>
<li><a href="/">Home</a></li>
- <li><a href="/list/movie/S/">S</a></li>
- <li><a href="/movie/sorority_row/">Main Page</a></li>
- <li><a href="/movie/sorority_row/trailer.html">Trailers</a></li>
- <li><a href="/movie/sorority_row/review.html">Reviews</a></li>
- <li><a href="/movie/sorority_row/news.html">News and Articles</a></li>
- <li><a href="/movie/sorority_row/photo.html">Pictures and Stills</a></li>
- <li><a href="/movie/sorority_row/premiere.html">Premiere Photos</a></li>
- <li><a href="/dvd/sorority_row/">DVD</a></li>
- <li><a href="/soundtrack/sorority_row/">Soundtrack</a></li>
- <li><a href="/movie/sorority_row/wallpaper.html">Wallpapers</a></li>
</ul>
</div>
</div>
</section>
<section class="bg-accent section-space-less30">
<div class="container">
<div class="row">
<div class="col-12">
<div class="ne-banner-layout1 mb-30 text-center">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-5315453046799966" data-ad-slot="3899962133" style="display:inline-block;width:728px;height:90px"></ins>
<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row mb-30-r">
<div class="col-lg-8 col-md-12">
<style>
				.border-bottom:before {
					background-color : #f8f8f8;
				}
			</style>
<section>
<div class="container">
<div class="row">
<div class="col-12">
<div class="topic-border color-cinnabar mb-30 width-100">
<div class="topic-box-lg color-cinnabar">Sorority Row Soundtrack</div>
</div>
</div>
<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
<div class="mb-25 position-relative">
<a class="img-opacity-hover" href="https://www.amazon.com/exec/obidos/ASIN/B002IRDDJI/sportgallery-20">
<img alt="Sorority Row Soundtrack" class="img-fluid mb-15" data-qazy="true" src="https://images-na.ssl-images-amazon.com/images/P/B002IRDDJI.01.MZZZZZZZ.jpg"/>
</a>
<h2 class="title-medium-dark size-md">
<a href="https://www.amazon.com/exec/obidos/ASIN/B002IRDDJI/sportgallery-20">Sorority Row Soundtrack</a>
</h2>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Performer</div>
<div class="profile_item">-</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Label</div>
<div class="profile_item">Koch Records</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Release Date</div>
<div class="profile_item">September 01, 2009</div>
</div>
<div class="mb-30">
<div class="profile_item" style="font-weight:500;">Composer</div>
<div class="profile_item">Lucian Piane</div>
</div>
<table class="table">
<thead>
<tr>
<th>#</th>
<th>Track Listing</th>
</tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td>
Tear Me Up [Explicit]<br/><strong>Stefy Ray</strong> </td>
</tr>
<tr>
<td>2</td>
<td>
Get U Home (paul Oakenfold Remix) [Explicit]<br/><strong><a href="/celebrity/shwayze/" title="Shwayze">Shwayze</a></strong> </td>
</tr>
<tr>
<td>3</td>
<td>
Ghosts [Explicit]<br/><strong>Ladytron</strong> </td>
</tr>
<tr>
<td>4</td>
<td>
I Get Around [Explicit]<br/><strong>Dragonette</strong> </td>
</tr>
<tr>
<td>5</td>
<td>
42 West Avenue [Explicit]<br/><strong>Cahier No 9</strong> </td>
</tr>
<tr>
<td>6</td>
<td>
Get Up [Explicit]<br/><strong>A.D.</strong> </td>
</tr>
<tr>
<td>7</td>
<td>
Alcoholic [Explicit]<br/><strong>Cash Crop</strong> </td>
</tr>
<tr>
<td>8</td>
<td>
Break It Down [Explicit]<br/><strong>Alana D</strong> </td>
</tr>
<tr>
<td>9</td>
<td>
I Lke Dem Girls [Explicit]<br/><strong>Sizzle C</strong> </td>
</tr>
<tr>
<td>10</td>
<td>
This Night [Explicit]<br/><strong>Ron Underwood</strong> </td>
</tr>
<tr>
<td>11</td>
<td>
Say What You Want [Explicit]<br/><strong>The DeeKompressors</strong> </td>
</tr>
<tr>
<td>12</td>
<td>
Tears For Affairs [Explicit]<br/><strong><a href="/movie/camera_obscura/" title="Camera Obscura">Camera Obscura</a></strong> </td>
</tr>
<tr>
<td>13</td>
<td>
Doin' My Thing [Explicit]<br/><strong>King Juju</strong> </td>
</tr>
<tr>
<td>14</td>
<td>
I'm Good, I'm Gone (black Kids Remix) [Explicit]<br/><strong><a href="/celebrity/lykke_li/" title="Lykke Li">Lykke Li</a></strong> </td>
</tr>
<tr>
<td>15</td>
<td>
Emergency [Explicit]<br/><strong>Aimee Allen</strong> </td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</section>
<div class="mb-30">
<section class="bg-body">
<div class="container">
<div class="row">
<div class="col-12">
<div class="ne-banner-layout1 mt-20 text-center">
<div id="ads_container_104124" website_id="104" widget_id="124"></div>
<script type="text/javascript">var uniquekey = "104124"; </script>
<script src="https://cdn.whizzco.com/scripts/widget/widget_t.js" type="text/javascript"></script>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
<div class="ne-sidebar sidebar-break-md col-lg-4 col-md-12">
<div class="sidebar-box">
<div class="topic-border color-scampi mb-5">
<div class="topic-box-lg color-scampi">Most Read</div>
</div>
<div class="row">
<div class="col-12">
<div class="img-overlay-70 img-scale-animate mb-30">
<a href="/news/view/00164950.html" target="_blank"><img alt="Laverne Cox Quits Sex Industry Documentary Amid Backlash" class="img-fluid width-100" data-qazy="true" src="/display/images/300x400/2021/01/10/00164950.jpg"/></a>
<div class="topic-box-top-lg">
<div class="topic-box-sm color-cod-gray mb-20">Movie</div>
</div>
<div class="mask-content-lg">
<div class="post-date-light">
<ul>
<li>
<span>
<i aria-hidden="true" class="fa fa-calendar"></i>
</span></li>
</ul>
</div>
<h2 class="title-medium-light size-lg">
<a href="/news/view/00164950.html" target="_blank">Laverne Cox Quits Sex Industry Documentary Amid Backlash</a>
</h2>
</div>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164995.html" target="_blank">
<img alt="Jack Quaid Defends His Character for Killing Amandla Stenberg's Rue in 'The Hunger Games'" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/11/00164995.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164995.html" target="_blank">Jack Quaid Defends His Character for Killing Amandla Stenberg's Rue in 'The Hunger Games'</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164952.html" target="_blank">
<img alt="'Star Wars: Rogue Squadron' Hires 'Doctor Strange 2' Screenwriter" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/10/00164952.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164952.html" target="_blank">'Star Wars: Rogue Squadron' Hires 'Doctor Strange 2' Screenwriter</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164946.html" target="_blank">
<img alt="Carey Mulligan's 'Promising Young Woman' Wins Big at Columbus Awards" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/10/00164946.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164946.html" target="_blank">Carey Mulligan's 'Promising Young Woman' Wins Big at Columbus Awards</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164972.html" target="_blank">
<img alt="'Home Alone 2' Fans Demand Removal of Trump's Cameo Following Capitol Riot" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/11/00164972.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164972.html" target="_blank">'Home Alone 2' Fans Demand Removal of Trump's Cameo Following Capitol Riot</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164958.html" target="_blank">
<img alt="'Nomadland' Wins Best Picture at National Society of Film Critics Awards" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/11/00164958.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164958.html" target="_blank">'Nomadland' Wins Best Picture at National Society of Film Critics Awards</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165008.html" target="_blank">
<img alt="'Black Panther 2' to Explore Other Characters and Different Subcultures " class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165008.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165008.html" target="_blank">'Black Panther 2' to Explore Other Characters and Different Subcultures </a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165037.html" target="_blank">
<img alt="2021 Gotham Awards Sees 'Nomadland' Taking Top Honor" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165037.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165037.html" target="_blank">2021 Gotham Awards Sees 'Nomadland' Taking Top Honor</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165040.html" target="_blank">
<img alt="Ryan Reynolds Jokes About Manipulating Disney Into Agreeing to 'Deadpool 3' R-Rating" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165040.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165040.html" target="_blank">Ryan Reynolds Jokes About Manipulating Disney Into Agreeing to 'Deadpool 3' R-Rating</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165004.html" target="_blank">
<img alt="'Bridgerton' Star Rege-Jean Page Reacts to James Bond Rumors" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165004.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165004.html" target="_blank">'Bridgerton' Star Rege-Jean Page Reacts to James Bond Rumors</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165054.html" target="_blank">
<img alt="Paul McCartney's Daughter Mary Tapped to Direct Abbey Road Studios Documentary " class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/13/00165054.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165054.html" target="_blank">Paul McCartney's Daughter Mary Tapped to Direct Abbey Road Studios Documentary </a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165028.html" target="_blank">
<img alt="'Morbius' Delayed Again as Theatergoing Continues to Struggle Amid COVID-19 Crisis" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165028.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165028.html" target="_blank">'Morbius' Delayed Again as Theatergoing Continues to Struggle Amid COVID-19 Crisis</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165055.html" target="_blank">
<img alt="Jessica Chastain Ensures Her Co-Stars Share Ownership and Profits of New Movie" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/13/00165055.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165055.html" target="_blank">Jessica Chastain Ensures Her Co-Stars Share Ownership and Profits of New Movie</a>
</h3>
</div>
</div>
</div>
</div>
<div class="sidebar-box">
<div class="ne-banner-layout1 text-center">
</div>
</div>
</div>
</div>
</div>
</section>
<footer>
<div class="footer-area-bottom">
<div class="container">
<div class="row">
<div class="col-12 text-center">
<a class="footer-logo img-fluid" href="/">
<img alt="AceShowbiz logo" class="/assets/img-fluid" src="/assets/img/logo.png"/>
</a>
<h2 class="title-bold-light">
<a href="/site/about.php" title="About">About</a> |
<a href="/site/faq.php" title="Authors">FAQ</a> |
<a href="/site/term.php" title="Term of Use">Term of Use</a> |
<a href="/site/sitemap.php" title="Sitemap">Sitemap</a> |
<a href="/site/contact.php" title="Contact Us">Contact Us</a>
</h2>
<h3 class="title-medium-light size-md mb-10">© 2005-2021 <a href="https://www.aceshowbiz.com" target="_blank" title="AceShowbiz">AceShowbiz</a>. All Rights Reserved.</h3>
</div>
</div>
</div>
</div>
</footer>
</div>
<script>
			if ('serviceWorker' in navigator) {
				navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
					// Registration was successful
					console.log('ServiceWorker registration successful with scope: ', registration.scope);
				}).catch(function(err) {
					// registration failed :(
					console.log('ServiceWorker registration failed: ', err);
				});
			}
			
			self.addEventListener('fetch', function(event) {
			console.log(event.request.url);
			event.respondWith(
			caches.match(event.request).then(function(response) {
			return response || fetch(event.request);
			})
			);
			});			
		</script>
<script type="text/javascript">(function(){window['__CF$cv$params']={r:'610b88c1ae01a2ca',m:'5052d3da6c05f263729d5c1eceb2886aa6456102-1610503338-1800-AQtGlChJzvgmIawlwPrSKdXc+iv6W0TqtVGDz3OOlDPumtWWKAa+xKZliL5Za8Y6osjbOIc5EANNxojNcbXzYpCr6Z0v6nx6hTOREhuqR/c/Kkfv9TE4ZV5/zKxo5su1pIIv0Fg6AgQdPk1CHH41xRA=',s:[0x251630cd65,0x27bfefa233],}})();</script></body>
</html>