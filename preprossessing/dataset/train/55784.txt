<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1"><meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible"/><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><title>
	Australian Accounting Standards Board (AASB) - Home
</title><meta content="" name="AGLS.Function" scheme=""/>
<meta content="" name="DC.Type.aggregationLevel"/>
<meta content="" name="DC.Creator"/>
<meta content="" name="DC.Publisher"/>
<meta content="" name="DC.Rights"/>
<meta content="" name="DC.Title"/>
<meta content="" name="DC.Format"/>
<meta content="" name="DC.Subject"/>
<meta content="" name="DC.Date.modified"/>
<meta content="" name="DC.Type.documentType"/>
<meta content="" name="DC.Description"/>
<meta content="" name="DC.Coverage.spatial"/>
<meta content="" name="DC.Identifier"/>
<link href="css/reset.css" media="all" rel="stylesheet" type="text/css"/><link href="css/global.css" media="all" rel="stylesheet" type="text/css"/><link href="css/home-new.css?v=1" media="all" rel="stylesheet" type="text/css"/><link href="css/large.css" rel="alternate stylesheet" title="large" type="text/css"/><link href="css/print.css" media="print" rel="stylesheet" type="text/css"/>
<!--[if IE 7]>
  <link rel="stylesheet" type="text/css" href="css/home-new-ie.css" media="all" />
  <![endif]-->
<script src="Scripts/jquery.js" type="text/javascript"></script>
<script src="Scripts/textresize.js" type="text/javascript">
</script>
<script src="Scripts/swfobject.js" type="text/javascript">
</script>
<script type="text/javascript">
  <!--
    $(document).ready(function () {
      /*$('#FirstTab').focus();*/


      /*$('#FirstTab').focus();*/

      $('#FirstTab').click(function () {
        $('#ntop').attr("tabindex", -1).css("border", "none").focus();
      });
    });

  -->
  </script>
<script type="text/javascript">
//<![CDATA[
var DID=253039;
var pssl=(window.location.protocol == "https:") ? "https://stats.sa-as.com/lib.js":"http://stats.sa-as.com/lib.js";
document.writeln('<scr'+'ipt async src="'+pssl+'" type="text\/javascript"><\/scr'+'ipt>');
//]]>
</script>
<meta content="The Australian Accounting Standards Board is responsible for developing, issuing and maintaining Australian accounting standards and related pronouncements" name="description"/><meta content="Accounting, standards, interpretations, regulations, board, pronouncements, guidance, concepts, open for comment, decisions, rejections, submissions, publications" name="keywords"/></head>
<body id="Body1">
<form action="./" id="Form1" method="post">
<input id="__VIEWSTATE" name="__VIEWSTATE" type="hidden" value="/wEPDwULLTExODg3Nzc3NDQPZBYEAgEPZBYCAgMPFgIeBFRleHQFnAQ8bWV0YSBuYW1lPSJBR0xTLkZ1bmN0aW9uIiBzY2hlbWU9IiIgY29udGVudD0iIiAvPgo8bWV0YSBuYW1lPSJEQy5UeXBlLmFnZ3JlZ2F0aW9uTGV2ZWwiIGNvbnRlbnQ9IiIgLz4KPG1ldGEgbmFtZT0iREMuQ3JlYXRvciIgY29udGVudD0iIi8+CjxtZXRhIG5hbWU9IkRDLlB1Ymxpc2hlciIgY29udGVudD0iIi8+CjxtZXRhIG5hbWU9IkRDLlJpZ2h0cyIgY29udGVudD0iIi8+CjxtZXRhIG5hbWU9IkRDLlRpdGxlIiBjb250ZW50PSIiLz4KPG1ldGEgbmFtZT0iREMuRm9ybWF0IiBjb250ZW50PSIiLz4KPG1ldGEgbmFtZT0iREMuU3ViamVjdCIgY29udGVudD0iIi8+CjxtZXRhIG5hbWU9IkRDLkRhdGUubW9kaWZpZWQiIGNvbnRlbnQ9IiIvPgo8bWV0YSBuYW1lPSJEQy5UeXBlLmRvY3VtZW50VHlwZSIgY29udGVudD0iIi8+CjxtZXRhIG5hbWU9IkRDLkRlc2NyaXB0aW9uIiBjb250ZW50PSIiLz4KPG1ldGEgbmFtZT0iREMuQ292ZXJhZ2Uuc3BhdGlhbCIgY29udGVudD0iIi8+CjxtZXRhIG5hbWU9IkRDLklkZW50aWZpZXIiIGNvbnRlbnQ9IiIvPgpkAgMPZBYCAgEPZBYCAgQPZBYCAgkPD2QWAh4FdGl0bGUFBlNlYXJjaGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgQFDXByb25vdW5jbWVudHMFDXByb25vdW5jbWVudHMFC3NpdGVjb250ZW50BQtzaXRlY29udGVudIWjRMwuKMdqGrzkgMXRObephQlq5wm3zP3sD2bpAcrz"/>
<input id="__VIEWSTATEGENERATOR" name="__VIEWSTATEGENERATOR" type="hidden" value="8D0E13E6"/>
<input id="__EVENTVALIDATION" name="__EVENTVALIDATION" type="hidden" value="/wEdAAWvRwnh9J4KKY1rWZFWZ4zebCnXAlnqWtXcl/7DZIEvj1SejCWTG4QIUiLnFANFliDhjzuUDIh6uPo1NUJc0c0W+vjkPinmZA1Z6u6ej9r2xMcC0fZLa3FSt1TZyPhBYFP1qFSMMn7oinoVlL8XSbXd"/>
<div id="main_container">
<div id="site_container">
<div id="headcontainer">
<div id="logo">
<a href="Home.aspx"><img alt="Australian Accounting Standards Board" border="0" height="56" src="images/AASB_logo.gif" width="314"/></a> </div>
<div id="top_right_nav">
<p align="right" style="line-height:8px"><a href="#top" id="FirstTab" tabindex="1">Skip To Content</a>  |  <a href="Contact-Us.aspx">Contact Us</a>  |  <a href="About-the-AASB.aspx">About AASB</a>  |  <a href="About-the-AASB/Links.aspx">Links</a> </p>
<p align="right" style="line-height:8px;margin-top:7px">
<a href="https://twitter.com/AASBaustralia" style="padding-left:5px;" target="_blank" title="AASB on Twitter"><img alt="AASB on Twitter" src="/images/twitter-large.png" style=""/></a>
<a href="https://www.linkedin.com/company/aasb" style="padding-left:2px;" target="_blank" title="AASB on LinkedIn"><img alt="AASB on LinkedIn" height="30" src="/images/linkedin-large.png" style="" width="30"/></a>
</p>
<script type="text/javascript">jQuery('#FirstTab').focus();</script>
</div>
</div>
<div id="dropshadow">
<div id="top-nav">
<h2 style="margin:0; font-size: 0em;">Top Navigation</h2>
<ul>
<li><a class="nav-active" href="Home.aspx" id="home"><span>Home</span></a></li>
<li><a href="Pronouncements.aspx" id="current"><span>Pronouncements</span></a></li>
<li><a href="Work-In-Progress.aspx" id="work"><span>Work In Progress</span></a></li>
<li><a href="News.aspx" id="news"><span>News</span></a></li>
<li><a href="Hot-Topics.aspx" id="pub"><span>Hot Topics</span></a></li>
<li><a href="Research-Centre.aspx" id="research-centre"><span>Research Centre</span></a></li>
<li><a href="AASB-Board.aspx" id="board"><span>AASB Board</span></a></li>
</ul>
</div>
<div id="hero_container">
<div id="flashcontent">
<div id="bannerWrapper" style="position: relative;">
<div id="bannerImageWrapper" style="width: auto; position: absolute; top: 0; left: 0;">
<img alt="Australian Accouting Standards Board" id="bannerImage" src="images/aasb_header.jpg"/>
</div>
<div style="height: 235px; width: 345px; float: right; margin-right: 30px;">
<span id="bannerTopText" style="display: inline-block; padding-bottom: 0; color: #306fbd !important; top: 70px">Australian Accounting Standards Board</span><!--<br/>-->
<span id="bannerBottomText" style="color: #306fbd !important; top: 85px;">Developing, issuing and maintaining Australian Accounting Standards and related pronouncements</span>
</div>
</div>
</div>
</div>
<div id="feat-nav">
<h2 style="margin:0; font-size: 0em;">Feat Nav</h2>
<!--********************************************************************************-->
<!--Start Home Page Link-->
<div class="Home_Page_Link"><ul>
<li><a class="feat-left" href="http://www.aasb.gov.au/About-the-AASB/The-standard-setting-process.aspx">
<span>The Standard-Setting Process </span>
</a></li>
<li><a class="feat-center" href="http://www.aasb.gov.au/News/Register-for-AASB-email-updates.aspx">
<span>AASB News and Alerts </span>
</a></li>
<li><a class="feat-center" href="http://www.aasb.gov.au/Work-In-Progress/Open-for-comment.aspx">
<span>Open for Comment </span>
</a></li>
<li><a class="feat-right" href="http://www.aasb.gov.au/News/AASB-upcoming-events.aspx">
<span>AASB Events </span>
</a></li></ul>
</div>
<!--End Home Page Link-->
<div class="clear"></div>
<!--********************************************************************************-->
</div>
</div>
<div id="module_container">
<div id="latestnews_contain">
<div id="latestnews_head"><div><h1>Home</h1><h2 style="padding-left: 0;"><a id="ntop" name="top" style="display: inline-block; width: auto; height: 0;"> </a>- Latest News</h2></div></div>
<div id="latestnews">
<p><a href="https://www.aasb.gov.au/admin/file/content102/c3/RR15_AuditorDisclosureRequirements_12-20.pdf" target="_blank">Research Report 15</a> compares Australian and selected overseas jurisdictions’ auditor remuneration disclosure requirements and identifies factors that could be considered in implementing the Parliamentary Joint Committee on Corporations and Financial Services Inquiry into the Regulation of Auditing recommendation on the audit and non-audit services fee disclosure requirements. <a href="/News/AASB-Research-Report-15-Review-of-Auditor-Remuneration-Disclosure-Requirements-?newsID=386111" title="AASB Research Report 15 Review of Auditor Remuneration Disclosure Requirements ">Read More</a></p>
<p>Our offices will be closed for the holiday period from 24 December and will re-open on Monday 11 January 2021.  We wish you a happy and safe holiday period.</p>
<p>The Australian Accounting Standards Board (AASB) in conjunction with the University of New South Wales, co-hosted the 2020 AASB Virtual Research Forum on Monday, November 30, via Zoom, where academics and financial reporting stakeholders from the public sector, for-profit and not-for-profit sectors came together to discuss the following three research projects. <a href="/News/AASB-Research-Forum-2020-Presentations-Are-Now-Online?newsID=386110" title="AASB Research Forum 2020 Presentations Are Now Online">Read More</a></p>
<p>What is your experience applying the standards for group accounting? Your feedback is requested to inform the <a href="https://www.aasb.gov.au/admin/file/content105/c9/ITC43_12-20.pdf" target="_blank">Post- implementation Review</a>  of IFRS 10 <em>Consolidated Financial Statements</em>, IFRS 11 <em>Joint Arrangements</em> and IFRS 12 <em>Disclosure of Interests in Other Entities</em>. <a href="/News/AASB-Invitation-to-Comment-ITC-43-Request-for-Comment-on-IASB-Request-for-Information-on-Post-implementation-Review-IFRS-10--11-and-12-?newsID=386108" title="AASB Invitation to Comment ITC 43 Request for Comment on IASB Request for Information on Post-implementation Review–IFRS 10, 11 and 12 ">Read More</a></p>
<a href="News.aspx"><img alt="More News" height="27" src="images/more_news.jpg" width="86"/></a>
</div>
</div>
<div id="quicklinks_contain">
<div id="quicklinks_head"><h2>Quick Links</h2>
<div id="quicklinks">
<ul>
<li><a href="http://www.aasb.gov.au/Pronouncements/Current-standards.aspx">Table of Standards</a></li>
<li><a href="http://www.aasb.gov.au/Pronouncements/Interpretations.aspx">Table of Interpretations</a></li>
<li><a href="http://www.aasb.gov.au/Hot-Topics/COVID-19-Guidance.aspx">COVID-19 Guidance</a></li>
<li><a href="http://www.aasb.gov.au/Research-Centre/Research-Forum-2021.aspx">AASB Research Forum 2021 - Expressions of Interest</a></li>
<li><a href="http://www.aasb.gov.au/Hot-Topics.aspx">Hot Topics</a></li>
<li><a href="http://www.aasb.gov.au/Pronouncements/Board-agenda-decisions.aspx">IFRS compilations of latest agenda decisions</a></li>
<li><a href="http://www.aasb.gov.au/AASB-Board/AASB-Board-Strategy-and-Corporate-Plan.aspx">AASB Board Strategy and Corporate Plan</a></li>
<li><a href="http://www.aasb.gov.au/News/AASB-upcoming-events.aspx">Upcoming Events</a></li>
<li><a href="http://www.aasb.gov.au/Work-In-Progress/AASB-Work-Program.aspx">AASB Work Program</a></li>
<li><a href="http://www.aasb.gov.au/About-the-AASB/AASB-Project-Advisory-Panels.aspx">AASB Project Advisory Panels</a></li>
<li><a href="http://www.aasb.gov.au/admin/file/content102/c3/Differential_Reporting_Project_Update_12_01_2015.pdf">Differential Reporting Project Update</a></li>
<li><a href="http://www.aasb.gov.au/Pronouncements/Search-for-a-specific-document.aspx">Search for a specific document</a></li>
<li><a href="http://www.aasb.gov.au/Pronouncements/Search-by-reporting-period.aspx">Search by reporting period</a></li>
<li><a href="http://www.aasb.gov.au/Work-In-Progress/Submissions-from-AASB.aspx">Accounting Submissions made by AASB</a></li>
<li><a href="http://www.aasb.gov.au/FOI/Information-Publication-Scheme.aspx">Information Publication Scheme</a></li>
</ul>
</div>
</div>
</div>
<div id="quicksearch_contain">
<div id="quicksearch_head"><h2>Quick Search</h2></div>
<div id="Panel1">
<div id="quicksearch">
<p>
<label for="txt_quicksearch"><strong>Enter keywords</strong></label></p>
<div id="searchbox">
<input id="txt_quicksearch" name="txt_quicksearch" type="text"/>
</div>
<fieldset>
<legend><span><strong>to search</strong></span></legend>
<p>
<input id="pronouncments" name="searchType" type="radio" value="pronouncments"/><label for="pronouncments">Latest Pronouncements</label>
</p>
<p>
<input id="sitecontent" name="searchType" type="radio" value="sitecontent"/><label for="sitecontent">Site Content</label>
</p>
</fieldset>
<p class="sub">
                   (excluding Pronouncements
                   <br/>
                   &amp; WIP)</p>
<input class="btnQuickSearch" id="btn_quicksearch" name="btn_quicksearch" onclick="return ;" title="Search" type="submit" value="Search"/>
<ul>
<li><a href="Help.aspx">Help with searches?</a></li>
</ul>
</div>
</div>
</div>
<div class="clear"></div>
</div>
</div>
</div>
<div id="foot_container"></div>
<div id="foot_nav"><a href="Help.aspx">Help</a> | <a href="Contact-Us.aspx">Contact Us</a> | <a href="Sitemap.aspx">Sitemap</a> | <a href="Privacy.aspx">Privacy</a> | <a href="Disclaimer.aspx">Disclaimer</a> |  <a href="Accessibility.aspx">Accessibility</a> | <a href="/FOI.aspx">FOI</a> | <a href="Copyright.aspx">Copyright</a> | <a href="About-the-AASB/Links.aspx">Links</a></div>
<div id="icon_nav"><a href="http://www.iconinc.com.au" target="_blank">site by icon.inc</a></div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4617318-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</form>
</body>
</html>
