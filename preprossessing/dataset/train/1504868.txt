<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>// Uni-i Acoustic Industrial Ltd. //</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="css-style.css" rel="stylesheet" title="default" type="text/css"/>
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	background-image: url(images/home/home_bkgd.gif);
	background-repeat: repeat-x;
}
-->
</style></head>
<script language="javascript">
</script>
<body onload="MM_preloadImages('images/home/nav/about_on.png','images/home/nav/products_on.png','images/home/nav/news_on.png','images/home/nav/factory_on.png','images/home/nav/download_on.png','images/home/nav/contact_on.png')">
<center>
<table cellpadding="0" cellspacing="0" height="462" width="965">
<tr height="80"><td colspan="3"> </td></tr>
<tr>
<td><img src="images/home/shadow_left.png"/></td>
<td style="border:solid #FFF  1"><img height="414" src="images/home/main.png" width="939"/><br/>
<a href="about.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','images/home/nav/about_on.png',1)"><img alt="About UNI" border="0" height="48" name="Image4" src="images/home/nav/about.png" width="180"/></a><a href="product-main.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image5','','images/home/nav/products_on.png',1)"><img alt="Products" border="0" height="48" name="Image5" src="images/home/nav/products.png" width="122"/></a><a href="news.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image6','','images/home/nav/news_on.png',1)"><img alt="News &amp; Events" border="0" height="48" name="Image6" src="images/home/nav/news.png" width="178"/></a><a href="factory.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image7','','images/home/nav/factory_on.png',1)"><img alt="Factory Tour" border="0" height="48" name="Image7" src="images/home/nav/factory.png" width="152"/></a><a href="download.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image8','','images/home/nav/download_on.png',1)"><img alt="Download" border="0" height="48" name="Image8" src="images/home/nav/download.png" width="138"/></a><a href="contactus.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image9','','images/home/nav/contact_on.png',1)"><img alt="Contact Us" border="0" height="48" name="Image9" src="images/home/nav/contact.png" width="169"/></a></td>
<td><img src="images/home/shadow_right.png"/></td>
</tr>
<tr><td></td>
<td class="copyright">All Contents ©Copyright Uni-i Acoustic Industrial (HK) Limited. All rights reserved.</td>
<td></td>
</tr>
</table>
</center>
</body>
</html>
