<!DOCTYPE html>
<html class="no-js">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<title>Assamiya Khabor</title>
<meta content="Assamiya Khabor a Assamese Newspaper Published from Assam." name="description"/>
<meta content="width=device-width" name="viewport"/>
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link href="publishfinal/css/normalize.css" rel="stylesheet"/>
<link href="publishfinal/css/main.css" rel="stylesheet"/>
</head>
<body>
<!-- Add your site or application content here -->
<div class="menu">
<div class="menuinner">
<a class="brand">Assamiya Khabor</a>
<a href="publishfinal/contact.html">Contact</a>
<a href="publishfinal/sunday_magazine/index.html">Sunday Magazine</a>
<a href="publishfinal/archive.html">Archive</a>
<a class="active" href="">Home</a>
</div>
</div>
<div class="wrapper">
<a href="publishfinal/asset/guwahati/current/1.html"><img src="publishfinal/img/page1.jpg"/></a>
</div>
<div class="buttonhold">
<a class="button blue big" href="publishfinal/asset/guwahati/current/1.html">Assamiya Khabor Guwahati Edition</a>
<a class="button green" href="publishfinal/asset/jorhat/current/1.html">Assamiya Khabor Jorhat Edition</a>
</div>
<div class="footer">
<p class="footert">On behalf of Sailendra Kownar Dutta, Managing Director, Frontier Publications Pvt. Ltd. </p>
<p class="footert">Printed and Published by Smritalina Das -  Director</p>
<p class="boldt"> Biswajit Das - Executive Editor</p>
<p>© 2017. Assamiya Khabor All Right Reserved </p>
<p><a href="http://www.quick-counter.net/" title="HTML hit counter - Quick-counter.net"><img alt="HTML hit counter - 
Quick-counter.net" border="0" height="55" src="http://www.quick-counter.net/
aip.php?tp=bt&amp;tz=Asia%2FCalcutta" width="218"/></a></p>
</div>
<!-- Add your site or application content here -->
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36399996-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
