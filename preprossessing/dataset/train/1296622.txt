<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- Search Tags -->
<title>Binder | Admin Panel</title>
<link href="css/style.css?1610474005" rel="stylesheet" type="text/css"/>
<link href="rs-plugin/css/style.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="rs-plugin/css/settings.css" media="screen" rel="stylesheet" type="text/css"/>
<link crossorigin="anonymous" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" rel="stylesheet"/>
<link href="fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="css/jquery.timepicker.css" rel="stylesheet" type="text/css"/>
<link href="img/logo.png" rel="shortcut icon"/>
<script src="js/jquery-1.12.4.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/jquery.inputmask.bundle.js"></script>
<script src="js/inputmask.numeric.extensions.js"></script>
<script src="js/jquery.timepicker.js"></script>
<script src="js/pagination.js"></script>
<script src="js/custom.js?1610474005"></script>
<script>
	   $(window).load(function() {
	     $('#status').fadeOut();
	     $('#preloader').delay(350).fadeOut('slow');
	     $('body').delay(350).css({'overflow':'visible'});
	   })
	</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-126191217-1"></script>
<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-126191217-1');
	</script>
</head>
<body class="resetPassword">
<div align="center" id="preloader">
<div id="loading">
<img alt="Loading.." height="140" src="img/loader.gif"/>
</div>
</div>
<div class="section mini dashboardscreen1">
<div class="wdth section mini" style="text-align:center;">
<div class="col30 marginauto section mini " style="border: solid 1px #e7e7e7;padding: 30px 30px;background: white;box-shadow: 0px 0px 20px rgba(0,0,0,0.2);border-radius: 8px;">
<div class="logo">
<p><img alt="logo" src="img/logo.png" style="height: 100px;"/></p>
</div>
<h1 style="font-weight: 400; margin-bottom: 0px;">Welcome to Binder</h1>
<h4 style="font-weight: 400;">Login in. To see it in action.</h4>
<br/>
<div class="form">
<div class="col100">
<div class="form">
<form action="?login=ok" id="resetpass" method="post" novalidate="novalidate">
<p><input id="email" name="email" placeholder="Email" type="text" value="admin@admin.com"/></p>
<p><input id="pass" name="pass" placeholder="Password" type="password" value="123456"/></p>
<p><input style="background:#fb3c73; color:white; border:none; cursor:pointer;" type="submit" value="Login"/></p>
</form>
</div>
</div>
<div class="clear"></div>
</div>
</div>
</div>
</div>
<style>
  body{
    margin:0;
    padding:0;
    font-family: "Roboto","Helvetica Neue",Helvetica,Arial,sans-serif;
      font-size: 12px;
    font-weight: 300;
    background: #ff9966;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to right, #ff5e62, #ff9966);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to right, #ff5e62, #ff9966); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

  }
</style>
<!-- Modal 7 (Ajax Modal)-->
<div class="PopupParent" id="PopupParent" style="background: rgba(0, 0, 0, 0.3); display: none;  position:  fixed; top: 0;width:  100%;height:  100%;z-index:  99999;">
<div class="wdth" style="width:60%; margin-top:60px;background:  white;padding: 20px 20px; height: 550px;  border-radius:  5px; text-align:center;">
<button onclick="ClosePopup();" style="position: absolute; margin-top: -37px; margin-left: 370px; background: #ff9966; background: -webkit-linear-gradient(to right, #ff5e62, #ff9966); background: linear-gradient(to right, #ff5e62, #ff9966);  color:white; border: 0;border-radius: 50%;font-size: 21px;width: 30px;height: 30px; z-index: 99999;" type="button">×</button>
<div class="modal-content" style="">
<div class="modal-body" id="contentReceived">
                Loading... 
            </div>
</div>
</div>
</div>
<script>
    	    function ClosePopup()
    		{
    
    			document.getElementById("PopupParent").style.display="none";
    
    		} 
    		
    		
            
            
    </script>
</body>
</html>
