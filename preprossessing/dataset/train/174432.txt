<!DOCTYPE html>
<!--[if lte IE 7]>     <html class="no-js ie-oldie" lang="de" 
  xmlns:og="http://ogp.me/ns#"
  xmlns:article="http://ogp.me/ns/article#"
  xmlns:book="http://ogp.me/ns/book#"
  xmlns:profile="http://ogp.me/ns/profile#"
  xmlns:video="http://ogp.me/ns/video#"
  xmlns:product="http://ogp.me/ns/product#"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#"  lang="de" dir="ltr"> <![endif]--><!--[if IE 8]>         <html class="no-js ie8" lang="de" 
  xmlns:og="http://ogp.me/ns#"
  xmlns:article="http://ogp.me/ns/article#"
  xmlns:book="http://ogp.me/ns/book#"
  xmlns:profile="http://ogp.me/ns/profile#"
  xmlns:video="http://ogp.me/ns/video#"
  xmlns:product="http://ogp.me/ns/product#"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#"  lang="de" dir="ltr"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" dir="ltr" lang="de" xmlns:article="http://ogp.me/ns/article#" xmlns:book="http://ogp.me/ns/book#" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/terms/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:product="http://ogp.me/ns/product#" xmlns:profile="http://ogp.me/ns/profile#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:video="http://ogp.me/ns/video#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#"> <!--<![endif]-->
<head>
<script type="text/javascript">
      var gaProperty = 'UA-65709911-6';
      var disableStr = 'ga-disable-' + gaProperty;
      if (document.cookie.indexOf(disableStr + '=true') > - 1) {
        window[disableStr] = true;
      }
      function gaOptout() {
        document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
        window[disableStr] = true;
        alert('Das Tracking durch Google Analytics wurde in Ihrem Browser für diese Website deaktiviert.');
      }
    </script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-65709911-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag() {
  dataLayer.push(arguments);
  }
  gtag('js', new Date());
  gtag('config', 'UA-65709911-6', { 'anonymize_ip': true });
    </script>
<title>Autorenwelt</title>
<!--[if IE]><![endif]-->
<link href="//ajax.googleapis.com" rel="preconnect"/>
<link href="//ajax.googleapis.com" rel="dns-prefetch"/>
<link href="//languages" rel="preconnect"/>
<link href="//languages" rel="dns-prefetch"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="https://www.autorenwelt.de/system/files/autorenwelt_bm_rgb_favicon_32px.png" rel="shortcut icon"/>
<meta content="Die ganze Welt der Autorinnen und Autoren: Informationen, Netzwerke, Community. Jetzt anmelden und Teil der Autorenwelt werden!" name="description"/>
<meta content="Drupal 7 (http://drupal.org)" name="generator"/>
<link href="https://www.autorenwelt.de/" rel="canonical"/>
<link href="https://www.autorenwelt.de/" rel="shortlink"/>
<meta content="Autorenwelt" property="og:site_name"/>
<meta content="website" property="og:type"/>
<meta content="https://www.autorenwelt.de/" property="og:url"/>
<meta content="Autorenwelt" property="og:title"/>
<meta content="Die ganze Welt der Autorinnen und Autoren: Informationen, Netzwerke, Community. Jetzt anmelden und Teil der Autorenwelt werden!" property="og:description"/>
<meta content="https://www.autorenwelt.de/sites/all/themes/custom/og-image.jpg" property="og:image"/>
<link href="https://www.autorenwelt.de/sites/all/themes/custom/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/><link href="https://www.autorenwelt.de/sites/all/themes/custom/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="https://www.autorenwelt.de/sites/all/themes/custom/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="https://www.autorenwelt.de/sites/all/themes/custom/apple-touch-icon.png" rel="apple-touch-icon"/>
<link href="https://www.autorenwelt.de/sites/all/themes/custom/apple-startup.png" rel="apple-touch-startup-image"/>
<meta content="width" name="MobileOptimized"/>
<meta content="true" name="HandheldFriendly"/> <meta content="width=device-width, initial-scale=1" name="viewport"/> <meta content="width=device-width, initial-scale=1, maximum-scale=1.0" name="viewport"/> <meta content="siw6REuUSmzC1pPdrUrnd4danOCKQ0BKScnUYKt9sNFGFzRhoTaqKdZJqq8uIF6b" name="siwecos-site-verification"/>
<meta content="on" http-equiv="cleartype"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<link href="/sites/default/files/advagg_css/css__IIpOi6q3pSzEA8URiOmXZ6Hh8PfOQU79P7DyATRkMu8__gl_ZUr6-Awe1iH_SIpuJPc6QRxAkGZy-VjrXRb2NVzw__FN_EAM9X694iQ4KmM-4TfP_6N9-ohC1JR3TCWcBvGFM.css" rel="stylesheet"/>
<style>#sliding-popup.sliding-popup-bottom,#sliding-popup.sliding-popup-bottom .eu-cookie-withdraw-banner,.eu-cookie-withdraw-tab{background:#870067;}#sliding-popup.sliding-popup-bottom.eu-cookie-withdraw-wrapper{background:transparent}#sliding-popup .popup-content #popup-text h1,#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text h3,#sliding-popup .popup-content #popup-text p,#sliding-popup label,#sliding-popup div,.eu-cookie-compliance-secondary-button,.eu-cookie-withdraw-tab{color:#ffffff !important;}.eu-cookie-withdraw-tab{border-color:#ffffff;}.eu-cookie-compliance-more-button{color:#ffffff !important;}
</style>
<link href="/sites/default/files/advagg_css/css__b69BUdYJNaYtK68POq5ILJCVDes78DlDIBUuZbBMorc__XG0RxjZnhsyXEnrlYDxjktkWXT3Nr3oUxBaTV2s5gvg__FN_EAM9X694iQ4KmM-4TfP_6N9-ohC1JR3TCWcBvGFM.css" rel="stylesheet"/>
<script src="//use.typekit.com/bnd2xda.js" type="text/javascript"></script>
<script type="text/javascript">try {
  Typekit.load();
  }
  catch (e) {
  }</script>
<!--[if lt IE 9]>
        <script src="https://www.autorenwelt.de/sites/all/themes/mothership/mothership/js/respond.min.js"></script>
      <![endif]-->
<!--[if lt IE 9]>
      <script src="https://www.autorenwelt.de/sites/all/themes/mothership/mothership/js/html5.js"></script>
    <![endif]-->
</head>
<body class="front no-sidebars page-start path-start not-logged-in">
<a class="element-invisible element-focusable" href="#main-content">Direkt zum Inhalt</a>
<header role="banner">
<div class="header-region">
<figure class="logo">
<a href="/" rel="home" title="Startseite">
<img alt="Startseite" src="https://www.autorenwelt.de/sites/all/themes/custom/logo.png"/>
</a>
</figure>
<div class="pushmenu">
<a class="pushmenu-toggler" data-icon="" href="#"></a>
<nav role="navigation">
<ul><li><a class="shop-link" href="https://shop.autorenwelt.de/">Zum Shop</a></li>
<li class="active"><a class="active" href="/">Home</a></li>
<li><a class="theme_aktuelles" href="/blog">Impulse</a></li>
<li class="collapsed"><a class="theme_literatur" href="/verzeichnis/menschen">Literaturbetrieb</a></li>
<li class="expanded"><a class="theme_magazin" href="/magazin/federwelt/aktuelles-heft">Magazine</a><ul><li class="expanded"><a href="/magazin/federwelt/aktuelles-heft">Federwelt</a><ul><li><a href="/magazin/federwelt/aktuelles-heft">Aktuelles Heft</a></li>
<li><a href="/magazin/federwelt/archiv">Archiv</a></li>
<li><a href="/team">Team</a></li>
<li><a href="/magazine/federwelt/inserieren-der-federwelt">Inserieren</a></li>
</ul></li>
<li class="expanded"><a href="/magazin/derselfpublisher/aktuelles-heft">der selfpublisher</a><ul><li><a href="/magazin/derselfpublisher/aktuelles-heft">Aktuelles Heft</a></li>
<li><a href="/magazin/derselfpublisher/archiv">Archiv</a></li>
<li><a href="/magazine/der-selfpublisher/team-der-selfpublisher">Team</a></li>
<li><a href="/magazine/der-selfpublisher/inserieren-im-magazin-der-selfpublisher">Inserieren</a></li>
</ul></li>
<li><a href="/magazine/magazine-bestellen">Bestellen</a></li>
</ul></li>
<li class="collapsed"><a href="/forum">Forum</a></li>
</ul>
</nav>
</div>
<div class="loginLinks">
<a href="/person/register">Registrierung</a><a href="/user/login">Login</a> </div>
<div id="flap">
<a class="flap-toggler" data-icon="" href="#"></a>
<div class="flap anonymous ">
<div class="boxes-box" id="boxes-box-box_profile_anonymous"><div class="boxes-box-content"><a href="https://www.autorenwelt.de/person/register">Registrieren</a><a href="https://www.autorenwelt.de/user/login">Anmelden</a><a href="https://www.autorenwelt.de/user/password">Passwort vergessen</a></div></div>
</div>
</div>
</div>
</header>
<div id="content">
<div class="page">
<div id="main-content" role="main">
<noscript>
<div class="messages info">
<strong>
          Bitte aktivieren Sie JavaScript! Ansonsten können Sie die Seiten nur
          eingeschränkt nutzen.
        </strong><br/><br/>
        Vielen Dank.
      </div>
</noscript>
<div class="panel-page">
<header role="banner">
<div class="top-default">
<div id="start-intros">
<ul class="intros"> <li class="">
<img alt="Foto, das Dirk Eickenhorst zeigt" height="480" src="https://www.autorenwelt.de/system/files/intro-aw-eickenhorst_dirk.jpg" typeof="foaf:Image" width="1280"/>
<div class="copy left grey ">
<h5 class="sheadline">»Die Autorenwelt ...</h5>
<div class="text">»... verpasst mir den Motivationsschub, den ich als Geschichtenerfinder brauche.«</div>
<div class="signature">Dirk Eickenhorst</div>
</div> </li>
<li class="">
<img alt="Foto, das Dorit Kostall zeigt." height="480" src="https://www.autorenwelt.de/system/files/intro-aw-kostall-dorit.jpg" typeof="foaf:Image" width="1280"/>
<div class="copy left grey ">
<h5 class="sheadline">»Die Autorenwelt ist ...</h5>
<div class="text">... mein Heimathafen. Hierher kehre ich nach meinen Schreibabenteuern zurück, um Neuigkeiten und Erfahrungen auszutauschen.«</div>
<div class="signature">Dorit Kostall</div>
</div> </li>
<li class="">
<img alt="neues Bild von Jasmin" height="480" src="https://www.autorenwelt.de/system/files/intro-aw-zipperling_jasmin_neu.jpg" typeof="foaf:Image" width="1280"/>
<div class="copy right grey ">
<h5 class="sheadline">»Durch die Autorenwelt kann ich mich ...</h5>
<div class="text">... viel besser vernetzen. Nirgendwo sonst kann ich mit einem Klick Personen und Organisationen aus der Welt der Schreibwütigen nach Orten und Genre filtern.«
</div>
<div class="signature">Jasmin Zipperling</div>
</div> </li>
</ul>
</div>
</div>
<div class="top-mobile"></div>
</header>
<div id="panel-content" role="main">
<div class="boxes-box" id="boxes-box-box_benutzer_registrierung"><div class="boxes-box-content"><section class="email-login">
<h2>Mitglied werden</h2>
<p>Sehen und gesehen werden: Registrieren Sie sich kostenlos und erstellen Sie Ihr Profil.</p>
</section>
<div>
<a class="btn btn-icon btn-icon-email" href="person/register" title="Mit E-Mail registrieren">Mit E-Mail registrieren</a>
</div>
<section class="user-login">
<h2>Ich bin bereits Mitglied!</h2>
<p>
<a href="user/password" title="Password vergessen?">Password vergessen?</a>
</p>
</section>
</div></div>
<form accept-charset="UTF-8" action="/start?destination=start" id="user-login-form" method="post"><div class="form-type-textfield form-required">
<label class="required" for="edit-name">Benutzername</label>
<input id="edit-name" maxlength="60" name="name" placeholder="Benutzername" required="" size="30" type="text" value=""/>
</div>
<div class="form-type-password form-required">
<label class="required" for="edit-pass">Passwort</label>
<input id="edit-pass" maxlength="128" name="pass" placeholder="Passwort" size="30" type="password"/>
</div>
<ul><li class="first"><a href="/user/register" title="Ein neues Benutzerkonto erstellen.">Neues Benutzerkonto erstellen</a></li>
<li class="last"><a href="/user/password" title="Ein neues Passwort per E-Mail anfordern.">Neues Passwort anfordern</a></li>
</ul><input name="form_build_id" type="hidden" value="form-SdiBEVZzILryPcZIr5zt6yL9lmk3ZMvtIU0_ad47I4Q"/>
<input name="form_id" type="hidden" value="user_login_block"/>
<div class="form-actions form-wrapper" id="edit-actions"><input id="edit-submit" name="op" type="submit" value="Anmelden"/></div></form>
</div>
<footer role="contentinfo">
<div class="panel-pane pane-views pane-werbebanner">
<div class="advertising">
<div>
<div about="/das-autorenhandbuch" class="ds-1col node-ct-advertising view-mode-full clearfix" typeof="sioc:Item foaf:Document">
<img alt="" height="150" src="https://www.autorenwelt.de/sites/default/files/advertising/hb8-aw-banner_0.gif" typeof="foaf:Image" width="1180"/><a href="http://www.uschtrin.de/produkte/weiteres/handbuch" target="_blank">http://www.uschtrin.de/produkte/weiteres/handbuch</a></div>
</div>
</div>
</div>
<div class="panel-pane pane-views pane-blogs home-block">
<h2>Impulse</h2>
<div class="blog homepage-blog blog-teaser-content clearfix">
<div class="grid-col grid-xl-4 grid-s-8 teaser-blog">
<div class="teaser-blog-wrap">
<figure><a href="/blog/branchen-news/wichtige-termine-bei-der-vg-wort-sowie-zinslose-darlehen-ueber-den-sozialfonds"><img alt="Meldetermine bei der VG WORT" height="480" src="https://www.autorenwelt.de/system/files/styles/bs_thumbnail-blog/private/bn_vg-wort-termine2.jpg?itok=Jmifq5lu" typeof="foaf:Image" width="720"/></a></figure>
<div class="teaser-blog-cont">
<p class="teaser-blog-signature is-bold"><span><time content="2020-12-27T00:00:00+01:00" datatype="xsd:dateTime" datetime="2020-12-27T00:00:00+01:00" property="dc:date">27.12.2020</time></span> – 
  <span><span class="teaser-blog-autor"><a href="/person/sandra-uschtrin">Sandra</a></span>
</span><span><span class="teaser-blog-autor"><a href="/person/sandra-uschtrin">Uschtrin</a></span>
</span>
</p>
<a href="/blog/branchen-news/wichtige-termine-bei-der-vg-wort-sowie-zinslose-darlehen-ueber-den-sozialfonds"><h5>Wichtige Termine bei der VG WORT sowie zinslose Darlehen über den Sozialfonds</h5></a>
<span class="teaser-blog-summary">31. Dezember 2020 und 31. Januar 2021: Nur wer einen Wahrnehmungsvertrag abgeschlossen hat, kann seine Werke bei der VG WORT melden. Und nur wer seine Werke meldet, bekommt Geld.</span><br/><a href="/blog/branchen-news/wichtige-termine-bei-der-vg-wort-sowie-zinslose-darlehen-ueber-den-sozialfonds"><span class="more-link"> Mehr dazu...</span></a>
</div>
</div> </div>
<div class="grid-col grid-xl-4 grid-s-8 teaser-blog">
<div class="teaser-blog-wrap">
<figure><a href="/blog/branchen-news/neuer-vorstand-beim-selfpublisher-verband-0"><img alt="Jeanette Lagall und Katharina Lankers - neuer Vorstand beim Selfpublisher-Verband " height="480" src="https://www.autorenwelt.de/system/files/styles/bs_thumbnail-blog/private/bn_sp-verband_neuer-vorstand2020.jpg?itok=-DazGY4f" typeof="foaf:Image" width="720"/></a></figure>
<div class="teaser-blog-cont">
<p class="teaser-blog-signature is-bold"><span><time content="2020-12-27T00:00:00+01:00" datatype="xsd:dateTime" datetime="2020-12-27T00:00:00+01:00" property="dc:date">27.12.2020</time></span> – 
  <span><span class="teaser-blog-autor"><a href="/person/sandra-uschtrin">Sandra</a></span>
</span><span><span class="teaser-blog-autor"><a href="/person/sandra-uschtrin">Uschtrin</a></span>
</span>
</p>
<a href="/blog/branchen-news/neuer-vorstand-beim-selfpublisher-verband-0"><h5>Neuer Vorstand beim Selfpublisher-Verband</h5></a>
<span class="teaser-blog-summary">Der Selfpublisher-Verband e.V. hat in seiner Mitgliederversammlung am 5. Dezember 2020 turnusgemäß einen neuen Vorstand gewählt. Neue 1. Vorsitzende ist Jeanette Lagall. Sie löst</span><br/><a href="/blog/branchen-news/neuer-vorstand-beim-selfpublisher-verband-0"><span class="more-link"> Mehr dazu...</span></a>
</div>
</div> </div>
<div class="grid-col grid-xl-4 grid-s-8 teaser-blog">
<div class="teaser-blog-wrap">
<figure><a href="/blog/branchen-news/leitfaden-fuer-die-organisation-von-digitalen-lesungen-und-werkstaetten"><img alt=" Leitfaden des A*dS und Bibliosuisse für die Organisation von digitalen Lesungen und Werkstätten" height="480" src="https://www.autorenwelt.de/system/files/styles/bs_thumbnail-blog/private/bn_ads-leitfaden.jpg?itok=i9gT4ivl" typeof="foaf:Image" width="720"/></a></figure>
<div class="teaser-blog-cont">
<p class="teaser-blog-signature is-bold"><span><time content="2020-12-27T00:00:00+01:00" datatype="xsd:dateTime" datetime="2020-12-27T00:00:00+01:00" property="dc:date">27.12.2020</time></span> – 
  <span><span class="teaser-blog-autor"><a href="/person/sandra-uschtrin">Sandra</a></span>
</span><span><span class="teaser-blog-autor"><a href="/person/sandra-uschtrin">Uschtrin</a></span>
</span>
</p>
<a href="/blog/branchen-news/leitfaden-fuer-die-organisation-von-digitalen-lesungen-und-werkstaetten"><h5> Leitfaden für die Organisation von digitalen Lesungen und Werkstätten</h5></a>
<span class="teaser-blog-summary">Der A*dS Autorinnen und Autoren der Schweiz und Bibliosuisse haben einen kurzen Leitfaden für digitale Veranstaltungen wie Lesungen und Schreibwerkstätten erarbeitet. Er ist für</span><br/><a href="/blog/branchen-news/leitfaden-fuer-die-organisation-von-digitalen-lesungen-und-werkstaetten"><span class="more-link"> Mehr dazu...</span></a>
</div>
</div> </div>
</div>
</div>
<div class="home-block" id="advantages">
<h2 class="pane-title">Unsere Vorteile</h2>
<!-- html5 1 col stacked -->
<article class="node-page view-mode-full" role="article">
<div>
<div class="field field-name-field-sp-3column-text"><!-- html5 1 col stacked -->
<article class="entity entity-field-collection-item field-collection-item-field-sp-3column-text view-mode-full" role="article">
<div>
<span class="icons set" data-icon=""></span><h4 class="field field-type-text"><span>Wissen</span></h4><p>Hier finde ich alles, was ich wissen muss, um als AutorIn erfolgreich zu sein.</p>
</div>
</article>
<!-- html5 1 col stacked -->
<article class="entity entity-field-collection-item field-collection-item-field-sp-3column-text view-mode-full" role="article">
<div>
<span class="icons set" data-icon=""></span><h4 class="field field-type-text"><span>Kontakte</span></h4><p>Netzwerken und neue Kontakte knüpfen – das A und O für alle, die täglich mit Buchstaben jonglieren.</p>
</div>
</article>
<!-- html5 1 col stacked -->
<article class="entity entity-field-collection-item field-collection-item-field-sp-3column-text view-mode-full" role="article">
<div>
<span class="icons set" data-icon=""></span><h4 class="field field-type-text"><span>Gemeinschaft</span></h4><p>Mich mit anderen für ein faires Miteinander im Literaturbetrieb einsetzen, diese Community macht’s möglich!</p>
</div>
</article>
</div> </div>
</article>
</div>
<div class="home-block" id="mission">
<h2 class="pane-title">Unsere Mission</h2>
<!-- html5 1 col stacked -->
<article class="node-page view-mode-full" role="article">
<div>
<h1>Hier entsteht das Zentrum des Literaturbetriebs – in den Händen der AutorInnen!</h1><div class="field field-name-field-sp-3column-text"><!-- html5 1 col stacked -->
<article class="entity entity-field-collection-item field-collection-item-field-sp-3column-text view-mode-full" role="article">
<div>
<h6>Ist-Zustand</h6><p>Viele AutorInnen, die keine Bestseller schreiben, sondern einfach nur gute Bücher, sind unzufrieden mit den Leistungen ihrer Verlage. Marketing? Möglichst täglich etwas auf Facebook zu posten und zu twittern gehört – neben Lesungen, der eigenen Homepage oder dem Verteilen von Werbeflyern – längst zu den Pflichten des modernen Autors. Wer das nicht tut, hat bei großen Publikumsverlagen kaum noch eine Chance. Nur regelmäßig für neuen Stoff zu sorgen, der sich hinlänglich gut verkauft, reicht schon lange nicht mehr aus. Und bei kleinen Verlagen sind die Verdienstmöglichkeiten für AutorInnen in der Regel schlecht. Da kann man sein Glück lieber gleich als Selfpublisher versuchen, sich die Leistungen, die man benötigt, hinzukaufen und seine Texte über Amazon publizieren. Nur dass Amazon eben auf Dauer keine Alternative ist.</p>
</div>
</article>
<!-- html5 1 col stacked -->
<article class="entity entity-field-collection-item field-collection-item-field-sp-3column-text view-mode-full" role="article">
<div>
<h6>Ziele</h6><p>Entstehen soll eine Plattform von und für AutorInnen, die ihnen in naher Zukunft selbst gehört. Auf ihr soll sämtliches Wissen und sollen sämtliche Dienstleistungen vermittelt und bereitgestellt werden, die AutorInnen brauchen, um ihre Texte zu veröffentlichen. Die Plattform soll das Zuhause der deutschsprachigen AutorInnen im Internet sein und den Prozess ihrer Emanzipation gegenüber den Verlagen und dem Online-Händler Amazon maßgeblich vorantreiben. Gemeinsam mit freien Lektorinnen, fair arbeitenden Dienstleistern und TestleserInnen sollen AutorInnen an ihren Texten und deren Präsentation auf dieser Plattform so lange feilen können, bis ihre Texte »reif« sind. Über einen eigenen Shop, der zur Plattform gehört, werden diese Texte an die Leserinnen und Leser verkauft. Der Shop ist wirtschaftlich das Herz der Plattform.</p>
</div>
</article>
<!-- html5 1 col stacked -->
<article class="entity entity-field-collection-item field-collection-item-field-sp-3column-text view-mode-full" role="article">
<div>
<h6>3 Phasen</h6><p>In Phase 1 - in der wir uns jetzt befinden - bilden wir den Literaturbetrieb über ein Content-Management-System neu ab. Veranstalter von Wettbewerben und Seminaren pflegen ihre Ausschreibungen bzw. Angebote selbst in das System ein. AutorInnen und alle Personen, die mit Büchern, E-Books, Schreiben und Veröffentlichen zu tun haben, tragen sich mit ihrem Profil in das System ein, zeigen, wer sie sind und was sie machen. Dasselbe gilt für literarische Einrichtungen aller Art. Phase 2 hat das Ziel, weitere Leistungen über Module anzubieten. Denkbar sind die Module »Blog«, »Forum«, »Wissen« oder »Lektorate«. Phase 3 betrifft das Veröffentlichen selbst. Das »Publishing-Modul« wird eine Mischung aus Shop und E-Book-Datenbank sein. Über dieses Modul können AutorInnen ihre Texte direkt an ihre Leserinnen und Leser verkaufen.</p>
</div>
</article>
</div> </div>
</article>
</div>
</footer>
</div>
<div class="breadcrumb-footer">
</div>
</div><!-- /main-->
</div><!-- /page-->
<footer role="contentinfo">
<div id="foot-wrapper">
<nav role="navigation">
<ul><li><a href="https://autorenprogramm.autorenwelt.de" target="_blank">Autorenprogramm</a></li>
<li><a href="/materialien-lesermarketing">Banner&amp;Flyer</a></li>
<li><a href="/kontakt">Kontakt</a></li>
<li class="collapsed"><a href="/newsletter">Newsletter</a></li>
<li><a href="/impressum">Impressum</a></li>
<li><a href="/agb">AGB</a></li>
<li><a href="/datenschutz">Datenschutz</a></li>
<li><a href="/mediadaten">Mediadaten</a></li>
<li><a href="/presse-autorenwelt" title="Presse">Presse</a></li>
<li><a href="/faq">FAQ</a></li>
</ul>
</nav>
<div>
<div class="boxes-box" id="boxes-box-box_socials"><div class="boxes-box-content"><div class="social">
<bold>Mit uns im Netz verbunden bleiben </bold>
<a data-icon="" href="https://www.facebook.com/pages/Autorenwelt/596803893727481" id="fb" target="_blank">Facebook</a>
<a data-icon="" href="https://twitter.com/autorenwelt" id="tw" target="_blank">Twitter</a>
​</div></div></div>
</div>
</div>
</footer>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write("<script src='/sites/all/modules/jquery_update/replace/jquery/1.8/jquery.min.js'>\x3C/script>")</script>
<script src="/sites/default/files/advagg_js/js__ZyeOaiFuDejQQbhUV7yg7atYZnj4WLfH77o0scv4068__MZdWWgUEYpsEWLcU0RqkaXMsEyksbpCgnf4XwXRkqz0__FN_EAM9X694iQ4KmM-4TfP_6N9-ohC1JR3TCWcBvGFM.js"></script>
<script src="/sites/default/files/advagg_js/js__1EQrqF0xK1oOdXmfVgbw9rBqbQVr6ZNR7LIb6JDEoLc__NcbPgl6yce1cPkJcub1SVldKst10pVo0nN36XtkHZn8__FN_EAM9X694iQ4KmM-4TfP_6N9-ohC1JR3TCWcBvGFM.js"></script>
<script src="/sites/default/files/advagg_js/js__2rIb5nyVelFOSf0x-E76VRL7ohe722W7nF0IkZs9OeA__t_EiiBSaCRpEXzpNKNwrVOpj9VE9o31qyq-0TjCgVYg__FN_EAM9X694iQ4KmM-4TfP_6N9-ohC1JR3TCWcBvGFM.js"></script>
<script src="/sites/default/files/advagg_js/js__POMprf-eESqvKF1OXCB8XLM9I0Wo3IistsSwwGtxZxA__P0zbsI5Zsl8s6nPHnyRJhbwN5ysN5obGPChZfMk_qGY__FN_EAM9X694iQ4KmM-4TfP_6N9-ohC1JR3TCWcBvGFM.js"></script>
<script>window.CKEDITOR_BASEPATH = '/sites/all/libraries/ckeditor/'</script>
<script src="/sites/default/files/advagg_js/js__Ha_WvclS7egS-ejz1xG7w8zN0nmWNuKQDd4haBx7PzI__sEyHUijKOF8vM9CacE5x4QaMsanPcLszA4bIYVrSmaQ__FN_EAM9X694iQ4KmM-4TfP_6N9-ohC1JR3TCWcBvGFM.js"></script>
<script>jQuery.extend(Drupal.settings,{"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"custom","theme_token":"crZAgZ_tzNPlosL3OcBProZeBWZ0t7OU6f5kMRG2GkY","css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"modules\/forum\/forum.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"sites\/all\/modules\/pollfield\/pollfield.css":1,"sites\/all\/modules\/quote\/quote.css":1,"sites\/all\/modules\/taxonomy_access\/taxonomy_access.css":1,"sites\/all\/themes\/custom\/templates\/panels\/mothership-html5page\/mothership-html5page.admin.css":1,"sites\/all\/modules\/contrib\/eu_cookie_compliance\/css\/eu_cookie_compliance.css":1,"0":1,"sites\/all\/themes\/custom\/dist\/styles\/forum.css":1,"sites\/all\/themes\/custom\/dist\/styles\/main.css":1},"js":{"sites\/all\/modules\/contrib\/eu_cookie_compliance\/js\/eu_cookie_compliance.js":1,"\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/1.8.3\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery-html-prefilter-3.5.0-backport.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/external\/jquery.cookie.js":1,"public:\/\/languages\/de_zp1tiaRbaNLh0zqK08F4xHN51HnzCwzrQyPf-jXmO_w.js":1,"sites\/all\/modules\/quote\/quote.js":1,"sites\/all\/themes\/custom\/dist\/scripts\/main.js":1}},"quote_nest":"2","better_exposed_filters":{"views":{"werbebanner":{"displays":{"block_2":{"filters":[]}}},"blogs":{"displays":{"block_1":{"filters":[]}}},"startintros":{"displays":{"block":{"filters":[]}}}}},"urlIsAjaxTrusted":{"\/start?destination=start":true},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":0,"popup_clicking_confirmation":false,"popup_scrolling_confirmation":false,"popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--opt-in\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003ECookies helfen uns dabei, unsere Dienste anzubieten, zu sch\u00fctzen und zu verbessern. Wenn Sie unsere Webseite weiterhin verwenden, erkl\u00e4ren Sie sich mit der Verwendung von Cookies einverstanden. Zur \u003Ca href=\u0022https:\/\/www.autorenwelt.de\/datenschutz\u0022 title=\u0022Datenschutzerkl\u00e4rung\u0022\u003EDatenschutzerkl\u00e4rung\u003C\/a\u003E.\u003C\/p\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003EMehr Informationen\u003C\/button\u003E\n          \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-secondary-button\u0022\u003EOK\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022decline-button eu-cookie-compliance-default-button\u0022 \u003EAblehnen\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","use_mobile_message":false,"mobile_popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--opt-in\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n                    \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003EMehr Informationen\u003C\/button\u003E\n          \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-secondary-button\u0022\u003EOK\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022decline-button eu-cookie-compliance-default-button\u0022 \u003EAblehnen\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","mobile_breakpoint":"768","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class=\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EThank you for accepting cookies\u003C\/h2\u003E\n\u003Cp\u003EYou can now hide this message or find out more about cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button eu-cookie-compliance-hide-button\u0022\u003EAusblenden\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button-thank-you\u0022 \u003EMore info\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_use_bare_css":false,"popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"\/datenschutz","popup_link_new_window":1,"popup_position":null,"fixed_top_position":1,"popup_language":"de","store_consent":false,"better_support_for_screen_readers":1,"reload_page":0,"domain":"","domain_all_sites":null,"popup_eu_only_js":0,"cookie_lifetime":"100","cookie_session":false,"disagree_do_not_show_popup":0,"method":"opt_in","whitelisted_cookies":"","withdraw_markup":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003EDatenschutzeinstellungen\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-withdraw-banner\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003EWir verwenden Cookies auf dieser Seite, um Ihnen die Arbeit mit der Seite zu erleichtern. Sie haben zugestimmt, dass wir Cookies verwenden d\u00fcrfen.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button\u0022\u003EZustimmung zur\u00fccknehmen\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","withdraw_enabled":false,"withdraw_button_on_info_popup":0,"cookie_categories":[],"enable_save_preferences_button":1,"fix_first_cookie_category":1,"select_all_categories_by_default":0}});</script>
<script>var eu_cookie_compliance_cookie_name = "";</script>
<script src="/sites/default/files/advagg_js/js__WcTpt-w0y1FyXrxZNriX8Ts1KjsFTDpdvZaw9yhRn24___SeDJKIvrikPXIIr55mRB1jrZkJNqFYEnfX8AHNM0No__FN_EAM9X694iQ4KmM-4TfP_6N9-ohC1JR3TCWcBvGFM.js"></script>
</body>
</html>
