<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>About Page template By Adobe Dreamweaver CC</title>
<link href="AboutPageAssets/styles/aboutPageStyle.css" rel="stylesheet" type="text/css"/>
<!--The following script tag downloads a font from the Adobe Edge Web Fonts server for use within the web page. We recommend that you do not modify it.-->
<script>var __adobewebfontsappname__="dreamweaver"</script><script src="http://use.edgefonts.net/montserrat:n4:default;source-sans-pro:n2:default.js" type="text/javascript"></script>
</head>
<body>
<!-- Header content -->
<header>
<div class="profileLogo">
<!-- Profile logo. Add a img tag in place of <span>. -->
</div>
<div class="profilePhoto">
<!-- Profile photo -->
<img alt="sample" src="images/logo2.jpg"/> </div>
<!-- Identity details -->
<section class="profileHeader">
<h1>Ravenhill Corporate Pty Ltd</h1>
<h3>Provision of Company Secretarial, Financial Reporting and other Business Services</h3>
<hr/>
<div>
<div>
<div>
<p>Ravenhill Corporate Pty Ltd specialises in the provision of Company Secretarial, Accounting, Financial Reporting and associated Business Services (together “Corporate Services”) for listed and non-listed entities.Â  Ravenhill provides a full service product incorporating pre and post IPO corporate compliance, governance and financial accounting management tailored to listed and unlisted entities. A summary of Ravenhill’s core competencies are:-</p>
</div>
</div>
</div>
<div>
<div>
<div>
<div>
<p><strong><em><u>Company Secretarial Services</u></em></strong></p>
<p>Ravenhill provides all aspects of Company Secretarial services including company administration, corporate governance and corporate compliance, liaison with regulatory bodies and commercial support to senior management and the Board of directors.</p>
<p><strong><em><u>Financial Reporting/Chief Financial Officer Services</u></em></strong><strong><u></u></strong></p>
<p>Ravenhill provides a range of financial services including bookkeeping, management of accounting and payroll as well as the preparation of various statutory annual, half yearly and quarterly financial reports.</p>
<p><strong><em><u>Corporate Governance</u></em></strong></p>
<p>Ravenhill also can advise and manage the Corporate Governance requirements including the measurement against the ASX Governance Council’s Good Corporate and Best Practice Recommendations.</p>
<p><strong><em><u>IPO Compliance</u></em></strong></p>
<p>All aspects of IPO compliance management including, assistance with capital raising, management of the due diligence process, submission of required ASIC &amp; ASX Forms, liaison with solicitor’s for the preparation of prospectus document, liaising with independent experts engaged in preparing reports and general administration of the IPO process through to admission to the official list of ASX.</p>
<p><strong><em><u>Non-Executive Directorship and Other Business Services</u></em></strong></p>
<p>Ravenhill also provides other business services including, but not limited to, the incorporation of new companies, Non-Executive Director services, deregistration of dormant subsidiary services, establishment and/or renewal of Directors and Officers Liability, Professional Indemnity and other general insurance requirements.</p>
</div>
</div>
</div>
</div>
<p>CONTACT: Ravenhill Corporate, 467 Liddelow Road Banjup Western Australia 6164 tel +61 417 714 292</p>
</section>
<aside class="socialNetworkNavBar">
<div class="socialNetworkNav"></div>
</aside>
</header>
<!-- content -->
<footer>
<p class="footerDisclaimer">2013-2018  Copyright - <span>Ravenhill Corporate Pty Ltd All Rights Reserved</span></p>
<p class="footerNote">Russell Hardwick BBus CPA ACIS - <a href="mailto:russell@ravenhillcorporate.com.au">email</a></p>
</footer>
</body>
</html>
