<!DOCTYPE html>
<html lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="Buy tires online and save! 4 Tires Canada offer discount on summer and winter tires with delivery. Best deals on wheels packages." name="description"/>
<meta content="Buy tires online and save! 4 Tires Canada offer discount on summer and winter tires with delivery. Best deals on wheels packages." property="og:description"/>
<meta content="4tires.ca" name="keywords"/>
<title>Online Tires Canada | Discount on tire and wheels</title>
<meta content="Online Tires Canada | Discount on tire and wheels" property="og:title"/>
<meta content="https://www.4tires.ca/" property="og:url"/>
<meta content="https://www.4tires.ca/skin/img/4tires/logo.jpg" property="og:image"/>
<meta content="4tires.ca" property="og:site_name"/>
<meta content="website" property="og:type"/>
<meta content="4tires.ca - tires online" name="author"/>
<meta content="info@4tires.ca" name="reply-to"/>
<meta content="15 days" name="revisit-after"/>
<meta content="Copyright 2021 - 4tires.ca Tires Online . All rights reserved." name="copyright"/>
<meta content="all" name="robots"/>
<meta content="#eb2326" name="theme-color"/>
<link href="https://www.4tires.ca/skin/img/4tires/logo.jpg" rel="image_src"/>
<link href="https://www.4tires.ca/xfavicon.ico.pagespeed.ic.qyYr7pdkcu.png" rel="icon" type="image/x-icon"/>
<link href="https://www.4tires.ca/xfavicon.ico.pagespeed.ic.qyYr7pdkcu.png" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="https://www.4tires.ca/xmobile_icon.png.pagespeed.ic.-cbspBVVfk.png" rel="apple-touch-icon"/>
<link href="/skin/css/bootstrap.min.css+select2.min.css+global.css+f1.css+pe-icons.css+4tires.css+slider.css+search.css+promotions.css+best-seller.css+slick-slider.css.pagespeed.cc.E6jr4GFG54.css" media="all" rel="stylesheet" type="text/css"/>
</head> <body>
<header>
<div class="container-fluid" id="top-promo">
FOR 5% INSTANT REBATE (4 TIRES AND 550$ MINIMUM) USE PROMO CODE: WEB-5 </div>
<div class="container-fluid container" id="top-container">
<div class="row align-items-center">
<div class="col">
<a href="/"><img alt="4tires.ca - Home" id="logo" src="/skin/img/4tires/xlogo.png.pagespeed.ic.g9yU7QruPP.png"/></a>
</div>
<div class="col">
<a href="tel:1-877-965-4449" id="top-phone">
<i class="pe-icons-cellphone"></i>1-877-965-4449 </a>
</div>
<div class="col text-right d-none d-lg-block" id="top-container-menu">
<i class="pe-icons-flag-canada"></i><a href="/cart/index" id="top-cart-link"><i class="pe-icons-cart"></i>Cart </a>
<i class="divider"></i>
<a href="http://4pneus.ca">Français</a>
<a href="https://www.facebook.com/4TIRES" target="_blank"><i class="pe-icons-facebook"></i></a>
</div>
</div>
</div> <div class="container-fluid" id="main-nav">
<div class="container">
<nav class="navbar navbar-expand-lg navbar-dark">
<button aria-controls="main-nav-collapsible" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler pull-right" data-target="#main-nav-collapsible" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="main-nav-collapsible">
<ul class="navbar-nav navbar-right justify-content-lg-between">
<li class="nav-item active">
<a class="nav-link" href="/">Home</a>
</li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="#">Tires</a>
<div class="dropdown-menu">
<a class="dropdown-item " href="/summer-tires">Summer tires</a>
<a class="dropdown-item " href="/winter-tires">Winter tires</a>
</div>
</li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="#">Wheels</a>
<div class="dropdown-menu">
<a class="dropdown-item " href="/search/wheels">Wheels by vehicle</a>
<a class="dropdown-item " href="/search/kits">Tires and wheels packages</a>
</div>
</li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="#">Manufacturers</a>
<div class="dropdown-menu">
<a class="dropdown-item " href="/brands/bridgestone/tires">Bridgestone</a>
<a class="dropdown-item " href="/brands/continental/tires">Continental</a>
<a class="dropdown-item " href="/brands/falken/tires">Falken</a>
<a class="dropdown-item " href="/brands/firestone/tires">Firestone</a>
<a class="dropdown-item " href="/brands/general/tires">General</a>
<a class="dropdown-item " href="/brands/good-year/tires">Goodyear</a>
<a class="dropdown-item " href="/brands/michelin/tires">Michelin</a>
<a class="dropdown-item " href="/brands/pirelli/tires">Pirelli</a>
<a class="dropdown-item " href="/brands/toyo/tires">Toyo</a>
<a class="dropdown-item " href="/brands/yokohama/tires">Yokohama</a>
</div>
</li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="#">Rankings</a>
<div class="dropdown-menu">
<a class="dropdown-item " href="/best-summer-tires">Best summer tires</a>
<a class="dropdown-item " href="/best-winter-tires">Best winter tires</a>
<a class="dropdown-item " href="/tires/studded-tires">Studded tires</a>
</div>
</li>
<li class="nav-item">
<a class="nav-link" href="/promotions/index">Mail-in rebates</a>
</li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="#">Information</a>
<div class="dropdown-menu">
<a class="dropdown-item " href="/cms/view/id/6/tires-shipping">Shipping info </a>
<a class="dropdown-item " href="/cms/view/id/12/guarantee">Guarantee</a>
<a class="dropdown-item " href="/cms/view/id/4/tires-faq">FAQ</a>
</div>
</li>
<li class="nav-item ">
<a class="nav-link" href="#">Blog</a>
</li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="#">Partners</a>
<div class="dropdown-menu">
<a class="dropdown-item" href="https://www.pneusecono.com" target="_blank">Pneus Econo</a>
<a class="dropdown-item" href="https://www.4pneus.ca" target="_blank">4pneus.ca</a>
</div>
</li>
<li class="nav-item dropdown-divider d-lg-none"></li>
<li class="nav-item d-list-item d-lg-none">
<a class="nav-link" href="/cart/index"><i class="pe-icons-cart"></i>Cart </a>
</li>
<li class="nav-item d-list-item d-lg-none">
<a class="nav-link" href="http://4pneus.ca">Français</a>
</li>
</ul>
</div>
</nav>
</div>
</div> </header>
<div class="container-fluid">
<div class="tires-search-banner row winter">
<div class="container">
<div class="row">
<div class="col-12 col-lg-4 col-xxl-5 tires-slides-container">
<div class="tires-search-slide">
</div>
</div>
<div class="col-12 col-lg-8 col-xxl-6 tires-slides-search-container">
<div class="tires-search-container">
<div class="tires-search-header">
<div class="tires-search-magnifier">
</div>
<div class="tires-search-title">
SEARCH </div>
</div>
<div class="tires-search-content">
<div class="tires-search-form-tabs">
<div class="tires-search-tab tires active" data-type="tire">
<span class="tires-search-tab-icon"></span>
TIRES </div>
<div class="tires-search-tab wheels" data-type="wheel">
<span class="tires-search-tab-icon"></span>
WHEELS </div>
<div class="tires-search-tab kits" data-type="kit">
<span class="tires-search-tab-icon"></span>
KITS </div>
</div>
<div class="tires-search-forms-container">
<div class="tires-search-form tires-search-form-tires active" data-type="tire">
<form action="/produits/search-dimensions" data-seo-url-prefix="/search/tires" id="search-form-tires">
<div class="tires-search-form-fields active">
<p class="tires-search-subheader">
<span class="d-block d-md-inline">TIRES BY SIZE</span>
<span class="d-block d-md-none"> </span>
</p>
<select class="tires-search-select" data-placeholder="WIDTH" id="tires-search-select-largeur" name="treadwidth" style="width: 100%">
<option></option>
</select>
<select class="tires-search-select" data-placeholder="PROFILE" id="tires-search-select-rapport" name="profile" style="width: 100%">
<option></option>
</select>
<select class="tires-search-select" data-placeholder="DIAMETER" id="tires-search-select-diametre" name="diameter" style="width: 100%">
<option></option>
</select>
</div>
<div class="tires-search-form-buttons">
<div class="tires-search-form-button summer" data-category="4" data-seo-season="summer">
<span class="tires-search-form-icon"></span>
SUMMER <span class="tires-search-form-checkmark"></span>
</div>
<div class="tires-search-form-button winter" data-category="3" data-seo-season="winter">
<span class="tires-search-form-icon"></span>
WINTER <span class="tires-search-form-checkmark"></span>
</div>
<input data-seo-season="winter" name="categorie" type="hidden" value="3"/>
<button class="tires-search-form-button search">
SEARCH </button>
</div>
</form>
</div>
<div class="tires-search-form tires-search-form-wheels" data-type="wheel">
<form action="/roues/search-dimensions-wheels" id="search-form-wheels">
<div class="tires-search-form-fields step1 active">
<p class="tires-search-subheader">
<span class="d-block d-md-inline">SEARCH WHEELS &amp; MAGS BY VEHICLE</span>
<span class="d-none d-md-inline">|</span>
<span class="d-block d-md-inline">STEP 1 OF 2</span>
</p>
<select class="tires-search-select annee" data-placeholder="YEAR" name="annee" style="width:100%">
<option></option>
</select>
<select class="tires-search-select marque" data-placeholder="MAKE" name="marque" style="width:100%">
<option></option>
</select>
<select class="tires-search-select model" data-placeholder="MODEL" name="model" style="width:100%">
<option></option>
</select>
<select class="tires-search-select modelsoptions" data-placeholder="OPTIONS" name="modelsoptions" style="width:100%">
<option></option>
</select>
</div>
<div class="tires-search-form-fields step2">
<p class="tires-search-subheader">
<span class="d-block d-md-inline">SEARCH WHEELS &amp; MAGS BY VEHICLE</span>
<span class="d-none d-md-inline">|</span>
<span class="d-block d-md-inline">STEP 2 OF 2</span>
</p>
<p class="tires-search-instructions">CHOOSE DIAMETER</p>
<p class="tires-search-form-no-results">
<span>NO PRODUCTS FOUND</span><br/>
<a href="#">&lt; TRY AGAIN</a>
</p>
<div class="tires-search-form-sizes-container">
<img class="lazyload ajax-laoder-sizes" data-src="/skin/slider/ajax-loader-sizes.gif" loading="lazy"/>
</div>
</div>
<div class="tires-search-form-buttons">
<div class="tires-search-form-button summer" data-category="4" data-seo-season="summer">
<span class="tires-search-form-icon"></span>
SUMMER <span class="tires-search-form-checkmark"></span>
</div>
<div class="tires-search-form-button winter" data-category="3" data-seo-season="winter">
<span class="tires-search-form-icon"></span>
WINTER <span class="tires-search-form-checkmark"></span>
</div>
<input data-seo-season="winter" name="categorie" type="hidden" value="3"/>
<button class="tires-search-form-button search" data-step-1-text="CONTINUE" data-step-2-text="SEARCH">
CONTINUE </button>
</div>
</form>
</div>
<div class="tires-search-form tires-search-form-kits" data-type="kit">
<form action="/kits/search-dimensions-wheels-for-kit" id="search-form-kits">
<div class="tires-search-form-fields step1 active">
<p class="tires-search-subheader">
<span class="d-block d-md-inline">KITS BY VEHICLE</span>
<span class="d-none d-md-inline">|</span>
<span class="d-block d-md-inline">STEP 1 OF 2</span>
</p>
<select class="tires-search-select annee" data-placeholder="YEAR" name="annee" style="width: 100%">
<option></option>
</select>
<select class="tires-search-select marque" data-placeholder="MAKE" name="marque" style="width: 100%">
<option></option>
</select>
<select class="tires-search-select model" data-placeholder="MODEL" name="model" style="width: 100%">
<option></option>
</select>
<select class="tires-search-select modelsoptions" data-placeholder="OPTIONS" name="modelsoptions" style="width: 100%">
<option></option>
</select>
</div>
<div class="tires-search-form-fields step2">
<p class="tires-search-subheader">
<span class="d-block d-md-inline">KITS BY VEHICLE</span>
<span class="d-none d-md-inline">|</span>
<span class="d-block d-md-inline">STEP 2 OF 2</span>
</p>
<p class="tires-search-instructions">CHOOSE DIAMETER</p>
<p class="tires-search-form-no-results">
<span>NO PRODUCTS FOUND</span><br/>
<a href="#">&lt; TRY AGAIN</a>
</p>
<div class="tires-search-form-sizes-container">
<img class="lazyload ajax-laoder-sizes" data-src="/skin/slider/ajax-loader-sizes.gif" loading="lazy"/>
</div>
</div>
<div class="tires-search-form-buttons">
<div class="tires-search-form-button summer" data-category="4" data-seo-season="summer">
<span class="tires-search-form-icon"></span>
SUMMER <span class="tires-search-form-checkmark"></span>
</div>
<div class="tires-search-form-button winter" data-category="3" data-seo-season="winter">
<span class="tires-search-form-icon"></span>
WINTER <span class="tires-search-form-checkmark"></span>
</div>
<input data-seo-season="winter" name="categorie" type="hidden" value="3"/>
<button class="tires-search-form-button search" data-step-1-text="CONTINUE" data-step-2-text="SEARCH">
CONTINUE </button>
</div>
</form>
</div>
</div>
</div>
</div> </div>
</div>
</div>
</div> </div>
<div class="container-fluid black-bg" id="main-content">
<div class="container lg-padded-top">
<div class="row xl-marged-bottom">
<div class="col offset-lg-1 col-lg-10 text-center">
<h1 class="red-bar">About 4 Tires Canada</h1>
<p></p><p>4Tires.ca is an Online tires website that offer top brands products at cheap prices. We sell summer and winter tires in Canada at discount prices from all major manufacturers such as: Toyo, Yokohama, Bridgestone, Firestone, GoodYear, Dunlop, Falken, Kumho, Pirelli, Continental, General, Maxtrek, Minerva, Cooper, Sailun and many more. We also offer a wide selection of Wheels and packages. Delivery is free almost everywhere in Ontario, Toronto, Ottawa, and Québec or at a very low price for Nova-Scotia and New-Brunswick. We offer many products including rims and wheels online for Canadian and a wide variety of winter and summer tires for sale for passenger, light truck and performance vehicles.</p>
</div>
</div>
<div class="row justify-content-center">
<div class="col col-sm-4 col-lg-3 home-callout-box xl-marged-bottom">
<div class="home-icon-truck">
<p class="home-icon-label">DELIVERY TO YOUR HOME</p>
</div>
<p class="text-center">
Order online and get your order delivered to your doorstep! </p>
</div>
<div class="col col-sm-4 col-lg-3 home-callout-box xl-marged-bottom">
<div class="home-icon-medal">
<p class="home-icon-label">BEST PRICE GUARANTEE</p>
</div>
<p class="text-center">
We guarantee and match the lowest prices on the market. </p>
</div>
<div class="col col-sm-4 col-lg-3 home-callout-box xl-marged-bottom">
<div class="home-icon-tire">
<p class="home-icon-label">LARGE SELECTION OF TIRES AND WHEELS</p>
</div>
<p class="text-center">
Get the most popular brands, without breaking the bank! </p>
</div>
</div>
</div> </div>
<div class="container-fluid" id="best-seller-container">
<div class="container">
<div class="row xxl-marged-top lg-marged-bottom">
<div class="col">
<h2 class="red-bar">Tires :</h2>
</div>
</div>
<div class="row xxl-marged-bottom">
<div class="col best-seller-item-slider three-col clearfix">
<a class="best-seller-item" href="/tires/continental-viking-contact-7-winter">
<div class="best-seller-item-image">
<img alt="CONTINENTAL VIKING CONTACT 7" class="lazyload" data-src="https://cdn.pneusecono.com/images/produits/Viking-Contact-.jpg" loading="lazy"/>
</div>
<div class="best-seller-item-text">
<p class="best-seller-item-model">CONTINENTAL VIKING CONTACT 7</p>
</div>
</a>
<a class="best-seller-item" href="/tires/bridgestone-blizzak-ws90-winter">
<div class="best-seller-item-image">
<img alt="BRIDGESTONE BLIZZAK WS90" class="lazyload" data-src="https://cdn.pneusecono.com/images/produits/Bridgestone-Blizzak-WS90.jpg" loading="lazy"/>
</div>
<div class="best-seller-item-text">
<p class="best-seller-item-model">BRIDGESTONE BLIZZAK WS90</p>
</div>
</a>
</div>
</div>
<script>document.addEventListener("DOMContentLoaded",function(){$('.best-seller-item-slider').slick({dots:false,infinite:false,slidesToShow:3,slidesToScroll:1,responsive:[{breakpoint:576,settings:{slidesToShow:1}},{breakpoint:720,settings:{slidesToShow:2}},{breakpoint:960,settings:{slidesToShow:3}}]});});</script>
</div>
</div>
<div id="brands-container">
<div class="container-fluid">
<div class="container">
<div class="row xxl-marged-top xl-marged-bottom">
<div class="col-12">
<h3 class="red-bar">Manufacturers</h3>
</div>
</div>
<div class="row lg-marged-bottom">
<div class="col-12 col-sm-6 col-lg-3 lg-marged-bottom" id="fabricant_container_44">
<a class="fabricant_logo_container lg-marged-bottom" href="/brands/bridgestone/tires" title="Bridgestone Tires ">
<img alt="BRIDGESTONE" class="lazyload fabricant_logo_grid" data-src="https://cdn.pneusecono.com/images/fabricants/Bridgestone-test-.png" loading="lazy"/>
</a>
<p class="fabricant_text_grid">If you are looking for first quality products without compromise, Bridgestone Tires is the way to go. That Japan company is one of the worldwide leader in their industry [...]</p>
</div>
<div class="col-12 col-sm-6 col-lg-3 lg-marged-bottom" id="fabricant_container_31">
<a class="fabricant_logo_container lg-marged-bottom" href="/brands/continental/tires" title="Continental Tires">
<img alt="CONTINENTAL" class="lazyload fabricant_logo_grid" data-src="https://cdn.pneusecono.com/images/fabricants/continental-logo1.jpg" loading="lazy"/>
</a>
<p class="fabricant_text_grid">The company Continental is a worldwide leader in the industry for cars, trucks and high performance vehicles since 1871. Continental is also consider as one the largest [...]</p>
</div>
<div class="col-12 col-sm-6 col-lg-3 lg-marged-bottom" id="fabricant_container_14">
<a class="fabricant_logo_container lg-marged-bottom" href="/brands/pirelli/tires" title="Pirelli Tires ">
<img alt="PIRELLI" class="lazyload fabricant_logo_grid" data-src="https://cdn.pneusecono.com/images/fabricants/Pirelli-Logo3.jpg" loading="lazy"/>
</a>
<p class="fabricant_text_grid">The Italian tire manufacturer Pirelli is one of the top 5 worldwide. This company is well known for offering very high performance products for luxury sports cars such [...]</p>
</div>
<div class="col-12 col-sm-6 col-lg-3 lg-marged-bottom" id="fabricant_container_1">
<a class="fabricant_logo_container lg-marged-bottom" href="/brands/toyo/tires" title="Toyo Tires">
<img alt="TOYO" class="lazyload fabricant_logo_grid" data-src="https://cdn.pneusecono.com/images/fabricants/Toyo-Tires2.jpg" loading="lazy"/>
</a>
<p class="fabricant_text_grid"> 
Toyo is a Japan company that is recognise worldwide for offering high quality products for cars and trucks. Toyo produce a wide variety of different models to [...]</p>
</div>
</div>
</div>
</div>
</div>
<div class="container-fluid light-grey-bg lg-marged-top md-padded-top md-padded-bottom">
<div class="container">
<div class="box-featured-container">
<div class="box-featured-subcontainer">
<p class="brand_title">BF GOODRICH</p>
<ul class="box-featured-list">
<li class="box-featured-listitem">
<a href="/tires/bf-all-terrain-ta-ko-2-lt-10-plis-summer" title="BF GOODRICH BF ALL TERRAIN TA KO 2 LT 10 PLIS">BF ALL TERRAIN TA KO 2 LT 10 PLIS</a>
</li>
</ul>
</div>
<div class="box-featured-subcontainer">
<p class="brand_title">BRIDGESTONE</p>
<ul class="box-featured-list">
<li class="box-featured-listitem">
<a href="/tires/bridgestone-blizzak-ws90-winter" title="BRIDGESTONE BLIZZAK WS90">BLIZZAK WS90</a>
</li>
<li class="box-featured-listitem">
<a href="/tires/bridgestone-ws80-winter" title="BRIDGESTONE WS80">WS80</a>
</li>
</ul>
</div>
<div class="box-featured-subcontainer">
<p class="brand_title">CONTINENTAL</p>
<ul class="box-featured-list">
<li class="box-featured-listitem">
<a href="/tires/continental-extreme-contact-dws06-summer" title="CONTINENTAL EXTREME CONTACT DWS06">EXTREME CONTACT DWS06</a>
</li>
<li class="box-featured-listitem">
<a href="/tires/continental-pure-contact-ls-summer" title="CONTINENTAL PURE CONTACT LS">PURE CONTACT LS</a>
</li>
<li class="box-featured-listitem">
<a href="/tires/continental-viking-contact-7-winter" title="CONTINENTAL VIKING CONTACT 7">VIKING CONTACT 7</a>
</li>
</ul>
</div>
<div class="box-featured-subcontainer">
<p class="brand_title">COOPER</p>
<ul class="box-featured-list">
<li class="box-featured-listitem">
<a href="/tires/discover-true-north-winter" title="COOPER DISCOVER TRUE NORTH">DISCOVER TRUE NORTH</a>
</li>
</ul>
</div>
<div class="box-featured-subcontainer">
<p class="brand_title">FALKEN</p>
<ul class="box-featured-list">
<li class="box-featured-listitem">
<a href="/tires/falken-wildpeak-a-t-3w-summer" title="FALKEN WILDPEAK A/T 3W">WILDPEAK A/T 3W</a>
</li>
</ul>
</div>
<div class="box-featured-subcontainer">
<p class="brand_title">FIRESTONE</p>
<ul class="box-featured-list">
<li class="box-featured-listitem">
<a href="/tires/firestone-firehawk-indy-500-summer" title="FIRESTONE FIREHAWK INDY 500">FIREHAWK INDY 500</a>
</li>
</ul>
</div>
<div class="box-featured-subcontainer">
<p class="brand_title">GENERAL</p>
<ul class="box-featured-list">
<li class="box-featured-listitem">
<a href="/tires/general-altimax-rt43-summer" title="GENERAL ALTIMAX RT43">ALTIMAX RT43</a>
</li>
</ul>
</div>
<div class="box-featured-subcontainer">
<p class="brand_title">GOOD YEAR</p>
<ul class="box-featured-list">
<li class="box-featured-listitem">
<a href="/tires/good-year-wrangler-duratrac-summer" title="GOOD YEAR WRANGLER DURATRAC">WRANGLER DURATRAC</a>
</li>
<li class="box-featured-listitem">
<a href="/tires/goodyear-winter-command-winter" title="GOOD YEAR GOODYEAR WINTER COMMAND">GOODYEAR WINTER COMMAND</a>
</li>
</ul>
</div>
<div class="box-featured-subcontainer">
<p class="brand_title">MICHELIN</p>
<ul class="box-featured-list">
<li class="box-featured-listitem">
<a href="/tires/michelin-defender-t-h-summer" title="MICHELIN DEFENDER T + H">DEFENDER T + H</a>
</li>
<li class="box-featured-listitem">
<a href="/tires/michelin-pilot-sport-4s-summer" title="MICHELIN PILOT SPORT 4S">PILOT SPORT 4S</a>
</li>
<li class="box-featured-listitem">
<a href="/tires/michelin-x-ice-snow-winter" title="MICHELIN X-ICE SNOW">X-ICE SNOW</a>
</li>
</ul>
</div>
<div class="box-featured-subcontainer">
<p class="brand_title">PIRELLI</p>
<ul class="box-featured-list">
<li class="box-featured-listitem">
<a href="/tires/pirelli-cinturato-p7-a-s-plus-2-summer" title="PIRELLI CINTURATO P7 A/S PLUS 2">CINTURATO P7 A/S PLUS 2</a>
</li>
<li class="box-featured-listitem">
<a href="/tires/pirelli-p4-plus-summer" title="PIRELLI P4 PLUS">P4 PLUS</a>
</li>
</ul>
</div>
<div class="box-featured-subcontainer">
<p class="brand_title">TOYO</p>
<ul class="box-featured-list">
<li class="box-featured-listitem">
<a href="/tires/toyo-extensa-a-s-2-summer" title="TOYO EXTENSA A/S II">EXTENSA A/S II</a>
</li>
</ul>
</div>
<div class="box-featured-subcontainer">
<p class="brand_title">YOKOHAMA</p>
<ul class="box-featured-list">
<li class="box-featured-listitem">
<a href="/tires/yokohama-avid-ascend-gt-summer" title="YOKOHAMA AVID ASCEND GT">AVID ASCEND GT</a>
</li>
<li class="box-featured-listitem">
<a href="/tires/yokohama-avid-ascend-lx-summer" title="YOKOHAMA AVID ASCEND LX">AVID ASCEND LX</a>
</li>
<li class="box-featured-listitem">
<a href="/tires/yokohama-ice-guard-ig53-winter" title="YOKOHAMA ICE GUARD IG53">ICE GUARD IG53</a>
</li>
</ul>
</div>
</div>
</div>
</div> <div class="container-fluid" id="footer_container">
<div class="container">
<div class="row">
<div class="col-sm lg-marged-bottom">
<p class="footer-header">TO REACH US</p>
<p class="footer-coords footer-icon-item">
<i class="pe-icons-clock"></i>Monday to Friday <br/>9:00am - 4:00pm</p>
<p class="footer-coords footer-icon-item">
<i class="pe-icons-cellphone"></i>1-877-965-4449</p>
<p class="footer-coords footer-icon-item">
<i class="pe-icons-pointer-blanc"></i>4904 Ambroise-Lafortune,<br/>Boisbriand, QC <br/>J7H 1S6</p>
<p class="footer-coords footer-icon-item">
<i class="pe-icons-facebook"></i><a href="https://www.facebook.com/4TIRES" target="_blank">4tires.ca</a>
</p>
</div>
<div class="col-sm lg-marged-bottom">
<p class="footer-header">NAVIGATION</p>
<ul class="footer-nav">
<li>
<a href="/">Home</a>
</li>
<li>
<a href="/best-winter-tires">Best winter tires</a>
</li>
<li>
<a href="/brands">Brands</a>
</li>
<li>
<a href="/promotions/index">Tire Rebates</a>
</li>
</ul>
</div>
<div class="col-sm lg-marged-bottom">
<p class="footer-header">INFORMATION</p>
<ul class="footer-nav">
<li>
<a href="/cms/view/id/4/tires-faq">FAQ</a>
</li>
<li>
<a href="/cms/view/id/6/tires-shipping">Shipping info </a>
</li>
<li>
<a href="/cms/view/id/12/guarantee">Guarantee</a>
</li>
</ul>
</div>
<div class="col-sm lg-marged-bottom">
<p class="footer-header">ONLINE PAYMENT</p>
<p class="footer-icon-item">
<i class="pe-icons-lock-1"></i>Secure online payment </p>
<img alt="Secure payment using your credit card" class="lazyload" data-src="/skin/img/xonline-payment.png.pagespeed.ic.6LqUpLaBmX.png" loading="lazy"/>
</div>
</div>
</div>
</div> <script defer="" src="/skin/js/jquery-3.4.1.min.js.pagespeed.jm.tJmcu2pzqb.js"></script>
<script defer="" src="/skin/js/bootstrap.bundle.js.pagespeed.jm.Iz-PkNhBE3.js"></script>
<script defer="" src="/skin/js/slick.min.js.pagespeed.jm.7-LcV797cx.js"></script>
<script defer="" src="/skin/js/select2.min.js.pagespeed.jm.n-DLQ87mhh.js"></script>
<script defer="" src="/skin/js/search.js.pagespeed.jm.Ui7Fmto828.js"></script> <script>document.addEventListener("DOMContentLoaded",function(){(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create','UA-30650509-2','4tires.ca');ga('send','pageview');(function($){var defaults={sm:576,md:768,lg:992,xl:1200,xxl:1400,navbar_expand:'lg',delay:300};var navbar=jQuery('#main-nav nav');var screen_width=$(document).width();if(screen_width>=defaults[defaults.navbar_expand]){navbar.find('.dropdown').hover(function(){var dropdown=$(this);if(dropdown.data('hidingTimeout')){clearTimeout(dropdown.data('hidingTimeout'));dropdown.data('hidingTimeout',null);}dropdown.addClass('show');dropdown.find('.dropdown-menu').first().addClass('show');},function(){var dropdown=$(this);dropdown.data('hidingTimeout',setTimeout(function(){dropdown.removeClass('show');dropdown.find('.dropdown-menu').first().removeClass('show');},defaults.delay));});}$('.dropdown-menu a.dropdown-toggle').on('click',function(e){if(!$(this).next().hasClass('show')){$(this).parents('.dropdown-menu').first().find('.show').removeClass("show");}var $subMenu=$(this).next(".dropdown-menu");$subMenu.toggleClass('show');$(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown',function(e){$('.dropdown-submenu .show').removeClass("show");});return false;});})(jQuery);setTimeout(function(){$(document.body).addClass('domready');},0);if('loading'in HTMLImageElement.prototype){var images=document.querySelectorAll("img.lazyload");images.forEach(function(img){img.src=img.dataset.src;});}else{var script=document.createElement("script");script.defer=true;script.src="https://cdnjs.cloudflare.com/ajax/libs/lazysizes/4.1.8/lazysizes.min.js";document.body.appendChild(script);}});</script>
<!-- Page rendered at 2021-01-05 15:24:39 --> </body>
</html>
