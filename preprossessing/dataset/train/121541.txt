<!DOCTYPE html>
<html dir="ltr" lang="en" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
<link href="http://www.w3.org/1999/xhtml/vocab" rel="profile"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Drupal 7 (http://drupal.org)" name="Generator"/>
<link href="https://www.andreacheng.com/sites/default/files/icon_2.png" rel="shortcut icon" type="image/png"/>
<title>home | Andrea Cheng</title>
<link href="https://www.andreacheng.com/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.andreacheng.com/sites/default/files/css/css_0EKM-yZMZBQCiQKekYAxFMK0rLsD2lY4Fr9Fdo6NAok.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.andreacheng.com/sites/default/files/css/css_PGbJgHCUCBf4dg7K9Kt8aAwsApndP4GZ9RuToPy3-Fk.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@3.0.2/dist/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.andreacheng.com/sites/default/files/css/css_2ZIZOAVINhd9eAxEjB5eLWZ26h4z7jtwgQ2ZggjZkyI.css" media="all" rel="stylesheet" type="text/css"/>
<!-- HTML5 element support for IE6-8 -->
<!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script>window.jQuery || document.write("<script src='/sites/all/modules/jquery_update/replace/jquery/1.10/jquery.min.js'>\x3C/script>")</script>
<script src="https://www.andreacheng.com/sites/default/files/js/js_38VWQ3jjQx0wRFj7gkntZr077GgJoGn5nv3v05IeLLo.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.0.2/dist/js/bootstrap.min.js"></script>
<script>document.createElement( "picture" );</script>
<script src="https://www.andreacheng.com/sites/default/files/js/js_rsGiM5M1ffe6EhN-RnhM5f3pDyJ8ZAPFJNKpfjtepLk.js"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-53240707-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<script src="https://www.andreacheng.com/sites/default/files/js/js_kfMYxXShlHmQrqWkWHLPXsFb2EndUTctbtVpq6e9QWg.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"andreacheng","theme_token":"8qRWs2za4jSUDO51giMh1JZbreTl8umEnA3gwns7ZFM","js":{"sites\/all\/modules\/picture\/picturefill2\/picturefill.min.js":1,"sites\/all\/modules\/picture\/picture.min.js":1,"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"\/\/code.jquery.com\/jquery-1.10.2.min.js":1,"0":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery-html-prefilter-3.5.0-backport.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"https:\/\/cdn.jsdelivr.net\/npm\/bootstrap@3.0.2\/dist\/js\/bootstrap.min.js":1,"1":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"2":1,"sites\/all\/themes\/andreacheng\/js\/jquery.cycle.min.js":1,"sites\/all\/themes\/andreacheng\/js\/jquery.maximage.min.js":1,"sites\/all\/themes\/andreacheng\/js\/andreacheng.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/picture\/picture_wysiwyg.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"https:\/\/cdn.jsdelivr.net\/npm\/bootstrap@3.0.2\/dist\/css\/bootstrap.min.css":1,"sites\/all\/themes\/andreacheng\/css\/style.css":1}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"bootstrap":{"anchorsFix":1,"anchorsSmoothScrolling":1,"formHasError":1,"popoverEnabled":1,"popoverOptions":{"animation":1,"html":0,"placement":"bottom","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
</head>
<body class="navbar-is-fixed-top html front not-logged-in no-sidebars page-home role-anonymous-user">
<div id="skip-link">
<a class="element-invisible element-focusable" href="#main-content">Skip to main content</a>
</div>
<header class="navbar navbar-fixed-top navbar-default" id="navbar" role="banner">
<div class="container">
<div class="navbar-header">
<a class="name navbar-brand" href="/" title="Home">Andrea Cheng</a>
<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="navbar-collapse collapse">
<nav role="navigation">
<ul class="menu nav navbar-nav"><li class="first leaf"><a href="/news" title="">News</a></li>
<li class="leaf"><a href="/books" title="">Books</a></li>
<li class="leaf"><a href="/resources" title="">Resources</a></li>
<li class="last leaf"><a href="/about/about-andrea">About</a></li>
</ul> <div class="administer-block-container">
<div class="administer-block">
<div class="btn-group">
<button class="btn btn-info dropdown-toggle" data-toggle="dropdown" type="button">Administration<span class="caret"></span></button>
<ul class="dropdown-menu">
<li>
<a class="admin-item" href="/node/add/news" id="news-erstellen">New news</a></li>
<li>
<a class="admin-item" href="/node/add/project" id="projekt-erstellen">New project</a></li>
<li>
<a class="admin-item" href="/node/add/book" id="book-erstellen">New book</a></li>
<li class="divider"></li>
<li>
<a class="admin-item" href="/admin/content" id="inhalt-bearbeiten">Content list</a></li>
<li>
<a class="admin-item" href="/reorder-books" id="reorder-books">Reorder books</a></li>
<li class="divider"></li>
<li>
<a class="admin-item" href="/admin/config/development/performance" id="cache">Clear cache</a></li>
<li class="divider"></li>
<li>
<a class="admin-item" href="/user/logout" id="abmelden">Logout</a></li>
</ul>
</div> </div>
</div>
<div class="secondary-menu-container">
<div class="secondary-menu">
<a class="facebook-link" href="http://www.facebook.com/authorandreacheng" target="_blank"></a> </div>
</div>
</nav>
</div>
</div>
</header>
<div class="main-container container">
<header id="page-header" role="banner">
</header> <!-- /#page-header -->
<div class="row">
<section class="col-sm-12">
<a id="main-content"></a>
<h1 class="page-header">home</h1>
<div class="region region-content">
<section class="block block-system clearfix" id="block-system-main">
<img alt="" id="js-loader" src="sites/all/themes/andreacheng/loader_78x78.gif"/>
<div class="view view-front-slider view-id-front_slider view-display-id-page_1 front-slider view-dom-id-3fdce0e31bc03c5413e5e8a449d4eee3">
<div>
<img alt="" class="img-responsive" height="662" src="https://www.andreacheng.com/sites/default/files/Year%20of%20the%20Baby%20copy-banner_0.jpg" typeof="foaf:Image" width="1000"/>
<div class="quote"><div class="quote-from">from <a href="/book/year-baby">The Year of the Baby</a></div><div class="quote-text">I pick up my sister and smell her baby smell.  She rubs her eyes with her fists the way she always does when she's sleepy.  Then she  opens her mouth and puts it on my cheek.
     "Was that a kiss?" I ask. <a href="/book/year-baby"><span class="quote-more">More</span><span class="quote-more-arrow" data-icon="g"></span></a></div></div> </div>
<div>
<img alt="" class="img-responsive" height="1166" src="https://www.andreacheng.com/sites/default/files/year%20of%20the%20book-banner-1.jpg" typeof="foaf:Image" width="1558"/>
<div class="quote"><div class="quote-from">from <a href="/book/year-book">The Year of the Book</a></div><div class="quote-text">I'd rather stay with Ray than go onto the fourth grade playground where Laura and Allison stand so close that there's no space left for me.<a href="/book/year-book"><span class="quote-more">More</span><span class="quote-more-arrow" data-icon="g"></span></a></div></div> </div>
<div>
<img alt="" class="img-responsive" height="1175" src="https://www.andreacheng.com/sites/default/files/banner2.jpg" typeof="foaf:Image" width="2363"/>
<div class="quote"><div class="quote-from">from <a href="/book/etched-clay">Etched in Clay</a></div><div class="quote-text">Master says Dave--that suits you.  That's your name."  He can call me whatever he pleases, tom or John or Will or Dave, no matter.
I had another name once.<a href="/book/etched-clay"><span class="quote-more">More</span><span class="quote-more-arrow" data-icon="g"></span></a></div></div> </div>
<div>
<img alt="" class="img-responsive" height="741" src="https://www.andreacheng.com/sites/default/files/lemon-sisters-illustration.png" typeof="foaf:Image" width="1202"/>
<div class="quote"><div class="quote-from">from <a href="/book/lemon-sisters">The Lemon Sisters</a></div><div class="quote-text">By the Ginkgo tree I see three children wearing hats – one red, one blue, one yellow – and mittened hands already busy scooping, piling, dumping. They mound and carve and pat until they have a house around the tree trunk with one chair for each.<a href="/book/lemon-sisters"><span class="quote-more">More</span><span class="quote-more-arrow" data-icon="g"></span></a></div></div> </div>
</div><div class="max-nav">
<!--<div class="front-page-down-div"><a href="#front-content" class="front-page-down"><span aria-hidden="true" data-icon="j"></span></a></div>-->
<a id="arrow_left">c</a>
<a id="arrow_right">b</a>
</div>
<div id="front-caption"></div>
</section>
<section class="block block-block clearfix" id="block-block-1">
<a aria-hidden="true" class="front-page-down" data-icon="j" href="#front-content"></a>
</section>
<section class="block block-views clearfix" id="block-views-welcome-block">
<div class="view view-welcome view-id-welcome view-display-id-block row view-dom-id-12bd85fa1c86455db6beff26c4ebd2ef">
<div class="view-header">
<div id="front-content"></div> </div>
<div class="view-content">
<div>
<div class="col-sm-12 header"><h6>Welcome</h6></div><div class="col-sm-12 welcome-row"><div class="row"><div class="col-sm-3 welcome-image"><img alt="" class="img-responsive" height="500" src="https://www.andreacheng.com/sites/default/files/styles/author_photo/public/Andrea%20Cheng-1.jpg?itok=6YZ7vsw_" typeof="foaf:Image" width="500"/></div><div class="col-sm-9 welcome-text"><p>Andrea Cheng (1957 – 2015) was a children's book writer and illustrator. So many thanks to all of her readers for your letters and posts. Andrea loved writing for you. You continue to give life to her books!</p>
</div></div></div> </div>
</div>
</div>
</section>
<section class="block block-views clearfix" id="block-views-front-news-block">
<div class="view view-front-news view-id-front_news view-display-id-block row view-dom-id-a16ac2ef2bcb6cf0c3b29233f917e918">
<div class="view-content">
<table class="views-view-grid cols-4">
<tbody>
<tr>
<td>
<div class="news-item"><h6 class="header">News</h6><h3 class="news-title">Andrea Cheng :  1957 – 2015</h3><div class="news-image"></div><div class="news-body"><p>Andrea Cheng died on December 26th, 2015, after a long illness. Her family asks that readers not send gifts or flowers. </p>
<p>Andrea did finish two manuscripts that will be coming out soon. Thank you for reading!</p>
</div></div> </td>
<td>
<div class="news-item"><h6 class="header">News</h6><h3 class="news-title">Important List</h3><div class="news-image"></div><div class="news-body"><p>Nice to see <em>When the Bees Fly Home</em> on this important list! <a href="http://www.mothering.com/articles/20-picture-books-strong-girls-sensitive-boys/">http://www.mothering.com/articles/20-picture-books-strong-girls-sensitiv...</a></p>
</div></div> </td>
<td>
<div class="news-item"><h6 class="header">News</h6><h3 class="news-title">Shanghai Messenger</h3><div class="news-image"></div><div class="news-body"><p>Shanghai Messenger (illustrated by Ed Young) is now available in paperback!<a href="https://www.leeandlow.com/books/2449">https://www.leeandlow.com/books/2449</a> </p>
</div></div> </td>
<td>
<div class="news-item"><h6 class="header">News</h6><h3 class="news-title">Looking Glass Review</h3><div class="news-image"></div><div class="news-body"><p>Nice review from <a href="http://lookingglassreview.com/books/the-year-of-the-fortune-cookie" title="Looking Glass Review">Looking Glass </a>for The Year of the Fortune Cookie. </p>
</div></div> </td>
</tr>
</tbody>
</table>
</div>
</div>
</section>
</div>
</section>
</div>
</div>
<footer class="footer container">
<div class="region region-footer">
<section class="block block-views clearfix" id="block-views-footer-block">
<div class="view view-footer view-id-footer view-display-id-block row view-dom-id-813178b8d7f786fb582ea0a63a4ec2e7">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
<div> <div><ul><li><p>All content copyright<br/>Andrea Cheng 2016.</p>
</li>
<li><p><a href="http://www.andreacheng.com/contact">Contact</a></p>
</li>
</ul></div> </div> </div>
</div>
</div>
</section>
</div>
</footer>
<script src="https://www.andreacheng.com/sites/default/files/js/js_7Ukqb3ierdBEL0eowfOKzTkNu-Le97OPm-UqTS5NENU.js"></script>
<script src="https://www.andreacheng.com/sites/default/files/js/js_MRdvkC2u4oGsp5wVxBG1pGV5NrCPW3mssHxIn6G9tGE.js"></script>
</body>
</html>
