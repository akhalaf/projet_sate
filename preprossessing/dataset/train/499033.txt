<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<base href="http://www.entecfurnaces.com/"/>
<title>404 Page not Found</title>
<meta content="" name="description"/>
<meta content="" name="keywords"/>
<meta content="Entec Industrial Furnaces Pvt. Ltd" name="author"/>
<link href="http://www.entecfurnaces.com/tmp/project%09%0a" rel="canonical"/>
<meta content="global" name="distribution"/>
<meta content="English" name="language"/>
<meta content="general" name="rating"/>
<meta content="index, follow" name="ROBOTS"/>
<meta content="Daily" name="revisit-after"/>
<meta content="index, follow" name="googlebot"/>
<meta content="index, follow" name="bingbot"/>
<!-- Stylesheets -->
<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="entec/css/bootstrap.css" rel="stylesheet"/>
<link href="entec/css/style.css" rel="stylesheet"/>
<link href="entec/css/responsive.css" rel="stylesheet"/>
<link href="entec/css/mobile-menu.css" rel="stylesheet"/>
<!-- Responsive -->
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-76295365-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-76295365-1');
</script>
</head><body>
<div class="page-wrapper">
<!-- Main Header-->
<header class="main-header">
<!--Header Top-->
<div class="header-top">
<div class="auto-container">
<div class="inner-container clearfix">
<div class="top-left">
<div class="text">Welcome To Entec Industrial Furnaces Pvt. Ltd</div>
</div>
<div class="top-right">
<div class="text" style="padding-right:20px;">Have any question? +91-9810005354</div>
<div class="social-buttons">
<!-- facebook  Button -->
<a class="social-margin" href="https://www.facebook.com/EntecIndustrialFurnacesPvtLtd/" target="_blank" title="facebook">
<div class="social-icon facebook"> <i aria-hidden="true" class="fab fa-facebook"></i> </div>
</a>
<a class="social-margin" href="https://twitter.com/entecfurnaces" target="_blank" title="twitter">
<div class="social-icon twitter"> <i aria-hidden="true" class="fab fa-twitter"></i> </div>
</a>
<a class="social-margin" href="https://in.pinterest.com/entecfurnaces/" target="_blank" title="pinterest">
<div class="social-icon pinterest"> <i aria-hidden="true" class="fab fa-pinterest-p"></i> </div>
</a>
<!-- pinterest Button -->
</div>
</div>
</div>
</div>
</div>
<!-- Header Upper -->
<div class="header-upper">
<div class="auto-container">
<div class="clearfix">
<div class="logo-outer">
<div class="logo"><a href="/" title="Entec Industrial Furnaces Pvt. Ltd"><img alt="Entec Industrial Furnaces Pvt. Ltd" src="images/entec-industrial-furnaces-logo.png" title="Entec Industrial Furnaces Pvt. Ltd"/></a></div>
</div>
<div class="upper-right clearfix">
<!--Info Box-->
<div class="upper-column info-box">
<div class="icon-box"><span class="icon flaticon-home"></span></div>
<ul>
<li><span>Plot No. 186, Sector-24 Faridabad</span></li>
<li>Haryana (India)</li>
</ul>
</div>
<!--Info Box-->
<div class="upper-column info-box">
<div class="icon-box"><span class="icon flaticon-mail-1"></span></div>
<ul>
<li><span>Send your mail at</span></li>
<li><a href="mailto:info@entecfurnaces.com">info@entecfurnaces.com</a></li>
</ul>
</div>
<!--Info Box-->
<div class="upper-column info-box">
<div class="icon-box"><span class="icon flaticon-phone-call"></span></div>
<ul>
<li><span>Call Us At</span></li>
<li>+91-8800660953</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<!-- Header Lower -->
<div class="header-lower">
<div class="auto-container">
<div class="inner-container clearfix">
<!--mobile-menu-->
<div class="header">
<!--<a href="#" class="logo">Responsive Nav</a>-->
<button class="nav-toggle" id="nav-toggle">Menu</button>
<nav class="nav-collapse" id="nav">
<ul class="menu-items">
<li><a href="/" title="Home">Home</a></li>
<li><a href="company-profile.html" title="Company Profile">Company Profile</a></li>
<li class="dropdown">
<a class="has-dropdown" href="our-products.html" title="Our Products">Our Products</a>
<ul class="sub-menu">
<li class="dropdown">
<a class="has-dropdown" href="industrial-furnace.html" title="Industrial Furnace">Industrial Furnace</a>
<ul class="sub-menu">
<li><a href="heat-treatment-furnace.html" title="Heat Treatment Furnace">Heat Treatment Furnace</a>
</li>
<li><a href="crucible-furnace.html" title="Crucible Furnace">Crucible Furnace</a>
<ul class="sub-menu">
<li><a href="crucible-type-aluminium-furnace.html" title="Crucible Type Aluminium Furnace">Crucible Type Aluminium Furnace</a></li>
</ul>
</li>
<li><a href="zinc-melting-furnace.html" title="Zinc Melting Furnace">Zinc Melting Furnace</a>
</li>
<li><a href="rotary-furnace.html" title="Rotary Furnace">Rotary Furnace</a>
<ul class="sub-menu">
<li><a href="aluminium-rotary-furnace.html" title="Aluminium Rotary Furnace">Aluminium Rotary Furnace</a></li>
</ul>
</li>
<li><a href="billet-reheating-furnace.html" title="Billet Reheating Furnace">Billet Reheating Furnace</a>
</li>
<li><a href="lead-refining-furnace.html" title="Lead Refining Furnace">Lead Refining Furnace</a>
</li>
<li><a href="lead-melting-rotary-furnace.html" title="Lead Melting Rotary Furnace">Lead Melting Rotary Furnace</a>
</li>
<li><a href="hydraulic-tilting-aluminium-melting-skelner-furnace.html" title="Hydraulic Tilting Aluminium Melting Skelner Furnace">Hydraulic Tilting Aluminium Melting Skelner Furnace</a>
</li>
<li><a href="tower-furnace.html" title="Tower Furnace">Tower Furnace</a>
</li>
<li><a href="electric-holding-furnace.html" title="Electric Holding Furnace">Electric Holding Furnace</a>
</li>
<li><a href="copper-melting-furnace.html" title="Copper Melting Furnace">Copper Melting Furnace</a>
</li>
<li><a href="aluminium-melting-furnace.html" title="Aluminium Melting Furnace">Aluminium Melting Furnace</a>
</li>
<li><a href="regenerative-furnace.html" title="Regenerative Furnace">Regenerative Furnace</a>
</li>
<li><a href="lead-refining-pot.html" title="Lead Refining Pot">Lead Refining Pot</a>
</li>
<li><a href="molten-metal-vacuum-laddle.html" title="Molten Metal Vacuum Laddle">Molten Metal Vacuum Laddle</a>
</li>
</ul>
</li>
<li class="dropdown">
<a class="has-dropdown" href="industrial-burner.html" title="Industrial Burner">Industrial Burner</a>
<ul class="sub-menu">
<li><a href="oil-fuel-burner.html" title="Oil Fuel Burner">Oil Fuel Burner</a>
</li>
<li><a href="gas-fuel-burner.html" title="Gas Fuel Burner">Gas Fuel Burner</a>
</li>
<li><a href="dual-fuel-burner.html" title="Dual Fuel Burner">Dual Fuel Burner</a>
</li>
<li><a href="regenerative-burner.html" title="Regenerative Burner">Regenerative Burner</a>
</li>
</ul>
</li>
<li class="dropdown">
<a class="has-dropdown" href="air-pollution-system.html" title="Air Pollution System">Air Pollution System</a>
</li>
<li class="dropdown">
<a class="has-dropdown" href="ingot-stacking-machine.html" title="Ingot Stacking Machine">Ingot Stacking Machine</a>
</li>
<li class="dropdown">
<a class="has-dropdown" href="scrap-charging-machine.html" title="Scrap Charging Machine">Scrap Charging Machine</a>
</li>
<li class="dropdown">
<a class="has-dropdown" href="aluminium-ingot-casting-machine.html" title="Aluminium Ingot Casting Machine">Aluminium Ingot Casting Machine</a>
</li>
<li class="dropdown">
<a class="has-dropdown" href="lead-ingot-casting-machine.html" title="Lead Ingot Casting Machine">Lead Ingot Casting Machine</a>
</li>
<li class="dropdown">
<a class="has-dropdown" href="wet-scrubber-type-air-pollution-control-system.html" title="Wet Scrubber Type Air Pollution Control System">Wet Scrubber Type Air Pollution Control System</a>
</li>
<li class="dropdown">
<a class="has-dropdown" href="baghouse-type-air-pollution-control-system.html" title="Baghouse Type Air Pollution Control System">Baghouse Type Air Pollution Control System</a>
</li>
<li class="dropdown">
<a class="has-dropdown" href="oil-heating-and-pumping-station.html" title="Oil Heating And Pumping Station">Oil Heating And Pumping Station</a>
</li>
<li class="dropdown">
<a class="has-dropdown" href="radiant-type-recuperator.html" title="Radiant Type Recuperator">Radiant Type Recuperator</a>
</li>
<li class="dropdown">
<a class="has-dropdown" href="lead-ingot-machine.html" title="Lead Ingot">Lead Ingot Machine</a>
</li>
</ul>
</li>
<li><a href="our-clients.html" title="Our Clients">Our Clients</a></li>
<!-- <li><a href="blog.html">Blog</a></li>-->
<li><a href="sitemap.html" title="sitemap">Sitemap</a></li>
<li><a href="blogs.html" title="Blogs">Blogs</a></li>
<li><a href="contact-us.html" title="Contact Us">Contact Us</a></li>
</ul>
</nav>
</div>
<!--mobile-manu-->
<div class="nav-outer">
<!-- Main Menu -->
<nav class="main-menu navbar-expand-md ">
<div class="navbar-header">
<!-- Toggle Button -->
<button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button"> <span class="icon flaticon-menu-button"></span> </button>
</div>
<div class="collapse navbar-collapse clearfix" id="navbarSupportedContent">
<ul class="navigation clearfix">
<li class="current"><a href="/" title="Home">Home</a></li>
<li><a href="company-profile.html" title="Company Profile">Company Profile</a></li>
<li class="dropdown"><a href="our-products.html" title="Our-Products">Our Products</a>
<ul>
<li class="dropdown"><a href="industrial-furnace.html" title="Industrial Furnace">Industrial Furnace					  						 					  </a>
<ul>
<li class="dropdown-1"><a href="heat-treatment-furnace.html" title="Heat Treatment Furnace">Heat Treatment Furnace						   						  </a>
</li>
<li class="dropdown-1"><a href="crucible-furnace.html" title="Crucible Furnace">Crucible Furnace						   						 						  </a>
<ul>
<li><a href="crucible-type-aluminium-furnace.html" title="Crucible Type Aluminium Furnace">Crucible Type Aluminium Furnace</a></li>
</ul>
</li>
<li class="dropdown-1"><a href="zinc-melting-furnace.html" title="Zinc Melting Furnace">Zinc Melting Furnace						   						  </a>
</li>
<li class="dropdown-1"><a href="rotary-furnace.html" title="Rotary Furnace">Rotary Furnace						   						 						  </a>
<ul>
<li><a href="aluminium-rotary-furnace.html" title="Aluminium Rotary Furnace">Aluminium Rotary Furnace</a></li>
</ul>
</li>
<li class="dropdown-1"><a href="billet-reheating-furnace.html" title="Billet Reheating Furnace">Billet Reheating Furnace						   						  </a>
</li>
<li class="dropdown-1"><a href="lead-refining-furnace.html" title="Lead Refining Furnace">Lead Refining Furnace						   						  </a>
</li>
<li class="dropdown-1"><a href="lead-melting-rotary-furnace.html" title="Lead Melting Rotary Furnace">Lead Melting Rotary Furnace						   						  </a>
</li>
<li class="dropdown-1"><a href="hydraulic-tilting-aluminium-melting-skelner-furnace.html" title="Hydraulic Tilting Aluminium Melting Skelner Furnace">Hydraulic Tilting Aluminium Melting Skelner Furnace						   						  </a>
</li>
<li class="dropdown-1"><a href="tower-furnace.html" title="Tower Furnace">Tower Furnace						   						  </a>
</li>
<li class="dropdown-1"><a href="electric-holding-furnace.html" title="Electric Holding Furnace">Electric Holding Furnace						   						  </a>
</li>
<li class="dropdown-1"><a href="copper-melting-furnace.html" title="Copper Melting Furnace">Copper Melting Furnace						   						  </a>
</li>
<li class="dropdown-1"><a href="aluminium-melting-furnace.html" title="Aluminium Melting Furnace">Aluminium Melting Furnace						   						  </a>
</li>
<li class="dropdown-1"><a href="regenerative-furnace.html" title="Regenerative Furnace">Regenerative Furnace						   						  </a>
</li>
<li class="dropdown-1"><a href="lead-refining-pot.html" title="Lead Refining Pot">Lead Refining Pot						   						  </a>
</li>
<li class="dropdown-1"><a href="molten-metal-vacuum-laddle.html" title="Molten Metal Vacuum Laddle">Molten Metal Vacuum Laddle						   						  </a>
</li>
</ul>
</li>
<li class="dropdown"><a href="industrial-burner.html" title="Industrial Burner">Industrial Burner					  						 					  </a>
<ul>
<li class="dropdown-1"><a href="oil-fuel-burner.html" title="Oil Fuel Burner">Oil Fuel Burner						   						  </a>
</li>
<li class="dropdown-1"><a href="gas-fuel-burner.html" title="Gas Fuel Burner">Gas Fuel Burner						   						  </a>
</li>
<li class="dropdown-1"><a href="dual-fuel-burner.html" title="Dual Fuel Burner">Dual Fuel Burner						   						  </a>
</li>
<li class="dropdown-1"><a href="regenerative-burner.html" title="Regenerative Burner">Regenerative Burner						   						  </a>
</li>
</ul>
</li>
<li class=""><a href="air-pollution-system.html" title="Air Pollution System">Air Pollution System					  					  </a>
</li>
<li class=""><a href="ingot-stacking-machine.html" title="Ingot Stacking Machine">Ingot Stacking Machine					  					  </a>
</li>
<li class=""><a href="scrap-charging-machine.html" title="Scrap Charging Machine">Scrap Charging Machine					  					  </a>
</li>
<li class=""><a href="aluminium-ingot-casting-machine.html" title="Aluminium Ingot Casting Machine">Aluminium Ingot Casting Machine					  					  </a>
</li>
<li class=""><a href="lead-ingot-casting-machine.html" title="Lead Ingot Casting Machine">Lead Ingot Casting Machine					  					  </a>
</li>
<li class=""><a href="wet-scrubber-type-air-pollution-control-system.html" title="Wet Scrubber Type Air Pollution Control System">Wet Scrubber Type Air Pollution Control System					  					  </a>
</li>
<li class=""><a href="baghouse-type-air-pollution-control-system.html" title="Baghouse Type Air Pollution Control System">Baghouse Type Air Pollution Control System					  					  </a>
</li>
<li class=""><a href="oil-heating-and-pumping-station.html" title="Oil Heating And Pumping Station">Oil Heating And Pumping Station					  					  </a>
</li>
<li class=""><a href="radiant-type-recuperator.html" title="Radiant Type Recuperator">Radiant Type Recuperator					  					  </a>
</li>
<li class=""><a href="lead-ingot-machine.html" title="Lead Ingot">Lead Ingot Machine					  					  </a>
</li>
</ul>
</li>
<li><a href="our-clients.html" title="Our Clients">Our Clients</a></li>
<!-- <li><a href="blog.html">Blog</a></li>-->
<li><a href="sitemap.html" title="sitemap">Sitemap</a></li>
<li><a href="blogs.html" title="Blogs">Blogs</a></li>
<li><a href="contact-us.html" title="Contact Us">Contact Us</a></li>
</ul>
</div>
</nav>
<!-- Main Menu End-->
</div>
<!-- Outer Box -->
<div class="outer-box">
<!-- Btn Box -->
<div class="btn-box"> <a class="theme-btn btn-style-one" href="contact-us.html" title="Get A Quote"> Get A Quote</a> </div>
</div>
</div>
</div>
</div>
<!--End Header Lower-->
</header>
<section class="page-title" style="background-image:url(entec/images/background/8.jpg);">
<div class="auto-container">
<div class="inner-container clearfix">
<div class="title-box">
<h1>404</h1>
<p404>
</p404></div>
</div>
</div>
</section>
<!--End Page Title-->
<section class="formative_breadcrumb hidden-xs">
<div class="container">
<div class="row">
<div class="col-lg-12">
<ul class="breadcrumb">
<li>
<a href="/" title="Home">Home
                </a>
</li>
<li class="active">               
                  404
              </li>
</ul>
</div>
</div>
</div>
</section>
<section class="about-section">
<div class="auto-container">
<div class="row">
<div class="content-column col-lg-12 col-md-12 col-sm-12 order-2">
<h2 class="error">
 	This page doesn’t exist in our website.	
	<br/>
</h2>
<br/>
<br/>
<h3>You can still browse our products – <a class="text-danger" href="our-products.html">VIEW PRODUCTS</a></h3>
<br/>
</div>
</div>
<div class="close"></div>
</div>
</section>
<footer class="main-footer" style="background-image: url(entec/images/background/2.jpg);">
<div class="auto-container">
<!--Widgets Section-->
<div class="widgets-section">
<div class="row">
<!--Big Column-->
<div class="big-column col-xl-6 col-lg-12 col-md-12 col-sm-12">
<div class="row">
<!--Footer Column-->
<div class="footer-column col-xl-7 col-lg-6 col-md-6 col-sm-12">
<div class="footer-widget about-widget">
<div class="footer-logo">
<figure class="image"><a href="/" title="Entec Industrial Furnaces Pvt. Ltd"><img alt="Entec Industrial Furnaces Pvt. Ltd" src="entec/images/footer-logo.png" title="Entec Industrial Furnaces Pvt. Ltd"/></a></figure>
</div>
<div class="widget-content">
<div class="text"><p style="text-align:justify"><strong>Entec Industrial Furnaces Pvt. Ltd</strong> was launched in the <strong>year 2002</strong> and based in <strong>Faridabad</strong>, India. Our main focus is on furnaces, burners or many other industrial products.</p>
</div>
<ul class="social-links">
<li><a href="https://www.facebook.com/EntecIndustrialFurnacesPvtLtd/" target="_blank"><i class="fab fa-facebook-f" title="facebook"></i></a></li>
<li><a href="https://twitter.com/entecfurnaces" target="_blank"><i class="fab fa-twitter" title="twitter"></i></a></li>
<li><a href="https://in.pinterest.com/entecfurnaces/" target="_blank"><i class="fab fa-pinterest" title="pinterest"></i></a></li>
</ul>
</div>
</div>
</div>
<!--Footer Column-->
<div class="footer-column col-xl-5 col-lg-6 col-md-6 col-sm-12">
<div class="footer-widget links-widget">
<p class="widget-title">Our Products</p>
<div class="widget-content">
<ul class="list clearfix">
<li><a href="industrial-furnace.html" title="Industrial Furnace Manufacturers">Industrial Furnace</a></li>
<li><a href="heat-treatment-furnace.html" title="Heat Treatment Furnace Manufacturers">Heat Treatment Furnace</a></li>
<li><a href="crucible-furnace.html" title="Crucible Furnace Manufacturers">Crucible Furnace</a></li>
<li><a href="crucible-type-aluminium-furnace.html" title="Crucible Type Aluminium Furnace Manufacturers">Crucible Type Aluminium Furnace</a></li>
<li><a href="zinc-melting-furnace.html" title="Zinc Melting Furnace Manufacturers">Zinc Melting Furnace</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<!--Big Column-->
<div class="big-column col-xl-6 col-lg-12 col-md-12 col-sm-12">
<div class="row clearfix">
<div class="footer-column col-xl-5 col-lg-6 col-md-6 col-sm-12">
<div class="footer-widget links-widget">
<p class="widget-title">Quick Links</p>
<div class="widget-content">
<ul class="list clearfix">
<li><a href="/" title="Gas Burner Manufacturers">Home</a></li>
<li><a href="company-profile.html" title="About Entec Industrial Furnaces Pvt. Ltd">Company Profile</a></li>
<li><a href="our-products.html" title="Industrial Furnace Suppliers">Our Products</a></li>
<li><a href="sitemap.html" title="Leading Industrial Oil Manufacturers in India">Sitemap</a></li>
<li><a href="our-presence.html" title="Our National and International Market place">Our Presence</a></li>
<li><a href="contact-us.html" title="Entec Industrial Furnaces Contact Details">Contact Us</a></li>
</ul>
</div>
</div>
</div>
<div class="footer-column col-xl-7 col-lg-6 col-md-6 col-sm-12">
<!--Footer Column-->
<div class="footer-widget popular-posts">
<p class="widget-title">Get In Touch</p>
<!--Footer Column-->
<div class="widget-content">
<p><i class="flaticon-location-pin"></i> :  Plot No. 186, Sector-24 Faridabad, Haryana (India)</p>
<p><i class="flaticon-phone"></i> : Mr. Vinay Agnihotri  (+91-9810005354) </p>
<p><i class="flaticon-phone"></i> : Mr. Sunil Sehgal  (+91-9811121733)</p>
<p><i class="flaticon-phone"></i> : Mr. Anirudh Agnihotri (+91-8800660953)</p>
<p><i class="flaticon-envelope-of-white-paper"></i> : <a href="mailto:info@entecfurnaces.com">info@entecfurnaces.com</a></p>
</div>
</div>
</div>
<!--Footer Column-->
</div>
</div>
</div>
</div>
</div>
<!--Footer Bottom-->
<div class="footer-bottom">
<div class="auto-container">
<div class="copyright-text">Copyright © 2019 Entec Industrial Furnaces Pvt. Ltd All Rights Reserved . Website Designed &amp; SEO By Web Click India <a href="https://www.webclickindia.com/" target="_blank" title="Website Designing Company in Delhi india">Website Designing Company India</a></div>
</div>
</div>
</footer>
<!-- End Main Footer -->
</div>
<ul class="menu bottomRight">
<li class="share top"> <i class="fas fa-share-alt"></i>
<ul class="submenu">
<li><a class="facebook" href="https://www.facebook.com/EntecIndustrialFurnacesPvtLtd/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
<li><a class="twitter" href="https://twitter.com/entecfurnaces" target="_blank"><i class="fab fa-twitter"></i></a></li>
<li><a class="pinterest" href="https://in.pinterest.com/entecfurnaces/" target="_blank"><i class="fab fa-pinterest"></i></a></li>
</ul>
</li>
</ul>
<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            whatsapp: "+91-9811121733", // WhatsApp number
            call_to_action: "Message us", // Call to action
            position: "left", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->
<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div>
<script src="entec/js/jquery.js"></script>
<!--<script src="entec/js/popper.min.js"></script>-->
<script src="entec/js/bootstrap.min.js"></script>
<script src="entec/js/jquery.fancybox.js"></script>
<script src="entec/js/owl.js"></script>
<script src="entec/js/wow.js"></script>
<script src="entec/js/script.js"></script>
<script src="entec/js/mobile-menu.js"></script>
</body>
</html>