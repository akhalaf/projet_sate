<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "58951",
      cRay: "610beea41a782388",
      cHash: "1aba3063786cb18",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYWZsYW1wb3JuLmNvbS8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "dy5e6cwoP/slfZnal2wn/mE+EnCOuDk8RJo8GxjgUMQSVuCwAxMDkMQG2Zcx7cx6K1zzc/xi2cvFNLZ6G1fRBPf8O7wjSLrh81QKViQAA+eMNB6IhewE75srGXlMkGxzrIJNSUzx2M55HHf2d6c2rdSiWVA28bo+dMXkktiHe/yCEryL2mwlkAT+AbCcu9mMNXlDCT+WNF4ynE6wjI/pwjTOo9Mvc/1pXHJ/wZ4xxpQaRbC+Jchbn/PVVmpjZjpecJokzD37PPIiC/iqpQduR0OEglPOVGHqVyHAb2pnZELQiNjc0FEPhBeN3LAByUsXgNVITal1gCAI+K6AVsAyKvfNwKJE07Me4yCnm7RZvq8hZmf+duUwTNu6R3rdi9b8uFeDjXm8ifVg/9f1aV1LglQtM5DNskUXcZvAsm2MQkEbZR4SS5tjmoEsmeQBcTdL6J05GSVGO1EoxRW54hCfBnnHqozsjRPbBjvTtI4ZpTjwudcWnMRyNKLiheW5kDYRHBJbU5149dyWYobFAjQXdGPWnE/GcqxfSj0oFhG72a4XU0AC9UZJaVjIC6YjxHsJlZe/fVqTofgBjt4sgwFStAMRtNMIyHDBJksHLa0hqUpi2NTsXkgB+THKT3u7zBq/brEsxLur8FJq95gya5oEgOwbDqfpKopFWPzPzGLOSenNDcCGEtFpYZLRfhqmaiXCuPpr1xpLe2aoPAecupcjqsxta/ZqZ+QaQEklXLD+OLHGtid86lE0wNLQP8G5+24Y",
        t: "MTYxMDUwNzUxMC40MzAwMDA=",
        m: "NXZqcbbEZl/7Qw8nI5F/+LfAEaVqGiCXvhxISOcEmxA=",
        i1: "S9XA2QeiXXzMe81gkI7jLw==",
        i2: "G1tIlUek6UNSM+Qs4dQ3EA==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "38vIOEWbFd6UgIs8oEzzNNom/igY9pkggHx1AzAVTd4=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.aflamporn.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=4d86605238373c1ee8f4f8679c3676a6c575830d-1610507510-0-AS-4Xg8AMB1aPGhtVGb-8kou1DJ340QrlBo_fIxr5b0lh8-WJ5N9fmQQncFZ-EuOmNGER28bbBVmirWYktklxHsQ5B96AcfREFWQjns2Ly2Y1MeHaelC6FJJVnEa1G0LsElAQV5HMNsivQ7cDx58L_SknmCqMKM1IXc68vn-00xQNSnzj110hJ7XxAEkmMAvMEwjx1xfBd2tYnjzpnTEiordkxwJBshvjp5E8JGbT8YKYmE9OddbZrtOpBEI9-NYVyWKsVi9BOSqeNnk0ekRiG2Cz5B9SezRBMWeOsjYo1t8tF0-AlJ5Ey9lqL1_ZarLK7RjsEbJ83OJi0nDCkYcQFmWTOLKXv-FH3ME5WRnwVHdY3lZylgAOT7dgDzGi_tF1a_R29IM6ZApn49ekVa2fqhLTzXZe2aeFpVEHGX67tabfcxQ9FtTxtsuSty1irkqlzPAVUmoo28fUHKAGj5vjFFz0ebBEs0q9HpI-lA1ZgdeM7EUHQuTYyn6V-cKLnf_Iq-YiLQ-xNwFRDabRFYbguU" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="a98ca29f2059253a40d604ba1cb4ac81704039a1-1610507510-0-AYjYW01V2ZJxCwcnOG49YjpfKaK8RXOvItubX+JBEpqQSiOSB6MF18lC+1A0AX97rNrKAh7UEZUZP9M5lLX4ISAvL85cLhT4XNkIPi629m+1/b8nHMqFYHHk9jN06h/AMNVJTQdUgpc1FSG1nwrQYl/lwViYd7DR0SuBD9++egA4XefbcaNfHWF2RzMguunsbc6RFYvCoCFL6bYV04AjYs/hfYXQCPYAQto640jVS+bEi/0cyyVeiBysr2lJjXBeXizn0UE3mgQ9lBZDeMQ6Yo0KCJF8P787FQWeZ/JKWDd8KqVMllWazlbBhBVaAeei/In4paqe4FD6etUTXOvuGPuDmroAv5jnnfccHGBmYdtYcwQmarijKWNhEhuRyFqzr/WIY+jH93t2Ggcb49vBDU80eBT0D5E1tBiPmXUReTOSDhurnCSRzADc5B98AEm/XD5mt1uPIu1sakujJsuwY2d5XGUER/0bZezLqbK8QVafln//WawQfM1x/QQl6NC1Kcy1hbmsp3dG+E46pMQyC3G5URdflwFcVXdTw19qbvGyfnv0a/nye4HladWgNV/ImHwRA6rCyADjMhcIQuCSysbDmov+oEm9pTcwH2Sh+QMmh4kFQAi9Zfik5S3w788IAdwiohsTveK9YTLDFnugmh6fD98iuk7RNShjiPg+KhQx5oy/K1txrPx28aFbuBIfDLNiIqk3yNhaKJk9n1gm95NSJXDElllJZ5MY9c8I9GTAcgeGSPPloiiPs5kYNLYqoViEtS9E4ks8ZiDXLdZrWlTT4dEgr3dqDKMAFa/rUCP6B8LTw712sIGor27PZtZ4uwT5p9KxDQzlH6UF4bP7oZcB6ZDhfFZ+EdgDwtKrHCYxiynb3oLchVG913mgR/xZiSDs2PILcEbLNlWYFZjURcut/a7F8QKz/6zvSJA2ARnU17QdLtcPOgfcOW4rm/PB5MrjB9Frj9flIviH9R0LKCxKgPmPFteQLdfUxqNtxcS2CuSbL5RT9Ag11/vS/EkEN2usdpVmbNHpr8lB9knhIH73zB1qEIJbv7nykouziTUVlzbKMTIbTxIlz3MKWjvLbaeHc/PD5mXWabhfQwTe8tPOWBqt5GbTIsOEisWqtuotkFw01BYEh9h/imFNq76d5lsjZJn62AzEhMZ4ZK6IdJ17IJ2T7IGLmBPfU1HCHPsIXE/BUzJn3gAk0DekdSrMtiZe5ptkylRdZwwKlCyvLK87hGh3/lzAo3VvQSJoGuPZvMUufpcLRJo0h+bgfxVaq6f+IkNe9Fmxe2q6KX/fLarc52/CuVZg8GCFdJAwQvRD"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="d61031c92285346465b5ea835cff1a8e"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610beea41a782388')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610beea41a782388</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
