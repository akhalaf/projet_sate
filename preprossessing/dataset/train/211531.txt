<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>BestBuyIndia.com</title>
<meta content="best buy india, bestbuyindia, bbi, bestbuy, best buy, bestbuy india, electronics, televisions, iphone, android, samsung, galaxy, tablet, mobile phones, cellular phones, cell phones, cellphones, cellphone, celphone, phone" name="keywords"/>
<meta content="BestBuyIndia.com is Indias top domain for shopping" name="description"/>
<meta content="ALL" name="ROBOTS"/>
<meta content="INDEX, FOLLOW" name="GOOGLEBOT"/>
<meta content="2 weeks" name="revisit-after"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=9" http-equiv="X-UA-Compatible"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<meta content="website" property="og:type"/>
<meta content="BestBuyIndia.com" property="og:title"/>
<meta content="BestBuyIndia.com is Indias top domain for shopping" property="og:description"/>
<meta content="http://www.bestbuyindia.com" property="og:url"/>
<!-- No OG Image --> <!-- No OG Audio --> <!-- No OG Video -->
<link href="http://platform.in/images/favicon.ico" rel="shortcut icon"/>
<script language="JavaScript">
		<!--
			var jsScriptRoot = 'http://platform.in/';
		//-->
		</script>
<link href="http://platform.in/style/basic.css" rel="stylesheet" type="text/css"/>
<link href="http://platform.in/domains/themes/7/style/style.css?v=3" rel="stylesheet" type="text/css"/>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
<script language="JavaScript" src="http://platform.in/js/main_if.js" type="text/javascript"></script>
</head>
<body>
<div id="fb-root"></div>
<form action="" id="idAjaxForm" style="display:none;"></form>
<div id="divFullBody">
<div class="mobile-menu">
<button class="m-menu-close pull-right">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<div style="clear:both;height:10px;"></div>
<ul class="nav nav-stacked">
<li><a href="http://www.bestbuyindia.com"><span class="glyphicon glyphicon-home"></span> Home</a></li>
<li><a href="http://platform.in/privacy_policy.php"><span class="glyphicon glyphicon-user"></span> Privacy Policy</a></li>
<li><a href="http://platform.in/aboutus.php"><span class="glyphicon glyphicon-info-sign"></span> About Us</a></li>
<li><a href="http://platform.in/contactus.php"><span class="glyphicon glyphicon-envelope"></span> Contact Us</a></li>
</ul>
</div>
<!-- ===============================  Header Commons Ends Here ===============================  -->
<div class="Header" style="background-color: #295dfe">
<div class="container">
<div class="row">
<div class="col">
				BestBuyIndia.com is For Sale			</div>
</div>
</div>
</div>
<div class="container">
<div class="DomainMid">
<div class="row">
<div class="col-xs-12 col-sm-6">
<div class="text-center" style="padding: 2em 3em">
<h1>Complete This Form</h1><h3>To send an inquiry to the owner of BestBuyIndia.com.</h3><hr/><h3>The owner of BestBuyIndia.com has chosen to receive offer inquiries regarding this domain name.</h3><h3>Note that the owner may disregard your inquiry if your offer does not meet his or her expectations.  Lets make this your Best Buy yet!</h3> </div>
</div>
<div class="col-xs-12 col-sm-6">
<form id="idInquiryForm" onsubmit="return submitInquiryForm(this);" style="padding: 3em 2em">
<div class="form-group">
<label class="control-label" for="idField1">First Name <strong class="required">*</strong></label>
<div class="">
<input class="form-control" data-ks-validate="required|alpha_spaces" id="idField1" name="firstName" placeholder="First Name" type="text"/>
</div>
</div>
<div class="form-group">
<label class="control-label" for="idField2">Last Name <strong class="required">*</strong></label>
<div class="">
<input class="form-control" data-ks-validate="required|alpha_spaces" id="idField2" name="lastName" placeholder="Last Name" type="text"/>
</div>
</div>
<div class="form-group">
<label class="control-label" for="idField3">Email Address <strong class="required">*</strong></label>
<div class="">
<input class="form-control" data-ks-validate="required|email" id="idField3" name="email" placeholder="Email Address" type="email"/>
</div>
</div>
<div class="form-group">
<label class="control-label" for="idField4">Phone Number <strong class="required">*</strong></label>
<div class="">
<input class="form-control" data-ks-validate="required|phone" id="idField4" name="phone" placeholder="Phone Number" type="tel"/>
</div>
</div>
<div class="form-group">
<label class="control-label" for="idField5">Your Offer <strong class="required">*</strong></label>
<div class="">
<input class="form-control" data-ks-validate="required|decimal" id="idField5" name="offer" placeholder="Your Offer" type="text"/>
</div>
</div>
<div class="form-group">
<label class="control-label" for="idField6">Your inquiry message or details to your offer <strong class="required">*</strong></label>
<div class="">
<textarea class="form-control" id="idField6" name="message" placeholder="Your inquiry message or details to your offer"></textarea>
</div>
</div>
<div class="form-group">
<script src="https://www.google.com/recaptcha/api.js"></script><div class="g-recaptcha" data-sitekey="6LdaXRkUAAAAAJ21guTsI97aMjvyFvJkmpYoNOD6"></div> </div>
<div class="form-group">
<div class="text-center">
<button class="btn btn-primary btn-xl" id="idSubmitButton" style="border: none; background-color: #ff002d" type="submit">Submit</button>
<button class="btn btn-primary btn-xl" disabled="" id="idSubmittingButton" style="display:none;border:none;background-color:#ff002d" type="submit">Processing...</button>
</div>
</div>
</form>
<div class="text-center text-success" id="idInquirySent" style="display:none;padding:5em 3em">
<h2>Your Inquiry was successfully sent!<br/><br/>Thank you for your interest.</h2>
</div>
</div>
</div>
</div>
</div>
<script src="http://platform.in/js/ks-form-validations.js"></script>
<script>
	function submitInquiryForm(frm) {
		$('#idSubmitButton').hide();
		$('#idSubmittingButton').show();
		postData = buildPOST(frm);
		getContentFromUrl("inquiry-submit.php", $('#divTempConent').get(0), 'POST', postData, 'submittedInquiryForm()');

		return false;
	}
	function submittedInquiryForm() {
		$('#idSubmitButton').show();
		$('#idSubmittingButton').hide();

		response = $('#divTempConent').html();

		if ('ok' == response) {
			$('#idInquiryForm').hide();
			$('#idInquirySent').show();
		} else {
			grecaptcha.reset();
			showModalError('Error Sending Inquiry', response);
		}
	}
</script>
<div class="Footer">
<div class="container">
<div class="row">
<div class="col-xs-12 text-center">
<a href="http://platform.in/privacy_policy.php">Privacy Policy</a>
</div>
</div>
</div>
</div>
<!-- ===============================  Footer Commons Starts Here ===============================  -->
</div>
<div id="divTempConent" style="display:none;"></div>
<iframe frameborder="0" height="0" id="ifAjax" name="ifAjax" onload="ifAjaxOnLoad()" src="" style="display:none;" width="0"></iframe>
<div id="divAjax" style="display:none;">
<form id="frmAjax"></form>
</div>
</body>
<script language="JavaScript" src="http://platform.in/js/kCommons.js.php" type="text/javascript"></script>
</html>
