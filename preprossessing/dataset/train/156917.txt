<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "10769",
      cRay: "610ec75ba9a8dd0e",
      cHash: "a74070410086181",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXJ5ZGlnaXRhbC50di8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "d+O9hp95asCmMRFbCFO0pabZ0HzeIXbLCkmYxQvXJm+M4AOOcBeT8M6PBcLSXzUQO+RBUSOJoIh9+0dVdEsXQj38E+B8krFJDBjdanUGnlRgxW03SIIdyYSfxSLlN5R/XKeFStjq3osDHPOo1YkImBmmltymVuUsiBl3wVB3MlphFt6vJZGGMurHSMYAXW51cPnxnnfblXGvhWYk5zLFixqLaLxBs/w2icz/RV2AqluUi2faCOsTzXcq1ktVBbRfess8I6eimAnNH307xmPe8A9GqhDt32UnSY4FVAZyP5O/YWqoAXnM0Frylf78cX9ylGtH5tdD5NHoB5GLQ9ThoweKtDcIWayKlB7HZnPrHYVBH1upqPQKnqmhAVC2W8qNiZ8PQI9nX3mxFhHnevrnIXnkCC70+huM4mTbtOGHLfN7UoUwombwpwqzKWeaQJFhdDrH5SxpBpH8QZS5Wcv6D/Dmq7Uu+wyb3Bgge/s47pvIPInsMqtpNoc/E1dXbQ0m4YJ8efPhSDV7jxou4hU5+cPBmEpH0iqH/Vi7fHWNLt/UAMn5bBEMmxNJQ0pCxUyLU2EDmNfcBrJNeM2qjV9fWfOFtP6FVNNYUhWi+j5PZTXr/H8wuZ1nEemcWdSO4R9iN736me7GkQGXo5LN3DqvH3bLpi63K1zA1XS2MPD7OPtnPFdUiwP4nDb3Zr3OC6btwqoSiF0w6hX+be6zMgNDZkonhOgXgQ6FU9qEBuyK32KOZRKwzPZlv1fNBE2/UWvK",
        t: "MTYxMDUzNzM1OC42NjgwMDA=",
        m: "GsXrFAbELWFasmvIy6c8Euu0QPj4RVZYAnuVYpzt/7k=",
        i1: "HIlUAQc9or7+If6ro8CpoA==",
        i2: "hlAkTj40kmcTKQ82R7CgPw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "PWEDDWEfXfRe5eDkYmC4igE3ON1ZyVuMFcO81hQuxkI=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.arydigital.tv</h2>
</div><!-- /.header -->
<a href="https://madbbs.org/feminine.php?showtopic=992"><span style="display: none;">table</span></a>
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=e6cf7bdbe27886b52113bfb275d7a6156ad43a90-1610537358-0-Ad0EIxdIVtMhCDqsuS_a620G3W8iZPcav6PKjukyCHoTazQ3b0-PHQNl0vlvdut7HECCWS08ooeEQh1Sv6MZp1th88PR_GSItc9o4prWihe7C70r_7sWPB1UU2dzaLfU7WapyzRzGq2Frkuo9czUSkNPJmIaXtHHNdsfRXh2GqAgvaSVe74otG9L2EKwe1QPojK5xZmHMlHj050HrjzSQlsA9n4irJegRWRokcLtiFuDmXS8nZMUQzt7iJ6RmvAVBuExu5Zcpu8TFrM3Wy4EeajC9Avj1TYJ39rxZqKUtoEbckm16127qIKUggaVRf3M1GJKeZ1dX0ccy5shEX__x8jjGCeOXJXeNY098m29AffV58LLDDsjcBU-K5VV1aXsJOzUKcbjcaMcZG58jhmyMOaqwOwUh5jSe-A40Jh-gtpMqquwDw67evFWG75-Jo8ylHn22obpcYlm--Vo5oIfPZ5Wy56PG9mBLEwI-o-xN4k7tvxzZqxgBsnFUmZuXcYHRj9faCyQvRcB-nkXBo7VGDo" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="787ff7034f0978f81f29cbc393c4facf5630bf48-1610537358-0-AUlE3bqSJkqhDdgXe25fdUznRBb57Dzb89Pvs/flpZST26jWSLar7dUUEhmURE1T+nY5DCJIeYcRmsCDlFGGZI4i4o/obFGOH8ML0hE1l894/lXIwyoAT1ccLrVCeYlohYKjGqmDx1WPgQomDuxKn2TCjdTyi3XRQq1MbUPLHlxYE8l3+baceC7oyxxnxOT9NgFJKTfUPfKKC72EIu+QsFvbZtM5gdn/TCXF3kCUet2vHKkWCvOiGFi5ab6MTHVIEZdkteqByRFqV7qip8MHDr5SarLAkr4n6fKNXfnD8i6HuRKA7fkQo3ZBJjP7f462MG5SZpxndLVvCEtu73hffIa7bhYPG9op88UJ+SoCIt3BJB9LV93aPbTXElEYxqya1BsewBLOkxVLm8h/VGZ2RpJTMkh97IJgJDLGP3dmI3Grw6thsvG4pqpj9OSIQy+qHPz+NyHwT7hTySE0+AnhL8c8PVTPHIVsLmz198DHdAnsoXYyzoVaumZe9RmDHhxmxJpniNSmejKMl//CR+pZSQ/Cw0TpM3TTLsPjMv2xyaHOfeYG5kEpp3wsqy0zG0cCQBioZqaGDW2iHHQkKGEiIvPCOkBCS1KeIrLGyluKhKXN3UquXsRG0qxRsKX7pSAsn3SZQf98FbWAH+d1DdNTFMcQQ/nvv/33wKRdPwSPprWSoPMkNTF6B3Tki3T6S+8JnlYZEXQuPBIsYGaBml6US6llLr0yyLU1BAsVRkuWUStOzbz4lFmWMY4fqtT42qqQ6KXZxUDVejpyEKtO0OUR/SWqponQPoKmJNPP4IZCXk/JJxrHg1J3JjHsaowAI5fQxOZhCqemz48c4WnGpOFnP+wOOT9dIcXIZZuo9vL1qhrmhXhDSB4P+Y6YSSSHfH2tUB4lq4K3hKgYl8mYVdNhGB4x9N/n90JKnqDYB0m0lhqL3yS8hIjkX6gyvzaGM5KpUQhD1dquVbzfIziWkMvdofw+PFYNBifWJbMRH9+MVi32CAWhKg9iYbATLlFYnHcpuDapu/L2lGvfDZ36i4nT0Y2VvBwDQoRDxXXbx6s6uLWo2N5bEOjz6ctrsqiBxNbnLKf6l+DKxWtcL/AwvsGkD/qi6tlGH2fOT2jNCLlJMc2KIL1Mwi3mPbsXZ/p7R+QdQn+TPwwnlcFox7VPMjl5LUGCr8jGvMWbidEerkbHFcY2aLjHX1o8l9PEz6WqZgKK6Nkm6g71Tvt/Dt3gKU6pfMDvwttCV8UOR3XOtt0GWlv3JW2Kvl3/15QRNTev/B2DaA048N3hNhPv1pytjcHRO1JOVAlZhxkaz3CPNrVDcxFVDsUP/IQwb5Q3RjXiXKilLw=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="300702c289330efb4fe4391392ba46be"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610ec75ba9a8dd0e')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610ec75ba9a8dd0e</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
