<!DOCTYPE html>
<html lang="en">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-5448000-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-5448000-1');
</script>
<!-- End Google Analytics -->
<!-- Google Auto-Ads -->
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-6937604939325041",
          enable_page_level_ads: true
     });
</script>
<!-- End Google Auto-Ads -->
<link href="/favicon.ico" rel="shortcut icon"/>
<title>BigOpolis Daily Fill-it-In Word Puzzle Home</title>
<meta charset="utf-8"/>
<meta content="puzzles daily word fill in,fill-in puzzle,fill-ins,fill-it-ins,daily crossword fill in,online fill-in,word game,fill it in crossword,daily word puzzles,casual online word game,play online fill-in,crossword puzzle,print fill-in,printable fill-in,printable free fillin,number fill-in puzzle,number fillin,number fill-ins,number fill-it-ins,crosspatch,fun word game,crisscross,wordfit,big opolus,bigpolis,bigopulus,bigoppolis,obiglois,big o polis,big opolis" name="keywords"/>
<meta content="Follow, Index" name="robots"/>
<meta content="Alex Popadich" name="author"/>
<meta content="Fill-it-in puzzle word game. Fit the words into the crossword style grid. Play the interactive game or print them out. Play a game now. It's free, it's fun and it's easy. Puzzles for all to enjoy." name="description"/>
<meta content="website" property="og:type"/>
<meta content="https://www.bigopolis.com" property="og:url"/>
<meta content="BigOpolis Fill-it-In Puzzles" property="og:site_name"/>
<meta content="BigOpolis Fill-it-In Puzzles" property="og:title"/>
<meta content="Fill-it-in puzzle word game. Fit the words into the crossword style grid. Play the interactive game or print them out. Play a game now. It's free, it's fun and it's easy. Puzzles for all to enjoy." property="og:description"/>
<meta content="https://www.bigopolis.com/common/images/bigop_billboard.png" property="og:image"/>
<meta content="400" property="og:image:width"/>
<meta content="320" property="og:image:height"/>
<link href="/common/templates/style_grey_1030.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/common/css/magazine_new.css" media="screen" rel="stylesheet" type="text/css"/>
<style media="screen" type="text/css">
.fb_iframe_widget span 
{
    vertical-align: baseline !important;
}
</style>
<link href="/common/components/floatingshare/css/floating-share.css" rel="stylesheet" type="text/css"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"/>
<!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<!-- Facebook  -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- Facebook  -->
<div id="wrapper">
<div id="inner">
<div id="masthead"><h1><a href="/">BigOpolis Header</a></h1>
<ul>
<li><a href="BigOp/index.html">Home</a></li>
<li><a href="BigOp/puzzles.html">Puzzles</a></li>
<li><a href="BigOp/store/index.html">Store</a></li>
<li><a href="BigOp/about/index.html">About</a></li>
<li><a href="BigOp/faq/index.html">FAQ</a></li>
<li><a href="BigOp/contact/index.html">Contact</a></li>
<li class="primarySubscribe">
<!-- social buttons  -->
<span class="primarySubscribeLink"><div class="fb-share-button" data-href="https://www.bigopolis.com" data-layout="button_count"></div></span>
</li>
</ul>
</div><!-- close: masthead -->
<!--  BEGIN CONTENT  -->
<div id="content">
<p class="phil" data-href="http://www.bigopolis.com"><strong>BigOpolis</strong> Fill-it-In Puzzles</p>
<p><img alt="BigOpolis Fill-it-In Puzzles" class="billboard" height="320" src="/common/images/bigop_billboard.png" width="400"/></p>
<p class="phil">Fill-it-in puzzles for casual gamers.</p>
<p>Free online fill-it-in puzzles, a different puzzle every day -- Play Now!.</p>
<!-- CTA -->
<div class="playnow">
<a class="rollover" href="/BigOp/fill-it-ins/xrisxros.php" title="Play Now"><span class="displace">Play Fill-it-In Now</span></a>
</div>
<p>
Learn new words without trying, exercise your memory skills. Fun, relaxing, fill-it-in puzzles are similar to a crossword puzzle but much easier to solve. We offer a variety of fill in puzzles direct to your computer every day, just click the "Play Now" button. All puzzles have a printable version for those of you who like to put pen to paper. 
</p>
<p>
Wikipedia's description of a <a href="https://en.wikipedia.org/wiki/Fill-In_(puzzle)">fill in puzzle</a>
</p>
</div>
<!--  END CONTENT  -->
<!-- start sidebar -->
<div id="sidebar">
<div id="sponsor">
<!-- google adsense -->
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Sidebar Default Ad -->
<ins class="adsbygoogle" data-ad-client="ca-pub-6937604939325041" data-ad-slot="2715486591" style="display:inline-block;width:300px;height:250px"></ins>
<script>
		     (adsbygoogle = window.adsbygoogle || []).push({});
		</script>
<!-- google adsense -->
</div>
<!-- sidebar menu system -->
<ul>
<li class="sidenav">Puzzles</li>
<li class="sidenav"><a href="/BigOp/fill-it-ins/xrisxros.php">Daily</a></li>
<li class="sidenav"><a href="/BigOp/puzzle-archive/index.html">Sample</a></li>
<li class="sidenav"><a href="/BigOp/puzzles.html">Archive</a></li>
<li class="sidenav">Help</li>
<li class="sidenav"><a href="/BigOp/fill-it-ins/xrisxros_help.html">How to Play</a></li>
<li class="sidenav">Communication</li>
<li class="sidenav"><a href="/BigOp/faq/index.html">FAQ</a></li>
<li class="sidenav"><a href="/BigOp/contact/index.html">Contact</a></li>
<li class="sidenav">Extras</li>
<li class="sidenav"><a href="http://dmoz-odp.org/Games/Video_Games/Word_Games/Browser_Based/">Other Word Games</a></li>
<li class="sidenav"><a href="/BigOp/links.html">Links</a></li>
</ul>
<!-- big al -->
<div class="centeredsidebar">
<a href="/BigOp/links.html"><img alt="bigal pict" src="/common/images/silhouette_bigal.gif"/></a>
</div>
<p>
<span class="sideCommentNum">BigOpolis -- you know like "Metropolis".<br/>It means there's room for all kinds of stuff in here.</span>
</p>
<!-- Natures Beauty Group Flickr -->
<h1>Pictures of the Day</h1>
<div class="centeredsidebar">
<script src="https://www.flickr.com/badge_code_v2.gne?count=3&amp;source=group&amp;group=689259%40N25&amp;display=random&amp;size=s" type="text/javascript"></script>
</div>
<div id="sponsor">
</div>
<!-- BigOpolis Magazine #4-->
<h1>Fill-it-In Collection #4</h1>
<div class="centeredsidebar bookletbigop">
<span class="sideDescription">
    		Purchase BigOpolis Collection #4 an eBook of 150 fill-it-in puzzles. These are high quality PDFs, ready to print and play.
    	</span>
<span class="bookletheader">
<a class="ec_ejc_thkbx" href="https://www.e-junkie.com/ecom/gb.php?c=cart&amp;i=1664365&amp;cl=145495&amp;ejc=2" onclick="javascript:return EJEJC_lc(this);" target="ej_ejc"><img alt="Add to Cart" border="0" height="292" src="/common/images/Collection_004_cover.png" width="225"/></a>
</span>
<span class="bookletbuttonbar">
<a href="https://www.e-junkie.com/ecom/gb.php?i=1664365&amp;c=single&amp;cl=145495" target="ejejcsingle"><img alt="Buy Now" border="0" src="https://www.e-junkie.com/ej/x-click-butcc.gif"/></a>
</span>
<span class="bookletbuttonbar">
<a class="ec_ejc_thkbx" href="https://www.e-junkie.com/ecom/gb.php?c=cart&amp;i=1664365&amp;cl=145495&amp;ejc=2" onclick="javascript:return EJEJC_lc(this);" target="ej_ejc"><img alt="Add to Cart" border="0" src="https://www.e-junkie.com/ej/ej_add_to_cart.gif"/></a>
<br/>
<a class="ec_ejc_thkbx" href="https://www.e-junkie.com/ecom/gb.php?c=cart&amp;cl=145495&amp;ejc=2" onclick="javascript:return EJEJC_lc(this);" target="ej_ejc"><img alt="View Cart" border="0" src="https://www.e-junkie.com/ej/ej_view_cart.gif"/></a>
<script language="javascript" type="text/javascript">
			<!--
			function EJEJC_lc(th) { return false; }
			// -->
			</script>
<script src="https://www.e-junkie.com/ecom/box.js" type="text/javascript"></script>
</span>
<span class="bookletbuttonbar">
<p><strong>$1.19</strong></p>
</span>
<div class="bookletfooter">
<br/>
</div>
</div>
<!-- BigOpolis Magazine #4-->
</div> <!-- close sidebar -->
<!-- footer -->
<div id="footer">
<ul id="footermenu">
<li class="first"><a href="BigOp/index.html">home</a></li>
<li><a href="BigOp/puzzles.html">puzzles</a></li>
<li><a href="BigOp/about/index.html">about</a></li>
<li><a href="BigOp/faq/index.html">faq</a></li>
<li><a href="BigOp/contact/index.html">contact</a></li>
</ul>
<p class="authorinfo">design by Alex Popadich.</p>
<div class="clear"></div>
<p>© <script type="text/javascript">var d = new Date(); document.write (d.getFullYear());</script> <a href="https://www.xephyr.com" style="color:#fff;text-decoration:underline;">Xephyr Systems</a>.    <a href="/BigOp/about/privacy_policy.html" style="color:#fff;text-decoration:underline;"> Privacy Policy</a></p>
</div> <!-- close footer -->
</div> <!-- inner -->
</div> <!-- wrapper -->
<!-- Load JQuery directly from Google  -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
<script src="/common/components/floatingshare/js/jquery.floating-share.js" type="text/javascript"></script>
<script>
    $(function(){
		$("body").floatingShare({
			place: "top-left", // alternatively top-right
            counter: false, // set to false to hide counters of pinterest, facebook, twitter and linkedin
            buttons: ["facebook","twitter","google-plus","linkedin","pinterest","stumbleupon","envelope"], // all of the currently avalaible social buttons
            title: "BigOpolis Fill-it-In Puzzles", // your title, default is current page's title
            url: "http://www.bigopolis.com",  // your url, default is current page's url
            text: "share with ", // the title of a tags
            description: $("meta[name='description']").attr("content"), // your description, default is current page's description
		});
	});
    </script>
</body>
</html>
