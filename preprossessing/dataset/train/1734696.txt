<!DOCTYPE html>
<html><head>
<meta charset="utf-8"/>
<meta content="telephone=no" name="format-detection"/>
<meta content="width=device-width,user-scalable=no" name="viewport"/>
<meta content="坐车网，公交查询,公交路线查询,公交线路，公交车站查询,广州公交，深圳公交，北京公交，上海公交，乘车路线查询,地铁线路图，地铁查询，公交地铁综合换乘，城际换乘查询" name="keywords"/>
<meta content="坐车网，公交地铁综合换乘查询网站，北京、广州、上海、深圳等城市公交查询,公交车地铁综合换乘,城际换乘查询。汽车票的预订。" name="description"/>
<script>
(function(a,e){var g=window._$responsesize||0.5,f=a.documentElement,h="orientationchange"in window?"orientationchange":"resize",d=function(){var b=f.clientWidth,a=window.devicePixelRatio||1;if(b){var d=-1!=navigator.userAgent.toLowerCase().indexOf("mobile"),c=document.documentElement,e=c.clientHeight,c=c.clientWidth;d&&1<a&&e>c?(b=50+(100*(b/640)-50)*g,f.style.fontSize=b+"px"):f.style.fontSize="50px"}};a.addEventListener&&(e.addEventListener(h,d,!1),d())})(document,window);
</script>
<script>
	window._$responsesize = parseFloat("" || "0.5");
	var _purl_css = "https://z1.zuoche.cn";
	var _purl_js = "https://z1.zuoche.cn";
	var _purl_image = "https://z1.zuoche.cn";
</script>
<link href="https://z1.zuoche.cn/touch/css/lib_rem.css" rel="stylesheet"/>
<link href="https://z1.zuoche.cn/touch/css/newcommon.css?20200107" rel="stylesheet"/>
<script src="https://z1.zuoche.cn/touch/js/lib_c.js?20181017" type="text/javascript"></script>
<title>温馨提示</title>
<style>
.errorinfo {
	padding: 0 0.3rem;
	padding-top: 2rem;
	color: #666;
	text-align: center;
	min-height: 4rem;
	word-break:break-all;
	line-height: 1.4;
}
</style>
</head>
<body class="disableselect">
<section class="flexlayout zcheader intranpage">
<a class="header_btn_left" href="javascript:history.back()"></a>
<a class="flexitem_auto header_title">线路信息</a>
<a class="header_btn_right"></a>
</section>
<div class="errorinfo">找不到线路<br/></div>
<section class="footarea">
<section class="othernewfooter">
<a class="splitline" href="index.jspx">首页</a>
<a class="splitline" href="about.jspx">关于</a>
<a href="/pdfjs/web/viewer.html?file=隐私政策.pdf">隐私政策</a>
<a href="feedback.jspx">反馈</a>
</section>
</section>
<script type="text/javascript">
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?75f647144409498610c5443da6d55c0d";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
</body>
</html>