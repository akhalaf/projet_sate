<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- See CSS for credits -->
<head>
<title>AardWiki - Sundered Vale</title>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<!--HeaderText--><style type="text/css"><!--
  ul, ol, pre, dl, p { margin-top:0px; margin-bottom:0px; }
  code.escaped { white-space: nowrap; }
  .vspace { margin-top:1.33em; }
  .indent { margin-left:40px; }
  .outdent { margin-left:40px; text-indent:-40px; }
  a.createlinktext { text-decoration:none; border-bottom:1px dotted gray; }
  a.createlink { text-decoration:none; position:relative; top:-0.5em;
    font-weight:bold; font-size:smaller; border-bottom:none; }
  img { border:0px; }
  .editconflict { color:green; 
  font-style:italic; margin-top:1.33em; margin-bottom:1.33em; }

  table.markup { border:2px dotted #ccf; width:90%; }
  td.markup1, td.markup2 { padding-left:10px; padding-right:10px; }
  table.vert td.markup1 { border-bottom:1px solid #ccf; }
  table.horiz td.markup1 { width:23em; border-right:1px solid #ccf; }
  table.markup caption { text-align:left; }
  div.faq p, div.faq pre { margin-left:2em; }
  div.faq p.question { margin:1em 0 0.75em 0; font-weight:bold; }
   
    .frame 
      { border:1px solid #cccccc; padding:4px; background-color:#f9f9f9; }
    .lfloat { float:left; margin-right:0.5em; }
    .rfloat { float:right; margin-left:0.5em; }
a.varlink { text-decoration:none; }

--></style> <meta content="index,follow" name="robots"/>
<link href="http://www.aardwolf.com/wiki/pub/skins/monobook/monobook.css" rel="stylesheet" type="text/css"/>
<style type="text/css">#header { border-bottom: none; }</style>
</head>
<body>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38718183-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<div id="globalwrapper">
<!--PageLeftFmt--><div id="pageleft">
<div id="pageleftcontent">
<div class="pageleftbody" id="sidebar">
<p class="sidehead"> What Links Here
</p><div class="fpltemplate"><p class="vspace"></p><dl><dt><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Area/Area">Area</a> /</dt><dd>
</dd><dt> </dt><dd><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Area/Area">Area</a>
</dd></dl>
</div>
<p>
</p>
<p class="vspace"></p><p class="sidehead"> Aardwolf Main
</p><ul><li><a class="urllink" href="http://www.aardwolf.com" rel="nofollow">Aardwolf MUD Home</a>
</li><li><a class="urllink" href="http://www.aardwolf.com/play/index.htm" rel="nofollow">Play Aardwolf MUD</a>
</li><li><a class="urllink" href="http://www.aardwolf.com/blog" rel="nofollow">Aardwolf MUD Blog</a>
</li></ul><p class="vspace"></p><p class="sidehead"> Aardwolf Info
</p><ul><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/HomePage">Home</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/Immortals">Admin Team</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Area/Area">Areas</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Clans/Clans">Clans</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/Class">Classes</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Clients/Clients">Clients</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/Epics">Epics</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Videos/Videos">Video Tutorials</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/General">General Info</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/WhatToDoIfMudIsDown">What to do if mud is down</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Help/Help">Game Helpfiles</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Links/Links">Links to external sites</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Item/Item">Items</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/DailyBlessing">Daily Blessing</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/Enchanting">Enchanting Gear</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/Portals">Portals</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/LeadersHelp">Leaders Help</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/NewbieInfo">Newbie Info</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/Games">Games</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/Profiles">Player Profiles</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/Races">Races</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/Spells">Spells/Skills</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/Subclasses">Subclasses</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/VI">VI-Visually Impaired</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/Tiers">Tiers</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/BuildersHelp">BuildersHelp</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/HelpersHelp">HelpersHelp</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/Raiding">Raiding</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/MazeDesign">MazeDesign</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/SocialMedia">Social Media</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Clients/Tech">Client Protocols</a>
</li></ul><p class="vspace"></p><p class="sidehead"> Aardwolf Wiki
</p><ul><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/Helpfile">AardWiki Helpfile</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/Contributors">Biggest Contributors</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/FullIndex">Full Index</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/WikiSandbox">WikiSandbox</a>
</li></ul><p class="vspace"></p><p class="sidehead"> Wiki Help
</p><ul><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/PmWiki/BasicEditing">Basic Editing</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/PmWiki/DocumentationIndex">Documentation Index</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/Questions">Have a question?</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Main/AdminRequests">Admin Requests</a>
</li><li><a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Area/SunderedVale?action=logout">Log Out</a>
</li></ul><p class="vspace"></p>
</div><!-- id='sidebar' -->
<div class="pageleftbody" id="sidesearch">
<form action="http://www.aardwolf.com/wiki/index.php/Site/Search" name="searchform">
<input name="pagename" type="hidden" value="Site/Search"/>
<a href="http://www.aardwolf.com/wiki/index.php/Site/Search">Search</a>:<br/>
<input class="searchbox" name="q" type="text" value=""/>
<input class="searchbutton" type="submit" value="Go"/>
</form>
</div><!-- id='sidesearch' -->
</div><!-- id='pageleft_content' -->
<div id="pagelogo">
<a href="http://www.aardwolf.com/wiki/index.php" style="background-image: url(http://www.aardmud.org/wiki/docs/AardLogoLarge.gif);" title="AardWiki - Sundered Vale"></a>
</div><!-- id='pagelogo' -->
</div><!--/PageLeftFmt-->
<a name="topcontent"></a>
<div id="content">
<!--PageHeaderFmt--><div id="header"></div><!--/PageHeaderFmt-->
<!--PageTabsFmt--><div id="tabs"><div class="accountLink"><a href="http://www.aardwolf.com/wiki/index.php/Main/Account">Create Account</a></div>
<ul><li class="browse"><p id="active">View</p>
</li><li class="edit"><a accesskey="e" class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Area/SunderedVale?action=edit" rel="nofollow">Edit</a>
</li><li class="diff"><a accesskey="h" class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Area/SunderedVale?action=diff" rel="nofollow">History</a>
</li><li class="print"><a accesskey="" class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Area/SunderedVale?action=print" rel="nofollow">Print</a>
</li></ul>
</div><!--/PageTabsFmt-->
<div id="tabpage">
<div id="contentbody">
<!--PageTitleFmt--><h1 class="titlepage">Sundered Vale

</h1><!--/PageTitleFmt-->
<div id="rightbody">
<!--PageRightFmt--><!--/PageRightFmt-->
</div><!-- id='rightbody' -->
<!--PageText-->
<div id="wikitext">
<pre> Creator: <a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Area/AardwolfCommunity">Aardwolf Community</a>
 Level Range: 5-20
 Repop Message: An eerie silence reminds you the woodland creatures have all fled this place.
</pre><p class="vspace"></p><h2 class="For those areas that have it can be For those areas that have it can be off others but stories probably shouldn t go in comments" style="text-align: left;">Story </h2>
<pre> <strong>Help Sundered Vale's aid worker.</strong> (Designer: Dras)
</pre><p>===NEWS FLASH===NEWS FLASH===NEWS FLASH===NEWS FLASH===NEWS FLASH===
Residents of <a class="wikilink" href="http://www.aardwolf.com/wiki/index.php/Area/TheGrandCityOfAylor">Aylor</a> were awoken late last night to a rumbling from the sky,
but this was no ordinary thunderstorm rolling through!  The whole sky lit
up, in what members of the Vampire community are calling "The brightest
night in our long and spooky memories."  An outspoken leader of the Dwarf
community had this to report: "I was working on a tunnel and there was all
of a sudden this shaking and I thought I was a goner for sure!  But then it
stopped, and I came up to see if anyone else had noticed, but everyone was
just pointing to the south, spouting some nonsense about a ball of fire and
smoke."  We'll keep you up to date as we learn more.
</p>
<p class="vspace"></p><h2>Goals.</h2>
<p>An aid worker has traveled from Aylor to the Vale to help out. Unfortunately,
she doesn't seem to be able to find her way through the destruction. Travel
to the Sundered Vale and see if you can help.
</p>
<p class="vspace"></p><pre>             Level Range         : 5 to 20
             Goal Difficulty     : Easy
             Goal Recommended at : Level 5
</pre><p class="vspace"></p><p>Area added March 6, 2008.
</p>
<p class="vspace"></p><h2>Comments</h2>
<h2>Speedwalks</h2>
<p>Runto Sundered
</p>
<p class="vspace"></p><h2>External Links</h2>
<ul><li><a class="attachlink" href="http://www.aardwolf.com/wiki/uploads/VI%20Accessible%20Map%20of%20Sundered%20Vale.xls" rel="nofollow">VI Accessible Map of Sundered Vale.xls</a><a class="createlink" href="http://www.aardwolf.com/wiki/index.php/Area/SunderedVale?action=upload&amp;upname=VI Accessible Map of Sundered Vale.xls"><img src="http://www.aardwolf.com/wiki/pub/skins/monobook/attachment.png"/></a>
</li><li><a class="urllink" href="http://emeraldknights.net/PublicMaps/sunderedvale.gif" rel="nofollow" target="_blank">Emerald's Sundered Vale Map</a>
</li><li><a class="urllink" href="http://maps.gaardian.com/index.php?areaid=253" rel="nofollow" target="_blank">Gaardian's text searchable map of Sundered Vale</a> (<a class="urllink" href="http://maps.gaardian.com/vi-index.php?areaid=253" rel="nofollow" target="_blank">VI accessible version</a>)
</li></ul>
</div>
<span style="clear:both;"></span>
</div><!-- id='contentbody' -->
</div><!-- id='tabpage' -->
<!--PageFooterFmt--><div id="footer">
<div class="lastmod">Page last modified on March 13, 2019, at 08:26 PM EST</div>
<ul>
<li class="navbox"><a href="http://www.aardwolf.com/wiki/index.php/Area/SunderedVale#topcontent">▲ Top ▲</a></li>
<li><a href="http://www.aardwolf.com/wiki/index.php/Site/Search">Search</a></li>
<li><a href="http://www.aardwolf.com/wiki/index.php/Area/RecentChanges">Recent Changes</a></li>
<li><a href="http://www.aardwolf.com/wiki/index.php/Site/AllRecentChanges">All Recent Changes</a></li>
</ul>
</div><!--/PageFooterFmt-->
</div><!-- id='content' -->
</div><!-- id='globalwrapper' -->
</body>
</html>
