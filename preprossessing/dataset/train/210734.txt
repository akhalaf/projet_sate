<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<style type="text/css">
</style>
<link href="../css/artist2.css" rel="stylesheet" type="text/css"/>
<title>Bert Christensen's Cyberspace Home, Clarence Alphonse Gagnon, Laurentian Village</title>
</head>
<body style="color: #AFA4B6">
<div class="leftpanel" style="background-color: #AFA4B6; ">
<div class="masthead">
	Bert Christensen's<br/>
	Cyberspace Home</div>
<div class="navigation">
<a href="index.htm">Menu</a><br/>
<br/>
<a href="../../index.html">Home</a><br/>
<br/>
<a href="gallen-Kallela.htm">Next Painting</a></div>
</div>
<div class="artistname">
		Clarence Alphonse Gagnon</div>
<div class="nationality">
	Canadian</div>
<div class="dates">
	1881  - 1942</div>
<div class="pixtitle">
			"Laurentian Village"</div>
<div class="pixbox">
<img alt="Laurentian Village" height="468" src="images/gagnon.jpg" width="600"/></div>
</body>
</html>
