<!DOCTYPE html>
<html lang="es">
<head itemscope="" itemtype="http://schema.org/WebSite">
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<title>Consulado Honorario de Portugal en Córdoba</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://consuladoptcba.com/feed/" rel="alternate" title="Consulado Honorario de Portugal en Córdoba » Feed" type="application/rss+xml"/>
<link href="https://consuladoptcba.com/comments/feed/" rel="alternate" title="Consulado Honorario de Portugal en Córdoba » RSS de los comentarios" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/consuladoptcba.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.16"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://consuladoptcba.com/wp-content/plugins/blossomthemes-email-newsletter/public/css/blossomthemes-email-newsletter-public.min.css?ver=2.0.0" id="blossomthemes-email-newsletter-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://consuladoptcba.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.4" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://consuladoptcba.com/wp-content/themes/blossom-feminine/css/owl.carousel.min.css?ver=2.2.1" id="owl-carousel-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://consuladoptcba.com/wp-content/themes/blossom-feminine/css/animate.min.css?ver=3.5.2" id="animate-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C300%2C300italic%2Cregular%2Citalic%2C500%2C500italic%2C700%2C700italic%2C900%2C900italic%7CParisienne%3Aregular" id="blossom-feminine-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://consuladoptcba.com/wp-content/themes/blossom-feminine/style.css?ver=1.1.7" id="blossom-feminine-style-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://consuladoptcba.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://consuladoptcba.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://consuladoptcba.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://consuladoptcba.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://consuladoptcba.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.16" name="generator"/>
<link href="https://consuladoptcba.com/" rel="canonical"/>
<link href="https://consuladoptcba.com/" rel="shortlink"/>
<link href="https://consuladoptcba.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fconsuladoptcba.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://consuladoptcba.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fconsuladoptcba.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<link href="https://consuladoptcba.com/wp-content/uploads/2018/12/cropped-portugal-2-e1544192989515-1-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://consuladoptcba.com/wp-content/uploads/2018/12/cropped-portugal-2-e1544192989515-1-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://consuladoptcba.com/wp-content/uploads/2018/12/cropped-portugal-2-e1544192989515-1-180x180.png" rel="apple-touch-icon-precomposed"/>
<meta content="https://consuladoptcba.com/wp-content/uploads/2018/12/cropped-portugal-2-e1544192989515-1-270x270.png" name="msapplication-TileImage"/>
<style media="all" type="text/css">     
    .content-newsletter .blossomthemes-email-newsletter-wrapper.bg-img:after,
    .widget_blossomthemes_email_newsletter_widget .blossomthemes-email-newsletter-wrapper:after{
        background: rgba(221, 51, 51, 0.8);    }
    
    /* primary color */
    a{
    	color: #dd3333;
    }
    
    a:hover,
    a:focus{
    	color: #dd3333;
    }

    .secondary-nav ul li a:hover,
    .secondary-nav ul li a:focus,
    .secondary-nav ul li:hover > a,
    .secondary-nav ul li:focus > a,
    .secondary-nav .current_page_item > a,
    .secondary-nav .current-menu-item > a,
    .secondary-nav .current_page_ancestor > a,
    .secondary-nav .current-menu-ancestor > a,
    .header-t .social-networks li a:hover,
    .header-t .social-networks li a:focus,
    .main-navigation ul li a:hover,
    .main-navigation ul li a:focus,
    .main-navigation ul li:hover > a,
    .main-navigation ul li:focus > a,
    .main-navigation .current_page_item > a,
    .main-navigation .current-menu-item > a,
    .main-navigation .current_page_ancestor > a,
    .main-navigation .current-menu-ancestor > a,
    .banner .banner-text .cat-links a:hover,
    .banner .banner-text .cat-links a:focus,
    .banner .banner-text .title a:hover,
    .banner .banner-text .title a:focus,
    #primary .post .text-holder .entry-header .entry-title a:hover,
    #primary .post .text-holder .entry-header .entry-title a:focus,
    .widget ul li a:hover,
    .widget ul li a:focus,
    .site-footer .widget ul li a:hover,
    .site-footer .widget ul li a:focus,
    #crumbs a:hover,
    #crumbs a:focus,
    .related-post .post .text-holder .cat-links a:hover,
    .related-post .post .text-holder .cat-links a:focus,
    .related-post .post .text-holder .entry-title a:hover,
    .related-post .post .text-holder .entry-title a:focus,
    .comments-area .comment-body .comment-metadata a:hover,
    .comments-area .comment-body .comment-metadata a:focus,
    .search #primary .search-post .text-holder .entry-header .entry-title a:hover,
    .search #primary .search-post .text-holder .entry-header .entry-title a:focus,
    .site-title a:hover,
    .site-title a:focus,
    .widget_bttk_popular_post ul li .entry-header .entry-meta a:hover,
    .widget_bttk_popular_post ul li .entry-header .entry-meta a:focus,
    .widget_bttk_pro_recent_post ul li .entry-header .entry-meta a:hover,
    .widget_bttk_pro_recent_post ul li .entry-header .entry-meta a:focus,
    .widget_bttk_popular_post .style-two li .entry-header .cat-links a,
    .widget_bttk_pro_recent_post .style-two li .entry-header .cat-links a,
    .widget_bttk_popular_post .style-three li .entry-header .cat-links a,
    .widget_bttk_pro_recent_post .style-three li .entry-header .cat-links a,
    .widget_bttk_posts_category_slider_widget .carousel-title .title a:hover,
    .widget_bttk_posts_category_slider_widget .carousel-title .title a:focus,
    .site-footer .widget_bttk_posts_category_slider_widget .carousel-title .title a:hover,
    .site-footer .widget_bttk_posts_category_slider_widget .carousel-title .title a:focus,
    .portfolio-sorting .button:hover,
    .portfolio-sorting .button:focus,
    .portfolio-sorting .button.is-checked,
    .portfolio-item .portfolio-img-title a:hover,
    .portfolio-item .portfolio-img-title a:focus,
    .portfolio-item .portfolio-cat a:hover,
    .portfolio-item .portfolio-cat a:focus,
    .entry-header .portfolio-cat a:hover,
    .entry-header .portfolio-cat a:focus{
        color: #dd3333;
    }

    .category-section .col .img-holder .text-holder,
    .pagination a{
        border-color: #dd3333;
    }

    .category-section .col .img-holder .text-holder span,
    #primary .post .text-holder .entry-footer .btn-readmore:hover,
    #primary .post .text-holder .entry-footer .btn-readmore:focus,
    .pagination a:hover,
    .pagination a:focus,
    .widget_calendar caption,
    .widget_calendar table tbody td a,
    .widget_tag_cloud .tagcloud a:hover,
    .widget_tag_cloud .tagcloud a:focus,
    #blossom-top,
    .single #primary .post .entry-footer .tags a:hover,
    .single #primary .post .entry-footer .tags a:focus,
    .error-holder .page-content a:hover,
    .error-holder .page-content a:focus,
    .widget_bttk_author_bio .readmore:hover,
    .widget_bttk_author_bio .readmore:focus,
    .widget_bttk_social_links ul li a:hover,
    .widget_bttk_social_links ul li a:focus,
    .widget_bttk_image_text_widget ul li .btn-readmore:hover,
    .widget_bttk_image_text_widget ul li .btn-readmore:focus,
    .widget_bttk_custom_categories ul li a:hover .post-count,
    .widget_bttk_custom_categories ul li a:hover:focus .post-count,
    .content-instagram ul li .instagram-meta .like,
    .content-instagram ul li .instagram-meta .comment,
    #secondary .widget_blossomtheme_featured_page_widget .text-holder .btn-readmore:hover,
    #secondary .widget_blossomtheme_featured_page_widget .text-holder .btn-readmore:focus,
    #secondary .widget_blossomtheme_companion_cta_widget .btn-cta:hover,
    #secondary .widget_blossomtheme_companion_cta_widget .btn-cta:focus,
    #secondary .widget_bttk_icon_text_widget .text-holder .btn-readmore:hover,
    #secondary .widget_bttk_icon_text_widget .text-holder .btn-readmore:focus,
    .site-footer .widget_blossomtheme_companion_cta_widget .btn-cta:hover,
    .site-footer .widget_blossomtheme_companion_cta_widget .btn-cta:focus,
    .site-footer .widget_blossomtheme_featured_page_widget .text-holder .btn-readmore:hover,
    .site-footer .widget_blossomtheme_featured_page_widget .text-holder .btn-readmore:focus,
    .site-footer .widget_bttk_icon_text_widget .text-holder .btn-readmore:hover,
    .site-footer .widget_bttk_icon_text_widget .text-holder .btn-readmore:focus{
        background: #dd3333;
    }

    .pagination .current,
    .post-navigation .nav-links .nav-previous a:hover,
    .post-navigation .nav-links .nav-next a:hover,
    .post-navigation .nav-links .nav-previous a:focus,
    .post-navigation .nav-links .nav-next a:focus{
        background: #dd3333;
        border-color: #dd3333;
    }

    #primary .post .entry-content blockquote,
    #primary .page .entry-content blockquote{
        border-bottom-color: #dd3333;
        border-top-color: #dd3333;
    }

    #primary .post .entry-content .pull-left,
    #primary .page .entry-content .pull-left,
    #primary .post .entry-content .pull-right,
    #primary .page .entry-content .pull-right{border-left-color: #dd3333;}

    .error-holder .page-content h2{
        text-shadow: 6px 6px 0 #dd3333;
    }

    
    body,
    button,
    input,
    select,
    optgroup,
    textarea{
        font-family : Georgia, serif;
        font-size   : 16px;
    }

    .banner .banner-text .title,
    #primary .sticky .text-holder .entry-header .entry-title,
    #primary .post .text-holder .entry-header .entry-title,
    .author-section .text-holder .title,
    .post-navigation .nav-links .nav-previous .post-title,
    .post-navigation .nav-links .nav-next .post-title,
    .related-post .post .text-holder .entry-title,
    .comments-area .comments-title,
    .comments-area .comment-body .fn,
    .comments-area .comment-reply-title,
    .page-header .page-title,
    #primary .post .entry-content blockquote,
    #primary .page .entry-content blockquote,
    #primary .post .entry-content .pull-left,
    #primary .page .entry-content .pull-left,
    #primary .post .entry-content .pull-right,
    #primary .page .entry-content .pull-right,
    #primary .post .entry-content h1,
    #primary .page .entry-content h1,
    #primary .post .entry-content h2,
    #primary .page .entry-content h2,
    #primary .post .entry-content h3,
    #primary .page .entry-content h3,
    #primary .post .entry-content h4,
    #primary .page .entry-content h4,
    #primary .post .entry-content h5,
    #primary .page .entry-content h5,
    #primary .post .entry-content h6,
    #primary .page .entry-content h6,
    .search #primary .search-post .text-holder .entry-header .entry-title,
    .error-holder .page-content h2,
    .widget_bttk_author_bio .title-holder,
    .widget_bttk_popular_post ul li .entry-header .entry-title,
    .widget_bttk_pro_recent_post ul li .entry-header .entry-title,
    .widget_bttk_posts_category_slider_widget .carousel-title .title,
    .content-newsletter .blossomthemes-email-newsletter-wrapper .text-holder h3,
    .widget_blossomthemes_email_newsletter_widget .blossomthemes-email-newsletter-wrapper .text-holder h3,
    #secondary .widget_bttk_testimonial_widget .text-holder .name,
    #secondary .widget_bttk_description_widget .text-holder .name,
    .site-footer .widget_bttk_description_widget .text-holder .name,
    .site-footer .widget_bttk_testimonial_widget .text-holder .name,
    .portfolio-text-holder .portfolio-img-title,
    .portfolio-holder .entry-header .entry-title,
    .single-blossom-portfolio .post-navigation .nav-previous a,
    .single-blossom-portfolio .post-navigation .nav-next a,
    .related-portfolio-title{
        font-family: Roboto;
    }

    .site-title{
        font-size   : 40px;
        font-family : Parisienne;
        font-weight : 400;
        font-style  : normal;
    }
    
               
    </style> <style id="wp-custom-css" type="text/css">
			.site-info{
    display:none;
}		</style>
</head>
<body class="home page-template-default page page-id-26 wp-custom-logo full-width" itemscope="" itemtype="http://schema.org/WebPage">
<div class="site" id="page">
<header class="site-header wow fadeIn" data-wow-delay="0.1s" id="masthead" itemscope="" itemtype="http://schema.org/WPHeader">
<div class="header-t">
<div class="container">
<div id="secondary-toggle-button"><i class="fa fa-bars"></i></div>
<nav class="secondary-nav" id="secondary-navigation" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
</nav><!-- #secondary-navigation -->
<div class="right">
<div class="tools">
<div class="form-section">
<span id="btn-search"><i class="fa fa-search"></i></span>
<div class="form-holder">
<form action="https://consuladoptcba.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Buscar:</span>
<input class="search-field" name="s" placeholder="Buscar …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Buscar"/>
</form> </div>
</div>
</div>
<ul class="social-networks">
<li><a href="https://www.facebook.com/" rel="nofollow" target="_blank"><i class="fa fa-facebook"></i></a></li>
<li><a href="http://youtube.com" rel="nofollow" target="_blank"><i class="fab fa-youtube"></i></a></li>
<li><a href="https://www.instagram.com/" rel="nofollow" target="_blank"><i class="fa fa-instagram"></i></a></li>
</ul>
</div>
</div>
</div><!-- .header-t -->
<div class="header-m site-branding">
<div class="container" itemscope="" itemtype="http://schema.org/Organization">
<a class="custom-logo-link" href="https://consuladoptcba.com/" itemprop="url" rel="home"><img alt="Consulado Honorario de Portugal en Córdoba" class="custom-logo" height="184" itemprop="logo" sizes="(max-width: 250px) 100vw, 250px" src="https://consuladoptcba.com/wp-content/uploads/2018/12/cropped-portugal-2-e1544192989515.png" srcset="https://consuladoptcba.com/wp-content/uploads/2018/12/cropped-portugal-2-e1544192989515.png 250w, https://consuladoptcba.com/wp-content/uploads/2018/12/cropped-portugal-2-e1544192989515-82x60.png 82w" width="250"/></a> <h1 class="site-title" itemprop="name"><a href="https://consuladoptcba.com/" itemprop="url" rel="home">Consulado Honorario de Portugal en Córdoba</a></h1>
</div>
</div><!-- .header-m -->
<div class="header-b">
<div class="container">
<div id="primary-toggle-button"><i class="fa fa-bars"></i>Navegación</div>
<nav class="main-navigation" id="site-navigation" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
<div class="menu-menu-principal-container"><ul class="menu" id="primary-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-21" id="menu-item-21"><a href="http://consuladoptcba.com">Inicio</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-26 current_page_item menu-item-28" id="menu-item-28"><a href="https://consuladoptcba.com/">Cónsul Honorario de Portugal en Córdoba</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31" id="menu-item-31"><a href="https://consuladoptcba.com/nuestras-oficinas/">Nuestras oficinas</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-39" id="menu-item-39"><a href="https://consuladoptcba.com/contacto/">Contacto</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-59" id="menu-item-59"><a href="https://consuladoptcba.com/category/noticias/">Noticias</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79" id="menu-item-79"><a href="https://consuladoptcba.com/tramites/">Tramites</a></li>
</ul></div> </nav><!-- #site-navigation -->
</div>
</div><!-- .header-b -->
</header><!-- #masthead -->
<div class="banner layout-one wow fadeIn" data-wow-delay="0.1s">
<div class="owl-carousel" id="banner-slider">
<div class="item">
<img alt="" class="attachment-blossom-feminine-slider size-blossom-feminine-slider wp-post-image" height="649" sizes="(max-width: 1032px) 100vw, 1032px" src="https://consuladoptcba.com/wp-content/uploads/2018/12/FAdeA-01-2-1032x649.jpg" srcset="https://consuladoptcba.com/wp-content/uploads/2018/12/FAdeA-01-2-1032x649.jpg 1032w, https://consuladoptcba.com/wp-content/uploads/2018/12/FAdeA-01-2-320x200.jpg 320w" width="1032"/>
<div class="banner-text">
<span class="cat-links" itemprop="about"><a href="https://consuladoptcba.com/category/noticias/" rel="category tag">Noticias</a></span><h2 class="title"><a href="https://consuladoptcba.com/2018/12/18/segunda-visita-del-sr-embajador-a-cordoba-6-y-7-de-noviembre-2017/">Segunda visita del Sr. Embajador a Córdoba (6 y 7 de noviembre/2017)</a></h2> </div>
</div>
<div class="item">
<img alt="" class="attachment-blossom-feminine-slider size-blossom-feminine-slider wp-post-image" height="547" sizes="(max-width: 1136px) 100vw, 1136px" src="https://consuladoptcba.com/wp-content/uploads/2018/12/Heterónimos-II-2.jpg" srcset="https://consuladoptcba.com/wp-content/uploads/2018/12/Heterónimos-II-2.jpg 1136w, https://consuladoptcba.com/wp-content/uploads/2018/12/Heterónimos-II-2-300x144.jpg 300w, https://consuladoptcba.com/wp-content/uploads/2018/12/Heterónimos-II-2-768x370.jpg 768w, https://consuladoptcba.com/wp-content/uploads/2018/12/Heterónimos-II-2-1024x493.jpg 1024w, https://consuladoptcba.com/wp-content/uploads/2018/12/Heterónimos-II-2-125x60.jpg 125w" width="1136"/>
<div class="banner-text">
<span class="cat-links" itemprop="about"><a href="https://consuladoptcba.com/category/noticias/" rel="category tag">Noticias</a></span><h2 class="title"><a href="https://consuladoptcba.com/2018/12/17/heteronimos-navegantes-de-um-mesmo-corpo-abril-2016/">HETERONIMOS – Navegantes de um mesmo corpo (abril/2016)</a></h2> </div>
</div>
<div class="item">
<img alt="" class="attachment-blossom-feminine-slider size-blossom-feminine-slider wp-post-image" height="649" src="https://consuladoptcba.com/wp-content/uploads/2018/12/Inauguración-3-e1545080473731.jpg" width="973"/>
<div class="banner-text">
<span class="cat-links" itemprop="about"><a href="https://consuladoptcba.com/category/noticias/" rel="category tag">Noticias</a></span><h2 class="title"><a href="https://consuladoptcba.com/2018/12/17/inauguracion-exhibicion-de-arte-04-11-2015/">Inauguración – Exhibición de arte 04/11/2015</a></h2> </div>
</div>
<div class="item">
<img alt="" class="attachment-blossom-feminine-slider size-blossom-feminine-slider wp-post-image" height="649" src="https://consuladoptcba.com/wp-content/uploads/2018/12/Mundo-en-las-Escuelas-2017-1-1024x649.jpg" width="1024"/>
<div class="banner-text">
<span class="cat-links" itemprop="about"><a href="https://consuladoptcba.com/category/noticias/" rel="category tag">Noticias</a></span><h2 class="title"><a href="https://consuladoptcba.com/2018/12/17/el-mundo-en-las-escuelas-desde-2014/">El Mundo en las Escuelas (desde 2014)</a></h2> </div>
</div>
</div>
</div>
<div class="container main-content">
<div class="site-content" id="content">
<div class="row">
<div class="content-area" id="primary">
<main class="site-main" id="main">
<article class="post-26 page type-page status-publish hentry" id="post-26">
<div class="post-thumbnail"></div>
<div class="text-holder">
<div class="entry-content" itemprop="text">
<div class="text"><h2><img alt="" class="size-medium wp-image-27 alignleft" height="300" sizes="(max-width: 184px) 100vw, 184px" src="http://consuladoptcba.com/wp-content/uploads/2018/12/consulado-1-184x300.jpg" srcset="https://consuladoptcba.com/wp-content/uploads/2018/12/consulado-1-184x300.jpg 184w, https://consuladoptcba.com/wp-content/uploads/2018/12/consulado-1-37x60.jpg 37w, https://consuladoptcba.com/wp-content/uploads/2018/12/consulado-1.jpg 465w" width="184"/></h2>
<p> </p>
</div> </div><!-- .entry-content -->
<footer class="entry-footer">
</footer><!-- .entry-footer home-->
</div><!-- .text-holder -->
</article><!-- #post-26 -->
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- .row/not-found -->
</div><!-- #content -->
</div><!-- .container/.main-content -->
<footer class="site-footer" id="colophon" itemscope="" itemtype="http://schema.org/WPFooter">
<div class="site-info">
<div class="container">
<span class="copyright">2021 Copyright  <a href="https://consuladoptcba.com/">Consulado Honorario de Portugal en Córdoba</a>. </span><a href="https://blossomthemes.com/themes/blossom-feminine-free-wordpress-theme" rel="author" target="_blank"> Blossom Feminine</a>por Blossom Themes.Funciona con <a href="https://wordpress.org/" target="_blank">WordPress</a> .                    
		</div>
</div>
</footer><!-- #colophon -->
<div id="blossom-top">
<span><i class="fa fa-angle-up"></i>ARRIBA</span>
</div>
</div><!-- #page -->
<script type="text/javascript">
/* <![CDATA[ */
var bten_ajax_data = {"ajaxurl":"https:\/\/consuladoptcba.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script defer="defer" src="https://consuladoptcba.com/wp-content/plugins/blossomthemes-email-newsletter/public/js/blossomthemes-email-newsletter-public.min.js?ver=2.0.0" type="text/javascript"></script>
<script src="https://consuladoptcba.com/wp-content/plugins/blossomthemes-email-newsletter/public/js/all.min.js?ver=5.6.3" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/consuladoptcba.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://consuladoptcba.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.4" type="text/javascript"></script>
<script src="https://consuladoptcba.com/wp-content/themes/blossom-feminine/js/v4-shims.min.js?ver=5.2.0" type="text/javascript"></script>
<script src="https://consuladoptcba.com/wp-content/themes/blossom-feminine/js/sticky-kit.min.js?ver=1.1.3" type="text/javascript"></script>
<script src="https://consuladoptcba.com/wp-content/themes/blossom-feminine/js/owl.carousel.min.js?ver=2.2.1" type="text/javascript"></script>
<script src="https://consuladoptcba.com/wp-content/themes/blossom-feminine/js/jquery.matchHeight.min.js?ver=0.7.2" type="text/javascript"></script>
<script src="https://consuladoptcba.com/wp-content/themes/blossom-feminine/js/wow.min.js?ver=1.1.3" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var blossom_feminine_data = {"rtl":"","animation":"fadeOutLeft"};
/* ]]> */
</script>
<script src="https://consuladoptcba.com/wp-content/themes/blossom-feminine/js/custom.min.js?ver=1.1.7" type="text/javascript"></script>
<script src="https://consuladoptcba.com/wp-includes/js/wp-embed.min.js?ver=4.9.16" type="text/javascript"></script>
</body>
</html>
