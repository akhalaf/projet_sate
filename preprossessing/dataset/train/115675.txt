<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-US" prefix="og: http://ogp.me/ns#">
<![endif]--><!--[if IE 8]>
<html class="ie ie8" lang="en-US" prefix="og: http://ogp.me/ns#">
<![endif]--><!--[if !(IE 7) | !(IE 8)  ]><!--><html lang="en-US" prefix="og: http://ogp.me/ns#">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<link href="https://american-automobiles.com/xmlrpc.php" rel="pingback"/>
<title>G - Manufacturers - American Automobiles</title>
<style type="text/css">		#content table, #content tr td {
    border: none
}		body p, body ul, body ol, body li, body dl, body address, body table { font-size: 17px; }</style>
<!-- This site is optimized with the Yoast SEO Premium plugin v8.3 - https://yoast.com/wordpress/plugins/seo/ -->
<link href="https://american-automobiles.com/g-manufacturers/" rel="canonical"/>
<meta content="en_US" property="og:locale"/>
<meta content="article" property="og:type"/>
<meta content="G - Manufacturers - American Automobiles" property="og:title"/>
<meta content="Click on the Red links below to learn more about these American Automobile Manufacturers! American Automobile Name / Manufacturers Name Gaeth – Gaeth Motor Car Co. Cleveland, OH 1902-1911 Gale – Western Tool Works Galesburg, IL 1904-1910 Galloway – The...Read more" property="og:description"/>
<meta content="https://american-automobiles.com/g-manufacturers/" property="og:url"/>
<meta content="American Automobiles" property="og:site_name"/>
<meta content="https://www.facebook.com/AmericanAutomobiles" property="article:publisher"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Click on the Red links below to learn more about these American Automobile Manufacturers! American Automobile Name / Manufacturers Name Gaeth – Gaeth Motor Car Co. Cleveland, OH 1902-1911 Gale – Western Tool Works Galesburg, IL 1904-1910 Galloway – The...Read more" name="twitter:description"/>
<meta content="G - Manufacturers - American Automobiles" name="twitter:title"/>
<meta content="https://www.american-automobiles.com/images/Great-Western-1.jpg" name="twitter:image"/>
<script type="application/ld+json">{"@context":"https:\/\/schema.org","@type":"Organization","url":"https:\/\/american-automobiles.com\/","sameAs":["https:\/\/www.facebook.com\/AmericanAutomobiles"],"@id":"https:\/\/american-automobiles.com\/#organization","name":"Farber and Associates LLC","logo":""}</script>
<!-- / Yoast SEO Premium plugin. -->
<link href="//apis.google.com" rel="dns-prefetch"/>
<link href="//connect.facebook.net" rel="dns-prefetch"/>
<link href="//platform.twitter.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://american-automobiles.com/feed/" rel="alternate" title="American Automobiles » Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/american-automobiles.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://american-automobiles.com/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://american-automobiles.com/wp-content/themes/happenstance-premium/css/colors/red.css?ver=5.4.4" id="happenstance-style-red-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://american-automobiles.com/wp-content/themes/happenstance-premium/css/elegantfont.css?ver=5.4.4" id="happenstance-elegantfont-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://american-automobiles.com/wp-content/themes/happenstance-premium/style.css?ver=5.4.4" id="happenstance-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Oswald&amp;subset=latin%2Clatin-ext&amp;ver=5.4.4" id="happenstance-google-font-default-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://american-automobiles.com/wp-content/themes/happenstance-premium/functions/fe/wp-tab-widget/css/wp-tab-widget.css?ver=5.4.4" id="happenstance-tab-widget-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://american-automobiles.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://american-automobiles.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='https://american-automobiles.com/wp-content/themes/happenstance-premium/js/html5.js?ver=3.6'></script>
<![endif]-->
<link href="https://american-automobiles.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://american-automobiles.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://american-automobiles.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.4" name="generator"/>
<link href="https://american-automobiles.com/?p=65" rel="shortlink"/>
<link href="https://american-automobiles.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Famerican-automobiles.com%2Fg-manufacturers%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://american-automobiles.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Famerican-automobiles.com%2Fg-manufacturers%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-689647-37"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-689647-37');
</script>
<script async="" data-ad-client="ca-pub-8665185188886016" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<meta content="G – Manufacturers" property="og:title"/>
<meta content="American Automobiles" property="og:site_name"/>
<meta content="https://american-automobiles.com/g-manufacturers/" property="og:url"/>
<meta content="article" property="og:type"/><style type="text/css">/* =Responsive Map fix
-------------------------------------------------------------- */
.happenstance_map_canvas img {
	max-width: none;
}</style><!--[if IE]>
<style type="text/css" media="screen">
#container-shadow, .attachment-post-thumbnail, .attachment-square-thumb {
        behavior: url("https://american-automobiles.com/wp-content/themes/happenstance-premium/css/pie/PIE.php");
        zoom: 1;
}
</style>
<![endif]-->
</head>
<body class="page-template-default page page-id-65" data-rsssl="1" id="wrapper">
<div class="pattern"></div>
<div id="container">
<div id="container-shadow">
<header id="header">
<div id="ticker-wrapper">
<div class="ticker-box">
<div class="ticker-arrow-1"></div>
<div class="ticker-arrow-2"></div>
<ul id="ticker">
</ul>
</div>
</div>
<div class="header-content-wrapper">
<div class="header-content">
<p class="site-title"><a href="https://american-automobiles.com/">American Automobiles</a></p>
<p class="site-description">The American Automobiles and American Automobile Manufacturers</p>
<form action="https://american-automobiles.com/" id="searchform" method="get">
<div class="searchform-wrapper"><input id="s" name="s" placeholder="Search here..." type="text" value=""/>
<input class="send icon_search" name="searchsubmit" type="submit" value="U"/></div>
</form> </div>
</div>
<div class="menu-box-container">
<div class="menu-box-wrapper">
<div class="menu-box">
<a class="link-home" href="https://american-automobiles.com/"><i aria-hidden="true" class="icon_house"></i></a>
<div class="menu-menu-container"><ul class="menu" id="nav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-9" id="menu-item-9"><a href="https://american-automobiles.com/">Home – American Automobiles</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1400" id="menu-item-1400"><a href="https://american-automobiles.com/affiliate-disclosure/">Affiliate Disclosure</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1405" id="menu-item-1405"><a href="https://american-automobiles.com/privacy-policy/">Privacy Policy</a></li>
</ul></div> </div>
</div>
</div>
<div class="header-image">
<img alt="American Automobiles" class="header-img" src="https://american-automobiles.com/wp-content/uploads/2019/01/B.jpg"/>
</div>
</header> <!-- end of header -->
<div id="wrapper-content">
<div id="main-content">
<div class="hentry" id="content"> <div class="content-headline">
<h1 class="entry-headline"><span class="entry-headline-text">G – Manufacturers</span></h1>
</div>
<div class="entry-content">
<p>Click on the Red links below to learn more about these American Automobile Manufacturers!<br/>
American Automobile Name / Manufacturers Name</p>
<p><font face="Verdana" size="2"><b><a href="Gaeth.html">Gaeth</a></b> – Gaeth Motor Car Co. Cleveland, OH 1902-1911<br/>
<b>Gale</b> – Western Tool Works Galesburg, IL 1904-1910<br/>
<b><a href="Galloway.html">Galloway</a></b> – The William Galloway Co. Waterloo, IA 1905-1917<br/>
<b><a href="Gardner.html">Gardner</a></b> – Gardner Motor Co Inc. St. Louis, MO 1919-1931<br/>
<b><a href="Garford.html">Garford</a></b> – Garford Mfg. Co. Elyria, OH 1906-1912; 1916<br/>
<b><a href="Gary.html">Gary</a></b> – Gary Automobile Mfg. Co. Gary, IN 1916-1917<br/>
<b>Gaslight</b> – Gaslight Motors Corp. Detroit, MI 1960-c.1961<br/>
<b><a href="Gasmobile.html">Gasmobile</a></b> – Automobile Company of America Marion, NJ 1900-1902<br/>
<b><a href="Gatts.html">Gatts</a></b> – Alfred P. Gatts Brown Country, OH 1905<br/>
<b><a href="Gaylord.html">Gaylord</a> (1)</b> – Gaylord Cars Ltd. Chicago, IL 1955-1956<br/>
<b><a href="Gaylord-2.html">Gaylord</a> (2)</b> – Gaylord Motor Car Co. Gaylord, MI 1910-1913<br/>
<b><a href="Gearless.html">Gearless</a></b> – Gearless Transmission Co. Rochester, NY 1907-1908<br/>
<b><a href="Gearless.html">Gearless</a></b> – Gearless Motor Car Co. Rochester, NY 1908-1909<br/>
<a href="Gem.html"><b>Gem</b> </a>– Gem Motor Car Co. Jackson, MI and Grand Rapids, MI 1917-1919<br/>
<b><a href="General.html">General</a></b> (see <a href="Hansen.html">Hansen</a>)<br/>
<b><a href="Genesee.html">Genesee</a></b> – Genesee Motor Co. Batavia, NY 1912<br/>
<b><a href="Geneva.html">Geneva</a></b> – Schoeneck Co. Chicago, IL 1916-1917<br/>
<b>Genie</b> – British Motor Car Importers San Francisco, CA 1962 (to date)<br/>
<b><a href="German-American.html">German-American</a></b> – German-American Automobile Co. New York, NY 1902<br/>
<b>Geronimo</b> – Geronimo Motor Co. Enid, OK 1917-1920<br/>
<b><a href="Ghent.html">Ghent</a></b> – Ghent Motor Co. Ottawa, IL 1917-1918<br/>
<b><a href="Gibson.html">Gibson</a></b> – C. D. P. Gibson Jersey City, NJ 1899<br/>
<b>Gillette</b> (see Amplex)<br/>
<b><a href="GJG.html">G. J. G.</a></b> – G. J. G. Motor Car Co. White Plains, NY 1909-1911<br/>
<b>Glassic</b> – Glassic Industries Inc. West Palm Beach, FL 1966-1968<br/>
<b>Gleason</b> – Kansas City Vehicle Co. Kansas City, MO 1909-1914<br/>
<b><a href="Glide.html">Glide</a></b> – The Bartholomew Co. Peoria, IL 1903-1920<br/>
<b><a href="Globe.html">Globe</a></b> – Globe Motors Co. Cleveland, OH 1921-1922<br/>
<b><a href="Golden-Gate.html">Golden Gate</a></b> – A. Schilling &amp; Sons San Francisco, CA 1894-1895<br/>
<b><a href="Goodspeed.html">Goodspeed</a></b> – Commonwealth Motors Co. Chicago, IL 1922<br/>
<b><a href="Gove.html">Gove</a></b> – Gove Motor Truck Co. Detroit, MI 1921<br/>
<b>Graham-Fox</b> (see Compound)<br/>
<b><a href="Graham-Motorette.html">Graham Motorette</a></b> – Charles Sefrin &amp; Co. Brooklyn, NY 1902-1903<br/>
<b>Graham-Paige, Graham</b> – Graham-Paige Motors Corp. Detroit, MI 1927-1941<br/>
<b>Grant</b> – Grant Motor Car Corp. Findlay, OH 1913-1922<br/>
<b>Graves &amp; Congdon</b> (see Crown)<br/>
<b><a href="Gray.html">Gray</a></b> – Gray Motor Corp. Detroit, MI 1922-1926<br/>
<b>Great Eagle</b> – U.S. Carriage Co. Columbus, OH 1910-1918<br/>
<b><a href="Great-Smith.html">Great Smith</a></b> The Smith Automobile Co. Topeka, Kansas 1907-1911<br/>
<b>Great Southern</b> – Great Southern Automobile Co. Birmingham, AL 1910-1914<br/>
<b>Great Western</b> – Model Automobile Co. Peru, IN 1908-1909<br/>
<b>Great Western</b> – Great Western Automobile Co. Peru, IN 1909-1916<br/>
<b><a href="Greeley.html">Greeley</a></b> – E. N. Miller Greeley, CO 1903<br/>
<b><a href="Greenleaf.html">Greenleaf</a></b> – Greenleaf Cycle Co. Lansing, MI 1902<br/>
<b>Gregory</b> – Front Drive Motor Co. Kansas City, MO 1918-1922<br/>
<b>Gregory</b> – Ben Gregory Kansas, City, MO 1949-1952<br/>
<b>Gremlin</b> (see American Motors)<br/>
<b>Greuter</b> (see Holyoke)<br/>
<b>Griffith-TVR, Griffith</b> – Griffith Motors Syosset, Long Island, NY 1964-1965<br/>
<b>Griffith-TVR, Griffith</b> – Griffith Motors Plainview, Long Island, NY 1965-1968<br/>
<b><a href="Griswold.html">Griswold</a></b> – Griswold Motor Car Co. Detroit, MI 1907<br/>
<b><a href="Gurley.html">Gurley</a></b> – T. W. Gurley Meyersdale, PA 1901<br/>
<b><a href="Guy-Vaughan.html">Guy Vaughan</a></b> – Vaughan Car Co. Kingston, NY 1910-1913<br/>
<b><a href="Gyroscope.html">Gyroscope</a></b> – Blomstrom Mfg. Co. Detroit, MI 1908<br/>
<b><a href="Gyroscope.html">Gyroscope</a></b> – Lion Motor Car Co. Adrian, MI 1909 </font></p>
<p></p><center><br/>
<img src="https://www.american-automobiles.com/images/Great-Western-1.jpg"/><br/>
<b>1912 Great Western 40</b><br/>
</center>
<div class="social-share">
<fb:like href="https://american-automobiles.com/g-manufacturers/" layout="button_count" send="true" show_faces="true" width="200"></fb:like>
<a class="twitter-share-button" data-url="https://american-automobiles.com/g-manufacturers/" href="http://twitter.com/share">Tweet</a>
<g:plusone href="https://american-automobiles.com/g-manufacturers/" size="medium"></g:plusone>
</div>
</div>
</div> <!-- end of content -->
<aside id="sidebar">
<div class="sidebar-widget facebook-like-box" id="happenstancehead-2"> <p class="sidebar-headline"><span class="sidebar-headline-text">American Automobiles Facebook</span></p><div class="fb-like-box-wrapper"><div class="fb-like-box" data-colorscheme="light" data-header="false" data-height="" data-href="https://www.facebook.com/AmericanAutomobiles/" data-show-border="false" data-show-faces="false" data-stream="false" data-width="352"></div></div></div><div class="sidebar-widget widget_nav_menu" id="nav_menu-2"> <p class="sidebar-headline"><span class="sidebar-headline-text">Site Navigation</span></p><div class="menu-menu-1-container"><ul class="menu" id="menu-menu-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-11" id="menu-item-11"><a href="https://american-automobiles.com/">Home – American Automobiles</a></li>
</ul></div></div><div class="sidebar-widget widget_nav_menu" id="nav_menu-3"> <p class="sidebar-headline"><span class="sidebar-headline-text">American Automobiles by Manufacturer</span></p><div class="menu-menu-2-container"><ul class="menu" id="menu-menu-2"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20" id="menu-item-20"><a href="https://american-automobiles.com/a-manufacturers/">A – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-43" id="menu-item-43"><a href="https://american-automobiles.com/b-manufacturers/">B – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48" id="menu-item-48"><a href="https://american-automobiles.com/c-manufacturers/">C – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52" id="menu-item-52"><a href="https://american-automobiles.com/d-manufacturers/">D – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-60" id="menu-item-60"><a href="https://american-automobiles.com/e-manufacturers/">E – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64" id="menu-item-64"><a href="https://american-automobiles.com/f-manufacturers/">F – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-65 current_page_item menu-item-67" id="menu-item-67"><a aria-current="page" href="https://american-automobiles.com/g-manufacturers/">G – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71" id="menu-item-71"><a href="https://american-automobiles.com/h-manufacturers/">H – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-76" id="menu-item-76"><a href="https://american-automobiles.com/i-manufacturers/">I – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-80" id="menu-item-80"><a href="https://american-automobiles.com/j-manufacturers/">J – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-83" id="menu-item-83"><a href="https://american-automobiles.com/k-manufacturers/">K – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-86" id="menu-item-86"><a href="https://american-automobiles.com/l-manufacturers/">L – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-89" id="menu-item-89"><a href="https://american-automobiles.com/m-manufacturers/">M – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-92" id="menu-item-92"><a href="https://american-automobiles.com/n-manufacturers/">N – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-95" id="menu-item-95"><a href="https://american-automobiles.com/o-manufacturers/">O – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-100" id="menu-item-100"><a href="https://american-automobiles.com/p-manufacturers/">P – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-101" id="menu-item-101"><a href="https://american-automobiles.com/q-manufacturers/">Q – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-104" id="menu-item-104"><a href="https://american-automobiles.com/r-manufacturers/">R – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-107" id="menu-item-107"><a href="https://american-automobiles.com/s-manufacturers/">S – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-111" id="menu-item-111"><a href="https://american-automobiles.com/t-manufacturers/">T – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-114" id="menu-item-114"><a href="https://american-automobiles.com/u-manufacturers/">U – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-117" id="menu-item-117"><a href="https://american-automobiles.com/v-manufacturers/">V – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-120" id="menu-item-120"><a href="https://american-automobiles.com/w-manufacturers/">W – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-123" id="menu-item-123"><a href="https://american-automobiles.com/x-manufacturers/">X – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-127" id="menu-item-127"><a href="https://american-automobiles.com/y-manufacturers/">Y – Manufacturers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-130" id="menu-item-130"><a href="https://american-automobiles.com/z-manufacturers/">Z – Manufacturers</a></li>
</ul></div></div><div class="widget_text sidebar-widget widget_custom_html" id="custom_html-3"> <p class="sidebar-headline"><span class="sidebar-headline-text">FREE STUFF</span></p><div class="textwidget custom-html-widget"><!-- CSS Code -->
<style scoped="" type="text/css">
table.GeneratedTable {
width:100%;
background-color:#FFFFFF;
border-collapse:collapse;border-width:3px;
border-color:#FF0700;
border-style:solid;
color:#000000;
}

table.GeneratedTable td, table.GeneratedTable th {
border-width:3px;
border-color:#FF0700;
border-style:solid;
padding:3px;
}

table.GeneratedTable thead {
background-color:#FF0700;
}
</style>
<!-- HTML Code -->
<table class="GeneratedTable">
<tbody>
<tr>
<td><h2 class="" data-css="tve-u-166a0265a5f"><strong><a href="https://51classicchevy.com/free-woodworking/" rel="noopener noreferrer" target="_blank">Free Download</a> - Get 50 Woodworking Plans &amp; a 440-Page Guide Book Absolutely FREE!</strong></h2>
<h2 class="" data-css="tve-u-166a0265a5f"><strong><a href="https://51classicchevy.com/free-keto-plan/" rel="noopener noreferrer" target="_blank">Custom Keto Meal Plan</a> For Your Target Weight</strong></h2>
<h2 class="" data-css="tve-u-166a0265a5f"><strong><a href="https://51classicchevy.com/free-expert-secrets-book/" rel="noopener noreferrer" target="_blank">Get Paid for Your Advice!</a> Free "Expert Secrets" Book Plus 5 Bonuses!*</strong></h2>
*The book is FREE + a small shipping fee.<br/><br/></td>
</tr>
</tbody>
</table></div></div></aside> <!-- end of sidebar --> </div> <!-- end of main-content -->
</div> <!-- end of wrapper-content -->
<footer id="wrapper-footer">
<div class="widget_text footer-signature"><div class="widget_text footer-signature-content"><div class="textwidget custom-html-widget"><center>
This site is © Copyright Farber and Associates, LLC All Rights Reserved    
<br/>
<b>NOTICE:</b> American-Automobiles.com is a participant in the eBay Partner Network (EPN). When you click on links to various merchants on this site and make a purchase, this can result in this site earning a commission. Affiliate programs and affiliations include, but are not limited to, the eBay Partner Network. Please visit our <a href="https://american-automobiles.com/affiliate-disclosure">Affiliate Disclosure</a> page for more information.</center></div></div></div></footer> <!-- end of wrapper-footer -->
</div> <!-- end of container-shadow -->
</div> <!-- end of container -->
<script src="//apis.google.com/js/plusone.js?ver=5.4.4" type="text/javascript"></script>
<script src="//connect.facebook.net/en_US/all.js?ver=5.4.4#xfbml=1" type="text/javascript"></script>
<script src="//platform.twitter.com/widgets.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://american-automobiles.com/wp-content/themes/happenstance-premium/js/caroufredsel.js?ver=6.2.1" type="text/javascript"></script>
<script src="https://american-automobiles.com/wp-content/themes/happenstance-premium/js/caroufredsel-settings.js?ver=1.0" type="text/javascript"></script>
<script src="https://american-automobiles.com/wp-content/themes/happenstance-premium/js/flexslider.js?ver=2.2.2" type="text/javascript"></script>
<script src="https://american-automobiles.com/wp-content/themes/happenstance-premium/js/flexslider-settings.js?ver=2.2.0" type="text/javascript"></script>
<script src="https://american-automobiles.com/wp-content/themes/happenstance-premium/js/placeholders.js?ver=2.0.8" type="text/javascript"></script>
<script src="https://american-automobiles.com/wp-content/themes/happenstance-premium/js/scroll-to-top.js?ver=1.0" type="text/javascript"></script>
<script src="https://american-automobiles.com/wp-content/themes/happenstance-premium/js/menubox.js?ver=1.0" type="text/javascript"></script>
<script src="https://american-automobiles.com/wp-content/themes/happenstance-premium/js/selectnav.js?ver=0.1" type="text/javascript"></script>
<script src="https://american-automobiles.com/wp-content/themes/happenstance-premium/js/responsive.js?ver=1.0" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpt = {"ajax_url":"https:\/\/american-automobiles.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script src="https://american-automobiles.com/wp-content/themes/happenstance-premium/functions/fe/wp-tab-widget/js/wp-tab-widget.js?ver=1.0" type="text/javascript"></script>
<script src="https://american-automobiles.com/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
</body>
</html>
<!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/


Served from: american-automobiles.com @ 2021-01-13 05:39:25 by W3 Total Cache
-->