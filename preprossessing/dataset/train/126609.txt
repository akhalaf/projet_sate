<!DOCTYPE html>
<html dir="ltr" lang="en" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article# book: http://ogp.me/ns/book# profile: http://ogp.me/ns/profile# video: http://ogp.me/ns/video# product: http://ogp.me/ns/product# content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" name="viewport"/>
<meta content="Drupal 7 (http://drupal.org)" name="generator"/>
<link href="https://www.ans.iastate.edu/" rel="canonical"/>
<link href="https://www.ans.iastate.edu/" rel="shortlink"/>
<meta content="Department of Animal Science" property="og:site_name"/>
<meta content="https://www.ans.iastate.edu/luggage-default-404-page" property="og:url"/>
<meta content="Page not found" property="og:title"/>
<title>Page not found | Department of Animal Science</title>
<style media="all" type="text/css">
@import url("https://www.ans.iastate.edu/modules/system/system.base.css?qlrehw");
@import url("https://www.ans.iastate.edu/modules/system/system.menus.css?qlrehw");
@import url("https://www.ans.iastate.edu/modules/system/system.messages.css?qlrehw");
@import url("https://www.ans.iastate.edu/modules/system/system.theme.css?qlrehw");
</style>
<style media="all" type="text/css">
@import url("https://www.ans.iastate.edu/modules/aggregator/aggregator.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/modules/calendar/css/calendar_multiday.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/modules/date/date_api/date.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/modules/date/date_repeat_field/date_repeat_field.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/modules/environment_indicator/environment_indicator.css?qlrehw");
@import url("https://www.ans.iastate.edu/modules/field/theme/field.css?qlrehw");
@import url("https://www.ans.iastate.edu/modules/node/node.css?qlrehw");
@import url("https://www.ans.iastate.edu/modules/search/search.css?qlrehw");
@import url("https://www.ans.iastate.edu/modules/user/user.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/modules/views/css/views.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/modules/luggage/luggage_ckeditor/ckeditor/css/ckeditor.css?qlrehw");
</style>
<style media="all" type="text/css">
@import url("https://www.ans.iastate.edu/sites/all/modules/ctools/css/ctools.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/modules/panels/css/panels.css?qlrehw");
</style>
<link href="//cdn.theme.iastate.edu/font-awesome/css/font-awesome.css" media="all" rel="stylesheet" type="text/css"/>
<link href="//cdn.theme.iastate.edu/nimbus-sans/css/nimbus-sans.css" media="all" rel="stylesheet" type="text/css"/>
<style media="all" type="text/css">
@import url("https://www.ans.iastate.edu/sites/all/themes/omega/alpha/css/alpha-reset.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/themes/omega/alpha/css/alpha-mobile.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/themes/omega/alpha/css/alpha-alpha.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/css/formalize.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/themes/omega/omega/css/omega-text.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/themes/omega/omega/css/omega-menu.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/themes/omega/omega/css/omega-forms.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/themes/omega/omega/css/omega-visuals.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/css/suitcase.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/css/suitcase_responsive.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/css/sm-core-css.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/css/sm-menu-toggle.css?qlrehw");
</style>
<style media="all" type="text/css">
@import url("https://www.ans.iastate.edu/sites/all/modules/custom/ans/ans.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/modules/luggage/luggage_bean_adventure/css/luggage_bean_adventure.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/modules/luggage/luggage_bean_card/css/luggage_bean_card.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/modules/luggage/luggage_bean_hero/css/luggage_bean_hero.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/modules/luggage/luggage_bean_hero_ex/css/luggage_bean_hero_ex.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/modules/luggage/luggage_bean_menu/css/luggage_bean_menu.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/modules/luggage/luggage_bean_stat/css/luggage_bean_stat.css?qlrehw");
@import url("https://www.ans.iastate.edu/sites/all/modules/luggage/luggage_bean_video/css/luggage_bean_video.css?qlrehw");
</style>
<!--[if (lt IE 9)&(!IEMobile)]>
<style type="text/css" media="all">
@import url("https://www.ans.iastate.edu/sites/all/themes/omega/alpha/css/grid/alpha_fluid/normal/alpha-fluid-normal-12.css?qlrehw");
</style>
<![endif]-->
<!--[if gte IE 9]><!-->
<style media="all and (min-width: 740px)" type="text/css">
@import url("https://www.ans.iastate.edu/sites/all/themes/omega/alpha/css/grid/alpha_fluid/normal/alpha-fluid-normal-12.css?qlrehw");
</style>
<!--<![endif]-->
<!--[if lt IE 9]><script src="https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/js/html5shiv.min.js"></script><![endif]-->
<script src="https://www.ans.iastate.edu/misc/jquery.js?v=1.4.4" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/misc/jquery-extend-3.4.0.js?v=1.4.4" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/misc/jquery-html-prefilter-3.5.0-backport.js?v=1.4.4" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/misc/jquery.once.js?v=1.2" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/misc/drupal.js?qlrehw" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/sites/all/modules/environment_indicator/tinycon.min.js?qlrehw" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/sites/all/modules/environment_indicator/environment_indicator.js?qlrehw" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/sites/all/modules/environment_indicator/color.js?qlrehw" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/sites/all/modules/matomo/matomo.js?qlrehw" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var _paq = _paq || [];(function(){var u=(("https:" == document.location.protocol) ? "https://trends.ent.iastate.edu/" : "http://trends.ent.iastate.edu/");_paq.push(["setSiteId", "148"]);_paq.push(["setTrackerUrl", u+"piwik.php"]);_paq.push(["setDocumentTitle", "404/URL = " + encodeURIComponent(document.location.pathname+document.location.search) + "/From = " + encodeURIComponent(document.referrer)]);_paq.push(["trackPageView"]);_paq.push(["setIgnoreClasses", ["no-tracking","colorbox"]]);_paq.push(["enableLinkTracking"]);var d=document,g=d.createElement("script"),s=d.getElementsByTagName("script")[0];g.type="text/javascript";g.defer=true;g.async=true;g.src=u+"piwik.js";s.parentNode.insertBefore(g,s);})();
//--><!]]>
</script>
<script src="https://www.ans.iastate.edu/sites/all/modules/luggage/luggage_bean_menu/js/luggage_bean_menu.js?qlrehw" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/js/jquery.formalize.js?qlrehw" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/js/suitcase_facet.js?qlrehw" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/js/suitcase_sticky_menu.js?qlrehw" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/js/jquery.smartmenus.min.js?qlrehw" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/js/jquery.smartmenus.keyboard.min.js?qlrehw" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/js/suitcase_smart_menu.js?qlrehw" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/js/suitcase_image_floats.js?qlrehw" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/js/suitcase_responsive_tables.js?qlrehw" type="text/javascript"></script>
<script src="https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/js/suitcase_external_links.js?qlrehw" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"suitcase_interim","theme_token":"PGi1WSxxLus4xCK1f1wohGDdmwQd100luJdfX4lmwPw","js":{"sites\/all\/modules\/luggage\/luggage_announcements\/js\/flexslider.load.js":1,"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery-html-prefilter-3.5.0-backport.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/environment_indicator\/tinycon.min.js":1,"sites\/all\/modules\/environment_indicator\/environment_indicator.js":1,"sites\/all\/modules\/environment_indicator\/color.js":1,"sites\/all\/modules\/matomo\/matomo.js":1,"0":1,"sites\/all\/modules\/luggage\/luggage_bean_menu\/js\/luggage_bean_menu.js":1,"sites\/all\/themes\/suitcase_interim\/js\/jquery.formalize.js":1,"sites\/all\/themes\/suitcase_interim\/js\/suitcase_facet.js":1,"sites\/all\/themes\/suitcase_interim\/js\/suitcase_sticky_menu.js":1,"sites\/all\/themes\/suitcase_interim\/js\/jquery.smartmenus.min.js":1,"sites\/all\/themes\/suitcase_interim\/js\/jquery.smartmenus.keyboard.min.js":1,"sites\/all\/themes\/suitcase_interim\/js\/suitcase_smart_menu.js":1,"sites\/all\/themes\/suitcase_interim\/js\/suitcase_image_floats.js":1,"sites\/all\/themes\/suitcase_interim\/js\/suitcase_responsive_tables.js":1,"sites\/all\/themes\/suitcase_interim\/js\/suitcase_external_links.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/aggregator\/aggregator.css":1,"sites\/all\/modules\/calendar\/css\/calendar_multiday.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/modules\/date\/date_repeat_field\/date_repeat_field.css":1,"sites\/all\/modules\/environment_indicator\/environment_indicator.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/luggage\/luggage_ckeditor\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"\/\/cdn.theme.iastate.edu\/font-awesome\/css\/font-awesome.css":1,"\/\/cdn.theme.iastate.edu\/nimbus-sans\/css\/nimbus-sans.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-reset.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-mobile.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-alpha.css":1,"sites\/all\/themes\/suitcase_interim\/css\/formalize.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-text.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-menu.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-forms.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-visuals.css":1,"sites\/all\/themes\/suitcase_interim\/css\/suitcase.css":1,"sites\/all\/themes\/suitcase_interim\/css\/suitcase_responsive.css":1,"sites\/all\/themes\/suitcase_interim\/css\/sm-core-css.css":1,"sites\/all\/themes\/suitcase_interim\/css\/sm-menu-toggle.css":1,"sites\/all\/modules\/custom\/ans\/ans.css":1,"sites\/all\/modules\/luggage\/luggage_bean_adventure\/css\/luggage_bean_adventure.css":1,"sites\/all\/modules\/luggage\/luggage_bean_card\/css\/luggage_bean_card.css":1,"sites\/all\/modules\/luggage\/luggage_bean_hero\/css\/luggage_bean_hero.css":1,"sites\/all\/modules\/luggage\/luggage_bean_hero_ex\/css\/luggage_bean_hero_ex.css":1,"sites\/all\/modules\/luggage\/luggage_bean_menu\/css\/luggage_bean_menu.css":1,"sites\/all\/modules\/luggage\/luggage_bean_stat\/css\/luggage_bean_stat.css":1,"sites\/all\/modules\/luggage\/luggage_bean_video\/css\/luggage_bean_video.css":1,"ie::normal::sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_fluid\/normal\/alpha-fluid-normal-12.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_fluid\/normal\/alpha-fluid-normal-12.css":1}},"matomo":{"trackMailto":1},"urlIsAjaxTrusted":{"\/clubs\/judging\/dairy\/dairy_history.html":true}});
//--><!]]>
</script>
</head>
<body class="html not-front not-logged-in page-luggage-default-404-page context-luggage-default-404-page">
<div class="page clearfix" id="page">
<header class="section section-header" id="section-header" role="banner">
<div id="section-header-container">
<div id="skip-link">
<a class="element-invisible element-focusable" href="#main-content">Skip to main content</a>
</div>
<div class="zone-wrapper zone-isu-menu-wrapper clearfix" id="zone-isu-menu-wrapper"> <div class="zone zone-isu-menu clearfix container-12" id="zone-isu-menu">
<div class="grid-12 region region-isu-menu" id="region-isu-menu">
<div class="region-inner region-isu-menu-inner">
<nav aria-labelledby="isu-index isu-quick-links" class="navigation" id="isu-menu-nav">
<h2 class="element-invisible" id="isu-index">ISU Index Menu</h2>
<ul class="sm" data-sm-options="{ subIndicatorsText: '', subMenusMinWidth: '', subMenusMaxWidth: '', subIndicatorsPos: 'append' }" id="isu-index-menu">
<li><a href="https://www.iastate.edu" title="Iowa State University Home Page">iastate.edu</a></li>
<li>
<a href="https://www.iastate.edu/index/A">Index</a>
<ul>
<li><a href="https://www.iastate.edu/index/A/">A</a></li>
<li><a href="https://www.iastate.edu/index/B/">B</a></li>
<li><a href="https://www.iastate.edu/index/C/">C</a></li>
<li><a href="https://www.iastate.edu/index/D/">D</a></li>
<li><a href="https://www.iastate.edu/index/E/">E</a></li>
<li><a href="https://www.iastate.edu/index/F/">F</a></li>
<li><a href="https://www.iastate.edu/index/G/">G</a></li>
<li><a href="https://www.iastate.edu/index/H/">H</a></li>
<li><a href="https://www.iastate.edu/index/I/">I</a></li>
<li><a href="https://www.iastate.edu/index/J/">J</a></li>
<li><a href="https://www.iastate.edu/index/K/">K</a></li>
<li><a href="https://www.iastate.edu/index/L/">L</a></li>
<li><a href="https://www.iastate.edu/index/M/">M</a></li>
<li><a href="https://www.iastate.edu/index/N/">N</a></li>
<li><a href="https://www.iastate.edu/index/O/">O</a></li>
<li><a href="https://www.iastate.edu/index/P/">P</a></li>
<li><a href="https://www.iastate.edu/index/Q/">Q</a></li>
<li><a href="https://www.iastate.edu/index/R/">R</a></li>
<li><a href="https://www.iastate.edu/index/S/">S</a></li>
<li><a href="https://www.iastate.edu/index/T/">T</a></li>
<li><a href="https://www.iastate.edu/index/U/">U</a></li>
<li><a href="https://www.iastate.edu/index/V/">V</a></li>
<li><a href="https://www.iastate.edu/index/W/">W</a></li>
<li><a href="https://www.iastate.edu/index/X/">X</a></li>
<li><a href="https://www.iastate.edu/index/Y/">Y</a></li>
<li><a href="https://www.iastate.edu/index/Z/">Z</a></li>
</ul>
</li>
</ul>
<h2 class="element-invisible" id="isu-quick-links">ISU Quick Links Menu</h2>
<ul class="sm" data-sm-options="{ subIndicatorsText: '', subMenusMinWidth: '', subMenusMaxWidth: '', subIndicatorsPos: 'append' }" id="isu-quick-links-menu">
<li><a href="https://info.iastate.edu/">Directory</a></li>
<li><a href="https://www.fpm.iastate.edu/maps/">Maps</a></li>
<li><a href="https://web.iastate.edu/safety/">Safety</a></li>
<li data-sm-reverse="true">
<a href="https://web.iastate.edu/signons">Sign Ons</a>
<ul>
<li><a href="https://accessplus.iastate.edu/">AccessPlus</a></li>
<li><a href="https://canvas.iastate.edu/">Canvas</a></li>
<li><a href="https://iastate.box.com/">CyBox</a></li>
<li><a href="http://cymail.iastate.edu/">CyMail</a></li>
<li><a href="https://outlook.iastate.edu/">Outlook</a></li>
<li><a href="https://web.iastate.edu/signons">More Sign Ons...</a></li>
</ul>
</li>
</ul>
</nav>
</div>
</div>
</div>
</div><div class="zone-wrapper zone-branding-wrapper clearfix" id="zone-branding-wrapper"> <div class="zone zone-branding clearfix container-12" id="zone-branding">
<div class="grid-6 region region-branding" id="region-branding">
<div class="region-inner region-branding-inner">
<a href="https://www.iastate.edu/" id="isu_header_wordmark" title="Iowa State University Homepage"><img alt="Iowa State University" src="https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/images/isu.svg"/></a>
<span class="site-name-level-2"><a href="https://www.cals.iastate.edu">College of Agriculture and Life Sciences</a></span>
<hr/>
<div class="site-name-slogan">
<span class="site-name-level-3"><a href="/" title="Home">Department of Animal Science</a></span>
</div>
</div>
</div>
<div class="grid-6 region region-search" id="region-search">
<div class="region-inner region-search-inner">
<div class="search-form-container has-site-name-level-2 has-site-name-level-3"><div class="block block-search block-form block-search-form odd block-without-title" id="block-search-form">
<div class="block-inner clearfix">
<div class="content clearfix">
<form accept-charset="UTF-8" action="/clubs/judging/dairy/dairy_history.html" id="search-block-form" method="post" role="search"><div><div class="container-inline">
<h2 class="element-invisible">Search form</h2>
<div class="form-item form-type-textfield form-item-search-block-form">
<label class="element-invisible" for="edit-search-block-form--2">Search </label>
<input class="form-text" id="edit-search-block-form--2" maxlength="128" name="search_block_form" placeholder="Search" size="15" title="Enter the terms you wish to search for." type="text" value=""/>
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input class="form-submit" id="edit-submit" name="op" type="submit" value="  "/></div><input name="form_build_id" type="hidden" value="form-iyS9RzT7qnBOiamxu0A-XeeOXt2kLS5C-UPsLZXeAIA"/>
<input name="form_id" type="hidden" value="search_block_form"/>
</div>
</div></form> </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div><div class="zone-wrapper zone-menu-wrapper clearfix" id="zone-menu-wrapper"> <div class="zone zone-menu clearfix container-12" id="zone-menu">
<div class="grid-12 region region-menu" id="region-menu">
<div class="region-inner region-menu-inner">
<nav aria-labelledby="main-menu-label" class="navigation" id="main-menu-nav">
<h2 class="element-invisible" id="main-menu-label">Main menu</h2>
<!-- Mobile menu toggle button (hamburger/x icon) -->
<input class="sm-menu-state" id="main-menu-state" type="checkbox"/>
<label class="sm-menu-btn" for="main-menu-state">
<span class="sm-menu-btn-icon"></span> Main Menu
      </label>
<ul class="sm" data-sm-options='{ subIndicatorsText: "+", subMenusMinWidth: "", subMenusMaxWidth: "", subIndicatorsPos: "append" }' id="main-menu"><li class="first leaf"><a href="/about/welcome" title="">About Us</a></li>
<li class="leaf"><a href="/students" title="">Students</a></li>
<li class="leaf"><a href="/people/faculty" title="">People</a></li>
<li class="leaf"><a href="/projects" title="">Research</a></li>
<li class="collapsed"><a href="/farms">Farms</a></li>
<li class="leaf"><a href="/extension">Extension &amp; Outreach</a></li>
<li class="last leaf"><a href="/seminars">Seminars</a></li>
</ul>
</nav>
</div>
</div>
</div>
</div><div class="zone-wrapper zone-secondary-menu-wrapper clearfix" id="zone-secondary-menu-wrapper"> <div class="zone zone-secondary-menu clearfix container-12" id="zone-secondary-menu">
<div class="grid-12 region region-secondary-menu" id="region-secondary-menu">
<div class="region-inner region-secondary-menu-inner">
</div>
</div> </div>
</div> </div>
</header>
<section aria-label="main content" class="section section-content" id="section-content">
<div class="zone-wrapper zone-content-wrapper clearfix" id="zone-content-wrapper"> <div class="zone zone-content clearfix container-12" id="zone-content">
<div class="grid-12" id="breadcrumb"><h2 class="element-invisible">You are here</h2><div class="breadcrumb"><a href="/">Home</a></div></div>
<main class="grid-12 region region-content" id="region-content">
<div class="region-inner region-content-inner">
<a id="main-content"></a>
<h1 class="title" id="page-title">Page not found</h1>
<div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
<div class="block-inner clearfix">
<div class="content clearfix">
      The requested page "/clubs/judging/dairy/dairy_history.html" could not be found.    </div>
</div>
</div>
</div>
</main>
</div>
</div></section>
<footer class="section section-footer" id="section-footer" role="contentinfo">
<div class="zone-wrapper zone-footer-wrapper clearfix" id="zone-footer-wrapper"> <div class="zone zone-footer clearfix container-12" id="zone-footer">
<div class="grid-3 region region-footer-first" id="region-footer-first">
<div class="region-inner region-footer-first-inner">
<div class="block block-block block-3 block-block-3 odd block-without-title" id="block-block-3">
<div class="block-inner clearfix">
<div class="content clearfix">
<p><a href="https://www.iastate.edu" id="isu_footer_wordmark"><img alt="Iowa State University" src="https://www.ans.iastate.edu/sites/all/themes/suitcase_interim/images/isu_stacked.svg"/></a></p>
<p><a href="https://www.cals.iastate.edu/"><strong>College of Agriculture and Life Sciences</strong></a></p>
</div>
</div>
</div>
</div>
</div>
<div class="grid-3 region region-footer-second" id="region-footer-second">
<div class="region-inner region-footer-second-inner">
<div class="block block-block block-5 block-block-5 odd block-without-title" id="block-block-5">
<div class="block-inner clearfix">
<div class="content clearfix">
<p><strong>Department of Animal Science</strong><br/>
1221 Kildee Hall<br/>
Iowa State University Ames, Iowa 50011-3150<br/>
phone: <a href="tel:+15152942160">(515) 294-2160</a><br/>
fax: (515) 294-6994<br/>
email: <a href="mailto:answeb@iastate.edu">answeb@iastate.edu</a></p>
<p><a href="https://www.ans.iastate.edu/ans-it">Animal Science IT Support</a></p>
</div>
</div>
</div>
</div>
</div>
<div class="grid-3 region region-footer-third" id="region-footer-third">
<div class="region-inner region-footer-third-inner">
<div class="block block-menu block-menu-social block-menu-menu-social odd block-without-title" id="block-menu-menu-social">
<div class="block-inner clearfix">
<div class="content clearfix">
<ul class="menu"><li class="first leaf"><a href="https://www.facebook.com/IowaStateUniversityAnimalScience/" title=""><span aria-hidden="true" class="fa fa-facebook-official"></span><span class="social-title"> Like us at ISU Department of Animal Science</span></a></li>
<li class="leaf"><a href="https://www.facebook.com/IowaStateUniversityMeatLaboratory/" title=""><span aria-hidden="true" class="fa fa-facebook-official"></span><span class="social-title"> Like us at ISU Meat Laboratory</span></a></li>
<li class="last leaf"><a href="https://www.instagram.com/isuanimalscience/?hl=en" title=""><span aria-hidden="true" class="fa fa-instagram"></span><span class="social-title"> Instagram</span></a></li>
</ul> </div>
</div>
</div>
<div class="block block-block block-12 block-block-12 even block-without-title" id="block-block-12">
<div class="block-inner clearfix">
<div class="content clearfix">
<ul class="menu">
<li class="first leaf">
<a href="https://www.ans.iastate.edu/site-index"><span aria-hidden="true" class="fa fa-list-ul"></span>Site Index</a>
</li>
<li class="last leaf">
<a href="https://www.ans.iastate.edu/sitemap" title="Display a site map with RSS feeds."><span aria-hidden="true" class="fa fa-sitemap"></span>Site map</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="grid-3 region region-footer-fourth" id="region-footer-fourth">
<div class="region-inner region-footer-fourth-inner">
<div class="block block-block block-11 block-block-11 odd block-without-title" id="block-block-11">
<div class="block-inner clearfix">
<div class="content clearfix">
<p>Copyright © 2021 <br/> Iowa State University of Science and Technology. All rights reserved.</p>
<ul class="menu">
<li class="first leaf">
<a href="https://www.policy.iastate.edu/policy/discrimination">Non-discrimination Policy</a>
</li>
<li class="leaf">
<a href="https://www.policy.iastate.edu/electronicprivacy">Privacy Policy</a>
</li>
<li class="last leaf">
<a href="https://www.digitalaccess.iastate.edu">Digital Access &amp; Accessibility</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div></footer>
</div>
<div class="region region-page-bottom" id="region-page-bottom">
<div class="region-inner region-page-bottom-inner">
</div>
</div>
<script src="https://www.ans.iastate.edu/sites/all/modules/luggage/luggage_announcements/js/flexslider.load.js?qlrehw" type="text/javascript"></script>
</body>
</html>