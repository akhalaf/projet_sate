<!DOCTYPE html>
<html><!-- #BeginTemplate "/Templates/page.dwt" --><!-- DW6 --><head>
<!-- #BeginEditable "doctitle" -->
<title>Google Chrome Fans: talks about Google Chrome News, Tutorial, Themes, Extensions &amp; Plugins download, source code</title>
<meta content="Google Chrome, Google Chrome Fans, Chrome News, Chrome Tutorial, Chrome Add-ons, Chrome Add-ons download, Chrome Theme, Chrome source code, Chrome Extensions, Google, WebKit, Chrome Fans, Free download add-ons, Browser, Firefox, Internet Explorer, Safari, Opera" name="keywords"/>
<meta content="Google Chrome Fans talks about Google Chrome, it's here to share the latest news and tutorial about Google Chrome, help solve the problems of Chrome users, provide the Chrome Add-ons, extensions, themes and source code download." name="description"/>
<!-- #EndEditable -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="ChromeFans.org" name="author"/>
<meta content="ALL" name="robots"/>
<link href="https://image.chromefans.org/css-js/2012.cssgz" rel="stylesheet" type="text/css"/>
<link href="https://feeds.feedburner.com/chromefans" rel="alternate" title="RSS" type="application/rss+xml"/>
<script src="https://image.chromefans.org/css-js/base.jsgz" type="text/javascript"></script>
<script type="text/javascript">var switchTo5x=true;</script><script src="https://www.chromefans.org/base/buttons.js" type="text/javascript"></script><script type="text/javascript">stLight.options({publisher:'d7ab5b2d-74d9-4228-9efb-448605b2d374'});</script>
</head>
<body onload="init();">
<!-- Begin: ChromeFans TopLogo -->
<div id="top_logo">
<div id="top_bar">
<div id="top_bar_items">
<div id="top_bar_left">
<ul id="topcssmenu">
<li class="l1"><a href="https://www.chromefans.org" title="Chrome Fans">Chrome Fans</a></li>
<li>|</li>
<li class="l1"><a href="https://tools.chromefans.org" title="Free Webmaster Tools, Chrome Extensions">Webmaster Tools <span class="dropicon"> </span></a>
<ul>
<li><a href="https://www.openadmintools.com" target="_blank" title="Open Admin Tools: Website Stats">Open Admin Tools</a></li>
<li><a href="https://ping.openadmintools.com" target="_blank" title="Free online ping: ping to any domain or ip from worldwide locations">Free Online Ping</a></li>
<li><a href="https://ip.openadmintools.com" target="_blank" title="GEO IP lookup">GEO IP Lookup</a></li>
<li><a href="https://topwebhosting.chromefans.org/" title="Top 10 Web Hosting">Top 10 Web Hosting</a></li>
<li><a href="https://pagerank.chromefans.org/" title="Open SEO Stats (PageRank Status) - Chrome SEO Toolbar, PageRank Checker for Chrome">Open SEO Stats - Chrome SEO Toolbar</a></li>
<li><a href="https://pagerank.chromefans.org/pagerank-checker/" title="Online PageRank Checker, ">Online PageRank Checker</a></li>
<li><a href="https://whois.chromefans.org/" title="WHOIS Lookup">WHOIS Lookup</a></li>
<li><a href="https://md5calculator.chromefans.org/" title="Free MD5 Hash Calculator">MD5 Hash Calculator</a></li>
<li><a href="https://fileicons.chromefans.org/" title="Free File Icons Project">File Icons Project</a></li>
<li><a class="l2_sp" href="https://googleplussearch.chromefans.org/" title="Search Google Plus">Google Plus Search Engine</a></li>
<li><a href="https://searchcoupons.chromefans.org/" title="Search free coupons">Coupons Search Engine</a></li>
<li><a href="https://attachmenticons.chromefans.org/" title="Attachment Icons for Gmail™ and Google Apps™ ">Attachment Icons for Gmail™ and Google Apps™ </a></li>
</ul>
</li>
</ul>
</div>
<div id="top_bar_right">
<a href="https://feeds.feedburner.com/chromefans" id="rss16" title="Subscribe ChromeFans by RSS Feed">RSS</a> | 
    <a href="https://feedburner.google.com/fb/a/mailverify?uri=chromefans" title="Subscribe ChromeFans by Email">Email Subscribe</a> |
    <a href="https://www.chromefans.org/about/#submit" id="submit16" title="Submit your article to ChromeFans">Submit article</a> | 
    <a href="https://www.chromefans.org/about/" title="Contact the webmaster">Contact</a>
</div>
</div>
</div>
<div id="top_logo_items">
<div id="top_logo_left" onclick="document.location='https://www.chromefans.org/'" title="ChromeFans.org: Google Chrome technology news, tutorial, free themes and add-ons download">
<div id="top_logo_left_text">Webmaster tools, Google Chrome technology news, tutorials, free themes and extensions download</div>
</div>
<div id="top_logo_right">
<div id="top_logo_right_search">
<form action="https://www.chromefans.org/search/" method="get" target="_top">
<div id="search_box">
<input id="search_input" maxlength="255" name="q" onblur="if(this.value=='')this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue)this.value='';" size="10" type="text" value="Search..."/>
<input id="search_btn" name="" type="submit" value=""/>
</div>
</form>
</div>
</div>
</div>
</div>
<!-- End: ChromeFans TopLogo -->
<!-- Begin: Chrome menu -->
<div id="top_menu">
<div id="top_menu_area">
<div id="top_menu_items">
<div id="menu_bar">
<ul id="cssmenu">
<li class="l1"><a href="https://www.chromefans.org/" title="ChromeFans: Google Chrome technology news, tutorial, free themes and extensions download">Home</a></li>
<li class="l1"><a href="https://tools.chromefans.org" title="Free Web Tools, Free Webmaster Tools, Free Chrome Extensions">Free Web Tools &amp; Extensions <span class="dropicon"> </span></a>
<ul>
<li><a href="https://www.openadmintools.com" target="_blank" title="Open Admin Tools: Website Stats">Open Admin Tools</a></li>
<li><a href="https://ping.openadmintools.com" target="_blank" title="Free online ping: ping to any domain or ip from worldwide locations">Free Online Ping</a></li>
<li><a href="https://ip.openadmintools.com" target="_blank" title="GEO IP lookup">GEO IP Lookup</a></li>
<li><a href="https://topwebhosting.chromefans.org/" title="Top 10 Web Hosting">Top 10 Web Hosting</a></li>
<li><a href="https://pagerank.chromefans.org/" title="Open SEO Stats (PageRank Status) - Chrome SEO Toolbar, PageRank Checker for Chrome">Open SEO Status - Chrome SEO Toolbar</a></li>
<li><a href="https://pagerank.chromefans.org/pagerank-checker/" title="Online PageRank Checker, ">Online PageRank Checker</a></li>
<li><a href="https://whois.chromefans.org/" title="WHOIS Lookup">WHOIS Lookup</a></li>
<li><a href="https://md5calculator.chromefans.org/" title="Free MD5 Hash Calculator">MD5 Hash Calculator</a></li>
<li><a href="https://fileicons.chromefans.org/" title="Free File Icons Project">File Icons Project</a></li>
<li><a class="l2_sp" href="https://attachmenticons.chromefans.org/" title="Attachment Icons for Gmail™ and Google Apps™ ">Attachment Icons for Gmail™ and Google Apps™ </a></li>
<li><a href="https://downloads.chromefans.org/" title="Chrome Downloads Extension">Chrome Downloads Extension</a></li>
<li><a href="https://searchcoupons.chromefans.org/" title="Search free coupons">Coupons Search Engine</a></li>
<li><a href="https://googleplussearch.chromefans.org/" title="Search Google Plus">Google Plus Search Engine</a></li>
</ul>
</li>
<li class="l1"><a href="https://www.chromefans.org/chrome-tutorial/" title="Google Chrome Tutorials">Tutorials</a></li>
<li class="l1"><a href="https://www.chromefans.org/chrome-news/" title="Google Chrome News">News</a></li>
<li class="l1"><a href="https://www.chromefans.org/chrome-plugins/" title="Free Google Chrome Extesions, Plug-ins">Extensions</a></li>
<li class="l1"><a href="https://www.chromefans.org/chrome-themes/" title="Free Google Chrome themes">Themes</a></li>
<li class="l1"><a href="https://www.chromefans.org/chrome-resource/" title="Other resources about Google Chrome">Resources</a></li>
<li><a href="https://forum.chromefans.org/" title="Google Chrome Forum: Discuss Google Chrome news, tips, problems, themes, plugins, extensions, etc.">Forum</a></li>
</ul>
</div>
</div>
</div>
<div id="top_menu_bottomall"><div id="top_menu_bottom"></div></div>
</div>
<!-- End: Chrome menu -->
<div id="body_center">
<div id="blogarea">
<!-- Begin: Chrome left panel -->
<div id="blog_l">
<!-- #BeginEditable "sitebody" -->
<div id="h1area"><h1>Welcome to Chrome Fans Blog</h1><div id="tips">Google Chrome technology news, tutorials,
free themes and extensions download
</div></div>
<div id="bt_content">
<div class="title">
<div class="title_line"></div>
<div class="title_logo"></div>
<div class="title_text_comment">
<div class="title_text">
<div class="title_h"><h2><a class="title_link" href="https://www.chromefans.org/chrome-plugins/open-seo-stats-10-released.htm" title="Open SEO Stats 10.0 Released">Open SEO Stats 10.0 Released</a></h2></div>
<div class="title_date">June 30, 2019</div>
</div>
<div class="title_comment">
<div class="title_comment_num"><a href="https://www.chromefans.org/chrome-plugins/open-seo-stats-10-released.htm#addreview" title="0 comment(s)">0</a></div>
</div>
</div>
</div>
<div class="desc">
<div class="title_line"></div>
<div class="desc_logo"><a href="https://www.chromefans.org/chrome-plugins/open-seo-stats-10-released.htm" title="Open SEO Stats 10.0 Released"><img alt="Open SEO Stats 10.0 Released" class="desc_img" height="160" src="https://image.chromefans.org/i200/id223/open-seo-stats.png" width="300"/></a></div><div class="desc_text">The Chrome SEO extension - Open SEO Stats(Formerly: PageRank Status) has been updated to version 10.0. This is a major update: 1. Support for 360, Goo.ne.jp, Sogou and Yandex new search results of pages indexed. These search engines changed the output formats of search results recently...</div>
</div>
<div class="title">
<div class="title_line"></div>
<div id="index_ad" style="text-align:center;margin:10px 0px 0px 0px;">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-5801877696325956";
/* CF.ORG www_index_468x60 */
google_ad_slot = "5991888890";
google_ad_width = 468;
google_ad_height = 60;
//-->
</script>
<script src="https://pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript">
</script>
</div></div>
<div class="title">
<div class="title_line"></div>
<div class="title_logo"></div>
<div class="title_text_comment">
<div class="title_text">
<div class="title_h"><h2><a class="title_link" href="https://www.chromefans.org/chrome-plugins/google-pagerank-is-officially-dead-now.htm" title="Google PageRank is officially dead now">Google PageRank is officially dead now</a></h2></div>
<div class="title_date">May 25, 2016</div>
</div>
<div class="title_comment">
<div class="title_comment_num"><a href="https://www.chromefans.org/chrome-plugins/google-pagerank-is-officially-dead-now.htm#addreview" title="12 comment(s)">12</a></div>
</div>
</div>
</div>
<div class="desc">
<div class="title_line"></div>
<div class="desc_logo"><a href="https://www.chromefans.org/chrome-plugins/google-pagerank-is-officially-dead-now.htm" title="Google PageRank is officially dead now"><img alt="Google PageRank is officially dead now" class="desc_img" height="160" src="https://image.chromefans.org/i200/id222/pagerank-is-dead.jpg" width="300"/></a></div><div class="desc_text">Google PageRank is officially dead now. On March 7 2016, Google officially killed off Toolbar PageRank scores to the few browser tools and web site tools that use it. Google has confirmed it in real life if you do not believe me. :-)</div>
</div>
<div class="title">
<div class="title_line"></div>
<div class="title_logo"></div>
<div class="title_text_comment">
<div class="title_text">
<div class="title_h"><h2><a class="title_link" href="https://www.chromefans.org/webmaster/social-search-engine.htm" title="Social Search Engine">Social Search Engine</a></h2></div>
<div class="title_date">September 29, 2015</div>
</div>
<div class="title_comment">
<div class="title_comment_num"><a href="https://www.chromefans.org/webmaster/social-search-engine.htm#addreview" title="3 comment(s)">3</a></div>
</div>
</div>
</div>
<div class="desc">
<div class="title_line"></div>
<div class="desc_logo"><a href="https://www.chromefans.org/webmaster/social-search-engine.htm" title="Social Search Engine"><img alt="Social Search Engine" class="desc_img" height="160" src="https://image.chromefans.org/i200/id221/social-networks-search.jpg" width="300"/></a></div><div class="desc_text">Social Search Engine finds social information from multiple social networking sites including Facebook, Twitter, Google Plus, Blogspot, LinkedIn, YouTube  and many more at same time. It's based Google Custom Engine and saves your lots of times as you don't have to search each social site individually.</div>
</div>
<div class="title">
<div class="title_line"></div>
<div class="title_logo"></div>
<div class="title_text_comment">
<div class="title_text">
<div class="title_h"><h2><a class="title_link" href="https://www.chromefans.org/webmaster/ping-to-any-domain-or-ip-address-from-worldwide-locations.htm" title="Ping to any domain or IP address from worldwide locations">Ping to any domain or IP address from worldwide locations</a></h2></div>
<div class="title_date">September 18, 2015</div>
</div>
<div class="title_comment">
<div class="title_comment_num"><a href="https://www.chromefans.org/webmaster/ping-to-any-domain-or-ip-address-from-worldwide-locations.htm#addreview" title="4 comment(s)">4</a></div>
</div>
</div>
</div>
<div class="desc">
<div class="title_line"></div>
<div class="desc_logo"><a href="https://www.chromefans.org/webmaster/ping-to-any-domain-or-ip-address-from-worldwide-locations.htm" title="Ping to any domain or IP address from worldwide locations"><img alt="Ping to any domain or IP address from worldwide locations" class="desc_img" height="160" src="https://image.chromefans.org/i200/id220/free-online-ping-3.jpg" width="300"/></a></div><div class="desc_text">Ping is usually used to detect the actual internet speed and quality.  Open Admin Tools provides a new web-base services for webmaster: Free Online Ping. With this free web tools, you can ping to any domain or IP address from worldwide locations and shows how long it takes for packets to reach host, and shows the ping results on Google Map.</div>
</div>
<div class="title">
<div class="title_line"></div>
<div class="title_logo"></div>
<div class="title_text_comment">
<div class="title_text">
<div class="title_h"><h2><a class="title_link" href="https://www.chromefans.org/chrome-plugins/open-seo-stats-90-released.htm" title="Open SEO Stats 9.0 Released">Open SEO Stats 9.0 Released</a></h2></div>
<div class="title_date">August 18, 2015</div>
</div>
<div class="title_comment">
<div class="title_comment_num"><a href="https://www.chromefans.org/chrome-plugins/open-seo-stats-90-released.htm#addreview" title="6 comment(s)">6</a></div>
</div>
</div>
</div>
<div class="desc">
<div class="title_line"></div>
<div class="desc_logo"><a href="https://www.chromefans.org/chrome-plugins/open-seo-stats-90-released.htm" title="Open SEO Stats 9.0 Released"><img alt="Open SEO Stats 9.0 Released" class="desc_img" height="160" src="https://image.chromefans.org/i200/id219/open-seo-stats-9-small.png" width="300"/></a></div><div class="desc_text">Open SEO Stats(Formerly: PageRank Status) has been updated to version 9.0! This is a major update based on fans summit feedback. Below are the update notes: 1. Add a search box in the top section of the extension. You can directly enter the site URL to check the website stats now. 2. Provide web-based SEO report at OpenAdminTools.com...</div>
</div>
<div class="title">
<div class="title_line"></div>
<div class="title_logo"></div>
<div class="title_text_comment">
<div class="title_text">
<div class="title_h"><h2><a class="title_link" href="https://www.chromefans.org/chrome-plugins/pagerank-status-renamed-to-open-seo-stats-because-of-google-single-purpose-issue.htm" title="PageRank Status renamed to Open SEO Stats because of Google Single Purpose issue">PageRank Status renamed to Open SEO Stats because of Google Single Purpose issue</a></h2></div>
<div class="title_date">September 18, 2014</div>
</div>
<div class="title_comment">
<div class="title_comment_num"><a href="https://www.chromefans.org/chrome-plugins/pagerank-status-renamed-to-open-seo-stats-because-of-google-single-purpose-issue.htm#addreview" title="14 comment(s)">14</a></div>
</div>
</div>
</div>
<div class="desc">
<div class="title_line"></div>
<div class="desc_onlytext">PageRank Status extension is available at Chrome Web Store now! I renamed PageRank Status to Open SEO Stats because of Google Single Purpose issue, you can install it from Chrome Web Store.</div>
</div>
<div class="title">
<div class="title_line"></div>
<div class="title_logo"></div>
<div class="title_text_comment">
<div class="title_text">
<div class="title_h"><h2><a class="title_link" href="https://www.chromefans.org/chrome-plugins/google-removed-pagerank-status-temporarily-because-of-single-purpose-issue.htm" title="Google removed PageRank Status temporarily, because of Single Purpose issue">Google removed PageRank Status temporarily, because of Single Purpose issue</a></h2></div>
<div class="title_date">September 14, 2014</div>
</div>
<div class="title_comment">
<div class="title_comment_num"><a href="https://www.chromefans.org/chrome-plugins/google-removed-pagerank-status-temporarily-because-of-single-purpose-issue.htm#addreview" title="9 comment(s)">9</a></div>
</div>
</div>
</div>
<div class="desc">
<div class="title_line"></div>
<div class="desc_logo"><a href="https://www.chromefans.org/chrome-plugins/google-removed-pagerank-status-temporarily-because-of-single-purpose-issue.htm" title="Google removed PageRank Status temporarily, because of Single Purpose issue"><img alt="Google removed PageRank Status temporarily, because of Single Purpose issue" class="desc_img" height="160" src="https://image.chromefans.org/i200/id217/pagerank-status.jpg" width="300"/></a></div><div class="desc_text">Google removed PageRank Status from Chrome Web Store temporarily, because of Single Purpose issue. They told me that PageRank Status has too many features and I must split this extension to different extensions according Google new policy. I am working for restoring PageRank Status to Chrome Web Store, it maybe available after 4~7 business days.</div>
</div>
<div class="title">
<div class="title_line"></div>
<div class="title_logo"></div>
<div class="title_text_comment">
<div class="title_text">
<div class="title_h"><h2><a class="title_link" href="https://www.chromefans.org/chrome-tutorial/how-to-check-the-page-load-time-of-current-website.htm" title="How to check the page load time of current website">How to check the page load time of current website</a></h2></div>
<div class="title_date">December 21, 2012</div>
</div>
<div class="title_comment">
<div class="title_comment_num"><a href="https://www.chromefans.org/chrome-tutorial/how-to-check-the-page-load-time-of-current-website.htm#addreview" title="64 comment(s)">64</a></div>
</div>
</div>
</div>
<div class="desc">
<div class="title_line"></div>
<div class="desc_logo"><a href="https://www.chromefans.org/chrome-tutorial/how-to-check-the-page-load-time-of-current-website.htm" title="How to check the page load time of current website"><img alt="How to check the page load time of current website" class="desc_img" height="160" src="https://image.chromefans.org/i200/id216/page-speed.jpg" width="300"/></a></div><div class="desc_text"><p><strong>Page load time</strong> is an very important part of  web user experience, it's also becoming a more important factor when it comes to search engine rankings. If your site is too slow, perhaps you need to optimize the web site or migrate to a faster server. There are many tools for checking how long it takes to load your website. Today I integrated this feature to <a href="http://pagerank.chromefans.org/" title="PageRank Stauts">PageRank Status 6.30</a> (A SEO toolbar for Chrome), it shows the page load time of current web page and rates it, and also provides the timing-related information...</p></div>
</div>
<div class="title">
<div class="title_line"></div>
<div class="title_logo"></div>
<div class="title_text_comment">
<div class="title_text">
<div class="title_h"><h2><a class="title_link" href="https://www.chromefans.org/chrome-plugins/how-to-create-your-own-chrome-extension-in-just-a-few-minutes.htm" title="How to Create Your Own Chrome Extension in Just a Few Minutes">How to Create Your Own Chrome Extension in Just a Few Minutes</a></h2></div>
<div class="title_date">December 15, 2012</div>
</div>
<div class="title_comment">
<div class="title_comment_num"><a href="https://www.chromefans.org/chrome-plugins/how-to-create-your-own-chrome-extension-in-just-a-few-minutes.htm#addreview" title="10 comment(s)">10</a></div>
</div>
</div>
</div>
<div class="desc">
<div class="title_line"></div>
<div class="desc_logo"><a href="https://www.chromefans.org/chrome-plugins/how-to-create-your-own-chrome-extension-in-just-a-few-minutes.htm" title="How to Create Your Own Chrome Extension in Just a Few Minutes"><img alt="How to Create Your Own Chrome Extension in Just a Few Minutes" class="desc_img" height="160" src="https://image.chromefans.org/i200/id214/how-to-create-chrome-extension.jpg" width="300"/></a></div><div class="desc_text"><p>Wouldn’t it be great if you could make your own extension in just a few minutes? </p><p>Wips.com is a startup company with over 1milion users located in Europe that developed a technology that allows that.  Using the technology you can create extensions in just a few minutes for:</p><ul><li>YouTube channels; </li><li>RSS news; </li><li>Search extensions; </li><li>Toolbar; </li><li>Custom extensions.</li></ul></div>
</div>
<div class="title">
<div class="title_line"></div>
<div class="title_logo"></div>
<div class="title_text_comment">
<div class="title_text">
<div class="title_h"><h2><a class="title_link" href="https://www.chromefans.org/chrome-tutorial/how-to-analyze-meta-tags-of-current-web-page-in-google-chrome.htm" title="How to analyze Meta Tags of current web page in Google Chrome">How to analyze Meta Tags of current web page in Google Chrome</a></h2></div>
<div class="title_date">November 27, 2012</div>
</div>
<div class="title_comment">
<div class="title_comment_num"><a href="https://www.chromefans.org/chrome-tutorial/how-to-analyze-meta-tags-of-current-web-page-in-google-chrome.htm#addreview" title="14 comment(s)">14</a></div>
</div>
</div>
</div>
<div class="desc">
<div class="title_line"></div>
<div class="desc_logo"><a href="https://www.chromefans.org/chrome-tutorial/how-to-analyze-meta-tags-of-current-web-page-in-google-chrome.htm" title="How to analyze Meta Tags of current web page in Google Chrome"><img alt="How to analyze Meta Tags of current web page in Google Chrome" class="desc_img" height="160" src="https://image.chromefans.org/i200/id213/analzye-meta-tags.png" width="300"/></a></div><div class="desc_text"><p>Meta Tags always goes inside the HEAD area and will not be displayed on the page. If you want to check or analyze the Meta Tags, you can right click the mouse button in the web page and then select View Page Source, how ever, this method is not convenient.</p><p></p><p>I released <strong>PageRank Status</strong> 6.0 today. PageRank Status is a SEO extension for Chrome browser to easily access the Gogole PageRank, AlexaRank, balnklinks, traffic data, cached pages, whois, SNS profile and more. In version 6.0, it supports for displaying the <strong>SEO releated meta elements</strong> of current web page...</p></div>
</div>
</div>
<!-- #EndEditable -->
</div>
<!-- End: Chrome left panel -->
<!-- Begin: Chrome right panel -->
<div id="blog_r">
<div id="blog_r_ad300"><script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- CF.ORG.2012 all_right_300x250 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-5801877696325956" data-ad-slot="2408075267" style="display:inline-block;width:300px;height:250px"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script></div>
<!-- Begin: Chrome Social News -->
<div class="blog_r_box">
<div class="box_title"><div class="box_h"><h3>Social News</h3></div></div>
<div class="box_text">
<div id="sns_followme">Follow me</div>
<table id="sns_btns">
<tr>
<td class="sns_btn"><a href="https://www.facebook.com/people/Andrew-Shen/1401158947" rel="nofollow" title="Follow me on Facebook"><div class="sns_link"></div></a></td><td class="sns_free"></td>
<td class="sns_btn"><a href="https://twitter.com/#!/lovechrome" rel="nofollow" title="Follow me on Twitter"><div class="sns_link"></div></a></td><td class="sns_free"></td>
<td class="sns_btn"><a href="https://plus.google.com/107516790688996665941" rel="publisher" title="Follow me on Google Plus"><div class="sns_link"></div></a></td><td class="sns_free"></td>
<td class="sns_btn"><a href="https://feedburner.google.com/fb/a/mailverify?uri=chromefans" rel="nofollow" title="Subscribe by email"><div class="sns_link"></div></a></td><td class="sns_free"></td>
<td class="sns_btn"><a href="https://feeds.feedburner.com/chromefans" rel="nofollow" title="Subscribe by RSS"><div class="sns_link"></div></a></td>
</tr>
</table>
<form action="https://feedburner.google.com/fb/a/mailverify" id="sns_email_sub" method="post" onsubmit="window.open('https://feedburner.google.com/fb/a/mailverify?uri=chromefans', 'popupwindow', 'scrollbars=yes,width=560,height=520');return true" target="popupwindow">
<div id="sns_email_icon"><a href="https://feedburner.google.com/fb/a/mailverify?uri=chromefans" rel="nofollow" title="Subscribe to Freshshare By email">Subscribe by E-mail:</a></div>
<div id="sns_email_inputs">
<div id="sns_email_input"><input name="email" onblur="if(this.value=='')this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue)this.value='';" type="text" value="Enter your email"/></div>
<div id="sns_email_submit"><input name="" type="submit" value=""/></div>
</div>
<input name="uri" type="hidden" value="chromefans"/>
<input name="loc" type="hidden" value="en_US"/>
<div id="sns_email_why"><a href="https://www.chromefans.org/about/why-subscribe.htm" title="Why Subscribe?">Why Subscribe?</a></div>
</form>
</div>
</div>
<!-- End: Chrome Social News -->
<!-- Begin: Free Chrome Online Tools -->
<div class="blog_r_box">
<div class="box_title"><div class="box_h"><h3>Free Web Tools &amp; Extensions</h3></div></div>
<div class="box_text">
<div id="FreeTools">
<ul>
<li><a href="https://www.openadmintools.com" target="_blank" title="Open Admin Tools: Website Stats">Open Admin Tools</a> - Website Stats</li>
<li><a href="https://ping.openadmintools.com" target="_blank" title="Free online ping: ping to any domain or ip from worldwide locations">Free Online Ping</a></li>
<li><a href="https://ip.openadmintools.com" target="_blank" title="GEO IP lookup">GEO IP Lookup</a></li>
<li><a href="https://topwebhosting.chromefans.org/" title="Top 10 Web Hosting">Top 10 Web Hosting</a></li>
<li><a href="https://pagerank.chromefans.org/" title="PageRank Checker, PageRank Status, Open SEO Stats">Open SEO Stats</a> - Chrome SEO Toolbar</li>
<li><a href="https://pagerank.chromefans.org/pagerank-checker/" title="Online PageRank Checker, ">Online PageRank Checker</a></li>
<li><a href="https://whois.chromefans.org/" title="WHOIS Lookup">WHOIS Lookup</a></li>
<li><a href="https://md5calculator.chromefans.org/" title="Free MD5 Hash Calculator">Free MD5 Hash Calculator</a></li>
<li><a href="https://fileicons.chromefans.org/" title="Free File Icons Project">File Icons Project</a><hr/></li>
<li><a href="https://attachmenticons.chromefans.org/" title="Attachment Icons for Gmail™ and Google Apps™ ">Attachment Icons for Gmail &amp; Google Apps</a></li>
<li><a href="https://downloads.chromefans.org/" title="Chrome Downloads Extension">Chrome Downloads Extension</a></li>
<li><a href="https://searchcoupons.chromefans.org/" title="">Coupons Search Engine</a></li>
<li><a href="https://googleplussearch.chromefans.org/" title="">Google Plus Search Engine</a></li>
</ul>
</div>
</div>
</div>
<!-- End: Free Chrome Online Tools -->
<!-- #BeginEditable "right_edit" -->
<!-- Begin: Chrome Most Popular Articles -->
<div class="blog_r_box">
<div class="box_title"><div class="box_h"><h3>Most Popular Articles</h3></div></div>
<div class="box_text">
<div id="MostPopular">
<ul>
<li><a href="https://www.chromefans.org/chrome-plugins/open-seo-stats-10-released.htm" title="Open SEO Stats 10.0 Released">Open SEO Stats 10.0 Released</a></li>
</ul>
</div>
</div>
</div>
<!-- End: Chrome Most Popular Articles -->
<!-- Begin: Chrome Screen Resolutions -->
<div class="blog_r_box">
<div class="box_title"><div class="box_h"><h3>Screen Resolution Statistics</h3></div></div>
<div class="box_text">
<div id="ScreenRes">
<!-- Screen Resolution Report BEGIN -->
<script src="https://stats.screenresolution.org/site-report.js?siteid=2" type="text/javascript"></script><div id="srReport">Powered by <a href="https://www.screenresolution.org/" title="Screen Resolution Statistics">ScreenResolution.org</a></div>
<!-- Screen Resolution Report END -->
</div>
</div>
</div>
<!-- End: Chrome Screen Resolutions -->
<!-- Begin: Chrome External Resources -->
<div class="blog_r_box">
<div class="box_title"><div class="box_h"><h3>External Resources</h3></div></div>
<div class="box_text">
<div id="OtherRes">
<ul>
<li><a class="external" href="http://www.google.com/chrome">Download Google Chrome</a></li>
<li><a class="external" href="http://en.wikipedia.org/wiki/Google_Chrome">Wikipedia: Google Chrome</a></li>
<li><a class="external" href="http://shareweb20.com/">Share Web 2.0</a></li>
<li><a class="external" href="http://www.download2pc.com/" title="Free game downloads, free anti-virus software, free MP3 DVD program downloads and desktop stuff">Download software for free</a></li>
<li><a class="external" href="http://blog.freshshare.com/" title="Software How-to Tutorials and Reviews">Freshshare Blog</a></li>
<li><a class="external" href="http://www.downloadyoutubechrome.com/" title="Download Youtube Chrome Extension">Download Youtube Chrome Extension</a></li>
</ul>
</div>
</div>
</div>
<!-- End: Chrome External Resources -->
<!-- Begin: About ChromeFans -->
<div class="blog_r_box">
<div class="box_title"><div class="box_h"><h3>About ChroneFans.org</h3></div></div>
<div class="box_text">
<div id="about_cf">
  ChromeFans.org was created by a Google fans, it's not affiliated with Google. It talks about <b>Google Chrome</b>, ChromeFans is here to share the latest news and tutorials about Google Chrome, help solve the problems of Chrome users, provide the Chrome <b>Add-ons</b>, <b>Extensions</b>, <b>Themes</b> and <b>source codes</b> download.
  </div>
</div>
</div>
<!-- End: About ChromeFans -->
<div class="blog_r_box">
<div class="box_text" style="text-align:center">
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- CF.ORG www_page_160x600 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-5801877696325956" data-ad-slot="9388900395" style="display:inline-block;width:160px;height:600px"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
</div>
<!-- #EndEditable -->
</div>
<!-- End: Chrome right panel -->
</div>
</div>
<!-- begin: Chrome footer -->
<div id="footer">
<div class="gray_line"></div>
<div id="footer_links">
<a href="https://www.chromefans.org/">ChromeFans.org</a> | 
  <a href="https://www.chromefans.org/about/privacy.htm">Privacy</a> | 
  <a href="https://www.chromefans.org/about/">Contact</a> | 
  <a href="https://www.chromefans.org/chrome-resource/">Other Resources</a> | 
  <a href="https://www.chromefans.org/about/linktous.htm">Link to us</a> | 
  <a href="https://www.chromefans.org/about/sitemap.htm">Sitemap</a> |
  <a href="https://www.chromefans.org/google_sitemap_index.xml">Google Sitemap</a><hr/>
  Copyright © <script type="text/javascript">ShowCP();</script> ChromeFans.org, All Rights Reserved. 
  </div>
</div>
<!-- end: Chrome footer -->
</body>
<!-- #EndTemplate --></html>