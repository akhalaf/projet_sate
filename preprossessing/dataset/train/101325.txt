<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<!-- 
	featured by Melanie Meyer, www.melaniemeyer.de

	This website is powered by TYPO3 - inspiring people to share!
	TYPO3 is a free open source Content Management Framework initially created by Kasper Skaarhoj and licensed under GNU/GPL.
	TYPO3 is copyright 1998-2015 of Kasper Skaarhoj. Extensions are copyright of their respective owners.
	Information and contribution at http://typo3.org/
-->
<base href="https://www.allnet.de/"/>
<link href="https://www.allnet.de/typo3conf/ext/abz_eff_template/templates_layout4/media/allnet.ico" rel="shortcut icon" type="image/x-icon; charset=binary"/>
<link href="https://www.allnet.de/typo3conf/ext/abz_eff_template/templates_layout4/media/allnet.ico" rel="icon" type="image/x-icon; charset=binary"/>
<meta content="TYPO3 4.5 CMS" name="generator"/>
<link href="/typo3temp/stylesheet_4ecfded831.css?1473425655" media="all" rel="stylesheet" type="text/css"/>
<link href="/typo3conf/ext/abz_eff_template/templates_layout4/css/startstart/bootstrap.css?1461260975" media="all" rel="stylesheet" type="text/css"/>
<link href="/typo3conf/ext/abz_eff_template/templates_layout4/css/startstart/style.css?1527512217" media="all" rel="stylesheet" type="text/css"/>
<link href="/typo3conf/ext/abz_eff_template/templates_layout4/css/startstart/slick.css?1461260975" media="all" rel="stylesheet" type="text/css"/>
<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="/typo3conf/ext/abz_eff_template/templates_layout4/js/startstart/main_startstart.js?1518614694" type="text/javascript"></script>
<script src="/typo3conf/ext/abz_eff_template/templates_layout4/js/startstart/bootstrap.min.js?1461260977" type="text/javascript"></script>
<script src="/typo3conf/ext/abz_eff_template/templates_layout4/js/startstart/slick.min.js?1461260978" type="text/javascript"></script>
<script src="/typo3temp/javascript_a708894199.js?1473425639" type="text/javascript"></script>
<title>ALLNET GmbH -Startseite</title> <meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!--[if lt IE 9]>
			<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->
<link href="favicon.ico" rel="shortcut icon"/><!-- 16x16 -->
<link href="favicon.png" rel="apple-touch-icon-precomposed" sizes="144x144"/><!-- 144x144 -->
<link href="favicon.png" rel="apple-touch-icon-precomposed" sizes="114x114"/><!-- 114x114 -->
<link href="favicon.png" rel="apple-touch-icon-precomposed" sizes="72x72"/><!-- 72x72 -->
<link href="favicon.png" rel="apple-touch-icon-precomposed"/><!-- 57x57 -->
<script type="text/javascript">
var gaProperty = 'UA-82196940-1';
var disableStr = 'ga-disable-' + gaProperty;
if (document.cookie.indexOf(disableStr + '=true') > -1) {
window[disableStr] = true;
}
function gaOptout() {
document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
window[disableStr] = true;
alert('Das Tracking durch Google Analytics wurde in Ihrem Browser fÃ¼r diese Website deaktiviert.');
}
</script>
</head>
<body>
<div class="wrapper">
<!--	<div class="scene">
	  <div data-offset="90" class="xstar-1 parallax"></div>
	  <div data-offset="30" class="xstar-2 parallax"></div>
	  <div data-offset="20" class="xstar-3 parallax"></div>
	</div>	 -->
<header>
<div id="navi-top">
<div class="navbar-header">
<button class="navbar-toggle" data-target="#nav_collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<div class="container">
<div class="icons">
<ul>
<li><a href="/de/"><img alt="" src="/typo3conf/ext/abz_eff_template/templates_layout4/media/startstart/ic_home.png"/></a></li>
<li><a href="/de/allnet-brand/unternehmen/kontakt/"><img alt="" src="/typo3conf/ext/abz_eff_template/templates_layout4/media/startstart/ic_mail.png"/></a></li>
<li><a href="/de/allnet-brand/sitemap-brand/"><img alt="" src="/typo3conf/ext/abz_eff_template/templates_layout4/media/startstart/ic_site.png"/></a></li>
</ul>
</div>
<div style="float:left; margin: 6px 0 0 80px; font-size:16px; font-weight:bold;">Tel.: <a href="tel:+498989422222" style="color:#FFF">+49 (0)89 / 89422222</a></div>
<div class="langs">
						Bitte wählen Sie Ihr Land: 
						<div class="tx-srlanguagemenu-pi1">
<form action="" id="sr_language_menu_form">
<div class="de" id="lang_icon"></div>
<fieldset>
<legend>Sprache wÃ¤hlen</legend>
<label for="sr_language_menu_select">Sprache wÃ¤hlen</label>
<select id="sr_language_menu_select" name="L" onchange="top.location.replace(this.options[this.selectedIndex].value );" size="1" title="Bitte wÃ¤hlen Sie Ihr Land:">
<option class="tx-srlanguagemenu-pi1-option-1" value="/en/">en</option>
<option class="tx-srlanguagemenu-pi1-option-2" value="/at/">at</option>
<option class="tx-srlanguagemenu-pi1-option-3" selected="selected" value="/de/">de</option>
<option class="tx-srlanguagemenu-pi1-option-4" value="/es/">es</option>
<option class="tx-srlanguagemenu-pi1-option-5" value="/fr/">fr</option>
<option class="tx-srlanguagemenu-pi1-option-6" value="/hu/">hu</option>
</select>
</fieldset>
</form>
</div>
</div>
<div class="collapse navbar-collapse" id="nav_collapse">
<ul class="nav navbar-nav">
<!-- META_NAVI start-->
<!--			      			<li class="active"><a href="/">Startseite </a></li>             
							<li><a href="#">Kontakt</a></li>               
							<li><a href="#">Impressum </a></li>              
							<li><a href="#">AGB</a></li> -->
<!-- META_NAVI end-->
</ul>
</div>
<div class="clear"></div>
</div>
</div>
</div>
</header>
<div id="content">
<div class="container">
<div class="row">
<div class="col-sm-8">
<div class="logo"><a href="/"><img alt="" src="/typo3conf/ext/abz_eff_template/templates_layout4/media/startstart/logo.png"/></a></div>
<div class="col-sm-3">
<div class="main-text">
<div class="circles"><strong>Willkommen im neuen ALLNET Universum</strong><p style="margin-top: 0pt; margin-bottom: 0pt;">Sie befinden sich jetzt auf der Startseite. Nun kÃ¶nnen Sie zwischen dem ALLNET Brand, der ALLNET Distribution, dem LED Paradies Synergy21, den Messtechnik Spezialisten ALLDAQ, dem innovativen Elektronik Lernsystem <br/>BrickÂ´RÂ´knowledge und 802.lab, unserem Schulungszentrum wÃ¤hlen.</p></div>
</div>
<div class="circle-1">
<a href="https://distribution.allnet.de"><img alt="ALLNET Distribution" src="/typo3conf/ext/abz_eff_template/templates_layout4/media/startstart/icon_distri.png"/></a><div class="text"><div class="circles"><strong>ALLNET Distribution</strong>Als Distributor konzentrieren wir uns auf ein umfassendes Produktportfolio zukunftsorientierter Netzwerk-, Ãberwachungs- und KommunikationslÃ¶sungen . Wir beliefern Reseller und SystemhÃ¤user jeder GrÃ¶Ãenordnung. In diesem Bereich sind wir spezialisiert auf individuelle Projektierung, Logistikdienstleistungen und Fulfillment Services fÃ¼r ISP und Carrier. Auch im Pre- und After Sales werden unsere Kunden immer von Spezialisten betreut. Die ALLNET "Chefbetreuung" garantiert Ihnen Erfolg und zufriedene Kunden.</div></div>
</div>
<div class="circle-2">
<a href="https://www.alldaq.com" target="alldaq"><img alt="ALLDAQ" src="/typo3conf/ext/abz_eff_template/templates_layout4/media/startstart/icon_2.png"/></a><div class="text"><div class="circles"><strong>ALLDAQ</strong>ALLDAQ wurde als neue Business-Unit der <br/>ALLNET GmbH Anfang 2014 gegrÃ¼ndet. ALLDAQÂ´s Portfolio gliedert sich in die Bereiche Entwicklung und Distribution, womit ein breites Spektrum an Messtechnik-LÃ¶sungen fÃ¼r den Einsatz in Industrie, Forschung, Service und Ausbildung geboten wird. </div></div>
</div>
<div class="circle-3">
<a href="https://802lab.de" target="802lab"><img alt="802.lab" src="/typo3conf/ext/abz_eff_template/templates_layout4/media/startstart/icon_3.png"/></a><div class="text"><div class="circles"><strong>802.lab</strong>Da der IKT-Markt stÃ¤ndig neue Produkte und LÃ¶sungen hervorbringt und damit auch umfassendes technisches Wissen erfordert, fÃ¼hlen wir uns verpflichtet, unsere Erfahrung und auch das Wissen unserer Herstellerpartner an Sie weiterzugeben. Das 802.lab Schulungszentrum zÃ¤hlt zu den fÃ¼hrenden Weiterbildungseinrichtungen fÃ¼r FachhÃ¤ndler in Deutschland. Zusammen mit unseren Herstellern bieten wir Ihnen mit knapp 150 Schulungen pro Jahr umfassende Schulungen und Zertifizierungen in den wichtigsten Bereichen moderner ITK-Produkte.</div></div>
</div>
<div class="circle-7">
<a href="https://shop.allnet.de/?__shop=1"><img alt="ALLNET Shop" src="/typo3conf/ext/abz_eff_template/templates_layout4/media/startstart/icon_7.png"/></a><div class="text"><div class="circles"><strong>ALLNET Shop</strong>Hier kommen Sie schnell und direkt zu unserem Fachhandelsshop und kÃ¶nnen sofort mit Ihrer Bestellung beginnen.<br/>In unserem Onlineshop haben Sie Zugriff auf das gesamte Distributions- und ALLNET Brand Portfolio.</div></div>
</div>
<div class="circle-9">
<img alt="" class="empty" src="/typo3conf/ext/abz_eff_template/templates_layout4/media/startstart/icon_empty.png"/><div class="text"> </div>
</div>
</div>
<div class="col-sm-5 spacer">
						 
					</div>
<div class="col-sm-3">
<div class="circle-4">
<a href="https://www.allnet.de/de/allnet-brand/"><img alt="ALLNET Brand" src="/typo3conf/ext/abz_eff_template/templates_layout4/media/startstart/icon_brand.png"/></a><div class="text"><div class="circles"><strong>ALLNET Brand</strong><span id="docs-internal-guid-997a01ea-5fd1-2048-d7ee-6b6b58422ac0"></span>
<p style="margin-top: 0pt; margin-bottom: 0pt;"><span style="line-height: 16.56px;">ALLNET ist Entwickler und Hersteller moderner Netzwerk- und Kommunikationstechnologie und viele innovative Produkte aus den Bereichen, Netzwerk, Storage, Security und Home Automation werden in unseren eigenen Labors entwickelt. Durch FlexibilitÃ¤t, Schnelligkeit und Innovationskraft entwickelt ALLNET eigene LÃ¶sungen, Produkte und GeschÃ¤ftsmodelle, die die Marktanforderungen von heute und morgen erfÃ¼llen.</span></p></div></div>
</div>
<div class="circle-5">
<a href="https://www.synergy21.de" target="s21"><img alt="Synergy21" src="/typo3conf/ext/abz_eff_template/templates_layout4/media/startstart/icon_5.png"/></a><div class="text"><div class="circles"><strong>Synergy21</strong>Synergy 21 ist seit dem Jahr 2008 im Markt fÃ¼r energiesparende Beleuchtung aktiv und verfÃ¼gt mittlerweile Ã¼ber ein extrem breites Sortiment in den Bereichen LED, Controller, Netzteile und Consumer. Egal ob akkubetriebener Baustrahler oder leistungsstarke Hochleistungsstrahler fÃ¼r den AuÃenbereich, Synergy 21 hat immer eine energiesparende Alternative. Mit Synergy 21 sind Sie bereit fÃ¼r die LEDevolution!</div></div>
</div>
<div class="circle-6">
<a href="https://www.brickrknowledge.de/" target="brk"><img alt="Brick'R'knowledge" src="/typo3conf/ext/abz_eff_template/templates_layout4/media/startstart/icon_6.png"/></a><div class="text"><div class="circles"><strong>Brick'R'knowledge</strong>Das innovative Brick'R'knowledge System ermÃ¶glicht es, sich spielerisch mit Elektrotechnik und den wichtigsten Komponenten, GrÃ¶Ãen und Funktionsweisen auseinander zu setzen. Das System hilft zudem dabei, Elektronikwissen zu sammeln und mit Lerneffekt gebÃ¼ndelt an die nÃ¤chste Generation weiter zu geben. Die unterschiedlichen Brick Bausteine lassen sich ganz einfach zu individuellen LÃ¶sungen zusammenstecken und machen so einfache und auch komplexe Themen der Elektrotechnik erlebbar. </div></div>
</div>
<div class="circle-8">
<a href="https://allcycle.org"><img alt="ALLCYCLE" src="/typo3conf/ext/abz_eff_template/templates_layout4/media/startstart/icon_8.png"/></a><div class="text"><div class="circles"><strong>ALLCYCLE</strong>ALLCYCLE ist Ihr neuer Containerdienst fÃ¼r Germering und Umgebung! Container stellen Dank der einfachen und schnellen Anlieferung und Abholung ein komfortables und flexibles Beladen sicher und sorgen so fÃ¼r eine schnelle Sauberkeit. Die Container bieten Dank des hohen Volumens eine kostengÃ¼nstige MÃ¶glichkeit, kleinere und groÃe Mengen effektiv zu entsorgen. Dank ausgeklÃ¼gelter Logistik lassen sich Standzeiten und damit auch Kosten minimieren.</div></div>
</div>
</div>
</div>
<div class="col-sm-3 news-box">
<div class="news">
<div class="ststnewsheader">Brand News</div>
<div class="news-1">
<div class="csc-default" id="c16018">
<div class="slide">
<a href="https://shop.allnet.de/detail/index/sArticle/329706" target="_top" title="ALLNET CO2 Raumluft-Ampel - Zum infektionsschutzgerechten LÃ¼ften"><img alt="" border="0" height="108" src="/typo3temp/pics/0a311bc1cc.jpg" width="162"/></a>
<strong>ALLNET CO2 Raumluft-Ampel - Zum infektionsschutzgerechten LÃ¼ften</strong>
<p></p>
</div>
<div class="slide">
<a href="https://shop.allnet.de/detail/index/sArticle/327640" target="_top" title="ALL-SGI8016PM - ALLNET 12 Port PoE Industrial Switch"><img alt="" border="0" height="108" src="/typo3temp/pics/a0fd69b3a7.jpg" width="162"/></a>
<strong>ALL-SGI8016PM - ALLNET 12 Port PoE Industrial Switch</strong>
<p></p>
</div>
<div class="slide">
<a href="https://newsletter.allnet.de/mailing/2020/2020-12-14/2020-12-14_0d2f881c2836e83d04e672645eb94aa4_newsletter.html" target="_top" title="ALLNET Medienkonverter - Ãbertragungsmedien schnell und einfach verbinden"><img alt="" border="0" height="108" src="/typo3temp/pics/fa25769bbd.jpg" width="162"/></a>
<strong>ALLNET Medienkonverter - Ãbertragungsmedien schnell und einfach verbinden</strong>
<p></p>
</div>
<div class="slide">
<a href="https://newsletter.allnet.de/mailing/2020/2020-12-14/2020-12-14_e7683b2c1332cc0f436520bcffbec8a5_newsletter.html" target="_top" title="Die wohl umweltfreundlichste &amp; cleverste Powerbank"><img alt="" border="0" height="108" src="/typo3temp/pics/beb7858d7b.jpg" width="162"/></a>
<strong>Die wohl umweltfreundlichste &amp; cleverste Powerbank</strong>
<p></p>
</div>
<div class="slide">
<a href="https://newsletter.allnet.de/mailing/2020/2020-12-14/2020-12-14_3b905521a39c3758c37a769bb966f43f_newsletter.html" target="_top" title="Quick Charge PD USB Netzteile â Flexibel und sehr leistungsfÃ¤hig"><img alt="" border="0" height="108" src="/typo3temp/pics/24ac27421d.jpg" width="162"/></a>
<strong>Quick Charge PD USB Netzteile â Flexibel und sehr leistungsfÃ¤hig</strong>
<p></p>
</div>
</div>
</div>
<div class="divider"></div>
<div class="ststnewsheader">Distributions News</div>
<div class="news-2">
<div class="csc-default" id="c16025">
<div class="slide">
<a href="https://newsletter.allnet.de/mailing//2020-08-17/2020-08-17_13469a5221af63f681a205b3c4a351de_newsletter.html" target="_top" title="Silver Peak â Designed for today's cloud-first enterprise"><img alt="" border="0" height="108" src="/typo3temp/pics/4afbb2f3af.jpg" width="162"/></a>
<strong>Silver Peak â Designed for today's cloud-first enterprise</strong>
<p></p>
</div>
<div class="slide">
<a href="https://crs.allnet.de/x1x9N6vI" target="_top" title="Drahtlose LÃ¶sungen fÃ¼r eine neue Generation"><img alt="" border="0" height="108" src="/typo3temp/pics/03066f6f42.jpg" width="162"/></a>
<strong>Drahtlose LÃ¶sungen fÃ¼r eine neue Generation</strong>
<p></p>
</div>
<div class="slide">
<a href="https://shop.allnet.de/detail/index/sArticle/325492" target="_top" title="Snom M900 Outdoor DECT-IP Basisstation"><img alt="" border="0" height="108" src="/typo3temp/pics/3cca2f2db6.jpg" width="162"/></a>
<strong>Snom M900 Outdoor DECT-IP Basisstation</strong>
<p></p>
</div>
<div class="slide">
<a href="http://www.patton.com/press/Patton-und-Fluxpunkt-integrieren-Microsoft-Teams-in-die-STARFACE-PBX-Umgebung/" target="_top" title="Patton und Fluxpunkt integrieren MS Teams in die STARFACE PBX..."><img alt="" border="0" height="108" src="/typo3temp/pics/767281df7f.jpg" width="162"/></a>
<strong>Patton und Fluxpunkt integrieren MS Teams in die STARFACE PBX...</strong>
<p></p>
</div>
<div class="slide">
<a href="https://crs.allnet.de/gYv6v2s0" target="_top" title="Jetzt den neuen Shelly Katalog kostenlos anfordern!"><img alt="" border="0" height="108" src="/typo3temp/pics/d2df584fa8.jpg" width="162"/></a>
<strong>Jetzt den neuen Shelly Katalog kostenlos anfordern!</strong>
<p></p>
</div>
</div>
</div>
<div class="divider"></div>
<div class="ststnewsheader">Events</div>
<div class="news-3">
<div class="csc-default" id="c16032">
<div class="slide">
<a href="http://maker-faire.de/hannover/" target="_top" title="Maker Faire Hannover, 11. - 12.09"><img alt="" border="0" height="108" src="/typo3temp/pics/4476bb4d46.jpg" width="162"/></a>
<strong>Maker Faire Hannover, 11. - 12.09</strong>
<p></p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="copy">© 1991-2021 ALLNET GmbH Computersysteme<br/>
<a href="http://www.allnet.de/de/allnet-brand/imprint-allnet-products/" style="color: #fff;">Impressum</a> - <a href="http://www.allnet.de/de/allnet-brand/unternehmen/agb/" style="color:#fff;">AGB</a> - <a href="http://www.allnet.de/de/allnet-brand/unternehmen/datenschutz/" style="color:#fff;">Datenschutz</a></div>
</div>
<script src="/typo3conf/ext/sg_solr/Resources/Public/JavaScript/Suggest.js?1457461348" type="text/javascript"></script>
<script src="/typo3conf/ext/sg_solr/Resources/Public/JavaScript/ScrollBrowser.js?1457461348" type="text/javascript"></script>
<script src="/typo3conf/ext/abz_eff_template/templates_layout4/js/analytics.js?1522822841" type="text/javascript"></script>
</body>
</html>
<!-- Cached page generated 13-01-21 00:31. Expires 14-01-21 00:31 -->
