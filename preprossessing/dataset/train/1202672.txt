<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Fritzer Rechtsanwälte in Chemnitz, Dresden, Leipzig, Berlin, Mellrichstadt</title>
<link href="assets/css/master_v2.css" rel="stylesheet"/>
<!--[if lt IE 9]>
    <script type="text/javascript">
      document.createElement("nav");
      document.createElement("header");
      document.createElement("footer");
      document.createElement("section");
      document.createElement("aside");
      document.createElement("article");
      document.createElement("hgroup");
    </script>
    <![endif]-->
</head>
<body>
<div class="loadingSite"></div><div id="master">
<div id="fundament"></div>
<header>
<div id="logo">
<a href="#" title="Rechtsanwälte Fritzer">Rechtsanwälte Fritzer</a>
</div>
<nav>
<ul>
<li class="selected"><a href="#de">DE</a></li>
<!--li><a href="#en">EN</a></li-->
<li class="modal-click"><a class="ajax_modal" href="#kontakt-modal">Kontakt</a></li>
<li class="last modal-click"><a class="ajax_modal" href="#impressum-modal">Impressum</a></li>
</ul>
</nav>
</header>
<div id="two">
<ol>
<li class="kanzlei" id="listItem0">
<h2><span>Slide One</span></h2>
<div>
<div class="headerimage">
<div id="ra-tabs">
<a class="tooltip" href="#barbara-fritzer" title="Barbara Fritzer">Barbara Fritzer</a>
<!--a href="#anja-weber"  class="tooltip" title="Anja Weber">Anja Weber</a-->
</div>
</div>
<div class="subnav">
<ul>
<li><a data-himg="assets/images/pics/kanzlei-0.jpg" href="#ueber-uns">Über Uns</a></li>
<li><a data-himg="assets/images/pics/kanzlei-1.jpg" href="#anwaelte" id="ras">Die Rechtsanwälte</a></li>
<li><a data-himg="assets/images/pics/kanzlei-0.jpg" href="#anspruch">Unser Anspruch</a></li>
</ul>
</div>
<div class="tabs">
<div class="tab_content" id="ueber-uns">
<h3>Über Uns</h3>
<p>Seit über 20 Jahren können wir auf eine erfolgreiche Tätigkeit in einem unserer Kernbereiche – der Betreuung von Insolvenzverfahren sowie die Umsetzung von Sanierungs- und Restrukturierungskonzepten - zurückblicken.
Rechtsanwältin Barbara Fritzer wurde in einer Vielzahl von Verfahren selbst zur Verwalterin bzw. Insolvenzverwalterin oder Treuhänderin in den Bundesländern Sachsen, Thüringen, Sachsen-Anhalt, Brandenburg, Berlin und Bayern bestellt.</p><p>Daneben gewinnt auch immer mehr der Bereich der Unternehmenssanierung an Bedeutung – sei es mit den Instrumenten des Insolvenzrechtes oder auch außerhalb eines insolvenzrechtlichen Ordnungsverfahrens.</p><p>Wir beraten weiterhin umfassend auf dem Gebiet des Energiewirtschaftrechts und der rechtlichen Gestaltung von Forschungs- und Entwicklungskooperationen.</p><p>Die Neuordnung im Energiesektor eröffnet Chancen für Wettbewerber vor allem im Bereich Erneuerbarer Energien. Projektgesellschaften und Anlagenbetreiber sind regelmäßig mit Problemen im Rahmen des Genehmigungsverfahrens, des Netzanschlusses und vergütungsrechtlicher Fragen konfrontiert.</p>
</div>
<div class="tab_content" id="anwaelte">
<div class="sub_tab_content" id="barbara-fritzer">
<h3>Barbara Fritzer</h3>
<p><strong>Fachgebiete</strong><br/>
	     	
	     	- Insolvenzrecht und Sanierung<br/>
	     	- Allgemeines Wirtschaftsrecht<br/>
	     	- Recht der Forschungs- und Entwicklungskooperationen<br/>
	     	- Energierecht</p>
<p><strong>CURRICULUM VITAE</strong><br/>

- seit 1994 Rechtsanwältin in Dresden<br/>
- seit 1996 Insolvenzverwalterin und Treuhänderin an den Amtsgerichten<br/>   Dresden, Chemnitz, Erfurt, Schweinfurt, Cottbus, Berlin, Halle/Saale und Hof/Bayern<br/>
- 1997-2004 Sozia einer überregionalen mittelständischen Anwaltskanzlei<br/>
- seit 2005 Geschäftsführung FRITZER Rechtsanwälte
</p>
<p><strong>Mitgliedschaften</strong><br/>
	     Mitglied im Verband Insolvenzverwalter Deutschlands e. V. – V.I.D.  • Mitglied der Arbeitsgemeinschaft für Insolvenzrecht im Deutschen Anwaltverein - DAV • Mitglied der europäischen Organisation der Insolvenz- und Konkursverwalter – INSOL Europe • Mitglied im Internationalen Wirtschaftsforum für regenerative Energien  • Mitglied im Fachverband Biogas e. V.</p>
<p><strong>Sprachen</strong><br/>
	     Deutsch • Englisch • Französisch</p>
</div>
<div class="sub_tab_content" id="anja-weber">
<h3>Anja Weber</h3>
<p><strong>Fachgebiete</strong><br/>
	  	- Allgemeines Wirtschaftsrecht<br/>
	  	- Handels- und Gesellschaftsrecht<br/>
	  	- Insolvenzrecht</p>
<p><strong>Kurzbeschreibung</strong><br/>
	  • seit 2009 Rechtsanwältin der Kanzlei FRITZER Rechtsanwälte • 2005 - 2009 Rechtsanwältin in der Kanzlei Pfefferle, Koch, Helberg &amp; Partner • 2004 - 2005 Tätigkeit in der Rechts- und Personalabteilung der Marsh GmbH in Stuttgart • 2004 Zweites Juristisches Staatsexamen • 2002 - 2004 Rechtsreferendariat am LG Bautzen • 2002 Erstes Juristisches Staatsexamen • 1995 – 2002 Studium der Rechtswissenschaften in Dresden</p>
<p><strong>Mitgliedschaften</strong><br/>
	  • Mitglied im Thüringer Arbeitskreis für Unternehmens- und Insolvenzrecht e.V. • Mitglied im Verband junger Insolvenzverwalter • Community-Leiterin des Netzwerkes Unternehmerinnenabend.de in Erfurt</p>
<p><strong>Sprachen</strong><br/>
	 Deutsch • Englisch </p>
</div>
</div>
<div class="tab_content" id="anspruch">
<h3>Unser Anspruch</h3>
<p>Manches ist gut, manches geht besser. Unsere Erfahrung zeigt überraschend oft, dass ein Unternehmen nicht von Kopf bis Fuß umgekrempelt werden muss. </p><p>Die betriebserhaltende Sanierungslösung steht bei uns im Vordergrund bei der Neupositionierung eines Unternehmens.</p><p>Mit Feingefühl für die komplexen Zusammenhänge im Unternehmen können wenige, konsequent umgesetzte Änderungen das Boot wieder ins Gleichgewicht bringen. Wir entwickeln Sanierungsstrategien und können aufgrund langjähriger Erfahrung im Bank- und Finanzrecht Konzepte für eine Neuausrichtung des Unternehmens erstellen.</p></div>
</div>
<li class="standorte" id="listItem1">
<h2><span>Slide Two</span></h2>
<div>
<div class="headerimage"></div>
<div class="subnav-noTabContent">
<ul>
<li><a data-himg="assets/images/pics/dresden.jpg" href="#standort-dresden">Dresden</a></li>
<li><a data-himg="assets/images/pics/berlin.jpg" href="#standort-berlin">Berlin</a></li>
<li><a data-himg="assets/images/pics/chemnitz.jpg" href="#standort-chemnitz">Chemnitz</a></li>
<li><a data-himg="assets/images/pics/erfurt.jpg" href="#standort-erfurt">Erfurt</a></li>
</ul>
</div>
<div class="cart">
<img alt="Standort Dresden" class="tooltip" id="standort-dresden" src="assets/images/de/standorte/dresden.png"/>
<img alt="Standort Berlin" class="tooltip" id="standort-berlin" src="assets/images/de/standorte/berlin.png"/>
<img alt="Standort Chemnitz" class="tooltip" id="standort-chemnitz" src="assets/images/de/standorte/chemnitz.png"/>
<img alt="Standort Erfurt" class="tooltip" id="standort-erfurt" src="assets/images/de/standorte/erfurt.png"/>
<a class="standort-dresden" data-himg="assets/images/pics/dresden.jpg" href="#standort-dresden">Dresden</a>
<a class="standort-berlin" data-himg="assets/images/pics/berlin.jpg" href="#standort-berlin">Berlin</a>
<a class="standort-chemnitz" data-himg="assets/images/pics/chemnitz.jpg" href="#standort-chemnitz">Chemnitz</a>
<a class="standort-erfurt" data-himg="assets/images/pics/erfurt.jpg" href="#standort-erfurt">Erfurt</a>
</div>
</div>
</li>
<li class="leistungen" id="listItem2">
<h2><span>Slide Three</span></h2>
<div>
<div class="headerimage"></div>
<div class="subnav">
<ul>
<li><a data-himg="assets/images/pics/header-leistungen.jpg" href="#wirtschafts-insolvenzrecht">Wirtschafts- &amp;<br/> Insolvenzrecht</a></li>
<li><a data-himg="assets/images/pics/header-leistungen.jpg" href="#sanierung-unternehmensnachfolge">Sanierung &amp;<br/> Unternehmensnachfolge</a></li>
<li><a data-himg="assets/images/pics/header-leistungen.jpg" href="#energiewirtschaft">Energiewirtschaft</a></li>
<li><a data-himg="assets/images/pics/header-leistungen.jpg" href="#forschung-entwicklung">Forschung &amp; <br/> Entwicklung</a></li>
</ul>
</div>
<div class="tabs">
<div class="tab_content" id="wirtschafts-insolvenzrecht">
<h3>Wirtschafts- und Insolvenzrecht</h3>
<p>Das allgemeine Wirtschaftsrecht mit den Schwerpunkten des Insolvenzrechts, des Handels-und Gesellschaftsrechts, des Bankrechts sowie des Rechts der Kreditsicherheiten gehört zu den Kernkompetenzen unserer Kanzlei.</p><p>Wir betreuen eine Vielzahl laufender Insolvenzverfahren und können auf erfolgreiche Sanierungslösungen und Unternehmensrestrukturierungen zurückblicken.</p><p>Hierbei rücken zunehmend Fragen des Europäischen Wirtschaftsrechts und des Internationalen Insolvenzrechts in den Kontext. Um diesen Herausforderungen gerecht zu werden, haben wir unsere Tätigkeit auch im Besonderen auf die Betreuung von Sekundärinsolvenzverfahren im Rahmen grenzüberschreitender Insolvenzverfahren ausgerichtet.</p><p>Diese Kompetenz wird durch unsere Spezialkenntnisse auf den Gebieten des Energierechts sowie der Vertragsgestaltung von Forschungs- und Entwicklungskooperationen zusätzlich abgerundet. Hierdurch sind wir auch in der Lage, den besonderen Anforderungen an die Betreuung von Unternehmensinsolvenzen in diesen Bereichen gerecht zu werden. </p>
</div>
<div class="tab_content" id="sanierung-unternehmensnachfolge">
<h3>Sanierung und Unternehmensnachfolge</h3>
<p>Die Sanierung von Unternehmen gewinnt an Bedeutung, um Arbeitsplätze, Kapital und das Unternehmen als Marktteilnehmer zu erhalten. Unsere Kanzlei hat sich bereits im Bereich Insolvenz auf die Fortführung der Unternehmen konzentriert. Durch die Neuerungen des Insolvenzrechts ist es uns möglich geworden, viele Unternehmen zu sanieren und nach erfolgreicher Sanierung als verlässlichen Partner im Marktgeschehen agieren zu lassen. Unsere Juristen, Wirtschaftswissenschaftler und Steuerberater helfen dem Unternehmen, schnell aus schwierigen Situationen herauszufinden. Mit unserer umfangreichen Erfahrung im Hintergrund helfen wir jederzeit gern, Krise und Insolvenz zu vermeiden.</p>
<p>Zu einem weiteren Beratungsgebiet entwickelt sich zunehmend die Thematik der Unternehmensnachfolge. So vielen Unternehmen wie nie zuvor steht in den kommenden Jahren der Wechsel von einer Hand in die andere bevor. Dabei ist für das Unternehmen entscheidend, dass dies mit ausreichend Planungsvorlauf für die rechtliche und steuerliche Einbettung erfolgt.</p>
<p>Unsere Kanzlei steht Ihnen und Ihrem Unternehmen hierbei als erfahrener Partner zur Verfügung.</p>
</div>
<div class="tab_content" id="energiewirtschaft">
<h3>Energiewirtschaft</h3>
<p>Wir haben unsere Kernkompetenzen erweitert und beraten mittelständische Unternehmen in allen Fragen des Energierechts. Vor allem Rechtsfragen im Bereich Erneuerbarer Energien bilden einen Schwerpunkt der beratenden Tätigkeit. Wir entwickeln Konzepte einer künftigen Gesellschaftsstruktur und sind ein wichtiger Partner in allen Fragen der Planung und Durchführung von Biogas- und Photovoltaikprojekten.</p>
<p>Im Rahmen der Genehmigung einer Anlage sowie des Netzanschlusses ergeben sich in der Regel eine Vielzahl von Rechtsproblemen. Diese reichen von den Anforderungen an immissionsschutzrechtliche Vorgaben bis zu den Einzelheiten der Vergütung für den eingespeisten Strom.</p><p>Unser Beratungsansatz erstreckt sich von der Klärung einzelner Rechtsfragen und der Wahrnehmung rechtlicher Interessen bis zur Betreuung eines ganzen Vorhabens – vom Genehmigungsverfahren bis zur Gestaltung der Verträge mit den Netzbetreibern.</p>
</div>
<div class="tab_content" id="forschung-entwicklung">
<h3>Forschung und Entwicklung</h3>
<p>Wir beraten umfassend zu Rechtsfragen auf den Gebieten der Forschung und Entwicklung.</p>
<p>Ein Schwerpunkt bildet hierbei die Gestaltung horizontaler und vertikaler Forschungs- und Entwicklungsverträge. Kooperationen in diesem Bereich erfordern spezielle Rechtskenntnisse des Patent-, Marken- und Lizenzrechtes.</p><p>Regelmäßig sind Eigentums- und Verwertungsrechte an entwickelten Schutzrechten sowie an erworbenem technischem Know-how von existenzieller Bedeutung für die beteiligten Kooperationspartner.</p><p>Wir entwickelten Konzepte, um in diesem Bereich für die nötige Rechtssicherheit zu sorgen.</p>
</div>
</div>
</div>
</li>
<li class="aktuelles" id="listItem3">
<h2><span>Slide Four</span></h2>
<div>
<div class="headerimage"></div>
<div class="subnav">
<ul>
<li><a data-himg="assets/images/pics/header-aktuelles.jpg" href="#aktuelles-kanzlei">Neues aus der Kanzlei</a></li>
<li><a data-himg="assets/images/pics/header-aktuelles.jpg" href="#links">Links</a></li>
</ul>
</div>
<div class="tabs">
<div class="tab_content" id="aktuelles-kanzlei">
<p><b>PRAKTIKANT (w/m/d) Jr. Consultant Unternehmensnachfolge / Mergers &amp; Acquisitions</b></p>
<hr/>
<p>Seit Jahren beraten wir Unternehmen im Bereich des Unternehmensverkaufs- und Nachfolge. Dabei erstrecken sich unsere Leistungen von der betriebswirtschaftlichen Analyse hin zur Verhandlungsführung mit Investoren. Wir begleiten den Mittelstand und bereiten Ihn auf neue Herausforderungen vor!<br/>
<br/>
<b>Das erwartet Sie bei uns – Erfahrungen, von denen Sie ein Leben lang profitieren</b><br/>
Als Teil unseres M&amp;A Teams begleiten Sie einzelne Prozesse im Verkauf von Unternehmen als interner Berater und unterstützen Senior Consultants und die Führungsebene. Dabei erwartet Sie eine Vielzahl von Aufgaben:<br/>
<br/>
- Analyse von einzelnen Unternehmensbereichen<br/>
- Erstellung von verschiedenen Bewertungsmodellen<br/>
- Koordination zwischen externen Partnern und internen Consultants<br/>
- Unterstützung in der Aufbereitung von Unternehmensportraits<br/>
- Erstellung von Business-Präsentationen und Strategien<br/>
<br/>
<b>Das erwarten wir von Ihnen!</b><br/>
<br/>
- Immatrikulation in ein Studium der Wirtschaftswissenschaften, Wirtschaftsrecht, Wirtschaftsinformatik oder Vergleichbares – mind. 3 Semester<br/>
- Erste praktische Erfahrung<br/>
- Hohe Affinität zum unternehmerischen Denken<br/>
- Motivation und Einsatzbereitschaft für eigene Projekte<br/>
- Flexibilität, Verantwortungsbewusstsein und hohe Detailorientiertheit<br/>
- Konzeptionelle, analytische und kreative Arbeitsweise<br/>
- Gute Englischkenntnisse<br/>
- Sicherer Umgang mit MS-Office<br/><br/>
<b>Die Praktikumsdauer beträgt 3 Monate mit flexiblen Arbeitszeiten.</b><br/>
<br/>
Haben wir Ihr Interesse geweckt?<br/>
Dann senden Sie uns Ihre Bewerbung (Anschreiben + Lebenslauf).<br/>
Ansprechpartner: Ron Schimkat<br/>
ron.schimkat@ra-fritzer.de</p>
</div>
<div class="tab_content" id="links">
<p>Das Sächsische Staatsministerium der Justiz stellt hier Informationen zu einzelnen Verfahren bei den sächsischen Amtsgerichten bereit. <a href="https://www.justiz.sachsen.de/gerichtstafel" target="_blank">https://www.justiz.sachsen.de/gerichtstafel </a></p><br/>
<p>Einen Überblick über aktuelle Verfahrensrichtlinien bietet <a href="https://www.rws-verlag.de/" target="_blank">https://www.rws-verlag.de/ </a>, Informationen zu den Gerichten in Bayern erhalten Sie bei <a href="https://www.justiz.bayern.de/" target="_blank">https://www.justiz.bayern.de/</a></p> </div>
</div>
</div>
</li>
<li class="partner" id="listItem4">
<h2><span>Slide Five</span></h2>
<div>
<div class="headerimage"></div>
<div class="subnav">
<ul>
<li><a data-himg="assets/images/pics/header-partner.jpg" href="#unsere-partner">Unsere Partner</a></li>
</ul>
</div>
<div class="tabs">
<div class="tab_content" id="unsere-partner">
<h3>Unsere Partner</h3>
<p>
<a href="https://provenio.de" target="_blank" title="PROVENIO Unternehmensberatung GmbH"><img alt="logo-provenio" src="assets/images/logo-provenio.jpg" width="250"/></a>
<a href="https://advice-steuerberatung.de/" target="_blank" title="advice Steuerberatung Peter Rost"><img alt="logo-advice" src="assets/images/advice_steuerberatung.jpg" width="250"/></a></p>
</div>
</div>
</div>
</li>
<noscript>
<p>Please enable JavaScript to get the full experience.</p>
</noscript>
</div><div id="modal"><a class="close-modal" href="#close-modal">X</a></div>
</li></ol></div>
<!--	</div>-->
<div class="modal_content" id="kontakt-modal">
<table><tr><td>
<form accept-charset="utf-8" action="sendmail.php" id="contactform" method="post">
<h2>Kontakt</h2>
<p class="errormsg" id="badcaptcha" style="display:none;">Die Rechenaufgabe wurde nicht richtig gelöst.</p>
<p class="successmsg" id="success" style="display:none;">Ihre E-Mail wurde erfolgreich gesendet!</p>
<p class="errormsg" id="bademail" style="display:none;">Bitte geben Sie eine gültige E-Mail Adresse ein, Name darf nicht leer sein!</p>
<p class="errormsg" id="badserver" style="display:none;">Beim senden der E-Mail ist ein Fehler aufgetereten, bitte versuchen Sie es später noch einmal</p>
<label for="name">Ihr Name: *</label>
<input id="nameinput" name="name" type="text" value=""/>
<label for="email">Ihre E-Mail Adresse: *</label>
<input id="emailinput" name="email" type="text" value=""/>
<label for="security">Sicherheitsabfrage: * 
		<span class="math">5 + 4 = </span></label>
<input class="mathfield" id="math" name="math" type="text" value=""/>
<input name="math-answer" type="hidden" value="9"/>
<label for="comment">Ihre Nachricht: *</label>
<textarea cols="20" id="commentinput" name="comment" rows="7"></textarea><br/>
<input class="submit" id="submitinput" name="submit" type="submit" value="Nachricht abschicken"/>
<!--<input type="hidden" id="from_email" name="from_email" value=""/>-->
</form>
</td><td>
<div id="map"><h2>Anfahrt Dresden</h2>
<div id="map-inner"></div>
<!--<a href="https://maps.google.com/maps?ll=51.077785.750026&z=16&t=m&hl=de-DE" target="_blank" class="button-route">Anfahrt planen</a>-->
</div>
</td></tr>
</table>
</div>
<div class="modal_content" id="impressum-modal">
<h2>Impressum</h2>
<p><strong>FRITZER Rechtsanwälte</strong><br/>
Erna-Berger-Straße 3 <br/>
01097 Dresden</p>
<p>Tel.: +49.(0)351.20573-0 <br/>
Fax: +49.(0)351.20573-11 <br/>
Mail: <a href="mailto:info@ra-fritzer.de">info@ra-fritzer.de</a></p>
<p>Geschäftsführerin Rechtsanwältin Barbara Fritzer<br/> 
USt-Id-Nr. DE19731800 <br/>
Gerichtsstand: Dresden </p>
<p>Die in der Kanzlei FRITZER Rechtsanwälte tätigen Anwälte haben ihre Berufszulassung in Deutschland erworben und sind Mitglieder der jeweils nachfolgend aufgeführten Rechtsanwaltskammer:</p>
<p><strong>Rechtsanwältin Barbara Fritzer</strong><br/>

Rechtsanwaltskammer Sachsen,  Glacisstrasse 6, 01099 Dresden, 
Internet: <a href="https://www.rak-sachsen.de">www.rak-sachsen.de</a></p>
<p><strong>Rechtsanwältin Barbara Fritzer</strong><br/>
Allianz Versicherungs-AG, 10900 Berlin, Internet: <a href="https://www.allianz.de/">www.allianz.de</a></p>
<p>Die berufsrechtlichen Regelungen für Rechtsanwälte richten sich insbesondere nach der BRAO, das RVG, die BORA, Grundlagen der Deutschen Anwaltsgebühren sowie die Berufsregeln der Rechtsanwälte der Europäischen Gemeinschaft (www.brak.de) 
Die berufsrechtlichen Regelungen für Insolvenzverwalter richten sich in der Kanzlei FRITZER Rechtsanwälte nach der InsO, der InsVV sowie den Berufsgrundsätzen der Insolvenzverwalter (www.vid.de ) </p>
<p>Auf dieser Homepage veröffentlichte Links wurden mit Sorgfalt recherchiert und zusammengestellt. Die Redaktion hat keinen Einfluss auf die aktuelle und zukünftige Gestaltung und Inhalte der verlinkten Seiten. Die Redaktion ist nicht für den Inhalt der verknüpften Seiten verantwortlich und macht sich den Inhalt nicht zu eigen. Für illegale, fehlerhafte oder unvollständige Inhalte sowie für Schäden, die durch die Nutzung oder Nichtnutzung der Informationen entstehen, haftet allein der Anbieter der Web-Site, auf die verwiesen wurde.</p>
<p><strong>Verantwortlich für „www.ra-fritzer.de“ gemäß § 6 MDStV ist:</strong><br/>
Rechtsanwältin Barbara Fritzer, Erna-Berger-Straße 3, 01097 Dresden.</p>
<p><strong>Konzept, Layout, Design &amp; Programmierung</strong><br/> 
K&amp;C Advertising KG | <a href="https://www.kucwerbung.de/">www.kucwerbung.de</a></p>
<p><strong>Technische Betreuung</strong><br/>
Firma Datom [www.datom.de]</p>
</div>
<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<!--<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>--> <!-- easing -->
<script src="assets/js/jquery.easing.js"></script>
<!-- liteAccordion js -->
<script src="assets/js/liteaccordion.jquery.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/core.js"></script>
</div></body>
</html>