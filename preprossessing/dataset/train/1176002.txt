<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html dir="ltr" lang="de" xml:lang="de" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title> Princely International University</title>
<meta content="Distance Learning, Distance Learning Courses, Distance Learning Education, Distance Learning Study " name="keywords"/>
<meta content=" princely.university offers distance learning courses, study  and education that are specifically designed for working professionals." name="description"/>
<link href="css/css.css" rel="stylesheet" type="text/css"/>
<link href="css/css-740.css" rel="stylesheet" type="text/css"/>
<link href="css/css-400.css" rel="stylesheet" type="text/css"/>
<link href="images/snicon.png" rel="shortcut icon" type="image/x-icon"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/js.js" type="text/javascript"></script>
<!-- Facebook Pixel Code -->
<script>

!function(f,b,e,v,n,t,s)


{if(f.fbq)return;n=f.fbq=function(){n.callMethod?


n.callMethod.apply(n,arguments):n.queue.push(arguments)};


if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';


n.queue=[];t=b.createElement(e);t.async=!0;


t.src=v;s=b.getElementsByTagName(e)[0];


s.parentNode.insertBefore(t,s)}(window,document,'script',


'https://connect.facebook.net/en_US/fbevents.js');


fbq('init', '1934367179909185'); 


fbq('track', 'PageView');

</script>
<noscript>
<img height="1" src="https://www.facebook.com/tr?id=1934367179909185&amp;ev=PageView
&amp;noscript=1" width="1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<!-- Global site tag (gtag.js) - Google Ads: 775576209 --> <script async="" src="https://www.googletagmanager.com/gtag/js?id=AW-775576209"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-775576209'); </script>
<!-- Event snippet for Fill Application Form conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. --> <script> function gtag_report_conversion(url) { var callback = function () { if (typeof(url) != 'undefined') { window.location = url; } }; gtag('event', 'conversion', { 'send_to': 'AW-775576209/FR_8CIfWzpYBEJG16fEC', 'event_callback': callback }); return false; } </script>
<meta content="gtag" name="amp-google-client-id-api"/> <script async="" custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
<!-- Global site tag (gtag) - Google Ads: 775576209  <amp-analytics type="gtag" data-credentials="include"> <script type="application/json"> { "vars": { "gtag_id": "AW-775576209", "config": { "AW-775576209": { "groups": "default" } } }, "triggers": { "C_5Ir3ri9myH4": { "on": "click", "selector": "CSS_SELECTOR", "vars": { "event_name": "conversion", "send_to": ["AW-775576209/FR_8CIfWzpYBEJG16fEC"] } } } } </script> </amp-analytics> -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-119281563-1"></script>
<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());

 

  gtag('config', 'UA-119281563-1');

</script>
<link href="css/princely.css" rel="stylesheet" type="text/css"/>
<script src="js/scroller.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="js/stmenu.js" type="text/javascript"></script>
<!--<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>-->
<meta content="Ov-0MaBX_XnFI-8SJhWyZy00DsLQvRDXFwIMhUo0lcE" name="google-site-verification"/>
<meta content="15D8EA1093709FC336808A18E1C762C3" name="msvalidate.01"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-119281563-1"></script>
<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());

 

  gtag('config', 'UA-119281563-1');

</script>
</head>
<body>
<a href="application.php">
<img class="anbg" src="images/anbg.png"/></a>
<!-- BEGIN ProvideSupport.com Graphics Chat Button Code -->
<div class="lsbg" id="cinK0C"></div><div class="lsbg" id="scnK0C"></div><div id="sdnK0C" style="display:none"></div><script type="text/javascript">var senK0C=document.createElement("script");senK0C.type="text/javascript";var senK0Cs=(location.protocol.indexOf("https")==0?"https":"http")+"://image.providesupport.com/js/princely/safe-standard.js?ps_h=nK0C&ps_t="+new Date().getTime();setTimeout("senK0C.src=senK0Cs;document.getElementById('sdnK0C').appendChild(senK0C)",1)</script><noscript><div class="lsbg" style="display:inline"><a href="http://www.providesupport.com?messenger=princely">Online Help Desk</a></div></noscript>
<!-- END ProvideSupport.com Graphics Chat Button Code -->
<!--<img src="images/lsbg.png" class="lsbg"  />-->
<script>
$(document).ready(function() {
    $('.lsbg img').css('display','none');
});
</script>
<a href="index.php#newsprograms">
<img class="nlbg" src="images/nlbg.png"/></a>
<input id="base" type="hidden" value="http://princely.university/"/>
<div class="center_container">
<div class="top">
<div class="logotop">
<a href="index.php">
<img class="toplogo" src="images/logo.png"/></a>
<h2>PRINCELY International University</h2>
<a href="arabic/index.php">
<h3>Ar</h3>
</a>
<a href="french/index.php">
<h3>Fr</h3>
</a>
<a href="">
<h3 class="activelang">En</h3>
</a>
</div>
<div class="menu">
<div class="menu-nav"></div>
<ul class="needclass">
<li><a href="index.php"><h4 class="homemenu"> </h4></a></li>
<li><h4 class="clickparent" id="1">About Princely</h4>
<ul class="child" id="ulchild_1">
<li><a href="aboutus.php"><h4>Who are we ?</h4></a></li>
<li><a href="welcome.php"><h4>Welcome Note</h4></a></li>
<li><a href="princeleyadv.php"><h4>Princely Advantages</h4></a></li>
<li><a href="offices.php"><h4>Offices</h4></a></li>
</ul>
</li>
<li><h4 class="clickparent" id="2">Admission Process</h4>
<ul class="child" id="ulchild_2">
<li><a href="process.php"><h4>Process</h4></a></li>
<li><a href="requisite.php"><h4>Requisite</h4></a></li>
<li><a href="application.php"><h4>APPLY NOW</h4></a></li>
</ul></li>
<li><h4 class="clickparent" id="3">Majors Offered</h4>
<ul class="child" id="ulchild_3">
<li><a href="majors.php"><h4>Majors</h4></a></li>
<li><a href="programs.php"><h4>Programs</h4></a></li>
</ul></li>
<li><a href="feestruct.php"><h4>Fee Structure </h4></a></li>
<li><a href="verificationserv.php"><h4>Verification Service</h4></a></li>
<li><a href="contactus.php"><h4 style="background:none;padding-left:0 ; min-width:50px">Contact Us</h4></a></li></ul>
<ul id="menu-nau-ul">
<li><a href="index.php"><h4 class="homemenu"> </h4></a></li>
<li><h4 class="clickparent" id="1">About Princely</h4>
<ul class="child" id="ulchild_1">
<li><a href="aboutus.php"><h4>Who are we ?</h4></a></li>
<li><a href="welcome.php"><h4>Welcome Note</h4></a></li>
<li><a href="princeleyadv.php"><h4>Princely Advantages</h4></a></li>
<li><a href="offices.php"><h4>Offices</h4></a></li>
</ul>
</li>
<li><h4 class="clickparent" id="2">Admission Process</h4>
<ul class="child" id="ulchild_2">
<li><a href="process.php"><h4>Process</h4></a></li>
<li><a href="requisite.php"><h4>Requisite</h4></a></li>
<li><a href="application.php"><h4>APPLY NOW</h4></a></li>
</ul></li>
<li><h4 class="clickparent" id="3">Majors Offered</h4>
<ul class="child" id="ulchild_3">
<li><a href="majors.php"><h4>Majors</h4></a></li>
<li><a href="programs.php"><h4>Programs</h4></a></li>
</ul></li>
<li><a href="feestruct.php"><h4>Fee Structure </h4></a></li>
<li><a href="verificationserv.php"><h4>Verification Service</h4></a></li>
<li><a href="contactus.php"><h4 class="contact_menu" style="background:none">Contact Us</h4></a></li></ul>
<a href="https://www.facebook.com/Princely.university"><img class="fb" src="images/fsi.png"/></a>
<a href="https://www.linkedin.com/company/international-educational-consultants-iec-?trk=top_nav_home"><img class="ln" src="images/insi.png"/></a>
</div>
</div>
</div><div class="center_container">
<div class="maindata">
<div class="bannerhome">
<div id="slideshow">
<div class="next"></div>
<div class="prev"></div>
<div class="banabsolute">
<div class="bancontainer">
<img src="images/ban-1.jpg"/>
<h3>Get your Bachelor Degree with 130 credits.</h3>
</div>
<div class="bancontainer">
<img src="images/ban-2.jpg"/>
<h3>Choose your master from a multitude of Majors.</h3>
</div>
</div></div>
<a href="login.php?type=1">
<div class="firstlink">
<div class="areainner">
<h2>Student Area</h2>
<h3>Princely existing students private area</h3>
</div>
</div></a>
<a href="login.php?type=0">
<div class="ndlink">
<div class="areainner">
<h2>Faculty Area</h2>
<h3>Princely faculty members private area</h3>
</div>
</div></a>
<a href="application.php">
<div class="rdlink">
<div class="applyinner">
<h3>Get Your Degree</h3>
<h2>Apply Now</h2>
</div>
</div></a>
<div class="shadowtitle">
</div>
<div class="bannernews">
<div class="lisicon">
</div>
<div class="relativenews">
<div class="absolutenews">
<a href="news.php?news_id=13"><h3 class="innernews">Princely International University Receives 2014 Best of Manhattan Award</h3></a>
<a href="news.php?news_id=5"><h3 class="innernews">PIU has obtained a Certificate of Institutional Validation and has also achieved Accreditation Candidate Status.</h3></a>
<a href="news.php?news_id=6"><h3 class="innernews">Students From Nigeria &amp; Africa are welcome to apply to PIU.</h3></a>
<a href="news.php?news_id=7"><h3 class="innernews">Students from Nigeria &amp; Africa has the right to apply for Financial aid.</h3></a>
<a href="news.php?news_id=14"><h3 class="innernews">IAO certificate of International Accreditation has been granted to PIU</h3></a>
<a href="news.php?news_id=15"><h3 class="innernews">According to the Swiss Institute for Quality Standards (SIQS) and EBA research your companys quality standards, technologies, staff and modern management methods meet international quality standards.</h3></a>
<a href="news.php?news_id=16"><h3 class="innernews">The EBA Nomination Council has selected Princely International University as a nominee for the prestigious award in the quality and management sphere. The European Quality Award (patent #2351135)</h3></a>
</div>
</div>
<div class="larrownews"></div>
<div class="rarrownews"></div>
</div>
</div>
<div class="welcomediv">
<h2>Welcome to <b class="bigredtitle">Princely</b>!</h2>
<p>One of the great benefits of being part of Princely International University is that through your distance learning study, you will be supported by a continuous education expert team that is dedicated to helping working adults, from all ages, in their distance education to meet professional goals.</p>
<p>We all have preconceived notions and myths closing the doors to future opportunities of success, still distance learning is a key to be on that road to success.<br/>
Some find it hard to believe that a perfectly valid degree can be legitimately earned via distance learning and yet, it is possible. In fact, most companies &amp; corporations today highly consider employees showing the willpower, responsibility &amp; motivation for self-improvement through distance learning continuing education.</p>
<a href="welcome.php">
<img src="images/readmore.png"/>
</a>
</div>
<div class="ourprogrmas">
<a href="programs.php">
<h2>Our Programs</h2></a>
<div class="innerprograms">
<h3>. Bachelorâs Program</h3>
<p>Get your Bachelor Degree with 130 credits.</p>
<a href="programs.php?program=1">
<img src="images/readmore.png"/>
</a>
</div>
<div class="innerprograms">
<h3>. PhD Program</h3>
<p>Get Your Doctorate by choosing your own thesis.</p>
<a href="programs.php?program=3">
<img src="images/readmore.png"/>
</a>
</div>
<div class="innerprograms">
<h3>. Mastersâs Program</h3>
<p>Choose your master from a multitude
of Majors.</p>
<a href="programs.php?program=2">
<img src="images/readmore.png"/>
</a>
</div>
<div class="innerprograms">
<h3>. Expert Program</h3>
<p> 	Specially designed for employee looking
to expand their know-how.</p>
<a href="programs.php?program=4">
<img src="images/readmore.png"/>
</a>
</div>
<div class="programsbanner">
<input id="base" type="hidden" value=""/>
<img class="bannerprograms" id="i_1" src="images/ban-2.jpg" style="display:block"/>
<img class="bannerprograms" id="i_2" src="images/ban-1.jpg"/>
<div class="bullets">
<img class="bulletsbanners" id="b_1" src="images/ba.png"/>
<img class="bulletsbanners" id="b_2" src="images/bia.png"/>
</div>
</div>
<div class="newsprograms" id="newsprograms">
<h4>Newsletter</h4>
<h5>Subscribe to our newsletter</h5>
<form action="" method="post">
<input name="name" onblur="javascript:if(this.value==''||this.value==null) this.value='Enter your Fullname'" onfocus="javascript:if(this.value=='Enter your Fullname') this.value=''" required="required" type="text" value="Enter your Fullname"/>
<input name="email" onblur="javascript:if(this.value==''||this.value==null) this.value='Enter your Email'" onfocus="javascript:if(this.value=='Enter your Email') this.value=''" required="required" type="text" value="Enter your Email"/>
<input id="Subscribe" name="Subscribe" type="submit" value=""/>
</form>
</div><div class="studentdiv">
<h2>Student Guide</h2>
<h3>. Student Guide</h3>
<p>Students can find in this section all the  details and guidance related to application and studying procedures.  </p>
<a href="guide.php">
<img src="images/readmore.png"/>
</a>
</div>
</div>
<div class="rightheader">
<div class="tastemon">
<h2>Student Testimonials</h2>
<!--<p>My experience pursuing my degree with Princely International University has been a rewarding experience in my academic career.</p>
<h3>Majed Awwad (2006)</h3>-->
<script language="javascript">
											new pausescroller(pausecontent, "pscroller1", "smallcontent", 12000)	
										</script>
</div>
<div class="partnership">
<a href="partnership.php">
<h2>Partnership Program</h2></a>
<h3>. College partnership</h3>
<p>Deliver your students a Princely Degree by Affiliation.</p>
<h3>. Representatives</h3>
<p>Become a Princely representative in your Country.</p>
<a href="partnership.php">
<img src="images/readmore.png"/></a>
</div>
<div class="jobopp">
<a href="job.php">
<h2>Job Opportunity</h2></a>
<p>We have several opportunities in our Academic and Managerial divisions.</p>
<a href="job.php">
<img src="images/readmore.png"/></a>
</div>
<div class="downloada">
<h2>Download Area</h2>
<h3>. Application Form</h3>
<a href="PRINCELY Application Form.pdf">
<img src="images/pdf.png"/></a>
<a href="application.jpg">
<img src="images/jpg.png"/>
</a>
<a href="Princely E-Application.doc">
<img src="images/word.png"/>
</a>
<h3>. Student Guide</h3>
<a href="PIU study guide.pdf">
<img src="images/en.png"/>
</a>
<a href="PIU Study Guide French.pdf">
<img src="images/fr.png"/>
</a>
<a href="PIU Study Guide Arabic.pdf">
<img src="images/lb.png"/>
</a>
</div>
<a href="policies.php">
<h6>Policies</h6></a>
<a href="accreditation.php">
<h6>Accreditations</h6></a>
<a href="financialaid.php">
<h6>Financial Assistance</h6></a>
<a href="qanda.php">
<h6>Q &amp; A</h6></a>
</div></div>
</div>
<div class="footer">
<div class="center_container">
<div class="footerinner">
<a href="aboutus.php">
<h3>About Princely</h3></a>
<a href="process.php">
<h3>Admission Process</h3></a>
<a href="majors.php">
<h3>Majors Offered</h3></a>
<a href="feestruct.php">
<h3>Fee Structure</h3></a>
<a href="verificationserv.php">
<h3>Verification Service</h3></a>
<!--<a href="" >
<h3>Service</h3></a>-->
<a href="contactus.php">
<h3>Contact Us</h3></a>
</div>
<div class="footerinner">
<a href="policies.php">
<h3>Policies</h3></a>
<a href="accreditation.php">
<h3>Accreditations</h3></a>
<a href="financialaid.php">
<h3>Financial Assistance</h3></a>
<a href="qanda.php">
<h3>Q &amp; A</h3></a>
</div>
<div class="footerinner">
<a href="application.php">
<h3>Apply Now</h3></a>
<a href="login.php?type=1">
<h3>Student Login</h3></a>
<a href="login.php?type=0">
<h3>Faculty Login</h3></a>
<a href="download.php">
<h3>Download Area</h3></a>
</div>
<div class="footerinner2" style="border:none">
<h3>JOIN OUR SOCIAL MEDIA</h3>
<a href="https://www.facebook.com/Princely.university">
<img src="images/fsi.png"/></a>
<a href="https://www.linkedin.com/company/international-educational-consultants-iec-?trk=top_nav_home">
<img src="images/insi.png"/></a>
</div>
<!--
<div class="footerinner3">
<h3>PRINCELY INTERNATIONAL UNIVERSITY</h3>
<h3>ASIA HEADQUARTER</h3>
<h3 style="margin-top:20px">Beirut -Lebanon</h3>
<h3>+961 3 883114 </h3>
<h3>asia@princely.university</h3>
</div>-->
<div class="footerinner4">
<!--
<h3>PRINCELY INTERNATIONAL UNIVERSITY</h3>
<h3>ASIA HEADQUARTER</h3>
<h3 style="margin-top:20px">Beirut -Lebanon</h3>
<h3>+961 3 883114 </h3>
<h3>asia@princely.university</h3>
<h4>JOIN OUR SOCIAL MEDIA</h4>-->
<a href="https://www.facebook.com/Princely.university">
<img class="facemargin" src="images/fsi.png"/></a>
<a href="https://www.linkedin.com/company/international-educational-consultants-iec-?trk=top_nav_home">
<img src="images/insi.png"/></a>
</div>
<h2>Copyright Â© 2000 â Princely International University</h2>
<!--<h2>LEFICO @ 21 All Right Deserves</h2>
<a href="index.php">
<img src="images/palogo.png" class="logopro"  /></a>-->
</div>
</div></body>
</html>
