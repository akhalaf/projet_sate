<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
    //<![CDATA[
    (function(){
      
      window._cf_chl_opt={
        cvId: "1",
        cType: "non-interactive",
        cNounce: "21859",
        cRay: "611952dd8f26d1b7",
        cHash: "45cd925ed7fba74",
        cFPWv: "b",
        cRq: {
          ru: "aHR0cHM6Ly9vZmZlci5hbmFib2xpY2Nvb2tpbmcuY29tLw==",
          ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
          rm: "R0VU",
          d: "JotkmclU77iGUzFgw5kHpg1Tv1rLEkCDd2CeTujCqH/WNecrm9d755zMAPNC14Rg1m2Ooh2sTkxPUhKPrMCWOuWGtnoUgn1vC0fGdZulZuu4cRtd7rz/w3AK7fTSGklzCl0oQPvLuqyVCKEJXABqRT4Y6y/bzvl/2ApUgURRRXW0qsgTRgb3/OolXCmki8iIvWQzpQgoy8oRBhK0BVgfUXKlukooyAdSCXNq/7dPRJEsHTMNfDVNpb7xPHPauE58wKazfIgvXyYZ5myjaxVzXl0BNAJ5I3dpDaS28MsSqxSDFBAnZjWOmv9dQRST0C5nIqstm3Me5U5zhSc5qq/Whce46d/ymwCm1gDQvfy6JlR2r2RCuug1PEStYjX5NpD1g6Wc+PRPcMstt5gi4IhRMUvHgc/ebrHm2WEu1UHedfPgt4XzlVXObPRiiQCFLs1ISP13cS/DykIuFh6OecD258PHQKcxExozxJxFASHm/TOwF69aO6HFvwikWsjUXnJkVGBvIAgCnRMR2i/1x3SMpspC9mqpkGHxml0RK3hDzCyrQMi2ma7y8SeeOE2l4A1GGpkKgi5yGRBHGJvdXw2h6Y5duW7u3mwS/O8OUSlciL2py0GO+WWeVpZDoyF4/x27gy7Vzd/ul0dyxol7jl6B9wCt71ZZ5PV4jU4oEinT70727dFlLZDEvsn6d1X+g7YrAB8UCepGH0DG8ydU22eKuYY7whuu16HA+UH1F6xMH/AvjrXFLV+eE6g5JI1M6kid8ZFUwBoGxqKZIg8CxBVc+8qBo+DTjM/ki+Oer1zQmXM=",
          t: "MTYxMDY0NzkzMC40OTUwMDA=",
          m: "cegQvB6NnhBCz7UGPX3cIUFt2JrMsz6TzrfnoIyiulQ=",
          i1: "r7k7S64t4vmDH5/y6KNvlQ==",
          i2: "kZmwZ5CTA1OtqrgF/3vHBw==",
          uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
          hh: "sDgp8lqLz1tFaKYLan3PpCs/igMV9TxboSV72Tt80/0=",
        }
      }
      window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
      
      var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
        var a = document.getElementById('cf-content');a.style.display = 'block';
        var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
        var trkjs = isIE ? new Image() : document.createElement('img');
        trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=611952dd8f26d1b7");
        trkjs.id = "trk_jschal_js";
        trkjs.setAttribute("alt", "");
        document.body.appendChild(trkjs);
        
        var cpo = document.createElement('script');
        cpo.type = 'text/javascript';
        cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
        var done = false;
        cpo.onload = cpo.onreadystatechange = function() {
          if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
            done = true;
            cpo.onload = cpo.onreadystatechange = null;
            window._cf_chl_enter()
          }
        };
        document.getElementsByTagName('head')[0].appendChild(cpo);
      
      }, false);
    })();
    //]]>
  </script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> offer.anaboliccooking.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=fd5b8973756790f23e310c02282a3a80e6597d19-1610647930-0-AdDlW7uKnmrND428m8a-2rSNQ75YLwwRwVfj4rXlASbwiRzoCSX0NBM1GBsiFXd9sO33YgmLJ21QZXYvkMeH45lb0W6VFhhcqepptdYs_T5OLGbXK3WUsQmp769SJf8L-2aAAdhkWpRY2zbeRc0Gy2BTU7GMzAbCW_tfUAENnUdvs-j0xf8mTD2zU2JWVcx0o9OwyhbWXl2o-oosTK8D2UQNo_GFB7KlgMYVPUmlqNbQDEHgAiQ9R4k2yNYZd_VDvit2hp-vmFrzKW9pxdtl8RZGUo1jnwgu2Fo1rZLVV84DT2oOfTYUZvWR0-fBda2k1-Crt8sbnvYZLqjHF6JEL1hA4tQYCi_lssEFwlkxMujmU6Cs3n6zPCMwfIoqviSr1A" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="2adf20136695384b0330f944f5dcc00a387ccc8f-1610647930-0-AfcgO8uRQz5MuD6XsuxRcj6bdYHO+yrGnBLxrbRQYEaIIp/4BdyfHM6AMxqEwGUZBitc0A0tA+gJwcKGvoY4Ln2qS4A30xj3+sB6xFKO7/L+tbaNNn1An7xVdQUdjZS3AfZCO8Qdd7xYNDTWDe2at8bEZ/XabPmqBlN0uNmIv1lwx2PSbMwoMD/1Vtw7MFcdf/ZxlQMWXowKyqTxYo1XpXEkbWY3tH2QpOfeNPpKIVRawF1WOmzlfvVgaxQQQCtwlIfayIq7yT0QgELAtmNOz8mYPQw9bZzvtBmKrkgiSZo2OF3RaO7AcAb2JG5WsfczzlbH3Zuhi7pw8g8fqiJM58KTzFn8yrzhngMcxuqCTOPNJp3sh2w+HnTxA8wIdmCYeX6UkxZyHx6lZjryCNGjUfOUNRsA8oaF8/wWry2EYMvE3HfF9VfUoEyV8EQGDCz/PtLkLWU2oQUT34pHl2U5/H1AcvG972T04PGNHJlPYh3JXmF4Ie3of7e+IkJAIcybhpZ6jTrJQ2PryTIITaUeOBop1cqagrl5HTWbUX/X/jJRT1WFinZEOex6SjsVllW9jDAxtnGr70ALZ3MXKapMM/r4jplPo+VxyZTN2KhIfzqpDrNt169iA1zRwuzHxg03b2IG9ABqA31J754L36Hh5EmH6BbOAqa77qP7i5ohcHdShugGmqZVJ3DR+wnbiCceNyYenrdxdtkp6alAs0ZM6c1WMqB6Ah+r592kDRjEwZ3/LCh3R6gJu9VvTjbk69Co1s6sMcMzFFKmNFU3/aSCqLBWXaORMOclGgIZLLN8+kydo+A4+JCnZH0Iu4J23jHAPjKIAnwEoJpV3+ICDomF+3GiNielQuO4VCF9BVpTWG8/Hf3AqZmUy2RVRY3YgQ+BM4aVT89qUsfMJmDRlO1Uy5NKJgDR8GTd/Yu62PnJUip9ypfifAnsUct9sT91yYeCMbMoqSvJge+dweRYY3ZYN2fkVqwZAmkCiNtzFvc3BggAVjGYZfwwEisPCZifHjwVmwrW7WAoFstw3SmN33RkU+s2q+OeF5elUHe/m9coxnMMtUYbmOk59cn3ER+KR92SWJEQwyOekEnLAgH5bBqWo4pdo1+oc2q2AobC6Vy7l3+YiLdhCjXlT1tdKTdAzw2vAnmj5rido/gJfsFPEBOx/wl438oknHhJD7bvHvBeMjNPMsaMsBvmkHCIuXluBE6G8LuZ5DTBcAKNGvKP3hOLMUPKCuzrO+cGD6sdgv4cn1c5kgsljl40cNLtwfqeec3xBLSXV4x32bUWUn0zQV+9t7uRPFR6Cnhhal6wtDufNInZbscHrSTi2kSeDEARjiBhAQ=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="08941d161b2c2073e2d4fa81c2a0e567"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610647934.495-qFuFUqulkT"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=611952dd8f26d1b7')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>611952dd8f26d1b7</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
