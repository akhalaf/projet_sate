<html><head><link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css"/>
<script src="//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js" type="text/javascript"></script>
<!DOCTYPE html>

<!--[if IE 6]>
<html id="ie6" lang="az-Latn">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="az-Latn">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="az-Latn">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<!--<![endif]-->
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<meta content="IE=Edge" http-equiv="X-UA-Compatible"/>
<title>Asan İmza | New generation identification!</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="/favicon.png" rel="icon" type="image/png"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://asanimza.az/wp-content/themes/amid/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://asanimza.az/xmlrpc.php" rel="pingback"/>
<link href="https://fonts.googleapis.com/css?family=Exo+2:400,700&amp;subset=cyrillic" rel="stylesheet"/>
<script src="https://asanimza.az/wp-content/themes/amid/js/jquery.js" type="text/javascript"></script>
<script src="https://asanimza.az/wp-content/themes/amid/js/jquery.textgrad.js" type="text/javascript"></script>
<!--[if lt IE 9]>
		<script src="https://asanimza.az/wp-content/themes/amid/js/html5.js" type="text/javascript"></script>
		<![endif]-->
<script type="text/javascript">
			var search_translation = 'AXTARIŞ';
		</script>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/asanimza.az\/wp-includes\/js\/wp-emoji-release.min.js?ver=5c9776abc37763c808ea7815e4847720"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://asanimza.az/wp-content/plugins/logic_gdpr/cookie-notice/css/front.min.css?ver=5c9776abc37763c808ea7815e4847720" id="cookie-notice-front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://asanimza.az/wp-content/plugins/lightbox-gallery/colorbox/example1/colorbox.css?ver=5c9776abc37763c808ea7815e4847720" id="colorbox-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://asanimza.az/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://asanimza.az/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var cnArgs = {"ajaxurl":"https:\/\/asanimza.az\/wp-admin\/admin-ajax.php","hideEffect":"fade","onScroll":"no","onScrollOffset":"100","cookieName":"cookie_notice_accepted","cookieValue":"true","cookieTime":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"","cache":"","refuse":"no","revoke_cookies":"0","revoke_cookies_opt":"automatic","secure":"1"};
/* ]]> */
</script>
<script src="https://asanimza.az/wp-content/plugins/logic_gdpr/cookie-notice/js/front.min.js?ver=1.2.44" type="text/javascript"></script>
<link href="https://asanimza.az/wp-json/" rel="https://api.w.org/"/>
<link href="https://asanimza.az/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://asanimza.az/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<link href="//asanimza.az/wp-content/uploads/custom-css-js/7531.css?v=4944" id="7531-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://asanimza.az/wp-content/plugins/lightbox-gallery/lightbox-gallery.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		if (window.wfLogHumanRan) { return; }
		window.wfLogHumanRan = true;
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//asanimza.az/?wordfence_lh=1&hid=8767DF6794A8348ED1671E4B1E456E15');
</script><link href="https://asanimza.az/az/" hreflang="az" rel="alternate"/>
<link href="https://asanimza.az/en/" hreflang="en" rel="alternate"/>
<link href="https://asanimza.az/ru/" hreflang="ru" rel="alternate"/>
<link href="https://asanimza.az/" hreflang="x-default" rel="alternate"/>
<link href="https://asanimza.az/wp-content/uploads/2017/08/favicon-1.png" rel="icon" sizes="32x32"/>
<link href="https://asanimza.az/wp-content/uploads/2017/08/favicon-1.png" rel="icon" sizes="192x192"/>
<link href="https://asanimza.az/wp-content/uploads/2017/08/favicon-1.png" rel="apple-touch-icon-precomposed"/>
<meta content="https://asanimza.az/wp-content/uploads/2017/08/favicon-1.png" name="msapplication-TileImage"/>
</head><body>
<div id="container">
<div class="line-bg"> </div>
<div class="content single-header" id="header">
<div class="upper-menu">
<ul class="upper-menu-lang">
<li class="lang-item lang-item-38 lang-item-az lang-item-first current-lang"><a href="https://asanimza.az/az/" hreflang="az-Latn" lang="az-Latn">AZ</a></li>
<li class="lang-item lang-item-24 lang-item-en"><a href="https://asanimza.az/en/" hreflang="en-US" lang="en-US">EN</a></li>
<li class="lang-item lang-item-23 lang-item-ru"><a href="https://asanimza.az/ru/" hreflang="ru-RU" lang="ru-RU">RU</a></li>
</ul>
<ul class="upper-menu-search">
<li><a href="#"><span class="search-icon"> </span></a></li>
</ul>
</div>
<div class="lower-menu">
<a href="https://asanimza.az/az/"><img class="header-logo" src="https://asanimza.az/wp-content/themes/amid/images/logo.png"/></a>
<div class="mobile-clear"></div>
<ul class="menu" id="menu-header-in-azerbaijan"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-150" id="menu-item-150"><a href="http://www.asanimza.az/az/" title="ANA SƏHİFƏ">ANA SƏHİFƏ</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1198" id="menu-item-1198"><a href="https://asanimza.az/category/introduction-az/" title="GİRİŞ">GİRİŞ</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-880" id="menu-item-880"><a href="https://asanimza.az/category/asan-imza-az/" title="ASAN İMZA">ASAN İMZA</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-881" id="menu-item-881"><a href="https://asanimza.az/category/asan-doc-az/" title="ASAN DOC">ASAN DOC</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-882" id="menu-item-882"><a href="https://asanimza.az/category/news-n-az/news-az/" title="XƏBƏRLƏR">XƏBƏRLƏR</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-2678" id="menu-item-2678"><a href="https://asanimza.az/category/faq-az/">FAQ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-493" id="menu-item-493"><a href="https://asanimza.az/contact-az/" title="ƏLAQƏ">ƏLAQƏ</a></li>
</ul> </div>
<div class="clear"></div>
<div class="slideshow_container slideshow_container_slideshow-jquery-image-gallery-custom-styles_1" data-slideshow-id="478" data-style-name="slideshow-jquery-image-gallery-custom-styles_1" data-style-version="1472113177" style="height: 350px; max-width: 960px;">
<div class="slideshow_loading_icon"></div>
<div class="slideshow_content" style="display: none;">
<div class="slideshow_view">
<div class="slideshow_slide slideshow_slide_image">
<img alt="banner_1_az_06" height="350" src="https://asanimza.az/wp-content/uploads/2013/06/banner_1_az_06.png" width="960"/>
<div class="slideshow_description_box slideshow_transparent">
<div class="slideshow_title">banner_1_az_06</div> </div>
</div>
<div style="clear: both;"></div></div><div class="slideshow_view">
<div class="slideshow_slide slideshow_slide_image">
<img alt="banner_2_az" height="350" src="https://asanimza.az/wp-content/uploads/2013/04/banner_2_az.png" width="960"/>
<div class="slideshow_description_box slideshow_transparent">
<div class="slideshow_title">banner_2_az</div> </div>
</div>
<div style="clear: both;"></div></div><div class="slideshow_view">
<div class="slideshow_slide slideshow_slide_image">
<img alt="banner_3_az" height="350" src="https://asanimza.az/wp-content/uploads/2013/04/banner_3_az.png" width="960"/>
<div class="slideshow_description_box slideshow_transparent">
<div class="slideshow_title">banner_3_az</div> </div>
</div>
<div style="clear: both;"></div></div>
</div>
<div class="slideshow_controlPanel slideshow_transparent" style="display: none;"><ul><li class="slideshow_togglePlay" data-pause-text="Pause" data-play-text="Play"></li></ul></div>
<div class="slideshow_button slideshow_previous slideshow_transparent" data-previous-text="Previous" role="button" style="display: none;"></div>
<div class="slideshow_button slideshow_next slideshow_transparent" data-next-text="Next" role="button" style="display: none;"></div>
<div class="slideshow_pagination" data-go-to-text="Go to slide" style="display: none;"><div class="slideshow_pagination_center"></div></div>
<!-- WordPress Slideshow Version 2.3.1 -->
</div>
</div>
<div id="wrapper">
<div id="portals">
<ul class="portals-holder content">
<li>
<h1>asandoc nədİr?					</h1>
<p>
</p><p>Pasportların və sertifikatlardan ibarət olan rəqəmsal şəxsiyyət vəsiqələri kimi ənənəvi identifikasiya sənədlərinin müvafiq CA-da (Sertifikat Mərkəzində) və şəxsi açarların smart kartda (Rəqəmsal İD kart) və ya mobil telefonda SIM kartda (Asan İmza və ya Mobil İmza) saxlanması arasındakı əsas fərqlərdən biri, sonuncunun imzaları vermək funksiyasına malik olmasıdır. </p>
<a class="read-more" href="https://asanimza.az/asan-doc-az/2013/what-is-asandoc/" title="AsanDoc nədir?">Əlavə məlumat <span class="arrow"></span></a>
<a class="download" href="http://www.asanimza.az/download/" target="_blank">ENDİRMƏK</a>
</li>
<li>
<h1>asan İmza nədİr?					</h1>
<p>
</p><p>Asan İmza (Mobil imza) e-xidmətlərə daxil olan və rəqəmsal imzalar edən zaman kimliyinizi təsdiqləmək üçün sizin mobil identifikasiyanızdır. Asan İmza (Mobil imza) bütün mövcud e-xidmətlərdən istifadəni mümkün edir.</p>
<a class="read-more" href="https://asanimza.az/asan-imza-az/2013/what-is-the-asan-imza/" title="Asan İmza nədir?">Əlavə məlumat <span class="arrow"></span></a>
<a class="download" href="http://www.asanimza.az/download/" target="_blank">ENDİRMƏK</a>
</li>
</ul>
</div>
<div class="content">
<hr class="news-faq-hr"/>
<hr class="news-faq-hr"/>
</div>
<div class="mobile-clear"></div>
<div class="content" id="news-faq">
<ul class="news">
<li>
<span class="date">24.12.2020</span>
<h1><a href="https://asanimza.az/news-n-az/news-az/2020/happy-birthday-mr-president_az/" title="Ad gününüz mübarək, cənab Prezident!">Ad gününüz mübarək, cənab Prezident!</a></h1>
<p>
</p><div class="social-plugins">
<div class="social-plugin-share">
<div class="share-facebook">
<a href="javascript:;" onclick="window.open('http://www.facebook.com/sharer.php?u=https://asanimza.az/news-n-az/news-az/2020/happy-birthday-mr-president_az/&amp;t=Ad gününüz mübarək, cənab Prezident!','share','height=300,width=500');" target="_blank">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/fb.png"/>
</a>
</div>
<div class="share-twitter">
<a href="javascript:;" onclick="window.open('https://twitter.com/share?url=https://asanimza.az/news-n-az/news-az/2020/happy-birthday-mr-president_az/&amp;text=Ad gününüz mübarək, cənab Prezident!','share','height=300,width=500');" target="_blank">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/twit.png"/>
</a>
</div>
<div class="share-g">
<a href="javascript:;" onclick="window.open('https://plus.google.com/share?url=https://asanimza.az/news-n-az/news-az/2020/happy-birthday-mr-president_az/&amp;hl=Ad gününüz mübarək, cənab Prezident!','share','height=300,width=500');" target="_blank">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/g.png"/>
</a>
</div>
<div class="dots">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/mummud_mouseover.png"/>
</div>
</div>
<div class="social-plugin-switch">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/mummud_default.png"/>
</div>
<div class="clear"></div>
</div>
<a class="read-more" href="https://asanimza.az/news-n-az/news-az/2020/happy-birthday-mr-president_az/" title="Ad gününüz mübarək, cənab Prezident!">Əlavə məlumat <span class="arrow"></span></a>
</li>
<li>
<span class="date">10.12.2020</span>
<h1><a href="https://asanimza.az/news-n-az/news-az/2020/victory-parade-dedicated-to-the-victory-of-the-azerbaijan-army-in-the-second-patriotic-war_az/" title="Azərbaycan ordusunun İkinci Vətən müharibəsində Qələbəyə həsr olunmuş “Zəfər Paradı” keçirildi">Azərbaycan ordusunun İkinci Vətən müharibəsində Qələbəyə həsr olunmuş “Zəfər Paradı” keçirildi</a></h1>
<p>
</p><div class="social-plugins">
<div class="social-plugin-share">
<div class="share-facebook">
<a href="javascript:;" onclick="window.open('http://www.facebook.com/sharer.php?u=https://asanimza.az/news-n-az/news-az/2020/victory-parade-dedicated-to-the-victory-of-the-azerbaijan-army-in-the-second-patriotic-war_az/&amp;t=Azərbaycan ordusunun İkinci Vətən müharibəsində Qələbəyə həsr olunmuş “Zəfər Paradı” keçirildi','share','height=300,width=500');" target="_blank">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/fb.png"/>
</a>
</div>
<div class="share-twitter">
<a href="javascript:;" onclick="window.open('https://twitter.com/share?url=https://asanimza.az/news-n-az/news-az/2020/victory-parade-dedicated-to-the-victory-of-the-azerbaijan-army-in-the-second-patriotic-war_az/&amp;text=Azərbaycan ordusunun İkinci Vətən müharibəsində Qələbəyə həsr olunmuş “Zəfər Paradı” keçirildi','share','height=300,width=500');" target="_blank">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/twit.png"/>
</a>
</div>
<div class="share-g">
<a href="javascript:;" onclick="window.open('https://plus.google.com/share?url=https://asanimza.az/news-n-az/news-az/2020/victory-parade-dedicated-to-the-victory-of-the-azerbaijan-army-in-the-second-patriotic-war_az/&amp;hl=Azərbaycan ordusunun İkinci Vətən müharibəsində Qələbəyə həsr olunmuş “Zəfər Paradı” keçirildi','share','height=300,width=500');" target="_blank">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/g.png"/>
</a>
</div>
<div class="dots">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/mummud_mouseover.png"/>
</div>
</div>
<div class="social-plugin-switch">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/mummud_default.png"/>
</div>
<div class="clear"></div>
</div><p>Zəfər sənindir, Ana Vətən!</p>
<a class="read-more" href="https://asanimza.az/news-n-az/news-az/2020/victory-parade-dedicated-to-the-victory-of-the-azerbaijan-army-in-the-second-patriotic-war_az/" title="Azərbaycan ordusunun İkinci Vətən müharibəsində Qələbəyə həsr olunmuş “Zəfər Paradı” keçirildi">Əlavə məlumat <span class="arrow"></span></a>
</li>
<li>
<span class="date">01.12.2020</span>
<h1><a href="https://asanimza.az/news-n-az/news-az/2020/lachin-is-ours_az/" title="Laçın bizimdir!">Laçın bizimdir!</a></h1>
<p>
</p><div class="social-plugins">
<div class="social-plugin-share">
<div class="share-facebook">
<a href="javascript:;" onclick="window.open('http://www.facebook.com/sharer.php?u=https://asanimza.az/news-n-az/news-az/2020/lachin-is-ours_az/&amp;t=Laçın bizimdir!','share','height=300,width=500');" target="_blank">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/fb.png"/>
</a>
</div>
<div class="share-twitter">
<a href="javascript:;" onclick="window.open('https://twitter.com/share?url=https://asanimza.az/news-n-az/news-az/2020/lachin-is-ours_az/&amp;text=Laçın bizimdir!','share','height=300,width=500');" target="_blank">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/twit.png"/>
</a>
</div>
<div class="share-g">
<a href="javascript:;" onclick="window.open('https://plus.google.com/share?url=https://asanimza.az/news-n-az/news-az/2020/lachin-is-ours_az/&amp;hl=Laçın bizimdir!','share','height=300,width=500');" target="_blank">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/g.png"/>
</a>
</div>
<div class="dots">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/mummud_mouseover.png"/>
</div>
</div>
<div class="social-plugin-switch">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/mummud_default.png"/>
</div>
<div class="clear"></div>
</div><p>Laçın bizimdir! Qarabağ Azərbaycandır!</p>
<a class="read-more" href="https://asanimza.az/news-n-az/news-az/2020/lachin-is-ours_az/" title="Laçın bizimdir!">Əlavə məlumat <span class="arrow"></span></a>
</li>
<li>
<span class="date">26.11.2020</span>
<h1><a href="https://asanimza.az/news-n-az/news-az/2020/asan-imza-now-can-be-obtained-at-kapital-bank-branches_az/" title="Artıq “Asan İmza”nı Kapital Bank filiallarında əldə etmək mümkün oldu">Artıq “Asan İmza”nı Kapital Bank filiallarında əldə etmək mümkün oldu</a></h1>
<p>
</p><div class="social-plugins">
<div class="social-plugin-share">
<div class="share-facebook">
<a href="javascript:;" onclick="window.open('http://www.facebook.com/sharer.php?u=https://asanimza.az/news-n-az/news-az/2020/asan-imza-now-can-be-obtained-at-kapital-bank-branches_az/&amp;t=Artıq “Asan İmza”nı Kapital Bank filiallarında əldə etmək mümkün oldu','share','height=300,width=500');" target="_blank">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/fb.png"/>
</a>
</div>
<div class="share-twitter">
<a href="javascript:;" onclick="window.open('https://twitter.com/share?url=https://asanimza.az/news-n-az/news-az/2020/asan-imza-now-can-be-obtained-at-kapital-bank-branches_az/&amp;text=Artıq “Asan İmza”nı Kapital Bank filiallarında əldə etmək mümkün oldu','share','height=300,width=500');" target="_blank">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/twit.png"/>
</a>
</div>
<div class="share-g">
<a href="javascript:;" onclick="window.open('https://plus.google.com/share?url=https://asanimza.az/news-n-az/news-az/2020/asan-imza-now-can-be-obtained-at-kapital-bank-branches_az/&amp;hl=Artıq “Asan İmza”nı Kapital Bank filiallarında əldə etmək mümkün oldu','share','height=300,width=500');" target="_blank">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/g.png"/>
</a>
</div>
<div class="dots">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/mummud_mouseover.png"/>
</div>
</div>
<div class="social-plugin-switch">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/mummud_default.png"/>
</div>
<div class="clear"></div>
</div><p style="text-align: justify;">Kapital Bank mövcud korporativ müştərilərin, eləcə də bank hesabı açmaq üçün filiallara müraciət edən sahibkarların və hüquqi şəxslərin xidmətdən rahat şəkildə faydalanmasını təmin etmək üçün daha bir yeniliyi təqdim edir.</p>
<a class="read-more" href="https://asanimza.az/news-n-az/news-az/2020/asan-imza-now-can-be-obtained-at-kapital-bank-branches_az/" title="Artıq “Asan İmza”nı Kapital Bank filiallarında əldə etmək mümkün oldu">Əlavə məlumat <span class="arrow"></span></a>
</li>
<li>
<span class="date">23.11.2020</span>
<h1><a href="https://asanimza.az/news-n-az/news-az/2020/az%c9%99rbaycanin-r%c9%99q%c9%99msal-ticar%c9%99t-qovsagi-v%c9%99-asan-imza-bmt-nin-speca-iqtisadi-forumu-2020-d%c9%99-t%c9%99qdim-olundu/" title="Azərbaycanın Rəqəmsal Ticarət Qovşağı və “Asan İmza” BMT-nin SPECA İQTİSADİ FORUMU 2020-də təqdim olundu">Azərbaycanın Rəqəmsal Ticarət Qovşağı və “Asan İmza” BMT-nin SPECA İQTİSADİ FORUMU 2020-də təqdim olundu</a></h1>
<p>
</p><div class="social-plugins">
<div class="social-plugin-share">
<div class="share-facebook">
<a href="javascript:;" onclick="window.open('http://www.facebook.com/sharer.php?u=https://asanimza.az/news-n-az/news-az/2020/az%c9%99rbaycanin-r%c9%99q%c9%99msal-ticar%c9%99t-qovsagi-v%c9%99-asan-imza-bmt-nin-speca-iqtisadi-forumu-2020-d%c9%99-t%c9%99qdim-olundu/&amp;t=Azərbaycanın Rəqəmsal Ticarət Qovşağı və “Asan İmza” BMT-nin SPECA İQTİSADİ FORUMU 2020-də təqdim olundu','share','height=300,width=500');" target="_blank">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/fb.png"/>
</a>
</div>
<div class="share-twitter">
<a href="javascript:;" onclick="window.open('https://twitter.com/share?url=https://asanimza.az/news-n-az/news-az/2020/az%c9%99rbaycanin-r%c9%99q%c9%99msal-ticar%c9%99t-qovsagi-v%c9%99-asan-imza-bmt-nin-speca-iqtisadi-forumu-2020-d%c9%99-t%c9%99qdim-olundu/&amp;text=Azərbaycanın Rəqəmsal Ticarət Qovşağı və “Asan İmza” BMT-nin SPECA İQTİSADİ FORUMU 2020-də təqdim olundu','share','height=300,width=500');" target="_blank">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/twit.png"/>
</a>
</div>
<div class="share-g">
<a href="javascript:;" onclick="window.open('https://plus.google.com/share?url=https://asanimza.az/news-n-az/news-az/2020/az%c9%99rbaycanin-r%c9%99q%c9%99msal-ticar%c9%99t-qovsagi-v%c9%99-asan-imza-bmt-nin-speca-iqtisadi-forumu-2020-d%c9%99-t%c9%99qdim-olundu/&amp;hl=Azərbaycanın Rəqəmsal Ticarət Qovşağı və “Asan İmza” BMT-nin SPECA İQTİSADİ FORUMU 2020-də təqdim olundu','share','height=300,width=500');" target="_blank">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/g.png"/>
</a>
</div>
<div class="dots">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/mummud_mouseover.png"/>
</div>
</div>
<div class="social-plugin-switch">
<img src="https://asanimza.az/wp-content/themes/amid/images/share/mummud_default.png"/>
</div>
<div class="clear"></div>
</div><p style="text-align: justify;">18-19 noyabr tarixlərində BMT-nin Asiya və Sakit Okean üçün İqtisadi və Sosial Komissiyası (UNESCAP), BMT-nin Avropa İqtisadi Komissiyası (UNECE) və BMT-nin Mərkəzi Asiya Ölkələrinin İqtisadiyyatı üçün Xüsusi Proqramı (SPECA) nümayəndə heyətlərinin rəhbərləri ilə birlikdə virtual rejimdə “COVID-19 pandemiyasından sonra sosial-iqtisadi dirçəlişin dəstəklənməsində regional əməkdaşlıq” mövzusunda SPECA İQTİSADİ FORUMU – 2020 keçirdi.</p>
<a class="read-more" href="https://asanimza.az/news-n-az/news-az/2020/az%c9%99rbaycanin-r%c9%99q%c9%99msal-ticar%c9%99t-qovsagi-v%c9%99-asan-imza-bmt-nin-speca-iqtisadi-forumu-2020-d%c9%99-t%c9%99qdim-olundu/" title="Azərbaycanın Rəqəmsal Ticarət Qovşağı və “Asan İmza” BMT-nin SPECA İQTİSADİ FORUMU 2020-də təqdim olundu">Əlavə məlumat <span class="arrow"></span></a>
</li>
<li class="read-all"><a href="/category/category/news-az/">Bütün xəbərlər <span class="arrow"></span></a></li>
</ul>
<!--MUUD -->
<!--<ul class="faq">
				<li class="faq-title">Activities</li>
													<li>Sorry, no activities added yet!</li>
							</ul> -->
<!--FAQ -->
<ul class="faq">
<li class="newsletter"><!-- Widget by WYSIWYG Widgets v2.3.8 - https://wordpress.org/plugins/wysiwyg-widgets/ --><style type="text/css">
    #mc_embed_signup{margin-bottom:1cm; font:14px Helvetica,Arial,sans-serif; }
    #mce-EMAIL-label{color: #4D4D4D; font-size:13px; font-family:Arial,sans-serif; }
</style>
<div id="mc_embed_signup">
<form action="//bestsolutions.us11.list-manage.com/subscribe/post?u=f8c334affc6b302a09dcbd5eb&amp;id=e750b11d3d" class="validate" id="mc-embedded-subscribe-form" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
<div id="mc_embed_signup_scroll">
<h2>Korporativ bülletenimizə abunə olun</h2>
<div class="mc-field-group">
<label for="mce-EMAIL" id="mce-EMAIL-label">E-mail ünvanınızı daxil edin: </label><br/>
<input class="email" id="mce-EMAIL" name="EMAIL" type="email"/>
</div>
<div class="clear" id="mce-responses">
<div class="response" id="mce-error-response" style="display:none"></div>
<div class="response" id="mce-success-response" style="display:none"></div>
</div>
<div style="position: absolute; left: -5000px;"><input name="b_f8c334affc6b302a09dcbd5eb_e750b11d3d" tabindex="-1" type="text" value=""/></div>
<div class="clear"><input class="button" id="mc-embedded-subscribe" name="subscribe" type="submit" value="Abunə olmaq"/></div>
</div>
</form>
</div>
<p><script src="//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js" type="text/javascript"></script><script type="text/javascript">(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script></p>
<!-- / WYSIWYG Widgets --></li>
<li class="faq-title">FAQ</li>
<li><a href="https://asanimza.az/faq-az/2016/where-can-i-find-the-list-of-documents-required-for-obtaining-asan-imza_az/"><span class="title">Asan İmzanın əldə edilməsi üçün lazım olan sənədlərin siyahısı ilə necə tanış ola bilərəm?</span><span class="arrow"></span></a></li>
<li><a href="https://asanimza.az/faq-az/2016/i-want-to-use-the-service-asan-imza-but-forgot-my-user-id-how-can-i-get-this-id_az/"><span class="title">Asan İmza xidmətindən istifadə etmək istəyirəm, lakin İstifadəçi ID-mi unutmuşam.  Bu ID-ni necə əldə etmək mümkündür?</span><span class="arrow"></span></a></li>
<li><a href="https://asanimza.az/faq-az/2015/i-have-purchased-an-asan-imza-sim-card-how-can-i-change-my-asan-imza-user-id_az/"><span class="title">Asan İmza kartı əldə etmişəm. İstifadəçi ID-mi necə dəyişə bilərəm?</span><span class="arrow"></span></a></li>
<li><a href="https://asanimza.az/faq-az/2015/a-message-error-0035-appears-on-the-mobile-phones-screen-when-using-asan-imza_az/"><span class="title">Asan İmzadan istifadə zamanı mobil telefonumda “Səhv 0035” xətası çıxır</span><span class="arrow"></span></a></li>
<li><a href="https://asanimza.az/faq-az/2015/when-registering-on-the-websites-integrated-with-asan-imza-i-am-asked-to-enter-user-id-how-can-i-get-this-id_az/"><span class="title">Asan İmzanın istifadə edildiyi saytlardan qeydiyyatdan keçdiyim zaman “İstifadəçi ID”si tələb olunur. Bu ID-ni necə əldə etmək mümkündür?</span><span class="arrow"></span></a></li>
<li><a href="https://asanimza.az/faq-az/2015/i-have-become-an-asan-imza-user-but-i-cannot-perform-registration-of-notifications_az/"><span class="title">Əmək müqaviləsi bildirişlərini qeydiyyata ala bilmirəm?</span><span class="arrow"></span></a></li>
<li><a href="https://asanimza.az/faq-az/2015/what-should-i-do-if-puk-code-is-blocked_az/"><span class="title">PUK kod bloka düşdükdə nə etmək lazımdır?</span><span class="arrow"></span></a></li>
<li><a href="https://asanimza.az/faq-az/2015/faq-asandoc-installation-manual-az/"><span class="title">AsanDoc: instalyasiya təlimatı</span><span class="arrow"></span></a></li>
<li><a href="https://asanimza.az/faq-az/2014/labor_contract_with_asanimza_az/"><span class="title">“Asan İmza” – mobil elektron imza vasitəsilə Əmək müqaviləsi bildirişlərinin qeydiyyata alınması qaydası</span><span class="arrow"></span></a></li>
<li><a href="https://asanimza.az/faq-az/2013/asan-imzanin-mobil-id-istifad%c9%99sind%c9%99-7-%c9%99sas-t%c9%99hluk%c9%99sizlik-qaydasi/"><span class="title">“Asan İmza”nın (Mobil ID) istifadəsində 7 əsas təhlükəsizlik qaydası</span><span class="arrow"></span></a></li>
<li><a href="https://asanimza.az/faq-az/2013/list-of-unsupported-mobile-devices-az/"><span class="title">Dəstəkləməyən mobil qurğuların siyahısı</span><span class="arrow"></span></a></li>
<li><a href="https://asanimza.az/faq-az/2013/three-certificates-az/"><span class="title">Mənim üç şirkətdə imza hüququm vardır. Bunun üçün mən üç müxtəlif Asan İmza SIM kartı əldə etməliyəm?</span><span class="arrow"></span></a></li>
<li><a href="https://asanimza.az/faq-az/2013/how-does-the-asandoc-service-work-3/"><span class="title">AsanDoc xidməti necə işləyir?</span><span class="arrow"></span></a></li>
<li><a href="https://asanimza.az/faq-az/2013/signing-service-2/"><span class="title">İmzalama xidməti</span><span class="arrow"></span></a></li>
<li><a href="https://asanimza.az/faq-az/2013/getting-signing-certificates-service-2/"><span class="title">İmzalama sertifikatlarının əldə edilməsi xidməti</span><span class="arrow"></span></a></li>
<li class="read-more"><a href="/category/faq-az/">Hamısını oxu <span class="arrow"></span></a></li>
</ul>
</div>
<div class="clear"></div>
</div>
<div id="footer">
<div class="content">
<div class="grid">
<div class="spacer"></div>
<h1>KÖMƏK LAZIMDIR?</h1>
<!-- Widget by WYSIWYG Widgets v2.3.8 - https://wordpress.org/plugins/wysiwyg-widgets/ --><p>Bizimlə <span style="color: #ff00ff;">(012) 599 -1230</span> nömrəsinə zəng etməklə, yaxud <script language="JavaScript">
var username = "info";
var hostname = "asanimza.az";
var linktext = username + "@" + hostname ;
document.write("<a href='" + "mail" + "to:" + linktext + "'>" + linktext + "</a>");
</script> ünvanına<br/>
məktub yazmaqla əlaqə saxlaya bilərsiniz.</p>
<p> </p>
<p><a href="http://www.facebook.com/asan.imza"><img alt="facebook_32" class="alignleft size-full wp-image-2362" height="32" src="https://www.asanimza.az/wp-content/uploads/2013/03/facebook_32.png" width="32"/></a><a href="https://www.youtube.com/channel/UCfNpqVYygYFVLNAATB5DY0w"><img alt="youtube" class="alignleft size-full wp-image-2362" height="32" src="https://www.asanimza.az/wp-content/uploads/2014/09/youtube.png" width="32"/></a></p>
<!-- / WYSIWYG Widgets --> </div>
<div class="grid">
<h1>SAYTIN XƏRİTƏSİ</h1><div class="menu-footer-in-azerbaijan-container"><ul class="menu" id="menu-footer-in-azerbaijan"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1110" id="menu-item-1110"><a href="/">ANA SƏHİFƏ</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1197" id="menu-item-1197"><a href="https://asanimza.az/category/introduction-az/">GİRİŞ</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1100" id="menu-item-1100"><a href="https://asanimza.az/category/asan-imza-az/">ASAN İMZA</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1103" id="menu-item-1103"><a href="https://asanimza.az/category/asan-doc-az/" title="ASAN DOC">ASAN DOC</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1109" id="menu-item-1109"><a href="https://asanimza.az/category/news-n-az/news-az/">XƏBƏRLƏR</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1106" id="menu-item-1106"><a href="https://asanimza.az/category/faq-az/">TEZ-TEZ VERİLƏN SUALLAR</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1095" id="menu-item-1095"><a href="https://asanimza.az/contact-az/">ƏLAQƏ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1092" id="menu-item-1092"><a href="https://asanimza.az/sitemap-az/">SAYTIN XƏRİTƏSİ</a></li>
</ul></div> </div>
<div class="copyright"><!-- Widget by WYSIWYG Widgets v2.3.8 - https://wordpress.org/plugins/wysiwyg-widgets/ --><p>Müəllif hüququ 2013 <a href="http://www.bestsolutions.az" target="_blank" title="B.EST Solutions">B.EST Solutions</a>. Bütün hüquqlar qorunur. | Hazırladı <a href="http://www.finestmedia.ee" target="_blank" title="Finestmedia Ltd.">Finestmedia Ltd</a>.</p>
<!-- / WYSIWYG Widgets --></div>
<div class="netty"><a href="http://www.netty.az/winners.shtml" target="_blank"><img alt="Netty2013 Winner" border="0" height="60" src="https://www.netty.az/img/netty2013win.gif" width="120"/></a></div>
<div class="mobile-gov"><a href="http://www.m4life.org/conferences/mgovsummit2016/wp-content/uploads/2018/06/conference-proceedings-template-22.12.pdf" target="_blank"><img alt="Netty2013 Winner" border="0" height="90" src="https://asanimza.az/wp-content/themes/amid/images/mobile-gov3.jpg" width="120"/></a></div>
</div>
</div>
<link href="https://asanimza.az/wp-content/themes/amid/css/responsive.css" media="all" rel="stylesheet" type="text/css"/>
<script type="text/javascript">var language = 'az';</script>
<script src="https://asanimza.az/wp-content/themes/amid/js/main.js" type="text/javascript"></script>
<script data-id="yayFon" data-widget-id="widget-d4f9011b-fa5d-4e78-9497-286fd18136e8" src="https://yayfon.com/widgetCaller/yayCallButton.min.js" type="text/javascript"></script>
</div>
<script type="text/javascript">
// <![CDATA[
// ]]>
</script>
<link href="https://asanimza.az/wp-content/plugins/slideshow-jquery-image-gallery/style/SlideshowPlugin/functional.css?ver=2.3.1" id="slideshow-jquery-image-gallery-stylesheet_functional-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://asanimza.az/wp-admin/admin-ajax.php?action=slideshow_jquery_image_gallery_load_stylesheet&amp;style=slideshow-jquery-image-gallery-custom-styles_1&amp;ver=1472113177" id="slideshow-jquery-image-gallery-ajax-stylesheet_slideshow-jquery-image-gallery-custom-styles_1-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://asanimza.az/wp-content/plugins/lightbox-gallery/js/jquery.colorbox.js?ver=5c9776abc37763c808ea7815e4847720" type="text/javascript"></script>
<script src="https://asanimza.az/wp-content/plugins/lightbox-gallery/js/jquery.tooltip.js?ver=5c9776abc37763c808ea7815e4847720" type="text/javascript"></script>
<script src="https://asanimza.az/wp-content/plugins/lightbox-gallery/lightbox-gallery.js?ver=5c9776abc37763c808ea7815e4847720" type="text/javascript"></script>
<script src="https://asanimza.az/wp-includes/js/wp-embed.min.js?ver=5c9776abc37763c808ea7815e4847720" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var SlideshowPluginSettings_478 = {"animation":"slide","slideSpeed":"1.5","descriptionSpeed":"0.4","intervalSpeed":"7","slidesPerView":"1","maxWidth":"960","aspectRatio":"3:1","height":"350","imageBehaviour":"stretch","showDescription":"false","hideDescription":"true","preserveSlideshowDimensions":"false","enableResponsiveness":"true","play":"true","loop":"true","pauseOnHover":"true","controllable":"false","hideNavigationButtons":"false","showPagination":"true","hidePagination":"false","controlPanel":"false","hideControlPanel":"true","waitUntilLoaded":"true","showLoadingIcon":"true","random":"false","avoidFilter":"true"};
var slideshow_jquery_image_gallery_script_adminURL = "https:\/\/asanimza.az\/wp-admin\/";
/* ]]> */
</script>
<script src="https://asanimza.az/wp-content/plugins/slideshow-jquery-image-gallery/js/min/all.frontend.min.js?ver=2.3.1" type="text/javascript"></script>
<!-- start Simple Custom CSS and JS -->
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", () => {
        const cards = Array.from(document.getElementsByClassName('question-card'));
        cards.forEach(card => {
            const arrow = card.querySelector(".arrow-part span");
            const expandable = card.querySelector(".expandable-part");
            const expandableText = expandable.querySelector(".expandable-text");
            const middle = card.querySelector(".middle-part");
            expandable.style.height = "0px";
            middle.onclick = () => {
                const curHeight = Number(expandable.style.height.replace("px", ""));
                const expanded = Boolean(curHeight);
                expandable.style.height = expanded?"0px":`${expandableText.scrollHeight}px`;
                arrow.style.transform = !expanded?"rotate(90deg)":"";
            }

        })
    });</script>
<!-- end Simple Custom CSS and JS -->
<div class="cn-bottom wp-default" id="cookie-notice" role="banner" style="color: #fff; background-color: #0d8eca;"><div class="cookie-notice-container"><div id="cn-notice-text">We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it.</div><div class="cookie-notice-buttons"><a class="cn-set-cookie cn-button wp-default button" data-cookie-set="accept" href="#" id="cn-accept-cookie">Ok</a><a class="cn-more-info cn-button wp-default button" href="http://asanimza.az/customer-data-privacy-policy/" id="cn-more-info" target="_blank">Privacy policy</a>
</div>
</div>
</div></body></html>
