<!DOCTYPE HTML>
<html data-config='{"twitter":0,"plusone":0,"facebook":0,"style":"ATC"}' dir="ltr" lang="en-gb">
<head>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta charset="utf-8"/>
<base href="https://www.atcouncil.org/"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>Home</title>
<link href="/?format=feed&amp;type=rss" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="/?format=feed&amp;type=atom" rel="alternate" title="Atom 1.0" type="application/atom+xml"/>
<link href="https://www.atcouncil.org/component/search/?Itemid=461&amp;format=opensearch" rel="search" title="Search www.atcouncil.org" type="application/opensearchdescription+xml"/>
<link href="/templates/yoo_avenue/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/plugins/system/jce/css/content.css?f9179b2834665d973bc00941b33848d7" rel="stylesheet"/>
<script src="/media/jui/js/jquery.min.js?f9179b2834665d973bc00941b33848d7"></script>
<script src="/media/jui/js/jquery-noconflict.js?f9179b2834665d973bc00941b33848d7"></script>
<script src="/media/jui/js/jquery-migrate.min.js?f9179b2834665d973bc00941b33848d7"></script>
<script src="/media/jui/js/bootstrap.min.js?f9179b2834665d973bc00941b33848d7"></script>
<script src="/media/widgetkit/uikit2-aa77b77b.js"></script>
<script src="/media/widgetkit/wk-scripts-f5df59bf.js"></script>
<link href="/templates/yoo_avenue/apple_touch_icon.png" rel="apple-touch-icon-precomposed"/>
<link href="/templates/yoo_avenue/styles/ATC/css/bootstrap.css" rel="stylesheet"/>
<link href="/templates/yoo_avenue/styles/ATC/css/theme.css" rel="stylesheet"/>
<link href="/templates/yoo_avenue/css/custom.css" rel="stylesheet"/>
<script src="/templates/yoo_avenue/warp/vendor/uikit/js/uikit.js"></script>
<script src="/templates/yoo_avenue/warp/vendor/uikit/js/components/autocomplete.js"></script>
<script src="/templates/yoo_avenue/warp/vendor/uikit/js/components/search.js"></script>
<script src="/templates/yoo_avenue/warp/vendor/uikit/js/components/tooltip.js"></script>
<script src="/templates/yoo_avenue/warp/js/social.js"></script>
<script src="/templates/yoo_avenue/js/theme.js"></script>
</head>
<body class="tm-sidebar-a-right tm-sidebar-b-right tm-sidebars-2 tm-isblog">
<div class="tm-page-bg">
<div class="uk-container uk-container-center">
<div class="tm-container">
<div class="tm-headerbar uk-clearfix uk-hidden-small">
<a class="tm-logo" href="https://www.atcouncil.org">
<div style="height: 245px;"><img alt="" height="245" src="/images/headers/atc-header-cv.jpg" width="959"/></div></a>
</div>
<div class="tm-top-block tm-grid-block">
<nav class="tm-navbar uk-navbar">
<ul class="uk-navbar-nav uk-hidden-small"><li class="uk-active"><a href="/">Home</a></li><li aria-expanded="false" aria-haspopup="true" class="uk-parent" data-uk-dropdown="{'preventflip':'y'}"><a href="/about-atc">About ATC</a><div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-1"><div class="uk-grid uk-dropdown-grid"><div class="uk-width-1-1"><ul class="uk-nav uk-nav-navbar"><li><a href="/about-atc/employee-bios">ATC Staff</a></li><li><a href="/about-atc/board-of-directors">Board of Directors</a></li><li><a href="/about-atc/atc-endowment-fund">Endowment Fund</a></li><li><a href="/about-atc/news-portal">News</a></li><li><a href="/about-atc/partners-and-links">Partners and Links</a></li><li><a href="/about-atc/staff-contact-information">Contact Information</a></li></ul></div></div></div></li><li aria-expanded="false" aria-haspopup="true" class="uk-parent" data-uk-dropdown="{'preventflip':'y'}"><a href="#">Projects</a><div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-1"><div class="uk-grid uk-dropdown-grid"><div class="uk-width-1-1"><ul class="uk-nav uk-nav-navbar"><li><a href="/projects/projects-active">Active Projects</a></li><li><a href="/projects/projects-archived">Completed Projects</a></li></ul></div></div></div></li><li><a href="/events">Events</a></li><li><a href="/training-info1">Trainings</a></li><li><a href="https://store.atcouncil.org/">Products</a></li><li aria-expanded="false" aria-haspopup="true" class="uk-parent" data-uk-dropdown="{'preventflip':'y'}"><a href="https://hazards.atcouncil.org/" rel="noopener noreferrer" target="_blank">Design Loads</a><div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-1"><div class="uk-grid uk-dropdown-grid"><div class="uk-width-1-1"><ul class="uk-nav uk-nav-navbar"><li><a href="https://hazards.atcouncil.org/" rel="noopener noreferrer" target="_blank">Windspeed</a></li><li><a href="https://hazards.atcouncil.org/" rel="noopener noreferrer" target="_blank">Ground Snow Loads</a></li><li><a href="https://hazards.atcouncil.org/" rel="noopener noreferrer" target="_blank">Tornado</a></li><li><a href="https://hazards.atcouncil.org/" rel="noopener noreferrer" target="_blank">Seismic</a></li></ul></div></div></div></li><li><a href="http://nehrp-consultants.org/index.html" rel="noopener noreferrer" target="_blank">NEHRP CJV</a></li><li><a href="https://www.sacsteel.org/" rel="noopener noreferrer" target="_blank">SAC Steel</a></li></ul>
<a class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas="" href="#offcanvas"></a>
</nav>
<div class="tm-toolbar uk-clearfix uk-hidden-small">
<div class="uk-float-left"><div class="uk-panel"><ul class="uk-breadcrumb"><li class="uk-active"><span>Home</span></li></ul>
</div></div>
<div class="uk-float-right"><div class="uk-panel _gobtn">
<form action="/" class="uk-search" data-uk-search="{'source': '/component/search/?tmpl=raw&amp;type=json&amp;ordering=&amp;searchphrase=all', 'param': 'searchword', 'msgResultsHeader': 'Search Results', 'msgMoreResults': 'More Results', 'msgNoResults': 'No results found', flipDropdown: 1}" id="search-27-5ffee4b6a322f" method="post">
<input class="uk-search-field" name="searchword" placeholder="search..." type="text"/>
<input name="task" type="hidden" value="search"/>
<input name="option" type="hidden" value="com_search"/>
<input name="Itemid" type="hidden" value="461"/>
</form>
</div></div>
</div>
</div>
<section class="tm-top-b uk-grid tm-grid-block" data-uk-grid-match="{target:'&gt; div &gt; .uk-panel'}">
<div class="uk-width-1-1"><div class="uk-panel uk-panel-box _sidebox_grey"><h3 class="uk-panel-title">A Commitment to Ethics, Equity, Diversity, and Inclusion</h3>
<p><img src="/images/ATC_circlegradient.png" style="margin-right: 10px; margin-left: 10px; float: right;" width="175"/>As structural engineers, we frame our contributions to society through our professionalism and ethics in the duties we perform. In our practice, we serve society and are active participants in the fabric of the communities in which we live. As structural engineers, we cannot limit ourselves to solving only problems of science and engineering. Ignoring the social, economic, and political contexts surrounding our engineering work is a path to irrelevancy. Our active participation includes a moral obligation to right the social and ethical wrongs we observe.</p>
<p>Therefore, as part of our active strategic planning process, we will critically examine the ethical values, equity, and diversity present in our internal processes and practices related to our staff, clients, and consultants.  Please read our <a href="/docman/other-documents/274-atc-statement-on-ethics-diversity-equity-and-inclusion-2020-06/file" rel="noopener noreferrer" target="_blank">full statement</a> here.</p></div></div>
</section>
<div class="tm-middle uk-grid" data-uk-grid-match="">
<div class="tm-main uk-width-medium-1-3">
<section class="tm-main-top uk-grid tm-grid-block" data-uk-grid-match="{target:'&gt; div &gt; .uk-panel'}">
<div class="uk-width-1-1"><div class="uk-panel uk-panel-box _sidebox_grey"><h3 class="uk-panel-title">Now Available!</h3>
<p><a href="https://hazards.atcouncil.org/#/wind?lat=18.33576499999999&amp;lng=-64.896335&amp;address=U.S.%20Virgin%20Islands" rel="noopener noreferrer" target="_blank"><img src="/images/ATC-Haz-USVI.png" style="float: right; margin-right: 3px; margin-left: 3px;" width="120"/></a>U.S. Virgin Islands Special Wind Region is now integrated into the <a data-saferedirecturl="https://www.google.com/url?q=https://sable.madmimi.com/c/29313?id%3D3155431.8242.1.68415142904f54c56f35f702fa1f02f5&amp;source=gmail&amp;ust=1596133612357000&amp;usg=AFQjCNEcbBujVVkT2JWlt_2js4pY8UifQw" href="https://hazards.atcouncil.org/#/wind?lat=18.33576499999999&amp;lng=-64.896335&amp;address=U.S.%20Virgin%20Islands" rel="noopener noreferrer" target="_blank"><strong>ATC Hazards by Location</strong></a> online tool!</p></div></div>
</section>
<main class="tm-content">
<div id="system-message-container">
</div>
</main>
</div>
<aside class="tm-sidebar-a uk-width-medium-1-3"><div class="uk-panel uk-panel-box _sidebox_grey"><h3 class="uk-panel-title">Sign Up for ATC E-Mails</h3>
<p style="text-align: center;"><a href="/join-atc-s-e-mail-distribution-list" rel="noopener noreferrer" target="_blank"><img src="/images/SignUpButton.png" width="142"/></a></p>
<p style="text-align: center;"><span style="font-size: 12pt;"><span style="font-size: 8pt;"> </span>Receive up-to-date information on events and products.</span></p></div></aside>
<aside class="tm-sidebar-b uk-width-medium-1-3"><div class="uk-panel uk-panel-box _sidebox_grey"><h3 class="uk-panel-title">ATC Hazards by Location</h3>
<p><a href="https://hazards.atcouncil.org/" rel="noopener noreferrer" target="_blank"><img height="119" src="/images/HazardsSquare-crop.png" style="display: block; margin-left: auto; margin-right: auto;" width="234"/></a></p></div></aside>
</div>
<section class="tm-bottom-a uk-grid tm-grid-block" data-uk-grid-match="{target:'&gt; div &gt; .uk-panel'}">
<div class="uk-width-1-1"><div class="uk-panel uk-panel-box _sidebox_grey"><h3 class="uk-panel-title">Recently Released</h3>
<p><em><span style="font-size: 11pt;"><strong><a href="/docman/other-documents/275-atc-141/file" rel="noopener noreferrer" target="_blank"><img src="/images/ATC-141_Cover_bordered.png" style="float: left; margin-right: 10px;" width="150"/></a>September 19, 2017 Puebla-Morelos, Mexico Earthquake: Seismological and Structural Observations by the ATC Reconnaissance Team</strong></span></em></p>
<p>The <a href="/docman/other-documents/275-atc-141/file" rel="noopener noreferrer" target="_blank">ATC-141 report</a> documents the findings of the field team, including observation of earthquake effects for 70 buildings, microtremors recorded using monitoring instruments at 7 buildings, and a compilation of processed ground motion recordings from UNAM (National Autonomous University of Mexico) and CIRES (Center of Instrumentation and Seismic Records). This report also serves as a reference to two papers approved for publication in Earthquake Engineering Research Institute (EERI) Spectra Journal.</p>
<hr/>
<p><span style="font-size: 11pt;"><strong><img src="/images/00-CUREE-ATC143-2020_GeneralGuide_Cover-Front-bordered.jpg" style="float: right; margin-left: 10px;" width="150"/>CEA-EDA-01, <em> Earthquake Damage Assessment and Repair Guidelines for Residential Wood-Frame Buildings, Volume 1 – General Guidelines</em></strong></span></p>
<p>This two-volume series describes the process of identifying, evaluating, and repairing common earthquake damage in typical residential wood-frame houses and is intended to increase the efficiency, consistency, and reliability of the earthquake damage assessment and repair process. Volume 1 (<a href="/atc-143" rel="noopener noreferrer" target="_blank">CEA-EDA-01</a>) is intended to be used by insurance claim representatives, building contractors, homeowners, and others familiar with construction and repair. </p>
<hr/>
<p><span style="font-size: 11pt;"><strong><a href="/atc-143" rel="noopener noreferrer" target="_blank"><img src="/images/00-CEA-EDA-02_E_Cover-Front-bordered.jpg" style="float: left; margin-right: 10px;" width="150"/></a>CEA-EDA-02, <em>Earthquake Damage Assessment and Repair Guidelines for Residential Wood-Frame Buildings, Volume 2 – Engineering Guidelines</em><br/></strong></span></p>
<p>Volume 2 (<a href="/atc-143" rel="noopener noreferrer" target="_blank">CEA-EDA-02</a>) is intended to be used by structural and geotechnical engineers, and others with relevant technical experience. The <em>Guidelines</em> help users create a conceptual scope of repair for a wood-frame house damaged by an earthquake.</p>
<hr/>
<p><a href="/atc-137-2-urm" rel="noopener noreferrer" target="_blank"><img src="/images/00-ATC-137-2_Cover-bordered.png" style="float: right; margin-left: 10px;" width="150"/></a><span style="font-size: 11pt;"><strong><em>Proceedings: FEMA-Sponsored Summit on Unreinforced Masonry Buildings in Utah</em></strong></span></p>
<p>The Applied Technology Council is pleased to announce the availability for free of ATC-137-2,<a href="/atc-137-2-urm" rel="noopener noreferrer" target="_blank"> <em>Proceedings: FEMA-Sponsored Summit on Unreinforced Masonry Buildings in Utah</em></a> presents the summit agenda, include slides from the plenary presentations, and summarize the themes expressed during the breakout discussions. </p>
<hr/>
<p><span style="font-size: 11pt;"><strong><img src="/images/FEMAP-530_Cover-bordered.png" style="float: left; margin-right: 10px;" width="150"/>FEMA P-530, <em>Earthquake Safety at Home</em></strong></span></p>
<p>Half of all Americans live in areas subject to earthquake risk, and most Americans will travel to seismically active regions in their lifetime. <a data-saferedirecturl="https://www.google.com/url?q=https://sable.madmimi.com/c/29313?id%3D3321277.8305.1.80099eca573d7153584264ae2e36ed8f&amp;source=gmail&amp;ust=1589319750145000&amp;usg=AFQjCNFLrz3CiJDlPVNwZzCow59NdtjkbA" href="https://www.fema.gov/sites/default/files/2020-08/fema_earthquakes_fema-p-530-earthquake-safety-at-home-march-2020.pdf" rel="noopener noreferrer" target="_blank">FEMA P-530, <em>Earthquake Safety at Home</em></a>, shows readers why earthquakes matter where they live, and how they can Prepare, Protect, Survive, Respond, Recover, and Repair in response to an earthquake.</p>
<p align="left"> </p>
<p> </p></div></div>
</section>
<div class="tm-block-bottom">
<footer class="tm-footer">
<a class="tm-totop-scroller" data-uk-smooth-scroll="" href="#"></a>
<div class="uk-panel">
<div><address><span style="font-family: Arial,Helvetica,Geneva,Swiss,SunSans-Regular;">Applied Technology Council • 201 Redwood Shores Parkway, Suite 240 • Redwood City, California 94065 • (650) 595-1542</span></address></div></div>
</footer>
</div>
</div>
</div>
</div>
<div class="uk-offcanvas" id="offcanvas">
<div class="uk-offcanvas-bar"><ul class="uk-nav uk-nav-offcanvas"><li class="uk-active"><a href="/">Home</a></li><li class="uk-parent"><a href="/about-atc">About ATC</a><ul class="uk-nav-sub"><li><a href="/about-atc/employee-bios">ATC Staff</a></li><li><a href="/about-atc/board-of-directors">Board of Directors</a></li><li><a href="/about-atc/atc-endowment-fund">Endowment Fund</a></li><li><a href="/about-atc/news-portal">News</a></li><li><a href="/about-atc/partners-and-links">Partners and Links</a></li><li><a href="/about-atc/staff-contact-information">Contact Information</a></li></ul></li><li class="uk-parent"><a href="#">Projects</a><ul class="uk-nav-sub"><li><a href="/projects/projects-active">Active Projects</a></li><li><a href="/projects/projects-archived">Completed Projects</a></li></ul></li><li><a href="/events">Events</a></li><li><a href="/training-info1">Trainings</a></li><li><a href="https://store.atcouncil.org/">Products</a></li><li class="uk-parent"><a href="https://hazards.atcouncil.org/" rel="noopener noreferrer" target="_blank">Design Loads</a><ul class="uk-nav-sub"><li><a href="https://hazards.atcouncil.org/" rel="noopener noreferrer" target="_blank">Windspeed</a></li><li><a href="https://hazards.atcouncil.org/" rel="noopener noreferrer" target="_blank">Ground Snow Loads</a></li><li><a href="https://hazards.atcouncil.org/" rel="noopener noreferrer" target="_blank">Tornado</a></li><li><a href="https://hazards.atcouncil.org/" rel="noopener noreferrer" target="_blank">Seismic</a></li></ul></li><li><a href="http://nehrp-consultants.org/index.html" rel="noopener noreferrer" target="_blank">NEHRP CJV</a></li><li><a href="https://www.sacsteel.org/" rel="noopener noreferrer" target="_blank">SAC Steel</a></li></ul></div>
</div>
<script type="text/javascript">/*joomlatools job scheduler*/
!function(){function e(e,t,n,o){try{o=new(this.XMLHttpRequest||ActiveXObject)("MSXML2.XMLHTTP.3.0"),o.open("POST",e,1),o.setRequestHeader("X-Requested-With","XMLHttpRequest"),o.setRequestHeader("Content-type","application/x-www-form-urlencoded"),o.onreadystatechange=function(){o.readyState>3&&t&&t(o.responseText,o)},o.send(n)}catch(c){}}function t(n){e(n,function(e,o){try{if(200==o.status){var c=JSON.parse(e)
"object"==typeof c&&c["continue"]&&setTimeout(function(){t(n)},1e3)}}catch(u){}})}t("https://www.atcouncil.org/index.php?option=com_joomlatools&controller=scheduler")}()</script></body>
</html>