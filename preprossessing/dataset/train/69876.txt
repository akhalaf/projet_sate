<!DOCTYPE html>
<html lang="bg">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-15103454-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-15103454-1');
</script>
<title>��������� ���-���� �� ����� | ACMilan-bg.com</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="��������� ���-���� �� ����� ����� ������� ������ ������ ��� ���� ����� ��������" name="Description"/>
<meta content="���-���� �����, ��������� ���� �����, ����� �����, ����� �������, ����� ������, ����� ������, ��� ���� ����� ��������" name="keywords"/>
<meta charset="utf-8"/>
<meta content="����� ��������" name="author"/>
<meta content="index, follow" name="robots"/>
<meta content="2 days" name="revisit-after"/>
<meta content="YqkipM9ozS7gGd0AzHZYoB-a3uK5xL8EmF9F_4DAq80" name="google-site-verification"/>
<link href="//fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800&amp;subset=latin-ext" rel="stylesheet"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"/>
<link href="css/font-awesome.css?rand=4032" media="all" rel="stylesheet" type="text/css"/><!-- fontawesome css -->
<link href="css/bootstrap.css?rand=7066" media="all" rel="stylesheet" type="text/css"/><!-- Bootstrap stylesheet -->
<link href="css/style.css?rand=3987" media="all" rel="stylesheet" type="text/css"/><!-- stylesheet -->
</head>
<body>
<!-- header -->
<header>
<div class="container">
<!-- nav -->
<nav class="navbar navbar-inverse">
<div class="container-fluid">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<button class="navbar-toggle" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<div class="logo">
<h2><a href="index.php" title="��� ���� ����� ��������">ACMilan-bg.com</a></h2>
</div>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav">
<li><a href="index.php">������</a></li>
<li><a href="/forum/search.php?search_id=active_topics" target="_blank">�����</a></li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">���������� ⇓</a>
<ul class="dropdown-menu">
<li><a href="scorers.php">�����������</a></li>
<li><a href="serie-a-programa-2020-2021.php">����� "�" 20/21</a></li>
<li><a href="kupa+na+italia+2021.php">���� �� ������ 20/21</a></li>
<li><a href="europa-league-2021.php">���� ������ 20/21</a></li>
<li><a href="systav+treniori+futbolisti+milan+20202021.php">������ �� ����� 20/21</a></li>
<li class="text-center">---------------------------</li>
<li><a href="clprogram1314.php">���������� ���� 13/14</a></li>
<li><a href="uefaprogram0809.php">���� �� ���� 08/09</a></li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">������� ⇓</a>
<ul class="dropdown-menu">
<li><a href="history.php">�����</a></li>
<li><a href="san-siro-history.php">��� ����</a></li>
<li><a href="milanello.php">��������</a></li>
<li><a href="trofei-milan.php">������</a></li>
<li><a href="acmvs.php">���������</a></li>
<li><a href="archiv.php">������� ������</a></li>
<li><a href="legends.php">���������</a></li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">������ ⇓</a>
<ul class="dropdown-menu">
<li><a href="acmilan-ultras.php">�������</a></li>
<li><a href="tattoo.php">����������</a></li>
<li><a href="milan-curva-sud-pesni.php">����� �� Curva Sud</a></li>
<li><a href="famousint.php">����� �����</a></li>
<li><a href="museum.php">����� �� ����</a></li>
</ul>
</li>
<li><a href="addnews.php">������ ������</a></li>
</ul>
</div>
<!-- /.navbar-collapse -->
</div>
<!-- /.container-fluid -->
</nav>
<!-- //nav -->
</div>
</header>
<!-- //header -->
<!-- google mobile ads -->
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-1448719579000760",
    enable_page_level_ads: true
  });
</script>
<!-- //google mobile ads -->
<!-- google analytics -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-15103454-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-15103454-1');
</script>
<!-- //google analytics -->
<!-- main-content -->
<div class="wthree-main-content">
<!-- blog-section -->
<section class="blog-posts">
<!-- blog -->
<div class="blog">
<div class="container">
<h2 class="agile-inner-title">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Milan2017 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-1448719579000760" data-ad-format="auto" data-ad-slot="5563391806" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</h2>
<div class="bolg-posts">
<!-- begin interview -->
<div class="col-md-12 blog-top">
<div class="blog-in"> <a class="blog-post" href="leon_news_full.php?id=33738"> <img alt="��������� ���������� � ������� ������ �������� ��������" src="images/60005aa99ecaf.jpg"/></a>
<div class="blog-grid">
<div class="date">
<span class="date-in">14.1.2021</span>
<div class="clearfix"> </div>
</div>
<h3><a href="leon_news_full.php?id=33738" title="��������� ���������� � ������� ������ �������� ��������">��������� ���������� � ������� ������ �������� ��������</a>
</h3>
<p></p><p>������ ���������� � �������������� � ��������� ������� ������ �������� ��������.</p>
<p> </p>
<p>�������� ������� ����� ������� �������� ������, �� ����������� �� ���������� �� � �������...<br/><br/><a class="smore" href="leon_news_full.php?id=33738">��� ������ ������</a>
</p>
</div>
<div class="clearfix"></div>
</div>
<i class="black"> </i>
</div>
<!-- end interview -->
<!-- begin interview -->
<div class="col-md-12 blog-top">
<div class="blog-in"> <a class="blog-post" href="leon_news_full.php?id=33737"> <img alt="����������, �� �� ���������: ����� � ����� �� �����" src="images/600044ffa90f8.jpg"/></a>
<div class="blog-grid">
<div class="date">
<span class="date-in">14.1.2021</span>
<div class="clearfix"> </div>
</div>
<h3><a href="leon_news_full.php?id=33737" title="����������, �� �� ���������: ����� � ����� �� �����">����������, �� �� ���������: ����� � ����� �� �����</a>
</h3>
<p></p><p>����� �� ��������� � ������ �� ��������� �� ������� �����. �������������� �� ������� � ������ ���� �� "��� ����" �� ���� �� ������, � ���� ������ "����������" �� ����� �� �� �������� �� �...<br/><br/><a class="smore" href="leon_news_full.php?id=33737">��� ������ ������</a>
</p>
</div>
<div class="clearfix"></div>
</div>
<i class="black"> </i>
</div>
<!-- end interview -->
<!-- begin interview -->
<div class="col-md-12 blog-top">
<div class="blog-in"> <a class="blog-post" href="leon_news_full.php?id=33736"> <img alt="����: � ���� �� ������� ��������� ������ � ������� �� �����" src="images/60000314a475a.jpg"/></a>
<div class="blog-grid">
<div class="date">
<span class="date-in">14.1.2021</span>
<div class="clearfix"> </div>
</div>
<h3><a href="leon_news_full.php?id=33736" title="����: � ���� �� ������� ��������� ������ � ������� �� �����">����: � ���� �� ������� ��������� ������ � ������� �� �����</a>
</h3>
<p></p><p>��������� ��������� ������� ��������� ����. ������� ����� �� ����� ����������� �� ������� ��� ����������� �� �� �������-������� ��������. "������� ��, �� ���� ��������� �� ������� �� � ��...<br/><br/><a class="smore" href="leon_news_full.php?id=33736">��� ������ ������</a>
</p>
</div>
<div class="clearfix"></div>
</div>
<i class="black"> </i>
</div>
<!-- end interview -->
<!-- begin interview -->
<div class="col-md-12 blog-top">
<div class="blog-in"> <a class="blog-post" href="leon_news_full.php?id=33735"> <img alt="����� ���������� ��������� �� ����� ����, ���� � ���������" src="images/5fffd21673dc6.jpeg"/></a>
<div class="blog-grid">
<div class="date">
<span class="date-in">14.1.2021</span>
<div class="clearfix"> </div>
</div>
<h3><a href="leon_news_full.php?id=33735" title="����� ���������� ��������� �� ����� ����, ���� � ���������">����� ���������� ��������� �� ����� ����, ���� � ���������</a>
</h3>
<p></p><p>����� � ��������� 34-�������� ����� ���������. �������� ��������� � ��������� ������� ����������� �� ������, �� �� ������ ������������ �� �������� � �����, ��������� �������� ����� �� ���...<br/><br/><a class="smore" href="leon_news_full.php?id=33735">��� ������ ������</a>
</p>
</div>
<div class="clearfix"></div>
</div>
<i class="black"> </i>
</div>
<!-- end interview -->
<!-- begin interview -->
<div class="col-md-12 blog-top">
<div class="blog-in"> <a class="blog-post" href="leon_news_full.php?id=33734"> <img alt="���������� ����� �� ������ �� � ���� ������" src="images/5fff22d611282.jpg"/></a>
<div class="blog-grid">
<div class="date">
<span class="date-in">13.1.2021</span>
<div class="clearfix"> </div>
</div>
<h3><a href="leon_news_full.php?id=33734" title="���������� ����� �� ������ �� � ���� ������">���������� ����� �� ������ �� � ���� ������</a>
</h3>
<p></p><p>���� ���� ����� ����� �������� ������ ���� ���������� �� �����, ���� ����� ��������� ���������� � ��� �� ������ � 119-��� ������.</p>
<p> </p>
<p>����� ������ �� �������� ���� �� 1/4-���...<br/><br/><a class="smore" href="leon_news_full.php?id=33734">��� ������ ������</a>
</p>
</div>
<div class="clearfix"></div>
</div>
<i class="black"> </i>
</div>
<!-- end interview -->
<!-- begin interview -->
<div class="col-md-12 blog-top">
<div class="blog-in"> <a class="blog-post" href="leon_news_full.php?id=33733"> <img alt="���� �������� �� ������: ����� ����� ����� ������ �� ������, � ������ � ..." src="images/5fff0a7d50b77.jpg"/></a>
<div class="blog-grid">
<div class="date">
<span class="date-in">13.1.2021</span>
<div class="clearfix"> </div>
</div>
<h3><a href="leon_news_full.php?id=33733" title="���� �������� �� ������: ����� ����� ����� ������ �� ������, � ������ � ...">���� �������� �� ������: ����� ����� ����� ������ �� ������, � ������ � ...</a>
</h3>
<p></p><p>���� ����������� �� TuttoMercatoWeb ������� ������� ������� �������� �� ������ � ������, ������� �������� , �� �� ��������� ���������� �������: </p>
<p> </p>
<p>"������� ����� �� �� ����...<br/><br/><a class="smore" href="leon_news_full.php?id=33733">��� ������ ������</a>
</p>
</div>
<div class="clearfix"></div>
</div>
<i class="black"> </i>
</div>
<!-- end interview --><ul class="start">
<li><span style="color:#b30000;">1</span></li>
<li><a href="leon_news.php?cat=&amp;last=6&amp;cur_page=2">2</a></li>
<li><a href="leon_news.php?cat=&amp;last=12&amp;cur_page=3">3</a></li>
<li><a href="leon_news.php?cat=&amp;last=18&amp;cur_page=4">4</a></li>
<li><a href="leon_news.php?cat=&amp;last=24&amp;cur_page=5">5</a></li>
...	<li><a href="leon_news.php?cat=&amp;last=32718&amp;cur_page=5454">5454</a></li>
<li><a class="next" href="leon_news.php?cat=&amp;last=6&amp;cur_page=2"> &gt;&gt; </a></li>
</ul>
<div class="clearfix"> </div>
</div>
</div>
</div>
</section>
</div>
<!-- //main-content -->
<!-- footer -->
<footer>
<div class="agileinfo-footer">
<div class="container">
<div class="agileits-footer-top">
<div class="col-sm-4 w3ls-address-grid">
<i class="glyphicon glyphicon-check"></i>
<div class="w3-address1">
<h6>���� �� ������</h6>
<p>
 

- <a href="http://acmilan-bg.com/forum/search.php?search_id=active_topics" target="_blank" title="����� �����">
��� ��������� ����</a>
</p>
</div>
<div class="clearfix"> </div>
</div>
<div class="col-sm-4 w3ls-address-grid">
<i class="glyphicon glyphicon-time"></i>
<div class="w3-address1">
<h6>������� ���</h6>
<p>

18.01.2021 | 21:45 �.
<br/>������ - �����
<br/><a href="http://acmilan-bg.com/forum/viewtopic.php?f=43&amp;t=320&amp;sk=t&amp;sd=d"><img alt="��� ���� ����� ��������" class="img-responsive-next-game" src="next-games/cagliari-milan.png"/></a>
<br/>- <a href="/leon_news_full.php?id=22022" style="background-color:#FFDF00; border-radius:12px 20px;padding:0.2em; color:#000" title="������ �� ����� ������">������ ���� ������</a>
</p>
<div class="w3-address1">
<h6>�������� ���</h6>
<p>
����� 0 (5) : (4) 0 ������

<br/><br/>- <a href="/serie-a-programa-2020-2021.php" style="background-color:#b30000; border-radius:12px 20px;padding:0.2em;" title="��������� ����� �">��������� ����� "�"</a>
<br/><br/>- <a href="/scorers.php" style="background-color:#b30000; border-radius:12px 20px;padding:0.2em;" title="����������� ����� �">�����������</a>
</p>
</div>
</div>
<div class="clearfix"> </div>
</div>
<div class="col-sm-4 w3ls-address-grid">
<i class="glyphicon glyphicon-user"></i>
<div class="w3-address1">
<h6>���������</h6>
<p><a href="http://www.rotatape.com" target="_blank" title="����� ���� ����"><img alt="����� ���� ����" class="img-responsive-partners" src="rotatape-banner.jpg"/></a>
</p>
</div>
<div class="clearfix"> </div>
</div>
<div class="clearfix"> </div>
</div>
</div>
<div class="agileits-footer-bottom">
<div class="container">
<div class="container-switch">
<button id="switch" onclick="toggleTheme()"><img alt="����� �����" src="theme.png"/> - ����� ����� �� �����</button>
</div>
<script>
        // function to set a given theme/color-scheme
        function setTheme(themeName) {
            localStorage.setItem('theme', themeName);
            document.documentElement.className = themeName;
        }

        // function to toggle between light and dark theme
        function toggleTheme() {
            if (localStorage.getItem('theme') === 'theme-dark') {
                setTheme('theme-light');
            } else {
                setTheme('theme-dark');
            }
        }

        // Immediately invoked function to set the theme on initial load
        (function () {
            if (localStorage.getItem('theme') === 'theme-dark') {
                setTheme('theme-dark');
            } else {
                setTheme('theme-light');
            }
        })();
    </script>
<p class="footer-class">��������� ���-���� �� ����� �2005-2021<br/><a href="/terms-of-use.php" style="color:#fff;">���� ������� �� ������ �� ��������� �� www.acmilan-bg.com</a></p>
<ul class="w3layouts-agileits-social">
<li><a href="https://www.facebook.com/ACMilan.bg.fan.site" target="_blank" title="����� �������� ��� �������"><i class="fa fa-facebook"></i></a></li>
<li><a href="https://twitter.com/ACMilanBg" target="_blank" title="����� �������� � ������"><i class="fa fa-twitter"></i></a></li>
<li><a href="http://acmilan-bg.com/rss.xml" target="_blank" title="����� �������� RSS"><i class="fa fa-rss"></i></a></li>
</ul>
<div class="clearfix"> </div>
</div>
</div>
</div>
</footer>
<!-- //footer -->
<script src="js/jquery-2.1.4.min.js" type="text/javascript"></script><!-- Required-js -->
<script src="js/bootstrap.min.js"></script><!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
				$(document).ready(function() {
					/*
						var defaults = {
						containerID: 'toTop', // fading element id
						containerHoverID: 'toTopHover', // fading element hover id
						scrollSpeed: 1200,
						easingType: 'linear' 
						};
					*/
										
					$().UItoTop({ easingType: 'easeOutQuart' });
										
					});
			</script>
<!-- start-smoth-scrolling -->
<script src="js/move-top.js" type="text/javascript"></script>
<script src="js/easing.js" type="text/javascript"></script>
<script type="text/javascript">
				jQuery(document).ready(function($) {
					$(".scroll").click(function(event){		
						event.preventDefault();
						$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
					});
				});
			</script>
<!-- start-smoth-scrolling -->
<!-- //here ends scrolling icon -->
<script>
(function(i,s,o,g,r,a,m){i['TyxoObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//s.tyxo.com/c.js','tx');
tx('create', 'TX-459418812580');
tx('pageview');
</script>
<script async="" data-ad-client="ca-pub-1448719579000760" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</body>
</html>
