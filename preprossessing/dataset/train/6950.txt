<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<title> Kabal og kortspill | 123kortspill.no</title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="kabal, kabaler, kortspill, edderkoppkabal, freecell, spill, spille, online, gratis, underholdning" name="keywords"/>
<meta content="Legg gratis kabal og spill underholdende kortspill med 123kortspill.no - 
Gratis kabaler og kortspill på nett." name="description"/>
<link href="favicon.ico" rel="shortcut icon"/>
<link href="https://fonts.googleapis.com/css?family=PT+Mono|Ubuntu&amp;display=swap" rel="stylesheet"/>
<link href="templates/blue3/css/reset.css" rel="stylesheet"/> <!-- CSS reset -->
<link href="templates/blue3/css/style.css" rel="stylesheet"/> <!-- Resource style -->
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
	     (adsbygoogle = window.adsbygoogle || []).push({
	          google_ad_client: "ca-pub-9605897519319769",
	          enable_page_level_ads: true
	     });
	</script>
</head>
<body>
<header class="cd-auto-hide-header">
<div class="logo"><a href="//www.123kortspill.no/"><img alt="Logo" src="templates/blue3/images/logo.png"/></a></div>
<nav class="cd-primary-nav">
<a class="nav-trigger" href="#cd-navigation">
<span>
<em aria-hidden="true"></em>
				Meny
			</span>
</a> <!-- .nav-trigger -->
<ul id="cd-navigation">
<li><a href="online-kabal-1.html">Kabal</a></li><li><a href="online-kortspill-1.html">Kortspill</a></li>
<li><a href="apper.html">Apper</a></li>
</ul>
</nav> <!-- .cd-primary-nav -->
</header> <!-- .cd-auto-hide-header -->
<main class="cd-main-content">
<div id="wrapper">
<div id="main">
<h1>Legg kabal online</h1>
<p>123kortspill.no er Norges første og største nettsted viet til kabal.

Spill kabal gratis online direkte i din nettleser. Med Norges størte samling av gratis kabaler finner du fort din favorittkabal. Vi har mange morsomme kabaler for barn og voksne, og blir du lei av en kabal, har vi alltid en kabal med større utfordringer.

Itillegg kan du spille populære kortspill online. Prøv for eksempel hjerter, hopp i havet, texas hold 'em, uno eller 31.

Lykke til! </p>
<h1>Kabal</h1>
<p>
</p><div class="wrapole clear">
<div>
<center>
<a class="game_link" href="spill-kabal-syverkabal.html">
<img class="game_icon" src="media/games_icons/klondike.jpg" width="90%"/>
<br/>
					7'er kabal</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-freecell.html">
<img class="game_icon" src="media/games_icons/freecell.jpg" width="90%"/>
<br/>
					Freecell</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-edderkoppkabal.html">
<img class="game_icon" src="media/games_icons/spider.jpg" width="90%"/>
<br/>
					Edderkoppkabal</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-pyramidekabal.html">
<img class="game_icon" src="media/games_icons/pyramid.jpg" width="90%"/>
<br/>
					Pyramide kabal</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-40thieves.html">
<img class="game_icon" src="media/games_icons/40thieves.jpg" width="90%"/>
<br/>
					De Førti Røverne</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-yukon.html">
<img class="game_icon" src="media/games_icons/yukon.jpg" width="90%"/>
<br/>
					Yukon kabal</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-golf.html">
<img class="game_icon" src="media/games_icons/golf.jpg" width="90%"/>
<br/>
					Golf kabal</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-canfield.html">
<img class="game_icon" src="media/games_icons/canfield.jpg" width="90%"/>
<br/>
					Canfield kabal</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-skorpionen.html">
<img class="game_icon" src="media/games_icons/scorpion.jpg" width="90%"/>
<br/>
					Skorpionen kabal</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-idioten.html">
<img class="game_icon" src="media/games_icons/acesup.jpg" width="90%"/>
<br/>
					Idioten kabal</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-montecarlo.html">
<img class="game_icon" src="media/games_icons/montecarlo.jpg" width="90%"/>
<br/>
					Monte Carlo</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-pokerkabal.html">
<img class="game_icon" src="media/games_icons/pokerkabal.jpg" width="90%"/>
<br/>
					Poker kabal</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-labellelucie.html">
<img class="game_icon" src="media/games_icons/labellelucie.jpg" width="90%"/>
<br/>
					Den Vakre Louise</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-blomsterhagen.html">
<img class="game_icon" src="media/games_icons/flowergarden.jpg" width="90%"/>
<br/>
					Flower Garden</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-tripeaks.html">
<img class="game_icon" src="media/games_icons/tripeaks.jpg" width="90%"/>
<br/>
					Tripeaks kabal</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-baronessen.html">
<img class="game_icon" src="media/games_icons/baroness.jpg" width="90%"/>
<br/>
					Baronessen kabal</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-kingalbert.html">
<img class="game_icon" src="media/games_icons/kingalbert.jpg" width="90%"/>
<br/>
					King Albert kabal</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-calculation.html">
<img class="game_icon" src="media/games_icons/calculation.jpg" width="90%"/>
<br/>
					Calculation kabal</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-spiderette.html">
<img class="game_icon" src="media/games_icons/spiderette.jpg" width="90%"/>
<br/>
					Spiderette kabal</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-lyktemannen.html">
<img class="game_icon" src="media/games_icons/lyktemannen.jpg" width="90%"/>
<br/>
					Lyktemannen kabal</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kabal-simon.html">
<img class="game_icon" src="media/games_icons/simon.jpg" width="90%"/>
<br/>
					Simple Simon</a><br/>&amp;nbsp
					</center>
</div>
</div>
<p>
<a href="online-kabal-1.html">Finn flere kabaler her...</a>
</p>
<h1>Kortspill</h1>
<p>
</p><div class="wrapole clear">
<div>
<center>
<a class="game_link" href="spill-kortspill-hjerter.html">
<img class="game_icon" src="media/games_icons/hearts.jpg" width="90%"/>
<br/>
					Hjerter</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kortspill-poker.html">
<img class="game_icon" src="media/games_icons/poker.jpg" width="90%"/>
<br/>
					Texas Hold Em Poker</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kortspill-uno.html">
<img class="game_icon" src="media/games_icons/uno.jpg" width="90%"/>
<br/>
					Uno</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kortspill-hoppihavet.html">
<img class="game_icon" src="media/games_icons/gofish.jpg" width="90%"/>
<br/>
					Hopp i Havet</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kortspill-videopoker.html">
<img class="game_icon" src="media/games_icons/videopoker.jpg" width="90%"/>
<br/>
					Video Poker</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kortspill-blackjack.html">
<img class="game_icon" src="media/games_icons/blackjack.jpg" width="90%"/>
<br/>
					Blackjack</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kortspill-3cardpoker.html">
<img class="game_icon" src="media/games_icons/3card.jpg" width="90%"/>
<br/>
					3 Card Poker</a><br/>&amp;nbsp
					</center>
</div>
<div>
<center>
<a class="game_link" href="spill-kortspill-krig.html">
<img class="game_icon" src="media/games_icons/war.jpg" width="90%"/>
<br/>
					Kortspillet Krig</a><br/>&amp;nbsp
					</center>
</div>
</div>
<p>
<a href="online-kortspill-1.html">Finn flere kortspill her...</a>
</p>
<p>
<b>Bøker om kabal og kortspill</b><br/>
Vil du lære mer om populære kortspill eller alverdens klassiske kabaler? Læs mer om historien bak kabalene og om 
strategiene i spillene. Finn en kabalbok eller bøker om kortspill <a href="boeker.html">her</a>.
				</p>
<p>
<b>Spill online</b><br/>
Hvis du vil legge flere <a href="http://www.xlspill.no/online-kabal-1.html">kabaler</a>, 
<a href="http://www.xlspill.no/online-brettspill-1.html">spille brettspill</a>, løse 
<a href="http://www.xlspill.no/spill-hjernetrim-sudoku.html">sudoku</a> oppgaver eller spille andre 
<a href="http://www.xlspill.no/online-hjernetrim-1.html">hjernetrim spill</a>, så er xlspill.no kanskje noe for deg. Her finnes det massevis av 
gratis <a href="http://www.xlspill.no/">online spill</a> som kan spilles direkte i 
nettleseren.
				</p>
<p>
<b>Kabal på jobben øker produktivitet</b><br/>
Trenger du pause fra jobben? 
Ifølge Aftenposten viser resultatene av nederlandsk forskning, 
at kabal på jobben kan hjelpe med trivsel og produktivitet. 
Les hele saken her.<br/>
Unn deg en velfortjent pause!
				</p>
<p>
<b>Kabal på nett?</b><br/>
Spill online kabaler, freecell kabal, edderkoppkabal, pyramidekabal og 7'er kabal,
itillegg til 15 andre online kortspill som du kan spille helt gratis. Her kan du spille hjerter, poker, hopp i havet eller 
kortspillet 31.
				</p>
<p>
<b>Hekta på kabal online?</b><br/>
Ifølge Nettavisen er kabal mer vanedannende enn det avanserte "World of Warcraft". 
Er du hekta på kabal? Er det vanskelig for deg å stoppe å spille før kabalen har gått opp? 
Les hele saken her.
				</p><p>
<b>Lei av kabal?</b><br/>
Om du kjenner til de morsomme kortspill på Monte Carlos' kasinoer eller til 
spilleautomaten FruitMachine er kanskje gratiskasinoet 123casino.no noe for deg. 
Det finnes massevis av morsomme spilleautomater og 
andre spendende spill.
				</p>
</div>
<div id="aside">
<p>
</p><h1>Populære emner</h1>
<p>
</p><center>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 123kortspill_responsiv -->
<ins class="adsbygoogle" data-ad-client="ca-pub-9605897519319769" data-ad-format="auto" data-ad-slot="8044900041" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</center>
</div>
</div>
</main> <!-- .cd-main-content -->
<main class="cd-footer-content">
<div id="wrapper">
<div class="wrapole clear">
<div>
<h1>Kabal</h1>
<p>
<a href="spill-kabal-syverkabal.html">7'er kabal</a><br/>
<a href="spill-kabal-freecell.html">Freecell</a><br/>
<a href="spill-kabal-pyramidekabal.html">Pyramiden</a><br/>
<a href="spill-kabal-40thieves.html">Førti Røverne</a><br/>
<a href="spill-kabal-edderkoppkabal.html">Edderkoppkabal</a><br/>
<a href="spill-kabal-yukon.html">Yukon</a><br/>
<a href="spill-kabal-golf.html">Golf</a><br/>
<a href="spill-kabal-canfield.html">Dæmonen</a><br/>
<a href="spill-kabal-idioten.html">Idioten</a><br/>
<a href="spill-kabal-pokerkabal.html">Poker kabal</a><br/>
<a href="spill-kabal-skorpionen.html">Skorpionen</a><br/>
<a href="spill-kabal-blomsterhagen.html">Blomsterhagen</a><br/>
<a href="spill-kabal-montecarlo.html">Monte Carlo</a><br/>
<a href="spill-kabal-tripeaks.html">Tripeaks</a><br/>
<a href="spill-kabal-baronessen.html">Baronessen</a><br/>
<a href="spill-kabal-labellelucie.html">Vakre Louise</a><br/>
<a href="spill-kabal-calculation.html">Calculation</a><br/>
<a href="spill-kabal-spiderette.html">Spiderette</a><br/>
<a href="spill-kabal-kingalbert.html">Kong Albert</a><br/>
<a href="spill-kabal-lyktemannen.html">Lyktemannen</a><br/>
<a href="spill-kabal-simon.html">Simple Simon</a><br/>
</p>
</div>
<div>
<h1>Kortspill</h1>
<p>
<a href="spill-kortspill-hjerter.html">Hjerter</a><br/>
<a href="spill-kortspill-poker.html">Texas Hold 'em</a><br/>
<a href="spill-kortspill-uno.html">Uno</a><br/>
<a href="spill-kortspill-hoppihavet.html">Hopp i Havet</a><br/>
<a href="spill-kortspill-blackjack.html">Blackjack</a><br/>
<a href="spill-kortspill-krig.html">Krig</a><br/>
<a href="spill-kortspill-videopoker.html">Video Poker</a><br/>
<a href="spill-kortspill-3cardpoker.html">3 Card Poker</a><br/>
</p>
</div>
<div>
<h1>Sider</h1>
<p>
<a href="apper.html">Apper</a><br/>
<a href="info-kabal-1.html">Kabal regler</a><br/>
<a href="lenker.html">Lenker</a><br/>
<a href="cookies.html">Informasjonskapsler</a><br/>
<a href="browser.html">Moderne nettleser</a><br/>
<a href="boeker.html">Bøker</a><br/>
<a href="nojava.html">Java</a><br/>
<a href="noflash.html">Flash</a><br/>
</p>
</div>
<div>
<h1>Prøv også</h1>
<p><a href="http://www.tankespill.no/">Hjernetrim</a></p>
</div>
</div>
</div>
</main>
<main class="cd-footer-content">
<div id="wrapper">
<h1>123kortspill.no - 2019</h1>
<p>
<a href="http://www.solitaireclassics.com/"><img alt="Solitaire Classics" border="0" src="templates/blue3/images/us.png"/></a>
<a href="http://www.123patience.de/"><img alt="Patience DE" border="0" src="templates/blue3/images/de.png"/></a>
<a href="http://www.123patiens.se/"><img alt="Patiens SE" border="0" src="templates/blue3/images/sv.png"/></a>
<a href="http://www.kabaler.dk/"><img alt="Kabale DK" border="0" src="templates/blue3/images/da.png"/></a>
<a href="http://www.123kortspill.no/"><img alt="Kabal NO" border="0" src="templates/blue3/images/no.png"/></a>
</p>
</div>
</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script>
	if( !window.jQuery ) document.write('<script src="templates/blue3/js/jquery-3.0.0.min.js"><\/script>');
</script>
<script src="templates/blue3/js/main.js"></script> <!-- Resource jQuery -->
<!-- Histats.com  START  (aync)-->
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,1773897,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('//s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="/" target="_blank"><img alt="website page counter" border="0" src="//sstatic1.histats.com/0.gif?1773897&amp;101"/></a></noscript>
<!-- Histats.com  END  -->
<script language="JavaScript" src="//www.123kortspill.no/js/cookiewarn.js" type="text/javascript"></script>
</body>
</html>
