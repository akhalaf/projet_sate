<html>
<head>
<title>BooniePlanet - We're Sorry</title>
<link href="css/style.css" rel="stylesheet"/>
</head>
<body>
<div class="main_content">
<div class="bg_img">
<img src="img/goodbye_BP.png"/>
<div class="button_container">
<div class="left glow btn">
<a href="https://www.moviestarplanet.com/">
<img alt="MovieStarPlanet" src="img/MSP_Logo.png"/>
</a>
</div>
<div class="center glow btn">
<a href="https://www.moviestarplanet2.com/">
<img alt="MovieStarPlanet2" src="img/MSP2_Logo.png"/>
</a>
</div>
<div class="right glow btn">
<a href="https://www.blockstarplanet.com/">
<img alt="BlockStarPlanet" src="img/BSP_Logo.png"/>
</a>
</div>
</div>
</div>
</div>
</body>
</html>