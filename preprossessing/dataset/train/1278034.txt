<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>404 - Page Not Found</title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300" rel="stylesheet" type="text/css"/>
<link href="https://components.mywebsitebuilder.com/gator/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://components.mywebsitebuilder.com/gator/holding.css" rel="stylesheet"/>
</head>
<body>
<div class="wrapper">
<div class="cell">
<div class="row">
<div class="columns small-12 text-center">
<a href="https://www.gator.com"><div class="site-logo"></div></a>
<div class="pnf">
<img alt="404" src="https://components.mywebsitebuilder.com/gator/404.png"/>
</div>
<h1>Page Not Found</h1>
<p>Oh, man. Looks like you’re totally lost.</p>
</div>
</div>
<div class="row">
<div class="columns small-12 text-center">
<a class="button large round" href="/">
                        Homepage
                    </a>
</div>
</div>
</div>
</div>
</body>
</html>
