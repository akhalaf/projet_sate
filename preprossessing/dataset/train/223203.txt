<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 no-js" lang="en-US"> <![endif]--><!--[if IE 7 ]>    <html class="ie ie7 no-js" lang="en-US"> <![endif]--><!--[if IE 8 ]>    <html class="ie ie8 no-js" lang="en-US"> <![endif]--><!--[if IE 9 ]>    <html class="ie ie9 no-js" lang="en-US"> <![endif]--><!--[if gt IE 9]><!--><html class="no-js" lang="en-US"><!--<![endif]-->
<head profile="http://gmpg.org/xfn/11">
<title>BioWare | Rich Stories, Unforgettable Characters, And Vast Worlds</title>
<!-- Site Settings Meta -->
<meta charset="utf-8"/>
<meta content="index,follow" name="robot"/>
<meta content="©2021 Electronic Arts Inc. Trademarks belong to their respective owners. All rights reserved." name="copyright"/>
<meta content="Electronic Arts Inc." name="author"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" name="viewport"/>
<!-- SEO Meta -->
<meta content="BioWare develops high quality console, PC and online role-playing games, focused on rich stories, unforgettable characters and vast worlds to discover. Since 1995, BioWare has created some of the world's most critically acclaimed titles." name="description"/>
<meta content="bioware, ea, electronic arts, role-playing, games, anthem, dragon age, mass effect, baldur's gate, neverwinter nights, star wars: knights of the old republic, edmonton, austin, dai, mass effect 3, jobs, gaming, publisher, PC" name="keywords"/>
<!-- Facebook OpenGraph Meta -->
<meta content="BioWare" property="og:site_name"/>
<meta content="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/themes/bioware/assets/img/apple-touch-icon-144x144.png" property="og:image"/>
<!-- Icons -->
<link href="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/themes/bioware/assets/img/favicon.ico" rel="shortcut icon"/>
<link href="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/themes/bioware/assets/img/apple-touch-icon.png" rel="apple-touch-icon"/>
<link href="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/themes/bioware/assets/img/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/themes/bioware/assets/img/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/themes/bioware/assets/img/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<meta content="BioWare - Rich Stories, Unforgettable Characters, And Vast Worlds" name="application-name"/>
<meta content="#ffffff" name="msapplication-TileColor"/>
<meta content="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/themes/bioware/assets/img/apple-touch-icon.png" name="msapplication-TileImage"/>
<link href="https://www.bioware.com/xmlrpc.php" rel="pingback"/>
<link href="https://www.bioware.com/wp-content/themes/bioware/style.css" rel="stylesheet"/>
<script async="async" src="//consent.trustarc.com/notice?domain=ea.com&amp;c=teconsent&amp;js=nj&amp;noticeType=bb&amp;text=true"></script>
<script>var _STATE={};function runOnce(){if(!_STATE.hasRunOnce)if(window.PrivacyManagerAPI){window.addEventListener("message",function(e){try{var t=JSON.parse(e.data);t.PrivacyManagerAPI&&handleAPIResponse(t.PrivacyManagerAPI)}catch(e){"SyntaxError"!=e.name&&console.log(e)}},!1);var e={PrivacyManagerAPI:{self:"ea.com",action:"getConsent",timestamp:(new Date).getTime(),type:"functional"}};window.top.postMessage(JSON.stringify(e),"*"),_STATE.hasRunOnce=!0,_STATE.i&&clearInterval(_STATE.i)}else"undefined"==typeof truste||void 0===truste.eu.noticeLP||"behavior"in truste.eu.noticeLP||(_STATE.hasLoadedFunctional||(activateElement(document.querySelectorAll(".trustecm[trackertype=functional]")),_STATE.hasLoadedFunctional=!0),_STATE.hasRunOnce=!0,_STATE.i&&clearInterval(_STATE.i))}function getBehavior(){var e="",t=new RegExp("\\s*notice_behavior\\s*=\\s*([^;]*)").exec(document.cookie);return t&&t.length>1&&(e=t[1]),e}function handleAPIResponse(e){if(e.source&&"ea.com"==e.self&&(!/.*(,|)eu/i.test(getBehavior())||"asserted"==e.source)){_STATE.hasLoadedFunctional||"approved"==PrivacyManagerAPI.callApi("getConsent","ea.com",null,null,"functional").consent&&(activateElement(document.querySelectorAll(".trustecm[trackertype=functional]")),_STATE.hasLoadedFunctional=!0)}}function activateElement(list){if(!(list instanceof Array||list instanceof NodeList))throw"Illegal argument - must be an array";for(var item,i=list.length;i-- >0;)if(item=list[i],item.class="trustecm_done","script"===item.nodeName.toLowerCase()){var z=item.getAttribute("thesrc");if(z){var y=document.createElement("script");y.src=z,y.async=item.async,item.parentNode.insertBefore(y,item)}else eval(item.text||item.textContent||item.innerText)}}_STATE.i=setInterval(runOnce,10);</script>
<script>(async()=>{const a=fetch("https://www.ea.com/ccpa/banner").then(a=>a.text()).then(a=>{const b=new DOMParser().parseFromString(a,"text/html").querySelector(".eapl-ccpa-banner__link");return b});class b extends HTMLElement{constructor(){super()}connectedCallback(){const b=this.attachShadow({mode:"open"});a.then(a=>{if(a){var c=document.createElement("style");c.styleSheet?c.styleSheet.cssText="a { color: #666; text-decoration: none; } a:hover{ color: #576B8C; }":c.appendChild(document.createTextNode("a { color: #666; text-decoration: none; } a:hover{ color: #576B8C; }")),b.appendChild(c),b.appendChild(a.cloneNode(!0)),document.getElementById("california").style.display="block"}})}}customElements.define("ccpa-block",b)})();</script>
<script async="" class="trustecm" src="https://www.googletagmanager.com/gtag/js?id=UA-34713340-1" trackertype="functional"></script>
<script class="trustecm" trackertype="functional" type="text/plain">
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-34713340-1');
	</script>
<!-- wp_head() -->
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.bioware.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.1"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://eaassets-a.akamaihd.net/www.bioware.com/wp-includes/css/dist/block-library/style.min.css?ver=5.5.1" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bioware.com/wp-content/themes/bioware/assets/js/lib/magnific/magnific.css?ver=5.5.1" id="magnific_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bioware.com/wp-content/themes/bioware/overrides.css?ver=5.5.1" id="client_overrides-css" media="all" rel="stylesheet" type="text/css"/>
<script id="modernizr-js" src="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/themes/bioware/assets/js/lib/modernizr.js?ver=1383591210" type="text/javascript"></script>
<script id="jquery-js" src="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/themes/bioware/assets/js/lib/jquery.js?ver=1383591210" type="text/javascript"></script>
<script id="jquery_columnize-js" src="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/themes/bioware/assets/js/lib/jquery.columnize.js?ver=1396454037" type="text/javascript"></script>
<script id="plugins-js" src="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/themes/bioware/assets/js/plugins-ck.js?ver=1383591210" type="text/javascript"></script>
<script id="functions-js" src="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/themes/bioware/assets/js/functions.js?ver=1550190893" type="text/javascript"></script>
<link href="https://www.bioware.com/wp-json/" rel="https://api.w.org/"/><link href="https://www.bioware.com/wp-json/wp/v2/pages/5" rel="alternate" type="application/json"/><link href="https://www.bioware.com/" rel="canonical"/>
<link href="https://www.bioware.com/" rel="shortlink"/>
<link href="https://www.bioware.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.bioware.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://www.bioware.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.bioware.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
</head>
<body class="home page-template page-template-homepage page-template-homepage-php page page-id-5 id-5">
<div id="trunk-outer">
<div class="top_nav_mobile slideLeft"><ul class="menu" id="menu-main-nav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64" id="menu-item-64"><a href="https://www.bioware.com/about/">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-513" id="menu-item-513"><a href="https://www.bioware.com/games/">Games</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-102" id="menu-item-102"><a href="https://www.bioware.com/studios/">Studios</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1304" id="menu-item-1304"><a href="https://www.bioware.com/studios/bioware-edmonton/">Edmonton</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1306" id="menu-item-1306"><a href="https://www.bioware.com/studios/bioware-austin/">Austin</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-62" id="menu-item-62"><a href="https://www.bioware.com/careers/">Careers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61" id="menu-item-61"><a href="https://www.bioware.com/news/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2960" id="menu-item-2960"><a href="https://www.bioware.com/digitalmedia/">Digital Media</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1408" id="menu-item-1408"><a href="https://www.bioware.com/products/">Products</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1278" id="menu-item-1278"><a href="https://www.origin.com/en-ca/store/browse?q=bioware" rel="noopener noreferrer" target="_blank">Games</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1280" id="menu-item-1280"><a href="https://gear.bioware.com/" rel="noopener noreferrer" target="_blank">Merchandise</a></li>
</ul>
</li>
</ul></div> <header id="header">
<div class="contain">
<div class="menu-dropdown"><a class="no-link" href="#"></a></div>
<div id="site-logo">
<a href="https://www.bioware.com">BioWare: A Division of EA</a>
<img alt="BioWare" class="print-only" src="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/themes/bioware/assets/img/BioWare_dark_en.png"/>
</div>
<div class="top_nav"><ul class="menu" id="menu-main-nav-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64"><a href="https://www.bioware.com/about/">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-513"><a href="https://www.bioware.com/games/">Games</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-102"><a href="https://www.bioware.com/studios/">Studios</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1304"><a href="https://www.bioware.com/studios/bioware-edmonton/">Edmonton</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1306"><a href="https://www.bioware.com/studios/bioware-austin/">Austin</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-62"><a href="https://www.bioware.com/careers/">Careers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61"><a href="https://www.bioware.com/news/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2960"><a href="https://www.bioware.com/digitalmedia/">Digital Media</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1408"><a href="https://www.bioware.com/products/">Products</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1278"><a href="https://www.origin.com/en-ca/store/browse?q=bioware" rel="noopener noreferrer" target="_blank">Games</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1280"><a href="https://gear.bioware.com/" rel="noopener noreferrer" target="_blank">Merchandise</a></li>
</ul>
</li>
</ul></div> </div>
</header>
<div class="slideRight slideContent" id="trunk-inner">
<div id="wrapper">
<div id="main">
<article class="home-slides" id="section-home">
<div class="flexslider">
<ul class="slides">
<li class="slide rwdbg slide-what-role-will-you-play dark" data-rwdbg-640="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2017/06/HomePageSlider.jpg" data-rwdbg-768="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2017/06/HomePageSlider.jpg" data-rwdbg-default="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2017/06/HomePageSlider.jpg">
<div class="contain">
<h1 class="big" data-innertext="What Role&lt;br&gt; Will You Play?">
							What Role<br/> Will You Play?						</h1>
<p data-innertext="Creating Worlds of Adventure, Conflict, and Companionship that Inspire You to Become the Hero of Your Story">
							Creating Worlds of Adventure, Conflict, and Companionship that Inspire You to Become the Hero of Your Story						</p>
<a class="b" href="https://www.bioware.com/careers/#current-openings">Browse Current Openings</a>
</div>
</li>
<li class="slide rwdbg slide-create-something-amazing dark" data-rwdbg-640="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2013/08/backdrop_dai2.jpg" data-rwdbg-768="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2013/08/backdrop_dai2.jpg" data-rwdbg-default="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2013/08/backdrop_dai2.jpg">
<div class="contain">
<h1 class="big" data-innertext="Create Something Amazing.">
							Create Something Amazing.						</h1>
<p data-innertext="We’re Passionate about Quality in Our Workplace and In Our Products">
							We’re Passionate about Quality in Our Workplace and In Our Products						</p>
<a class="b" href="https://www.bioware.com/about">Discover Our Culture</a>
</div>
</li>
<li class="slide rwdbg slide-welcome-to-adventure light" data-rwdbg-640="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2013/08/MEA_Carousel-home640.jpg" data-rwdbg-768="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2013/08/MEA_Carousel-home768.jpg" data-rwdbg-default="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2013/08/MEA_Carousel.jpg">
<div class="contain">
<h1 class="big" data-innertext="Welcome to&lt;br&gt;Adventure.">
							Welcome to<br/>Adventure.						</h1>
<p data-innertext="BioWare Creates Games Focused on Rich Stories, Unforgettable Characters, and Vast Worlds">
							BioWare Creates Games Focused on Rich Stories, Unforgettable Characters, and Vast Worlds						</p>
<a class="b" href="https://www.bioware.com/studios">Explore The Studios</a>
</div>
</li>
<li class="slide rwdbg slide-work-hard-play-hard dark" data-rwdbg-640="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2013/08/home_4_01-640x500.jpg" data-rwdbg-768="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2013/08/home_4_01-768x650.jpg" data-rwdbg-default="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2013/08/home_4_01.jpg">
<div class="contain">
<h1 class="big" data-innertext="Work Hard,&lt;br&gt; Play Hard.">
							Work Hard,<br/> Play Hard.						</h1>
<p data-innertext="We’re a Company that Encourages Teamwork and Cares about Employees as Individuals">
							We’re a Company that Encourages Teamwork and Cares about Employees as Individuals						</p>
<a class="b" href="https://www.bioware.com/careers/#meet-the-team">Meet The Team</a>
</div>
</li>
</ul>
</div>
<div class="contain">
<ul class="slider-control">
<li></li>
<li></li>
<li></li>
<li></li>
</ul>
</div>
</article>
<div class="callouts games-nav">
<ul class="menu contain">
<li>
<a href="http://anthemthegame.com/">
<img alt="" class="attachment-col3 size-col3" height="48" loading="lazy" sizes="(max-width: 380px) 100vw, 380px" src="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2017/06/Anthem_Logo-380x48.png" srcset="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2017/06/Anthem_Logo.png 380w, https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2017/06/Anthem_Logo-300x38.png 300w, https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2017/06/Anthem_Logo-180x23.png 180w, https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2017/06/Anthem_Logo-360x45.png 360w" width="380"/> </a>
</li>
<li>
<a href="https://www.masseffect.com/">
<img alt="" class="attachment-col3 size-col3" height="133" loading="lazy" sizes="(max-width: 300px) 100vw, 300px" src="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2020/11/MELE-logo.png" srcset="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2020/11/MELE-logo.png 300w, https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2020/11/MELE-logo-180x80.png 180w" width="300"/> </a>
</li>
<li>
<a href="http://www.dragonage.com/">
<img alt="" class="attachment-col3 size-col3" height="101" loading="lazy" sizes="(max-width: 380px) 100vw, 380px" src="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2014/06/DragonAgeInquisition_Logo-380x101.png" srcset="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2014/06/DragonAgeInquisition_Logo-380x101.png 380w, https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2014/06/DragonAgeInquisition_Logo-300x79.png 300w, https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2014/06/DragonAgeInquisition_Logo-1024x272.png 1024w, https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2014/06/DragonAgeInquisition_Logo-180x47.png 180w, https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2014/06/DragonAgeInquisition_Logo-360x95.png 360w, https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2014/06/DragonAgeInquisition_Logo-760x202.png 760w, https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2014/06/DragonAgeInquisition_Logo-480x127.png 480w, https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2014/06/DragonAgeInquisition_Logo-960x255.png 960w, https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2014/06/DragonAgeInquisition_Logo-2048x544.png 2048w" width="380"/> </a>
</li>
<li>
<a href="http://www.swtor.com/">
<img alt="" class="attachment-col3 size-col3" height="104" loading="lazy" sizes="(max-width: 380px) 100vw, 380px" src="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2013/08/logo_swtor-380x104.png" srcset="https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2013/08/logo_swtor-380x104.png 380w, https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2013/08/logo_swtor-300x81.png 300w, https://eaassets-a.akamaihd.net/www.bioware.com/wp-content/uploads/2013/08/logo_swtor.png 382w" width="380"/> </a>
</li>
</ul>
</div>
</div>
<div class="sticky-push"></div>
</div> <!-- #wrapper -->
<footer class="source-org vcard copyright" id="footer">
<div class="contain">
<div class="nav nav-subsites"><ul class="menu" id="menu-footer-nav"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-200" id="menu-item-200"><a rel="noopener noreferrer" target="_blank">Social</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-202" id="menu-item-202"><a href="http://blog.bioware.com/" rel="noopener noreferrer" target="_blank">Blog</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-203" id="menu-item-203"><a href="http://twitter.com/BioWare" rel="noopener noreferrer" target="_blank">Twitter</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2687" id="menu-item-2687"><a href="https://www.instagram.com/biowarebase/" rel="noopener noreferrer" target="_blank">Instagram</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-204" id="menu-item-204"><a href="http://www.facebook.com/BioWare" rel="noopener noreferrer" target="_blank">Facebook</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1979" id="menu-item-1979"><a href="https://www.youtube.com/channel/UCePE_9yUaBe478B_CuCBcoA" rel="noopener noreferrer" target="_blank">YouTube</a></li>
</ul>
</li>
<li class="no-link menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-201" id="menu-item-201"><a rel="noopener noreferrer" target="_blank">Store</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-205" id="menu-item-205"><a href="https://www.origin.com/en-ca/store/browse?q=bioware" rel="noopener noreferrer" target="_blank">Games</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-206" id="menu-item-206"><a href="https://gear.bioware.com" rel="noopener noreferrer" target="_blank">Merchandise</a></li>
</ul>
</li>
</ul></div> <div class="legal">
<div class="logos">
<a class="ea" href="https://www.ea.com/">EA</a>
<a class="bioware" href="https://www.bioware.com">BioWare</a>
</div>
<div class="nav nav-legal"><ul class="menu" id="menu-footer-legal"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-70" id="menu-item-70"><a href="http://tos.ea.com/legalapp/WEBTERMS/US/en/PC/">User Agreement</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-71" id="menu-item-71"><a href="http://tos.ea.com/legalapp/WEBPRIVACY/US/en/PC/">Privacy &amp; Cookie Policy (Your Privacy Rights)</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-68" id="menu-item-68"><a href="http://www.ea.com/1/product-eulas">Legal</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-500" id="menu-item-500"><a href="https://help.ea.com/en/">Support</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-676" id="menu-item-676"><a href="http://investor.ea.com/search.cfm?method=and&amp;keyword=bioware">Press</a></li>
</ul></div> <div><small class="copy">©2021  Electronic Arts Inc.</small></div>
<div class="menu-legal-container">
<div class="copy" id="teconsent"></div>
<div class="copy" id="california"><ccpa-block></ccpa-block></div>
</div>
</div>
</div>
</footer>
<script>
				// Used by Gallery Custom Links to handle tenacious Lightboxes
				jQuery(document).ready(function () {

					function mgclInit() {
						if (jQuery.fn.off) {
							jQuery('.no-lightbox, .no-lightbox img').off('click'); // jQuery 1.7+
						}
						else {
							jQuery('.no-lightbox, .no-lightbox img').unbind('click'); // < jQuery 1.7
						}
						jQuery('a.no-lightbox').click(mgclOnClick);

						if (jQuery.fn.off) {
							jQuery('a.set-target').off('click'); // jQuery 1.7+
						}
						else {
							jQuery('a.set-target').unbind('click'); // < jQuery 1.7
						}
						jQuery('a.set-target').click(mgclOnClick);
					}

					function mgclOnClick() {
						if (!this.target || this.target == '' || this.target == '_self')
							window.location = this.href;
						else
							window.open(this.href,this.target);
						return false;
					}

					// From WP Gallery Custom Links
					// Reduce the number of  conflicting lightboxes
					function mgclAddLoadEvent(func) {
						var oldOnload = window.onload;
						if (typeof window.onload != 'function') {
							window.onload = func;
						} else {
							window.onload = function() {
								oldOnload();
								func();
							}
						}
					}

					mgclAddLoadEvent(mgclInit);
					mgclInit();

				});
			</script>
<script id="wp-embed-js" src="https://eaassets-a.akamaihd.net/www.bioware.com/wp-includes/js/wp-embed.min.js?ver=5.5.1" type="text/javascript"></script>
</div><!-- #trunk-inner -->
</div><!-- #trunk-outer -->
<div id="consent_blackbar"></div>
</body>
</html>
<!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Object Caching 89/101 objects using memcached
Page Caching using memcached (SSL caching disabled) 
Content Delivery Network via eaassets-a.akamaihd.net/www.bioware.com
Database Caching 8/16 queries in 0.025 seconds using memcached

Served from: www.bioware.com @ 2021-01-14 18:21:32 by W3 Total Cache
-->