<!DOCTYPE html>
<html dir="ltr" lang="en-GB">
<head>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>404 Page not found</title>
<link href="/media/gantry5/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/media/gantry5/engines/nucleus/css-compiled/nucleus.css" rel="stylesheet" type="text/css"/>
<link href="/templates/rt_isotope/custom/css-compiled/isotope__error.css" rel="stylesheet" type="text/css"/>
<link href="/templates/rt_isotope/custom/css-compiled/isotope-joomla__error.css" rel="stylesheet" type="text/css"/>
<link href="/templates/rt_isotope/custom/css-compiled/custom__error.css" rel="stylesheet" type="text/css"/>
<script src="https://ajax.googleapis.com/ajax/libs/mootools/1.5.2/mootools.min.js" type="text/javascript"></script>
<script src="/templates/rt_isotope/js/headroom.js" type="text/javascript"></script>
<!--[if (gte IE 8)&(lte IE 9)]>
        <script type="text/javascript" src="/media/gantry5/assets/js/html5shiv-printshiv.min.js"></script>
        <link rel="stylesheet" href="/media/gantry5/engines/nucleus/css/nucleus-ie9.css" type="text/css"/>
        <script type="text/javascript" src="/media/gantry5/assets/js/matchmedia.polyfill.js"></script>
        <![endif]-->
</head>
<body class="gantry site com_gantry5 view- no-layout no-task dir-ltr itemid-105 outline-_error g-offcanvas-left g-error g-style-preset1">
<div data-g-offcanvas-css3="1" data-g-offcanvas-swipe="1" id="g-offcanvas">
<div class="g-grid">
<div class="g-block size-100">
<div class="g-content g-particle" id="mobile-menu-6235-particle"> <div data-g-menu-breakpoint="48rem" id="g-mobilemenu-container"></div>
</div>
</div>
</div>
</div>
<div id="g-page-surround">
<div aria-controls="g-offcanvas" aria-expanded="false" class="g-offcanvas-hide g-offcanvas-toggle" data-offcanvas-toggle=""><i class="fa fa-fw fa-bars"></i></div>
<section class="g-headroom" id="g-navigation">
<div class="g-container"> <div class="g-grid">
<div class="g-block size-20">
<div class="g-content g-particle" id="logo-7061-particle"> <a aria-label="Balfour Properties" class="g-logo" href="/" rel="home" target="_self" title="Balfour Properties">
                        Balfour Properties
            </a>
</div>
</div>
<div class="g-block size-80">
<div class="g-content g-particle" id="menu-2019-particle"> <nav class="g-main-nav" data-g-hover-expand="true" role="navigation">
<ul class="g-toplevel">
<li class="g-menu-item g-menu-item-type-component g-menu-item-105 active g-standard ">
<a class="g-menu-item-container" href="/index.php">
<i aria-hidden="true" class="fa fa-home"></i>
<span class="g-menu-item-content">
<span class="g-menu-item-title">Home</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-131 g-standard ">
<a class="g-menu-item-container" href="/index.php/about-us">
<i aria-hidden="true" class="fa fa-info-circle"></i>
<span class="g-menu-item-content">
<span class="g-menu-item-title">About Us</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-132 g-standard ">
<a class="g-menu-item-container" href="/index.php/vacancies">
<i aria-hidden="true" class="fa fa-building-o"></i>
<span class="g-menu-item-content">
<span class="g-menu-item-title">Vacancies</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-133 g-standard ">
<a class="g-menu-item-container" href="/index.php/rental-application">
<i aria-hidden="true" class="fa fa-wpforms"></i>
<span class="g-menu-item-content">
<span class="g-menu-item-title">Rental Application</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-134 g-parent g-standard g-menu-item-link-parent ">
<a class="g-menu-item-container" href="/index.php/contact-us">
<i aria-hidden="true" class="fa fa-phone-square"></i>
<span class="g-menu-item-content">
<span class="g-menu-item-title">Contact Us</span>
</span>
<span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
<ul class="g-dropdown g-inactive g-zoom g-dropdown-right">
<li class="g-dropdown-column">
<div class="g-grid">
<div class="g-block size-100">
<ul class="g-sublevel">
<li class="g-level-1 g-go-back">
<a class="g-menu-item-container" data-g-menuparent="" href="#"><span>Back</span></a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-160 ">
<a class="g-menu-item-container" href="/index.php/contact-us/covid-19-announcement">
<span class="g-menu-item-content">
<span class="g-menu-item-title">COVID-19 Announcement</span>
</span>
</a>
</li>
</ul>
</div>
</div>
</li>
</ul>
</li>
</ul>
</nav>
</div>
</div>
</div>
</div>
</section>
<section id="g-top">
<div class="g-container"> <div class="g-grid">
<div class="g-block size-100 nomarginall nopaddingall">
<div class="g-system-messages">
<jdoc:include type="message"></jdoc:include>
</div>
</div>
</div>
</div>
</section>
<header id="g-header">
<div class="g-container"> <div class="g-grid">
<div class="g-block size-100 center">
<div class="g-content g-particle" id="infolist-3465-particle"> <div class="g-infolist g-1cols g-layercontent noborder">
<div class="g-infolist-item ">
<div class="g-infolist-item-text g-infolist-textstyle-header">
<div class="g-infolist-item-title ">
             
             
              Whooops! Error!
             
                      </div>
<div class="g-infolist-item-desc">
                        Looks like something went wrong. Sorry!
                      </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</header>
<section class="g-wrapper" id="g-container-5921">
<div class="g-container"> <div class="g-grid">
<div class="g-block size-100">
<section id="g-mainbar">
<div class="g-grid">
<div class="g-block size-100 center g-error">
<div class="g-content">
<h1>404 Page not found</h1>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
</section>
<section data-enllax-ratio="0.4" data-enllax-type="background" id="g-extension">
<div class="g-container"> <div class="g-grid">
<div class="g-block size-100 center">
<div class="g-content g-particle" id="infolist-8094-particle"> <div class="g-infolist g-1cols g-layercontent noborder">
<div class="g-infolist-item ">
<div class="g-infolist-item-text g-infolist-textstyle-header">
<div class="g-infolist-item-title ">
<a href="#">
             
              Nothing to See Here
                        </a>
</div>
<div class="g-infolist-item-desc">
                        Please use the navigation above or contact us to find what you are looking for.
                      </div>
<div class="g-infolist-link">
<a class="button " href="#">              Contact Us
            </a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section id="g-copyright">
<div class="g-container"> <div class="g-grid">
<div class="g-block size-33-3">
<div class="g-content g-particle" id="branding-8113-particle"> <div class="g-branding g-branding">
    Powered by <a class="g-powered-by" href="http://www.gantry.org/" title="Gantry Framework">Gantry<span class="hidden-tablet"> Framework</span></a>
</div>
</div>
</div>
<div class="g-block size-33-3">
<div class="g-content g-particle" id="copyright-3234-particle"> <div class="g-copyright ">
	Copyright ©
	2007 - 	2020
	<a href="" target="_blank" title="Balfour Properties">		Balfour Properties
	</a></div>
</div>
</div>
<div class="g-block size-33-3">
<div class="g-content g-particle" id="totop-8262-particle"> <div class="totop g-particle">
<div class="g-totop">
<a href="#" id="g-totop" rel="nofollow">
<i class="fa fa-arrow-circle-o-up fa-2x"></i> </a>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
<script src="/media/gantry5/assets/js/main.js" type="text/javascript"></script>
<jdoc:include name="debug" type="modules"></jdoc:include>
</body>
</html>
