<!DOCTYPE html>
<html lang="en">
<head>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-58643-34"></script>
<script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', "UA-58643-34");
    </script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Pastebin.com - Not Found (#404)</title>
<link href="/favicon.ico" rel="shortcut icon"/>
<meta content="Pastebin.com is the number one paste tool since 2002. Pastebin is a website where you can store text online for a set period of time." name="description"/>
<meta content="Pastebin.com is the number one paste tool since 2002. Pastebin is a website where you can store text online for a set period of time." property="og:description"/>
<meta content="231493360234820" property="fb:app_id"/>
<meta content="Pastebin.com - Not Found (#404)" property="og:title"/>
<meta content="article" property="og:type"/>
<meta content="https://pastebin.com/T2uiQHKN" property="og:url"/>
<meta content="https://pastebin.com/i/facebook.png" property="og:image"/>
<meta content="Pastebin" property="og:site_name"/>
<meta content="jkUAIOE8owUXu8UXIhRLB9oHJsWBfOgJbZzncqHoF4A" name="google-site-verification"/>
<link href="https://pastebin.com/T2uiQHKN" rel="canonical"/>
<meta content="width=device-width, initial-scale=0.75, maximum-scale=1.0, user-scalable=yes" name="viewport"/>
<meta content="_csrf-frontend" name="csrf-param"/>
<meta content="6BjIAZfDQmZHSc9Mo2GDEveDfGeJuJjHojA_DvMQMO6eTP1eoo0wVS8_oQ7FEtR2jewyNsDw8a-Pb0lMokNx1w==" name="csrf-token"/>
<link href="/assets/c80611c4/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/themes/pastebin/css/vendors.bundle.css?0b12af05872f846600ac" rel="stylesheet"/>
<link href="/themes/pastebin/css/app.bundle.css?0b12af05872f846600ac" rel="stylesheet"/>
<!-- 1-7L1jsUhq --></head><body><p>
﻿<script>
var datalayer= {
z_cltr: '%%CLICK_URL_UNESC%%',
z_imtr: '%%VIEW_URL_UNESC%%'
}
</script>
</p><div id="zbf8e95d3-c7c2-4b2e-8ead-ef78ee01f949" style="display:none"></div>
<script>!function(a,n,e,t,r){tagsync=e;var c=window[a];if(tagsync){var d=document.createElement("script");d.src="https://3933.tm.zedo.com/v1/3bb8ab82-c828-44f4-996d-e2a88f013315/atm.js",d.async=!0;var i=document.getElementById(n);if(null==i||"undefined"==i)return;i.parentNode.appendChild(d,i),d.onload=d.onreadystatechange=function(){var a=new zTagManager(n);a.initTagManager(n,c,this.aync,t,r)}}else document.write("<script src='https://3933.tm.zedo.com/v1/3bb8ab82-c828-44f4-996d-e2a88f013315/tm.js?data="+a+"'><"+"/script>")}("datalayer","zbf8e95d3-c7c2-4b2e-8ead-ef78ee01f949",true, 1 , 1);</script>
<!-- 0-g6EV1wGn -->
<div class="wrap">
<div class="header">
<div class="container">
<div class="header__container">
<div class="header__left">
<a class="header__logo" href="/">
                    Pastebin                </a>
<div class="header__links h_1024">
<a class="pro" href="/pro">GO</a>
<a href="/doc_api">API</a>
<a href="/tools">tools</a>
<a href="/faq">faq</a>
</div>
<a class="header__btn" href="/">
                    paste                </a>
</div>
<div class="header__right">
<div class="header_sign">
<a class="btn-sign sign-in" href="/login">Login</a>
<a class="btn-sign sign-up" href="/signup">Sign up</a>
</div>
</div>
</div>
</div>
</div>
<div class="container">
<div class="content">
<!-- 1-7L1jsUhq -->
<!-- 0-g6EV1wGn -->
<div style="padding-bottom:10px;">
<!-- 728x90 Placement Tag -->
<script async="" src="https://ssl.cdne.cpmstar.com/cached/js/lib.js"></script>
<div class="div-84910" style="width:728px; height:90px"></div>
<script>
(function(w,pid){
var y = "cpmstarx", els = w.document.getElementsByClassName("div-"+pid), pindex = els.length-1, el = els[pindex];
w[y]=w[y]||{};(w[y].libcmd=w[y].libcmd||[]).push({kind:'asynctagfetch',el:el,pid:pid,pindex:pindex});
})(window,84910);
</script>
</div>
<div class="page -top -right">
<div class="content__title">Not Found (#404)</div>
<div class="content__text">
<div class="notice -no-margin">
            This page is no longer available. It has either expired, been removed by its creator, or removed by one of the Pastebin staff.        </div>
</div>
</div>
<div style="clear: both;"></div>
<!-- 1-7L1jsUhq -->
<!-- 0-g6EV1wGn -->
<div style="padding-top:10px;">
<!-- 970x250 Placement Tag -->
<script async="" src="https://ssl.cdne.cpmstar.com/cached/js/lib.js"></script>
<div class="div-84911" style="width:970px; height:250px"></div>
<script>
(function(w,pid){
var y = "cpmstarx", els = w.document.getElementsByClassName("div-"+pid), pindex = els.length-1, el = els[pindex];
w[y]=w[y]||{};(w[y].libcmd=w[y].libcmd||[]).push({kind:'asynctagfetch',el:el,pid:pid,pindex:pindex});
})(window,84911);
</script>
</div>
</div>
<div class="sidebar h_1024">
<div class="sidebar__title">
<a href="/archive">Public Pastes</a>
</div>
<ul class="sidebar__menu">
<li>
<a href="/EUdVDJvb">scrape_me</a>
<div class="details">
                                            Python |
                    
                    2 min ago                </div>
</li>
<li>
<a href="/MJdCAb8Y">Untitled</a>
<div class="details">
                                            Rust |
                    
                    6 min ago                </div>
</li>
<li>
<a href="/gpWJUyJD">clean_private_info...</a>
<div class="details">
                                            Bash |
                    
                    34 min ago                </div>
</li>
<li>
<a href="/LNfgT74w">Untitled</a>
<div class="details">
                                            C++ |
                    
                    1 hour ago                </div>
</li>
<li>
<a href="/XB3VTrWc">alternate mobile m...</a>
<div class="details">
                                            PHP |
                    
                    2 hours ago                </div>
</li>
<li>
<a href="/SqwseiSM">My Log File</a>
<div class="details">
                                            HTML 5 |
                    
                    2 hours ago                </div>
</li>
<li>
<a href="/KuKieLw2">Untitled</a>
<div class="details">
                                            Lua |
                    
                    3 hours ago                </div>
</li>
<li>
<a href="/T0G12VQV">Untitled</a>
<div class="details">
                                            C++ |
                    
                    3 hours ago                </div>
</li>
</ul>
<!-- 1-7L1jsUhq -->
<!-- 0-g6EV1wGn -->
<div style="padding-top:10px;">
<!-- 160x600 Placement Tag -->
<script async="" src="https://ssl.cdne.cpmstar.com/cached/js/lib.js"></script>
<div class="div-84917" style="width:160px; height:600px"></div>
<script>
(function(w,pid){
var y = "cpmstarx", els = w.document.getElementsByClassName("div-"+pid), pindex = els.length-1, el = els[pindex];
w[y]=w[y]||{};(w[y].libcmd=w[y].libcmd||[]).push({kind:'asynctagfetch',el:el,pid:pid,pindex:pindex});
})(window,84917);
</script>
</div>
</div>
</div>
</div>
<div class="top-footer">
<a class="icon-link -size-24-24 -chrome" href="/tools#chrome" title="Google Chrome Extension"></a>
<a class="icon-link -size-24-24 -firefox" href="/tools#firefox" title="Firefox Extension"></a>
<a class="icon-link -size-24-24 -iphone" href="/tools#iphone" title="iPhone/iPad Application"></a>
<a class="icon-link -size-24-24 -windows" href="/tools#windows" title="Windows Desktop Application"></a>
<a class="icon-link -size-24-24 -android" href="/tools#android" title="Android Application"></a>
<a class="icon-link -size-24-24 -macos" href="/tools#macos" title="MacOS X Widget"></a>
<a class="icon-link -size-24-24 -opera" href="/tools#opera" title="Opera Extension"></a>
<a class="icon-link -size-24-24 -unix" href="/tools#pastebincl" title="Linux Application"></a>
</div>
<footer class="footer">
<div class="container">
<div class="footer__container">
<div class="footer__left">
<a href="/">create new paste</a> <span class="footer__devider"> / </span>
<a href="/languages">syntax languages</a> <span class="footer__devider"> / </span>
<a href="/archive">archive</a> <span class="footer__devider"> / </span>
<a href="/faq">faq</a> <span class="footer__devider"> / </span>
<a href="/tools">tools</a> <span class="footer__devider"> / </span>
<a href="/night_mode">night mode</a> <span class="footer__devider"> / </span>
<a href="/doc_api">api</a> <span class="footer__devider"> / </span>
<a href="/doc_scraping_api">scraping api</a>
<br/>
<a href="/doc_privacy_statement">privacy statement</a> <span class="footer__devider"> / </span>
<a href="/doc_cookies_policy">cookies policy</a> <span class="footer__devider"> / </span>
<a href="/doc_terms_of_service">terms of service</a><sup style="color:#999">updated</sup> <span class="footer__devider"> / </span>
<a href="/doc_security_disclosure">security disclosure</a> <span class="footer__devider"> / </span>
<a href="/dmca">dmca</a> <span class="footer__devider"> / </span>
<a href="/report-abuse">report abuse</a> <span class="footer__devider"> / </span>
<a href="/contact">contact</a>
<br/>
<!-- 1-7L1jsUhq -->
<!-- 0-g6EV1wGn -->
<br/>
<span class="footer__bottom h_800">
    By using Pastebin.com you agree to our <a href="/doc_cookies_policy">cookies policy</a> to enhance your experience.
    <br/>
    Site design &amp; logo © 2021 Pastebin</span>
</div>
<div class="footer__right h_1024">
<a class="icon-link -size-40-40 -facebook-circle" href="https://facebook.com/pastebin" rel="nofollow" target="_blank" title="Like us on Facebook"></a>
<a class="icon-link -size-40-40 -twitter-circle" href="https://twitter.com/pastebin" rel="nofollow" target="_blank" title="Follow us on Twitter"></a>
</div>
</div>
</div>
</footer>
<div class="popup-container">
<div class="popup-box -cookies" data-name="l2c_1">
            We use cookies for various purposes including analytics. By continuing to use Pastebin, you agree to our use of cookies as described in the <a href="/doc_cookies_policy">Cookies Policy</a>.             <span class="cookie-button js-close-cookies">OK, I Understand</span>
</div>
<div class="popup-box -pro" data-name="l2c_2_pg">
<div class="pro-promo-img">
<a href="/signup">
<img alt="" src="/themes/pastebin/img/hello.png"/>
</a>
</div>
<div class="pro-promo-text">
                Not a member of Pastebin yet?<br/>
<a href="/signup"><b>Sign Up</b></a>, it unlocks many cool features!            </div>
<div class="close js-close-pro-guest" title="Close Me"> </div>
</div>
</div>
<span class="cd-top"></span>
<script src="/assets/9ce1885/jquery.min.js"></script>
<script src="/assets/f04f76b8/yii.js"></script>
<script>
    const POST_EXPIRATION_NEVER = 'N';
    const POST_EXPIRATION_BURN = 'B';
    const POST_STATUS_PUBLIC = '0';
    const POST_STATUS_UNLISTED = '1';
</script>
<script src="/themes/pastebin/js/vendors.bundle.js?0b12af05872f846600ac"></script>
<script src="/themes/pastebin/js/app.bundle.js?0b12af05872f846600ac"></script>
<!-- 1-7L1jsUhq -->
<!-- 0-g6EV1wGn -->
</body></html>
