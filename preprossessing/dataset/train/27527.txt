<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Free 3D Screensavers - Download 3D Screensavers</title>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="3Planesoft is an independent screensaver developing company specializing 3D screensavers. The company has released 48 titles and is currently the most popular 3D screensaver maker on the Internet." name="description"/>
<meta content="3d screensaver, screensavers, download, wallpapers, animated wallpapers" name="keywords"/>
<meta content="en" http-equiv="content-language"/>
<meta content="no-cache" http-equiv="pragma"/>
<meta content="no-cache, no-store, must-revalidate" http-equiv="cache-control"/>
<meta content="0" http-equiv="expires"/>
<link href="https://cdn.3planesoft.com/css/main.css?11-01-2021" rel="stylesheet" type="text/css"/>
<link href="https://cdn.3planesoft.com/favicon.ico" rel="shortcut icon"/>
<!--<script src="https://browser.sentry-cdn.com/5.10.2/bundle.min.js" integrity="sha384-ssBfXiBvlVC7bdA/VX03S88B5MwXQWdnpJRbUYFPgswlOBwETwTp6F3SMUNpo9M9" crossorigin="anonymous"></script>-->
<script src="https://cdn.3planesoft.com/js/misc.js?v4" type="text/javascript"></script>
<!--[if lte IE 6]>
		<link rel="stylesheet" type="text/css" href="https://cdn.3planesoft.com/css/iefixes.css"/>
<![endif]-->
<meta content="Premium 3D Screensavers" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="https://www.3planesoft.com/" property="og:url"/>
<meta content="https://www.3planesoft.com/images/logo.jpg" property="og:image"/>
<meta content="3Planesoft" property="og:site_name"/>
<meta content="100000842594676" property="fb:admins"/>
<meta content="3Planesoft is an independent screensaver developing company specializing 3D screensavers. The company has released over 100 titles and is currently the most popular 3D screensaver maker on the Internet." property="og:description"/>
<!--[if lt IE 6]>
		<style type="text/css">
			#recommended-item-4 { margin-right: -9px; }
		</style>
<![endif]-->
<style type="text/css">
			body { background-image: url(https://cdn.3planesoft.com/media/backgrounds/back_wintercottage.jpg);
			background-color: white;



			}
		</style>
<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-1497590-1']);
			_gaq.push(['_setDomainName', '.3planesoft.com']);
			_gaq.push(['_setAllowLinker', true]);
			_gaq.push(['_setAllowHash', false]);
			_gaq.push(['_trackPageview']);
			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
<script type="text/javascript">
			var js_lang = "en";
		</script>
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '434705007484255');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=434705007484255&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
</head>
<body>
<div id="global">
<div id="cart-num-products">0</div>
<h1 id="slogan" style="color: #ffffff;">Best 3D screensavers and wallpapers</h1>
<div id="languages">
<a class="language language-active" id="language-en" title="English"></a>
<a class="language" href="//de.3planesoft.com/" id="language-de" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'lang', 'click/image', 'de']); }" title="Deutsch"></a>
<a class="language" href="//fr.3planesoft.com/" id="language-fr" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'lang', 'click/image', 'fr']); }" title="Français"></a>
<a class="language" href="//ru.3planesoft.com/" id="language-ru" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'lang', 'click/image', 'ru']); }" title="Русский"></a>
</div>
<div id="blocks-wrapper">
<div id="block-left">
<a href="/" id="logo" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'logo', 'click/image']); }"></a>
<div id="contentframe-left"></div>
<div id="mainframe-topleft"></div>
<ul class="mainframe-left" id="categories">
<li><a class="category" href="/all-screensavers/
" id="category-all" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'left-menu/category', 'click/item', 'all']); }"><span>All screensavers for Windows</span></a></li>
<li><a class="category" href="/nature-screensavers/winter-cottage-3d-screensaver/
" id="category-nature" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'left-menu/category', 'click/item', 'nature']); }"><span>Nature</span></a></li>
<li><a class="category" href="/adventure-screensavers/western-railway-3d-screensaver/
" id="category-adventure" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'left-menu/category', 'click/item', 'adventure']); }"><span>Adventure</span></a></li>
<li><a class="category" href="/holidays-screensavers/christmas-cottage-3d-screensaver/
" id="category-holidays" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'left-menu/category', 'click/item', 'holidays']); }"><span>Holidays</span></a></li>
<li><a class="category" href="/fish-screensavers/tropical-fish-3d-screensaver/
" id="category-fish" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'left-menu/category', 'click/item', 'fish']); }"><span>Fish</span></a></li>
<li><a class="category" href="/fantasy-screensavers/cuckoo-clock-3d-screensaver/
" id="category-fantasy" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'left-menu/category', 'click/item', 'fantasy']); }"><span>Fantasy</span></a></li>
<li><a class="category" href="/space-screensavers/earth-3d-screensaver/
" id="category-space" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'left-menu/category', 'click/item', 'space']); }"><span>Space</span></a></li>
<li><a class="category" href="/clock-screensavers/digital-clock-3d-screensaver/
" id="category-clock" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'left-menu/category', 'click/item', 'clock']); }"><span>Clock</span></a></li>
<li><a class="category" href="/fireplace-screensavers/fireplace-3d-screensaver/
" id="category-fireplace" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'left-menu/category', 'click/item', 'fireplace']); }"><span>Fireplace</span></a></li>
<li><a class="category" href="/mac/snow-village-3d/
" id="category-mac" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'left-menu/category', 'click/item', 'mac']); }"><span>MacOS</span></a></li>
<li><a class="category" href="/ios/fireplace-3d/
" id="category-ios" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'left-menu/category', 'click/item', 'ios']); }"><span>iPhone/iPad</span></a></li>
<li><a class="category" href="/android/sharks-3d-live-wallpaper/
" id="category-android" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'left-menu/category', 'click/item', 'android']); }"><span>Android</span></a></li>
<li><a class="category" href="/earth-3d-world-atlas/
" id="category-atlas" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'left-menu/category', 'click/item', 'earth-3d-world-atlas']); }"><span>Earth 3D - World Atlas</span></a></li>
<li><a class="category" href="/earth-3d-animal-atlas/
" id="category-animalatlas" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'left-menu/category', 'click/item', 'earth-3d-animal-atlas']); }"><span>Earth 3D - Animal Atlas</span></a></li>
</ul>
<div class="mainframe-left" id="left-blocks">
<div class="form-popup mfp-hide" id="newsletter-form">
<h1>3Planesoft newsletter</h1>
<form accept-charset="utf-8" action="/subscribe/" class="form-container" id="signup-form" method="POST" name="signup-form">
<input id="id_email" name="email" placeholder="Enter email" required="" type="text"/>
<script src="https://www.google.com/recaptcha/api.js?render=6LcxkrgZAAAAAFYL-tZh0HijmzE0cYzMmG24RO2B&amp;hl=en"></script>
<script type="text/javascript">
    grecaptcha.ready(function() {
        grecaptcha.execute('6LcxkrgZAAAAAFYL-tZh0HijmzE0cYzMmG24RO2B', {action: 'form'})
        .then(function(token) {
            console.log("reCAPTCHA validated for 'data-widget-uuid=\"02c7a4211838422faca21968e5e56354\"'. Setting input value...")
            var element = document.querySelector('.g-recaptcha[data-widget-uuid="02c7a4211838422faca21968e5e56354"]');
            element.value = token;
        });
    });
</script>
<input class="g-recaptcha" data-callback="onSubmit_02c7a4211838422faca21968e5e56354" data-sitekey="6LcxkrgZAAAAAFYL-tZh0HijmzE0cYzMmG24RO2B" data-size="normal" data-widget-uuid="02c7a4211838422faca21968e5e56354" id="id_captcha" name="captcha" required="" required_score="0.5" type="hidden"/>
<button class="btn" id="submit-btn" type="submit">Submit</button>
</form>
<div id="signup-loading">
<img id="loading-image" src=""/>
</div>
<div id="signup-status">
<p id="status"></p>
<button class="btn" id="status-back" type="submit">Back</button>
</div>
</div>
<div id="newsletter">
<a href="#" id="subscribe-mail" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'newsletter', 'click/link', 'subscribe']); }; return false;" title="Subscribe"></a>
<a href="#" id="subscribe-button" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'newsletter', 'click/link', 'subscribe']); }; return false;" title="Subscribe">Subscribe</a>
</div>
<div id="social-buttons">
<a href="https://www.facebook.com/3Planesoft" id="facebook" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'social', 'click/image', 'facebook']); }" rel="nofollow" target="_blank" title="Become a fan on Facebook"></a>
<a href="https://www.youtube.com/user/3Planesoftcom" id="youtube" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'social', 'click/image', 'youtube']); }" rel="nofollow" target="_blank" title="Check out our videos on YouTube"></a>
<a href="https://instagram.com/3Planesoft" id="instagram" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'social', 'click/image', 'instagram']); }" rel="nofollow" target="_blank" title="Follow us on Instagram"></a>
<a href="https://vk.com/3planesoft" id="vkontakte" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'social', 'click/image', 'vk']); }" rel="nofollow" target="_blank" title="Официальная группа Вконтакте"></a>
</div>
</div>
</div>
<div id="block-center">
<div id="content-wrapper">
<ul id="menu">
<li><a class="menu-item-active" id="menu-item-home"><span class="menu-item-span">Home</span></a></li>
<li><a class="menu-item" href="/packs/" id="menu-item-packs" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'top-menu/tab', 'click/item', 'packs']); }"><span class="menu-item-span">Packs</span></a></li>
<li><a class="menu-item" href="/cart/" id="menu-item-cart" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'top-menu/tab', 'click/item', 'cart']); }"><span class="menu-item-span"><span class="image-cart image-cart-en">Cart</span></span></a></li>
<li><a class="menu-item" href="/help/" id="menu-item-help" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'top-menu/tab', 'click/item', 'help']); }"><span class="menu-item-span">Help</span></a></li>
</ul>
<div id="contentframe-top">
<div class="menu-item-bottom" id="menu-item-home-bottom"></div>
</div>
<div id="content">
<script>
var spot_list = [

	["Winter Cottage", "o4tyH9WfTQI", "/nature-screensavers/winter-cottage-3d-screensaver/"],

	["Earth", "cFC-Q4erD9E", "/space-screensavers/earth-3d-screensaver/"],

	["Winter Walk", "gW7TovxUyOE", "/nature-screensavers/winter-walk-3d-screensaver/"],

	["Christmas Cottage", "p7r9R9WpApY", "/holidays-screensavers/christmas-cottage-3d-screensaver/"],

	["Fall Watermill", "xpqjKpJMqLI", "/nature-screensavers/fall-watermill-3d-screensaver/"],

];

function HideMoreInfo() {
	document.getElementById("button-moreinfo").style.opacity = 0;
}

function ShowMoreInfo() {
	document.getElementById("button-moreinfo").style.opacity = 1;
}

var currentVideoId = "o4tyH9WfTQI";
var lastPreviewSpotIndex = 0;
var lastPreviewSpot = null;
function SelectSpot(index)
{
	HideMoreInfo();

	var spot = document.getElementById("spot-promo");
	var spot_previews_container = document.getElementById("spot-previews-container");
	var element = document.getElementById("preview-image" + (index + 1));

	if (lastPreviewSpot == null)
		lastPreviewSpot = spot_previews_container.children[0];

	lastPreviewSpot.className = lastPreviewSpot.className.replace(" spot-preview-selected", "");
	element.parentElement.className += " spot-preview-selected";

	spot.children[1].href = spot_list[index][2];
	spot.children[1].onclick = function() { if (typeof _gaq !== 'undefined') { _gaq.push(['_set','hitCallback', function() { window.location.href = spot_list[index][2]; }]); _gaq.push(['_trackEvent', 'home/promo', 'click/more-info' , spot_list[index][0]]); return false; }};

	currentVideoId = spot_list[index][1];
	if(typeof player != 'undefined' && typeof player.playVideo == 'function') {
		player.loadVideoById(currentVideoId);
	};

	lastPreviewSpot = element.parentElement;
	lastPreviewSpotIndex = index;
}
</script>
<h2 class="section-caption">BESTSELLERS</h2>
<div class="spot-container">
<div id="spot-promo">
<div class="disable-events" id="spot-video"></div>
<a href="/nature-screensavers/winter-cottage-3d-screensaver/" id="button-moreinfo" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_set','hitCallback', function() { window.location.href = '/nature-screensavers/winter-cottage-3d-screensaver/'; }]); _gaq.push(['_trackEvent', 'home/promo', 'click/more-info' ,'Winter Cottage']); return false; }">More Info</a>
</div>
<div id="spot-previews-container">
<div class="spot-preview spot-preview-selected">
<i class="preview-uparrow-border" id="preview-uparrow-border1"></i>
<i class="preview-uparrow" id="preview-uparrow1"></i>
<img id="preview-image1" onclick="SelectSpot(0); if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/promo-preview-1', 'click/image' ,'Winter Cottage']); }" src="https://cdn.3planesoft.com/media/previews/wintercottage_16_9screen01video.jpg" title="Winter Cottage"/>
</div>
<div class="spot-preview">
<i class="preview-uparrow-border" id="preview-uparrow-border2"></i>
<i class="preview-uparrow" id="preview-uparrow2"></i>
<img id="preview-image2" onclick="SelectSpot(1); if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/promo-preview-2', 'click/image' ,'Earth']); }" src="https://cdn.3planesoft.com/media/previews/earth_16_9screen01video.jpg" title="Earth"/>
</div>
<div class="spot-preview">
<i class="preview-uparrow-border" id="preview-uparrow-border3"></i>
<i class="preview-uparrow" id="preview-uparrow3"></i>
<img id="preview-image3" onclick="SelectSpot(2); if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/promo-preview-3', 'click/image' ,'Winter Walk']); }" src="https://cdn.3planesoft.com/media/previews/winterwalk_16_9screen01video.jpg" title="Winter Walk"/>
</div>
<div class="spot-preview">
<i class="preview-uparrow-border" id="preview-uparrow-border4"></i>
<i class="preview-uparrow" id="preview-uparrow4"></i>
<img id="preview-image4" onclick="SelectSpot(3); if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/promo-preview-4', 'click/image' ,'Christmas Cottage']); }" src="https://cdn.3planesoft.com/media/previews/christmascottage_16_9screen01video.jpg" title="Christmas Cottage"/>
</div>
<div class="spot-preview">
<i class="preview-uparrow-border" id="preview-uparrow-border5"></i>
<i class="preview-uparrow" id="preview-uparrow5"></i>
<img id="preview-image5" onclick="SelectSpot(4); if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/promo-preview-5', 'click/image' ,'Fall Watermill']); }" src="https://cdn.3planesoft.com/media/previews/fallwatermill_16_9screen01video.jpg" title="Fall Watermill"/>
</div>
</div>
</div>
<script>
var player;
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

function InitYoutube(video_id) {
	var onPlayerReady = function(event) {
		if(currentVideoId != video_id)
			player.loadVideoById(currentVideoId);

		document.addEventListener("click", function(e) {
			if(e.target.id == "spot-promo") {
				if(player.getPlayerState() == 5) {
					player.playVideo();
				}
			}
		});
	};

	function onPlayerStateChange(event) {
		if (event.data == YT.PlayerState.PLAYING) {
			setTimeout(function() { ShowMoreInfo(); }, 1000);
			setTimeout(function() { HideMoreInfo(); }, player.getDuration() * 1000 - 2000);
		}
		if (event.data == YT.PlayerState.ENDED) {
			SelectSpot(lastPreviewSpotIndex == 4 ? 0 : lastPreviewSpotIndex + 1);
		}
	}

	player = new YT.Player('spot-video', {
		width: 573,
		height: 322,
		videoId: video_id,
		playerVars: {
			'autoplay': 1,
			'controls': 0,
			'disablekb': 1,
			'fs': 0,
			'loop': 1,
			'modestbranding': 1,
			'rel': 0,
			'showinfo': 0,
			'mute': 1,
			'autohide': 1,
			'playsinline': 1
		},
		events : {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange
		}
	});
}

window.onYouTubeIframeAPIReady = function() { InitYoutube('o4tyH9WfTQI'); };
</script>
<div id="recommended">
<h2 class="section-caption">RECOMMENDED</h2>
<div class="recommended-item" id="recommended-item-1">
<a href="/holidays-screensavers/snow-village-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/recommended-1', 'click/image', 'Snow Village']); }"><img alt="Snow Village" class="image-recommended" height="105" src="https://cdn.3planesoft.com/media/medium/snow_village.jpg" width="139"/></a>
<a class="recommended-link" href="/holidays-screensavers/snow-village-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/recommended-1', 'click/title', 'Snow Village']); }">Snow Village</a>
</div>
<div class="recommended-item" id="recommended-item-2">
<a href="/clock-screensavers/digital-clock-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/recommended-2', 'click/image', 'Digital Clock']); }"><img alt="Digital Clock" class="image-recommended" height="105" src="https://cdn.3planesoft.com/media/medium/digital_clock.jpg" width="139"/></a>
<a class="recommended-link" href="/clock-screensavers/digital-clock-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/recommended-2', 'click/title', 'Digital Clock']); }">Digital Clock</a>
</div>
<div class="recommended-item" id="recommended-item-3">
<a href="/holidays-screensavers/haunted-house-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/recommended-3', 'click/image', 'Haunted House']); }"><img alt="Haunted House" class="image-recommended" height="105" src="https://cdn.3planesoft.com/media/medium/haunted_house.jpg" width="139"/></a>
<a class="recommended-link" href="/holidays-screensavers/haunted-house-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/recommended-3', 'click/title', 'Haunted House']); }">Haunted House</a>
</div>
<div class="recommended-item" id="recommended-item-4">
<a href="/nature-screensavers/winter-garden-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/recommended-4', 'click/image', 'Winter Garden']); }"><img alt="Winter Garden" class="image-recommended" height="105" src="https://cdn.3planesoft.com/media/medium/winter_garden.jpg" width="139"/></a>
<a class="recommended-link" href="/nature-screensavers/winter-garden-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/recommended-4', 'click/title', 'Winter Garden']); }">Winter Garden</a>
</div>
</div>
<div class="clear"></div>
<div id="top10">
<div class="top10-item">
<a href="/adventure-screensavers/western-railway-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-1', 'click/image', 'Western Railway']); }"><img alt="Western Railway" class="image-thumb" height="63" src="https://cdn.3planesoft.com/media/small/western_railway.gif" width="84"/></a>
<div class="top10-item-content">
<div class="top10-item-link"><a href="/adventure-screensavers/western-railway-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-1', 'click/title', 'Western Railway']); }">WESTERN RAILWAY</a></div>
<div class="top10-item-download"><a href="/files/railway.exe" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-1', 'click/download', 'Western Railway']); }"></a></div>
<p>A 3D journey on a legendary American 4-4-0 train that'll take your breath away.</p>
</div>
</div>
<div class="top10-item" style="margin-right: -9px;">
<a href="/adventure-screensavers/futuristic-city-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-2', 'click/image', 'Futuristic City']); }"><img alt="Futuristic City" class="image-thumb" height="63" src="https://cdn.3planesoft.com/media/small/futuristic_city.gif" width="84"/></a>
<div class="top10-item-content">
<div class="top10-item-link"><a href="/adventure-screensavers/futuristic-city-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-2', 'click/title', 'Futuristic City']); }">FUTURISTIC CITY</a></div>
<div class="top10-item-download"><a href="/files/futuristiccity.exe" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-2', 'click/download', 'Futuristic City']); }"></a></div>
<p>Gaze into the future from your desk with this 22nd-century live wallpaper!</p>
</div>
</div>
<div class="top10-item">
<a href="/adventure-screensavers/battleship-missouri-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-3', 'click/image', 'Battleship Missouri']); }"><img alt="Battleship Missouri" class="image-thumb" height="63" src="https://cdn.3planesoft.com/media/small/battleship.gif" width="84"/></a>
<div class="top10-item-content">
<div class="top10-item-link"><a href="/adventure-screensavers/battleship-missouri-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-3', 'click/title', 'Battleship Missouri']); }">BATTLESHIP MISSOURI</a></div>
<div class="top10-item-download"><a href="/files/battleship.exe" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-3', 'click/download', 'Battleship Missouri']); }"></a></div>
<p>One of the greatest battleships in naval history now on your desktop.</p>
</div>
</div>
<div class="top10-item" style="margin-right: -9px;">
<a href="/nature-screensavers/fall-village-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-4', 'click/image', 'Fall Village']); }"><img alt="Fall Village" class="image-thumb" height="63" src="https://cdn.3planesoft.com/media/small/fall_village.gif" width="84"/></a>
<div class="top10-item-content">
<div class="top10-item-link"><a href="/nature-screensavers/fall-village-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-4', 'click/title', 'Fall Village']); }">FALL VILLAGE</a></div>
<div class="top10-item-download"><a href="/files/fallvillage.exe" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-4', 'click/download', 'Fall Village']); }"></a></div>
<p>A fall evening in a little village clad in a golden attire of fallen leaves.</p>
</div>
</div>
<div class="top10-item">
<a href="/nature-screensavers/summer-wonderland-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-5', 'click/image', 'Summer Wonderland']); }"><img alt="Summer Wonderland" class="image-thumb" height="63" src="https://cdn.3planesoft.com/media/small/summer_wonderland.gif" width="84"/></a>
<div class="top10-item-content">
<div class="top10-item-link"><a href="/nature-screensavers/summer-wonderland-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-5', 'click/title', 'Summer Wonderland']); }">SUMMER WONDERLAND</a></div>
<div class="top10-item-download"><a href="/files/summerwonderland.exe" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-5', 'click/download', 'Summer Wonderland']); }"></a></div>
<p>The shadowy meadow will protect everyone from the blazing summer sun</p>
</div>
</div>
<div class="top10-item" style="margin-right: -9px;">
<a href="/nature-screensavers/tyrannosaurus-rex-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-6', 'click/image', 'Tyrannosaurus Rex']); }"><img alt="Tyrannosaurus Rex" class="image-thumb" height="63" src="https://cdn.3planesoft.com/media/small/tyrannosaurus_rex.gif" width="84"/></a>
<div class="top10-item-content">
<div class="top10-item-link"><a href="/nature-screensavers/tyrannosaurus-rex-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-6', 'click/title', 'Tyrannosaurus Rex']); }">TYRANNOSAURUS REX</a></div>
<div class="top10-item-download"><a href="/files/tyrannosaurusrex.exe" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-6', 'click/download', 'Tyrannosaurus Rex']); }"></a></div>
<p>Follow the T-Rex to discover the infinite variety of prehistoric life.</p>
</div>
</div>
<div class="top10-item">
<a href="/nature-screensavers/spring-walk-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-7', 'click/image', 'Spring Walk']); }"><img alt="Spring Walk" class="image-thumb" height="63" src="https://cdn.3planesoft.com/media/small/spring_walk.gif" width="84"/></a>
<div class="top10-item-content">
<div class="top10-item-link"><a href="/nature-screensavers/spring-walk-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-7', 'click/title', 'Spring Walk']); }">SPRING WALK</a></div>
<div class="top10-item-download"><a href="/files/springwalk.exe" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-7', 'click/download', 'Spring Walk']); }"></a></div>
<p>The forest is awakening from its winter sleep and filling up with lovely birdsong.</p>
</div>
</div>
<div class="top10-item" style="margin-right: -9px;">
<a href="/fish-screensavers/tropical-fish-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-8', 'click/image', 'Tropical Fish']); }"><img alt="Tropical Fish" class="image-thumb" height="63" src="https://cdn.3planesoft.com/media/small/tropical_fish.gif" width="84"/></a>
<div class="top10-item-content">
<div class="top10-item-link"><a href="/fish-screensavers/tropical-fish-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-8', 'click/title', 'Tropical Fish']); }">TROPICAL FISH</a></div>
<div class="top10-item-download"><a href="/files/fish.exe" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-8', 'click/download', 'Tropical Fish']); }"></a></div>
<p>Tropical Fish 3D Screensaver is a unique virtual underwater world.</p>
</div>
</div>
<div class="top10-item">
<a href="/fireplace-screensavers/fireplace-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-9', 'click/image', 'Fireplace']); }"><img alt="Fireplace" class="image-thumb" height="63" src="https://cdn.3planesoft.com/media/small/fireplace.gif" width="84"/></a>
<div class="top10-item-content">
<div class="top10-item-link"><a href="/fireplace-screensavers/fireplace-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-9', 'click/title', 'Fireplace']); }">FIREPLACE</a></div>
<div class="top10-item-download"><a href="/files/fireplace.exe" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-9', 'click/download', 'Fireplace']); }"></a></div>
<p>Turn your PC into a fireplace with the help of the Fireplace screensaver.</p>
</div>
</div>
<div class="top10-item" style="margin-right: -9px;">
<a href="/clock-screensavers/mechanical-clock-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-10', 'click/image', 'Mechanical Clock']); }"><img alt="Mechanical Clock" class="image-thumb" height="63" src="https://cdn.3planesoft.com/media/small/mechanical_clock.gif" width="84"/></a>
<div class="top10-item-content">
<div class="top10-item-link"><a href="/clock-screensavers/mechanical-clock-3d-screensaver/" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-10', 'click/title', 'Mechanical Clock']); }">MECHANICAL CLOCK</a></div>
<div class="top10-item-download"><a href="/files/clock.exe" onclick="if (typeof _gaq !== 'undefined') { _gaq.push(['_trackEvent', 'home/top-10', 'click/download', 'Mechanical Clock']); }"></a></div>
<p>Fascinated by clock mechanisms? Get this cool Mechanical Clock 3D screensaver.</p>
</div>
</div>
</div>
</div>
<div class="clear"></div>
<div id="contentframe-bottom"></div>
</div>
</div>
<div id="block-right">
<div id="contentframe-right"></div>
<div id="mainframe-topright"></div>
<div id="mainframe-right"></div>
</div>
</div>
<div class="clear"></div>
<div id="footer">
<div id="links">
<a>Home</a>
	
	 |

	
			<a href="/packs/">Packs</a>
	
	 |

	
			<a href="/affiliates/">Affiliates</a>
	
	 |

	
			<a href="/cart/">Cart</a>
	
	 |

	
			<a href="/help/">Help</a>
</div>
<div id="footer-languages">
<a class="language language-active" id="footerlanguage-en" title="English"></a>
<a class="language" href="//de.3planesoft.com/?ref=678" id="footerlanguage-de" title="Deutsch"></a>
<a class="language" href="//fr.3planesoft.com/?ref=678" id="footerlanguage-fr" title="Français"></a>
<a class="language" href="//ru.3planesoft.com/?ref=678" id="footerlanguage-ru" title="Русский"></a>
</div>
<div id="copyright">
			© 2002-2021 3Planesoft | <span id="legal"><a href="/legal/">Legal</a></span>
</div>
</div>
</div>
<script src="https://cdn.3planesoft.com/js/video.js?v1"></script>
<style>
.mfp-img {
	height: 1000px !important;
}
</style>
<script language="JavaScript" src="https://cdn.3planesoft.com/js/cookies.js" type="text/javascript"></script>
<script language="JavaScript" src="https://cdn.3planesoft.com/js/precart.js?v3" type="text/javascript"></script>
</body>
</html>