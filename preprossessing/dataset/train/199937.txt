<html>
<head>
<title>Untitled Document</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
</head>
<body class="sub">
<table border="0" cellpadding="4" cellspacing="2" style="text-align: center" width="99%">
<tr>
<td class="StoryContentColor" rowspan="3" style="vertical-align: top" width="25%"><p><a href="index.php"><img border="0" height="135" src="gregscake.jpg" width="143"/></a></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">Our BBQChef33, Phil 
        Rizzardi, has been nominated to run for 2008 K.C.B.S Board of Directors. 
        The founder of The BBQ-Brethren, Webmaster, Pitmaster, and affectionately 
        known as the Grand Poobah of The Brethren.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2"><strong><em>Current 
        Endorsments</em></strong><em>: </em></font></p>
<p><em><font face="Arial, Helvetica, sans-serif" size="2"><strong>Bill Arnold 
        - Blues Hog Inc.</strong></font></em></p>
<p><em><font face="Arial, Helvetica, sans-serif" size="2"><strong>Ray Lampe 
        - Dr. BBQ </strong></font></em></p>
<p><font face="Arial, Helvetica, sans-serif" size="2"><em><strong>Dave Klose 
        - Klose BBQ Pits Houston TX</strong></em></font></p>
<p><em><font face="Arial, Helvetica, sans-serif" size="2"><strong>Jerry 
        Mullane KCBS Instructor</strong></font></em></p>
<p><em><strong><font face="Arial, Helvetica, sans-serif" size="2">Thom Emery 
        &amp; Neil Strawder California BBQ Association</font></strong></em></p>
<p><strong><font face="Arial, Helvetica, sans-serif"><em><font size="2">Jay 
        Curry - Spicewine IronWork</font></em><font size="2">s, </font></font></strong></p>
<p><strong><font face="Arial, Helvetica, sans-serif"><font size="2"><em>Arlie 
        Bragg - Arlie Que BBQ</em></font></font></strong></p>
<p> <font face="Arial, Helvetica, sans-serif" size="3"><a href="index.php">Go 
        to our Website</a></font> </p>
<p><font face="Arial, Helvetica, sans-serif" size="3"><a href="/forum">Go 
        to our Forum</a></font></p>
<p><strong><font face="Arial, Helvetica, sans-serif" size="2">The </font></strong><font face="Arial, Helvetica, sans-serif" size="2"><strong>BBQ-Brethren: 
        Our Charter</strong></font></p>
<p align="left"><font face="Arial, Helvetica, sans-serif" size="2">• To promote the art of low and slow 
        thru education, friendships, and camaraderie</font></p>
<p align="left"><font face="Arial, Helvetica, sans-serif" size="2">.• To assist our members to produce 
        the best BBQ they ever made, and thru this food, comes our ability to 
        educate the public, our friends, our family, to what is REAL BBQ.</font></p>
<p align="left"><font face="Arial, Helvetica, sans-serif" size="2"> • To build the best knowledge base 
        on the internet for BBQ. </font></p>
<p align="left"><font face="Arial, Helvetica, sans-serif" size="2"> • To maintain an open mind that there 
        is NO ONE WAY, and No perfect cooker. We live by the fact that it is the 
        cook not the cooker, its the passion not the steel, that produces great 
        BBQ. We support and respect all techniques and all pits. </font></p>
<p align="left"><font face="Arial, Helvetica, sans-serif" size="2"> • To encourage those that have even 
        the slightest interest or curiosity in Competition BBQ to get out there 
        and compete. Our new and our veteran competitors are here to help you 
        take that walk to the podium.</font></p>
<p align="left"><font face="Arial, Helvetica, sans-serif" size="2"> • Most important, To have FUN. To 
        enjoy our craft, to enjoy our friendships and to build on the passion 
        that lets us go to bed smelling like smoke and wake up with soot on our 
        pillows. </font><br/>
</p>
<p><a href="teams.htm"> </a><br/>
<br/>
</p></td>
<td height="1065" style="vertical-align: top" width="78%"><h2> <font color="#CC6600" face="Woodbadge" size="7">The 
        BBQ-Brethren</font></h2>
<hr/> <table border="0" cellpadding="4" cellspacing="2" height="280" width="100%">
<tr>
<td height="276" style="vertical-align: top" width="63%"><img height="266" src="KCBS%20BIO%20Photo.jpg" width="409"/>
</td>
<td width="37%"><div align="center">
<p><font face="Arial, Helvetica, sans-serif" size="2"><strong>Who 
                is Phil Rizzardi?</strong></font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2"><strong><font color="#333333">A 
                long time BBQ enthusiast, a Competitor, KCBS certified judge, 
                and founder of </font></strong></font></p>
<p><font color="#FF0000" face="Arial, Helvetica, sans-serif" size="4"><strong>The 
                BBQ Brethren</strong></font></p>
<p><strong><font color="#FF0000" face="Arial, Helvetica, sans-serif" size="3">***************************************</font></strong></p>
<p><font color="#333333"><strong><font face="Arial, Helvetica, sans-serif" size="2">"My 
                desire is to bring a strong background in corporate management, 
                technology, and love of BBQ to the KCBS Board of Directors, with 
                member involvment, support and regional representations as my 
                overriding goal."</font></strong></font><br/>
</p>
</div></td>
</tr>
</table>
<div align="left">
<hr align="left"/>
<font face="Arial, Helvetica, sans-serif">Besides being a KCBS certified 
        judge and competitor, I am the founder of the very successful online BBQ 
        community, “The BBQ-Brethren” and the pit master for our flagship 
        team. Like KCBS, The Brethren was established specifically to promote 
        our craft, and to bring together BBQ enthusiasts from around the country. 
        Our membership now exceeds 4,100 enthusiasts visiting from 58 countries. 
        Not only does this make me easily accessible to hear your voice, but also, 
        it is this devotion and experience that will enable me to see to it that 
        the ideas and concerns of KCBS cooks, judges, reps, and general membership 
        are addressed. 
        <p>Professionally, I am a Technology Manager for a major defense contractor 
          and have been with them for 28 years. My responsibilities have included 
          the management of a large staff and million dollar budgets. I'm formally 
          trained to work with people, initiate change and assist in long-range 
          growth of a corporation through team building. My management skills 
          are geared towards being a leader, not a boss, fostering a loyalty in 
          our employees that encourages them to work productively and efficiently. 
          My staff works with me, not for me, and we do things together as a team. 
          My technical responsibilities are to establish and implement processes 
          that enable our division to perform our functions effectively and efficiently 
          with minimal waste and overhead.</p>
<p> In my personal life, I’ve owned a small computer consulting 
          company since 1984. Additionally, I have served as President of my hometown 
          Civic Association since 1987. In that capacity, I have the responsibility 
          of meeting with elected town board members, developers, and representatives 
          of businesses, to ensure they hear and understand the concerns of our 
          residents. Our association has been responsible for many positive changes 
          that have allowed our community to grow, improve, and prosper. On behalf 
          of town residents, I have negotiated with the corporate offices of large 
          business chains, and their attorneys. We fought hard against negative 
          impacts and always supported the positive. A major accomplishment of 
          our association was to have our Town Board address, update, and rewrite 
          our community's outdated strategic plan, originally written in 1915. 
          It is this practical experience that I offer as evidence of the skills 
          I possess. I believe these are the assets required to assist in the 
          growth of KCBS and work with the members of the board.</p>
<p> As both a competitive cook and a judge in the Northeast, I am witnessing 
          the increased popularity of BBQ in this region. Like other areas of 
          the country, the art of “low and slow” is gaining momentum 
          and these areas need representation. My major short term goal is to 
          strengthen representation of the new regions where BBQ is starting to 
          boom. The Northeast is lacking in representation and recognition within 
          KCBS, as are other understated regions such as PNW and West coast. These 
          regions need representation as BBQ grows and flourishes. I would attempt 
          to organize some regional representation within KCBS for theses areas 
          by involving people from the many BBQ organizations such as NEBS, CBBQA, 
          Mid Atlantic BBQ Association, etc. I would also drive to have KCBS meet 
          directly with theses regions and ultimately strive to have KCBS become 
          more regionally aligned. </p>
<p> <font face="Arial, Helvetica, sans-serif">In the long term I plan 
          to utilize my professional abilities in Information Technology to assist 
          KCBS to run more efficiently and effectively.</font> </p>
</font>
<p><font face="Arial, Helvetica, sans-serif">The KCBS board needs experienced 
          business people running the business side of KCBS to help it grow and 
          flourish. However, KCBS also needs people who will keep the ideals and 
          goals of the BBQ enthusiast in mind. The membership needs someone to 
          listen to and bring their issues to the attention of the Board. As the 
          leader and founder of The BBQ Brethren, a large international BBQ Community 
          of 4000+ members in 58 countries, I have the channels already in place 
          to hear the voices and needs of many KCBS members and bring them directly 
          to the table. I have the open channel, the training, and the experience 
          to hear, understand, and act on your issues. If elected to assist in 
          the growth of KCBS and the growth of our craft, I will bring to the 
          table a solid background in corporate business management and technology, 
          a vision for the future, and a proven passion, and devotion to the art 
          of BBQ.</font></p>
<p align="center"><font face="Arial, Helvetica, sans-serif"><em><strong><font color="#FF0000" size="4"><br/>
<font size="3">I ask for your vote, thank you for your consideration 
          </font></font></strong></em></font><font face="Arial, Helvetica, sans-serif" size="3"><em><strong><font color="#FF0000">and 
          taking the time to read this. </font></strong></em></font></p>
<p align="center"><font color="#FF0000" size="4"><em><strong><font face="Arial, Helvetica, sans-serif">If 
          you have any questions, feel free to email me at </font></strong></em></font></p>
<p align="center"><font color="#FF0000" size="4"><em><strong><font face="Arial, Helvetica, sans-serif">bbqchef33@bbq-brethren.com</font>
</strong></em></font></p>
<p>  </p>
<p>***********************************************************************************</p>
<p><font color="#FF0000"><strong>Endorsment letter from Jerry Mullane. 
          </strong></font></p>
<p>-----Original Message-----<br/>
          From: KCBS Jersey &lt;kcbsjersey@aol.com&gt;<br/>
          To: kcbsjersey@aol.com<br/>
          Sent: Mon, 24 Dec 2007 10:55 am<br/>
          Subject: Happy Holidays - CBJ's</p>
<p><br/>
          This letter is going out to both our new CBJ’s and former students. 
          If you have completed a questionnaire prior, please do not fill out 
          another one.<br/>
          The questionnaire is at the bottom of this letter. Thank you and Happy 
          Holidays</p>
<p><br/>
          At our last CBJ Class I was asked by current members who I thought should 
          be elected to the Board of Directors in the upcoming elections. After 
          a lot of thinking and soul searching I have come to the following conclusions.</p>
<p>First let me say that the candidates I will vote for are strictly my 
          choices and no one has asked me to write these endorsements.</p>
<p>I have no agenda other than to endorse two genuine BBQ candidates who 
          I feel will have the most impact on our organization as a whole, as 
          well as having the pleasure of knowing both of them for many years in 
          my position as a Contest Rep, CBJ Instructor and in my personal life. 
          The two Gentlemen are as<br/>
          follows:</p>
<p><strong>Phil<br/>
          Rizzardi</strong><br/>
          Phil has been a long time friend since I first met him at a CBJ class 
          I was conducting in Philadelphia. He and his teammates had traveled 
          from Long Island, NY to become CBJ’s and learn more about what 
          we look for as judges during BBQ competitions. He is Founder and head 
          of the BBQ-Brethren, a 4100 member strong BBQ Group with firm beliefs 
          in promoting Competition BBQ throughout the North East and Mid Atlantic 
          regions as well as countrywide thru the many local and International 
          BBQ-Brethren groups. 58 Countries and growing.<br/>
          Phil is dedicated to our sport and has had a hand in many contests as 
          a Promoter, Organizer and cook. He and his group have made the difference 
          in making many of the East Coast contest grow from small, startup contest 
          to well over 50+ team contests with their support and participation. 
          He has volunteered to cook for our CBJ classes on a number of occasions 
          as well as mentored many backyard cooks, bring them into the Competition 
          BBQ world</p>
<p>Phil brings a strong diverse background in Information Technology as 
          well</p>
<p>Corporate management, which I feel is a must for KCBS in the future. 
          In order for us to move forward we need Board members like Phil who 
          have a background in dealing with the daily problems an organization 
          of our size produces as well as motivating us to resolve differences 
          and move on to a more productive tomorrow.<br/>
          Learn more about Phil at: http://www.bbq-brethren.com/kcbs.htm</p>
<p><strong>John<br/>
          Markus</strong><br/>
          I have known John for about 5 years. Originally from Ohio, now living 
          in New York State.<br/>
          We first met when he was filming a documentary about BBQ with Paul Kirk, 
          The Baron of BBQ, and we have remained friends ever since.<br/>
          John brings a unique talent to KCBS with his background in TV and films, 
          first as an Emmy Award-winning TV writer and Supervising producer for 
          the Bill Cosby Show winning an Emmy, a Peabody and two Humanities prizes, 
          and then co-created the Cosby spin-off "A Different World". 
        </p>
<p>The reason I have endorsed these 2 gentlemen besides knowing them personally 
          and agreeing with their views and ideals is we need more representation 
          on the East Coast and both Phil and John can provide that extra resource 
          we need in addition to Linda Mullane. The East Coast is the fastest 
          growing area of BBQ, but with the least representation. KCBS has approximately 
          275 contests with the East Coast hosting 54+ or roughly 20% of all the 
          KCBS contests for 2008. We need more members on the KCBS board who will 
          help lead KCBS and at the same time be aware of the unique position 
          the East Coast is in. More and more you see the Mid Atlantic and New 
          England teams scoring high in the prestigious contests and more prize 
          money is being offered on the East Coast each year. This is our time 
          and I believe the only way to insure our position is to increase our 
          representation on the KCBS Board. Keep in mind this is your organization 
          and you should have a say. </p>
<p><br/>
          This is your chance.<br/>
          Although you have 4 votes I am suggesting you use only 2. Phil Rizzardi 
          and John Markus. They can make a difference in East Coast BBQ and KCBS.</p>
<p>My final point about the 2 candidates above is they do not ask what’s 
          in it for “them” but what can they contribute to serve us 
          all. Their mantras, “It’s not me but we” and what 
          can I do today to make our organization better tomorrow?</p>
<p> As I stated in the beginning of this email, I was asked at a recent 
          CBJ class my opinion and this is it. I have 4 votes but will only cast 
          2 as not to equalize my voting power.<br/>
          This is strictly my decision.<br/>
          Vote with your head not your heart. Once again Thank you for your time 
          and I hope we see each other this coming season New CBJ Questionaire</p>
<p>Jerry Mullane (Jersey)<br/>
          KCBS CBJ Instructor</p>
<p>*************************************************************************************** 
        </p>
<p>Below is a copy of the KCBS questionaire with my answers published 
          in the December Bullsheet. </p>
<p><em>1. Please describe the skills you possess (which you believe are 
          stronger than the other candidates) which would make you an asset as 
          a board member, in dealing with the challenges facing KCBS, and give 
          an example of how those skills would serve the KCBS board of Directors 
          and its membership</em>.</p>
<p><strong>I have worked in the information technology field for 30 years. 
          As a technology process owner for a major defense contractor, my responsibilities 
          are to define and implement processes and technolgies that help our 
          organization run better and more efficient. </strong></p>
<p><strong>In addition I am the founder and leader of the BBQ Brethren, 
          a large 4000 member BBQ organization with a charter nearly identical 
          to KCBS. Our goal is to help the growth of BBQ across the country and 
          to encourage and mentor backyard enthusiasts, in many cases, bringing 
          them into the world of competition BBQ</strong>. </p>
<p><em>2. If you are a Cook, Judge, Rep or Backyard Cook, please identify 
          the major KCBS issues concerning, one of areas in which you are involved. 
          Describe the major issue, your strategies to correct or improve the 
          issue, and what you see as the biggest challenge to the success of your 
          plan. </em></p>
<p><strong>As both a cook and judge coming from the Northeast, we are 
          lacking in representation and recognition within KCBS, as is other understated 
          regions such as PNW and West coast. These regions need representation 
          as BBQ grows and flourishes. I would attempt to organize some regional 
          representation for theses areas by involving people from the many BBQ 
          organizations such as NEBS, CBBQA, Mid Atlantic BBQ Association, etc. 
          Also drive to have kcbs meet directly with theses regions and ultimately 
          have KCBS become more regionally aligne</strong>d. </p>
<p><em>3. Identify your major short term goal and the major long term 
          goal, if elected to the Board of KCBS and your plan to implement change 
          or improvement in order to carry out each of these goals. </em></p>
<p><strong>My major short term goal is to strengthen representation of 
          the new regions where BBQ is starting to boom. The northeast, the eastern 
          seaboard and the west coast. As the leader and founder of a large international 
          BBQ Community of 4000+ members I have the vehicle already in place to 
          hear the voices and needs of the KCBS members and bring them directly 
          to the table. As a long long term I plan to utilize my professional 
          abilities in Information Technology to assist KCBS to run more efficiently 
          and effectively.</strong> </p>
<p><strong>Another goal, which I would like to address is to research 
          ways for KCBS to take a more proactive role in our contests. Have our 
          sanctioning body set the bar for performance. Things along the lines 
          of giving organizers incentives for well run contests, team friendly 
          events, perks for high percentages of certified judges, etc. </strong></p>
<p><em>4 If elected please explain “you level of commitment, time 
          and energy” for committee projects and monthly reports, board 
          meetings and attendance, as well as representing KCBS to the public 
          and being responsive to our members. </em></p>
<p><strong>I already travel to many states for BBQ events of various types, 
          which will make it easy to add some trips for KCBS meetings. I can easily 
          attend board meetings via teleconference and take on some projects where 
          my technical/professional expertise or position in the BBQ world can 
          be best utilized. </strong></p>
<p><em>5. Please describe your participation in KCBS activities and years 
          of experience.</em></p>
<p><strong>I have been a cook for 25 years and a KCBS member, judge and 
          competitor since 2003. </strong></p>
<p><em>6. Please explain why you want to be a member of the KCBS Board. 
          </em></p>
<p> <strong>A huge part of my life, my family and my time are dedicated 
          to BBQ. Bringing the BBQ Brethren from a small group up to 4000 members 
          in 4 years shows the dedication and passion I have for our addiction. 
          Joining the KCSB board seems like a natural evolution in my roles to 
          further assist in the growth of BBQ. </strong><br/>
</p>
</div>
</td>
</tr>
<tr>
<td height="338" style="vertical-align: top"> <a href="http://www.tinycounter.com" target="_blank" title="hit counter"><img alt="hit counter" border="0" src="http://mycounter.tinycounter.com/index.php?user=rizzaph"/></a></td>
</tr>
<tr>
<td height="27" style="vertical-align: top"> </td>
</tr>
</table>
</body>
</html>
