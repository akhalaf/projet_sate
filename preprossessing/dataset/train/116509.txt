<!DOCTYPE html>
<html class="touch-styles" lang="en-US" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
<head>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- This is Squarespace. --><!-- thomas-murray-xbh8 -->
<base href=""/>
<meta charset="utf-8"/>
<title>Americans and Friends Living in Portugal</title>
<link href="https://assets.squarespace.com/universal/default-favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://www.americansinportugal.org" rel="canonical"/>
<meta content="Americans and Friends Living in Portugal" property="og:site_name"/>
<meta content="Americans and Friends Living in Portugal" property="og:title"/>
<meta content="https://www.americansinportugal.org" property="og:url"/>
<meta content="website" property="og:type"/>
<meta content="Americans and Friends Living in Portugal" itemprop="name"/>
<meta content="https://www.americansinportugal.org" itemprop="url"/>
<meta content="Americans and Friends Living in Portugal" name="twitter:title"/>
<meta content="https://www.americansinportugal.org" name="twitter:url"/>
<meta content="summary" name="twitter:card"/>
<meta content="" name="description"/>
<link href="https://images.squarespace-cdn.com" rel="preconnect"/>
<script src="//use.typekit.net/ik/vSi4NTBPUwH4HPBlE79Zek0MZGu2Rz512-4HoS1qYGqfe7w2fFHN4UJLFRbh52jhWD9o52btZRFRZcjajD93FeB8wQqUwDqDwy7CMPG0ieyzdABDOAuuS14ypPutjc6tO1FUiABkZWF3jAF8OcFzdPUydAmk-AFyd1FTd1gzS1suZhBCOcNkZkUCdhFydeyzSabCieyzdABDOAuuS14ypPutjc6tO1FUiABkZWF3jAF8OcFzdPUaiaS0jAu8Sc8RjAoD-Ao3da4XiAiydfozScSCiaiaO1sGdhuySkuliW4kjWqlpe4G5foDSWmyScmDSeBRZPoRdhXK2YgkdayTdAIldcNhjPJPjAszjc9lZhBkjAuzdcblSY4zH6qJ73IbMg6gJMJ7fbRKHyMMeMw6MKG4fHvgIMMjgfMfH6GJCwbgIMMjgPMfH6qJnbIbMg6eJMJ7fbKOMsMMeMS6MKG4fHqgIMMjffMfH6qJKbIbMg64JMJ7fbK0MsMMegM6MKG4fJ3gIMMjIPMfH6qJvDbbMs6IJMJ7fbRXFgMgeMj6MKG4fVMXIMIjgkMfH6qJvRbbMs65JMJ7fbRUFgMgegI6MTMgxSAzDM9.js" type="text/javascript"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script type="text/javascript">SQUARESPACE_ROLLUPS = {};</script>
<script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/cldr-resource-pack-d2b6094e965c882c2de89-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-cldr_resource_pack');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/cldr-resource-pack-d2b6094e965c882c2de89-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-vendors-2ec094db00b9e6d92d8fd-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common_vendors');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/common-vendors-2ec094db00b9e6d92d8fd-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-2c32d110777189549ff4d-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/common-2c32d110777189549ff4d-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/performance-e8f0bc9aa26cee507ecf9-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-performance');</script>
<script crossorigin="anonymous" defer="" src="//assets.squarespace.com/universal/scripts-compressed/performance-e8f0bc9aa26cee507ecf9-min.en-US.js"></script><script data-name="static-context">Static = window.Static || {}; Static.SQUARESPACE_CONTEXT = {"facebookAppId":"314192535267336","facebookApiVersion":"v6.0","rollups":{"squarespace-announcement-bar":{"js":"//assets.squarespace.com/universal/scripts-compressed/announcement-bar-8b244fce99594deac3684-min.en-US.js"},"squarespace-audio-player":{"css":"//assets.squarespace.com/universal/styles-compressed/audio-player-03a5305221e9f3857f5d3fbff2cd9bbe-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/audio-player-9d33505677455a9b22add-min.en-US.js"},"squarespace-blog-collection-list":{"css":"//assets.squarespace.com/universal/styles-compressed/blog-collection-list-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/blog-collection-list-ab4142fcacca918cf4e2d-min.en-US.js"},"squarespace-calendar-block-renderer":{"css":"//assets.squarespace.com/universal/styles-compressed/calendar-block-renderer-1e8a762808391e4b0bd8945da50793ac-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/calendar-block-renderer-82b361c64e6e75913711e-min.en-US.js"},"squarespace-chartjs-helpers":{"css":"//assets.squarespace.com/universal/styles-compressed/chartjs-helpers-9935a41d63cf08ca108505d288c1712e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/chartjs-helpers-270e1573dd28dff07fc7c-min.en-US.js"},"squarespace-comments":{"css":"//assets.squarespace.com/universal/styles-compressed/comments-f794dccd3bb871fc0cbc0bb7ad024168-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/comments-1b8d1adb275f098d8dc6d-min.en-US.js"},"squarespace-commerce-cart":{"js":"//assets.squarespace.com/universal/scripts-compressed/commerce-cart-9fd3c3147f23827502474-min.en-US.js"},"squarespace-dialog":{"css":"//assets.squarespace.com/universal/styles-compressed/dialog-4c984bcaacc45888f9092057493234b6-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/dialog-614b07c3f84e1e3b30662-min.en-US.js"},"squarespace-events-collection":{"css":"//assets.squarespace.com/universal/styles-compressed/events-collection-1e8a762808391e4b0bd8945da50793ac-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/events-collection-415312b37ef6978cf711b-min.en-US.js"},"squarespace-form-rendering-utils":{"js":"//assets.squarespace.com/universal/scripts-compressed/form-rendering-utils-da71321e53a08371a214c-min.en-US.js"},"squarespace-forms":{"css":"//assets.squarespace.com/universal/styles-compressed/forms-763d974ea7719bb18959e8f0a891abe6-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/forms-9fe4eb33891804b4464fe-min.en-US.js"},"squarespace-gallery-collection-list":{"css":"//assets.squarespace.com/universal/styles-compressed/gallery-collection-list-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/gallery-collection-list-5d5ff461e8bba64f298dc-min.en-US.js"},"squarespace-image-zoom":{"css":"//assets.squarespace.com/universal/styles-compressed/image-zoom-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/image-zoom-f7178bbfa97ac5234c120-min.en-US.js"},"squarespace-pinterest":{"css":"//assets.squarespace.com/universal/styles-compressed/pinterest-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/pinterest-9dd1acd10aa47a7154983-min.en-US.js"},"squarespace-popup-overlay":{"css":"//assets.squarespace.com/universal/styles-compressed/popup-overlay-68d60e7bd84500af34df575998cc00d0-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/popup-overlay-0149a748bc121034a13df-min.en-US.js"},"squarespace-product-quick-view":{"css":"//assets.squarespace.com/universal/styles-compressed/product-quick-view-eedd090fe95cd960919fcf1e0a4293c3-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/product-quick-view-ce2b51182ccd8ac6accc6-min.en-US.js"},"squarespace-products-collection-item-v2":{"css":"//assets.squarespace.com/universal/styles-compressed/products-collection-item-v2-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/products-collection-item-v2-5426bac76cb8bd5a92e8b-min.en-US.js"},"squarespace-products-collection-list-v2":{"css":"//assets.squarespace.com/universal/styles-compressed/products-collection-list-v2-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/products-collection-list-v2-0f57d23347251b2736f90-min.en-US.js"},"squarespace-search-page":{"css":"//assets.squarespace.com/universal/styles-compressed/search-page-207da8872118254c0a795bf9b187c205-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/search-page-ac207ca39a69cb84f84f0-min.en-US.js"},"squarespace-search-preview":{"js":"//assets.squarespace.com/universal/scripts-compressed/search-preview-638ba2bf8ec524b820947-min.en-US.js"},"squarespace-share-buttons":{"js":"//assets.squarespace.com/universal/scripts-compressed/share-buttons-32cc08f2dd7137611cfc4-min.en-US.js"},"squarespace-simple-liking":{"css":"//assets.squarespace.com/universal/styles-compressed/simple-liking-9ef41bf7ba753d65ec1acf18e093b88a-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/simple-liking-146a4d691830282d9ce5a-min.en-US.js"},"squarespace-social-buttons":{"css":"//assets.squarespace.com/universal/styles-compressed/social-buttons-bf7788a87c794b73afd9d5c49f72f4f3-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/social-buttons-aa06a0ebdaa97ed82d7ae-min.en-US.js"},"squarespace-tourdates":{"css":"//assets.squarespace.com/universal/styles-compressed/tourdates-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/tourdates-ee4869629f6a684b35f94-min.en-US.js"},"squarespace-website-overlays-manager":{"css":"//assets.squarespace.com/universal/styles-compressed/website-overlays-manager-4f212ab97f9bc590002bb2ff55f69409-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/website-overlays-manager-9e5a6a309dfd7e877bf6f-min.en-US.js"}},"pageType":2,"website":{"id":"59848f3cff7c504ae1913932","identifier":"thomas-murray-xbh8","websiteType":1,"contentModifiedOn":1609419471818,"cloneable":false,"hasBeenCloneable":false,"siteStatus":{},"language":"en-US","timeZone":"America/New_York","machineTimeZoneOffset":-18000000,"timeZoneOffset":-18000000,"timeZoneAbbr":"EST","siteTitle":"Americans and Friends Living in Portugal","fullSiteTitle":"Americans and Friends Living in Portugal","siteDescription":"<p>Americans in Portugal is a volunteer organization to serve Americans and friends living in Portugal. Primary activities are to offer fun social activities to meet new friends, including events and talks aimed at topics that the group would find interesting. Besides a focus on social and educational activities, another focus is opportunities and help for improving business and investing in Portugal.</p>","location":{"addressTitle":"Americans in Portugal","addressLine1":"Rua do Algarve 4","addressLine2":"Estoril","addressCountry":"Portugal"},"shareButtonOptions":{},"authenticUrl":"https://www.americansinportugal.org","internalUrl":"https://thomas-murray-xbh8.squarespace.com","baseUrl":"https://www.americansinportugal.org","primaryDomain":"www.americansinportugal.org","sslSetting":3,"isHstsEnabled":false,"typekitId":"","statsMigrated":false,"imageMetadataProcessingEnabled":false,"screenshotId":"876210852c9020c44ff65037dfcdd20ad1059db3e1ca8918bc17c62e446bcd46","showOwnerLogin":false},"websiteSettings":{"id":"59848f3cff7c504ae1913935","websiteId":"59848f3cff7c504ae1913932","subjects":[],"country":"US","simpleLikingEnabled":true,"mobileInfoBarSettings":{"isContactEmailEnabled":false,"isContactPhoneNumberEnabled":false,"isLocationEnabled":false,"isBusinessHoursEnabled":false},"commentLikesAllowed":true,"commentAnonAllowed":true,"commentThreaded":true,"commentApprovalRequired":false,"commentAvatarsOn":true,"commentSortType":2,"commentFlagThreshold":0,"commentFlagsAllowed":true,"commentEnableByDefault":true,"commentDisableAfterDaysDefault":0,"disqusShortname":"","commentsEnabled":false,"contactPhoneNumber":"","businessHours":{"monday":{"text":"","ranges":[{}]},"tuesday":{"text":"","ranges":[{}]},"wednesday":{"text":"","ranges":[{}]},"thursday":{"text":"","ranges":[{}]},"friday":{"text":"","ranges":[{}]},"saturday":{"text":"","ranges":[{}]},"sunday":{"text":"","ranges":[{}]}},"storeSettings":{"returnPolicy":null,"termsOfService":null,"privacyPolicy":null,"expressCheckout":false,"continueShoppingLinkUrl":"/","useLightCart":false,"showNoteField":false,"shippingCountryDefaultValue":"US","billToShippingDefaultValue":false,"showShippingPhoneNumber":true,"isShippingPhoneRequired":false,"showBillingPhoneNumber":true,"isBillingPhoneRequired":false,"currenciesSupported":["CHF","HKD","MXN","EUR","DKK","USD","CAD","MYR","NOK","THB","AUD","SGD","ILS","PLN","GBP","CZK","SEK","NZD","PHP","RUB"],"defaultCurrency":"USD","selectedCurrency":"USD","measurementStandard":1,"showCustomCheckoutForm":false,"enableMailingListOptInByDefault":false,"sameAsRetailLocation":false,"merchandisingSettings":{"scarcityEnabledOnProductItems":false,"scarcityEnabledOnProductBlocks":false,"scarcityMessageType":"DEFAULT_SCARCITY_MESSAGE","scarcityThreshold":10,"multipleQuantityAllowedForServices":true,"restockNotificationsEnabled":false,"restockNotificationsMailingListSignUpEnabled":false,"relatedProductsEnabled":false,"relatedProductsOrdering":"random","soldOutVariantsDropdownDisabled":false,"productComposerOptedIn":false,"productComposerABTestOptedOut":false},"isLive":false,"multipleQuantityAllowedForServices":true},"useEscapeKeyToLogin":true,"trialAssistantEnabled":true,"ssBadgeType":1,"ssBadgePosition":4,"ssBadgeVisibility":1,"ssBadgeDevices":1,"pinterestOverlayOptions":{"mode":"disabled"},"ampEnabled":false},"cookieSettings":{"isCookieBannerEnabled":false,"isRestrictiveCookiePolicyEnabled":false,"isRestrictiveCookiePolicyAbsolute":false,"cookieBannerText":"","cookieBannerTheme":"","cookieBannerVariant":"","cookieBannerPosition":"","cookieBannerCtaVariant":"","cookieBannerCtaText":""},"websiteCloneable":false,"collection":{"title":"Home","id":"5985a16686e6c09a29e7e4f0","fullUrl":"/","type":10,"permissionType":1},"subscribed":false,"appDomain":"squarespace.com","templateTweakable":true,"tweakJSON":{"aspect-ratio":"Auto","banner-slideshow-controls":"Arrows","gallery-arrow-style":"No Background","gallery-aspect-ratio":"3:2 Standard","gallery-auto-crop":"true","gallery-autoplay":"false","gallery-design":"Grid","gallery-info-overlay":"Show on Hover","gallery-loop":"false","gallery-navigation":"Bullets","gallery-show-arrows":"true","gallery-transitions":"Fade","galleryArrowBackground":"rgba(34,34,34,1)","galleryArrowColor":"rgba(255,255,255,1)","galleryAutoplaySpeed":"3","galleryCircleColor":"rgba(255,255,255,1)","galleryInfoBackground":"rgba(0, 0, 0, .7)","galleryThumbnailSize":"100px","gridSize":"280px","gridSpacing":"10px","logoContainerWidth":"140px","product-gallery-auto-crop":"false","product-image-auto-crop":"true","siteTitleContainerWidth":"220px","tweak-v1-related-products-title-spacing":"50px"},"templateId":"52a74dafe4b073a80cd253c5","templateVersion":"7","pageFeatures":[1,2,4],"gmRenderKey":"QUl6YVN5Q0JUUk9xNkx1dkZfSUUxcjQ2LVQ0QWVUU1YtMGQ3bXk4","templateScriptsRootUrl":"https://static1.squarespace.com/static/ta/52a74d9ae4b0253945d2aee9/1039/scripts/","betaFeatureFlags":["seven_one_image_overlay_opacity","ORDERS-SERVICE-check-digital-good-access-with-service","product_composer_feedback_form_on_save","page_interactions_improvements","commerce_afterpay","nested_categories_migration_enabled","commerce_setup_wizard","campaigns_email_reuse_template_flow","list_sent_to_groups","omit_tweakengine_tweakvalues","domains_universal_search","commerce_subscription_order_delay","campaigns_single_opt_in","ORDERS-SERVICE-reset-digital-goods-access-with-service","seven_one_image_effects","commerce_afterpay_toggle","commerce_pdp_edit_mode","domain_info_via_registrar_service","crm_campaigns_sending","seven_one_portfolio_hover_layouts","campaigns_new_sender_profile_page","ORDER_SERVICE-submit-subscription-order-through-service","customer_notifications_panel_v2","commerce_activation_experiment_add_payment_processor_card","generic_iframe_loader_for_campaigns","domains_use_new_domain_connect_strategy","commerce_add_to_cart_rate_limiting","domains_transfer_flow_improvements","seven-one-main-content-preview-api","commerce_instagram_product_checkout_links","seven_one_frontend_render_gallery_section","gallery_captions_71","commerce_restock_notifications","seven-one-content-preview-section-api","domain_locking_via_registrar_service","commerce_minimum_order_amount","donations_customer_accounts","commerce_reduce_cart_calculations","commerce-recaptcha-enterprise","commerce_orders_elasticsearch_migration","dg_downloads_from_fastly","events_panel_70","seven-one-menu-overlay-theme-switcher","seven_one_frontend_render_page_section","animations_august_2020_new_preset","domains_transfer_flow_hide_preface","commerce_tax_panel_v2","commerce_category_id_discounts_enabled","domains_allow_async_gsuite","ORDER_SERVICE-submit-reoccurring-subscription-order-through-service","member_areas_ga","campaigns_user_templates_in_sidebar","domain_deletion_via_registrar_service","newsletter_block_captcha","local_listings","seven_one_header_editor_update"],"yuiEliminationExperimentList":[{"name":"statsMigrationJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"ContributionConfirmed-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"TextPusher-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"MenuItemWithProgress-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"imageProcJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"QuantityChangePreview-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"CompositeModel-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"HasPusherMixin-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"INACTIVE"},{"name":"ProviderList-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"MediaTracker-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"pushJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"internal-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"BillingPanel-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"PopupOverlayEditor-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"CoverPagePicker-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"}],"impersonatedSession":false,"demoCollections":[{"collectionId":"52c71337e4b07aa29e46fa44","deleted":false}],"tzData":{"zones":[[-300,"US","E%sT",null]],"rules":{"US":[[1967,2006,null,"Oct","lastSun","2:00","0","S"],[1987,2006,null,"Apr","Sun>=1","2:00","1:00","D"],[2007,"max",null,"Mar","Sun>=8","2:00","1:00","D"],[2007,"max",null,"Nov","Sun>=1","2:00","0","S"]]}}};</script><script type="text/javascript"> SquarespaceFonts.loadViaContext(); Squarespace.load(window);</script><script type="application/ld+json">{"url":"https://www.americansinportugal.org","name":"Americans and Friends Living in Portugal","description":"<p>Americans in Portugal is a volunteer organization to serve Americans and friends living in Portugal. Primary activities are to offer fun social activities to meet new friends, including events and talks aimed at topics that the group would find interesting. Besides a focus on social and educational activities, another focus is opportunities and help for improving business and investing in Portugal.</p>","@context":"http://schema.org","@type":"WebSite"}</script><script type="application/ld+json">{"openingHours":", , , , , , ","@context":"http://schema.org","@type":"LocalBusiness"}</script><link href="//static1.squarespace.com/static/sitecss/59848f3cff7c504ae1913932/11/52a74dafe4b073a80cd253c5/59859df96a4963b1c4e36d32/1039-05142015/1590231198350/site.css?&amp;filterFeatures=false" rel="stylesheet" type="text/css"/><script>Static.COOKIE_BANNER_CAPABLE = true;</script>
<!-- End of Squarespace Headers -->
<script>/* Must be below squarespace-headers */(function(){var e='ontouchstart'in window||navigator.msMaxTouchPoints;var t=document.documentElement;if(!e&&t){t.className=t.className.replace(/touch-styles/,'')}})()
    </script>
</head>
<body class="transparent-header enable-nav-button nav-button-style-outline nav-button-corner-style-pill banner-button-style-solid banner-button-corner-style-pill banner-slideshow-controls-arrows meta-priority-date hide-entry-author hide-list-entry-footer hide-blog-sidebar center-navigation--info event-show-past-events event-thumbnails event-thumbnail-size-32-standard event-date-label event-list-show-cats event-list-date event-list-time event-list-address event-icalgcal-links event-excerpts gallery-design-grid aspect-ratio-auto lightbox-style-light gallery-navigation-bullets gallery-info-overlay-show-on-hover gallery-aspect-ratio-32-standard gallery-arrow-style-no-background gallery-transitions-fade gallery-show-arrows gallery-auto-crop product-list-titles-under product-list-alignment-center product-item-size-11-square product-image-auto-crop product-gallery-size-11-square show-product-price show-product-item-nav product-social-sharing tweak-v1-related-products-image-aspect-ratio-11-square tweak-v1-related-products-details-alignment-center newsletter-style-light hide-opentable-icons opentable-style-dark small-button-style-solid small-button-shape-square medium-button-style-solid medium-button-shape-square large-button-style-solid large-button-shape-square image-block-poster-text-alignment-center image-block-card-dynamic-font-sizing image-block-card-content-position-center image-block-card-text-alignment-left image-block-overlap-dynamic-font-sizing image-block-overlap-content-position-center image-block-overlap-text-alignment-left image-block-collage-dynamic-font-sizing image-block-collage-content-position-top image-block-collage-text-alignment-left image-block-stack-dynamic-font-sizing image-block-stack-text-alignment-left button-style-solid button-corner-style-square tweak-product-quick-view-button-style-floating tweak-product-quick-view-button-position-bottom tweak-product-quick-view-lightbox-excerpt-display-truncate tweak-product-quick-view-lightbox-show-arrows tweak-product-quick-view-lightbox-show-close-button tweak-product-quick-view-lightbox-controls-weight-light native-currency-code-usd collection-type-page collection-layout-default collection-5985a16686e6c09a29e7e4f0 homepage mobile-style-available has-banner-image general-page" id="collection-5985a16686e6c09a29e7e4f0">
<a class="body-overlay" href="#"></a>
<div class="sqs-announcement-bar-dropzone"></div>
<div id="sidecarNav">
<div class="nav-wrapper" data-content-field="navigation-mobileNav" id="mobileNavWrapper">
<nav id="mobileNavigation">
<div class="collection active homepage">
<a href="/">
                Home
              </a>
</div>
<div class="folder">
<div class="folder-toggle" data-href="/about">About</div>
<div class="subnav">
<div class="collection">
<a href="/what-we-do">
                  About
                </a>
</div>
<div class="collection">
<a href="/new-page">
                  The Board
                </a>
</div>
</div>
</div>
<div class="collection">
<a href="/residency-information">
                Applying for/Renewing Residency and Portuguese Citizenship
              </a>
</div>
<div class="collection">
<a href="/healthcare">
                Healthcare
              </a>
</div>
<div class="collection">
<a href="/resources">
                General FAQ's
              </a>
</div>
<div class="collection">
<a href="/professional-services">
                Professional Services
              </a>
</div>
<div class="folder">
<div class="folder-toggle" data-href="/events-1">Events</div>
<div class="subnav">
<div class="collection">
<a href="/future-events">
                  Future Events
                </a>
</div>
<div class="collection">
<a href="/past-events-photo-gallery">
                  Past Events Photo Gallery
                </a>
</div>
</div>
</div>
<div class="collection">
<a href="/some-travel-ideas-note-most-hotels-cost-between-45-and-55-euros">
                Some Travel Ideas
              </a>
</div>
<div class="collection">
<a href="/latest-news-of-portugal">
                Latest News of Portugal
              </a>
</div>
<div class="collection">
<a href="/new-page-1">
                National Budget Differences
              </a>
</div>
<div class="collection">
<a href="/read-me">
                Contact Us
              </a>
</div>
</nav>
</div>
</div>
<div class="clearfix" id="siteWrapper">
<div class="sqs-cart-dropzone"></div>
<header class="show-on-scroll" data-offset-behavior="bottom" data-offset-el=".index-section" id="header" role="banner">
<div class="header-inner">
<div class="wrapper" data-content-field="site-title" id="siteTitleWrapper">
<h1 class="site-title" id="siteTitle"><a href="/">Americans and Friends Living in Portugal</a></h1>
</div><!--
--><div class="mobile-nav-toggle"><div class="top-bar"></div><div class="middle-bar"></div><div class="bottom-bar"></div></div><div class="mobile-nav-toggle fixed-nav-toggle"><div class="top-bar"></div><div class="middle-bar"></div><div class="bottom-bar"></div></div><!--
--><div id="headerNav">
<div class="nav-wrapper" data-content-field="navigation-mainNav" id="mainNavWrapper">
<nav data-content-field="navigation-mainNav" id="mainNavigation">
<div class="collection active homepage">
<a href="/">
                Home
              </a>
</div>
<div class="folder">
<div class="folder-toggle" data-href="/about">About</div>
<div class="subnav">
<div class="collection">
<a href="/what-we-do">
                  About
                </a>
</div>
<div class="collection">
<a href="/new-page">
                  The Board
                </a>
</div>
</div>
</div>
<div class="collection">
<a href="/residency-information">
                Applying for/Renewing Residency and Portuguese Citizenship
              </a>
</div>
<div class="collection">
<a href="/healthcare">
                Healthcare
              </a>
</div>
<div class="collection">
<a href="/resources">
                General FAQ's
              </a>
</div>
<div class="collection">
<a href="/professional-services">
                Professional Services
              </a>
</div>
<div class="folder">
<div class="folder-toggle" data-href="/events-1">Events</div>
<div class="subnav">
<div class="collection">
<a href="/future-events">
                  Future Events
                </a>
</div>
<div class="collection">
<a href="/past-events-photo-gallery">
                  Past Events Photo Gallery
                </a>
</div>
</div>
</div>
<div class="collection">
<a href="/some-travel-ideas-note-most-hotels-cost-between-45-and-55-euros">
                Some Travel Ideas
              </a>
</div>
<div class="collection">
<a href="/latest-news-of-portugal">
                Latest News of Portugal
              </a>
</div>
<div class="collection">
<a href="/new-page-1">
                National Budget Differences
              </a>
</div>
<div class="collection">
<a href="/read-me">
                Contact Us
              </a>
</div>
</nav>
</div>
<!-- style below blocks out the mobile nav toggle only when nav is loaded -->
<style>.mobile-nav-toggle-label { display: inline-block !important; }</style>
</div>
</div>
</header>
<div class="sqs-layout promoted-gallery-wrapper" id="promotedGalleryWrapper"><div class="row"><div class="col"></div></div></div>
<div class="banner-thumbnail-wrapper" data-annotation-alignment="bottom left" data-collection-id="5985a16686e6c09a29e7e4f0" data-content-field="main-image" data-edit-main-image="">
<div class="color-overlay"></div>
<figure class="loading content-fill" id="thumbnail">
<img alt="puzzle photo.jpg" data-image="https://images.squarespace-cdn.com/content/v1/59848f3cff7c504ae1913932/1504598469936-FE37184X06IOA9UZ9U3H/ke17ZwdGBToddI8pDm48kHb-xihk4Lp9l7a03Qiyrn8UqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKc9NUrbN6QqJePPA2PASsmlXfP1nvv2WCdNHw9jBMJunoqTK2AY4y6L1BHIbWxoker/puzzle+photo.jpg" data-image-dimensions="1170x351" data-image-focal-point="0.5,0.5" data-src="https://images.squarespace-cdn.com/content/v1/59848f3cff7c504ae1913932/1504598469936-FE37184X06IOA9UZ9U3H/ke17ZwdGBToddI8pDm48kHb-xihk4Lp9l7a03Qiyrn8UqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKc9NUrbN6QqJePPA2PASsmlXfP1nvv2WCdNHw9jBMJunoqTK2AY4y6L1BHIbWxoker/puzzle+photo.jpg"/>
</figure>
</div>
<main id="page" role="main">
<!--
--><!--
--><!--
--><div class="main-content" data-collection-id="5985a16686e6c09a29e7e4f0" data-content-field="main-content" data-edit-main-image="" id="content">
<div class="sqs-layout sqs-grid-12 columns-12" data-type="page" data-updated-on="1535479990587" id="page-5985a16686e6c09a29e7e4f0"><div class="row sqs-row"><div class="col sqs-col-12 span-12"><div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-20d2d7d07ef691626788"><div class="sqs-block-content"><h2><strong>Americans in Portugal</strong></h2></div></div><div class="row sqs-row"><div class="col sqs-col-2 span-2"><div class="sqs-block spacer-block sqs-block-spacer sized vsize-1" data-block-type="21" id="block-3f0aecaba7151fa7367c"><div class="sqs-block-content"> </div></div></div><div class="col sqs-col-8 span-8"><div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-9b68428d3dc4235f06c2"><div class="sqs-block-content"><p>The Americans in Portugal club promotes social and cultural events, lectures, local commemorations as well as celebrations of historic American dates. Dinners are scheduled to motivate social interaction between all members, bringing everyone together and promoting common objectives. Though much of our experise regards citizens of the USA, we feel that anyone from the Americas is an American and are welcome. All who have a connection and an affinity to Americans are also welcome. Anyone curious about us are welcome, too.</p><p>Our club is based on the spirit of a community which embraces all Americans that come to live and work in Portugal and the many friends of any nationality which share American values and our way of life. Community spirit, collaboration, embracing all faiths and races, transparency and democracy, these fundamental ethics are the objectives of the club. </p><p>Many of our members are Portuguese and this is very positive as so many Americans have Portuguese roots, resident Portuguese have families in America and Portugal has been a long-time friend of the USA.</p><p>In line with tradition, we continue to maintain and offer a general orientation to new members coming to live in Portugal.  This is made possible by our diverse list of contacts, created over the years, which will indicate services rendered by third parties across a wide spectrum of activities.</p><p>The club through its website and email list will keep all members advised of important happenings and important dates.  Please contact us with a request to be on the email list if you are not already on it. We welcome your ideas, suggestions and posts. Together we hope to build an even stronger community, dedicated to honoring the progressive and humanistic values we all believe in.</p><p>We look forward to your membership and participation.</p><p> </p></div></div></div><div class="col sqs-col-2 span-2"><div class="sqs-block spacer-block sqs-block-spacer sized vsize-1" data-block-type="21" id="block-ee21fe402d941b112f04"><div class="sqs-block-content"> </div></div></div></div><div class="row sqs-row"><div class="col sqs-col-6 span-6"><div class="sqs-block image-block sqs-block-image" data-aspect-ratio="83.08026030368764" data-block-type="5" id="block-5ca195a7a93b88f4c7cc"><div class="sqs-block-content">
<div class=" image-block-outer-wrapper layout-caption-below design-layout-inline combination-animation-none individual-animation-none individual-text-animation-none " data-test="image-block-inline-outer-wrapper">
<figure class=" sqs-block-image-figure intrinsic " style="max-width:750.0px;">
<a class=" sqs-block-image-link " href="/what-we-do-bedford">
<div class=" image-block-wrapper has-aspect-ratio " data-animation-role="image" style="padding-bottom:83.08026123046875%;">
<noscript><img alt="" src="https://images.squarespace-cdn.com/content/v1/59848f3cff7c504ae1913932/1503134163563-L5WM70BN1K97DNTIPITT/ke17ZwdGBToddI8pDm48kBxX7QcyDclQBhYCHwbxl9YUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8PaoYXhp6HxIwZIk7-Mi3Tsic-L2IOPH3Dwrhl-Ne3Z2cYSQx44Ke2W_Sil3mM9rjxDy3bM_ekv-qrbBkbvMWLYxb27qhdBlCJwccbVYQTp-/image-asset.jpeg"/></noscript><img alt="" class="thumb-image" data-image="https://images.squarespace-cdn.com/content/v1/59848f3cff7c504ae1913932/1503134163563-L5WM70BN1K97DNTIPITT/ke17ZwdGBToddI8pDm48kBxX7QcyDclQBhYCHwbxl9YUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8PaoYXhp6HxIwZIk7-Mi3Tsic-L2IOPH3Dwrhl-Ne3Z2cYSQx44Ke2W_Sil3mM9rjxDy3bM_ekv-qrbBkbvMWLYxb27qhdBlCJwccbVYQTp-/image-asset.jpeg" data-image-dimensions="750x1000" data-image-focal-point="0.5,0.5" data-image-id="598ec9701e5b6cfb68c4c907" data-load="false" data-src="https://images.squarespace-cdn.com/content/v1/59848f3cff7c504ae1913932/1503134163563-L5WM70BN1K97DNTIPITT/ke17ZwdGBToddI8pDm48kBxX7QcyDclQBhYCHwbxl9YUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8PaoYXhp6HxIwZIk7-Mi3Tsic-L2IOPH3Dwrhl-Ne3Z2cYSQx44Ke2W_Sil3mM9rjxDy3bM_ekv-qrbBkbvMWLYxb27qhdBlCJwccbVYQTp-/image-asset.jpeg" data-type="image"/>
</div>
</a>
</figure>
</div>
</div></div><div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-yui_3_17_2_2_1504341146353_8056"><div class="sqs-block-content"><p><em>All photography provided by Thomas Murray</em></p></div></div></div><div class="col sqs-col-6 span-6"><div class="sqs-block image-block sqs-block-image" data-aspect-ratio="83.73101952277658" data-block-type="5" id="block-b9b60c6953329489ab01"><div class="sqs-block-content">
<div class=" image-block-outer-wrapper layout-caption-below design-layout-inline combination-animation-none individual-animation-none individual-text-animation-none " data-test="image-block-inline-outer-wrapper">
<figure class=" sqs-block-image-figure intrinsic " style="max-width:1090.0px;">
<a class=" sqs-block-image-link " href="/take-action-bedford">
<div class=" image-block-wrapper has-aspect-ratio " data-animation-role="image" style="padding-bottom:83.73101806640625%;">
<noscript><img alt="Can you guess where this is?" src="https://images.squarespace-cdn.com/content/v1/59848f3cff7c504ae1913932/1503134036703-Z25S250LA4B4OJC61VFN/ke17ZwdGBToddI8pDm48kCtSbPbvv6e8IktVqvWwC-17gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QHyNOqBUUEtDDsRWrJLTmFk_H6M1tkD9NpL7mXac0oXK6tshSUusHXLocOKt1LRJ0pqa2t5qGUxwNj24FfCyt/image-asset.jpeg"/></noscript><img alt="Can you guess where this is?" class="thumb-image" data-image="https://images.squarespace-cdn.com/content/v1/59848f3cff7c504ae1913932/1503134036703-Z25S250LA4B4OJC61VFN/ke17ZwdGBToddI8pDm48kCtSbPbvv6e8IktVqvWwC-17gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QHyNOqBUUEtDDsRWrJLTmFk_H6M1tkD9NpL7mXac0oXK6tshSUusHXLocOKt1LRJ0pqa2t5qGUxwNj24FfCyt/image-asset.jpeg" data-image-dimensions="1090x1500" data-image-focal-point="0.5,0.5" data-image-id="598ec9a9e45a7cf6ebe599b5" data-load="false" data-src="https://images.squarespace-cdn.com/content/v1/59848f3cff7c504ae1913932/1503134036703-Z25S250LA4B4OJC61VFN/ke17ZwdGBToddI8pDm48kCtSbPbvv6e8IktVqvWwC-17gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QHyNOqBUUEtDDsRWrJLTmFk_H6M1tkD9NpL7mXac0oXK6tshSUusHXLocOKt1LRJ0pqa2t5qGUxwNj24FfCyt/image-asset.jpeg" data-type="image"/>
</div>
</a>
<figcaption class="image-caption-wrapper">
<div class="image-caption"><p><strong>Can you guess where this is?</strong></p></div>
</figcaption>
</figure>
</div>
</div></div></div></div></div></div></div>
</div><!--
-->
</main>
<div id="preFooter">
<div class="pre-footer-inner">
<div class="sqs-layout sqs-grid-12 columns-12" data-layout-label="Pre-Footer Content" data-type="block-field" data-updated-on="1406661050940" id="preFooterBlocks"><div class="row sqs-row"><div class="col sqs-col-12 span-12"></div></div></div>
</div>
</div>
<footer id="footer" role="contentinfo">
<div class="footer-inner">
<div class="nav-wrapper back-to-top-nav"><nav><div class="back-to-top"><a href="#header">Back to Top</a></div></nav></div>
<div id="siteInfo"><span class="site-address">Americans in Portugal, Rua do Algarve 4, Estoril, Portugal</span></div>
<div class="sqs-layout sqs-grid-12 columns-12" data-layout-label="Footer Content" data-type="block-field" data-updated-on="1456895570456" id="footerBlocks"><div class="row sqs-row"><div class="col sqs-col-12 span-12"><div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-713fe491a2c303b33937"><div class="sqs-block-content"><p class="text-align-center">Powered by <a href="http://www.squarespace.com?channel=word_of_mouth&amp;subchannel=customer&amp;source=footer&amp;campaign=4fd1028ee4b02be53c65dfb3" target="_blank">Squarespace</a></p></div></div></div></div></div>
</div>
</footer>
</div>
<script src="https://static1.squarespace.com/static/ta/52a74d9ae4b0253945d2aee9/1039/scripts/site-bundle.js" type="text/javascript"></script>
<script data-sqs-type="imageloader-bootstrapper" type="text/javascript">(function() {if(window.ImageLoader) { window.ImageLoader.bootstrap({}, document); }})();</script><script>Squarespace.afterBodyLoad(Y);</script>
</body>
</html>
