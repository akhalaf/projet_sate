<!DOCTYPE html>
<html dir="ltr" lang="nl" prefix="content: http://purl.org/rss/1.0/modules/content/  dc: http://purl.org/dc/terms/  foaf: http://xmlns.com/foaf/0.1/  og: http://ogp.me/ns#  rdfs: http://www.w3.org/2000/01/rdf-schema#  schema: http://schema.org/  sioc: http://rdfs.org/sioc/ns#  sioct: http://rdfs.org/sioc/types#  skos: http://www.w3.org/2004/02/skos/core#  xsd: http://www.w3.org/2001/XMLSchema# ">
<head>
<meta charset="utf-8"/>
<noscript><style>form.antibot * :not(.antibot-message) { display: none !important; }</style>
</noscript><script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-116956470-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("set", "page", "/404.html?page=" + document.location.pathname + document.location.search + "&from=" + document.referrer);ga("send", "pageview");</script>
<link href="http://www.baj.be/nl/404-pagina" rel="canonical"/>
<meta content="Drupal 8 (https://www.drupal.org)" name="Generator"/>
<meta content="width" name="MobileOptimized"/>
<meta content="true" name="HandheldFriendly"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="/sites/baj/files/18033226_898154980324743_6661525416966056979_n.png" rel="shortcut icon" type="image/png"/>
<link href="http://www.baj.be/nl/404-pagina" hreflang="nl" rel="alternate"/>
<link href="http://www.baj.be/nl/404-pagina" rel="revision"/>
<title>404 pagina | BAJ Beton</title>
<link href="/sites/baj/files/css/css_c-SJEfTBk6-BFaY3SaS5e0IG_-L7tf0X4Z5fHcRKGV8.css" media="all" rel="stylesheet"/>
<link href="/sites/baj/files/css/css_oWKqQjrnXLpht0SbQe0PIRrOxo0HbwBGrSwxB1p9Ego.css" media="all" rel="stylesheet"/>
<!--[if lte IE 8]>
<script src="/sites/baj/files/js/js_VtafjXmRvoUgAzqzYTA3Wrjkx9wcWhjP0G4ZnnqRamA.js"></script>
<![endif]-->
<script data-search-pseudo-elements="" src="/libraries/fontawesome/js/all.min.js?v=5.13.1"></script>
<script data-search-pseudo-elements="" src="/libraries/fontawesome/js/v4-shims.min.js?v=5.13.1"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,300i,400,400i,500,700,900" rel="stylesheet"/>
</head>
<body class="path-skin node--type-page">
<a class="visually-hidden focusable skip-link" href="#main-content">
      Overslaan en naar de inhoud gaan
    </a>
<div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas="">
<div id="mmenu-menu"></div>
<div class="page">
<header class="region-header">
<div class="region-header__top clearfix">
<div class="region region-headertop">
<div id="block-vragen">
<h2>Vragen?</h2>
<p><a href="mailto:info@baj.be">info@baj.be</a></p>
</div><div class="socialmedia socialmedia--header" id="block-socialmedia">
<div class="socialmedia__links">
<a class="socialmedia__link socialmedia__facebook" href="https://www.facebook.com/BajBeton" target="_blank"><svg class="socialmedia__icon"><use xlink:href="#icon-facebook"></use></svg></a>
</div>
</div><div class="search search--header" id="block-search">
<div class="search__content">
<form accept-charset="UTF-8" action="/search" class="views-exposed-form bef-exposed-form" data-drupal-selector="views-exposed-form-search-block-search" id="views-exposed-form-search-block-search" method="get">
<div class="form--inline clearfix">
<div class="js-form-item form-item js-form-type-search-api-autocomplete form-item-search-api-fulltext js-form-item-search-api-fulltext">
<label data-title="Zoeken" for="edit-search-api-fulltext">Zoeken</label>
<div class="search__overlay">
<div class="search__overlay__inner">
<input class="form-autocomplete form-text" data-autocomplete-path="/nl/search_api_autocomplete/search?display=block_search&amp;&amp;filter=search_api_fulltext" data-drupal-selector="edit-search-api-fulltext" data-search-api-autocomplete-search="search" id="edit-search-api-fulltext" maxlength="128" name="search_api_fulltext" size="30" type="text" value=""/>
</div>
</div>
</div><div class="button js-form-submit form-submit form-actions">
<input class="button js-form-submit form-submit" data-drupal-selector="edit-submit-search" id="edit-submit-search" type="submit" value="Apply"/>
</div>
</div>
</form>
</div>
</div><div class="language-switcher-language-url" id="block-taalkeuze" role="navigation">
<ul class="links"><li class="en" data-drupal-link-query='{"_exception_statuscode":404,"destination":"\/nl"}' data-drupal-link-system-path="node/79" hreflang="en"><a class="language-link" data-drupal-link-query='{"_exception_statuscode":404,"destination":"\/nl"}' data-drupal-link-system-path="node/79" href="/en/node/79?destination=/nl&amp;_exception_statuscode=404" hreflang="en">English</a></li><li class="fr" data-drupal-link-query='{"_exception_statuscode":404,"destination":"\/nl"}' data-drupal-link-system-path="node/79" hreflang="fr"><a class="language-link" data-drupal-link-query='{"_exception_statuscode":404,"destination":"\/nl"}' data-drupal-link-system-path="node/79" href="/fr/node/79?destination=/nl&amp;_exception_statuscode=404" hreflang="fr">French</a></li><li class="nl" data-drupal-link-query='{"_exception_statuscode":404,"destination":"\/nl"}' data-drupal-link-system-path="node/79" hreflang="nl"><a class="language-link" data-drupal-link-query='{"_exception_statuscode":404,"destination":"\/nl"}' data-drupal-link-system-path="node/79" href="/nl/404-pagina?destination=/nl&amp;_exception_statuscode=404" hreflang="nl">Dutch</a></li></ul>
</div><nav aria-labelledby="block-topnavigation-menu" class="block block-menu navigation menu--top-navigation" id="block-topnavigation">
<h2 class="visually-hidden" id="block-topnavigation-menu">top navigation</h2>
<ul class="menu top-menu">
<li class="menu-item">
<a data-drupal-link-system-path="node/47" href="/nl/questions-frequemment" target="_self">FAQ</a>
</li>
<li class="menu-item">
<a data-drupal-link-system-path="node/48" href="/nl/baj" target="_self">Over ons</a>
</li>
<li class="menu-item">
<a data-drupal-link-system-path="user/login" href="/nl/user/login?current=/sites/baj/files/2018-03/Richtlijnen%2Bvoor%2Bhet%2Bplaatsen.pdf">Inloggen</a>
</li>
</ul>
</nav>
</div>
</div>
<div class="header">
<div class="header__nav clearfix">
<div class="inner--wide relative">
<div class="header__logo" id="block-logoblock">
<a href="/">
<img src="/sites/baj/files/styles/logo/public/2018-02/logo.png?itok=H8iSjMdO"/>
</a>
</div><nav aria-labelledby="block-hoofdnavigatie-menu" class="block block-menu navigation menu--main" id="block-hoofdnavigatie">
<h2 class="visually-hidden" id="block-hoofdnavigatie-menu">Main navigation</h2>
<ul class="menu main-menu">
<li class="menu-item">
<a data-drupal-link-system-path="&lt;front&gt;" href="/nl" target="_self">Home</a>
</li>
<li class="menu-item menu-item--expanded">
<a data-drupal-link-system-path="node/8" href="/nl/offre" target="_self">Aanbod</a>
<ul class="menu">
<li class="menu-item">
<a data-drupal-link-system-path="node/63" href="/nl/brochure" target="_self">Brochure</a>
</li>
<li class="menu-item menu-item--expanded">
<a data-drupal-link-system-path="node/116" href="/nl/tuinomheining-en-schutting">Tuinomheining en schutting</a>
<ul class="menu">
<li class="menu-item">
<a data-drupal-link-system-path="node/52" href="/nl/houtsystemen" target="_self">Houtschermen</a>
</li>
</ul>
</li>
<li class="menu-item">
<a data-drupal-link-system-path="node/75" href="/nl/funderingsmaterialen" target="_self">Funderingsmaterialen</a>
</li>
<li class="menu-item">
<a data-drupal-link-system-path="node/76" href="/nl/afwerkingsproducten" target="_self">Afwerkingsproducten</a>
</li>
<li class="menu-item">
<a data-drupal-link-system-path="node/58" href="/nl/overige-producten" target="_self">Overige producten</a>
</li>
</ul>
</li>
<li class="menu-item menu-item--expanded">
<a data-drupal-link-system-path="node/98" href="/nl/information" target="_self">Algemene info</a>
<ul class="menu">
<li class="menu-item">
<a data-drupal-link-system-path="node/48" href="/nl/baj" target="_self">Over ons</a>
</li>
<li class="menu-item">
<a data-drupal-link-system-path="node/81" href="/nl/algemene-voorwaarden" target="_self">Algemene Voorwaarden</a>
</li>
<li class="menu-item">
<a data-drupal-link-system-path="node/64" href="/nl/technische-info" target="_self">Technische info &amp; Plaatsingsrichtlijnen</a>
</li>
<li class="menu-item">
<a data-drupal-link-system-path="node/45" href="/nl/les-avantages" target="_self">Voordelen</a>
</li>
<li class="menu-item">
<a data-drupal-link-system-path="node/47" href="/nl/questions-frequemment" target="_self">Veelgestelde vragen</a>
</li>
</ul>
</li>
<li class="menu-item">
<a data-drupal-link-system-path="node/47" href="/nl/questions-frequemment" target="_self">Veelgestelde Vragen</a>
</li>
<li class="menu-item">
<a data-drupal-link-system-path="node/12" href="/nl/contact" target="_self">Contact</a>
</li>
<li class="menu-item">
<a data-drupal-link-system-path="node/90" href="/nl/offerte" target="_self">Offerte maker</a>
</li>
</ul>
</nav>
<button class="hamburger hamburger--squeeze" id="menu_icon" type="button">
<span class="hamburger-box">
<span class="hamburger-inner"></span>
</span>
<span class="hamburger-label">Menu</span>
</button>
</div>
</div>
<div class="views-element-container header-block clearfix" id="block-views-block-header-block-header">
<div about="/nl/404-pagina" class="header__cont" data-history-node-id="79" role="article" style="background-image:url( /sites/baj/files/styles/header_detail/public/2018-04/Woodox_2.JPG?h=69dba732&amp;itok=L14LJSQQ )" typeof="schema:WebPage">
</div>
</div>
</div>
</header>
<main class="main-content">
<a id="main-content"></a>
<div class="region region-content">
<div class="hidden" data-drupal-messages-fallback=""></div><div id="block-content">
<article about="/nl/404-pagina" class="node--page" data-history-node-id="79" typeof="schema:WebPage">
<div class="paragraphs"> <div class="paragraph paragraph--text paragraph-id--406 paragraph--type--text paragraph--view-mode--default clearfix">
<div class="inner--narrow">
<div class="text__text text text--black"> <p>Deze pagina werd niet gevonden.</p>
</div> </div>
</div>
</div> </article>
</div>
</div>
</main>
<footer>
<div class="region region-footer">
<div class="footer" id="block-footerblock" style="background-image:url( /sites/baj/files/styles/footer_bg/public/2018-02/DSC_196.jpg?h=242cd5c8&amp;itok=sp9SThtA )">
<div class="inner--wide">
<div class="footer__cont clearfix">
<div class="footer__left clearfix">
<div class="header__logo header__logo--footer" id="block-logoblockfooter">
<img src="/sites/baj/files/styles/logo_footer/public/2018-02/logo_0.png?itok=KiuNZrmM"/>
</div>
<div class="offices offices--footer">
<div class="company-name">BAJ Beton</div>
<div about="/nl/node/80" class="office office--footer" data-history-node-id="80" role="article">
<div>
<div class="office__content">
<div class="office__address">
<div class="office__name"><span class="field field--name-title field--type-string field--label-hidden">Kantoor BAJ-Beton</span>
</div>
<div class="office__street">Europark 2002</div> <span class="office__zipcode">3530</span> <span class="office__municipality">Houthalen, België </span> </div>
<div class="office__contact">
<div><span class="office__prefix">T.</span><span class="office__phone">      +32 11 34 08 50
</span></div> <div><span class="office__prefix">E.</span><span class="office__e-mail">      info@baj.be
</span></div> </div>
</div>
</div>
</div>
<div about="/nl/node/6" class="office office--footer" data-history-node-id="6" role="article">
<div>
<div class="office__content">
<div class="office__address">
<div class="office__name"><span class="field field--name-title field--type-string field--label-hidden">Plaatsingsdienst: Calleeuw-Lambrechts </span>
</div>
<div class="office__street">Vogelsancklaan 14</div> <span class="office__zipcode">3550</span> <span class="office__municipality">Heusden - Zolder, België </span> </div>
<div class="office__contact">
<div><span class="office__prefix">T.</span><span class="office__phone">      +32 11 42 11 25
</span></div> <div><span class="office__prefix">E.</span><span class="office__e-mail">      calleeuw-lambrechts@skynet.be
</span></div> </div>
</div>
</div>
</div>
<div class="block--flanders">
<p>Gerealiseerd met de steun van</p>
<img alt="" src="/sites/baj/themes/novsubtheme/img/flanders.png"/>
</div>
</div>
<nav aria-labelledby="block-menu-footer-menu" class="block block-menu navigation menu--footer" id="block-menu-footer">
<h2 class="visually-hidden" id="block-menu-footer-menu">Footer navigation</h2>
<ul class="menu footer-menu">
<li class="menu-item">
<a data-drupal-link-system-path="node/12" href="/nl/contact" target="_self">Openingsuren</a>
</li>
<li class="menu-item">
<a data-drupal-link-system-path="node/47" href="/nl/questions-frequemment" target="_self">FAQ</a>
</li>
<li class="menu-item">
<a data-drupal-link-system-path="node/81" href="/nl/algemene-voorwaarden" target="_self">Algemene voorwaarden</a>
</li>
<li class="menu-item">
<a data-drupal-link-system-path="node/86" href="/nl/privacy-policy" target="_self" title="Privacy Policy BAJ-BETON">Privacy Policy</a>
</li>
</ul>
</nav>
</div>
</div>
</div>
</div>
</div>
</footer>
</div>
</div>
<script data-drupal-selector="drupal-settings-json" type="application/json">{"path":{"baseUrl":"\/","scriptPath":null,"pathPrefix":"nl\/","currentPath":"","currentPathIsAdmin":false,"isFront":false,"currentLanguage":"nl"},"pluralDelimiter":"\u0003","suppressDeprecationErrors":true,"ajaxPageState":{"libraries":"better_exposed_filters\/general,classy\/base,classy\/messages,core\/html5shiv,core\/normalize,fontawesome\/fontawesome.svg.shim,google_analytics\/google_analytics,novsubtheme\/fonts,novsubtheme\/geocomplete,novsubtheme\/global-js,novsubtheme\/global-styling,novsubtheme\/gmap,novtheme\/cookieconsent,novtheme\/fonts,novtheme\/global-js,novtheme\/global-styling,novtheme\/header-slick,novtheme\/scroll,paragraphs\/drupal.paragraphs.unpublished,search_api_autocomplete\/search_api_autocomplete,system\/base,views\/views.module","theme":"novsubtheme","theme_token":null},"ajaxTrustedUrl":{"\/search":true},"google_analytics":{"trackOutbound":true,"trackMailto":true,"trackDownload":true,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"search_api_autocomplete":{"search":{"delay":0,"auto_submit":true}},"language":"nl","user":{"uid":0,"permissionsHash":"48893cec3a7b4e37b2ad7365a528b351c7da6c0ebf0c9880c9ef753dfb513aa6"}}</script>
<script src="/sites/baj/files/js/js_2ZptiBW9WCItr4f_2t5_OKUNECG4HxWOm3B_fzcQL-E.js"></script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyDysh0MeNA33B8oaf396enfzyboP7NdxmM&amp;libraries=places&amp;language=nl"></script>
<script src="/sites/baj/files/js/js_9HzM71vgg2-kUAyDVRiOr7lwZiQwJz0OmIl9lMMABb4.js"></script>
</body>
</html>