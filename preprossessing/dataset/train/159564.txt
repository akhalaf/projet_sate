<!DOCTYPE html>
<html>
<head>
<title>Overseas Manpower Consultancy | International Manpower Recruitment Consultant</title>
<meta content="Hire proficient international manpower. As one of the best international recruitment agencies, we offer you efficient manpower from many countries like - India, Bangladesh, Nepal, etc." name="description"/>
<meta content="Overseas manpower consultancy, International recruitment agencies, Overseas manpower consultant, overseas manpower, hire overseas manpower, manpower from India, manpower from Bangladesh, manpower from Nepal, international manpower in Romania, international manpower" name="keywords"/>
<meta charset="utf-8"/>
<meta class="viewport" content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="img/favicon.ico" rel="shortcut icon"/>
<link href="css/buttons/social-icons.css" rel="stylesheet"/>
<link href="css/font-awesome.min.css" rel="stylesheet"/>
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<link href="css/jslider.css" rel="stylesheet"/>
<link href="css/settings.css" rel="stylesheet"/>
<link href="css/jquery.fancybox.css" rel="stylesheet"/>
<link href="css/animate.css" rel="stylesheet"/>
<link href="css/video-js.min.css" rel="stylesheet"/>
<link href="css/morris.css" rel="stylesheet"/>
<link href="css/royalslider/royalslider.css" rel="stylesheet"/>
<link href="css/royalslider/skins/minimal-white/rs-minimal-white.css" rel="stylesheet"/>
<link href="css/layerslider/layerslider.css" rel="stylesheet"/>
<link href="css/ladda.min.css" rel="stylesheet"/>
<link href="css/style.min1.css" rel="stylesheet"/>
<link href="css/responsive.css" rel="stylesheet"/>
<link href="css/customizer/pages.css" rel="stylesheet"/>
<link href="css/customizer/home-pages-customizer.css" rel="stylesheet"/>
<style>
  .full-width-box .container {
  top:200px;
  padding:0 60px;
  }
  select {
	border:1px solid #484848;
  }
  </style>
</head>
<body class="fixed-header">
<div class="page-box">
<div class="page-box-content">
<header class="header header-two">
<div class="container">
<div class="row">
<div class="col-xs-6 col-md-2 col-lg-3 logo-box" style="padding-right:0px;">
<div class="logo">
<a href="index.html">
<img alt="" class="logo-img" src="img/logo.png"/> </a> </div>
<!--<div class="logo2">
		  <a href="index.html">
			<img src="img/30_years.png" class="logo-img" alt="">
		  </a>
		</div>-->
</div><!-- .logo-box -->
<div class="col-xs-6 col-md-10 col-lg-9 right-box">
<div class="right-box-wrapper">
<div class="header-icons">
<div class="social-icon"><a class="sm-icon" href="https://www.facebook.com/groups/723982598177156/?ref=share" target="_blank"><span class="fb-icon"><i class="fa fa-facebook-square"></i> </span></a>
<a class="sm-icon" href="https://www.linkedin.com/company/asiapower-overseas-employment-services" target="_blank"><span class="linkedin-icon"><i class="fa fa-linkedin-square"></i> </span></a>
</div>
<div id="google_translate_element" style="text-align:right;"><span style="font-size:12px;">Translate this page to <span></span></span></div>
<!--<div class="email-header hidden-600">
			  <a href="#">
				 <image src="img/png-icons/email-icon.png" alt="" width="16" height="16" style="vertical-align: top;">
			  </a>
			</div>
			<div class="phone-header hidden-600">
			  <a href="#">
				<image src="img/png-icons/phone-icon.png" alt="" width="16" height="16" style="vertical-align: top;">
				</a>
			</div>-->
</div>
<div class="primary">
<div class="navbar navbar-default" role="navigation">
<button class="navbar-toggle btn-navbar collapsed" data-target=".primary .navbar-collapse" data-toggle="collapse" type="button">
<span class="text">Menu</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span></button>
<nav class="collapse collapsing navbar-collapse">
<ul class="nav navbar-nav navbar-center">
<li>
<a href="index.html">Home</a></li>
<li class="parent megamenu promo">
<a href="employer.html">Employers</a>
<ul class="sub">
<li class="sub-wrapper">
<div class="sub-list">
<div class="box">
<ul>
<li><a href="employer.html">Employer Home</a></li>
<li><a href="http://asiapower.in/SearchJobseeker.aspx#modal" target="_blank">Employer Login<!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="item-new">New</span>--></a></li>
<li><a href="http://asiapower.in/RegisterEmployer.aspx" target="_blank">New Employer<!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="item-new" style="margin-left:5px;">New</span>--></a></li>
<li><a href="http://asiapower.in/SearchJobseeker.aspx" target="_blank">Jobseeker Database  <span class="item-new">New</span></a></li>
<li><a href="services.html">Services </a></li>
<li><a href="http://asiapower.in/rpo/index.html">RPO                           <span class="item-new">New</span></a></li>
<li><a href="http://asiapower.in/sectors" target="_blank">Our Sectors                <span class="item-new">New</span></a></li>
</ul>
</div>
<!-- .box -->
<div class="box">
<ul>
<li><a href="recruitment_network.html">Recruitment Network <span class="item-new bg-warning">Wow</span></a></li>
<li><a href="asiapower_advantage.html">Asiapower Advantage</a></li>
<li><a href="manpower_trends.html">Manpower Trends &amp; Quotes</a></li>
<!--<li><a href="our_asset.html">Our Asset</a></li>-->
<li><a href="case_studies.html">Case Studies           <span class="item-new" style="margin-left:9px;">New</span></a></li>
<li><a href="clientele.html">Our Clientele            <span class="item-new" style="margin-left:8px;">New</span></a></li>
<li><a href="employer_faq.html">FAQ's                       <span class="item-new">New</span></a></li>
<li><a href="employer_enquiry.html" target="_blank">Enquiry Form </a></li>
<!--<li><a href="http://asiapower.in/ebrochure/catalogue.html" target="_blank">E-Brochure </a></li>-->
</ul>
</div><!-- .box -->
</div><!-- .sub-list -->
<div class="promo-block">
<a href="#">
<img alt="" height="270" src="img/content/employer-menu.jpg" width="315"/> </a> </div><!-- .promo-block -->
</li>
</ul>
</li>
<!--<li class="parent megamenu promo">
					<a href="#">Jobseekers</a>
					<ul class="sub">
					  <li class="sub-wrapper">
						<div class="sub-list">
						  <div class="box">
							<ul>
							  <li><a href="#">Jobseeker Login <span class="item-new">New</span></a></li>
							  <li><a href="#">New Jobseeker <span class="item-new">New</span></a></li>
							  <li><a href="#">Search Jobs <span class="item-new">New</span></a></li>
							  <li><a href="#">Services </a></li>
							  <li><a href="#">Recruitment Network <span class="item-new bg-warning">Wow</span></a></li>
							  <li><a href="#">Asiapower Advantage</a></li>
							  <li><a href="#">Our Asset</a></li>

							</ul>
						  </div>
						  <div class="box">
							<ul>
							  <li><a href="#">Case Studies</a></li>
							  <li><a href="#">Our Clientele</a></li>
							  <li><a href="#">Jobseeker Resources</a></li>
							  <li><a href="#">Jobseeker FAQ's <span class="item-new bg-warning">Wow</span></a></li>
							  <li><a href="#">Enquiry Form </a></li>
							  <li><a href="#">E-Brochure </a></li>
							</ul>
						  </div>
						</div>
						
						<div class="promo-block">
						  <a href="#">
							<img src="img/content/jobseeker-menu.jpg" width="315" height="270" alt="">
						  </a>
						</div>
					  </li>
					</ul>
				  </li>-->
<li class="parent">
<a>Asiapower</a>
<ul class="sub">
<li><a href="asiapower_advantage.html">Asiapower Advantage</a></li>
<li><a href="sectors" target="_blank">Our Sectors</a></li>
<li><a href="manpower_trends.html">Manpower Trends &amp; Quotes</a></li>
<!--<li><a href="our_asset.html">Our Asset</a></li>-->
<li><a href="clientele.html">Our Clientele <span class="item-new">New</span></a></li>
<li><a href="photo_gallery.html">Photo Gallery</a></li>
<li><a href="team.html">Our Success Team</a></li>
<!--<li><a href="http://asiapower.in/ebrochure/catalogue.html" target="_blank">E-Brochure</a></li>-->
</ul>
</li>
<li class="parent">
<a>Resources</a>
<ul class="sub">
<li><a href="employer_faq.html">Employer FAQ's <span class="item-new">New</span></a></li>
</ul>
</li>
<li class="parent">
<a>Reach Us</a>
<ul class="sub">
<li><a href="reach_us.html">Offices</a></li>
<!--<li><a href="#">Employer Feedback</a></li>-->
<li><a href="employer_enquiry.html" target="_blank">Enquiry Form</a></li>
</ul>
</li>
<li>
<a href="jobseeker.html"><font color="#be2a32"><i class="fa fa-group"></i> Jobseekers Zone</font></a> </li>
<li>
<img alt="" height="60" src="img/ukas-logo.jpg" width="94"/> </li>
</ul>
</nav>
</div>
</div><!-- .primary -->
</div>
</div>
<!--<div class="phone-active col-sm-9 col-md-9">
		<a href="#" class="close"><span>close</span>Ã</a>
		<span class="title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> <strong>( +91 - 22 ) 4225 0000</strong>
	  </div>
	  <div class="email-active col-sm-9 col-md-9">
		<a href="#" class="close"><span>close</span>Ã</a>
		<span class="title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> <strong><a href="mailto:recruit@asiapower.co" target="_blank">recruit@asiapower.co</a></strong>	
      </div>-->
</div><!--.row -->
</div>
</header><!-- .header -->
<div class="full-width-box bottom-padding" id="cm-video-bg">
<div class="fwb-bg fwb-video band-16">
<video autoplay="" loop="loop" muted="" poster="video/welding.jpg">
<source src="video/apoes_home.mp4" type="video/mp4">
<source codecs="theora, vorbis" src="video/apoes_home.ogg" type="video/ogg"/>
<source src="video/apoes_home.webm" type="video/webm">
</source></source></video>
<div class="overlay"></div>
</div>
<div class="container">
<div class="row text-left" style="padding:20px 60px 0 60px;">
<div class="content-box col-sm-6 col-md-6" style="padding:0 7px;">
<h2 class="title light white" style="font-size:38px;margin:0 0 5px;"><a href="http://asiapower.in/sectors" target="_blank"><img src="img/delivering.png"/></a></h2>
<!--<p class="description white">
			  You can form the page yourself by placing any information you want â graphs, tables, buttons, slides, animated content, maps and so on.</p>-->
<!--<button class="btn btn-red btn-default">Read more</button>-->
<br/>
</div>
<div class="col-sm-3 col-md-3" style="padding:0 7px;">
<div class="package" style="background-color:#c01922;">
<a href="employer.html"><img src="img/emp.jpg"/></a>
<div class="bottom-box" style="background-color:#c01922;padding-bottom:10px;padding-top:10px;border-top-width:0px;">
<a class="more" href="employer.html" style="color:#f7f7f7;">EMPLOYERS <span class="glyphicon glyphicon-arrow-right" style="font-size:10px;"></span></a>
</div>
</div><!-- .package -->
</div>
<div class="col-sm-3 col-md-3" style="padding:0 7px;">
<div class="package">
<a href="jobseeker.html"><img src="img/jobs.jpg"/></a>
<div class="bottom-box" style="background-color:#c01922;padding-bottom:10px;padding-top:10px;border-top-width:0px;">
<a class="more" href="jobseeker.html" style="color:#f7f7f7;">JOBSEEKERS <span class="glyphicon glyphicon-arrow-right" style="font-size:10px;"></span></a>
</div>
</div><!-- .package -->
</div>
</div>
<div class="row" style="padding:0 60px 0 60px;">
<div class="col-sm-3 col-md-3" style="padding:0 7px;">
</div>
<div class="col-sm-3 col-md-3" style="padding:0 7px;">
<div class="package">
<a href="http://asiapower.in/cp/" target="_blank"><img src="img/corp-presentation.jpg"/></a>
<div class="bottom-box" style="background-color:#cb0f19;padding-bottom:10px;padding-top:10px;border-top-width:0px;">
<a class="more" href="http://asiapower.in/cp/" style="color:#f7f7f7;" target="_blank">Corporate Presentation <!--<span class="glyphicon glyphicon-arrow-right" style="font-size:10px;"></span>--></a>
</div>
</div><!-- .package -->
</div>
<div class="col-sm-3 col-md-3" style="padding:0 7px;">
<div class="package">
<a href="clientele.html"><img src="img/asset2.jpg"/></a>
</div><!-- .package -->
</div>
<div class="col-sm-3 col-md-3" style="padding:0 7px;">
<div class="package">
<a href="http://asiapower.in/searchjobseeker.aspx" target="_blank"><img src="img/emp-module.jpg"/></a>
<div class="bottom-box" style="background-color:#cb0f19;padding-bottom:10px;padding-top:10px;border-top-width:0px;">
<a class="more" href="http://asiapower.in/searchjobseeker.aspx" style="color:#f7f7f7;" target="_blank">Read more <span class="glyphicon glyphicon-arrow-right" style="font-size:10px;"></span></a>
</div>
</div><!-- .package -->
</div>
</div>
</div>
</div><!-- .full-width-box -->
<footer id="footer">
<div class="footer-bottom">
<div class="container">
<div class="row">
<!--<div class="copyright col-xs-12 col-sm-4 col-md-4">
		  <a href="index.html">Home</a> | <a href="http://asiapower.in/asset.html">Employers</a> | <a href="http://asiapower.in/searchjobs.aspx">Jobseekers</a> | <a href="http://asiapower.in/asset.html">Asiapower</a> | <a href="http://asiapower.in/reachus.html">Reach Us</a>
		</div>-->
<div class="copyright col-xs-12 col-sm-4 col-md-4" style="text-align:center;">
		  Â© 1984 - <script type="text/javascript">document.write(new Date().getFullYear());</script> Asiapower Overseas Employment Services.<br/>All rights reserved. <!--<a href="#">Privacy Policy</a>-->
</div>
<div class="copyright col-xs-12 col-sm-2 col-md-2" style="text-align:center;">
		  Concept &amp; Vision: <a href="http://leindia.in/" target="_blank">Limited Edition</a> <!--<a href="#">Privacy Policy</a>-->
</div>
<div class="copyright col-xs-12 col-sm-2 col-md-1" style="text-align:center;">
<a href="rss.xml">RSS</a> | <a href="sitemap.xml">Sitemap</a> <!--<a href="#">Privacy Policy</a>-->
</div>
<div class="copyright col-xs-12 col-sm-2 col-md-1" style="text-align:center;">
<a class="sm-icon" href="https://www.facebook.com/groups/723982598177156/?ref=share" target="_blank"><span class="fb-icon"><i class="fa fa-facebook-square"></i> </span></a>
<a class="sm-icon" href="https://www.linkedin.com/company/asiapower-overseas-employment-services" target="_blank"><span class="linkedin-icon"><i class="fa fa-linkedin-square"></i> </span></a>
</div>
<div class="copyright col-xs-12 col-sm-3 col-md-3" style="text-align:center;">
<a href="http://ithink.co/website_design_redesign.html" target="_blank">Web Design</a>: <a href="http://ithink.co/" target="_blank">Think Technology Services</a>
</div>
<div class="col-xs-12 col-sm-1 col-md-1">
<a class="up" href="#">
<span class="glyphicon glyphicon-arrow-up"></span>
</a>
</div>
</div>
</div>
</div><!-- .footer-bottom -->
</footer>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/raphael.min.js"></script>
<script src="js/video.js"></script>
<script src="js/main.js"></script>
<script type="text/javascript">
            var vlanguages = ["en","fr","ar","zh-CN","ko","tr","it","ja","es","pl","ru"];
            var vpageLanguage = "en";
            var vincludedLanguages = "";
            for(var i = 0; i < vlanguages.length; i++){
                if(vlanguages[i] != "-")
                    vincludedLanguages += vlanguages[i] + ',';
            }
            vincludedLanguages = vincludedLanguages.substr(0, vincludedLanguages.length-1);
            var vautoDisplay = true;
            var vmultilanguagePage = true;
            var vgaTrack = true;
            var vgaId = true ? "" : "";
            
            function googleTranslateElementInit() {
                new google.translate.TranslateElement({pageLanguage: vpageLanguage, includedLanguages: vincludedLanguages, layout: google.translate.TranslateElement.InlineLayout.VERTICAL, autoDisplay: vautoDisplay, multilanguagePage: vmultilanguagePage, gaTrack: vgaTrack, gaId: vgaId}, 'google_translate_element');
            }
        </script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-10836989-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</div></div></body>
</html>