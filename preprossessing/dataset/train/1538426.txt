<!DOCTYPE html>
<html lang="pt-BR">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-3385226-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-3385226-6');
</script>
<!-- FIM DA Global site tag (gtag.js) - Google Analytics -->
<!-- Inicio do Google Adsense Anuncios Automaticos -->
<script async="" data-ad-client="ca-pub-4820827552868370" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Fim do Google Adsense Anuncios Automaticos -->
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://vidadeviajante.com.br/xmlrpc.php" rel="pingback"/>
<title>Página não encontrada – Vida de Viajante</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://vidadeviajante.com.br/feed/" rel="alternate" title="Feed para Vida de Viajante »" type="application/rss+xml"/>
<link href="https://vidadeviajante.com.br/comments/feed/" rel="alternate" title="Feed de comentários para Vida de Viajante »" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/vidadeviajante.com.br\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://vidadeviajante.com.br/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vidadeviajante.com.br/wp-content/plugins/themeisle-companion/obfx_modules/gutenberg-blocks/assets/fontawesome/css/all.min.css?ver=2.10.1" id="font-awesome-5-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vidadeviajante.com.br/wp-content/plugins/themeisle-companion/obfx_modules/gutenberg-blocks/assets/fontawesome/css/v4-shims.min.css?ver=2.10.1" id="font-awesome-4-shims-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Lato%3A300%2C400%2C700%2C400italic%7CMontserrat%3A400%2C700%7CHomemade+Apple&amp;subset=latin%2Clatin-ext" id="zerif_font-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2C400%2C400italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&amp;subset=latin&amp;ver=5.5.3" id="zerif_font_all-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vidadeviajante.com.br/wp-content/themes/zerif-lite/css/bootstrap.css?ver=5.5.3" id="zerif_bootstrap_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vidadeviajante.com.br/wp-content/themes/zerif-lite/css/font-awesome.min.css?ver=v1" id="zerif_fontawesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vidadeviajante.com.br/wp-content/themes/zerif-lite/style.css?ver=1.8.5.49" id="zerif_style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="zerif_style-inline-css" type="text/css">

		.page-template-builder-fullwidth {
			overflow: hidden;
		}
		@media (min-width: 768px) {
			.page-template-builder-fullwidth-std .header > .elementor {
				padding-top: 76px;
			}
		}

</style>
<link href="https://vidadeviajante.com.br/wp-content/themes/zerif-lite/css/responsive.css?ver=1.8.5.49" id="zerif_responsive_style-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='zerif_ie_style-css'  href='https://vidadeviajante.com.br/wp-content/themes/zerif-lite/css/ie.css?ver=1.8.5.49' type='text/css' media='all' />
<![endif]-->
<link href="https://vidadeviajante.com.br/wp-content/plugins/tablepress/css/default.min.css?ver=1.12" id="tablepress-default-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://vidadeviajante.com.br/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='https://vidadeviajante.com.br/wp-content/themes/zerif-lite/js/html5.js?ver=5.5.3' id='zerif_html5-js'></script>
<![endif]-->
<link href="https://vidadeviajante.com.br/wp-json/" rel="https://api.w.org/"/><link href="https://vidadeviajante.com.br/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://vidadeviajante.com.br/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<!-- Enter your scripts here --><!-- <meta name="NextGEN" version="3.4.7" /> -->
<style id="custom-background-css" type="text/css">
body.custom-background { background-image: url("https://vidadeviajante.com.br/wp-content/uploads/2020/01/usa-784432.jpg"); background-position: left top; background-size: contain; background-repeat: no-repeat; background-attachment: fixed; }
</style>
<link href="https://vidadeviajante.com.br/wp-content/uploads/2019/08/viajem-boa-e-barata-150x150.png" rel="icon" sizes="32x32"/>
<link href="https://vidadeviajante.com.br/wp-content/uploads/2019/08/viajem-boa-e-barata-250x250.png" rel="icon" sizes="192x192"/>
<link href="https://vidadeviajante.com.br/wp-content/uploads/2019/08/viajem-boa-e-barata-250x250.png" rel="apple-touch-icon"/>
<meta content="https://vidadeviajante.com.br/wp-content/uploads/2019/08/viajem-boa-e-barata.png" name="msapplication-TileImage"/>
<style id="wp-custom-css" type="text/css">
			.video-responsive{overflow:hidden;padding-bottom:56.25%;position:relative;height:0;margin:0px 0px}
.video-responsive iframe{left:0;top:0;height:80%;width:80%;position:absolute;}		</style>
</head>
<body class="error404 wp-custom-logo elementor-default elementor-kit-2610">
<div id="mobilebgfix">
<div class="mobile-bg-fix-img-wrap">
<div class="mobile-bg-fix-img"></div>
</div>
<div class="mobile-bg-fix-whole-site">
<header class="header" id="home" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
<div class="navbar navbar-inverse bs-docs-nav" id="main-nav" role="banner">
<div class="container">
<div class="navbar-header responsive-logo">
<button class="navbar-toggle collapsed" data-target=".bs-navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Alternar navegação</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<div class="navbar-brand" itemscope="" itemtype="http://schema.org/Organization">
<a class="custom-logo-link" href="https://vidadeviajante.com.br/" rel="home"><img alt="Vida de Viajante" class="custom-logo" height="77" src="https://vidadeviajante.com.br/wp-content/uploads/2019/05/cropped-Logotipo-Vida-de-Viajante-JPG-BG-Branco-sem-www-300x77-1.jpeg" width="300"/></a>
</div> <!-- /.navbar-brand -->
</div> <!-- /.navbar-header -->
<nav class="navbar-collapse bs-navbar-collapse collapse" id="site-navigation" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
<a class="screen-reader-text skip-link" href="#content">Pular para o conteúdo</a>
<ul class="nav navbar-nav navbar-right responsive-nav main-nav-list" id="menu-menu-top"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-400" id="menu-item-400"><a>WorldVentures</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-301" id="menu-item-301"><a href="https://vidadeviajante.com.br/worldventures/historia-da-empresa/">História da Empresa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-304" id="menu-item-304"><a href="https://vidadeviajante.com.br/worldventures/worldventures-hoje/">WorldVentures Hoje</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1505" id="menu-item-1505"><a href="https://vidadeviajante.com.br/worldventures/worldventures-no-brasil/">WorldVentures no Brasil</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-303" id="menu-item-303"><a href="https://vidadeviajante.com.br/worldventures/premios-e-reconhecimentos/">Prêmios e Reconhecimentos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-302" id="menu-item-302"><a href="https://vidadeviajante.com.br/worldventures/celebridades/">Celebridades</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-407" id="menu-item-407"><a href="https://vidadeviajante.com.br/worldventures/valores-da-worldventures/">Valores da WorldVentures</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1301" id="menu-item-1301"><a href="https://vidadeviajante.com.br/worldventures/estrategia-do-oceano-azul/">Estratégia do Oceano Azul</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1302" id="menu-item-1302"><a href="https://vidadeviajante.com.br/worldventures/dreamtrips-oceano-azul/">Aqui é o Oceano Azul</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-457" id="menu-item-457"><a href="https://vidadeviajante.com.br/worldventures/links/">Links da Empresa</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-401" id="menu-item-401"><a>DreamTrips</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-445" id="menu-item-445"><a href="https://vidadeviajante.com.br/dreamtrips-clube-vip-de-viagens/beneficios-do-clube/">Benefícios do Clube</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2272" id="menu-item-2272"><a href="https://vidadeviajante.com.br/dreamtrips-clube-vip-de-viagens/comparacoes-de-precos/">Comparações de Preços</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2436" id="menu-item-2436"><a href="https://vidadeviajante.com.br/dreamtrips-clube-vip-de-viagens/comparacoes-de-precos/hoteis-no-brasil-pela-dreamtrips/">Hotéis no Brasil</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2435" id="menu-item-2435"><a href="https://vidadeviajante.com.br/dreamtrips-clube-vip-de-viagens/comparacoes-de-precos/hoteis-no-mundo-pela-dreamtrips/">Hotéis no Mundo</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2590" id="menu-item-2590"><a href="https://vidadeviajante.com.br/dreamtrips-clube-vip-de-viagens/comparacoes-de-precos/cruzeiros/">Cruzeiros</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1773" id="menu-item-1773"><a href="https://vidadeviajante.com.br/dreamtrips-clube-vip-de-viagens/tour-pela-plataforma-dreamtrips/">Tour pela Plataforma</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-905" id="menu-item-905"><a href="https://vidadeviajante.com.br/dreamtrips-clube-vip-de-viagens/planos-dreamtrips/">Planos DreamTrips</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1735" id="menu-item-1735"><a href="https://vidadeviajante.com.br/dreamgiver-saving-card/">DreamGiver</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-971" id="menu-item-971"><a>Sobre Nós</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-972" id="menu-item-972"><a href="https://vidadeviajante.com.br/sobre-nos/somos-todos-um/">Somos Todos Um</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1008" id="menu-item-1008"><a href="https://vidadeviajante.com.br/sobre-nos/nosso-mindset/">Nosso Mindset</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1616" id="menu-item-1616"><a href="https://vidadeviajante.com.br/sobre-nos/nossos-diretores/">One Big Team</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1091" id="menu-item-1091"><a href="https://vidadeviajante.com.br/sobre-nos/depoimentos-de-membros-worldventures-e-dreamtrips/">Depoimentos</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1959" id="menu-item-1959"><a>Tome Consciência</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1956" id="menu-item-1956"><a href="https://vidadeviajante.com.br/you-should-be-here/resumos-de-livros/">Microbooks em Áudio</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1832" id="menu-item-1832"><a href="https://vidadeviajante.com.br/pra-voce/insights-e-reflexoes/">Insights e Reflexões</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-424" id="menu-item-424"><a href="https://vidadeviajante.com.br/faq/">FAQ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-684" id="menu-item-684"><a href="https://vidadeviajante.com.br/quero-fazer-parte/">[COMO FAZER PARTE]</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1894" id="menu-item-1894"><a href="https:/vidadeviajante.com.br/blog">BLOG</a></li>
</ul> </nav>
</div> <!-- /.container -->
</div> <!-- /#main-nav -->
<!-- / END TOP BAR -->
<div class="clear"></div>
</header> <!-- / END HOME SECTION  -->
<div class="site-content" id="content">
<div class="container">
<div class="content-left-wrap col-md-9">
<div class="content-area" id="primary">
<main class="site-main" id="main">
<article>
<header class="entry-header">
<h1 class="entry-title">Epa! Essa página não pode ser encontrada.</h1>
</header><!-- .entry-header -->
<div class="entry-content">
<p>Parece que nada foi encontrado neste lugar. Quem sabe você possa tentar um dos links abaixo ou uma busca?</p>
</div><!-- .entry-content -->
</article><!-- #post-## -->
</main><!-- #main -->
</div><!-- #primary -->
</div>
<div class="sidebar-wrap col-md-3 content-left-wrap">
<div class="widget-area" id="secondary" role="complementary">
<aside class="widget widget_search" id="search-2"><form action="https://vidadeviajante.com.br/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Pesquisar por:</span>
<input class="search-field" name="s" placeholder="Pesquisar …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Pesquisar"/>
</form></aside><aside class="widget widget_nav_menu" id="nav_menu-2"><h2 class="widget-title">Viaje por aqui:</h2><div class="menu-menu-lateral-container"><ul class="menu" id="menu-menu-lateral"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-396" id="menu-item-396"><a>WorldVentures</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-310" id="menu-item-310"><a href="https://vidadeviajante.com.br/worldventures/historia-da-empresa/">História da Empresa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-307" id="menu-item-307"><a href="https://vidadeviajante.com.br/worldventures/worldventures-hoje/">WorldVentures Hoje</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1504" id="menu-item-1504"><a href="https://vidadeviajante.com.br/worldventures/worldventures-no-brasil/">WorldVentures no Brasil</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-309" id="menu-item-309"><a href="https://vidadeviajante.com.br/worldventures/premios-e-reconhecimentos/">Prêmios e Reconhecimentos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-308" id="menu-item-308"><a href="https://vidadeviajante.com.br/worldventures/celebridades/">Celebridades</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-408" id="menu-item-408"><a href="https://vidadeviajante.com.br/worldventures/valores-da-worldventures/">Valores da WorldVentures</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1299" id="menu-item-1299"><a href="https://vidadeviajante.com.br/worldventures/estrategia-do-oceano-azul/">Estratégia do Oceano Azul</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1300" id="menu-item-1300"><a href="https://vidadeviajante.com.br/worldventures/dreamtrips-oceano-azul/">Aqui é o Oceano Azul</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2057" id="menu-item-2057"><a href="https://vidadeviajante.com.br/worldventures/links/">Links da Empresa</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-393" id="menu-item-393"><a>DreamTrips</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-444" id="menu-item-444"><a href="https://vidadeviajante.com.br/dreamtrips-clube-vip-de-viagens/beneficios-do-clube/">Benefícios do Clube</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2273" id="menu-item-2273"><a href="https://vidadeviajante.com.br/dreamtrips-clube-vip-de-viagens/comparacoes-de-precos/">Comparações de Preços</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2434" id="menu-item-2434"><a href="https://vidadeviajante.com.br/dreamtrips-clube-vip-de-viagens/comparacoes-de-precos/hoteis-no-brasil-pela-dreamtrips/">Hotéis no Brasil</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2433" id="menu-item-2433"><a href="https://vidadeviajante.com.br/dreamtrips-clube-vip-de-viagens/comparacoes-de-precos/hoteis-no-mundo-pela-dreamtrips/">Hotéis no Mundo</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2591" id="menu-item-2591"><a href="https://vidadeviajante.com.br/dreamtrips-clube-vip-de-viagens/comparacoes-de-precos/cruzeiros/">Cruzeiros</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1774" id="menu-item-1774"><a href="https://vidadeviajante.com.br/dreamtrips-clube-vip-de-viagens/tour-pela-plataforma-dreamtrips/">Tour pela Plataforma</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-904" id="menu-item-904"><a href="https://vidadeviajante.com.br/dreamtrips-clube-vip-de-viagens/planos-dreamtrips/">Planos DreamTrips</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1736" id="menu-item-1736"><a href="https://vidadeviajante.com.br/dreamgiver-saving-card/">DreamGiver</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-973" id="menu-item-973"><a>Sobre Nós</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-974" id="menu-item-974"><a href="https://vidadeviajante.com.br/sobre-nos/somos-todos-um/">Somos Todos Um</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1005" id="menu-item-1005"><a href="https://vidadeviajante.com.br/sobre-nos/nosso-mindset/">Nosso Mindset</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1615" id="menu-item-1615"><a href="https://vidadeviajante.com.br/sobre-nos/nossos-diretores/">One Big Team</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1092" id="menu-item-1092"><a href="https://vidadeviajante.com.br/sobre-nos/depoimentos-de-membros-worldventures-e-dreamtrips/">Depoimentos</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1960" id="menu-item-1960"><a>Tome Consciência</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1955" id="menu-item-1955"><a href="https://vidadeviajante.com.br/you-should-be-here/resumos-de-livros/">Microbooks em Áudio</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1831" id="menu-item-1831"><a href="https://vidadeviajante.com.br/pra-voce/insights-e-reflexoes/">Insights e Reflexões</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-432" id="menu-item-432"><a href="https://vidadeviajante.com.br/faq/">FAQ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-683" id="menu-item-683"><a href="https://vidadeviajante.com.br/quero-fazer-parte/">[COMO FAZER PARTE]</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1895" id="menu-item-1895"><a href="https:/vidadeviajante.com.br/blog">BLOG</a></li>
</ul></div></aside><aside class="widget widget_categories" id="categories-3"><h2 class="widget-title">Posts sobre:</h2>
<ul>
<li class="cat-item cat-item-18"><a href="https://vidadeviajante.com.br/ysbh/gente-de-visao/">Gente de Visão</a>
</li>
<li class="cat-item cat-item-11"><a href="https://vidadeviajante.com.br/ysbh/nossa-jornada/">Nossa Jornada</a>
</li>
<li class="cat-item cat-item-4"><a href="https://vidadeviajante.com.br/ysbh/worldventures/">WorldVentures</a>
</li>
</ul>
</aside><aside class="widget widget_media_image" id="media_image-8"><a href="https://vidadeviajante.com.br/quero-fazer-parte/"><img alt="" class="image wp-image-2673 attachment-full size-full" height="133" loading="lazy" src="https://vidadeviajante.com.br/wp-content/uploads/2020/06/Quero-saber-mais-7.png" style="max-width: 100%; height: auto;" width="400"/></a></aside>
</div><!-- #secondary -->
</div><!-- .sidebar-wrap -->
</div><!-- .container -->
</div><!-- .site-content -->
<footer id="footer" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
<div class="footer-widget-wrap"><div class="container"><div class="footer-widget col-xs-12 col-sm-4">
<aside class="widget footer-widget-footer widget_recent_entries" id="recent-posts-2">
<h1 class="widget-title">BLOG:</h1>
<ul>
<li>
<a href="https://vidadeviajante.com.br/cristiane-santos-de-servidora-publica-a-viajante-profissional/">Cristiane Santos: de Servidora Pública a Viajante Profissional</a>
</li>
<li>
<a href="https://vidadeviajante.com.br/estela-alves-de-bancaria-a-viajante-profissional/">Estela Alves: de Bancária a Viajante Profissional</a>
</li>
<li>
<a href="https://vidadeviajante.com.br/monick-porto-da-area-de-instrumentacao-cirurgica-a-viajante-profissional/">Monick Porto: da área de Instrumentação Cirúrgica a Viajante Profissional</a>
</li>
</ul>
</aside></div><div class="footer-widget col-xs-12 col-sm-4"><aside class="widget footer-widget-footer widget_categories" id="categories-2"><h1 class="widget-title">Posts Sobre:</h1>
<ul>
<li class="cat-item cat-item-18"><a href="https://vidadeviajante.com.br/ysbh/gente-de-visao/">Gente de Visão</a>
</li>
<li class="cat-item cat-item-11"><a href="https://vidadeviajante.com.br/ysbh/nossa-jornada/">Nossa Jornada</a>
</li>
<li class="cat-item cat-item-4"><a href="https://vidadeviajante.com.br/ysbh/worldventures/">WorldVentures</a>
</li>
</ul>
</aside></div><div class="footer-widget col-xs-12 col-sm-4"><aside class="widget footer-widget-footer widget_tag_cloud" id="tag_cloud-2"><h1 class="widget-title">TAGs</h1><div class="tagcloud"><a aria-label="Desenvolvimento Pessoal (9 itens)" class="tag-cloud-link tag-link-13 tag-link-position-1" href="https://vidadeviajante.com.br/you-should-be-here/desenvolvimento-pessoal/" style="font-size: 20.564102564103pt;">Desenvolvimento Pessoal</a>
<a aria-label="Economia Colaborativa (1 item)" class="tag-cloud-link tag-link-8 tag-link-position-2" href="https://vidadeviajante.com.br/you-should-be-here/economia-colaborativa/" style="font-size: 8pt;">Economia Colaborativa</a>
<a aria-label="Entrevistas (11 itens)" class="tag-cloud-link tag-link-19 tag-link-position-3" href="https://vidadeviajante.com.br/you-should-be-here/entrevistas/" style="font-size: 22pt;">Entrevistas</a>
<a aria-label="Eventos (2 itens)" class="tag-cloud-link tag-link-6 tag-link-position-4" href="https://vidadeviajante.com.br/you-should-be-here/eventos/" style="font-size: 11.230769230769pt;">Eventos</a>
<a aria-label="Finanças (1 item)" class="tag-cloud-link tag-link-17 tag-link-position-5" href="https://vidadeviajante.com.br/you-should-be-here/financas/" style="font-size: 8pt;">Finanças</a>
<a aria-label="MindSet (2 itens)" class="tag-cloud-link tag-link-12 tag-link-position-6" href="https://vidadeviajante.com.br/you-should-be-here/mindset/" style="font-size: 11.230769230769pt;">MindSet</a>
<a aria-label="Premiações (2 itens)" class="tag-cloud-link tag-link-9 tag-link-position-7" href="https://vidadeviajante.com.br/you-should-be-here/premiacoes/" style="font-size: 11.230769230769pt;">Premiações</a>
<a aria-label="Produtividade (3 itens)" class="tag-cloud-link tag-link-16 tag-link-position-8" href="https://vidadeviajante.com.br/you-should-be-here/produtividade/" style="font-size: 13.384615384615pt;">Produtividade</a>
<a aria-label="Resumos de Livros (10 itens)" class="tag-cloud-link tag-link-14 tag-link-position-9" href="https://vidadeviajante.com.br/you-should-be-here/resumos-de-livros/" style="font-size: 21.282051282051pt;">Resumos de Livros</a>
<a aria-label="WorldVentures (6 itens)" class="tag-cloud-link tag-link-5 tag-link-position-10" href="https://vidadeviajante.com.br/you-should-be-here/worldventures/" style="font-size: 17.871794871795pt;">WorldVentures</a></div>
</aside></div></div></div>
<div class="container">
<div class="col-md-12 copyright"><div class="zerif-copyright-box"><a class="zerif-copyright" rel="nofollow">Zerif Lite </a>desenvolvido por <a class="zerif-copyright" href="https://themeisle.com" rel="nofollow" target="_blank">ThemeIsle</a></div></div> </div> <!-- / END CONTAINER -->
</footer> <!-- / END FOOOTER  -->
</div><!-- mobile-bg-fix-whole-site -->
</div><!-- .mobile-bg-fix-wrap -->
<!-- ngg_resource_manager_marker --><!-- Enter your scripts here --><script id="zerif_bootstrap_script-js" src="https://vidadeviajante.com.br/wp-content/themes/zerif-lite/js/bootstrap.min.js?ver=1.8.5.49" type="text/javascript"></script>
<script id="zerif_knob_nav-js" src="https://vidadeviajante.com.br/wp-content/themes/zerif-lite/js/jquery.knob.js?ver=1.8.5.49" type="text/javascript"></script>
<script id="zerif_smoothscroll-js" src="https://vidadeviajante.com.br/wp-content/themes/zerif-lite/js/smoothscroll.js?ver=1.8.5.49" type="text/javascript"></script>
<script id="zerif_scrollReveal_script-js" src="https://vidadeviajante.com.br/wp-content/themes/zerif-lite/js/scrollReveal.js?ver=1.8.5.49" type="text/javascript"></script>
<script id="zerif_script-js" src="https://vidadeviajante.com.br/wp-content/themes/zerif-lite/js/zerif.js?ver=1.8.5.49" type="text/javascript"></script>
<script id="wp-embed-js" src="https://vidadeviajante.com.br/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
</body>
</html>
