<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<title>Bob Jones University | Accredited Christian Liberal Arts University</title>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="max-snippet:-1, max-image-preview:large, max-video-preview:-1, noodp" name="robots"/>
<script async="" type="text/javascript">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PFLQLH8');
_gaq={push:function(){ga('send','pageview',arguments[0][1]);}}</script>
<link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,400i,600|Source+Sans+Pro:300,400,400i,600" rel="stylesheet"/>
<meta content="christian university, christian education, christian college, bible college, private christian school, south carolina colleges, seminary, greenville, south carolina, liberal arts" name="keywords"/>
<meta content="Located in Greenville, South Carolina, Bob Jones University is an accredited Christian liberal arts university focused on educating students to reflect and serve Christ." name="description"/>
<style type="text/css">
.collage-primary article.collage-article.ab-1 {background-image: url('/images/banners/new/20140206prayer-group-de04-320.jpg');}
@media (min-width: 321px)  {.collage-primary article.collage-article.ab-1 {background-image: url('/images/banners/new/20140206prayer-group-de04-500.jpg');}}
@media (min-width: 500px)  {.collage-primary article.collage-article.ab-1 {background-image: url('/images/banners/new/20140206prayer-group-de04-650.jpg');}}
@media (min-width: 650px)  {.collage-primary article.collage-article.ab-1 {background-image: url('/images/banners/new/20140206prayer-group-de04-800.jpg');}}
@media (min-width: 800px)  {.collage-primary article.collage-article.ab-1 {background-image: url('/images/banners/new/20140206prayer-group-de04-768.jpg');}}
@media (min-width: 1200px) {.collage-primary article.collage-article.ab-1 {background-image: url('/images/banners/new/20140206prayer-group-de04-1024.jpg');}}
@media (min-width: 1600px) {.collage-primary article.collage-article.ab-1 {background-image: url('/images/banners/new/20140206prayer-group-de04-1280.jpg');}}
.collage-primary article.collage-article.ab-2 {background-image: url('/images/banners/new/20201116class-dr20-800.jpg');}
@media (min-width: 321px)  {.collage-primary article.collage-article.ab-2 {background-image: url('/images/banners/new/20201116class-dr20-800.jpg');}}
@media (min-width: 500px)  {.collage-primary article.collage-article.ab-2 {background-image: url('/images/banners/new/20201116class-dr20-800.jpg');}}
@media (min-width: 650px)  {.collage-primary article.collage-article.ab-2 {background-image: url('/images/banners/new/20201116class-dr20-800.jpg');}}
@media (min-width: 800px)  {.collage-primary article.collage-article.ab-2 {background-image: url('/images/banners/new/20201116class-dr20-768.jpg');}}
@media (min-width: 1200px) {.collage-primary article.collage-article.ab-2 {background-image: url('/images/banners/new/20201116class-dr20-1024.jpg');}}
@media (min-width: 1600px) {.collage-primary article.collage-article.ab-2 {background-image: url('/images/banners/new/20201116class-dr20.jpg');}}
</style> <meta content="113091022136975" property="fb:app_id"/>
<meta content="bjuedu" property="fb:pages"/>
<meta content="en_US" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="Bob Jones University" property="og:title"/>
<meta content="https://www.bju.edu/index.php" property="og:url"/>
<meta content="https://www.bju.edu/images/ogp.jpg" property="og:image"/>
<meta content="https://www.bju.edu/images/ogp.jpg" property="og:image:secure_url"/>
<meta content="1437" property="og:image:width"/>
<meta content="734" property="og:image:height"/>
<meta content="Bob Jones University" property="og:site_name"/>
<meta content="Located in Greenville, South Carolina, Bob Jones University is an accredited Christian liberal arts university focused on educating students to reflect and serve Christ." property="og:description"/>
<meta content="https://www.bju.edu/images/ogp.jpg" name="thumbnail"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@bjuedu" name="twitter:site"/>
<meta content="https://www.bju.edu/images/ogp.jpg" name="twitter:image"/>
<meta content="Located in Greenville, South Carolina, Bob Jones University is an accredited Christian liberal arts university focused on educating students to reflect and serve Christ." name="twitter:description"/>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "Bob Jones University",
  "url": "https://www.bju.edu",
  "logo": "https://www.bju.edu/images/bju_logo_512.png",
  "contactPoint": [{
    "@type": "ContactPoint",
    "telephone": "+1-864-242-5100",
    "contactType": "Customer Service",
    "areaServed": "US",
    "availableLanguage": "English"
  }],
  "sameAs": [
    "https://www.facebook.com/bjuedu",
    "https://twitter.com/bjuedu",
    "https://www.instagram.com/bjuedu/",
    "https://www.youtube.com/user/BobJonesUniv"
  ]
}
</script>
<meta content="GWc1EfBvDoZA2g6mmjuaM_akFDCbzLNazPldlBkqfzc" name="google-site-verification"/>
<meta content="623C88A3A5263703EC1D802C4FE676D1" name="msvalidate.01"/>
<link href="https://feeds2.feedburner.com/BJUNews" rel="alternate" title="BJU News" type="application/rss+xml"/>
<meta content="True" name="HandheldFriendly"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<link href="/images/favicon/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/images/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/images/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/images/favicon/site.webmanifest" rel="manifest"/>
<link color="#00b5ef" href="/images/favicon/safari-pinned-tab.svg" rel="mask-icon"/>
<link href="/images/favicon/favicon.ico" rel="shortcut icon"/>
<meta content="#2b5797" name="msapplication-TileColor"/>
<meta content="/images/favicon/browserconfig.xml" name="msapplication-config"/>
<meta content="#ffffff" name="theme-color"/>
<script src="/inc/js/jquery-3.5.1.min.js" type="text/javascript"></script>
<link href="/inc/css/style.css?20200914001" media="screen, print" rel="stylesheet" type="text/css"/>
</head>
<body class="home no-js">
<noscript>
<img height="1" src="https://www.facebook.com/tr?id=1401142010168915&amp;ev=PageView&amp;noscript=1" style="display:none;" width="1"/>
<iframe height="0" src="https://www.googletagmanager.com/ns.html?id=GTM-PFLQLH8" style="display:none;visibility:hidden;" width="0"></iframe>
</noscript>
<div id="skiptocontent">
<p><a href="#body">Skip to main content</a></p>
</div>
<svg style="display: none;" xmlns="https://www.w3.org/2000/svg">
<defs>
<symbol id="magnifying-glass" viewbox="0 0 32 32">
<path d="M20.1,23.1c-2.1,1.5-4.6,2.3-7.4,2.3C5.7,25.4,0,19.7,0,12.7S5.7,0,12.7,0s12.7,5.7,12.7,12.7 c0,2.7-0.9,5.3-2.3,7.4l8.3,8.3c0.8,0.8,0.8,2.1,0,3l0,0c-0.8,0.8-2.1,0.8-3,0L20.1,23.1L20.1,23.1z M12.7,22.5 c5.4,0,9.7-4.3,9.7-9.7s-4.3-9.7-9.7-9.7S3,7.4,3,12.8S7.3,22.5,12.7,22.5L12.7,22.5z"></path>
</symbol>
<symbol id="arrow-down" viewbox="0 0 32 32">
<polygon points="30.1,9.6 32,10.9 16,22.5 0,10.9 1.9,9.6 16,19.7 "></polygon>
</symbol>
<symbol id="x-close" viewbox="0 0 32 32">
<path d="M18.308,16.003L31.445,2.939c0.63-0.623,0.63-1.633,0-2.254 c-0.628-0.623-1.649-0.623-2.277,0L16.042,13.738L2.796,0.49c-0.628-0.63-1.649-0.63-2.277,0c-0.628,0.631-0.628,1.653,0,2.283 l13.237,13.238L0.471,29.222c-0.628,0.623-0.628,1.633,0,2.254c0.628,0.623,1.649,0.623,2.277,0l13.274-13.2l13.194,13.195 c0.628,0.63,1.649,0.63,2.277,0c0.628-0.631,0.628-1.653,0-2.283L18.308,16.003z"></path>
</symbol>
<symbol id="gift" viewbox="0 0 32 32">
<path d="M32,8h-7.436C26.515,7.156,28,5.541,28,3.333C28,1.246,26.674,0,24.454,0c-2.808,0-4.125,2.274-5.287,4.28 C18.218,5.918,17.398,7.333,16,7.333c-1.399,0-2.218-1.415-3.167-3.053C11.671,2.274,10.354,0,7.546,0C5.326,0,4,1.246,4,3.333 C4,5.541,5.485,7.156,7.436,8H0v7.333h1.333V32H12h8h10.667V15.333H32V8z M20.321,4.949c1.077-1.859,2.094-3.615,4.133-3.615 c1.468,0,2.213,0.673,2.213,2c0,2.504-2.712,4-5.333,4h-2.609C19.333,6.654,19.831,5.795,20.321,4.949z M5.333,3.333 c0-1.327,0.744-2,2.213-2c2.039,0,3.056,1.756,4.133,3.615c0.49,0.846,0.988,1.706,1.596,2.385h-2.609 C8.045,7.333,5.333,5.837,5.333,3.333z M1.333,9.333H12V14H1.333V9.333z M2.667,30.667V15.333H12v15.333H2.667z M13.333,30.667 V9.333h5.333v21.333H13.333z M29.333,30.667H20V15.333h9.333V30.667z M30.667,14H20V9.333h10.667V14z"></path>
</symbol>
<symbol id="action-info" viewbox="0 0 55 55">
<path d="M24.303,23.334c-1.883,1.503-3.035,2.692-3.453,3.567c0,0.153,0.133,0.419,0.399,0.799 c0.266,0.381,0.437,0.571,0.514,0.571c0.38-0.457,0.971-1.046,1.77-1.77c0.799-0.722,1.427-1.084,1.883-1.084 c0.38,0,0.571,0.457,0.571,1.37c0,0.419-0.077,1.027-0.228,1.826l-2.226,11.359c-0.305,1.561-0.457,2.759-0.457,3.596 c0,0.99,0.237,1.76,0.713,2.312c0.476,0.551,1.075,0.827,1.798,0.827c1.37,0,3.026-0.779,4.966-2.34 c1.941-1.56,3.14-2.797,3.596-3.71c0-0.152-0.134-0.409-0.399-0.771c-0.267-0.361-0.438-0.542-0.514-0.542 c-0.191,0.191-0.485,0.495-0.885,0.913c-0.399,0.42-0.923,0.885-1.569,1.399c-0.648,0.514-1.162,0.77-1.542,0.77 c-0.38,0-0.57-0.475-0.57-1.427c0-0.57,0.037-1.027,0.114-1.369l2.34-11.816c0.304-1.484,0.457-2.701,0.457-3.653 c0-0.95-0.238-1.702-0.714-2.254s-1.056-0.828-1.741-0.828C27.795,21.079,26.188,21.831,24.303,23.334z M28.213,9.492 c-0.723,0.799-1.084,1.732-1.084,2.797c0,0.837,0.247,1.551,0.742,2.14c0.494,0.591,1.122,0.885,1.883,0.885 c1.028,0,1.912-0.418,2.654-1.256c0.742-0.836,1.113-1.807,1.113-2.911c0-0.799-0.256-1.474-0.77-2.026 C32.237,8.57,31.6,8.293,30.84,8.293C29.812,8.293,28.936,8.693,28.213,9.492z"></path>
</symbol>
<symbol id="action-visit" viewbox="0 0 55 55">
<path d="M41.363,18.022C41.363,10.092,34.935,3.66,27,3.66c-7.932,0-14.363,6.431-14.363,14.362 c0,2.62,0.714,5.067,1.938,7.182h-0.016L26.998,50.34l12.447-25.135h-0.018C40.655,23.09,41.363,20.642,41.363,18.022 M27.002,25.202c-3.966,0-7.182-3.215-7.182-7.181c0-3.964,3.216-7.182,7.182-7.182c3.969,0,7.181,3.218,7.181,7.182 C34.183,21.987,30.971,25.202,27.002,25.202"></path>
</symbol>
<symbol id="action-contact" viewbox="0 0 55 55">
<path d="M12.189,47.782c-0.491,0-0.773-0.019-0.818-0.019l0,0c0,0,5.37-4.495,6.588-10.719l0,0 c-8.271-2.288-14.56-7.701-14.658-14.807l0,0C3.521,12.722,14.422,6.248,27,6.218l0,0c12.574,0.03,23.48,6.504,23.699,16.019l0,0 c-0.113,7.639-7.346,13.319-16.549,15.268l0,0c-8.57,9.481-18.891,10.277-21.895,10.277l0,0 C12.232,47.782,12.21,47.782,12.189,47.782L12.189,47.782z M12.687,13.826c-3.581,2.338-5.513,5.324-5.509,8.411l0,0 c-0.099,4.872,5.211,9.798,13.419,11.553l0,0l1.685,0.363l-0.172,1.675c-0.144,1.78-0.808,4.472-2.684,6.931l0,0 c3.703-1.204,8.189-3.571,12.21-8.202l0,0l0.438-0.508l0.667-0.127c8.568-1.618,14.192-6.677,14.081-11.685l0,0 c0.004-3.087-1.931-6.073-5.509-8.411l0,0C37.755,11.51,32.661,10.002,27,10.009l0,0c-0.015,0-0.029,0-0.043,0l0,0 C21.314,10.009,16.236,11.516,12.687,13.826L12.687,13.826z"></path>
</symbol>
<symbol id="action-apply" viewbox="0 0 55 55">
<path d="M54.43,26.817c-0.288,0.26-3.509,3.261-3.509,3.261l-2.787-3.098c0,0,3.253-3.031,3.53-3.281 c0.45-0.404,1.26-0.676,1.909,0.045c0.439,0.487,0.576,0.641,1.088,1.21C55.205,25.559,55.071,26.241,54.43,26.817z M42.845,4.138 H11.926v39.165h14.057l-4.91,4.123H7.803V0.016h39.165v24.163l-4.123,3.757V4.138z M27.647,51.108l-7.475,3.876l4.674-6.989 L27.647,51.108z M36.661,27.844H18.11v-4.123h18.551V27.844z M18.11,9.292h18.551v4.122H18.11V9.292z M36.661,20.629H18.11v-4.123 h18.551V20.629z M46.505,28.549l2.787,3.098L29.493,49.253l-2.788-3.099L46.505,28.549z"></path>
</symbol>
<symbol id="social-facebook" viewbox="0 0 130 130">
<path d="M 55.00,125.00 C 55.00,125.00 55.00,82.00 55.00,82.00 55.00,82.00 41.00,82.00 41.00,82.00 41.00,82.00 41.00,66.00 41.00,66.00 41.00,66.00 55.00,66.00 55.00,66.00 55.00,50.44 54.69,33.16 74.00,29.06 78.53,28.18 86.79,29.00 92.00,29.06 92.00,29.06 92.00,44.00 92.00,44.00 88.43,44.00 82.19,43.60 79.13,45.31 73.44,48.50 75.00,60.21 75.00,66.00 75.00,66.00 92.00,66.00 92.00,66.00 92.00,66.00 89.00,82.00 89.00,82.00 89.00,82.00 75.00,82.00 75.00,82.00 75.00,82.00 75.00,125.00 75.00,125.00 82.95,124.82 90.23,121.85 97.00,117.79 124.14,101.51 133.21,67.45 120.56,39.00 112.07,19.91 91.10,5.10 70.00,5.00 70.00,5.00 60.00,5.00 60.00,5.00 52.99,5.09 47.46,6.59 41.00,9.21 -4.08,27.44 -8.24,91.11 32.00,116.91 38.86,121.31 46.82,124.62 55.00,125.00 Z"></path>
</symbol>
<symbol id="social-twitter" viewbox="0 0 32 32">
<path d="M28.724,9.474c0.012,0.281,0.019,0.565,0.019,0.849c0,8.675-6.603,18.679-18.678,18.679c-3.708,0-7.158-1.086-10.064-2.95 c0.514,0.061,1.036,0.092,1.567,0.092c3.075,0,5.906-1.05,8.153-2.811c-2.872-0.052-5.297-1.951-6.132-4.558 c0.401,0.076,0.812,0.117,1.235,0.117c0.598,0,1.179-0.08,1.73-0.23c-3.003-0.603-5.266-3.256-5.266-6.437c0-0.028,0-0.055,0-0.083 c0.885,0.492,1.898,0.787,2.974,0.822C2.499,11.786,1.34,9.777,1.34,7.499c0-1.202,0.324-2.33,0.889-3.3 c3.238,3.972,8.075,6.585,13.532,6.86c-0.112-0.481-0.17-0.982-0.17-1.496c0-3.626,2.94-6.565,6.565-6.565 c1.888,0,3.594,0.797,4.792,2.073c1.496-0.295,2.901-0.841,4.169-1.594c-0.49,1.533-1.531,2.819-2.887,3.632 C29.557,6.951,30.823,6.598,32,6.075C31.12,7.392,30.007,8.548,28.724,9.474z"></path>
</symbol>
<symbol id="social-instagram" viewbox="0 0 32 32">
<g>
<path d="M15.994,2.836c4.273,0,4.775,0.019,6.463,0.095c1.562,0.07,2.406,0.33,2.971,0.552c0.749,0.292,1.283,0.635,1.841,1.194 s0.908,1.092,1.194,1.841c0.216,0.565,0.483,1.41,0.552,2.971c0.076,1.689,0.095,2.19,0.095,6.463s-0.019,4.775-0.095,6.463 c-0.07,1.562-0.33,2.406-0.552,2.971c-0.292,0.749-0.635,1.283-1.194,1.841s-1.092,0.908-1.841,1.194 c-0.565,0.216-1.41,0.483-2.971,0.552c-1.689,0.076-2.19,0.095-6.463,0.095s-4.775-0.019-6.463-0.095 c-1.562-0.07-2.406-0.33-2.971-0.552c-0.749-0.292-1.283-0.635-1.841-1.194s-0.908-1.092-1.194-1.841 c-0.216-0.565-0.483-1.41-0.552-2.971c-0.076-1.689-0.095-2.19-0.095-6.463s0.019-4.775,0.095-6.463 c0.07-1.562,0.33-2.406,0.552-2.971c0.292-0.749,0.635-1.283,1.194-1.841S5.81,3.769,6.559,3.483 c0.565-0.216,1.41-0.483,2.971-0.552C11.219,2.848,11.721,2.836,15.994,2.836 M15.994-0.047c-4.343,0-4.889,0.019-6.597,0.095 C7.695,0.125,6.533,0.398,5.517,0.791C4.463,1.198,3.575,1.75,2.686,2.639S1.251,4.423,0.838,5.471 C0.444,6.487,0.171,7.648,0.095,9.356C0.019,11.058,0,11.604,0,15.947s0.019,4.889,0.095,6.597 c0.076,1.702,0.349,2.863,0.743,3.886c0.406,1.054,0.959,1.943,1.848,2.832c0.889,0.889,1.784,1.435,2.832,1.848 c1.016,0.394,2.178,0.667,3.886,0.743s2.248,0.095,6.597,0.095s4.889-0.019,6.597-0.095c1.702-0.076,2.863-0.349,3.886-0.743 c1.054-0.406,1.943-0.959,2.832-1.848c0.889-0.889,1.435-1.784,1.848-2.832c0.394-1.016,0.667-2.178,0.743-3.886 C31.981,20.836,32,20.296,32,15.947s-0.019-4.889-0.095-6.597c-0.076-1.702-0.349-2.863-0.743-3.886 c-0.406-1.054-0.959-1.943-1.848-2.832c-0.889-0.889-1.784-1.435-2.832-1.848c-1.016-0.394-2.178-0.667-3.886-0.743 C20.883-0.028,20.337-0.047,15.994-0.047L15.994-0.047z"></path>
<path d="M15.994,7.731c-4.533,0-8.216,3.676-8.216,8.216s3.683,8.216,8.216,8.216s8.216-3.683,8.216-8.216 S20.527,7.731,15.994,7.731z M15.994,21.28c-2.946,0-5.333-2.387-5.333-5.333s2.387-5.333,5.333-5.333s5.333,2.387,5.333,5.333 S18.94,21.28,15.994,21.28z"></path>
<circle cx="24.533" cy="7.407" r="1.917"></circle>
</g>
</symbol>
<symbol id="social-youtube" viewbox="0 0 32 32">
<path d="M31.67,9.179c0,0-0.312-2.353-1.271-3.389c-1.217-1.358-2.58-1.366-3.205-1.443C22.717,4,16.002,4,16.002,4h-0.015 c0,0-6.715,0-11.191,0.347C4.171,4.424,2.809,4.432,1.591,5.79C0.633,6.826,0.32,9.179,0.32,9.179S0,11.94,0,14.701v2.588 c0,2.763,0.32,5.523,0.32,5.523s0.312,2.352,1.271,3.386c1.218,1.358,2.815,1.317,3.527,1.459C7.677,27.919,15.995,28,15.995,28 s6.722-0.012,11.199-0.355c0.625-0.08,1.988-0.088,3.205-1.446c0.958-1.034,1.271-3.386,1.271-3.386s0.32-2.761,0.32-5.523v-2.588 C31.99,11.94,31.67,9.179,31.67,9.179z M12,22V10l10,6L12,22z"></path>
</symbol>
</defs>
</svg>
<div id="globalwrapper">
<nav id="navigation">
<div class="nav-logo">
<a href="/"><img alt="Bob Jones University" src="/images/logo-main.svg"/></a>
</div>
<div class="nav-icon">
<button title="Open Navigation"><span></span></button>
</div>
<div class="nav-menus">
<div class="nav-main">
<div class="nav-search">
<form action="/search/" method="get">
<input class="search" name="q" placeholder="Search" type="search"/>
</form>
<ul>
<li><a href="/site-index.php">A-Z Index</a></li>
<li><a href="/contact/directory.php">Directory</a></li>
</ul>
</div>
<ul>
<li class="admission haschildren">
<a href="/admission/">Admission &amp; Aid</a>
<div class="subnavtoggle">
<a href="javascript:;">
<svg><use xlink:href="#arrow-down"></use></svg>
</a>
</div>
<div class="dropdown">
<ul>
<li><a href="/admission/apply/">Apply</a></li>
<li><a href="/admission/tuition-aid/">Tuition &amp; Aid</a></li>
<li><a href="/admission/visit/">Visit</a></li>
<li><a href="/admission/request-info.php">Request Info</a></li>
<li><a href="/admission/counselors/">Admission Counselors</a></li>
<li><a href="/admission/admitted-students/">Admitted Students</a></li>
</ul>
</div>
</li>
<li class="academics haschildren">
<a href="/academics/">Academics</a>
<div class="subnavtoggle">
<a href="javascript:;">
<svg><use xlink:href="#arrow-down"></use></svg>
</a>
</div>
<div class="dropdown">
<ul>
<li><a href="/academics/programs/">Programs of Study</a></li>
<li><a href="/academics/academic-experience.php">Academic Experience</a></li>
<li><a href="/academics/faculty/">Faculty</a></li>
<li><a href="/academics/bjuonline/">Online Learning</a></li>
<li><a href="/academics/resources-support/">Resources &amp; Support</a></li>
<li><a href="/academics/accreditation.php">Accreditation</a></li>
<li><a href="/academics/resources-support/transcripts/">Request a Transcript</a></li>
</ul>
</div>
</li>
<li class="life haschildren">
<a href="/life-faith/">Life &amp; Faith</a>
<div class="subnavtoggle">
<a href="javascript:;">
<svg><use xlink:href="#arrow-down"></use></svg>
</a>
</div>
<div class="dropdown">
<ul>
<li><a href="/life-faith/faith-worship/">Faith &amp; Worship</a></li>
<li><a href="/life-faith/get-involved/">Get Involved</a></li>
<li><a href="/life-faith/housing-dining/">Housing &amp; Dining</a></li>
<li><a href="/life-faith/support/">Student Support</a></li>
<li><a href="/life-faith/greenville/">Life in Greenville, S.C.</a></li>
</ul>
</div>
</li>
<li class="about haschildren">
<a href="/about/">About</a>
<div class="subnavtoggle">
<a href="javascript:;">
<svg><use xlink:href="#arrow-down"></use></svg>
</a>
</div>
<div class="dropdown">
<ul>
<li><a href="/about/fast-facts.php">BJU at a Glance</a></li>
<li><a href="/about/campus-map/">Campus Map</a></li>
<li><a href="/about/creed-mission.php">Creed and Mission</a></li>
<li><a href="/about/university-leadership/">University Leadership</a></li>
<li><a href="/about/history.php">Our History</a></li>
<li><a href="https://bju.careers" target="_blank">Careers at BJU</a></li>
</ul>
</div>
</li>
<li class="events haschildren">
<a href="/events/">Events</a>
<div class="subnavtoggle">
<a href="javascript:;">
<svg><use xlink:href="#arrow-down"></use></svg>
</a>
</div>
<div class="dropdown">
<ul>
<li><a href="/events/calendar/">Calendar</a></li>
<li><a href="/events/chapel/">Chapel</a></li>
<li><a href="/events/fine-arts/">Fine Arts</a></li>
<li><a href="/events/commencement/">Commencement</a></li>
<li><a href="/events/youth/">Youth Events</a></li>
<li><a href="/events/near-you/">Near You</a></li>
<li><a href="/live/">Webcasts</a></li>
</ul>
</div>
</li>
<li class="athletics">
<a href="https://www.bjubruins.com/">Athletics</a>
</li>
<li class="search"><a href="javascript:;">
<svg class="magnifying-glass"><use xlink:href="#magnifying-glass"></use></svg>
<svg class="close"><use xlink:href="#x-close"></use></svg>
</a></li>
</ul>
</div>
<div class="nav-secondary">
<ul class="nav-secondary-audiences">
<li><a href="/students/">Students</a></li>
<li><a href="/parents/">Parents</a></li>
<li><a href="https://bjualumni.com/" target="_blank">Alumni</a></li>
<li><a href="https://bjualumni.com/current-results/" target="_blank">Donors</a></li>
</ul>
<ul class="nav-secondary-actions">
<li><a href="/admission/visit/">Visit</a></li>
<li><a href="/admission/apply/">Apply</a></li>
<li><a href="/admission/request-info.php">Request Info</a></li>
</ul>
</div>
</div>
</nav>
<div id="wrapper">
<div id="body">
<div id="main">
<div class="clearfix" id="collage">
<div class="collage-primary">
<article class="collage-article ab-2">
<a data-analytics="2019-09-programs" href="/academics/programs/">
<div class="collage-overlay"></div>
<div class="collage-article-heading">
<p>Flexible Programs. Life Preparation.</p>
</div>
</a>
</article>
</div>
<div class="collage-secondary">
<div class="collage-secondary-top">
<article class="collage-article rank">
<a data-analytics="2020-09-ranking" href="/news/2020-09-14-ranking.php">
<div class="collage-overlay"></div>
<div class="collage-article-heading">
<p>A Best Value College</p>
</div>
</a>
</article>
<article class="collage-article financial-aid">
<a data-analytics="2019-01-scholarshipsgry" href="/admission/tuition-aid/grants-scholarships.php">
<div class="collage-overlay"></div>
<div class="collage-article-heading">
<p>2021-22 Grants &amp; Scholarships</p>
</div>
</a>
</article>
</div>
<div class="collage-secondary-bottom">
<div class="collage-blocks">
<div class="collage-block visit-bju">
<a data-analytics="2018-09-visit" href="/admission/visit/">
<div class="collage-overlay"></div>
<p>Visit Us</p>
</a>
</div>
<div class="collage-block financial-aid">
<a data-analytics="2020-10-apply" href="/admission/apply/">
<div class="collage-overlay"></div>
<p>Apply</p>
</a>
</div>
</div>
</div>
</div>
</div>
<p style="text-align:center;padding:.6em 0;background:#e0f6ff;font-size:1.15em"><a href="coronavirus/">› Coronavirus Information</a></p>
<div class="page-width" id="intro">
<p id="tagline">LEARN. LOVE. LEAD.</p>
<p>We are committed to providing an outstanding Christian liberal arts education purposely designed to inspire a lifelong pursuit of learning, loving and leading.</p>
</div>
<div id="study">
<div class="page-width">
<h2>What do you want to study?</h2>
<script src="/inc/js/majorsearch.min.20191118.js"></script>
<div style="max-width: 690px; margin: 0 auto;">
<div id="programsearch" style="margin-top: 0.7em;">
<div id="programinput">
<label for="program">Business, Ministry, Nursing…</label>
<input data-type="all" id="program" name="program" type="text"/>
<span id="programclear"></span>
</div>
<div id="results"></div>
</div>
<p><a class="button clear small" href="/academics/programs/">View all programs</a></p>
</div>
</div>
</div>
<div id="infographics">
<div class="infographics-inner">
<div class="row">
<div class="fourth tablet-half mobile-half">
<figure class="infographic">
<a data-analytics="2016-08-affordable" href="/admission/tuition-aid/">
<p>#2</p>
<figcaption>
<p>Best Value College</p>
</figcaption>
</a>
</figure>
</div>
<div class="fourth tablet-half mobile-half">
<figure class="infographic">
<a data-analytics="2015-12-countries" href="/life-faith/">
<p>40+</p>
<figcaption>
<p>Countries represented in the student body</p>
</figcaption>
</a>
</figure>
</div>
<div class="fourth tablet-half mobile-half">
<figure class="infographic">
<a data-analytics="2015-12-ratio" href="/academics/faculty/">
<p>13:1</p>
<figcaption>
<p>Student to Faculty Ratio</p>
</figcaption>
</a>
</figure>
</div>
<div class="fourth tablet-half mobile-half">
<figure class="infographic">
<a data-analytics="2015-12-aid" href="/admission/tuition-aid/">
<p>94%</p>
<figcaption>
<p>Of new students who apply for aid receive it</p>
</figcaption>
</a>
</figure>
</div>
</div>
</div>
</div>
<div id="newsfeed">
<div class="page-width">
<div class="clearfix" id="newsfeed-container">
<article class="card facebook photo"><a href="https://www.facebook.com/bjuedu/photos/a.10158859014311236/10158859014676236/?type=3&amp;theater" target="_blank"><div class="card-feature"><img src="https://www.bju.edu/galleries/newsfeed/cache/1610472708_82418.jpg"/></div><div class="card-content"><div class="card-content-action"></div><p class="card-content-description">Students move into the dorms as we start the spring semester! | Photo Gallery</p><div class="card-context clearfix"><div class="card-info-source">Bob Jones University</div><div class="card-info-date">Jan 12</div></div></div></a></article>
<article class="card instagram photo"><a href="https://www.instagram.com/p/CJ6MMkwjmXc/" target="_blank"><div class="card-feature"><img src="https://www.bju.edu/galleries/newsfeed/cache/1610378751_18282.jpg"/></div><div class="card-content"><div class="card-content-action"></div><p class="card-content-description">Today’s the day! 🥳🎉 Welcome back to all returning students &amp; a big welcome to all of our new students this semester! 👏 Let’s make it great! #bjuedu</p><div class="card-context clearfix"><div class="card-info-source">@bjuedu</div><div class="card-info-date">Jan 11</div></div></div></a></article>
<article class="card twitter tweet"><a href="https://twitter.com/BJUedu/status/1347621274714050569" target="_blank"><div class="card-feature"><img src="https://www.bju.edu/galleries/newsfeed/cache/1610376846_18807.jpg"/></div><div class="card-content"><div class="card-content-action"></div><p class="card-content-description">Students come back Monday &amp; we can't wait! 🥳🎉 Looking forward to another great semester! ✅</p><div class="card-context clearfix"><div class="card-info-source">@BJUedu</div><div class="card-info-date">Jan 8</div></div></div></a></article>
<article class="card bju news"><a href="https://www.bju.edu/live/"><div class="card-feature"><img src="https://www.bju.edu/galleries/newsfeed/cache/1609882403_91280.jpg"/></div><div class="card-content"><div class="card-content-action"></div><p class="card-content-title">Opening Services Webcast</p><p class="card-content-description">We'll be streaming our opening services with Evangelist Jeremy Frazor on Jan. 12-13.</p><div class="card-context clearfix"><div class="card-info-source">News</div><div class="card-info-date">Jan 5</div></div></div></a></article>
</div>
<p style="text-align: center;"><a class="button secondary" href="/news/">More News &amp; Social Media</a></p>
<script src="/inc/js/isotope.pkgd.min.js"></script>
<script src="/inc/js/imagesloaded.pkgd.min.js"></script>
<script>
		socialIsLoaded = false;
		function loadSocialHighlights() {
			socialIsLoaded = true;
			$('#newsfeed-container').imagesLoaded( function() {
				window.setTimeout(function(){ // sooo hackish... because of a bug i can't figure out
					$('#newsfeed-container').isotope({
						itemSelector: '.card',
						layoutMode: 'masonry',
						transitionDuration: '0.25s'
					});
				}, 1);
			});
			try {
				window.dataLayer.push({
					'event': 'ManGAEvent',
					'eventCategory': 'Home page',
					'eventAction': 'scroll',
					'eventLabel': 'news'
				});
			} catch(e) { }
		}

		$(document).ready(function(){
			// Load social when user scrolls down
			var offset = $('#newsfeed').offset().top - $(window).height() - 100;
			if (offset - $(window).scrollTop() < 0) {
				loadSocialHighlights();
			}
			$(document).scroll(function(){
				if (socialIsLoaded) return;
				if (offset - $(window).scrollTop() < 0) {
					loadSocialHighlights();
				}
			});
		});
		</script>
</div>
</div>
<div id="events">
<div class="page-width">
<div class="row">
<div class="third"><div class="day clearfix"><div class="date-info"><p><a class="blocklink" href="/events/calendar/2021/1/13"><span class="weekday">Wed</span><span class="month">Jan</span><span class="date">13</span></a></p></div>
<div class="event-info"><p><strong>Classes Begin</strong>
</p></div></div></div><div class="third"><div class="day clearfix"><div class="date-info"><p><a class="blocklink" href="/events/calendar/2021/1/13"><span class="weekday">Wed</span><span class="month">Jan</span><span class="date">13</span></a></p></div>
<div class="event-info"><p><strong>Evangelistic Meeting</strong>
<br/>7 p.m., Founder's Memorial Amphitorium</p></div></div></div><div class="third"><div class="day clearfix"><div class="date-info"><p><a class="blocklink" href="/events/calendar/2021/1/18"><span class="weekday">Mon</span><span class="month">Jan</span><span class="date">18</span></a></p></div>
<div class="event-info"><p><strong>Martin Luther King Jr. Day</strong>
</p></div></div></div> </div>
<p class="more-events"><a class="button secondary" href="/events/calendar/">View Calendar</a></p>
</div>
</div>
<script>
// Track clicks on hero links and stats
$(document).ready(function(){
	$('#collage a').click(function(e) {
		e.preventDefault();
		try {
			window.dataLayer.push({
				'event': 'ManGAEvent',
				'eventCategory': 'Home Page Hero',
				'eventAction': 'click',
				'eventLabel': $(this).data('analytics')
			});
		} catch(e) { }
		window.location = $(this).attr('href');
	});
	$('#infographics a').click(function(e) {
		e.preventDefault();
		try {
			window.dataLayer.push({
				'event': 'ManGAEvent',
				'eventCategory': 'Home Page Stats',
				'eventAction': 'click',
				'eventLabel': $(this).data('analytics')
			});
		} catch(e) { }
		window.location = $(this).attr('href');
	});
});

</script>
</div>
</div>
<footer>
<div id="footer-top">
<div id="footer-actions">
<ul>
<li class="info"><a href="/admission/request-info.php"><span class="circle"><svg><use xlink:href="#action-info"></use></svg></span><span class="text">Request Info</span></a></li>
<li class="visit"><a href="/admission/visit/"><span class="circle"><svg><use xlink:href="#action-visit"></use></svg></span><span class="text">Visit</span></a></li>
<li class="apply"><a href="/admission/apply/"><span class="circle"><svg><use xlink:href="#action-apply"></use></svg></span><span class="text">Apply</span></a></li>
<li class="contact"><a href="/contact/"><span class="circle"><svg><use xlink:href="#action-contact"></use></svg></span><span class="text">Contact Us</span></a></li>
</ul>
</div>
</div>
<div class="clearfix" id="footer-bottom">
<div class="footer-meta1">
<div id="footer-search">
<form action="/search/" method="get">
<input name="q" placeholder="Search..." type="search"/>
<svg><use xlink:href="#magnifying-glass"></use></svg>
<input name="cx" type="hidden" value="017456826151234432158:1tysj4acmty"/>
<input name="cof" type="hidden" value="FORID:10"/>
<input name="ie" type="hidden" value="UTF-8"/>
</form>
</div>
</div>
<div class="footer-meta2">
<div id="footer-social">
<ul class="clearfix">
<li class="facebook"><a href="/connect#fb" title="Facebook"><svg><use xlink:href="#social-facebook"></use></svg></a></li>
<li class="twitter"><a href="/connect#tw" title="Twitter"><svg><use xlink:href="#social-twitter"></use></svg></a></li>
<li class="youtube"><a href="/connect#yt" title="YouTube"><svg><use xlink:href="#social-youtube"></use></svg></a></li>
<li class="instagram"><a href="/connect#ig" title="Instagram"><svg><use xlink:href="#social-instagram"></use></svg></a></li>
</ul>
</div>
<p class="copyright"><small><a href="http://bju.careers/" target="_blank">Careers</a> | <a href="/website-policies.php">Policies</a> | <a href="/consumer-information.php">Consumer Info</a> | <a href="https://today.bju.edu/news">Public Relations</a></small></p>
</div>
<div class="footer-meta3">
<p><a href="/"><img alt="Bob Jones University" src="/images/template/footer-logo.png?48"/></a></p>
</div>
</div>
</footer>
</div>
</div>
<script src="/inc/js/scripts.min.js?20201104001" type="text/javascript"></script>
<script src="/inc/js/jquery.reveal.min.20150513.js" type="text/javascript"></script>
<script src="/inc/js/jquery.qtip.min.js" type="text/javascript"></script>
<script type="text/javascript">
	// Lazy load extra CSS files
	var cb = function() {
		var extraCSSFiles = ["\/inc\/css\/print.css","\/inc\/css\/reveal.css","\/inc\/css\/jquery.qtip.min.css"];
		for (var i = 0; i < extraCSSFiles.length; i++) {
			var l = document.createElement('link'); l.rel = 'stylesheet';
			l.href = extraCSSFiles[i];
			if (l.href.search('print') > -1) l.media="print";
			var h = document.getElementsByTagName('head')[0];
			h.appendChild(l);
		};
	};
	var raf = requestAnimationFrame || mozRequestAnimationFrame || webkitRequestAnimationFrame || msRequestAnimationFrame;
	if (raf) raf(cb);
	else window.addEventListener('load', cb);
	</script>
</body>
</html>