<!DOCTYPE html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8"/>
<title>adgoal</title>
<meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport"/>
<link href="/css/main.css" rel="stylesheet"/>
<link href="/favicon/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/favicon/android-icon-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/favicon/manifest.json" rel="manifest"/>
<meta content="/favicon/ms-icon-144x144.png" name="msapplication-TileImage"/>
<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-75151864-1', 'auto');
	  ga('set', 'anonymizeIp', true);
	  ga('send', 'pageview');
	</script>
<link href="https://chrome.google.com/webstore/detail/gmngdhfopilamoapipkbpgceiabacleg" rel="chrome-webstore-item"/>
</head>
<body><style>
/* Style The Dropdown Button */
.dropbtn {

    color: white;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
    position: relative;
    display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {
    display: block;
}
</style>
<div class="header">
<div class="header-meta">
<div class="grid">
<div class="row">
<ul class="list-nav float-right">
<li>
<a href="https://adgoal.freshdesk.com/support/tickets/new" target="_blank" title="Contact">Contact</a>
</li>
<li>
<a href="/en/login.html" title="Login">Login</a>
</li>
<li>
<a href="/de" id="toggle-lang" title="Deutsch">Deutsch</a>
</li>
</ul>
</div>
</div>
</div>
<!-- Ende Meta header -->
<div class="header-main">
<div class="grid">
<div class="row">
<a class="logo" href="/" title="adgoal">adgoal</a>
<a class="trigger-menu" href="#">Trigger Menu</a>
<!-- Import Main Nav -->
<ul class="list-nav float-right" id="mainmenu">
<li>
<a class="" href="/en/publisher.html" title="Publisher">Publisher</a>
</li>
<li class="mobile-only ">
<a class="nav-products " href="/en/products/smartlink.html" title="Products">Products</a>
<div class="header-sub">
<div class="grid">
<div class="row text-center">
<ul class="list-nav">
<li class="mobile-only">
<a class="nav-products" href="#">Zurück</a>
</li>
<li>
<a class="" href="/en/products/smartlink.html" title="adgoal SmartLink">SmartLink</a>
</li>
<li>
<a class="" href="/en/products/smartsocial.html" title="adgoal SmartSocial">SmartSocial</a>
</li>
<li>
<a class="" href="/en/products/universalsearch.html" title="adgoal UniversalSearch">UniversalSearch</a>
</li>
<li>
<a class="" href="/en/products/api.html" title="adgoal API">API</a>
</li>
</ul>
</div>
</div>
</div>
</li>
<li class="hidden-on-mobile">
<div class="dropdown">
<a class="dropbtn" href="javascript:void(0);">Products</a>
<div class="header-sub dropdown-content">
<div class="grid">
<div class="row text-center">
<ul class="list-nav">
<li class="mobile-only">
<a class="nav-products" href="#">Zurück</a>
</li>
<li>
<a class="" href="/en/products/smartlink.html" title="adgoal SmartLink">SmartLink</a>
</li>
<li>
<a class="" href="/en/products/smartsocial.html" title="adgoal SmartSocial">SmartSocial</a>
</li>
<li>
<a class="" href="/en/products/universalsearch.html" title="adgoal UniversalSearch">UniversalSearch</a>
</li>
<li>
<a class="" href="/en/products/api.html" title="adgoal API">API</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</li>
<li>
<a class="" href="/en/about.html" title="About">About</a>
</li>
<li>
<a class="" href="/en/advertiser.html" title="Advertiser">Advertiser</a>
</li>
<li>
<a class="button" href="/en/signup.html" title="Signup">Signup</a>
</li>
</ul>
</div>
</div>
</div>
<!-- Ende Main Nav -->
</div>
<div class="page-content">
<div class="section intro">
<div class="grid">
<div class="row">
<div class="span8 center text-center">
<h1>Make money
                        <br/>
                        with your content.</h1>
</div>
</div>
</div>
<a class="button next-section" href="#section-publisher">Weiter</a>
</div>
<div class="section publisher" id="section-publisher">
<div class="grid">
<div class="row section-intro">
<div class="span8 center text-center">
<h2>
<span>Our services</span>
                        For Publishers                    </h2>
<p class="subline">We focus on optimizing your monetization so you can focus on your content.</p>
</div>
</div>
<div class="row publisher-details">
<div class="span3 connected text-center">
<a class="circle icon-editorial" href="/en/publisher.html#section-blog" title="Learn more"></a>
<h3>For Editorials &amp; Blogs</h3>
<p>Your quality articles offer relevant information on products or sales.</p>
<a class="button" href="/en/publisher.html#section-blog" title="Learn more">Learn more</a>
</div>
<div class="span3 connected text-center">
<a class="circle icon-smd" href="/en/publisher.html#section-social-md" title="Learn more"></a>
<h3>For Social Media</h3>
<p>Your reach out to followers on Facebook, YouTube or Twitter.</p>
<a class="button" href="/en/publisher.html#section-social-md" title="Learn more">Learn more</a>
</div>
<div class="span3 connected text-center">
<a class="circle icon-community" href="/en/publisher.html#section-communities" title="Learn more"></a>
<h3>For Communities</h3>
<p>You run a vital community with a lot of user-generated content.</p>
<a class="button" href="/en/publisher.html#section-communities" title="Learn more">Learn more</a>
</div>
<div class="span3 connected text-center">
<a class="circle icon-marketer" href="/en/publisher.html#section-marketer" title="Learn more"></a>
<h3>For Marketers</h3>
<p>You work with publisher sites and help to monetization their content.</p>
<a class="button" href="/en/publisher.html#section-marketer" title="Learn more">Learn more</a>
</div>
</div>
</div>
<a class="button next-section" href="#section-products">Weiter</a>
</div>
<div class="section contrast products" id="section-products">
<div class="grid">
<div class="row section-intro">
<div class="span8 center text-center">
<h2>
<span>Out products</span>
                        The perfect fit for your content                    </h2>
<p class="subline">Discreet monetization solutions for every publisher concept.</p>
</div>
</div>
</div>
<div class="products-container">
<ul class="section-list highlight">
<li class="span4">
<span class="icon smartlink"></span>
<h3 class="divider">adgoal SmartLink</h3>
<p>Automatically converts links into affiliate links. Userfriendly and set up in 5 Minutes.</p>
<a class="button" href="/en/products/smartlink.html" title="Learn more">Learn more</a>
</li>
<li class="span4">
<span class="icon smartsocial"></span>
<h3 class="divider">adgoal SmartSocial</h3>
<p>Shortened affiliate links on-the-fly for your social media channels.</p>
<a class="button" href="/en/products/smartsocial.html" title="Learn more">Learn more</a>
</li>
<li class="span4">
<span class="icon universalsearch"></span>
<h3 class="divider">adgoal UniversalSearch</h3>
<p>Visual highlighting of search results in all search engines.</p>
<a class="button" href="/en/products/universalsearch.html" title="Learn more">Learn more</a>
</li>
<li class="span4">
<span class="icon api"></span>
<h3 class="divider">adgoal API</h3>
<p>All the power of adgoal in one API. Improve your business model with us.</p>
<a class="button" href="/en/products/api.html" title="Learn more">Learn more</a>
</li>
</ul>
</div>
</div>
<div class="section backgroundimage facts" id="section-facts">
<div class="grid">
<div class="row section-intro">
<div class="span8 center text-center">
<h2>
<span>Interesting</span>
                        Some facts and numbers                    </h2>
<p class="subline">Well connected to almost every advertiser in Europe and around the world.</p>
</div>
</div>
<ul class="section-list row facts-list">
<li class="span4 text-center connected">
<span class="fact" data-from="0" data-speed="3000" data-to="30">30</span>
<span class="dot"></span>
<span class="fact-item">Countries</span>
</li>
<li class="span4 text-center connected">
<span class="fact" data-from="0" data-speed="3000" data-to="60">60</span>
<span class="dot"></span>
<span class="fact-item">Networks</span>
</li>
<li class="span4 text-center connected">
<span class="fact" data-from="0" data-speed="3000" data-to="60000">60.000</span>
<span class="dot"></span>
<span class="fact-item">Affiliate Programs</span>
</li>
</ul>
</div>
<a class="button next-section" href="#section-request">Weiter</a>
</div>
<div class="section request contrast" id="section-request">
<div class="grid">
<div class="row">
<div class="span6 section-intro center">
<h2>
<span>Get in touch</span> Contact us            </h2>
<p class="subline small">Do you have any questions or are you interested in working with us? We are looking forward to your message! We are also happy to talk to you in person - just drop us a line and we will get back to you!</p>
<ul class="link-list">
<li class="icon mail">
<h4>
<a href="https://adgoal.freshdesk.com/support/tickets/new" target="_blank">Support-Message</a>
</h4>
<p>24h, 356 days a year.</p>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="page-footer">
<div class="grid">
<div class="row">
<ul class="list-nav span8">
<li>
<a href="/en/publisher.html" title="Publisher">Publisher</a>
</li>
<li>
<a href="/en/products/smartlink.html" title="Products">Products</a>
</li>
<li>
<a href="/en/about.html" title="About">About</a>
</li>
<li>
<a href="/en/advertiser.html" title="Advertiser">Advertiser</a>
</li>
<li>
<a href="/en/jobs.html" title="Jobs">Jobs</a>
</li>
<li>
<a href="/en/login.html" title="LoginLogin">Login</a>
</li>
</ul>
<div class="span4 text-right">
<p>© adgoal 2021 | <a href="/en/imprint.html">Imprint</a> | <a href="/en/terms.html">Terms</a> | <a href="/en/privacy.html">Privacy</a></p>
</div>
</div>
</div>
</div>
<script src="/js/jquery.min.js"></script>
<script src="/js/plugins.js"></script>
<script src="/js/main.js"></script>
</body>
</html>