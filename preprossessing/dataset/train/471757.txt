<!DOCTYPE html>
<html class="no-js" lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://elkcountryvisitorcenter.com/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>

	<script src="https://elkcountryvisitorcenter.com/wp-content/themes/elkcounty/js/html5.js"></script>

	<![endif]-->
<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
<title>Page not found – Elk Country Visitor Center</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://elkcountryvisitorcenter.com/feed/" rel="alternate" title="Elk Country Visitor Center » Feed" type="application/rss+xml"/>
<link href="https://elkcountryvisitorcenter.com/comments/feed/" rel="alternate" title="Elk Country Visitor Center » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/elkcountryvisitorcenter.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://elkcountryvisitorcenter.com/wp-admin/admin-ajax.php?action=frmpro_css&amp;ver=11121646" id="formidable-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://elkcountryvisitorcenter.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://elkcountryvisitorcenter.com/wp-content/uploads/maxmegamenu/style.css?ver=77b61b" id="megamenu-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://elkcountryvisitorcenter.com/wp-includes/css/dashicons.min.css?ver=5.6" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Noto+Sans%3A400italic%2C700italic%2C400%2C700%7CNoto+Serif%3A400italic%2C700italic%2C400%2C700%7CInconsolata%3A400%2C700&amp;subset=latin%2Clatin-ext" id="twentyfifteen-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://elkcountryvisitorcenter.com/wp-content/themes/elkcounty/genericons/genericons.css?ver=3.2" id="genericons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://elkcountryvisitorcenter.com/wp-content/themes/elkcounty/style.css?ver=5.6" id="twentyfifteen-style-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='twentyfifteen-ie-css'  href='https://elkcountryvisitorcenter.com/wp-content/themes/elkcounty/css/ie.css?ver=20141010' type='text/css' media='all' />
<![endif]-->
<!--[if lt IE 8]>
<link rel='stylesheet' id='twentyfifteen-ie7-css'  href='https://elkcountryvisitorcenter.com/wp-content/themes/elkcounty/css/ie7.css?ver=20141010' type='text/css' media='all' />
<![endif]-->
<script id="jquery-core-js" src="https://elkcountryvisitorcenter.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://elkcountryvisitorcenter.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<link href="https://elkcountryvisitorcenter.com/wp-json/" rel="https://api.w.org/"/><link href="https://elkcountryvisitorcenter.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://elkcountryvisitorcenter.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<link href="https://elkcountryvisitorcenter.com/wp-content/uploads/2017/02/elk_favicon.jpg" rel="icon" sizes="32x32"/>
<link href="https://elkcountryvisitorcenter.com/wp-content/uploads/2017/02/elk_favicon.jpg" rel="icon" sizes="192x192"/>
<link href="https://elkcountryvisitorcenter.com/wp-content/uploads/2017/02/elk_favicon.jpg" rel="apple-touch-icon"/>
<meta content="https://elkcountryvisitorcenter.com/wp-content/uploads/2017/02/elk_favicon.jpg" name="msapplication-TileImage"/>
<style type="text/css">/** Mega Menu CSS: fs **/</style>
<script src="https://elkcountryvisitorcenter.com/wp-content/themes/elkcounty/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body class="error404 mega-menu-header mega-menu-primary">
<div class="wrapper">
<div id="wrap_header">
<div class="container" style="height: 70px;">
<div class="col-sm-3">
</div>
<div class="col-sm-6">
<div class="header-menu">
<div class="mega-menu-wrap" id="mega-menu-wrap-header"><div class="mega-menu-toggle"><div class="mega-toggle-block mega-menu-toggle-block mega-toggle-block-right mega-toggle-block-1" id="mega-toggle-block-1"></div></div><ul class="mega-menu max-mega-menu mega-menu-horizontal mega-no-js" data-breakpoint="0" data-document-click="collapse" data-effect="fade_up" data-effect-mobile="disabled" data-effect-speed="200" data-effect-speed-mobile="0" data-event="hover_intent" data-hover-intent-interval="100" data-hover-intent-timeout="300" data-mobile-force-width="false" data-mobile-state="collapse_all" data-second-click="go" data-unbind="true" data-vertical-behaviour="standard" id="mega-menu-header"><li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-item-6" id="mega-menu-item-6"><a class="mega-menu-link" href="/about-us/" tabindex="0">ABOUT US</a></li><li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-item-79" id="mega-menu-item-79"><a class="mega-menu-link" href="/educators/" tabindex="0">EDUCATORS</a></li><li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-item-91" id="mega-menu-item-91"><a class="mega-menu-link" href="/pa-wilds/" tabindex="0">PA WILDS</a></li><li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-item-90" id="mega-menu-item-90"><a class="mega-menu-link" href="/plan-an-event/" tabindex="0">PLAN AN EVENT</a></li><li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-item-92" id="mega-menu-item-92"><a class="mega-menu-link" href="/contact-us/" tabindex="0">CONTACT US</a></li></ul></div> </div>
</div>
<div class="col-sm-3" style="margin-top: 3px; text-align: right;"><div class="header-sidebar">
<div> <div class="textwidget"><a href="http://www.youtube.com/results?search_query=Elk+County+PA&amp;aq=f" rel="noopener" target="_blank"><img align="right" border="0" class="image image-_original " height="19" src="http://seven31.net/elkcountryvisitorcenter.com/wp-content/uploads/2017/02/icoYT.jpg" width="53"/></a><a href="http://www.facebook.com/#%21/pages/Elk-Country-Visitor-Center/163723837777" rel="noopener" target="_blank"><img align="right" alt="Facebook" border="0" class="image image-_original " height="19" src="http://seven31.net/elkcountryvisitorcenter.com/wp-content/uploads/2017/02/icoFB.jpg" title="Facebook" width="60"/></a></div>
</div><div> <div class="textwidget"><a href="http://elkcountryvisitorcenter.com/support-overview/get-the-elk-country-enewsletter/">eNewsletter Sign Up</a></div>
</div></div>
</div>
</div>
<div class="container">
<div class="col-sm-12"><a class="navbar-brand" href="https://elkcountryvisitorcenter.com/" rel="home" title="Elk Country Visitor Center"><img alt="" height="140" src="http://elkcountryvisitorcenter.com/wp-content/themes/elkcounty/i/elk_logo.png"/></a>
</div>
</div>
<div class="container">
<div id="menu-wrapper">
<div class="col-sm-12">
<div class="primary-menu">
<div class="mega-menu-wrap" id="mega-menu-wrap-primary"><div class="mega-menu-toggle"><div class="mega-toggle-block mega-menu-toggle-block mega-toggle-block-right mega-toggle-block-1" id="mega-toggle-block-1"></div></div><ul class="mega-menu max-mega-menu mega-menu-horizontal mega-no-js" data-breakpoint="0" data-document-click="collapse" data-effect="fade_up" data-effect-mobile="disabled" data-effect-speed="200" data-effect-speed-mobile="0" data-event="hover_intent" data-hover-intent-interval="100" data-hover-intent-timeout="300" data-mobile-force-width="false" data-mobile-state="collapse_all" data-second-click="go" data-unbind="true" data-vertical-behaviour="standard" id="mega-menu-primary"><li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-item-31" id="mega-menu-item-31"><a class="mega-menu-link" href="/general-information/" tabindex="0">Plan</a></li><li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-item-30" id="mega-menu-item-30"><a class="mega-menu-link" href="/learn-overview/" tabindex="0">Learn</a></li><li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-item-29" id="mega-menu-item-29"><a class="mega-menu-link" href="/overview/" tabindex="0">Explore</a></li><li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-item-28" id="mega-menu-item-28"><a class="mega-menu-link" href="/support-overview/" tabindex="0">Support</a></li><li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-item-27" id="mega-menu-item-27"><a class="mega-menu-link" href="/share-overview/" tabindex="0">Share</a></li></ul></div> </div>
</div>
</div>
</div>
</div>
</div>
<div class="content-area" id="primary">
<main class="site-main" id="main" role="main">
<section class="error-404 not-found">
<header class="page-header">
<h1 class="page-title">Oops! That page can’t be found.</h1>
</header><!-- .page-header -->
<div class="page-content">
<p>It looks like nothing was found at this location. Maybe try a search?</p>
<form action="https://elkcountryvisitorcenter.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit screen-reader-text" type="submit" value="Search"/>
</form> </div><!-- .page-content -->
</section><!-- .error-404 -->
</main><!-- .site-main -->
</div><!-- .content-area -->
<div class="containerFooter"><div class="wrapFooter">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td align="center" valign="top">
<div class="clear-block block block-views" id="block-views-location_hours-block_1">
<div class="content">
<div class="view view-location-hours view-id-location_hours view-display-id-block_1 view-dom-id-de050287f2efc004a9de1d199b7dcedc">
<div class="view-header">
<p><span class="typYellow2"><strong>LOCATION:</strong></span><br/>
<strong>Elk Country Visitor Center</strong></p>
</div>
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
<div class="views-field views-field-field-address-value"><span class="field-content">950 Winslow Hill Road, Benezette, PA 15821</span></div>
</div>
</div>
</div>
</div>
</div>
<div class="clear-block block block-views" id="block-views-location_hours-block_2">
<div class="content">
<div class="view view-location-hours view-id-location_hours view-display-id-block_2 view-dom-id-3b023f54d305a80c9a2ce7f4ae107010">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
<div class="views-field views-field-field-phone-value"><span class="field-content">814-787-5167</span></div>
</div>
</div>
</div>
</div>
</div>
</td>
<td align="center" valign="top" width="10"> <img alt="" class="alignnone size-full wp-image-191" height="140" loading="lazy" src="http://seven31.net/elkcountryvisitorcenter.com/wp-content/uploads/2017/02/divide.jpg" width="1"/></td>
<td align="center" valign="top">
<div class="clear-block block block-views" id="block-views-location_hours-block_3">
<div class="content">
<div class="view view-location-hours view-id-location_hours view-display-id-block_3 view-dom-id-1b7ab407404a494bc638a0550793d401">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
<div class="views-field views-field-field-hours-value">
<p><span class="views-label views-label-field-hours-value">Hours: </span></p>
<div class="field-content">
<p style="text-align: center;"><strong>April – May:</strong> Thursday – Monday | 9am – 5pm<br/>
(closed Tues &amp; Wed)<br/>
<strong>June – August:</strong> 7 days a week 8am – 8pm<br/>
<strong>September – October:</strong> 7 Days a Week | 8am – 8pm<br/>
<strong>November – December:</strong> Thursday – Monday | 9am – 5pm<br/>
(closed Tues &amp; Wed)<br/>
<strong>January – March:</strong> Saturday &amp; Sunday Only | 9am – 5pm<br/>
Closed: Easter, Thanksgiving, 1st day of Buck, Christmas*, New Years* (*close at noon on eve of)</p>
<p> </p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</td>
</tr>
</tbody>
</table>
<div class="lineHort">
<div class="clear-block block block-block" id="block-block-2">
<div class="content">
<p><a href="/contact-us/"><strong>CONTACT US</strong></a>    |   <strong><a href="/privacy-policy/">PRIVACY</a></strong></p>
</div>
</div>
</div>
<div class="lineHort">
<div class="clear-block block block-block" id="block-block-1">
<div class="content">
<p>©2016 Elk Country Visitor Center. All Rights Reserved.</p>
</div>
</div>
</div>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td align="right" valign="middle">
<div class="clear-block block block-block" id="block-block-3">
<div class="content">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td align="right" valign="middle" width="228"><a href="http://www.experienceelkcountry.com/" rel="noopener" target="_blank"><img class="alignnone wp-image-56 size-full" height="68" loading="lazy" src="http://seven31.net/elkcountryvisitorcenter.com/wp-content/uploads/2017/02/logoKeystone.gif" width="66"/></a></td>
<td align="center" valign="middle" width="180"><a href="http://www.dcnr.state.pa.us/" rel="noopener" target="_blank"><img class="alignnone wp-image-55 size-full" height="60" loading="lazy" src="http://seven31.net/elkcountryvisitorcenter.com/wp-content/uploads/2017/02/logoPA.jpg" width="164"/></a></td>
<td align="left" valign="middle" width="233"><a href="http://pawilds.com/" rel="noopener" target="_blank"><img class="alignnone wp-image-57 size-full" height="50" loading="lazy" src="http://seven31.net/elkcountryvisitorcenter.com/wp-content/uploads/2017/02/logoWilds.jpg" width="61"/></a></td>
</tr>
</tbody>
</table>
</div>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
<script id="twentyfifteen-skip-link-focus-fix-js" src="https://elkcountryvisitorcenter.com/wp-content/themes/elkcounty/js/skip-link-focus-fix.js?ver=20141010" type="text/javascript"></script>
<script id="twentyfifteen-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var screenReaderText = {"expand":"<span class=\"screen-reader-text\">expand child menu<\/span>","collapse":"<span class=\"screen-reader-text\">collapse child menu<\/span>"};
/* ]]> */
</script>
<script id="twentyfifteen-script-js" src="https://elkcountryvisitorcenter.com/wp-content/themes/elkcounty/js/functions.js?ver=20150330" type="text/javascript"></script>
<script id="hoverIntent-js" src="https://elkcountryvisitorcenter.com/wp-includes/js/hoverIntent.min.js?ver=1.8.1" type="text/javascript"></script>
<script id="megamenu-js-extra" type="text/javascript">
/* <![CDATA[ */
var megamenu = {"timeout":"300","interval":"100"};
/* ]]> */
</script>
<script id="megamenu-js" src="https://elkcountryvisitorcenter.com/wp-content/plugins/megamenu/js/maxmegamenu.js?ver=2.9.2" type="text/javascript"></script>
<script id="wp-embed-js" src="https://elkcountryvisitorcenter.com/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
</body>
</html>
