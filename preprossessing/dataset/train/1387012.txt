<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7 no-js" lang="fi"><![endif]--><!--[if IE 8]><html class="ie ie8 no-js" lang="fi"><![endif]--><!--[if !(IE 7) | !(IE 8)  ]><!--><html class="no-js" lang="fi"><!--<![endif]-->
<head>
<!-- Title & Meta
  ================================================== -->
<meta charset="utf-8"/>
<title>Sivua ei löytynyt » Sulunpuu</title>
<meta content="Sulunpuu Oy" name="author"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=2" name="viewport"/>
<!-- Adaptive Images
	================================================== -->
<script>document.cookie='resolution='+Math.max(screen.width,screen.height)+("devicePixelRatio" in window ? ","+devicePixelRatio : ",1")+'; path=/';</script>
<!--[if lt IE 9]>
		<script src="/wp-content/themes/sulunpuu/js/html5shiv.js"></script>
	<![endif]-->
<!-- Favicons & Pingback
	================================================== -->
<link href="https://sulunpuu.fi/favicon.ico" rel="shortcut icon"/>
<link href="https://sulunpuu.fi/xmlrpc.php" rel="pingback"/>
<!-- This site is optimized with the Yoast SEO plugin v15.5 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex, follow" name="robots"/>
<meta content="fi_FI" property="og:locale"/>
<meta content="Sivua ei löytynyt » Sulunpuu" property="og:title"/>
<meta content="Sulunpuu" property="og:site_name"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://sulunpuu.fi/#website","url":"https://sulunpuu.fi/","name":"Sulunpuu","description":"","potentialAction":[{"@type":"SearchAction","target":"https://sulunpuu.fi/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"fi"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//ajax.googleapis.com" rel="dns-prefetch"/>
<link href="//netdna.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://sulunpuu.fi/feed/" rel="alternate" title="Sulunpuu » syöte" type="application/rss+xml"/>
<link href="https://sulunpuu.fi/comments/feed/" rel="alternate" title="Sulunpuu » kommenttien syöte" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/sulunpuu.fi\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://sulunpuu.fi/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/wp-content/themes/sulunpuu/css/bootstrap.min.css?ver=1" id="bootstrap-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css?ver=5.6" id="icon-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/wp-content/themes/sulunpuu/style.css?ver=1" id="main-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/wp-content/plugins/easy-fancybox/css/jquery.fancybox.min.css?ver=1.3.24" id="fancybox-css" media="screen" rel="stylesheet" type="text/css"/>
<script id="jquery-js" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js?ver=1.9.1" type="text/javascript"></script>
<script id="bootstrap-js-js" src="/wp-content/themes/sulunpuu/js/bootstrap.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="modernizr-js" src="/wp-content/themes/sulunpuu/js/modernizr.js?ver=2.6.2" type="text/javascript"></script>
<script id="responsivenav-js" src="/wp-content/themes/sulunpuu/js/responsive-nav.min.js?ver=1.0.14" type="text/javascript"></script>
<link href="https://sulunpuu.fi/wp-json/" rel="https://api.w.org/"/></head>
<body class="error404">
<header id="primary-head">
<div class="container">
<nav id="primary-nav">
<div class="menu"><ul class="menu" id="menu-paavalikko-main-navigation"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-12" id="menu-item-12"><a href="https://sulunpuu.fi/">Etusivu</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15" id="menu-item-15"><a href="https://sulunpuu.fi/palvelut/">Palvelut</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14" id="menu-item-14"><a href="https://sulunpuu.fi/galleria/">Galleria</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17" id="menu-item-17"><a href="https://sulunpuu.fi/yhteystiedot/">Yhteystiedot</a></li>
</ul></div> </nav>
<div id="logo">
<a href="https://sulunpuu.fi">
<img src="/wp-content/themes/sulunpuu/images/sulunpuu.png"/>
</a>
</div>
</div>
</header>
<div class="container content">
<div class="nav-image-wrapper">
<nav id="secondary-nav">
<div class="menu"><ul class="menu" id="menu-paavalikko-main-navigation-1"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-12"><a href="https://sulunpuu.fi/">Etusivu</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15"><a href="https://sulunpuu.fi/palvelut/">Palvelut</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14"><a href="https://sulunpuu.fi/galleria/">Galleria</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17"><a href="https://sulunpuu.fi/yhteystiedot/">Yhteystiedot</a></li>
</ul></div> </nav> <div id="main">
<h1 class="page-title">Ei löytynyt</h1>
<p>Valitettavasti etsimääsi sivua tai artikkelia ei löytynyt.</p>
</div>
<footer id="primary-footer">
<div class="container">
<p>© 2014 Sulunpuu Oy</p>
<p>Tallilantie 11, 40800 Vaajakoski</p>
<p>Puh. 014 333 0333, 0400 642 595 • <a href="mailto:sulunpuu@co.inet.fi">sulunpuu (at) co.inet.fi</a></p>
<p>Avoinna ma-pe klo 8-17, la sopimuksen mukaan</p>
</div>
</footer>
<script type="text/javascript">

	var navigation = responsiveNav("#primary-nav", { 
		label: "<i id='navopen-icon' class='closed'></i>",
		open: function () {
          var toggle = document.getElementById("navopen-icon");
          toggle.className = toggle.className.replace(/(^|\s)closed(\s|$)/, "opened");
        },
        close: function () {
          var toggle = document.getElementById("navopen-icon");
          toggle.className = toggle.className.replace(/(^|\s)opened(\s|$)/, "closed");
        },
        insert: 'before'
	});

</script>
<script id="jquery-fancybox-js" src="/wp-content/plugins/easy-fancybox/js/jquery.fancybox.min.js?ver=1.3.24" type="text/javascript"></script>
<script id="jquery-fancybox-js-after" type="text/javascript">
var fb_timeout, fb_opts={'overlayShow':true,'hideOnOverlayClick':true,'showCloseButton':true,'margin':20,'centerOnScroll':true,'enableEscapeButton':true,'autoScale':true };
if(typeof easy_fancybox_handler==='undefined'){
var easy_fancybox_handler=function(){
jQuery('.nofancybox,a.wp-block-file__button,a.pin-it-button,a[href*="pinterest.com/pin/create"],a[href*="facebook.com/share"],a[href*="twitter.com/share"]').addClass('nolightbox');
/* IMG */
var fb_IMG_select='a[href*=".jpg"]:not(.nolightbox,li.nolightbox>a),area[href*=".jpg"]:not(.nolightbox),a[href*=".jpeg"]:not(.nolightbox,li.nolightbox>a),area[href*=".jpeg"]:not(.nolightbox),a[href*=".png"]:not(.nolightbox,li.nolightbox>a),area[href*=".png"]:not(.nolightbox)';
jQuery(fb_IMG_select).addClass('fancybox image').attr('rel','gallery');
jQuery('a.fancybox,area.fancybox,li.fancybox a').each(function(){jQuery(this).fancybox(jQuery.extend({},fb_opts,{'type':'image','easingIn':'easeOutBack','easingOut':'easeInBack','opacity':false,'hideOnContentClick':false,'titleShow':true,'titlePosition':'inside','titleFromAlt':true,'showNavArrows':true,'enableKeyboardNav':true,'cyclic':true}))});
/* Inline */
jQuery('a.fancybox-inline,area.fancybox-inline,li.fancybox-inline a').each(function(){jQuery(this).fancybox(jQuery.extend({},fb_opts,{'type':'inline','autoDimensions':true,'scrolling':'no','easingIn':'easeOutBack','easingOut':'easeInBack','opacity':false,'hideOnContentClick':false,'titleShow':false}))});};
jQuery('a.fancybox-close').on('click',function(e){e.preventDefault();jQuery.fancybox.close()});
};
var easy_fancybox_auto=function(){setTimeout(function(){jQuery('#fancybox-auto').trigger('click')},1000);};
jQuery(easy_fancybox_handler);jQuery(document).on('post-load',easy_fancybox_handler);
jQuery(easy_fancybox_auto);
</script>
<script id="jquery-mousewheel-js" src="/wp-content/plugins/easy-fancybox/js/jquery.mousewheel.min.js?ver=3.1.13" type="text/javascript"></script>
<script id="wp-embed-js" src="https://sulunpuu.fi/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
</div></div></body>
</html>