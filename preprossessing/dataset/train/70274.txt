<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US"
	prefix="og: https://ogp.me/ns#" >
<![endif]--><!--[if IE 7]>
<html id="ie7" lang="en-US"
	prefix="og: https://ogp.me/ns#" >
<![endif]--><!--[if IE 8]>
<html id="ie8" lang="en-US"
	prefix="og: https://ogp.me/ns#" >
<![endif]--><!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!--><html lang="en-US" prefix="og: https://ogp.me/ns#">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="https://www.acos.com.au/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
	<script src="https://www.acos.com.au/wp-content/themes/Divi/js/html5.js" type="text/javascript"></script>
	<![endif]-->
<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>
<script>var et_site_url='https://www.acos.com.au';var et_post_id='0';function et_core_page_resource_fallback(a,b){"undefined"===typeof b&&(b=a.sheet.cssRules&&0===a.sheet.cssRules.length);b&&(a.onerror=null,a.onload=null,a.href?a.href=et_site_url+"/?et_core_page_resource="+a.id+et_post_id:a.src&&(a.src=et_site_url+"/?et_core_page_resource="+a.id+et_post_id))}
</script><title>Page not found – ACOS Personnel</title>
<!-- All in One SEO 4.0.12 -->
<meta content="noindex" name="robots"/>
<link href="https://www.acos.com.au/icici/info.htm%09/" rel="canonical"/>
<script class="aioseo-schema" type="application/ld+json">
			{"@context":"https:\/\/schema.org","@graph":[{"@type":"WebSite","@id":"https:\/\/www.acos.com.au\/#website","url":"https:\/\/www.acos.com.au\/","name":"ACOS Personnel","description":"ACOS Personnel","publisher":{"@id":"https:\/\/www.acos.com.au\/#organization"},"potentialAction":{"@type":"SearchAction","target":"https:\/\/www.acos.com.au\/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"Organization","@id":"https:\/\/www.acos.com.au\/#organization","name":"ACOS Personnel","url":"https:\/\/www.acos.com.au\/"},{"@type":"BreadcrumbList","@id":"https:\/\/www.acos.com.au\/icici\/info.htm%09%0A\/#breadcrumblist","itemListElement":[{"@type":"ListItem","@id":"https:\/\/www.acos.com.au\/#listItem","position":1,"item":{"@type":"WebPage","@id":"https:\/\/www.acos.com.au\/#item","name":"Home","description":"ACOS Personnel","url":"https:\/\/www.acos.com.au\/"},"nextItem":"https:\/\/www.acos.com.au\/icici\/info.htm%09%0A\/#listItem"},{"@type":"ListItem","@id":"https:\/\/www.acos.com.au\/icici\/info.htm%09%0A\/#listItem","position":2,"item":{"@id":"https:\/\/www.acos.com.au\/icici\/info.htm%09%0A\/#item","url":"https:\/\/www.acos.com.au\/icici\/info.htm%09%0A\/"},"previousItem":"https:\/\/www.acos.com.au\/#listItem"}]}]}
		</script>
<!-- All in One SEO -->
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//maxcdn.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.acos.com.au/feed/" rel="alternate" title="ACOS Personnel » Feed" type="application/rss+xml"/>
<link href="https://www.acos.com.au/comments/feed/" rel="alternate" title="ACOS Personnel » Comments Feed" type="application/rss+xml"/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.14.0 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script data-cfasync="false" type="text/javascript">
    (window.gaDevIds=window.gaDevIds||[]).push("dZGIzZG");
	var mi_version         = '7.14.0';
	var mi_track_user      = true;
	var mi_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-77341245-6';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}

	if ( 'undefined' === typeof gaOptout ) {
		function gaOptout() {
			__gaTrackerOptout();
		}
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-77341245-6', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('require', 'displayfeatures');
		__gaTracker('send','pageview','/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer);
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<!-- / Google Analytics by MonsterInsights -->
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.acos.com.au\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<meta content="Acos Theme v.1.0.0" name="generator"/><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.acos.com.au/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.acos.com.au/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.acos.com.au/wp-content/plugins/google-analytics-for-wordpress/assets/css/frontend.min.css?ver=7.14.0" id="monsterinsights-popular-posts-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.acos.com.au/wp-content/themes/acos/css/jquery.bxslider.css?ver=5.5.3" id="style.bxslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,latin-ext&amp;display=swap" id="divi-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.acos.com.au/wp-content/themes/acos/style.css?ver=4.7.7" id="divi-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.acos.com.au/wp-content/plugins/custom-facebook-feed/assets/css/cff-style.css?ver=2.18.1" id="cff-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?ver=5.5.3" id="sb-font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.acos.com.au/wp-includes/css/dashicons.min.css?ver=5.5.3" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.acos.com.au/wp-content/plugins/add-to-any/addtoany.min.css?ver=1.15" id="addtoany-css" media="all" rel="stylesheet" type="text/css"/>
<script id="monsterinsights-frontend-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[]","home_url":"https:\/\/www.acos.com.au","hash_tracking":"false"};
/* ]]> */
</script>
<script id="monsterinsights-frontend-script-js" src="https://www.acos.com.au/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.14.0" type="text/javascript"></script>
<script id="jquery-core-js" src="https://www.acos.com.au/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="addtoany-js" src="https://www.acos.com.au/wp-content/plugins/add-to-any/addtoany.min.js?ver=1.1" type="text/javascript"></script>
<script id="js.bxslider-js" src="https://www.acos.com.au/wp-content/themes/acos/js/jquery.bxslider.min.js?ver=5.5.3" type="text/javascript"></script>
<link href="https://www.acos.com.au/wp-json/" rel="https://api.w.org/"/><link href="https://www.acos.com.au/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.acos.com.au/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<script data-cfasync="false">
window.a2a_config=window.a2a_config||{};a2a_config.callbacks=[];a2a_config.overlays=[];a2a_config.templates={};
(function(d,s,a,b){a=d.createElement(s);b=d.getElementsByTagName(s)[0];a.async=1;a.src="https://static.addtoany.com/menu/page.js";b.parentNode.insertBefore(a,b);})(document,"script");
</script>
<script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		if (window.wfLogHumanRan) { return; }
		window.wfLogHumanRan = true;
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//www.acos.com.au/?wordfence_lh=1&hid=E51E3D7D36681079003AFFE08C1CFECE');
</script><meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/><link as="font" crossorigin="anonymous" href="https://www.acos.com.au/wp-content/themes/Divi/core/admin/fonts/modules.ttf" rel="preload"/><link href="/wp-content/uploads/2017/12/acos-favicon.jpg" rel="shortcut icon"/><link href="https://www.acos.com.au/wp-content/et-cache/global/et-divi-customizer-global-16103326010815.min.css" id="et-divi-customizer-global-cached-inline-styles" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" rel="stylesheet"/></head>
<body class="error404 et_pb_button_helper_class et_fullwidth_nav et_non_fixed_nav et_show_nav et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_footer_columns4 et_boxed_layout et_cover_background et_pb_gutter et_pb_gutters3 et_right_sidebar et_divi_theme acos-theme et-db et_minified_js et_minified_css">
<div id="page-container">
<header data-height-onload="80" id="main-header">
<div class="container clearfix et_menu_container">
<div class="logo_container">
<span class="logo_helper"></span>
<a href="https://www.acos.com.au/" style="float:left;">
<img alt="ACOS Personnel" data-height-percentage="72" id="logo" src="/wp-content/uploads/2019/03/acos-logo.jpg"/>
</a>
</div>
<div data-fixed-height="40" data-height="80" id="et-top-navigation">
<div class="header_phone"><a href="tel:0288248488">CALL 02 8824 8488</a></div>
<ul class="et-social-icons">
<li class="et-social-icon et-social-facebook">
<a class="icon" href="http://www.facebook.com/acospersonnel">
<span>Facebook</span>
</a>
</li>
<li class="et-social-icon et-social-linkedin">
<a class="icon" href="https://www.linkedin.com/company/acos-personnel‎">
<span>LinkedIn</span>
</a>
</li>
</ul> <nav id="top-menu-nav">
<ul class="nav" id="top-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-1007" id="menu-item-1007"><a href="https://www.acos.com.au/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-103" id="menu-item-103"><a href="https://www.acos.com.au/about-us/">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-102" id="menu-item-102"><a href="https://www.acos.com.au/looking-for-staff/">Looking for Staff</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-184" id="menu-item-184"><a href="https://www.acos.com.au/looking-for-staff/services/">Services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-183" id="menu-item-183"><a href="https://www.acos.com.au/looking-for-staff/request-a-call/">Request A Call</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-180" id="menu-item-180"><a href="https://www.acos.com.au/looking-for-staff/feedback/">Feedback</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-182" id="menu-item-182"><a href="https://www.acos.com.au/looking-for-staff/feedback/feedback-from-companies/">Feedback From Companies</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-181" id="menu-item-181"><a href="https://www.acos.com.au/looking-for-staff/feedback/feedback-from-candidates/">Feedback From Candidates</a></li>
</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-99" id="menu-item-99"><a href="https://www.acos.com.au/job-seekers/">Job Seekers</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-194" id="menu-item-194"><a href="https://www.acos.com.au/job-seekers/current-jobs/">Current Jobs</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-193" id="menu-item-193"><a href="https://www.acos.com.au/job-seekers/register-online/">Register Online</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-192" id="menu-item-192"><a href="https://www.acos.com.au/job-seekers/career-advice/">Career Advice</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-208" id="menu-item-208"><a href="https://www.acos.com.au/job-seekers/code-of-conduct/">Code of Conduct</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-97" id="menu-item-97"><a href="https://www.acos.com.au/contact-us/">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-96" id="menu-item-96"><a href="https://www.acos.com.au/timesheet-log-in/">Timesheet Log-in</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-805" id="menu-item-805"><a href="https://www.acos.com.au/blog/">Blog</a></li>
</ul> </nav>
<div id="et_mobile_nav_menu">
<div class="mobile_nav closed">
<span class="select_page">Select Page</span>
<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
</div>
</div> </div> <!-- #et-top-navigation -->
</div> <!-- .container -->
<div class="et_search_outer">
<div class="container et_search_form_container">
<form action="https://www.acos.com.au/" class="et-search-form" method="get" role="search">
<input class="et-search-field" name="s" placeholder="Search …" title="Search for:" type="search" value=""/> </form>
<span class="et_close_search_field"></span>
</div>
</div>
</header> <!-- #main-header -->
<div id="et-main-area">
<div id="main-content">
<div class="container">
<div class="clearfix" id="content-area">
<div id="left-area">
<article class="et_pb_post not_found" id="post-0">
<div class="entry">
<!--If no results are found-->
<h1 class="not-found-title">No Results Found</h1>
<p>The page you requested could not be found. Try refining your search, or use the navigation above to locate the post.</p>
</div>
<!--End if no results are found--> </article> <!-- .et_pb_post -->
</div> <!-- #left-area -->
<div id="sidebar">
<div class="et_pb_widget widget_search" id="search-2"><form action="https://www.acos.com.au/" class="searchform" id="searchform" method="get" role="search">
<div>
<label class="screen-reader-text" for="s">Search for:</label>
<input id="s" name="s" type="text" value=""/>
<input id="searchsubmit" type="submit" value="Search"/>
</div>
</form></div> <!-- end .et_pb_widget -->
<div class="et_pb_widget widget_recent_entries" id="recent-posts-2">
<h4 class="widgettitle">Recent Posts</h4>
<ul>
<li>
<a href="https://www.acos.com.au/flexible-working-arrangements/">Flexible Working Arrangements</a>
</li>
<li>
<a href="https://www.acos.com.au/employment-types/">Employment Types</a>
</li>
<li>
<a href="https://www.acos.com.au/ending-employment/">Ending Employment</a>
</li>
<li>
<a href="https://www.acos.com.au/probation-period/">Probation Period</a>
</li>
</ul>
</div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_recent_comments" id="recent-comments-2"><h4 class="widgettitle">Recent Comments</h4><ul id="recentcomments"></ul></div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_archive" id="archives-2"><h4 class="widgettitle">Archives</h4>
<ul>
<li><a href="https://www.acos.com.au/2019/03/">March 2019</a></li>
<li><a href="https://www.acos.com.au/2019/02/">February 2019</a></li>
</ul>
</div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_categories" id="categories-2"><h4 class="widgettitle">Categories</h4>
<ul>
<li class="cat-item cat-item-1"><a href="https://www.acos.com.au/category/uncategorized/">Uncategorized</a>
</li>
</ul>
</div> <!-- end .et_pb_widget --> </div> <!-- end #sidebar -->
</div> <!-- #content-area -->
</div> <!-- .container -->
</div> <!-- #main-content -->
<span class="et_pb_scroll_top et-pb-icon"></span>
<footer id="main-footer">
<div id="et-footer-nav">
<div class="container">
<ul class="bottom-nav" id="menu-privacy-policy"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-390" id="menu-item-390"><a href="https://www.acos.com.au/about-us/privacy-policy/">Click here to view our Privacy Policy</a></li>
</ul> </div>
</div> <!-- #et-footer-nav -->
<div id="footer-bottom">
<div class="container clearfix">
<ul class="et-social-icons">
<li class="et-social-icon et-social-facebook">
<a class="icon" href="http://www.facebook.com/acospersonnel">
<span>Facebook</span>
</a>
</li>
<li class="et-social-icon et-social-linkedin">
<a class="icon" href="https://www.linkedin.com/company/acos-personnel‎">
<span>LinkedIn</span>
</a>
</li>
</ul>
<div style="float: left; color: #fff; font-size: 12px;">Copyright © ACOS Personnel 2021. ACOS GROUP PTY LTD. ALL RIGHTS RESERVED.</div>
<div style="float: right; color: #fff; font-size: 12px;">Designed by <a href="http://www.pitstop101media.com.au/" style="color: #fff;" target="_blank">Pitstop 101 Media</a></div>
</div> <!-- .container -->
</div>
</footer> <!-- #main-footer -->
</div> <!-- #et-main-area -->
</div> <!-- #page-container -->
<div id="aioseo-admin"></div><!-- Custom Facebook Feed JS -->
<script type="text/javascript">
var cfflinkhashtags = "true";
</script>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.acos.com.au\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://www.acos.com.au/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="google-recaptcha-js" src="https://www.google.com/recaptcha/api.js?render=6LewTokUAAAAAGeFAb3D7vKEk0y5pWnaX1saEYM7&amp;ver=3.0" type="text/javascript"></script>
<script id="wpcf7-recaptcha-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7_recaptcha = {"sitekey":"6LewTokUAAAAAGeFAb3D7vKEk0y5pWnaX1saEYM7","actions":{"homepage":"homepage","contactform":"contactform"}};
/* ]]> */
</script>
<script id="wpcf7-recaptcha-js" src="https://www.acos.com.au/wp-content/plugins/contact-form-7/modules/recaptcha/script.js?ver=5.3.2" type="text/javascript"></script>
<script id="divi-custom-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var DIVI = {"item_count":"%d Item","items_count":"%d Items"};
var et_shortcodes_strings = {"previous":"Previous","next":"Next"};
var et_pb_custom = {"ajaxurl":"https:\/\/www.acos.com.au\/wp-admin\/admin-ajax.php","images_uri":"https:\/\/www.acos.com.au\/wp-content\/themes\/Divi\/images","builder_images_uri":"https:\/\/www.acos.com.au\/wp-content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"c00abcade0","subscription_failed":"Please, check the fields below to make sure you entered the correct information.","et_ab_log_nonce":"f567629643","fill_message":"Please, fill in the following fields:","contact_error_message":"Please, fix the following errors:","invalid":"Invalid email","captcha":"Captcha","prev":"Prev","previous":"Previous","next":"Next","wrong_captcha":"You entered the wrong number in captcha.","wrong_checkbox":"Checkbox","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","ab_tests":[],"is_ab_testing_active":"","page_id":"","unique_test_id":"","ab_bounce_rate":"","is_cache_plugin_active":"yes","is_shortcode_tracking":"","tinymce_uri":""}; var et_builder_utils_params = {"condition":{"diviTheme":true,"extraTheme":false},"scrollLocations":["app","top"],"builderScrollLocations":{"desktop":"app","tablet":"app","phone":"app"},"onloadScrollLocation":"app","builderType":"fe"}; var et_frontend_scripts = {"builderCssContainerPrefix":"#et-boc","builderCssLayoutPrefix":"#et-boc .et-l"};
var et_pb_box_shadow_elements = [];
var et_pb_motion_elements = {"desktop":[],"tablet":[],"phone":[]};
var et_pb_sticky_elements = [];
/* ]]> */
</script>
<script id="divi-custom-script-js" src="https://www.acos.com.au/wp-content/themes/Divi/js/custom.unified.js?ver=4.7.7" type="text/javascript"></script>
<script id="cffscripts-js" src="https://www.acos.com.au/wp-content/plugins/custom-facebook-feed/assets/js/cff-scripts.js?ver=2.18.1" type="text/javascript"></script>
<script id="et-core-common-js" src="https://www.acos.com.au/wp-content/themes/Divi/core/admin/js/common.js?ver=4.7.7" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.acos.com.au/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
<script type="text/javascript">
	
	jQuery(document).ready(function($) {
 
	    $('.bxslider').bxSlider( { 
			auto: true,
			mode: 'vertical',
			speed: 1000,
			captions: false,
			infiniteLoop: true,
			controls: false,
			pager: false,
			responsive: true
		} );
		
	});
	
	</script>
</body>
</html>