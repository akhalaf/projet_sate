<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "86181",
      cRay: "610b2173ff9a248d",
      cHash: "f533e2e39073bcd",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuNDExbWFuaWEuY29tL01NQS9jb2x1bW5zLzIwMTQyNw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "tyCKrAtJr9SJcT1VH+tUpQHSwubXDq/5pl4WlYQ+QrvtkNXmEoTM5NTO+Q6PHlmi8mvSXu5lq0LJawaqYbYQaSzUCUSFAT44kWvwXrWwpfXaF5HUWDCMfT8udWVeDx5DgBXcbxBimG9BSpE+SlAbkG5lGnRty+Fh6i4QjHcZBsifA7M8MZ5us2w0u95Cg8oP7puXanqZkShAsJd0Ia9i8klqr2kM3sNURKnwF12rtVfMQ8r/kbmgFqfkvl69j+pbQ/ZTHdQ61A/v9iUN9fLBPjy6zmXCRJGQUI3ySfL77FUty57ymN0b089b7VGWlkq5dGcYq/ftTJ8R52XtdDiyUdqQhbOAaVboXYPWc3cQjFQU+59AlAvyH7ZD7IrK8iLc7xCrxLrEbejkL29wBncvsFcBA1GNnHt3HkODIeJtvHtPk596ep5u+XDoOh/GHU4g0O7R28zAJKyfm//FgQ8AjwxCvN9xkQVBnGJqsfuCItiWNvPqC3wYt1eT0torNz9TLX4Rp7uBZJ8kVhTQYp6QlgrKzUH9QRhd0xNGeKbR9G2iuKeQpq5O+7y6mo7EXEtbqgGPwngYzha7Nz5ICPq+1zx2c9T9gDPk0r8E3EKQdtv76J7+vg0Eb05sBfxIfe12Hs2rvGyIhTaivFAIYu6rJkONuDHAVtqpFYQmPV8N5XhrROVaujzG9fFhHKnjh8W9QSQPqU2TymD6S91NFN1kAPzuqp8TRR74/GjEr6UP+W7xdDlq3ZI3pJw6ZqM2zNNW",
        t: "MTYxMDQ5OTEwNS45MjEwMDA=",
        m: "K7AGUz1JEVfbSdNktU/2BqL2oZRImjH2b68jMY1Nka0=",
        i1: "dTnIsBKogEw0rFKxwOTefw==",
        i2: "4CRVi8JhMtZUW7ehUtZC5Q==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "8Th5lpHJJvTylEZagdvhV5TXjOofKOGaVDxny/SJPZU=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610b2173ff9a248d");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> 411mania.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/MMA/columns/201427?__cf_chl_jschl_tk__=0485b9574057423912d82d6ddc5d50262697651b-1610499105-0-AdkLUuv9LfW5cipqoXPKberN8iuxEBkRGcKqG4DgvzJW5Gpj4cPy8RBWZEXrafl6p2aLIFmAeThhs6-6RpPyaWLdfrXz8teZR76H3ekDwb8BIzw3RHdQkDjX5o9dS5OQ9BhU1M4nYySyk-fURgu61Wm5liTAIb-q5zoSZYwPD1iFXZiEnL8aFZo-gHoEwPXvlzNWbvaZUNmF0Y2PTTDi--aOhI_PaDekvpiTicFu3gQIPjUbJWRIbKyS7rsX6T0kriSTPoPl-R7h7Ik6q-93thjJvurDcj2wuG_8AbOPMjrzA6XqRTWEsN1ymcdIaD4mV2vWQrjcW0wXYs4oYzMpVjM" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="e0627de67cfa0c55923be8f695aa451d7bb4a509-1610499105-0-ASSL0NjU56ZRapUqzu4stJNI/Db5mfSOBob9zaV+iuK36HNzawRMU4jcqa2lRFxgyKcGGo7plpNiK5ec9O1jiC0xa1ukAiMvA3xnbvoB2FxJ4O4x6CgEZefMFHD9diF/JlzXEB7aD3JIv1eynZU9515O3VKmpAUZG38YvPRI4nO1s0DwRyglrYccvptHVqAmkjDdVHpliwXatY0BpF7JiTuS1TjaQ9jkzW2uJXPxsoyptrjCXfyZrmDVfS7teNagisNJBQAaYOGpfWKNcq+b29c2cn0LNwayWaJUqiLDWoODGPHpSwtLw0FA3u8aB6g9MRJyWbFnWErXfGa2wvk/iCBs7AQm5xGddWWxGijwe5hUXtaAADtpMqvULfsc16eqrC3nMGdtyWCnbjz/forAIv+ULkOWANnZc2OzLvBen6n8Ue4CPM6ODbo8eW3sCTqmr6nAL6EpDAbXCOWWn2khFVPjlZ7gXMhIKK8sRuF2jlC/MAhhYObqcKSjdcUOJzeJM4HgJd6D9uEUSoJZ4f/whuS+4w52RhDIUSLIsFRzg3zVzWY4ZNuV5wibwlfPuZl0mWpj88tw5SlIt/QUVKwSsCz2nBBTuaTJNehA8EqVQMl4/hc+x+jHjplw757I437AOWOPLs6BG+GjDDS2XYfOjifBWj8BQahoLMT2atxK2YB36kihdnD1aAAAkXqMYYCG8xxQr1Z4xKS9x8+bonjGM/Kh+/3odGi/WEwDAG6TYO5OEQ+ZoHSAI6oX3uy0ubC5VK9h2qT1EENDsxH4LyGrmex+0zVlJWj5l1BvgmNXobElf0ANYFyISVXP+rjBpW9PXOxpPwUcyk63kt+5K3sWP0wJ8ZrlV/XDa9tSdNofxX93j2+6ZYAhH5dTufqHC3gm4yUiSgRbqgC4mGufS5Gumwb6cCmcPdLa5+XKd4CvPWuSBJm6gGIY6cvc+NE1CtxfzLOjSIdDV1p5UiRkATqkqWzXw+5DvOynY0VVXJ/k+6H9eW4BTcfuFcijhoOLFUD+piLGVZ5Xoeh8hCMODdCdHkJMV6fIccJ4v1kKpNWvnnPIrCIfqc1/5yX/7qR0BUQQXc53L65XXv6OidhYJhVutJCdvTEYy9Eo8AdCDDRXU4rE1kKZ7j5q6AxAXzhsTWmKwlTOLh3KSnBVTSM/bkGiUr+C0aCSZu5NIZ3cm0J+mJZDFEMF9CqXq/NOFx4c/qwz8+FaoYbM8f6xpNAHCWfn2OTJenztHPsfolmJT6xWnlRRGXWDfrrDO8fgoO4JvpfchAjh54ovPr5tIsN9lrYqDtREZKHIuUmxCR0tOXlLkr7m"/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="07fc270a8a6bb0183e36dc170d5e945e"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610499109.921-FTdzrEGwbk"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610b2173ff9a248d')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610b2173ff9a248d</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
