<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>imported furniture, luxury imported furniture, luxury furniture in delhi</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="We offers architecture and design consultancy services, as well as a retail division with the largest selection of premium furniture and artifacts under one roof." name="description"/>
<meta content="imported furniture, luxury imported furniture, luxury furniture in delhi" name="keywords"/>
<!-- Bootstrap Core CSS -->
<link href="https://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css"/>
<link href="https://www.elitaire.in/resource/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://www.elitaire.in/resource/css/font-awesome.css" rel="stylesheet"/>
<!-- Custom CSS -->
<link href="https://www.elitaire.in/resource/css/style.css" rel="stylesheet"/>
<link href="https://www.elitaire.in/resource/css/menuzord.css" id="main-css" rel="stylesheet" type="text/css"/>
<link href="https://www.elitaire.in/resource/magnific-popup/magnific-popup.css" rel="stylesheet"/>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
        #dp_swf_engine{
            display: none !important;
        }
        </style>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
<div class="">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header" style="float: left">
<a class="navbar-brand" href="https://www.elitaire.in/"><img alt="Elitaire" src="https://www.elitaire.in/resource/images/logo.png"/></a>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="yearscel"><img alt="Celebrating 25 Years" src="https://www.elitaire.in/resource/images/25-years.png"/></div>
<div class="menuzord" id="menuzord">
<ul class="menuzord-menu menuzord-indented scrollable">
<li><a href="https://www.elitaire.in/aboutus"><i class=""></i> About Us</a>
</li>
<li><a href="https://www.elitaire.in/promoter"><i class=""></i> Promoter</a>
</li>
<li><a href="javascript:void(0)"><i class=""></i> Interiors</a>
<ul class="dropdown">
<li><a href="https://www.elitaire.in/projects">Projects</a>
</li>
<li><a href="https://www.elitaire.in/furniture">Furniture &amp; Accessories</a>
</li>
<li><a href="https://www.elitaire.in/artifacts">Artifacts</a>
</li>
</ul>
</li>
<li><a href="https://www.elitaire.in/clients"><i class=""></i> Our Clients</a>
</li>
<li><a href="https://www.elitaire.in/photogallery"><i class="fa fa-photo"></i> Photos Gallery</a>
</li>
<li><a href="https://www.elitaire.in/media"><i class=""></i> Media</a>
</li>
<li><a href="https://www.elitaire.in/virtualtour"><i class=""></i> Virtual Tour</a>
</li>
<li><a href="http://www.elitaire.in/blog/"><i class=""></i> Blog</a>
</li>
<li><a href="https://www.elitaire.in/careers"><i class=""></i> Careers</a>
</li>
<li><a href="https://www.elitaire.in/contactus"><i class=""></i> Contact us</a>
</li>
</ul>
</div> <!-- /.navbar-collapse -->
</div>
<!-- /.container -->
</nav>
<header class="carousel slide" id="myCarousel" style="position: fixed; top:0; left:0; width: 100%; height: 100%;">
<div class="carousel-inner">
<div class="item active ">
<!-- Set the first background image using inline CSS below. -->
<div class="fill" style="background-image:url('https://www.elitaire.in/resource/images/slider/image13.jpg');"></div>
</div>
<div class="item ">
<!-- Set the first background image using inline CSS below. -->
<div class="fill" style="background-image:url('https://www.elitaire.in/resource/images/slider/image14.jpg');"></div>
</div>
<div class="item ">
<!-- Set the first background image using inline CSS below. -->
<div class="fill" style="background-image:url('https://www.elitaire.in/resource/images/slider/image15.jpg');"></div>
</div>
<div class="item ">
<!-- Set the first background image using inline CSS below. -->
<div class="fill" style="background-image:url('https://www.elitaire.in/resource/images/slider/image16.jpg');"></div>
</div>
<div class="item ">
<!-- Set the first background image using inline CSS below. -->
<div class="fill" style="background-image:url('https://www.elitaire.in/resource/images/slider/image17.jpg');"></div>
</div>
</div>
<!-- Controls -->
<a class="left carousel-control" data-slide="prev" href="#myCarousel">
<span class="icon-prev"></span>
</a>
<a class="right carousel-control" data-slide="next" href="#myCarousel">
<span class="icon-next"></span>
</a>
</header> <!-- /.container -->
<div class="navbar navbar-inverse navbar-fixed-bottom footern">
<div class="">
<div class="col-md-3 col-sm-3 copyrighttex">
        © Copyright 2021, Dolphin Mart Pvt. Ltd.    
        </div>
<div class="col-xs-12 col-sm-6 partner-sites">
<a href="http://www.dolphin-india.com" target="_blank"><img alt="Dmart Exclusif" src="https://www.elitaire.in/resource/images/partner-sites/dolphin.png"/></a>
<a href="http://www.dmartexclusif.com" target="_blank"><img alt="Dolphin Mart" src="https://www.elitaire.in/resource/images/partner-sites/dmart-exclusif.png"/></a>
<a href="http://www.woodmart.in" target="_blank"><img alt="Woodmart" src="https://www.elitaire.in/resource/images/partner-sites/woodmart.png"/></a>
<a href="http://www.iotagifts.in" target="_blank"><img alt="Iota Gifts" src="https://www.elitaire.in/resource/images/partner-sites/iota.png"/></a>
<a href="http://www.bionatural.in" target="_blank"><img alt="Bio Natural" src="https://www.elitaire.in/resource/images/partner-sites/bio-natural.png"/></a>
</div>
<div class="col-md-3 col-sm-3">
<div class="footer-right">
<a class="btn btn-xs btn-social-icon btn-facebook" href="http://www.facebook.com/elitaire" target="_blank">
<i class="fa fa-facebook"></i></a>
<a class="btn btn-xs btn-social-icon btn-google-plus" href="https://plus.google.com/+elitaireindia" target="_blank"><i class="fa fa-google-plus"></i></a>
<a class="btn btn-xs btn-social-icon btn-twitter" href="https://twitter.com/elitaire" target="_blank"><i class="fa fa-twitter"></i></a>
<a class="btn btn-xs btn-social-icon btn-instagram" href="https://www.instagram.com/elitaire.india/" target="_blank"><i class="fa fa-instagram"></i></a>
</div>
<div style="clear:both; height:5px;"></div>
<div class="copyrighttexr">Developed by <span><a class="link-01" href="http://www.svcreation.in" target="_blank"> S.V. Creation</a></span>
</div>
</div>
</div>
</div>
<script type="text/javascript">var siteroot="https://www.elitaire.in/";</script>
<!-- jQuery -->
<script src="https://www.elitaire.in/resource/js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="https://www.elitaire.in/resource/js/bootstrap.min.js"></script>
<script src="https://www.elitaire.in/resource/js/fsubmit.js"></script>
<script src="https://www.elitaire.in/resource/js/menuzord.js" type="text/javascript"></script>
<script src="https://www.elitaire.in/resource/magnific-popup/jquery.magnific-popup.js" type="text/javascript"></script> <script type="text/javascript">
		    jQuery(document).ready(function(){
				jQuery("#menuzord").menuzord({


				});

			});
		</script>
<script>
$('.gallery').each(function() { // the containers for all your galleries
    $(this).magnificPopup({
        delegate: 'a.galleryimg', // the selector for gallery item
        type: 'image',
        gallery: {
          enabled:true
        }
    });
});
</script>
<script type="text/javascript">
      $(document).ready(function() {
        $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
          disableOn: 700,
          type: 'iframe',
          mainClass: 'mfp-fade',
          removalDelay: 160,
          preloader: false,

          fixedContentPos: false
        });
      });
    </script>
<script>
	$('#myCarousel').carousel({
  interval: 6000,
  cycle: true
}); 
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-85990616-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-85990616-4');
</script>
</body>
</html>