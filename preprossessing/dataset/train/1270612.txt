<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Satya</title>
<script async="" data-ad-client="ca-pub-7889063089967245" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="keywords"/>
<meta content="" name="description"/>
<!-- Favicons -->
<link href="images/favicon.ico" rel="icon"/>
<link href="img/favicon.ico" rel="apple-touch-icon"/>
<!-- Bootstrap CSS File -->
<link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<link href="fonts/stylesheet.css" rel="stylesheet"/>
<!-- Libraries CSS Files -->
<link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link href="http://cdn.bootcss.com/animate.css/3.5.1/animate.min.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="js/float-panel.js"></script>
<!-- Owl Stylesheets -->
<link href="css/owl.carousel.min.css" rel="stylesheet"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<!-- javascript -->
<script src="css/owl.carousel.js"></script>
<!-- Main Stylesheet File -->
<link href="css/style.css" rel="stylesheet"/>
<link href="css/responsive.css" rel="stylesheet"/>
<script>
    function pageRedirect() {
      window.location.href = "https://www.satyagroups.in/the-hermitage-never-before-offer/google/";
    } 
       function pageRedirec() {
      window.location.href = "https://www.satyagroups.in/the-hive-office-spaces-payment-plan/google/";
    } 
</script>
</head>
<body>
<!-------------------------->
<header class="" id="header">
<div class="header float-panel" data-scroll="500" data-top="0">
<div class="strip desktop-show">
<div class="container">
<ul>
<li><label>Toll Free</label><a href="tel:18001026510">1800 1026 510 ,</a><a href="tel:911244989300">+91 124 4989300</a></li>
<li><a class="facebook" href="https://www.facebook.com/satyadevelopers" target="_blank"></a>
<a class="linkedin" href="https://www.linkedin.com/company/satya-developers" target="_blank"></a>
<a class="twitter" href="https://twitter.com/satyadevelopers" target="_blank"></a>
<a class="youtube" href="https://www.youtube.com/user/satyagroup2013" target="_blank"></a>
<!--<a class="google"   href="https://google.com" target="_blank"></a>-->
</li>
</ul>
</div>
</div>
<div class="midheader">
<div class="container">
<div class="pull-left" id="logo">
<a href="/"><img alt="satya" src="images/satya.png" title="satya"/></a>
</div>
<div class="menus desktop-show">
<nav class="pull-right" id="nav-menu-container">
<ul class="nav-menu">
<li class="menu-active"><a href="/">Home</a></li>
<li class=""><a href="aboutus">About Us</a></li>
<li class="submenu desktop-show" data-target="#demo" data-toggle="collapse" id="prmenu"><a class="dropicon">Project</a></li>
<li class=""><a href="career">Career</a></li>
<li class=""><a href="media">Media</a></li>
<li class=""><a href="nri-corner">NRI Corner</a></li>
<li class=""><a href="contactus">Contact Us</a></li>
</ul>
</nav><!-- #nav-menu-container -->
<div class="collapse" id="demo">
<div class="topmenu">
<div class="row">
<div class="col-xs-12 col-md-12">
<div class="topmbox">
<h4>GURUGRAM</h4>
<ul>
<li><label>
                                                    RESIDENTIAL</label></li>
<li><a href="22/the-hermitage">
                                                    The Hermitage</a></li>
<li><a href="24/nora">
                                                    Nora</a></li>
<li><a href="23/the-legend">
                                                    The Legend</a></li>
</ul>
<ul>
<li><label>
                                                    COMMERCIAL</label></li>
<li><a href="7/the-hive">
                                                    The Hive</a></li>
<li><a href="11/element-one-tower-a">
                                                    Element One Tower A</a></li>
<li><a href="25/centrum-plaza">
                                                    Centrum Plaza</a></li>
<li><a href="26/galaxy-">
                                                    Galaxy </a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="topmenu btmmenu">
<div class="row">
<div class="col-xs-12 col-md-6">
<div class="topmbox snd">
<h4>INDORE</h4>
<ul>
<li><label>RESIDENTIAL</label></li>
<li><a href="29/malwa-jewels">
                                                    Malwa Jewels</a></li>
<li><a href="27/malwa-county">
                                                    Malwa County</a></li>
<li><a href="28/malwa-heights">
                                                    Malwa Heights</a></li>
<!--<li><a href="project-detail?pid=25">Centrum Plaza</a></li>-->
</ul>
</div>
</div>
<div class="col-xs-12 col-md-6">
<div class="topmbox snd bdrn">
<h4>BATHINDA</h4>
<ul>
<li><label>COMMERCIAL</label></li>
<li><a href="30/city-centre">
                                                    City Centre</a></li>
<!--<li><a href="project-detail?pid=25">Centrum Plaza</a></li>-->
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="navbar">
<nav class="menu">
<div class="mmenu">
<div class="leftmenu">
<ul class="navbarmenu">
<li><a href="/">HOME</a></li>
<li><a href="aboutus">ABOUT US</a></li>
</ul>
</div>
<div class="sitemap mobilemenu">
<ul class="accordion" id="accordion">
<li class="">
<div class="link"><span>PROJECTS</span><i class="fa fa-chevron-down"></i></div>
<ul class="sitemapdtl" style="display:none;">
<ul class="stmp">
<span class="stp-box">
<ul>
<li>
<span class="set">
<a>GURUGRAM <span class="fa fa-chevron-down"></span>
</a>
<ul class="content">
<li><label>RESIDENTIAL</label></li>
<li><a href="project-detail?pid=22">The Hermitage</a></li>
<li><a href="project-detail?pid=24">Nora</a></li>
<li><a href="project-detail?pid=23">The Legend</a></li>
</ul>
<ul class="content">
<li><label>COMMERCIAL</label></li>
<li><a href="project-detail?pid=7">The Hive</a></li>
<li><a href="project-detail?pid=11">Element One</a></li>
<li><a href="project-detail?pid=25">Centrum Plaza</a></li>
<li><a href="project-detail?pid=26">Galaxy</a></li>
</ul>
<!--            <ul class="content">-->
<!--<li><label>HOSPITALITY</label></li>-->
<!--                                <li><a href="project-detail?pid=26">Galaxy Hotel</a></li>-->
<!--            </ul>-->
</span>
</li>
</ul>
<ul>
<li>
<span class="set">
<a>INDORE <span class="fa fa-chevron-down"></span>
</a>
<ul class="content">
<li><label>RESIDENTIAL</label></li>
<li><a href="project-detail?pid=29">Malwa Jewels</a></li>
<li><a href="project-detail?pid=27">Malwa County</a></li>
<li><a href="project-detail?pid=28">Malwa Heights</a></li>
</ul>
</span>
</li>
</ul>
<ul>
<li>
<span class="set">
<a>BATHINDA <span class="fa fa-chevron-down"></span>
</a>
<ul class="content">
<li><label>COMMERCIAL</label></li>
<li><a href="project-detail?pid=30">City Centre</a></li>
</ul>
</span>
</li>
</ul>
</span>
</ul>
</ul>
</li>
</ul>
</div>
<div class="leftmenu">
<ul class="navbarmenu">
<li><a href="career">CAREER</a></li>
<li><a href="media">MEDIA</a></li>
<li><a href="nri-corner">NRI CORNER</a></li>
<li class="brnon"><a href="contactus">CONTACT US</a></li>
</ul>
</div>
</div>
</nav>
<a class="nav-toggle mobile-show">
<span></span>
<span></span>
<span></span>
</a>
</div>
</div>
</div>
</div>
</header>
<div class="fxlayer"></div>
<script>
    $(document).ready(function() {
        function resizeNav() {
            $(".menu").css({
                "height": window.innerHeight
            });
            var radius = Math.sqrt(Math.pow(window.innerHeight, 2) + Math.pow(window.innerWidth, 2));
            var diameter = radius * 2;
            $(".nav-layer").width(diameter);
            $(".nav-layer").height(diameter);
            $(".nav-layer").css({
                "margin-top": -radius,
                "margin-left": -radius
            });
        }
        $(".nav-toggle").click(function() {
            $(".nav-toggle, .nav-layer, .menu").toggleClass("open");
        });
        $(window).resize(resizeNav);
        resizeNav();
    });
</script><!--==========================
    Intro Section
  ============================-->
<section id="intro">
<div class="intro-container">
<div class="carousel slide carousel-fade" data-interval="6000" data-ride="carousel" id="introCarousel">
<ol class="carousel-indicators">
</ol>
<div class="carousel-inner" role="listbox">
<!--  <div class="carousel-item">-->
<!--  <div class="carousel-background"><img src="images/hermitagebanner.jpg" alt="slider"></div>-->
<!--</div>-->
<div class="carousel-item active new-three" onclick="pageRedirect()">
<div class="carousel-background"><img alt="slider" src="images/satyabanner.jpg"/></div>
</div>
<div class="carousel-item new-two" onclick="pageRedirec()">
<div class="carousel-background"><img alt="slider" src="images/satya-hbanner2.jpg"/></div>
</div>
<div class="carousel-item one">
<div class="carousel-background"><img alt="slider" src="images/slider4.jpg"/></div>
</div>
<!--<div class="carousel-item two">-->
<!--  <div class="carousel-background"><img src="images/slider1.jpg" alt="slider"></div>-->
<!--</div>-->
<div class="carousel-item three">
<div class="carousel-background"><img alt="slider" src="images/slider2-2.jpg"/></div>
<div class="slidercontent element">
<h4>Element One</h4>
<span></span>
<h5>the highstreet action begins</h5>
</div>
</div>
<div class="carousel-item four">
<div class="carousel-background"><img alt="slider" src="images/slider2.jpg"/></div>
<div class="slidercontent element blk-clr">
<h4>Element One</h4>
<span></span>
<h5>the highstreet action begins</h5>
</div>
</div>
<div class="carousel-item five">
<div class="carousel-background"><img alt="slider" src="images/slider3.jpg"/></div>
<div class="slidercontent">
<h4> The Hive</h4>
<span></span>
<h5>Bringing it all together at<br/>
              Sector 102, Gurugram</h5>
</div>
</div>
</div>
</div>
<!--<div id="introCarousel" class="carousel  slide carousel-fade mobile-show" data-ride="carousel" data-interval="5000">-->
<!--   <ol class="carousel-indicators">-->
<!--   </ol>-->
<!--   <div class="carousel-inner" role="listbox">-->
<!--     <div class="carousel-item active ">-->
<!--       <div class="carousel-background"><img src="images/mbanner1.jpg" alt="slider"></div>-->
<!--     </div>-->
<!--      <div class="carousel-item">-->
<!--       <div class="carousel-background"><img src="images/mbanner2.jpg" alt="slider">-->
<!--         <div class="slidercontent blk-clr element ">-->
<!--         <h4> The Hermitage</h4>-->
<!--         <span></span>-->
<!--         <h5>Gurugram's New address<br />-->
<!--           for the select few</h5>-->
<!--       </div>-->
<!--       </div>-->
<!--     </div>-->
<!--     <div class="carousel-item ">-->
<!--       <div class="carousel-background"><img src="images/mbanner3.jpg" alt="slider">-->
<!--          <div class="slidercontent element">-->
<!--         <h4>Element One</h4>-->
<!--         <span></span>-->
<!--         <h5>the highstreet action begins</h5>-->
<!--       </div>-->
<!--       </div>-->
<!--     </div>-->
<!--      <div class="carousel-item ">-->
<!--       <div class="carousel-background"><img src="images/mbanner4.jpg" alt="slider">-->
<!--          <div class="slidercontent element">-->
<!--         <h4>Element One</h4>-->
<!--         <span></span>-->
<!--         <h5>the highstreet action begins</h5>-->
<!--       </div>-->
<!--       </div>-->
<!--     </div>-->
<!--      <div class="carousel-item ">-->
<!--       <div class="carousel-background"><img src="images/mbanner5.jpg" alt="slider">-->
<!--        <div class="slidercontent">-->
<!--         <h4> The Hive</h4>-->
<!--         <span></span>-->
<!--         <h5>Bringing it all together at<br />-->
<!--           Sector 102, Gurugram's</h5>-->
<!--       </div>-->
<!--       </div>-->
<!--     </div>-->
<!--     </div>-->
<!--   </div>-->
</div>
<div class="sliderpopupbutton">
<div class="phoneicon">
<a href="tel:18001026510"><img alt="contacticon" class="mobile-show" src="images/contacticon.png"/>
</a>
<img alt="contacticon" class="desktop-show" onclick="openNav()" src="images/contacticon.png"/>
<div class="sidenav" id="mySidenav">
<a class="closebtn" href="javascript:void(0)" onclick="closeNav()">×</a>
<a href="tel:18001026510"><img alt="contacticon" src="images/contacticon.png"/>1800 1026 510 </a>
</div>
</div>
<a href="https://satyagroups.in/blog/" target="_blank"><div class="enquireicon">
<img alt="blog" src="images/blog.png"/>
</div>
</a>
</div> <div class="desktop-show" id="zio"><a href="#donw"><img alt="dropicon" id="donw" src="images/dropicon.png"/></a></div>
</section>
<!-- #intro -->
<!--==========================
      Featured Services Section
    ============================-->
<section class="wow fadeInUp section-bg" id="featured-services">
<div class="aboutus">
<div class="container">
<header class="section-header">
<h1>Satya Group</h1>
</header>
<div class="aboutbox">
<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 pull-left p-0"> <img alt="" class="pull-left" src="images/aboutus.jpg"/> </div>
<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 pull-right p-0">
<div class="abtright">
<h2>THE COMPANY</h2>
<p>Satya Group is a well established real estate organization based in Gurugram. It has well earned its legacy of more than 4 decades, with a zeal for providing enhanced services to customers, and bring a difference to real estate sector. Having delivered over 8 million sq. ft. of commercial and residential projects across India, Satya Group has grown as one of the leading and most dependable names in real estate and property development Delhi NCR. It has successfully delivered residential, commercial and township projects in Delhi NCR (Gurugram), Bathinda and Indore.<br/>
<a class="bttn btn-change8" href="aboutus.php">Read More</a> </p>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- #Project slider -->
<!--==========================
      Project slider
    ============================-->
<section class="wow fadeInUp section-bg" id="projectslider">
<div class="container">
<div class="projectslider">
<header class="section-header">
<h3>Projects</h3>
</header>
<div class="web-container">
<ul class="citytab">
<li><a class="slideractive" id="fanchor" onclick="return tab_change('ftab','stab','ttab','fanchor','sanchor','tanchor')">Gurugram</a></li>
<li><a id="sanchor" onclick="return tab_change('stab','ftab','ttab','sanchor','fanchor','tanchor')">Indore</a></li>
<li><a id="tanchor" onclick="return tab_change('ttab','ftab','stab','tanchor','fanchor','sanchor')">Bathinda</a></li>
</ul>
<div class="owl-carousel owl-theme" id="ftab" style="display:block">
<!-- Loop Start -->
<div class="item">
<div class="pro-box">
<div class="pro-img"> <img alt="THE HERMITAGE" src="control/folder/712391147.jpg"/> </div>
<div class="pro-hndg">
<h5>THE HERMITAGE</h5>
<ul>
<li>
                        STATUS
                        <label>Delivered</label>
</li>
<li>TYPE
                      <label>Residential</label>
</li>
<li>LOCATION
                      <label>Sector 103, Gurugram</label>
</li>
<li>CONFIGURATION
                      <label>2, 3 and 4 BHK Apartments | Penthouses | Villas</label>
</li>
</ul>
<p></p>
<a class="bttn btn-change8" href="project-detail.php?pid=22">Know More </a> </div>
</div>
</div>
<div class="item">
<div class="pro-box">
<div class="pro-img"> <img alt="THE HIVE" src="control/folder/568756103.jpg"/> </div>
<div class="pro-hndg">
<h5>THE HIVE</h5>
<ul>
<li>
                        STATUS
                        <label>Under Construction</label>
</li>
<li>TYPE
                      <label>Commercial</label>
</li>
<li>LOCATION
                      <label>Sector 102, Gurugram</label>
</li>
<li>CONFIGURATION
                      <label>Highstreet Retail, Multiplex, Food Court, Restaurants, Smart Offices And Serviced Apartments</label>
</li>
</ul>
<p></p>
<a class="bttn btn-change8" href="project-detail.php?pid=7">Know More </a> </div>
</div>
</div>
<div class="item">
<div class="pro-box">
<div class="pro-img"> <img alt="ELEMENT ONE TOWER A" src="control/folder/519592285.jpg"/> </div>
<div class="pro-hndg">
<h5>ELEMENT ONE TOWER A</h5>
<ul>
<li>
                        STATUS
                        <label>Delivered</label>
</li>
<li>TYPE
                      <label>Commercial</label>
</li>
<li>LOCATION
                      <label>Sector 47 &amp; 49, Gurugram</label>
</li>
<li>CONFIGURATION
                      <label>High Street Retail &amp; Fully Furnished  Serviced Apartments</label>
</li>
</ul>
<p></p>
<a class="bttn btn-change8" href="project-detail.php?pid=11">Know More </a> </div>
</div>
</div>
<div class="item">
<div class="pro-box">
<div class="pro-img"> <img alt="NORA" src="control/folder/828136104.jpg"/> </div>
<div class="pro-hndg">
<h5>NORA</h5>
<ul>
<li>
                        STATUS
                        <label>Delivered</label>
</li>
<li>TYPE
                      <label>Residential</label>
</li>
<li>LOCATION
                      <label>Sector 103, Gurugram</label>
</li>
<li>CONFIGURATION
                      <label>1 Room Apartments</label>
</li>
</ul>
<p></p>
<a class="bttn btn-change8" href="project-detail.php?pid=24">Know More </a> </div>
</div>
</div>
<div class="item">
<div class="pro-box">
<div class="pro-img"> <img alt="THE LEGEND" src="control/folder/281039017.jpg"/> </div>
<div class="pro-hndg">
<h5>THE LEGEND</h5>
<ul>
<li>
                        STATUS
                        <label>Delivered</label>
</li>
<li>TYPE
                      <label>Residential</label>
</li>
<li>LOCATION
                      <label>Sector 57, Gurugram</label>
</li>
<li>CONFIGURATION
                      <label>Apartments, Penthouses , Villas</label>
</li>
</ul>
<p></p>
<a class="bttn btn-change8" href="project-detail.php?pid=23">Know More </a> </div>
</div>
</div>
<div class="item">
<div class="pro-box">
<div class="pro-img"> <img alt="CENTRUM PLAZA" src="control/folder/590527875.jpg"/> </div>
<div class="pro-hndg">
<h5>CENTRUM PLAZA</h5>
<ul>
<li>
                        STATUS
                        <label>Delivered</label>
</li>
<li>TYPE
                      <label>Commercial</label>
</li>
<li>LOCATION
                      <label>Sector 53, Gurugram</label>
</li>
<li>CONFIGURATION
                      <label>Ultra Modern Offices</label>
</li>
</ul>
<p></p>
<a class="bttn btn-change8" href="project-detail.php?pid=25">Know More </a> </div>
</div>
</div>
<div class="item">
<div class="pro-box">
<div class="pro-img"> <img alt="GALAXY " src="control/folder/257378832.jpg"/> </div>
<div class="pro-hndg">
<h5>GALAXY </h5>
<ul>
<li>
                        STATUS
                        <label>Delivered</label>
</li>
<li>TYPE
                      <label>Commercial</label>
</li>
<li>LOCATION
                      <label>Sector 15, Gurugram</label>
</li>
<li>CONFIGURATION
                      <label>Galaxy</label>
</li>
</ul>
<p></p>
<a class="bttn btn-change8" href="project-detail.php?pid=26">Know More </a> </div>
</div>
</div>
<!-- Loop End  -->
</div>
<div class="owl-carousel owl-theme" id="stab" style="display: none;">
<!-- Loop Start -->
<div class="item">
<div class="pro-box">
<div class="pro-img"> <img alt="MALWA JEWELS" src="control/folder/205394696.jpg"/> </div>
<div class="pro-hndg">
<h5>MALWA JEWELS</h5>
<ul>
<li>
                        STATUS
                        <label>Delivered</label>
</li>
<li>TYPE
                      <label>Residential</label>
</li>
<li>LOCATION
                      <label>AB Bypass Road, Indore</label>
</li>
<li>CONFIGURATION
                      <label>Luxury villas &amp; apartments</label>
</li>
</ul>
<p></p>
<a class="bttn btn-change8" href="project-detail.php?pid=29">Know More </a> </div>
</div>
</div>
<div class="item">
<div class="pro-box">
<div class="pro-img"> <img alt="MALWA COUNTY" src="control/folder/128870203.jpg"/> </div>
<div class="pro-hndg">
<h5>MALWA COUNTY</h5>
<ul>
<li>
                        STATUS
                        <label>Delivered</label>
</li>
<li>TYPE
                      <label>Residential</label>
</li>
<li>LOCATION
                      <label>AB Bypass Road, Indore</label>
</li>
<li>CONFIGURATION
                      <label>Integrated township. Residential commercial, retail &amp; institutional infrastructure.</label>
</li>
</ul>
<p></p>
<a class="bttn btn-change8" href="project-detail.php?pid=27">Know More </a> </div>
</div>
</div>
<div class="item">
<div class="pro-box">
<div class="pro-img"> <img alt="MALWA HEIGHTS" src="control/folder/504488393.jpg"/> </div>
<div class="pro-hndg">
<h5>MALWA HEIGHTS</h5>
<ul>
<li>
                        STATUS
                        <label>Delivered</label>
</li>
<li>TYPE
                      <label>Residential</label>
</li>
<li>LOCATION
                      <label>AB Bypass Road, Indore</label>
</li>
<li>CONFIGURATION
                      <label>Mid rise apartments</label>
</li>
</ul>
<p></p>
<a class="bttn btn-change8" href="project-detail.php?pid=28">Know More </a> </div>
</div>
</div>
<!-- Loop End  -->
</div>
<div class="owl-carousel owl-theme" id="ttab" style="display:none;">
<!-- Loop Start -->
<div class="item">
<div class="pro-box">
<div class="pro-img"> <img alt="CITY CENTRE" src="control/folder/109781516.jpg"/> </div>
<div class="pro-hndg">
<h5>CITY CENTRE</h5>
<ul>
<li>
                        STATUS
                        <label>Delivered</label>
</li>
<li>LOCATION
                      <label>Retail Mall cum Multiplex</label>
</li>
<li>TYPE
                      <label>Commercial Retail</label>
</li>
<li>CONFIGURATION
                      <label>Civil Lines, Bathinda</label>
</li>
</ul>
<p></p>
<a class="bttn btn-change8" href="project-detail.php?pid=30">Know More </a> </div>
</div>
</div>
<!-- Loop End  -->
</div>
</div>
</div>
<!--<div class="pbtn">
    <a href="projects.php" class="bttn btn-change8">View All Projects</a>
    </div>-->
</div>
<!-- Nav tabs -->
</section>
<!-- #Project slider -->
<!--==========================
      footer
    ============================-->
<section class="section-bg" id="footer">
<div class="container">
<header class="section-header">
<h3>Contact</h3>
</header>
<div class="footer">
<div class="inerfooter">
<div class="innerbox">
<img alt="Home" src="images/home.jpg"/>
<p>Plot No.8, Sector-44,<br/>
                        Gurugram, Haryana<br/>
                        122002, India</p>
</div>
<div class="innerbox">
<img alt="phone" src="images/phone.jpg"/>
<p>1800 1026 510, <br/>+91 124 4989300</p>
</div>
<div class="innerbox">
<img alt="Email" src="images/email.jpg"/>
<p><a href="mailto:info@satyadevelopers.com">info@satyadevelopers.com</a></p>
<p><a href="corp.comm@satyadevelopers.com">corp.comm@satyadevelopers.com</a></p>
</div>
<div class="innerbox">
<img alt="location" src="images/location.jpg"/>
<p><a href="https://goo.gl/maps/SJUoe639dbLF9PR58" target="_blank">Google Map</a></p>
</div>
</div>
</div>
<div class="sitemap">
<ul class="accordion" id="accordion2">
<li class="">
<span class="link ftrclr" onclick="myFunction()">SITEMAP<i class="fa fa-chevron-down"></i></span>
<ul class="sitemapdtl" id="myDIV">
<span class="stmp">
<div class="stp-box">
<h4><a href="index">Home</a></h4>
</div>
<div class="stp-box">
<h4><a href="aboutus">Satya Group</a></h4>
<ul>
<li><a href="aboutus#numbers">Corporate Ethos</a></li>
<li><a href="aboutus#leader">Leadership</a></li>
<li><a href="aboutus#teams">Team</a></li>
<li><a href="aboutus#leader">Quality</a></li>
</ul>
</div>
<div class="stp-box">
<h4><a href="">Projects</a></h4>
<ul>
<li>
<span class="set">
<a>Residential <i class="fa fa-plus"></i>
</a>
<ul class="content">
<li><a href="project-detail?pid=22">The Hermitage</a></li>
<li><a href="project-detail?pid=24">Nora</a></li>
<li><a href="project-detail?pid=23">The Legend</a></li>
<li><a href="project-detail?pid=29">Malwa Jewels</a></li>
<li><a href="project-detail?pid=27">Malwa County</a></li>
<li><a href="project-detail?pid=28">Malwa Heights</a></li>
</ul>
</span>
</li>
</ul>
<ul>
<li>
<span class="set">
<a>Commercial <i class="fa fa-plus"></i>
</a>
<ul class="content">
<li><a href="project-detail?pid=7">The Hive</a></li>
<li><a href="project-detail?pid=11">Element One</a></li>
<li><a href="project-detail?pid=25">Centrum Plaza</a></li>
<li><a href="project-detail?pid=30">City Centre</a></li>
</ul>
</span>
</li>
</ul>
<ul>
<li>
<span class="set">
<a>Hospitality <i class="fa fa-plus"></i>
</a>
<ul class="content">
<li><a href="project-detail?pid=26">Galaxy Hotel</a></li>
</ul>
</span>
</li>
</ul>
</div>
<div class="stp-box">
<h4><a href="career">Career</a></h4>
</div>
<div class="stp-box">
<h4><a href="media">Media</a></h4>
<ul>
<li><a href="media">Press Releases</a></li>
<li><a href="events&amp;exhibitions">Events &amp; Exhibitions</a></li>
<li><a href="outdoordisplay">OutDoor Display</a></li>
<li><a href="lifeatsatya">Life at Satya</a></li>
</ul>
</div>
<div class="stp-box">
<h4><a href="nri-corner">NRI Corner</a></h4>
</div>
<div class="stp-box">
<h4><a href="contactus">Contact Us</a></h4>
</div>
</span>
</ul>
</li>
</ul>
</div>
</div>
<div class="copyright">
<div class="container">
<p class="pull-left">
<a href="disclaimer" target="_blank">disclaimer</a>
</p>
<p class="pull-right"><a href="http://triverseadvertising.com" target="_blank">designed &amp; developed by: triverse</a></p>
</div>
</div>
</section>
<script>
    $(document).ready(function() {
        $(".set > a").on("click", function() {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(this)
                    .siblings(".content")
                    .slideUp(200);
                $(".set > a i")
                    .removeClass("fa-minus")
                    .addClass("fa-plus");
            } else {
                $(".set > a i")
                    .removeClass("fa-minus")
                    .addClass("fa-plus");
                $(this)
                    .find("i")
                    .removeClass("fa-plus")
                    .addClass("fa-minus");
                $(".set > a").removeClass("active");
                $(this).addClass("active");
                $(".content").slideUp(200);
                $(this)
                    .siblings(".content")
                    .slideDown(200);
            }
        });
        $('#prmenu').click(function() {
            $('.fxlayer').show();
        });
        $('.fxlayer').click(function() {
            $('#demo').removeClass('show');
            $(this).hide();
        })
        $('.mprojectmenu').click(function() {
            alert('hello');
            $('.promaincat').slideToggle();
        });
    });

    function myFunction() {
        var element = document.getElementById("myDIV");
        element.classList.toggle("mystyle");
    }
</script><script type="text/javascript">
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:25,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
</script>
<script type="text/javascript">
    function tab_change(fid,sid,tid,fanc,sanc,tanc)
    {
        document.getElementById(fid).style.display='block';
        document.getElementById(sid).style.display='none';
        document.getElementById(tid).style.display='none';
        document.getElementById(fanc).className='slideractive';
        document.getElementById(sanc).className='';
        document.getElementById(tanc).className='';
    }
</script>
<script type="text/javascript">
/* Set the width of the side navigation to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "270px";
}

/* Set the width of the side navigation to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
  </script>
<script type="text/javascript">
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("#zio a[href^='#']").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>
<script type="text/javascript">
$(document).ready(function () {
  var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
  
  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });  
});
</script>
<script type="text/javascript">
$(function() {
	var Accordion = function(el, multiple) {
		this.el = el || {};
		this.multiple = multiple || false;

		// Variables privadas
		var links = this.el.find('.link');
		// Evento
		links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
	}

	Accordion.prototype.dropdown = function(e) {
		var $el = e.data.el;
			$this = $(this),
			$next = $this.next();

		$next.slideToggle();
		$this.parent().toggleClass('open');

		if (!e.data.multiple) {
			$el.find('.contnt').not($next).slideUp().parent().removeClass('open');
		};
	}	

	var accordion = new Accordion($('#accordion'), false);
	var accordion2 = new Accordion($('#accordion2'), false);
});
</script>
<!-- JavaScript Libraries -->
<script src="lib/jquery/jquery.min.js"></script>
<script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="lib/superfish/superfish.min.js"></script>
<script src="lib/wow/wow.min.js"></script>
<script src="lib/touchSwipe/jquery.touchSwipe.min.js"></script>
<!-- Template Main Javascript File -->
<script src="js/main.js"></script>
</body>
</html>
