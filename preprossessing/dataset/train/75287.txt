<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml"><!-- #BeginTemplate "/Templates/topictemplatewithhead2.dwt" --><!-- DW6 -->
<head>
<!-- #BeginEditable "doctitle" -->
<title>Graphic Design Schools, Colleges Education</title>
<meta content="Provides graphic design schools, colleges and program information for the budding graphic designer seeking the proper education." name="Description"/>
<meta content="graphic design schools,colleges,education" name="Keywords"/>
<!-- #EndEditable -->
<!-- Place this tag in the <head> of your document -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<mm:editable> </mm:editable> <!-- Dreamweaver will delete this comment. -->
<link href="start.css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,900" rel="stylesheet"/>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-8502382924427656",
          enable_page_level_ads: true
     });
</script>
</head>
<body>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="blacktop2011" width="100%">
<tr>
<td width="50%"> </td>
<td width="328"><a href="http://www.adigitaldreamer.com"><img alt="graphic designer home" border="0" height="116" src="images/11/logo4h.gif" width="100"/></a></td>
<td width="228"><a href="http://www.adigitaldreamer.com"><img alt="A Digital Dreamer Logo" border="0" height="116" src="images/11/logo4t.jpg" width="232"/></a></td>
<td class="blacktop2011b" width="480"><h1><!-- #BeginEditable "Topic Title" --> Graphic Design Schools, Colleges, Education <!-- #EndEditable --><img alt="filler" height="4" src="images/a_filler.gif" width="480"/> </h1>
</td>
<td width="50%"><img height="5" src="images/a_filler.gif" width="260"/>
</td>
</tr>
</table>
<div id="centeringdiv">
<div class="mymenu" id="mymenudiv">
<ul><li><a href="http://www.adigitaldreamer.com">Home</a></li>
<li>Careers
  
   <ul>
<li><a href="http://www.adigitaldreamer.com/video-game-design-information.htm">Game Design</a></li>
<li><a href="http://www.adigitaldreamer.com/graphic-design-information.htm">Graphic Design</a></li>
<li><a href="http://www.adigitaldreamer.com/animation-information.htm">Computer Animation</a></li>
</ul>
</li><li>Schools
    <ul>
<li><a href="http://www.adigitaldreamer.com/game-design-schools.htm">Game Design</a></li>
<li><a href="http://www.adigitaldreamer.com/graphic-design-schools.htm">Graphic Design</a></li>
<li><a href="http://www.adigitaldreamer.com/animation-schools.htm">Computer Animation</a></li>
<li><a href="http://www.adigitaldreamer.com/photography-schools.htm">Photography</a></li>
</ul>
</li>
<li>Free Downloads
  
  <ul>
<li><a href="http://www.adigitaldreamer.com/gallery/">Free Stock Photos</a></li>
<li><a href="http://www.adigitaldreamer.com/freefonts/">Free Fonts</a></li>
<li><a href="http://www.adigitaldreamer.com/freephotoshopbrushes/">Free Photoshop Brush</a></li></ul></li><li><a href="http://www.adigitaldreamer.com/talk-to-us.htm">Contact</a></li>
</ul>
</div>
</div>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="bg" width="975">
<tr>
<td align="center" colspan="3" valign="top"><br/>
<table border="0" cellpadding="0" cellspacing="0" width="90%">
<tr align="right">
<td class="smallish" width="300"> </td>
<td class="smallish" width="100%">
<a href="http://www.adigitaldreamer.com" title="home">Home</a>/ <!-- #BeginEditable "crumbs" --> Graphic Arts Education <!-- #EndEditable --></td>
</tr>
</table>
<table border="0" cellpadding="20" cellspacing="0" class="articlebox2" width="94%">
<tr align="center" valign="top">
<td align="left">
<!-- #BeginEditable "table" -->
<h2 class="tophead">Graphic Design Schools</h2>
<p><img alt="graphic design schools - colleges - education" class="imagesindent" height="144" src="images/010/eyeschools.jpg" width="193"/>There are many<em> <strong>graphic design schools</strong></em> and colleges offering a wide range of degrees and certificates. Below are some training options for those wanting a strong start for a career in graphic design. <br/>
<br/>
            Just request more information to see if one of these schools is a good fit (it's totally free), and get your dream career started in this exciting industry. Before you make any decisions after narrowing down any school programs, make sure to ask important questions like employment rate after graduating the program, connections to the graphic design, industry, and any other questions you might have. Not all schools / campuses are created equally, so get the information you need.<br/>
<br/>
</p>
<h2 class="tophead">Sponsored Graphic Design Schools</h2>
<br/>
<div class="schoolboxes" id="schoolcss4"><span class="schooltitles"><a href="http://o1.qnsr.com/cgi/r?;n=203;c=1471860;s=1069;x=7936;f=201504081706560;u=j;z=TIMESTAMP" target="_blank">Full Sail University</a></span><span class="brightnboldonblack"><b><br/>
<a href="http://o1.qnsr.com/cgi/r?;n=203;c=1471860;s=1069;x=7936;f=201504081706560;u=j;z=TIMESTAMP" id="request" target="_blank" title="Request Information"><span>Request Information</span></a>
<br/>
</b></span><span class="locations"><b>Location:</b></span><span class="descsmallnew"> Florida &amp; Online</span><br/>
<h4><span class="brightnboldonblack"><br/>
<img alt="full sail" class="imagesindent" height="250" src="schools/images/fullsail.jpg" width="350"/></span>Digital Arts and Design:</h4>
<p> You can take your passion for communicating ideas through visual messaging and turn them into a creative future at Full Sail University. Full Sail's Digital Arts &amp; Design bachelor's degree program combines art fundamentals with the design industry's current technology. You will learn how to expand your artistic talents to create powerful images for entertainment, advertising and branding, and the web, through hands-on projects and assignments.</p>
<p class="descsmallnew"> </p>
<p class="descsmallnew"> </p>
</div>
<p> </p>
<p></p>
<div class="qs-listings cachedWidget" id="sl-listings"></div>
<script>

var q=
{
    webSiteName : 'adigitaldreamer.com',
    affiliateKey : '91708',
    tag : '204580591',
    widgetInstanceKey : '25553210',
    id : 'sl-listings',
    templateGroup : 'default',
    baseHref : '//hqx-qmp.quinstreet.com/hqxapi/qsctredirect/',
	clientModel : 'CLICK, PUSH',
    q : 'graphic design',
    customParams :
    {
        tag : '204580591',
        requestInfoUrl : encodeURIComponent('http://www.adigitaldreamer.com/request-gd.htm'),
    },

};


/* Add additional customParams here */



/* Add new optional parameters here */



q.customParams = encodeURIComponent(JSON.stringify(q.customParams));

var qs = '';

var qss = document.createElement('script');

var qshead = document.getElementsByTagName('head')[0];

for(var p in q) qs += p + '=' + q[p] + '&';

qss.src = '//sl-qmp.quinstreet.com/listings?' + qs;

qss.async = true;

qshead.appendChild(qss);

</script>
<br/>
<br/>
<div id="poweredby2183" style="padding-left:80px;"></div><script id="widget_2183" src="//widgets.quinstreet.com/2183/check/5" type="text/javascript"></script><noscript>This widget requires JavaScript to run. <a href="//widgets.quinstreet.com/termsAndConditions">Visit Site for more</a>...</noscript>
<br/>
<div class="schoolboxes" id="schoolcss3"><span class="schooltitles"><a href="http://o1.qnsr.com/cgi/r?;n=203;c=1467132;s=1069;x=7936;f=201503021508260;u=j;z=TIMESTAMP" target="_blank"><img alt="" border="0" class="requestinfo" height="72" src="images/requestinfo.jpg" width="182"/></a><a href="http://o1.qnsr.com/cgi/r?;n=203;c=1467132;s=1069;x=7936;f=201503021508260;u=j;z=TIMESTAMP" target="_blank">The Art Institutes</a></span> <span class="hot"><b>(Hot!)</b></span><span class="brightnboldonblack"><b><br/>
</b></span><span class="locations"><b>Location:</b></span><span class="descsmallnew"> Various Locations Across the US and Canada</span><br/>
<br/>
<p class="brightnboldonblack"><br/>
<b>Program of Graphic Design:</b></p>
<p>Good designers are an interesting breed; they live and breathe in the aesthetic and functional realm. Their discerning eyes notice type treatments on commercials and movie credits. A great-looking chair makes them green with envy. </p>
<p>If you are a kindred soul to this kind of thinking, welcome to design. We know you. You create the interesting visuals that make all the difference in the world.</p>
<p>But first things first. You’ll need an education, taught by faculty who act like clients. Then mentors. Then back again. And you’ll need a portfolio, which will become your all-consuming passion as you’re preparing to graduate. Potential employers will want to see what you can do. You’ll be ready.</p>
<p>Begin a career in Graphic Design today through The Art Institutes' Graphic Design program. </p>
</div>
<!-- #EndEditable -->
</td>
</tr>
<tr align="center" valign="top">
<td align="left"> </td>
</tr>
<tr align="center" valign="top">
<td align="left">
</td>
</tr>
</table>
<br/> <br/>
</td>
<td align="center" bgcolor="#f2f2f2" valign="top" width="255">
<p>
<a href="http://o1.qnsr.com/cgi/r?;n=203;c=1471860;s=1069;x=7936;f=201504081706560;u=j;z=TIMESTAMP" target="_blank"><img alt="Fullsail" height="432" src="images/16/fullsail2.gif" width="235"/></a>
</p><p>
<a href="https://twitter.com/adigitaldreamer" target="_blank"><img border="0" height="55" hspace="3" src="images/socialmedia/twitter.png" width="55"/></a><a href="http://pinterest.com/adigitaldreamer/" target="_blank"><img border="0" height="55" src="images/socialmedia/pintrest.png" width="55"/></a><a href="http://www.facebook.com/wwwadigitaldreamer" target="_blank"><img border="0" height="55" hspace="3" src="images/socialmedia/facebook.png" width="55"/></a><a href="https://plus.google.com/114239509455945922015" target="_blank"><img border="0" height="55" src="images/socialmedia/googleplus.png" width="55"/></a>
</p>
<table border="0" width="92%">
<tr>
<td> <a class="twitter-timeline" data-widget-id="735635591182749698" href="https://twitter.com/adigitaldreamer">Tweets by @adigitaldreamer</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></td>
</tr>
</table>
<br/>
<a href="game-design-schools.htm" target="_blank"><img alt="game art and design schools" border="9" height="82" src="images/13/schools180.jpg" width="180"/><br/>
            Game Art &amp; Design Schools </a>
<p><a href="articles/becomeavideogamedesigner.htm"><img alt="become a game designer" border="9" height="82" src="images/13/becomevgd180.jpg" width="180"/><br/>
            Become a 
            Game Designer</a><br/>
</p>
<p><a href="game-programming-degrees.htm" target="_blank"><img alt="game programming degree" border="9" height="82" src="images/13/programming180.jpg" width="180"/><br/>
            Game Programming Degree</a></p>
<p><a href="animation-schools.htm"><img alt="animation schools" border="9" height="82" src="images/13/animschools180.jpg" width="180"/><br/>
            Computer Animation Schools</a></p>
<p><a href="graphic-design-schools.htm"><br/>
</a><a href="https://plus.google.com/u/0/100582102118405107081?rel=author">Google </a></p>
<p> </p>
<p> <br/>
<br/>
<br/>
</p></td>
</tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="blackb2011" width="100%">
<tr>
<td align="center"><br/>
<table border="0" cellpadding="5" cellspacing="0" class="textbottom" width="760">
<tr class="textbottom">
<td><a href="articles/video-game-testers.htm">Video Game Tester Career</a></td>
<td><a href="http://www.adigitaldreamer.com/graphic-design-information.htm" title="graphic design information">Graphic Design Information</a></td>
<td><a href="http://www.skulpt.com" target="_blank">3D Artist Careers</a></td>
<td><a href="http://www.atozfonts.com" target="_blank" title="A to Z Fonts">A to Z Fonts</a></td>
</tr>
<tr class="textbottom">
<td><a href="http://www.findacareercollege.com/" target="_blank">Find Career Information </a></td>
<td><a href="http://www.digipen.edu">Digipen</a></td>
<td><a href="http://www.adigitaldreamer.com/gallery/" target="_blank">Free Stock Photography</a></td>
<td><a href="game-design-software.htm">Video Game Software</a></td>
</tr>
<tr class="textbottom">
<td><a href="https://www.artstation.com/">Art Station</a></td>
<td><a href="http://www.atozstockimages.com/">Download free stock images</a></td>
<td><a href="animation-schools.htm" title="computer animation schools">3D Animation Training</a><a href="http://www.atozfonts.com" target="_blank" title="A to Z Fonts"></a></td>
<td><a href="https://www.unrealengine.com" title="UE4">UE4</a></td>
</tr>
<!-- #BeginEditable "linkedout" --> <tr>
<td align="center"> </td>
<td><a href="#graphicdesignschools">Graphic Design Schools Colleges Education</a></td>
<td align="center"><a href="http://www.adigitaldreamer.com/graphic-design-schools.htm"></a></td>
<td align="center"> </td>
</tr><!-- #EndEditable -->
</table>
<p><br/>
<span class="tophead"> <!-- #BeginEditable "bottombig" -->Become a <a href="index.html">Graphic Designer </a><!-- #EndEditable --></span><br/>
<br/>
<span class="locations">Copyright © 2019 A Digital Dreamer. All rights reserved.</span> </p>
<p><br/>
</p></td>
</tr>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-1642762-1";
urchinTracker();
</script>
</table>
<!-- Place this tag in your head or just before your close body tag -->
<script src="https://apis.google.com/js/plusone.js" type="text/javascript"></script>
<script async="" defer="" src="//assets.pinterest.com/js/pinit.js"></script>
<script id="widget_5791" src="//sl-qmp.quinstreet.com/widgets/sst5791" type="text/javascript"></script><noscript>This widget requires JavaScript to run. <a href="//widgets.quinstreet.com/termsAndConditions">Visit Site for more</a>...</noscript>
</body>
<!-- #EndTemplate --></html>
