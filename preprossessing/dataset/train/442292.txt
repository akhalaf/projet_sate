<!DOCTYPE html>
<html lang="en">
<head>
<title>Dr Joerg Rhau | Orthopaedic Surgeon Brisbane | Shoulder Surgeon Cleveland, QLD</title>
<meta content="Dr Joerg Rhau is an orthopaedic surgeon who is specialised in shoulder, elbow, wrist and hand surgery. Dr Rhau practices in Cleveland, Beenleigh and Greenslopesin Brisbane, QLD." name="description"/>
<meta content="dr joerg rhau, orthopaedic surgeon, shoulder surgeon" name="keywords"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" name="SKYPE_TOOLBAR"/>
<meta content="width=device-width, initial-scale=1, minimum-scale=1, user-scalable=1" name="viewport"/>
<meta content="AU-QLD" name="geo.region"/>
<meta content="Greenslopes" name="geo.placename"/>
<meta content="-27.508864;153.051576" name="geo.position"/>
<meta content="-27.508864, 153.051576" name="ICBM"/>
<meta content="AU-QLD" name="geo.region"/>
<meta content="Beenleigh" name="geo.placename"/>
<meta content="-27.71747;153.199579" name="geo.position"/>
<meta content="-27.71747, 153.199579" name="ICBM"/>
<meta content="AU-QLD" name="geo.region"/>
<meta content="Cleveland" name="geo.placename"/>
<meta content="-27.53128;153.265889" name="geo.position"/>
<meta content="-27.53128, 153.265889" name="ICBM"/>
<link href="images/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="css/ypo-style.css" media="all" rel="stylesheet"/>
<link href="css/style.css" media="all" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,800" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"/>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/ypo-functions.js"></script>
<script>
    $(document).ready(function(e) {


        $('body').ypoPlugin({


            scroller: true,


            mobileMenu: true,


            slideMenu: false,


            siteMap: true,


            country_code: ['+61'],


            show_country_code: false,


            phone_numbers: ["(07) 3394-4370"],


            phone_info: ["Telephone Number of Dr Joerg Rhau"],


            phone_link: ["general-appointment-shoulder-upper-limb-qld.html"],


            response_banner: false,


            swap_banner: false,


            accordion: true,


            stickyHeader: false,


            stickyHieght: 120,


            onView: false,


            onViewList: (''),


            parallaxEffect: false,


            disableMobileLink: $(''),


            accessibility: true,


            fontResize: true,


            fontResizeList: ('.service-table-right ul ul li, .menu li, .appointment a, .con-list, .profile-content, .profile-content1, .pr-content p, .your-care-list li, .s-surger-content, .we-aim, .therapy-list li a, .resource-list li a, .adrress-1, .adrress-2, .adrress-3, .adrress-head1, .adrress-head2, .adrress-head3, .do-list li a, .quick-links-list li a, .what-do, .quick-links, .website-search, .lang, .follow, .follow-list li a span, .copy, #Footer1 p a, .textMain .service-shoulder-list ul li, .textMain .service-shoulder-list2 ul li, .textMain .service-shoulder-list3 ul li'),


            removeElements: ('#Banner-Container,#Banner-Container-S,#Map-Container, .service-shoulder, .service-elbow, .service-wrist, .service-sports'),


            removeId: ('#Slider1, #flexisel1, #Slider3'),


            removeChildId: (''),


            removeClass: ('rslides'),


            removeAttrClass: (''),


            removeChildClass: ('')


        });


    });

</script>
<!--Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-133866783-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-133866783-1');

</script>
</head>
<body class="home">
<div id="Access-Container"><div id="Accessibility"><div class="dfw-off"><ul><li><a aria-label="close Accessible View" href="javascript:void(0);" id="dfw-close">X</a></li><li><a aria-label="Switch to Accessibility Site" href="?dfw=on" id="dfw-on">Accessibility View <span></span></a></li></ul></div><div class="dfw-on"><ul class="table-div"><li class="table-cell access-links"><a aria-label="Back to Main Site" href="?dfw=off" id="dfw-off"><span></span>Back to Main Site</a> | <a accesskey="k" aria-label="More information about accessibility" href="accessibility.html">Accessibility</a></li><li class="table-cell access-settings"><div class="fontsize">Text Size: <span class="fontminus"><a aria-label="Decrease Font Size" href="javascript:void(0);">A</a></span><span class="fontreset"><a aria-label="Make Font Reset" href="javascript:void(0);">A</a></span><span class="fontplus"><a aria-label="Increase Font Size" href="javascript:void(0);">A</a></span></div><div class="dfw-mode">Contrast: <span class="dfw-whiteMode" id="dfw-WhiteMode"><a aria-label="Normal Text" class="dfw-active" href="javascript:void(0);">C</a></span> | <span class="dfw-blackMode" id="dfw-BlackMode"><a aria-label="High Contrast Text" href="javascript:void(0);">C</a></span></div></li></ul></div></div></div>
<div id="Container">
<header>
<div class="dfw-skip"><a href="#Main-Menu">Skip Header</a></div>
<div id="Header">
<div class="table-div">
<div class="table-cell logo"> <a aria-label="" href="/"><img alt="Dr Joerg Rhau Orthopaedic Surgeon" src="images/dr-joerg-rhau-orthopedic-surgeon.png"/></a> </div>
<div class="table-cell call-appoint">
<!--div class="appoint"><span class="tel1"></span></div-->
<ul class="head-appoint-list">
<li class="appointment"><a class="telehealth-btn" href="telehealth-appointments.html">TeleHealth</a></li>
<li class="appointment"><a href="online-appointment-shoulder-upper-limb-qld.html">Request an appointment</a></li>
<li class="call-number"><span class="tel1"></span></li>
</ul>
<nav>
<div class="dfw-skip"><a href="#Content-Container">Skip Menu</a></div>
<div id="Main-Menu">
<ul class="menu">
<li class="menu-home"><a accesskey="h" href="/"><span>Home</span></a></li>
<li class="menu-about"><a accesskey="u" href="about-shoulder-upper-limb-qld.html">About</a>
<ul>
<li><a href="dr-joerg-rhau-shoulder-upper-limb-qld.html">Dr Joerg Rhau</a></li>
<li><a href="my-team-shoulder-upper-limb-qld.html">My Team</a></li>
<li><a href="contact-us-shoulder-upper-limb-qld.html">Practice Locations</a></li>
</ul>
</li>
<li class="menu-shoulder"><a accesskey="s" href="shoulder-shoulder-upper-limb-qld.html">Shoulder</a>
<ul>
<li><a href="subacromial-impingement-syndrome-shoulder-upper-limb-qld.html">Subacromial Impingement Syndrome</a></li>
<li><a href="rotator-cuff-tear-shoulder-upper-limb-qld.html">Rotator Cuff Tear</a></li>
<li><a href="shoulder-instability-shoulder-upper-limb-qld.html">Shoulder Instability / Shoulder Dislocation</a></li>
<li><a href="long-head-of-biceps-tendinopathy-shoulder-upper-limb-qld.html"> Long Head of Biceps Tendinopathy &amp; SLAP Tear</a></li>
<li><a href="shoulder-osteoarthritis-shoulder-upper-limb-qld.html">Shoulder Osteoarthritis / Glenohumeral Joint Osteoarthritis / Total Shoulder Replacement &amp; Reversed Total Shoulder Replacement</a></li>
<li><a href="frozen-shoulder-shoulder-upper-limb-qld.html">Frozen Shoulder (Adhesive Capsulitis)</a></li>
<li><a href="snapping-scapula-syndrome-shoulder-upper-limb-qld.html">Snapping Scapula Syndrome</a></li>
<!--<li><a href="clavicle-fracture-shoulder-upper-limb-qld.html">Clavicle Fracture</a></li>
                  <li><a href="proximal-humerus-fracture-shoulder-upper-limb-qld.html">Proximal Humerus Fracture</a></li>
                  <li><a href="ac-joint-instability-shoulder-upper-limb-qld.html">AC Joint instability / Acromioclavicular Joint Instability</a></li>-->
<li><a href="shoulder-shoulder-upper-limb-qld.html">Other</a></li>
</ul>
</li>
<li class="menu-elbow"><a accesskey="r" href="elbow-shoulder-upper-limb-qld.html">Elbow</a>
<ul>
<li><a href="epicondylitis-shoulder-upper-limb-qld.html">Epicondylitis / Tennis &amp; Golfer’s Elbow</a></li>
<li><a href="ulnar-neuritis-shoulder-upper-limb-qld.html">Ulnar Neuritis / Cubital Tunnel Syndrome</a></li>
<li><a href="biceps-tendon-tear-shoulder-upper-limb-qld.html">Biceps Tendon Tear</a></li>
<li><a href="elbow-stiffness-loose-bodies-shoulder-upper-limb-qld.html">Elbow Stiffness &amp; Loose Bodies</a></li>
<li><a href="elbow-instability-shoulder-upper-limb-qld.html">Elbow Instability / Posterolateral Rotatory Elbow Instability</a></li>
<li><a href="elbow-arthritis-shoulder-upper-limb-qld.html">Elbow Arthritis</a></li>
<li><a href="olecranon-bursitis-shoulder-upper-limb-qld.html">Olecranon Bursitis</a></li>
<!--<li><a href="fracture-of-radial-head-shoulder-upper-limb-qld.html">Fracture of Radial Head</a></li>
                  <li><a href="fracture-of-distal-humerus-shoulder-upper-limb-qld.html">Fracture of Distal Humerus</a></li>
                  <li><a href="fracture-of-olecranon-shoulder-upper-limb-qld.html">Fracture of Olecranon</a></li>
                  <li><a href="fracture-of-capitellum-shoulder-upper-limb-qld.html">Fracture of Capitellum</a></li>
                  <li><a href="fracture-of-coronoid-process-shoulder-upper-limb-qld.html">Fracture of Coronoid Process</a></li>-->
<li><a href="elbow-shoulder-upper-limb-qld.html">Other</a></li>
</ul>
</li>
<li class="menu-wrist"><a accesskey="n" href="wrist-hand-shoulder-upper-limb-qld.html">Wrist &amp; Hand</a>
<ul>
<li><a href="carpal-tunnel-syndrome-shoulder-upper-limb-qld.html">Carpal Tunnel Syndrome</a></li>
<li><a href="wrist-ganglion-shoulder-upper-limb-qld.html">Wrist Ganglion</a></li>
<li><a href="polyarthritis-fingers-thumb-arthritis-shoulder-upper-limb-qld.html">Polyarthritis of the Fingers / Thumb arthritis</a></li>
<li><a href="carpal-scapholunate-instability-shoulder-upper-limb-qld.html">Carpal &amp; Scapholunate Instability</a></li>
<li><a href="trigger-finger-shoulder-upper-limb-qld.html">Trigger Finger &amp; Trigger Tumb</a></li>
<li><a href="ulnar-wrist-pain-plus-variant-shoulder-upper-limb-qld.html">Ulnar Wrist Pain / Ulnar Plus Variant</a></li>
<li><a href="wrist-hand-shoulder-upper-limb-qld.html">Other</a></li>
</ul>
</li>
<li class="menu-first"><a accesskey="v" href="first-visit-shoulder-upper-limb-qld.html">First Visit</a>
<ul>
<li><a href="surgery-fees-shoulder-upper-limb-qld.html">Surgery Fees</a></li>
<li><a href="consultation-fees-shoulder-upper-limb-qld.html">Consultation fees</a></li>
<li><a href="referral-shoulder-upper-limb-qld.html">Referrals</a></li>
<li><a href="faq-shoulder-upper-limb-qld.html">FAQ</a></li>
</ul>
</li>
<li class="menu-contact"><a accesskey="c" href="contact-us-shoulder-upper-limb-qld.html">Contact</a></li>
</ul>
<div class="clear-float"></div>
</div>
<div id="Mobile-Menu">
<div class="toggleMenu"><a href="javascript:void(0);">Menu<span></span></a></div>
<div class="Wrapper"></div>
</div>
</nav>
</div>
</div>
</div>
</header>
<div class="banner-icn-list"> <a class="ban-cal" href="contact-us-shoulder-upper-limb-qld.html"><span>Practice Locations</span></a> <a class="ban-loc" href="contact-us-shoulder-upper-limb-qld.html"><span>Office Hours</span></a> <a class="ban-time" href="tel:07 3394 4370"><span>Call for Appointment</span></a></div>
<div id="Banner-Container" role="presentation">
<div id="Banner">
<ul class="rslides" id="Slider1">
<li class="banner0">
<div class="theme-L"></div>
<div class="theme-R"></div>
<div class="banner-content">
<div class="banner-head"> Celebrate Freedom</div>
<p> Shoulder &amp; Elbow Joint Pain</p>
<a class="ban-readmore" href="#">Read More <span class="arrow-left"> </span></a> </div>
</li>
<li class="banner1">
<div class="theme-L"></div>
<div class="theme-R"></div>
<div class="banner-content">
<div class="banner-head"> Improve Your Dexterity</div>
<p> Hand Surgery</p>
<a class="ban-readmore" href="#">Read More <span class="arrow-left"> </span></a> </div>
</li>
<li class="banner2">
<div class="theme-L"></div>
<div class="theme-R"></div>
<div class="banner-content">
<div class="banner-head"> Peace of Mind</div>
<p> With the Right Team</p>
<a class="ban-readmore" href="#">Read More <span class="arrow-left"> </span></a> </div>
</li>
<li class="banner3">
<div class="theme-L"></div>
<div class="theme-R"></div>
<div class="banner-content">
<div class="banner-head">Improve Your Dexterity</div>
<p> Hand Surgery</p>
<a class="ban-readmore" href="#">Read More <span class="arrow-left"> </span></a> </div>
</li>
<li class="banner4">
<div class="theme-L"></div>
<div class="theme-R"></div>
<div class="banner-content">
<div class="banner-head"> Regain Mobility</div>
<p> Sporting Injuries</p>
<a class="ban-readmore" href="#">Read More <span class="arrow-left"> </span></a> </div>
</li>
<li class="banner5">
<div class="theme-L"></div>
<div class="theme-R"></div>
<div class="banner-content">
<div class="banner-head"> Peace of Mind</div>
<p> With the Right Team</p>
<a class="ban-readmore" href="#">Read More <span class="arrow-left"> </span></a> </div>
</li>
</ul>
<div class="Banner-Tabs">
<ul>
<li><a href="javascript:void(0);"><span>01</span></a></li>
<li><a href="javascript:void(0);"><span>02</span></a></li>
<li><a href="javascript:void(0);"><span>03</span></a></li>
<li><a href="javascript:void(0);"><span>04</span></a></li>
<li><a href="javascript:void(0);"><span>05</span></a></li>
<li><a href="javascript:void(0);"><span>06</span></a></li>
</ul>
</div>
</div>
<div class="bottom-border"></div>
</div>
<section>
<div id="Service-container">
<div class="dfw-skip"> <a href="#Profile-container">Skip Our Services</a> </div>
<div id="Service-intra">
<div class="service-table">
<div class="service-table-left">
<div class="on-banner-content">
<div class="left-attach"></div>
<div class="title"> Our <span>Services</span> </div>
<div class="Service-Tabs">
<ul class="home-service-list">
<li class="home-sholuder-pain"> <a href="service-shoulder-upper-limb-qld.html#shoulder">Shoulder Pain</a> </li>
<li class="home-elbow-pain"> <a href="service-shoulder-upper-limb-qld.html#elbow">Elbow Pain</a> </li>
<li class="home-sports"> <a href="service-shoulder-upper-limb-qld.html#sporting">Sporting Injuries</a> </li>
<li class="home-wrist"> <a class="home-wrist-a" href="service-shoulder-upper-limb-qld.html#wrist">Wrist &amp; Hand</a> </li>
</ul>
</div>
<div class="ser-arrow-left"></div>
</div>
</div>
<div class="service-table-right">
<div class="right-attach"></div>
<ul class="rslides" id="Slider3">
<li>
<div class="con-list">
<p>Shoulder</p>
<ul>
<li><a href="subacromial-impingement-syndrome-shoulder-upper-limb-qld.html">Subacromial Impingement Syndrome</a></li>
<li><a href="rotator-cuff-tear-shoulder-upper-limb-qld.html"> Rotator Cuff Tear</a></li>
<li><a href="shoulder-instability-shoulder-upper-limb-qld.html"> Shoulder Instability / Shoulder Dislocation</a></li>
<li><a href="frozen-shoulder-shoulder-upper-limb-qld.html"> Frozen Shoulder (Adhesive Capsulitis)</a></li>
<li><a href="snapping-scapula-syndrome-shoulder-upper-limb-qld.html"> Snapping Scapula Syndrome</a></li>
<li><a href="clavicle-fracture-shoulder-upper-limb-qld.html">Clavicle (collar bone) Fracture</a></li>
<li><a href="proximal-humerus-fracture-shoulder-upper-limb-qld.html">Proximal Humerus Fracture</a></li>
<li><a href="ac-joint-instability-shoulder-upper-limb-qld.html">Injury to the AC Joint (Acromioclavicular Joint)</a></li>
<li><a href="scapula-fracture-shoulder-upper-limb-qld.html">Fracture of the Shoulder Blade (Scapula Fracture)</a></li>
<li><a href="arthroscopic-shoulder-surgery-shoulder-upper-limb-qld.html">Arthroscopic Shoulder Surgery (Keyhole surgery)</a></li>
<li><a href="arthroscopic-subacromial-decompression-shoulder-upper-limb-qld.html">Arthroscopic Subacromial Decompression</a></li>
</ul>
<a aria-label="Read More About Shoulder" class="service-readmore" href="service-shoulder-upper-limb-qld.html#shoulder">Read More</a> </div>
</li>
<li>
<div class="con-list">
<p>Elbow</p>
<ul>
<li><a href="epicondylitis-shoulder-upper-limb-qld.html">Epicondylitis / Tennis &amp; Golferâs Elbow</a></li>
<li><a href="ulnar-neuritis-shoulder-upper-limb-qld.html">Ulnar Neuritis / Cubital Tunnel Syndrome</a></li>
<li><a href="biceps-tendon-tear-shoulder-upper-limb-qld.html">Biceps Tendon Tear</a></li>
<li><a href="elbow-stiffness-loose-bodies-shoulder-upper-limb-qld.html">Elbow Stiffness &amp; Loose Bodies</a></li>
<li><a href="elbow-instability-shoulder-upper-limb-qld.html">Elbow Instability / Posterolateral Rotatory Elbow Instability</a></li>
<li><a href="elbow-arthritis-shoulder-upper-limb-qld.html">Elbow Arthritis</a></li>
<li><a href="olecranon-bursitis-shoulder-upper-limb-qld.html">Olecranon Bursitis</a></li>
<li><a href="arthroscopic-elbow-surgery-shoulder-upper-limb-qld.html">Arthroscopic Elbow Surgery (Keyhole surgery)</a></li>
<li><a href="tennis-elbow-surgery-shoulder-upper-limb-qld.html">Tennis Elbow Surgery</a></li>
<li><a href="golfers-elbow-surgery-shoulder-upper-limb-qld.html">Golferâs Elbow Surgery</a></li>
<li><a href="total-elbow-replacement-shoulder-upper-limb-qld.html">Total Elbow Replacement</a></li>
</ul>
<a aria-label="Read More About Elbow" class="service-readmore" href="service-shoulder-upper-limb-qld.html#elbow">Read More</a> </div>
</li>
<li>
<div class="con-list">
<p>Sporting Injuries</p>
<ul>
<li>Sterno-clavicular joint dislocation (SCJ dislocation)</li>
<li>Clavicle (collar bone) fracture</li>
<li>Acromio-clavicular dislocations (ACJ dislocation)</li>
<li>Shoulder dislocation</li>
<li>Greater tuberosity fracture</li>
<li>Proximal humerus fracture</li>
<li>Rotator cuff tear</li>
<li>Long head of biceps tendon rupture</li>
<li>Tennis elbow (Lateral epicondylitis)</li>
<li>Golfer’s elbow (Medial epicondylitis)</li>
<li>Elbow dislocation</li>
</ul>
<a aria-label="Read More About Sporting Injuries" class="service-readmore" href="service-shoulder-upper-limb-qld.html#sporting">Read More</a> </div>
</li>
<li>
<div class="con-list">
<p>Wrist &amp; Hand</p>
<ul>
<li><a href="carpal-tunnel-syndrome-shoulder-upper-limb-qld.html">Carpal Tunnel Syndrome</a></li>
<li><a href="wrist-ganglion-shoulder-upper-limb-qld.html">Wrist Ganglion</a></li>
<li><a href="polyarthritis-fingers-thumb-arthritis-shoulder-upper-limb-qld.html">Polyarthritis of the Fingers / Thumb arthritis</a></li>
<li><a href="carpal-scapholunate-instability-shoulder-upper-limb-qld.html">Carpal &amp; Scapholunate Instability</a></li>
<li><a href="trigger-finger-shoulder-upper-limb-qld.html">Trigger Finger &amp; Trigger Tumb (Stenosing Tenosynovitis)</a> </li>
<li><a href="ulnar-wrist-pain-plus-variant-shoulder-upper-limb-qld.html">Ulnar Wrist Pain / TFCC Tear / Ulnar Plus Variant</a></li>
<li><a href="fractures-wrist-radius-ulna-shoulder-upper-limb-qld.html">Fractures of the wrist (Radius and Ulna)</a></li>
<li><a href="hand-fractures-metacarpal-and-phalangeal-bones-shoulder-upper-limb-qld.html">Hand fractures (Metacarpal and Phalangeal bones)</a></li>
<li><a href="wrist-arthroscopy-shoulder-upper-limb-qld.html">Wrist Arthroscopy</a></li>
<li><a href="wrist-joint-replacement-shoulder-upper-limb-qld.html">Wrist Joint Replacement</a></li>
<li><a href="xiaflex-for-dupuytrens-contracture-shoulder-upper-limb-qld.html">Xiaflex for Dupuytrenâs Contracture</a></li>
</ul>
<a aria-label="Read More About Wrist and Hand" class="service-readmore" href="service-shoulder-upper-limb-qld.html#wrist">Read More</a> </div>
</li>
</ul>
</div>
</div>
</div>
</div>
<div id="Profile-container">
<div class="dfw-skip"> <a href="#Surgery-container">Skip About Doctor</a> </div>
<div id="Profile-intra">
<div class="profile-table">
<div class="profile-cell1">
<p class="about-dr">About Doctor</p>
<h1 class="dr-name">Joerg Rhau <span>orthopaedic surgeon</span></h1>
<p class="profile-content">Dr Joerg Rhau is an internationally trained orthopaedic surgeon specialising in shoulder and upper limb surgery.</p>
<p class="profile-content1">Solid education, extensive experience and a commitment to enhancing quality of life are exemplified by proven patient outcomes.</p>
<a aria-label="Read More About Doctor Joerg Rhau orthopaedic surgeon" class="prf-readmore" href="dr-joerg-rhau-shoulder-upper-limb-qld.html">Read More <span class="arrow-left"></span></a>
<div class="clear-float"></div>
<div class="green-box-border">
<div class="green-box-border1"></div>
</div>
<div class="pr-content">
<p class="pr-content-italic"><i>I'm always extremely excited to see patients returning to a better quality of life following treatment of injury or degenerative condition. I feel privileged to be a part of the journey.</i></p>
<div class="pr-content1"></div>
</div>
</div>
<div class="profile-cell2"> <a aria-label="" href="/"><img alt="Dr Joerg rhau orthopedic surgen" src="images/joerg-rahu.png"/></a>
<div class="pr2-content">
<div class="your-care"> Your <span>Care</span> </div>
<ul class="your-care-list">
<li>Your Care</li>
<li>Expert Surgeon</li>
<li>Dedicated Team</li>
<li>Full Assessment</li>
</ul>
<div class="ser-arrow-left"></div>
</div>
</div>
</div>
</div>
</div>
<div id="Surgery-container">
<div class="dfw-skip"> <a href="#Team-container">Skip Shoulder Surgery</a> </div>
<div id="Surgery-intra">
<div class="surgery-table">
<div class="surgery-cell1">
<div class="sur-left-ban"></div>
</div>
<div class="surgery-cell2">
<div class="surgery-contnent">
<p class="candidate">AM I A CANDIDATE FOR</p>
<p class="s-surger">SHOULDER <span>SURGERY?</span></p>
<p class="s-surger-content">Coming soon</p>
<a aria-label="Take Your Test" class="take-test" href="am-i-a-candidate-shoulder-upper-limb-qld.html">Take your Test <span class="arrow-left"></span></a> </div>
</div>
</div>
</div>
</div>
<div id="Team-container">
<div class="dfw-skip"> <a href="#Resource-container">Skip Multidisciplinary Treatment Team</a> </div>
<div id="Team-intra">
<div class="green-box-border-l"></div>
<p class="m-team">MULTIDISCIPLINARY TREATMENT TEAM</p>
<p class="we-aim">We aim to deliver a coordinated and comprehensive treatment. For further care, patients being treated by Dr Rhau can access our :</p>
<ul class="therapy-list">
<li class="hand-therpy"> <a href="my-team-shoulder-upper-limb-qld.html#hand-therapist">Hand &amp; Occupational<br/>
            Therapist</a> </li>
<li class="physio-therpy"> <a href="my-team-shoulder-upper-limb-qld.html#physiotherapist">Physiotherapist</a> </li>
</ul>
</div>
</div>
<div id="Resource-container">
<div class="dfw-skip"> <a href="#Address-container">Skip Patient Resources</a> </div>
<div id="Resorce-intra">
<p class="patient-resource">Patient Resources</p>
<ul class="resource-list">
<li class="first-visit-guide"> <a href="first-visit-shoulder-upper-limb-qld.html">Patient<br/>
            First Visit Guide</a> </li>
<li class="forms-download"> <a href="forms-downloads-shoulder-upper-limb-qld.html">Forms and<br/>
            Downloads</a> </li>
<!-- <li class="payment"><a href="payment-options-shoulder-upper-limb-qld.html">Payment<br /> -->
<!-- Options</a></li> -->
<li class="urgent-appointment"> <a href="online-appointment-shoulder-upper-limb-qld.html">Urgent<br/>
            Appointment</a> </li>
</ul>
</div>
</div>
<div id="Map-container">
<div id="Map-intra">
<ul class="rslides" id="Slider2">
<li class="banner0">
<div class="theme-L"></div>
<div class="theme-R"></div>
<a class="map-logo" href="#"><img alt="joerg rahu " src="images/map-jorg-rahu.png"/></a> </li>
<li class="banner1">
<div class="theme-L"></div>
<div class="theme-R"></div>
<a class="map-logo" href="#"><img alt="joerg rahu " src="images/map-jorg-rahu.png"/></a> </li>
<li class="banner2">
<div class="theme-L"></div>
<div class="theme-R"></div>
<a class="map-logo" href="#"><img alt="joerg rahu " src="images/map-jorg-rahu.png"/></a> </li>
<li class="banner3">
<div class="theme-L"></div>
<div class="theme-R"></div>
<a class="map-logo" href="#"><img alt="joerg rahu " src="images/map-jorg-rahu.png"/></a> </li>
</ul>
</div>
</div>
<div id="Address-container">
<div class="dfw-skip"> <a href="#Logo-container">Skip Practice Location</a> </div>
<div id="Address-intra">
<div class="Map-Tabs">
<ul class="address-list">
<li class="first-address"> <a href="greenslopes-rooms-shoulder-upper-limb-qld.html">
<div>
<p class="adrress-head1">Greenslopes consulting rooms Brisbane</p>
<p class="adrress-1">Level 1, Suite 4, 631 Logan Road<br/>
                  Greenslopes, QLD 4120<br/>
<span>Direction</span></p>
<p class="border-triangle"></p>
<!--a class="ad-arrow-left" href="#"> </a-->
<p class="border-triangle"></p>
<p class="ad-arrow-left"></p>
</div>
</a> </li>
<li class="second-address"> <a href="cleveland-rooms-shoulder-upper-limb-qld.html">
<div>
<p class="adrress-head2">Cleveland consulting rooms Brisbane</p>
<p class="adrress-2">11/197 Bloomfield St<br/>
                  Cleveland, QLD 4163<br/>
<span>Direction</span></p>
<p class="border-triangle"></p>
<p class="ad-arrow-left"></p>
</div>
</a> </li>
<li class="third-address"> <a href="beenleigh-rooms-shoulder-upper-limb-qld.html">
<div>
<p class="adrress-head3">Beenleigh consulting rooms Brisbane</p>
<p class="adrress-3">Level 1,<br/>
                 147 George St Beenleigh<br/>
<span>Direction</span></p>
<p class="border-triangle"></p>
<p class="ad-arrow-left"></p>
</div>
</a> </li>
<li> <a href="mackay-consulting-shoulder-upper-limb-qld.html">
<div>
<p class="adrress-head4">Mackay Consulting</p>
<p class="adrress-4">Specialist Day Hospital<br/>
                 85 Willetts Rd<br/>
                 Mackay QLD 4740<br/>
<span>Direction</span></p>
<p class="border-triangle"></p>
<p class="ad-arrow-left"></p>
</div>
</a> </li>
</ul>
</div>
</div>
</div>
<div id="Logo-container">
<div class="dfw-skip"> <a href="#Footer-Container">Skip Credibility Logo</a> </div>
<div id="Logo-intra">
<ul id="flexisel1">
<li><a href="https://www.surgeons.org/" rel="nofollow" target="_blank"><img alt="Royal Australasian College of Surgeons" src="images/fracs.png"/></a></li>
<li><a href="https://www.aoa.org.au/" rel="nofollow" target="_blank"><img alt="Australian Orthopaedic Association" src="images/faorthaa.png"/></a></li>
<li><a href="https://www.qldshouldersociety.com.au/" rel="nofollow" target="_blank"><img alt="Queens land shoulder society" src="images/queensland.png"/></a></li>
<li><a href="http://qhssasm.aoa.org.au/" rel="nofollow" target="_blank"><img alt="Queensland Hand Surgery Society" src="images/queensland-hand.png"/></a></li>
</ul>
</div>
</div>
</section>
<footer>
<div id="Footer-Container">
<div id="Footer">
<div>
<p class="what-do">WHAT WE DO</p>
<ul class="do-list">
<li> <a href="shoulder-shoulder-upper-limb-qld.html">Shoulder Pain</a> </li>
<li> <a href="elbow-shoulder-upper-limb-qld.html">Elbow Pain</a> </li>
<li> <a href="sports-injuries-shoulder-upper-limb-qld.html">Sporting Injuries</a> </li>
<li> <a href="wrist-hand-shoulder-upper-limb-qld.html">Wrist &amp; Hand</a> </li>
<li class="rahu-img"><img alt="Joerg Rahu" src="images/footer-rahu.png"/></li>
</ul>
</div>
<div>
<p class="quick-links">Quick links</p>
<ul class="quick-links-list">
<li> <a href="/">Home</a> </li>
<li> <a href="about-shoulder-upper-limb-qld.html">About</a> </li>
<li> <a href="#">Conditions</a> </li>
<li> <a href="#">Advanced Treatments</a> </li>
<li> <a href="referral-shoulder-upper-limb-qld.html">Referrers</a> </li>
<li> <a href="blog.html">Blog</a> </li>
<li> <a href="contact-us-shoulder-upper-limb-qld.html">Contact</a> </li>
<li> <a href="glossary-shoulder-upper-limb-qld.html">Glossary</a> </li>
</ul>
</div>
<div>
<p class="website-search">Website Search</p>
<ul class="website-search-list">
<li class="search">
<div class="ggl-search">
<form action="result.php" class="col-divide" id="newsearch" method="get" name="newsearch">
<label class="nodisplay" for="tfq"><span>newsearch</span></label>
<input class="tftextinput2" id="tfq" name="search" placeholder="Search..." type="text" value=""/>
<button class="tfbutton2" onclick='window.location.assign("result.php?search=" + document.form.search.value.replace(/ /g,"%20"))' style="cursor:pointer;" type="submit" value=""><span>Website Search</span></button>
</form>
</div>
</li>
<!--<li class="drocolia"><a href="https://play.google.com/store/apps/details?id=com.doctoralia&hl=en_IN" target="_blank" rel="nofollow"> <i>Click to download app</i></a></li>-->
</ul>
</div>
<div>
<p class="lang">Language</p>
<div class="ggl-translt">
<div id="google_translate_element"></div>
<div id="google_translate_element"></div>
<script>
                        function googleTranslateElementInit() {
                            new google.translate.TranslateElement({
                                pageLanguage: 'en',
                                layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
                                multilanguagePage: true,
                                autoDisplay: false
                            }, 'google_translate_element');
                            var removePopup = document.getElementById('goog-gt-tt');
                            removePopup.parentNode.removeChild(removePopup);
                        }

                    </script>
<script src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<p class="follow">Follow Us</p>
<ul class="follow-list">
<li class="fb"> <a aria-label="Follow Dr Joerg Rhau Orthopaedic Surgeon on Facebook" href="https://www.facebook.com/DrJoergRhauQLD" rel="nofollow" target="_blank"> <span>Facebook</span> </a> </li>
<li class="tw"> <a aria-label="Follow Dr Joerg Rhau Orthopaedic Surgeon on Twitter" href="https://twitter.com/DrJoergRhau" rel="nofollow" target="_blank"> <span>Twitter</span></a> </li>
<!--<li class="pin"><a href="https://www.pinterest.com/login/" target="_blank" rel="nofollow"> </a></li>-->
<li class="in"> <a aria-label="Follow Dr Joerg Rhau Orthopaedic Surgeon on LinkedIn" href="https://www.linkedin.com/in/joerg-rhau-85a8a1a2" rel="nofollow" target="_blank"> <span>LinkedIn</span></a> </li>
<li class="yt"> <a aria-label="Subscribe Dr Joerg Rhau Orthopaedic Surgeon on Youtube" href="https://www.youtube.com/channel/UCuf82TqgDSdl-XAyz_NQyUQ" rel="nofollow" target="_blank"> <span>Youtube</span></a> </li>
<!--<li class="gl"><a href="https://plus.google.com/" target="_blank" rel="nofollow"> </a></li>-->
</ul>
</div>
</div>
</div>
<div id="Footer-Container1">
<div id="Footer1">
<p class="copy">© Dr Joerg Rhau Orthopaedic Surgeon Greenslopes Shoulder &amp; Upper Limb Clinic, QLD</p>
<p><a href="disclaimer.html">Disclaimer</a> | <a href="privacy.html">Privacy</a> | <a href="medico-legal.html">Medico Legal</a> | <a href="sitemap.html">Sitemap</a> | <a href="feedback.html">Feedback</a> | <a href="tell-a-friend.html">Tell a Friend</a></p>
<div class="ypo-logo"> <a href="https://www.yourpracticeonline.com.au/" target="_blank"><img alt="Your Practice Online" src="images/ypo-logo.png"/></a> </div>
</div>
</div>
</footer>
</div>
<script src="js/responsiveslides.js"></script>
<script src="js/jquery.flexisel.js"></script>
<script src="js/functions.js"></script>
<script src="js/html5.js"></script>
<script src="https://www.ypo.education/js/jsembedcode.js"></script>
<script src="https://popup.yourpractice.online/mJotM14SodI_r9fP7msM7kgGtJP7vkzcFquRhSw9q28/" type="text/javascript"></script>
</body>
</html>
