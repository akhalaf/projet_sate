<!DOCTYPE html>
<html lang="en-us">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Unity WebGL Player | Imagine Math Facts</title>
<link href="TemplateData/style.css" rel="stylesheet"/>
<link href="imgs/IL_favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<script src="TemplateData/bbslider.js"></script>
<script src="TemplateData/bbfader.js"></script>
<script src="TemplateData/UnityProgress.js"></script>
<script src="Build/UnityLoader.js"></script>
<script> var gameInstance = UnityLoader.instantiate("gameContainer", "Build/WebGL.json", {onProgress: JUnityProgress});</script>
</head>
<body class="template">
<p class="header"><span>Unity WebGL Player | </span>Imagine Math Facts</p>
<div class="slider">
<div id="bb-slider"></div>
</div>
<div class="template-wrap clear">
<div id="gameContainer" style="width: 640px; height: 480px; margin: auto"></div>
<br/>
<br/>
<div>
<div class="fullscreen"><img alt="Fullscreen" height="38" onclick="gameInstance.SetFullscreen(1);" src="TemplateData/fullscreen.png" title="Fullscreen" width="38"/></div>
</div>
</div>
<div id="cache-message">
<p>
        It looks like you need to recache.  Please push the button below.
    </p>
<button onclick="clearCache()">ReCache</button>
</div>
<script type="text/javascript">
	var QueryString = function () {
        var query_string = {};
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            if (typeof query_string[pair[0]] === "undefined") {
                query_string[pair[0]] = decodeURIComponent(pair[1]);
            } else if (typeof query_string[pair[0]] === "string") {
                var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
                query_string[pair[0]] = arr;
            } else {
                query_string[pair[0]].push(decodeURIComponent(pair[1]));
            }
        }
        return query_string;
    }();

	var ios = (window.navigator.userAgent.toLowerCase().match(/(ipad)/g) ? true : false) || 
	(navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1);
    if (ios) {
        window.location = ('imaginemathfacts://' + QueryString.token);
    } else if(!QueryString.token) {
        if(window.location.toString().match(/(mathfacts-dev)/gi)){
            window.location = ("https://math-staging-rc.nonprod.imaginelearning.com/" + window.location.search);
        } else {
            window.location = ("https://math.imaginelearning.com" + window.location.search);
        }
    } 
</script>
</body>
</html>