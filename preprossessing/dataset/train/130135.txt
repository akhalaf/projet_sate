<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Antiochian Orthodox Christian Archdiocese of North America</title>
<base href="/"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="favicon.ico" rel="icon" type="image/x-icon"/>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<!--@*GoogleAnalytics*@-->
<!--<script>
    (function (i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments);
      }, i[r].l = 1 * new Date();
      a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m);
    })(window, document, 'script', '/js/analytics.js', 'ga');
    //ga('create', "@ViewBag.GoogleAnalyticsKey", 'auto');
    ga('create', "UA-3058780-4", 'auto');
  </script>-->
<!--@*appInsights*@-->
<!--<script>
    var appInsights = window.appInsights || function (config) {
      function i(config) { t[config] = function () { var i = arguments; t.queue.push(function () { t[config].apply(t, i) }) } } var t = { config: config }, u = document, e = window, o = "script", s = "AuthenticatedUserContext", h = "start", c = "stop", l = "Track", a = l + "Event", v = l + "Page", y = u.createElement(o), r, f; y.src = config.url || "https://az416426.vo.msecnd.net/scripts/a/ai.0.js"; u.getElementsByTagName(o)[0].parentNode.appendChild(y); try { t.cookie = u.cookie } catch (p) { } for (t.queue = [], t.version = "1.0", r = ["Event", "Exception", "Metric", "PageView", "Trace", "Dependency"]; r.length;)i("track" + r.pop()); return i("set" + s), i("clear" + s), i(h + a), i(c + a), i(h + v), i(c + v), i("flush"), config.disableExceptionTracking || (r = "onerror", i("_" + r), f = e[r], e[r] = function (config, i, u, e, o) { var s = f && f(config, i, u, e, o); return s !== !0 && t["_" + r](config, i, u, e, o), s }), t
    }({
       //instrumentationKey: "@ViewBag.AppInsightsKey"
      instrumentationKey: "a00b5f2f-f3f1-45fe-a020-96239b51f159"
    });

    window.appInsights = appInsights;
    appInsights.trackPageView();
  </script>-->
<!--@*NaviGationMenuClose*@-->
<script>
    jQuery(document).ready(function () {
      jQuery(document).click(function (event) {
        var clickover = jQuery(event.target);
        var opened = jQuery(".navbar-collapse").hasClass("navbar-collapse collapse in");
        if (opened === true && !clickover.hasClass("navbar-toggle")) {
          jQuery("button.navbar-toggle").click();
        }
      });
    });
  </script>
<link href="styles.e574d329a06284a6c232.css" rel="stylesheet"/></head>
<body>
<app-root></app-root>
<script src="runtime-es2015.5bc68c0dd8cf137fbe82.js" type="module"></script><script src="polyfills-es2015.bb23b67cfd8558b4d779.js" type="module"></script><script defer="" nomodule="" src="runtime-es5.ee0aae13fb762b150814.js"></script><script defer="" nomodule="" src="polyfills-es5.f565851450cb0aee3fdb.js"></script><script defer="" src="scripts.90d9376b58c8fdaf3102.js"></script><script src="main-es2015.36fe0fba6c56e6de2b4d.js" type="module"></script><script defer="" nomodule="" src="main-es5.207109fb961198ae98da.js"></script></body>
</html>
