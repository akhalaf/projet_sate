<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="all,follow" name="robots"/>
<meta content="index,follow,snippet,archive" name="googlebot"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>            Homepage
    </title>
<meta content="                    Homepage. 
    " name="description"/>
<meta content="" name="keywords"/>
<meta content="Fastop S.r.l." name="author"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet"/>
<!-- Bootstrap and Font Awesome css -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"/>
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Css animations  -->
<link href="/themes/universal/css/animate.css" rel="stylesheet"/>
<link href="/vendor/ekko-lightbox/ekko-lightbox.css" rel="stylesheet"/>
<!-- Theme stylesheet, if possible do not edit this stylesheet -->
<link href="/themes/universal/css/style.default.css" id="theme-stylesheet" rel="stylesheet"/>
<!-- Custom stylesheet - for your changes -->
<link href="/themes/universal/css/custom.css" rel="stylesheet"/>
<!-- Responsivity for older IE -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
<!-- Favicon and apple touch icons-->
<link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="img/apple-touch-icon.png" rel="apple-touch-icon"/>
<link href="img/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="img/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="img/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="img/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="img/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="img/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="img/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<!-- owl carousel css -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" rel="stylesheet"/>
<link href="/themes/universal/css/owl.carousel.css" rel="stylesheet"/>
<link href="/themes/universal/css/owl.theme.css" rel="stylesheet"/>
<link href="/themes/universal/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<!-- rel alternate for internationalization  https://support.google.com/webmasters/answer/189077 -->
<link href="//www.fastop.it/it/" hreflang="it" rel="alternate"/>
<link href="//www.fastop.it/en/" hreflang="en" rel="alternate"/>
<link href="//www.fastop.it/de/" hreflang="de" rel="alternate"/>
<script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-57218130-1', 'auto');
            ga('send', 'pageview');

        </script>
</head>
<body class="boxed section section-1 " data-nav="section-1">
<div id="all">
<header>
<!-- *** TOP ***
_________________________________________________________ -->
<div id="top">
<div class="container-fluid">
<div class="row">
<div class="col-xs-5 payoff">
<p class="">Precision Engineering</p>
<p class="hidden-md hidden-lg"><a data-animate-hover="pulse" href="#"><i class="fa fa-phone"></i></a> <a data-animate-hover="pulse" href="#"><i class="fa fa-envelope"></i></a>
</p>
</div>
<div class="col-xs-7 tools">
<form _lpchecked="1" action="/en/search" class="search" lpformnum="1" method="get">
<div class="input-group">
<input class="form-control" name="search" type="text"/>
<span class="input-group-btn">
<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
</span>
</div>
<!-- /input-group -->
</form>
<div class="language-selector">
<label>language <i class="fa fa-caret-down"></i></label>
<ul id="languages">
<li><a href="//www.fastop.it/it/">IT</a></li>
<li><a href="//www.fastop.it/en/">EN</a></li>
<li><a href="//www.fastop.it/de/">DE</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<!-- *** TOP END *** -->
<!-- *** NAVBAR ***
_________________________________________________________ -->
<div class="navbar-affixed-top" data-offset-top="200" data-spy="affix">
<div class="navbar navbar-default yamm" id="navbar" role="navigation">
<div class="container-fluid">
<div class="navbar-header">
<a class="navbar-brand home" href="/en/">
<img alt="Fastop S.r.l. logo" class="hidden-xs hidden-sm img-responsive" src="https://www.fastop.it/media/cache/hp_brands/rc/4Faeo422//media/brands/1-logofastop150x45.png"/>
<img alt="Fastop S.r.l. logo" class="visible-xs visible-sm img-responsive" src="https://www.fastop.it/media/cache/hp_brands/rc/4Faeo422//media/brands/1-logofastop150x45.png"/><span class="sr-only">Fastop S.r.l. - homepage</span>
</a>
<div class="navbar-buttons">
<button class="navbar-toggle btn-template-main" data-target="#navigation" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<i class="fa fa-align-justify"></i>
</button>
</div>
</div>
<!--/.navbar-header -->
<div class="navbar-collapse collapse" id="navigation">
<ul class="nav navbar-nav navbar-right">
<li class="homepage ">
<a href="/en/">
                            Home
                                                    </a>
</li>
<li class="section section-2 ">
<a href="/en/about-us">
                            About Us
                                                    </a>
</li>
<li class="section section-8 ">
<a href="/en/MachineList">
                            Machine list
                                                    </a>
</li>
<li class="section section-3 dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="/en/quality-system">
                            Quality
                            <span class="caret"></span> </a>
<ul class="dropdown-menu" role="menu">
<li class="section section-3 ">
<a href="/en/quality-system">
                            Quality System
                                                    </a>
</li>
</ul>
</li>
<li class="section section-9 ">
<a href="/en/alloy-surchages-metal-price-trend">
                            Alloy surcharges
                                                    </a>
</li>
<li class="section section-6 ">
<a href="/en/contacts-en">
                            Contacts
                                                    </a>
</li>
</ul>
</div>
<!--/.nav-collapse -->
<div class="collapse clearfix" id="search">
<form class="navbar-form" role="search">
<div class="input-group">
<input class="form-control" placeholder="Search" type="text"/>
<span class="input-group-btn">
<button class="btn btn-template-main" type="submit"><i class="fa fa-search"></i></button>
</span>
</div>
</form>
</div>
<!--/.nav-collapse -->
</div>
</div>
<!-- /#navbar -->
</div>
<!-- *** NAVBAR END *** -->
</header>
<section>
<!-- *** HOMEPAGE CAROUSEL ***
_________________________________________________________ -->
<div class="home-carousel">
<div class="dark-mask"></div>
<div class="container">
<div class="col-md-9 slideshow">
<div class="homepage owl-carousel">
<div class="item">
<div class="row">
<div class="col-sm-12 right">
<img alt="" class="img-responsive" src="https://www.fastop.it/media/cache/hp_slideshow/media/albumImages/1-reparto-fantina-mobile-cnc-800x488.jpeg" title=""/>
<div class="overlay">
<h3>large production capacity</h3>
</div>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-12 right">
<img alt="" class="img-responsive" src="https://www.fastop.it/media/cache/hp_slideshow/media/albumImages/1-cmm-dea-renishaw-900x488.jpeg" title=""/>
<div class="overlay">
<h3>Certified quality system</h3>
<div class="text">we continuously improve our processes</div>
</div>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-12 right">
<img alt="" class="img-responsive" src="https://www.fastop.it/media/cache/hp_slideshow/media/albumImages/1-misurazione-laser-aeroel-900x488.jpeg" title=""/>
<div class="overlay">
<h3>Innovation</h3>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-3 sidebar">
<div class="inner">
<h1>Fastop S.r.l.</h1>
<p></p><p>boasts over 50 years of experience in supplying bespoke precision machined goods.</p>
<p>The company is structured to respond to the needs of all clients, whether small, medium or large, from procuring primary materials, production, cleaning, heat and surface treatments, packing and delivery.</p>
<p> </p>
</div>
</div>
<!-- /.project owl-slider -->
</div>
</div>
<!-- *** HOMEPAGE CAROUSEL END *** -->
</section>
<!-- Footer / End -->
<footer id="footer">
<div class="container">
<div class="col-md-2 col-sm-6">
<a href="/en/">
<img alt="Fastop S.r.l. logo" class="footer-logo hidden-xs hidden-sm img-responsive" src="https://www.fastop.it/media/cache/hp_brands/rc/4Faeo422//media/brands/1-logofastop150x45.png"/>
<img alt="Fastop S.r.l. logo" class="footer-logo visible-xs visible-sm img-responsive" src="https://www.fastop.it/media/cache/hp_brands/rc/4Faeo422//media/brands/1-logofastop150x45.png"/>
<span class="sr-only">Fastop S.r.l. - homepage</span>
</a>
</div>
<div class="col-md-2 col-sm-6">
<h4>Address</h4>

                            via Cal Longa, 8<br/>
            
                            31028 Vazzola (TV)
                - ITALY
            

        </div>
<div class="col-md-2 col-sm-6">
<h4>Contacts</h4>

                            T: +39 0438 740 577<br/>
                                        F: +39 0438 740 560<br/>
</div>
<div class="col-md-2 col-sm-6">
<h4>Email</h4>
<a href="mailto:info@fastop.it">info@fastop.it</a>
</div>
<div class="col-md-2 col-sm-6">
<h4>Corporate Data</h4>

                            VAT.N.: IT03314380266  - SDI: M5UXCR1<br/>
</div>
<div class="col-md-2 col-sm-6">
<h4>Social</h4>
<ul class="socials">
</ul>
</div>
</div>
<!-- /.container -->
</footer>
<!-- /#footer -->
<!-- *** FOOTER END *** -->
<!-- *** COPYRIGHT ***
_________________________________________________________ -->
<div id="copyright">
<div class="container">
<div class="col-md-8">
<p class="pull-left"></p><p>© Fastop S.R.L. controlled and administrated by Immobiliare FAST S.r.l.</p>
</div>
<div class="col-md-4">
<p class="pull-right">Credits <a href="http://www.wysi.it" target="_blank">Sviluppo Web</a> by <a href="http://www.wysi.it" target="_blank">Wysi</a>
<!-- Not removing these links is part of the licence conditions of the template. Thanks for understanding :) -->
</p>
</div>
</div>
</div>
</div>
<script type="text/javascript">
        var markerBase = '/themes/universal/images/';
        var _SIZE_FB_APPID_ = '';
    </script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
        window.jQuery || document.write('<script src="/themes/universal/js/jquery-1.11.0.min.js"><\/script>')
    </script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="/themes/universal/js/jquery.cookie.js"></script>
<script src="/themes/universal/js/waypoints.min.js"></script>
<script src="/themes/universal/js/jquery.counterup.min.js"></script>
<script src="/themes/universal/js/jquery.parallax-1.1.3.js"></script>
<script src="/themes/universal/js/front.js"></script>
<script src="/vendor/parsley/parsley.min.js"></script>
<script src="/vendor/parsley/i18n/en.js"></script> <script src="/vendor/ekko-lightbox/ekko-lightbox.js"></script>
<!-- owl carousel -->
<script src="/themes/universal/js/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="/themes/universal/js/custom.js" type="text/javascript"></script>
<script type="text/javascript">
      document.addEventListener('DOMContentLoaded', function(event) {
        cookieChoices.showCookieConsentBar('This site uses cookies to improve our services and your experience. If you continue browsing, you accept their use',
          'Ok, I agree!', 'Read more', '/en/informativa-privacy-11');
      });
    </script>
<script src="/vendor/cookiechoices/cookiechoices.js"></script>
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
</body>
</html>
