<!DOCTYPE html>
<html lang="en">
<head>
<title>CapitalPlaza</title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="assets/images/favicon.ico" rel="shortcut icon"/>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
<!--Import materialize.css-->
<link href="materialize/css/materialize.min.css" media="screen,projection" rel="stylesheet" type="text/css"/>
<link href="assets/css/style.css" media="screen,projection" rel="stylesheet" type="text/css"/>
<link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<link href="assets/fonts/supermarket/stylesheet.css" rel="stylesheet"/>
<link href="assets/js/lightbox/css/lightbox.css" rel="stylesheet"/>
<link href="assets/js/FullscreenOverlayStyles/css/normalize.css" rel="stylesheet" type="text/css"/>
<link href="assets/js/FullscreenOverlayStyles/css/style6.css" rel="stylesheet" type="text/css"/>
<link href="backoffice/assets/selectize.js-master/dist/css/selectize.bootstrap3.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="overlay overlay-door" style="z-index: 99999; background: rgba(0,0,0,0.8)">
<button class="overlay-close" type="button">Close</button>
<div class="serach_popUP">
<div class="boxContenct" style="width: 0">
<form action="store.php" method="post">
<h6>ค้นหาร้านค้าที่ต้องการ</h6>
<input class="input_contact_box" name="title" type="text" value=""/>
<h6>ค้นหาร้านค้าตามหมวดสินค้า</h6>
<select class="demo-default" id="select-state" multiple="" name="tags[]" placeholder="Select a Tags..." style="width:100%; ">
<option value="">Select a Tag...</option>
<option value="ADAPTOR">ADAPTOR</option><option value="CASE TABLET">CASE TABLET</option><option value="CASEมือถือ">CASEมือถือ</option><option value="COFFEE">COFFEE</option><option value="MOBILE">MOBILE</option><option value="POWER BANK">POWER BANK</option><option value="USB CHARGER">USB CHARGER</option><option value="กบไฟฟ้า">กบไฟฟ้า</option><option value="กรอบเครื่อง">กรอบเครื่อง</option><option value="กระเป๋าดูดทรัพย์">กระเป๋าดูดทรัพย์</option><option value="กระเป๋ามือถือ">กระเป๋ามือถือ</option><option value="กระเป๋าเก็บหูฟัง">กระเป๋าเก็บหูฟัง</option><option value="กระเป๋าโน๊ตบุ๊ค">กระเป๋าโน๊ตบุ๊ค</option><option value="กล้องจุลทรรศน์">กล้องจุลทรรศน์</option><option value="กล้องติดรถ">กล้องติดรถ</option><option value="กาแฟ">กาแฟ</option><option value="กาแฟสด">กาแฟสด</option><option value="กิ๊ฟช็อป">กิ๊ฟช็อป</option><option value="จอ LED">จอ LED</option><option value="จอทัชสกรีน">จอทัชสกรีน</option><option value="จอยสติ๊กติดหน้าจอ">จอยสติ๊กติดหน้าจอ</option><option value="จำหน่ายมือถือ">จำหน่ายมือถือ</option><option value="จิ๊กซอโรตารี่">จิ๊กซอโรตารี่</option><option value="ชุดน็อตประกอบ">ชุดน็อตประกอบ</option><option value="ชุดไขควง">ชุดไขควง</option><option value="ซองใส่ TABLET">ซองใส่ TABLET</option><option value="ซองใส่โทรศัพท์">ซองใส่โทรศัพท์</option><option value="ซ่อมมือถือ">ซ่อมมือถือ</option><option value="ซ่อมอุปกรณ์ IC">ซ่อมอุปกรณ์ IC</option><option value="ซิมการ์ด">ซิมการ์ด</option><option value="ติดฟิล์มกันรอย">ติดฟิล์มกันรอย</option><option value="ตู้ไฟ">ตู้ไฟ</option><option value="ที่วางโทรศัพท์มือถือ">ที่วางโทรศัพท์มือถือ</option><option value="ปลดล็อคเครื่อง">ปลดล็อคเครื่อง</option><option value="พัดลมมือถือ">พัดลมมือถือ</option><option value="ฟิล์มกระจก">ฟิล์มกระจก</option><option value="ฟิล์มกันรอย">ฟิล์มกันรอย</option><option value="มือถือใหม่">มือถือใหม่</option><option value="ลอกจอมือถือ">ลอกจอมือถือ</option><option value="ลำโพงบูลทูธ">ลำโพงบูลทูธ</option><option value="วอชาร์จ">วอชาร์จ</option><option value="สว่านไฟฟ้า">สว่านไฟฟ้า</option><option value="สวิทซ์ไฟ">สวิทซ์ไฟ</option><option value="สายชาร์จ">สายชาร์จ</option><option value="สายแพร">สายแพร</option><option value="สายไฟ">สายไฟ</option><option value="หินเจียร">หินเจียร</option><option value="หูฟัง">หูฟัง</option><option value="อะไหล่ IC">อะไหล่ IC</option><option value="อัพเกรดโปรแกรม">อัพเกรดโปรแกรม</option><option value="อุปกรณ์ภายในเครื่องกรอง">อุปกรณ์ภายในเครื่องกรอง</option><option value="อุปกรณ์มือถือ">อุปกรณ์มือถือ</option><option value="อุปกรณ์ไฟฟ้า">อุปกรณ์ไฟฟ้า</option><option value="อุปกรณ์ไอที">อุปกรณ์ไอที</option><option value="เครื่องกรองน้ำ">เครื่องกรองน้ำ</option><option value="เครื่องกระตุ้นแบต">เครื่องกระตุ้นแบต</option><option value="เครื่องมือซ่อมมือถือ">เครื่องมือซ่อมมือถือ</option><option value="เครื่องมือโรงกลึง">เครื่องมือโรงกลึง</option><option value="เคสมือถือ">เคสมือถือ</option><option value="เคสลายการ์ตูน">เคสลายการ์ตูน</option><option value="เบรคเกอร์">เบรคเกอร์</option><option value="เบอร์มงคล">เบอร์มงคล</option><option value="เปลี่ยนจอมือถือ">เปลี่ยนจอมือถือ</option><option value="เปลี่ยนแบตเตอรี่">เปลี่ยนแบตเตอรี่</option><option value="เมมโมรี่การ์ด">เมมโมรี่การ์ด</option><option value="เลนส์กล้องมือถือ">เลนส์กล้องมือถือ</option><option value="แก้ iCloud">แก้ iCloud</option><option value="แบตสำรอง">แบตสำรอง</option><option value="แบตเตอรี่">แบตเตอรี่</option><option value="แฟลชโปรแกรม">แฟลชโปรแกรม</option><option value="แหวนติดมือถือ">แหวนติดมือถือ</option><option value="ใบเลื่อยวงเดือน">ใบเลื่อยวงเดือน</option><option value="ใส้กรองน้ำ">ใส้กรองน้ำ</option><option value="ไม้ SELFIE">ไม้ SELFIE</option><option value="ไมค์โครโฟน">ไมค์โครโฟน</option>
</select>
<div class="setCenter"> <button class="waves-effect waves-light btn" type="submit">ค้นหา</button> </div>
</form>
</div>
</div>
</div>
<header>
<nav>
<div class="nav-wrapper container">
<a class="brand-logo" href="#" style="height:100%;"><img class="responsive-img" src="assets/images/logo.png" style="height:70%; margin-top:8px;"/></a>
<div class="mini_iconSocial topicon">
<div class="wrapSocial">
<a href="https://line.me/R/ti/p/@capitalplaza" target="_blank">
<div class="itemSocial">
<div class="itemIconSocial line"><img alt="" src="assets/images/line.png"/></div>
</div>
</a>
<a href="https://www.facebook.com/Capital.Klongthom/" target="_blank">
<div class="itemSocial">
<div class="itemIconSocial facebook"><i aria-hidden="true" class="fa fa-facebook"></i></div>
</div>
</a>
<a href="tel:085-176-3377">
<div class="itemSocial">
<div class="itemIconSocial phone"><i aria-hidden="true" class="fa fa-phone"></i></div>
</div>
</a>
<a href="/cdn-cgi/l/email-protection#c9aaa8b9a0bda8a5e7a2a5a6a7aebda1a6a489aea4a8a0a5e7aaa6a4">
<div class="itemSocial">
<div class="itemIconSocial email"><i aria-hidden="true" class="fa fa-envelope-o"></i></div>
</div>
</a>
<a href="https://www.google.com/maps/preview/@13.744721,100.507064,16z" target="_blank">
<div class="itemSocial">
<div class="itemIconSocial location"><img alt="" src="assets/images/placeholder.png"/></div>
</div>
</a>
</div>
</div>
</div>
</nav>
<div class="bgcHeader">
<div class="container menu">
<ul class="menu-list">
<a href="index.php">
<li class="menu_item active_menu">หน้าแรก</li>
</a>
<a href="about.php">
<li class="menu_item">เกี่ยวกับเรา</li>
</a>
<a href="hightlight.php">
<li class="menu_item">ไฮไลท์</li>
</a>
<a href="product.php">
<li class="menu_item">สินค้าและโปรโมชั่น</li>
</a>
<a href="store.php">
<li class="menu_item">ร้านค้า</li>
</a>
<a href="news.php">
<li class="menu_item">ข่าวสาร/ประชาสัมพันธ์</li>
</a>
<a href="activities.php">
<li class="menu_item">กิจกรรม</li>
</a>
<a href="join.php">
<li class="menu_item">ร่วมเป็นส่วนหนึ่งกับเรา</li>
</a>
<a href="contact.php">
<li class="menu_item">ติดต่อเรา</li>
</a>
</ul>
<div class="search" id="trigger-overlay">
<input disabled="true" name="" placeholder="ค้นหาร้าน" type="text" value=""/>
<button class="search_btn"><i class="material-icons">search</i></button>
</div>
</div>
</div>
</header>
<div class="padding_header">
<div class="banner headBanner"></div>
</div>
<div class="titleBar">
<div class="container title">
<img alt="" class="responsive-img" src="assets/images/mascot1.png" style="height:80%"/>
<label for="">Capital Plaza</label>
</div>
</div>
<a data-lightbox="image-1" data-title="Map" href="assets/images/map.jpg">
<div class="banner" style="background-image:url('assets/images/map.svg'); height:450px;"></div>
</a>
<br/>
<section>
<div class="container detail">
<div class="row">
<div class="col s12">
<p>“แคปปิตอล พลาซ่า”  ศูนย์รวมอุปกรณ์มือถือค้าใจกลางย่านคลองถมเสือป่า เราดำเนินงานมากว่า 10 ปี โดยเราได้คัดสรรร้านค้าและสินค้าที่มีคุณภาพมาตรฐานให้บริการมากมายไม่ว่าจะเป็น  อุปกรณ์เสริมมือถือ สายชาต  หูฟัง ลำโพงบูลทูธ  เคสมือถือ  กล้องติดรถ จอมือถือ ซองใส่มือถือ แบตสำรอง ร้านซ่อมมือถือ ฯลฯ เรามีร้านค้าปลีก-ส่งมากมายกว่า 150 ร้านค้าให้ท่านเลือกสรรในราคาถูกสุดๆตามแบบฉบับคลองถม</p>
<div class="col s12 label"> <a href="about.php"><label for="คลิกอ่านเพิ่ม" style="cursor: pointer;">คลิกอ่านเพิ่ม</label></a></div>
</div>
</div>
</div>
</section>
<br/>
<br/>
<section>
<div class="container hightlight_thumb">
<div class="row" style="display: flex;align-items: center;">
<div class="col s5" style="padding-right: 0; width: 38.65%;">
<div class="col s12" style="padding-right: 0;">
<div class="image_thumnail square-element_big" style="background-image:url('backoffice/images/hightlight/desktop/5d0c54f5d4891.jpg')">
<div class="bgOpacity">
<a href="highlight_detail.php?id=22">
<div class="itemTitle">
<h2> โอกาสนี้ไม่ควรพลาดเปิดจองแล้ววันนี้ !! </h2>
</div>
</a>
<p>ผู้ประกอบการที่สนใจเช่าพื้นที่อาคารเพื่อประกอบกิจการค้าขายบนพื้นที่ทำเลทองโอกาสนี้ไม่ควรพลาดสนใจสอบถามได้ที่สำนักงานได้ทุกวันทำการโทร.02-2244955,085-1...</p>
<div class="col s12 label"> <a href="highlight_detail.php?id=22"><label for="คลิกอ่านเพิ่ม" style="color: #dedede; cursor: pointer;">คลิกอ่านเพิ่ม</label></a></div>
</div>
</div>
</div>
</div>
<div class="col s7" style="padding-left: 0">
<div class="col s4">
<div class="image_mini_thumnail square-element" style="  background-image:url('backoffice/images/hightlight/desktop/5d0c7bf41cfe4.jpg')">
<div class="bgOpacity">
<a href="highlight_detail.php?id=18">
<div class="itemTitle">
<h2>Capital Plaza</h2>
</div>
</a>
<p>    </p>
<div class="col s12 label"> <a href="highlight_detail.php?id=18"><label for="คลิกอ่านเพิ่ม" style="cursor: pointer; color: #dedede; font-size: 14px;">คลิกอ่านเพิ่ม</label></a></div>
</div>
</div>
</div>
<div class="col s4">
<div class="image_mini_thumnail square-element" style="  background-image:url('backoffice/images/hightlight/desktop/5d0c7cfbc781c.jpg')">
<div class="bgOpacity">
<a href="highlight_detail.php?id=14">
<div class="itemTitle">
<h2>Capital Plaza</h2>
</div>
</a>
<p></p>
<div class="col s12 label"> <a href="highlight_detail.php?id=14"><label for="คลิกอ่านเพิ่ม" style="cursor: pointer; color: #dedede; font-size: 14px;">คลิกอ่านเพิ่ม</label></a></div>
</div>
</div>
</div>
<div class="col s4">
<div class="image_mini_thumnail square-element" style="  background-image:url('backoffice/images/hightlight/desktop/5d0c7c70b48ba.jpg')">
<div class="bgOpacity">
<a href="highlight_detail.php?id=16">
<div class="itemTitle">
<h2>Capital Plaza</h2>
</div>
</a>
<p></p>
<div class="col s12 label"> <a href="highlight_detail.php?id=16"><label for="คลิกอ่านเพิ่ม" style="cursor: pointer; color: #dedede; font-size: 14px;">คลิกอ่านเพิ่ม</label></a></div>
</div>
</div>
</div>
<div class="col s4">
<div class="image_mini_thumnail square-element" style="margin-top:25px;  background-image:url('backoffice/images/hightlight/desktop/5d0c7cb619512.jpg')">
<div class="bgOpacity">
<a href="highlight_detail.php?id=15">
<div class="itemTitle">
<h2>Capital Plaza</h2>
</div>
</a>
<p>              </p>
<div class="col s12 label"> <a href="highlight_detail.php?id=15"><label for="คลิกอ่านเพิ่ม" style="cursor: pointer; color: #dedede; font-size: 14px;">คลิกอ่านเพิ่ม</label></a></div>
</div>
</div>
</div>
<div class="col s4">
<div class="image_mini_thumnail square-element" style="margin-top:25px;  background-image:url('backoffice/images/hightlight/desktop/5d0c7764c7ffa.jpg')">
<div class="bgOpacity">
<a href="highlight_detail.php?id=17">
<div class="itemTitle">
<h2>Capital Plaza</h2>
</div>
</a>
<p></p>
<div class="col s12 label"> <a href="highlight_detail.php?id=17"><label for="คลิกอ่านเพิ่ม" style="cursor: pointer; color: #dedede; font-size: 14px;">คลิกอ่านเพิ่ม</label></a></div>
</div>
</div>
</div>
<div class="col s4">
<div class="image_mini_thumnail square-element" style="margin-top:25px;  background-image:url('backoffice/images/hightlight/desktop/5d0c7d8f60f79.jpg')">
<div class="bgOpacity">
<a href="highlight_detail.php?id=13">
<div class="itemTitle">
<h2>Capital Plaz</h2>
</div>
</a>
<p></p>
<div class="col s12 label"> <a href="highlight_detail.php?id=13"><label for="คลิกอ่านเพิ่ม" style="cursor: pointer; color: #dedede; font-size: 14px;">คลิกอ่านเพิ่ม</label></a></div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<br/>
<div class="moreicon">
<a href="hightlight.php">
<div class="circle"></div>
<div class="circle"></div>
<div class="circle"></div>
</a>
</div>
<br/>
<div class="titleBar">
<div class="container title">
<img alt="" class="responsive-img" src="assets/images/mascot_01.png" style="height:80%"/>
<label for="" style="color:#d7fae3;">สินค้าและโปรโมชั่น</label>
</div>
</div>
<section>
<div class="container">
<div class="row">
<div class="col s6">
<div class="product">
<div class="container">
<div class="row">
<a href="product_detail.php?id=10">
<div class="col s4 product_img" style="background-image:url('backoffice/images/product/desktop/5990764e46aa5.jpg')">
</div>
</a>
<div class="col s8 product_detail">
<div class="wrapProduct_detail">
<a href="product_detail.php?id=10">
<h5>สวัสดีค่ะ แฟนเพจที่น่ารักทุกคน </h5>
</a>
<p>สวัสดีค่ะแฟนเพจที่น่ารักทุกคนในช่วงนี้หากใครได้ติดตามข่าวสารคงพบจะทราบข่าวเกี่ยวกับการระบาดของ"ไข้ซิกา"กันมาบ้างนะคะโรคนี้ถึงแม้จะไม่เป็นอันตรายมากเท่ากับไข้เลือดออกแต่ก็ยังไม่สามารถป้องกันหรือรักษ...</p>
</div>
<div class="col s12 label"> <label for="คลิกอ่านเพิ่ม"><a href="product_detail.php?id=10">คลิกอ่านเพิ่ม</a></label></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<div class="moreicon">
<a href="product.php">
<div class="circle" style="background-color:#5bc8b3;"></div>
<div class="circle" style="background-color:#5bc8b3;"></div>
<div class="circle" style="background-color:#5bc8b3;"></div>
</a>
</div>
<div class="mini_banner">
<div class="container">
<div class="row">
<div class="col s4"><img class="responsive-img" src="assets/images/message.png"/></div>
<div class="col s4"><img class="responsive-img" src="assets/images/messageLOGO.png"/></div>
<div class="col s4"><img class="responsive-img" src="assets/images/messageTime.png"/></div>
</div>
</div>
</div>
<div class="parallax-container">
<div class="parallax"><img src="assets/images/5-min.png"/></div>
</div>
<div class="titleBar">
<div class="container title">
<img alt="" class="responsive-img" src="assets/images/mascot_06.png" style="height:80%"/>
<label for="" style="color:#f8ed9a;">ร้านค้า</label>
<div class="search_store" style="width: 85%; height: 100%;">
<div class="search_store_pop" style="height: 100%;display: flex;justify-content: end;align-items: center;color: white;float:right;font-size: 26px; cursor: pointer;">
<img class="responsive-img" src="assets/images/search.png" style="height: 60%"/> ค้นหาร้านค้า</div>
</div>
</div>
</div>
<section>
<div class="container">
<div class="row">
<div class="col s6">
<div class="product">
<div class="container">
<div class="row">
<div class="col s4 product_img" style="background-image:url('backoffice/images/store/desktop/59f6ab9077da0.jpg')">
</div>
<div class="col s8 product_detail">
<div class="wrapProduct_detail">
<a href="store_detail.php?id=64">
<h5 style="color:#152e6f;">JOY  SHOP</h5>
</a>
<p>เคสมือถือสวยงามราคาถูกต้องร้านนี้ จำหน่ายเคสมือถือซองใส่มือถือ เคสลายการ์ตูนปลีกและส่งทุกรุ่นทุกยี่ห้อสอบถามข้อมูลเพิ่มเติม 084-6415589 อาคารแคปปิตอลพล่าซ่าชั้น1โซนกลางสินค้าตัว...</p>
</div>
<div class="col s12 label" style="display: flex; align-items: center;">
<div class="col s8 " style="text-align: left; padding: 0;"> <label for="" style="font-size: 16px;"><i aria-hidden="true" class="fa fa-tags"></i>



                        : CASEมือถือ ซองใส่โทรศัพท์ เคสลายการ์ตูน เคสมือถือ </label></div>
<div class="col s4 "> <label for="อ่านเพิ่ม"><a href="store_detail.php?id=64">คลิกอ่านเพิ่ม</a></label></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col s6">
<div class="product">
<div class="container">
<div class="row">
<div class="col s4 product_img" style="background-image:url('backoffice/images/store/desktop/59e9c2519e2ee.jpg')">
</div>
<div class="col s8 product_detail">
<div class="wrapProduct_detail">
<a href="store_detail.php?id=53">
<h5 style="color:#152e6f;">ร้านเฟรนโฟน</h5>
</a>
<p>รับซ่อมโทรศัพท์มือถือทุกรุ่นทุกอาการปลดล็อคเครื่องแก้iCloudแฟลชโปรแกรมรับซ่อมเครื่องตกน้ำซ่อมอุปกรณ์ICเปลี่ยนแบตเปลี่ยนจอมือถือทุกรุ่นเปลี่ยนจอทัสกรีนรับติดฟิล์มกระจกลอกจอมือถือและรับอัพโปรแกรม&amp;nbs...</p>
</div>
<div class="col s12 label" style="display: flex; align-items: center;">
<div class="col s8 " style="text-align: left; padding: 0;"> <label for="" style="font-size: 16px;"><i aria-hidden="true" class="fa fa-tags"></i>



                        : ซ่อมมือถือ ซ่อมอุปกรณ์ IC แก้ iCloud ปลดล็อคเครื่อง เปลี่ยนจอมือถือ </label></div>
<div class="col s4 "> <label for="อ่านเพิ่ม"><a href="store_detail.php?id=53">คลิกอ่านเพิ่ม</a></label></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col s6">
<div class="product">
<div class="container">
<div class="row">
<div class="col s4 product_img" style="background-image:url('backoffice/images/store/desktop/59f6f566e3c30.jpg')">
</div>
<div class="col s8 product_detail">
<div class="wrapProduct_detail">
<a href="store_detail.php?id=76">
<h5 style="color:#152e6f;">ร้านศุภกิจโชติ</h5>
</a>
<p>จำหน่ายอุปกรณ์นำเข้าสว่านไฟฟ้าเลื่อยวงเดือนกบไฟฟ้าหินเจียรปั้มน้ำจิ๊กซอโรตารี่ฯลฯ ตั้งอยู่ภายในอาคารแคปปิตอลพลาซ่าล็อคD1-5สนใจสอบถามข้อมูลเพิ่มเติมได้ที่โทร.081-4826367ตัวอย่างสินค้า...</p>
</div>
<div class="col s12 label" style="display: flex; align-items: center;">
<div class="col s8 " style="text-align: left; padding: 0;"> <label for="" style="font-size: 16px;"><i aria-hidden="true" class="fa fa-tags"></i>



                        : สว่านไฟฟ้า กบไฟฟ้า หินเจียร จิ๊กซอโรตารี่ ใบเลื่อยวงเดือน </label></div>
<div class="col s4 "> <label for="อ่านเพิ่ม"><a href="store_detail.php?id=76">คลิกอ่านเพิ่ม</a></label></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col s6">
<div class="product">
<div class="container">
<div class="row">
<div class="col s4 product_img" style="background-image:url('backoffice/images/store/desktop/59b000768a8cc.jpg')">
</div>
<div class="col s8 product_detail">
<div class="wrapProduct_detail">
<a href="store_detail.php?id=14">
<h5 style="color:#152e6f;">ร้าน BL </h5>
</a>
<p>จำหน่ายอุปกรณ์เสริมมือถือ ราคาปลีก/ราคาส่ง โดยมีสินค้าแบรนด์ของบริษัทแบตสำรองอุปกรณ์เสริมภายในรถยนต์ซองหนังอุปกรณ์เชฟฟี่ สายชาตหูฟังลำโพงบูลทูธและสินค้าอีกมากมายปลอดภัยมั่นใจได้100%...</p>
</div>
<div class="col s12 label" style="display: flex; align-items: center;">
<div class="col s8 " style="text-align: left; padding: 0;"> <label for="" style="font-size: 16px;"><i aria-hidden="true" class="fa fa-tags"></i>



                        : หูฟัง สายชาร์จ แบตสำรอง ไม้ SELFIE USB CHARGER </label></div>
<div class="col s4 "> <label for="อ่านเพิ่ม"><a href="store_detail.php?id=14">คลิกอ่านเพิ่ม</a></label></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col s6">
<div class="product">
<div class="container">
<div class="row">
<div class="col s4 product_img" style="background-image:url('backoffice/images/store/desktop/59f6c21161452.jpg')">
</div>
<div class="col s8 product_detail">
<div class="wrapProduct_detail">
<a href="store_detail.php?id=72">
<h5 style="color:#152e6f;">360' ACCESSORIES</h5>
</a>
<p>จำหน่ายอุปกรณ์เสริมมือถือทุกรุ่นหูฟังบูลทูธสายชาต  ฟิล์มกระจกทุกรุ่นฟิล์มกันรอย  ลำโพงบูลทูธไมค์โครโฟนฯลฯ จำหน่ายทั้งราคาปลีกและส่งอาคารแคปปิตอลพลาซ่าล็อคC1-3-2,E1-1 ส...</p>
</div>
<div class="col s12 label" style="display: flex; align-items: center;">
<div class="col s8 " style="text-align: left; padding: 0;"> <label for="" style="font-size: 16px;"><i aria-hidden="true" class="fa fa-tags"></i>



                        : ฟิล์มกระจก ฟิล์มกันรอย ลำโพงบูลทูธ ไมค์โครโฟน พัดลมมือถือ </label></div>
<div class="col s4 "> <label for="อ่านเพิ่ม"><a href="store_detail.php?id=72">คลิกอ่านเพิ่ม</a></label></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col s6">
<div class="product">
<div class="container">
<div class="row">
<div class="col s4 product_img" style="background-image:url('backoffice/images/store/desktop/59ddd797f3800.jpg')">
</div>
<div class="col s8 product_detail">
<div class="wrapProduct_detail">
<a href="store_detail.php?id=31">
<h5 style="color:#152e6f;">HE MOBILE</h5>
</a>
<p>จำหน่ายอุปกรณ์เสริมมือถือทุกรุ่นหูฟังบูลทูธสายชาตฟิล์มกันรอย ฟิล์มกระจก สายชาร์จเคสมือมือพัดลมมือถือ ลำโพงแบตเตอรี่ฯลฯจำหน่ายทั้งปลีกและส่งราคามิตรภาพ สอบถามข้อมูลเพิ่มเติมโทร.0...</p>
</div>
<div class="col s12 label" style="display: flex; align-items: center;">
<div class="col s8 " style="text-align: left; padding: 0;"> <label for="" style="font-size: 16px;"><i aria-hidden="true" class="fa fa-tags"></i>



                        : ฟิล์มกระจก สายชาร์จ ADAPTOR POWER BANK ลำโพงบูลทูธ </label></div>
<div class="col s4 "> <label for="อ่านเพิ่ม"><a href="store_detail.php?id=31">คลิกอ่านเพิ่ม</a></label></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<div class="moreicon">
<a href="store.php">
<div class="circle" style="background-color:#152e6f;"></div>
<div class="circle" style="background-color:#152e6f;"></div>
<div class="circle" style="background-color:#152e6f;"></div>
</a>
</div>
<section>
<div class="parallax-container bannerText">
<div class="parallax"><img src="assets/images/____IMG_5660.jpg"/></div>
<div class="bgOpacity_bannerText">
<div class="container">
<div class="row">
<div class="col s12">
<p>แคปปิตอล พลาซ่า เราเป็นศูนย์รวมร้าน จำหน่ายอุปกรณ์มือถือที่รวมอยู่ในสถานที่เดียวกัน
                มากที่สุด<br/>บนถนนเจริญกรุง ท่านสามารถหาอุปกรณ์มือถือ และ Gadget ได้ครบ จบในที่เดียว</p>
</div>
</div>
</div>
</div>
</div>
</section>
<div class="titleBar">
<div class="container title">
<img alt="" class="responsive-img" src="assets/images/mascot_04.png" style="height:80%"/>
<label style="color:#eecef7;">ข่าวสาร ประชาสัมพันธ์</label>
</div>
</div>
<br/>
<section>
<div class="container">
<div class="row setTable">
<div class="col s4">
<a href="news_detail.php?id=4" style="color:black; outline: none;">
<div class="news">
<div class="news_img" style="background-image:url('backoffice/images/news/cover/desktop/59909910428fc.jpg')">
</div>
<h5>บริการพื้นที่จอด ที่จอดรถ "คลองถม เซ็นเตอร์"</h5>
<hr/>
<p>"คลองถมเซ็นเตอร์"มีให้บริการพื้นที่สำหรับจอดรถยนต์และรถจักรยานยนต์ที่สะดวกที่สุดและใหญ่ที่สุดในย่านถนนวรจักรพร้อมบริการทุกท่านแล้วที่"คลองถมเซ็นเตอร์"เปิดให้บริการทุกวันตลอด24ชั่วโมง!! มีบริกา...</p>
</div>
</a>
</div>
<div class="col s4">
<a href="news_detail.php?id=11" style="color:black; outline: none;">
<div class="news">
<div class="news_img" style="background-image:url('backoffice/images/news/cover/desktop/5e8461c3cb347.jpg')">
</div>
<h5>CT Contact Center </h5>
<hr/>
<p>CTContactCenterหากคุณกำลังตามหาอุปกรณ์มือถือราคาส่ง...เราช่วยคุณได้!!! จากสถานการณ์การระบาดของ#Covid19เราอาจจะต้องห่างกันสักพักในยุคsocialdistancing การต้องเว้นระยะห่างทางสังคมเช่นนี้แต่เ...</p>
</div>
</a>
</div>
<div class="col s4">
<a href="news_detail.php?id=9" style="color:black; outline: none;">
<div class="news">
<div class="news_img" style="background-image:url('backoffice/images/news/cover/desktop/5caad2cc9466c.jpg')">
</div>
<h5> โอกาสนี้ไม่ควรพลาดเปิดจองแล้ววันนี้ !! </h5>
<hr/>
<p>ผู้ประกอบการที่สนใจเช่าพื้นที่อาคารเพื่อประกอบกิจการค้าขายบนพื้นที่ทำเลทองของผู้ขายโอกาสนี้ไม่ควรพลาดสนใจสอบถามได้ที่สำนักงานได้ทุกวันครับโทร.02-2244955,085-1763377Line:@capitalplaza...</p>
</div>
</a>
</div>
</div>
</div>
</section>
<div class="moreicon">
<a href="news.php">
<div class="circle" style="background-color:#b45dcc;"></div>
<div class="circle" style="background-color:#b45dcc;"></div>
<div class="circle" style="background-color:#b45dcc;"></div>
</a>
</div>
<div class="titleBar">
<div class="container title">
<img alt="" class="responsive-img" src="assets/images/mascot_02.png" style="height:80%"/>
<label for="">กิจกรรม</label>
</div>
</div>
<br/>
<section>
<div class="container">
<div class="row setTable">
<div class="col s4">
<a href="activity_detail.php?id=1" style="color:black; outline: none;">
<div class="news">
<div class="news_img" style="background-image:url('backoffice/images/activity/cover/desktop/598d4ef894611.jpg')">
</div>
<h5>โละ!! ล้างสต็อก </h5>
<hr/>
<p>คลองถมเซ็นเตอร์ โละ!! ล้างสต็อก...</p>
</div>
</a>
</div>
<div class="col s4">
<a href="activity_detail.php?id=7" style="color:black; outline: none;">
<div class="news">
<div class="news_img" style="background-image:url('backoffice/images/activity/cover/desktop/5991bedcb7c98.jpg')">
</div>
<h5>ภาพกิจกรรม</h5>
<hr/>
<p>Cometoexperiencethemarketatmosphere.</p>
</div>
</a>
</div>
</div>
</div>
</section>
<div class="moreicon">
<a href="activities.php">
<div class="circle" style="background-color:#152e6f;"></div>
<div class="circle" style="background-color:#152e6f;"></div>
<div class="circle" style="background-color:#152e6f;"></div>
</a>
</div>
<div class="titleBar">
<div class="container title">
<img alt="" class="responsive-img" src="assets/images/mascot_03.png" style="height:80%"/>
<label for="">ร่วมเป็นส่วนหนึ่งกับเรา</label>
</div>
</div>
<div class="banner" style="background-image:url('assets/images/IMG_5615.jpg'); height:600px; background-position:bottom;"></div>
<br/>
<section>
<div class="container detail">
<div class="row">
<div class="col s12 we_detail" style="text-align: left;">
<p style="text-align:left">
</p><div><br/></div><div><br/></div><div>รวมเติบโตไป กับ “แคปปิตอล พลาซ่า” <br/></div><div style="text-align: left;"> สำหรับท่านที่สนใจพื้นที่ค้าขายทำเลทองใจกลางกรุงเทพฯ แคปปิตอล พลาซ่า เป็นทำเลที่ท่านกำลังมองหาอยู่ ด้วยทำเลย่านการค้าเก่าแก่คลองถม เสือป่า ที่มีผู้คนคึกคัก หนาแน่น เดินทางสะดวก พร้อมทั้งมั่นใจได้กับทีมงานผู้บริหารมืออาชีพกับประสบการณ์การบริหารตลาด “คลองถม เซ็นเตอร์ ” มากว่า 20 ปี

หากท่านมีข้อสงสัย หรือว่าอยากได้คำแนะนำในการทำธุรกิจ ทาง แคปปิตอล พลาซ่ายินดีให้คำปรึกษากับท่าน ท่านสามารถติดต่อเพื่อเข้าเยี่ยมชมพื้นที่ได้ที่ โทร.02-2244955 , 085-1763377 หรือ Line ID: @capitalplaza</div><div style="text-align: left;"><br/></div><div style="text-align: center;"> มาร่วมเป็นส่วนหนึ่งและเติบโตไปกับเราที่ แคปปิตอล พลาซ่า  <br/></div>
</div>
</div>
</div>
<div class="moreicon">
<a href="join.php">
<div class="circle"></div>
<div class="circle"></div>
<div class="circle"></div>
</a>
</div>
</section>
<div class="fixed-action-btn" style="display: none;">
<a class="btn-floating btn-large grey totop">
<i class="large material-icons">arrow_upward</i>
</a>
</div>
<footer>
<div class="footer_bar">
<div class="container">
<din class="row">
<div class="col s12">
<img alt="" class="responsive-img" src="assets/images/Asset1.png"/>
</div>
</din>
</div>
</div>
<div class="footer_detail">
<div class="container">
<div class="row">
<div class="col s4">
<p style="color:#152e70">บริษัท แคปปิตอลพลาซ่า จำกัด<br/> เลขที่ 311 ถนนเจริญกรุง 17<br/> แขวงป้อมปราบ
              เขตป้อมปราบศัตรูพ่าย <br/> กรุงเทพมหานคร

              10100</p>
</div>
<div class="col s4">
<p style="color:#152e70">Mobile : 085-176-3377<br/> Office : 02-224-3635<br/> Email :
              <a class="__cf_email__" data-cfemail="482b2938213c292466232427262f3c202725082f25292124662b2725" href="/cdn-cgi/l/email-protection">[email protected]</a><br/> Website : www.capitalplaza.co.th</p>
</div>
<div class="col s4 mini_iconSocial">
<h6 style="color:#152e70">Website : www.capitalplaza.co.th </h6>
<div class="wrapSocial">
<a href="https://line.me/R/ti/p/@capitalplaza" target="_blank">
<div class="itemSocial">
<div class="itemIconSocial line"><img alt="" src="assets/images/line.png"/></div>
</div>
</a>
<a href="https://www.facebook.com/Capital.Klongthom/" target="_blank">
<div class="itemSocial">
<div class="itemIconSocial facebook"><i aria-hidden="true" class="fa fa-facebook"></i></div>
</div>
</a>
<a href="tel:085-176-3377">
<div class="itemSocial">
<div class="itemIconSocial phone"><i aria-hidden="true" class="fa fa-phone"></i></div>
</div>
</a>
<a href="/cdn-cgi/l/email-protection#3b585a4b524f5a5715505754555c4f5354567b5c565a525715585456">
<div class="itemSocial">
<div class="itemIconSocial email"><i aria-hidden="true" class="fa fa-envelope-o"></i></div>
</div>
</a>
<a href="https://www.google.com/maps/preview/@13.744721,100.507064,16z" target="_blank">
<div class="itemSocial">
<div class="itemIconSocial location"><img alt="" src="assets/images/placeholder.png"/></div>
</div>
</a>
</div>
</div>
<br/>
</div>
</div>
</div>
<div class="footerCopy">
<p>Copyright <span style="font-family:sans-serif; color:#FF962F">©</span> 2017 CAPITAL PLAZA Co., LTD. All
        Right



        Reserved</p>
</div>
</footer>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="materialize/js/materialize.min.js" type="text/javascript"></script>
<script src="assets/js/myScript.js" type="text/javascript"></script>
<script src="assets/js/lightbox/js/lightbox.js" type="text/javascript"></script>
<script src="assets/js/FullscreenOverlayStyles/js/modernizr.custom.js"></script>
<script src="assets/js/FullscreenOverlayStyles/js/classie.js"></script>
<script src="assets/js/FullscreenOverlayStyles/js/demo1.js"></script>
<script src="backoffice/assets/selectize.js-master/dist/js/standalone/selectize.js" type="text/javascript"></script>
<script>
    function adjustHeight() {



      var myWidth = jQuery('.square-element').width();



      var myHeight = myWidth + 'px';



      jQuery('.square-element').css('height', myHeight);



      return myHeight;



    }











    function adjustHeight_big() {



      var myWidth = jQuery('.square-element_big').width();



      var myHeight = myWidth + 'px';



      jQuery('.square-element_big').css('height', myHeight);



      return myHeight;



    }











    // calls adjustHeight on window load



    jQuery(window).load(function () {



      adjustHeight();



      adjustHeight_big();



    });







    // calls adjustHeight anytime the browser window is resized



    jQuery(window).resize(function () {



      adjustHeight();



      adjustHeight_big();



    });
  </script>
<script>
    $(document).ready(function () {



      $('.we_detail div br').remove();



      $('#select-state').selectize({



        plugins: ['remove_button'],



        delimiter: ',',



        persist: false



      });



    });
  </script>
</body>
</html>