<!DOCTYPE html>
<html lang="en-GB">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-108552646-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-108552646-1');
</script>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>Page not found  |  </title>
<link href="https://www.csrnet.org.uk/wp-content/themes/csr/style.css" rel="stylesheet" type="text/css"/>
<meta content="initial-scale=1" name="viewport"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<link href="https://www.csrnet.org.uk/wp-content/themes/csr/favicon.ico" rel="shortcut icon"/>
<link href="//ajax.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.csrnet.org.uk/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.4" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Lora%3A400%2C700%7COpen+Sans%3A400%2C700%2C400i%2C600&amp;ver=4.9.16" id="googleFonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css?ver=4.9.16" id="fontAwsome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.csrnet.org.uk/wp-json/" rel="https://api.w.org/"/>
<style id="wp-custom-css" type="text/css">
			.row-slider .middle-text h1 {
    margin: auto;
}
.slide .middle.middle-text {
    max-width: 1000px;
}
.row-heading .logo img {
    max-width: 250px;
}		</style>
</head>
<body class="error404">
<div class="row row-heading">
<div class="wrap">
<div class="full logo text-center">
<a href="https://www.csrnet.org.uk" title="Council For Social Responsibility"><img alt="Council For Social Responsibility Logo" src="https://www.csrnet.org.uk/wp-content/themes/csr/images/logo-csr.png"/></a>
</div>
<div class="full site-title">
<p class="site-desc">Sustainable Faith Based Social Action</p>
</div>
</div>
</div>
<div class="row site-navigation ">
<nav class="main-navigation wrap" role="navigation">
<a class="burger-button" href="#" id="burgerButton">
<span class="burger-icon"></span> <span>MENU</span>
</a>
<div class="menu-main-menu-container"><ul class="menu" id="menu-main-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-153" id="menu-item-153"><a href="/programme/">Projects 2020</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-taxonomy menu-item-object-programme-category menu-item-305" id="menu-item-305"><a href="https://www.csrnet.org.uk/programme-category/social-action/">Social Action</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-programme-category menu-item-306" id="menu-item-306"><a href="https://www.csrnet.org.uk/programme-category/social-business/">Social Business</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-programme-category menu-item-307" id="menu-item-307"><a href="https://www.csrnet.org.uk/programme-category/social-engagement/">Social Engagement</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-programme-category menu-item-308" id="menu-item-308"><a href="https://www.csrnet.org.uk/programme-category/social-transformation/">Social Transformation</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32" id="menu-item-32"><a href="https://www.csrnet.org.uk/whats-new/">What’s New</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31" id="menu-item-31"><a href="https://www.csrnet.org.uk/resources/">Resources</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30" id="menu-item-30"><a href="https://www.csrnet.org.uk/contact/">Contact</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-487" id="menu-item-487"><a href="https://www.csrnet.org.uk/whats-new/vacancies/">Vacancies</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3450" id="menu-item-3450"><a href="https://www.csrnet.org.uk/safeguarding/">Safeguarding</a></li>
</ul></div> </nav><!-- .site-navigation .main-navigation -->
</div>
<div class="row row-buttons">
<div class="wrap">
<a class="btn btn-yellow trans" href="https://www.csrnet.org.uk/who-we-are/">Who are we</a>
<a class="btn btn-blue trans" href="https://www.csrnet.org.uk/programme/">Our Programmes</a>
</div>
</div>
<div class="row row-download">
<div class="wrap">
<h3>Download the latest version of our full RDP guide and see the support we offer and how others are using it.</h3>
<div class="d1-2 no-pad">
<p class="guide-text"><span>Free</span> CSR Rapid Development Guide</p>
</div>
<div class="d1-2 no-pad">
<form class="subscribe">
<span class="guide-email">Email Address</span>
<input class="guide-input" placeholder="john.smith@example.com"/>
<button class="guide-btn trans">-&gt;</button>
</form>
</div>
<div class="clearfix"></div>
<p class="guide-fn">*Your details will not be shared with any third parties organisations.</p>
</div>
</div>
<div class="row row-events">
<h2>News &amp; Events</h2>
<div class="wrap">
<div class="d1-5">
<a href="https://www.csrnet.org.uk/st-ms-2021/">
<h3>St M’s 2021</h3>
<p>Set up with lottery funding in 2019, one of the plans for St M’s 2021 is to launch a safe,</p>
</a>
<nav class="cats"><span>Category:</span><br/><a href="https://www.csrnet.org.uk/category/engagement/" rel="category tag">Engagement</a>, <a href="https://www.csrnet.org.uk/category/enterprise/" rel="category tag">Enterprise</a></nav>
</div>
<div class="d1-5">
<a href="https://www.csrnet.org.uk/looking-forward-to-2021/">
<h3>Looking forward to 2021</h3>
<p>As we enter a new year, CSR welcomes a new chair Professor Sir Jonathan Montgomery, with a passion for social</p>
</a>
<nav class="cats"><span>Category:</span><br/><a href="https://www.csrnet.org.uk/category/action/" rel="category tag">Action</a></nav>
</div>
<div class="d1-5">
<a href="https://www.csrnet.org.uk/covid-19/">
<h3>Covid-19</h3>
<p>With the world in these unprecedented times, we wanted to advise that as a social action charity, CSR remain operational</p>
</a>
<nav class="cats"><span>Category:</span><br/><a href="https://www.csrnet.org.uk/category/action/" rel="category tag">Action</a></nav>
</div>
<div class="d1-5">
<a href="https://www.csrnet.org.uk/gnn-annual-event-2019/">
<h3>GNN ANNUAL EVENT 2019</h3>
<p>GOOD NEIGHBOURS ANNUAL EVENT 2019 WHEN: Wednesday 16th October 2018 10am -4pm WHERE: Hope Church, Middle Brook Street, Winchester SO23</p>
</a>
<nav class="cats"><span>Category:</span><br/><a href="https://www.csrnet.org.uk/category/engagement/" rel="category tag">Engagement</a></nav>
</div>
<div class="d1-5">
<a href="https://www.csrnet.org.uk/horsebox-cafe-holy-roast/">
<h3>Horsebox Cafe – Holy Roast</h3>
<p>Our pop up coffee horse box engages church and communities over a cuppa and cake and can be safely operated</p>
</a>
<nav class="cats"><span>Category:</span><br/><a href="https://www.csrnet.org.uk/category/enterprise/" rel="category tag">Enterprise</a></nav>
</div>
</div>
</div>
<div class="row row-bottom-extras">
<div class="wrap">
<div class="d1-3 no-pad trans">
<a href="https://www.csrnet.org.uk/programme/">
<h3>LIVE PROJECTS</h3>
<img alt="b3" height="240" src="https://www.csrnet.org.uk/wp-content/uploads/2016/12/b3-420x240.jpg" width="420"/>
<div class="overlay trans">
<div class="middle">
<h4>View All</h4>
<p></p>
</div>
</div>
</a>
</div>
<div class="d1-3 no-pad trans">
<a href="https://www.csrnet.org.uk/resources/">
<h3>Resources</h3>
<img alt="b2" height="240" src="https://www.csrnet.org.uk/wp-content/uploads/2016/12/b2-420x240.jpg" width="420"/>
<div class="overlay trans">
<div class="middle">
<h4>View All</h4>
<p></p>
</div>
</div>
</a>
</div>
<div class="d1-3 no-pad trans">
<a href="https://www.csrnet.org.uk/category/films/">
<h3>Films</h3>
<img alt="good-neighbours-network" height="240" src="https://www.csrnet.org.uk/wp-content/uploads/2017/05/good-neighbours-network-420x240.jpg" width="420"/>
<div class="overlay trans">
<div class="middle">
<h4>View All</h4>
<p>'Good Neighbours Movie’ just released</p>
</div>
</div>
</a>
</div>
</div>
</div>
<footer class="row row-footer">
<a href="https://www.csrnet.org.uk" title="Council For Social Responsibility"><img alt="Council For Social Responsibility Logo" src="https://www.csrnet.org.uk/wp-content/themes/csr/images/logo-csr-footer.png"/></a>© 2021 - A company limited by guarantee Registered in London No. 7717141 Charity No. 1145162
</footer>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>
<script src="https://www.csrnet.org.uk/wp-content/themes/csr/js/scripts.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.csrnet.org.uk\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://www.csrnet.org.uk/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.4" type="text/javascript"></script>
</body>
</html>
