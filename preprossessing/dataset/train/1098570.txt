<!DOCTYPE html>
<html dir="ltr" lang="es-MX">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="Drive4Marketing" name="author"/>
<link href="images/favicon.png" rel="shortcut icon" type="image/png"/>
<!-- Stylesheets
    ============================================= -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="style.css" rel="stylesheet" type="text/css"/>
<link href="css/dark.css" rel="stylesheet" type="text/css"/>
<link href="css/font-icons.css" rel="stylesheet" type="text/css"/>
<link href="css/animate.css" rel="stylesheet" type="text/css"/>
<link href="css/magnific-popup.css" rel="stylesheet" type="text/css"/>
<link href="css/tabs.css" rel="stylesheet" type="text/css"/>
<link href="css/responsive.css" rel="stylesheet" type="text/css"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<!--[if lt IE 9]>
    	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
<!-- External JavaScripts
    ============================================= -->
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/plugins.js" type="text/javascript"></script>
<script src="js/instafeed.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var feed = new Instafeed({
        get: 'user',
        userId: 1430423920,
        limit: '6',
        resolution: 'low_resolution',
        accessToken: '1430423920.467ede5.4f1a7fb0f56e402e84b347f445d82a07'
    });
    feed.run();
    </script>
<script type="text/javascript">
    $(function () {
    $("[name=toggler]").click(function () {
        $("[name=toggler]").removeClass('active');
        $(this).addClass('active');
        $('.toHide').hide();
        $("#blk-" + $(this).val()).fadeIn();
    });
    });
    </script>
<!-- Document Title
    ============================================= -->
<title>Pappel'O</title>
</head>
<body class="stretched sticky-responsive-menu">
<!-- Document Wrapper
    ============================================= -->
<div class="clearfix" id="wrapper">
<div class="page-section" id="home" style="position:absolute;top:0;left:0;width:100%;height:200px;z-index:-2;"></div>
<section class="slider-parallax full-screen with-header swiper_wrapper clearfix" id="slider">
<div class="swiper-container swiper-parent">
<div class="iconos">
<a href="https://www.facebook.com/pages/Pappelo-Social-Scrapbooking/119234724927731?fref=ts" target="_blank"><img src="images/graficos/facebook-icon.png" style="position:absolute;top:30px;right:180px;z-index:99999;"/></a>
<a href="https://instagram.com/pappelomty/" target="_blank"> <img src="images/graficos/instagram-icon.png" style="position:absolute;top:30px;right:120px;z-index:99999;"/></a>
<a href="https://www.pinterest.com/pappelomty/" target="_blank"><img src="images/graficos/pinterest-icon.png" style="position:absolute;top:30px;right:60px;z-index:99999;"/></a>
</div>
<div class="swiper-wrapper">
<div class="swiper-slide dark" style="background-image: url('images/graficos/sliderhome001.jpg');"></div>
<div class="swiper-slide dark" style="background-image: url('images/graficos/sliderhome002.jpg');"></div>
</div>
<div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
<div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
</div>
<script>
                jQuery(document).ready(function($){
                    var swiperSlider = new Swiper('.swiper-parent',{
                        paginationClickable: false,
                        slidesPerView: 1,
                        grabCursor: true,
                        onSwiperCreated: function(swiper){
                            $('[data-caption-animate]').each(function(){
                                var $toAnimateElement = $(this);
                                var toAnimateDelay = $(this).attr('data-caption-delay');
                                var toAnimateDelayTime = 0;
                                if( toAnimateDelay ) { toAnimateDelayTime = Number( toAnimateDelay ) + 750; } else { toAnimateDelayTime = 750; }
                                if( !$toAnimateElement.hasClass('animated') ) {
                                    $toAnimateElement.addClass('not-animated');
                                    var elementAnimation = $toAnimateElement.attr('data-caption-animate');
                                    setTimeout(function() {
                                        $toAnimateElement.removeClass('not-animated').addClass( elementAnimation + ' animated');
                                    }, toAnimateDelayTime);
                                }
                            });
                        },
                        onSlideChangeStart: function(swiper){
                            $('#slide-number-current').html(swiper.activeIndex + 1);
                            $('[data-caption-animate]').each(function(){
                                var $toAnimateElement = $(this);
                                var elementAnimation = $toAnimateElement.attr('data-caption-animate');
                                $toAnimateElement.removeClass('animated').removeClass(elementAnimation).addClass('not-animated');
                            });
                        },
                        onSlideChangeEnd: function(swiper){
                            $('#slider .swiper-slide').each(function(){
                                if($(this).find('video').length > 0) { $(this).find('video').get(0).pause(); }
                            });
                            $('#slider .swiper-slide:not(".swiper-slide-active")').each(function(){
                                if($(this).find('video').length > 0) {
                                    if($(this).find('video').get(0).currentTime != 0 ) $(this).find('video').get(0).currentTime = 0;
                                }
                            });
                            if( $('#slider .swiper-slide.swiper-slide-active').find('video').length > 0 ) { $('#slider .swiper-slide.swiper-slide-active').find('video').get(0).play(); }
                            $('#slider .swiper-slide.swiper-slide-active [data-caption-animate]').each(function(){
                                var $toAnimateElement = $(this);
                                var toAnimateDelay = $(this).attr('data-caption-delay');
                                var toAnimateDelayTime = 0;
                                if( toAnimateDelay ) { toAnimateDelayTime = Number( toAnimateDelay ) + 300; } else { toAnimateDelayTime = 300; }
                                if( !$toAnimateElement.hasClass('animated') ) {
                                    $toAnimateElement.addClass('not-animated');
                                    var elementAnimation = $toAnimateElement.attr('data-caption-animate');
                                    setTimeout(function() {
                                        $toAnimateElement.removeClass('not-animated').addClass( elementAnimation + ' animated');
                                    }, toAnimateDelayTime);
                                }
                            });
                        }
                    });
                    $('#slider-arrow-left').on('click', function(e){
                        e.preventDefault();
                        swiperSlider.swipePrev();
                    });
                    $('#slider-arrow-right').on('click', function(e){
                        e.preventDefault();
                        swiperSlider.swipeNext();
                    });
                    $('#slide-number-current').html(swiperSlider.activeIndex + 1);
                    $('#slide-number-total').html(swiperSlider.slides.length);
                });
            </script>
</section>
<!-- Header
        ============================================= -->
<header class="full-header" id="header">
<div id="header-wrap">
<div class="container clearfix">
<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
<!-- Logo
                    ============================================= -->
<div id="logo">
<a class="standard-logo" data-dark-logo="images/logopappelo.png" href="/"><img alt="Pappel'o Logo" src="images/logopappelo.png"/></a>
<a class="retina-logo" data-dark-logo="images/logopappelo.png" href="/"><img alt="Pappel'o Logo" src="images/logopappelo.png"/></a>
</div><!-- #logo end -->
<!-- Primary Navigation
                    ============================================= -->
<nav id="primary-menu">
<ul class="one-page-menu">
<li><a data-href="#home" href="#"><div><span>INICIO</span></div></a></li>
<li><a data-href="#section-about" href="#"><div><span>NOSOTROS</span></div></a></li>
<li><a data-href="#section-work" href="#"><div><span>SERVICIOS</span></div></a></li>
<li><a data-href="#section-team" href="#"><div><span>SOCIAL</span></div></a></li>
<li><a data-href="#section-contact" href="#"><div><span>CONTACTO</span></div></a></li>
</ul>
</nav><!-- #primary-menu end -->
</div>
</div>
</header><!-- #header end -->
<!-- Content
        ============================================= -->
<section id="content">
<div class="content-wrap2">
<section class="page-section noleftpadding norightpadding nobottompadding" id="section-about" style="background-color:#fffdf8;">
<div class="iconos">
<img src="images/graficos/rosa.png" style="position:absolute;top:60px;left:295px;"/>
<img src="images/graficos/mariposa.png" style="position:absolute;top:360px;left:45%;z-index:99999;"/>
</div>
<div class="col_half nobottommargin" style="margin-top:60px;">
<!-- Tabs navigation -->
<ul class="pi-tabs-navigation pi-tabs-navigation-big pi-responsive-sm" id="myTabs">
<li class="pi-active"><a href="#historia">HISTORIA</a></li>
<li><a href="#mision">MISIÃN</a></li>
</ul>
<!-- End tabs navigation -->
<!-- Tabs content -->
<div class="pi-tabs-content pi-tabs-content-shadow">
<!-- Tabs content item -->
<div class="pi-tab-pane pi-active" id="historia">
<h2>Sobre Pappel'O</h2>
                                     PappelâO se fundÃ³ en el aÃ±o 2003, siendo una tienda Ãºnica en su estilo ya que fusiona dos conceptos basados en creatividad e imaginaciÃ³n: Scrapbooking e Imprenta Social.
                                    <br/><br/>
                                    La uniÃ³n de estos dos conceptos logra que PappelâO sea una tienda mÃ¡s completa, dedicada completamente a nuestros clientes, brindÃ¡ndoles nuestra experiencia, compromiso y atenciÃ³n personalizada en todos los eventos sociales importantes de su vida.
                                    <br/><br/>
                                    En PappelâO nos complace ayudarte a crear, imaginar y plasmar tus ideas convirtiÃ©ndolas en una realidad y en un hermoso e inolvidable recuerdo.
            </div>
<!-- End tabs content item -->
<!-- Tabs content item -->
<div class="pi-tab-pane" id="mision">
<h2>Nuestra MisiÃ³n</h2>
                                    La misiÃ³n de PappelâO tiene como mÃ¡xima prioridad contribuir a la creatividad e imaginaciÃ³n de nuestros clientes. 
                                    <br/> <br/>  
                                    Es nuestra dedicaciÃ³n proveer a todas las personas que visitan nuestra tienda o nuestra pÃ¡gina de Facebook con una extensa variedad de herramientas novedosas para que sus proyectos transmitan el sentimiento que estÃ¡n buscando.
                                    <br/><br/>
                                    Porque en PappelâO buscamos ser tu primera opciÃ³n y compartir contigo la felicidad de tus momentos.
            </div>
<!-- End tabs content item -->
</div>
<!-- End tabs content -->
</div>
<div class="col_half col_last nobottommargin" style="margin-bottom:0px;">
<div class="fslider" data-easing="easeInQuad">
<div class="flexslider">
<div class="slider-wrap">
<div class="slide">
<a href="#">
<img alt="Pappelo" src="images/graficos/nosotros-01.png"/>
</a>
</div>
<div class="slide">
<a href="#">
<img alt="Pappelo" src="images/graficos/nosotros-02.png"/>
</a>
</div>
<div class="slide">
<a href="#">
<img alt="Pappelo" src="images/graficos/nosotros-03.png"/>
</a>
</div>
<div class="slide">
<a href="#">
<img alt="Pappelo" src="images/graficos/nosotros-04.png"/>
</a>
</div>
</div>
</div>
</div>
</div>
<div class="clear"></div>
</section>
<section class="page-section col-padding" id="section-work" style="background:url(images/patrones/servicios.png);background-repeat: repeat;">
<div class="heading-block">
<h2>Nuestros Servicios</h2>
</div>
<div class="container clearfix">
<!-- Tabs -->
<div class="pi-tabs-vertical pi-responsive-sm">
<!-- Tabs navigation -->
<ul class="pi-tabs-navigation pi-tabs-navigation-big" id="myTabs-6">
<li class="pi-active"><a href="#Accesorios"><button class="active" id="rdb3" name="toggler" type="button"></button></a></li>
<li><a href="#Tarjetas"><button id="rdb2" name="toggler" type="button"></button></a></li>
<li><a href="#Regalos"><button id="rdb1" name="toggler" type="button"></button></a></li>
<li><a href="#Detalles"><button id="rdb5" name="toggler" type="button"></button></a></li>
<li><a href="#Asesorias"><button id="rdb4" name="toggler" type="button"></button></a></li>
<li><a href="#IMPRENTA"><button id="rdb6" name="toggler" type="button"></button></a></li>
</ul>
<!-- End tabs navigation -->
<!-- Tabs content -->
<div class="pi-tabs-content pi-tabs-content-shadow">
<!-- Tabs content item -->
<div class="pi-tab-pane" id="IMPRENTA">
<p style="font-size:18px;margin-bottom:-20px;">
                        IMPRENTA
                    </p>
<img src="images/servicios/servicio-imprenta2.png"/>
<p style="margin-top:20px;line-height:1.5;">
                        Nuestra imprenta estÃ¡ a tu disposiciÃ³n para que puedas imprimir lo que necesites como tarjetas para regalos, sobres para despedida de soltera, papelerÃ­a personal, etc.
                    </p>
</div>
<!-- End tabs content item -->
<!-- Tabs content item -->
<div class="pi-tab-pane pi-active" id="Accesorios">
<p style="font-size:18px;margin-bottom:-20px;">
                       ACCESORIOS PARA SCRAPBOOKS
                    </p>
<img src="images/servicios/servicio-accesorios2.png"/>
<p style="margin-top:20px;line-height:1.5;">
                        Todo lo que buscas para realizar tus scrapbooks, desde cortes de letras y formas, papel decorativo, cintas, hasta herramientas como tijeras, troqueles y pegamentos.
                    </p>
</div>
<!-- End tabs content item -->
<!-- Tabs content item -->
<div class="pi-tab-pane" id="Detalles">
<p style="font-size:18px;margin-bottom:-20px;">
                       DETALLES Y RECUERDOS PARA BODAS
                    </p>
<img src="images/servicios/servicio-brides2.png"/>
<p style="margin-top:20px;line-height:1.5;">
                       Queremos que las novias encuentren en nosotros el apoyo para realizar los artÃ­culos necesarios para este momento tan importante.
                    </p>
</div>
<!-- End tabs content item -->
<!-- Tabs content item -->
<div class="pi-tab-pane" id="Tarjetas">
<p style="font-size:18px;margin-bottom:-20px;">
                       TARJETAS
                    </p>
<img src="images/servicios/servicio-tarjetas2.png"/>
<p style="margin-top:20px;line-height:1.5;">
                      Encuentra en nuestra tienda un amplio surtido de tarjetas con diferentes mensajes y diseÃ±os para toda ocasiÃ³n.
                    </p>
</div>
<!-- End tabs content item -->
<!-- Tabs content item -->
<div class="pi-tab-pane" id="Regalos">
<p style="font-size:18px;margin-bottom:-20px;">
                       REGALOS Y ENVOLTURAS
                    </p>
<img src="images/servicios/servicio-regalo2.png"/>
<p style="margin-top:20px;line-height:1.5;">
                        Contamos con una gran variedad de detalles y opciones para decorar tus obsequios de una manera linda y original que sorprenderÃ¡n a tus seres queridos.
                    </p>
</div>
<!-- End tabs content item -->
<!-- Tabs content item -->
<div class="pi-tab-pane" id="Asesorias">
<p style="font-size:18px;margin-bottom:-20px;">
                       ASESORÃAS Y TALLERES
                    </p>
<img src="images/servicios/servicio-asesorias2.png"/>
<p style="margin-top:20px;line-height:1.5;">
                        En PappelâO te ayudamos y asesoramos a impulsar tu creatividad con el fin de mejorar tus proyectos y convertirlos en realidad.
                    </p>
</div>
<!-- End tabs content item -->
</div>
</div>
<section class="page-section col-padding nobottompadding camara" id="section-team" style="background:url(images/patrones/social.jpg);background-repeat: repeat;">
<div class="heading-block">
<img src="images/graficos/letrero-siguenos.png"/>
</div>
<div class="container clearfix">
<div class="iconos">
<img src="images/graficos/rosa3.png" style="position:absolute;top:330px;left:0px;"/>
<img src="images/graficos/tape02.png" style="position:absolute;top:-40px;right:0px;"/>
<img src="images/graficos/camara.png" style="position:absolute;top:330px;right:0px;"/>
</div>
<div id="instafeed"></div>
<div class="clear"></div>
<div class="col_one_third top-padding">
<a href="https://www.facebook.com/pages/Pappelo-Social-Scrapbooking/119234724927731?fref=ts" target="_blank"><img src="images/graficos/letrero-facebook.png"/></a>
</div>
<div class="col_one_third top-padding">
<a href="https://instagram.com/pappelomty/" target="_blank"><img src="images/graficos/letrero-instagram.png"/></a>
</div>
<div class="col_one_third col_last top-padding">
<a href="https://www.pinterest.com/pappelomty/" target="_blank"><img src="images/graficos/letrero-pinterest.png"/></a>
</div>
</div>
</section>
<section class="page-section col-padding nobottompadding" id="section-contact" style="background-color:#e6f2e8;">
<div class="heading-block" style="margin-bottom:20px;">
<img src="images/graficos/letrero-visita.png"/>
</div>
<div class="container clearfix">
<!-- Contact Form
                        ============================================= -->
<!-- Google Map
                        ============================================= -->
<div class="col_full">
<div class="iconos">
<img src="images/mapa/fondo-arriba.png" style="position:absolute;top:-45px;right:35px;z-index:99999;"/>
<img src="images/mapa/fondo-lateral.png" style="position:absolute;top:5px;right:4px;z-index:99999;"/>
<img src="images/mapa/fondo-abajo.png" style="position:absolute;top:407px;right:11px;z-index:99999;"/>
<img src="images/graficos/globo.png" style="position:absolute;top:262px;left:1px;z-index:99999;"/>
<img src="images/graficos/rosa4.png" style="position:absolute;top:-76px;right:0px;z-index:99999;"/>
</div>
<section class="gmap" id="google-map" style="height: 410px;border:20px solid white;"></section>
<script src="maps/api/js_sensor_false.js" type="text/javascript"></script>
<script src="js/jquery.gmap.js" type="text/javascript"></script>
<script type="text/javascript">
                                jQuery('#google-map').gMap({
                                    address: 'Avenida Bosques del Valle 110, San Pedro Garza Garcia',
                                    maptype: 'ROADMAP',
                                    zoom: 17,
                                    markers: [
                                        {
                                            address: "Avenida Bosques del Valle 110, San Pedro Garza Garcia",
                                            html: '<div style="width: 100px;"><h4 style="margin-bottom: 8px;">Pappel\'O</div>',
                                            icon: {
                                                image: "images/map-icon.png",
                                                iconsize: [32, 39],
                                                iconanchor: [32,39]
                                            }
                                        }
                                    ],
                                    doubleclickzoom: false,
                                    controls: {
                                        panControl: true,
                                        zoomControl: true,
                                        mapTypeControl: true,
                                        scaleControl: false,
                                        streetViewControl: false,
                                        overviewMapControl: false
                                    }
                                });
                            </script>
</div><!-- Google Map End -->
<div class="col_half">
<div data-notify-msg="&lt;i class=icon-ok-sign&gt;&lt;/i&gt; Message Sent Successfully!" data-notify-type="success" id="contact-form-result"></div>
<form action="/" class="nobottommargin" method="post">
<div class="form-process"></div>
<div class="col_one_fourth">
<label for="template-contactform-name">Nombre <small>*</small></label>
</div>
<div class="col_three_fourth col_last">
<input class="sm-form-control required" id="nombre" name="nombre" type="text" value=""/>
</div>
<div class="col_one_fourth">
<label for="template-contactform-email">Correo electrÃ³nico <small>*</small></label>
</div>
<div class="col_three_fourth col_last">
<input class="required email sm-form-control" id="correo" name="correo" type="text" value=""/>
</div>
<input name="captcha" type="hidden" value=""/>
<div class="col_full" style="padding-left:0px;padding-right:0px;">
<label for="template-contactform-message">Mensaje <small>*</small></label>
<textarea class="required sm-form-control notes" cols="30" id="mensaje" name="mensaje" rows="5"></textarea>
</div>
<div class="col_full">
<button class="button button-3d nomargin" id="template-contactform-submit" name="template-contactform-submit" type="submit" value="submit">ENVIAR</button>
</div>
</form>
</div><!-- Contact Form End -->
<!-- Informacion
                        ============================================= -->
<div class="col_half col_last">
                        Avenida Bosques del Valle, 110-7<br/>
                        Col. Bosques del Valle<br/>
                        San Pedro Garza GarcÃ­a, Nuevo LeÃ³n<br/>
<a href="tel:8186762455">Tel. (81) 8676 2455</a><br/>
                        Lunes a Viernes: 10:00 am - 7:30 pm <br/>
                        SÃ¡bado: 10:00  am - 6:00 pm 
                    </div>
</div>
</section>
</div>
</section><!-- #content end -->
<!-- Footer
        ============================================= -->
<footer class="dark" id="footer">
<!-- Copyrights
            ============================================= -->
<div id="copyrights">
<div class="container clearfix">
<div class="col_full center nobottommargin">
<a href="https://www.facebook.com/pages/Pappelo-Social-Scrapbooking/119234724927731?fref=ts" target="_blank">
<img src="images/graficos/iconos-footer-facebook.png"/>
</a>
<a href="https://instagram.com/pappelomty/" target="_blank">
<img src="images/graficos/iconos-footer-instagram.png"/>
</a>
<a href="https://www.pinterest.com/pappelomty/" target="_blank">
<img src="images/graficos/iconos-footer-pinterest.png"/>
</a>
<div class=" clearfix">
                            Pappel'O © DiseÃ±o Web por <strong><a href="http://www.polun.mx/">Polun</a></strong>. Todos los derechos reservados.
                        </div>
<div class="clear"></div>
</div>
</div>
</div><!-- #copyrights end -->
</footer><!-- #footer end -->
</div><!-- #wrapper end -->
<!-- Go To Top
    ============================================= -->
<div class="icon-angle-up" id="gotoTop"></div>
<!-- Footer Scripts
    ============================================= -->
<script src="js/functions.js" type="text/javascript"></script>
<script src="js/pi.tab.js"></script>
<script src="js/pi.init.tab.js"></script>
</section></div></body>
</html>