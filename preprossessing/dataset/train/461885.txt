<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>ERROR 404 - Not Found</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="noindex" name="robots"/>
<style type="text/css">
    <!--
    body {
        color: #444444;
        background-color: #EEEEEE;
        font-family: Tahoma, Geneva, sans-serif;
        font-size: 100%;
    }
    h1 {}
    h2 { font-size: 1.0em; font-weight: normal; }
    #page{
        background-color: #FFFFFF;
        width: 60%;
        margin: 24px auto;
        padding: 12px;
    }
    #header {
        padding: 6px ;
        text-align: center;
    }
    .headerbar { 
		background-color: #C55042; 
		color: #FFFFFF;
		border-bottom: #4C83BF 15px solid;
		border-top: #4C83BF 15px solid;  
	}
    #content {
		color: #006699;
		padding: 34px 34px 164px 34px;
    }
    #content a {
		color: #0099CC;
		text-decoration: none;
    }
    #content a:hover {
	color: #003399;
	text-decoration: underline;
    }
    #footer {
        color: #DAE7F3;
        background: #4C83BF;
        padding: 10px 20px;
        border-top: 5px #efefef solid;
        font-size: 0.8em;
        text-align: center;
    }
    #footer a {
        color: #DAE7F3;
		text-decoration: none;
    }
    #footer a:hover {
		color: #FFFFFF;
		text-decoration: underline;
    }
    -->
    </style>
</head>
<body>
<div id="page">
<div class="headerbar" id="header">
<h1>ERROR 404 - Not Found</h1>
</div>
<div id="content">
<h2>The following error occurred:</h2>
<p>ERROR 404 - Document Not Found. The requested URL was not found on this server.</p>
<p><br/>&lt;- <a href="../">Jump to this website's Home Page</a></p>
</div>
<div id="footer">
<p> </p>
</div>
</div>
</body>
</html>