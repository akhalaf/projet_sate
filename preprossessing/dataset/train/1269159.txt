<html>
<head>
<meta charset="utf-8"/>
<title>sarbottom</title>
<!--<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />-->
<!--fonts-->
<link href="http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic" rel="stylesheet" type="text/css"/>
<!--//fonts-->
<link href="https://sarbottam.com/wp-content/themes/me/css/bootstrap.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sarbottam.com/wp-content/themes/me/css/style.css" media="all" rel="stylesheet" type="text/css"/>
<!-- for-mobile-apps -->
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Kong Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" name="keywords"/>
<!--<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>-->
<!-- //for-mobile-apps -->
<!-- js -->
<script src="https://sarbottam.com/wp-content/themes/me/js/jquery.min.js" type="text/javascript"></script>
<!-- js -->
<!-- start-smooth-scrolling -->
<script src="https://sarbottam.com/wp-content/themes/me/js/move-top.js" type="text/javascript"></script>
<script src="https://sarbottam.com/wp-content/themes/me/js/easing.js" type="text/javascript"></script>
<script type="text/javascript">

			jQuery(document).ready(function($) {

				$(".scroll").click(function(event){		

					event.preventDefault();

					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);

				});

			});

		</script>
<!-- start-smooth-scrolling -->
<link href="https://sarbottam.com/feed/" rel="alternate" title="sarbottom » Feed" type="application/rss+xml"/>
<link href="https://sarbottam.com/comments/feed/" rel="alternate" title="sarbottom » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"https:\/\/sarbottam.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.3.25"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://sarbottam.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.3.1" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A300%2C400%2C700%2C300italic%2C400italic%2C700italic%7CBitter%3A400%2C700&amp;subset=latin%2Clatin-ext" id="twentythirteen-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sarbottam.com/wp-content/themes/me/genericons/genericons.css?ver=3.03" id="genericons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sarbottam.com/wp-content/themes/me/css/slippry.css?ver=4.3.25" id="slippry-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sarbottam.com/wp-content/themes/me/css/jquery.fancybox.css?ver=4.3.25" id="fancybox-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sarbottam.com/wp-content/themes/me/style.css?ver=2013-07-18" id="twentythirteen-style-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='twentythirteen-ie-css'  href='https://sarbottam.com/wp-content/themes/me/css/ie.css?ver=2013-07-18' type='text/css' media='all' />
<![endif]-->
<script src="https://sarbottam.com/wp-includes/js/jquery/jquery.js?ver=1.11.3" type="text/javascript"></script>
<script src="https://sarbottam.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1" type="text/javascript"></script>
<script src="https://sarbottam.com/wp-content/themes/me/js/jquery.fancybox.js?ver=4.3.25" type="text/javascript"></script>
<script src="https://sarbottam.com/wp-content/themes/me/js/jquery.fancybox.pack.js?ver=4.3.25" type="text/javascript"></script>
<script src="https://sarbottam.com/wp-content/themes/me/js/custom.js?ver=4.3.25" type="text/javascript"></script>
<link href="https://sarbottam.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://sarbottam.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.3.25" name="generator"/>
<link href="https://sarbottam.com/" rel="canonical"/>
<link href="https://sarbottam.com/" rel="shortlink"/>
<link href="http://sarbottam.com/wp-content/uploads/2015/08/favicon.ico" rel="shortcut icon"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<style id="twentythirteen-header-css" type="text/css">
			.site-header {
			background: url(https://sarbottam.com/wp-content/themes/me/images/headers/circle.png) no-repeat scroll top;
			background-size: 1600px auto;
		}
		@media (max-width: 767px) {
			.site-header {
				background-size: 768px auto;
			}
		}
		@media (max-width: 359px) {
			.site-header {
				background-size: 360px auto;
			}
		}
		</style>
</head>
<body class="home page page-id-5 page-template page-template-page-template page-template-home page-template-page-templatehome-php single-author">
<!-- banner -->
<div class="banner" id="home">
<div class="container">
<div class="top-header row">
<script src="https://sarbottam.com/wp-content/themes/me/js/classie.js"></script>
<!--top-nav---->
<!--<div class="logo"> <a href=""><img src="/images/logo.png" alt=""/></a> </div>-->
<div class="top-nav">
<div class="nav-icon"> <a class="right_bt" href="#" id="activator"><span> </span> </a> </div>
<div class="box" id="box">
<div class="box_content">
<div class="box_content_center">
<div class="form_content">
<div class="menu_box_list">
<ul>
<li><a class="scroll" href="#home"><span>home</span></a></li>
<li><a class="scroll" href="#about"><span>about</span></a></li>
<li><a class="scroll" href="#services"><span>services</span></a></li>
<li><a class="scroll" href="#portfolio"><span>portfolio</span></a></li>
<li><a class="scroll" href="#testimonial"><span>Testimonial</span></a></li>
<li><a class="scroll" href="#blog"><span>blog</span></a></li>
<li><a class="scroll" href="#contact"><span>Contact</span></a></li>
<div class="clearfix"> </div>
</ul>
</div>
<a class="boxclose" id="boxclose"> <span> </span></a> </div>
</div>
</div>
</div>
</div>
<!---start-click-drop-down-menu----->
<!----start-dropdown--->
<script type="text/javascript">

						var $ = jQuery.noConflict();

							$(function() {

								$('#activator').click(function(){

									$('#box').animate({'top':'0px'},900);

								});

								$('#boxclose').click(function(){

								$('#box').animate({'top':'-1000px'},900);

								});

							});

							$(document).ready(function(){

							//Hide (Collapse) the toggle containers on load

							$(".toggle_container").hide(); 

							//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)

							$(".trigger").click(function(){

								$(this).toggleClass("active").next().slideToggle("slow");

									return false; //Prevent the browser jump to the link anchor

							});

												

						});

					</script>
<!---//End-click-drop-down-menu----->
<div class="clearfix"> </div>
</div>
<div class="banner-info">
<div class="banner-left">
<!--<img src="/images/tup.png" alt=""/>-->
</div>
<div class="banner-right">
<h1>I'M Sarbottam</h1>
<div class="border"></div>
<h2>Sales &amp; Marketing Head at 711 Academy</h2>
<a download="" href="#">DOWNLOAD MY RESUME</a>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!--about-->
<div class="about text-center" id="about">
<div class="container">
<h3>ABOUT ME</h3>
<div class="strip text-center"><img alt=" " src="https://sarbottam.com/wp-content/themes/me/images/about.png"/></div>
<p>I am a ambitious, self-made, work alcoholic but down to earth person. I like to balance professional &amp; family life. Professional life gives you exposure, confidence &amp; sense of achievement. I believe the fulfillment one gets from one’s work is very important for wellbeing. I also participate in family get-togethers, functions, parties, etc. My favourite pastime is to watch English movies, reading fictions &amp; cooking.</p>
<ul>
<li><a class="fb" href="#"></a></li>
<li><a class="twit" href="#"></a></li>
<li><a class="in" href="#"></a></li>
<li><a class="drib" href="#"></a></li>
<li><a class="goog" href="#"></a></li>
<li><a class="pin" href="#"></a></li> </ul>
</div>
</div>
<!--//about-->
<div class="about-back"></div>
<!--my skill-->
<div class="my-skills text-center">
<div class="container">
<h3>MY SKILLS</h3>
<div class="strip text-center"><img alt=" " src="https://sarbottam.com/wp-content/themes/me/images/skill.png"/></div>
<div class="skill-grids">
<div class="col-md-2 skills-grid text-center">
<div class="circle" id="circles-1"></div>
<p>Photoshop</p>
</div>
<div class="col-md-2 skills-grid text-center">
<div class="circle" id="circles-2"></div>
<p>Illustrator</p>
</div>
<div class="col-md-2 skills-grid text-center">
<div class="circle" id="circles-3"></div>
<p>Photography</p>
</div>
<div class="col-md-2 skills-grid text-center">
<div class="circle" id="circles-4"></div>
<p>Html 5 / CSS 3</p>
</div>
<div class="col-md-2 skills-grid text-center">
<div class="circle" id="circles-5"></div>
<p>C4D</p>
</div>
<div class="col-md-2 skills-grid text-center">
<div class="circle" id="circles-6"></div>
<p>After Effect</p>
</div>
<div class="clearfix"> </div>
<script src="https://sarbottam.com/wp-content/themes/me/js/circles.js" type="text/javascript"></script>
<script>
var colors = [
		['#6ed4c0', '#ffffff'], ['#6ed4c0', '#ffffff'], ['#6ed4c0', '#ffffff'], ['#6ed4c0', '#ffffff'], ['#6ed4c0', '#ffffff'], ['#6ed4c0', '#ffffff']
		];
		for (var i = 1; i <= 7; i++) {
		var child = document.getElementById('circles-' + i),
		percentage = 30 + (i * 10);
		Circles.create({
		id:         child.id,
                percentage: percentage,
                radius:     80,
                width:      10,
                number:   	percentage / 1,
                text:       '%',
                colors:     colors[i - 1]
        });
}
	</script>
</div>
</div>
</div>
<!--//my skill-->
<div class="my-skill-back"></div>
<!--education-->
<div class="education text-center">
<div class="container">
<div class="edu-info">
<h3>EDUCATION</h3>
</div>
<div class="strip text-center"><img alt=" " src="https://sarbottam.com/wp-content/themes/me/images/edu.png"/></div>
<div class="edu-grids">
<div class="col-md-4 edu-grid">
<p>2012 - 2013</p>
<span>Graduated</span> <img alt="" src="https://sarbottam.com/wp-content/themes/me/images/arrow.png"/>
<div class="edu-border">
<div class="edu-grid-master">
<h3>MASTER DEGREE OF COMPUTER SCIENCE</h3>
<h4>Oxford University</h4>
</div>
<div class="edu-grid-info">
<h5></h5><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</div>
</div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!--//education-->
<div class="strip-border">
<p></p>
</div>
<!--work-->
<div class="work-experience text-center">
<div class="container">
<div class="work-info">
<h3>WORK EXPERIENCE</h3>
</div>
<div class="strip text-center"><img alt=" " src="https://sarbottam.com/wp-content/themes/me/images/work.png"/></div>
<div class="work-grids">
<div class="col-md-4 w-grid">
<div class="work-grid">
<h3>2013 - Now</h3>
<div class="work-grid-info">
<h4>Google</h4>
<h5>Visual Designer</h5>
          Visual Designer          </div>
</div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!--//work-->
<div class="services-back"></div>
<!--services-->
<div class="services text-center" id="services">
<div class="container">
<div class="ser-info">
<h3>SERVICES</h3>
</div>
<div class="strip text-center"><img alt=" " src="https://sarbottam.com/wp-content/themes/me/images/ser.png"/></div>
<div class="ser-grids">
<div class="col-md-3 ser-grid">
<div class="ser-image1"></div>
<h3>Web Design</h3>
<p>I’m gonna shoot you in the head<br/>
then and there. Then I’m gonna shoot<br/>
that bitch in the kneecaps.</p>
</div>
<div class="col-md-3 ser-grid">
<div class="ser-image2"></div>
<h3>Graphic Design</h3>
<p>I’m gonna shoot you in the head<br/>
then and there. Then I’m gonna shoot<br/>
that bitch in the kneecaps.</p>
</div>
<div class="col-md-3 ser-grid">
<div class="ser-image3"></div>
<h3>Content Writer</h3>
<p>I’m gonna shoot you in the head<br/>
then and there. Then I’m gonna shoot<br/>
that bitch in the kneecaps.</p>
</div>
<div class="col-md-3 ser-grid">
<div class="ser-image4"></div>
<h3>Development</h3>
<p>I’m gonna shoot you in the head<br/>
then and there. Then I’m gonna shoot<br/>
that bitch in the kneecaps.</p>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!--//services-->
<!--portfolio-->
<div class="gallery-section text-center" id="portfolio">
<div class="container">
<h3>PORTFOLIO</h3>
<div class="strip text-center"><img alt=" " src="https://sarbottam.com/wp-content/themes/me/images/port.png"/></div>
<p>You think water moves fast? You should see ice. It moves like it has a mind. Like it knows it killed the world once and got a taste for murder. After the avalanche, it took us a week to climb out. Now, I don’t know exactly when we turned on each other, but I know that seven of us survived the slide and only five made it out. Now we took an oath, that I’m breaking now. We said we’d say it was the snow that killed the other two, but it wasn’t. Nature is lethal but it doesn’t hold a candle to man.</p>
<div class="gallery-grids">
<div class="top-gallery">
<div class="col-md-4 gallery-grid gallery1">
<a class="swipebox" href="https://sarbottam.com/wp-content/themes/me/images/p1.jpg">
<img alt="no img" class="img-responsive" src="https://sarbottam.com/wp-content/uploads/2015/08/p1.jpg"/>
<div class="textbox">
<h4>Project Name</h4>
<p>WEB DESIGN</p>
<div class="button">
<a class="swipebox" href="#">VIEW</a></div>
</div>
</a></div>
<div class="col-md-4 gallery-grid gallery1">
<a class="swipebox" href="https://sarbottam.com/wp-content/themes/me/images/p1.jpg">
<img alt="no img" class="img-responsive" src="https://sarbottam.com/wp-content/uploads/2015/08/p2.jpg"/>
<div class="textbox">
<h4></h4>
<p></p>
<div class="button">
<a class="swipebox" href="">VIEW</a></div>
</div>
</a></div>
<div class="col-md-4 gallery-grid gallery1">
<a class="swipebox" href="https://sarbottam.com/wp-content/themes/me/images/p1.jpg">
<img alt="no img" class="img-responsive" src="https://sarbottam.com/wp-content/uploads/2015/08/p3.jpg"/>
<div class="textbox">
<h4></h4>
<p></p>
<div class="button">
<a class="swipebox" href="">VIEW</a></div>
</div>
</a></div>
<div class="col-md-4 gallery-grid gallery1">
<a class="swipebox" href="https://sarbottam.com/wp-content/themes/me/images/p1.jpg">
<img alt="no img" class="img-responsive" src="https://sarbottam.com/wp-content/uploads/2015/08/p4.jpg"/>
<div class="textbox">
<h4></h4>
<p></p>
<div class="button">
<a class="swipebox" href="">VIEW</a></div>
</div>
</a></div>
<div class="col-md-4 gallery-grid gallery1">
<a class="swipebox" href="https://sarbottam.com/wp-content/themes/me/images/p1.jpg">
<img alt="no img" class="img-responsive" src="https://sarbottam.com/wp-content/uploads/2015/08/p5.jpg"/>
<div class="textbox">
<h4></h4>
<p></p>
<div class="button">
<a class="swipebox" href="">VIEW</a></div>
</div>
</a></div>
<div class="clearfix"> </div>
</div>
<link href="css/swipebox.css" rel="stylesheet"/>
<script src="https://sarbottam.com/wp-content/themes/me/js/jquery.swipebox.min.js"></script>
<script type="text/javascript">
						jQuery(function($) {
							$(".swipebox").swipebox();

						});
					</script>
</div>
</div>
</div>
<!--//portfolio-->
<!--process-->
<div class="process text-center">
<div class="container">
<h3>PROCESS</h3>
<div class="strip text-center"><img alt=" " src="https://sarbottam.com/wp-content/themes/me/images/pro.png"/></div>
<div class="process-girds">
<div class="col-md-2 process-pad">
<div class="process-gird">
<div class="process-imagea"></div>
<p>IDEA</p>
<img alt="" src="https://sarbottam.com/wp-content/themes/me/images/6.png"/> </div>
</div>
<div class="col-md-2 process-pad">
<div class="process-gird">
<div class="process-imageb"></div>
<p>CONCEPT</p>
<img alt="" class="pro-imga" src="https://sarbottam.com/wp-content/themes/me/images/6.png"/> </div>
</div>
<div class="col-md-2 process-pad">
<div class="process-gird">
<div class="process-imagec"></div>
<p>DESIGN</p>
<img alt="" class="pro-img" src="https://sarbottam.com/wp-content/themes/me/images/6.png"/> </div>
</div>
<div class="col-md-2 process-pad">
<div class="process-gird">
<div class="process-imaged"></div>
<p>DEVELOP</p>
<img alt="" class="pro-imgb" src="https://sarbottam.com/wp-content/themes/me/images/6.png"/> </div>
</div>
<div class="col-md-2 process-pad">
<div class="process-gird">
<div class="process-imagee"></div>
<p>TEST</p>
<img alt="" src="https://sarbottam.com/wp-content/themes/me/images/6.png"/> </div>
</div>
<div class="col-md-2 process-pad">
<div class="process-gird">
<div class="process-imagef"></div>
<p>LAUNCH</p>
</div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!--//process-->
<div class="process-back"></div>
<!--testimonials-->
<div class="testimonials" id="testimonial">
<div class="container">
<h3>TESTIMONIAL</h3>
<div class="strip text-center"><img alt=" " src="https://sarbottam.com/wp-content/themes/me/images/test.png"/></div>
<!-- responsiveslides -->
<script src="https://sarbottam.com/wp-content/themes/me/js/responsiveslides.min.js"></script>
<script>

							// You can also use "$(window).load(function() {"

							$(function () {
							 // Slideshow 4
							$("#slider3").responsiveSlides({
								auto: true,
								pager: false,
								nav: true,
								speed: 500,
								namespace: "callbacks",
								before: function () {
							$('.events').append("<li>before event fired.</li>");
							},
							after: function () {
								$('.events').append("<li>after event fired.</li>");
								}
								});
								});
						</script>
<!-- responsiveslides -->
<div class="callbacks_container" id="top">
<ul class="rslides" id="slider3">
<li>
<div class="test-banner"> <img alt="" class="quoa" src="https://sarbottam.com/wp-content/themes/me/images/quo2.png"/>
<div class="test-left">
<img alt="" src="https://sarbottam.com/wp-content/uploads/2015/12/eee.png"/>
</div>
<div class="test-right">
<p>However unreal it may seem, we are connected, you and I. We’re on the same curve, just on opposite ends.</p>
<h4>Sam L. J. - Pulp Fiction</h4>
</div>
<div class="clearfix"></div>
<img alt="" class="quo" src="https://sarbottam.com/wp-content/themes/me/images/quo1.png"/> </div>
</li>
<li>
<div class="test-banner"> <img alt="" class="quoa" src="https://sarbottam.com/wp-content/themes/me/images/quo2.png"/>
<div class="test-left">
<img alt="" src="https://sarbottam.com/wp-content/uploads/2015/12/7.png"/>
</div>
<div class="test-right">
<p>However unreal it may seem, we are connected,you and I. We’re on the same curve, just on opposite ends.</p>
<h4>Sam L. J. - Pulp Fiction</h4>
</div>
<div class="clearfix"></div>
<img alt="" class="quo" src="https://sarbottam.com/wp-content/themes/me/images/quo1.png"/> </div>
</li>
</ul>
</div>
</div>
</div>
<!--testimonials-->
<!--clients-->
<div class="clients text-center">
<div class="container">
<h4>Clients</h4>
<div class="strip-line"></div>
<div class="client-grids">
<div class="col-md-4 cl-grid">
<div class="client-grid"> <img alt="" src="https://sarbottam.com/wp-content/themes/me/images/10.png"/> </div>
</div>
<div class="col-md-4 cl-grid">
<div class="client-grid"> <img alt="" src="https://sarbottam.com/wp-content/themes/me/images/11.png"/> </div>
</div>
<div class="col-md-4 cl-grid">
<div class="client-grid"> <img alt="" src="https://sarbottam.com/wp-content/themes/me/images/12.png"/> </div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!--clients-->
<div class="client-back"></div>
<!--blog-->
<div class="blog" id="blog">
<div class="container">
<div class="blog-info text-center">
<h3>BLOG</h3>
<div class="strip text-center"><img alt=" " src="https://sarbottam.com/wp-content/themes/me/images/blog.png"/></div>
</div>
<div class="blog-grids">
<div class="col-md-4 blog-text-info">
<div class="blog-grid"> <a href="single.html"><img alt="" src="https://sarbottam.com/wp-content/themes/me/images/a.jpg"/></a>
<div class="blog-text"> <a href="single.html">Nature is lethal but it doesn't hold a candle to man.</a>
<div class="stripa"></div>
<p>Your bones don't break, mine do. That's clear. Your cells react to bacteria 
              
              
              
              and viruses differently than mine. You don't get sick, I do.</p>
</div>
<div class="blog-para">
<ul>
<li><a href="#">3 Comments</a></li>
<li><a href="#">8 Share</a></li>
</ul>
</div>
<div class="blog-pos">
<p>5<span>May</span></p>
</div>
</div>
</div>
<div class="col-md-4 blog-text-info">
<div class="blog-grid"> <a href="single.html"><img alt="" src="https://sarbottam.com/wp-content/themes/me/images/b.jpg"/></a>
<div class="blog-text"> <a href="single.html">Nature is lethal but it doesn't hold a candle to man.</a>
<div class="stripa"></div>
<p>Your bones don't break, mine do. That's clear. Your cells react to bacteria 
              
              
              
              and viruses differently than mine. You don't get sick, I do.</p>
</div>
<div class="blog-para">
<ul>
<li><a href="#">3 Comments</a></li>
<li><a href="#">8 Share</a></li>
</ul>
</div>
<div class="blog-pos">
<p>5<span>May</span></p>
</div>
</div>
</div>
<div class="col-md-4 blog-text-info">
<div class="blog-grid"> <a href="single.html"><img alt="" src="https://sarbottam.com/wp-content/themes/me/images/c.jpg"/></a>
<div class="blog-text"> <a href="single.html">Nature is lethal but it doesn't hold a candle to man.</a>
<div class="stripa"></div>
<p>Your bones don't break, mine do. That's clear. Your cells react to bacteria 
              
              
              
              and viruses differently than mine. You don't get sick, I do.</p>
</div>
<div class="blog-para">
<ul>
<li><a href="#">3 Comments</a></li>
<li><a href="#">8 Share</a></li>
</ul>
</div>
<div class="blog-pos">
<p>5<span>May</span></p>
</div>
</div>
</div>
<div class="col-md-4 blog-text-info">
<div class="blog-grid"> <a href="single.html"><img alt="" src="https://sarbottam.com/wp-content/themes/me/images/a.jpg"/></a>
<div class="blog-text"> <a href="single.html">Nature is lethal but it doesn't hold a candle to man.</a>
<div class="stripa"></div>
<p>Your bones don't break, mine do. That's clear. Your cells react to bacteria 
              
              
              
              and viruses differently than mine. You don't get sick, I do.</p>
</div>
<div class="blog-para">
<ul>
<li><a href="#">3 Comments</a></li>
<li><a href="#">8 Share</a></li>
</ul>
</div>
<div class="blog-pos">
<p>5<span>May</span></p>
</div>
</div>
</div>
<div class="col-md-4 blog-text-info">
<div class="blog-grid"> <a href="single.html"><img alt="" src="https://sarbottam.com/wp-content/themes/me/images/b.jpg"/></a>
<div class="blog-text"> <a href="single.html">Nature is lethal but it doesn't hold a candle to man.</a>
<div class="stripa"></div>
<p>Your bones don't break, mine do. That's clear. Your cells react to bacteria 
              
              
              
              and viruses differently than mine. You don't get sick, I do.</p>
</div>
<div class="blog-para">
<ul>
<li><a href="#">3 Comments</a></li>
<li><a href="#">8 Share</a></li>
</ul>
</div>
<div class="blog-pos">
<p>5<span>May</span></p>
</div>
</div>
</div>
<div class="col-md-4 blog-text-info">
<div class="blog-grid"> <a href="single.html"><img alt="" src="https://sarbottam.com/wp-content/themes/me/images/c.jpg"/></a>
<div class="blog-text"> <a href="single.html">Nature is lethal but it doesn't hold a candle to man.</a>
<div class="stripa"></div>
<p>Your bones don't break, mine do. That's clear. Your cells react to bacteria 
              
              
              
              and viruses differently than mine. You don't get sick, I do.</p>
</div>
<div class="blog-para">
<ul>
<li><a href="#">3 Comments</a></li>
<li><a href="#">8 Share</a></li>
</ul>
</div>
<div class="blog-pos">
<p>5<span>May</span></p>
</div>
</div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!--//blog-->
<div class="contact-back"></div>
<!--contact-->
<div class="contact" id="contact">
<div class="container">
<div class="contact-info text-center">
<h3>CONTACT</h3>
<div class="strip text-center"><img alt=" " src="https://sarbottam.com/wp-content/themes/me/images/con1.png"/></div>
</div>
<div class="contact-grids">
<div class="col-md-5 contact-left">
<h3>Say Hi to Me</h3>
<div class="stripb"></div>
<ul>
<li>DA-31,Sector-1, </li>
<li>Saltlake City</li>
<li>Kolkata-700064</li>
<li>9836871711</li>
<li><a href="mailto:sarbottam.das@besi.co.in">sarbottam.das@besi.co.in</a></li>
<li><a href="#">www.711academy.com</a></li>
</ul>
</div>
<div class="col-md-7 contact-right">
<h3>Drop Me A Line</h3>
<div class="stripb"></div>
<div class="wpcf7" dir="ltr" id="wpcf7-f8-o1" lang="en-US" role="form">
<div class="screen-reader-response"></div>
<form action="/#wpcf7-f8-o1" class="wpcf7-form" method="post" novalidate="novalidate">
<div style="display: none;">
<input name="_wpcf7" type="hidden" value="8"/>
<input name="_wpcf7_version" type="hidden" value="4.3.1"/>
<input name="_wpcf7_locale" type="hidden" value="en_US"/>
<input name="_wpcf7_unit_tag" type="hidden" value="wpcf7-f8-o1"/>
<input name="_wpnonce" type="hidden" value="57999a3890"/>
</div>
<p><span class="wpcf7-form-control-wrap name1"><input aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" name="name1" placeholder="Your name" size="40" type="text" value=""/></span><br/>
<span class="wpcf7-form-control-wrap email1"><input aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" name="email1" placeholder="Your Email" size="40" type="email" value=""/></span><br/>
<span class="wpcf7-form-control-wrap msg"><textarea aria-invalid="false" class="wpcf7-form-control wpcf7-textarea" cols="40" name="msg" placeholder="Your Message" rows="10"></textarea></span><br/>
<input class="wpcf7-form-control wpcf7-submit" type="submit" value="SEND"/></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div> </div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!--//contact-->
<div class="footer-top"></div>
<!--resume-->
<div class="resume text-center">
<div class="container">
<div class="strip text-center"><a href="#"><img alt=" " src="https://sarbottam.com/wp-content/themes/me/images/down.png"/></a></div>
<div class="down"><a download="" href="#">Download My Resume</a></div>
</div>
</div>
<!--//resume-->
<!--footer-->
<div class="footer">
<div class="container">
<p>Copyright © 2015 Sarbottam. Site by <a href="#"> Milee</a></p>
</div>
</div>
<!--//footer-->
<!-- here stars scrolling icon -->
<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {

				containerID: 'toTop', // fading element id

				containerHoverID: 'toTopHover', // fading element hover id

				scrollSpeed: 1200,

				easingType: 'linear' 

				};
			*/
			$().UItoTop({ easingType: 'easeOutQuart' });
			});
	</script>
<!-- //here ends scrolling icon -->
<script src="https://sarbottam.com/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"https:\/\/sarbottam.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","recaptchaEmpty":"Please verify that you are not a robot.","sending":"Sending ..."};
/* ]]> */
</script>
<script src="https://sarbottam.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.3.1" type="text/javascript"></script>
<script src="https://sarbottam.com/wp-includes/js/masonry.min.js?ver=3.1.2" type="text/javascript"></script>
<script src="https://sarbottam.com/wp-includes/js/jquery/jquery.masonry.min.js?ver=3.1.2" type="text/javascript"></script>
<script src="https://sarbottam.com/wp-content/themes/me/js/slippry.min.js?ver=4.3.25" type="text/javascript"></script>
<script src="https://sarbottam.com/wp-content/themes/me/js/functions.js?ver=20150330" type="text/javascript"></script>
</body>
</html>