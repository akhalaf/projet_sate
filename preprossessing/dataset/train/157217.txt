<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>АСУ XXI ВЕК - Организация холодного обзвона и активных продаж</title>
<meta content="Организуем у Вас или сделаем за Вас холодный обзвон и активные продажи. Работаем по всей России!" name="description"/>
<meta content="повышение продаж, холодный обзвон, активные продажи, как заставить менеджеров звонить, холодные продажи, усиление продаж" name="keywords"/>
<meta content="DataLife Engine (http://dle-news.ru)" name="generator"/>
<link href="https://asales.pro/index.php?do=opensearch" rel="search" title="АСУ XXI ВЕК - Организация холодного обзвона и активных продаж" type="application/opensearchdescription+xml"/>
<link href="https://asales.pro/" rel="canonical"/>
<link href="https://asales.pro/rss.xml" rel="alternate" title="АСУ XXI ВЕК - Организация холодного обзвона и активных продаж" type="application/rss+xml"/>
<link href="/engine/classes/min/index.php?charset=utf-8&amp;f=engine/editor/css/default.css&amp;v=26" rel="stylesheet" type="text/css"/>
<script src="/engine/classes/min/index.php?charset=utf-8&amp;g=general&amp;v=26"></script>
<script defer="" src="/engine/classes/min/index.php?charset=utf-8&amp;f=engine/classes/js/jqueryui.js,engine/classes/js/dle_js.js&amp;v=26"></script>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="true" name="HandheldFriendly"/>
<meta content="telephone=no" name="format-detection"/>
<meta content="user-scalable=yes, initial-scale=1.0, maximum-scale=1.0, width=device-width" name="viewport"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="default" name="apple-mobile-web-app-status-bar-style"/>
<link href="/templates/asu/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<meta content="/templates/asu/images/logo.png" property="og:image"/>
<meta content="9821768b722fd3f2" name="yandex-verification"/>
<meta content="8e91e48a4141a363" name="yandex-verification"/>
<!--Редактор-->
<script src="/templates/asu/js/jHtmlArea-0.8.js" type="text/javascript"></script>
<link href="/templates/asu/css/jHtmlArea.css" media="none" onload="if(media!='all') media='all'" rel="Stylesheet" type="text/css"/>
<link href="/templates/asu/css/engine.css" media="none" onload="if(media!='all') media='all'" rel="stylesheet" type="text/css"/>
<link href="/templates/asu/css/slick.css?v=11" rel="stylesheet" type="text/css"/>
<link href="/templates/asu/css/styles.css?v=13" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<script type="text/javascript" src="/templates/asu/js/html5shiv.min.js"></script>
<![endif]-->
<!--<script src='/templates/asu/js/device.min.js'></script>-->
<script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function () { try { w.yaCounter24997127 = new Ya.Metrika({ id: 24997127, clickmap: true, trackLinks: true, accurateTrackBounce: true, webvisor: true, trackHash: true }); } catch (e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript>
<div><img alt="" src="https://mc.yandex.ru/watch/24997127" style="position:absolute; left:-9999px;"/></div>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '220397906109001');
fbq('track', 'PageView');
</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=220397906109001&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<!-- End Facebook Pixel Code -->
</noscript>
<!-- Getresponse Analytics -->
<script async="" src="https://www.email.asales.pro/script/ga.js?grid=pAFgIMkdddXgIBnN4" type="text/javascript"></script>
<!-- End Getresponse Analytics -->
</head>
<body>
<div class="page">
<header class="abs"> <div class="stuck_container" id="stuck_container">
<div class="container">
<div class="brand">
<a href="/"><img alt="АСУXXIВек" class="logo" src="/templates/asu/images/logo.png"/></a>
</div>
<div id="freecall"><span>8 800 333 47 73</span><br/>Делаем активные продажи<br/>Работаем по всей России</div>
<nav class="nav">
<ul class="sf-menu" data-type="navbar">
<li class="active"> <a href="/">Главная</a></li>
<li> <a href="#">Мероприятия</a>
<ul>
<li class="sub-sub-m"><a>ХОЛОДНЫЕ ЗВОНКИ</a>
<ul>
<li><a href="/webinar-scenariy-za-3-minuty.html">Вебинар «Как написать продающий сценарий за 3 минуты»</a></li>
<li><a href="/polchasa-v-shkure-menedzhera-holodnyh-zvonkov.html">Мастер-класс «Полчаса в шкуре менеджера холодных звонков»</a></li>
</ul>
</li>
<li class="sub-sub-m"><a>ОТДЕЛ ПРОДАЖ</a>
<ul>
<li><a href="/tri-glavnye-problemy-otdela-prodazh.html">Вебинар «3 главные проблемы отдела продаж»</a></li>
<li><a href="/pravilnye-zarplaty-dlya-menedzherov-v-b2b.html">Вебинар «Правильные зарплаты для менеджеров по продажам в b2b»</a></li>
<li><a href="/webinar-nastojchivyj-menedzher.html">Вебинар «Настойчивый менеджер»</a></li>
<li><a href="/kak-ne-teryat-klientov.html">Вебинар «Как не терять клиентов, когда у Вас 100 входящих звонков в день»</a></li>
</ul>
</li>
<li class="sub-sub-m"><a>CRM</a>
<ul>
<li><a href="/vebinar-crm-dlya-rukovoditelei.html">Вебинар «CRM для руководителей. Управление клиентской базой»</a></li>
</ul>
</li>
<li class="sub-sub-m"><a>ПРОДВИЖЕНИЕ</a>
<ul>
<li><a href="/webinar-sarafan-expertnosti.html">Вебинар «Как продавать то, что страшно покупать»</a></li>
<li><a href="/kak-sozdat-potok-zhirnyh-klientov-v-yuridicheskuyu-firmu.html">Вебинар «Поток жирных клиентов в юридическую фирму»</a></li>
</ul>
</li>
<li><a href="/otvety-na-voprosy.html">ВЕБИНАР "ОТВЕТЫ НА ВОПРОСЫ ПО ПРОДАЖАМ" (КАЖДЫЕ 2 НЕДЕЛИ)</a></li>
<li><a href="/sozdanie-udalennogo-otdela.html">"ВЕБИНАР "КАК СОЗДАТЬ УДАЛЕННЫЙ ОТДЕЛ ПРОДАЖ"</a></li>
</ul>
</li>
<li> <a href="#">Продукты</a>
<ul>
<li class="sub-sub-m">
<a>Сценарии продаж</a>
<ul>
<li><a href="/prodayushiy-scenariy-holodnogo-zvonka.html">Вебинар «Как написать продающий сценарий»</a></li>
<li><a href="/razrabotka-scenariya.html">Разработка сценария</a></li>
<li><a href="/korporativnaya-kniga-prodazh.html">Разработка Корпоративной книги продаж</a></li>
</ul>
</li>
<li class="sub-sub-m">
<a>Холодные звонки</a>
<ul>
<li><a href="/zvonar-v-arendu.html">Звонарь в аренду</a></li>
<li><a href="/centr-holodnyh-prodazh.html">Центр Холодных Продаж</a></li>
<li><a href="/1c-crm-centr-holodnyh-prodazh.html">1С:Центр Холодных Продаж</a></li>
</ul>
</li>
<li class="sub-sub-m">
<a>Отдел продаж</a>
<ul>
<li><a href="/otdel-aktivnyh-prodazh.html">Методика «Как организовать отдел активных продаж»</a></li>
<li><a href="/otdel-aktivnyh-prodazh-pod-klyuch-za-3-mesyaca.html">Отдел активных продаж под ключ</a></li>
<li><a href="/udalenniy-otdel-prodazh-za-den.html">Удаленный отдел продаж</a></li>
<li><a href="/razrabotka-struktury-otdela-aktivnyh-prodazh.html">РАЗРАБОТКА СТРУКТУРЫ ОТДЕЛА ПРОДАЖ</a></li>
<li><a href="/kommercheskoe-predlozhenie.html">Разработка коммерческого предложения</a></li>
<li><a href="/korporativnaya-kniga-prodazh.html">Разработка Корпоративной книги продаж</a></li>
<li><a href="/razrabotka-sistemy-oplaty-truda.html">Разработка системы оплаты труда</a></li>
<li><a href="/centr-holodnyh-prodazh.html">Центр Холодных Продаж</a></li>
<li><a href="/crm_otdel_aktivnih_prodaj.html">Настойчивый менеджер</a></li>
<li><a href="/rukovoditel-otdela-prodazh-v-arendu.html">Руководитель отдела продаж в аренду</a></li>
<li><a href="/nayim-menedjerov.html">Найм менеджеров</a></li>
</ul>
</li>
<li><a href="/trening-dlya-menedzherov-po-prodazham-menedzher-aktivnyh-prodazh.html">Тренинги для менеджеров</a></li>
<li class="sub-sub-m">
<a>Продвижение/Реклама</a>
<ul>
<li><a href="/obsledovanie-rynka-sozdanie-utp.html">Обследование рынка, создание УТП</a></li>
<li><a href="/landing-page.html">Продающая страница</a></li>
<li><a href="/sarafan-expertnosti.html">Сарафан экспертности</a></li>
</ul>
</li>
<li class="sub-sub-m">
<a>1С</a>
<ul>
<li><a href="/1c-programs.html">Программные продукты 1С</a></li>
<li><a href="/1c-soprovozhdenie.html">Сопровождение 1С</a></li>
</ul>
</li>
<li class="sub-sub-m big__menu">
<a class="big__menu-link">комплекты для самостоятельной разработки</a>
<ul>
<li><a href="/script-3min.html">Создать сценарий за 3 мин</a></li>
<li><a href="/scenariy-svoimi-rukami.html">Комплект «Сценарий своими руками»</a></li>
<li><a href="/kniga-prodazh-samostoyatelno.html">Корпоративная книга продаж своими руками</a></li>
<li><a href="/biblioteka-obhoda-vozrazheniy.html">Библиотека обработчиков типовых возражений</a></li>
<li><a href="/kak-prodavat-v-krizis.html">Библиотека обработчиков по коронавирусу</a></li>
<li><a href="/shablon-kommercheskogo-predlozheniya.html">Шаблон продающего КП</a></li>
<li><a href="/sbornik-akciy.html">Сборник акций</a></li>
<li><a href="/sistema-oplaty-truda.html">Система оплаты труда удаленного отдела продаж</a></li>
<li><a href="/kak-sklonit-klienta-oplatit-scheta.html">Как склонить клиента оплатить счета</a></li>
</ul>
</li>
</ul>
</li>
<li> <a href="/clients-reviews.html">Отзывы</a></li>
<!--li  >  <a href="#">Выполненные проекты</a> <ul>
							<li > <a href="/vypolnennye-proekty/">Кейсы</a></li>
							<li > <a href="/clients-reviews.html">Отзывы</a></li>
						</ul>
					</li-->
<li><a class="ancLinks" href="#yandex-map">Контакты </a> </li> </ul> </nav> </div> </div> </header>
<main>
<section>
<div class="camera_container">
<div class="camera_wrap" id="camera">
<div data-src="/templates/asu/images/page-1_slide01.jpg"></div>
<div data-src="/templates/asu/images/page-1_slide02.jpg"></div>
<div data-src="/templates/asu/images/page-1_slide03.jpg"></div>
</div>
<div class="camera_cnt">
<h2><a href="/script-3min.html">Хочу свой скрипт продаж за 3 минуты</a></h2>
<h2><a href="/otdel-aktivnyh-prodazh-pod-klyuch-za-3-mesyaca.html">Хочу свой отдел активных продаж</a></h2>
<h2><a href="/zvonar-v-arendu.html">Назвоните мне клиентов</a></h2>
<h2><a href="/crm_otdel_aktivnih_prodaj.html">Хочу увеличить продажи на уже существующей<br/>клиентской базе</a></h2>
</div>
</div>
</section>
<section class="well1">
<div class="container">
<h2>Клиенты о том, как <span>мы подняли им продажи</span></h2>
<div class="row mt2">
<div class="col-xs-12 col-sm-5">
<div class="box">
<div class="box_cnt__no-flow">
<!-- <div class="videoblock"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/xgeVospNNGk?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></div> -->
<div class="youtube" data-embed="xgeVospNNGk">
<button aria-label="Смотреть" class="ytp-large-play-button ytp-button"><svg height="100%" version="1.1" viewbox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z" fill="#212121" fill-opacity="0.8"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg></button>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-7">
<div class="box">
<div class="box_cnt__no-flow">
<h3>Коростелев Дмитрий Николаевич<span><br/>Генеральный директор ООО «Парадный Петербург», г. Санкт-Петербург</span></h3>
<p>«…Из десяти человек, которые пытались у нас работать колл-менеджерами, человек шесть мы выгнали, потому что не всех возможно приучить разговаривать именно по сценарию. Но те люди, которые готовы это делать, делают это просто и без особых возражений…<br/>…Сейчас звонит всего два человека, т.к. необходимости в большом штате звонарей особо то и нет…<br/>…<span class="fontweight400">за 3 месяца проведено 185 встреч</span>!…<br/>…Скрипт так написан, что, по словам моих колл-менеджеров, от встречи не уйти…Мои сотрудники разговаривают так, как обычно разговариваю я!…<br/>…<span class="fontweight400">валовая прибыль увеличилась на процентов 40-50</span>…»</p>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-5">
<div class="box">
<div class="box_cnt__no-flow">
<a class="imgzoom" href="/files/reviews/manotom.pdf" target="_blank"><img src="/files/reviews/manotom.jpg"/></a>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-7">
<div class="box">
<div class="box_cnt__no-flow">
<h3>Солонин Евгений Васильевич<br/><span>Директор ООО "Торгово-Производственное Объединение "Манотомь"</span></h3>
<p>…постоянный рост прибыли за счет того, что с обзвона приходит большое количество новых клиентов. <span class="fontweight400">За сентябрь клиентская база выросла на 30%, прибыль от новых клиентов составила 11%</span> от всего оборота компании, за октябрь – прирост клиентской базы - еще 26%, рост оборота –15% от общей суммы реализации...</p>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-5">
<div class="box">
<div class="box_cnt__no-flow">
<!-- <div class="videoblock"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/pj9GssL7MaM?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></div> -->
<div class="youtube" data-embed="pj9GssL7MaM">
<button aria-label="Смотреть" class="ytp-large-play-button ytp-button"><svg height="100%" version="1.1" viewbox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z" fill="#212121" fill-opacity="0.8"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg></button>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-7">
<div class="box">
<div class="box_cnt__no-flow">
<h3>Петрова Надежда Викторовна<span><br/>Владелец ООО «АУРА», г. Санкт-Петербург</span></h3>
<p>« …Дела на мебельном рынке все хуже и хуже. Лучше не становится. Работая со старыми клиентами мы видим, как тяжело работать сейчас…<br/>…Самую тяжелую, неблагодарную работу отдали в АСУ XXI Век. И работать стало намного легче. Легче искать менеджера, который будет продавать по встречам. Я даже в объявлении, когда ищу менеджера, пишу: «…холодные звонки делать не нужно…»…<br/>…Когда АСУ XXI Век начали звонить — я результат увидела сразу. Я видела, что это приносит результат. У меня был реально культурный шок…<br/>…Сценарий тоже так построен, чтобы не дать клиенту опомниться, чтобы сразу назначить встречу…<br/>…<span class="fontweight400">АСУ XXI Век назначили нам 246 встреч с клиентами. Порядка 100 клиентов точно стали нашими! Обороты компании выросли примерно на 30%!</span> …» </p>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-5">
<div class="box">
<div class="box_cnt__no-flow">
<!-- <div class="videoblock"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2RhKYkTd3sE?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></div> -->
<div class="youtube" data-embed="2RhKYkTd3sE">
<button aria-label="Смотреть" class="ytp-large-play-button ytp-button"><svg height="100%" version="1.1" viewbox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z" fill="#212121" fill-opacity="0.8"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg></button>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-7">
<div class="box">
<div class="box_cnt__no-flow">
<h3>ООО «Ситра-Т»</h3>
<p>…<span class="fontweight400">Работа call-менеджера дает чудотворные результаты</span>…<br/>…приходится сдерживать работу call-менеджера – не хватает менеджеров…<br/>…работа менеджера-профессионала не тратится на обзвон. Он работает с теплыми клиентами…<br/>…<span class="fontweight400">В месяц благодаря обзвону появляется 10-20 новых клиентов!</span> Решение «АСУ XXI Век» позволило значительно повысить обороты нашей компании! </p>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="parallax parallax_2" id="expert" style="background:url('/templates/asu/images/parallax01.jpg') no-repeat scroll 0% 0% / cover;">
<div class="container">
<h2><span>Экспертность АСУ XXI Век</span><br/>в активных продажах и холодном обзвоне:</h2>
<div class="row">
<ul class="marked-list" style="color:#f0f0f0;font-size:18px;">
<li>Более 10 лет помогает клиентам организовывать у себя отделы активных продаж и холодного обзвона с помощью продукта «<a class="underline" href="/centr-holodnyh-prodazh.html" target="_blank">Центр холодных продаж</a>»!</li>
<li>Оказывает услугу «<a class="underline" href="/zvonar-v-arendu.html" target="_blank">Звонарь в аренду</a>», звоним за клиентов более 5 лет.</li>
<li>44 000 звонков в месяц. Более 800 клиентов в месяц.</li>
<li>Разработали ПО «<a class="underline" href="/1c-crm-centr-holodnyh-prodazh.html" target="_blank">1С:Центр Холодных продаж</a>» для холодного обзвона и активных продаж! </li>
<li>Разрабатывает лучшие <a class="underline" href="/razrabotka-scenariya.html" target="_blank">сценарии холодного звонка</a> в стране! 150 – 200 сценариев в год! </li>
<li>15 продуктов для организации холодного обзвона от сценария холодного звонка до сопровождения холодного обзвона у клиентов! </li>
<li>Сотни успешных проектов холодного обзвона и благодарных клиентов (<a class="underline" href="/clients-reviews.html" target="_blank">Отзывы</a>)</li>
</ul>
</div>
</div>
</section>
<section class="well2">
<div class="container">
<h2>Новое в <span>блоге</span></h2>
<div class="row">
<div class="col-xs-offset-2 col-xs-8 col-sm-offset-0 col-sm-4">
<div class="box4">
<a class="blogimage" href="https://asales.pro/blog/247-novogodnij-fljesh-mob-2020-21-rasskazhi-o-svoih-uspehah-i-poluchi-novogodnij-podarok.html" title="Новогодний флэш-моб 2020-21 «Расскажи о своих успехах и получи новогодний подарок!»"><img alt="Новогодний флэш-моб 2020-21 «Расскажи о своих успехах и получи новогодний подарок!»" src="https://asales.pro/uploads/posts/2020-11/-mob.jpg"/></a>
<div class="box4_cnt" data-equal-group="2" style="height: 170px;"><div class="box_inner">
<h3><a href="https://asales.pro/blog/247-novogodnij-fljesh-mob-2020-21-rasskazhi-o-svoih-uspehah-i-poluchi-novogodnij-podarok.html" title="Новогодний флэш-моб 2020-21 «Расскажи о своих успехах и получи новогодний подарок!»">Новогодний флэш-моб 2020-21 «Расскажи о своих успехах и получи новогодний подарок!»</a></h3>
<p><a href="https://asales.pro/blog/247-novogodnij-fljesh-mob-2020-21-rasskazhi-o-svoih-uspehah-i-poluchi-novogodnij-podarok.html" title="Читать далее - Новогодний флэш-моб 2020-21 «Расскажи о своих успехах и получи новогодний подарок!»"> ... </a></p><meta content="929ea270265cbba75c8ef0b7ec87a0f1" name="cmsmagazine"/></div></div><script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?160",t.onload=function(){VK.Retargeting.Init("VK-RTRG-351933-6oWm0"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img alt="" src="https://vk.com/rtrg?p=VK-RTRG-351933-6oWm0" style="position:fixed; left:-999px;"/></noscript><div class="box4_panel">
<span style="margin-right:10px;"><span class="fa fa-calendar"></span> 22-12-2020, 08:06</span>
<span><span class="fa fa-user"></span> Просмотров: 787</span>
</div>
</div>
</div>
<div class="col-xs-offset-2 col-xs-8 col-sm-offset-0 col-sm-4">
<div class="box4">
<a class="blogimage" href="https://asales.pro/blog/237-poka-my-rabotaem-drugie-nervno-kurjat-v-storonke.html" title="Пока мы работаем, другие нервно курят в сторонке"><img alt="Пока мы работаем, другие нервно курят в сторонке" src="https://asales.pro/uploads/posts/2020-11/medium/1604325564_ologka_rop_s_knopkoy.png"/></a>
<div class="box4_cnt" data-equal-group="2" style="height: 170px;"><div class="box_inner">
<h3><a href="https://asales.pro/blog/237-poka-my-rabotaem-drugie-nervno-kurjat-v-storonke.html" title="Пока мы работаем, другие нервно курят в сторонке">Пока мы работаем, другие нервно курят в сторонке</a></h3>
<p>Как найти положительные отзывы в холодных звонках<a href="https://asales.pro/blog/237-poka-my-rabotaem-drugie-nervno-kurjat-v-storonke.html" title="Читать далее - Пока мы работаем, другие нервно курят в сторонке"> ... </a></p><meta content="929ea270265cbba75c8ef0b7ec87a0f1" name="cmsmagazine"/></div></div><script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?160",t.onload=function(){VK.Retargeting.Init("VK-RTRG-351933-6oWm0"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img alt="" src="https://vk.com/rtrg?p=VK-RTRG-351933-6oWm0" style="position:fixed; left:-999px;"/></noscript><div class="box4_panel">
<span style="margin-right:10px;"><span class="fa fa-calendar"></span> 2-11-2020, 17:19</span>
<span><span class="fa fa-user"></span> Просмотров: 650</span>
</div>
</div>
</div>
<div class="col-xs-offset-2 col-xs-8 col-sm-offset-0 col-sm-4">
<div class="box4">
<a class="blogimage" href="https://asales.pro/blog/236-7-raz-otmer-1-prezentuj-priemy-professionalnoj-prezentacii-produkta.html" title="7 раз отмерь - 1 презентуй: приёмы профессиональной презентации продукта"><img alt="7 раз отмерь - 1 презентуй: приёмы профессиональной презентации продукта" src="https://asales.pro/uploads/posts/2020-10/medium/1603461831_1.png"/></a>
<div class="box4_cnt" data-equal-group="2" style="height: 170px;"><div class="box_inner">
<h3><a href="https://asales.pro/blog/236-7-raz-otmer-1-prezentuj-priemy-professionalnoj-prezentacii-produkta.html" title="7 раз отмерь - 1 презентуй: приёмы профессиональной презентации продукта">7 раз отмерь - 1 презентуй: приёмы профессиональной презентации продукта</a></h3>
<p>Холодные звонки с правильным входом в диалог <a href="https://asales.pro/blog/236-7-raz-otmer-1-prezentuj-priemy-professionalnoj-prezentacii-produkta.html" title="Читать далее - 7 раз отмерь - 1 презентуй: приёмы профессиональной презентации продукта"> ... </a></p><meta content="929ea270265cbba75c8ef0b7ec87a0f1" name="cmsmagazine"/></div></div><script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?160",t.onload=function(){VK.Retargeting.Init("VK-RTRG-351933-6oWm0"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img alt="" src="https://vk.com/rtrg?p=VK-RTRG-351933-6oWm0" style="position:fixed; left:-999px;"/></noscript><div class="box4_panel">
<span style="margin-right:10px;"><span class="fa fa-calendar"></span> 21-10-2020, 17:01</span>
<span><span class="fa fa-user"></span> Просмотров: 554</span>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="bg-secondary well4">
<div class="container">
<ul class="flex-list">
<li><img alt="" src="/templates/asu/images/volkswagen.png"/></li>
<li><img alt="" src="/templates/asu/images/aviasejls.png"/></li>
<li><img alt="" src="/templates/asu/images/yandex-taxi.png"/></li>
<li><img alt="" src="/templates/asu/images/logo-tinkoff.png"/></li>
<li><img alt="" src="/templates/asu/images/logo_regionhimsnab.png"/></li>
<li><img alt="" src="/templates/asu/images/logo_leniipoekt.png"/></li>
</ul><ul class="flex-list">
<li><img alt="" src="/templates/asu/images/logo_aif.png"/></li>
<li><img alt="" src="/templates/asu/images/logo_port_vysockiy.png"/></li>
<li><img alt="" src="/templates/asu/images/logo_smit.png"/></li>
<li><img alt="" src="/templates/asu/images/logo_terma_energo.png"/></li>
<li><img alt="" src="/templates/asu/images/logo_iva.png"/></li>
<li><img alt="" src="/templates/asu/images/logo_zavod_energiya.png"/></li>
</ul>
<ul class="flex-list" style="margin-top:30px;">
<li><img alt="" src="/templates/asu/images/yandex-dost.png"/></li>
<li><img alt="" src="/templates/asu/images/logo_spik_szma.png"/></li>
<li><img alt="" src="/templates/asu/images/logo-mango.png"/></li>
<li><img alt="" src="/templates/asu/images/omk.jpg"/></li>
<li><img alt="" src="/templates/asu/images/logo_pervaya_polosa.png"/></li>
<li><img alt="" src="/templates/asu/images/interra.png"/></li>
</ul>
</div>
</section>
<section class="map">
<div class="map_model" id="yandex-map"></div>
</section>
<section class="bg1 well3">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-4">
<div class="addr">
<i aria-hidden="true" class="fa fa-map-marker linecons"></i>
<p>г. Санкт-Петербург<br/>ул. Мира, д. 3</p>
</div>
</div>
<div class="col-xs-12 col-sm-4">
<div class="addr">
<i aria-hidden="true" class="fa fa-envelope linecons"></i>
<p><a href="mailto:orders@asales.pro" title="Написать письмо">orders@asales.pro</a></p>
</div>
</div>
<div class="col-xs-12 col-sm-4">
<div class="addr">
<i aria-hidden="true" class="fa fa-phone-square linecons"></i>
<p><a href="tel:88003334773" title="Позвонить">8-800-333-47-73</a><br/><a href="tel:+78123097386" title="Позвонить">+7-812-309-73-86</a></p>
</div>
</div>
</div>
</div>
</section>
<div class="formmsg" id="buy" style="height:524px;">
<div id="formbgopacity" style="height:524px;">
<h2>Узнайте, как повысить продажи в вашем бизнесе!<br/>Запишитесь на бесплатную консультацию!</h2>
<form action="/modules/mailer-main.php" id="buyform" method="post" onsubmit="sendmsg(); return false;">
<div class="input-group-margin text-center" style="padding-top: 40px;">
<input id="buyer" maxlength="50" name="buyer" placeholder="Ваше имя" type="text" value=""/>
<input id="buyerphone" maxlength="20" name="buyerphone" placeholder="Ваш телефон" required="required" type="text" value=""/>
<div id="result-form"><input onclick="yaCounter24997127.reachGoal('mainform'); return true;" type="submit" value="Получить консультацию"/></div>
<input class="input_type_form" name="telefone" type="text"/>
</div>
</form>
</div>
</div>
<script type="text/javascript">
								function sendmsg() {
									var formData = new FormData($('#buyform')[0]);
									var loadingImg = "<br><center><img src='/templates/asu/images/loading.svg'></center>";
									$('#result-form').html(loadingImg);
									$.ajax({
										type: "POST",
										processData: false,
										contentType: false,
										url: "/modules/mailer-main.php",
										data: formData
									})
									.done(function (data) {
										$('#result-form').html(data);
									});
								};
							</script>
</main>
<footer>
<div class="container tc">
<div class="row">
<div class="col-xs-12 col-sm-4 text-left">
<p class="copy"><span class="copy-brand">ООО "Компания АСУ XXI Век"</span> © <span id="copyright-year"></span>
<br/><a class="copy-brand" href="tel:88003334773">8-800-333-47-73</a>   <a class="copy-brand" href="tel:+78123097386">+7-812-309-73-86</a></p>
</div>
<div class="col-xs-12 col-sm-4 text-center">
<ul class="social-list">
<li><a href="https://www.youtube.com/user/retivykhsergey?sub_confirmation=1" target="_blank" title="Наш Youtube канал"><span class="fa fa-youtube"></span></a></li>
<li><a href="https://vk.com/tehniki_prodazh" target="_blank" title="Мы в Вконтакте"><span class="fa fa-vk"></span></a></li>
<li><a href="https://www.facebook.com/%D0%9A%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D1%8F-%D0%90%D0%A1%D0%A3-XXI-%D0%92%D0%B5%D0%BA-381397718584118" target="_blank" title="Мы в Facebook"><span class="fa fa-facebook"></span></a></li>
</ul>
</div>
<div class="col-xs-12 col-sm-4 text-right" style="font-size:12px;"><img src="/templates/asu/images/pci-dss.png"/>
<br/>Данные защищены по международному стандарту PCI DSS и SSL</div>
</div>
</div>
</footer>
</div>
<div id="overlay"></div>
<script src="/templates/asu/js/jquery.cookie.min.js" type="text/javascript"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="/templates/asu/js/pointer-events.js"></script>
<script type="text/javascript">
$('html').addClass('lt-ie11');
$(document).ready(function(){
PointerEventsPolyfill.initialize({});
});
</script>
<![endif]-->
<script src="/templates/asu/js/tmstickup.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#stuck_container').TMStickUp({})
	});
</script>
<script src="/templates/asu/js/jquery.ui.totop.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$().UItoTop({
			easingType: 'easeOutQuart',
			containerClass: 'toTop fa fa-angle-up'
		});
	});
</script>
<script type="text/javascript">
	var currentYear = (new Date).getFullYear();
	$(document).ready(function () {
		$("#copyright-year").text((new Date).getFullYear());
	});
</script>
<script src="/templates/asu/js/jquery.easing.min.js" type="text/javascript"></script>
<script src="/templates/asu/js/superfish.js" type="text/javascript"></script>
<script src="/templates/asu/js/jquery.rd-navbar.js" type="text/javascript"></script>
<script src="/templates/asu/js/camera.min.js" type="text/javascript"></script>
<script type="text/javascript">
	var o = $('#camera');
	$(document).ready(function () {
		o.camera({
			autoAdvance: true,
			height: '33%',
			minHeight: '450px',
			pagination: false,
			thumbnails: false,
			playPause: false,
			hover: false,
			loader: 'none',
			navigation: true,
			navigationHover: false,
			mobileNavHover: false,
			fx: 'simpleFade'
		})
	});
</script>
<script src="/templates/asu/js/jquery.equalheights.js" type="text/javascript"></script>
<script src="/templates/asu/js/jquery.maskedinput.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$("input[name=bill_phone]").mask("+9 (999) 999-9999?99999");
		$("input[name=buyerphone]").mask("+9 (999) 999-9999?99999");
		$("input[name=custom_telephone]").mask("+9 (999) 999-9999?99999");
	});
</script>
<script type="text/javascript">
	( function() {

    var youtube = document.querySelectorAll( ".youtube" );
    
    for (var i = 0; i < youtube.length; i++) {
        
        var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";
        
        var image = new Image();
                image.src = source;
                image.addEventListener( "load", function() {
                    youtube[ i ].appendChild( image );
                }( i ) );
        
                youtube[i].addEventListener( "click", function() {

                    var iframe = document.createElement( "iframe" );

                            iframe.setAttribute( "frameborder", "0" );
                            iframe.setAttribute( "allowfullscreen", "" );
                            iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );

                            this.innerHTML = "";
                            this.appendChild( iframe );
                } );    
    };
    
} )();
</script>
<!--<script type="text/javascript" src="/templates/asu/js/script.js"></script>-->
<script src="/templates/asu/js/lib.js?v=2" type="text/javascript"></script>
<script src="https://api-maps.yandex.ru/2.1.64/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript">ymaps.ready(function () { var myMap = new ymaps.Map("yandex-map", { center: [59.959113, 30.313646], zoom: 16, controls: ["smallMapDefaultSet"] }); var myPlacemark = new ymaps.Placemark(myMap.getCenter(), { balloonContentBody: ["<address>", "<strong>\u0410\u0421\u0423XXI\u0412\u0435\u043a - \u0445\u043e\u043b\u043e\u0434\u043d\u044b\u0439 \u043e\u0431\u0437\u0432\u043e\u043d \u0438 \u0430\u043a\u0442\u0438\u0432\u043d\u044b\u0435 \u043f\u0440\u043e\u0434\u0430\u0436\u0438</strong>", "<br/>", "\u0410\u0434\u0440\u0435\u0441: \u0443\u043b. \u041c\u0438\u0440\u0430, \u0434.3", "</address>"].join("") }, { preset: "islands#redDotIcon" }); myMap.behaviors.disable("scrollZoom"); myMap.geoObjects.add(myPlacemark) });</script>
<script src="/templates/asu/js/modalwindows.js" type="text/javascript"></script>
<script src="/templates/asu/js/popup-3problem.js" type="text/javascript"></script>
<script charset="utf-8" src="https://yastatic.net/browser-updater/v1/script.js" type="text/javascript"></script>
<script>var yaBrowserUpdater = new ya.browserUpdater.init({ "lang": "ru", "browsers": { "yabrowser": "15.1", "chrome": "4", "ie": "10", "opera": "10.5", "safari": "4", "fx": "4", "amigo": "Infinity", "iron": "35", "flock": "Infinity", "palemoon": "25", "camino": "Infinity", "maxthon": "4.5", "seamonkey": "2.3" }, "theme": "yellow" });</script>
<div id="pp">
<div id="pp-close" title="Закрыть">х</div>
<div style="font-size:20px;margin-top:36px;">Подпишитесь на рассылку «Методы</div>
<div style="font-size:20px;">усиления продаж» и Вы БЕСПЛАТНО</div>
<div style="background-color: rgb(45, 151, 177); border-radius: 8px; color: rgb(255, 255, 255); font-size: 20px; width: 206px; padding: 4px; margin-top: 5px;">получите ВИДЕОРОЛИК</div>
<div style="color: rgb(255, 255, 255); font-size: 38px; margin-top: 14px;">«Три смертельные</div>
<div style="font-size: 28px; margin-top: 22px;">проблемы отделов продаж»</div>
<div id="pp-left">
<div class="text-uppercase" style="margin-top: 30px; font-weight: bold;">Из видео ролика вы узнаете:</div>
<div aria-hidden="true" class="fa fa-check-square-o" style="color:#39b7ce;font-size:21px;float:left;"></div>
<div style="padding-left:24px;">Отличается ли Ваш отдел продаж от отдела продаж «как у всех»</div>
<div style="height:5px;"></div>
<div aria-hidden="true" class="fa fa-check-square-o" style="color:#39b7ce;font-size:21px;float:left;"></div>
<div style="padding-left:24px;">Поймете почему Вам никогда не удастся заставить менеджеров делать холодные звонки.</div>
<div style="height:5px;"></div>
<div aria-hidden="true" class="fa fa-check-square-o" style="color:#39b7ce;font-size:21px;float:left;"></div>
<div style="padding-left:24px;">Узнаете как менеджеры «калибруют» клиентские базы.</div>
<div style="height:5px;"></div>
<div aria-hidden="true" class="fa fa-check-square-o" style="color:#39b7ce;font-size:21px;float:left;"></div>
<div style="padding-left:24px;">Узнаете самую заветную мечту менеджеров и на что они идут ради достижения этой цели.</div>
<div style="height:5px;"></div>
<div aria-hidden="true" class="fa fa-check-square-o" style="color:#39b7ce;font-size:21px;float:left;"></div>
<div style="padding-left:24px;">Узнаете как решать основные проблемы в отделе продаж и увеличить продажи в несколько раз с тем же количеством сотрудников.</div>
<div style="height:10px"></div>
</div>
<div id="pp-right">
<div>Подпишитесь!</div>
<form accept-charset="utf-8" action="https://www.email.asales.pro/add_subscriber.html" method="post" submit="yaCounter24997127.reachGoal('subscribe_tri'); return true;" target="_blank">
<input name="first_name" placeholder="Ваше имя" required="required" type="text" value=""/>
<input name="email" placeholder="Ваш email адрес" required="required" type="email" value=""/>
<input name="campaign_token" type="hidden" value="jt0Hf"/>
<input name="thankyou_url" type="hidden" value="https://asales.pro/confirm-popup-3problem.html"/>
<input name="forward_data" type="hidden" value=""/>
<input name="subscribe" onclick="cookieyear();" type="submit" value="Подписаться"/>
</form>
</div>
</div>
<div id="pp-bg"></div>
<script async="async" charset="UTF-8" src="//eyenewton.ru/scripts/callback.min.js" type="text/javascript"></script>
<script type="text/javascript">/*<![CDATA[*/var newton_callback_id = "14fccb8b767d8e23ceb33203ff65f949";/*]]>*/</script>
<a id="mibew-agent-button" onclick="Mibew.Objects.ChatPopups['58591679ce1bbd6f'].open();return false;" target="_blank"></a>
<script src="/mibew/js/compiled/chat_popup.js" type="text/javascript"></script>
<script type="text/javascript">Mibew.ChatPopup.init({ "id": "58591679ce1bbd6f", "url": "\/mibew\/chat?locale=ru", "preferIFrame": true, "modSecurity": false, "width": 640, "height": 480, "resizable": true, "styleLoader": "\/mibew\/chat\/style\/popup" });</script>
<div id="mibew-invitation"></div>
<script src="/mibew/js/compiled/widget.js" type="text/javascript"></script>
<script type="text/javascript">Mibew.Widget.init({ "inviteStyle": "\/mibew\/styles\/invitations\/default\/invite.css", "requestTimeout": 10000, "requestURL": "\/mibew\/widget", "locale": "ru", "visitorCookieName": "MIBEW_VisitorID" });</script>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-105702701-1"></script>
<script>window.dataLayer = window.dataLayer || []; function gtag() { dataLayer.push(arguments) }; gtag('js', new Date()); gtag('config', 'UA-105702701-1');</script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.3/jquery.inputmask.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>
 --><!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.3/phone-codes/phone.js"></script> -->
<!-- <script src="https://syn.su/js/phone-addon.js"></script> -->
<!-- <script>
	$(document).ready(function(){
		$("#phone").inputmask("+9 (999) 999-9999");
		$("input[name=bill_phone]").inputmask("+9 (999) 999-9999");
		$("input[name=buyerphone]").inputmask("+9 (999) 999-9999");
		$("input[name=custom_telephone]").inputmask("+9 (999) 999-9999");
  // var formattedPhone = Inputmask.format("999999999", { alias: "phone", inputFormat: "+9 (999) 999-999"});
});
</script>
 -->
<script>
(function(w, d, s, h, id) {
w.roistatProjectId = id; w.roistatHost = h;
var p = d.location.protocol == "https:" ? "https://" : "http://";
var u = /^.roistat_visit=[^;]+(.)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, 'script', 'cloud.roistat.com', '485ee9ee6fef315f2db805a5a63618bd');
</script>
<script src="/templates/asu/js/slick.min.js"></script>
</body>
</html>
<!-- DataLife Engine Copyright SoftNews Media Group (http://dle-news.ru) -->
