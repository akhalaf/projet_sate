<?xml version="1.0" encoding="iso-8859-1"?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- START TEMPLATE bahninfo/header.tpl --><html lang="de_DE" xml:lang="de_DE" xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
form {
display: inline;
}
</style>
<title>BahnInfo-Forum ::  www.bahninfo-forum.de</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="https://www.bahninfo-forum.de/css.php?0,css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.bahninfo-forum.de/css.php?0,css_print" media="print" rel="stylesheet" type="text/css"/>
<script src="https://www.bahninfo-forum.de/javascript.php" type="text/javascript"></script>
<script type="text/javascript">
// <![CDATA[
  fileLoadingImage = 'https://www.bahninfo-forum.de/mods/embed_images/viewers/lightbox/code/images/loading.gif';
  fileBottomNavCloseImage = 'https://www.bahninfo-forum.de/mods/embed_images/viewers/lightbox/code/images/close.gif';
</script><style type="text/css">
  #imageData #bottomNavClose{ text-align:right; }
  #prevLink, #nextLink{
    background: transparent url(https://www.bahninfo-forum.de/mods/embed_images/viewers/lightbox/code/images/blank.gif) no-repeat;
  }
  #prevLink:hover, #prevLink:visited:hover {
    background: url(https://www.bahninfo-forum.de/mods/embed_images/viewers/lightbox/code/images/prevlabel.gif) left 15% no-repeat;
  }
  #nextLink:hover, #nextLink:visited:hover {
    background: url(https://www.bahninfo-forum.de/mods/embed_images/viewers/lightbox/code/images/nextlabel.gif) right 15% no-repeat;
  }
// ]]>
</style>
<!--[if lte IE 6]>
  <style type="text/css">
  #phorum {
  width:       expression(document.body.clientWidth > 1220               ? '1220px': 'auto' );
  margin-left: expression(document.body.clientWidth > 1220               ? parseInt((document.body.clientWidth-1220)/2) : 0 );
  }
  </style>
  <![endif]-->
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<!--
Some Icons courtesy of:
  FAMFAMFAM - http://www.famfamfam.com/lab/icons/silk/
  Tango Project - http://tango-project.org/
-->
</head>
<body onload="">
<div id="phorum">
<div class="logged-out" id="user-info">
<span class="welcome">Willkommen!</span>
<a class="icon icon-key-go" href="https://www.bahninfo-forum.de/login.php">Einloggen</a>
<a class="icon icon-user-add" href="https://www.bahninfo-forum.de/register.php">Ein neues Profil erzeugen</a>
</div>
<div id="logo">
<div class="bi-logo">
<a href="https://www.bahninfo-forum.de/">
<img alt="BahnInfo-Forum" border="0" height="50" src="/templates/bahninfo/images/logo.png" width="139"/>
</a>
</div>
<div class="bi-ad">
<span style="text-align: center; display: block;">* Werbung *</span>
<a href="https://bit.ly/bi-bahnberufe" rel="noopener" style="display: inline-block; margin-right: 10px; margin-bottom: 10px;" target="_blank">
<img border="0" src="/templates/bahninfo/images/customers/bahnberufe.gif" style="margin: 0; width: 100%; max-width: 468px;"/>
</a>
</div>
<div class="clearfix"></div>
</div> <!-- end of div id=logo -->
<div id="breadcrumb">
<!-- a  href="https://www.bahninfo-forum.de/index.php">Startseite</a -->
<a href="https://www.bahninfo-forum.de/index.php">Startseite</a>
<span id="breadcrumbx"></span>
</div> <!-- end of div id=breadcrumb -->
<div class="icon-zoom" id="search-area">
<form action="https://www.bahninfo-forum.de/search.php" id="header-search-form" method="get">
<input name="forum_id" type="hidden" value="0"/>
<input name="phorum_page" type="hidden" value="search"/>
<input name="match_forum" type="hidden" value="ALL"/>
<input name="match_dates" type="hidden" value="365"/>
<input name="match_threads" type="hidden" value="0"/>
<input name="match_type" type="hidden" value="ALL"/>
<input class="styled-text" name="search" size="20" type="text" value=""/><input class="styled-button" type="submit" value="Suche"/><br/>
<a href="https://www.bahninfo-forum.de/search.php">erweitert</a>
</form>
</div> <!-- end of div id=search-area -->
<div id="page-info">
<span class="h1 heading">BahnInfo-Forum</span>
</div> <!-- end of div id=page-info -->
<!-- END TEMPLATE bahninfo/header.tpl -->
<!-- BEGIN TEMPLATE index_new.tpl -->
<table cellspacing="1" class="list tborder fullwidth"><!-- if class="tborder", we need cellspacing=1 -->
<tr>
<th align="left">
<img alt="•" class="icon1616" src="/templates/bahninfo/images/folder.png"/>
                        Foren                    </th>
<th>Themen</th>
<th>Beiträge</th>
<th>Letzter Beitrag</th>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?33">Ankündigungen</a></span>
<p></p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        16                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        328                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        01.11.2019 19:19                    </td>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?20">Auslandsforum</a></span>
<p>Das Auslandsforum - für alle Eisenbahn- und ÖPNV-Themen außerhalb von Deutschland  - talk about railroads in German or English!</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        320                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        1.305                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        10.01.2021 15:25                    </td>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?10">Bayrisches Nahverkehrsforum</a></span>
<p>Unser Nahverkehrsforum für Bayern mit Ausnahme von Franken.</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        1.329                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        7.672                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        06.09.2020 04:25                    </td>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?9">Berliner Nahverkehrsforum</a></span>
<p>Das Forum für den ÖPNV in Berlin und Brandenburg (VBB-Gebiet).</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        9.060                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        386.495                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        13.01.2021 15:17                    </td>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?8">Deutschlandforum</a></span>
<p>Das Forum für alle Themen rund um die Eisenbahn in ganz Deutschland - soweit für diese keine extra Region vorhanden ist.</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        1.576                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        9.077                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        08.01.2021 19:32                    </td>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?34">Fernbus-Forum</a></span>
<p>Dieses Forum dient der Diskussion zu Fernbusthemen.</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        31                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        185                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        08.03.2020 18:15                    </td>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?7">Forum für Modellbahnen, Simulationen und Spiele</a></span>
<p>... alles über die Modelleisenbahn, Simulationsprogramme und Spiele mit Bahnbezug</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        336                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        934                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        05.01.2021 22:56                    </td>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?24">Nahverkehrsforum Bremen / Niedersachsen</a></span>
<p>Das Forum für den Nahverkehr in Bremen und Niedersachsen</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        550                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        4.943                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        13.01.2021 09:34                    </td>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?28">Nahverkehrsforum Franken</a></span>
<p>Unser Forum für den Nahverkehr in Franken (z. B. Nürnberg, Fürth, Würzburg)</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        2.416                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        41.184                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        13.01.2021 08:43                    </td>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?19">Nahverkehrsforum Hessen</a></span>
<p>Das Forum für den ÖPNV in Hessen zu den Themen Bus &amp; Bahn u. a.  im Rhein-Main-Verkehrsverbund - RMV (u. a. Frankfurt/Main, Offenbach, Wiesbaden) und im Nordhessischen Verkehrsverbund NVV (u. a. Kassel)</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        277                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        918                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        11.01.2021 16:03                    </td>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?11">Nahverkehrsforum Nordrhein-Westfalen</a></span>
<p>Das Forum für den Nahverkehr in Nordrhein-Westfalen</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        816                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        2.736                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        22.08.2020 23:39                    </td>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?22">Nahverkehrsforum Ostdeutschland</a></span>
<p>Unser Nahverkehrsforum für Freunde des ÖPNV im Osten Deutschlands</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        446                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        1.570                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        28.12.2020 13:00                    </td>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?27">Nahverkehrsforum Schleswig-Holstein</a></span>
<p>Unser Nahverkehrsforum für Schleswig-Holstein</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        1.008                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        14.786                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        13.01.2021 15:33                    </td>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?18">Nahverkehrsforum Südwestdeutschland</a></span>
<p>Das Nahverkehrsforum für den ÖPNV in Baden-Württemberg, Rheinland-Pfalz und dem Saarland</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        363                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        1.154                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        23.12.2020 17:06                    </td>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?35">RadfahrInfo-Forum</a></span>
<p>Das Forum über alles, was am (Zwei-)Rad dreht.
</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        15                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        111                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        27.12.2020 14:28                    </td>
</tr>
<tr>
<th align="left">
<img alt="•" class="icon1616" src="/templates/bahninfo/images/folder.png"/>
                        Hamburger Nahverkehrsforen                    </th>
<th>Themen</th>
<th>Beiträge</th>
<th>Letzter Beitrag</th>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?25">Hamburger Busforum</a></span>
<p>Das Forum für alle Themen, die sich mit dem Thema "Busse" im Gebiet des HVV beschäftigen.</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        2.106                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        41.377                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        12.01.2021 10:44                    </td>
</tr>
<tr>
<td class="alt1" width="55%">
<span class="h3"><a href="https://www.bahninfo-forum.de/list.php?5">Hamburger Nahverkehrsforum</a></span>
<p>Das Forum für den Hamburger Nahverkehr zu den Themen U-Bahn, S-Bahn, Regionalbahn, Schiff und Straßenbahn.</p>
</td>
<td align="center" class="alt2" nowrap="nowrap" width="12%">
                        7.513                                            </td>
<td align="center" class="alt1" nowrap="nowrap" width="12%">
                        151.925                                            </td>
<td align="center" class="alt2" nowrap="nowrap" width="21%">
                        13.01.2021 15:33                    </td>
</tr>
</table>
<!-- END TEMPLATE index_new.tpl -->
<!-- BEGIN TEMPLATE bahninfo/footer.tpl -->
<div id="footer-links"><a href="https://www.bahninfo-forum.de/impressum.php">Impressum</a> | <a href="https://www.bahninfo-forum.de/datenschutz.php">Datenschutzerklärung</a> | <a href="https://www.bahninfo-forum.de/forumregeln.php">Forumregeln</a>
<div id="footer-plug">
	      Powered by <a href="http://www.phorum.org/">Phorum</a>.
	      Classic Emerald template modified by <a href="https://www.bahninfo.de/">BahnInfo</a>.<br/>
	       <br/>
</div>
</div>
</div> <!-- end of div id="phorum" -->
</body>
</html>
<!-- END TEMPLATE bahninfo/footer.tpl -->
