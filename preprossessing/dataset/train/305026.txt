<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>Welcome to CeyBank.lk</title>
<link href="http://www.ceybank.com/css/main.css" rel="stylesheet" type="text/css"/>
<link href="http://www.ceybank.com/css/formee-structure.css" rel="stylesheet" type="text/css"/>
<link href="http://www.ceybank.com/css/formee-style.css" rel="stylesheet" type="text/css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="http://www.ceybank.com/js/modernizr-2.0.6.min.js"></script>
<script src="http://www.ceybank.com/js/formee.js"></script>
</head>
<body>
<!-- HEADER -->
<header id="site-header">
<section class="site-section">
<div class="logo floatleft"><a href="/"></a></div>
<div class="header-middle floatleft">
<div class="top-menu floatright">
<a class="corporate" href="http://www.ceybank.com/how-to-invest#corporate-investors">Corporate Investors</a>
<a class="individual" href="http://www.ceybank.com/how-to-invest#individual-investors">Individual Investors</a>
<a class="foreign" href="http://www.ceybank.com/how-to-invest#foreign-investors">Foreign Investors</a><div class="clearfix"></div>
</div>
<div class="tatoo floatright"></div><div class="clearfix"></div>
</div>
<!--<div class="lang-menu floatleft"></div><div class="clearfix"></div>-->
<div><img border="0" class="lang-menu floatleft" src="http://www.ceybank.com/images/header-right-curvs.png" usemap="#Map"/>
<map name="Map">
<area coords="3,3,80,38" href="sinhala" shape="rect"/>
<area coords="3,40,80,77" href="tamil" shape="rect"/>
</map>
</div>
</section>
</header>
<!-- NAVIGATION -->
<nav id="site-nav">
<section class="site-section">
<a class="home" href="http://www.ceybank.com/">HOME</a>
<a class="about" href="http://www.ceybank.com/pages/about_us">ABOUT US</a>
<a class="product" href="http://www.ceybank.com/pages/products">PRODUCTS &amp; SERVICES</a>
<a class="how" href="http://www.ceybank.com/pages/how_to_invest">HOW TO INVEST</a>
<a class="static" href="http://www.ceybank.com/pages/statistics">STATISTICS</a>
<a class="faq" href="http://www.ceybank.com/pages/faq">FAQ</a>
<a class="qlinks" href="http://www.ceybank.com/pages/quick_links">QUICK LINKS</a>
<a class="contact" href="http://www.ceybank.com/pages/contact_us">CONTACT US</a>
<div class="clearfix"></div>
</section>
</nav>
<!-- SLIDER -->
<style type="text/css">

.site-section .col .col-3-right.floatleft .box table {

	text-align: center;

}

.site-section .col .col-3-right.floatleft .box table tr td {

	text-align: left;

}

.pdfs {

	margin-left: 0px;

	padding-left: 0px!important;

}

.tital3 {

	font-size: 12px;

}

.pdfs li {

	background: url(../images/application_pdf.png) no-repeat 0 0;

	padding-left: 60px;

	line-height: 60px;

	display: block;

}

</style>
<section class="site-section">
<!-- <div id="piecemaker">

    <p>Please download adobe flash player.</p>

  </div> -->
<div id="cssSlider">
<div id="sliderImages">
<a href="http://www.ceybank.com/downloads">
<img alt="" id="si_1" src="http://www.ceybank.com/piecemaker/contents/cnew01.jpg"/></a>
<a href="http://www.ceybank.com/downloads">
<img alt="" id="si_2" src="http://www.ceybank.com/piecemaker/contents/cnew02.jpg"/></a>
<div style="clear:left;"></div>
</div>
</div>
</section>
<section class="site-section">
<div class="col">
<div class="col-3-left floatleft">
<a class="heading utf" href="">Unit Trust Funds</a>
<div class="icon-menu">
<a class="ccgf" href="http://www.ceybank.com/ceybank-country-growth-fund"><span>Ceybank Century Growth Fund</span></a>
<a class="cutf" href="http://www.ceybank.com/ceybank-unit-trust"><span>Ceybank Unit Trust Fund</span></a>
<a class="cspf" href="http://www.ceybank.com/ceybank-savings-plus"><span>Ceybank Savings Plus Fund</span></a>
<a class="csgf" href="http://www.ceybank.com/ceybank-surakum"><span>Ceybank Surakum Gilt Fund</span></a>
<a class="csgf" href="http://www.ceybank.com/ceybank-gilt-edge-fund-b-series"><span>Ceybank Gilt Edge Fund (B Series)</span></a>
<a class="csgf" href="http://www.ceybank.com/ceybank_high_yield_found"><span>Ceybank High Yield Fund</span></a>
</div>
<a class="heading ip" href="http://www.ceybank.com/investment-plans">Investment Plans</a>
<a class="heading pm" href="http://www.ceybank.com/portfolio-managment">Portfolio Managment</a>
<a class="heading ql" href="http://www.ceybank.com/advisory-services">Advisory Services</a>
<a class="heading gl" href="http://www.ceybank.com/gallery">Picture Gallery</a>
<!--<div class="news-index">

        <b>News</b>

        <ul id="news-slide">

            <li><i>05 April, 2012</i><p>The management of CAML is vested in the Board, comprising seven Directors with rich experience .....</p></li>

        </ul>

    </div>-->
</div>
<div class="col-3-centre floatleft contant">
<h1>Ceybank Asset Management Ltd.</h1>
<p style="text-align:left;">Ceybank Asset Management Ltd (Ceybank AML) is a leading Sri Lankan asset management company which caters to a wide variety of investor needs.</p>
<p>Our Objective is to fulfill the diverse investor needs and expectations by providing channels of investment to the Capital and Money markets through a range of Products &amp; Services. (Funds/ Investment Plans/ Portfolio management &amp; Investment Advisory services).</p>
<p>Ceybank AML caters to both the individual and Corporate investor across the risk/return spectrum from High Growth to Capital Preservation providing cash management and Tax efficient investment solutions, to name a few. We take pride in saying that <b>"Whoever you are, Whatever you do, Ceybank has an Investment Solution for you"</b></p>
<p>Ceybank AML is licensed and regulated by the Securities &amp; Exchange Commission of Sri Lanka (SEC) to manage unit trusts and investment portfolios. The company is owned by four giants in the industry. Bank of Ceylon (BOC), Sri Lanka Insurance Ltd (SLIC), Carson Cumberbatch PLC (Carsons) and Unit Trust of India (UTI).</p>
<p>The National Savings Bank (NSB) acts as the Trustee for all Ceybank Unit Trust Funds</p>
<div><img src="http://www.ceybank.com/images/annual-report-new.jpg"/></div>
<ul class="pdfs">
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/Ceybank-Gilt-Edged-Fund-A-series-Annual-Report-2019.pdf" target="_blank"><strong>Ceybank Gilt Edged Fund (A Series) Annual Report 2019</strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/Ceybank-Gilt-Edged-Fund-B-series-Annual-Report-2019.pdf" target="_blank"><strong>Ceybank Gilt Edged Fund (B Series) Annual Report 2019</strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/Ceybank-Surakum-Fund-Annual-Report-2019.pdf" target="_blank"><strong>Ceybank Surakum Fund Annual Report 2019</strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/Ceybank-Savings-Plus-Fund-Annual-Report-2019.pdf" target="_blank"><strong>Ceybank Savings Plus Fund Annual Report 2019</strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/Ceybank-High-Yeild-Fund-Annual-Report-2019.pdf" target="_blank"><strong>Ceybank High Yield Fund Annual Report 2019</strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/surakum-fund-annual-report-31-december-2018.pdf" target="_blank"><strong>Ceybank Surakum Fund Anual Report 31 December 2018</strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/savings-plus-fund-annual-report-31-december-2018.pdf" target="_blank"><strong>Ceybank Savings Plus Fund Annual Report 31 December 2018</strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/gilt-edged-fund-B-series-ann-rep-2018.pdf" target="_blank"><strong>Ceybank Gilt Edged Fund (B Series) Annual Report 31 December 2018 </strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/gilt-edged-fund-A-series-ann-rep-2018.pdf" target="_blank"><strong>Ceybank Gilt Edged Fund (A Series) Annual Report 31 December 2018 </strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/high-yield-fund-annual-report-31-december-2018.pdf" target="_blank"><strong>Ceybank High Yield Fund Annual Report 31 December 2018 </strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/ceybank-century-growth-fund-annual-report-31-mar-2019.pdf" target="_blank"><strong>Ceybank Century Growth Fnd Annual Report -<br/>2018 -2019</strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/ceybank-unit-trust-fundannual-report-31-mar-2018-2019.pdf" target="_blank"><strong>Ceybank Unit Trust Fund Annual Report -<br/>2018- 2019</strong></a></li>
<!--  <li style="line-height:22px; margin-bottom: 10px;"><a href="docs/Ceybank Gilt Edged Fund (A Series) Annual Report - 31 December 2016.pdf" target="_blank"><strong>Ceybank Gilt Edged Fund (A Series) Annual Report -<br>31 December 2016</strong></a></li>

      <li style="line-height:22px; margin-bottom: 10px;"><a href="docs/Ceybank Gilt Edged Fund (B Series) Annual Report - 31 December 2016.pdf" target="_blank"><strong>Ceybank Gilt Edged Fund (B Series) Annual Report -<br>31 December 2016</strong></a></li>

      <li style="line-height:22px; margin-bottom: 10px;"><a href="docs/Ceybank Savings Plus Fund  Annual  Report-31 December 2016.pdf" target="_blank"><strong>Ceybank Savings Plus Fund  Annual  Report-<br>31 December 2016</strong></a></li>

      <li style="line-height:45px; margin-bottom: 10px;"><a href="docs/Ceybank Surakum Fund Annua Report- 31 December 2016.pdf" target="_blank"><strong>Ceybank Surakum Fund Annua Report 31 December 2016</strong></a></li>



       -->
<br/>
<div><img src="http://www.ceybank.com/images/2018-r-banner.png"/></div>
<ul class="pdfs">
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/Surakum-Half-Year-Report-30-June-2020.pdf" target="_blank"><strong>Surakum Fund - 30th June 2020</strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/High-Yeild-Half-Year-Report-30-June-2020.pdf" target="_blank"><strong>Ceybank High Yield Fund - 30th June 2020</strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/B-Series-Half-Year-Report-30-June-2020.pdf" target="_blank"><strong>Gilt Edge Fund - (B Series) 30th June 2020</strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/A-Series-Half-Year-Report-30-June-2020.pdf" target="_blank"><strong>Gilt Edge Fund - (A Series) 30th June 2020</strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/CCGF-Interim-Report-Sept-2019.pdf" target="_blank"><strong>Ceybank Century Growth Fund 30 September 2019</strong></a></li>
<li style="line-height:22px; margin-bottom: 10px;"><a href="docs/Interim-Report-Sept-2019.pdf" target="_blank"><strong>Ceybank Unit Trust Fund 30 September 2019</strong></a></li>
<li><a href="http://www.ceybank.com/docs/Ceybank-Gilt-Edged-Fund-A-Series-Interim-Report-30-June-2019.pdf" style="line-height:45px;" target="_blank"><strong>

          Ceybank Gilt Edged Fund (A Series) Interim Report 2019</strong></a></li>
<li><a href="http://www.ceybank.com/docs/Ceybank-High-Yield-Fund-Interim-Report-30-June-2019.pdf" style="line-height:45px;" target="_blank"><strong>

          Ceybank High Yield Fund Interim Report 2019</strong></a></li>
<li><a href="http://www.ceybank.com/docs/Ceybank-Surakum-Fund-Interim-Report-30-June-2019.pdf" style="line-height:45px;" target="_blank"><strong>

          Ceybank Surakum Fund Interim Report 2019</strong></a></li>
<li><a href="http://www.ceybank.com/docs/Ceybank-Savings-Plus-Fund-Interim-Report-30-June-2019.pdf" style="line-height:45px;" target="_blank"><strong>Ceybank Savings Plus Fund Interim Report 2019</strong></a></li>
<li><a href="http://www.ceybank.com/docs/Ceybank-GIlt-Edge-B-series-Fund-Interim-Report-30-June-2019.pdf" style="line-height:45px;" target="_blank"><strong>Ceybank Gilt Edged Fund (B Series) Interim Report 2019</strong></a></li>
<!--  <li style="line-height:45px;"><a href="docs/Ceybank Unit-trust-Fund-Interim Report-September-2016.pdf" target="_blank"><strong>Ceybank Unit trust Fund Interim Report September 2016</strong></a></li>

          <li style="line-height:45px;"><a href="docs/Ceybank-Century-Growth-Fund-Interim-Report-September-2016.pdf" target="_blank"><strong>Ceybank Century Growth Fund Interim Report September 2016</strong></a></li>

          <li><a href="docs/Ceybank-Surakum-Fund-Interim-Report-June-2016.pdf" target="_blank"><b>Ceybank Surakum Fund Interim Report 2016</b></a></li>

          <li><a href="docs/Ceybank-Savings-Plus-Fund-Interim-Report-June 2016.pdf" target="_blank"><b>Ceybank Savings Plus Fund Interim Report 2016</b></a></li>

          <li><a href="docs/Ceybank-GIlt Edge-B-series-Fund-Interim-Report-June-2016.pdf" target="_blank"><b>Ceybank Gilt Edged Fund (B Series) Interim Report 2016</b></a></li> -->
</ul>
<!-- -->
<!-- <li style="line-height:45px;"><a href="docs/cgef-annual-report-2015.pdf" target="_blank"><strong>Ceybank Gilt Edged Fund Annual Report 2015</strong></a></li>-->
</ul>
</div>
<div class="col-3-right floatleft"> <a class="contactus" href="http://www.ceybank.com/contact-us">Contact US +94(11) 7602000</a><br/>
<h2>Unit Trust Prices as at: <span class="text-blue">2021-01-12</span></h2>
<div class="box">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<th>Fund</th>
<th>Bid Price</th>
<th>Offer Price</th>
</tr>
<tr>
<td>Ceybank Unit Trust</td>
<td><p>    24.63</p></td>
<td><p>    26.25</p></td>
</tr>
<tr>
<td>Ceybank Century Growth</td>
<td><p>    76.36</p></td>
<td><p>    79.83</p></td>
</tr>
<tr>
<td>Ceybank Savings Plus</td>
<td><p>    0.2600</p></td>
<td><p>    0.2600</p></td>
</tr>
<tr>
<td>Ceybank Surakum</td>
<td><p>    12.3513</p></td>
<td><p>    12.3513</p></td>
</tr>
<tr>
<td>Ceybank Glit Edge (A Series)</td>
<td><p>    14.4424</p></td>
<td><p>    14.4424</p></td>
</tr>
<tr>
<td>Ceybank Glit Edge (B Series)</td>
<td><p>    10.8814</p></td>
<td><p>    10.8814</p></td>
</tr>
<tr>
<td>Ceybank High Yield </td>
<td><p>    13.7467</p></td>
<td><p>    13.7467</p></td>
</tr>
</table>
</div>
<br/>
<a class="button howtoinvest" href="http://www.ceybank.com/how-to-invest">How to invest</a> <a class="button download" href="http://www.ceybank.com/downloads">Download</a> </div>
<!--<div class="col-3-right floatleft" style="padding-top:55px;"><img src="http://www.ceybank.com/images/gilt-edged-A-series.jpg" /></div>-->
<div class="clearfix"> </div>
</div>
</section>
<!-- GOOGLE ANALYTICS -->
<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');



  ga('create', 'UA-78729293-1', 'auto');

  ga('send', 'pageview');



</script>
<!-- FOR FLASH SLIDER -->
<script src="http://www.ceybank.com/piecemaker/scripts/swfobject/swfobject.js" type="text/javascript"></script>
<script type="text/javascript">



    var flashvars = {};



    flashvars.cssSource = "http://www.ceybank.com/piecemaker/piecemaker.css";



    flashvars.xmlSource = "http://www.ceybank.com/piecemaker/piecemaker.xml";



		



    var params = {};



    params.play = "true";



    params.menu = "false";



    params.scale = "showall";



    params.wmode = "transparent";



    params.allowfullscreen = "true";



    params.allowscriptaccess = "always";



    params.allownetworking = "all";



	  



    swfobject.embedSWF('http://www.ceybank.com/piecemaker/piecemaker.swf', 'piecemaker', '990', '300', '10', null, flashvars,    

    params, null);



    



</script>
<style type="text/css">

  

  .socialmediaiconsside { width: 45px; height: 200px; position: absolute; z-index: 9999; right: 0; top: 15%; }

</style>
<!-- FOOTER -->
<footer id="site-footer">
<!--<section class="site-section">
    	<div class="quicklinks floatleft">
        	<b>Quick Links</b>
        	<ul>
            	<li><a href="http://www.ceybank.com/audited-annual-accounts">Audited Annual Accounts</a></li>
            	<li><a href="http://www.ceybank.com/ceybank-surakum">Ceybank Surakum</a></li>
            	<li><a href="http://www.ceybank.com/ceybank-country-growth-fund">Ceybank Century Growth Fund</a></li>
            	<li><a href="http://www.ceybank.com/ceybank-unit-trust">Ceybank Unit Trust</a></li>
            	<li><a href="http://www.ceybank.com/ceybank-savings-plus">Ceybank Savings Plus</a></li>
            	<li><a href="http://www.ceybank.com/ceybank-gilt-edged-fund">Ceybank Gilt-Edged Fund</a></li>
            	<li><a href="http://www.ceybank.com/how-to-invest">How to invest</a></li>
            	<li><a href="http://www.ceybank.com/downloads">Downloads</a></li>
            </ul>
        </div>
        <div class="partners floatright"><img src="http://www.ceybank.com/images/logo-s.jpg"></div><div class="clearfix"></div>
    </section>-->
<div class="copyrights">
<section class="site-section">
<span class="floatleft">© 2012 Ceybank. All Rights Reserved</span><span class="floatright">Solution by <a href="http://www.efuturesworld.com">EFutures</a></span>
<div class="clearfix"></div>
</section>
</div>
</footer>
</body>
</html>