<!DOCTYPE HTML>
<html lang="pl">
<head>
<meta charset="utf-8"/>
<title>Sklep internetowy</title>
<meta content="domyślne, słowa, kluczowe" name="keywords"/>
<meta content="domyślny opis strony" name="description"/>
<meta content="index,follow" name="robots"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
<base href="https://dc-components.eu"/>
<link href="szablony/standardowy.rwd/css/style.php?ncss=style,boxy,moduly,podstrony" rel="stylesheet" type="text/css"/>
<script src="javascript/jquery.js"></script>
<script src="javascript/skrypty.php"></script>
</head>
<body style="background:#ffffff">
<header id="NaglowekSklepu">
<div id="GornaNawigacja">
<div class="Strona">
<div id="ZmianaJezyka">
<span class="Flaga" id="Jezyk1"><img alt="Polski" src="images/flagi/poland.png" title="Polski"/></span><span class="Flaga" id="Jezyk2"><img alt="Angielski" class="FlagaOff" src="images/flagi/england.png" title="Angielski"/></span>
</div>
<div id="Schowek">
<a href="https://dc-components.eu/schowek.html">Schowek (0)</a>
</div>
<div id="Logowanie">
<a href="https://dc-components.eu/logowanie.html" rel="nofollow">Zaloguj się</a>
<a href="https://dc-components.eu/rejestracja.html" rel="nofollow">Załóż konto</a>
</div>
</div>
</div>
</header>
<div id="Strona">
<a href="https://dc-components.eu" id="LinkLogo"><img alt="dc-components" src="images/naglowek-2.jpg" title="dc-components"/></a>
<div id="SzukanieKoszyk">
<form action="szukaj.html" id="Wyszukiwanie" method="post" onsubmit="return sprSzukaj(this,'InSzukaj')">
<div>
<input alt="Szukaj" id="ButSzukaj" src="szablony/standardowy.rwd/obrazki/szablon/szukaj.png" type="image"/>
<input id="InSzukaj" name="szukaj" size="30" type="text" value="Wpisz szukaną frazę ..."/>
<input name="postget" type="hidden" value="tak"/>
<input name="opis" type="hidden" value="tak"/>
<input name="nrkat" type="hidden" value="tak"/>
<input name="kodprod" type="hidden" value="tak"/>
</div>
<a href="https://dc-components.eu/wyszukiwanie-zaawansowane.html">wyszukiwanie zaawansowane</a>
<div class="cl"></div>
</form>
<div id="Koszyk">
<a href="https://dc-components.eu/koszyk.html">
<span><img alt="Koszyk" src="szablony/standardowy.rwd/obrazki/szablon/koszyk.png"/></span>
<span>
<strong>Koszyk</strong>
                
                                        
                    Twój koszyk jest pusty ...
                    
                                        
                </span>
</a>
</div>
<div class="cl"></div>
</div>
<div class="Rozwiniete" id="GorneMenu">
<ul><li aria-haspopup="true"><a href="https://dc-components.eu/www.blog.dc-components.eu" target="_blank">dc-components blog</a></li><li aria-haspopup="true"><a href="https://dc-components.eu/kontakt-f-1.html">Kontakt</a></li><li aria-haspopup="true"><a href="https://dc-components.eu/regulamin-pm-11.html">Regulamin</a></li></ul>
</div>
<div id="Nawigacja" itemscope="" itemtype="http://schema.org/BreadcrumbList">
<span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a class="NawigacjaLink" href="https://dc-components.eu" itemprop="item"><span itemprop="name">Strona główna</span></a><meta content="1" itemprop="position"/></span><span class="Nawigacja"> » </span><span class="Nawigacja">Błąd !! Nie odnaleziono szukanej strony ...</span>
</div>
<div class="cl"></div>
<div id="LewaKolumna" style="width:235px">
<div id="LewaKolumnaPrzerwa">
<div class="CalyBoxKategorie">
<div class="BoxNaglowekKategorie">Kategorie</div>
<div class="BoxKategorie BoxZawartosc">
<ul><li><a href="https://dc-components.eu/kondensatory-clarity-cap-c-1.html">Kondensatory Clarity Cap</a></li>
<li><a href="https://dc-components.eu/hifi-tuning-c-60.html">HIfi-Tuning<small>Bezpieczniki i akcesoria firmy hifi-tuning</small></a></li>
<li><a href="https://dc-components.eu/adaptery-przejsciowki-c-24.html">Adaptery / przejściówki </a></li>
<li><a href="https://dc-components.eu/akcesoria-c-38.html">Akcesoria</a></li>
<li><a href="https://dc-components.eu/akcesoria-do-gramofonow-c-15.html">Akcesoria do gramofonów</a></li>
<li><a href="https://dc-components.eu/akcesoria-do-kabli-zasilajacych-c-59.html">Akcesoria do kabli zasilajacych.</a></li>
<li><a href="https://dc-components.eu/cyna-i-spoiwo-lutownicze-c-20.html">Cyna i spoiwo lutownicze</a></li>
<li><a href="https://dc-components.eu/gniazda-glosnikowe-c-33.html">Gniazda głośnikowe.</a></li>
<li><a href="https://dc-components.eu/gniazda-i-wtyki-zasilajace-na-kabel-c-35.html">Gniazda i wtyki zasilające na kabel</a></li>
<li><a href="https://dc-components.eu/gniazda-montazowe-do-obudowy-schuko-c-49.html">Gniazda montażowe do obudowy - Schuko</a></li>
<li><a href="https://dc-components.eu/gniazda-rca-c-32.html">Gniazda RCA</a></li>
<li><a href="https://dc-components.eu/gniazda-xlr-c-27.html">Gniazda XLR</a></li>
<li><a href="https://dc-components.eu/gniazda-zasilajace-iec-do-obudowy-c-30.html">Gniazda zasilające iec do obudowy</a></li>
<li><a href="https://dc-components.eu/gniazdka-zasilajace-do-montazu-w-scianie-c-23.html">Gniazdka zasilające do montażu w ścianie</a></li>
<li><a href="https://dc-components.eu/instalacja-230v-c-50.html">Instalacja 230V</a></li>
<li><a href="https://dc-components.eu/kable-przewody-audio-c-10.html">Kable i przewody</a></li>
<li><a href="https://dc-components.eu/kolce-nozki-c-11.html">Kolce / Nóżki</a></li>
<li><a href="https://dc-components.eu/podzespoly-do-modyfikacji-c-16.html">Podzespoły do modyfikacji.</a></li>
<li><a href="https://dc-components.eu/potencjometry-i-osprzet-c-21.html">Potencjometry i osprzęt.</a></li>
<li><a href="https://dc-components.eu/przedwzmacniacze-gramofonowe-c-14.html">Przedwzmacniacze gramofonowe</a></li>
<li><a href="https://dc-components.eu/sluchawki-c-12.html">Słuchawki</a></li>
<li><a href="https://dc-components.eu/wtyki-bnc-c-22.html">Wtyki BNC</a></li>
<li><a href="https://dc-components.eu/wtyki-do-sluchawek-jack-c-31.html">Wtyki do słuchawek / Jack</a></li>
<li><a href="https://dc-components.eu/wtyki-do-wkladek-gramofonowych-c-13.html">Wtyki do wkładek gramofonowych</a></li>
<li><a href="https://dc-components.eu/wtyki-glosnikowe-bananowe-c-28.html">Wtyki głośnikowe bananowe</a></li>
<li><a href="https://dc-components.eu/wtyki-glosnikowe-widelkowe-c-29.html">Wtyki głośnikowe widełkowe</a></li>
<li><a href="https://dc-components.eu/wtyki-rca-c-34.html">Wtyki RCA</a></li>
<li><a href="https://dc-components.eu/wtyki-xlr-c-26.html">Wtyki XLR</a></li>
<li><a href="https://dc-components.eu/zworki-bi-wire-c-25.html">Zworki bi-wire</a></li>
<li><a href="https://dc-components.eu/kable-hdmi-c-52.html">Kable HDMI<small>Kable HDMI do systemów video i kina domowego.</small></a></li>
<li><a href="https://dc-components.eu/ex-demo-uzywane-c-51.html">EX-Demo - Używane</a></li>
<li><a href="https://dc-components.eu/odtwarzacze-sieciowe-streamery-c-58.html">Odtwarzacze sieciowe / Streamery</a></li>
</ul>
</div>
</div>
<div class="CalyBox" id="boxFiltry">
<div class="BoxNaglowek">Dodatkowe opcje przeglądania</div>
<div class="BoxTresc BoxZawartosc">
<form action="/brak-strony.html" class="cmxform" id="filtrBox" method="post">
<div id="filtryBox"></div>
</form><script>$(document).ready(function() { filtryBox() })</script>
</div>
</div>
<div class="CalyBox">
<div class="BoxNaglowek">Kontakt</div>
<div class="BoxTresc BoxZawartosc">
<ul class="BoxKontakt"><li class="Firma">dc-components - Jacek Pisarczyk<br/>
Spółdzielcza 52/20<br/>
57-300 Kłodzko<br/>
Polska<br/>NIP: 883-173-10-35<br/></li><li class="Iko Mail"><b>E-mail:</b><a href="https://dc-components.eu/kontakt-f-1.html">info@dc-components.eu</a></li><li class="Iko Tel"><b>Telefon</b>793-009-041<br/></li><li class="Iko Godziny"><b>Godziny działania sklepu</b>poniedziałek - piątek: 9.00 - 16.00</li></ul>
</div>
</div>
<div class="CalyBox">
<div class="BoxNaglowek">Cennik</div>
<div class="BoxTresc BoxZawartosc">
<div class="Cennik"><a href="https://dc-components.eu/cennik.html/typ=html"><img alt="Pobierz cennik HTML" src="szablony/standardowy.rwd/obrazki/cennik/html.png" title="Pobierz cennik HTML"/></a><a href="https://dc-components.eu/cennik.html/typ=xls"><img alt="Pobierz cennik XLS" src="szablony/standardowy.rwd/obrazki/cennik/xls.png" title="Pobierz cennik EXCEL"/></a></div>
</div>
</div><div class="BoxRwdUkryj">
<div class="CalyBox">
<div class="BoxNaglowek">Chmura tagów</div>
<div class="BoxTresc BoxZawartosc">
<div id="tagCloud"><script>wyswietlTagi("25");</script></div>
</div>
</div></div>
</div>
</div>
<div id="SrodekKolumna">
<h1 class="StrNaglowek">
<span>Brak danych do wyświetlenia</span>
</h1>
<section class="StrTresc">
<p class="Informacja">
        Błąd !! Nie odnaleziono szukanej strony ...
    </p>
<a class="przycisk Prawy" href="https://dc-components.eu/">Przejdź do strony głównej</a>
<div class="cl"></div>
</section>
</div>
<div class="cl"></div>
<footer id="Stopka">
<div class="KolumnaStopki">
<strong>
                Informacje
                <span class="StopkaRozwin"></span>
</strong>
<ul><li><a href="https://dc-components.eu/regulamin-pm-11.html">Regulamin</a></li><li><a href="https://dc-components.eu/polityka-prywatnosci-pm-17.html">Polityka prywatności</a></li><li><a href="https://dc-components.eu/informacja-o-cookie-pm-20.html">Informacja o cookie</a></li></ul>
</div>
<div class="KolumnaStopki">
<strong>
                Oferta dla Firm
                <span class="StopkaRozwin"></span>
</strong>
<ul><li><a href="https://dc-components.eu/oferta-dla-firm-pm-18.html">Oferta dla Firm</a></li></ul>
</div>
<div class="KolumnaStopki">
<strong>
                Dostawa i zwroty.
                <span class="StopkaRozwin"></span>
</strong>
<ul><li><a href="https://dc-components.eu/zasady-wysylki-pm-16.html">Zasady wysyłki</a></li><li><a href="https://dc-components.eu/zwroty-i-reklamacje-pm-22.html">Zwroty i reklamacje</a></li></ul>
</div>
<div class="KolumnaStopki">
<strong>
                Kontakt
                <span class="StopkaRozwin"></span>
</strong>
<ul><li><a href="https://dc-components.eu/kontakt-f-1.html">Kontakt</a></li></ul>
</div>
<div class="cl"></div>
</footer>
<div class="Copy">
<a href="https://www.shopgold.pl" target="_blank">Oprogramowanie sklepu shopGold.pl</a>
</div>
</div>
<script type="text/javascript">var infoCookieTekst = "Korzystanie z tej witryny oznacza wyrażenie zgody na wykorzystanie plików cookies. Więcej informacji możesz znaleźć w naszej Polityce Cookies.";var infoCookieAkcept = "Nie pokazuj więcej tego komunikatu";$.InfoCookie();</script>
<script> $(window).load(function() { $.ZaladujObrazki(false); }); </script>
<div id="RwdWersja"></div>
</body>
</html>
