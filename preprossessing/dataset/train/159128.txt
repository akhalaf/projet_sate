<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Home</title>
<meta content="" name="description"/>
<meta content="" name="keywords"/><meta content="width=device-width, initial-scale=1.0" name="viewport"/><meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/><meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="css/main.css" rel="stylesheet"/>
<link href="css/bootstrap.css" rel="stylesheet"/>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/bootstrap-select.js" type="text/javascript"></script>
<link href="css/bootstrap-select.css" rel="stylesheet" type="text/css"/>
</head><body>
<div class="row top-container">
<div class="container">
<div class="rfloat">
<a class="top-link r-margin-10" href="index.php?mod=registration">Register</a>
<a class="top-link r-margin-30" href="javascript:ShowLogin()">Sign in</a>
</div>
<div class="clearfix"></div>
<img class="site-logo" src="logo.jpg" style="position:relative;top:-35px"/>
</div>
</div>
<div class="row nav-container">
<div class="container">
<div class="container">
<div class="col-md-10 main-search-form">
<form action="index.php" id="search-form" method="post">
<input name="mod" type="hidden" value="search"/>
<input name="proceed_search" type="hidden" value="1"/>
<div class="row">
<div class="col-md-5 col-sm-10 search-text-column">
<input class="search-form-field" name="search_by" placeholder="Search" type="text" value=""/>
</div>
<div class="col-md-3 search-dropdown-column search-dropdown-column">
<select class="selectpicker" data-live-search="true" name="search_category" title="Category">
<option style="display:none" value="">Category</option>
<option value="1">Announcements</option><option value="2">Appliances</option><option value="3">Arts and Crafts</option><option value="4">Audio and Video Electronics</option><option value="5">Books</option><option value="6">Business Opportunities</option><option value="7">Business Services</option><option value="8">Cameras and Optics</option><option value="9">Clothing and Accessories</option><option value="10">Collectibles</option><option value="11">Computers and Software</option><option value="12">Employment</option><option value="13">Furniture</option><option value="14">Health and Beauty</option><option value="15">Home and Offices</option><option value="16">Music and CDs</option><option value="17">Musical Instruments</option><option value="18">Personals</option><option value="19">Pets and Supplies</option><option value="20">Sports and Games</option><option value="21">Tickets</option><option value="22">Travel</option><option value="23">Tuition</option><option value="24">Vehicles</option><option value="25">Various</option>
</select>
</div>
<div class="col-md-3 search-dropdown-column search-dropdown-column">
<select class="selectpicker" data-live-search="true" name="search_location" title="Location">
<option style="display:none" value="">Location</option>
<option value="1">Afghanistan</option>
<option value="2">Armenia</option>
<option value="3">Azerbaijan</option>
<option value="4">Bahrain</option>
<option value="5">Bangladesh</option>
<option value="6">Bhutan</option>
<option value="7">Brunei</option>
<option value="8">Cambodia</option>
<option value="9">China</option>
<option value="10">Cyprus</option>
<option value="11">Georgia</option>
<option value="12">India</option>
<option value="13">Indonesia</option>
<option value="14">Iran</option>
<option value="15">Iraq</option>
<option value="16">Israel</option>
<option value="17">Japan</option>
<option value="18">Jordan</option>
<option value="19">Kazakhstan</option>
<option value="20">Kuwait</option>
<option value="21">Kyrgyzstan</option>
<option value="22">Laos</option>
<option value="23">Lebanon</option>
<option value="24">Malaysia</option>
<option value="25">Maldives</option>
<option value="26">Mongolia</option>
<option value="27">Myanmar</option>
<option value="28">Nepal</option>
<option value="29">North Korea</option>
<option value="30">Oman</option>
<option value="31">Pakistan</option>
<option value="32">Philippines</option>
<option value="33">Qatar</option>
<option value="34">Saudi Arabia</option>
<option value="35">Singapore</option>
<option value="36">South Korea</option>
<option value="37">Sri Lanka</option>
<option value="38">State of Palestine</option>
<option value="39">Syria</option>
<option value="40">Tajikistan</option>
<option value="41">Thailand</option>
<option value="42">Timor-Leste</option>
<option value="43">Turkey</option>
<option value="44">Turkmenistan</option>
<option value="45">United Arab Emirates</option>
<option value="46">Uzbekistan</option>
<option value="47">Vietnam</option>
<option value="48">Yemen</option>
</select>
</div>
<div class="col-md-1 col-sm-2 search-button-column">
<button class="btn btn-lg btn-warning search-icon-button" type="submit"><input alt="search icon" border="0" class="search-img-back" src="images/search_bg_button.png" type="image"/></button>
</div>
</div>
</form>
</div>
<div class="col-md-2">
<a class="btn btn-lg btn-warning search-form-button" href="index.php?mod=new_listing" id="add-listing-button">Post a New Ad</a>
</div>
</div>
</div>
<div class="clear"></div>
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="top-links collapse navbar-collapse">
<ul class="nav navbar-nav">
<li><a href="http://www.asianclassified.com">Home</a></li>
<li><a href="en_Latest+Ads.html">Latest Ads</a></li>
<li><a href="en_Email+Alerts.html">Email Alerts</a></li>
<li><a href="en_News.html">News</a></li>
<li><a href="en_Contact.html">Contact</a></li>
</ul>
</div>
</div>
</div>
<div class="container">
<div class="margin-top-5">
<div class="text-left col-lg-8">
</div>
<div class="text-right col-lg-4">
<button class="btn btn-xs btn-default btn-gradient" data-target=".cat-collapse" data-toggle="collapse" type="button">
				Browse by Location 
			</button>
</div>
</div>
<div class="clear"></div>
<div class="clear margin-top-10"></div>
<div class="container">
<div class="collapse cat-collapse text-left">
<div class="container">
<hr class="clear"/>
<br/>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-afghanistan-1.html">Afghanistan</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-armenia-2.html">Armenia</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-azerbaijan-3.html">Azerbaijan</a></span>
</div>
<div class="clear"></div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-bahrain-4.html">Bahrain</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-bangladesh-5.html">Bangladesh</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-bhutan-6.html">Bhutan</a></span>
</div>
<div class="clear"></div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-brunei-7.html">Brunei</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-cambodia-8.html">Cambodia</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-china-9.html">China</a></span>
</div>
<div class="clear"></div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-cyprus-10.html">Cyprus</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-georgia-11.html">Georgia</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-india-12.html">India</a></span>
</div>
<div class="clear"></div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-indonesia-13.html">Indonesia</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-iran-14.html">Iran</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-iraq-15.html">Iraq</a></span>
</div>
<div class="clear"></div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-israel-16.html">Israel</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-japan-17.html">Japan</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-jordan-18.html">Jordan</a></span>
</div>
<div class="clear"></div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-kazakhstan-19.html">Kazakhstan</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-kuwait-20.html">Kuwait</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-kyrgyzstan-21.html">Kyrgyzstan</a></span>
</div>
<div class="clear"></div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-laos-22.html">Laos</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-lebanon-23.html">Lebanon</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-malaysia-24.html">Malaysia</a></span>
</div>
<div class="clear"></div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-maldives-25.html">Maldives</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-mongolia-26.html">Mongolia</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-myanmar-27.html">Myanmar</a></span>
</div>
<div class="clear"></div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-nepal-28.html">Nepal</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-north-korea-29.html">North Korea</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-oman-30.html">Oman</a></span>
</div>
<div class="clear"></div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-pakistan-31.html">Pakistan</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-philippines-32.html">Philippines</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-qatar-33.html">Qatar</a></span>
</div>
<div class="clear"></div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-saudi-arabia-34.html">Saudi Arabia</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-singapore-35.html">Singapore</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-south-korea-36.html">South Korea</a></span>
</div>
<div class="clear"></div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-sri-lanka-37.html">Sri Lanka</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-state-of-palestine-38.html">State of Palestine</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-syria-39.html">Syria</a></span>
</div>
<div class="clear"></div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-tajikistan-40.html">Tajikistan</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-thailand-41.html">Thailand</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-timorleste-42.html">Timor-Leste</a></span>
</div>
<div class="clear"></div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-turkey-43.html">Turkey</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-turkmenistan-44.html">Turkmenistan</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-united-arab-emirates-45.html">United Arab Emirates</a></span>
</div>
<div class="clear"></div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-uzbekistan-46.html">Uzbekistan</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-vietnam-47.html">Vietnam</a></span>
</div>
<div class="col-md-4 no-left-padding margin-bottom-10">
<span class="category_link">
<a class="category_link" href="location-yemen-48.html">Yemen</a></span>
</div>
<div class="clear"></div><div class="clear"></div>
<hr/>
</div>
</div>
<wsa carousel=""></wsa>
</div>
<div class="text-left col-lg-8">
<h3>Featured Ads</h3>
<div class="carousel slide" id="myCarousel">
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#myCarousel"><img class="img-shadow indicator-image" src="thumbnails/20649637.jpg"/></li><li data-slide-to="1" data-target="#myCarousel"><img class="img-shadow indicator-image" src="thumbnails/27076902.jpg"/></li> </ol>
<div class="carousel-inner">
<div class="item active slide-back">
<div class="container">
<div class="xcarousel-caption">
<a class="carousel-link" href="ad-free-eos-rewards-1853.html">
<img align="right" alt="Free $50 EOS Rewards From Coinbase (Invitation Link Only)" class="img-shadow" height="160" src="uploaded_images/20649637.jpg" style="margin-left:15px"/><h2 class="no-top-margin hide-xs">Free $50 EOS Rewards From Coinbase (Invitation Lin...</h2>
<p class="hide-sm">Here is an invitation link to earn free EOS cryptocurrency at coinbase.com:https://coinbase.com/earn/eos/invite/znrwg1qp</p>
</a>
<br/>
</div>
</div>
</div><div class="item slide-back">
<div class="container">
<div class="xcarousel-caption">
<a class="carousel-link" href="ad-get-your-crypto-1854.html">
<img align="right" alt="Get Your Crypto Address Before It Gone!" class="img-shadow" height="160" src="uploaded_images/27076902.jpg" style="margin-left:15px"/><h2 class="no-top-margin hide-xs">Get Your Crypto Address Before It Gone!</h2>
<p class="hide-sm">Get your crypto currency domain address before it is registered by someone. There is no yearly fees. https://bit.ly/3fUzRxu</p>
</a>
<br/>
</div>
</div>
</div> </div>
<a class="left carousel-control" data-slide="prev" href="#myCarousel"><img class="carousel-icon" src="images/carousel-arrow-left.png"/></a>
<a class="right carousel-control" data-slide="next" href="#myCarousel"><img class="carousel-icon" src="images/carousel-arrow-right.png"/></a>
</div>
<br/>
<script type="text/javascript">

if(document.getElementById("myCarousel"))
{


  $(document).ready(function() {
	$('.carousel').carousel({
	  interval: 4000
	})
  });
}
    </script>
<h3>Browse the Classified Ads by Category</h3>
<br/>
<div class="clear"></div><br/>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-announcements-1.html" title="Announcements">Announcements</a> <span class="sub_category_link">(1)</span></span><span class="sub_category_link">Events</span><span class="sub_category_link">, Legal notices</span>
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-appliances-2.html" title="Appliances">Appliances</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Air Conditioning or Heating</span><span class="sub_category_link">, Cooking or Ovens</span><span class="sub_category_link">, Dishwashers</span><span class="sub_category_link">, Lighting or Electricals</span><span class="sub_category_link">, Other Announcements</span><span class="sub_category_link">, Other Appliances</span><span class="sub_category_link">, Phones</span><span class="sub_category_link">, Refrigerators or Freezers</span>...
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-arts-and-crafts-3.html" title="Arts and Crafts">Arts and Crafts</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Ceramics or Clay</span><span class="sub_category_link">, Glass</span><span class="sub_category_link">, Metal</span><span class="sub_category_link">, Needlework or Textiles</span><span class="sub_category_link">, Other Announcements</span><span class="sub_category_link">, Other Arts</span><span class="sub_category_link">, Other Crafts</span><span class="sub_category_link">, Paintings</span>...
</div>
<div class="clear"></div><br/>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-audio-and-video-4.html" title="Audio and Video Electronics">Audio and Video Electronics</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Amplifiers</span><span class="sub_category_link">, </span><span class="sub_category_link">, Camcorders or Cameras</span><span class="sub_category_link">, Cassette Players</span><span class="sub_category_link">, CD or MD or MP3 Players</span><span class="sub_category_link">, Decoders</span><span class="sub_category_link">, DSS or Satellite</span><span class="sub_category_link">, DVD or LD or VCD Players</span>...
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-books-5.html" title="Books">Books</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Arts or Entertainment</span><span class="sub_category_link">, Biographies</span><span class="sub_category_link">, Business or Finance</span><span class="sub_category_link">, Children or Young Adults</span><span class="sub_category_link">, College Textbooks</span><span class="sub_category_link">, Comics or Humor</span><span class="sub_category_link">, Computers or Technology</span><span class="sub_category_link">, Fiction or Literature</span>...
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-business-opportunities-6.html" title="Business Opportunities">Business Opportunities</a> <span class="sub_category_link">(1)</span></span><span class="sub_category_link">Brokerage</span><span class="sub_category_link">, Distributors</span><span class="sub_category_link">, Franchising</span><span class="sub_category_link">, Home-based</span><span class="sub_category_link">, Import or Export</span><span class="sub_category_link">, Internet</span><span class="sub_category_link">, Investors</span><span class="sub_category_link">, Networking M-L-M</span>...
</div>
<div class="clear"></div><br/>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-business-services-7.html" title="Business Services">Business Services</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Administrative</span><span class="sub_category_link">, Advertising</span><span class="sub_category_link">, Architecture</span><span class="sub_category_link">, Arts or Entertainment</span><span class="sub_category_link">, Consulting</span><span class="sub_category_link">, Education</span><span class="sub_category_link">, Finance or Accounting</span><span class="sub_category_link">, Health Care</span>...
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-cameras-and-optics-8.html" title="Cameras and Optics">Cameras and Optics</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">35mm SLR</span><span class="sub_category_link">, Binoculars</span><span class="sub_category_link">, Compact or Point and Shoot</span><span class="sub_category_link">, Digital or Camcorders</span><span class="sub_category_link">, Lenses or Filters</span><span class="sub_category_link">, Lighting Equipments</span><span class="sub_category_link">, Other Announcements</span><span class="sub_category_link">, Other Cameras</span>...
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-clothing-and-accessories-9.html" title="Clothing and Accessories">Clothing and Accessories</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Casual Wear</span><span class="sub_category_link">, Formal Wear</span><span class="sub_category_link">, Jewelry</span><span class="sub_category_link">, Other Accessories</span><span class="sub_category_link">, Other Announcements</span><span class="sub_category_link">, Other Clothing</span><span class="sub_category_link">, Shoes</span><span class="sub_category_link">, Sleepwear or Underwear</span>...
</div>
<div class="clear"></div><br/>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-collectibles-10.html" title="Collectibles">Collectibles</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Antiques</span><span class="sub_category_link">, Arts or Crafts</span><span class="sub_category_link">, Books</span><span class="sub_category_link">, Coins or Currency</span><span class="sub_category_link">, Comics</span><span class="sub_category_link">, Dolls</span><span class="sub_category_link">, Jewelry or Watches</span><span class="sub_category_link">, Other Announcements</span>...
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-computers-and-software-11.html" title="Computers and Software">Computers and Software</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Desktops</span><span class="sub_category_link">, Hardware and Accessories</span><span class="sub_category_link">, Internet</span><span class="sub_category_link">, Internet Appliances</span><span class="sub_category_link">, Monitors</span><span class="sub_category_link">, Networking or Servers</span><span class="sub_category_link">, Notebooks or Laptops</span><span class="sub_category_link">, Other Announcements</span>...
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-employment-12.html" title="Employment">Employment</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Administrative</span><span class="sub_category_link">, Advertising</span><span class="sub_category_link">, Architecture</span><span class="sub_category_link">, Arts or Entertainment</span><span class="sub_category_link">, Consulting</span><span class="sub_category_link">, Education</span><span class="sub_category_link">, Finance or Accounting</span><span class="sub_category_link">, Health Care</span>...
</div>
<div class="clear"></div><br/>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-furniture-13.html" title="Furniture">Furniture</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Bath Room</span><span class="sub_category_link">, Bedroom</span><span class="sub_category_link">, Dining Room</span><span class="sub_category_link">, Family or Living Room</span><span class="sub_category_link">, Kids Room</span><span class="sub_category_link">, Kitchen</span><span class="sub_category_link">, Office</span><span class="sub_category_link">, Other Announcements</span>...
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-health-and-beauty-14.html" title="Health and Beauty">Health and Beauty</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Aromatherapy</span><span class="sub_category_link">, Birth Control or Family Planning</span><span class="sub_category_link">, Cosmetics</span><span class="sub_category_link">, Exercise Equipment</span><span class="sub_category_link">, Eye Care</span><span class="sub_category_link">, Hair Care</span><span class="sub_category_link">, Massage or Spa</span><span class="sub_category_link">, Nail Care</span>...
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-home-and-offices-15.html" title="Home and Offices">Home and Offices</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Apartments or Condos</span><span class="sub_category_link">, Financing or Insurance</span><span class="sub_category_link">, Houses</span><span class="sub_category_link">, Land and Farm</span><span class="sub_category_link">, Offices or Commercials</span><span class="sub_category_link">, Other Announcements</span><span class="sub_category_link">, Other Homes and Offices</span><span class="sub_category_link">, Repair or Renovation or Service</span>...
</div>
<div class="clear"></div><br/>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-music-and-cds-16.html" title="Music and CDs">Music and CDs</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Blues</span><span class="sub_category_link">, Children</span><span class="sub_category_link">, Classical</span><span class="sub_category_link">, Country</span><span class="sub_category_link">, Easy Listening</span><span class="sub_category_link">, Electronic</span><span class="sub_category_link">, Folk</span><span class="sub_category_link">, Hardcore or Punk</span>...
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-musical-instruments-17.html" title="Musical Instruments">Musical Instruments</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Brass</span><span class="sub_category_link">, Keyboards</span><span class="sub_category_link">, Other Announcements</span><span class="sub_category_link">, Other Musical Instruments</span><span class="sub_category_link">, Percussion</span><span class="sub_category_link">, Strings</span><span class="sub_category_link">, Woodwinds</span>
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-personals-18.html" title="Personals">Personals</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Activity Partners</span><span class="sub_category_link">, Dating</span><span class="sub_category_link">, Matrimonial</span><span class="sub_category_link">, Missed Connections</span><span class="sub_category_link">, Other Announcements</span><span class="sub_category_link">, Other Personals</span><span class="sub_category_link">, Pen Pal</span>
</div>
<div class="clear"></div><br/>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-pets-and-supplies-19.html" title="Pets and Supplies">Pets and Supplies</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Birds</span><span class="sub_category_link">, Cats and Dogs</span><span class="sub_category_link">, Fish</span><span class="sub_category_link">, Other Announcements</span><span class="sub_category_link">, Other Pets</span><span class="sub_category_link">, Reptiles</span><span class="sub_category_link">, Supplies</span>
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-sports-and-games-20.html" title="Sports and Games">Sports and Games</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Auto Racing</span><span class="sub_category_link">, Board Games</span><span class="sub_category_link">, Bowling</span><span class="sub_category_link">, Boxing</span><span class="sub_category_link">, Card Games</span><span class="sub_category_link">, Cycling</span><span class="sub_category_link">, Fishing</span><span class="sub_category_link">, Football</span>...
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-tickets-21.html" title="Tickets">Tickets</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Concerts</span><span class="sub_category_link">, Other Announcements</span><span class="sub_category_link">, Other Tickets</span><span class="sub_category_link">, Sports</span><span class="sub_category_link">, Theater</span>
</div>
<div class="clear"></div><br/>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-travel-22.html" title="Travel">Travel</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Attractions</span><span class="sub_category_link">, Hotel &amp; Lodging Places</span><span class="sub_category_link">, Other Announcements</span><span class="sub_category_link">, Other Travel</span><span class="sub_category_link">, Packaged Tours</span><span class="sub_category_link">, Restaurants</span><span class="sub_category_link">, Services or Insurance</span>
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-tuition-23.html" title="Tuition">Tuition</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Accounting</span><span class="sub_category_link">, Arts or Music</span><span class="sub_category_link">, Biology</span><span class="sub_category_link">, Chemistry</span><span class="sub_category_link">, Computer Science</span><span class="sub_category_link">, Economics</span><span class="sub_category_link">, Geography</span><span class="sub_category_link">, History</span>...
</div>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-vehicles-24.html" title="Vehicles">Vehicles</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Buses &amp; Trucks</span><span class="sub_category_link">, Cars</span><span class="sub_category_link">, Financing or Insurance</span><span class="sub_category_link">, Jeeps or 4WDs</span><span class="sub_category_link">, Motorcycles</span><span class="sub_category_link">, Other Announcements</span><span class="sub_category_link">, Other Vehicles</span><span class="sub_category_link">, Parts or Accessories</span>...
</div>
<div class="clear"></div><br/>
<div class="col-md-4 no-left-padding">
<span class="category_link"><a href="category-various-25.html" title="Various">Various</a> <span class="sub_category_link">(0)</span></span><span class="sub_category_link">Boats</span><span class="sub_category_link">, Merchandise</span><span class="sub_category_link">, Other Announcements</span><span class="sub_category_link">, Other items</span><span class="sub_category_link">, Others</span><span class="sub_category_link">, Shopping</span></div><div class="clear"></div>
</div>
<div class="text-left col-lg-4">
<wsa banners_1=""></wsa>
<h3>Latest Ads</h3>
<hr/>
<a href="ad-get-your-crypto-1854.html"><img align="left" alt="Get Your Crypto Address Before It Gone!" class="img-shadow img-right-margin" src="thumbnails/27076902.jpg" width="50"/></a>
<h4 class="no-top-margin"><a href="ad-get-your-crypto-1854.html">
		Get Your Crypto Address Before It Gone!	</a></h4>
<span class="sub-text">
	Get your crypto currency domain address before it is registered by someone. There ...	</span>
<hr/>
<a href="ad-free-eos-rewards-1853.html"><img align="left" alt="Free $50 EOS Rewards From Coinbase (Invitation Link Only)" class="img-shadow img-right-margin" src="thumbnails/20649637.jpg" width="50"/></a>
<h4 class="no-top-margin"><a href="ad-free-eos-rewards-1853.html">
		Free $50 EOS Rewards From Coinbase (Invitation Link Only)	</a></h4>
<span class="sub-text">
	Here is an invitation link to earn free EOS cryptocurrency at coinbase.com:https://coinbase.com/earn/eos/invite/znrwg1qp	</span>
<hr/>
<h4 class="no-top-margin"><a href="ad-domain-for-sale-563.html">
		Domain For Sale - HalalSpice.com	</a></h4>
<span class="sub-text">
	HalalSpice.com for sale. A great domain for individual or business owner who does ...	</span>
<hr/>
<h4 class="no-top-margin"><a href="ad-adorable-pug-pups-540.html">
		Adorable Pug Pups	</a></h4>
<span class="sub-text">
	After 60 years of breeding purebred dogs we offerTop Quality, satisfaction guaranteed.We have ...	</span>
<hr/>
<a href="ad-earn-free-crypto-255.html"><img align="left" alt="Earn FREE crypto with Coinbase Earn" class="img-shadow img-right-margin" src="thumbnails/94726525.jpg" width="50"/></a>
<h4 class="no-top-margin"><a href="ad-earn-free-crypto-255.html">
		Earn FREE crypto with Coinbase Earn	</a></h4>
<span class="sub-text">
	You can easily earn some crypto currencies with coinbase by learning about it. ...	</span>
<hr/>
<br/>
<h3>Latest News</h3>
<div class="list-group add-border">
</div>
<br/><br/>
</div>
<div class="clear"></div>
<br/><br/>
</div>
<div class="row bottom-container">
<div class="container">
<br/>
<ul class="bottom-menu">
<li>
<a href="http://www.asianclassified.com">Home</a>
</li>
<li>
<a href="en_Latest+Ads.html">Latest Ads</a>
</li>
<li>
<a href="en_Email+Alerts.html">Email Alerts</a>
</li>
<li>
<a href="en_Post+a+New+Ad.html">Post a New Ad</a>
</li>
<li>
<a href="en_Modify+Your+Listing.html">Modify Your Listing</a>
</li>
<li>
<a href="en_Browse+by+Location.html">Browse by Location</a>
</li>
<li>
<a href="en_News.html">News</a>
</li>
<li>
<a href="en_Contact.html">Contact</a>
</li>
<li>
<a href="en_About+us.html">About us</a>
</li>
<li>
<a href="en_Terms+and+Conditions.html">Terms and Conditions</a>
</li>
</ul>
<hr/>
<span class="small-font">Copyright © 2020 Asianclassified.com. All Rights Reserved</span>
</div>
</div>
<script src="js/login.js"></script>
<div id="main-login-form">
<a href="javascript:HideLogin()"><img alt="close" class="close-login-icon" src="images/closeicon.png"/></a>
<h3 class="lfloat" id="top_msg_header">
	Login	</h3>
<hr class="clear"/>
<script>
			function ValidateLoginForm(x)
			{
				if(x.Email.value=="")
				{
					document.getElementById("top_msg_header").innerHTML=
					"Please enter your username!";
					x.Email.focus();
					return false;
				}
				else
				if(x.Password.value=="")
				{
				
					document.getElementById("top_msg_header").innerHTML=
					"Please enter your password!";
					x.Password.focus();
					return false;
				}
				return true;
			}
			</script>
<form action="USERS/loginaction.php" class="no-margin" method="post" onsubmit="return ValidateLoginForm(this)">
<table class="bcollapse">
<tr>
<td>Username:</td>
<td><input class="login-form-field" name="Email" size="40" type="text"/></td>
</tr>
<tr>
<td>Password:</td>
<td><input class="login-form-field" name="Password" size="40" type="password"/></td>
</tr>
<tr>
<td colspan="2">
<br/>
<a class="lfloat margin-20" href="mod-forgotten_password.html">forgotten password?</a>
<input class="ibutton rfloat margin-20" type="submit" value="Login"/></td>
</tr>
</table>
</form>
<br/>
<br/>
</div>
<script src="js/bootstrap.js"></script>
<script type="text/javascript">
        $(window).on('load', function () {

            $('.selectpicker').selectpicker({
                'selectedText': 'cat'
            });

        });
    </script>
</body>
</html>
