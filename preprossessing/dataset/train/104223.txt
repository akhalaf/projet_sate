<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="text/css" http-equiv="content-style-type"/>
<meta content="ÂçÄÍ¾¦²ñ¤Î¡Ö¥¢¥ë¥Õ¥¡¥ª¥Õ¥£¥¹¡×¤Î²ñ°÷¸þ¤±¥µ¥¤¥È¤Ç¤¹¡£¤´ÍøÍÑÃæ¤Î¤ªµÒÍÍ¤Ï¤³¤Á¤é¤«¤é¥í¥°¥¤¥ó¤Ç¤­¤Þ¤¹¡£¤Þ¤¿¡¢¥µ¡¼¥Ó¥¹¤Ë´Ø¤¹¤ëºÇ¿·¾ðÊó¤ä¤´ÍøÍÑ¤Î¼ê°ú¤­¡¢¤è¤¯¤¢¤ë¤´¼ÁÌä¤Ê¤É¤â·ÇºÜ¤·¤Æ¤¤¤Þ¤¹¡£" lang="ja" name="description"/>
<title>¥¢¥ë¥Õ¥¡¥ª¥Õ¥£¥¹²ñ°÷¥µ¥¤¥È</title>
<link href="/new/service/atom.xml" rel="alternate" title="¥¢¥ë¥Õ¥¡¥ª¥Õ¥£¥¹¡§¥µ¡¼¥Ó¥¹´ØÏ¢¾ðÊó¡ÊRSS¡Ë" type="application/rss+xml"/>
<link href="/new/mente/atom.xml" rel="alternate" title="¥¢¥ë¥Õ¥¡¥ª¥Õ¥£¥¹¡§¥á¥ó¥Æ¥Ê¥ó¥¹¾ðÊó¡ÊRSS¡Ë" type="application/rss+xml"/>
<link href="/new/trouble/atom.xml" rel="alternate" title="¥¢¥ë¥Õ¥¡¥ª¥Õ¥£¥¹¡§¾ã³²¾ðÊó¡ÊRSS¡Ë" type="application/rss+xml"/>
<link href="/stylesheet/import.css" rel="stylesheet" type="text/css"/>
<link href="/stylesheet/size01.css" rel="stylesheet" title="size01" type="text/css"/>
<link href="/stylesheet/size02.css" rel="alternate stylesheet" title="size02" type="text/css"/>
<link href="/stylesheet/size03.css" rel="alternate stylesheet" title="size03" type="text/css"/>
<link href="/stylesheet/print.css" media="print" rel="stylesheet" type="text/css"/>
<script src="/js/template.js" type="text/javascript"></script>
</head>
<body id="index">
<div id="header">
<div id="ttl">
<h1><a href="https://www.otsuka-shokai.co.jp/" rel="external"><img alt="³ô¼°²ñ¼ÒÂçÄÍ¾¦²ñ" height="15" src="/img/logo.gif" width="149"/></a></h1>
<h2><a href="/"><img alt="¥¢¥ë¥Õ¥¡¥ª¥Õ¥£¥¹²ñ°÷¥µ¥¤¥È" height="18" src="/img/ttl_site.gif" width="298"/></a></h2>
</div><!-- ttl -->
<ul id="supplementation_navi">
<li><a href="/yakkan.html"><img alt="¥µ¡¼¥Ó¥¹Ìó´¾" height="11" src="/img/btn_stipulation_o.gif" width="66"/></a></li>
<li><a href="/sitemap.html"><img alt="¥µ¥¤¥È¥Þ¥Ã¥×" height="11" src="/img/btn_sitemap_o.gif" width="62"/></a></li>
</ul>
<dl id="retrieval">
<dt><img alt="¥µ¥¤¥ÈÆâ¸¡º÷" height="13" src="/img/ttl_retrieval.gif" width="74"/></dt>
<dd>
<!--¸¡º÷³«»Ï-->
<form action="https://bizsearch-asp.accelatech.com/bizasp/index.php" method="get" name="AccelaBizSearchASPForm">
<input id="text_box" name="q" type="text" value=""/>
<input id="retrieval_btn" type="submit" value=""/>
<input name="corpId" type="hidden" value="atc090012"/>
<input name="en" type="hidden" value="0"/>
<input name="layout" type="hidden" value="2"/>
</form>
<!--¸¡º÷½ªÎ»-->
</dd>
</dl>
<dl id="size">
<dt><img alt="Ê¸»ú¤ÎÂç¤­¤µÊÑ¹¹" height="13" src="/img/ttl_size.gif" width="97"/></dt>
<dd>
<ul>
<li><a href="#size01" id="size01" rel="style"><img alt="¾®" height="21" src="/img/btn_size01_o.gif" width="31"/></a></li>
<li><a href="#size02" id="size02" rel="style"><img alt="Âç" height="21" src="/img/btn_size02_o.gif" width="31"/></a></li>
<li><a href="#size03" id="size03" rel="style"><img alt="ÆÃÂç" height="21" src="/img/btn_size03_o.gif" width="31"/></a></li>
</ul>
</dd>
</dl>
<ul id="gn">
<li><a href="/index.html"><img alt="HOME" height="39" src="/img/gn01_h.gif" width="105"/></a></li>
<li><a href="/login/"><img alt="¥í¥°¥¤¥ó" height="39" src="/img/gn02_o.gif" width="170"/></a></li>
<li><a href="/new/index.htm"><img alt="ºÇ¿·¾ðÊó" height="39" src="/img/gn03_o.gif" width="170"/></a></li>
<li><a href="/support/index.html"><img alt="¤´ÍøÍÑ¤Î¼ê°ú¤­" height="39" src="/img/gn04_o.gif" width="170"/></a></li>
<li><a href="/faq/index.html"><img alt="¤è¤¯¤¢¤ë¤´¼ÁÌä" height="39" src="/img/gn05_o.gif" width="170"/></a></li>
<li><a href="/inquiry.html"><img alt="¤ªÌä¤¤¹ç¤ï¤»" height="39" src="/img/gn06_o.gif" width="170"/></a></li>
</ul>
</div><!-- header -->
<div id="contents">
<div id="sideContents">
<div id="logIn">
<h3><img alt="²ñ°÷¤ÎÊý¤Ï¤³¤Á¤é" height="18" src="/img/ttl_logIn.gif" width="185"/></h3>
<p><a href="/login/"><img alt="¥í¥°¥¤¥ó" height="46" src="/img/btn_logIn_o.gif" width="185"/></a></p>
<p class="lastItem">³Æ¼ïÀßÄê¤äµ¡Ç½¤Î¤´ÍøÍÑ¤µ¤ì¤ë¾ì¹ç¤Ï¡¢¤³¤Á¤é¤«¤é¥í¥°¥¤¥ó¤·¤Æ¤¯¤À¤µ¤¤¡£</p>
</div><!-- logIn -->
<div id="idLogIn">
<div class="idLogin_inner01">
<p><a href="https://oapp.alpha-mail.ne.jp/r/start_menu" target="_blank"><img alt="ÂçÄÍID¤Ç¥í¥°¥¤¥ó" src="/img/btn_id_logIn_01_o.gif"/></a></p>
<p class="inquiry"><a class="inquiryLink" href="https://mypage.otsuka-shokai.co.jp/caifaq-oid" target="_blank">ÂçÄÍID¤Ë´Ø¤¹¤ë¤ªÌä¤¤¹ç¤ï¤»</a></p>
</div><!-- idLogin_inner01 -->
<div class="outerLine">
<div class="innerLine">
<div class="idLogin_inner02">
<p><a href="https://mypage.otsuka-shokai.co.jp/account/temp-registration/input" target="_blank"><img alt="¿·µ¬ÅÐÏ¿¡ÊÌµÎÁ¡Ë" src="/img/btn_id_logIn_02_o.gif"/></a></p>
<p><a href="https://mypage.otsuka-shokai.co.jp/" target="_blank"><img alt="¤ªµÒÍÍ¥Þ¥¤¥Ú¡¼¥¸" src="/img/btn_id_logIn_03_o.gif"/></a></p>
</div><!-- idLogin_inner02 -->
</div><!-- innerLine -->
</div><!-- outerLine -->
</div><!-- idLogIn -->
<p class="out_banner">
<a href="https://webdirect.tanomail.com/office/?i4c=52&amp;i4a=774354" rel="external"><img alt="¥¢¥ë¥Õ¥¡¥ª¥Õ¥£¥¹¤ª¿½¤·¹þ¤ß¤Ï¤³¤Á¤é" height="49" src="/img/btn_office_sales_o.gif" width="215"/></a>
</p>
<p class="out_banner" style="margin-bottom: 10px;"><a href="/s/" rel="external"><img alt="¥¹¥Þ¡¼¥È¥Õ¥©¥ó¤Ç¤Î¥í¥°¥¤¥ó¤Ï¤³¤Á¤é" height="46" src="/img/s_banner_o.gif" width="215"/></a></p>
<!--p id="procedure"><a href="http://www.alpha-web.jp/service/asp/office/sfa.htm" rel="external"><img src="/img/btn_sfa.gif" width="215" height="65" alt="±Ä¶È»Ù±ç(SFA)¥ª¥×¥·¥ç¥ó¾ÜºÙ¤Ï¤³¤Á¤é" /></a>
</p-->
<div class="border" id="faq">
<h3><a href="/faq/index.html"><img alt="¤è¤¯¤¢¤ë¤´¼ÁÌä" height="25" src="/img/ttl_faq_o.gif" width="215"/></a></h3>
<ul class="arrow01">
<li><a href="/faq/general/">¥µ¡¼¥Ó¥¹Á´ÈÌ</a></li>
<li><a href="/faq/admin/">´ÉÍý¼Ôµ¡Ç½</a></li>
<li><a href="/faq/user/">ÍøÍÑ¼Ôµ¡Ç½</a></li>
<li><a href="/faq/list.html">¤è¤¯¤¢¤ë¤´¼ÁÌä°ìÍ÷</a></li>
</ul>
</div><!-- faq -->
<p class="border" id="function"><a href="https://www.alpha-web.jp/service/asp/office/index.htm" rel="external"><img alt="µ¡Ç½¾Ò²ð µ¡Ç½¤äÎÁ¶â¤òÃÎ¤ê¤¿¤¤Êý¤Ï¤³¤Á¤é" height="59" src="/img/btn_function_o.gif" width="205"/></a></p>
</div><!-- sideContents -->
<div id="mainContents">
<div id="serviceNews">
<h3><a href="/new/index.htm#service"><img alt="¥µ¡¼¥Ó¥¹´ØÏ¢¾ðÊó" height="35" src="/img/ttl_serviceNews_o.gif" width="434"/></a></h3>
<p class="rss arrow01_p"><a href="/rss.html">RSS¤ÎÍøÍÑÊýË¡</a></p>
<ul class="topics">
<li><span>2020年12月15日</span>
<p><a href="/new/service/2020/1215_10349.html">Ç¯ËöÇ¯»ÏµÙ¶È¤Î¤´°ÆÆâ</a></p></li>
<li><span>2020年12月 4日</span>
<p><a href="/new/service/2020/1204_10298.html">¡Ö2021Ç¯¹ñÌ±¤Î½ËÆü¡×¹¹¿·¤Î¤ªÃÎ¤é¤»</a></p></li>
<li><span>2020年9月28日</span>
<p><a href="/new/service/2020/0928_10078.html">²ñ°÷¥µ¥¤¥È¤«¤é¥í¥°¥¤¥ó¤Ç¤­¤Ê¤¤¾ì¹ç¤ÎÂÐ½è¤Ë¤Ä¤¤¤Æ</a></p></li>
</ul>
<ul class="btn_list roll" style="overflow:hidden; _zoom:1">
<li><a href="/new/index.htm#mente"><img alt="¥á¥ó¥Æ¥Ê¥ó¥¹¾ðÊó" height="34" src="/img/btn_maintenanceNews_o.gif" width="152"/></a></li>
<li><a href="/new/index.htm#trouble"><img alt="¾ã³²¾ðÊó" height="34" src="/img/btn_troubleNews_o.gif" width="152"/></a></li>
</ul>
<div class="top_trouble">
<a href="/new/index.htm#trouble"><img src="./top_trouble.gif"/></a>
</div>
</div><!-- serviceNews -->
<div id="importantNews">
<h3><img alt="½ÅÍ×¤Ê¤ªÃÎ¤é¤»" height="32" src="/img/ttl_importantNews.gif" width="268"/></h3>
<ul class="arrow02">
<li><a href="/new/service/2018/0712_7907.html">¾ã³²ÄÌÃÎ¥µ¡¼¥Ó¥¹ FAXÇÛ¿®½ªÎ»¤Î¤ªÃÎ¤é¤»</a></li>
<li><a href="rss.html">¥µ¡¼¥Ó¥¹´ØÏ¢¾ðÊó¡¢¥á¥ó¥Æ¥Ê¥ó¥¹¾ðÊó¡¢¾ã³²¾ðÊó¤ÎRSSÇÛ¿®¤Ë¤Ä¤¤¤Æ</a></li>
<li><a href="https://online.alpha-web.jp/aweb/aweb_f/notice-service.asp" target="_blank">¾ã³²ÄÌÃÎ¥µ¡¼¥Ó¥¹¡ÊÌµÎÁ¡Ë¤ò¤´ÍøÍÑ¤¯¤À¤µ¤¤</a></li>
</ul>
</div><!-- importantNews -->
<div id="guidance">
<h3><a href="/support/index.html"><img alt="¤´ÍøÍÑ¤Î¼ê°ú¤­" height="35" src="/img/ttl_guidance_o.gif" width="720"/></a></h3>
<div class="lay_left">
<h4><a href="/help/office_kanriTOC.html" target="_blank"><img alt="´ÉÍý¼Ô¥Þ¥Ë¥å¥¢¥ë" height="30" src="/img/ttl_guidance01_o.gif" width="465"/></a></h4>
<div class="guidance_contents" id="gc01">
<div id="gc01_1">
<h5><img alt="´ðËÜÀßÄê" height="13" src="/img/ttl_guidance01_basis.gif" width="56"/></h5>
<ul class="arrow02">
<li><a href="/help/kanri_01.html" target="_blank">´ðËÜÀßÄê</a></li>
</ul>
</div>
<div id="gc01_2">
<h5><img alt="¥Ç¡¼¥¿¶¦Í­/¥É¥­¥å¥á¥ó¥È´ÉÍýµ¡Ç½" height="13" src="/img/ttl_guidance01_data.gif" width="197"/></h5>
<ul class="arrow02">
<li><a href="/help/kanri_03.html" target="_blank">¥­¥ã¥Ó¥Í¥Ã¥È´ÉÍý</a></li></ul>
</div>
<div id="gc01_3">
<h5><img alt="¥°¥ë¡¼¥×¥¦¥§¥¢µ¡Ç½" height="13" src="/img/ttl_guidance01_group.gif" width="121"/></h5>
<ul class="arrow02">
<li><a href="/help/kanri_02.html" target="_blank">¥°¥ë¡¼¥×¥¦¥§¥¢</a></li>
</ul>
</div>
<div id="gc01_4">
<h5><img alt="¥ï¡¼¥¯¥Õ¥í¡¼µ¡Ç½¡Ê¥ª¥×¥·¥ç¥ó¡Ë" height="13" src="/img/ttl_guidance01_work.gif" width="182"/></h5>
<ul class="arrow02">
<li><a href="/help/kanri_04.html" target="_blank">¥ï¡¼¥¯¥Õ¥í¡¼</a></li>
</ul>
</div>
<p>¡¡</p>
<p class="right arrow01_p"><a href="/help/office_kanriTOC.html" target="_blank">¤½¤ÎÂ¾µ¡Ç½¤Î¥Þ¥Ë¥å¥¢¥ë¤ò¸«¤ë</a></p>
</div><!-- guidance_contents -->
<h4><a href="/help/office_riyousyaTOC.html" target="_blank"><img alt="ÍøÍÑ¼Ô¥Þ¥Ë¥å¥¢¥ë" height="30" src="/img/ttl_guidance02_o.gif" width="465"/></a></h4>
<div class="guidance_contents">
<div id="gc02_1">
<h5><img alt="´ðËÜÀßÄê" height="13" src="/img/ttl_guidance01_basis.gif" width="56"/></h5>
<ul class="arrow02">
<li><a href="/help/riyousya_01.html" target="_blank">´ðËÜÁàºî</a></li>
</ul>
</div>
<div id="gc02_2">
<h5><img alt="¥Ç¡¼¥¿¶¦Í­/¥É¥­¥å¥á¥ó¥È´ÉÍýµ¡Ç½" height="13" src="/img/ttl_guidance01_data.gif" width="197"/></h5>
<ul class="arrow02">
<li><a href="/help/riyousya_10.html" target="_blank">¥­¥ã¥Ó¥Í¥Ã¥È</a></li>
<li><a href="/help/riyousya_11.html" target="_blank">¥Ä¡¼¥ë</a></li>
</ul>
</div>
<div id="gc02_3">
<h5><img alt="¥°¥ë¡¼¥×¥¦¥§¥¢µ¡Ç½" height="13" src="img/ttl_guidance01_group.gif" width="121"/></h5>
<ul class="arrow02">
<li><a href="/help/riyousya_02.html" target="_blank">ÅÁ¸À¥á¡¼¥ë</a></li>
<li><a href="/help/riyousya_03.html" target="_blank">¥¹¥±¥¸¥å¡¼¥é</a></li>
<li><a href="/help/riyousya_04.html" target="_blank">»ÜÀßÍ½Ìó</a></li>
<li><a href="/help/riyousya_05.html" target="_blank">ToDo</a></li>
<li><a href="/help/riyousya_06.html" target="_blank">·Ç¼¨ÈÄ</a></li>
<li><a href="/help/riyousya_07.html" target="_blank">¥ê¥ó¥¯</a></li>
<li><a href="/help/riyousya_08.html" target="_blank">¥¢¥É¥ì¥¹Ä¢</a></li>
<li><a href="/help/riyousya_09.html" target="_blank">²óÍ÷ÈÄ</a></li>
</ul>
</div>
<div id="gc02_4">
<h5><img alt="¥ï¡¼¥¯¥Õ¥í¡¼µ¡Ç½¡Ê¥ª¥×¥·¥ç¥ó¡Ë" height="13" src="img/ttl_guidance01_work.gif" width="182"/></h5>
<ul class="arrow02">
<li><a href="/help/riyousya_12.html" target="_blank">¥ï¡¼¥¯¥Õ¥í¡¼</a></li>
</ul>
<p>¡¡</p>
<ul class="arrow02">
<li><a href="/help/riyousya_13.html" target="_blank">·ÈÂÓÅÅÏÃ</a></li>
</ul>
</div>
<p class="right arrow01_p"><a href="/help/office_riyousyaTOC.html" target="_blank">¤½¤ÎÂ¾µ¡Ç½¤Î¥Þ¥Ë¥å¥¢¥ë¤ò¸«¤ë</a></p>
</div><!-- guidance_contents -->
</div><!-- lay_left -->
<div class="lay_right">
<h4><a href="/support/tool/index.html"><img alt="³Æ¼ï¥Ä¡¼¥ë" height="30" src="/img/ttl_guidance03_o.gif" width="245"/></a></h4>
<div class="guidance_contents" id="gc03">
<ul class="arrow02">
<li><a href="/support/tool/lhaca/index.html">²òÅà¥½¥Õ¥È¥À¥¦¥ó¥í¡¼¥É</a></li>
<li><a href="/support/tool/viewer/index.html">¿ÞÌÌ¥Ó¥å¡¼¥ï¥À¥¦¥ó¥í¡¼¥É</a></li>
<li><a href="/support/tool/template/index.html">¥ï¡¼¥¯¥Õ¥í¡¼¥ª¥×¥·¥ç¥ó¿½ÀÁ½ñ¥Æ¥ó¥×¥ì¡¼¥È¥À¥¦¥ó¥í¡¼¥É</a></li>
</ul>
</div><!-- guidance_contents -->
<h4><a href="/support/manual/index.html"><img alt="¤´ÍøÍÑ¤Î¼ê°ú¤­¤ò¥×¥ê¥ó¥È¤¹¤ë" height="30" src="/img/ttl_guidance04_o.gif" width="245"/></a></h4>
<div class="guidance_contents" id="gc04">
<ul class="arrow02">
<li><a href="/support/manual/index.html">¤´ÍøÍÑ¤Î¼ê°ú¤­PDFÈÇ</a></li>
</ul>
</div><!-- guidance_contents -->
<h4><a href="/support/environment/index.html" rel="popup"><img alt="Æ°ºî´Ä¶­¤ò³ÎÇ§¤¹¤ë" height="30" src="/img/ttl_guidance05_o.gif" width="245"/></a></h4>
<div class="guidance_contents" id="gc05">
<ul class="arrow02">
<li><a href="/support/environment/index.html" rel="popup">Æ°ºî´Ä¶­¡¦Ãí°Õ»ö¹à</a></li>
</ul>
</div><!-- guidance_contents -->
</div><!-- lay_right -->
</div><!-- guidance -->
<p class="topLink"><a href="#header">¤³¤Î¥Ú¡¼¥¸¤ÎÀèÆ¬¤Ø</a></p>
</div><!-- mainContents -->
</div><!-- contents -->
<div id="footer">
<p class="mark"><a href="https://privacymark.jp/" target="_blank"><img alt="¥×¥é¥¤¥Ð¥·¡¼¥Þ¡¼¥¯" height="68" src="/img/mark02.gif" width="62"/></a></p>
<ul>
<li><a href="https://www.otsuka-shokai.co.jp/policy/index.html" rel="external">¾ðÊó¥»¥­¥å¥ê¥Æ¥£´ðËÜÊý¿Ë</a> <span>|</span> </li>
<li><a href="https://www.otsuka-shokai.co.jp/privacy/index.html" rel="external">¸Ä¿Í¾ðÊóÊÝ¸îÊý¿Ë</a> <span>|</span> </li>
<li><a href="/how.html">¥µ¥¤¥ÈÍøÍÑ¾ò·ï</a> <span>|</span> </li>
<li><a href="https://www.alpha-web.jp/" target="_blank">¤¿¤è¤ì¡¼¤ë¥¤¥ó¥¿¡¼¥Í¥Ã¥È¥µ¡¼¥Ó¥¹</a></li>
</ul>
<p id="copyright"><img alt="Copyright" height="11" src="/img/copyright.gif" width="327"/></p>
</div><!-- footer -->
</body>
</html>
