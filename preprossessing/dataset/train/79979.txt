<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="dbe36749c9d3c5e9bb97" name="wot-verification"/>
<title>AdwareMedic</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- Latest compiled and minified CSS -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet"/>
<link href="styles.css" rel="stylesheet" type="text/css"/>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<!-- /home/thomasareed/adwaremedic.com/functions.php -->
<body>
<div id="title">
<div class="container">
<div id="title-content">
<h1>ADWAREMEDIC<br/><span>is now</span><br/>MALWAREBYTES<br/>ANTI-MALWARE<br/>FOR MAC</h1>
<h2>And, yes, it's still free.</h2>
<a class="gray-trans-lg" href="https://www.malwarebytes.com/mac/">LEARN MORE</a>
</div>
</div>
</div>
<!--<div class="blue-bg">
    <div class="container">
      <div class="light-blue-bg col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <h2>Press Release</h2>
        <h3>Malwarebytes Releases New Mac Product and Acquires AdwareMedic</h3>
        <h4>Release of Malwarebytes Anti-Malware for Mac brings powerful security layer to Mac users; Acquisition to build on current OS X protection capabilities.</h4>
        <p><b>SAN JOSE, Ca.</b>, July 15, 2015&mdash;Malwarebytes&trade;, the company founded to protect people and businesses from advanced online threats, today announced the release of Malwarebytes Anti-Malware for Mac, its first product dedicated to the underserved Mac user communities. The new product is designed to detect and remove malware, adware, and PUPs (potentially unwanted programs), capabilities that have been repeatedly requested by customers. Additionally, Malwarebytes announced the acquisition of AdwareMedic by The Safe Mac. AdwareMedic creator and owner Thomas Reed will join the company as Director of Mac Offerings, leading a growing team of Mac developers and researchers at Malwarebytes.</p>

        <p>"We've had repeated requests from our customers and community for malware protection on the Mac, and are now proud to unveil the first version of Malwarebytes Anti-Malware for Mac," said Chad Bacher, VP of Products for Malwarebytes. "Our vision is to provide protection across all devices, regardless of type or operating system."</p>

        <p>Traditionally, Mac users have been viewed as safe from malware, even as new threats have been publicized. According to a June 2015 OPSWAT report, only half of Mac users have antivirus protection, and that protection does not typically detect adware. In the last two years, there has been a proliferation of new adware—including Genieo, Conduit, and VSearch—that inject ads and pop-up hyperlinks in web pages, change the user's homepage and search engine, and insert unwanted toolbars into the browser. For the last several years, Reed has been programming AdwareMedic to detect and remove these known threats. AdwareMedic has become a popular adware removal tool and has been downloaded 2.8 million times so far in 2015 alone. In June, there were approximately 450,000 new downloads and 200,000 updates. Reed now brings those capabilities and his expertise to Malwarebytes.</p>

        <p>"I've been a fan of Malwarebytes for years and am thrilled to be joining the team. I'm also excited about being involved in the creation of new anti-malware capabilities for the Mac," said Reed. "Mac users need protection against what is becoming an epidemic of adware."</p>

        <p>"It used to be that Mac users were relatively safe from adware and malware. That's plainly not the case anymore. The bad guys are writing Trojans and ad pop-ups for the Mac," said Marcin Kleczynski, CEO of Malwarebytes. "So in keeping with our vision that everyone has the right to a malware-free existence, we're offering a free Mac anti-malware product. Malwarebytes Anti-Malware for Mac has been built from the ground up for the Mac environment—it's not just a simple port of our PC product. It's a natural but unique addition to the Malwarebytes family."</p>

        <p><em>Malwarebytes Anti-Malware for Mac 1.0</em> will be available as a consumer download on July 15. Small business and enterprise versions will be unveiled this fall.</p>

        <p>The release of Malwarebytes Anti-Malware for Mac comes at a time of unprecedented growth at Malwarebytes. The company is also rapidly expanding into the enterprise market as security breaches hit record highs, with high-profile customers in wide-ranging industry sectors such as retail, corporate, Internet, financial services, education, entertainment, life sciences, government and critical infrastructure. Malwarebytes Anti-Malware has been downloaded 500 million times, with over 5 billion pieces of malware removed; in 2014 alone, Malwarebytes cleaned 250 million computers.</p>

        <p><b>CONTACT:</b><br/>
          Doron Aronson<br/>
          Malwarebytes<br/>
          1 (408) 221-6992<br/>
          <a href="mailto:daronson@malwarebytes.org">daronson@malwarebytes.org</a></p>

<p>Paula Dunne, CONTOS DUNNE COMMUNICATIONS<br/>
  1 (408) 776-1400 (o)<br/>
  1 (408) 893-8750 (m)<br/>
  <a href="mailto:paula@contosdunne.com">paula@contosdunne.com</a></p>

      </div>
      <div class="sidebar col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <h2>Contact Us</h2>
        <p><a href="https://support.malwarebytes.org/?b_id=9511">Support Forum</a></p>
        <h2>About Malwarebytes</h2>
        <p><a href="https://www.malwarebytes.org">Malwarebytes&trade;</a> provides anti-malware and anti-exploit software designed to protect businesses and consumers against zero-day threats that consistently escape detection by traditional antivirus solutions. Malwarebytes Anti-Malware earned an “Outstanding” rating by CNET editors, is a PCMag.com Editor’s Choice, and was the only security software to earn a perfect malware remediation score from AV-TEST.org. That’s why more than 50,000 SMBs and Enterprise businesses worldwide trust Malwarebytes™ to protect their data. Founded in 2008, Malwarebytes is headquartered in California, operates offices in Europe, and employs a global team of researchers and experts. For more information, please visit us at <a href="https://www.malwarebytes.org">www.malwarebytes.org.</a></p>
      </div>
    </div>
  </div>-->
<script>
//if ('MutationObserver' in window)
//{
//// select the target node
//var target = document.querySelector('#download');
// 
//// create an observer instance
//var observer = new MutationObserver(function(mutations) {
//  mutations.forEach(function(mutation) {
//    window.alert("The Download button has been modified by adware running on your computer, to prevent you from downloading AdwareMedic! Download AdwareMedic from a different computer, then transfer it to this one via flash drive.");
//  });    
//});
// 
//// configuration of the observer:
//var config = { attributes: true, attributeFilter: ['href'] };
// 
//// pass in the target node, as well as the observer options
//observer.observe(target, config);
//}
</script>
</body>
</html>