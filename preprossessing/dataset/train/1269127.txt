<!DOCTYPE html>
<html lang="tr-TR">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<title>Sayfa bulunamadı – Saray Belediyesi</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.saray.bel.tr/feed/" rel="alternate" title="Saray Belediyesi » beslemesi" type="application/rss+xml"/>
<link href="https://www.saray.bel.tr/comments/feed/" rel="alternate" title="Saray Belediyesi » yorum beslemesi" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.saray.bel.tr\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.saray.bel.tr/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.saray.bel.tr/wp-includes/css/dashicons.min.css?ver=5.4.4" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.saray.bel.tr/wp-content/plugins/everest-forms/assets/css/everest-forms.css?ver=1.5.2" id="everest-forms-general-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.saray.bel.tr/wp-content/plugins/translatepress-multilingual/assets/css/trp-floater-language-switcher.css?ver=1.6.7" id="trp-floater-language-switcher-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.saray.bel.tr/wp-content/plugins/translatepress-multilingual/assets/css/trp-language-switcher.css?ver=1.6.7" id="trp-language-switcher-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans%3A400%2C600&amp;ver=5.4.4" id="colormag_google_fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.saray.bel.tr/wp-content/themes/colormag/style.css?ver=5.4.4" id="colormag_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.saray.bel.tr/wp-content/themes/colormag/fontawesome/css/font-awesome.css?ver=4.2.1" id="colormag-fontawesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.saray.bel.tr/wp-content/themes/colormag/js/magnific-popup/magnific-popup.css?ver=20150310" id="colormag-featured-image-popup-css-css" media="all" rel="stylesheet" type="text/css"/>
<script>if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script><script src="https://www.saray.bel.tr/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.saray.bel.tr/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<!--[if lte IE 8]>
<script type='text/javascript' src='https://www.saray.bel.tr/wp-content/themes/colormag/js/html5shiv.min.js?ver=5.4.4'></script>
<![endif]-->
<link href="https://www.saray.bel.tr/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.saray.bel.tr/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.saray.bel.tr/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.4" name="generator"/>
<meta content="Everest Forms 1.5.2" name="generator"/>
<link href="https://www.saray.bel.tr/components/com_config/Doc/document.php%09" hreflang="tr-TR" rel="alternate"/>
<link href="https://www.saray.bel.tr/en/components/com_config/Doc/document.php%09/" hreflang="en-US" rel="alternate"/>
<link href="https://www.saray.bel.tr/wp-content/uploads/2019/07/index2-1-150x150.png" rel="icon" sizes="32x32"/>
<link href="https://www.saray.bel.tr/wp-content/uploads/2019/07/index2-1-e1564067996622.png" rel="icon" sizes="192x192"/>
<link href="https://www.saray.bel.tr/wp-content/uploads/2019/07/index2-1-e1564067996622.png" rel="apple-touch-icon"/>
<meta content="https://www.saray.bel.tr/wp-content/uploads/2019/07/index2-1-e1564067996622.png" name="msapplication-TileImage"/>
</head>
<body class="error404 wp-custom-logo everest-forms-no-js translatepress-tr_TR " data-rsssl="1">
<div class="hfeed site" id="page">
<header class="site-header clearfix " id="masthead">
<div class="clearfix" id="header-text-nav-container">
<div class="news-bar">
<div class="inner-wrap clearfix">
<div class="date-in-header">
			Salı, Ocak 12, 2021		</div>
<div class="social-links clearfix">
<ul>
<li><a href="https://www.facebook.com/belediyesaray/" target="_blank"><i class="fa fa-facebook"></i></a></li><li><a href="https://twitter.com/saraybelediye" target="_blank"><i class="fa fa-twitter"></i></a></li><li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li><li><a href="https://www.instagram.com/belediyesaray/?igshid=1c0y7m3evl8c2" target="_blank"><i class="fa fa-instagram"></i></a></li><li><a href="#" target="_blank"><i class="fa fa-pinterest"></i></a></li><li><a href="#" target="_blank"><i class="fa fa-youtube"></i></a></li> </ul>
</div><!-- .social-links -->
</div>
</div>
<div class="inner-wrap">
<div class="clearfix" id="header-text-nav-wrap">
<div id="header-left-section">
<div id="header-logo-image">
<a class="custom-logo-link" href="https://www.saray.bel.tr/" rel="home"><img alt="Saray Belediyesi" class="custom-logo" height="100" src="https://www.saray.bel.tr/wp-content/uploads/2019/07/index2-1-e1564067996622.png" width="100"/></a> </div><!-- #header-logo-image -->
<div class="screen-reader-text" id="header-text">
<h3 id="site-title">
<a href="https://www.saray.bel.tr/" rel="home" title="Saray Belediyesi">Saray Belediyesi</a>
</h3>
<p id="site-description">Saray Belediyesine Hoş Geldiniz</p>
<!-- #site-description -->
</div><!-- #header-text -->
</div><!-- #header-left-section -->
<div id="header-right-section">
<div class="clearfix" id="header-right-sidebar">
<aside class="widget widget_media_image clearfix" id="media_image-3"><img alt="" class="image wp-image-367 attachment-1000x116 size-1000x116" height="116" sizes="(max-width: 1000px) 100vw, 1000px" src="https://www.saray.bel.tr/wp-content/uploads/2019/07/0022-1024x119.png" srcset="https://www.saray.bel.tr/wp-content/uploads/2019/07/0022-1024x119.png 1024w, https://www.saray.bel.tr/wp-content/uploads/2019/07/0022-300x35.png 300w, https://www.saray.bel.tr/wp-content/uploads/2019/07/0022-768x89.png 768w, https://www.saray.bel.tr/wp-content/uploads/2019/07/0022.png 1594w" style="max-width: 100%; height: auto;" width="1000"/></aside> </div>
</div><!-- #header-right-section -->
</div><!-- #header-text-nav-wrap -->
</div><!-- .inner-wrap -->
<nav class="main-navigation clearfix" id="site-navigation" role="navigation">
<div class="inner-wrap clearfix">
<p class="menu-toggle"></p>
<div class="menu-primary-container"><ul class="menu" id="menu-anamenu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-371" id="menu-item-371"><a href="https://saray.bel.tr">ANA SAYFA</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-508" id="menu-item-508"><a>BAŞKAN</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-520" id="menu-item-520"><a href="https://www.saray.bel.tr/baskana-mesaj/">Başkan’a Mesaj</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-521" id="menu-item-521"><a href="https://www.saray.bel.tr/baskanin-mesaji/">Başkan’ın Mesajı</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-522" id="menu-item-522"><a href="https://www.saray.bel.tr/516-2/">Başkan’ın Öz Geçmişi</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-525" id="menu-item-525"><a>KURUMSAL</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-540" id="menu-item-540"><a href="https://www.saray.bel.tr/organizasyon-semasi/">Organizasyon Şeması</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-545" id="menu-item-545"><a href="https://www.saray.bel.tr/541-2/">Vizyon ve Misyonumuz</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-547" id="menu-item-547"><a>Müdürlüklerimiz</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-551" id="menu-item-551"><a href="https://www.saray.bel.tr/insan-kaynaklari-ve-egitim-mudurlugu/">İnsan Kaynakları ve Eğitim Müdürlüğü</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-559" id="menu-item-559"><a href="https://www.saray.bel.tr/zabita-mudurlugu/">Zabıta Müdürlüğü</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-567" id="menu-item-567"><a href="https://www.saray.bel.tr/park-ve-bahceler-mudurlugu/">Park ve Bahçeler Müdürlüğü</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-571" id="menu-item-571"><a href="https://www.saray.bel.tr/568-2/">Kültür ve Sosyal İşler Müdürlüğü</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-575" id="menu-item-575"><a href="https://www.saray.bel.tr/mali-hizmetler-mudurlugu/">Mali Hizmetler Müdürlüğü</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-606" id="menu-item-606"><a href="https://www.saray.bel.tr/yazi-isleri-mudurlugu/">Yazı İşleri Müdürlüğü</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-613" id="menu-item-613"><a href="https://www.saray.bel.tr/fen-isleri-mudurlugu/">Fen İşleri Müdürlüğü</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-617" id="menu-item-617"><a href="https://www.saray.bel.tr/temizlik-isleri-mudurlugu/">Temizlik İşleri Müdürlüğü</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-774" id="menu-item-774"><a href="https://www.saray.bel.tr/771-2/">Muhtarlık İşleri Müdürlüğü</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-579" id="menu-item-579"><a href="https://www.saray.bel.tr/evlendirme-memurlugu/">Evlendirme Memurluğu</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-659" id="menu-item-659"><a>Meclis ve Encümen</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-813" id="menu-item-813"><a href="https://saray.bel.tr/807-2/">Meclis</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-814" id="menu-item-814"><a href="https://www.saray.bel.tr/1767-2/">Encümen</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-591" id="menu-item-591"><a href="https://www.saray.bel.tr/muhtarliklar/">Muhtarlıklar</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-623" id="menu-item-623"><a href="https://www.saray.bel.tr/618-2/">Hukuk İşleri Birimi</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-527" id="menu-item-527"><a href="https://ebelediye.saray.bel.tr/ebelediye/sorgulama/borcSorgulama.xhtml" rel="noopener noreferrer" target="_blank">E-BELEDİYE</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-594" id="menu-item-594"><a>SARAY</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-665" id="menu-item-665"><a href="https://www.saray.bel.tr/sarayin-tarihi/">Saray’ın Tarihi</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-675" id="menu-item-675"><a href="https://www.saray.bel.tr/ataturk-saray-da/">Atatürk Saray’da</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-691" id="menu-item-691"><a href="https://www.saray.bel.tr/saray-ilcesinin-dogal-ortam-ozellikleri-ve-cevre-yapisi/">Saray İlçesinin Doğal Ortam Özellikleri</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-692" id="menu-item-692"><a href="https://www.saray.bel.tr/sarayin-nufus-ozellikleri/">Saray’ın Nüfus Özellikleri</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-693" id="menu-item-693"><a>Saray’da Sanayi Faaliyetleri</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1583" id="menu-item-1583"><a>YEREL ANONSLAR</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1526" id="menu-item-1526"><a href="https://www.saray.bel.tr/belediye-megafonundan-yapilan-anonslar/">Yerel Anonslar</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1533" id="menu-item-1533"><a href="https://www.saray.bel.tr/cenaze-anonslari/">Cenaze Anonsları</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2409" id="menu-item-2409"><a>İLETİŞİM</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-601" id="menu-item-601"><a href="https://www.saray.bel.tr/iletisim/">İletişim ve Adres</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-964" id="menu-item-964"><a href="https://www.saray.bel.tr/bu-belediye-bizim-2/">Bu Belediye Bizim Projesi</a></li>
</ul>
</li>
</ul></div>
<div class="random-post">
<a href="https://www.saray.bel.tr/2020/06/02/belediyemiz-2020-yili-evsel-kati-atik-toplama-ve-tasima-tarifelerinin-belirlenmesine-yonelik-calisma-raporu/" title="Rastgele bir yazı görüntüle"><i class="fa fa-random"></i></a>
</div>
<i class="fa fa-search search-top"></i>
<div class="search-form-top">
<form action="https://www.saray.bel.tr/" class="search-form searchform clearfix" method="get">
<div class="search-wrap">
<input class="s field" name="s" placeholder="Ara" type="text"/>
<button class="search-icon" type="submit"></button>
</div>
</form><!-- .searchform --> </div>
</div>
</nav>
</div><!-- #header-text-nav-container -->
</header>
<div class="clearfix" id="main">
<div class="inner-wrap clearfix">
<div id="primary">
<div class="clearfix" id="content">
<section class="error-404 not-found">
<div class="page-content">
<header class="page-header">
<h1 class="page-title">Off! Bu sayfa bulunamadı.</h1>
</header>
<p>Görünen o ki burada hiç bir şey bulunamadı. Bir de aşağıdan arama yapmayı deneyin.</p>
<form action="https://www.saray.bel.tr/" class="search-form searchform clearfix" method="get">
<div class="search-wrap">
<input class="s field" name="s" placeholder="Ara" type="text"/>
<button class="search-icon" type="submit"></button>
</div>
</form><!-- .searchform -->
</div><!-- .page-content -->
</section><!-- .error-404 -->
</div><!-- #content -->
</div><!-- #primary -->
<div id="secondary">
<aside class="widget_text widget widget_custom_html clearfix" id="custom_html-3"><div class="textwidget custom-html-widget"><div id="fb-root"></div>
<script async="" crossorigin="anonymous" defer="" nonce="gsuR839B" src="https://connect.facebook.net/tr_TR/sdk.js#xfbml=1&amp;version=v9.0"></script>
<div class="fb-page" data-adapt-container-width="true" data-height="" data-hide-cover="false" data-href="https://www.facebook.com/belediyesaray\" data-show-facepile="true" data-small-header="true" data-tabs="timeline" data-width=""><blockquote cite="https://www.facebook.com/belediyesaray/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/belediyesaray/">Saray Belediye Başkanlığı</a></blockquote></div></div></aside><aside class="widget widget_media_image clearfix" id="media_image-17"><h3 class="widget-title"><span>YEREL ANONSLAR</span></h3><a href="https://www.saray.bel.tr/belediye-megafonundan-yapilan-anonslar/"><img alt="" class="image wp-image-1506 attachment-full size-full" height="400" sizes="(max-width: 800px) 100vw, 800px" src="https://www.saray.bel.tr/wp-content/uploads/2020/05/anonslar.jpg" srcset="https://www.saray.bel.tr/wp-content/uploads/2020/05/anonslar.jpg 800w, https://www.saray.bel.tr/wp-content/uploads/2020/05/anonslar-300x150.jpg 300w, https://www.saray.bel.tr/wp-content/uploads/2020/05/anonslar-768x384.jpg 768w" style="max-width: 100%; height: auto;" width="800"/></a></aside><aside class="widget widget_media_image clearfix" id="media_image-11"><h3 class="widget-title"><span>HALK MASASI</span></h3><a href="https://saray.bel.tr/halk-masasi/"><img alt="" class="image wp-image-884 attachment-full size-full" height="232" src="https://www.saray.bel.tr/wp-content/uploads/2019/08/halkyeni.jpg" style="max-width: 100%; height: auto;" title="HALK MASASI" width="267"/></a></aside><aside class="widget widget_media_image clearfix" id="media_image-13"><h3 class="widget-title"><span>NÖBETÇİ ECZANELER</span></h3><a href="https://saray.bel.tr/nobetci-eczaneler/"><img alt="" class="image wp-image-984 attachment-full size-full" height="300" sizes="(max-width: 300px) 100vw, 300px" src="https://www.saray.bel.tr/wp-content/uploads/2019/10/eczane.jpg" srcset="https://www.saray.bel.tr/wp-content/uploads/2019/10/eczane.jpg 300w, https://www.saray.bel.tr/wp-content/uploads/2019/10/eczane-150x150.jpg 150w" style="max-width: 100%; height: auto;" width="300"/></a></aside><aside class="widget widget_media_image clearfix" id="media_image-9"><h3 class="widget-title"><span>OTOBÜS SAATLERİ VE GÜZERGÂHLARI</span></h3><figure class="wp-caption alignnone" style="width: 300px"><a href="https://saray.bel.tr/otobus-saatleri-ve-guzergahlari/"><img alt="" class="image " height="204" src="https://saray.bel.tr/wp-content/uploads/2019/07/Sarayulasim-300x204.jpg" width="300"/></a><figcaption class="wp-caption-text">Saray İlçesine Ait Otobüs Hareket Güzergahları ve Saatleri</figcaption></figure></aside>
</div>
</div><!-- .inner-wrap -->
</div><!-- #main -->
<footer class="clearfix " id="colophon">
<div class="footer-widgets-wrapper">
<div class="inner-wrap">
<div class="footer-widgets-area clearfix">
<div class="tg-footer-main-widget">
<div class="tg-first-footer-widget">
<aside class="widget widget_text clearfix" id="text-9"> <div class="textwidget"><p><strong>Saray Belediye Başkanlığı © 2020 Tüm Hakları Saklıdır.</strong></p>
</div>
</aside> </div>
</div>
<div class="tg-footer-other-widgets">
<div class="tg-second-footer-widget">
</div>
<div class="tg-third-footer-widget">
</div>
<div class="tg-fourth-footer-widget">
</div>
</div>
</div>
</div>
</div> <div class="footer-socket-wrapper clearfix">
<div class="inner-wrap">
<div class="footer-socket-area">
<div class="footer-socket-right-section">
<div class="social-links clearfix">
<ul>
<li><a href="https://www.facebook.com/belediyesaray/" target="_blank"><i class="fa fa-facebook"></i></a></li><li><a href="https://twitter.com/saraybelediye" target="_blank"><i class="fa fa-twitter"></i></a></li><li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li><li><a href="https://www.instagram.com/belediyesaray/?igshid=1c0y7m3evl8c2" target="_blank"><i class="fa fa-instagram"></i></a></li><li><a href="#" target="_blank"><i class="fa fa-pinterest"></i></a></li><li><a href="#" target="_blank"><i class="fa fa-youtube"></i></a></li> </ul>
</div><!-- .social-links -->
</div>
<div class="footer-socket-left-section">
<div class="copyright">Tüm hakları saklıdır © 2021 <a href="https://www.saray.bel.tr/" title="Saray Belediyesi"><span>Saray Belediyesi</span></a>.<br/>Tema: <a href="https://themegrill.com/themes/colormag" rel="author" target="_blank" title="ThemeGrill"><span>ThemeGrill</span></a> tarafından ColorMag. Altyapı <a href="https://wordpress.org" target="_blank" title="WordPress"><span>WordPress</span></a>.</div> </div>
</div>
</div>
</div>
</footer>
<a href="#masthead" id="scroll-up"><i class="fa fa-chevron-up"></i></a>
</div><!-- #page -->
<div class="trp-language-switcher-container trp-floater-ls-names trp-bottom-right" data-no-translation="" id="trp-floater-ls" onclick="">
<div class="trp-with-flags" id="trp-floater-ls-current-language">
<a class="trp-floater-ls-disabled-language trp-ls-disabled-language" href="javascript:void(0)" onclick="void(0)">
<img alt="tr_TR" class="trp-flag-image" height="12" src="https://www.saray.bel.tr/wp-content/plugins/translatepress-multilingual/assets/images/flags/tr_TR.png" title="Turkish" width="18"/>Turkish				</a>
</div>
<div class="trp-with-flags" id="trp-floater-ls-language-list">
<a href="https://www.saray.bel.tr/en/components/com_config/Doc/document.php%09/" title="English">
<img alt="en_US" class="trp-flag-image" height="12" src="https://www.saray.bel.tr/wp-content/plugins/translatepress-multilingual/assets/images/flags/en_US.png" title="English" width="18"/>English					</a>
<a class="trp-floater-ls-disabled-language trp-ls-disabled-language" href="javascript:void(0)">
<img alt="tr_TR" class="trp-flag-image" height="12" src="https://www.saray.bel.tr/wp-content/plugins/translatepress-multilingual/assets/images/flags/tr_TR.png" title="Turkish" width="18"/>Turkish				</a>
</div>
</div>
<script type="text/javascript">
		var c = document.body.className;
		c = c.replace( /everest-forms-no-js/, 'everest-forms-js' );
		document.body.className = c;
	</script>
<script src="https://www.saray.bel.tr/wp-content/themes/colormag/js/jquery.bxslider.min.js?ver=4.2.10" type="text/javascript"></script>
<script src="https://www.saray.bel.tr/wp-content/themes/colormag/js/navigation.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.saray.bel.tr/wp-content/themes/colormag/js/colormag-custom.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.saray.bel.tr/wp-content/themes/colormag/js/sticky/jquery.sticky.js?ver=20150309" type="text/javascript"></script>
<script src="https://www.saray.bel.tr/wp-content/themes/colormag/js/magnific-popup/jquery.magnific-popup.min.js?ver=20150310" type="text/javascript"></script>
<script src="https://www.saray.bel.tr/wp-content/themes/colormag/js/fitvids/jquery.fitvids.js?ver=20150311" type="text/javascript"></script>
<script src="https://www.saray.bel.tr/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
</body>
</html>
