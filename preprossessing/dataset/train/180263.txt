<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "24929",
      cRay: "610fa133c916238c",
      cHash: "d184570ae76300e",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXpjb3N0dW1lLmNvbS8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "JLQMVR4Y7pSOuy7pyc3t3aGeICEu2rh3qvNNLz0OZyPxj+4jRzgn2ZBq93cHDuHg9NeGvo37sHm3DyYG270AG8lkKcel61oXr3oWbQlH5w8jbTERc5Fzu2p/EFsW7Qibnzrxu82/wXjR3trI3ax0M10u5/uZc+UgyU0bVy32dLVfKL7MSNCDC5nXoZLAGrRCuJ7L8jOah1NfyXMplYGSOrEJyDwb/wXWymgvenEqcEspSF/icHPd4+NyfvJIciAnpJJPKrMqZaDsirylI/skf3KCm6CIrjWJu/GehxkIMhqrg1ShQ6JwNVKz8xvG4ZMG1c+0JxyEbxCh/lV6GflEHPA6ULbGfam3zXoFjFKVfRbU52T3l23gMM9zq3xonMCuXlm/FZpAEhaGiesTsfZXC+Q7d4WfNJUtZM9dbl4CrCXVZ3vOwY0sNlqzZrf0SrblBXUYu/1OlBTs0YgyWFMmZURBJmQ5K3pL+7KFGoaCu0CpZb3sd0VSDI1qS5TggCfPD8cN9vwu6BORO4maijgaxze4MI7rTGUm8/ZNf37oqZXdX8U8LOESJDhGg4d6M+5C6lQVjKOj7S5g85eNad/18UjbzTOVyF+1ZsVA3coi93RpHrFOdn00I3eBVBlvCgP5B2rjxFdfZKRA9leUqkQVwnZ7Usqe2cb4R5aAZGQas0fE4QhQ/QeZaAut9rkU5UFH5+Fy4dzqZGwwS5iFBmpS8/OnD/gBIeYQWSsO4gLWt3cWjBO5OWohXpkLu+YX5sTA",
        t: "MTYxMDU0NjI4MS42MDYwMDA=",
        m: "W5z62eeO3yg3WqKPHWHuQYge8gC38ZdAacQaTxXwfpI=",
        i1: "n9C99ltHhPlFxe7IRiqfnQ==",
        i2: "pk+R7IBjyselCT3lQylmGw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "7FahP8bLIAZoUZZczfWQZZ5vxHcisls/bzc9VXTSoRc=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.azcostume.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=a4b5f2f60f524523e63e8efbd1f6fcb66d24f42e-1610546281-0-AfMeYbloZ3W07JVnOoKUUmNJDlBgXx02h6e2OiPYMAFoiJrXYzzjt96fCWfo0wgSWi6SdnlYObZHU8gKl3QGKS_4zcCq84fZfWc7vahlBQUQjEt69_WWABXJznBQBnxR_trWDK2svKXxiMaLBPzqXPeWY3wHh7MWMsdMdS9va8c0xz--3WDQ7eI67Wku3vO-CWJcae9d8eWsbx4voXCDtCgM70g4aEG3i6boDOFboMTEjAG7anZIfyMRV5-b0IkRK-Fu-m-J0q0ahK15D5BFs4QQZYbfDT1SjWd29jilMfaeaEkJ-3b_NPIvBE1ccYNhDWzByAbiY7m9_CwyF5eVBtN_qJG-McjlkCEKDlPLWKeCVCA60JCUa-_7rXU4ZvhHlvxsw7tK1HrLLDG6AGrmRgcg554ZJty6htQFar08WDX3wHm007ZIrcv2S0LXLuAFIIYc-Bkyd1GA22nampoJSNQcTv9m_0MSzNBJ9sQtxwVoMFC8iD1prrRTXF6nKfMsTmCAyC5rFV17T0aQ34b52r0" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="bc03f7501dbe6c5759318feadcb213b63296a3d3-1610546281-0-AfdKxH/H4tnBApITOw4gdf8BPjOoncWwMIc9Xk/PFsamx40dtk0Pxw8jcPCGufXE1ooxrKZ9tG0b3oK0BmC5j2K7DyqTfuRmfAg1n+ehIIOD9KDHsRU0X4Tlzs50iDB2nHdZu3pzx8vKQvjNw5jfryRtpNIEsZiaI5C7Yia1o37fwvyLUbkt0coYeAlT0t4/Ytq2YghAG56AUHW06s108zkDuzJmgXt6BIZIIC2oafuRwhePKaj4fWDhHOGKJXEBSFuHQ+H43JYGs9hRaJnd1TI1Ads3bctzTjgf7+c2P3kDjSPqQwekdM3yIZvKwOYLBAyzLBSAZHpZYzHC14mG8+YwZG2BYdX131qZB9wn3A1W2j9IzAy9kD4o5GKYC02iP6QJQIw8nCwX7LJKNXIPMI8j8U87XRdECyycE1NUjGiN9lz12GI9sYOq4bbF6OqaxRWMbN92K876hRYhW+vQrM/rlohvJF6HztB0qm14cKLaoKWcY5nCsQrxoX1yLTeCdlgWFGciaBwAEHuvk1LDrf+SRotaRO8aJ4Meog5ujFmZCfA0IEE+iAStg13ITnkwDnh/+scbccoE2BRNoqRcChD/5q10tUSfr9HQoGwtXyOBXoVvOt9YNj6hOpPfzOduqhge7knFLu8QtQ1DQD/MDkuCM9kOYoqJOPhoC91ztWiv31bfcr4pBGWInlSyvqcdmciKTS4P2HBWq5Ua47e3GZxWo2Jv0DywRHGmOqWttzC03zid8HM1GnFkSKdLJ3Qaw83EaPe2DhPPPDJnj3ExfPLjgxG7aVKwIJgHaLEJcuTXDvSUDgYbOee8wcUw2xyGKWUZ2m+Gp3tf6o11i6rWg7V7NoJ6UicVvv2Sigp7ImJEqCbHaPFE6RnkDwxFhXI9FBBG8MebtIMnhju44eEbmaFhbuNlEve1Z7+mv5ZXhqNRFhJItnnb5G/KAMzMWmBaDj6omi5itT/k98+uflxmxGOuz1j92zvMV5twGSKj9QfUGK8lEbz0KEhEA+NOyp+vdZ9aVhNJkXE7g9NPqp4wEsYscaXaxzOGrU0OtNcYcCGBeEe4BuB69vCaCgRNAZsvIvF9JdqaG2gVENX8IbQo7b+xYXIenk7T0UdJZKKjgzCy7jmTDZvxUhZJNEii/se8glqnuHy2+tPKD0hfq4MGXWFKG/+G2cm8y+sl/xvh0+VoEM3HCmdb5h6ozLFfnyTIyHeY9iH7oTRLa/bJPqltsSxsu/BpAwiRpGKZod2FYyTq6cgMtPXcmqfC+p1QwgGzy39OltjGbtYkfpzvmZq2FQrqaTsMPKRnOT+nrcl225xs"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="1359b9ffd5b4f71f3b5418dc7ba8d756"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610fa133c916238c')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610fa133c916238c</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
