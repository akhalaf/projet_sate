<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=1100" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Site not found | </title>
<link href="https://hub-hachette.s3.amazonaws.com/static/CACHE/css/output.7216fdc94638.css" rel="stylesheet" type="text/css"/>
<link href="https://hub-hachette.s3.amazonaws.com/static/zco/css/not_minified.css" rel="stylesheet"/>
<script type="text/javascript">
        var TIMEZONE_URL = "/json/timezones/";
        var DOMAIN_TIME_OFFSET = "";
        var CURRENT_TIMEZONE = "";
    </script>
</head>
<body>
<script></script>
<div class="page-wrapper" id="everything-but-the-footer">
<div class="site-header" id="head">
<div class="logo-user">
<a class="logo" href="/">
</a>
</div>
<div class="header-controls">
<div class="user">
</div>
</div>
<div class="clear"></div>
<div class="clear"></div>
</div>
<div class="main-content">
<div class="iframe-loading" id="iframe_page_loading" style="display: none; position: fixed; top: 50%; left: 50%; margin-left: -100px; margin-top: -100px;"><img src="https://hub-hachette.s3.amazonaws.com/static/zco/img/loader.gif" width="200px"/></div>
<div class="local-nav" id="local-nav">
<ul></ul>
<div class="clear"></div>
</div>
<!-- iFrame to cover everything-but the tabs -->
<iframe height="300" id="iframe_full_page" scrolling="no" style="overflow:hidden; display: none;" width="100%"></iframe>
<div class="local-head " id="local-head">
<div class="content local-head-wrapper">
<ul class="page-head-controls jui-primary"></ul>
<div class="page-title js-page-title">
<h2>Site not found</h2>
<span></span>
</div>
<div class="clear"></div>
</div>
</div>
<div id="header-triangle"></div>
<!-- iFrame to cover everything-but the tabs and page title -->
<iframe height="300" id="iframe_page_title" scrolling="no" style="overflow:hidden; display:none;" width="100%"></iframe>
<!-- Primary content -->
<div class="main" id="id_block_main">
<!-- Primary content -->
<div class="span-24 main last">
<p>The requested site does not exist.</p>
</div>
</div>
</div>
</div><!-- /page-wrapper -->
<div class="footer">
<div class="footer-section copyright-notice">
<div class="inner">
                © 2021 ZOO Digital<br/>                
                Interactions with this system will be logged
                
                
                
                
            </div>
</div>
<div class="footer-section zoo-footer-logo">
</div>
<div class="footer-section last version-number">
<div class="inner changelog-frontend-activate" onclick="_zooflash.app.showChangelog(true)">
</div>
</div>
</div>
<!-- DIALOGS -->
<div id="ajaxUploadDialog" style="display:none" title="File Upload">
<form id="ajaxUploadForm">
<div><input id="Filedata" name="Filedata" type="file"/></div>
<div id="ajaxUploadError">
</div>
</form>
</div>
<div id="eventReportDialog" style="display:none" title="History Report">
<form id="eventReportForm">
<table cellspacing="0">
<tr><td style="text-align:right;">From: </td><td><input id="eventsFrom" name="eventsFrom" tabindex="-1" type="text"/></td></tr>
<tr><td style="text-align:right;">To: </td><td><input id="eventsTo" name="eventsTo" tabindex="-1" type="text"/></td></tr>
</table>
<div><span id="eventReportNumResults"></span></div>
</form>
</div>
<div id="objectEventHistoryDialog" style="display:none" title="History">
</div>
<script type="text/javascript">
        var ENABLE_SHORT_DURATIONS=false;
        var STATIC_PREFIX = 'https://hub-hachette.s3.amazonaws.com/static/';
        var MEDIA_URL='';
        var STATIC_URL='https://hub-hachette.s3.amazonaws.com/static/';
        var Q  = '';
        var SEARCH_BAR_VAL = Q;
        var SEARCH_URL = '/entities/search_results/';
        
    </script>
<script src="https://hub-hachette.s3.amazonaws.com/static/CACHE/js/output.f5a7711c96f7.js"></script>
<script type="text/javascript">
  (function(d) {
      var config = {
      kitId: 'ydk0znz',
      scriptTimeout: 3000
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='//use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)  })(document);
    </script>
<script id="zooflash-announcement-template" type="text/template">
<div :class="'announcement alert ' + (an.css_class ? an.css_class : 'alert-default')" role="alert">
    <button type="button" class="close announcement-dismiss" v-on:click="dismiss">
        <span aria-hidden="true">&times;</span>
    </button>
    <span class="announcement-content">
        <div v-if="an.admin_only" class="glyphicon glyphicon-start announcement-star"></div>
        <p v-if="an.link" class="alert-heading announcement-name">
            <a v-if="an.link" :href="an.link" target="_blank">
                <strong>${ an.name }</strong>
            </a>
        </p>
        <p v-else class="alert-heading announcement-name">
            <strong>${ an.name }</strong>
        </p>
        <p class="announcement-body" v-html="an.body"></p>
    </span>
</div>
</script>
<script id="zooflash-change-log" type="text/template">
<div class="zooflash-popup">
    <div class="modal in" style="display: block">
        <div class="zooflash-overlay" v-on:click="hide"></div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="padding: 15px 0; margin: 0 15px;">
                    <button type="button" class="close" v-on:click="hide">
                        <span style="padding-top: 0px;">&times;</span>
                    </button>
                    <h4 class="modal-title">Change Log</h4>
                </div>
                <div class="modal-body main-body" style="height: calc(90vh - 170px)">
                    <div class="panel panel-primary" style="height: 100%; overflow:auto;">
                        <div class="panel-heading">
                            <h3 class="panel-title">Version ${ version.version_number }</h3>
                        </div>
                        <div class="panel-body" >
                            <h4>${ formatDate(version.release_date) }</h4>
                            <span class="release-note-description" v-html="version.release_note"></span>
                            <div v-for="release_note in version.releaseNotes" :class="'panel panel-' + (release_note.admin_only ? 'danger' : 'info')">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <div v-if="release_note.admin_only" class="glyphicon glyphicon-star" style="margin-right: 10px; float: left"></div>
                                        <div style="float: left">${ release_note.title }</div>
                                        <div style="clear:both"></div>
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <p class="release-note-description" v-html="release_note.description"></p>
                                    <div v-if="release_note.thumbnail_image">
                                        <img :src="release_note.thumbnail_image" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 0px; border-top: 0;">
                    <div style="border-top: 1px solid #e5e5e5; width: 100%">
                        <div style="margin: 15px; padding-top: 15px;">
                            <div v-if="versions.length > 1" class="zooflash-button-container">
                                <button v-if="index === lastIndex" type="button" class="btn btn-default" disabled>
                                    Oldest Release
                                </button>
                                <button v-else type="button" class="btn btn-sm btn-default" v-on:click="page(1)">
                                    Previous Release
                                </button>
                                <button v-if="index === 0" type="button" class="btn btn-sm btn-default" disabled>
                                    Latest Release
                                </button>
                                <button v-else type="button" class="btn btn-sm btn-default" v-on:click="page(-1)">
                                    Next Release
                                </button>
                            </div>
                            <div class="zooflash-button-container">
                                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" v-on:click="hide">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</script>
<script id="zooflash-popup" type="text/template">
<div class="zooflash-popup">
    <div class="modal in" style="display: block; padding-right: 0px;">
        <div class="zooflash-overlay" v-on:click="hide"></div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 v-if="singleTitle">${ singleTitle }</h2>
                    <ul v-else class="nav nav-pills" style="margin-left: 0px !important; padding: 0px !important;">
                        <li v-if="popup.specialAnnouncement" :class="view === 'specialAnnouncement' ? 'active' : ''" v-on:click="select('specialAnnouncement')">
                            <a href="#" class="zooflash-popup-menu">${ popup.specialAnnouncement.name }</a>
                        </li>
                        <li v-if="popup.whatsnew.length" :class="view === 'whatsnew' ? 'active' : ''" v-on:click="select('whatsnew')">
                            <a href="#" class="zooflash-popup-menu">What's new</a>
                        </li>
                        <li v-if="popup.comingsoon.length" :class="view === 'comingsoon' ? 'active' : ''" v-on:click="select('comingsoon')">
                            <a href="#" class="zooflash-popup-menu">Coming Soon</a>
                        </li>
                    </ul>
                </div>
                <div v-if="view === 'specialAnnouncement'" class="tab-content">
                    <div role="tabpanel" class="popup-element zooflash-special-announcement-element">
                        <div class="modal-body" style="height: calc(90vh - 170px)">
                            <div :class="'panel panel-' + (popup.specialAnnouncement.admin_only ? 'danger ' : 'primary ') + popup.specialAnnouncement.css_class" style="max-height: 100%; overflow:auto;">
                                <div class="panel-heading">
                                    <div v-if="popup.specialAnnouncement.admin_only" class="glyphicon glyphicon-star" style="margin-right: 10px; float: left"></div>
                                    <div style="float:left">
                                        <div v-if="popup.specialAnnouncement.link">
                                            <a :href="popup.specialAnnouncement.link" target="_blank">
                                                <h3 class="panel-title">${ popup.specialAnnouncement.name }</h3>
                                            </a>
                                        </div>
                                        <div v-else>
                                            <h3 class="panel-title">${ popup.specialAnnouncement.name }</h3>
                                        </div>
                                    </div>
                                    <div style="clear: both"></div>
                                </div>
                                <div class="panel-body" v-html="popup.specialAnnouncement.body"></div>
                            </div>
                        </div>
                        <div class="modal-footer" style="padding: 0px; border-top: 0;">
                            <div style="border-top: 1px solid #e5e5e5; width: 100%">
                                <div style="margin: 15px; padding-top: 15px;">
                                    <button type="button" class="btn btn-sm btn-default" v-on:click="hide">Hide for Now</button>
                                    <button type="button" class="btn btn-sm btn-default" v-on:click="markAsRead">Mark as Read</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-if="view === 'whatsnew' || view === 'comingsoon'" class="popup-element zooflash-comingsoon-element active">
                    <div class="modal-body" style="height: calc(90vh - 170px)">
                        <div class="panel panel-primary modal-active" style="max-height: 100%; overflow:auto;">
                            <div class="panel-heading">
                                <h3 class="panel-title">Version ${ version.version_number }</h3>
                            </div>
                            <div class="panel-body">
                                <h4 v-if="view === 'whatsnew'">${ formatDate(version.release_date) }</h4>
                                <span v-if="view === 'whatsnew'" class="release-note-description" v-html="version.release_note"></span>
                                <div v-for="release_note in version.release_notes" :class="'panel panel-' + (release_note.admin_only ? 'danger' : 'info')">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <div v-if="release_note.admin_only" class="glyphicon glyphicon-star" style="margin-right: 10px; float: left"></div>
                                            <div style="float: left">${ release_note.title }</div>
                                            <div style="clear:both"></div>
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        <p class="release-note-description" v-html="release_note.description"></p>
                                        <div v-if="release_note.thumbnail_image">
                                            <img :src="release_note.thumbnail_image" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="padding: 0px; border-top: 0">
                        <div style="border-top: 1px solid #e5e5e5; width: 100%">
                            <div style="margin: 15px; padding-top: 15px;">
                                <div v-if="view === 'whatsnew' && versions.length > 1" style="display: inline-block">
                                    <button v-if="index === lastIndex" type="button" class="btn btn-sm btn-default" disabled>
                                        Oldest Release
                                    </button>
                                    <button v-else type="button" class="btn btn-sm btn-default" v-on:click="page(1)">
                                        Previous Release
                                    </button>
                                    <button v-if="index === 0" type="button" class="btn btn-sm btn-default" disabled>
                                        Latest Release
                                    </button>
                                    <button v-else type="button" class="btn btn-sm btn-default" v-on:click="page(-1)">
                                        Next Release
                                    </button>
                                </div>
                                <div style="display: inline-block">
                                    <button type="button" class="btn btn-sm btn-default" v-on:click="hide">Hide for Now</button>
                                    <button type="button" class="btn btn-sm btn-default" v-on:click="markAsRead">Mark as Read</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</script>
<div class="zooflash " id="zooflash-root" style="display: none">
<transition name="zfade">
<div class="zooflash-disable-page-overlay" v-if="pageDisable.issues.length &gt; 0">
<div class="zooflash-disable-page-content">
<h5 v-if="pageDisable.issues.length === 1">${ pageDisable.issues[0].title }</h5>
<h5 v-else="">We have encountered some problems</h5>
<hr/>
<div>
<p :key="issue.id" v-for="issue in pageDisable.issues">${ issue.text }</p>
</div>
</div>
</div>
</transition>
<transition name="zfade">
<div class="announcements" v-if="announcements.length">
<span class="dismiss-all" v-on:click="dismissAllAnnouncements()">Dismiss all</span>
<div class="zooflash-list-of-announcements">
<transition-group name="zlist" tag="div">
<z-announcement :an="an" :key="an.id" v-for="an in announcements"></z-announcement>
</transition-group>
</div>
</div>
</transition>
<transition name="zfade">
<z-changelog :versions="versions" v-if="show.changeLog"></z-changelog>
</transition>
<transition name="zfade">
<z-popup :popup="popup" v-if="show.popup"></z-popup>
</transition>
</div>
<script>
var zooflashEndpoints = {};
zooflashEndpoints.ANNOUNCEMENTS_GET_ALL = "/zooflash/api/announcement/get/all";
zooflashEndpoints.ANNOUNCEMENTS_GET_SPECIAL = "/zooflash/api/announcement/get/special";
zooflashEndpoints.ANNOUNCEMENTS_GET = "/zooflash/api/announcement/get/0";
zooflashEndpoints.ANNOUNCEMENTS_DISMISS_ALL = "/zooflash/api/announcement/dismiss/all";
zooflashEndpoints.ANNOUNCEMENTS_DISMISS = "/zooflash/api/announcement/dismiss/0";
zooflashEndpoints.VERSIONS_GET_ALL = "/zooflash/api/softwareversion/get/all";
zooflashEndpoints.WHATSNEW_GET_ALL = "/zooflash/api/whatsnew/get/all";
zooflashEndpoints.WHATSNEW_MARK_AS_READ = "/zooflash/api/whatsnew/mark-as-read";
zooflashEndpoints.COMINGSOON_GET_ALL = "/zooflash/api/comingsoon/get/all";
zooflashEndpoints.COMINGSOON_MARK_AS_READ = "/zooflash/api/comingsoon/mark-as-read/0";
zooflashEndpoints.GET_ALL = "/zooflash/api/user/get/all";
zooflashEndpoints.DISMISS_ALL = "/zooflash/api/user/dismiss/all";
zooflashEndpoints.SERVER_CHECK = "/json_server_check";
var ZOOFLASH_VUE_URL = "https://hub-hachette.s3.amazonaws.com/static/zooflash/js/vue.min.js";
var ZOOFLASH_APP_VERSION = "";

var ZOOFLASH_FIRST_REQUEST_DELAY = 0;

var ZOOFLASH_REFRESH_TIMER = 0;

var ZOOFLASH_SERVER_CHECK_TIMER = 0;
</script>
<script src="https://hub-hachette.s3.amazonaws.com/static/zooflash/js/zooflash.js?zooflash="></script>
<link href="https://hub-hachette.s3.amazonaws.com/static/zooflash/css/zooflash.css?zooflash=" rel="stylesheet"/>
</body>
</html>
