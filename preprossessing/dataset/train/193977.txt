<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Punch Recipes at Bar None</title>
<meta content="Lots of punch recipes, easy to print and easier to drink.  Recipes include links to ingredient descriptions and other similar drinks." name="Description"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- Bootstrap CSS-->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Google fonts - Vidaloka and Poppins-->
<link href="https://fonts.googleapis.com/css?family=Vidaloka|Poppins:300,400,500" rel="stylesheet"/>
<!-- Bootstrap Select-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css" rel="stylesheet"/>
<!-- Fancy Box-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.1/jquery.fancybox.min.css" rel="stylesheet"/>
<!-- Font Awesome CSS-->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<!-- Custom Font Icons CSS-->
<!-- <link rel="stylesheet" href="/theme/css/custom-fonticons.css"> -->
<!-- BN stylesheet - for your changes-->
<link href="/theme/css/barnone.css" rel="stylesheet"/>
<!-- Favicon-->
<link href="https://www.barnonedrinks.com/favicon.ico" rel="icon"/>
<!-- Google Ads = DFP -->
<script async="async" src="https://www.googletagservices.com/tag/js/gpt.js"></script>
<script>
      var googletag = googletag || {};
      googletag.cmd = googletag.cmd || [];
    </script>
<script>
      googletag.cmd.push(function() {
        googletag.defineSlot('/1017476/SSLLeaderboard', [728, 90], 'div-gpt-ad-1514679710045-0').addService(googletag.pubads());
        googletag.defineSlot('/1017476/SSLSkyscraper', [160, 600], 'div-gpt-ad-1514679710045-1').addService(googletag.pubads());
        googletag.defineSlot('/1017476/barnone_product_feature_160x223', [160, 223], 'div-gpt-ad-1514679710045-2').addService(googletag.pubads());
        googletag.defineSlot('/1017476/SSLCube', [336, 280], 'div-gpt-ad-1514679710045-3').addService(googletag.pubads());
        googletag.defineSlot('/1017476/SSLLeaderboardFooter', [728, 90], 'div-gpt-ad-1514679710045-4').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
      });
    </script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-147538-1"></script>
<script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-147538-1');
    </script>
</head>
<body>
<!-- Main Navbar-->
<header class="header">
<nav class="navbar navbar-expand-lg">
<div class="container">
<!-- Navbar Brand --><a class="navbar-brand" href="/"><img alt="Bar None Drinks" src="/theme/img/logo.png"/></a>
<!-- Toggle Button-->
<button aria-controls="navbarcollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler d-print-none" data-target="#navbarcollapse" data-toggle="collapse" type="button"><span>Menu</span><i class="fa fa-bars"></i></button>
<!-- Navbar Menu -->
<div class="collapse navbar-collapse d-print-none" id="navbarcollapse">
<ul class="navbar-nav ml-auto">
<li class="nav-item dropdown"><a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="/drinks/" id="navbarDrinks">Drinks</a>
<div aria-labelledby="navbarDrinks" class="dropdown-menu">
<a class="dropdown-item" href="/drinks/by_category/">By Category</a>
<a class="dropdown-item" href="/drinks/by_ingredient/">By Ingredient</a>
<a class="dropdown-item" href="/drinks/top_ingredients/">Top Ingredients</a>
<a class="dropdown-item" href="/drinks/random/">Random</a>
<a class="dropdown-item" href="/drinks/recent/">Newly Added</a>
</div>
</li>
<li class="nav-item dropdown"><a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="/games/" id="navbarGames">Games</a>
<div aria-labelledby="navbarGames" class="dropdown-menu">
<a class="dropdown-item" href="/games/by_category/">By Category</a>
<a class="dropdown-item" href="/games/random/">Random</a>
</div>
</li>
<li class="nav-item dropdown"><a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="/tips/" id="navbarTips">Tips</a>
<div aria-labelledby="navbarTips" class="dropdown-menu">
<a class="dropdown-item" href="/tips/dictionary/">Ingredient Dictionary</a>
<a class="dropdown-item" href="/tips/techniques/">Techniques</a>
<a class="dropdown-item" href="/tips/equipping/">Equipping</a>
<a class="dropdown-item" href="/tips/reference/">Reference</a>
</div>
</li>
<li class="nav-item dropdown"><a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="/tips/articles/" id="navbarArticles">Articles</a>
<div aria-labelledby="navbarArticles" class="dropdown-menu">
<a class="dropdown-item" href="/tips/articles/columns.html">Columns</a>
<a class="dropdown-item" href="/tips/reviews/">Reviews and Interviews</a>
<a class="dropdown-item" href="/forums/">Forum Posts</a>
<a class="dropdown-item" href="/press/">Industry News</a>
</div>
</li>
</ul>
</div>
<script>
            (function() {
              var cx = '015727763658184789878:n5loqqstfnm';
              var gcse = document.createElement('script');
              gcse.type = 'text/javascript';
              gcse.async = true;
              gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
              var s = document.getElementsByTagName('script')[0];
              s.parentNode.insertBefore(gcse, s);
            })();
          </script>
<div class="bnsearch d-print-none">
<gcse:searchbox-only></gcse:searchbox-only>
</div>
</div>
</nav>
</header>
<div class="bnbanner-outer bngray d-print-none">
<!-- /1017476/SSLLeaderboard -->
<div class="bnbanner-inner" id="div-gpt-ad-1514679710045-0">
<script>
      googletag.cmd.push(function() { googletag.display('div-gpt-ad-1514679710045-0'); });
      </script>
</div>
</div>
<div class="container">
<div class="row">
<main class="col-lg-9">
<ul class="breadcrumb d-print-none">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item"><a href="/drinks/">Drinks</a></li>
<li class="breadcrumb-item"><a href="/drinks/by_category/">By Category</a></li>
<li class="breadcrumb-item active">Punches</li>
</ul>
<section class="content">
<div class="bnd-c-nav">
<a href="/drinks/by_category/cocktails-2/">Cocktails</a> | <a href="/drinks/by_category/hot-drinks-1/">Hot Drinks</a> | <a href="/drinks/by_category/jello-shots-8/">Jello Shots</a> | <a href="/drinks/by_category/martinis-7/">Martinis</a> | <a href="/drinks/by_category/non-alcoholic-3/">Non-Alcoholic</a> | <a href="/drinks/by_category/punches-4/">Punches</a> | <a href="/drinks/by_category/shooters-5/">Shooters</a>
</div>
<div class="bnd-c-nav">
<a href="/drinks/by_category/punches-4/-.html">#</a> | <a href="/drinks/by_category/punches-4/a.html">A</a> | <a href="/drinks/by_category/punches-4/b.html">B</a> | <a href="/drinks/by_category/punches-4/c.html">C</a> | <a href="/drinks/by_category/punches-4/d.html">D</a> | <a href="/drinks/by_category/punches-4/e.html">E</a> | <a href="/drinks/by_category/punches-4/f.html">F</a> | <a href="/drinks/by_category/punches-4/g.html">G</a> | <a href="/drinks/by_category/punches-4/h.html">H</a> | <a href="/drinks/by_category/punches-4/i.html">I</a> | <a href="/drinks/by_category/punches-4/j.html">J</a> | <a href="/drinks/by_category/punches-4/k.html">K</a> | <a href="/drinks/by_category/punches-4/l.html">L</a> | <a href="/drinks/by_category/punches-4/m.html">M</a> | <a href="/drinks/by_category/punches-4/n.html">N</a> | <a href="/drinks/by_category/punches-4/o.html">O</a> | <a href="/drinks/by_category/punches-4/p.html">P</a> | <a href="/drinks/by_category/punches-4/q.html">Q</a> | <a href="/drinks/by_category/punches-4/r.html">R</a> | <a href="/drinks/by_category/punches-4/s.html">S</a> | <a href="/drinks/by_category/punches-4/t.html">T</a> | <a href="/drinks/by_category/punches-4/u.html">U</a> | <a href="/drinks/by_category/punches-4/v.html">V</a> | <a href="/drinks/by_category/punches-4/w.html">W</a> | <a href="/drinks/by_category/punches-4/y.html">Y</a> | <a href="/drinks/by_category/punches-4/z.html">Z</a>
</div>
<div class="bnd-c-text">
<h2>Punches</h2>
<div class="bnd-c-text-sect">
<p></p><p class="bnd-c-text-sect">
So you've got a large party to serve up some refreshments to.  Although you can really scale almost any recipe, the ones found here are generally suited for serving the masses.  Be warned though, some punches cover the taste of alcohol and can sneak up on your un-suspecting recipients.
</p>
<p class="bnd-c-text-sect">
A punch is actually quite a general term for several types of beverages.  While the word may sometimes be used to describe non-alcoholic beverages, it is often used when referring to mixed drinks, as well as to other beverages that contain alcohol.
</p>
<p class="bnd-c-text-sect">
Punches are usually created by combining various ingredients with fruit juice and/or fruit.  It is a drink that is most often made in large batches and served in a punch bowl to accommodate big crowds at a party, get together, or function.
</p>
<p class="bnd-c-text-sect">
There are two versions of where punch may have originated.
</p>
<p class="bnd-c-text-sect">
Some say that the word itself is actually Hindi.  Panch, which means five in Hindi, was a drink created by combining five ingredients, usually lemon, sugar, water, spices or tea, and arrack.
</p>
<p class="bnd-c-text-sect">  
British East India Company sailors brought the drink back with them when they returned to England.  The drink later became popular in other countries in Europe.
</p>
<p class="bnd-c-text-sect">
An alternate version has the word punch derived from the word puncheon.  A puncheon is a cask that is able to hold an astounding 72 gallons.  The large vessel could easily be transformed into a punch bowl.
</p>
<p class="bnd-c-text-sect">
Regardless of where the word first originated, it is known that the term can be traced back to 1632, where it was written in British records.  Punches with a brandy or wine base, or those of the popular Wassail type, where well known at that time.
</p>
<p class="bnd-c-text-sect">  
Just a few short years later, in 1655, rum became popular in Jamaica, and so did modern punch.  Punch houses were being referred to by the year 1671.
</p>
<p><b><i>Please start by choosing a letter at the top of the page, or try one of the examples below.</i></b>
</p></div>
<h3>Example Punches</h3>
<div class="bnd-c-text-sect">
<dl>
<dt><a href="/drinks/a/anti-freeze-3-7758.html" title="Anti-Freeze #3 drink recipe"><b>Anti-Freeze #3</b></a></dt>
<dd>Everclear, Maui Blue Hawaiian Schnapps, Orange Juice</dd>
<dt><a href="/drinks/a/antifreeze-7821.html" title="Antifreeze drink recipe"><b>Antifreeze</b></a></dt>
<dd>Everclear, Lemonade Mix, Mountain Dew</dd>
<dt><a href="/drinks/c/citrus-berry-punch-6367.html" title="Citrus Berry Punch drink recipe"><b>Citrus Berry Punch</b></a></dt>
<dd>Canadian Mist, Club Soda, Cranberry Juice, Lime</dd>
<dt><a href="/drinks/h/hop-skip-and-go-naked-9397.html" title="Hop-Skip and Go-Naked drink recipe"><b>Hop-Skip and Go-Naked</b></a></dt>
<dd>Beer, Lemon Lime Soda, Lemonade Mix, Vodka, White Rum</dd>
<dt><a href="/drinks/w/watermelon-margarita-6744.html" title="Watermelon Margarita drink recipe"><b>Watermelon Margarita</b></a></dt>
<dd>Lime Juice, Orange Liqueur, Tequila, Watermelon</dd>
</dl>
</div>
</div>
</section>
</main>
<!-- Sidebar -->
<aside class="col-lg-3 d-print-none">
<div class="widget">
<header>
<h3 class="has-lines"><small>Advertisement</small></h3>
</header>
<!-- /1017476/SSLSkyscraper -->
<div id="div-gpt-ad-1514679710045-1">
<script>
              googletag.cmd.push(function() { googletag.display('div-gpt-ad-1514679710045-1'); });
              </script>
</div>
</div>
<div class="widget">
<header>
<h3 class="has-lines"><small>Advertisement</small></h3>
</header>
<!-- /1017476/barnone_product_feature_160x223 -->
<div id="div-gpt-ad-1514679710045-2">
<script>
              googletag.cmd.push(function() { googletag.display('div-gpt-ad-1514679710045-2'); });
              </script>
</div>
</div>
</aside>
</div>
</div>
<div class="bnbanner-outer bngray d-print-none">
<!-- /1017476/SSLLeaderboard -->
<div class="bnbanner-inner" id="div-gpt-ad-1514679710045-4">
<script>
      googletag.cmd.push(function() { googletag.display('div-gpt-ad-1514679710045-4'); });
      </script>
</div>
</div>
<!-- Page Footer-->
<footer class="main-footer">
<div class="container">
<div class="row">
<div class="contact col-md-6">
<h3>Contact us</h3>
<div class="info">
<p>329 Nottingham Drive 
              </p><p>Nanaimo BC Canada V9T 4T1 
              </p><p>Phone/Fax: (815)301-8807
            </p></div>
</div>
<div class="site-links col-md-3">
<h3>Useful links</h3>
<div class="menus d-flex">
<ul class="list-unstyled">
<li><a href="/info/advertising/">Advertising</a></li>
<li><a href="/info/">Site Info</a></li>
<li><a href="/submit/">Email Us</a></li>
</ul>
</div>
</div>
<div class="site-links col-md-3">
<h3>Legal stuff</h3>
<div class="menus d-flex">
<ul class="list-unstyled">
<li><a href="/info/legal/copyright.html">Copyright</a></li>
<li><a href="/info/legal/disclaimer.html">Disclaimer</a></li>
<li><a href="/info/legal/privacy.html">Privacy Policy</a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="copyrights text-center">
<p>© <span class="text-primary">Bar None Drinks</span> - All Rights Reserved.</p>
</div>
</footer>
<!-- Javascript files-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"> </script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.1/jquery.fancybox.min.js"></script>
<script src="/theme/js/front.js"></script>
</body>
</html>