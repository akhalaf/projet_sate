<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en">
<![endif]--><!--[if IE 7]>
<html id="ie7" lang="en">
<![endif]--><!--[if IE 8]>
<html id="ie8" lang="en">
<![endif]--><!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!--><html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<link href="https://www.rwbcenter.com/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
	<script src="https://www.rwbcenter.com/wp-content/themes/Divi/js/html5.js" type="text/javascript"></script>
	<![endif]-->
<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>
<title>404 Not Found | Reprieve Well-Being Center</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.rwbcenter.com/feed/" rel="alternate" title="Reprieve Well-Being Center » Feed" type="application/rss+xml"/>
<link href="https://www.rwbcenter.com/comments/feed/" rel="alternate" title="Reprieve Well-Being Center » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.rwbcenter.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.8.3"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b!==c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<meta content="Divi v.2.7.3" name="generator"/><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,latin-ext" id="divi-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rwbcenter.com/wp-content/themes/Divi/style.css?ver=2.7.3" id="divi-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rwbcenter.com/wp-content/themes/Divi/epanel/shortcodes/css/shortcodes.css?ver=2.7.3" id="et-shortcodes-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rwbcenter.com/wp-content/themes/Divi/epanel/shortcodes/css/shortcodes_responsive.css?ver=2.7.3" id="et-shortcodes-responsive-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rwbcenter.com/wp-content/themes/Divi/includes/builder/styles/magnific_popup.css?ver=2.7.3" id="magnific-popup-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.rwbcenter.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.rwbcenter.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.rwbcenter.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.rwbcenter.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.rwbcenter.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.8.3" name="generator"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/> <style id="theme-customizer-css">
																	a { color: #754c76; }
							#main-header, #main-header .nav li ul, .et-search-form, #main-header .et_mobile_menu { background-color: #754c76; }
											#top-header, #et-secondary-nav li ul { background-color: #718061; }
													.et_header_style_centered .mobile_nav .select_page, .et_header_style_split .mobile_nav .select_page, .et_nav_text_color_light #top-menu > li > a, .et_nav_text_color_dark #top-menu > li > a, #top-menu a, .et_mobile_menu li a, .et_nav_text_color_light .et_mobile_menu li a, .et_nav_text_color_dark .et_mobile_menu li a, #et_search_icon:before, .et_search_form_container input, span.et_close_search_field:after, #et-top-navigation .et-cart-info { color: #ffffff; }
			.et_search_form_container input::-moz-placeholder { color: #ffffff; }
			.et_search_form_container input::-webkit-input-placeholder { color: #ffffff; }
			.et_search_form_container input:-ms-input-placeholder { color: #ffffff; }
											#top-menu li a { font-size: 16px; }
			body.et_vertical_nav .container.et_search_form_container .et-search-form input { font-size: 16px !important; }
		
					#top-menu li a, .et_search_form_container input {
									font-weight: normal; font-style: normal; text-transform: uppercase; text-decoration: none; 													letter-spacing: 1px;
							}

			.et_search_form_container input::-moz-placeholder {
									font-weight: normal; font-style: normal; text-transform: uppercase; text-decoration: none; 													letter-spacing: 1px;
							}
			.et_search_form_container input::-webkit-input-placeholder {
									font-weight: normal; font-style: normal; text-transform: uppercase; text-decoration: none; 													letter-spacing: 1px;
							}
			.et_search_form_container input:-ms-input-placeholder {
									font-weight: normal; font-style: normal; text-transform: uppercase; text-decoration: none; 													letter-spacing: 1px;
							}
		
					#top-menu li.current-menu-ancestor > a, #top-menu li.current-menu-item > a,
			.et_color_scheme_red #top-menu li.current-menu-ancestor > a, .et_color_scheme_red #top-menu li.current-menu-item > a,
			.et_color_scheme_pink #top-menu li.current-menu-ancestor > a, .et_color_scheme_pink #top-menu li.current-menu-item > a,
			.et_color_scheme_orange #top-menu li.current-menu-ancestor > a, .et_color_scheme_orange #top-menu li.current-menu-item > a,
			.et_color_scheme_green #top-menu li.current-menu-ancestor > a, .et_color_scheme_green #top-menu li.current-menu-item > a { color: #ffffff; }
																#footer-bottom { background-color: #754c76; }#footer-info, #footer-info a { color: #754c76; }										
		
																														
		@media only screen and ( min-width: 981px ) {
																			.et_header_style_left #et-top-navigation, .et_header_style_split #et-top-navigation  { padding: 43px 0 0 0; }
				.et_header_style_left #et-top-navigation nav > ul > li > a, .et_header_style_split #et-top-navigation nav > ul > li > a { padding-bottom: 43px; }
				.et_header_style_split .centered-inline-logo-wrap { width: 85px; margin: -85px 0; }
				.et_header_style_split .centered-inline-logo-wrap #logo { max-height: 85px; }
				.et_pb_svg_logo.et_header_style_split .centered-inline-logo-wrap #logo { height: 85px; }
				.et_header_style_centered #top-menu > li > a { padding-bottom: 15px; }
				.et_header_style_slide #et-top-navigation, .et_header_style_fullscreen #et-top-navigation { padding: 34px 0 34px 0 !important; }
									.et_header_style_centered #main-header .logo_container { height: 85px; }
														#logo { max-height: 70%; }
				.et_pb_svg_logo #logo { height: 70%; }
																.et_header_style_centered.et_hide_primary_logo #main-header:not(.et-fixed-header) .logo_container, .et_header_style_centered.et_hide_fixed_logo #main-header.et-fixed-header .logo_container { height: 15.3px; }
																.et-fixed-header#top-header, .et-fixed-header#top-header #et-secondary-nav li ul { background-color: #718061; }
													.et-fixed-header #top-menu li a { font-size: 16px; }
										.et-fixed-header #top-menu a, .et-fixed-header #et_search_icon:before, .et-fixed-header #et_top_search .et-search-form input, .et-fixed-header .et_search_form_container input, .et-fixed-header .et_close_search_field:after, .et-fixed-header #et-top-navigation .et-cart-info { color: #ffffff !important; }
				.et-fixed-header .et_search_form_container input::-moz-placeholder { color: #ffffff !important; }
				.et-fixed-header .et_search_form_container input::-webkit-input-placeholder { color: #ffffff !important; }
				.et-fixed-header .et_search_form_container input:-ms-input-placeholder { color: #ffffff !important; }
										.et-fixed-header #top-menu li.current-menu-ancestor > a,
				.et-fixed-header #top-menu li.current-menu-item > a { color: #ffffff !important; }
						
					}
		@media only screen and ( min-width: 1350px) {
			.et_pb_row { padding: 27px 0; }
			.et_pb_section { padding: 54px 0; }
			.single.et_pb_pagebuilder_layout.et_full_width_page .et_post_meta_wrapper { padding-top: 81px; }
			.et_pb_section.et_pb_section_first { padding-top: inherit; }
			.et_pb_fullwidth_section { padding: 0; }
		}
		@media only screen and ( max-width: 980px ) {
																				}
		@media only screen and ( max-width: 767px ) {
														}
	</style>
<style id="module-customizer-css">
</style>
<link href="http://www.rwbcenter.com/wp-content/uploads/2016/06/favicon.jpg" rel="shortcut icon"/> <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<style id="et-custom-css" type="text/css">
.entry-content .et_pb_row_0 {
padding-top: 0 !important;
}

.entry-content .et_pb_section_0 {
padding-top: 0 !important;
}
</style></head>
<body class="error404 et_pb_button_helper_class et_fixed_nav et_show_nav et_cover_background et_secondary_nav_enabled et_pb_gutter et_pb_gutters et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_right_sidebar unknown">
<div id="page-container">
<div id="top-header">
<div class="container clearfix">
<div id="et-info">
<span id="et-info-phone">(248) 229-2884</span>
<a href="mailto:drkathymorrow@rwbcenter.com"><span id="et-info-email">drkathymorrow@rwbcenter.com</span></a>
</div> <!-- #et-info -->
<div id="et-secondary-menu">
</div> <!-- #et-secondary-menu -->
</div> <!-- .container -->
</div> <!-- #top-header -->
<header data-height-onload="85" id="main-header">
<div class="container clearfix et_menu_container">
<div class="logo_container">
<span class="logo_helper"></span>
<a href="https://www.rwbcenter.com/">
<img alt="Reprieve Well-Being Center" data-height-percentage="70" id="logo" src="http://www.rwbcenter.com/wp-content/uploads/2016/06/logo-white.png"/>
</a>
</div>
<div data-fixed-height="40" data-height="85" id="et-top-navigation">
<nav id="top-menu-nav">
<ul class="nav" id="top-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-49" id="menu-item-49"><a href="https://www.rwbcenter.com/">Home</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-103" id="menu-item-103"><a href="/#services">Services</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-130" id="menu-item-130"><a href="/#team">Our Staff</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-104" id="menu-item-104"><a href="/#contact">Contact</a></li>
</ul> </nav>
<div id="et_mobile_nav_menu">
<div class="mobile_nav closed">
<span class="select_page">Select Page</span>
<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
</div>
</div> </div> <!-- #et-top-navigation -->
</div> <!-- .container -->
<div class="et_search_outer">
<div class="container et_search_form_container">
<form action="https://www.rwbcenter.com/" class="et-search-form" method="get" role="search">
<input class="et-search-field" name="s" placeholder="Search …" title="Search for:" type="search" value=""/> </form>
<span class="et_close_search_field"></span>
</div>
</div>
</header> <!-- #main-header -->
<div id="et-main-area">
<div id="main-content">
<div class="container">
<div class="clearfix" id="content-area">
<div id="left-area">
<article class="et_pb_post not_found" id="post-0">
<div class="entry">
<!--If no results are found-->
<h1>No Results Found</h1>
<p>The page you requested could not be found. Try refining your search, or use the navigation above to locate the post.</p>
</div>
<!--End if no results are found--> </article> <!-- .et_pb_post -->
</div> <!-- #left-area -->
<div id="sidebar">
<div class="et_pb_widget widget_search" id="search-2"><form action="https://www.rwbcenter.com/" class="searchform" id="searchform" method="get" role="search">
<div>
<label class="screen-reader-text" for="s">Search for:</label>
<input id="s" name="s" type="text" value=""/>
<input id="searchsubmit" type="submit" value="Search"/>
</div>
</form></div> <!-- end .et_pb_widget --> <div class="et_pb_widget widget_recent_entries" id="recent-posts-2"> <h4 class="widgettitle">Recent Posts</h4> <ul>
<li>
<a href="https://www.rwbcenter.com/uncategorized/hello-world/">Hello world!</a>
</li>
</ul>
</div> <!-- end .et_pb_widget --> <div class="et_pb_widget widget_recent_comments" id="recent-comments-2"><h4 class="widgettitle">Recent Comments</h4><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link"><a class="url" href="http://wordpress.org/" rel="external nofollow">Mr WordPress</a></span> on <a href="https://www.rwbcenter.com/uncategorized/hello-world/#comment-1">Hello world!</a></li></ul></div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_archive" id="archives-2"><h4 class="widgettitle">Archives</h4> <ul>
<li><a href="https://www.rwbcenter.com/2016/04/">April 2016</a></li>
</ul>
</div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_categories" id="categories-2"><h4 class="widgettitle">Categories</h4> <ul>
<li class="cat-item cat-item-1"><a href="https://www.rwbcenter.com/category/uncategorized/">Uncategorized</a>
</li>
</ul>
</div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_meta" id="meta-2"><h4 class="widgettitle">Meta</h4> <ul>
<li><a href="https://www.rwbcenter.com/wp-login.php">Log in</a></li>
<li><a href="https://www.rwbcenter.com/feed/">Entries <abbr title="Really Simple Syndication">RSS</abbr></a></li>
<li><a href="https://www.rwbcenter.com/comments/feed/">Comments <abbr title="Really Simple Syndication">RSS</abbr></a></li>
<li><a href="https://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress.org</a></li> </ul>
</div> <!-- end .et_pb_widget --> </div> <!-- end #sidebar -->
</div> <!-- #content-area -->
</div> <!-- .container -->
</div> <!-- #main-content -->
<span class="et_pb_scroll_top et-pb-icon"></span>
<footer id="main-footer">
<div id="footer-bottom">
<div class="container clearfix">
<p id="footer-info">Designed by <a href="http://www.elegantthemes.com" title="Premium WordPress Themes">Elegant Themes</a> | Powered by <a href="http://www.wordpress.org">WordPress</a></p>
</div> <!-- .container -->
</div>
</footer> <!-- #main-footer -->
</div> <!-- #et-main-area -->
</div> <!-- #page-container -->
<style id="et-builder-page-custom-style" type="text/css">
				 .et_pb_section { background-color: ; }
			</style><script src="https://www.rwbcenter.com/wp-content/themes/Divi/includes/builder/scripts/frontend-builder-global-functions.js?ver=2.7.3" type="text/javascript"></script>
<script src="https://www.rwbcenter.com/wp-content/themes/Divi/includes/builder/scripts/jquery.mobile.custom.min.js?ver=2.7.3" type="text/javascript"></script>
<script src="https://www.rwbcenter.com/wp-content/themes/Divi/js/custom.js?ver=2.7.3" type="text/javascript"></script>
<script src="https://www.rwbcenter.com/wp-content/themes/Divi/js/smoothscroll.js?ver=2.7.3" type="text/javascript"></script>
<script src="https://www.rwbcenter.com/wp-content/themes/Divi/includes/builder/scripts/jquery.fitvids.js?ver=2.7.3" type="text/javascript"></script>
<script src="https://www.rwbcenter.com/wp-content/themes/Divi/includes/builder/scripts/waypoints.min.js?ver=2.7.3" type="text/javascript"></script>
<script src="https://www.rwbcenter.com/wp-content/themes/Divi/includes/builder/scripts/jquery.magnific-popup.js?ver=2.7.3" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var et_pb_custom = {"ajaxurl":"https:\/\/www.rwbcenter.com\/wp-admin\/admin-ajax.php","images_uri":"https:\/\/www.rwbcenter.com\/wp-content\/themes\/Divi\/images","builder_images_uri":"https:\/\/www.rwbcenter.com\/wp-content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"4855f72501","subscription_failed":"Please, check the fields below to make sure you entered the correct information.","et_ab_log_nonce":"11de8ab272","fill_message":"Please, fill in the following fields:","contact_error_message":"Please, fix the following errors:","invalid":"Invalid email","captcha":"Captcha","prev":"Prev","previous":"Previous","next":"Next","wrong_captcha":"You entered the wrong number in captcha.","is_builder_plugin_used":"","is_divi_theme_used":"1","widget_search_selector":".widget_search","is_ab_testing_active":"","page_id":"","unique_test_id":"","ab_bounce_rate":"","is_cache_plugin_active":"no","is_shortcode_tracking":""};
/* ]]> */
</script>
<script src="https://www.rwbcenter.com/wp-content/themes/Divi/includes/builder/scripts/frontend-builder-scripts.js?ver=2.7.3" type="text/javascript"></script>
<script src="https://www.rwbcenter.com/wp-includes/js/wp-embed.min.js?ver=4.8.3" type="text/javascript"></script>
</body>
</html>