<!DOCTYPE html>
<html dir="ltr" itemscope="" itemtype="http://schema.org/Blog" lang="el" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8"/>
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>
    <script>
        //Custom Options (added to url options)
        $.fpOptions = {
            url: "http://www.pezoulia.gr/%CE%B5%CF%80%CE%B9%CE%BA%CE%BF%CE%B9%CE%BD%CF%89%CE%BD%CE%AF%CE%B1/",
            url_text: "Επικοινωνήστε μαζί μας",
            message: "<b><span  style='font-size:18px;'>Pezoulia.gr - Προσφορά Μαϊου 2016</span></b> -  3 διανυχτερεύσεις <br>για 2 άτομα <b>150€</b> <br> για 4 άτομα  <b> 240€</b><br><span style='font-size:10px;'>(στις τιμές δεν συμπεριλμβάνεται διατροφή.)</span>"
        };
    </script>
    <script type="text/javascript" id="FooterPopup" src="http://www.pezoulia.gr/wp-content/themes/pezoulia/static/js/footer-popup.js" async="true"></script>
-->
<title>Αρχική - Πεζούλια, Παραδοσιακές Κατοικίες, Διαμονή, Σελάκανο, Κρητη</title>
<link href="https://pezoulia.gr/wp-content/themes/pezoulia/style.css" rel="stylesheet"/>
<link href="http://pezoulia.gr/xmlrpc.php" rel="pingback"/>
<!-- All in One SEO Pack 2.2.7.2 by Michael Torbert of Semper Fi Web Design[976,1051] -->
<meta content="Τα καταλύματα Πεζούλια στον οικισμό του Σελακάνου σας περιμένουν να γευθείτε τον φυσικό πλούτο της περιοχής Μέσα στο Δάσος από πρίνους και πεύκα του" itemprop="description" name="description"/>
<link href="https://www.pezoulia.gr/" rel="canonical"/>
<meta content="Πεζούλια, Παραδοσιακές Κατοικίες, Διαμονή, Σελάκανο, Κρητη" property="og:title"/>
<meta content="blog" property="og:type"/>
<meta content="https://www.pezoulia.gr/" property="og:url"/>
<meta content="https://pezoulia.gr/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png" property="og:image"/>
<meta content="Πεζούλια, Παραδοσιακές Κατοικίες, Διαμονή, Σελάκανο, Κρητη" property="og:site_name"/>
<meta content="Τα καταλύματα Πεζούλια στον οικισμό του Σελακάνου σας περιμένουν να γευθείτε τον φυσικό πλούτο της περιοχής Μέσα στο Δάσος από πρίνους και πεύκα του Σελάκανου, κοντά σε πηγές, ποτάμια και φαράγγια, θα βρείτε την πολυτέλεια και την γαλήνη που αναζητάτε. Τα καταλύματα «Πεζούλια» λειτουργούν όλο το χρόνο και είναι στην διάθεσή σας για αξέχαστες διακοπές. Το συγκρότημα των παραδοσιακών ξενώνων και κατοικιών βρίσκεται μέσα στον οικισμό του Σελάκανου, πολύ κοντά στο δάσος και σε υψόμετρο 930 μ. Ιδανική αφετηρία για ημερήσιες εκδρομές στα γραφικά ορεινά της Δίκτης, αλλά και τις καυτές παραλίες του Νότου. Ένας ονειρεμένος προορισμός στην ενδοχώρα της Κρήτης με δροσερό καλοκαίρι, αλλά και ένα χειμωνιάτικο τοπίο που συγκλονίζει." property="og:description"/>
<meta content="summary" name="twitter:card"/>
<meta content="Τα καταλύματα Πεζούλια στον οικισμό του Σελακάνου σας περιμένουν να γευθείτε τον φυσικό πλούτο της περιοχής Μέσα στο Δάσος από πρίνους και πεύκα του Σελάκανου, κοντά σε πηγές, ποτάμια και φαράγγια, θα βρείτε την πολυτέλεια και την γαλήνη που αναζητάτε. Τα καταλύματα «Πεζούλια» λειτουργούν όλο το χρόνο και είναι στην διάθεσή σας για αξέχαστες διακοπές. Το συγκρότημα των παραδοσιακών ξενώνων και κατοικιών βρίσκεται μέσα στον οικισμό του Σελάκανου, πολύ κοντά στο δάσος και σε υψόμετρο 930 μ. Ιδανική αφετηρία για ημερήσιες εκδρομές στα γραφικά ορεινά της Δίκτης, αλλά και τις καυτές παραλίες του Νότου. Ένας ονειρεμένος προορισμός στην ενδοχώρα της Κρήτης με δροσερό καλοκαίρι, αλλά και ένα χειμωνιάτικο τοπίο που συγκλονίζει." name="twitter:description"/>
<meta content="https://pezoulia.gr/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png" itemprop="image"/>
<!-- /all in one seo pack -->
<link href="https://www.pezoulia.gr/feed/" rel="alternate" title="Κανάλι RSS » Πεζούλια, Παραδοσιακές Κατοικίες, Διαμονή, Σελάκανο, Κρητη" type="application/rss+xml"/>
<link href="https://www.pezoulia.gr/comments/feed/" rel="alternate" title="Κανάλι σχολίων » Πεζούλια, Παραδοσιακές Κατοικίες, Διαμονή, Σελάκανο, Κρητη" type="application/rss+xml"/>
<link href="https://www.pezoulia.gr/%ce%b1%cf%81%cf%87%ce%b9%ce%ba%ce%ae/feed/" rel="alternate" title="Κανάλι σχολίων αρχική » Πεζούλια, Παραδοσιακές Κατοικίες, Διαμονή, Σελάκανο, Κρητη" type="application/rss+xml"/>
<link href="https://pezoulia.gr/wp-content/plugins/wp-flexible-map/css/styles.css?ver=1.10.0" id="flxmap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://pezoulia.gr/wp-content/plugins/lightbox-plus/css/shadowed/colorbox.css?ver=2.0.2" id="lightboxStyle-css" media="screen" rel="stylesheet" type="text/css"/>
<script src="https://pezoulia.gr/wp-includes/js/l10n.js?ver=20101110" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js?ver=3.2.1" type="text/javascript"></script>
<script src="https://pezoulia.gr/wp-content/plugins/dropdown-menu-widget/scripts/include.js?ver=3.2.1" type="text/javascript"></script>
<script src="https://pezoulia.gr/wp-content/plugins/dropdown-menu-widget/scripts/hoverIntent.js?ver=3.2.1" type="text/javascript"></script>
<script src="https://pezoulia.gr/wp-includes/js/comment-reply.js?ver=20090102" type="text/javascript"></script>
<link href="https://www.pezoulia.gr/" rel="index" title="Πεζούλια, Παραδοσιακές Κατοικίες, Διαμονή, Σελάκανο, Κρητη"/>
<link href="https://www.pezoulia.gr/%ce%b4%cf%89%ce%bc%ce%ac%cf%84%ce%b9%ce%b1/" rel="next" title="δωμάτια"/>
<!-- Booking Calendar Scripts --> <script type="text/javascript">
    var jWPDev = jQuery.noConflict();
    var wpdev_bk_plugin_url = 'http://pezoulia.gr/wp-content/plugins/BookingCalendarPro';
    var wpdev_bk_today = new Array( parseInt(2021),  parseInt(01),  parseInt(14),  parseInt(06),  parseInt(16)  );
    var visible_booking_id_on_page = [];
    var booking_max_monthes_in_calendar = '1y';
    var user_unavilable_days = [];
                                var wpdev_bk_edit_id_hash = '';
                                            // Check for correct URL based on Location.href URL, its need for correct aJax request
                var real_domain = window.location.href;
                var start_url = '';
                var pos1 = real_domain.indexOf('//'); //get http
                if (pos1 > -1 ) { start_url= real_domain.substr(0, pos1+2); real_domain = real_domain.substr(pos1+2);   }  //set without http
                real_domain = real_domain.substr(0, real_domain.indexOf('/') );    //setdomain
                var pos2 = wpdev_bk_plugin_url.indexOf('//');  //get http
                if (pos2 > -1 ) wpdev_bk_plugin_url = wpdev_bk_plugin_url.substr(pos2+2);    //set without http
                wpdev_bk_plugin_url = wpdev_bk_plugin_url.substr( wpdev_bk_plugin_url.indexOf('/') );    //setdomain
                wpdev_bk_plugin_url = start_url + real_domain + wpdev_bk_plugin_url;
                ///////////////////////////////////////////////////////////////////////////////////////

                var wpdev_bk_plugin_filename = 'wpdev-booking.php';
                                var multiple_day_selections = 50;
                                    var wpdev_bk_pro =1;
                var wpdev_bk_is_dynamic_range_selection = false;
                var message_verif_requred = 'This field is required';
                var message_verif_requred_for_check_box = 'This checkbox must be checked';
                var message_verif_emeil = 'Incorrect email field';
                var message_verif_selectdts = 'Please, select reservation date(s) at Calendar.';
                var parent_booking_resources = [];

                var new_booking_title= 'Thank you for your online reservation. <br/> We will send confirmation of your booking as soon as possible.';
                var new_booking_title_time= 7000;
                var type_of_thank_you_message = 'message';
                var thank_you_page_URL = 'http://localhost/pezoulia/wordpress';
                var is_am_pm_inside_time = false;
                </script>
<script type="text/javascript">
                        var message_time_error = 'Uncorrect date format';
                    </script>
<script type="text/javascript">
                        var days_select_count= 3;
                        var range_start_day= -1;
                        var days_select_count_dynamic= 1;
                        var range_start_day_dynamic= -1;
                                                    var is_select_range = 0;
                                                var message_starttime_error = 'Start Time is invalid, probably by requesting time(s) already booked, or already in the past!';
                        var message_endtime_error   =   'End Time is invalid, probably by requesting time(s) already booked, or already in the past, or less then start time if only 1 day selected.!';
                        var message_rangetime_error   =   'Probably by requesting time(s) already booked, or already in the past!';
                        var message_durationtime_error   =   'Probably by requesting time(s) already booked, or already in the past!';
                    </script>
<script type="text/javascript">
                        var bk_cost_depends_from_selection_line1 = 'EUR per 1 day';
                        var bk_cost_depends_from_selection_line2 = '% from the cost of 1 day ';
                        var bk_cost_depends_from_selection_line3 = 'Additional cost in EUR per 1 day';

                        var bk_cost_depends_from_selection_line14summ = 'EUR  for all days!';
                        var bk_cost_depends_from_selection_line24summ = '%  for all days!';

                    </script>
<script type="text/javascript">
                             var message_verif_visitors_more_then_available   =   'Probably number of selected visitors more, then available at selected day(s)!';
                                            var is_use_visitors_number_for_availability =  false;
                
                                                </script>
<script src="http://pezoulia.gr/wp-content/plugins/BookingCalendarPro/js/datepick/jquery.datepick.js" type="text/javascript"></script> <script src="http://pezoulia.gr/wp-content/plugins/BookingCalendarPro/js/datepick/jquery.datepick-el.js" type="text/javascript"></script> <script src="http://pezoulia.gr/wp-content/plugins/BookingCalendarPro/js/wpdev.bk.js" type="text/javascript"></script> <script src="http://pezoulia.gr/wp-content/plugins/BookingCalendarPro/js/tooltip/tools.tooltip.min.js" type="text/javascript"></script> <script src="http://pezoulia.gr/wp-content/plugins/BookingCalendarPro/include/js/jquery.meio.mask.min.js" type="text/javascript"></script> <script src="http://pezoulia.gr/wp-content/plugins/BookingCalendarPro/include/js/wpdev.bk.pro.js" type="text/javascript"></script> <script src="http://pezoulia.gr/wp-content/plugins/BookingCalendarPro/include/js/wpdev.bk.premium.js" type="text/javascript"></script> <script src="http://pezoulia.gr/wp-content/plugins/BookingCalendarPro/include/js/wpdev.bk.premiumplus.js" type="text/javascript"></script> <script src="http://pezoulia.gr/wp-content/plugins/BookingCalendarPro/include/js/wpdev.bk.hotel.js" type="text/javascript"></script> <!-- End Booking Calendar Scripts --> <link href="http://www.pezoulia.gr/wp-content/plugins/BookingCalendarPro/css/skins/traditional.css" rel="stylesheet" type="text/css"/> <link href="http://pezoulia.gr/wp-content/plugins/BookingCalendarPro/css/client.css" rel="stylesheet" type="text/css"/>
<meta content="el" http-equiv="Content-Language"/>
<style media="screen" type="text/css">
.qtrans_flag span { display:none }
.qtrans_flag { height:17px; width:25px; display:block }
.qtrans_flag_and_text { padding-left:20px }
.qtrans_flag_EL { background:url(https://pezoulia.gr/wp-content/plugins/qtranslate/flags/gr.png) no-repeat }
.qtrans_flag_en { background:url(https://pezoulia.gr/wp-content/plugins/qtranslate/flags/gb.png) no-repeat }
.qtrans_flag_de { background:url(https://pezoulia.gr/wp-content/plugins/qtranslate/flags/de.png) no-repeat }
</style>
<link href="http://www.pezoulia.gr/en/" hreflang="en" rel="alternate"/>
<link href="http://www.pezoulia.gr/de/" hreflang="de" rel="alternate"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<!-- Dropdown Menu Widget Styles by shailan (http://shailan.com) v1.7.2 on wp3.2.1 -->
<link href="https://pezoulia.gr/wp-content/plugins/dropdown-menu-widget/css/shailan-dropdown.css" rel="stylesheet" type="text/css"/>
<style media="all" type="text/css">
	ul.dropdown { white-space: nowrap;	}	
	/** Show submenus */
	ul.dropdown li:hover > ul, ul.dropdown li.hover ul{ display: block; }
	
	/** Show current submenu */
	ul.dropdown li.hover ul, ul.dropdown ul li.hover ul, ul.dropdown ul ul li.hover ul, ul.dropdown ul ul ul li.hover ul, ul.dropdown ul ul ul ul li.hover ul , ul.dropdown li:hover ul, ul.dropdown ul li:hover ul, ul.dropdown ul ul li:hover ul, ul.dropdown ul ul ul li:hover ul, ul.dropdown ul ul ul ul li:hover ul { display: block; } 
				
			
</style>
<!-- /Dropdown Menu Widget Styles -->
<link href="http://pezoulia.gr/" rel="shortlink"/>
<link href="https://pezoulia.gr/wp-content/themes/pezoulia/style.css/images/favicon.ico" rel="shortcut icon"/>
</head>
<body class="home page page-id-5 page-template-default">
<div id="wrap">
<!-- <div id="header">Header</div><!--END header -->
<!--
	<div id="page-wrap">

		<div id="header">
			<h1><a href="http://www.pezoulia.gr/">Πεζούλια, Παραδοσιακές Κατοικίες, Διαμονή, Σελάκανο, Κρητη</a></h1>
			<div class="description"></div>
		</div>
        
 -->
<aside>
<div id="left">
<div class="logo">
<a href="/index.php"><img alt="Pezoulia.gr - Παραδοσιακές Κατοικίες - Διαμονή - Σελάκανο" border="0" src="https://pezoulia.gr/wp-content/themes/pezoulia/images/logo_selakano_EL.jpg" title="Pezoulia.gr - Παραδοσιακές Κατοικίες - Διαμονή - Σελάκανο"/></a>
</div> <!--END Div logo-->
<div class="left_images" style="padding-left:5px;">
<img alt="Παραδοσιακές Κατοικιες - Πεζουλια - Σελάκανο" src="https://pezoulia.gr/wp-content/themes/pezoulia/images/index_left_image.jpg" title="Παραδοσιακές Κατοικιες - Πεζουλια - Σελάκανο"/>
<a href="http://selakano.meteokrites.gr/" target="_blank"> <img alt="Καιρός - Παραδοσιακές Κατοικιες - Πεζούλια - Σελάκανο" src="https://pezoulia.gr/wp-content/themes/pezoulia/images/Weather-banner-pezoulia.jpg" title="Καιρός - Πεζούλια - Σελάκανο"/></a>
<a href="/διακοπές-για-παιδιά/"> <img alt="Παραδοσιακές Κατοικιες - Πεζούλια - Σελάκανο" src="https://pezoulia.gr/wp-content/themes/pezoulia/images/pezoulia-kids.jpg" title="Διακοπές για παιδιά - Πεζούλια - Σελάκανο"/></a>
<img alt="Εποχές Pezoulia.gr - Σελάκανο" src="https://pezoulia.gr/wp-content/themes/pezoulia/images/me_afetiria_EL.jpg" title="Εποχές Pezoulia.gr - Σελάκανο"/>
<a alt="Χειμώνας - Χιόνια Πεζούλια" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/1-xeimonas.jpg" rel="lightbox[volta]" title="Χειμώνας - Χιόνια Πεζούλια"><img alt="Gallery - Πεζούλια - Σελάκανο" border="0" src="https://pezoulia.gr/wp-content/themes/pezoulia/images/me_afetiria_photos_EL.jpg" title="Gallery - Πεζούλια - Σελάκανο"/></a>
<a alt="Χειμώνας - Χιόνια Πεζούλια" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/2-xeimonas.jpg" rel="lightbox[volta]" title="Χειμώνας - Χιόνια Πεζούλια"></a>
<a href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/3-xeimonas.jpg" rel="lightbox[volta]" title="Χειμώνας - Χιόνια Πεζούλια"></a>
<a alt="Χειμώνας - Πεζούλια - Διαμονή" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/4-xeimonas.jpg" rel="lightbox[volta]" title="Χειμώνας - Πεζούλια - Διαμονή"></a>
<a alt="Χειμώνας - Πεζούλια - Διαμονή" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/5-xeimonas.jpg" rel="lightbox[volta]" title="Χειμώνας - Πεζούλια - Διαμονή"></a>
<a alt="Φθινόπωρο - Σελάκανο" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/3.jpg" rel="lightbox[volta]" title="Φθινόπωρο - Σελάκανο"></a>
<a alt="Σελάκανο" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/1.jpg" rel="lightbox[volta]" title="Σελάκανο"></a>
<a alt="Γυπαετός - Άνοιξη" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/6.jpg" rel="lightbox[volta]" title="Γυπαετός - Άνοιξη"></a>
<a alt="Άνοιξη - Σελάκανο" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/7.jpg" rel="lightbox[volta]" title="Άνοιξη - Σελάκανο"></a>
<a alt="Όλο τον χρόνο - Σελάκανο" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/9.jpg" rel="lightbox[volta]" title="Όλο τον χρόνο - Σελάκανο"></a>
<a alt="Ορειβασία - Φθινόπωρο Άνοιξη" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/11.jpg" rel="lightbox[volta]" title="Ορειβασία - Φθινόπωρο Άνοιξη"></a>
<a alt="Πρωτομαγιά - Άνοιξη Σελάκανο" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/spring-1.jpg" rel="lightbox[volta]" title="Πρωτομαγιά - Άνοιξη Σελάκανο"></a>
<a href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/spring-2.jpg" rel="lightbox[volta]" title="Ιερός Ναός  - Άνοιξη Σελάκανο"></a>
<a alt="Κυκλάμινα  - Άνοιξη Σελάκανο" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/spring-3.jpg" rel="lightbox[volta]" title="Κυκλάμινα  - Άνοιξη Σελάκανο"></a>
<a alt=" Άνοιξη Σελάκανο" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/spring-4.jpg" rel="lightbox[volta]" title=" Άνοιξη Σελάκανο"></a>
<a alt="Δάσος Σελάκανο  - Άνοιξη" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/spring-5.jpg" rel="lightbox[volta]" title="Δάσος Σελάκανο  - Άνοιξη"></a>
<a alt="Αμυγδαλιά  - Άνοιξη Σελάκανο" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/spring-6.jpg" rel="lightbox[volta]" title="Αμυγδαλιά  - Άνοιξη Σελάκανο"></a>
<a alt="Μαργαρίτα  - Άνοιξη Σελάκανο" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/spring-7.jpg" rel="lightbox[volta]" title="Μαργαρίτα  - Άνοιξη Σελάκανο"></a>
<a alt="Άνοιξη Σελάκανο" href="https://pezoulia.gr/wp-content/themes/pezoulia/images/volta_sta_pezoulia/spring-8.jpg" rel="lightbox[volta]" title=" Άνοιξη Σελάκανο"></a>
</div> <!-- END left_images-->
</div> <!-- END left -->
<!-- All this stuff in here only shows up if you DON'T have any widgets active in this zone -->
</aside>
<div id="center">
<div class="menu">
<div class="widget shailan-dropdown-menu-widget">
<!-- Dropdown Menu Widget by shailan (http://shailan.com)  v1.7.2 on wp3.2.1 -->
<!-- Menu Type : navmenu_3 -->
<div class="shailan-dropdown-menu">
<div id="shailan-dropdown-wrapper--1">
<div align="left" class="dropdown-horizontal-container dm-align-left clearfix"><table cellpadding="0" cellspacing="0"><tr><td><ul class="dropdown dropdown-horizontal dropdown-align-left" id="menu-main_menu"><li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-5 current_page_item menu-item-24" id="menu-item-24"><a href="https://www.pezoulia.gr/">αρχική</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25" id="menu-item-25"><a href="https://www.pezoulia.gr/%ce%b4%cf%89%ce%bc%ce%ac%cf%84%ce%b9%ce%b1/">δωμάτια</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30" id="menu-item-30"><a href="https://www.pezoulia.gr/%cf%84%ce%bf%cf%80%ce%bf%ce%b8%ce%b5%cf%83%ce%af%ce%b1/">τοποθεσία</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28" id="menu-item-28"><a href="https://www.pezoulia.gr/%cf%80%ce%b1%cf%81%ce%bf%cf%87%ce%ad%cf%82/">παροχές</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27" id="menu-item-27"><a href="https://www.pezoulia.gr/reservation/">κρατήσεις</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29" id="menu-item-29"><a href="https://www.pezoulia.gr/%cf%84%ce%b9%ce%bc%ce%ad%cf%82/">τιμές</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33" id="menu-item-33"><a href="https://www.pezoulia.gr/links-2/">links</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26" id="menu-item-26"><a href="https://www.pezoulia.gr/%ce%b5%cf%80%ce%b9%ce%ba%ce%bf%ce%b9%ce%bd%cf%89%ce%bd%ce%af%ce%b1/">επικοινωνία</a></li>
</ul></td>
</tr></table>
</div></div>
</div>
<!--/ Dropdown Menu Widget -->
</div> </div><!--END menu-->
<span class="text" id="post-5">
<div class="entry">
<p><img alt="Αρχική-Πεζούλια,Παραδοσιακές Κατοικίες, Διαμονή, Σελάκανο, Κρήτη" class="alignnone size-full wp-image-344" height="345" src="http://www.pezoulia.gr/wp-content/uploads/2011/10/home2-pezoulia.jpg" title="Αρχική-Πεζούλια,Παραδοσιακές Κατοικίες, Διαμονή, Σελάκανο, Κρήτη" width="517"/> <img alt="Καλοσόρισμα - Πεζούλια,Παραδοσιακές Κατοικίες, Διαμονή, Σελάκανο, Κρητη" class="alignnone size-full wp-image-34" height="44" src="/wp-content/uploads/2011/10/kalwsorisate.jpg" title="Καλοσόρισμα - Πεζούλια,Παραδοσιακές Κατοικίες, Διαμονή, Σελάκανο, Κρητη" width="517"/></p>
<p><strong>Τα καταλύματα Πεζούλια στον οικισμό του Σελακάνου σας περιμένουν να γευθείτε τον φυσικό πλούτο της περιοχής</strong></p>
<p>Μέσα στο Δάσος από πρίνους και πεύκα του Σελάκανου, κοντά σε πηγές, ποτάμια και φαράγγια, θα βρείτε την πολυτέλεια και την γαλήνη που αναζητάτε. Τα καταλύματα «Πεζούλια» λειτουργούν όλο το χρόνο και είναι στην διάθεσή σας για αξέχαστες διακοπές.</p>
<p>Το συγκρότημα των παραδοσιακών ξενώνων και κατοικιών βρίσκεται μέσα στον οικισμό του Σελάκανου, πολύ κοντά στο δάσος και σε υψόμετρο 930 μ. Ιδανική αφετηρία για ημερήσιες εκδρομές στα γραφικά ορεινά της Δίκτης, αλλά και τις καυτές παραλίες του Νότου. Ένας ονειρεμένος προορισμός στην ενδοχώρα της Κρήτης με δροσερό καλοκαίρι, αλλά και ένα χειμωνιάτικο τοπίο που συγκλονίζει.<!-- PHP 5.x --></p>
<!-- RESERVATION CODE-->
<!-- END RESERVATION CODE-->
</div>
</span></div>
</div> <!-- END Center -->
<div id="right">
<div class="flags">
<ul class="qtrans_language_chooser" id="qtranslate-chooser"><li class="lang-EL active"><a class="qtrans_flag qtrans_flag_EL" href="http://www.pezoulia.gr/" hreflang="EL" title="Ελληνικά"><span style="display:none">Ελληνικά</span></a></li><li class="lang-en"><a class="qtrans_flag qtrans_flag_en" href="http://www.pezoulia.gr/en/" hreflang="en" title="English"><span style="display:none">English</span></a></li><li class="lang-de"><a class="qtrans_flag qtrans_flag_de" href="http://www.pezoulia.gr/de/" hreflang="de" title="German"><span style="display:none">German</span></a></li></ul><div class="qtrans_widget_end"></div>
</div><!--END of flags class-->
<div class="right_images">
<a href="/map/"><img alt="Βρείτε μας στο χάρτη - Πεζούλια - Σελάκανο" border="0" src="https://pezoulia.gr/wp-content/themes/pezoulia/images/map_EL.jpg" title="Βρείτε μας στο χάρτη - Πεζούλια - Σελάκανο"/></a>
<a href="/activities/"><img alt="Δραστηριότητες - πεζούλια - Σελάκανο" border="0" src="https://pezoulia.gr/wp-content/themes/pezoulia/images/activities_EL.jpg" title="Δραστηριότητες - Πεζούλια - Σελάκανο"/></a>
<a href="/που-θα-φάμε/"><img alt="Ταβέρνα - Πεζούλια - Σελάκανο" border="0" src="https://pezoulia.gr/wp-content/themes/pezoulia/images/w2eat_EL.jpg" title="Ταβέρνα - Πεζούλια - Σελάκανο"/></a>
<a href="/reservation/"><img alt="Κρατήσεις - Πεζούλια - Σελάκανο" border="0" src="https://pezoulia.gr/wp-content/themes/pezoulia/images/reservation_EL.jpg" title="Κρατήσεις - Πεζούλια - Σελάκανο"/></a>
</div> <!-- END right_images -->
<div class="digilodge_logos">
<img alt="digilodge_logos" border="0" src="https://pezoulia.gr/wp-content/themes/pezoulia/images/digilodge.jpg" usemap="#digilodge_logos"/>
<map name="digilodge_logos">
<area alt="Youtube" coords="10,8,138,50" href="http://digilodge-portal.digitalaid.gr/" shape="rect" title="Ψηφιακές Ενισχύσεις"/>
<area alt="Europe Union" coords="7,63,71,122" href="http://europa.eu/" shape="rect" title="Europe Union"/>
<area alt="ΕΣΠΑ" coords="81,69,143,113" href="http://www.espa.gr/el/Pages/default.aspx" shape="rect" title="ΕΣΠΑ"/>
</map>
</div><!-- END digilodge_logos -->
</div> <!-- END Right -->
<footer>
<!-- END WRAP-->
<div class="copyright">
			©2021 Διαμερίσματα Πεζούλια - Κρήτη
		</div>
<div class="created_by">site by <a href="http://www.load.gr"><strong>L</strong>OAD</a></div>
<script src="http://pezoulia.gr/wp-content/plugins/lightbox-plus/js/jquery.colorbox-min.js?ver=1.3.17.2" type="text/javascript"></script>
<!-- Lightbox Plus v2.3 - 2011.08.11 - Message: 0-->
<script type="text/javascript">
jQuery(document).ready(function($){
  $("a[rel*=lightbox]").colorbox({opacity:0.8});
});
</script>
<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-69056465-1', 'auto');
	  ga('send', 'pageview');

	</script>
</footer></body>
</html>
