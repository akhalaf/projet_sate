<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="utf-8"/>
<title>Begin.ru – Бизнес-образование в России и за рубежом</title>
<meta content="Обучение за рубежом, executive mba, mba, магистратура, аспирантура, второе высшее, gmat, toefl, ielts, cae, стипендии и гранты, рейтинги вузов и бизнес-школ," name="keywords"/>
<meta content="Образовательные выставки, новости бизнес-образования, рейтинги бизнес-школ, программы магистратуры и MBA, международные экзамены: GMAT, TOEFL, IELTS, CAE" name="description"/>
<meta content="width=device-width" name="viewport"/>
<meta content="100003727896958" property="fb:admins"/>
<base href="https://www.begin.ru/"/>
<link href="/assets/templates/default/img/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/assets/templates/site/css/application.css?22092019" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700&amp;subset=latin,cyrillic-ext,cyrillic" rel="stylesheet" type="text/css"/>
<meta content="Begin.ru – Бизнес-образование в России и за рубежом" property="og:title"/>
<meta content="article" property="og:type"/>
<meta content="http://www.begin.ru/" property="og:url"/>
<meta content="http://www.begin.ru/assets/templates/site/images/logo-big.png" property="og:image"/>
<meta content="Begin.ru" property="og:site_name"/>
<meta content="Образовательные выставки, новости бизнес-образования, рейтинги бизнес-школ, программы магистратуры и MBA, международные экзамены: GMAT, TOEFL, IELTS, CAE" property="og:description"/>
<script src="/assets/templates/site/js/min/jquery-1.10.1.min.js" type="text/javascript"></script>
<link href="https://www.begin.ru/" rel="canonical"/>
</head>
<body>
<div class="wrapper">
<header class="header">
<div class="header-container limiter">
<a class="logo" href="http://www.begin.ru/"><img alt="" src="/assets/templates/site/images/logo-big.png"/></a>
<span class="icon-menu"></span>
<div class="header-search">
<div class="search">
<div class="bg-form">
<form action="search/" method="get">
<input class="header-search_button searchSubmit" type="submit" value=""/>
<input class="header-search_field" name="query" type="text" value=""/>
</form>
</div>
</div>
</div>
<div class="header-menu">
<a href="programs/">Программы обучения</a><a href="events/">Мероприятия</a><a href="novosti-i-stati/">Новости и статьи</a><a href="partners.html">Партнеры</a><a href="karta-sajta.html">Карта сайта</a>
</div>
</div>
</header>
<nav class="menu limiter">
<span class="menu-more">
<span class="menu-item"><a href="programs/">Программы обучения</a></span><span class="menu-item"><a href="events/">Мероприятия</a></span><span class="menu-item"><a href="novosti-i-stati/">Новости и статьи</a></span><span class="menu-item"><a href="partners.html">Партнеры</a></span><span class="menu-item"><a href="karta-sajta.html">Карта сайта</a></span>
</span>
<span class="menu-item menu-item__dropdown">
<a href="add/obrazovanie-za-rubezhom/">Образование за рубежом</a>
<i class="icon icon_dropdown"></i>
<span class="menu-item_group">
<a href="add/obrazovanie-za-rubezhom/vyisshee-obrazovanie-za-rubezhom/">Высшее образование за рубежом</a><a href="add/obrazovanie-za-rubezhom/yazyikovyie-kursyi-za-rubezhom.html">Языковые курсы за рубежом</a><a href="add/obrazovanie-za-rubezhom/besplatnoe-obuchenie-v-stranax-skandinavii.html">Бесплатное обучение в странах Скандинавии</a><a href="add/obrazovanie-za-rubezhom/detskie-lagerya-za-rubezhom.html">Детские лагеря за рубежом</a><a href="add/obrazovanie-za-rubezhom/srednee-obrazovanie-v-ssha/">Среднее образование в США</a><a href="add/obrazovanie-za-rubezhom/srednee-obrazovanie-za-rubezhom/">Среднее образование за рубежом</a><a href="add/obrazovanie-za-rubezhom/programmyi-kulturnogo-obmena-za-rubezhom/">Программы культурного обмена за рубежом</a><a href="add/obrazovanie-za-rubezhom/vozmozhnosti-distanczionnogo-obrazovaniya.html">Возможности дистанционного образования</a><a href="add/obrazovanie-za-rubezhom/samyie-neobyichnyie-speczialnosti-i-gde-im-uchat.html">Самые необычные специальности и где на них учат</a><a href="add/obrazovanie-za-rubezhom/magistratura-v-universitete-madrida.html">Магистратура в университете Мадрида</a><a href="add/obrazovanie-za-rubezhom/obuchenie-v-milane-italyanskoj-stolicze-modyi-i-dizajna.html">Обучение в Милане, итальянской столице моды и дизайна</a><a href="add/obrazovanie-za-rubezhom/magistratura-v-gollandii.html">Магистратура в Голландии</a><a href="add/obrazovanie-za-rubezhom/vozmozhnosti-besplatnogo-obucheniya-v-germanii.html">Возможности бесплатного обучения в Германии</a><a href="add/obrazovanie-za-rubezhom/obrazovanie-v-vengrii-izuchaem-arxitekturu.html">Образование в Венгрии: изучаем архитектуру</a><a href="add/obrazovanie-za-rubezhom/distanczionnoe-obuchenie-marketingu-v-avstrii-vmeste-s-elac/">Дистанционное обучение маркетингу в Австрии вместе с eLAC: уверенный старт для международной карьеры</a>
</span>
</span><span class="menu-item menu-item__dropdown">
<a href="add/mezhdunarodnye-ekzameny/">Международные экзамены</a>
<i class="icon icon_dropdown"></i>
<span class="menu-item_group">
<a href="add/mezhdunarodnye-ekzameny/toefl/">TOEFL (Test of English as a Foreign Language)</a><a href="add/mezhdunarodnye-ekzameny/cae/">CАЕ (Cambridge English: Advanced)</a><a href="add/mezhdunarodnye-ekzameny/ielts/">IELTS (International English Language Testing System)</a><a href="add/mezhdunarodnye-ekzameny/gmat/">GMAT (The Graduate Management Admission Test)</a><a href="add/mezhdunarodnye-ekzameny/gre/">GRE (Graduate Record Examinations)</a><a href="add/mezhdunarodnye-ekzameny/sat/">SAT (Scholastic Aptitude Test)</a>
</span>
</span><span class="menu-item menu-item__dropdown">
<a href="add/spravochnik/">Справочник</a>
<i class="icon icon_dropdown"></i>
<span class="menu-item_group">
<a href="add/spravochnik/obrazovatelnyie-vyistavki/">Образовательные выставки</a><a href="add/spravochnik/doctor-of-business-administration.html">Doctor of Business Administration</a><a href="add/spravochnik/doktorantura-phd/">Докторантура (PhD)</a><a href="add/spravochnik/aspirantura.html">Аспирантура</a><a href="add/spravochnik/luchshie-universitetskie-goroda-mira/">Лучшие университетские города мира</a><a href="add/spravochnik/luchshie-vuzyi-mira/">Лучшие вузы мира</a><a href="add/spravochnik/vostrebovannyie-speczialnosti/">Востребованные специальности</a><a href="add/spravochnik/postgrad-education-abroad/">Постдипломное образование в разных странах</a><a href="add/spravochnik/online-education/">Обучение online</a><a href="add/spravochnik/professionalnye-sertifikaty/">Профессиональные сертификаты</a><a href="add/spravochnik/kakie-gumanitarnyie-professii-vostrebovanyi.html">Какие гуманитарные профессии востребованы</a>
</span>
</span><span class="menu-item menu-item__dropdown">
<a href="add/magistratura/">Магистратура</a>
<i class="icon icon_dropdown"></i>
<span class="menu-item_group">
<a href="add/magistratura/cho-takoe-magistrtura.html">Что такое магистратура и с чем туда идут</a><a href="add/magistratura/magistratura-i-zakonodatelstvo.html">Магистратура и законодательство</a><a href="add/magistratura/novie-vozmozhnosti.html">Магистратура: новые возможности для карьеры</a><a href="add/magistratura/dvuhurovnevaia-sistema.html">Двухуровневая система образования</a><a href="add/magistratura/magistr-vs-aspirant.html">Что выбрать: магистратуру или аспирантуру?</a><a href="add/magistratura/magistratura.html">О магистратуре</a><a href="add/magistratura/magistratura-ili-mba.html">Магистратура или MBA?</a><a href="add/magistratura/magistratura-za-rubezhom/">Магистратура за рубежом</a><a href="add/magistratura/spetcialnosti-magistratury.html">Специальности магистратуры</a><a href="add/magistratura/magistratura-v-avstrii/">Магистратура в Австрии</a>
</span>
</span><span class="menu-item menu-item__dropdown">
<a href="add/vtoroe-vyisshee/">Второе высшее</a>
<i class="icon icon_dropdown"></i>
<span class="menu-item_group">
<a href="add/vtoroe-vyisshee/vtoroe-vysshee-obrazovanie-ili-magistratura.html">Второе высшее образование: бакалавриат или магистратура?</a><a href="add/vtoroe-vyisshee/vtoroe-vysshee-v-moskve.html">Второе высшее образование в Москве</a><a href="add/vtoroe-vyisshee/vtoroe-vysshee-obrazovanie-besplatno.html">Бесплатное второе высшее образование</a><a href="add/vtoroe-vyisshee/programmyi-vtorogo-vyisshego-obrazovaniya/">О программах второго высшего образования</a>
</span>
</span><span class="menu-item menu-item__dropdown">
<a href="add/all-about-mba/">Все об MBA</a>
<i class="icon icon_dropdown"></i>
<span class="menu-item_group">
<a href="add/all-about-mba/opredelenie-mba.html">Определение и специфика MBA</a><a href="add/all-about-mba/chto-daet-obrazovanie-mba.html">Что дает образование МВА?</a><a href="add/all-about-mba/akkreditacziya-programm-mba.html">Аккредитация программ МВА</a><a href="add/all-about-mba/vidy-programm-mba/">Виды программ МВА</a><a href="add/all-about-mba/biznes-obrazovanie-v-raznykh-regionakh-mira/">Бизнес-образование за рубежом</a><a href="add/all-about-mba/finansirovanie-obucheniya.html">Сколько стоит MBA</a><a href="add/all-about-mba/postuplenie-v-biznes-shkolu.html">Как поступить в бизнес-школу</a><a href="add/all-about-mba/kak-pisat-esse-na-programmy-mba/">Примеры эссе на программы MBA</a><a href="add/all-about-mba/recomendations-letters.html">Рекомендательные письма</a><a href="add/all-about-mba/executive-mba/">Executive MBA</a><a href="add/all-about-mba/6-sovetov-dlya-sostavleniya-rezyume-na-programmu-mba/">Шесть советов для составления резюме на программу MBA</a>
</span>
</span><span class="menu-item"><a href="add/stipendii/">Стипендии</a></span><span class="menu-item"><a href="add/blogs/">Личный опыт</a></span>
<span class="icon-close"></span>
</nav>
<div class="banner limiter" data-zone="13"></div>
<div class="page limiter">
<div class="page-wrapper">
<section class="section topic">
<div class="topic-items">
<a class="topic-item" data-id="19147" data-slide-index="0" href="novosti-i-stati/profexamen-v-universitetah/">В университетах могут ввести экзамен на профпригодность</a>
<a class="topic-item" data-id="19146" data-slide-index="1" href="novosti-i-stati/biometriya-na-examenah/">Студентам предложили использовать биометрию на экзаменах</a>
<a class="topic-item" data-id="19145" data-slide-index="2" href="novosti-i-stati/pravila-attestatsii-dlya-aspirantov/">Как изменятся правила аттестации для аспирантов</a>
<a class="topic-item" data-id="19144" data-slide-index="3" href="novosti-i-stati/prodolzhit-li-rasti-stoimost-obuchenia/">Продолжит ли расти стоимость обучения в вузах</a>
<a class="topic-item" data-id="19143" data-slide-index="4" href="novosti-i-stati/kak-izmenitsa-vysshee-obrazovanie/">Как изменится высшее образование в следующие 10 лет</a>
</div>
<div class="topic-slider">
<div class="slider" id="topic-slider">
<div class="topic-preview">
<div class="topic-preview_image"><img alt="" src="/assets/media/2020/images/20201225-profexamen-main.jpg"/></div>
</div>
<div class="topic-preview">
<div class="topic-preview_image"><img alt="" src="/assets/media/2020/images/20201225-biometric-main.jpg"/></div>
</div>
<div class="topic-preview">
<div class="topic-preview_image"><img alt="" src="/assets/media/2020/images/20201225-attestatsiya-main.jpg"/></div>
</div>
<div class="topic-preview">
<div class="topic-preview_image"><img alt="" src="/assets/media/2020/images/20201218-stoimost_obucheniya-main.jpg"/></div>
</div>
<div class="topic-preview">
<div class="topic-preview_image"><img alt="" src="/assets/media/2020/images/20201218-vyschee_obrazovanie-main.jpg"/></div>
</div>
</div>
</div>
<div class="topic-desc">
<div class="topic-desc-item __active" id="desc19147">
<span class="topic-desc-item_date">25.12.2020</span>
<a class="topic-desc-item_title" href="novosti-i-stati/profexamen-v-universitetah/">В университетах могут ввести экзамен на профпригодность</a>
<a class="topic-desc-item_cont" href="novosti-i-stati/profexamen-v-universitetah/">В российских учебных заведениях будет запущен пилотный проект по профессиональному экзамену для выпускников. Согласно инициативе, планируется добавить еще один экзамен, который будет определять профпригодность студента для будущего работодателя.</a>
</div>
<div class="topic-desc-item " id="desc19146">
<span class="topic-desc-item_date">25.12.2020</span>
<a class="topic-desc-item_title" href="novosti-i-stati/biometriya-na-examenah/">Студентам предложили использовать биометрию на экзаменах</a>
<a class="topic-desc-item_cont" href="novosti-i-stati/biometriya-na-examenah/">Министерство цифрового развития, связи и массовых коммуникаций выступило с предложением использовать Единую биометрическую систему (ЕБС) на дистанционной сессии.</a>
</div>
<div class="topic-desc-item " id="desc19145">
<span class="topic-desc-item_date">25.12.2020</span>
<a class="topic-desc-item_title" href="novosti-i-stati/pravila-attestatsii-dlya-aspirantov/">Как изменятся правила аттестации для аспирантов</a>
<a class="topic-desc-item_cont" href="novosti-i-stati/pravila-attestatsii-dlya-aspirantov/">В Госдуме в ходе третьего чтения принят законопроект, который устанавливает новые требования для аспирантуры и вступает в силу 1 сентября 2021 года.</a>
</div>
<div class="topic-desc-item " id="desc19144">
<span class="topic-desc-item_date">18.12.2020</span>
<a class="topic-desc-item_title" href="novosti-i-stati/prodolzhit-li-rasti-stoimost-obuchenia/">Продолжит ли расти стоимость обучения в вузах</a>
<a class="topic-desc-item_cont" href="novosti-i-stati/prodolzhit-li-rasti-stoimost-obuchenia/">Ежегодные повышения платы за обучение уже стали привычными в большинстве российских вузов. По состоянию на март 2020 года средняя стоимость года учебы в одном из университетов Москвы или Санкт-Петербурга составляла около 250 тысяч рублей в год.</a>
</div>
<div class="topic-desc-item " id="desc19143">
<span class="topic-desc-item_date">18.12.2020</span>
<a class="topic-desc-item_title" href="novosti-i-stati/kak-izmenitsa-vysshee-obrazovanie/">Как изменится высшее образование в следующие 10 лет</a>
<a class="topic-desc-item_cont" href="novosti-i-stati/kak-izmenitsa-vysshee-obrazovanie/">2020 год принес много неожиданностей студентам и преподавателям по всему миру. Из-за карантинных ограничений многие университеты были вынуждены перевести учащихся на дистанционное обучение, а также искать способы принимать онлайн экзамены и защиту дипломных работ.</a>
</div>
</div>
<div class="section_link"><a href="novosti-i-stati/">Все новости</a></div>
</section>
<div class="banner banner-content" data-zone="11" style="display: none;"></div>
<section class="section manual">
<h2 class="section-title">Практическое руководство</h2>
<div class="manual-form">
<h4 class="manual-form_title"><a href="programs/">Программы MBA</a></h4>
<form action="programs/" class="form" id="manual-form">
<input name="srcmba" type="hidden" value="1"/>
<div class="form-control">
<div class="form-control_value">
<select class="field" name="pr_type">
<option value="15">MBA</option>
<option value="16">EMBA</option>
</select>
</div>
</div>
<div class="form-control">
<div class="form-control_title">в</div>
<div class="form-control_value">
<select class="field" name="country">
<option value="0">Все страны</option>
<option selected="selected" value="1">Россия</option>
<option value="10">Австралия</option>
<option value="11">Австрия</option>
<option value="12">Азербайджан</option>
<option value="15">Аргентина</option>
<option value="16">Армения</option>
<option value="17">Белоруссия</option>
<option value="18">Бельгия</option>
<option value="19">Болгария</option>
<option value="21">Бразилия</option>
<option value="22">Великобритания</option>
<option value="23">Венгрия</option>
<option value="24">Вьетнам</option>
<option value="25">Германия</option>
<option value="26">Гонконг</option>
<option value="27">Греция</option>
<option value="28">Грузия</option>
<option value="29">Дания</option>
<option value="30">Египет</option>
<option value="31">Израиль</option>
<option value="32">Индия</option>
<option value="33">Индонезия</option>
<option value="34">Иордания</option>
<option value="35">Ирландия</option>
<option value="36">Исландия</option>
<option value="37">Испания</option>
<option value="38">Италия</option>
<option value="3">Казахстан</option>
<option value="39">Канада</option>
<option value="40">Кипр</option>
<option value="41">Киргизия</option>
<option value="42">Китай</option>
<option value="46">Латвия</option>
<option value="47">Литва</option>
<option value="48">Лихтенштейн</option>
<option value="49">Люксембург</option>
<option value="50">Македония</option>
<option value="51">Малайзия</option>
<option value="52">Мальта</option>
<option value="53">Марокко</option>
<option value="54">Мексика</option>
<option value="55">Молдавия</option>
<option value="56">Монако</option>
<option value="57">Монголия</option>
<option value="58">Нидерланды</option>
<option value="59">Новая Зеландия</option>
<option value="60">Норвегия</option>
<option value="61">ОАЭ</option>
<option value="62">Оман</option>
<option value="63">Пакистан</option>
<option value="65">Польша</option>
<option value="66">Португалия</option>
<option value="67">Румыния</option>
<option value="69">Саудовская Аравия</option>
<option value="71">Сербия</option>
<option value="72">Сингапур</option>
<option value="73">Сирия</option>
<option value="74">Словакия</option>
<option value="75">Словения</option>
<option value="76">Соединенные Штаты Америки</option>
<option value="77">Таджикистан</option>
<option value="78">Таиланд</option>
<option value="79">Тунис</option>
<option value="80">Туркмения</option>
<option value="81">Турция</option>
<option value="82">Узбекистан</option>
<option value="2">Украина</option>
<option value="83">Финляндия</option>
<option value="84">Франция</option>
<option value="85">Хорватия</option>
<option value="86">Черногория</option>
<option value="87">Чехия</option>
<option value="88">Швейцария</option>
<option value="89">Швеция</option>
<option value="92">Эстония</option>
<option value="44">Южная Корея</option>
<option value="93">Южно-Африканская Республика</option>
<option value="94">Япония</option>
<option value="1000">Другие страны за рубежом</option>
</select>
</div>
</div>
<div class="form-control">
<div class="form-control_title">по специальности</div>
<div class="form-control_value">
<select class="field" name="spec">
<option selected="" value="0">Все</option>
<option value="1">Государственное и муниципальное управление</option>
<option value="2">Естественные науки</option>
<option value="14">Индустрия гостеприимства (рестораны, гостиницы, туризм)</option>
<option value="3">Инженерно-технические специальности (кроме IT)</option>
<option value="4">Информационные технологии и телекоммуникации</option>
<option value="5">Искусство и дизайн</option>
<option value="6">История, культурология</option>
<option value="7">Лингвистика, филология, иностранные языки</option>
<option value="8">Маркетинг, реклама, PR</option>
<option value="9">Медицина и здравоохранение</option>
<option value="24">Международная и внешнеэкономическая деятельность, таможня, экспорт</option>
<option value="20">Налоги и налогообложение</option>
<option value="10">Педагогика</option>
<option value="11">Продажи</option>
<option value="23">Производственная деятельность</option>
<option value="12">Психология</option>
<option value="22">Строительство, недвижимость, девелопмент</option>
<option value="21">Транспорт и логистика</option>
<option value="17">Управление персоналом</option>
<option value="15">Физическая культура и спорт</option>
<option value="13">Философия, социология, политология</option>
<option value="16">Финансы и бухгалтерский учет</option>
<option value="25">Фотография</option>
<option value="18">Экономика, управление и менеджмент</option>
<option value="19">Юриспруденция и право</option>
</select>
</div>
</div>
<div class="form-button">
<input class="button" id="quick-search" type="submit" value="БЫСТРЫЙ ПОИСК"/>
</div>
</form>
</div>
<div class="manual-links">
<a href="add/all-about-mba/vidy-programm-mba/">Виды программ MBA</a>
<a href="add/all-about-mba/akkreditacziya-programm-mba.html">Аккредитация программ MBA и бизнес-школ</a>
<a href="add/ratings/osnovnye-mirovye-reitingi.html">Основные рейтинги MBA и Executive MBA</a>
</div>
<div class="manul-training training">
<h4 class="training-title"><a href="add/mezhdunarodnye-ekzameny/">Подготовиться к поступлению</a></h4>
<div class="training-items">
<div class="training-item">
<div class="training-item_img"><img alt="" src="/assets/templates/site/css/images/training-1.png"/></div>
<div class="training-item_cont">
                    Сдать тесты <a href="add/mezhdunarodnye-ekzameny/gmat/">GMAT</a> и <a href="add/mezhdunarodnye-ekzameny/toefl/">TOEFL</a> Объяснения к решениям
                  </div>
</div>
<div class="training-item">
<div class="training-item_img"><img alt="" src="/assets/templates/site/css/images/training-2.png"/></div>
<div class="training-item_cont">
                    Получить <a href="add/all-about-mba/recomendations-letters.html">рекомендательные письма.</a> Примеры рекомендаций
                  </div>
</div>
<div class="training-item">
<div class="training-item_img"><img alt="" src="/assets/templates/site/css/images/training-3.png"/></div>
<div class="training-item_cont">
                    Написать <a href="add/all-about-mba/kak-pisat-esse-na-programmy-mba/">эссе.</a> Примеры эссе в бизнес-школы за рубежом
                  </div>
</div>
<div class="training-item">
<div class="training-item_img"><img alt="" src="/assets/templates/site/css/images/training-4.png"/></div>
<div class="training-item_cont">
                    Узнать больше.<br/> Заказать <a href="add/putevoditel-po-mba.html">Путеводитель по MBA</a>
</div>
</div>
</div>
</div>
<div class="magistr">
<h4 class="magistr-title"><a href="programs/">Программы магистратуры</a></h4>
<div class="manual-form">
<form action="programs/" class="form" id="manual-form">
<input name="srcmba" type="hidden" value="1"/>
<div class="form-control">
<div class="form-control_value">
<select class="field" id="" name="pr_type">
<option value="3">Магистратура</option>
</select>
</div>
</div>
<div class="form-control">
<div class="form-control_title">в</div>
<div class="form-control_value">
<select class="field" name="country">
<option value="0">Все страны</option>
<option selected="selected" value="1">Россия</option>
<option value="10">Австралия</option>
<option value="11">Австрия</option>
<option value="12">Азербайджан</option>
<option value="15">Аргентина</option>
<option value="16">Армения</option>
<option value="17">Белоруссия</option>
<option value="18">Бельгия</option>
<option value="19">Болгария</option>
<option value="21">Бразилия</option>
<option value="22">Великобритания</option>
<option value="23">Венгрия</option>
<option value="24">Вьетнам</option>
<option value="25">Германия</option>
<option value="26">Гонконг</option>
<option value="27">Греция</option>
<option value="28">Грузия</option>
<option value="29">Дания</option>
<option value="30">Египет</option>
<option value="31">Израиль</option>
<option value="32">Индия</option>
<option value="33">Индонезия</option>
<option value="34">Иордания</option>
<option value="35">Ирландия</option>
<option value="36">Исландия</option>
<option value="37">Испания</option>
<option value="38">Италия</option>
<option value="3">Казахстан</option>
<option value="39">Канада</option>
<option value="40">Кипр</option>
<option value="41">Киргизия</option>
<option value="42">Китай</option>
<option value="46">Латвия</option>
<option value="47">Литва</option>
<option value="48">Лихтенштейн</option>
<option value="49">Люксембург</option>
<option value="50">Македония</option>
<option value="51">Малайзия</option>
<option value="52">Мальта</option>
<option value="53">Марокко</option>
<option value="54">Мексика</option>
<option value="55">Молдавия</option>
<option value="56">Монако</option>
<option value="57">Монголия</option>
<option value="58">Нидерланды</option>
<option value="59">Новая Зеландия</option>
<option value="60">Норвегия</option>
<option value="61">ОАЭ</option>
<option value="62">Оман</option>
<option value="63">Пакистан</option>
<option value="65">Польша</option>
<option value="66">Португалия</option>
<option value="67">Румыния</option>
<option value="69">Саудовская Аравия</option>
<option value="71">Сербия</option>
<option value="72">Сингапур</option>
<option value="73">Сирия</option>
<option value="74">Словакия</option>
<option value="75">Словения</option>
<option value="76">Соединенные Штаты Америки</option>
<option value="77">Таджикистан</option>
<option value="78">Таиланд</option>
<option value="79">Тунис</option>
<option value="80">Туркмения</option>
<option value="81">Турция</option>
<option value="82">Узбекистан</option>
<option value="2">Украина</option>
<option value="83">Финляндия</option>
<option value="84">Франция</option>
<option value="85">Хорватия</option>
<option value="86">Черногория</option>
<option value="87">Чехия</option>
<option value="88">Швейцария</option>
<option value="89">Швеция</option>
<option value="92">Эстония</option>
<option value="44">Южная Корея</option>
<option value="93">Южно-Африканская Республика</option>
<option value="94">Япония</option>
<option value="1000">Другие страны за рубежом</option>
</select>
</div>
</div>
<div class="form-control">
<div class="form-control_title">по специальности</div>
<div class="form-control_value">
<select class="field" name="spec">
<option selected="" value="0">Все</option>
<option value="1">Государственное и муниципальное управление</option>
<option value="2">Естественные науки</option>
<option value="14">Индустрия гостеприимства (рестораны, гостиницы, туризм)</option>
<option value="3">Инженерно-технические специальности (кроме IT)</option>
<option value="4">Информационные технологии и телекоммуникации</option>
<option value="5">Искусство и дизайн</option>
<option value="6">История, культурология</option>
<option value="7">Лингвистика, филология, иностранные языки</option>
<option value="8">Маркетинг, реклама, PR</option>
<option value="9">Медицина и здравоохранение</option>
<option value="24">Международная и внешнеэкономическая деятельность, таможня, экспорт</option>
<option value="20">Налоги и налогообложение</option>
<option value="10">Педагогика</option>
<option value="11">Продажи</option>
<option value="23">Производственная деятельность</option>
<option value="12">Психология</option>
<option value="22">Строительство, недвижимость, девелопмент</option>
<option value="21">Транспорт и логистика</option>
<option value="17">Управление персоналом</option>
<option value="15">Физическая культура и спорт</option>
<option value="13">Философия, социология, политология</option>
<option value="16">Финансы и бухгалтерский учет</option>
<option value="25">Фотография</option>
<option value="18">Экономика, управление и менеджмент</option>
<option value="19">Юриспруденция и право</option>
</select>
</div>
</div>
<div class="form-button">
<input class="button" id="quick-search" type="submit" value="БЫСТРЫЙ ПОИСК"/>
</div>
</form>
</div>
<div class="manual-links">
<a href="add/magistratura/novie-vozmozhnosti.html">Магистратура: новые возможности для карьеры</a>
<a href="add/magistratura/magistr-vs-aspirant.html">Что выбрать: магистратуру или аспирантуру?</a>
</div>
</div>
</section>
<section class="section articles">
<h2 class="section-title">Тенденции и аналитика</h2>
<div class="articles-items">
<div class="articles-item">
<a class="articles-item_title" href="articles/kak-otkryit-svoj-it-startap-s-diplomom-mba/">Как открыть свой IT-стартап с дипломом MBA</a>
<a class="articles-item_desc" href="articles/kak-otkryit-svoj-it-startap-s-diplomom-mba/">Основатель стартапа Applause делится секретами построения успешного бизнеса</a>
</div>
<div class="articles-item">
<a class="articles-item_title" href="articles/kuda-postupat-chtobyi-rabotat-v-apple-p-i-g-i-chanel/">Куда поступать, чтобы работать в Apple, P&amp;G и Chanel</a>
<a class="articles-item_desc" href="articles/kuda-postupat-chtobyi-rabotat-v-apple-p-i-g-i-chanel/">Выпускников каких бизнес-школ берут чаще всего на работу крупные компании? От Apple и P&amp;G до Chanel и…</a>
</div>
<div class="articles-item">
<a class="articles-item_title" href="articles/portret-studenta-harvard-business-school/">Портрет студента Harvard Business School</a>
<a class="articles-item_desc" href="articles/portret-studenta-harvard-business-school/">Каких студентов хочет видеть одна из самых влиятельных и престижных бизнес-школ мира</a>
</div>
<div class="articles-item">
<a class="articles-item_title" href="articles/prosto-wow-polza-onlajn-igr-dlya-kareryi-v-biznese/">Просто WoW: польза онлайн-игр для карьеры в бизнесе</a>
<a class="articles-item_desc" href="articles/prosto-wow-polza-onlajn-igr-dlya-kareryi-v-biznese/">В своей статье для издания Forbes руководитель направления маркетинга немецкой компании SAP Кэти Купер…</a>
</div>
</div>
<div class="section_link"><a href="articles/">Все статьи</a></div>
</section>
<section class="section author">
<h2 class="section-title">Авторская колонка</h2>
<div class="author-colum">
<div class="author-colum_img" style="background-image:url(/assets/media/Persons/20161213_ak_yuri_tazov_1.jpg)"></div>
<div class="author-colum-cont">
<div class="author-colum_name">Юрий Тазов</div>
<a class="author-colum_title" href="author/yurij-tazov/mba-polnaya-kasha.html">МВА. Полная каша</a>
<a class="author-colum_desc" href="author/yurij-tazov/mba-polnaya-kasha.html">Неожиданно, в самый разгар кризиса (или в пору его окончания и победоносного выхода из него) СМИ демонстрируют живой интерес к МВА. Названия на любой вкус: от брутального - «Скидки на МВА раздают силой» до гастрономического - «Каша из МВА». Обилие статей очень радует, даже если иногда нам предлагаются застарелые мифы или новое мифотворчество на тему МВА. Ситуация понятна: падающий спрос диктует новые правила игры и выживать как-то надо. Поэтому появляются довольно оригинальные мысли.</a>
</div>
</div>
<div class="section_link"><a href="author/">Все статьи</a></div>
</section>
</div>
<aside class="sidebar __right">
<div class="sidebar-block banner" data-zone="6"></div>
<div class="sidebar-block fb">
<fb:fan connections="8" height="320" logobar="0" profile_id="111028795582204" stream="0" width="240">
</fb:fan>
</div>
<div class="sidebar-block banner notice" data-block=".notice-content" data-zone="3">
<h2 class="notice-title">На заметку</h2>
<div class="notice-content"></div>
</div>
<div class="sidebar-block banner" data-zone="5"></div>
<!--div class="sidebar-block banner" data-zoneid="53"></div-->
<div class="sidebar-block subscription">
<div class="subscription-title">Подписка на новости</div>
<form action="server/subscribe/" class="form subscription-form sub" id="subscription">
<div class="form-container">
<input class="field" data-validate="required" id="firstname" name="firstname" placeholder="Имя" type="text"/>
<input class="field" data-validate="required" id="lastname" name="lastname" placeholder="Фамилия" type="text"/>
<input class="field" data-validate="required, email" id="email" name="email" placeholder="E-mail" type="text"/>
<div class="form-control">
<input checked="" class="check" id="subscribe_dop" name="subscribe_dop" type="checkbox"/>
<label for="subscribe_dop"><span class="check-label"></span> бизнес-образования и программ MBA</label>
</div>
<div class="form-control">
<input checked="" class="check" id="subscribe_vis" name="subscribe_vis" type="checkbox"/>
<label for="subscribe_vis"><span class="check-label"></span> высшего образования</label>
</div>
<div class="form__agreem">
<input class="check" data-validate="required" id="check_agreem" name="check" type="checkbox"/>
<label for="check_agreem">
<span class="check-label"></span> Я подтверждаю свое согласие с условиями <a href="/assets/media/files/soglashenie-begin.pdf" target="_blank">Пользовательского соглашения</a> и  <a href="/assets/media/files/politika-personalnykh-dannykh.pdf" target="_blank">Политикой конфиденциальности</a>
</label>
</div>
<input class="form-button button __blue" type="submit" value="Подписаться"/>
</div>
</form>
</div>
<div class="sidebar-block social">
<div class="sidebar-block_title">BeginGroup в сети</div>
<div class="social-buttons">
<a href="https://www.facebook.com/begingroup" target="_blank"><img alt="" src="/assets/templates/site/css/images/icon-fb.png"/></a>
<a href="https://twitter.com/Begin_Group" target="_blank"><img alt="" src="/assets/templates/site/css/images/icon-tw.png"/></a>
</div>
</div>
</aside>
</div>
</div>
<footer class="footer">
<div class="footer-wrapper">
<div class="footer-container limiter">
<div class="footer-info">
<span class="footer-info_item">
<span>© 2021 Begin Group.</span>
</span>
<span class="footer-info_item">
<span>Телефон: +7 (495) 933-1098</span>
<span>E-mail: <a href="mailto:info@begingroup.com">info@begingroup.com</a></span>
</span>
<span class="footer-info_item">
<span><a href="/price" target="_blank">Реклама на сайте</a></span>
<!--LiveInternet counter-->
<script type="text/javascript">
        			document.write("<a href='//www.liveinternet.ru/click' "+ "target=_blank><img src='//counter.yadro.ru/hit?t52.11;r"+ escape(document.referrer)+((typeof(screen)=="undefined")?"": ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth? 
        			screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+ 
        			";"+Math.random()+ 
        			"' alt='' title='LiveInternet: показано число просмотров и"+ 
        			" посетителей за 24 часа' "+ 
        			"border=0 width=88 height=31><\/a>")
        		</script>
<!--/LiveInternet-->
<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=919020&amp;from=informer" rel="nofollow" target="_blank"><img alt="Яндекс.Метрика" onclick="try{Ya.Metrika.informer({i:this,id:919020,lang:'ru'});return false}catch(e){}" src="//bs.yandex.ru/informer/919020/3_1_FFFFFFFF_EFEFEFFF_0_pageviews" style="width:88px; height:31px; border:0;" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"/></a>
<!-- /Yandex.Metrika informer -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter919020 = new Ya.Metrika({id:919020,
                                webvisor:true,
                                clickmap:true,
                                trackLinks:true,
                                accurateTrackBounce:true,
                                trackHash:true});
                    } catch(e) { }
                });
            
                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
            
                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks");
            </script>
<noscript><div><img alt="" src="//mc.yandex.ru/watch/919020" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- begin of Top100 code -->
<script id="top100Counter" src="//counter.rambler.ru/top100.jcn?3081319" type="text/javascript"></script>
<noscript><a href="//top100.rambler.ru/navi/3081319/"><img alt="Rambler's Top100" border="0" src="//counter.rambler.ru/top100.cnt?3081319"/></a></noscript>
<!-- end of Top100 code -->
<!--Rating@Mail.ru counter-->
<script type="text/javascript"> 
            var _tmr = window._tmr || (window._tmr = []); 
            _tmr.push({id: "1471190", type: "pageView", start: (new Date()).getTime()}); 
            (function (d, w, id) { 
            if (d.getElementById(id)) return; 
            var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id; 
            ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js"; 
            var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);}; 
            if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } 
            })(document, window, "topmailru-code"); 
          </script>
<!--//Rating@Mail.ru counter-->
<!-- Google Analitycs -->
<script> 
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ 
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), 
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) 
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); 
            
            ga('create', 'UA-15838940-1', 'auto'); 
            ga('send', 'pageview'); 
          
          </script>
<!-- Google Analitycs END -->
</span>
</div>
<span class="footer-dev">
<span>Поддержка сайта: <a href="http://веб-аудитор.рф/development/" target="_blank">WEB Аудитор</a></span>
</span>
</div>
</div>
</footer>
<script src="/assets/templates/site/js/jquery.submitForm.js" type="text/javascript"></script>
<script src="/assets/templates/site/js/common.js" type="text/javascript"></script>
<script src="/assets/templates/site/js/survey.js" type="text/javascript"></script>
<script defer="" src="/assets/components/badv/js/web/init.js" type="text/javascript"></script>
<!-- Код тега ремаркетинга Google -->
<script type="text/javascript">
  /* <![CDATA[ */
  var google_conversion_id = 949753931;
  var google_custom_params = window.google_tag_params;
  var google_remarketing_only = true;
  /* ]]> */
</script>
<script src="//www.googleadservices.com/pagead/conversion.js" type="text/javascript"></script>
<noscript>
<div style="display:inline;">
<img alt="" height="1" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/949753931/?guid=ON&amp;script=0" style="border-style:none;" width="1"/>
</div>
</noscript>
<script src="/assets/templates/site/js/min/jquery.bxslider.min.js" type="text/javascript"></script>
<script type="text/javascript">
      (function(){
        var $items = $('.topic-desc-item');
        
        var slider = $('#topic-slider').bxSlider({
          mode: 'fade',
          onSlideBefore: function($slideElement, oldIndex, newIndex){
            $('.topic-desc-item.__active').removeClass('__active');
            $($items[newIndex]).addClass('__active');
          }
        });
        
        $('.topic-item').click(function(){
          location.href = this.href;
        });
        
        $('.topic-item').mouseover(function(){
          var index = $(this).data('slide-index');
          slider.goToSlide(index);
        });
      })();
    </script>
</body>
</html>