<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Duke Blue Devils BASKETBALL Tickets - 2021 - 2022</title>
<link href="https://ballparks.com/tickets/stylesheet-site.css" rel="stylesheet" type="text/css"/>
<link href="https://ballparks.com/tickets/stylesheet-custom.css" rel="stylesheet" type="text/css"/>
<script src="https://ballparks.com/tickets/cssutils.js" type="text/javascript"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-326787-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-326787-1');
</script>
</head>
<body bgcolor="#ffffff" bottommargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="changecss('.sideborder','borderColor','1e771b')" rightmargin="0" topmargin="0">
<table align="center" bgcolor="#FFFFFF" cellpadding="0" cellspacing="2" width="778">
<tr><td colspan="2"><img alt="" border="0" height="6" src="https://ballparks.com/tickets/images/blank.gif" width="1"/></td></tr>
<tr>
<td align="left" colspan="2"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td><img alt="ballparks logo" border="0" height="54" src="https://ballparks.com/tickets/images/bplogo.gif" width="299"/></td><td align="right"><img alt="ticket triangle logo" border="0" height="44" src="https://ballparks.com/tickets/images/ttlogo.gif" width="348"/></td></tr></table></td>
</tr>
<tr><td colspan="2"><img alt="" border="0" height="3" src="https://ballparks.com/tickets/images/blank.gif" width="1"/></td></tr>
<tr><td bgcolor="#006000" colspan="2"><img alt="" border="0" height="2" src="https://ballparks.com/tickets/images/blank.gif" width="760"/></td></tr>
<tr><td><img alt="" border="0" height="1" src="https://ballparks.com/tickets/images/blank.gif" width="1"/></td></tr>
<tr>
<td align="left" bgcolor="#1e771b" class="sideborder" valign="top">
<table align="center">
<tr>
<td align="center">
<table align="center" cellpadding="0" cellspacing="0">
<tr><td><img alt="" border="0" height="10" src="https://ballparks.com/tickets/images/blank.gif" width="200"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="../sports/duke_blue_devils_tickets.htm">Duke Blue Devils Tickets</a> </td></tr><tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr><tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="../sports/florida_state_seminoles_tickets.htm">Florida State Seminoles Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="6" src="blank.gif" width="1"/></td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="../sports/sports_tickets.htm">Sports Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="https://ballparks.com/tickets/nfl/super_bowl_tickets.htm">Super Bowl Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="https://ballparks.com/tickets/ncaafootball/ncaa_football_bowl_tickets.htm">NCAA Bowl Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="https://ballparks.com/tickets/ncaabasketball/ncaa_basketball_tournament.htm">NCAA Tournament Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="https://ballparks.com/tickets/u2/u2_tickets.htm">U2 Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="../ncaafootball/ncaa_football_tickets.htm">NCAA Football Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="../sports/college_sports_tickets.htm">College Sports Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="../ncaabasketball/ncaa_basketball_tickets.htm">NCAA Basketball Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="../nhl/nhl_tickets.htm">NHL Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="../nfl/nfl_tickets.htm">NFL Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="../nba/nba_tickets.htm">NBA Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="../mlb_tickets.htm">MLB Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="../concert_tickets/index.htm">Concert Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="../theater/theater_tickets.htm">Theater &amp; Musical Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="../other/other_tickets.htm">Other Event Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="4" src="blank.gif" width="1"/></td></tr>
<tr><td class="sidedash" nowrap="">  <a class="sidemenu" href="../concerts2/concert_tickets.htm">Alternate Concert Tickets</a> </td></tr>
<tr><td><img alt="" border="0" height="10" src="blank.gif" width="1"/></td></tr>
</table></td>
</tr>
</table>
</td>
<td align="center" valign="top">
<table border="0" cellpadding="4" cellspacing="4" width="100%">
<tr><td><p align="justify"><a href="https://ballparks.com">Ballparks</a> is now proud to announce our affiliation with <a class="pagelinks" href="https://tickettriangle.com">TicketTriangle.com</a>, an independent ticket agency offering BASKETBALL for all sports and teams at all venues.  If you would like to attend a BASKETBALL event or to view BASKETBALL schedules and information, Ballparks and TicketTriangle is your source. Just click the team link below to find Duke Blue Devils ticket information.</p></td></tr>
<tr>
<td align="left" class="indypage_sports_header" valign="top">Duke Blue Devils BASKETBALL Tickets</td>
</tr>
<tr>
<td>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr>
<td valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="left"><a class="mainpage_sports_link_medium" href="http://orders.tickettriangle.com/ResultsGeneral.aspx?stype=2&amp;kwds=Duke Blue Devils">Duke Blue Devils Tickets</a></td>
</tr>
<tr><td><img alt="" border="0" height="6" src="https://ballparks.com/tickets/images/blank.gif" width="1"/></td></tr>
<tr>
<td>
<table bgcolor="#1e771b" cellpadding="3" width="100%">
<tr><td colspan="3"><strong><font color="#FFFFFF">2021 - 2022 Schedule of Events</font></strong></td></tr>
<tr bgcolor="#cccccc"><td valign="top" width="160"><a href="pittsburgh_panthers_duke_blue_devils_basketball_sports_tickets.htm">Pittsburgh Panthers vs. Duke Blue Devils</a></td><td valign="top">Petersen Events Center, Pittsburgh, PA</td><td nowrap="" valign="top">01/19/2021 9:00PM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=Pittsburgh Panthers vs. Duke Blue Devils" rel="nofollow"><strong>Buy Now</strong></a></td></tr><tr bgcolor="#aaaaaa"><td valign="top" width="160"><a href="louisville_cardinals_duke_blue_devils_basketball_sports_tickets.htm">Louisville Cardinals vs. Duke Blue Devils</a></td><td valign="top">KFC Yum! Center, Louisville, KY</td><td nowrap="" valign="top">01/23/2021 4:00PM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=Louisville Cardinals vs. Duke Blue Devils" rel="nofollow"><strong>Buy Now</strong></a></td></tr><tr bgcolor="#cccccc"><td valign="top" width="160"><a href="duke_blue_devils_georgia_tech_yellow_jackets_basketball_sports_tickets.htm">Duke Blue Devils vs. Georgia Tech Yellow Jackets</a></td><td valign="top">Cameron Indoor Stadium, Durham, NC</td><td nowrap="" valign="top">01/26/2021 9:00PM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=Duke Blue Devils vs. Georgia Tech Yellow Jackets" rel="nofollow"><strong>Buy Now</strong></a></td></tr><tr bgcolor="#aaaaaa"><td valign="top" width="160"><a href="duke_blue_devils_clemson_tigers_basketball_sports_tickets.htm">Duke Blue Devils vs. Clemson Tigers</a></td><td valign="top">Cameron Indoor Stadium, Durham, NC</td><td nowrap="" valign="top">01/30/2021 12:00PM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=Duke Blue Devils vs. Clemson Tigers" rel="nofollow"><strong>Buy Now</strong></a></td></tr><tr bgcolor="#cccccc"><td valign="top" width="160"><a href="miami_hurricanes_duke_blue_devils_basketball_sports_tickets.htm">Miami Hurricanes vs. Duke Blue Devils</a></td><td valign="top">The Watsco Center At UM, Miami, FL</td><td nowrap="" valign="top">02/01/2021 7:00PM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=Miami Hurricanes vs. Duke Blue Devils" rel="nofollow"><strong>Buy Now</strong></a></td></tr><tr bgcolor="#aaaaaa"><td valign="top" width="160"><a href="duke_blue_devils_north_carolina_tar_heels_basketball_sports_tickets.htm">Duke Blue Devils vs. North Carolina Tar Heels</a></td><td valign="top">Cameron Indoor Stadium, Durham, NC</td><td nowrap="" valign="top">02/06/2021 6:00PM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=Duke Blue Devils vs. North Carolina Tar Heels" rel="nofollow"><strong>Buy Now</strong></a></td></tr><tr bgcolor="#cccccc"><td valign="top" width="160"><a href="duke_blue_devils_notre_dame_fighting_irish_basketball_sports_tickets.htm">Duke Blue Devils vs. Notre Dame Fighting Irish</a></td><td valign="top">Cameron Indoor Stadium, Durham, NC</td><td nowrap="" valign="top">02/09/2021 9:00PM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=Duke Blue Devils vs. Notre Dame Fighting Irish" rel="nofollow"><strong>Buy Now</strong></a></td></tr><tr bgcolor="#aaaaaa"><td valign="top" width="160"><a href="north_carolina_state_wolfpack_duke_blue_devils_basketball_sports_tickets.htm">North Carolina State Wolfpack vs. Duke Blue Devils</a></td><td valign="top">PNC Arena, Raleigh, NC</td><td nowrap="" valign="top">02/13/2021 4:00PM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=North Carolina State Wolfpack vs. Duke Blue Devils" rel="nofollow"><strong>Buy Now</strong></a></td></tr><tr bgcolor="#cccccc"><td valign="top" width="160"><a href="wake_forest_demon_deacons_duke_blue_devils_basketball_sports_tickets.htm">Wake Forest Demon Deacons vs. Duke Blue Devils</a></td><td valign="top">Lawrence Joel Veterans Memorial Coliseum, Winston Salem, NC</td><td nowrap="" valign="top">02/17/2021 8:30PM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=Wake Forest Demon Deacons vs. Duke Blue Devils" rel="nofollow"><strong>Buy Now</strong></a></td></tr><tr bgcolor="#aaaaaa"><td valign="top" width="160"><a href="duke_blue_devils_virginia_cavaliers_basketball_sports_tickets.htm">Duke Blue Devils vs. Virginia Cavaliers</a></td><td valign="top">Cameron Indoor Stadium, Durham, NC</td><td nowrap="" valign="top">02/20/2021 3:30AM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=Duke Blue Devils vs. Virginia Cavaliers" rel="nofollow"><strong>Buy Now</strong></a></td></tr><tr bgcolor="#cccccc"><td valign="top" width="160"><a href="duke_blue_devils_syracuse_orange_basketball_sports_tickets.htm">Duke Blue Devils vs. Syracuse Orange</a></td><td valign="top">Cameron Indoor Stadium, Durham, NC</td><td nowrap="" valign="top">02/22/2021 7:00PM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=Duke Blue Devils vs. Syracuse Orange" rel="nofollow"><strong>Buy Now</strong></a></td></tr><tr bgcolor="#aaaaaa"><td valign="top" width="160"><a href="duke_blue_devils_louisville_cardinals_basketball_sports_tickets.htm">Duke Blue Devils vs. Louisville Cardinals</a></td><td valign="top">Cameron Indoor Stadium, Durham, NC</td><td nowrap="" valign="top">02/27/2021 6:00PM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=Duke Blue Devils vs. Louisville Cardinals" rel="nofollow"><strong>Buy Now</strong></a></td></tr><tr bgcolor="#cccccc"><td valign="top" width="160"><a href="georgia_tech_yellow_jackets_duke_blue_devils_basketball_sports_tickets.htm">Georgia Tech Yellow Jackets vs. Duke Blue Devils</a></td><td valign="top">McCamish Pavilion, Atlanta, GA</td><td nowrap="" valign="top">03/02/2021 7:00PM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=Georgia Tech Yellow Jackets vs. Duke Blue Devils" rel="nofollow"><strong>Buy Now</strong></a></td></tr><tr bgcolor="#aaaaaa"><td valign="top" width="160"><a href="north_carolina_tar_heels_duke_blue_devils_basketball_sports_tickets.htm">North Carolina Tar Heels vs. Duke Blue Devils</a></td><td valign="top">Dean E. Smith Center, Chapel Hill, NC</td><td nowrap="" valign="top">03/06/2021 6:00PM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=North Carolina Tar Heels vs. Duke Blue Devils" rel="nofollow"><strong>Buy Now</strong></a></td></tr><tr bgcolor="#cccccc"><td valign="top" width="160"><a href="duke_blue_devils_pittsburgh_panthers_basketball_sports_tickets.htm">Duke Blue Devils vs. Pittsburgh Panthers</a></td><td valign="top">Cameron Indoor Stadium, Durham, NC</td><td nowrap="" valign="top">12/29/2070 8:00PM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=Duke Blue Devils vs. Pittsburgh Panthers" rel="nofollow"><strong>Buy Now</strong></a></td></tr><tr bgcolor="#aaaaaa"><td valign="top" width="160"><a href="florida_state_seminoles_duke_blue_devils_basketball_sports_tickets.htm">Florida State Seminoles vs. Duke Blue Devils</a></td><td valign="top">Donald L. Tucker Civic Center, Tallahassee, FL</td><td nowrap="" valign="top">01/02/2071 8:00PM</td><td nowrap="" valign="middle"><a class="columnlist" href="http://orders.tickettriangle.com/ResultsEvent.aspx?event=Florida State Seminoles vs. Duke Blue Devils" rel="nofollow"><strong>Buy Now</strong></a></td></tr>
</table>
</td>
</tr>
<tr><td><img alt="" border="0" height="6" src="https://ballparks.com/tickets/images/blank.gif" width="1"/></td></tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr valign="top">
<td>
<p align="justify">To order BASKETBALL tickets online please click the above link.  We specialize in selling tickets to all types of sporting events.</p>
<p align="justify">TicketTriangle.com is a top source for Duke Blue Devils tickets on the Internet! We can help provide premium Duke Blue Devils tickets or any other BASKETBALL event as well as other major sports events throughout the country. We put you into quality seats for the Duke Blue Devils ticket of your choice.</p>
<p align="justify">We can always locate the hard to find Duke Blue Devils BASKETBALL along with seats for any other BASKETBALL event. We carry Duke Blue Devils tickets, some of the best seats available, including front row, and many of our tickets are fairly cheap and a good bargain. Most of our inventory are sold out BASKETBALL events. You can order your discount Duke Blue Devils tickets through the TicketTriangle website 24 hours a day 7 days a week.</p>
</td>
</tr>
<tr><td><img alt="" border="0" height="20" src="blank.gif" width="1"/></td></tr>
</table>
</td>
</tr>
</table>
</body>
</html>
