<html>
<head>
<title>Access denied</title>
<style>.wrapper{font-family:courier,arial,helvetica;max-width:600px;position:absolute;left:20px;right:20px;margin:auto;top:50%;padding-bottom:40px;text-align:center;transform:translate3d(0,-50%,0)}img{width:280px;margin:0 auto 4em;display:block;opacity:.6}.meta{font-size:80%}</style>
</head>
<body>
<div class="wrapper">
<div class="cf-error-details cf-error-1020">
<h1>Access denied</h1>
<p>This website is using a security service to protect itself from online attacks.</p>
<ul class="cferror_details">
<li>Ray ID: 611be2ac289b2308</li>
<li>Timestamp: 2021-01-15 01:39:52 UTC</li>
<li>Your IP address: 2001:250:3c02:749:e444:4db8:6cea:7c56</li>
<li class="XXX_no_wrap_overflow_hidden">Requested URL: www.alternativeto.net/software/it-asset-tool/ </li>
<li>Error reference number: 1020</li>
<li>Server ID: FL_23F346</li>
<li>User-Agent: python-requests/2.22.0</li>
</ul>
</div>
<p>
Questions? Please ping us on twitter
<a href="https://twitter.com/alternativeto">@alternativeto</a> or by
e-mail support@alternativeto.net. Please include the error id and / or
your IP.
</p>
<p class="meta">Your ip: 2001:250:3c02:749:e444:4db8:6cea:7c56, Error ID: 611be2ac289b2308</p>
</div>
</body>
</html>