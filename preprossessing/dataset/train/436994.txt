<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- DW6 -->
<head>
<!-- Copyright 2005 Macromedia, Inc. All rights reserved. -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="noindex" name="robots"/>
<!--authpro additions for security-->
<meta content="-1" http-equiv="Expires"/>
<meta content="no-cache" http-equiv="Pragma"/>
<meta content="noarchive" name="robots"/>
<meta content="noindex" name="robots"/>
<!--end authpro additions-->
<title>DP3, Inc. Error Page</title>
<link href="ns3/v3.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="masthead">
<h1 id="siteName"><img alt="logo" src="images/logo.gif"/>	DP3, Inc. </h1>
<div id="globalNav">
<table border="0" width="100%">
<tr>
<td align="left" width="50%"> </td>
</tr>
</table>
</div>
<h2 align="center" id="pageName">DP3 Error Page</h2>
<p>If you have reached this error page from a link within the DP3 web site, we regret that we had to remove this page for confidentiality reasons.  We apologize for the inconvenience.  Please contact a DP3 trustee with any questions.</p>
</div>
<!--end headlines -->
<div align="center" id="siteInfo">
<a href="http://www.dp3.org/ns3/about_DP3.html" onclick="return popitup3('http://www.dp3.org/ns3/about_DP3.html')" onmouseout="hideddrivetip()" onmouseover="ddrivetip('Brief Fact Sheet about DP3','#9DBAE6', 300)" target="_blank">About DP3</a> | <a href="http://www.dp3.org/ns3/privacy3.html" onmouseout="hideddrivetip()" onmouseover="ddrivetip('What we do with information we collect about our members.','#9DBAE6', 300)">Privacy Policy</a> | <a href="http://www.dp3.org/ns/VEBA.html" onmouseout="hideddrivetip()" onmouseover="ddrivetip('Click for the VBT home page.&lt;br /&gt;&lt;br /&gt;The Voluntary Benefits Trust for Airline Retirees is a health insurance program for retired pilots who do not have other viable options for their health insurance needs.','#9DBAE6', 300)" target="_blank">VBT</a> | <a href="http://www.dp3.org/ns3/contact3.html" onmouseout="hideddrivetip()" onmouseover="ddrivetip('Click this link to contact the DP3 trustees.  Fill out the web form and submit; a DP3 trustee will respond as soon as possible.','#9DBAE6', 300)">Contact Us</a> <br/> 
©2017 | <font color="#000000">DP3, Inc., 8014 Cumming Hwy, Ste 403-333, Canton, GA 30115</font> | Fax #: 888-505-1242 or 678-493-8616 
</div>
</body>
</html>
