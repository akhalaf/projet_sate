<!DOCTYPE html>
<!--[if IEMobile 7]><html class="no-js iem7 oldie"><![endif]--><!--[if (IE 7)&!(IEMobile)]><html class="no-js ie7 oldie" lang="en"><![endif]--><!--[if (IE 8)&!(IEMobile)]><html class="no-js ie8 oldie" lang="en"><![endif]--><!--[if (gt IE 8)|(gt IEMobile 7)]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<title>22Social | Redirect</title>
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta content="Upgrade Your Fan Page" property="og:title"/>
<meta content="Increase Engagement with Stunning Media, Build Custom Audiences, Collect Real Leads, Sell Digital Products and Reward Your Best Customers - All On One App." property="og:description"/>
<meta content="https://www.22s.com/assets/images/share-details2.png" property="og:image"/>
<meta content="http://www.22s.com/" property="og:url"/>
<meta content="0" property="fb:admins"/>
<!-- http://davidbcalhoun.com/2010/viewport-metatag -->
<meta content="True" name="HandheldFriendly"/>
<meta content="320" name="MobileOptimized"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<!-- For all browsers -->
<link href="https://c388595.ssl.cf1.rackcdn.com/developr/css/reset.css" rel="stylesheet" type="text/css"/>
<link href="https://www.22s.com/assets/css/style.css?v=1610637395" rel="stylesheet"/>
<link href="https://c388595.ssl.cf1.rackcdn.com/developr/css/colors.css" rel="stylesheet" type="text/css"/>
<link href="https://c388595.ssl.cf1.rackcdn.com/developr/css/print.css" media="print" rel="stylesheet" type="text/css"/>
<!-- For progressively larger displays -->
<link href="https://c388595.ssl.cf1.rackcdn.com/developr/css/480.css" media="only all and (min-width: 480px)" rel="stylesheet" type="text/css"/>
<link href="https://c388595.ssl.cf1.rackcdn.com/developr/css/768.css" media="only all and (min-width: 768px)" rel="stylesheet" type="text/css"/>
<link href="https://c388595.ssl.cf1.rackcdn.com/developr/css/992.css" media="only all and (min-width: 992px)" rel="stylesheet" type="text/css"/>
<link href="https://c388595.ssl.cf1.rackcdn.com/developr/css/1200.css" media="only all and (min-width: 1200px)" rel="stylesheet" type="text/css"/>
<link href="https://c388595.ssl.cf1.rackcdn.com/developr/css/2x.css" media="only all and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min-device-pixel-ratio: 1.5)" rel="stylesheet" type="text/css"/>
<!-- Webfonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Satisfy" rel="stylesheet" type="text/css"/>
<!-- Additional styles -->
<link href="https://c388595.ssl.cf1.rackcdn.com/developr/css/styles/form.css" rel="stylesheet" type="text/css"/>
<link href="https://c388595.ssl.cf1.rackcdn.com/developr/css/styles/switches.css" rel="stylesheet" type="text/css"/>
<link href="https://c388595.ssl.cf1.rackcdn.com/developr/css/styles/dashboard.css" rel="stylesheet" type="text/css"/>
<!-- JavaScript at bottom except for Modernizr -->
<script src="https://c388595.ssl.cf1.rackcdn.com/developr/js/libs/modernizr.custom.js" type="text/javascript"></script>
<!-- For Modern Browsers -->
<link href="https://3169d7e8b6983f35af09-c0960d9221f27f5f328b8302c50d3e23.ssl.cf1.rackcdn.com/developr/img/favicons/favicon.png" rel="shortcut icon"/>
<!-- For everything else -->
<link href="https://3169d7e8b6983f35af09-c0960d9221f27f5f328b8302c50d3e23.ssl.cf1.rackcdn.com/developr/img/favicons/favicon.ico" rel="shortcut icon"/>
<!-- For retina screens -->
<link href="https://3169d7e8b6983f35af09-c0960d9221f27f5f328b8302c50d3e23.ssl.cf1.rackcdn.com/developr/img/favicons/apple-touch-icon-retina.png" rel="apple-touch-icon-precomposed" sizes="114x114"/>
<!-- For iPad 1-->
<link href="https://3169d7e8b6983f35af09-c0960d9221f27f5f328b8302c50d3e23.ssl.cf1.rackcdn.com/developr/img/favicons/apple-touch-icon-ipad.png" rel="apple-touch-icon-precomposed" sizes="72x72"/>
<!-- For iPhone 3G, iPod Touch and Android -->
<link href="https://3169d7e8b6983f35af09-c0960d9221f27f5f328b8302c50d3e23.ssl.cf1.rackcdn.com/developr/img/favicons/apple-touch-icon.png" rel="apple-touch-icon-precomposed"/>
<!-- iOS web-app metas -->
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="black" name="apple-mobile-web-app-status-bar-style"/>
<!-- Startup image for web apps -->
<link href="https://3169d7e8b6983f35af09-c0960d9221f27f5f328b8302c50d3e23.ssl.cf1.rackcdn.com/developr/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)" rel="apple-touch-startup-image"/>
<link href="https://3169d7e8b6983f35af09-c0960d9221f27f5f328b8302c50d3e23.ssl.cf1.rackcdn.com/developr/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)" rel="apple-touch-startup-image"/>
<link href="https://3169d7e8b6983f35af09-c0960d9221f27f5f328b8302c50d3e23.ssl.cf1.rackcdn.com/developr/img/splash/iphone.png" media="screen and (max-device-width: 320px)" rel="apple-touch-startup-image"/>
<!-- Microsoft clear type rendering -->
<meta content="on" http-equiv="cleartype"/>
<style type="text/css">
		html {
			background: fixed;
			-webkit-background-size: auto;
			-moz-background-size: auto;
			-o-background-size: auto;
			background-size: auto;
			background-color: #FFFFFF;
			/* IE10 Consumer Preview */ 
			background-image: -ms-radial-gradient(center top, circle closest-corner, #FFFFFF 0%, #E7EBF2 100%);
			/* Mozilla Firefox */ 
			background-image: -moz-radial-gradient(center top, circle closest-corner, #FFFFFF 0%, #E7EBF2 100%);
			/* Opera */ 
			background-image: -o-radial-gradient(center top, circle closest-corner, #FFFFFF 0%, #E7EBF2 100%);
			/* Webkit (Safari/Chrome 10) */ 
			background-image: -webkit-gradient(radial, center top, 0, center top, 487, color-stop(0, #FFFFFF), color-stop(1, #E7EBF2));
			/* Webkit (Chrome 11+) */ 
			background-image: -webkit-radial-gradient(center top, circle closest-corner, #FFFFFF 0%, #E7EBF2 100%);
			/* W3C Markup, IE10 Release Preview */ 
			background-image: radial-gradient(circle closest-corner at center top, #FFFFFF 0%, #E7EBF2 100%);		
			}
	</style>
</head>
<body class="clearfix with-menu with-shortcuts">
<!-- Prompt IE 6 users to install Chrome Frame -->
<!--[if lt IE 7]><p class="message red-gradient simpler">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
<div style="width:100%;">
<div style="position: absolute; top: 50%; margin-top: -50px; left: 0; width: 100%;">
<div class="align-center" style="margin-left: auto; margin-right: auto; width: 100%; height: 100px">
<h1 class="thin" style="text-shadow: 0 0 #FFF;">
<!-- <span class="icon-clock green">-->
<span>
</span> Loading...
	
	</h1>
</div>
</div>
</div>
<script type="text/javascript">

    setTimeout("tts_redirect()", 1000);
	
	function tts_redirect() {
		
		window.location = 'http://www.facebook.com/22Social/app/147250335456844';
    }
    
</script>
<div id="footer"></div>
</body>
</html>