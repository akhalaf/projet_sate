<html><body><p>ï»¿<!DOCTYPE html>

</p>
<meta charset="utf-8"/>
<title>Lawren Harris - Autumn Harbour - A lost painting found</title>
<meta content="width=980" name="viewport"/>
<meta content="en-gb" http-equiv="Content-Language"/>
<meta content="Lawren Harris, Autumn Harbour, lost painting found" name="keywords"/>
<meta content="Lawren Harris Autumn Harbour a lost painting found. A long lost Lawren Harris painting may have been found. Dr. Kelly Akers of ProSpect Scientific Inc. has arranged for an examination of two Canadian oil paintings." name="description"/>
<meta content="Autumn Harbour" name="author"/>
<meta content="Â© intellectual copyright - www.autumnharbour.ca - 2014" name="copyright"/>
<meta content="2019-03-22T18:28:49+00:00" name="date"/>
<meta content="index,follow" name="robots"/>
<link charset="utf-8" href="css/styles.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/slider.js" type="text/javascript"></script>
<script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="js/totop.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		/* var defaults = {
  			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
 		}; */
		$().UItoTop({ easingType: 'easeOutQuart' });
	});
</script>
<link href="wpscripts/wpstyles.css" rel="stylesheet" type="text/css"/>
<link href="wpscripts/nav_172style.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
      /*Master Page StyleSheet*/
#main .header { background: transparent url(css/images/header2.jpg) top right repeat-x; }
#main .footer { background: transparent url(css/images/footer2.jpg) bottom right repeat-x; }
.head-shadow { text-shadow: 0px 2px 0px #000;}




#wplightbox_captiontable {

top:550px !Important;/* The main caption position */
}            
#wplightbox_table {
top:190px !Important;/* The main lightbox position */ 
}
#wplightbox_closediv {
 margin:150.0px 0.0px 0.0px +42.0px !Important;/* The close button position */
}



      .P-1 { text-align:center;line-height:1px;font-family:"Trebuchet MS", sans-serif;font-style:normal;font-weight:normal;color:#ebebeb;background-color:transparent;font-variant:normal;font-size:13.0px;vertical-align:0; }
      .C-1 { line-height:18.00px;font-family:"Trebuchet MS", sans-serif;font-style:normal;font-weight:normal;color:#ebebeb;background-color:transparent;text-decoration:none;font-variant:normal;font-size:13.3px;vertical-align:0; }
      .P-2 { text-align:center;line-height:1px;font-family:"Verdana", sans-serif;font-style:italic;font-weight:normal;color:#dbdbdb;background-color:transparent;font-variant:normal;font-size:12.0px;vertical-align:0; }
      .C-2 { line-height:14.00px;font-family:"Verdana", sans-serif;font-style:italic;font-weight:normal;color:#dbdbdb;background-color:transparent;text-decoration:none;font-variant:normal;font-size:12.0px;vertical-align:0; }
      .P-3 { text-align:center;margin-bottom:0.0px;line-height:1px;font-family:"Trebuchet MS", sans-serif;font-style:italic;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:13.0px;vertical-align:0; }
      .C-3 { line-height:18.00px;font-family:"Trebuchet MS", sans-serif;font-style:italic;font-weight:normal;color:#ffffff;background-color:transparent;text-decoration:none;font-variant:normal;font-size:13.3px;vertical-align:0; }
      .P-4 { text-align:center;margin-bottom:0.0px;line-height:1px;font-family:"Trebuchet MS", sans-serif;font-style:normal;font-weight:normal;color:#ebebeb;background-color:transparent;font-variant:normal;font-size:13.0px;vertical-align:0; }
      .C-4 { line-height:18.00px;font-family:"Trebuchet MS", sans-serif;font-style:italic;font-weight:normal;color:#ebebeb;background-color:transparent;text-decoration:none;font-variant:normal;font-size:13.3px;vertical-align:0; }
      .P-5 { text-align:center;line-height:1px;font-family:"Trebuchet MS", sans-serif;font-style:italic;font-weight:normal;color:#ebebeb;background-color:transparent;font-variant:normal;font-size:19.0px;vertical-align:0; }
      .C-5 { line-height:24.00px;font-family:"Trebuchet MS", sans-serif;font-style:italic;font-weight:normal;color:#ebebeb;background-color:transparent;text-decoration:none;font-variant:normal;font-size:18.7px;vertical-align:0; }
      .OBJ-1 { background:#000000; }
      .P-6 { text-align:center;line-height:1px;font-family:"Tahoma", sans-serif;font-style:italic;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:11.0px;vertical-align:0; }
      .C-6 { line-height:13.00px;font-family:"Tahoma", sans-serif;font-style:italic;font-weight:normal;color:#ffffff;background-color:transparent;text-decoration:none;font-variant:normal;font-size:10.7px;vertical-align:0; }
      .C-7 { line-height:13.00px;font-family:"Tahoma", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;text-decoration:none;font-variant:normal;font-size:10.7px;vertical-align:0; }
    </style>
<script src="wpscripts/jsNavBarFuncs.js" type="text/javascript"></script>
<script src="wpscripts/wp_navbar_menub.js" type="text/javascript"></script>
<script src="wpscripts/nav_172tree.js" type="text/javascript"></script>
<script src="wpscripts/jquery.js" type="text/javascript"></script>
<script src="wpscripts/jquery.event.move.js" type="text/javascript"></script>
<script src="wpscripts/jquery.event.swipe.js" type="text/javascript"></script>
<script src="wpscripts/jquery.wpslider.js" type="text/javascript"></script>
<script type="text/javascript">
      $(document).ready(function() {
      $slider_19.restrictSlides();
      });
    </script>
<script src="jquery.vegas.min.js" type="text/javascript"></script>
<link href="background-gallery-styles.css" rel="stylesheet" type="text/css"/>
<script charset="utf-8" type="text/javascript">
$(function() {
 $.vegas('slideshow', {
  delay:3300,
  backgrounds:[
    { src:'backgrounds/08.gif', fade:1000 },
    { src:'backgrounds/10.gif', fade:1000 },
    { src:'backgrounds/11.gif', fade:1000 },
    { src:'backgrounds/09.gif', fade:1000 },
    { src:'backgrounds/07.gif', fade:1000 }

  ]
 })

 $.vegas('overlay', {
    src:'04.png'
 });
});
</script>
<style type="text/css">

.vegas-loading {display: none !important;}

</style>
<link href="favicon.ico" rel="icon" type="image/x-icon"/>
<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<!--Master Page Body Start-->
<div id="main">
<div class="header">
<div class="footer">
<div>
<div id="divMain" style="background:transparent;margin-left:auto;margin-right:auto;position:relative;width:980px;height:743px;">
<map id="map1" name="map1">
<area alt="" coords="89,42,92,31,91,24,91,17,88,17,88,37,85,40,81,42,79,39,76,36,77,29,77,17,75,17,75,36,71,41,68,40,63,38,65,30,65,17,61,17,61,28,62,38,64,43,71,45,77,41,80,43,85,45" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
<area alt="" coords="131,45,138,39,141,36,134,38,130,41,125,42,121,39,118,37,126,32,130,27,141,16,126,15,119,17,111,21,110,31,114,37,119,47" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
<area alt="" coords="214,29,214,1,211,1,211,22,193,22,193,1,190,1,190,43,193,43,193,24,211,24,211,43,214,43" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
<area alt="" coords="233,42,226,39,221,24,228,18,238,18,243,21,246,24,246,33,244,39,239,40" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
<area alt="" coords="116,36,113,23,120,18,131,18,133,20,128,25,124,28,121,32,118,35" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
<area alt="" coords="39,44,31,39,26,24,33,18,43,18,52,23,54,34,45,39" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
<area alt="" coords="291,30,291,17,289,17,289,43,291,43" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
<area alt="" coords="241,43,246,39,249,43,249,17,246,20,242,16,239,14,231,15,222,19,216,30,223,38,228,43,235,45" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
<area alt="" coords="165,33,165,22,164,21,162,17,158,15,153,15,148,17,147,18,144,17,144,43,147,43,147,23,148,22,150,18,157,17,160,21,164,27,162,37,162,43,165,43" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
<area alt="" coords="50,41,51,39,54,43,54,17,51,20,47,16,44,14,36,15,26,19,23,28,31,45,43,46" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
<area alt="" coords="316,37,313,32,312,29,308,28,305,25,303,24,302,20,306,19,310,21,310,24,315,21,310,16,304,16,300,19,298,29,305,30,309,30,312,35,311,38,309,42,302,42,302,34,299,36,299,41,302,44,313,44" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
<area alt="" coords="275,36,275,30,276,27,277,24,279,20,285,20,284,16,281,17,279,17,276,19,275,20,275,17,272,17,272,43,275,43" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
<area alt="" coords="259,36,259,32,260,28,260,27,261,24,263,21,267,20,268,20,268,16,265,17,262,18,261,20,259,21,259,17,256,17,256,43,259,43" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
<area alt="" coords="101,36,101,27,102,26,105,20,110,20,109,16,106,17,105,17,101,19,101,17,98,17,98,43,101,43" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
<area alt="" coords="292,12,292,11,293,8,288,8,288,13,290,14" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
<area alt="" coords="20,42,20,41,3,41,3,1,1,1,1,43,20,43" href="https://host4.uniservehosting.com:2096/webmaillogout.cgi" shape="poly" target="_blank"/>
</map>
<img alt="" src="wpimages/wpd59f80ae_06.png" style="position:absolute;left:276px;top:31px;width:321px;height:51px;" usemap="#map1"/>
<a href="http://en.wikipedia.org/wiki/Lawren_Harris" target="_blank">
<img alt="Lawren Harris - 1925" src="images/Lawren_Harris_1925_small.jpg" style="position:absolute;left:647px;top:8px;width:65px;height:86px;" title="Lawren Harris - 1925"/>
</a>
<div style="position:absolute;left:279px;top:79px;width:306px;height:21px;overflow:hidden;">
<p class="Body P-1"><span class="C-1">October 23, 1885 - January 29, 1970</span></p>
</div>
<div style="position:absolute;left:268px;top:13px;width:336px;height:19px;overflow:hidden;">
<h1 class="Body2 P-2"><span class="C-2">A LONG, LOST LAWREN HARRIS HAS BEEN FOUND</span></h1>
</div>
<div style="position:absolute;left:807px;top:56px;width:57px;height:44px;">
<script language="JavaScript">
<!--

//Disable right mouse click Script
//By Maximus (maximus@nsimail.com) w/ mods by DynamicDrive
//For full source code, visit http://www.dynamicdrive.com

var message="Site Copy Locked";

///////////////////////////////////
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}

function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}

if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}

document.oncontextmenu=new Function("alert(message);return false")

// --> 
</script>
</div><script id="nav_172_script" type="text/javascript">try {
        var navtree_nav_172 = WpNavBar.getNavTreeCustom( nav_172tree, {'m_sThisPageUrl':'index.html',
'm_sNavBarTarget':'_self',
'm_bFlash':false,
'm_bIncludeChildren':true,
'm_bIsCustom':true} );
        if( !navtree_nav_172 ) throw WpNavBar.getErrorObj( 'Link tree could not be read' );
        var nav_172 = new wp_navbar_menub("nav_172", navtree_nav_172, {'m_bVertical':false,'m_bPopupBelow':0,'m_bPopupRight':0,'m_iTimeOut':300,'m_iPopupAlignmentH':0,'m_sId':'nav_172',
'm_sScriptId':'nav_172_script',
'm_iLeft':40,
'm_iTop':676,
'm_sCssClass':'nav_172style',
'm_iWidth':900,
'm_iHeight':27}, {'m_iMaxCssLevel':1,
top:{'m_iHorizontalPadding':0,'m_iVerticalPadding':0,'m_iHorizontalPosition':1,'m_iVerticalPosition':0,'m_bWrap':true,'m_iHorizontalButtonSeparation':2,'m_iVerticalButtonSeparation':2,'m_bButtonsSameSize':false,'m_bStretchToWidth':false},
level1:{'m_bFirstPopupSameSize':true,'m_iMinWidth':120,'m_iFirstPopupOffset':36,'m_iInterPopupOffset':1,'m_iOpacity':100,'m_bFade':true,'m_iFadeSpeed':5,
separator:{'m_bAllowSeparators':true}}});
        } catch(e){
        document.write( '<div style="position:absolute;left:40;top:676;width:900;height:27">There was an error generating the navbar:<br>' + e.message + '<\/div>' );
        }
      </script><div style="position:absolute;left:399px;top:705px;width:182px;height:22px;overflow:hidden;"><p class="Body2 P-3"><span class="C-3">contact@autumnharbour.ca</span></p></div>
<img alt="" src="wpimages/wp1718fe35_06.png" style="position:absolute;left:196px;top:155px;width:604px;height:467px;"/>
<div style="position:absolute;left:377px;top:522px;width:225px;height:38px;overflow:hidden;">
<h2 class="Body P-4"><span class="C-1">Autumn Harbour by <span class="C-4">Lawren Harris</span></span></h2><h3 class="Body P-4"><span class="C-1">a long, lost picture</span></h3>
</div>
<div style="position:absolute;left:226px;top:173px;width:516px;height:27px;overflow:hidden;"><h1 class="Body P-5"><span class="C-5">Lawren Harris Autumn Harbour a lost painting found </span></h1></div><div class="OBJ-1" id="slider_19" style="position:absolute;left:201px;top:569px;width:577px;height:31px;overflow:hidden;"><div id="slider_19_P1" style="position:absolute;left:0px;right:0px;width:577px;height:31px;"><div style="position:absolute;left:9px;top:10px;width:564px;height:21px;overflow:hidden;"><p class="Slider-Body5 P-6"><span class="C-6">background image:<span class="C-7">  Lawren Harris - Autumn (Kempenfelt Bay, Lake Simcoe) 1916</span></span></p></div>
</div>
<div id="slider_19_P2" style="position:absolute;left:0px;right:0px;width:577px;height:31px;visibility:hidden;display:none;">
<div style="position:absolute;left:9px;top:10px;width:564px;height:21px;overflow:hidden;">
<p class="Slider-Body5 P-6"><span class="C-6">background image:<span class="C-7">  Lawren Harris - Hurdy Gurdy 1913</span></span></p>
</div>
</div>
<div id="slider_19_P3" style="position:absolute;left:0px;right:0px;width:577px;height:31px;visibility:hidden;display:none;">
<div style="position:absolute;left:9px;top:10px;width:564px;height:21px;overflow:hidden;">
<p class="Slider-Body5 P-6"><span class="C-6">background image:<span class="C-7">  Lawren Harris - January Thaw, Edge Of Town</span></span></p>
</div>
</div>
<div id="slider_19_P4" style="position:absolute;left:0px;right:0px;width:577px;height:31px;visibility:hidden;display:none;">
<div style="position:absolute;left:9px;top:10px;width:564px;height:21px;overflow:hidden;">
<p class="Slider-Body5 P-6"><span class="C-6">background image:<span class="C-7">  Lawren Harris - Old Houses, Wellington Street 1910</span></span></p>
</div>
</div>
<div id="slider_19_P5" style="position:absolute;left:0px;right:0px;width:577px;height:31px;visibility:hidden;display:none;">
<div style="position:absolute;left:9px;top:10px;width:564px;height:21px;overflow:hidden;">
<p class="Slider-Body5 P-6"><span class="C-6">background image:<span class="C-7">  Lawren Harris - Vacant House In The Ward</span></span></p>
</div>
</div>
</div>
<div style="position:absolute;left:41px;top:85px;width:161px;height:32px;"></div>
<img alt="Lawren Harris - Autumn Harbour" src="images/autumn_harbour_indexpage.jpg" style="position:absolute;left:282px;top:216px;width:417px;height:296px;" title="Lawren Harris - Autumn Harbour"/>
</div>
<script type="text/javascript">
      var $slider_19 = new $.fn.wpslider( { g_nDivId:'#slider_19', g_nTransitStyle:3, g_nLoopTime:3300, g_nAnimationTime:500, g_bPlayAtStart:true, g_bPlayAfterPress:true, g_bHandleMouseWheel:false, g_nStartPanel:2, g_strEasing: "swing" } );
    </script>
<!--Master Page Body End-->
</div></div>
<script src="js/fade.js" type="text/javascript"></script>
<script charset="utf-8" type="text/javascript">
  $(document).ready(function(){
    $("a[rel^='prettyPhoto']").prettyPhoto({theme:'facebook'});
  });
</script>
</div></div></body></html>