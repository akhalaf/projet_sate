<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Lima Central Catholic Sectional </title>
</head>
<body>
<h2 align="center"><font color="#0000FF" face="Arial">2011 Lima Central Catholic Division
III Sectional Wrestling Tournament<br/>
<small><small>February 18 &amp; 19, 2011</small></small></font></h2>
<font size="4">
<p align="center" style="BORDER-RIGHT: rgb(255,0,0) solid; BORDER-TOP: rgb(255,0,0) solid; BORDER-LEFT: rgb(255,0,0) solid; BORDER-BOTTOM: rgb(255,0,0) solid"></p></font><font size="3"><font color="#FF0000"><font face="Arial"><strong>Hit
refresh or reload to view latest information.   AOL users: Press [Ctrl] &amp;
[F5] simultaneously!</strong></font></font>
</font><div align="center"><center>
<table border="0" width="100%">
<tr>
<td><p align="left"><strong><big><font face="Arial"><a href="../../new_system/default.asp">Online
    Roster and Seed Form</a>  <br/>
<font color="#FF0000" size="2">   *Entry window opens: 8:00 AM, Sunday, January </font></font><font color="#FF0000" face="Arial" size="2">16<br/>
       *Entry window closes: 8:00 PM, Wednesday, February 16</font></big></strong></p></td>
</tr>
<tr>
<td><p align="left"><strong><font face="Arial"><big><a href="entrygrd.htm">Entry Grid</a>
       </big></font></strong></p></td>
</tr>
<tr>
<td><big><strong><font face="Arial"><a href="res11.htm">2011 Results</a>   </font><a href="brackets11.pdf"><font color="#0000FF" face="Arial">Brackets</font></a></strong></big></td>
</tr>
<tr>
<td><big><strong><font color="#0000FF" face="Arial">Archived Results: </font><a href="res10.htm"><font face="Arial">2010</font></a><font color="#0000FF" face="Arial"> </font><font face="Arial"><a href="res09.htm">2009</a></font><font color="#0000FF" face="Arial"> </font><font face="Arial"><a href="res08.htm">2008</a></font><font color="#0000FF" face="Arial"> </font><font face="Arial"><a href="res07.htm">2007</a> <a href="res06.htm">2006</a> <a href="res05.htm">2005</a> <a href="res04.htm">2004</a> <a href="res03.htm">2003</a></font></strong></big></td>
</tr>
</table>
</center></div><div align="center"><center>
<table border="0" width="100%">
<tr>
<td width="50%"><font face="Arial"><big><a href="mailto:help@baumspage.com"><strong>Help</strong></a><big>
</big></big></font></td>
<td width="50%"><p align="right"><a href="../index.htm"><strong><font face="Arial"><big>Back</big></font></strong></a></p></td>
</tr>
</table>
</center></div>
<p> </p>
</body>
</html>
