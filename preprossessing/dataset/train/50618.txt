<!DOCTYPE html>
<html lang="en"><head><title>
Malajube Tabs: 29 Tabs Total @ 911Tabs </title><meta charset="utf-8"/><meta content="text/html" http-equiv="Content-Type"/><meta content="Accurate Malajube guitar, bass, drum, piano, guitar pro and power tabs at 911Tabs.Com - tabs search engine" name="description"/><meta content="Malajube guitar tabs, bass tabs, chords, drum tabs, guitar pro, power tabs, piano tabs, sheet music, free guitar tab" name="keywords"/><meta content="IE=edge" http-equiv="X-UA-Compatible"/><meta content="width=1060" name="viewport"/><link href="http://www.911tabs.com/about/all_updates.xml.php" rel="alternate" title="911Tabs.com - All Updates" type="application/rss+xml"/><link href="/rss/songs_updates.xml" rel="alternate" title="911Tabs.com - New added songs" type="application/rss+xml"/><link href="/rss/top_tabs.xml" rel="alternate" title="911Tabs.com - Top 100 tabs" type="application/rss+xml"/><link href="/rss/top_bands.xml" rel="alternate" title="911Tabs.com - Top 100 bands" type="application/rss+xml"/><link href="http://www.911tabs.com/tabs/m/malajube/" rel="canonical"/><link href="/rss/m/malajube/rss.xml" rel="alternate" title="911Tabs.com - Malajube tabs" type="application/rss+xml"/><meta content="!" name="fragment"/><link href="https://plus.google.com/110625930262592885110" rel="publisher"/><link href="/public/img/touch-icon-iphone.png" rel="apple-touch-icon"/><link href="/public/img/touch-icon-ipad.png" rel="apple-touch-icon" sizes="76x76"/><link href="/public/img/touch-icon-iphone-retina.png" rel="apple-touch-icon" sizes="120x120"/><link href="/public/img/touch-icon-ipad-retina.png" rel="apple-touch-icon" sizes="152x152"/><link href="/public/css/style.css?2005054940" rel="stylesheet"/><!--[if IE]><script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script><script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]--><script src="https://yandex.st/jquery/1.8.0/jquery.min.js"></script><script>window.jQuery || document.write('<script src="/public/vendor/jquery-1.8.0.min.js"><\/script>')</script><!-- Google Analytics --><script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-4563747-1', '911tabs.com');
ga('require', 'displayfeatures');
ga('send', 'pageview');
</script><!-- End Google Analytics --></head><body class=""><script>
const isMobile = window.navigator.userAgent.match(/Mobile/) && !window.navigator.userAgent.match(/iPad/);
window.UGAPP = {};
window.UGAPP.cmp = {
footerMenuSelector: '.footer',
linkTemplate: ' / <a href="#" class="js-cmp-footer-trigger">Privacy Settings</a>',
theme: 'external'
}
window.UGAPP.config = {
is_mobile: isMobile,
staticPrefix: '//cdn.ustatik.com/',

isGDPRAvailable: false

}
<!-- region APR -->
window.UGAPP.bidding = {"analytics":{"logUrl":"\/\/track.ultimate-guitar.com\/ug\/","pageType":"911_911_band","region":"APR","device":null,"variation":null,"hasHardRefresh":true},"refresh":{"limit":20,"maxVisibleTime":1000,"delay":30000,"delayDeviation":10000,"enabled":true,"hasHardRefresh":true,"availableBidders":["appnexus","indexExchange","pubmatic","rubicon","sonobi","openx","adform","aol","brealtime","audienceNetwork","improvedigital","adkernelAdn","districtM","oftmedia"]},"minRefreshCpm":0.04,"cpmFloor":0.1,"needErrorLogging":false,"timeout":1000,"dfpId":74268401,"units":[{"code":"ad_cs_13848769_300_250","dfpCode":"911_ATF_300","sizes":[[300,250]],"placement":"ATF","bids":[{"bidder":"oftmedia","params":{"placementId":"13841237"}},{"bidder":"adform","params":{"mid":"596197"}},{"bidder":"appnexus","params":{"placementId":"13848769"}},{"bidder":"rubicon","params":{"zoneId":"1042042","siteId":"212208","accountId":"15178"}},{"bidder":"sonobi","params":{"dom_id":"911_ATF_300","placement_id":"f3d400e7eb7a9ccce391"}},{"bidder":"brealtime","params":{"placementId":"13882542"}},{"bidder":"districtM","params":{"placementId":"13889707"}},{"bidder":"indexExchange","params":{"id":"02","size":[300,250],"siteId":"296501"}},{"bidder":"pubmatic","params":{"adSlot":"1574313@300x250","publisherId":"72623"}}],"mediaType":"banner","appnexusPlacementId":13848769},{"code":"ad_cs_13848769_728_90","dfpCode":"911_ATF_728","sizes":[[728,90]],"placement":"ATF","bids":[{"bidder":"oftmedia","params":{"placementId":"13841252"}},{"bidder":"adform","params":{"mid":"596196"}},{"bidder":"appnexus","params":{"placementId":"13848769"}},{"bidder":"rubicon","params":{"accountId":"15178","siteId":"212208","zoneId":"1042042"}},{"bidder":"sonobi","params":{"dom_id":"911_ATF_728","placement_id":"d63d60eb59158bafa370"}},{"bidder":"brealtime","params":{"placementId":"13882547"}},{"bidder":"districtM","params":{"placementId":"13889714"}},{"bidder":"indexExchange","params":{"id":"03","size":[728,90],"siteId":"296502"}},{"bidder":"pubmatic","params":{"publisherId":"72623","adSlot":"1574314@911_728"}}],"mediaType":"banner","appnexusPlacementId":13848769},{"code":"ad_cs_13849140_300_250","dfpCode":"911_BTFL_300","sizes":[[300,250]],"placement":"BTFL","bids":[{"bidder":"oftmedia","params":{"placementId":"13841239"}},{"bidder":"adform","params":{"mid":"596226"}},{"bidder":"appnexus","params":{"placementId":"13849140"}},{"bidder":"rubicon","params":{"accountId":"15178","siteId":"212208","zoneId":"1042064"}},{"bidder":"sonobi","params":{"dom_id":"911_BTFL_300","placement_id":"b5ca3ded4117b95a82dd"}},{"bidder":"brealtime","params":{"placementId":"13882598"}},{"bidder":"districtM","params":{"placementId":"13889719"}}],"mediaType":"banner","appnexusPlacementId":13849140},{"code":"ad_cs_13849132_300_250","dfpCode":"911_BTFR_300","sizes":[[300,250]],"placement":"BTFR","bids":[{"bidder":"oftmedia","params":{"placementId":"13841240"}},{"bidder":"adform","params":{"mid":"596227"}},{"bidder":"appnexus","params":{"placementId":"13849132"}},{"bidder":"rubicon","params":{"zoneId":"1042066","siteId":"212208","accountId":"15178"}},{"bidder":"sonobi","params":{"dom_id":"911_BTFR_300","placement_id":"4448a362a4d1c7b0e25a"}},{"bidder":"brealtime","params":{"placementId":"13882599"}},{"bidder":"indexExchange","params":{"id":"05","size":[300,250],"siteId":"296504"}},{"bidder":"pubmatic","params":{"adSlot":"1574316@300x250","publisherId":"72623"}}],"mediaType":"banner","appnexusPlacementId":13849132}],"complexUnits":[],"refreshBidders":[],"priceGranularity":[{"precision":2,"min":0,"max":1,"increment":0.01},{"precision":2,"min":1,"max":2,"increment":0.02},{"precision":2,"min":2,"max":5,"increment":0.05},{"precision":2,"min":5,"max":10,"increment":0.1},{"precision":2,"min":10,"max":20,"increment":0.2},{"precision":2,"min":20,"max":50,"increment":0.5}],"dynamicLoad":true,"apstagEnabled":true};

window.UGAPP.bidding.isGDPRAvailable = window.UGAPP.config.isGDPRAvailable;

window.UGAPP.bidding.complexUnits = [];

window.UGAPP.bidding.slots = window.UGAPP.bidding.units.map(function(unit) {
return {
code: unit.code,
appnexusPlacementId: unit.appnexusPlacementId,
refresh: unit.refresh || {},
tag: null,
dynamic: unit.dynamic,
}
})
var pbjs = pbjs || {};
pbjs.que = pbjs.que || [];
var googletag = window.googletag || {};
googletag.cmd = googletag.cmd || [];
window.Muscula = { settings:{
logId:'b5d3f593-4b51-4716-95db-0ee993bd5a1d', suppressErrors: false, googleAnalyticsEvents: 'none'
}};
var biddingHelpers = {
gptLoaded: false,
loadScript: function(src) {
var script = document.createElement('script');
script.async=true;
script.type = 'text/javascript';
script.src = src;
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(script, node);
},
loadPrebid: function() {
this.loadScript('//cdn.ustatik.com/vendor/prebid/prebid.full.js');},
loadAmazonApstag: function () {
!function (a9, a, p, s, t, A, g) {
if (a[a9]) return;
function q(c, r) {
a[a9]._Q.push([c, r])
}
a[a9] = {
init: function () {
q("i", arguments)
},
fetchBids: function () {
q("f", arguments)
},
setDisplayBids: function () {
},
targetingKeys: function () {
return []
}, _Q: []
};
A = p.createElement(s);
A.async = !0;
A.src = t;
g = p.getElementsByTagName(s)[0];
g.parentNode.insertBefore(A, g)
}("apstag", window, document, "script", "//c.amazon-adsystem.com/aax2/apstag.js");
var apstagParams = {
pubID: 'fa84ab71-a984-4d4b-9cab-fa3b3879ff93',
adServer: 'googletag',
}
if (window.UGAPP.bidding.isGDPRAvailable) {
apstagParams.gdpr = {
enabled: true,
cmpTimeout: 100000000
}
}
apstag.init(apstagParams)
},
apstagFetchBids: function (units) {
apstag.fetchBids({
slots: units.map(function (unit) {
return {
slotID: unit.code,
slotName: '/' + window.UGAPP.bidding.dfpId + '/' + unit.dfpCode,
sizes: unit.sizes
};
}),
timeout: window.UGAPP.bidding.timeout
}, function (bids) {
googletag.cmd.push(function () {
apstag.setDisplayBids()
})
})
},
loadAdServer: function() {
if (this.gptLoaded) return;
this.loadScript('//www.googletagservices.com/tag/js/gpt.js');
this.gptLoaded = true;
},
loadErrorService: function() {
var protocol = window.location.protocol === 'https:' ? 'https:' : 'http:';
this.loadScript(protocol + '//musculahq.appspot.com/Muscula8.js');
window.Muscula.run=function(){var a;eval(arguments[0]);window.Muscula.run=function(){};};
window.Muscula.errors=[];window.onerror=function(){window.Muscula.errors.push(arguments);
return window.Muscula.settings.suppressErrors===undefined;}
},
getAdomikRandomAdGroup: function () {
var rand = Math.random()
switch (true) {
case rand < 0.09:
return 'ad_ex' + (Math.floor(100 * (rand)))
case rand < 0.10:
return 'ad_bc'
default:
return 'ad_opt'
}
},
defineGptTag: function(params, needRefresh) {
var dfpId = window.UGAPP.bidding.dfpId
var dfpCode = needRefresh ? params.dfpCode + '_REFRESH' : params.dfpCode;
var path = '/' + dfpId + '/' + dfpCode;
var tag = googletag
.defineSlot(path, params.sizes, params.code)
.addService(googletag.pubads())
.setTargeting('ad_group', biddingHelpers.getAdomikRandomAdGroup())
.setTargeting('is_refresh', '0')
if (window.UGAPP.bidding.analytics.variationDfpId)
{
tag.setTargeting('variation', window.UGAPP.bidding.analytics.variationDfpId);
}
return tag
},
getUnitByCode: function(code) {
return window.UGAPP.bidding.units.filter(function(unit) {
return unit.code === code
})[0]
},
createContainerFrame: function(width, height, container_id) {
var containerFrame = window.document.createElement('iframe');
containerFrame.width = width;
containerFrame.height = height;
containerFrame.scrolling = "no";
containerFrame.marginWidth = "0";
containerFrame.marginHeight = "0";
containerFrame.frameBorder = "0";
containerFrame.style.border = "0px";
containerFrame.style.verticalAlign = "bottom";
window.document.getElementById(container_id).appendChild(containerFrame);
return containerFrame;
},
runComplexAd: function(complexUnit) {
CMNUNT.load(complexUnit.cmnUNT, {
containerId: complexUnit.code
});
},
getDeviceClass: function() {
var device_class;
if (navigator.userAgent.match(/crawler|bot|bingpreview/i)) device_class = 'bot';
else if (navigator.userAgent.match(/Android/i)) device_class = 'android';
else if (navigator.userAgent.match(/iPhone/i)) device_class = 'iphone';
else if (navigator.userAgent.match(/iPad/i)) device_class = 'ipad';
else device_class = 'desktop';
return device_class;
}
};
window.UGAPP.bidding.analytics.device = biddingHelpers.getDeviceClass();
if (window.UGAPP.bidding.complexUnits && !isMobile) {
window.addEventListener('DOMContentLoaded', function() {
window.UGAPP.bidding.complexUnits.map(biddingHelpers.runComplexAd);
})
}
if (!isMobile) {
window.biddingHelpers.loadScript('//cdn.ustatik.com/public/cmp/index.js?6');
}
</script><script>var user = [];</script><main class="content-scroll"><div class="small-header-cont"><header class="small-header ui-small-header"><div class="container"><div class="navbar-brand"><div class="b-service-change"><a class="logo" data-tracking="Header,Click,Home" href="/"><i></i></a><a class="blog" data-tracking="Header,Click,Blog" href="/blog"><i></i></a><div class="changer"></div></div></div><div class="small-header-btn-hide ui-hide-search">×</div><form action="/search.php" class="mainpage-serch-form mainpage-serch-form--header" id="searchForm_mainpage" name="search_form"><button aria-label="Search in 911tabs" class="btn btn-search pull-right" data-tracking="Header,Click,Search" id="gbqfb" type="submit"><i></i></button><div class="ov-h"><input autocomplete="off" id="s" name="search" placeholder="Search for artists or songs" type="search" value=""/><div class="search-help" id="ss_container" style="display: none;"></div></div></form><!-- <div style="display: none" id="signup" class="btn btn-green btn-signup btn-signup--at-search" data-tracking="Header,Click,Sign up">Sign up</div>--></div><div id="navigation"><div class="container"><div class="small-logo"></div><ul class="nav navbar-nav"><li class="first"><a data-tracking="Header,Click,Guitar channel" href="/guitar_tabs/">Guitar</a></li><li><a data-tracking="Header,Click,Guitar Pro channel" href="/guitar_pro_tabs/">Guitar Pro</a></li><li><a data-tracking="Header,Click,Bass channel" href="/bass_tabs/">Bass</a></li><li><a data-tracking="Header,Click,Piano channel" href="/piano_tabs/">Piano</a></li><li><a data-tracking="Header,Click,Sheet Music channel" href="/sheet_music_tabs/">Sheet Music</a></li><li><a data-tracking="Header,Click,Drum channel" href="/drum_tabs/">Drum</a></li><li><a data-tracking="Header,Click,Power channel" href="/power_tabs/">Power</a></li><li><a data-tracking="Header,Click,Video channel" href="/video_tabs/">Video</a></li><li><a data-tracking="Header,Click, Top 100 tabs" href="/top/">Top 100 tabs</a></li><li class="dropdown"><a class="dropdown-toggle active" data-toggle="dropdown" data-tracking="Header,Click,Alpha" href="#">A-Z</a><div class="dropdown-menu navigation-az" role="menu"><div class="ov-h"><a href="/bands/a/">A</a><a href="/bands/b/">B</a><a href="/bands/c/">C</a><a href="/bands/d/">D</a><a href="/bands/e/">E</a><a href="/bands/f/">F</a><a href="/bands/g/">G</a><a href="/bands/h/">H</a><a href="/bands/i/">I</a><a href="/bands/j/">J</a><a href="/bands/k/">K</a><a href="/bands/l/">L</a><a class="active" href="/bands/m/">M</a><a href="/bands/n/">N</a><a href="/bands/o/">O</a><a href="/bands/p/">P</a><a href="/bands/q/">Q</a><a href="/bands/r/">R</a><a href="/bands/s/">S</a><a href="/bands/t/">T</a><a href="/bands/u/">U</a><a href="/bands/v/">V</a><a href="/bands/w/">W</a><a href="/bands/x/">X</a><a href="/bands/y/">Y</a><a href="/bands/z/">Z</a><a href="/bands/0-9/">0-9</a></div></div></li></ul><ul class="nav navbar-nav pull-right" style="display: none"><li class=" first"><a class="navigation-show-search ui-show-search" data-tracking="Header,Click,Search nav" href="#"><i></i></a></li><li class="dropdown navigation-profile notlogin"><a class="dropdown-toggle" data-toggle="dropdown" data-tracking="Header,Click,Profile" href="#"><i></i></a><div class="dropdown-menu" role="menu"><div class="navigation-profile-menu"><span class="btn btn-green btn-signup" data-tracking="Header,Click,Sign in" id="signin">Sign in</span><div class="line-text"><span>or</span></div><a class="btn facebook-login" data-tracking="Header,Click,Facebook login" href="#"><i></i>Log in with Facebook</a><a class="btn google-login" data-tracking="Header,Click,Google+ login" href="#"><i></i>Log in with Google+</a></div></div></li><li class="dropdown navigation-profile dn login"><a class="dropdown-toggle" data-toggle="dropdown" data-tracking="Header,Click, Profile" href="#"><img alt="" class="ava" src=""/></a><div class="dropdown-menu" role="menu"><div class="navigation-profile-menu"><ul><li><div class="username"></div></li><li><a data-tracking="Header Profile,Click,Profile" href="/my/">Profile</a></li><li><a data-tracking="Header Profile,Click,Favorite artist" href="/my/updates.php"><span>Favourite artists </span> <span class="count fav"></span></a></li><li><a data-tracking="Header Profile,Click,Songbooks" href="/my/"><span>Songbook</span> <span class="count sb"></span></a></li><li><a data-tracking="Header Profile,Click,Submit tab" href="/my/submit.php">Submit tab</a></li><li><a data-tracking="Header Profile,Click,Logout" href="/logout" id="logout">Logout</a></li></ul></div></div></li></ul></div></div></header></div><section class="container"><div class="row"><!-- Top Add --><div class="top-add ads_banner"><bidding-unit data-width="728" id="ad_cs_13848769_728_90"><script></script></bidding-unit> </div><!-- Top Add --></div><div class="row"><script src="/band_view.php?key=d8e0"></script><article class="b-info col-md-12"><div class="left-add ads_banner"><bidding-unit data-width="300" id="ad_cs_13848769_300_250"><script></script></bidding-unit> </div><section class="ov-h" itemscope="" itemtype="http://schema.org/MusicGroup"><ol class="breadcrumb" id="breadcrumb"><li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title"><span class="t911">911</span>tabs</span></a></li><li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/bands/m/" itemprop="url"><span itemprop="title">Bands - M</span></a></li></ol><div class="clearfix"><h1 itemprop="name">Malajube chords &amp; tabs</h1><div class="controls"><div class="btn-area"><div class="btn-loader"></div><div class="add-to-fav btn" data-band="45885" data-tracking="Artist,Click,Add to favorites"><span class="star"></span>Add to favorites</div></div><script src="/public/js/add_favorite.js"></script></div></div><div class="numbers"><div class="tabs-count"><b>29</b>tabs </div><div class="views-count"><b>2,171</b>views
</div><div class="updated-count"><b content="09-12-2016" itemprop="dateModified">sep 12, 2016</b>last updated
</div></div></section></article><section class="band-head clearfix"><h2>Top tabs</h2><div class="all">All types</div><div class="type"><div data-type="guitar">Guitar</div><div data-type="guitar pro">G. Pro</div><div data-type="bass">Bass</div><div data-type="piano">Piano</div><div data-type="video">Video</div><div data-type="drum">Drum</div><div data-type="power">Power</div><div data-type="sheet music">Sheet M.</div></div></section><article class="b-table b-table_band col-md-12"><div class="line"><a class="song" href="/tabs/m/malajube/montreal_40_tab.htm" title="Montreal -40">Montreal -40<div>4 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/montreal_40_guitar_tab.htm" title="Montreal -40 Guitar">4</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/pate_filo_tab.htm" title="Pate Filo">Pate Filo<div>4 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/pate_filo_guitar_tab.htm" title="Pate Filo Guitar">4</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/etienne_daout_tab.htm" title="Etienne DAout">Etienne DAout<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/etienne_daout_guitar_tab.htm" title="Etienne DAout Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/la_monogamie_tab.htm" title="La Monogamie">La Monogamie<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/la_monogamie_guitar_tab.htm" title="La Monogamie Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/le_metronome_tab.htm" title="Le Metronome">Le Metronome<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/le_metronome_guitar_tab.htm" title="Le Metronome Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line-separator"><h2>All tabs</h2></div><div class="line"><a class="song" href="/tabs/m/malajube/casse_cou_tab.htm" title="Casse-cou">Casse-cou<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/casse_cou_guitar_tab.htm" title="Casse-cou Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/etienne_daout_tab.htm" title="Etienne DAout">Etienne DAout<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/etienne_daout_guitar_tab.htm" title="Etienne DAout Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/filles_a_plumes_tab.htm" title="Filles A Plumes">Filles A Plumes<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/filles_a_plumes_guitar_tab.htm" title="Filles A Plumes Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/grand_galion_tab.htm" title="Grand Galion">Grand Galion<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/grand_galion_guitar_tab.htm" title="Grand Galion Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/jus_de_canneberges_tab.htm" title="Jus De Canneberges">Jus De Canneberges<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/jus_de_canneberges_guitar_tab.htm" title="Jus De Canneberges Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/le_bataillon_tab.htm" title="Le Bataillon">Le Bataillon<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/le_bataillon_guitar_tab.htm" title="Le Bataillon Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/le_crabe_tab.htm" title="Le Crabe">Le Crabe<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/le_crabe_guitar_tab.htm" title="Le Crabe Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/le_jus_de_citron_tab.htm" title="Le Jus De Citron">Le Jus De Citron<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/le_jus_de_citron_guitar_tab.htm" title="Le Jus De Citron Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/le_metronome_tab.htm" title="Le Metronome">Le Metronome<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/le_metronome_guitar_tab.htm" title="Le Metronome Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/le_robot_sexy_tab.htm" title="Le Robot Sexy">Le Robot Sexy<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/le_robot_sexy_guitar_tab.htm" title="Le Robot Sexy Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/luna_tab.htm" title="Luna">Luna<div>3 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/luna_guitar_tab.htm" title="Luna Guitar">3</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/la_monogamie_tab.htm" title="La Monogamie">La Monogamie<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/la_monogamie_guitar_tab.htm" title="La Monogamie Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/montreal_40_tab.htm" title="Montreal -40">Montreal -40<div>4 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/montreal_40_guitar_tab.htm" title="Montreal -40 Guitar">4</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/pate_filo_tab.htm" title="Pate Filo">Pate Filo<div>4 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/pate_filo_guitar_tab.htm" title="Pate Filo Guitar">4</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/p_te_filo_tab.htm" title="P Te Filo">P Te Filo<div>1 tabs:</div></a><div class="type-count"><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><a class="ta" href="/tabs/m/malajube/drum_tabs/p_te_filo_drum_tab.htm" title="P Te Filo Drum">1</a><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/st_fortunat_tab.htm" title="St-Fortunat">St-Fortunat<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/st_fortunat_guitar_tab.htm" title="St-Fortunat Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/synesth%c3%a9sie_tab.htm" title="Synesthésie">Synesthésie<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/synesth%c3%a9sie_guitar_tab.htm" title="Synesthésie Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/ton_plat_favori_tab.htm" title="Ton Plat Favori">Ton Plat Favori<div>1 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/ton_plat_favori_guitar_tab.htm" title="Ton Plat Favori Guitar">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/ursuline_tab.htm" title="Ursuline">Ursuline<div>1 tabs:</div></a><div class="type-count"><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><a class="ta" href="/tabs/m/malajube/drum_tabs/ursuline_drum_tab.htm" title="Ursuline Drum">1</a><div class="zero ta">—</div><div class="zero ta">—</div></div></div><div class="line"><a class="song" href="/tabs/m/malajube/la_valerie_tab.htm" title="La Valerie">La Valerie<div>2 tabs:</div></a><div class="type-count"><a class="ta" href="/tabs/m/malajube/guitar_tabs/la_valerie_guitar_tab.htm" title="La Valerie Guitar">1</a><a class="ta" href="/tabs/m/malajube/guitar_pro_tabs/la_valerie_guitar_pro_tab.htm" title="La Valerie Guitar Pro">1</a><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div><div class="zero ta">—</div></div></div><ul class="pagination"></ul><table><tr><div class="row bottom-double-add ads_banner" style="width: 100%"><div style="display: inline-block; margin-right: 10px;"><bidding-unit data-width="300" id="ad_cs_13849140_300_250"><script></script></bidding-unit></div><div style="display: inline-block; margin-left: 10px;"><bidding-unit data-width="300" id="ad_cs_13849132_300_250"><script></script></bidding-unit></div></div></tr></table></article><div class="col-md-2 col-md-pull-10 left-add ads_banner"><bidding-unit data-width="160" id="ad_cs_13848769_160_90"><script></script></bidding-unit> </div></div><table><tr><div class="row bottom-double-add ads_banner"><div style="display: inline-block; margin-right: 10px;"><bidding-unit data-width="300" id="ad_cs_13849140_300_250"><script></script></bidding-unit></div><div style="display: inline-block; margin-left: 10px;"><bidding-unit data-width="300" id="ad_cs_13849132_300_250"><script></script></bidding-unit></div></div></tr></table></section><footer class="footer"><div class="container"><div class="row"><div class="col-md-3">
911Tabs.com © 2021 </div><ul class="nav navbar-nav col-md-6"><li><a data-tracking="Footer,Click,About" href="/about/">About</a></li><li><a data-tracking="Footer,Click,Contact" href="/about/contact.php">Contact</a></li><li><a data-tracking="Footer,Click,Privacy Policy" href="/about/privacy_policy.php">Privacy Policy</a></li><li><a data-tracking="Footer,Click,ToS" href="/about/tos.php">TOS</a></li><li class="to-mobile"><a href="#"><i style="display: inline-block;background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAA8BAMAAAD1QKUnAAAALVBMVEUAAAD//+v//+v//+v//+v//+v//+v//+v//+v//+v//+v//+v//+v//+v//+tknAagAAAADnRSTlMAh3jbppl3RiEC57J0uxZVgy4AAACESURBVCjPY2CYfiwNDnIqGRg47N4hgccNDJPfoQBLhjpUgecMeagCjxj0QFTdw46OPrDAQ4jAulehofvgAjBAvMDTUCQQBRQwYEACLKMCowJ0FyAhJV8vQRV4zcAghyLwnIGhD4cANIs9YYDKPIJlQvdCaCbEyKaYGZlhuqIgHAhVMgAAlsk5Jb1Pc1EAAAAASUVORK5CYII=);width: 16px;height: 30px;-webkit-background-size: 16px 30px;background-size: 16px 30px;margin-right: 8px;position: relative;top: 10px;"></i>Mobile version</a></li></ul><div class="col-md-3 follow">
Follow us
<a class="fb icn" href="https://www.facebook.com/pages/911tabscom/728079037218852" target="_blank"></a><a class="gpl icn" href="https://plus.google.com/110625930262592885110?prsrc=3" target="_blank"></a><a class="icn rss" data-tracking="Footer,Click,RSS" href="/about/all_updates.xml.php" target="_blank"></a></div></div></div></footer></main><!-- Yandex.Metrika counter --><script>
(function (d, w, c) {
(w[c] = w[c] || []).push(function() {
try {
w.yaCounter19940668 = new Ya.Metrika({id:19940668,
webvisor:true,
clickmap:true,
trackLinks:true,
accurateTrackBounce:true});
w.yaCounter = w.yaCounter19940668;
} catch(e) { }
});
var n = d.getElementsByTagName("script")[0],
s = d.createElement("script"),
f = function () { n.parentNode.insertBefore(s, n); };
s.type = "text/javascript";
s.async = true;
s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
if (w.opera == "[object Opera]") {
d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script><noscript><div><img alt="" src="//mc.yandex.ru/watch/19940668" style="position:absolute; left:-9999px;"/></div></noscript><!-- /Yandex.Metrika counter --><!-- Begin comScore Tag --><script>
document.write(unescape("%3Cscript src='" + (document.location.protocol == "https:" ?
"https://sb" : "http://b") + ".scorecardresearch.com/beacon.js' %3E%3C/script%3E"));
</script><script>
COMSCORE.beacon({
c1:2,
c2:6745264,
c3:"",
c4:document.location,
c5:"",
c6:"",
c15:""
});
</script><noscript><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6745264&amp;c3=&amp;c4=&amp;c5=&amp;c6=&amp;c15=&amp;cj=1"/></noscript><!-- End comScore Tag --> <script src="/public/js/common.js?2005054940"></script><script src="/mau.php"></script><div class="overlay js-nps-overlay" style="display: none;"></div><div class="popup-nps popup-nps--bad-ads close js-popup-nps" style="right: -550px;"><div class="popup-nps-btn js-nps-btn"><div class="rate-us" style="display:block;"><span>Report Bad Ads</span></div></div><div class="js-send"><div class="popup-nps-thanks-h popup-nps-text--bad-ads-h">Hi! You can now report bad ads if you suffer from sound/video ads.</div><div class="popup-nps-text popup-nps-text--bad-ads-text">Just be on page with bad ads and submit it to us via form below. We'll get snapshot of this page, ads identifiers and will analyze it.</div><textarea class="popup-nps-comment" id="nps_comment" placeholder="What's wrong with ads on the page?"></textarea><div class="btn-area"><button class="popup-nps-submit js-popup-nps-submit" disabled="">Report</button><div class="btn-loader"></div></div></div><div class="js-sended" style="display: none;"><div class="popup-nps-thanks-ico"></div><div class="popup-nps-thanks-h">Thanks!</div><div class="popup-nps-thanks-text">You are awesome!</div></div></div><script>
$(document).ready(function(){
$('.js-popup-nps').on( 'click', '.close .js-nps-btn', function(){
$('.js-popup-nps').animate({'right': 0}, 500, function(){$(this).addClass('open').removeClass('close')});
$('.js-nps-btn .rate-us').fadeOut(200);
$('.js-nps-overlay').fadeIn(500);
});
$('.js-popup-nps').on( 'click', '.open .js-nps-btn', function(){
$('.js-popup-nps').animate({'right': -550}, 500, function(){$(this).removeClass('open').addClass('close')});
$('.js-nps-btn .rate-us').fadeIn(200);
$('.js-nps-overlay').fadeOut(500);
});
$('.js-popup-nps-submit').on('click',function(){
$(this).fadeOut(0);
var banners = [];
$('.ads_banner').each(function(i, el){
var $el = $(el);
banners.push({'class':$el.attr('class'),'html':$el.html()});
});
$.ajax({
url: "/ajax/bad_ads.php",
type: 'POST',
data: {
'report':JSON.stringify(banners),
'comment':$('#nps_comment').val()
},
dataType: "json",
complete: function() {
$('.js-send').fadeOut(300);
$('.js-sended').fadeIn(300);
setTimeout(function(){$('.js-nps-btn').trigger('click')}, 1600);
}
});
});
$('#nps_comment').on('keyup',function(){
$('.js-popup-nps-submit').prop('disabled',true);
if($(this).val() != '') {
$('.js-popup-nps-submit').prop('disabled',false);
} else {
$('.js-popup-nps-submit').prop('disabled', true);
}
});
});
</script> </body></html>