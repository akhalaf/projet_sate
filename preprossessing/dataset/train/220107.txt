<html lang="en"><head>
<title>404 - Page Not Found</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="/errors/biz.css" rel="stylesheet" type="text/css"/>
<link href="/errors/displaytag.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="/errors/aff.js"></script>
<script language="javascript" src="/errors/cal.js"></script>
<!-- Main Body and the Container Tbl -->
</head><body onload="return myFocus()">
<table align="left" border="0" cellpadding="0" cellspacing="0" class="containertbl">
<tbody><tr><td>
<!-- The above main body tbl is closed in Footer.jsp include -->
<table border="0" cellpadding="0" cellspacing="0" class="logotbl">
<tbody><tr>
<td valign="bottom"><img src="/errors/Alacriti_Logo_Sml.gif"/></td>
</tr>
</tbody></table>
<table border="0" cellpadding="0" cellspacing="0" class="headertbl">
<tbody><tr height="20" valign="center">
<td align="right" valign="center" width="100%">
</td>
</tr>
</tbody></table>
<!-- the underline table -->
<table border="0" cellpadding="0" cellspacing="0" class="topmenutbl" width="100%">
<tbody><tr><td></td></tr>
</tbody></table>
<!-- Main Content Table starts here--->
<!-- Main Content Table will contain two <td>s , one for left nav OR right nav and second for content-->
<table border="0" cellpadding="0" cellspacing="0" class="simpletbl" width="100%">
<tbody><tr valign="top">
<!-- This table is closed in the Footer.jsp-->
<!-- the td block where the main content will go always as tables -->
<td width="100%">
<table border="0" cellpadding="0" cellspacing="0" class="maintbl">
<tbody><tr>
<td>
<!-- put your content below inside one or multiple tables -->
<table class="seperatortbl"><tbody><tr><td></td></tr></tbody></table>
<table align="center" border="0" cellpadding="0" cellspacing="0" valign="middle" width="70%">
<tbody><tr>
<td align="left" valign="center" width="70%">
<span class="form_text"><b>Page Not Available.</b></span><br/><br/>
<span class="form_text">Please try the following:</span><br/>
<span class="form_text">If you typed the page address in the Address bar, make sure that it is spelled correctly.</span><br/>
<span class="form_text">Click the Back button to try another link.</span><br/><br/>
<span class="form_text">Please contact Customer Service for assistance.</span><br/><br/>
</td>
</tr>
</tbody></table>
<!-- close the main content td  -->
</td>
</tr>
</tbody></table>
</td>
<!-- put the right nav after this td -->
<!-- closing the Main Content table -->
</tr>
</tbody></table>
<!-- closed the content tbl -->
<!-- Footer begins here -->
<br/>
<table border="0" cellpadding="0" cellspacing="0" class="footertbl" width="100%">
<tbody><tr align="right"><td class="copyright">Copyright© 2019 Alacriti Inc. All rights reserved.</td></tr>
</tbody></table>
<!-- Footer ends here -->
<!-- closing the Container tbl and the body -->
</td></tr>
</tbody></table>
<!-- body closed ; html tag will be closed in every jsp -->
</body></html>
