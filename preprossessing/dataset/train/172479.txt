<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Make Your Used Car Trade-In Worth More - Auto Broker Magic</title>
<meta content="Increase the value of your used car and get a better price by following these tips. Get a better answer to the question, how much is my car worth. " name="DESCRIPTION"/>
<link href="https://www.auto-broker-magic.com/trade-in.html" rel="canonical"/>
<meta content="trade-in, car value, trade" name="KEYWORDS"/>
<meta content="English" http-equiv="CONTENT-LANGUAGE"/>
<meta content="SearchEngine" http-equiv="VW96.OBJECT TYPE"/>
<meta content="General" name="RATING"/>
<meta content="index,follow" name="ROBOTS"/>
<meta content="true" name="MSSmartTagsPreventParsing"/>
<style type="text/css">
BODY {
  SCROLLBAR-HIGHLIGHT-COLOR: #ffffff; SCROLLBAR-SHADOW-COLOR: #e1dcb5; SCROLLBAR-3DLIGHT-COLOR: #e1dcb5; SCROLLBAR-ARROW-COLOR: #FF8111; SCROLLBAR-TRACK-COLOR: #ffffff; SCROLLBAR-DARKSHADOW-COLOR: #ff0000; SCROLLBAR-BASE-COLOR: #ffffff
}
</style>
<style type="text/css">
A {
  TEXT-DECORATION: none
}
</style>
<style>
<!--
a:hover{color:#cc9955;}
-->
</style>
<style type="text/css">
<!-- 
H1 {font-family: Verdana; margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; font-size: 24; font-weight: bold;  color: #000080}
H2 {font-family: Verdana,Times New Roman,Tahoma,Arial,Helvetica; font-size: 14; font-weight: bold;  margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; color: #000080}
H3 {font-family: Verdana,Times New Roman,Tahoma,Arial,Helvetica; font-size: 16; font-weight: bold;  margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; color: #000000}
H4 {font-family: Verdana,Times New Roman,Tahoma,Arial,Helvetica; margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; font-size:12; font-weight: normal;  color: #000000}
-->
</style>
</head>
<body bgcolor="#F2F5F7" leftmargin="0" link="blue" rightmargin="0" topmargin="0" vlink="blue"><br/>
<table border="0" cellpadding="0" width="100%">
<td bgcolor="#F2F5F7" colspan="3" valign="top">
                                                                   <a href="https://www.auto-broker-magic.com/"><img alt="Auto Broker Magic" border="0" height="40" src="images/logo4.jpg" width="270"/></a>
</td>
</table>
<style type="text/css"> <!-- #navbar ul { margin: 0; padding: 5px; list-style-type: none; text-align: center; background-color: #F2F5F7; } #navbar ul li { display: inline; } #navbar ul li a { text-decoration: none; padding: .2em 1em; color: #fff; background-color: #819FF7; } #navbar ul li a:hover { color: #000; background-color: #D3DCFF; } --> </style> <div id="navbar"> <ul>                                                                  <li><a href="https://www.auto-broker-magic.com/">Home</a></li> <li><a href="https://www.auto-broker-magic.com/buy-new-car.html">New Car Buying Guide</a></li> <li><a href="https://www.auto-broker-magic.com/usedcarbuying.html">Used Car Buying Guide</a></li> <li><a href="https://www.auto-broker-magic.com/car-loans-used.html">Car Loans</a></li> <li><a href="https://www.auto-broker-magic.com/car-insurance.html">Car Insurance</a></li> </ul> </div>
<table align="center" border="1" bordercolor="#D3DCFF" cellpadding="6" cellspacing="0" width="1000">
<tr>
<td bgcolor="#ffffff" valign="top" width="250">
<br/>
<div align="center"><img alt="Josh Rosenberg" border="0" height="160" src="images/josh-2011.jpg" width="225"/></div>
<div class="menubar">
<center><font color="#000000" face="VERDANA" size="2"><b>Josh Rosenberg</b>
</font></center>
<br/>
<hr/>
<br/><br/>
<b>
<font color="#000000" face="VERDANA" size="2">

  <img src="images/grip201.gif"/> <a href="https://www.auto-broker-magic.com/car-guide-tips.html"><u>Car Buying Articles</u></a><br/><br/>

  <img src="images/grip201.gif"/> <a href="https://www.auto-broker-magic.com/Contact_Us.html"><u>Contact Us</u></a></font></b>
<br/><br/>
<br/>
<center><font color="#000080" face="verdana" size="3"><b><u>Money Saving Tips</u>:</b></font></center>
<br/><b>
<font color="#000000" face="VERDANA" size="2">

  <img src="images/grip201.gif"/> <a href="https://www.auto-broker-magic.com/newcarpurchase.html"><u>Top New Car Buying Tactic</u></a><br/><br/>

  <img src="images/grip201.gif"/> <a href="https://www.auto-broker-magic.com/carbuying.html"><u>Used Car Buying Review</u></a><br/><br/>

  <img src="images/grip201.gif"/> <a href="https://www.auto-broker-magic.com/used-car.html"><u>Effectivie Used Car Tactic</u></a><br/><br/>

  <img src="images/grip201.gif"/> <a href="https://www.auto-broker-magic.com/price-quotes.html"><u>New Car Price Quotes</u></a><br/><br/>

  <img src="IMAGES/GRIP201.GIF"/> <a href="https://www.auto-broker-magic.com/truecar-review.html"><u>Why Use TrueCar?</u></a><br/><br/>

  <img src="images/grip201.gif"/> <a href="https://www.auto-broker-magic.com/used-car-ratings.html"><u>Used Car Ratings</u></a><br/><br/>

  <img src="images/grip201.gif"/> <a href="https://www.auto-broker-magic.com/cheap-used-cars.html"><u>Best Cheap Car Sources</u></a><br/><br/>

  <img src="images/grip201.gif"/> <a href="https://www.auto-broker-magic.com/car-selling.html"><u>Buy And Sell Cars</u></a><br/><br/>

  <img src="images/grip201.gif"/> <a href="https://www.auto-broker-magic.com/Used_Car_Aucti.html"><u>Auto Auction Sources</u></a><br/><br/>

  <img src="images/grip201.gif"/> <a href="https://www.auto-broker-magic.com/residual-value.html"><u>Residual Value Calculator</u></a><br/><br/>

  <img src="images/grip201.gif"/> <a href="https://www.auto-broker-magic.com/improve-mileage.html"><u>Increase Gas Mileage</u></a><br/><br/> 

  <img src="images/grip201.gif"/> <a href="https://www.auto-broker-magic.com/gas-mileage.html"><u>Best Gas Mileage Cars</u></a><br/><br/>

  <img src="images/grip201.gif"/> <a href="https://www.auto-broker-magic.com/Canada_Import.html"><u>Import Car To Canada</u></a></font></b><br/><br/>
<br/>
<hr/>
</div>
</td>
<td valign="top">
<table cellspacing="6"><tr>
<td bgcolor="#ffffff" valign="top">
<br/>
<blockquote><center>
<br/>
<h1>How To Make Your Car's Trade-In <br/>Value Worth More</h1></center>
<br/>
<p align="JUSTIFY">
<font color="#000000" face="VERDANA" size="3">First, you need to understand that the Dealer's "impression" of your car is going to have a significant influence on the price you get for it.  You see, most Dealers do not have the time to put your vehicle through an extensive mechanical check while you are waiting.  Instead, they want to get back and close the deal with you.

<br/><br/>So, they'll look it over as carefully as they can in a fairly short timeframe, and of course, they'll drive it briefly as well.  Their primary objective is to make sure that the vehicle does not have any "major" problems.

<br/><br/>(Incidentally, if you're trading in your car for another used one, here's a <a href="https://www.auto-broker-magic.com/usedcarbuying.html"><b><u>Lowest Price Used Car Buying Guide</u></b></a> that you should really read before making your purchase.)</font></p>
<p align="JUSTIFY"><font color="#424242" face="verdana" size="4"><b><u>The Numbers Game</u></b>:
</font></p>
<p align="JUSTIFY">
<font color="#000000" face="VERDANA" size="3">They'll then come up with a number based on similar vehicles sold recently at Dealer Auctions and cut that a bit to cover themselves in case they find a somehat significant problem later (or several minor ones).  And unfortunately, many will offer you a lower price simply to see how informed you are and to test what they can get away with.

<br/><br/>The purpose of this article is not, however, to deal with various Dealer tactics and what you should do in preparation for them (see the articles referenced at the bottom of this page for this).  What I'm focusing on here are the ways to "impress" the Dealer with your product so that you are maximizing their trust in your vehicle and increasing its value to them.</font></p>
<p align="JUSTIFY">
<font color="#424242" face="verdana" size="4"><b><u>Think Like A "Seller"</u></b>:</font>
<br/><br/>
<font color="#000000" face="VERDANA" size="3">To get the most for your trade-in, right off the bat you need to change your mindset.  You need to think in terms of being a "seller".  The Dealership is selling a car to you.  And in return, you are selling a car to them.  

<br/><br/>Okay?  You're not just a car buyer.  You are "selling" your car as well.

<br/><br/>I've always been surprised by how dirty people's trade-ins are.  And not just dirty outside, but extremely messy and cluttered inside as well and with trunks loaded with items from years gone by, including empty oil containers and various car parts.

<br/><br/>I think many people feel it's best to bring in something that's dirty and messy so they can say they weren't planning to make a car purchase.  I believe they think they are conveying the message that since they weren't planning on buying today, that they better be getting a great deal.  On numerous occasions, I've even seen this strategy passed around as advice on the internet.

<br/><br/>Well, it's not a very good approach and it's not very good advice.  First of all, Dealers are quite clever and are fully aware of this tactic.  Remember, they sell cars every day and have seen it all over and over again.  

<br/><br/>Secondly, and more important, a dirty, smelly, neglected looking car makes the Dealer think it's very likely the owners didn't take very good care of it or have it maintained regularly.   This isn't good for the price they'll likely offer.

<br/><br/>Instead, you want the vehicle you're trading-in to get the same positive response from the Dealer that the Dealer's car had on you.  Now perhaps this isn't always possible because you may be trading in a real clunker.  Well, all the more reason to put it in its best light.
</font></p>
<p align="JUSTIFY">
<font color="#424242" face="verdana" size="4"><b><u>Surprise, Surprise ... Wash The Darn Car!</u></b>:</font>
<br/><br/>
<font color="#000000" face="VERDANA" size="3">You may even want to have it detailed.  You want the vehicle to be washed and waxed.  You should shampoo the carpet and floor mats.  And you defintely want to use odor neutralizers or ionizers.  This is especially true for a smoker's car.

<br/><br/>We all know how important "first impression" is.  A shiny and clean car immediately strikes the Dealer as a nice surprise and he'll begin to entertain the idea that you actually cared about the car ... a strong indication that you likely maintained the car properly as well.</font></p>
<p align="JUSTIFY">
<font color="#424242" face="verdana" size="4"><b><u>Empty Your Trunk And Glove Compartment</u></b>:</font>
<br/><br/>
<font color="#000000" face="VERDANA" size="3">Again, neatness sends just the right message.  Get everything out of the trunk and vacuum it.  If there are any stains, shampoo it.

<br/><br/>The glove compartment should be neat and tidy (also the center console compartment if you have one ... okay, you can keep some neatly placed CDs there).  All you want in the glove box is a neat pile (or a sealed plastic baggie is even better) including the Owner's Manual, your registration and any other vehicle related documents.

<br/><br/>If you've had work done on the car, include those maintenance records as well.  If you have any small care products, such as touch-up paint or leather cleaner or conditioner (another good sign for a Dealer), they should be kept here as well.
</font></p>
<p align="JUSTIFY">
<font color="#424242" face="verdana" size="4"><b><u>Gain More Than You Spend</u></b>:</font>
<br/><br/>
<font color="#000000" face="VERDANA" size="3">After the Dealer has assured himself that your car doesn't have any major problems and seems to be in decent shape, he'll look for signs to indicate if you're the type of person that has been having it properly maintained.  He'll look for small things such as uneven wear on the tires (neglecting a possible balancing or alignment job) or oil that is dirty and overdue for a change.

<br/><br/>So, put on new wiper blades, top off fluids, change the oil and air filter if needed, check the tires and even have a tune-up done.  The Dealer is going to notice these things.  This will increase your credibility as being someone who takes care of their car and, by now, the respect factor from the Dealer is definitely on the rise.

<br/><br/>You're doing great so far.  Now it's time to top it off.
</font></p>
<p align="JUSTIFY">
<font color="#424242" face="verdana" size="4"><b><u>Point Out Something Wrong</u></b>:</font>
<br/><br/>
<font color="#000000" face="VERDANA" size="3">Yup.  Tell the Dealer about a minor problem.  It might be a few scratches on the side of the car.  It might be a burnt out interior light bulb.  It might be a loose screw holding the fender ... or slightly corroded battery cables... or whatever.  Find something minor and point it out.

<br/><br/>Again, this is building up your credibility and the Dealer is thinking, "Geez, if this is all he's worried about, I bet this car is in good condition".

<br/><br/>However, if you do have some sort of major problem, disclose it upfront.  The Dealer's going to find it anyway.  And because there's been so many other positives about the trade-in, and you've already won lots of credibility and respect points, it's less likely the Dealer will kill you for it price-wise, and will simply deduct his cost of the repair (less than what you'd pay).</font></p>
<p align="JUSTIFY">
<font color="#424242" face="verdana" size="4"><b><u>Planning Ahead</u></b>:</font>
<br/><br/>
<font color="#000000" face="VERDANA" size="3">All of the above are steps you can take right now to increase your vehicle's value.  However, there's an easier overall approach ... take good care of your vehicle from the moment you purchase it and keep all of the records.

<br/><br/>Realize that one day you are going to trade it in or sell it and simply follow its recommended maintenance schedule.  I don't think anything impresses a Dealer more when an owner has done all the above, and then puts the icing on the cake by saying, "Oh, I've got all the manitenance records since I owned it in the glove compartment".

<br/>
<br/><br/>By doing these things, you'll be far ahead of the typical trade-in customer.  By adopting the mindset of a "seller" and preparing your vehicle to look and drive its best, you'll be adding to its value.  The closer a vehicle is to being "lot ready", the more value it has to the Dealer.

<br/><br/>Good luck and all the best  - Josh
</font></p>
<br/><br/>
<center>
<p align="JUSTIFY">
<font color="#000000" face="VERDANA" size="3">And like <a href="https://www.auto-broker-magic.com/facebook.html"><b><u>our Facebook page</u></b></a> for more tips, car news and fun.
</font></p></center>
</blockquote>
<br/><br/>
<blockquote><blockquote>
<p align="JUSTIFY">
<font color="#000000" face="ARIAL" size="3"><b><u>Related Topics:</u></b></font>
<br/><br/><font color="#000000" face="VERDANA" size="3"> <a href="https://www.auto-broker-magic.com/car-value.html"><b><u>How Trade-In Value Is Determined</u></b></a>
<br/><br/> <a href="https://www.auto-broker-magic.com/car-information.html"><b><u>Finding Accurate Trade-In Values</u></b></a>
</font></p></blockquote></blockquote>
<br/><br/><br/>
<center>
<br/>
<font color="navy" face="ARIAL" size="2"><b><i>Auto Broker Magic</i></b></font><br/><font color="#000000" face="ARIAL" size="1">Increasing Your Trade- In Price<br/>
     West Palm Beach, Florida  <br/><a href="https://www.auto-broker-magic.com/Site_Map.html"><u>Site Map</u></a> |  <a href="https://www.auto-broker-magic.com/privacy.html"><u>Disclosure</u></a>
</font><br/>
<font color="#000000" face="VERDANA" size="1">© copyright 2010 - 2020, Josh Rosenberg.  All Rights Reserved.</font></center><br/>
</td></tr></table></td>
<!-- BEGIN WebSTAT Activation Code -->
<script language="JavaScript" src="https://secure.webstat.com/cgi-bin/wsv2.cgi?74359" type="text/javascript"></script>
<noscript>
<a href="http://www.webstat.com">
<img alt="Website Analytics and Website Statistics by WebSTAT" border="0" src="https://secure.webstat.com/scripts/wsb.php?ac=74359"/></a>
</noscript>
<!-- END WebSTAT Activation Code -->
</tr></table></body></html>