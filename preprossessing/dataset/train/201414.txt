<!DOCTYPE html>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge, chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="deny" http-equiv="X-Frame-Options"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="Blue Cross Blue Shield Global Core offers provider search, medical translations, travel alerts and more services for eligible blue cross blue shield members." name="description"/>
<meta content="medical translations, city health profiles, provider search, travel alerts, blue cross blue shield" name="keywords"/>
<title>Blue Cross Blue Shield Global Core -- 
Home</title>
<link href="/Assets/css/_bootstrap.css" rel="stylesheet"/>
<link href="/Assets/css/bcww-custom.css?v=03242020" rel="stylesheet"/>
<link href="/Assets/css/font-awesome.css" rel="stylesheet"/>
<link href="/Assets/css/jquery-ui-1.12.1.css" rel="stylesheet" type="text/css"/>
<link href="/apple-touch-icon-57x57.png?v=JyypzX5yp5" rel="apple-touch-icon" sizes="57x57"/>
<link href="/apple-touch-icon-60x60.png?v=JyypzX5yp5" rel="apple-touch-icon" sizes="60x60"/>
<link href="/apple-touch-icon-72x72.png?v=JyypzX5yp5" rel="apple-touch-icon" sizes="72x72"/>
<link href="/apple-touch-icon-76x76.png?v=JyypzX5yp5" rel="apple-touch-icon" sizes="76x76"/>
<link href="/apple-touch-icon-114x114.png?v=JyypzX5yp5" rel="apple-touch-icon" sizes="114x114"/>
<link href="/apple-touch-icon-120x120.png?v=JyypzX5yp5" rel="apple-touch-icon" sizes="120x120"/>
<link href="/apple-touch-icon-144x144.png?v=JyypzX5yp5" rel="apple-touch-icon" sizes="144x144"/>
<link href="/apple-touch-icon-152x152.png?v=JyypzX5yp5" rel="apple-touch-icon" sizes="152x152"/>
<link href="/apple-touch-icon-180x180.png?v=JyypzX5yp5" rel="apple-touch-icon" sizes="180x180"/>
<link href="/favicon-32x32.png?v=JyypzX5yp5" rel="icon" sizes="32x32" type="image/png"/>
<link href="/favicon-194x194.png?v=JyypzX5yp5" rel="icon" sizes="194x194" type="image/png"/>
<link href="/favicon-96x96.png?v=JyypzX5yp5" rel="icon" sizes="96x96" type="image/png"/>
<link href="/android-chrome-192x192.png?v=JyypzX5yp5" rel="icon" sizes="192x192" type="image/png"/>
<link href="/favicon-16x16.png?v=JyypzX5yp5" rel="icon" sizes="16x16" type="image/png"/>
<link href="/manifest.json?v=JyypzX5yp5" rel="manifest"/>
<link color="#5bbad5" href="/safari-pinned-tab.svg?v=JyypzX5yp5" rel="mask-icon"/>
<link href="/favicon.ico?v=JyypzX5yp5" rel="shortcut icon"/>
<meta content="Blue Cross Blue Shield Global Core" name="apple-mobile-web-app-title"/>
<meta content="Blue Cross Blue Shield Global Core -- 
Home " name="application-name"/>
<meta content="#da532c" name="msapplication-TileColor"/>
<meta content="/mstile-144x144.png?v=JyypzX5yp5" name="msapplication-TileImage"/>
<meta content="#ffffff" name="theme-color"/>
<script src="/bundles/modernizr?v=wBEWDufH_8Md-Pbioxomt90vm6tJN2Pyy9u9zHtWsPo1"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="/Assets/JavaScript/html5shiv.js"></script>
    <script src="/Assets/JavaScript/respond.js"></script>
    <![endif]-->
</head>
<body>
<section>
<div class="container bg-secondary noPad">
<div class="row bg-secondary noPad">
<div class="col-sm-8">
<div class="col-md-9 col-sm-9 col-xs-12">
<a href="/"><img alt="BlueCard Logo" src="/assets/images/BCBS_GlobalCoreLogo.png"/></a>
<h5 class="text-success"><em>formerly BlueCard Worldwide®</em></h5>
</div>
<div class="col-sm-12 col-xs-12 text-right">
<!---
	<div class="col-sm-12 col-xs-4 hidden-print">
                <ul class="nav navbar-nav navbar-right">
                    
                    
                </ul>
        </div>
--->
</div>
</div>
<div class="col-sm-4 col-xs-12 text-right">
<h5 class="noPad-Left">Powered by <span class="text-secondary"><strong>GeoBlue</strong></span> </h5>
</div>
<div class="col-sm-12 col-xs-12 hidden-print pull-right">
<ul class="nav navbar-nav navbar-right">
</ul>
</div>
</div>
</div>
</section>
<br/>
<div class="navbar navbar-default navbar-static-top"></div>
<section class="section">
<div class="container noPad ">
<div class="row bg-home">
<div class="container-tr">
<div class="col-md-8 noPad-Right">
<br/><br/><br/><br/>
<div class="well bg-tertiarytint pad10 noBorder col-xs-11 col-sm-12">
<h4 class="text-success">
                            As an eligible Blue Cross and Blue Shield member, you can use this website to find doctors and
                            hospitals outside of the United States, Puerto Rico and U.S. Virgin Islands. You can
                            also find other valuable resources to help you stay safe and healthy around the world.
                        </h4>
</div>
<div class="clearfix"></div>
</div>
</div>
<div class="col-md-4 alphaLogInBox">
<div class="bg-tertiary pad20 noBorder">
<h5 class="text-inverse">Login to take advantage of the many tools available on the website. </h5>
<div class="panel panel-default pre-scrollable mTop20" style="padding:10px;">
<h5>Terms of Use and End User License Agreement</h5>
<p class="small">This Terms of Use and End User License Agreement (this “Agreement”) describes the terms on which you (“You,” “Your,” or “End User” may access and use (i) the website located at www.bcbsglobalcore.com that links to this Agreement (the “Website”) and (ii) the software application for mobile devices available for download on the Apple App Store and Google Play, including any user manuals or help files contained therein and any modifications, updates, revisions or enhancements thereto (the "Licensed Application” and together with the Website, the “Products”) which are owned and operated by Worldwide Insurance Services, LLC (Worldwide Services Insurance Agency, LLC in California and New York) (“WIS”), an independent licensee of the Blue Cross Blue Shield Association (“BCBSA”).  BCBSA has contracted with WIS to access and use their provider network for the Blue Cross Blue Shield Global Core program. WIS and BCBSA are unaffiliated, independent companies. Blue Cross Blue Shield Global Core is a BCBSA program providing medical assistance and claims support services to eligible Blue Cross Blue Shield members.</p>
<h5>ACCEPTANCE OF TERMS OF USE</h5>
<p class="small"><strong>By using the Website and/or downloading the Licensed Application, You signify that you have read, understand and agree to be bound by this Agreement.</strong> Your continued use of the Products following the posting of any changes to this Agreement constitutes acceptance of those changes.  In the event of any inconsistency between the WIS Privacy Policy,  <a class="text-link" href="/Home/PrivacyPolicy/">(the "WIS Privacy Policy")</a>, and this Agreement, this Agreement shall control.</p>
<p class="small">WIS may revise this Agreement from time to time by updating this posting, with the new terms taking effect on the date of posting.  You should review this Agreement every time you use the Products because it is binding on you. If we make changes to our Terms of Use and you continue to use this Website or the Products you are impliedly agreeing to our Terms of Use.<strong> IF YOU DO NOT AGREE TO THE TERMS AND CONDITIONS SET FORTH HEREIN, PLEASE REFRAIN FROM USING THE WEBSITE AND THE PRODUCTS.</strong></p><br/>
<h5>1.	  PRODUCTS ACCESS</h5>
<p class="small">1.1	 You and any family members registered under your account shall be bound by the terms and conditions in this Agreement. Access to the Products and the International Provider Search feature excluding claims and claims history is protected through the first three letters or numbers of the Member ID number provided by your Blue Cross and Blue Shield company ("BCBS Company"). Access to claims and claims history is protected by the username and password you may create after accessing the Products. You acknowledge, agree, and warrant that you will provide access to your Member ID number or username and password only to your spouse and/or dependents and only to the extent that they also have coverage under your BCBS Plan, and you agree that any user of your Member ID number or username and password is bound by the terms set forth in this Agreement.</p><br/>
<h5>2.	  TERMINATION</h5>
<p class="small">2.1	 WIS reserves the right, in its sole discretion, to terminate your access to all or part of the Products, with or without notice.  An example of activity that may lead to a termination of your use of the Products include your breach of any of the terms and conditions of this Agreement.</p><br/>
<h5>3.	  THE PRODUCTS DO NOT PROVIDE MEDICAL ADVICE, BENEFITS, COVERAGE, CLAIMS OR INSURANCE ADVICE </h5>
<p class="small">3.1	 Material accessed or obtained through the Products is provided for informational purposes only.  Inclusion of information on the Products does not imply any medical advice, benefits, coverage, claims, insurance advice, recommendation or warranty.  All information on these Products comes from WIS and/or third-parties and their employees and is solely intended to provide information regarding WIS, the third-parties and the services they provide.  Information provided on the Products is not a substitute for the advice of an appropriate medical professional or healthcare provider.  While WIS facilitates relationships with healthcare providers and pharmacies (“Providers”), WIS, BCBSA, and BCBS Companies (“Companies”) assume no liability for any medical advice, consultation or services furnished by such Providers.</p>
<p class="small">3.2	 The Companies do not recommend or endorse any specific services or other information that may be mentioned on the Products.  The Companies do not endorse any Providers, nor do they assume liability for facilitating provider-patient relationships with such Providers.  Reliance on any information provided by the Companies, their employees or otherwise appearing on the Products is solely at your own risk.</p>
<p class="small">3.3	 While all attempts will be made to keep this site current, information may become outdated over time, or superseded by subsequent disclosure.  The Products could also include technical or other inaccuracies or typographical errors.  The Companies assume no liability for the accuracy or completeness of, nor any liability to update, the information contained on the Products.  Changes may be periodically added to the information and these changes will be incorporated in new editions of the Products.</p><br/>
<h5>4.	  PROPRIETARY RIGHTS IN PRODUCT CONTENT</h5>
<p class="small">4.1	 WIS retains all copyright and other proprietary rights in the contents of the Products (the “Content”) other than the third-party copyrighted content.  Elements of the Products are protected by copyright, trade dress and other laws, and may not be copied or imitated in whole or in part.  Nothing shall be construed as granting you any license under any patent, trademark or copyright of WIS or any third party, other than the license identified in Section 6 of this Agreement.  Certain portions of the Products contain information supplied and updated by third parties, or include links to third-party sites.  WIS is not responsible for, and makes no warranty as to the accuracy of, such information or sites.</p>
<p class="small">4.2	 WIS claims no ownership in, nor any affiliation with, third-party trademarks or brand names that may appear on this site.  Such third-party trademarks are used only to identify the products and services of their respective owners, and no sponsorship or endorsement on the part of WIS should be inferred from their use.  You are not permitted to use the trademarks displayed on the Products without the prior written consent of WIS or the third party that may own the trademarks.</p>
<p class="small">4.3	 No Content may be modified, copied, distributed, framed, reproduced, republished, downloaded, displayed, posted, transmitted or sold in any form or by any means, in whole or in part, without WIS’s prior written permission.  You may download or print a copy of any portion of the Content solely for your personal, non-commercial use, provided that you keep all copyright or other proprietary notices intact.  You may not republish Content on the Products or incorporate the information in any other database or compilation.  Any other use of the Content is strictly prohibited.</p>
<p class="small">4.4	 Additionally, you represent and warrant that (i) You are not located in a country that is subject to any U.S. Government embargo, or that has been designated by the U.S. Government as a “terrorist supporting” country; and (ii) that you are not listed on any U.S. Government list of prohibited or restricted parties, including but not limited to the Office of Foreign Assets Control (OFAC) list.</p><br/>
<h5>5.	  LINKS TO OTHER WEBSITES</h5>
<p class="small">5.1	 The Products may contain links to other websites for your convenience in locating information and services that may be of interest.  The Companies expressly disclaim any responsibility for the content, accuracy or opinions expressed in such websites, and such websites are not investigated, monitored or checked for accuracy or completeness by the Companies.  Inclusion of any linked website on or through the Products does not imply approval or endorsement of the linked website by the Companies.  If you decide to leave the Products and access these third-party websites, you do so at your own risk.</p><br/>
<h5>6.	  LICENSE GRANT AND LICENSE RESTRICTIONS</h5>
<p class="small">6.1	 WIS provides the Licensed Application and licenses its use solely pursuant to the terms stated below:</p>
<ul class="list-unstyled">
<li><p style="font-size: 91%">      (a)	  You are granted a non-transferable, non-sub licensable, limited license to use the Licensed Application under the terms stated in this Agreement solely for your individual use, which may be personal, business or professional in nature.  Title and ownership of the Licensed Application and of all copyright and other proprietary rights in the Content of the Licensed Application remain with WIS.  WIS reserves all rights not expressly granted to you;</p></li>
<li><p style="font-size: 91%">      (b)	  The Licensed Application may be used by you on any authorized devices, which you own or control and for which the Licensed Application is designed to operate;</p></li>
<li><p style="font-size: 91%">      (c)	  You may not make copies, translations or modifications of or to the Licensed Application (or any Content therein).  You may not alter, obscure or remove the copyright notice on any copy of the Licensed Application.  You may not reverse-engineer, disassemble or decompile the Licensed Application (or any content therein) or otherwise attempt to discover the source code or structural framework of the Licensed Application (or any content therein);</p></li>
<li><p style="font-size: 91%">      (d)	  You may not assign, sell, distribute, lease, rent, sublicense or transfer the Licensed Application (or any Content therein) or this license or disclose the Licensed Application (or any content therein) to any other person.  You may not make the Licensed Application (or any content therein) available over a network where it could be used by multiple devices at the same time; and</p></li>
<li><p style="font-size: 91%">      (e)	  WIS may terminate the license granted hereunder at any time.  The license granted hereunder automatically terminates if you fail to comply with any provision of this Agreement.  You agree upon termination to destroy the Licensed Application, together with all copies, modifications and merged portions in any form, including any copy on any devices that you own or control.</p></li>
<li><p style="font-size: 91%">      (f)	  You shall not disclose or use any Confidential Information except as expressly permitted under this Agreement.  You shall hold all Confidential Information in confidence during the term of this Agreement and for a period of three (3) years after the termination of this Agreement.  You shall take all reasonable steps to ensure that Confidential Information is not disclosed or distributed to third parties who are not subject in writing to the confidentiality obligations of this Section. "Confidential Information" shall mean this Agreement, all Licensed Application, data, drawings, benchmark tests, specifications, trade secrets, object code and source code of the Licensed Application, and any other proprietary information supplied to you by WIS, including all items defined as "confidential information" by WIS.</p></li>
<li><p style="font-size: 91%">      (g)	  You may not assign or sublicense any of your rights or delegate any of your obligations under this Agreement without the prior written consent of WIS. Any such assignment or sublicense shall be null and void.  Subject to the foregoing, this Agreement will bind and inure to the benefit of the parties, their respective successors and permitted assigns.</p></li>
</ul>
<h5>7.	  LIMITATION OF LIABILITY</h5>
<p class="small">7.1	 The Companies, their affiliates and any of their directors, officers, employees or agents shall not, under any circumstances, be liable for direct, consequential, incidental, indirect or special damages of any kind, or any other damages whatsoever, including, without limitation, those resulting from loss of use, data or profits, and whether resulting from the use or inability of use of any contents of the Products (or a website linked to the Products), or any other cause and even if caused by negligence or if the Companies have been apprised of the likelihood of such damages occurring.</p>
<p class="small">7.2	 The above limitation, or exclusion, may not apply to you to the extent that applicable law prohibits the limitation or exclusion of liability for incidental or consequential damages.</p><br/>
<h5>8.	  DISCLAIMER OF WARRANTIES</h5>
<p class="small">8.1	 YOU UNDERSTAND AND AGREE THAT THE PRODUCTS ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS AND THAT THE COMPANIES DO NOT ASSUME ANY RESPONSIBILITY FOR PROMPT OR PROPER DELIVERY, OR RETENTION OF ANY PERSONAL INFORMATION.  THE COMPANIES EXPRESSLY DISCLAIM ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.</p>
<p class="small">8.2	 THE COMPANIES MAKE NO WARRANTY THAT (1) THE PRODUCTS WILL MEET YOUR REQUIREMENTS, (2) THE PRODUCTS WILL BE UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE, (3) THE QUALITY OF ANY INFORMATION OR OTHER MATERIAL OBTAINED BY YOU THROUGH THE PRODUCTS WILL MEET YOUR EXPECTATIONS, AND (4) ANY ERRORS IN THE SOFTWARE WILL BE CORRECTED.</p>
<p class="small">8.3	 ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE PRODUCTS IS DONE AT YOUR OWN DISCRETION AND RISK AND YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH MATERIAL.</p>
<p class="small">8.4	 NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM WIS OR THROUGH OR FROM THE PRODUCTS SHALL CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THIS AGREEMENT.</p>
<p class="small">8.5	 SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES OR THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES.  ACCORDINGLY, SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU.</p><br/>
<h5>9.	  WAIVER OF JURY TRIAL AND CLASS ACTIONS</h5>
<p class="small">9.1	 BY ENTERING INTO THESE TERMS OF USE, YOU AND WIS ACKNOWLEDGE AND AGREE TO WAIVE CERTAIN RIGHTS TO LITIGATE DISPUTES IN COURT, TO RECEIVE A JURY TRIAL OR TO PARTICIPATE AS A PLAINTIFF OR AS A CLASS MEMBER IN ANY CLAIM ON A CLASS OR CONSOLIDATED BASIS OR IN A REPRESENTATIVE CAPACITY.  YOU AND WIS BOTH AGREE THAT ANY ARBITRATION WILL BE CONDUCTED ON AN INDIVIDUAL BASIS AND NOT A CONSOLIDATED, CLASS-WIDE OR REPRESENTATIVE BASIS AND THE ARBITRATOR SHALL HAVE NO AUTHORITY TO PROCEED WITH AN ARBITRATION ON A CLASS OR REPRESENTATIVE BASIS.  THE ARBITRATOR MAY AWARD INJUNCTIVE RELIEF ONLY IN FAVOR OF THE INDIVIDUAL PARTY SEEKING RELIEF AND ONLY TO THE EXTENT NECESSARY TO PROVIDE RELIEF WARRANTED BY THAT PARTY’S INDIVIDUAL CLAIM.  IF FOR ANY REASON THE ARBITRATION CLAUSE SET FORTH IN THESE TERMS OF USE IS DEEMED INAPPLICABLE OR INVALID, OR TO THE EXTENT THE ARBITRATION CLAUSE ALLOWS FOR LITIGATION OF DISPUTES IN COURT, YOU AND WIS BOTH WAIVE, TO THE FULLEST EXTENT ALLOWED BY LAW, ANY RIGHT TO PURSUE OR TO PARTICIPATE AS A PLAINTIFF OR AS A CLASS MEMBER IN ANY CLAIM ON A CLASS OR CONSOLIDATED BASIS OR IN A REPRESENTATIVE CAPACITY.</p><br/>
<h5>10.	  INDEMNIFICATION</h5>
<p class="small">10.1	 By accepting this Agreement, you agree to defend, indemnify and otherwise hold harmless the Companies and their officers, employees, agents, subsidiaries, affiliates, licensors, suppliers and other partners from any direct, indirect, incidental, special, consequential or exemplary damages resulting from your use of the Products.</p>
<p class="small">10.2	 Without limitation of the terms and conditions set forth in the WIS Privacy Policy, you understand and agree that WIS may disclose personally identifiable information if required to do so by law or in the good faith belief that such disclosure is reasonably necessary to comply with legal process, enforce this Agreement, or protect the rights, property or safety of the Companies and the public.</p><br/>
<h5>11.	  JURISDICTION</h5>
<p class="small">11.1	 The Products (excluding linked sites) are controlled by WIS from its offices in the Commonwealth of Pennsylvania in the United States.  By accessing the Products, you and WIS agree that all matters relating to your access to, or use of, the Products shall be governed by the statutes and laws of the Commonwealth of Pennsylvania, without regard to its conflicts of laws principles.  You and WIS also agree, and submit to the exclusive personal jurisdiction and venue of the courts of the State of Pennsylvania with respect to such matters.  WIS makes no representation that materials on the Products are appropriate or available for use in other locations, and accessing them from territories where their contents are illegal is prohibited.  Those who choose to access this site from other locations do so on their own initiative, and are responsible for compliance with local laws.</p><br/>
<h5>12.	  DIGITAL MILLENIUM COPYRIGHT ACT (DMCA)</h5>
<p class="small">GeoBlue complies with the DMCA.  If you have a concern regarding the use of copyrighted material on this Site, please contact GeoBlue, Attn: DMCA, 922 First Ave, King of Prussia, PA 19406, or alternately, you may send such concern via e-mail to: customerservice@bcbsglobalcore.com.  The DMCA specifies that all infringement claims must be in writing (either electronic mail or paper letter) and must include the following: (a) a physical or electronic signature of the copyright holder or a person authorized to act on his or her behalf; (b) a description of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works at a single online site are covered by a single notification, a representative list of such works at that site; (c) a description of the material that is claimed to be infringing or to be the subject of infringing activity, and information reasonably sufficient to permit the service provider to locate the material; (d) information reasonably sufficient to permit the service provider to contact you, such as an address, telephone number and, if available, an electronic mail address; (e) a statement that the you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent or the law; and (f) a statement that the information in the notification is accurate, and under penalty of perjury, that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.
                        Upon receipt of a properly filed complaint satisfying the requirements of the DMCA, GeoBlue will remove or block access to the allegedly infringing material, and may terminate the End User’s account per these Terms of Use.  If you believe in good faith that a notice of copyright infringement has been filed wrongly, you may submit a counter-notice to GeoBlue, Attn: DMCA, 933 First Avenue, King of Prussia, PA 19406 or alternately, you may send the counter-notice via e-mail to: customerservice@bcbsglobalcore.com.  If GeoBlue receives a valid counter notification, the DMCA provides that the removed or blocked information may be restored or access re-enabled.  GeoBlue may replace the removed material and cease disabling access to it in not less than ten (10) business days following receipt of the counter notification, unless GeoBlue first receives notice from the complaining party that such complaining party has filed an action seeking a court order to restrain the alleged infringer from engaging in infringing activity relating to the materials on this Site.
                        </p>
<h5>13.	  NOTICES</h5>
<p class="small">13.1	 You agree that WIS may communicate with you under this Agreement by means of e-mail, telephone call, a general notice posted on the Products, or by written communication delivered by first-class U.S. mail to the address that you have provided to WIS.  Once you opt-in to receive e-mails and telephone calls from WIS, and unless and until you elect or elected to opt-out of receiving e-mails or telephone calls from us, for contractual purposes, you: a) consent to receive communications from WIS via electronic means and/or telephone calls; and b) agree that all terms and conditions, agreements, notices, disclosures and other communications that we may provide to you whether electronically or via telephone calls, satisfy any legal requirements that such communications would satisfy if it were in writing. The foregoing does not affect your non-waivable rights. You may revoke your authorization for us to send you e-mails and/or contact you via telephone concerning the WIS Products and other matters pertaining to this Agreement, at any time by sending your request in writing specifying what you want to do to WIS at any time via e-mail or by letter delivered by first-class postage prepared U.S. mail or overnight courier to the following address:</p>
<p class="small">WIS<br/>Attn:  Terms of Use and End User License Agreement<br/>933 First Ave <br/>King of Prussia, PA 19087 <br/>E-mail: <a class="text-link" href="mailto:customerservice@bcbsglobalcore.com">customerservice@bcbsglobalcore.com</a>
</p><br/>
<h5>14.	  SURVIVAL</h5>
<p class="small">14.1	 The provisions of this Agreement entitled "Limitation of Liability," "Disclaimer of Warranties," "Indemnification," "Jurisdiction" and "General Provisions" will survive the termination of this Agreement.</p><br/>
<h5>15.	  GENERAL PROVISIONS</h5>
<p class="small">15.1	 Except as provided in a particular “Legal Notice” on the Products, this Agreement, along with the WIS Privacy Policy, constitute the entire agreement and understanding between you and WIS with respect to use of the Products, superseding all prior or contemporaneous communications with WIS.  </p>
<p class="small">15.2	 The provisions of this Agreement are severable, and in the event any provision is determined to be invalid or unenforceable, such invalidity or unenforceability shall not in any way affect the validity or enforceability of the remaining provisions.  </p>
<p class="small">15.3	 A printed version of this Agreement shall be admissible in judicial or administrative proceedings based upon or relating to use of the Products to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form.  </p>
<p class="small">15.4	 The section titles of this Agreement are displayed for convenience only and have no legal effect.  </p>
<p class="small">15.5	 Nothing in this Agreement shall be deemed to confer any third-party rights or benefits.</p>
<p class="small">15.6	 Use of the term "Products" also includes use of the Website or the Licensed Application individually. </p>
<p class="small">If you have any questions or comments or receive any unwanted e-mail from the Website, please contact our Webmaster via e-mail at <a class="text-link" href="mailto:customerservice@bcbsglobalcore.com">customerservice@bcbsglobalcore.com</a>.</p>
<p class="small">Copyright© 2021 Worldwide Insurance Services, LLC. All Rights Reserved.</p>
</div>
<form action="/Account/Login/?ReturnUrl=%2F" class="" id="main-form" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="l7l4kU4A0YRuUlE1fSWwhbR5bjYwnwOLRSDchgb5-Q22c3eBRqqtFmAOSWc15tgOrrLPfBTaBuRW3om0ac0H1POXccHhJVuqAiqgoXcjd0I1"/> <div class="form-group primaryhorizontalline">
<div class="checkbox">
<label class="text-inverse small ">
<input data-val="true" data-val-equalto="Please provide your acknowledgment by selecting the Terms and Conditions checkbox." data-val-equalto-other="*.IsTrue" id="TermsChecked" name="TermsChecked" type="checkbox" value="true"/><input name="TermsChecked" type="hidden" value="false"/>
<strong>I accept the terms and conditions described above.</strong>
</label>
</div>
</div>
<div class="form-group ">
<p class="text-inverse small">
                                Enter the <strong>first three letters or numbers</strong> of the Member ID number on your card.
                            </p>
<img src="/Assets/Images/card_sm.png"/>
<input data-val="true" data-val-regex="Please enter a valid ID code" data-val-regex-pattern="^([a-zA-Z0-9]{3})$" data-val-required="Please enter a valid ID code" id="AlphaPrefix" maxlength="3" name="AlphaPrefix" style="width:35%; margin:0 2% 0 2%;" type="text" value=""/>
<input class="btn btn-secondary mRight10" id="main-form-submit" style="padding:3px 12px" type="submit" value="GO"/>
</div>
</form> </div>
</div>
<div class="row">
<div class="col-md-12 text-center col-xs-12 ">
<div class="extraMargin50 hidden-xs hidden-sm"></div>
<h3 class="text-success">First time logging in or filing an international claim on the <span class="text-primary">Blue Cross Blue Shield Global<sup>®</sup> Core</span> website?<br/> <a class="text-link" href="/Account/Register/">Register</a>* for access.</h3>
<h5 class="text-success">Claims may be submitted for benefits for covered services received outside the United States, Puerto Rico and the U.S. Virgin Islands. </h5>
<button class="btn btn-success btn-lg pad20" onclick="window.location.href='/Account/AccountLogin/?ReturnUrl=%2F'" type="submit">LOGIN</button><br/><br/>
<h5 class="text-left mLeft10 mTop-10 text-success text-center">*You must be a registered user to submit a claim online.</h5>
</div>
</div>
<br/>
</div>
</div>
<div class="container align-center">
<div class="col-sm-1 col-md-1 hidden-xs">
</div>
<div class="alert alert" id="covidAlert" role="alert">
<h1><a class="alert-link text-link" href="https://www.who.int/health-topics/coronavirus" target="_blank">Coronavirus Disease (COVID-19)</a></h1>
<ul>
<li>
<p>For the latest details on the Coronavirus in specific countries, along with prevention strategies, symptoms and general information, please read more at  <a class="text-link" href="https://www.who.int/health-topics/coronavirus" target="_blank">World Health Organization's website</a>.</p>
</li>
<li>
<p>Please see your Plan website or call the customer service number on the back of your ID card for detailed information on your benefit coverage.</p>
</li>
<li>
<p>To obtain medical assistance or provider information, please contact the medical assistance vendor at 1.800.810.BLUE or call collect at 1.804.673.1177.</p>
</li>
<li>
<p><a class="text-primary" href="/Assets/Documents/COVID-19_member_flyer_core v6.pdf" target="_blank">View additional information</a> on COVID-19 support.</p>
</li>
<li>
<p><strong>Important Notice about Paper Claims:</strong> For expedited claims processing, we strongly encourage you to submit claims online using the <a class="text-link" href="/Claims/">eClaim</a> process available in your member account or email form and documentation to <a class="text-link" href="mailto:claims@bcbsglobalcore.com">claims@bcbsglobalcore.com</a>.</p>
</li>
</ul>
</div>
<div class="col-sm-11 col-md-11">
<a href="/Home/MobileApp/"><img class="img-responsive hidden-xs" src="/Assets/Images/mobile-banner.png"/></a>
</div>
</div>
</section>
<footer>
<section>
<div class="container">
<div class="row">
<hr style="border-color: black; max-width: 900px"/>
<!--FOOTER Large/Medium Screen-->
<div class="col-sm-offset-2 col-sm-offset-2 text-center no-gutter hidden-print">
<div class="footerlinks text-center">
<p class="col-sm-2 col-xs-12"><a href="/">Home</a></p>
<p class="col-sm-3 col-xs-12"><a href="/Home/PrivacyPolicy/">Privacy Policy</a></p>
<p class="col-sm-3 col-xs-12"><a href="/Home/TermsOfUse/">Terms of Use</a></p>
<p class="col-sm-1 col-xs-12"><a href="/Home/Help/">Help</a></p>
</div>
</div>
</div>
<br/>
<div class="row">
<div class="col-sm-12 ">
<div class="hidden-print col-sm-12 text-muted">
<p class="text-sm">
                            © 2021 GeoBlue. All rights reserved. GeoBlue is the trade name for the international health
                            insurance program of Worldwide Insurance Services (WIS), an independent licensee of Blue Cross Blue Shield Association. Blue Cross Blue Shield Association is an association of independent Blue Cross and Blue Shield companies. Blue Cross Blue Shield Global is a brand owned by Blue
                            Cross Blue Shield Association.
                        </p>
</div>
</div>
</div>
</div>
</section>
</footer>
<script src="/Assets/JavaScript/jquery-1.12.1.min.js" type="text/javascript"></script>
<script src="/Assets/JavaScript/jquery-ui-1.12.1.min.js" type="text/javascript"></script>
<script src="/Assets/JavaScript/bootstrap.min.js" type="text/javascript"></script>
<script src="/Assets/JavaScript/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/Assets/JavaScript/placeholder.min.js" type="text/javascript"></script>
<script type="text/javascript">
        $("input.date-picker").datepicker(
	    {
	        dateFormat: "mm/dd/yy",
	        changeMonth: true,
	        changeYear: true,
	        yearRange: "c-122:c+10"
	    });
        $(".date-picker-btn").click(function (evt) {
            $(this).siblings('input.date-picker').focus();
        });
    </script>
<script type="text/javascript">
		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments);
			}, i[r].l = 1 * new Date(); a = s.createElement(o),
			m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g;
			m.parentNode.insertBefore(a, m);
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-1861751-28', 'auto');
		ga('send', 'pageview');

		//send User Agent to allow Gomez Testing hits to be filtered
		var dimensionValue = navigator.userAgent;
		if (dimensionValue == "") {
			dimesnionValue = "TEST";
		};
		ga('set', 'dimension2', dimensionValue);
		//determine file path and name for event
		var pathArray = $(location).attr('pathname').split("/");
		var pathLength = pathArray.length - 1, fileName, pathName = "";
		for (var i = 1; i < pathLength; i++) {
			pathName += "/";
			pathName += pathArray[i];
		}
		if (pathName == "") {
			pathName = "/";
		}
		if (pathArray[i] == "") {
			fileName = "index";
		} else {
			fileName = pathArray[i];
		};

		//set Event counter
		var eCounter = 0;

		//trigger event
		$('a').click(function () {
			sendEvent($(this));
		});

		$(':button').click(function () {
			sendEvent($(this));
		});

		$('div.eventButton').click(function () {
			sendEvent($(this));
		});

		function sendEvent(eID) {
			eCounter++;
			var hasEvent = eID.attr('onclick') || "noEvent";
			var eIDLink = eID.attr('href') || "notDoc";
			if (eIDLink.substr(eIDLink.length - 4, 4) == ".pdf") {
				ga('send', 'event', pathName, fileName, eIDLink);
			}
			else if ((hasEvent.substring(0, 18) != "ga('send', 'event'")) {
				if (eID.attr('title')) {
					ga('send', 'event', pathName, fileName, eID.attr('title'));
				};
			};
		};

	</script>
<script type="text/javascript">
        $('#main-form-submit').click(function (e) {
            $(this).prop('disabled', true);
            $('#main-form').submit();
        });

    </script>
<script type="text/javascript">

        //disable bootstrap.dropdown-toggle for web and only enable it for mobile
        function hoverMenu() {
            //bootstrap.dropdownHover is by default disabled for mobile
            if ($(window).width() > 992) {
                $('.dropdown-toggle').addClass('disabled');
                $('.dropdown').mouseover(function () {
                    $(this).addClass('open');
                }).mouseout(function () {
                    $(this).removeClass('open');
                });
            } else {
                $('.dropdown-toggle').removeClass('disabled');
            }
        };

        $(document).ready(function () {
            hoverMenu();
        });

        if (typeof window.onresize != "undefined" && window.onresize != null) {
            window.onresize = hoverMenu();
        }
        else if (typeof document.body.onresize != "undefined" && document.body.onresize != null) {
            document.body.onresize = hoverMenu();
        }
    </script>
</body>
</html>
