<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="es"> <![endif]--><!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="es"> <![endif]--><!--[if IE 8]>         <html class="no-js lt-ie9" lang="es"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="es"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<title>No se encontró la página | Pegaso Consultoría y Construcción</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="Remodelación y Mantenimiento" name="description"/>
<link href="http://www.pegasocyc.com.mx/wp-content/uploads/2013/11/favicon1.png" rel="shortcut icon" type="image/x-icon"/>
<script type="text/javascript">
  	var virtue_URL = "https://www.pegasocyc.com.mx/wp-content/themes/virtue.1.0.0";
  </script>
<link href="//ajax.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.pegasocyc.com.mx\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.16"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.pegasocyc.com.mx/wp-content/themes/virtue.1.0.0/assets/css/bootstrap.css" rel="stylesheet"/>
<link href="https://www.pegasocyc.com.mx/wp-content/themes/virtue.1.0.0/assets/css/bootstrap-responsive.css" rel="stylesheet"/>
<link href="https://www.pegasocyc.com.mx/wp-content/themes/virtue.1.0.0/assets/css/virtue.css" rel="stylesheet"/>
<link href="https://www.pegasocyc.com.mx/wp-content/themes/virtue.1.0.0/assets/css/skins/default.css" rel="stylesheet"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
<script>window.jQuery || document.write('<script src="https://www.pegasocyc.com.mx/wp-content/themes/virtue.1.0.0/assets/js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script src="https://www.pegasocyc.com.mx/wp-content/themes/virtue.1.0.0/assets/js/vendor/modernizr-2.6.2.min.js" type="text/javascript"></script>
<link href="https://www.pegasocyc.com.mx/wp-json/" rel="https://api.w.org/"/>
<style media="screen" type="text/css">

  #thelogo {width:350px; height:130px; background:url(http://www.pegasocyc.com.mx/wp-content/uploads/2013/11/Logotipo_Pegaso_home.png) no-repeat;}
  #logo {padding-top:25px; padding-bottom:10px; margin-left:0px; margin-right:0px;}
  #nav-main ul.sf-menu {margin-top:40px; margin-bottom:10px;}
  .logofont {font-family:Arial;}, #logo a.brand {font-size:38px; font-weight:normal; line-height:40px; color:;}
  h1, h2, h3, h4, h5, .headerfont, .tp-caption {font-family:Arial;}
  h1 {font-size:38px; font-weight:normal; line-height:40px; color:;}
  h2 {font-size:32px; font-weight:normal; line-height:40px; color:;}
  h3 {font-size:28px; font-weight:normal; line-height:40px; color:;}
  h4 {font-size:24px; font-weight:normal; line-height:40px; color:;}
  h5 {font-size:18px; font-weight:normal; line-height:14px; color:24px;}
  body {font-family:Arial; font-size:14px; font-weight:normal; line-height:20px; color:;}
  .sf-menu a, .menufont, .topbarmenu ul li {font-family:Arial;}
  #nav-main ul.sf-menu a {font-size:12px; font-weight:normal; line-height:18px; color:;}
  #nav-second ul.sf-menu a {font-size:18px; font-weight:normal; line-height:22px; color:;}

  .home-message:hover {background-color:; background-color: rgba(, , , 0.6);}
  nav.woocommerce-pagination ul li a:hover, .wp-pagenavi a:hover, .accordion-heading .accordion-toggle.open {border-color: ;}
  a:hover, #nav-main ul.sf-menu ul li a:hover, .product_price ins .amount, .color_primary, .primary-color, #logo a.brand, #nav-main ul.sf-menu a:hover,
  .woocommerce-message:before, .woocommerce-info:before, #nav-second ul.sf-menu a:hover, .footerclass a:hover {color: ;}
  .widget_price_filter .ui-slider .ui-slider-handle, .product_item .kad_add_to_cart:hover, .kad-btn-primary, .woocommerce-message .button, 
  #containerfooter .menu li a:hover, .bg_primary, .portfolionav a:hover, .home-iconmenu a:hover, p.demo_store, .topclass {background: ;}

  a {color: ;}
  .kad-btn-primary:hover, .woocommerce-message .button:hover {background: ;}

  .contentclass {background:    ;}
  .headerclass {background:    ;}
  .navclass {background:    ;}
  .featclass {background:    ;}
  .footerclass {background:#005fab    ;}
  body {background: url(http://www.pegasocyc.com.mx/wp-content/uploads/2013/11/fondo.jpg) no-repeat  ; background-attachment:fixed;}

  
</style>
<link href="https://www.pegasocyc.com.mx/feed/" rel="alternate" title="Pegaso Consultoría y Construcción Feed" type="application/rss+xml"/>
</head>
<body class="error404 wide">
<div class="container" id="wrapper">
<!--[if lt IE 8]><div class="alert">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</div><![endif]-->
<header class="banner headerclass" role="banner">
<div class="container">
<div class="row">
<div class="span4 clearfix">
<div class="logocase" id="logo">
<a class="brand logofont" href="https://www.pegasocyc.com.mx/">
<div id="thelogo"></div> </a>
</div> <!-- Close #logo -->
</div><!-- close span5 -->
<div class="span8">
<nav class="clearfix" id="nav-main" role="navigation">
</nav>
</div> <!-- Close span7 -->
</div> <!-- Close Row -->
</div> <!-- Close Container -->
<section class="navclass" id="cat_nav">
<div class="container">
<nav class="clearfix" id="nav-second" role="navigation">
<ul class="sf-menu" id="menu-pegaso"><li class="menu-inicio menu-inicio"><a href="https://www.pegasocyc.com.mx/">INICIO</a></li>
<li class="menu-ingenieria-de-puentes menu-ingenieria-de-puentes"><a href="https://www.pegasocyc.com.mx/ingenieria-de-puentes/">INGENIERÍA DE PUENTES</a></li>
<li class="menu-ingenieria-de-vialidades menu-ingenieria-de-vialidades"><a href="https://www.pegasocyc.com.mx/ingenieria-de-vialidades/">INGENIERÍA DE VIALIDADES</a></li>
<li class="menu-obra-nueva menu-obra-nueva"><a href="https://www.pegasocyc.com.mx/obra-nueva/">OBRA NUEVA</a></li>
<li class="menu-remodelacion-y-mantenimiento menu-remodelacion-y-mantenimiento"><a href="https://www.pegasocyc.com.mx/remodelacion-y-mantenimiento/">REMODELACIÓN Y MANTENIMIENTO</a></li>
<li class="menu-contacto menu-contacto"><a href="https://www.pegasocyc.com.mx/contacto/">CONTACTO</a></li>
</ul> </nav>
</div><!--close container-->
</section>
</header>
<div class="wrap contentclass" role="document">
<div class="titleclass" id="pageheader">
<div class="container">
<div class="page-header">
<h1>
    Not Found  </h1>
</div> </div><!--container-->
</div><!--titleclass-->
<div class="container" id="content">
<div class="row">
<div class="main span12" role="main">
<div class="alert">
  Sorry, but the page you were trying to view does not exist.</div>
<p>It looks like this was the result of either:</p>
<ul>
<li>a mistyped address</li>
<li>an out-of-date link</li>
</ul>
<form action="https://www.pegasocyc.com.mx/" class="searchform" id="searchform" method="get" role="search">
<div>
<label class="screen-reader-text" for="s">Buscar:</label>
<input id="s" name="s" type="text" value=""/>
<input id="searchsubmit" type="submit" value="Buscar"/>
</div>
</form></div>
</div><!-- /.row-->
</div><!-- /.content -->
</div><!-- /.wrap -->
<footer class="content-info footerclass" id="containerfooter" role="contentinfo">
<div class="container">
<div class="row">
</div>
<div class="footercredits clearfix">
<p>© 2021 Pegaso Consultoría y Construcción</p>
</div>
</div>
</footer>
<script src="https://www.pegasocyc.com.mx/wp-content/themes/virtue.1.0.0/assets/js/plugins.js" type="text/javascript"></script>
<script src="https://www.pegasocyc.com.mx/wp-content/themes/virtue.1.0.0/assets/js/main.js" type="text/javascript"></script>
<script src="https://www.pegasocyc.com.mx/wp-includes/js/wp-embed.min.js?ver=4.9.16" type="text/javascript"></script>
</div><!--Wrapper-->
</body>
</html>
