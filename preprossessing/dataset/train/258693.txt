<!DOCTYPE html>
<html class="no-js">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Customized Software Development Company | BridgeLogic
</title>
<meta content="BridgeLogic Software is an ISO 9001:2015 Certified ERP software development company. We provide specialized services like School Management, NBFC, Health Care, Loan Management Software, Mobile Applications &amp; more.
" name="description"/>
<meta content="ERP Software,Custom Loan Management Software, School Erp, Real Estate Erp Provider
" name="keyword"/>
<meta content="BRIDGELOGIC SOFTWARE PVT LTD" name="author"/>
<meta content="INDEX,FOLLOW" name="robots"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link href="assets/images/logo/favicon.png" rel="shortcut icon"/>
<!-- Google Webfonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"/>
<!-- Theme Style -->
<link href="assets/css/style.css" rel="stylesheet"/>
<!-- Animate.css -->
<link href="assets/css/animate.css" rel="stylesheet"/>
<!-- Icomoon Icon Fonts-->
<link href="assets/css/icomoon.css" rel="stylesheet"/>
<!-- Owl Carousel -->
<link href="assets/css/owl.carousel.min.css" rel="stylesheet"/>
<link href="assets/css/owl.theme.default.min.css" rel="stylesheet"/>
<!-- Magnific Popup -->
<link href="assets/css/magnific-popup.css" rel="stylesheet"/>
<!-- Modernizr JS -->
<script src="assets/js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->
<style>
            @media screen and ( min-width: 1200px )
            {	.container{
                  width: 98% !important;
              }
            }

        </style>
<script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-103576842-1', 'auto');
            ga('send', 'pageview');

        </script>
</head>
<body>
<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="exampleModal" role="dialog" tabindex="-1">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button aria-label="Close" class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">×</span>
</button>
<h5 class="modal-title" id="exampleModalLabel">Enquiry</h5>
</div>
<div class="modal-body">
<div class="row">
<form action="#" class="" id="data_form" method="post" name="data_form">
<div class="col-md-6 col-xs-12 col-sm-6">
<div class="form-group">
<label class="sr-only" for="name">Name</label>
<input class="form-control input-sm" id="name" name="name" placeholder="Name*" type="text"/>
<input id="page" name="page" type="hidden" value="http://www.bridgelogicsystem.com/login123/%09%0A"/>
</div>
</div>
<div class="col-md-6 col-xs-12 col-sm-6">
<div class="form-group">
<label class="sr-only" for="email">Email</label>
<input class="form-control input-sm" id="email" name="email" placeholder="Email*" type="text"/>
</div>
</div>
<div class="col-md-6 col-xs-12 col-sm-6">
<div class="form-group">
<label class="sr-only" for="email">Contact No</label>
<input class="form-control input-sm" id="contact_no" name="contact_no" placeholder="Contact No.*" type="text"/>
</div>
</div>
<div class="col-md-6 col-xs-12 col-sm-6">
<div class="form-group">
<label class="sr-only" for="Enquiry For">Enquiry For</label>
<select class=" form-control input-sm" id="enquiry_for" name="enquiry_for">
<option value="Custom Software">Customized Software</option>
<option value="NBFC (Non Banking Finance Company)">NBFC (Non Banking Finance Company)</option>
<option value="Banking">Banking</option>
<option value="Vehicle Finance">Vehicle Finance</option>
<option value="Mobile Application">Mobile Application</option>
<option value="Real Estate">Real Estate</option>
<option value="School / College ">School / College</option>
<option value="eCommerce Solutions">eCommerce Solutions</option>
<option value="Project Management">Project Management</option>
<option value="Others">Others</option>
</select>
</div>
</div>
<div class="col-md-6 col-xs-12 col-sm-6">
<div class="form-group">
<label class="sr-only" for="Organization Name">Organization Name</label>
<input class="form-control input-sm" id="org_name" name="org_name" placeholder="Organization Name" type="text"/>
</div>
</div>
<div class="col-md-6 col-xs-12 col-sm-6">
<div class="form-group">
<label class="sr-only" for="Organization Location">Organization Location</label>
<input class="form-control input-sm" id="org_location" name="org_location" placeholder="Organization Location" type="text"/>
</div>
</div>
<div class="col-md-12 col-xs-12 col-sm-12">
<div class="form-group">
<label class="sr-only" for="message">Message</label>
<textarea class="form-control input-sm" id="message" name="message" placeholder="Message" rows="3"></textarea>
</div>
</div>
<div class="col-md-9">
<div id="result"></div>
</div>
<div class="col-md-3">
<div class="form-group text-right">
<input class="btn btn-primary btn-sm " id="button_submit" onclick="doCallAjax('data_form', './post.html?submit=true', 'result')" type="button" value="Send"/>
</div>
</div>
</form> </div>
</div>
<div class="modal-footer">
<!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                         <button type="button" class="btn btn-primary">Save changes</button>-->
</div>
</div>
</div>
</div>
<header id="fh5co-header" role="banner">
<div class="bg-blue row hidden-lg hidden-sm">
<div class=" col-xs-12 text-center text-bold text-white no-padding-top padding1 " style="line-height:31px;">
<i class="fa fa-phone text-bold text-white"> : </i> +91 731 4281318 

                </div>
</div>
<div class="bg-blue row hidden-xs text-white">
<div class=" col-xs-offset-1 col-sm-4 col-xs-12 text-left no-padding-top padding1 " style="line-height:31px;">
<i class="fa fa-phone text-bold"> : </i> <b>+91 731 4281318 </b> <b>  |   </b>
<i class="fa fa-envelope-open text-bold"> : </i><a class="text-white" href="mailto:sales@bridgelogicsystem.com"> <b>sales@bridgelogicsystem.com</b></a>
</div>
<div class="col-sm-5 text-right no-padding-top hidden-xs bounceInRight animated " style="line-height:31px;">
<a class="text-white" href="contacts.html"><b><span class="glyphicon glyphicon-facetime-video "></span> DEMO REQUEST</b></a>
</div>
<div class="col-sm-2 no-padding-top padding1 hidden-xs"></div>
</div>
<a class="btn btn-flat btn-danger bounceInUp animated" data-target="#exampleModal" data-toggle="modal" href="#" style="float: left;
                   position: fixed;
                   top: 120px;
                   z-index: 999;
                   right: 0px;margin:0px;"><i class="fa fa-envelope"></i> Enquire Now</a>
<nav class="navbar navbar-default" role="navigation">
<div class="container-fluid">
<div class="navbar-header">
<!-- Mobile Toggle Menu Button -->
<a aria-controls="navbar" aria-expanded="false" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-target="#fh5co-navbar" data-toggle="collapse" href="#"><i></i></a>
<a class="navbar-brand bounceInLeft animated" href="./"><img src="assets/images/logo/logo.png" style="width:80%;"/></a>
</div>
<div class="navbar-collapse collapse" id="fh5co-navbar">
<ul class="nav navbar-nav navbar-right">
<li><a href="./"><span>Home <span class="border"></span></span></a></li>
<li><a href="industries.html"><span>Industries <i class="fa fa-angle-down"></i><span class="border"></span></span></a>
<ul class="dropdown-menu">
<li><a href="industry-nbfc-solutions.html">NBFC &amp; MFI</a></li>
<li><a href="industry-banking-solutions.html">Banking</a></li>
<li><a href="industry-infra-sector-solutions.html">Infrastructure</a></li>
<li><a href="industry-health-care-solutions.html">Health Care</a></li>
<li><a href="industry-education-solutions.html">Education</a></li>
</ul>
</li>
<li><a href="services.html"><span>Services <i class="fa fa-angle-down"></i><span class="border"></span></span></a>
<ul class="dropdown-menu">
<li><a href="services-software-development.html">Software Development</a></li>
<li><a href="services-mobile-application.html">Mobile Apps</a></li>
<li><a href="services-api-vpn-integration.html">API Integration</a></li>
<li><a href="services-data-analysis.html">Data Analysis</a></li>
<li><a href="services-support-maintenance.html">Support &amp; Maintenance</a></li>
<li><a href="services-gis-consultancy.html">GIS Consultancy</a></li>
</ul>
</li>
<li><a href="about-us.html"><span>About Us <span class="border"></span></span></a></li>
<li><a href="contacts.html"><span>Contact Us <span class="border"></span></span></a></li>
<li><a href="careers.html"><span>Careers<span class="border"></span></span></a></li>
</ul>
</div>
</div>
</nav>
</header>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function ()
    {
        if ($("div").hasClass("home"))
        {
            $('.navbar-nav li:first-child').addClass('active');
        }
    });
</script>
<style>
    .top_box {
        width: 100%;
        float: left;
        background: #F9F9F9;;
        padding: 8px;
        margin-bottom: 20px;
    }
   @media only screen and (max-width:768px){
    .padding10{
        padding:15px;
    }
    }
</style>
<div class="home">
<div class="fh5co-slider">
<div class="owl-carousel owl-carousel-fullwidth">
<div class="item" style="background-image:url(assets/images/slide_3.jpg)">
<div class="fh5co-overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2">
<div class="fh5co-owl-text-wrap">
<div class="fh5co-owl-text text-center to-animate">
<h1 class="fh5co-lead">Infrastructure Solutions</h1>
<h2 class="fh5co-sub-lead">Experience the cutting-edge technology benefit in development of land to selling of plot or flat.</h2>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="item" style="background-image:url(assets/images/slide_1.jpg)">
<div class="fh5co-overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2">
<div class="fh5co-owl-text-wrap">
<div class="fh5co-owl-text text-center to-animate">
<h1 class="fh5co-lead">Loan Management Software</h1>
<h2 class="fh5co-sub-lead">Automate your sales force, application processing, collection etc. with enriched reporting power.</h2>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="item" style="background-image:url(assets/images/slide_2.jpg)">
<div class="fh5co-overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2">
<div class="fh5co-owl-text-wrap">
<div class="fh5co-owl-text text-center to-animate">
<h1 class="fh5co-lead">Core Banking Solution</h1>
<h2 class="fh5co-sub-lead">Deposits, Loans, Membership, Collection, Dividend etc. may technically enrich your co-operative society or bank.</h2>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="item" style="background-image:url(assets/images/slide_4.jpg)">
<div class="fh5co-overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2">
<div class="fh5co-owl-text-wrap">
<div class="fh5co-owl-text text-center to-animate">
<h1 class="fh5co-lead">Health Care Solutions</h1>
<h2 class="fh5co-sub-lead">Keep the track of your sales force and boost them with technology advantage, which helps to come up with desired results in minimal effort &amp; time. Keep up-to-date record of inventories.</h2>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="fh5co-main">
<div class="container " id="fh5co-products ">
<div class="row ">
<div class="">
<span style="text-align:center;font-size:17px;">The successful key to software solutions is to find the right companion 
                        for software development. BridgeLogic Software is a well known ISO 
                        9001:2015 Certified IT company located in Indore, Madhya Pradesh, Center 
                        of India. We specializes in custom software development &amp; mobile 
                        application and provides these specialized services worldwide. We at 
                        BridgeLogic works hard to offer you a reliable Loan Management Software, 
                        NBFC Software, Microfinance Software, Health Care Software, Campus ERP 
                        Software, Mobile Application Development and more. Our mission is to 
                        provide turnkey solutions by using strategic innovative tactics to bring 
                        your creative ideas into reality.</span></div>
</div>
<div class="row ">
<div class="headingBox">
<h2 class="line_heading"><span class="bg-green">Focused Industries</span></h2> </div>
</div>
<div class="row ">
<div class="col-md-2 col-xs-12 padding10 ">
<div class="top_box top_box01"> <a href="industry-nbfc-solutions.html"> <img alt="NBFC,Loan Management Software,Loan Servicing, Loan Origination, MFI Software" class="img-responsive center" src="assets/images/box-index/loan_management_software.png"/>
<h2 class="fh5co-section-lead text-center">NBFC</h2>
</a>
</div>
</div>
<div class="col-md-2 col-xs-12 padding10 ">
<div class="top_box top_box01">
<a href="industry-banking-solutions.html">
<img alt="Retail Non Banking , Banking Software, Banking data services, Data Analysis for Banking Data, Anual Banking Data Repots, Coperative Banking Services and software" class="img-responsive center " src="assets/images/box-index/banking.png"/>
<h3 class="fh5co-section-lead text-center">BANKING</h3>
<!--                        <h4>Banking Software</h4>-->
</a>
</div>
</div>
<div class="col-md-2 col-xs-12 padding10 ">
<div class="top_box top_box01">
<a href="industry-education-solutions.html">
<img alt="campus erp software, college management software, campus erp solution, school, college, SME" class="img-responsive center " src="assets/images/box-index/ecampus.png"/>
<h3 class="fh5co-section-lead text-center">COLLEGE/SCHOOL</h3>
<!--                        <h4>Management Software</h4>-->
</a>
</div>
</div>
<div class="col-md-2 col-xs-12 padding10 ">
<div class="top_box top_box01">
<a href="industry-infra-sector-solutions.html">
<img alt="Infrastructure Development Software, Real estate commercial software, rent management software, infra marketing" class="img-responsive center " src="assets/images/box-index/realestate.png"/>
<h3 class="fh5co-section-lead text-center">REAL ESTATE </h3>
<!--                        <h4>Infrastructure Software</h4>-->
</a>
</div>
</div>
<div class="col-md-2 col-xs-12 padding10 ">
<div class="top_box top_box01">
<a href="industry-health-care-solutions.html">
<img alt="Hospital Management Software, ware housing management, RM reporting Tools, Accounts Management Software,Health care, medicine management" class="img-responsive center " src="assets/images/box-index/healthcare.png"/>
<h3 class="fh5co-section-lead text-center">HEALTH CARE</h3>
<!--                        <h4>RM Reporting Tools</h4>-->
</a>
</div>
</div>
<div class="col-md-2 col-xs-12 padding10 ">
<div class="top_box top_box01">
<a href="industry-customized-solutions.html">
<img alt="customized erp solution, erp services in india, erp solution and services for customized data anlysis" class="img-responsive center " src="assets/images/box-index/custom-erp.png"/>
<h3 class="fh5co-section-lead text-center">OTHERS</h3>
<!--                        <h4>Custom ERP | Software</h4>-->
</a>
</div>
</div>
</div>
</div>
<div class="container" id="fh5co-products">
<div class="row">
<div class="headingBox">
<h2 class="line_heading"><span class="bg-orange">Services Offered</span></h2> </div>
</div>
<div class="row">
<div class="col-md-2 col-xs-12 padding10 ">
<div class="top_box top_box01">
<a href="services-software-development.html">
<img alt="NBFC,Loan Management Software,Loan Servicing, Loan Origination, MFI Software" class="img-responsive center " src="assets/images/box-index/software.jpg"/>
<!--                            <h3 class="fh5co-section-lead text-center">NBFC</h3>-->
<h3 class="fh5co-section-lead text-center">SOFTWARE</h3>
</a>
</div>
</div>
<!--                <div class="col-md-3 col-sm-4 col-xs-6 col-xs-12 ">
                                       <div class="top_box top_box01">
                                            <a href="industry-nbfc-solutions.html">
                                            <img src="assets/images/box-index/credit_loan_management_soft.png" alt="Vehicle Loan Management, Automobiles Two Wheeler, Four Wheeler, Three Wheeler,MCV, HCV,ICV,SCU Loan Software" class="img-responsive center ">
                                            <h3 class="fh5co-section-lead text-center">VEHICLE | AUTOMOBILES</h3>
                                            <h4>Loan Origination | Servicing</h4>
                    
                    </a>
                                        </div>
                                    </div>-->
<div class="col-md-2 col-xs-12 padding10 ">
<div class="top_box top_box01">
<a href="services-mobile-application.html">
<img alt="Mobile Apps Development for Financial Application" class="img-responsive center " src="assets/images/box-index/mobile_apps.jpg"/>
<!--                        <h3 class="fh5co-section-lead text-center">BANKING</h3>-->
<h3 class="fh5co-section-lead text-center">MOBILE APPS</h3>
</a>
</div>
</div>
<!--                <div class="col-md-3 col-sm-4 col-xs-6 col-xs-12 ">
                                       <div class="top_box top_box01">
                                            <a href="services-mobile-application.html">
                                            <img src="assets/images/box-index/amortization.png" alt="Mobile apps for andriod, window,ios for mutiple loans software, collection mobile apps with advance technologies having Hybrid mobile application" class="img-responsive center ">
                                            <h3 class="fh5co-section-lead text-center">MOBILE APPS</h3>
                                            <h4>IOS | ANDRIOD</h4>
                    </a>
                    
                                        </div>
                                    </div>-->
<div class="col-md-2 col-xs-12 padding10 ">
<div class="top_box top_box01">
<a href="services-api-vpn-integration.html">
<img alt="Api for CIBIL, Transunion, Equifax, Any CIBIL SCORE" class="img-responsive center " src="assets/images/box-index/api.png"/>
<!--                        <h3 class="fh5co-section-lead text-center">COLLEGE/SCHOOL</h3>-->
<h3 class="fh5co-section-lead text-center">API INTEGRATION</h3>
</a>
</div>
</div>
<div class="col-md-2 col-xs-12 padding10 ">
<div class="top_box top_box01">
<a href="services-data-analysis.html">
<img alt="Data Analysis" class="img-responsive center " src="assets/images/box-index/data_analysis.jpg"/>
<!--                        <h3 class="fh5co-section-lead text-center">REAL ESTATE </h3>-->
<h3 class="fh5co-section-lead text-center">DATA ANALYSIS</h3>
</a>
</div>
</div>
<div class="col-md-2 col-xs-12 padding10 ">
<div class="top_box top_box01">
<a href="services-support-maintenance.html">
<img alt="MAINTENANCE" class="img-responsive center " src="assets/images/box-index/website-maintenance.jpg"/>
<!--                        <h3 class="fh5co-section-lead text-center">HEALTH CARE</h3>-->
<h3 class="fh5co-section-lead text-center">MAINTENANCE</h3>
</a>
</div>
</div>
<div class="col-md-2 col-xs-12 padding10 ">
<div class="top_box top_box01">
<a href="services-gis-consultancy.html">
<img alt="customized gis data anlysis, GIS data center services" class="img-responsive center " src="assets/images/box-index/gis_data.jpg"/>
<!--                        <h3 class="fh5co-section-lead text-center">OTHERS</h3>-->
<h3 class="fh5co-section-lead text-center">GIS CONSULTANCY</h3>
</a>
</div>
</div>
</div>
</div>
<!-- Products -->
<div class="container">
<div class="row">
<div class="headingBox">
<h2 class="line_heading"><span class="bg-blue">Key Features</span></h2> </div>
</div>
<div class="col-md-12">
<div class="row">
<div class="col-md-4 col-sm-4 fh5co-feature-border">
<div class="fh5co-feature">
<div class="fh5co-feature-icon to-animate">
<i class="icon-bag"></i>
</div>
<div class="fh5co-feature-text">
<h3>100% Customized</h3>
<p style="font-size:13px;">Experience 100% customized application, hand picked for your business requirement.</p>
</div>
</div>
<div class="fh5co-feature no-border">
<div class="fh5co-feature-icon to-animate">
<i class="icon-head"></i>
</div>
<div class="fh5co-feature-text">
<h3>Standardization</h3>
<p style="font-size:13px;">Experience the industry standardization to meet with requirements of regulatory bodies.</p>
</div>
</div>
</div>
<div class="col-md-4 col-sm-4">
<div class="fh5co-feature">
<div class="fh5co-feature-icon to-animate">
<i class="icon-microphone"></i>
</div>
<div class="fh5co-feature-text">
<h3>Secured</h3>
<p style="font-size:13px;">Secure your valauble data from phishing, hackers and competitors.</p>
</div>
</div>
<div class="fh5co-feature no-border">
<div class="fh5co-feature-icon to-animate">
<i class="icon-clock2"></i>
</div>
<div class="fh5co-feature-text">
<h3>Transformation</h3>
<p style="font-size:13px;">Transform your ideas into software with technical partner.</p>
</div>
</div>
</div>
<div class="col-md-4 col-xs-12">
<div class="row">
<div class="direct-chat-messages">
<!--                            <div class="direct-chat-msg">
                                
                                                                <div class="direct-chat-text">
                                                                    100% customized modules development
                                                                </div>
                                
                                                            </div>-->
<div class="direct-chat-msg ">
<div class="direct-chat-text">
                                        No need to change existing process flow
                                    </div>
</div>
<div class="direct-chat-msg">
<div class="direct-chat-text">
                                        Team of dedicated domain expertise professionals
                                    </div>
</div>
<div class="direct-chat-msg ">
<div class="direct-chat-text">
                                        24/7 Support
                                    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Features -->
<div class="container">
<div class="row">
<div class="headingBox">
<h2 class="line_heading"><span class="bg-orange">Our Clients</span></h2> </div>
</div>
<div class="row text-center">
<div class="customer-logos">
<div class="slide"> <img alt="" class="resourceimgs no-padding" src="./assets/clients/1railway.jpg" style="border: 1px solid #f0efef;"/></div>
<div class="slide"> <img alt="" class="resourceimgs no-padding" src="./assets/clients/2bcfpl.jpg" style="border: 1px solid #f0efef;"/></div>
<div class="slide"> <img alt="" class="resourceimgs no-padding" src="./assets/clients/3dubaiautogallery.jpg" style="border: 1px solid #f0efef;"/></div>
<div class="slide"> <img alt="" class="resourceimgs no-padding" src="./assets/clients/4BBBlogo.png" style="border: 1px solid #f0efef;"/></div>
<div class="slide"> <img alt="" class="resourceimgs no-padding" src="./assets/clients/5isfc.jpg" style="border: 1px solid #f0efef;"/></div>
<div class="slide"> <img alt="" class="resourceimgs no-padding" src="./assets/clients/6tdsgroup.jpg" style="border: 1px solid #f0efef;"/></div>
<div class="slide"> <img alt="" class="resourceimgs no-padding" src="./assets/clients/7pgisystem.png" style="border: 1px solid #f0efef;"/></div>
<div class="slide"> <img alt="" class="resourceimgs no-padding" src="./assets/clients/amodi.png" style="border: 1px solid #f0efef;"/></div>
<div class="slide"> <img alt="" class="resourceimgs no-padding" src="./assets/clients/evs.png" style="border: 1px solid #f0efef;"/></div>
<div class="slide"> <img alt="" class="resourceimgs no-padding" src="./assets/clients/makemyemi.jpg" style="border: 1px solid #f0efef;"/></div>
<div class="slide"> <img alt="" class="resourceimgs no-padding" src="./assets/clients/particled.png" style="border: 1px solid #f0efef;"/></div>
<div class="slide"> <img alt="" class="resourceimgs no-padding" src="./assets/clients/pgp.png" style="border: 1px solid #f0efef;"/></div>
<div class="slide"> <img alt="" class="resourceimgs no-padding" src="./assets/clients/rcm.png" style="border: 1px solid #f0efef;"/></div>
<div class="slide"> <img alt="" class="resourceimgs no-padding" src="./assets/clients/savs.png" style="border: 1px solid #f0efef;"/></div>
<div class="slide"> <img alt="" class="resourceimgs no-padding" src="./assets/clients/ssim.png" style="border: 1px solid #f0efef;"/></div>
</div>
</div>
</div>
</div>
</div>
<footer id="fh5co-footer border-top">
<div class="container border-top">
<div class="col-md-3 col-sm-3">
<div class="fh5co-footer-widget top-level" style="color:#0a0a0a;">
<h4 class="fh5co-footer-lead text-blue text-bold"><img src="assets/images/logo/logo.png"/></h4>
<h4 class=" fh5co-sidebox fh5co-sidebox-lead">BridgeLogic Software Pvt. Ltd.</h4> <br/>
<i class="fa fa-phone text-green"></i> +(91) 731 428 1318<br/>
<i class="fa fa-mobile text-red"></i>   +(91) 888 966 7220<br/>
<i class="fa fa-envelope text-blue"></i>  <a href="mailto:sales@bridgelogicsystem.com">sales@bridgelogicsystem.com</a><br/>
</div>
</div>
<div class="col-md-4 col-sm-4 text-right">
<h4 class="fh5co-footer-lead text-blue text-bold text-left"><br/></h4>
<img class=" text-center img-responsive" src="assets/images/iso_certification.png" style=""/>
</div>
<div class="col-md-5 col-sm-5 col-xs-12 text-right">
<h4 class="fh5co-footer-lead text-blue text-bold text-left"><br/><br/><br/><br/><br/><br/></h4>
<ul class="fh5co-social ">
<li><a href="https://www.facebook.com/bridgelogic/" style="color:#ecebea !important;" target="_blank"><i class="icon-facebook text-blue" style="font-size:25px;"></i></a></li>
<li><a href="https://twitter.com/bridgelogic" style="color:#ecebea !important;" target="_blank"><i class="icon-twitter text-blue" style="font-size:25px;"></i></a></li>
<li><a href="https://www.linkedin.com/in/bridgelogicsystem/" style="color:#ecebea !important;" target="_blank"><i class="icon-linkedin text-blue" style="font-size:25px;"></i></a></li>
</ul>
<span class="text-black"> All rights reserved BridgeLogic Software Pvt.Ltd.</span> <br/><a href="terms-and-condition.html">Terms &amp; Conditions</a> |<a href="privacy-policy.html"> Privacy Policy</a>  |<a href="sitemap.xml"> <i class="fa fa-sitemap"></i></a>  |<a href="rss.xml"> <i class="fa fa-rss"></i></a>
</div>
</div>
</footer>
<!-- jQuery -->
<script src="assets/js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="assets/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- Owl carousel -->
<script src="assets/js/owl.carousel.min.js"></script>
<!-- Waypoints -->
<script src="assets/js/jquery.waypoints.min.js"></script>
<!-- Magnific Popup -->
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<!-- Main JS -->
<script src="assets/js/slick.js" type="text/javascript"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
		var cpage=document.location.pathname.match(/[^\/]+$/)[0];
		$(".nav li a[href='"+cpage+"']").parent().addClass("active");
	</script>
<script type="text/javascript">function add_chatinline(){var hccid=17347143;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
add_chatinline(); </script>
</body>
</html>
