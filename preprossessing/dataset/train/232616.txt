<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "97650",
      cRay: "6111267d7aed2495",
      cHash: "c48ec07f0b990d5",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYml0b2ZmdW4uY29tL3dlaXJkX3N0dWZmX3doZW5faGVsbF9mcmVlemVzX292ZXIuaHRt",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "KJXziUgfj/Fj51gVWlkP8iXhVxzL4tWpNh3lXadvH8q6AsdurX/AL4EyFDMSq59P+cIT9W6g7Xa8ARjFklUWbkt848RCNPqR7GhxsQGBMqrcZvZmumZnWTH3ccJ8nHDOvCw1osCnCJkdVY5GjtZRB1oVkQK0okkSIyLPM8IHiROZsyqpzlC4j2fZK81HgfwOwApqpqZBLSo91No0wP9gMTPxpOPH2fTelkyHsaFW/jJlOURsI6bie5++qdTuwPqvH49+6Z/WZsqjrhgAgN9dYJQuIZFHf4dBlHA4CM2G1uwK+9RVlUL+IosIpxfi5aozoouINyefe1CJsWI0x17qLfzHS8utRmauKxjv4MBQnnLDa1BiD1BlofZCRyn9dRkE0OkVB6/zqB6L51DkVhQ9Ot3fQtK2ECPfqdjmm4hZxeahiudGMHRERQXWHFjyXNQM7wWy+e9L/IngdiO0oQw+sO9iyA1uJ5ZaJhIZCIvGcFoyztKArA59R9jEaOwtCXPcg9T4ELLdnuQ+wpGFu1p4KOvo5BL7dv4AI9vHnKnL6Y8j0OANP2G6+nePM9CDHfj6GOqBACPYQ1IkJb9h6GBGgLOK60Ut7y4bDCWS0OY+mR0C7VZSfuVaOUbDlfjmB6o0f5z+Wig9urpS7jnOoRywuoXCbjKe7xMdQ+u7RisDhsnii/RkgGHqeHOlwKhhgbAWlpF1PLVJjApgEXoufwVVb+Wq5ViNq8F763CvUgsBJmOlj3pCFkYZOMWSKQ7sCcJ2",
        t: "MTYxMDU2MjIyNi44MDEwMDA=",
        m: "gbFW1DAtVcviWODKRkQTNhFWWnE48HTSjCcT7/Fl7QI=",
        i1: "s9+yB/MWao6g4CJ8itrrKA==",
        i2: "pyjyCn2/sIVH0YWxT367hw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "d9XlVfCyOCzOQs8z++/XE8WT2ciN5mdq59pf38rNXzk=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.bitoffun.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/weird_stuff_when_hell_freezes_over.htm?__cf_chl_captcha_tk__=e7c0724e1a4ce3c018320eda4ee5a6b48bc6a9f2-1610562226-0-AVKNk8XIqngb4oQM69PPZHpf13C0u36tpD-LvjetIQsPA-IxYspxTuzOPXqxNV1qrux2e-hShVMllnzM-e3idU5H8ub0mjQ_8aHcF1FLiEWOAk1MkI074O9bfJoRRHSSquUmUs6pmQHJm0T9tI1_xlxnyJtkEfGKgYEeEyUUQxWduPgxNozi9ClwxVtiJR02ESne6FKH-gQt-yoT_m-1y7ZJD9AIQ3z8AsY_pOwLA4ck2T8zt2bF2DnsaFL-wz5Sc_VfrvcbCk-NLbKp_XhMLng_GccnbRuRXCnVhXPJCpRANmMLQSXNo2ZtF2nxAFMjoO65bCGZLO8eaEQcmq4-Bgi9qPXuJ8bBDDKBBN5DJkbSWJTlyF37UfFk9TSidfcHSoeu-d0lm6YpAilAoLYIWjrQOqUSwrJ9L_DGUjt_7NrV_S6wGBDrApctDVzDVBICabkbquZcCYpW0uXX-yP0aK8_0RQszNa67BzA79HvvGno9vCq70gCQoXX7Kk63lATevv7pA33HIFJlpvI4_pTemuITy26jdKNHDAlYoeJjlF34tiHwbgYa6saJ1a0Fa2MOA" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="4b24906e526069a4b0bf275a77679ce1b17cd844-1610562226-0-Aa5vKWTmXoC7Mpix65VDt8AEuLWjPCUEXwlDnT+jGc+4vTRrP72OcYXLJuy6ts4+L/ZlWDnChgH0/XJxIHjLT5W1GIjv9R+R19FZ6jyLG72wqpRamnG2NCu9kbMhWlWgcBmVSZnx+kHLbQd8IiODv6IEzaxGWK5TPQzsb5pU7gIu1326FVZS3676RQDPEI7yp33dZjZyw9wjsCgv5Fz0/98690tqbbnjNbyX061o9Wkqd93ZhQZyALB1EC61l6Czrj5Q8fwSDo7764AM1AzETNARce84KL+koJUWwVMFgNtrATuAf0l4XSFrFPm1AWIIOVIP5HPMthgo8Vu71AEdU407guzKj+sAvsKzAT4nqUFQQbLiLaclcE7KSLZyFQEjZT1SqhXskjCahrotSeiBY/6fX1xB4qKh8PFaFzLB+NQ4NTgRHYKlsfU18b0GIWOwEFHhL+JPWqu9P3K2hpD7UQ5wW5ld/RUJdmNwk7k6obKycU4x4m9R+T0d1CsLCdNyNdrk0zyYX7ITsJIiPDH8hV2lduf7VqPDw1SFXfbFjnGz0dm3pfwem7OHpyjos36aWut/Q+r8dBSuSfup9XP2p4pZUlohc8AzvdxpfF8imajnmkJaQxrpu7WYTqvup/Db7kZV6Y2hwiUByR6sW1U6s+3dUmkqvkh3U8KuqdwgWfJcMOEBSI4HrgqgAHeW6mVDbQtYiYVbzRHjkYLHjsXco7x56pfhVYcMdIOwXSWcb1/Y+EobSV20Eobu1a3/z7mY5/qCwcRh0/GIjWe+vzVWoWFP2gqp9bBXXu1Z6+OIoZ0A9UQn0LxOwP17xYJ9ZTBDjBFqneSENHMpY+VxWs9H6K8gm8Zvr8duN0KL32EnX8Mx8W48ZsPKX5dzp7kuN9CZy68M4nw7AqUOgGeaFOXLSUQ+ggW839cip8JX6PQ7veYn9ZDnCbaasdQxjzjlrz0iESMgzgsdHBxWiPBwM6NQoFlmeAEg4JCO6cBwOJ9BYBSPAux5JiS0a5r8PDPG4azO++HgI564+idxCQhhZwDRKrJyuaebsBLe9Z7M+rYexhqCDFS03x26Q3UwMzv/PymoqZwqFe1a+ScnozrkgbbLdRs9SLMYARIVR8RJcNmDdR9SDWgpnh1oskJWNIRUE0+dUJ0tg8Gy6Q7VgRhpm6c6K+ZohH05Ryx9Y050sD7TkEnFJEP0fTxE6ku8qICeLspkmesvN6FGx99tb9jlRVS2SXEnxm/IGvZsZcZcKsZ6xDILa9+n5oeCJeJKnhL6+ipP/t1QBdO+2Orlxb8MyWtCROttAMGzr6Tfb2KxSl386g7kQLXHspAqbDFpQX2c+cNWhA=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="69e57c513fbf25fdf8bd4ad191330843"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=6111267d7aed2495')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">6111267d7aed2495</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:8d2b:d30c:bf6f:658f</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
