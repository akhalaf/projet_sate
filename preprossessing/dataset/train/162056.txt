<!DOCTYPE html>
<html>
<head>
<title>Assetto Corsa Database</title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<meta content="assetto corsa database" name="keywords"/>
<meta content="Assetto Corsa Database" name="description"/>
<meta content="header.language" name="Language"/>
<meta content="header.robots" name="robots"/>
<meta content="header.revisitAfter" name="revisit-after"/>
<meta content="header.publisher" name="publisher"/>
<meta content="header.copyright" name="copyright"/>
<meta content="header.audience" name="audience"/>
<meta content="header.contentLanguage" http-equiv="content-language"/>
<link href="/css/bootstrap.css?1438723025" media="screen" rel="stylesheet" type="text/css"/>
<link href="/css/star-rating.min.css?1449840866" media="screen" rel="stylesheet" type="text/css"/>
<link href="/css/style.css?1438723025" media="screen" rel="stylesheet" type="text/css"/>
<link href="/js/fancybox/jquery.fancybox.css?1438723065" media="screen" rel="stylesheet" type="text/css"/>
<meta content="Assetto Corsa Database" property="og:title"/>
<meta content="http://assetto-db.com/img/logos/thumbnail.jpg" property="og:image"/>
<meta content="http://assetto-db.com" property="og:url"/>
<meta content="Assetto Corsa Database" property="og:description"/>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
<script src="/js/jquery-1.10.1.min.js?1438723064" type="text/javascript"></script>
<script src="/js/fancybox/jquery.fancybox.pack.js?1438723065" type="text/javascript"></script>
<script src="/js/jquery-ui.js?1438723064" type="text/javascript"></script>
<script src="/js/jquery.lazyload.min.js?1438723064" type="text/javascript"></script>
<script src="/js/bootstrap.min.js?1438723063" type="text/javascript"></script>
<script src="/js/star-rating.min.js?1439569514" type="text/javascript"></script>
<script src="/js/util.js?1438723064" type="text/javascript"></script>
<script src="/js/funciones.js?1438723063" type="text/javascript"></script>
<link href="http://assetto-db.com" rel="canonical"/>
<link href="/img/.ico" rel="Shortcut Icon" type="image/x-icon"/>
<link href="http://assetto-db.com/img/logos/thumbnail.jpg" rel="apple-touch-icon"/>
<link href="http://assetto-db.com/img/logos/thumbnail.jpg" rel="image_src"/>
<!-- GOOGLE ANALYTICS -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-5141214-13', 'auto');
  ga('send', 'pageview');

</script>
<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
<link href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" rel="stylesheet" type="text/css"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#252e39"
	    },
	    "button": {
	      "background": "#14a7d0"
	    }
	  },
	  "content": {
	    "message": "We use cookies to personalise content and ads and to analyse our traffic. We also share information about your use of our site with advertising and analytics partners who may combine it with other information that you've provided to them or that they've collected from your use of their services.",
	    "href": "/cookies"
	  }
	})});
	</script>
<!-- End Cookie Consent plugin -->
</head>
<body class="portada ">
<header>
<nav class="navbar navbar-default navbar-fixed-top">
<div class="container">
<div class="navbar-header">
<a class="navbar-brand" href="/">assetto-DB.com</a>
<button aria-controls="navbar" aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="navbar-collapse collapse" id="navbar">
<ul class="nav navbar-nav">
<li class="dropdown ">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="/cars" role="button">Cars <span class="caret"></span></a>
<ul class="dropdown-menu">
<li class=""><a href="/cars">Brand List</a></li>
<li class=""><a href="/cars/latest">Latest updated cars</a></li>
</ul>
</li>
<li class=""><a href="/tracks/latest">Tracks</a></li>
<li class=""><a href="/skins">Skins</a></li>
<li class="disabled "><a disabled="disabled" href="/#">Apps</a></li>
</ul>
<ul class="nav navbar-nav navbar-right">
<li class="active"><a href="/contact">There is a new mod? Tell us</a></li>
</ul>
</div><!--/.nav-collapse -->
</div>
</nav>
</header>
<div class="container">
<div class="row">
<div class="col-md-12">
<a name="errorMessages"></a>
<div class="panel panel-danger" id="secMessage">
<div class="panel-body bg-danger">
<p class="text-danger" id="errMessagesContent"></p>
<button aria-label="Close" class="close" onclick="return clearError();" type="button"><span aria-hidden="true">×</span></button>
</div>
</div>
<div class="panel panel-danger" id="wrnMessage">
<div class="panel-body bg-warning">
<p class="text-warning" id="warnMessagesContent"></p>
<button aria-label="Close" class="close" onclick="return clearWarning();" type="button"><span aria-hidden="true">×</span></button>
</div>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-12">
<ol class="breadcrumb">
<li><a href="/">Home</a></li>
</ol>
</div>
<div class="col-sm-12 col-md-12">
<div class="bs-callout bs-callout-default">
<p>Welcome to Assetto Corsa Database. This site tries to collect all existing Assetto Corsa cars, tracks and skins.</p>
<p>You can search whether your favorite cars are available and download them (may be private, so no direct download) or go to the original site.</p>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h2 class="panel-title">Latest updated cars</h2>
</div>
<div class="panel-body">
<div class="bs-callout bs-callout-primary">
<h4 class="" style="display:inline">Cars mods:</h4>
<span>If any car does not sound, please, use this fix to resolve the issue: <strong><a href="http://adf.ly/10667159/sound-fix-ac-14" target="_blank">sound fix for AC 1.4</a></strong></span>
</div>
<div class="row">
<div class="col-md-12"> <script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Assetto-db go-back -->
<ins class="adsbygoogle" data-ad-client="ca-pub-8043859693649760" data-ad-format="auto" data-ad-slot="4569841934" style="display:block"></ins>
<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
</div>
</div>
<div class="row">
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Ford Focus RS MK2 Time Attack</span></div>
<div class="carImage"><a href="/car/bfr_focus_rs_mk2_ta"><img class="img-responsive" src="/img/previews/bfr_focus_rs_mk2_ta/med/00_frozen_white_design.png"/></a></div>
<div class="carBrand well well-sm"><img src="/img/brands/Rm9yZA==.png" title="Ford"/> Ford									</div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Ford Focus RS MK2</span></div>
<div class="carImage"><a href="/car/bfr_focus_rs_mk2"><img class="img-responsive" src="/img/previews/bfr_focus_rs_mk2/med/01_frozen_white.png"/></a></div>
<div class="carBrand well well-sm"><img src="/img/brands/Rm9yZA==.png" title="Ford"/> Ford									</div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Ford Focus RS MK2 S1</span></div>
<div class="carImage"><a href="/car/bfr_focus_rs_mk2_s1"><img class="img-responsive" src="/img/previews/bfr_focus_rs_mk2_s1/med/00_blue_electric.png"/></a></div>
<div class="carBrand well well-sm"><img src="/img/brands/Rm9yZA==.png" title="Ford"/> Ford									</div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Ford Focus RS MK2 Junior CUP</span></div>
<div class="carImage"><a href="/car/bfr_focus_rs_mk2_cup"><img class="img-responsive" src="/img/previews/bfr_focus_rs_mk2_cup/med/21_kfc.png"/></a></div>
<div class="carBrand well well-sm"><img src="/img/brands/Rm9yZA==.png" title="Ford"/> Ford									</div>
</div>
</div><div class="row"> <div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Ford Focus RS MK2 Super CUP</span></div>
<div class="carImage"><a href="/car/bfr_focus_rs_mk2_super_cup"><img class="img-responsive" src="/img/previews/bfr_focus_rs_mk2_super_cup/med/42_alitalia.png"/></a></div>
<div class="carBrand well well-sm"><img src="/img/brands/Rm9yZA==.png" title="Ford"/> Ford									</div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Ford Focus RS MK2 Time Attack Evolution</span></div>
<div class="carImage"><a href="/car/bfr_focus_rs_mk2_ta_evo"><img class="img-responsive" src="/img/previews/bfr_focus_rs_mk2_ta_evo/med/12_repsol.png"/></a></div>
<div class="carBrand well well-sm"><img src="/img/brands/Rm9yZA==.png" title="Ford"/> Ford									</div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Seat Leon 2003 Cupra R Gr.N</span></div>
<div class="carImage"><a href="/car/seat_leon_cupra_r_grn"><img class="img-responsive" src="/img/previews/seat_leon_cupra_r_grn/med/asturias.png"/></a></div>
<div class="carBrand well well-sm"><img src="/img/brands/U2VhdA==.png" title="Seat"/> Seat					<div class="pull-right"><input autocomplete="off" class="rating-review" data-rtl="true" data-show-caption="false" data-show-clear="false" data-size="xs" id="editor-review" max="5" min="0" readonly="" step="0.5" type="number" value="4"/></div> </div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Seat Leon 2003 Cupra R 225CV</span></div>
<div class="carImage"><a href="/car/seat_leon_cupra_r"><img class="img-responsive" src="/img/previews/seat_leon_cupra_r/med/rally.png"/></a></div>
<div class="carBrand well well-sm"><img src="/img/brands/U2VhdA==.png" title="Seat"/> Seat					<div class="pull-right"><input autocomplete="off" class="rating-review" data-rtl="true" data-show-caption="false" data-show-clear="false" data-size="xs" id="editor-review" max="5" min="0" readonly="" step="0.5" type="number" value="4.5"/></div> </div>
</div>
</div><div class="row"> <div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Seat Leon 2003 1.9 TDI 110CV</span></div>
<div class="carImage"><a href="/car/seat_leon_tdi"><img class="img-responsive" src="/img/previews/seat_leon_tdi/med/light_grey.png"/></a></div>
<div class="carBrand well well-sm"><img src="/img/brands/U2VhdA==.png" title="Seat"/> Seat					<div class="pull-right"><input autocomplete="off" class="rating-review" data-rtl="true" data-show-caption="false" data-show-clear="false" data-size="xs" id="editor-review" max="5" min="0" readonly="" step="0.5" type="number" value="3.5"/></div> </div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>RENAULT 5 ALPINE COUPE</span></div>
<div class="carImage"><a href="/car/j3d_renault_5_alpine"><img class="img-responsive" src="/img/previews/j3d_renault_5_alpine/med/10_elf_champion.png"/></a></div>
<div class="carBrand well well-sm"><img src="/img/brands/UmVuYXVsdA==.png" title="Renault"/> Renault									</div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Opel Kadett</span></div>
<div class="carImage"><a href="/car/opel_kadett"><img class="img-responsive" src="/img/previews/opel_kadett/med/polarweiss.png"/></a></div>
<div class="carBrand well well-sm"><img src="/img/brands/T3BlbA==.png" title="Opel"/> Opel					<div class="pull-right"><input autocomplete="off" class="rating-review" data-rtl="true" data-show-caption="false" data-show-clear="false" data-size="xs" id="editor-review" max="5" min="0" readonly="" step="0.5" type="number" value="4.5"/></div> </div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Opel Kadett S2</span></div>
<div class="carImage"><a href="/car/opel_kadett_s2"><img class="img-responsive" src="/img/previews/opel_kadett_s2/med/2014-2015.png"/></a></div>
<div class="carBrand well well-sm"><img src="/img/brands/T3BlbA==.png" title="Opel"/> Opel					<div class="pull-right"><input autocomplete="off" class="rating-review" data-rtl="true" data-show-caption="false" data-show-clear="false" data-size="xs" id="editor-review" max="5" min="0" readonly="" step="0.5" type="number" value="4.5"/></div> </div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h2 class="panel-title">Latest updated tracks</h2>
</div>
<div class="panel-body">
<div class="row">
<div class="col-md-12"> <script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Assetto-db go-back -->
<ins class="adsbygoogle" data-ad-client="ca-pub-8043859693649760" data-ad-format="auto" data-ad-slot="4569841934" style="display:block"></ins>
<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
</div>
</div>
<div class="row">
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Mt. Akina Uphill</span></div>
<div class="carImage"><a href="/track/mount_akina_2017/layout_uphill"><img class="img-responsive" src="/img/tracks/mount_akina_2017_layout_uphill/med/outline.png" style="background-size: cover; background-image: url('/img/tracks/mount_akina_2017_layout_uphill/med/preview.png')"/></a></div>
<div class="carBrand well well-sm">Japan									</div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Mt. Akina Drift</span></div>
<div class="carImage"><a href="/track/mount_akina_2017/layout_drift"><img class="img-responsive" src="/img/tracks/mount_akina_2017_layout_drift/med/outline.png" style="background-size: cover; background-image: url('/img/tracks/mount_akina_2017_layout_drift/med/preview.png')"/></a></div>
<div class="carBrand well well-sm">Japan									</div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Mt. Akina Downhill</span></div>
<div class="carImage"><a href="/track/mount_akina_2017/layout_downhill"><img class="img-responsive" src="/img/tracks/mount_akina_2017_layout_downhill/med/outline.png" style="background-size: cover; background-image: url('/img/tracks/mount_akina_2017_layout_downhill/med/preview.png')"/></a></div>
<div class="carBrand well well-sm">Japan									</div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>rouen-les-essarts-2016-rtb-extralong</span></div>
<div class="carImage"><a href="/track/rouen-les-essarts-2016-rtb-extralong/rouen-les-essarts-2016-rtb-extralong"><img class="img-responsive" src="/img/tracks/rouen-les-essarts-2016-rtb-extralong_rouen-les-essarts-2016-rtb-extralong/med/outline.png" style="background-size: cover; background-image: url('/img/tracks/rouen-les-essarts-2016-rtb-extralong_rouen-les-essarts-2016-rtb-extralong/med/preview.png')"/></a></div>
<div class="carBrand well well-sm">France									</div>
</div>
</div><div class="row"> <div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>rouen-les-essarts-2016-rtb-court</span></div>
<div class="carImage"><a href="/track/rouen-les-essarts-2016-rtb-court/rouen-les-essarts-2016-rtb-court"><img class="img-responsive" src="/img/tracks/rouen-les-essarts-2016-rtb-court_rouen-les-essarts-2016-rtb-court/med/outline.png" style="background-size: cover; background-image: url('/img/tracks/rouen-les-essarts-2016-rtb-court_rouen-les-essarts-2016-rtb-court/med/preview.png')"/></a></div>
<div class="carBrand well well-sm">France									</div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>rouen-les-essarts-2016-rtb-long</span></div>
<div class="carImage"><a href="/track/rouen-les-essarts-2016-rtb-long/rouen-les-essarts-2016-rtb-long"><img class="img-responsive" src="/img/tracks/rouen-les-essarts-2016-rtb-long_rouen-les-essarts-2016-rtb-long/med/outline.png" style="background-size: cover; background-image: url('/img/tracks/rouen-les-essarts-2016-rtb-long_rouen-les-essarts-2016-rtb-long/med/preview.png')"/></a></div>
<div class="carBrand well well-sm">France									</div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Macau Guia Circuit</span></div>
<div class="carImage"><a href="/track/macau/macau"><img class="img-responsive" src="/img/tracks/macau_macau/med/outline.png" style="background-size: cover; background-image: url('/img/tracks/macau_macau/med/preview.png')"/></a></div>
<div class="carBrand well well-sm">Hongkong									</div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Singapore Street Circuit Night ACF1S</span></div>
<div class="carImage"><a href="/track/singapore_street_circuit_night/singapore_street_circuit_night"><img class="img-responsive" src="/img/tracks/singapore_street_circuit_night_singapore_street_circuit_night/med/outline.png" style="background-size: cover; background-image: url('/img/tracks/singapore_street_circuit_night_singapore_street_circuit_night/med/preview.png')"/></a></div>
<div class="carBrand well well-sm">Singapore									</div>
</div>
</div><div class="row"> <div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Long Beach Formula Drift</span></div>
<div class="carImage"><a href="/track/long_beach_2012_fd/long_beach_2012_fd"><img class="img-responsive" src="/img/tracks/long_beach_2012_fd_long_beach_2012_fd/med/outline.png" style="background-size: cover; background-image: url('/img/tracks/long_beach_2012_fd_long_beach_2012_fd/med/preview.png')"/></a></div>
<div class="carBrand well well-sm">USA									</div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Long Beach 2012</span></div>
<div class="carImage"><a href="/track/long_beach_2012/long_beach_2012"><img class="img-responsive" src="/img/tracks/long_beach_2012_long_beach_2012/med/outline.png" style="background-size: cover; background-image: url('/img/tracks/long_beach_2012_long_beach_2012/med/preview.png')"/></a></div>
<div class="carBrand well well-sm">California									</div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Tougeattack Akina Downhill</span></div>
<div class="carImage"><a href="/track/tougeattack_akinadownhill/tougeattack_akinadownhill"><img class="img-responsive" src="/img/tracks/tougeattack_akinadownhill_tougeattack_akinadownhill/med/outline.png" style="background-size: cover; background-image: url('/img/tracks/tougeattack_akinadownhill_tougeattack_akinadownhill/med/preview.png')"/></a></div>
<div class="carBrand well well-sm">Japan									</div>
</div>
<div class="col-md-3 carBlock">
<div class="carName well well-sm"><span>Targa Florio</span></div>
<div class="carImage"><a href="/track/targa-florio/targa-florio"><img class="img-responsive" src="/img/tracks/targa-florio_targa-florio/med/outline.png" style="background-size: cover; background-image: url('/img/tracks/targa-florio_targa-florio/med/preview.png')"/></a></div>
<div class="carBrand well well-sm">Italy									</div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h2 class="panel-title">Latest updated skins</h2>
</div>
<div class="panel-body">
<div class="row" style="padding-bottom: 15px;">
<div class="col-md-12"> <script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Assetto-db go-back -->
<ins class="adsbygoogle" data-ad-client="ca-pub-8043859693649760" data-ad-format="auto" data-ad-slot="4569841934" style="display:block"></ins>
<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
</div>
</div>
<div class="row">
<div class="col-md-3">
<div class="thumbnail">
<a class="fancybox" href="/img/previews/sareni_camaro_gt3/blancpain.png" rel="skins" title="Blancpain">
<img class="img-responsive img-rounded" src="/img/previews/sareni_camaro_gt3/peq/blancpain.png"/>
</a>
<div class="caption">
<h4 class="text-center" style="margin:0;">Blancpain</h4>
</div>
<a class="btn btn-primary btn-xs btn-block" href="http://adf.ly/10667159/www.racedepartment.com%2Fdownloads%2Fcamaro-gt3-blancpain.11998%2F" rel="nofollow" target="_blank">Download skin</a>
<a class="btn btn-default btn-xs btn-block" href="/car/sareni_camaro_gt3">View car</a>
</div>
</div>
<div class="col-md-3">
<div class="thumbnail">
<a class="fancybox" href="/img/previews/sareni_camaro_gt3/gulf.png" rel="skins" title="Gulf">
<img class="img-responsive img-rounded" src="/img/previews/sareni_camaro_gt3/peq/gulf.png"/>
</a>
<div class="caption">
<h4 class="text-center" style="margin:0;">Gulf</h4>
</div>
<a class="btn btn-primary btn-xs btn-block" href="http://adf.ly/10667159/www.racedepartment.com%2Fdownloads%2Fcamaro-gt3-gulf-racing.12081%2F" rel="nofollow" target="_blank">Download skin</a>
<a class="btn btn-default btn-xs btn-block" href="/car/sareni_camaro_gt3">View car</a>
</div>
</div>
<div class="col-md-3">
<div class="thumbnail">
<a class="fancybox" href="/img/previews/sareni_camaro_gt3/redbull.png" rel="skins" title="Redbull">
<img class="img-responsive img-rounded" src="/img/previews/sareni_camaro_gt3/peq/redbull.png"/>
</a>
<div class="caption">
<h4 class="text-center" style="margin:0;">Redbull</h4>
</div>
<a class="btn btn-primary btn-xs btn-block" href="http://adf.ly/10667159/www.racedepartment.com%2Fdownloads%2Fcamaro-gt3-redbull.12094%2F" rel="nofollow" target="_blank">Download skin</a>
<a class="btn btn-default btn-xs btn-block" href="/car/sareni_camaro_gt3">View car</a>
</div>
</div>
<div class="col-md-3">
<div class="thumbnail">
<a class="fancybox" href="/img/previews/sareni_camaro_gt3/shell.png" rel="skins" title="Shell">
<img class="img-responsive img-rounded" src="/img/previews/sareni_camaro_gt3/peq/shell.png"/>
</a>
<div class="caption">
<h4 class="text-center" style="margin:0;">Shell</h4>
</div>
<a class="btn btn-primary btn-xs btn-block" href="http://adf.ly/10667159/www.racedepartment.com%2Fdownloads%2Fcamaro-gt3-shell.11989%2F" rel="nofollow" target="_blank">Download skin</a>
<a class="btn btn-default btn-xs btn-block" href="/car/sareni_camaro_gt3">View car</a>
</div>
</div>
</div><div class="row"> <div class="col-md-3">
<div class="thumbnail">
<a class="fancybox" href="/img/previews/ferrari_f40/ferrari.png" rel="skins" title="Ferrari">
<img class="img-responsive img-rounded" src="/img/previews/ferrari_f40/peq/ferrari.png"/>
</a>
<div class="caption">
<h4 class="text-center" style="margin:0;">Ferrari</h4>
</div>
<a class="btn btn-primary btn-xs btn-block" href="http://adf.ly/10667159/mega.nz%2F%23%21xQpkxSRA%21Zm3WORqvZaur_8fd0xXqgyf4ABmMwEXdYdV-YN5iBUM" rel="nofollow" target="_blank">Download skin</a>
<a class="btn btn-default btn-xs btn-block" href="/car/ferrari_f40">View car</a>
</div>
</div>
<div class="col-md-3">
<div class="thumbnail">
<a class="fancybox" href="/img/previews/urd_px1_shiro/nissan_16_23.png" rel="skins" title="Nissan 16 23">
<img class="img-responsive img-rounded" src="/img/previews/urd_px1_shiro/peq/nissan_16_23.png"/>
</a>
<div class="caption">
<h4 class="text-center" style="margin:0;">Nissan 16 23</h4>
</div>
<a class="btn btn-primary btn-xs btn-block" href="http://adf.ly/10667159/www.racedepartment.com%2Fdownloads%2Fnissan-gt-r-lm-nismo-2016.11262%2F" rel="nofollow" target="_blank">Download skin</a>
<a class="btn btn-default btn-xs btn-block" href="/car/urd_px1_shiro">View car</a>
</div>
</div>
<div class="col-md-3">
<div class="thumbnail">
<a class="fancybox" href="/img/previews/urd_px1_shiro/nissan_15_21.png" rel="skins" title="Nissan 15 21">
<img class="img-responsive img-rounded" src="/img/previews/urd_px1_shiro/peq/nissan_15_21.png"/>
</a>
<div class="caption">
<h4 class="text-center" style="margin:0;">Nissan 15 21</h4>
</div>
<a class="btn btn-primary btn-xs btn-block" href="http://adf.ly/10667159/www.racedepartment.com%2Fdownloads%2Fnissan-gt-r-lm-nismo-2015-21.11272%2F" rel="nofollow" target="_blank">Download skin</a>
<a class="btn btn-default btn-xs btn-block" href="/car/urd_px1_shiro">View car</a>
</div>
</div>
<div class="col-md-3">
<div class="thumbnail">
<a class="fancybox" href="/img/previews/urd_px1_darche/porsche_team_18_wec_15.png" rel="skins" title="Porsche Team 18 Wec 15">
<img class="img-responsive img-rounded" src="/img/previews/urd_px1_darche/peq/porsche_team_18_wec_15.png"/>
</a>
<div class="caption">
<h4 class="text-center" style="margin:0;">Porsche Team 18 Wec 15</h4>
</div>
<a class="btn btn-primary btn-xs btn-block" href="http://adf.ly/10667159/www.racedepartment.com%2Fdownloads%2Fporsche-team-wec-2015.9921%2F" rel="nofollow" target="_blank">Download skin</a>
<a class="btn btn-default btn-xs btn-block" href="/car/urd_px1_darche">View car</a>
</div>
</div>
</div><div class="row"> <div class="col-md-3">
<div class="thumbnail">
<a class="fancybox" href="/img/previews/urd_px1_darche/porsche_team_17_wec_15.png" rel="skins" title="Porsche Team 17 Wec 15">
<img class="img-responsive img-rounded" src="/img/previews/urd_px1_darche/peq/porsche_team_17_wec_15.png"/>
</a>
<div class="caption">
<h4 class="text-center" style="margin:0;">Porsche Team 17 Wec 15</h4>
</div>
<a class="btn btn-primary btn-xs btn-block" href="http://adf.ly/10667159/www.racedepartment.com%2Fdownloads%2Fporsche-team-wec-2015.9921%2F" rel="nofollow" target="_blank">Download skin</a>
<a class="btn btn-default btn-xs btn-block" href="/car/urd_px1_darche">View car</a>
</div>
</div>
<div class="col-md-3">
<div class="thumbnail">
<a class="fancybox" href="/img/previews/urd_px1_darche/porsche_team_17_lms_15.png" rel="skins" title="Porsche Team 17 Lms 15">
<img class="img-responsive img-rounded" src="/img/previews/urd_px1_darche/peq/porsche_team_17_lms_15.png"/>
</a>
<div class="caption">
<h4 class="text-center" style="margin:0;">Porsche Team 17 Lms 15</h4>
</div>
<a class="btn btn-primary btn-xs btn-block" href="http://adf.ly/10667159/www.racedepartment.com%2Fdownloads%2Fporsche-team-le-mans-2015.9714%2F" rel="nofollow" target="_blank">Download skin</a>
<a class="btn btn-default btn-xs btn-block" href="/car/urd_px1_darche">View car</a>
</div>
</div>
<div class="col-md-3">
<div class="thumbnail">
<a class="fancybox" href="/img/previews/urd_px1_darche/porsche_team_19_lms_15.png" rel="skins" title="Porsche Team 19 Lms 15">
<img class="img-responsive img-rounded" src="/img/previews/urd_px1_darche/peq/porsche_team_19_lms_15.png"/>
</a>
<div class="caption">
<h4 class="text-center" style="margin:0;">Porsche Team 19 Lms 15</h4>
</div>
<a class="btn btn-primary btn-xs btn-block" href="http://adf.ly/10667159/www.racedepartment.com%2Fdownloads%2Fporsche-team-le-mans-2015.9714%2F" rel="nofollow" target="_blank">Download skin</a>
<a class="btn btn-default btn-xs btn-block" href="/car/urd_px1_darche">View car</a>
</div>
</div>
<div class="col-md-3">
<div class="thumbnail">
<a class="fancybox" href="/img/previews/urd_px1_darche/porsche_team_18_lms_15.png" rel="skins" title="Porsche Team 18 Lms 15">
<img class="img-responsive img-rounded" src="/img/previews/urd_px1_darche/peq/porsche_team_18_lms_15.png"/>
</a>
<div class="caption">
<h4 class="text-center" style="margin:0;">Porsche Team 18 Lms 15</h4>
</div>
<a class="btn btn-primary btn-xs btn-block" href="http://adf.ly/10667159/www.racedepartment.com%2Fdownloads%2Fporsche-team-le-mans-2015.9714%2F" rel="nofollow" target="_blank">Download skin</a>
<a class="btn btn-default btn-xs btn-block" href="/car/urd_px1_darche">View car</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<hr class="borde0"/>
<div class="container">
<p class="pull-left">assetto-db.com - Assetto Corsa Database</p>
<p class="pull-right"><a href="/cookies">Cookies policy</a></p>
</div>
<script type="text/javascript">
		setError("");
		setWarning("");
		$(function() {
		
			/* var isDevice = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? true : false;
	
			if(isDevice)
	   		{
		        $("img.lazy").each(function(){
		        	$(this).attr("src", $(this).attr("data-original"));
					$(this).removeAttr("data-original");
				});
		
		    }else{ 
			
			    $("img.lazy").lazyload();
			}*/
			
			$(".fancybox").fancybox({
		    	openEffect	: 'elastic',
		    	closeEffect	: 'elastic',
		    	nextEffect : 'none',
		    	prevEffect : 'none',
		    	type : 'image'
			});

			$("input.rating-review").rating({
				starCaptions: {
				    0.5: 'Don&#039;t try it',
				    1: 'Not worth a try',
				    1.5: 'Try if you love it',
				    2: 'Not so bad',
				    2.5: 'Fair',
				    3: 'Nicely done',
				    3.5: 'Good one',
				    4: 'Really good',
				    4.5: 'You should have it',
				    5: 'Must have'
				}
			});
		});
	</script>
</body>
</html>