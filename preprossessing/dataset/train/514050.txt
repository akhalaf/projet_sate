<!DOCTYPE html>
<html class="error-page" dir="ltr" lang="it-it" xml:lang="it-it" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<base href="https://www.eurosirel.it/it/modules/mod_articles_latest_extended/tmpl/netflix/envio.php"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>404 - Errore: 404</title>
<link href="/images/icon-eurosirel.png" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/templates/shaper_yoga/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/templates/shaper_yoga/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/templates/shaper_yoga/css/template.css" rel="stylesheet" type="text/css"/>
<link href="/plugins/system/gdpr/assets/css/cookieconsent.min.css" rel="stylesheet" type="text/css"/>
<link href="/media/widgetkit/wk-styles-3069f278.css" id="wk-styles-css" rel="stylesheet" type="text/css"/>
<style type="text/css">
div.cc-window.cc-floating{max-width:24em}@media(max-width: 639px){div.cc-window.cc-floating:not(.cc-center){max-width: none}}div.cc-window, span.cc-cookie-settings-toggler{font-size:16px}div.cc-revoke{font-size:16px}div.cc-settings-label,span.cc-cookie-settings-toggle{font-size:14px}div.cc-window.cc-banner{padding:1em 1.8em}div.cc-window.cc-floating{padding:2em 1.8em}input.cc-cookie-checkbox+span:before, input.cc-cookie-checkbox+span:after{border-radius:1px}
	</style>
<script class="joomla-script-options new" type="application/json">{"csrf.token":"5956b7ce4e836b5b38df05678307192e","system.paths":{"root":"","base":""}}</script>
<script src="/media/jui/js/jquery.min.js?575a49052d8d5ac8976bd331ed9c23d4" type="text/javascript"></script>
<script src="/media/jui/js/jquery-noconflict.js?575a49052d8d5ac8976bd331ed9c23d4" type="text/javascript"></script>
<script src="/media/jui/js/jquery-migrate.min.js?575a49052d8d5ac8976bd331ed9c23d4" type="text/javascript"></script>
<script src="/media/system/js/core.js?575a49052d8d5ac8976bd331ed9c23d4" type="text/javascript"></script>
<script defer="defer" src="/plugins/system/gdpr/assets/js/cookieconsent.min.js" type="text/javascript"></script>
<script defer="defer" src="/plugins/system/gdpr/assets/js/init.js" type="text/javascript"></script>
<script src="/media/widgetkit/uikit2-47f56ea8.js" type="text/javascript"></script>
<script src="/media/widgetkit/wk-scripts-35fd5d3b.js" type="text/javascript"></script>
<script type="text/javascript">
;(function ($) {
	$.ajaxSetup({
		headers: {
			'X-CSRF-Token': Joomla.getOptions('csrf.token')
		}
	});
})(jQuery);var gdprConfigurationOptions = { complianceType: 'info',
																			  disableFirstReload: 0,
																	  		  blockJoomlaSessionCookie: 0,
																			  blockExternalCookiesDomains: 0,
																			  externalAdvancedBlockingModeCustomAttribute: '',
																			  allowedCookies: '',
																			  blockCookieDefine: 0,
																			  autoAcceptOnNextPage: 0,
																			  revokable: 0,
																			  lawByCountry: 0,
																			  checkboxLawByCountry: 0,
																			  cacheGeolocationCountry: 1,
																			  countryAcceptReloadTimeout: 1000,
																			  usaCCPARegions: null,
																			  dismissOnScroll: 0,
																			  dismissOnTimeout: 0,
																			  containerSelector: 'body',
																			  hideOnMobileDevices: 0,
																			  autoFloatingOnMobile: 0,
																			  autoFloatingOnMobileThreshold: 1024,
																			  autoRedirectOnDecline: 0,
																			  autoRedirectOnDeclineLink: '',
																			  showReloadMsg: 0,
																			  showReloadMsgText: 'Applying preferences and reloading the page...',
																			  defaultClosedToolbar: 0,
																			  toolbarLayout: 'basic',
																			  toolbarTheme: 'block',
																			  toolbarButtonsTheme: 'decline_first',
																			  revocableToolbarTheme: 'basic',
																			  toolbarPosition: 'bottom',
																			  toolbarCenterTheme: 'compact',
																			  revokePosition: 'revoke-top',
																			  toolbarPositionmentType: 1,
																			  popupEffect: 'fade',
																			  popupBackground: '#000000',
																			  popupText: '#ffffff',
																			  popupLink: '#ffffff',
																			  buttonBackground: '#ffffff',
																			  buttonBorder: '#ffffff',
																			  buttonText: '#000000',
																			  highlightOpacity: '100',
																			  highlightBackground: '#333333',
																			  highlightBorder: '#ffffff',
																			  highlightText: '#ffffff',
																			  highlightDismissBackground: '#333333',
																		  	  highlightDismissBorder: '#ffffff',
																		 	  highlightDismissText: '#ffffff',
																			  hideRevokableButton: 0,
																			  hideRevokableButtonOnscroll: 0,
																			  customRevokableButton: 0,
																			  headerText: 'Cookies used on the website!',
																			  messageText: 'Questo sito utilizza cookie tecnici e di terze parti per migliorare l\'esperienza sul sito.',
																			  denyMessageEnabled: 0, 
																			  denyMessage: 'You have declined cookies, to ensure the best experience on this website please consent the cookie usage.',
																			  placeholderBlockedResources: 0, 
																			  placeholderBlockedResourcesAction: '',
																	  		  placeholderBlockedResourcesText: 'You must accept cookies and reload the page to view this content',
																			  placeholderIndividualBlockedResourcesText: 'You must accept cookies from {domain} and reload the page to view this content',
																			  dismissText: 'Ho letto',
																			  allowText: 'Accetto',
																			  denyText: 'Declino',
																			  cookiePolicyLinkText: 'Cookie policy',
																			  cookiePolicyLink: '/cookie-policy',
																			  cookiePolicyRevocableTabText: 'Cookie policy',
																			  privacyPolicyLinkText: 'Informativa Privacy',
																			  privacyPolicyLink: '/informativa-privacy-e-cookie-policy',
																			  categoriesCheckboxTemplate: 'cc-checkboxes-light',
																			  toggleCookieSettings: 0,
																	  		  toggleCookieSettingsText: '<span class="cc-cookie-settings-toggle">Settings <span class="cc-cookie-settings-toggler">&#x25EE;</span></span>',
																			  toggleCookieSettingsButtonBackground: '#333333',
																			  toggleCookieSettingsButtonBorder: '#FFFFFF',
																			  toggleCookieSettingsButtonText: '#FFFFFF',
																			  showLinks: 1,
																			  blankLinks: '_blank',
																			  autoOpenPrivacyPolicy: 0,
																			  openAlwaysDeclined: 0,
																			  cookieSettingsLabel: 'Cookie settings:',
															  				  cookieSettingsDesc: 'Choose which kind of cookies you want to disable by clicking on the checkboxes. Click on a category name for more informations about used cookies.',
																			  cookieCategory1Enable: 0,
																			  cookieCategory1Name: 'Necessary',
																			  cookieCategory1Locked: 0,
																			  cookieCategory2Enable: 0,
																			  cookieCategory2Name: 'Preferences',
																			  cookieCategory2Locked: 0,
																			  cookieCategory3Enable: 0,
																			  cookieCategory3Name: 'Statistics',
																			  cookieCategory3Locked: 0,
																			  cookieCategory4Enable: 0,
																			  cookieCategory4Name: 'Marketing',
																			  cookieCategory4Locked: 0,
																			  cookieCategoriesDescriptions: {},
																			  alwaysReloadAfterCategoriesChange: 0,
																			  preserveLockedCategories: 0,
																			  reloadOnfirstDeclineall: 0,
																			  trackExistingCheckboxSelectors: '',
															  		  		  trackExistingCheckboxConsentLogsFormfields: 'name,email,subject,message',
																			  allowallShowbutton: 0,
																			  allowallText: 'Allow all cookies',
																			  allowallButtonBackground: '#FFFFFF',
																			  allowallButtonBorder: '#FFFFFF',
																			  allowallButtonText: '#000000',
																			  includeAcceptButton: 0,
																			  optoutIndividualResources: 0,
																			  externalAdvancedBlockingModeTags: 'iframe,script,img,source,link',
																			  debugMode: 0
																		};var gdpr_ajax_livesite='https://www.eurosirel.it/';var gdpr_enable_log_cookie_consent=0;
	</script>
</head>
<body>
<div class="error-page-inner has-background" style="background-image: url(https://www.eurosirel.it/images/cta-bg.jpg);">
<div>
<div class="container">
<div class="error-logo-wrap">
<img alt="logo" class="error-logo" src="https://www.eurosirel.it//images/demo/yoga-404.png"/>
</div>
<h1 class="error-code">404</h1>
<p class="error-message">Page not found</p>
<a class="btn btn-primary btn-lg" href="/" title="HOME">Go Back Home</a>
</div>
</div>
</div>
</body>
</html>