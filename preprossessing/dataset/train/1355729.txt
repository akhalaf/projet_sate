<!DOCTYPE html>
<html lang="pl-PL" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="https://specjalservice.pl/xmlrpc.php" rel="pingback"/>
<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>
<script>var et_site_url='https://specjalservice.pl';var et_post_id='0';function et_core_page_resource_fallback(a,b){"undefined"===typeof b&&(b=a.sheet.cssRules&&0===a.sheet.cssRules.length);b&&(a.onerror=null,a.onload=null,a.href?a.href=et_site_url+"/?et_core_page_resource="+a.id+et_post_id:a.src&&(a.src=et_site_url+"/?et_core_page_resource="+a.id+et_post_id))}
</script><title>Strony nie znaleziono - Specjal Service</title>
<!-- This site is optimized with the Yoast SEO plugin v9.6 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex,follow" name="robots"/>
<meta content="pl_PL" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Strony nie znaleziono - Specjal Service" property="og:title"/>
<meta content="Specjal Service" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Strony nie znaleziono - Specjal Service" name="twitter:title"/>
<!-- / Yoast SEO plugin. -->
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://specjalservice.pl/feed/" rel="alternate" title="Specjal Service » Kanał z wpisami" type="application/rss+xml"/>
<link href="https://specjalservice.pl/comments/feed/" rel="alternate" title="Specjal Service » Kanał z komentarzami" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/specjalservice.pl\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.0.11"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<meta content="Divi-child v." name="generator"/><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://specjalservice.pl/wp-includes/css/dist/block-library/style.min.css?ver=5.0.11" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://specjalservice.pl/wp-content/themes/Divi/style.css?ver=5.0.11" id="parent-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://specjalservice.pl/wp-content/themes/Divi-child/fontawesome/css/all.css?ver=5.0.11" id="fontawesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,latin-ext" id="divi-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://specjalservice.pl/wp-content/themes/Divi-child/style.css?ver=3.19.4" id="divi-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://specjalservice.pl/wp-includes/css/dashicons.min.css?ver=5.0.11" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://specjalservice.pl/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://specjalservice.pl/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://specjalservice.pl/wp-json/" rel="https://api.w.org/"/>
<link href="https://specjalservice.pl/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://specjalservice.pl/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.0.11" name="generator"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/><link href="" rel="shortcut icon"/><link href="https://specjalservice.pl/wp-content/cache/et/global/et-divi-customizer-global-16103863315152.min.css" id="et-divi-customizer-global-cached-inline-styles" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" rel="stylesheet"/></head>
<body class="error404 et_pb_button_helper_class et_fixed_nav et_show_nav et_hide_fixed_logo et_cover_background et_pb_gutter et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns_1_5_3_5_1_5 et_header_style_left et_right_sidebar et_divi_theme et-db et_minified_js et_minified_css">
<div id="page-container">
<header data-height-onload="85" id="main-header">
<div class="container clearfix et_menu_container">
<div class="logo_container">
<span class="logo_helper"></span>
<a href="https://specjalservice.pl/">
<img alt="Specjal Service" data-height-percentage="84" id="logo" src="https://specjalservice.pl/wp-content/uploads/2019/02/logo-2.png"/>
</a>
</div>
<div data-fixed-height="38" data-height="85" id="et-top-navigation">
<nav id="top-menu-nav">
<ul class="nav" id="top-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-155" id="menu-item-155"><a href="https://specjalservice.pl/">Strona Główna</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-154" id="menu-item-154"><a href="https://specjalservice.pl/zakres-uslug/">Zakres usług</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-214" id="menu-item-214"><a href="https://specjalservice.pl/wyposazenie/">Sprzęt</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-151" id="menu-item-151"><a href="https://specjalservice.pl/galeria/">Galeria</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-153" id="menu-item-153"><a href="https://specjalservice.pl/ubezpieczenia/">Ubezpieczenia</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-152" id="menu-item-152"><a href="https://specjalservice.pl/kontakt/">Kontakt</a></li>
</ul> </nav>
<div id="et_mobile_nav_menu">
<div class="mobile_nav closed">
<span class="select_page">Zaznacz stronę</span>
<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
</div>
</div> </div> <!-- #et-top-navigation -->
</div> <!-- .container -->
<div class="et_search_outer">
<div class="container et_search_form_container">
<form action="https://specjalservice.pl/" class="et-search-form" method="get" role="search">
<input class="et-search-field" name="s" placeholder="Wyszukiwanie …" title="Szukaj:" type="search" value=""/> </form>
<span class="et_close_search_field"></span>
</div>
</div>
</header> <!-- #main-header -->
<div id="et-main-area">
<div id="main-content">
<div class="container">
<div class="clearfix" id="content-area">
<div id="left-area">
<article class="et_pb_post not_found" id="post-0">
<div class="entry">
<!--If no results are found-->
<h1>Nie znaleziono żadnych wyników</h1>
<p>Nie znaleziono szukanej strony. Proszę spróbować innej definicji wyszukiwania lub zlokalizować wpis przy użyciu nawigacji powyżej.</p>
</div>
<!--End if no results are found--> </article> <!-- .et_pb_post -->
</div> <!-- #left-area -->
</div> <!-- #content-area -->
</div> <!-- .container -->
</div> <!-- #main-content -->
<footer id="main-footer">
<div id="footer-bottom">
<div class="container clearfix">
<div id="footer-info">COPYRIGHT SPECJAL SERVICE 2019</div> </div> <!-- .container -->
</div>
</footer> <!-- #main-footer -->
</div> <!-- #et-main-area -->
</div> <!-- #page-container -->
<script type="text/javascript">
</script>
<script type="text/javascript">
/* <![CDATA[ */
var DIVI = {"item_count":"%d Item","items_count":"%d Items"};
var et_shortcodes_strings = {"previous":"Poprzednie","next":"Nast\u0119pne"};
var et_pb_custom = {"ajaxurl":"https:\/\/specjalservice.pl\/wp-admin\/admin-ajax.php","images_uri":"https:\/\/specjalservice.pl\/wp-content\/themes\/Divi\/images","builder_images_uri":"https:\/\/specjalservice.pl\/wp-content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"5c3cfbecd7","subscription_failed":"Prosz\u0119, sprawd\u017a pola poni\u017cej, aby upewni\u0107 si\u0119, \u017ce wpisa\u0142e\u015b poprawn\u0105 informacj\u0119.","et_ab_log_nonce":"ccba3dd524","fill_message":"Prosz\u0119 wype\u0142ni\u0107 nast\u0119puj\u0105ce pola:","contact_error_message":"Napraw poni\u017csze b\u0142\u0119dy:","invalid":"Nieprawid\u0142owy adres e-mail","captcha":"Captcha","prev":"Przed","previous":"Poprzednie","next":"Dalej","wrong_captcha":"Wpisa\u0142e\u015b\/-a\u015b w captcha nieprawid\u0142owy numer.","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","is_ab_testing_active":"","page_id":"","unique_test_id":"","ab_bounce_rate":"","is_cache_plugin_active":"no","is_shortcode_tracking":"","tinymce_uri":""};
var et_pb_box_shadow_elements = [];
/* ]]> */
</script>
<script src="https://specjalservice.pl/wp-content/themes/Divi/js/custom.min.js?ver=3.19.4" type="text/javascript"></script>
<script src="https://specjalservice.pl/wp-content/themes/Divi/core/admin/js/common.js?ver=3.19.4" type="text/javascript"></script>
<script src="https://specjalservice.pl/wp-includes/js/wp-embed.min.js?ver=5.0.11" type="text/javascript"></script>
</body>
</html>
