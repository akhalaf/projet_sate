<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US">
<![endif]--><!--[if IE 7]>
<html id="ie7" lang="en-US">
<![endif]--><!--[if IE 8]>
<html id="ie8" lang="en-US">
<![endif]--><!--[if !(IE 6) & !(IE 7) & !(IE 8)]><!--><html lang="en-US">
<!--<![endif]-->
<head>
<meta charset="utf-8"/><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={licenseKey:"58647cf5d1",applicationID:"19417140"};window.NREUM||(NREUM={}),__nr_require=function(e,t,n){function r(n){if(!t[n]){var i=t[n]={exports:{}};e[n][0].call(i.exports,function(t){var i=e[n][1][t];return r(i||t)},i,i.exports)}return t[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var i=0;i<n.length;i++)r(n[i]);return r}({1:[function(e,t,n){function r(){}function i(e,t,n){return function(){return o(e,[u.now()].concat(c(arguments)),t?null:this,n),t?void 0:this}}var o=e("handle"),a=e(6),c=e(7),f=e("ee").get("tracer"),u=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var d=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],p="api-",l=p+"ixn-";a(d,function(e,t){s[t]=i(p+t,!0,"api")}),s.addPageAction=i(p+"addPageAction",!0),s.setCurrentRouteName=i(p+"routeName",!0),t.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,t){var n={},r=this,i="function"==typeof t;return o(l+"tracer",[u.now(),e,n],r),function(){if(f.emit((i?"":"no-")+"fn-start",[u.now(),r,i],n),i)try{return t.apply(this,arguments)}catch(e){throw f.emit("fn-err",[arguments,this,e],n),e}finally{f.emit("fn-end",[u.now()],n)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,t){m[t]=i(l+t)}),newrelic.noticeError=function(e,t){"string"==typeof e&&(e=new Error(e)),o("err",[e,u.now(),!1,t])}},{}],2:[function(e,t,n){function r(){return c.exists&&performance.now?Math.round(performance.now()):(o=Math.max((new Date).getTime(),o))-a}function i(){return o}var o=(new Date).getTime(),a=o,c=e(8);t.exports=r,t.exports.offset=a,t.exports.getLastTimestamp=i},{}],3:[function(e,t,n){function r(e,t){var n=e.getEntries();n.forEach(function(e){"first-paint"===e.name?d("timing",["fp",Math.floor(e.startTime)]):"first-contentful-paint"===e.name&&d("timing",["fcp",Math.floor(e.startTime)])})}function i(e,t){var n=e.getEntries();n.length>0&&d("lcp",[n[n.length-1]])}function o(e){e.getEntries().forEach(function(e){e.hadRecentInput||d("cls",[e])})}function a(e){if(e instanceof m&&!g){var t=Math.round(e.timeStamp),n={type:e.type};t<=p.now()?n.fid=p.now()-t:t>p.offset&&t<=Date.now()?(t-=p.offset,n.fid=p.now()-t):t=p.now(),g=!0,d("timing",["fi",t,n])}}function c(e){d("pageHide",[p.now(),e])}if(!("init"in NREUM&&"page_view_timing"in NREUM.init&&"enabled"in NREUM.init.page_view_timing&&NREUM.init.page_view_timing.enabled===!1)){var f,u,s,d=e("handle"),p=e("loader"),l=e(5),m=NREUM.o.EV;if("PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){f=new PerformanceObserver(r);try{f.observe({entryTypes:["paint"]})}catch(v){}u=new PerformanceObserver(i);try{u.observe({entryTypes:["largest-contentful-paint"]})}catch(v){}s=new PerformanceObserver(o);try{s.observe({type:"layout-shift",buffered:!0})}catch(v){}}if("addEventListener"in document){var g=!1,y=["click","keydown","mousedown","pointerdown","touchstart"];y.forEach(function(e){document.addEventListener(e,a,!1)})}l(c)}},{}],4:[function(e,t,n){function r(e,t){if(!i)return!1;if(e!==i)return!1;if(!t)return!0;if(!o)return!1;for(var n=o.split("."),r=t.split("."),a=0;a<r.length;a++)if(r[a]!==n[a])return!1;return!0}var i=null,o=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var c=navigator.userAgent,f=c.match(a);f&&c.indexOf("Chrome")===-1&&c.indexOf("Chromium")===-1&&(i="Safari",o=f[1])}t.exports={agent:i,version:o,match:r}},{}],5:[function(e,t,n){function r(e){function t(){e(a&&document[a]?document[a]:document[i]?"hidden":"visible")}"addEventListener"in document&&o&&document.addEventListener(o,t,!1)}t.exports=r;var i,o,a;"undefined"!=typeof document.hidden?(i="hidden",o="visibilitychange",a="visibilityState"):"undefined"!=typeof document.msHidden?(i="msHidden",o="msvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(i="webkitHidden",o="webkitvisibilitychange",a="webkitVisibilityState")},{}],6:[function(e,t,n){function r(e,t){var n=[],r="",o=0;for(r in e)i.call(e,r)&&(n[o]=t(r,e[r]),o+=1);return n}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],7:[function(e,t,n){function r(e,t,n){t||(t=0),"undefined"==typeof n&&(n=e?e.length:0);for(var r=-1,i=n-t||0,o=Array(i<0?0:i);++r<i;)o[r]=e[t+r];return o}t.exports=r},{}],8:[function(e,t,n){t.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,t,n){function r(){}function i(e){function t(e){return e&&e instanceof r?e:e?f(e,c,o):o()}function n(n,r,i,o){if(!p.aborted||o){e&&e(n,r,i);for(var a=t(i),c=v(n),f=c.length,u=0;u<f;u++)c[u].apply(a,r);var d=s[w[n]];return d&&d.push([b,n,r,a]),a}}function l(e,t){h[e]=v(e).concat(t)}function m(e,t){var n=h[e];if(n)for(var r=0;r<n.length;r++)n[r]===t&&n.splice(r,1)}function v(e){return h[e]||[]}function g(e){return d[e]=d[e]||i(n)}function y(e,t){u(e,function(e,n){t=t||"feature",w[n]=t,t in s||(s[t]=[])})}var h={},w={},b={on:l,addEventListener:l,removeEventListener:m,emit:n,get:g,listeners:v,context:t,buffer:y,abort:a,aborted:!1};return b}function o(){return new r}function a(){(s.api||s.feature)&&(p.aborted=!0,s=p.backlog={})}var c="nr@context",f=e("gos"),u=e(6),s={},d={},p=t.exports=i();p.backlog=s},{}],gos:[function(e,t,n){function r(e,t,n){if(i.call(e,t))return e[t];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:r,writable:!0,enumerable:!1}),r}catch(o){}return e[t]=r,r}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],handle:[function(e,t,n){function r(e,t,n,r){i.buffer([e],r),i.emit(e,t,n)}var i=e("ee").get("handle");t.exports=r,r.ee=i},{}],id:[function(e,t,n){function r(e){var t=typeof e;return!e||"object"!==t&&"function"!==t?-1:e===window?0:a(e,o,function(){return i++})}var i=1,o="nr@id",a=e("gos");t.exports=r},{}],loader:[function(e,t,n){function r(){if(!E++){var e=b.info=NREUM.info,t=p.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&t))return u.abort();f(h,function(t,n){e[t]||(e[t]=n)});var n=a();c("mark",["onload",n+b.offset],null,"api"),c("timing",["load",n]);var r=p.createElement("script");r.src="https://"+e.agent,t.parentNode.insertBefore(r,t)}}function i(){"complete"===p.readyState&&o()}function o(){c("mark",["domContent",a()+b.offset],null,"api")}var a=e(2),c=e("handle"),f=e(6),u=e("ee"),s=e(4),d=window,p=d.document,l="addEventListener",m="attachEvent",v=d.XMLHttpRequest,g=v&&v.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:v,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var y=""+location,h={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1184.min.js"},w=v&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),b=t.exports={offset:a.getLastTimestamp(),now:a,origin:y,features:{},xhrWrappable:w,userAgent:s};e(1),e(3),p[l]?(p[l]("DOMContentLoaded",o,!1),d[l]("load",r,!1)):(p[m]("onreadystatechange",i),d[m]("onload",r)),c("mark",["firstbyte",a.getLastTimestamp()],null,"api");var E=0},{}],"wrap-function":[function(e,t,n){function r(e){return!(e&&e instanceof Function&&e.apply&&!e[a])}var i=e("ee"),o=e(7),a="nr@original",c=Object.prototype.hasOwnProperty,f=!1;t.exports=function(e,t){function n(e,t,n,i){function nrWrapper(){var r,a,c,f;try{a=this,r=o(arguments),c="function"==typeof n?n(r,a):n||{}}catch(u){p([u,"",[r,a,i],c])}s(t+"start",[r,a,i],c);try{return f=e.apply(a,r)}catch(d){throw s(t+"err",[r,a,d],c),d}finally{s(t+"end",[r,a,f],c)}}return r(e)?e:(t||(t=""),nrWrapper[a]=e,d(e,nrWrapper),nrWrapper)}function u(e,t,i,o){i||(i="");var a,c,f,u="-"===i.charAt(0);for(f=0;f<t.length;f++)c=t[f],a=e[c],r(a)||(e[c]=n(a,u?c+i:i,o,c))}function s(n,r,i){if(!f||t){var o=f;f=!0;try{e.emit(n,r,i,t)}catch(a){p([a,n,r,i])}f=o}}function d(e,t){if(Object.defineProperty&&Object.keys)try{var n=Object.keys(e);return n.forEach(function(n){Object.defineProperty(t,n,{get:function(){return e[n]},set:function(t){return e[n]=t,t}})}),t}catch(r){p([r])}for(var i in e)c.call(e,i)&&(t[i]=e[i]);return t}function p(t){try{e.emit("internal-error",t)}catch(n){}}return e||(e=i),n.inPlace=u,n.flag=a,n}},{}]},{},["loader"]);</script>
<meta content="width=device-width" name="viewport"/>
<link href="https://www.altamontcapital.com/wp-content/themes/alt2016/favicon.ico" rel="shortcut icon"/>
<title>Altamont Capital Partners</title>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.altamontcapital.com/wp-content/themes/alt2016/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.altamontcapital.com/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
<script src="https://www.altamontcapital.com/wp-content/themes/alt2016/js/html5.js" type="text/javascript"></script>
<![endif]-->
<link href="//www.altamontcapital.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.altamontcapital.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7.3"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.altamontcapital.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.6.1" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.altamontcapital.com/wp-content/themes/alt2016/css/styles.css?ver=4.7.3" id="theme-main-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.altamontcapital.com/wp-content/themes/alt2016/css/vendor/owl.carousel.css?ver=4.7.3" id="owl-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.altamontcapital.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.altamontcapital.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.altamontcapital.com/wp-content/themes/alt2016/js/redirect.js?ver=4.7.3" type="text/javascript"></script>
<script src="https://www.altamontcapital.com/wp-content/themes/alt2016/js/vendor/webfontloader.js?ver=4.7.3" type="text/javascript"></script>
<script src="https://www.altamontcapital.com/wp-content/themes/alt2016/js/vendor/mediaCheck-min.js?ver=4.7.3" type="text/javascript"></script>
<script src="https://www.altamontcapital.com/wp-content/themes/alt2016/js/vendor/jquery.matchHeight-min.js?ver=4.7.3" type="text/javascript"></script>
<script src="https://www.altamontcapital.com/wp-content/themes/alt2016/js/base.js?ver=4.7.3" type="text/javascript"></script>
<script src="https://www.altamontcapital.com/wp-content/themes/alt2016/js/vendor/owl.carousel.min.js?ver=4.7.3" type="text/javascript"></script>
<script src="https://www.altamontcapital.com/wp-content/themes/alt2016/js/vendor/jquery.fittext.js?ver=4.7.3" type="text/javascript"></script>
<script src="https://www.altamontcapital.com/wp-content/themes/alt2016/js/home.js?ver=4.7.3" type="text/javascript"></script>
<script src="https://www.altamontcapital.com/wp-content/themes/alt2016/js/archive.js?ver=4.7.3" type="text/javascript"></script>
<script src="https://www.altamontcapital.com/wp-content/themes/alt2016/js/news.js?ver=4.7.3" type="text/javascript"></script>
<script src="https://www.altamontcapital.com/wp-content/themes/alt2016/js/approach.js?ver=4.7.3" type="text/javascript"></script>
<link href="https://www.altamontcapital.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.altamontcapital.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.altamontcapital.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<link href="https://www.altamontcapital.com/" rel="canonical"/>
<link href="https://www.altamontcapital.com/" rel="shortlink"/>
<link href="https://www.altamontcapital.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.altamontcapital.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://www.altamontcapital.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.altamontcapital.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
</head>
<body class="home page-template page-template-template-page-home page-template-template-page-home-php page page-id-31">
<div class="hfeed" id="page">
<header id="branding" role="banner">
<div class="container">
<div class="header-logo col-md-3">
<a href="https://www.altamontcapital.com/">
<img src="https://www.altamontcapital.com/wp-content/uploads/2016/04/logo.png"/>
</a>
<div class="main-nav-toggle">
<div class="icon icon-bars js-main-nav-toggle"></div>
</div>
</div>
<div class="main-nav-container col-md-9">
<nav class="main-nav js-main-nav">
<div class="menu-main-container"><ul class="menu" id="menu-main"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-322" id="menu-item-322"><a href="https://www.altamontcapital.com/approach/">Approach</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18" id="menu-item-18"><a href="https://www.altamontcapital.com/team/">Team</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1161" id="menu-item-1161"><a href="https://www.altamontcapital.com/advisors/">Advisors</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17" id="menu-item-17"><a href="https://www.altamontcapital.com/portfolio/">Portfolio</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16" id="menu-item-16"><a href="https://www.altamontcapital.com/news/">News</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-305" id="menu-item-305"><a href="https://www.altamontcapital.com/wp-content/uploads/2020/10/Altamont-Firm-Overview.-October2020pdf.pdf" target="_blank">Firm Overview</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15" id="menu-item-15"><a href="https://www.altamontcapital.com/contact/">Contact Us</a></li>
</ul></div> </nav>
</div>
</div>
</header><!-- #branding -->
<div id="main">
<div id="primary">
<div id="content" role="main">
<section class="home-slider">
<div class="js-home-slider">
<div class="home-slide js-home-slide">
<img class="js-image-switch" data-desktop-src="https://www.altamontcapital.com/wp-content/uploads/2016/01/team-1700x750.jpg" data-mobile-src="https://www.altamontcapital.com/wp-content/uploads/2016/01/team-768x935.jpg"/>
<div class="slide-text">
<h2 class="home-header">Committed, long-term partners.</h2>
</div>
</div>
<div class="home-slide js-home-slide">
<img class="js-image-switch" data-desktop-src="https://www.altamontcapital.com/wp-content/uploads/2016/01/slide3-e1457456287860-1700x750-1457465715.jpg" data-mobile-src="https://www.altamontcapital.com/wp-content/uploads/2016/01/slide3-768x935.jpg"/>
<div class="slide-text">
<h2 class="home-header">Flexible. Creative. Pragmatic.</h2>
</div>
</div>
<div class="home-slide js-home-slide">
<img class="js-image-switch" data-desktop-src="https://www.altamontcapital.com/wp-content/uploads/2016/01/Picture3_AltamontBW4518-1700x750.jpg" data-mobile-src="https://www.altamontcapital.com/wp-content/uploads/2016/01/Picture3_AltamontBW4518-768x935.jpg"/>
<div class="slide-text">
<h2 class="home-header">Capable. Experienced. Patient.</h2>
</div>
</div>
<div class="home-slide js-home-slide">
<img class="js-image-switch" data-desktop-src="https://www.altamontcapital.com/wp-content/uploads/2016/04/GROUP-9779_ret-1700x750.jpg" data-mobile-src="https://www.altamontcapital.com/wp-content/uploads/2016/04/GROUP-9779_ret-768x935.jpg"/>
<div class="slide-text">
<h2 class="home-header">Business Builders.</h2>
</div>
</div>
<div class="home-slide js-home-slide">
<img class="js-image-switch" data-desktop-src="https://www.altamontcapital.com/wp-content/uploads/2016/01/GROUP-9795_ret-1700x750-1461213506.jpg" data-mobile-src="https://www.altamontcapital.com/wp-content/uploads/2016/01/GROUP-9795_ret-768x935.jpg"/>
<div class="slide-text">
<h2 class="home-header">Differentiated Approach.</h2>
</div>
</div>
</div>
<div class="next-section js-next-section">
<i class="icon icon-angle-down"></i>
</div>
</section>
<section class="home-section home-section-green" id="section-1">
<div class="container">
<div class="section-upper-stroke"></div>
<div class="inner">
<h2 class="home-header">Altamont Capital Partners</h2>
<div class="section-paragraph">
<p>is a private equity firm with over $2.5 billion of capital under management, focused primarily on making long term, control investments in middle market businesses. We are often drawn to companies undergoing a transition, either operational or strategic, which we believe we can help navigate. Our approach relies on partnering with strong management, supporting the business with our considerable resources, and bringing a constructive and pragmatic mindset to drive significant long-term value.</p>
</div>
</div>
<div class="section-lower-stroke"></div>
</div>
<div class="next-section js-next-section">
<i class="icon icon-angle-down"></i>
</div>
</section>
<section class="home-section home-section-white" id="section-2">
<div class="container">
<div class="section-upper-stroke"></div>
<div class="inner">
<h2 class="home-header">We match the right talent to the opportunity.</h2>
<div class="section-paragraph">
<p>It’s core to our investment philosophy that as a firm and individually, we engage collaboratively with our companies and provide significant support as and when needed. For this reason, the Altamont team is filled with experienced people who are willing and able to roll up their sleeves and support management in the hard work often required to make an investment successful. In addition, we have world class expert resources in-house and in our network available to further support our companies.</p>
</div>
<div class="button-container">
<a class="button" href="/team/">
                More on our team              </a>
</div>
</div>
<div class="section-lower-stroke"></div>
</div>
<div class="next-section js-next-section">
<i class="icon icon-angle-down"></i>
</div>
</section>
<section class="home-section home-section-green" id="section-3">
<div class="container">
<div class="section-upper-stroke"></div>
<div class="inner">
<h2 class="home-header">Our approach is differentiated.</h2>
<div class="section-paragraph">
<p>Our approach emphasizes business-building and long term results. As a result, we believe our investment returns are generally driven more by operational and strategic improvements rather than by financial engineering or short-term ‘fix-its.’ During diligence, we work together with management to develop a plan to build the business, set the capital structure, and identify the resources required for success. As that plan is executed, we go all-in to support the success of the company, sharing our experience, providing hands-on assistance, and accessing the talent and resources in our network.</p>
</div>
<div class="button-container">
<a class="button" href="/approach/">
                More on our approach              </a>
</div>
</div>
<div class="section-lower-stroke"></div>
</div>
<div class="next-section js-next-section">
<i class="icon icon-angle-down"></i>
</div>
</section>
<section class="home-section home-section-white" id="section-4">
<div class="container">
<div class="section-upper-stroke"></div>
<div class="inner">
<h2 class="home-header">We have considerable experience in our core industry sectors.</h2>
<div class="section-paragraph">
<p>We have been investing and working for many years in these sectors, including business services, consumer, financial services, healthcare, industrial, and restaurants, retail &amp; franchising, and have deep industry-specific expertise and networks of relationships. This focus and depth of knowledge allows us to operate as a strategic investor in many areas.</p>
</div>
<div class="button-container">
<a class="button" href="/portfolio/">
                More on our portfolio              </a>
</div>
</div>
<div class="section-lower-stroke"></div>
</div>
<div class="next-section js-next-section">
<i class="icon icon-angle-down"></i>
</div>
</section>
</div><!-- #content -->
</div><!-- #primary -->
</div><!-- #main -->
<footer class="main-footer" id="colophon" role="contentinfo">
<div class="container">
<li class="widget widget_nav_menu" id="nav_menu-4"><div class="menu-footer-container"><ul class="menu" id="menu-footer"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-31 current_page_item menu-item-33" id="menu-item-33"><a href="https://www.altamontcapital.com/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-382" id="menu-item-382"><a href="https://www.altamontcapital.com/approach/">Approach</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29" id="menu-item-29"><a href="https://www.altamontcapital.com/team/">Team</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1162" id="menu-item-1162"><a href="https://www.altamontcapital.com/advisors/">Advisors</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28" id="menu-item-28"><a href="https://www.altamontcapital.com/portfolio/">Portfolio</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27" id="menu-item-27"><a href="https://www.altamontcapital.com/news/">News</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-306" id="menu-item-306"><a href="https://www.altamontcapital.com/wp-content/uploads/2020/08/Altamont-Firm-Overview.-Aug2020pdf.pdf" target="_blank">Firm Overview</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26" id="menu-item-26"><a href="https://www.altamontcapital.com/contact/">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25" id="menu-item-25"><a href="https://www.altamontcapital.com/terms-of-use/">Terms of Use</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24" id="menu-item-24"><a href="https://www.altamontcapital.com/privacy-policy/">Privacy Policy</a></li>
</ul></div></li>
<li class="widget widget_text" id="text-5"> <div class="textwidget"><a class="footer-social-link" href="https://www.linkedin.com/company/altamont-capital-partners" target="_blank" title="Altamont Capital on LinkedIn">
<img alt="" src="/wp-content/uploads/2016/03/icon-linkedin.png"/>
</a></div>
</li>
<li class="widget widget_text" id="text-6"><h2 class="widgettitle">Altamont Capital Partners</h2>
<div class="textwidget">400 Hamilton Avenue, Suite 230<br/>
Palo Alto, CA 94301<br/>
<a class="__cf_email__" data-cfemail="1f5e5c4f32567179705f7e736b7e7270716b7c7e6f766b7e73317c7072" href="/cdn-cgi/l/email-protection">[email protected]</a><br/>
650 264 7750<br/></div>
</li>
</div>
</footer><!-- #colophon -->
</div><!-- #page -->
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="https://www.altamontcapital.com/wp-includes/js/comment-reply.min.js?ver=4.7.3" type="text/javascript"></script>
<script src="https://www.altamontcapital.com/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpcf7 = {"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script src="https://www.altamontcapital.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.6.1" type="text/javascript"></script>
<script src="https://www.altamontcapital.com/wp-includes/js/wp-embed.min.js?ver=4.7.3" type="text/javascript"></script>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"58647cf5d1","applicationID":"19417140","transactionName":"Y1JSYBEHWBdSW0QKXVoYcVcXD1kKHExVDkJYVkRRThZXA1YVWAxfUQ==","queueTime":0,"applicationTime":83,"atts":"TxVRFlkdSxk=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>
</html>