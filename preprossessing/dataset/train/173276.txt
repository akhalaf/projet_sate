<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
    //<![CDATA[
    (function(){
      window._cf_chl_opt={
        cvId: "1",
        cType: "interactive",
        cNounce: "93074",
        cRay: "6119c8e11cdc2362",
        cHash: "e7499423af43454",
        cFPWv: "b",
        cRq: {
          ru: "aHR0cHM6Ly93d3cuYXV0b2RvYy5iZy8=",
          ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
          rm: "R0VU",
          d: "IWd515bnL9Uscid6+ber5jZB5AoE5meilMneRIrJvlIIs6nYPrBN1z5KnTg9+I2tiO69KKmWkyX9UIBgrcoB609HDJQioQtXk1O0mo6+mUrC3Di0ARfTuuJdP4AsGeRcTsuVs4iuP/BW+szntW+TUXHSkREUVIHnZJbbApPCsbKPQW5XoS+JH6gMkhOB0nuLRZntiVq9/1d1+KMSVW1MHkzVs/PtXsLPlIpk3PnYQtguO2xHckrZhlE6QiEK3AiNonGL0ZpO7mJOkVkp38ED5B6WwyTflSx1r00c5pGC6TlGKXqdidAoQDgV1fZHEaxFAfdWTSX9Ivkze9YdKhRka7btW4b2hpJAv2yOphT8Xg1tvutzoMmvlmPFrTXo4M0rdYpKp4imFyqocp38m3cdg9i1HZv91LWw0EBxpyQxG51pnhTO8hnpT+bNyB4WwSg51RfK8KKjJlP8SVF5XxjA4DsDXxDbGdF8rNyYoSrp7eXMngbDdo62Nlbh/jbB8jH9+h49B9xKC6orq5GmxA/SUlhRGlqaFnBfqzNoIIe9ttHCAl9JjIOQerEI+4aeVtoTWQNqRHroR20su+zDJ1SR+8o8CZS2PeG8zPPnzEuQp1hEFHPO4BcN7ZKT158EAphLj9xREGb23CuP6MMxb2cGnAxAlIYHkIM2alZF28qfIHMSYWHDjE2xSO5Ddp0rn+gj6uBqfM8IldOg1EPYb15EkZWMjqKI6ZLY29mGtPoTB/b0NULRas8N5eb7/aq8UeqjFOJPeskSXz4K9R5FLhSvmA==",
          t: "MTYxMDY1Mjc2NC4zMzgwMDA=",
          m: "SJB9ghAMvq3HMskDeualLkenUsCZArYtJP7mYOqZtyg=",
          i1: "OH7ASaLq2PErfsPgEmx4Lg==",
          i2: "kvkfsSezI+wZ2nO36djj1g==",
          uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
          hh: "C6vqbgHOZrRgrdK4go4NkYB9uBIJugh37lxwJws5azI=",
        }
      }
      window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
      var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
        var cpo = document.createElement('script');
        cpo.type = 'text/javascript';
        cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
        var done = false;
        cpo.onload = cpo.onreadystatechange = function() {
          if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
            done = true;
            cpo.onload = cpo.onreadystatechange = null;
            window._cf_chl_enter()
          }
        };
        document.getElementsByTagName('head')[0].appendChild(cpo);
      }, false);
    })();
    //]]>
  </script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.autodoc.bg</h2>
</div>
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=69b9ff8e66bb5faf512730c6877f2e431bb97980-1610652764-0-AQhh3GsXlWYcCnfuyr5H-EZi2RaU9LMeNYBVS9mbb_Yl39-2x3_GfdnueeelkaSNEXrGqxg8gY9V4IADrgdJ4iiYzAvPoz-pTe77CRp4Ew23xdne9erOoD_oaZQE8hQLDwUt0G8WzTMvHo0QXaLkjoMhZVOcaIdavUYY7abkcdmSUVjczHJ9AgCRNfBxFLV_lx-D1fEHgZPqbNU9Txz-b1AcpPQRjransQbzu-yjmnrNyZkHbnxMkz7RPjJ0KRpGFlBTrYKq-_RlbZHxANm6NsiaCWeHA8NMAkT2B6tuRzMGaESwYfdR6zTbOXICEOfm0I26oe7Ce_Z1UWzGTSRkD2ceVonDpGO2oSDV7STEjdpUkdhHJpMAAntVJE-4-Fvv_d94gXfDk5FNk7ramlxgfLTABuDFfJLqR-4dYlWkJxl-pGjQC_hHt6rFyAsbIK8hMW1o7-epaMxQ9I8f7_fUfCZh8IjeayFdd78pOEdA5AxSnWsoYSobypNU0oqqpCvh54CDcPcjn5XXDfkrFyj2cx9FOKgqDABrLd9u04FS52pmy9hglbEStj5HF2l-l4ZlhRq-0ltUOCy7n_uT3VMByL0" class="challenge-form interactive-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="a2d2e88211687036637d05386b88493428a70224-1610652764-0-AS/kJRz+HFCqbOLHN/QLQEdCQ0jXJDPMDjIqvJ1XHwwwT0N0tjPK9dzQjdvPu6DnLk1RNh6QFVI7kDDDq9LHb2Rt3nLlDCKe7thCpCyNVJN3ttUcsZYbq60UnDAG8Tf2wked3D3BgsNv7rIOpgMt62ISzMZUBRNHzD1IJcbvTWwWbbgwczfUIqoYM2iZLmmuqDlm9tbLYZleWrXpG7gqPF4xvFIIc5e2Z3btOqjsm0nT0CQ9XiJkp3/gxrdQWnW8rBzk0jV+ixywM30jQcujqqtCQj6omv4YXQSsiXFKsX7B+vggK/6Xg/GHEp0qomM8L0/DK2FHmeY9nIV02pv6YzCFlrVCZger5BlXNiqU4rfAitA574vxC4kFic8YjfglFKXYUl2r61mX3ne7pnz2rvAuDKkoMi8ZaSH94BC/sFRwnznaTKWNr7xgeufCBdyCIjbJ7J83dOW2WKFewq2H7o8fGqXhI4LMmgxdI8uANDz2gmQR86AoBRMPx7j4YLf/FJtOXhPjjD5bQ+foak26RWoI7sL1x5pH/NALekofWZ5FxUmjEr39pK7yXZwz3s2Q2frUJmi7ebAJ8dg11mfJrP269qvlKqSS3a/jRZO+KvTipeb/2x67r7IAii2RN90K+qAVMwvMjX2JRCTcbrr32aF86Y7tZyCjkYz+sCa+Qm2UhMxJwqPueOTYYE84elveRnNYSgmKwsU3BkpYal6BxbLM3FwlO11xayyNTNWEfUcICbPe2D0AjPvKvZJ/k/5XOBX/7maCeylu3KXCrd9V3iwzRcOD2kOxd7SDfo1U+L259kNtcIs+nO/JDdZOX7AWqfTlMGyMQ5Qhr4QRV4FF0oitl4uftCWdK+RWr/Z1ZazG0L6QpI/GKiblFJqB/4BYUD5xz8jRVkmnV8ee4DcI4Zdue9fK8uRy1+2sYbN4N7KYbBJt2yon1p7h2IVWodfL8SApPnrJJN6LfZ4vPXzjWs/W10zyeaTk70aC+ztDJfi789OAwb+jlLrtY87KmZcDctWZQMciyg49BCVHne3/+DQhTFE24PRD7hVvb+ZgEcF1an+04NldTECzro/ZTkv2cnmOaIBCXQZXy9t/XqbmAYu928mVhmE2B+LkPW12QddMPDdddhBy8tZpOzIOV/DGv0uAb2rMDzPpskm+o7X16yZAUOVebFqlER7taE72Hz9d3D5Frg170eeAGcSD0ZGsfnLCZw0xfti0+/djl10eLVJBaipZGx+eS/G+CNNIMkizor5pORGGEDqtdeTMtNxAXDO+uZvoOJcMzESUAdcYvOx4+UI25q1J5FcUlsVZOVfx"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="5d00f17ad4dab52ec7a2b101255e7e75"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=6119c8e11cdc2362')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div>
</div>
</div>
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div>
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">6119c8e11cdc2362</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:e444:4db8:6cea:7c56</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div>
</div>
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
