<html>
<head><title>Baseball-Cards.com - Page Not Found</title>
<meta content="Page NOT FOUND page" name="description"/>
<meta content="Vintage baseball cards not found" name="keywords"/>
<script language="javascript" src="/js/all.min.js" type="text/javascript"></script>
<link href="/css/style.min.css" rel="stylesheet" type="text/css"/>
</head>
<body alink="#FFFFFF" bgcolor="white" leftmargin="3" link="#FF0000" marginheight="0" marginwidth="0" text="#000000" topmargin="0" vlink="E47833">
<basefont size="3"></basefont>
<!-- ==================================================================== -->
<!--  TOP OF PAGE  ====================================================== -->
<!-- ==================================================================== -->
<!-- ==================================================================== -->
<!-- TOP OF PAGE  (to copy)                                               -->
<!-- -->
<!-- to AUCSHELL1.HTML, VINTAGE/INDEX.HTML  GRADE-CARDS.COM/INDEX         -->
<!-- to www.GRADED-CARDS.COM/INDEX.HTML  &  GRADLINKS.HTML  (with mods)   -->
<!-- -->
<!-- SEARCH FOR MOD   to see mods to standard                             -->
<!--    banner size 890 to 800                                            -->
<!-- ==================================================================== -->
<basefont color="BLACK" size="3"></basefont>
<a name="top"></a>
<center>
<!-- ==================================================================== -->
<!--  TOP BANNER & LINKS AREA                                             -->
<!-- ==================================================================== -->
<div id="site_nav">
<a href="https://www.baseball-cards.com"><!-- generate a random <IMG> at top of page --><script language="Javascript" type="text/javascript">getimage()</script></a>
<table style="border-radius:25px;">
<tr>
<td>●<a href="/auction/">Card Auction</a></td>
<td>●<a href="/vintage-baseball-cards/">vintage baseball/football cards</a></td>
<td>●<a href="/ultra-pro-card-supplies/">Card Supplies</a></td>
<td>
<form>
<select onchange="selectJump(this)" size="1">
<option selected="" value="#">ON SALE...</option>
<option value="/autographed-team-baseballs/">Autographs,Balls,UDA</option>
<option value="/vintage-baseball-cards/vintage-football.shtml">Old Football cards  </option>
<option value="/vintage-baseball-cards/vintage-non-sports.shtml">Old Non-Sport Cards </option>
<option value="/wirephoto/">UPI/AP Wirephotos   </option>
<option value="/vintage-baseball-cards/complete-baseball-cards-sets.shtml">Complete Card Sets  </option>
<option value="/vintage-baseball-cards/psa-graded-vintage-baseball-cards.shtml">PSA Graded Cards </option>
<option value="/ccards/ccardsb.shtml">UDA Comm. cards     </option>
<option value="/vintage-baseball-cards/boxing-track-cards.shtml">Vintage Boxing cards</option>
<option value="/vintage-baseball-cards/golf-cards.shtml">Vintage Golf cards  </option>
<option value="/players/players.shtml">Players Pages       </option>
</select>
</form>
</td>
<td>
<form>
<select onchange="selectJump(this)" size="1">
<option selected="" value="#">                                       INFO...   </option>
<option value="/auction/better-auction.shtml">      Us vs eBay</option>
<option value="/auction/faq.shtml">                 FAQ       </option>
<option value="/links/">                                 Links     </option>
<option value="/members/">                               Members   </option>
<option value="/auction/contactus.shtml">           Contact Us</option>
</select>
</form>
</td>
</tr>
</table>
</div>
<!-- ==================================================================== -->
<!--  END   TOP BANNER & LINKS AREA                                       -->
<!-- ==================================================================== -->
<!-- ====================================== -->
<!--            F R E E B I E S             -->
<!-- ====================================== -->
<div class="freediv">
<table class="freebies">
<tr>
<td><font color="YELLOW" face="Comic Sans MS" size="7">FREE</font></td>
<td><img alt="Free Baseball Cards" height="50" src="/images/free10-1-s.jpg" style="border-radius:25px;" width="172"/></td>
<td class="middiv">
<a href="/auction/free-baseball-cards.shtml">
<font color="#33ffCC">
             (10) NM/MINT <br/>
        Vintage Hall-of-Famers<br/></font>
</a>
<span class="smallprint">
               click for details 
           </span>
</td>
<td><img alt="Free Baseball Cards" height="50" src="/images/free10-1-s.jpg" style="border-radius:25px;" width="172"/></td>
<td><font color="YELLOW" face="Comic Sans MS" size="7">FREE</font></td>
</tr>
</table>
</div>
<!-- ====================================== -->
<!--  END FREEBIES                          -->
<!-- ====================================== -->
<!-- ==================================================================== -->
<!--  END OF TOP OF PAGE TO COPY                                          -->
<!-- ==================================================================== -->
<!-- ==================================================================== -->
<!--  END TOP OF PAGE  ================================================== -->
<!-- ==================================================================== -->
<table width="800"><tr><td>
<center>
<br/>
<!-- ==================================================================== -->
<!--  HEADER  =========================================================== -->
<!-- ==================================================================== -->
<img src="/images/construction.gif"/>
<font color="RED" size="6">
<b>  ERROR   -   
  </b></font> <font color="GREEN" size="6">
    PAGE NOT FOUND
  <img src="/images/construction.gif"/><p>
</p></font>
<table border="2" bordercolor="BLACK" cellpadding="5">
<tr bgcolor="RED">
<td>
<img src="/images/arrow-back.jpg"/>
</td>
<td>
<font color="BLACK" size="5"><b>
            You may want to click your browser's       <br/>
            BACK ARROW to return to the prior page   <br/>
            or ... use one of the links above or below. <br/>
</b></font>
</td>
</tr>
</table>
</center></td></tr></table>
</center>
<!-- ==================================================================== -->
<!--  TOP OF PAGE  ====================================================== -->
<!-- ==================================================================== -->
<!-- ==================================================================== -->
<!--  START OF BOTTOM OF PAGE                                             -->
<!-- ==================================================================== -->
<center>
<div id="pagebottom">
<hr noshade="" size="6" width="750"/>
<div class="pb_right_white_box">
<!-- ============================================================== -->
<!-- CONTACT US                                                     -->
<!-- ============================================================== -->
<div style="border: 1px solid grey; padding: .1em; margin-bottom: .1em; font-size: 100%;">
<b> Buying Vintage Baseball / Sports / Non-Sports Collections </b> <br/>
             Email: Joe [at] Baseball-Cards [dot] com  (re:auctions)

      </div>
<!-- ============================================================== -->
<!-- TEXT LINKS (internal)  ======================================= -->
<!-- ============================================================== -->
<div class="pb_right_white_links">
<a href="/auction/"> <b>Baseball Card Auctions</b></a>
<a href="/auction/better-auction.shtml"> My Auction vs 'the Bay'</a>
<a href="/auction/faq.shtml"> FAQ</a>
<br/>
<a href="/auction/tutorial.shtml"> Card Auction Tutorial</a>
<a href="/vintage-baseball-cards/"><b>Vintage Baseball Cards</b></a>
<a href="/autographed-team-baseballs/"><b>Autographed Team Balls</b></a>
<br/>
<a href="/ultra-pro-card-supplies/"><b>Ultra-Pro Baseball Card Supplies</b></a>
<a href="/vintage-baseball-cards/psa-graded-vintage-baseball-cards.shtml"> PSA Graded Cards</a>
<a href="/vintage-baseball-cards/complete-baseball-cards-sets.shtml"> Complete Card Sets</a>
<br/>
<a href="/vintage-baseball-cards/golf-cards.shtml">Golf</a>
<a href="/vintage-baseball-cards/boxing-track-cards.shtml">Boxing &amp; Sports</a>
<a href="/vintage-baseball-cards/non-sports-vintage-cards.shtml">Non-Sports Cards</a>
<a href="/wirephoto/">Wire Photos</a>
<br/>
<!-- ================================== -->
<!-- ====== Copyright ================= -->
<!-- ================================== -->
<address>
           ©1995-2019 Baseball-Cards.com / Joseph Juhasz / All Rights Reserved
        </address>
</div>
</div>
</div>
</center>
<!-- ================================================================== -->
<!--  END OF BOTTOM OF PAGE                                             -->
<!-- ================================================================== -->
<!-- ==================================================================== -->
<!--  END TOP OF PAGE  ================================================== -->
<!-- ==================================================================== -->
</body>
</html>
