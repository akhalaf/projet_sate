<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<meta content="" name="description"/>
<title>ardgroup.ru</title>
<!-- Bootstrap core CSS -->
<link href="/templates/sb-creative/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Custom fonts for this template -->
<link href="/templates/sb-creative/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic" rel="stylesheet" type="text/css"/>
<!-- Plugin CSS -->
<link href="/templates/sb-creative/vendor/magnific-popup/magnific-popup.css" rel="stylesheet"/>
<!-- Custom styles for this template -->
<link href="/templates/sb-creative/css/creative.min.css" rel="stylesheet"/>
</head>
<body id="page-top">
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
<div class="container">
<a class="navbar-brand js-scroll-trigger" href="#page-top">www.ardgroup.ru</a>
<button aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right" data-target="#navbarResponsive" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarResponsive">
<ul class="navbar-nav ml-auto">
<li class="nav-item">
<!--              <a class="nav-link js-scroll-trigger" href="#contact">Контакты</a> -->
</li>
</ul>
</div>
</div>
</nav>
<header class="masthead text-center text-white d-flex">
<div class="container my-auto">
<div class="row">
<div class="col-lg-10 mx-auto">
<h1 class="text-uppercase">
<strong>www.ardgroup.ru</strong>
</h1>
<hr/>
</div>
<div class="col-lg-8 mx-auto">
<p class="text-faded mb-5">Этот домен продается</p>
<a class="btn btn-primary btn-xl js-scroll-trigger" href="https://www.nic.ru/shop/ardgroup.ru" id="shopbtn">Купить прямо сейчас </a>
</div>
</div>
</div>
</header>
<!-- Bootstrap core JavaScript -->
<script src="/templates/sb-creative/vendor/jquery/jquery.min.js"></script>
<script src="/templates/sb-creative/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Plugin JavaScript -->
<script src="/templates/sb-creative/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="/templates/sb-creative/vendor/scrollreveal/scrollreveal.min.js"></script>
<script src="/templates/sb-creative/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<!-- Custom scripts for this template -->
<script src="/templates/sb-creative/js/creative.min.js"></script>
<script>
      var btn = document.getElementById("shopbtn");
      btn.addEventListener("click", sendRequest);
      function sendRequest(event){
        var request = new XMLHttpRequest();      
        var body = "host=" + "www.ardgroup.ru";
        request.open("POST", "/onclick");
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.send(body);     
      }
    </script>
</body>
</html>
<!--wdm-->