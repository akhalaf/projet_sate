<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="keywords"/>
<meta content="" name="author"/>
<meta content="326516237427150" property="fb:app_id"/>
<meta content="0X6Igz7B0N9tpyQbMPKrSfvSyaX7ccxKbFozBx3i" name="csrf-token"/>
<meta content="v1.1.34" name="app-version"/>
<meta content="https://static.neris-assets.com/" name="app-jscdn"/>
<meta content="0" name="app-bs-uid"/>
<meta content="" name="api-token"/>
<meta content="en" name="app-locale"/>
<title>

    Free personality test, type descriptions, relationship and career advice

 | 16Personalities</title>
<link href="https://static.neris-assets.com/images/favicons/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="https://static.neris-assets.com/images/favicons/apple-touch-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="https://static.neris-assets.com/images/favicons/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="https://static.neris-assets.com/images/favicons/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="https://static.neris-assets.com/images/favicons/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="https://static.neris-assets.com/images/favicons/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="https://static.neris-assets.com/images/favicons/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="https://static.neris-assets.com/images/favicons/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="https://static.neris-assets.com/images/favicons/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://static.neris-assets.com/images/favicons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="https://static.neris-assets.com/images/favicons/favicon-194x194.png" rel="icon" sizes="194x194" type="image/png"/>
<link href="https://static.neris-assets.com/images/favicons/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="https://static.neris-assets.com/images/favicons/android-chrome-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="https://static.neris-assets.com/images/favicons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<meta content="#ffffff" name="msapplication-TileColor"/>
<meta content="https://static.neris-assets.com/images/favicons/mstile-144x144.png" name="msapplication-TileImage"/>
<meta content="#ffffff" name="theme-color"/>
<link href="https://static.neris-assets.com/css/main-core--d8dab4.css" rel="stylesheet"/>
<link href="https://www.16personalities.com/css/fonts.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600|Open+Sans:400,400i,600,600i,700&amp;subset=cyrillic,latin-ext" rel="stylesheet"/>
<link href="https://www.16personalities.com/css/font-awesome.min.css" rel="stylesheet"/>
<link href="https://static.neris-assets.com/css/large--412765.css" media="screen and (min-width: 768px)" rel="stylesheet"/>
<link href="https://www.16personalities.com" rel="canonical"/>
<link href="https://www.16personalities.com/ar" hreflang="ar" rel="alternate"/>
<link href="https://www.16personalities.com/bg" hreflang="bg" rel="alternate"/>
<link href="https://www.16personalities.com/ch" hreflang="ch" rel="alternate"/>
<link href="https://www.16personalities.com/cs" hreflang="cs" rel="alternate"/>
<link href="https://www.16personalities.com/da" hreflang="da" rel="alternate"/>
<link href="https://www.16personalities.com/de" hreflang="de" rel="alternate"/>
<link href="https://www.16personalities.com/ee" hreflang="ee" rel="alternate"/>
<link href="https://www.16personalities.com/es" hreflang="es" rel="alternate"/>
<link href="https://www.16personalities.com/el" hreflang="el" rel="alternate"/>
<link href="https://www.16personalities.com/fa" hreflang="fa" rel="alternate"/>
<link href="https://www.16personalities.com/fi" hreflang="fi" rel="alternate"/>
<link href="https://www.16personalities.com/fr" hreflang="fr" rel="alternate"/>
<link href="https://www.16personalities.com/he" hreflang="he" rel="alternate"/>
<link href="https://www.16personalities.com/hi" hreflang="hi" rel="alternate"/>
<link href="https://www.16personalities.com/hr" hreflang="hr" rel="alternate"/>
<link href="https://www.16personalities.com/hu" hreflang="hu" rel="alternate"/>
<link href="https://www.16personalities.com/id" hreflang="id" rel="alternate"/>
<link href="https://www.16personalities.com/is" hreflang="is" rel="alternate"/>
<link href="https://www.16personalities.com/it" hreflang="it" rel="alternate"/>
<link href="https://www.16personalities.com/ja" hreflang="ja" rel="alternate"/>
<link href="https://www.16personalities.com/ko" hreflang="ko" rel="alternate"/>
<link href="https://www.16personalities.com/nl" hreflang="nl" rel="alternate"/>
<link href="https://www.16personalities.com/no" hreflang="no" rel="alternate"/>
<link href="https://www.16personalities.com/pl" hreflang="pl" rel="alternate"/>
<link href="https://www.16personalities.com/br" hreflang="br" rel="alternate"/>
<link href="https://www.16personalities.com/ro" hreflang="ro" rel="alternate"/>
<link href="https://www.16personalities.com/ru" hreflang="ru" rel="alternate"/>
<link href="https://www.16personalities.com/sk" hreflang="sk" rel="alternate"/>
<link href="https://www.16personalities.com/sl" hreflang="sl" rel="alternate"/>
<link href="https://www.16personalities.com/sr" hreflang="sr" rel="alternate"/>
<link href="https://www.16personalities.com/sv" hreflang="sv" rel="alternate"/>
<link href="https://www.16personalities.com/sw" hreflang="sw" rel="alternate"/>
<link href="https://www.16personalities.com/th" hreflang="th" rel="alternate"/>
<link href="https://www.16personalities.com/tr" hreflang="tr" rel="alternate"/>
<link href="https://www.16personalities.com/uk" hreflang="uk" rel="alternate"/>
<script>

    (function(w,d,n){w.jscdn=function(){return(d.head.querySelector('meta[name="'+n+'"]')||{}).content||'/'}}(window,document,'app-jscdn'))

    window.appRoutes = window.appRoutes || {};

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-27031617-1', 'auto');
    ga('set', 'anonymizeIp', true);

</script>
<style>[v-cloak]{display:none !important;}</style>
<script>
    function toggleMobileMenu(){document.getElementById('mobile-menu').style.display='flex'===document.getElementById('mobile-menu').style.display?'none':'flex'}
</script> <link href="https://www.16personalities.com/free-personality-test" rel="prerender"/>
</head>
<body class="" role="document">
<div class="main-wrapper" id="main-app">
<div class="navbar sticky ">
<a class="logo " href="https://www.16personalities.com">
<img alt="16Personalities" src="https://static.neris-assets.com/images/logo.svg"/>
</a>
<nav class="middle-section ">
<ul>
<li class=" test-link">
<a href="https://www.16personalities.com/free-personality-test">Take the Test</a>
</li>
<li class="">
<a href="https://www.16personalities.com/personality-types">Personality Types</a>
<ul class="wide">
<li class="types">
<a href="https://www.16personalities.com/personality-types#analysts">
<img src="https://static.neris-assets.com/images/academy/analysts/icons/academic-path.svg"/>
<div class="info">
<div class="title">Analysts</div>
<div class="subtitle">Intuitive (<strong>N</strong>) and Thinking (<strong>T</strong>) personality types, known for their rationality, impartiality, and intellectual excellence.</div>
</div>
</a>
<ul>
<li>
<a href="https://www.16personalities.com/intj-personality">Architect <span>INTJ</span></a>
</li>
<li>
<a href="https://www.16personalities.com/intp-personality">Logician <span>INTP</span></a>
</li>
<li>
<a href="https://www.16personalities.com/entj-personality">Commander <span>ENTJ</span></a>
</li>
<li>
<a href="https://www.16personalities.com/entp-personality">Debater <span>ENTP</span></a>
</li>
</ul>
</li>
<li class="types">
<a href="https://www.16personalities.com/personality-types#diplomats">
<img src="https://static.neris-assets.com/images/academy/diplomats/icons/romantic-relationships.svg"/>
<div class="info">
<div class="title">Diplomats</div>
<div class="subtitle">Intuitive (<strong>N</strong>) and Feeling (<strong>F</strong>) personality types, known for their empathy, diplomatic skills, and passionate idealism.</div>
</div>
</a>
<ul>
<li>
<a href="https://www.16personalities.com/infj-personality">Advocate <span>INFJ</span></a>
</li>
<li>
<a href="https://www.16personalities.com/infp-personality">Mediator <span>INFP</span></a>
</li>
<li>
<a href="https://www.16personalities.com/enfj-personality">Protagonist <span>ENFJ</span></a>
</li>
<li>
<a href="https://www.16personalities.com/enfp-personality">Campaigner <span>ENFP</span></a>
</li>
</ul>
</li>
<li class="types">
<a href="https://www.16personalities.com/personality-types#sentinels">
<img src="https://static.neris-assets.com/images/academy/sentinels/icons/careers.svg"/>
<div class="info">
<div class="title">Sentinels</div>
<div class="subtitle">Observant (<strong>S</strong>) and Judging (<strong>J</strong>) personality types, known for their practicality and focus on order, security, and stability.</div>
</div>
</a>
<ul>
<li>
<a href="https://www.16personalities.com/istj-personality">Logistician <span>ISTJ</span></a>
</li>
<li>
<a href="https://www.16personalities.com/isfj-personality">Defender <span>ISFJ</span></a>
</li>
<li>
<a href="https://www.16personalities.com/estj-personality">Executive <span>ESTJ</span></a>
</li>
<li>
<a href="https://www.16personalities.com/esfj-personality">Consul <span>ESFJ</span></a>
</li>
</ul>
</li>
<li class="types">
<a href="https://www.16personalities.com/personality-types#explorers">
<img src="https://static.neris-assets.com/images/system/explorers-menu.svg"/>
<div class="info">
<div class="title">Explorers</div>
<div class="subtitle">Observant (<strong>S</strong>) and Prospecting (<strong>P</strong>) personality types, known for their spontaneity, ingenuity, and flexibility.</div>
</div>
</a>
<ul>
<li>
<a href="https://www.16personalities.com/istp-personality">Virtuoso <span>ISTP</span></a>
</li>
<li>
<a href="https://www.16personalities.com/isfp-personality">Adventurer <span>ISFP</span></a>
</li>
<li>
<a href="https://www.16personalities.com/estp-personality">Entrepreneur <span>ESTP</span></a>
</li>
<li>
<a href="https://www.16personalities.com/esfp-personality">Entertainer <span>ESFP</span></a>
</li>
</ul>
</li>
</ul>
</li>
<li class="">
<a href="https://www.16personalities.com/academy">Premium Profiles</a>
</li>
<li class="">
<a href="https://www.16personalities.com/tools">Tools &amp; Assessments</a>
<ul class="wide">
<li>
<a href="https://www.16personalities.com/tools/personal-growth">
<img src="https://static.neris-assets.com/images/academy/sentinels/icons/personal-growth.svg"/>
<div class="info">
<div class="title">Personal Growth</div>
<div class="subtitle">Discover and understand your strengths and weaknesses.</div>
</div>
</a>
</li>
<li>
<a href="https://www.16personalities.com/tools/relationships">
<img src="https://static.neris-assets.com/images/academy/sentinels/icons/romantic-relationships.svg"/>
<div class="info">
<div class="title">Relationships</div>
<div class="subtitle">Deepen your relationships, both romantic and otherwise.</div>
</div>
</a>
</li>
<li>
<a href="https://www.16personalities.com/tools/career">
<img src="https://static.neris-assets.com/images/academy/sentinels/icons/careers.svg"/>
<div class="info">
<div class="title">Professional Success</div>
<div class="subtitle">Kick-start your career or get better at navigating it.</div>
</div>
</a>
</li>
</ul>
</li>
<li class="">
<a href="https://www.16personalities.com/articles?category=list">Library</a>
<ul class="wide">
<li>
<a href="https://www.16personalities.com/articles?category=list">
<img src="https://static.neris-assets.com/images/academy/sentinels/icons/introduction.svg"/>
<div class="info">
<div class="title">Articles</div>
<div class="subtitle">Get tips, advice, and deep insights into personality types.</div>
</div>
</a>
</li>
<li>
<a href="https://www.16personalities.com/articles/our-theory">
<img src="https://static.neris-assets.com/images/academy/sentinels/icons/theory.svg"/>
<div class="info">
<div class="title">Theory</div>
<div class="subtitle">Understand the meaning and impact of personality traits.</div>
</div>
</a>
</li>
<li>
<a href="https://www.16personalities.com/insights">
<img src="https://static.neris-assets.com/images/system/insights.svg"/>
<div class="info">
<div class="title">Surveys</div>
<div class="subtitle">Explore and participate in hundreds of our studies.</div>
</div>
</a>
</li>
<li>
<a href="https://www.16personalities.com/country-profiles">
<img src="https://static.neris-assets.com/images/system/country-profiles-menu.svg"/>
<div class="info">
<div class="title">Country Profiles</div>
<div class="subtitle">Examine our regional and country personality profiles.</div>
</div>
</a>
</li>
</ul>
</li>
</ul>
</nav>
<div class="right-section ">
<a class="search-toggle" href="#">
<icon-search></icon-search>
</a>
<a class="language-toggle" href="https://www.16personalities.com/languages"><span class="f16 f16-globe"></span></a>
<a class="no-formatting with-border login-link" href="javascript:">Log In</a>
<a class="btn btn-action test-button" href="https://www.16personalities.com/free-personality-test">Take the Test</a>
</div>
<nav-mobile-toggle class="index"></nav-mobile-toggle>
<nav-mobile id="mobile-menu">
<div class="menu anonymous">
<div class="part padded">
<a class="btn btn-action" href="https://www.16personalities.com/free-personality-test">Take the Test</a>
</div>
<div class="part">
<a class="no-formatting " href="https://www.16personalities.com/personality-types">Personality Types</a>
<a class="no-formatting academy " href="https://www.16personalities.com/academy">Premium Profiles</a>
<a class="no-formatting academy " href="https://www.16personalities.com/tools">Tools &amp; Assessments</a>
<div class="no-formatting item expandable ">
<div class="title" onclick="this.parentElement.classList.toggle('expanded');">
                            Library
                            <span class="fas fa-caret-right"></span>
<span class="fas fa-caret-down"></span>
</div>
<div class="submenu">
<a class="no-formatting item " href="https://www.16personalities.com/articles?category=list">Articles</a>
<a class="no-formatting item " href="https://www.16personalities.com/articles/our-theory">Theory</a>
<a class="no-formatting item " href="https://www.16personalities.com/insights">Surveys</a>
<a class="no-formatting item " href="https://www.16personalities.com/country-profiles">Country Profiles</a>
</div>
</div>
<div class="no-formatting item">Search</div>
</div>
<div class="part no-border login">
<div class="no-formatting item">Log In to Your Profile</div>
</div>
<div class="part bottom">
<a class="no-formatting item " href="https://www.16personalities.com/contact-us">Contact Us</a>
<a class="no-formatting item " href="https://www.16personalities.com/languages">Switch Language</a>
</div>
</div>
</nav-mobile>
</div>
<nav class="bottom">
<a href="https://www.16personalities.com/free-personality-test">
<span class="f16 f16-profile"></span>
<span class="title">Test</span>
</a>
<a class="" href="https://www.16personalities.com/personality-types">
<span class="f16 f16-friends"></span>
<span class="title">Types</span>
</a>
<a class="" href="https://www.16personalities.com/academy">
<svg viewbox="0 0 60 60" xmlns="http://www.w3.org/2000/svg"><polygon class="cls-1" points="0 17.91 0 13.61 30 0 59.74 12.6 60 18.19 54.86 16.66 55.47 20.63 4.91 19.77 5.56 16.19 0 17.91"></polygon><polygon class="cls-1" points="7.89 22.47 16.03 21.93 16.29 46.48 19.91 46.48 19.53 22.75 27.67 22.2 27.16 46.48 32.07 46.48 32.33 22.75 38.66 23.02 38.66 46.61 43.65 46.61 44.45 21.79 52.21 22.75 50.95 46.48 54.31 47.03 55.09 51.42 4.14 51.56 4.01 46.48 9.44 46.75 7.89 22.47"></polygon><polygon class="cls-1" points="0.39 53.92 0.13 60 59.1 60 58.45 53.79 0.39 53.92"></polygon></svg>
<span class="title">Premium</span>
</a>
<a class="" href="https://www.16personalities.com/tools">
<svg viewbox="0 0 60 60" xmlns="http://www.w3.org/2000/svg"><polygon class="cls-1" points="56.71 -0.11 40.69 4.71 43.03 6.94 53.49 5.61 38.5 20.23 21.67 16.13 23.5 11.35 18.19 7.36 10.59 10.18 12.14 16.97 7.64 27.95 3.58 27.82 -0.13 32.45 3.93 38.49 10.33 38.27 13.53 31.66 10.04 29.46 14.47 19.25 19.99 19.41 33.04 21.58 31.13 23.62 32.24 28.96 39.19 30.3 43.79 26.66 41.14 22.86 57.27 5.55 54.79 18.17 60.13 15.12 60.13 -0.11 56.71 -0.11"></polygon><polygon class="cls-1" points="1.6 59.89 -0.13 44.52 8.15 43.16 10.46 59.89 1.6 59.89"></polygon><polygon class="cls-1" points="15.27 59.89 24.51 59.89 23.55 31.75 16.81 31.75 15.27 59.89"></polygon><polygon class="cls-1" points="29.52 59.89 27.59 36.24 37.6 37.19 36.84 59.89 29.52 59.89"></polygon><polygon class="cls-1" points="41.84 59.89 43 43.21 48.39 43.21 48.39 59.89 41.84 59.89"></polygon><polygon class="cls-1" points="52.43 59.89 51.66 22.55 60.13 20.93 60.13 59.89 52.43 59.89"></polygon></svg>
<span class="title">Toolkits</span>
</a>
<a class="" href="https://www.16personalities.com/articles">
<span class="f16 f16-additional-tests"></span>
<span class="title">Library</span>
</a>
</nav>
<main class="q-hm">
<div class="hero">
<div class="text-wrapper">
<h1>“It’s so incredible to finally be understood.”</h1>
<p class="subtitle">Take our Personality Test and get a “freakishly accurate” description of who you are and why you do things the way you do.</p>
</div>
<div class="action-wrapper">
<a class="btn btn-action" dusk="quiz-button" href="https://www.16personalities.com/free-personality-test">Take the Test <span class="fas fa-arrow-right"></span></a>
</div>
</div>
<div class="scene">
<img src="https://static.neris-assets.com/images/homepage/welcome.svg"/>
<svg preserveaspectratio="none" viewbox="0 0 1920 476"><polygon class="c1" points="1920 476 1810 403 1158 259 578 391 420 303 219 442 0 476 0 0 1920 0 1920 476"></polygon></svg>
</div>
<div class="stats-wrapper">
<div class="stats">
<div class="counter">353,239,930</div>
<div class="text">Tests taken so far</div>
</div>
<p class="subtitle">Curious how accurate we are about you?<br/> <a class="with-border" href="https://www.16personalities.com/free-personality-test">Take the test</a> and find out.</p>
</div>
<div class="reviews">
<div class="review diplomat">
<div class="f16 f16-quote"></div>
<div class="text">“I was honestly shocked how accurate it was. I teared up a bit because it was like there was a person looking inside my mind and telling me what they saw.”</div>
<img data-src="https://static.neris-assets.com/images/personality-types/avatars/faces/infj-advocate-s3-v1-female.svg"/>
<div class="name">Emma – “The Advocate”</div>
</div>
<div class="review explorer">
<div class="f16 f16-quote"></div>
<div class="text">“I can’t believe how accurate this was... Word for word. I never felt so understood. I thought I was the only one of my kind.”</div>
<img data-src="https://static.neris-assets.com/images/personality-types/avatars/faces/isfp-adventurer-s3-v1-female.svg"/>
<div class="name">Ashley – “The Adventurer”</div>
</div>
<div class="review analyst">
<div class="f16 f16-quote"></div>
<div class="text">“It is undeniably eerie how the description and traits nailed me so thoroughly. Wish I had known about my personality type 20 years ago, that would have saved me much grief.”</div>
<img data-src="https://static.neris-assets.com/images/personality-types/avatars/faces/intj-architect-s3-v1-male.svg"/>
<div class="name">Augustine – “The Architect”</div>
</div>
</div>
<div class="middle-cta">
<a class="btn btn-action btn-lg" href="https://www.16personalities.com/free-personality-test">Take the Test <span class="fas fa-arrow-right"></span></a>
</div>
<div class="middle-cover">
<div class="vertical-line"></div>
<svg preserveaspectratio="none" viewbox="0 0 1920 190"><polygon class="c1" points="1920 313 0 313 0 181 465 0 1127 125 1513 62 1920 182 1920 313"></polygon></svg>
</div>
<div class="other-actions">
<h2>What else can you do here?</h2>
<div class="horizontal-line"></div>
<div class="actions">
<div class="action first">
<div class="info">
<div class="number">01</div>
<h3>Understand others</h3>
<p>In our free type descriptions you’ll learn what really drives, inspires, and worries different personality types, helping you build more meaningful relationships.</p>
<a class="btn" href="https://www.16personalities.com/personality-types">Read about Types</a>
</div>
<div class="image-wrapper">
<img data-src="https://static.neris-assets.com/images/homepage/personality_types.svg"/>
</div>
</div>
<div class="action second">
<div class="image-wrapper">
<img data-src="https://static.neris-assets.com/images/homepage/academy.svg"/>
</div>
<div class="info">
<div class="number">02</div>
<h3>Get a roadmap for success</h3>
<p>Our premium profiles are for those who want to dive deeper into their personality and learn how to grow and better navigate the world around them.</p>
<a class="btn" href="https://www.16personalities.com/academy">Get Your Premium Profile</a>
</div>
</div>
<div class="action third">
<div class="info">
<div class="number">03</div>
<h3>Join our community</h3>
<p>You are not alone. Our free forum is full of people just like you. Ask and give advice, connect with friends, hear stories, or maybe meet your love.</p>
<a class="btn" href="https://www.16personalities.com/community/discussions">Enter Discussions</a>
</div>
<div class="image-wrapper">
<img data-src="https://static.neris-assets.com/images/homepage/community.svg"/>
</div>
</div>
</div>
</div>
<svg class="trust-background-1" preserveaspectratio="none" viewbox="0 0 1920 187"><polygon class="c1" points="1920,187 0,187 0,181 82,172 458,31 778,110 1354,0 1920,182"></polygon></svg>
<div class="why-trust">
<h2>Why trust us?</h2>
<div class="horizontal-line"></div>
<div class="trust-wrapper">
<div class="point">
<div class="icon-wrapper">
<img class="icon" data-src="https://static.neris-assets.com/images/homepage/research_model.svg"/>
</div>
<div class="info">
<h3>Modern and Reliable Framework</h3>
<div class="description">Our <a href="/articles/our-theory" title="Our framework">personality model</a> incorporates the latest advances in psychometric research, combining time-tested concepts with robust and highly accurate testing techniques.</div>
</div>
</div>
<div class="point">
<div class="icon-wrapper">
<img class="icon" data-src="https://static.neris-assets.com/images/homepage/studies.svg"/>
</div>
<div class="info">
<h3>Hundreds of Pioneering Studies</h3>
<div class="description">Dig into <a href="/articles" title="Our studies">our studies</a> on personality types and their impact on our lives – <a href="/country-profiles" title="Country profiles">geographical distribution</a>, social attitudes, relationships, and much more.</div>
</div>
</div>
<div class="point">
<div class="icon-wrapper">
<img class="icon" data-src="https://static.neris-assets.com/images/homepage/languages.svg"/>
</div>
<div class="info">
<h3>Available in a Number of Languages</h3>
<div class="description">At <a href="https://www.16personalities.com/languages">37 languages</a>, our test is the most translated major personality test on the internet. Speaking French, Spanish or Lithuanian? Take the test in your language!</div>
</div>
</div>
</div>
<div class="action-row">
<a class="btn btn-action btn-lg" href="https://www.16personalities.com/free-personality-test">Take the Test <span class="fas fa-arrow-right"></span></a>
</div>
</div>
<svg class="trust-background-2" preserveaspectratio="none" viewbox="0 0 1920 187"><path class="c1" d="M0 0h1920v64l-238-20-448 143L396 45 0 103z"></path></svg>
</main>
<transition name="slide-fade">
<quiz-splash-modal :new-text="true" data-chunk="guest-chunk" illustration="https://static.neris-assets.com/images/dialogs/test-results/.svg"></quiz-splash-modal>
</transition>
<transition name="slide-fade">
<auth-registration :scores="[0,
            0,
            0,
            0,
            0,
        ]" data-chunk="guest-chunk" invite-code="" personality="" personality-nice="Architect" v-bind:reload="true" variant="">
<template v-slot:terms="">
<div class="terms" key="registration-terms-slot">
                We'll never sell or inappropriately share your personal data. See our <a class="with-border" href="https://www.16personalities.com/terms/privacy" target="_blank">Privacy Policy</a> for more info. By continuing, you agree to our <a class="with-border" href="https://www.16personalities.com/terms" target="_blank">Terms &amp; Conditions</a>.
            </div>
</template>
</auth-registration>
</transition>
<transition name="slide-fade">
<auth-login-modal data-chunk="guest-chunk">
<p>Not a member yet? Create a free profile by taking our <a class="with-border" href="https://www.16personalities.com/free-personality-test">personality test</a> or <a class="with-border" href="https://www.16personalities.com/auth/sign-up">entering your results</a> yourself.</p>
</auth-login-modal>
</transition>
<transition name="slide-fade">
<auth-forgot-password data-chunk="guest-chunk"></auth-forgot-password>
</transition>
<app-errors></app-errors> <transition name="slide-fade">
<search-modal action="https://www.16personalities.com/search"></search-modal>
</transition>
</div>
<footer class="" id="footer">
<div class="copyright">
        ©2011-2021 NERIS Analytics Limited
    </div>
<nav class="links">
<a class="with-border" href="https://www.16personalities.com/contact-us">Contact</a>
<a class="with-border" href="https://www.16personalities.com/orders/testimonials">Testimonials</a>
<a class="with-border" href="https://www.16personalities.com/insights/evolve-survey">Help Us Evolve!</a>
<a class="with-border" href="https://www.16personalities.com/terms">Terms &amp; Conditions</a>
<a class="with-border" href="https://www.16personalities.com/terms/privacy">Privacy Policy</a>
<a class="with-border mindtrackers" href="https://mindtrackers.com" rel="external" target="_blank">Team Assessments
            <svg viewbox="0 0 576 512" xmlns="http://www.w3.org/2000/svg"><path d="M448 241.823V464c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V112c0-26.51 21.49-48 48-48h339.976c10.691 0 16.045 12.926 8.485 20.485l-24 24a12.002 12.002 0 0 1-8.485 3.515H54a6 6 0 0 0-6 6v340a6 6 0 0 0 6 6h340a6 6 0 0 0 6-6V265.823c0-3.183 1.264-6.235 3.515-8.485l24-24c7.559-7.56 20.485-2.206 20.485 8.485zM564 0H428.015c-10.658 0-16.039 12.93-8.485 20.485l48.187 48.201-272.202 272.202c-4.686 4.686-4.686 12.284 0 16.971l22.627 22.627c4.687 4.686 12.285 4.686 16.971 0l272.201-272.201 48.201 48.192c7.513 7.513 20.485 2.235 20.485-8.485V12c0-6.627-5.373-12-12-12z" fill="currentColor"></path></svg>
</a>
</nav>
<div class="social">
<a class="no-formatting" href="https://www.facebook.com/16Personalities/" rel="noreferrer" target="_blank"><span class="fab fa-facebook"></span></a>
<a class="no-formatting" href="https://www.instagram.com/16personalitiesofficial/" rel="noreferrer" target="_blank"><span class="fab fa-instagram"></span></a>
<a class="no-formatting" href="https://twitter.com/16personalities" rel="noreferrer" target="_blank"><span class="fab fa-twitter"></span></a>
</div>
<!-- version v1.1.34 -->
</footer>
<div class="svg-icons">
<svg id="spin-icon" viewbox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M460.115 373.846l-6.941-4.008c-5.546-3.202-7.564-10.177-4.661-15.886 32.971-64.838 31.167-142.731-5.415-205.954-36.504-63.356-103.118-103.876-175.8-107.701C260.952 39.963 256 34.676 256 28.321v-8.012c0-6.904 5.808-12.337 12.703-11.982 83.552 4.306 160.157 50.861 202.106 123.67 42.069 72.703 44.083 162.322 6.034 236.838-3.14 6.149-10.75 8.462-16.728 5.011z"></path></svg>
</div>
<script src="https://static.neris-assets.com/js/manifest--b50d93.js"></script>
<script src="https://static.neris-assets.com/js/vendor--c54c6e.js"></script>
<script src="https://static.neris-assets.com/js/app--4fbbcc.js"></script>
<script async="" src="https://static.neris-assets.com/js/lazyload--188595.js"></script>
<script src="https://static.neris-assets.com/js/footer--7d6489.js"></script>
<script>

    
        ga('send', 'pageview', {
            'dimension5': 'anonymous',
            'dimension11': 'en'
        });

    
</script>
</body>
</html>
