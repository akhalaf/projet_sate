<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]--><!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]--><!--[if gt IE 9]><!--><html class="no-js" itemscope="" itemtype="http://schema.org/Product" lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1" name="viewport"/>
<meta content="" name="csrf-token"/>
<title>Page not found - The One Point</title>
<link href="https://www.theonepoint.co.uk/favicon.png" rel="shortcut icon" type="image/x-icon"/>
<link href="https://www.theonepoint.co.uk/css/flexboxgrid.css" rel="stylesheet"/>
<link href="https://www.theonepoint.co.uk/css/style.css" rel="stylesheet"/>
<link href="https://www.theonepoint.co.uk/css/font-awesome.css" rel="stylesheet"/>
<link href="https://www.theonepoint.co.uk/css/menumaker.css" rel="stylesheet"/>
<link href="https://www.theonepoint.co.uk/css/nav.css" rel="stylesheet"/>
<script src="https://www.theonepoint.co.uk/js/modernizr-2.6.2.min.js"></script>
<script src="https://use.typekit.net/kvj7pss.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
<script src="https://secure.herb2warn.com/js/199226.js" type="text/javascript"></script>
<link href="https://www.theonepoint.co.uk/css/hover.css" rel="stylesheet"/>
<link href="https://www.theonepoint.co.uk/css/jquery.fullPage.css" rel="stylesheet"/>
<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet"/>
<link href="https://www.theonepoint.co.uk/css/mobile.css" rel="stylesheet"/>
</head>
<body class="layout about">
<header class="header">
<div class="overlay overlay-hugeinc" id="layer1">
<button class="overlay-close" type="button">Close</button>
<nav>
<ul></ul>
</nav>
</div>
<div class="wrap container-fluid">
<div class="row middle-xs">
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-6">
<a href="https://www.theonepoint.co.uk"><img alt="The One Point" src="https://www.theonepoint.co.uk/images/pages/global/of/top-logo.png"/></a>
</div>
<div class="col-lg-6 col-md-10 col-sm-10 col-xs-0 menu-container">
<nav class="invisible-xs menu" id="cssmenu">
<ul class="row middle-xs center-xs end-sm center-md">
<li><a href="https://www.theonepoint.co.uk">Home</a></li>
<li class="has-sub"><a href="https://www.theonepoint.co.uk/about">About</a>
<ul>
<li><a href="https://www.theonepoint.co.uk/about/intro">About Us</a></li>
<li><a href="https://www.theonepoint.co.uk/about/the-one-point-foundation">The One Point Foundation</a></li>
<li><a href="https://www.theonepoint.co.uk/about/industry-awards-accreditations">Industry Awards &amp; Accreditations</a></li>
<li><a href="https://www.theonepoint.co.uk/about/what-our-customers-say">What Our Customers Say</a></li>
<li><a href="https://www.theonepoint.co.uk/about/public-sector-and-charity">Public Sector, Charity and Schools</a></li>
<li><a href="https://www.theonepoint.co.uk/about/meet-the-team">Meet the Team</a></li>
<li><a href="https://www.theonepoint.co.uk/about/join-the-team">Join the Team</a></li>
<li><a href="https://www.theonepoint.co.uk/about/events">Events</a></li>
</ul>
</li>
<li class="has-sub"><a href="https://www.theonepoint.co.uk/services">Services</a>
<ul>
<li><a href="https://www.theonepoint.co.uk/services/it-services-support">IT Services &amp; Support</a></li>
<li><a href="https://www.theonepoint.co.uk/services/telecoms-voip">Telecoms &amp; VoIP</a></li>
<li><a href="https://www.theonepoint.co.uk/services/mobile">Mobile</a></li>
<li><a href="https://www.theonepoint.co.uk/services/digital-software-services">Digital &amp; Software Services</a></li>
<li><a href="https://www.theonepoint.co.uk/services/sage-services">Sage Services</a></li>
<li><a href="https://www.theonepoint.co.uk/services/print-information-management">Print and Information Management</a></li>
<li><a href="https://www.theonepoint.co.uk/services/offers">Offers</a></li>
</ul>
</li>
<li class="has-sub"><a href="https://www.theonepoint.co.uk/products">Products</a>
<ul>
<li><a href="https://www.theonepoint.co.uk/products/the-one-plan">The One Plan - All Inclusive Technical Support</a></li>
<li><a href="https://www.theonepoint.co.uk/products/the-one-cloud">The One Cloud - Secure Data Backup</a></li>
<li><a href="https://www.theonepoint.co.uk/products/the-one-network">The One Network - Hosted VoIP</a></li>
<li><a href="https://www.theonepoint.co.uk/services/taas">TaaS - Technology as a Service</a></li>
<li><a href="https://www.theonepoint.co.uk/products/the-one-app">The One App - Bespoke Mobile Apps</a></li>
<li><a href="https://www.theonepoint.co.uk/products/the-one-crm-erp">The One CRM / ERP</a></li>
<li><a href="https://www.theonepoint.co.uk/products/the-one-tracker">The One Tracker</a></li>
<li><a href="https://www.theonepoint.co.uk/products/lightspeed">LightSpeed in Hull</a></li>
<li><a href="https://www.theonepoint.co.uk/products/m2m-iot">M2M &amp; IoT</a></li>
<li><a href="https://www.theonepoint.co.uk/products/office-365">Microsoft 365</a></li>
<li><a href="https://www.theonepoint.co.uk/products/mobile-device-management">Mobile Device Management (MDM)</a></li>
<li><a href="https://www.theonepoint.co.uk/products/sage-services">Sage Services</a></li>
</ul>
</li>
<li class="supp_nav"><a href="https://www.theonepoint.co.uk/support">Support</a></li>
<li><a href="https://www.theonepoint.co.uk/news">News</a></li>
<li><a href="https://www.theonepoint.co.uk/blog">Blog</a></li>
<li><a href="https://www.theonepoint.co.uk/contact">Contact</a></li>
</ul>
</nav>
</div>
<div class="toggle col-xs-6 end-xs visible-xs"><a class="menu-btn trigger-overlay" data-overlay="layer1" href="#"><i class="fa fa-bars"></i></a></div>
<div class="col-xs-3 end-xs tel invisible-xs" x-ms-format-detection="none">
<div class="row middle-xs">
<div class="col-md-8" style="font-size: 12px;">
<strong>Hull:</strong> <a href="tel:01482420150">01482 420150</a><br/><strong>Gateshead:</strong> <a href="tel:01916914722">01916 914722</a>
</div>
<div class="col-md-4">
<span class="social_icons">
<a class="twitter" href="https://twitter.com/theonepoint" target="_blank"><i class="fa fa-twitter"></i></a>
<a class="facebook" href="https://www.facebook.com/theonepoint" target="_blank"><i class="fa fa-facebook"></i></a>
<a class="linkedin" href="https://www.linkedin.com/company/the-one-point" target="_blank"><i class="fa fa-linkedin"></i></a>
<a class="instagram" href="https://www.instagram.com/theonepointltd/" target="_blank"><i class="fa fa-instagram"></i></a>
</span>
</div>
</div>
</div>
</div>
<section class="hero">
<article class="row middle-xs feat_text">
<div class="wrap container-fluid">
<div class="row middle-xs">
<div class="col-xlg-5 col-lg-8 col-md-8 col-sm-11 col-xs-11">
<h1>Sorry the page cannot be found<span>.</span></h1>
</div>
</div>
</div>
</article>
</section>
<section class="benefits">
<div class="wrap container-fluid">
<div class="row">
<div class="col-xlg-5 col-lg-7 col-md-7 col-sm-12 col-xs-12">
<h2>Why work with us</h2>
<div class="toggle-group">
<h3 class="toggleme">One Single Point Of Contact (SPOC) for all your managed IT, Telecoms, Mobile and Digital Services</h3>
<div class="toggle-text">
<p>Forget the frustration of having multiple providers of technology at your business and have just one point of contact along with a dedicated team to meet all of your needs and bring greater efficiency to your business. Bespoke solutions dedicated to ensuring the most efficient running of your business.</p>
</div>
<h3 class="toggleme">Dedicated support 24 hours a day, 365 days a year</h3>
<div class="toggle-text">
<p>No matter what time the technology problem and no matter what the issue, our award-winning customer service, technical support and engineering teams provide full network management to remote backup and disaster recovery.</p>
</div>
<h3 class="toggleme">Benefit from the latest industry advances, knowledge and advice</h3>
<div class="toggle-text">
<p>We continually invest in training our teams and securing industry leading accreditations to ensure we are experts in what we do, providing our customers with very best service levels and advice, delivering all the services of an internal IT department at a fraction of the cost, and with a far greater depth of knowledge and current thinking.</p>
</div>
<h3 class="toggleme">We’re accredited by the biggest names in technology</h3>
<div class="toggle-text">
<p>Microsoft, Apple, Sage, Dell, IBM, 02, BT, KCOM, NEC, and Ofcom are all amongst the leading technology companies and organisations to have accredited our work as suppliers, giving our customers the assurance that our services meet the highest of standards.</p>
</div>
</div>
</div>
<div class="col-xlg-5 col-lg-5 col-md-5 col-sm-12 col-xs-12 col-xlg-offset-2 col-md-offset-0">
<form class="cont_form" id="workWithUsForm" method="post">
<input name="page" type="hidden" value="https://www.theonepoint.co.uk/about"/>
<ul>
<li class="field">
<input class="input floatlabel" id="name" name="name" placeholder="Name" required="" title="Please enter your name" type="text"/>
</li>
<li class="field">
<input class="email input floatlabel" id="email" name="email" placeholder="Email Address" required="" title="Please enter a valid email address" type="text"/>
</li>
<li class="field">
<input class="input floatlabel" id="tel" name="tel" placeholder="Telephone Number" required="" title="Please enter your telephone number" type="text"/>
</li>
<li class="field">
<textarea class="input textarea floatlabel" id="address" name="message" placeholder="Your Message" required="" title="Please enter your message"></textarea>
</li>
<li class="top20px">
<input class="btn white" name="Submit22" type="submit" value="Get in touch today"/>
</li>
</ul>
</form> </div>
</div>
</div>
</section>
<section class="how_work">
<div class="wrap container-fluid">
<div class="row middle-xs">
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
<h2>Our Values</h2>
</div>
</div>
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<p><img alt="we are helpful" src="https://www.theonepoint.co.uk/images/how-we-work-image1.jpg"/></p>
<h3>Helpful</h3>
<p>At The One Point we take great pride in our extraordinary level of customer service and satisfaction. We are always prepared to go the extra mile for every customer to save them money, time and become more profitable through technology. Our priority is always to delight customers with our expertise and our passion.</p>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<p><img alt="we are responsive" src="https://www.theonepoint.co.uk/images/how-we-work-image2.jpg"/></p>
<h3>Responsive</h3>
<p>We know a speedy response to each and every technical issue – no matter how big or small - is key to our customers. As such we pride ourselves on our response times – getting the job done with an average response time under 1 hour. Our Technical Support Desk is manned 24 hours by a team of qualified, full-time employees, whilst our mobile field engineers are equipped to attend your premises.</p>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<p><img alt="we are trusted" src="https://www.theonepoint.co.uk/images/how-we-work-image3.jpg"/></p>
<h3>Trusted</h3>
<p>The One Point is trusted as accredited partners by the biggest names in IT Services &amp; Support, Voice &amp; Data Communications and Digital Services. These include Microsoft, Sage, Dell, IBM, 02, BT, KCOM, NEC, Ofcom. We are an approved supplier to the NHS and in the top 5% of Microsoft Partners in the UK for IT.</p>
</div>
</div>
</div>
</section>
<section class="interested" id="large-header">
<div class="wrap container-fluid">
<div class="row center-xs">
<div class="col-xlg-5 col-lg-6 col-md-7 col-sm-12 col-xs-12 text_holder">
<h2>Let us remove your day to day troubles to let you focus on business</h2>
<p>Our experts can provide a free audit of your IT, Telecoms, Mobile and Digital needs and offer solutions to help improve your performance – call <a href="tel:01482420150">01482 420150</a></p>
<p><a class="btn white" href="https://www.theonepoint.co.uk/contact">Enquire today</a></p>
</div>
</div>
</div>
</section>
<a name="newsletter"></a>
<section class="signup">
<form id="subForm" method="post">
<div class="wrap container-fluid">
<div class="row middle-xs">
<div class="col-xs-12">
<h2>Sign up for our latest news and updates</h2>
</div>
</div>
<div class="row middle-xs">
<div class="col-xlg-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
<input name="firstName" placeholder="First Name" required="" type="text"/>
</div>
<div class="col-xlg-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
<input name="lastName" placeholder="Last Name" required="" type="text"/>
</div>
<div class="col-xlg-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
<input name="email" placeholder="Email Address" required="" type="email"/>
</div>
<div class="col-xlg-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
<button class="btn white" type="submit">Sign up</button>
</div>
</div>
</div>
</form>
</section>
<section class="footer_nav">
<div class="wrap container-fluid">
<div class="row center-xs start-xlg start-md start-sm">
<div class="col-xlg-4 col-lg-12 col-md-12 col-sm-12 col-xs-12 news_foot">
<h3>Latest News</h3>
<h4><a href="https://www.theonepoint.co.uk/news/the-one-point-take-part-in-the-pass-it-on-campaign">The One Point take part in the 'Pass IT On' Campaign</a></h4>
<h5>12th Jan 2021</h5>
<p>The One Point and Robinson Contract Services take part in the 'Pass IT On' Campaign.... <a href="https://www.theonepoint.co.uk/news/the-one-point-take-part-in-the-pass-it-on-campaign">[...]</a></p>
</div>
<div class="col-xlg-2 col-lg-3 col-md-3 col-sm-6 col-xs-12 sales_block">
<h3>Sales</h3>
<p><a href="tel:01482420150">01482 420150</a><br/>
<a href="mailto:info@theonepoint.co.uk">info@theonepoint.co.uk</a></p>
<h3 class="neghead">Technical Help</h3>
<p><a href="tel:01482420150">01482 420150</a><br/>
<a href="mailto:help@theonepoint.co.uk">help@theonepoint.co.uk</a></p>
<h3 class="neghead">Accounts</h3>
<p><a href="mailto:accounts@theonepoint.co.uk">accounts@theonepoint.co.uk</a></p>
</div>
<div class="col-xlg-2 col-lg-3 col-md-3 col-sm-6 col-xs-12">
<h3>Head Office Location</h3>
<p>The One Point Limited<br/>
          The One Point HQ<br/>
          The View<br/>
          Bridgehead Business Park<br/>
          Hessle<br/>
          Hull<br/>
          HU13 0GD<br/>
          Tel: <span><a href="tel:01482420150">01482 420 150</a></span></p>
<p></p>
</div>
<div class="col-xlg-2 col-lg-3 col-md-3 col-sm-6 col-xs-12">
<h3>Our Regional Offices</h3>
<p><a href="https://theonepoint.co.uk/contact/barnsley">Barnsley Regional Office</a></p>
<p><a href="https://theonepoint.co.uk/contact/gateshead">Gateshead Regional Office</a></p>
</div>
<div class="col-xlg-2 col-lg-3 col-md-3 col-sm-6 col-xs-12">
<h3>Links</h3>
<ul>
<li><a href="https://theonepoint.co.uk/about">About The One Point</a></li>
<li><a href="https://theonepoint.co.uk/about/the-one-point-foundation">About The One Point Foundation</a></li>
<li><a href="https://theonepoint.co.uk/finances">Finance Options</a></li>
<li><a href="https://theonepoint.co.uk/terms-conditions">Terms &amp; Conditions</a></li>
<li><a href="https://theonepoint.co.uk/site-map">Site Map</a></li>
<li><a href="https://theonepoint.co.uk/privacy-policy">Privacy Policy</a></li>
<li><a href="https://theonepoint.co.uk/customer-complaints">Customer Complaints</a></li>
<li><a href="https://theonepoint.co.uk/ofcom-regulations">Ofcom regulations</a></li>
<li><a href="https://theonepoint.co.uk/environmental-policy">Environmental Policy</a></li>
<li><a href="https://theonepoint.co.uk/anti-slavery-policy">Anti-Slavery Policy</a></li>
</ul>
</div>
</div>
</div>
</section>
<footer class="footer">
<div class="wrap container-fluid">
<div class="row middle-xs center-xs start-xlg start-md start-sm">
<div class="col-xlg-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
          © 2021 The One Point All rights reserved. <br/>Calls may be recorded for quality and training.
      </div>
<div class="col-xlg-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
<div class="row middle-xs">
<div class="col-lg-1 col-xs-2 center-xs">
<img alt="The One Point Foundation logo" src="https://www.theonepoint.co.uk/images/pages/global/footer/of/footer-the-one-point-foundation.png"/>
</div>
<div class="col-lg-2 col-xs-2 center-xs">
<img alt="IT @ Spectrum logo" src="https://www.theonepoint.co.uk/images/pages/global/footer/of/footer-spectrum.png"/>
</div>
<div class="col-lg-2 col-xs-2 end-xs">
<img alt="Microsoft logo" src="https://www.theonepoint.co.uk/images/pages/global/footer/of/footer-microsoft.png"/>
</div>
<div class="col-lg-1 col-xs-2 center-xs">
<img alt="O2 logo" src="https://www.theonepoint.co.uk/images/pages/global/footer/of/footer-o2.png"/>
</div>
<div class="col-lg-1 col-xs-4 start-xs">
<img alt="Sage logo" src="https://www.theonepoint.co.uk/images/pages/global/footer/of/footer-sage.png"/>
</div>
<div class="col-lg-4 col-xs-3 start-xs">
<img alt="Footer Accreditations" src="https://www.theonepoint.co.uk/images/microsoft-gold-logo/footer-accreditations.png"/>
</div>
</div>
</div>
</div>
</div>
</footer>
<script>
var oldieCheck = Boolean(document.getElementsByTagName('html')[0].className.match(/\soldie\s/g));
if(!oldieCheck) {
document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"><\/script>');
} else {
document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"><\/script>');
}
</script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/additional-methods.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.all.js"></script>
<script src="https://www.theonepoint.co.uk/js/mediaCheck-min.js"></script>
<script src="https://www.theonepoint.co.uk/js/fullscreenmenu.js"></script>
<script src="https://www.theonepoint.co.uk/js/classie.js"></script>
<script src="https://www.theonepoint.co.uk/js/jvFloat.js"></script>
<script src="https://www.theonepoint.co.uk/js/menumaker.js"></script>
<script src="https://www.theonepoint.co.uk/js/main.js"></script>
<script src="https://www.theonepoint.co.uk/js/jquery.vide.js"></script>
<script>
$(function() {

  $(document).on('submit', '#workWithUsForm', function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: "https://www.theonepoint.co.uk/form/workwithus",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: $(this).serialize(),
            success: function (data) {
                swal('Form Sent!','success');
                $('#workWithUsForm').trigger("reset");
            }
        });

    });

});
</script>
<script>
$(function() {
    
    $(document).on('submit', '#subForm', function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: "https://www.theonepoint.co.uk/form/newsletter",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: $(this).serialize(),
            success: function (data) {
                swal('Thank you!','You have subscribed to our newsletter.');
                $('#subForm').trigger("reset");
            }
        });

    });

});
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PB49SPJ');

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-12939721-27', 'theonepoint.co.uk');
  ga('send', 'pageview');
</script>
<script src="https://secure.leadforensics.com/js/152102.js" type="text/javascript"></script>
</div></header></body>
</html>
