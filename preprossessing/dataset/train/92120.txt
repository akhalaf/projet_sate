<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8"/>
<title>Welche Kalenderwoche ist heute? | Übersicht und Infos zur Kalenderwoche (KW)</title> <meta content="Die aktuelle Kalenderwoche für heute. ➜ Übersicht mit allen Kalenderwochen des aktuellen Jahres. ➜ KW der Vor- und Folgejahre. ➜ FAQ und mehr." name="description"/> <link href="https://www.aktuelle-kalenderwoche.org/" rel="canonical"/> <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="/css/screen.css" rel="stylesheet"/>
<link href="/img/favicons/favicon.ico" rel="shortcut icon" type="image/x-icon; charset=binary"/>
<link href="/img/favicons/favicon.ico" rel="icon" type="image/x-icon; charset=binary"/>
<link href="/img/favicons/touch_icon_ak_57.png" rel="apple-touch-icon"/>
<link href="/img/favicons/touch_icon_ak_72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/img/favicons/touch_icon_ak_114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="/img/favicons/touch_icon_ak_144.png" rel="apple-touch-icon" sizes="144x144"/>
</head>
<body>
<header id="header">
<div class="container">
<a href="https://www.aktuelle-kalenderwoche.org/"><img alt="Aktuelle Kalenderwoche" id="logo" src="/img/logo.png"/></a>
<nav id="social_nav">
<ul>
<li><a class="facebook" href="https://www.facebook.com/share.php?u=https://www.aktuelle-kalenderwoche.org/" target="_blank" title="Auf Facebook weitersagen"> </a></li>
<li><a class="twitter" href="https://twitter.com/home?status=https://www.aktuelle-kalenderwoche.org/" target="_blank" title="Auf Twitter weitersagen"> </a></li>
</ul>
</nav>
</div>
</header>
<main id="main">
<div id="title">
<div class="bg">
<div class="container">
<h2>Kalenderwochen <span class="cw">02</span></h2>
<span class="daterange">11.01.2021 - 17.01.2021</span>
</div>
</div>
</div>
<div id="pager">
<div class="container">
<div class="pager">
<div class="top">
<a class="left" href="https://www.aktuelle-kalenderwoche.org/2021/01">&lt; KW 01</a>
<a class="right" href="https://www.aktuelle-kalenderwoche.org/2021/03">KW 03 &gt;</a>
</div>
<div class="bottom">
<a class="left" href="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2021">Kalenderwoche 2021</a><span class="pipe"> | </span><span class="right"><select class="calendaryear_select" onchange="location.href=document.location.href=this.value;">
<option value="">Jahr wählen</option>
<option value="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2016">2016</option>
<option value="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2017">2017</option>
<option value="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2018">2018</option>
<option value="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2019">2019</option>
<option value="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2020">2020</option>
<option value="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2022">2022</option>
<option value="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2023">2023</option>
<option value="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2024">2024</option>
<option value="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2025">2025</option>
<option value="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2026">2026</option>
</select></span>
</div>
</div>
</div>
</div>
<div class="container">
<div id="main_content">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-7482472868197043" data-ad-format="auto" data-ad-slot="9959238002" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<span class="opener">
            Die <strong>Kalenderwoche für heute</strong> ist 02.
        <br/>Sie dauert von Montag, den 11.01.2021 bis Sonntag, den 17.01.2021.
         
</span>
<div class="row row_2_col">
<div class="box first">
<div class="csc-default" id="c6">
<div class="csc-header csc-header-n1"><h1 class="csc-firstHeader">Die Kalenderwoche</h1></div>
<p class="bodytext">Jeder kennt sie, hat schon von ihr gehört und trotzdem bleibt sie vielen fremd: Die Kalenderwoche. Und das geht schon gleich am Jahresanfang los: Wir haben den ersten Januar, also auch die erste Kalenderwoche. Richtig? Naja, kommt drauf an: In vier von sieben Fällen ist das richtig und in drei von sieben Fällen falsch.<br/><br/>Warum das so ist und, noch wichtiger, welche Kalenderwoche wir aktuell haben, für diese Antworten gibt es uns. :-)</p></div>
<div class="csc-default" id="c26">
<div class="csc-header csc-header-n2"><h1 class="csc-header-alignment-left">Was ist die Kalenderwoche?</h1></div>
<p class="bodytext">Die Kalenderwoche ist eine Woche, die mit dem Montag beginnt und mit dem Sonntag endet. Das Jahr hat insgesamt meist 52 Kalenderwochen.</p></div>
<div class="csc-default" id="c27">
<div class="csc-header csc-header-n3"><h1>Wann beginnt die erste Kalenderwoche eines Jahres?</h1></div>
<p class="bodytext">Die erste Kalenderwoche eines Jahres ist die Woche, die mindestens vier Tage des neuen Jahres beinhaltet. <br/><br/>Fällt also beispielsweise der 1. Januar auf einen Dienstag, beginnt die erste Kalenderwoche mit Montag, den 31.12., da diese Woche sechs Tage des neuen Jahres enthält (Dienstag, Mittwoch, Donnerstag, Freitag, Samstag und Sonntag). <br/><br/>Fällt der 1. Januar hingegen auf den Freitag, dann beginnt die erste Kalenderwoche des neuen Jahres mit Montag, dem 04.01., da die Vorwoche nur drei Tage des neuen Jahres enthält (Freitag, Samstag, Sonntag).</p></div>
</div>
<div class="box last">
<div class="csc-default" id="c28">
<div class="csc-header csc-header-n1"><h1 class="csc-firstHeader">Gibt es für die Kalenderwoche eine offizielle Regelung?</h1></div>
<p class="bodytext">Ja, die ISO 8601, die als ersten Wochentag den Montag festlegt.</p></div>
<div class="csc-default" id="c29">
<div class="csc-header csc-header-n2"><h1>Gilt die Regelung für die Kalenderwoche international?</h1></div>
<p class="bodytext">Ja und nein. In vielen Ländern wird diese Vorgehensweise verwendet. In zahlreichen anderen Ländern, wie beispielsweise den USA, Kanada, Mexiko und Australien, beginnt die Woche jedoch mit dem Sonntag. Ausserdem beginnt dort die erste Kalenderwoche immer mit dem 01. Januar, egal auf welchen Wochentag dieser fällt.<br/><br/>Im Mittleren Osten beginnt die Woche jedoch überwiegend mit dem Samstag. Ausserdem beginnt auch dort die erste Kalenderwoche immer mit dem 01. Januar, egal auf welchen Wochentag dieser fällt.</p></div>
<div class="csc-default" id="c30">
<div class="csc-header csc-header-n3"><h1>Gibt es eine internationale Schreibweise für die Kalenderwochen?</h1></div>
<p class="bodytext">Ja, auch die gibt es nach ISO 8601. Demnach wird die Kalenderwoche wie folgt geschrieben:<br/><br/>YYYY-Www oder YYYYWww<br/>YYYY-Www-D oder YYYYWwwD<br/><br/>Demnach würde in Deutschland Montag, der 31.12.2012 wie folgt bezeichnet: 2013-W01-1<br/>In den USA hingegen wäre es 2013-W01-2<br/><br/>Und im nahen Osten 2013-W01-3.</p></div>
</div>
</div>
</div>
</div>
</main>
<footer id="footer">
<div id="footer_container">
<div id="footer_content" style="padding-bottom:70px;">
<nav id="nav_years">
                  Alle Kalenderwochen: <ul>
<li><a href="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2016">2016</a></li>
<li><a href="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2017">2017</a></li>
<li><a href="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2018">2018</a></li>
<li><a href="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2019">2019</a></li>
<li><a href="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2020">2020</a></li>
<li><a href="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2021">2021</a></li>
<li><a href="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2022">2022</a></li>
<li><a href="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2023">2023</a></li>
<li><a href="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2024">2024</a></li>
<li><a href="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2025">2025</a></li>
<li><a href="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2026">2026</a></li>
<li><a href="https://www.aktuelle-kalenderwoche.org/kalenderwochen/2027">2027</a></li>
</ul>
</nav>
<nav id="nav_service">
<ul>
<li><a class="title" href="https://www.aktuelle-kalenderwoche.org/impressum" title="Impressum">Impressum</a></li>
<li><a class="title" href="https://www.aktuelle-kalenderwoche.org/datenschutz" title="Datenschutz">Datenschutz</a></li>
<li><a class="title" href="https://www.calendar-week.org" title="English Version">English Version</a></li>
</ul>
</nav>
</div>
</div>
</footer>
<script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
                  ga('create', 'UA-2212393-37', 'aktuelle-kalenderwoche.org');
         window.cookieconsent_options = {"message":"Cookies erleichtern die Bereitstellung unserer Dienste. Mit der Nutzung unserer Dienste erklären Sie sich damit einverstanden, dass wir Cookies verwenden.","dismiss":"Ok, danke.","learnMore":"Weitere Informationen","link":"https://www.aktuelle-kalenderwoche.org/datenschutz/"};
                  ga('set', 'anonymizeIp', true);
         ga('send', 'pageview');
         
       </script>
<script async="" src="/js/cookieconsent.latest.min.js" type="text/javascript"></script>
</body>
</html>
