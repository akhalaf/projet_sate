<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "86920",
      cRay: "6109832da92e18e8",
      cHash: "c8b763ea5eecc30",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly95b3V3YXRjaC1zZXJpZS5jb20vZ29zc2lwLWdpcmwtc3RyZWFtaW5nLWdyYXR1aXQtUUVWVlNWLmh0bWwlMDklMGE=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "7lfs4UIt3J6ulzVe6o97+6iScobvIIcpasTzk6iq0XTf3QgWj/HMlT5tW+p9LAXB22d5762bGNqtclf//CrHMyIMJ/CxOfYwDbKnW/pte/kF0KAlrbEgdp5K37smYdTYf3tP9Ks0FWvTFdcHeOOZu3hTX68swMMtaF/hAHnc8HkGBzZ6a5LdjcPhoqm/WWzqgwwzyvyDgw6KJa+CIkwAi0M9vVgipONz59q9rEA9M5krEX1WV7jnMhoDgSoySZxmYAVg1gdF79iHLmRYcY+vsfuACwTYWwF9prap6+LP9Qr75FKSpSkxHyPguVy4vrod8CduCG1Q6pBPkga23zEI16ghNAmfxCqiiqXh5U4ff501BgAAc9L1Jd6woZ0sHeibAFUzvJK6IVGFxW/MsLMQeQlsA2ahqMXyLl8t4ZJ5Bn3KyTJuaOakoTHwnItMA07zLhd4U/lIGbkpwfi4J5xQ9D8FLnqLjxzJsR9zaEX3je5AIhMUUai/5bAAI/Hy7Zg2Iwp8jtbEOJF5lKs7p97EpW+DruAFZgUpU5tmxEju0MAFsoIu7Yncftl0bOuo2baB9fFEZC9bRkvPOBqOTqIiPd3D+25Qt4nRzBAbQE1U3KSvbp8d0FXc+AmmC90gu/vGcL43rcD71uiaXXkz+tUCnN+MuY5TyKVEF/7JOkDoLh/ZExFgqFryrJlNKRhwu7RPJveGM7M27zaQ5PpMRGYx3zevSqP7u6pVfHULeWHFHQZ/YME3IXVv+mD6k2kZeHmzx8nG8WGpIpZ3aYILueWM/g==",
        t: "MTYxMDQ4MjEzNy4yMjkwMDA=",
        m: "Sk1VD32/ikJvVswmaB6ncVT73CSIO0Nz7syALdPlVsA=",
        i1: "WDxSyG1lxUSKR/4FQPC9Bg==",
        i2: "4pq0Z1JjIdnWYHeNBP3NwA==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "DVTLaHlsOSgEctTf7KV+lSPlYzb0LsWhsIuI6cMai3Q=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=6109832da92e18e8");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> youwatch-serie.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/gossip-girl-streaming-gratuit-QEVVSV.html%09%0a?__cf_chl_jschl_tk__=c1e4ab0e93c3cc836eeaf7572db7fa7948b45370-1610482137-0-Aa6c_yb6VfD6_RaQ6WhFxbThT8s8kJ_SV2799MwZFa6kOtTEl3o8eAR91fjcAIZWcxk5KTBqZsQ0_mzb0nALdzbXyhnMPkfvojzxn_TC8oYgG6B4fQa3bp8BC9pouT40cnTYqTIcx-qOLpecfwFrpm82nQUI8sGmkm0EFyrTu1fGcBZlDoBUXOLe-Pj_jhrqM2H0RYYf-DvQTF6GjA4wVX95IKNX1qWSINLGPHgZkvMxyiAUtdbDfuaPEV_K3msAuOORc6-AqV_Wci3U2BgPnpXgClowLarMFA1mH_QYFOwMB0JPZajVSQdhaLq9EvbmNI5eqnaX-yFuYI-SfDzDn8QL8t4H515RulDfW9SXBrN0uA_acPy8n6N3pWz_-zkl3smdywWTAiwbVmfqE2Em90M" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="75c40d5a35c2a63c45efd6f82401703c6d2d29cb-1610482137-0-ASGP+Qu5FULD13XFXJG3CZ5+64B6FNdrRCQqNn74h6neElPA53r7yjkDNObcJ/CDJSAkOPhOxrjPC8PJ1DQEM5PHlIZ2NI90DW/QcS+5ys1wMhP2f20eMuhTlgDNCPtHV3dVz0CtcUgPr9cEdRtfSlhg0efCzH35SzzNW5f2AYMaWZC2aC1Ip2DFE+/I/lUYsTcEft2EuPCKhlTjV2MLGIPpRZcHsgY7YCGq7aPkM0iXS5QryYBIuDP4IfbZauPJKSvIhh1+xeqVePWLrPg9Py5Zl9RODTy842VxE5cIYxoOjG0nY2VT9ExJwikxBfNVZzuyVWmKrPgncWk19/Ginz1RLByFB5V+pBgUrh1lvVyFXiuJx49cRp9jM6KrlvjQxDri/esaPNapJAzqjPY60iVMmrg0Fcf7iKqEzudKAk5RBB5cuDSiB+mQ1CyuKs5r/zk5GPwyX5l2QUV4nZT/Y7B8wOT/+erX8rt6+yEJ1/jbZR0mjjMUrN6NqBa54SkZR27UBoRTPnVX37pidYe1XhEK9IzI/jHfpOyeF6Z83PfOoMql7CoSYinFWc7Ufp5cgDhByuUP109AhJy2anQfZq4cptYECxRkI1gJ/3DEHJ/ci32s3K8UOPOqNNliEL1diE7dB8qSYIP6vrEV2pxjgFp+KOAaqYJwotM2zr7ZxZLC4n0QY43gsSD/lWfKRSpQ7ch3FiSJYrms/UiwfxJivtWdIq4UuITzZRjTZvtPDRC7APWxelbyQsg4MsqmQM+rMD1UhxjJW7UDMgf2iPzwNA/7DUR2nxN7IgBr0DOKBpd8vfTTw8WHDhQR1CZUOKXDEA6p+48yM2wBwxfrpMca+CxmrlgHS8c3eX5Z2XyVQmn6enno+e8YZMwQT8AJeu6Z2lsRUAHJZS7hig+3fZ8zHBfU94zkp55toLmJUMkRR0L6hX0s5AG5UM4qKwbJ2Ok8cJNlPismljDlr+sOsFRSgv9VsbCu/9RlY4n0/BA4x8TeFQfNBHLwy4rqjF3xDKDVohXUXmYRyx8721JKCiVJ7au5Q4IE48/LpnFM90F3s4/kj7UOpvW6vcn+OfGlbSDaMeLhqNBHdlQHz0L3Zxvm++quOX0FFRBoImhysglO/ksJC4JwGCGoDFXFa5cACmm+4xnThEBixnAKiT1zOwAY3Lh/2S9UhEDCOadOs7gIJt+AzJTGIhoRm3atLJXWtmrktvQmGvEZ4FiXhT93bqs/BSw3ZJOQvSivIMVNEzavoH2K9Uim5It0K3dNsk9tikQLLnpmAUYtgFxMLnhcfHl2Cm0cqMMYW6Fe8t+C0iWeD5yMEoUIOU6p2fyXwEWffZeunw=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="15a363dc5f5f65ad2d7799e8486e7d0f"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610482141.229-dQ3m2+/mZp"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=6109832da92e18e8')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>6109832da92e18e8</code></span>
</div>
</td>
<a href="https://derchris.net/fungoidintensity.php?goto=175" style="position: absolute; top: -250px; left: -250px;"></a>
</tr>
</table>
</body>
</html>
