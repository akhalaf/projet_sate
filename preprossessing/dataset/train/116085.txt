<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>American Electric</title>
<meta content="At American Electric, we do it all. Our Construction group can tackle the full range of electrical construction projects, with design/build capabilities for both commercial and industrial work." name="description"/>
<meta content="HVAC,SCADA,TEGG,electrical contractor" name="keywords"/>
<link href="css/screen.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="images/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="images/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="images/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="images/site.webmanifest" rel="manifest"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-164706670-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-164706670-1');
</script>
</head>
<body id="homepage">
<div id="header">
<div id="logo"><img alt="American Electric" height="85" src="images/header.jpg" width="1024"/></div>
<div id="header_photo"><img alt="crane tech" height="146" src="images/hero_shots/homepage.jpg" width="1024"/></div>
</div>
<div id="page">
<div id="copy_column">
<!-- <div id="emailbox_index">
<div align="right">
<table width="170" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#006699" valign="top">
<td width="9" rowspan="2"><img src="https://img.constantcontact.com/ui/images/visitor/tl_brdr2_trans.gif" width="9" height="9" border="0"></td>
<td width="152" height="1" bgcolor="#006699"><img src="https://img.constantcontact.com/ui/images/spacer.gif" border="0" width="1" height="1"></td>
<td width="9" rowspan="2" align="right"><img src="https://img.constantcontact.com/ui/images/visitor/tr_brdr2_trans.gif" width="9" height="9" border="0"></td>
</tr>
<tr>
<td width="152" height="8" bgcolor="#ffffff"><img src="https://img.constantcontact.com/ui/images/spacer.gif" border="0" width="1" height="1"></td>
</tr>
<tr>
<td width="170" colspan="3" style="border-left: 1px solid #006699;border-right: 1px solid #006699;"><div align="center">
<form name="ccoptin" action="https://visitor.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:3;">
<font style="font-weight: bold; font-family:Arial; font-size:18px; color:#006699;">Join Our Email List</font><br>
<font style="font-weight: normal; font-family:Arial; font-size:10px; color:#000000;">Email:</font> <input type="text" name="ea" size="14" value="" style="font-family: Arial; font-size:10px; border:1px solid #999999;"> <input type="submit" name="go" value="Join" class="submit" style="font-family:Arial,Helvetica,sans-serif; font-size:11px;">
<input type="hidden" name="m" value="1103050552414">
<input type="hidden" name="p" value="oi">
</form>
</div></td>
</tr>
<tr bgcolor="#006699" valign="bottom">
<td rowspan="2"><img src="https://img.constantcontact.com/ui/images/visitor/bl_brdr2_trans.gif" width="9" height="9" border="0" /></td>
<td width="152" height="8" bgcolor="#ffffff"><img src="https://img.constantcontact.com/ui/images/spacer.gif" border="0" width="1" height="1"></td>
<td rowspan="2" align="right"><img src="https://img.constantcontact.com/ui/images/visitor/br_brdr2_trans.gif" width="9" height="9" border="0"></td>
</tr>
<tr>
<td width="152" bgcolor="#006699"><img src="https://img.constantcontact.com/ui/images/spacer.gif" border="0" width="1" height="1"></td>
</tr>
</table>
</div>

<div align="right" style="padding-top:5px;">
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://img.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt=""/></a>
</div>
</div>
 -->
<div id="index_text">
<h1><span class="head">Aloha</span></h1>
<p>American Electric Company, LLC has been active in various phases of electrical construction for industrial plants, petrochemical plants, power generation plants, governmental and military facilities, commercial buildings, hotels, medical facilities, co-gen systems, and industrial and commercial controls. We have performed work on private, Federal, State, and City projects.</p></div>
<p>American Electric is one of the largest contractors in the state and is a   signatory  to IBEW Local 1186, Electrical Worker's Union in Honolulu. We can take on and successfully complete an extremely broad range of electrical construction projects including design build projects.</p>
<p><img alt="Waikiki Skyline" height="244" src="images/photos/Homepage/Waikiki_skyline_night.jpg" width="600"/></p>
</div>
<div id="nav_column"><!-- #BeginLibraryItem "/Library/nav_left.lbi" -->
<div align="right"> <br/>
<a href="tegg.htm"><img alt="TEGG" border="0" height="36" src="images/logo_tegg.gif" width="94"/></a> <br/>
<ul id="nav_primary">
<li><a href="index.htm" id="tab_homepage">Home</a></li>
<li><a href="construction.htm" id="tab_construction">Construction</a></li>
<li><a href="design-build.htm" id="tab_build">Design / Build</a></li>
<li><a href="high-voltage.htm" id="tab_voltage">High Voltage</a></li>
<li><a href="controls.htm" id="tab_controls">Controls</a></li>
<li><a href="facility-services.htm" id="tab_services">Facility Services</a></li>
<li><a href="telecom-services.htm" id="tab_telecom">Telecom Services</a></li>
<li><a href="alternative-energy.htm" id="tab_energy">Alternative Energy</a></li>
<li><a href="residential.htm" id="tab_residential">Residential Services</a></li>
<li><a href="experience.htm" id="tab_experience">Experience</a></li>
<li><a href="contact.htm" id="tab_contact">Contact</a></li>
</ul>
<ul id="nav_secondary">
<li> <a href="links.htm" id="tab_links">Links</a></li>
<li><a href="employment.htm" id="tab_employment">Employment</a></li>
</ul>
</div>
<!-- #EndLibraryItem --></div>
<br clear="all"/>
<div id="footer"> </div>
</div>
</body>
</html>
