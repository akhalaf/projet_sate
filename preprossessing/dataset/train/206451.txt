<!DOCTYPE html>
<html lang="ru">
<head>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="origin-when-cross-origin" name="referrer"/>
<style>
    /* open-sans-regular - cyrillic_latin */
    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: local('Open Sans Regular'), local('OpenSans-Regular'),
            url('/resources/fonts/open-sans-v17-cyrillic_latin-regular.woff2') format('woff2'), 
            url('/resources/fonts/open-sans-v17-cyrillic_latin-regular.woff') format('woff');
    }

    /* lora-700 - cyrillic_latin */
    @font-face {
        font-family: 'Lora';
        font-style: normal;
        font-weight: 700;
        font-display: swap;
        src: local('Lora Bold'), local('Lora-Bold'),
            url('/resources/fonts/lora-v14-cyrillic_latin-700.woff2') format('woff2'),
            url('/resources/fonts/lora-v14-cyrillic_latin-700.woff') format('woff');
    }
    </style>
<link crossorigin="" href="https://pagead2.googlesyndication.com" rel="dns-prefetch"/>
<base href="https://beginpc.ru/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="обучение компьютеру, компьютер для начинающих, компьютерная грамотность, основы работы на компьютере, компьютер для чайников" name="keywords"/>
<meta content="Основы работы на компьютере для новичков и чайников. Простое объяснение сложных вопросов для начинающих пользователей компьютера." name="description"/>
<title>Главная | BeginPC.ru</title>
<link href="/?format=feed&amp;type=rss" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="/?format=feed&amp;type=atom" rel="alternate" title="Atom 1.0" type="application/atom+xml"/>
<link href="https://beginpc.ru/" rel="canonical"/>
<link href="/templates/livecomp/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/templates/livecomp/css/new.css?v=4" rel="stylesheet" type="text/css"/>
<link href="/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="/apple-touch-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/favicon-196x196.png" rel="icon" sizes="196x196" type="image/png"/>
<link href="/favicon-160x160.png" rel="icon" sizes="160x160" type="image/png"/>
<link href="/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<meta content="#2b5797" name="msapplication-TileColor"/>
<meta content="/mstile-144x144.png" name="msapplication-TileImage"/>
<script async="" data-ad-client="ca-pub-4391180145495977" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<body>
<!--LiveInternet counter-->
<script type="text/javascript"><!--
        new Image().src = "//counter.yadro.ru/hit?r"+
        escape(document.referrer)+((typeof(screen)=="undefined")?"":
        ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
        screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
        ";h"+escape(document.title.substring(0,80))+
        ";"+Math.random();//--></script><!--/LiveInternet--><div class="container">
<a class="skiptocontent" href="#content">Перейти к основному содержанию</a>
<header itemscope="" itemtype="http://schema.org/WPHeader"><meta content="Главная" itemprop="headline"/>
<meta content="Основы работы на компьютере для новичков и чайников. Простое объяснение сложных вопросов для начинающих пользователей компьютера." itemprop="description"/>
<meta content="обучение компьютеру, компьютер для начинающих, компьютерная грамотность, основы работы на компьютере, компьютер для чайников" itemprop="keywords"/>
<div id="icons-head">
<a aria-label="Поиск" href="/search" title="Поиск">
<svg class="icon-header" height="25" viewbox="0 0 25 25" width="25" xmlns="http://www.w3.org/2000/svg"><g><path d="m24.2143,21.26622l-5.91616,-5.03179c-0.61159,-0.55042 -1.26567,-0.80311 -1.79408,-0.77872c1.3966,-1.63583 2.2402,-3.75816 2.2402,-6.07773c0,-5.17286 -4.19341,-9.36627 -9.36627,-9.36627c-5.17282,0 -9.36627,4.1934 -9.36627,9.36627s4.1934,9.36627 9.36627,9.36627c2.31956,0 4.44191,-0.84359 6.07773,-2.2402c-0.02439,0.52837 0.2283,1.1824 0.77872,1.79398l5.03173,5.91621c0.8616,0.95726 2.26897,1.03796 3.12741,0.17943c0.85857,-0.85853 0.77799,-2.26591 -0.17928,-3.12746zm-14.83631,-5.64406c-3.44855,0 -6.24418,-2.79568 -6.24418,-6.24418c0,-3.44854 2.79563,-6.24418 6.24418,-6.24418c3.4485,0 6.24418,2.79564 6.24418,6.24418c0,3.4485 -2.79559,6.24418 -6.24418,6.24418z"></path></g></svg></a>
<a aria-label="Авторизация" href="/login" title="Войти">
<svg class="icon-header" height="25" viewbox="0 0 25 25" width="25" xmlns="http://www.w3.org/2000/svg"><g><path d="m12.5,0.00001c-4.79359,0 -8.70624,3.57036 -8.70624,7.9446c0,2.7339 1.53126,5.1664 3.84785,6.59685c-4.43663,1.73627 -7.5791,5.75275 -7.5791,10.42729l2.4875,0c0,-5.02856 4.43935,-9.07954 9.94999,-9.07954c5.51065,0 9.94999,4.05099 9.94999,9.07954l2.4875,0c0,-4.67454 -3.14247,-8.69102 -7.5791,-10.42729c2.31659,-1.43046 3.84785,-3.86296 3.84785,-6.59685c0,-4.37424 -3.91265,-7.9446 -8.70625,-7.9446zm0,2.26989c3.44925,0 6.21875,2.52721 6.21875,5.67472c0,3.1475 -2.7695,5.67471 -6.21875,5.67471c-3.44925,0 -6.21875,-2.52721 -6.21875,-5.67471c0,-3.1475 2.76949,-5.67472 6.21875,-5.67472z"></path></g></svg></a>
<a aria-label="Подписаться на RSS" href="http://feeds.feedburner.com/beginpc" rel="noopener noreferrer nofollow" target="_blank" title="Подписаться на канал новостей">
<svg class="icon-header" height="25" viewbox="0 0 25 25" width="25" xmlns="http://www.w3.org/2000/svg"><g><path d="m12.47705,25.01265c-6.93896,0.02508 -12.63363,-5.74991 -12.47811,-12.79423c0.15052,-6.72327 5.71979,-12.29754 12.65875,-12.21723c6.85874,0.08025 12.51827,5.79 12.3427,12.83438c-0.17063,6.70312 -5.69468,12.21723 -12.52334,12.17707zm10.99806,-12.49319c-0.04016,-6.15127 -4.90195,-10.92275 -10.74718,-11.04822c-6.05088,-0.12541 -11.19867,4.73639 -11.19867,11.03821c0,6.16128 4.91699,10.91768 10.75215,11.03815c6.03087,0.1204 11.13847,-4.73139 11.1937,-11.02814z"></path><path d="m5.26214,8.32999c0,-0.9332 0,-1.84139 0,-2.75452c7.11966,-0.26088 14.32954,6.04087 14.27938,14.54523c-0.90312,0 -1.80624,0 -2.72943,0c-0.06023,-3.19097 -1.13392,-5.97564 -3.35159,-8.26856c-2.22768,-2.29792 -4.97219,-3.45692 -8.19835,-3.52216z"></path><path d="m11.95529,20.14078c-0.03509,-3.77304 -3.04552,-6.84867 -6.69315,-6.85367c0,-0.91313 0,-1.83132 0,-2.74444c4.71631,-0.21579 9.5079,4.07908 9.40751,9.59812c-0.44652,0 -0.89305,0 -1.34464,0s-0.89812,0 -1.36972,0z"></path><path d="m9.05025,18.18406c0.005,1.08376 -0.84792,1.95172 -1.90159,1.94672c-1.0436,-0.005 -1.89656,-0.86803 -1.90656,-1.93171c-0.00504,-1.07368 0.84792,-1.94672 1.9116,-1.94165c1.05864,0.005 1.89152,0.84789 1.89656,1.92664z"></path></g></svg></a>
</div>
<div class="header-inner">
<a class="brand" href="/">
<span class="site-title" title="Компьютер для новичков">BeginPC.ru</span>
</a>
<p>Компьютер для новичков и не только</p>
</div>
</header><nav class="navigation" itemscope="" itemtype="http://schema.org/SiteNavigationElement"><div class="navbar pull-left">
<a class="btn btn-navbar collapsed" data-target=".nav-collapse" data-toggle="collapse">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</a>
</div>
<div class="nav-collapse">
<ul class="nav menu nav-pills mod-list">
<li class="item-101 default current active"><a href="/" itemprop="url">Главная</a></li>
<li class="item-102"><a href="/hardware" itemprop="url">Комплектующие</a></li>
<li class="item-103"><a href="/windows" itemprop="url">Windows</a></li>
<li class="item-104"><a href="/internet" itemprop="url">Интернет</a></li>
<li class="item-105"><a href="/raznoe" itemprop="url">Разное</a></li>
<li class="item-106"><a href="/test-computer" itemprop="url">О компьютере</a></li>
</ul>
</div>
</nav><main id="content"><div class="wrapper">
<div id="system-message-container">
</div>
<div class="blog" itemscope="" itemtype="https://schema.org/Blog">
<div class="category-desc clearfix">
<p>Современный мир уже невозможно представить без компьютеров и других новых электронных устройств, которые появляются практически каждый день. Количество людей ими пользующихся так же стремительно растет, охватывая все более широкие слои населения. Компьютерная грамотность становится важным навыком, без которого сложно устроится на хорошую работу. Молодым обучение компьютеру дается легко, что нельзя сказать про людей уже в возрасте. Этот ресурс посвящен компьютерам и ноутбукам с операционной системой Windows, как наиболее распространенным на сегодняшний день, однако часть информации актуальна и для других операционных систем.</p>
<p>Данный сайт предназначен в первую очередь для начинающих пользователей компьютера в не зависимости от их возраста, которые делают свои первые неуверенные шаги в его освоении. Ведь именно новички испытывают наибольшие трудности в самых простых повседневных операциях в ОС Windows. Здесь вы найдете подробные уроки с картинками для начинающих изучать компьютер самостоятельно.</p>
<p>С другой стороны сайт будет полезен и более грамотным пользователям компьютера, поскольку на нем можно найти описание более серьезных проблем, с которыми вы можете столкнуться в процессе освоения компьютера. Так же его можно использовать, как онлайн справочник, если у вас возникли трудности в каком-либо вопросе.</p> </div>
<div class="items-leading clearfix">
<div class="leading-0" itemprop="blogPost" itemscope="" itemtype="https://schema.org/BlogPosting">
<div class="page-header">
<h1 itemprop="headline">
<a href="/windows/what-is-environment-variables-windows" itemprop="url">
						Что такое переменные среды Windows					</a>
</h1>
<dd itemprop="author" itemscope="" itemtype="https://schema.org/Person">
<meta content="BeginPC" itemprop="name"/>
</dd>
<dd itemprop="publisher" itemscope="" itemtype="http://schema.org/Organization">
<span itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
<link href="/images/logo_site.png" itemprop="url image"/></span>
<meta content="BeginPC.ru" itemprop="name"/>
<meta content="" itemprop="telephone"/>
<meta content="Россия" itemprop="address"/>
</dd>
<time datetime="2020-06-01T15:28:00+03:00" itemprop="datePublished"></time><time datetime="2020-06-01T15:30:18+03:00" itemprop="dateModified"></time>
</div>
<dl class="article-info muted">
<dt class="article-info-term">
</dt>
<dd class="category-name">
																		Категория: <a href="/windows" itemprop="genre">Windows</a> </dd>
</dl>
<div class="begin">
<img alt="переменные окружения Windows" class="caption" height="113" itemprop="image" loading="lazy" src="/images/windows/10/environment_variables_small.jpg" title="Переменные среды" width="150"/>
</div>
<div itemprop="articleBody">
<p>В операционных системах широко используются переменные для удобного доступа и хранения различных данных. Они оказываются очень удобными при составлении различных сценариев, работе в командной строке и находят применение в том числе в Проводнике Windows.</p>
</div>
</div>
<div class="leading-1" itemprop="blogPost" itemscope="" itemtype="https://schema.org/BlogPosting">
<div class="page-header">
<h1 itemprop="headline">
<a href="/windows/10-samykh-poleznykh-komand-komandnoy-stroki-windows" itemprop="url">
						10 самых полезных команд командной строки Windows					</a>
</h1>
<dd itemprop="author" itemscope="" itemtype="https://schema.org/Person">
<meta content="BeginPC" itemprop="name"/>
</dd>
<dd itemprop="publisher" itemscope="" itemtype="http://schema.org/Organization">
<span itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
<link href="/images/logo_site.png" itemprop="url image"/></span>
<meta content="BeginPC.ru" itemprop="name"/>
<meta content="" itemprop="telephone"/>
<meta content="Россия" itemprop="address"/>
</dd>
<time datetime="2020-05-07T14:56:57+03:00" itemprop="datePublished"></time><time datetime="2020-05-07T14:56:57+03:00" itemprop="dateModified"></time>
</div>
<dl class="article-info muted">
<dt class="article-info-term">
</dt>
<dd class="category-name">
																		Категория: <a href="/windows" itemprop="genre">Windows</a> </dd>
</dl>
<div class="begin">
<img alt="командная строка" class="caption" height="150" itemprop="image" loading="lazy" src="/images/windows/cmd_small.jpg" title="CMD" width="150"/>
</div>
<div itemprop="articleBody">
<p>Несмотря на возможности графического интерфейса операционной системы, командная строка все еще остается востребованной даже среди начинающих пользователей компьютера, не говоря уже о продвинутых пользователях и администраторах. Поэтому мы рассмотрим 10 команд, которые наиболее востребованы и которые должен знать каждый владелец компьютера в независимости от его опыта.</p>
</div>
</div>
<div class="leading-2" itemprop="blogPost" itemscope="" itemtype="https://schema.org/BlogPosting">
<div class="page-header">
<h1 itemprop="headline">
<a href="/internet/udalennaya-rabota-za-i-protiv" itemprop="url">
						Удаленная работа: за и против					</a>
</h1>
<dd itemprop="author" itemscope="" itemtype="https://schema.org/Person">
<meta content="BeginPC" itemprop="name"/>
</dd>
<dd itemprop="publisher" itemscope="" itemtype="http://schema.org/Organization">
<span itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
<link href="/images/logo_site.png" itemprop="url image"/></span>
<meta content="BeginPC.ru" itemprop="name"/>
<meta content="" itemprop="telephone"/>
<meta content="Россия" itemprop="address"/>
</dd>
<time datetime="2020-04-26T13:26:53+03:00" itemprop="datePublished"></time><time datetime="2020-04-26T13:26:53+03:00" itemprop="dateModified"></time>
</div>
<dl class="article-info muted">
<dt class="article-info-term">
</dt>
<dd class="category-name">
																		Категория: <a href="/internet" itemprop="genre">Интернет</a> </dd>
</dl>
<div class="begin">
<img alt="дистанционная занятость" class="caption" height="113" itemprop="image" loading="lazy" src="/images/internet/chelovek_za_computerom_doma_small.jpg" title="Работа из дома" width="150"/>
</div>
<div itemprop="articleBody">
<p>Постепенно набирает популярность удаленная работа через интернет охватывающая все больше профессий. Последние события с распространением коронавируса только подстегнули этот процесс. Какие есть преимущества для сотрудника работать из дома и какие могут быть негативные моменты с этим связанные. На что обратить внимание при принятия решения.</p>
</div>
</div>
<div class="leading-3" itemprop="blogPost" itemscope="" itemtype="https://schema.org/BlogPosting">
<div class="page-header">
<h1 itemprop="headline">
<a href="/internet/spisok-onlayn-kursov-dlya-samoizolyacii" itemprop="url">
						Как провести время на самоизоляции с пользой					</a>
</h1>
<dd itemprop="author" itemscope="" itemtype="https://schema.org/Person">
<meta content="BeginPC" itemprop="name"/>
</dd>
<dd itemprop="publisher" itemscope="" itemtype="http://schema.org/Organization">
<span itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
<link href="/images/logo_site.png" itemprop="url image"/></span>
<meta content="BeginPC.ru" itemprop="name"/>
<meta content="" itemprop="telephone"/>
<meta content="Россия" itemprop="address"/>
</dd>
<time datetime="2020-04-07T20:11:37+03:00" itemprop="datePublished"></time><time datetime="2020-04-26T15:00:56+03:00" itemprop="dateModified"></time>
</div>
<dl class="article-info muted">
<dt class="article-info-term">
</dt>
<dd class="category-name">
																		Категория: <a href="/internet" itemprop="genre">Интернет</a> </dd>
</dl>
<div class="begin">
<img alt="онлайн курсы" class="caption" height="100" itemprop="image" loading="lazy" src="/images/internet/online_learn_small.png" title="Дистанционное образование" width="150"/>
</div>
<div itemprop="articleBody">
<p>Оказавшись дома на самоизоляции многие люди задают вопросом, чем заняться. Это отличная возможность заняться самообразованием, на которое часто не хватает времени и сил. Список из 50 площадок с онлайн-курсами на любой вкус многие из которых с бесплатным доступом поможет вам сделать первый шаг и углубить свои профессиональные знания или получить новую профессию.</p>
</div>
</div>
<div class="leading-4" itemprop="blogPost" itemscope="" itemtype="https://schema.org/BlogPosting">
<div class="page-header">
<h1 itemprop="headline">
<a href="/windows/keyboard-shortcuts-windows-for-text" itemprop="url">
						Комбинации клавиш для работы с текстом					</a>
</h1>
<dd itemprop="author" itemscope="" itemtype="https://schema.org/Person">
<meta content="BeginPC" itemprop="name"/>
</dd>
<dd itemprop="publisher" itemscope="" itemtype="http://schema.org/Organization">
<span itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
<link href="/images/logo_site.png" itemprop="url image"/></span>
<meta content="BeginPC.ru" itemprop="name"/>
<meta content="" itemprop="telephone"/>
<meta content="Россия" itemprop="address"/>
</dd>
<time datetime="2020-03-23T19:49:57+03:00" itemprop="datePublished"></time><time datetime="2020-03-23T19:49:57+03:00" itemprop="dateModified"></time>
</div>
<dl class="article-info muted">
<dt class="article-info-term">
</dt>
<dd class="category-name">
																		Категория: <a href="/windows" itemprop="genre">Windows</a> </dd>
</dl>
<div class="begin">
<img alt="комбинации клавиш windows" class="caption" height="250" itemprop="image" loading="lazy" src="/images/windows/hot_keys.png" title="Горячие клавиши" width="270"/>
</div>
<div itemprop="articleBody">
<p>Использование мыши очень облегчает работу за компьютером, однако есть ситуации, когда удобней и быстрее воспользоваться помощью клавиатуры, чем искать нужный пункт в меню программы. Особенно это заметно при работе с текстом, тем более если речь при этом идет о ноутбуке без мыши. Поэтому существует целый список сочетаний клавиш для текста, облегчающий работу с ним.</p>
</div>
</div>
<div class="leading-5" itemprop="blogPost" itemscope="" itemtype="https://schema.org/BlogPosting">
<div class="page-header">
<h1 itemprop="headline">
<a href="/internet/setting-up-content-security-policy" itemprop="url">
						Настройка Content Security Policy на сайте					</a>
</h1>
<dd itemprop="author" itemscope="" itemtype="https://schema.org/Person">
<meta content="BeginPC" itemprop="name"/>
</dd>
<dd itemprop="publisher" itemscope="" itemtype="http://schema.org/Organization">
<span itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
<link href="/images/logo_site.png" itemprop="url image"/></span>
<meta content="BeginPC.ru" itemprop="name"/>
<meta content="" itemprop="telephone"/>
<meta content="Россия" itemprop="address"/>
</dd>
<time datetime="2020-03-08T00:39:04+03:00" itemprop="datePublished"></time><time datetime="2020-03-15T01:06:45+03:00" itemprop="dateModified"></time>
</div>
<dl class="article-info muted">
<dt class="article-info-term">
</dt>
<dd class="category-name">
																		Категория: <a href="/internet" itemprop="genre">Интернет</a> </dd>
</dl>
<div class="begin">
<img alt="CSP" class="caption" height="183" itemprop="image" loading="lazy" src="/images/internet/content_security_policy_small.png" title="Защита от XSS" width="150"/>
</div>
<div itemprop="articleBody">
<p>В интернете много угроз и безопасность сайта имеет важное значение. Одной из разновидностью угроз, применяемых злоумышленниками, является внедрение вредоносного контента или по-другому межсайтовые скриптовые атаки, сокращенно XSS. Есть разные способы защиты от них, в том числе применяется политика безопасного контента (CSP) позволяющая существенно осложнить жизнь атакующих. Рассмотрим примеры настройки для разных ситуаций.</p>
</div>
</div>
<div class="leading-6" itemprop="blogPost" itemscope="" itemtype="https://schema.org/BlogPosting">
<div class="page-header">
<h1 itemprop="headline">
<a href="/windows/kak-udalit-znachok-set-iz-oblasti-navigatsii-provodnika-windows" itemprop="url">
						Как удалить значок Сеть из области навигации Проводника Windows					</a>
</h1>
<dd itemprop="author" itemscope="" itemtype="https://schema.org/Person">
<meta content="BeginPC" itemprop="name"/>
</dd>
<dd itemprop="publisher" itemscope="" itemtype="http://schema.org/Organization">
<span itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
<link href="/images/logo_site.png" itemprop="url image"/></span>
<meta content="BeginPC.ru" itemprop="name"/>
<meta content="" itemprop="telephone"/>
<meta content="Россия" itemprop="address"/>
</dd>
<time datetime="2020-02-28T01:19:08+03:00" itemprop="datePublished"></time><time datetime="2020-03-13T15:40:09+03:00" itemprop="dateModified"></time>
</div>
<dl class="article-info muted">
<dt class="article-info-term">
</dt>
<dd class="category-name">
																		Категория: <a href="/windows" itemprop="genre">Windows</a> </dd>
</dl>
<div class="begin">
<img alt="иконки быстрого доступа в области навигации проводника windows" class="caption" height="100" itemprop="image" loading="lazy" src="/images/windows/not_network_icon_explorer_small.png" title="Отсутсвует значок сеть" width="150"/>
</div>
<div itemprop="articleBody">
<p>Многие пользователи компьютера хотят настроить внешний вид Проводника Windows под себя. Например, удалить ненужные им значки в панели навигации, чтобы они не отвлекали внимание. Хотя графический интерфейс не предоставляет такой возможности, это можно сделать с помощью соответствующих настроек реестра. Вашему вниманию предлагается пошаговая инструкция позволяющая добиться нужного эффекта.</p>
</div>
</div>
<div class="leading-7" itemprop="blogPost" itemscope="" itemtype="https://schema.org/BlogPosting">
<div class="page-header">
<h1 itemprop="headline">
<a href="/windows/software-reporter-tool-gruzit-protsessor-kompyutera" itemprop="url">
						Software Reporter Tool грузит процессор компьютера					</a>
</h1>
<dd itemprop="author" itemscope="" itemtype="https://schema.org/Person">
<meta content="BeginPC" itemprop="name"/>
</dd>
<dd itemprop="publisher" itemscope="" itemtype="http://schema.org/Organization">
<span itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
<link href="/images/logo_site.png" itemprop="url image"/></span>
<meta content="BeginPC.ru" itemprop="name"/>
<meta content="" itemprop="telephone"/>
<meta content="Россия" itemprop="address"/>
</dd>
<time datetime="2020-02-21T00:47:06+03:00" itemprop="datePublished"></time><time datetime="2020-03-13T15:47:33+03:00" itemprop="dateModified"></time>
</div>
<dl class="article-info muted">
<dt class="article-info-term">
</dt>
<dd class="category-name">
																		Категория: <a href="/windows" itemprop="genre">Windows</a> </dd>
</dl>
<div class="begin">
<img alt="Загрузка центрального процессора software_reporter_tool.exe " class="caption" height="248" itemprop="image" loading="lazy" src="/images/windows/software_reporter_tool_taskmanager.png" title="Диспетчер задач" width="265"/>
</div>
<div itemprop="articleBody">
<p>Ваш компьютер стал вдруг тормозить и с трудом откликается на ваши действия, а в диспетчере задач Windows видно процесс с именем software_reporter_tool.exe очень сильно загружающий процессор? Самое время, разобраться что происходит и откуда он взялся, а также как можно избежать повторения подобного поведения в будущем.</p>
</div>
</div>
<div class="leading-8" itemprop="blogPost" itemscope="" itemtype="https://schema.org/BlogPosting">
<div class="page-header">
<h1 itemprop="headline">
<a href="/windows/review-windows-10x-for-dual-screen-devices" itemprop="url">
						Подробности будущей Windows 10X для устройств с двумя экранами					</a>
</h1>
<dd itemprop="author" itemscope="" itemtype="https://schema.org/Person">
<meta content="BeginPC" itemprop="name"/>
</dd>
<dd itemprop="publisher" itemscope="" itemtype="http://schema.org/Organization">
<span itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
<link href="/images/logo_site.png" itemprop="url image"/></span>
<meta content="BeginPC.ru" itemprop="name"/>
<meta content="" itemprop="telephone"/>
<meta content="Россия" itemprop="address"/>
</dd>
<time datetime="2020-02-13T00:25:16+03:00" itemprop="datePublished"></time><time datetime="2020-02-13T00:25:16+03:00" itemprop="dateModified"></time>
</div>
<dl class="article-info muted">
<dt class="article-info-term">
</dt>
<dd class="category-name">
																		Категория: <a href="/windows" itemprop="genre">Windows</a> </dd>
</dl>
<div class="begin">
<img alt="Устройство с Windows 10X" class="caption" height="138" itemprop="image" loading="lazy" src="/images/windows/10x/device_with_windows10x_small.jpg" title="Windows 10X" width="150"/>
</div>
<div itemprop="articleBody">
<p>Майкрософт раскрыл официальные подробности новой операционной системы Windows 10X предназначенной для еще не вышедших складных и двух экранных устройств. Первыми гаджетами с двумя экранами должны стать Surface Neo и Surface Duo выход которых ожидается в конце 2020 года. Давайте разберемся, какие нововведения ждут пользователей новой операционной системы от Microsoft.</p>
</div>
</div>
<div class="leading-9" itemprop="blogPost" itemscope="" itemtype="https://schema.org/BlogPosting">
<div class="page-header">
<h1 itemprop="headline">
<a href="/internet/kak-proverit-skorost-interneta-windows10" itemprop="url">
						Как проверить скорость интернета в Windows 10					</a>
</h1>
<dd itemprop="author" itemscope="" itemtype="https://schema.org/Person">
<meta content="BeginPC" itemprop="name"/>
</dd>
<dd itemprop="publisher" itemscope="" itemtype="http://schema.org/Organization">
<span itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
<link href="/images/logo_site.png" itemprop="url image"/></span>
<meta content="BeginPC.ru" itemprop="name"/>
<meta content="" itemprop="telephone"/>
<meta content="Россия" itemprop="address"/>
</dd>
<time datetime="2020-02-10T19:50:04+03:00" itemprop="datePublished"></time><time datetime="2020-03-13T15:49:49+03:00" itemprop="dateModified"></time>
</div>
<dl class="article-info muted">
<dt class="article-info-term">
</dt>
<dd class="category-name">
																		Категория: <a href="/internet" itemprop="genre">Интернет</a> </dd>
</dl>
<div class="begin">
<img alt="спидометр интернета" class="caption" height="150" itemprop="image" loading="lazy" src="/images/internet/internet_speedometr_small.png" title="Скорость подключения" width="150"/>
</div>
<div itemprop="articleBody">
<p>Когда вы подключаетесь к новому интернет-провайдеру или сайты начинают медленно открываться обычно возникает желание выяснить реальную скорость вашего интернета. Есть много способов проверить ее: начиная от встроенных средств операционной системы и заканчивая различными специализированными программами и веб-сервисами.</p>
</div>
</div>
</div>
<!-- end items-leading -->
</div>
<div aria-label="breadcrumbs" class="breadcrumbs" role="navigation">
<ul class="breadcrumb" itemscope="" itemtype="https://schema.org/BreadcrumbList">
<li>
				Вы здесь:  
			</li>
<li class="active" itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
<span itemprop="name">
						Главная					</span>
<meta content="1" itemprop="position"/>
</li>
</ul>
</div>
</div>
</main><aside id="right-col"><div class="well ">
<div class="custom">
<!-- Yandex.RTB R-A-518495-3 -->
<div id="yandex_rtb_R-A-518495-3"></div>
<script type="text/javascript">(function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-518495-3",
                renderTo: "yandex_rtb_R-A-518495-3",
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");</script>
</div>
</div>
<div class="well ">
<h3 class="page-header">Рекомендуемое</h3>
<ul class="mostread mod-list">
<li>
<a href="/windows/ochishchaem-papku-winsxs-windows-7-pravilno">
<span>
				Очищаем папку WinSxS в Windows 7 правильно			</span>
</a>
</li>
<li>
<a href="/windows/propal-znachok-smeny-yazyka-na-paneli-zadach">
<span>
				Пропал значок смены языка на панели задач			</span>
</a>
</li>
<li>
<a href="/internet/spisok-onlayn-kursov-dlya-samoizolyacii">
<span>
				Как провести время на самоизоляции с пользой			</span>
</a>
</li>
<li>
<a href="/windows/komandnaya-stroka">
<span>
				Командная строка в Windows			</span>
</a>
</li>
<li>
<a href="/windows/zapusk-programmy-ot-imeni-drugogo-polzovatelya-v-windows">
<span>
				Запуск программы от имени другого пользователя в Windows			</span>
</a>
</li>
<li>
<a href="/hardware/chto-takoe-materinskaya-plata">
<span>
				Что такое материнская плата			</span>
</a>
</li>
<li>
<a href="/internet/chto-takoe-browser">
<span>
				Что такое браузер			</span>
</a>
</li>
<li>
<a href="/hardware/operativnaya-pamyat-kompyutera">
<span>
				Оперативная память компьютера			</span>
</a>
</li>
<li>
<a href="/hardware/what-is-video-card">
<span>
				Что такое видеокарта			</span>
</a>
</li>
<li>
<a href="/windows/nastroika-vneshnego-vida-provodnika-windows-7">
<span>
				Настройка внешнего вида проводника в Windows 7			</span>
</a>
</li>
</ul>
</div>
</aside>
</div>
<footer><div id="copyright">
<nav itemscope="" itemtype="http://schema.org/SiteNavigationElement"><ul class="nav menu mod-list">
<li class="item-133"><a href="/kontakty" itemprop="url">Контакты</a></li>
<li class="item-150"><a href="/map" itemprop="url">Карта сайта</a></li>
</ul></nav><p>© 2013-2021 Компьютер для новичков. Все права защищены.</p>
<p>При перепечатке или копировании материалов индексируемая ссылка на сайт обязательна.</p>
</div>
<!--noindex-->
<div id="counters">
<!--LiveInternet logo-->
<a href="//www.liveinternet.ru/click" rel="nofollow noopener" target="_blank"><img alt="счетчик" border="0" height="31" src="//counter.yadro.ru/logo?11.2" title="LiveInternet: показано число просмотров за 24 часа, посетителей за 24 часа и за сегодня" width="88"/></a>
<!--/LiveInternet-->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
				var yaParams = {ip: "2001:250:3c02:749:e444:4db8:6cea:7c56"};
				(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
				m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
				(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
				
				ym(51773795, "init", {
						id:51773795,
						params:window.yaParams,
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true,
						webvisor:true
				});
				</script><noscript><div><img alt="счетчик ya" src="https://mc.yandex.ru/watch/51773795" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
</div>
<!--/noindex-->
<svg alt="стрелка вверх страницы" height="50" id="to-up" tabindex="0" viewbox="0 0 50 50" width="50" xmlns="http://www.w3.org/2000/svg"><g><ellipse cx="25" cy="25" fill="#2775a5" rx="24" ry="24" stroke="#bfe2ff" stroke-width="2"></ellipse></g><g><path d="m39.71009,33.30704c0,0.8333 -0.67551,1.50881 -1.5088,1.50881l-13.20167,-5.6579l-13.20179,5.65796c-0.83336,0 -1.5088,-0.67556 -1.5088,-1.50887l13.57893,-24.51747c0,0 1.1316,-1.1316 2.26308,0c1.13166,1.13166 13.57905,24.51747 13.57905,24.51747z" fill="#ffffff"></path></g></svg><script defer="" src="/templates/livecomp/js/jui/jquery.min.js"></script>
<script defer="" src="/media/jui/js/bootstrap.min.js"></script>
<script defer="" src="/templates/livecomp/js/work.js?v=7"></script>
</footer>
</body>
</html>
