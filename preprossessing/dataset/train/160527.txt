<!DOCTYPE html>
<!--[if lt IE 7]>     <html class="no-js ie lt-ie9 lt-ie8 lt-ie7"><![endif]--><!--[if IE 7]>        <html class="no-js ie lt-ie9 lt-ie8"><![endif]--><!--[if IE 8]>        <html class="no-js ie lt-ie9"><![endif]--><!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
<title>The 404 Channel - AskMen</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
<meta content="The 404 Channel" name="title"/>
<meta content="" name="idSubChannel"/>
<meta content="" name="subchannel"/>
<meta content="36" name="idChannel"/>
<meta content="The 404 Channel" name="channel"/>
<meta content="404" name="cRoot"/>
<meta content="" name="subRoot"/>
<meta content="1" name="isIndex"/>
<meta content="Channel" name="type_of"/>
<meta content="https://askmen.com/404/" name="url"/>
<meta content="" name="keywords"/>
<meta content="" name="description"/>
<meta content="0" name="pindex"/>
<meta content="1" name="pages"/>
<meta content="8434698722" property="fb:page_id"/>
<meta content="212161225505134" property="fb:app_id"/>
<meta content="The 404 Channel" property="og:title"/>
<meta content="AskMen" property="og:site_name"/>
<meta content="" property="og:image"/>
<meta content="https://askmen.com/404/" property="og:url"/>
<meta content="" property="og:description"/>
<!--[if !IE]><!-->
<link as="style" href="/css/responsive/app/fonts.css" rel="preload"/>
<link href="/css/responsive/app/fonts.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/avalanche.css" rel="preload"/>
<link href="/css/responsive/app/avalanche.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/main.css" rel="preload"/>
<link href="/css/responsive/app/main.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/tiles.css" rel="preload"/>
<link href="/css/responsive/app/tiles.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/top_menu.css" rel="preload"/>
<link href="/css/responsive/app/top_menu.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/menu.css" rel="preload"/>
<link href="/css/responsive/app/menu.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/article.css" rel="preload"/>
<link href="/css/responsive/app/article.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/aside.css" rel="preload"/>
<link href="/css/responsive/app/aside.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/channels.css" rel="preload"/>
<link href="/css/responsive/app/channels.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/ads.css" rel="preload"/>
<link href="/css/responsive/app/ads.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/responsive.css" rel="preload"/>
<link href="/css/responsive/app/responsive.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/plus.css" rel="preload"/>
<link href="/css/responsive/app/plus.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/popup.css" rel="preload"/>
<link href="/css/responsive/app/popup.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/top_10.css" rel="preload"/>
<link href="/css/responsive/app/top_10.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/vendor/fotorama.css" rel="preload"/>
<link href="/css/responsive/vendor/fotorama.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/one.css" rel="preload"/>
<link href="/css/responsive/app/one.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/share.css" rel="preload"/>
<link href="/css/responsive/app/share.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/article_pagination.css" rel="preload"/>
<link href="/css/responsive/app/article_pagination.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/gallery.css" rel="preload"/>
<link href="/css/responsive/app/gallery.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/breadcrumbs.css" rel="preload"/>
<link href="/css/responsive/app/breadcrumbs.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/animations.css" rel="preload"/>
<link href="/css/responsive/app/animations.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/search.css" rel="preload"/>
<link href="/css/responsive/app/search.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/nine_shop.css" rel="preload"/>
<link href="/css/responsive/app/nine_shop.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/comments.css" rel="preload"/>
<link href="/css/responsive/app/comments.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/author_profile.css" rel="preload"/>
<link href="/css/responsive/app/author_profile.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/homepage_hero.css" rel="preload"/>
<link href="/css/responsive/app/homepage_hero.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/mymagazine.css" rel="preload"/>
<link href="/css/responsive/app/mymagazine.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/abyss.css" rel="preload"/>
<link href="/css/responsive/app/abyss.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/mega_lists.css" rel="preload"/>
<link href="/css/responsive/app/mega_lists.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/acquire.css" rel="preload"/>
<link href="/css/responsive/app/acquire.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/reviews.css" rel="preload"/>
<link href="/css/responsive/app/reviews.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/icons.css" rel="preload"/>
<link href="/css/responsive/app/icons.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/tentpoles.css" rel="preload"/>
<link href="/css/responsive/app/tentpoles.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/guyq.css" rel="preload"/>
<link href="/css/responsive/app/guyq.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/messages.css" rel="preload"/>
<link href="/css/responsive/app/messages.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/vendor/effects.css" rel="preload"/>
<link href="/css/responsive/vendor/effects.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/vendor/jquery.fancybox.css" rel="preload"/>
<link href="/css/responsive/vendor/jquery.fancybox.css" rel="stylesheet"/>
<link as="style" href="/css/responsive/app/email_widget.css" rel="preload"/>
<link href="/css/responsive/app/email_widget.css" rel="stylesheet"/>
<!--<![endif]-->
<link as="script" href="/js/responsive/vendor/json3.min.js" rel="preload"/>
<script src="/js/responsive/vendor/json3.min.js"></script>
<link as="script" href="/js/responsive/vendor/jquery-1.11.1.js" rel="preload"/>
<script src="/js/responsive/vendor/jquery-1.11.1.js"></script>
<link as="script" href="/js/responsive/vendor/modernizr.js" rel="preload"/>
<script src="/js/responsive/vendor/modernizr.js"></script>
<link as="script" href="/js/responsive/app/lazyload.js" rel="preload"/>
<script src="/js/responsive/app/lazyload.js"></script>
<link as="script" href="/js/responsive/app/extensions.js" rel="preload"/>
<script src="/js/responsive/app/extensions.js"></script>
<link as="script" href="/js/responsive/vendor/jquery.easing.1.3.js" rel="preload"/>
<script src="/js/responsive/vendor/jquery.easing.1.3.js"></script>
<link as="script" href="/js/responsive/vendor/jquery.tap.js" rel="preload"/>
<script src="/js/responsive/vendor/jquery.tap.js"></script>
<link as="script" href="/js/responsive/vendor/jquery.cookie.js" rel="preload"/>
<script src="/js/responsive/vendor/jquery.cookie.js"></script>
<link as="script" href="/js/responsive/vendor/store.js" rel="preload"/>
<script src="/js/responsive/vendor/store.js"></script>
<link as="script" href="/js/responsive/vendor/menu-aim.js" rel="preload"/>
<script src="/js/responsive/vendor/menu-aim.js"></script>
<link as="script" href="/js/responsive/vendor/md5.js" rel="preload"/>
<script src="/js/responsive/vendor/md5.js"></script>
<link as="script" href="/js/responsive/app/event.js" rel="preload"/>
<script src="/js/responsive/app/event.js"></script>
<link as="script" href="/js/responsive/app/util.js" rel="preload"/>
<script src="/js/responsive/app/util.js"></script>
<link as="script" href="/js/responsive/app/analytics.js" rel="preload"/>
<script src="/js/responsive/app/analytics.js"></script>
<link as="script" href="/js/responsive/app/ab_tests.js" rel="preload"/>
<script src="/js/responsive/app/ab_tests.js"></script>
<link as="script" href="/js/responsive/app/meta.js" rel="preload"/>
<script src="/js/responsive/app/meta.js"></script>
<link as="script" href="/js/responsive/app/one.js" rel="preload"/>
<script src="/js/responsive/app/one.js"></script>
<link as="script" href="/js/responsive/app/init.js" rel="preload"/>
<script src="/js/responsive/app/init.js"></script>
<link as="script" href="/js/responsive/app/api.js" rel="preload"/>
<script src="/js/responsive/app/api.js"></script>
<link as="script" href="/js/responsive/app/cookie_consent.js" rel="preload"/>
<script src="/js/responsive/app/cookie_consent.js"></script>
<link as="script" href="/js/responsive/app/history.js" rel="preload"/>
<script src="/js/responsive/app/history.js"></script>
<link as="script" href="/js/responsive/app/element.js" rel="preload"/>
<script src="/js/responsive/app/element.js"></script>
<link as="script" href="/js/responsive/app/plus.js" rel="preload"/>
<script src="/js/responsive/app/plus.js"></script>
<link as="script" href="/js/responsive/app/email_subscription.js" rel="preload"/>
<script src="/js/responsive/app/email_subscription.js"></script>
<link as="script" href="/js/responsive/app/popup.js" rel="preload"/>
<script src="/js/responsive/app/popup.js"></script>
<link as="script" href="/js/responsive/app/menu.js" rel="preload"/>
<script src="/js/responsive/app/menu.js"></script>
<link as="script" href="/js/responsive/app/aside.js" rel="preload"/>
<script src="/js/responsive/app/aside.js"></script>
<link as="script" href="/js/responsive/app/pageload.js" rel="preload"/>
<script src="/js/responsive/app/pageload.js"></script>
<link as="script" href="/js/responsive/app/channel_load.js" rel="preload"/>
<script src="/js/responsive/app/channel_load.js"></script>
<link as="script" href="/js/responsive/app/apstag.js" rel="preload"/>
<script src="/js/responsive/app/apstag.js"></script>
<link as="script" href="/js/responsive/app/recover.js" rel="preload"/>
<script src="/js/responsive/app/recover.js"></script>
<link as="script" href="/js/responsive/app/apstag.js" rel="preload"/>
<script src="/js/responsive/app/apstag.js"></script>
<link as="script" href="/js/responsive/app/prebid.js" rel="preload"/>
<script src="/js/responsive/app/prebid.js"></script>
<link as="script" href="/js/responsive/app/ads.js" rel="preload"/>
<script src="/js/responsive/app/ads.js"></script>
<link as="script" href="/js/responsive/app/video.js" rel="preload"/>
<script src="/js/responsive/app/video.js"></script>
<link as="script" href="/js/responsive/app/privacy_policy.js" rel="preload"/>
<script src="/js/responsive/app/privacy_policy.js"></script>
<link as="script" href="/js/responsive/app/gallery.js" rel="preload"/>
<script src="/js/responsive/app/gallery.js"></script>
<link as="script" href="/js/responsive/app/tentpole.js" rel="preload"/>
<script src="/js/responsive/app/tentpole.js"></script>
<link as="script" href="/js/responsive/app/mymagazine.js" rel="preload"/>
<script src="/js/responsive/app/mymagazine.js"></script>
<link as="script" href="/js/responsive/app/questions.js" rel="preload"/>
<script src="/js/responsive/app/questions.js"></script>
<link as="script" href="/js/responsive/app/messages.js" rel="preload"/>
<script src="/js/responsive/app/messages.js"></script>
<link as="script" href="/js/responsive/app/mega_list.js" rel="preload"/>
<script src="/js/responsive/app/mega_list.js"></script>
<link as="script" href="/js/responsive/app/acquire.js" rel="preload"/>
<script src="/js/responsive/app/acquire.js"></script>
<link as="script" href="/js/responsive/app/notifications.js" rel="preload"/>
<script src="/js/responsive/app/notifications.js"></script>
<link as="script" href="/js/responsive/app/share.js" rel="preload"/>
<script src="/js/responsive/app/share.js"></script>
<link as="script" href="/js/responsive/app/comment.js" rel="preload"/>
<script src="/js/responsive/app/comment.js"></script>
<link as="script" href="/js/responsive/app/nine_shop.js" rel="preload"/>
<script src="/js/responsive/app/nine_shop.js"></script>
<link as="script" href="/js/responsive/app/carousel.js" rel="preload"/>
<script src="/js/responsive/app/carousel.js"></script>
<link as="script" href="/js/responsive/lib/comscore.js" rel="preload"/>
<script src="/js/responsive/lib/comscore.js"></script>
<link as="script" href="js/responsive/vendor/jquery.knob.js" rel="preload"/>
<script src="/js/responsive/vendor/jquery.knob.js"></script>
<link as="script" href="/js/responsive/vendor/sly.js" rel="preload"/>
<script src="/js/responsive/vendor/sly.js"></script>
<link as="script" href="/js/responsive/vendor/html5-history.js" rel="preload"/>
<script src="/js/responsive/vendor/html5-history.js"></script>
<link as="script" href="/js/responsive/vendor/fotorama.js" rel="preload"/>
<script src="/js/responsive/vendor/fotorama.js"></script>
<link as="script" href="/js/responsive/vendor/swfobject.js" rel="preload"/>
<script src="/js/responsive/vendor/swfobject.js"></script>
<link as="script" href="/js/responsive/vendor/imagesloaded.js" rel="preload"/>
<script src="/js/responsive/vendor/imagesloaded.js"></script>
<link as="script" href="/js/responsive/vendor/jquery.fitvids.js" rel="preload"/>
<script src="/js/responsive/vendor/jquery.fitvids.js"></script>
<link as="script" href="/js/responsive/vendor/jquery.fancybox.js" rel="preload"/>
<script src="/js/responsive/vendor/jquery.fancybox.js"></script>
<link as="script" href="/js/responsive/vendor/jquery.timeago.js" rel="preload"/>
<script src="/js/responsive/vendor/jquery.timeago.js"></script>
<link as="script" href="/js/responsive/vendor/select2.js" rel="preload"/>
<script src="/js/responsive/vendor/select2.js"></script>
<link as="script" href="/js/responsive/vendor/autosize.js" rel="preload"/>
<script src="/js/responsive/vendor/autosize.js"></script>
<link as="script" href="/js/responsive/vendor/push.js" rel="preload"/>
<script src="/js/responsive/vendor/push.js"></script>
<link as="script" href="/js/responsive/vendor/optimizely.js" rel="preload"/>
<script src="/js/responsive/vendor/optimizely.js"></script>
<link as="script" href="/js/responsive/vendor/stack-sonar.js" rel="preload"/>
<script src="/js/responsive/vendor/stack-sonar.js"></script>
<link as="script" href="/js/responsive/app/adinfo.js" rel="preload"/>
<script src="/js/responsive/app/adinfo.js"></script>
<link as="script" href="/js/responsive/app/inject.js" rel="preload"/>
<script src="/js/responsive/app/inject.js"></script>
<link as="script" href="/js/responsive/app/abyss.js" rel="preload"/>
<script src="/js/responsive/app/abyss.js"></script>
<link href="/css/responsive/app/error.css" rel="stylesheet"/>
<script type="text/javascript">
        var _sf_async_config = _sf_async_config || {};
        _sf_async_config.uid = 21105;
        _sf_async_config.domain = 'askmen.com';
        _sf_async_config.flickerControl = false;
        _sf_async_config.useCanonical = true;
        _sf_async_config.useCanonicalDomain = true;
    </script>
<script async="" src="//static.chartbeat.com/js/chartbeat_mab.js"></script>
</head>
<body class="tiny error-404 index loggedOut" id="TINY">
<div class="menu headerMenu" id="askmenMenu">
<div id="headerOuter">
<header data-track-category="Header Menu">
<div class="js__menu-hamburger-icon menu-toggle__container">
<div class="menu-toggle__hamburger-icon menu-toggle__hamburger-icon--dark" data-track-action="Hamburger"></div>
</div>
<div class="headerDefault">
<div id="askMenLogo">
<a class="offsetText" href="/" title="AskMen">AskMen</a>
</div>
<ul id="headerMenuRight">
<li class="search js__user-menu" data-menu="search-menu"><span class="circle icon-search"></span></li>
<li class="messages js__user-menu" data-menu="messages-menu"><span class="circle icon-message-filled"></span></li>
<li class="user js__user-menu" data-menu="user-menu"><span class="circle avatar"></span></li>
<li class="signUp" data-track-action="Sign up / Login" onclick="return AM.One.login()">
Sign up <span>Log in</span>
</li>
</ul></div>
<div class="headerTitle" data-track-category="Article Title Bar">
<div class="logo"><a class="offsetText" href="/">AskMen</a></div>
<ul class="share">
<li class="plusIcon" data-track-action="Plus"></li>
<li class="facebook external-social-icon" data-track-action="Facebook"></li>
<li class="twitter external-social-icon" data-track-action="Twitter"></li>
<li class="whatsapp external-social-icon optional" data-track-action="WhatsApp" style="display:none"></li>
<li class="shareCounter">
<div class="shareTotal">
<div class="shareCount">0</div>
<div class="shareText">Shares</div>
</div>
</li>
</ul>
<div class="title"></div>
</div> </header>
<div class="js__user-dropdown search-menu" data-track-category="Search Menu">
<div class="searchContainer">
<form action="/search" class="searchForm">
<div class="searchInputContainer">
<label class="offsetText" for="searchInput1">Search AskMen</label>
<input class="searchInput" id="searchInput1" name="q" placeholder="Ask Us" type="text"/>
<button class="searchSubmit" data-track-action="Search submit button" type="submit">Search</button>
</div>
</form>
</div>
</div>
</div>
<div class="js__user-dropdown messages-menu notificationWindow">
<div class="container">
<div class="header">
<span class="icon-message-filled"></span>
<span class="title">Messages <span class="count"></span></span>
<span class="none">You have no messages</span>
</div>
<ul></ul>
</div>
</div> <div class="js__user-dropdown user-menu notificationWindow noNotifications" data-track-category="User Menu">
<div class="container">
<div class="header">
<span class="icon-bell"></span>
<span class="title">Notifications <span class="count"></span></span>
<span class="none">You have no notifications</span>
</div>
<ul></ul>
<div class="user-footer">
<a class="logout" href="/logout" onclick="AM.One.logout();return false;"><span class="icon-power"></span> Log out</a>
<a class="questions" href="/">My guyQ</a>
<a class="stacks" href="/mymagazine">My Stacks</a>
<a href="/preferences">Settings</a>
</div>
</div>
</div>
<div class="js__menu-container menu__container " id="quickMenu">
<div class="menu__askmen-logo">
<a class="offsetText menu__askmen-logo-link" href="/" title="AskMen">AskMen</a>
</div>
<div class="js__menu-cross-icon menu-toggle__cross-icon menu-toggle__cross-icon--dark" data-track-action="Cross Icon">
</div>
<div class="menu__content-container">
<form action="/search" class="menu__search-field">
<label class="offsetText" for="searchInput">Search AskMen</label>
<input class="menu__search-input" id="searchInput" name="q" placeholder="Ask Us" type="text"/>
<button class="menu__search-button" data-track-action="Search submit button" type="submit">
<span class="offsetText">Search submit button</span>
<span class="circle icon-search menu__search-icon"></span>
</button>
</form>
<a class="menu__guyq-icon icon-guyq guyQLink" data-track-action="guyQ" href="/answers/">guyQ</a>
<div class="menu__item-container">
<span class="menu__item-label js__menu-item">News</span>
<span class="js__dropdown-arrow menu__item-arrow"></span>
<div class="js__menu-dropdown menu__subchannel-container ">
<a class="menu__subchannel-link" data-track-action="All" href="/news/">All</a>
<a class="menu__subchannel-link" data-track-action="Career &amp; Money" href="/news/career_money/">Career &amp; Money</a>
<a class="menu__subchannel-link" data-track-action="Dating" href="/news/dating/">Dating</a>
<a class="menu__subchannel-link" data-track-action="Entertainment" href="/news/entertainment/">Entertainment</a>
<a class="menu__subchannel-link" data-track-action="Food &amp; Booze" href="/news/food_booze/">Food &amp; Booze</a>
<a class="menu__subchannel-link" data-track-action="Health" href="/news/health/">Health</a>
<a class="menu__subchannel-link" data-track-action="Home &amp; Auto" href="/news/home_auto/">Home &amp; Auto</a>
<a class="menu__subchannel-link" data-track-action="Style &amp; Fashion" href="/news/style_fashion/">Style &amp; Fashion</a>
<a class="menu__subchannel-link" data-track-action="Tech" href="/news/tech/">Tech</a>
<a class="menu__subchannel-link" data-track-action="Black Friday" href="/deals/">Black Friday</a> </div>
</div>
<div class="menu__item-container">
<span class="menu__item-label js__menu-item">Sex</span>
<span class="js__dropdown-arrow menu__item-arrow"></span>
<div class="js__menu-dropdown menu__subchannel-container ">
<a class="menu__subchannel-link" data-track-action="All" href="/sex/">All</a>
<a class="menu__subchannel-link" data-track-action="Sex Positions" href="/sex/sex_positions/">Sex Positions</a>
<a class="menu__subchannel-link" data-track-action="Sex Tips" href="/sex/sex_tips/">Sex Tips</a>
<a class="menu__subchannel-link" data-track-action="Sex Toys &amp; Games" href="/sex/sex_toys_and_games/">Sex Toys &amp; Games</a>
<a class="menu__subchannel-link" data-track-action="Sexual Experiences" href="/sex/sexual_experiences/">Sexual Experiences</a>
<a class="menu__subchannel-link" data-track-action="Sexual Health" href="/sex/sexual_health/">Sexual Health</a> </div>
</div>
<div class="menu__item-container">
<span class="menu__item-label js__menu-item">Dating</span>
<span class="js__dropdown-arrow menu__item-arrow"></span>
<div class="js__menu-dropdown menu__subchannel-container ">
<a class="menu__subchannel-link" data-track-action="All" href="/dating/">All</a>
<a class="menu__subchannel-link" data-track-action="Dating Advice" href="/dating/dating_advice/">Dating Advice</a>
<a class="menu__subchannel-link" data-track-action="Dating Experiences" href="/dating/dating_experiences/">Dating Experiences</a>
<a class="menu__subchannel-link" data-track-action="Best Online Dating Sites" href="/dating/online-dating-sites">Best Online Dating Sites</a>
<a class="menu__subchannel-link" data-track-action="Relationship Advice" href="/dating/relationship_advice/">Relationship Advice</a> </div>
</div>
<div class="menu__item-container">
<span class="menu__item-label js__menu-item">Grooming</span>
<span class="js__dropdown-arrow menu__item-arrow"></span>
<div class="js__menu-dropdown menu__subchannel-container ">
<a class="menu__subchannel-link" data-track-action="All" href="/grooming/">All</a>
<a class="menu__subchannel-link" data-track-action="Fragrances" href="/grooming/fragrances/">Fragrances</a>
<a class="menu__subchannel-link" data-track-action="Hair" href="/grooming/hair/">Hair</a>
<a class="menu__subchannel-link" data-track-action="Shaving" href="/grooming/shaving">Shaving</a>
<a class="menu__subchannel-link" data-track-action="Skin" href="/grooming/skin/">Skin</a> </div>
</div>
<div class="menu__item-container">
<span class="menu__item-label js__menu-item">Style</span>
<span class="js__dropdown-arrow menu__item-arrow"></span>
<div class="js__menu-dropdown menu__subchannel-container ">
<a class="menu__subchannel-link" data-track-action="All" href="/style/">All</a>
<a class="menu__subchannel-link" data-track-action="Accessories" href="/style/accessories/">Accessories</a>
<a class="menu__subchannel-link" data-track-action="Fashion Advice" href="/style/fashion_advice/">Fashion Advice</a>
<a class="menu__subchannel-link" data-track-action="Fashion Trends" href="/style/fashion_trends/">Fashion Trends</a>
<a class="menu__subchannel-link" data-track-action="Shopping" href="/style/shopping/">Shopping</a>
<a class="menu__subchannel-link" data-track-action="Underwear" href="/style/underwear/">Underwear</a>
<a class="menu__subchannel-link" data-track-action="Watches" href="/style/watches/">Watches</a> </div>
</div>
<div class="menu__item-container">
<span class="menu__item-label js__menu-item">Fitness</span>
<span class="js__dropdown-arrow menu__item-arrow"></span>
<div class="js__menu-dropdown menu__subchannel-container ">
<a class="menu__subchannel-link" data-track-action="All" href="/fitness/">All</a>
<a class="menu__subchannel-link" data-track-action="Health" href="/fitness/health/">Health</a>
<a class="menu__subchannel-link" data-track-action="Mental Health" href="/fitness/mental_health/">Mental Health</a>
<a class="menu__subchannel-link" data-track-action="Nutrition" href="/fitness/nutrition/">Nutrition</a>
<a class="menu__subchannel-link" data-track-action="Weight Loss" href="/fitness/weight_loss/">Weight Loss</a>
<a class="menu__subchannel-link" data-track-action="Workout" href="/fitness/workout/">Workout</a> </div>
</div>
<div class="menu__item-container">
<span class="menu__item-label js__menu-item">Gear</span>
<span class="js__dropdown-arrow menu__item-arrow"></span>
<div class="js__menu-dropdown menu__subchannel-container ">
<a class="menu__subchannel-link" data-track-action="All" href="/gear/">All</a>
<a class="menu__subchannel-link" data-track-action="Hobby" href="/gear/hobby/">Hobby</a>
<a class="menu__subchannel-link" data-track-action="Home" href="/gear/home/">Home</a>
<a class="menu__subchannel-link" data-track-action="Tech" href="/gear/tech/">Tech</a>
<a class="menu__subchannel-link" data-track-action="Toys" href="/gear/toys/">Toys</a>
<a class="menu__subchannel-link" data-track-action="Travel" href="/gear/travel/">Travel</a>
<a class="menu__subchannel-link" data-track-action="Work" href="/gear/work/">Work</a> </div>
</div>
<div class="menu__item-container">
<span class="menu__item-label js__menu-item">Man Skills</span>
<span class="js__dropdown-arrow menu__item-arrow"></span>
<div class="js__menu-dropdown menu__subchannel-container ">
<a class="menu__subchannel-link" data-track-action="All" href="/man_skills/">All</a>
<a class="menu__subchannel-link" data-track-action="Auto" href="/man_skills/auto/">Auto</a>
<a class="menu__subchannel-link" data-track-action="Essential" href="/man_skills/essential/">Essential</a>
<a class="menu__subchannel-link" data-track-action="Home" href="/man_skills/home/">Home</a>
<a class="menu__subchannel-link" data-track-action="Random" href="/man_skills/random/">Random</a>
<a class="menu__subchannel-link" data-track-action="Survival" href="/man_skills/survival/">Survival</a> </div>
</div>
<div class="menu__item-container">
<a class="menu__item-label" data-track-action="Amplify" href="/specials/amplify">Amplify</a>
</div>
<div class="menu__item-container">
<a class="menu__item-label" data-track-action="Subscribe" href="http://subscribe.askmen.com">Subscribe</a>
</div>
</div>
</div>
</div>
<div id="main">
<article id="articleContainer">
<div class="error-404__main-image lazyload" data-src="https://images.askmen.com/channels/404bg.jpg">
<div class="error-404__header-text">404</div>
<div class="error-404__subheader-text">Oops, this page doesn't exist</div>
</div>
<div class="grid grid--center">
<div class="3/4--small-and-up grid__cell">
<div class="error-404__question-container">
<div class="error-404__question-text">
Didn't find what you are looking for?
</div>
<a class="question-cta__button question-cta__button--dark error-404__question-button" href="/answers/">
<span class="icon-speech-bubble"></span> Ask A Question
</a>
</div>
</div>
</div>
<div class="grid" data-track-category="404 Article Links">
<div class="1/2--small-and-up grid__cell">
<div class="error-404__block">
<div class="tile-label tile-label--dating tile-label--large">
<a class="tile-label__title" data-track-action="Dating &amp; Sex Label" href="/dating/">Dating &amp; Sex</a> </div>
<ul class="error-404__article-list">
<li><a class="error-404__article-link" data-track-action="Best Online Dating Sites of 2016" href="/dating/online-dating-sites/">Best Online Dating Sites of 2016</a></li>
<li><a class="error-404__article-link" data-track-action="Top 10: Hookup Websites" href="/top_10/dating/top-10-hookup-websites.html">Top 10: Hookup Websites</a></li>
<li><a class="error-404__article-link" data-track-action="Dating With Tinder" href="/dating/curtsmith/dating-with-tinder.html">Dating With Tinder</a></li>
<li><a class="error-404__article-link" data-track-action="Top 10: First Date Ideas" href="/top_10/dating/top-10-first-date-ideas.html">Top 10: First Date Ideas</a></li>
<li><a class="error-404__article-link" data-track-action="The Best Sex Positions You Should Try Tonight" href="/sex/sex_positions/the-best-sex-positions-you-should-try-tonight/">The Best Sex Positions You Should Try Tonight</a></li>
</ul>
</div>
</div>
<div class="1/2--small-and-up grid__cell">
<div class="error-404__block">
<div class="tile-label tile-label--health tile-label--large">
<a class="tile-label__title" data-track-action="Health &amp; Sports Label" href="/sports/">Health &amp; Sports</a> </div>
<ul class="error-404__article-list">
<li><a class="error-404__article-link" data-track-action="How To Get Rid Of Man Boobs" href="/sports/bodybuilding_150/156_fitness_tip.html">How To Get Rid Of Man Boobs</a></li>
<li><a class="error-404__article-link" data-track-action="Top 10: Post-Workout Foods" href="/top_10/fitness/top-10-post-workout-foods.html">Top 10: Post-Workout Foods</a></li>
<li><a class="error-404__article-link" data-track-action="Best Fitness Trackers" href="/entertainment/guy_gear/best-fitness-trackers.html">Best Fitness Trackers</a></li>
<li><a class="error-404__article-link" data-track-action="Top 10: Foods For Muscle Growth" href="/top_10/fitness/top-10-foods-for-muscle-growth.html">Top 10: Foods For Muscle Growth</a></li>
<li><a class="error-404__article-link" data-track-action="Top 10: Leg Exercises" href="/top_10/fitness/2_fitness_list.html">Top 10: Leg Exercises</a></li>
</ul>
</div>
</div>
<div class="1/2--small-and-up grid__cell">
<div class="error-404__block">
<div class="tile-label tile-label--grooming tile-label--large">
<a class="tile-label__title" data-track-action="Grooming Label" href="/grooming/">Grooming</a> </div>
<ul class="error-404__article-list">
<li><a class="error-404__article-link" data-track-action="Best Beard Trimmers" href="/grooming/appearance/best-beard-trimmers.html">Best Beard Trimmers</a></li>
<li><a class="error-404__article-link" data-track-action="Best Beard Oils Reviewed" href="/grooming/appearance/best-beard-oils-reviewed.html">Best Beard Oils Reviewed</a></li>
<li><a class="error-404__article-link" data-track-action="Barbershop Inspired Hairstyles For Men" href="/grooming/appearance/barbershop-inspired-hairstyles-for-men.html">Barbershop Inspired Hairstyles For Men</a></li>
<li><a class="error-404__article-link" data-track-action="How To Grow Your Hair Out" href="/fashion/sachin_fashion/how-to-grow-your-hair-out.html">How To Grow Your Hair Out</a></li>
<li><a class="error-404__article-link" data-track-action="Best Hair Products For Men" href="/fashion/fashiontip/40_fashion_advice.html">Best Hair Products For Men</a></li>
</ul>
</div>
</div>
<div class="1/2--small-and-up grid__cell">
<div class="error-404__block">
<div class="tile-label tile-label--style tile-label--large">
<a class="tile-label__title" data-track-action="Style Label" href="/style/">Style</a> </div>
<ul class="error-404__article-list">
<li><a class="error-404__article-link" data-track-action="Best Watches Under $500" href="/fashion/fashion-news/best-watches-under-500.html">Best Watches Under $500</a></li>
<li><a class="error-404__article-link" data-track-action="Buying The Perfect Pair Of Men's Jeans" href="/fashion/fashiontip_200/231_fashion_advice.html">Buying The Perfect Pair Of Men's Jeans</a></li>
<li><a class="error-404__article-link" data-track-action="Choosing The Best Underwear For Men" href="/fashion/fashiontip/tip18.html">Choosing The Best Underwear For Men</a></li>
<li><a class="error-404__article-link" data-track-action="What To Wear To A Wedding" href="/fashion/fashiontip_250/263_fashion_advice.html">What To Wear To A Wedding</a></li>
<li><a class="error-404__article-link" data-track-action="Men's Fashion Hacks That Handsome Men Know" href="/style/fashion_advice/men-s-fashion-hacks-that-handsome-men-know.html">Men's Fashion Hacks That Handsome Men Know</a></li>
</ul>
</div>
</div>
<div class="1/2--small-and-up grid__cell">
<div class="error-404__block">
<div class="tile-label tile-label--entertainment tile-label--large">
<a class="tile-label__title" data-track-action="Entertainment Label" href="/entertainment/">Entertainment</a> </div>
<ul class="error-404__article-list">
<li><a class="error-404__article-link" data-track-action="Netflix Date Movies" href="/recess/fun_lists/netflix-date-movies.html">Netflix Date Movies</a></li>
<li><a class="error-404__article-link" data-track-action="Best Horror Movies On Netflix" href="/recess/fun_lists/best-horror-movies-on-netflix/">Best Horror Movies On Netflix</a></li>
<li><a class="error-404__article-link" data-track-action="The 30 Greatest Movie Speeches Ever" href="/recess/recommended/the-30-greatest-movie-speeches-of-all-time/">The 30 Greatest Movie Speeches Ever</a></li>
<li><a class="error-404__article-link" data-track-action="Best Roles By Worst Actors" href="/recess/fun_lists/best-roles-by-worst-actors.html">Best Roles By Worst Actors</a></li>
<li><a class="error-404__article-link" data-track-action="Hottest Actresses On TV" href="/recess/fun_lists/hottest-actresses-on-tv/">Hottest Actresses On TV</a></li>
</ul>
</div>
</div>
<div class="1/2--small-and-up grid__cell">
<div class="error-404__block">
<div class="tile-label tile-label--dispatch tile-label--large">
<a class="tile-label__title" data-track-action="Dispatch Label" href="/news/">Dispatch</a> </div>
<ul class="error-404__article-list">
<li><a class="error-404__article-link" data-track-action="Latest Articles" href="/news/">Latest Articles</a></li>
</ul>
</div>
</div>
</div>
</article>
</div>
<script>
$(function() {
    ga('send', 'event', {
        eventCategory: 'Error',
        eventAction: '404',
        eventLabel: 'page:' + document.location.href + ' | ref: ' + document.referrer
    });
});
</script>
</body>
</html>