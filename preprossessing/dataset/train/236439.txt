<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>Blatube</title>
<base href="/"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="favicon.ico" rel="icon" type="image/x-icon"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
<script src="https://vk.com/js/api/openapi.js?154" type="text/javascript"></script>
<script>
    var global = global || window;
    var Buffer = Buffer || [];
    var process = process || {
      env: { DEBUG: undefined },
      version: []
    };
  </script>
</head>
<body>
<app-root></app-root>
<script src="runtime.a136eed977b1987a18e2.js" type="text/javascript"></script><script src="polyfills.c438f3175dd8093e2a86.js" type="text/javascript"></script><script src="styles.826d6837c8c22e74b2f7.js" type="text/javascript"></script><script src="scripts.d0f4bba640fe71a8fb7f.js" type="text/javascript"></script><script src="vendor.1d8149e547a7fa7a2fdc.js" type="text/javascript"></script><script src="main.0172b368babb28f955b8.js" type="text/javascript"></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(34442530, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/34442530" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
