<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" class="ie ie6" lang="en-CA">
<![endif]--><!--[if IE 7]>
<html id="ie7" class="ie ie7" lang="en-CA">
<![endif]--><!--[if IE 8]>
<html id="ie8" class="ie ie8" lang="en-CA">
<![endif]--><!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!--><html lang="en-CA">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<title>Home - Animal Guardian</title>
<meta content="IE=Edge;chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<!-- Stylesheet -->
<!--[if lt IE 9]>
	<link rel="stylesheet" href="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/css/ie.css" type="text/css" media="all" />
<![endif]-->
<link href="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/css/style-default.css" media="all" rel="stylesheet" type="text/css"/>
<!-- Favicons -->
<link href="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/img/favicon.ico" rel="shortcut icon"/>
<!-- RSS -->
<link href="https://animalguardian.org/xmlrpc.php" rel="pingback"/>
<!-- This site is optimized with the Yoast SEO plugin v15.4 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" name="robots"/>
<link href="https://animalguardian.org/" rel="canonical"/>
<meta content="en_US" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="Home - Animal Guardian" property="og:title"/>
<meta content="https://animalguardian.org/" property="og:url"/>
<meta content="Animal Guardian" property="og:site_name"/>
<meta content="2017-07-26T18:06:24+00:00" property="article:modified_time"/>
<meta content="Written by" name="twitter:label1"/>
<meta content="Andrew" name="twitter:data1"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://animalguardian.org/#website","url":"https://animalguardian.org/","name":"Animal Guardian","description":"Registered Charitable Pet Rescue &amp; Adoption Organization","potentialAction":[{"@type":"SearchAction","target":"https://animalguardian.org/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-CA"},{"@type":"WebPage","@id":"https://animalguardian.org/#webpage","url":"https://animalguardian.org/","name":"Home - Animal Guardian","isPartOf":{"@id":"https://animalguardian.org/#website"},"datePublished":"2013-03-15T16:17:23+00:00","dateModified":"2017-07-26T18:06:24+00:00","inLanguage":"en-CA","potentialAction":[{"@type":"ReadAction","target":["https://animalguardian.org/"]}]}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//ajax.googleapis.com" rel="dns-prefetch"/>
<link href="//platform-api.sharethis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://animalguardian.org/feed/" rel="alternate" title="Animal Guardian » Feed" type="application/rss+xml"/>
<link href="https://animalguardian.org/comments/feed/" rel="alternate" title="Animal Guardian » Comments Feed" type="application/rss+xml"/>
<link crossorigin="" href="https://secureservercdn.net" rel="preconnect"/>
<link href="https://animalguardian.org/home/feed/" rel="alternate" title="Animal Guardian » Home Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/secureservercdn.net\/198.71.233.64\/b09.755.myftpupload.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6&time=1610535032"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/plugins/formidable/css/formidableforms.css?ver=1212257&amp;time=1610535032" id="formidable-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6&amp;time=1610535032" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-js" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js?ver=1.7.1" type="text/javascript"></script>
<script id="fittext-js" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/js/jquery.fittext.js?ver=5.6&amp;time=1610535032" type="text/javascript"></script>
<script id="mobilemenu-js" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/js/jquery.mobilemenu.js?ver=5.6&amp;time=1610535032" type="text/javascript"></script>
<script id="superfish-js" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/js/superfish-compile.js?ver=5.6&amp;time=1610535032" type="text/javascript"></script>
<script id="colorbox-js" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/js/jquery.colorbox-min.js?ver=5.6&amp;time=1610535032" type="text/javascript"></script>
<script id="tooltip-js" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/js/jquery.tipTip.minified.js?ver=5.6&amp;time=1610535032" type="text/javascript"></script>
<script id="jqueryui-js" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/js/jquery-ui-1.8.min.js?ver=5.6&amp;time=1610535032" type="text/javascript"></script>
<script id="flexslider-js" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/js/jquery.flexslider-min.js?ver=5.6&amp;time=1610535032" type="text/javascript"></script>
<script id="googleanalytics-platform-sharethis-js" src="//platform-api.sharethis.com/js/sharethis.js#product=ga&amp;property=5f25ce808936a1001219a4f2" type="text/javascript"></script>
<link href="https://animalguardian.org/wp-json/" rel="https://api.w.org/"/><link href="https://animalguardian.org/wp-json/wp/v2/pages/11" rel="alternate" type="application/json"/><link href="https://animalguardian.org/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-includes/wlwmanifest.xml?time=1610535032" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<link href="https://animalguardian.org/" rel="shortlink"/>
<link href="https://animalguardian.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fanimalguardian.org%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://animalguardian.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fanimalguardian.org%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<script type="text/javascript">
            jQuery(window).load(function() {
				if (jQuery().flexslider) {
					jQuery('.flexslider').flexslider({
						 animation: "fade", 						 slideDirection: "horizontal", 						 slideshowSpeed: 5000, 						 animationDuration: 600, 												 controlNav: false 					 });
				};
			});
		</script>
<link href="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com//wp-content/uploads/2013/12/tagsdoglogo180x150.jpg" rel="shortcut icon"/>
<style type="text/css">
 body { font-size:16px; color: #444; } 

 .container h1,  .container h2,  .container h3,  .container h4,  .container h5,  .container h6 	{  color:#4C9395; } 

</style>
<meta content="bcfK9hmUMRWhEBoYAwpmy1fw7auRxzBnuj8C4kjjH9M" name="google-site-verification"/>
</head>
<body class="home page-template page-template-template-home page-template-template-home-php page page-id-11 unknown singular">
<!-- HEADER BEGIN -->
<header id="header">
<div class="container" id="pre-header">
<div class="row">
<!-- ADDTHIS BEGIN -->
<div class="col_3"><ul class="social_toolbox fleft"><li><a href="mailto:tagsinfo@yahoo.ca" target="_blank">
<img alt="Email icon" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/img/icons/email.png"/>
</a></li><li><a href="https://www.facebook.com/AnimalGuardian" target="_blank">
<img alt="Facebook icon" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/img/icons/facebook.png"/>
</a></li><li><a href="http://pinterest.com/tagsinfo/" target="_blank">
<img alt="Flickr icon" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/img/icons/flickr.png"/>
</a></li><li><a href="https://twitter.com/tagsinfo" target="_blank">
<img alt="Twitter icon" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/img/icons/twitter.png"/>
</a></li><li><a href="http://youtu.be/BVVdvWabyZQ" target="_blank">
<img alt="Youtube icon" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/img/icons/youtube.png"/>
</a></li></ul></div><div class="col_5 pre_8 last" id="secondary-nav" role="navigation">
<div class="menu-secondary-container"><ul class="unstyled" id="menu-secondary"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1033" id="menu-item-1033"><a href="https://animalguardian.org/newsletter/">Newsletter</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1654" id="menu-item-1654"><a href="https://animalguardian.org//html_forms/admin/login.php">Login</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4562" id="menu-item-4562"><a href="https://animalguardian.org/contact-us-2/">Contact Us</a></li>
</ul></div> </div>
<!-- SECONDARY NAVIGATION END --> </div>
</div>
<!-- MAIN HEADER BEGIN -->
<div class="row" id="main-header">
<!-- LOGO BEGIN -->
<div class="col_5" id="logo">
<a href="https://animalguardian.org/" rel="home" title="Animal Guardian">
<img alt="logo" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com//wp-content/uploads/2013/03/logo1.png"/>
</a>
</div>
<!-- LOGO END -->
<div id="sf-nav">
<!-- PRIMARY NAVIGATION BEGIN -->
<div id="nav-wrapper" role="navigation">
<div class="menu-primary-container"><ul class="sf-menu" id="menu-primary"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-11 current_page_item menu-item-13" id="menu-item-13"><a aria-current="page" href="https://animalguardian.org/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-34" id="menu-item-34"><a href="https://animalguardian.org/about/">About</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1051" id="menu-item-1051"><a href="https://animalguardian.org/about/story-of-tags/">Story of TAGS</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79" id="menu-item-79"><a href="https://animalguardian.org/about/happy-tails/">Happy Tails</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-639" id="menu-item-639"><a href="https://animalguardian.org/news/">News</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-80" id="menu-item-80"><a href="https://animalguardian.org/about/in-memoriam/">In Memoriam</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78" id="menu-item-78"><a href="https://animalguardian.org/about/sponsorssupporters/">Sponsors &amp; Supporters</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-82" id="menu-item-82"><a href="https://animalguardian.org/about/services/">Services</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-469" id="menu-item-469"><a href="https://animalguardian.org/about/services/pet-training/">Pet Training</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-470" id="menu-item-470"><a href="https://animalguardian.org/about/services/microchip-your-pet/">Microchip Your Pet</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-468" id="menu-item-468"><a href="https://animalguardian.org/about/forms/">Application Forms</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-471" id="menu-item-471"><a href="https://animalguardian.org/about/services/dog-walking-park/">Dog Walking Park</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-472" id="menu-item-472"><a href="https://animalguardian.org/about/services/pet-surrendering/">Pet Surrendering</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-32" id="menu-item-32"><a href="https://animalguardian.org/how-to-help/">How to Help</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4454" id="menu-item-4454"><a href="https://animalguardian.org/permalink-https-animalguardian-org-how-to-help-volunteer-positions/">Volunteer Positions</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-315" id="menu-item-315"><a href="https://animalguardian.org/how-to-help/adopting-a-rescue/">Adopt a Rescue</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-616" id="menu-item-616"><a href="https://animalguardian.org/how-to-help/adopting-a-rescue/adoptable-pets/">Adoptable Pets</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-613" id="menu-item-613"><a href="https://animalguardian.org/how-to-help/adopting-a-rescue/adoption-process/">Adoption Process</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-76" id="menu-item-76"><a href="https://animalguardian.org/how-to-help/foster/">Foster</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-75" id="menu-item-75"><a href="https://animalguardian.org/how-to-help/volunteer/">Volunteer</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3581" id="menu-item-3581"><a href="https://animalguardian.org/how-to-help/donate-2/">Donate</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2889" id="menu-item-2889"><a href="https://animalguardian.org/how-to-help/merchandise/">Merchandise</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31" id="menu-item-31"><a href="https://animalguardian.org/events/">Events</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-116" id="menu-item-116"><a href="https://animalguardian.org/blog/">Blog</a></li>
</ul></div> </div>
<!-- PRIMARY NAVIGATION END -->
</div>
</div>
<!-- MAIN HEADER END -->
<div class="slider-container row">
<!-- SLIDER BEGIN -->
<div class="flexslider mobile-hide col_16">
<ul class="slides">
<li>
<a href="https://animalguardian.org/about/services/"><img alt="" class="attachment-slide-image size-slide-image wp-post-image" height="390" loading="lazy" sizes="(max-width: 960px) 100vw, 960px" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2020/03/covid19-960x390.jpg" srcset="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2020/03/covid19.jpg 960w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2020/03/covid19-300x122.jpg 300w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2020/03/covid19-768x312.jpg 768w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2020/03/covid19-640x260.jpg 640w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2020/03/covid19-380x154.jpg 380w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2020/03/covid19-940x382.jpg 940w" title="" width="960"/></a>
</li>
<li>
<img alt="" class="attachment-slide-image size-slide-image wp-post-image" height="390" loading="lazy" sizes="(max-width: 960px) 100vw, 960px" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/12/newbanner2.jpg?time=1610535032" srcset="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/12/newbanner2.jpg 960w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/12/newbanner2-300x121.jpg 300w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/12/newbanner2-640x260.jpg 640w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/12/newbanner2-380x154.jpg 380w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/12/newbanner2-940x381.jpg 940w" title="" width="960"/> </li>
<li>
<a href="https://animalguardian.org/permalink-https-animalguardian-org-how-to-help-volunteer-positions/"><img alt="" class="attachment-slide-image size-slide-image wp-post-image" height="390" loading="lazy" sizes="(max-width: 960px) 100vw, 960px" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/12/were-hiring-960x390.jpg" srcset="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/12/were-hiring.jpg 960w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/12/were-hiring-300x122.jpg 300w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/12/were-hiring-768x312.jpg 768w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/12/were-hiring-640x260.jpg 640w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/12/were-hiring-380x154.jpg 380w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/12/were-hiring-940x382.jpg 940w" title="" width="960"/></a>
</li>
<li>
<img alt="" class="attachment-slide-image size-slide-image wp-post-image" height="390" loading="lazy" sizes="(max-width: 960px) 100vw, 960px" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/08/slide-960x390.jpg" srcset="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/08/slide.jpg 960w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/08/slide-300x122.jpg 300w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/08/slide-768x312.jpg 768w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/08/slide-640x260.jpg 640w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/08/slide-380x154.jpg 380w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/08/slide-940x382.jpg 940w" title="" width="960"/> </li>
<li>
<a href="https://animalguardian.org/how-to-help/foster/foster-home-application-form/"><img alt="" class="attachment-slide-image size-slide-image wp-post-image" height="390" loading="lazy" sizes="(max-width: 960px) 100vw, 960px" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2014/03/foster-homes-needed-slide.jpg?time=1610535032" srcset="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2014/03/foster-homes-needed-slide.jpg 960w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2014/03/foster-homes-needed-slide-300x121.jpg 300w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2014/03/foster-homes-needed-slide-640x260.jpg 640w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2014/03/foster-homes-needed-slide-380x154.jpg 380w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2014/03/foster-homes-needed-slide-940x381.jpg 940w" title="" width="960"/></a>
</li>
<li>
<img alt="" class="attachment-slide-image size-slide-image wp-post-image" height="390" loading="lazy" sizes="(max-width: 960px) 100vw, 960px" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/11/facebookslider-960x390.jpg" srcset="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/11/facebookslider.jpg 960w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/11/facebookslider-300x121.jpg 300w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/11/facebookslider-640x260.jpg 640w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/11/facebookslider-380x154.jpg 380w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/11/facebookslider-940x381.jpg 940w" title="" width="960"/> </li>
</ul>
</div>
<!-- SLIDER END -->
<!-- ALTERNATIVE TEXT BEGIN
            This will replace the Slider on mobile version -->
<div class="mobile-only col_16" id="slider-mobile">
<h1 class="seriftype respond_1 textcenter"></h1>
</div>
<!-- ALTERNATIVE TEXT END -->
</div>
</header>
<!-- #HEADER END -->
<!-- PAGE CONTENT BEGIN -->
<div class="container">
<!-- #ACTION BEGIN -->
<section class="row" id="action">
<div class="col_16">
<div class="antique-ribbon-wrapper">
<div class="antique-ribbon"><span class="seriftype">
                
    		How to Help               
            </span></div>
</div>
</div>
<!-- Custom Menu begin -->
<div class="menu-ctoa-container" id="cta-container"><ul class="overview" id="menu-ctoa"><li class="col_4 menu-item menu-item-type-post_type menu-item-object-page menu-item-626" id="menu-item-626"><div class="intro-widget round-4"><a href="https://animalguardian.org/how-to-help/adopting-a-rescue/adoptable-pets/" title="Adopt a pet and save a life."><div class="intro-img mobile-hide"></div>
<div class="intro-content"><h3 class="replace tshadow">Adoptable Pets</h3><p class="mobile-hide">Adopt a pet and save a life.</p></div></a></div></li>
<li class="col_4 menu-item menu-item-type-post_type menu-item-object-page menu-item-3613" id="menu-item-3613"><div class="intro-widget round-4"><a href="https://animalguardian.org/how-to-help/donate-2/" title="100% of your donations goes to helping the animals in our program as we have no paid staff."><div class="intro-img mobile-hide"></div>
<div class="intro-content"><h3 class="replace tshadow">Donate</h3><p class="mobile-hide">100% of your donations goes to helping the animals in our program as we have no paid staff.</p></div></a></div></li>
<li class="col_4 menu-item menu-item-type-post_type menu-item-object-page menu-item-87" id="menu-item-87"><div class="intro-widget round-4"><a href="https://animalguardian.org/how-to-help/foster/" title="Foster Homes are the difference between Life and Death to an animal in need."><div class="intro-img mobile-hide"></div>
<div class="intro-content"><h3 class="replace tshadow">Foster</h3><p class="mobile-hide">Foster Homes are the difference between Life and Death to an animal in need.</p></div></a></div></li>
<li class="col_4 menu-item menu-item-type-post_type menu-item-object-page menu-item-88" id="menu-item-88"><div class="intro-widget round-4"><a href="https://animalguardian.org/how-to-help/volunteer/" title="The Animal Guardian Society volunteers are the most valued assets of our charity."><div class="intro-img mobile-hide"></div>
<div class="intro-content"><h3 class="replace tshadow">Volunteer</h3><p class="mobile-hide">The Animal Guardian Society volunteers are the most valued assets of our charity.</p></div></a></div></li>
</ul></div> <!-- Custom Menu end -->
<hr/>
</section>
<!-- #ACTION END --> <!-- INTRO BEGIN -->
<section class="home-page-intro row">
<article class="intro">
<header class="entry-header">
<h2 class="entry-title vintage-type replace">
            MISSION STATEMENT            </h2>
</header>
<div class="entry-content">
<p>It is our mission to provide re-homing and medical care to displaced companion animals and to support the community in areas of humane education, behaviour counseling, the promotion of responsible pet ownership, and the needs of animals.</p> <p><a class="btn primary" href="https://animalguardian.org/?page_id=23">
                click here for more info                </a></p>
</div>
</article>
<hr class="thin"/>
</section>
<!-- INTRO END --><!-- PROFILES BEGIN -->
<section id="profiles">
<div class="row divider-line">
<div class="fancy-ribbon-wrapper seriftype">
<div class="fancy-ribbon"><span>
			Events            </span> </div>
</div>
</div>
<div class="profiles-content row ">
<div class="profile-post col_4">
<div class="profile-image fleft boxshadow"><img alt="" class="attachment-profile-thumbnail size-profile-thumbnail wp-post-image" height="60" loading="lazy" sizes="(max-width: 60px) 100vw, 60px" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/12/we-are-hiring-566-60x60.jpg" srcset="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/12/we-are-hiring-566-60x60.jpg 60w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/12/we-are-hiring-566-300x300.jpg 300w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/12/we-are-hiring-566-150x150.jpg 150w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/12/we-are-hiring-566-380x380.jpg 380w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/12/we-are-hiring-566-50x50.jpg 50w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/12/we-are-hiring-566-85x85.jpg 85w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/12/we-are-hiring-566-e1575476994180.jpg 200w" width="60"/></div>
<div class="profile-post-content">
<h4 class="replace inset"><a href="https://animalguardian.org/profile/were-hiring/" rel="bookmark" title="Permalink to We’re Hiring!">We’re Hiring!</a></h4>
<p>We are looking for Committed, Responsible, and Enthusiastic Volunteers to join our team! These positions are vital to our program and continuing our mission of saving animals in need. Please click here for more information.  </p>
</div>
</div>
<div class="profile-post col_4">
<div class="profile-image fleft boxshadow"><img alt="" class="attachment-profile-thumbnail size-profile-thumbnail wp-post-image" height="60" loading="lazy" sizes="(max-width: 60px) 100vw, 60px" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/05/Bingo-Events-60x60.jpg" srcset="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/05/Bingo-Events-60x60.jpg 60w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/05/Bingo-Events-50x50.jpg 50w, https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/05/Bingo-Events-85x85.jpg 85w" width="60"/></div>
<div class="profile-post-content">
<h4 class="replace inset"><a href="https://animalguardian.org/profile/playbingo/" rel="bookmark" title="Permalink to BINGO">BINGO</a></h4>
<p>Come out and enjoy playing bingo while supporting The Animal Guardian Society</p>
<p><a class="cta" href="https://animalguardian.org/profile/playbingo/">Read More »</a></p>
</div>
</div>
</div>
</section>
<!-- PROFILES END --> <!-- #DONATE BEGIN -->
<section class="row" id="donate">
<div class="col_16">
<div class="vintage-ribbon-wrapper">
<div class="vintage-ribbon seriftype inset-light"><span>
            	Donate Now           		
            </span></div>
</div>
</div>
<div class="donate-content col_16">
<div class="donation-button"><a href="https://animalguardian.org//how-to-help/donate/" title="Donations"><img alt="100% of your donations goes to helping the animals in our program as we have no paid staff. TAGS’s largest expense by far is the veterinary bills. We want to be sure that all our dogs are healthy, and sometimes this means expensive medications or surgeries that cost hundreds or even thousands of dollars. In addition, all TAGS dogs are spayed or neutered, vaccinated and microchipped prior to adoption. While TAGS is fortunate to receive some donations of dog food, we also spend money purchasing quality dog food to feed to our dogs in foster care. Other expenses include the production of training, educational and promotional material." src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com//wp-content/uploads/2013/03/dollarDonationCan11.jpg"/> </a></div><div class="donation-button"><a href="https://animalguardian.org//how-to-help/foster/" title="Foster Home"><img alt="MILLIONS OF DOGS
DIE EVERY YEAR...
The difference between life and death is a 
foster home.
We can’t save lives without YOU
If you can provide a temporary, loving home, 
we will supply food and medical care." src="http://dev.animalguardian.org/wp-content/uploads/2013/03/foster-home-300x203.jpg"/> </a></div><div class="donation-button"><a href="http://kuranda.com/donate/7027" title="Donate a Kuranda Bed"><img alt="Please consider purchasing and donating a shelter bed to TAGS.  These beds provide a comfortable resting place for shelter pets.  " src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2013/03/kuranda_final.jpg"/> </a></div> </div>
</section>
<!-- #DONATE END --> <!-- MAIN CONTENT BEGIN -->
<section class="home-page-content row">
<div class="col_8 article">
<h2 class="entry-title replace vintage-type"><a href="https://animalguardian.org/signs-your-cat-may-be-sick/" rel="bookmark" title="Permalink to Signs your Cat may be Sick">Signs your Cat may be Sick</a></h2>
<div class="figure"><a href="https://animalguardian.org/signs-your-cat-may-be-sick/"><img alt="" class="attachment-post-image-home size-post-image-home wp-post-image" height="250" loading="lazy" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2019/03/Calico_Cat_Sick-640x250.png" width="640"/> </a></div>
<p>~by Nomi Berger~ Has your puss-in-boots lost the spring in her step? Has he turned tail and taken off at your approach? Has she given her food a sudden “paws down”? Has the love light dimmed in his eyes Then, perhaps all’s not well in kitty town. The more attuned you are to your cat’s daily routines and behavior, the sooner you’ll notice even</p>
<p><a class="cta" href="https://animalguardian.org/signs-your-cat-may-be-sick/">Read More »</a></p>
</div>
<div class="col_8 article last">
<h2 class="entry-title replace vintage-type"><a href="https://animalguardian.org/twos-the-ticket/" rel="bookmark" title="Permalink to Two’s the Ticket">Two’s the Ticket</a></h2>
<div class="figure"><a href="https://animalguardian.org/twos-the-ticket/"><img alt="" class="attachment-post-image-home size-post-image-home wp-post-image" height="250" loading="lazy" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/uploads/2018/08/shutterstock_95676709_twokittens-640x250.jpg" width="640"/> </a></div>
<p>~ by Nomi Berger Question: Why are kittens born in litters? Answer: To be nourished and nurtured, socialized and stimulated. Question: Why should kittens be adopted in pairs? Answer: To KEEP them socialized and stimulated, and to satisfy their innate curiosity. From birth, close interaction with their mother and littermates allow kittens to mature into sociable and well-adjusted, healthy and happy cats. While separating</p>
<p><a class="cta" href="https://animalguardian.org/twos-the-ticket/">Read More »</a></p>
</div>
</section>
<!-- MAIN CONTENT END -->
</div>
<!-- PAGE CONTENT END -->
<!-- FOOTER BEGIN -->
<footer id="footer">
<div class="container">
<div class="row">
</div>
<!-- FOOTER WIDGETS END -->
<div class="row">
<!-- CREDIT BEGIN -->
<div class="footer-credit-wrapper">
<div class="footer-credit seriftype">
<span>
                    Copyright The Animal Guardian Society 2013. All Rights Reserved. | Powered by<a href="http://www.wordpress.org"> Wordpress</a> | Customized by <a href="http://www.webrite.ca">Webrite Design Solution</a> </span>
</div>
</div>
<!-- CREDIT END -->
<!-- SROLL TO TOP BEGIN-->
<p class="mobile-hide" id="back-top">
<a href="#top"> <span></span></a>
</p>
<!-- SROLL TO TOP END-->
</div>
</div>
</footer>
<!-- FOOTER END -->
<script id="custom-js" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-content/themes/bhinneka/js/p2-init.js?ver=5.6&amp;time=1610535032" type="text/javascript"></script>
<script id="comment-reply-js" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-includes/js/comment-reply.min.js?ver=5.6&amp;time=1610535032" type="text/javascript"></script>
<script id="wp-embed-js" src="https://secureservercdn.net/198.71.233.64/b09.755.myftpupload.com/wp-includes/js/wp-embed.min.js?ver=5.6&amp;time=1610535032" type="text/javascript"></script>
</body>
</html>