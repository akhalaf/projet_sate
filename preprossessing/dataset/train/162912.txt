<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "97517",
      cRay: "610ef57fc86c1ac0",
      cHash: "49de8d9f5142d90",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXNzb2NpYXRpb24xOTAxLmZyLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "kPc4DrG/RpcEcX9Wz9HKR+obeb9PwOkTe+90sJPtdARQ+DtGqchpBHrqVXNubHWw1gj2hsx95htNAgGPqlw5T5cVcLVQApIvO+vrsWo0y/NEcpMjz+yVRkwQkiVz5W45tutFBwUkqHVnYpGL0X31/e+A55SBQ4K10rlcuNjsJ+PXDfQibyEgS62p9OqtK4kA/7tZD4DibIG9e+H1P4PL7WqkbW2CugQ23ZYdNq06vE09kNBkJph2LW9RirvVoIO0nzBsYnUnkR46RyI/HNH4fnct5tJfEo/QiwXhHK6JQs5c8qwKaOW6uK9pmtDKr0vI0IqTM8esUV5t3OAkaOzqy9f42r8ENW85P4mJMBBY3E7GaE6yC4D2iaLElTYvTPrp8yldNGl9v4BHaxjoOR3qkmgsPXVOt/MziOeWuSSl7y+FGGlx6ip2zXjDCunRBnBgod838cm3cpRcAM3/AE3bp3G+W7vDibeYegqXxNZ0SBDlNfVIrAs//39hrAo7dv7QSfibq6b1HO+0VVcC6QtR5Ur5Xu1UAe5SFvORRWvNkqJFdF9A2F/PgVWzsmzD4KxJ+rmS+e4Ug+bJ8TbnXqZ7eLhgwpkzwZUxc21GcKm7Duk1i5OARTDh71esPh5fFkSEvj+n2mTLNArmasKxPWdn7tfb6FXEpGQUNk+zhW6PaEtn4oLiAzKymC5A+h9hICLf/UT3SlNt6aYZoxTtiTQZy/vh9WPGlL0VVfZ+a+FpzzU8/acza692e0giPbcldS9h",
        t: "MTYxMDUzOTI0OC42MDkwMDA=",
        m: "zOXyqUbCGq5PWm+cyAgGnLPCG9B9xA37eT5jk22cUjg=",
        i1: "JmBho/Ad4IUbEolwltlp0g==",
        i2: "G1fHain3ogA4lNXlnJWjxg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "G4OtqvCNF0Kj15GTeUNmjPCSnxrl1+CudX7ZAD7kXEw=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610ef57fc86c1ac0");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> association1901.fr.</h1>
<a href="https://chattard.com/grandfather.php?sid=6"><span style="display: none;">table</span></a>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=7c76f1a541927a816c685b352d2e8a2b34e01999-1610539248-0-AVXtjTM9F9GR0HvFI4hOE-YuWtFBxztP1C7ep4JzNnDTpPjWeI4FhJd5rLjxZkmob7U2wz-OTq7CQJJFGlpeKUsFCps6rb6TdkAPgHGdv-beTO4zf3KWsgExjir-REKuSO2oikheoNNfc3WCK1oe5SU9vU1LV_JUmklx8ubjNvEsg-MjQ1hmDAy5Tfdpx4xOJYj8jU7GKihxX0qYOEkbb_MU39Orev63ErnOQedfvDphUqZ751m-TFg-EoPRrcmHkN6derHGSYyyMg-o4oaOgA9_lg-LGqtq9iUDqM5FP4xvRHqBEiOAy_LndbAEigNiXA" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="cadd121796f6014c8333dee0ba3e0ae8c345d53f-1610539248-0-Ae/qmrnwmZ9uvo6ays0kB4lBVzOohGUTmxDbcjLk7Zia+TLDcIcpHMW4wiROwArmE4umkipX1MZL5X/2Fv7+ymESiD+aBCgn3C4Qgu4rbPX4RI7LgosJbYSMjWdHzg+DpbapDZG+DawuhHVss1yvmf/492MucN4UxGkTUZr+6+4NOmsU6yjL7slmMtBVoXv2JyHwWu3UEvqd08y7B2e6jQe3OL9h4i/jQge3pPRJlC3IO+sFsJjNicQ9ccZyS4q5LUvI1AU33CbUigMm+oflITuFLTnJllaP5BG0aVSMFRTLZqlfvx9KITwrr44cQviN2M0DsLTdKOMpPrP2hlerlYqIr8Z6sz6S+A+0gQ1ixoVUCEMyLxtgCzPm8KKj4klclRd+ErtBKelKlHXvgyORgihEb+aUiL3cKoyTeQwvtB6m2Chhl/pGKZOHNs+AQJHhK1h+Te9xLhaC98aTkkBqzwyS8q8ZYGflr0YhS+hd6qciIG5G+BdybJrs6mI6zbR8tfS5gCglTh+8BKyJMKysgU5qxWB9GXJnvQ+nUhUiwiks3cqUExmF8STj5tCwYCgFMmjHB2CeIFyw03JosRE/UaE4+JsfHe6E4Vp2aajZRilTD7kAWy1SH6NU1EJIuwmIa+kNrngySIpJ/pVDv93kNZqacnr3RK2ri3IW4DI9b1HV70F4MLowF3W7qto8MICXQdQ4EwfvxVU75lESQMR9eTDLb/hf+dNj/XFPSyR0k2p3iXUUOUsSSB9EFa0RCQmNsU3ryrDxeH+C70ArPvMQzDb1889/8KX6UttjLD06QEM5S1T6CxzLqaoOT4Pu6OdYE2p/vuEI/kyImVGv3NT7kBEghydxRujug2il9qghPv90tN8u9E1mJZMLt9sI7jP5cBNJ3tDj9LYJajZY0wCEet5f3dK4PdkRq8OlWhmm78+uUdsnm4yP8E+qd5DlSKka4xYHHvCCo1++hK7duvytcRbxG94d3liuBh/AsadplaRlgG7bNc0HVYJlavqMxI7sY+5/eZ8jSif+HUi3Pok6vfgu9BPw435OTQCYRxebG34Bq0EVpMis953k7+4BUKKq8hnKpT7wxdEm39GCrutY6fSShZcuUIIyfngtwDOAq2SJsU05iXdGMWboiamwGCpqvA7RSLXQGSe7ZwtcttDDn9aReqaF8r3x2WT7KrEjMx/NF0pX+eJDd/10umwXISGWV7v9Kjbfnaccxt4Yyovj2g2e8EGU7Y8jBRgnU1QE4WIGa20L4R30A+XTmpXni75L4DXdEwwUl4Kjvw3iIrF9u8ahK7Xj0g9kUqMjVsIUcn1NOG3STLDRztPedXmCZDc59w=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="eb291f69648ceffb1932be49067d5bb4"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610539252.609-SiHGcqYTpi"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610ef57fc86c1ac0')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610ef57fc86c1ac0</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
