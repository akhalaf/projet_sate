<!DOCTYPE html>
<html dir="ltr" lang="hr-hr" xml:lang="hr-hr" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<base href="http://creatus.hr/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="stanovi, prodaja stanova, Solin, građenje" name="keywords"/>
<meta content="inžinjering u graditeljstvu" name="description"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>Creatus - Naslovna</title>
<link href="/index.php?format=feed&amp;type=rss" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="/index.php?format=feed&amp;type=atom" rel="alternate" title="Atom 1.0" type="application/atom+xml"/>
<link href="/templates/creatus/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/plugins/system/jce/css/content.css?8b9061f96f39b2c6ab03418d7605aad2" rel="stylesheet" type="text/css"/>
<link href="/media/modals/css/bootstrap.min.css?v=11.7.0" rel="stylesheet" type="text/css"/>
<link href="/templates/creatus/css/template.css" rel="stylesheet" type="text/css"/>
<link href="http://www.creatus.hr/modules/mod_jfslideshow/assets/skitter/css/skitter.styles.css" rel="stylesheet" type="text/css"/>
<link href="http://www.creatus.hr/modules/mod_jfslideshow/assets/css/slideshow.css" rel="stylesheet" type="text/css"/>
<link href="http://www.creatus.hr/modules/mod_jfslideshow/assets/css/responsivetext.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
#mod_jfslideshow_wrapper{
	max-height: 560px !important;
}
	</style>
<script class="joomla-script-options new" type="application/json">{"csrf.token":"548b4465523da746bffe71d951572b3d","system.paths":{"root":"","base":""},"rl_modals":{"class":"modal_link","defaults":{"opacity":"0.8","maxWidth":"95%","maxHeight":"95%","current":"{current} \/ {total}","previous":"previous","next":"next","close":"close","xhrError":"This content failed to load.","imgError":"This image failed to load."},"auto_correct_size":1,"auto_correct_size_delay":0}}</script>
<script src="/media/jui/js/jquery.min.js?8b9061f96f39b2c6ab03418d7605aad2" type="text/javascript"></script>
<script src="/media/jui/js/jquery-noconflict.js?8b9061f96f39b2c6ab03418d7605aad2" type="text/javascript"></script>
<script src="/media/jui/js/jquery-migrate.min.js?8b9061f96f39b2c6ab03418d7605aad2" type="text/javascript"></script>
<script src="/media/system/js/caption.js?8b9061f96f39b2c6ab03418d7605aad2" type="text/javascript"></script>
<script src="/media/system/js/core.js?8b9061f96f39b2c6ab03418d7605aad2" type="text/javascript"></script>
<script src="/media/modals/js/jquery.touchSwipe.min.js" type="text/javascript"></script>
<script src="/media/modals/js/jquery.modals.min.js?v=11.7.0" type="text/javascript"></script>
<script src="/media/modals/js/script.min.js?v=11.7.0" type="text/javascript"></script>
<script src="/media/jui/js/bootstrap.min.js?8b9061f96f39b2c6ab03418d7605aad2" type="text/javascript"></script>
<script src="/templates/creatus/js/template.js" type="text/javascript"></script>
<script src="http://www.creatus.hr/modules/mod_jfslideshow/assets/skitter/js/skitter.min.jquery.js" type="text/javascript"></script>
<script src="http://www.creatus.hr/modules/mod_jfslideshow/assets/js/jquery.easing.min.js" type="text/javascript"></script>
<script src="http://www.creatus.hr/modules/mod_jfslideshow/assets/skitter/js/animate-colors-min.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(window).on('load',  function() {
				new JCaption('img.caption');
			});
	</script>
<link href="//fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet" type="text/css"/>
<style type="text/css">
			h1,h2,h3,h4,h5,h6,.site-title{
				font-family: 'Ubuntu', sans-serif;
			}
		</style>
<style type="text/css">
		body.site
		{
			border-top: 3px solid #00baff;
			background-color: #282828		}
		a
		{
			color: #00baff;
		}
		.navbar-inner, .nav-list > .active > a, .nav-list > .active > a:hover, .dropdown-menu li > a:hover, .dropdown-menu .active > a, .dropdown-menu .active > a:hover, .nav-pills > .active > a, .nav-pills > .active > a:hover,
		.btn-primary
		{
			background: #00baff;
		}
		.navbar-inner
		{
			-moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
			-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
			box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
		}
	</style>
<!--[if lt IE 9]>
		<script src="/media/jui/js/html5.js"></script>
	<![endif]-->
</head>
<body class="site com_content view-featured no-layout no-task itemid-102">
<!-- Body -->
<div class="body">
<!-- Header -->
<div class="headerbg"></div>
<header class="header" role="banner">
<div class="header-inner clearfix">
<a class="brand pull-left" href="/">
<span class="site-title" title="Creatus">Creatus</span> </a>
<div class="site-subtitle pull-left">
<div class="custom">
<p>inžinjering u graditeljstvu</p></div>
</div>
<div class="header-contact pull-right">
<div class="custom">
<p>Creatus d.o.o.<br/>inžinjering u graditeljstvu<br/>PPC 96, 21210 Solin</p></div>
</div>
<div class="header-search pull-right">
</div>
</div>
</header>
<div class="container">
<nav class="navigation" role="navigation">
<ul class="nav menu nav-pills mod-list">
<li class="item-102 default current active"><a href="/index.php">Naslovna</a></li><li class="item-103"><a href="/index.php/gaspini-2-objekt-a">Objekt A</a></li><li class="item-104"><a href="/index.php/gaspini-2-objekt-b">Objekt B</a></li><li class="item-129"><a href="/index.php/gaspini-2-objekt-c">Objekt C</a></li><li class="item-105"><a href="/index.php/reference">Reference</a></li><li class="item-106"><a href="/index.php/kontakt">Kontakt</a></li></ul>
</nav>
<div class="moduletable">
<div style="clear: both;"></div>
<div id="mod_jfslideshow_wrapper">
<div class="box_skitter mod_jfslideshow" id="mod_jfslideshow">
<ul>
<li>
<a href="#random"><img class="random" src="/images/gaspini2/Creatus-Solin2018_01.jpg"/></a> <div class="label_text">
<div class="label_skitter_container">
<h2 class=" text-shadow slideSloganText" style="font-size:18px; line-height:18px; color:#ffffff;">Novo u prodaji Gašpini 2!</h2> </div>
</div>
</li>
<li>
<a href="#random"><img class="random" src="/images/gaspini2/Creatus-Solin2018_02.jpg"/></a> <div class="label_text">
<div class="label_skitter_container">
<h2 class=" text-shadow slideSloganText" style="font-size:18px; line-height:18px; color:#ffffff;">stanovi u Solinu</h2> </div>
</div>
</li>
<li>
<a href="#random"><img class="random" src="/images/gaspini2/Creatus-Solin2018_03.jpg"/></a> <div class="label_text">
<div class="label_skitter_container">
<h2 class=" text-shadow slideSloganText" style="font-size:18px; line-height:18px; color:#ffffff;">od 1-sobnih do 3-sobnih</h2> </div>
</div>
</li>
<li>
<a href="#random"><img class="random" src="/images/gaspini2/Creatus-Solin2018_Z1-3.jpg"/></a> <div class="label_text">
<div class="label_skitter_container">
<h2 class=" text-shadow slideSloganText" style="font-size:18px; line-height:18px; color:#ffffff;">od 43 do 79m²</h2> </div>
</div>
</li>
<li>
<a href="#random"><img class="random" src="/images/gaspini2/Creatus-Solin2018_Z2-2.jpg"/></a> <div class="label_text">
<div class="label_skitter_container">
<h2 class=" text-shadow slideSloganText" style="font-size:18px; line-height:18px; color:#ffffff;">U mirnoj novoizgrađenoj ulici</h2> </div>
</div>
</li>
<li>
<a href="#random"><img class="random" src="/images/gaspini2/Creatus-Solin2018_Z2-3.jpg"/></a> <div class="label_text">
<div class="label_skitter_container">
<h2 class=" text-shadow slideSloganText" style="font-size:18px; line-height:18px; color:#ffffff;">Dobra prometna povezanost</h2> </div>
</div>
</li>
</ul>
</div>
<a class="jfprev_button prevsldide1" href="#">prev</a>
<a class="jfnext_button nextslide1" href="#">next</a>
<div class="jflabel_skitter"></div>
<div style="clear: both;"></div>
</div>
<div style="clear: both;"></div>
<script type="text/javascript">	
	var structure = '' +
		'<div class="container_skitter">' +
		'<div class="image">' +
		'<a target="_blank" href="/"><img class="image_main" alt=""/></a>' +		
		'</div>' +		
		'</div>' +
		'';
	jQuery('#mod_jfslideshow').css({'height':800, 'width':1200});
	jQuery('#mod_jfslideshow').skitter({
		
		animation: 'random',		
		numbers: false,		
		structure: structure,
		velocity: 1.3,
		interval: 3000,
		navigation: 0,
		auto_play: 1,
		responsive: true
	 });
</script> </div>
<div class="container2">
<div class="row-fluid">
<main class="span12" id="content" role="main">
<!-- Begin Content -->
<div id="system-message-container">
</div>
<div class="blog-featured" itemscope="" itemtype="https://schema.org/Blog">
<div class="items-leading clearfix">
<div class="leading-0 clearfix" itemprop="blogPost" itemscope="" itemtype="https://schema.org/BlogPosting">
<h3>O PROJEKTU:</h3>
<p>Objekti su naglašene južne orijentacije te prometno povezani. Smješteni su u neposrednoj blizini naselja ispod Gašpini 2 nasuprot ulaza u arheološko nalazište Salone. Na lokaciji je predviđena izgradnja tri višestambena objekata. U blizini se nalaze svi popratni sadržaji koji zadovoljavaju potrebe svakodnevnog života: vrtić, škola, trgovina, ljekarna, pekara, javni prijevoz, hotel Salona ... (<!-- START: Modals --><a class="modal_link" data-modal-class-name="no_title" data-modal-iframe="true" data-modal-inner-height="95%" data-modal-inner-width="95%" href="https://www.google.com/maps/d/embed?mid=1WF8AxM3iTfr_wLbH6CGeI13_r3VqGs5n" target="_blank">Pozicija na google maps</a><!-- END: Modals -->)</p>
<p>Koncepcija formiranja novog prostora /građevina/ proizašao je iz većeg oblika same lokacije na površini preko 4.500 m<sup>2</sup>, a u skladu s odredbama DPU-a gdje je predviđeno produljenje postojeće prometnice. Prema razmišljanju o kvalitetnoj dispoziciji prostora i samoj formi buduće građevine sa širokim rasponom različitih tipologija jednosobnih, dvosobnih i trosobnih stanova koje su osmišljene da zadovolje potrebe svojih budućih vlasnika i stanara. Na navedenoj lokaciji smještene su tri samostalne višestambene građevine s pripadajućim vanjskim i samostalnim garažnim parking mjestima.</p>
<p>Objekti su projektirani osluškujući potrebe, želje i mogućnosti kupaca pritom vodeći računa o maksimalnoj iskorištenosti prostora i organizaciji interijera. Kvalitetom izgradnje, vanjskim i unutarnjim uređenjem, kupcima su osigurana kvalitetna, funkcionalna i estetski prihvatljiva rješenja uređenja stana.</p>
<h3>Stambeni objekti:</h3>
<ul>
<li>Početak gradnje: listopad 2018.</li>
<li>Završetak gradnje: travanj 2020.</li>
<li>Ukupno etaža: 4 (Pr+3K)</li>
<li>Ukupno stanova u objektima: 52</li>
<li>Ukupno parking mjesta: 81</li>
<li>Ukupno garažnih mjesta: 7</li>
<li>Ukupno spremišta: 9</li>
<li>Ukupna bruto površina objekata: 3.512,00 m<sup>2</sup></li>
<li>Uz svaki stan parking mjesto</li>
<li>Smještaj objekata u mirnoj novoizgrađenoj ulici</li>
<li>uz dobru prometnu povezanost</li>
</ul>
<h4>Građevinske dozvole:</h4>
<p>SPLITSKO-DALMATINSKA ŽUPANIJA<br/>Upravni odjel za graditeljstvo i prostorno uređenje - Solin</p>
<p>Klasa: UP-I-361-03/18-01/000004<br/>Ur.br: 2181/1-11-00-06/04-18-0004 od 27.02.2018 godine,<br/>pravomoćna 24.03.2018. godine.</p>
<h3 class="brand">Stanovi se prodaju po sistemu<br/>„ključ u ruke"</h3>
<h3> </h3>
</div>
</div>
</div>
<!-- End Content -->
</main>
</div>
</div>
</div>
</div>
<!-- Footer -->
<footer class="footer" role="contentinfo">
<div class="container">
<p class="pull-right">
<a href="#top" id="back-top">
					Na vrh stranice				</a>
</p>
<p>
			© 2021 Creatus  - web &amp; design: <a href="http://www.2d3dstudio.hr">2D3D studio</a> </p>
</div>
</footer>
</body>
</html>
