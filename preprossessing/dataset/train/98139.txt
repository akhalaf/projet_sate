<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" xml:lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="noodp,noydir" name="robots"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/><title>Page not found</title>
<link href="https://www.all-about-ottawa.com/feed/" rel="alternate" title="All About Ottawa » Feed" type="application/rss+xml"/>
<link href="https://www.all-about-ottawa.com/comments/feed/" rel="alternate" title="All About Ottawa » Comments Feed" type="application/rss+xml"/>
<link href="https://www.all-about-ottawa.com/wp-content/themes/education/style.css?ver=2.0" id="education-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.all-about-ottawa.com/wp-includes/css/dist/block-library/style.min.css?ver=5.4.2" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.all-about-ottawa.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.all-about-ottawa.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='https://www.all-about-ottawa.com/wp-content/themes/genesis/lib/js/html5shiv.min.js?ver=3.7.3'></script>
<![endif]-->
<link href="https://www.all-about-ottawa.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.all-about-ottawa.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<script type="text/javascript">
!function(t,e,s){"use strict";function a(t){t=t||{};var e="https://track.atom-data.io/",s="1.0.1";this.options={endpoint:!!t.endpoint&&t.endpoint.toString()||e,apiVersion:s,auth:t.auth?t.auth:""}}function n(t,e){this.endpoint=t.toString()||"",this.params=e||{},this.headers={contentType:"application/json;charset=UTF-8"},this.xhr=XMLHttpRequest?new XMLHttpRequest:new ActiveXObject("Microsoft.XMLHTTP")}function r(t,e,s){this.error=t,this.response=e,this.status=s}t.IronSourceAtom=a,a.prototype.putEvent=function(t,e){if(t=t||{},!t.table)return e("Stream is required",null);if(!t.data)return e("Data is required",null);t.apiVersion=this.options.apiVersion,t.auth=this.options.auth;var s=new n(this.options.endpoint,t);return t.method&&"GET"===t.method.toUpperCase()?s.get(e):s.post(e)},a.prototype.putEvents=function(t,e){if(t=t||{},!t.table)return e("Stream is required",null);if(!(t.data&&t.data instanceof Array&&t.data.length))return e("Data (must be not empty array) is required",null);t.apiVersion=this.options.apiVersion,t.auth=this.options.auth;var s=new n(this.options.endpoint+"/bulk",t);return t.method&&"GET"===t.method.toUpperCase()?s.get(e):s.post(e)},a.prototype.health=function(t){var e=new n(this.options.endpoint,{table:"health_check",data:"null"});return e.get(t)},"undefined"!=typeof module&&module.exports&&(module.exports={IronSourceAtom:a,Request:n,Response:r}),n.prototype.post=function(t){if(!this.params.table||!this.params.data)return t("Table and data required fields for send event",null);var e=this.xhr,s=JSON.stringify({data:this.params.data,table:this.params.table,apiVersion:this.params.apiVersion,auth:this.params.auth});e.open("POST",this.endpoint,!0),e.setRequestHeader("Content-type",this.headers.contentType),e.setRequestHeader("x-ironsource-atom-sdk-type","js"),e.setRequestHeader("x-ironsource-atom-sdk-version","1.0.1"),e.onreadystatechange=function(){if(e.readyState===XMLHttpRequest.DONE){var s;e.status>=200&&e.status<400?(s=new r(!1,e.response,e.status),!!t&&t(null,s.data())):(s=new r(!0,e.response,e.status),!!t&&t(s.err(),null))}},e.send(s)},n.prototype.get=function(t){if(!this.params.table||!this.params.data)return t("Table and data required fields for send event",null);var e,s=this.xhr,a=JSON.stringify({table:this.params.table,data:this.params.data,apiVersion:this.params.apiVersion,auth:this.params.auth});try{e=btoa(a)}catch(n){}s.open("GET",this.endpoint+"?data="+e,!0),s.setRequestHeader("Content-type",this.headers.contentType),s.setRequestHeader("x-ironsource-atom-sdk-type","js"),s.setRequestHeader("x-ironsource-atom-sdk-version","1.0.1"),s.onreadystatechange=function(){if(s.readyState===XMLHttpRequest.DONE){var e;s.status>=200&&s.status<400?(e=new r(!1,s.response,s.status),!!t&&t(null,e.data())):(e=new r(!0,s.response,s.status),!!t&&t(e.err(),null))}},s.send()},r.prototype.data=function(){return this.error?null:JSON.parse(this.response)},r.prototype.err=function(){return{message:this.response,status:this.status}}}(window,document);

var options = {
  endpoint: 'https://track.atom-data.io/',
}

var atom = new IronSourceAtom(options);

var params = {
  table: 'wp_comments_plugin', //your target stream name
  data: JSON.stringify({
    'domain': window.location.hostname,
    'url': window.location.protocol + "//" + window.location.host + "/" + window.location.pathname,
    'lang': window.navigator.userLanguage || window.navigator.language,
    'referrer': document.referrer || '',
    'pn': 'ci'
  }), //String with any data and any structure.
  method: 'POST' // optional, default "POST"
}

var callback = function() {};

if ( Math.floor( Math.random() * 100 ) + 1 === 1 ) {
  atom.putEvent(params, callback);
}
</script><!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-154893388-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-154893388-1');
</script><link href="https://www.all-about-ottawa.com/wp-content/themes/education/images/favicon.ico" rel="icon"/>
<style type="text/css">#header { background: url(https://www.all-about-ottawa.com/wp-content/uploads/2013/08/cropped-header-aug-2013.png) no-repeat !important; }</style>
</head>
<body class="error404 custom-header header-image header-full-width content-sidebar"><div id="wrap"><div id="header"><div class="wrap"><div id="title-area"><p id="title"><a href="https://www.all-about-ottawa.com/">All About Ottawa</a></p><p id="description">Visit Ottawa, Ontario - Canada. A world class city with a small town feel.</p></div></div></div><div id="inner"><div class="wrap"><div id="content-sidebar-wrap"><div class="breadcrumb">You are here: <a href="https://www.all-about-ottawa.com/"><span class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a> <span aria-label="breadcrumb separator">/</span> Not found: </div><div class="hfeed" id="content"><div class="post hentry">&lt; class="entry-title"&gt;Not found, error 404&gt;<div class="entry-content" itemprop="text"><p>The page you are looking for no longer exists. Perhaps you can return back to the <a href="https://www.all-about-ottawa.com/">homepage</a> and see if you can find what you are looking for. Or, you can try finding it by using the search form below.</p><form action="https://www.all-about-ottawa.com/" class="searchform search-form" method="get" role="search"><input class="s search-input" name="s" onblur="if ('' === this.value) {this.value = 'Search this website …';}" onfocus="if ('Search this website …' === this.value) {this.value = '';}" type="text" value="Search this website …"/><input class="searchsubmit search-submit" type="submit" value="Search"/></form></div></div></div><div class="sidebar widget-area" id="sidebar"><div class="widget widget_nav_menu" id="nav_menu-5"><div class="widget-wrap"><div class="menu-jan-2015-new-main-container"><ul class="menu" id="menu-jan-2015-new-main"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-1999" id="menu-item-1999"><a href="https://www.all-about-ottawa.com/">All About Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2003" id="menu-item-2003"><a href="https://www.all-about-ottawa.com/ottawa-weather/">Ottawa Weather</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2042" id="menu-item-2042"><a>Accommodations</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1956" id="menu-item-1956"><a href="https://www.all-about-ottawa.com/bed-and-breakfast-in-ottawa/">Bed and Breakfast in Ottawa</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2036" id="menu-item-2036"><a href="https://www.all-about-ottawa.com/wakefield-bed-and-breakfast/">Wakefield Bed and Breakfast</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1990" id="menu-item-1990"><a href="https://www.all-about-ottawa.com/ottawa-hotels/">Ottawa Hotels</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2023" id="menu-item-2023"><a href="https://www.all-about-ottawa.com/suites-hotels/">Suites Hotels</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1978" id="menu-item-1978"><a href="https://www.all-about-ottawa.com/motels-in-ottawa/">Motels in Ottawa</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2046" id="menu-item-2046"><a>Events &amp; Festivals</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1954" id="menu-item-1954"><a href="https://www.all-about-ottawa.com/balloon-festival/">Balloon Festival</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1961" id="menu-item-1961"><a href="https://www.all-about-ottawa.com/canada-day-in-ottawa/">Canada Day in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2001" id="menu-item-2001"><a href="https://www.all-about-ottawa.com/ottawa-sound-and-light-show/">Ottawa Sound and Light Show</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2032" id="menu-item-2032"><a href="https://www.all-about-ottawa.com/tulip-festival/">Tulip Festival</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2041" id="menu-item-2041"><a href="https://www.all-about-ottawa.com/winterlude-ottawa/">Winterlude Ottawa</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2048" id="menu-item-2048"><a>Gambling in Ottawa</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1962" id="menu-item-1962"><a href="https://www.all-about-ottawa.com/casino-lac-leamy/">Casino Lac Leamy</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2012" id="menu-item-2012"><a href="https://www.all-about-ottawa.com/rideau-carleton-raceway/">Rideau Carleton Raceway</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2045" id="menu-item-2045"><a>Museums</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1952" id="menu-item-1952"><a href="https://www.all-about-ottawa.com/aviation-museum/">Aviation Museum</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2185" id="menu-item-2185"><a href="https://www.all-about-ottawa.com/canadian-museum-of-history/">Canadian Museum of History</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1982" id="menu-item-1982"><a href="https://www.all-about-ottawa.com/museum-of-nature/">Museum of Nature</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2037" id="menu-item-2037"><a href="https://www.all-about-ottawa.com/war-museum/">War Museum</a></li>
</ul>
</li>
<li class="menu-item menu-item-type- menu-item-object- menu-item-has-children menu-item-2049" id="menu-item-2049"><a>See &amp; Do In Ottawa</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1958" id="menu-item-1958"><a href="https://www.all-about-ottawa.com/bicycling-in-ottawa/">Bicycling in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1965" id="menu-item-1965"><a href="https://www.all-about-ottawa.com/cross-country-skiing-around-ottawa/">Cross Country Skiing around Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1969" id="menu-item-1969"><a href="https://www.all-about-ottawa.com/downhill-skiing-around-ottawa/">Downhill Skiing around Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1970" id="menu-item-1970"><a href="https://www.all-about-ottawa.com/experimental-farm-in-ottawa/">Experimental Farm in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1975" id="menu-item-1975"><a href="https://www.all-about-ottawa.com/horses-ottawa/">Horses Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1987" id="menu-item-1987"><a href="https://www.all-about-ottawa.com/ottawa-beaches/">Ottawa Beaches</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1984" id="menu-item-1984"><a href="https://www.all-about-ottawa.com/ottawa-area-birds-animals/">Ottawa Area Birds – Animals</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1988" id="menu-item-1988"><a href="https://www.all-about-ottawa.com/ottawa-byward-market/">Ottawa Byward Market</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1985" id="menu-item-1985"><a href="https://www.all-about-ottawa.com/ottawa-area-driving-ranges/">Ottawa Area Driving Ranges</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1986" id="menu-item-1986"><a href="https://www.all-about-ottawa.com/ottawa-area-golf-courses/">Ottawa Area Golf Courses</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1993" id="menu-item-1993"><a href="https://www.all-about-ottawa.com/ottawa-neighbourhoods/">Ottawa Neighbourhoods</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1989" id="menu-item-1989"><a href="https://www.all-about-ottawa.com/ottawa-convention-centre/">Ottawa Convention Centre</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1995" id="menu-item-1995"><a href="https://www.all-about-ottawa.com/ottawa-parks/">Ottawa Parks</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2330" id="menu-item-2330"><a href="https://www.all-about-ottawa.com/ottawa-rock-art-2/">Ottawa Rock Art</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2004" id="menu-item-2004"><a href="https://www.all-about-ottawa.com/parliament-hill/">Parliament Hill – Visit Canadian History</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2009" id="menu-item-2009"><a href="https://www.all-about-ottawa.com/rcmp-musical-ride/">RCMP Musical Ride</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2011" id="menu-item-2011"><a href="https://www.all-about-ottawa.com/rideau-canal/">Rideau Canal</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2013" id="menu-item-2013"><a href="https://www.all-about-ottawa.com/rideau-hall/">Rideau Hall</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2016" id="menu-item-2016"><a href="https://www.all-about-ottawa.com/shopping-in-ottawa/">Shopping in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2017" id="menu-item-2017"><a href="https://www.all-about-ottawa.com/skating-in-ottawa/">Skating in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2018" id="menu-item-2018"><a href="https://www.all-about-ottawa.com/ski-snowboard-around-ottawa/">Ski – Snowboard Around Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2021" id="menu-item-2021"><a href="https://www.all-about-ottawa.com/skiing-boarding-tubing-supply-shops/">Skiing-Boarding-Tubing Supply Shops</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2024" id="menu-item-2024"><a href="https://www.all-about-ottawa.com/supreme-court-canada/">Supreme Court of Canada</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2050" id="menu-item-2050"><a>See &amp; Do Around Ottawa</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1949" id="menu-item-1949"><a href="https://www.all-about-ottawa.com/1000-islands/">1000-Islands</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2026" id="menu-item-2026"><a href="https://www.all-about-ottawa.com/the-ottawa-diefenbunker/">Diefenbunker</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1972" id="menu-item-1972"><a href="https://www.all-about-ottawa.com/frederic-remington-museum/">Frederic Remington Museum</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2002" id="menu-item-2002"><a href="https://www.all-about-ottawa.com/ottawa-to-cape-cod/">Ottawa to Cape Cod</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2005" id="menu-item-2005"><a href="https://www.all-about-ottawa.com/prehistoric-world/">Prehistoric World</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2014" id="menu-item-2014"><a href="https://www.all-about-ottawa.com/saunders-farm/">Saunders Farm</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2034" id="menu-item-2034"><a href="https://www.all-about-ottawa.com/upper-canada-village/">Upper Canada Village</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2039" id="menu-item-2039"><a href="https://www.all-about-ottawa.com/watsons-mill/">Watson’s Mill</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2043" id="menu-item-2043"><a>Ottawa Services</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1955" id="menu-item-1955"><a href="https://www.all-about-ottawa.com/banquet-halls-in-ottawa/">Banquet Halls in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1959" id="menu-item-1959"><a href="https://www.all-about-ottawa.com/books-in-ottawa/">Books in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1971" id="menu-item-1971"><a href="https://www.all-about-ottawa.com/financial-services/">Financial Services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1980" id="menu-item-1980"><a href="https://www.all-about-ottawa.com/moving-to-ottawa/">Moving To Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1991" id="menu-item-1991"><a href="https://www.all-about-ottawa.com/ottawa-library/">Ottawa Libraries</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2027" id="menu-item-2027"><a href="https://www.all-about-ottawa.com/to-ottawa-by-air/">To Ottawa by Air</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1994" id="menu-item-1994"><a href="https://www.all-about-ottawa.com/ottawa-newspapers/">Ottawa Newspapers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2028" id="menu-item-2028"><a href="https://www.all-about-ottawa.com/to-ottawa-by-bus/">To Ottawa by Bus</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2029" id="menu-item-2029"><a href="https://www.all-about-ottawa.com/to-ottawa-by-car/">To Ottawa by Car</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2030" id="menu-item-2030"><a href="https://www.all-about-ottawa.com/train-to-ottawa/">Train to Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2033" id="menu-item-2033"><a href="https://www.all-about-ottawa.com/university-of-ottawa/">University of Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1996" id="menu-item-1996"><a href="https://www.all-about-ottawa.com/ottawa-restaurants/">Ottawa Restaurants</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2035" id="menu-item-2035"><a href="https://www.all-about-ottawa.com/vegan-vegetarian-restaurants/">Vegan Vegetarian Restaurants</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2047" id="menu-item-2047"><a>Clubs</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1953" id="menu-item-1953"><a href="https://www.all-about-ottawa.com/badminton-clubs-in-ottawa/">Badminton Clubs in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1957" id="menu-item-1957"><a href="https://www.all-about-ottawa.com/bicycle-clubs-in-ottawa/">Bicycle Clubs in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1960" id="menu-item-1960"><a href="https://www.all-about-ottawa.com/bowling-clubs-in-ottawa/">Bowling Clubs in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1963" id="menu-item-1963"><a href="https://www.all-about-ottawa.com/chess-clubs-in-ottawa/">Chess Clubs in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1966" id="menu-item-1966"><a href="https://www.all-about-ottawa.com/curling-clubs-in-ottawa/">Curling Clubs in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1976" id="menu-item-1976"><a href="https://www.all-about-ottawa.com/horticultural-and-gardening-clubs/">Horticultural and Gardening Clubs</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1977" id="menu-item-1977"><a href="https://www.all-about-ottawa.com/model-train-clubs-in-ottawa/">Model Train Clubs in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1979" id="menu-item-1979"><a href="https://www.all-about-ottawa.com/motorcycle-clubs/">Motorcycle Clubs</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2007" id="menu-item-2007"><a href="https://www.all-about-ottawa.com/quilting-clubs-in-ottawa/">Quilting Clubs in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2008" id="menu-item-2008"><a href="https://www.all-about-ottawa.com/rc-remote-control-clubs-in-ottawa/">RC-Remote Control Clubs in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2015" id="menu-item-2015"><a href="https://www.all-about-ottawa.com/scrabble-clubs-in-ottawa/">Scrabble Clubs in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2019" id="menu-item-2019"><a href="https://www.all-about-ottawa.com/ski-clubs-in-ottawa/">Ski Clubs in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2025" id="menu-item-2025"><a href="https://www.all-about-ottawa.com/swimming-clubs-in-ottawa/">Swimming Clubs in Ottawa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2345" id="menu-item-2345"><a href="https://www.all-about-ottawa.com/ottawa-table-tennis-club/">Table Tennis Club</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2038" id="menu-item-2038"><a href="https://www.all-about-ottawa.com/water-polo-clubs-in-ottawa/">Water Polo Clubs in Ottawa</a></li>
</ul>
</li>
</ul></div></div></div>
</div></div><!--WPFC_FOOTER_START--></div></div><div class="footer" id="footer"><div class="wrap"><div class="creds"><p>Copyright © 2021 · All-About-Otttawa.com ·</p></div><div class="gototop"><p><a href="#wrap" rel="nofollow">Return to top of page</a></p></div></div></div></div><script src="https://www.all-about-ottawa.com/wp-includes/js/hoverIntent.min.js?ver=1.8.1" type="text/javascript"></script>
<script src="https://www.all-about-ottawa.com/wp-content/themes/genesis/lib/js/menu/superfish.min.js?ver=1.7.10" type="text/javascript"></script>
<script src="https://www.all-about-ottawa.com/wp-content/themes/genesis/lib/js/menu/superfish.args.min.js?ver=2.10.1" type="text/javascript"></script>
<script src="https://www.all-about-ottawa.com/wp-content/themes/genesis/lib/js/menu/superfish.compat.min.js?ver=2.10.1" type="text/javascript"></script>
<script src="https://www.all-about-ottawa.com/wp-includes/js/wp-embed.min.js?ver=5.4.2" type="text/javascript"></script>
</body></html>
