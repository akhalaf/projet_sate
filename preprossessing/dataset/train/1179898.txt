<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Ошибка 404</title>
<meta content="" name="keywords"/>
<meta content="" name="description"/>
<link href="https://189131.selcdn.ru/leonardo/uploadsForSiteId/200009/siteFavicon/639c027f-d5d7-4951-9342-fc09b0efb499.png" rel="shortcut icon" type="image/x-icon"/>
<meta content="" name="csrf-token"/>
<link href="/css/app.css?id=160b8dcf9b8c91a50645" rel="stylesheet"/>
<link href="/css/detsad.css?id=26e583769100ecc13271" rel="stylesheet"/>
</head>
<body>
<style>
    body {
        background: url('/img/backgrounds/color (64).png') !important;
    }
    body {
  color: rgba(255, 255, 255, 1);
  background: url('/assets/uploads/backgrounds/27 _bg_fon') repeat; }

h1 > a, h2 > a, h3 > a, h4 > a, h5 > a {
  color: rgba(251, 143, 97, 1); }

hr {
  border-top: 1px solid rgba(255, 255, 255, 1); }

.breadcrumb {
  background-color: #f5f5f5; }

.list-group-item {
  background-color: #f5f5f5;
  border: 1px solid rgba(255, 255, 255, 1); }

a {
  color: rgba(251, 143, 97, 1); }

a:hover {
  color: rgba(251, 143, 97, 1); }

.footer {
  background-color: rgba(49, 100, 162, 1); }

.panel {
  margin-bottom: 0px;
  background-color: rgba(46, 97, 162, 1);
  border-left: none;
  border-right: none;
  border-top: none;
  border-radius: 0px;
  -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
  box-shadow: 0 1px 1px rgba(0, 0, 0, .05); }

.panel-white {
  background-color: rgba(46, 97, 162, 1);
  padding: 1px; }

.container-panel {
  background: rgba(46, 97, 162, 1); }

</style>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49881079 = new Ya.Metrika({id:49881079,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true});
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img alt="" src="//mc.yandex.ru/watch/49881079" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<div>
<div class="body-container">
<div class="error-container">
<div class="green-error-box">
<a href="/"><img alt="Сожалеем, но такой страницы не существует..." class="error-image" height="335" src="/theme/server_errors/tv_404.jpg" width="325"/></a>
<h1 class="heading" id="page-not-found">Страница не существует...</h1>
<h2>Вероятно она была удалена или перемещена автором, либо ее здесь никогда небыло.</h2>
<ul id="steps">
<!--<li>Revisit the <a href="" onclick="history.go(-1)" >previous page</a></li>-->
<li>Перейти на <a href="/">Главную страницу</a></li>
</ul>
<form action="/search" id="search-box" method="get">
<p>Искать на сайте</p>
<input id="q" name="q" placeholder="Поисковая фраза" type="text"/>
<a class="button-base button-black_orange_greenbg" href="#" id="topSearchOptions" onclick="jQuery('#search-box').submit();"><span>Искать</span></a>
</form>
</div>
</div>
</div>
<admin-app></admin-app>
<public-app></public-app>
</div>
<script>
  window.Laravel = {"csrfToken":null};
  window.Leonardo = {"isLocalServer":false,"isMySite":false,"requestUrl":"http:\/\/profi-ska.ru\/wp-includes\/%09%0A","domain":"http:\/\/profi-ska.ru","user":null,"userRoles":null,"site":{"id":200009,"name":"\u0421\u0435\u0432\u0435\u0440\u043e-\u041a\u0430\u0432\u043a\u0430\u0437\u0441\u043a\u0430\u044f \u043c\u0435\u0436\u043e\u0442\u0440\u0430\u0441\u043b\u0435\u0432\u0430\u044f \u0430\u043a\u0430\u0434\u0435\u043c\u0438\u044f \u043f\u043e\u0432\u044b\u0448\u0435\u043d\u0438\u044f \u043a\u0432\u0430\u043b\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u0438, \u043f\u043e\u0434\u0433\u043e\u0442\u043e\u0432\u043a\u0438 \u0438 \u043f\u0435\u0440\u0435\u043f\u043e\u0434\u0433\u043e\u0442\u043e\u0432\u043a\u0438 \u043a\u0430\u0434\u0440\u043e\u0432","url":"profi-ska.ru","nas_punkt":null,"contact":null,"street":null,"family_id":null,"parent_id":null,"etalon_id":null,"general_cat_id":null,"site_type":null,"template_name":"detsad","metrika":"49881079","analytics":null,"ya_webmaster":null,"g_webmaster":null,"blocks_data":{"top_title":"<p align=\"center\" style=\"text-align:center; margin:0cm 0cm 10pt\">&nbsp;<\/p>\n\n<p align=\"center\" style=\"text-align:center; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><b><i><span style=\"font-size:22.5pt\"><span style=\"font-family:&quot;Monotype Corsiva&quot;\">&laquo;\u0421\u0435\u0432\u0435\u0440\u043e-\u041a\u0430\u0432\u043a\u0430\u0437\u0441\u043a\u0430\u044f \u043c\u0435\u0436\u043e\u0442\u0440\u0430\u0441\u043b\u0435\u0432\u0430\u044f \u0430\u043a\u0430\u0434\u0435\u043c\u0438\u044f \u043f\u043e\u0432\u044b\u0448\u0435\u043d\u0438\u044f \u043a\u0432\u0430\u043b\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u0438, \u043f\u043e\u0434\u0433\u043e\u0442\u043e\u0432\u043a\u0438 \u0438 \u043f\u0435\u0440\u0435\u043f\u043e\u0434\u0433\u043e\u0442\u043e\u0432\u043a\u0438 \u043a\u0430\u0434\u0440\u043e\u0432&raquo;<\/span><\/span><\/i><\/b><\/span><\/span><\/span><\/p>\n\n<p align=\"center\" style=\"text-align:center; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><b><i><span style=\"font-size:22.5pt\"><span style=\"font-family:&quot;Monotype Corsiva&quot;\">\u041d\u041e\u0427\u0423 \u0414\u041f\u041e<\/span><\/span><\/i><\/b><\/span><\/span><\/span><\/p>","lastupdates":"<span><\/span>","recentworks":"<span><\/span>","welcomemessage":"<p align=\"center\" style=\"margin-top:0cm; margin-right:7.1pt; margin-bottom:.0001pt; margin-left:0cm; text-align:center; margin:0cm 0cm 10pt\"><span style=\"font-size:28px;\"><span style=\"line-height:150%\"><span style=\"font-family:Calibri,sans-serif\"><b><span style=\"line-height:150%\"><span style=\"font-family:&quot;Monotype Corsiva&quot;\"><span style=\"color:#365f91\">\u0414\u041e\u0411\u0420\u041e \u041f\u041e\u0416\u0410\u041b\u041e\u0412\u0410\u0422\u042c!<\/span><\/span><\/span><\/b><\/span><\/span><\/span><\/p>","servicesitems_list":"<span><\/span>","footer_contact_item":"<p>\n                                \u0433. \u041a\u0440\u0430\u0441\u043d\u043e\u0434\u0430\u0440<br>\n                                \u0443\u043b. \u0421\u0435\u043b\u0435\u0437\u043d\u0435\u0432\u0430, 203<br>\n                               \u0422\u0435\u043b\u0435\u0444\u043e\u043d: 8-861-210 31 82<br>\n                                Email: <a href=\"index.html#\">s-k_akadem@mail.ru <\/a><\/p>","header_right_images":"<span><\/span>","header_features_list":"<ul><li class=\"animated slideInLeft third\"><span><i class=\"fa fa-tablet\"><\/i><a href=\"https:\/\/skakadem.grandclass.net\/login\/?next=\/app\/course\" target=\"_blank\">&quot;\u041b\u0438\u0447\u043d\u044b\u0439 \u043a\u0430\u0431\u0438\u043d\u0435\u0442&quot;<\/a><\/span><\/li><\/ul>"},"config_template":{"head":null,"item":"templater-9-3-skvoznaya","logo":"https:\/\/189131.selcdn.ru\/leonardo\/uploadsForSiteId\/200009\/siteLogo\/e002b9b7-3f64-4c8d-8cac-a63fdd5dc345.png","menu":"default_no_muare","index":"3-6-3","colors":{"color-body":"rgba(46, 97, 162, 1)","color-text":"rgba(255, 255, 255, 1)","color-block":"rgba(46, 97, 162, 1)","color-footer":"rgba(49, 100, 162, 1)","color-link-hover":"rgba(251, 143, 97, 1)","color-link-default":"rgba(251, 143, 97, 1)","color-panel-border":"rgba(255, 255, 255, 1)"},"favicon":"https:\/\/189131.selcdn.ru\/leonardo\/uploadsForSiteId\/200009\/siteFavicon\/639c027f-d5d7-4951-9342-fc09b0efb499.png","menu_type":"bootstrap","background":"color (64).png","head_image":"https:\/\/189131.selcdn.ru\/leonardo\/uploadsForSiteId\/200009\/siteHeader\/0066f350-e0e5-47e5-8924-975dfe691742.jpg","customStyle":null,"ya_metrika_is_show":true},"migration_data":null,"active":true,"special_version":true,"expired_at":null,"deleted_at":null,"created_at":"2018-05-06 00:04:37","updated_at":"2021-01-02 19:55:03","ssl_enabled":true,"ssl_try_maked_at":"2020-08-01 01:00:02","ssl_expired_at":"2020-08-03 18:00:22","templates":null,"is_v2_active":false,"index_grid_id":1,"content_grid_id":3,"family":[]}};
</script>
<script src="/js/bootstrap.js?id=840dfe0f78d4f360a658"></script>
<script src="/js/all.js?id=da071483039468539344"></script>
<script src="/js/public-app.js?id=9956da0b3f4c2a38c7c1"></script>
</body>
</html>
