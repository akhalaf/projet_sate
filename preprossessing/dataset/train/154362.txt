<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="An artificial intelligence powered content writer that automatically generates complete high-quality, unique, SEO-friendly articles in less than 60 seconds." name="description"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<title>High quality, AI-powered article creation - Article Forge</title>
<!-- favicon icon -->
<link href="https://www.articleforge.com/ico/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="https://www.articleforge.com/ico/apple-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="https://www.articleforge.com/ico/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="https://www.articleforge.com/ico/apple-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="https://www.articleforge.com/ico/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="https://www.articleforge.com/ico/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="https://www.articleforge.com/ico/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="https://www.articleforge.com/ico/apple-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="https://www.articleforge.com/ico/apple-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://www.articleforge.com/ico/android-icon-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="https://www.articleforge.com/ico/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="https://www.articleforge.com/ico/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="https://www.articleforge.com/ico/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<!-- inject css start -->
<!--== bootstrap -->
<link href="https://www.articleforge.com/css/bootstrap.min.css?c033b45d3a7250ceb21c53590c7e564c" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Cabin:400,400i,500,500i,600,600i,700,700i" rel="stylesheet"/>
<!--== animate -->
<link href="https://www.articleforge.com/css/animate.css?c033b45d3a7250ceb21c53590c7e564c" rel="stylesheet" type="text/css"/>
<!--== fontawesome -->
<link href="https://www.articleforge.com/css/fontawesome-all.css?c033b45d3a7250ceb21c53590c7e564c" rel="stylesheet" type="text/css"/>
<!--== line-awesome -->
<link href="https://www.articleforge.com/css/line-awesome.min.css?c033b45d3a7250ceb21c53590c7e564c" rel="stylesheet" type="text/css"/>
<!--== magnific-popup -->
<link href="https://www.articleforge.com/css/magnific-popup/magnific-popup.css?c033b45d3a7250ceb21c53590c7e564c" rel="stylesheet" type="text/css"/>
<!--== owl-carousel -->
<link href="https://www.articleforge.com/css/owl-carousel/owl.carousel.css?c033b45d3a7250ceb21c53590c7e564c" rel="stylesheet" type="text/css"/>
<!--== base -->
<link href="https://www.articleforge.com/css/base.css?c033b45d3a7250ceb21c53590c7e564c" rel="stylesheet" type="text/css"/>
<!--== shortcodes -->
<link href="https://www.articleforge.com/css/shortcodes.css?c033b45d3a7250ceb21c53590c7e564c" rel="stylesheet" type="text/css"/>
<!--== default-theme -->
<link href="https://www.articleforge.com/css/style.css?c033b45d3a7250ceb21c53590c7e564c" rel="stylesheet" type="text/css"/>
<!--== responsive -->
<link href="https://www.articleforge.com/css/responsive.css?c033b45d3a7250ceb21c53590c7e564c" rel="stylesheet" type="text/css"/>
<!--== color -->
<link href="https://www.articleforge.com/css/theme-color/orange.css?c033b45d3a7250ceb21c53590c7e564c" rel="stylesheet" type="text/css"/>
<!-- inject css end -->
<script type="text/javascript">
        var _kmq = _kmq || [];
        var _kmk = _kmk || '5f7eb23adea47aecff7b4a126b6393d97a817138';
        function _kms(u){
          setTimeout(function(){
            var d = document, f = d.getElementsByTagName('script')[0],
            s = d.createElement('script');
            s.type = 'text/javascript'; s.async = true; s.src = u;
            f.parentNode.insertBefore(s, f);
          }, 1);
        }
        _kms('//i.kissmetrics.com/i.js');
        _kms('//scripts.kissmetrics.com/' + _kmk + '.2.js');
        _kmq.push(['record', 'visit home']);
        _kmq.push(["set", {"SPT2-12 Flat Header vs Product Dropdown": "Flat Header"}]);
_kmq.push(["set", {"SPT2-09 AI Mention in Pricing Block": "Yes AI mention"}]);
_kmq.push(["set", {"SPT2-04c Home Video Placement": "Video Hero"}]);
_kmq.push(["set", {"SPT2-06b Testimonials": "No Testimonials"}]);
_kmq.push(["set", {"SPT2-08 Content Woes Block Length": "Short Content"}]);
_kmq.push(["set", {"SPT2-11 Uniqueness Block": "Yes Uniqueness Block"}]);

    </script>
<!-- amCharts javascript sources -->
<script src="https://www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
<script src="https://www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
<script src="https://www.amcharts.com/lib/3/plugins/responsive/responsive.min.js"></script>
</head>
<body class="home-5">
<!-- page wrapper start -->
<div class="page-wrapper"> <!-- preloader start -->
<div id="ht-preloader">
<div class="loader clear-loader">
<div class="loader-box"></div>
<div class="loader-box"></div>
<div class="loader-box"></div>
<div class="loader-box"></div>
<div class="loader-wrap-text">
<div class="text"><span>A</span><span>I</span><span>A</span><span>I</span><span>A</span><span>I</span>
</div>
</div>
</div>
</div>
<!-- preloader end --> <!--header start-->
<header class="header" id="site-header">
<div class="" id="header-wrap">
<div class="container">
<div class="row">
<div class="col-lg-12">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg">
<a class="navbar-brand logo" href="https://www.articleforge.com/">
<img alt="" class="img-center" id="logo-img" src="https://www.articleforge.com/images/logo-transparent.png"/>
</a>
<button aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarNavDropdown" data-toggle="collapse" type="button"> <span></span>
<span></span>
<span></span>
</button>
<div class="collapse navbar-collapse" id="navbarNavDropdown">
<!-- Left nav -->
<ul class="nav navbar-nav ml-auto mr-auto" id="main-menu">
<li class="nav-item"> <a class="nav-link " href="https://www.articleforge.com/how-it-works">How it Works</a>
</li>
<li class="nav-item"><a class="nav-link " href="https://www.articleforge.com/case-studies/">Case Studies</a>
</li>
<li class="nav-item"><a class="nav-link " href="https://www.articleforge.com/pricing">Pricing</a>
</li>
<li class="nav-item"><a class="nav-link " href="https://www.articleforge.com/contact">Contact</a>
</li>
<li class="nav-item"> <a class="nav-link " href="https://www.articleforge.com/login">Login</a>
</li>
</ul>
</div>
<a class="btn btn-theme btn-sm trial-btn-header" data-text="Try it free" href="https://www.articleforge.com/pricing"> <span>Try </span><span>it </span><span>free</span>
</a>
</nav>
</div>
</div>
</div>
</div>
</header>
<!--header end-->
<!--hero section start-->
<section class="fullscreen-banner banner p-0">
<div class="align-center pt-0">
<div class="container">
<div class="row align-items-center">
<div class="col-lg-6 col-md-12 img-side text-right">
<img alt="" src="images/banner/05.png" style="max-width: 930px; width: 100%;"/>
<div class="video-box">
<div class="video-btn video-btn-pos"> <a class="play-btn popup-youtube main-video" href="https://www.youtube.com/watch?v=hISMownTovo?&amp;autoplay=1&amp;rel=0&amp;modestbranding=1"><i class="la la-play"></i></a>
<div class="spinner-eff">
<div class="spinner-circle circle-1"></div>
<div class="spinner-circle circle-2"></div>
</div>
</div>
</div> </div>
<div class="col-lg-6 col-md-12 ml-auto md-mt-5 md-mb-7">
<h1 class="mb-4">Get <span class="font-weight-bold text-uppercase">High Quality</span> Content In Under 60 Seconds</h1>
<p class="lead mb-4">Using advanced artificial intelligence and deep learning, Article Forge writes completely unique, on-topic, high-quality articles with the click of a button.</p>
<a class="btn btn-theme trial-btn" data-text="Start my free trial!" href="#pricing" style="visibility: visible;"> <span>Start </span><span>my </span><span>free </span><span>trial!</span>
</a>
</div>
</div>
</div>
</div>
<svg class="wave-animation" preserveaspectratio="none" viewbox="0 24 150 28" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
<defs>
<path d="M-160 44c30 0
      58-18 88-18s
      58 18 88 18
      58-18 88-18
      58 18 88 18
      v44h-352z" id="gentle-wave"></path>
</defs>
<g class="wave-bg">
<use fill="#f8f8f8" x="50" xlink:href="#gentle-wave" y="0"></use>
<use fill="#fbfbfb" x="50" xlink:href="#gentle-wave" y="3"></use>
<use fill="#ffffff" x="50" xlink:href="#gentle-wave" y="6"></use>
</g>
</svg>
</section>
<!--hero section end-->
<!--body content start-->
<div class="page-content">
<!--about start-->
<section class="p-0 pos-r" data-bg-img="images/pattern/03.png" id="video" style='background-image: url("images/pattern/03.png"); visibility: visible;'>
<div class="container">
<div class="row text-center">
<div class="col-lg-10 col-md-12 ml-auto mr-auto">
<h3 class="text-black font-weight-normal line-h-2">Article Forge uses <span class="text-theme">artificial intelligence</span> to write <span class="text-theme">unique content</span> with the same quality as a <span class="text-theme">human</span> for a fraction of the <span class="text-theme">cost</span>.</h3>
</div>
</div>
</div>
</section>
<!--about end-->
<section class="pos-r pt-10" data-bg-img="images/pattern/03.png">
<div class="container">
<div class="row align-items-center">
<div class="col-lg-6 col-md-12">
<img alt="" class="img-fluid w-100" src="images/svg/03.svg"/>
</div>
<div class="col-lg-6 col-md-12 md-mt-5">
<div class="section-title mb-3">
<h2 class="">Let Artificial Intelligence Solve Your Content Woes</h2>
</div>
<p class="lead mb-2">Content is expensive and time-consuming to create, but it doesn't have to be. Article Forge's artificial intelligence powered content writer was born out of five years of research, and its technology allows our users to generate high-quality articles with the click of a button.</p>
</div>
</div>
</div>
</section>
<!--tab start-->
<section class="pos-r o-hidden" id="samples">
<div class="row text-center">
<div class="col-lg-8 col-md-12 ml-auto mr-auto">
<div class="section-title">
<h2 class="title">Article Forge Samples</h2>
<p>All of the below content was written automatically by Article Forge:</p>
</div>
</div>
</div>
<div class="bg-animation">
<img alt="" class="" src="images/pattern/03.png"/>
</div>
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="tab style-2" style="background:white;">
<!-- Nav tabs -->
<nav>
<div class="nav nav-tabs" id="nav-tab" role="tablist">
<a aria-selected="true" class="nav-item nav-link active" data-toggle="tab" href="#tab1-1" id="nav-tab1" role="tab"> <i class="flaticon-software"></i>
<h5>Technology Article</h5>
</a> <a aria-selected="false" class="nav-item nav-link" data-toggle="tab" href="#tab1-2" id="nav-tab2" role="tab"><i class="flaticon-research"></i> <h5>Local Article</h5></a>
<a aria-selected="false" class="nav-item nav-link" data-toggle="tab" href="#tab1-3" id="nav-tab3" role="tab"><i class="flaticon-briefing"></i> <h5>Business Article</h5></a>
<a aria-selected="false" class="nav-item nav-link" data-toggle="tab" href="#tab1-4" id="nav-tab4" role="tab"><i class="flaticon-happiness"></i> <h5>Health Article</h5></a>
<a aria-selected="false" class="nav-item nav-link" data-toggle="tab" href="#tab1-5" id="nav-tab5" role="tab"><i class="flaticon-price-tag"></i> <h5>Finance Article</h5></a>
</div>
</nav>
<!-- Tab panes -->
<div class="tab-content" id="nav-tabContent">
<div class="tab-pane fade show active" id="tab1-1" role="tabpanel">
<div class="row align-items-center">
<div class="col-lg-8 offset-lg-2 col-md-12 md-mt-5">
<h4 class="mb-4">The Advantages and Disadvantages of Bitcoin</h4>
<p class="mb-4">
                    Bitcoin is the hottest thing in the world of money today. It's a virtual currency that has captured the attention of so many people who are very interested to know more about it. It's basically a new way of acquiring money with no central authority. No country or financial institution acts as a middleman in this process which makes this currency secure and easy to use. All you need to do is store your Bitcoins on your computer and generate them using software with a process called "mining".
                    <br/><br/>
                    The advantages of using Bitcoins are so many and they will surely take your fancy. The first advantage of using Bitcoins is the low cost of transaction fees. They are less than a penny per transaction making it easy for anyone to start mining some extra money. Another benefit is how easy it is to keep track of your earnings and expenditures through online accounts. Plus with this currency it's possible to transfer funds without ever leaving the comfort of your home.
                    <br/><br/>
                    Of course there are disadvantages to using Bitcoins as well. One disadvantage is the speed at which the coins are created and circulated within the network. If you want to make transactions for extremely large amounts, this can become a big problem. Also one must make sure to keep one's own private keys safe to avoid losing all of one's coins.
                    <br/><br/>
<small><b>
<span class="text-theme">Main keyword:</span> bitcoin
                      <br/>
<span class="text-theme">Sub keywords:</span> advantages, disadvantages
                    </b></small>
</p>
</div>
</div>
</div>
<div class="tab-pane fade" id="tab1-2" role="tabpanel">
<div class="row align-items-center">
<div class="col-lg-8 offset-lg-2 col-md-12 md-mt-5">
<h4 class="mb-4">How to Find a Baltimore Plumber</h4>
<p class="mb-4">
                    So you want to know where to find a Baltimore plumber? No matter what you're looking for there are plenty of ways to get the information you need to be able to find the best plumber out there. Whether you're looking for someone to help you fix a broken water line or just want to figure out where to find the best plumber, you need to be able to do your research and find the plumber that's right for you. What's the best way to find a Baltimore plumber? Well it's easy isn't it?
                    <br/><br/>
                    Before you start your search, you should first check out the BBB (Better Business Bureau) website to see if there's a Baltimore plumber that has been rated the best in Baltimore; they have a directory of Maryland licensed plumbers in general. You can click on the plumbers you want to check out on the website. You can get a free estimate, a list of their information and links to their websites. There are even links to several review websites if you're interested in finding out how the person has been rated.
                    <br/><br/>
                    You can also do your research online to find a Baltimore plumber. There are many websites that can tell you how they have been rated. You can even make a list of the plumbers you want to check out and get an estimate. With just a little bit of research and time, you should be able to find the best plumber for you and your needs.
                    <br/><br/>
<small><b>
<span class="text-theme">Main keyword:</span> Baltimore plumber
                    </b></small>
</p>
</div>
</div>
</div>
<div class="tab-pane fade" id="tab1-3" role="tabpanel">
<div class="row align-items-center">
<div class="col-lg-8 offset-lg-2 col-md-12 md-mt-5">
<h4 class="mb-4">Rustic Living Room Furniture</h4>
<p class="mb-4">Why settle for standard living room furniture when you can get a stunning piece of rustic furniture that is a one of a kind piece of art. The wood used in the Elite Furniture Warehouse is sourced from world class plantations where only the best timber is used. By staying true to its name of Elite Furniture Warehouse these pieces of furniture are built to last. Each piece is constructed to the highest degree of quality, and are also carefully chosen by the experienced hands of the Elite Furniture Warehouse staff to make sure that each piece is selected based on the texture, shape and style of each piece of furniture and has been painstakingly sorted out for long-term use.
                    <br/><br/>
                    The advantage of using quality timber from these forest plantations is that it maintains the natural color and grain of the wood, making it extremely durable and long-lasting. In addition, the exotic tropical forests used to source the wood for furniture are generally protected from logging and it also means that the timber is protected from pests, including termites, mould and mites.
                    <br/><br/>
                    A piece of furniture such as this goes a long way to transforming your living room into an oasis. When you walk into your living room you will see first-hand the beauty of the furniture, with its touches of natural wood - the hand-made details, its styling and the natural grain of the wood that so perfectly suits a rustic living room. The Elite Furniture Warehouse has a range of rustic living room furniture that is sure to transform the look of your living room, all hand crafted to fit in with the rustic or modern setting.
                    <br/><br/>
<small><b>
<span class="text-theme">Main keyword:</span> rustic living room furniture
                      <br/>
<span class="text-theme">Sub keyword:</span> Elite Furniture Warehouse (this could be replaced by the name of your company!)
                    </b></small></p>
</div>
</div>
</div>
<div class="tab-pane fade" id="tab1-4" role="tabpanel">
<div class="row align-items-center">
<div class="col-lg-8 offset-lg-2 col-md-12 md-mt-5">
<h4 class="mb-4">How to Gain Muscle - Develop the Body You Have Always Wanted</h4>
<p class="mb-4">Bodybuilding workouts need to be performed if you want to gain muscle as quickly as possible. Those who are new to the practice of bodybuilding will often get discouraged when they cannot see any gain in the time it takes to reach their goals. If results cannot be seen in a matter of weeks, they will quit and think their next attempt will not be any different. The actual fact is that gaining muscle requires time and dedication.
                    <br/><br/>
                    Muscle building exercises have to be progressed gradually and with enough rest between sessions. You can easily develop the muscles if you combine exercise with enough protein. There are a variety of protein powders that you can use to easily build muscle fast, including milk shakes, lean meat and vegetable powders and the amino acid aminos.
                    <br/><br/>
                    In a few weeks you will start to see the results of your hard work. But don't you dare quit the gym because your hard work hasn't paid off. Although the body needs time to get the required protein levels and build up muscles, the other essential supplements will greatly help in the way of results. When you are done with your workout, it is important to consume some carbs as well. Avoid alcohol and junk food, which have a detrimental effect on the body. If the key ingredients needed to create muscles are there, the body will be stronger and the results will be real quick.
                    <br/><br/>
<small><b>
<span class="text-theme">Main keyword:</span> gain muscle
                      <br/>
<span class="text-theme">Sub keywords:</span> exercise, protein
                    </b></small></p>
</div>
</div>
</div>
<div class="tab-pane fade" id="tab1-5" role="tabpanel">
<div class="row align-items-center">
<div class="col-lg-8 offset-lg-2 col-md-12 md-mt-5">
<h4 class="mb-4">How to Get Out of Debt - Using a Debt Consolidation Loan to Get Out of Debt Fast</h4>
<p class="mb-4">If you are dealing with debt on a regular basis then you know how crushing it can be and how extremely difficult it can be to pay it off. There are companies that offer many different ways to help you get out of debt. The ones that service personal loans can help you get a good consolidation loan to help you manage your debt.
                    <br/><br/>
                    A debt consolidation loan is one that is based on your credit, income and a whole bunch of other factors. You can find one that will fit all your unique-to-you circumstances. Plus a good financial consultant can help you go through all of the different options. It can take some research to find the right lender, but the overall process will be fairly simple.
                    <br/><br/>
                    Now that you know how to get out of debt by consolidating your debt it is time to get moving. You can do this starting right now online with a debt consolidation loan. Plus you can use a credit counselor to get the info you need to make sure you find the right lender. And with some good old fashioned hard work and grit you will finally get out of debt and back on the road to true financial freedom.
                    <br/><br/>
<small><b>
<span class="text-theme">Main keyword:</span> how to get out of debt
                      <br/>
<span class="text-theme">Sub keyword:</span> debt consolidation
                    </b></small></p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div> </section>
<!--tab end-->
<!--step start-->
<section class="pos-r o-hidden text-center" data-bg-img="images/pattern/01.png" id="how-it-works">
<div class="container">
<div class="row">
<div class="col-lg-6 col-md-10 ml-auto mr-auto">
<div class="section-title">
<h2 class="title">How Article Forge Works</h2>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-4 col-md-12">
<div class="work-process">
<div class="box-loader"> <span></span>
<span></span>
<span></span>
</div>
<div class="step-num-box">
<div class="step-icon"><span><i class="la la-lightbulb-o"></i></span>
</div>
<div class="step-num">01</div>
</div>
<div class="step-desc">
<h4>Enter Keyword</h4>
<p class="mb-0">Enter your keyword, any optional sub keywords, your article length, and any other requirements into the Article Forge system.</p>
</div>
</div>
</div>
<div class="col-lg-4 col-md-12 md-mt-5">
<div class="work-process">
<div class="box-loader"> <span></span>
<span></span>
<span></span>
</div>
<div class="step-num-box">
<div class="step-icon"><span><i class="la la-rocket"></i></span>
</div>
<div class="step-num">02</div>
</div>
<div class="step-desc">
<h4>Wait 60 Seconds</h4>
<p class="mb-0">During those 60 seconds, Article Forge will intelligently write you a high quality, completely unique article.</p>
</div>
</div>
</div>
<div class="col-lg-4 col-md-12 md-mt-5">
<div class="work-process">
<div class="step-num-box">
<div class="step-icon"><span><i class="la la-check-square"></i></span>
</div>
<div class="step-num">03</div>
</div>
<div class="step-desc">
<h4>Receive Article</h4>
<p class="mb-0">You have your article and are ready to use it wherever and however you want!</p>
</div>
</div>
</div>
</div>
</div>
</section>
<!--step end-->
<!-- pricing block start-->
<section class="pos-r" id="pricing" style="padding-bottom: 100px;">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="tab">
<div class="row align-items-center">
<div class="col-lg-12 col-md-12">
<!-- Nav tabs -->
<div class="section-title mb-3" style="text-align:center;">
<h2 class="title">Start your <span class="text-theme">absolutely free</span> 5-day trial today!</h2>
<p class="mb-0 text-black">See for yourself how Article Forge will revolutionize your content writing process.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="hexagon-bg style-2">
<div class="hexagon"></div>
<div class="hexagon"></div>
<div class="hexagon"></div>
<div class="hexagon"></div>
</div>
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="tab">
<div class="row align-items-center">
<div class="col-lg-12 col-md-12">
<!-- Tab panes -->
<div class="tab-content px-0 pb-0" id="nav-tabContent">
<div class="row align-items-center">
<div class="col-lg-4 offset-lg-2 col-md-12">
<div class="price-table mt-3">
<div class="price-inside">Monthly</div>
<div class="price-header mb-3">
<div class="price-value">
<h2><span>$</span>57</h2><small>/mo</small>
<span><small> </small></span>
</div>
<h3 class="price-title">Monthly</h3>
</div>
<div class="price-list">
<ul class="list-unstyled">
<li>AI-powered writer</li>
<li>Human quality content</li>
<li>Content passes Copyscape</li>
<li>One click article generation</li>
<li>60 second article turnaround</li>
<li>Automatically posts to blogs</li>
</ul><br/>
</div>
<a class="btn btn-theme btn-circle mt-4 trial-btn-monthly" data-text="Start my free trial!" href="https://af.articleforge.com/users/sign_up?plan_id=17"> <span>Start </span><span>my </span><span>free </span><span>trial! </span>
</a>
</div>
</div>
<div class="col-lg-4 col-md-12">
<div class="price-table mt-3">
<div class="price-inside">Yearly</div>
<div class="price-header mb-3">
<div class="price-value">
<div class="price-value">
<h2><span>$</span>27</h2><small>/mo</small>
</div>
<span><small>Billed annually</small></span>
</div>
<h3 class="price-title">Yearly</h3>
</div>
<div class="price-list">
<ul class="list-unstyled">
<li>AI-powered writer</li>
<li>Human quality content</li>
<li>Content passes Copyscape</li>
<li>One click article generation</li>
<li>60 second article turnaround</li>
<li>Automatically posts to blogs</li>
</ul><br/>
</div>
<a class="btn btn-theme btn-circle mt-4 trial-btn-yearly" data-text="Start my free trial!" href="https://af.articleforge.com/users/sign_up?plan_id=19"> <span>Start </span><span>my </span><span>Free </span><span>Trial!</span>
</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!--pricing block end--> <!--money back block start-->
<section class="p-0 z-index-1 mb-8">
<div class="container ml-auto mr-auto" style="width:95%; max-width: 1300px">
<div class="row subscribe-inner align-items-center">
<div class="ml-3 mr-3">
<h4 class="pb-2">30-Day No Risk Money Back Guarantee</h4>
<img align="left" class="mt-3" src="images/guarantee.jpg" style="padding-right: 25px;" width="150"/>
<p class="lead mb-0">
              We are confident that Article Forge will revolutionize how you generate and use content so we want to make sure that there is absolutely no risk for you to try Article Forge.
              <br/><b>So, in addition to our 5 day free trial, we are also offering a no strings attached 30 day money back guarantee.</b>
              If you use Article Forge to create less than ten articles and find that it doesn't live up to your expectations just contact us and we'll give you a no hassle, no questions asked refund!</p>
</div>
</div>
</div>
</section>
<!-- money back block end-->
</div>
<!--body content end-->
<footer class="footer dark-bg pos-r animatedBackground" data-bg-img="https://www.articleforge.com/images/pattern/03.png">
<div class="footer-wave" data-bg-img="https://www.articleforge.com/images/bg/08.png">
</div>
<div class="primary-footer">
<div class="container">
<div class="row">
<div class="col-lg-5 col-md-12 md-mt-5">
<ul class="media-icon list-unstyled">
<li>
<h5 class="text-white">Article Forge</h5>
<p class="mb-0">100 International Drive</p>
<p class="mb-0">Floor 23</p>
<p class="mb-0">Baltimore, MD, 21202</p>
<p class="mb-0">USA</p>
</li>
</ul>
</div>
<div class="col-lg-7 col-md-12 pl-md-5 sm-mt-5 footer-list justify-content-between d-flex">
<ul class="list-unstyled w-100">
<li><a href="https://www.articleforge.com/">Home</a>
</li>
<li><a href="https://www.articleforge.com/contact">Contact Us</a>
</li>
<li><a href="https://www.articleforge.com/login">Login</a>
</li>
</ul>
<ul class="list-unstyled w-100">
<li><a href="https://www.articleforge.com/how-it-works">How it Works</a>
</li>
<li><a href="https://www.articleforge.com/where-to-use-article-forge">Use Cases</a>
</li>
<li><a href="https://www.articleforge.com/case-studies">Case Studies</a>
</li>
</ul>
<ul class="list-unstyled w-100">
<li><a href="https://www.articleforge.com/pricing">Pricing</a>
</li>
<li><a href="https://www.articleforge.com/foreign-language">Foreign Language</a>
</li>
<li><a href="https://www.articleforge.com/affiliate">NEW! - Affiliate Program</a>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="secondary-footer">
<div class="container">
<div class="copyright">
<div class="row align-items-center">
<div class="col-lg-8 col-md-12"> <span>Copyright © 2020 <u><a href="http://cortx.com/" target="_blank">Cortx</a></u> | All Rights Reserved | <a href="https://www.articleforge.com/terms-of-service" style="color: #FFFFFF;" target="_blank">Terms of Service</a> | <a href="https://www.articleforge.com/privacy-policy" style="color: #FFFFFF;" target="_blank">Privacy Policy</a></span>
</div>
<div class="col-lg-4 col-md-12 text-lg-right md-mt-3">
<div class="footer-social">
<ul class="list-inline">
<li class="mr-2"><a href="https://www.facebook.com/ArticleForge/"><i class="fab fa-facebook-f"></i> Facebook</a>
</li>
<li class="mr-2"><a href="https://twitter.com/articleforge"><i class="fab fa-twitter"></i> Twitter</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</footer>
<!--footer end-->
</div>
<!-- page wrapper end --> <!--back-to-top start-->
<div class="scroll-top"><a class="smoothscroll" href="#top" style="max-width: 50px; max-height: 50px;"><i class="flaticon-go-up-in-web" style="line-height: 55px;"></i></a></div>
<!--back-to-top end--> <!-- Perfect Tense Script-->
<script src="https://cdn.jsdelivr.net/npm/perfecttense-editor@latest/dist/pte.umd.js">
</script>
<script>
    PerfectTenseEditor({ clientId: "5e1f4ee2902c7604e9ec2f5a" });
  </script> <!-- inject js start -->
<!--== jquery -->
<script src="https://www.articleforge.com/js/jquery.min.js?"></script>
<!--== popper -->
<script src="https://www.articleforge.com/js/popper.min.js?"></script>
<!--== bootstrap -->
<script src="https://www.articleforge.com/js/bootstrap.min.js?"></script>
<!--== appear -->
<script src="https://www.articleforge.com/js/jquery.appear.js?"></script>
<!--== modernizr -->
<script src="https://www.articleforge.com/js/modernizr.js?"></script>
<!--== easing -->
<script src="https://www.articleforge.com/js/jquery.easing.min.js?"></script>
<!--== menu -->
<script src="https://www.articleforge.com/js/menu/jquery.smartmenus.js?"></script>
<!--== owl-carousel -->
<script src="https://www.articleforge.com/js/owl-carousel/owl.carousel.min.js?"></script>
<!--== magnific-popup -->
<script src="https://www.articleforge.com/js/magnific-popup/jquery.magnific-popup.min.js?"></script>
<!--== counter -->
<script src="https://www.articleforge.com/js/counter/counter.js?"></script>
<!--== countdown -->
<script src="https://www.articleforge.com/js/countdown/jquery.countdown.min.js?"></script>
<!--== map api -->
<script src="https://maps.googleapis.com/maps/api/js"></script>
<!--== map -->
<script src="https://www.articleforge.com/js/map.js?"></script>
<!--== wow -->
<script src="https://www.articleforge.com/js/wow.min.js?"></script>
<!--== theme-script -->
<script src="https://www.articleforge.com/js/theme-script.js?"></script>
<script>
    $(document).ready(function() {

        $(".trial-btn").click(function() {
            _kmq.push(["record", "Start My Free Trial Button Click"]);
        });
        $(".trial-btn-header").click(function() {
            _kmq.push(["record", "Start My Free Trial Button Click Header"]);
        });
        $(".trial-btn-cta-block").click(function() {
            _kmq.push(["record", "Start My Free Trial Button Click CTA Block"]);
        });
        $(".trial-btn-monthly").click(function() {
            _kmq.push(["record", "Start My Free Trial Button Click Monthly"]);
        });
        $(".trial-btn-yearly").click(function() {
            _kmq.push(["record", "Start My Free Trial Button Click Yearly"]);
        });

        $(".affiliate-btn-hero").click(function() {
            _kmq.push(["record", "Affiliate CTA Click", {"CTA Location":"Hero"}]);
        });
        $(".affiliate-btn-cta-block").click(function() {
            _kmq.push(["record", "Affiliate CTA Click", {"CTA Location":"Bottom Block"}]);
        });

        $(".main-video").click(function() {
            _kmq.push(["record", "Main Video Click"]);
        });
        $(".best-practices-video").click(function() {
            _kmq.push(["record", "Best Practices Video Click"]);
        });

        $(".integration-seo-ap").click(function() {
            _kmq.push(["record", "Click Integration", {"Integration":"SEO AutoPilot"}]);
        });
        $(".integration-wai").click(function() {
            _kmq.push(["record", "Click Integration", {"Integration":"WordAi"}]);
        });
        $(".integration-cyberseo").click(function() {
            _kmq.push(["record", "Click Integration", {"Integration":"CyberSEO"}]);
        });
        $(".integration-rankerx").click(function() {
            _kmq.push(["record", "Click Integration", {"Integration":"RankerX"}]);
        });
        $(".integration-gsa-ser").click(function() {
            _kmq.push(["record", "Click Integration", {"Integration":"GSA Search Engine Ranker"}]);
        });
        $(".integration-senuke-tng").click(function() {
            _kmq.push(["record", "Click Integration", {"Integration":"SEnuke TNG"}]);
        });
      });
  </script>
<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-62756043-1', 'auto');
	  ga('send', 'pageview');
	</script>
<!-- inject js end --> </body>
</html>