<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="Civil Marriage Celebrant covering the New England and North West. For a personal and memorable ceremony - Weddings, Commitment, Naming, Vow Renewal, Remembrance and Master of Ceremonies." name="description"/>
<meta content="celebrant tamworth, tamworth celebrant, weddings tamworth, celebrant north west, celebrant new england, marriage celebrant, ceremonies tamworth, wedding ceremonies tamworth, naming day tamworth, commitment ceremony tamworth, vow renewal tamworth, wedding services tamworth, civil ceremony tamworth, civil ceremonies tamworth" name="keywords"/>
<meta content="text/html;charset=utf-8" http-equiv="content-type"/>
<title>Ceremonies By Donna || Tamworth Celebrant</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
<link href="style.css" rel="stylesheet"/>
</head>
<body>
<div id="container">
<a href="index.php"><div id="header">
</div></a>
<div class="menu">
<ul class="drop">
<li><a href="index.php">home</a></li>
<li><a href="../about_donna/index.php">about donna</a></li>
<li><a href="../weddings/index.php"><span>weddings</span></a></li>
<li> <a href="../other_ceremonies/index.php">other ceremonies</a>
<ul>
<li><a href="../other_ceremonies/#commitment_ceremony">Commitment Ceremony</a></li>
<li><a href="../other_ceremonies/#naming_ceremony">Naming Ceremony</a></li>
<li><a href="../other_ceremonies/#vow_renewal_ceremony">Vow Renewal Ceremony</a></li>
<li><a href="../other_ceremonies/#memorial_remembrance_or_reflection_ceremony">Memorial, Remembrance or Reflection Ceremony</a></li>
<li><a href="../other_ceremonies/#master_of_ceremonies">Master of Ceremonies</a></li>
</ul>
</li><li><a href="../testimonials/index.php"><span>testimonials</span></a></li>
<li><a href="../contact_donna/index.php">contact donna</a></li>
</ul>
<br style="clear:left"/>
</div>
<div id="container2">
<ul id="slideshow">
<li>
<h3>TinySlideshow v1</h3>
<span>photos/slide1.jpg</span>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
<a href="#"><img alt="slide 1" src="thumbnails/slide1.jpg"/></a>
</li>
<li>
<h3>Slide 2</h3>
<span>photos/slide8.jpg</span>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
<a href="#"><img alt="slide2" src="thumbnails/slide8.jpg"/></a>
</li>
<li>
<h3>Slide 3</h3>
<span>photos/the-pavillion.jpg</span>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
<a href="#"><img alt="slide 3" src="thumbnails/the-pavillion.jpg"/></a>
</li>
<li>
<h3>Slide 4</h3>
<span>photos/mywedding1.jpg</span>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
<a href="#"><img alt="slide 4" src="thumbnails/mywedding1.jpg"/></a>
</li>
<li>
<h3>Slide 5</h3>
<span>photos/naming-ceremony.jpg</span>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
<img alt="slide 5" src="thumbnails/naming-ceremony.jpg"/>
</li>
<li>
<h3>Slide 6</h3>
<span>photos/slide4.jpg</span>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
<a href="#"><img alt="slide 6" src="thumbnails/slide4.jpg"/></a>
</li>
<li>
<h3>Slide 7</h3>
<span>photos/dag.jpg</span>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
<a href="#"><img alt="slide 7" src="thumbnails/dag.jpg"/></a>
</li>
</ul>
<div id="wrapper">
<div id="fullsize">
<div class="imgnav" id="imgprev" title="Previous Image"></div>
<div id="imglink"></div>
<div class="imgnav" id="imgnext" title="Next Image"></div>
<div id="image"></div>
<div id="information">
<h3></h3>
<p></p>
</div>
</div>
<div id="thumbnails">
<div id="slideleft" title="Slide Left"></div>
<div id="slidearea">
<div id="slider"></div>
</div>
<div id="slideright" title="Slide Right"></div>
</div>
</div>
<script src="compressed.js" type="text/javascript"></script>
<script type="text/javascript">
	$('slideshow').style.display='none';
	$('wrapper').style.display='block';
	var slideshow=new TINY.slideshow("slideshow");
	window.onload=function(){
		slideshow.auto=true;
		slideshow.speed=5;
		slideshow.link="linkhover";
		slideshow.info="information";
		slideshow.thumbs="slider";
		slideshow.left="slideleft";
		slideshow.right="slideright";
		slideshow.scrollSpeed=4;
		slideshow.spacing=5;
		slideshow.active="#fff";
		slideshow.init("slideshow","image","imgprev","imgnext","imglink");
	}
</script>
<p align="center"><img src="assets/saying.png"/></p></div>
</div>
<div align="center" id="copyright">
<p><a href="https://www.facebook.com/CeremoniesByDonna" target="_blank">Find us on Facebook</a></p>
<p>Copyright © Ceremonies by Donna<br/>
    Designed and Constructed by: <a href="http://www.webpageme.com.au" target="_blank">Webpage Me Pty. Ltd.</a></p>
</div>
</body>
</html>
