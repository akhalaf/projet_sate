<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "21199",
      cRay: "6111a3582b061a3e",
      cHash: "1a0b616424f62d7",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYm9ydXRvZ2V0LmluZm8v",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "ZzNyMtM5QY/Bo+N03M+ulp3vgOhkAHXW4NPTdrUHYpTf/JPnsyfs4HFb6ttXubF1I6J/rWh0LmpS65GdKnynMHTFofNWz9V5sqeLBZnOPokC/EZsMItU/0OZAGt/Zj1VTOWI/34ALViX2tO4gicKaESgVLYpBK79woQEngA0Qv5gFSeEG9y98kZQT6MS2Pq8X8BWY88U700d8yHANOvHuXJacielztmZ+dyHQLD8/HKxJhxY9CR+iS0tzVsE6iUnzdMc0QEoRBYeMJ57YSFaGM7+WDuORJmMcAfBsVMeV/7qtgVwwCHpcwoxtEzRmqPAis7PssEJRmE4wUks0I13Tc/xzr8GDbBq7U9KYl7fSPbDUsPwJzBLvkkxmrK7BcRXeCmsUs18zJHD86iYUHyUppFIfyBIA8xBlpeRdo0QZVQdBcmCjPIHc7yJxHTjt5Z/gzGIO7kra7doSoesabkX2aWaPDchJW7MWEI4PSk+5hIrrJbgNyImAr8VPPnSWlms1pN9yDudz8ZpdoOtreMEu3ujJZd7atc0mwWHy526WpZdMB7LQ/wZw8K6OAVl+kWOX/XnkiCRRS4TtVcuBq0wOcB0pSaekMlOEU/l7LT4JvXZR/V6WQxziOzPASnKTo73/vRSIWXBL48V/yWYcuqrlB/vYftYz61PQQV9nFl28b/EKVMdcfY0bnSFmzDvrI6Quxful7xgv9T72xBJ5hWJM6Sslm9TPLZCd776kr7DdaiEVO0C4IohilC8c2MEo4GAPJQx/K5QPZiTKtDSX+ZihQ==",
        t: "MTYxMDU2NzM0MC44MjkwMDA=",
        m: "gM/tE706IhnD/Mo5Yr9rJrx67qIGmq1jDwybLL03hDk=",
        i1: "/LzLmZTVTQmtVIJtcTWNAQ==",
        i2: "gZd95uOo6vdKDgWC2XzHTw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "RA954X20LG4jTFyTQ0LH1nIbcl6JZB7YG0AkygG2ufU=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=6111a3582b061a3e");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> borutoget.info.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=86dd97524b951504fcf1f14d1ec169480a31eb48-1610567340-0-AczNs8-wG3zX1C8et_D3Tt-9GiMiXfnfw1EGP2LI86jynclAETt1_GNYUXJ4eG25GPbEVpuiQqjk5IdoChUZKQWrI_Bf0uyyR9YqBd98TFbm9NpBnv9cox6hTXqbSZGEbt5uc54ETzGEiseHobVF7CmLONONrnr0v6PYO1EXczaIv7ozjRZDbMF7ZkBtZDkPUiBk-on0nEL-6trOSbrTYneTKe5PvUDEF38k2bgHZfR4WNmGOw6i3wtAPdTRuEuQe5qKyOtI9EslgiTMnGG4M_GCzJL9kKuySJs8j9ljOv6i2Mj6Aqz9fJ12y2TPRcis9qH38FUEsp1mSAjAkGy3-NI" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="05fe0849f174f4722d3765f7affa72f4ba938f02-1610567340-0-AZ1BEMhrn4AQyOOUFQ3MDbYcfmfZVHVlralMNcjGH/Wfx/nY+5P7rhYQZ0T33Vq5jOJXlyqMbwNj4XaXnZFoRSXh61HQ/8HonsR2apOGaphac/5Z4H8Z5znK04VAD7HIxTpw8l3Ak0nNPPzdlQT0D4V69HO+5wLdfZve+RSnZ8dblCQHFhlMB3NhfW2OMz3YpwhY5L+TbDHghVlJs/cMIRaRMxlHkGnjchC6CvenQbc/UqlmqudGGpMqn9IvCH9o5xy22YCQ47idU8u88PY1KEiMHI3QU7IAxlp8M4qHPB1b23+hG5XTM7kzJ1NgSyDpHflWZVKVtsSswMEgYKjGjAOMQdjFgC5QfRSlSaqkGC4vjCp94A72+SH31UToKe70zXLTywtFc+SdmTNPhH0U9ndiRauI9vWn2cvFKSLUoc/nom6r2cTVmkj+2eWnwtQsPgYyRnr83oMtrj8JmAY704tUJrlTex+qHA0ZoEj6Of0nMOMiwSRggdouVu/DaZP5wHnEgT+NQzFsdmXorMRar1Jc+4nz1U8QDmAB+KuMOqr0r+bgqbyGRkvbyV3M2PpMyzpVHyb4sERBquMdZ8j0Hq+LS9u/WCxzKO6BsQgkI4+i+mZqxEi90SCwJZP5KrgL0HTvfMEZ4pBQp2kww4cnfq2mcC8tuQfaQ8U/Ht/5ZCrha4qcCGCuvk5lP7zoqgncP4YTLuAkJLEFYdmZpi9Hq85x98TyLaaWsoLp9yJnH7ahjY+h3PLs8N6hZG1ISkY0heDe+qEFHWhLHmJACGEFO1HZGEa3owWZ/Ry0irfOTzkCmbcimh+wcMO2I/8GJHy3fO1dZnYsCUc25knBDADOJHI8UZ9fqGlYZx9RvrIN9fwddlF3zIHIIrv9BR4/SSbAQ1wfhzyrjfgiZwkKQyeloyWO9QrXjvMO8gYvyWmNzC9fyYHS6ceMp5KXaWGMwMdJA85YrWvdq0ALxhO46VItHbhCS/C1quTdSWn4bKSq/bIjroJRgosrxMeL5Aj9UErfnMPmnhzgC43ldhRjvu8v7fLjciiQ5vmx1q0qe8+iszsI7pc6b6QzLoke99nCjeHlIFVxnDGIgPKqtX66ZbqNsNaX+mLYwZ/nmpv9J9p4OImTeRf9juCedATNUBVuDq01VraK7xXvTCJWlyvM7Q1Sxv6sQBD7RLpzSGCyGZ9zIaL2QVEulL02X8tc7RMUH70br5zA8kozsqLhinmWWQGjHS/4QRj8G9V9QRQ2e57X7Lh8PN9V+GLYTowEawlujXa9ZVWjQdxo6smMKDwZb33j8wdQ9mKHj4V79JsvYCwxRYjn"/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="f7c8b8315febe1827c311158316f4354"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610567344.829-glovdgTiXI"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=6111a3582b061a3e')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>6111a3582b061a3e</code></span>
</div>
</td>
<a href="https://tempestsw.com/nightingalesguilt.php?asp=86"></a>
</tr>
</table>
</body>
</html>
