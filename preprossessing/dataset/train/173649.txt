<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<link href="css/animate.css" rel="stylesheet" type="text/css"/>
<title>Memeplex</title>
<link href="images/favicon.png" rel="shortcut icon" type="image/png"/>
</head>
<body>
<section class="logosec">
<div class="container">
<div class="navbar-header"> <a class="navbar-brand" href="index.html"><img class="img-responsive logohed" src="images/memeplex-logo.png"/></a></div>
<div class="advertisement-sec">
<div class="" id="navbar">
<ul class="nav navbar-nav top-barmenu">
<li class="active"><a href="index.html">Technology</a></li>
<li><a href="about.html">About</a></li>
<li><a href="contact-us.html">Contact Us</a></li>
</ul>
</div>
</div>
</div>
</section>
<section class="total-wrap">
<div class="top-section-wrapper">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="top-title">
<h3>Our Technology</h3>
<p>We're always developing new tools, many of which are for our own internal use. However, here's a small cross-section of some of the tools we are working on, are have created in the past. Some require login, some need to be downloaded, but others are free!</p>
</div>
</div>
</div>
</div>
</div>
<div class="softw_1">
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="img_block">
<img alt="" class="img-responsive" src="umixdjapp.png"/>
</div>
</div>
<div class="col-md-6 align-self-center">
<div class="software_text1">
<h3>uMix DJ</h3>
<p>iOS app which allows you to "mix" songs together, using blends and scratches. Like having a DJ and 2x 1210s in your pocket!</p>
<a class="vs-btn" href="https://apps.apple.com/gb/app/umix-dj/id689336221">Check uMix on The App Store</a>
</div>
</div>
</div>
</div>
</div>
<div class="softw_1 ">
<div class="container">
<div class="row reciv">
<div class="col-md-6 align-self-center">
<div class="software_text1">
<h3>uBub News Reader</h3>
<p>Website (and iOS app) which allows you to follow and share your favourite websites, all from one single place.</p>
<a class="vs-btn" href="http://ubub.com/welcome">Visit uBub (Website)</a> <a class="vs-btn" href="https://apps.apple.com/us/app/ubub/id1179663211">Get uBub App for iOS</a>
</div>
</div>
<div class="col-md-6">
<div class="img_block">
<img alt="" class="img-responsive" src="ububapp.png"/>
</div>
</div>
</div>
</div>
</div>
<div class="softw_1">
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="img_block">
<img alt="" class="img-responsive" src="vresearcher.png"/>
</div>
</div>
<div class="col-md-6 align-self-center">
<div class="software_text1">
<h3>Video Researcher</h3>
<p>Server-based tool to research YouTube niches &amp; keywords, using the YouTube &amp; SEMRush APIs. Great for helping decide which niches to focus on, and videos to create.</p>
<a class="vs-btn" href="http://autojvx.com/vresearch/">Login to Video Researcher</a>
</div></div></div>
</div>
</div>
<div class="softw_1 ">
<div class="container">
<div class="row reciv">
<div class="col-md-6 align-self-center">
<div class="software_text1">
<h3>Tee Maker</h3>
<p>Create super fast t-shirt designs, which you can then sell on eCommerce platforms such as TeeSpring. Currently free!</p>
<a class="vs-btn" href="http://www.autojvx.com/teemaker/">Launch TeeMaker</a>
</div>
</div>
<div class="col-md-6">
<div class="img_block">
<img alt="" class="img-responsive" src="autodesigner.png"/>
</div>
</div>
</div>
</div>
</div>
</section>
<footer>
<div class="container">
<div class="row">
<div class="col-md-12">
<p>Copyright © 2020 Memeplex Ltd. All Rights Reserved | <a href="privacy-policy.html">Privacy Policy</a> | <a href="terms-of-service.html">Terms Of Service</a></p>
</div>
</div>
</div>
</footer>
</body>
</html>