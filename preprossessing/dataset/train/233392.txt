<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Отзывы о фирмах Казахстана BizGid.kz - Справочник фирм, база предприятий Казахстана</title>
<meta content="text/html, charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="all" name="robots"/>
<meta content="База предприятий Казахстана с адресами, телефонами и честными отзывами. Адреса и телефоны фирм и компаний Казахстана - более 50000 фирм в нашем каталоге." name="description"/>
<meta content="Фирмы Казахстана, Справочник фирм Казахстана, предприятия Казахстана, деловой сайт Казахстан, бизнес справки Казахстан, телефоны Казахстана, база фирм Казахстана, база предприятий Казахстана, фирмы Астаны, фирмы Алматы, фирмы Павлодара" name="keywords"/>
<link href="/style/style.css" rel="stylesheet" type="text/css"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js?ver=1.4.2" type="text/javascript"></script>
</head>
<body>
<div id="container">
<div class="top">
<div id="logo" style="display: inline-block; float: left;">
<img alt="BizGid.kz" height="73" src="/images/logo.png" title="BizGid.kz - Деловой справочник Казахстана" width="180"/>
<div id="currentCity">Справочник фирм <strong>Казахстана</strong></div>
</div>
<div id="right_head">
<div id="menu">
<ul>
<li><a href="/"><span>Фирмы и предприятия Казахстана</span></a></li>
<li><a href="/postal/"><span>Почтовые индексы Казахстана</span></a></li>
</ul>
</div>
</div>
<div id="search_head">
<div class="ya-site-form ya-site-form_inited_no" onclick="return {'action':'http://bizgid.kz/%D0%BF%D0%BE%D0%B8%D1%81%D0%BA/','arrow':false,'bg':'transparent','fontsize':16,'fg':'#000000','language':'ru','logo':'rb','publicname':'Справочник фирм и почтовые индексы Казахстана','suggest':true,'target':'_self','tld':'ru','type':2,'usebigdictionary':true,'searchid':2289088,'input_fg':'#000000','input_bg':'#ffffff','input_fontStyle':'normal','input_fontWeight':'normal','input_placeholder':'Поиск по сайту','input_placeholderColor':'#000000','input_borderColor':'#7f9db9'}"><form accept-charset="utf-8" action="https://yandex.ru/search/site/" method="get" target="_self"><input name="searchid" type="hidden" value="2289088"/><input name="l10n" type="hidden" value="ru"/><input name="reqenc" type="hidden" value=""/><input name="text" type="search" value=""/><input type="submit" value="Найти"/></form></div><style type="text/css">.ya-page_js_yes .ya-site-form_inited_no { display: none; }</style><script type="text/javascript">(function(w,d,c){var s=d.createElement('script'),h=d.getElementsByTagName('script')[0],e=d.documentElement;if((' '+e.className+' ').indexOf(' ya-page_js_yes ')===-1){e.className+=' ya-page_js_yes';}s.type='text/javascript';s.async=true;s.charset='utf-8';s.src=(d.location.protocol==='https:'?'https:':'http:')+'//site.yandex.net/v2.0/js/all.js';h.parentNode.insertBefore(s,h);(w[c]||(w[c]=[])).push(function(){Ya.Site.Form.init()})})(window,document,'yandex_site_callbacks');</script>
</div>
</div>
<div class="clear"></div>
<div id="cities"><ul class="navigation">
<li><a href="/aktau/">Актау</a></li> <li><a href="/aktobe/">Актобе</a></li> <li><a href="/almaty/">Алматы</a></li> <li><a href="/astana/">Астана</a></li> <li><a href="/atirau/">Атырау</a></li> <li><a href="/kapshagai/">Капшагай</a></li> <li><a href="/karaganda/">Караганда</a></li> <li><a href="/kaskelen/">Каскелен</a></li> <li><a href="/kostanay/">Костанай</a></li> <li><a href="/pavlodar/">Павлодар</a></li> <li><a href="/semey/">Семей</a></li> <li><a href="/taldikorgan/">Талдыкорган</a></li> <li><a href="/taraz/">Тараз</a></li> <li><a href="/ustkamenogorsk/">Усть-Каменогорск</a></li> <li><a href="/shimkent/">Шымкент</a></li> </ul></div>
<div id="content">
<h1>Адреса, телефоны и отзывы о фирмах , Казахстан</h1>
<div id="path"><div style="float: left;">Добро пожаловать на наш сайт. У нас Вы сможете почитать отзывы о фирмах Казахстана, зарегистрировать компанию в справочнике фирм Казахстана, а также создать мини сайт Вашей компании совершенно бесплатно!</div><div id="copyright"></div><div style="clear: both;"></div>
</div>
<h2>Найти информацию о фирмах Казахстана в городе:</h2>
<p>В нашем справочнике - огромная база предприятий, в ней Вы найдете контакты нужного предприятия, в том числе адрес, контактный телефон, сайт, а также отзывы, написанные реальными людьми!</p>
<h3><a href="/aktau/">Актау</a></h3>
<h3><a href="/aktobe/">Актобе</a></h3>
<h3><a href="/almaty/">Алматы</a></h3>
<h3><a href="/astana/">Астана</a></h3>
<h3><a href="/atirau/">Атырау</a></h3>
<h3><a href="/kapshagai/">Капшагай</a></h3>
<h3><a href="/karaganda/">Караганда</a></h3>
<h3><a href="/kaskelen/">Каскелен</a></h3>
<h3><a href="/kostanay/">Костанай</a></h3>
<h3><a href="/pavlodar/">Павлодар</a></h3>
<h3><a href="/semey/">Семей</a></h3>
<h3><a href="/taldikorgan/">Талдыкорган</a></h3>
<h3><a href="/taraz/">Тараз</a></h3>
<h3><a href="/ustkamenogorsk/">Усть-Каменогорск</a></h3>
<h3><a href="/shimkent/">Шымкент</a></h3>
<h2>Где найти почтовые индексы городов Казахстана?</h2>
<p>Мы обновили наш сайт, теперь Вы можете выбрать в правом верхнем углу ссылку для перехода к почтовым индексам (кодам) всех населенных пунктов республики Казахстан или перейти по ссылке - <a href="/postal/">Почтовые индексы Казахстана</a> 2016</p>
</div>
<table cellpadding="0" cellspacing="0" id="footer" width="100%">
<tr>
<td width="180">
<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t26.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число посетителей за"+
" сегодня' "+
"border='0' width='88' height='15'><\/a>")
//--></script><!--/LiveInternet-->
</td>
<td align="center">
<ul>
<li><a href="/contacts/">Контакты</a></li>
<li><a href="/termsofuse/">Правила использования</a></li>
<li><a href="/privacy/">Конфиденциальность</a></li>
</ul>
        Копирование данных с сайта для коммерческого использования разрешено только с письменного разрешения администрации сайта
        <br/><br/>
<div>2014-2021 © BizGid.kz</div>
</td>
<td align="right" width="215">
<div style="display: inline; float: left; padding-top: 5px;">
</div>
</td>
</tr>
</table>
</div>
</body>
<!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter30920696 = new Ya.Metrika({ id:30920696, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img alt="" src="https://mc.yandex.ru/watch/30920696" style="position:absolute; left:-9999px;"/></div></noscript><!-- /Yandex.Metrika counter -->
</html>