<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html dir="ltr" lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Centre Hospitalier Edouard Toulouse</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="skel/css/style.css" media="screen, projection" rel="stylesheet" type="text/css"/>
<link href="skel/css/style_print.css" media="print" rel="stylesheet" type="text/css"/>
<!--[if IE]><link rel="stylesheet" href="skel/css/ie.css" type="text/css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" href="skel/css/ie6.css" type="text/css" /><![endif]-->
<link href="skel/img/favicon.png" rel="shortcut icon" type="image/png"/>
<link href="spip.php?page=backend&amp;lang=fr" rel="alternate" title="Fil RSS général du site  Centre Hospitalier Edouard Toulouse" type="application/rss+xml"/>
<meta content="index, follow, all" name="Robots"/>
<meta content="https://www.ch-edouard-toulouse.fr" name="Identifier-URL"/>
<meta content="General" name="rating"/>
<meta content="TRUE" name="MSSmartTagsPreventParsing"/>
<script type="text/javascript">/* <![CDATA[ */
var box_settings = {tt_img:true,sel_g:"#documents_portfolio a[type='image/jpeg'],#documents_portfolio a[type='image/png'],#documents_portfolio a[type='image/gif']",sel_c:".mediabox",trans:"elastic",speed:"200",ssSpeed:"2500",maxW:"90%",maxH:"90%",minW:"400px",minH:"",opa:"0.9",str_ssStart:"Diaporama",str_ssStop:"Arrêter",str_cur:"{current}/{total}",str_prev:"Précédent",str_next:"Suivant",str_close:"Fermer",splash_url:""};
var box_settings_splash_width = "600px";
var box_settings_splash_height = "90%";
var box_settings_iframe = true;
/* ]]> */</script>
<!-- insert_head_css --><link href="plugins-dist/mediabox/colorbox/black-striped/colorbox.css" media="all" rel="stylesheet" type="text/css"/><link href="plugins-dist/porte_plume/css/barre_outils.css?1536646915" media="all" rel="stylesheet" type="text/css"/>
<link href="local/cache-css/cssdyn-css_barre_outils_icones_css-f0e4cb50.css?1536765941" media="all" rel="stylesheet" type="text/css"/>
<link href="plugins/gis/lib/leaflet/dist/leaflet.css" rel="stylesheet"/>
<link href="plugins/gis/lib/leaflet/plugins/leaflet-plugins.css" rel="stylesheet"/>
<link href="plugins/gis/lib/leaflet/plugins/leaflet.markercluster.css" rel="stylesheet"/>
<link href="plugins/gis/css/leaflet_nodirection.css" rel="stylesheet"/><script src="prive/javascript/jquery.js?1536646965" type="text/javascript"></script>
<script src="prive/javascript/jquery-migrate-3.0.1.js?1536646965" type="text/javascript"></script>
<script src="prive/javascript/jquery.form.js?1536646965" type="text/javascript"></script>
<script src="prive/javascript/jquery.autosave.js?1536646965" type="text/javascript"></script>
<script src="prive/javascript/jquery.placeholder-label.js?1536646966" type="text/javascript"></script>
<script src="prive/javascript/ajaxCallback.js?1536646965" type="text/javascript"></script>
<script src="prive/javascript/js.cookie.js?1536646965" type="text/javascript"></script>
<script src="prive/javascript/jquery.cookie.js?1536646964" type="text/javascript"></script>
<!-- insert_head -->
<script src="plugins-dist/mediabox/javascript/jquery.colorbox.js?1536646928" type="text/javascript"></script>
<script src="plugins-dist/mediabox/javascript/spip.mediabox.js?1536646928" type="text/javascript"></script><script src="plugins-dist/porte_plume/javascript/jquery.markitup_pour_spip.js?1536646913" type="text/javascript"></script>
<script src="plugins-dist/porte_plume/javascript/jquery.previsu_spip.js?1536646913" type="text/javascript"></script>
<script src="local/cache-js/jsdyn-javascript_porte_plume_start_js-20f6904a.js?1536765941" type="text/javascript"></script>
<script src="//maps.google.com/maps/api/js?language=fr&amp;key=ABQIAAAAmCTzud4p7B2aEZUdUPDmwBSDPGbnJ4xx_SP7WCFPgFb71W7qsxROBsyAOU1YWCIVLgWzjSjSAOvmeQ" type="text/javascript"></script>
<script src="skel/js/gui.js" type="text/javascript"></script> <meta content="Situé à Marseille (15ème), L'hôpital Edouard Toulouse accueille des patients présentant toutes les pathologies mentales." name="Description"/>
</head>
<body><div id="bodywrap">
<!-- banner -->
<div id="banner"><a name="top"></a>
<h1>Centre Hospitalier Edouard Toulouse</h1>
<h2>Marseille</h2>
<object data="skel/img/bandeau.swf" height="152" type="application/x-shockwave-flash" width="910">
<param name="movie" value="skel/img/bandeau.swf"/>
<img alt="logo" id="logo_website" src="skel/img/logo.png"/>
</object>
</div>
<!-- #bannner -->
<!-- container -->
<div id="container"><div class="home_welcome" id="main">
<!-- menu left -->
<div id="menul" style="height:325px">
<h1><a class="section1" href="spip.php?rubrique1">Le Centre Hospitalier Edouard Toulouse</a></h1>
<h1><a class="section2" href="spip.php?rubrique7">Espace usagers</a></h1>
<h1><a class="section3" href="spip.php?rubrique13">NOS OFFRES D’EMPLOI</a></h1>
<h1><a class="section5" href="spip.php?rubrique17">Vie Sociale </a></h1>
<div id="count-mp">0</div>
<div id="count-drh">            6</div>
</div>
<!-- #menu left -->
<!-- content -->
<div id="content">
<a accesskey="2" name="contenu"></a>
<div class="home_intro"><p><strong>Le CH Edouard Toulouse</strong> établissement public de santé mentale situé dans le 15ème arrondissement de Marseille  dispose de structures intra et extra hospitalières pour la prise en charge de patients (adultes, adolescents, enfants) souffrant de pathologie psychiatrique. Il dessert les 1er, 2ème, 3ème, 13ème, 14ème, 15ème, 16ème arrondissements ainsi que Septèmes les Vallons et les Pennes Mirabeau.</p></div>
<img alt="Actualités du CH Edouard Toulouse" src="skel/img/but_actu_ch.png"/>
<div class="home_news news_ch">
<a class="illus illus1" href="spip.php?article636"><img alt="Affiches d'information de Santé Publique France sur le Covid19." class="spip_logo spip_logos" height="80" src="local/cache-gd2/66/54396af89624c9412ec4028549b5f3.jpg?1585730418" width="120"/></a>
<h1><a href="spip.php?article636">Affiches d’information de Santé Publique France sur le Covid19.</a></h1>
<p>Affiches Covid 19 Santé Publique.</p> <a class="more" href="spip.php?article636">Lire la suite</a>
</div>
<div class="home_news news_ch">
<a class="illus illus2" href="spip.php?article634"><img alt="INFOS COVID 19" class="spip_logo spip_logos" height="80" src="local/cache-gd2/13/59b670952a9b828d398e20cdbd0860.jpg?1585149974" width="120"/></a>
<h1><a href="spip.php?article634">INFOS COVID 19 </a></h1>
<p><strong>L’hôpital Edouard Toulouse vous informe des mesures prises dans le cadre de l’ épidémie qui sévit actuellement sur notre territoire. </strong></p> <a class="more" href="spip.php?article634">Lire la suite</a>
</div>
<div class="home_news news_ch">
<h1><a href="spip.php?article602">L’HÔPITAL EDOUARD TOULOUSE RECRUTE DES MEDECINS PSYCHIATRES</a></h1>
<p><strong> Ces postes sont gratifiés de prime d’engagement de carrière hospitalière.</strong></p>
<p><strong>Contact médical : Docteur Yves GUILLERMAIN, Président de la CME : 0763195465 ou secrétariat CME : 0491969679<br class="autobr"/>
Mail yves.guillermain@ch-edouard-toulouse.fr ou secrétariat CME :  isabelle.mesle@ch-edouard-toulouse.fr  </strong></p> <a class="more" href="spip.php?article602">Lire la suite</a>
</div>
<img alt="Actualités des associations" src="skel/img/but_actu_asso.png"/>
<div class="home_news news_asso">
<h1><a href="spip.php?article640">34èmes Journées de l’AMPI</a></h1>
<p>15-16 Octobre 2020 -  IFSI La Blancarde.</p> <a class="more" href="spip.php?article640">Lire la suite</a>
</div>
</div>
<!-- #content -->
<!-- more -->
<div id="more">
<form action="spip.php?page=recherche" method="get"><div>
<label for="recherche">Recherche</label>
<input class="text" id="recherche" name="recherche" onfocus="this.value=''" type="text" value="CHERCHER"/>
<input alt="OK" class="image" src="skel/img/but_search.png" type="image"/>
<input name="page" type="hidden" value="recherche"/>
</div></form>
<a class="map" href="spip.php?rubrique9"><img alt="carte de marseille" src="skel/img/map.png"/></a>
<ul>
<li><a class="even" href="spip.php?rubrique19">&gt; Urgences</a></li>
<li><a href="spip.php?rubrique36">&gt; Offres d’emploi</a></li>
<li><a class="even" href="spip.php?rubrique22">&gt; Actualités</a></li>
<li><a href="spip.php?rubrique21">&gt; Partenaires</a></li>
<li><a class="even" href="spip.php?rubrique20">&gt; Contacts</a></li>
<li><a href="spip.php?rubrique90">&gt; Bienvenue au Théâtre l’Astronef  - Dans le 15ème arrondissement de Marseille.</a></li>
</ul>
</div>
<!-- #more -->
<hr/>
</div></div>
<!-- #container  -->
<!--footer-->
<div id="footer"><div>
<ul>
<li class="first"><a href="spip.php?article3">Contacts</a></li>
<li><a href="spip.php?article2">Mentions légales</a></li>
<li><a href="spip.php?page=plan">Plan du site</a></li>
<li><a href="spip.php?page=backend">Flux RSS</a></li>
</ul>
</div></div>
<!-- #footer -->
</div></body></html>
