<!DOCTYPE html>
<html lang="en">
<head>
<title>【贝多多官网】全资产记账管家_理财信用卡基金记账管理</title>
<meta charset="utf-8"/>
<meta content="webkit|ie-comp|ie-stand" name="renderer"/>
<meta content="29be79001f7715cf" property="wb:webmaster"/>
<meta content="3550251275641117521676375" property="qc:admins"/>
<meta content="eagsR5x8V8" name="baidu-site-verification"/>
<meta content="lZVNn3q2AR" name="baidu-site-verification"/>
<meta content="WAYc1iSxqo" name="baidu-site-verification"/>
<meta content="66f1fd95148c6f6fffd650d7292f3503" name="360-site-verification"/>
<meta content="183fc7c866ccae512828fe1c3d9a9b18" name="360-site-verification"/>
<meta content="5d4b775f121411532006718b30ba594d_1463408545" name="shenma-site-verification"/>
<meta content="74dd06720e909f046ed19a70bbdba1d6_1463408445" name="shenma-site-verification"/>
<meta content="vEqNfUvEYM" name="sogou_site_verification"/>
<meta content="IE=edge, chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<meta content="贝多多,全资产记账,银行理财记账,信用卡管家" name="keywords"/>
<meta content="贝多多是一款全资产记账工具，支持银行理财记账、信用卡记账、基金记账、互联网宝宝记账及银行理财记账，同时还有理财社区供大家提高理财能力。" name="description"/>
<meta content="no-transform" http-equiv="Cache-Control"/>
<meta content="no-siteapp" http-equiv="Cache-Control"/>
<meta content="format=html5; url=http://wap.beidd.com/" http-equiv="mobile-agent"/>
<meta content="format=xhtml; url=http://wap.beidd.com/" http-equiv="mobile-agent"/>
<link href="/css/style.css?version=20170607" rel="stylesheet"/>
<link href="/css/index.css?version=20170607" rel="stylesheet"/>
<script src="/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="/js/jquery.cookie.js" type="text/javascript"></script>
<script src="/js/plugIn/My97DatePicker/WdatePicker.js"></script>
<script src="/js/common/common.js?version=20170607" type="text/javascript"></script>
<script src="/js/common/sliderImg.js?version=20170607" type="text/javascript"></script>
<script src="/js/index.js?version=20170607"></script>
<script type="text/javascript">
        var _vds = _vds || [];
        window._vds = _vds;
        (function(){
            _vds.push(['setAccountId', '839c663b4cc37978']);

            var user_id = "";
            if(user_id){
                _vds.push(['setCS1', 'user_id', user_id]);
            }

            var user_nick = "";
            if(user_nick){
                _vds.push(['setCS2', 'user_nick', user_nick]);
            }

            (function() {
                var vds = document.createElement('script');
                vds.type='text/javascript';
                vds.async = true;
                vds.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'dn-growing.qbox.me/vds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(vds, s);
            })();
        })();
    </script>
<script>
        (function(){
            var bp = document.createElement('script');
            var curProtocol = window.location.protocol.split(':')[0];
            if (curProtocol === 'https') {
                bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
            }
            else {
                bp.src = 'http://push.zhanzhang.baidu.com/push.js';
            }
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(bp, s);
        })();
    </script>
<script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?d93daaf6610b44378ac87bfded2b2561";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
<!-- 手机访问pc网站，自动跳转到wap站 -->
<script src="/js/uaredirect.js" type="text/javascript"></script>
<script type="text/javascript">uaredirect("http://wap.beidd.com/");</script>
</head>
<body style="background: white;">
<link href="/css/header.css?version=20170607" rel="stylesheet"/>
<div class="header header-index down-header">
<div class="top-layout clr">
<a class="bdd-logo" href="/index"><h1 class="logo" id="logo"></h1></a>
<div class="nav_head clr" id="nav-head">
<ul class="clr" id="index_head_list">
<li class="li li_login fr" id="li_login">
<div id="loginTag" style="display: none">
<a class="login-btn" href="#loginBox" id="loginBtn">登录</a><span class="seq">|</span><a class="regist-btn" href="/register" id="regist-btn">注册</a>
</div>
<div id="loggedBtn" style="display: none">
<a id="slogan"></a><span class="seq">|</span><a class="log-out" href="/action/user/logout">退出</a>
</div>
</li>
<li class="li fr" id="li_down"><a href="/down">下载</a></li>
<li class="li fr" id="li_calc"><a href="/calc">计算器</a></li>
<!--li id="li_bbs" class="li fr"><a href="/bbs/bbs_index">社区</a></li-->
<!--li id="li_news" class="li fr"><a href="/news/index">资讯</a></li-->
<!--li id="li_platform" class="li li-platform fr">
                    <a href="/platform_nav">平台</a>
                    <ul class="platform-first-menu">
                        <li><a href="/platform_nav">平台导航</a></li>
                        <li class="li-hot-platform">
                            <a class="hot-platform-nav" href="javascript:;">热门平台</a>
                            <ul class="platform-second-menu hot-platform-menu">
                                <li><a href="/platform/tuandai" title="团贷网" target="_blank">团贷网</a></li>
                                <li><a href="/platform/touna" title="投哪网" target="_blank">投哪网</a></li>
                                <li><a href="/platform/ppmoney" title="PPmoney" target="_blank">PPmoney</a></li>
                            </ul>
                            <i class="arrow-right"></i>
                        </li>
                        <li class="li-risk-platform" id="li-risk-platform">
                            <a class="risk-platform-nav" href="javascript:;">问题平台</a>
                            <ul class="platform-second-menu risk-platform-menu" id="risk-platform-menu">
                                
                            </ul>
                            <i class="arrow-right"></i>
                        </li>
                    </ul>
                </li-->
<li class="li fr" id="li_record"><a href="/record/record">记账</a>
</li><li class="li fr" id="li_index"><a href="/index">首页</a></li>
</ul>
</div>
<!--div class="down_nav_head clr" id="down_nav_head" style="display: none">
            <ul id="down_head_list" class="down-head-list clr">
                <li class="top-qr"><span class="top-icon-qr"></span></li>
                <li class="top-ios-download"><a href="https://itunes.apple.com/cn/app/bei-duo-duo-p2p-ji-zhang-ben/id982758376?mt=8">立即下载</a></li>
                <li class="top-android-download"><a href="http://res.daiyoubang.com/apks/beidd.apk">立即下载</a></li>
            </ul>
        </div-->
</div>
</div>
<script src="/js/common/common.js" type="text/javascript"></script>
<script src="/js/header.js?version=20170607" type="text/javascript"></script>
<link href="/css/header_reset.css?version=20170607" rel="stylesheet"/>
<!--内容 start-->
<!-- banner -->
<div class="banner">
<div class="slider-banner" id="slider-banner">
<!-- 轮播图 -->
<ul id="slider">
<li>
<img alt="从记账开始学习理财，带给你更多的金钱和财务自由" src="/img/index/index_bg03.jpg"/>
<!-- 文字 -->
<div class="text-area">
<h2 class="text-area-title">全资产记账</h2>
<p class="text-area-content">从记账开始学习理财，带给你更多的金钱和财务自由</p>
</div>
</li>
<li>
<img alt="助你了解自身财务状况，轻松掌握家庭资产分布" src="/img/index/index_bg01.jpg"/>
<!-- 文字 -->
<div class="text-area">
<h2 class="text-area-title">全资产记账</h2>
<p class="text-area-content">助你了解自身财务状况，轻松掌握家庭资产分布</p>
</div>
</li>
<li>
<img alt="资产记账管理，帮你更好的打理个人资产" src="/img/index/index_bg02.jpg"/>
<!-- 文字 -->
<div class="text-area">
<h2 class="text-area-title">全资产记账</h2>
<p class="text-area-content">资产记账管理，帮你更好的打理个人资产</p>
</div>
</li>
<li>
<img alt="从记账开始学习理财，带给你更多的金钱和财务自由" src="/img/index/index_bg03.jpg"/>
<!-- 文字 -->
<div class="text-area">
<h2 class="text-area-title">全资产记账</h2>
<p class="text-area-content">从记账开始学习理财，带给你更多的金钱和财务自由</p>
</div>
</li>
<li>
<img alt="助你了解自身财务状况，轻松掌握家庭资产分布" src="/img/index/index_bg01.jpg"/>
<!-- 文字 -->
<div class="text-area">
<h2 class="text-area-title">全资产记账</h2>
<p class="text-area-content">助你了解自身财务状况，轻松掌握家庭资产分布</p>
</div>
</li>
</ul>
<!-- 角标 -->
<ol id="indicator">
</ol>
</div>
<!-- 下载 -->
<div class="app-download crl">
<a class="android-down" href="http://res.daiyoubang.com/apks/beidd.apk"></a>
<a class="ios-down" href="https://itunes.apple.com/cn/app/bei-duo-duo-p2p-ji-zhang-ben/id982758376?mt=8"></a>
</div>
</div>
<!-- 贝多多简介 -->
<div class="bdd-brief layout">
<h2>贝多多</h2>
<p class="bdd-brief-subhead">贝多多是一款全资产记账管理工具，能将你的理财投资、信用卡账单、基金、股票、银行卡等所有资产、负债一站式管理起来做到一个APP，管理所有资产，方便你更好的打理自身资产。</p>
<ul class="product-show-list clr">
<li>
<img alt="全资产记账" src="/img/index/icon_zichantongji.png"/>
<p class="title">全资产记账</p>
<p class="content">一站式资产记账管理</p>
</li>
<li>
<img alt="p2p记账" src="/img/index/iocn_p2pzhangbeng.png"/>
<p class="title">p2p记账</p>
<p class="content">综合评级，精准记账，回款及时提醒</p>
</li>
<li>
<img alt="信用卡管理" src="/img/index/icon_xinyongka.png"/>
<p class="title">信用卡管理</p>
<p class="content">多张信用卡一并管理，还款提醒不用担心</p>
</li>
</ul>
<a class="look-detail" href="/down">查看详情</a>
</div>
<!-- 理财社区 -->
<!--div class="community layout">
    <h3 class="module-title">理财社区</h3>
    <p class="community-title-subhead">在分享交流中增加理财知识，提升财商</p>
    <div class="community-con clr">
        <ul class="community-list fl" id="community-list1">

        </ul>
        <ul class="community-list fl" id="community-list2">

        </ul>
    </div>
    <a class="look-detail" href="/bbs/bbs_index">查看更多</a>
</div-->
<!-- 用户评价 -->
<!--div class="comment layout">
    <h3 class="module-title">用户评价</h3>
    <p class="comment-title-subhead">轻松踏上理财路程，体会不一样的成就感</p>
    <div class="slider-wrap" id="slider-user-list">
        <div class="slider-con">
            <ul class="slider-list clearfix">
                <li class="first-li">
                    <div class="user-comment-area item1">
                        <dl class="default-info">
                            <dt class="user-name">浩瀚的大洋</dt>
                            <dd>
                                <p class="user-title">理财专栏作家</p>
                                <p class="user-brief">贝多多记账是一款非常专业的银行理财、基金记账管理软件...</p>
                            </dd>
                        </dl>
                        <dl class="hover-info">
                            <dt class="user-name">浩瀚的大洋</dt>
                            <dd><p class="user-comment">贝多多记账是一款非常专业的银行理财、基金记账管理软件，同时提供丰富的理财资讯供投资者了解金融行情，适合有管理投资需求的广大用户。</p></dd>
                        </dl>
                    </div>
                </li>
                <li class="second-li">
                    <div class="user-comment-area item2">
                        <dl class="default-info">
                            <dt class="user-name">乐山大佛</dt>
                            <dd>
                                <p class="user-title">资深理财规划师</p>
                                <p class="user-brief">精准记账，回款及时提醒--理财记账...</p>
                            </dd>
                        </dl>
                        <dl class="hover-info">
                            <dt class="user-name">乐山大佛</dt>
                            <dd><p class="user-comment">精准记账，回款及时提醒--理财记账我选贝多多</p></dd>
                        </dl>
                    </div>
                </li>
                <li class="third-li">
                    <div class="user-comment-area item3">
                        <dl class="default-info">
                            <dt class="user-name">天天</dt>
                            <dd>
                                <p class="user-title">资深互联网金融投资者</p>
                                <p class="user-brief">贝多多精确记录了我的每一笔投资、回款、收益，实现...</p>
                            </dd>
                        </dl>
                        <dl class="hover-info">
                            <dt class="user-name">天天</dt>
                            <dd><p class="user-comment">贝多多精确记录了我的每一笔投资、回款、收益，实现资产尽览，又有社区教我学习投资理财技巧，了解行业发展，实现财富增值。感谢贝多多，让我记账更方便，理财更简单，生活更精彩。</p></dd>
                        </dl>
                    </div>
                </li>
                <li class="fourth-li">
                    <div class="user-comment-area item4">
                        <dl class="default-info">
                            <dt class="user-name">36kr</dt>
                            <dd>
                                <p class="user-title">互联网生态服务平台</p>
                                <p class="user-brief">贝多多在资产记账的细分领域回归产品本源，使用户得到...</p>
                            </dd>
                        </dl>
                        <dl class="hover-info">
                            <dt class="user-name">36kr</dt>
                            <dd><p class="user-comment">贝多多在资产记账的细分领域回归产品本源，使用户得到良好的记账体验，不仅有强大的银行理财记账功能，围绕家庭资产进一步拓展可记账的资产类别...</p></dd>
                        </dl>
                    </div>
                </li>
                <li class="fifth-li">
                    <div class="user-comment-area item5">
                        <dl class="default-info">
                            <dt class="user-name">东郭</dt>
                            <dd>
                                <p class="user-title">互金领域法律人</p>
                                <p class="user-brief">我所理解的贝多多，是一个以记账为核心功能、以投资交流...</p>
                            </dd>
                        </dl>
                        <dl class="hover-info">
                            <dt class="user-name">东郭</dt>
                            <dd><p class="user-comment">我所理解的贝多多，是一个以记账为核心功能、以投资交流分享为特色的工具型社交平台。在贝多多，你会体验到“当记账成为一种习惯，财富增值更简单”。</p></dd>
                        </dl>
                    </div>
                </li>
                <li class="sixth-li">
                    <div class="user-comment-area item6">
                        <dl class="default-info">
                            <dt class="user-name">相忘于江湖</dt>
                            <dd>
                                <p class="user-title">资深互金投资者</p>
                                <p class="user-brief">记账软件贴近实操，对用户运用场景有全方位的廊阔，覆盖...</p>
                            </dd>
                        </dl>
                        <dl class="hover-info">
                            <dt class="user-name">相忘于江湖</dt>
                            <dd><p class="user-comment">记账软件贴近实操，对用户运用场景有全方位的廊阔，覆盖了绝大部分的用户需求。同时与用户互动有力，随时响应新的需求，可谓与时俱进，是市面上难得的一款理财记账超级运用APP。</p></dd>
                        </dl>
                    </div>
                </li>

                <li class="seventh-li">
                    <div class="user-comment-area item7">
                        <dl class="default-info">
                            <dt class="user-name">互金投资手记</dt>
                            <dd>
                                <p class="user-title">资深互金投资者</p>
                                <p class="user-brief">现在一直用贝多多来记录我的投资理财。支持的理财品种全...</p>
                            </dd>
                        </dl>
                        <dl class="hover-info">
                            <dt class="user-name">互金投资手记</dt>
                            <dd><p class="user-comment">
                                现在一直用贝多多来记录我的投资理财。支持的理财品种全、界面简洁美观、资产一目了然、用户体验好、理财干货多。除了记账，平时还用贝多多来追踪基金、股票。祝贝多多越做越好。</p></dd>
                        </dl>
                    </div>
                </li>
            </ul>
        </div>
        <a href="javascript:;" class="slider-btn go-prev"></a>
        <a href="javascript:;" class="slider-btn go-next"></a>
    </div>
</div-->
<!-- 合作伙伴 -->
<!--div class="partner">
    <div class="partner-box">
        <div class="index-tit">
            <h3 class="module-title">合作伙伴</h3>
        </div>
        <ul class="partner-list clr">
            <li><a href="javascript:;"><img src="/img/index/partner01.jpg" alt="腾讯网"></a></li>
            <li><a href="javascript:;"><img src="/img/index/partner02.jpg" alt="36Kr"></a></li>
            <li><a href="javascript:;"><img src="https://bbsimg.cardniu.com/static/image/perfect/2017logo.png" alt="卡牛"></a></li>
            <li><a href="javascript:;"><img src="https://res.sui.com/img/common/logo-s-v12.png" alt="随手记"></a></li>
            <li><a href="/platform/ppmoney"><img src="/img/index/partner03.jpg" alt="ppmoney"></a></li>
            <li><a href="/platform/ppdai"><img src="/img/index/partner04.jpg" alt="拍拍贷"></a></li>
            <li><a href="/platform/niwodai"><img src="/img/index/partner05.jpg" alt="你我贷"></a></li>
            <li><a href="/platform/touna"><img src="/img/index/partner06.jpg" alt="投哪网"></a></li>
            <li><a class="tuandai" href="/platform/tuandai"><img src="/img/index/partner07.jpg" alt="团贷网"></a></li>
            <li><a class="sohu" href="javascript:;"><img src="/img/index/partner08.jpg" alt="搜狐"></a></li>
        </ul>
    </div>
</div-->
<!--内容 End-->
<link href="/css/footer.css?version=20170607" rel="stylesheet"/>
<!--div class="links">
  <div class="layout-links">
    <span>友情链接:</span>
    <a href="http://www.qianxiangbank.com" target="_blank">钱香金融</a>
    <a href="http://xd.yxpuhui.com/" target="_blank">宜信贷款</a>
    <a href="http://www.51wddh.com" target="_blank">网贷平台</a>
    <a href="http://www.darenloan.com/" target="_blank">达人贷</a>
    <a href="http://www.xpshe.com" target="_blank">星评社</a>
    <a href="http://daikuan.2345.com/" target="_blank">手机app贷款</a>
  </div>
</div-->
<div class="footer">
<div class="fw clr">
<dl class="dl1">
<dt>网站功能</dt>
<dd><a href="/general/aboutus">关于贝多多</a><a href="/client_account">网贷记帐</a><a href="/jsp/help.jsp">使用帮助</a></dd>
</dl>
<dl class="dl2">
<dt>联系我们</dt>
<dd><p>官方QQ群：333820915<br/>服务邮箱：kefu@beidd.com</p></dd>
</dl>
<dl class="dl3">
<dt>关注我们</dt>
<dd>
<a href="javascript:void(0)"><i class="icn weixin"></i><div class="two-code"><img alt="" class="footer_img" src="/img/wx-qr.png"/></div></a>
<a href="javascript:void(0)"><i class="icn qq"></i><div class="qq-group"><img alt="" class="footer_img" src="/img/qq-group.png"/></div></a>
</dd>
</dl>
<dl class="dl4">
<dt>下载客户端</dt>
<dd class="clr">
<img alt="" class="fl footer_img" src="/img/two-code.jpg"/>
<div class="fl footer_img download">
<a href="https://itunes.apple.com/us/app/dai-you-bang-zui-liu-xing/id982758376?l=zh&amp;ls=1&amp;mt=8" rel="nofollow" target="_blank"><img alt="" class="footer_img" src="/img/ios.png"/></a>
<a href="http://appstore.huawei.com/app/C10261905" rel="nofollow" target="_blank"><img alt="" class="footer_img" src="/img/android.png"/></a>
</div>
</dd>
</dl>
</div>
<div class="copyright"><span>Copyright © 2015-2021 深圳贝多多科技有限公司 版权所有 </span>
<script type="text/javascript">
      var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
      document.write(unescape("%3Cspan id='cnzz_stat_icon_1257615261'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol +
              "s4.cnzz.com/z_stat.php%3Fid%3D1257615261%26show%3Dpic1' type='text/javascript'%3E%3C/script%3E"));
    </script>
<span><a class="about" href="https://beian.miit.gov.cn" target="_blank"> 粤ICP备15013016号-2</a></span><a href="http://webscan.360.cn/index/checkwebsite/url/www.daiyoubang.com" rel="nofollow"><img border="0" class="footer_img" src="http://img.webscan.360.cn/status/pai/hash/81795b24d57b6e50a90ef4894b3d7ba3/?size=74x27" style="margin:0 5px;"/></a> </div>
<div class="copyright" style="margin-top: -20px;">
<a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=44030502004700" target="_blank">
<img src="http://res.daiyoubang.com/org_logo/beian.png"/><span>粤公网安备 44030502004700号</span>
</a>
</div>
</div>
<script src="/js/jquery.cookie.js" type="text/javascript"></script>
<div class="mask" id="loginBox">
<div class="login clr">
<h2 class="hd"><a class="close" href=""><i class="icn"></i></a>登录</h2>
<div class="bd clr">
<div class="passport fl">
<form action="/action/user/login" id="loginForm" method="post">
<ul>
<li><label class="label">手机号：</label><input style="display:none"/><input class="ipt" id="phone-login" name="phoneNo" placeholder="手机号" type="text"/></li>
<li class="hide" id="error-phone-li"><label class="label"></label><p class="error-tip" id="error-phone"></p></li>
<li><label class="label">密码：</label><input class="ipt" id="pwd-login" name="password" placeholder="密码" type="password"/></li>
<li class="hide" id="error-pwd-li"><label class="label"></label><p class="error-tip" id="error-pwd"></p></li>
<li class="forget-pwd"><a class="" href="/password_retrieve">忘记密码？</a><label><input id="remember" type="checkbox"/>记录用户</label></li>
<li><input class="submit" onclick="saveUser()" type="submit" value="登录"/></li>
<li class="hide" id="error-li"><label class="label"></label><p class="error-tip" id="loginError"></p></li>
</ul>
</form>
<div class="sns-login"><span>社交帐号登录</span>
<a href="/action/3rd-party/qq"><i class="icn qq"></i>QQ登录</a>
<a href="/action/3rd-party/weixin"><i class="icn wx"></i>微信登录</a>
<a href="/action/3rd-party/weibo"><i class="icn sina"></i>微博登录</a></div>
</div>
<div class="reg-btn fr">
<p>还没有帐号？</p>
<a href="/register">马上注册</a>
</div>
</div>
</div>
<link href="/css/tool/tool.css" rel="stylesheet"/>
<div class="loading"></div>
</div>
<script src="/js/footer.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('#li_index').addClass('on');
        $('#li_record').removeClass('on');
        $('#li_platform').removeClass('on');
        $('#li_news').removeClass('on');
        $('#li_bbs').removeClass('on');
        $('#li_calc').removeClass('on');
        $('#li_down').removeClass('on');
    });
</script>
</body>
</html>