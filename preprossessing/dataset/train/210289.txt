<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Biography of Henry Melchior Muhlenberg Richards</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Biography of Henry Melchior Muhlenberg Richards" name="description"/>
<meta content="genealogy, Berks County, Pennsylvania, biography, biographies, pennsylvania, Henry Melchior Muhlenberg Richards" name="keywords"/>
<!-- <link rel="stylesheet" type="text/css" href="/filename.css" /> -->
</head>
<body bgcolor="#ECF4FF">
<font face="Verdana, Arial, Helvetica, sans-serif">
<h2>HENRY MELCHIOR MUHLENBERG RICHARDS</h2>
<p>Source: <i>Pennsylvania, A History</i>, George P. Donehoo, (New York: Lewis Historical Publishing Co., 1926), p. 169</p>
<p>Surnames:  Richards, Garber, Weiser, Muhlenberg, Paul, Roeder, Turner, Van Leer,
Bittner, Peck, Bennetch</p>
<p>A descendant on both his father's and his mother's side from an
ancestry of patriotic and distinguished men and women, Mr. Richards has
contributed his full share to the family honors and has most worthily
lived up to the family tradition of public service to community, State
and country.  Throughout his long life, filled with activities as
varied as they were useful, he has reached eminence in whatever he has
undertaken and has established for himself a high reputation in three
distinct fields, military, industrial, and literary.</p>
<p>Henry Melchior Muhlenberg Richards was born at Easton, Northampton
County, Pennsylvania, August 16, 1848, a son of Rev. John William and
Andora (Garber) Richards.  His paternal grandfather was Matthias
Richards, Jr., younger son of a prominent and wealthy landed proprietor
of New Hanover Township, Philadelphia, now Montgomery County, where he
was born in 1758.  He came to Reading in 1776 and served as a private
in the militia from 1777 until the Continental Army went into camp at
Valley Forge when the militia was discharged, taking part in the
battles of Brandywine and Germantown, and promoted to major of Fourth
Battalion, 1780.   He was a self-made man, remarkably well educated for
his times, and greatly respected by his fellow-citizens.  He served as
a justice of the peace for a total of forty years; was Associate Judge
of the Berks County courts; was appointed inspector of customs under
Tenche Cox and General Peter Muhlenberg; became a member of Congress
for the counties of Berks and Lancaster, from 1807 to 1811; was
appointed collector of revenue by President Madison in 1812; and was
appointed clerk of the Orphans' Court in 1823 by his intimate friend,
Governor Joseph Hiester.   He was like all of his family a faithful
Lutheran, being a member and trustee of Trinity Lutheran Church,
Reading, in which city he died in 1830.  In 1782 he married Maria
Salome Muhlenberg, born in 1766, died in 1827, the youngest daughter of
Henry Melchior and Anna Maria (Weiser) Muhlenberg, the latter a
daughter of Colonel Conrad Weiser, of Berks County, a
lieutenant-colonel in the French and Indian War, and later head of the
Indian Bureau of the Province of Pennsylvania.  Henry Melchior
Muhlenberg, D. D., considered the Patriarch of the Lutheran Church in
America, was the founder of the Muhlenberg family in this country to
which he had come from Germany in 1742.  He first went to Georgia, but
soon afterwards came to Pennsylvania where he settled at The Trappe,
now in Montgomery County, living there until his death in 1787 and
establishing a high reputation for learning.   He was the father of
three sons, all of whom became eminent leaders in the affairs of their
country: Major General Peter Muhlenberg, one of President Washington's
generals throughout the entire Revolutionary War, a member of Congress
while Washington was President, and later United States Senator from
Pennsylvania; Frederick Augustus Muhlenberg, a member of the
Continental Congress, Speaker of the House during the First and Third
congresses of the United States, and twice candidate of the Federal
party for governor of Pennsylvania; Henry Ernest Muhlenberg, D. D.,
eminent naturalist and Lutheran clergyman.   The descendants of these
three eminent men, as well as those of their four sisters, all achieved
much distinction in the many different fields of human endeavor to
which they devoted themselves.</p>
<p>John William Richards, D. D., the father of Henry Melchior
Muhlenberg Richards, was born April 18, 1803, and died at Reading,
January 24, 1854.  He was one of the children of Matthias, Jr., and
Maria Salome (Muhlenberg) Richards, and was educated at the Reading
Academy under Dr. John Grier, later reading theology under the Rev.
Henry Augustus Muhlenberg.  In 1828 he was ordained a minister in the
Lutheran Church, and from then on until 1843 served with great
distinction various Lutheran congregations in Lancaster, Berks and
Montgomery counties, and finally in Germantown.  In 1843 he was made
secretary of the Lutheran Ministerium of Pennsylvania, in which office
he served three terms, and in 1850 was elected president of this body,
holding this office until the time of his death.  In the same year he
was called to Trinity Lutheran Church, Reading, having been pastor of
St. John's Evangelical Lutheran Church at Easton since 1845.  In 1851
the degree of D. D. was conferred upon him by Jefferson College, a
Presbyterian institution of the old school.  In 1835 he married Andora
Garber, only daughter of Henry and Susanna (Paul) Garber, of Garwood,
Montgomery County, born May 21, 1815, died May 26, 1892.  Dr. and Mrs.
Richards were the parents of four children:  Adelaide Susanna; Andora
Elizabeth; Matthias Henry; and Henry Melchior Muhlenberg.</p>
<p>Henry Melchior Muhlenberg Richards came to Reading with his parents
as a small child, and was educated in the public schools of Reading,
graduating from its high school in 1864.  During the Civil War, in
1863, he served as a private in Company A, 26th Emergency Regiment of
Pennsylvania Volunteers, doing duty as a drummer.  He participated in
the battle of Gettysburg and served throughout the entire campaign
incident to that battle, narrowly escaping from capture and
imprisonment.  In 1864 he reenlisted in Company A, 195th Regiment,
Pennsylvania Volunteers, and served under General Sheridan in West
Virginia.  He entered the United States Naval Academy n 1865 and
graduated in 1869, being a star or honor graduate, was publicly
complimented by Admiral David D. Porter, and received his diploma at
the hands of General Ulysses S. Grant.  During his attendance at the
Naval Academy he made a cruise in 1866 on the historic U. S. S.
"Macedonian," captured from the British in the War of 1812; in 1867 he
cruised along the coasts of Europe on the U. S. S. "Savannah,"
participating in the great naval ovation to the Empress Eugenie at
Cherbourg, France, and in 1868, on the same ship, among the islands off
the west coast of Africa.  He was one of the last officers to be
attached to the U. S. S. "Constitution" ("Old Ironsides").</p>
<p>After graduation he was ordered to the U. S. S. "Juniata," attached
to the European squadron, and during this turn of service he
participated in the following important and interesting events:  At
Tunis, Africa, in April, 1870, averting a threatened outbreak against
the Christians; on active duty in Spain during the Carlist Insurrection
in May and June, 1870, when he was attacked by brigands near San Roque
and narrowly escaped death or capture; during the Franco-German War of
1870-1871, with the French fleet off Heligoland, in August and
September, 1870, awaiting an attack by the German fleet until dispersed
by a hurricane, and later with the German fleet at Wilhelmshaven; in
October and November, 1870, at Havre, France, anticipating an attack by
Prussian Uhlans; with Bourbaki's army in Switzerland, after its defeat
at Belfort in January, 1871; and in Marseilles, in April, 1871, during
the outbreak of the Commune and its defeat by the governmental troops. 
During 1872 he was on duty at the Torpedo Station, Newport, Rhode
Island, at which time he invented an earth connection, circuit closing
fuse, which was far in advance of anything then in use and which was
adopted by the United States Government.  In 1873 and 1874 he was
attached to the U. S. S. "Narragansett" under Commander, later Admiral,
George Dewey on surveying duty in the Pacific Ocean.  In April, 1873,
he was on duty in Panama during a revolutionary outbreak, and later
visited the savage Indians of Tiburon Island and the Yaqui tribes along
the River Mayo.  In January, 1875, having reached the rank of
lieutenant, he retired to civil life in order to devote more of his
time to his family.</p>
<p>After retiring from the Naval Service, Mr. Richards became connected
with the Philadelphia &amp; Reading Railway, at Reading, at first in the
office of the general superintendent and later in that of the engineer
of machinery, remaining until the fall of 1878.  From 1878 to 1881 he
was associated with Charles M. Roeder in the insurance business and
then became identified with the Reading Bolt &amp; Nut Works and the
rolling mills of J. H. Sternbergh &amp; Son.  In 1899 he assisted in the
consolidation of that plant and others, and in their organization into
the American Iron &amp; Steel Manufacturing Company, the largest bolt and
nut works in the world, with general offices at Lebanon.  At that time
he was made general auditor, treasurer, and a director of the new
company, in which offices he served until 1917 when the various plants
of his company were taken over by the Bethlehem Steel Company and when
Mr. Richards retired into private life.</p>
<p>During the Spanish-American War he returned to active duty with the
Navy, serving throughout all the operations in the West Indies under
Admiral Sampson.  Previously, in 1892, when war seemed imminent between
the United States and Chile, he had also volunteered for service. 
During the World War Lieutenant Richards volunteered for active service
in both the Navy and Army, but could not be commissioned, having passed
the legal age.  However he served most effectively as director of naval
service for the Lebanon County branch of the Pennsylvania Council of
National Defense and Committee of Public Safety, and was also one of
the "four-minute" speakers at many public gatherings.</p>
<p>In politics he is a supporter of the Republican Party, but, though
frequently pressed to accept public office, has always declined to do
so.  The only office he ever held was that of city treasurer of
Lebanon, in which he served from 1918 to 1920, and which was thrust
upon him unexpectedly and without solicitation on his part.  However he
has been at various times the recipient of honorary appointments in
connection with his well-known and deep interest in historical matters.
 In 1893 he was appointed by Governor Pattison a member of the
commission on "Frontier Forts of Pennsylvania, Prior to 1783," and his
exhaustive report on this subject was ordered printed by the
Legislature and has become the standard work on the subject.  In 1918
he was appointed by Governor Brumbaugh a member of the advisory
committee for the Preservation of Public Records, and later in the same
year he was made a member of the Pennsylvania War History Commission. 
Muhlenberg College, Allentown, Pennsylvania, conferred upon him, June
16, 1910, the honorary degree of Literary Doctor in recognition of his
many published works, mainly of a historic character, which have
appeared from time to time in the leading periodicals of the country
and from the presses of various prominent publishers.</p>
<p>His religious affiliations are with the Evangelical Lutheran Church,
of which all his ancestors have been distinguished members.  For
eighteen years he was superintendent of the Trinity Lutheran Sunday
School at Reading, introducing many changes and improvements.  Upon his
removal to Lebanon he was elected a trustee of Salem Evangelical
Lutheran Church and engaged actively in the work of its Bible School. 
Later he became identified with St. James Evangelical Lutheran Church,
and became superintendent of its Sunday School.  While a resident of
Reading he was for some time a member of the board of directors of the
Homeopathic Hospital.  In Lebanon he was far [for] many years president
of the Associated Charities, and a trustee of the Southeastern
Playground Association, as well as president of the Fidelity Building &amp;
Loan Association.</p>
<p>He is a member of many societies, including the following:
Pennsylvania Society, Sons of the Revolution; Military Order, Loyal
Legion of the United States; Grand Army of the Republic, in which he
has been National Aide-de-Camp, Post Quartermaster, and a delegate to
many encampments; Naval Order of the United States, in which he has
been General Registrar, State Historian, and has also held other
offices; Military Order of Foreign Wars, in which he has been a
national delegate; Naval and Military Order Spanish-American War;
Veterans of Foreign Wars of United States, in which he has been
National Officer of the Day, Department Patriotic Instructor and
delegate to many encampments; Naval Association of Reading; Order of
Washington, in which he has been Deputy Commander for Pennsylavnia;
Naval Academy Graduates' Association; Navy Athletic Association;
Military Society of the Frontier, in which he has been Second
Vice-Commander General; United Spanish War Veterans, in which he has
been Camp Chaplain; St. John's Lodge, No. 435, Reading, Free and
Accepted Masons, of which he is a Past Master; American National Red
Cross Society; American Humane Association; American Red Star Animal
Relief; National Security League; American Forestry Society; Societe
Academique d'Histoire Internationale, Paris, France, of which is is a
member of honor; Pennsylvania Federation of Historical Societies, of
which he has been president; Historical Society of Pennsylvania;
Genealogical Society of Pennsylvania; National Geographic Society;
National Genealogical Society, of which he has been State President for
Pennsylvania; Pennsylvania-German Society, of which he has been
secretary, president and chairman of the executive board; Wyoming
Historical and Geological Society of Pennsylvania; Site and Relic
Society of Germantown, Pennsylvania; Lebanon County Historical Society,
of which he has been president; Historical Society of Berks County,
Pennsylvania; Historical Society of Montgomery County, Pennsylvania;
Lutheran Historical Society, of which he has been vice-president; and
Northampton County Historical and Genealogical Society.  He is not very
much interested in club life, but is a member of the Authors' Club of
London, England, and of La Renaissance Nationale of Paris, France.</p>
<p>Mr. Richards married, at Reading, December 26, 1871, Ella Van Leer,
daughter of Branson and Drucilla (Turner) Van Leer, the former a
descendant of Werner von Loehr, Patrician Mayor of the ancient German
city of Mayence in 1521, the latter descended from many distinguished
English and allied lines.  Mr. and Mrs. Richards are the parents of
four children: 1. Rev. Henry Branson Richards, born February 5, 1873;
married Martha Ann Bittner.  2.  Charles Matthias Richards, M. D., born
April 19, 1875; married Laura May Peck.  3.  Florence Richards, born
March 23, 1878.  4.  Alice Richards, born September 8, 1880; married
Ira Leonard Bennetch, the latter a descendant of the ancient French
family of Basnage.  The Richards family home is located at No. 4 West
High Street, Lebanon.</p>
<hr style="color:black; height:5px; width:50%; text-align:center"/>
<div align="center"><!-- #config timefmt="%A %B %d %Y" -->
<p><small>Last Modified Thursday, 16-Oct-2008 20:58:02 EDT</small></p></div>
<div align="center">
<a href="../../bcdisplay.html">Home Page</a>
          
<a href="index.html">Pennsylvania Biographies</a>
</div>
<div align="center">
<div align="center">
<p><small>This Berks County genealogy Web site is part of the <a href="http://www.pagenweb.org/" shape="rect">PAGenWeb Project</a> and the <a href="http://www.usgenweb.org/" shape="rect">USGenWeb Project</a>.</small></p>
</div>
<div align="center">
<p><small>© copyright 2004-2017 <a href="mailto:janielou13@yahoo.com" shape="rect">Jane Unger</a> for the Berks County PAGenWeb Project unless explicitly otherwise noted. All rights reserved.</small></p>
<p><small>Unless indicated otherwise in a particular page carrying this copyright notice,
permission to use, copy, and distribute documents and related graphics delivered from
this World Wide Web server (<a href="http://berks.pa-roots.com/">http://berks.pa-roots.com/</a>) for non-commercial use is
hereby granted, provided that the above copyright notice appears in all copies and that
both that copyright notice and this permission notice appear. All other rights reserved.
Jane Unger disclaims all warranties with regard to this information. The
information described herein is provided as is without warranty of any kind, either
expressed or implied.</small></p>
</div>
</div>
</font>
</body>
</html>
