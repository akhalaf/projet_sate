<!DOCTYPE html>
<html lang="cs">
<head>
<meta charset="utf-8"/>
<meta content="" name="affiliate"/>
<meta content="velka" name="subaccount"/>
<meta content="ubuntu_fullloan" name="theme"/>
<meta content="" name="form_theme"/>
<meta content="light" name="gdpr_design"/>
<meta content="1.2" name="gdpr_design_version"/>
<meta content="index" name="page"/>
<meta content="ui.scroll, ui.mobile, ui.cookies_policy" name="components"/>
<meta content="9ed38d946f4c1738be65a0081c26e8726a411db6" name="revision"/>
<meta content="" name="parameters_priority"/>
<meta content="https://financecdn.com/cdn/landings/img/ubuntu_fullloan/fb_preview_cs.png" property="og:image"/>
<meta content="website" property="og:type"/>
<meta content="" property="og:title"/>
<meta content="PenÃ­ze do 15 minut na ÃºÄtÄ." property="og:description"/>
<meta content="200" property="og:image:width"/>
<meta content="200" property="og:image:height"/>
<title></title>
<script async="" src="https://www.datadoghq-browser-agent.com/datadog-rum-eu.js" type="text/javascript">
</script>
<script>
    var DD_interval = setInterval(function() {
        if (window.DD_RUM) {
            window.DD_RUM.init({
                clientToken: 'pub440d682c8a0d0fb3c065eb860e08a358',
                applicationId: 'c2350aa9-a98d-44a1-b36d-e2cdee90e04f',
                sampleRate: 5,
            });
            clearInterval(DD_interval);
        }
    }, 30);
</script>
<meta content="Online pÅ¯jÄka, rychlÃ¡ pÅ¯jÄka, vyhledÃ¡vaÄ pÅ¯jÄek, penÃ­ze rychle" name="keywords"/>
<meta content="PenÃ­ze do 15 minut na ÃºÄtÄ." name="description"/>
<meta content="consumer_loan" name="product"/>
<meta content="width=device-width, initial-scale=1, user-scalable=no, height=device-height" name="viewport"/>
<link href="/cdn/landings/img/ubuntu_fullloan/favicon_cs.ico" rel="shortcut icon"/>
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '190521605398815', {type: 'remarketing_id'});
fbq('addPixelId', '190521605398815', {type: 'fb_pixel'});
fbq('trackSingle', '190521605398815', 'PageView');
</script>
</head>
<body class="index">
<div id="header">
<div class="mobile_toggle"></div>
<div class="container">
<div id="logo">
<img alt="logo" src=" logo.png "/>
<span>PÅ¯jÄka Online</span>
</div>
<div id="menu_top">
<div class="toggle">
<span><a href="#guide">proÄ my</a></span>
<span><a href="#reviews">ohlasy</a></span>
<span><a href="#footer">kontakt</a></span>
</div>
</div>
</div>
</div>
<div id="main">
<div id="content">
<div class="section" id="form">
<div class="container">
<h1>PÅ¯jÄte si aÅ¾ 300 000 kÄ <span>NÃ¡Å¡ systÃ©m pomÃ¡hÃ¡ kaÅ¾dÃ½ mÄsÃ­c najÃ­t vhodnou pÅ¯jÄku pro vÃ­ce neÅ¾ 90 000 Å¾adatelÅ¯ po celÃ©m svÄtÄ.</span></h1>
<div class="MiniForm">
<form id="ShortForm">
<h3>VyplÅte nezÃ¡vaznou vÃ­cekrokovou Å¾Ã¡dost za 3 minuty!</h3>
<div class="message ok">
<img alt="loader" height="140" src="/cdn/landings/img/ubuntu_fullloan/loader.gif"/>
</div>
<div class="message error">
<div class="message_text">
<div>VaÅ¡e zprÃ¡va nebyla odeslÃ¡na.<br/> ProsÃ­m zkuste to pozdÄji nebo se s nÃ¡mi spojte pomocÃ­ emailu: info@volsor.com</div>
<div class="back_button">ZpÄt na Å¾Ã¡dost</div>
</div>
</div>
<div class="form_line">
<div class="select">
<select id="amount" name="requested_amount">
<option value="10000">
                        10000 KÄ
                </option>
<option value="20000">
                        20000 KÄ
                </option>
<option value="30000">
                        30000 KÄ
                </option>
<option value="40000">
                        40000 KÄ
                </option>
<option selected="selected" value="50000">
                        50000 KÄ
                </option>
<option value="60000">
                        60000 KÄ
                </option>
<option value="80000">
                        80000 KÄ
                </option>
<option value="100000">
                        100000 KÄ
                </option>
<option value="120000">
                        120000 KÄ
                </option>
<option value="150000">
                        150000 KÄ
                </option>
<option value="180000">
                        180000 KÄ
                </option>
<option value="200000">
                        200000 KÄ
                </option>
<option value="300000">
                        300000 KÄ
                </option>
</select>
</div>
</div>
<div class="form_line">
<div class="input"><input class="required" data-msg-email="NesprÃ¡vnÃ½ formÃ¡t" data-msg-required="Toto pole je povinnÃ©" id="email" name="email" placeholder="JakÃ½ je vÃ¡Å¡ email?" type="email"/></div>
</div>
<div class="form_line">
<div class="input"><input class="required customphone" data-msg-customphone="NesprÃ¡vnÃ½ formÃ¡t" data-msg-maxlength="NesprÃ¡vnÃ½ formÃ¡t" data-msg-required="Toto pole je povinnÃ©" id="cell_phone" name="cell_phone" placeholder="JakÃ½ je vÃ¡Å¡ telefon?" type="tel"/></div>
</div>
<div class="form_line agreement terms_agree">
<div class="checkbox">
<input class="required" data-msg-required="MusÃ­te souhlasit s podmÃ­nkami" id="terms-gdpr" name="agree" type="checkbox"/>
<label class="no-check" for="terms-gdpr"><i></i>ProhlaÅ¡uji, Å¾e jsem se seznÃ¡mil s <span class="modal" id="terms-gdpr_label">informacemi tÃ½kajÃ­cÃ­mi se zpracovÃ¡nÃ­ osobnÃ­ch ÃºdajÅ¯, a Å¾e jsem byl pouÄen o mÃ½ch prÃ¡vech a povinnostech.</span></label>
</div>
</div>
<div class="form_line agreement remarketing_agree">
<div class="checkbox">
<input data-msg-required="MusÃ­te souhlasit s podmÃ­nkami" id="remarketing-gdpr" name="remarketing_agree" type="checkbox"/>
<label class="no-check" for="remarketing-gdpr"><i></i>SouhlasÃ­m se <span class="modal" id="remarketing-gdpr_label">zpracovÃ¡nÃ­m osobnÃ­ch ÃºdajÅ¯ formou zasÃ­lÃ¡nÃ­ obchodnÃ­ch sdÄlenÃ­.</span></label>
</div>
</div>
<div class="form_line submit">
<input type="submit" value="PenÃ­ze hned"/>
</div>
<div class="form_line">
<span class="secure"></span>
</div>
<input class="honey" name="email_repeat" size="1" type="email" value=""/>
</form>
</div>
<div class="section" id="condition">
<div class="container">
<h2></h2>
<div class="condition_text">
<ol>
<li>VyplÅte jednoduchÃ½<br/>a bezpeÄnÃ½ formulÃ¡Å</li>
<li>SchvÃ¡lenÃ­ pÅ¯jÄky<br/>bÄhem 5 minut</li>
<li>PenÃ­ze na vaÅ¡em ÃºÄtÄ</li>
</ol>
</div>
</div>
</div>
</div>
</div>
<div class="section" id="guide">
<div class="container">
<h2>ProÄ <span>vetsipujcka.cz</span>?</h2>
<div class="guide_text"><p><span>vetsipujcka.cz</span> je napojenÃ¡ na vyhledÃ¡vaÄ pÅ¯jÄek, kterÃ½ Å¡etÅÃ­ vÃ¡Å¡ drahocennÃ½ Äas. SluÅ¾ba je zdarma a je to nejsnaÅ¾Å¡Ã­ moÅ¾nost, jak poÅ¾Ã¡dat o pÅ¯jÄku online.</p>
</div>
</div>
</div>
<div class="section" id="advantages">
<div class="container">
<div class="block advantage one">
<div class="advantage_icon"><img alt="advantage" src="/cdn/landings/img/ubuntu_fullloan/icon_advantage_1.png"/></div>
<div class="advantage_title">PÅ¯jÄka pro kaÅ¾dÃ©ho</div>
<div class="advantage_text">VÃ¡Å¡ sociÃ¡lnÃ­ status nehraje roli â naÅ¡e pÅ¯jÄky pomÃ¡hajÃ­ krÃ½t vÃ½daje zamÄstnancÅ¯m, podnikatelÅ¯m i matkÃ¡m na mateÅskÃ©.</div>
</div>
<div class="block advantage two">
<div class="advantage_icon"><img alt="advantage" src="/cdn/landings/img/ubuntu_fullloan/icon_advantage_2.png"/></div>
<div class="advantage_title">VÅ¡e pÅes internet</div>
<div class="advantage_text">UÅ¾ nebudete muset trÃ¡vit hodiny a hodiny Äasu na cestÃ¡ch, abyste osobnÄ navÅ¡tÃ­vili pÅ¯ltucet ÃºvÄrovÃ½ch spoleÄnostÃ­ (a tam vÃ¡m Åekli, Å¾e vÃ¡m pÅ¯jÄku stejnÄ neposkytnou). MÃ­sto toho radÄji vyplÅte nÃ¡Å¡ krÃ¡tkÃ½ formulÃ¡Å!</div>
</div>
<div class="block advantage one">
<div class="advantage_icon"><img alt="advantage" src="/cdn/landings/img/ubuntu_fullloan/icon_advantage_3.png"/></div>
<div class="advantage_title">DiskrÃ©tnÃ­ jednÃ¡nÃ­</div>
<div class="advantage_text">VaÅ¡e anonymita bude dokonalÃ¡. PouÅ¾Ã­vÃ¡me modernÃ­ technologie a pravidelnÃ© aktualizace a mÅ¯Å¾ete si bÃ½t jisti, Å¾e vaÅ¡e osobnÃ­ informace zÅ¯stanou pouze mezi nÃ¡mi.</div>
</div>
<div class="block advantage two">
<div class="advantage_icon"><img alt="advantage" src="/cdn/landings/img/ubuntu_fullloan/icon_advantage_4.png"/></div>
<div class="advantage_title">BezpeÄnÃ½ pÅenos dat</div>
<div class="advantage_text">CelÃ½ proces odeslÃ¡nÃ­ vaÅ¡Ã­ Å¾Ã¡dosti o ÃºvÄr, jejÃ­ho vyhodnocenÃ­ a nÃ¡slednÃ©ho spÃ¡rovÃ¡nÃ­ s ÃºvÄrovou spoleÄnostÃ­ je perfektnÄ zabezpeÄenÃ½. Å½Ã¡dnÃ© Ãºniky dat, Å¾Ã¡dnÃ© zdrÅ¾ovÃ¡nÃ­ a Å¾Ã¡dnÃ­ nenechavci zachycujÃ­cÃ­ informace o vÃ¡s nebo o objemu vaÅ¡Ã­ pÅ¯jÄky.</div>
</div>
<div class="block advantage one">
<div class="advantage_icon"><img alt="advantage" src="/cdn/landings/img/ubuntu_fullloan/icon_advantage_5.png"/></div>
<div class="advantage_title">Pouze ovÄÅenÃ­ partneÅi</div>
<div class="advantage_text">ProvÄÅili jsme spoleÄnosti za vÃ¡s â naÅ¡i ÃºvÄrovÃ­ partneÅi patÅÃ­ k ÄeskÃ© Å¡piÄce. VÅ¾dy doruÄÃ­ penÃ­ze okamÅ¾itÄ a bez skrytÃ½ch poplatkÅ¯ Äi jinÃ½ch Å¡pinavÃ½ch trikÅ¯.</div>
</div>
<div class="block advantage two">
<div class="advantage_icon"><img alt="advantage" src="/cdn/landings/img/ubuntu_fullloan/icon_advantage_6.png"/></div>
<div class="advantage_title">SluÅ¾ba je zdarma!</div>
<div class="advantage_text">NaÅ¡e zprostÅedkovÃ¡nÃ­ tÃ© nejvÃ½hodnÄjÅ¡Ã­ pÅ¯jÄky vÃ¡s nebude stÃ¡t ani halÃ©Å. VyplÅte online formulÃ¡Å, naleznÄte svou optimÃ¡lnÃ­ ÃºvÄrovou spoleÄnost a pouÅ¾ijte zÃ­skanÃ© penÃ­ze na cokoli!</div>
</div>
</div>
</div>
<div class="section" id="reviews">
<div class="container">
<h2>Ohlasy</h2>
<div class="carousel">
<ul>
<li>
<div class="block review">
<div class="review_img"><img alt="review" src="/cdn/landings/img/ubuntu_fullloan/review_img5.jpg"/></div>
<div class="review_text"> âKdyÅ¾ potÅebuji rychle penÃ­ze, tak se pravidelnÄ obracÃ­m na vaÅ¡i sluÅ¾bu vyhledÃ¡vaÄe pÅ¯jÄek. VÅ¾dy mi nabÃ­dnete tu nejlepÅ¡Ã­ moÅ¾nost. DÃ­ky!â </div>
<div class="review_name"><span>Ivana MrÃ¡zkovÃ¡</span> - spokojenÃ¡ zÃ¡kaznice.</div>
</div>
</li>
<li>
<div class="block review">
<div class="review_img"><img alt="review" src="/cdn/landings/img/ubuntu_fullloan/review_img3.jpg"/></div>
<div class="review_text"> âVyhledÃ¡vaÄ <span>vetsipujcka.cz</span> mi Å¡etÅÃ­ Äas, uÅ¾ nemusÃ­m vyplÅovat nÄkolik Å¾Ã¡dostÃ­ u rÅ¯znÃ½ch spoleÄnostÃ­. PenÃ­ze jsem obrÅ¾el hned na ÃºÄet.â </div>
<div class="review_name"><span>Milan ÄernÃ½</span> - spokojenÃ½ zÃ¡kaznÃ­k.</div>
</div>
</li>
<li>
<div class="block review">
<div class="review_img"><img alt="review" src="/cdn/landings/img/ubuntu_fullloan/review_img2.jpg"/></div>
<div class="review_text"> âPotÅebovala jsem okamÅ¾itÄ penÃ­ze na zÃ¡meÄnÃ­ka. Moc jste mi pomohli a penÃ­ze jsem dostala.â </div>
<div class="review_name"><span>AdÃ©la StiborovÃ¡</span> - spokojenÃ¡ zÃ¡kaznice.</div>
</div>
</li>
<li>
<div class="block review">
<div class="review_img"><img alt="review" src="/cdn/landings/img/ubuntu_fullloan/review_img4.jpg"/></div>
<div class="review_text"> âVyplnil jsem Å¾Ã¡dost a byla mi nabÃ­dnuta spoleÄnost, kterÃ¡ mi ten samÃ½ den poslala penÃ­ze na ÃºÄet. VÅ¡e probÄhlo v poÅÃ¡dku!â </div>
<div class="review_name"><span>Ing. LukÃ¡Å¡ VÃ¡penÃ­k</span> - spokojenÃ½ zÃ¡kaznÃ­k.</div>
</div>
</li>
</ul>
</div>
</div>
</div>
<div class="section" id="contact_text">
<div class="container">
<div class="block info">
<div class="contact_info">Tyto internetovÃ© strÃ¡nky jsou napojeny na automatizovanÃ½ systÃ©m vyhledÃ¡vÃ¡nÃ­ finanÄnÃ­ch sluÅ¾eb, napÅ. (zÃ¡)pÅ¯jÄek Äi ÃºvÄrÅ¯, nebo jinÃ½ch sluÅ¾eb nabÃ­zenÃ½ch provozovatelem tÄchto strÃ¡nek (dÃ¡le tÃ©Å¾ jen jako <strong>"SystÃ©m"</strong>). SystÃ©m pouze zpracovÃ¡vÃ¡ Å¾Ã¡dosti zÃ¡jemcÅ¯ o poskytnutÃ­ sluÅ¾eb nabÃ­zenÃ½ch tÅetÃ­mi osobami.<br/><br/>SystÃ©m samostatnÄ neposkytuje Å¾Ã¡dnÃ© finanÄnÃ­ Äi jinÃ© sluÅ¾by, pouze spojuje zÃ¡jemce o poskytnutÃ­ nabÃ­zenÃ½ch sluÅ¾eb s jejich koncovÃ½mi poskytovateli.<br/><br/>AutoÅi SystÃ©mu nenesou odpovÄdnost za zpÅ¯sob propagace tÄchto internetovÃ½ch strÃ¡nek v pÅÃ­padech, kdy tyto internetovÃ© strÃ¡nky jsou propagovÃ¡ny ze strany affiliate partnerÅ¯.<br/><br/>AutoÅi SystÃ©mu nejsou odpovÄdni za Ãºjmu, kterÃ¡ by uÅ¾ivatelÅ¯m tÄchto strÃ¡nek mohla vzniknout v souvislosti s prÃ¡vnÃ­m jednÃ¡nÃ­m, kterÃ© tito svobodnÄ uÄinili, a to na zÃ¡kladÄ informacÃ­, kterÃ© jsou obsahem tÄchto internetovÃ½ch strÃ¡nek.</div>
</div>
</div>
</div>
<div class="section mfp-hide" id="contacts">
<h2>Kontakt</h2>
<div class="block feedback">
<form class="feedback_form" id="FeedbackForm">
<div class="message ok">
<div class="message_text">
            VÃ¡Å¡ vzkaz byl ÃºspÄÅ¡nÄ odeslÃ¡n
        </div>
</div>
<div class="message error">
<div class="message_text">
<div>VaÅ¡e zprÃ¡va nebyla odeslÃ¡na.<br/> ProsÃ­m zkuste to pozdÄji nebo se s nÃ¡mi spojte pomocÃ­ emailu: info@volsor.com</div>
<div class="back_button">ZpÄt na Å¾Ã¡dost</div>
</div>
</div>
<div class="form_line">
<div class="w50">
<label for="feedback_name">JakÃ© je vaÅ¡e jmÃ©no?</label>
<div class="input"><input class="required" data-msg-required="Toto pole je povinnÃ©" id="feedback_name" name="name" placeholder="NapiÅ¡te VaÅ¡e jmÃ©no" type="text"/></div>
</div>
<div class="w50">
<label for="feedback_last_name">JakÃ© je vaÅ¡e pÅÃ­jmenÃ­?</label>
<div class="input"><input class="required" data-msg-required="Toto pole je povinnÃ©" id="feedback_last_name" name="last_name" placeholder="NapiÅ¡te VaÅ¡e pÅÃ­jmenÃ­" type="text"/></div>
</div>
</div>
<div class="form_line">
<div class="w50">
<label for="feedback_email">JakÃ½ je vÃ¡Å¡ email?</label>
<div class="input"><input class="required" data-msg-email="NesprÃ¡vnÃ½ formÃ¡t" data-msg-required="Toto pole je povinnÃ©" id="feedback_email" name="owner_email" placeholder="NapiÅ¡te VÃ¡Å¡ email" type="email"/>
</div>
</div>
<div class="w50">
<label for="feedback_phone">JakÃ½ je vÃ¡Å¡ telefon?</label>
<div class="input"><input class="required customphone" data-msg-customphone="NesprÃ¡vnÃ½ formÃ¡t" data-msg-maxlength="NesprÃ¡vnÃ½ formÃ¡t" data-msg-required="Toto pole je povinnÃ©" id="feedback_phone" maxlength="17" name="owner_cell_phone" placeholder="NapiÅ¡te VÃ¡Å¡ telefon" type="tel"/></div>
</div>
</div>
<div class="form_line">
<div class="textarea"><textarea class="required" data-msg-required="Toto pole je povinnÃ©" id="textFeedback" name="message" placeholder="NapiÅ¡te VaÅ¡Ã­ zprÃ¡vu" rows="5"></textarea></div>
</div>
<div class="form_line submit">
<input type="submit" value="Odeslat"/>
</div>
<input class="honey" name="email_repeat" size="1" type="email" value=""/>
</form>
</div>
</div>
</div>
</div>
<div id="footer">
<div class="container">
<div class="menu_footer">
<a href="https://www.volsor.com/" target="_blank">Affiliate rychlÃ¡ pÅ¯jÄka</a>
<span class="modal" id="contact">Kontakt</span>
<a href="https://odhlasit.creditsec.cz" target="_blank">OdhlÃ¡sit se</a>
</div>
<div class="copyright">Copyright Â© 2021. <span>vetsipujcka.cz</span>. VÅ¡echna prÃ¡va vyhrazena.</div>
</div>
<div class="container">
<div class="company_info">
<div>VOLSOR s.r.o.<br/>IÄO: 013 85 011;<br/>DIÄ: CZ01385011</div>
<div>sÃ­dlem Praha, Praha 1,<br/>StarÃ© MÄsto, Na PÅÃ­kopÄ 392/9,<br/>PSÄ 110 00</div>
<div>zapsanÃ¡ v obchodnÃ­m rejstÅÃ­ku,<br/>vedenÃ©m MÄstskÃ½m soudem v Praze,<br/>oddÃ­l C, vloÅ¾ka 201387</div>
</div>
</div>
</div>
<div>
<div class="conditions_popup pop_up" id="terms_popup">
<span class="button b-close"><span>X</span></span>
<div class="terms_popup_container"></div>
<div class="agree b-close"><span>SouhlasÃ­m se zpracovÃ¡nÃ­m osobnÃ­ch ÃºdajÅ¯ a s podmÃ­nkami</span></div>
</div>
<div class="conditions_popup pop_up" id="conditions_popup">
<span class="button b-close"><span>X</span></span>
<div class="conditions_popup_container"></div>
<div class="agree b-close"><span>SouhlasÃ­m se zpracovÃ¡nÃ­m osobnÃ­ch ÃºdajÅ¯ a s podmÃ­nkami</span></div>
</div>
<link href="/cdn/landings/css/cs/ubuntu_fullloan.v2.24.min.css" rel="stylesheet"/>
<script src="/cdn/landings/js/app.v2.24.min.js"></script>
<link href="//fonts.googleapis.com/css?family=Ubuntu|Open+Sans:400,700&amp;subset=cyrillic-ext,latin-ext" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 972106283;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
<script src="//www.googleadservices.com/pagead/conversion.js" type="text/javascript">
</script>
<noscript>
<div style="display:inline;">
<img alt="" height="1" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/972106283/?value=0&amp;guid=ON&amp;script=0" style="border-style:none;" width="1"/>
</div>
</noscript>
<script type="text/javascript">
            /* <![CDATA[ */
            var seznam_retargeting_id = 25471;
            /* ]]> */
        </script>
<script src="//c.imedia.cz/js/retargeting.js" type="text/javascript">
</script>
</div>
<div class="cookies-policy">
<div class="cookies-policy-text">
<p>Tyto webovÃ© strÃ¡nky pouÅ¾Ã­vajÃ­ k poskytovÃ¡nÃ­ sluÅ¾eb, personalizaci reklam a analÃ½ze nÃ¡vÅ¡tÄvnosti soubory cookies.<br/> PouÅ¾Ã­vÃ¡nÃ­m tÄchto strÃ¡nek souhlasÃ­te s jejich pouÅ¾itÃ­m.</p>
</div>
<div class="cookies-policy-check">
<a href="" id="cookies-policy-agree"><i>â</i><span>RozumÃ­m</span></a>
</div>
</div>
</body>
</html>
