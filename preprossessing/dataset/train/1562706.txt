<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>File Not Found</title>
<style type="text/css">
<!--
.Stil1 {
	font-family: Arial;
	font-size: 15px;
	color: #F47820;
	font-weight: bold;
}
.Stil2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
}
.Stil4 {
	font-family: Arial;
	color: #C6C7C1;
	font-size: 11px;
	text-decoration:none;
	font-weight: bold;
}
-->
A:hover { color: #F47820; font-weight:bold; font-size:11px;}


</style>
</head>
<body>
<table align="center" border="0" width="600">
<tr>
<td colspan="5" height="20"> </td>
</tr>
<tr>
<td width="200"> </td>
<td colspan="3"><img height="31" src="/error/404.gif" width="494"/></td>
<td> </td>
</tr>
<tr>
<td width="200"><img height="10" src="/error/line.gif" width="200"/></td>
<td width="170"><img height="141" src="/error/404_logo.jpg" width="170"/></td>
<td width="0"> </td>
<td width="0"><table border="0" cellspacing="0" width="600">
<tr>
<td colspan="3" height="0"> </td>
</tr>
<tr>
<td colspan="3" height="0"><span class="Stil1">| Die angeforderte Seite wurde nicht gefunden | </span></td>
</tr>
<tr>
<td height="0" valign="middle" width="0"> </td>
<td height="0" valign="middle" width="0"> </td>
<td height="0" width="0"> </td>
</tr>
<tr>
<td height="0" valign="middle" width="0"><img height="7" src="/error/punkt.gif" width="8"/></td>
<td height="0" valign="middle" width="0"><div align="left"><span class="Stil2">Prüfen Sie bitte die eingegebene Adresse.</span></div></td>
<td height="0" width="0"> </td>
</tr>
<tr>
<td height="0" valign="middle" width="0"><img height="7" src="/error/punkt.gif" width="8"/></td>
<td class="Stil2" height="0" valign="middle" width="0"><div align="left">Korrigieren Sie bitte eventuell fehlerhafte Verlinkungen.</div></td>
<td height="0" width="0"> </td>
</tr>
<tr>
<td height="0" valign="middle" width="0"><img height="7" src="/error/punkt.gif" width="8"/></td>
<td class="Stil2" height="0" valign="middle" width="0">Die Datei, auf die die URL verweist, ist verschoben, umbenannt oder gelöscht worden.</td>
<td height="0" width="0"> </td>
</tr>
<tr>
<td height="0" valign="middle" width="0"> </td>
<td height="0" valign="top" width="0"> </td>
<td height="0" width="0"> </td>
</tr>
</table></td>
<td width="0"> </td>
</tr>
<tr>
<td width="200"> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr>
<td width="200"> </td>
<td colspan="3"><a class="Stil4" href="http://www.world4you.com/error404">Eine Kundenwebsite im hochverfügbaren World4You Hostingnetzwerk.</a></td>
<td> </td>
</tr>
</table>
<p align="center"> </p>
<p> </p>
<p> 	</p>
</body>
</html>
