<!DOCTYPE html>
<html lang="de-DE"><head>
<meta charset="utf-8"/>
<link crossorigin="anonymous" href="https://u.jimcdn.com/" rel="dns-prefetch preconnect"/>
<link crossorigin="anonymous" href="https://assets.jimstatic.com/" rel="dns-prefetch preconnect"/>
<link crossorigin="anonymous" href="https://image.jimcdn.com" rel="dns-prefetch preconnect"/>
<link crossorigin="anonymous" href="https://fonts.jimstatic.com" rel="dns-prefetch preconnect"/>
<link crossorigin="anonymous" href="https://www.google-analytics.com" rel="dns-prefetch preconnect"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="" name="description"/>
<meta content="index, follow, archive" name="robots"/>
<meta content="" property="st:section"/>
<meta content="Ihr Tisch ...mein Selbstgenähtes" name="twitter:title"/>
<meta content="Herzlich Willkommen bei Tisch und Decke Aufgrund des Coronavirus werden viele Märkte abgesagt...ich wünsche uns allen Gesundheit und das wir das auch finanziell überleben... Aufgrund der nochmals gestiegenen Stoffpreise und der Standplätze erhöhen sich die Preise ab 01.10.2020 um ca. 1 Euro pro Teil😳 Endlich wieder Afrika -Stoffe" name="twitter:description"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="https://www.tisch-und-decke.de/" property="og:url"/>
<meta content="Ihr Tisch ...mein Selbstgenähtes" property="og:title"/>
<meta content="Herzlich Willkommen bei Tisch und Decke Aufgrund des Coronavirus werden viele Märkte abgesagt...ich wünsche uns allen Gesundheit und das wir das auch finanziell überleben... Aufgrund der nochmals gestiegenen Stoffpreise und der Standplätze erhöhen sich die Preise ab 01.10.2020 um ca. 1 Euro pro Teil😳 Endlich wieder Afrika -Stoffe" property="og:description"/>
<meta content="website" property="og:type"/>
<meta content="de_DE" property="og:locale"/>
<meta content="tisch-und-deckes Webseite!" property="og:site_name"/>
<meta content="https://image.jimcdn.com/app/cms/image/transf/dimension=4096x4096:format=jpg/path/s6bc6a53c4f56ddac/image/iaf4db737708e122f/version/1583847116/image.jpg" name="twitter:image"/>
<meta content="https://image.jimcdn.com/app/cms/image/transf/dimension=4096x4096:format=jpg/path/s6bc6a53c4f56ddac/image/iaf4db737708e122f/version/1583847116/image.jpg" property="og:image"/>
<meta content="3264" property="og:image:width"/>
<meta content="1836" property="og:image:height"/>
<meta content="https://image.jimcdn.com/app/cms/image/transf/dimension=4096x4096:format=jpg/path/s6bc6a53c4f56ddac/image/iaf4db737708e122f/version/1583847116/image.jpg" property="og:image:secure_url"/><title>Seite nicht gefunden - tisch-und-deckes Webseite!</title>
<link href="data:image/png;base64,iVBORw0KGgo=" rel="icon" type="image/png"/>
<link href="https://www.tisch-und-decke.de/not-found/" rel="canonical"/>
<script src="https://assets.jimstatic.com/ckies.js.2f2ba40710ea5042aa2f.js"></script>
<script src="https://assets.jimstatic.com/cookieControl.js.8402eb3cce1955d899d4.js"></script>
<script>window.CookieControlSet.setToNormal();</script>
<style>html,body{margin:0}.hidden{display:none}.n{padding:5px}#cc-website-title a {text-decoration: none}.cc-m-image-align-1{text-align:left}.cc-m-image-align-2{text-align:right}.cc-m-image-align-3{text-align:center}</style>
<link href="https://u.jimcdn.com/cms/o/s6bc6a53c4f56ddac/layout/dm_3404004461cb68b870b2272358c9c583/css/layout.css?t=1526995254" id="jimdo_layout_css" rel="stylesheet" type="text/css"/>
<script>     /* <![CDATA[ */     /*!  loadCss [c]2014 @scottjehl, Filament Group, Inc.  Licensed MIT */     window.loadCSS = window.loadCss = function(e,n,t){var r,l=window.document,a=l.createElement("link");if(n)r=n;else{var i=(l.body||l.getElementsByTagName("head")[0]).childNodes;r=i[i.length-1]}var o=l.styleSheets;a.rel="stylesheet",a.href=e,a.media="only x",r.parentNode.insertBefore(a,n?r:r.nextSibling);var d=function(e){for(var n=a.href,t=o.length;t--;)if(o[t].href===n)return e.call(a);setTimeout(function(){d(e)})};return a.onloadcssdefined=d,d(function(){a.media=t||"all"}),a};     window.onloadCSS = function(n,o){n.onload=function(){n.onload=null,o&&o.call(n)},"isApplicationInstalled"in navigator&&"onloadcssdefined"in n&&n.onloadcssdefined(o)}     /* ]]> */ </script> <script>
// <![CDATA[
onloadCSS(loadCss('https://assets.jimstatic.com/web.css.ec74bcad89a485bd6a5639535a46c0ba.css') , function() {
    this.id = 'jimdo_web_css';
});
// ]]>
</script>
<link as="style" href="https://assets.jimstatic.com/web.css.ec74bcad89a485bd6a5639535a46c0ba.css" rel="preload"/>
<noscript>
<link href="https://assets.jimstatic.com/web.css.ec74bcad89a485bd6a5639535a46c0ba.css" rel="stylesheet"/>
</noscript>
<script>
    //<![CDATA[
        var jimdoData = {"isTestserver":false,"isLcJimdoCom":false,"isJimdoHelpCenter":false,"isProtectedPage":false,"cstok":"","cacheJsKey":"7478ced5ba329aed2360a7b6d75bc916c3c029be","cacheCssKey":"7478ced5ba329aed2360a7b6d75bc916c3c029be","cdnUrl":"https:\/\/assets.jimstatic.com\/","minUrl":"https:\/\/assets.jimstatic.com\/app\/cdn\/min\/file\/","authUrl":"https:\/\/a.jimdo.com\/","webPath":"https:\/\/www.tisch-und-decke.de\/","appUrl":"https:\/\/a.jimdo.com\/","cmsLanguage":"de_DE","isFreePackage":false,"mobile":false,"isDevkitTemplateUsed":true,"isTemplateResponsive":true,"websiteId":"s6bc6a53c4f56ddac","pageId":1,"packageId":2,"shop":{"deliveryTimeTexts":{"1":"1 - 3 Tage Lieferzeit","2":"3 - 5 Tage Lieferzeit","3":"5 - 8 Tage Lieferzeit"},"checkoutButtonText":"Zur Kasse","isReady":false,"currencyFormat":{"pattern":"#,##0.00 \u00a4","convertedPattern":"#,##0.00 $","symbols":{"GROUPING_SEPARATOR":".","DECIMAL_SEPARATOR":",","CURRENCY_SYMBOL":"\u20ac"}},"currencyLocale":"de_DE"},"tr":{"gmap":{"searchNotFound":"Die angegebene Adresse konnte nicht gefunden werden.","routeNotFound":"Die Anfahrtsroute konnte nicht berechnet werden. M\u00f6gliche Gr\u00fcnde: Die Startadresse ist zu ungenau oder zu weit von der Zieladresse entfernt."},"shop":{"checkoutSubmit":{"next":"N\u00e4chster Schritt","wait":"Bitte warten"},"paypalError":"Da ist leider etwas schiefgelaufen. Bitte versuche es erneut!","cartBar":"Zum Warenkorb","maintenance":"Dieser Shop ist vor\u00fcbergehend leider nicht erreichbar. Bitte probieren Sie es sp\u00e4ter noch einmal.","addToCartOverlay":{"productInsertedText":"Der Artikel wurde dem Warenkorb hinzugef\u00fcgt.","continueShoppingText":"Weiter einkaufen","reloadPageText":"neu laden"},"notReadyText":"Dieser Shop ist noch nicht vollst\u00e4ndig eingerichtet.","numLeftText":"Mehr als {:num} Exemplare dieses Artikels sind z.Z. leider nicht verf\u00fcgbar.","oneLeftText":"Es ist leider nur noch ein Exemplar dieses Artikels verf\u00fcgbar."},"common":{"timeout":"Es ist ein Fehler aufgetreten. Die von dir ausgew\u00e4hlte Aktion wurde abgebrochen. Bitte versuche es in ein paar Minuten erneut."},"form":{"badRequest":"Es ist ein Fehler aufgetreten: Die Eingaben konnten leider nicht \u00fcbermittelt werden. Bitte versuche es sp\u00e4ter noch einmal!"}},"jQuery":"jimdoGen002","isJimdoMobileApp":false,"bgConfig":{"id":132313778,"type":"picture","options":{"fixed":true},"images":[{"id":8268972978,"url":"https:\/\/image.jimcdn.com\/app\/cms\/image\/transf\/none\/path\/s6bc6a53c4f56ddac\/backgroundarea\/id7251799d31738f1\/version\/1502893429\/image.jpg","focalPointX":68.2539876302000010355186532251536846160888671875,"focalPointY":60.8809552873999990652009728364646434783935546875}]},"bgFullscreen":null,"responsiveBreakpointLandscape":767,"responsiveBreakpointPortrait":480,"copyableHeadlineLinks":false,"tocGeneration":false,"googlemapsConsoleKey":false,"loggingForAnalytics":false,"loggingForPredefinedPages":false,"isFacebookPixelIdEnabled":false,"userAccountId":"1914b213-ba57-4a4d-8e3f-39bee557a296"};
    // ]]>
</script>
<script> (function(window) { 'use strict'; var regBuff = window.__regModuleBuffer = []; var regModuleBuffer = function() { var args = [].slice.call(arguments); regBuff.push(args); }; if (!window.regModule) { window.regModule = regModuleBuffer; } })(window); </script>
<script async="true" src="https://assets.jimstatic.com/web.js.d18a1736bab11baa99eb.js"></script>
</head>
<body class="body cc-page j-m-gallery-styles j-m-video-styles j-m-hr-styles j-m-header-styles j-m-text-styles j-m-emotionheader-styles j-m-htmlCode-styles j-m-rss-styles j-m-form-styles-disabled j-m-table-styles j-m-textWithImage-styles j-m-downloadDocument-styles j-m-imageSubtitle-styles j-m-flickr-styles j-m-googlemaps-styles j-m-blogSelection-styles-disabled j-m-comment-styles-disabled j-m-jimdo-styles j-m-profile-styles j-m-guestbook-styles j-m-promotion-styles j-m-twitter-styles j-m-hgrid-styles j-m-shoppingcart-styles j-m-catalog-styles j-m-product-styles-disabled j-m-facebook-styles j-m-sharebuttons-styles j-m-formnew-styles-disabled j-m-callToAction-styles j-m-turbo-styles j-m-spacing-styles j-m-googleplus-styles j-m-dummy-styles j-m-search-styles j-m-booking-styles j-footer-styles cc-pagemode-overlay cc-content-parent" id="page-2547400778">
<div class="cc-content-parent" id="cc-inner">
<input class="jtpl-navigation__checkbox" id="jtpl-navigation__checkbox" type="checkbox"/><!-- _main.sass --><div class="jtpl-main cc-content-parent">
<div background-area="" class="jtpl-background-area"></div>
<!-- _cart.sass -->
<div class="jtpl-cart">
</div>
<!-- END _cart.sass -->
<!-- _header.sass -->
<header class="jtpl-header navigation-colors"><div class="jtpl-topbar-section navigation-vertical-alignment">
<div class="jtpl-logo">
<div class="cc-single-module-element" id="cc-website-logo"><div class="j-module n j-imageSubtitle" id="cc-m-13413862878"><div class="cc-m-image-container"><figure class="cc-imagewrapper cc-m-image-align-3">
<a href="https://www.tisch-und-decke.de/" target="_self"><img alt="" class="" data-image-id="8266042178" data-src="https://image.jimcdn.com/app/cms/image/transf/dimension=200x10000:format=jpg/path/s6bc6a53c4f56ddac/image/i6ef0ab05b1162359/version/1504613331/image.jpg" data-src-height="162" data-src-width="490" id="cc-m-imagesubtitle-image-13413862878" sizes="(min-width: 200px) 200px, 100vw" src="https://image.jimcdn.com/app/cms/image/transf/dimension=200x10000:format=jpg/path/s6bc6a53c4f56ddac/image/i6ef0ab05b1162359/version/1504613331/image.jpg" srcset="https://image.jimcdn.com/app/cms/image/transf/dimension=200x10000:format=jpg/path/s6bc6a53c4f56ddac/image/i6ef0ab05b1162359/version/1504613331/image.jpg 200w, https://image.jimcdn.com/app/cms/image/transf/dimension=320x10000:format=jpg/path/s6bc6a53c4f56ddac/image/i6ef0ab05b1162359/version/1504613331/image.jpg 320w, https://image.jimcdn.com/app/cms/image/transf/dimension=400x10000:format=jpg/path/s6bc6a53c4f56ddac/image/i6ef0ab05b1162359/version/1504613331/image.jpg 400w"/></a>
</figure>
</div>
<div class="cc-clear"></div>
<script id="cc-m-reg-13413862878">// <![CDATA[

    window.regModule("module_imageSubtitle", {"data":{"imageExists":true,"hyperlink":"","hyperlink_target":"_blank","hyperlinkAsString":"","pinterest":"0","id":13413862878,"widthEqualsContent":"1","resizeWidth":"200","resizeHeight":67},"id":13413862878});
// ]]>
</script></div></div>
</div>
<!-- _navigation.sass -->
<nav class="jtpl-navigation" data-dropdown="true"><div data-container="navigation"><div class="j-nav-variant-nested"><ul class="cc-nav-level-0 j-nav-level-0"><li class="jmd-nav__list-item-0" id="cc-nav-view-2547400778"><a data-link-title="Startseite" href="/">Startseite</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2547400878"><a data-link-title="Grössen" href="/grössen/">Grössen</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2547401078"><a data-link-title="Motive" href="/motive/">Motive</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2547791878"><a data-link-title="Termine" href="/termine/">Termine</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2547401278"><a data-link-title="Ich über mich" href="/ich-über-mich/">Ich über mich</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2547401378"><a data-link-title="Kontakt" href="/kontakt/">Kontakt</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2547785978"><a data-link-title="Karrierte Maiglöckchen" href="/karrierte-maiglöckchen/">Karrierte Maiglöckchen</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2626803278"><a data-link-title="Impressum" href="/impressum/">Impressum</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2626803578"><a data-link-title="Widerrufsrecht" href="/widerrufsrecht/">Widerrufsrecht</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2628108678"><a data-link-title="DSGVO" href="/dsgvo/">DSGVO</a></li></ul></div></div>
</nav><!-- END _navigation.sass --><!-- _mobile-navigation.sass --><label class="jtpl-navigation__label navigation-colors__menu-icon" for="jtpl-navigation__checkbox">
<span class="jtpl-navigation__borders navigation-colors__menu-icon"></span>
</label>
<div class="jtpl-mobile-navigation navigation-colors">
<div data-container="navigation"><div class="j-nav-variant-nested"><ul class="cc-nav-level-0 j-nav-level-0"><li class="jmd-nav__list-item-0" id="cc-nav-view-2547400778"><a data-link-title="Startseite" href="/">Startseite</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2547400878"><a data-link-title="Grössen" href="/grössen/">Grössen</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2547401078"><a data-link-title="Motive" href="/motive/">Motive</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2547791878"><a data-link-title="Termine" href="/termine/">Termine</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2547401278"><a data-link-title="Ich über mich" href="/ich-über-mich/">Ich über mich</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2547401378"><a data-link-title="Kontakt" href="/kontakt/">Kontakt</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2547785978"><a data-link-title="Karrierte Maiglöckchen" href="/karrierte-maiglöckchen/">Karrierte Maiglöckchen</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2626803278"><a data-link-title="Impressum" href="/impressum/">Impressum</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2626803578"><a data-link-title="Widerrufsrecht" href="/widerrufsrecht/">Widerrufsrecht</a></li><li class="jmd-nav__list-item-0" id="cc-nav-view-2628108678"><a data-link-title="DSGVO" href="/dsgvo/">DSGVO</a></li></ul></div></div>
</div>
<!-- END _mobile-navigation.sass -->
</div>
</header><!-- END _header.sass --><!-- _section-main.sass --><div class="jtpl-section-main cc-content-parent">
<div class="jtpl-content content-options cc-content-parent">
<!-- breadcrumb.sass -->
<div class="jtpl-breadcrumb breadcrumb-options">
<div data-container="navigation"><div class="j-nav-variant-breadcrumb"><ol></ol></div></div>
</div>
<!-- END breadcrumb.sass -->
<div data-container="content" id="content_area"><meta content="404" name="jimdo-status-code"/><h1>Die Seite wurde nicht gefunden</h1><br/><p>Die gewünschte Seite existiert leider nicht (mehr), oder ein Tippfehler verhindert den Aufruf. <a href="https://www.tisch-und-decke.de/">Hier geht es zur Startseite</a></p><br/><br/>
<div class="j-static-page"><h1 class="n">Sitemap</h1>
<ul class="sitemap"><li><a href="/">Startseite</a></li>
<li><a href="/grössen/">Grössen</a></li>
<li><a href="/motive/">Motive</a></li>
<li><a href="/termine/">Termine</a></li>
<li><a href="/ich-über-mich/">Ich über mich</a></li>
<li><a href="/kontakt/">Kontakt</a></li>
<li><a href="/karrierte-maiglöckchen/">Karrierte Maiglöckchen</a></li>
<li><a href="/impressum/">Impressum</a></li>
<li><a href="/widerrufsrecht/">Widerrufsrecht</a></li>
<li><a href="/dsgvo/">DSGVO</a></li>
</ul></div></div>
</div>
</div>
<!-- END _section-main.sass -->
<!-- _section-aside.sass -->
<aside class="jtpl-section-aside sidebar-options"><div class="jtpl-section-aside__inner">
<div data-container="sidebar"><div id="cc-matrix-3829202278"><div class="j-module n j-spacing " id="cc-m-13411533578">
<div class="cc-m-spacer" style="height: 24px;">
</div>
</div><div class="j-module n j-hgrid " id="cc-m-13411533678"> <div class="cc-m-hgrid-column" style="width: 36.6%;">
<div id="cc-matrix-3829202578"><div class="j-module n j-header " id="cc-m-13411533778"><h3 class="" id="cc-m-header-13411533778">Tisch und Decke</h3></div><div class="j-module n j-hr " id="cc-m-13425998478"> <hr/>
</div><div class="j-module n j-text " id="cc-m-13411533878"><p>
<span style="font-size: 18px;">Britta Küsters</span>
</p></div><div class="j-module n j-facebook " id="cc-m-13428849678"> <div class="j-search-results">
<button class="fb-placeholder-button j-fb-placeholder-button"><span class="like-icon"></span>Mit Facebook verbinden</button>
<div class="fb-like" data-action="like" data-href="https://www.tisch-und-decke.de/" data-layout="button" data-share="false" data-show-faces="false"></div>
</div><script>// <![CDATA[

    window.regModule("module_facebook", []);
// ]]>
</script></div></div> </div>
<div class="cc-m-hgrid-separator" data-display="cms-only"><div></div></div>
<div class="cc-m-hgrid-column" style="width: 29.18%;">
<div id="cc-matrix-3829202678"><div class="j-module n j-header " id="cc-m-13411533978"><h3 class="" id="cc-m-header-13411533978">kuestershb@t-online.de</h3></div><div class="j-module n j-hr " id="cc-m-13425997678"> <hr/>
</div><div class="j-module n j-text " id="cc-m-13426008078"><p style="text-align: center;">
<span style="font-size: 18px;">Whatsapp 0157 39253442</span>
</p></div></div> </div>
<div class="cc-m-hgrid-separator" data-display="cms-only"><div></div></div>
<div class="cc-m-hgrid-column last" style="width: 30.18%;">
<div id="cc-matrix-3829202878"><div class="j-module n j-imageSubtitle " id="cc-m-13484116478"><figure class="cc-imagewrapper cc-m-image-align-1 cc-m-width-maxed">
<img alt="" class="" data-image-id="8283312378" data-src="https://image.jimcdn.com/app/cms/image/transf/dimension=292x10000:format=jpg/path/s6bc6a53c4f56ddac/image/ic83310e32949ee0d/version/1504613537/image.jpg" data-src-height="162" data-src-width="490" id="cc-m-imagesubtitle-image-13484116478" sizes="(min-width: 292px) 292px, 100vw" src="https://image.jimcdn.com/app/cms/image/transf/dimension=292x10000:format=jpg/path/s6bc6a53c4f56ddac/image/ic83310e32949ee0d/version/1504613537/image.jpg" srcset="https://image.jimcdn.com/app/cms/image/transf/dimension=292x10000:format=jpg/path/s6bc6a53c4f56ddac/image/ic83310e32949ee0d/version/1504613537/image.jpg 292w, https://image.jimcdn.com/app/cms/image/transf/dimension=320x10000:format=jpg/path/s6bc6a53c4f56ddac/image/ic83310e32949ee0d/version/1504613537/image.jpg 320w, https://image.jimcdn.com/app/cms/image/transf/none/path/s6bc6a53c4f56ddac/image/ic83310e32949ee0d/version/1504613537/image.jpg 490w"/>
</figure>
<div class="cc-clear"></div>
<script id="cc-m-reg-13484116478">// <![CDATA[

    window.regModule("module_imageSubtitle", {"data":{"imageExists":true,"hyperlink":"","hyperlink_target":"","hyperlinkAsString":"","pinterest":"0","id":13484116478,"widthEqualsContent":"1","resizeWidth":"292","resizeHeight":97},"id":13484116478});
// ]]>
</script></div></div> </div>
<div class="cc-m-hgrid-overlay" data-display="cms-only"></div>
<br class="cc-clear"/>
</div></div></div>
</div>
</aside><!-- END _section-aside.sass --><!-- _footer.sass --><footer class="jtpl-footer footer-options"><div class="jtpl-footer__inner">
<div data-container="footer" id="contentfooter">
<div class="j-meta-links">
<a href="/about/">Impressum</a> | <a href="//www.tisch-und-decke.de/j/privacy">Datenschutz</a> | <a href="javascript:window.CookieControl.showCookieSettings();" id="cookie-policy">Cookie-Richtlinie</a> | <a href="/sitemap/">Sitemap</a> </div>
<div class="j-admin-links">
<span class="loggedout">
<a href="/login" id="login" rel="nofollow">Anmelden</a>
</span>
<span class="loggedin">
<a href="https://cms.e.jimdo.com/app/cms/logout.php" id="logout" rel="nofollow" target="_top">
        Abmelden    </a>
    |
    <a href="https://a.jimdo.com/app/auth/signin/jumpcms/?page=2547400778" id="edit" rel="nofollow" target="_top">Bearbeiten</a>
</span>
</div>
</div>
</div>
</footer><!-- END _footer.sass -->
</div>
<!-- END _main.sass -->
</div>
<div class="hidden" id="loginbox">
<div id="loginbox-header">
<a class="cc-close" href="#" title="Dieses Element zuklappen">zuklappen</a>
<div class="c"></div>
</div>
<div id="loginbox-content">
<div id="resendpw"></div>
<div id="loginboxOuter"></div>
</div>
</div>
<div class="hidden" id="loginbox-darklayer"></div>
<script>// <![CDATA[

    window.regModule("web_login", {"url":"https:\/\/www.tisch-und-decke.de\/","pageId":1});
// ]]>
</script>
<div class="cc-individual-cookie-settings" data-nosnippet="true" id="cc-individual-cookie-settings" style="display: none">
</div>
<script>// <![CDATA[

    window.regModule("web_individualCookieSettings", {"categories":[{"type":"NECESSARY","name":"Unbedingt erforderlich","description":"Unbedingt erforderliche Cookies erm\u00f6glichen grundlegende Funktionen und sind f\u00fcr die einwandfreie Funktion der Website erforderlich. Daher kann man sie nicht deaktivieren. Diese Art von Cookies wird ausschlie\u00dflich von dem Betreiber der Website verwendet (First-Party-Cookie) und s\u00e4mtliche Informationen, die in den Cookies gespeichert sind, werden nur an diese Website gesendet.","required":true,"cookies":[{"key":"cookielaw","name":"cookielaw","description":"Cookielaw\n\nDieses Cookie zeigt das Cookie-Banner an und speichert die Cookie-Einstellungen des Besuchers.\n\nAnbieter:\nJimdo GmbH, Stresemannstrasse 375, 22761 Hamburg, Deutschland.\n\nCookie-Name: ckies_cookielaw\nCookie-Laufzeit: 1 Jahr\n\nCookie-Richtlinie:\nhttps:\/\/www.jimdo.com\/de\/info\/cookies\/policy\/\n\nDatenschutzerkl\u00e4rung:\nhttps:\/\/www.jimdo.com\/de\/info\/datenschutzerklaerung\/ ","required":true},{"key":"control-cookies-wildcard","name":"ckies_*","description":"Jimdo Control Cookies\n\nSteuerungs-Cookies zur Aktivierung der vom Website-Besucher ausgew\u00e4hlten Dienste\/Cookies und zur Speicherung der entsprechenden Cookie-Einstellungen. \n\nAnbieter:\nJimdo GmbH, Stresemannstra\u00dfe 375, 22761 Hamburg, Deutschland.\n\nCookie-Namen: ckies_*, ckies_postfinance, ckies_stripe, ckies_powr, ckies_google, ckies_cookielaw, ckies_ga, ckies_jimdo_analytics, ckies_fb_analytics, ckies_fr\n\nCookie-Laufzeit: 1 Jahr\n\nCookie-Richtlinie:\nhttps:\/\/www.jimdo.com\/de\/info\/cookies\/policy\/\n\nDatenschutzerkl\u00e4rung:\nhttps:\/\/www.jimdo.com\/de\/info\/datenschutzerklaerung\/ ","required":true}]},{"type":"FUNCTIONAL","name":"Funktionell","description":"Funktionelle Cookies erm\u00f6glichen dieser Website, bestimmte Funktionen zur Verf\u00fcgung zu stellen und Informationen zu speichern, die vom Nutzer eingegeben wurden \u2013 beispielsweise bereits registrierte Namen oder die Sprachauswahl. Damit werden verbesserte und personalisierte Funktionen gew\u00e4hrleistet.","required":false,"cookies":[{"key":"powr","name":"powr","description":"POWr.io Cookies\n\nDiese Cookies registrieren anonyme, statistische Daten \u00fcber das Verhalten des Besuchers dieser Website und sind verantwortlich f\u00fcr die Gew\u00e4hrleistung der Funktionalit\u00e4t bestimmter Widgets, die auf dieser Website eingesetzt werden. Sie werden ausschlie\u00dflich f\u00fcr interne Analysen durch den Webseitenbetreiber verwendet z. B. f\u00fcr den Besucherz\u00e4hler.\n\nAnbieter:\nPowr.io, POWr HQ, 340 Pine Street, San Francisco, California 94104, USA.\n\nCookie Namen und Laufzeiten:\nyahoy_unique_[unique id] (Laufzeit: Sitzung), POWR_PRODUCTION  (Laufzeit: Sitzung),  ahoy_visitor  (Laufzeit: 2 Jahre),   ahoy_visit  (Laufzeit: 1 Tag), src (Laufzeit: 30 Tage) Security, _gid Persistent (Laufzeit: 1 Tag).\n\nCookie-Richtlinie:\nhttps:\/\/www.powr.io\/privacy \n\nDatenschutzerkl\u00e4rung:\nhttps:\/\/www.powr.io\/privacy ","required":false}]},{"type":"PERFORMANCE","name":"Performance","description":"Die Performance-Cookies sammeln Informationen dar\u00fcber, wie diese Website genutzt wird. Der Betreiber der Website nutzt diese Cookies um die Attraktivit\u00e4t, den Inhalt und die Funktionalit\u00e4t der Website zu verbessern.","required":false,"cookies":[{"key":"jimdo_analytics","name":"jimdo_analytics","description":"Jimdo Statistics\n\nDie sog. Jimdo Statistikfunktion ist eine Tracking-Technologie, basierend auf Google Analytics, welche von der Jimdo GmbH betrieben wird.\n\nAnbieter:\nJimdo GmbH, Stresemannstrasse 375, 22761 Hamburg, Deutschland.\n\nCookie-Namen und Laufzeiten:\nckies_jimdo_analytics (Laufzeit: 2 Jahre), __utma (Laufzeit: 2 Jahre), __utmb (Laufzeit: 30 Minuten), __utmc (Laufzeit: Sitzung), __utmz (Laufzeit: 6 Monate), __utmt_b (Laufzeit: 1 Tag), __utm[unique ID] (Laufzeit: 2 Jahre), __ga (Laufzeit: 2 Jahre), __gat (Laufzeit: 1 Min), __gid (Laufzeit: 24 Stunden), __ga_disable_* (Laufzeit: 100 Jahre).\n\nCookie-Richtlinie:\nhttps:\/\/www.jimdo.com\/de\/info\/cookies\/policy\/\n\nDatenschutzerkl\u00e4rung:\nhttps:\/\/www.jimdo.com\/de\/info\/datenschutzerklaerung\/ ","required":false}]},{"type":"MARKETING","name":"Marketing \/ Third Party","description":"Marketing- \/ Third Party-Cookies stammen unter anderem von externen Werbeunternehmen und werden verwendet, um Informationen \u00fcber die vom Nutzer besuchten Websites zu sammeln, um z. B. zielgruppenorientierte Werbung f\u00fcr den Benutzer zu erstellen.","required":false,"cookies":[]}],"pagesWithoutCookieSettings":["\/about\/","\/j\/privacy"],"cookieSettingsHtmlUrl":"\/app\/module\/cookiesettings\/getcookiesettingshtml"});
// ]]>
</script>
</body>
</html>
