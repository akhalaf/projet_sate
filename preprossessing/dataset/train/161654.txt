<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]--><!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]--><!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="" rel="canonical"/>
<title>Aspyr</title>
<meta content="Hi, We're Aspyr, a leading entertainment publisher that creates, packages and delivers fun to millions around the world. Hailing from Austin, Texas, Aspyr strives relentlessly to ensure a quality experience for our industry partners and our players." name="description"/>
<meta content="aspyr,mac,publisher,pc,videogames,games" name="keywords"/>
<meta content="authenticity_token" name="csrf-param"/>
<meta content="5UUHlV0XA07PPgZJCebehJc7iCwqqwUv6Y3RF+QzhqWueobZJtxRxmhB5coiGxtJ0zRWazG81ty3lAU5sVXgwA==" name="csrf-token"/>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "Aspyr Media",
    "url": "https://www.aspyr.com",
    "logo": "https://assets.aspyr.com.s3.amazonaws.com/uploads/logo.png",
    "sameAs": [
      "https://www.facebook.com/aspyrmedia",
      "https://twitter.com/aspyrmedia",
      "https://www.youtube.com/c/aspyrmedia",
      "https://plus.google.com/+aspyrmedia"
    ]
  }
  </script>
<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-6028353-1', 'auto');
      ga('send', 'pageview');
  </script>
<!-- Facebook OG -->
<meta content="Aspyr Media" property="og:title"/>
<meta content="article" property="og:type"/>
<meta content="" property="og:url"/>
<meta content="Aspyr Media" property="og:site_name"/>
<meta content="" property="og:description"/>
<!-- Viewport params (for iOS) -->
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/>
<!-- Favicon -->
<link href="/assets/favicon/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="/assets/favicon/apple-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="/assets/favicon/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/assets/favicon/apple-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/assets/favicon/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="/assets/favicon/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/assets/favicon/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="/assets/favicon/apple-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/assets/favicon/apple-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/assets/favicon/android-icon-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="/assets/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/assets/favicon/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="/assets/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/assets/favicon/manifest.json" rel="manifest"/>
<meta content="#ffffff" name="msapplication-TileColor"/>
<meta content="/assets/favicon/ms-icon-144x144.png" name="msapplication-TileImage"/>
<meta content="#ffffff" name="theme-color"/>
<link href="https://fonts.googleapis.com/css?family=Roboto&amp;display=swap" rel="stylesheet"/>
<script src="https://cdn.auth0.com/js/lock/11.x/lock.min.js"></script>
<!-- CSS Styles -->
<link data-turbolinks-track="true" href="/assets/application-c6e1726d853af80d8f43bd49087c58bf8421139cdd3ab20d862b5abeb3fc60f7.css" media="all" rel="stylesheet"/>
<script data-turbolinks-track="true" src="/assets/application-caad1411b96bb6a811d7575a4f13debc56af7d1230287b17157093e1f0687176.js"></script>
<meta content="57b2095adf38e85972149305ff801c29e2deb4b012e52a8a" name="state"/>
</head>
<body class="home is-top">
<!-- STICKY TOP BAR -->
<div class="c-topbar">
<a class="c-topbar__branding" data-turbolinks="false" href="/">
<img alt="Aspyr" src="/assets/aspyr-newlogo-white.svg"/>
</a>
<nav class="c-topbar__nav">
<a class="js-search-toggle" href="#search"><span class="fas fa-search"></span></a>
<a data-turbolinks="false" href="/account">
<span class="fas fa-user"></span>
</a> <button aria-controls="navigation" aria-expanded="false" aria-label="Menu" class="c-topbar__hamburger js-menu-toggle" type="button">
<span>Menu</span>
</button>
</nav>
</div>
<!-- MENU -->
<nav class="c-menu js-menu">
<div>
<ul>
<li><a href="/">Home</a></li>
<li><a href="/about">About Us</a></li>
<li><a href="/games">Games</a></li>
<li><a href="/careers">Careers</a></li>
<li><a class="c-menu__small" href="https://support.aspyr.com/">Get Support</a></li>
<li><a class="c-menu__small" href="/account">Register My Game</a></li>
<li><a class="c-menu__small" href="/contact">Contact Us</a></li>
</ul>
</div>
</nav>
<!-- SEARCH -->
<div class="c-search js-search" id="search">
<div>
<form action="/search.html" class="c-search__form">
<label class="sr-only" for="search">Search</label>
<input aria-describedby="search-button" aria-label="game search" class="form-control" name="search_term" placeholder="Game Search" type="search"/>
<button class="btn" id="search-button" type="submit"><span class="fas fa-search"></span></button>
</form>
<p>Search by Platform</p>
<div class="c-search__buttons row">
<div class="col-6 col-sm-4 col-md-3">
<a class="btn btn-block btn-silver" href="/search?search_term=&amp;platform%5B%5D=Android" style="margin-bottom: 10px">Android</a>
</div>
<div class="col-6 col-sm-4 col-md-3">
<a class="btn btn-block btn-silver" href="/search?search_term=&amp;platform%5B%5D=iOS" style="margin-bottom: 10px">iOS</a>
</div>
<div class="col-6 col-sm-4 col-md-3">
<a class="btn btn-block btn-silver" href="/search?search_term=&amp;platform%5B%5D=Linux" style="margin-bottom: 10px">Linux</a>
</div>
<div class="col-6 col-sm-4 col-md-3">
<a class="btn btn-block btn-silver" href="/search?search_term=&amp;platform%5B%5D=Mac" style="margin-bottom: 10px">Mac</a>
</div>
<div class="col-6 col-sm-4 col-md-3">
<a class="btn btn-block btn-silver" href="/search?search_term=&amp;platform%5B%5D=Nintendo" style="margin-bottom: 10px">Nintendo</a>
</div>
<div class="col-6 col-sm-4 col-md-3">
<a class="btn btn-block btn-silver" href="/search?search_term=&amp;platform%5B%5D=PS4" style="margin-bottom: 10px">PS4</a>
</div>
<div class="col-6 col-sm-4 col-md-3">
<a class="btn btn-block btn-silver" href="/search?search_term=&amp;platform%5B%5D=Windows" style="margin-bottom: 10px">Windows</a>
</div>
<div class="col-6 col-sm-4 col-md-3">
<a class="btn btn-block btn-silver" href="/search?search_term=&amp;platform%5B%5D=Xbox One" style="margin-bottom: 10px">Xbox One</a>
</div>
</div>
</div>
</div>
<style>
  @media only screen and (max-width: 823px) and (min-width: 600px) {
    .c-menu a {
      font-size: inherit !important;
      padding: 0 0 0 0;
    }
  }
</style>
<!-- HERO CAROUSEL -->
<div class="c-hero">
<!-- FOR DESKTOP -->
<div class="c-hero__carousel c-hero__carousel--desktop cycle-slideshow" data-cycle-auto-height="2" data-cycle-fx="scrollHorz" data-cycle-next="#hero-next" data-cycle-pager="#hero-pager" data-cycle-prev="#hero-prev" data-cycle-slides="&gt; div" data-cycle-swipe="true" data-cycle-timeout="0" id="hero">
<div class="c-hero__slide">
<img alt="image 1" src="/assets/home-hero-1-1a59bb170bb129f5f5265abfa13ce6098d738d23725877c6f558af298166be4e.jpg" style="width: 100%;"/>
<!--<div class="c-hero__slide-caption">Create the Impossible</div>-->
</div>
<div class="c-hero__slide">
<img alt="image 2" src="/assets/home-hero-2-87004a1b32a833c3dde3aacbe4060af9d767e89f1e49a54c49dbb5d0bab7b520.jpg" style="width: 100%;"/>
<!--<div class="c-hero__slide-caption">Lorem Ipsum Dolor</div>-->
</div>
<div class="c-hero__slide">
<img alt="image 3" src="/assets/home-hero-3-c0990034e10bc3a983539080e578a27cbdcd304c23333aef68d5c88c7d7f4348.jpg" style="width: 100%;"/>
<!--<div class="c-hero__slide-caption">Consectetur Adipiscing Elit</div>-->
</div>
</div>
<div class="fal fa-long-arrow-left prev" id="hero-prev"></div>
<div class="fal fa-long-arrow-right next" id="hero-next"></div>
<div class="cycle-pager c-hero__pager" id="hero-pager"></div>
<!-- FOR MOBILE -->
<div class="c-hero__carousel c-hero__carousel--mobile cycle-slideshow" data-cycle-auto-height="1" data-cycle-fx="scrollHorz" data-cycle-pager="#hero-pager-mobile" data-cycle-slides="&gt; div" data-cycle-swipe="true" data-cycle-timeout="0" id="hero">
<div class="c-hero__slide" style="background:url('/assets/home-hero-1.jpg') center center no-repeat;background-size:cover;"></div>
<div class="c-hero__slide" style="background:url('/assets/home-hero-2.jpg') center center no-repeat;background-size:cover;"></div>
<div class="c-hero__slide" style="background:url('/assets/home-hero-3.jpg') center center no-repeat;background-size:cover;"></div>
</div>
<div class="cycle-pager c-hero__pager" id="hero-pager-mobile"></div>
<h1 class="c-hero__caption">Create the Impossible</h1>
</div>
<!-- FOCUS BLOCK -->
<!-- Background image shown for desktop -->
<!-- parallax js init code in footer -->
<div class="c-focus c-focus--gradient-top c-parallax" id="js-parallax-window-1">
<div class="c-parallax__image js-fadein-onscroll c-fadein-onscroll u-hidden-mobile" id="js-parallax-bg-1" style="background:url('/assets/home-brands') center center no-repeat; background-size:cover;"></div>
<div class="container">
<div class="row">
<div class="col-md-5">
<p class="u-subheading u-color-blue">Our Experience</p>
<h2 class="c-focus__heading">Success with the Biggest Brands</h2>
<p>
<a class="btn btn-primary btn-lg" href="about">About Us</a>
</p>
</div>
<!-- MOBILE ONLY - THIS CAN BE A DIFFERENT IMAGE PREPPED SPECIFICALLY FOR MOBILE -->
<div class="col-12 c-focus__mobile-img">
<img alt="only shown below 768px" class="img-fluid" src="/assets/home-brands-5e4d265d0429c410cf6f14650077f270d9096e47cf0b94a8afa464ce94a62517.gif"/>
</div>
</div>
</div>
</div>
<!-- FOCUS BLOCK: RIGHT -->
<!-- Background image shown for desktop -->
<div class="c-focus c-parallax c-focus--hiring" id="js-parallax-window-2">
<div class="c-parallax__image js-fadein-onscroll c-fadein-onscroll u-hidden-mobile" id="js-parallax-bg-2" style="background:url('/assets/home-careers.jpg') center center no-repeat; background-size:cover;"></div>
<div class="container">
<div class="row">
<div class="col-md-5 offset-md-7">
<p class="u-subheading u-color-white">Our Team</p>
<h2 class="c-focus__heading">We’re Hiring</h2>
<p>
<a class="btn btn-white btn-lg" href="careers">Careers</a>
</p>
</div>
<!-- MOBILE ONLY - THIS CAN BE A DIFFERENT IMAGE PREPPED SPECIFICALLY FOR MOBILE -->
<div class="col-12 c-focus__mobile-img">
<img alt="only shown below 768px" class="img-fluid" src="/assets/home-careers-706f2678bb49d5876b39fbc8924cadec955b67ed91fb5fe4ff83ae6a2bb29597.jpg"/>
</div>
</div>
</div>
</div>
<script>
    /**
     * PARALLAX
     * Replace '1' with a unique ID for this block
     */
    var plxWindow1, plxBg1;
    $(document).on('turbolinks:load', function() {

        if ($("#js-parallax-window-1").length) {
            plxWindow1 = $("#js-parallax-window-1");
            plxBg1 = $("#js-parallax-bg-1");
            parallax1();
        }
    });

    $(window).scroll(function(e) {
        if (plxWindow1.length) {
            parallax1();
        }
    });

    function parallax1(){
        if( plxWindow1.length > 0 ) {
            var plxBackground = plxBg1;
            var plxWindow = plxWindow1;

            var plxWindowTopToPageTop = $(plxWindow).offset().top;
            var windowTopToPageTop = $(window).scrollTop();
            var plxWindowTopToWindowTop = plxWindowTopToPageTop - windowTopToPageTop;

            var plxBackgroundTopToPageTop = $(plxBackground).offset().top;
            var windowInnerHeight = window.innerHeight;
            var plxBackgroundTopToWindowTop = plxBackgroundTopToPageTop - windowTopToPageTop;
            var plxBackgroundTopToWindowBottom = windowInnerHeight - plxBackgroundTopToWindowTop;
            var plxSpeed = 0.35;

            plxBackground.css('top', - (plxWindowTopToWindowTop * plxSpeed) + 'px');
        }
    }

    /**
     * PARALLAX
     * Replace '2' with a unique ID for this block
     */
    var plxWindow2, plxBg2;
    $(document).on('turbolinks:load', function() {
        if ($("#js-parallax-window-2").length) {
            plxWindow2 = $("#js-parallax-window-2");
            plxBg2 = $("#js-parallax-bg-2");
            parallax2();
        }
    });

    $(window).scroll(function(e) {
        if (plxWindow2.length) {
            parallax2();
        }
    });

    function parallax2(){
        if( plxWindow2.length > 0 ) {
            var plxBackground = plxBg2;
            var plxWindow = plxWindow2;

            var plxWindowTopToPageTop = $(plxWindow).offset().top;
            var windowTopToPageTop = $(window).scrollTop();
            var plxWindowTopToWindowTop = plxWindowTopToPageTop - windowTopToPageTop;

            var plxBackgroundTopToPageTop = $(plxBackground).offset().top;
            var windowInnerHeight = window.innerHeight;
            var plxBackgroundTopToWindowTop = plxBackgroundTopToPageTop - windowTopToPageTop;
            var plxBackgroundTopToWindowBottom = windowInnerHeight - plxBackgroundTopToWindowTop;
            var plxSpeed = 0.35;

            plxBackground.css('top', - (plxWindowTopToWindowTop * plxSpeed) + 'px');
        }
    }
</script>
<!-- GLOBAL FOOTER -->
<footer class="c-footer">
<div class="container">
<div class="row">
<div class="col-12 c-footer__col">
<p>
<a class="c-footer__branding" href="/" title="homepage"><img alt="Aspyr" src="/assets/aspyr-newlogo-blue.svg"/></a>
</p>
</div>
<div class="col-12 c-footer__col">
<p>
<a class="c-footer__social-link" href="https://www.facebook.com/aspyrmedia/" target="_blank" title="connect with us on facebook"><span class="fab fa-facebook-f"></span></a>
<a class="c-footer__social-link" href="https://twitter.com/AspyrMedia" target="_blank" title="connect with us on twitter"><span class="fab fa-twitter"></span></a>
<a class="c-footer__social-link" href="https://www.youtube.com/c/aspyrmedia" target="_blank" title="connect with us on youtube"><span class="fab fa-youtube"></span></a>
<a class="c-footer__social-link" href="https://www.linkedin.com/company/aspyr-media/" target="_blank" title="connect with us on linkedin"><span class="fab fa-linkedin-in"></span></a>
<a class="c-footer__social-link" href="https://discord.gg/aspyrmedia" target="_blank" title="connect with us on discord"><span class="fab fa-discord"></span></a>
</p>
</div>
<div class="col-12 c-footer__col">
<p>
<a class="c-footer__link" href="/contact">Contact Us</a>
<a class="c-footer__link" href="/careers">Join Our Team</a>
<a class="c-footer__link" href="https://support.aspyr.com/">Game Support</a>
<a class="c-footer__link" href="/account">Register Your Game</a>
</p>
</div>
<div class="col-12 c-footer__col">
<p>
<a class="c-footer__link--small" href="/legal">Legal</a>
<a class="c-footer__link--small" href="/terms">Terms</a>
<a class="c-footer__link--small" href="/privacy">Privacy</a>
</p>
</div>
</div>
</div>
</footer>
<script>
</script>
</body>
</html>
