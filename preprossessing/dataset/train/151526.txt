<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]--><!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]--><!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<title>Armin Sadeghi</title>
<meta content="" name="description"/>
<meta content="width=device-width" name="viewport"/>
<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="apple-touch-icon-precomposed.png" rel="apple-touch-icon-precomposed"/>
<link href="apple-touch-icon-72x72-precomposed.png" rel="apple-touch-icon-precomposed"/>
<link href="apple-touch-icon-114x114-precomposed.png" rel="apple-touch-icon-precomposed"/>
<link href="apple-touch-icon-144x144-precomposed.png" rel="apple-touch-icon-precomposed"/>
<link href="font/font-awesome.css" rel="stylesheet"/>
<link href="css/normalize.min.css" rel="stylesheet"/>
<link href="css/main.css" rel="stylesheet"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-160721761-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-160721761-1');
</script>
<script src="jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="js/vendor/jquery.hashchange.min.js"></script>
<script src="js/vendor/jquery.easytabs.min.js"></script>
<script src="js/main.js"></script>
<!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
      <![endif]-->
</head>
<body class="bg-fixed bg-1">
<!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
<div class="main-container">
<div class="main wrapper clearfix">
<!-- Header Start -->
<!--   <header id="header">
            <div id="logo">
                <h2>
                    Armin Sadeghi
                </h2>-->
<h4>
</h4>
</div>
<!-- Header End -->
<!-- Main Tab Container -->
<div class="tab-container" id="tab-container">
<!-- Tab List -->
<ul class="etabs">
<li class="tab" id="tab-about">
<a href="#about"><i class="icon-user"></i><span> About Me</span></a>
</li>
<li class="tab">
<a href="#education"><i class="icon-book"></i><span> Education</span></a>
</li>
<!--<li class='tab'>
                  <a href="#portfolio"><i class="icon-heart"></i><span> Portfolio</span></a>
                </li>-->
<li class="tab">
<a href="#Download"><i class="icon-download-alt"></i><span> Download</span></a>
</li>
</ul>
<!-- End Tab List -->
<div id="tab-data-wrap">
<!-- About Tab Data -->
<div id="about">
<section class="clearfix">
<div class="g2">
<div class="photo">
<img alt="Armin Sadeghi" src="image/new.jpg"/>
</div>
<div class="info">
<h2>
                                    Armin Sadeghi
                                </h2>
<h4>
</h4>
<p>
                                   
							       My name is Armin Sadeghi. I am a Physics PhD student at <a href="http://ut.ac.ir/en" style="font-size:normal">University of Tehran</a> majoring Gravitation and Cosmology.
                                </p>
</div>
</div>
<div class="g1">
<div class="main-links sidebar">
<ul>
<li>
<a href="#education">Education</a>
</li>
<!--  <li>
                                        <a href="#portfolio">My Portfolio</a>
                                    </li>-->
<li>
<a href="#Download">Download</a>
</li>
<!--       <li>
                                        <a href="#features">Features</a>
                                    </li>-->
</ul>
</div>
</div>
<div class="break"></div>
<div class="contact-info">
<div class="g1">
<div class="item-box clearfix">
<i class="icon-envelope"></i>
<div class="item-data">
<h3><a href="mailto:armin.sadeghi.h@gmail.com">armin.sadeghi.h@gmail.com</a></h3>
<p>Email Address</p>
</div>
</div>
</div>
<div class="g1">
<div class="item-box clearfix">
<i class="icon-linkedin"></i>
<div class="item-data">
<h3><a href="https://www.linkedin.com/in/arminsadeghi/">Find me on Linkdin</a></h3>
<p>Linkedin Profile</p>
</div>
</div>
</div>
<div class="g1">
<div class="item-box clearfix">
<i class="icon-facebook-sign"></i>
<div class="item-data">
<h3><a href="https://www.facebook.com/armin.sadeghi.s">Find me on Facebook</a></h3>
<p>Facebook</p>
</div>
</div>
</div>
</div>
</section><!-- content -->
</div>
<!-- End About Tab Data -->
<!-- Resume Tab Data -->
<div id="education">
<section class="clearfix">
<div class="g2">
<h3>
                                Education
                            </h3>
<ul class="no-list work">
<li>
<h5>
<a href="http://en.ut.ac.ir">University of Tehran - PhD</a><br/>
</h5><span class="label label-info">2019- Ongoing</span>
<p>
<b>Major:</b>
                                        Gravitation and Cosmology
										</p><p>
										Supervisor: <a href="http://physics.ut.ac.ir/people/faculty/31-shojai-baghini-fatimah">Dr. Fatimah Shojai Baghini</a>
</p></li>
<li>
<h5>
<a href="http://en.ut.ac.ir">University of Tehran - MSc</a><br/>
</h5><span class="label label-info">2015-2018</span>
<p>
<b>Major:</b>
                                        Gravitation and Astrophysics
										</p><p>
<b>Dissertation title:</b>
										Actions and Cauchy Problem in General Relativity 
										</p><p>
										Supervisor: <a href="http://physics.ut.ac.ir/people/faculty/31-shojai-baghini-fatimah">Dr. Fatimah Shojai Baghini</a>
</p></li>
<li>
<h5>
<a href="http://znu.ac.ir/en">University of Zanjan - BSc</a><br/>
</h5><span class="label label-info">2009-2014</span>
<p>
<b>Major:</b>
										Theoretical Physics
								</p></li>
</ul>
</div>
</section></div>
<!--       Interests
                                </h5>
                                <div class="meter emerald">
                                    <span style="width: 99%"><span>Modified & Quantum Gravity</span></span>
                                </div>
                                <div class="meter carrot">
                                    <span style="width: 80%"><span>String Theory</span></span>
                                </div>
								<div class="meter asbestos">
                                    <span style="width: 75%"><span>Black Holes & G-Waves</span></span>
                                </div>
                                <div class="meter wisteria">
                                    <span style="width: 45%"><span>Cosmology</span></span>
                                </div>
                               <!-- <div class="meter sunflower">
                                    <span style="width: 40%"><span>Cosmology</span></span>
                                </div>
                                <div class="meter wisteria">
                                    <span style="width: 50%"><span>Android</span></span>
                                </div>
                                <div class="meter midnight">
                                    <span style="width: 50%"><span>Sql Server</span></span>
                                </div>
                                <div class="meter carrot">
                                    <span style="width: 30%"><span>Neo4j</span></span>
                                </div>
                                <div class="meter pomengrate">
                                    <span style="width: 25%"><span>Hadoop</span></span>
                                </div>
                                <div class="break"></div>-->
<!--        Applications
                                </h5>
                                <div class="meter sunflower">
                                    <span style="width: 100%"><span>Latex</span></span>
                                </div>
                                <div class="meter midnight">
                                    <span style="width: 60%"><span>MATHEMATICA</span></span>
                                </div>
                                <div class="meter pomengrate">
                                    <span style="width: 50%"><span>Web Design</span></span>
                                </div>
                                <div class="break"></div>
                                <h5>
                              <!--      Software
                                </h5>
                                <div class="meter pomengrate">
                                    <span style="width: 90%"><span>Microsoft Office</span></span>
                                </div>

                                <div class="meter sunflower">
                                    <span style="width: 65%"><span>Linux</span></span>
                                </div>

                                <div class="meter wisteria">
                                    <span style="width: 100%"><span>Windows</span></span>
                                </div>
                                <div class="meter asbestos">
                                    <span style="width: 85%"><span>Latex</span></span>
                                </div>
                                <div class="meter midnight">
                                    <span style="width: 85%"><span>Latex</span></span>
                                </div>-->
<div id="Download">
<section class="clearfix">
<section class="clearfix">
<div class="g2">
<h3>
                                Download
                            </h3>
<ul class="no-list work">
<li>
<p>
						             Click the links to access problems and solutions.
									   </p><p>
<a href="/Gravitation/2020GR2/index.html">TA Gravitation 2</a>
</p><p>
                                        The Course has ended.
									</p></li><li>
<p>
<a href="/Gravitation/2019GR1/index.html">TA Gravitation 1</a>
</p><p>
                                        The Course has ended.
									</p></li><li>
</li></ul>
</div>
</section></section></div>
<!-- End Resume Tab Data -->
<!-- Portfolio Tab Data -->
<!-- <div id="portfolio">
                    <section class="clearfix">
                        <!--<div class="g1">
                            <div class="image">
                                <img src="http://lorempixel.com/306/306/sports/1" alt="">
                                <div class="image-overlay">
                                    <div class="image-link">
                                        <a href="http://lorempixel.com/606/606/sports/1" class="btn">Full View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="g1">
                            <div class="image">
                                <img src="http://lorempixel.com/306/306/sports/2" alt="">
                                <div class="image-overlay">
                                    <div class="image-link">
                                        <a href="http://lorempixel.com/606/606/sports/2" class="btn">Full View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="g1">
                            <div class="image">
                                <img src="http://lorempixel.com/306/306/sports/3" alt="">
                                <div class="image-overlay">
                                    <div class="image-link">
                                        <a href="http://lorempixel.com/606/606/sports/3" class="btn">Full View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="break"></div>
                        <div class="g1">
                            <div class="image">
                                <img src="http://lorempixel.com/306/306/sports/4" alt="">
                                <div class="image-overlay">
                                    <div class="image-link">
                                        <a href="http://lorempixel.com/606/606/sports/4" class="btn">Full View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="g1">
                            <div class="image">
                                <img src="http://lorempixel.com/306/306/sports/5" alt="">
                                <div class="image-overlay">
                                    <div class="image-link">
                                        <a href="http://lorempixel.com/606/606/sports/5" class="btn">Full View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="g1">
                            <div class="image">
                                <img src="http://lorempixel.com/306/306/business/2" alt="">
                                <div class="image-overlay">
                                    <div class="image-link">
                                        <a href="http://lorempixel.com/606/606/business/2" class="btn">Full View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                   </section>-->
</div>
<!-- End Portfolio Data -->
<!-- Download Tab Data -->
<!-- End Download Data -->
</div>
</div>
<!-- End Tab Container -->
<!--  <footer>
            <p>

            </p>
        </footer>-->
<!-- #main -->
<!-- #main-container -->
</body>
</html>
