<!DOCTYPE html>
<html lang="ja" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="ç¬ç«ããã¸ã£ã¼ããªãºã ã®ç¢ºç«ã«åãã¦" name="description"/>
<meta content="ã¢ã¸ã¢ãã¬ã¹ã»ã¤ã³ã¿ã¼ãã·ã§ãã«" name="author"/>
<link href="http://www.asiapress.org/" rel="start" title="TOP"/>
<!-- OGP -->
<meta content="ã¢ã¸ã¢ãã¬ã¹ã»ã¤ã³ã¿ã¼ãã·ã§ãã«" property="og:site_name"/>
<meta content="ç¬ç«ããã¸ã£ã¼ããªãºã ã®ç¢ºç«ã«åãã¦" property="og:description"/>
<meta content="ã¢ã¸ã¢ãã¬ã¹ã»ã¤ã³ã¿ã¼ãã·ã§ãã«" property="og:title"/>
<meta content="http://www.asiapress.org/" property="og:url"/>
<meta content="website" property="og:type"/>
<!-- twitter:card -->
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@API_official" name="twitter:site"/>
<title>ã¢ã¸ã¢ãã¬ã¹ã»ã¤ã³ã¿ã¼ãã·ã§ãã« – ç¬ç«ããã¸ã£ã¼ããªãºã ã®ç¢ºç«ã«åãã¦</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="http://www.asiapress.org/feed/" rel="alternate" title="ã¢ã¸ã¢ãã¬ã¹ã»ã¤ã³ã¿ã¼ãã·ã§ãã« » ãã£ã¼ã" type="application/rss+xml"/>
<link href="http://www.asiapress.org/comments/feed/" rel="alternate" title="ã¢ã¸ã¢ãã¬ã¹ã»ã¤ã³ã¿ã¼ãã·ã§ãã« » ã³ã¡ã³ããã£ã¼ã" type="application/rss+xml"/>
<script>
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/www.01.asiapress.org\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style>
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="http://www.asiapress.org/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet"/>
<link href="http://www.asiapress.org/wp-content/themes/liquid-corporate/css/bootstrap.min.css?ver=5.5.3" id="bootstrap-css" media="all" rel="stylesheet"/>
<link href="http://www.asiapress.org/wp-content/themes/liquid-corporate/css/icomoon.css?ver=5.5.3" id="icomoon-css" media="all" rel="stylesheet"/>
<link href="http://www.asiapress.org/wp-content/themes/liquid-corporate/style.css?ver=5.5.3" id="liquid-style-css" media="all" rel="stylesheet"/>
<link href="http://www.asiapress.org/wp-content/themes/liquid-corporate-Child/style.css?ver=5.5.3" id="child-style-css" media="all" rel="stylesheet"/>
<link href="http://www.asiapress.org/wp-content/themes/liquid-corporate/css/block.css?ver=5.5.3" id="liquid-block-style-css" media="all" rel="stylesheet"/>
<link href="http://www.asiapress.org/wp-content/plugins/newpost-catch/style.css?ver=5.5.3" id="newpost-catch-css" media="all" rel="stylesheet"/>
<script id="jquery-core-js" src="http://www.asiapress.org/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"></script>
<script id="bootstrap-js" src="http://www.asiapress.org/wp-content/themes/liquid-corporate/js/bootstrap.min.js?ver=5.5.3"></script>
<link href="http://www.asiapress.org/wp-json/" rel="https://api.w.org/"/><link href="http://www.asiapress.org/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="http://www.asiapress.org/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<link href="http://www.asiapress.org/wp-content/uploads/2018/06/asiapress_favicon.gif" rel="icon" sizes="32x32"/>
<link href="http://www.asiapress.org/wp-content/uploads/2018/06/asiapress_favicon.gif" rel="icon" sizes="192x192"/>
<link href="http://www.asiapress.org/wp-content/uploads/2018/06/asiapress_favicon.gif" rel="apple-touch-icon"/>
<meta content="http://www.asiapress.org/wp-content/uploads/2018/06/asiapress_favicon.gif" name="msapplication-TileImage"/>
<style id="wp-custom-css">
			.headline { background-image: url("http://www.asiapress.org/wp-content/uploads/2018/07/header.png"); background-position: top center; background-size: 100%; background-repeat: no-repeat; background-attachment: scroll; }

.asia-top-apn {
	display: flex;
	margin: 0px 15px 25px 0px;
}

.sidebar {
    padding-top: 0;
}		</style>
<!--[if lt IE 9]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- hreflang -->
<link href="http://www.asiapress.org/" hreflang="ja" rel="alternate"/>
<link href="http://www.asiapress.org/en/" hreflang="en" rel="alternate"/>
<link href="http://www.asiapress.org/kr/" hreflang="kr" rel="alternate"/>
<!-- CSS -->
<style>
/*  customize  */
.liquid_bg, .carousel-indicators .active, .icon_big, .navbar-nav > .nav-item:last-child:not(.sticky-none) a,
.has-liquid-theme-background-color {
    background-color: #dd3333 !important;
}
.liquid_bc, .post_body h1 span, .post_body h2 span, .ttl span,
.archive .ttl_h1, .search .ttl_h1, .headline, .formbox a,
.has-liquid-theme-background-color.is-style-blockbox {
    border-color: #dd3333 !important;
}
.breadcrumb {
    border-top: 3px solid #dd3333 !important;
}
.liquid_color, .navbar .current-menu-item, .navbar .current-menu-parent, .navbar .current_page_item,
.has-liquid-theme-color {
    color: #dd3333 !important;
}
a, a:hover, a:active, a:visited,
.post_body a, .post_body a:hover, .post_body a:active, .post_body a:visited,
footer a, footer a:hover, footer a:active, footer a:visited {
    color: #1e73be;
}
body .headline, body .headline a, .navbar, body .navbar a {
    color: #333333 !important;
}
.headline .sns a, .navbar-toggler .icon-bar {
    background-color: #333333 !important;
}
/*  custom head  */
</style>
</head>
<body class="home blog">
<!-- FB -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<a id="top"></a>
<div class="wrapper">
<div class="headline">
<div class="logo_text">
<div class="container">
<div class="row">
<div class="col-md-3 order-md-last">
<div class="lang">
<span class="lang_001" title="ja">
                            æ¥æ¬èª</span>
<a class="lang_002" href="http://www.asiapress.org/en/" title="en">
                            English</a>
<a class="lang_003" href="http://www.asiapress.org/kr/" title="kr">
                            íêµ­ì´</a>
</div>
</div>
<div class="col-md-9 order-md-first">
<h1 class="subttl">
                        ç¬ç«ããã¸ã£ã¼ããªãºã ã®ç¢ºç«ã«åãã¦                    </h1>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-sm-6">
<a class="logo" href="http://www.asiapress.org/" title="ã¢ã¸ã¢ãã¬ã¹ã»ã¤ã³ã¿ã¼ãã·ã§ãã«">
<img alt="ã¢ã¸ã¢ãã¬ã¹ã»ã¤ã³ã¿ã¼ãã·ã§ãã«" src="http://www.asiapress.org/wp-content/uploads/2018/06/cropped-API_top_logo-1.jpg"/>
</a>
</div>
<div class="col-sm-6">
</div>
</div>
</div>
</div>
<nav class="navbar navbar-light navbar-expand-md flex-column">
<div class="container">
<!-- Global Menu -->
<ul class="nav navbar-nav" id="menu-%e3%83%88%e3%83%83%e3%83%97%e3%83%a1%e3%82%a4%e3%83%b3%e3%83%a1%e3%83%8b%e3%83%a5%e3%83%bc"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1055 nav-item" id="menu-item-1055"><a href="http://www.asiapress.org/what/">ã¢ã¸ã¢ãã¬ã¹ã¨ã¯</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1056 nav-item" id="menu-item-1056"><a href="http://www.asiapress.org/asiapress-member/">ã¡ã³ãã¼ãã¡</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-57 nav-item" id="menu-item-57"><a href="http://www.asiapress.org/video-journalism/">ãããªã¸ã£ã¼ããªãºã </a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1048 nav-item" id="menu-item-1048"><a href="http://www.asiapress.org/media-corporate/archives-service/">ãããªæ åã»åçãã¼ã¿ã®éä¿¡</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-59 nav-item" id="menu-item-59"><a href="http://www.asiapress.org/what/lecture-screening-writing/">å·ç­ã»è¬æ¼ç­ã®ä¾é ¼</a></li>
<li class="sticky-none menu-item menu-item-type-custom menu-item-object-custom menu-item-58 nav-item" id="menu-item-58"><a href="http://www.asiapress.org/category/book-dvd/">æ¸ç±ã»ï¼¤ï¼¶ï¼¤</a></li>
</ul> <button class="navbar-toggler collapsed" type="button">
<span class="sr-only">Menu</span>
<span class="icon-bar top-bar"></span>
<span class="icon-bar middle-bar"></span>
<span class="icon-bar bottom-bar"></span>
</button>
</div>
<div class="container searchform_nav d-none d-md-none">
<div class="searchform">
<form action="http://www.asiapress.org/" class="search-form" method="get">
<fieldset class="form-group">
<label class="screen-reader-text">æ¤ç´¢</label>
<input class="form-control search-text" name="s" placeholder="æ¤ç´¢" type="text" value=""/>
<button class="btn btn-primary" type="submit" value="Search"><i class="icon icon-search"></i></button>
</fieldset>
</form>
</div> </div>
</nav>
<!-- cover -->
<!-- carousel -->
<!-- /cover -->
<div class="mainpost">
<div class="container">
<!-- biz -->
<!-- /biz -->
<!-- pages -->
<!-- /pages -->
<div class="row newposts">
<div class="col-md-8 mainarea">
<div class="row widgets">
<div class="col-12" id="text-4"><div class="widget widget_text"> <div class="textwidget"><div class="ttl"><i class="icon icon-list"></i> å ±éã¦ã§ãã¸ã£ã¼ãã«</div>
<p><a href="http://www.asiapress.org/apn/" rel="noopener noreferrer" target="_blank"><img alt="" class="alignleft size-full wp-image-1046" height="84" loading="lazy" src="http://www.asiapress.org/img/APN_logo_L.jpg" style="border: solid 0px #99ccff;" width="730"/></a></p>
</div>
</div></div> </div>
<!-- single 1 -->
<div class="ttl"><i class="icon icon-list"></i> Whatâs New</div>
<div class="row single">
<article class="list col-md-12 post-1230 post type-post status-publish format-standard hentry category-new category-program-notice">
<a class="post_links" href="http://www.asiapress.org/new/tbs-nk-korona/" title="ãçªçµã®ãæ¡åã10æ10æ¥ï¼åï¼17:30ï½å ±éç¹éï¼TBSç³»åï¼ãã³ã­ãç¦ã®åæé®®ã®ï¼çµ¶å¯¾ç§å¯ï¼ã">
<div class="list-block">
<div class="post_thumb" style="background-image: url('http://www.asiapress.org/wp-content/uploads/2020/10/20201010_NK_TW01.jpg')"><span> </span></div> <div class="list-text">
<span class="post_time"><i class="icon icon-clock"></i> 2020å¹´10æ10æ¥</span>
<span class="post_cat"><i class="icon icon-folder"></i> Whatâs New</span> <h3 class="list-title post_ttl">
                           ãçªçµã®ãæ¡åã10æ10æ¥ï¼åï¼17:30ï½å ±éç¹éï¼TBSç³»åï¼ãã³ã­ãç¦ã®åæé®®ã®ï¼çµ¶å¯¾ç§å¯ï¼ã                       </h3>
</div>
</div>
</a>
</article>
</div>
<div class="more"><a href="http://www.asiapress.org/category/new/">More &gt;&gt;</a></div>
<!-- single 2 -->
<div class="ttl"><i class="icon icon-list"></i> ã¡ãã£ã¢ã»æ³äººåã ãµã¼ãã¹</div>
<div class="row single">
<article class="list col-md-12 post-90 post type-post status-publish format-standard has-post-thumbnail hentry category-media-corporate">
<a class="post_links" href="http://www.asiapress.org/media-corporate/archives-service/" title="ãããªæ åã»åçãã¼ã¿ã®éä¿¡">
<div class="list-block">
<div class="post_thumb" style="background-image: url('http://www.asiapress.org/wp-content/uploads/2018/07/APN_kiji_archive001.gif')"><span> </span></div> <div class="list-text">
<span class="post_time"><i class="icon icon-clock"></i> 2018å¹´6æ8æ¥</span>
<span class="post_cat"><i class="icon icon-folder"></i> ã¡ãã£ã¢ã»æ³äººåã ãµã¼ãã¹</span> <h3 class="list-title post_ttl">
                           ãããªæ åã»åçãã¼ã¿ã®éä¿¡                       </h3>
</div>
</div>
</a>
</article>
</div>
<div class="more"><a href="http://www.asiapress.org/category/media-corporate/">More &gt;&gt;</a></div>
<!-- single 3 -->
<div class="ttl"><i class="icon icon-list"></i> ãã­ã¥ã¡ã³ã¿ãªã¼ã»æ ç»</div>
<div class="row single">
<article class="list col-md-12 post-548 post type-post status-publish format-standard has-post-thumbnail hentry category-documentary-movies">
<a class="post_links" href="http://peace-tigris.com/#new_tab" title="ã¤ã©ã¯ã» ãã°ãªã¹ã«æµ®ãã¶å¹³å ï¼ç£ç£ï¼ç¶¿äºå¥é½ï¼">
<div class="list-block">
<div class="post_thumb" style="background-image: url('http://www.asiapress.org/wp-content/uploads/2018/06/20141012_tigris_watai200.jpg')"><span> </span></div> <div class="list-text">
<span class="post_time"><i class="icon icon-clock"></i> 2014å¹´11æ15æ¥</span>
<span class="post_cat"><i class="icon icon-folder"></i> ãã­ã¥ã¡ã³ã¿ãªã¼ã»æ ç»</span> <h3 class="list-title post_ttl">
                           ã¤ã©ã¯ã» ãã°ãªã¹ã«æµ®ãã¶å¹³å ï¼ç£ç£ï¼ç¶¿äºå¥é½ï¼                       </h3>
</div>
</div>
</a>
</article>
<article class="list col-md-12 post-553 post type-post status-publish format-standard hentry category-documentary-movies">
<a class="post_links" href="http://www.asiapress.org/documentary-movies/tonarubito/" title="é£ãäººï¼ç£ç£ï¼åå·åä¹ï¼">
<div class="list-block">
<div class="post_thumb" style="background-image: url('http://www.asiapress.org/wp-content/uploads/2018/06/tonaru-hito_02.jpg')"><span> </span></div> <div class="list-text">
<span class="post_time"><i class="icon icon-clock"></i> 2011å¹´11æ15æ¥</span>
<span class="post_cat"><i class="icon icon-folder"></i> ãã­ã¥ã¡ã³ã¿ãªã¼ã»æ ç»</span> <h3 class="list-title post_ttl">
                           é£ãäººï¼ç£ç£ï¼åå·åä¹ï¼                       </h3>
</div>
</div>
</a>
</article>
<article class="list col-md-12 post-1002 post type-post status-publish format-standard has-post-thumbnail hentry category-documentary-movies">
<a class="post_links" href="http://www.geocities.co.jp/WallStreet/7486/#new_tab" title="ãªã¬ã®å¿ã¯è² ãã¦ãªããå¨æ¥æé®®äººæ°å®å©¦ å®ç¥éã®ãããã  (ç£ç£: å®æµ·é¾)">
<div class="list-block">
<div class="post_thumb" style="background-image: url('http://www.asiapress.org/wp-content/uploads/2018/07/i.jpg')"><span> </span></div> <div class="list-text">
<span class="post_time"><i class="icon icon-clock"></i> 2007å¹´4æ5æ¥</span>
<span class="post_cat"><i class="icon icon-folder"></i> ãã­ã¥ã¡ã³ã¿ãªã¼ã»æ ç»</span> <h3 class="list-title post_ttl">
                           ãªã¬ã®å¿ã¯è² ãã¦ãªããå¨æ¥æé®®äººæ°å®å©¦ å®ç¥éã®ãããã  (ç£ç£: å®æµ·é¾)                       </h3>
</div>
</div>
</a>
</article>
<article class="list col-md-12 post-567 post type-post status-publish format-standard has-post-thumbnail hentry category-documentary-movies">
<a class="post_links" href="http://www.tongpoo-films.jp/littlebirds/#new_tab" title="ãªãã«ã»ãã¼ãº ã¤ã©ã¯ã»æ¦ç«ã®å®¶æãã¡ ï¼ç£ç£ï¼ç¶¿äºå¥é½ï¼">
<div class="list-block">
<div class="post_thumb" style="background-image: url('http://www.asiapress.org/wp-content/uploads/2018/06/LittleBirds005-0221fs.gif')"><span> </span></div> <div class="list-text">
<span class="post_time"><i class="icon icon-clock"></i> 2006å¹´4æ20æ¥</span>
<span class="post_cat"><i class="icon icon-folder"></i> ãã­ã¥ã¡ã³ã¿ãªã¼ã»æ ç»</span> <h3 class="list-title post_ttl">
                           ãªãã«ã»ãã¼ãº ã¤ã©ã¯ã»æ¦ç«ã®å®¶æãã¡ ï¼ç£ç£ï¼ç¶¿äºå¥é½ï¼                       </h3>
</div>
</div>
</a>
</article>
<article class="list col-md-12 post-558 post type-post status-publish format-standard has-post-thumbnail hentry category-documentary-movies">
<a class="post_links" href="http://www.asiapress.org/documentary-movies/hurui-ghada/" title="ã¬ã¼ãããã¬ã¹ããã®è©© ï¼æ®å½±ï½¥ç£ç£ãå¤å±ã¿ããï¼">
<div class="list-block">
<div class="post_thumb" style="background-image: url('http://www.asiapress.org/wp-content/uploads/2018/06/ghada_00242ws.jpg')"><span> </span></div> <div class="list-text">
<span class="post_time"><i class="icon icon-clock"></i> 2005å¹´12æ15æ¥</span>
<span class="post_cat"><i class="icon icon-folder"></i> ãã­ã¥ã¡ã³ã¿ãªã¼ã»æ ç»</span> <h3 class="list-title post_ttl">
                           ã¬ã¼ãããã¬ã¹ããã®è©© ï¼æ®å½±ï½¥ç£ç£ãå¤å±ã¿ããï¼                       </h3>
</div>
</div>
</a>
</article>
</div>
<div class="more"><a href="http://www.asiapress.org/category/documentary-movies/">More &gt;&gt;</a></div>
</div><!-- /col -->
<div class="col-md-4 sidebar">
<div class="row widgets">
<div class="widget_text d-none d-md-block col-12" id="custom_html-5"><div class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><a class="twitter-timeline" data-height="1250" data-lang="en" data-theme="light" data-width="350" href="https://twitter.com/API_official?ref_src=twsrc%5Etfw">Tweets by API_official</a> <script async="" charset="utf-8" src="https://platform.twitter.com/widgets.js"></script></div></div></div> </div>
</div> </div><!-- /row -->
<!-- map -->
</div>
</div>
<div class="pagetop">
<a aria-label="top" href="#top"><i class="icon icon-arrow-up2"></i></a>
</div>
<footer>
<div class="container">
<div class="row widgets">
<div class="col-sm-4" id="liquid_fb-2"><div class="widget widget_liquid_fb"> <div class="ttl">Facebook Page</div> <div class="fb-page" data-adapt-container-width="true" data-hide-cover="false" data-href="https://www.facebook.com/Asiapress-Official-296096067191240/" data-show-facepile="true" data-show-posts="false" data-small-header="false" data-width="500"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div></div>
</div></div> <div class="col-sm-4" id="text-3"><div class="widget widget_text"><div class="ttl">ã¢ã¯ã»ã¹</div> <div class="textwidget"><p><strong>âã¢ã¸ã¢ãã¬ã¹ï¼å¤§éªãªãã£ã¹ï¼</strong>ã <a href="https://www.google.com/maps/place/%E3%80%92530-0021+%E5%A4%A7%E9%98%AA%E5%BA%9C%E5%A4%A7%E9%98%AA%E5%B8%82%E5%8C%97%E5%8C%BA%E6%B5%AE%E7%94%B0%EF%BC%91%E4%B8%81%E7%9B%AE%EF%BC%92%E2%88%92%EF%BC%93+%E3%82%B5%E3%83%8C%E3%82%AB%E3%82%A4%E3%83%88%E3%83%93%E3%83%AB/@34.7090305,135.5080506,18z/data=!3m1!4b1!4m5!3m4!1s0x6000e6be8d1e52bd:0x454f473d6f70adf8!8m2!3d34.7090073!4d135.5091563?hl=ja" rel="noopener noreferrer" target="_blank">å°å³ï¼ï¼</a><br/>
ã530-0021<br/>
å¤§éªåºå¤§éªå¸ååºæµ®ç°1-2-3 ãµãã«ã¤ããã«305<br/>
TEL (06)6373-2444 FAX (06)6224-3226ãããosaka@asiapress.org</p>
<p><strong>âã¢ã¸ã¢ãã¬ã¹ï¼æ±äº¬ãªãã£ã¹ï¼</strong><br/>
ã101-0064<br/>
æ±äº¬é½åä»£ç°åºç¿æ¥½çº2-2-3 NSãã«202<br/>
TEL 070-3263-5097 tokyo@asiapress.org</p>
</div>
</div></div><div class="widget_text col-sm-4" id="custom_html-4"><div class="widget_text widget widget_custom_html"><div class="ttl">ã¢ã¸ã¢ãã¬ã¹ã®ã¦ã§ããµã¤ã</div><div class="textwidget custom-html-widget"><ul>
<li>â<a href="http://www.asiapress.org/apn/" rel="noopener noreferrer" target="_blank">ã¢ã¸ã¢ãã¬ã¹ã»ãããã¯ã¼ã¯ï¼æ¥æ¬èªï¼</a></li>
<li>â<a href="http://www.asiapress.org/korean/" rel="noopener noreferrer" target="_blank">ìììíë ì¤ ë¶íë³´ë ï¼éå½èªï¼</a></li>
<li>â<a href="http://www.asiapress.org/rimjin-gang/" rel="noopener noreferrer" target="_blank">Rimjiin-gangï¼è±èªï¼</a></li>
<li>â<a href="https://www.youtube.com/user/ASIAPRESSmovie/videos" rel="noopener noreferrer" target="_blank">YouTube ã¢ã¸ã¢ãã¬ã¹ãã£ã³ãã«</a></li>
<li>â<a href="http://asiapress.shop-pro.jp/" rel="noopener noreferrer" target="_blank">ã¢ã¸ã¢ãã¬ã¹æ¸ç±è²©å£²</a></li>
<li>â<a href="http://www.asiapress.org/">ã¢ã¸ã¢ãã¬ã¹å¬å¼ï¼å½ãµã¤ãï¼</a></li>
</ul>
</div></div></div> </div>
</div>
<div class="foot">
<div class="container com">
<a class="logo" href="http://www.asiapress.org/" title="ã¢ã¸ã¢ãã¬ã¹ã»ã¤ã³ã¿ã¼ãã·ã§ãã«">
<img alt="ã¢ã¸ã¢ãã¬ã¹ã»ã¤ã³ã¿ã¼ãã·ã§ãã«" src="http://www.asiapress.org/wp-content/uploads/2018/06/cropped-API_top_logo-1.jpg"/>
</a>
</div>
<div class="container sns">
<a href="https://www.facebook.com/Asiapress-Official-296096067191240/" target="_blank"><i class="icon icon-facebook"></i>
                Facebook</a>
<a href="https://twitter.com/API_official" target="_blank"><i class="icon icon-twitter"></i>
                Twitter</a>
<a href="http://www.asiapress.org/feed/index.xml"><i class="icon icon-feed2"></i> Feed</a> </div>
</div>
<div class="copy">
        (C)        2020        <a href="http://www.asiapress.org/">ã¢ã¸ã¢ãã¬ã¹ã»ã¤ã³ã¿ã¼ãã·ã§ãã«</a>. All rights reserved.        <!-- Powered by -->
<!-- /Powered by -->
</div>
</footer>
</div><!--/wrapper-->
<script id="liquid-script-js" src="http://www.asiapress.org/wp-content/themes/liquid-corporate/js/common.min.js?ver=5.5.3"></script>
<script id="page-links-to-js" src="http://www.asiapress.org/wp-content/plugins/page-links-to/dist/new-tab.js?ver=3.3.4"></script>
<script id="wp-embed-js" src="http://www.asiapress.org/wp-includes/js/wp-embed.min.js?ver=5.5.3"></script>
</body>
</html>
