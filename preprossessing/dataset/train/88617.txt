<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]--><!--[if IE 7]>
<html class="lt-ie9 lt-ie8"> <![endif]--><!--[if IE 8]>
<html class="lt-ie9"> <![endif]--><!--[if gt IE 8]><!--><html class="lang_en" lang="en" os="Linux"><!--<![endif]--><head>
<meta charset="utf-8"/>
<title>AirDroid | Delight Your Multi-Screen Life</title>
<meta content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1" name="viewport"/>
<meta content="airdroid://launch/main" property="al:android:url"/>
<meta content="com.sand.airdroid" property="al:android:package"/>
<meta content="AirDroid" property="al:android:app_name"/>
<meta content="website" property="og:type"/>
<meta content="AirDroid - Android on Computer" property="og:title"/>
<meta content="AirDroid" property="og:site_name"/>
<meta content="https://www.airdroid.com" property="og:url"/>
<meta content="Access Android phone/tablet from computer remotely and securely. Manage SMS, files, photos and videos, WhatsApp, Line, WeChat and more on computer." property="og:description"/>
<meta content="https://cdn1.airdroid.com/images/banner_v4.png" property="og:image"/>
<meta content="167747143358481" property="fb:app_id"/><meta content="windows android, pc client, mac android, web, texting on computer, android manager, notification, airmirror, file transfer, android on computer, control android, sms from computer" name="keywords"/><meta content="Access Android phone/tablet from computer remotely and securely. Manage SMS, files, photos and videos, WhatsApp, Line, WeChat and more on computer." name="description"/>
<link href="//cdn1.airdroid.com/www_asset_202012251340/img/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<script>
        if (navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
        ) {
            var uri = "https://m.airdroid.com" + window.location.search;
            if (navigator.userAgent.match(/Android/i)) {
                document.location = uri;
            } else {
                window.location.replace(uri);
            }
        }
    </script>
<link href="//cdn1.airdroid.com/www_asset_202012251340/css/main.min.css" rel="stylesheet"/>
<link href="//cdn1.airdroid.com/www_asset_202012251340/css/home.min.css" rel="stylesheet"/>
<script src="//cdn1.airdroid.com/www_asset_202012251340/js/framework.min.js"></script>
<script>
        if (util.browser.ie10) {
            document.documentElement.className += ' lt-ie11';
        }
    </script>
<script src="//cdn1.airdroid.com/www_asset_202012251340/lang/en.js"></script></head>
<body id="home">
<div class="mod-header i-mobile-hide">
<div class="item-header-wrap">
<div class="i-container i-clearfix">
<h1 class="item-logo">
<a class="item-logo-link" href="/en/">
<span class="item-logo-label">manage android via browser</span>
</a>
</h1>
<ul class="item-navs i-clearfix">
<li class="item-nav-item">
<a class="item-nav-link item-nav-item-selected" data-type="headerNav-goPersonal" href="/en/index.html">Personal</a>
</li>
<li class="item-nav-item">
<a class="item-nav-link" data-type="headerNav-goBusiness" href="/en/bizHome.html">Business</a>
</li>
<li class="item-nav-item">
<a class="item-nav-link" data-type="headerNav-goRs" href="/en/remotesupport.html">Remote Support</a>
</li>
<li class="item-nav-item">
<a class="item-nav-link link-price" data-type="headerNav-goPrice" href="/en/pricing/?init=biz">Pricing</a>
</li>
<li class="item-nav-item">
<a class="item-nav-link" data-type="headerNav-goDownLoadPage" href="/en/get.html">Download</a>
</li>
<li class="item-nav-item">
<a class="item-nav-link" data-type="headerNav-helpCenter" href="https://help.airdroid.com/" target="_blank">Help</a>
</li>
<li class="item-nav-item item-actions">
<a class="item-nav-link" data-type="headerNav-goSignup" href="/en/signup/">Sign up </a>
<span class="item-nav-link-delimiter"> / </span>
<a class="item-nav-link" data-type="headerNav-goSignin" href="/en/signin/">Sign in</a>
</li>
<li class="item-nav-item item-profile i-hide">
<div class="item-profile-info">
<a class="item-nav-link item-profile-link" href="/en/account/" id="profile_username_link"></a>
</div>
<div class="item-dropdown item-profile-actions">
<i class="item-dropdown-arrow-up"></i>
<ul class="item-dropdown-list">
<li class="item-dropdown-item item-profile-action-user-center">
<a href="/en/account/">User center</a>
</li>
<li class="item-dropdown-item item-dropdown-item-division"></li>
<li class="item-dropdown-item item-profile-action-signout">
<a href="javascript:;">Sign out</a>
</li>
</ul>
</div>
</li>
<li class="item-nav-item item-nav-item-round nav-item-last">
<a class="item-nav-link" data-type="headerNav-goWeb" href="javascript:;" id="go_web">AirDroid Web</a>
<a class="item-nav-link nav-item-left i-hide" data-type="headerNav-goContactUs" href="https://help.airdroid.com/hc/requests/new" id="go_contact_us" target="_blank">CONTACT US</a>
<a class="item-nav-link nav-item-right i-hide" data-type="headerNav-goPartnerProgram" href="/en/partnerProgram.html" id="go_partner">Partner Program</a>
</li>
</ul>
</div>
</div>
</div>
<div class="mode-header-position"></div>
<div class="mod-header i-mobile-visible">
<div class="item-header-wrap">
<div class="i-clearfix">
<h1 class="item-logo">
<a class="item-logo-link" href="javascript:;">
<span class="item-logo-label">manage android via browser</span>
</a>
</h1>
</div>
</div>
</div>
<div class="mod-home">
<div class="banner-new">
<div class="banner-new-content">
<h2>AirMirror</h2>
<h3>
            Your Remote Android in Hand
        </h3>
<p>
            AirMirror App which based on remote control technology. Provide convenient remote control and manage solutions on mobile device
        </p>
<a class="button" href="#go_download_airmirror">Download now</a>
</div>
</div>
<div id="home-swiper-banner">
<div class="swiper-container swiper-container-horizontal" id="home-swiper">
<div class="swiper-wrapper">
<div class="swiper-slide">
<div class="swiper-banner-0">
<div class="swiper-banner-0-content">
<h2>AirMirror</h2>
<h3>
                            Your Remote Android in Hand
                        </h3>
<p>
                            AirMirror App which based on remote control technology. Provide convenient remote control and manage solutions on mobile device
                        </p>
<a class="button button-primary" href="#go_download_airmirror">
                            Download now
                        </a>
</div>
</div>
</div>
<div class="swiper-slide">
<div class="swiper-banner-1">
<div class="swiper-banner-1-content">
<h2>AirDroid</h2>
<h3>Delight Your Multi-Screen Life</h3>
<p>AirDroid enables you to transfer files across devices, control mobile devices remotely, receive and reply to messages on computer.</p>
<a class="button" href="#go_download_airdroid">Download now</a>
</div>
</div>
</div>
<div class="swiper-slide">
<div class="swiper-banner-2">
<div class="swiper-banner-2-content">
<h2>AirDroid</h2>
<h3>Comprehensive Cross-Platform Support</h3>
<p>Enjoy the best experience anywhere anytime with the comprehensive and mature support for all major platforms.</p>
<a class="button" href="#go_download_airdroid">Download now</a>
</div>
</div>
</div>
<div class="swiper-slide">
<div class="swiper-banner-3">
<div class="swiper-banner-2-content">
<h2>AirDroid</h2>
<h3>Your Best Mobile Device Management Suite</h3>
<p>File transfer, remote control, SMS &amp; notifications on computer, they all just works.</p>
<a class="button" href="#go_download_airdroid">Download now</a>
</div>
</div>
</div>
</div>
<div class="swiper-pagination"></div>
<div class="swiper-button-next"></div>
<div class="swiper-button-prev"></div>
</div>
</div>
<div class="item-nav-cases">
<div class="item-wrap">
<ul class="item-nav-cases-group">
<li>
<a class="file-transfer" href="#file_transfer">
<span>File transfer</span>
</a>
</li>
<li>
<a class="remote-control" href="#remote_control">
<span>Remote control</span>
</a>
</li>
<li>
<a class="msg-notice" href="#msg_notice">
<span>SMS &amp; Notifications</span>
</a>
</li>
<li>
<a class="remote-camera" href="#remote_camera">
<span>Remote camera</span>
</a>
</li>
<li>
<a class="web-fast-operation" href="#web_fast_operation">
<span>Web app</span>
</a>
</li>
</ul>
</div>
</div>
<ul class="item-use-cases">
<li class="bg-gray">
<div class="item-wrap c-content">
<div class="title">
<h4>
                    We've launched AirMirror App which based on remote control technology
                </h4>
<p>
                    Remote control mobile devices directly by phone
                </p>
</div>
<div class="flex-txt">
<div class="txt-airMirror">
<div class="tips-left-airMirror">
<img src="//cdn1.airdroid.com/www_asset_202012251340/img/home/ic_game.png"/>
<div>
<h5>
                                AFK Assistance
                            </h5>
<p>
                                Completing tasks on time while playing your mobile games like a pro
                            </p>
</div>
</div>
<div class="tips-left-airMirror">
<img src="//cdn1.airdroid.com/www_asset_202012251340/img/home/ic_camera.png"/>
<div>
<h5>
                                Remote Monitor
                            </h5>
<p>
                                Checking the surrounding with device camera for guarding family safe
                            </p>
</div>
</div>
</div>
<div class="img-ariMirror" id="animation_airmirror"></div>
<div class="txt-airMirror">
<div class="tips-right-airMirror">
<img src="//cdn1.airdroid.com/www_asset_202012251340/img/home/ic_remote_support.png"/>
<div>
<h5>
                                Remote Support
                            </h5>
<p>
                                Solving phone's problem remotely for family by AirMirror
                            </p>
</div>
</div>
<div class="tips-right-airMirror">
<img src="//cdn1.airdroid.com/www_asset_202012251340/img/home/ic_remote_contral.png"/>
<div>
<h5>
                                Remote Control
                            </h5>
<p>
                                Solving the urgent business easily through the phone when youâre away from the office
                            </p>
</div>
</div>
</div>
</div>
<div class="button-download">
<a href="#go_download_airmirror">
                    Download now
                </a>
</div>
</div>
</li>
<li id="file_transfer">
<div class="item-wrap">
<div class="img">
<img src="//cdn1.airdroid.com/www_asset_202012251340/img/home/case_1.png"/>
</div>
<div class="txt">
<div class="txt-content">
<h4>
                        File transfer - Transfer files without cable
                    </h4>
<p>
                        Transfer files, photos, documents, music or APKs, all without a cable. You can even transfer folders and manage files remotely.
                    </p>
<a href="/en/pricing/" target="_blank">
                        Folder transfer is for Premium accounts only
                    </a>
</div>
</div>
</div>
</li>
<li class="bg-gray" id="remote_control">
<div class="item-wrap">
<div class="txt">
<div class="txt-content">
<h4>
                        Remote control - Control your devices anytime
                    </h4>
<p>
                        No matter it's local or remote network, you can use Remote Control to take complete control of your Android devices. Screen Mirroring allows you to present your device screen, doing game live streaming and demonstrating apps, etc. You can also use the remote keyboard to type on device using your computer keyboard.
                    </p>
<p class="desc">
                        Independent remote control App which support access mobile devices from the phone.
                    </p>
<a href="#go_download_airmirror">
                        Get AirMirror
                    </a>
</div>
</div>
<div class="img">
<img src="//cdn1.airdroid.com/www_asset_202012251340/img/home/case_2.png"/>
</div>
</div>
</li>
<li id="msg_notice">
<div class="item-wrap">
<div class="img">
<img src="//cdn1.airdroid.com/www_asset_202012251340/img/home/case_3.png"/>
</div>
<div class="txt">
<div class="txt-content">
<h4>
                        SMS &amp; Notifications - Never miss an important message during work
                    </h4>
<p>
                        The desktop notification feature allows you to mirror notifications of SMS, emails, app notifications (like WhatsApp, Kik, Line, etc.) to your computer and quickly reply them. No more need to check your phone, and never miss an important message.
                    </p>
</div>
</div>
</div>
</li>
<li class="bg-gray" id="remote_camera">
<div class="item-wrap">
<div class="txt">
<div class="txt-content">
<h4>
                        Remote Camera
                    </h4>
<p>
                        Start device camera remotely, monitor the environment around the device in real-time.
                    </p>
<a href="/en/pricing/" target="_blank">
                        Remote connection is for Premium accounts only
                    </a>
</div>
</div>
<div class="img">
<img src="//cdn1.airdroid.com/www_asset_202012251340/img/home/case_5.png"/>
</div>
</div>
</li>
<li id="web_fast_operation">
<div class="item-wrap">
<div class="img">
<img src="//cdn1.airdroid.com/www_asset_202012251340/img/home/case_4.png"/>
</div>
<div class="txt">
<div class="txt-content">
<h4>
                        Web app - Manage files on device via a web browser
                    </h4>
<p>
                        No need to install any client, no restriction to any system, you just need a web browser to enjoy all the AirDroid features to manage your devices. The best solution during travel or for managing devices on computer without the desktop client installed.
                    </p>
</div>
</div>
</div>
</li>
</ul>
<div class="item-use-accounts">
<ul>
<li>
<div class="use-accounts-content">
<h4>Free account</h4>
<p>Allows you to transfer files across devices, control mobile devices remotely, receive and reply to messages on computer. It fulfills the basic needs.</p>
<a class="button" href="/en/signup/">Sign up</a>
</div>
</li>
<li>
<div class="use-accounts-content vip">
<h4>Premium account</h4>
<p>More remote connection data quota, more devices under one account, folder transfer, and remove ads, etc. It's the ultimate AirDroid experience.</p>
<a href="/en/pricing/">View the list of all the Premium account features</a>
<a class="button" href="/en/pricing/">Upgrade to Premium</a>
</div>
</li>
</ul>
</div>
<div class="item-download-airdroid" id="download_airdroid3">
<div id="go_download_airdroid"></div>
<div class="item-wrap">
<h4 class="item-download-title">Download AirDroid</h4>
<ul class="item-download-group">
<li class="item-download-android">
<a class="airdroid-download-item" href="javascript:;" id="item-download-android">
                    Android
                </a>
</li>
<li class="item-download-ios">
<a class="airdroid-download-item" href="javascript:;" id="item-go-appstore">
                    iOS
                </a>
</li>
<li class="item-line">
<div class="type-dividing-line"></div>
</li>
<li class="item-download-windows">
<a class="airdroid-download-item" data-ext="win" href="javascript:;" id="item-download-windows">
                    Windows
                </a>
</li>
<li class="item-download-macOSX">
<a class="airdroid-download-item" data-ext="mac" href="javascript:;" id="item-download-mac">
                    Mac OS X
                </a>
</li>
<li class="item-download-chrome item-last-download">
<a href="javascript:;" id="item-go-web">
                    AirDroid Web
                </a>
</li>
</ul>
<div id="go_download_airmirror"></div>
<h4 class="item-download-title" style="margin-top: 350px">
            Download AirMirror
        </h4>
<ul class="item-download-group item-download-group-airmirror">
<li class="item-download-android-airmirror">
<a class="airdroid-download-item" href="javascript:;" id="item-download-android-airmirror">
                    Android
                </a>
</li>
<li class="item-download-ios-airmirror">
<span class="airdroid-download-item airdroid-download-ios-item">
                    iOS
                </span>
</li>
</ul>
</div>
</div>
<div class="item-pc-download">
<img alt="pic_download" src="//cdn1.airdroid.com/www_asset_202012251340/img/home/pic_download.png"/>
</div>
<div class="item-media-coverage item-wrap i-mobile-hide">
<ul class="item-media-coverage-list">
<li>
<img alt="pcworld" height="52" src="//cdn1.airdroid.com/www_asset_202012251340/img/home/pcworld.png" style="padding-bottom: 8px;"/>
<p> AirDroid 3 blurs device lines even further with new PC and Mac clients, Android screen mirroring. </p>
</li>
<li>
<img alt="pcworld" height="54" src="//cdn1.airdroid.com/www_asset_202012251340/img/home/bgr-black.png" style="padding-bottom: 6px;"/>
<p> AirDroid is an awesome app that has brought iOS 8 - like Continuity features to Android devices since long
            before Continuity ever existed. </p>
</li>
<li>
<img alt="pcworld" height="60" src="//cdn1.airdroid.com/www_asset_202012251340/img/home/lifehacker.png"/>
<p> AirDroid on the web is still as awesome as it always was, but the new desktop clients - available for both
            Windows and Mac - is pretty snazzy. </p>
</li>
</ul>
<a class="item-all-reviews i-mobile-hide" href="/en/reviews">All reviews &gt;</a>
</div>
<div class="mobile-copyright i-mobile-visible">Â© 2020 Sand Studio</div>
</div>
<div class="mod-footer i-clearfix i-mobile-hide" style="position: relative;">
<div class="item-wrap">
<ul class="item-site-nav">
<li class="item-nav-item item-airdroid-intro">
<dl>
<dt>About AirDroid</dt>
<dd>AirDroid makes your multi-screen life easier and more focused by helping you access and manage your phone from any computer, anywhere. You can send SMS, view app notifications, transfer files and fully control your phone on computer with AirDroid.</dd>
</dl>
</li>
<li class="item-nav-item">
<dl>
<dt>AirDroid</dt>
<dd><a data-type="footerNav-evolution" href="/en/download.html">Evolution</a></dd>
<dd><a data-type="footerNav-goPremium" href="/en/pricing/" target="_blank">Premium</a></dd>
<dd><a data-type="footerNav-eula" href="/en/legal/eula.html">EULA</a></dd>
<dd><a data-type="footerNav-privacy" href="/en/legal/privacy.html">Privacy Terms</a></dd>
<dd><a data-type="footerNav-paymentTerms" href="/en/legal/payment_terms.html">Payment Terms</a></dd>
</dl>
</li>
<li class="item-nav-item">
<dl>
<dt>Community</dt>
<dd><a data-type="footerNav-blog" href="https://blog.airdroid.com/" target="_blank">Blog</a></dd>
<dd>
<a class="item-help-en" data-type="footerNav-support" href="https://help.airdroid.com/" target="_blank">Help Center</a>
<a class="item-help-ja i-hide" data-type="footerNav-support" href="https://help.airdroid.com/" target="_blank">Help Center</a>
</dd>
</dl>
</li>
<li class="item-nav-item item-nav-item-last">
<dl>
<dt>About</dt>
<dd><a data-type="footerNav-about" href="/en/aboutUs.html">About us</a></dd>
<dd><a data-type="footerNav-partnership" href="/en/partnership.html">Partnership</a></dd>
<dd><a data-type="footerNav-Sponsorship" href="https://blog.airdroid.com/post/sponsorship-opportunities/" target="_blank">Sponsorship</a></dd>
<dd>Business Contact: <a href="mailto:sales@airdroid.com">sales@airdroid.com</a></dd>
<dd class="record" style="display: none;"><a href="http://www.beian.miit.gov.cn" rel="nofollow noopener noreferrer" target="_blank">é½ICPå¤13005846å·-2</a></dd>
</dl>
</li>
</ul>
<div class="item-social">
<ul class="item-social-list">
<li class="item-social-item">
<a class="item-social-icon item-social-icon-twitter" data-type="footerNav-goTwitter" href="https://twitter.com/#!/AirDroidTeam" rel="nofollow noopener noreferrer" target="_blank">twitter</a>
</li>
<li class="item-social-item">
<span class="item-social-splitter">/</span>
<a class="item-social-icon item-social-icon-facebook" data-type="footerNav-goFacebook" href="https://www.facebook.com/AirDroid" rel="nofollow noopener noreferrer" target="_blank">facebook</a>
</li>
<li class="item-social-item">
<span class="item-social-splitter">/</span>
<a class="item-social-icon item-social-icon-youtube" data-type="footerNav-goYoutube" href="https://www.youtube.com/user/AirDroidTeam" rel="nofollow noopener noreferrer" target="_blank">youtube</a>
</li>
<li class="item-social-item item-social-item-weixin">
<span class="item-social-splitter">/</span>
<a class="item-social-icon item-social-icon-weixin" data-type="footerNav-goWeixin" href="javascript:;">weixin</a>
<img class="item-weixin-block" src="//cdn1.airdroid.com/www_asset_202012251340/img/home/pic_qrcode.png"/>
</li>
</ul>
<p class="item-copyright">Â© 2020 Sand Studio</p>
<div class="item-lang-change">
<div class="item-nav-item-lang-container">
<a class="item-nav-item-lang" href="javascript:;">English</a><span class="item-nav-item-arrow"></span>
</div>
<div class="item-dropdown item-dropdown-lang">
<ul class="item-dropdown-list">
<li class="item-dropdown-item">
<a class="item-choose-lang" data-lang="en" href="javascript:;">English</a>
</li>
<li class="item-dropdown-item item-dropdown-item-division"></li>
<li class="item-dropdown-item">
<a class="item-choose-lang" data-lang="de" href="javascript:;">Deutsch</a>
</li>
<li class="item-dropdown-item item-dropdown-item-division"></li>
<li class="item-dropdown-item">
<a class="item-choose-lang" data-lang="es-ES" href="javascript:;">EspaÃ±ol</a>
</li>
<li class="item-dropdown-item item-dropdown-item-division"></li>
<li class="item-dropdown-item">
<a class="item-choose-lang" data-lang="fr" href="javascript:;">FranÃ§ais</a>
</li>
<li class="item-dropdown-item item-dropdown-item-division"></li>
<li class="item-dropdown-item">
<a class="item-choose-lang" data-lang="it" href="javascript:;">Italiano</a>
</li>
<li class="item-dropdown-item item-dropdown-item-division"></li>
<li class="item-dropdown-item">
<a class="item-choose-lang" data-lang="pt-BR" href="javascript:;">PortuguÃªs(BR)</a>
</li>
<li class="item-dropdown-item item-dropdown-item-division"></li>
<li class="item-dropdown-item">
<a class="item-choose-lang" data-lang="ja" href="javascript:;">æ¥æ¬èª</a>
</li>
<li class="item-dropdown-item item-dropdown-item-division"></li>
<li class="item-dropdown-item">
<a class="item-choose-lang" data-lang="zh-CN" href="javascript:;">ç®ä½ä¸­æ</a>
</li>
<li class="item-dropdown-item item-dropdown-item-division"></li>
<li class="item-dropdown-item">
<a class="item-choose-lang" data-lang="zh-TW" href="javascript:;">ç¹é«ä¸­æ</a>
</li>
<li class="item-dropdown-item item-dropdown-item-division"></li>
<li class="item-dropdown-item">
<a class="item-choose-lang" data-lang="pt-PT" href="javascript:;">PortuguÃªs(PT)</a>
</li>
<li class="item-dropdown-item item-dropdown-item-division"></li>
<li class="item-dropdown-item">
<a class="item-choose-lang" data-lang="ru" href="javascript:;">ÑÑÑÑÐºÐ¸Ð¹</a>
</li>
<li class="item-dropdown-item item-dropdown-item-division"></li>
</ul>
<i class="item-dropdown-arrow-down"></i>
</div>
</div>
</div>
</div>
<div class="back-top">
<img src="//cdn1.airdroid.com/www_asset_202012251340/img/dyn_top_hover.png"/>
</div>
</div>
<div class="mod-footer i-clearfix i-mobile-visible" style="position: relative;">
<p>Â© 2020 Sand Studio</p>
</div>
<div class="mode-cookie-tip i-hide">
    We use cookies to ensure you get the best experience. By using our website you agree to our <a class="cookie-tip-link" href="/en/legal/privacy.html">Privacy Policy</a>.
    <span class="cookie-tip-close"></span>
</div>
<script src="//cdn1.airdroid.com/www_asset_202012251340/js/swiper.min.js"></script>
<script src="//cdn1.airdroid.com/www_asset_202012251340/js/basedownload.min.js"></script>
<script src="//cdn1.airdroid.com/www_asset_202012251340/js/home.min.js"></script>
</body></html>