<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "66321",
      cRay: "61109ae91e791228",
      cHash: "94451626994cf25",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmVpY2h0aGF1cy5jb20vP2g9cmFuZG9t",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "uNZdCBM3+tU1Pm3UPYVfeI3NOwcNcT0e0c2xBQwB3hgIaIDQOJXVT5zCEp8X75QxYBscHBaYhiIurLKTPW4eyW8AebFR288AbHP2wnDJBq2IRRtKOBIBqaU2VjUEoxhngKyVNwqCsupab6BL4fPBtoihLegRreFufTsxxJP0/NUofZLFSVwYO7qVtFtuA0B7TzcbJqUZE1OAbH7QS0AA4zbqhUsxI56GptRrsmRBnVv3vlzYy8aLG6/FGPXsxQUKQqbD14Avu4/bN0pIbCtrXnQkKTd9cEuDQWpEYw0QQEICGYi+eOT8dVMdoQ8yoJekqZAeREE+pNgNK5oviM3XsB4/WmPUCwpvMTsmJlEeSZDn8AYtP/yd3wlf3RqY5hL32SMPVEABSI6uKPfn2swG4VpmFnYSV2COpj6EuLI4YZezPHo0QFyVTkM9w08Fq1syyvLzdfYjwtbQgPJGD3xfQ7qREdj48ep7iOU7nGPIIYxOF0ocek+nq1sCAw5PKE93QU1rHIbmvWtx+rsgufxVdSyzMdtJ6Lv6CDBpy1pFhQkik9Bxa576vIXOjOUiHXmdUWknyxnMjuO+TtVkuEuA5xVzO3PesKshx4vttDkn4eHK9M7dTSMqmHoFL71ErApEHdCLzDpfU0G5KCzrVfZcG3tRulJkJldzPbT+dQf66kosKF7jRFiNLKCnyN3w59zwAS7dJyq7HItx9y8mZvYEfiMTRR094dsxifEbogmQEluq+EGUmFHce8tg7mMe5Yfp",
        t: "MTYxMDU1NjUwOS42MTkwMDA=",
        m: "b7QiB5eDPMiS9KHWs0uqtuDcIE9iXX2fqVmas8AvGZw=",
        i1: "6DEILFCtETtCSkopuoN6Ag==",
        i2: "P/tNwJ8xEkLWsLoYVT9s5g==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "sx9ZMh6KEnIQetYrxMKt0OpCpeXTf1+fOFNkffhEWsM=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.beichthaus.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?h=random&amp;__cf_chl_captcha_tk__=e2c761a13784cf9387790eb5c45ee24ac6ece7c8-1610556509-0-AZtZI9KVgE2-p-Twtl2_zrlrrdhDNnmoZamkFHhMIF41Ufu71DjXS1sC2mTVK2MnhZaNiRRtXm6e9CSVWRlQM4ltzNrtlYNsh1g40H_vRWFou6aLSTKk4oYBSH44KZcXWU9HjtgzZ81Rv8eFzBtCtqJ0ScLB6P-BB_AdjOjba64SYqUk6Y9rKYIJ4nISfnWYGJvghEc9-g-QKWjJIdgEe9qUpU7EDweBf16Zgfhz_IV2TAcpW4u19rgARm-jC8QCWpo6JTyHV0qNLtNhFUOwp2qmCipjfD0SKCZpW1-Hxgkw8RXDYvmIi-TJAizzhbcI6SkPawl-sIYhKiVAZl1zUHI4jkCHsngkqEDkD4LZtcXgmf6KhftwIlePZr79EWSot8wkG0ORuRMgAwp25BJeFH5V9WXuBUZsg9IdyMJIKh9_mMCI3AyeqtXfIZt3_YfDc0BJHsEdY8VFyiA4nUFtnLownD1AGikmxuI6220ryzf-uy8Qf4gWxfbTfhPaONWo8iK2Cw6AxYoj_XMyNdZGxCDaQeDpVxQckEp7eK_2Wnji" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="11322866fb3f8e25950b351a08b3d031325d8075-1610556509-0-AZ1gz3Da/wd+oq8EGNyFe8h1mFkOVHDy95N5VnWgz7zCs0oMMTPIuoqv6sU/PvWbbUfVa+M5vehrBHObbSN9Nk7qU9RWC6GFvJgv5O1h38PceR9H7kPT2eZcZAkC/RTOPzf1HHLcCEECXq6WIbxO5A7UiJdRa5F8+g+SBTi60XNm5jVvOaHxpfXzTzIB9gv/sE6VnkUokO+gSlswzpP6peGwHv7+ohbTxIANDLphXfKVaquFHA0EQbiDTL51Tp4+YfNiTFGcVyJn7x/qvCKuN8q1+c9oorRmYdaS65Hzl7C1amaiNxot6ezae0iMBkg5YpuhGjTHgu+ttRvQb0ihK02KlPPVHk0kO9dnLp5r1RFIcYoFeMCuknnalCd4jyBsGOMlm8nOcZU5XxQRUbgLyZhbtofF2wTvGTflI6rRLkeE7o+/jEaQpDQi5VrCt1IylXFLqjhI+eMXmJ9K4ggiYvgIP1R9hHQa/uZw7LFr/1wvJU4Z2+Gpwg+3RRpy9SO7gJEUOEh+xnN3p49YPDH8T4JuKHlmQziB7a6QmH2YXPEepSQEAvfEBh1Gu9iI3NOtS4VWe4d03sQiJjT+ivqwsTI6hvB3X44p0mIfLMA4O6xcUw/+QwghfrPDStRec/jtlm3lY3XGLS7f6fpG3NRIuM5V4fim6l59unsnVwUI9ePL9UwqWitd+uTb8XVdFVlD0vmS/JZnqSeVh7nrnASGrrXx8UAvNZtXt5uRzar7isnKvLUUAxx/fj1cFTBYP8/yajQjiZh3CUL40G4UIMrj1A45wlpnLq8D41of1R/9Unqm74EMxrZjfovTd0EsFL1MjkbtSPTkRf1vaSAqrPX8wmcv88xarOAXaptfPohv/RH0oxEB5Xn2YLlTsQnnN2GX0sDtygsW+0k2NvEydkXnQKx/ISeh0KQkH8m9Dyp97knoyR7vkpjhonoN+MgP/5i/uHQqBDkS2bSxl2aD+/13NLqaG6qcXuo1t6rWJ0Zsenb3I0iAowRSnP8UeZ3/61OH6cBa31qpv9Fap1AKLp6XnzK8YuMJn20qjCZJR2o8KbyZBJv8bEKp6reAeRtCs158sA7TQxmqABVutIR8vU/UIncgfwL700IjsS6HQpcQjrt+/UjDH+fB3SfQ24QGmQ3M8x7Z2T+CEgFwXr3wbm9wVFRVcqeyOVJTSkLJUZtCvkOfMVhZyef36gsi+GYBY3dZRw=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="e298d467e16760a4c968ee420cf8c209"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=61109ae91e791228')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">61109ae91e791228</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:8d2b:d30c:bf6f:658f</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
