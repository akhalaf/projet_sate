<!DOCTYPE html>
<html style="background: rgba(2, 1, 1, 0.03);">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Cat loves pizza 😻🍕.ws - Emoji URL Shortener</title>
<meta content="URL shortener like bitly, but with emojis. We convert your boring text based URL into interesting emoji URLs!" name="Description"/>
<meta content="emoji url shortener" name="Keywords"/>
<meta content="ALL" name="robots"/>
<meta content="vqoY-AjFHwfnae1myjVz82fF5b92lOMY1z_JAxM7jko" name="google-site-verification"/>
<meta content="Cat loves pizza 😻🍕.ws - Emoji URL Shortener" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="URL shortener like bitly, but with emojis. We convert your boring text based URL into interesting emoji URLs!" property="og:description"/>
<meta content="https://😻🍕.ws" property="og:url"/>
<meta content="https://xn--vi8hl0c.ws/img/emojilink-logo.png" property="og:image"/>
<meta content="width=device-width" name="viewport"/>
<link href="https://😻🍕.ws" rel="canonical"/>
<link href="https://xn--vi8hl0c.ws/img/emojilink-icon.ico" rel="shortcut icon"/>
<link href="theme/default/uikit-2.26.4/css/uikit.almost-flat.min.css" rel="stylesheet"/>
<link href="theme/default/uikit-2.26.4/css/components/placeholder.almost-flat.min.css" rel="stylesheet"/>
<link href="theme/default/uikit-2.26.4/css/components/form-file.almost-flat.min.css" rel="stylesheet"/>
<link href="theme/default/uikit-2.26.4/css/components/notify.almost-flat.min.css" rel="stylesheet"/>
<link href="theme/default/uikit-2.26.4/css/customize.css" rel="stylesheet"/>
<link href="theme/default/fa-4.6.3/css/font-awesome.min.css" rel="stylesheet"/>
<link href="theme/default/emoji-picker-gh-pages/lib/css/nanoscroller.css" rel="stylesheet"/>
<link href="theme/default/emoji-picker-gh-pages/lib/css/emoji.css" rel="stylesheet"/>
<script src="theme/default/jquery/jquery-3.1.0.min.js"></script>
<script src="theme/default/uikit-2.26.4/js/uikit.min.js"></script>
<script src="theme/default/uikit-2.26.4/js/components/autocomplete.min.js"></script>
<script src="theme/default/uikit-2.26.4/js/components/notify.min.js"></script>
<script src="theme/default/emoji-picker-gh-pages/lib/js/nanoscroller.min.js"></script>
<script src="theme/default/emoji-picker-gh-pages/lib/js/tether.min.js"></script>
<script src="theme/default/emoji-picker-gh-pages/lib/js/config.js"></script>
<script src="theme/default/emoji-picker-gh-pages/lib/js/util.js"></script>
<script src="theme/default/emoji-picker-gh-pages/lib/js/jquery.emojiarea.js"></script>
<script src="theme/default/emoji-picker-gh-pages/lib/js/emoji-picker.js"></script>
<script src="https://cdn.jsdelivr.net/clipboard.js/1.5.13/clipboard.min.js"></script>
<style>
            html{
                font-size:100%;
            }
            h2 {
                font-size:180%;
            }
            @media(max-width:480px){
                html{
                    font-size: 130%;
                    line-height: 130%;
                }
                h2 {
                    font-size:130%;
                    line-height: 130%;
                }
            }
        </style>
<script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-90236088-1', 'auto');
          ga('send', 'pageview');
          
        </script>
<!-- Facebook Pixel Code -->
<script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1762237440773651'); // Insert your pixel ID here.
        fbq('track', 'PageView');
        </script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=1762237440773651&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
</head>
<body style="background: rgba(245, 245, 245, 0.1);min-width:520px;">
<div class="uk-container uk-container-center" style="padding-top: 20px;">
<div class="uk-grid">
<div class="uk-width-1-1 uk-text-center" style="padding-top:15px;">
<img alt="cat-emoji" height="160" src="img/cat-hearted-eyes.png" width="160"/>
<img alt="pizza-emoji" height="160" src="img/pizza.png" width="160"/>
</div>
<div class="uk-width-1-1 uk-text-center" style="padding-top:15px;">
<h1>Cat loves pizza <a href="https://😻🍕.ws" title="Cat loves pizza emoji URL shortener">😻🍕.ws</a> emoji URL shortener</h1>
<h2>URL shortener like bitly, but with emojis</h2>
</div>
<div class="uk-width-small-1-1 uk-width-medium-8-10 uk-width-large-3-5 uk-container-center" style="padding-top:15px;">
<form class="uk-form uk-form-stacked">
<div class="uk-form-row">
<label class="uk-form-label uk-text-center" for="autolink">Link to be shorten</label>
<div class="uk-form-controls">
<input autofocus="" class="uk-width-1-1 uk-form-large" id="autolink" name="link" placeholder="Paste link here 😛😛😛" type="text"/>
</div>
</div>
<div class="uk-form-row">
<div class="uk-form-controls">
<input checked="" id="old-devices-compatible" type="checkbox"/>
<label for="old-devices-compatible"><strong>Take care of older devices in auto picking emoji link <br/>(see <a href="https://emojilink.me/emoji-link/how-to-take-care-of-older-devices-in-auto-generating-emoji-link" target="_blank" title="How to take care of older devices in auto generating emoji link">How to take care of older devices in auto picking emoji link</a>)</strong></label>
</div>
</div>
<div class="uk-form-row">
<div class="uk-form-controls">
<input checked="" id="no-unhappy-regional" type="checkbox"/>
<label for="no-unhappy-regional"><strong>Do not use <a href="https://emojilink.me/emoji-link/unhappy-negative-regional-emojis" target="_blank" title="unhappy, negative and regional emojis">unhappy, negative and regional emojis</a> in auto generating emoji link</strong></label>
</div>
</div>
<div class="uk-form-row">
<label class="uk-form-label uk-text-center" for="customizelink" style="color:#5b23ff;">Click GO! to auto pick emoji link<br/>-- OR --<br/>Choose my own emojis (Optional)</label>
<div class="uk-form-controls">
<div class="uk-grid" style="margin-left:0;width:105%;">
<input class="uk-width-1-1 uk-form-large" data-emoji-input="unicode" data-emojiable="true" id="customizelink" name="customemojis" placeholder="Choose emojis here 😊😊😊" style="width:95%;" type="text"/>
</div>
</div>
</div>
<div class="uk-form-row">
<div class="uk-form-controls">
<button class="uk-button uk-button-large uk-button-success uk-width-1-1" disabled="" id="go" style="font-weight:bold;">GO!</button>
</div>
</div>
<div class="uk-form-row">
<div class="uk-form-controls">
<label>Seek help? Please visit our <a href="https://emojilink.me/emoji-link-faq" title="emojilink-faq">FAQ</a></label>
</div>
</div>
</form>
</div>
<div class="uk-width-1-1 uk-text-center" id="loading" style="padding-top:20px;display:none;">
<i class="fa fa-circle-o-notch fa-spin fa-5x fa-fw"></i>
</div>
<div class="uk-width-1-1" id="results" style="display:none;">
<div class="mw-visible-xlarge uk-width-small-1-1 uk-width-medium-8-10 uk-width-large-3-5 uk-container-center" id="desktop-results" style="padding-top:15px;">
<form class="uk-form uk-form-stacked">
<div class="uk-form-row">
<div class="uk-form-controls">
<label class="uk-form-label uk-text-center" style="display:inline-block;width:54%;">Your new link</label>
<label style="display:inline-block;width:14%;"></label>
<label class="uk-form-label uk-text-center" style="display:inline-block;width:30%;">Share your link</label>
</div>
</div>
<!-- with emoji domain -->
<div class="uk-form-row">
<div class="uk-form-controls">
<div class="uk-grid" style="margin-left:0;">
<input class="uk-form-large" id="generated-ed-link" style="width:54%;" type="text"/>
<button class="uk-button uk-button-large uk-button-primary" data-clipboard-text="" style="margin-left:1%;width:14%;" type="button"><i class="uk-icon-justify uk-icon-copy"></i> Copy</button>
<div class="uk-text-center my-share-link" style="color:#FFF;">
<a data-share-google-ed="" href="https://plus.google.com/share?url=https%3A//xn--vi8hl0c.ws/" target="_blank"><i class="uk-icon-google-plus-square uk-icon-large uk-margin-small-right"></i></a>
<a data-share-fb-ed="" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//xn--vi8hl0c.ws/" target="_blank"><i class="uk-icon-facebook-square uk-icon-large uk-margin-small-right"></i></a>
<a data-share-linkedin-ed="" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A//xn--vi8hl0c.ws/&amp;title=URL%20shortener%20like%20bitly,%20but%20with%20emojis&amp;summary=&amp;source=" target="_blank"><i class="uk-icon-linkedin-square uk-icon-large uk-margin-small-right"></i></a>
<a data-share-twitter-ed="" href="https://twitter.com/home?status=https%3A//xn--vi8hl0c.ws/" target="_blank"><i class="uk-icon-twitter-square uk-icon-large uk-margin-small-right"></i></a>
</div>
</div>
</div>
</div>
<div class="uk-form-row" style="margin-top:60px;">
<label class="uk-form-label">If above emoji domain not working in your social website, use below instead:<br/><a href="https://emojilink.me/emoji-link-faq#emoji-hyperlink-in-social-page" target="_blank">How to copy and paste the emoji hyperlink into my social page?</a></label>
</div>
<!-- with traditional domain -->
<div class="uk-form-row">
<div class="uk-form-controls">
<div class="uk-grid" style="margin-left:0;">
<input class="uk-form-large" id="generated-td-link" style="width:54%;" type="text"/>
<button class="uk-button uk-button-large uk-button-primary" data-clipboard-text="" style="margin-left:1%;width:14%;" type="button"><i class="uk-icon-justify uk-icon-copy"></i> Copy</button>
<div class="uk-text-center my-share-link" style="color:#FFF;">
<a data-share-google-td="" href="https://plus.google.com/share?url=https%3A//xn--vi8hl0c.ws/" target="_blank"><i class="uk-icon-google-plus-square uk-icon-large uk-margin-small-right"></i></a>
<a data-share-fb-td="" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//xn--vi8hl0c.ws/" target="_blank"><i class="uk-icon-facebook-square uk-icon-large uk-margin-small-right"></i></a>
<a data-share-linkedin-td="" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A//xn--vi8hl0c.ws/&amp;title=URL%20shortener%20like%20bitly,%20but%20with%20emojis&amp;summary=&amp;source=" target="_blank"><i class="uk-icon-linkedin-square uk-icon-large uk-margin-small-right"></i></a>
<a data-share-twitter-td="" href="https://twitter.com/home?status=https%3A//xn--vi8hl0c.ws/" target="_blank"><i class="uk-icon-twitter-square uk-icon-large uk-margin-small-right"></i></a>
</div>
</div>
<label class="uk-form-label uk-margin-small-top">Still not work? Click <a href="" id="url-encode"><i class="uk-icon-refresh uk-icon-small"></i></a> to copy and paste again</label>
</div>
</div>
</form>
</div>
<div class="mw-hidden-xlarge uk-width-small-1-1 uk-width-medium-8-10 uk-width-large-3-5 uk-container-center" id="mobile-results" style="padding-top:15px;">
<form class="uk-form uk-form-stacked">
<!-- with emoji domain -->
<div class="uk-form-row">
<label class="uk-form-label">Your new link</label>
<div class="uk-form-controls">
<input class="uk-width-7-10 uk-form-large" id="generated-ed-link-m" type="text"/>
<button class="uk-button uk-button-large uk-button-primary" data-clipboard-text="" style="margin-left:1%;width:25%;" type="button"><i class="uk-icon-justify uk-icon-copy"></i> Copy</button>
</div>
</div>
<div class="uk-form-row">
<label class="uk-form-label">Share your link</label>
<div class="uk-form-controls">
<div class="my-share-link" style="color:#FFF;">
<a data-share-google-ed="" href="https://plus.google.com/share?url=https%3A//xn--vi8hl0c.ws/" target="_blank"><i class="uk-icon-google-plus-square uk-icon-large uk-margin-small-right"></i></a>
<a data-share-fb-ed="" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//xn--vi8hl0c.ws/" target="_blank"><i class="uk-icon-facebook-square uk-icon-large uk-margin-small-right"></i></a>
<a data-share-linkedin-ed="" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A//xn--vi8hl0c.ws/&amp;title=URL%20shortener%20like%20bitly,%20but%20with%20emojis&amp;summary=&amp;source=" target="_blank"><i class="uk-icon-linkedin-square uk-icon-large uk-margin-small-right"></i></a>
<a data-share-twitter-ed="" href="https://twitter.com/home?status=https%3A//xn--vi8hl0c.ws/" target="_blank"><i class="uk-icon-twitter-square uk-icon-large uk-margin-small-right"></i></a>
</div>
</div>
</div>
<div class="uk-form-row" style="margin-top:60px;">
<label class="uk-form-label">If above emoji domain not working in your social website, use below instead:<br/><a href="https://emojilink.me/emoji-link-faq#emoji-hyperlink-in-social-page" target="_blank">How to copy and paste the emoji hyperlink into my social page?</a></label>
</div>
<!-- with traditional domain -->
<div class="uk-form-row">
<label class="uk-form-label">Your new link</label>
<div class="uk-form-controls">
<input class="uk-width-7-10 uk-form-large" id="generated-td-link-m" type="text"/>
<button class="uk-button uk-button-large uk-button-primary" data-clipboard-text="" style="margin-left:1%;width:25%;" type="button"><i class="uk-icon-justify uk-icon-copy"></i> Copy</button>
</div>
</div>
<div class="uk-form-row">
<label class="uk-form-label">Share your link</label>
<div class="uk-form-controls">
<div class="my-share-link" style="color:#FFF;">
<a data-share-google-td="" href="https://plus.google.com/share?url=https%3A//xn--vi8hl0c.ws/" target="_blank"><i class="uk-icon-google-plus-square uk-icon-large uk-margin-small-right"></i></a>
<a data-share-fb-td="" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//xn--vi8hl0c.ws/" target="_blank"><i class="uk-icon-facebook-square uk-icon-large uk-margin-small-right"></i></a>
<a data-share-linkedin-td="" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A//xn--vi8hl0c.ws/&amp;title=URL%20shortener%20like%20bitly,%20but%20with%20emojis&amp;summary=&amp;source=" target="_blank"><i class="uk-icon-linkedin-square uk-icon-large uk-margin-small-right"></i></a>
<a data-share-twitter-td="" href="https://twitter.com/home?status=https%3A//xn--vi8hl0c.ws/" target="_blank"><i class="uk-icon-twitter-square uk-icon-large uk-margin-small-right"></i></a>
</div>
<label class="uk-form-label uk-margin-small-top">Still not work? Click <a href="" id="url-encode-m"><i class="uk-icon-refresh uk-icon-small"></i></a> to copy and paste again</label>
</div>
</div>
</form>
</div>
</div>
<div class="uk-width-1-1 uk-text-center my-share-link-bottom" style="color:#FFF;padding-top:50px;">
<a href="https://plus.google.com/share?url=https%3A//emojilink.me/" target="_blank"><i class="uk-icon-google-plus-square uk-icon-large uk-margin-small-right" style="font-size:500%;"></i></a>
<a href="https://www.facebook.com/sharer/sharer.php?u=https%3A//emojilink.me/" target="_blank"><i class="uk-icon-facebook-square uk-icon-large uk-margin-small-right" style="font-size:500%;"></i></a>
<a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A//emojilink.me/&amp;title=URL%20shortener%20like%20bitly,%20but%20with%20emojis&amp;summary=&amp;source=" target="_blank"><i class="uk-icon-linkedin-square uk-icon-large uk-margin-small-right" style="font-size:500%;"></i></a>
<a href="https://twitter.com/home?status=https%3A//emojilink.me/" target="_blank"><i class="uk-icon-twitter-square uk-icon-large uk-margin-small-right" style="font-size:500%;"></i></a>
</div>
<div class="uk-width-1-1 uk-text-center" style="padding-top:15px;">
<a href="https://emojilink.me" title="Cat loves pizza 😻🍕.ws Emoji URL Shortener">English</a> | <a href="https://emojilink.me/hk" title="Hong Kong Cat loves pizza 😻🍕.ws Emoji URL Shortener">Hong Kong</a> | <a href="https://emojilink.me/tw" title="Taiwan Cat loves pizza 😻🍕.ws Emoji URL Shortener">Taiwan</a> | <a href="https://emojilink.me/kr" title="Korea Cat loves pizza 😻🍕.ws Emoji URL Shortener">Korea</a>
</div>
<div class="uk-width-1-1 uk-text-center" style="padding-top:15px;">
<p class="uk-margin-bottom">
                        by <a href="/cdn-cgi/l/email-protection#c5ada0a9a9aa85a0a8aaafaca9acabaeeba8a0">Steven</a> | <a href="https://emojilink.me/emoji-link-faq" title="emojilink-faq">FAQ</a> | <a href="https://emojilink.me/privacy-policy" title="privacy-policy">Privacy</a> | <a href="https://emojilink.me/terms-of-service" title="terms-of-service">TOS</a> | <a href="https://emojilink.me/emoji-link/emoji-keyboard" title="emoji keyboard">Emoji keyboard</a><br/>
                        Cat loves pizza emoji URL shortener 2018
                    </p>
</div>
</div>
</div>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>
            $(function() {
              // Initializes and creates emoji set from sprite sheet
              window.emojiPicker = new EmojiPicker({
                emojiable_selector: '[data-emojiable=true]',
                assetsPath: 'https://emojilink.me/theme/default/emoji-picker-gh-pages/lib/img/',
                popupButtonClasses: 'fa fa-smile-o'
              });
              // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
              // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
              // It can be called as many times as necessary; previously converted input fields will not be converted again
              window.emojiPicker.discover();
              
                // emoji class modifier
                $('.emoji-wysiwyg-editor').css({'width': '95%',
                                                'background-color': 'white'
                });
                $('.emoji-picker-icon').css({'position': 'relative',
                                           'font-size': '25px',
                                           'padding-left': '2px',
                                           'right': '30px'
                });
                
                // Copy button
                var clipboard = new Clipboard('button[data-clipboard-text]');

                clipboard.on('success', function(e) {
                    UIkit.notify("<i class=\"uk-icon-check\"></i> Link copied", {status:'info', timeout:'1000'});
                });
                
                $("#autolink").on('keydown paste input change', function (e) {
                    if ($("#autolink").val().match(/^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/i) !== null) {
                        $("#go").prop('disabled', false);
                    } else {
                        $("#go").prop('disabled', true);
                        var len = $("#autolink").val().length;
                        setTimeout(function() {
                            if ($("#autolink").val() != '' && len == $("#autolink").val().length) {
                                switch (true) {
                                    case ($("#autolink").val().match(/^h$/i) !== null):
                                    break;
                                    case ($("#autolink").val().match(/^ht$/i) !== null):
                                    break;
                                    case ($("#autolink").val().match(/^htt$/i) !== null):
                                    break;
                                    case ($("#autolink").val().match(/^http$/i) !== null):
                                    break;
                                    case ($("#autolink").val().match(/^https?$/i) !== null):
                                    break;
                                    case ($("#autolink").val().match(/^https?:$/i) !== null):
                                    break;
                                    case ($("#autolink").val().match(/^https?:\/$/i) !== null):
                                    break;
                                    case ($("#autolink").val().match(/^https?:\/\/$/i) !== null):
                                    break;
                                    case ($("#autolink").val().match(/^https?:\/\//i) === null):
                                        $("#autolink").val('http://'+$("#autolink").val());
                                        $("#autolink").trigger('change');
                                    break;
                                }
                            }
                        }, 500);
                    }
                });
                
                $("#go").click(function(e) {
                    e.preventDefault();
                    $("#go").prop('disabled', true);
                    if ($("#results").is(':visible')) {
                        $("#results").hide();
                    }
                    $("#loading").show();
                    $.ajax({
                        url: 'https://emojilink.me/getlink',
                        dataType: 'json',
                        type: 'POST',
                        data: {link: $("#autolink").val(), olddevicescompatible: $("#old-devices-compatible").prop('checked'), nounhappyregional: $("#no-unhappy-regional").prop('checked'), customemojis: $("#customizelink").val()},
                        success: function (res) {
                            $("#loading").hide();
                            if (res.result === 'ok') {
                                $("#generated-ed-link").val(res.link.ed_link);
                                $("#generated-ed-link").next().attr('data-clipboard-text', res.link.ed_link);
                                $("#generated-td-link").val(res.link.td_link);
                                $("#generated-td-link").next().attr('data-clipboard-text', res.link.td_link);
                                $("#generated-ed-link-m").val(res.link.ed_link);
                                $("#generated-ed-link-m").next().attr('data-clipboard-text', res.link.ed_link);
                                $("#generated-td-link-m").val(res.link.td_link);
                                $("#generated-td-link-m").next().attr('data-clipboard-text', res.link.td_link);
                                
                                $("a[data-share-google-ed]").attr('href', 'https://plus.google.com/share?url=https%3A//xn--vi8hl0c.ws/'+res.link.emojis);
                                $("a[data-share-fb-ed]").attr('href', 'https://www.facebook.com/sharer/sharer.php?u=https%3A//xn--vi8hl0c.ws/'+res.link.emojis);
                                $("a[data-share-linkedin-ed]").attr('href', 'https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A//xn--vi8hl0c.ws/' + res.link.emojis + '&amp;title=&amp;summary=&amp;source=');
                                $("a[data-share-twitter-ed]").attr('href', 'https://twitter.com/home?status=https%3A//xn--vi8hl0c.ws/'+res.link.emojis);
                                
                                $("a[data-share-google-td]").attr('href', 'https://plus.google.com/share?url=https%3A//emojilink.me/'+encodeURI(res.link.emojis));
                                $("a[data-share-fb-td]").attr('href', 'https://www.facebook.com/sharer/sharer.php?u=https%3A//emojilink.me/'+encodeURI(res.link.emojis));
                                $("a[data-share-linkedin-td]").attr('href', 'https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A//emojilink.me/' + encodeURI(res.link.emojis) + '&amp;title=&amp;summary=&amp;source=');
                                $("a[data-share-twitter-td]").attr('href', 'https://twitter.com/home?status=https%3A//emojilink.me/'+encodeURI(res.link.emojis));
                                $("#results").fadeIn('fast', function() {
                                    $(this).show();
                                });
                                $("#autolink").val('');
                                $("#customizelink").val('');
                                $('div.emoji-wysiwyg-editor').html('');
                                UIkit.Utils.scrollToElement(UIkit.$("#results"), { duration: 1000 });
                            } else if (res.result.match(/Sorry, your emojis combination is already used by others, please pick another combination/) !== null) {
                                UIkit.notify("Sorry, your emojis combination is already used by others, please pick another combination", {status:'danger', timeout: 0});
                                $("#autolink").trigger('change');
                            } else if (res.result.match(/Sorry, invalid emojis detected or number of emojis exceeded 25/) !== null) {
                                UIkit.notify("Sorry, invalid emojis detected or number of emojis exceeded 25", {status:'danger', timeout: 0});
                                $("#autolink").trigger('change');
                            } else if (res.result.match(/Sorry, the link is too long/) !== null) {
                                UIkit.notify("Sorry, the link is too long", {status:'danger', timeout: 0});
                                $("#autolink").trigger('change');
                            } else if (res.result.match(/Sorry, emojilink.me link cannot be shorten/) !== null) {
                                UIkit.notify("Sorry, emojilink.me link cannot be shorten", {status:'danger', timeout: 0});
                                $("#autolink").trigger('change');
                            } else {
                                UIkit.notify("Error, please try again", {status:'danger'});
                                $("#autolink").trigger('change');
                            }
                        },
                        error: function () {
                            $("#go").prop('disabled', false);
                        }
                    });
                    
                    fbq('track', 'getEmojiLink', {
                    url: $("#autolink").val()
                    });
                });
                
                var emojis = '';
                
                $("a").click(function(e) {
                    if (typeof $(this).attr('id') !== 'undefined' && $(this).attr('id').match(/^url-encode/) !== null) {
                        e.preventDefault();
                        if ($("#generated-td-link").val().match(/%/) === null) {
                            m = $("#generated-td-link").next().attr('data-clipboard-text').match(/^https:\/\/emojilink.me\/([\s\S]+)$/);
                            emojis = m[1];
                            $("#generated-td-link").val('https://emojilink.me/'+encodeURI(emojis));
                            $("#generated-td-link").next().attr('data-clipboard-text', 'https://emojilink.me/'+encodeURI(emojis));
                            $("#generated-td-link-m").val('https://emojilink.me/'+encodeURI(emojis));
                            $("#generated-td-link-m").next().attr('data-clipboard-text', 'https://emojilink.me/'+encodeURI(emojis));
                        } else {
                            $("#generated-td-link").val('https://emojilink.me/'+emojis);
                            $("#generated-td-link").next().attr('data-clipboard-text', 'https://emojilink.me/'+emojis);
                            $("#generated-td-link-m").val('https://emojilink.me/'+emojis);
                            $("#generated-td-link-m").next().attr('data-clipboard-text', 'https://emojilink.me/'+emojis);
                        }
                    }
                });
            });
        </script>
</body>
</html>