<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>AdBoost - Accesso cliente</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<!--<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon">-->
<link href="/cliente/css/stile.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="container">
<div id="header">
<div id="logo"> </div>
</div>
<div id="frame">
<div id="content">
<p class="titolo-blu">Autenticazione Utente</p>
<form action="login.php" id="form" method="post" name="form">
<div id="content-frame">
<table cellpadding="0" cellspacing="0" class="fixed">
<tr>
<td height="56">
<label class="lab">
                                	Username <span class="required">*</span>
<input class="auto" id="username" name="username" type="text" value=""/>
</label>
</td>
<td height="56">
<label class="lab">
                                    Password <span class="required">*</span>
<input class="auto" id="password" name="password" type="password"/>
</label>
</td>
</tr>
<tr>
<td colspan="2" height="36" valign="bottom">
<button class="btn-std" id="Submit" name="Submit" type="submit" value="Accedi"><span>Accedi</span></button>
<!--<input name="Submit" type="submit" class="button-std" id="Submit" value="Accedi">-->
</td>
</tr>
</table>
</div>
<!-- chiudo content-frame -->
</form>
</div> <!-- chiudo content -->
</div> <!-- chiudo frame -->
<div id="footer">Powered by: <b>Ediscom Internet Solutions</b> P.I. 09311070016</div>
</div>
</body>
</html>