<!DOCTYPE html>
<html dir="ltr" lang="ja" prefix="content: http://purl.org/rss/1.0/modules/content/  dc: http://purl.org/dc/terms/  foaf: http://xmlns.com/foaf/0.1/  og: http://ogp.me/ns#  rdfs: http://www.w3.org/2000/01/rdf-schema#  schema: http://schema.org/  sioc: http://rdfs.org/sioc/ns#  sioct: http://rdfs.org/sioc/types#  skos: http://www.w3.org/2004/02/skos/core#  xsd: http://www.w3.org/2001/XMLSchema# ">
<head>
<meta charset="utf-8"/>
<link href="https://www.atmark-techno.com/" rel="shortlink"/>
<meta content="アットマークテクノ" property="og:site_name"/>
<link href="https://www.atmark-techno.com/" rel="canonical"/>
<meta content="Website" property="og:type"/>
<meta content="アットマークテクノは、ARM＋Linuxの省電力CPUボードを中核とした産業機器向け「組み込みプラットフォーム」のリーディングカンパニーです。" name="description"/>
<meta content="https://www.atmark-techno.com/" property="og:url"/>
<meta content="アットマークテクノは、ARM＋Linuxの省電力CPUボードを中核とした産業機器向け「組み込みプラットフォーム」のリーディングカンパニーです。" name="abstract"/>
<meta content="アットマークテクノ | 組み込みプラットフォーム" property="og:title"/>
<meta content="アーム,ARM,ARM9,ARM7,ARM11,ARM926EJ-S,ARM1136JF-S,VFP,Freescale,フリースケール,組込み,組込,組み込み,リナックス,Linux,CPUボード,ボードコンピュータ,小型,省電力,低電力,温度拡張,低価格,短納期,ファンレス,産業機器,組み込みソフト,組み込み教材,組み込み技術,組み込み開発,開発環境,エンベデッド" name="keywords"/>
<meta content="https://www.atmark-techno.com/files/images/og/wac_ogp-image.jpg" property="og:image"/>
<meta content="Drupal 8 (https://www.drupal.org)" name="Generator"/>
<meta content="width" name="MobileOptimized"/>
<meta content="true" name="HandheldFriendly"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="/themes/custom/atweb/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="https://www.atmark-techno.com/node/1" rel="revision"/>
<title>アットマークテクノ | 組み込みプラットフォーム</title>
<link href="/sites/www.atmark-techno.com/files/css/css_pN_aNY_1eLt0WlUbmimNvK8eF4lkcIrV3SSIjBt5NlA.css" media="all" rel="stylesheet"/>
<link href="/sites/www.atmark-techno.com/files/css/css_DLsACxdnuhfMB2F02e5HdkMGResiEqK1OhP3Yih1viI.css" media="all" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat:700" media="all" rel="stylesheet"/>
<!--[if lte IE 8]>
<script src="/sites/www.atmark-techno.com/files/js/js_VtafjXmRvoUgAzqzYTA3Wrjkx9wcWhjP0G4ZnnqRamA.js"></script>
<![endif]-->
</head>
<body class="path-frontpage page-node-type-top-page">
<a class="visually-hidden focusable skip-link" href="#main-content">
      メインコンテンツに移動
    </a>
<div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas="">
<!-- container -->
<div id="container">
<div id="top-navigation-wrapper">
<div id="header-wrapper">
<div id="header">
<div class="close" id="sp-global-nav-toggler">
<span class="navbar-toggler-icon"></span>
</div>
<div class="crearfix" id="logo">
<div class="region region-header">
<div class="block block-system block-system-branding-block" id="block-atweb-branding">
<a class="site-logo" href="/" rel="home">
<img alt="ホーム" src="/themes/custom/atweb/logo.png"/>
</a>
</div>
</div>
</div>
<div id="search">
<div class="region region-search">
<div class="search-block-form block block-search container-inline" data-drupal-selector="search-block-form" id="block-atweb-search" role="search">
<form accept-charset="UTF-8" action="/search/node" id="search-block-form" method="get">
<div class="js-form-item form-item js-form-type-search form-type-search js-form-item-keys form-item-keys form-no-label">
<label class="visually-hidden" for="edit-keys">検索</label>
<input class="form-search" data-drupal-selector="edit-keys" id="edit-keys" maxlength="128" name="keys" size="15" title="検索したいキーワードを入力してください。" type="search" value=""/>
</div>
<div class="form-actions js-form-wrapper form-wrapper" data-drupal-selector="edit-actions" id="edit-actions"><input class="button js-form-submit form-submit" data-drupal-selector="edit-submit" id="edit-submit" type="submit" value=""/>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
<div id="menu-bar-wrapper">
<div class="region region-menu-bar">
<div class="block block-block-content block-block-content4d8d92cf-0e09-43b7-a7f8-fcadc3415206" id="block-global-navigation">
<div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><div class="content" id="global-nav">
<ul class="navbar">
<li class="nav-item dropdown">
<span class="nav-menu dropdown-toggle">ニュース</span>
<ul class="dropdown-menu">
<li class="dropdown-item">
<a href="/news/press-releases" title="プレスリリース">プレスリリース</a>
</li>
<li class="dropdown-item border-item">
<a href="/news/notices" title="お知らせ">お知らせ</a>
</li>
<li class="dropdown-item border-item">
<a href="/events" title="イベント情報">イベント情報</a>
</li>
</ul>
</li>
<li class="nav-item dropdown middle">
<span class="nav-menu dropdown-toggle">製品・サービス</span>
<ul class="dropdown-menu">
<li class="dropdown-item">
<a href="https://armadillo.atmark-techno.com/" target="_blank">Armadillo</a>
</li>
<li class="dropdown-item border-item">
<a href="https://armadillo.atmark-techno.com/cactusphere" target="_blank">Cactusphere</a>
</li>
<li class="dropdown-item border-item">
<a href="https://open-degu.com/" target="_blank">Degu</a>
</li>
<li class="dropdown-item border-item">
<a href="https://node-eye.com" target="_blank">node-eye</a>
</li>
<li class="dropdown-item border-item">
<a href="http://suzaku.atmark-techno.com/" target="_blank">SUZAKU</a>
</li>
</ul>
</li>
<li class="nav-item dropdown middle">
<span class="nav-menu dropdown-toggle">サポート</span>
<ul class="dropdown-menu">
<li class="dropdown-item">
<a href="https://armadillo.atmark-techno.com/support/warranty" target="_blank">サポート・製品保証</a>
</li>
</ul>
</li>
<li class="nav-item dropdown middle">
<span class="nav-menu dropdown-toggle">会社情報</span>
<ul class="dropdown-menu">
<li class="dropdown-item">
<a href="/at-concept">事業内容</a>
</li>
<li class="dropdown-item border-item">
<a href="/company">会社概要・沿革</a>
</li>
<li class="dropdown-item border-item">
<a href="/company/office">事業所一覧</a>
</li>
<li class="dropdown-item border-item">
<a href="/company/environment-plan">環境・品質への取り組み</a>
</li>
</ul>
</li>
<li class="nav-item dropdown middle">
<span class="nav-menu dropdown-toggle">採用情報</span>
<ul class="dropdown-menu">
<li class="dropdown-item">
<a href="/special/recruit/career.html" target="_blank">中途採用</a>
</li>
<li class="dropdown-item border-item">
<a href="/special/recruit/" target="_blank">新卒採用</a>
</li>
<li class="dropdown-item border-item">
<a href="/employment/part-time">パート・アルバイト</a>
</li>
</ul>
</li>
<li class="nav-item dropdown last">
<span class="nav-menu dropdown-toggle">お問い合わせ</span>
<ul class="dropdown-menu">
<li class="dropdown-item">
<a href="/contact/logo">商標・著作権について</a>
</li>
<li class="dropdown-item border-item">
<a href="https://armadillo.atmark-techno.com/purchase" target="_blank">購入方法</a>
</li>
<li class="dropdown-item border-item">
<a href="/dm_register">メルマガ登録</a>
</li>
<li class="dropdown-item border-item">
<a href="/contact">お問い合わせ</a>
</li>
</ul>
</li>
</ul>
</div></div>
</div>
<div class="block block-block-content block-block-contentf3baaeeb-fbdd-487f-8993-3a3982424943" id="block-slide-show">
<div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><div class="flexslider">
<ul class="slides">
<!-- 2枚目以降 降順 -->
<!-- 2枚目以降 降順 end -->
<!-- 1枚目 -->
<li><a href="/at-concept"><img alt="アットマークテクノの事業内容" src="/sites/www.atmark-techno.com/files/images/2020-06/wac_banner.jpg"/></a></li>
<!-- 1枚目end -->
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- content -->
<div class="clearfix" id="contents">
<div id="content-header">
</div>
<div id="beta">
<div class="region region-content">
<div class="hidden" data-drupal-messages-fallback=""></div>
<div class="block block-system block-system-main-block" id="block-atweb-content">
<article about="/node/1" class="node node--type-top-page node--view-mode-full" data-history-node-id="1" role="article">
<div class="node__content">
<div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><div class="d-none mb20">
<img alt="mobile-banner.jpg" data-id="mobile-banner.jpg" src="https://www.atmark-techno.com/sites/www.atmark-techno.com/files/images/2020-07/mobile-banner.jpg"/>
</div>
<div class="box8 mr">
<a href="/at-concept"><div class="button longtext"><p>組み込みプラットフォームとは</p></div></a>
</div>
<div class="box6">
<p><b>アットマークテクノの組み込みプラットフォーム</b><br/><span style="font-size: 12px;">アットマークテクノは、省電力CPUボードを中核とした産業機器向け「組み込みプラットフォーム」のリーディングカンパニーです。</span></p>
</div>
<hr/>
<div class="btn-wrapper">
<div class="box8 mr">
<a href="https://armadillo.atmark-techno.com" target="_blank"><div class="button img armadillo"><p><img alt="Armadillo" data-id="logo_btn_armadillo.png" src="https://www.atmark-techno.com/sites/www.atmark-techno.com/files/logo_images/2020-09/logo_btn_armadillo_02.png" width="300"/></p></div></a>
<p class="text-center">Armコア・Linuxプリインストール<br/>小型・省電力組み込みプラットフォーム</p>
</div>
<div class="box8 mr">
<a href="https://armadillo.atmark-techno.com/cactusphere" target="_blank"><div class="button img armadillo"><p><img alt="Cactusphere" data-id="logo_btn_cactusphere.png" src="https://www.atmark-techno.com/sites/www.atmark-techno.com/files/logo_images/2020-09/logo_btn_cactusphere.png" width="300"/></p></div></a>
<p class="text-center">既存設備に簡単後付け！すぐにIoT化<br/>Microsoft Azure対応 IoTアダプタ</p>
</div>
<div class="box8">
<a href="https://open-degu.com/" target="_blank"><div class="button img armadillo"><p><img alt="Degu" data-id="logo_btn_degu.png" src="https://www.atmark-techno.com/sites/www.atmark-techno.com/files/logo_images/2020-09/logo_btn_degu.png" width="300"/></p></div></a>
<p class="text-center">メッシュネットワーク対応<br/>IoTセンサー&amp;ゲートウェイ</p>
</div>
<div class="clear"></div>
</div></div>
<div id="section-news">
<div class="views-element-container"><div class="view-news-list news-list-wrapper">
<div class="news-header">
<div class="title">
<p>NEWS</p>
</div>
</div>
<div class="item-list">
<ul>
<li><div class="views-field views-field-title"><span class="field-content">[2020.12.18] <a href="/news/notices/202012_armadillo_cpex" hreflang="ja">Armadillo出荷50万台記念キャンペーン ～2021年1月29日(金)まで応募期間延長</a></span></div></li>
<li><div class="views-field views-field-title"><span class="field-content">[2020.12.16] <a href="/news/notices/202012_holidays" hreflang="ja">年末年始休業のお知らせ</a></span></div></li>
<li><div class="views-field views-field-title"><span class="field-content">[2020.11.9] <a href="/news/notices/20201114_maintenance" hreflang="ja">メンテナンスに伴うWebサービス等の一時停止のお知らせ（2020年11月14日実施）</a></span></div></li>
<li><div class="views-field views-field-title"><span class="field-content">[2020.11.4] <a href="/news/notices/202011_chip1stop" hreflang="ja">チップワンストップ社との販売代理店契約締結のお知らせ</a></span></div></li>
<li><div class="views-field views-field-title"><span class="field-content">[2020.10.16] <a href="/news/notices/202010_sales-tokyo" hreflang="ja">東京営業所 移転のお知らせ・移転に伴う休業日のお知らせ</a></span></div></li>
</ul>
</div>
<div class="old-news-link">
<p>
<a class="triangle" href="/news/press-releases">過去のプレスリリース一覧</a>
<a class="triangle notices" href="/news/notices">過去のお知らせ一覧</a>
</p>
</div>
</div>
</div>
</div>
<div id="section-event">
<div class="views-element-container"><div class="view-event-list news-list-wrapper">
<div class="news-header">
<div class="title">
<p>EVENT</p>
</div>
</div>
<div class="item-list">
<ul>
<li><div class="views-field views-field-title"><span class="field-content">[2020.12.4] <a href="/events/202012_algyan" hreflang="ja">IoT ALGYAN主催「Cactusphere」超入門体験ハンズオン（2021/1/9, オンライン開催）～実機プレゼントつき！誰でも簡単にIoTを体験できます</a></span></div></li>
<li><div class="views-field views-field-title"><span class="field-content">[2020.11.16] <a href="/events/202011_et2020" hreflang="ja">ET &amp; IoT Digital 2020（2020/11/16～12/18, オンライン開催）～Cactusphereについてご紹介</a></span></div></li>
<li><div class="views-field views-field-title"><span class="field-content">[2020.11.4] <a href="/events/202011_chip1stop_expo" hreflang="ja">チップワンストップ オンライン展示会 2020 秋（2020/10/27～12/4, オンライン開催）～セミナー講演視聴で割引クーポンゲット！</a></span></div></li>
<li><div class="views-field views-field-title"><span class="field-content">[2020.10.20] <a href="/events/202010_algyan" hreflang="ja">IoT ALGYAN主催「Cactusphere」超入門体験ハンズオン（2020/11/7, オンライン開催）～製品貸出あり！誰でも簡単にIoTを体験できます</a></span></div></li>
<li><div class="views-field views-field-title"><span class="field-content">[2020.10.12] <a href="/events/202010_smart-factory" hreflang="ja">スマートファクトリーJapan2020 ONLINE（2020/10/14～11/13, オンライン開催）～MicrosoftブースにCactusphereを出展</a></span></div></li>
</ul>
</div>
<div class="old-news-link">
<p>
<a class="triangle" href="/events">過去のイベント一覧</a>
</p>
</div>
</div>
</div>
</div>
</div>
</article>
</div>
</div>
<!-- end of beta -->
</div>
<!-- end of contents -->
</div>
<!-- footer -->
<div id="footer">
<!-- footer-inner -->
<div id="footer-inner">
<!-- footer-bunner -->
<div id="footer-bunner">
<div class="region region-footer">
<div class="block block-block-content block-block-content86123fe1-dacf-4fd0-988d-0dbad9b72940" id="block-footer-menu">
<div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><div>
<ul class="footer-icons">
<li><a href="https://www.facebook.com/atmarktechno/" target="_blank"><img src="/themes/custom/atweb/images/icons/icon_facebook.svg"/></a></li>
<li><a href="/contact"><img src="/themes/custom/atweb/images/icons/icon_mail.svg"/></a></li>
</ul>
<ul class="footer-links">
<li><a href="//armadillo.atmark-techno.com" target="_blank">Armadillo サイト</a></li>
<li><a href="/terms">サイト利用規約</a></li>
<li><a href="/privacy">プライバシーポリシー</a></li>
<li><a href="/english">English</a></li>
</ul>
<p>Copyright© 2001-2021 Atmark Techno, Inc.</p>
<p>本サイトに記載の商品名または会社名は、各社・各団体の商標または登録商標です。™、®マークは付記していない場合があります。</p>
</div></div>
</div>
</div>
<!-- end of footer-bunner -->
</div>
<!-- footer-bottom -->
<div id="footer-bottom">
<!-- end of footer-bottom -->
</div>
<!-- end of footer-inner -->
</div>
<!-- end of footer -->
</div>
<!-- end of container -->
</div>
</div>
<script data-drupal-selector="drupal-settings-json" type="application/json">{"path":{"baseUrl":"\/","scriptPath":null,"pathPrefix":"","currentPath":"node\/1","currentPathIsAdmin":false,"isFront":true,"currentLanguage":"ja"},"pluralDelimiter":"\u0003","suppressDeprecationErrors":true,"ajaxTrustedUrl":{"\/search\/node":true},"user":{"uid":0,"permissionsHash":"9d13744c65bd0fa722b4bd4b007d3428ba5ca0703f2eb53b0b0ea1498a40ebd7"}}</script>
<script src="/sites/www.atmark-techno.com/files/js/js_znt-u8_5apv0zcaXWWA6swAey6RxXPvBZre-_WjTK2w.js"></script>
</body>
</html>
