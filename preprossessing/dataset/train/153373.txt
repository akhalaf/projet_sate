<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="de-DE">
<![endif]--><!--[if IE 8]>
<html class="ie ie8" lang="de-DE">
<![endif]--><!--[if !(IE 7) | !(IE 8) ]><!--><html lang="de-DE">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>ART+COM Studios</title>
<link href="https://artcom.de/wp-content/plugins/sitepress-multilingual-cms/res/css/language-selector.css?v=3.1.5" media="all" rel="stylesheet" type="text/css"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://artcom.de/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
    <script src="https://artcom.de/wp-content/themes/artcom2014/js/html5.js"></script>
    <![endif]-->
<link href="https://artcom.de/wp-content/themes/artcom2014/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://artcom.de/wp-content/themes/artcom2014/images/ac-touch-icon.png" rel="apple-touch-icon"/>
<link href="https://artcom.de/wp-content/themes/artcom2014/images/ac-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="https://artcom.de/wp-content/themes/artcom2014/images/ac-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="https://artcom.de/wp-content/themes/artcom2014/images/ac-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<!-- All in One SEO Pack 3.3.4 by Michael Torbert of Semper Fi Web Design[325,347] -->
<meta content="ART+COM designs and develops innovative media installations, environments and architecture." name="description"/>
<script class="aioseop-schema" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://artcom.de/#organization","url":"https://artcom.de/","name":"ART+COM Studios","sameAs":[]},{"@type":"WebSite","@id":"https://artcom.de/#website","url":"https://artcom.de/","name":"ART+COM Studios","publisher":{"@id":"https://artcom.de/#organization"}},{"@type":"WebPage","@id":"https://artcom.de/#webpage","url":"https://artcom.de/","inLanguage":"de-DE","name":"ART+COM Studios","isPartOf":{"@id":"https://artcom.de/#website"},"datePublished":"2014-07-18T15:01:40+02:00","dateModified":"2016-09-21T14:01:12+02:00","about":{"@id":"https://artcom.de/#organization"},"description":"ART+COM gestaltet und entwickelt innovative mediale Installationen, R\u00e4ume und Architekturen."}]}</script>
<link href="https://artcom.de/" rel="canonical"/>
<!-- All in One SEO Pack -->
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://artcom.de/front/feed/" rel="alternate" title="ART+COM Studios » ART+COM Studios Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/artcom.de\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://artcom.de/wp-includes/css/dist/block-library/style.min.css" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://artcom.de/wp-content/themes/artcom2014/scripts/components/videojs/dist/video-js/video-js.css" id="videojs-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://artcom.de/wp-content/themes/artcom2014/scripts/components/bootstrap-touch-carousel/dist/css/bootstrap-touch-carousel.css" id="bootstrap-touch-carousel-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://artcom.de/wp-content/themes/artcom2014/style.css" id="artcom2014-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://artcom.de/wp-includes/css/dashicons.min.css" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://artcom.de/wp-content/plugins/video-embed-thumbnail-generator/css/kgvid_styles.css" id="kgvid_video_styles-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://artcom.de/wp-includes/js/dist/vendor/lodash.min.js" type="text/javascript"></script>
<script type="text/javascript">
window.lodash = _.noConflict();
</script>
<script src="https://artcom.de/wp-includes/js/jquery/jquery.js" type="text/javascript"></script>
<script src="https://artcom.de/wp-includes/js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
<script src="https://artcom.de/wp-content/themes/artcom2014/scripts/components/jquery.equalheights/jquery.equalheights.min.js" type="text/javascript"></script>
<script src="https://artcom.de/wp-content/themes/artcom2014/scripts/components/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://artcom.de/wp-content/themes/artcom2014/scripts/components/videojs/dist/video-js/video.dev.js" type="text/javascript"></script>
<script src="https://artcom.de/wp-content/themes/artcom2014/scripts/components/videojs-resolution-selector/video-quality-selector.js" type="text/javascript"></script>
<script src="https://artcom.de/wp-content/themes/artcom2014/scripts/hyphenator/hyphenator.js" type="text/javascript"></script>
<script src="https://artcom.de/wp-content/themes/artcom2014/scripts/components/jquery-hammerjs/jquery.hammer-full.min.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var media_items = [];
var departmentIds = ["19","4","5"];
var ajax_info = {"url":"https:\/\/artcom.de\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script src="https://artcom.de/wp-content/themes/artcom2014/scripts/scripts.min.js" type="text/javascript"></script>
<script src="https://artcom.de/wp-content/plugins/sitepress-multilingual-cms/res/js/jquery.cookie.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpml_browser_redirect_params = {"pageLanguage":"de","languageUrls":{"de_DE":"https:\/\/artcom.de\/","de":"https:\/\/artcom.de\/","DE":"https:\/\/artcom.de\/","en_US":"https:\/\/artcom.de\/en\/","en":"https:\/\/artcom.de\/en\/","US":"https:\/\/artcom.de\/en\/"},"cookie":{"name":"_icl_visitor_lang_js","domain":"artcom.de","path":"\/","expiration":24}};
/* ]]> */
</script>
<script src="https://artcom.de/wp-content/plugins/sitepress-multilingual-cms/res/js/browser-redirect.js" type="text/javascript"></script>
<link href="https://artcom.de/wp-json/" rel="https://api.w.org/"/>
<link href="https://artcom.de/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://artcom.de/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<link href="https://artcom.de/" rel="shortlink"/>
<link href="https://artcom.de/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fartcom.de%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://artcom.de/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fartcom.de%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<meta content="WPML ver:3.1.5 stt:3,1;0" name="generator"/>
<link href="https://artcom.de/" hreflang="de-DE" rel="alternate"/>
<link href="https://artcom.de/en/" hreflang="en-US" rel="alternate"/>
<style type="text/css">
</style><style type="text/css">.background-art {
    background-image: url(https://artcom.de/wp-content/uploads/2016/09/2013_Tri_LandingPage-670x900.jpg);
}
@media
(-webkit-min-device-pixel-ratio: 2),
(   min--moz-device-pixel-ratio: 2),
(     -o-min-device-pixel-ratio: 2/1),
(        min-device-pixel-ratio: 2),
(                min-resolution: 192dpi),
(                min-resolution: 2dppx) {
    .background-art {
        background-image: url(https://artcom.de/wp-content/uploads/2016/09/2013_Tri_LandingPage-670x900@2x.jpg);
    }
}
@media (min-width: 768px ) {
    .background-art {
        background-image: url(https://artcom.de/wp-content/uploads/2016/09/2013_Tri_LandingPage-1360x767.jpg);
    }
}
@media
    only (min-width: 768px) and (-webkit-min-device-pixel-ratio: 2),
    only (min-width: 768px) and (   min--moz-device-pixel-ratio: 2),
    only (min-width: 768px) and (     -o-min-device-pixel-ratio: 2/1),
    only (min-width: 768px) and (        min-device-pixel-ratio: 2),
    only (min-width: 768px) and (                min-resolution: 192dpi),
    only (min-width: 768px) and (                min-resolution: 2dppx)
    .background-art {
        background-image: url(https://artcom.de/wp-content/uploads/2016/09/2013_Tri_LandingPage-1360x767@2x.jpg);
    }
}
@media (min-width: 1200px ) {
    .background-art {
        background-image: url(https://artcom.de/wp-content/uploads/2016/09/2013_Tri_LandingPage.jpg);
    }
}
.background-communication {
    background-image: url(https://artcom.de/wp-content/uploads/2016/09/2014_DB_Artwall_LandingPage-670x900.jpg);
}
@media
(-webkit-min-device-pixel-ratio: 2),
(   min--moz-device-pixel-ratio: 2),
(     -o-min-device-pixel-ratio: 2/1),
(        min-device-pixel-ratio: 2),
(                min-resolution: 192dpi),
(                min-resolution: 2dppx) {
    .background-communication {
        background-image: url(https://artcom.de/wp-content/uploads/2016/09/2014_DB_Artwall_LandingPage-670x900@2x.jpg);
    }
}
@media (min-width: 768px ) {
    .background-communication {
        background-image: url(https://artcom.de/wp-content/uploads/2016/09/2014_DB_Artwall_LandingPage-1360x765.jpg);
    }
}
@media
    only (min-width: 768px) and (-webkit-min-device-pixel-ratio: 2),
    only (min-width: 768px) and (   min--moz-device-pixel-ratio: 2),
    only (min-width: 768px) and (     -o-min-device-pixel-ratio: 2/1),
    only (min-width: 768px) and (        min-device-pixel-ratio: 2),
    only (min-width: 768px) and (                min-resolution: 192dpi),
    only (min-width: 768px) and (                min-resolution: 2dppx)
    .background-communication {
        background-image: url(https://artcom.de/wp-content/uploads/2016/09/2014_DB_Artwall_LandingPage-1360x765@2x.jpg);
    }
}
@media (min-width: 1200px ) {
    .background-communication {
        background-image: url(https://artcom.de/wp-content/uploads/2016/09/2014_DB_Artwall_LandingPage-2560x1440.jpg);
    }
}
.background-research {
    background-image: url(https://artcom.de/wp-content/uploads/2016/09/1996_Terravision_LandingPage-670x900.jpg);
}
@media
(-webkit-min-device-pixel-ratio: 2),
(   min--moz-device-pixel-ratio: 2),
(     -o-min-device-pixel-ratio: 2/1),
(        min-device-pixel-ratio: 2),
(                min-resolution: 192dpi),
(                min-resolution: 2dppx) {
    .background-research {
        background-image: url(https://artcom.de/wp-content/uploads/2016/09/1996_Terravision_LandingPage-670x900@2x.jpg);
    }
}
@media (min-width: 768px ) {
    .background-research {
        background-image: url(https://artcom.de/wp-content/uploads/2016/09/1996_Terravision_LandingPage-1360x850.jpg);
    }
}
@media
    only (min-width: 768px) and (-webkit-min-device-pixel-ratio: 2),
    only (min-width: 768px) and (   min--moz-device-pixel-ratio: 2),
    only (min-width: 768px) and (     -o-min-device-pixel-ratio: 2/1),
    only (min-width: 768px) and (        min-device-pixel-ratio: 2),
    only (min-width: 768px) and (                min-resolution: 192dpi),
    only (min-width: 768px) and (                min-resolution: 2dppx)
    .background-research {
        background-image: url(https://artcom.de/wp-content/uploads/2016/09/1996_Terravision_LandingPage-1360x850@2x.jpg);
    }
}
@media (min-width: 1200px ) {
    .background-research {
        background-image: url(https://artcom.de/wp-content/uploads/2016/09/1996_Terravision_LandingPage-2304x1440.jpg);
    }
}
</style></head>
<body class="home page-template-default page page-id-5 front-page-body" data-spy="scroll" data-target=".anchor-navbar">
<noscript>
<div style="margin-top:100px; left:0; right:0; text-align:center; position:absolute; color:red; font-family:sans-serif; font-size:20px;">Please enable JavaScript before using our website.</div>
</noscript>
<!--[if lt IE 9]>
<div style="margin-top:130px; left:0; right:0; text-align:center; position:absolute; color:red; font-family:sans-serif; font-size:20px;">Internet Explorer version 8 and below is not supported by our website.</div>
<![endif]-->
<div class="container" id="page">
<div class="introduction">
<div class="row front-logo">
<a href="https://artcom.de/about/"><img class="col-sm-offset-4 col-sm-4 col-xs-offset-2 col-xs-8" src="https://artcom.de/wp-content/themes/artcom2014/images/Logo_AC_STUDIOS_white.svg"/></a> </div>
<div class="front-menu row">
<ul id="menu-front"><li class="col-sm-4 col-sm-offset-0 col-xs-offset-2 col-xs-8" data-id="19" id="menu-item-19">
<a href="https://artcom.de/department/art/">Art</a>
</li><li class="col-sm-4 col-sm-offset-0 col-xs-offset-2 col-xs-8" data-id="4" id="menu-item-4">
<a href="https://artcom.de/department/communication/">Communication</a>
</li><li class="col-sm-4 col-sm-offset-0 col-xs-offset-2 col-xs-8" data-id="5" id="menu-item-5">
<a href="https://artcom.de/department/research/">Research</a>
</li></ul> </div>
</div>
<div class="background-slideshow">
<ul class="slideshow-front" id="slideshow-front"><li class="background-art" data-id="19" id="background-19"></li><li class="background-communication" data-id="4" id="background-4"></li><li class="background-research" data-id="5" id="background-5"></li></ul> </div>
<!--
Performance optimized by W3 Total Cache. Learn more: https://www.w3-edge.com/products/


Served from: artcom.de @ 2021-01-14 19:58:12 by W3 Total Cache
--></div></body></html>