<!DOCTYPE html>
<html><head>
<meta charset="utf-8"/>
<meta content="initial-scale=1.0, user-scalable=no" name="viewport"/>
<!-- WARNING: for iOS 7, remove the width=device-width and height=device-height attributes. See https://issues.apache.org/jira/browse/CB-4323 -->
<meta content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" name="viewport"/>
<title>Tools</title>
<link href="/app.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="app">
<div class="container">
<div class="row">
<div class="twelve columns" id="calculator">
<div class="row">
<div class="six columns">
<div class="row valign-center">
<div class="six columns">
<h3>
                    {{ t('settings') }}
                  </h3>
</div>
<div class="six columns">
<div class="row">
<div class="nine columns">
<ul class="u-inline-list">
<li data-balloon="English" data-balloon-pos="down" v-on:click="setLocale('en')">
<span class="flag-icon flag-icon-us"></span>
</li>
<li data-balloon="íêµ­ì´" data-balloon-pos="down" v-on:click="setLocale('ko')">
<span class="flag-icon flag-icon-kr"></span>
</li>
<li data-balloon="francais" data-balloon-pos="down" v-on:click="setLocale('fr')">
<span class="flag-icon flag-icon-fr"></span>
</li>
<li data-balloon="Deutsch" data-balloon-pos="down" v-on:click="setLocale('de')">
<span class="flag-icon flag-icon-de"></span>
</li>
</ul>
</div>
<div class="three columns">
<span data-balloon="Toggle dark mode" data-balloon-pos="down" v-on:click="toggleDark()">
<i class="fa fa-lightbulb-o fa-fw"></i>
</span>
</div>
</div>
</div>
</div>
<p>
                {{{ t('intro') }}}
              </p>
<p>
                {{{ t('discussion') }}}
              </p>
<form class="u-margin-top-2">
<div id="targets">
<div class="row">
<div class="six columns">
<label>{{ t('target') }}</label>
<select class="u-full-width" id="target_picker" v-model="selected">
<option v-bind:value="target.id" v-for="target in targets">{{ target.name }}</option>
</select>
</div>
</div>
</div>
<div id="inputs">
<div class="row">
<div class="six columns" v-if="target">
<label>{{ t('interest') }}</label>
<select class="u-full-width" id="interest_picker" v-model="interest">
<option v-bind:value="interest" v-for="interest in validInterests">{{ interest }}</option>
</select>
</div>
<div class="six columns" v-if="interest">
<label>{{ t('favor') }}</label>
<select class="u-full-width" id="favor_picker" v-model="favor">
<option v-bind:value="favor" v-for="favor in validFavor">{{ favor }}</option>
</select>
</div>
</div>
<div class="row">
<div class="six columns" v-if="favor">
<label>{{ t('goal') }}</label>
<select class="u-full-width" id="goal_picker" v-model="goal">
<option v-bind:value="goal.id" v-for="goal in goals">{{ goal.name }}</option>
</select>
</div>
<div class="six columns" v-if="favor &amp;&amp; (goal != null &amp;&amp; goal != 6)">
<label> </label>
<input class="u-full-width" type="number" v-model="goalParam"/>
</div>
</div>
<div class="u-margin-top-2 u-text-right" v-if="goal != null">
<input class="button-primary" type="button" v-on:click="submit" value="Calculate"/>
</div>
</div>
</form>
</div>
<div class="six columns">
<h3>{{ t('result') }}</h3>
<div v-if="target">
<table class="u-full-width">
<tbody>
<tr>
<td style="width: 10%"><b>{{ t('name') }}</b></td>
<td>{{ target.name }}</td>
</tr>
<tr>
<td style="width: 10%"><b>{{ t('category') }}</b></td>
<td>{{ target.category.name }}</td>
</tr>
<tr>
<td style="width: 10%"><b>{{ t('constellation') }}</b></td>
<td>{{ target.constellation.name }}</td>
</tr>
</tbody>
</table>
</div>
<div v-else="">
<p>
                  {{ t('settings-prompt') }}
                </p>
</div>
<div v-if="result">
<div class="alert alert-info" v-if="result.experimental">
<i class="fa fa-fw fa-flask"></i>
                  {{{ t('experimental') }}}
                </div>
<p>
                  {{ t('instructions') }}
                </p>
<ol>
<li v-for="k in result.knowledge_ids">
                    {{ knowledgeFor(k).name }}
                  </li>
</ol>
</div>
<div class="u-text-center" v-if="loading">
<i class="fa fa-spinner fa-pulse fa-2x"></i>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script src="/vendor.js"></script>
<script src="/app.js"></script>
<script>require('initialize');</script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-75477761-1', 'auto');
    ga('send', 'pageview');
  </script>
</body>
</html>
