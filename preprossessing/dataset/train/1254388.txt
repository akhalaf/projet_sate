<!DOCTYPE html>
<html dir="ltr" lang="ru">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="https://russianpoet.ru/sites/all/themes/bassoon/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<meta content="Drupal 7 (https://www.drupal.org)" name="generator"/>
<link href="https://russianpoet.ru/" rel="canonical"/>
<link href="https://russianpoet.ru/" rel="shortlink"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width" name="MobileOptimized"/>
<meta content="true" name="HandheldFriendly"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!--[if IEMobile]>  <meta http-equiv="cleartype" content="on">  <![endif]-->
<title>Страница не найдена | RussianPoet - Cайт поэта Адамовича Эдуарда</title>
<link href="https://russianpoet.ru/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://russianpoet.ru/sites/default/files/css/css_BlQKA1gKkEoh7vs3m3fqhwaANUwlHRDRj5TlqX7c2HQ.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://russianpoet.ru/sites/default/files/css/css_fEIYHf7a6__HmaZI0WMfFyuL6hwZU7kJ4ooi1KzkINI.css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lte IE 7]>
<link type="text/css" rel="stylesheet" href="https://russianpoet.ru/sites/all/themes/bassoon/css/ielt7.css?q3w4l6" media="all" />
<![endif]-->
<!--[if IE]>
<link type="text/css" rel="stylesheet" href="https://russianpoet.ru/sites/all/themes/bassoon/css/ie.css?q3w4l6" media="all" />
<![endif]-->
<link href="https://russianpoet.ru/sites/default/files/css/css_SmXKP5fkC27wqfDQhSoOeLji_jSpX8Xi9AIHaB0P3lQ.css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://russianpoet.ru/sites/default/files/js/js_qikmINIYTWe4jcTUn8cKiMr8bmSDiZB9LQqvceZ6wlM.js" type="text/javascript"></script>
<script src="https://russianpoet.ru/sites/default/files/js/js_R9UbiVw2xuTUI0GZoaqMDOdX0lrZtgX-ono8RVOUEVc.js" type="text/javascript"></script>
<script src="https://russianpoet.ru/sites/default/files/js/js_GcYOtUttgE9ol1r2g7GszW_iHVs8I8WrAkIw4fRAUC0.js" type="text/javascript"></script>
<script src="//yandex.st/share/cnt.share.js" type="text/javascript"></script>
<script src="https://russianpoet.ru/sites/default/files/js/js_3QkdMhrUxuXFP_MHofVm6EtOjm5vVISINEfQEmW3odc.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"bassoon","theme_token":"9RmxqvtEyGvCcs08bWNjdo0dBHo_v6slcSoG4YKHkCM","js":{"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"public:\/\/languages\/ru_k1zQjISwH1JWB0F3Ov-oOka0DvK-SwgmMqOoIGmwL1U.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_load.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_inline.js":1,"\/\/yandex.st\/share\/cnt.share.js":1,"sites\/all\/themes\/bassoon\/js\/script.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/book\/book.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/yashare_counters\/css\/yashare_counters.css":1,"sites\/all\/modules\/youtube\/css\/youtube.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"sites\/all\/themes\/bassoon\/css\/ielt7.css":1,"sites\/all\/themes\/bassoon\/css\/ie.css":1,"sites\/all\/themes\/bassoon\/css\/layout.css":1,"sites\/all\/themes\/bassoon\/css\/style.css":1}},"colorbox":{"transition":"elastic","speed":"350","opacity":"0.85","slideshow":false,"slideshowAuto":false,"slideshowSpeed":"2500","slideshowStart":"start slideshow","slideshowStop":"stop slideshow","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","overlayClose":true,"returnFocus":true,"maxWidth":"98%","maxHeight":"98%","initialWidth":"300","initialHeight":"250","fixed":true,"scrolling":false,"mobiledetect":true,"mobiledevicewidth":"480px"},"urlIsAjaxTrusted":{"\/search":true}});
//--><!]]>
</script>
<!--[if lt IE 9]>
    <script src="/sites/all/themes/bassoon/js/html5-respond.js"></script>
  <![endif]-->
<!--[if lt IE 9]>
  <script src="/sites/all/themes/bassoon/js/html5shiv.js"></script>
  <![endif]-->
<script src="//vk.com/js/api/openapi.js?105"></script>
<script type="text/javascript">
  VK.init({apiId: API_ID, onlyWidgets: true});
</script>
</head>
<body class="html not-front not-logged-in one-sidebar sidebar-first page-wp page-wp-wp-content page-wp-wp-content-themes page-wp-wp-content-themes-default page-wp-wp-content-themes-default-images page-wp-wp-content-themes-default-images-">
<div id="skip-link">
<a class="element-invisible element-focusable" href="#main-content">Перейти к основному содержанию</a>
</div>
<div class="wrapper-main">
<div class="wrapper-left">
<div class="logo"><a href="/" rel="home" title="Главная"></a></div>
<section class="region region-sidebar-first" id="sidebar_first">
<article class="block block-views" id="block-views-exp-search-result-page">
<form accept-charset="UTF-8" action="/search" id="views-exposed-form-search-result-page" method="get"><div><div class="views-exposed-form">
<div class="views-exposed-widgets clearfix">
<div class="views-exposed-widget views-widget-filter-search_api_views_fulltext" id="edit-search-api-views-fulltext-wrapper">
<div class="views-widget">
<div class="form-item form-type-textfield form-item-search-api-views-fulltext">
<input class="form-text" id="edit-search-api-views-fulltext" maxlength="128" name="search_api_views_fulltext" size="30" type="text" value="поиск"/>
</div>
</div>
</div>
<div class="views-exposed-widget views-submit-button">
<input class="form-submit" id="edit-submit-search-result" name="" type="submit" value="Применить"/> </div>
</div>
</div>
</div></form>
</article>
</section>
</div>
<div class="wrapper-content">
<h1 class="title" id="page-title">Страница не найдена</h1> <section class="region region-content" id="content">
<article class="block block-system" id="block-system-main">

      
  Страница "/wp/wp-content/themes/default/images/%09%0a" не найдена.
</article>
<article class="block block-yashare-counters" id="block-yashare-counters-yashare-counters">
<div class="yashare-auto-init" data-yasharel10n="ru" data-yasharequickservices="moimir,gplus,odnoklassniki,twitter,vkontakte,facebook,yaru," data-yasharetheme="counter" data-yasharetype="small"></div></article></section></div>
</div>
<div class="clearfix"></div>
<div class="wrapper-link">
<a href="/" rel="home" title="Главная"><img alt="Главная" src="/sites/all/themes/bassoon/images/small-logo.png"/></a>
</div>
<div class="wrapper-bottom">
<section class="region region-footer" id="footer">
<article class="block block-block" id="block-block-1">
<p><strong><a href="/">Главная</a> - <a href="/vyistupleniya">Выступления</a> - <a href="/stihotvoreniya">Стихотворения</a> - <a href="/blog">Блог</a></strong></p>
<p>Все права защищены. При использовании материалов ссылка обязательна.<br/>
© RussianPoet – Cайт поэта Адамовича Эдуарда, 2019</p>
</article>
</section>
</div> <!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=15247636&amp;from=informer" rel="nofollow" target="_blank"><img alt="Яндекс.Метрика" class="ym-advanced-informer" data-cid="15247636" data-lang="ru" src="https://informer.yandex.ru/informer/15247636/3_1_FFFFFFFF_EFEFEFFF_0_pageviews" style="width:88px; height:31px; border:0;" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"/></a>
<!-- /Yandex.Metrika informer -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter15247636 = new Ya.Metrika({
                    id:15247636,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/15247636" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
