<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<link href="https://www.agencyzed.com/wp-content/themes/DeepFocus/style.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.agencyzed.com/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="https://www.agencyzed.com/wp-content/themes/DeepFocus/css/ie6style.css" />
	<script type="text/javascript" src="https://www.agencyzed.com/wp-content/themes/DeepFocus/js/DD_belatedPNG_0.0.8a-min.js"></script>
	<script type="text/javascript">DD_belatedPNG.fix('img#logo, #search-form, #featured, span.date, .footer-widget ul li, span.overlay, a.readmore, a.readmore span, #recent-posts a#left-arrow, #recent-posts a#right-arrow, h4#recent, div#breadcrumbs, #sidebar h4');</script>
<![endif]-->
<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="https://www.agencyzed.com/wp-content/themes/DeepFocus/css/ie7style.css" />
<![endif]-->
<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" href="https://www.agencyzed.com/wp-content/themes/DeepFocus/css/ie8style.css" />
<![endif]-->
<script type="text/javascript">
	document.documentElement.className = 'js';
</script>
<script>var et_site_url='https://www.agencyzed.com';var et_post_id='0';function et_core_page_resource_fallback(a,b){"undefined"===typeof b&&(b=a.sheet.cssRules&&0===a.sheet.cssRules.length);b&&(a.onerror=null,a.onload=null,a.href?a.href=et_site_url+"/?et_core_page_resource="+a.id+et_post_id:a.src&&(a.src=et_site_url+"/?et_core_page_resource="+a.id+et_post_id))}
</script><title>404 Not Found|Agency Zed Productions Inc</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.agencyzed.com/feed" rel="alternate" title="Agency Zed Productions Inc » Feed" type="application/rss+xml"/>
<link href="https://www.agencyzed.com/comments/feed" rel="alternate" title="Agency Zed Productions Inc » Comments Feed" type="application/rss+xml"/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.12.2 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script data-cfasync="false" type="text/javascript">
	var mi_version         = '7.12.2';
	var mi_track_user      = true;
	var mi_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-40391862-1';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}

	if ( 'undefined' === typeof gaOptout ) {
		function gaOptout() {
			__gaTrackerOptout();
		}
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-40391862-1', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('send','pageview','/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer);
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<!-- / Google Analytics by MonsterInsights -->
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.agencyzed.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=ef17189b58bb47b20a7e4ddea1fa4d99"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<link href="https://www.agencyzed.com/wp-content/themes/DeepFocus/style-Noise.css" media="screen" rel="stylesheet" type="text/css"/>
<meta content="DeepFocus v.5.1.13" name="generator"/><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.agencyzed.com/wp-includes/css/dist/block-library/style.min.css?ver=ef17189b58bb47b20a7e4ddea1fa4d99" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.agencyzed.com/wp-content/themes/DeepFocus/css/responsive.css?ver=ef17189b58bb47b20a7e4ddea1fa4d99" id="et_responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=:&amp;subset=latin" id="et-gf--css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.agencyzed.com/wp-content/plugins/elegantbuilder/style.css?ver=2.4" id="et_lb_modules-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.agencyzed.com/wp-content/plugins/wp-google-map-plugin/assets/css/frontend.css?ver=ef17189b58bb47b20a7e4ddea1fa4d99" id="wpgmp-frontend_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.agencyzed.com/wp-content/themes/DeepFocus/epanel/shortcodes/css/shortcodes-legacy.css?ver=5.1.13" id="et-shortcodes-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.agencyzed.com/wp-content/themes/DeepFocus/epanel/shortcodes/css/shortcodes_responsive.css?ver=5.1.13" id="et-shortcodes-responsive-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.agencyzed.com/wp-content/themes/DeepFocus/includes/page_templates/js/magnific_popup/magnific_popup.css?ver=1.3.4" id="magnific_popup-css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.agencyzed.com/wp-content/themes/DeepFocus/includes/page_templates/page_templates.css?ver=1.8" id="et_page_templates-css" media="screen" rel="stylesheet" type="text/css"/>
<script id="monsterinsights-frontend-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[]","home_url":"https:\/\/www.agencyzed.com","hash_tracking":"false"};
/* ]]> */
</script>
<script id="monsterinsights-frontend-script-js" src="https://www.agencyzed.com/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.12.2" type="text/javascript"></script>
<script id="jquery-core-js" src="https://www.agencyzed.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<link href="https://www.agencyzed.com/wp-json/" rel="https://api.w.org/"/><link href="https://www.agencyzed.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.agencyzed.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/> <style type="text/css">
		h1, h2, h3, h4, h5, h6, ul.nav a, #featured h2.title, #tagline, h3.hometitle, .entry h2.title, .entry h1.title, .comment-author, h3#comments, #reply-title span, .entry h1, .entry h2, .entry h3, .entry h4, .entry h5, .entry h6, div.service a.readmore, #portfolio-items a.readmore, .entry a.readmore, .reply-container a, #blog-content h4.widgettitle, h4.widgettitle, .wp-pagenavi { font-family: '', ; }body { font-family: '', ; }		</style>
<style type="text/css">
		div.pp_default .pp_content_container .pp_details { color: #666; }
	</style>
<link href="https://www.agencyzed.com/wp-content/cache/et/global/et--customizer-global-160831026734.min.css" id="et--customizer-global-cached-inline-styles" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" rel="stylesheet"/>
</head>
<body class="error404 et_includes_sidebar">
<div id="top">
<div class="container">
<div id="header">
<a href="https://www.agencyzed.com/">
<img alt="Agency Zed Productions Inc" id="logo" src="http://www.agencyzed.com/wp-content/uploads/2013/01/agencyzed_bk-1.png"/>
</a>
<div class="clearfix" id="menu">
<ul class="nav" id="primary">
<li><a href="https://www.agencyzed.com/">Home</a></li>
<li class="page_item page-item-2"><a href="https://www.agencyzed.com/sample-page">Careers</a></li>
<li class="page_item page-item-162"><a href="https://www.agencyzed.com/about">Company</a></li>
<li class="page_item page-item-675"><a href="https://www.agencyzed.com/contact-us">Contact us</a></li>
<li class="cat-item cat-item-2"><a href="https://www.agencyzed.com/archives/category/blog">Blog</a>
</li>
<li class="cat-item cat-item-4"><a href="https://www.agencyzed.com/archives/category/portfolio">Portfolio</a>
<ul class="children">
<li class="cat-item cat-item-50"><a href="https://www.agencyzed.com/archives/category/portfolio/online">Online</a>
</li>
<li class="cat-item cat-item-49"><a href="https://www.agencyzed.com/archives/category/portfolio/video">Video</a>
</li>
</ul>
</li>
</ul> <!-- end ul#nav -->
<div id="search-form">
<form action="https://www.agencyzed.com/" id="searchform" method="get">
<input id="searchinput" name="s" type="text" value="search this site..."/>
<input id="searchsubmit" src="https://www.agencyzed.com/wp-content/themes/DeepFocus/images/search-btn.png" type="image"/>
</form>
</div> <!-- end #search-form -->
<a class="closed" href="#" id="mobile_nav">Navigation Menu<span></span></a> </div> <!-- end #menu -->
</div> <!-- end #header -->
</div> <!-- end .container -->
</div> <!-- end #top --> <div id="content-full">
<div id="hr">
<div id="hr-center">
<div id="intro">
<div class="center-highlight">
<div class="container">
<div id="breadcrumbs">
<a href="https://www.agencyzed.com/">Home</a> <span class="raquo">»</span>

									No results found					
</div> <!-- end #breadcrumbs -->
</div> <!-- end .container -->
</div> <!-- end .center-highlight -->
</div> <!-- end #intro -->
</div> <!-- end #hr-center -->
</div> <!-- end #hr -->
<div class="center-highlight">
<div class="container">
<div class="clearfix" id="content-area">
<div id="left-area">
<div class="entry">
<h1 class="title">No Results Found</h1>
<p>The page you requested could not be found. Try refining your search, or use the navigation above to locate the post.</p>
</div>
</div> <!-- end #left-area -->
<div id="sidebar">
<div class="widget widget_recent_entries" id="recent-posts-2">
<h4 class="widgettitle">Recent Work</h4>
<ul>
<li>
<a href="https://www.agencyzed.com/archives/1084">SMART Journey</a>
</li>
<li>
<a href="https://www.agencyzed.com/archives/1066">ReaderBound.com</a>
</li>
<li>
<a href="https://www.agencyzed.com/archives/1057">5 Keys to Delivering Great Products</a>
</li>
<li>
<a href="https://www.agencyzed.com/archives/1033">Turner-Riggs.com</a>
</li>
<li>
<a href="https://www.agencyzed.com/archives/983">Build your Collaboration Solution</a>
</li>
</ul>
</div><!-- end .widget --><div class="widget widget_archive" id="archives-2"><h4 class="widgettitle">Archives</h4>
<ul>
<li><a href="https://www.agencyzed.com/archives/date/2018/02">February 2018</a></li>
<li><a href="https://www.agencyzed.com/archives/date/2016/12">December 2016</a></li>
<li><a href="https://www.agencyzed.com/archives/date/2016/05">May 2016</a></li>
<li><a href="https://www.agencyzed.com/archives/date/2015/05">May 2015</a></li>
<li><a href="https://www.agencyzed.com/archives/date/2014/06">June 2014</a></li>
<li><a href="https://www.agencyzed.com/archives/date/2014/02">February 2014</a></li>
<li><a href="https://www.agencyzed.com/archives/date/2013/03">March 2013</a></li>
<li><a href="https://www.agencyzed.com/archives/date/2012/04">April 2012</a></li>
<li><a href="https://www.agencyzed.com/archives/date/2011/09">September 2011</a></li>
<li><a href="https://www.agencyzed.com/archives/date/2010/09">September 2010</a></li>
<li><a href="https://www.agencyzed.com/archives/date/2008/09">September 2008</a></li>
<li><a href="https://www.agencyzed.com/archives/date/2008/08">August 2008</a></li>
<li><a href="https://www.agencyzed.com/archives/date/2008/07">July 2008</a></li>
<li><a href="https://www.agencyzed.com/archives/date/2008/01">January 2008</a></li>
<li><a href="https://www.agencyzed.com/archives/date/2006/09">September 2006</a></li>
</ul>
</div><!-- end .widget --><div class="widget widget_categories" id="categories-2"><h4 class="widgettitle">Categories</h4>
<ul>
<li class="cat-item cat-item-2"><a href="https://www.agencyzed.com/archives/category/blog">Blog</a>
</li>
<li class="cat-item cat-item-3"><a href="https://www.agencyzed.com/archives/category/featured">Featured</a>
</li>
<li class="cat-item cat-item-50"><a href="https://www.agencyzed.com/archives/category/portfolio/online">Online</a>
</li>
<li class="cat-item cat-item-4"><a href="https://www.agencyzed.com/archives/category/portfolio">Portfolio</a>
</li>
<li class="cat-item cat-item-49"><a href="https://www.agencyzed.com/archives/category/portfolio/video">Video</a>
</li>
</ul>
</div><!-- end .widget -->
</div> <!-- end #sidebar -->
</div> <!-- end #content-area -->
</div> <!-- end .container -->
<div id="footer">
<div id="footer-wrapper">
<div id="footer-center">
<div class="container">
<div class="clearfix" id="footer-widgets">
</div> <!-- end #footer-widgets -->
<!-- <p id="copyright">Designed by  <a href="http://www.elegantthemes.com" title="Elegant Themes">Elegant Themes</a> | Powered by  <a href="http://www.wordpress.org">Wordpress</a></p> -->
<p id="copyright">Copyright © 2019 <a href="http://www.agencyzed.com" title="Agency Zed">Agency Zed Productions Inc.</a></p>
</div> <!-- end .container -->
</div> <!-- end #footer-center -->
</div> <!-- end #footer-wrapper -->
</div> <!-- end #footer -->
</div> <!-- end .center-highlight -->
</div> <!-- end #content-full -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js" type="text/javascript"></script>
<script src="https://www.agencyzed.com/wp-content/themes/DeepFocus/js/jquery.cycle.all.min.js" type="text/javascript"></script>
<script src="https://www.agencyzed.com/wp-content/themes/DeepFocus/js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="https://www.agencyzed.com/wp-content/themes/DeepFocus/js/superfish.js" type="text/javascript"></script>
<script src="https://www.agencyzed.com/wp-content/themes/DeepFocus/js/scrollTo.js" type="text/javascript"></script>
<script src="https://www.agencyzed.com/wp-content/themes/DeepFocus/js/serialScroll.js" type="text/javascript"></script>
<script type="text/javascript">
   //<![CDATA[
      jQuery.noConflict();

      jQuery('ul.nav').superfish({
         delay:       300,                            // one second delay on mouseout
         animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation
         speed:       'fast',                          // faster animation speed
         autoArrows:  true,                           // disable generation of arrow mark-up
         dropShadows: false                            // disable drop shadows
      });

      jQuery('ul.nav > li > a.sf-with-ul').parent('li').addClass('sf-ul');

      et_search_bar();

      if (jQuery('#blog').length) {
         jQuery('#blog').serialScroll({
            target:'.recentscroll',
            items:'li', // Selector to the items ( relative to the matched elements, '#sections' in this case )
            prev:'#controllers2 a#cright-arrow',// Selector to the 'prev' button (absolute!, meaning it's relative to the document)
            next:'#controllers2 a#cleft-arrow',// Selector to the 'next' button (absolute too)
            axis:'y',// The default is 'y' scroll on both ways
            duration:200,// Length of the animation (if you scroll 2 axes and use queue, then each axis take half this time)
            force:true // Force a scroll to the element specified by 'start' (some browsers don't reset on refreshes)
         });
      }

      var $portfolioItem = jQuery('.item');
      $portfolioItem.find('.item-image').css('background-color','#000000');
      jQuery('.zoom-icon, .more-icon').css('opacity','0');

      $portfolioItem.hover(function(){
         jQuery(this).find('.item-image').stop(true, true).animate({top: -10}, 500).find('img.portfolio').stop(true, true).animate({opacity: 0.7},500);
         jQuery(this).find('.zoom-icon').stop(true, true).animate({opacity: 1, left: 43},400);
         jQuery(this).find('.more-icon').stop(true, true).animate({opacity: 1, left: 110},400);
      }, function(){
         jQuery(this).find('.zoom-icon').stop(true, true).animate({opacity: 0, left: 31},400);
         jQuery(this).find('.more-icon').stop(true, true).animate({opacity: 0, left: 128},400);
         jQuery(this).find('.item-image').stop(true, true).animate({top: 0}, 500).find('img.portfolio').stop(true, true).animate({opacity: 1},500);
      });

      
      function et_cycle_integration(){
         var $featured = jQuery('#featured'),
            $featured_content = jQuery('#slides'),
            $controller = jQuery('#controllers'),
            $slider_control_tab = $controller.find('a.switch');

         if ($featured_content.length) {
            jQuery('div.slide .description').css({'visibility':'hidden','opacity':'0'});

         $featured_content.css( 'backgroundImage', 'none' );
         if ( $featured_content.find('.slide').length == 1 ){
            $featured_content.find('.slide').css({'position':'absolute','top':'0','left':'0'}).show();
            jQuery('#controllers-wrapper').hide();
         }

            $featured_content.cycle({
               fx: 'fade',
               timeout: 0,
               speed: 700,
               cleartypeNoBg: true
            });

         var et_leftMargin = parseFloat( $featured.width()- $featured.find('#controllers-wrapper').outerWidth(true)) / 2;
         $featured.find('#controllers-wrapper').css({'left' : et_leftMargin});

            var pause_scroll = false;

         jQuery('#featured .slide').hover(function(){
            jQuery('div.slide:visible .description').css('visibility','visible').stop().animate({opacity: 1, top:43},500);
            pause_scroll = true;
         },function(){
            jQuery('div.slide:visible .description').stop().animate({opacity: 0, top:33},500,function(){
              jQuery(this).css('visibility','hidden');
            });
            pause_scroll = false;
         });
         };

         $slider_control_tab.hover(function(){
            jQuery(this).find('img').stop().animate({opacity: 1},300);
         }).mouseleave(function(){
            if (!jQuery(this).hasClass("active")) jQuery(this).find('img').stop().animate({opacity: 0.7},300);
         });


         var ordernum;

         function gonext(this_element){
            $controller.find("a.active").removeClass('active');

            this_element.addClass('active');

            ordernum = this_element.attr("rel");
            $featured_content.cycle(ordernum-1);

            //SjQuery('div.slide:visible .description').stop().animate({opacity: 0, top:33},500);

            if (typeof interval != 'undefined') {
               clearInterval(interval);
               auto_rotate();
            };
         }

         $slider_control_tab.click(function(){
            gonext(jQuery(this));
            return false;
         });


         var $nextArrow = $featured.find('a#right-arrow'),
            $prevArrow = $featured.find('a#left-arrow');

         $nextArrow.click(function(){
            var activeSlide = $controller.find('a.active').attr("rel"),
               $nextSlide = $controller.find('a.switch:eq('+ activeSlide +')');

            if ($nextSlide.length) gonext($nextSlide)
            else gonext($controller.find('a.switch:eq(0)'));

            return false;
         });

         $prevArrow.click(function(){
            var activeSlide = $controller.find('a.active').attr("rel")-2,
               $nextSlide = $controller.find('a.switch:eq('+ activeSlide +')');

            if ($nextSlide.length) gonext($nextSlide);
            else {
               var slidesNum = $slider_control_tab.length - 1;
               gonext($controller.find('a.switch:eq('+ slidesNum +')'));
            };

            return false;
         });


         
            auto_rotate();

            function auto_rotate(){

               interval = setInterval(function() {
                  if (!pause_scroll) $nextArrow.click();
               }, 3500);
            }

         
      };


      <!---- Search Bar Improvements ---->
      function et_search_bar(){
         var $searchform = jQuery('#menu div#search-form'),
            $searchinput = $searchform.find("input#searchinput"),
            searchvalue = $searchinput.val();

         $searchinput.focus(function(){
            if (jQuery(this).val() === searchvalue) jQuery(this).val("");
         }).blur(function(){
            if (jQuery(this).val() === "") jQuery(this).val(searchvalue);
         });
      };

      var $footer_widget = jQuery("#footer .widget");

      if ($footer_widget.length) {
         $footer_widget.each(function (index, domEle) {
            // domEle == this
            if ((index+1)%3 == 0) jQuery(domEle).addClass("last").after("<div class='clear'></div>");
         });
      };

      jQuery(window).load(function(){
         var $single_gallery_thumb = jQuery('.gallery-thumb');
         
         if ($single_gallery_thumb.length) {
           var single_gallery_thumb = $single_gallery_thumb.width(),
             offset = single_gallery_thumb-434;

           if ( offset < 0 ) {
             jQuery('.gallery-thumb-bottom').css({'width':'auto','padding':'0 '+(single_gallery_thumb / 2)+'px'});

           }
           else jQuery('.gallery-thumb-bottom').css('width',offset);
         }
      });
   //]]>
   </script>
<script id="flexslider-js" src="https://www.agencyzed.com/wp-content/themes/DeepFocus/js/jquery.flexslider-min.js?ver=1.0" type="text/javascript"></script>
<script id="et_flexslider_script-js" src="https://www.agencyzed.com/wp-content/themes/DeepFocus/js/et_flexslider.js?ver=1.0" type="text/javascript"></script>
<script id="wpgmp-google-api-js" src="https://maps.google.com/maps/api/js?libraries=geometry%2Cplaces%2Cweather%2Cpanoramio%2Cdrawing&amp;language=en&amp;ver=ef17189b58bb47b20a7e4ddea1fa4d99" type="text/javascript"></script>
<script id="wpgmp-google-map-main-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpgmp_local = {"all_location":"All","show_locations":"Show Locations","sort_by":"Sort by","wpgmp_not_working":"Not working...","place_icon_url":"https:\/\/www.agencyzed.com\/wp-content\/plugins\/wp-google-map-plugin\/assets\/images\/icons\/"};
/* ]]> */
</script>
<script id="wpgmp-google-map-main-js" src="https://www.agencyzed.com/wp-content/plugins/wp-google-map-plugin/assets/js/maps.js?ver=2.3.4" type="text/javascript"></script>
<script id="et-core-common-js" src="https://www.agencyzed.com/wp-content/themes/DeepFocus/core/admin/js/common.js?ver=3.20.2" type="text/javascript"></script>
<script id="easing-js" src="https://www.agencyzed.com/wp-content/themes/DeepFocus/includes/page_templates/js/jquery.easing-1.3.pack.js?ver=1.3.4" type="text/javascript"></script>
<script id="magnific_popup-js" src="https://www.agencyzed.com/wp-content/themes/DeepFocus/includes/page_templates/js/magnific_popup/jquery.magnific-popup.js?ver=1.3.4" type="text/javascript"></script>
<script id="et-ptemplates-frontend-js-extra" type="text/javascript">
/* <![CDATA[ */
var et_ptemplates_strings = {"captcha":"Captcha","fill":"Fill","field":"field","invalid":"Invalid email"};
/* ]]> */
</script>
<script id="et-ptemplates-frontend-js" src="https://www.agencyzed.com/wp-content/themes/DeepFocus/includes/page_templates/js/et-ptemplates-frontend.js?ver=1.1" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.agencyzed.com/wp-includes/js/wp-embed.min.js?ver=ef17189b58bb47b20a7e4ddea1fa4d99" type="text/javascript"></script>
</body>
</html>