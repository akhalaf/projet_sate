<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Feltrex, Pioneros de la calidad en Republica Dominicana</title>
<link href="https://feltrex.com.do/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://feltrex.com.do/favicon.ico" rel="icon" type="image/x-icon"/>
<!-- Bootstrap -->
<link href="https://feltrex.com.do/wp-content/themes/feltrex/css/bootstrap.min.css" media="all" rel="stylesheet"/>
<link href="https://feltrex.com.do/wp-content/themes/feltrex/css/lightbox.min.css" media="all" rel="stylesheet"/>
<link href="https://feltrex.com.do/wp-content/themes/feltrex/style.css" media="all" rel="stylesheet"/>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://feltrex.com.do/wp-content/themes/feltrex/js/bootstrap.min.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-97760076-1', 'auto');
  ga('send', 'pageview');
</script>
<script id="navegg" src="https://tag.navdmp.com/tm45668.js" type="text/javascript"></script>
<script src="https://feltrex.com.do/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://feltrex.com.do/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://feltrex.com.do/wp-content/plugins/lightbox-gallery/js/jquery.colorbox.js?ver=4.8.2" type="text/javascript"></script>
<script src="https://feltrex.com.do/wp-content/plugins/lightbox-gallery/js/jquery.tooltip.js?ver=4.8.2" type="text/javascript"></script>
<script src="https://feltrex.com.do/wp-content/plugins/lightbox-gallery/lightbox-gallery.js?ver=4.8.2" type="text/javascript"></script>
<link href="https://feltrex.com.do/wp-content/plugins/lightbox-gallery/lightbox-gallery.css" rel="stylesheet" type="text/css"/>
</head>
<body class="error404" data-rsssl="1">
<nav class="navbar navbar-inverse navbar-feltrex navbar-fixed-top">
<div class="container container-w">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<button aria-expanded="false" class="navbar-toggle collapsed" data-target="#primary-navbar-collapse-1" data-toggle="collapse" type="button">
<span class="sr-only">Desplegar menus</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="https://feltrex.com.do">
<img alt="Feltrex, Pioneros de la calidad en Republica Dominicana" src="https://feltrex.com.do/wp-content/themes/feltrex/images/logo.png"/>
</a>
</div>
<div class="collapse navbar-collapse" id="primary-navbar-collapse-1">
<form action="https://feltrex.com.do" class="navbar-form navbar-right" method="get">
<div class="form-group has-feedback">
<label class="control-label sr-only" for="inputSearch">Input with success</label>
<input aria-describedby="searchStatus" class="form-control" id="inputSearch" name="s" type="text"/>
<span aria-hidden="true" class="glyphicon glyphicon-search form-control-feedback"></span>
<span class="sr-only" id="searchStatus">(success)</span>
</div>
</form>
<div class="iso-check hidden-xs">
<img src="https://feltrex.com.do/wp-content/themes/feltrex/images/iso-mini-1.jpg"/>
</div>
<div class=" " id="bs-primary-navbar-collapse-1"><ul class="nav navbar-nav pull-right" id="menu-primary"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5" id="menu-item-5"><a href="https://feltrex.com.do/nosotros/" title="Nosotros">Nosotros</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-49" id="menu-item-49"><a href="https://feltrex.com.do/category/productos/" title="Productos">Productos</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-9" id="menu-item-9"><a href="https://feltrex.com.do/category/blog/" title="Blog">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-165" id="menu-item-165"><a href="https://feltrex.com.do/responsabilidad-social/" title="RSE">RSE</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8" id="menu-item-8"><a href="https://feltrex.com.do/contacto/" title="Contacto">Contacto</a></li>
</ul></div> </div>
</div>
</nav>
<br/><br/><br/>
<div class="container" style="text-align: center; padding: 100px 0 50px 0;">
<h1>404</h1>
<p>La página solicitada no existe</p>
</div>
<div id="footer">
<div class="container container-w small">
<div class="row">
<div class="col-sm-5">
<div class="social-container">
<a class="social-btn" href="https://www.facebook.com/feltrexrd/" target="_blank"><img src="https://feltrex.com.do/wp-content/themes/feltrex/images/facebook.png"/></a>
<a class="social-btn" href="https://www.instagram.com/feltrexrd/" target="_blank"><img src="https://feltrex.com.do/wp-content/themes/feltrex/images/instagram.png"/></a>
<a class="social-btn" href="https://twitter.com/feltrexrd" target="_blank"><img src="https://feltrex.com.do/wp-content/themes/feltrex/images/twitter.png"/></a>
</div>
<div>
<strong>Santo Domingo</strong>, Roberto Pastoriza 654, Evaristo Morales <strong class="text-success">T</strong> 809.567.1510<br/>
<strong class="text-success">T</strong> 1.809.200.1436 (sin cargos) <strong>República Dominicana</strong> <strong class="text-success">M</strong> tusalud@feltrex.com.do<br/>
<strong>RNC: 1-01-05037-3</strong>
</div>
</div>
<div class="col-sm-7 text-right ">
<span class="tree pull-right hidden-xs">
<img src="https://feltrex.com.do/wp-content/themes/feltrex/images/footer-tree.png"/>
</span>
<span class="pull-right tree-text">
<span class="text-success">Un Árbol</span><br/>
<span>es el más noble ejemplo de vida. Cuidando lo más preciado del ser humano, su salud.</span>
</span>
</div>
</div>
</div>
</div>
<script src="https://feltrex.com.do/wp-content/themes/feltrex/js/lightbox.min.js" type="text/javascript"></script>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-107907909-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-107907909-1');
</script>
</body>
</html>