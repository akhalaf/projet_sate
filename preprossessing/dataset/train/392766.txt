<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" class="ancient-ie old-ie no-js" lang="it-IT" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<![endif]--><!--[if IE 7]>
<html id="ie7" class="ancient-ie old-ie no-js" lang="it-IT" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<![endif]--><!--[if IE 8]>
<html id="ie8" class="old-ie no-js" lang="it-IT" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<![endif]--><!--[if IE 9]>
<html id="ie9" class="old-ie9 no-js" lang="it-IT" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<![endif]--><!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!--><html class="no-js" lang="it-IT" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<script type="text/javascript">
function createCookie(a,d,b){if(b){var c=new Date;c.setTime(c.getTime()+864E5*b);b="; expires="+c.toGMTString()}else b="";document.cookie=a+"="+d+b+"; path=/"}function readCookie(a){a+="=";for(var d=document.cookie.split(";"),b=0;b<d.length;b++){for(var c=d[b];" "==c.charAt(0);)c=c.substring(1,c.length);if(0==c.indexOf(a))return c.substring(a.length,c.length)}return null}function eraseCookie(a){createCookie(a,"",-1)}
function areCookiesEnabled(){var a=!1;createCookie("testing","Hello",1);null!=readCookie("testing")&&(a=!0,eraseCookie("testing"));return a}(function(a){var d=readCookie("devicePixelRatio"),b=void 0===a.devicePixelRatio?1:a.devicePixelRatio;areCookiesEnabled()&&null==d&&(createCookie("devicePixelRatio",b,7),1!=b&&a.location.reload(!0))})(window);
</script> <link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.dataiadela.it/xmlrpc.php" rel="pingback"/>
<!--[if IE]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-125957382-9"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125957382-9');
</script>
<title>404 Not Found | Ristorante Da Taiadèla</title>
<!-- SEO Ultimate (http://www.seodesignsolutions.com/wordpress-seo/) -->
<!-- /SEO Ultimate -->
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.dataiadela.it/feed/" rel="alternate" title="Ristorante Da Taiadèla » Feed" type="application/rss+xml"/>
<link href="https://www.dataiadela.it/comments/feed/" rel="alternate" title="Ristorante Da Taiadèla » Feed dei commenti" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.dataiadela.it\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.16"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.dataiadela.it/wp-content/plugins/LayerSlider/static/css/layerslider.css?ver=5.3.2" id="layerslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&amp;subset=latin%2Clatin-ext" id="ls-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.dataiadela.it/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.7" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.dataiadela.it/wp-content/plugins/go_pricing/assets/css/go_pricing_styles.css?ver=2.4.5" id="go_pricing_styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.dataiadela.it/wp-content/plugins/go_pricing/assets/plugins/js/mediaelementjs/mediaelementplayer.min.css?ver=2.4.5" id="go_pricing_jqplugin-mediaelementjs-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.dataiadela.it/wp-content/plugins/go_pricing/assets/plugins/js/mediaelementjs/skin/mediaelementplayer.css?ver=2.4.5" id="go_pricing_jqplugin-mediaelementjs-skin-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.dataiadela.it/wp-content/plugins/recent-tweets-widget/tp_twitter_plugin.css?ver=1.0" id="tp_twitter_plugin_css-css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.dataiadela.it/wp-content/plugins/revslider/rs-plugin/css/settings.css?ver=4.6.92" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
</style>
<link href="https://fonts.googleapis.com/css?family=Roboto%3A400%2C400italic%2C500%7COpen+Sans&amp;ver=4.9.16" id="dt-web-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.dataiadela.it/wp-content/themes/dt-the7/css/main.min.css?ver=2.1.3" id="dt-main-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='dt-old-ie-css'  href='https://www.dataiadela.it/wp-content/themes/dt-the7/css/old-ie.min.css?ver=2.1.3' type='text/css' media='all' />
<![endif]-->
<link href="https://www.dataiadela.it/wp-content/themes/dt-the7/css/font-awesome.min.css?ver=2.1.3" id="dt-awsome-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.dataiadela.it/wp-content/themes/dt-the7/css/fontello/css/fontello.css?ver=2.1.3" id="dt-fontello-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='dt-custom-old-ie.less-css'  href='https://www.dataiadela.it/wp-content/uploads/wp-less/dt-the7/css/custom-old-ie-0a906add4c.css?ver=2.1.3' type='text/css' media='all' />
<![endif]-->
<link href="https://www.dataiadela.it/wp-content/uploads/wp-less/dt-the7/css/main-789b205cc6.css?ver=2.1.3" id="dt-main.less-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.dataiadela.it/wp-content/uploads/wp-less/dt-the7/css/custom-0a906add4c.css?ver=2.1.3" id="dt-custom.less-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.dataiadela.it/wp-content/uploads/wp-less/dt-the7/css/media-789b205cc6.css?ver=2.1.3" id="dt-media.less-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.dataiadela.it/wp-content/themes/dt-the7/style.css?ver=2.1.3" id="style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="style-inline-css" type="text/css">
.post-content .text-secondary {
display:none;}
.border-box{
    border-bottom: 10px solid #61100c !important;
 border-top: 10px solid #61100c  !important;
background:#7a120d;
}


.border-box-left{

 border-left: 10px solid #61100c  !important;
background:#7a120d;
}
.border-box-box{
border:1px solid #E2ACA6 !important;
margin-top:40px;
}


.servizi-margin{
margin-top:75px !important;

}
.border-top-box{
border-top:5px solid #7a120d; !important;
}

.shadow-boxes{
box-shadow: 0px 0px 23px -10px rgba(0,0,0, 0.75); 
margin:10px;


}

.margin-top{
margin-top:30px;
}




a.ultb3-btn {
    padding: 10px 20px !important;
}

a.slider-slide {
    background-color: #61100c;
    padding: 10px 25px;
    color: white;
}
#main-nav > li {
    margin: 0 6px !important;
}













</style>
<link href="https://www.dataiadela.it/wp-content/uploads/smile_fonts/Defaults/Defaults.css?ver=4.9.16" id="bsf-Defaults-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.dataiadela.it/wp-content/plugins/LayerSlider/static/js/greensock.js?ver=1.11.8" type="text/javascript"></script>
<script src="https://www.dataiadela.it/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.dataiadela.it/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.dataiadela.it/wp-content/plugins/LayerSlider/static/js/layerslider.kreaturamedia.jquery.js?ver=5.3.2" type="text/javascript"></script>
<script src="https://www.dataiadela.it/wp-content/plugins/LayerSlider/static/js/layerslider.transitions.js?ver=5.3.2" type="text/javascript"></script>
<script type="text/javascript">
var mejsL10n = {"language":"it","strings":{"mejs.install-flash":"Stai usando un browser che non ha Flash player abilitato o installato. Attiva il tuo plugin Flash player o scarica l'ultima versione da https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen-off":"Disattiva lo schermo intero","mejs.fullscreen-on":"Vai a tutto schermo","mejs.download-video":"Scarica il video","mejs.fullscreen":"Schermo intero","mejs.time-jump-forward":["Vai avanti di 1 secondo","Salta in avanti di %1 secondi"],"mejs.loop":"Attiva\/disattiva la riproduzione automatica","mejs.play":"Play","mejs.pause":"Pausa","mejs.close":"Chiudi","mejs.time-slider":"Time Slider","mejs.time-help-text":"Usa i tasti freccia sinistra\/destra per avanzare di un secondo, su\/gi\u00f9 per avanzare di 10 secondi.","mejs.time-skip-back":["Torna indietro di 1 secondo","Vai indietro di %1 secondi"],"mejs.captions-subtitles":"Didascalie\/Sottotitoli","mejs.captions-chapters":"Capitoli","mejs.none":"Nessuna","mejs.mute-toggle":"Cambia il muto","mejs.volume-help-text":"Usa i tasti freccia su\/gi\u00f9 per aumentare o diminuire il volume.","mejs.unmute":"Togli il muto","mejs.mute":"Muto","mejs.volume-slider":"Cursore del volume","mejs.video-player":"Video Player","mejs.audio-player":"Audio Player","mejs.ad-skip":"Salta pubblicit\u00e0","mejs.ad-skip-info":["Salta in 1 secondo","Salta in %1 secondi"],"mejs.source-chooser":"Scelta sorgente","mejs.stop":"Stop","mejs.speed-rate":"Velocit\u00e0 di riproduzione","mejs.live-broadcast":"Diretta streaming","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanese","mejs.arabic":"Arabo","mejs.belarusian":"Bielorusso","mejs.bulgarian":"Bulgaro","mejs.catalan":"Catalano","mejs.chinese":"Cinese","mejs.chinese-simplified":"Cinese (semplificato)","mejs.chinese-traditional":"Cinese (tradizionale)","mejs.croatian":"Croato","mejs.czech":"Ceco","mejs.danish":"Danese","mejs.dutch":"Olandese","mejs.english":"Inglese","mejs.estonian":"Estone","mejs.filipino":"Filippino","mejs.finnish":"Finlandese","mejs.french":"Francese","mejs.galician":"Galician","mejs.german":"Tedesco","mejs.greek":"Greco","mejs.haitian-creole":"Haitian Creole","mejs.hebrew":"Ebraico","mejs.hindi":"Hindi","mejs.hungarian":"Ungherese","mejs.icelandic":"Icelandic","mejs.indonesian":"Indonesiano","mejs.irish":"Irish","mejs.italian":"Italiano","mejs.japanese":"Giapponese","mejs.korean":"Coreano","mejs.latvian":"Lettone","mejs.lithuanian":"Lituano","mejs.macedonian":"Macedone","mejs.malay":"Malese","mejs.maltese":"Maltese","mejs.norwegian":"Norvegese","mejs.persian":"Persiano","mejs.polish":"Polacco","mejs.portuguese":"Portoghese","mejs.romanian":"Romeno","mejs.russian":"Russo","mejs.serbian":"Serbo","mejs.slovak":"Slovak","mejs.slovenian":"Sloveno","mejs.spanish":"Spagnolo","mejs.swahili":"Swahili","mejs.swedish":"Svedese","mejs.tagalog":"Tagalog","mejs.thai":"Thailandese","mejs.turkish":"Turco","mejs.ukrainian":"Ucraino","mejs.vietnamese":"Vietnamita","mejs.welsh":"Gallese","mejs.yiddish":"Yiddish"}};
</script>
<script src="https://www.dataiadela.it/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.6-78496d1" type="text/javascript"></script>
<script src="https://www.dataiadela.it/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=4.9.16" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
/* ]]> */
</script>
<script src="https://www.dataiadela.it/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.tools.min.js?ver=4.6.92" type="text/javascript"></script>
<script src="https://www.dataiadela.it/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js?ver=4.6.92" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var dtLocal = {"passText":"Per visualizzare questo post protetto, inserire la password qui sotto:","moreButtonText":{"loading":"Caricamento..."},"postID":"687","ajaxurl":"https:\/\/www.dataiadela.it\/wp-admin\/admin-ajax.php","contactNonce":"ea7f8bab67","ajaxNonce":"5cc136005b","pageData":"","themeSettings":{"smoothScroll":"off","lazyLoading":false,"accentColor":{"mode":"gradient","color":["#661712","#7f0000"]},"mobileHeader":{"firstSwitchPoint":970},"content":{"responsivenessTreshold":970,"textColor":"#777777","headerColor":"#333333"},"stripes":{"stripe1":{"textColor":"#777777","headerColor":"#333333"},"stripe2":{"textColor":"#f9f9f9","headerColor":"#f9f9f9"},"stripe3":{"textColor":"#2f363d","headerColor":"#2f363d"}}}};
/* ]]> */
</script>
<script src="https://www.dataiadela.it/wp-content/themes/dt-the7/js/above-the-fold.min.js?ver=2.1.3" type="text/javascript"></script>
<link href="https://www.dataiadela.it/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.dataiadela.it/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.dataiadela.it/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.16" name="generator"/>
<script type="text/javascript">
			jQuery(document).ready(function() {
				// CUSTOM AJAX CONTENT LOADING FUNCTION
				var ajaxRevslider = function(obj) {
				
					// obj.type : Post Type
					// obj.id : ID of Content to Load
					// obj.aspectratio : The Aspect Ratio of the Container / Media
					// obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content
					
					var content = "";

					data = {};
					
					data.action = 'revslider_ajax_call_front';
					data.client_action = 'get_slider_html';
					data.token = 'dcd99579c5';
					data.type = obj.type;
					data.id = obj.id;
					data.aspectratio = obj.aspectratio;
					
					// SYNC AJAX REQUEST
					jQuery.ajax({
						type:"post",
						url:"https://www.dataiadela.it/wp-admin/admin-ajax.php",
						dataType: 'json',
						data:data,
						async:false,
						success: function(ret, textStatus, XMLHttpRequest) {
							if(ret.success == true)
								content = ret.data;								
						},
						error: function(e) {
							console.log(e);
						}
					});
					
					 // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
					 return content;						 
				};
				
				// CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
				var ajaxRemoveRevslider = function(obj) {
					return jQuery(obj.selector+" .rev_slider").revkill();
				};

				// EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
				var extendessential = setInterval(function() {
					if (jQuery.fn.tpessential != undefined) {
						clearInterval(extendessential);
						if(typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
							jQuery.fn.tpessential.defaults.ajaxTypes.push({type:"revslider",func:ajaxRevslider,killfunc:ajaxRemoveRevslider,openAnimationSpeed:0.3});   
							// type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
							// func: the Function Name which is Called once the Item with the Post Type has been clicked
							// killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
							// openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
						}
					}
				},30);
			});
		</script>
<script type="text/javascript">
			dtGlobals.logoEnabled = 1;
			dtGlobals.logoURL = 'https://www.dataiadela.it/wp-content/uploads/2015/05/mobi.png 1x';
			dtGlobals.logoW = '181';
			dtGlobals.logoH = '40';
			smartMenu = 1;
		</script>
<meta content="Powered by Visual Composer - drag and drop page builder for WordPress." name="generator"/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://www.dataiadela.it/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="https://www.dataiadela.it/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>
<body class="error404 image-blur rollover-show-icon accent-gradient srcset-enabled btn-material style-ios boxes-solid-bg bold-icons phantom-fade wpb-js-composer js-comp-ver-4.12 vc_responsive">
<div id="load"><div class="pace pace-active"><div class="pace-activity"></div></div></div>
<div id="page">
<!-- left, center, classic, side -->
<!-- !Header -->
<header class="solid-bg show-device-logo show-mobile-logo dt-parent-menu-clickable shadow-decoration logo-center" id="header" role="banner"><!-- class="overlap"; class="logo-left", class="logo-center", class="logo-classic" -->
<!-- !Top-bar -->
<div class="text-small solid-bg top-bar-hide" id="top-bar" role="complementary">
<div class="wf-wrap">
<div class="wf-container-top">
<div class="wf-table wf-mobile-collapsed">
<div class=" wf-td"><span class="mini-contacts address">Via Turati 1, 40019 - S.Agata Bolognese (BO)</span><span class="mini-contacts phone">051 95 66 80</span><span class="mini-contacts email">info@dataiadela.it</span></div>
</div><!-- .wf-table -->
</div><!-- .wf-container-top -->
</div><!-- .wf-wrap -->
</div><!-- #top-bar -->
<div class="wf-wrap">
<div class="wf-table">
<div class="wf-td">
<!-- !- Branding -->
<div class="wf-td" id="branding">
<a href="https://www.dataiadela.it/"><img alt="Ristorante Da Taiadèla" class=" preload-me" height="70" srcset="https://www.dataiadela.it/wp-content/uploads/2015/05/med.png 1x, https://www.dataiadela.it/wp-content/uploads/2015/05/retina.png 2x" width="316"/><img alt="Ristorante Da Taiadèla" class="mobile-logo preload-me" height="70" srcset="https://www.dataiadela.it/wp-content/uploads/2015/05/med.png 1x, https://www.dataiadela.it/wp-content/uploads/2015/05/retina.png 2x" width="316"/></a>
<div class="assistive-text" id="site-title">Ristorante Da Taiadèla</div>
<div class="assistive-text" id="site-description">Una passione tutta Emiliana</div>
</div>
</div><!-- .wf-td -->
</div><!-- .wf-table -->
</div><!-- .wf-wrap -->
<div class="navigation-holder">
<div class="wf-wrap ">
<!-- !- Navigation -->
<nav id="navigation">
<ul class="fancy-rollovers wf-mobile-hidden gradient-decor underline-hover" id="main-nav">
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-218 first"><a href="https://www.dataiadela.it/"><span>Home</span></a></li>
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-844 has-children"><a href="https://www.dataiadela.it/il-ristorante-2/"><span>Il Ristorante</span></a><div class="sub-nav gradient-hover"><ul>
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-842 first has-children level-arrows-on"><a href="https://www.dataiadela.it/menu-2/"><span>Menu</span></a><div class="sub-nav gradient-hover"><ul>
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-843 first level-arrows-on"><a href="https://www.dataiadela.it/la-nostra-pasta-sfoglia-2/"><span>La Nostra Pasta Sfoglia</span></a></li>
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-861 level-arrows-on"><a href="https://www.dataiadela.it/braceria-2/"><span>Braceria</span></a></li>
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-839 level-arrows-on"><a href="https://www.dataiadela.it/da-asporto/"><span>Da asporto</span></a></li> </ul></div></li> </ul></div></li>
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-841 has-children"><a href="https://www.dataiadela.it/servizi-2/"><span>Servizi</span></a><div class="sub-nav gradient-hover"><ul>
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-840 first level-arrows-on"><a href="https://www.dataiadela.it/prenotazione-2/"><span>Prenotazione Tavoli e asporto</span></a></li> </ul></div></li>
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-224 has-children"><a href="https://www.dataiadela.it/news/"><span>Ultime News</span></a><div class="sub-nav gradient-hover"><ul>
<li class=" menu-item menu-item-type-post_type menu-item-object-post menu-item-948 first level-arrows-on"><a href="https://www.dataiadela.it/chiusura-invernale-dal-311218-al-060119-compresi/"><span>CHIUSURA INVERNALE DAL 31\12\20 AL 03\01\21 COMPRESI</span></a></li>
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-929 level-arrows-on"><a href="https://www.dataiadela.it/apertura-come-da-decreto/"><span>APERTURA COME DA DECRETO</span></a></li>
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-874 level-arrows-on"><a href="https://www.dataiadela.it/tartufo/"><span>Tartufo</span></a></li>
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-222 level-arrows-on"><a href="https://www.dataiadela.it/prima-comunione-e-santa-cresima/"><span>Eventi</span></a></li>
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-221 level-arrows-on"><a href="https://www.dataiadela.it/documenti/"><span>Documenti</span></a></li> </ul></div></li>
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-223"><a href="https://www.dataiadela.it/galleria/"><span>Galleria</span></a></li>
<li class=" menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-577 has-children"><a href="#"><span>Privacy</span></a><div class="sub-nav gradient-hover"><ul>
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-578 first level-arrows-on"><a href="https://www.dataiadela.it/gdpr-richiedi-dati-personali/"><span>GDPR -Richiedi dati personali</span></a></li> </ul></div></li>
<li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-220"><a href="https://www.dataiadela.it/contatti/"><span>Contatti</span></a></li>
</ul>
<a class="accent-bg" href="#show-menu" id="mobile-menu" rel="nofollow">
<span class="menu-open">Menù</span>
<span class="menu-back">Indietro</span>
<span class="wf-phone-visible"> </span>
</a>
<div class="right-block text-near-menu wf-td"><div class="mini-search">
<form action="https://www.dataiadela.it/" class="searchform" method="get" role="search">
<input class="field searchform-s" name="s" placeholder="Digita e premi invio …" type="text" value=""/>
<input class="assistive-text searchsubmit" type="submit" value="Vai!"/>
<a class="submit text-disable" href="#go" id="trigger-overlay"> </a>
</form>
</div></div>
</nav>
</div><!-- .wf-wrap -->
</div><!-- .navigation-holder -->
</header><!-- #masthead -->
<div class="page-title title-left transparent-bg" style="min-height: 106px;">
<div class="wf-wrap">
<div class="wf-container-title">
<div class="wf-table" style="height: 106px;">
<div class="wf-td hgroup"><h1 class="h3-size">Pagina non trovata</h1></div><div class="wf-td"><div class="assistive-text">Sei qui:</div><ol class="breadcrumbs text-normal" xmlns:v="http://rdf.data-vocabulary.org/#"><li typeof="v:Breadcrumb"><a href="https://www.dataiadela.it/" property="v:title" rel="v:url" title="">Home</a></li><li class="current">Errore 404</li></ol></div>
</div>
</div>
</div>
</div>
<div class="sidebar-none" id="main"><!-- class="sidebar-none", class="sidebar-left", class="sidebar-right" -->
<div class="main-gradient"></div>
<div class="wf-wrap">
<div class="wf-container-main">
<!-- Content -->
<div class="content" id="content" role="main" style="min-height: 500px; text-align:center">
<article class="post error404 not-found" id="post-0">
<h1 class="entry-title">Oops! La pagina non è stata trovata.</h1>
<p>Sembra che non sia stato trovato nulla in questa posizione. Provi ad utilizzare la ricerca?</p>
<form action="https://www.dataiadela.it/" class="searchform" method="get" role="search">
<input class="field searchform-s" name="s" placeholder="Digita e premi invio …" type="text" value=""/>
<input class="assistive-text searchsubmit" type="submit" value="Vai!"/>
<a class="submit" href="#go"></a>
</form>
</article><!-- #post-0 .post .error404 .not-found -->
</div><!-- #content .site-content -->
</div><!-- .wf-container -->
</div><!-- .wf-wrap -->
</div><!-- #main -->
<!-- !Footer -->
<footer class="footer transparent-bg" id="footer">
<!-- !Bottom-bar -->
<div class="solid-bg" id="bottom-bar" role="contentinfo">
<div class="wf-wrap">
<div class="wf-container-bottom">
<div class="wf-table wf-mobile-collapsed">
<div class="wf-td">
<div class="wf-float-left">

							Copyright 2015 - Designed by Magellano
						</div>
</div>
<div class="wf-td">
</div>
</div>
</div><!-- .wf-container-bottom -->
</div><!-- .wf-wrap -->
</div><!-- #bottom-bar -->
</footer><!-- #footer -->
<a class="scroll-top" href="#"></a>
</div><!-- #page -->
<div id="su-footer-links" style="text-align: center;"></div><script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.dataiadela.it\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://www.dataiadela.it/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.7" type="text/javascript"></script>
<script src="https://www.dataiadela.it/wp-content/plugins/go_pricing/assets/js/go_pricing_scripts.js?ver=2.4.5" type="text/javascript"></script>
<script src="https://www.dataiadela.it/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=4.9.16" type="text/javascript"></script>
<script src="https://www.dataiadela.it/wp-content/themes/dt-the7/js/main.min.js?ver=2.1.3" type="text/javascript"></script>
<script src="https://www.dataiadela.it/wp-includes/js/wp-embed.min.js?ver=4.9.16" type="text/javascript"></script>
</body>
</html>