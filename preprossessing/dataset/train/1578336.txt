<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "94043",
      cRay: "61096eebdc10361f",
      cHash: "e5a28635e1fc819",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cud2VzdGNoYW5nZS50b3Av",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "/t8Kt+zuvZHAbKgK7ZZ8mBRBJTo0t4h9zVcCL5/f2B41gcnk7WfdeOblzO7Ibf5WkHpjzuIqLxR890wfaVdZ3z+kkSVkn5+/cOWgT463KwOR1z0ZOJOWnnsEdtrHNM8BrWqjPq4kxD1zTTPpLiXs7T/vB463i/6OBP9xxVuRshkDBmh4Gck2GaA4O4f/YAe8heY/091TDOHW8zEw1CGWGJXwZhtR7PoKRGbtyi8yzX775S5kye6E50MMQiFOACT9CGKsZM6EZRYsfgYOsIQqcq3NNMtbwYlkxCk8A0uqLxIOdzBmZKcrS/S6Xz1pHnpNEW9/ve+vc6rvGUDHgpw6VDWlOXMOSZaBydYABz1HWUWHAYxkeVc+xkhiLMjN64l0krcTTsnlF/KeCN2G6CazXlikujva5TXJjcMeCLv2MXYqnLEmvO4P2lNO/D2vRUrxKehtUsT9S+2ycB8ZZ1/yFTM7KzVA9/5OzVKJrPQkJDGOYWZP/tc3VmPDRoKnbbEoZI2hmoDcxcRQAoMngYbZ5bZQpRzJOyutSxT353CsmCOJOmAWooTeKzANgzdVRbCFDWVYXjpr3nzZMDODdB+N0Y+Rfyh78pRRZy4Qk3kR7pMt3EGyMUwdMw7SmuN3q5kexGQCCobl2b2QpeCb1sGFGti1Xg8gVja0NWp1EEmi9RCVttpmVy/urTRQYuRga7ubtgLZagsy6yv17ZhGEqgkqgzooaBA115z/s0CSqGAtCjAS0HI860aIx7DG3ZED99o",
        t: "MTYxMDQ4MTMwNy41MDAwMDA=",
        m: "W8GRtIZdQY9fZEPZvMXe97ROA4LoKMOyg2ivQArX2Fc=",
        i1: "kMLHNDI4pWmiykdby980Pg==",
        i2: "NUOuTCIwmBIy3fN20FL8dw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "PhEx4FoMxGz6NMURZ+oMufFsaUhlI3Dg8a++e+yuXNg=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.westchange.top</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=ec23431da8b36c64d9367027f43920bc739315e1-1610481307-0-ATomWokVCUlfg6pLWp5W1jj5MQXGzbeBG_LBHLY-fhz8W9207H2rTC8ymvHyHfK8mio2dTPjUBybPhm7Nim5p2ZmklFwESy6cvyiWndtdJFJfAzT2pFuLAPszrF65dxjC9Bm_Q-77RHsJRb4_L0LNNzqR5dMFUa_SxvWLgq8MNYkiiXlS_bh0gs7qF3T_skcw-NYbbcfcAr53pWt_uqKwRebYcEvHPHDitQXfnIzFscqfOpTH49N_4b25QRHbLmdv_UdTa76ZlU2krbvPju0P-HBrnneHWuXYbm_jZ_bexcbS-m_9cebw3TEoFOBD5poyGHHlAq8bJhZuvIRPlx4sBIFKQzCMTmhQ2Wd-U4z7TM_G2HSC1eYaUWAKPPwEqqFSF8oS0xz4KSQQ2ZBv2A2lF0D4q-3ux44WLcZI6h5F_BDbt67afAsGDZUdvbVhGePWWxGtEra9mMBk_ASXJ4EUPoJtZ3culji9TcVyWd_ZjSAut5cpo-sd-JGB-Fqst312pwxh0tvX3Id9xdfJBHmFM0" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="e8a534386ee85942a4039ba4a95ac08f8e087c9d-1610481307-0-ARGxWpXFP+2QKAd7gJWtZx1Hn1bnOrQ5CWsp1jMbx2eQps/HZ1MK3sZhSCaJFy60rTYsrUUlZwRVDxSNIGuI+Ati76amhX01a2EVdMezZtQBr9NrudwHW9OgwFZ/3mAdu9QgvBE9NmgrboCo/GK3KYo7C1wiDI/pyuhtjjM5ER/FScSH5Qxv7tkkBQOPpXUC19wFDE7LbyThh49ywQlXCpShdjUMt85os26cDRbdO4dwzHMhnHpUIOLZ5EPHqrnsgE37KJeZ1vuFDJTMzwh5GpK+mmerDXdg0kJiitrdXS70rCpzcgXaXFAh17RE4GJ62njU25Dhdhl1N17KIYfiKw/5E745zgOI9CSuQ4I9yvWau6Q3tqiYgAiTk/sxBQSJPcFNrjCobSZo7VaU3MmqfGeq3cjAfdGrqDL1er4Uu1vYXHAlZ+vQD2wjE5KhjdLEvGg54Fp6ojS7JaWSqTCUQFY9K/O+0utkcfOOKFMrD37yJIevRsd6JGAE8e3aH+o/P4UqmwcYiIWBe1mrrQvBkdstyq8+duTN6RPHZ2lz1S/jUHRjcWHTVKqgdocODqOuHyXAPSewYTZ0qJRhoPrt5buWxTOmkGINq9XGWEMuCl1KVJMU8SH6Yra16SPmyvB1unhNhef8BuX0aF45FxRyFWl1Tb1H7WW344qsKbJhDFJpOZVL3Q5B9XANQ02R1Vjuqa22LhEScao91zJB9T93ZlENbyvBzghVVth4wPWDz2ngtMXL/NZpIThRDlEUce1DsqWGntkJsNrbOoMrLQshe1d8p88ZlqfBlWRG+1fR9e5OgAyE9gqY3zS4PvX7iZI8zjFT026S5Swo2D8KJ4E83JbCO00mcRk4AWUMnWtp+eDI3VW+oiiaBnHxpjcx07E9Ae9kQ087ZIyrojZAwd2BFeLhs3AI+E7UjxKibevwTw5J3r5BHoIwSQSzt45qFxKc377STpzSNXKybMmJPEXoNNwOrvkWoJRYy7BDsEXyz3poVGJXUMwovZ4DyThSnwPVgzGJcWd36H1B0LOIWzqaLvkX71+9rRDQSeDcQ1xjZlqLKWWmOJtin5RvY9ZJOEAaWx9ZQ9XlyzKewYh1dDNGbSnBEQ/tj0gzjUhsTL+eSbl2qU/ZqqpLC2ygUabr+Qq1B5P/awMFMvQon5q2EfAiCGZi5Iuq3syk06H+NNEiWcmU16RJNOkevMTkLaMv1BkWsd1gC4xNRSucCqDyqVcBPgLtwNNUDtqxYgZ3ba+rOgIJBmB76SC12YtsDGHY9/rLZdvwzogcELoIUf2RHpRIx3M="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="9d26e91418749d436ea8e50eb69a6828"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=61096eebdc10361f')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">61096eebdc10361f</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 210.75.253.169</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
