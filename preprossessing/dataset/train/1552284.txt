<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="it"> <![endif]--><!--[if IE 7]>    <html class="ie7" lang="it"> <![endif]--><!--[if IE 8]>    <html class="ie8" lang="it"> <![endif]--><!--[if IE 9]>    <html class="ie9" lang="it"> <![endif]--><!--[if !IE]> <html lang="it"><![endif]--><html lang="it" xml:lang="it" xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- Palm -->
<meta content="True" name="HandheldFriendly"/>
<!-- Windows -->
<meta content="320" name="MobileOptimized"/>
<!-- Safari, Android, BB, Opera -->
<meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" name="viewport"/>
<meta content="black-translucent" name="apple-mobile-web-app-status-bar-style"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Volersi Bio -  Home page</title>
<meta content="Volersi Bio" name="description"/>
<meta content="Volersi Bio" name="keywords"/>
<meta content="INDEX,FOLLOW" name="robots"/>
<link href="https://arteracdn.net/volersibio.com/skin/frontend/default/MAG080123/favicon.1433541600.ico" rel="icon" type="image/x-icon"/>
<link href="https://arteracdn.net/volersibio.com/skin/frontend/default/MAG080123/favicon.1433541600.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:400" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Arimo" rel="stylesheet" type="text/css"/>
<!--[if lt IE 7]>
<script type="text/javascript">
//<![CDATA[
    var BLANK_URL = 'https://arteracdn.net/volersibio.com/js/blank.html';
    var BLANK_IMG = 'https://arteracdn.net/volersibio.com/js/spacer.1415142000.gif';
//]]>
</script>
<![endif]-->
<link href="https://arteracdn.net/volersibio.com/media/css_secure/0b35663a5a4abfcc43cf095e9fbfa034.1590314395.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://arteracdn.net/volersibio.com/media/css_secure/95e8a695e0b73890fdb4b8659f1e5e03.1590314395.css" media="print" rel="stylesheet" type="text/css"/>
<script src="https://arteracdn.net/volersibio.com/js/prototype/prototype.1415142000.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/lib/ccard.1415142000.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/prototype/validation.1415142000.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/scriptaculous/builder.1415142000.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/scriptaculous/effects.1415142000.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/scriptaculous/dragdrop.1415142000.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/scriptaculous/controls.1415142000.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/scriptaculous/slider.1415142000.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/varien/js.1415142000.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/varien/form.1415142000.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/varien/menu.1415142000.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/mage/translate.1415142000.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/mage/cookies.1415142000.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/FMEinstantsearch/search.1489100400.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/FMEinstantsearch/libs/modernizr.2.6.2.1489100400.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/megnor/html5.1433541600.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/megnor/jquery-1.7.1.min.1433541600.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/megnor/tm_jquery.flexslider.min.1433541600.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/megnor/megnor.min.1433541600.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/megnor/jquery.selectbox-0.2.min.1433541600.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/megnor/carousel.min.1433541600.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/megnor/jstree.min.1433541600.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/megnor/scrolltop.min.1433541600.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/js/megnor/advancedmenu.1433541600.js" type="text/javascript"></script>
<script src="https://arteracdn.net/volersibio.com/skin/frontend/default/MAG080123/js/custom.1433541600.js" type="text/javascript"></script>
<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="https://arteracdn.net/volersibio.com/media/css_secure/b1cdbcc5401179d1fd52e067320bfe63.1590314395.css" media="all" />
<![endif]-->
<!--[if lt IE 7]>
<script type="text/javascript" src="https://arteracdn.net/volersibio.com/js/lib/ds-sleight.1415142000.js"></script>
<script type="text/javascript" src="https://arteracdn.net/volersibio.com/skin/frontend/base/default/js/ie6.1415142000.js"></script>
<![endif]-->
<script type="text/javascript">
//<![CDATA[
Mage.Cookies.path     = '/';
Mage.Cookies.domain   = '.volersibio.com';
//]]>
</script>
<script type="text/javascript">
//<![CDATA[
optionalZipCountries = ["IE","PA","HK","MO"];
//]]>
</script>
<script type="text/javascript">//<![CDATA[
        var Translator = new Translate({"Please select an option.":"Selezionare una opzione.","This is a required field.":"Questo \u00e8 un campo obbligatorio.","Please enter a valid number in this field.":"Inserire un numero valido in questo campo.","Please use letters only (a-z or A-Z) in this field.":"Utilizzare solo lettere in questo campo (a-z o A-Z).","Please use only letters (a-z), numbers (0-9) or underscore(_) in this field, first character should be a letter.":"Utilizzare solo lettere (a-z), numeri (0-9) o underscore(_) in questo campo, la prima lettera deve essere una lettera.","Please enter a valid phone number. For example (123) 456-7890 or 123-456-7890.":"Inserisci un numero di telefono valido. Per esempio (123) 456-7890 o 123-456-7890.","Please enter a valid date.":"Inserire una data valida.","Please enter a valid email address. For example johndoe@domain.com.":"Inserire un indirizzo email valido. Per esempio johndoe@domain.com.","Please enter 6 or more characters. Leading or trailing spaces will be ignored.":"Inserire 6 o pi\u00f9 caratteri. Gli spazi iniziali o finali saranno ignorati.","Please make sure your passwords match.":"Assicurati che le password corrispondano.","Please enter a valid URL. For example http:\/\/www.example.com or www.example.com":"Inserire un URL valido. Per esempio http:\/\/www.example.com o www.example.com","Please enter a valid social security number. For example 123-45-6789.":"Inserire un numero valido di previdenza sociale. Per esempio 123-45-6789.","Please enter a valid zip code. For example 90602 or 90602-1234.":"Inserire un codice zip valdio. Per esempio 90602 o 90602-1234.","Please enter a valid zip code.":"Inserire codice zip valido.","Please use this date format: dd\/mm\/yyyy. For example 17\/03\/2006 for the 17th of March, 2006.":"Utilizzare questo formato della data: dd\/mm\/yyyy. Per esempio 17\/03\/2006 per il 17 di Marzo, 2006.","Please enter a valid $ amount. For example $100.00.":"Inserire un importo valido di $. Per esempio $100.00.","Please select one of the above options.":"Selezionare una delle opzioni soprastanti.","Please select one of the options.":"Selezionare una delle opzioni.","Please select State\/Province.":"Selezionare Stato\/Provincia.","Please enter a number greater than 0 in this field.":"Insereire un numero maggiore di 0 in questo campo.","Please enter a valid credit card number.":"Inserire un numero di carta di credito valido.","Please wait, loading...":"Attendere prego, caricamento...","Complete":"Completo","Add Products":"Aggiungi prodotti","Please choose to register or to checkout as a guest":"Scegli di registrarti o di fare il checkout come ospite","Please specify payment method.":"Specificare il metodo di pagamento.","Add to Cart":"Aggiungi al carrello","In Stock":"Disponibile","Out of Stock":"Non disponibile"});
        //]]></script>
<!-- Facebook Pixel Code -->
<script src="//www.facelook.no/en_US/pixel.js">
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '462468421206579');
fbq('track', 'PageView');
</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=462468421206579&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<!-- End Facebook Pixel Code --></head>
<body class=" cms-index-index cms-home">
<div class="wrapper">
<noscript>
<div class="global-site-notice noscript">
<div class="notice-inner">
<p>
<strong>JavaScript seems to be disabled in your browser.</strong><br/>
                    You must have JavaScript enabled in your browser to utilize the functionality of this website.                </p>
</div>
</div>
</noscript>
<div class="page">
<header class="header-container">
<div class="header">
<div class="header-bottom">
<h1 class="logo"><strong>Volersi Bio</strong><a class="logo" href="https://volersibio.com/" title="Volersi Bio"><img alt="Volersi Bio" src="https://arteracdn.net/volersibio.com/skin/frontend/default/MAG080123/images/logo.1464991200.png"/></a></h1>
<div class="quick-access">
<div class="header_top">
<div class="welcome-msg"></div>
</div>
<div class="header-cart">
<div class="block-cart btn-slide">
<div class="cart-label">
<div class="cart_mini_right">
                            (0 prodotti) -             			<span class="price">0,00 €</span> <div class="right_arrow"></div>
</div>
</div>
<div class="block-content" id="panel">
<div class="top_arrow"></div>
<div class="cart_topbg">
<div class="main-cart">
<p class="empty">Carrello vuoto</p>
</div>
</div>
</div>
</div>
</div>
<form action="https://volersibio.com/catalogsearch/result/" id="search_mini_form" method="get"> <div class="input-box">
<label for="search">Search:</label> <input autocomplete="off" class="input-text required-entry" id="search" maxlength="128" name="q" placeholder="Cerca intero negozio qui..." type="search" value=""/>
<button class="button search-button" title="Ricerca" type="submit"><span><span>Ricerca</span></span></button> </div>
</form>
<div class="clearfix"></div>
<div id="searchcontent"></div>
<script type="text/javascript">
    //<![CDATA[
        var instantsearch = new Instantsearch(
        'https://volersibio.com/instantsearch/index/',
        
        'search'
    );
    
    Event.observe('search', 'keyup', function(event){           
        instantsearch.search();     
    });

    //]]>
    </script>
<div class="tm_headerlinkmenu">
<div class="tm_headerlinks_inner"><div class="headertoggle_img"> </div></div>
<ul class="links">
<li class="first"><a href="https://volersibio.com/customer/account/" title="Il Mio Account">Il Mio Account</a></li>
<li><a href="https://volersibio.com/wishlist/" title="La lista dei desideri">La lista dei desideri</a></li>
<li><a class="top-link-cart" href="https://volersibio.com/checkout/cart/" title="Carrello">Carrello</a></li>
<li><a class="top-link-checkout" href="https://volersibio.com/checkout/" title="Vai alla cassa">Vai alla cassa</a></li>
<li class=" last"><a href="https://volersibio.com/customer/account/login/" title="Entra">Entra</a></li>
</ul>
</div>
</div>
</div>
</div>
<nav class="nav-container">
<div class="nav-inner">
<div id="advancedmenu">
<div class="menu">
<div class="parentMenu menu0 home_link">
<a href="https://volersibio.com/">
<span>Home</span>
</a>
</div>
</div>
<!-- roberto inizio menu -->
<!-- roberto inizio menu -->
<div class="menu" id="menu71" onmouseout="megnorHideMenuPopup(this, event, 'popup71', 'menu71')" onmouseover="megnorShowMenuPopup(this, 'popup71');">
<div class="parentMenu">
<a href="http://volersibio.com/index.php/pranzo_e_cena.html">
<span>Pranzo e Cena</span>
</a>
</div>
</div>
<div class="megnor-advanced-menu-popup" id="popup71" onmouseout="megnorHideMenuPopup(this, event, 'popup71', 'menu71')" onmouseover="megnorPopupOver(this, event, 'popup71', 'menu71')">
<div class="megnor-advanced-menu-popup_inner">
<div class="block1">
<div class="column first odd"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/aiuti-in-cucina.html"><span>Aiuti in cucina</span></a></div></div><div class="column even"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/cereali.html"><span>Cereali</span></a></div></div><div class="column odd"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/pasta-e-riso.html"><span>Pasta e riso</span></a></div></div><div class="clearBoth"></div><div class="column first odd"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/semi-e-legumi.html"><span>Semi e legumi</span></a></div></div><div class="column even"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/spezie-e-condimenti.html"><span>Spezie e condimenti</span></a></div></div><div class="column odd"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/farine-e-lieviti.html"><span>Farine e lieviti</span></a></div></div><div class="clearBoth"></div>
</div>
<div class="clearBoth"></div>
</div>
</div> <div class="menu" id="menu72" onmouseout="megnorHideMenuPopup(this, event, 'popup72', 'menu72')" onmouseover="megnorShowMenuPopup(this, 'popup72');">
<div class="parentMenu">
<a href="http://volersibio.com/index.php/colazione_e_merenda.html">
<span>Colazione e Merenda</span>
</a>
</div>
</div>
<div class="megnor-advanced-menu-popup" id="popup72" onmouseout="megnorHideMenuPopup(this, event, 'popup72', 'menu72')" onmouseover="megnorPopupOver(this, event, 'popup72', 'menu72')">
<div class="megnor-advanced-menu-popup_inner">
<div class="block1">
<div class="column first odd"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/colazione-e-merenda/bevande-e-dessert.html"><span>Bevande e dessert</span></a></div></div><div class="column even"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/colazione-e-merenda/composte-e-polpe-di-frutta.html"><span>composte e polpe di frutta</span></a></div></div><div class="column odd"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/colazione-e-merenda/prodotti-da-forno.html"><span>Prodotti da forno</span></a></div></div><div class="clearBoth"></div><div class="column first odd"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/colazione-e-merenda/creme-spalmabili-e-dolcificanti.html"><span>Creme spalmabili e dolcificanti</span></a></div></div><div class="column even"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/colazione-e-merenda/snack.html"><span>Snack</span></a></div></div>
</div>
<div class="clearBoth"></div>
</div>
</div> <div class="menu" id="menu74" onmouseout="megnorHideMenuPopup(this, event, 'popup74', 'menu74')" onmouseover="megnorShowMenuPopup(this, 'popup74');">
<div class="parentMenu">
<a href="http://volersibio.com/index.php/benessere.html">
<span>Benessere</span>
</a>
</div>
</div>
<div class="megnor-advanced-menu-popup" id="popup74" onmouseout="megnorHideMenuPopup(this, event, 'popup74', 'menu74')" onmouseover="megnorPopupOver(this, event, 'popup74', 'menu74')">
<div class="megnor-advanced-menu-popup_inner">
<div class="block1">
<div class="column first odd"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/benessere/cura-della-persona.html"><span>Cura della persona</span></a></div></div><div class="column even"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/benessere/integratori.html"><span>Integratori</span></a></div></div><div class="column odd"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/benessere/macrobiotica.html"><span>Macrobiotica</span></a></div></div><div class="clearBoth"></div><div class="column first odd"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/benessere/infanzia.html"><span>Infanzia</span></a></div></div>
</div>
<div class="clearBoth"></div>
</div>
</div> <div class="menu" id="menu73" onmouseout="megnorHideMenuPopup(this, event, 'popup73', 'menu73')" onmouseover="megnorShowMenuPopup(this, 'popup73');">
<div class="parentMenu">
<a href="http://volersibio.com/index.php/bio_casa.html">
<span>Bio Casa</span>
</a>
</div>
</div>
<div class="megnor-advanced-menu-popup" id="popup73" onmouseout="megnorHideMenuPopup(this, event, 'popup73', 'menu73')" onmouseover="megnorPopupOver(this, event, 'popup73', 'menu73')">
<div class="megnor-advanced-menu-popup_inner">
<div class="block1">
<div class="column first odd"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/bio-casa/linea-casa.html"><span>Linea casa</span></a></div></div><div class="column last even"><div class="itemMenu level1"><a class="itemMenuName level1" href="http://volersibio.com/index.php/bio-casa/oggettistica-e-pubblicazioni.html"><span>Oggettistica e pubblicazioni</span></a></div></div>
</div>
<div class="clearBoth"></div>
</div>
</div>
<!-- roberto fine menu -->
<!-- roberto fine menu -->
<div class="clearBoth"></div>
</div>
<script type="text/javascript">
//<![CDATA[
var CUSTOMMENU_POPUP_WIDTH = 0;
var CUSTOMMENU_POPUP_TOP_OFFSET = 0;
var CUSTOMMENU_POPUP_RIGHT_OFFSET_MIN = 0;
var CUSTOMMENU_POPUP_DELAY_BEFORE_DISPLAYING = 0;
var CUSTOMMENU_POPUP_DELAY_BEFORE_HIDING = 0;
var megnorCustommenuTimerShow = {};
var megnorCustommenuTimerHide = {};
//]]>
</script>
<div class="header_cms_block">
<div class="header_cms_outer">
<div class="header_inner header">
<ul class="links">
<li class="first"><a href="https://volersibio.com/customer/account/" title="Il Mio Account">Il Mio Account</a></li>
<li><a href="https://volersibio.com/wishlist/" title="La lista dei desideri">La lista dei desideri</a></li>
<li><a class="top-link-cart" href="https://volersibio.com/checkout/cart/" title="Carrello">Carrello</a></li>
<li><a class="top-link-checkout" href="https://volersibio.com/checkout/" title="Vai alla cassa">Vai alla cassa</a></li>
<li class=" last"><a href="https://volersibio.com/customer/account/login/" title="Entra">Entra</a></li>
</ul>
</div>
</div>
</div>
<section class="main-container col3-layout">
<h2 style="display:none;"> </h2>
<div class="main">
<div class="col-wrapper">
<article class="col-main">
<p><img alt="" src="https://arteracdn.net/volersibio.com/media/wysiwyg/presto_on_line.1490137200.jpg"/></p>
<p><img alt="" height="500" src="https://arteracdn.net/volersibio.com/media/wysiwyg/100495110_2568967613417397_7105022118391185408_n.1592651663.png" style="vertical-align: baseline;" width="500"/></p>
<p> </p>
<p> </p>
<div class="flexslider">
<p style="text-align: center;"> </p>
</div>
<p> </p><div class="sub_banner"><a href="#"><img alt="" src="https://arteracdn.net/volersibio.com/skin/frontend/default/MAG080123/images/banners/sub_banner.1433541600.jpg"/></a></div>
<div class="sub_banner"> </div>
<div class="sub_banner"> </div>
<p> </p> <div class="std"><p> </p><div class="featured-products">
<div class="category-title"><h2><a href="https://volersibio.com//featured-products">Prodotti in Offerta</a></h2></div>
<p class="note-msg"></p>
<p></p><div class="new-products">
<div class="category-title"><h2><a href="https://volersibio.com/newproducts">Latest Products</a></h2></div>
<p class="note-msg"></p>
</div>
<p> </p>
<h1 class="article-title">Seguici su Facebook</h1>
<div class="article-body">
<div id="fb-root"> </div>
<script type="text/javascript">// <![CDATA[
(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
// ]]></script>
<div class="fb-like-box" data-header="false" data-href="https://www.facebook.com/volersibio" data-show-faces="true" data-stream="true" data-width="580"> </div>
</div></div>
<aside class="col-left sidebar"><script type="text/javascript">
//<![CDATA[
function toggleMenu(el, over)
{
	if (over) {
		Element.addClassName(el, 'over');
	}
	else {
		Element.removeClassName(el, 'over');
	}
}
//]]>
</script>
<!-- ================== Tree View ================ -->
<div class="block block-side-nav-container">
<div class="block-title">
<strong><span>Categorie Prodotti</span></strong>
</div>
<div class="block-content">
<div class="side-nav">
<ul class="treeview-side treeview" id="category-treeview">
<li class="level0 nav-1">
<a href="https://volersibio.com/aiuti-in-cucina.html">
<span>Aiuti in cucina</span>
</a>
</li> <li class="level0 nav-2">
<a href="https://volersibio.com/ricorrenze.html">
<span>Ricorrenze</span>
</a>
</li> <li class="level0 nav-3">
<a href="https://volersibio.com/bevande-e-dessert.html">
<span>Bevande e dessert</span>
</a>
</li> <li class="level0 nav-4">
<a href="https://volersibio.com/cereali.html">
<span>Cereali</span>
</a>
</li> <li class="level0 nav-5">
<a href="https://volersibio.com/composte-e-polpe-di-frutta.html">
<span>composte e polpe di frutta</span>
</a>
</li> <li class="level0 nav-6">
<a href="https://volersibio.com/creme-spalmabili-e-dolcificanti.html">
<span>Creme spalmabili e dolcificanti</span>
</a>
</li> <li class="level0 nav-7">
<a href="https://volersibio.com/cura-della-persona.html">
<span>Cura della persona</span>
</a>
</li> <li class="level0 nav-8">
<a href="https://volersibio.com/farine-e-lieviti.html">
<span>Farine e lieviti</span>
</a>
</li> <li class="level0 nav-9">
<a href="https://volersibio.com/infanzia.html">
<span>Infanzia</span>
</a>
</li> <li class="level0 nav-10">
<a href="https://volersibio.com/integratori.html">
<span>Integratori</span>
</a>
</li> <li class="level0 nav-11">
<a href="https://volersibio.com/macrobiotica.html">
<span>Macrobiotica</span>
</a>
</li> <li class="level0 nav-12">
<a href="https://volersibio.com/linea-casa.html">
<span>Linea casa</span>
</a>
</li> <li class="level0 nav-13">
<a href="https://volersibio.com/oggettistica-e-pubblicazioni.html">
<span>Oggettistica e pubblicazioni</span>
</a>
</li> <li class="level0 nav-14">
<a href="https://volersibio.com/pasta-e-riso.html">
<span>Pasta e riso</span>
</a>
</li> <li class="level0 nav-15">
<a href="https://volersibio.com/preparati.html">
<span>Preparati</span>
</a>
</li> <li class="level0 nav-16">
<a href="https://volersibio.com/prodotti-da-forno.html">
<span>Prodotti da forno</span>
</a>
</li> <li class="level0 nav-17">
<a href="https://volersibio.com/semi-e-legumi.html">
<span>Semi e legumi</span>
</a>
</li> <li class="level0 nav-18">
<a href="https://volersibio.com/snack.html">
<span>Snack</span>
</a>
</li> <li class="level0 nav-19">
<a href="https://volersibio.com/spezie-e-condimenti.html">
<span>Spezie e condimenti</span>
</a>
</li> <li class="level0 nav-20">
<a href="https://volersibio.com/0-sfuso.html">
<span>0 Sfuso</span>
</a>
</li> <li class="level0 nav-21">
<a href="https://volersibio.com/alimentari-secchi.html">
<span>alimentari secchi</span>
</a>
</li> <li class="level0 nav-22">
<a href="https://volersibio.com/frigo.html">
<span>Frigo</span>
</a>
</li> </ul>
</div>
</div>
</div>
<script type="text/javascript">
//<![CDATA[
    function validatePollAnswerIsSelected()
    {
        var options = $$('input.poll_vote');
        for( i in options ) {
            if( options[i].checked == true ) {
                return true;
            }
        }
        return false;
    }
//]]>
</script>
<div class="block block-poll">
<div class="block-title">
<strong><span>Sondaggio pubblico</span></strong>
</div>
<form action="https://volersibio.com/poll/vote/add/poll_id/1/" id="pollForm" method="post" onsubmit="return validatePollAnswerIsSelected();">
<div class="block-content">
<p class="block-subtitle">Cosa preferisci mangiare</p>
<ul id="poll-answers">
<li>
<input class="radio poll_vote" id="vote_5" name="vote" type="radio" value="5"/>
<span class="label"><label for="vote_5">Carne</label></span>
</li>
<li>
<input class="radio poll_vote" id="vote_6" name="vote" type="radio" value="6"/>
<span class="label"><label for="vote_6">Pesce</label></span>
</li>
<li>
<input class="radio poll_vote" id="vote_7" name="vote" type="radio" value="7"/>
<span class="label"><label for="vote_7">Verdura</label></span>
</li>
<li>
<input class="radio poll_vote" id="vote_8" name="vote" type="radio" value="8"/>
<span class="label"><label for="vote_8">Legumi</label></span>
</li>
</ul>
<script type="text/javascript">decorateList('poll-answers');</script>
<div class="actions">
<button class="button" title="Vota" type="submit"><span><span>Vota</span></span></button>
</div>
</div>
</form>
</div>
<div class="block block-banner">
<div class="banner1"><a href="#"><img alt="Prodotti senza Glutine" src="https://arteracdn.net/volersibio.com/media/wysiwyg/test/senzaglutine_200.1433628000.jpg" title="Prodotti senza Glutine" width="220"/></a></div>
<div class="banner2"><img alt="Prodotti per Vegani" src="https://arteracdn.net/volersibio.com/media/wysiwyg/test/logo-vegan_200.1433628000.jpg" title="Prodotti per Vegani" width="220"/></div>
</div>
<div class="block block-list block-compare">
<div class="block-title">
<strong><span>Confronta Prodotti                    </span></strong>
</div>
<div class="block-content">
<p class="empty">Nessun prodotto selezionato</p>
</div>
</div>
<div class="block block-subscribe">
<div class="block-title">
<strong><span>Newsletter</span></strong>
</div>
<div>
<form action="https://volersibio.com/newsletter/subscriber/new/" id="newsletter-validate-detail" method="post">
<div class="block-content">
<div class="form-subscribe-header">
<label for="newsletter">Registrati per ricevere le nostre offerte</label>
</div>
<div class="input-box">
<input class="input-text required-entry validate-email" id="newsletter" name="email" title="Iscriviti alla nostra newsletter" type="text"/>
</div>
<div class="actions">
<button class="button" title="Iscriviti" type="submit"><span><span>Iscriviti</span></span></button>
</div>
</div>
</form>
<script type="text/javascript">
    //<![CDATA[
        var newsletterSubscriberFormDetail = new VarienForm('newsletter-validate-detail');
    //]]>
    </script>
</div>
</div>
</aside>
</div>
<aside class="col-right sidebar"><div class="block block-cart">
<div class="block-title">
<strong><span>Carrello</span></strong>
</div>
<div class="block-content">
<p class="empty">Carrello vuoto</p>
</div>
</div>
<div class="block block-banner">
<div class="banner1"> </div>
<div class="banner1"><a href="#"><img alt="Agricoltura Biologica" src="https://arteracdn.net/volersibio.com/media/wysiwyg/test/bio_logo_220.1433628000.jpg" title="Agricoltura Biologica"/></a></div>
<div class="banner1"><img alt="" height="220" src="https://arteracdn.net/volersibio.com/media/wysiwyg/PALM_OIL_FREE.1463868000.jpg" width="220"/></div>
<div class="banner1"> </div>
</div>
</aside>
</article></div>
<footer class="footer-container">
<div class="footer_inner">
<div class="footer">
<div class="footer_top" id="footer">
<div class="footer-area" id="block_1">
<p><span>alimenti per allergie, intolleranze, alimenti biologici, prodotti vegani, macrobiotica, integratori alimentari, bio cosmesi, cura del corpo, bio detergenza</span></p>
</div>
<div class="footer-area" id="block_2">
<p> </p>
</div>
<div class="footer-area" id="block_3">
<h6>Contattaci</h6>
<ul>
<li>Volersi Bio srls</li>
<li>Via G. Boccaccio,94</li>
<li>Altamura BA </li>
<li>Tel 080 92 44 360</li>
<li><a href="mailto:info@volersibio.com">info@volersibio.com</a></li>
<li>volersibio@gmail.com</li>
</ul>
</div>
<div class="footer-area" id="block_4">
<div class="content">
</div>
<div class="social_block">
<h6>Seguici su</h6>
<ul>
<li class="facebook"><a href="https://www.facebook.com/volersibio" target="_blank">Facebook</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="footer_bottom">
<div class="inner_footer_bottom">
<ul class="links">
<li class="first"><a href="https://volersibio.com/catalog/seo_sitemap/category/" title="Mappa del Sito">Mappa del Sito</a></li>
<li><a href="https://volersibio.com/catalogsearch/term/popular/" title="Condizioni Ricerca">Condizioni Ricerca</a></li>
<li><a href="https://volersibio.com/catalogsearch/advanced/" title="Ricerca Avanzata">Ricerca Avanzata</a></li>
<li><a href="https://volersibio.com/sales/guest/form/" title="Ordini">Ordini</a></li>
<li class=" last"><a href="https://volersibio.com/contacts/" title="Contattaci">Contattaci</a></li>
</ul>
<address></address>
</div>
</div>
</footer>
</div>
</section></div>
</nav></header></div></div></body>
</html>
