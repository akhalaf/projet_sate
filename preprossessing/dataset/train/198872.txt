<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" id="min-width" lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml"><head><title>Connexion</title><meta content="text/html; charset=utf-8" http-equiv="content-type"/><meta content="text/javascript" http-equiv="content-script-type"/><meta content="text/css" http-equiv="content-style-type"/><link href="https://illiweb.com/fa/favicon/discussion.ico" rel="shortcut icon" type="image/x-icon"/><meta content="noindex, follow" name="robots"/><link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet" type="text/css"/> <link href="https://fonts.googleapis.com/css?family=Lora|Inconsolata|Roboto+Condensed|Poppins|Old+Standard+TT" rel="stylesheet"/><link href="https://fonts.googleapis.com/css?family=PT+Serif" rel="stylesheet"/><link href="https://fonts.googleapis.com/css?family=Berkshire+Swash|Roboto|Mrs+Sheppards|Playfair+Display|Lobster|Lobster+Two|Abril+Fatface" rel="stylesheet"/><meta content="Connexion" name="title"/><link href="/0-ltr.css" rel="stylesheet" type="text/css"/><link href="/improvedsearch.xml" rel="search" title="Bazzart" type="application/opensearchdescription+xml"/><link href="https://www.annuairedeforums.com/search/improvedsearch.xml" rel="search" title="Rechercher des forums" type="application/opensearchdescription+xml"/><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script><script src="https://illiweb.com/rs3/89/frm/lang/fr.js" type="text/javascript"></script><script type="text/javascript">//<![CDATA[
$(document).ready(function(){});//]]></script><script src="/99059.js" type="text/javascript"></script>
<script src="https://www.googletagmanager.com/gtag/js?id=UA-144388882-1" type="text/javascript"></script>
<script src="https://illiweb.com/rs3/89/frm/jquery/cookie/jquery.cookie.js" type="text/javascript"></script>
<script src="https://illiweb.com/rs3/89/frm/mentions/tooltipster.js" type="text/javascript"></script>
<script src="https://illiweb.com/rs3/89/frm/mentions/init.js" type="text/javascript"></script>
<script src="https://illiweb.com/rs3/89/frm/jquery/toolbar/FAToolbar.js" type="text/javascript"></script><script type="text/javascript">
//<![CDATA[

  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144388882-1');

    var setScreen = function() {
        var w = document.documentElement.clientWidth || document.body.clientWidth;
        var h = document.documentElement.clientHeight || document.body.clientHeight;
        $.cookie("_fa-screen", '{"w":' + w + ',"h":' + h + '}', { expires: 15, domain: 'www.bazzart.org' });
        console.log( '{"w":' + w + ',"h":' + h + '}');
    }
    setScreen();
            window.Criteo = window.Criteo || {};
            window.Criteo.events = window.Criteo.events || [];

            // Declare this above the adunits
            var width = window.screen.width||window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth;
            var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || width <= 768 );
            var CriteoAdUnits = "" 

            Criteo.events.push(function() {
                
                Criteo.Passback.RequestBids(CriteoAdUnits, 2000);
            });   
        function CriteoAdblock(containerid, zoneid, pbcontainerid) { 
            Criteo.events.push(function() {
                Criteo.DisplayAcceptableAdIfAdblocked({
                    "zoneid": zoneid,
                    "containerid": containerid,
                    "overrideZoneFloor": false,
                    "callbacksuccess": function() {
                                        document.getElementById(pbcontainerid).style.display = "none"; //if adblock, collapse passback container id
                                      }
                });
                window.addEventListener("message", function (e) {
                    if (e.data && e.data == 'criteo-adblock-passback-'+zoneid)
                        document.getElementById(containerid).style.display = "none";                    
                    else return;
                }, false);
            });
        }

                if(typeof(_userdata) == "undefined")
					var _userdata = new Object();
                _userdata["session_logged_in"] = 0;
                _userdata["username"] = "Anonymous";
                _userdata["user_id"] = -1;
                _userdata["user_level"] = 0;
                _userdata["user_lang"] = "fr";
                _userdata["activate_toolbar"] = 1;
                _userdata["fix_toolbar"] = 1;
                _userdata["notifications"] = 1;
                _userdata["avatar"] = "<img loading=\"lazy\" src=\"https://2img.net/u/2914/86/77/05/avatars/gallery/20060310.png\"   alt=\"Anonymous\" />";
                _userdata["user_posts"] = 0;
                _userdata["user_nb_privmsg"] = 0;
                _userdata["point_reputation"] = 3;
                

				if(typeof(_lang) == "undefined")
					var _lang = new Object();
                _lang["Share"] = "Partagez";
                _lang["Login"] = "Connexion";
                _lang["Register"] = "S'enregistrer";
                _lang["Welcome"] = "Bienvenue";
                _lang["Notifications"] = "Notifications";
                _lang["See_my_profile"] = "Voir mon profil";
                _lang["Edit_profile"] = "Editer mon profil";
                _lang["All_Topics"] = "Mes sujets";
                _lang["All_Messages"] = "Mes messages";
                _lang["js_topics_followed"] = "Mes sujets suivis";
                _lang["Admin_panel"] = "Panneau d'administration";
                _lang["Logout"] = "Déconnexion";

                _lang["Notif_see_all"] = "Voir toutes les notifications";
                _lang["Notif_priv_msg"] = "Vous avez reçu un <a href=\"/privmsg?folder=inbox&amp;nid=%(nid)s\">message privé</a> de la part de <a href=\"/u%(id)d\">%(name)s</a>";
                _lang["Notif_report"] = "<a href=\"/u%(id)d\">%(name)s</a> a créé un <a href=\"/report?nid=%(nid)s\">rapport de message</a>";
                _lang["Notif_friend_req"] = "Vous avez reçu une <a href=\"/profile?mode=editprofile&amp;nid=%(nid)s&amp;page_profil=friendsfoes\">demande d'ami</a> de la part de <a href=\"/u%(id)d\">%(name)s</a>";
                _lang["Notif_group_req"] = "<a href=\"/u%(id)d\">%(name)s</a> a effectué une demande d'adhésion au groupe <a href=\"/g%(group_id)d-%(group_url_name)s?nid=%(nid)s\">%(group_name)s</a>";
                _lang["Notif_friend_con"] = "<a href=\"/u%(id)d\">%(name)s</a> vient de se connecter au forum";
                _lang["Notif_wall_msg"] = "<a href=\"/u%(id)d\">%(name)s</a> a écrit un message sur <a href=\"/u%(self)dwall?nid=%(nid)s\">votre mur</a>";
                _lang["Notif_abuse"] = "<a href=\"/admin/?mode=active&amp;nid=%(nid)s&amp;part=misc&amp;sub=support\">Un abus</a> a été signalé";
                _lang["Notif_topic_watch2"] = "<a href=\"/u%(id)d\">%(name)s</a> a écrit un message dans <a href=\"/t%(topic_id)d-%(topic_name)s?nid=%(nid)s#%(post_id)d\">%(topic_title)s</a>";
                _lang["Notif_topic_watch_p2"] = "<a href=\"/u%(id)d\">%(name)s</a> a écrit un message dans <a href=\"/t%(topic_id)dp%(start)d-%(topic_name)s?nid=%(nid)s#%(post_id)d\">%(topic_title)s</a>";
                _lang["Notif_topic_watch_guest2"] = "Un invité a écrit un message dans <a href=\"/t%(topic_id)d-%(topic_name)s?nid=%(nid)s#%(post_id)d\">%(topic_title)s</a>";
                _lang["Notif_topic_watch_p_guest2"] = "Un invité a écrit un message dans <a href=\"/t%(topic_id)dp%(start)d-%(topic_name)s?nid=%(nid)s#%(post_id)d\">%(topic_title)s</a>";                
				_lang["Notif_topic_watch"] = "<a href=\"/u%(id)d\">%(name)s</a> a écrit un message dans <a href=\"/t%(topic_id)d-%(topic_name)s?nid=%(nid)s#%(post_id)d\">un sujet que vous suivez</a>";
                _lang["Notif_topic_watch_p"] = "<a href=\"/u%(id)d\">%(name)s</a> a écrit un message dans <a href=\"/t%(topic_id)dp%(start)d-%(topic_name)s?nid=%(nid)s#%(post_id)d\">un sujet que vous suivez</a>";
                _lang["Notif_topic_watch_guest"] = "Un invité a écrit un message dans <a href=\"/t%(topic_id)d-%(topic_name)s?nid=%(nid)s#%(post_id)d\">un sujet que vous suivez</a>";
                _lang["Notif_topic_watch_p_guest"] = "Un invité a écrit un message dans <a href=\"/t%(topic_id)dp%(start)d-%(topic_name)s?nid=%(nid)s#%(post_id)d\">un sujet que vous suivez</a>";
                _lang["Notif_mention"] = "<a href=\"/u%(id)d\">%(name)s</a> vous a tagué dans <a href=\"/t%(topic_id)dp%(start)d-%(topic_name)s?nid=%(nid)s#%(post_id)d\">un sujet</a>";
				_lang["Notif_like"] = "<a href=\"/u%(id)d\">%(name)s</a> a aimé votre message dans <a href=\"/t%(topic_id)dp%(start)d-%(topic_name)s?nid=%(nid)s#%(post_id)d\">%(topic_display_name)s</a>";
				_lang["Notif_dislike"] = "<a href=\"/u%(id)d\">%(name)s</a> n'a pas aimé votre message dans <a href=\"/t%(topic_id)dp%(start)d-%(topic_name)s?nid=%(nid)s#%(post_id)d\">%(topic_display_name)s</a>";
                _lang["Notif_hashtag"] = "Le mot-clé <a href=\"/tags/%(tag)s\">#%(tag)s</a> a été taggué dans <a href=\"/t%(topic_id)dp%(start)d-%(topic_name)s?nid=%(nid)s#%(post_id)d\">un sujet</a>";
				_lang["Notif_forum_watch"] = "%(name)s a publié <a title=\"%(topic_title)s\" href=\"/t%(topic_id)d-%(topic_name)s?nid=%(nid)s\">%(topic_title)s</a> dans le forum <a href=\"/f%(forum_id)d-%(forum_name)s?nid=%(nid)s\" title=\"%(forum_title)s\">%(forum_title)s</a>";
                _lang["Notif_advert_validate"] = "Votre annonce \"%(ad_title)s\" a été validée. <a href=\"/d%(ad_id)d-%(ad_url)s?nid=%(nid)s\">Voir l'annonce</a>";
                _lang["Notif_advert_delete"] = "Votre annonce \"%(ad_title)s\" a été suprimée.";
                _lang["Notif_advert_refuse"] = "Votre annonce \"%(ad_title)s\" a été  refusée. <a href=\"/post?d=%(ad_id)d&amp;f=%(forum_id)d&amp;nid=%(nid)s\">Voir l'annonce</a>";
                _lang["Notif_advert_expired"] = "Votre annonce \"\" est arrivée au terme de sa diffusion. Vous pouvez la remettre en ligne en <a href=\"/post?d=%(ad_id)d&amp;f=%(forum_id)d&amp;nid=%(nid)s\">cliquant ici</a>";
                _lang["All_PMs"] = "Mes messages privés";
                _lang["rank_title"] = "&#9674; Étranger de Camus";
                _lang["No_assigned_rank"] ="Aucun rang spécial assigné";
                _lang["Posts"] = "Messages";
                _lang["PMs"] = "MPs";
                _lang["Reputation"] ="Réputation";
                _lang["Twitter"] ="forumactif";
            

                    if(typeof(_board) == "undefined")
                        var _board = new Object();	
                    _board["reputation_active"] = 0;
                    _board["Forumotion"] = "Forumactif";
                    _board["toolbar_title_url"] = "https://www.forumactif.com";
                    _board["toolbar_logo"] = "https://2img.net/i/fa/i/toolbar/pa0.png";
                
$(document).ready(function(){Toolbar.init({"tbHeight":30,"tbTemplate":"subsilver"})})
//]]>
</script>
<link href="https://illiweb.com/rs3/89/frm/mentions/tooltipster.css" rel="stylesheet" type="text/css"/><style type="text/css">
#left table.d3ca77eb {border:0;border-collapse:collapse;}#left table.d3ca77eb table {table-layout:fixed;border-collapse:collapse;}div.d3ca77eb iframe{display: block!important!;visibility: visible!important!}iframe[src*=adstune]:not([style*=display]), iframe[src*=criteo]:not([style*=display]), iframe[src*=ad6b]:not([style*=display]), iframe[src*=z5x]:not([style*=display]), iframe[src*=doubleclick]:not([style*=display]) {display: block !important;visibility: visible !important;}div.hc1ab25e{ padding:10px 0 0 0 !important; }div.hc1ab25e{ font-size: 11px; color:#6f666a; }
body { margin: 0; padding: 0 !important; }
</style>
<!--[if lte IE 6]>
<style type="text/css">
/* http://msdn.microsoft.com/en-us/library/ms537634.aspx#Implement */
.fa_toolbar_XL_Sized { width: expression(document.documentElement.clientWidth ? (document.documentElement.clientWidth < 980 ? "980px": "100%") : (document.body.clientWidth < 980 ? "980px": "100%")); }
.fa_toolbar_L_Sized { width: expression(document.documentElement.clientWidth ? (document.documentElement.clientWidth < 774 ? "774px": "auto") : (document.body.clientWidth < 774 ? "774px": "auto")); }
.fa_toolbar_M_Sized { width: expression(document.documentElement.clientWidth ? (document.documentElement.clientWidth < 519 ? "519px": "auto") : (document.body.clientWidth < 519 ? "519px": "auto")); }
#fa_toolbar #fa_magnifier, #fa_toolbar #fa_fb, #fa_toolbar #fa_twitter, #fa_toolbar #fa_gp, #fa_toolbar #fa_mail, #fa_toolbar #fa_rss, #fa_toolbar #fa_hide, #fa_toolbar_hidden #fa_show { background-image: url('https://2img.net/i//fa/i/toolbar/toolbar.gif'); width: 30px; height: 30px; cursor: pointer; }
span.fa_tbMainElement, span.fa_tbMainElement a, #fa_show { zoom: 1; display: inline-block !important; line-height:30px; height: 30px; }
div.fa_tbMainElement, div.fa_tbMainElement a { zoom: 1; display: inline !important; line-height:30px; height: 30px; }
#fa_search #fa_textarea { margin-top: 1px; }
#fa_show { z-index:20002; }
#fa_menu { display: inline !important; height:30px; line-height:30px; }
#fa_menulist { width:175px; margin-top:30px; }
#fa_menulist li.fa_separator { font-size:1px; }
#fa_menu:hover :visited, #fa_right.notification #fa_notifications { color: #333333; background-color: #FFFFFF; }
#notif_list { margin-top:30px !important; }
.fa_fix { position:absolute !important; top: expression(0+((e=document.documentElement.scrollTop)?e:document.body.scrollTop)+"px") !important; z-index:10002; }
#fa_toolbar #live_notif { width: 330px; position: absolute; margin-top:30px; }
#fa_toolbar #fa_right #notif_list li { width:32em; }
</style>
<![endif]-->
<!--[if IE 7]>
<style type="text/css">
.fa_tbMainElement, .fa_tbMainElement a, #fa_toolbar_hidden > #fa_show { zoom: 1; display: inline !important; }
#fa_search #fa_textarea { margin-top: 1px; }
#fa_right > div { zoom: 1; display: inline !important; }
#fa_menulist { width:175px !important; margin-top: 30px !important; margin-left: 1px !important; }
#fa_menulist li a { white-space: nowrap; }
#fa_menulist li.fa_separator { font-size: 1px; line-height: 1px; }
#fa_toolbar #fa_right #notif_list { margin-top: 30px !important; }
#fa_toolbar #live_notif { width: 330px; position: absolute; margin-top:30px; }
#fa_toolbar #fa_right #notif_list li { width:30em; }
#fa_toolbar #fa_right #notif_list li .contentText { height: 2.6em; }
</style>
<![endif]-->
<script>if ('serviceWorker' in navigator && 'PushManager' in window) {
                            navigator.serviceWorker.register('/serviceworker.js')
                                .then(function(swReg) {
                                
                                  swRegistration = swReg;
                                  
                                  
                                })
                        }
                        
                     </script><script type="text/javascript">
    $(function(){
        $("#web_redirect").on("click", function(){
            var d = new Date();
            var cookie_string = "_mobile_version=0";
            d.setTime(d.getTime()+(365*24*60*60*1000));
            var expires = d.toUTCString();
            cookie_string += "; path=/";
            cookie_string += "; domain=" + window.location.hostname;
            cookie_string += "; expires=" + expires;
            document.cookie = cookie_string;
            location.reload();
        });
    }) ;
</script><script type="text/javascript">
    $(function(){
        $("#mobi_redirect").on("click", function(){
            var d = new Date();
            var cookie_string = "_mobile_version=1";
            d.setTime(d.getTime()+(365*24*60*60*1000));
            var expires = d.toUTCString();
            cookie_string += "; domain=" + window.location.hostname;
            cookie_string += "; expires=" + expires;
            document.cookie = cookie_string;
            location.reload();
        });
    }) ;
</script></head><body background="https://2img.net/i/fa/empty.gif" bgcolor="#e1ede6" link="#7c64de" text="#6f666a" vlink="#7c64de"><a name="top"></a><table align="center" border="0" cellpadding="10" cellspacing="0" class="bodylinewidth" width="50%"><tr><td class="bodyline"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td align="center" valign="middle" width="100%"><a href="/"><img alt="Bazzart" border="0" id="i_logo" src="https://2img.net/nsa40.casimages.com/img/2020/05/05/200505093028102441.gif" vspace="1"/></a><br/><div class="maintitle"></div><br/><span class="gen"><script src="/h2-"></script><script src="https://www.bazzart.org/12784.js"></script></span></td><table align="center" border="0" cellpadding="0" cellspacing="0"><tr><td><div class="barrenav"><a class="mainmenu" href="/"><img alt="Accueil" border="0" height="13" hspace="2" id="i_icon_mini_index" src="https://2img.net/i/fa/empty.gif" title="Accueil"/>Accueil</a>  <a class="mainmenu" href="/search" onclick="showhide(document.getElementById('search_menu')); return false;"><img alt="Rechercher" border="0" height="13" hspace="2" id="i_icon_mini_search" src="https://2img.net/i/fa/empty.gif" title="Rechercher"/>Rechercher</a>  <div id="search_menu" style="display:none;position:absolute;z-index:10000"><form action="/search" method="get"><input name="mode" type="hidden" value="searchbox"/><table border="0" cellpadding="3" cellspacing="0" class="forumline"><tr><th class="thHead">Rechercher</th></tr><tr><td align="center" class="row2"><input class="post" name="search_keywords" size="24" style="height:20px;margin-top:6px;margin-right:3px;" type="text"/><input class="button" type="submit" value="Go"/></td></tr><tr><td align="center" class="row2" nowrap="nowrap"><span class="genmed"> Résultats par : <input id="rposts" name="show_results" type="radio" value="posts"/><label for="rposts"> Messages</label><input checked="checked" id="rtopics" name="show_results" type="radio" value="topics"/><label for="rtopics"> Sujets</label> </span></td></tr><tr><td align="center" class="row2"><span class="genmed"><a href="/search" rel="nofollow" title="Recherche avancée"><img alt="Recherche avancée" border="0" height="13" hspace="3" src="https://2img.net/i/empty.gif" width="12"/> Recherche avancée</a></span></td></tr></table></form></div><a class="mainmenu" href="/register"><img alt="S'enregistrer" border="0" height="13" hspace="2" id="i_icon_mini_register" src="https://2img.net/i/fa/empty.gif" title="S'enregistrer"/>S'enregistrer</a>  <a class="mainmenu" href="/login" rel="nofollow"><img alt="Connexion" border="0" height="13" hspace="2" id="i_icon_mini_login" src="https://2img.net/i/fa/empty.gif" title="Connexion"/>Connexion</a>  </div> </td><!--
<table border="0" cellspacing ="" cellpadding="1"  class="cadrenews" align="center"><tr><td rowspan="3"> <div style="BACKGROUND-COLOR:#4B223E05; border: 1px solid #cd87b440 ; margin-top: -4px; max-width:900px;"><div style="display:inline-block;vertical-align:middle; height: 111px; width: 290px; overflow: auto;font-size: 11px; margin-right: 3px ; margin-left: 3px; text-align: justify; "> <span style="width: 150px; height: 30px;"> <span class="titrepetitePA"> BIENVENUE SUR BAZZART</span></span> Bazzart est tout d'abord un <u>forum de créations</u> pour les RPG où les membres peuvent partager ainsi avatars, icônes, gifs mais aussi des ressources. Vous y trouverez également des codes libres services, des lieux de partage IRL et un coin de publicité. En cas de problème pour activer votre compte, vous pourrez demander son activation juste<a rel="nofollow" target="_blank" class="postlink" href="https://www.bazzart.org/t134882-faire-activer-son-compte"> ici</a> et un membre du staff se chargera de la faire.</div></div></td><td><div style="BACKGROUND-COLOR:#4B223E05; border: 1px solid #cd87b440 ; margin-top: -4px;"> <div style="display:inline-block;vertical-align:middle; height: 111px; margin-left: 3px; margin-right: 3px; text-align: justify; font-size: 11px; width: 290px; overflow: auto;"> <span style="width: 150px; height: 30px;"><span class="titrepetitePA"> RÈGLES A RESPECTER</span></span> Nous rappelons que les <b> liens externes</b> à Bazzart dans les signatures sont interdits sous peine de recevoir un avertissement. Nous vous prions d'aller jeter un coup d'oeil dans le <a rel="nofollow" target="_blank" class="postlink" href="https://www.bazzart.org/t15-regles-generales">règlement</a>  en cas de doute. Nous vous rappelons que l'esprit de Bazzart sont le partage et le respect et que nous n'accepterons pas des comportements allant à cette encontre. Désormais, vous pourrez demander votre suppression <a rel="nofollow" target="_blank" class="postlink" href="https://www.bazzart.org/t134950-suppression-de-compte">ici</a> et joindre les liens à archiver. </div></td><td><div style="BACKGROUND-COLOR:#4B223E05; border: 1px solid #cd87b440 ; margin-top: -4px; "> <div style="display:inline-block;vertical-align:middle; height: 111px; margin-left: 1px; text-align: justify; font-size: 11px; width: 290px; overflow: auto;"><span style="width: 150px; height: 30px;"><span class="titrepetitePA">LES NOUVEAUTÉS</span></span>Pour cette nouvelle version, Bazzart a crée de nouvelles catégories; celle de <a rel="nofollow" target="_blank" class="postlink" href="https://www.bazzart.org/f289-recherche-de-rpg">recherches de rpg </a>mais aussi celle <a rel="nofollow" target="_blank" class="postlink" href="https://www.bazzart.org/f283-les-galeries">des galeries </a>dans laquelle vous pourrez librement poster vos créations (suite à une suggestion). N'hésitez pas à aller lire les nouveautés apportées au système du bazz'jury juste <a rel="nofollow" target="_blank" class="postlink" href="https://www.bazzart.org/t134937-fonctionnement-formulaire">ici </a>; les candidatures pour intégrer le bazz'jury sont désormais ouvertes à tout le monde ! Pour ne louper aucune nouveauté sur le forum, vous pouvez librement vous inscrire à la <a rel="nofollow" target="_blank" class="postlink" href="https://www.bazzart.org/t134944-s-inscrire-a-la-newsletter">newsletter </a>nouvellement instaurée ! </div></div></td></tr></table>--><div id="page-body"><div class="no-left" id="emptyidcc"><table cellpadding="0" cellspacing="0" class="three-col" width="100%"><tbody><tr><td valign="top" width="0"><div id="emptyidleft"></div></td><td valign="top" width="100%"><form action="/login" method="post" name="form_login"><table align="center" border="0" cellpadding="0" cellspacing="2" width="100%"><tr><td class="nav"><a class="nav" href="/"><span>Bazzart</span></a></td></tr></table><table align="center" border="0" cellpadding="4" cellspacing="0" class="forumline" width="100%"><tr><th class="thHead" colspan="3" height="25" nowrap="nowrap">Veuillez entrer votre nom d'utilisateur et votre mot de passe pour vous connecter.</th></tr><tr><td align="center" class="row1" width="100%"><table border="0" cellpadding="0" cellspacing="0"><tr><td class="row1 align_gauche"><table border="0" cellpadding="0" cellspacing="1" width="100%"><tr><td align="center" colspan="2"> </td></tr><tr><td class="align_droite" width="50%"><span class="gen">Nom d'utilisateur:</span></td><td width="50%"><input maxlength="40" name="username" size="25" type="text" value=""/></td></tr><tr><td class="align_droite"><span class="gen">Mot de passe:</span></td><td><input maxlength="32" name="password" size="25" type="password"/></td></tr><tr align="center"><td colspan="2"><span class="gen">Connexion automatique: <input checked="checked" name="autologin" type="checkbox"/></span></td></tr><tr align="center"><td colspan="2"><input name="redirect" type="hidden" value=""/><input name="query" type="hidden" value=""/><input class="mainoption" name="login" type="submit" value="Connexion"/></td></tr><tr align="center"><td colspan="2"><br/><span class="gensmall"><a class="gensmall" href="/register">:: S'enregistrer</a>  |  <a class="gensmall" href="/profile?mode=sendpassword">:: Récupérer mon mot de passe</a></span></td></tr></table></td></tr></table></td></tr></table></form><script type="text/javascript">//<![CDATA[
if(document.forms['form_login'].elements['username'].value == '') {document.forms['form_login'].elements['username'].focus();} else {document.forms['form_login'].elements['password'].focus();}//]]></script></td><td valign="top" width="0"><div id="emptyidright"></div></td></tr></tbody></table></div></div><!--
 close div id="page-body" --><div id="page-footer"><div align="center"><div class="gen"><strong><a href="https://www.forumactif.com" target="_blank">Créer un forum</a></strong> | <span class="gensmall">©</span> <a href="https://www.forumactif.com/phpbb" target="_blank">phpBB</a> | <a href="https://forum.forumactif.com/" name="bottom" target="_blank">Forum gratuit d'entraide</a> | <a href="/abuse?page=%2Flogin&amp;report=1" rel="nofollow">Signaler un abus</a> | <strong><a href="https://www.forumactif.com/forum-gratuit" target="_blank">Forum gratuit</a></strong></div></div></div></tr></table><script type="text/javascript">//<![CDATA[
fa_endpage();//]]></script></tr></table></td></tr></table></body></html>