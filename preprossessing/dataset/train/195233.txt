<!DOCTYPE html>
<html lang="en-us">
<head>
<title>Baseball History in 1914 Federal League by Baseball Almanac</title>
<meta charset="utf-8"/>
<meta content="Baseball History 1914 Federal League FL baseball history 1914 federal league fl" name="keywords"/>
<meta content="Baseball history in 1914 Federal League by Baseball Almanac - a walk through the 1914 Federal League season with stats, top 25, final standings, rosters and other baseball history." name="description"/>
<meta content="index, follow" name="robots"/>
<meta content="Baseball Almanac, Inc." name="Author"/>
<!-- Mobile viewport -->
<meta content="width=device-width; initial-scale=1.0" name="viewport"/>
<link href="/css/styles1.1.css" rel="stylesheet"/>
<link href="/css/menu.css" rel="stylesheet"/>
</head>
<body>
<!-- BEGIN GOOGLE ANALYTICS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-1805063-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-1805063-1');
</script>
<!-- END GOOGLE ANALYTICS -->
<div class="google-adsense" style="text-align: center; margin: 10px 0">
<!-- BEGIN FREESTAR -->
<script data-cfasync="false" type="text/javascript">
  var freestar = freestar || {};
  freestar.hitTime = Date.now();
  freestar.queue = freestar.queue || [];
  freestar.config = freestar.config || {};
  freestar.debug = window.location.search.indexOf('fsdebug') === -1 ? false : true;
  freestar.config.enabled_slots = [];
  !function(a,b){var c=b.getElementsByTagName("script")[0],d=b.createElement("script"),e="https://a.pub.network/baseball-almanac-com";e+=freestar.debug?"/qa/pubfig.min.js":"/pubfig.min.js",d.async=!0,d.src=e,c.parentNode.insertBefore(d,c)}(window,document);
  freestar.initCallback = function () { (freestar.config.enabled_slots.length === 0) ? freestar.initCallbackCalled = false : freestar.newAdSlots(freestar.config.enabled_slots) }
</script>
<!-- END FREESTAR -->
<!-- Tag ID: Baseballalmanac_leaderboard_ATF -->
<div align="center" id="Baseballalmanac_leaderboard_ATF">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_leaderboard_ATF", slotId: "Baseballalmanac_leaderboard_ATF" });
</script>
</div>
</div>
<div id="wrapper">
<div class="header-container">
<div class="header">
<div class="container">
<a class="flex-none" href="/">
<span class="hidden">Baseball Almanac</span>
<img alt="Baseball Almanac" class="logo" src="/images/baseball-almanac-logo.png"/>
</a>
<div class="menu-items hidden" data-menu="">
<div>
<a data-flip="" data-toggle="menu-1" href="#">
                    History
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-1="">
<li><a href="/asgmenu.shtml">All-Star Game</a></li>
<li><a href="/League_Championship_Series.shtml">A.L.C.S. &amp; N.L.C.S.</a></li>
<li><a href="/me_award.shtml">Awards</a></li>
<li><a href="/stadium.shtml">Ballparks</a></li>
<li><a href="/college/colleges.shtml">College Baseball</a></li>
<li><a href="/division_series/division_series.shtml">Division Series</a></li>
<li><a href="/draft/baseball_draft.shtml">Draft</a></li>
<li><a href="/mgrmenu.shtml">Managers</a></li>
<li><a href="/opening_day/opening_day.shtml">Opening Day</a></li>
<li><a href="/teammenu.shtml">Team by Team</a></li>
<li><a href="/umpiresmenu.shtml">Umpires</a></li>
<li><a href="/wild_card/MLB_Wild_Card_Game.shtml">Wild Card Game</a></li>
<li><a href="/ws/wsmenu.shtml">World Series</a></li>
<li><a href="/yearmenu.shtml">Year by Year</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-2" href="#">
                    Players
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-2="">
<li><a href="/fammenu.shtml">Baseball Families</a></li>
<li><a href="/players/baseball_biographies.shtml">Biographies</a></li>
<li><a href="/players/birthplace.php">Birthplace Analysis</a></li>
<li><a href="/featmenu.shtml">Fabulous Feats</a></li>
<li><a href="/frstmenu.shtml">Famous Firsts</a></li>
<li><a href="/graves/baseball_graves.shtml">Grave Sites</a></li>
<li><a href="/hofmenu.shtml">Hall of Fame</a></li>
<li><a href="/players/baseball_interviews.shtml">Interviews</a></li>
<li><a href="/deaths/chart/baseball_player_obituaries.shtml">Obituaries</a></li>
<li><a href="/players/deathplace.php">Place of Death Analysis</a></li>
<li><a href="/players/ballplayer.shtml">The Ballplayers</a></li>
<li><a href="/quomenu.shtml">Quotes</a></li>
<li><a href="/players/baseball_births.php">Year of Birth Analysis</a></li>
<li><a href="/players/baseball_deaths.php">Year of Death Analysis</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-3" href="#">
                    Leaders
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-3="">
<li><a href="/baseball_attendance.shtml">Attendance Data</a></li>
<li><a href="/himenu.shtml">Hitting Charts</a></li>
<li><a href="/pimenu.shtml">Pitching Charts</a></li>
<li><a href="/rb_menu.shtml">Record Books</a></li>
<li><a href="/teamstats/statmaster.php">Statmaster</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-4" href="#">
                    Left Field
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-4="">
<li><a href="/players/Oldest_Living_Baseball_Players.php">500 Oldest Players</a></li>
<li><a href="/automenu.shtml">Autographs</a></li>
<li><a href="/baseball_cards/baseball_cards.php">Baseball Cards</a></li>
<li><a href="/charts/baseball_charts.shtml">Baseball Charts</a></li>
<li><a href="/limenu.shtml">Baseball Lists</a></li>
<li><a href="/bookmenu.shtml">Book Shelf</a></li>
<li><a href="/players/Cups_of_Coffee.php">Cups of Coffee</a></li>
<li><a href="/gam_menu.shtml">Fun &amp; Games</a></li>
<li><a href="/humomenu.shtml">Humor &amp; Jokes</a></li>
<li><a href="/mve_time.shtml">Movie Time</a></li>
<li><a href="/baseball_news.shtml">News Feeds</a></li>
<li><a href="/poems.shtml">Poetry &amp; Song</a></li>
<li><a href="/articles/articles.shtml">Research Articles</a></li>
<li><a href="/baseball_uniform_numbers.shtml">Uniform Numbers</a></li>
<li><a href="/prz_menu.shtml">U.S. Presidents</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-5" href="#">
                    Help
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-5="">
<li><a href="/about.shtml">Advertising</a></li>
<li><a href="/blog/">Blog</a></li>
<li><a href="/feedmenu.shtml">Feedback</a></li>
<li><a href="/mlmstart.shtml">Newsletter</a></li>
<li><a href="/rulemenu.shtml">Rules</a></li>
<li><a href="/scoring.shtml">Scoring</a></li>
<li><a href="/Search.shtml">Search &amp; Find</a></li>
<li><a href="/bstatmen.shtml">Stats 101</a></li>
</ul>
</div>
<div class="extra">
<a class="bg-blue-500" href="https://twitter.com/BaseballAlmanac" target="_blank">
<i class="fab fa-twitter"></i>
                    Follow @BaseballAlmanac
                </a>
<a class="bg-blue-700" href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank">
<i class="fab fa-facebook"></i>
                    Find us on Facebook
                </a>
</div>
</div>
<div class="overlay hidden" data-menu="" data-proxy="menu"></div>
<form class="search-form">
<div class="social">
<div data-search="">
<a href="https://twitter.com/BaseballAlmanac" target="_blank"><i class="fab fa-twitter"></i></a>
<a href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank"><i class="fab fa-facebook-f"></i></a>
<a href="https://www.instagram.com/baseballalmanac/" target="_blank"><i class="fab fa-instagram"></i></a>
<span></span>
</div>
<input class="search hidden" data-search="" data-toggle-focus="" id="q" name="q" placeholder="Custom Search" type="text"/>
<a data-toggle="search" data-toggle-bg=""><i class="fas fa-search"></i></a>
<a class="" data-toggle="menu" data-toggle-bg="" data-toggle-scroll=""><i class="fas fa-bars"></i></a>
</div>
</form>
<script>
            // JavaScript code that should follow the search box code (must part)
  			// add a listener for form submission, i.e. when user hits Enter or clicks to any submit button form has
  			document.querySelector('.search-form').addEventListener('submit', function(e) {
    			// do not actually submit the form, we'll do something else :)
    			e.preventDefault();
    			// read the search query for input tag, i.e. user searches for "django" let's say
    			var q = document.querySelector('input[name="q"]').value;
    			// just proceed if user has typed something
    			if (q.length > 0) {
      				// go to search results page which is search.html here but can be anything you like with "gsc.q" hash parameter equal to search query
      				window.open('/custom_search.shtml?q=' + q, '_self');
    			}
  			});

			// check if there is any text in the search string
			if (window.location.search.length > 0) {
  				// retrieve the "q" keyed search string value
  				var q = window.location.search.substring(1).split('&').filter(function(x) {
    				return x.substring(0, 2) === 'q=';
  				})[0].substring(2);
  				// put the value to the search box if it is not empty
  				if (q.length > 0) {
    				document.querySelector('input[name="q"]').value = q;
  				}
			}
  		</script>
</div>
</div>
<script src="/js/navigation.min.js" type="text/javascript"></script>
</div>
<div class="container">
<div class="intro">
<h1><a href="../yearmenu.shtml" title="YEAR-BY-YEAR BASEBALL HISTORY">YEAR IN REVIEW</a> : 1914 Federal League</h1>
<h2>Off the field...</h2>
<p>The United States finally completed the construction of the Panama Canal. The fifty-one mile long waterway ran across the Isthmus of Panama, connecting the Atlantic (by way of the Caribbean Sea) and Pacific oceans. After the United States acquired territory in the Caribbean and in the Pacific as a result of the Spanish-American War (1899), U.S. control over a man-made canal seemed imperative. In 1912, "The Panama Canal Act" was passed (exempting tolls from American cargo ships engaged in coastwise trade) igniting a protest by Great Britain that was eventually repealed in 1914 through the efforts of President Woodrow Wilson.</p>
<h2>In the American League...</h2>
<p>Cleveland Indians shortstop Ray Chapman stumbled his way into an unwanted record on June 20<sup>th</sup> after committing four errors in the fifth inning during a 7-1 loss to the New York Yankees at League Park II.</p>
<p>During the second game of an August doubleheader in Washington, Detroit Tigers pitcher Hooks Dauss combined with four Senators aces to hit a record seven batters for a Major League mark that remained unmatched until the 1971 season.</p>
<p>In September, New York Yankees shortstop Roger Peckinpaugh replaced Frank Chance to become the club's all-time youngest skipper (twenty-three), and the seventh in its twelve-year existence. He later went on to win nine of seventeen games and eventually managed Cleveland in 1928.</p>
<h2>In the National League...</h2>
<p>On June 9<sup>th</sup> at the Baker Bowl, Pittsburgh Pirate legend Honus Wagner joined Cap Anson as the only other member of the "3,000 Hit Club". Wagner collected the game-winning double off the Philadelphia Phillies' Erskine Mayer in the ninth-inning of his two-thousand three-hundred thirty second game.</p>
<p>Pittsburgh and New York went head-to-head for a twenty-one innings on July 17<sup>th</sup> before Larry Doyle's two-run home run sealed a 3-1 Giants victory over the Pirates. The Forbes Field marathon set a Major League mark as the longest "non-walk game" in the history of organized baseball.</p>
<p>Brooklyn Dodgers first baseman Jake Daubert tied a Major League mark on August 15<sup>th</sup> after recording four sacrifice bunts in the second game of a doubleheader sweep against the Philadelphia Phillies (8-4, 13-5). Daubert had also placed two sacrifice bunts in the first game after an ankle injury impeded his ability to run.</p>
<h2>In the Federal League...</h2>
<p>1914 debuted the short-lived Federal League after John T. Powers of Chicago convinced a group of entrepreneurs that the growing popularity of baseball could support a third major league. Eight teams entered the inaugural season with clubs based in Brooklyn, Chicago, St. Louis and Pittsburgh as well as Baltimore, Kansas City, Buffalo and Indianapolis which had been the home for AAA teams. All eight cities constructed brand new ballparks including the Chicago Whales who played in what would eventually be known as Wrigley Field.</p>
<p>To effectively compete, the owners lured eighty-one former Major League players (eighteen of which were active) and one-hundred forty Minor League players (twenty-five of which were active) into the Federal League Baseball Company, Inc.</p>
<p>On May 6<sup>th</sup>, Pittsburgh Rebel Ed Lennox collected the only Federal League cycle during a 10-4 win over the Kansas City Packers.</p>
<h2>Around the League...</h2>
<p>A joint committee representing both the American and National Leagues voted that a "runner touched or held by a coach while rounding third base was officially out" and that "coaches could now assist other members of their team, not just the base runners". Pitchers were also allowed to stand on the rubber (vs. standing behind the rubber until ready to pitch) and base runners were no longer permitted to run on an infield fly. A motion to eliminate the intentional walk was also rejected along with an attempt to legalize Sunday baseball in Massachusetts.</p>
<p>In April, the twenty-five player limit was suspended in both the American and National Leagues. With uncertainty over who has signed with what teams, it was almost impossible to verify how many players could be on any club's roster at any one time.</p>
<p>On April 22<sup>nd</sup>, a nineteen year-old pitcher named Babe Ruth made his debut in the International League with a six-hit, 6-0 win for Baltimore over Buffalo. The second batter he faced was Joe McCarthy, the manager he would later play for as a New York Yankee.</p>
</div>
<div class="topquote">
<img alt="Baseball Almanac Top Quote" src="/images/typewriter-with-paper.png"/>
<p>"I believe that man (James A. - the Federal League President) Gilmore not only can convince a millionaire that the moon is made of green cheese, but can induce him to invest money in a cheese factory on the moon." - National League President John Tener</p>
</div>
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header" colspan="5">
<h2>1914 Federal League Player Review</h2>
<p>Hitting Statistics League Leaderboard</p>
</td>
</tr>
<tr>
<td class="banner">Statistic</td>
<td class="banner">Name(s)</td>
<td class="banner">Team(s)</td>
<td class="banner">#</td>
<td class="banner">Top 25</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hiwalk4.shtml" title="YEAR BY YEAR LEADERS FOR BASES ON BALLS">Base on Balls</a></td>
<td class="datacolBox"><a href="/players/player.php?p=wicklal01">Al Wickland</a></td>
<td class="datacolBox">Chicago</td>
<td class="datacolBox">81</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=BB&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hibavg3.shtml" title="YEAR BY YEAR LEADERS FOR BATTING AVERAGE">Batting Average</a></td>
<td class="datacolBox"><a href="/players/player.php?p=kauffbe01">Benny Kauff</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">.370</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=BAVG&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hi2b3.shtml" title="YEAR BY YEAR LEADERS FOR DOUBLES">Doubles</a></td>
<td class="datacolBox"><a href="/players/player.php?p=kauffbe01">Benny Kauff</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">44</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=2B&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hihits4.shtml" title="YEAR BY YEAR LEADERS FOR HITS">Hits</a></td>
<td class="datacolBox"><a href="/players/player.php?p=kauffbe01">Benny Kauff</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">211</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=H&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hihr5.shtml" title="YEAR BY YEAR LEADERS FOR HOME RUNS">Home Runs</a></td>
<td class="datacolBox"><a href="/players/player.php?p=zwilldu01">Dutch Zwilling</a></td>
<td class="datacolBox">Chicago</td>
<td class="datacolBox">16</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=HR&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hiobp4.shtml" title="YEAR BY YEAR LEADERS FOR ON BASE PERCENTAGE">On Base Percentage</a></td>
<td class="datacolBox"><a href="/players/player.php?p=kauffbe01">Benny Kauff</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">.447</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=OBP&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hirbi5.shtml" title="YEAR BY YEAR LEADERS FOR RUNS BATTED IN">RBI</a></td>
<td class="datacolBox"><a href="/players/player.php?p=laporfr01">Frank LaPorte</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">107</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=RBI&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hiruns2.shtml" title="YEAR BY YEAR LEADERS FOR RUNS SCORED">Runs</a></td>
<td class="datacolBox"><a href="/players/player.php?p=kauffbe01">Benny Kauff</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">120</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=R&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hislug4.shtml" title="YEAR BY YEAR LEADER FOR SLUGGING AVERAGE">Slugging Average</a></td>
<td class="datacolBox"><a href="/players/player.php?p=evansst01">Steve Evans</a></td>
<td class="datacolBox">Brooklyn</td>
<td class="datacolBox">.556</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=SLG&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hisb4.shtml" title="YEAR BY YEAR LEADERS FOR STOLEN BASES">Stolen Bases</a></td>
<td class="datacolBox"><a href="/players/player.php?p=kauffbe01">Benny Kauff</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">75</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=SB&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hitotb4.shtml" title="YEAR BY YEAR LEADERS FOR TOTAL BASES">Total Bases</a></td>
<td class="datacolBox"><a href="/players/player.php?p=kauffbe01">Benny Kauff</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">305</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=TB&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue middle" rowspan="2"><a href="../hitting/hitrip4.shtml" title="YEAR BY YEAR LEADERS FOR TRIPLES">Triples</a></td>
<td class="datacolBox"><a href="/players/player.php?p=esmonji01">Jimmy Esmond</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox" rowspan="2">15</td>
<td class="datacolBox" rowspan="2"><a href="/yearly/top25.php?s=3B&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBox"><a href="/players/player.php?p=evansst01">Steve Evans</a></td>
<td class="datacolBox">Brooklyn</td>
</tr>
<tr>
<td class="banner">Statistic</td>
<td class="banner">Name(s)</td>
<td class="banner">Team(s)</td>
<td class="banner">#</td>
<td class="banner">Top 25</td>
</tr>
</table>
</div>
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header" colspan="5">
<h2>1914 Federal League Pitcher Review</h2>
<p>Pitching Statistics League Leaderboard</p>
</td>
</tr>
<tr>
<td class="banner">Statistic</td>
<td class="banner">Name(s)</td>
<td class="banner">Team(s)</td>
<td class="banner">#</td>
<td class="banner">Top 25</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../pitching/picomg4.shtml" title="YEAR BY YEAR LEADERS FOR COMPLETE GAMES">Complete Games</a></td>
<td class="datacolBox"><a href="/players/player.php?p=hendrcl01">Claude Hendrix</a></td>
<td class="datacolBox">Chicago</td>
<td class="datacolBox">34</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=CG&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue middle"><a href="../pitching/piera4.shtml" title="YEAR BY YEAR LEADERS FOR EARNED RUN AVERAGE">ERA</a></td>
<td class="datacolBox"><a href="/players/player.php?p=johnsra01">Rankin Johnson</a></td>
<td class="datacolBox">Chicago</td>
<td class="datacolBox">1.58</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=ERA&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue middle" rowspan="2"><a href="../pitching/pigamp4.shtml" title="YEAR BY YEAR LEADERS FOR GAMES PITCHED">Games</a></td>
<td class="datacolBox"><a href="/players/player.php?p=falkecy01">Cy Falkenberg</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox" rowspan="2">49</td>
<td class="datacolBox" rowspan="2"><a href="/yearly/top25.php?s=G&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBox"><a href="/players/player.php?p=hendrcl01">Claude Hendrix</a></td>
<td class="datacolBox">Chicago</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../pitching/pisave4.shtml" title="YEAR BY YEAR LEADERS FOR SAVES">Saves</a></td>
<td class="datacolBox"><a href="/players/player.php?p=fordru01">Russ Ford</a></td>
<td class="datacolBox">Buffalo</td>
<td class="datacolBox">6</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=SV&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue"><a href="../pitching/pishut4.shtml" title="YEAR BY YEAR LEADERS FOR SHUTOUTS">Shutouts</a></td>
<td class="datacolBox"><a href="/players/player.php?p=falkecy01">Cy Falkenberg</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">9</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=SHU&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue"><a href="../pitching/pistrik4.shtml" title="YEAR BY YEAR LEADERS FOR STRIKEOUTS">Strikeouts</a></td>
<td class="datacolBox"><a href="/players/player.php?p=falkecy01">Cy Falkenberg</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">236</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=K&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue"><a href="../pitching/piwper4.shtml" title="YEAR BY YEAR LEADERS FOR WINNING PERCENTAGE">Winning Percentage</a></td>
<td class="datacolBox"><a href="/players/player.php?p=fordru01">Russ Ford</a></td>
<td class="datacolBox">Buffalo</td>
<td class="datacolBox">.778</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=WP&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="datacolBlue"><a href="../pitching/piwins4.shtml" title="YEAR BY YEAR LEADERS FOR WINS">Wins</a></td>
<td class="datacolBox"><a href="/players/player.php?p=hendrcl01">Claude Hendrix</a></td>
<td class="datacolBox">Chicago</td>
<td class="datacolBox">29</td>
<td class="datacolBox"><a href="/yearly/top25.php?s=W&amp;l=FL&amp;y=1914">Top 25</a></td>
</tr>
<tr>
<td class="banner">Statistic</td>
<td class="banner">Name(s)</td>
<td class="banner">Team(s)</td>
<td class="banner">#</td>
<td class="banner">Top 25</td>
</tr>
</table>
</div>
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header" colspan="5">
<h2>1914 Federal League</h2>
<p>Team Standings</p>
</td>
</tr>
<tr>
<td class="banner">Team [Click for roster]</td>
<td class="banner">Wins</td>
<td class="banner">Losses</td>
<td class="banner">WP</td>
<td class="banner">GB</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teamstats/roster.php?y=1914&amp;t=IND">Indianapolis Hoosiers</a></td>
<td class="datacolBox">88</td>
<td class="datacolBox">65</td>
<td class="datacolBox">.575</td>
<td class="datacolBox">0</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teamstats/roster.php?y=1914&amp;t=CHF">Chicago Whales</a></td>
<td class="datacolBox">87</td>
<td class="datacolBox">67</td>
<td class="datacolBox">.565</td>
<td class="datacolBox">1½</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teamstats/roster.php?y=1914&amp;t=BLF">Baltimore Terrapins</a></td>
<td class="datacolBox">84</td>
<td class="datacolBox">70</td>
<td class="datacolBox">.545</td>
<td class="datacolBox">4½</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teamstats/roster.php?y=1914&amp;t=BUF">Buffalo Buffeds</a></td>
<td class="datacolBox">80</td>
<td class="datacolBox">71</td>
<td class="datacolBox">.530</td>
<td class="datacolBox">7</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teamstats/roster.php?y=1914&amp;t=BRF">Brooklyn Tip-Tops</a></td>
<td class="datacolBox">77</td>
<td class="datacolBox">77</td>
<td class="datacolBox">.500</td>
<td class="datacolBox">11½</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teamstats/roster.php?y=1914&amp;t=KCF">Kansas City Packers</a></td>
<td class="datacolBox">67</td>
<td class="datacolBox">84</td>
<td class="datacolBox">.444</td>
<td class="datacolBox">20</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teamstats/roster.php?y=1914&amp;t=PTF">Pittsburgh Rebels</a></td>
<td class="datacolBox">64</td>
<td class="datacolBox">86</td>
<td class="datacolBox">.427</td>
<td class="datacolBox">22½</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teamstats/roster.php?y=1914&amp;t=SLF">St. Louis Terriers</a></td>
<td class="datacolBox">62</td>
<td class="datacolBox">89</td>
<td class="datacolBox">.411</td>
<td class="datacolBox">25</td>
</tr>
</table>
</div>
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header" colspan="3">
<h2>1914 Federal League Team Review</h2>
<p>Hitting Statistics League Leaderboard</p>
</td>
</tr>
<tr>
<td class="banner">Statistic</td>
<td class="banner">Team</td>
<td class="banner">#</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hiwalk4.shtml" title="YEAR BY YEAR LEADERS FOR BASES ON BALLS">Base on Balls</a></td>
<td class="datacolBox">Chicago</td>
<td class="datacolBox">520</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hibavg3.shtml" title="YEAR BY YEAR LEADERS FOR BATTING AVERAGE">Batting Average</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">.285</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hi2b3.shtml" title="YEAR BY YEAR LEADERS FOR DOUBLES">Doubles</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">230</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hihits4.shtml" title="YEAR BY YEAR LEADERS FOR HITS">Hits</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">1,474</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hihr5.shtml" title="YEAR BY YEAR LEADERS FOR HOME RUNS">Home Runs</a></td>
<td class="datacolBox">Chicago</td>
<td class="datacolBox">52</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hiobp4.shtml" title="YEAR BY YEAR LEADERS FOR ON BASE PERCENTAGE">On Base Percentage</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">.349</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hiruns2.shtml" title="YEAR BY YEAR LEADERS FOR RUNS SCORED">Runs</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">762</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hislug4.shtml" title="YEAR BY YEAR LEADER FOR SLUGGING AVERAGE">Slugging Average</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">.383</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../hitting/hisb4.shtml" title="YEAR BY YEAR LEADERS FOR STOLEN BASES">Stolen Bases</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">273</td>
</tr>
<tr>
<td class="datacolBlue middle" rowspan="2"><a href="../hitting/hitrip4.shtml" title="YEAR BY YEAR LEADERS FOR TRIPLES">Triples</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox" rowspan="2">90</td>
</tr>
<tr>
<td class="datacolBox">Pittsburgh</td>
</tr>
</table>
</div>
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header" colspan="3">
<h2>1914 Federal League Team Review</h2>
<p>Pitching Statistics League Leaderboard</p>
</td>
</tr>
<tr>
<td class="banner">Statistic</td>
<td class="banner">Team</td>
<td class="banner">#</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../pitching/picomg4.shtml" title="YEAR BY YEAR LEADERS FOR COMPLETE GAMES">Complete Games</a></td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">104</td>
</tr>
<tr>
<td class="datacolBlue middle"><a href="../pitching/piera4.shtml" title="YEAR BY YEAR LEADERS FOR EARNED RUN AVERAGE">ERA</a></td>
<td class="datacolBox">Chicago</td>
<td class="datacolBox">2.44</td>
</tr>
<tr>
<td class="datacolBlue">Fewest Hits Allowed</td>
<td class="datacolBox">Chicago</td>
<td class="datacolBox">1,204</td>
</tr>
<tr>
<td class="datacolBlue">Fewest Home Runs Allowed</td>
<td class="datacolBox">Indianapolis</td>
<td class="datacolBox">29</td>
</tr>
<tr>
<td class="datacolBlue">Fewest Walks Allowed</td>
<td class="datacolBox">Baltimore</td>
<td class="datacolBox">392</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../pitching/pisave4.shtml" title="YEAR BY YEAR LEADERS FOR SAVES">Saves</a></td>
<td class="datacolBox">Buffalo</td>
<td class="datacolBox">16</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../pitching/pishut4.shtml" title="YEAR BY YEAR LEADERS FOR SHUTOUTS">Shutouts</a></td>
<td class="datacolBox">Chicago</td>
<td class="datacolBox">17</td>
</tr>
<tr>
<td class="datacolBlue"><a href="../pitching/pistrik4.shtml" title="YEAR BY YEAR LEADERS FOR STRIKEOUTS">Strikeouts</a></td>
<td class="datacolBox">Baltimore</td>
<td class="datacolBox">732</td>
</tr>
<tr>
<td class="datacolBox" colspan="3">Seasonal Events: All-Star Game | <a href="../ws/yr1914ws.shtml">World Series</a><br/>Navigation: <a href="../yearmenu.shtml">Year in Review Menu</a> | Previous Season | <a href="yr1915f.shtml">Next Season</a><br/>Miscellaneous: <a href="yr1914a.shtml">A.L. Leaderboard</a> | <a href="yr1914n.shtml">N.L. Leaderboard</a> <br/> <a href="final.php?y=1914&amp;l=FL">Retirements</a> | <a href="debut.php?y=1914&amp;l=FL">Rookies List</a></td>
</tr>
</table>
</div>
<img alt="baseball almanac flat baseball" class="flat-baseball-img" src="../images/ball-red.png"/>
<!-- Tag ID: Baseballalmanac_Medrec_A -->
<div align="center" id="Baseballalmanac_Medrec_A">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_Medrec_A", slotId: "Baseballalmanac_Medrec_A" });
</script>
</div>
<div class="fast-facts">
<h3><img alt="baseball almanac fast facts" src="/images/fast-facts-logo.png"/></h3>
<p>Did you know that a Chicago newspaper broke a story in early 1914 describing secret meetings between the Federal League president and <a href="/players/player.php?p=cobbty01">Ty Cobb</a>? Attempts were made to sign The Georgia Peach to a five-year $75,000 contract, but <a href="/players/player.php?p=cobbty01">Cobb</a> declined and remained with the Detroit Tigers.</p>
<p>Future members of the National Baseball Hall of Fame who participated in the Federal League during 1914 included <a href="/players/player.php?p=brownmo01">Mordecai Brown</a>, <a href="/players/player.php?p=mckecbi01">Bill McKechnie</a>, <a href="/players/player.php?p=roushed01">Edd Roush</a>, <a href="/players/player.php?p=tinkejo01">Joe Tinker</a> and <a href="/players/player.php?p=wardjo01">John Montgomery Ward</a>.</p>
<p>When the first Federal League Opening Day game was played on April 13, 1914, many fans were turned away due to ballpark restrictions. Many of those "rejected" fans literally crossed the street where they were able to watch an exhibition game at Orioles Park between the International League Orioles, who had a hometown phenom on the mound named <a href="/players/player.php?p=ruthba01">Babe Ruth</a>, and the <a href="../teams/giants.shtml">National League Giants</a>.</p>
</div>
</div>
<div class="footer">
<!-- Tag ID: Baseballalmanac_leaderboard_BTF -->
<div align="center" id="Baseballalmanac_leaderboard_BTF">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_leaderboard_BTF", slotId: "Baseballalmanac_leaderboard_BTF" });
</script>
</div>
<div class="container">
<div class="notes">
<a href="/"><img alt="Baseball Almanac" src="/images/baseball-almanac-logo.png" style="max-width: 180px; margin; 0 auto"/></a>
<p>Where what happened yesterday<br/> is being preserved today.</p>
<div class="social">
<a href="https://twitter.com/BaseballAlmanac" target="_blank"><i class="fab fa-twitter"></i></a>
<a href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank"><i class="fab fa-facebook-f"></i></a>
<a href="https://www.instagram.com/baseballalmanac/" target="_blank"><i class="fab fa-instagram"></i></a>
</div>
</div>
<div class="links">
<div>
<span>Stats</span>
<a href="/me_award.shtml">Awards</a>
<a href="/featmenu.shtml">Fabulous Feats</a>
<a href="/frstmenu.shtml">Famous Firsts</a>
<a href="/hofmenu.shtml">Hall of Fame</a>
<a href="/himenu.shtml">Hitting Charts</a>
<a href="/limenu.shtml">Legendary Lists</a>
<a href="/pimenu.shtml">Pitching Charts</a>
<a href="/rb_menu.shtml">Record Books</a>
<a href="/rulemenu.shtml">Rules</a>
<a href="/scoring.shtml">Scoring</a>
<a href="/teamstats/statmaster.php">Statmaster</a>
<a href="/bstatmen.shtml">Stats 101</a>
<a href="/yearmenu.shtml">Year by Year</a>
</div>
<div>
<span>People</span>
<a href="/automenu.shtml">Autographs</a>
<a href="/players/ballplayer.shtml">Ballplayers</a>
<a href="/fammenu.shtml">Baseball Families</a>
<a href="/players/baseball_interviews.shtml">Interviews</a>
<a href="/mgrmenu.shtml">Managers</a>
<a href="/deaths/chart/baseball_player_obituaries.shtml">Obituaries</a>
<a href="/quomenu.shtml">Quotes</a>
<a href="/teammenu.shtml">Team by Team</a>
<a href="/umpiresmenu.shtml">Umpires</a>
<a href="/prz_menu.shtml">US Presidents</a>
</div>
<div>
<span>Places</span>
<a href="/asgmenu.shtml">All-Star Game</a>
<a href="/stadium.shtml">Ballparks</a>
<a href="/division_series/division_series.shtml">Division Series</a>
<a href="/graves/baseball_graves.shtml">Grave Sites</a>
<a href="/League_Championship_Series.shtml">LCS</a>
<a href="/opening_day/opening_day.shtml">Opening Day</a>
<a href="/ws/wsmenu.shtml">World Series</a>
</div>
<div>
<span>Other</span>
<a href="/about.shtml">Advertising</a>
<a href="/baseball_cards/baseball_cards.php">Baseball Cards</a>
<a href="/bookmenu.shtml">Book Shelf</a>
<a href="/feedmenu.shtml">Feedback</a>
<a href="/gam_menu.shtml">Fun &amp; Games</a>
<a href="/humomenu.shtml">Humor &amp; Jokes</a>
<a href="/mve_time.shtml">Movie Time</a>
<a href="/mlmstart.shtml">Newsletter</a>
<a href="/baseball_news.shtml">News Feeds</a>
<a href="/poems.shtml">Poetry &amp; Song</a>
<a href="/privacy-policy.shtml">Privacy Policy</a>
<a href="/Search.shtml">Search &amp; Find</a>
<a href="/support.shtml">Support</a>
</div>
</div>
<div class="copyright">
<div class="legal">
<p>Copyright 1999-<script type="text/javascript">
				copyright=new Date();
				update=copyright.getFullYear();
				document.write(update);
				</script>. All Rights Reserved by Baseball Almanac, Inc.<br/>Hosted by <a href="https://www.hosting4less.com" rel="nofollow" target="_blank">Hosting 4 Less</a>. Part of the <a href="https://www.baseball-almanac.com/">Baseball Almanac</a> Family</p>
</div>
<div class="external">
<a href="http://www.755homeruns.com/" rel="nofollow" target="_blank">755 Home Runs</a>
<a href="http://www.baseball-boxscores.com/" rel="nofollow" target="_blank">Baseball Box Scores</a>
<a href="http://www.baseball-fever.com/" rel="nofollow" target="_blank">Baseball Fever</a>
<a href="http://www.todayinbaseballhistory.com/" rel="nofollow" target="_blank">Today in Baseball History</a>
</div>
</div>
</div><br/><br/><br/><br/><br/>
</div>
</div>
</body>
</html>