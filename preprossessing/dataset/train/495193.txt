<!DOCTYPE html>
<!--[if IEMobile 7]><html class="iem7" lang="en" dir="ltr"><![endif]--><!--[if lte IE 6]><html class="lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]--><!--[if (IE 7)&(!IEMobile)]><html class="lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]--><!--[if IE 8]><html class="lt-ie9" lang="en" dir="ltr"><![endif]--><!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html dir="ltr" lang="en" prefix="fb: http://www.facebook.com/2008/fbml content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head>
<meta charset="utf-8"/>
<link href="http://endakenny.com.au/misc/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<meta content="Drupal 7 (http://drupal.org)" name="Generator"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="width" name="MobileOptimized"/>
<meta content="true" name="HandheldFriendly"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<title>Page not found | Enda Kenny</title>
<style>@import url("http://endakenny.com.au/modules/system/system.base.css?objbfh");
@import url("http://endakenny.com.au/modules/system/system.menus.css?objbfh");
@import url("http://endakenny.com.au/modules/system/system.messages.css?objbfh");
@import url("http://endakenny.com.au/modules/system/system.theme.css?objbfh");</style>
<style>@import url("http://endakenny.com.au/modules/comment/comment.css?objbfh");
@import url("http://endakenny.com.au/sites/all/modules/date/date_api/date.css?objbfh");
@import url("http://endakenny.com.au/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?objbfh");
@import url("http://endakenny.com.au/modules/field/theme/field.css?objbfh");
@import url("http://endakenny.com.au/modules/node/node.css?objbfh");
@import url("http://endakenny.com.au/sites/all/modules/pushtape_ui/css/pushtape.css?objbfh");
@import url("http://endakenny.com.au/modules/search/search.css?objbfh");
@import url("http://endakenny.com.au/modules/user/user.css?objbfh");
@import url("http://endakenny.com.au/sites/all/modules/views/css/views.css?objbfh");</style>
<style>@import url("http://endakenny.com.au/sites/all/modules/colorbox/styles/default/colorbox_style.css?objbfh");
@import url("http://endakenny.com.au/sites/all/modules/ctools/css/ctools.css?objbfh");
@import url("http://endakenny.com.au/sites/all/modules/lightbox2/css/lightbox.css?objbfh");
@import url("http://endakenny.com.au/sites/all/libraries/superfish/css/superfish.css?objbfh");
@import url("http://endakenny.com.au/sites/all/libraries/superfish/style/blue.css?objbfh");</style>
<style media="screen">@import url("http://endakenny.com.au/sites/all/themes/adaptivetheme/at_core/css/at.settings.style.headings.css?objbfh");
@import url("http://endakenny.com.au/sites/all/themes/adaptivetheme/at_core/css/at.settings.style.image.css?objbfh");
@import url("http://endakenny.com.au/sites/all/themes/adaptivetheme/at_core/css/at.layout.css?objbfh");</style>
<style>@import url("http://endakenny.com.au/sites/all/themes/corolla/css/html-elements.css?objbfh");
@import url("http://endakenny.com.au/sites/all/themes/corolla/css/forms.css?objbfh");
@import url("http://endakenny.com.au/sites/all/themes/corolla/css/tables.css?objbfh");
@import url("http://endakenny.com.au/sites/all/themes/corolla/css/page.css?objbfh");
@import url("http://endakenny.com.au/sites/all/themes/corolla/css/articles.css?objbfh");
@import url("http://endakenny.com.au/sites/all/themes/corolla/css/comments.css?objbfh");
@import url("http://endakenny.com.au/sites/all/themes/corolla/css/fields.css?objbfh");
@import url("http://endakenny.com.au/sites/all/themes/corolla/css/blocks.css?objbfh");
@import url("http://endakenny.com.au/sites/all/themes/corolla/css/navigation.css?objbfh");
@import url("http://endakenny.com.au/sites/all/themes/corolla/css/corolla.settings.style.css?objbfh");
@import url("http://endakenny.com.au/sites/default/files/color/corolla-31ff4d3b/colors.css?objbfh");</style>
<style media="print">@import url("http://endakenny.com.au/sites/all/themes/corolla/css/print.css?objbfh");</style>
<link href="http://endakenny.com.au/sites/default/files/adaptivetheme/corolla_files/corolla.responsive.layout.css?objbfh" media="only screen" rel="stylesheet" type="text/css"/>
<style media="screen">@import url("http://endakenny.com.au/sites/default/files/adaptivetheme/corolla_files/corolla.fonts.css?objbfh");</style>
<link href="http://endakenny.com.au/sites/all/themes/corolla/css/responsive.smartphone.portrait.css?objbfh" media="only screen and (max-width:320px)" rel="stylesheet" type="text/css"/>
<link href="http://endakenny.com.au/sites/all/themes/corolla/css/responsive.smartphone.landscape.css?objbfh" media="only screen and (min-width:321px) and (max-width:480px)" rel="stylesheet" type="text/css"/>
<link href="http://endakenny.com.au/sites/all/themes/corolla/css/responsive.tablet.portrait.css?objbfh" media="only screen and (min-width:481px) and (max-width:768px)" rel="stylesheet" type="text/css"/>
<link href="http://endakenny.com.au/sites/all/themes/corolla/css/responsive.tablet.landscape.css?objbfh" media="only screen and (min-width:769px) and (max-width:1024px)" rel="stylesheet" type="text/css"/>
<link href="http://endakenny.com.au/sites/all/themes/corolla/css/responsive.desktop.css?objbfh" media="only screen and (min-width:1025px)" rel="stylesheet" type="text/css"/>
<!--[if (lt IE 9)&(!IEMobile 7)]>
<style media="screen">@import url("http://endakenny.com.au/sites/default/files/adaptivetheme/corolla_files/corolla.lt-ie9.layout.css?objbfh");</style>
<![endif]-->
<!--[if lte IE 9]>
<style media="screen">@import url("http://endakenny.com.au/sites/all/themes/corolla/css/ie-lte-9.css?objbfh");</style>
<![endif]-->
<script src="http://endakenny.com.au/misc/jquery.js?v=1.4.4"></script>
<script src="http://endakenny.com.au/misc/jquery.once.js?v=1.2"></script>
<script src="http://endakenny.com.au/misc/drupal.js?objbfh"></script>
<script src="http://endakenny.com.au/sites/all/modules/admin_menu/admin_devel/admin_devel.js?objbfh"></script>
<script src="http://endakenny.com.au/sites/all/modules/pushtape_ui/js/pushtape.js?objbfh"></script>
<script src="http://endakenny.com.au/sites/all/modules/addthis/addthis.js?objbfh"></script>
<script src="http://endakenny.com.au/sites/all/libraries/colorbox/jquery.colorbox-min.js?objbfh"></script>
<script src="http://endakenny.com.au/sites/all/modules/colorbox/js/colorbox.js?objbfh"></script>
<script src="http://endakenny.com.au/sites/all/modules/colorbox/styles/default/colorbox_style.js?objbfh"></script>
<script src="http://endakenny.com.au/sites/all/modules/lightbox2/js/lightbox.js?1610601381"></script>
<script src="http://endakenny.com.au/sites/all/libraries/superfish/jquery.hoverIntent.minified.js?objbfh"></script>
<script src="http://endakenny.com.au/sites/all/libraries/superfish/sfsmallscreen.js?objbfh"></script>
<script src="http://endakenny.com.au/sites/all/libraries/superfish/supposition.js?objbfh"></script>
<script src="http://endakenny.com.au/sites/all/libraries/superfish/superfish.js?objbfh"></script>
<script src="http://endakenny.com.au/sites/all/libraries/superfish/supersubs.js?objbfh"></script>
<script src="http://endakenny.com.au/sites/all/modules/superfish/superfish.js?objbfh"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"corolla","theme_token":"g9Tcw2mkKK3XFPMfiiO5ZW9H9nwpJnA3RvIPjgF2apM","js":{"0":1,"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"sites\/all\/modules\/pushtape_ui\/js\/pushtape.js":1,"sites\/all\/modules\/addthis\/addthis.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/lightbox2\/js\/lightbox.js":1,"sites\/all\/libraries\/superfish\/jquery.hoverIntent.minified.js":1,"sites\/all\/libraries\/superfish\/sfsmallscreen.js":1,"sites\/all\/libraries\/superfish\/supposition.js":1,"sites\/all\/libraries\/superfish\/superfish.js":1,"sites\/all\/libraries\/superfish\/supersubs.js":1,"sites\/all\/modules\/superfish\/superfish.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/pushtape_ui\/css\/pushtape.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/lightbox2\/css\/lightbox.css":1,"sites\/all\/libraries\/superfish\/css\/superfish.css":1,"sites\/all\/libraries\/superfish\/style\/blue.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.headings.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.image.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.layout.css":1,"sites\/all\/themes\/corolla\/css\/html-elements.css":1,"sites\/all\/themes\/corolla\/css\/forms.css":1,"sites\/all\/themes\/corolla\/css\/tables.css":1,"sites\/all\/themes\/corolla\/css\/page.css":1,"sites\/all\/themes\/corolla\/css\/articles.css":1,"sites\/all\/themes\/corolla\/css\/comments.css":1,"sites\/all\/themes\/corolla\/css\/fields.css":1,"sites\/all\/themes\/corolla\/css\/blocks.css":1,"sites\/all\/themes\/corolla\/css\/navigation.css":1,"sites\/all\/themes\/corolla\/css\/fonts.css":1,"sites\/all\/themes\/corolla\/css\/corolla.settings.style.css":1,"sites\/all\/themes\/corolla\/color\/colors.css":1,"sites\/all\/themes\/corolla\/css\/print.css":1,"public:\/\/adaptivetheme\/corolla_files\/corolla.responsive.layout.css":1,"public:\/\/adaptivetheme\/corolla_files\/corolla.fonts.css":1,"sites\/all\/themes\/corolla\/css\/responsive.smartphone.portrait.css":1,"sites\/all\/themes\/corolla\/css\/responsive.smartphone.landscape.css":1,"sites\/all\/themes\/corolla\/css\/responsive.tablet.portrait.css":1,"sites\/all\/themes\/corolla\/css\/responsive.tablet.landscape.css":1,"sites\/all\/themes\/corolla\/css\/responsive.desktop.css":1,"public:\/\/adaptivetheme\/corolla_files\/corolla.lt-ie9.layout.css":1,"sites\/all\/themes\/corolla\/css\/ie-lte-9.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true},"lightbox2":{"rtl":0,"file_path":"\/(\\w\\w\/)public:\/","default_image":"\/sites\/all\/modules\/lightbox2\/images\/brokenimage.jpg","border_size":10,"font_color":"000","box_color":"fff","top_position":"","overlay_opacity":"0.8","overlay_color":"000","disable_close_click":true,"resize_sequence":0,"resize_speed":400,"fade_in_speed":400,"slide_down_speed":600,"use_alt_layout":false,"disable_resize":false,"disable_zoom":false,"force_show_nav":false,"show_caption":true,"loop_items":false,"node_link_text":"View Image Details","node_link_target":false,"image_count":"Image !current of !total","video_count":"Video !current of !total","page_count":"Page !current of !total","lite_press_x_close":"press \u003Ca href=\u0022#\u0022 onclick=\u0022hideLightbox(); return FALSE;\u0022\u003E\u003Ckbd\u003Ex\u003C\/kbd\u003E\u003C\/a\u003E to close","download_link_text":"","enable_login":false,"enable_contact":false,"keys_close":"c x 27","keys_previous":"p 37","keys_next":"n 39","keys_zoom":"z","keys_play_pause":"32","display_image_size":"original","image_node_sizes":"()","trigger_lightbox_classes":"","trigger_lightbox_group_classes":"","trigger_slideshow_classes":"","trigger_lightframe_classes":"","trigger_lightframe_group_classes":"","custom_class_handler":0,"custom_trigger_classes":"","disable_for_gallery_lists":true,"disable_for_acidfree_gallery_lists":true,"enable_acidfree_videos":true,"slideshow_interval":5000,"slideshow_automatic_start":true,"slideshow_automatic_exit":true,"show_play_pause":true,"pause_on_next_click":false,"pause_on_previous_click":true,"loop_slides":false,"iframe_width":600,"iframe_height":400,"iframe_border":1,"enable_video":false},"addthis":{"widget_url":"http:\/\/s7.addthis.com\/js\/250\/addthis_widget.js?async=1"},"superfish":{"1":{"id":"1","sf":{"animation":{"opacity":"show","height":"show"},"speed":"\u0027fast\u0027","autoArrows":true,"dropShadows":true,"disableHI":false},"plugins":{"smallscreen":{"mode":"window_width","addSelected":false,"menuClasses":false,"hyperlinkClasses":false,"title":"Main menu"},"supposition":true,"bgiframe":false,"supersubs":{"minWidth":"8","maxWidth":"12","extraWidth":1}}}}});</script>
<!--[if lt IE 9]>
<script src="http://endakenny.com.au/sites/all/themes/adaptivetheme/at_core/scripts/html5.js?objbfh"></script>
<![endif]-->
</head>
<body class="html not-front not-logged-in one-sidebar sidebar-second page-mom site-name-hidden site-name-enda-kenny section-mom color-scheme-custom corolla bs-n bb-n mb-dd rc-6 rct-6">
<div id="skip-link">
<a class="element-invisible element-focusable" href="#main-content">Skip to main content</a>
</div>
<div id="page-wrapper">
<div class="page ssc-n ssw-n ssa-l sss-n btc-n btw-b bta-l bts-n ntc-n ntw-b nta-l nts-n ctc-n ctw-b cta-l cts-n ptc-n ptw-b pta-l pts-n" id="page">
<div id="menu-bar-wrapper">
<div class="container clearfix">
<div class="nav clearfix" id="menu-bar"><nav class="block block-block menu-wrapper menu-bar-wrapper clearfix odd first block-count-1 block-region-menu-bar block-3" id="block-block-3">
<h2 class="element-invisible block-title element-invisible">Enda Kenny</h2>
<p><strong><span style="font-size: x-large;"><span style="font-size: xx-large;">Enda Kenny </span></span></strong><span style="color: #cc0000; font-size: medium;">Singer/Songwriter</span>                     </p>
</nav><nav class="block block-superfish menu-wrapper menu-bar-wrapper clearfix even last block-count-2 block-region-menu-bar block-1" id="block-superfish-1">
<h2 class="element-invisible block-title element-invisible">Main menu</h2>
<ul class="menu sf-menu sf-main-menu sf-horizontal sf-style-blue sf-total-items-7 sf-parent-items-2 sf-single-items-5" id="superfish-1"><li class="first odd sf-item-1 sf-depth-1 sf-no-children" id="menu-640-1"><a class="sf-depth-1" href="/content/about" title="Information about Enda.">About</a></li><li class="middle even sf-item-2 sf-depth-1 sf-no-children" id="menu-726-1"><a class="sf-depth-1" href="/content/gigs">Gigs</a></li><li class="middle odd sf-item-3 sf-depth-1 sf-total-children-7 sf-parent-children-0 sf-single-children-7 menuparent" id="menu-1238-1"><a class="sf-depth-1 menuparent" href="/releases" title="Albums released by Enda. Click the album name or image to view the track list and listen to samples of songs.">Music</a><ul><li class="first odd sf-item-1 sf-depth-2 sf-no-children" id="menu-1280-1"><a class="sf-depth-2" href="/content/heart-tattoo">Heart Tattoo</a></li><li class="middle even sf-item-2 sf-depth-2 sf-no-children" id="menu-1261-1"><a class="sf-depth-2" href="/content/pearler-0">Pearler</a></li><li class="middle odd sf-item-3 sf-depth-2 sf-no-children" id="menu-1262-1"><a class="sf-depth-2" href="/content/here-and-there-0">Here and There</a></li><li class="middle even sf-item-4 sf-depth-2 sf-no-children" id="menu-1263-1"><a class="sf-depth-2" href="/content/cloud-lining-0">Cloud Lining</a></li><li class="middle odd sf-item-5 sf-depth-2 sf-no-children" id="menu-1265-1"><a class="sf-depth-2" href="/content/six-one-0">Six of One</a></li><li class="middle even sf-item-6 sf-depth-2 sf-no-children" id="menu-1264-1"><a class="sf-depth-2" href="/content/bakers-dozen-0">Bakers Dozen</a></li><li class="last odd sf-item-7 sf-depth-2 sf-no-children" id="menu-1266-1"><a class="sf-depth-2" href="/content/twelve-songs-0">Twelve Songs</a></li></ul></li><li class="middle even sf-item-4 sf-depth-1 sf-total-children-3 sf-parent-children-0 sf-single-children-3 menuparent" id="menu-825-1"><a class="sf-depth-1 menuparent" href="/content/photos" title="">Photos</a><ul><li class="first odd sf-item-1 sf-depth-2 sf-no-children" id="menu-1269-1"><a class="sf-depth-2" href="/content/national-folk-festival" title="Photos taken in various years at The National Folk Festival in Canberra.">National Folk Festival</a></li><li class="middle even sf-item-2 sf-depth-2 sf-no-children" id="menu-1268-1"><a class="sf-depth-2" href="/content/snowy-mountains-folk-festival-2009">Snowy Mountains Folk Festival 2009</a></li><li class="last odd sf-item-3 sf-depth-2 sf-no-children" id="menu-1270-1"><a class="sf-depth-2" href="/content/illawarra-folk-festival" title="">Illawarra Folk Festival</a></li></ul></li><li class="middle odd sf-item-5 sf-depth-1 sf-no-children" id="menu-1234-1"><a class="sf-depth-1" href="/shop">Shop</a></li><li class="middle even sf-item-6 sf-depth-1 sf-no-children" id="menu-727-1"><a class="sf-depth-1" href="/content/media">Media</a></li><li class="last odd sf-item-7 sf-depth-1 sf-no-children" id="menu-679-1"><a class="sf-depth-1" href="/content/contact" title="Details of how to contact Enda.">Contact</a></li></ul>
</nav></div> </div>
</div>
<div id="header-wrapper">
<div class="container clearfix">
<header class="clearfix with-logo" role="banner">
<div class="branding-elements clearfix" id="branding">
<div id="logo">
<a href="/" title="Home page"><img alt="Enda Kenny" class="site-logo image-style-none" src="http://endakenny.com.au/sites/default/files/headerimage-923.jpg" typeof="foaf:Image"/></a> </div>
<hgroup class="element-invisible" id="name-and-slogan">
<h1 class="element-invisible" id="site-name"><a href="/" title="Home page">Enda Kenny</a></h1>
</hgroup>
</div>
</header>
</div>
</div>
<div id="content-wrapper">
<div class="container">
<div id="columns">
<div class="columns-inner clearfix">
<div id="content-column">
<div class="content-inner">
<section id="main-content" role="main">
<div class="content-margin">
<div class="content-style">
<header class="clearfix">
<h1 id="page-title">
                            Page not found                          </h1>
</header>
<div id="content">
<div class="block block-system no-title odd first last block-count-3 block-region-content block-main" id="block-system-main">  
  
  The requested page "/mom%09%0A" could not be found.
  </div> </div>
</div>
</div>
</section>
</div>
</div>
<div class="region region-sidebar-second sidebar"><div class="region-inner clearfix"><section class="block block-block odd first last block-count-4 block-region-sidebar-second block-2" id="block-block-2"><div class="block-inner clearfix">
<h2 class="block-title">Coming Events</h2>
<div class="block-content content"><p><span style="line-height: 1.538em;">Enda Kenny is an Irish-Born songwriter who has made his home in Melbourne, Australia since the late 1980's. His thoughtful, descriptive stories of his adoptive homeland have struck a chord with festival audiences all over Australia and are always memorable and spiced with plenty of humour. Proud to sing of where he lives rather than where he left, he continues to grow his international audience with recent trips to Germany, Holland, UK, NZ and Canada. </span></p>
<p><em><span style="line-height: 1.538em;">"On small stages to intimate audiences or large stages to festival audiences he always holds the crowd in the palm of his hand. Witty and intelligent songwriting and a</span></em><em style="line-height: 1.538em;"><span style="line-height: 1.538em;"> brilliant night's entertainment" Eleanor</span></em><em><span style="line-height: 1.538em;"> McEvoy</span></em><span style="line-height: 1.538em;"><strong>.</strong></span></p>
<p>After a decade of juggling performance with a career in Public Housing, the release of <em>Heart Tattoo</em> allowed a return to full-time music in 2013. He has played live shows in eight countries since then and continues to grow his reputation as a cracking solo entertainer through extensive touring in the northern summers.</p>
<p><em>"The finest Irish songwriter living overseas"  Colum Sands</em></p>
<p><em></em><span style="font-size: 13.008px; line-height: 1.538em;">  </span></p>
<p><span style="font-size: 13.008px; line-height: 1.538em;">Check the gigs page for full details.  ALL SHOWS IN NZ AND WA CANCELLED DUE TO CORONAVIRUS - AWAITING UPDATE FOR JULY &amp; AUGUST UK TOUR 2020</span></p>
<p><span style="font-size: 13.008px; line-height: 1.538em;"><br/></span></p>
<p><span style="line-height: 1.538em;"><br/></span></p>
<p><span style="line-height: 1.538em;"><br/></span></p>
<p>To Join Enda's mailing list, email enda(at)endakenny.com.au</p>
<p><span style="line-height: 1.538em;">see </span><a href="http://www.endakenny.com.au/content/gigs" style="line-height: 1.538em;">gigs</a><span style="line-height: 1.538em;"> page for details</span></p>
</div>
</div></section></div></div>
</div>
</div>
</div>
</div>
<div id="footer-panels-wrapper">
<div class="container clearfix">
<!-- Four column Gpanel -->
<div class="at-panel gpanel panel-display four-4x25 clearfix">
<div class="panel-row row-1 clearfix">
<div class="region region-four-second"><div class="region-inner clearfix"><div class="block block-block no-title odd first last block-count-5 block-region-four-second block-4" id="block-block-4"><div class="block-inner clearfix">
<div class="block-content content"><p><span style="color: #cc3333;">All Content Copyright Enda Kenny 2013</span></p>
</div>
</div></div></div></div> </div>
<div class="panel-row row-2 clearfix">
<div class="region region-four-third"><div class="region-inner clearfix"><div class="block block-block no-title odd first last block-count-6 block-region-four-third block-8" id="block-block-8"><div class="block-inner clearfix">
<div class="block-content content"><p>Site Design by D<a href="mailto:don@bogong-tech.com.au">on Williams.</a></p>
</div>
</div></div></div></div> </div>
</div>
</div>
</div>
</div>
</div>
<script>var addthis_config = {"services_compact":"more","data_track_clickback":false,"ui_508_compliant":false,"ui_click":false,"ui_cobrand":"","ui_delay":0,"ui_header_background":"","ui_header_color":"","ui_open_windows":false,"ui_use_css":true,"ui_use_addressbook":false,"ui_language":"en"}
var addthis_share = {"templates":{"twitter":"{{title}} {{url}} via @AddThis"}}</script>
</body>
</html>
