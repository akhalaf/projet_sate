<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="ca">
<![endif]--><!--[if IE 7]>
<html id="ie7" lang="ca">
<![endif]--><!--[if IE 8]>
<html id="ie8" lang="ca">
<![endif]--><!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!--><html lang="ca">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="https://cetebal.com/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
	<script src="https://cetebal.com/wp-content/themes/Divi/js/html5.js" type="text/javascript"></script>
	<![endif]-->
<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>
<script>var et_site_url='https://cetebal.com';var et_post_id='global';function et_core_page_resource_fallback(a,b){"undefined"===typeof b&&(b=a.sheet.cssRules&&0===a.sheet.cssRules.length);b&&(a.onerror=null,a.onload=null,a.href?a.href=et_site_url+"/?et_core_page_resource="+a.id+et_post_id:a.src&&(a.src=et_site_url+"/?et_core_page_resource="+a.id+et_post_id))}
</script><title>404 Not Found | Centre Tecnològic de la Fusta</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://cetebal.com/ca/feed/" rel="alternate" title="Centre Tecnològic de la Fusta » canal d'informació" type="application/rss+xml"/>
<link href="https://cetebal.com/ca/comments/feed/" rel="alternate" title="Centre Tecnològic de la Fusta » Canal dels comentaris" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/cetebal.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.16"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<meta content="Cetebal v.1.0.0" name="generator"/><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://cetebal.com/wp-content/plugins/cookie-notice/css/front.min.css?ver=4.9.16" id="cookie-notice-front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//cetebal.com/wp-content/plugins/sitepress-multilingual-cms/templates/language-switchers/menu-item/style.css?ver=1" id="wpml-menu-item-0-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,latin-ext" id="divi-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cetebal.com/wp-content/themes/Cetebal/style.css?ver=3.0.97" id="divi-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cetebal.com/wp-includes/css/dashicons.min.css?ver=4.9.16" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://cetebal.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://cetebal.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var cnArgs = {"ajaxurl":"https:\/\/cetebal.com\/wp-admin\/admin-ajax.php","hideEffect":"fade","onScroll":"","onScrollOffset":"100","cookieName":"cookie_notice_accepted","cookieValue":"TRUE","cookieTime":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"","cache":""};
/* ]]> */
</script>
<script src="https://cetebal.com/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.2.42" type="text/javascript"></script>
<link href="https://cetebal.com/ca/wp-json/" rel="https://api.w.org/"/>
<link href="https://cetebal.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://cetebal.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.16" name="generator"/>
<meta content="WPML ver:3.9.4 stt:8,2;" name="generator"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/><link href="http://cetebal.com/wp-content/uploads/2018/01/logo.png" rel="shortcut icon"/><style id="custom-background-css" type="text/css">
body.custom-background { background-color: #ffffff; }
</style>
<link href="https://cetebal.com/wp-content/cache/et/global/et-divi-customizer-global-16101335576098.min.css" id="et-divi-customizer-global-cached-inline-styles" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" rel="stylesheet"/></head>
<body class="error404 custom-background et_pb_button_helper_class et_fullwidth_nav et_fullwidth_secondary_nav et_fixed_nav et_show_nav et_hide_fixed_logo et_cover_background et_secondary_nav_enabled et_secondary_nav_two_panels et_pb_gutter et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_right_sidebar et_divi_theme et_minified_js et_minified_css">
<div id="page-container">
<div id="top-header">
<div class="container clearfix">
<div id="et-info">
<span id="et-info-phone">971 559 696</span>
<a href="mailto:info@cetebal.com"><span id="et-info-email">info@cetebal.com</span></a>
<ul class="et-social-icons">
<li class="et-social-icon et-social-facebook">
<a class="icon" href="https://www.facebook.com/CETEBAL-CENTRE-TECNOLOGIC-BALEAR-DE-LA-FUSTA-300359148913/">
<span>Facebook</span>
</a>
</li>
<li class="et-social-icon et-social-twitter">
<a class="icon" href="https://twitter.com/FFSCETEBAL">
<span>Twitter</span>
</a>
</li>
</ul> </div> <!-- #et-info -->
<div id="et-secondary-menu">
<div class="et_duplicate_social_icons">
<ul class="et-social-icons">
<li class="et-social-icon et-social-facebook">
<a class="icon" href="https://www.facebook.com/CETEBAL-CENTRE-TECNOLOGIC-BALEAR-DE-LA-FUSTA-300359148913/">
<span>Facebook</span>
</a>
</li>
<li class="et-social-icon et-social-twitter">
<a class="icon" href="https://twitter.com/FFSCETEBAL">
<span>Twitter</span>
</a>
</li>
</ul>
</div><ul class="menu" id="et-secondary-nav"><li class="menu-item wpml-ls-slot-4 wpml-ls-item wpml-ls-item-ca wpml-ls-current-language wpml-ls-menu-item wpml-ls-first-item menu-item-type-wpml_ls_menu_item menu-item-object-wpml_ls_menu_item menu-item-wpml-ls-4-ca"><a href="http://cetebal.com/ca/" title="Català"><span class="wpml-ls-native">Català</span></a></li>
<li class="menu-item wpml-ls-slot-4 wpml-ls-item wpml-ls-item-es wpml-ls-menu-item wpml-ls-last-item menu-item-type-wpml_ls_menu_item menu-item-object-wpml_ls_menu_item menu-item-wpml-ls-4-es"><a href="http://cetebal.com/es/" title="Español"><span class="wpml-ls-native">Español</span></a></li>
</ul> </div> <!-- #et-secondary-menu -->
</div> <!-- .container -->
</div> <!-- #top-header -->
<header data-height-onload="101" id="main-header">
<div class="container clearfix et_menu_container">
<div class="logo_container">
<span class="logo_helper"></span>
<a href="https://cetebal.com/ca/">
<img alt="Centre Tecnològic de la Fusta" data-height-percentage="60" id="logo" src="http://cetebal.com/wp-content/uploads/2018/01/logo.png"/>
</a>
</div>
<div data-fixed-height="30" data-height="101" id="et-top-navigation">
<nav id="top-menu-nav">
<ul class="nav" id="top-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-202" id="menu-item-202"><a href="https://cetebal.com/ca/">Inici</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-55" id="menu-item-55"><a href="https://cetebal.com/ca/qui-som/">Qui som</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-220" id="menu-item-220"><a href="https://cetebal.com/ca/qui-som/">Qui som</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-118" id="menu-item-118"><a href="https://cetebal.com/ca/qui-som/arees/">Àrees</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-150" id="menu-item-150"><a href="https://cetebal.com/ca/socis/">Socis</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-165" id="menu-item-165"><a href="https://cetebal.com/ca/infraestructura/">Infraestructura</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-177" id="menu-item-177"><a href="https://cetebal.com/ca/la-fusta/">La fusta</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-191" id="menu-item-191"><a href="https://cetebal.com/ca/noticies/">Notícies</a></li>
</ul> </nav>
<div id="et_mobile_nav_menu">
<div class="mobile_nav closed">
<span class="select_page">Select Page</span>
<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
</div>
</div> </div> <!-- #et-top-navigation -->
</div> <!-- .container -->
<div class="et_search_outer">
<div class="container et_search_form_container">
<form action="https://cetebal.com/ca/" class="et-search-form" method="get" role="search">
<input class="et-search-field" name="s" placeholder="Search …" title="Search for:" type="search" value=""/> </form>
<span class="et_close_search_field"></span>
</div>
</div>
</header> <!-- #main-header -->
<div id="et-main-area">
<div id="main-content">
<div class="container">
<div class="clearfix" id="content-area">
<div id="left-area">
<article class="et_pb_post not_found post-692 post type-post status-publish format-standard has-post-thumbnail hentry category-uncategorized-ca" id="post-0">
<div class="entry">
<!--If no results are found-->
<h1>No Results Found</h1>
<p>The page you requested could not be found. Try refining your search, or use the navigation above to locate the post.</p>
</div>
<!--End if no results are found--> </article> <!-- .et_pb_post -->
</div> <!-- #left-area -->
<div id="sidebar">
<div class="et_pb_widget widget_recent_entries" id="recent-posts-2"> <h4 class="widgettitle">Recent Posts</h4> <ul>
<li>
<a href="https://cetebal.com/ca/grup-orell-empresa-asociada-a-cetebal-nos-presenta-uno-de-sus-ultimos-proyectos/">Grup Orell, empresa asociada a CETEBAL, nos presenta uno de sus últimos proyectos</a>
</li>
<li>
<a href="https://cetebal.com/ca/direccio-general-de-politica-industrial-sapropa-al-sector-de-la-fusta-i-moble-i-cetebal/">Direcció General de Política Industrial s’apropa al sector de la fusta i moble i CETEBAL</a>
</li>
<li>
<a href="https://cetebal.com/ca/cetebal-participa-del-curs-de-cluster-manager/">CETEBAL participa del curs de cluster manager</a>
</li>
<li>
<a href="https://cetebal.com/ca/realitzats-tallers-i-webinars-per-aprofundir-en-el-llenguatge-dels-arquitectes/">Realitzats tallers i webinars per aprofundir en el llenguatge dels arquitectes</a>
</li>
<li>
<a href="https://cetebal.com/ca/miquel-duran-fusteria-realitza-mobiliari-al-museu-dhistoria-de-manacor/">Miquel Duran Fusteria realitza mobiliari al Museu d’Història de Manacor</a>
</li>
</ul>
</div> <!-- end .et_pb_widget --> </div> <!-- end #sidebar -->
</div> <!-- #content-area -->
</div> <!-- .container -->
</div> <!-- #main-content -->
<span class="et_pb_scroll_top et-pb-icon"></span>
<footer id="main-footer">
<div class="container">
<div class="clearfix" id="footer-widgets">
<div class="footer-widget"><div class="fwidget et_pb_widget widget_media_image" id="media_image-3"><img alt="" class="image wp-image-40 attachment-full size-full" height="84" src="https://cetebal.com/wp-content/uploads/2018/01/logo.png" style="max-width: 100%; height: auto;" width="178"/></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div class="fwidget et_pb_widget widget_text" id="text-2"> <div class="textwidget"><h3><strong>CETEBAL</strong></h3>
<p>Centre Tecnològic Balear de la Fusta G57006116</p>
<p>Carrer Forners 80<br/>
Polígon Industrial de Manacor<br/>
07500, Manacor<br/>
Illes Balears</p>
<p>971 559 696<br/>
<a href="mailto:info@cetebal.com" rel="noopener" target="_blank">info@cetebal.com</a></p>
<p> </p>
</div>
</div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div class="fwidget et_pb_widget widget_nav_menu" id="nav_menu-4"><h4 class="title">Menú</h4><div class="menu-peu-container"><ul class="menu" id="menu-peu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-243" id="menu-item-243"><a href="https://cetebal.com/ca/qui-som/">Qui som</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-242" id="menu-item-242"><a href="https://cetebal.com/ca/socis/">Socis</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-241" id="menu-item-241"><a href="https://cetebal.com/ca/infraestructura/">Infraestructura</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-240" id="menu-item-240"><a href="https://cetebal.com/ca/la-fusta/">La fusta</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-239" id="menu-item-239"><a href="https://cetebal.com/ca/noticies/">Notícies</a></li>
</ul></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget last"><div class="fwidget et_pb_widget widget_nav_menu" id="nav_menu-6"><h4 class="title">Legal</h4><div class="menu-legal-container"><ul class="menu" id="menu-legal"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-386" id="menu-item-386"><a href="https://cetebal.com/ca/avis-legal/">Avís legal</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-375" id="menu-item-375"><a href="https://cetebal.com/ca/politica-de-privacidad-informacion-detallada-sobre-proteccion-de-datos/">Política de privacidad</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-376" id="menu-item-376"><a href="https://cetebal.com/ca/politica-de-cookies/">Política de cookies</a></li>
</ul></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --> </div> <!-- #footer-widgets -->
</div> <!-- .container -->
<div id="footer-bottom">
<div class="container clearfix">
<ul class="et-social-icons">
<li class="et-social-icon et-social-facebook">
<a class="icon" href="https://www.facebook.com/CETEBAL-CENTRE-TECNOLOGIC-BALEAR-DE-LA-FUSTA-300359148913/">
<span>Facebook</span>
</a>
</li>
<li class="et-social-icon et-social-twitter">
<a class="icon" href="https://twitter.com/FFSCETEBAL">
<span>Twitter</span>
</a>
</li>
</ul><div id="footer-info">Copyright © CETEBAL 2018 - <a href="http://cetebal.com/ca/avis-legal/" target="_blank">Avís legal</a></div> </div> <!-- .container -->
</div>
</footer> <!-- #main-footer -->
</div> <!-- #et-main-area -->
</div> <!-- #page-container -->
<script type="text/javascript">function showhide_toggle(e,t,r,g){var a=jQuery("#"+e+"-link-"+t),s=jQuery("a",a),i=jQuery("#"+e+"-content-"+t),l=jQuery("#"+e+"-toggle-"+t);a.toggleClass("sh-show sh-hide"),i.toggleClass("sh-show sh-hide").toggle(),"true"===s.attr("aria-expanded")?s.attr("aria-expanded","false"):s.attr("aria-expanded","true"),l.text()===r?(l.text(g),a.trigger("sh-link:more")):(l.text(r),a.trigger("sh-link:less")),a.trigger("sh-link:toggle")}</script>
<script type="text/javascript">
  if ( undefined !== window.jQuery ) {
    jQuery('#et_pb_contact_legal_3_0').siblings('label').html('<i></i> Acepto l\'<a href="http://cetebal.com/ca/avis-legal/">Avís legal</a> i la <a href="http://cetebal.com/ca/politica-de-privacidad-informacion-detallada-sobre-proteccion-de-datos/">Política de privacitat</a>');
  }
</script>
<script type="text/javascript">
		var et_animation_data = [];
	</script>
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,regular,700&amp;subset=latin,latin-ext" id="et-builder-googlefonts-css" media="all" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
/* <![CDATA[ */
var et_shortcodes_strings = {"previous":"Previous","next":"Next"};
var et_pb_custom = {"ajaxurl":"https:\/\/cetebal.com\/wp-admin\/admin-ajax.php","images_uri":"https:\/\/cetebal.com\/wp-content\/themes\/Divi\/images","builder_images_uri":"https:\/\/cetebal.com\/wp-content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"eb1e0c6888","subscription_failed":"Please, check the fields below to make sure you entered the correct information.","et_ab_log_nonce":"e3850e0421","fill_message":"Please, fill in the following fields:","contact_error_message":"Please, fix the following errors:","invalid":"Invalid email","captcha":"Captcha","prev":"Prev","previous":"Previous","next":"Next","wrong_captcha":"You entered the wrong number in captcha.","is_builder_plugin_used":"","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","is_ab_testing_active":"","page_id":"692","unique_test_id":"","ab_bounce_rate":"5","is_cache_plugin_active":"no","is_shortcode_tracking":""};
var et_pb_box_shadow_elements = [];
/* ]]> */
</script>
<script src="https://cetebal.com/wp-content/themes/Divi/js/custom.min.js?ver=3.0.97" type="text/javascript"></script>
<script src="https://cetebal.com/wp-content/themes/Divi/core/admin/js/common.js?ver=3.0.97" type="text/javascript"></script>
<script src="https://cetebal.com/wp-includes/js/wp-embed.min.js?ver=4.9.16" type="text/javascript"></script>
<div class="cn-bottom bootstrap" id="cookie-notice" role="banner" style="color: #fff; background-color: #000;"><div class="cookie-notice-container"><span id="cn-notice-text">Utilizamos cookies para asegurar que damos la mejor experiencia al usuario en nuestro sitio web. Si continúa utilizando este sitio asumiremos que está de acuerdo.</span><a class="cn-set-cookie button bootstrap" data-cookie-set="accept" href="#" id="cn-accept-cookie">Estoy de acuerdo</a>
</div>
</div></body>
</html>