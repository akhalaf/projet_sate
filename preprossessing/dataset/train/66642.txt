<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "60725",
      cRay: "610b8a59cd7a24b2",
      cHash: "323fb5aadf84093",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYWNjb3VudGluZ3dlYi5jb20vdGVjaG5vbG9neS9hY2NvdW50aW5nLXNvZnR3YXJlL2Nsb3VkLWNvbXB1dGluZy12ZXJzdXMtc29mdHdhcmUtYXMtYS1zZXJ2aWNl",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "PNlkSxhC9XoT3C0bDVWExf9NpKO/x2ROY7tKN32O8DKS/+Z1P5yQNY9AL04IzeTEMfvtHl13bvPByKLtXiLtlHk4UlSutqWDEAeZoQ3gkv0z2qJMC+u3+NwTp0pu8iGHiGRiR7ssfQixtix/b9TJHgTGMX0Ks83JHJjTgpLERjv8vgWDA5YeflRH4yqFodWXIeVyNOjkXbbbKVGYuFLyDESnaDLnvK04jWfepjtF6NB+LlABYuycXImghIJ2MQUG/pXSMgDJkz4axXk0PABXCpre+dUjiZkHM2paYsquxx1KHKCy8qZigw3btj5GDDfcxdZ0GfNf9a0Tk1xUeBAMlKTj2jzDoBI2GN1b2ITV7Kr+218qg6sjXLgqb4rV1mufRnxHM8SF1l9iL0i4jl0Ws1DzSxdi8FcxxnHM28Fl3qWIDqSAm88BF57q8Sm6KCLfQpG+gXLkLXq9ZXmTwVz5HKO7ZevEW9YcXgfBP0bHPeSO2ZnTLw5Sj9z9OCRo+hsrtEJpt+IDQHDVPLJCvCVqLxLDtMkz2ceLkeEe9vb4wLKG6662a5XN8Vf1P1Mp+6MUUxWrOoNHpQJp00jS2uFwHJ6PlltJ6N4ubJBuCrk2zPWleaEvWG9q/f5CeALrP8tITzeP8zSjC198ZTvnuJXcoD6/GZZzNp9Af8seMnEX5h98YVzD1fBgfmglbWc7Jfsjobg2llAeMZbFbSCVnmDtcPCBp2qGSTVdWlWYLSuU5VJX5mlw9R4rD+3Vh2MQz3rE",
        t: "MTYxMDUwMzQwMi41MjcwMDA=",
        m: "eyH3ZZFIHcdfCJBQKE5DrEZVO/qHXdC3unC5Vin1ZAw=",
        i1: "h9UxXzSlP1lEEZQz34WZtw==",
        i2: "3yLbecScAiBSAzRZJAPCwg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "2/G0MtUGs/8oFgw8Tyf53XR3mFTohv1CigkIAcXcvYk=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610b8a59cd7a24b2");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> accountingweb.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/technology/accounting-software/cloud-computing-versus-software-as-a-service?__cf_chl_jschl_tk__=31f0c810bd7661cbf4297993f7c6a40416d61628-1610503402-0-AfuZJdljZbsV_wjj5tNf1g4uqK8XtwZl0t-ht8kfa81kQ4PnOaq_EdCGF6nf8MHYy70AdP2uDQq1j0yhxMbFt1394_lKl3286yqZEX4W26XytX6GkzbGlCC80ta1-BKXRffHnnBVwwuAlux-xQeS5S3ZmI1NsAvjweY_R-C5OaAeouaA-knT9Fj_d8Iad_5S9LkbkAzDMprgPozhJQt5WxZpuBg6KM-uhcwaLyvggrUPL3_SyVBIHBvISf0Z4jx3c2HLcsNMlb9js88y3t-xO0sR01eJKqU6gPMfY15ygtiesKTkjVCULDmhbxdH6G0kL8nOjJkULZ65N8jTg4UiRlnsgW9ZAat4AvtdXYbGWrqO175uj_le_Stz5EFjfy3IGSbvXnCOtUAG8_-PxzboOFU" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="188bef03413c12bb1e691fef0e423201a0afe363-1610503402-0-AVRiA0qJydAYCnLPodlCk5xouPrxkO9hroSxf4wJX/0JcFu85HtR7BU+18c/0Qf5FR111MkOTngklzCjwu9dpDe1D9XP+4U0XlqV04qoX6py1sfejB6qV7CyJ6ZpUW/741nGhPs336OMZraZ0b6pW/xtkJ5Say0sjP351ojjt0P5eJ8OsDH3n+Cf39STT4hyZYma3N6Ghtn1hl4yV0zgZ5rIUZbQhLajyTO++1HIs0Uluqmx1AzAnuv0wMhuAZb6NVVccMomkgrVZlm9WBEg8ArSih9+A1hZBV/JX1yMbVzozGtmyfPZC5AvEovO4OX26gMxKn3YHuFwjd5bMUACj1rmsxRumV/6bFY1t2B6Ir+sjEVaZaQhOXJIApgiSuyXnUmHnahQeBgNDPj93dCMpgE4MPeZEn5WGnmbjb7kC42Fez0527nVlYQZK6g//KzftKN0yFe3zI/d/JOTIKp1lyRK6xbBP0JFrGcfRGoCrmPhVBqj9AiAMy0pkH4QMC001KZ3AkhrBwgwinqLMu0QA6SaUV3ltZKimGcg46W2XMnGTzBhI8cRvCqCNePIc4omDXGr91gWW0t2nw8k9n1KbP3f7TQl9rpaGmgS/buiVtoHQ0Bo1TeF4ex632kByj3730xGtsjSEOwTwP58H9rRjofHmIrDXlozTMQ33JTQ77LO4jZs+iX1YyifdsZGwIdvEHofaPmi/PZ9gzw0TRxKUZ3nwWkz+0GfMkZg8CCQp/mqcTOZt2knwn4/uEarh9Mm4dsejUNz6/SpOcBAew3/yNsNdgJRFbhDmgseSOSqG+kxsR1KMbTkuVsLC/bapVF4e5+ZuFybuQTGu8f/yrdsEEkuODGAaeLX1YYHpM/vbMoUpy/U4d405lO3B4WykeHPBJ1+JKM006WbiKIwePIWZbwoZ2lqlnc3aMywTky6gO9ZpmKTVQv/8u88lwXmrWjbVjCwE/ncR4SSqxvb0j84cxQecxOHbPU/PlpYqQ31jPJI7DJQT9ZwgwL0ShkpRlH7JR+zSwJ/h0/7Myfls7KHYXkjPKJ/rnTJUJ2DJz/3jq9TChg4q7JW5iWr78I4AIqEN47LQONEJu31puX0bPv3SvVIrKljWezoOfvlQ4CwcepQYiazWEeo74KhCDONyOOnrs2LxqMoY/g1w7TVzHU4pM+IgQD8hu+sWJCd2PHVc3DBSXs8U9GH2vDK+NVVuQ6EiIlet5+tYfeF7MGhS6Y5PFsrKBc9t8qw1NgchOayAerXxxxsWF0661DolqKCYw0Kp1qHi+yJYA91oW4aN049LXECt//uTWZeLJEnoN9ziD80L9YXvPcicty2eI2FoF6y8A=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="ed40798a19c0eead27d07ee6a9abdc76"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610503406.527-7tJ6IM7Y1g"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610b8a59cd7a24b2')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610b8a59cd7a24b2</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
