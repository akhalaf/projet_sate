<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>:: Badr Airlines ::</title>
<!-- Web Fonts from Google  -->
<link href="https://fonts.googleapis.com/css?family=Hind:300,400,500,600,700&amp;subset=devanagari,latin-ext" rel="stylesheet"/>
<!-- Bootstrap -->
<link href="../css/bootstrap.min.css" rel="stylesheet"/>
<!-- Custom CSS -->
<link href="../css/custom.css" rel="stylesheet"/>
<link href="../css/arabic_font.css" rel="stylesheet"/>
<link href="../css/animate.min.css" rel="stylesheet"/>
<!-- Datepicker -->
<link href="../css/datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="../js/datepicker.min.js"></script>
<script src="../js/datepicker.en.js"></script>
<script src="../scripts/multiplecity.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top shadow">
<div class="container">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<button aria-controls="navbar" aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="index.php" id="brand" target="_top"><img id="logo" src="../images/logo.png"/></a>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="navbar">
<ul class="nav navbar-nav">
<li class="active"><a href="index.php" target="_top"><span class="glyphicon glyphicon-home"></span><span class="sr-only">(current)</span></a></li>
<!-- <li ><a target="_top"  href="http://fo-emea.ttinteractive.com/Zenith/FrontOffice/(S(vnboots5g0o0xeexl0bityyw))/badrairlines/en-GB?__cnv=TUYDP">Book</a></li> -->
<li><a href="https://fo-emea.ttinteractive.com/Zenith/FrontOffice/badrairlines/en-Gb/Home/FindBooking" target="_top">Manage Booking</a></li>
<!--
            <li class="dropdown mega-dropdown" >
              <a target="_top"  href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Book<span class="caret"></span></a>
                <ul class="dropdown-menu mega-dropdown-menu">
                    <h2>Book a flight</h2>
                    <iframe class="iframebook" src="http://fo-emea.ttinteractive.com/zenith/frontoffice/badrairlines/en-GB/?mode=iframe" ></iframe>
                </ul>
            </li>
            -->
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" target="_top">Travel<span class="caret"></span></a>
<ul class="dropdown-menu">
<!-- <li ><a target="_top"  href="https://fo-emea.ttinteractive.com/Zenith/FrontOffice/badrairlines/en-Gb/Home/FindBooking">Manage Booking</a></li>
                <li role="separator" class="divider"></li> -->
<li><a href="offers.php" target="_top">Offers</a></li>
<li class="divider" role="separator"></li>
<li><a href="group-book.php" target="_top">Group Book</a></li>
<li class="divider" role="separator"></li>
<li><a href="unaccompanied-minor.php" target="_top">Unaccompanied Minor</a></li>
<li class="divider" role="separator"></li>
<li><a href="timetable.php" target="_top">Timetable</a></li>
<li class="divider" role="separator"></li>
<li><a href="terms-and-conditions.php" target="_top">FAQ &amp; Terms and Conditions</a></li>
</ul>
</li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" target="_top">Destinations<span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="local-destinations.php" target="_top">Local Destinations</a></li>
<li class="divider" role="separator"></li>
<li><a href="global-destinations.php" target="_top">Global Destinations</a></li>
<li class="divider" role="separator"></li>
<li><a href="future-destinations.php" target="_top">Future Destinations</a></li>
</ul>
</li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" target="_top">Services<span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="charter-flight.php" target="_top">Charter Flights</a></li>
<li class="divider" role="separator"></li>
<li><a href="cargo.php" target="_top">Cargo Services</a></li>
<li class="divider" role="separator"></li>
<li class="dropdown"><a href="#" tabindex="-1" target="_top">Medical &amp; Special Needs <i class="glyphicon glyphicon-triangle-right"></i></a>
<ul class="dropdown-menu dropdown-submenu">
<li><a href="request-for-special.php" tabindex="-1" target="_top">Request for special</a></li>
<li class="divider" role="separator"></li>
<li><a href="wheel-chair.php" target="_top">Wheel Chair</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="about.php" target="_top">About us</a></li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" target="_top">Contact Us<span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="office-contacts.php" target="_top">Office Contacts</a></li>
<li class="divider" role="separator"></li>
<li class="dropdown"><a href="#" tabindex="-1" target="_top">Help <i class="glyphicon glyphicon-triangle-right"></i></a>
<ul class="dropdown-menu dropdown-submenu">
<li><a href="general-help.php" tabindex="-1" target="_top">General help</a></li>
<li class="divider" role="separator"></li>
<li><a href="refund-request.php" target="_top">Request Refund</a></li>
<li><a href="refund-request-SDG.php" target="_top">Request Refund in SDG</a></li>
</ul>
</li>
</ul>
</li>
</ul>
<ul class="nav navbar-nav navbar-right">
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" target="_top"><span class="glyphicon glyphicon-user"></span> Login <span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="https://fo-emea.ttinteractive.com/Zenith/FrontOffice/badrairlines/en-GB/Customer/Login" target="_blank">Customer</a></li>
<li class="divider" role="separator"></li>
<li><a href="https://emea.ttinteractive.com/otds/index.asp" target="_blank">Travel Agency</a></li>
<li class="divider" role="separator"></li>
<li><a href="https://fo-emea.ttinteractive.com/Zenith/FrontOffice/badrairlines/en-GB/Customer/Create" target="_blank">Register</a></li>
</ul>
</li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" target="_top"><span class="glyphicon glyphicon-globe"></span></a>
<ul class="dropdown-menu">
<li class="active"><a href="" id="eng" target="_top">English</a></li>
<li class="divider" role="separator"></li>
<li><a href="" id="arb" target="_top">العربية</a></li>
</ul>
</li>
</ul>
</div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>
</body>
</html>
