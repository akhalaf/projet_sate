<!DOCTYPE HTML>
<!--
	Twenty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
--><html>
<head>
<title>Yanic Guerin Architecte</title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport"/>
<meta content="Bienvenue sur le Site de Yanic Guerin Architecte DPLG Ã  la Chataigneraie (85120)" name="description"/>
<meta content="Architecte,VendÃ©e,Charente-maritime,Deux-sÃ¨vres,Yanic,Yannick,Yanick,Guerin,GuÃ©rin" name="keywords"/>
<meta content="Wwe-ZYevczKQt4_oXI0AeTufXUOhnjLsi4msuJK54Ts" name="google-site-verification"/>
<link href="assets/css/main.css" rel="stylesheet"/>
<noscript><link href="assets/css/noscript.css" rel="stylesheet"/></noscript>
</head>
<body class="index is-preload">
<div id="page-wrapper">
<!-- Header -->
<header class="alt" id="header">
<h1 id="logo"><a href="index.html">Yanic Guerin <span>Architecte D.P.L.G</span></a></h1>
<nav id="nav">
<ul>
<li class="current"><a href="index.html">Accueil</a></li>
<li><a href="realisations.html">RÃ©alisations</a></li>
<li><a href="contact.html">Contact</a></li>
</ul>
</nav>
</header>
<!-- Banner -->
<section id="banner">
<div class="inner">
<!-- Two -->
<section class="wrapper style1 container special">
<div class="row">
<div class="col-6 col-12-narrower">
<img src="images/accueil.jpg" width="100%"/>
</div>
<div class="col-6 col-12-narrower">
<br/><br/>
<p>Architecte D.P.L.G Ã  la ChÃ¢taigneraie (85120)</p><br/><br/>
<p>Pour tous vos projets de construction 
									ou de rÃ©novation en vendÃ©e et dÃ©partements limitrofs.</p>
</div>
</div>
</section>
</div>
</section>
<div class="bg">
						Yanic Guerin Architecte - 11 rue du Commerce - 85120 La ChÃ¢taigneraie<br/>
					02 51 52 78 65- Â© 2019 Guerin Yanic Architecte
</div>
<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.dropotron.min.js"></script>
<script src="assets/js/jquery.scrolly.min.js"></script>
<script src="assets/js/jquery.scrollex.min.js"></script>
<script src="assets/js/browser.min.js"></script>
<script src="assets/js/breakpoints.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>
</div></body>
</html>