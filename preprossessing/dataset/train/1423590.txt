<!DOCTYPE html>
<html class="no-js" lang="es-CO">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport"/><title>No se encontró la página – Temporizar</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://temporizar.com/feed/" rel="alternate" title="Temporizar » Feed" type="application/rss+xml"/>
<link href="https://temporizar.com/comments/feed/" rel="alternate" title="Temporizar » RSS de los comentarios" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/temporizar.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://temporizar.com/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://temporizar.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.9" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://temporizar.com/wp-content/plugins/login-and-logout-redirect/public/css/login-and-logout-redirect-public.css?ver=2.0.0" id="login-and-logout-redirect-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://temporizar.com/wp-content/plugins/salient-social/css/style.css?ver=1.1" id="salient-social-css" media="all" rel="stylesheet" type="text/css"/>
<style id="salient-social-inline-css" type="text/css">

  .sharing-default-minimal .nectar-love.loved,
  body .nectar-social[data-color-override="override"].fixed > a:before, 
  body .nectar-social[data-color-override="override"].fixed .nectar-social-inner a,
  .sharing-default-minimal .nectar-social[data-color-override="override"] .nectar-social-inner a:hover {
    background-color: #be1622;
  }
  .nectar-social.hover .nectar-love.loved,
  .nectar-social.hover > .nectar-love-button a:hover,
  .nectar-social[data-color-override="override"].hover > div a:hover,
  #single-below-header .nectar-social[data-color-override="override"].hover > div a:hover,
  .nectar-social[data-color-override="override"].hover .share-btn:hover,
  .sharing-default-minimal .nectar-social[data-color-override="override"] .nectar-social-inner a {
    border-color: #be1622;
  }
  #single-below-header .nectar-social.hover .nectar-love.loved i,
  #single-below-header .nectar-social.hover[data-color-override="override"] a:hover,
  #single-below-header .nectar-social.hover[data-color-override="override"] a:hover i,
  #single-below-header .nectar-social.hover .nectar-love-button a:hover i,
  .nectar-love:hover i,
  .hover .nectar-love:hover .total_loves,
  .nectar-love.loved i,
  .nectar-social.hover .nectar-love.loved .total_loves,
  .nectar-social.hover .share-btn:hover, 
  .nectar-social[data-color-override="override"].hover .nectar-social-inner a:hover,
  .nectar-social[data-color-override="override"].hover > div:hover span,
  .sharing-default-minimal .nectar-social[data-color-override="override"] .nectar-social-inner a:not(:hover) i,
  .sharing-default-minimal .nectar-social[data-color-override="override"] .nectar-social-inner a:not(:hover) {
    color: #be1622;
  }
</style>
<link href="https://temporizar.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/font-awesome.min.css?ver=4.7.0" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://temporizar.com/wp-content/themes/salient/css/grid-system.css?ver=12.0" id="salient-grid-system-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://temporizar.com/wp-content/themes/salient/css/style.css?ver=12.0" id="main-styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://temporizar.com/wp-content/themes/salient/css/plugins/jquery.fancybox.css?ver=3.3.1" id="fancyBox-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700&amp;subset=latin%2Clatin-ext" id="nectar_default_font_open_sans-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://temporizar.com/wp-content/themes/salient/css/responsive.css?ver=12.0" id="responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://temporizar.com/wp-content/themes/salient/css/skin-material.css?ver=12.0" id="skin-material-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://temporizar.com/wp-content/themes/salient/css/elements/widget-nectar-posts.css?ver=12.0" id="nectar-widget-posts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://temporizar.com/wp-content/themes/salient/css/salient-dynamic-styles.css?ver=19488" id="dynamic-css-css" media="all" rel="stylesheet" type="text/css"/>
<style id="dynamic-css-inline-css" type="text/css">
@media only screen and (min-width:1000px){body #ajax-content-wrap.no-scroll{min-height:calc(100vh - 111px);height:calc(100vh - 111px)!important;}}@media only screen and (min-width:1000px){#page-header-wrap.fullscreen-header,#page-header-wrap.fullscreen-header #page-header-bg,html:not(.nectar-box-roll-loaded) .nectar-box-roll > #page-header-bg.fullscreen-header,.nectar_fullscreen_zoom_recent_projects,#nectar_fullscreen_rows:not(.afterLoaded) > div{height:calc(100vh - 110px);}.wpb_row.vc_row-o-full-height.top-level,.wpb_row.vc_row-o-full-height.top-level > .col.span_12{min-height:calc(100vh - 110px);}html:not(.nectar-box-roll-loaded) .nectar-box-roll > #page-header-bg.fullscreen-header{top:111px;}.nectar-slider-wrap[data-fullscreen="true"]:not(.loaded),.nectar-slider-wrap[data-fullscreen="true"]:not(.loaded) .swiper-container{height:calc(100vh - 109px)!important;}.admin-bar .nectar-slider-wrap[data-fullscreen="true"]:not(.loaded),.admin-bar .nectar-slider-wrap[data-fullscreen="true"]:not(.loaded) .swiper-container{height:calc(100vh - 109px - 32px)!important;}}#nectar_fullscreen_rows{background-color:;}
/*Blog */
.sin-categoria,
#single-below-header,
#author-bio {
  display: none!important;
}


div#footer-widgets::after {
  content: '';
  position: absolute;
  display: block;
  width: 77px;
  height: 130px;
  background: url(https://temporizar.testbh.com/wp-content/uploads/2020/02/A__Flor.png);
  right: 0;
  bottom: 0px;
  background-repeat: no-repeat;
  background-size: contain;
}

/*Footer */
#footer-widgets .container .row {
  padding:0!important;
}
.container .row .col .widget_text.widget.widget_custom_html .textwidget.custom-html-widget .footer-container img {
  margin-bottom:0;
}

.container .row .col .widget_text.widget.widget_custom_html .textwidget.custom-html-widget .footer-container img {
  width: 480px;
}
/*Second Footer*/
#copyright.row {
  padding:0 !important;
  position:initial;
}

.row .container .col .absolute-footer-container {
  padding:0 0 20px 0 !important;
  display: flex;
  width: 100%;

}
.row .container .col .absolute-footer-container > div > a {
  padding:5px;
}
.row .container .col .absolute-footer-container > div {
  text-align: center;
}
.row .container .col .absolute-footer-container > div:nth-child(1){
  width: 4%;
}
.row .container .col .absolute-footer-container > div:nth-child(2){
  width: 32%;
}
.row .container .col .absolute-footer-container > div:nth-child(3){
  width: 15%;
}
.row .container .col .absolute-footer-container > div:nth-child(4){
  width: 20%;
}
.row .container .col .absolute-footer-container > div:nth-child(5){
  width: 25%;
}
.row .container .col .absolute-footer-container > div:nth-child(6){
  width: 4%;
}

#footer-outer[data-full-width="1"] .container {
  padding: 0 44px;
}

.row .container .col .absolute-footer-container > div > p > img,
.row .container .col .absolute-footer-container > div > a > img {
  width: 30px;
  margin-bottom: 0;
  position: relative;
  top: 14px;
  right: 10px;
}
#footer-outer {
  display: grid;
}
#footer-widgets {
  order: 2;
}
#copyright {
  order: 1;
}

#footer-outer #copyright .container .col.span_5 > p {
  display: none;
}

@media only screen and (max-width:980px) {
  .row .container .col .absolute-footer-container {
      display: block;
  }
  .row .container .col .absolute-footer-container > div:nth-child(1){
      display:none;
  }
  .row .container .col .absolute-footer-container > div:nth-child(2){
      width: 100%;
  }
  .row .container .col .absolute-footer-container > div:nth-child(3){
      width: 100%;
  }
  .row .container .col .absolute-footer-container > div:nth-child(4){
      width: 100%;
  }
  .row .container .col .absolute-footer-container > div:nth-child(5){
      width: 100%;
  }
  .row .container .col .absolute-footer-container > div:nth-child(6){
      display:none;
  }
  div#footer-widgets::after {
    width: 120px;
    height: 220px;
    background-size:cover;
  }
}
@media only screen and (max-width:690px) {
  div#footer-widgets::after {
    width: 50px;
    height: 90px;
  }
}
</style>
<link href="//temporizar.com/wp-content/plugins/wp-client/css/user_style.css?ver=4.7.4.1" id="wpc_user_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//temporizar.com/wp-content/plugins/wp-client/css/user/general.css?ver=4.7.4.1" id="wpc_user_general_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//temporizar.com/wp-content/plugins/wp-client/css/ez_hub_bar.css?ver=4.7.4.1" id="wp-client-ez-hub-bar-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto%3A400&amp;ver=1600105692" id="redux-google-fonts-salient_redux-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://temporizar.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://temporizar.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://temporizar.com/wp-content/plugins/login-and-logout-redirect/public/js/login-and-logout-redirect-public.js?ver=2.0.0" type="text/javascript"></script>
<link href="https://temporizar.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://temporizar.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://temporizar.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.4" name="generator"/>
<script type="text/javascript"> var root = document.getElementsByTagName( "html" )[0]; root.setAttribute( "class", "js" ); </script><meta content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." name="generator"/>
<link href="https://temporizar.com/wp-content/uploads/2020/05/cropped-favicon-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://temporizar.com/wp-content/uploads/2020/05/cropped-favicon-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://temporizar.com/wp-content/uploads/2020/05/cropped-favicon-180x180.png" rel="apple-touch-icon"/>
<meta content="https://temporizar.com/wp-content/uploads/2020/05/cropped-favicon-270x270.png" name="msapplication-TileImage"/>
<noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
</head>
<body class="error404 material wpb-js-composer js-comp-ver-6.1 vc_responsive elementor-default" data-aie="none" data-ajax-transitions="false" data-animated-anchors="true" data-apte="standard" data-bg-header="false" data-body-border="off" data-boxed-style="" data-button-style="slightly_rounded_shadow" data-cad="750" data-cae="easeOutCubic" data-cart="false" data-col-gap="default" data-dropdown-style="minimal" data-ext-responsive="true" data-fancy-form-rcs="default" data-flex-cols="true" data-footer-reveal="false" data-footer-reveal-shadow="none" data-force-header-trans-color="light" data-form-style="default" data-form-submit="regular" data-full-width-header="false" data-header-breakpoint="1000" data-header-color="custom" data-header-format="default" data-header-inherit-rc="false" data-header-resize="1" data-header-search="false" data-hhun="0" data-is="minimal" data-loading-animation="none" data-ls="fancybox" data-m-animate="0" data-megamenu-width="contained" data-permanent-transparent="false" data-remove-m-parallax="" data-remove-m-video-bgs="" data-responsive="1" data-rsssl="1" data-slide-out-widget-area="true" data-slide-out-widget-area-style="slide-out-from-right" data-smooth-scrolling="0" data-transparent-header="false" data-user-account-button="false" data-user-set-ocm="off">
<script type="text/javascript"> if(navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/)) { document.body.className += " using-mobile-browser "; } </script><div class="ocm-effect-wrap"><div class="ocm-effect-wrap-inner">
<div data-header-mobile-fixed="1" id="header-space"></div>
<div data-box-shadow="large" data-cart="false" data-condense="false" data-format="default" data-full-width="false" data-has-buttons="no" data-has-menu="true" data-header-button_style="default" data-header-resize="1" data-lhe="default" data-logo-height="55" data-m-logo-height="50" data-megamenu-rt="0" data-mobile-fixed="1" data-padding="28" data-permanent-transparent="false" data-ptnm="false" data-remove-fixed="0" data-shrink-num="6" data-transparency-option="0" data-user-set-bg="#ffffff" data-using-logo="1" data-using-pr-menu="false" data-using-secondary="0" id="header-outer">
<div class="nectar" id="search-outer">
<div id="search">
<div class="container">
<div id="search-box">
<div class="inner-wrap">
<div class="col span_12">
<form action="https://temporizar.com/" method="GET" role="search">
<input name="s" placeholder="Search" type="text" value=""/>
<span>Hit enter to search or ESC to close</span> </form>
</div><!--/span_12-->
</div><!--/inner-wrap-->
</div><!--/search-box-->
<div id="close"><a href="#">
<span class="close-wrap"> <span class="close-line close-line1"></span> <span class="close-line close-line2"></span> </span> </a></div>
</div><!--/container-->
</div><!--/search-->
</div><!--/search-outer-->
<header id="top">
<div class="container">
<div class="row">
<div class="col span_3">
<a data-supplied-ml="false" data-supplied-ml-starting="false" data-supplied-ml-starting-dark="false" href="https://temporizar.com" id="logo">
<img alt="Temporizar" class="stnd dark-version" src="https://temporizar.com/wp-content/uploads/2020/01/logo_final-1.png"/>
</a>
</div><!--/span_3-->
<div class="col span_9 col_last">
<div class="slide-out-widget-area-toggle mobile-icon slide-out-from-right" data-custom-color="false" data-icon-animation="simple-transform">
<div> <a aria-expanded="false" aria-label="Navigation Menu" class="closed" href="#sidewidgetarea">
<span aria-hidden="true"> <i class="lines-button x2"> <i class="lines"></i> </i> </span>
</a></div>
</div>
<nav>
<ul class="sf-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-29" id="menu-item-29"><a href="https://temporizar.com/">Inicio</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28" id="menu-item-28"><a href="https://temporizar.com/noticias/">Noticias</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-68" id="menu-item-68"><a href="#">Nosotros</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27" id="menu-item-27"><a href="https://temporizar.com/nuestra-compania/">Nuestra Compañia</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-289" id="menu-item-289"><a href="https://temporizar.com/nuestro-portafolio/">Nuestro Portafolio</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6060" id="menu-item-6060"><a href="https://temporizar.t3rsc.co/">Vacantes</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26" id="menu-item-26"><a href="https://temporizar.com/contactanos/">Contáctanos</a></li>
</ul>
<ul class="buttons sf-menu" data-user-set-ocm="off">
</ul>
</nav>
</div><!--/span_9-->
</div><!--/row-->
</div><!--/container-->
</header>
</div>
<div id="ajax-content-wrap">
<div class="container-wrap">
<div class="container main-content">
<div class="row">
<div class="col span_12">
<div id="error-404">
<h1>404</h1>
<h2>Page Not Found</h2>
<a class="nectar-button large regular-button accent-color has-icon" data-color-override="false" data-hover-color-override="false" href="https://temporizar.com"><span>Back Home </span><i class="icon-button-arrow"></i></a>
</div>
</div><!--/span_12-->
</div><!--/row-->
</div><!--/container-->
</div><!--/container-wrap-->
<div data-bg-img-overlay="0.8" data-cols="1" data-copyright-line="false" data-custom-color="true" data-disable-copyright="false" data-full-width="1" data-link-hover="default" data-matching-section-color="false" data-midnight="light" data-using-bg-img="false" data-using-widget-area="true" id="footer-outer">
<div data-cols="1" data-has-widgets="true" id="footer-widgets">
<div class="container">
<div class="row">
<div class="col span_12">
<!-- Footer widget area 1 -->
<div class="widget_text widget widget_custom_html" id="custom_html-2"><div class="textwidget custom-html-widget"><div class="footer-container">
<a href="http://peoplegrowth.com.co/" rel="noopener noreferrer" target="_blank">
<img src="https://temporizar.com/wp-content/uploads/2020/01/footer_2.png"/>
</a>
</div></div></div> </div><!--/span_3-->
</div><!--/row-->
</div><!--/container-->
</div><!--/footer-widgets-->
<div class="row" data-layout="default" id="copyright">
<div class="container">
<div class="col span_7 col_last">
<ul class="social">
</ul>
</div><!--/span_7-->
<div class="col span_5">
<div class="widget">
</div>
<p>
</p><div class="absolute-footer-container"> <div> </div> <div> <p><img alt="" height="30" src="/wp-content/uploads/2020/01/ubicacion.png" width="30"/>Sede Principal Carrera 7B             #123-77 Bogotá D.C. - Colombia</p> </div> <div> <p><img alt="" height="30" src="/wp-content/uploads/2020/01/contacto.png" width="30"/>PBX: (571) 5188436</p> </div> <div> <p><img alt="" height="30" src="/wp-content/uploads/2020/01/correo.png" width="30"/> <a href="mailto:servicio.cleinte@temporizar.com">servicio.cliente@temporizar.com</a></p> </div> <div> <a href="https://co.linkedin.com/company/temporizar-colombia" target="_blank"> <img alt="" height="30" src="/wp-content/uploads/2020/01/linkedin.png" width="30"/> </a> <a href="https://es-la.facebook.com/Temporizar/" target="_blank"> <img alt="" height="30" src="/wp-content/uploads/2020/01/facebook.png" width="30"/> </a> <a href="https://www.instagram.com/temporizarcolombia/" target="_blank"> <img alt="" height="30" src="/wp-content/uploads/2020/01/instagram.png" width="30"/> </a> </div> <div> </div> </div>
</div><!--/span_5-->
</div><!--/container-->
</div><!--/row-->
</div><!--/footer-outer-->
<div class="slide-out-from-right dark" id="slide-out-widget-area-bg">
</div>
<div class="slide-out-from-right" data-back-txt="Back" data-dropdown-func="separate-dropdown-parent-link" id="slide-out-widget-area">
<div class="inner-wrap">
<div class="inner" data-prepend-menu-mobile="false">
<a class="slide_out_area_close" href="#">
<span class="close-wrap"> <span class="close-line close-line1"></span> <span class="close-line close-line2"></span> </span> </a>
<div class="off-canvas-menu-container mobile-only">
<ul class="menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-29"><a href="https://temporizar.com/">Inicio</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="https://temporizar.com/noticias/">Noticias</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-68"><a href="#">Nosotros</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27"><a href="https://temporizar.com/nuestra-compania/">Nuestra Compañia</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-289"><a href="https://temporizar.com/nuestro-portafolio/">Nuestro Portafolio</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6060"><a href="https://temporizar.t3rsc.co/">Vacantes</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26"><a href="https://temporizar.com/contactanos/">Contáctanos</a></li>
</ul>
<ul class="menu secondary-header-items">
</ul>
</div>
</div>
<div class="bottom-meta-wrap"></div><!--/bottom-meta-wrap--></div> <!--/inner-wrap-->
</div>
</div> <!--/ajax-content-wrap-->
<a class=" " id="to-top"><i class="fa fa-angle-up"></i></a>
</div></div><!--/ocm-effect-wrap--><script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/temporizar.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://temporizar.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.9" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var nectarLove = {"ajaxurl":"https:\/\/temporizar.com\/wp-admin\/admin-ajax.php","postID":"6860","rooturl":"https:\/\/temporizar.com","loveNonce":"f10b482ecd"};
/* ]]> */
</script>
<script src="https://temporizar.com/wp-content/plugins/salient-social/js/salient-social.js?ver=1.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7_redirect_forms = {"505":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","delay_redirect":"0","after_sent_script":"","thankyou_page_url":""},"503":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","delay_redirect":"0","after_sent_script":"","thankyou_page_url":""},"180":{"page_id":"185","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","delay_redirect":"5000","after_sent_script":"","thankyou_page_url":"https:\/\/temporizar.com\/mensaje-contacto\/"},"176":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","delay_redirect":"0","after_sent_script":"","thankyou_page_url":""}};
/* ]]> */
</script>
<script src="https://temporizar.com/wp-content/plugins/wpcf7-redirect/js/wpcf7-redirect-script.js" type="text/javascript"></script>
<script src="https://temporizar.com/wp-content/themes/salient/js/third-party/jquery.easing.js?ver=1.3" type="text/javascript"></script>
<script src="https://temporizar.com/wp-content/themes/salient/js/third-party/jquery.mousewheel.js?ver=3.1.13" type="text/javascript"></script>
<script src="https://temporizar.com/wp-content/themes/salient/js/priority.js?ver=12.0" type="text/javascript"></script>
<script src="https://temporizar.com/wp-content/themes/salient/js/third-party/transit.js?ver=0.9.9" type="text/javascript"></script>
<script src="https://temporizar.com/wp-content/themes/salient/js/third-party/waypoints.js?ver=4.0.1" type="text/javascript"></script>
<script src="https://temporizar.com/wp-content/plugins/salient-portfolio/js/third-party/imagesLoaded.min.js?ver=4.1.4" type="text/javascript"></script>
<script src="https://temporizar.com/wp-content/themes/salient/js/third-party/hoverintent.js?ver=1.9" type="text/javascript"></script>
<script src="https://temporizar.com/wp-content/themes/salient/js/third-party/jquery.fancybox.min.js?ver=3.3.1" type="text/javascript"></script>
<script src="https://temporizar.com/wp-content/themes/salient/js/third-party/superfish.js?ver=1.4.8" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var nectarLove = {"ajaxurl":"https:\/\/temporizar.com\/wp-admin\/admin-ajax.php","postID":"6860","rooturl":"https:\/\/temporizar.com","disqusComments":"false","loveNonce":"f10b482ecd","mapApiKey":""};
/* ]]> */
</script>
<script src="https://temporizar.com/wp-content/themes/salient/js/init.js?ver=12.0" type="text/javascript"></script>
<script src="https://temporizar.com/wp-content/plugins/salient-core/js/third-party/touchswipe.min.js?ver=1.0" type="text/javascript"></script>
<script src="https://temporizar.com/wp-content/plugins/page-links-to/dist/new-tab.js?ver=3.3.5" type="text/javascript"></script>
<script src="//temporizar.com/wp-content/plugins/wp-client/js/pages/ez_hub_bar.js?ver=4.7.4.1" type="text/javascript"></script>
<script src="https://temporizar.com/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
<script async="" src="//code.jivosite.com/widget/lQ6OMGvQl3"></script>
</body>
</html>