<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html version="XHTML+RDFa 1.0" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="http://rdf.data-vocabulary.org/#">
<head id="Head1"><meta content="IE=edge" http-equiv="X-UA-Compatible"/><meta content="index,follow" name="ROBOTS"/><meta content="NOODP,NOYDIR" name="ROBOTS"/><meta content="app-id=619303487" name="apple-itunes-app"/>
<script src="https://scripts.bookwire.com/Scripts/jquery-1.4.1.min.js?4.0.4.009" type="text/javascript"></script>
<script src="https://scripts.bookwire.com/Scripts/jquery-1.4.1.js?4.0.4.009" type="text/javascript"></script>
<script src="https://scripts.bookwire.com/Scripts/jquery-1.4.1-vsdoc.js?4.0.4.009" type="text/javascript"></script>
<script src="https://scripts.bookwire.com/Scripts/jquery-ui-1.8.18.custom.min.js?4.0.4.009" type="text/javascript"></script>
<script src="https://scripts.bookwire.com/Scripts/json2.js?4.0.4.009" type="text/javascript"></script>
<script src="https://scripts.bookwire.com/Scripts/jquery.blockUI.js?4.0.4.009" type="text/javascript"></script>
<script src="https://scripts.bookwire.com/Scripts/jquery.cookie.js?4.0.4.009" type="text/javascript"></script>
<script src="https://scripts.bookwire.com/Scripts/jsfcore.js?4.0.4.009" type="text/javascript"></script>
<script src="https://scripts.bookwire.com/Scripts/JsUtils.js?4.0.4.009" type="text/javascript"></script>
<script src="https://scripts.bookwire.com/Scripts/masterpage.js?4.0.4.009" type="text/javascript"></script>
<script src="https://scripts.bookwire.com/Scripts/BookwireSearchFunction.js?4.0.4.009" type="text/javascript"></script>
<link href="https://images.bookwire.com/App_Themes/loginStyles.css?4.0.4.009" rel="stylesheet" type="text/css"/><link href="https://images.bookwire.com/App_Themes/basestyle.css?4.0.4.009" rel="stylesheet" type="text/css"/><link href="https://images.bookwire.com/App_Themes/masterPage.css?4.0.4.009" rel="stylesheet" type="text/css"/><link href="https://images.bookwire.com/App_Themes/bookwire.css?4.0.4.009" rel="stylesheet" type="text/css"/><title>
	John Peel Books | Bookwire
</title><link href="https://images.bookwire.com/App_Themes/Images/pq-favicon.ico?4.0.4.009" id="Link1" rel="shortcut icon" type="image/x-icon"/><link href="https://images.bookwire.com/App_Themes/Images/pq-favicon.ico?4.0.4.009" id="Link2" rel="icon" type="image/ico"/>
<script src="https://scripts.bookwire.com/Scripts/jquery-1.4.1.min.js?4.0.4.009" type="text/javascript"></script>
<script src="https://scripts.bookwire.com/Scripts/BookwireSearchFunction.js?4.0.4.009" type="text/javascript"></script>
<script src="https://scripts.bookwire.com/Scripts/masterpage.js?4.0.4.009" type="text/javascript"></script>
<script src="https://scripts.bookwire.com/Scripts/AuthorPage.js?4.0.4.009" type="text/javascript"></script>
<link href="https://images.bookwire.com/App_Themes/home.css?4.0.4.009" rel="stylesheet" type="text/css"/>
<link href="https://images.bookwire.com/App_Themes/style.css?4.0.4.009" rel="stylesheet" type="text/css"/>
<meta content='John Peel is the author of "The Zanti Misfits", "Suddenly Twins!", "Lost in Vegas!" and "The Death of Princes"' name="description"/><meta content="books, romance books, book, buy books, discount books, cheap books, bookstores, bookstore, booksellers, bookseller, bargain books, bargain book, new books, hard-to-find books, bestsellers, bestseller, audio books, audio book, computer books, children's book, children's books, book reviews, book review, textbooks, fiction, non-fiction, nonfiction" name="keywords"/></head>
<body>
<form action="./John-Peel-62580" id="form1" method="post">
<input id="__VIEWSTATE" name="__VIEWSTATE" type="hidden" value="/wEPDwUJNDYyNjczODU0D2QWAmYPZBYEAgEPZBYQAgQPFQtCaHR0cHM6Ly9zY3JpcHRzLmJvb2t3aXJlLmNvbS9TY3JpcHRzL2pxdWVyeS0xLjQuMS5taW4uanM/NC4wLjQuMDA5Pmh0dHBzOi8vc2NyaXB0cy5ib29rd2lyZS5jb20vU2NyaXB0cy9qcXVlcnktMS40LjEuanM/NC4wLjQuMDA5RGh0dHBzOi8vc2NyaXB0cy5ib29rd2lyZS5jb20vU2NyaXB0cy9qcXVlcnktMS40LjEtdnNkb2MuanM/NC4wLjQuMDA5TWh0dHBzOi8vc2NyaXB0cy5ib29rd2lyZS5jb20vU2NyaXB0cy9qcXVlcnktdWktMS44LjE4LmN1c3RvbS5taW4uanM/NC4wLjQuMDA5N2h0dHBzOi8vc2NyaXB0cy5ib29rd2lyZS5jb20vU2NyaXB0cy9qc29uMi5qcz80LjAuNC4wMDlAaHR0cHM6Ly9zY3JpcHRzLmJvb2t3aXJlLmNvbS9TY3JpcHRzL2pxdWVyeS5ibG9ja1VJLmpzPzQuMC40LjAwOT9odHRwczovL3NjcmlwdHMuYm9va3dpcmUuY29tL1NjcmlwdHMvanF1ZXJ5LmNvb2tpZS5qcz80LjAuNC4wMDk5aHR0cHM6Ly9zY3JpcHRzLmJvb2t3aXJlLmNvbS9TY3JpcHRzL2pzZmNvcmUuanM/NC4wLjQuMDA5OWh0dHBzOi8vc2NyaXB0cy5ib29rd2lyZS5jb20vU2NyaXB0cy9Kc1V0aWxzLmpzPzQuMC40LjAwOTxodHRwczovL3NjcmlwdHMuYm9va3dpcmUuY29tL1NjcmlwdHMvbWFzdGVycGFnZS5qcz80LjAuNC4wMDlIaHR0cHM6Ly9zY3JpcHRzLmJvb2t3aXJlLmNvbS9TY3JpcHRzL0Jvb2t3aXJlU2VhcmNoRnVuY3Rpb24uanM/NC4wLjQuMDA5ZAIFDxYCHgRocmVmBUBodHRwczovL2ltYWdlcy5ib29rd2lyZS5jb20vQXBwX1RoZW1lcy9sb2dpblN0eWxlcy5jc3M/NC4wLjQuMDA5ZAIGDxYCHwAFPmh0dHBzOi8vaW1hZ2VzLmJvb2t3aXJlLmNvbS9BcHBfVGhlbWVzL2Jhc2VzdHlsZS5jc3M/NC4wLjQuMDA5ZAIHDxYCHwAFP2h0dHBzOi8vaW1hZ2VzLmJvb2t3aXJlLmNvbS9BcHBfVGhlbWVzL21hc3RlclBhZ2UuY3NzPzQuMC40LjAwOWQCCA8WAh8ABT1odHRwczovL2ltYWdlcy5ib29rd2lyZS5jb20vQXBwX1RoZW1lcy9ib29rd2lyZS5jc3M/NC4wLjQuMDA5ZAIKDxYCHwAFRmh0dHBzOi8vaW1hZ2VzLmJvb2t3aXJlLmNvbS9BcHBfVGhlbWVzL0ltYWdlcy9wcS1mYXZpY29uLmljbz80LjAuNC4wMDlkAgsPFgIfAAVGaHR0cHM6Ly9pbWFnZXMuYm9va3dpcmUuY29tL0FwcF9UaGVtZXMvSW1hZ2VzL3BxLWZhdmljb24uaWNvPzQuMC40LjAwOWQCDA9kFgJmDxUGQmh0dHBzOi8vc2NyaXB0cy5ib29rd2lyZS5jb20vU2NyaXB0cy9qcXVlcnktMS40LjEubWluLmpzPzQuMC40LjAwOUhodHRwczovL3NjcmlwdHMuYm9va3dpcmUuY29tL1NjcmlwdHMvQm9va3dpcmVTZWFyY2hGdW5jdGlvbi5qcz80LjAuNC4wMDk8aHR0cHM6Ly9zY3JpcHRzLmJvb2t3aXJlLmNvbS9TY3JpcHRzL21hc3RlcnBhZ2UuanM/NC4wLjQuMDA5PGh0dHBzOi8vc2NyaXB0cy5ib29rd2lyZS5jb20vU2NyaXB0cy9BdXRob3JQYWdlLmpzPzQuMC40LjAwOTlodHRwczovL2ltYWdlcy5ib29rd2lyZS5jb20vQXBwX1RoZW1lcy9ob21lLmNzcz80LjAuNC4wMDk6aHR0cHM6Ly9pbWFnZXMuYm9va3dpcmUuY29tL0FwcF9UaGVtZXMvc3R5bGUuY3NzPzQuMC40LjAwOWQCAw9kFgQCAQ8QZA8WBmYCAQICAgMCBAIFFgYQBQNBbGwFD3F1aWNrc2VhcmNoLWFsbGcQBQdTdWJqZWN0BRNxdWlja3NlYXJjaC1zdWJqZWN0ZxAFBVRpdGxlBRFxdWlja3NlYXJjaC10aXRsZWcQBQZBdXRob3IFEnF1aWNrc2VhcmNoLWF1dGhvcmcQBQlQdWJsaXNoZXIFFXF1aWNrc2VhcmNoLXB1Ymxpc2hlcmcQBQxTZXJpZXMgVGl0bGUFF3F1aWNrc2VhcmNoLXNlcmllc3RpdGxlZ2RkAgQPZBYOAgEPFgIeCWlubmVyaHRtbAUJSm9obiBQZWVsZAIDDxYCHwEFb0pvaG4gUGVlbCBpcyB0aGUgYXV0aG9yIG9mICJUaGUgWmFudGkgTWlzZml0cyIsICJTdWRkZW5seSBUd2lucyEiLCAiTG9zdCBpbiBWZWdhcyEiIGFuZCAiVGhlIERlYXRoIG9mIFByaW5jZXMiLmQCBQ8WBB8ABTJodHRwczovL3d3dy5ib29rd2lyZS5jb20vYXV0aG9yYmlvL0pvaG4tUGVlbC02MjU4MB4HVmlzaWJsZWhkAgcPZBYKAgEPFgQeBXRpdGxlBQlKb2huIFBlZWwfAQUSQm9va3MgYnkgSm9obiBQZWVsZAIDDxYCHwAFR2h0dHBzOi8vd3d3LmJvb2t3aXJlLmNvbS9ib29rcy9hdXRob3IvSm9obi1QZWVsL0Jvb2tzLUJ5P2F1dGhvcklkPTYyNTgwZAIFDzwrAAkBAA8WBB4IRGF0YUtleXMWAB4LXyFJdGVtQ291bnQCBGQWCGYPZBYGZg8VDg1Cb29rIG9mIE1hZ2ljGGh0dHBzOi8vd3d3LmJvb2t3aXJlLmNvbRFVU0EvQm9vay1vZi1NYWdpYw05NzgwNzM4NzA2MTUzCUpvaG4tUGVlbAc5NTE2NjI0DTk3ODA3Mzg3MDYxNTMNQm9vayBvZiBNYWdpYxhodHRwczovL3d3dy5ib29rd2lyZS5jb20RVVNBL0Jvb2stb2YtTWFnaWMNOTc4MDczODcwNjE1MwlKb2huLVBlZWwHOTUxNjYyNA1Cb29rIG9mIE1hZ2ljZAIBDxYCHgVzdHlsZQUMZGlzcGxheTpub25lFgJmDxUB0gE8aW5wdXQgdHlwZT0nYnV0dG9uJyBjbGFzcz0nZW1wdHlSYXRpbmcnLz48aW5wdXQgdHlwZT0nYnV0dG9uJyBjbGFzcz0nZW1wdHlSYXRpbmcnLz48aW5wdXQgdHlwZT0nYnV0dG9uJyBjbGFzcz0nZW1wdHlSYXRpbmcnLz48aW5wdXQgdHlwZT0nYnV0dG9uJyBjbGFzcz0nZW1wdHlSYXRpbmcnLz48aW5wdXQgdHlwZT0nYnV0dG9uJyBjbGFzcz0nZW1wdHlSYXRpbmcnLz5kAgIPFQQKUGVlbCwgSm9obgpQZWVsLCBKb2huCVBhcGVyYmFjawUkNC45OWQCAQ9kFgZmDxUOEVRoZSBaYW50aSBNaXNmaXRzGGh0dHBzOi8vd3d3LmJvb2t3aXJlLmNvbRVVU0EvVGhlLVphbnRpLU1pc2ZpdHMNOTc4MDgxMjU5MDYzMAlKb2huLVBlZWwINDc0MTk0NTMNOTc4MDgxMjU5MDYzMBFUaGUgWmFudGkgTWlzZml0cxhodHRwczovL3d3dy5ib29rd2lyZS5jb20VVVNBL1RoZS1aYW50aS1NaXNmaXRzDTk3ODA4MTI1OTA2MzAJSm9obi1QZWVsCDQ3NDE5NDUzEVRoZSBaYW50aSBNaXNmaXRzZAIBDxYCHwYFDGRpc3BsYXk6bm9uZRYCZg8VAdIBPGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+ZAICDxUEClBlZWwsIEpvaG4KUGVlbCwgSm9obglQYXBlcmJhY2sFJDMuOTlkAgIPZBYGZg8VDgxUaGUgSW5ub2NlbnQYaHR0cHM6Ly93d3cuYm9va3dpcmUuY29tEFVTQS9UaGUtSW5ub2NlbnQNOTc4MDgxMjU2NDU1NglKb2huLVBlZWwINDc1Mjk3NzMNOTc4MDgxMjU2NDU1NgxUaGUgSW5ub2NlbnQYaHR0cHM6Ly93d3cuYm9va3dpcmUuY29tEFVTQS9UaGUtSW5ub2NlbnQNOTc4MDgxMjU2NDU1NglKb2huLVBlZWwINDc1Mjk3NzMMVGhlIElubm9jZW50ZAIBDxYCHwYFDGRpc3BsYXk6bm9uZRYCZg8VAdIBPGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+ZAICDxUEClBlZWwsIEpvaG4KUGVlbCwgSm9obglQYXBlcmJhY2sFJDMuOTlkAgMPZBYGZg8VDgpSZXZvbHV0aW9uGGh0dHBzOi8vd3d3LmJvb2t3aXJlLmNvbQ5VU0EvUmV2b2x1dGlvbg05NzgwNDM5MDYwMzMyCUpvaG4tUGVlbAc2MTUwODMzDTk3ODA0MzkwNjAzMzIKUmV2b2x1dGlvbhhodHRwczovL3d3dy5ib29rd2lyZS5jb20OVVNBL1Jldm9sdXRpb24NOTc4MDQzOTA2MDMzMglKb2huLVBlZWwHNjE1MDgzMwpSZXZvbHV0aW9uZAIBDxYCHwYFDGRpc3BsYXk6bm9uZRYCZg8VAdIBPGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+ZAICDxUEClBlZWwsIEpvaG4KUGVlbCwgSm9obglQYXBlcmJhY2sFJDQuOTlkAgcPPCsACQEADxYEHwQWAB8FAgRkFghmD2QWBmYPFQ4PQm9vayBvZiBUaHVuZGVyGGh0dHBzOi8vd3d3LmJvb2t3aXJlLmNvbRNVU0EvQm9vay1vZi1UaHVuZGVyDTk3ODA3Mzg3MDYxNDYJSm9obi1QZWVsBzk1MTY2MjINOTc4MDczODcwNjE0Ng9Cb29rIG9mIFRodW5kZXIYaHR0cHM6Ly93d3cuYm9va3dpcmUuY29tE1VTQS9Cb29rLW9mLVRodW5kZXINOTc4MDczODcwNjE0NglKb2huLVBlZWwHOTUxNjYyMg9Cb29rIG9mIFRodW5kZXJkAgEPFgIfBgUMZGlzcGxheTpub25lFgJmDxUB0gE8aW5wdXQgdHlwZT0nYnV0dG9uJyBjbGFzcz0nZW1wdHlSYXRpbmcnLz48aW5wdXQgdHlwZT0nYnV0dG9uJyBjbGFzcz0nZW1wdHlSYXRpbmcnLz48aW5wdXQgdHlwZT0nYnV0dG9uJyBjbGFzcz0nZW1wdHlSYXRpbmcnLz48aW5wdXQgdHlwZT0nYnV0dG9uJyBjbGFzcz0nZW1wdHlSYXRpbmcnLz48aW5wdXQgdHlwZT0nYnV0dG9uJyBjbGFzcz0nZW1wdHlSYXRpbmcnLz5kAgIPFQQKUGVlbCwgSm9obgpQZWVsLCBKb2huCVBhcGVyYmFjawUkNS45OWQCAQ9kFgZmDxUODkxvc3QgaW4gVmVnYXMhGGh0dHBzOi8vd3d3LmJvb2t3aXJlLmNvbRFVU0EvTG9zdC1pbi1WZWdhcw05NzgwNjcxMDA3MTAyCUpvaG4tUGVlbAg0NzU5MjgxMw05NzgwNjcxMDA3MTAyDkxvc3QgaW4gVmVnYXMhGGh0dHBzOi8vd3d3LmJvb2t3aXJlLmNvbRFVU0EvTG9zdC1pbi1WZWdhcw05NzgwNjcxMDA3MTAyCUpvaG4tUGVlbAg0NzU5MjgxMw5Mb3N0IGluIFZlZ2FzIWQCAQ8WAh8GBQxkaXNwbGF5Om5vbmUWAmYPFQHSATxpbnB1dCB0eXBlPSdidXR0b24nIGNsYXNzPSdlbXB0eVJhdGluZycvPjxpbnB1dCB0eXBlPSdidXR0b24nIGNsYXNzPSdlbXB0eVJhdGluZycvPjxpbnB1dCB0eXBlPSdidXR0b24nIGNsYXNzPSdlbXB0eVJhdGluZycvPjxpbnB1dCB0eXBlPSdidXR0b24nIGNsYXNzPSdlbXB0eVJhdGluZycvPjxpbnB1dCB0eXBlPSdidXR0b24nIGNsYXNzPSdlbXB0eVJhdGluZycvPmQCAg8VBApQZWVsLCBKb2huClBlZWwsIEpvaG4JUGFwZXJiYWNrBSQzLjk5ZAICD2QWBmYPFQ4PU3VkZGVubHkgVHdpbnMhGGh0dHBzOi8vd3d3LmJvb2t3aXJlLmNvbRJVU0EvU3VkZGVubHktVHdpbnMNOTc4MDc0MzQxNzYyNAlKb2huLVBlZWwHNzA1NTc5NQ05NzgwNzQzNDE3NjI0D1N1ZGRlbmx5IFR3aW5zIRhodHRwczovL3d3dy5ib29rd2lyZS5jb20SVVNBL1N1ZGRlbmx5LVR3aW5zDTk3ODA3NDM0MTc2MjQJSm9obi1QZWVsBzcwNTU3OTUPU3VkZGVubHkgVHdpbnMhZAIBDxYCHwYFDGRpc3BsYXk6bm9uZRYCZg8VAdIBPGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+ZAICDxUEClBlZWwsIEpvaG4KUGVlbCwgSm9obglQYXBlcmJhY2sFJDQuOTlkAgMPZBYGZg8VDhRUaGUgRGVhdGggb2YgUHJpbmNlcxhodHRwczovL3d3dy5ib29rd2lyZS5jb20YVVNBL1RoZS1EZWF0aC1vZi1QcmluY2VzDTk3ODA2NzE1NjgwODUJSm9obi1QZWVsCDQ3MTgxNTgyDTk3ODA2NzE1NjgwODUUVGhlIERlYXRoIG9mIFByaW5jZXMYaHR0cHM6Ly93d3cuYm9va3dpcmUuY29tGFVTQS9UaGUtRGVhdGgtb2YtUHJpbmNlcw05NzgwNjcxNTY4MDg1CUpvaG4tUGVlbAg0NzE4MTU4MhRUaGUgRGVhdGggb2YgUHJpbi4uLmQCAQ8WAh8GBQxkaXNwbGF5Om5vbmUWAmYPFQHSATxpbnB1dCB0eXBlPSdidXR0b24nIGNsYXNzPSdlbXB0eVJhdGluZycvPjxpbnB1dCB0eXBlPSdidXR0b24nIGNsYXNzPSdlbXB0eVJhdGluZycvPjxpbnB1dCB0eXBlPSdidXR0b24nIGNsYXNzPSdlbXB0eVJhdGluZycvPjxpbnB1dCB0eXBlPSdidXR0b24nIGNsYXNzPSdlbXB0eVJhdGluZycvPjxpbnB1dCB0eXBlPSdidXR0b24nIGNsYXNzPSdlbXB0eVJhdGluZycvPmQCAg8VBApQZWVsLCBKb2huClBlZWwsIEpvaG4JUGFwZXJiYWNrBSQ1Ljk5ZAIJDzwrAAkBAA8WBB8EFgAfBQIEZBYIZg9kFgZmDxUOA1RhZxhodHRwczovL3d3dy5ib29rd2lyZS5jb20HVVNBL1RhZw05NzgwMTQwMzYwNTMwCUpvaG4tUGVlbAcxNTYxNDM2DTk3ODAxNDAzNjA1MzADVGFnGGh0dHBzOi8vd3d3LmJvb2t3aXJlLmNvbQdVU0EvVGFnDTk3ODAxNDAzNjA1MzAJSm9obi1QZWVsBzE1NjE0MzYDVGFnZAIBDxYCHwYFDGRpc3BsYXk6bm9uZRYCZg8VAdIBPGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+ZAICDxUEClBlZWwsIEpvaG4KUGVlbCwgSm9obglQYXBlcmJhY2sFJDIuOTlkAgEPZBYGZg8VDg1UaGUgTGFzdCBEcm9wGGh0dHBzOi8vd3d3LmJvb2t3aXJlLmNvbRFVU0EvVGhlLUxhc3QtRHJvcA05NzgwNjcxNTM1MzA4CUpvaG4tUGVlbAcxOTY4MjIyDTk3ODA2NzE1MzUzMDgNVGhlIExhc3QgRHJvcBhodHRwczovL3d3dy5ib29rd2lyZS5jb20RVVNBL1RoZS1MYXN0LURyb3ANOTc4MDY3MTUzNTMwOAlKb2huLVBlZWwHMTk2ODIyMg1UaGUgTGFzdCBEcm9wZAIBDxYCHwYFDGRpc3BsYXk6bm9uZRYCZg8VAdIBPGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+ZAICDxUEClBlZWwsIEpvaG4KUGVlbCwgSm9obglQYXBlcmJhY2sFJDMuOTlkAgIPZBYGZg8VDhNCb29rIG9mIERlYWQgVGhpbmdzGGh0dHBzOi8vd3d3LmJvb2t3aXJlLmNvbRdVU0EvQm9vay1vZi1EZWFkLVRoaW5ncw05NzgwOTc3OTg1NjMwC1RpbmEtTC1KZW5zCDE0MDI1Mzk3DTk3ODA5Nzc5ODU2MzATQm9vayBvZiBEZWFkIFRoaW5ncxhodHRwczovL3d3dy5ib29rd2lyZS5jb20XVVNBL0Jvb2stb2YtRGVhZC1UaGluZ3MNOTc4MDk3Nzk4NTYzMAtUaW5hLUwtSmVucwgxNDAyNTM5NxRCb29rIG9mIERlYWQgVGhpbi4uLmQCAQ8WAh8GBQxkaXNwbGF5Om5vbmUWAmYPFQHSATxpbnB1dCB0eXBlPSdidXR0b24nIGNsYXNzPSdlbXB0eVJhdGluZycvPjxpbnB1dCB0eXBlPSdidXR0b24nIGNsYXNzPSdlbXB0eVJhdGluZycvPjxpbnB1dCB0eXBlPSdidXR0b24nIGNsYXNzPSdlbXB0eVJhdGluZycvPjxpbnB1dCB0eXBlPSdidXR0b24nIGNsYXNzPSdlbXB0eVJhdGluZycvPjxpbnB1dCB0eXBlPSdidXR0b24nIGNsYXNzPSdlbXB0eVJhdGluZycvPmQCAg8VBBBHb2xkbWFuLCBLZW5uZXRoEEdvbGRtYW4sIEtlbm5ldGgJUGFwZXJiYWNrBiQxOC4wMGQCAw9kFgZmDxUOFFRoZSBEZWF0aCBvZiBQcmluY2VzGGh0dHBzOi8vd3d3LmJvb2t3aXJlLmNvbRhVU0EvVGhlLURlYXRoLW9mLVByaW5jZXMNOTc4MDc0MzQyMjg4OAlKb2huLVBlZWwIMTk1MDE4NjMNOTc4MDc0MzQyMjg4OBRUaGUgRGVhdGggb2YgUHJpbmNlcxhodHRwczovL3d3dy5ib29rd2lyZS5jb20YVVNBL1RoZS1EZWF0aC1vZi1QcmluY2VzDTk3ODA3NDM0MjI4ODgJSm9obi1QZWVsCDE5NTAxODYzFFRoZSBEZWF0aCBvZiBQcmluLi4uZAIBDxYCHwYFDGRpc3BsYXk6bm9uZRYCZg8VAdIBPGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+PGlucHV0IHR5cGU9J2J1dHRvbicgY2xhc3M9J2VtcHR5UmF0aW5nJy8+ZAICDxUEClBlZWwsIEpvaG4KUGVlbCwgSm9obhRFbGVjdHJvbmljIGJvb2sgdGV4dABkAgkPFgQfBgUMZGlzcGxheTpub25lHwJoFgYCAQ8WBB8DBQlKb2huIFBlZWwfAQUfRm9ydGhjb21pbmcgQm9va3MgYnkgIEpvaG4gUGVlbGQCAw8WAh8ABVBodHRwczovL3d3dy5ib29rd2lyZS5jb20vYm9va3MvYXV0aG9yL0pvaG4tUGVlbC9Gb3J0aGNvbWluZy1Cb29rcz9hdXRob3JJZD02MjU4MGQCBQ88KwAJAGQCCw9kFgYCAQ8WBB8DBQlKb2huIFBlZWwfAQUVQm9va3MgYWJvdXQgSm9obiBQZWVsZAIDDxYCHwAFSmh0dHBzOi8vd3d3LmJvb2t3aXJlLmNvbS9ib29rcy9hdXRob3IvSm9obi1QZWVsL0Jvb2tzLUFib3V0P2F1dGhvcklkPTYyNTgwZAIFDzwrAAkAZAINDxYCHwJoFgQCAQ8WBB8DBQlKb2huIFBlZWwfAQUZQmVzdCBTZWxsZXJzIEJ5IEpvaG4gUGVlbGQCBQ8WAh8ABUtodHRwczovL3d3dy5ib29rd2lyZS5jb20vYm9va3MvYXV0aG9yL0pvaG4tUGVlbC9CZXN0LVNlbGxlcnM/YXV0aG9ySWQ9NjI1ODBkZDub6onfb2UjhJDwpvIsD9d3mvrmi40TPGs9Kz85acYJ"/>
<input id="__VIEWSTATEGENERATOR" name="__VIEWSTATEGENERATOR" type="hidden" value="FC597DDD"/>
<input id="__EVENTVALIDATION" name="__EVENTVALIDATION" type="hidden" value="/wEdAAZRybHW3/UFvrpi4ZH+h9hx2Pj5DI9J0guJ+nUlgVpirOPt1qWUDW/Nq87Nip0964dLXXPdLCqpDB1FPy4Kb6Rms2j8DPxjT/l8HkmI2uB2gaDP1IiWTmIEFttKRsTq20ARPAaN2JbHs1oKxDhoeoqteyfShPQZKV1W7duX+pJCmA=="/>
<div align="center">
<div class="main_container">
<div class="header">
<div class="logo">
<a href="#" id="lnklogo">
<img alt="Bookwire Logo" id="imgBIPLogo" src="https://images.bookwire.com/App_Themes/Images/logo_bookwire.gif?4.0.4.009" style="border: 0px; height: 109px;"/>
</a>
</div>
<div class="search_container " id="divSrchContainer">
<div class="divForPublishers">
<a class="linkForPublishers" href="/ForPublishers" id="lnkForPublisher">For Publishers</a>
</div>
<div class="common-search-select-text-container" id="divContainer">
<div class="search-floatleft" id="divSearchoptions">
<div class="common-select-type">
<select id="ddlSearchType" name="ctl00$ddlSearchType" style="display: none">
<option value="quicksearch-all">All</option>
<option value="quicksearch-subject">Subject</option>
<option value="quicksearch-title">Title</option>
<option value="quicksearch-author">Author</option>
<option value="quicksearch-publisher">Publisher</option>
<option value="quicksearch-seriestitle">Series Title</option>
</select>
<span id="spnSearchType"></span>
<img alt="Search Type" src="https://images.bookwire.com/App_Themes/Images/search_arrow_down.jpg?4.0.4.009" title="Search Type"/>
</div>
<div>
<ul id="searchTypeUl" style="margin-top: 10px">
<li>All</li>
<li>Subject</li>
<li>Title</li>
<li>Author</li>
<li>Publisher</li>
<li>Series Title</li>
</ul>
</div>
</div>
<div class="divTextBox" id="divTextBox">
<input class="common-search-text-box" id="txtSearch" name="ctl00$txtSearch" onblur="watermark('txtSearch','Enter your query here');" onfocus="watermark('txtSearch','Enter your query here');" onkeydown="return Search_KeyPress(event);" type="text" value="Enter your query here"/>
</div>
<div class="common-search-button-container" onclick="return btnSearch_Click(event);">
<img alt="Search Title" src="https://images.bookwire.com/App_Themes/Images/search_mag_glass.jpg?4.0.4.009" title="Search Title"/>
</div>
</div>
</div>
</div>
<div class="main_middle">
<div class="content_area">
<div class="content_left">
<div class="topBlockAuthor">
<div class="divAuthorNameHeader">
<h1 class="H1TopTitleSpan" id="ContentPlaceHolder_Main_authorNameHeader">John Peel</h1>
<div class="divTopAuthorBioDetails" id="ContentPlaceHolder_Main_divTopAuthorBioDetails">John Peel is the author of "The Zanti Misfits", "Suddenly Twins!", "Lost in Vegas!" and "The Death of Princes".</div>
</div>
<div class="button_container">
</div>
</div>
<br/>
<div style="clear: both">
</div>
<div class="side_block_rows" id="ContentPlaceHolder_Main_divRowHeaderBooksBy">
<div class="row_header">
<div class="row_header_tab">
<span id="ContentPlaceHolder_Main_lblHeaderBooksBy" title="John Peel">Books by John Peel</span>
</div>
<div class="row_header_link">
<a href="https://www.bookwire.com/books/author/John-Peel/Books-By?authorId=62580" id="ContentPlaceHolder_Main_lnkBooksBy">View more</a>
</div>
</div>
<div class="book_container_row">
<div>
<table cellspacing="0" class="NewTitles" id="ContentPlaceHolder_Main_dlBooksBy">
<tr>
<td>
<div>
<div class="outer">
<div class="transform">
<a class="titleslink" href="https://www.bookwire.com/book/USA/Book-of-Magic-9780738706153-John-Peel-9516624" title="Book of Magic">
<img alt="Book Cover" class="imgcover" src="https://secure.syndetics.com/index.aspx?isbn=9780738706153/mc.gif&amp;client=bipsite&amp;type=nocover&amp;imagelinking=1" style="width: 98px; height: 149px; margin-top: 2px; margin-bottom: 1px;
                                                    margin-left: 2px;"/>
</a>
</div>
</div>
</div>
<div class="titleContentBox float-left">
<a class="titleslink" href="https://www.bookwire.com/book/USA/Book-of-Magic-9780738706153-John-Peel-9516624" title="Book of Magic">
<strong>
                                            Book of Magic</strong> </a>
<br/>
<div class="divRatingStar" id="ContentPlaceHolder_Main_dlBooksBy_divRating_0" style="display:none">
<input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/>
</div>
<span title="Peel, John">Peel, John</span>
<br/>
                                    Paperback: <span class="priceSpan">
                                        $4.99</span>
</div>
</td><td>
<div>
<div class="outer">
<div class="transform">
<a class="titleslink" href="https://www.bookwire.com/book/USA/The-Zanti-Misfits-9780812590630-John-Peel-47419453" title="The Zanti Misfits">
<img alt="Book Cover" class="imgcover" src="https://secure.syndetics.com/index.aspx?isbn=9780812590630/mc.gif&amp;client=bipsite&amp;type=nocover&amp;imagelinking=1" style="width: 98px; height: 149px; margin-top: 2px; margin-bottom: 1px;
                                                    margin-left: 2px;"/>
</a>
</div>
</div>
</div>
<div class="titleContentBox float-left">
<a class="titleslink" href="https://www.bookwire.com/book/USA/The-Zanti-Misfits-9780812590630-John-Peel-47419453" title="The Zanti Misfits">
<strong>
                                            The Zanti Misfits</strong> </a>
<br/>
<div class="divRatingStar" id="ContentPlaceHolder_Main_dlBooksBy_divRating_1" style="display:none">
<input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/>
</div>
<span title="Peel, John">Peel, John</span>
<br/>
                                    Paperback: <span class="priceSpan">
                                        $3.99</span>
</div>
</td><td>
<div>
<div class="outer">
<div class="transform">
<a class="titleslink" href="https://www.bookwire.com/book/USA/The-Innocent-9780812564556-John-Peel-47529773" title="The Innocent">
<img alt="Book Cover" class="imgcover" src="https://secure.syndetics.com/index.aspx?isbn=9780812564556/mc.gif&amp;client=bipsite&amp;type=nocover&amp;imagelinking=1" style="width: 98px; height: 149px; margin-top: 2px; margin-bottom: 1px;
                                                    margin-left: 2px;"/>
</a>
</div>
</div>
</div>
<div class="titleContentBox float-left">
<a class="titleslink" href="https://www.bookwire.com/book/USA/The-Innocent-9780812564556-John-Peel-47529773" title="The Innocent">
<strong>
                                            The Innocent</strong> </a>
<br/>
<div class="divRatingStar" id="ContentPlaceHolder_Main_dlBooksBy_divRating_2" style="display:none">
<input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/>
</div>
<span title="Peel, John">Peel, John</span>
<br/>
                                    Paperback: <span class="priceSpan">
                                        $3.99</span>
</div>
</td><td>
<div>
<div class="outer">
<div class="transform">
<a class="titleslink" href="https://www.bookwire.com/book/USA/Revolution-9780439060332-John-Peel-6150833" title="Revolution">
<img alt="Book Cover" class="imgcover" src="https://secure.syndetics.com/index.aspx?isbn=9780439060332/mc.gif&amp;client=bipsite&amp;type=nocover&amp;imagelinking=1" style="width: 98px; height: 149px; margin-top: 2px; margin-bottom: 1px;
                                                    margin-left: 2px;"/>
</a>
</div>
</div>
</div>
<div class="titleContentBox float-left">
<a class="titleslink" href="https://www.bookwire.com/book/USA/Revolution-9780439060332-John-Peel-6150833" title="Revolution">
<strong>
                                            Revolution</strong> </a>
<br/>
<div class="divRatingStar" id="ContentPlaceHolder_Main_dlBooksBy_divRating_3" style="display:none">
<input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/>
</div>
<span title="Peel, John">Peel, John</span>
<br/>
                                    Paperback: <span class="priceSpan">
                                        $4.99</span>
</div>
</td>
</tr>
</table>
</div>
<div>
<table cellspacing="0" class="NewTitles" id="ContentPlaceHolder_Main_dlBooksBySecondRow">
<tr>
<td>
<div>
<div class="outer">
<div class="transform">
<a class="titleslink" href="https://www.bookwire.com/book/USA/Book-of-Thunder-9780738706146-John-Peel-9516622" title="Book of Thunder">
<img alt="Book Cover" class="imgcover" src="https://secure.syndetics.com/index.aspx?isbn=9780738706146/mc.gif&amp;client=bipsite&amp;type=nocover&amp;imagelinking=1" style="width: 98px; height: 149px; margin-top: 2px; margin-bottom: 1px;
                                                    margin-left: 2px;"/>
</a>
</div>
</div>
</div>
<div class="titleContentBox float-left">
<a class="titleslink" href="https://www.bookwire.com/book/USA/Book-of-Thunder-9780738706146-John-Peel-9516622" title="Book of Thunder">
<strong>
                                            Book of Thunder</strong> </a>
<br/>
<div class="divRatingStar" id="ContentPlaceHolder_Main_dlBooksBySecondRow_divRating_0" style="display:none">
<input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/>
</div>
<span title="Peel, John">Peel, John</span>
<br/>
                                    Paperback: <span class="priceSpan">
                                        $5.99</span>
</div>
</td><td>
<div>
<div class="outer">
<div class="transform">
<a class="titleslink" href="https://www.bookwire.com/book/USA/Lost-in-Vegas-9780671007102-John-Peel-47592813" title="Lost in Vegas!">
<img alt="Book Cover" class="imgcover" src="https://secure.syndetics.com/index.aspx?isbn=9780671007102/mc.gif&amp;client=bipsite&amp;type=nocover&amp;imagelinking=1" style="width: 98px; height: 149px; margin-top: 2px; margin-bottom: 1px;
                                                    margin-left: 2px;"/>
</a>
</div>
</div>
</div>
<div class="titleContentBox float-left">
<a class="titleslink" href="https://www.bookwire.com/book/USA/Lost-in-Vegas-9780671007102-John-Peel-47592813" title="Lost in Vegas!">
<strong>
                                            Lost in Vegas!</strong> </a>
<br/>
<div class="divRatingStar" id="ContentPlaceHolder_Main_dlBooksBySecondRow_divRating_1" style="display:none">
<input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/>
</div>
<span title="Peel, John">Peel, John</span>
<br/>
                                    Paperback: <span class="priceSpan">
                                        $3.99</span>
</div>
</td><td>
<div>
<div class="outer">
<div class="transform">
<a class="titleslink" href="https://www.bookwire.com/book/USA/Suddenly-Twins-9780743417624-John-Peel-7055795" title="Suddenly Twins!">
<img alt="Book Cover" class="imgcover" src="https://secure.syndetics.com/index.aspx?isbn=9780743417624/mc.gif&amp;client=bipsite&amp;type=nocover&amp;imagelinking=1" style="width: 98px; height: 149px; margin-top: 2px; margin-bottom: 1px;
                                                    margin-left: 2px;"/>
</a>
</div>
</div>
</div>
<div class="titleContentBox float-left">
<a class="titleslink" href="https://www.bookwire.com/book/USA/Suddenly-Twins-9780743417624-John-Peel-7055795" title="Suddenly Twins!">
<strong>
                                            Suddenly Twins!</strong> </a>
<br/>
<div class="divRatingStar" id="ContentPlaceHolder_Main_dlBooksBySecondRow_divRating_2" style="display:none">
<input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/>
</div>
<span title="Peel, John">Peel, John</span>
<br/>
                                    Paperback: <span class="priceSpan">
                                        $4.99</span>
</div>
</td><td>
<div>
<div class="outer">
<div class="transform">
<a class="titleslink" href="https://www.bookwire.com/book/USA/The-Death-of-Princes-9780671568085-John-Peel-47181582" title="The Death of Princes">
<img alt="Book Cover" class="imgcover" src="https://secure.syndetics.com/index.aspx?isbn=9780671568085/mc.gif&amp;client=bipsite&amp;type=nocover&amp;imagelinking=1" style="width: 98px; height: 149px; margin-top: 2px; margin-bottom: 1px;
                                                    margin-left: 2px;"/>
</a>
</div>
</div>
</div>
<div class="titleContentBox float-left">
<a class="titleslink" href="https://www.bookwire.com/book/USA/The-Death-of-Princes-9780671568085-John-Peel-47181582" title="The Death of Princes">
<strong>
                                            The Death of Prin...</strong> </a>
<br/>
<div class="divRatingStar" id="ContentPlaceHolder_Main_dlBooksBySecondRow_divRating_3" style="display:none">
<input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/>
</div>
<span title="Peel, John">Peel, John</span>
<br/>
                                    Paperback: <span class="priceSpan">
                                        $5.99</span>
</div>
</td>
</tr>
</table>
</div>
<div>
<table cellspacing="0" class="NewTitles" id="ContentPlaceHolder_Main_dlBooksByThirdRow">
<tr>
<td>
<div>
<div class="outer">
<div class="transform">
<a class="titleslink" href="https://www.bookwire.com/book/USA/Tag-9780140360530-John-Peel-1561436" title="Tag">
<img alt="Book Cover" class="imgcover" src="https://secure.syndetics.com/index.aspx?isbn=9780140360530/mc.gif&amp;client=bipsite&amp;type=nocover&amp;imagelinking=1" style="width: 98px; height: 149px; margin-top: 2px; margin-bottom: 1px;
                                                    margin-left: 2px;"/>
</a>
</div>
</div>
</div>
<div class="titleContentBox float-left">
<a class="titleslink" href="https://www.bookwire.com/book/USA/Tag-9780140360530-John-Peel-1561436" title="Tag">
<strong>
                                            Tag</strong> </a>
<br/>
<div class="divRatingStar" id="ContentPlaceHolder_Main_dlBooksByThirdRow_divRating_0" style="display:none">
<input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/>
</div>
<span title="Peel, John">Peel, John</span>
<br/>
                                    Paperback: <span class="priceSpan">
                                        $2.99</span>
</div>
</td><td>
<div>
<div class="outer">
<div class="transform">
<a class="titleslink" href="https://www.bookwire.com/book/USA/The-Last-Drop-9780671535308-John-Peel-1968222" title="The Last Drop">
<img alt="Book Cover" class="imgcover" src="https://secure.syndetics.com/index.aspx?isbn=9780671535308/mc.gif&amp;client=bipsite&amp;type=nocover&amp;imagelinking=1" style="width: 98px; height: 149px; margin-top: 2px; margin-bottom: 1px;
                                                    margin-left: 2px;"/>
</a>
</div>
</div>
</div>
<div class="titleContentBox float-left">
<a class="titleslink" href="https://www.bookwire.com/book/USA/The-Last-Drop-9780671535308-John-Peel-1968222" title="The Last Drop">
<strong>
                                            The Last Drop</strong> </a>
<br/>
<div class="divRatingStar" id="ContentPlaceHolder_Main_dlBooksByThirdRow_divRating_1" style="display:none">
<input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/>
</div>
<span title="Peel, John">Peel, John</span>
<br/>
                                    Paperback: <span class="priceSpan">
                                        $3.99</span>
</div>
</td><td>
<div>
<div class="outer">
<div class="transform">
<a class="titleslink" href="https://www.bookwire.com/book/USA/Book-of-Dead-Things-9780977985630-Tina-L-Jens-14025397" title="Book of Dead Things">
<img alt="Book Cover" class="imgcover" src="https://secure.syndetics.com/index.aspx?isbn=9780977985630/mc.gif&amp;client=bipsite&amp;type=nocover&amp;imagelinking=1" style="width: 98px; height: 149px; margin-top: 2px; margin-bottom: 1px;
                                                    margin-left: 2px;"/>
</a>
</div>
</div>
</div>
<div class="titleContentBox float-left">
<a class="titleslink" href="https://www.bookwire.com/book/USA/Book-of-Dead-Things-9780977985630-Tina-L-Jens-14025397" title="Book of Dead Things">
<strong>
                                            Book of Dead Thin...</strong> </a>
<br/>
<div class="divRatingStar" id="ContentPlaceHolder_Main_dlBooksByThirdRow_divRating_2" style="display:none">
<input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/>
</div>
<span title="Goldman, Kenneth">Goldman, Kenneth</span>
<br/>
                                    Paperback: <span class="priceSpan">
                                        $18.00</span>
</div>
</td><td>
<div>
<div class="outer">
<div class="transform">
<a class="titleslink" href="https://www.bookwire.com/book/USA/The-Death-of-Princes-9780743422888-John-Peel-19501863" title="The Death of Princes">
<img alt="Book Cover" class="imgcover" src="https://secure.syndetics.com/index.aspx?isbn=9780743422888/mc.gif&amp;client=bipsite&amp;type=nocover&amp;imagelinking=1" style="width: 98px; height: 149px; margin-top: 2px; margin-bottom: 1px;
                                                    margin-left: 2px;"/>
</a>
</div>
</div>
</div>
<div class="titleContentBox float-left">
<a class="titleslink" href="https://www.bookwire.com/book/USA/The-Death-of-Princes-9780743422888-John-Peel-19501863" title="The Death of Princes">
<strong>
                                            The Death of Prin...</strong> </a>
<br/>
<div class="divRatingStar" id="ContentPlaceHolder_Main_dlBooksByThirdRow_divRating_3" style="display:none">
<input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/><input class="emptyRating" type="button"/>
</div>
<span title="Peel, John">Peel, John</span>
<br/>
                                    Electronic book text: <span class="priceSpan">
</span>
</div>
</td>
</tr>
</table>
</div>
</div>
</div>
<br/>
<br/>
</div>
<div class="content_right">
</div>
<div class="clear_util">
</div>
</div>
</div>
<div class="footer">
<a href="mailto:isbn-san@bowker.com?subject=bookwire" style="color: white;">Contact</a>
                | <a href="http://www.proquest.com/about/terms-and-conditions.html" style="color: white;" target="_blank">
                    Terms of Use</a> | <a href="http://www.bowker.com/en-US/site/privacy.shtml" style="color: white;" target="_blank">
                        Privacy Policy</a>
<br/>
                Copyright ® 2013 R.R. Bowker LLC. All rights reserved.
                <div id="consent_blackbar"></div>
<div id="teconsent"></div>
<script async="async" crossorigin="" src="//consent.truste.com/notice?domain=proquestproduct.com&amp;c=teconsent&amp;js=nj&amp;noticeType=bb"></script>
</div>
</div>
</div>
<input id="hdnSearchType" name="ctl00$hdnSearchType" type="hidden" value="quicksearch-all"/>
<input id="hdnSearchTerm" name="ctl00$hdnSearchTerm" type="hidden"/>
<input id="hdnSearchTypeIndex" name="ctl00$hdnSearchTypeIndex" type="hidden"/>
<input id="actElem" type="hidden"/>
<!--     IPAddress:52.71.11.190, IP-0AF23A45 -->
</form>
</body>
</html>
