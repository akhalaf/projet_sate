<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]--><!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]--><!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]--><!--[if gt IE 8]><!--><html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="LiveWhale" name="generator"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width" name="viewport"/>
<meta content="" name="id"/>
<meta content="#FCF8ED" name="msapplication-TileColor"/>
<meta content="/images/favicon.png" name="msapplication-TileImage"/>
<meta content="12/09/2015 10:59am EST" name="lw-template-updated"/>
<title>: No Page Found</title>
<script>
	/*<![CDATA[*/
	var livewhale={"liveurl_dir":"\/live","host":"www.alma.edu","page":"\/templates\/details\/404.php","request":"\/athletics\/football\/Coach_Leister","ajax_timeout":30,"client_name":"Alma College","cookie_host":".alma.edu","cookie_prefix":"lw_7efb4216_","date_format_us":"m\/d\/Y","date_format_euro":"d-m-Y","time_format_us":"g:ia","time_format_euro":"H:i","timezone_format":"us","has_ssl":"true","group_id":"","group_title":"","group_title_real":"","group_fullname":"","group_fullname_real":"","group_twitter_name":"","group_facebook_name":"","group_instagram_name":"","group_directory":"","qa_revision":1573702022,"theme":"alma"};
	/*]]>*/
	</script>
<link href="/live/resource/css/livewhale/theme/core/styles/common.rev.1574356560.css" rel="stylesheet" type="text/css"/>
<link href="/live/resource/css/%5Clivewhale%5Cstyles%5Clwui%5Clw-overlay.css/%5Clivewhale%5Cstyles%5Clwui%5Clw-slideshow.css/%5Clivewhale%5Cstyles%5Clwui%5Clw-timepicker.rev.1574356560.css" rel="stylesheet" type="text/css"/>
<link href="/live/resource/css/_i/themes/alma/styles/widgets.rev.1595000448.css" rel="stylesheet" type="text/css"/>
<link href="/live/resource/css/livewhale/theme/core/styles/frontend.rev.1574356560.css" rel="stylesheet" type="text/css"/>
<link href="/live/resource/css/%5C_i%5Cthemes%5Calma%5Cstyles%5Calmacustom.css/%5C_i%5Cthemes%5Calma%5Cstyles%5Cback-to-top.css/%5C_i%5Cthemes%5Calma%5Cstyles%5Cbackend.css/%5C_i%5Cthemes%5Calma%5Cstyles%5Cbootstrap.css/%5C_i%5Cthemes%5Calma%5Cstyles%5Cextra.css/%5C_i%5Cthemes%5Calma%5Cstyles%5Cihsi.css/%5C_i%5Cthemes%5Calma%5Cstyles%5CminEmoji.css/%5C_i%5Cthemes%5Calma%5Cstyles%5Csocial.css/%5C_i%5Cthemes%5Calma%5Cstyles%5Ctemplates.rev.1586893586.css" rel="stylesheet" type="text/css"/>
<link href="/styles/alma.css" rel="stylesheet" type="text/css"/>
<link href="/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<script src="/scripts/modernizr.js"></script>
<script src="https://use.fontawesome.com/4ae4311d8d.js"></script>
<script async="async" src="https://admissions.alma.edu/ping">/**/</script>
<link href="https://www.alma.edu/live/rss/news/header/Plaid%20Works/max/10/group/Newsroom/exclude_tag/exclude/require_image/true/class/plaid-works-news/class/cf/format_widget/%3Cimg%20style%3D%22display%3A%20none%3B%22%20src%3D%22https%3Awww.alma.eduliveimagegid12width973height4083202_chapel_panonew2.rev.1386267003.jpg%22%20data-share-image%3D%22https%3Awww.alma.eduliveimagegid12width973height4083202_chapel_panonew2.rev.1386267003.jpg%22%3E%3C%21--%20decorative%20--%3E" rel="alternate" title="News: Plaid Works" type="application/rss+xml"/>
<link href="https://www.alma.edu/athletics/football/Coach_Leister" rel="canonical"/>
<script>
	/*<![CDATA[*/
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-2556394-1', 'auto', {'name': 'main', 'allowLinker': true});
	ga('require', 'linker');ga('linker:autoLink', ['goalmacollege.org'] );
	ga('main.send', 'pageview');
	/*]]>*/
	</script>
<!--[if lt IE 9]><script src="/live/resource/js/livewhale/thirdparty/html5shiv.rev.1510102491.js"></script><![endif]-->
<style>
#lw-inline-style-4e649e4a {display:none;visibility:hidden;width:0px;height:0px;}
</style>
</head>
<body class="body_404 intro_hidden body_templates">
<!-- Google Tag Manager -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="async" src="https://www.googletagmanager.com/gtag/js?id=UA-2556394-1">
</script>
<script>/*<![CDATA[*/
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-2556394-1');
/*]]>*/</script>
<script>/*<![CDATA[*/
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KLDQCJR');
/*]]>*/</script> <noscript><iframe id="lw-inline-style-4e649e4a" src="https://www.googletagmanager.com/ns.html?id=GTM-KLDQCJR"></iframe></noscript> <!-- End Google Tag Manager -->
<header aria-label="site header" class="siteheader" id="siteheader" role="banner">
<div class="skipnav">
<a href="#page">Skip to main content</a>
</div>
<nav aria-label="audience navigation" class="container cf" id="audience-nav">
<div class="site-logo">
<a class="ir" href="/" title="Alma College"><img alt="Alma College Logo" src="/live/resource/image/images/common/bg-header-logo.rev.1510102390.png"/></a>
</div>
<div class="header-search-form">
<div class="wrapper">
<form action="/search/" id="quick-access-form" method="get">
<div class="input-group">
<input aria-label="search alma.edu" autocomplete="off" class="lw_qa_page" data-qa-url="/includes/quickaccess.php" id="quickquery" name="q" placeholder="Search alma.edu" type="text" value=""/> <button aria-label="search" class="btn search-btn" type="submit"><span class="fa fa-search"></span></button>
</div>
</form>
<div class="js-qa-results qa_blur">
</div>
<div class="corner left">
<div>
</div>
</div>
<div class="corner right">
<div>
</div>
</div>
</div>
</div>
<div class="lw_widget lw_widget_type_navigation lw_widget_navigation audience_nav" id="lw_widget_da32e914">
<ul class="lw_widget_results lw_widget_results_navigation">
<li class="lw_item_1 lw_files_php lw_item_b75e5008 lw_section">
<a href="/gateways/students/">Students</a>
</li>
<li class="lw_item_2 lw_files_php lw_item_9f37f83b lw_section">
<a href="/gateways/faculty-staff/">Faculty &amp; Staff</a>
</li>
<li class="lw_item_3 lw_files_php lw_item_643a5fb8 lw_section">
<a href="/gateways/parents/">Parents &amp; Family</a>
</li>
<li class="lw_item_4 lw_files_php lw_item_6ec2861f lw_section">
<a href="/gateways/alumni/">Alumni</a>
</li>
<li class="lw_item_5 lw_files_php lw_item_007a22cc lw_section">
<a href="/gateways/news-events/">News &amp; Events</a>
</li>
<li class="lw_item_6 lw_files_php lw_item_771b0e6b">
<a href="/giving/">Giving</a>
</li>
</ul><span class="lw_widget_syntax lw_hidden" title='&lt;widget type="navigation"&gt;&lt;arg id="id"&gt;65&lt;/arg&gt;&lt;arg id="class"&gt;audience_nav&lt;/arg&gt;&lt;/widget&gt;'></span>
</div>
</nav>
<div class="main-nav frame">
<nav aria-label="main navigation" class="container" id="main-nav">
<!-- main navigation -->
<div class="lw_widget lw_widget_type_navigation lw_widget_navigation main_nav" id="lw_widget_d4db7d48">
<ul class="lw_widget_results lw_widget_results_navigation">
<li class="lw_item_1 lw_files_php lw_item_4fb27af2">
<a href="/about/">About</a>
</li>
<li class="lw_item_2 lw_files_php lw_item_c003223f lw_section">
<a href="/admissions/">Admissions &amp; Aid</a>
</li>
<li class="lw_item_9 lw_files_php lw_item_70ea5f1b">
<a href="/academics/">Academics</a>
</li>
<li class="lw_item_10 lw_files_php lw_item_bf91e44e">
<a href="/student-life/">Life at Alma</a>
</li>
<li class="lw_item_11 lw_item_dd927c2a">
<a class="lw_external_url" href="http://www.almascots.com" target="_blank">Athletics</a>
</li>
<li class="lw_item_12 lw_files_php lw_item_63f94354">
<a href="/academics/support/center-for-student-opportunity/career-services/career-outcomes/">Success after Alma</a>
</li>
</ul><span class="lw_widget_syntax lw_hidden" title='&lt;widget type="navigation"&gt;&lt;arg id="id"&gt;64&lt;/arg&gt;&lt;arg id="class"&gt;main_nav&lt;/arg&gt;&lt;/widget&gt;'></span>
</div>
</nav>
</div>
</header>
<div class="content cf" id="page">
<div class="container cf">
<article>
<div id="custom-404">
<!--     <div class="search-form-404">
      <div class="wrapper">
        <form id="quick-access-form-404" method="get" action="/search/">
          
          <input type="text" aria-label="search alma.edu" class="lw_qa_widget_47" name="q" placeholder="Try a Search!" id="quickquery-404" value="" autocomplete="off"/>
          
        </form>
        <div class="js-qa-results qa_blur"></div>
      </div>
    </div> -->
<div class="search-form-404">
<div class="wrapper">
<form action="/search/" id="quick-access-form-404" method="get">
<div class="input-group">
<input aria-label="search alma.edu" autocomplete="off" class="lw_qa_widget_47" id="quickquery-404" name="q" placeholder="Try a search!" type="text" value=""/>
<button aria-label="search" class="btn search-btn" type="submit">
<span class="fa fa-search"></span>
</button>
</div>
</form>
<div class="js-qa-results qa_blur"></div>
</div>
</div>
</div>
<div class="editable optional lw_hidden" id="intro">
</div>
<div class="editable" id="main">
</div>
</article>
</div>
</div><div class="cf" id="plaid-works-news-wrapper">
<div class="frame">
<img alt="" data-share-image="https://www.alma.edu/live/image/gid/12/width/973/height/408/3202_chapel_panonew2.rev.1386267003.jpg" src="/live/image/gid/12/width/973/height/408/3202_chapel_panonew2.rev.1386267003.jpg" style="display: none;"/><!-- decorative --><div class="lw_widget lw_widget_type_news lw_widget_news lw_widget_id_62 lw_widget_footer_plaid_works_news plaid-works-news cf" id="lw_widget_e88f71bc"><h3>Plaid Works</h3><ul class="lw_widget_results lw_widget_results_news"><li class="lw_item_1 lw_has_image"><span class="lw_news_image lw_item_thumb lw_has_image"><a href="/live/news/2616-alma-college-fall-2020-deans-list" title="Alma College Fall 2020 Dean’s List"><img alt="Alma College Fall 2020 Dean’s List" class="lw_image" data-max-h="790" data-max-w="903" height="150" src="/live/image/gid/22/width/150/height/150/crop/1/src_region/0,0,903,790/21696_Street_banner.rev.1610469383.jpg" srcset="/live/image/scale/2x/gid/22/width/150/height/150/crop/1/src_region/0,0,903,790/21696_Street_banner.rev.1610469383.jpg 2x, /live/image/scale/3x/gid/22/width/150/height/150/crop/1/src_region/0,0,903,790/21696_Street_banner.rev.1610469383.jpg 3x" width="150"/></a></span><div class="lw_news_headline"><a href="/live/news/2616-alma-college-fall-2020-deans-list">Alma College Fall 2020 Dean’s List</a></div></li><li class="lw_item_2 lw_has_image"><span class="lw_news_image lw_item_thumb lw_has_image"><a href="/live/news/2615-mlk-day-your-way-to-offer-students-varied-ways-to" title="‘MLK Day: Your Way’ to Offer Students Varied Ways to Celebrate and Reflect"><img alt="‘MLK Day: Your Way’ to Offer Students Varied Ways to Celebrate and Reflect" class="lw_image" data-max-h="3200" data-max-w="2936" height="150" src="/live/image/gid/22/width/150/height/150/crop/1/src_region/0,0,2936,3200/21690_Copy_of_C5DM3563.rev.1610371911.jpg" srcset="/live/image/scale/2x/gid/22/width/150/height/150/crop/1/src_region/0,0,2936,3200/21690_Copy_of_C5DM3563.rev.1610371911.jpg 2x, /live/image/scale/3x/gid/22/width/150/height/150/crop/1/src_region/0,0,2936,3200/21690_Copy_of_C5DM3563.rev.1610371911.jpg 3x" width="150"/></a></span><div class="lw_news_headline"><a href="/live/news/2615-mlk-day-your-way-to-offer-students-varied-ways-to">‘MLK Day: Your Way’ to Offer Students Varied Ways to Celebrate and Reflect</a></div></li><li class="lw_item_3 lw_has_image"><span class="lw_news_image lw_item_thumb lw_has_image"><a href="/live/news/2600-alma-college-igem-team-examines-soil-cleanup-at" title="Alma College iGEM team Examines Soil Cleanup at Local Superfund Site"><img alt="Alma College iGEM team Examines Soil Cleanup at Local Superfund Site" class="lw_image" data-max-h="1476" data-max-w="1318" height="150" src="/live/image/gid/22/width/150/height/150/crop/1/src_region/0,0,1318,1476/21662_Screen_Shot_2020-12-14_at_12.47.22_PM.rev.1609776269.jpg" srcset="/live/image/scale/2x/gid/22/width/150/height/150/crop/1/src_region/0,0,1318,1476/21662_Screen_Shot_2020-12-14_at_12.47.22_PM.rev.1609776269.jpg 2x, /live/image/scale/3x/gid/22/width/150/height/150/crop/1/src_region/0,0,1318,1476/21662_Screen_Shot_2020-12-14_at_12.47.22_PM.rev.1609776269.jpg 3x" width="150"/></a></span><div class="lw_news_headline"><a href="/live/news/2600-alma-college-igem-team-examines-soil-cleanup-at">Alma College iGEM team Examines Soil Cleanup at Local Superfund Site</a></div></li><li class="lw_item_4 lw_has_image"><span class="lw_news_image lw_item_thumb lw_has_image"><a href="/live/news/2596-alma-college-student-wins-prize-in-sodexo-contest" title="Alma College Student Wins Prize in Sodexo Contest"><img alt="Alma College Student Wins Prize in Sodexo Contest" class="lw_image" data-max-h="955" data-max-w="700" height="150" src="/live/image/gid/22/width/150/height/150/crop/1/src_region/0,0,700,955/21635_callie_hale.rev.1607952202.jpg" srcset="/live/image/scale/2x/gid/22/width/150/height/150/crop/1/src_region/0,0,700,955/21635_callie_hale.rev.1607952202.jpg 2x, /live/image/scale/3x/gid/22/width/150/height/150/crop/1/src_region/0,0,700,955/21635_callie_hale.rev.1607952202.jpg 3x" width="150"/></a></span><div class="lw_news_headline"><a href="/live/news/2596-alma-college-student-wins-prize-in-sodexo-contest">Alma College Student Wins Prize in Sodexo Contest</a></div></li><li class="lw_item_5 lw_has_image"><span class="lw_news_image lw_item_thumb lw_has_image"><a href="/live/news/2593-alma-college-wins-15000-michigan-humanities-grant" title="Alma College Wins $15,000 Michigan Humanities Grant"><img alt="Alma College Wins $15,000 Michigan Humanities Grant" class="lw_image" data-max-h="3200" data-max-w="2133" height="150" src="/live/image/gid/22/width/150/height/150/crop/1/src_region/0,0,2133,3200/21626_AlmaC_101520_sjp1415.rev.1607606028.jpg" srcset="/live/image/scale/2x/gid/22/width/150/height/150/crop/1/src_region/0,0,2133,3200/21626_AlmaC_101520_sjp1415.rev.1607606028.jpg 2x, /live/image/scale/3x/gid/22/width/150/height/150/crop/1/src_region/0,0,2133,3200/21626_AlmaC_101520_sjp1415.rev.1607606028.jpg 3x" width="150"/></a></span><div class="lw_news_headline"><a href="/live/news/2593-alma-college-wins-15000-michigan-humanities-grant">Alma College Wins $15,000 Michigan Humanities Grant</a></div></li><li class="lw_item_6 lw_has_image"><span class="lw_news_image lw_item_thumb lw_has_image"><a href="/live/news/2589-science-students-study-potential-effects-of-road" title="Science Students Study Potential Effects of Road Salts on Environment"><img alt="Science Students Study Potential Effects of Road Salts on Environment" class="lw_image" data-max-h="3200" data-max-w="2400" height="150" src="/live/image/gid/22/width/150/height/150/crop/1/src_region/0,0,2400,3200/21620_IMG_2592.rev.1607453148.jpg" srcset="/live/image/scale/2x/gid/22/width/150/height/150/crop/1/src_region/0,0,2400,3200/21620_IMG_2592.rev.1607453148.jpg 2x, /live/image/scale/3x/gid/22/width/150/height/150/crop/1/src_region/0,0,2400,3200/21620_IMG_2592.rev.1607453148.jpg 3x" width="150"/></a></span><div class="lw_news_headline"><a href="/live/news/2589-science-students-study-potential-effects-of-road">Science Students Study Potential Effects of Road Salts on Environment</a></div></li><li class="lw_item_7 lw_has_image"><span class="lw_news_image lw_item_thumb lw_has_image"><a href="/live/news/2588-college-hopes-to-flush-out-covid-19-cases-on" title="College Hopes to ‘Flush Out’ COVID-19 Cases on Campus"><img alt="College Hopes to ‘Flush Out’ COVID-19 Cases on Campus" class="lw_image" data-max-h="3200" data-max-w="2400" height="150" src="/live/image/gid/22/width/150/height/150/crop/1/src_region/0,0,2400,3200/21619_wastewater1.rev.1607445994.jpg" srcset="/live/image/scale/2x/gid/22/width/150/height/150/crop/1/src_region/0,0,2400,3200/21619_wastewater1.rev.1607445994.jpg 2x, /live/image/scale/3x/gid/22/width/150/height/150/crop/1/src_region/0,0,2400,3200/21619_wastewater1.rev.1607445994.jpg 3x" width="150"/></a></span><div class="lw_news_headline"><a href="/live/news/2588-college-hopes-to-flush-out-covid-19-cases-on">College Hopes to ‘Flush Out’ COVID-19 Cases on Campus</a></div></li><li class="lw_item_8 lw_has_image"><span class="lw_news_image lw_item_thumb lw_has_image"><a href="/live/news/2587-alma-colleges-oldest-book-discovered-in-special" title="Alma College’s Oldest Book Discovered in Special Collections Library"><img alt="Alma College’s Oldest Book Discovered in Special Collections Library" class="lw_image" data-max-h="3200" data-max-w="2400" height="150" src="/live/image/gid/22/width/150/height/150/crop/1/src_region/0,0,2400,3200/21613_IMG_2723.rev.1607365016.jpg" srcset="/live/image/scale/2x/gid/22/width/150/height/150/crop/1/src_region/0,0,2400,3200/21613_IMG_2723.rev.1607365016.jpg 2x, /live/image/scale/3x/gid/22/width/150/height/150/crop/1/src_region/0,0,2400,3200/21613_IMG_2723.rev.1607365016.jpg 3x" width="150"/></a></span><div class="lw_news_headline"><a href="/live/news/2587-alma-colleges-oldest-book-discovered-in-special">Alma College’s Oldest Book Discovered in Special Collections Library</a></div></li><li class="lw_item_9 lw_has_image"><span class="lw_news_image lw_item_thumb lw_has_image"><a href="/live/news/2571-education-department-receives-recognition-from" title="Education Department Receives Recognition from Accreditation Group"><img alt="Education Department Receives Recognition from Accreditation Group" class="lw_image" data-max-h="1400" data-max-w="2108" height="150" src="/live/image/gid/22/width/150/height/150/crop/1/src_region/0,0,2108,1400/21507_Screen_Shot_2020-11-09_at_4.23.24_PM.rev.1605025421.png" srcset="/live/image/scale/2x/gid/22/width/150/height/150/crop/1/src_region/0,0,2108,1400/21507_Screen_Shot_2020-11-09_at_4.23.24_PM.rev.1605025421.png 2x, /live/image/scale/3x/gid/22/width/150/height/150/crop/1/src_region/0,0,2108,1400/21507_Screen_Shot_2020-11-09_at_4.23.24_PM.rev.1605025421.png 3x" width="150"/></a></span><div class="lw_news_headline"><a href="/live/news/2571-education-department-receives-recognition-from">Education Department Receives Recognition from Accreditation Group</a></div></li><li class="lw_item_10 lw_has_image"><span class="lw_news_image lw_item_thumb lw_has_image"><a href="/live/news/2572-alma-college-set-to-host-open-house-for-new-mfa" title="Alma College Set to Host Open House for New MFA Program"><img alt="Alma College Set to Host Open House for New MFA Program" class="lw_image" data-max-h="2129" data-max-w="3200" height="150" src="/live/image/gid/22/width/150/height/150/crop/1/src_region/0,0,3200,2129/21484_glenn-carstens-peters-npxXWgQ33ZQ-unsplash.rev.1604513671.jpg" srcset="/live/image/scale/2x/gid/22/width/150/height/150/crop/1/src_region/0,0,3200,2129/21484_glenn-carstens-peters-npxXWgQ33ZQ-unsplash.rev.1604513671.jpg 2x, /live/image/scale/3x/gid/22/width/150/height/150/crop/1/src_region/0,0,3200,2129/21484_glenn-carstens-peters-npxXWgQ33ZQ-unsplash.rev.1604513671.jpg 3x" width="150"/></a></span><div class="lw_news_headline"><a href="/live/news/2572-alma-college-set-to-host-open-house-for-new-mfa">Alma College Set to Host Open House for New MFA Program</a></div></li></ul><span class="lw_widget_syntax lw_hidden" data-widget-id="62" title='&lt;widget type="news"&gt;&lt;arg id="header"&gt;Plaid Works&lt;/arg&gt;&lt;arg id="max"&gt;10&lt;/arg&gt;&lt;arg id="paginate"&gt;false&lt;/arg&gt;&lt;arg id="group"&gt;Newsroom&lt;/arg&gt;&lt;arg id="exclude_tag"&gt;exclude&lt;/arg&gt;&lt;arg id="thumb_width"&gt;150&lt;/arg&gt;&lt;arg id="thumb_height"&gt;150&lt;/arg&gt;&lt;arg id="require_image"&gt;true&lt;/arg&gt;&lt;arg id="class"&gt;plaid-works-news&lt;/arg&gt;&lt;arg id="class"&gt;cf&lt;/arg&gt;&lt;arg id="format"&gt;&lt;span class="lw_news_image lw_item_thumb lw_has_image"&gt;&lt;a href="{href}" title="{headline_clean}"&gt;&lt;img src="{image_src}" alt="{headline_clean}" class="lw_image" width="150" height="150"/&gt;&lt;/a&gt;&lt;/span&gt;&lt;div class="lw_news_headline"&gt;{headline}&lt;/div&gt;&lt;/arg&gt;&lt;arg id="format_widget"&gt;&lt;img style="display: none;" src="/live/image/gid/12/width/973/height/408/3202_chapel_panonew2.jpg" data-share-image="https://www.alma.edu/live/image/gid/12/width/973/height/408/3202_chapel_panonew2.jpg"/&gt;{widget}&lt;/arg&gt;&lt;/widget&gt;'></span></div>
</div>
</div> <footer aria-label="footer" class="sitefooter cf">
<div class="frame">
<div class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1a">
<div class="hi-icon-bkgd">
<div class="hi-icon"><a class="footer-quicklink" href="https://www.alma.edu/admissions/">Info</a></div>
<div class="hi-icon"><a class="footer-quicklink" href="https://www.alma.edu/admissions/apply/">Apply</a></div>
<div class="hi-icon"><a class="footer-quicklink" href="https://www.alma.edu/admissions/campus-visits/">Visit</a></div>
</div>
</div>
<div class="container cf">
<div class="address">
<div class="vcard">
<a class="fn org ir" href="/">Alma College</a>
<span class="addr">
<span class="street-address">614 W. Superior St.</span>
<span class="locality">Alma</span>, <span class="region">Michigan</span> <span class="postal-code">48801</span>
</span>
<span class="tel"><a href="tel:+19894637111">(989) 463-7111</a></span>
</div>
<p><a href="/about/contact-us/">Contact Us</a> | <a href="/about/visiting/driving-directions.php">Maps &amp; Directions</a></p>
</div>
<nav aria-label="footer navigation" id="footer-nav">
<div class="news_links">
<span>News &amp; Events</span>
<ul>
<li><a href="/calendar">Campus Calendar</a></li>
<li><a href="/newsroom">Newsroom</a></li>
<li><a href="/offices/registrar/academic-calendar/">Academic Calendar</a></li>
<li><a href="/about/alma-at-a-glance/college-policies-disclosures/">Policies &amp; Disclosures</a></li>
</ul>
</div>
<div class="news_links">
<span>Helpful Links</span>
<ul>
<li><a href="/offices">Offices &amp; Services</a></li>
<li><a href="/offices/human-resources/job-openings/">Employment</a></li>
<li><a href="/offices/communication-and-marketing/emergency-information/">Emergency Info</a></li>
<li><a href="/livewhale/?login&amp;referer=1">Editor Login</a></li>
</ul>
</div>
<div class="plaidworks">
<span>#PlaidWorks</span>
<ul>
<li><a href="http://www.facebook.com/pages/Alma-MI/Alma-College/133636558992">Facebook</a></li>
<li><a href="http://www.twitter.com/almacollege">Twitter</a></li>
<li><a href="http://instagram.com/almacollege">Instagram</a></li>
<li><a href="https://www.youtube.com/almacollege">YouTube</a></li>
</ul>
</div>
</nav>
</div>
</div>
</footer>
<!-- START FOOTER SCRIPTS -->
<script src="/live/resource/js/core%5Cbabel-external-helpers.rev.1574356560.js"></script>
<script src="/live/resource/js/%5Clivewhale%5Cthirdparty%5Cfrontend.js/%5Clivewhale%5Cscripts%5Clwui%5Cjquery.lw-overlay.js/%5Clivewhale%5Cscripts%5Clwui%5Cjquery.lw-slideshow.js/%5Clivewhale%5Cscripts%5Ccommon.rev.1574356560.js"></script>
<script src="/live/resource/js/%5Clivewhale%5Ctheme%5Ccore%5Cscripts%5Cfrontend.js/%5Clivewhale%5Ctheme%5Ccore%5Cscripts%5Cquickaccess.rev.1574356560.js"></script>
<script src="/live/resource/js/%5Clivewhale%5Ctheme%5Cglobal%5Cscripts%5Ccalendar-custom.js/%5Clivewhale%5Ctheme%5Cglobal%5Cscripts%5Cheader.js/%5Clivewhale%5Ctheme%5Cglobal%5Cscripts%5CjMinEmoji.js/%5Clivewhale%5Ctheme%5Cglobal%5Cscripts%5CjRespond.min.js/%5Clivewhale%5Ctheme%5Cglobal%5Cscripts%5Cjquery.color.js/%5Clivewhale%5Ctheme%5Cglobal%5Cscripts%5Cjquery.dotdotdot.min.js/%5Clivewhale%5Ctheme%5Cglobal%5Cscripts%5Cjquery.event.move.js/%5Clivewhale%5Ctheme%5Cglobal%5Cscripts%5Cjquery.smooth-scroll.js/%5Clivewhale%5Ctheme%5Cglobal%5Cscripts%5Csave-and-share.js/%5Clivewhale%5Ctheme%5Cglobal%5Cscripts%5Ctwitter-links.rev.1574363277.js"></script>
<script src="/live/resource/js/%5C_i%5Cthemes%5Calma%5Cscripts%5Caccordion.js/%5C_i%5Cthemes%5Calma%5Cscripts%5Cback-to-top.js/%5C_i%5Cthemes%5Calma%5Cscripts%5Ccampaign.js/%5C_i%5Cthemes%5Calma%5Cscripts%5CcropImages.js/%5C_i%5Cthemes%5Calma%5Cscripts%5Cfooter.js/%5C_i%5Cthemes%5Calma%5Cscripts%5Cisotope.pkgd.min.js/%5C_i%5Cthemes%5Calma%5Cscripts%5Csocial.rev.1574356560.js"></script>
<!-- END FOOTER SCRIPTS -->
</body>
</html>