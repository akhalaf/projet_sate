<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE 8]>			<html class="ie ie8" xmlns="http://www.w3.org/1999/xhtml" lang="pl_PL" xml:lang="pl_PL"> <![endif]--><!--[if IE 9]>			<html class="ie ie9" xmlns="http://www.w3.org/1999/xhtml" lang="pl_PL" xml:lang="pl_PL"> <![endif]--><!--[if gt IE 9]><!--><html lang="pl_PL" xml:lang="pl_PL" xmlns="http://www.w3.org/1999/xhtml"> <!--<![endif]-->
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>P.U.H.P. Ambit sp. z o.o., Stacja Demontażu Pojazdów, Przerób Złomu, Skup Złomu, Sprzedaż Stali, Handel Pojazdami, Kasacja Pojazdów, Złomowanie Pojazdów</title>
<meta content="ambit, firma zajmująca się recyklingiem pojazdów i odpadów" name="description"/>
<meta content="Ambit, ares, pzu, warta, autoczęści, auto części, szrot, złom, powypadkowe, autoholowanie, lora, samochody osobowe, terenowe, dostawcze, ciężarowe, uzywane, z drugiej reki, transport, cena, ceny, najnizsze, bmw, mercedes, land rover, citroen, laweta, auutotransport, powypadkowe, wypadkowe, renault, ford, audi, skup części, sprzedaż części, samochody osobowe, terenowe, dostawcze, ciężarowe, uzywane, z drugiej reki, transport, cena, ceny, najnizsze, bmw, mercedes, land rover, citroen, laweta, auutotransport, powypadkowe, wypadkowe, renault, ford, audi, podzespoły,części  zamienne,silnik,zawieszenie, most,resory,amortyzatory,sprzedaż,autoczęsci,terenowe, Tarmot, Warmot, , A-z Moto, Vazmania, 4x4, Jeep, Humer, off-road, metale, metal, złom, scrap, recykling, recycling,, Kompleksowa obsługa usuwania odpadów przemysłowych, recycling, wrak samochodowy, części samochodowe, zagospodarowanie, ekologia, utylizacja, demontaż wraków, auto kasacja, panta, parkowo, marput, impresja, ares, rozbiór aut, zaswiadczenia, kadilak, alfa romeo, bmw, daewoo, akcesoria, nadwozia, silnik i osprzęt, oświetlenie, układ napędowy, zawieszenie, alternator, amortyzator, aparat zapłonowy, atrapa, belka silnika, cewka, skrzynia, drzwi, drążek, halogen, wyszukiwarka czę¶ci, zakład przerobu złomu, recycling" name="keywords"/>
<meta content="Kioskeo 0.1alfa" name="generator"/>
<meta content="index, follow" name="robots"/>
<meta content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" name="viewport"/>
<link href="/themes/ambit/i/icon.png" rel="icon" type="image/png"/>
<link href="/themes/ambit/i/icon.ico" rel="shortcut icon"/>
<!-- mobile settings -->
<meta content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" name="viewport"/>
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<!-- WEB FONTS : use %7C instead of | (pipe) -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css"/>
<!-- CORE CSS -->
<link href="/themes/ambit/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<!-- SWIPER SLIDER -->
<link href="/themes/ambit/assets/plugins/slider.swiper/dist/css/swiper.min.css" rel="stylesheet" type="text/css"/>
<!-- REVOLUTION SLIDER -->
<link href="/themes/ambit/assets/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css"/>
<link href="/themes/ambit/assets/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css"/>
<!-- THEME CSS -->
<link href="/themes/ambit/assets/css/essentials.css" rel="stylesheet" type="text/css"/>
<link href="/themes/ambit/assets/css/layout.css" rel="stylesheet" type="text/css"/>
<!-- PAGE LEVEL SCRIPTS -->
<link href="/themes/ambit/assets/css/header-1.css" rel="stylesheet" type="text/css"/>
<link href="/themes/ambit/assets/css/color_scheme/green.css" id="color_scheme" rel="stylesheet" type="text/css"/>
<meta content="5zhWMSi5TDhiHRp9EEPFtsifru8SDcTUSx8XGg8rXJ4" name="google-site-verification"/>
<link href="http://ambit.pl/" rel="canonical"/>
<link href="http://ambit.pl/" rel="shortlink"/>
<link href="/themes/ambit/i/icon.png" rel="shortcut icon"/>
<link href="/themes/ambit/main.css" rel="stylesheet" type="text/css"/>
</head>
<body class="body">
<!-- wrapper -->
<div id="wrapper">
<div class="sticky clearfix" id="header">
<!-- TOP NAV -->
<header id="topNav">
<div class="container">
<!-- Mobile Menu Button -->
<button class="btn btn-mobile" data-target=".nav-main-collapse" data-toggle="collapse">
<i class="fa fa-bars"></i>
</button>
<!-- Logo -->
<a class="logo pull-left" href="/">
<img alt="" src="/themes/ambit/i/logo1.png"/>
</a>
<div class="navbar-collapse pull-right nav-main-collapse collapse">
<nav class="nav-main">
<!--
									NOTE

									For a regular link, remove "dropdown" class from LI tag and "dropdown-toggle" class from the href.
									Direct Link Example:

									<li>
										<a href="#">HOME</a>
									</li>
								-->
<ul class="nav nav-pills nav-main" id="topMain">
<li class="dropdown"><!-- HOME -->
<a href="/">STRONA GŁÓWNA</a>
</li>
<li class="dropdown"><!-- PAGES -->
<a class="dropdown-toggle" href="/?m=artykul&amp;kat=44">
											O NAS
										</a>
<ul class="dropdown-menu">
<li>
<a href="https://www.ambit.pl/?m=artykul&amp;kat=44">
													FIRMA
												</a>
</li>
<li>
<a href="https://www.ambit.pl/?m=artykul&amp;kat=45">
													DOKUMENTY
												</a>
</li>
<li>
<a href="https://www.ambit.pl/?m=artykul&amp;kat=60">
													Klauzula RODO
												</a>
</li>
<li>
<a href="https://www.ambit.pl/?m=artykul&amp;kat=55">
													Do pobrania
												</a>
</li>
</ul>
</li>
<li class="dropdown"><!-- PAGES -->
<a class="dropdown-toggle" href="">
											OFERTA
										</a>
<ul class="dropdown-menu">
<li>
<a href="https://www.ambit.pl/?m=artykul&amp;kat=46">
													Recykling pojazdów
												</a>
</li>
<li>
<a href="https://www.ambit.pl/?m=artykul&amp;kat=64">
													ReBon
												</a>
</li>
<li>
<a href="http://czesci.ambit.pl">
													Cześci samochodowe
												</a>
</li>
<li>
<a href="http://siec.ambit.pl">
													Sieć recyklingu pojazdów
												</a>
</li>
<li>
<a href="https://www.ambit.pl/?m=artykul&amp;kat=49">
													Skup i przerób złomu
												</a>
</li>
<li>
<a href="https://www.ambit.pl/?m=artykul&amp;kat=50">
													Handel wyrobami hutniczymi
												</a>
</li>
<li>
<a href="https://ambit.pl/?m=post&amp;id=3496">
													SAMOCHODY POWYPADKOWE
												</a>
</li>
<li>
<a href="https://ambit.pl/?m=post&amp;id=3519">
													Samochody do demontażu
												</a>
</li>
</ul>
</li>
<li class="dropdown"><!-- PAGES -->
<a class="dropdown-toggle" href="/?m=artykul&amp;kat=51">
											ARES
										</a>
<ul class="dropdown-menu">
<li>
<a href="https://www.ambit.pl/?m=artykul&amp;kat=51">
													Opis systemu
												</a>
</li>
<li>
<a href="http://www.autozlomonline.pl/body_firmy_zrzeszone.php">
													Firmy zrzeszone
												</a>
</li>
<li>
<a href="https://www.ambit.pl/?m=artykul&amp;kat=52">
													Prezentacje
												</a>
</li>
</ul>
</li>
<li class="dropdown"><!-- PAGES -->
<a class="dropdown-toggle" href="/?m=kontakt&amp;kat=56">
											KONTAKT
										</a>
<ul class="dropdown-menu">
<li>
<a href="https://www.ambit.pl/?m=kontakt&amp;kat=56">
													Kontakt
												</a>
</li>
</ul>
</li>
</ul>
</nav>
</div>
</div>
</header>
<!-- /Top Nav -->
</div> <!-- REVOLUTION SLIDER -->
<section class="slider fullwidthbanner-container roundedcorners" id="slider">
<div class="fullwidthbanner" data-height="450" data-navigationstyle="">
<ul class="hide">
<!-- SLIDE  -->
<li data-masterspeed="1000" data-saveperformance="off" data-slotamount="1" data-title="Slide" data-transition="random">
<img alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" data-lazyload="https://www.ambit.pl/ic//0/3/48-PRZOD-bon-png-48-980-220-0-e.jpg" src="assets/images/1x1.png"/>
<div class="tp-dottedoverlay twoxtwo"><!-- dotted overlay --></div>
<div class="overlay dark-5"><!-- dark overlay [1 to 9 opacity] --></div>
<div class="tp-caption customin ltl tp-resizeme text_white" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="1800" data-splitin="none" data-splitout="none" data-start="18000" data-x="center" data-y="105" style="z-index: 10;">
<span class="weight-300"> </span>
</div>
<div class="tp-caption customin ltl tp-resizeme large_bold_white" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1200" data-x="center" data-y="55" style="z-index: 8;">
								BON PREMIOWY
							</div>
<div class="tp-caption customin ltl tp-resizeme small_light_white font-lato" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1400" data-x="center" data-y="145" style="z-index: 10; width: 100%; max-width: 750px; white-space: normal; text-align:center; font-size:20px;">
Dodatkowe 2OOzł <b>premii</b><br/> przy przekazaniu
kompletnego pojazdu <br/>do jednej z naszych
Stacji Demontażu
							</div>
<div class="tp-caption customin ltl tp-resizeme" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1550" data-x="center" data-y="310" style="z-index: 10;">
<a class="btn btn-default btn-lg" href="https://ambit.pl/?m=artykul&amp;kat=64">
<span>REGULAMIN</span>
</a>
</div>
</li>
<!-- SLIDE  -->
<li data-masterspeed="1000" data-saveperformance="off" data-slotamount="1" data-title="Slide" data-transition="random">
<img alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" data-lazyload="https://www.ambit.pl/ic//d/1/29-slide1-s-jpg-29-980-220-0-e.jpg" src="assets/images/1x1.png"/>
<div class="tp-dottedoverlay twoxtwo"><!-- dotted overlay --></div>
<div class="overlay dark-5"><!-- dark overlay [1 to 9 opacity] --></div>
<div class="tp-caption customin ltl tp-resizeme text_white" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1000" data-x="center" data-y="105" style="z-index: 10;">
<span class="weight-300">RECYKLING SAMOCHODÓW  </span>
</div>
<div class="tp-caption customin ltl tp-resizeme large_bold_white" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1200" data-x="center" data-y="155" style="z-index: 10;">
								ZŁOMOWANIE POJAZDÓW
							</div>
<div class="tp-caption customin ltl tp-resizeme small_light_white font-lato" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1400" data-x="center" data-y="245" style="z-index: 10; width: 100%; max-width: 750px; white-space: normal; text-align:center; font-size:20px;">
								700 zł i więcej za każdy pojazd.
							</div>
<div class="tp-caption customin ltl tp-resizeme" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1550" data-x="center" data-y="313" style="z-index: 10;">
<a class="btn btn-default btn-lg" href="?m=artykul&amp;kat=46">
<span>Dowiedz się więcej...</span>
</a>
</div>
</li>
<!-- SLIDE  -->
<li data-masterspeed="1000" data-saveperformance="off" data-slotamount="1" data-title="Slide" data-transition="random">
<img alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" data-lazyload="https://www.ambit.pl/ic//e/1/30-slide-3-jpg-30-980-220-0-e.jpg" src="assets/images/1x1.png"/>
<div class="tp-dottedoverlay twoxtwo"><!-- dotted overlay --></div>
<div class="overlay dark-5"><!-- dark overlay [1 to 9 opacity] --></div>
<div class="tp-caption customin ltl tp-resizeme text_white" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1000" data-x="center" data-y="105" style="z-index: 10;">
<span class="weight-300">W tej wyjątkowej sytuacji prosimy o zamawianie części telefonicznie lub internet.</span>
</div>
<div class="tp-caption customin ltl tp-resizeme large_bold_white" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1200" data-x="center" data-y="155" style="z-index: 10;">
								CZĘŚCI Z DEMONTAŻU
							</div>
<div class="tp-caption customin ltl tp-resizeme small_light_white font-lato" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1400" data-x="center" data-y="245" style="z-index: 10; width: 100%; max-width: 750px; white-space: normal; text-align:center; font-size:20px;">Dowóz część do warsztatów w cenie (Białystok i okolice)
							</div>
<div class="tp-caption customin ltl tp-resizeme" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1550" data-x="center" data-y="313" style="z-index: 10;">
<a class="btn btn-default btn-lg" href="http://czesci.ambit.pl">
<span>zamów części online</span>
</a>
</div>
</li>
<!-- SLIDE  -->
<li data-masterspeed="1000" data-saveperformance="off" data-slotamount="1" data-title="Slide" data-transition="random">
<img alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" data-lazyload="https://www.ambit.pl/ic//b/2/43-handel-png-43-980-220-0-e.jpg" src="assets/images/1x1.png"/>
<div class="tp-dottedoverlay twoxtwo"><!-- dotted overlay --></div>
<div class="overlay dark-5"><!-- dark overlay [1 to 9 opacity] --></div>
<div class="tp-caption customin ltl tp-resizeme text_white" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1000" data-x="center" data-y="105" style="z-index: 10;">
<span class="weight-300">Sprzedaż pojazdów </span>
</div>
<div class="tp-caption customin ltl tp-resizeme large_bold_white" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1200" data-x="center" data-y="155" style="z-index: 10;">
								Samochody powypadkowe
							</div>
<div class="tp-caption customin ltl tp-resizeme small_light_white font-lato" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1400" data-x="center" data-y="245" style="z-index: 10; width: 100%; max-width: 750px; white-space: normal; text-align:center; font-size:20px;">.....
							</div>
<div class="tp-caption customin ltl tp-resizeme" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1550" data-x="center" data-y="313" style="z-index: 10;">
<a class="btn btn-default btn-lg" href="http://powypadkowe.ambit.pl">
<span>zobacz ofertę...</span>
</a>
</div>
</li>
<!-- SLIDE  -->
<li data-masterspeed="1000" data-saveperformance="off" data-slotamount="1" data-title="Slide" data-transition="random">
<img alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" data-lazyload="https://www.ambit.pl/ic//c/1/28-SIEC-1170x260-jpg-28-980-220-0-e.jpg" src="assets/images/1x1.png"/>
<div class="tp-dottedoverlay twoxtwo"><!-- dotted overlay --></div>
<div class="overlay dark-5"><!-- dark overlay [1 to 9 opacity] --></div>
<div class="tp-caption customin ltl tp-resizeme text_white" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1000" data-x="center" data-y="105" style="z-index: 10;">
<span class="weight-300">UMOWA O ZAPEWNIENIE SIECI </span>
</div>
<div class="tp-caption customin ltl tp-resizeme large_bold_white" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1200" data-x="center" data-y="155" style="z-index: 10;">
								SIEĆ RECYKLINGU POJAZDÓW
							</div>
<div class="tp-caption customin ltl tp-resizeme small_light_white font-lato" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1400" data-x="center" data-y="245" style="z-index: 10; width: 100%; max-width: 750px; white-space: normal; text-align:center; font-size:20px;">.....
							</div>
<div class="tp-caption customin ltl tp-resizeme" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1550" data-x="center" data-y="313" style="z-index: 10;">
<a class="btn btn-default btn-lg" href="http://siec.ambit.pl">
<span>Dowiedz się więcej...</span>
</a>
</div>
</li>
<!-- SLIDE  -->
<li data-masterspeed="1000" data-saveperformance="off" data-slotamount="1" data-title="Slide" data-transition="random">
<img alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" data-lazyload="https://www.ambit.pl/ic//8/2/40-koronawirus-png-40-980-220-0-e.jpg" src="assets/images/1x1.png"/>
<div class="tp-dottedoverlay twoxtwo"><!-- dotted overlay --></div>
<div class="overlay dark-5"><!-- dark overlay [1 to 9 opacity] --></div>
<div class="tp-caption customin ltl tp-resizeme text_white" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="1800" data-splitin="none" data-splitout="none" data-start="18000" data-x="center" data-y="105" style="z-index: 10;">
<span class="weight-300"> </span>
</div>
<div class="tp-caption customin ltl tp-resizeme large_bold_white" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1200" data-x="center" data-y="55" style="z-index: 8;">
								WAŻNA INFORMACJA
							</div>
<div class="tp-caption customin ltl tp-resizeme small_light_white font-lato" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1400" data-x="center" data-y="145" style="z-index: 10; width: 100%; max-width: 750px; white-space: normal; text-align:center; font-size:20px;">
								W związku z zaistniałą sytuacją epidemiologiczną SARS-CoV-2 pragniemy poinformować, iż dokonujemy
wszelkich starań, by zapewnić ciągłość w funkcjonowaniu naszych oddziałów. 
W trosce o zdrowie i bezpieczeństwo naszych Klientów i Pracowników, wdrożyliśmy procedury dotyczące obsługi klienta.
Zwracamy się z prośba o składanie zamówień telefonicznych oraz za pośrednictwem naszej strony oraz allegro.
							</div>
<div class="tp-caption customin ltl tp-resizeme" data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-easing="easeOutQuad" data-elementdelay="0.01" data-endeasing="Power4.easeIn" data-endelementdelay="0.1" data-endspeed="1000" data-speed="800" data-splitin="none" data-splitout="none" data-start="1550" data-x="center" data-y="310" style="z-index: 10;">
<a class="btn btn-default btn-lg" href="http://ambit.pl?m=kontakt&amp;kat=56">
<span>KONTAKT</span>
</a>
</div>
</li>
</ul>
<div class="tp-bannertimer"></div>
</div>
</section>
<!-- /REVOLUTION SLIDER -->
<section>
<div class="container">
<p class="lead">Firma AMBIT Spółka z o.o. powstała w 1992 roku. Działalność firmy to: recykling pojazdów samochodowych, skup i sprzedaż samochodów używanych i powypadkowych, skup i przerób złomu, sprzedaż części z demontażu, sprzedaż stali, wyrobów hutniczych.</p>
<div class="divider divider-center divider-color"><!-- divider -->
<i class="fa fa-chevron-down"></i>
</div>
<!-- FEATURED BOXES 3 -->
<div class="row">
<div class="col-md-4 col-xs-6">
<div class="text-center">
<i class="ico-color ico-lg ico-rounded ico-hover et-gears"></i>
<h4><a href="http://czesci.ambit.pl">Części Zamienne.</a></h4>
<p class="font-lato size-20"><a href="http://czesci.ambit.pl">Sprzedaż części zamiennych z demontażu.</a></p>
</div>
</div>
<div class="col-md-4 col-xs-6">
<div class="text-center">
<i class="ico-color ico-lg ico-rounded ico-hover et-recycle"></i>
<h4><a href="http://ambit.pl/?m=artykul&amp;kat=46">Złomowanie</a></h4>
<p class="font-lato size-20"><a href="http://ambit.pl/?m=artykul&amp;kat=46">Recykling pojazdów samochodowych.</a></p>
</div>
</div>
<div class="col-md-4 col-xs-6">
<div class="text-center">
<i class="ico-color ico-lg ico-rounded ico-hover et-speedometer"></i>
<h4><a href="https://ambit.pl/?m=post&amp;id=3496">Samochody powypadkowe.</a></h4>
<p class="font-lato size-20"><a href="https://ambit.pl/?m=post&amp;id=3496">Skup i sprzedaż samochodów używanych i powypadkowych.</a></p>
</div>
</div>
</div>
<!-- /FEATURED BOXES 3 -->
<div class="divider divider-center divider-color"><!-- divider -->
<i class="fa fa-chevron-down"></i>
</div>
<!-- / -->
<section class="parallax parallax-2" style="background-image: url('/themes/ambit/i/paralax.jpg');">
<div class="overlay dark-2"><!-- dark overlay [1 to 9 opacity] --></div>
<div class="container">
<div class="callout alert alert-transparent noborder noradius nomargin">
<div class="text-center">
<h3 style="text-align: center; ">Jesteśmy członkiem stowarzyszenia <span><strong>FORS</strong></span> od 1999 roku!</h3>
<p class="font-lato size-20" style="text-align: center;">
						Razem z FORS działamy na rzecz ochrony środowiska.<br/><br/>
<a class="btn btn-primary btn-lg margin-top-30" href="http://fors.pl">Dowiedz się więcej!</a></p>
</div>
</div>
<!-- /CALLOUT -->
</div>
</section>
<!-- -->
<section>
<div class="container">
<div class="row">
<div class="col-md-6">
<h3 class="size-20">Skorzystaj z naszych usług:</h3>
<ul class="list-unstyled">
<li><i class="fa fa-check"></i> Sprzedaż pojazdów powypadkowych i pojazdów po profesjonalnej naprawie.</li>
<li><i class="fa fa-check"></i> Złomowanie pojazdów , skup pojazdów, w tym uszkodzonych, zapewniamy transport na terenie kraju.</li>
<li><i class="fa fa-check"></i> Sprzedaż części z demontażu.</li>
<li><i class="fa fa-check"></i> Skup i przerób złomu.</li>
<li><i class="fa fa-check"></i> Sprzedaż stali,  wyrobów hutniczych atrakcyjne ceny.</li>
</ul>
</div>
<div class="col-md-6">
<h3 class="size-20 text-center">Opinie o Nas:</h3>
<!--
								Note: remove class="rounded" from the img for squared image!
							-->
<ul class="row clearfix list-unstyled">
<li class="col-md-6">
<div class="testimonial">
<figure class="pull-left">
<img alt="" src="/cms/lib/image.php?id=31&amp;width=300&amp;height=300"/>
</figure>
<div class="testimonial-content">
<p>Polecam, mają dużą bazę i kompetentną obsługę.</p>
<cite>
												Anna Z. <span>Rzeczoznawca</span>
</cite>
</div>
</div>
</li>
<li class="col-md-6">
<div class="testimonial">
<figure class="pull-left">
<img alt="" src="/cms/lib/image.php?id=32&amp;width=300&amp;height=300&amp;p=e"/>
</figure>
<div class="testimonial-content">
<p>Części nie są najtańsze, ale za to przetestowane i dobrej jakości.</p>
<cite>
												Heniek K. <span>Mechanik</span>
</cite>
</div>
</div>
</li>
</ul>
</div>
</div>
</div>
</section>
<!-- / -->
<!-- Content -->
<!--
	INFO BAR
	outside .container and section
-->
<section class="info-bar info-bar-color">
<div class="container">
<div class="row">
<div class="col-sm-4">
<i class="glyphicon glyphicon-cog"></i>
<h3><a href="http://czesci.ambit.pl">Części Zamienne.</a></h3>
<p><a href="http://czesci.ambit.pl">Sprzedaż części zamiennych z demontażu.</a></p>
</div>
<div class="col-sm-4">
<i class="glyphicon glyphicon-refresh"></i>
<h3><a href="http://ambit.pl/?m=artykul&amp;kat=46">Złomowanie</a></h3>
<p><a href="http://ambit.pl/?m=artykul&amp;kat=46">Recykling pojazdów samochodowych.</a></p>
</div>
<div class="col-sm-4">
<i class="glyphicon glyphicon-zoom-in"></i>
<h3><a href="https://ambit.pl/?m=post&amp;id=3496">Samochody powypadkowe</a></h3>
<p><a href="https://ambit.pl/?m=post&amp;id=3496">Skup i sprzedaż samochodów używanych i powypadkowych.</a></p>
</div>
</div>
</div>
</section>
<!-- /INFO BAR -->
<footer id="footer">
<div class="container">
<div class="row margin-top-60 margin-bottom-40 size-13">
<!-- col #1 -->
<div class="col-md-3 col-sm-3">
<!-- Footer Logo
            <!-- <img class="footer-logo" src="/themes/ambit/i/kogo_kontakt.png" alt="" />  -->
<p><span style="line-height: 1.5em;"><b>Ambit sp. z o.o.</b><br/></span><span style="line-height: 17.1429px;">ul. Białostocka 27</span><br style="line-height: 17.1429px;"/><span style="line-height: 17.1429px;">16-002 Dobrzyniewo Duże k/Białegostoku</span><br style="line-height: 17.1429px;"/><span style="line-height: 17.1429px;">Podlasie</span><br style="line-height: 17.1429px;"/><span style="line-height: 17.1429px;">GPS N= 53°11'54,8' E=23°01'03''</span><br style="line-height: 17.1429px;"/><span style="line-height: 17.1429px;">Tel: 85 719 77 96</span><br style="line-height: 17.1429px;"/><span style="line-height: 17.1429px;">Tel: 85 719 75 42 </span><br style="line-height: 17.1429px;"/><span style="line-height: 17.1429px;">Fax: 85 719 79 33 </span></p><p>Czynne: pn-pt 7-17, sob 7-13</p><p><br/></p><p><span style="line-height: 17.1429px;">Sprzedaż Stali i Wyrobów Hutniczych<br/></span>Czynne: pn-pt 7-16, sob 7-13</p><br/><p><span style="line-height: 17.1429px;"><br/></span></p>
</div>
<!-- /col #1 -->
<!-- col #2 -->
<div class="col-md-5 col-sm-5">
<h4 class="letter-spacing-1">Nasza oferta:</h4>
<p>Złomowanie pojazdów, skup samochodów powypadkowych, skup złomu, odpadów i sprzętu elektrycznego i elektronicznego. Sprzedaż części samochodowych z demontażu, sprzedaż pojazdów powypadkowych, handel pojazdami, sprzedaż wyrobów hutniczych.</p>
<hr/>
<!-- Social Icons -->
<div class="clearfix">
<a class="social-icon social-icon-sm social-icon-border social-facebook pull-left" data-placement="top" data-toggle="tooltip" href="https://www.facebook.com/ambit.ambit.9" title="Facebook">
<i class="icon-facebook"></i>
<i class="icon-facebook"></i>
</a>
<a class="social-icon social-icon-sm social-icon-border social-twitter pull-left" data-placement="top" data-toggle="tooltip" href="#" title="Twitter">
<i class="icon-twitter"></i>
<i class="icon-twitter"></i>
</a>
<a class="social-icon social-icon-sm social-icon-border social-gplus pull-left" data-placement="top" data-toggle="tooltip" href="#" title="Google plus">
<i class="icon-gplus"></i>
<i class="icon-gplus"></i>
</a>
</div>
<!-- /Social Icons -->
</div>
<!-- /col #2 -->
<!-- col #3 -->
<div class="col-md-4 col-sm-4">
<div class="height-300 grayscale" id="map3"></div>
</div>
<!-- /col #3 -->
</div>
</div>
<div class="copyright">
<div class="container">
			© 2016 Ambit sp. z o. o.
		</div>
</div>
</footer>
</div>
<!-- /wrapper -->
<!-- SCROLL TO TOP -->
<a href="#" id="toTop"></a>
<!-- PRELOADER -->
<div id="preloader">
<div class="inner">
<span class="loader"></span>
</div>
</div><!-- /PRELOADER -->
<!-- JAVASCRIPT FILES -->
<script type="text/javascript">var plugin_path = '/themes/ambit/assets/plugins/';</script>
<script src="/themes/ambit/assets/plugins/jquery/jquery-2.2.3.min.js" type="text/javascript"></script>
<script src="/themes/ambit/assets/js/scripts.js" type="text/javascript"></script>
<!-- REVOLUTION SLIDER -->
<script src="/themes/ambit/assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="/themes/ambit/assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="/themes/ambit/assets/js/view/demo.revolution_slider.js" type="text/javascript"></script>
<!-- AJAX CONTACT -->
<script src="/themes/ambit/assets/js/contact.js" type="text/javascript"></script>
<script src="/themes/ambit/main.js" type="text/javascript"></script>
<script src="/cms/libe2/jquery.cookiesdirective.js" type="text/javascript"></script>
<script type="text/javascript">

			jQuery(document).ready(function(){

		
		jQuery.cookiesDirective({
		    privacyPolicyUri: '',
			explicitConsent: false,
			position : 'bottom',
			message : 'Nasza strona używa cookies. Jeżeli nie wyrażasz na to zgody możesz zablokować to w ustawieniach swojej przeglądarki!',
            duration: 0,
			backgroundColor: '#000000',
			linkColor: '#ffffff'
			
		});
	


			});

		</script>



        Ten tekst możesz zmienić w zakładce konfiguracja, pozycja: tracking_pl

</section></div></body></html>
