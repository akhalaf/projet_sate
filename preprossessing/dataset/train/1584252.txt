<!DOCTYPE html>
<html dir="ltr" lang="en-gb" xml:lang="en-gb" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<base href="https://www.wigandpeneast.com/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="pizza, pasta, food, restaurant, Iowa, Iowa City, Ankeny, Coralville, pub, delivery" name="keywords"/>
<meta content="Three locations in central and eastern Iowa. Wig and Pen Pizza serves the Iowa City, Coralville, and Des Moines areas." name="description"/>
<title>Wig and Pen Pizza - Home</title>
<link href="/templates/protostar/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/templates/protostar/css/template.css" rel="stylesheet" type="text/css"/>
<link href="/templates/protostar/css/custom.css" rel="stylesheet" type="text/css"/>
<link href="https://www.wigandpeneast.com/modules/mod_bt_contentslider/tmpl/css/btcontentslider.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
#btcontentslider143 .bt_handles{top: 0px !important;right: 5px !important}
		@media screen and (max-width: 480px){.bt-cs .bt-row{width:100%!important;}}
	</style>
<script class="joomla-script-options new" type="application/json">{"csrf.token":"cb24ad517c913244b4cf3f96913231fb","system.paths":{"root":"","base":""}}</script>
<script src="/media/jui/js/jquery.min.js?8e9a84e71fb1ae07d94ad8b0209ec32f" type="text/javascript"></script>
<script src="/media/jui/js/jquery-noconflict.js?8e9a84e71fb1ae07d94ad8b0209ec32f" type="text/javascript"></script>
<script src="/media/jui/js/jquery-migrate.min.js?8e9a84e71fb1ae07d94ad8b0209ec32f" type="text/javascript"></script>
<script src="/media/jui/js/bootstrap.min.js?8e9a84e71fb1ae07d94ad8b0209ec32f" type="text/javascript"></script>
<script src="/media/system/js/core.js?8e9a84e71fb1ae07d94ad8b0209ec32f" type="text/javascript"></script>
<script src="/templates/protostar/js/template.js" type="text/javascript"></script>
<script src="https://www.wigandpeneast.com/modules/mod_bt_contentslider/tmpl/js/slides.js" type="text/javascript"></script>
<script src="https://www.wigandpeneast.com/modules/mod_bt_contentslider/tmpl/js/default.js" type="text/javascript"></script>
<script src="https://www.wigandpeneast.com/modules/mod_bt_contentslider/tmpl/js/jquery.easing.1.3.js" type="text/javascript"></script>
<script type="text/javascript">
(function($){$(window).ready(function(){$(".wf-media-input").removeAttr("readonly");})})(jQuery);
	(function ($) {

	 	$(document).ready(function(){
			$("#front-main").css({ height: $(window).innerHeight() -300 }); 
			  $(window).resize(function(){
			    $("#front-main").css({ height: $(window).innerHeight() -300 });
			  });
		     
		     
	 	});
		
		

	})(jQuery);
	

	</script>
<link href="//fonts.googleapis.com/css?family=Passion+One" rel="stylesheet" type="text/css"/>
<style type="text/css">
			h1,h2,h3,h4,h5,h6,.site-title{
				font-family: 'Passion One', sans-serif;
			}
		</style>
<style type="text/css">
		body.site
		{
			border-top: 3px solid #0088cc;
			background-color: #f4f6f7		}
		a
		{
			color: #0088cc;
		}
		.navbar-inner, .nav-list > .active > a, .nav-list > .active > a:hover, .dropdown-menu li > a:hover, .dropdown-menu .active > a, .dropdown-menu .active > a:hover, .nav-pills > .active > a, .nav-pills > .active > a:hover,
		.btn-primary
		{
			background: #0088cc;
		}
		.navbar-inner
		{
			-moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
			-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
			box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
		}
	</style>
<script data-chownow-company-id="2865" src="https://cf.chownowcdn.com/latest/static/integrations/ordering-modal.min.js"></script>
<!--[if lt IE 9]>
		<script src="/media/jui/js/html5.js"></script>
	<![endif]-->
<link href="http://fonts.googleapis.com/css?family=Passion+One" rel="stylesheet" type="text/css"/>
<style type="text/css">

p { font-family: "Passion One";

}

</style>
<link href="http://fonts.googleapis.com/css?family=Passion+One" rel="stylesheet" type="text/css"/>
<style type="text/css">

h2, h3 { font-family: "Passion One";

}

</style>
</head>
<body class="site com_blankcomponent view-default no-layout no-task itemid-101 fluid">
<!-- Body -->
<div class="body front">
<div class="container-fluid front-display">
<!-- Header -->
<header class="header" role="banner">
<div class="header-inner clearfix">
<a class="brand pull-left" href="/">
<img alt="Wig and Pen Pizza" src="https://www.wigandpeneast.com/images/WigPen.Basic.Logo.jpg"/> </a>
<div id="header_text">
<div class="custom">
<p style="text-align: right;"><span class="main_header">FOUR CONVENIENT LOCATIONS • SAME GREAT PIZZA</span></p></div>
</div>
</div>
</header>
<div id="front-main">
<div id="display-main-content">
</div>
<div class="display1-4">
<div id="display">
<div class="displaybox" id="display1d">
<div id="mod-container">
<div class="custom_head">
<h2 style="text-align: center;">NORTH <br/>LIBERTY</h2></div>
<div class="custom">
<p style="text-align: center;"><img alt="wigpen nl sm" src="/images/wigpen-nl-sm.jpg"/></p>
<p style="text-align: center;"><span style="font-size: 18pt;">319.665.2255</span></p>
<p style="text-align: center;"><span style="font-size: 18pt;"></span><span style="font-size: 14pt;">201 Hwy 965 NE<br/></span><span style="font-size: 14pt;">North Liberty, IA 52317<br/></span><span style="font-size: 10pt;">(just south of Bluebird Diner)</span></p>
<p style="text-align: center;"><span style="color: #993300; font-size: 16px; text-align: center;">CARRY OUT &amp; DELIVERY</span></p>
<p style="text-align: center;"><span style="font-size: 14pt;"><a href="http://wigandpeneast.com/menuNL.pdf?ver=2.0" target="_blank">MENU</a>  |  <a href="https://www.google.com/maps/place/Wig+%26+Pen+Pizza/@41.7483868,-91.6109961,17z/data=!3m1!4b1!4m5!3m4!1s0x87e444b2bceef9b3:0xa9e36595836f6012!8m2!3d41.7483868!4d-91.6088074" target="_blank">DIRECTIONS</a><br/><a class="chownow-order-online" href="https://ordering.chownow.com/order/2865/locations" target="_blank">ORDER ONLINE</a></span></p></div>
</div>
</div>
<div class="displaybox" id="display1c">
<div id="mod-container">
<div class="custom_head">
<h2 style="text-align: center;">ANKENY / <br/>DES MOINES</h2></div>
<div class="custom">
<p style="text-align: center;"><img alt="wigpen ank sm" src="/images/wigpen-ank-sm.jpg"/></p>
<p style="text-align: center;"><span style="font-size: 18pt;">515.963.9777</span></p>
<p style="text-align: center;"><span style="font-size: 18pt;"></span><span style="font-size: 14pt;">2005 S. Ankeny Blvd. Suite 300<br/></span><span style="font-size: 14pt;">Ankeny, IA 50023<br/></span><span style="font-size: 10pt;">(across from DMACC)</span></p>
<p style="text-align: center;"><span style="color: #993300; font-size: 16px; text-align: center;">DINE IN &amp; CARRY OUT</span></p>
<p style="text-align: center;"><span style="font-size: 14pt;"><a href="http://wigandpeneast.com/menuAnkeny.pdf?ver=2.0" target="_blank">MENU</a>  | <a href="https://www.google.com/maps/place/Wig+%26+Pen+Pizza+Pub/@41.709845,-93.6019537,17z/data=!4m6!1m3!3m2!1s0x87ee854a2f1eb67f:0xe346d242a4fc6704!2sWig+%26+Pen+Pizza+Pub!3m1!1s0x87ee854a2f1eb67f:0xe346d242a4fc6704" target="_blank">DIRECTIONS</a></span></p></div>
</div>
</div>
<div class="displaybox" id="display1b">
<div id="mod-container">
<div class="custom_head">
<h2 style="text-align: center;">CORALVILLE <br/>STRIP</h2></div>
<div class="custom">
<p><img alt="wig pen cv sm" src="/images/wig_pen-cv-sm.jpg" style="display: block; margin-left: auto; margin-right: auto;"/></p>
<p style="text-align: center;"><span style="font-size: 18pt;">319.354.2767</span></p>
<p style="text-align: center;"><span style="font-size: 18pt;"></span><span style="font-size: 14pt;">1220 Hwy 6 West<br/></span><span style="font-size: 14pt;">Iowa City 52246<br/></span><span style="font-size: 10pt;">(next to the Vine on the Coralville Strip)</span></p>
<p style="text-align: center;"><span style="color: #993300; font-size: 16px; text-align: center;">DINE IN &amp; CARRY OUT</span></p>
<p style="text-align: center;"><span style="font-size: 14pt;"><a href="http://www.wigandpenpizzapub.com" target="_blank">MENU</a>   | <a href="https://www.google.com/maps/place/Wig+%26+Pen+Pizza+Pub/@41.6667219,-91.5591149,15z/data=!4m2!3m1!1s0x0:0xc856abcb9ad1a146?sa=X&amp;ved=0ahUKEwiA0LOogoLKAhVJ5GMKHaOhBpMQ_BIIeDAK" target="_blank">DIRECTIONS</a><br/><a class="chownow-order-online" href="https://ordering.chownow.com/order/2865/locations" target="_blank">ORDER ONLINE</a></span></p></div>
</div>
</div>
<div class="displaybox" id="display1a">
<div id="mod-container">
<div class="custom_head">
<h2 style="text-align: center;">EASTSIDE <br/>IOWA CITY</h2></div>
<div class="custom">
<p style="text-align: center;"><img alt="wigpen east sm" src="/images/wigpen-east-sm.jpg"/></p>
<p style="text-align: center;"><span style="font-size: 18pt;">319.351.2327</span></p>
<p style="text-align: center;"><span style="font-size: 18pt;"></span><span style="font-size: 14pt;">363 North 1st Avenue<br/></span><span style="font-size: 14pt;">Iowa City 52245<br/></span><span style="font-size: 10pt;">(across from Regina High School)</span></p>
<p style="text-align: center;"><span style="font-size: 12pt; color: #993300;">CARRY OUT &amp; DELIVERY</span></p>
<p style="text-align: center;"><span style="font-size: 14pt;"><a href="http://order.wigandpenpizza.com/zgrid/proc/site/sitep.jsp" target="_blank">ORDER ONLINE</a>   | <a href="https://www.google.com/maps/place/Wig+%26+Pen+East/@41.6655364,-91.5044095,17z/data=!3m1!4b1!4m2!3m1!1s0x87e46a18b020f9bf:0x99a0e2c0aea8a3a9" target="_blank">DIRECTIONS</a></span></p></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="container-fluid front1-4">
<div id="front1">
</div>
</div>
<div class="container-fluid front2-">
<div id="front2">
</div>
</div>
<div class="container-fluid front3-">
<div id="front3">
</div>
</div>
<div class="container-fluid front4-">
<div id="front4">
</div>
</div>
<div class="container-fluid bottom1-4">
<div id="bottom1">
</div>
</div>
<div class="container-fluid bottom2-">
<div id="bottom2">
</div>
</div>
<div class="container-fluid bottom3-">
<div id="bottom3">
</div>
</div>
<div class="container-fluid bottom4-Array">
<div id="bottom4">
</div>
</div>
</div>
<!-- Footer -->
<footer class="footer" role="contentinfo">
<div class="container-fluid" id="footer-1">
<div class="container-fluid footer-2">
<div id="footer">
<div class="footerbox" id="footer1a">
<div id="mod-container">
<div class="bt-cs" id="btcontentslider143" style="display:none;width:auto">
<div class="slides_container" style="width:auto;">
<div class="slide" style="width:auto">
<div class="bt-row bt-row-first" style="width:100%">
<div class="bt-inner">
<div class="bt-introtext">
<p>"...conventional expectations should be thrown out the window here. It’s taken me almost three years to do it, but I’ve finally found a non-chain pizza place within an hour’s trip that I can genuinely enjoy. It’s truly been a long time coming."  - Dan vs. Food</p> </div>
</div>
<!--end bt-inner -->
</div>
<!--end bt-row -->
<div style="clear: both;"></div>
</div>
<!--end bt-main-item page	-->
<div class="slide" style="width:auto">
<div class="bt-row bt-row-first" style="width:100%">
<div class="bt-inner">
<div class="bt-introtext">
<p>"Two of our editors go on a pilgrimage to Iowa City every year, and can personally attest to the beauty of Wig and Pen’s Flying Tomato Pie, which is a sort of pan/stuffed pizza hybrid that involves an entire grocery store’s dairy section of cheese, and a buttery crust. That originality alone is enough to put them at number one in the Hawkeye State." - Thrillist</p> </div>
</div>
<!--end bt-inner -->
</div>
<!--end bt-row -->
<div style="clear: both;"></div>
</div>
<!--end bt-main-item page	-->
<div class="slide" style="width:auto">
<div class="bt-row bt-row-first" style="width:100%">
<div class="bt-inner">
<div class="bt-introtext">
<p>"Envision a chewy, deep-crust, perfectly cooked pizza - smothered with cheese and topped with tomato slices. You take a bite - it melts in your mouth....you've just experienced the Flying Tomato. It should be a crime to have such enjoyment at a diner. DON'T order this pizza if you are watching your caloric intake :)"  - kathybrez on TripAdvisor</p> </div>
</div>
<!--end bt-inner -->
</div>
<!--end bt-row -->
<div style="clear: both;"></div>
</div>
<!--end bt-main-item page	-->
<div class="slide" style="width:auto">
<div class="bt-row bt-row-first" style="width:100%">
<div class="bt-inner">
<div class="bt-introtext">
<p>"Words cannot express my love for the Wig &amp; Pen. Seriously the best Chicago-style pizza outside of Chicago... I have to thank fellow Yelper Brendan M. for introducing me to this cheesy, buttery deliciousness. There's nothing like this in CA, and quite frankly the "Chicago-style" pizza I had at O'Hare failed miserably in comparison. The cheese was so fresh and melty. The tomatoes were the perfect mix of spice and sauce. The crust... OH THE CRUST... delectable buttery goodness.  This is a required stop EVERY TIME I'm in the Cedar Rapids-Coralville-Iowa City area.  GO HAWKEYES!" - Christine Q. on Yelp</p> </div>
</div>
<!--end bt-inner -->
</div>
<!--end bt-row -->
<div style="clear: both;"></div>
</div>
<!--end bt-main-item page	-->
</div>
</div>
<!--end bt-container -->
<div style="clear: both;"></div>
<script type="text/javascript">	
	if(typeof(btcModuleIds)=='undefined'){var btcModuleIds = new Array();var btcModuleOpts = new Array();}
	btcModuleIds.push(143);
	btcModuleOpts.push({
			slideEasing : 'easeInQuad',
			fadeEasing : 'easeInQuad',
			effect: 'slide,slide',
			preloadImage: 'https://www.wigandpeneast.com//modules/mod_bt_contentslider/tmpl/images/loading.gif',
			generatePagination: true,
			play: 20000,						
			hoverPause: true,	
			slideSpeed : 500,
			autoHeight:true,
			fadeSpeed : 500,
			equalHeight:false,
			width: 'auto',
			height: 'auto',
			pause: 100,
			preload: true,
			paginationClass: 'bt_handles',
			generateNextPrev:false,
			prependPagination:true,
			touchScreen:0	});
</script>
</div>
</div>
<div class="footerbox" id="footer1b">
<div id="mod-container">
<div class="custom">
<p><img alt="pizza" src="/images/maps/pizza.jpg" style="display: block; margin-left: auto; margin-right: auto;"/></p></div>
</div>
</div>
</div>
</div>
</div>
<div class="container-fluid" id="copyright-top">
<div id="inside-padding">
</div>
</div>
<div class="container-fluid" id="copyright">
<div id="inside-padding">
<p class="pull-right">
<a href="#top" id="back-top">
					Back to Top				</a>
</p>
<p>
				© 2021 Wig and Pen Pizza			</p>
</div>
</div>
</footer>
</body>
</html>
