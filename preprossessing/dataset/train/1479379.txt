<!DOCTYPE html>
<html class="error-page" dir="ltr" lang="it-it" xml:lang="it-it" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<base href="https://www.trovaweb.net/%09%0A"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="trovaweb,elenco aziende,elenco professionisti,shopping,eventi,recensioni,annunci,coupon,foto,video" name="keywords"/>
<meta content=" TrovaWeb © 2019 - tutti i diritti Riservati" name="rights"/>
<meta content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" name="robots"/>
<meta content="TrovaWeb una Vetrina Completa per dare Visibilità ad Aziende e Professionisti sul Web, Ottima indicizzazione - Annunci - Eventi - Coupon - Foto e Video" name="description"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>404 - Errore: 404</title>
<link href="/images/favicon.png" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/templates/shaper_spectrum/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/templates/shaper_spectrum/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/templates/shaper_spectrum/css/template.css" rel="stylesheet" type="text/css"/>
<link href="/plugins/system/gdpr/assets/css/cookieconsent.min.css" rel="stylesheet" type="text/css"/>
<link href="/plugins/system/gdpr/assets/css/jquery.fancybox.min.css" rel="stylesheet" type="text/css"/>
<link href="/media/widgetkit/wk-styles-9c38109f.css" id="wk-styles-css" rel="stylesheet" type="text/css"/>
<style type="text/css">
div.cc-window.cc-floating{max-width:24em}@media(max-width: 639px){div.cc-window.cc-floating:not(.cc-center){max-width: none}}div.cc-window, span.cc-cookie-settings-toggler{font-size:16px}div.cc-revoke{font-size:16px}div.cc-settings-label,span.cc-cookie-settings-toggle{font-size:14px}div.cc-window.cc-banner{padding:1em 1.8em}div.cc-window.cc-floating{padding:2em 1.8em}input.cc-cookie-checkbox+span:before, input.cc-cookie-checkbox+span:after{border-radius:1px}
	</style>
<script class="joomla-script-options new" type="application/json">{"csrf.token":"89cdc58bc3b88a153a1fa55417145e7e","system.paths":{"root":"","base":""}}</script>
<script src="/media/jui/js/jquery.min.js?5cc6fcc4bc3c8360a39504f6dfa16565" type="text/javascript"></script>
<script src="/media/jui/js/jquery-noconflict.js?5cc6fcc4bc3c8360a39504f6dfa16565" type="text/javascript"></script>
<script src="/media/jui/js/jquery-migrate.min.js?5cc6fcc4bc3c8360a39504f6dfa16565" type="text/javascript"></script>
<script src="/media/system/js/core.js?5cc6fcc4bc3c8360a39504f6dfa16565" type="text/javascript"></script>
<script defer="defer" src="/plugins/system/gdpr/assets/js/jquery.fancybox.min.js" type="text/javascript"></script>
<script defer="defer" src="/plugins/system/gdpr/assets/js/cookieconsent.min.js" type="text/javascript"></script>
<script defer="defer" src="/plugins/system/gdpr/assets/js/init.js" type="text/javascript"></script>
<script src="/media/widgetkit/uikit2-2212951d.js" type="text/javascript"></script>
<script src="/media/widgetkit/wk-scripts-ce27d824.js" type="text/javascript"></script>
<script type="text/javascript">
;(function ($) {
	$.ajaxSetup({
		headers: {
			'X-CSRF-Token': Joomla.getOptions('csrf.token')
		}
	});
})(jQuery);var gdprConfigurationOptions = { complianceType: 'opt-out',
																			  disableFirstReload: 0,
																	  		  blockJoomlaSessionCookie: 1,
																			  blockExternalCookiesDomains: 0,
																			  externalAdvancedBlockingModeCustomAttribute: '',
																			  allowedCookies: '',
																			  blockCookieDefine: 1,
																			  autoAcceptOnNextPage: 1,
																			  revokable: 0,
																			  lawByCountry: 0,
																			  checkboxLawByCountry: 0,
																			  cacheGeolocationCountry: 1,
																			  countryAcceptReloadTimeout: 1000,
																			  usaCCPARegions: null,
																			  dismissOnScroll: 0,
																			  dismissOnTimeout: 0,
																			  containerSelector: 'body',
																			  hideOnMobileDevices: 0,
																			  autoFloatingOnMobile: 0,
																			  autoFloatingOnMobileThreshold: 1024,
																			  autoRedirectOnDecline: 0,
																			  autoRedirectOnDeclineLink: '',
																			  showReloadMsg: 0,
																			  showReloadMsgText: 'Applying preferences and reloading the page...',
																			  defaultClosedToolbar: 0,
																			  toolbarLayout: 'basic-close',
																			  toolbarTheme: 'block',
																			  toolbarButtonsTheme: 'decline_first',
																			  revocableToolbarTheme: 'advanced',
																			  toolbarPosition: 'bottom-left',
																			  toolbarCenterTheme: 'compact',
																			  revokePosition: 'revoke-top',
																			  toolbarPositionmentType: 1,
																			  popupEffect: 'fade',
																			  popupBackground: '#000000',
																			  popupText: '#ffffff',
																			  popupLink: '#ffffff',
																			  buttonBackground: '#ffffff',
																			  buttonBorder: '#ffffff',
																			  buttonText: '#000000',
																			  highlightOpacity: '100',
																			  highlightBackground: '#333333',
																			  highlightBorder: '#ffffff',
																			  highlightText: '#ffffff',
																			  highlightDismissBackground: '#333333',
																		  	  highlightDismissBorder: '#ffffff',
																		 	  highlightDismissText: '#ffffff',
																			  hideRevokableButton: 0,
																			  hideRevokableButtonOnscroll: 0,
																			  customRevokableButton: 0,
																			  headerText: 'Cookies used on the website!',
																			  messageText: 'Questo sito web utilizza i Cookies per assicurarti la migliore esperienza sul nostro sito.',
																			  denyMessageEnabled: 1, 
																			  denyMessage: 'You have declined cookies, to ensure the best experience on this website please consent the cookie usage.',
																			  placeholderBlockedResources: 0, 
																			  placeholderBlockedResourcesAction: '',
																	  		  placeholderBlockedResourcesText: 'You must accept cookies and reload the page to view this content',
																			  placeholderIndividualBlockedResourcesText: 'You must accept cookies from {domain} and reload the page to view this content',
																			  dismissText: 'Ignora',
																			  allowText: 'Consenti',
																			  denyText: 'Rifiuta',
																			  cookiePolicyLinkText: 'Cookie policy',
																			  cookiePolicyLink: 'https://www.trovaweb.net/note-legali',
																			  cookiePolicyRevocableTabText: 'Cookie policy',
																			  privacyPolicyLinkText: 'Privacy policy',
																			  privacyPolicyLink: 'https://www.trovaweb.net/note-legali',
																			  categoriesCheckboxTemplate: 'cc-checkboxes-light',
																			  toggleCookieSettings: 0,
																	  		  toggleCookieSettingsText: '<span class="cc-cookie-settings-toggle">Settings <span class="cc-cookie-settings-toggler">&#x25EE;</span></span>',
																			  toggleCookieSettingsButtonBackground: '#333333',
																			  toggleCookieSettingsButtonBorder: '#FFFFFF',
																			  toggleCookieSettingsButtonText: '#FFFFFF',
																			  showLinks: 1,
																			  blankLinks: '_blank',
																			  autoOpenPrivacyPolicy: 0,
																			  openAlwaysDeclined: 1,
																			  cookieSettingsLabel: 'Impostazioni Cookies:',
															  				  cookieSettingsDesc: 'Scegli il tipo di cookie che desideri disattivare facendo clic sulle caselle di controllo. Fare clic sul nome di una categoria per ulteriori informazioni sui cookie utilizzati',
																			  cookieCategory1Enable: 1,
																			  cookieCategory1Name: 'Necessari',
																			  cookieCategory1Locked: 0,
																			  cookieCategory2Enable: 1,
																			  cookieCategory2Name: 'Preferenze',
																			  cookieCategory2Locked: 0,
																			  cookieCategory3Enable: 1,
																			  cookieCategory3Name: 'Statistiche',
																			  cookieCategory3Locked: 0,
																			  cookieCategory4Enable: 0,
																			  cookieCategory4Name: 'Marketing',
																			  cookieCategory4Locked: 0,
																			  cookieCategoriesDescriptions: {},
																			  alwaysReloadAfterCategoriesChange: 0,
																			  preserveLockedCategories: 0,
																			  reloadOnfirstDeclineall: 0,
																			  trackExistingCheckboxSelectors: '',
															  		  		  trackExistingCheckboxConsentLogsFormfields: 'name,email,subject,message',
																			  allowallShowbutton: 0,
																			  allowallText: 'Allow all cookies',
																			  allowallButtonBackground: '#FFFFFF',
																			  allowallButtonBorder: '#FFFFFF',
																			  allowallButtonText: '#000000',
																			  includeAcceptButton: 0,
																			  optoutIndividualResources: 0,
																			  externalAdvancedBlockingModeTags: 'iframe,script,img,source,link',
																			  debugMode: 0
																		};var gdpr_ajax_livesite='https://www.trovaweb.net/';var gdpr_enable_log_cookie_consent=1;var gdprUseCookieCategories=1;var gdpr_ajaxendpoint_cookie_category_desc='https://www.trovaweb.net/index.php?option=com_gdpr&task=user.getCookieCategoryDescription&format=raw';var gdprCookieCategoryDisabled1=1;var gdprCookieCategoryDisabled4=1;var gdprJSessCook='f36e3fdc449552febfa4b31e423e7720';var gdprJSessVal='97927aa84dd8d914940e178c9524295e';var gdprJAdminSessCook='e4e3bb7d7b79cb407b9386eba0becc7e';var gdprPropagateCategoriesSession=1;var gdprFancyboxWidth=700;var gdprFancyboxHeight=800;var gdprCloseText='Chiudi';var gdprUseFancyboxLinks=1;GOOGLE_MAPS_API_KEY = "AIzaSyDNNU4ZxRZS2WptrBakHLqhH5A_aDVMHCw";
	</script>
<!-- Start: Dati strutturati di Google  -->
<script type="application/ld+json"> { "@context": "https://schema.org", "@type": "WebSite", "url": "https://www.trovaweb.net", "name": "TrovaWeb", "alternateName": "Incubatore di Servizi Web per Aziende e Professionisti", "potentialAction": { "@type": "SearchAction", "target": "https://www.trovaweb.net/index.php?option=com_search&searchphrase=all&searchword={search_term}", "query-input": "required name=search_term" } } </script> <script type="application/ld+json"> { "@context": "https://schema.org", "@type": "Organization", "url": "https://www.trovaweb.net", "logo": "https://www.trovaweb.net/images/trovaweb-logo.png" } </script> <script type="application/ld+json"> { "@context": "https://schema.org", "@type": "Organization", "name": "TrovaWeb", "url": "https://www.trovaweb.net", "sameAs": [ "https://www.facebook.com/trovaweb/", "https://twitter.com/trovaweb", "https://www.instagram.com/trovaweb/" ] } </script> <script type="application/ld+json"> { "@context": "https://schema.org", "@type": "LocalBusiness", "@id": "https://www.trovaweb.net/%09%0A", "name": "TrovaWeb", "image": "https://www.trovaweb.net/images/trovaweb-logo.png", "url": "https://www.trovaweb.net/%09%0A", "telephone": "800681408", "priceRange": "€99 - €2000", "address": { "@type": "PostalAddress", "streetAddress": "Via Ansaldo Patti 28", "addressLocality": "Messina", "addressRegion": "ME", "postalCode": "98121", "addressCountry": "IT" }, "geo": { "@type": "GeoCoordinates", "latitude": "38.2041003", "longitude": "15.552750599999968" }, "openingHoursSpecification": [ { "@type": "OpeningHoursSpecification", "dayOfWeek": "Monday", "opens": "09:00", "closes": "18:00" }, { "@type": "OpeningHoursSpecification", "dayOfWeek": "Tuesday", "opens": "09:00", "closes": "18:00" }, { "@type": "OpeningHoursSpecification", "dayOfWeek": "Wednesday", "opens": "09:00", "closes": "18:00" }, { "@type": "OpeningHoursSpecification", "dayOfWeek": "Thursday", "opens": "09:00", "closes": "18:00" }, { "@type": "OpeningHoursSpecification", "dayOfWeek": "Friday", "opens": "09:00", "closes": "18:00" } ] } </script>
<!-- End: Dati strutturati di Google  -->
<!-- Joomla Facebook Integration Begin -->
<script type="text/javascript">
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '276001093821323', {}, {agent: 'pljoomla'});
fbq('track', 'PageView');
</script>
<noscript>
<img alt="fbpx" height="1" src="https://www.facebook.com/tr?id=276001093821323&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/>
</noscript>
<!-- DO NOT MODIFY -->
<!-- Joomla Facebook Integration end -->
</head>
<body>
<div class="error-page-inner " style="background-image: url();">
<div>
<div class="container">
<div class="error-logo-wrap">
<img alt="logo" class="error-logo" src="/templates/shaper_spectrum/images/presets/preset1/logo.png"/>
</div>
<img alt="logo" class="error-logo" src="https://www.trovaweb.net/images/demo/404.png"/>
<p class="error-message-title">OH CRAP!</p>
<p class="error-message">Page not found</p>
<a class="btn btn-primary btn-lg" href="/" title="HOME">Go Back Home</a>
</div>
</div>
</div>
</body>
</html>