<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	Zontera
</title><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><link href="favicon.ico" rel="icon" type="image/x-icon"/><link href="/css/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css"/><link href="/css/main.css" rel="stylesheet" type="text/css"/><script src="/controls/js/jquery.js" type="text/javascript"></script><script src="/controls/js/jquery-ui.js" type="text/javascript"></script><script src="/controls/js/jquery.autocomplete.js" type="text/javascript"></script><script src="/controls/js/AC_RunActiveContent.js" type="text/javascript"></script><script src="/controls/js/swfobject.js" type="text/javascript"></script><script src="/controls/js/common.js" type="text/javascript"></script><script src="/controls/js/slideshow.js" type="text/javascript"></script>
<script type="text/javascript">	
		var siteURL = 'http://www.zontera.com';
		var startupSlide = '';
	</script>
</head>
<body>
<div class="home clearfix" id="main_page">
<form action="default.aspx" id="aspnetForm" method="post" name="aspnetForm">
<input id="__VIEWSTATE" name="__VIEWSTATE" type="hidden" value="/wEPDwUKLTU4OTA1NjYzNA9kFgJmD2QWAgIDD2QWAmYPZBYCAgIPDxYCHghJbWFnZVVybAUsaHR0cDovL3d3dy56b250ZXJhLmNvbS9pbWFnZXMvYnRuX3NlYXJjaC5naWZkZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WAQUaY3RsMDAkdXNlcl9tZW51JGlidG5TZWFyY2j/nhOKEdtsFATgE5gN94X2t+8ijw=="/>
<input id="__EVENTVALIDATION" name="__EVENTVALIDATION" type="hidden" value="/wEWCgKQh9p7Aoet984PAum+tOsFAui+tOsFAuu+tOsFAuq+tOsFAu2+tOsFAuy+tOsFAu++tOsFAufg4cwFvizBVwfC6ho53I6s6uqNWeJpoeg="/>
<!--header-->
<div class="header">
<div class="top">
<div class="logo"><a href="http://www.zontera.com"><strong>Zontera</strong></a></div>
<div class="user_menu">
<div class="search">
<input class="input_search" id="ctl00_user_menu_search_field" name="ctl00$user_menu$search_field" onblur="CorrectSearch(this);" onclick="ClearSearch(this);" type="text" value="search..."/>
<select id="ctl00_user_menu_search_autocomplete" name="ctl00$user_menu$search_autocomplete" style="display:none">
<option value="1">Floating Flash</option>
<option value="2">Flash Video Boxless</option>
<option value="3">Transitional Flash</option>
<option value="4">Sticky Flash</option>
<option value="5">Video In-Stream &amp; Overlay</option>
<option value="6">Underline &amp; Text</option>
<option value="7">Site Splitter</option>
</select>
<input alt="search" border="0" class="btn_search" id="ctl00_user_menu_ibtnSearch" name="ctl00$user_menu$ibtnSearch" onclick="return CheckSearch();" src="http://www.zontera.com/images/btn_search.gif" type="image"/>
</div>
<div class="links">
<a href="http://www.zontera.com/inside/about_us.aspx" title="About us">About us</a>
<a href="http://www.zontera.com/contact.aspx" title="Contact">Contact</a>
<a class="login" href="http://myaccount.zontera.com/" title="Customer login">Customer login</a>
</div>
</div>
</div>
<div class="menu_container" id="menu_container">
<!--main_menu-->
<ul class="men" id="main_menu">
<li class="first"><a href="http://www.zontera.com"><img alt="" class="sel" src="http://www.zontera.com/images/icon_home.gif"/></a></li>
<li class="" id="1"><a href="http://www.zontera.com/inside/ad_technology.aspx" id="top_a_1">Ad Server</a>
<ul class="shadow" id="sub_ul_1" style="display:none;">
<li class="sub_1" id="sub_li_1"><a class="sub_1 sub_menu_link" href="http://www.zontera.com/inside/ad_technology/all_one_simplified.aspx" id="sub_a_1">All-In-One &amp; Simplified</a></li>
<li class="sub_1" id="sub_li_2"><a class="sub_1 sub_menu_link" href="http://www.zontera.com/inside/ad_technology/publishers.aspx" id="sub_a_2">for Publishers</a></li>
<li class="sub_1" id="sub_li_3"><a class="sub_1 sub_menu_link" href="http://www.zontera.com/inside/ad_technology/advertisers.aspx" id="sub_a_3">for Advertisers</a></li>
<li class="sub_1" id="sub_li_4"><a class="sub_1 sub_menu_link" href="http://www.zontera.com/inside/ad_technology/media_formats_creatives.aspx" id="sub_a_4">Media Formats &amp; Creatives</a></li>
<li class="sub_1" id="sub_li_5"><a class="sub_1 sub_menu_link" href="http://www.zontera.com/inside/ad_technology/video_advertising.aspx" id="sub_a_5">Video Advertising</a></li>
<li class="sub_1" id="sub_li_6"><a class="sub_1 sub_menu_link" href="http://www.zontera.com/inside/ad_technology/mobile_and_rss.aspx" id="sub_a_6">Mobile &amp; RSS</a></li>
<li class="sub_1" id="sub_li_7"><a class="sub_1 sub_menu_link" href="http://www.zontera.com/inside/ad_technology/contextual_targeting.aspx" id="sub_a_7">Contextual Targeting</a></li>
<li class="sub_1" id="sub_li_8"><a class="sub_1 sub_menu_link" href="http://www.zontera.com/inside/ad_technology/pricing.aspx" id="sub_a_8">Pricing</a></li>
<li class="sub_1" id="sub_li_9"><a class="sub_1 sub_menu_link" href="http://www.zontera.com/inside/ad_technology/privacy.aspx" id="sub_a_9">Privacy policy</a></li>
</ul>
</li>
<li class="" id="2"><a href="http://www.zontera.com/inside/ad_gallery.aspx" id="top_a_2">Ad Gallery</a>
<ul class="shadow" id="sub_ul_2" style="display:none;">
<li class="sub_2" id="sub_li_10"><a class="sub_2 sub_menu_link" href="http://www.zontera.com/inside/ad_gallery/floating_expandable_flash.aspx" id="sub_a_10">Floating &amp; Expandable Flash</a></li>
<li class="sub_2" id="sub_li_11"><a class="sub_2 sub_menu_link" href="http://www.zontera.com/inside/ad_gallery/peel_away.aspx" id="sub_a_11">Peel Away</a></li>
<li class="sub_2" id="sub_li_12"><a class="sub_2 sub_menu_link" href="http://www.zontera.com/inside/ad_gallery/site_splitter.aspx" id="sub_a_12">Site Splitter</a></li>
<li class="sub_2" id="sub_li_13"><a class="sub_2 sub_menu_link" href="http://www.zontera.com/inside/ad_gallery/sticky_flash.aspx" id="sub_a_13">Sticky Flash</a></li>
<li class="sub_2" id="sub_li_14"><a class="sub_2 sub_menu_link" href="http://www.zontera.com/inside/ad_gallery/underline_text.aspx" id="sub_a_14">Underline and Text</a></li>
<li class="sub_2" id="sub_li_15"><a class="sub_2 sub_menu_link" href="http://www.zontera.com/inside/ad_gallery/video_instream_overlay.aspx" id="sub_a_15">Video In-Stream and Overlay</a></li>
</ul>
</li>
<li class="none" id="3"><a href="http://www.zontera.com/inside/rising_stars.aspx" id="top_a_3">Rising Stars</a>
</li>
<li class="none last" id="4"><a href="http://www.zontera.com/inside/solutions.aspx" id="top_a_4">Solutions</a>
</li>
</ul>
<!--//main_menu-->
</div>
<div class="flash flash_text">
<div class="banner">
<div class="text_banner" id="default_slide">
<h1>benefit <br/>from the best <strong>ad technology</strong></h1>
<div class="text_b">
<p>X1 Ad Management System is a complete display, video, mobile and contextual ad server which can be accessed by both publisher/network/media house and agency/advertiser/account, for both brand advertising/direct response campaigns.</p>
</div>
<div class="btn_area"><a href="javascript://" onclick="GoToFlash('/new_introflash/preloader_display.swf')">play this movie now</a></div>
</div>
<div class="slides_container">
<div class="text_banner" id="display_ads_slide" style="display:none;">
<h1><strong>Display</strong>advertising</h1>
<div class="text_b">
<p>Connect with consumers via standard Display and Rich Media Ads like Flash, Expandable, Floating, Interstitials, Transitionals and so on.</p>
<br/>
<ul class="list">
<li><span>Full rich media kit</span></li>
<li><span>Inventory &amp; Optimization</span></li>
<li><span>Contextual engine</span></li>
<li><span>Flexible Licensing</span></li>
</ul>
</div>
<div class="btn_area"><a href="inside/ad_technology.aspx">More...</a></div>
</div>
<div class="text_banner" id="invideo_ads_slide" style="display:none;">
<h1><strong>In Video</strong>advertising</h1>
<div class="text_b">
<p>	Offers publishers and advertisers connection with consumers via Video Ads and extend your audience with pre-roll, mid-roll, post-roll and overlay in video stream creatives.</p>
<br/>
<ul class="list">
<li><span>Linear ads and Overlayers</span></li>
<li><span>Advertising breaks</span></li>
<li><span>Ads in live-streams</span></li>
</ul>
</div>
<div class="btn_area"><a href="inside/ad_technology/video_advertising.aspx">More...</a></div>
</div>
<div class="text_banner" id="intext_ads_slide" style="display:none;">
<h1><strong>Text</strong>advertising</h1>
<div class="text_b">
<p>Built-in, ready-to-use makes Contextual Ad Delivery with X1 Ad Management Server work easier. Use the power of Keywords.</p>
<br/>
<ul class="list">
<li><span>In-text ads</span></li>
<li><span>Classified ads</span></li>
<li><span>Contextual engine</span></li>
</ul>
</div>
<div class="btn_area"><a href="inside/ad_technology/contextual_targeting.aspx">More...</a></div>
</div>
<div class="text_banner" id="mobile_ads_slide" style="display:none;">
<h1><strong>Mobile</strong>advertising</h1>
<div class="text_b">
<p>Extend the audience deploying Ads in mobile web browsers or in rss syndications feeds.</p>
<br/>
<ul class="list">
<li><span>Image</span></li>
<li><span>Expandable</span></li>
<li><span>Push down creatives</span></li>
<li><span>in RSS ads</span></li>
<li><span>Complete tracking</span></li>
</ul>
</div>
<div class="btn_area"><a href="inside/ad_technology/mobile_and_rss.aspx">More...</a></div>
</div>
<div class="text_banner" id="in_image_slide" style="display:none;">
<h1><strong>In Image</strong>advertising</h1>
<div class="text_b">
<p>Capture the consumer visual engagement</p>
<p>with over the image ads</p>
<br/>
<ul class="list">
<li><span>Image</span></li>
<li><span>Image &amp; Text</span></li>
<li><span>Flash</span></li>
<li><span>Flash Expandable</span></li>
</ul>
</div>
<div class="btn_area"><a href="banners/in_image_units/expandable_flash/">More...</a></div>
</div>
<div class="text_banner" id="demo_slide" style="display:none;">
<h1><strong>Demographic</strong>prediction</h1>
<div class="text_b">
<p>Target ads on demographic patterns</p>
<br/>
<ul class="list">
<li><span>Basic demographic segments:</span></li>
<li><span>gender, age, married, children, income, education</span></li>
<li><span>Sociographic patterns</span></li>
</ul>
</div>
<div class="btn_area"><a href="inside/ad_technology/contextual_targeting.aspx">More...</a></div>
</div>
</div>
<div class="flash_banner">
<a href="javascript://" onclick="TogglePlay()"><img alt="" id="banner_default_img" src="images/img_banner.png" style="margin-top:3px; border:0px solid #ccc;"/></a>
<div id="bannerID">
<p style="font-size:14px; margin-top:30px;"> In order to see this presentation, you must have installed at least Flash Player version 10.0.12.36. 
								<br/>Click on the button to get the latest Flash Player version.</p>
<br/>
<p class="btn_area auto">
<a href="http://www.adobe.com/go/getflashplayer/" title="Get Flash Player">Get Flash Player</a>
</p>
</div>
</div>
</div>
<div class="flash_control">
<div class="control_btn"><a class="play" href="javascript://" id="play_pause" onclick="TogglePlay()"> </a></div>
<div class="controls">
<div class="buttons">
<a class="image_adv" href="javascript://" id="image_adv" onclick="GoToFlash('/new_introflash/preloader_display.swf')">
<strong>
<img alt="" src="http://www.zontera.com/images/image_adv_h.png" style="margin-left:2px; margin-top:0px; height:50px;"/>
<em><b>Display advertising</b></em>
</strong>
</a>
<a class="video_adv" href="javascript://" id="video_adv" onclick="GoToFlash('/new_introflash/preloader_video.swf')">
<strong>
<img alt="" src="http://www.zontera.com/images/video_adv_h.png" style="margin-left:2px; margin-top:0px; height:50px;"/>
<em><b>Video advertising</b></em>
</strong>
</a>
<a class="text_adv" href="javascript://" id="text_adv" onclick="GoToFlash('/new_introflash/preloader_text.swf')">
<strong>
<img alt="" src="http://www.zontera.com/images/text_adv_h.png" style="margin-left:2px; margin-top:0px; height:50px;"/>
<em><b>Text advertising</b></em>
</strong>
</a>
<a class="mobile_adv" href="javascript://" id="mobile_adv" onclick="GoToFlash('/new_introflash/preloader_mobile.swf')">
<strong>
<img alt="" src="http://www.zontera.com/images/mobile_adv_h.png" style="margin-left:2px; margin-top:0px; height:50px;"/>
<em><b>Mobile advertising</b></em>
</strong>
</a>
<a class="in_image_adv" href="javascript://" id="in_image_adv" onclick="GoToFlash('/new_introflash/preloader_image.swf')">
<strong>
<img alt="" src="http://www.zontera.com/images/in_image_adv_h.png" style="margin-left:2px; margin-top:0px; height:50px;"/>
<em><b>In image advertising</b></em>
</strong>
</a>
<a class="demo_adv" href="javascript://" id="demo_adv" onclick="GoToFlash('/new_introflash/preloader_matcher.swf')">
<strong>
<img alt="" src="http://www.zontera.com/images/demo2_adv_h.png" style="margin-left:2px; margin-top:0px; height:50px;"/>
<em><b>Demographic prediction</b></em>
</strong>
</a>
</div>
<div id="sliderImage" style="margin-left:10px"></div>
<div id="sliderVideo"></div>
<div id="sliderText"></div>
<div id="sliderMobile"></div>
<div id="sliderInImage"></div>
<div id="sliderDemo"></div>
</div>
</div>
</div>
</div>
<!--//header-->
<div id="container">
<div class="content_home">
<!--content-->
<div class="publishers">
<a href="http://www.zontera.com/inside/ad_technology/publishers.aspx" title="are you a publisher?"><h2><strong>are you a publisher?</strong></h2></a>
<p>Whether you are a publisher of a single website, or you manage a whole network of websites, the Zontera all-in-one adserving solution makes it easy for you to manage all your inventory and all the advertising options you wish to offer.</p>
<div class="btn_area"><a href="http://www.zontera.com/inside/ad_technology/publishers.aspx" title="are you a publisher?">learn more</a></div>
</div>
<div class="advertisers">
<a href="http://www.zontera.com/inside/ad_technology/advertisers.aspx" title="are you an advertiser or agency?"><h2><strong>are you an advertiser or agency?</strong></h2></a>
<p>Do you want to manage all your online advertising in one place, so that you can make true apples-by-apples comparisons? Meet Zontera, the all-in-one solution for your third-party adserving needs.</p>
<div class="btn_area"><a href="http://www.zontera.com/inside/ad_technology/advertisers.aspx" title="are you an advertiser or agency?">learn more</a></div>
</div>
<!--//content-->
</div>
<div class="clear"></div>
</div>
<script type="text/javascript">
//<![CDATA[
var timeoutId1; var timeoutId2; var timeoutId3; var timeoutId4; var timerArr = new Array(timeoutId1,timeoutId2,timeoutId3,timeoutId4); //]]>
</script>
</form>
<div id="footer">
<div class="footer_content clearfix">
<span>Copyright 2011©zontera.com</span>
<div>
<a href="http://www.zontera.com" title="home">home</a>
<a href="http://www.zontera.com/inside/about_us.aspx" title="about us">about us</a>
<a href="http://www.zontera.com/contact.aspx" title="contact">contact</a>
</div>
</div>
<script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-6014753-3']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
</script>
</div>
</div>
</body>
</html>
