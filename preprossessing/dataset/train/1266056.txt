<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<title>Coming Soon – sanrio.com.hk</title>
<meta content="noindex,follow" name="robots"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://sanrio.com.hk/feed/" rel="alternate" title="sanrio.com.hk » Feed" type="application/rss+xml"/>
<link href="https://sanrio.com.hk/comments/feed/" rel="alternate" title="sanrio.com.hk » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/sanrio.com.hk\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://sanrio.com.hk/wp-includes/css/dist/block-library/style.min.css?ver=5.2.9" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sanrio.com.hk/wp-content/themes/hello-elementor/style.min.css?ver=2.2.0" id="hello-elementor-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sanrio.com.hk/wp-content/themes/hello-elementor/theme.min.css?ver=2.2.0" id="hello-elementor-theme-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sanrio.com.hk/wp-json/" rel="https://api.w.org/"/>
<link href="https://sanrio.com.hk/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://sanrio.com.hk/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.2.9" name="generator"/>
<link href="https://sanrio.com.hk/?elementor_library=coming-soon" rel="canonical"/>
<link href="https://sanrio.com.hk/?p=6" rel="shortlink"/>
<link href="https://sanrio.com.hk/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fsanrio.com.hk%2F%3Felementor_library%3Dcoming-soon" rel="alternate" type="application/json+oembed"/>
<link href="https://sanrio.com.hk/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fsanrio.com.hk%2F%3Felementor_library%3Dcoming-soon&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<style>#wp-admin-bar-elementor-maintenance-on > a { background-color: #dc3232; }
			#wp-admin-bar-elementor-maintenance-on > .ab-item:before { content: "\f160"; top: 2px; }</style>
<meta content="width=device-width, initial-scale=1.0, viewport-fit=cover" name="viewport"/></head>
<body class="elementor_library-template elementor_library-template-elementor_canvas single single-elementor_library postid-6 elementor-default elementor-template-canvas elementor-page elementor-page-6 elementor-maintenance-mode">
<div class="elementor elementor-6" data-elementor-id="6" data-elementor-settings="[]" data-elementor-type="page">
<div class="elementor-inner">
<div class="elementor-section-wrap">
<section class="elementor-element elementor-element-060cbf2 elementor-section-height-full elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section" data-element_type="section" data-id="060cbf2">
<div class="elementor-container elementor-column-gap-no">
<div class="elementor-row">
<div class="elementor-element elementor-element-1a1d780 elementor-column elementor-col-100 elementor-top-column" data-element_type="column" data-id="1a1d780">
<div class="elementor-column-wrap elementor-element-populated">
<div class="elementor-widget-wrap">
<div class="elementor-element elementor-element-e60021f elementor-hidden-phone elementor-widget elementor-widget-image" data-element_type="widget" data-id="e60021f" data-widget_type="image.default">
<div class="elementor-widget-container">
<div class="elementor-image">
<img alt="" class="attachment-full size-full" height="667" sizes="(max-width: 1340px) 100vw, 1340px" src="https://sanrio.com.hk/wp-content/uploads/2019/09/coming_soon.top_.lg_.png" srcset="https://sanrio.com.hk/wp-content/uploads/2019/09/coming_soon.top_.lg_.png 1340w, https://sanrio.com.hk/wp-content/uploads/2019/09/coming_soon.top_.lg_-800x398.png 800w, https://sanrio.com.hk/wp-content/uploads/2019/09/coming_soon.top_.lg_-768x382.png 768w, https://sanrio.com.hk/wp-content/uploads/2019/09/coming_soon.top_.lg_-1200x597.png 1200w" width="1340"/> </div>
</div>
</div>
<div class="elementor-element elementor-element-b55a6d0 elementor-hidden-desktop elementor-hidden-tablet elementor-widget elementor-widget-image" data-element_type="widget" data-id="b55a6d0" data-widget_type="image.default">
<div class="elementor-widget-container">
<div class="elementor-image">
<img alt="" class="attachment-full size-full" height="793" src="https://sanrio.com.hk/wp-content/uploads/2019/09/coming_soon.top_.sm_.png" width="600"/> </div>
</div>
</div>
<section class="elementor-element elementor-element-049453e elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section" data-id="049453e">
<div class="elementor-container elementor-column-gap-no">
<div class="elementor-row">
<div class="elementor-element elementor-element-471e797 elementor-column elementor-col-66 elementor-inner-column" data-element_type="column" data-id="471e797">
<div class="elementor-column-wrap">
<div class="elementor-widget-wrap">
</div>
</div>
</div>
<div class="elementor-element elementor-element-2bf6fed elementor-column elementor-col-33 elementor-inner-column" data-element_type="column" data-id="2bf6fed">
<div class="elementor-column-wrap elementor-element-populated">
<div class="elementor-widget-wrap">
<div class="elementor-element elementor-element-177776f elementor-widget elementor-widget-image" data-element_type="widget" data-id="177776f" data-widget_type="image.default">
<div class="elementor-widget-container">
<div class="elementor-image">
<img alt="" class="attachment-large size-large" height="70" src="https://sanrio.com.hk/wp-content/uploads/2019/09/coming_soon.bottom.lg_.png" width="348"/> </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="elementor-element elementor-element-583357a elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section" data-id="583357a">
<div class="elementor-container elementor-column-gap-no">
<div class="elementor-row">
<div class="elementor-element elementor-element-0c21b0c elementor-column elementor-col-66 elementor-inner-column" data-element_type="column" data-id="0c21b0c">
<div class="elementor-column-wrap">
<div class="elementor-widget-wrap">
</div>
</div>
</div>
<div class="elementor-element elementor-element-7a37165 elementor-column elementor-col-33 elementor-inner-column" data-element_type="column" data-id="7a37165">
<div class="elementor-column-wrap elementor-element-populated">
<div class="elementor-widget-wrap">
<div class="elementor-element elementor-element-2de68eb elementor-widget__width-auto elementor-widget elementor-widget-image" data-element_type="widget" data-id="2de68eb" data-widget_type="image.default">
<div class="elementor-widget-container">
<div class="elementor-image">
<a data-elementor-open-lightbox="" href="https://www.facebook.com/pg/sanriohongkong">
<img alt="ic_facebook" src="https://sanrio.com.hk/wp-content/uploads/elementor/thumbs/ic_facebook-odniw3bmsbhg6l7jyjckgsbanib1fa43xangwham9c.png" title="ic_facebook"/> </a>
</div>
</div>
</div>
<div class="elementor-element elementor-element-06d73a5 elementor-widget__width-auto elementor-widget elementor-widget-image" data-element_type="widget" data-id="06d73a5" data-widget_type="image.default">
<div class="elementor-widget-container">
<div class="elementor-image">
<a data-elementor-open-lightbox="" href="https://www.instagram.com/sanrio.hk/">
<img alt="ic_instagram" src="https://sanrio.com.hk/wp-content/uploads/elementor/thumbs/ic_instagram-odnix9mdbv3cp1i43jms2zn1duik1ns1340bhxjwhc.png" title="ic_instagram"/> </a>
</div>
</div>
</div>
<div class="elementor-element elementor-element-a81d40c elementor-widget__width-auto elementor-widget elementor-widget-image" data-element_type="widget" data-id="a81d40c" data-widget_type="image.default">
<div class="elementor-widget-container">
<div class="elementor-image">
<a data-elementor-open-lightbox="" href="mailto:marketing@sanrio.com.hk">
<img alt="ic_email" src="https://sanrio.com.hk/wp-content/uploads/elementor/thumbs/ic_email-odnixj0r87g7x54gknp1rx9nbp886mtcgej6ap5yr4.png" title="ic_email"/> </a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
<link href="https://sanrio.com.hk/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=2.7.1" id="elementor-frontend-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sanrio.com.hk/wp-content/uploads/elementor/css/post-6.css?ver=1568883407" id="elementor-post-6-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sanrio.com.hk/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.4.0" id="elementor-icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sanrio.com.hk/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=2.7.1" id="elementor-animations-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sanrio.com.hk/wp-content/plugins/elementor-pro/assets/css/frontend.min.css?ver=2.6.5" id="elementor-pro-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sanrio.com.hk/wp-content/uploads/elementor/css/global.css?ver=1568883407" id="elementor-global-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;ver=5.2.9" id="google-fonts-1-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://sanrio.com.hk/wp-includes/js/wp-embed.min.js?ver=5.2.9" type="text/javascript"></script>
<script src="https://sanrio.com.hk/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://sanrio.com.hk/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://sanrio.com.hk/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=2.7.1" type="text/javascript"></script>
<script src="https://sanrio.com.hk/wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js?ver=2.6.5" type="text/javascript"></script>
<script type="text/javascript">
var ElementorProFrontendConfig = {"ajaxurl":"https:\/\/sanrio.com.hk\/wp-admin\/admin-ajax.php","nonce":"60dd9e0520","shareButtonsNetworks":{"facebook":{"title":"Facebook","has_counter":true},"twitter":{"title":"Twitter"},"google":{"title":"Google+","has_counter":true},"linkedin":{"title":"LinkedIn","has_counter":true},"pinterest":{"title":"Pinterest","has_counter":true},"reddit":{"title":"Reddit","has_counter":true},"vk":{"title":"VK","has_counter":true},"odnoklassniki":{"title":"OK","has_counter":true},"tumblr":{"title":"Tumblr"},"delicious":{"title":"Delicious"},"digg":{"title":"Digg"},"skype":{"title":"Skype"},"stumbleupon":{"title":"StumbleUpon","has_counter":true},"telegram":{"title":"Telegram"},"pocket":{"title":"Pocket","has_counter":true},"xing":{"title":"XING","has_counter":true},"whatsapp":{"title":"WhatsApp"},"email":{"title":"Email"},"print":{"title":"Print"}},"facebook_sdk":{"lang":"en_US","app_id":""}};
</script>
<script src="https://sanrio.com.hk/wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=2.6.5" type="text/javascript"></script>
<script src="https://sanrio.com.hk/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://sanrio.com.hk/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.7.3" type="text/javascript"></script>
<script src="https://sanrio.com.hk/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2" type="text/javascript"></script>
<script src="https://sanrio.com.hk/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=4.4.6" type="text/javascript"></script>
<script type="text/javascript">
var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"2.7.1","urls":{"assets":"https:\/\/sanrio.com.hk\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"general":{"elementor_global_image_lightbox":"yes","elementor_enable_lightbox_in_editor":"yes"}},"post":{"id":6,"title":"Coming Soon","excerpt":""}};
</script>
<script src="https://sanrio.com.hk/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=2.7.1" type="text/javascript"></script>
</body>
</html>
